@ECHO OFF
ECHO.
ECHO Do a fully-automated, clean build,
ECHO with build-number increment and Git updates?
ECHO.
ECHO Requirements: VS2015, Inno Setup 5.5, Git
ECHO.
ECHO All uncommited changes will be stashed.
rem ECHO Version number will be incremented and committed to repository
ECHO.
ECHO Ctrl-C to quit
PAUSE


REM Execute a powershell script that has the same path and name as this batch file (but different extension).
powershell -executionpolicy bypass "& '%~dpn0.ps1'"

PAUSE
