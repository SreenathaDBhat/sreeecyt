#
# To run this or other Powershell scripts without having to change the system execution policy,
# at the commandline or in a batch file type: powershell -executionpolicy bypass full\path\script.ps1.
# It's a good idea to view and edit this file in the 'Powershell Integrated Scripting Environment',
# where you get syntax colouring and online help, amongst other things. An easy way to to this is
# in Explorer, right-click this script and select 'Edit' from the context menu.
#



# Enforce minimum PowerShell version
#requires -version 2.0 
 
# Automatically implement the standard -Verbose, -Debug, -ErrorAction, -ErrorVariable, -Confirm, and -?
# arguments, etc.
[CmdletBinding()]
param
(
)
 
# Enforce all errors to become terminating unless you override with per-command -ErrorAction parameters
# or wrap in a try-catch block.
$script:ErrorActionPreference = "Stop"

# Ensure you only refer to variables that exist and enforces some other “best-practice” coding rules.
Set-StrictMode -Version Latest

# Gets the absolute path of the folder containing the script that is running - useful when referring to
# other files relative to the script, without relying on the current working directory. The variable name
# is chosen as such because this is the name of the variable PowerShell gives you for free in v3.
$PSScriptRoot = $MyInvocation.MyCommand.Path | Split-Path


# Include the file containing shared functions
. "$PSScriptRoot\sharedfunctions.ps1"




#function ReadVersion
#{
#    $versionFile = $args[0]
#    Casting content of version file to Version type will ensure it is in correct format,
#    as well as making it easier to modify.
#	[Version](get-content $versionFile -totalcount 1)
#}
#function WriteVersion
#{
#    $versionFile = $args[0]
#    $version     = $args[1]
#    set-content $versionFile $version
#}


function LockOpenVersionFileForRead
{
    $versionFileName = $args[0]

    # Open the file for exclusive access by this process. Allow both reading and writing, so a stream
    # writer can be attached later.
    # It seems that the file is only actually opened when a stream reader or writer is attached though.
    # Don't need to explicitly close it, as closing an attached stream reader or writer will do this
    # and in fact we want it to stay locked until the process exits.
    $versionFileStream = [System.IO.File]::Open($versionFileName, 'Open', 'ReadWrite', 'None')

    # It seems that the file is only actually opened when a stream reader or writer is attached,
    # so immediately do this too, to deny access to the file by other processes. Don't close
    # the reader while the script is running to ensure that this lock is maintained.
    # This ensures that version numbers are unique and sequential.
    $versionFileStreamReader = new-object System.IO.StreamReader($versionFileStream)
    
    return $versionFileStreamReader
}

function OpenVersionFileForWrite
{
    # Start with the stream reader returned by function LockOpenVersionFile.
    $versionFileStreamReader = $args[0]
    
    # Get the underlying stream (assuming stream was opened for both reading and writing).
    $versionFileStream = $versionFileStreamReader.BaseStream    
    
    # Truncate the file before writing the new version.
    $versionFileStream.SetLength(0)

    # Attach a stream writer to the stream. Don't close the already attached
    # stream reader, as that would close the underlying stream.
    $versionFileStreamWriter = new-object System.IO.StreamWriter($versionFileStream)
    
    return $versionFileStreamWriter
}


function SetAssemblyInformationalVersion
{
# AssemblyInformationalVersion is what is shown as 'Product version' in an assembly file's properties.
# Replace AssemblyInformationalVersion line in specified file (should be a project's AssemblyInfo.cs),
# with "[assembly: AssemblyInformationalVersion("a.b.c.d")]", where a.b.c.d is the specified version
	$filename = $args[0]
	$version  = $args[1]

    # Get file contents and pipe, line-by-line, to the foreach-object cmdlet, which replaces the contents
    # of the line if it contains the AssemblyInformationalVersion, then pipes the line to set-content,
    # which writes it back to the file. The get-content operation is surrounded by braces to make it
    # complete reading the file before outputting anything, so that set-content is able to write to it.
	(get-content $filename) | foreach-object {

			if ($_.Contains('[assembly: AssemblyInformationalVersion('))
			{
				$_ = '[assembly: AssemblyInformationalVersion("' + $version + '")]'
			}

			$_  # Pass this line through to next pipeline stage.

	} |	set-content $args[0] -encoding UTF8
}


function GitCheckCommits
{
    $lastBuildHeadFile = $args[0]
    $currentHeadFile   = $args[1]    

    write-host 'Stashing any uncommitted changes'
    start-process $git 'stash', 'save', '-u' -wait

    write-host 'Pulling the latest changes to the current branch from the origin repository'
    start-process $git 'pull' -wait

    write-host 'Checking whether anything has changed'
    start-process $git 'rev-parse', 'HEAD' -redirectstandardoutput $currentHeadFile -wait

    if (test-path $lastBuildHeadFile)
    {
        $diffs = compare-object (get-content $currentHeadFile) (get-content $lastBuildHeadFile)
    }
    else
    {
        $diffs = 'don''t know last build head'
    }
    
    return $diffs
}

function GitCleanTree
{
    # Prevent git clean stopping for user input if it fails to delete a file when cleaning.
    $env:GIT_ASK_YESNO = 'false'

    #get-childitem env:

    # For some reason Git often fails to remove the BuildOutput folder, so do it with rmdir.
    #RMDIR /S /Q BuildOutput

    write-host 'Cleaning the working tree of all untracked files, e.g. generated files'
    start-process $git 'clean', '-d', '-x', '-f', '-q' -wait

    # TODO: If the clean has failed, quit with an error!?
}

function GitStoreChanges
{
    $lastBuildHeadFile = $args[0]

    write-host 'Committing changes'
WriteToHostAndAppendToBuildLogFile "Git:"
WriteToHostAndAppendToBuildLogFile $git
    # Commit all files modified by the build, then do a pull before pushing the commit to origin, in case any commits have
    # been made to the origin repository during the build, as the push will fail if the local copy of the branch is not up
    # to date first. Must do the commit before the pull, even though this might cause a merge commit to be created when the
    # pull is done, as otherwise it will appear that commits to the origin repository during the build were incorporated
    # into the build (because our commit records the state of the tree at the time the build was done).
    # Note that the commit isn't done earlier in the build in case the build fails.    
    start-process $git 'add', '-A' -wait
    start-process $git 'commit', '-m', $version -wait
    start-process $git 'pull' -wait
    start-process $git 'push', 'origin', 'HEAD'  # Only do this for fully automated builds        

        
    # Store the HEAD hash corresponding to this build. This can later be compared
    # with the current HEAD, to see whether another build needs to be done.
    # Note that it must be stored outside the respository as its value isn't known
    # until after the build generated changes have been committed and it must still
    # exist after untracked files have been stashed and a pull has been done, the
    # next time a build is done.
    start-process $git 'rev-parse', 'HEAD' -redirectstandardoutput $lastBuildHeadFile -wait        
	WriteToHostAndAppendToBuildLogFile "Build Pushed to  git"
}

function UpdateProjectUIDs
{
# generate uids for given project using msbuild
    $msbuildPath = $args[0]
    $projectFileRelativePath = $args[1]
    
    WriteToHostAndAppendToBuildLogFile "update uids $projectFileRelativePath"

    $process = start-process $msbuildPath $projectFileRelativePath, '/t:updateuid' -wait -passthru
        
    return $process.ExitCode
}

function UpdateAllUIDs
{
# generate uids for all necessary projects, add new ones here, if they have xaml 
# resources that need translating (also add to lingobits project, .loc file)
    $buildResult = 0

    WriteToHostAndAppendToBuildLogFile "Updating all uids"
    # if ($buildResult -eq 0){ $buildResult = UpdateProjectUIDs $msbuild 'Scanner\LBS.ScanSystem.ScannerConsole\LBS.ScanSystem.ScannerConsole.csproj'}
    # if ($buildResult -eq 0){ $buildResult = UpdateProjectUIDs $msbuild 'QCModule\DigitalSlideView\DigitalSlideView.csproj'}
    # if ($buildResult -eq 0){ $buildResult = UpdateProjectUIDs $msbuild 'QCModule\Gateway\Gateway.csproj'}    
    # if ($buildResult -eq 0){ $buildResult = UpdateProjectUIDs $msbuild 'QCModule\StringResources\StringResources.csproj'}        

    return $buildResult
}

function RunLingobit
{
# run the lingobits project, this will build the translated resources in the culture folders
    $lingobitPath = $args[0]
    $locFilePath = $args[1]
    
    WriteToHostAndAppendToBuildLogFile "run lingobit $lingobitPath $locFilePath"

    $process = start-process $lingobitPath 'VersaLingobit\VersaLingobit.loc', '-silent','-lang:all', '-create', '-scan', '-save'  -wait -passthru
    return $process.ExitCode
}

###############################################################################
# Script execution starts here!
###############################################################################

# $scriptFolder = Split-Path -parent $MyInvocation.MyCommand.Path
# & "$scriptFolder\buildinternals.bat"

if (test-path 'env:\ProgramFiles(x86)')
{
    write-host '64-bit system detected'
    $programFiles32 = ${env:ProgramFiles(x86)}
}
else
{
    write-host '32-bit system detected'
    $programFiles32 = ${env:ProgramFiles}    
}

$PSScriptRoot = $MyInvocation.MyCommand.Path | Split-Path
WriteToHostAndAppendToBuildLogFile  "Current dir:"
WriteToHostAndAppendToBuildLogFile  $PSScriptRoot
$git = ${env:ProgramFiles} + '\Git\bin\git.exe'
$devenv2015 = $programFiles32 + '\Microsoft Visual Studio 14.0\Common7\IDE\devenv.exe'
$innosetup  = $programFiles32 + '\Inno Setup 5\ISCC.exe'
$lastBuildHeadFile = 'd:\#cytoappslastbuildhead'
$currentHeadFile   = 'd:\#cytoappscurrenthead'
$versionFilePath = $PSScriptRoot + '\CytoAppsVersion.txt'
$buildsFolder = $PSScriptRoot + '\InnoSetup\Output'
# The build log file can't be within the source tree as logging starts before the tree is cleaned.
$buildLogFile = 'e:\cytoappslastbuildlog.txt'
#$lingobit = $programFiles32 + '\lingobit localizer\localizer.com'
$msbuild = ${env:windir} + '\Microsoft.NET\Framework\v4.0.30319\msbuild.exe'

WriteToHostAndAppendToBuildLogFile "Starting Script."

# Truncate any previous log file and put date at top
get-date | out-file $buildLogFile


###############################################################################
# Open version file for exclusive access and read the version.

write-host "Reading version from $versionFilePath"

$versionFileStreamReader = LockOpenVersionFileForRead($versionFilePath)

if ($versionFileStreamReader -eq $null)
{
    WriteToHostAndAppendToBuildLogFile "Unable to lock $versionFilePath. Quitting."
    exit
}

# Read the version number from the file. Casting content of version file to Version type
# will ensure it is in correct format, as well as making it easier to modify.
$version = [Version]$versionFileStreamReader.ReadLine()

if ($version -eq $null -or $version.Revision -lt 0)
{
    WriteToHostAndAppendToBuildLogFile 'Version number could not be parsed! Must be 4-part version number. Quitting.'
    exit
}

# Increment revision number part of Version object
# (assuming we want to stick with previous versioning scheme of major.minor.subminor.build)
$version = new-object Version($version.Major, $version.Minor, $version.Build, ($version.Revision + 1))

WriteToHostAndAppendToBuildLogFile "Building new version $version"


###############################################################################
# Use Git to check whether anything has changed and clean the tree.

$diffs = GitCheckCommits $lastBuildHeadFile $currentHeadFile

if ($diffs -eq $null)
{
   WriteToHostAndAppendToBuildLogFile 'Nothing committed since last build. Quitting.'
   exit
}


GitCleanTree


###############################################################################
# Set the version in the assembly information for the main program and create a version.h for MFC programs.
# Note this must be done AFTER cleaning the tree!
$AssemblyPath= (get-item $PSScriptRoot ).parent.FullName  + "\Source\CaseBrowserModule\CaseBrowserModule\Properties\AssemblyInfo.cs" 
SetAssemblyInformationalVersion $AssemblyPath $version

#"#define VERSA_VERSION """ + $version + """" | out-file 'version.h'


###############################################################################
# Update any xaml resources with UIDs

#$updateUidsResult = 0
#$updateUidsResult = UpdateAllUids

#if ($updateUidsResult -ne 0)
#{ 
#    WriteToHostAndAppendToBuildLogFile 'Update UIDs failed. Quitting.'
#    exit
#}

###############################################################################
# Build the solutions
$SolutionPath=(get-item $PSScriptRoot ).parent.FullName
$buildResult = BuildAllSolutions $SolutionPath

if ($buildResult -ne 0)
{ 
    WriteToHostAndAppendToBuildLogFile 'Building solutions failed. Quitting.'
    exit
}

###############################################################################
# Generate languages with lingobit - Enable for VERSA translations
#$langResult = 0
#$langResult = RunLingobit $lingobit 'VersaLingobit\VersaLingobit.loc'

#if ($langResult -ne 0)
#{ 
#    WriteToHostAndAppendToBuildLogFile 'Lingobit fail. Quitting.'
#    exit
#}

###############################################################################
# Build the installer

write-host 'Building installer'

# Must use the -passthru parameter with start-process for it to return a value.
# Note that InnoSetup must have the preprocessor add-on installed with it.
$InnoSetupPath= $PSScriptRoot + "\InnoSetup\CytoApps.iss"
$installerProcess = start-process $innosetup $InnoSetupPath -wait -passthru

if ($installerProcess.ExitCode -ne 0)
{ 
    WriteToHostAndAppendToBuildLogFile 'Building installer failed. Quitting.'
    exit
}


# Move the installer to the repository folder and name with version number
$installerFileName = $buildsFolder + '\CytoApps' + $version + '.exe'

write-host "Moving output file to $installerFileName"

$MoveSetup=$PSScriptRoot + '\InnoSetup\Output\setup.exe'

move-item  $MoveSetup $installerFileName -force

if ($? -ne $true)
{
    WriteToHostAndAppendToBuildLogFile "Failed to move installer to $installerFileName. Quitting."
    exit
}


###############################################################################
# If the build has succeeded, store the incremented version.

$versionFileStreamWriter = OpenVersionFileForWrite($versionFileStreamReader)

if ($versionFileStreamWriter -eq $null)
{
    WriteToHostAndAppendToBuildLogFile "Unable to open $versionFilePath for write. Quitting."
    exit
}

# Write out the current version.
$versionFileStreamWriter.WriteLine($version)

# Ensure the write is flushed to disk.
$versionFileStreamWriter.Flush()        

###############################################################################
# Commit the changes.

GitStoreChanges $lastBuildHeadFile


###############################################################################
        
WriteToHostAndAppendToBuildLogFile 'Build Succeeded!'

