
function WriteToHostAndAppendToBuildLogFile
{
    $message = $args[0]

    write-host $message
    
    # Note that $buildLogFile must be defined in the caller's (or ancestor) scope.
    if (test-path variable:\buildLogFile)
    {
        $message | out-file $buildLogFile -append
    }
}

function CleanSolution
{
    $devEnvFullPath = $args[0]
    $solutionFileRelativePath = $args[1]
    $buildConfiguration = $args[2]
    
    # Must use the -passthru parameter with start-process for it to return a value.
    # Note that parameters must be separated by commas, but these will not be passed to the process.
    $process = start-process $devEnvFullPath $solutionFileRelativePath, '/clean', $buildConfiguration -wait -passthru
    
    return $process.ExitCode
}

function BuildSolution
{
    # $retVal = & "$devenv2010" @('IPMagic\TissueFind.sln', '/build', 'release|mixed platforms') | echo 'waiting'
    
    $devEnvFullPath = $args[0]
    $solutionFileRelativePath = $args[1]
    $buildConfiguration = $args[2]
    
    #write-host "Building $solutionFileRelativePath" -nonewline
    WriteToHostAndAppendToBuildLogFile "Building $solutionFileRelativePath"

    # Build the solution with /build, not /rebuild, as some solutions share projects but we only want to build them once.
    # Must use the -passthru parameter with start-process for it to return a value.
    # Note that parameters must be separated by commas, but these will not be passed to the process.
    $process = start-process $devEnvFullPath $solutionFileRelativePath, '/build', $buildConfiguration -wait -passthru
       # # #if ($process.ExitCode -ne 0){ write-host "`t - Failed!" }
    
    return $process.ExitCode
}

function BuildAllSolutions
{
    $solutionFileRelativePath = $args[0]
    $buildResult = 0	
	$SolutionPath=  $solutionFileRelativePath + "\Source\CytoApps.sln"
    # First clean the solutions. If Git has been already been used to clean the tree, this will have removed all
    # generated files more thoroughly and there won't be much left to do, but apparently cleaning the solution
    # with devenv also removes associated COM server data from the registry.
    # Note that all cleaning is done before any building is done, as many projects are built by more than one
    # solution and we want to avoid cleaning and building these multiple times.
    WriteToHostAndAppendToBuildLogFile "Cleaning solutions"	
    if ($buildResult -eq 0){ $buildResult = CleanSolution $devenv2015 $SolutionPath  '"release|Any CPU"' }
 
	WriteToHostAndAppendToBuildLogFile "Building solutions"
    # Now build the solutions
    if ($buildResult -eq 0){ $buildResult = BuildSolution $devenv2015 $SolutionPath '"release|Any CPU"' }
   

    return $buildResult
}

