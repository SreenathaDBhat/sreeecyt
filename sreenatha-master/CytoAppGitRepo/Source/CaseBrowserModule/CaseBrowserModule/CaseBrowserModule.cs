﻿using CaseBrowserModule.Presenters;
using CaseBrowserModule.Views;
using CytoApps.Infrastructure.UI;
using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;
using System.ComponentModel.Composition;

namespace CaseBrowserModule
{
    [ModuleExport(typeof(CaseBrowserModule))]
    public class CaseBrowserModule : IModule
    {
#pragma warning disable 0649, 0169
        [Import]
        private CaseBrowserPresenter _presenter;

        [Import]
        private IRegionManager _regionManager;
#pragma warning restore 0649, 0169

        public void Initialize()
        {
            _regionManager.RegisterViewWithRegion(RegionNames.MainRegion, typeof(CaseBrowserWorkspaceView));
        }
    }
}
