﻿using CytoApps.Infrastructure.UI.Models;
using CytoApps.Models;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace CaseBrowserModule.Converters
{

    /// <summary>
    /// Case Filter Template Selection based on type
    /// </summary>
    public sealed class CaseFilterTemplateSelector : DataTemplateSelector
    {
        public DataTemplate StringDataTemplate { get; set; }
        public DataTemplate DateDataTemplate { get; set; }
        public DataTemplate TextDataTemplate { get; set; }
        public DataTemplate NumberDataTemplate { get; set; }
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FilterInfo selectedCaseFilter = item as FilterInfo;
            if (selectedCaseFilter != null)

            {
                if (selectedCaseFilter.FieldType == FieldTypeEnum.Text)
                {
                    return StringDataTemplate;
                }
                else if (selectedCaseFilter.FieldType == FieldTypeEnum.Date)
                {
                    return DateDataTemplate;
                }
                else if (selectedCaseFilter.FieldType == FieldTypeEnum.MultiLineText)
                {
                    return TextDataTemplate;
                }
                else if (selectedCaseFilter.FieldType == FieldTypeEnum.Number)
                {
                    return NumberDataTemplate;
                }
                return null;
            }
            return null;
        }
    }

    /// <summary>
    /// Case Details Template Selection based on SelectedCaseDetail type
    /// </summary>
    public sealed class CaseDetailsTemplateSelector : DataTemplateSelector
    {
        private ItemsControl Control { get; set; }
        public DataTemplate CaseDetailsDataTemplate { get; set; }
        public DataTemplate TextEditableCaseDetailsDataTemplate { get; set; }
        public DataTemplate MultliLineTextEditableCaseDetailsDataTemplate { get; set; }
        public DataTemplate NumberEditableCaseDetailsDataTemplate { get; set; }
        public DataTemplate DateEditableCaseDetailsDataTemplate { get; set; }
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            CaseDetail selectedCaseDetail = item as CaseDetail;
            if (container != null)
            {
                Control = FindParent<ItemsControl>(container);

                if ((bool)Control.Tag)
                {
                    if (selectedCaseDetail != null)
                    {
                        if (selectedCaseDetail.Editable == true)
                        {
                            DataTemplate SelectedDataTemplate = null;
                            switch ((FieldTypeEnum)selectedCaseDetail.Type)
                            {
                                case FieldTypeEnum.Text:
                                    SelectedDataTemplate = TextEditableCaseDetailsDataTemplate;
                                    break;
                                case FieldTypeEnum.MultiLineText:
                                    SelectedDataTemplate = MultliLineTextEditableCaseDetailsDataTemplate;
                                    break;
                                case FieldTypeEnum.Number:
                                    SelectedDataTemplate = NumberEditableCaseDetailsDataTemplate;
                                    break;
                                case FieldTypeEnum.Date:
                                    SelectedDataTemplate = DateEditableCaseDetailsDataTemplate;
                                    break;
                            }
                            return SelectedDataTemplate;
                        }
                        else
                        {
                            return CaseDetailsDataTemplate;
                        }
                    }
                }
                else
                {
                    return CaseDetailsDataTemplate;
                }
            }
            return null;
        }


        public static T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            //get parent item
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
                return parent;
            else
                return FindParent<T>(parentObject);
        }
    }

    /// <summary>
    /// Context menu left click
    /// </summary>
    public static class LeftClickToOpenContextMenuBehavior
    {
        public static bool GetOpenContextMenuOnClickProperty(DependencyObject obj)
        {
            return (bool)obj.GetValue(OpenContextMenuOnClickProperty);
        }

        public static void SetOpenContextMenuOnClickProperty(DependencyObject obj, bool value)
        {
            obj.SetValue(OpenContextMenuOnClickProperty, value);
        }

        // Only Applicable to ComboBox Control 
        public static readonly DependencyProperty OpenContextMenuOnClickProperty =
            DependencyProperty.RegisterAttached("OpenContextMenuOnClick", typeof(bool), typeof(LeftClickToOpenContextMenuBehavior), new UIPropertyMetadata(false, OpenContextMenuOnButtonClick));

        private static void OpenContextMenuOnButtonClick(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as Button;
            if (control == null)
                return;

            if (e.NewValue is bool == false)
                return;

            if ((bool)e.NewValue)
                control.Click += OnButtonClick;
        }

        private static void OnButtonClick(object sender, RoutedEventArgs e)
        {
            // Only react to the ComboBox Onload Event
            // And Check wheather it hasItems then set selection to first item. 
            if (!Object.ReferenceEquals(sender, e.OriginalSource))
                return;
            Button button = e.OriginalSource as Button;
            if (button != null)
                button.ContextMenu.DataContext = button.DataContext;
            button.ContextMenu.IsOpen = true;
        }
    }

    ///<summary>
    ///window close behaviour
    /// </summary>
    /// 
    public sealed class WindowCloseBehaviour : Behavior<Window>
    {
        public static readonly DependencyProperty CommandProperty =
          DependencyProperty.Register(
          "Command",
          typeof(ICommand),
          typeof(WindowCloseBehaviour));

        public static readonly DependencyProperty CommandParameterProperty =
              DependencyProperty.Register(
              "CommandParameter",
              typeof(object),
              typeof(WindowCloseBehaviour));

        public static readonly DependencyProperty CloseButtonProperty =
          DependencyProperty.Register(
          "CloseButton",
          typeof(Button),
          typeof(WindowCloseBehaviour),
          new FrameworkPropertyMetadata(
                    null,
                    OnButtonChanged));

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        public Button CloseButton
        {
            get { return (Button)GetValue(CloseButtonProperty); }
            set { SetValue(CloseButtonProperty, value); }
        }

        private static void OnButtonChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var window = (Window)((WindowCloseBehaviour)d).AssociatedObject;
            ((Button)e.NewValue).Click += (s, e1) =>
            {
                var command = ((WindowCloseBehaviour)d).Command;
                var commandParameter = ((WindowCloseBehaviour)d).CommandParameter;
                if (command != null)
                {
                    command.Execute(commandParameter);
                }
                window.Close();
            };
        }
    }

    ///<summary>
    ///Items Control behavior
    /// </summary>
    public sealed class ItemsControlBehaviour
    {
        public static bool GetHasContentErrorsProperty(DependencyObject obj)
        {
            return (bool)obj.GetValue(HasContentErrorsProperty);
        }

        public static void SetHasContentErrorsProperty(DependencyObject obj, bool value)
        {
            obj.SetValue(HasContentErrorsProperty, value);
        }


        public static readonly DependencyProperty HasContentErrorsProperty =
            DependencyProperty.RegisterAttached("HasContentErrors", typeof(bool), typeof(ItemsControlBehaviour),
                new UIPropertyMetadata(false, OnCheckContentErrors));

        private static void OnCheckContentErrors(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as ItemsControl;
            if (control == null)
                return;
            if (control.HasItems)
            {
                control.DataContextChanged += Control_DataContextChanged;
            }

            control.Loaded += Control_Loaded;

        }

        private static void Control_Loaded(object sender, RoutedEventArgs e)
        {
            var control = sender as ItemsControl;
            if (control == null)
                return;
            if (control.HasItems)
            {
                control.DataContextChanged += Control_DataContextChanged;
            }
        }

        private static void Control_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }
    }
}
