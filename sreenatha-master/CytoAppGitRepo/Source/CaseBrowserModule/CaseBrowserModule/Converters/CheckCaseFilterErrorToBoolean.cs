﻿using CaseBrowserModule.Models;
using CytoApps.Infrastructure.UI.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace CaseBrowserModule.Converters
{
    public class CheckCaseFilterErrorToBoolean : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return true;
        }

       

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
      
    }

    public class SelectedCaseFindSourceConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ArrayList array = new ArrayList();
            object selectedCase = value[0];
            array.Add(selectedCase);
            ListBox parent = VisualTreeWalker.FindParentOfType<ListBox>(value[1] as Button);          
            string source = parent.Tag.ToString();
            array.Add(source);
            return array;
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
