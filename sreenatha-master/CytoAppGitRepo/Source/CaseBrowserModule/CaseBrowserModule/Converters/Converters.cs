﻿using CaseBrowserModule.Models;
using CytoApps.Infrastructure.UI.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace CaseBrowserModule.Converters
{

    public class SelectedCaseFindSourceConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            MultiCaseSelection multiCases = new MultiCaseSelection();
            ListBox parent = VisualTreeWalker.FindParentOfType<ListBox>(value[1] as Button);
            string source = parent.Tag.ToString();
            multiCases.Source = source;
            multiCases.SelectedCases.Add((CaseInfo)value[0]);
            foreach (CaseInfo caseInfo in parent.SelectedItems)
            {
                if(!multiCases.SelectedCases.Contains(caseInfo))
                {
                    multiCases.SelectedCases.Add(caseInfo);
                }
            }
            return multiCases;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


}
