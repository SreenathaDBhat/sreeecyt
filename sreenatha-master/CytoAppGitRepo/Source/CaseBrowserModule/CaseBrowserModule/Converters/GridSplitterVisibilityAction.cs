﻿using CytoApps.Infrastructure.UI.Utilities;
using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using System.Linq;

namespace CaseBrowserModule.Converters
{
    public class GridSplitterVisibilityAction : TargetedTriggerAction<UIElement>
    {
        public string RowName { get; set; }
        #region Overrides

        protected override void Invoke(object parameter)
        {
            var target =  VisualTreeWalker.FindParentOfType<Grid>(AssociatedObject);
            if (target != null)
            {
                if (!(AssociatedObject as Expander).IsExpanded)
                {
                    var row = target.RowDefinitions.SingleOrDefault(s => s.Name == RowName);
                    if (row != null)
                    {
                        row.Height = new GridLength(0, GridUnitType.Auto);
                    }

                    var splitter = VisualTreeWalker.FindChildOfType<GridSplitter>(target);
                    if (splitter != null)
                    {
                        splitter.Visibility = Visibility.Collapsed;
                    }
                }
                else
                {
                    var splitter = VisualTreeWalker.FindChildOfType<GridSplitter>(target);
                    if (splitter != null)
                    {
                        splitter.Visibility = Visibility.Visible;
                    }
                }
            }
        }

        #endregion
    }
}
