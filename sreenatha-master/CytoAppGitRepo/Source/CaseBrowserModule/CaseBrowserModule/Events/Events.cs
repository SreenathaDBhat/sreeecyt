﻿using CaseBrowserModule.Models;
using CaseBrowserModule.ViewModels;
using CytoApps.Models;
using Prism.Events;
using System.Collections.Generic;

namespace CaseBrowserModule.Events
{


    /// <summary>
    /// Advance search case event
    /// </summary>
    public class AdvanceSearchEvent : PubSubEvent<object>
    {
    }



    /// <summary>
    /// Selected case details event
    /// </summary>
    public class SelectedCaseDetailsEvent : PubSubEvent<CaseInfo>
    {
    }

    /// <summary>
    /// Open Color picker window event
    /// </summary>
    public class OpenColorPickerEvent : PubSubEvent<CaseStatus>
    {

    }


    /// <summary>
    /// Get the case status Flags
    /// </summary>
    public class GetUniqueFlagsEvent : PubSubEvent<List<CaseInfo>>

    {

    }

    public class CaseListChangedEventParameters
    {
        public CaseListChangedEventParameters(IEnumerable<Case> cases, DataSource dataSource)
        {
            CaseList = cases;
            DataSource = dataSource;
        }
        public IEnumerable<Case> CaseList;
        public DataSource DataSource;

    }

    public class CaseListChangedEvent : PubSubEvent<CaseListChangedEventParameters>
    {

    }

    public class UpdateCaseStatusEvent : PubSubEvent<CaseStatus>
    {

    }

    /// <summary>
    /// case selection changed event, payload contains values of old case and newly selected case
    /// </summary>
    public class CaseSelectionChangedEvent : PubSubEvent<System.Collections.ArrayList>
    {

    }
    /// <summary>
    /// Gets the list of users accessing the case
    /// </summary>
    public class CurrentCaseUsersEvent : PubSubEvent<CaseInfo>
    {

    }

}
