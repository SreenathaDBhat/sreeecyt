﻿using Prism.Mvvm;
using System;
using System.Collections.ObjectModel;

namespace CaseBrowserModule.Filters
{
    /// <summary>
    /// Case Filter for searching  cases
    /// </summary>
    public class CaseFilter : BindableBase
    {       

        #region Properties

        private ObservableCollection<CaseFilter> _additiobnalCaseFilters;
        public ObservableCollection<CaseFilter> AdditionalCaseFilters
        {
            get
            {
                if (_additiobnalCaseFilters == null)
                    _additiobnalCaseFilters = new ObservableCollection<CaseFilter>();
                return _additiobnalCaseFilters;
            }
            set
            {
                _additiobnalCaseFilters = value;
                this.OnPropertyChanged();
            }
        }

        private Type _caseFilterType;
        public Type CaseFilterType
        {
            get
            {
                return _caseFilterType;
            }
            set
            {
                _caseFilterType = value;
                this.OnPropertyChanged();
            }
        }

        private string _caseFilterName;
        public string CaseFilterName
        {
            get { return _caseFilterName; }
            set
            {
                _caseFilterName = value;
                OnPropertyChanged();
            }
        }

        private string _selectedValue;
        public string SelectedValue
        {
            get { return _selectedValue; }
            set
            {
                _selectedValue = value;
                this.OnPropertyChanged();
            }
        }
        
        #endregion      
    }
}
