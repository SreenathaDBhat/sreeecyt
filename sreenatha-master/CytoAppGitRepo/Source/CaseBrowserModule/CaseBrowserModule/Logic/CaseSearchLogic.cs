﻿using CaseBrowserModule.Models;
using CytoApps.Infrastructure.UI.Models;
using CytoApps.Infrastructure.Helpers;
using CytoApps.Localization;
using CytoApps.Models;
using System.Collections.Generic;
using CaseBrowserModule.Services;
using System.ComponentModel.Composition;
using CytoApps.Exceptions;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Linq;

namespace CaseBrowserModule.Logic
{
    /// <summary>
    /// This class is a business logic class for any search related logic
    /// Both basic and advance search logic is here
    /// This class makes call to sata service to get search results.
    /// </summary>
    [Export]
    public class CaseSearchLogic
    {
        [Import]
        public  IDataService _dataService
        {
            get;
            set;
        }
        public CaseSearchLogic()
        {

        }

        private IEnumerable<FilterInfo> _staticCaseFilters;
        public IEnumerable<FilterInfo> StaticCaseFilters
        {
            get
            {
                // default filters, not user defined as the casedetails fieldnames
                if (_staticCaseFilters == null)
                {
                    _staticCaseFilters = new FilterInfo[3] {
                                    new FilterInfo() { FieldName = Literal.Strings.Lookup("cbStr-CaseNameandNumber"), FieldType = FieldTypeEnum.Text,IsCaseTableField = true },
                                    new FilterInfo() { FieldName = Literal.Strings.Lookup("cbStr-LastAccessed"), FieldType = FieldTypeEnum.Date, IsCaseTableField = true },
                                    new FilterInfo() { FieldName = Literal.Strings.Lookup("cbStr-Keywords"), FieldType = FieldTypeEnum.MultiLineText, IsCaseTableField = true },
                                    
                    };
                }
                return _staticCaseFilters;
            }
        }


        ///<summary>
        ///Load the Operators for advance search window for numeric and date fields
        /// </summary>
        public List<FieldOperator> LoadOperators()
        {
            List<FieldOperator> Operators = new List<FieldOperator>();
            Operators.Add(new FieldOperator(Literal.Strings.Lookup("cbStr-Equal"), "=", "="));
            Operators.Add(new FieldOperator(Literal.Strings.Lookup("cbStr-NotEqual"), "!=", "≠"));
            Operators.Add(new FieldOperator(Literal.Strings.Lookup("cbStr-LessThan"), "<", "<"));
            Operators.Add(new FieldOperator(Literal.Strings.Lookup("cbStr-LessThanEqual"), "<=", "≤"));
            Operators.Add(new FieldOperator(Literal.Strings.Lookup("cbStr-GreaterThan"), ">", ">"));
            Operators.Add(new FieldOperator(Literal.Strings.Lookup("cbStr-GreaterThanEqual"), ">=", "≥"));
            return Operators;
        }
        /// <summary>
        /// This method resolves the data source and takes the criteria for the search and makes call to DAL methods 
        /// </summary>
        /// <param name="dataSource">data source from which the cases must be searched</param>
        /// <param name="criteria"> search criteria</param>
        /// <returns></returns>
        public IEnumerable<Case> GetCases(DataSource dataSource,Criteria criteria)
        {
            IEnumerable<Case> cases = null;
            try
            {
                var caseDAL = _dataService.CreateCaseDAL(dataSource);

                cases = caseDAL.GetCases(criteria);
            }
            catch (DALException exception)
            {
                throw exception;
            }

            return cases;
        }

        /// <summary>
        /// This method is to read the additional fields from the CaseDetails table of a particular data soruce
        /// </summary>
        /// <param name="dataSource"> data source to read from</param>
        /// <returns></returns>
        public List<Filter> GetAdditionalFilters(DataSource dataSource)
        {
            var additionalfields = new List<Filter>();
            try
            {
                var caseDAL = _dataService.CreateCaseDAL(dataSource);
                additionalfields.AddRange(caseDAL.GetCaseDetailsFields());
            }
            catch (DALException exception)
            {
                LogManager.Error("Get Flags and filters: ", exception);
                throw;
            }
            return additionalfields;
        }
        /// <summary>
        /// Method to get differnt status flags available from all datasoruces that are active.
        /// this also reads the flags that are selected earlier and saved in application settings
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="dataSourceExceptions"></param>
        /// <returns></returns>
        public List<Flag> GetFlags(SearchCriteriaInfo searchCriteria, out ConcurrentQueue<string> dataSourceExceptions)
        {
            var dataSourceExceptionsLocal = new ConcurrentQueue<string>();
            object sync = new object();
            List<Flag> uniqueFlags = new List<Flag>();
            var flags = new List<string>();
            var additionalFilters = new List<Filter>();
            Parallel.ForEach(DataSourceManager.Instance.DataSourceCollection, dataSource =>
            {
                if (dataSource.IsActive)
                {
                    try
                    {
                        var caseDAL = _dataService.CreateCaseDAL(dataSource);
                        var flaglist = caseDAL.GetUniqueCaseFlags();
                        lock (sync)
                        {
                            flags.AddRange(flaglist);
                        }

                    }
                    catch (DALException exception)
                    {
                        dataSourceExceptionsLocal.Enqueue(dataSource.Name);
                        LogManager.Error("Get Flags: ", exception);
                    }
                }
            });

            dataSourceExceptions = dataSourceExceptionsLocal;
            flags = flags.Distinct().ToList();
            var savedflagfilers = searchCriteria.SearchFilters.Where(x => x.FieldName == Literal.Strings.Lookup("cbStr-Flag"));
            List<string> savedFlags = savedflagfilers.Select(x => x.Value).ToList();
            uniqueFlags = flags.Select(x => new Flag() { FlagName = x, IsChecked = savedFlags.Contains(x) }).ToList();

            return uniqueFlags;
        }


        ///<summary>
        ///Get recent datasources for advance search and update the recent datassources selected for search
        /// </summary>
        public List<DataSource> GetSelectedDataSources(SearchCriteriaInfo searchCriteria)
        {
            List<DataSource> datasources = new List<DataSource>();
            foreach (DataSource dataSource in DataSourceManager.Instance.DataSourceCollection)
            {
                if (searchCriteria.DataSources.Contains(dataSource.ID.ToString()))
                    dataSource.IsSelected = true;
                datasources.Add(dataSource);

            }
            return datasources;
        }

    }
}
