﻿using CytoApps.Models;
using Prism.Mvvm;
using System;
using System.Windows;

namespace CaseBrowserModule.Models
{
    /// <summary>
    /// Wrapper class for Case 
    /// </summary>
    [Serializable]
    public class CaseInfo : BindableBase
    {

        #region Constructor
        public CaseInfo()
        {
        }

        public CaseInfo(Case @case, DataSource dataSource)
        {
            _case = @case;
            _caseName = _case.Name;
            _caseId = _case.Id;
            _flag = @case.Flag;
            _lastAccessed = @case.LastAccessed;
            IsAvailable = true;
            Lab = dataSource.Name;
            LabID = dataSource.ID;
        }

        public CaseInfo(Case @case)
        {
            _case = @case;
            _caseName = _case.Name;
            _caseId = _case.Id;
        }

        #endregion

        #region Properties

        [NonSerialized]
        private Case _case;
        [System.Xml.Serialization.XmlIgnore]
        public Case Case
        {
            get { return _case; }
            set
            {
                _case = value;
                OnPropertyChanged();
            }
        }

        private bool _isPinned;
        public bool IsPinned
        {
            get { return _isPinned; }
            set
            {
                _isPinned = value;
                OnPropertyChanged();
            }
        }

        private string _caseName;
        public string CaseName
        {
            get { return _caseName; }
            set
            {
                _caseName = value;
                OnPropertyChanged();
            }
        }

        private string _caseId;
        public string CaseId
        {
            get { return _caseId; }
            set
            {
                _caseId = value;
                OnPropertyChanged();
            }
        }

        private string _lab;
        public string Lab
        {
            get { return _lab; }
            set
            {
                _lab = value;
                OnPropertyChanged();
            }
        }

        private Guid _labID;
        public Guid LabID
        {
            get { return _labID; }
            set
            {
                _labID = value;
                OnPropertyChanged();
            }
        }

        private bool _isAvailable;
        public bool IsAvailable
        {
            get { return _isAvailable; }
            set
            {
                _isAvailable = value;
                OnPropertyChanged();
            }
        }

        private DateTime? _lastAccessed;
        public DateTime? LastAccessed
        {
            get { return _lastAccessed; }
            set
            {
                _lastAccessed = value;
                OnPropertyChanged();
            }
        }

        private string _flag;
        public string Flag
        {
            get { return _flag; }
            set
            {
                _flag = value;
                OnPropertyChanged();
            }
        }

        private LockResult _isCaseLockedByLegacy;
        public LockResult IsCaseLockedByLegacy
        {
            get { return _isCaseLockedByLegacy; }
            set
            {
                _isCaseLockedByLegacy = value;
                OnPropertyChanged();
            }
        }

        private string _color;
        public string Color
        {
            get
            {
                if (_color == string.Empty)
                    _color = Application.Current.Resources["TextBlockForegroundBrush"].ToString();
                return _color;
            }
            set
            {
                _color = value;
                OnPropertyChanged();
            }
        }

        private bool _isSelected;

        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
                OnPropertyChanged();
            }
        }

        #endregion
    }
}
