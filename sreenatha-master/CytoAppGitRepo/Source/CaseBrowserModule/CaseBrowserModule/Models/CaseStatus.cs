﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace CaseBrowserModule.Models
{
    /// <summary>
    /// Caset Status class used to change the case flag and show  color for the flag
    /// </summary>
    [Serializable]
    public class CaseStatus : BindableBase
    {

        private string _status;
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                OnPropertyChanged();
            }
        }

        private string _color;
        public string Color
        {
            get
            {
                if (_color == string.Empty)
                    _color = Application.Current.Resources["TextBlockForegroundBrush"].ToString();
                return _color;
            }
            set
            {
                _color = value;
                OnPropertyChanged();
            }
        }
    }
}
