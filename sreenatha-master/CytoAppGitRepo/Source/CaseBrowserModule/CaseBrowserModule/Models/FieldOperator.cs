﻿
namespace CaseBrowserModule.Models
{
    /// <summary>
    /// Case Filter operator for date comparision
    /// </summary>
    public class FieldOperator
    {
        #region Properties
        public string SqlOperator
        {
            get;
            private set;
        }
        public string DisplayOperator
        {
            get;
            private set;
        }
        public string Name
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public FieldOperator(string name, string sql, string display)
        {
            Name = name;
            SqlOperator = sql;
            DisplayOperator = display;
        }
        public override string ToString()
        {
            return Name;
        }
        #endregion
    }
}
