﻿using CytoApps.Models;
using System.Collections.Generic;
using System.ComponentModel;
using Prism.Mvvm;
using System;

namespace CaseBrowserModule.Models
{
    /// <summary>
    /// CaseFilterInfo class for search the cases from datasources
    /// </summary>
    [Serializable]
    public class FilterInfo : BindableBase
    {
        #region Constructor
        public object Clone()
        {
            return MemberwiseClone();
        }

        public FilterInfo()
        {
        }

        #endregion

        #region Properties

        private string _filedName;
        public string FieldName
        {
            get { return _filedName; }
            set
            {
                _filedName = value;
                OnPropertyChanged("FieldName");
            }
        }

        [NonSerialized]
        private string _value = string.Empty;
        [System.Xml.Serialization.XmlIgnore]
        public string Value
        {

            get { return _value; }
            set
            {
                _value = value;
                OnPropertyChanged("Value");
            }
        }
        private bool _isCaseTableField = false;
        public bool IsCaseTableField
        {
            get
            {
                return _isCaseTableField;
            }
            set
            {
                _isCaseTableField = value;
                OnPropertyChanged("IsCaseTableField");
            }
        }

        [NonSerialized]
        private FieldOperator _operator;
        [System.Xml.Serialization.XmlIgnore]
        public FieldOperator Operator
        {
            get { return _operator; }
            set
            {
                _operator = value;
                if(_operator!= null)
                   StoredOpeartor = _operator.Name;
                OnPropertyChanged("Operator");
            }
        }

        private FieldTypeEnum _fieldType;
        public FieldTypeEnum FieldType
        {
            get { return _fieldType; }
            set
            {
                _fieldType = value;
                this.OnPropertyChanged("FieldType");
            }
        }

        [NonSerialized]
        private bool _hasErrors;
        [System.Xml.Serialization.XmlIgnore]
        public bool HasErrors
        {
            get { return _hasErrors; }
            set
            {
                _hasErrors = value;
                OnPropertyChanged("HasErrors");
            }
        }
        private string _storedOperator = string.Empty;
        public string StoredOpeartor {
            get { return _storedOperator; }// Operator == null? string.Empty:Operator.Name; }
            set {

                _storedOperator = value;
                //if (Operator != null)
                //    _storedOperator = Operator.Name;
                //else
                //    _storedOperator = string.Empty;
               // if (Operator != null)// { Operator.Name = value; }
            }
        }

        #endregion

        //#region Event and Event Handlers
        //[NonSerialized]
        //public event PropertyChangedEventHandler PropertyChanged;
        //[System.Xml.Serialization.XmlIgnore]
        //protected void OnPropertyChanged(string propertyName)
        //{
        //    if (PropertyChanged != null)
        //    {
        //        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}

        //#endregion
    }
}
