﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaseBrowserModule.Models
{
    public class MultiCaseSelection
    {
        public MultiCaseSelection()
        {
            SelectedCases = new List<CaseInfo>();
        }
        public string Source
        {
            get;
            set;
        }
        public List<CaseInfo> SelectedCases
        {
            get;
            set;
        }
    }
}
