﻿using CytoApps.Infrastructure.UI.Models;
using CytoApps.Localization;
using System.Collections.Generic;
namespace CaseBrowserModule.Models
{
    /// <summary>
    /// This class is a wrapper class for Criteria class which is at data access layer.
    /// Class contains criteria elements which are needed for advanced search- Search filters,Data sources
    /// Also contains methods to store the search criteria to application settings and to read from settings.
    /// </summary>
    public class SearchCriteriaInfo
    {
        public SearchCriteriaInfo()
        {
           
        }
        #region Properties

        private List<FilterInfo> _searchFilters;
        public List<FilterInfo> SearchFilters
        {
            get
            {
                if (_searchFilters == null)
                    _searchFilters = new List<FilterInfo>();
                return _searchFilters;
            }
            set
            {
                _searchFilters = value;
            }
        }

        private List<string> _dataSources;
        public List<string> DataSources
        {
            get
            {
                if (_dataSources == null)
                    _dataSources = new List<string>();
                return _dataSources;
            }
            set
            {
                _dataSources = value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Load saved fitler details like filters ,flags and datasoruces from application settings and handle serialize logic
        /// </summary>
        public void LoadSearchCriteriaToSettings()
        {
        
                SearchFilters = Properties.CaseBrowser.Default.SearchFilters;
                DataSources = Properties.CaseBrowser.Default.RecentDataSources;
            
            foreach (FilterInfo c in SearchFilters)
            {
                if (c.StoredOpeartor != string.Empty)
                {
                    if (c.StoredOpeartor == Literal.Strings.Lookup("cbStr-Equal"))
                        c.Operator = new FieldOperator(c.StoredOpeartor, "=", "=");
                    else if (c.StoredOpeartor == Literal.Strings.Lookup("cbStr-NotEqual"))
                        c.Operator = new FieldOperator(c.StoredOpeartor, "!=", "≠");
                    else if (c.StoredOpeartor == Literal.Strings.Lookup("cbStr-LessThan"))
                        c.Operator = new FieldOperator(c.StoredOpeartor, "<", "<");
                    else if (c.StoredOpeartor == Literal.Strings.Lookup("cbStr-LessThanEqual"))
                        c.Operator = new FieldOperator(c.StoredOpeartor, "<=", "≤");
                    else if (c.StoredOpeartor == Literal.Strings.Lookup("cbStr-GreaterThan"))
                        c.Operator = new FieldOperator(c.StoredOpeartor, ">", ">");
                    else if (c.StoredOpeartor == Literal.Strings.Lookup("cbStr-GreaterThanEqual"))
                        c.Operator = new FieldOperator(c.StoredOpeartor, ">=", "≥");
                }
            }
           //  We need to persist value for only flag filter, for rest of search filter, there is no need to display stored value.
            foreach (FilterInfo c in SearchFilters)
            {
                if(c.FieldName != Literal.Strings.Lookup("cbStr-Flag"))
                {
                    c.Value = string.Empty;
                }
            }

        }
        /// <summary>
        /// Save search criteria details -filters and datasoruces to application settings
        /// </summary>
        public void SaveSearchCriteriaToSettings()
        {
            if (Properties.CaseBrowser.Default.SearchFilters != null)
            {
                Properties.CaseBrowser.Default.SearchFilters.Clear();
            }
            Properties.CaseBrowser.Default.SearchFilters = SearchFilters;

            if (Properties.CaseBrowser.Default.RecentDataSources != null)
            {
                Properties.CaseBrowser.Default.RecentDataSources.Clear();
            }
            Properties.CaseBrowser.Default.RecentDataSources = DataSources;
            Properties.CaseBrowser.Default.Save();
        }

        #endregion
    }
}
