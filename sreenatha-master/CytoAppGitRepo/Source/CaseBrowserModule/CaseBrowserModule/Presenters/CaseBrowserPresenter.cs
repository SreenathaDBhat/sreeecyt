﻿using CaseBrowserModule.Services;
using CaseBrowserModule.ViewModels;
using CaseBrowserModule.Views;
using CytoApps.Exceptions;
using CytoApps.Infrastructure.UI;
using CytoApps.Infrastructure.Helpers;
using CytoApps.Infrastructure.UI.Events;
using CytoApps.Models;
using Prism.Events;
using Prism.Regions;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using CaseBrowserModule.Events;
using System.Collections.Generic;
using CaseBrowserModule.Models;
using CytoApps.Infrastructure.UI.Controls;
using CytoApps.Localization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System;
using System.Windows.Threading;
using CaseBrowserModule.Logic;
using CytoApps.Infrastructure.UI.ApplicationCommands;
using System.Windows.Input;
using Prism.Commands;
using System.ComponentModel;
using System.Windows.Media;
using System.Collections;
using System.Globalization;

namespace CaseBrowserModule.Presenters
{
    /// <summary>
    /// This Presenter handles 
    /// Case items Display, Navigation, selection and Filtering
    /// It also publishes Case Selection event which will be subscribed to by all other Analysis plugins etc
    /// It also displays and maintains the pinned case list, which persists cases similar to 'Recent' list
    /// It provide advance search options get the cases
    /// </summary>
    [Export]
    class CaseBrowserPresenter
    {
        private IEventAggregator _eventAggregator;
        private IDataService _dataService;
        private IRegionManager _regionManager;
        private CaseSearchLogic _caseLogic;

        private CaseBrowserNavigationView _caseBrowserNavigationView;
        private CaseBrowserNavigationViewModel _caseBrowserNavigationViewModel;


        private BrandView _brandView;

        private CaseDetailsView _caseDetailsView;
        private CaseDetailsViewModel _caseDetailsViewModel;

        private PluginView _pluginView;
        private PluginViewModel _pluginViewModel;
        private List<CaseStatus> _casesStatusCollection;

        private Task _removeCrashLockTasks;

        [ImportingConstructor]
        public CaseBrowserPresenter(IEventAggregator eventAggregator, IRegionManager regionManager, IDataService dataService,CaseSearchLogic caseLogic)
        {
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _dataService = dataService;
            _caseLogic = caseLogic;

            SubscribeEvents();

            // Create Views and ViewModels maintained by this presenter
            _caseBrowserNavigationView = new CaseBrowserNavigationView();
            _caseBrowserNavigationViewModel = new CaseBrowserNavigationViewModel(_eventAggregator,_caseLogic);

            _caseDetailsView = new CaseDetailsView();
            _caseDetailsViewModel = new CaseDetailsViewModel(_eventAggregator);

            _caseBrowserNavigationView.DataContext = _caseBrowserNavigationViewModel;
            _caseDetailsView.DataContext = _caseDetailsViewModel;

            _brandView = new BrandView();

            _pluginView = new PluginView();
            _pluginViewModel = new PluginViewModel();
            _pluginView.DataContext = _pluginViewModel;

            if(Properties.CaseBrowser.Default.CasesStatusCollection ==null)
            {
                _casesStatusCollection = new List<CaseStatus>();
            }
            else
            {
                _casesStatusCollection = Properties.CaseBrowser.Default.CasesStatusCollection;
            }

            _caseBrowserNavigationViewModel.CaseStatsusCollection = _casesStatusCollection.ToObservableCollection();

            // Register views in appropriate places on the Application via the RegionManager
            regionManager.RegisterViewWithRegion(RegionNames.CaseBrowserRegion, () => _caseBrowserNavigationView);
            regionManager.RegisterViewWithRegion(RegionNames.CaseDetailsRegion, () => _caseDetailsView);
            regionManager.RegisterViewWithRegion(RegionNames.BrandRegion, () => _brandView);
            regionManager.RegisterViewWithRegion(RegionNames.SlidesRegion, () => _pluginView);

            //Search for pinned cases
            searchPinnedList();

            //removeCrashLockTasks = new Task(RemoveCrashLocks);
            RemoveCrashLocks();

            // Registering to global shutdown command, allowing the modules to set the Cancel property to true if the module wants to cancel the shutdown.
            HostCommands.ShutdownCommand.RegisterCommand(ShutdownCommand);
        }

        /// <summary>
        /// Removes enclusive lock when you restart the application
        /// Using last logged in user
        /// </summary>
        private void RemoveCrashLocks()
        {
            try
            {
                if (Properties.CaseBrowser.Default.LastAccessedCase != null && !Properties.CaseBrowser.Default.LastAccessedCase.IsCaseLockedByLegacy.IsLocked)
                {
                    var dataServer = DataSourceManager.Instance.DataSourceCollection.Where(dataSource => dataSource.ID == Properties.CaseBrowser.Default.LastAccessedCase.LabID).FirstOrDefault();
                    if (dataServer != null)
                    {
                        var caseDAL = _dataService.CreateCaseDAL(dataServer);
                        var caseObj = Properties.CaseBrowser.Default.LastAccessedCase;
                        var status = caseDAL.RemoveCrashLocksByUser(caseObj.CaseName, LoginHelper.GetValidUser());
                        LogManager.Info("Removed Crash Locks : " + status.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.Fatal(ex);
            }
        }

        public ICommand ShutdownCommand
        {
            get
            {
                return new DelegateCommand<ShutDownEventArgs>((args) =>
               {
                   args.CanClose = true;
                   if (_removeCrashLockTasks != null)
                       Task.WaitAll(_removeCrashLockTasks);
                   if (_caseBrowserNavigationViewModel.ActiveCase != null && !_caseBrowserNavigationViewModel.ActiveCase.IsCaseLockedByLegacy.IsLocked)
                   {
                       RemoveCaseLock(_caseBrowserNavigationViewModel.ActiveCase);
                       Properties.CaseBrowser.Default.LastAccessedCase = null;
                       Properties.CaseBrowser.Default.Save();
                   }
                  if(_caseDetailsViewModel.EditMode)
                    {
                        args.CancelEventArgs.Cancel = _caseDetailsViewModel.EditMode;                       
                    }
               });
            }
        }

        private void SubscribeEvents()
        {
            _eventAggregator.GetEvent<RefreshSelectedCaseEvent>().Subscribe(RefreshSelectedCase);
            _eventAggregator.GetEvent<AdvanceSearchEvent>().Subscribe(OnAdvanceSearch);
            _eventAggregator.GetEvent<SelectedCaseDetailsEvent>().Subscribe(SaveChangeCaseDetails);
            _eventAggregator.GetEvent<OpenColorPickerEvent>().Subscribe(OpenColorPicker);
            _eventAggregator.GetEvent<GetUniqueFlagsEvent>().Subscribe(GetUniqueFlags);
            _eventAggregator.GetEvent<UpdateCaseStatusEvent>().Subscribe(UpdateCaseStatus);
            _eventAggregator.GetEvent<CaseSelectionChangedEvent>().Subscribe(CaseSelectionChanged);
            _eventAggregator.GetEvent<CurrentCaseUsersEvent>().Subscribe(GetCurrentCaseUsers);
        }

        private void RefreshSelectedCase(object unused = null)
        {
            //Clear plugged regions from the saved plug-in collection before populating new plugins
            RegionManagerExtension.ClearPluggedRegions();
            //Clear slide views from the collection. This will remove views from the pluginview
            _pluginViewModel.SlideCollection.Clear();

            DataSource dataSource = DataSourceManager.Instance.DataSourceCollection.Where(caseDataAccess => caseDataAccess.Name == _caseDetailsViewModel.SelectedCaseInfo.Lab).FirstOrDefault();
            var caseDAL = _dataService.CreateCaseDAL(dataSource);
            List<CaseDataProxy> slideDataList = new List<CaseDataProxy>();
            var getSlides = new ObservableCollection<Slide>(caseDAL.GetSlides(_caseDetailsViewModel.SelectedCaseInfo.Case as Case));
            if (getSlides != null)
            {
                _caseDetailsViewModel.Slides = getSlides.ToObservableCollection(); ;
                LoadSlides(slideDataList, _caseDetailsViewModel.SelectedCaseInfo, dataSource);
            }
        }

        /// <summary>
        /// Displays list of users currently accessing the case 
        /// </summary>
        /// <param name="selectedCase"></param>
        private void GetCurrentCaseUsers(CaseInfo selectedCase)
        {
            if (selectedCase != null)
            {
                DataSource dataSource = DataSourceManager.Instance.DataSourceCollection.Where(caseDataAccess => caseDataAccess.Name == _caseDetailsViewModel.SelectedCaseInfo.Lab).FirstOrDefault();
                var caseDAL = _dataService.CreateCaseDAL(dataSource);
                var caseUsers = caseDAL.CurrentCaseUsers(selectedCase.Case);
                if (caseUsers != null)
                {
                    _caseDetailsViewModel.CaseUsers = caseUsers.ToObservableCollection();
                    _caseDetailsViewModel.IsUsersPopupOpen = true;
                }
            }
        }

        /// <summary>
        ///  Locks and unlocks the case based on case selection changed.
        /// </summary>
        /// <param name="cases"> array list conatins values od old,new case respectively</param>
        private void CaseSelectionChanged(ArrayList cases)
        {
            if (cases != null && cases.Count == 2)
            {
                if (cases[0] != null)
                {
                    var oldCase = cases[0] as CaseInfo;
                    if (oldCase.Case != null)
                    {
                        if (!oldCase.IsCaseLockedByLegacy.IsLocked)
                            RemoveCaseLock(cases[0] as CaseInfo);
                    }
                }
                if (cases[1] != null)
                {
                    if ((cases[1] as CaseInfo).Case != null)
                    {
                        OnChangeCase(cases[1] as CaseInfo);
                    }
                }
            }
        }

        /// <summary>
        /// Removes locks of the case
        /// </summary>
        /// <param name="caseInfo">case</param>
        private void RemoveCaseLock(CaseInfo caseInfo)
        {
            DataSource dataSource = DataSourceManager.Instance.DataSourceCollection.Where(caseDataAccess => caseDataAccess.Name == _caseDetailsViewModel.SelectedCaseInfo.Lab).FirstOrDefault();
            if (dataSource != null)
            {
                var caseDAL = _dataService.CreateCaseDAL(dataSource);
                caseDAL.RemoveCaseLocks(caseInfo.Case, LoginHelper.CurrentUser);
            }
        }

        /// <summary>
        /// update the case status
        /// </summary>
        /// <param name="caseStatus"></param>
        private void UpdateCaseStatus(CaseStatus caseStatus)
        {
            List<CaseInfo> allCases = (_caseBrowserNavigationViewModel.ActiveList == "Case") ? _caseBrowserNavigationViewModel.Cases.Where(x => x.IsSelected == true).ToList() : _caseBrowserNavigationViewModel.PinnedCases.Where(x => x.IsSelected == true).ToList();
            var dataSources = DataSourceManager.Instance.DataSourceCollection.Where(caseDataAccess => allCases.Any(caseInfo => caseInfo.Lab == caseDataAccess.Name)).ToList();
            Parallel.ForEach(dataSources, dataSource =>
            {
                if (dataSource.IsActive)
                {
                    try
                    {
                        List<CaseInfo> cases = allCases.Where(x => x.Lab == dataSource.Name).ToList();
                        var caseDAL = _dataService.CreateCaseDAL(dataSource);
                        bool IsUpdated = caseDAL.UpdateCaseFlag(cases.Select(x => x.CaseId).ToList(), caseStatus.Status);
                        if (IsUpdated)
                        {
                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                            {
                                foreach (CaseInfo caseInfo in cases)
                                {
                                    caseInfo.Flag = caseStatus.Status;
                                    caseInfo.Case.Flag = caseStatus.Status;
                                    caseInfo.Color = caseStatus.Color.ToString();
                                }

                            }));
                        }
                    }
                    catch (DALException exception)
                    {

                        LogManager.Error("Update flags Error: ", exception);
                    }
                }
            });
        }

        /// <summary>
        /// Get the unique flags
        /// </summary>
        /// <param name="unUsed"></param>
        private void GetUniqueFlags(List<CaseInfo> cases)
        {
            _caseBrowserNavigationViewModel.CaseStatsusCollection.Clear();
            var dataSources = DataSourceManager.Instance.DataSourceCollection.Where(caseDataAccess => cases.Any(caseInfo => caseInfo.Lab == caseDataAccess.Name)).ToList();
            List<string> caseFlags = new List<string>();
            Parallel.ForEach(dataSources, dataSource =>
            {
                if (dataSource.IsActive)
                {
                    try
                    {
                        var caseDAL = _dataService.CreateCaseDAL(dataSource);
                        var flags = caseDAL.GetAllCaseFlags();
                        if (flags != null)
                        {
                            foreach (string flag in flags)
                            {
                                caseFlags.Add(flag);

                                //caseFlags.Add(caseStatus);
                            }
                        }
                    }
                    catch (DALException exception)
                    {
                        LogManager.Error("Get flags Error: ", exception);
                    }
                }
            });

            List<string> flagList = new List<string>();
            if (dataSources.Count > 1)
            {

                flagList = caseFlags.GroupBy(x => x)
                             .Where(g => g.Count() > 1)
                             .Select(g => g.Key)
                             .ToList();
            }
            else
            {
                flagList = caseFlags;
            }
            foreach (string flag in flagList)
            {
                CaseStatus caseStatus = _casesStatusCollection.Where(x => x.Status == flag).FirstOrDefault();
                if (caseStatus == null)
                {
                    caseStatus = new CaseStatus()
                    {
                        Status = flag,
                        Color = Application.Current.Resources["TextBlockForegroundBrush"].ToString()
                    };
                }
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() => _caseBrowserNavigationViewModel.CaseStatsusCollection.Add(caseStatus)));
            }
        }

        /// <summary>
        /// This fucntion is used to set the color for the  Case status and open the color picker dialog for selection
        /// </summary>
        /// <param name="unUsed"></param>
        private void OpenColorPicker(CaseStatus caseStatus)
        {
            Color color = (Color)ColorConverter.ConvertFromString(caseStatus.Color);
            caseStatus.Color = ChooseColour(color).ToString();
            _caseBrowserNavigationViewModel.Cases.Where(x => x.Flag == caseStatus.Status).ToList().ForEach(x => { x.Color = caseStatus.Color.ToString(); });
            _caseBrowserNavigationViewModel.PinnedCases.Where(x => x.Flag == caseStatus.Status).ToList().ForEach(x => { x.Color = caseStatus.Color.ToString(); });
            if (color.ToString() != caseStatus.Color)
            {
                _casesStatusCollection = _caseBrowserNavigationViewModel.CaseStatsusCollection.Where(x => x.Color.ToString() != Application.Current.Resources["TextBlockForegroundBrush"].ToString()).ToList();
                SaveCaseStatus();
            }
        }

            private  void SaveCaseStatus()
        {
            Properties.CaseBrowser.Default.CasesStatusCollection = _casesStatusCollection.ToList();
            Properties.CaseBrowser.Default.Save();
        }
    

        /// <summary>
        /// Get the color using Color dialog.
        /// </summary>
        /// <param name="colourIn"></param>
        /// <returns></returns>
        private Color ChooseColour(Color colourIn)
        {
            Color colourOut = colourIn;
            try
            {
                System.Windows.Forms.ColorDialog cd = new System.Windows.Forms.ColorDialog();

                // Have to convert from System.Windows.Media.Color to System.Drawing.Color!!
                cd.Color = System.Drawing.Color.FromArgb(colourIn.R, colourIn.G, colourIn.B);


                if (cd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    colourOut = Color.FromArgb(255, cd.Color.R, cd.Color.G, cd.Color.B);

                    // Don't allow colour to be set to pure black, as for spectrachrome overlays this is treated as
                    // the transparent colour by ProbeCaseView (see ScoredCell.LoadOverlayImagesForReals() in ScoredCell.cs),
                    // so pure black overlays would be invisible.
                    if (colourOut == Color.FromArgb(255, 0, 0, 0))
                        colourOut = Color.FromArgb(255, 1, 1, 1);
                }
            }
            catch (Win32Exception ex)
            {
                throw ex;
            }

            return colourOut;
        }

        private void RefreshPinnedCases(object obj)
        {
            searchPinnedList();
        }

        /// <summary>
        /// Save the case details to the datasource
        /// </summary>
        /// <param name="selectedCase"></param>
        private void SaveChangeCaseDetails(CaseInfo selectedCase)
        {
            CaseInfo selectedCaseInfo = _caseDetailsViewModel.SelectedCaseInfo = selectedCase;
            DataSource dataSource = DataSourceManager.Instance.DataSourceCollection.Where(caseDataAccess => caseDataAccess.Name == selectedCaseInfo.Lab).FirstOrDefault();
            try
            {
                var caseDAL = _dataService.CreateCaseDAL(dataSource);
                bool success = caseDAL.SaveCaseDetails(selectedCaseInfo.Case.Id, _caseDetailsViewModel.CaseDetails);
                if (success)
                {
                    LogManager.Info(_caseDetailsViewModel.SelectedCaseInfo.CaseId.ToString() + Literal.Strings.Lookup("cbStr-CaseDetailedSavedSucessfully"));
                }
                else
                {
                    MessageBoxResult result1 = CustomPopUp.Show(_caseDetailsViewModel.SelectedCaseInfo.CaseId + Literal.Strings.Lookup("cbStr-CaseDetailNotSaved"), Literal.Strings.Lookup("cbStr-CaseBrowserTitle"), MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (DALException exception)
            {
                MessageBoxResult result = CustomPopUp.Show(string.Join(Environment.NewLine, exception.Message) + Environment.NewLine + Environment.NewLine + Literal.Strings.Lookup("cbStr-UnableToConnectDataSource"), Literal.Strings.Lookup("cbStr-SearchCases"), MessageBoxButton.OK, MessageBoxImage.Warning);
                CloneCaseDetails(_caseDetailsViewModel.ClonedCaseDetails, _caseDetailsViewModel.CaseDetails);
                LogManager.Error("Save Case Details: ", exception);
            }
            finally
            {
                //Sreenatha:For date field CaseDetails.Data has UTC Format and need to convert back to local time and even failure of saevchanges also
                ConvertUTCtoLocalDateTime(_caseDetailsViewModel.CaseDetails.Where(x => x.Type == 2).ToList());
                CloneCaseDetails(_caseDetailsViewModel.CaseDetails, _caseDetailsViewModel.ClonedCaseDetails);
            }
        }

        /// <summary>
        /// Keep copy of casedetails for any exception occurs while saving and canceled the changes made to the casedetials 
        /// </summary>
        /// <param name="fromCaseDetails"></param>
        /// <param name="toCaseDetails"></param>
        private void CloneCaseDetails(ObservableCollection<CaseDetail> fromCaseDetails, ObservableCollection<CaseDetail> toCaseDetails)
        {
            toCaseDetails.Clear();
            foreach (CaseDetail caseDetail in fromCaseDetails)
            {
                toCaseDetails.Add((CaseDetail)caseDetail.Clone());
            }
        }

        /// <summary>
        /// Convert UTC Time to local datetime format
        /// </summary>
        /// <param name="caseDetails"></param>
        private void ConvertUTCtoLocalDateTime(IEnumerable<CaseDetail> caseDetails)
        {
            //string dateFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
            if (caseDetails != null)
            {
                foreach (CaseDetail caseDetail in caseDetails)
                {
                    DateTime userdate;
                    if (DateTime.TryParse(caseDetail.Data, out userdate))
                    {
                        var utc = DateTime.SpecifyKind(userdate, DateTimeKind.Local);
                        caseDetail.Data = utc.ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern);
                    }
                }
            }
        }

        /// <summary>
        /// Method will select the case and cause the slides navigation area to be populated with the plugin analysis summaries
        /// Also prompt user to save any edit case details before we move off current case
        /// </summary>
        /// <param name="selectedCase"></param>
        private void OnChangeCase(CaseInfo selectedCase)
        {
            if (selectedCase != null)
            {
                _caseDetailsViewModel.IsVisible = true;
                _caseDetailsViewModel.IsUsersVisible = true;
                //Clear plugged regions from the saved plug-in collection before populating new plugins
                RegionManagerExtension.ClearPluggedRegions();

                PromptUserToSaveCaseDetails();

                //Clear slide views from the collection. This will remove views from the pluginview
                _pluginViewModel.SlideCollection.Clear();
                NavigateToSelectedCase(selectedCase);

            }
        }

        private void PromptUserToSaveCaseDetails()
        {
            if (_caseDetailsViewModel.EditMode)
            {
                MessageBoxResult result = CustomPopUp.Show(Literal.Strings.Lookup("cbStr-SaveChanges"), Literal.Strings.Lookup("cbStr-ChangeCaseTitle"), MessageBoxButton.YesNo, MessageBoxImage.Question, true);
                if (result == MessageBoxResult.Yes)
                {
                    SaveChangeCaseDetails(_caseDetailsViewModel.SelectedCaseInfo);
                }
                _caseDetailsViewModel.EditMode = false;
                _caseBrowserNavigationViewModel.OpenCaseStatusPopupCommand.CanExecute(false);
                _caseBrowserNavigationViewModel.canOpen = false;
            }
        }

        private void NavigateToSelectedCase(CaseInfo selectedCase)
        {
            List<CaseDataProxy> slideDataList = new List<CaseDataProxy>();
            _caseDetailsViewModel.SelectedCaseInfo = selectedCase;
            DataSource dataSource = DataSourceManager.Instance.DataSourceCollection.Where(caseDataAccess => caseDataAccess.Name == _caseDetailsViewModel.SelectedCaseInfo.Lab).FirstOrDefault();
            try
            {
                var caseDAL = _dataService.CreateCaseDAL(dataSource);
                selectedCase.IsCaseLockedByLegacy = caseDAL.IsCaseLockedByLegacy(_caseDetailsViewModel.SelectedCaseInfo.Case as Case, LoginHelper.CurrentUser);
                SetIsCaseEnabled(selectedCase);
                var caseDetails = caseDAL.GetCaseDetails(_caseDetailsViewModel.SelectedCaseInfo.Case as Case);
                ConvertUTCtoLocalDateTime(caseDetails.Where(x => x.Type == 2));
                _caseDetailsViewModel.CaseDetails = new ObservableCollection<CaseDetail>(caseDetails);
                _caseDetailsViewModel.ClonedCaseDetails = new ObservableCollection<CaseDetail>();
                foreach (CaseDetail caseDetail in caseDetails)
                {
                    _caseDetailsViewModel.ClonedCaseDetails.Add((CaseDetail)caseDetail.Clone());
                }
                var getSlides = new ObservableCollection<Slide>(caseDAL.GetSlides(_caseDetailsViewModel.SelectedCaseInfo.Case as Case));
                if (getSlides != null)
                {
                    _caseDetailsViewModel.Slides = getSlides.ToObservableCollection(); ;
                    LoadSlides(slideDataList, _caseDetailsViewModel.SelectedCaseInfo, dataSource);
                }
            }
            catch (DALException exception)
            {
                LogManager.Error("Navigating to selected case: ", exception);
                MessageBoxResult result = CustomPopUp.Show(string.Join(Environment.NewLine, exception.Message) + Environment.NewLine + Environment.NewLine + Literal.Strings.Lookup("cbStr-UnableToConnectDataSource"), Literal.Strings.Lookup("cbStr-CaseNavigation"), MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            catch (Exception exception)
            {
                LogManager.Error("Null referenceException ", exception);
                MessageBoxResult result = CustomPopUp.Show(string.Join(Environment.NewLine, exception.Message) + Environment.NewLine + Environment.NewLine + Literal.Strings.Lookup("cbStr-UnableToConnectDataSource"), Literal.Strings.Lookup("cbStr-CaseNavigation"), MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SetIsCaseEnabled(CaseInfo selectedCase)
        {
            if (_caseBrowserNavigationViewModel.ActiveCase != null && _caseBrowserNavigationViewModel.ActiveCase.Case != null)
            {
                if (_caseBrowserNavigationViewModel.ActiveCase.CaseId == selectedCase.CaseId)
                {
                    _caseBrowserNavigationViewModel.ActiveCase.IsCaseLockedByLegacy = selectedCase.IsCaseLockedByLegacy;
                }
            }
        }

        /// <summary>
        /// Load Slides Respective Region
        /// </summary>
        /// <param name="slideDataList"></param>
        /// <param name="selectedCaseInfo"></param>
        /// <param name="datasource"></param>
        internal void LoadSlides(List<CaseDataProxy> slideDataList, CaseInfo selectedCaseInfo, DataSource datasource)
        {
            if (_caseDetailsViewModel.Slides == null || _caseDetailsViewModel.Slides.Count == 0)
            {
                RegionManagerExtension.ClearPluggedRegions();
            }
            else
            {
                foreach (var slide in _caseDetailsViewModel.Slides)
                {
                    // Encapsulate the case data with its originating DataSource and pass it to interested parties. The modules that are interested can then
                    // use the dataSource and the slide id's to query the contents of the slides etc
                    CaseDataProxy caseDataProxy = new CaseDataProxy(datasource, selectedCaseInfo.Case, slide, _caseDetailsViewModel.Slides);
                    SlideViewModel slideVM = new SlideViewModel(caseDataProxy);
                    SlideView slideView = new SlideView();
                    slideView.DataContext = slideVM;
                    //assign a region name based on slide name, slide.name concat with PluginRegion 
                    string pluginRegionName = caseDataProxy.Slide.Name + Constants.PluginRegion;
                    caseDataProxy.PluginRegionName = pluginRegionName;

                    //define a control  that can hold multiple views from the published modules
                    System.Windows.Controls.ItemsControl RegionContentControl = new System.Windows.Controls.ItemsControl { Focusable = false };

                    // This registers the region name with Region Manager 
                    RegionManager.SetRegionName(RegionContentControl, pluginRegionName);

                    // This adds the region(control) to region manager.
                    RegionManager.SetRegionManager(RegionContentControl, _regionManager);

                    //Maintain a collection of plugged regions for clearing regions from Region Manager when it is not in use
                    RegionManagerExtension.PluggedRegions(pluginRegionName);

                    //Display dynamically created region in the Slide View
                    slideView.pluginPresenter.Content = RegionContentControl;
                    slideView.IsEnabled = !selectedCaseInfo.IsCaseLockedByLegacy.IsLocked;

                    _pluginViewModel.SlideCollection.Add(slideView);
                    slideDataList.Add(caseDataProxy);
                }
                Dispatcher.CurrentDispatcher.BeginInvoke((Action)delegate
                {
                    foreach (var slideProxy in slideDataList)
                    {
                        //send an event to all interested parties to get plug-in views
                        _eventAggregator.GetEvent<SelectedCaseSlidesEvent>().Publish(slideProxy);
                    }
                }, DispatcherPriority.Background);
            }
        }
        /// <summary>
        /// Shows the advancedSearch Dialog
        /// </summary>
        /// <param name="isOpenAdvanceSearchWindow"></param>
        private void OnAdvanceSearch(object unUsed)
        {
            var _advanceSearchView = new AdvanceSearchView();

            var advancedSearchVM =  new AdvanceSearchViewModel(_eventAggregator, _caseLogic);
            _advanceSearchView.DataContext = advancedSearchVM;

            //HACK SB:set the datacontext for Context Menu. WPF shortcoming
            _advanceSearchView.con.DataContext = advancedSearchVM;
            _advanceSearchView.ShowDialog();
        }
        /// <summary>
        ///  Check if case is already in the pinned list if so return same reference
        /// </summary>
        /// <param name="case"></param>
        /// <param name="dataSource"></param>
        /// <returns></returns>
        private CaseInfo GetPinnedCase(Case @case, DataSource dataSource)
        {
            CaseInfo caseinfo = null;
            if (_caseBrowserNavigationViewModel.PinnedCases.Where(c => c.LabID == dataSource.ID).Count() > 0)
            {
                caseinfo = _caseBrowserNavigationViewModel.PinnedCases.Where(caseInfo => caseInfo.CaseId == @case.Id && caseInfo.CaseName == @case.Name && caseInfo.LabID == dataSource.ID).FirstOrDefault();
            }
            return caseinfo;
        }
        /// <summary>
        /// Read pinned cases from casebrowser application settings.
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<CaseInfo> ReadPinnedCasesFromSettings()
        {
            return Properties.CaseBrowser.Default.PinnedCases.ToObservableCollection();
        }

        /// <summary>
        /// Get the recent pinned cases on start of the application
        /// </summary>
        private void searchPinnedList()
        {

            _caseBrowserNavigationViewModel.PinnedCases = ReadPinnedCasesFromSettings();
            if (_caseBrowserNavigationViewModel.PinnedCases.Count > 0)
            {
                Parallel.ForEach(DataSourceManager.Instance.DataSourceCollection, dataSource =>
                {
                    var pinnedList = _caseBrowserNavigationViewModel.PinnedCases.Where(caseInfo => caseInfo.LabID == dataSource.ID).ToList();
                    if (pinnedList.Count() > 0)
                    {
                        try
                        {
                            foreach (CaseInfo caseInfo in pinnedList)
                            {
                                caseInfo.Case = new Case();
                                caseInfo.Case.Name = caseInfo.CaseName;
                                caseInfo.Case.Id = caseInfo.CaseId;
                                caseInfo.Case.Flag = caseInfo.Flag;
                                caseInfo.Case.LastAccessed = caseInfo.LastAccessed;
                                CaseStatus caseStatus = _casesStatusCollection.Where(x => x.Status == caseInfo.Flag).FirstOrDefault();
                                if (caseStatus != null)
                                {
                                    caseInfo.Color = caseStatus.Color.ToString();
                                }
                            }
                            var caseDAL = _dataService.CreateCaseDAL(dataSource);
                            var cases = caseDAL.GetCases(null);
                            if (cases != null)
                            {
                                foreach (CaseInfo caseInfo in pinnedList)
                                {
                                    var casedetail = cases.Where(probeCase => probeCase.Name == caseInfo.CaseName && probeCase.Id == caseInfo.CaseId).FirstOrDefault();
                                    caseInfo.Case = casedetail;
                                    caseInfo.IsAvailable = true;
                                }
                            }
                            else
                            {
                                pinnedList.ForEach(x => x.IsAvailable = false);
                            }
                        }
                        catch (DALException exception)
                        {
                            pinnedList.ForEach(x => x.IsAvailable = false);
                            LogManager.Error("Searched Pinned list error: ", exception);
                        }
                    }
                });
            }
        }
    }
}
