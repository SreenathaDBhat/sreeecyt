﻿using System.ComponentModel.Composition;
using CytoApps.Models;
using CaseDataAccess;
using CytoApps.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using CytoApps.Infrastructure.Helpers;
using System.Linq;
using System.IO;
using System.Reflection;

/// <summary>
/// DataService is responsible for loading all the implementations of ICaseDataAccess and choosing the correct
/// inplemetation based on the DalTypeId in the datasource and creating an instance that is initiatized
/// with the correct connection parameters held in the DataSource
/// </summary>
namespace CaseBrowserModule.Services
{
    [Export(typeof(IDataService))]
    class DataService : IDataService
    {
        // all DAL assemblies found that derive from ICaseDataAccess
        private Type[] availCaseDalTypes = null;

        /// <summary>
        /// Create the appropriate instance from the set of available DAL's that implement ICaseDataAccess that match the DataSource
        /// </summary>
        /// <param name="dataSource"></param>
        /// <returns></returns>
        public ICaseDataAccess CreateCaseDAL(DataSource dataSource)
        {
            // import caseDALS using reflection not MEF (ExportFactory) due to lifetime concerns
            if (availCaseDalTypes == null)
                availCaseDalTypes = DalReflectionFinder.GetTypesForInterface<ICaseDataAccess>(@".");

            // Create a new instance every time, thread safety concerns
            var caseDAL = DalReflectionFinder.GetMatchingInstanceOf<ICaseDataAccess>(availCaseDalTypes, dataSource);

            // This will pass the connection parameters required (persisted by application per data source pointed at)
            caseDAL.Initialize(dataSource);

            return caseDAL;
        }
    }
}
