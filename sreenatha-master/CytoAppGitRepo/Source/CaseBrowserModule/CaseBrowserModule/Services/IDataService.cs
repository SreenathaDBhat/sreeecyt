﻿using CaseDataAccess;
using CytoApps.Models;
using System.Collections.Generic;

namespace CaseBrowserModule.Services
{
    public interface IDataService
    {
        ICaseDataAccess CreateCaseDAL(DataSource dataSource);
    }
}
