﻿using CytoApps.Infrastructure.UI.Models;
using CytoApps.Localization;
using CytoApps.Models;
using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace CaseBrowserModule.Validator
{
    /// <summary>
    /// Date Validation
    /// </summary>
    public class DateInputValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            DateTime date= DateTime.Today;
            //string[] dateFormat = cultureInfo.DateTimeFormat.GetAllDateTimePatterns();
            string dateFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern; 
            if (!string.IsNullOrWhiteSpace(value.ToString()))
            {
                if (!DateTime.TryParseExact(value.ToString(),dateFormat, cultureInfo, DateTimeStyles.None, out date))
                {
                    return new ValidationResult(false, BindingPropertyName + Literal.Strings.Lookup("cbStr-InvalidDateFormat") + Literal.Strings.Lookup("cbStr-DateFormat") + CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern);
                }
            }
            return ValidationResult.ValidResult;
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo, BindingExpressionBase owner)
        {
            ValidationResult validationResult;
            validationResult = base.Validate(value, cultureInfo, owner);

            if (((BindingExpression)owner).DataItem is FilterInfo)
            {
                ((FilterInfo)((BindingExpression)owner).DataItem).HasErrors = !(validationResult.IsValid);
                BindingPropertyName = ((FilterInfo)((BindingExpression)owner).DataItem).FieldName;
            }

            else if (((BindingExpression)owner).DataItem is CaseDetail)
            {
                BindingPropertyName = ((CaseDetail)((BindingExpression)owner).DataItem).FieldName;
                CaseDetail caseDetails = ((BindingExpression)owner).DataItem as CaseDetail;
                if (caseDetails.Mandatory)
                {
                    if (string.IsNullOrEmpty(value.ToString()))
                        validationResult = new ValidationResult(false, BindingPropertyName + Literal.Strings.Lookup("cbStr-EmptyString"));
                }
            }
            return validationResult;
        }
        public string BindingPropertyName { get; set; }
    }
}
