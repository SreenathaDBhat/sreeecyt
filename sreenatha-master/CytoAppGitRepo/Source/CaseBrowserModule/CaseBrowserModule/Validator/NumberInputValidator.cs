﻿using CytoApps.Infrastructure.UI.Models;
using CytoApps.Localization;
using CytoApps.Models;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Data;

namespace CaseBrowserModule.Validator
{
    /// <summary>
    /// Number Validation
    /// </summary>
    public class NumberInputValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (!string.IsNullOrWhiteSpace(value.ToString()))
            {
                if (!(Regex.IsMatch(value.ToString(), @"^\d+$")))
                {
                    return new ValidationResult(false, BindingPropertyName + Literal.Strings.Lookup("cbStr-NumberString"));
                }
            }
            return ValidationResult.ValidResult;
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo, BindingExpressionBase owner)
        {
            ValidationResult validationResult;
            validationResult = base.Validate(value, cultureInfo, owner);

            if (((BindingExpression)owner).DataItem is FilterInfo)
            {
                BindingPropertyName = ((FilterInfo)((BindingExpression)owner).DataItem).FieldName;
                ((FilterInfo)((BindingExpression)owner).DataItem).HasErrors = !(validationResult.IsValid);

            }
            else if (((BindingExpression)owner).DataItem is CaseDetail)
            {
                BindingPropertyName = ((CaseDetail)((BindingExpression)owner).DataItem).FieldName;
                CaseDetail caseDetails = ((BindingExpression)owner).DataItem as CaseDetail;
                if (caseDetails.Mandatory)
                {
                    if (string.IsNullOrEmpty(value.ToString()))
                        validationResult = new ValidationResult(false, BindingPropertyName + Literal.Strings.Lookup("cbStr-EmptyString"));
                }
            }
            return validationResult;
        }

        public string BindingPropertyName { get; set; }
    }
}
