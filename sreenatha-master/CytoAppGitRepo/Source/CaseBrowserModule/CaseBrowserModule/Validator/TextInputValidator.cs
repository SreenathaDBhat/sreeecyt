﻿using CytoApps.Localization;
using CytoApps.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace CaseBrowserModule.Validator
{
   public class TextInputValidator : ValidationRule
    {

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (string.IsNullOrEmpty(value.ToString()))
                return new ValidationResult(false, BindingPropertyName + Literal.Strings.Lookup("cbStr-EmptyString"));
            return ValidationResult.ValidResult;
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo, BindingExpressionBase owner)
        {
            if (((BindingExpression)owner).DataItem is CaseDetail)
            {
                BindingPropertyName = ((CaseDetail)((BindingExpression)owner).DataItem).FieldName;
                CaseDetail caseDetails = ((BindingExpression)owner).DataItem as CaseDetail;
                if (caseDetails.Mandatory)
                    return base.Validate(value, cultureInfo, owner);
            }
            return ValidationResult.ValidResult;
        }

        public string BindingPropertyName { get; set; }
    }
}
