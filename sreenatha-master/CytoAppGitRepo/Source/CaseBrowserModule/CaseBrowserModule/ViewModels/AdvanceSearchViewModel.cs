﻿using CaseBrowserModule.Events;
using CytoApps.Localization;
using CytoApps.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows.Input;
using CytoApps.Infrastructure.UI.Models;
using CaseBrowserModule.Logic;
using CytoApps.Infrastructure.Helpers;
using CytoApps.Infrastructure.UI.Controls;
using System.Windows;
using System.Threading.Tasks;
using CaseBrowserModule.Models;
using System;
using System.Collections.Concurrent;
using CytoApps.Exceptions;

namespace CaseBrowserModule.ViewModels
{
    /// <summary>
    /// Advance search view model class contains commands related to advance search 
    /// it uses CaseSearchLogic class to perform both basic and advance search
    /// </summary>
    public class AdvanceSearchViewModel : BindableBase
    {
        #region private members

        private IEventAggregator _eventAggregator;
        private CaseSearchLogic _caseSearchLogic;

        #endregion

        #region Constructor

        public AdvanceSearchViewModel(IEventAggregator eventAggregator,CaseSearchLogic caseSearchLogic)
        {
            _eventAggregator = eventAggregator;
            _caseSearchLogic = caseSearchLogic;
            CaseFilterOperators = _caseSearchLogic.LoadOperators();
            SearchCriteria = new SearchCriteriaInfo();
            GetSavedSearchCriteria();            
            SetFlagsAndFilters();
        }

        private void GetSavedSearchCriteria()
        {
            SearchCriteria.LoadSearchCriteriaToSettings();
            SearchCaseFilters = SearchCriteria.SearchFilters.ToObservableCollection();
            DataSources = _caseSearchLogic.GetSelectedDataSources(SearchCriteria).Where(datasource => datasource.IsActive == true).ToList();
        }
        #endregion

        #region Properties

        public ObservableCollection<FilterInfo> AllCaseFilters
        {
            get;
            set;
        }

        public List<FieldOperator> CaseFilterOperators
        {
            get;
            set;
        }

        private SearchCriteriaInfo _searchCriteria;

        public SearchCriteriaInfo SearchCriteria
        {
            get {
                
                return _searchCriteria;
            }
            set { _searchCriteria = value; }

        }

        private ObservableCollection<FilterInfo> _searchCaseFilters;
        public ObservableCollection<FilterInfo> SearchCaseFilters
        {
            get
            {
                if (_searchCaseFilters == null)
                    _searchCaseFilters = new ObservableCollection<FilterInfo>();
                return _searchCaseFilters;
                
            }
            set
            {
                _searchCaseFilters = value;
                this.OnPropertyChanged();
            }
        }

        private ObservableCollection<Flag> _flags;
        public ObservableCollection<Flag> Flags
        {
            get
            {
                if (_flags == null)
                    _flags = new ObservableCollection<Flag>();
                return _flags;
            }
            set
            {
                _flags = value;
                this.OnPropertyChanged();
            }
        }
        private List<DataSource> _dataSources;
        public List<DataSource> DataSources
        {
            get
            {
                return _dataSources;
            }
            set
            {
                _dataSources = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region Commands

        private DelegateCommand<Window> _searchCommand;
        public ICommand SearchCommand
        {
            get
            {
                return _searchCommand ?? (_searchCommand = new DelegateCommand<Window>((window) =>
                {
                    OnAdvanceSearch(window);
                },CanExecuteSearch));
            }
        }

        /// <summary>
        /// Save search button is enabled only if 
        /// - none of the filters have validation errors AND
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private bool CanExecuteSearch(Window arg)
        {
            var hasError = SearchCaseFilters.Where(filter => filter.HasErrors == true).Count();
            return (hasError == 0);
        }

        private DelegateCommand<FilterInfo> _addFilterCommand;
        public ICommand AddFilterCommand
        {
            get
            {
                return _addFilterCommand ?? (_addFilterCommand = new DelegateCommand<FilterInfo>((caseFilter) =>
                {
                        OnAddFilter((FilterInfo)caseFilter.Clone());
                }));
            }
        }

        private DelegateCommand<FilterInfo> _removeFilterCommand;
        public ICommand RemoveFilterCommand
        {
            get
            {
                return _removeFilterCommand ?? (_removeFilterCommand = new DelegateCommand<FilterInfo>((caseFilter) =>

                  {
                      OnRemoveFilter(caseFilter);
                      _searchCommand.RaiseCanExecuteChanged();
                  }));
            }
        }

        private DelegateCommand _checkHasErrorsCommand;
        public ICommand CheckHasErrorsCommand
        {
            get
            {
                return _checkHasErrorsCommand ?? (_checkHasErrorsCommand = new DelegateCommand(() =>
                {
                    _searchCommand.RaiseCanExecuteChanged();
                }));
            }
        }

        #endregion

        #region Methods


        /// <summary>
        /// On advance search
        /// - Create search criteria
        /// - Get the new cases
        /// - Save the search criteria
        /// - Close advance search view
        /// </summary>
        /// <param name="advanceSearchWindow"> advance search view which is to be closed</param>
        private void OnAdvanceSearch(Window advanceSearchWindow)
        {
            var selectedDataSources = DataSources.Where(dataSource => dataSource.IsSelected);

            if (selectedDataSources.Count() > 0)
            {
                // We handle exceptions from multiple threads
                var dataSourceExceptions = new ConcurrentQueue<string>();
                var filters = new List<Filter>();

                // Build search criteria with filters and flags selected
                SearchCriteria.SearchFilters = SearchCaseFilters.ToList();
                var flags = Flags.Where(flag => flag.IsChecked == true).Select(x => x.FlagName).ToList();
                foreach (string flag in flags)
                {
                    SearchCriteria.SearchFilters.Add(new FilterInfo() { FieldName = Literal.Strings.Lookup("cbStr-Flag"), FieldType = FieldTypeEnum.Text, IsCaseTableField = true, Value = flag });
                }
                foreach (FilterInfo filter in SearchCriteria.SearchFilters)
                {
                    filters.Add(new Filter() { FieldName = filter.FieldName, FieldType = filter.FieldType, Value = filter.Value, Operator = filter.Operator == null ? string.Empty : filter.Operator.SqlOperator, IsCaseTableField = filter.IsCaseTableField });
                }
                Criteria criteria = new Criteria() { Filters = filters };

                Parallel.ForEach(selectedDataSources, dataSource =>
                {
                    if (dataSource.IsActive)
                    {
                        try
                        {
                            var cases = _caseSearchLogic.GetCases(dataSource, criteria);
                            if (cases != null)
                            {
                                _eventAggregator.GetEvent<CaseListChangedEvent>().Publish(new CaseListChangedEventParameters(cases, dataSource));
                            }
                        }
                        catch (DALException exception)
                        {
                            dataSourceExceptions.Enqueue(dataSource.Name);
                            LogManager.Error("Filter Error: ", exception);
                        }
                    }
                });

                if (dataSourceExceptions.Count > 0)
                {
                    MessageBoxResult result = CustomPopUp.Show(string.Join(Environment.NewLine, dataSourceExceptions) + Environment.NewLine + Environment.NewLine + Literal.Strings.Lookup("cbStr-UnableToConnectDataSource"), Literal.Strings.Lookup("cbStr-AdvanceSearchCases"), MessageBoxButton.OK, MessageBoxImage.Warning);
                }

                SaveSearchCriteria();
                //TODO - Use interaction trigger to pop up and close the view and we don’t need  close or show dialogue
                advanceSearchWindow.Close();
            }
            else
            {
                CustomPopUp.Show(Literal.Strings.Lookup("cbStr-AdvanceSearchMessage"), Literal.Strings.Lookup("cbStr-AdvanceSearch"), System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information);
            }
        }

        public void SaveSearchCriteria()
        {
            var selectedDataSources = DataSources.Where(dataSource => dataSource.IsSelected);
            SearchCriteria.DataSources = selectedDataSources.Select(x => x.ID.ToString()).ToList();
            //Saving flags also as filters.
            SearchCriteria.SearchFilters = SearchCaseFilters.ToList();
            var flags = Flags.Where(flag => flag.IsChecked == true).Select(x => x.FlagName).ToList();
            foreach (string flag in flags)
            {
                SearchCriteria.SearchFilters.Add(new FilterInfo() { FieldName = Literal.Strings.Lookup("cbStr-Flag"), FieldType = FieldTypeEnum.Text, IsCaseTableField = true, Value = flag });
            }
            SearchCriteria.SaveSearchCriteriaToSettings();
        }

        /// <summary>
        /// Sets flags and filters for advance search view
        /// </summary>
        private void SetFlagsAndFilters()
        {
            ConcurrentQueue<string> dataSourceExceptions = new ConcurrentQueue<string>(); ;
            // Initialize the Flags property in the view
            Flags = _caseSearchLogic.GetFlags(SearchCriteria, out dataSourceExceptions).ToObservableCollection() ;
            SetFilters();
            if (dataSourceExceptions.Count > 0)
            {
                MessageBoxResult result = CustomPopUp.Show(string.Join(Environment.NewLine, dataSourceExceptions) + Environment.NewLine + Environment.NewLine + Literal.Strings.Lookup("cbStr-UnableToConnectDataSource"), Literal.Strings.Lookup("cbStr-AdvanceSearchCases"), MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        /// <summary>
        /// Sets filters for advance search  view 
        /// </summary>
        private void SetFilters()
        {
            List<FilterInfo> Filters = new List<FilterInfo>();
            Filters.AddRange(_caseSearchLogic.StaticCaseFilters);
            var sync = new object();

            var additionalFilters = new List<Filter>();
            Parallel.ForEach(DataSourceManager.Instance.DataSourceCollection, dataSource =>
            {
                if (dataSource.IsActive)
                {
                    try
                    {
                        var additionalfilterslist = _caseSearchLogic.GetAdditionalFilters(dataSource);
                        lock(sync)
                        {
                            additionalFilters.AddRange(additionalfilterslist);
                        }
                    }
                    catch (DALException exception)
                    {
                        LogManager.Error("Get filters: ", exception);
                    }
                }
            });
            //Remove flags from SerachFiltersDisplay and searchcriteria
            SearchCaseFilters = SearchCriteria.SearchFilters.ToObservableCollection();
            var tempSearchFiler = SearchCaseFilters.ToList();
            tempSearchFiler.RemoveAll(x => x.FieldName == Literal.Strings.Lookup("cbStr-Flag"));
            SearchCaseFilters = tempSearchFiler.ToObservableCollection();

            var additonalFields = additionalFilters.DistinctBy(x => x.FieldName.ToLower()).ToList();
            foreach (Filter Filter in additonalFields)
            {
                FilterInfo casefilter = new FilterInfo() { FieldName = Filter.FieldName, FieldType = Filter.FieldType, IsCaseTableField = false };
                Filters.Add(casefilter);
            }
            // Initialize the Properties of the view
            AllCaseFilters = Filters.ToObservableCollection();

        }
        private void OnRemoveFilter(FilterInfo caseFilter)
        {
            if (caseFilter != null)
            {
                SearchCaseFilters.Remove(caseFilter);
            }
        }
        /// <summary>
        /// On adding filter restrict the duplicate filters
        /// </summary>
        /// <param name="caseFilter"></param>
        private void OnAddFilter(FilterInfo caseFilter)
        {
            if (caseFilter != null)
            {
                // For date and number types, the search filter can be added twice.
                var count = SearchCaseFilters.Where(x => x.FieldName == caseFilter.FieldName).Count();
                if (caseFilter.FieldType == FieldTypeEnum.Date || caseFilter.FieldType == FieldTypeEnum.Number)
                {
                    if (count < 2)
                        SearchCaseFilters.Add(caseFilter);
                    caseFilter.Operator = CaseFilterOperators[0];
                }
                else
                {
                    if (count < 1)
                        SearchCaseFilters.Add(caseFilter);
                }
            }
        }
    }
    #endregion

}


