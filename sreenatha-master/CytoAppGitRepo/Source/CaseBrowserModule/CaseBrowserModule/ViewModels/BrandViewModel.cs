﻿using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CaseBrowserModule.ViewModels
{
    /// <summary>
    /// Case Navigation 
    /// </summary>
    [Export]
    class BrandViewModel : BindableBase
    {
        #region Constructor
        public BrandViewModel()
        {
        }
        #endregion

        #region Properties

        public string ProductVersion
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute), false);
                return attributes.Length == 0 ?"" :((AssemblyInformationalVersionAttribute)attributes[0]).InformationalVersion;
            }
        }

        #endregion
    }
}
