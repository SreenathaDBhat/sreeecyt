﻿using CaseBrowserModule.Events;
using CaseBrowserModule.Logic;
using CaseBrowserModule.Models;
using CytoApps.Exceptions;
using CytoApps.Infrastructure.Helpers;
using CytoApps.Infrastructure.UI.Controls;
using CytoApps.Infrastructure.UI.Events;
using CytoApps.Infrastructure.UI.Models;
using CytoApps.Localization;
using CytoApps.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;


namespace CaseBrowserModule.ViewModels
{
    /// <summary>
    /// Case Navigation 
    /// </summary>
    [Export]
    public class CaseBrowserNavigationViewModel : BindableBase
    {

        #region private memebers
        private IEventAggregator _eventAggregator;
        private CaseSearchLogic _caseSearchLogic;
        public bool canOpen = true;

        #endregion

        #region Constructor

        public CaseBrowserNavigationViewModel(IEventAggregator eventAggregator, CaseSearchLogic caseSearchLogic)
        {
            _eventAggregator = eventAggregator;
            _caseSearchLogic = caseSearchLogic;
            _eventAggregator.GetEvent<CaseListChangedEvent>().Subscribe(UpdateCaseCollections);
            _eventAggregator.GetEvent<DataSourceDeletedEvent>().Subscribe(UpdateCases);
        }
        #endregion

        #region Properties


        private ObservableCollection<CaseInfo> _cases;
        public ObservableCollection<CaseInfo> Cases
        {
            get
            {
                if (_cases == null)
                {
                    _cases = new ObservableCollection<CaseInfo>();
                }
                return _cases;
            }
            set
            {
                _cases = value;
                this.OnPropertyChanged();
            }
        }

        public ICollection<CaseInfo> FilteredList
        {
            get;
            set;
        }

        private ObservableCollection<CaseInfo> _pinnedCases;
        public ObservableCollection<CaseInfo> PinnedCases
        {
            get
            {
                if (_pinnedCases == null)
                {
                    _pinnedCases = new ObservableCollection<CaseInfo>();
                }
                return _pinnedCases;
            }
            set
            {
                _pinnedCases = value;
                this.OnPropertyChanged();
            }
        }

        private CaseInfo _activeCase;
        public CaseInfo ActiveCase
        {
            get
            {
                return _activeCase;
            }
            set
            {
                if (_activeCase != value)
                {
                    LockorUnlockCases(_activeCase, value);
                    _activeCase = value;
                    OnPropertyChanged();
                    Properties.CaseBrowser.Default.LastAccessedCase = _activeCase;
                    Properties.CaseBrowser.Default.Save();
                }
            }
        }


        /// <summary>
        /// Locks new case and Unlocks the old case
        /// </summary>
        /// <param name="oldValue">last selected case</param>
        /// <param name="newValue">new selected case</param>
        private void LockorUnlockCases(CaseInfo oldValue, CaseInfo newValue)
        {
            if (oldValue != newValue)
            {
                ArrayList cases = new ArrayList(2);
                cases.Add(oldValue ?? new CaseInfo());
                cases.Add(newValue ?? new CaseInfo());
                _eventAggregator.GetEvent<CaseSelectionChangedEvent>().Publish(cases);
            }
        }

        private string _searchFilter;
        public string SearchFilter
        {
            get
            {
                if (_searchFilter == null)
                {
                    _searchFilter = string.Empty;
                }
                return _searchFilter;
            }
            set
            {
                _searchFilter = value;
                OnPropertyChanged();
            }
        }

        private bool _isOpenCaseStatusPopup;

        public bool IsOpenCaseStatusPopup
        {
            get
            {
                return _isOpenCaseStatusPopup;
            }
            set
            {
                _isOpenCaseStatusPopup = value;
                OnPropertyChanged();
            }
        }

        private bool _stayOpenStatusPopup = false;
        public bool StayOpenStatusPopup
        {
            get
            {
                return _stayOpenStatusPopup;
            }
            set
            {
                _stayOpenStatusPopup = value;
                OnPropertyChanged();
            }
        }

        private string _activeList;
        public string ActiveList
        {
            get
            {

                return _activeList;
            }
            set
            {
                _activeList = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Commands

        private DelegateCommand _advanceSearchCommand;
        public ICommand AdvanceSearchCommand
        {
            get
            {
                return _advanceSearchCommand ?? (_advanceSearchCommand = new DelegateCommand(() =>
                {
                    ClearCaseList();
                    _eventAggregator.GetEvent<AdvanceSearchEvent>().Publish(null);
                }, () => true));
            }
        }

        private DelegateCommand _searchCommand;
        public ICommand SearchCommand
        {
            get
            {
                return _searchCommand ?? (_searchCommand = new DelegateCommand(() =>
                {
                    ClearCaseList();
                    SearchCases();

                }));
            }
        }

        /// <summary>
        /// Normal search operation in main window
        /// </summary>
        /// <param name="SearchFilter"></param>
        private void SearchCases()
        {
            Cases.Clear();
            var dataSourceExceptions = new ConcurrentQueue<string>();
            SearchCriteriaInfo searchCriteria = new SearchCriteriaInfo();
            searchCriteria.SearchFilters = new List<FilterInfo>();
            var filters = new List<Filter>();
            searchCriteria.SearchFilters.Add(new FilterInfo() { FieldName = Literal.Strings.Lookup("cbStr-CaseNameandNumber"), FieldType = FieldTypeEnum.Text, IsCaseTableField = true, Value = SearchFilter });
            foreach (FilterInfo filter in searchCriteria.SearchFilters)
            {
                filters.Add(new Filter() { FieldName = filter.FieldName, FieldType = filter.FieldType, Value = filter.Value, Operator = filter.Operator == null ? string.Empty : filter.Operator.SqlOperator, IsCaseTableField = filter.IsCaseTableField });
            }

            Criteria criteria = new Criteria() { Filters = filters };

            Parallel.ForEach(DataSourceManager.Instance.DataSourceCollection, dataSource =>
            {
                if (dataSource.IsActive)
                {
                    try
                    {
                        
                        var cases = _caseSearchLogic.GetCases(dataSource, criteria);
                        if (cases != null)
                        {   
                            UpdateCaseCollections(cases,dataSource);
                        }
                    }
                    catch (DALException exception)

                    {
                        dataSourceExceptions.Enqueue(dataSource.Name);
                        LogManager.Error("Search Cases Error: ", exception);
                    }
                }
            });
            if (dataSourceExceptions.Count > 0)
            {
                MessageBoxResult result = CustomPopUp.Show(string.Join(Environment.NewLine, dataSourceExceptions) + Environment.NewLine + Environment.NewLine + Literal.Strings.Lookup("cbStr-UnableToConnectDataSource"), Literal.Strings.Lookup("cbStr-SearchCases"), MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private DelegateCommand<CaseInfo> _pinCommand;
        public ICommand PinCommand
        {
            get
            {
                return _pinCommand ?? (_pinCommand = new DelegateCommand<CaseInfo>((selectedCase) =>
                {
                    OnPinCase(selectedCase);
                }));
            }
        }

        private DelegateCommand<object> _openCaseStatusPopupCommand;
        public ICommand OpenCaseStatusPopupCommand
        {
            get
            {
                return _openCaseStatusPopupCommand ?? (_openCaseStatusPopupCommand = new DelegateCommand<object>((selectedCase) =>
                {
                    ArrayList list = (ArrayList)selectedCase;
                    var selectedItem = list[0] as CaseInfo;

                    List<CaseInfo> caseList = new List<CaseInfo>();
                    if (list[1].ToString() == "Case")
                    {
                        ActiveList = "Case";
                        caseList = Cases.Where(x => x.IsSelected == true).ToList();
                    }

                    else if (list[1].ToString() == "PinnedCase")
                    {
                        ActiveList = "PinnedCase";
                        caseList = PinnedCases.Where(x => x.IsSelected == true).ToList();
                    }
                    if (canOpen)
                    {
                        IsOpenCaseStatusPopup = true;
                        _eventAggregator.GetEvent<GetUniqueFlagsEvent>().Publish(caseList);
                    }
                    canOpen = true;
                }));
            }
        }



        private DelegateCommand<CaseStatus> _openColorPickerPopupCommand;
        public ICommand OpenColorPickerPopupCommand
        {
            get
            {
                return _openColorPickerPopupCommand ?? (_openColorPickerPopupCommand = new DelegateCommand<CaseStatus>((caseStatus) =>
                {
                    StayOpenStatusPopup = true;
                    IsOpenCaseStatusPopup = true;
                    _eventAggregator.GetEvent<OpenColorPickerEvent>().Publish(caseStatus);
                    StayOpenStatusPopup = false;


                }));
            }
        }
        private DelegateCommand<CaseStatus> _selectedStatusCommand;
        public ICommand SelectedStatusCommand
        {
            get
            {
                return _selectedStatusCommand ?? (_selectedStatusCommand = new DelegateCommand<CaseStatus>((SelectedStatus) =>
                {
                    _eventAggregator.GetEvent<UpdateCaseStatusEvent>().Publish(SelectedStatus);
                    IsOpenCaseStatusPopup = false;
                }));
            }
        }

        private ObservableCollection<CaseStatus> _caseStatusCollection;
        public ObservableCollection<CaseStatus> CaseStatsusCollection
        {
            get
            {
                if (_caseStatusCollection == null)
                    _caseStatusCollection = new ObservableCollection<CaseStatus>();
                return _caseStatusCollection;
            }
            set
            {
                _caseStatusCollection = value;
                OnPropertyChanged();
            }
        }

        private CaseStatus _selectedStatus = null;
        public CaseStatus SelectedStatus
        {
            get
            {
                return _selectedStatus;
            }
            set
            {
                _selectedStatus = value;
                OnPropertyChanged();
            }
        }


        #endregion

        #region Methods

        private void UpdateCaseCollections(IEnumerable<Case> cases, DataSource dataSource)
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
            {
                foreach (Case @case in cases)
                {
                    CaseInfo caseinfo = GetPinnedCase(@case, dataSource);
                    if (caseinfo != null)
                        caseinfo.IsPinned = true;
                    else
                        caseinfo = new CaseInfo(@case, dataSource);
                    Cases.Add(caseinfo);

                    //update the status color               
                    CaseStatus caseStatus = CaseStatsusCollection.Where(x => x.Status == caseinfo.Flag).FirstOrDefault();
                    if (caseStatus != null)
                    {
                        caseinfo.Color = caseStatus.Color.ToString();
                    }
                    else
                    {
                        caseinfo.Color = Application.Current.Resources["TextBlockForegroundBrush"].ToString();
                    }

                }
            }));
        }

        /// <summary>
        /// update the case collection and check the case in pinned or not 
        ///  Check if case is already in the pinned list, if so, use that, otherwise add to main case list only
        /// </summary>
        /// <param name="cases"></param>
        /// <param name="dataSource"></param>
        private void UpdateCaseCollections(CaseListChangedEventParameters obj)
        {
            IEnumerable<Case> cases = obj.CaseList;
            DataSource dataSource = obj.DataSource;

            UpdateCaseCollections(cases, dataSource);
        }

        private void UpdateCases(DataSource dataSource)
        {
            // code to remove pinned cases that belong to the deleted datasource

            var cases = _caseSearchLogic.GetCases(dataSource, null);

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
            {
                foreach (Case caseItem in cases)
                {
                    CaseInfo pinnedCase = GetPinnedCase(caseItem, dataSource);
                    if (pinnedCase != null)
                    {
                        pinnedCase.IsPinned = false;
                        PinnedCases.Remove(pinnedCase);
                    }

                }

                var casesFromDeletedSource = Cases.Where(c => c.LabID == dataSource.ID).ToList();
                foreach (var caseItem in casesFromDeletedSource)
                {
                    Cases.Remove(caseItem);
                }

                SavePinnedCases();
            }));

            

        }

        private void ClearCaseList()
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
            {
                Cases.Clear();
            }));
        }
            
        /// <summary>
        ///  Check if case is already in the pinned list if so return same reference
        /// </summary>
        /// <param name="case"></param>
        /// <param name="dataSource"></param>
        /// <returns></returns>
        private CaseInfo GetPinnedCase(Case @case, DataSource dataSource)
        {
            CaseInfo caseinfo = null;
            if (PinnedCases.Where(c => c.LabID == dataSource.ID).Count() > 0)
            {
                caseinfo = PinnedCases.Where(caseInfo => caseInfo.CaseId == @case.Id && caseInfo.CaseName == @case.Name && caseInfo.LabID == dataSource.ID).FirstOrDefault();
            }
            return caseinfo;
        }
        private void OnPinCase(CaseInfo selectedCase)
        {
            if (selectedCase != null)
            {
                if (!selectedCase.IsPinned)
                {
                    selectedCase.IsPinned = true;
                    PinnedCases.Add(selectedCase);
                }
                else
                {
                    selectedCase.IsPinned = false;
                    PinnedCases.Remove(selectedCase);
                }
                SavePinnedCases();
            }
        }

        /// <summary>
        ///  on add or remove case from the pinned list, save new pinnedcaselist to case browser application settings.
        /// </summary>
        private void SavePinnedCases()
        {
            Properties.CaseBrowser.Default.PinnedCases = PinnedCases.ToList();
            Properties.CaseBrowser.Default.Save();
        }
        #endregion
    }
}
