﻿using CytoApps.Models;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Windows.Input;
using CaseBrowserModule.Models;
using Prism.Events;
using CaseBrowserModule.Events;
using System.Windows.Data;

namespace CaseBrowserModule.ViewModels
{
    /// <summary>
    /// Case Detail 
    /// </summary>
    [Export]
    public class CaseDetailsViewModel : BindableBase
    {
        #region Properties
        Prism.Events.IEventAggregator _eventAggregator;

        private ObservableCollection<CaseDetail> _caseDetails;
        public ObservableCollection<CaseDetail> CaseDetails
        {
            get
            {
                return _caseDetails;
            }
            set
            {
                _caseDetails = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<CaseDetail> ClonedCaseDetails
        {
            get;
            set;
        }

        private CaseInfo _selectedCaseInfo;
        public CaseInfo SelectedCaseInfo
        {
            get
            {
                return _selectedCaseInfo;
            }
            set
            {
                _selectedCaseInfo = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<Slide> _slides;
        public ObservableCollection<Slide> Slides
        {
            get
            {
                return _slides;
            }
            set
            {
                _slides = value;
                OnPropertyChanged();
            }
        }

        private Slide _selectedSlide;
        public Slide SelectedSlide
        {
            get
            {
                return _selectedSlide;
            }
            set
            {
                _selectedSlide = value;
            }
        }

        private bool _editMode = false;
        public bool EditMode
        {
            get { return _editMode; }
            set
            {
                _editMode = value;
                OnPropertyChanged();
            }
        }

        private bool _isVisible = false;
        public bool IsVisible
        {
            get
            {
                return _isVisible;
            }
            set
            {
                _isVisible = value;
                OnPropertyChanged();
            }
        }

        private bool _isUsersVisible;
        public bool IsUsersVisible
        {
            get { return _isUsersVisible; }
            set
            {
                _isUsersVisible = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<UserDetails> _caseUsers;
        public ObservableCollection<UserDetails> CaseUsers
        {
            get { return _caseUsers; }
            set
            {
                _caseUsers = value;
                OnPropertyChanged();
            }
        }

        private bool _isUsersPopupOpen;
        public bool IsUsersPopupOpen
        {
            get { return _isUsersPopupOpen; }
            set
            {
                _isUsersPopupOpen = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Commands
        private DelegateCommand<CaseDetail> _editCaseDetailsCommand;
        public ICommand EditCaseDetailsCommand
        {
            get
            {
                return _editCaseDetailsCommand ?? (_editCaseDetailsCommand = new DelegateCommand<CaseDetail>((CaseDetail) =>
                {
                    EditCaseDetails();
                }));
            }
        }

        private DelegateCommand<CaseDetail> _saveCaseDetailsCommand;
        public ICommand SaveCaseDetailsCommand
        {
            get
            {
                return _saveCaseDetailsCommand ?? (_saveCaseDetailsCommand = new DelegateCommand<CaseDetail>((CaseDetail) =>
                {
                    SaveCaseDetails();
                }));
            }
        }

        private DelegateCommand<CaseDetail> _cancelCaseDetailsCommand;
        public ICommand CancelCaseDetailsCommand
        {
            get
            {
                return _cancelCaseDetailsCommand ?? (_cancelCaseDetailsCommand = new DelegateCommand<CaseDetail>((CaseDetail) =>
                {
                    CancelCaseDetails();
                }));
            }
        }

        private DelegateCommand _openUsersPopupCommand;
        public ICommand OpenUsersPopupCommand
        {
            get
            {
                return _openUsersPopupCommand ?? (_openUsersPopupCommand = new DelegateCommand(() =>
                {
                    if (SelectedCaseInfo != null)
                    {
                        _eventAggregator.GetEvent<CurrentCaseUsersEvent>().Publish(SelectedCaseInfo);
                    }
                }));
            }
        }
        #endregion

        #region Methods
        private void EditCaseDetails()
        {
            EditMode = true;
            CollectionViewSource.GetDefaultView(CaseDetails).Refresh();
        }

        private void SaveCaseDetails()
        {
            EditMode = false;
            CollectionViewSource.GetDefaultView(CaseDetails).Refresh();
            _eventAggregator.GetEvent<SelectedCaseDetailsEvent>().Publish(SelectedCaseInfo);
        }

        private void CancelCaseDetails()
        {
            CaseDetails.Clear();
            foreach (CaseDetail caseDetail in ClonedCaseDetails)
            {
                CaseDetails.Add((CaseDetail)caseDetail.Clone());
            }
            EditMode = false;
            CollectionViewSource.GetDefaultView(CaseDetails).Refresh();
            ///  _eventAggregator.GetEvent<SelectedCaseEvent>().Publish(SelectedCaseDetail);

        }
        #endregion

        #region Constructor
        public CaseDetailsViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }
        #endregion
    }
}
