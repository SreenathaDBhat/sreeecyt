﻿using CaseBrowserModule.Views;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;

namespace CaseBrowserModule.ViewModels
{
    /// <summary>
    ///  this Viewmodel holds the collection of plugin-view recived from the published modules
    /// </summary>
    [Export]
    class PluginViewModel
    {
        private ObservableCollection<SlideView> _slideCollection;
        public ObservableCollection<SlideView> SlideCollection
        {
            get
            {
                if (_slideCollection == null)
                    _slideCollection = new ObservableCollection<SlideView>();
                return _slideCollection;
            }
            set { _slideCollection = value; }
        }
    }
}
