﻿using CytoApps.Models;
using Prism.Mvvm;

namespace CaseBrowserModule.ViewModels
{
    /// <summary>
    /// This view model holds the single slide information that is published to pluggable modules
    /// </summary>
    public class SlideViewModel : BindableBase
    {
        public SlideViewModel(CaseDataProxy caseDataProxy)
        {
            _slide = caseDataProxy.Slide;
        }

        private Slide _slide;
        public Slide Slide
        {
            get { return _slide; }
            set
            {
                _slide = value;
                OnPropertyChanged();
            }
        }
    }

}
