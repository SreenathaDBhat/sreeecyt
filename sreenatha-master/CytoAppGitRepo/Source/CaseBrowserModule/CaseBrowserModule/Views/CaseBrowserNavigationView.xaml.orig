﻿<UserControl x:Class="CaseBrowserModule.Views.CaseBrowserNavigationView"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008"            
             xmlns:Controls="clr-namespace:CytoApps.Infrastructure.UI.Controls;assembly=CytoApps.Infrastructure.UI"
             xmlns:Converters="clr-namespace:CytoApps.Infrastructure.UI.Converters;assembly=CytoApps.Infrastructure.UI"
             xmlns:i="http://schemas.microsoft.com/expression/2010/interactivity"
             xmlns:local="clr-namespace:CaseBrowserModule.Views"    
             xmlns:conv="clr-namespace:CytoApps.Infrastructure.UI.Converters;assembly=CytoApps.Infrastructure.UI"         
             mc:Ignorable="d" 
             d:DesignHeight="300" d:DesignWidth="300">
    <UserControl.Resources>
        <conv:ColorStringToBrushConverter x:Key="ColorStringToBrushConverter"/>
        <conv:SelectedCaseFindSourceConverter x:Key="SelectedCaseFindSourceConverter"/>
        <Style x:Key="RadioButtonStyle" TargetType="{x:Type RadioButton}">
            <Setter Property="Foreground" Value="{DynamicResource {x:Static SystemColors.ControlTextBrushKey}}"/>
            <Setter Property="Background" Value="#F4F4F4"/>
            <Setter Property="BorderBrush" Value="{StaticResource CheckBoxStroke}"/>
            <Setter Property="BorderThickness" Value="1"/>
            <Setter Property="Template">
                <Setter.Value>
                    <ControlTemplate TargetType="{x:Type RadioButton}">
                        <BulletDecorator Background="Transparent">
                            <ContentPresenter HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}" Margin="{TemplateBinding Padding}" RecognizesAccessKey="True" VerticalAlignment="{TemplateBinding VerticalContentAlignment}"/>
                        </BulletDecorator>
                        <ControlTemplate.Triggers>
                            <Trigger Property="HasContent" Value="true">
                                <Setter Property="FocusVisualStyle" Value="{StaticResource CheckRadioFocusVisual}"/>
                                <Setter Property="Padding" Value="4,0,0,0"/>
                            </Trigger>
                            <Trigger Property="IsEnabled" Value="false">
                                <Setter Property="Foreground" Value="{DynamicResource {x:Static SystemColors.GrayTextBrushKey}}"/>
                            </Trigger>
                        </ControlTemplate.Triggers>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
        </Style>
        <Canvas x:Key="appbar_pin" Width="76" Height="76" Clip="F1 M 0,0L 76,0L 76,76L 0,76L 0,0">
            <Path Width="36.271" Height="36.2948" Canvas.Left="19.8645" Canvas.Top="19.8526" Stroke="SkyBlue" StrokeThickness="2" Stretch="Fill" Fill="LightGray" Data="M 53,457 L 69,419 165,322 120,241 124,195 175,158 201,158 237,162 245,166 329,98 313,49 318,29 333,14 341,15 355,16 376,20 400,30 416,41 433,54 442,63 453,77 468,92 472,100 487,130 493,150 493,176 483,188 470,196 432,190 419,184 414,179 342,264 353,295 351,322 348,341 343,361 327,378 314,388 262,387 229,370 208,359 190,343 93,441 52,459">
                <Path.LayoutTransform>
                    <RotateTransform Angle="45"/>
                </Path.LayoutTransform>
            </Path>
        </Canvas>
        <Canvas x:Key="Errorunavailable" Width="76" Height="76" Clip="F1 M 0,0L 76,0L 76,76L 0,76L 0,0">
            <Path Width="36.271" Height="36.2948" Canvas.Left="19.8645" Canvas.Top="19.8526"  Stretch="Fill" Fill="LightGray" Data="M103,443 L103,445 L105,445 L105,443 Z M104,448 C99.5817218,448 96,444.418278 96,440 C96,435.581722 99.5817218,432 104,432 C108.418278,432 112,435.581722 112,440 C112,444.418278 108.418278,448 104,448 Z M103,435 L103,442 L105,442 L105,435 Z M103,435">
            </Path>
        </Canvas>

        <Style x:Key="headerStyle" TargetType="{x:Type TextBlock}">
            <Setter  Property="FontSize" Value="15" />
            <Setter  Property="SnapsToDevicePixels" Value="True" />
            <Setter  Property="VerticalAlignment" Value="Center" />
            <Setter Property="FontWeight" Value="ExtraBold" />
        </Style>
        <Style x:Key="subHeaderStyle" TargetType="{x:Type TextBlock}">
            <Setter  Property="FontSize" Value="12" />
            <Setter  Property="SnapsToDevicePixels" Value="True" />
            <Setter  Property="VerticalAlignment" Value="Center" />
        </Style>


            <DataTemplate  x:Key="CaseListTemplate">
            <Border BorderBrush="LightGray">
                    <Grid Margin="2" >
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="*"/>
                            <ColumnDefinition Width="*"/>
                            <ColumnDefinition Width="Auto"/>
                        </Grid.ColumnDefinitions>
                        <Grid.RowDefinitions>
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                        </Grid.RowDefinitions>
                        <TextBlock VerticalAlignment="Center" Text="{Binding CaseName}" Style="{StaticResource headerStyle}" Grid.Column="0" Grid.Row="0" Grid.ColumnSpan="2"  />
                        <StackPanel Grid.Column="2" Orientation="Horizontal"
                            Grid.Row="0" HorizontalAlignment="Right" >
                        <Button x:Name="pin"    BorderThickness="2"   HorizontalContentAlignment="Right" CommandParameter="{Binding}"
                            Command="{Binding RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type UserControl}},Path=DataContext.PinCommand}" 
                            Margin="0,0,0,0"  VerticalAlignment="Top" Width="50" Height="35"
                            Style="{DynamicResource TransparentButtonStyle}" >
                            <Rectangle Width="20" x:Name="rectangle_pin"
                               Height="20"
                               Fill="{Binding Path=Foreground, RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type Button}}}">
                                <Rectangle.OpacityMask>
                                    <VisualBrush Stretch="Fill"
                                         Visual="{DynamicResource appbar_pin}" />
                                </Rectangle.OpacityMask>
                            </Rectangle>
                        </Button>
                        <Grid ToolTip="Case is currently unavilable">
                            <Rectangle Width="20" x:Name="unavailableIcon"
                               Height="20"
                               Fill="Red">
                                <Rectangle.OpacityMask>
                                    <VisualBrush Stretch="Fill"
                                         Visual="{DynamicResource Errorunavailable}" />
                                </Rectangle.OpacityMask>
                            </Rectangle>
                            <Grid.Style>
                                <Style TargetType="Grid">
                                    <Setter Property="Visibility" Value="Collapsed"/>
                                    <Style.Triggers>
                                        <DataTrigger Binding="{Binding IsAvailable}" Value="False">
                                            <Setter Property="Visibility" Value="Visible" />
                                        </DataTrigger>
                                    </Style.Triggers>
                                </Style>
                            </Grid.Style>
                        </Grid>
                    </StackPanel>
                    <Button Name="btnStatus" VerticalAlignment="Center" Style="{x:Null}" Grid.Column="2" Grid.Row="1"                           
                            Command="{Binding DataContext.OpenCaseStatusPopupCommand,RelativeSource={RelativeSource AncestorType=UserControl, Mode=FindAncestor}}"  >
                        <Button.Template>
                            <ControlTemplate TargetType="Button">
                                <ContentPresenter />
                            </ControlTemplate>
                        </Button.Template>
                        <Button.CommandParameter >
                            <MultiBinding Converter="{StaticResource SelectedCaseFindSourceConverter}" >
                                <Binding />
                                <Binding  ElementName="btnStatus"/>
                            </MultiBinding>
                        </Button.CommandParameter>
                        <TextBlock VerticalAlignment="Center" Text="{Binding Flag, UpdateSourceTrigger=PropertyChanged,Mode=TwoWay}" Foreground="{Binding Color, UpdateSourceTrigger=PropertyChanged,Mode=TwoWay}" >
                            <TextBlock.Style >
                                <Style TargetType="TextBlock" BasedOn="{StaticResource subHeaderStyle}">
                                    <Style.Triggers>
                                        <Trigger Property ="IsMouseOver" Value="True">
                                            <Setter Property= "Foreground" Value="Turquoise"/>
                                            <Setter Property="Cursor" Value="Hand"/>
                                        </Trigger>                                      
                                    </Style.Triggers>
                                </Style>
                            </TextBlock.Style>
                        </TextBlock>
                    </Button>                  

                    <TextBlock VerticalAlignment="Center" Text="{Binding Lab}"  Style="{StaticResource subHeaderStyle}" Grid.Column="1"  ToolTip="{Binding Lab}" Grid.Row="1" HorizontalAlignment="Left" />
                    <TextBlock  Grid.Row="1" Grid.Column="0" VerticalAlignment="Center" Style="{StaticResource subHeaderStyle}" Text="{Binding Case.LastAccessed, StringFormat=D}" />
                </Grid>
            </Border>
            <DataTemplate.Triggers>
                <DataTrigger Binding="{Binding IsPinned}" Value="True">
                    <DataTrigger.Setters>
                        <Setter TargetName="pin" Property="Foreground" Value="Green"/>
                        <Setter TargetName="rectangle_pin" Property="LayoutTransform">
                            <Setter.Value>
                                <RotateTransform Angle="-45"/>
                            </Setter.Value>
                        </Setter>
                    </DataTrigger.Setters>
                </DataTrigger>
            </DataTemplate.Triggers>
        </DataTemplate>
                   

    </UserControl.Resources>

    <Grid>
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="*"/>
        </Grid.RowDefinitions>
<<<<<<< HEAD

        <Controls:SearchTextBox LabelText="Search Cases" SearchMode="Delayed" Height="25" Margin="1"  Grid.Row="0" Grid.Column="0" Text="{Binding SearchFilter,UpdateSourceTrigger=PropertyChanged}" >
            <i:Interaction.Triggers>
                <i:EventTrigger EventName="Search">
                    <i:InvokeCommandAction Command="{Binding SearchCommand}" CommandParameter="{Binding RelativeSource={RelativeSource Self}}" />
                </i:EventTrigger>
            </i:Interaction.Triggers>
        </Controls:SearchTextBox>
        <Button Grid.Row="0" Grid.Column="1" Command="{Binding AdvanceSearchCommand}" >
            <Path Fill="Turquoise" Data="M 0 0 L 6 6 L 12 0 Z" />
        </Button>

        <ListBox Tag="Case" x:Name="cases"  HorizontalContentAlignment="Stretch" Grid.Row="1" Grid.Column="0" Grid.ColumnSpan="2" SelectionMode="Extended"   
                  ItemTemplate="{StaticResource CaseListTemplate}" ItemsSource="{Binding Cases,Mode=TwoWay,UpdateSourceTrigger=PropertyChanged}" SelectedItem="{Binding SelectedCase,UpdateSourceTrigger=PropertyChanged,Mode=TwoWay}" >
            
        </ListBox>
        <Label Margin="2" Grid.Column="0" Grid.Row="1" Grid.ColumnSpan="2" Content="No cases found" FontWeight="Bold" FontStyle="Italic" HorizontalAlignment="Center">
            <Label.Style >
                <Style TargetType="{x:Type Label}">
                    <Setter Property="Visibility" Value="Collapsed"/>
                    <Style.Triggers>
                        <DataTrigger Binding="{Binding ElementName=cases, Path=Items.Count}" Value="0">
                            <Setter Property="Visibility" Value="Visible"/>
                        </DataTrigger>
                    </Style.Triggers>

                </Style>
            </Label.Style>
        </Label>

        <GridSplitter Grid.Row="2" Grid.Column="0" Grid.ColumnSpan="2" Height="5" HorizontalAlignment="Stretch" ResizeDirection="Rows"/>

        <ListBox Tag="PinnedCase"  Name="PinnedCases" HorizontalContentAlignment="Stretch" Grid.Row="3" Grid.Column="0" Grid.ColumnSpan="2"  SelectedItem="{Binding SelectedPinnedCase,UpdateSourceTrigger=PropertyChanged,Mode=TwoWay}"
                  ItemTemplate="{StaticResource CaseListTemplate}" ItemsSource="{Binding PinnedCases, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}" SelectionMode="Extended">
        </ListBox>

=======
        <Grid HorizontalAlignment="Stretch">
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="*"/>
                <!--<ColumnDefinition Width="Auto"/>-->
            </Grid.ColumnDefinitions>
            <Controls:SearchTextBox LabelText="Search Cases" SearchMode="Delayed" Height="25" HorizontalAlignment="Stretch"
                Text="{Binding SearchFilter,UpdateSourceTrigger=PropertyChanged}" SearchOptionCommand="{Binding AdvanceSearchCommand}" >
                <i:Interaction.Triggers>
                    <i:EventTrigger EventName="Search">
                        <i:InvokeCommandAction Command="{Binding SearchCommand}" CommandParameter="{Binding RelativeSource={RelativeSource Self}}" />
                    </i:EventTrigger>
                </i:Interaction.Triggers>
            </Controls:SearchTextBox>
            <!--<Button Command="{Binding AdvanceSearchCommand}" Height="20" Grid.Column="1">
                <Path Fill="Turquoise" Data="M 0 0 L 6 6 L 12 0 Z" />
            </Button>-->
        </Grid>
        <Grid Grid.Row="1" Margin="0,6,0,0">
            <Grid.RowDefinitions>
                <RowDefinition Height="60*"/>
                <RowDefinition Height="6"/>
                <RowDefinition Height="35*" MinHeight="200" MaxHeight="300"/>
            </Grid.RowDefinitions>
            <Label Margin="2" Content="No cases found" FontWeight="Bold" FontStyle="Italic" HorizontalAlignment="Center" Panel.ZIndex="1">
                <Label.Style >
                    <Style TargetType="{x:Type Label}">
                        <Setter Property="Visibility" Value="Collapsed"/>
                        <Style.Triggers>
                            <DataTrigger Binding="{Binding ElementName=cases, Path=Items.Count}" Value="0">
                                <Setter Property="Visibility" Value="Visible"/>
                            </DataTrigger>
                        </Style.Triggers>

                    </Style>
                </Label.Style>
            </Label>
            <ListBox x:Name="cases" SelectedItem="{Binding SelectedCase}" HorizontalContentAlignment="Stretch" 
                  ItemTemplate="{StaticResource CaseListTemplate}" ItemsSource="{Binding Cases,Mode=TwoWay,UpdateSourceTrigger=PropertyChanged}" SelectionMode="Extended">
            </ListBox>
            <GridSplitter Grid.Row="1" HorizontalAlignment="Stretch" ResizeDirection="Rows" Style="{DynamicResource HorizontalBevelGripGridSplitter}"/>
            <ListBox  SelectedItem="{Binding SelectedPinnedCase}" HorizontalContentAlignment="Stretch" Grid.Row="2"
            ItemTemplate="{StaticResource CaseListTemplate}" ItemsSource="{Binding PinnedCases, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}" SelectionMode="Extended">
            </ListBox>
        </Grid>
>>>>>>> f2a652a13189426d02eb76e8eb7e91d68988a5d1
        <Popup Name="CaseStatusPopup"  Placement="MousePoint" StaysOpen="{Binding StayOpenStatusPopup,UpdateSourceTrigger=PropertyChanged}"  IsOpen="{Binding IsOpenCaseStatusPopup}" Margin="5" PopupAnimation="Slide" AllowsTransparency="True" MinWidth="150">
            <Border  Padding="0" MinWidth="80" MinHeight="40" BorderThickness="1" CornerRadius="10" Background="{ DynamicResource WindowBackground}" BorderBrush="{ DynamicResource WindowBorder}">

                <ItemsControl HorizontalAlignment="Stretch" ItemsSource="{Binding CaseStatsusCollection , Mode=TwoWay ,UpdateSourceTrigger=PropertyChanged}" >
                    <ItemsControl.ItemTemplate>
                        <DataTemplate>
                            <Grid Margin="2">
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="100"/>
                                    <ColumnDefinition Width="5"/>
                                    <ColumnDefinition Width="30"/>
                                </Grid.ColumnDefinitions>
                                <Button  Grid.Column="0" Style="{DynamicResource TransparentButtonStyle}" HorizontalAlignment="Left"  Command="{Binding ElementName=CaseStatusPopup, Path=DataContext.SelectedStatusCommand}" CommandParameter="{Binding}" >
                                    <TextBlock Text="{Binding Status}" Foreground="{Binding Color,UpdateSourceTrigger=PropertyChanged ,Mode=TwoWay}">
                                        <TextBlock.Style >
                                            <Style TargetType="TextBlock">
                                                <Style.Triggers>
                                                    <Trigger Property ="IsMouseOver" Value="True">
                                                        <Setter Property= "Foreground" Value="Turquoise"/>
                                                        <Setter Property="Cursor" Value="Hand"/>
                                                    </Trigger>
                                                </Style.Triggers>
                                            </Style>
                                        </TextBlock.Style>
                                    </TextBlock>
                                </Button>
                                <Button Style="{x:Null}" Width="20" Height="20" Grid.Column="2" HorizontalAlignment="Right" VerticalAlignment="Center" Command="{Binding ElementName=CaseStatusPopup, Path=DataContext.OpenColorPickerPopupCommand}" CommandParameter="{Binding}" >
                                    <Button.Template>
                                        <ControlTemplate>
                                            <Grid>
                                                <Ellipse Fill="{Binding Color,Converter={StaticResource ColorStringToBrushConverter},UpdateSourceTrigger=PropertyChanged ,Mode=TwoWay}" Stroke="Black">
                                                </Ellipse>
                                            </Grid>
                                        </ControlTemplate>
                                    </Button.Template>
                                </Button>
                            </Grid>
                        </DataTemplate>
                    </ItemsControl.ItemTemplate>
                </ItemsControl>

            </Border>
        </Popup>

    </Grid>
</UserControl>
