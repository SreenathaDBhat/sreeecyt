﻿using CytoApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace CaseDataAccess.CaseSearch
{
    [DataContract]
    public class CaseWithCaseDetails
    {
        [DataMember]
        public Case Case
        {
            get;set;
        }

        [DataMember]
        public IEnumerable<CaseDetail> CaseDetails
        {
            get;
            set;
        }
    }
}
