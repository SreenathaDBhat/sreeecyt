﻿using CaseDataAccess.CaseSearch;
using CytoApps.Models;
using System.Collections.Generic;


namespace CaseDataAccess
{
    public interface ICaseDataAccess
    {
        bool Initialize(DataSource dataSource);
        IEnumerable<CaseDetail> GetCaseDetails(Case selectedCase);
        IEnumerable<Slide> GetSlides(Case selectedCase);
        IEnumerable<Case> GetCases(Criteria caseSearch);
        IEnumerable<Filter> GetCaseDetailsFields();
        bool TestConnection(DataSource selectedDataSource);
        bool SaveCaseDetails(string CaseId, IEnumerable<CaseDetail> CaseDetail);
        List<UserDetails> CurrentCaseUsers(Case @case);
        LockResult IsCaseLockedByLegacy(Case @case, UserDetails userdetails);
        void RemoveCaseLocks(Case @case, UserDetails userdetails);
        bool RemoveCrashLocksByUser(string caseName, UserDetails userdetails);

        IEnumerable<string> GetUniqueCaseFlags();
        IEnumerable<string> GetAllCaseFlags();
        bool UpdateCaseFlag(IEnumerable<string> CaseIds, string caseFlag);

        #region TEMPORARY
        IEnumerable<CaseWithCaseDetails> GetCasesForStats(Criteria caseSearch);
        #endregion
    }

    // define MetaData attribute for interface
    public interface ICaseDataAccessMetaData
    {
        string DataSourceType { get; }
    }
}
