﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CaseDataAccess
{
    public class SqlClauseBuilder
    {

        private int n;

        public SqlClauseBuilder()
        {
            n = 0;
        }

        public string And
        {
            get { return (n++) == 0 ? "WHERE " : " AND "; }
        }

        public string Or
        {
            get { return (n++) == 0 ? "WHERE " : " OR "; }
        }
    }
}
