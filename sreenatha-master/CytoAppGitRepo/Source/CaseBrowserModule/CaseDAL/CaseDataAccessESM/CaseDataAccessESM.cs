﻿using CaseDataAccess;
using System;
using System.Collections.Generic;
using CytoApps.Models;
using Core.DataServer.SoapClient;
using CytoApps.Exceptions;
using CaseDataAccess.CaseSearch;

namespace CytoApps.DAL.eSM
{
    /// <summary>
    /// Provides the ICaseDataAccess implementation when the remote casebase is accessed via the CytoApps
    /// eSlideManager dataserver extension
    /// </summary>
    [DalTypeId(Name = "eSM DataServer")]
    public class CaseDataAccessESM : ICaseDataAccess
    {
        private string _baseUrl;
        private string _token;

        public CaseDataAccessESM()
        {
        }

        public CaseDataAccessESM(string baseUrl, string token = null)
        {
            _baseUrl = baseUrl;
            _token = token;
        }

        public bool Initialize(DataSource dataSource)
        {
            var token = dataSource.SessionObject as string;
            if (token == null)
                token = GetToken(dataSource.ConnectionParameter[0].Value);

            _baseUrl = dataSource.ConnectionParameter[0].Value;
            _token = token;

            // store back in dataSource for reuse if we have to create new DAL
            dataSource.SessionObject = token;
            return true; // TODO validate
        }


        public IEnumerable<CaseDetail> GetCaseDetails(Case selectedCase)
        {
            var eSmCytoAppsCases = GetProxy();

            return eSmCytoAppsCases.GetCaseDetails(selectedCase);
        }

        public IEnumerable<Case> GetCases(Criteria caseSearch)
        {
            var eSmCytoAppsCases = GetProxy();

            return eSmCytoAppsCases.GetCases(caseSearch);
        }


        public IEnumerable<Slide> GetSlides(Case selectedCase)
        {
            var eSmCytoAppsCases = GetProxy();

            return eSmCytoAppsCases.GetSlideList(selectedCase.Id);
        }

        public bool TestConnection(DataSource selectedDataSource)
        {
            try
            {
                var proxy = GetProxy();
                return proxy != null && !string.IsNullOrWhiteSpace(proxy.Token);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SaveCaseDetails(string caseId, IEnumerable<CaseDetail> caseDetails)
        {
            var eSmCytoAppsCases = GetProxy();

            return eSmCytoAppsCases.SaveCaseDetails(caseId, caseDetails);
        }

        public IEnumerable<Filter> GetCaseDetailsFields()
        {
            var eSmCytoAppsCases = GetProxy();

            return eSmCytoAppsCases.GetCaseDetailsFields();
        }

        private eSmCytoAppsProxy GetProxy()
        {
            var dsRouter = new DataServerProxyRouterHttp();
            dsRouter.URIHost = _baseUrl;
            dsRouter.URIPort = 80;
            dsRouter.URIPath = "dataserver";

            if (_token == null)
            {
                _token = GetToken(_baseUrl);
            }

            return new eSmCytoAppsProxy(dsRouter) { Token = _token };
        }

        // REFACTOR, in several places. need a eSM core assembly maybe
        public static string GetToken(string baseUrl)
        {
            try
            {
                var dsRouter = new DataServerProxyRouterHttp();
                dsRouter.URIHost = baseUrl;
                dsRouter.URIPort = 80;
                dsRouter.URIPath = "dataserver";

                var coreProxy = new CoreProxy(dsRouter);
                coreProxy.Logon("administrator", "scanscope");

                return coreProxy.Token;
            }
            catch (Exception ex)
            {
                throw new DALException("eSM Web Server error:", ex, DALException.DALErrorType.Authentication);
            }

        }

        public IEnumerable<string> GetUniqueCaseFlags()
        {
            var eSmCytoAppsCases = GetProxy();

            return eSmCytoAppsCases.GetUniqueCaseFlags();
        }

        public IEnumerable<string> GetAllCaseFlags()
        {
            var eSmCytoAppsCases = GetProxy();

            return eSmCytoAppsCases.GetAllCaseFlags();
        }

        public bool UpdateCaseFlag(IEnumerable<string> CaseIds, string caseFlag)
        {
            var eSmCytoAppsCases = GetProxy();

            return eSmCytoAppsCases.UpdateCaseFlag(CaseIds, caseFlag);
        }

        public List<UserDetails> CurrentCaseUsers(Case @case)
        {
            var eSmCytoAppsCases = GetProxy();
            return eSmCytoAppsCases.CurrentCaseUsers(@case);
        }

        public LockResult IsCaseLockedByLegacy(Case @case, UserDetails userdetails)
        {
            var eSmCytoAppsCases = GetProxy();
            return eSmCytoAppsCases.IsCaseLockedByLegacy(@case, userdetails);
        }

        public void RemoveCaseLocks(Case @case, UserDetails userdetails)
        {
            var eSmCytoAppsCases = GetProxy();
            eSmCytoAppsCases.RemoveCaseLocks(@case, userdetails);
        }

        public bool RemoveCrashLocksByUser(string caseName, UserDetails userdetails)
        {
            var eSmCytoAppsCases = GetProxy();
            return eSmCytoAppsCases.RemoveCrashLocksByUser(caseName, userdetails);
        }

        public IEnumerable<CaseWithCaseDetails> GetCasesForStats(Criteria caseSearch)
        {
            throw new NotImplementedException();
        }
    }
}
