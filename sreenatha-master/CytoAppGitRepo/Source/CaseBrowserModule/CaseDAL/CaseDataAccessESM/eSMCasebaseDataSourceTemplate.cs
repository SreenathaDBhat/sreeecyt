﻿using CytoApps.Models;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System;

namespace CytoApps.DAL.eSM
{
    /// <summary>
    /// plugin class that defines connection parameters to access a particular type of datasource
    /// e.g. webservice might have username, password and baseurl
    /// </summary>
    [DalTypeId(Name = "eSM DataServer")]     
    public class eSMCasebaseDataSourceTemplate : IDataSourceTemplate
    {
        public eSMCasebaseDataSourceTemplate()
        {
            var list = new List<ConnectionParameter>();
            list.Add(new ConnectionParameter() { Key = "BaseURL", Value = string.Empty });
            ConnectionParameters = list;
        }

        public IEnumerable<ConnectionParameter> ConnectionParameters
        {
            get; private set;
        }

        public string DataSourceTypeId
        {
            get
            {
                return DalReflectionFinder.GetDalTypeId(GetType());
            }
        }
    }
}
