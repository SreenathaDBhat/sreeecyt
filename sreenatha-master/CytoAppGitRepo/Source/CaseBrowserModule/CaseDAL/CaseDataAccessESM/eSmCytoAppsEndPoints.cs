﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CytoApps.DAL.eSM
{
    /// <summary>
    /// List of dataserver services namespaces !!! note - case sensitive !!!
    /// </summary>
    public static class eSmCytoAppsEndPoint
    {
        public static string CytoAppsProxy = "Aperio/CytoAppsServices";
    };
}
