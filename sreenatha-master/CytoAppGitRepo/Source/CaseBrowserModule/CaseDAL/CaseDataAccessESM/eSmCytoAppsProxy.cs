﻿using Core.DataServer.SoapClient;
using CytoApps.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml;

namespace CytoApps.DAL.eSM
{
    /// <summary>
    /// eSMCytoAppsProxy
    /// Implements the calls to the eSM dataServer via SOAP web request (via base class)
    /// to handle calls to the case/slide/details functionality
    /// </summary>
    public class eSmCytoAppsProxy : DataServerProxy
    {
        protected const string APERIO_NAMESPACE = "http://www.aperio.com/webservices/";
        private XNamespace _nsAperio = APERIO_NAMESPACE;

        private DsResponse _lastResponse;

        public eSmCytoAppsProxy(IDataServerProxyRouter dsProxyRouter) : base(dsProxyRouter)
        {

        }

        public byte[] GetImageData()
        {
            ClearMethodParameters();

            var response = DoCall(eSmCytoAppsEndPoint.CytoAppsProxy, eSmCytoAppsProxyMethodNames.GetImage);

            int retCode = response.GetResult(eSmCytoAppsProxyMethodNames.GetImage).Code;

            byte[] imageDataRet = null;
            if (retCode == 0)
            {
                var imageDataBase64 = response.GetElement("ImageData");
                if (imageDataBase64 != null)
                {
                    imageDataRet = Convert.FromBase64String(imageDataBase64.Value);
                }
            }

            _lastResponse = response;
            return imageDataRet;
        }

        internal IEnumerable<Filter> GetCaseDetailsFields()
        {
            ClearMethodParameters();
            var response = DoCall(eSmCytoAppsEndPoint.CytoAppsProxy, eSmCytoAppsProxyMethodNames.GetCaseDetailsFields);

            int retCode = response.GetResult(eSmCytoAppsProxyMethodNames.GetCaseDetailsFields).Code;

            List<Filter> fieldsRet = null;
            if (retCode == 0)
            {
                var fieldsXml = response.GetElement("ArrayOfFilter").ToString();
                fieldsRet = eSmXmlDeserialize<List<Filter>>(fieldsXml);
            }
            _lastResponse = response;
            return fieldsRet;
        }

        internal IEnumerable<Case> GetCases(Criteria caseSearch)
        {
            ClearMethodParameters();

            // Serialize to XML and encode, doesn't seem a way to send xml without a parser at the other end
            var advancedXml = Serialize<Criteria>(caseSearch);

            AddMethodParameter("Advanced", Convert.ToBase64String(Encoding.Unicode.GetBytes(advancedXml)));

            var response = DoCall(eSmCytoAppsEndPoint.CytoAppsProxy, eSmCytoAppsProxyMethodNames.GetCasesAdvanced);

            int retCode = response.GetResult(eSmCytoAppsProxyMethodNames.GetCasesAdvanced).Code;

            List<Case> casesRet = null;
            if (retCode == 0)
            {
                var casesXml = response.GetElement("ArrayOfCase").ToString();
                casesRet = eSmXmlDeserialize<List<Case>>(casesXml);
            }
            _lastResponse = response;
            return casesRet;
        }

        internal IEnumerable<CaseDetail> GetCaseDetails(Case selectedCase)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", selectedCase.Id);

            var response = DoCall(eSmCytoAppsEndPoint.CytoAppsProxy, eSmCytoAppsProxyMethodNames.GetCaseDetails);

            int retCode = response.GetResult(eSmCytoAppsProxyMethodNames.GetCaseDetails).Code;

            List<CaseDetail> caseDetailsRet = null;
            if (retCode == 0)
            {
                var caseDetailXml = response.GetElement("ArrayOfCaseDetail").ToString();
                caseDetailsRet = eSmXmlDeserialize<List<CaseDetail>>(caseDetailXml);
            }
            _lastResponse = response;
            return caseDetailsRet;
        }

        internal bool SaveCaseDetails(string caseId, IEnumerable<CaseDetail> caseDetails)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", caseId);

            // Serialize to XML
            var detailsXml = Serialize<List<CaseDetail>>(caseDetails.ToList());

            AddMethodParameter("Details", Convert.ToBase64String(Encoding.Unicode.GetBytes(detailsXml)));

            var response = DoCall(eSmCytoAppsEndPoint.CytoAppsProxy, eSmCytoAppsProxyMethodNames.SaveCaseDetails);

            int retCode = response.GetResult(eSmCytoAppsProxyMethodNames.SaveCaseDetails).Code;

            bool ret = false;
            if (retCode == 0)
            {
                var success = response.GetElement("Success");
                if (success != null)
                {
                    ret = (success.Value.Equals("True", StringComparison.InvariantCultureIgnoreCase));
                }
            }
            _lastResponse = response;
            return ret;
        }

        public bool PushImageData(string imageId, byte[] imageData)
        {
            ClearMethodParameters();
            AddMethodParameter("ImageId", imageId);
            AddMethodParameter("ImageData", Convert.ToBase64String(imageData));

            var response = DoCall(eSmCytoAppsEndPoint.CytoAppsProxy, eSmCytoAppsProxyMethodNames.PutImage);

            int retCode = response.GetResult(eSmCytoAppsProxyMethodNames.PutImage).Code;

            bool ret = false;
            if (retCode == 0)
            {
                var success = response.GetElement("Success");
                if (success != null)
                {
                    ret = (success.Value.Equals("true", StringComparison.InvariantCultureIgnoreCase));
                }
            }

            _lastResponse = response;
            return ret;
        }


        public IEnumerable<Slide> GetSlideList(string caseId)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", caseId);

            var response = DoCall(eSmCytoAppsEndPoint.CytoAppsProxy, eSmCytoAppsProxyMethodNames.GetSlideList);

            int retCode = response.GetResult(eSmCytoAppsProxyMethodNames.GetSlideList).Code;

            List<Slide> slideListRet = null;
            if (retCode == 0)
            {
                var slideListXml = response.GetElement("ArrayOfSlide").ToString();
                slideListRet = eSmXmlDeserialize<List<Slide>>(slideListXml);
            }

            _lastResponse = response;
            return slideListRet;
        }

        internal IEnumerable<string> GetUniqueCaseFlags()
        {
            ClearMethodParameters();

            var response = DoCall(eSmCytoAppsEndPoint.CytoAppsProxy, eSmCytoAppsProxyMethodNames.GetUniqueCaseFlags);

            int retCode = response.GetResult(eSmCytoAppsProxyMethodNames.GetUniqueCaseFlags).Code;

            List<string> flagsRet = null;
            if (retCode == 0)
            {
                var flagsXml = response.GetElement("ArrayOfString").ToString();
                flagsRet = eSmXmlDeserialize<List<string>>(flagsXml);
            }
            _lastResponse = response;
            return flagsRet;
        }

        internal IEnumerable<string> GetAllCaseFlags()
        {
            ClearMethodParameters();

            var response = DoCall(eSmCytoAppsEndPoint.CytoAppsProxy, eSmCytoAppsProxyMethodNames.GetAllCaseFlags);

            int retCode = response.GetResult(eSmCytoAppsProxyMethodNames.GetAllCaseFlags).Code;

            List<string> flagsRet = null;
            if (retCode == 0)
            {
                var flagsXml = response.GetElement("ArrayOfString").ToString();
                flagsRet = eSmXmlDeserialize<List<string>>(flagsXml);
            }
            _lastResponse = response;
            return flagsRet;
        }

        internal bool UpdateCaseFlag(IEnumerable<string> CaseIds, string caseFlag)
        {
            ClearMethodParameters();

            // Serialize to XML
            var caseIDsXml = Serialize<List<string>>(CaseIds.ToList());

            AddMethodParameter("CaseIds", Convert.ToBase64String(Encoding.Unicode.GetBytes(caseIDsXml)));

            AddMethodParameter("CaseFlag", caseFlag);

            var response = DoCall(eSmCytoAppsEndPoint.CytoAppsProxy, eSmCytoAppsProxyMethodNames.UpdateCaseFlag);

            int retCode = response.GetResult(eSmCytoAppsProxyMethodNames.UpdateCaseFlag).Code;

            bool ret = false;
            if (retCode == 0)
            {
                var success = response.GetElement("Success");
                if (success != null)
                {
                    ret = (success.Value.Equals("True", StringComparison.InvariantCultureIgnoreCase));
                }
            }
            _lastResponse = response;
            return ret;
        }

        internal List<UserDetails> CurrentCaseUsers(Case @case)
        {
            ClearMethodParameters();
            var caseXML = Serialize<Case>(@case);
            AddMethodParameter("Case", Convert.ToBase64String(Encoding.Unicode.GetBytes(caseXML)));

            var response = DoCall(eSmCytoAppsEndPoint.CytoAppsProxy, eSmCytoAppsProxyMethodNames.CurrentCaseUsers);
            int retCode = response.GetResult(eSmCytoAppsProxyMethodNames.CurrentCaseUsers).Code;
            List<UserDetails> usersRet = new List<UserDetails>();
            if (retCode == 0)
            {
                var usersXml = response.GetElement("ArrayOfUserDetails").ToString();
                usersRet = eSmXmlDeserialize<List<UserDetails>>(usersXml);
            }
            _lastResponse = response;
            return usersRet;
        }

        internal LockResult IsCaseLockedByLegacy(Case @case, UserDetails userdetails)
        {
            ClearMethodParameters();
            var caseXML = Serialize<Case>(@case);
            AddMethodParameter("Case", Convert.ToBase64String(Encoding.Unicode.GetBytes(caseXML)));
            var userDetailsXML = Serialize<UserDetails>(userdetails);
            AddMethodParameter("UserDetails", Convert.ToBase64String(Encoding.Unicode.GetBytes(userDetailsXML)));

            var response = DoCall(eSmCytoAppsEndPoint.CytoAppsProxy, eSmCytoAppsProxyMethodNames.IsCaseLockedByLegacy);
            int retCode = response.GetResult(eSmCytoAppsProxyMethodNames.IsCaseLockedByLegacy).Code;
            LockResult lockResult = null;
            if (retCode == 0)
            {
                var lockResultString = response.GetElement("LockResult").ToString();
                lockResult = eSmXmlDeserialize<LockResult>(lockResultString);
            }
            _lastResponse = response;
            return lockResult;
        }

        internal void RemoveCaseLocks(Case @case, UserDetails userdetails)
        {
            ClearMethodParameters();
            var caseXML = Serialize<Case>(@case);
            AddMethodParameter("Case", Convert.ToBase64String(Encoding.Unicode.GetBytes(caseXML)));
            var userDetailsXML = Serialize<UserDetails>(userdetails);
            AddMethodParameter("UserDetails", Convert.ToBase64String(Encoding.Unicode.GetBytes(userDetailsXML)));

            var response = DoCall(eSmCytoAppsEndPoint.CytoAppsProxy, eSmCytoAppsProxyMethodNames.RemoveCaseLocks);

            int retCode = response.GetResult(eSmCytoAppsProxyMethodNames.RemoveCaseLocks).Code;

            _lastResponse = response;
        }

        internal bool RemoveCrashLocksByUser(string caseName, UserDetails userdetails)
        {
            ClearMethodParameters();
            AddMethodParameter("Case", caseName);
            var userDetailsXML = Serialize<UserDetails>(userdetails);
            AddMethodParameter("UserDetails", Convert.ToBase64String(Encoding.Unicode.GetBytes(userDetailsXML)));

            var response = DoCall(eSmCytoAppsEndPoint.CytoAppsProxy, eSmCytoAppsProxyMethodNames.RemoveCrashLocksByUser);
            int retCode = response.GetResult(eSmCytoAppsProxyMethodNames.RemoveCrashLocksByUser).Code;
            bool ret = false;
            if (retCode == 0)
            {
                var success = response.GetElement("Success");
                if (success != null)
                {
                    ret = (success.Value.Equals("True", StringComparison.InvariantCultureIgnoreCase));
                }
            }
            _lastResponse = response;
            return ret;
        }


        /// <summary>
        /// for debug really
        /// </summary>
        public string LastResponseString
        {
            get
            {
                return _lastResponse != null ? _lastResponse.Response : "No last response";
            }
        }

        /// <summary>
        /// hmmh, similar code in all analyse eSM proxy classes
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toDeserialize"></param>
        /// <returns></returns>
        private static T eSmXmlDeserialize<T>(string toDeserialize)
        {
            // hack out the namespace (added by eSM) from the xml string otherwise no deserialization
            toDeserialize = toDeserialize.Replace("xmlns=\"http://www.aperio.com/webservices/\"", "");

            // deserialize to class of type T
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            StringReader textReader = new StringReader(toDeserialize);
            return (T)xmlSerializer.Deserialize(textReader);
        }

        /// <summary>
        /// Serialize object to XML string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toSerialize"></param>
        /// <returns></returns>
        public static string Serialize<T>(T toSerialize)
        {
            // Reform xml declaration as we want to add it in another xml doc
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            var xmlSettings = new XmlWriterSettings
            {
                Indent = false,
                OmitXmlDeclaration = true
            };

            // Serialize object into xml using settings
            using (StringWriter stringWriter = new StringWriter())
            using (var xmlWriter = XmlWriter.Create(stringWriter, xmlSettings))
            {
                var namespaces = new XmlSerializerNamespaces();
                namespaces.Add(string.Empty, string.Empty);

                xmlSerializer.Serialize(xmlWriter, toSerialize, namespaces);
                return stringWriter.ToString();
            }
        }
    }
}
