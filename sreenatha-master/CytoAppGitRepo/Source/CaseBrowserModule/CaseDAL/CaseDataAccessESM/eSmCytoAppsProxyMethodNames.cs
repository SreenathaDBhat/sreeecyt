﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CytoApps.DAL.eSM
{
    internal static class eSmCytoAppsProxyMethodNames
    {
        internal static string GetImage = "GetImage";
        internal static string PutImage = "PutImage";
        internal static string GetSlideList = "GetSlideList";
        internal static string GetCaseDetails = "GetCaseDetails";
        internal static string GetCaseDetailsFields = "GetCaseDetailsFields";
        internal static string GetCasesAdvanced = "GetCasesAdvanced";
        internal static string SaveCaseDetails = "SaveCaseDetails";
        internal static string GetUniqueCaseFlags = "GetUniqueCaseFlags";
        internal static string GetAllCaseFlags = "GetAllCaseFlags";
        internal static string UpdateCaseFlag = "UpdateCaseFlag";
        internal static string IsCaseLockedByLegacy = "IsCaseLockedByLegacy";
        internal static string RemoveCrashLocksByUser = "RemoveCrashLocksByUser";
        internal static string RemoveCaseLocks = "RemoveCaseLocks";
        internal static string CurrentCaseUsers = "CurrentCaseUsers";
    }
}
