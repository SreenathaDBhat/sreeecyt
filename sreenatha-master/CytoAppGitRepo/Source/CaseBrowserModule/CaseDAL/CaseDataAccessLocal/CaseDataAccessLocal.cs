﻿using CaseDataAccess;
using CaseDataAccess.CaseSearch;
using CytoApps.EntityModel;
using CytoApps.Exceptions;
using CytoApps.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

/// <summary>
/// Contains functionality to access case, slide details from the local data source connection.
/// method to create new database connection bt passing connection string
/// </summary>
namespace CaseDataAccessLocal
{
    [DalTypeId(Name = "Local Casebase")]
    public class CaseDataAccessLocal : ICaseDataAccess
    {
        #region Private members
        private Chromoscan2Entities _db;
        private IntializeCytoAppsEntityModel _entityModel;
        private string _casePath;
        #endregion

        #region Constructor
        public CaseDataAccessLocal()
        {
        }

        public CaseDataAccessLocal(string dataSourceConnectionString, string casePath)
        {
            _casePath = casePath;
            _entityModel = new IntializeCytoAppsEntityModel();
            _db = _entityModel.GetCurrentDataBase(dataSourceConnectionString);
        }
        #endregion

        public bool Initialize(DataSource dataSource)
        {
            _casePath = dataSource.ConnectionParameter[1].Value;
            _entityModel = new IntializeCytoAppsEntityModel();
            _db = _entityModel.GetCurrentDataBase(dataSource.ConnectionParameter[0].Value);

            return true; //TODO, validate
        }


        #region private methods
        // REFACTOR, shouldn't throw an exception and shouldn't be doing a string look up 
        private bool TestCasePathConnection(string casePath)
        {
            try
            {
                return (Directory.Exists(casePath));
            }
            catch (Exception)
            {
                return false;
            }
        }

        // REFACTOR, lets keep dependency low, when user calls Test, expects true or false, not an exception
        private bool TestDatabaseConnection(string connectionString)
        {
            return _entityModel.TestDatabaseConnection(connectionString);
        }

        private string GetSlideRealName(string slidefolder)
        {
            string slidename;
            try
            {
                var slideDir = new DirectoryInfo(slidefolder);
                slidename = slideDir.Name;
                FileInfo nameFile = new FileInfo(Path.Combine(slidefolder, ".name"));
                if (nameFile.Exists)
                {
                    using (var reader = new StreamReader(nameFile.FullName, Encoding.Default))
                        slidename = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw new DALException("Cannot get the Slide name", ex, DALException.DALErrorType.Authorization);
            }
            return slidename;
        }
        #endregion

        #region public methods
        private IEnumerable<Case> GetCases()
        {
            List<Case> cases = new List<Case>();
            try
            {
                foreach (var casedata in _db.cases.ToList())
                {
                    Case caseModel = new Case(casedata.name, casedata.name)
                    {
                        Flag = casedata.flag,
                        Created = casedata.created,
                        Keywords = casedata.keywords,
                        LastAccessed = casedata.lastAccessed
                    };

                    cases.Add(caseModel);
                }
            }
            catch (Exception ex)
            {
                throw new DALException("Cannot retrieve Cases", ex, DALException.DALErrorType.DBConnection);
            }
            return cases;
        }

        public IEnumerable<CaseDetail> GetCaseDetails(Case selectedCase)
        {
            List<CaseDetail> caseDetails;
            try
            {
                caseDetails = new List<CaseDetail>();
                var caseEntity = _db.cases.Where(c => c.name == selectedCase.Name).FirstOrDefault();
                var casedetails = _db.caseDetails.Where(cd => cd.caseId == caseEntity.caseId);
                foreach (var casedetail in casedetails)
                {
                    CaseDetail caseDetail = new CaseDetail
                    {
                        FieldName = casedetail.fieldName,
                        Data = casedetail.data,
                        Type = casedetail.type,
                        Editable = casedetail.editable,
                        Mandatory = casedetail.mandatory
                    };
                    caseDetails.Add(caseDetail);
                }
            }
            catch (Exception ex)
            {
                throw new DALException("Cannot retrieve Case details for case " + selectedCase.Name, ex, DALException.DALErrorType.DBConnection);
            }
            return caseDetails;
        }

        public bool SaveCaseDetails(string CaseId, IEnumerable<CaseDetail> CaseDetails)
        {
            bool success = false;
            try
            {
                var caseEntity = _db.cases.Where(c => c.name == CaseId).FirstOrDefault();
                if (caseEntity == null)
                    throw new Exception("Can't find case " + CaseId);

                var dbcasedetails = _db.caseDetails.Where(cd => cd.caseId == caseEntity.caseId);
                foreach (var casedetail in dbcasedetails)
                {
                    var cdetails = CaseDetails.Where(c => c.FieldName == casedetail.fieldName).FirstOrDefault();
                    if (cdetails == null)
                        throw new Exception("Can't find field " + casedetail.fieldName);

                    if (cdetails.Type == 2)
                    {
                        cdetails.Data = DateTimeStringFromString(cdetails.Data);
                    }
                    casedetail.data = cdetails.Data;
                }
                _db.SaveChanges();
                success = true;
            }
            catch (Exception ex)
            {
                throw new DALException("Cannot save case details for case " + CaseId, ex, DALException.DALErrorType.DBConnection);
            }
            return success;

        }


        /// <summary>
        /// Get cases that matches thhe criteria
        /// </summary>
        /// <param name="criteria">Search criteria based on which search should be done.If there is no criteria then pass null</param>
        /// <returns></returns>
        public IEnumerable<Case> GetCases(Criteria Searchcriteria)
        {
            // if there is no criteria for search then just retrun all cases from the connected datasources
            if(Searchcriteria == null)
            {
                return GetCases();
            }
            List<Case> cases = new List<Case>();
            try
            {
                string commandText = GenerateSQL(Searchcriteria);

                var dt = _entityModel.ExecuteComplexSqlCommands(commandText);
                foreach (DataRow dr in dt.Rows)
                {
                    Case caseModel = new Case(dr[1].ToString(), dr[1].ToString())
                    {
                        Flag = Convert.ToString(dr[2]),
                        Created = Convert.ToDateTime(dr[3]),
                        Keywords = Convert.ToString(dr[5]),
                        LastAccessed = Convert.ToDateTime(dr[4])
                    };
                    cases.Add(caseModel);
                }
            }
            catch (Exception ex)
            {
                throw new DALException("Cannot retrieve Cases with Advanced search", ex, DALException.DALErrorType.DBConnection);
            }
            return cases;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="caseFilters">Search criteria containing filters. Null if we do not have any criteria and search for all cases</param>
        /// <returns></returns>
        public string GenerateSQL(Criteria caseFilters)
        {
            string commandText = "";
            //Query for flags

            var FlagFilters = caseFilters.Filters.Where(x => x.FieldName == "Flag");
            var Flags = FlagFilters.Select(x => x.Value).ToList();
            
            //Remove the flag filters for further search
            var caseFiltersLocal = new Criteria() { Filters = caseFilters.Filters.Where(x => x.FieldName != "Flag").ToList()};

            string flagsql = "";
            if (Flags != null && Flags.Count > 0)
            {
                flagsql = " table1.flag in ( ";
                for (int i = 0; i < Flags.Count(); i++)
                {
                    if (i != 0)
                        flagsql += ",";
                    flagsql += "N'" + Flags[i] + "'";
                }
                flagsql += ") ";
            }
            SqlClauseBuilder clauseBuilder;
            var additionalFilters = new List<Filter>();
            additionalFilters = caseFiltersLocal.Filters.Where(x => x.IsCaseTableField == false && !string.IsNullOrEmpty(x.Value)).ToList();

            if (additionalFilters != null)
            {
                if (additionalFilters.Count() == 0)
                {
                    commandText = "select table1.CaseId, table1.Name, table1.Flag, table1.Created, table1.LastAccessed, table1.Keywords from (select Name, CaseId, Flag, Keywords, LastAccessed, Created from Cases) as Table1 ";
                }
                else
                {
                    commandText = "select table1.CaseId, table1.Name, table1.Flag, table1.Created, table1.LastAccessed, table1.keywords " +
                                         "from ( select Name, CaseId, Flag, Keywords, LastAccessed, Created from Cases ) " +
                                         "as Table1 inner join (select Count(caseId) as MetaDataCount, caseId from CaseDetails ";
                    //Query for Complex Search 
                    clauseBuilder = new SqlClauseBuilder();
                    foreach (Filter caseFilter in additionalFilters)
                    {
                        commandText += string.Format(" {0} {1}", clauseBuilder.Or, ComplexQuerySection(caseFilter));
                    }
                    commandText += " group by caseId having Count(caseId) = " + additionalFilters.Count().ToString() + " ) as table2 on table1.caseid = table2.caseid ";
                }
            }
            //Query for simple case search            
            clauseBuilder = new SqlClauseBuilder();
            var basicFilters = new List<Filter>();
            basicFilters = caseFiltersLocal.Filters.Where(x => x.IsCaseTableField == true).ToList();
            if (basicFilters != null && basicFilters.Count > 0)
            {
                foreach (Filter caseFilter in basicFilters)
                {
                    if (caseFilter.FieldType == FieldTypeEnum.Text)
                    {
                        if (Regex.IsMatch(caseFilter.Value, "'"))
                        {
                            caseFilter.Value = caseFilter.Value.Replace("'", "''");
                        }
                        else if (@Regex.IsMatch(caseFilter.Value, @"^(?!\s*$).+")) //will match any string that contains at least one non - space character.
                        {
 
                            var value = caseFilter.Value.Replace("_", "[_]");  // underscore needs escaping or []
                            commandText += string.Format(" {0} {1}", clauseBuilder.And, "table1.Name like N'%" + value + "%' ");
                        }
                        else
                        {
                            // Empty value no command specifics needed. full text match so do not alter the commandtext
                        }
                    }
                    else if (caseFilter.FieldType == FieldTypeEnum.Date && !string.IsNullOrEmpty(caseFilter.Value))
                    {
                        commandText += clauseBuilder.And + string.Format(" CAST (Table1.LastAccessed AS DATE) {1} CAST ('{0}' AS DATE) ", DateTimeStringFromString(caseFilter.Value), caseFilter.Operator);

                    }
                    else if (caseFilter.FieldType == FieldTypeEnum.MultiLineText)
                    {
                        commandText += clauseBuilder.And + string.Format(" CAST(table1.Keywords as NVARCHAR(100)) LIKE N'%{0}%'", caseFilter.Value);
                    }
                }
            }
            if (flagsql != string.Empty)
                commandText += string.Format("{0} {1}", clauseBuilder.And, flagsql);

            return commandText;
        }

        public string ComplexQuerySection(Filter caseFilter)
        {
            if (caseFilter.Value == null || caseFilter.Value.Length == 0)
            {
                return null;
            }
            string id = GetHashCode().ToString(CultureInfo.InvariantCulture);

            if (caseFilter.FieldType == FieldTypeEnum.Number)
            {
                var str = string.Format(" (fieldName = N'{0}' and type = 3 and ISNUMERIC(CAST(data as NVARCHAR(50))) = 1 and {1}) ", caseFilter.FieldName, ComparisonPartString(caseFilter));
                return str;
            }
            else if (caseFilter.FieldType == FieldTypeEnum.Date)
            {
                var str = string.Format(" (fieldName = N'{0}' and type = 2 and ISDATE(CAST(data as NVARCHAR(50))) = 1 and {1}) ", caseFilter.FieldName, ComparisonPartString(caseFilter));
                return str;
            }
            else
            {
                if (Regex.IsMatch(caseFilter.Value, "'"))
                    caseFilter.Value = caseFilter.Value.Replace("'", "''");
                string valueParam = "%\\" + caseFilter.Value + "%";
                return string.Format("(fieldName = N'{0}' and substring(data,0,100) like '{1}' escape '\\') ", caseFilter.FieldName, valueParam);
            }
        }

        public string ComparisonPartString(Filter caseFilter)
        {
            string id = GetHashCode().ToString(CultureInfo.InvariantCulture);
            string valueParam = "%" + caseFilter.Value + "%";

            if (caseFilter.FieldType == FieldTypeEnum.Number)
            {
                var str = string.Format("CAST(substring(data,0,50) AS NUMERIC) {0} {1}", caseFilter.Operator, caseFilter.Value);
                return str;
            }
            else if (caseFilter.FieldType == FieldTypeEnum.Date)
            {
                var utc = DateTimeStringFromString(caseFilter.Value);
                var str = string.Format("CAST(substring(data,0,50) AS DATE) {0} CAST('{1}' AS DATE)", caseFilter.Operator, utc);
                return str;
            }
            else
            {
                return string.Format("substring(data,0,100) like N'{1}'", caseFilter.FieldName, valueParam);
            }
        }

        // This method tries to parse in different formats since the remote servers can be in any format.

        public static string DateTimeStringFromString(string userstring)
        {
            DateTime userdate;
            if (DateTime.TryParseExact(userstring,
                    "dd-MM-yyyy",
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None,
                    out userdate))
            {
                var utc = DateTime.SpecifyKind(userdate, DateTimeKind.Utc);
                return utc.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
            }
            else if (DateTime.TryParseExact(userstring,
                       "MM-dd-yyyy",
                        CultureInfo.InvariantCulture,
                        DateTimeStyles.None,
                       out userdate))
            {
                var utc = DateTime.SpecifyKind(userdate, DateTimeKind.Utc);
                return utc.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
            }
            else if (DateTime.TryParseExact(userstring,
                       "yyyy-dd-MM",
                        CultureInfo.InvariantCulture,
                        DateTimeStyles.None,
                       out userdate))
            {
                var utc = DateTime.SpecifyKind(userdate, DateTimeKind.Utc);
                return utc.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
            }

            else if (DateTime.TryParse(userstring, out userdate))
            {
                var utc = DateTime.SpecifyKind(userdate, DateTimeKind.Utc);
                return utc.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
            }

            else
            {
                return "NA";
            }
        }

        public IEnumerable<Slide> GetSlides(Case selectedCase)
        {
            string casePath = Path.Combine(_casePath, selectedCase.Name);
            List<Slide> slides = null;
            try
            {
                if (Directory.Exists(casePath))
                {
                    DirectoryInfo casePathInfo = new DirectoryInfo(casePath);

                    DirectoryInfo[] slideFolders = (casePathInfo.GetDirectories()
                                                    .Where(d => d.Name.StartsWith("slide", StringComparison.CurrentCultureIgnoreCase))).ToArray();
                    slides = new List<Slide>();
                    foreach (DirectoryInfo slideFolderInfo in slideFolders)
                    {
                        var slidename = GetSlideRealName(slideFolderInfo.FullName);

                        Slide slide = new Slide(selectedCase.Id, slidename, slideFolderInfo.Name);

                        // lets add in some info now about what we can tell about the slide to speed things up;
                        // Analysis plugin can use this or call its CanAnalyse DAL method (optimization for RemoteAccess)
                        slide.KnownSlideTypes = (UInt32)GetKnownSlideTypes(slideFolderInfo);
                        slides.Add(slide);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new DALException("Cannot enumerate slide folder", ex, DALException.DALErrorType.Authorization);
            }

            return slides;
        }

        private KnownSlides GetKnownSlideTypes(DirectoryInfo slideFolder)
        {
            KnownSlides slideTypes = KnownSlides.UnknownSlide;

            if (slideFolder.EnumerateFiles("*.scn").FirstOrDefault() != null)
                slideTypes = slideTypes | KnownSlides.SCNSlide;
            else if (slideFolder.EnumerateFiles("*.imageframes").FirstOrDefault() != null)
                slideTypes = slideTypes | KnownSlides.ProbeCaseViewSlide;
            else if (slideFolder.EnumerateDirectories("cell*").FirstOrDefault() != null)
                slideTypes = slideTypes | KnownSlides.KaryotypeSlide;
            return slideTypes;
        }

        ///<summary>
        ///Get the Unique Case detail fields
        /// </summary>
        public IEnumerable<Filter> GetCaseDetailsFields()
        {
            IEnumerable<Filter> casedetailsfields;
            try
            {
                casedetailsfields = _db.caseDetails.Select(c => new Filter { FieldName = c.fieldName, FieldType = (FieldTypeEnum)c.type }).Distinct();
            }
            catch (Exception ex)
            {
                throw new DALException("Cannot retrieve case details fields for Advanced search", ex, DALException.DALErrorType.DBConnection);
            }
            return casedetailsfields;
        }

        public bool TestConnection(DataSource selectedDataSource)
        {
            return (TestDatabaseConnection(selectedDataSource.ConnectionParameter[0].Value) && TestCasePathConnection(selectedDataSource.ConnectionParameter[1].Value));
        }


        /// <summary>
        /// Returns Unique Flags from Cases table. 
        /// this method is mainly used in advance search to display search criteria.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetUniqueCaseFlags()
        {
            List<string> flags = new List<string>();
            flags = _db.cases.Select(x => x.flag).Distinct().ToList();
            return flags;
        }

        /// <summary>
        /// returns all case flags( case status )
        /// operations like change case status requires all the case status.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetAllCaseFlags()
        {
            //TODO- SB need to change  implementation.
            List<string> flags = new List<string>();
            flags = _db.cases.Select(x => x.flag).Distinct().ToList();
            if (!flags.Contains("ForReview"))
            {
                flags.Add("ForReview");
            }
            if (!flags.Contains("InProgress"))
            {
                flags.Add("InProgress");
            }
            if (!flags.Contains("Completed"))
            {
                flags.Add("Completed");
            }
            return flags;
        }

        /// <summary>
        /// Change and save the case status flag.
        /// </summary>
        /// <param name="CaseIds">list of cases to be updated</param>
        /// <param name="caseFlag">new case status</param>
        /// <returns></returns>
        public bool UpdateCaseFlag(IEnumerable<string> CaseIds, string CaseFlag)
        {
            bool success = false;
            try
            {
                foreach (var caseId in CaseIds)
                {
                    var caseEntity = _db.cases.Where(c => c.name == caseId).FirstOrDefault();
                    if (caseEntity == null)
                    {
                        throw new Exception("Can't find case " + caseId);
                    }

                    caseEntity.flag = CaseFlag;
                    _db.SaveChanges();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                throw new DALException("Update case status failed", ex, DALException.DALErrorType.DBConnection);
            }
            return success;
        }

        /// <summary>
        /// Gets List of current users of the case
        /// </summary>
        /// <param name="case">selected case</param>
        /// <returns>list of users</returns>
        public List<UserDetails> CurrentCaseUsers(Case @case)
        {
            List<UserDetails> users = new List<UserDetails>();
            try
            {
                var caseEntity = _db.cases.Where(c => c.name == @case.Name).FirstOrDefault();
                var caseUsers = _db.caseGenericLocks.Where(c => c.CaseID == caseEntity.caseId).Select(u => u.UserID).Distinct().ToList();
                if (caseUsers != null)
                {
                    foreach (var user in caseUsers)
                    {
                        var userObj = _db.userDetails.Where(u => u.UserID == user).FirstOrDefault();
                        UserDetails _user = new UserDetails()
                        {
                            EmailId = userObj.EmailID,
                            FirstName = userObj.FirstName,
                            IsActive = userObj.IsActive,
                            LastAccessed = userObj.LastAccessed,
                            LastName = userObj.LastName,
                            MachineName = userObj.MachineName,
                            UserID = userObj.UserID,
                            Name = userObj.FirstName + " " + userObj.LastName
                        };
                        users.Add(_user);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new DALException("Get current case users failed", ex, DALException.DALErrorType.DBConnection);
            }
            return users;
        }

        /// <summary>
        /// Checks wheather case is exclusively locked if yes returns the users details
        /// </summary>
        /// <param name="case"></param>
        /// <param name="userdetails"></param>
        /// <returns></returns>
        public LockResult IsCaseLockedByLegacy(Case @case, UserDetails userdetails)
        {
            string casePath = Path.Combine(_casePath, @case.Name);
            LockResult result = new LockResult();
            try
            {
                if (Directory.Exists(casePath))
                {
                    if (!File.Exists(Path.Combine(casePath, Constants.LegacyLock)))
                    {
                        AddLegacyLockFile(casePath, userdetails);
                        AddMultiAccess(casePath, userdetails);
                    }
                    else if (!File.Exists(Path.Combine(casePath, Constants.AllowMultiAccess)))
                    {
                        string[] userInfo = ReadLegacyLock((Path.Combine(casePath, Constants.LegacyLock)));
                        UserDetails userDetails = new UserDetails()
                        {
                            Name = userInfo[0],
                            MachineName = userInfo[1]
                        };
                        result.User = userDetails;
                        result.IsLocked = true;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new DALException("Case not found exception", ex, DALException.DALErrorType.Authorization);
            }
        }

        /// <summary>
        /// Returns array of words
        /// </summary>
        /// <param name="legacyLock"> legacy lock path</param>
        /// <returns>array of strings</returns>
        private string[] ReadLegacyLock(string legacyLock)
        {
            string[] newStrArr = new string[10];
            if (legacyLock != null)
            {
                newStrArr = File.ReadAllText(legacyLock).Split(' ');
            }
            return newStrArr;
        }

        /// <summary>
        /// adds exclusive lock for the case
        /// </summary>
        /// <param name="casePath">case</param>
        /// <param name="userdetails">login - user</param>
        private void AddLegacyLockFile(string casePath, UserDetails userdetails)
        {
            string legacyLockPath = System.IO.Path.Combine(casePath, Constants.LegacyLock);
            if (Directory.Exists(casePath))
            {
                if (!File.Exists(legacyLockPath))
                {
                    using (StreamWriter sw = File.CreateText(legacyLockPath))
                    {
                        sw.WriteLine(("{0} {1} {2} {3:H:mm:ss MM/dd/yy } \n"), userdetails.UserID, userdetails.MachineName, "MODIFY", DateTime.Now);
                    }
                }
            }
        }

        /// <summary>
        /// adds allowmultiaccess.lock file to case folder to specify its locked by cyto apps
        /// </summary>
        /// <param name="casePath">selected case</param>
        /// <param name="userDetails">login - user</param>
        private void AddMultiAccess(string casePath, UserDetails userDetails)
        {
            string multiaccess = System.IO.Path.Combine(casePath, Constants.AllowMultiAccess);
            if (Directory.Exists(casePath))
            {
                if (File.Exists(multiaccess))
                {
                    using (StreamWriter writer =
                        new StreamWriter(multiaccess, true))
                    {
                        writer.WriteLine(userDetails.MachineName);
                    }
                }
                else
                {
                    using (StreamWriter sw = File.CreateText(multiaccess))
                    {
                        sw.WriteLine(userDetails.MachineName);
                    }
                }
            }
        }

        /// <summary>
        /// Removes machine name entry in allowmultiaccess.lock file on navigation or application close
        /// </summary>
        /// <param name="casePath">selected case</param>
        /// <param name="userDetails">login - user</param>
        private void RemoveMultiAccess(string casePath, UserDetails userDetails)
        {
            string multiaccess = System.IO.Path.Combine(casePath, Constants.AllowMultiAccess);
            if (Directory.Exists(casePath))
            {
                if (File.Exists(multiaccess))
                {
                    File.WriteAllLines(multiaccess, File.ReadLines(multiaccess).Where(machine => machine != userDetails.MachineName).ToList());
                    if (File.ReadAllLines(multiaccess).Length == 0)
                    {
                        File.Delete(multiaccess);
                    }
                }
            }
        }

        /// <summary>
        /// Removes exclusive lock and allowmultiaccess lock
        /// </summary>
        /// <param name="casePath">case path</param>
        private void ResetCaseLocks(string casePath)
        {
            string multiaccess = System.IO.Path.Combine(casePath, Constants.AllowMultiAccess);
            string legacyLockPath = System.IO.Path.Combine(casePath, Constants.LegacyLock);

            if (Directory.Exists(casePath))
            {
                if (File.Exists(multiaccess))
                {
                    File.Delete(multiaccess);
                }

                if (File.Exists(legacyLockPath))
                {
                    File.Delete(legacyLockPath);
                }
            }
        }

        /// <summary>
        /// Removes exclusive lock
        /// </summary>
        /// <param name="casePath">selected case</param>
        /// <param name="userDetails">login user</param>
        private void RemoveLegacyLock(string casePath, UserDetails userDetails)
        {
            if (Directory.Exists(casePath))
            {
                string legacyLockPath = System.IO.Path.Combine(casePath, Constants.LegacyLock);
                if (File.Exists(legacyLockPath))
                {
                    File.Delete(legacyLockPath);
                }
            }
        }

        /// <summary>
        /// Removes exclusive lock and allowmultiaccess lock
        /// </summary>
        /// <param name="case">case path</param>
        /// <param name="userdetails">logged in user</param>
        public void RemoveCaseLocks(Case @case, UserDetails userdetails)
        {
            string casePath = Path.Combine(_casePath, @case.Name);
            try
            {
                if (Directory.Exists(casePath))
                {
                    RemoveMultiAccess(casePath, userdetails);
                    RemoveLegacyLock(casePath, userdetails);
                }
            }
            catch (Exception ex)
            {
                throw new DALException("Case not found exception", ex, DALException.DALErrorType.Authorization);
            }
        }

        /// <summary>
        /// Clears all locks by logged in user name
        /// </summary>
        /// <param name="userdetails">logged in user</param>
        /// <returns></returns>
        public bool RemoveCrashLocksByUser(string caseName, UserDetails userdetails)
        {
            bool isCrashLocksCleared = false;
            try
            {
                var caseLockObj = _db.cases.Where(c => c.name == caseName).FirstOrDefault();
                if (caseLockObj != null)
                {
                    string casePath = Path.Combine(_casePath, caseLockObj.name);
                    ResetCaseLocks(casePath);
                    var caseSpecificLocks = _db.caseGenericLocks.Where(x => x.UserID == userdetails.UserID && x.CaseID == caseLockObj.caseId).ToList();
                    if (caseSpecificLocks != null)
                    {
                        foreach (var caseLock in caseSpecificLocks)
                        {
                            var genricLockEntity = _db.genericObjectLocks.Where(l => l.LockId == caseLock.LockID).FirstOrDefault();
                            if (genricLockEntity != null)
                            {
                                _db.genericObjectLocks.Remove(genricLockEntity);
                                _db.SaveChanges();
                            }
                            _db.caseGenericLocks.Remove(caseLock);
                            _db.SaveChanges();
                        }
                    }
                    isCrashLocksCleared = true;
                }
            }
            catch (Exception ex)
            {
                throw new DALException("Cannot clear crash locks", ex, DALException.DALErrorType.Authorization);
            }
            return isCrashLocksCleared;
        }


        #region TEMPORARY 
        public IEnumerable<CaseWithCaseDetails> GetCasesForStats(Criteria caseSearch)
        {
            List<CaseWithCaseDetails> cases = new List<CaseWithCaseDetails>();
            try
            {
                string commandText = GenerateSQL(caseSearch);
                
                var dt = _entityModel.ExecuteComplexSqlCommands(commandText);
                foreach (DataRow dr in dt.Rows)
                {
                    Case caseModel = new Case(dr[1].ToString(), dr[1].ToString())
                    {
                        Flag = Convert.ToString(dr[2]),
                        Created = Convert.ToDateTime(dr[3]),
                        Keywords = Convert.ToString(dr[5]),
                        LastAccessed = Convert.ToDateTime(dr[4])
                    };
                    var details = GetCaseDetails(caseModel);

                    cases.Add(new CaseWithCaseDetails() { Case = caseModel, CaseDetails = details });
                }
            }
            catch (Exception ex)
            {
                throw new DALException("Cannot retrieve Cases with Advanced search", ex, DALException.DALErrorType.DBConnection);
            }
            return cases;
        }
        #endregion
    }
    #endregion
}

