﻿using CytoApps.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;

namespace CaseDataAccessLocal
{
    /// <summary>
    /// plugin class that defines connection parameters to access a particular type of datasource
    /// e.g. casebase might have connection string for sqlserver and uncpath for casebase
    /// </summary>
    [DalTypeId(Name = "Local Casebase")]
    public class LocalCasebaseDataSourceTemplate : IDataSourceTemplate
    {
        public LocalCasebaseDataSourceTemplate()
        {
            var list = new List<ConnectionParameter>();
            list.Add(new ConnectionParameter() { Key = "ConnectionString", Value = string.Empty });
            list.Add(new ConnectionParameter() { Key = "CasePath", Value = string.Empty });
            ConnectionParameters = list;
        }

        public IEnumerable<ConnectionParameter> ConnectionParameters 
        {
            get; private set;
        }

        public string DataSourceTypeId
        {
            get
            {
                return DalReflectionFinder.GetDalTypeId(GetType());
            }
        }
    }
}
