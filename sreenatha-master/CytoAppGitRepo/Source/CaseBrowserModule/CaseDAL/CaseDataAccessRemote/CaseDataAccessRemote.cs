﻿using CytoApps.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Linq;
using CaseDataAccess;
using CaseDataAccess.CaseSearch;

namespace CaseDataAccessRemote
{
    public class CaseDataAccessRemote : ICaseDataAccess
    {
        private string _baseUrl;

        public CaseDataAccessRemote()
        {
        }
        public CaseDataAccessRemote(string baseUrl)
        {
            _baseUrl = baseUrl;
        }

        public IEnumerable<Case> GetCases()
        {
            using (var client = ConfigureClient())
            {
                HttpResponseMessage response = client.GetAsync("api/Case/Cases").Result;
                if (response.IsSuccessStatusCode)
                {
                    var cases = response.Content.ReadAsAsync<IEnumerable<Case>>().Result;
                    return cases;
                }
            }
            return null;
        }

        //TODO: Need to rectify it for advanced search
        public IEnumerable<Case> GetCases(string caseName)
        {

            using (var client = ConfigureClient())
            {
                HttpResponseMessage response = client.GetAsync("api/Case/Cases").Result;
                if (response.IsSuccessStatusCode)
                {
                    if (!string.IsNullOrWhiteSpace(caseName))
                    {
                        var cases = response.Content.ReadAsAsync<IEnumerable<Case>>().Result.Where(x => x.Name.Contains(caseName)).ToList();
                        return cases;
                    }
                    else
                    {
                        var cases = response.Content.ReadAsAsync<IEnumerable<Case>>().Result;
                        return cases;
                    }
                }
            }
            return null;
        }

        public IEnumerable<CaseDetail> GetCaseDetails(Case selectedCase)
        {
            using (var client = ConfigureClient())
            {
                HttpResponseMessage response = client.GetAsync("api/Case/Cases/" + selectedCase.Id + "/CaseDetails").Result;
                if (response.IsSuccessStatusCode)
                {
                    var details = response.Content.ReadAsAsync<IEnumerable<CaseDetail>>().Result;
                    return details;
                }
            }
            return null;
        }

        public IEnumerable<Slide> GetSlides(Case selectedCase)
        {
            using (var client = ConfigureClient())
            {
                // Note: trailing '/'  
                HttpResponseMessage response = client.GetAsync("api/Case/Cases/" + selectedCase.Id + "/Slides").Result;
                if (response.IsSuccessStatusCode)
                {
                    var slides = response.Content.ReadAsAsync<IEnumerable<Slide>>().Result;
                    return slides;
                }
            }
            return null;

        }

        private HttpClient ConfigureClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(_baseUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
            //                             BearerToken.AccessToken);

            return client;
        }

        public IEnumerable<Case> GetCases(Criteria caseSearch)
        {
            throw new NotImplementedException();
        }
        //TODO
        public IEnumerable<Filter> GetCaseDetailsFields()
        {
            return null;
            ;
        }

        public bool TestConnection(DataSource selectedDataSource)
        {
            return false;
        }

        //TODO
        public bool SaveCaseDetails(string CaseId, IEnumerable<CaseDetail> CaseDetail)
        {
            return true;
        }

        public bool Initialize(DataSource dataSource)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetUniqueCaseFlags()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetAllCaseFlags()
        {
            throw new NotImplementedException();
        }

        public bool UpdateCaseFlag(IEnumerable<string> CaseIds, string caseFlag)
        {
            throw new NotImplementedException();
        }

        public List<UserDetails> CurrentCaseUsers(Case @case)
        {
            throw new NotImplementedException();
        }

        public LockResult IsCaseLockedByLegacy(Case @case, UserDetails userdetails)
        {
            throw new NotImplementedException();
        }

        public void RemoveCaseLocks(Case @case, UserDetails userdetails)
        {
            throw new NotImplementedException();
        }

        public bool RemoveCrashLocksByUser(string caseName, UserDetails userdetails)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CaseWithCaseDetails> GetCasesForStats(Criteria caseSearch)
        {
            throw new NotImplementedException();
        }
    }
}
