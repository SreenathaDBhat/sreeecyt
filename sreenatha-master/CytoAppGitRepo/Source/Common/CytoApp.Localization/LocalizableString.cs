﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CytoApps.Localization
{
    public class Literal
    {

        private static StringTable _strings;
        public static StringTable Strings
        {
            get
            {
                if (_strings == null)
                    _strings = new StringTable();
                return _strings;
                ;
            }
        }
    }

    public class LocalizableString
    {
        private string pluralizedAlternative;
        public string Content { get; set; }
        public string Plural
        {
            get
            {
                if (pluralizedAlternative == null)
                    return Content;

                return pluralizedAlternative;
            }
            set
            {
                pluralizedAlternative = value;
            }
        }
    }

    public class StringTable
    {
        private Dictionary<string, LocalizableString> table;

        public StringTable()
        {
            ResourceDictionary resources = new ResourceDictionary();
            var applicationResource = Application.Current.Resources;
            table = new Dictionary<string, LocalizableString>();
            foreach (DictionaryEntry r in applicationResource)
            {
                if (r.Value == null && !(r.Value is LocalizableString))
                    continue;

                table.Add((string)r.Key, (LocalizableString)r.Value);
            }

            // any merged dictionaries
            foreach (var d in applicationResource.MergedDictionaries)
            {
                if (d.MergedDictionaries.Count > 0)

                    foreach (var dictionary in d.MergedDictionaries)
                    {
                        if (dictionary.Source == new Uri("pack://application:,,,/CytoApps.Localization;component/Resources/Strings.xaml"))
                        {
                            if (dictionary.MergedDictionaries.Count > 0)
                            {
                                foreach (var mdictionary in dictionary.MergedDictionaries)
                                {
                                    foreach (DictionaryEntry r in mdictionary)
                                    {
                                        if (r.Value == null && !(r.Value is LocalizableString))
                                            continue;
                                        table.Add((string)r.Key, (LocalizableString)r.Value);
                                    }
                                }
                            }

                        }
                    }
            }
        }

        private StringTable(int x)
        {
            table = new Dictionary<string, LocalizableString>();
        }

        public static StringTable Empty
        {
            get { return new StringTable(1); }
        }

        public string Lookup(string key)
        {
            if (table.ContainsKey(key))
                return table[key].Content;

            return string.Empty;
        }

        public string LookupPlural(string key)
        {
            if (table.ContainsKey(key))
                return table[key].Plural;

            return string.Empty;
        }

        public string Lookup(string key, int pluralTest)
        {
            if (pluralTest == 1)
                return Lookup(key);
            else
                return LookupPlural(key);

        }
    }
}
