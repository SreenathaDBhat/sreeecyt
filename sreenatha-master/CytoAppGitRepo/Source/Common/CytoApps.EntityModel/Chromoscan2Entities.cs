﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace CytoApps.EntityModel
{
    /// <summary>
    /// This is a partial class of auto-generated  Chromoscan2Entities class.
    /// This class using the base class DbContext property of taking connection string as a parameter
    /// </summary>
    public partial class Chromoscan2Entities : DbContext
    {
        public Chromoscan2Entities(string connectionString)
            : base(connectionString)
        {

        }
    }
}
