﻿using System;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;

namespace CytoApps.EntityModel
{
    public class IntializeCytoAppsEntityModel
    {
        #region Private members
        private const string _metaData = "res://*/Chromoscan2DataModel.csdl|res://*/Chromoscan2DataModel.ssdl|res://*/Chromoscan2DataModel.msl";
        private const string _initialCatalog = "Chromoscan2";
        private string _connectionString;
        private string _sqlConnectionString;
        #endregion

        private string CreateConnectionString(string dataSource)
        {
            const string appName = "EntityFramework";
            const string providerName = "System.Data.SqlClient";

            SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder();
            sqlBuilder.DataSource = dataSource;
            sqlBuilder.InitialCatalog = _initialCatalog;
            sqlBuilder.MultipleActiveResultSets = true;
            sqlBuilder.IntegratedSecurity = false;
            sqlBuilder.Password = "OldBoOt46XX";
            sqlBuilder.UserID = "cv";
            sqlBuilder.PersistSecurityInfo = true;
            sqlBuilder.ApplicationName = appName;
            _sqlConnectionString = sqlBuilder.ConnectionString;
            EntityConnectionStringBuilder efBuilder = new EntityConnectionStringBuilder();
            efBuilder.Metadata = _metaData;
            efBuilder.Provider = providerName;
            efBuilder.ProviderConnectionString = sqlBuilder.ConnectionString;
            return efBuilder.ConnectionString;
        }

        public bool TestDatabaseConnection(string connectionString)
        {
            string dataSourceConnectionString = CreateConnectionString(connectionString);
            using (var testDB = new Chromoscan2Entities(dataSourceConnectionString))
            {
                try
                {
                    testDB.Database.Connection.Open();
                    testDB.Database.Connection.Close();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public Chromoscan2Entities GetCurrentDataBase(string connectionString)
        {
            _connectionString = CreateConnectionString(connectionString);
            return new Chromoscan2Entities(_connectionString);
        }

        public System.Data.DataTable ExecuteComplexSqlCommands(string commandText)
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();
            SqlCommand command = new SqlCommand(commandText, con);
            command.CommandTimeout = 120;
            command.Prepare();
            System.Data.DataTable dt = new System.Data.DataTable();
            using (SqlDataAdapter da = new SqlDataAdapter(command))
            {
                da.Fill(dt);
            }
            return dt;
        }
    }
}
