//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CytoApps.EntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class caseGenericLock
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public caseGenericLock()
        {
            this.genericObjectLocks = new HashSet<genericObjectLock>();
        }
    
        public System.Guid LockID { get; set; }
        public System.Guid CaseID { get; set; }
        public string SlideID { get; set; }
        public string Type { get; set; }
        public string UserID { get; set; }
    
        public virtual @case @case { get; set; }
        public virtual userDetail userDetail { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<genericObjectLock> genericObjectLocks { get; set; }
    }
}
