﻿using System;

namespace CytoApps.Exceptions
{
    /// <summary>
    /// This exception is thrown when there is a DataAccess error
    /// </summary>
    public class DALException : Exception
    {
        public enum DALErrorType
        {
            Unknown = 0,
            CorruptSlide,
            Authentication,
            DBConnection,
            Authorization
        }

        private DALErrorType _errorType = DALErrorType.Unknown;

        public DALException(string message, DALErrorType error) : base(message)
        {
            _errorType = error;
        }

        public DALException(string message, Exception innerException, DALErrorType error) : base(message, innerException)
        {
            _errorType = error;
        }

        private DALErrorType ErrorType
        {
            get { return _errorType; }
        }
    }

    public class RemoteServerException : Exception
    {
        public RemoteServerException()
        { }

        public RemoteServerException(string message) : base(message)
        { }
        public RemoteServerException(string message, Exception innerException) : base(message, innerException)
        { }
        
    }
}
