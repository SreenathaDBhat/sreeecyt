﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CytoApps.Infrastructure.UI.Controls
{
    /// <summary>
	/// A ContentControl that animates the transition between content
	/// </summary>
	[TemplatePart(Name = "PART_PaintArea", Type = typeof(Shape)),
     TemplatePart(Name = "PART_MainContent", Type = typeof(ContentPresenter))]
    public class AnimatedContentControl : ContentControl
    {
        #region Generated static constructor
        static AnimatedContentControl()
        {
            // To override Default style of base Control with ours.
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AnimatedContentControl), new FrameworkPropertyMetadata(typeof(AnimatedContentControl)));
        }
        #endregion

        Shape _oldContent;
        ContentPresenter _newContent;

        /// <summary>
		/// This gets called when the template has been applied and we have our visual tree
		/// </summary>
		public override void OnApplyTemplate()
        {
            _oldContent = GetTemplateChild("PART_PaintArea") as Shape;
            _newContent = GetTemplateChild("PART_MainContent") as ContentPresenter;

            base.OnApplyTemplate();
        }

        protected override void OnContentChanged(object oldContent, object newContent)
        {
            BeginAnimation();
            base.OnContentChanged(oldContent, newContent);
        }

        protected override void OnContentTemplateChanged(DataTemplate oldContentTemplate, DataTemplate newContentTemplate)
        {
            BeginAnimation();
            base.OnContentTemplateChanged(oldContentTemplate, newContentTemplate);
        }

        /// <summary>
		/// Creates a brush based on the current appearnace of a visual element. The brush is an ImageBrush and once created, won't update its look
		/// </summary>
		/// <param name="v">The visual element to take a snapshot of</param>
		private Brush CreateBrushFromVisual(Visual v)
        {
            if (v == null)
                throw new ArgumentNullException("visual is null");
            var target = new RenderTargetBitmap((int)this.ActualWidth, (int)this.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            target.Render(v);
            var brush = new ImageBrush(target);
            brush.Freeze();
            return brush;
        }

        #region Animation Dependency Properties
        public TransitionType Transition
        {
            get { return (TransitionType)GetValue(TransitionProperty); }
            set { SetValue(TransitionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Transition.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TransitionProperty =
            DependencyProperty.Register("Transition", typeof(TransitionType), typeof(AnimatedContentControl), new PropertyMetadata(TransitionType.SlideAndFade));

        #endregion

        #region Animation methods
        private void BeginAnimation()
        {
            if (_oldContent != null && _newContent != null)
            {
                //Create Visual Image of old content. So that we can animate the old content
                _oldContent.Fill = CreateBrushFromVisual(_newContent);
                // Change visibility of old and new content
                _oldContent.Visibility = System.Windows.Visibility.Visible;
                _newContent.Visibility = System.Windows.Visibility.Collapsed;

                // Clone instance is used to produce modifiable copies of frozen Freezable objects
                Storyboard outTransitionClone = (this.FindResource(string.Format("{0}Out", Transition.ToString())) as Storyboard).Clone();
                Storyboard inTransitionClone = (this.FindResource(string.Format("{0}In", Transition.ToString())) as Storyboard).Clone();
                
                if (inTransitionClone != null && outTransitionClone != null)
                {
                    // atach handler for out transition completed
                    outTransitionClone.Completed += (s, e) =>
                    {
                        // Start animation for new content
                        inTransitionClone.Begin(_newContent);
                        // Change visibility of old and new content
                        _oldContent.Visibility = System.Windows.Visibility.Collapsed;
                        _newContent.Visibility = System.Windows.Visibility.Visible;
                    };
                    // Start animation for old content
                    outTransitionClone.Begin(_oldContent);
                }
            }
        }
        #endregion
    }

    /// <summary>
    /// This Enum is gives the list of predefine Transitions (Storyboards)
    /// in Themes/ Generic resource file.
    /// </summary>
    public enum TransitionType
    {
        Fade,
        Slide,
        SlideAndFade,
        Grow,
        GrowAndFade,
        Flip,
        FlipAndFade,
        Spin,
        SpinAndFade,
        SlideAndFadeVertical,
        Queue
    }
}
