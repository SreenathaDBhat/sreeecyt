﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Media;
using CytoApps.Infrastructure.UI.Utilities;

namespace CytoApps.Infrastructure.UI.Controls
{
    /// <summary>
    /// Interaction logic for ChartControl.xaml
    /// </summary>
    public partial class ChartControl : UserControl
    {
        public ChartControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Updates the gradients for PieSeries.
        /// </summary>
        private void PieChartSeriesUpdater(PieDataPoint pieDataPoint, Rect pieBounds)
        {
            var brush = pieDataPoint.Background as RadialGradientBrush;
            if (null != brush)
            {
                brush = brush.Clone();
                var center = new Point(
                    pieBounds.Left + ((pieBounds.Right - pieBounds.Left) / 2),
                    pieBounds.Top + ((pieBounds.Bottom - pieBounds.Top) / 2));
                brush.Center = center;
                brush.GradientOrigin = center;
                var radius = (pieBounds.Right - pieBounds.Left) / 2;
                brush.RadiusX = radius;
                brush.RadiusY = radius;
                pieDataPoint.Background = brush;
            }
        }

        public object SelectedSlice
        {
            get { return (object)GetValue(SelectedSliceProperty); }
            set { SetValue(SelectedSliceProperty, value); }
        }

        /// <summary>
        /// Selected Slice Property
        /// </summary>
        public static readonly DependencyProperty SelectedSliceProperty =
            DependencyProperty.Register("SelectedSlice", typeof(object), typeof(ChartControl), new FrameworkPropertyMetadata(SelectedSliceChanged)
            { DefaultValue = null, BindsTwoWayByDefault = true, DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });

        static void SelectedSliceChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var pieChart = o as ChartControl;
            if(pieChart != null)
            {
                PieSeries pieSeries = pieChart.PieChart;
                if(pieSeries.SelectedItem != e.NewValue)
                {
                    pieSeries.SelectedItem = e.NewValue;
                }
            }
        }

        public bool IsGradientColorEnabled
        {
            get { return (bool)GetValue(IsGradientColorEnabledProperty); }
            set { SetValue(IsGradientColorEnabledProperty, value); }
        }

        /// <summary>
        /// Gradient Color Property 
        /// </summary>
        public static readonly DependencyProperty IsGradientColorEnabledProperty =
            DependencyProperty.Register("IsGradientColorEnabled", typeof(bool), typeof(ChartControl), new FrameworkPropertyMetadata() { DefaultValue = false, BindsTwoWayByDefault = true, DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PieSeries pieSeries = sender as PieSeries;
            if (pieSeries != null)
            {
                SelectedSlice = pieSeries.SelectedItem;
            }
        }

        private void PieChart_Loaded(object sender, RoutedEventArgs e)
        {
            if (IsGradientColorEnabled)
                PieDataPointMappingModeUpdater.UpdatePieSeries(PieChart, PieChartSeriesUpdater, true);
            //Update Pie chart visuals
            PieChart.LegendItems.CollectionChanged += LegendItemsCollectionChanged;
        }

        /// <summary>
        /// Update Pie chart visualization to radient brush when collection updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LegendItemsCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (IsGradientColorEnabled)
                PieDataPointMappingModeUpdater.UpdatePieSeries(PieChart, PieChartSeriesUpdater, true);
        }

        private void PieChart_Unloaded(object sender, RoutedEventArgs e)
        {
           // if (IsGradientColorEnabled)
                PieDataPointMappingModeUpdater.UpdatePieSeries(PieChart, PieChartSeriesUpdater, false);
            //Update Pie chart visuals
            PieChart.LegendItems.CollectionChanged -= LegendItemsCollectionChanged;
        }
    }

    /// <summary>
    /// Class demonstrating one way to apply gradients to an entire pie (vs. each slice separately)
    /// </summary>
    public static class PieDataPointMappingModeUpdater
    {
        /// <summary>
        /// Updates the PieDataPoints of a PieSeries by applying the specified action to each.
        /// </summary>
        /// <param name="pieSeries">PieSeries instance to update.</param>
        /// <param name="updater">Action to run for each PieDataPoint.</param>
        /// <param name="keepUpdated">true to attach to the SizeChanged event of the PieSeries's PlotArea.</param>
        public static void UpdatePieSeries(PieSeries pieSeries, Action<PieDataPoint, Rect> updater, bool keepUpdated)
        {
            // Apply template to ensure visual tree containing PlotArea is created
            pieSeries.ApplyTemplate();
            // Find PieSeries's PlotArea element
            var children = Traverse<FrameworkElement>(
                pieSeries,
                e => VisualTreeChildren(e).OfType<FrameworkElement>(),
                element => null == element as Chart);
            var plotArea = children.OfType<Panel>().Where(e => "PlotArea" == e.Name).FirstOrDefault();
            // If able to find the PlotArea...
            if (null != plotArea)
            {
                // Calculate the diameter of the pie (0.95 multiplier is from PieSeries implementation)
                var diameter = Math.Min(plotArea.ActualWidth, plotArea.ActualHeight) * 0.95;
                // Calculate the bounding rectangle of the pie
                var leftTop = new Point((plotArea.ActualWidth - diameter) / 2, (plotArea.ActualHeight - diameter) / 2);
                var rightBottom = new Point(leftTop.X + diameter, leftTop.Y + diameter);
                var pieBounds = new Rect(leftTop, rightBottom);
                // Call the provided updater action for each PieDataPoint
                foreach (var pieDataPoint in plotArea.Children.OfType<PieDataPoint>())
                {
                    updater(pieDataPoint, pieBounds);
                }
                // to keep the gradients updated, hook up to PlotArea.SizeChanged as well
                if (keepUpdated)
                {
                    SizeChangedEventHandler sizeChangedEventHandler = (sender, e) =>
                    {
                        UpdatePieSeries(pieSeries, updater, false);
                    };    
                                    
                    plotArea.SizeChanged += sizeChangedEventHandler;

                    plotArea.SizeChanged -= sizeChangedEventHandler;
                }

            }
        }

        private static void PlotArea_Unloaded(object sender, RoutedEventArgs e)
        {
            (sender as Panel).SizeChanged -= delegate
            {
                UpdatePieSeries(null, null, false);
            };
        }

        /// <summary>
        /// Traverses a tree by accepting an initial value and a function that retrieves the child nodes of a node.
        /// </summary>
        /// <typeparam name="T">The type of the stream.</typeparam>
        /// <param name="initialNode">The initial node.</param>
        /// <param name="getChildNodes">A function that retrieves the child nodes of a node.</param>
        /// <param name="traversePredicate">A predicate that evaluates a node and returns a value indicating whether that node and it's children should be traversed.</param>
        /// <returns>A stream of nodes.</returns>
        private static IEnumerable<T> Traverse<T>(T initialNode, Func<T, IEnumerable<T>> getChildNodes, Func<T, bool> traversePredicate)
        {
            Stack<T> stack = new Stack<T>();
            stack.Push(initialNode);
            while (stack.Count > 0)
            {
                T node = stack.Pop();
                if (traversePredicate(node))
                {
                    yield return node;
                    IEnumerable<T> childNodes = getChildNodes(node);
                    foreach (T childNode in childNodes)
                    {
                        stack.Push(childNode);
                    }
                }
            }
        }

        /// <summary>
        /// Implementation of getChildNodes parameter to Traverse based on the visual tree.
        /// </summary>
        /// <param name="reference">Object in the visual tree.</param>
        /// <returns>Stream of visual children of the object.</returns>
        private static IEnumerable<DependencyObject> VisualTreeChildren(DependencyObject reference)
        {
            var childrenCount = VisualTreeHelper.GetChildrenCount(reference);
            for (var i = 0; i < childrenCount; i++)
            {
                yield return VisualTreeHelper.GetChild(reference, i);
            }
        }
    }

    /// <summary>
    /// Convert color to RadialGradientBrush
    /// </summary>
    public class ColorToRadialGradientConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Color actualColor;
            if (value != null)
            {
                actualColor = (Color)ColorConverter.ConvertFromString(value.ToString());
            }
            else
                actualColor = Colors.Gray;
            Color darkerColor = ColorParse.ChangeColorBrightness(actualColor, -0.4f);
            GradientStop stop1 = new GradientStop { Color = actualColor, Offset = 0.0 };
            GradientStop stop2 = new GradientStop { Color = actualColor, Offset = 0.85 };
            GradientStop stop3 = new GradientStop { Color = darkerColor, Offset = 1.0 };
            GradientStopCollection gradientCollection = new GradientStopCollection();
            gradientCollection.Add(stop1);
            gradientCollection.Add(stop2);
            gradientCollection.Add(stop3);

            RadialGradientBrush radialGradientBrush = new RadialGradientBrush
            {
                MappingMode = BrushMappingMode.Absolute,
                GradientStops = gradientCollection
            };

            return radialGradientBrush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
