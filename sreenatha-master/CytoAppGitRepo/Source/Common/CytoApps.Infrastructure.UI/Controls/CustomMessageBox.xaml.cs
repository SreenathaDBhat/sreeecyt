﻿using CytoApps.Infrastructure.UI.Utilities;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Animation;

namespace CytoApps.Infrastructure.UI.Controls
{
    /// <summary>
    /// Describes the Animation Type for the message box.
    /// </summary>
    public enum AnimationType
    {
        FadeAway = 0,
    }
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    internal partial class CustomMessageBox : Window
    {
        #region "Member variables"

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);
        private const int SecondstoTicksRatio = 10000000;

        #endregion

        #region Constructor

        internal CustomMessageBox(string message, string caption, MessageBoxButton button)
        {
            InitializeComponent();
            Message = message;
            Header = caption;
            DisplayButtons(button);
            _CustomPopUp.Icon = Image_MessageBox.Source;
            SetAnimationType(AnimationType.FadeAway);
        }

        internal CustomMessageBox(string message, string caption, MessageBoxButton button, MessageBoxImage image)
        {
            InitializeComponent();
            Message = message;
            Header = caption;
            Image_MessageBox.Visibility = System.Windows.Visibility.Collapsed;
            DisplayButtons(button);
            DisplayImage(image);
            _CustomPopUp.Icon = Image_MessageBox.Source;
            SetAnimationType(AnimationType.FadeAway);

        }

        internal CustomMessageBox(string message, string caption, MessageBoxButton button, MessageBoxImage image, bool disableAnimation)
        {
            InitializeComponent();
            Message = message;
            Header = caption;
            Image_MessageBox.Visibility = System.Windows.Visibility.Collapsed;
            DisplayButtons(button);
            DisplayImage(image);
            _CustomPopUp.Icon = Image_MessageBox.Source;
            DisbaleAnimation = disableAnimation;
            SetAnimationType(AnimationType.FadeAway);

        }
        #endregion

        #region Properties

        internal string Header
        {
            get
            {
                return txtTitle.Text;
            }
            set
            {
                txtTitle.Text = value;
            }
        }

        internal string Message
        {
            get
            {
                return TextBlock_Message.Text;
            }
            set
            {
                TextBlock_Message.Text = value;
            }
        }

        public MessageBoxResult Result { get; set; }

        private bool _disbaleAnimation = false;
        internal bool DisbaleAnimation
        {
            get
            {
                return _disbaleAnimation;
            }
            set
            {
                _disbaleAnimation = value;
            }
        }

        #endregion

        #region Event Handlers and Methods

        private void DisplayButtons(MessageBoxButton button)
        {
            switch (button)
            {
                case MessageBoxButton.OKCancel:
                    // Hide all but OK, Cancel
                    Button_OK.Visibility = System.Windows.Visibility.Visible;
                    Button_OK.Focus();
                    Button_Cancel.Visibility = System.Windows.Visibility.Visible;
                    Button_Yes.Visibility = System.Windows.Visibility.Collapsed;
                    Button_No.Visibility = System.Windows.Visibility.Collapsed;
                    break;
                case MessageBoxButton.YesNo:
                    // Hide all but Yes, No
                    Button_Yes.Visibility = System.Windows.Visibility.Visible;
                    Button_Yes.Focus();
                    Button_No.Visibility = System.Windows.Visibility.Visible;

                    Button_OK.Visibility = System.Windows.Visibility.Collapsed;
                    Button_Cancel.Visibility = System.Windows.Visibility.Collapsed;
                    break;
                case MessageBoxButton.YesNoCancel:
                    // Hide only OK
                    Button_Yes.Visibility = System.Windows.Visibility.Visible;
                    Button_Yes.Focus();
                    Button_No.Visibility = System.Windows.Visibility.Visible;
                    Button_Cancel.Visibility = System.Windows.Visibility.Visible;

                    Button_OK.Visibility = System.Windows.Visibility.Collapsed;
                    break;
                default:
                    // Hide all but OK
                    Button_OK.Visibility = System.Windows.Visibility.Visible;
                    Button_OK.Focus();
                    Button_Yes.Visibility = System.Windows.Visibility.Collapsed;
                    Button_No.Visibility = System.Windows.Visibility.Collapsed;
                    Button_Cancel.Visibility = System.Windows.Visibility.Collapsed;
                    break;
            }
        }

        private void DisplayImage(MessageBoxImage image)
        {
            Icon icon;

            switch (image)
            {
                case MessageBoxImage.Exclamation:       // Enumeration value 48 - also covers "Warning"
                    icon = SystemIcons.Exclamation;
                    break;
                case MessageBoxImage.Error:             // Enumeration value 16, also covers "Hand" and "Stop"
                    icon = SystemIcons.Hand;
                    break;
                case MessageBoxImage.Information:       // Enumeration value 64 - also covers "Asterisk"
                    icon = SystemIcons.Information;
                    break;
                case MessageBoxImage.Question:
                    icon = SystemIcons.Question;
                    break;
                default:
                    icon = SystemIcons.Information;
                    break;
            }
            Image_MessageBox.Source = icon.ToImageSource();
            Image_MessageBox.Visibility = System.Windows.Visibility.Visible;
        }

        private void Button_OK_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.OK;
            Close();
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Cancel;
            Close();
        }

        private void Button_Yes_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Yes;
            Close();
        }

        private void Button_No_Click(object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.No;
            Close();
        }

        private void _CustomPopUp_Loaded(object sender, RoutedEventArgs e)
        {
            var hwnd = new WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            Close();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Set the animation for the Message box , also sets the animation duration for Fading.
        /// </summary>
        /// <param name="type"></param>
        private void SetAnimationType(AnimationType type)
        {
            if (!DisbaleAnimation)
            {
                switch (type)
                {
                    case AnimationType.FadeAway:
                        var storyboardchildren = (this.Resources["storyBoard"] as Storyboard).Children;
                        //Time span from number of ticks.
                        storyboardchildren[1].BeginTime = new TimeSpan((int)5 * SecondstoTicksRatio);
                        border.Style = this.Resources["borderFadeAnimationStyle"] as Style;
                        break;
                }
            }
        }

        private void Window_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        #endregion


    }
}
