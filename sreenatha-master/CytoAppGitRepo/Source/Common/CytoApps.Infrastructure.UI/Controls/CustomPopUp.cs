﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CytoApps.Infrastructure.UI.Controls
{
    public static class CustomPopUp
    {
        ///<summary>
        ///Display a message box with title , message  and icon  and button
        /// </summary>
        public static MessageBoxResult Show(string message, string title, MessageBoxButton button, MessageBoxImage icon)
        {
            CustomMessageBox messageBox = new CustomMessageBox(message, title, button, icon);
            messageBox.Owner = Application.Current.MainWindow;
            messageBox.ShowDialog();
            return messageBox.Result;

        }

        public static MessageBoxResult Show(string message, string title, MessageBoxButton button, MessageBoxImage icon, bool disbaleAnimation)
        {
            CustomMessageBox messageBox = new CustomMessageBox(message, title, button, icon, disbaleAnimation);
            messageBox.Owner = Application.Current.MainWindow;
            messageBox.ShowDialog();
            return messageBox.Result;

        }
        public static MessageBoxResult Show(string message, string title, MessageBoxButton button, MessageBoxImage icon, Window parent)
        {
            CustomMessageBox messageBox = new CustomMessageBox(message, title, button, icon, false);
            messageBox.Owner = parent;
            messageBox.ShowDialog();
            return messageBox.Result;

        }
        public static MessageBoxResult Show(string message, string title, MessageBoxButton button, MessageBoxImage icon, Window parent, bool disbaleAnimation)
        {
            CustomMessageBox messageBox = new CustomMessageBox(message, title, button, icon, disbaleAnimation);
            messageBox.Owner = parent;
            messageBox.ShowDialog();
            return messageBox.Result;

        }
    }
}
