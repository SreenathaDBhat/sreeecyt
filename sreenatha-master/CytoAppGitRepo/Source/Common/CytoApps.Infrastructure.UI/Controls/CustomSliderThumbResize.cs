﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace CytoApps.Infrastructure.UI.Controls
{
    /// <summary>
    /// Slider Control with adjustable thumb size
    /// </summary>
    public class CustomSliderThumbResize : Slider
    {
        public double SliderThumbSize
        {
            get { return (double)GetValue(SliderThumbSizeProperty); }
            set { SetValue(SliderThumbSizeProperty, value); }
        }

        public static readonly DependencyProperty SliderThumbSizeProperty =
            DependencyProperty.Register("SliderThumbSize", typeof(double), typeof(CustomSliderThumbResize), new PropertyMetadata(10d));


        static CustomSliderThumbResize()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
            typeof(CustomSliderThumbResize),
            new FrameworkPropertyMetadata(typeof(CustomSliderThumbResize)));
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();            
        }   
    }
}
