﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace CytoApps.Infrastructure.UI.Controls
{
    public class ListBoxMultipleSelection
    {
        #region SelectedItems

        private static ListBox list;
        private static bool _isRegisteredSelectionChanged = false;

        ///
        /// SelectedItems Attached Dependency Property
        ///
        public static readonly DependencyProperty SelectedItemsProperty =
        DependencyProperty.RegisterAttached("SelectedItems", typeof(IList),
        typeof(ListBoxMultipleSelection),
        new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
        new PropertyChangedCallback(OnSelectedItemsChanged)));

        public static IList GetSelectedItems(DependencyObject d)
        {
            return (IList)d.GetValue(SelectedItemsProperty);
        }

        public static void SetSelectedItems(DependencyObject d, IList value)
        {
            d.SetValue(SelectedItemsProperty, value);
        }

        private static void OnSelectedItemsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!_isRegisteredSelectionChanged)
            {
                var listBox = (ListBox)d;
                list = listBox;
                listBox.SelectionChanged += listBox_SelectionChanged;
                _isRegisteredSelectionChanged = true;
            }
        }

        private static void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Get list box's selected items.
            IEnumerable listBoxSelectedItems = list.SelectedItems;
            //Get list from model
            IList ModelSelectedItems = GetSelectedItems(list);

            //Update the model
            ModelSelectedItems.Clear();

            if (list.SelectedItems != null)
            {
                foreach (var item in list.SelectedItems)
                    ModelSelectedItems.Add(item);
            }
            SetSelectedItems(list, ModelSelectedItems);
        }
        #endregion
    }

    public class ListBoxHelper
    {
        private static ListBox list;
        //public static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.RegisterAttached("SelectedItems", typeof(IList), typeof(ListBoxHelper),
        //     new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnSelectedItemsChanged)));

        public static readonly DependencyProperty SelectedItemsProperty =
     DependencyProperty.RegisterAttached("SelectedItems", typeof(IList),
     typeof(ListBoxHelper),
     new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
     new PropertyChangedCallback(OnSelectedItemsChanged)));

        public static IList GetSelectedItems(DependencyObject d)
        {
            return (IList)d.GetValue(SelectedItemsProperty);
        }
        public static void SetSelectedItems(DependencyObject d, IList value)
        {
            d.SetValue(SelectedItemsProperty, value);
        }
        private static void OnSelectedItemsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var listBox = (ListBox)d;
            ReSetSelectedItems(listBox);
            listBox.SelectionChanged += delegate
            {
                ReSetSelectedItems(listBox);
            };
        }
        private static void ReSetSelectedItems(ListBox listBox)
        {
            IList selectedItems = GetSelectedItems(listBox);
            selectedItems.Clear();
            if (listBox.SelectedItems != null)
            {
                foreach (var item in listBox.SelectedItems)
                    selectedItems.Add(item);
            }
        }

    }

    //public class ListBoxSelectedItemsBehavior : Behavior<ListBox>
    //{
    //    protected override void OnAttached()
    //    {
    //        AssociatedObject.SelectionChanged += AssociatedObjectSelectionChanged;
    //    }

    //    protected override void OnDetaching()
    //    {
    //        AssociatedObject.SelectionChanged -= AssociatedObjectSelectionChanged;
    //    }

    //    void AssociatedObjectSelectionChanged(object sender, SelectionChangedEventArgs e)
    //    {
    //      foreach ( var item in AssociatedObject.SelectedItems)
    //        {
    //            SelectedItems.Add(item);
    //        }
    //      //  SelectedItems = AssociatedObject.SelectedItems;
    //    }

    //    public static readonly DependencyProperty SelectedItemsProperty =
    //        DependencyProperty.Register("SelectedItems", typeof(IList), typeof(ListBoxSelectedItemsBehavior),
    //        new FrameworkPropertyMetadata(new List(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

    //    public IList SelectedItems
    //    {
    //        get { return (IList)GetValue(SelectedItemsProperty); }
    //        set { SetValue(SelectedItemsProperty, value); }
    //    }
    //}
}
