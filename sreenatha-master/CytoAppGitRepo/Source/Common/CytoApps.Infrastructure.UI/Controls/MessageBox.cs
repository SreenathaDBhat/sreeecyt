﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CytoApps.Infrastructure.UI.Controls
{
    public static class MessageBox 
    {

        public static bool GetIsShowDialog(DependencyObject obj)
        {
            return (bool)obj.GetValue(ShowDialogProperty);
        }

        public static void SetIsShowDialog(DependencyObject obj, bool value)
        {
            obj.SetValue(ShowDialogProperty, value);
        }
        public static DependencyProperty ShowDialogProperty = DependencyProperty.RegisterAttached("IsShowDialog", typeof(bool), typeof(MessageBox), new FrameworkPropertyMetadata(OnShowDialogChanged));

        public static DependencyProperty ShowDialogTitleProperty = DependencyProperty.RegisterAttached("ShowDialogTitle", typeof(string), typeof(MessageBox), new FrameworkPropertyMetadata());

        public static DependencyProperty ShowDialogMessageProperty = DependencyProperty.RegisterAttached("ShowDialogMessage", typeof(string), typeof(MessageBox), new FrameworkPropertyMetadata());

        public static DependencyProperty ShowDialogImageProperty = DependencyProperty.RegisterAttached("ShowDialogImage", typeof(MessageBoxImage), typeof(MessageBox), new FrameworkPropertyMetadata());
        public static DependencyProperty ShowDialogButtonPproperty = DependencyProperty.RegisterAttached("ShowDialogButton", typeof(MessageBoxButton), typeof(MessageBox), new FrameworkPropertyMetadata());

        public static object OnShowDialogChanged { get; private set; }
       

        
    }
}
