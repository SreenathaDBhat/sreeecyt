﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace CytoApps.Infrastructure.UI.Controls
{
    [TemplatePart(Name = "PART_LowerSlider", Type = typeof(Slider)),
     TemplatePart(Name = "PART_UpperSlider", Type = typeof(Slider))]
    public class RangeSlider : RangeBase
    {
        #region Generated static constructor
        static RangeSlider()
        {
            // To override Default style of base Control with ours.
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RangeSlider), new FrameworkPropertyMetadata(typeof(RangeSlider)));
        }
        #endregion

        Slider _lowerSlider;
        Slider _upperSlider;
        public RangeSlider()
        {
            this.Loaded += RangeSlider_Loaded;
        }

        private void RangeSlider_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            _lowerSlider.ValueChanged += LowerSlider_ValueChanged;
            _upperSlider.ValueChanged += UpperSlider_ValueChanged;
        }

        private void LowerSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            _upperSlider.Value = Math.Max(_upperSlider.Value, _lowerSlider.Value);
        }

        private void UpperSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            _lowerSlider.Value = Math.Min(_upperSlider.Value, _lowerSlider.Value);
        }

        public override void OnApplyTemplate()
        {
            _lowerSlider = GetTemplateChild("PART_LowerSlider") as Slider;
            _upperSlider = GetTemplateChild("PART_UpperSlider") as Slider;

            base.OnApplyTemplate();
        }

        #region Dependency Properties 
        //public double Minimum
        //{
        //    get { return (double)GetValue(MinimumProperty); }
        //    set { SetValue(MinimumProperty, value); }
        //}

        //public static readonly DependencyProperty MinimumProperty =
        //    DependencyProperty.Register("Minimum", typeof(double), typeof(RangeSlider), new UIPropertyMetadata(0d));

        public double LowerValue
        {
            get { return (double)GetValue(LowerValueProperty); }
            set { SetValue(LowerValueProperty, value); }
        }

        public static readonly DependencyProperty LowerValueProperty =
            DependencyProperty.Register("LowerValue", typeof(double), typeof(RangeSlider), new UIPropertyMetadata(0d));

        public double UpperValue
        {
            get { return (double)GetValue(UpperValueProperty); }
            set { SetValue(UpperValueProperty, value); }
        }

        public static readonly DependencyProperty UpperValueProperty =
            DependencyProperty.Register("UpperValue", typeof(double), typeof(RangeSlider), new UIPropertyMetadata(0d));

        //public double Maximum
        //{
        //    get { return (double)GetValue(MaximumProperty); }
        //    set { SetValue(MaximumProperty, value); }
        //}

        //public static readonly DependencyProperty MaximumProperty =
        //    DependencyProperty.Register("Maximum", typeof(double), typeof(RangeSlider), new UIPropertyMetadata(1d));
        #endregion
    }

    [TemplatePart(Name = "PART_DateLowerSlider", Type = typeof(Slider)),
     TemplatePart(Name = "PART_DateUpperSlider", Type = typeof(Slider))]
    public class DateRangeSlider : Control
    {
        #region Generated static constructor
        static DateRangeSlider()
        {
            // To override Default style of base Control with ours.
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DateRangeSlider), new FrameworkPropertyMetadata(typeof(DateRangeSlider)));
        }
        #endregion
        Slider _lowerSlider;
        Slider _upperSlider;
        public DateRangeSlider()
        {
            this.Loaded += DateRangeSlider_Loaded;
        }

        private void DateRangeSlider_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            _lowerSlider.ValueChanged += LowerSlider_ValueChanged;
            _upperSlider.ValueChanged += UpperSlider_ValueChanged;
        }

        private void LowerSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            _upperSlider.Value = Math.Max(_upperSlider.Value, _lowerSlider.Value);
        }

        private void UpperSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            _lowerSlider.Value = Math.Min(_upperSlider.Value, _lowerSlider.Value);
        }

        public override void OnApplyTemplate()
        {
            _lowerSlider = GetTemplateChild("PART_DateLowerSlider") as Slider;
            _upperSlider = GetTemplateChild("PART_DateUpperSlider") as Slider;

            base.OnApplyTemplate();
        }

        #region Dependency Property - Minimum
        public DateTime Minimum
        {
            get { return (DateTime)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }

        public static readonly DependencyProperty MinimumProperty =
            DependencyProperty.Register("Minimum", typeof(DateTime), typeof(DateRangeSlider), new UIPropertyMetadata(DateTime.Now.AddDays(-15)));
        #endregion

        #region Dependency Property - Lower Value
        public DateTime LowerValue
        {
            get { return (DateTime)GetValue(LowerValueProperty); }
            set { SetValue(LowerValueProperty, value); }
        }

        public static readonly DependencyProperty LowerValueProperty =
            DependencyProperty.Register("LowerValue", typeof(DateTime), typeof(DateRangeSlider), new UIPropertyMetadata(DateTime.Now.AddDays(-1)));
        #endregion

        #region Dependency Property - Upper Value
        public DateTime UpperValue
        {
            get { return (DateTime)GetValue(UpperValueProperty); }
            set { SetValue(UpperValueProperty, value); }
        }

        public static readonly DependencyProperty UpperValueProperty =
            DependencyProperty.Register("UpperValue", typeof(DateTime), typeof(DateRangeSlider), new UIPropertyMetadata(DateTime.Now.AddDays(1), new PropertyChangedCallback(OnUpperValueChanged)));

        public static void OnUpperValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }
        #endregion

        #region Dependency Property - Maximum
        public DateTime Maximum
        {
            get { return (DateTime)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register("Maximum", typeof(DateTime), typeof(DateRangeSlider), new UIPropertyMetadata(DateTime.Now.AddDays(15)));

        public static void OnMaximumChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            DateRangeSlider slider = (DateRangeSlider)d;

            //if (slider.IsUpperValueLockedToMax)
            //{
            //    slider.UpperValue = (DateTime)e.NewValue;
            //}
        }

        #endregion

        #region Dependency Property - Small Change
        public TimeSpan SmallChange
        {
            get { return (TimeSpan)GetValue(SmallChangeProperty); }
            set { SetValue(SmallChangeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SmallChange.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SmallChangeProperty =
            DependencyProperty.Register("SmallChange", typeof(TimeSpan), typeof(DateRangeSlider),
                new UIPropertyMetadata(new TimeSpan(0, 0, 0, 1), new PropertyChangedCallback(OnSmallChangePropertyChanged)));

        protected static void OnSmallChangePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
           // System.Diagnostics.Debug.WriteLine(e.NewValue);
        }
        #endregion

        #region Dependency Property - Large Change

        public TimeSpan LargeChange
        {
            get { return (TimeSpan)GetValue(LargeChangeProperty); }
            set { SetValue(LargeChangeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LargeChange.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LargeChangeProperty =
            DependencyProperty.Register("LargeChange", typeof(TimeSpan), typeof(DateRangeSlider),
                    new UIPropertyMetadata(new TimeSpan(0, 0, 1, 0), new PropertyChangedCallback(OnLargeChangePropertyChanged)));

        protected static void OnLargeChangePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //System.Diagnostics.Debug.WriteLine(e.NewValue);
        }
        #endregion

        #region Events

        public static readonly RoutedEvent LowerValueChangedEvent = EventManager.RegisterRoutedEvent("LowerValueChanged", RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<DateTime>), typeof(DateRangeSlider));
        public event RoutedPropertyChangedEventHandler<DateTime> LowerValueChanged
        {
            add { AddHandler(LowerValueChangedEvent, value); }
            remove { RemoveHandler(LowerValueChangedEvent, value); }
        }

        public static readonly RoutedEvent UpperValueChangedEvent = EventManager.RegisterRoutedEvent("UpperValueChanged", RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<DateTime>), typeof(DateRangeSlider));
        public event RoutedPropertyChangedEventHandler<DateTime> UpperValueChanged
        {
            add { AddHandler(UpperValueChangedEvent, value); }
            remove { RemoveHandler(UpperValueChangedEvent, value); }
        }

        #endregion //Events
    }
}
