﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace CytoApps.Infrastructure.UI.Controls
{
    /// <summary>
    /// Search Mode Enum Properties
    /// </summary>
    public enum SearchMode
    {
        Instant,
        Delayed
    }
    /// <summary>
    /// SearchTextBox Control inherited from TextBox
    /// </summary>
    public class SearchTextBox : TextBox
    {
        /// <summary>
        /// Label Text Property for adding wateramrk text inside the search textbox
        /// </summary>
        public static DependencyProperty LabelTextProperty =
            DependencyProperty.Register(
                       "LabelText",
                       typeof(string),
                       typeof(SearchTextBox));
        /// <summary>
        /// Label Text Foreground color property
        /// </summary>
        public static DependencyProperty LabelTextColorProperty =
            DependencyProperty.Register(
                "LabelTextColor",
                typeof(Brush),
                typeof(SearchTextBox));

        /// <summary>
        /// Search Mode for Search TextBox 
        /// 1.Instant : OntextChanged Search Functionality should work
        /// 2.Delayed : On Pressing Enter Keyboard button search functionality should work 
        /// </summary>
        public static DependencyProperty SearchModeProperty =
            DependencyProperty.Register(
                "SearchMode",
                typeof(SearchMode),
                typeof(SearchTextBox),
                new PropertyMetadata(SearchMode.Instant));

        /// <summary>
        /// Has TextProperty for changing visibility of label search text 
        /// </summary>
        private static DependencyPropertyKey HasTextPropertyKey =
            DependencyProperty.RegisterReadOnly(
                "HasText",
                typeof(bool),
                typeof(SearchTextBox),
                new PropertyMetadata());

        public static DependencyProperty HasTextProperty = HasTextPropertyKey.DependencyProperty;



        public ICommand SearchOptionCommand
        {
            get { return (ICommand)GetValue(SearchOptionCommandProperty); }
            set { SetValue(SearchOptionCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SearchOptionCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SearchOptionCommandProperty =
            DependencyProperty.Register("SearchOptionCommand", typeof(ICommand), typeof(SearchTextBox), new PropertyMetadata(null));



        public object SearchOptionCommandParameter
        {
            get { return (object)GetValue(SearchOptionCommandParameterProperty); }
            set { SetValue(SearchOptionCommandParameterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SearchOptionCommandParameter.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SearchOptionCommandParameterProperty =
            DependencyProperty.Register("SearchOptionCommandParameter", typeof(object), typeof(SearchTextBox), new PropertyMetadata(null));



        static SearchTextBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
            typeof(SearchTextBox),
            new FrameworkPropertyMetadata(typeof(SearchTextBox)));
        }

        /// <summary>
        /// OnTextChanged Event to begin Search
        /// </summary>
        /// <param name="e"></param>
        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            base.OnTextChanged(e);
            HasText = Text.Length != 0;

            if (SearchMode == SearchMode.Instant)
            {
                searchEventDelayTimer.Stop();
                searchEventDelayTimer.Start();
            }
        }

        public string LabelText
        {
            get { return (string)GetValue(LabelTextProperty); }
            set { SetValue(LabelTextProperty, value); }
        }

        public Brush LabelTextColor
        {
            get { return (Brush)GetValue(LabelTextColorProperty); }
            set { SetValue(LabelTextColorProperty, value); }
        }

        public SearchMode SearchMode
        {
            get { return (SearchMode)GetValue(SearchModeProperty); }
            set { SetValue(SearchModeProperty, value); }
        }

        public bool HasText
        {
            get { return (bool)GetValue(HasTextProperty); }
            private set { SetValue(HasTextPropertyKey, value); }
        }

        private static DependencyPropertyKey IsMouseLeftButtonDownPropertyKey =
        DependencyProperty.RegisterReadOnly(
        "IsMouseLeftButtonDown",
        typeof(bool),
        typeof(SearchTextBox),
        new PropertyMetadata());
        public static DependencyProperty IsMouseLeftButtonDownProperty =
            IsMouseLeftButtonDownPropertyKey.DependencyProperty;


        public bool IsMouseLeftButtonDown
        {
            get { return (bool)GetValue(IsMouseLeftButtonDownProperty); }
            private set { SetValue(IsMouseLeftButtonDownPropertyKey, value); }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            Border iconBorder = GetTemplateChild("PART_SearchIconBorder") as Border;
            if (iconBorder != null)
            {
                iconBorder.MouseLeftButtonDown += new MouseButtonEventHandler(IconBorder_MouseLeftButtonDown);
                iconBorder.MouseLeftButtonUp += new MouseButtonEventHandler(IconBorder_MouseLeftButtonUp);
                iconBorder.MouseLeave += new MouseEventHandler(IconBorder_MouseLeave);
            }
            Border optionBorder = GetTemplateChild("PART_SearchOptionBorder") as Border;
            if (optionBorder != null)
            {
                optionBorder.MouseLeftButtonDown += OptionBorder_MouseLeftButtonDown;
            }
        }

        private void OptionBorder_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (this.SearchOptionCommand != null)
            {
                if (this.SearchOptionCommand.CanExecute(SearchOptionCommandParameter))
                {
                    this.SearchOptionCommand.Execute(SearchOptionCommandParameter);
                }
            }
        }

        private void IconBorder_MouseLeftButtonDown(object obj, MouseButtonEventArgs e)
        {
            IsMouseLeftButtonDown = true;
        }


        private void IconBorder_MouseLeftButtonUp(object obj, MouseButtonEventArgs e)
        {
            if (!IsMouseLeftButtonDown) return;

            if (/*HasText && */ SearchMode == SearchMode.Instant)
            {
                this.Text = "";
            }
            if (/*HasText && */ SearchMode == SearchMode.Delayed)
            {
                RaiseSearchEvent();
            }

            IsMouseLeftButtonDown = false;
        }

        private void IconBorder_MouseLeave(object obj, MouseEventArgs e)
        {
            IsMouseLeftButtonDown = false;
        }

        public static DependencyProperty SearchEventTimeDelayProperty =
        DependencyProperty.Register(
        "SearchEventTimeDelay",
        typeof(Duration),
        typeof(SearchTextBox),
        new FrameworkPropertyMetadata(
            new Duration(new TimeSpan(0, 0, 0, 0, 500)),
            new PropertyChangedCallback(OnSearchEventTimeDelayChanged)));

        public Duration SearchEventTimeDelay
        {
            get { return (Duration)GetValue(SearchEventTimeDelayProperty); }
            set { SetValue(SearchEventTimeDelayProperty, value); }
        }

        static void OnSearchEventTimeDelayChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            SearchTextBox stb = o as SearchTextBox;
            if (stb != null)
            {
                stb.searchEventDelayTimer.Interval = ((Duration)e.NewValue).TimeSpan;
                stb.searchEventDelayTimer.Stop();
            }
        }
        public static readonly RoutedEvent SearchEvent =
        EventManager.RegisterRoutedEvent(
        "Search",
        RoutingStrategy.Bubble,
        typeof(RoutedEventHandler),
        typeof(SearchTextBox));


        public event RoutedEventHandler Search
        {
            add { AddHandler(SearchEvent, value); }
            remove { RemoveHandler(SearchEvent, value); }
        }
        private void RaiseSearchEvent()
        {
            RoutedEventArgs args = new RoutedEventArgs(SearchEvent);
            RaiseEvent(args);
        }

        private DispatcherTimer searchEventDelayTimer;
        public SearchTextBox() : base()
        {
            searchEventDelayTimer = new DispatcherTimer();
            searchEventDelayTimer.Interval = SearchEventTimeDelay.TimeSpan;
            searchEventDelayTimer.Tick += new EventHandler(OnSeachEventDelayTimerTick);
        }


        void OnSeachEventDelayTimerTick(object o, EventArgs e)
        {
            searchEventDelayTimer.Stop();
            RaiseSearchEvent();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Escape && SearchMode == SearchMode.Instant)
            {
                this.Text = "";
            }
            else if ((e.Key == Key.Return || e.Key == Key.Enter) && SearchMode == SearchMode.Delayed)
            {
                RaiseSearchEvent();
            }
            else
            {
                base.OnKeyDown(e);
            }
        }
        
    }
}
