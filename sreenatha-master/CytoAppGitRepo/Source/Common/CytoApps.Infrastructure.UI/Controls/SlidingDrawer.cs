﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace CytoApps.Infrastructure.UI.Controls
{
    public class SlidingDrawer : ContentControl
    {
        public static DependencyProperty IsOpenProperty = DependencyProperty.Register("IsOpen", typeof(bool), typeof(SlidingDrawer), new FrameworkPropertyMetadata()
        {
            PropertyChangedCallback = OnSliderPopupChanged,
            BindsTwoWayByDefault = true
        });

        private FrameworkElement PART_SlidingPanel;

        static SlidingDrawer()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SlidingDrawer), new FrameworkPropertyMetadata(typeof(SlidingDrawer)));
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            PART_SlidingPanel = (FrameworkElement)GetTemplateChild("PART_SlidingPanel");
            PART_SlidingPanel.MouseLeftButtonUp += new MouseButtonEventHandler(PART_SlidingPanel_MouseLeftButtonUp);

            Button closeButton = (Button)GetTemplateChild("PART_CloseButton");
            closeButton.Click += new RoutedEventHandler(closeButton_Click);
        }

        void closeButton_Click(object sender, RoutedEventArgs e)
        {
            IsOpen = false;
        }

        private void PART_SlidingPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            return;
        }

        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetCurrentValue(IsOpenProperty, value); }
        }

        private static void OnSliderPopupChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue.Equals(e.OldValue))
                return;

            SlidingDrawer tis = (SlidingDrawer)sender;

            bool v = (bool)e.NewValue;
            if (v)
            {
                Storyboard sb = (Storyboard)tis.PART_SlidingPanel.FindResource("extend");
                sb.Begin(tis.PART_SlidingPanel);
            }
            else
            {
                Storyboard sb = (Storyboard)tis.PART_SlidingPanel.FindResource("retract");
                sb.Begin(tis.PART_SlidingPanel);
            }
        }
    }
}
