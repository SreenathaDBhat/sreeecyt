﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace CytoApps.Infrastructure.UI.Controls
{
    public enum StretchSizeMode
    {
        Stretch,
        Size
    }

    public class ZoomChangedEventArgs : EventArgs
    {
        public double Zoom { get; set; }
    }

    public class ViewportChangedEventArgs : EventArgs
    {
        public Rect Viewport { get; set; }
    }

    public class StretchOrSizeCanvas : Panel, INotifyPropertyChanged
    {
        public static readonly DependencyProperty DomainWidthProperty = DependencyProperty.Register("DomainWidth", typeof(double), typeof(StretchOrSizeCanvas), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));
        public static readonly DependencyProperty DomainHeightProperty = DependencyProperty.Register("DomainHeight", typeof(double), typeof(StretchOrSizeCanvas), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));
        public static readonly DependencyProperty SizeModeProperty = DependencyProperty.RegisterAttached("SizeMode", typeof(StretchSizeMode), typeof(StretchOrSizeCanvas), new FrameworkPropertyMetadata(StretchSizeMode.Stretch, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));
        public static readonly DependencyProperty CenterOnProperty = DependencyProperty.RegisterAttached("CenterOn", typeof(Point), typeof(StretchOrSizeCanvas), new FrameworkPropertyMetadata(new Point(), FrameworkPropertyMetadataOptions.AffectsRender, new PropertyChangedCallback(OnCenterOn)));

        public static readonly DependencyProperty MaxZoomProperty = DependencyProperty.Register("MaxZoom", typeof(double), typeof(StretchOrSizeCanvas), new FrameworkPropertyMetadata(6.0, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

        public static readonly DependencyProperty ZoomFactorProperty = DependencyProperty.Register("ZoomFactor", typeof(double), typeof(StretchOrSizeCanvas), new FrameworkPropertyMetadata(2.0, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

        public event EventHandler<ZoomChangedEventArgs> ZoomChanged;
        public event EventHandler<ViewportChangedEventArgs> ViewportChanged;

        private double scale = 1;
        private double zoom = 1;
        private double panX = 0;
        private double panY = 0;
        private Point? pan;
        private UIElement inputElement;
        private Size availSize;

        public StretchOrSizeCanvas()
        {
            SizeChanged += new SizeChangedEventHandler(OnSizeChangedEvent);
        }

        static void OnCenterOn(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var p = (Point)e.NewValue;
            ((StretchOrSizeCanvas)sender).ZoomAndPanTo(p.X, p.Y, 300);
        }

        public Point CenterOn
        {
            get { return (Point)GetValue(CenterOnProperty); }
            set { SetValue(CenterOnProperty, value); }
        }

        private void OnSizeChangedEvent(object sender, SizeChangedEventArgs e)
        {
            Zoom = 1;
            FireZoomChanged();
            FireViewportChanged();
            InvalidateMeasure();
        }

        private void FireZoomChanged()
        {
            if (ZoomChanged != null)
                ZoomChanged(this, new ZoomChangedEventArgs { Zoom = this.Zoom });
        }

        private void FireViewportChanged()
        {
            if (ViewportChanged != null)
                ViewportChanged(this, new ViewportChangedEventArgs { Viewport = ViewportInDomainCoords() });
        }

        public void ResetZoom()
        {
            Zoom = 1;
            FireZoomChanged();
            FireViewportChanged();
            InvalidateArrange();
        }

        public Rect ViewportInDomainCoords()
        {
            var centre = CentreOfScreenInDomainCoords();
            var w = availSize.Width / (Zoom * Scale);
            var h = availSize.Height / (Zoom * Scale);
            return new Rect(centre.X - (w / 2), centre.Y - (h / 2), w, h);
        }

        public Point CentreOfScreenInDomainCoords()
        {
            double x = ((ActualWidth / 2) - panX) / (Zoom * Scale);
            double y = ((ActualHeight / 2) - panY) / (Zoom * Scale);
            return new Point() { X = x, Y = y };
        }

        public double MaxZoom
        {
            get { return (double)GetValue(MaxZoomProperty); }
            set { SetValue(MaxZoomProperty, value); }
        }

        public double ZoomFactor
        {
            get { return (double)GetValue(ZoomFactorProperty); }
            set { SetValue(ZoomFactorProperty, value); }
        }

        public double DomainWidth
        {
            get { return (double)GetValue(DomainWidthProperty); }
            set { SetValue(DomainWidthProperty, value); }
        }

        public double DomainHeight
        {
            get { return (double)GetValue(DomainHeightProperty); }
            set { SetValue(DomainHeightProperty, value); }
        }

        public static StretchSizeMode GetSizeMode(UIElement target)
        {
            object r = target.GetValue(SizeModeProperty);
            return (r == null || r == DependencyProperty.UnsetValue) ? StretchSizeMode.Stretch : (StretchSizeMode)r;
        }

        public static void SetSizeMode(UIElement target, StretchSizeMode value)
        {
            target.SetValue(SizeModeProperty, value);
        }

        private double Scale
        {
            get { return scale; }
            set { scale = value; Notify("DisplayScale"); }
        }

        private double Zoom
        {
            get { return zoom; }
            set { zoom = value; Notify("DisplayScale"); }
        }

        public double DisplayScale
        {
            get { return Scale * Zoom; }
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            foreach (UIElement child in Children)
                child.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));

            if (Math.Abs(DomainWidth) < double.Epsilon)
                return new Size(100, 100);

            bool wMajor = true;

            if (double.IsInfinity(availableSize.Width) && double.IsInfinity(availableSize.Height))
            {
                return base.MeasureOverride(availableSize);
            }
            else if (double.IsInfinity(availableSize.Width))
            {
                wMajor = false;
            }
            else if (double.IsInfinity(availableSize.Height))
            {
                wMajor = true;
            }
            else
            {
                wMajor = availableSize.Width >= availableSize.Height;
            }

            // might not need some of the above...

            availSize = availableSize;
            double aspect = DomainHeight / DomainWidth;

            double AW = availableSize.Width;
            double AH = availableSize.Height;
            double DW = DomainWidth;
            double DH = DomainHeight;

            bool domainWidthBigger = DW >= DH;
            bool availableWidthBigger = AW >= AH;

            if (domainWidthBigger && !availableWidthBigger)
            {
                return new Size(AW, AW * (DH / DW));
            }
            else if (!domainWidthBigger && availableWidthBigger)
            {
                return new Size(AH * (DW / DH), AH);
            }
            else if (domainWidthBigger && availableWidthBigger)
            {
                if (AW / AH >= DW / DH)
                {
                    return new Size(AH * (DW / DH), AH);
                }
                else
                {
                    return new Size(AW, AW * (DH / DW));
                }
            }
            else /* (!domainWidthBigger && !availableWidthBigger) */
            {
                if (AH / AW >= DH / DW)
                {
                    return new Size(AW, AW * (DH / DW));
                }
                else
                {
                    return new Size(AH * (DW / DH), AH);
                }
            }
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            if (inputElement == null || !Children.Contains(inputElement))
                ChooseInputElement();

            foreach (UIElement child in Children)
            {
                UIElement c = child;

                if (child is ContentPresenter)
                    c = (UIElement)VisualTreeHelper.GetChild(child, 0);

                StretchSizeMode mode = GetSizeMode(c);

                CalculateScale(finalSize);

                if (mode == StretchSizeMode.Size)
                    SizeArrange(child, Scale);
                else
                    StretchArrange(child, Scale);
            }

            return base.ArrangeOverride(finalSize);
        }

        private void CalculateScale(Size finalSize)
        {
            double AW = finalSize.Width;
            double AH = finalSize.Height;
            double DW = DomainWidth;
            double DH = DomainHeight;

            bool domainWidthBigger = DW >= DH;
            bool availableWidthBigger = AW >= AH;

            if (domainWidthBigger && !availableWidthBigger)
            {
                Scale = AW / DW;
            }
            else if (!domainWidthBigger && availableWidthBigger)
            {
                Scale = AH / DH;
            }
            else if (domainWidthBigger && availableWidthBigger)
            {
                if (AW / AH >= DW / DH)
                {
                    Scale = AH / DH;
                }
                else
                {
                    Scale = AW / DW;
                }
            }
            else /* (!domainWidthBigger && !availableWidthBigger) */
            {
                if (AH / AW >= DH / DW)
                {
                    Scale = AW / DW;
                }
                else
                {
                    Scale = AH / DH;
                }
            }
        }

        private void ChooseInputElement()
        {
            UIElement fallback = null;

            foreach (UIElement e in Children)
            {
                if (GetSizeMode(e) == StretchSizeMode.Stretch)
                {
                    inputElement = e;
                    return;
                }

                fallback = e;
            }

            inputElement = fallback;
        }

        private void StretchArrange(UIElement child, double correctScale)
        {
            if (Zoom == 1)
            {
                double w = DomainWidth * correctScale;
                double h = DomainHeight * correctScale;
                panX = (ActualWidth - w) / 2;
                panY = (ActualHeight - h) / 2;

                child.RenderTransform = new ScaleTransform(correctScale, correctScale);
                child.Arrange(new Rect(panX, panY, DomainWidth, DomainHeight));
            }
            else
            {
                double w = DomainWidth * correctScale * Zoom;
                double h = DomainHeight * correctScale * Zoom;
                double left = panX;
                double top = panY;

                child.RenderTransform = new ScaleTransform(correctScale * Zoom, correctScale * Zoom);
                child.Arrange(new Rect(left, top, DomainWidth, DomainHeight));
            }
        }

        private void SizeArrange(UIElement child, double correctScale)
        {
            if ((DomainWidth > 0.0) && (DomainHeight > 0.0))
            {
                if (Zoom == 1)
                {
                    double w = DomainWidth * correctScale;
                    double h = DomainHeight * correctScale;
                    panX = (ActualWidth - w) / 2;
                    panY = (ActualHeight - h) / 2;

                    child.Arrange(new Rect(panX, panY, w, h));
                }
                else
                {
                    double w = DomainWidth * correctScale * Zoom;
                    double h = DomainHeight * correctScale * Zoom;

                    child.Arrange(new Rect(panX, panY, w, h));
                }
            }
        }

        public void CentreOnDomainPoint(Point pt, double newZoom)
        {
            zoom = newZoom;
            double w = DomainWidth * scale * zoom;
            double h = DomainHeight * scale * zoom;
            double offsetx = pt.X * zoom * scale;
            double offsety = pt.Y * zoom * scale;
            panX = -offsetx + (ActualWidth / 2);
            panY = -offsety + (ActualHeight / 2);

            FireZoomChanged();
            FireViewportChanged();
            InvalidateArrange();
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            base.OnMouseWheel(e);
            e.Handled = true;

            double z = Math.Max(1, Math.Min(MaxZoom, (Zoom) * ((Math.Sign(e.Delta) < 0) ? 1 / ZoomFactor : ZoomFactor)));
            if (inputElement == null)
            {
                Zoom = 1;
                FireZoomChanged();
                FireViewportChanged();
                InvalidateArrange();
                return;
            }

            Point pt = e.GetPosition(inputElement);

            double oldW = DomainWidth * Zoom;
            double newW = DomainWidth * z;
            double oldH = DomainHeight * Zoom;
            double newH = DomainHeight * z;
            double wDelta = newW - oldW;
            double hDelta = newH - oldH;
            double x = pt.X / DomainWidth;
            double y = pt.Y / DomainHeight;


            panX -= wDelta * x * Scale;
            panY -= hDelta * y * Scale;
            Zoom = z;
            FireZoomChanged();
            FireViewportChanged();
            InvalidateArrange();
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            HijackMouseDown(e);
        }

        public void HijackMouseDown(MouseButtonEventArgs e)
        {
            if ((e.ChangedButton == MouseButton.Middle) && Zoom > 1)
            {
                CaptureMouse();
                pan = e.GetPosition(this);
                e.Handled = true;
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (IsMouseCaptured && Zoom > 1 && pan != null)
            {
                this.Cursor = Cursors.Hand;
                Point pt = e.GetPosition(this);
                panX += pt.X - pan.Value.X;
                panY += pt.Y - pan.Value.Y;
                pan = pt;
                FireViewportChanged();
                InvalidateArrange();
                e.Handled = true;
            }
            else
            {
                base.OnMouseMove(e);
            }
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
            if (IsMouseCaptured && (e.ChangedButton == MouseButton.Middle || e.ChangedButton == MouseButton.Left))
            {
                e.Handled = true;
                ReleaseMouseCapture();
                pan = null;
            }
        }

        public void ZoomAndPanTo(double x, double y, double targetLength, bool IsWidth = true)
        {
            //// Remove the delegate references just commnented out the code 
            //if (ActualHeight == 0)
            //{
            //    WaitUntilAMeasureHasHappenedAndThenZoomAndPan_HACK(x, y, targetLength, IsWidth);
            //}

            //else
            //{
            ZoomAndPan(x, y, targetLength, IsWidth);
            //}
        }

        private void WaitUntilAMeasureHasHappenedAndThenZoomAndPan_HACK(double x, double y, double targetLength, bool IsWidth)
        {
            ThreadPool.QueueUserWorkItem(delegate
            {
                while (ActualWidth == 0)
                    Thread.Sleep(10);

                Dispatcher.Invoke((ThreadStart)delegate
                {
                    ZoomAndPan(x, y, targetLength, IsWidth);

                }, DispatcherPriority.Normal);
            });
        }

        private void ZoomAndPan(double x, double y, double targetLength, bool IsWidth)
        {
            double z = Math.Max(1, Math.Min(MaxZoom, (IsWidth) ? DomainWidth / (targetLength) : DomainHeight / (targetLength)));
            Zoom = z;
            double ratio = (IsWidth) ? ActualHeight / ActualWidth : ActualWidth / ActualHeight;

            double targetHeight = (IsWidth) ? targetLength * ratio : targetLength;
            double targetWidth = (IsWidth) ? targetLength : targetLength * ratio;

            double adjustedTargetWidth = targetWidth * Zoom * Scale;
            double adjustedTargetHeight = targetHeight * Zoom * Scale;

            panX = -(x * Zoom * Scale) + (adjustedTargetWidth / 2);
            panY = -(y * Zoom * Scale) + (adjustedTargetHeight / 2);

            FireZoomChanged();
            FireViewportChanged();
            InvalidateArrange();
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
