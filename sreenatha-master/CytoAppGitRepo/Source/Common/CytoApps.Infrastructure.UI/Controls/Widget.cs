﻿using System.Windows;
using System.Windows.Controls;

namespace CytoApps.Infrastructure.UI.Controls
{
    public class Widget : HeaderedContentControl
    {
        #region Dependency Properties


        public object Glyph
        {
            get { return (object)GetValue(GlyphProperty); }
            set { SetValue(GlyphProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Glyph.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GlyphProperty =
            DependencyProperty.Register("Glyph", typeof(object), typeof(Widget), new PropertyMetadata(null));
        #endregion
    }
}
