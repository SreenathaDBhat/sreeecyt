﻿using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace CytoApps.Infrastructure.UI.Converters
{
    /// <summary>
    /// Add WaterMark Text to textbox
    /// To set watermark use http://schemas.microsoft.com/expression/2010/interactivity in xaml 
    /// use its reference and  namespace of this class
    //<i:Interaction.Behaviors>
    // <Converters:WatermarkBehavior WaterMark = "Hint Text" Foreground="Gray" />
    //</i:Interaction.Behaviors>
    /// </summary>
    public class WatermarkBehavior : Behavior<TextBox>
    {
        private WaterMarkAdorner adorner;

        public string WaterMark
        {
            get
            {
                return (string)GetValue(WaterMarkProperty);
            }
            set
            {
                SetValue(WaterMarkProperty, value);
            }
        }

        // Using a DependencyProperty for displaying watermark text
        public static readonly DependencyProperty WaterMarkProperty =
            DependencyProperty.Register("WaterMark", typeof(string), typeof(WatermarkBehavior), new PropertyMetadata("Watermark"));


        public double FontSize
        {
            get
            {
                return (double)GetValue(FontSizeProperty);
            }
            set
            {
                SetValue(FontSizeProperty, value);
            }
        }

        // Using a DependencyProperty for setting watermark FontSize.  
        public static readonly DependencyProperty FontSizeProperty =
            DependencyProperty.Register("FontSize", typeof(double), typeof(WatermarkBehavior), new PropertyMetadata(12.0));


        public Brush Foreground
        {
            get
            {
                return (Brush)GetValue(ForegroundProperty);
            }
            set
            {
                SetValue(ForegroundProperty, value);
            }
        }

        //  Using a DependencyProperty for setting watermark Foreground.
        public static readonly DependencyProperty ForegroundProperty =
            DependencyProperty.Register("Foreground", typeof(Brush), typeof(WatermarkBehavior), new PropertyMetadata(Brushes.Black));

        public string FontFamily
        {
            get
            {
                return (string)GetValue(FontFamilyProperty);
            }
            set
            {
                SetValue(FontFamilyProperty, value);
            }
        }

        // Using a DependencyProperty for setting watermark FontFamily.  
        public static readonly DependencyProperty FontFamilyProperty =
            DependencyProperty.Register("FontFamily", typeof(string), typeof(WatermarkBehavior), new PropertyMetadata("Segoe UI"));

        protected override void OnAttached()
        {
            adorner = new WaterMarkAdorner(AssociatedObject, WaterMark, FontSize, FontFamily, Foreground);
            AssociatedObject.Loaded += OnLoaded;
            AssociatedObject.GotFocus += OnFocus;
            AssociatedObject.LostFocus += OnLostFocus;
        }

        /// <summary>
        /// Load the attached adorner to the control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (!AssociatedObject.IsFocused)
            {
                if (string.IsNullOrEmpty(AssociatedObject.Text))
                {
                    var layer = AdornerLayer.GetAdornerLayer(AssociatedObject);
                    if (layer != null)
                    {
                        layer.Remove(adorner);
                        layer.Add(adorner);
                    }
                }
            }
        }

        /// <summary>
        /// Add adorner to control on lost focus and string is null/empty
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(AssociatedObject.Text))
            {
                var layer = AdornerLayer.GetAdornerLayer(AssociatedObject);
                layer.Add(adorner);
            }
        }

        /// <summary>
        /// Remove adorner when textbox is focused
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnFocus(object sender, RoutedEventArgs e)
        {
            var layer = AdornerLayer.GetAdornerLayer(AssociatedObject);
            layer.Remove(adorner);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
        }

        public class WaterMarkAdorner : Adorner
        {
            private string text;
            private double fontSize;
            private string fontFamily;
            private Brush foreground;

            public WaterMarkAdorner(UIElement element, string text, double fontsize, string font, Brush foreground)
                : base(element)
            {
                IsHitTestVisible = false;
                Opacity = 0.6;
                fontSize = fontsize;
                fontFamily = font;
                this.text = text;
                this.foreground = foreground;
            }

            protected override void OnRender(DrawingContext drawingContext)
            {
                base.OnRender(drawingContext);
                var text = new FormattedText(
                        this.text,
                        System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        new Typeface(fontFamily),
                        fontSize,
                        foreground);

                drawingContext.DrawText(text, new Point(3, 3));
            }
        }
    }

    /// <summary>
    /// Sets the focus to the specified Target.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class SetFocusAction : TargetedTriggerAction<UIElement>
    {
        protected override void Invoke(object parameter)
        {
            if (this.Target != null)
            {
                // Set Focus on Target object
                this.Target.Focus();
                Keyboard.Focus(this.Target);
            }
        }
    }

    /// <summary>
    /// Add InvokeKeyboardShortcutsBehavior To UIElement
    /// To invoke Keyboard shortcuts on specified UIElement
    /// use its reference and namespace of this class
    //<i:Interaction.Behaviors>
    // <Converters:InvokeKeyboardShortcutsBehavior />
    //</i:Interaction.Behaviors>
    /// </summary>
    public class InvokeKeyboardShortcutsBehavior : Behavior<FrameworkElement>
    {
        private static object associatedWindowsLock = new object();
        Window window = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="RegisterAppShortcutKeysBehavior"/> class.
        /// </summary>
        public InvokeKeyboardShortcutsBehavior()
        {
        }

        private void CommandTarget_Loaded(object sender, RoutedEventArgs e)
        {
            FrameworkElement commandTarget = sender as FrameworkElement;
            if (commandTarget != null)
            {
                commandTarget.Loaded -= CommandTarget_Loaded;
                //Register PreviewkeyDown on Parent window.
                RegisterAssociatedWindow(commandTarget);
                SetFocus(commandTarget);
            }
        }

        private void CommandTarget_Unloaded(object sender, RoutedEventArgs e)
        {
            FrameworkElement commandTarget = sender as FrameworkElement;
            if (commandTarget != null)
            {
                // Detach unload event handler
                commandTarget.Unloaded -= CommandTarget_Unloaded;
                //unregister PreviewkeyDown on Parent window, when task completes
                UnregisterAssociatedWindow(commandTarget);
            }
        }

        /// <summary>
        /// Called after the behavior is attached to an AssociatedObject.
        /// </summary>
        /// <remarks>Override this to hook up functionality to the AssociatedObject.</remarks>
        protected override void OnAttached()
        {
            base.OnAttached();
            //Get AssociatedObject
            var frameworkElement = this.AssociatedObject as FrameworkElement;
            if (frameworkElement != null)
            {
                //Check if its loaded
                if (!frameworkElement.IsLoaded)
                {
                    // Avoid multiple handlers
                    frameworkElement.Loaded -= CommandTarget_Loaded;
                    //Else attached loaded handler to get back when its loaded 
                    frameworkElement.Loaded += new RoutedEventHandler(CommandTarget_Loaded);
                }
                else
                {
                    //If object is load register PreviewkeyDown on Parent window
                    RegisterAssociatedWindow(frameworkElement);
                    SetFocus(frameworkElement);
                }
                // Avoid multiple handlers
                frameworkElement.Unloaded -= CommandTarget_Unloaded;
                // attach unload handler to unregister PreviewkeyDown on Parent window, when task completes
                frameworkElement.Unloaded += new RoutedEventHandler(CommandTarget_Unloaded);
            }
        }

        /// <summary>
        /// Called when the behavior is being detached from its AssociatedObject, but before it has actually occurred.
        /// </summary>
        /// <remarks>Override this to unhook functionality from the AssociatedObject.</remarks>
        protected override void OnDetaching()
        {
            var frameworkElement = this.AssociatedObject as FrameworkElement;
            if (frameworkElement != null)
            {
                //unregister PreviewkeyDown on Parent window, when task completes
                UnregisterAssociatedWindow(frameworkElement);
            }
            base.OnDetaching();
        }

        #region Registration Helper Methods
        private bool RegisterAssociatedWindow(FrameworkElement commandTarget)
        {
            if (commandTarget != null)
            {
                // Keep window reference to unregister PreviewKeyDown, in Unloaded method
                window = Window.GetWindow(commandTarget);
                if (window != null)
                {
                    lock (associatedWindowsLock)
                    {
                        window.PreviewKeyDown -= Window_PreviewKeyDown;
                        //Register PreviewkeyDown on Parent window
                        window.PreviewKeyDown += Window_PreviewKeyDown;
                    }
                }
                return (window != null);
            }
            else
            {
                return false;
            }
        }

        private void UnregisterAssociatedWindow(FrameworkElement commandTarget)
        {
            if (commandTarget != null)
            {
                if (window != null)
                {
                    lock (associatedWindowsLock)
                    {
                        //unregister PreviewkeyDown on Parent window, when task completes
                        window.PreviewKeyDown -= Window_PreviewKeyDown;
                        window = null;
                    }
                }
            }
        }
        #endregion

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            Window window = sender as Window;
            if (window != null)
            {
                if (this.Command != null)
                {
                    object commandParameter = this.CommandParameter;

                    if (this.AlwaysPassKeyEventArgsAsCommandParameter)
                    {
                        commandParameter = EventArgsConverter == null ? e : EventArgsConverter.Convert(e, null);
                    }

                    if (this.Command.CanExecute(commandParameter))
                    {
                        this.Command.Execute(commandParameter);
                    }
                }
            }
        }

        private void SetFocus(FrameworkElement frameworkElement)
        {
            frameworkElement.Focus();
            Keyboard.Focus(frameworkElement);
        }

        /// <summary>
        /// Gets or sets a converter used to convert the EventArgs when using
        /// <see cref="PassEventArgsToCommand"/>. If PassEventArgsToCommand is false,
        /// this property is never used.
        /// </summary>
        public IEventArgsConverter EventArgsConverter
        {
            get;
            set;
        }

        #region Dependency Properties
        public ICommand Command
        {
            get
            {
                return (ICommand)GetValue(CommandProperty);
            }
            set
            {
                SetValue(CommandProperty, value);
            }
        }
        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register("Command", typeof(ICommand), typeof(InvokeKeyboardShortcutsBehavior), new UIPropertyMetadata(null));

        public object CommandParameter
        {
            get
            {
                return (object)GetValue(CommandParameterProperty);
            }
            set
            {
                SetValue(CommandParameterProperty, value);
            }
        }
        public static readonly DependencyProperty CommandParameterProperty = DependencyProperty.Register("CommandParameter", typeof(object), typeof(InvokeKeyboardShortcutsBehavior), new UIPropertyMetadata(null));

        /// <summary>
        /// Gets or sets a value indicating whether to always pass KeyEventArgs as the command parameter.
        /// </summary>
        /// <value>A boolean.</value>
        public bool AlwaysPassKeyEventArgsAsCommandParameter
        {
            get
            {
                return (bool)GetValue(AlwaysPassKeyEventArgsAsCommandParameterProperty);
            }
            set
            {
                SetValue(AlwaysPassKeyEventArgsAsCommandParameterProperty, value);
            }
        }
        /// <summary>
        /// Identifies the AlwaysPassKeyEventArgsAsCommandParameter property.
        /// </summary>
        public static readonly DependencyProperty AlwaysPassKeyEventArgsAsCommandParameterProperty = DependencyProperty.Register("AlwaysPassKeyEventArgsAsCommandParameter", typeof(bool), typeof(InvokeKeyboardShortcutsBehavior), new UIPropertyMetadata(false));
        #endregion
    }

    /// <summary>
    /// Sets the focus to the specified Target.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class SetPropertyAction : TargetedTriggerAction<UIElement>
    {
        #region Properties
        #region PropertyName

        /// <summary>
        /// Property that is being set by this setter.
        /// </summary>
        public string PropertyName
        {
            get
            {
                return (string)GetValue(PropertyNameProperty);
            }
            set
            {
                SetValue(PropertyNameProperty, value);
            }
        }

        public static readonly DependencyProperty PropertyNameProperty =
            DependencyProperty.Register("PropertyName", typeof(string), typeof(SetPropertyAction),
            new PropertyMetadata(String.Empty));

        #endregion

        #region Value

        /// <summary>
        /// Property value that is being set by this setter.
        /// </summary>
        public object Value
        {
            get
            {
                return (object)GetValue(ValueProperty);
            }
            set
            {
                SetValue(ValueProperty, value);
            }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(object), typeof(SetPropertyAction),
            new PropertyMetadata(null));

        #endregion
        #endregion

        #region Overrides

        protected override void Invoke(object parameter)
        {
            var target = TargetObject ?? AssociatedObject;

            var targetType = target.GetType();

            var property = targetType.GetProperty(PropertyName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance);
            if (property == null)
                throw new ArgumentException(String.Format("Property not found: {0}", PropertyName));

            if (property.CanWrite == false)
                throw new ArgumentException(String.Format("Property is not settable: {0}", PropertyName));

            object convertedValue;

            if (Value == null)
                convertedValue = null;

            else
            {
                var valueType = Value.GetType();
                var propertyType = property.PropertyType;

                if (valueType == propertyType)
                    convertedValue = Value;

                else
                {
                    var propertyConverter = TypeDescriptor.GetConverter(propertyType);

                    if (propertyConverter.CanConvertFrom(valueType))
                        convertedValue = propertyConverter.ConvertFrom(Value);

                    else if (valueType.IsSubclassOf(propertyType))
                        convertedValue = Value;

                    else
                        throw new ArgumentException(String.Format("Cannot convert type '{0}' to '{1}'.", valueType, propertyType));
                }
            }

            property.SetValue(target, convertedValue);
        }

        #endregion
    }
}
