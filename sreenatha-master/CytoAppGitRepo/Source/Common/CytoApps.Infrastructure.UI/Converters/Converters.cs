﻿using CytoApps.Infrastructure.UI.Events;
using CytoApps.Infrastructure.UI.Utilities;
using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace CytoApps.Infrastructure.UI.Converters
{
    public class BoolToVisibiltyConverter : IValueConverter
    {
        public Visibility ValueWhenTrue { get; set; }
        public Visibility ValueWhenFalse { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is bool))
            {
                return ValueWhenFalse;
            }

            bool visible = (bool)value;
            return visible ? ValueWhenTrue : ValueWhenFalse;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(int), typeof(Visibility))]
    public class IntToVisibilityConverter : IValueConverter
    {
        private Visibility valueWhenZero = Visibility.Visible;
        private Visibility valueWhenNonZero = Visibility.Collapsed;

        public Visibility ValueWhenZero
        {
            get { return valueWhenZero; }
            set { valueWhenZero = value; }
        }

        public Visibility ValueWhenNonZero
        {
            get { return valueWhenNonZero; }
            set { valueWhenNonZero = value; }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((int)value == 0) ? valueWhenZero : valueWhenNonZero;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class AdditionConverter : IValueConverter
    {
        public double NumberToAdd { get; set; }

        public AdditionConverter()
        {
            NumberToAdd = 1;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is int)
            {
                return (int)((int)value + NumberToAdd);
            }
            else if (value is float)
            {
                return (float)((float)value + NumberToAdd);
            }
            else if (value is double)
            {
                return (double)((double)value + NumberToAdd);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ColorStringToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is string))
            {
                return null;
            }
            var color = (Color)ColorConverter.ConvertFromString(value as string);
            return new SolidColorBrush(color);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class NullToBoolConverter : IValueConverter
    {
        public bool ValueWhenNull { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value == null ? ValueWhenNull : !ValueWhenNull;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class NullToVisibilityConverter : IValueConverter
    {
        public Visibility ValueWhenNull { get; set; }
        public Visibility ValueWhenNotNull { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value == null ? ValueWhenNull : ValueWhenNotNull;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(bool), typeof(bool))]
    public class BoolInvertConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !((bool)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !((bool)value);
        }
    }


    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo language)
        {
            return (value is bool && (bool)value) ? Visibility.Visible : Visibility.Collapsed;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo language)
        {
            return value is Visibility && (Visibility)value == Visibility.Visible;
        }
    }

    public class DoubleConstantMultiplierConverter : IValueConverter
    {
        public double Multiplier { get; set; }

        public DoubleConstantMultiplierConverter()
        {
            Multiplier = 1;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is int)
            {
                return ((int)value) * Multiplier;
            }
            else if (value is float)
            {
                return ((float)value) * Multiplier;
            }
            else
            {
                return ((double)value) * Multiplier;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class CenterConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (double)value / 2;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class HeightConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (double)value / 4;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class InvertConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return -(double)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class InvertVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (Visibility)value == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class AnimatedTemplateSelector : DataTemplateSelector
    {
        public DataTemplate OutDataTemplate { get; set; }
        public DataTemplate InDataTemplate { get; set; }
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {

            if (item != null && (bool)item)
            {
                return InDataTemplate;
            }
            else
            {
                return OutDataTemplate;
            }
        }
    }

    public class ColorToBrushConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Color color = (Color)value;
            return new SolidColorBrush(color);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }   

    public class StringFormatConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values != null && !values.Any(a => a == DependencyProperty.UnsetValue) && values.Length > 0)
            {
                return string.Format("{0} ({1})", values[0], values[1]);
            }
            else
            {
                return null;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(DateTime), typeof(double))]
    public class DateTimeDoubleConverter : IValueConverter
    {
        /// <summary>
        /// Converts a DateTime Value to a Double Value using the Ticks of the DateTime instance.
        /// </summary>
        /// <param name="value">Instance of the DateTime class.</param>
        /// <param name="targetType">Target Type, which should be a Double.</param>
        /// <param name="parameter">Parameter used in the conversion.</param>
        /// <param name="culture">Globalization culture instance.</param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DateTime dt = DateTime.Parse(value.ToString());
            return dt.Ticks;
        }

        /// <summary>
        /// Converts a Double Value to a DateTime Value assuming the Double represents the amount of Ticks for a DateTime instance.
        /// </summary>
        /// <param name="value">Instance of the Double Class.</param>
        /// <param name="targetType">Target Type, which should be a DateTime</param>
        /// <param name="parameter">Parameter used in the conversion.</param>
        /// <param name="culture">Globalization culture instance.</param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double d = double.Parse(value.ToString());
            return new DateTime((long)d);
        }
    }

    [ValueConversion(typeof(TimeSpan), typeof(double))]
    public class TimeSpanToDoubleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            TimeSpan givenValue = (TimeSpan)value;
            return givenValue.Ticks;
        }

        public object ConvertBack(object value, Type targetType,
           object parameter, System.Globalization.CultureInfo culture)
        {
            return new TimeSpan(((long)value));
        }
    }


    public class ShutdownEventArgsConverter : IEventArgsConverter
    {
        public object Convert(object value, object parameter)
        {
            ShutDownEventArgs ShutDownEventArgs = new ShutDownEventArgs();
            ShutDownEventArgs.CancelEventArgs = (CancelEventArgs)value;
            return ShutDownEventArgs;
        }      
    }
}
