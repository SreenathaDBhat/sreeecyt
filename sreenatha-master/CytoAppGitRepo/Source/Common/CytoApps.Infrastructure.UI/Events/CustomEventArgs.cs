﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CytoApps.Infrastructure.UI.Events
{
    public class ShutDownEventArgs
    {

        public CancelEventArgs CancelEventArgs { get; set; }
        public bool CanClose { get; set; }
    }
}
