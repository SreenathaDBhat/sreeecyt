﻿using CytoApps.Models;
using Prism.Events;

namespace CytoApps.Infrastructure.UI.Events
{
    public class SelectedCaseSlidesEvent : PubSubEvent<CaseDataProxy> { }
    /// <summary>
    /// Selected case event
    /// </summary>
    public class RefreshSelectedCaseEvent : PubSubEvent<object>
    {
    }

    /// <summary>
    /// Data souce deleted event
    /// </summary>
    public class DataSourceDeletedEvent : PubSubEvent<DataSource>
    {
    }
}
