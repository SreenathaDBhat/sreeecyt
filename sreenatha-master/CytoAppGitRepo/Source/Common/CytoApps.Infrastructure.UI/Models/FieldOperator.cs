﻿
namespace CytoApps.Infrastructure.UI.Models
{
    /// <summary>
    ///  Filter operator for date and numeric fields
    /// </summary>
    public class FieldOperator
    {
        #region Properties
        public string SqlOperator
        {
            get;
            private set;
        }
        public string DisplayOperator
        {
            get;
            private set;
        }
        public string Name
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        public FieldOperator(string name, string sql, string display)
        {
            Name = name;
            SqlOperator = sql;
            DisplayOperator = display;
        }
        public override string ToString()
        {
            return Name;
        }
        //this is needed because when we bind the operator from filter to combobox of list of operattors, the equality is checked based on name.
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is FieldOperator))
                return false;

            return (((FieldOperator)obj).Name == this.Name);
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }
        #endregion
    }
}
