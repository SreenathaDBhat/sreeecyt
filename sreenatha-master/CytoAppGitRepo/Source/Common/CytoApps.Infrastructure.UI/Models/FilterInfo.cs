﻿using CytoApps.Models;
using Prism.Mvvm;
using System;

namespace CytoApps.Infrastructure.UI.Models
{
    /// <summary>
    /// FilterInfo class is a wrapper class at application layer for Filter class. We also need to store some of 
    /// the atributes in this class to application class hence it is marked Serializable.
    /// </summary>
    [Serializable]
    public class FilterInfo : BindableBase
    {
        #region Constructor
        public object Clone()
        {
            return MemberwiseClone();
        }

        public FilterInfo()
        {
        }

        #endregion
        #region Properties

        private string _filedName;
        public string FieldName
        {
            get { return _filedName; }
            set
            {
                _filedName = value;
                OnPropertyChanged("FieldName");
            }
        }

        private string _value = string.Empty;
        public string Value
        {

            get { return _value; }
            set
            {
                _value = value;
                OnPropertyChanged("Value");
            }
        }
        private bool isCaseTableField = false;
        public bool IsCaseTableField
        {
            get
            {
                return isCaseTableField;
            }
            set
            {
                isCaseTableField = value;
                OnPropertyChanged("IsCaseTableField");
            }
        }

        [NonSerialized]
        private FieldOperator _operator;
        [System.Xml.Serialization.XmlIgnore]
        public FieldOperator Operator
        {
            get { return _operator; }
            set
            {
                _operator = value;
                if(_operator!= null)
                   StoredOpeartor = _operator.Name;
                OnPropertyChanged("Operator");
            }
        }

        private FieldTypeEnum _fieldType;
        public FieldTypeEnum FieldType
        {
            get { return _fieldType; }
            set
            {
                _fieldType = value;
                this.OnPropertyChanged("FieldType");
            }
        }

        [NonSerialized]
        private bool _hasErrors;
        [System.Xml.Serialization.XmlIgnore]
        public bool HasErrors
        {
            get { return _hasErrors; }
            set
            {
                _hasErrors = value;
                OnPropertyChanged("HasErrors");
            }
        }
        //this is to help in serializing to application settings.
        private string _storedOperator = string.Empty;
        public string StoredOpeartor {
            get { return _storedOperator; }
            set {
                _storedOperator = value;
            }
        }

        #endregion
    }
}
