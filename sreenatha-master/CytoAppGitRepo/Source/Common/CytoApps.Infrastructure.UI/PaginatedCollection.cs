﻿ using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Prism.Mvvm;

namespace CytoApps.Infrastructure.UI
{
    public sealed class PaginatedCollection<T> : BindableBase
    {
        private int _firstVisibleItemIndex;
        private int _numberItemsPerPage;
        private ICollection<T> _source;

        public IEnumerable<T> CurrentPage
        {
            get;
            private set;
        }

        /// <summary>
        /// INCLUSIVE
        /// </summary>
        public int LastVisibleItemIndex
        {
            get
            {
                return validateLastIndex(_firstVisibleItemIndex + _numberItemsPerPage - 1);
            }
        }

        public int FirstVisibleItemIndex
        {
            get { return _firstVisibleItemIndex; }
            set
            {
                _firstVisibleItemIndex = validateFirstItemIndex(value);
                UpdateVisibleItemsWholesale();
                OnPropertyChanged("FirstVisibleItemIndex");
                OnPropertyChanged("LastVisibleItemIndex");
            }
        }

        public int NumberOfItemsPerPage
        {
            get { return _numberItemsPerPage; }
            set
            {
                var obs = CurrentPage as ObservableCollection<T>;
                if (obs != null && value < NumberOfItemsPerPage) // fast path
                {
                    while (obs.Count > value)
                        obs.RemoveAt(obs.Count - 1);

                    _numberItemsPerPage = value;
                }
                else
                {
                    _numberItemsPerPage = value;
                    UpdateVisibleItemsWholesale();
                }

                OnPropertyChanged("NumberOfItemsPerPage");
                OnPropertyChanged("FirstVisibleItemIndex");
                OnPropertyChanged("LastVisibleItemIndex");
            }
        }

        public ICollection<T> Source
        {
            get { return _source; }
            set
            {
                _source = value;
                OnPropertyChanged("Source");
                UpdateVisibleItemsWholesale();
                OnPropertyChanged("FirstVisibleItemIndex");
                OnPropertyChanged("LastVisibleItemIndex");
            }
        }

        

        private void setCurrentPage(ObservableCollection<T> p)
        {
            CurrentPage = p;
            OnPropertyChanged("CurrentPage");
        }

        private void UpdateVisibleItemsWholesale()
        {
            var first = FirstVisibleItemIndex;
            var last = LastVisibleItemIndex;

            var items = _source.Skip((int)_firstVisibleItemIndex)
                              .Take((int)(last - first) + 1);

            setCurrentPage(new ObservableCollection<T>(items));
        }

        private int validateFirstItemIndex(int i)
        {
            //NO! int biggestIndexStillLeavingFullScreensWorth = source.Count - numberItemsPerPage;
            //    i = Math.Min(i, biggestIndexStillLeavingFullScreensWorth);
            // Allow any value in range even if it leaves just one item in the page
            // - this is so that when moving up and down columns, the selection doesn't jump
            // to a different column when the page changes.
            i = Math.Min(_source.Count - 1, i);
            i = Math.Max(0, i);
            return i;
        }

        private int validateLastIndex(int i)
        {
            i = Math.Max(0, i);
            i = Math.Min(_source.Count - 1, i);
            return i;
        }

        /// <summary>
        /// Perform an optimised nudge upward - better than a wholesale FirstVisibleIndex change because
        /// we can simply add or insert the new items rather than trashing everything and starting over.
        /// Good for the possibly databound UI.
        /// </summary>
        /// <param name="numberOfVisibleColumns"></param>
        public void RowUp(int numberOfVisibleColumns)
        {
            int newFirstIndex = _firstVisibleItemIndex - numberOfVisibleColumns;
            newFirstIndex = validateFirstItemIndex(newFirstIndex);

            ObservableCollection<T> liveItems = CurrentPage as ObservableCollection<T>;
            if (liveItems == null)
            {
                liveItems = new ObservableCollection<T>();
            }

            for (int i = _firstVisibleItemIndex - 1; i >= newFirstIndex; --i)
            {
                liveItems.Insert(0, _source.ElementAt(i));
            }

            while (liveItems.Count > _numberItemsPerPage)
            {
                liveItems.RemoveAt(liveItems.Count - 1);
            }

            _firstVisibleItemIndex = newFirstIndex;
            OnPropertyChanged("FirstVisibleItemIndex");
            OnPropertyChanged("LastVisibleItemIndex");

            if (liveItems != CurrentPage)
            {
                CurrentPage = liveItems;
                OnPropertyChanged("CurrentPage");
            }

            // should we notify CurrentPage here or rely on clients being clued up enough to expect a notifying collection?
        }

        public void RowDown(int numberOfVisibleColumns)
        {
            int newFirstIndex = validateFirstItemIndex(_firstVisibleItemIndex + numberOfVisibleColumns);
            if (newFirstIndex < _firstVisibleItemIndex + numberOfVisibleColumns) // Already on last row.
                return;

            ObservableCollection<T> liveItems = CurrentPage as ObservableCollection<T>;
            if (liveItems == null)
            {
                liveItems = new ObservableCollection<T>();
            }
            else if (liveItems.Count > 0)
            {
                // remove items that scroll off the bottom of the screen
                int numberOfItemsToRemove = newFirstIndex - _firstVisibleItemIndex;
                for (int i = 0; i < numberOfItemsToRemove; ++i)
                    liveItems.RemoveAt(0);
            }

            int newLast = validateLastIndex(newFirstIndex + _numberItemsPerPage - 1);
            for (int i = LastVisibleItemIndex + 1; i <= newLast; ++i)
            {
                liveItems.Add(_source.ElementAt(i));
            }


            _firstVisibleItemIndex = newFirstIndex;
            OnPropertyChanged("FirstVisibleItemIndex");
            OnPropertyChanged("LastVisibleItemIndex");

            if (liveItems != CurrentPage)
            {
                CurrentPage = liveItems;
                OnPropertyChanged("CurrentPage");
            }
        }
    }
}