﻿using Microsoft.Practices.ServiceLocation;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CytoApps.Infrastructure.UI
{
    /// <summary>
    /// this utility Class that provides common methods which helps in view navigation. and holds the current plugin region and clears the same
    /// To use one can call: RegionManagerExtension.DeactivateViewFromRegion(RegionNames.MainRegion,typeof(CaseBrwoserWorkspaceView));
    /// </summary>
    public sealed class RegionManagerExtension
    {
        private static IRegionManager _regionManager;
        static RegionManagerExtension()
        {
            _regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();
        }

        /// <summary>
        /// Clear navigated view from the region
        /// </summary>
        /// <param name="regionName">Region Name</param>
        public static void ClearNavigatedViewsFromRegion(string regionName)
        {
            if (_regionManager.Regions[regionName] != null)
            {
                List<object> views = new List<object>(_regionManager.Regions[regionName].Views);
                foreach (var view in views)
                {
                    _regionManager.Regions[regionName].Remove(view);
                }
            }
        }

        /// <summary>
        /// Deactivate views from the region
        /// </summary>
        /// <param name="regionName">Region Name</param>
        /// <param name="view">View class name</param>
        public static void DeactivateViewFromRegion(string regionName, Type view)
        {
            if (_regionManager.Regions[regionName] != null)
            {
                IList<object> Views = new List<object>(_regionManager.Regions[regionName].Views);
                var deactivateView = Views.Where(deactivateview => deactivateview.GetType() == view).FirstOrDefault();
                if (deactivateView != null)
                {
                    _regionManager.Regions[regionName].Deactivate(deactivateView);
                }
            }
        }

        /// <summary>
        /// Check View exists in the region before activating or deactivating the view
        /// </summary>
        /// <param name="regionName">Region Name</param>
        /// <param name="view">View class name</param>
        /// <returns>bool</returns>
        public static bool CheckViewExistsInRegion(string regionName, Type view)
        {
            bool viewExists = false;
            IList<object> Views = new List<object>();
            foreach (var item in _regionManager.Regions[regionName].Views)
            {
                if (item.GetType() == view)
                {
                    viewExists = true;
                    return viewExists;
                }
            }
            return viewExists;
        }

        /// <summary>
        ///  Activate existing view in the region
        /// </summary>
        /// <param name="regionName">Region Name</param>
        /// <param name="view">View class name</param>
        public static void ActivateViewInRegion(string regionName, Type view)
        {
            if (_regionManager.Regions[regionName] != null)
            {
                IList<object> Views = new List<object>(_regionManager.Regions[regionName].Views);
                var activateView = Views.Where(activateview => activateview.GetType() == view).FirstOrDefault();
                if (activateView != null)
                {
                    _regionManager.Regions[regionName].Activate(activateView);
                }
            }
        }

        /// <summary>
        /// List plugin region name
        /// </summary>
        public static List<string> PluginsRegions
        {
            get;
            private set;
        }

        /// <summary>
        /// Holds plugin region names
        /// </summary>
        /// <param name="pluginRegionName"></param>
        public static void PluggedRegions(string pluginRegionName)
        {
            if (PluginsRegions == null)
                PluginsRegions = new List<string>();
            PluginsRegions.Add(pluginRegionName);
        }

        /// <summary>
        /// Clears plugin region from the regionManager
        /// </summary>
        public static void ClearPluggedRegions()
        {
            if (PluginsRegions != null)
            {
                foreach (var pluginRegion in PluginsRegions)
                {
                    _regionManager.Regions.Remove(pluginRegion);
                }
                PluginsRegions.Clear();
            }
        }
    }
}
