﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CytoApps.Infrastructure.UI
{
    public class RegionNames
    {
        public static string ToolBarRegion = "ToolBarRegion";
        public static string MainRegion = "MainRegion";
        public static string StatusBarRegion = "StatusBarRegion";

        // CaseBrowserModule Regions
        public static string CaseBrowserRegion = "CaseBrowserRegion";
        public static string CaseDetailsRegion = "CaseDetailsRegion";
        public static string SlidesRegion = "SlidesRegion";
        public static string PluginRegion = "PluginRegion";
        public static string BrandRegion = "BrandRegion";
    }
}
