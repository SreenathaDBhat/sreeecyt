﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CytoApps.Infrastructure.UI.Utilities
{
    /// <summary>
    /// Useful C# snippets from CambiaResearch.com
    /// </summary>
    public class ColorParse
    {

        public ColorParse()
        {
        }

        /// <summary>
        /// Convert a hex string to a .NET Color object.
        /// </summary>
        /// <param name="hexColor">a hex string: "#FFFFFF", "#000000"</param>
        public static Color HexStringToColor(string hexColor)
        {
            Color c = Colors.Black;
            if (TryParse(hexColor, out c))
                return c;
            else
                throw new ArgumentException();
        }

        /// <summary>
        /// Convert a hex string to a .NET Color object.
        /// </summary>
        /// <param name="hexColor">a hex string: "#FFFFFF", "#000000"</param>
        public static bool TryParse(string hexColor, out Color result)
        {
            result = Colors.Black;

            string hc = ExtractHexDigits(hexColor);
            if (hc.Length != 6)
            {
                return false;
            }
            string r = hc.Substring(0, 2);
            string g = hc.Substring(2, 2);
            string b = hc.Substring(4, 2);
            Color color = Colors.Transparent;
            try
            {
                int ri = 0;
                int gi = 0;
                int bi = 0;

                if (!int.TryParse(r, System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out ri))
                    return false;

                if (!int.TryParse(g, System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out gi))
                    return false;

                if (!int.TryParse(b, System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out bi))
                    return false;

                color = Color.FromArgb(255, (byte)ri, (byte)gi, (byte)bi);
            }
            catch
            {
                return false;
            }

            result = color;
            return true;
        }
        /// <summary>
        /// Extract only the hex digits from a string.
        /// </summary>
        public static string ExtractHexDigits(string input)
        {
            // remove any characters that are not digits (like #)
            Regex isHexDigit
               = new Regex("[abcdefABCDEF\\d]+", RegexOptions.Compiled);
            string newnum = "";
            foreach (char c in input)
            {
                if (isHexDigit.IsMatch(c.ToString()))
                    newnum += c.ToString();
            }
            return newnum;
        }


        public static string ToHexString(Color color)
        {
            return string.Format("#{0:x2}{1:x2}{2:x2}", color.R, color.G, color.B);
        }

        /// <summary>
        /// Creates color with corrected brightness.
        /// </summary>
        /// <param name="color">Color to correct.</param>
        /// <param name="correctionFactor">The brightness correction factor. Must be between -1 and 1. 
        /// Negative values produce darker colors.</param>
        /// <returns>
        /// Corrected <see cref="Color"/> structure.
        /// </returns>
        public static Color ChangeColorBrightness(Color color, float correctionFactor)
        {
            float red = (float)color.R;
            float green = (float)color.G;
            float blue = (float)color.B;

            if (correctionFactor < 0)
            {
                correctionFactor = 1 + correctionFactor;
                red *= correctionFactor;
                green *= correctionFactor;
                blue *= correctionFactor;
            }
            else
            {
                red = (255 - red) * correctionFactor + red;
                green = (255 - green) * correctionFactor + green;
                blue = (255 - blue) * correctionFactor + blue;
            }

            return Color.FromArgb(color.A, (byte)red, (byte)green, (byte)blue);
        }
    }
}
