﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CytoApps.Infrastructure.UI.Utilities
{
    public class DesignTimeBindableBase : BindableBase
    {
        /// <summary>
        /// Are we in design mode, stole of mvvmlite, quite useful
        /// </summary>
        private bool? _isInDesignMode;
        public bool IsInDesignMode
        {
            get
            {
                if (!_isInDesignMode.HasValue)
                {
                    var prop = System.ComponentModel.DesignerProperties.IsInDesignModeProperty;
                    _isInDesignMode =
                        (bool)System.ComponentModel.DependencyPropertyDescriptor
                        .FromProperty(prop, typeof(FrameworkElement))
                        .Metadata.DefaultValue;
                }

                return _isInDesignMode.Value;
            }
        }
    }
}
