﻿using System.Windows;

namespace CytoApps.Infrastructure.UI.Utilities
{
    /// <summary>
    /// This Class is used find actual width and actual height of respective element and aslo size of the UI Element.
    /// To use this class you need to set the Observe value to true and bind a property in the view set Mode to OneWayToSource & set the same in ViewModel with INotifyProperty implemented.
    /// Ready to use Note: You will get both Width,Height and Size(width,height)
    /// </summary>
    public static class SizeObserver
    {
        public static readonly DependencyProperty ObserveProperty = DependencyProperty.RegisterAttached(
        "Observe",
        typeof(bool),
        typeof(SizeObserver),
        new FrameworkPropertyMetadata(OnObserveChanged));

        public static readonly DependencyProperty ObservedWidthProperty = DependencyProperty.RegisterAttached(
            "ObservedWidth",
            typeof(double),
            typeof(SizeObserver));

        public static readonly DependencyProperty ObservedHeightProperty = DependencyProperty.RegisterAttached(
            "ObservedHeight",
            typeof(double),
            typeof(SizeObserver));

        public static readonly DependencyProperty ObservedSizeProperty = DependencyProperty.RegisterAttached(
            "ObservedSize",
            typeof(Size),
            typeof(SizeObserver),
            new PropertyMetadata(Size.Empty));

        public static bool GetObservedSize(FrameworkElement frameworkElement)
        {
            return (bool)frameworkElement.GetValue(ObservedSizeProperty);
        }

        public static void SetObservedSize(FrameworkElement frameworkElement, Size observedSize)
        {
            frameworkElement.SetValue(ObservedSizeProperty, observedSize);
        }

        public static bool GetObserve(FrameworkElement frameworkElement)
        {
            return (bool)frameworkElement.GetValue(ObserveProperty);
        }

        public static void SetObserve(FrameworkElement frameworkElement, bool observe)
        {
            frameworkElement.SetValue(ObserveProperty, observe);
        }

        public static double GetObservedWidth(FrameworkElement frameworkElement)
        {
            return (double)frameworkElement.GetValue(ObservedWidthProperty);
        }

        public static void SetObservedWidth(FrameworkElement frameworkElement, double observedWidth)
        {
            frameworkElement.SetValue(ObservedWidthProperty, observedWidth);
        }

        public static double GetObservedHeight(FrameworkElement frameworkElement)
        {
            return (double)frameworkElement.GetValue(ObservedHeightProperty);
        }

        public static void SetObservedHeight(FrameworkElement frameworkElement, double observedHeight)
        {
            frameworkElement.SetValue(ObservedHeightProperty, observedHeight);
        }

        /// <summary>
        /// Check wheather observe property is set to true
        /// </summary>
        /// <param name="dependencyObject">Framework Element(UI Element)</param>
        /// <param name="e">Value(True/False)</param>
        private static void OnObserveChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var frameworkElement = (FrameworkElement)dependencyObject;

            if ((bool)e.NewValue)
            {
                frameworkElement.SizeChanged += OnFrameworkElementSizeChanged;
                frameworkElement.Loaded += FrameworkElement_Loaded;
                UpdateObservedSizesForFrameworkElement(frameworkElement);
            }
            else
            {
                frameworkElement.SizeChanged -= OnFrameworkElementSizeChanged;
                frameworkElement.Loaded -= FrameworkElement_Loaded;
            }
        }

        /// <summary>
        /// Update Height and Width resepective to framework element on UIelement load
        /// </summary>
        /// <param name="sender">UIElement</param>
        /// <param name="e"></param>
        private static void FrameworkElement_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateObservedSizesForFrameworkElement((FrameworkElement)sender);
        }

        /// <summary>
        /// Update Height and Width resepective to framework element
        /// </summary>
        /// <param name="sender">UIElement</param>
        /// <param name="e"></param>
        private static void OnFrameworkElementSizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateObservedSizesForFrameworkElement((FrameworkElement)sender);
        }

        private static void UpdateObservedSizesForFrameworkElement(FrameworkElement frameworkElement)
        {
            // WPF 4.0 onwards
            frameworkElement.SetCurrentValue(ObservedWidthProperty, frameworkElement.ActualWidth);
            frameworkElement.SetCurrentValue(ObservedHeightProperty, frameworkElement.ActualHeight);
            frameworkElement.SetCurrentValue(ObservedSizeProperty, new Size(frameworkElement.ActualWidth, frameworkElement.ActualHeight));

            // WPF 3.5 and prior
            ////SetObservedWidth(frameworkElement, frameworkElement.ActualWidth);
            ////SetObservedHeight(frameworkElement, frameworkElement.ActualHeight);
        }
    }
}
