﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;

namespace CytoApps.Infrastructure.UI.Utilities
{
    /// <summary>
    /// Visual Tree Walker class is used to find parent/child dependency object.
    /// ex:VisualTreeWalker.FindParentOfType<TranslatingScalePanel>(sender) is used to find the parent of the dependency object sender.
    /// </summary>
    public sealed class VisualTreeWalker
    {
        private VisualTreeWalker()
        {
        }

        public static T FindChildOfType<T>(DependencyObject parent) where T : DependencyObject
        {
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; ++i)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                if (child.GetType() == typeof(T))
                    return (T)child;

                T foundChild = FindChildOfType<T>(child);
                if (foundChild != null)
                    return foundChild;
            }

            return default(T);
        }

        public static T FindChildOfType<T>(DependencyObject parent, string elementName) where T : FrameworkElement
        {
            return FindChildOfType<T>(parent, elementName, null);
        }

        public static T FindChildOfType<T>(DependencyObject parent, string elementName, object tag) where T : FrameworkElement
        {
            int count = VisualTreeHelper.GetChildrenCount(parent);
            var type = typeof(T);

            for (int i = 0; i < count; ++i)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                T ch = child as T;
                if (ch != null)
                {

                    bool ismatch = true;
                    if (tag != null)
                    {
                        if (tag != ch.Tag)
                            ismatch = false;
                    }

                    string name = (string)ch.GetValue(Control.NameProperty);
                    if (name != elementName)
                        ismatch = false;

                    if (ismatch)
                        return ch;
                }

                T foundChild = FindChildOfType<T>(child, elementName, tag);
                if (foundChild != null)
                    return foundChild;
            }

            return default(T);
        }

        public static T FindParentOfType<T>(DependencyObject obj) where T : DependencyObject
        {
            DependencyObject parent = VisualTreeHelper.GetParent(obj);
            while (parent != null)
            {
                if (parent.GetType() == typeof(T) || parent.GetType().IsSubclassOf(typeof(T)))
                    return (T)parent;
                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return null;
        }

        public static T FindParentOfType<T>(DependencyObject obj, string elementName) where T : DependencyObject
        {
            if (obj == null)
            {
                return null;
            }

            DependencyObject parent = VisualTreeHelper.GetParent(obj);
            if (parent is T && (string)parent.GetValue(Control.NameProperty) == elementName)
                return (T)parent;

            while (parent != null)
            {
                string name = (string)parent.GetValue(Control.NameProperty);

                if ((parent.GetType() == typeof(T) || parent.GetType().IsSubclassOf(typeof(T))) && name == elementName)
                {
                    return (T)parent;
                }
                else
                {
                    parent = VisualTreeHelper.GetParent(parent);
                }
            }

            return null;
        }
    }
}
