﻿using System.Linq;
using System.Windows;

namespace CytoApps.Infrastructure.UI.Utilities
{
    public static class WindowScreenBehavior
    {
        public static void MaximizeToSecondaryMonitor(this Window window)
        {
            var secondaryScreen = System.Windows.Forms.Screen.AllScreens.Where(s => !s.Primary).FirstOrDefault();
            if (secondaryScreen != null)
            {
                if (!window.IsLoaded)
                    window.WindowStartupLocation = WindowStartupLocation.Manual;

                var workingArea = secondaryScreen.WorkingArea;
                window.Left = workingArea.Left;
                window.Top = workingArea.Top;
                window.Width = workingArea.Width;
                window.Height = workingArea.Height;
                //// If window isn't loaded then maxmizing will result in the window displaying on the primary monitor
                if (window.IsLoaded)
                    window.WindowState = WindowState.Maximized;
            }
            else
            {
                MaximizeToPrimaryMonitor(window);
            }
        }

        public static void MaximizeToPrimaryMonitor(this Window window)
        {
            var primaryScreen = System.Windows.Forms.Screen.AllScreens.Where(s => s.Primary).FirstOrDefault();
            if (primaryScreen != null)
            {
                if (!window.IsLoaded)
                    window.WindowStartupLocation = WindowStartupLocation.Manual;

                var workingArea = primaryScreen.WorkingArea;
                window.Left = workingArea.Left;
                window.Top = workingArea.Top;
                window.Width = workingArea.Width;
                window.Height = workingArea.Height;
                window.WindowState = WindowState.Maximized;
            }
        }
    }

    /// <summary>
    /// Maximize window in Dual Monitor if available
    /// </summary>
    public class MaximizeWindowsAlt
    {

        public static bool GetMaximizeWindow(Window window)
        {
            return (bool)window.GetValue(MaximizeWindowProperty);
        }

        public static void SetMaximizeWindow(Window window, bool value)
        {
            window.SetValue(MaximizeWindowProperty, value);
        }

        // Using a DependencyProperty as the backing store for MaximizeWindow.
        public static readonly DependencyProperty MaximizeWindowProperty =
            DependencyProperty.RegisterAttached("MaximizeWindow", typeof(bool), typeof(MaximizeWindowsAlt), new UIPropertyMetadata(false, OnWindowMaximizeChanged));

        private static void OnWindowMaximizeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var appWindow = d as Window;
            if (e.NewValue is bool == false)
                return;

            if ((bool)e.NewValue)
            {
                appWindow.IsVisibleChanged += AppWindowIsVisibleChanged;
            }
            else
            {
                appWindow.IsVisibleChanged -= AppWindowIsVisibleChanged;
            }
        }

        private static void AppWindowIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var senderWindow = sender as Window;
            if (senderWindow != null)
            {
                senderWindow.Owner = System.Windows.Application.Current.MainWindow;
                if (senderWindow.IsVisible)
                {
                    var screen = System.Windows.Forms.Screen.FromHandle(new System.Windows.Interop.WindowInteropHelper(senderWindow.Owner).Handle);
                    if (screen.Primary)
                    {
                        senderWindow.MaximizeToSecondaryMonitor();
                    }
                    else
                    {
                        senderWindow.MaximizeToPrimaryMonitor();
                    }
                }
            }
        }
    }
}
