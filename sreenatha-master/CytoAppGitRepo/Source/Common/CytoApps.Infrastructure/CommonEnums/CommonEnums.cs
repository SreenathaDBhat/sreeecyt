﻿namespace CytoApps.Infrastructure.CommonEnums
{
    /// <summary>
    /// Each item indicates Language selection
    /// </summary>
    public enum Languages
    {
        English = 1,
        German = 2,
        Chinese = 3
    }

    /// <summary>
    /// Each item indicates theme selection
    /// </summary>
    public enum Themes
    {
        LightTheme = 1,
        DarkTheme
    }

    public enum NavigationDirection
    {
        Up = 1,
        Down,
        Left,
        Right
    }

}
