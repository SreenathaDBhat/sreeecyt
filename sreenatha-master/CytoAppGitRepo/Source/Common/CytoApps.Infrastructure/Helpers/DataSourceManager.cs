﻿using CytoApps.Models;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml.Linq;

/// <summary>
/// This is a singleton class making the single instance available to all the modules
/// This class encapsulates all the data source functionalities such as create/delete/update/read/save etc.
/// </summary>
namespace CytoApps.Infrastructure.Helpers
{
    public sealed class DataSourceManager
    {
        #region Private members
        private static readonly DataSourceManager instance = new DataSourceManager();
        private const string _folderName = "\\Leica";
        private static string _dataSourceFilePath;
        private FileInfo _dataSourceFile;
        #endregion

        #region Constructor
        DataSourceManager()
        {
            _dataSourceFilePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + _folderName + "\\Datasource.xml";
            _dataSourceFile = new FileInfo(_dataSourceFilePath);
            ReadDataSources();
        }
        #endregion

        #region Properties
        public static DataSourceManager Instance
        {
            get
            {
                return instance;
            }
        }

        private ObservableCollection<DataSource> _dataSourceCollection;
        public ObservableCollection<DataSource> DataSourceCollection
        {
            get
            {
                if (_dataSourceCollection == null)
                {
                    _dataSourceCollection = new ObservableCollection<DataSource>();
                }
                return _dataSourceCollection;
            }
            set
            {
                _dataSourceCollection = value;
            }
        }
        #endregion

        #region private methods
        private ObservableCollection<DataSource> ReadFromXML()
        {
            if (_dataSourceFile.Exists)
            {
                XElement dataSourceXML = XElement.Load(_dataSourceFile.FullName);
                return new ObservableCollection<DataSource>(from dataSourceNode in dataSourceXML.Elements("DataSource")
                                                            select ReadDataSourceNodeFromXML(dataSourceNode));
            }
            else
            {
                return new ObservableCollection<DataSource>();
            }
        }

        private static DataSource ReadDataSourceNodeFromXML(XElement dataSourceNode)
        {
            DataSource dataSourceItem = new DataSource();
            dataSourceItem.ID = new Guid(dataSourceNode.Attribute("id").Value);
            dataSourceItem.Name = dataSourceNode.Attribute("name").Value;
            dataSourceItem.DataSourceType = dataSourceNode.Attribute("type").Value;
            dataSourceItem.IsActive = Convert.ToBoolean(dataSourceNode.Attribute("IsActive").Value);
            foreach (XElement parameter in dataSourceNode.Element("ConnectionParameter").Elements())
            {
                dataSourceItem.ConnectionParameter.Add(new ConnectionParameter()
                {
                    Key = parameter.Name.LocalName,
                    Value = parameter.Value
                });
            }
            return dataSourceItem;
        }

        private void SaveToXML(ObservableCollection<DataSource> DataSourceCollection)
        {
            var dataSourceXML = new XElement("DataSources", from dataSourceItem in DataSourceCollection
                                                            select SaveDataSourceItemToXML(dataSourceItem));
            _dataSourceFile.Refresh();
            if (_dataSourceFile.Exists)
            {
                _dataSourceFile.Delete();
            }
            if (!_dataSourceFile.Directory.Exists)
            {
                Directory.CreateDirectory(_dataSourceFile.DirectoryName);
            }
            dataSourceXML.Save(_dataSourceFile.FullName);
        }

        private static XElement SaveDataSourceItemToXML(DataSource dataSourceItem)
        {
            XElement newDataSourceElement = new XElement("DataSource",
                                            new XElement("ConnectionParameter", from connectionParameterItem in dataSourceItem.ConnectionParameter
                                                                                select SaveDataSourceInfoItemToXML(connectionParameterItem)));
            newDataSourceElement.SetAttributeValue("id", dataSourceItem.ID.ToString());
            newDataSourceElement.SetAttributeValue("type", dataSourceItem.DataSourceType);
            newDataSourceElement.SetAttributeValue("name", dataSourceItem.Name);
            newDataSourceElement.SetAttributeValue("IsActive", dataSourceItem.IsActive.ToString());
            return newDataSourceElement;
        }

        private static XElement SaveDataSourceInfoItemToXML(ConnectionParameter connectionParameterItem)
        {
            return new XElement(connectionParameterItem.Key, connectionParameterItem.Value);
        }
        #endregion

        #region public methods
        public void DeleteDataSource(DataSource dataSourceItem)
        {
            DataSourceCollection.Remove(dataSourceItem);
            SaveToXML(DataSourceCollection);
        }

        public void SaveDataSource(DataSource dataSourceItem)
        {
            var currentDataSource = DataSourceCollection.Where(i => i.ID == dataSourceItem.ID).FirstOrDefault();
            if (currentDataSource != null)
            {
                DataSourceCollection.Remove(currentDataSource);
                DataSourceCollection.Add(dataSourceItem);
            }
            else
            {
                dataSourceItem.ID = Guid.NewGuid();
                DataSourceCollection.Add(dataSourceItem);
            }
            SaveToXML(DataSourceCollection);
        }

        public void ReadDataSources()
        {
            DataSourceCollection = ReadFromXML();
        }
        #endregion
    }
}
