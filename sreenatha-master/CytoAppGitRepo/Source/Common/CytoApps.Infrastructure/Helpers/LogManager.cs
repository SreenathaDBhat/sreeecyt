﻿using NLog;
using System;

namespace CytoApps.Infrastructure.Helpers
{
    public class LogManager
    {
        ///<summary>
        ///Logger
        /// </summary>
        private readonly static Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public static void Debug(string message)
        {
            if (!_logger.IsDebugEnabled)
            {
                return;
            }
            _logger.Debug(message);
        }
        public static void Error(string error, Exception exception = null)
        {
            if (!_logger.IsErrorEnabled)
            {
                return;
            }
            _logger.Error(exception, error);
        }
        public static void Fatal(Exception exception)
        {
            if (!_logger.IsFatalEnabled)
            {
                return;
            }
            _logger.Fatal(exception);
        }
        public static void Info(string message)
        {
            if (!_logger.IsInfoEnabled)
            {
                return;
            }
            _logger.Info(message);
        }
    }
}
