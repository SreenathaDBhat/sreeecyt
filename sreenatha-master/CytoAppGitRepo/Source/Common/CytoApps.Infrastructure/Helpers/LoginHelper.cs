﻿using CytoApps.Models;
using System;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

namespace CytoApps.Infrastructure.Helpers
{
    public static class LoginHelper
    {
        public static UserDetails GetValidUser()
        {
            UserDetails activeUser = new UserDetails();

            // HACK my machine isn't domained, do this otherwise DirectorySearcher takes ages to fail
            try
            {
                System.DirectoryServices.ActiveDirectory.Domain.GetComputerDomain();
            }
            catch (System.DirectoryServices.ActiveDirectory.ActiveDirectoryObjectNotFoundException)
            {
                activeUser.FirstName = Environment.UserName;
                activeUser.LastName = Environment.UserName;
                activeUser.UserID = Environment.UserName;
                activeUser.MachineName = Environment.MachineName;
                return activeUser;
            }

            try
            {
                DirectoryEntry ObjDirectory = new DirectoryEntry(Constants.ActiveDirectoryLink + Environment.UserDomainName.ToLower());
                if (ObjDirectory != null)
                {
                    using (DirectorySearcher Objdsearch = new DirectorySearcher(ObjDirectory))
                    {
                        SearchResult Objresults = null;
                        Objresults = Objdsearch.FindOne();
                        var userPrincipalDetails = UserPrincipal.Current;
                        activeUser = new UserDetails()
                        {
                            Name = userPrincipalDetails.DisplayName,
                            FirstName = userPrincipalDetails.GivenName,
                            LastName = userPrincipalDetails.Surname,
                            IsActive = true,
                            UserID = userPrincipalDetails.SamAccountName.ToLower(),
                            EmailId = userPrincipalDetails.EmailAddress,
                            MachineName = Environment.MachineName.ToLower(),
                            LastAccessed = DateTime.Now
                        };
                    }
                }

            }
            catch (Exception)
            {
                // Catch here, seems to be a COM exception, no mention in help?
            }

            return activeUser;
        }

        private static UserDetails _currentUser;
        public static UserDetails CurrentUser
        {
            get { return _currentUser; }
            set { _currentUser = value; }
        }

        public static void SetUser()
        {
            if (CurrentUser == null)
            {
                CurrentUser = GetValidUser();
            }
        }
    }
}
