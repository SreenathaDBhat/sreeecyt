﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CytoApps.Infrastructure.Helpers
{
  public static class StringExtensions
    {
        public static bool IgnoreCaseContains(this string sourceText, string searchText, StringComparison comparisionMode)
        {
            return (sourceText.IndexOf(searchText, comparisionMode) != -1);
        }
    }
}
