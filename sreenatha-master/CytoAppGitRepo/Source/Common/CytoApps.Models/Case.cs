﻿using System;
using System.Runtime.Serialization;

namespace CytoApps.Models
{
    [DataContract]
    public class Case
    {
        public Case()
        {
        }

        public Case(string name, string id)
        {
            Name = name;
            Id = id;
        }

        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Id
        {
            get;
            set;
        }

        [DataMember]
        public string Flag
        {
            get;
            set;
        }

        [DataMember]
        public DateTime Created
        {
            get;
            set;
        }

        [DataMember]
        public DateTime? LastAccessed
        {
            get;
            set;
        }

        [DataMember]
        public string Keywords
        {
            get;
            set;
        }
    }
}
