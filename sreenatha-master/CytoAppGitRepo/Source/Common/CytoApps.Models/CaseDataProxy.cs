﻿using System.Collections.Generic;

namespace CytoApps.Models
{
    /// <summary>
    /// Transfers Case, the Slides in that Case, along with the DataSource where the case originated
    /// Generally use for passing selected Case and its Slide to an Analysis plugin.
    /// The DataSource can be ued to find the originating source, generally so a module can query the contents
    /// of a slide.
    /// </summary>
    public class CaseDataProxy
    {
        public CaseDataProxy(DataSource dataSource, Case @case, Slide slide, IEnumerable<Slide> allSlides)
        {
            DataSource = dataSource;
            Case = @case;
            Slide = slide;
            // REFACTOR, lets pass all slides as well for now
            AllSlides = allSlides;
        }

        public DataSource DataSource
        {
            get;
            private set;
        }

        public Case Case
        {
            get;
            private set;
        }

        public Slide Slide
        {
            get;
            private set;
        }

        public IEnumerable<Slide> AllSlides
        {
            get;
            private set;
        }


        /// <summary>
        /// Adds analysed plugins to respective slide region
        /// </summary>
        public string PluginRegionName
        {
            get;
            set;
        }
    }
}
