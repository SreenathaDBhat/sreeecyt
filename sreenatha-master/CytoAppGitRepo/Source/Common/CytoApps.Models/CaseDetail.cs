﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CytoApps.Models
{
    [DataContract]
    public class CaseDetail
    {
        public object Clone()
        {
            return MemberwiseClone();
        }


        [DataMember]
        public string CaseId
        {
            get;
            set;
        }
        [DataMember]
        public string FieldName
        {
            get;
            set;
        }
        [DataMember]
        public string Data
        {
            get;
            set;
        }
        [DataMember]
        public bool Mandatory
        {
            get;
            set;
        }
        [DataMember]
        public short Type { get; set; }
        [DataMember]
        public int Sequence { get; set; }
        [DataMember]
        public bool Confidential { get; set; }
        [DataMember]
        public bool Editable { get; set; }
    }
}
