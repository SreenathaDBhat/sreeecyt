﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CytoApps.Models
{
    public class Constants
    {
        public const string ActiveDirectoryLink = "LDAP://";
        public const string AllowMultiAccess = "allowMultiAccess.lock";
        public const string LegacyLock = ".lock";
    }
}
