﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CytoApps.Models
{
    /// <summary>
    /// Advance search has list of flags and filters to search the databases. encapsulate search conditions to criteria and pass to DAL
    /// </summary>
    [DataContract]
    public class Criteria
    {

        public Criteria()
        {
        }

        [DataMember]
        public List<Filter> Filters
        {
            get;
            set;
        }

    }
}
