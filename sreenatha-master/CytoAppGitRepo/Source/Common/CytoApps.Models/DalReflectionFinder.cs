﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CytoApps.Models
{
    /// <summary>
    /// Helper class that will find Types that implement interface T (e.g, could be ICaseDataAccess or any other DAL interface)
    /// and will create an instance of the type that is matched via the DalTypeIdAttribute that decorates the interface implementation
    /// </summary>
    public class DalReflectionFinder
    {
        /// <summary>
        /// trawls folder for assemblies implementing interface T, loads all the assemblies which may be bad?
        /// </summary>
        /// <typeparam name="T">Interface of the DAL</typeparam>
        /// <param name="directory">folder to search dlls</param>
        /// <returns>array of Type where each Type implements interface T</returns>
        public static Type[] GetTypesForInterface<T>(string directory)
        {
            if (String.IsNullOrEmpty(directory))
                return null;

            DirectoryInfo info = new DirectoryInfo(directory);
            if (!info.Exists)
                return null;

            List<Type> InterfaceTypes = new List<Type>();

            var implementors = new List<T>();

            foreach (FileInfo file in info.GetFiles("*.dll")) // maybe naming convention to help speed things up?
            {
                Assembly currentAssembly = null;
                Type[] types = null;
                try
                {
                    //using Reflection, load Assembly into memory from disk
                    currentAssembly = Assembly.LoadFile(file.FullName);
                    types = currentAssembly.GetTypes();
                }
                catch (Exception)
                {
                    //ignore errors
                    continue;
                }

                foreach (Type type in types)
                {
                    if (!DoesTypeImplementInterface(type, typeof(T)))
                    {
                        continue;
                    }
                    InterfaceTypes.Add(type);
                }
            }
            return InterfaceTypes.ToArray();
        }

        /// <summary>
        /// Trys to match DataSource and Type (implementing T) and creates instance of Type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="types"></param>
        /// <param name="dataSource"></param>
        /// <returns></returns>
        public static T GetMatchingInstanceOf<T>(Type[] types, DataSource dataSource)
        {
            var match = types.Where(t => GetDalTypeId(t) == dataSource.DataSourceType).FirstOrDefault();

            if (match != null)
            {
                var plugin = (T)Activator.CreateInstance(match);
                return plugin;
            }
            return default(T);
        }

        /// <summary>
        /// Get an instance from type, maybe should validate type implements T?
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <returns></returns>
        public static T GetInstanceOfType<T>(Type type)
        {
            Debug.Assert(DoesTypeImplementInterface(type, typeof(T)));

            var plugin = (T)Activator.CreateInstance(type);
            return plugin;
        }

        /// <summary>
        /// Parses Type for Custom Attribute which links DataSourceTemplate to Implementation
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetDalTypeId(Type type)
        {
            string name = string.Empty;

            var customAttributes = (DalTypeIdAttribute[])type.GetCustomAttributes(typeof(DalTypeIdAttribute), true);
            if (customAttributes.Length > 0)
            {
                // assuming just the one
                var attribute = customAttributes[0];
                name = attribute.Name;
            }
            return name;
        }

        public static bool DoesTypeImplementInterface(Type type, Type interfaceType)
        {
            return (type != interfaceType && interfaceType.IsAssignableFrom(type));
        }
    }
}
