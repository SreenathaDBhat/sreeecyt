﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CytoApps.Models
{
    // Custom Attribute used to link Analysis plugin DAL's to a particular DataSourceTemplate
    [AttributeUsage(AttributeTargets.Class)]
    public class DalTypeIdAttribute : Attribute
    {
        public string Name { get; set; }
    }
}
