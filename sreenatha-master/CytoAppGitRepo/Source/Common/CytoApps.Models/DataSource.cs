﻿using System.Runtime.Serialization;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
//using CytoApps.Localization;

/// <summary>
/// Contains Data source information
/// InterfaceType can be either Local or Remote.
/// Local data source will use ConnectionString and CasePath infromation.
/// Remote data source will use BaseURL information.
/// Boolean 'IsActive' indicates currently used/active data sources from the list of data sources.
/// Contains 'ConnectionParameters' class defining 'Key' and 'Value' properties for storing data source paramenter information.
/// </summary>
namespace CytoApps.Models
{
    #region Enum
    public enum DataSourceStatus
    {
        InActive = 0,
        Active = 1
    }
    #endregion

    #region DataSourceClass
    [Serializable]
    [DataContract]
    public class DataSource : INotifyPropertyChanged, ICloneable
    {
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        public DataSource()
        {
        }

        public object Clone()
        {
            try
            {
                using (var stream = new MemoryStream())
                {
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(stream, this);
                    stream.Position = 0;
                    return (DataSource)formatter.Deserialize(stream);
                }
            }
            catch
            {
                throw;
            }
        }

        private Guid _id;
        [DataMember]
        public Guid ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                OnPropertyChanged("ID");
            }
        }

        private string _name;
        [DataMember]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        private string _dataSourceType;
        [DataMember]
        public string DataSourceType
        {
            get
            {
                return _dataSourceType;
            }
            set
            {
                _dataSourceType = value;
                OnPropertyChanged("DataSourceType");
            }
        }

        private bool _isActive;
        [DataMember]
        public bool IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
                OnPropertyChanged("IsActive");
            }
        }

        private ObservableCollection<ConnectionParameter> _connectionParameterCollection;
        [DataMember]
        public ObservableCollection<ConnectionParameter> ConnectionParameter
        {
            get
            {
                if (_connectionParameterCollection == null)
                {
                    _connectionParameterCollection = new ObservableCollection<ConnectionParameter>();
                }
                return _connectionParameterCollection;
            }
            set
            {
                _connectionParameterCollection = value;
                OnPropertyChanged("ConnectionParameter");
            }
        }

        [DataMember]
        public bool IsSelected
        {
            get;
            set;
        }

        public object SessionObject
        {
            get;
            set;
        }


        public DataSourceStatus Status
        {
            get
            {
                if (IsActive)
                    return DataSourceStatus.Active;
                else
                    return DataSourceStatus.InActive;
            }
        }

        public void CreateConnectionParamters(IDataSourceTemplate template)
        {
            ConnectionParameter.Clear();
            foreach (var p in template.ConnectionParameters)
                ConnectionParameter.Add(new ConnectionParameter(p.Key, p.Value));
        }

        protected void OnPropertyChanged(string prop)
        {
            // TODO Changed because Msbuild was giving error ( DataSource.cs(281,29): error CS1525: Invalid expression term '.' 
            // PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
    #endregion

    #region ConnectionParameters Class
    [Serializable]
    public class ConnectionParameter : INotifyPropertyChanged
    {
        public ConnectionParameter()
        {
        }

        public ConnectionParameter(string key, string value) : this()
        {
            Key = key;
            Value = value;
        }

        private string key;
        public string Key
        {
            get { return key; }
            set
            {
                if (key != value)
                {
                    key = value;
                    OnPropertyChanged("Key");
                }
            }
        }

        private string value;
        public string Value
        {
            get { return value; }
            set
            {
                if (this.value != value)
                {
                    this.value = value;
                    OnPropertyChanged("Value");
                }
            }
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string prop)
        {
            // TODO Changed because Msbuild was giving error ( DataSource.cs(281,29): error CS1525: Invalid expression term '.' 
            // PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
    #endregion

}
