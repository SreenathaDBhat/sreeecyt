﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace CytoApps.Models
{
    /// <summary>
    /// indicate if we know something about what type of slide it is
    /// </summary>
    [DataContract][Flags]
    public enum KnownSlides
    {
        UnknownSlide =0x00000000, // for assignment only
        [EnumMember]
        SCNSlide = 0x00000001,
        [EnumMember]
        KaryotypeSlide = 0x00000002,
        [EnumMember]
        ProbeCaseViewSlide = 0x00000004
    }
}
