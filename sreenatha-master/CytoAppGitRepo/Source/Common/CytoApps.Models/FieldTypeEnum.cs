﻿using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace CytoApps.Models
{
    /// <summary>
    /// Identifies the type of the Case field
    /// </summary>
    [DataContract][Flags]
    public enum FieldTypeEnum
    {
        [EnumMember]
        Text,
        [EnumMember]
        MultiLineText,
        [EnumMember]
        Date,
        [EnumMember]
        Number,
        [EnumMember]
        StringArray
    }  
}
