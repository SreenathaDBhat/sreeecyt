﻿using System.Runtime.Serialization;

namespace CytoApps.Models
{
    /// <summary>
    ///  Filter class to search the cases
    /// </summary>
    [DataContract]
    public class Filter
    {
        public Filter()
        { }

        [DataMember]
        public string FieldName
        {
            get;
            set;
        }

        [DataMember]
        public string Value
        {
            get;
            set;
        }

        [DataMember]
        public string Operator
        {
            get;
            set;
        }

        [DataMember]
        public FieldTypeEnum FieldType
        {
            get;
            set;
        }

        [DataMember]

        public bool IsCaseTableField
        {
            get;
            set;
        }

    }
}
