﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CytoApps.Models
{
    public interface IDataSourceTemplate
    {
        IEnumerable<ConnectionParameter> ConnectionParameters { get; }
        string DataSourceTypeId { get; }
    }
}
