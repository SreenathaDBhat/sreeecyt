﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace CytoApps.Models
{
    /// <summary>
    /// Lock Type - based on this display custom message to the user
    /// </summary>
    [DataContract]
    [Flags]
    public enum LockType
    {
        [EnumMember]
        ObjectLock = 1,
        [EnumMember]
        DirectoryNotFound,
        [EnumMember]
        FileNotFound
    }

    /// <summary>
    /// Information about type of lock, user currenly accessing the object, Islocked gives the status of the lock
    /// </summary>
    [DataContract]
    public class LockResult
    {
        private UserDetails _user;
        /// <summary>
        /// Holds the User details for LockResult entity
        /// </summary>
        [DataMember]
        public UserDetails User
        {
            get { return _user; }
            set { _user = value; }
        }

        private bool _isLocked;
        /// <summary>
        /// Holds the IsLocked value for LockResult entity
        /// </summary>
        [DataMember]
        public bool IsLocked
        {
            get { return _isLocked; }
            set { _isLocked = value; }
        }

        private LockType _lockType;
        /// <summary>
        /// Holds the message string  for LockResult entity
        /// </summary>
        [DataMember]
        public LockType LockType
        {
            get { return _lockType; }
            set { _lockType = value; }
        }
    }

    /// <summary>
    /// Locking interface to check lock status , lock object or feature & remove lock or feature.
    /// </summary>
    public interface ILocking
    {
        LockResult CheckKLockExists(UserDetails userDetails, string caseName, object lockingObject);
        void LockFeatureorObject(UserDetails userDetails, string caseName, object lockingObject);
        void RemoveLock(UserDetails userDetails, string caseName);
    }
}
