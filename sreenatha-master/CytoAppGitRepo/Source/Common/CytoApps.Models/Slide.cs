﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CytoApps.Models
{
    [DataContract]
    public class Slide
    {
        public Slide()
        {
        }

        public Slide(string caseId, string name, string id )
        {
            CaseId = caseId;
            Name = name;
            Id = id;
        }
        [DataMember]
        public string CaseId
        {
            get;
            set;
        }
        [DataMember]
        public string Name
        {
            get;
            set;
        }
        [DataMember]
        public string Id
        {
            get;
            set;
        }
        // have as uInt32 rather than the Flags enum KnownSlides as its not serializing properly to eSM
        [DataMember]
        public UInt32 KnownSlideTypes
        {
            get;
            set;
        }

    }
}
