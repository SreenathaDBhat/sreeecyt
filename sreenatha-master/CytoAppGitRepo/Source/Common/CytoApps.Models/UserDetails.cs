﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CytoApps.Models
{
    public class UserDetails
    {
        private string _firstName;
        /// <summary>
        /// Holds the First name for user entity
        /// </summary>
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _lastName;
        /// <summary>
        /// Holds the Last name for user entity
        /// </summary>
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private bool _isActive;
        /// <summary>
        /// Holds the IsActive value for user entity
        /// </summary>
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        private string _userID;
        /// <summary>
        /// Holds the Username value for user entity
        /// </summary>
        public string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        private string _machineName;
        public string MachineName
        {
            get { return _machineName; }
            set { _machineName = value; }
        }

        /// <summary>
        /// Holds the email of the user
        /// </summary>
        private string _emailId;
        public string EmailId
        {
            get { return _emailId; }
            set { _emailId = value; }
        }

        private string _name;
        /// <summary>
        /// Holds the full name of the user
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value != null)
                {
                    _name = value.TrimStart();
                }

            }
        }

        private DateTime _lastAccessed;
        /// <summary>
        /// Holds the last accessed datetime of the user
        /// </summary>
        public DateTime LastAccessed
        {
            get { return _lastAccessed; }
            set { _lastAccessed = value; }
        }
    }
}
