﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using AI.Effects;
using System.ComponentModel;

namespace AI
{
    public interface ChannelsWindowDelegate
    {
        void ChannelsWindowDidChange(ChannelsWindow wnd);
    }

    public partial class ChannelsWindow : INotifyPropertyChanged
    {
        public static DependencyProperty FluorsProperty = DependencyProperty.Register("Fluors", typeof(DisplayFluor[]), typeof(ChannelsWindow));
        public static DependencyProperty ColorModProperty = DependencyProperty.Register("ColorMod", typeof(ColorMod), typeof(ChannelsWindow));
        public static DependencyProperty MaxIndexZProperty = DependencyProperty.Register("MaxIndexZ", typeof(int), typeof(ChannelsWindow));
        
        public TypedWeakReference<ChannelsWindowDelegate> Delegate { get; set; }

        public ChannelsWindow()
        {
            DataContext = this;
            InitializeComponent();

            z_slider.ValueChanged += OnZSliderValueChanged;
        }

        // Fix Mantis #9209: SCN and DICOM DIR multichanel controls are no showing on LAN	
        // Fix Mantis #9192: Z-stack image chanel controls are not working	
        // Added flag to invalidate current visualization if changes on fluoro settings
        public bool InvalidateNeeded
        {
            get;
            set;
        }

        public DisplayFluor[] Fluors
        {
            get 
            { 
                var array = (DisplayFluor[])GetValue(FluorsProperty);
                if (array == null) {
                    return new DisplayFluor[0];
                } else {
                    return array;
                }
            }
            set { SetValue(FluorsProperty, value); }
        }
        public ColorMod ColorMod
        {
            get { return (ColorMod)GetValue(ColorModProperty); }
            set { SetValue(ColorModProperty, value); }
        }
        public int MaxIndexZ
        {
            get { return (int)GetValue(MaxIndexZProperty); }
            set { SetValue(MaxIndexZProperty, value); Notify("SliderMaxZ"); }
        }
        private bool containsMaxProjAtZIndexZero;
        public bool ContainsMaxProjAtZIndexZero
        {
            get { return containsMaxProjAtZIndexZero; }
            set
            {
                containsMaxProjAtZIndexZero = value;
                Notify("ContainsMaxProjAtZIndexZero");

                ShowMaxProjection = value; // Max proj shown by default if available
                Notify("ShowMaxProjection");

                Notify("SliderMaxZ");
            }
        }
        public int SliderMaxZ
        {
            get
            {
                if (MaxIndexZ > 0 && ContainsMaxProjAtZIndexZero)
                    return MaxIndexZ - 1;
                else
                    return MaxIndexZ;
            }
        }
        public int Z
        {
            get 
            {
                // Z values are zero-based. The slider range is from zero to SliderMaxZ, which
                // is one less than the total number of Z-planes if one of them is a maximum projection.
                // If maximum projections are turned on, return index zero (the plane they are kept in).
                // If there is a maximum projection but it isn't selected, adjust the returned value
                // so that the slider range effectively starts at 1.
                // If there are no max projections, just return the slider value.
                int integerSliderValue = (int)z_slider.Value;

                if (showMaxProjection)
                    return 0;
                else if (ContainsMaxProjAtZIndexZero)
                    return integerSliderValue + 1;
                else
                    return integerSliderValue;
            }
            set
            {
                z_slider.ValueChanged -= OnZSliderValueChanged;
                z_slider.Value = value;
                z_slider.ValueChanged += OnZSliderValueChanged;
            }
        }
        private bool showMaxProjection = false;
        public bool ShowMaxProjection
        {
            get { return showMaxProjection; }
            set { showMaxProjection = value; Notify("ShowZSlider"); }
        }
        public bool ShowZSlider
        {
            get { return !ShowMaxProjection; }
        }


        private void OnChangeColor(object sender, EventArgs e)
        {
            var changedChannel = (DisplayFluor)((FrameworkElement)sender).Tag;

            if (!changedChannel.SCNFluor.CanModifyColour)
                return;
           
            System.Windows.Forms.ColorDialog d = new System.Windows.Forms.ColorDialog();
            d.Color = System.Drawing.Color.FromArgb(255, changedChannel.RenderColor.R, changedChannel.RenderColor.G, changedChannel.RenderColor.B);
            if (d.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // Fix Mantis #9209: SCN and DICOM DIR multichanel controls are no showing on LAN	
                // Fix Mantis #9192: Z-stack image chanel controls are not working	
                // Avoid to invalidate screen if no changes on fluoro channels
                Color newColor = Color.FromArgb(255, d.Color.R, d.Color.G, d.Color.B);
                if (newColor != changedChannel.RenderColor)
                {
                    changedChannel.RenderColor = newColor;

                    // fluoro settings changes, set invalidation flag
                    InvalidateNeeded = false;
                    SendUpdateMessageToDelegate();
                }                
            }                        
        }

        private void OnChangeChannelVisibility(object sender, RoutedEventArgs e)
        {
            // Fix Mantis #9209: SCN and DICOM DIR multichanel controls are no showing on LAN	
            // Fix Mantis #9192: Z-stack image chanel controls are not working	
            // Fluoro settings changed, set invalidation flag
            InvalidateNeeded = false;
            SendUpdateMessageToDelegate();
        }

        private void SendUpdateMessageToDelegate()
        {
            if (Delegate != null && Delegate.Target != null)
                Delegate.Target.ChannelsWindowDidChange(this);            
        }

        private void OnReset(object sender, RoutedEventArgs e)
        {
            // Fix Mantis #9183: Image controls Reset button keeps refreshing the image every time user click on it
            // Avoid to refresh if no changes applied on image control panel
            bool changed = ColorMod != null ? ColorMod.Reset() : false;

            if (this.Fluors != null)
            {
                foreach (var i in this.Fluors)
                {
                    if (!i.IsVisible)
                    {
                        i.IsVisible = true;
                        changed = true;
                    }
                }
            }

            if (changed)
            {
                // Fix Mantis #9209: SCN and DICOM DIR multichanel controls are no showing on LAN	
                // Fix Mantis #9192: Z-stack image chanel controls are not working	
                // Fluoro settings changed, set invalidation flag
                InvalidateNeeded = true;
                SendUpdateMessageToDelegate();
            }                    
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void Notify(string s)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(s));
        }

        public AI.Imaging.Fluor[] ActiveFluors
        {
            get
            {
                return (from f in Fluors where f.IsVisible select f.SCNFluor).ToArray();
            }
        }

        private void OnZSliderValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // Fix Mantis #9252: Z-stack slider is not working
            // Fix Mantis #9281: Moving the Z-stack slider refreshes the image
            // Invalidate screen (missing)
            InvalidateNeeded = false;
            SendUpdateMessageToDelegate();
        }

        private void OnMaxProjOn(object sender, RoutedEventArgs e)
        {
            ShowMaxProjection = true;
            InvalidateNeeded = false;
            SendUpdateMessageToDelegate();
        }

        private void OnMaxProjOff(object sender, RoutedEventArgs e)
        {
            ShowMaxProjection = false;
            InvalidateNeeded = false;
            SendUpdateMessageToDelegate();
        }
    }
}
