/// <class>BrightExtractEffect</class>

/// <description>An effect that dims all but the brightest pixels.</description>

//-----------------------------------------------------------------------------------------
// Shader constant register mappings (scalars - float, double, Point, Color, Point3D, etc.)
//-----------------------------------------------------------------------------------------

/// <minValue>-1</minValue>
/// <maxValue>1</maxValue>
/// <defaultValue>0</defaultValue>
float rlev : register(C0);

/// <minValue>-1</minValue>
/// <maxValue>1</maxValue>
/// <defaultValue>0</defaultValue>
float glev : register(C1);

/// <minValue>-1</minValue>
/// <maxValue>1</maxValue>
/// <defaultValue>0</defaultValue>
float blev : register(C2);

/// <minValue>0</minValue>
/// <maxValue>1</maxValue>
/// <defaultValue>0</defaultValue>
float brightness : register(C3);

/// <minValue>1</minValue>
/// <maxValue>4</maxValue>
/// <defaultValue>0.5</defaultValue>
float contrast : register(C4);

//--------------------------------------------------------------------------------------
// Sampler Inputs (Brushes, including Texture1)
//--------------------------------------------------------------------------------------

sampler2D Texture1Sampler : register(S0);


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------

float4x4 scaleMat(float s)
{
    return float4x4(
	s, 0, 0, 0,
	0, s, 0, 0,
	0, 0, s, 0,
	0, 0, 0, 1);
}

float4 main(float2 uv : TEXCOORD) : COLOR
{
    // Look up the original image color.
    // Undo pre-multiplied alpha.
    float4 originalColor = tex2D(Texture1Sampler, uv);    
	float3 rgb = originalColor.rgb / originalColor.a;
	
	rgb.r = clamp(rlev + rgb.r, 0, 1);
	rgb.g = clamp(glev + rgb.g, 0, 1);
	rgb.b = clamp(blev + rgb.b, 0, 1);
	
	rgb += clamp(rgb + brightness, 0, 1);
	rgb = mul(rgb, scaleMat(contrast));
	
	if(neg)
		rgb = 1-rgb;
    
    // Re-apply alpha.
    return float4(rgb * originalColor.a, originalColor.a);
}


