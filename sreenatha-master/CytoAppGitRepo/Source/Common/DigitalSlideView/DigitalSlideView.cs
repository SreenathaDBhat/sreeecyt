﻿using System;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using AI.Imaging;

namespace AI
{
    public class DigitalSlideView : ContentControl
    {
        static DigitalSlideView()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DigitalSlideView), new FrameworkPropertyMetadata(typeof(DigitalSlideView)));
        }

        // Don't really need this anymore - only the width and height        
        public static readonly DependencyProperty ImageCollectionProperty = DependencyProperty.Register("ImageCollection", typeof(ISlideImageCollection), typeof(DigitalSlideView), new FrameworkPropertyMetadata(OnImageCollectionChanged));
        public static readonly DependencyProperty ViewportProperty = DependencyProperty.Register("Viewport", typeof(SlideViewViewport), typeof(DigitalSlideView));
        public static readonly DependencyProperty HackTagProperty = DependencyProperty.Register("HackTag", typeof(object), typeof(DigitalSlideView), new FrameworkPropertyMetadata(OnHackTagChanged));
        public static readonly DependencyProperty ScalingFactorProperty = DependencyProperty.Register("ScalingFactor", typeof(double), typeof(DigitalSlideView), new FrameworkPropertyMetadata(0.5));

        public event EventHandler WhatsOnDisplayChanged;

        private TileImager tileImager;

        private Border _overlays;
        private FrameworkElement _canvasV1;
        private FrameworkElement rotatable;
        private FrameworkElement scanAreaCanvas;

        public delegate int DEP_ZFunc();
        public DEP_ZFunc ZGetter = () => {return 0;};

        public DigitalSlideView()
        {
            if (tileImager == null)
                tileImager = new TileImager(Dispatcher);

            Loaded += (s, e) =>
            {
                SizeChanged += (ss, ee) =>
                {
                    if (ImageCollection != null)
                    {
                        SendViewportToTiler();
                    }
                };
            };

            Unloaded += (s, e) =>
            {
                if (tileImager != null)
                {
 //                   tileImager.Dispose();
 //                   tileImager = null;
                }
            };
        }

        public TileImager Imager
        {
            get { return tileImager; }
        }

        public SlideViewViewport Viewport
        {
            get { return (SlideViewViewport)GetValue(ViewportProperty); }
            set { SetValue(ViewportProperty, value); }
        }

        public ISlideImageCollection ImageCollection
        {
            get { return (ISlideImageCollection)GetValue(ImageCollectionProperty); }
            set { SetValue(ImageCollectionProperty, value); }
        }

        public double ScalingFactor
        {
            get { return (double)GetValue(ScalingFactorProperty); }
            set { SetValue(ScalingFactorProperty, value); }
        }

        // HackTag is like a Tag but one you can read from any thread.
        // Tag is a dependency property - they can only be touched, read OR write,
        // from the main thread. GAY.
        object _localHackTagCopy;
        public object HackTag
        {
            get { return _localHackTagCopy; }
            set { SetValue(HackTagProperty, value); }
        }

        private static void OnHackTagChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            DigitalSlideView me = (DigitalSlideView)sender;
            me._localHackTagCopy = e.NewValue;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            rotatable = (FrameworkElement)GetTemplateChild("rotatable");
            rotatable.DataContext = this;

            scanAreaCanvas = (FrameworkElement)GetTemplateChild("host");
            _canvasV1 = (FrameworkElement)GetTemplateChild("canvasV1");
            _overlays = (Border)GetTemplateChild("_overlays");

            ClipToBounds = true;
        }

        public void Dispose()
        {
            if (Viewport != null)
            {
                Viewport = null;
            }

            if (tileImager != null)
            {
                tileImager.WhatsOnDisplayChanged -= WhatsOnDisplay;
                tileImager.Dispose();
                tileImager = null;
            }
        }

        private void WhatsOnDisplay(object s, EventArgs e)
        {
            if (WhatsOnDisplayChanged != null)
                WhatsOnDisplayChanged(this, e);
        }

        private static void OnImageCollectionChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var me = sender as DigitalSlideView;
            var t = e.NewValue as ISlideImageCollection;
            var old = e.OldValue as ISlideImageCollection;

            if (me.tileImager != null)
            {
                me.tileImager.WhatsOnDisplayChanged -= me.WhatsOnDisplay;
                //me.tileImager.Dispose();
                //me.tileImager = null;
            }

            if (t == null)
            {
                return;
            }

            me.tileImager.SetSCN(t);
            me.tileImager.WhatsOnDisplayChanged += me.WhatsOnDisplay;

            me.SendViewportToTiler();
        }

        // Not sure about this...!!!!!!
        internal FrameworkElement Rotatable { get { return rotatable; } }
        internal FrameworkElement ScanAreaCanvas { get { return scanAreaCanvas; } }

        public Point SlidespacePointToScreenspace(Point slidespacePoint)
        {
            return ScanAreaCanvas.TranslatePoint(slidespacePoint, this);
        }

        public Point ScreenspacePointToSlidespace(Point p)
        {
            return TranslatePoint(p, ScanAreaCanvas);
        }

        public void InvalidateDisplay()
        {
            if (tileImager != null)
            {
                tileImager.InvalidateDisplay();
            } 
        }

        public void SendViewportToTiler(bool invalidate = true)
        {
            if (Viewport == null || tileImager == null)
                return;

            // Avoid to use visible area when a not consistent value is passed
            // It happens when SGC starts minimized, because the "not visible" viewport component returns width==height==0
            var v = VisibleArea;
            if (Double.IsNaN(v.Width) || Double.IsNaN(v.Height))
                return;
            v = Rect.Inflate(v, v.Width / 10.0, v.Height / 10.0);

            double a = Math.Abs(Math.Sin(Viewport.RotationDegrees) * 0.5);
            v = Rect.Inflate(v, v.Width * a, v.Width * a);
            
            if (tileImager != null)
            {
                int z = ZGetter();
                tileImager.UpdateVisibleTiles(VisibleArea, v, (int)ActualWidth, z, invalidate);
            }
        }

        public Rect VisibleArea
        {
            get
            {
                var mag = Viewport.Scale * ScalingFactor;
                var visibleWidth = (ActualWidth) / mag;
                var visibleHeight = (ActualHeight) / mag;
                var hw = visibleWidth / 2;
                var hh = visibleHeight / 2;
                var center = Viewport.LookAt;

                Rect actualRect = new Rect(new Point(center.X - hw, center.Y - hh), new Point(center.X + hw, center.Y + hh));

                var matrix = new Matrix();
                matrix.RotateAt(Viewport.RotationDegrees, Viewport.LookAt.X, Viewport.LookAt.Y);
                var a = matrix.Transform(actualRect.TopLeft);
                var b = matrix.Transform(actualRect.TopRight);
                var c = matrix.Transform(actualRect.BottomLeft);
                var d = matrix.Transform(actualRect.BottomRight);

                var minx = Math.Min(a.X, Math.Min(b.X, Math.Min(c.X, d.X)));
                var maxx = Math.Max(a.X, Math.Max(b.X, Math.Max(c.X, d.X)));
                var miny = Math.Min(a.Y, Math.Min(b.Y, Math.Min(c.Y, d.Y)));
                var maxy = Math.Max(a.Y, Math.Max(b.Y, Math.Max(c.Y, d.Y)));
                return new Rect(minx, miny, maxx - minx, maxy - miny);
            }
        }

        public double GetZoomtoFitValue()
        {
            double m = 0;

            double rotationRadians = Viewport.RotationDegrees * (Math.PI/180);
            double boundsWidthOfRotatedSlide = Math.Abs(ImageCollection.Bounds.Width * Math.Cos(rotationRadians)) + Math.Abs(ImageCollection.Bounds.Height * Math.Sin(rotationRadians));
            double boundsHeightOfRotatedSlide = Math.Abs(ImageCollection.Bounds.Width * Math.Sin(rotationRadians)) + Math.Abs(ImageCollection.Bounds.Height * Math.Cos(rotationRadians));

            if (boundsWidthOfRotatedSlide > boundsHeightOfRotatedSlide) {
                m = (1 / (boundsWidthOfRotatedSlide / ActualWidth)) * 0.95;
            } else {
                m = (1 / (boundsHeightOfRotatedSlide / ActualHeight)) * 0.95;
            }
            return m / ScalingFactor;            
        }

        public void ZoomtoFit(bool animate, bool gotoCenterOfSlideAswel)
        {
            var m = GetZoomtoFitValue();

            if (Viewport != null)
            {
                if (animate)
                {
                    if(gotoCenterOfSlideAswel)
                        Viewport.LookAt = new Point(ImageCollection.Bounds.Width / 2, ImageCollection.Bounds.Height / 2);

                    Viewport.Scale = m;
                }
                else
                {
                    if (gotoCenterOfSlideAswel)
                        Viewport.SetLookAt(new Point(ImageCollection.Bounds.Width / 2, ImageCollection.Bounds.Height / 2));

                    Viewport.SetScale(m); 
                }
            }

            SendViewportToTiler();
        }

        public void ZoomtoFit(bool animate)
        {
            ZoomtoFit(animate, true);
        }

        public void UpdateZ(int z)
        {
            if (null != tileImager)
            {
                tileImager.SetZ(z);
            }
        }

        public void UpdateFluors(Fluor [] fluor)
        {
            if(null!=tileImager)
            {
                tileImager.SetFluors(fluor);
            }
        }

        public void RefreshTileImages(bool invalidate=false)
        {       
            if (tileImager != null && invalidate)
            {
                tileImager.ReloadAll();                
            }
        }

        public UIElement Overlay
        {
            get { return _overlays.Child; }
            set { _overlays.Child = value; }
        }
    }
}
