﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using AI.Imaging;

namespace AI
{
    public class DSVMouseButtonEventArgs
    {
        public MouseButtonEventArgs MouseButtonEventArgs { get; private set; }
        public bool IsOutsideDeadzone { get; private set; }

        public DSVMouseButtonEventArgs(MouseButtonEventArgs e, bool outsideDeadzone)
        {
            MouseButtonEventArgs = e;
            IsOutsideDeadzone = outsideDeadzone;
        }
    }

	public interface IDigitalSlideViewInputDelegate
	{
		void MouseButtonPressed(object senbder, MouseButtonEventArgs e);
		void MouseButtonReleased(object senbder, DSVMouseButtonEventArgs e);
	}

	public sealed class DigitalSlideViewInput
	{
		public const double DoubleClickTime = 0.3;

		private TypedWeakReference<DigitalSlideView> _slideView;
		private IDigitalSlideViewInputDelegate _listener;
		private DigitalSlideViewInput[] _linkedSiblings;

		public DigitalSlideViewInput()
		{
			IsDoubleClickZoomEnabled = true;
		}

		public void LinkWith(DigitalSlideViewInput[] other)
		{
			_linkedSiblings = other;
		}

		public void Unlink()
		{
			_linkedSiblings = null;
		}

		public bool IsLinked
		{
			get { return _linkedSiblings != null; }
		}

		public void AttachTo(DigitalSlideView dsv)
		{
			_slideView = new TypedWeakReference<DigitalSlideView>(dsv);
			dsv.MouseDown += OnLmbDown;
			dsv.MouseUp += OnLmbUp;
			dsv.MouseMove += OnMouseMove;
			dsv.MouseWheel += OnMouseWheel;
		}

		public void Detach()
		{
			if (_slideView.Target == null)
				return;

			_slideView.Target.MouseDown -= OnLmbDown;
			_slideView.Target.MouseUp -= OnLmbUp;
			_slideView.Target.MouseMove -= OnMouseMove;
			_slideView.Target.MouseWheel -= OnMouseWheel;
			_slideView = null;
			Delegate = null;
		}

		public IDigitalSlideViewInputDelegate Delegate
		{
			get { return _listener; }
			set { _listener = value; }
		}

		#region Mouse
		private Point? _lastMousePos;
		private Point _deadzoneRoot;
        private Point _doubleClickPt = new Point(-1000, -1000);
		private DateTime _timeOfLastClick = DateTime.MinValue;
        private bool _outsideDeadzone;
		public bool IsDoubleClickZoomEnabled { get; set; }

		private void OnLmbDown(object sender, MouseButtonEventArgs e)
		{
			if (_listener != null)
			{
				_listener.MouseButtonPressed(sender,e);
			}

            _outsideDeadzone = false;

			if (!e.Handled)
			{
				DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
				if (slideView == null)
					return;

				_lastMousePos = e.GetPosition(slideView.Rotatable);
				_deadzoneRoot = e.GetPosition(null);

				// sender will NOT be 'this' if called via slidelinking
				if (sender == slideView)
				{
					slideView.Rotatable.CaptureMouse();
					if (_linkedSiblings != null)
						foreach(var ls in _linkedSiblings)
							ls.OnLmbDown(this, e);
				}                    
			}
		}

		private void OnLmbUp(object sender, MouseButtonEventArgs e)
		{
			DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
			if (slideView == null)
				return;

			if(slideView.Rotatable.IsMouseCaptured)
				slideView.Rotatable.ReleaseMouseCapture();

			_lastMousePos = null;

            if (_listener != null) {
                _listener.MouseButtonReleased(sender, new DSVMouseButtonEventArgs(e, _outsideDeadzone));
            }

			DateTime now = DateTime.Now;
			double timeSinceLastClick = (now - _timeOfLastClick).TotalSeconds;
			var pt = e.GetPosition(slideView.Rotatable);
			bool isDoubleClick = IsDoubleClickZoomEnabled && timeSinceLastClick < DoubleClickTime && Math.Abs(_doubleClickPt.X - pt.X) < 3.0 && Math.Abs(_doubleClickPt.Y - pt.Y) < 3.0;

			if (isDoubleClick)
			{
				var dc = e.GetPosition(slideView.ScanAreaCanvas);
                slideView.Viewport.SetLookAtAndMagnificationPower(dc, slideView.Viewport.NextMagnificationNotch());
			}
			else
			{
				_doubleClickPt = pt;
				_timeOfLastClick = now;
			}


			// SEND TO SIBLING MAYBE
			if (_linkedSiblings != null && sender == _slideView.Target)
			{
				if (isDoubleClick)
				{
					foreach (var ls in _linkedSiblings)
						ls.DoFakeDblClickAt(e.GetPosition(slideView));
				}
				else
				{
					foreach (var ls in _linkedSiblings)
						ls.OnLmbUp(this, e);
				}
			}

			// Fix Mantis #9167: There is not right click menu available if calibration window is open
			// Reset handle status to propagate event to context menu handler
			e.Handled = false;
		}

		private void DoFakeDblClickAt(Point pt)
		{
			DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
			if (slideView == null)
				return;

			if (slideView.Rotatable.IsMouseCaptured)
				slideView.Rotatable.ReleaseMouseCapture();
			_lastMousePos = null;
			Point dc = slideView.TranslatePoint(pt, slideView.ScanAreaCanvas);
			slideView.Viewport.LookAt = dc;
			slideView.Viewport.MoveMagnificationToNextNotch();
		}

		private void OnMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
		{            
			if (_lastMousePos == null)
			{
				return;
			}
			DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
			if (slideView == null)
				return;
			
			var pt = e.GetPosition(slideView.Rotatable);
			var last = _lastMousePos.Value;
			var mag = slideView.Viewport == null ? 1 : slideView.Viewport.Scale;
			double dx = (pt.X - last.X) / mag;
			double dy = (pt.Y - last.Y) / mag;
			_lastMousePos = pt;

			var screenPt = e.GetPosition(null);
			int deadzone = 5;
			bool outsideDeadzone = Math.Abs(screenPt.X - _deadzoneRoot.X) > deadzone || Math.Abs(screenPt.Y - _deadzoneRoot.Y) > deadzone;

			if (_outsideDeadzone || outsideDeadzone) // reads weird but the OR is intentional - you might leave deadzone then stray back into it....
			{
                _outsideDeadzone = true;
                slideView.Viewport.LookAt = new Point(slideView.Viewport.LookAt.X - dx, slideView.Viewport.LookAt.Y - dy);
			}

			if (_linkedSiblings != null && sender == _slideView.Target)
			{
				foreach (var ls in _linkedSiblings)
					ls.OnMouseMove(this, e);
			}
		}

		private void OnMouseWheel(object sender, MouseWheelEventArgs e)
		{
			DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
			if (slideView == null)
				return;

			var direction = e.Delta;
			e.GetPosition(slideView.Rotatable);

			if (Keyboard.IsKeyDown(Key.Z))
			{
				if (Math.Sign(direction) > 0)
				{
					throw new NotSupportedException();
				}
				else
				{
					throw new NotSupportedException();
				}
			}
			else
			{                
				if (_linkedSiblings != null)
				{
					ScaleBy(slideView.Viewport, direction);

					if (sender == _slideView.Target)
					{
						foreach (var ls in _linkedSiblings)
						{
							DigitalSlideView lsv = ls._slideView.Target as DigitalSlideView;
							if (lsv == null)
								continue;

							// Fix Mantis #9184: Images are not locking correctly when zooming in and out
							// Added missing call to siblings 
							ls.OnMouseWheel(this, e);
						}
					}
				}
				else
				{
					// Fix Mantis #8827: Zoom in viewer does not zoom in and out to same location on slide
					// Modified zoom algorithm to match DIH viewer implementation
					// get old coordinates/scale
					var oldScale = slideView.Viewport.Scale;
					var oldMouseXY = e.GetPosition(slideView.ScanAreaCanvas);

					// apply scale
					ScaleBy(slideView.Viewport, direction);
					var scaleMod = oldScale/slideView.Viewport.Scale;                    

					// get new coordinate and displacement
					Point lookXY = slideView.Viewport.LookAt;                                            
					Point deltaXY = slideView.VisibleArea.TopLeft;
					
					// Fixed Mantis #9249: The visualization is not consistent zooming after flipping the image
					// Added adjustment to take in account the flip operation of XY coordinates
					deltaXY.X += slideView.VisibleArea.Width / 2.0 - oldMouseXY.X;
					deltaXY.Y += slideView.VisibleArea.Height / 2.0 - oldMouseXY.Y;                        

					// apply new lookat coordinates
					slideView.Viewport.LookAt = new Point(lookXY.X - deltaXY.X * (1.0 - scaleMod), lookXY.Y - deltaXY.Y * (1.0 - scaleMod));                    
				}
			}
		}

		private void ScaleBy(SlideViewViewport slideViewViewport, int direction)
		{
			slideViewViewport.Scale += 0.3 * slideViewViewport.Scale * Math.Sign(direction);
		}

		#endregion
		#region Keyboard

		private int angleDelta = 0;
		
		public bool ControlPressed { get; private set; }
		public bool ShiftPressed { get; private set; }

		public void ResetRotate()
		{
            HardSetrotation(0, this);
		}

		public void IncrementRotation(bool persist)
		{
			DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
			if (slideView == null)
				return;

			if (slideView == null || slideView.Viewport == null )
				return;

			var to = slideView.Viewport.RotationDegrees + 90;
			var remainder = to % 90;
			to -= remainder;
			slideView.Viewport.RotationDegrees = to;
		}

		private void HardSetrotation(double to, DigitalSlideViewInput digitalSlideViewInput)
		{
			DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
			if (slideView == null)
				return;

			slideView.Viewport.RotationDegrees = to;
			if (_linkedSiblings != null && digitalSlideViewInput == this)
				foreach (var ls in _linkedSiblings)
					ls.HardSetrotation(to, this);
		}

		private void Rotate(double delta, object sender)
		{
			DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
			if (slideView == null)
				return;

			slideView.Viewport.RotationDegrees = (slideView.Viewport.RotationDegrees + delta) % 360;
			if (_linkedSiblings != null && sender == this)
				foreach (var ls in _linkedSiblings)
					ls.Rotate(delta, this);
		}
		public void RotateLeft()
		{
			angleDelta = Math.Min(++angleDelta, 5);
			Rotate(-angleDelta, this);
		}
		public void RotateRight()
		{
			angleDelta = Math.Min(++angleDelta, 5);
			Rotate(angleDelta, this);
		}

		public bool HandleKeyboardCtrls()
		{
			ControlPressed = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
			Notify("ControlPressed");
			ShiftPressed = Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift);
			Notify("ShiftPressed");

			return ControlPressed || ShiftPressed;
		}

		public bool HandleKeyDown(Key key)
		{
			return HandleKeyDown(key, null);
		}
		public bool HandleKeyDown(Key key, object sender)
		{
			bool handled = false;
			HandleKeyboardCtrls();

			if (key == Key.D)
			{
				RotateRight();
				handled = true;
			}
			else if (key == Key.A)
			{
				RotateLeft();
				handled = true;
			}

			if (_linkedSiblings != null && sender == null)
				foreach (var ls in _linkedSiblings)
					ls.HandleKeyDown(key, this);

			return handled;
		}

		public bool HandleKeyUp(Key key)
		{
			HandleKeyboardCtrls();
			return HandleKeyUp(key, ControlPressed);
		}

		public bool HandleKeyUp(Key key, bool ctrlPressed)
		{
			return HandleKeyUp(key, ctrlPressed, null);
		}

		private bool HandleKeyUp(Key key, bool ctrlPressed, object sender)
		{
			/*[rj 25.Sep.2013] Mantis Bug# 9365: I might be that the _slideView is NULL? Could not see it from the reported 
			 * call stack. But check it has no negative effect. 
			 */
			if (_slideView == null || _slideView.Target == null)
			{
				return false;
			}

			DigitalSlideView slideView = _slideView.Target as DigitalSlideView;

			bool handled = false;

			if (FOVPan(key, true))
			{
				handled = true;
			}
			else if (key == Key.F && ctrlPressed)
			{
				bool persist = sender == null;
				IncrementRotation(persist);
				handled = true;
			}
			else if (key == Key.NumPad0)
			{
				bool gotoCenterOfSlide = slideView.Viewport.Scale == slideView.GetZoomtoFitValue();

				slideView.ZoomtoFit(true, gotoCenterOfSlide);
				handled = true;
			}
            else if (key == Key.NumPad1)
            {
                if (IsMeasurable())
                    slideView.Viewport.MagnificationPower = 5;
                else
                    ZoomPercent(slideView.Viewport, 100);

                handled = true;
            }
			else if (key == Key.NumPad4)
			{
				if (IsMeasurable())
					slideView.Viewport.MagnificationPower = 10;
				else
					ZoomPercent(slideView.Viewport, 150);

				handled = true;
			}
			else if (key == Key.NumPad7)
			{
				if (IsMeasurable())
					slideView.Viewport.MagnificationPower = 20;
				else
					ZoomPercent(slideView.Viewport, 200);

				handled = true;
			}
            else if (key == Key.NumPad8)
            {
                if (IsMeasurable())
                    slideView.Viewport.MagnificationPower = 40;
                else
                    ZoomPercent(slideView.Viewport, 250);

                handled = true;
            }
            else if (key == Key.NumPad9)
            {
                if (IsMeasurable())
                    slideView.Viewport.MagnificationPower = 63;
                else
                    ZoomPercent(slideView.Viewport, 300);

                handled = true;
			}
			else if (key == Key.A || key == Key.D)
			{
				angleDelta = 0;
				handled = true;
			}

			if (_linkedSiblings != null && sender == null)
				foreach (var ls in _linkedSiblings)
					ls.HandleKeyUp(key, ctrlPressed, this);

			return handled;
		}

		private void ZoomPercent(SlideViewViewport Viewport, double pcent)
		{
			Viewport.Scale = (pcent / 100.0);
		}

		private bool IsMeasurable()
		{
			DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
			if (slideView == null)
				return false;

			var image = slideView.ImageCollection;
			return image.IsMeasurable;
		}

		private bool FOVPan(Key key, bool sendToSibling)
		{
			DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
			if (slideView == null)
				return false;

			if (sendToSibling && _linkedSiblings != null)
				foreach (var ls in _linkedSiblings)
					ls.FOVPan(key, false);

			slideView.Viewport.UseSprings = true;
			var va = slideView.VisibleArea;
			var viewport = slideView.Viewport;
			var image = slideView.ImageCollection;

			if (key == Key.Left || key == Key.Right || key == Key.Up || key == Key.Down)
			{
				double screenMoveDelta = 0.9;

				double dw = va.Width * screenMoveDelta * viewport.FlipScaleX;
				double dh = va.Height * screenMoveDelta * viewport.FlipScaleY;

				bool horz = key == Key.Left || key == Key.Right;
                double dist = (horz) ? dw : dh;

				double angle = -slideView.Viewport.RotationDegrees;
				switch (key)
				{
					case Key.Left: angle = (angle + 270.0) % 360; break;
					case Key.Right: angle = (angle + 90.0) % 360; break;
					case Key.Down: angle = (angle + 180.0) % 360; break;
				}

				var matrix = new Matrix();
				matrix.RotateAt(angle, 0, 0);

				var translated = matrix.Transform(new Point(0, dist));

				MoveBy(image, viewport, -translated.X, -translated.Y);
				return true;
			} 
			else if (key == Key.PageUp || key == Key.Add || key == Key.W)
			{
				ZoomInOneNotch(viewport);
				return true;
			}
			else if (key == Key.PageDown || key == Key.Subtract || key == Key.S)
			{
				ZoomOutOneNotch(viewport);
			}

			return false;
		}

		private void ZoomInOneNotch(SlideViewViewport viewport)
		{
			viewport.Scale *= 2;
		}
		private void ZoomOutOneNotch(SlideViewViewport viewport)
		{
			viewport.Scale *= 0.5;
		}

		private void MoveBy(ISlideImageCollection slide, SlideViewViewport viewport, double dx, double dy)
		{
			var look = viewport.LookAt;
			viewport.LookAt = new Point(
				Math.Max(0, Math.Min(slide.Bounds.Width, look.X + dx)),
				Math.Max(0, Math.Min(slide.Bounds.Height, look.Y + dy)));
		}

		public event PropertyChangedEventHandler PropertyChanged;
		private void Notify(string p)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(p));
			}
		}

		#endregion
		#region Context Menu and Whatnot

		public void ZoomtoFit(bool animate, bool center)
		{
			ZoomtoFit(animate, center, true);
		}
		private void ZoomtoFit(bool animate, bool center, bool sendToSibling)
		{
			DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
			if (slideView == null)
				return;

			slideView.ZoomtoFit(animate, center);

			if (_linkedSiblings != null && sendToSibling)
				foreach (var ls in _linkedSiblings)
					ls.ZoomtoFit(animate, center, false);
		}

		public double FlipScaleX
		{
			get 
			{
				DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
				if (slideView == null)
					return 0;
				return slideView.Viewport.FlipScaleX; 
			}
			set 
			{
				DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
				if (slideView == null)
					return;

				SetFlips(slideView.Viewport.FlipScaleX * -1, slideView.Viewport.FlipScaleY, true); 
			}
		}

		public double FlipScaleY
		{
			get 
			{
				DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
				if (slideView == null)
					return 0;
				return slideView.Viewport.FlipScaleY;
			}
			set 
			{
				DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
				if (slideView == null)
					return;

				SetFlips(slideView.Viewport.FlipScaleX, slideView.Viewport.FlipScaleY * -1, true); 
			}
		}

		private void SetFlips(double x, double y, bool sendToSibling)
		{
			DigitalSlideView slideView = _slideView.Target as DigitalSlideView;
			if (slideView == null)
				return;

			slideView.Viewport.FlipScaleY = y;
			slideView.Viewport.FlipScaleX = x;

			if (_linkedSiblings != null && sendToSibling)
				foreach (var ls in _linkedSiblings)
					ls.SetFlips(x, y, false);
		}

		#endregion
	}
}
