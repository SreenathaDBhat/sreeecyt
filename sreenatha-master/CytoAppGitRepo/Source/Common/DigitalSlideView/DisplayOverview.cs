﻿using AI.Imaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AI
{
    public sealed class DisplayOverview
    {
        internal DisplayOverview(SlideImage repImage, Tile repTile, Rect visibleArea, int z)
        {
            RepresentativeImage = repImage;
            RepresentativeTile = repTile;
            VisibleArea = visibleArea;
            Z = z;
        }

        public static DisplayOverview Nothing { get { return new DisplayOverview(null, null, Rect.Empty, -1); } }

        public int Z { get; private set; }
        public SlideImage RepresentativeImage { get; private set; }
        public Tile RepresentativeTile { get; private set; }
        public Rect VisibleArea { get; private set; }
        public bool ZStacks
        {
            get
            {
                return (from d in RepresentativeImage.Dimensions select d.Z).Distinct().Count() > 1;
            }
        }
    }
}
