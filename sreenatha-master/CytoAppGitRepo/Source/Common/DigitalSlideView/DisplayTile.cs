﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Threading;
using AI.Imaging;

namespace AI
{
    /// <summary>
    /// DisplayImage wraps an SCN file Image - a scan area, essentially.
    /// <see cref="VisibleTiles"/> is an observable list. Bind to this for updates when the viewport changes.
    /// </summary>
    public sealed class DisplayImage : Notifier
    {
        private SlideImage image;
        private ObservableCollection<DisplayTile> visibleTiles;
 
        private DisplayFluor[] fluors;
        private bool isVisible;

        public DisplayImage(SlideImage image)
        {
            isVisible = true;
            this.image = image;
            this.visibleTiles = new ObservableCollection<DisplayTile>();
            this.fluors = (from f in image.Fluors
                           select new DisplayFluor(f)).ToArray();
        }

        public String Name { get { return image.Name; } }

        public bool IsVisible
        {
            get { return isVisible; }
            set { isVisible = value; Notify("IsVisible"); }
        }

        public IEnumerable<DisplayTile> VisibleTiles
        {
            get { return visibleTiles; }
        }

        public IEnumerable<DisplayFluor> Fluors
        {
            get { return fluors; }
        }

        public bool IsFluorescent
        {
            get { return fluors.Length > 1; }
        }

        public DisplayFluor[] DuplicateFluors()
        {
            return (from f in fluors select (DisplayFluor)f.Clone()).ToArray();
        }


        public SlideImage Image { get { return image; } }

        internal void AddTiles(IEnumerable<DisplayTile> newTiles)
        {
            foreach (var t in newTiles)
                visibleTiles.Add(t);            
        }

        internal void AddTile(DisplayTile t)
        {
            visibleTiles.Add(t);
        }

        public int CurrentResolution { get; set; }

        internal void ClearAll()
        {
            visibleTiles.Clear();
        }

        public void MarkTilesForRefresh()
        {
            foreach (DisplayTile t in visibleTiles)
            {
                t.ClearZStackCache();
            }
        }

    }

    

    public sealed class DisplayTile : Notifier
    {
        private Tile tile;
        private Dictionary<int, ImageSource> zimages;
        private DisplayImage parent;
        
        public DisplayTile(DisplayImage parent, Tile tile, double tileSizeMicrons)
        {
            this.parent = parent;
            this.tile = tile;
            zimages = new Dictionary<int, ImageSource>();
            TileSize = tileSizeMicrons;
        }

        public DisplayImage ParentImage { get { return parent; } }
        public double TileSize { get; private set; }

        public Rect Position
        {
            get { return tile.Position; }
        }

        public ImageSource Image { get; set; }

        public ImageSource GetImage(int thisZ)
        {
            if (zimages.ContainsKey(thisZ))
                return zimages[thisZ];

            int best = int.MaxValue;
            ImageSource bestIm = null;

            foreach (var imz in zimages.Keys)
            {
                var d = Math.Abs(imz - this.z);
                if (d < best)
                {
                    best = d;
                    bestIm = zimages[imz];
                }
            }

            return bestIm;
        }

        int z;
        public int Z
        {
            get { return z; }
            set 
            { 
                z = value;
                Image = GetImage(z);
                Notify("Z");
            }
        }

        public Rect ContentSizeAsWPFViewport
        {
            get { return new Rect(0, 0, 1 / tile.ContentSize.Width, 1 / tile.ContentSize.Height); }
        }

        public Tile SCNTile
        {
            get { return tile; }
        }

        /// <summary>
        /// MUST be called on UI thread (or else it just wont work - wont crash either mind)
        /// </summary>
        internal void AbsorbImage(ImageSource newImage, int z)
        {
            Image = newImage;
            zimages[z] = newImage;
            NotifyImage();
        }

        internal void ClearZStackCache()
        {            
            zimages = new Dictionary<int, ImageSource>();

        }

        internal void NotifyImage()
        {
            Notify("Image");
        }

        private SolidColorBrush _debugBrush = null;
        public SolidColorBrush DebugBrush
        {
            get { return _debugBrush; }
            set { _debugBrush = value; Notify("DebugBrush"); }
        }
    }



    public sealed class DisplayFluor : Notifier, ICloneable
    {
        private Fluor scnFluor;
        private bool isVisible;
        private byte histoLow, histoHigh;

        public DisplayFluor(Fluor scnFluor)
        {
            this.scnFluor = scnFluor;
            this.isVisible = true;
            this.histoLow = 0;
            this.histoHigh = 255;
        }

        public Fluor SCNFluor
        {
            get { return scnFluor; }
        }

        public string Name
        {
            get { return scnFluor.Name; }
        }

        public Color RenderColor
        {
            get { return scnFluor.RenderColor; }
            set { scnFluor.RenderColor = value; Notify("RenderColor", "RenderColorBrush"); }
        }
        public Brush RenderColorBrush { get { return new SolidColorBrush(RenderColor); } }

        public bool IsVisible
        {
            get { return isVisible; }
            set { isVisible = value; Notify("IsVisible"); }
        }

        public byte HistogramLow
        {
            get { return histoLow; }
            set { histoLow = value; Notify("HistogramLow"); }
        }

        public byte HistogramHigh
        {
            get { return histoHigh; }
            set { histoHigh = value; Notify("HistogramHigh"); }
        }

        
        #region ICloneable Members

        private DisplayFluor()
        {
        }

        public object Clone()
        {
            var f = new DisplayFluor();
            f.histoHigh = histoHigh;
            f.histoLow = histoLow;
            f.isVisible = isVisible;
            f.scnFluor = scnFluor;
            return f;
        }

        #endregion
    }
}
