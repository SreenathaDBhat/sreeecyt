﻿using System;
using System.Windows.Data;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Globalization;

namespace AI.Converters
{
    public class TranslateTransformConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[2] == DependencyProperty.UnsetValue || values[4] == DependencyProperty.UnsetValue)
                return 0.0;

            double actualWidth = (double)values[0];
            double containerWidth = (double)values[1];
            double lookAt = (double)values[2];
            double mag = (double)values[3];
            double range = (double)values[4];
            double scaledWidth = actualWidth * mag;

            double offset = (lookAt - (range / 2)) * mag;
            var d = (containerWidth / 2) - (scaledWidth / 2) - offset;

            return d;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(object), typeof(Visibility))]
    public class NullToVisibilityConverter : IValueConverter
    {
        public Visibility ValueWhenNull { get; set; }
        public Visibility ValueWhenNotNull { get; set; }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value == null ? ValueWhenNull : ValueWhenNotNull;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    [ValueConversion(typeof(object), typeof(Visibility))]
    public class NullToBoolConverter : IValueConverter
    {
        public bool ValueWhenNull { get; set; }
        public bool ValueWhenNotNull { get; set; }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value == null ? ValueWhenNull : ValueWhenNotNull;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    [ValueConversion(typeof(object), typeof(Visibility))]
    public class BoolToVisibilityConverter : IValueConverter
    {
        public Visibility ValueWhenTrue { get; set; }
        public Visibility ValueWhenFalse { get; set; }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)value ? ValueWhenTrue : ValueWhenFalse;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class ConstantDoubleMultiplier : IValueConverter
    {
        public double Factor { get; set; }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is double)
                return (double)value * Factor;
            else if (value is int)
                return (int)value * Factor;
            else
                throw new NotSupportedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class ParameterDoubleMultiplier : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double v = (double)value;
            string p = (string)parameter;
            double dp = double.Parse(p);
            return v * dp;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class ParameterDoubleOffset : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double v = (double)value;
            string p = (string)parameter;
            double dp = double.Parse(p);
            return v + dp;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class AddAllDoubles : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double d = 1;

            foreach (object dd in values)
                if (dd != DependencyProperty.UnsetValue)
                    d += (double)dd;

            return d;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class IntToBoolConverter : IValueConverter
    {
        public bool ValueWhenZero { get; set; }
        public bool ValueWhenNonZero { get; set; }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (int)value == 0 ? ValueWhenZero : ValueWhenNonZero;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }


    public class IntToVisibilityConverter : IValueConverter
    {
        public Visibility ValueWhenZero { get; set; }
        public Visibility ValueWhenNonZero { get; set; }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is int)
            {
                return (int)value == 0 ? ValueWhenZero : ValueWhenNonZero;
            }
            else if (value is float)
            {
                return Math.Abs((float)value) < float.Epsilon ? ValueWhenZero : ValueWhenNonZero;
            }
            else if (value is double)
            {
                return Math.Abs((double)value) < double.Epsilon ? ValueWhenZero : ValueWhenNonZero;
            }
            else
            {
                return ValueWhenZero;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class NumToVisibilityConverter : IValueConverter
    {
        public double Number { get; set; }
        public Visibility ValueWhenMatched { get; set; }
        public Visibility ValueWhenNotMatched { get; set; }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double v = 0;
            if (value is int)
                v = (int)value;
            else if (value is float)
                v = (float)value;
            else if (value is double)
                v = (double)value;

            return Math.Abs(v - Number) < 0.01 ? ValueWhenMatched : ValueWhenNotMatched;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class RelativeIntToVisibilityConverter : IValueConverter
    {
        public int ReferenceValue { get; set; }
        public Visibility VisibilityWhenLessThanReferenceValue { get; set; }
        public Visibility VisibilityWhenGreaterThanOrEqualToReferenceValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double v = 0;
            if (value is int)
                v = (int)value;
            else if (value is float)
                v = (float)value;
            else if (value is double)
                v = (double)value;

            return v < ReferenceValue ? VisibilityWhenLessThanReferenceValue : VisibilityWhenGreaterThanOrEqualToReferenceValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class KnownHeightAspectBasedMultiplierForBitmapSources : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var bmp = value as BitmapSource;
            if (bmp == null)
                return 1;

            double aspect = (double)bmp.PixelWidth / (double)bmp.PixelHeight;
            double p = double.Parse((string)parameter);
            return aspect * p;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class KnownWidthAspectBasedMultiplierForBitmapSources : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var bmp = (BitmapSource)value;
            double aspect = bmp.PixelHeight / (double)bmp.PixelWidth;
            double p = double.Parse((string)parameter);
            return aspect * p;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class BoolInvertConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class RandomNumberGenerator : IValueConverter
    {
        public double Minimum { get; set; }
        public double Maximum { get; set; }
        private Random rand;

        public RandomNumberGenerator()
        {
            Minimum = 0;
            Maximum = 1;
            rand = new Random();
        }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (rand.NextDouble() * (Maximum - Minimum)) + Minimum;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class ColorToBrushConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Color color = (Color)value;
            return new SolidColorBrush(color);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class BoolToBrushConverter : IValueConverter
    {
        #region IValueConverter Members

        public Color ColourWhenTrue { get; set; }
        public Color ColourWhenFalse { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new SolidColorBrush(((bool)value == true) ? ColourWhenTrue : ColourWhenFalse);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    [ValueConversion(typeof(string), typeof(bool))]
    public class StringMatchConverter : IValueConverter
    {
        public string Text { get; set; }

        private bool invert = false;
        public bool Invert
        {
            get { return invert; }
            set { invert = value; }
        }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (Invert)
                return (bool)(Text != (string)value);
            else
                return (bool)(Text == (string)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class DoubleAdditionConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            foreach (var o in values)
                if (!(o is double))
                    return 0.0;

            double t = 0;
            foreach (double d in values)
            {
                t += d;
            }
            return t;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class InequalityToVisibilityConverter : IValueConverter
    {
        private double? _gte, _lt;
        public double GreaterThanOrEqualTo { get { return _gte.HasValue ? _gte.Value : 0; } set { _gte = value; } }
        public double LessThan { get { return _lt.HasValue ? _lt.Value : 0; } set { _lt = value; } }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return Visibility.Collapsed;

            double n = 0;
            if (value is int) n = (int)value;
            else if (value is double) n = (double)value;
            else if (value is float) n = (float)value;
            else throw new NotSupportedException("Unsupported input type: " + value.GetType().Name);

            if (_gte.HasValue)
                return n >= GreaterThanOrEqualTo ? Visibility.Visible : Visibility.Collapsed;
            else if (_lt.HasValue)
                return n < LessThan ? Visibility.Visible : Visibility.Collapsed;
            else
                throw new NotSupportedException("Set either of the inequality properties");
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(object), typeof(string))]
    public class StringToDateTimeConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }
            return ((DateTime)value).ToString(parameter as string, CultureInfo.InvariantCulture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrEmpty(value as string))
            {
                return null;
            }
            try
            {
                DateTime dt = DateTime.ParseExact(value as string, parameter as string, CultureInfo.InvariantCulture);
                return dt as DateTime?;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
