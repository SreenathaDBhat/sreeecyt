﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace AI
{
    public static class MakeXmlUseful
    {
        /// <summary>
        /// WARNING: This is SLOW. Dont use for even moderately sized xml.
        /// 
        /// NOTE: This was needed ages ago, when DIH favoured pretend xml.
        /// It's likely that this not the case anymore and we'll only cause problems doing this...
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static XElement StripXMLNamespacesAndParse(this string xml2)
        {
            var xml = xml2;
            xml = Regex.Replace(xml, "<(.*?) xmlns[:=].*?>", "<$1>");
            xml = Regex.Replace(xml, "\\w+:(\\w+=\".*?\")", "$1");
            xml = Regex.Replace(xml, "<\\w+:(.*?)>", "<$1>");
            xml = Regex.Replace(xml, "</\\w+:(\\w+)>", "</$1>");

            if (xml.StartsWith("﻿<?xml version=\"1.0\" encoding=\"utf-8\"?>"))
                xml = xml.Substring("﻿<?xml version=\"1.0\" encoding=\"utf-8\"?>".Length);

            var parsed = XElement.Parse(xml);
            return parsed;
        }

        /// <summary>
        /// Same as StripFakeXMLNamespacesAndParse but with some funky pre-processing
        /// to compensate for the NOT-REAL-XML responses DIH loves so.
        /// </summary>
        /// <param name="xml2"></param>
        /// <returns></returns>
        public static XElement StripFakeXMLNamespacesAndParse(this string xml2)
        {
            var xml = xml2;

            if (xml.StartsWith("﻿<?xml version=\"1.0\" encoding=\"utf-8\"?>"))
                xml = xml.Substring("﻿<?xml version=\"1.0\" encoding=\"utf-8\"?>".Length);

            xml = Regex.Replace(xml, "<(.*?) xmlns[:=].*?>", "<$1>");
            xml = Regex.Replace(xml, "\\w+:(\\w+=\".*?\")", "$1");
            xml = Regex.Replace(xml, "<\\w+:(.*?)>", "<$1>");
            xml = Regex.Replace(xml, "</\\w+:(\\w+)>", "</$1>");

            // BUGFIX: this cocks with RequestDirectoryList when the items have &s in them
            // Left it commented out rather than delete because it looks like it was a fix
            // itself...???
            //xml = xml.Replace("&", "&amp;"); // thinks it's a stylesheet lol

            xml = xml.Replace("getTileWidth()", "getTileWidth");
            xml = xml.Replace("getTileHeight()", "getTileHeight");


            xml = xml.Replace("<></>", ""); // HACK FIX: the DIH sometimes gives us back some dodgy ass shit.

            var parsed = XElement.Parse(xml);
            return parsed;
        }
    }
}
