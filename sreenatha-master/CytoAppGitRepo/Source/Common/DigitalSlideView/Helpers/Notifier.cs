﻿using System;
using System.ComponentModel;
using System.Windows.Threading;
using System.Threading;
using System.Windows;

namespace AI
{
    public class Notifier : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Notify(Dispatcher disp, string propertyName)
        {
            disp.Invoke((ThreadStart)delegate
            {
                Notify("Slides");

            }, DispatcherPriority.Normal);
        }

        public void Notify(params string[] propertyNames)
        {
            if (PropertyChanged != null)
            {
                foreach (var propertyName in propertyNames)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        public void Notify(Dispatcher disp, params string[] propertyNames)
        {
            disp.Invoke((ThreadStart)delegate
            {
                Notify(propertyNames);

            }, DispatcherPriority.Normal);
        }
    }
}
