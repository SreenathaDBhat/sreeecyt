﻿using System;

namespace AI
{
    public class TypedWeakReference<T> where T: class
    {
        private WeakReference _ref;

        public TypedWeakReference(T t)
        {
            _ref = new WeakReference(t);
        }

        public T Target
        {
            get { return _ref.IsAlive ? (T)_ref.Target : null; }
        }
    }
}
