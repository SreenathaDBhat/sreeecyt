﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;

namespace AI.Imp
{
    public class Channel
    {
        private byte[] pixels;
        private int width;
        private int height;
        private Color color;
        private string description;

        public Channel()
        {
        }

        public Channel(byte[] pixels, int width, int height, Color color)
        {
            this.pixels = pixels;
            this.width = width;
            this.height = height;
            this.color = color;
        }

        public Channel(byte[] pixels, int width, int height, string description) : this(pixels, width, height, Colors.White)
        {
            this.Description = description;
        }

        public Channel(byte[] pixels, int width, int height) : this(pixels, width, height, Colors.White)
        {
        }

        public Channel(int width, int height, Color color)
        {
            this.pixels = new byte[width * height];
            this.width = width;
            this.height = height;
            this.color = color;
        }

        public Channel(int width, int height) : this(width, height, Colors.White)
        {
        }

        public static Channel WhiteMask(int width, int height, string description, Color renderColour)
        {
            Channel mask = new Channel(width, height, Colors.White);
            mask.Description = description;
            mask.Color = renderColour;

            int len = width * height;
            for (int i = 0; i < len; ++i)
            {
                mask.pixels[i] = 255;
            }

            return mask;
        }

        public byte[] Pixels
        {
            get { return pixels; }
        }

        public int Width
        {
            get { return width; }
        }

        public int Height
        {
            get { return height; }
        }

        public byte GetPixel(int x, int y)
        {
            return pixels[y * width + x];
        }

        public void SetPixel(int x, int y, byte value)
        {
            pixels[y * width + x] = value;
        }

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }
    }



    public class Image : IEnumerable<Channel>
    {
        private List<Channel> channels = new List<Channel>();
        private int width;
        private int height;

        public Image(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        public void AddChannel(Channel channel)
        {
            if (channels.Count > 0)
            {
                if (channel.Width != width || channel.Height != height)
                    throw new NotSupportedException("All channels must be same size");
            }

            channels.Add(channel);
        }

        public static Image FromChannel(Channel channel)
        {
            Image img = new Image(channel.Width, channel.Height);
            img.AddChannel(channel);
            return img;
        }

        public IEnumerator<Channel> GetEnumerator()
        {
            return channels.GetEnumerator();
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return channels.GetEnumerator();
        }

        public byte GetPixel(int channelIndex, int x, int y)
        {
            return channels[channelIndex].GetPixel(x, y);
        }

        public void SetPixel(int channelIndex, int x, int y, byte value)
        {
            channels[channelIndex].SetPixel(x, y, value);
        }

        public int Width
        {
            get { return width; }
        }

        public int Height
        {
            get { return height; }
        }

        public int NumChannels
        {
            get { return channels.Count; }
        }

        public IList<Channel> Channels
        {
            get { return channels; }
        }

        public Channel AddChannel(Color color, string description)
        {
            Channel ch = new Channel(width, height, color);
            ch.Description = description;
            channels.Add(ch);
            return ch;
        }


    }
}
