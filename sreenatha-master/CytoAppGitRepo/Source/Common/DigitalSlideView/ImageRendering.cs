﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Windows.Controls;
using AI.Imaging;

namespace AI
{
    public static class ImageRendering
    {
        public static ImageSource BuildOverviewImage(this ISlideImageCollection scn, double outputWidth)
        {
            double outputHeight = outputWidth * (scn.Bounds.Height / (double)scn.Bounds.Width);
            RenderTargetBitmap bmp = new RenderTargetBitmap((int)outputWidth, (int)outputHeight, 96, 96, PixelFormats.Pbgra32);
            RenderOptions.SetBitmapScalingMode(bmp, BitmapScalingMode.NearestNeighbor);

            var overviewImage = FindOverviewImage(scn);

            double scaleX = outputWidth / overviewImage.Position.Width;
            double scaleY = outputHeight / overviewImage.Position.Height;
            var size = new Size(outputWidth, outputHeight);

            var resolution = BestResolution(overviewImage, 3);
            var tiles = overviewImage.VisibleTiles(resolution, overviewImage.Position);

            var visual = new DrawingVisual();

            //Anti-Aliasing gives us white lines, at certain scales, can only switch off anti aliasing for a group, so add tiles to this
            DrawingGroup group = new DrawingGroup();

            foreach (var t in tiles)
            {
                var tileImage = TileRenderer.Render(scn, overviewImage, t, 0);
                if (tileImage == null)
                    continue;

                if (t.ContentSize.Width < 0.99 || t.ContentSize.Height < 0.99)
                {
                    if (t.ContentSize.Width < 0.05 || t.ContentSize.Height < 0.05)
                    {
                        continue;
                    }

                    var crop = new Int32Rect(0, 0, (int)(tileImage.PixelWidth * t.ContentSize.Width), (int)(tileImage.PixelHeight * t.ContentSize.Height));
                    tileImage = new CroppedBitmap(tileImage, crop);
                }

                var r = new Rect(t.Position.X * scaleX, t.Position.Y * scaleY, t.Position.Width * scaleX, t.Position.Height * scaleY);
                var imageDrawing = new ImageDrawing(tileImage, r);
                group.Children.Add(imageDrawing);
            }

            using (var d = visual.RenderOpen())
            {
                // Switch off anti-aliasing else white lines
                RenderOptions.SetEdgeMode(group, EdgeMode.Aliased);
                d.DrawDrawing(group);
            }

            bmp.Render(visual);
            bmp.Freeze();

            int w = (int)outputWidth;
            int h = (int)outputHeight;
            var bytes = new byte[w * h * 4];
            bmp.CopyPixels(bytes, w * 4, 0);

            var bb = BitmapSource.Create(w, h, 96, 96, PixelFormats.Pbgra32, null, bytes, w * 4);
            bb.Freeze();
            return bb;
        }

        private static int BestResolution(SlideImage image, int targetNumTiles)
        {
            var resolutions = (from d in image.Dimensions
                               select d.Resolution).Distinct();

            int bestRes = -1;
            int bestDelta = 100000;

            foreach (var r in resolutions)
            {
                var num = image.VisibleTiles(r, image.Position).Count();
                var delta = Math.Abs(num - targetNumTiles);
                
                if (bestRes == -1 || delta < bestDelta)
                {
                    bestDelta = delta;
                    bestRes = r;
                }
            }

            return bestRes;
        }

        private static SlideImage FindOverviewImage(ISlideImageCollection imageCollection)
        {
            SlideImage img = null;

            foreach (var i in imageCollection.Images)
            {
                if (img == null || (i.TileSizeMicronsForBestResulutionTile >= img.TileSizeMicronsForBestResulutionTile && i.Position.Width * i.Position.Height > img.Position.Width * img.Position.Height))
                    img = i;
            }

            return img;
        }
    }
}
