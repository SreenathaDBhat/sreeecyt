﻿using System;
using System.Linq;
using AI.SCN;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using AI.Imaging;
using System.Collections.Generic;

namespace AI.Imp
{
    public static class ImageSifter
    {
        public static IEnumerable<SlideImage> ImagesForAnalysis(this ISlideImageCollection imageCollection, Rect region)
        {
            var hits = ImagesInRegion(imageCollection, region, true);
            hits = hits.Where(h => h.TileSizeMicronsForBestResulutionTile < 1000);
            return hits;
        }

        public static IEnumerable<SlideImage> ImagesInRegion(this ISlideImageCollection imageCollection, Rect region, bool forAnalysis )
        {
            var overlapping = imageCollection.Images.Where(i => Rect.Intersect(i.Position, region) != Rect.Empty);

            var images = new List<SlideImage>();
            foreach (var o in overlapping)
            {
                int highestRes = (from d in o.Dimensions select d.Resolution).Min();

                var dimension = o.Dimensions.Where(d => d.Resolution == highestRes).First();
                // Limit to useful dimensions, empirical hack here
                if ( dimension.TileSizeMicrons.Width <= 300.0)
                    images.Add(o);
            }
            return images;
        }

        public static int CountTilesToBeAnalysed(this ISlideImageCollection imageCollection, Rect region)
        {
            var images = ImagesForAnalysis(imageCollection, region);
            return CountTileInImages(images, region);
        }

        public static int CountVisibleTiles(this ISlideImageCollection imageCollection, Rect region)
        {
            var images = ImagesInRegion(imageCollection, region, false);
            return CountTileInImages(images, region);
        }

        private static int CountTileInImages(IEnumerable<SlideImage> images, Rect region)
        {
            int cnt = 0;
            if (images == null || images.Count() == 0)
                return 0;

            int highestRes = (from d in images.FirstOrDefault().Dimensions select d.Resolution).Min();

            foreach (var image in images)
            {
                var tiles = image.VisibleTiles(highestRes, region).ToArray();
                if (tiles != null)
                    cnt += tiles.Count();
            }
            return cnt;
        }

        public static BitmapSource RenderRegionForAnalysis(this ISlideImageCollection imageCollection, Rect region, int z)
        {            
            var regionImages = imageCollection.ImagesForAnalysis(region);
            if (0 < regionImages.Count())
            {
                var firstImage = regionImages.FirstOrDefault();
                int highestRes = (from d in firstImage.Dimensions select d.Resolution).Min(); // lower number = higher res.
                var dimension = firstImage.Dimensions.Where(d => d.Resolution == highestRes).First();
                var tileSizeMicrons = firstImage.Position.Width / dimension.NumberOfTileColumns;

                double scalex = (512 / tileSizeMicrons);
                double scaley = (512 / tileSizeMicrons);

                var visual = new DrawingVisual();
                //Anti-Aliasing gives us white lines, at certain scales, can only switch off anti aliasing for a group, so add tiles to this
                DrawingGroup group = new DrawingGroup();

                foreach (var regionImage in regionImages)
                {
                    var tiles = regionImage.VisibleTiles(highestRes, region);

                    foreach (var t in tiles)
                    {
                        var tileImage = TileRenderer.Render(imageCollection, regionImage, t, z);

                        if (t.ContentSize.Width < 0.99 || t.ContentSize.Height < 0.99)
                        {
                            int cw = (int)(tileImage.PixelWidth * t.ContentSize.Width);
                            int ch = (int)(tileImage.PixelHeight * t.ContentSize.Height);

                            var crop = new Int32Rect(0, 0, cw, ch);
                            if (crop.Width < 5 || crop.Height < 5)
                                return null;

                            tileImage = new CroppedBitmap(tileImage, crop);
                        }

                        var r = new Rect((t.Position.X - region.Left) * scalex, (t.Position.Y - region.Y) * scaley, t.Position.Width * scalex, t.Position.Height * scaley);

                        var imageDrawing = new ImageDrawing(tileImage, r);
                        group.Children.Add(imageDrawing);
                    }
                }

                using (var d = visual.RenderOpen())
                {
                    // Switch off anti-aliasing else white lines
                    RenderOptions.SetEdgeMode(group, EdgeMode.Aliased);
                    d.DrawDrawing(group);
                }

                // byte align, only necessry for dib stuff in vbscript
                int w = (int)(region.Width * scalex);
                int h = (int)(region.Height * scaley);

                w = (w / 4) * 4;

                RenderTargetBitmap bmp = new RenderTargetBitmap(w, h, 96, 96, PixelFormats.Pbgra32);
                bmp.Render(visual);
                bmp.Freeze();
                return bmp;
            }
            else
            {
                // Return white tile if no valid tile available
                var visual = new DrawingVisual();
                RenderTargetBitmap bmp = new RenderTargetBitmap(512, 512, 96, 96, PixelFormats.Pbgra32);
                bmp.Render(visual);
                bmp.Freeze();
                return bmp;
            }            
        }
    }
}
