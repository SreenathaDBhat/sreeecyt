﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;
using System.Threading.Tasks;
using AI.Imaging;
using System.Windows.Media.Imaging;

namespace AI
{
    public interface ISlideImageCollection
    {
        Rect Bounds { get; }
        IEnumerable<SlideImage> Images { get; }
        string Name { get; }
        string Filename { get; }
        int MaxIndexZ { get; }
        bool ContainsMaxProjAtZIndexZero { get; }
        Point RealOrigin { get; }
        Size SlideDimensions { get; }
        bool IsMeasurable { get; }
        bool IsAnalysable { get; }
        bool HasFluorescent { get; }
        string FormatType { get; }
        DisplayFluor[] Fluors { get; }

        Task<ImageSource> LoadThumb();
        Task<BitmapSource> ProvideTileImage(int x, int y, int z, int scale, IEnumerable<Fluor> includedFluors, int jpegQuality);

        ISlideImageTileRequest ProvideTileImageAsync(Tile t, TileLoadInfo[] dimensions, ISlideImageTileDelegate callbackDelegate, object context, IlluminationType illumination);
        byte[] ProvideTileImage(int dimensionID, int tileID);

        IEnumerable<string> ObjectivesUsed { get;}
        string DeviceModel { get; }
        string Barcode { get; }
        string SCNVersion { get; }

        void Release(); // for scn anyway we need it to close the tiff immediately rather than garabage collection
    }


    public delegate bool NeedTileDelegate(Rect tilePosition);

    public sealed class TileLoadInfo
    {
        public byte[] ImageData { get; set; }
        public Dimension Dimension { get; private set; }

        private TileLoadInfo()
        {
        }

        public static TileLoadInfo LoadRequest(Dimension dimension)
        {
            var t = new TileLoadInfo();
            t.Dimension = dimension;
            return t;
        }
    }

    public interface ISlideImageTileDelegate
    {
        void SlideImageTileDidLoad(ISlideImageTileRequest request, IEnumerable<TileLoadInfo> tiles);
        void SlideImageTileDidFailToLoad(ISlideImageTileRequest request, TileLoadInfo tile);
    }

    public interface ISlideImageTileRequest
    {
        void Start();
        void Cancel();

        Tile Tile { get; }
        IEnumerable<TileLoadInfo> Dimensions { get; }
        object Context { get; }
        IlluminationType Illumination { get; }
        void LoadTileImage();
    }

    public static class ISlideImageTileRequestExt
    {
        public static int Z(this ISlideImageTileRequest r)
        {
            var d = r.Dimensions.FirstOrDefault();
            return d == null ? -1 : d.Dimension.Z;
        }
    }
}
