﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;

namespace AI.Imaging
{
    public enum IlluminationType
    {
        Brightfield,
        Fluorescent
    }

    public class Fluor
    {
        public static Fluor Brightfield = new Fluor("BF", Colors.White);

        // Fix Mantis #9310: SCN Ariol Multifocal Fluorescence slide doesn't display z-stack slider in DIH 4.02 as in SGC 2.1
        // Added focal planes attribute
        public int FocalPlanes { get; set; }
        public string Name { get; internal set; }
        public Color RenderColor { get; internal set; }
        public Brush RenderColorBrush { get { return new SolidColorBrush(RenderColor); } }
        public int Tag { get; set; } // used for whatever..?
        public virtual bool CanModifyColour { get { return true;}}

        // Fix Mantis #9310: SCN Ariol Multifocal Fluorescence slide doesn't display z-stack slider in DIH 4.02 as in SGC 2.1
        // Added optional focal planes parameter in the constructor
        public Fluor(string name, Color renderColor, int focalPlanes=1)
        {
            Name = name;
            RenderColor = renderColor;
            FocalPlanes = focalPlanes;

            if (RenderColor.A == 0)
                RenderColor = Color.FromArgb(255, RenderColor.R, RenderColor.G, RenderColor.B);
        }


        private static byte ColorizeByte(uint baseValue, byte x)
        {
            if (baseValue == 0)
                return 0;
            else if (baseValue == 255)
                return x;

            uint result = x * baseValue;

            if (result % 255 < 128)
                result = result / 255;
            else
                result = 1 + result / 255;

            return (byte)result;
        }
 


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rgb"></param>
        /// <param name="gray">0..1</param>
        public unsafe virtual void GrayToRGB(byte* rgb, float gray)
        {
            var g = (byte)(gray * 255);
            *rgb = ColorizeByte(g, RenderColor.R);
            *(rgb + 1) = ColorizeByte(g, RenderColor.G);
            *(rgb + 2) = ColorizeByte(g, RenderColor.B);
        }
    }

    public class Dimension
    {
        public int Resolution { get; private set; }
        public int Z { get; private set; }
        public Fluor Fluor { get; private set; }

        public int NumberOfTileColumns { get; private set; }
        public int NumberOfTileRows { get; private set; }
        public int Identifier { get; internal set; }

        public Size TileSizeMicrons { get; private set; }

        public Dimension(int res, int z, Fluor fl, int cols, int rows, int identifier, Size tileSizeMicrons)
        {
            Resolution = res;
            Z = z;
            Fluor = fl;
            NumberOfTileColumns = cols;
            NumberOfTileRows = rows;
            Identifier = identifier;
            TileSizeMicrons = tileSizeMicrons;
        }

        public Dimension(int res, int z, Fluor fl, int cols, int rows, int identifier, double wm, double hm)
            : this(res, z, fl, cols, rows, identifier, new Size(wm, hm))
        {
        }
    }

    public class Tile
    {
        public Rect Position { get; internal set; }
        public int Resolution { get; internal set; }
        public Size ContentSize { get; internal set; }
        public int Identifier { get; internal set; }
        public bool IsTopLevelTile { get; internal set; }

        public Tile(Rect pos, int res, Size contentSize, int identifier, bool isTopLevel)
        {
            Position = pos;
            Resolution = res;
            ContentSize = contentSize;
            Identifier = identifier;
            IsTopLevelTile = isTopLevel;
        }
    }

    public class SlideImage
    {
        public string Name { get; internal set; }
        public Rect Position { get; internal set; }
        public IlluminationType Illumination { get; internal set; }
        public IEnumerable<Dimension> Dimensions { get; internal set; }
        public IEnumerable<Fluor> Fluors { get; internal set; }
        public int WorstResolution { get; private set; }
        public bool ContainsMaxProjAtZIndexZero { get; private set; }

        public SlideImage(string name, Rect pos, IlluminationType illum, IEnumerable<Dimension> dims, IEnumerable<Fluor> fluors, bool containsMaxProjAtZIndexZero = false)
        {
            Name = name;
            Position = pos;
            Illumination = illum;
            Dimensions = dims;           
            // Fix Mantis #9114 - LAN Z-Stacked slide images slider bar is missing
            // Added brightfield channel if no fluorescence channels defined
            Fluors = (IlluminationType.Brightfield != Illumination) ? fluors : new Fluor[0];

            ContainsMaxProjAtZIndexZero = containsMaxProjAtZIndexZero;
            
            WorstResolution = (from d in dims select d.Resolution).Max();
        }

        public int TileSizeMicronsForBestResulutionTile
        {
            get
            {
                Dimension bestResDimension = null;
                foreach (var d in Dimensions)
                {
                    if(bestResDimension == null || d.TileSizeMicrons.Width < bestResDimension.TileSizeMicrons.Width)
                        bestResDimension = d;
                }

                if (bestResDimension == null)
                    return -1;

                return (int)bestResDimension.TileSizeMicrons.Width;
            }
        }
    }
}
