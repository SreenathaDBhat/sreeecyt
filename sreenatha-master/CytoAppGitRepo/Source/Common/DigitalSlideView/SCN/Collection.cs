﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Diagnostics;
using BitMiracle.LibTiff.Classic;
using AI.Imaging;
using System.Threading;
using System.Windows.Threading;
using System.Threading.Tasks;
using System.Windows.Media;
using AI.Imp;
using System.Windows.Media.Imaging;

namespace AI.SCN
{
    /// <summary>
    /// Represents an SCN (Big)Tiff file.
    /// </summary>
    public sealed partial class ImageCollection : ISlideImageCollection
    {
        internal const int TilePixelSize = 512;

        private Tiff tiff;
        private int labelImageIdentifier;
        private string slideName;
        private SlideImage[] images;
        private Tiff.Tile[][] tiles;
        
        /// <summary>
        /// Open a SCN file and parse the image structure.
        /// </summary>
        /// <param name="filename">Path to the SCN tiff.</param>
        public ImageCollection(string filename)
        {
            tiff = null;
            while (tiff == null)
            {
                tiff = Tiff.Open(filename, "r");
                if (tiff == null)
                    Thread.Sleep(5); 
            }
            

            slideName = System.IO.Path.GetFileNameWithoutExtension(filename);
            Filename = filename;

            ParseXmlFromTiff();

            tiles = tiff.CacheOffsets();

            // Fix Mantis #9209: SCN and DICOM DIR multichanel controls are no showing on LAN	
            // Fix Mantis #9192: Z-stack image chanel controls are not working	
            // Load fluorescence channels
            LoadFluoroChannels();
        }

        // Fixed Mantis #9260: Image and thumbnail are displaying vertically instead of horizontal as it is in DIH 4.02
        // Added file type property
        private string formatType = "leicascn";
        public string FormatType { get { return formatType; } }

        // Fix Mantis #9209: SCN and DICOM DIR multichanel controls are no showing on LAN	
        // Fix Mantis #9192: Z-stack image chanel controls are not working	
        // Load fluorescence channels
        private void LoadFluoroChannels()
        {
            int fluoroMaxPlanes = 0;
            Dictionary<string, DisplayFluor> fluorMap = new Dictionary<string, DisplayFluor>();
            //fluorMap.Add("Brightfield", Fluor.Brightfield);
            foreach (SlideImage img in images)
            {
                foreach (Fluor fluor in img.Fluors)
                {
                    if (!fluorMap.ContainsKey(fluor.Name))
                    {
                        fluoroMaxPlanes = Math.Max(fluoroMaxPlanes, fluor.FocalPlanes);
                        fluorMap.Add(fluor.Name, new DisplayFluor(fluor));
                    }
                }
            }
            // Fixed applied for Mantis #9217: DICOMDIR Image is showing an incorrect channel control
            // Check if only overview channel is available (the same for SCN?)
            if (0>=fluoroMaxPlanes || fluorMap.Values.Count() == 1 && fluorMap.First().Value.Name == "Overview")
            {
                // brightfield image
                slideFluors = new DisplayFluor[0];
            }
            else
            {
                // fluorescence image
                slideFluors = fluorMap.Values.ToArray();

                // focal planes can be associated to each single channel or the full image (?)
                this.MaxIndexZ = Math.Max(fluoroMaxPlanes - 1, MaxIndexZ);
            }                       
        }

        // Added changes to fix Mantis #9220: Channel controls side of the panel is different than version 2.0
        // Used DisplayFluor instead of Fluor struct to fixed inconsistency in UI events for fluorescence controls
        private DisplayFluor[] slideFluors;
        public DisplayFluor[] Fluors
        {
            get
            {
                // Fix Mantis #9209: SCN and DICOM DIR multichanel controls are no showing on LAN	
                // Fix Mantis #9192: Z-stack image chanel controls are not working	
                // Return loaded fluorescence channels
                return slideFluors;
            }
        }

        #region Dispose

        private void Dispose(bool fromDestructor)
        {
            tiles = null;
            if (tiff != null)
            {
                tiff.Dispose();
                tiff = null;
            }

            if (!fromDestructor)
                GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Makes sure the tiff is closed.
        /// </summary>
        ~ImageCollection()
        {
            Dispose(true);
        }

        /// <summary>
        /// Makes sure the tiff is closed.
        /// </summary>
        public void Dispose()
        {
            Dispose(false);
        }

        #endregion

        public string Name { get { return slideName; } }

        /// <summary>
        /// Images (Scan Areas) contained in this collection (in no particular order).
        /// </summary>
        public IEnumerable<SlideImage> Images { get { return images; } }

        /// <summary>
        /// Overall width of the image collection. A union of the contained images.
        /// </summary>
        public double Width { get { return Bounds.Width; } }

        /// <summary>
        /// Overall Height of the image collection. A union of the contained images.
        /// </summary>
        public double Height { get { return Bounds.Height; } }

        /// <summary>
        /// File this image collection was loaded from.
        /// </summary>
        public string Filename { get; private set; }

        /// <summary>
        /// Maximum Z level (zero-based) of all images.
        /// </summary>
        public int MaxIndexZ { get; private set; }

        /// <summary>
        /// Images contain a maximum projection plane, at a z index of zero.
        /// </summary>
        public bool ContainsMaxProjAtZIndexZero { get; private set; }

        /// <summary>
        /// A union of contained image positions.
        /// </summary>
        public Rect Bounds { get; private set; }

        /// <summary>
        /// the actual origin of the image, the collection always has it at 0,0
        /// </summary>
        public Point RealOrigin { get; private set; }

        /// <summary>
        /// the actual dimensions of a physical slide
        /// </summary>
        public Size SlideDimensions { get; private set; }

        public bool IsMeasurable
        {
            get { return true; }
        }        

        public bool IsAnalysable
        {
            get { return true; }
        }

        public bool HasFluorescent
        {
            get { return Fluors.Length > 0; }
        }


        // discrete list of objectives used in SCN
        public IEnumerable<string> ObjectivesUsed{ get; private set; }

        public string SCNVersion { get; private set; }

        public string DeviceModel { get; private set; }

        public string Barcode { get; private set; }

        public bool supportsV2 { get { return false; } }
        public Task<BitmapSource> ProvideTileImage(int x, int y, int z, int scale, IEnumerable<Fluor> includedFluors, int jpegQuality)
        {
            return Task.Factory.StartNew(() => {
                Rect region = new Rect(x, y, 512, 512);
                BitmapSource bitmapSource = this.RenderRegionForAnalysis(region, z);
                return bitmapSource;
            }, CancellationToken.None);
        }

        /// <summary>
        /// Load the image data for a particular tile in a particular dimension.
        /// </summary>
        /// <param name="tile">Tile to load.</param>
        /// <param name="dimension">Dimension of the tile needed.</param>
        /// <returns>Image data (still encoded into a JPEG, or whatever).</returns>
        public byte[] ProvideTileImage(int dim, int tile)
        {
            try
            {
                var data2 = tiff.ReadTileBaby(tiles[dim][tile]);
                return data2;
            }
            catch (Exception )
            {
                
                return null;
            }
        }

        private byte[] ReadTile(short ifd, int index)
        {
            return tiff.TS_ReadTileData(ifd, index);
        }

        public ISlideImageTileRequest ProvideTileImageAsync(Tile t, TileLoadInfo[] dimensions, ISlideImageTileDelegate callbackDelegate, object context, IlluminationType illum)
        {
            return new TileImageRequest(t, this, callbackDelegate, dimensions, context, illum);
        }

        public Task<ImageSource> LoadThumb()
        {
            return Task.Factory.StartNew(() =>
            {
                return (ImageSource) TileRenderer.CacheOverviewImage(this);
            });
        }

        public ImageSource GetLabel()
        {
            if (labelImageIdentifier >= 0)
            {
                var bytes = ReadTile((short)labelImageIdentifier, 0); // Not actually a tiled image, but works all the same.
                if (bytes != null)
                {
                    BitmapImage bmp = null;
                    var visual = new DrawingVisual();
                    using (var d = visual.RenderOpen())
                    {
                        try
                        {
                            bmp = new BitmapImage();
                            bmp.BeginInit();
                            bmp.CacheOption = BitmapCacheOption.OnLoad;
                            bmp.StreamSource = new MemoryStream(bytes);
                            bmp.EndInit();
                            bmp.Freeze();
                        }
                        catch (Exception)
                        {
                            bmp = null;
                        }
                    }

                    return (ImageSource)bmp;
                }
            }
            return null;
        }

        public Task<ImageSource> LoadLabel()
        {
            return Task.Factory.StartNew(() =>
            {
                return GetLabel();
            });
        }

        public void Release()
        {
            // Close connection to tiff etc
            Dispose();
        }
    }

    public class TileImageRequest : ISlideImageTileRequest
    {
        private CancellationTokenSource cancel;
        private IEnumerable<TileLoadInfo> dimensions;
        private ISlideImageTileDelegate del;
        private ISlideImageCollection image;
        private Tile tile;
        private object context;

        public TileImageRequest(Tile tile, ISlideImageCollection image, ISlideImageTileDelegate callbackDelegate, TileLoadInfo[] dimensions, object context, IlluminationType illum)
        {
            this.context = context;
            this.tile = tile;
            this.del = callbackDelegate;
            this.dimensions = dimensions;
            this.image = image;
            Illumination = illum;
        }

        public object Context { get { return context; } } 
        public Tile Tile { get { return tile; } }
        public IEnumerable<TileLoadInfo> Dimensions { get { return dimensions; } }
        public IlluminationType Illumination { get; private set; }

        public void Start()
        {
            cancel = new CancellationTokenSource();
            var img = image;

            ThreadPool.QueueUserWorkItem(delegate
            {
                Parallel.ForEach(dimensions, d =>
                    {
                        try
                        {
                            d.ImageData = img.ProvideTileImage(d.Dimension.Identifier, tile.Identifier);
                            cancel.Token.ThrowIfCancellationRequested();
                        }
                       catch (Exception )
                        {
                            
                        }
                    });

                if(!cancel.IsCancellationRequested)
                    del.SlideImageTileDidLoad(this, dimensions);
            });
        }

        public void LoadTileImage()
        {
            var img = image;
            Parallel.ForEach(dimensions, d =>
            {
                try
                {
                    d.ImageData = img.ProvideTileImage(d.Dimension.Identifier, tile.Identifier);
                }
                catch (Exception )
                {
                    Trace.WriteLine("TILE FAILED TO LOAD");        
                }
            });
           del.SlideImageTileDidLoad(this, dimensions);
        }

        public void Cancel()
        {
            cancel.Cancel();
        }
    }
}
