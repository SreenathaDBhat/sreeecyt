﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;
using BitMiracle.LibTiff.Classic;
using AI.Imaging;

namespace AI.SCN
{
    internal sealed class Parser
    {
        private enum Version
        {
            Unknown,
            Ariol35,
            TweakedFluorescentStuffNov2010,
        }

        public string Barcode { get; internal set; }
        public int LabelImageIdentifier { get; internal set; }
        public SlideImage[] Images { get; internal set; }
        public Rect Bounds { get; internal set; }
        public int MaxIndexZ { get; internal set; }
        public bool ContainsMaxProjAtZIndexZero { get; internal set; }
        public Point RealOrigin { get; internal set; }
        public Size SlideDimensions { get; internal set; }
        public IEnumerable<string> ObjectivesUsed { get; internal set;}
        public string DeviceModel { get; internal set;}
        public string SCNVersion { get; internal set; } 

        public Parser(string xmlString)
        {
            var version = FigureOutWhatSortOfScnThiIs(xmlString);
            if (version == Version.Unknown)
                throw new NotSupportedException();

            var xml = xmlString.StripXMLNamespacesAndParse();

            // get device model
            var collectionNode = xml.Element("collection");
            DeviceModel = (from c in collectionNode.Descendants("device")
                             select c.Attribute("model").Value).FirstOrDefault();


            LabelImageIdentifier = -1; // Fail-safe default

            SCNVersion = "Unknown";
            if (version == Version.Ariol35)
            {
                SCNVersion = "v1.0";
                AI.SCN.Versions.Ariol35.Parse(this, xml);
            }
            else if (version == Version.TweakedFluorescentStuffNov2010)
            {
                SCNVersion = "v2.0";
                AI.SCN.Versions.FluorescentTweak_Nov2010.Parse(this, xml);
            }
        }

        private Version FigureOutWhatSortOfScnThiIs(string headerString)
        {
            if (headerString.Contains("http://www.leica-microsystems.com/scn/2010/10/01"))
                return Version.TweakedFluorescentStuffNov2010;
            else
                return Version.Ariol35;
        }
    }



    public sealed partial class ImageCollection
    {
        private void ParseXmlFromTiff()
        {
            var xmlString = tiff.GetSCNHeader();
            var parser = new Parser(xmlString);
            Barcode = parser.Barcode;
            labelImageIdentifier = parser.LabelImageIdentifier;
            images = parser.Images;
            Bounds = parser.Bounds;
            MaxIndexZ = parser.MaxIndexZ;
            ContainsMaxProjAtZIndexZero = parser.ContainsMaxProjAtZIndexZero;
            RealOrigin = parser.RealOrigin;
            SlideDimensions = parser.SlideDimensions;
            ObjectivesUsed = parser.ObjectivesUsed;
            DeviceModel = parser.DeviceModel;
            SCNVersion = parser.SCNVersion;
        }

        public XElement Describe()
        {
            return new XElement("Collection",
                new XAttribute("Name", Name),
                from i in images select new XElement("Image",
                                new XAttribute("Illumination", i.Illumination),
                                new XAttribute("Name", i.Name),
                                new XAttribute("X", i.Position.X),
                                new XAttribute("Y", i.Position.Y),
                                new XAttribute("Width", i.Position.Width),
                                new XAttribute("Height", i.Position.Height),
                                from d in i.Dimensions select new XElement("Dimension",
                                            new XAttribute("ID", d.Identifier),
                                            new XAttribute("Columns", d.NumberOfTileColumns),
                                            new XAttribute("Rows", d.NumberOfTileRows),
                                            new XAttribute("Resolution", d.Resolution),
                                            new XAttribute("Z", d.Z),
                                            new XAttribute("WidthMicrons", d.TileSizeMicrons.Width),
                                            new XAttribute("HeightMicrons", d.TileSizeMicrons.Height))
                ));
        }
    }
}
