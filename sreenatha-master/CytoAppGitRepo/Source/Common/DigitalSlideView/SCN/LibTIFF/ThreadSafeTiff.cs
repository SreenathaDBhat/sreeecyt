﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BitMiracle.LibTiff.Classic.Internal;
using System.IO;

namespace BitMiracle.LibTiff.Classic
{
    partial class Tiff
    {
        public byte[] TS_ReadTileData(int ifd, int index)
        {
            long diroff = m_header.tiff_diroff;
            for (int n = ifd; n > 0 && diroff != 0; n--)
            {
                long dummyOff;
                if (!advanceDirectory(ref diroff, out dummyOff))
                    return null;
            }

            TiffDirEntry[] entries;
            fetchDirectory(diroff, out entries, out diroff);
            if (entries == null || entries.Length == 0)
            {
                return null;
            }

            var stripOffsetsEntry = entries.Where(e => e.tdir_tag == TiffTag.STRIPOFFSETS || e.tdir_tag == TiffTag.TILEOFFSETS).FirstOrDefault();
            var stripLengthsEntry = entries.Where(e => e.tdir_tag == TiffTag.STRIPBYTECOUNTS || e.tdir_tag == TiffTag.TILEBYTECOUNTS).FirstOrDefault();

            if (stripLengthsEntry == null || stripOffsetsEntry == null)
                return null;

            long[] stripLengths = new long[stripLengthsEntry.tdir_count];
            fetchLong8Array(stripLengthsEntry, stripLengths);

            long[] stripOffsets = new long[stripOffsetsEntry.tdir_count];
            fetchLong8Array(stripOffsetsEntry, stripOffsets);

            byte[] buf = new byte[stripLengths[index]];

            lock (readLock)
            {
                if (!seekOK(stripOffsets[index]))
                {
                    return null;
                }

                int cc = readFile(buf, 0, buf.Length);
                if (cc != buf.Length)
                {
                    return null;
                }
            }

            return buf;
        }

        private object readLock = new object();

        public class Tile
        {
            public long Offset { get; set; }
            public long Length { get; set; }
        }

        public Tile[][] CacheOffsets()
        {
            List<Tile[]> ifds = new List<Tile[]>();

            long offset = m_header.tiff_diroff;

            while (true)
            {
                if (offset == 0)
                    break;

                seekOK(offset);
                long len;
                readLongOKNF(out len);

                if ((m_flags & TiffFlags.SWAB) == TiffFlags.SWAB)
                    SwabLongR(ref len);

                long thisOffset = offset;
                seekOK(offset);

                long numEntries = 0;
                readLongOKNF(out numEntries);

                TiffDirEntry[] entries = new TiffDirEntry[numEntries];
                int entrySize = sizeof(short) * 2 + sizeof(long) * 2;
                long totalSize = entrySize * numEntries;
                byte[] entryBytes = new byte[totalSize];
                readOK(entryBytes, totalSize);
                readDirEntry(entries, numEntries, entryBytes, 0);

                readLongOKNF(out offset);

                if ((m_flags & TiffFlags.SWAB) == TiffFlags.SWAB)
                    SwabLongR(ref offset);

                
                var stripOffsetsEntry = entries.Where(e => e.tdir_tag == TiffTag.STRIPOFFSETS || e.tdir_tag == TiffTag.TILEOFFSETS).FirstOrDefault();
                var stripLengthsEntry = entries.Where(e => e.tdir_tag == TiffTag.STRIPBYTECOUNTS || e.tdir_tag == TiffTag.TILEBYTECOUNTS).FirstOrDefault();

                long[] stripLengths = new long[stripLengthsEntry.tdir_count];
                fetchLong8Array(stripLengthsEntry, stripLengths);

                long[] stripOffsets = new long[stripOffsetsEntry.tdir_count];
                fetchLong8Array(stripOffsetsEntry, stripOffsets);

                var tiles = from i in Enumerable.Range(0, stripLengthsEntry.tdir_count)
                            select new Tile()
                                {

                                    Offset = stripOffsets[i],
                                    Length = stripLengths[i]
                                };

                ifds.Add(tiles.ToArray());
            }

            return ifds.ToArray();
        }

        public byte[] ReadTileBaby(Tile t)
        {
            byte[] b = new byte[t.Length];

            lock (readLock)
            {
                bool seekedOK = seekOK(t.Offset);
                if (seekedOK)
                {
                    bool read = readOK(b, t.Length);
                    if (!read)
                        return null;
                }
                
            }

            return b;
        }
    }
}
