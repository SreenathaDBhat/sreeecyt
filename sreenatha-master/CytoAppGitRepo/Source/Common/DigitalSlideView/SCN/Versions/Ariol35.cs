﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using AI.Imaging;
using System.Windows;
using System.Windows.Media;
using System.Globalization;

namespace AI.SCN.Versions
{
    internal static class Ariol35
    {
        public static void Parse(Parser p, XElement xml)
        {
            var collectionNode = xml.Element("collection");
            p.Barcode = collectionNode.Attribute("barcode").Value;

            var imgList = new List<SlideImage>();
            int s = 0;
            Point origin = new Point(double.MaxValue, double.MaxValue);

            // GAY: walk the structure to find the origin - in case its not 0,0
            foreach (var snode in collectionNode.Elements("image"))
            {
                var pos = ParseRectFromViewNode(snode.Element("view"));
                origin = new Point(
                    Math.Min(pos.X, origin.X),
                    Math.Min(pos.Y, origin.Y));
            }

            foreach (var snode in collectionNode.Elements("image"))
            {
                var pos = ParseRectFromViewNode(snode.Element("view"));
                pos = new Rect(pos.X - origin.X, pos.Y - origin.Y, pos.Width, pos.Height);

                var illum = ParseIlluminationSource(snode);
                var fluors = ParseFluors(snode);
                var dims = ParseDimensions(pos, snode, fluors);

                SlideImage img = new SlideImage((s++).ToString(), pos, illum, dims, fluors);
                imgList.Add(img);
            }

            p.Images = imgList.ToArray();
            if (p.Images.Length > 0)
            {
                p.Bounds = p.Images.First().Position;
                foreach (var i in p.Images.Skip(1))
                    p.Bounds = Rect.Union(p.Bounds, i.Position);
            }

            int biggestZ = 0;
            foreach (var i in imgList)
            {
                foreach(var d in i.Dimensions)
                {
                    biggestZ = Math.Max(biggestZ, d.Z);
                }
            }

            p.MaxIndexZ = biggestZ;
        }

        private static Dimension[] ParseDimensions(Rect imagePos, XElement snode, Fluor[] fluors)
        {
            var dimList = new List<Dimension>();

            foreach (var dimnode in snode.Element("pixels").Elements("dimension"))
            {
                var w = int.Parse(dimnode.Attribute("sizeX").Value, CultureInfo.InvariantCulture);
                var h = int.Parse(dimnode.Attribute("sizeY").Value, CultureInfo.InvariantCulture);
                var r = int.Parse(dimnode.Attribute("r").Value, CultureInfo.InvariantCulture);
                var z = int.Parse(dimnode.Attribute("z").Value, CultureInfo.InvariantCulture);
                var fl = int.Parse(dimnode.Attribute("c").Value, CultureInfo.InvariantCulture);
                var ifd = int.Parse(dimnode.Attribute("ifd").Value, CultureInfo.InvariantCulture);

                if (z > 0 && r != 0) // only accept multi z stacks when resolution=0  (ie the best quality layer)
                    continue;

                var cols = (int)Math.Ceiling((double)w / ImageCollection.TilePixelSize);
                var rows = (int)Math.Ceiling((double)h / ImageCollection.TilePixelSize);
                var tileSizeMicrons = new Size(ImageCollection.TilePixelSize / (w / imagePos.Width), ImageCollection.TilePixelSize / (h / imagePos.Height));

                var ff = fluors.Length <= fl ? Fluor.Brightfield : fluors[fl];
                dimList.Add(new Dimension(r, z, ff, cols, rows, ifd, tileSizeMicrons));
            }

            return dimList.ToArray();
        }

        private static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        private static Color ToWpfColor(string p)
        {
            var c = System.Drawing.ColorTranslator.FromHtml(p);
            return Color.FromArgb(c.A, c.R, c.G, c.B);
        }

        private static Rect ParseRectFromViewNode(XElement viewNode)
        {
            var r = new Rect(
                double.Parse(viewNode.Attribute("offsetX").Value, CultureInfo.InvariantCulture) / 1000.0,
                double.Parse(viewNode.Attribute("offsetY").Value, CultureInfo.InvariantCulture) / 1000.0,
                double.Parse(viewNode.Attribute("sizeX").Value, CultureInfo.InvariantCulture) / 1000.0,
                double.Parse(viewNode.Attribute("sizeY").Value, CultureInfo.InvariantCulture) / 1000.0);

            //return new Rect(0, 0, r.Width, r.Height);
            return r;
        }

        private static IlluminationType ParseIlluminationSource(XElement snode)
        {
            var scan = snode.Element("scanSettings");
            if (scan == null)
                return IlluminationType.Brightfield;

            var illum = scan.Element("illuminationSettings");
            if (illum == null)
                return IlluminationType.Brightfield;

            return illum.Element("illuminationSource").Value.ToLower() == "brightfield" ? IlluminationType.Brightfield : IlluminationType.Fluorescent;
        }

        private static Fluor[] ParseFluors(XElement snode)
        {
            var channels = snode.Descendants("channel");
            if (channels == null)
                return null;

            var dims = from cn in channels
                       select new
                       {
                           Index = int.Parse(cn.Attribute("index").Value, CultureInfo.InvariantCulture),
                           Name = cn.Attribute("index").Value,
                           RenderColor = ToWpfColor(cn.Element("argb").Value)
                       };

            return (from d in dims
                    orderby d.Index
                    select new Fluor(d.Name, d.RenderColor)).ToArray();
        }
    }
}
