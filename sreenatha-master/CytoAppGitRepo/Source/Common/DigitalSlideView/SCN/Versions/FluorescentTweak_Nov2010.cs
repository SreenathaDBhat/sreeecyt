﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using AI.Imaging;
using System.Xml.Linq;
using System.Windows.Media;
using System.Globalization;
using System.Text.RegularExpressions;

namespace AI.SCN.Versions
{
    internal static class FluorescentTweak_Nov2010
    {
        public static void Parse(Parser p, XElement xml)
        {
            var collectionNode = xml.Element("collection");

            var slideWidth = int.Parse(collectionNode.Attribute("sizeX").Value, CultureInfo.InvariantCulture);
            var slideHeight = int.Parse(collectionNode.Attribute("sizeY").Value, CultureInfo.InvariantCulture);
            p.SlideDimensions = new Size(slideWidth, slideHeight);
            
            var barcodeNode = collectionNode.Element("barcode");
            if (barcodeNode != null)
            {
                var bytes = Convert.FromBase64String(barcodeNode.Value);
                // remove unprintable characters, for some reason barcode has them
                var validBytes = (from b in bytes
                                  where !char.IsControl((char)b)
                                  select b).ToArray();
                p.Barcode = UTF8Encoding.UTF8.GetString(validBytes);
            }
            else
                p.Barcode = null;
            
            var imgList = new List<SlideImage>();
            int s = 0;
            Point origin = new Point(double.MaxValue, double.MaxValue);

            // GAY: walk the structure to find the origin - in case its not 0,0
            foreach (var snode in collectionNode.Elements("image"))
            {
                var pos = ParseRectFromViewNode(snode.Element("view"));
                origin = new Point(Math.Min(pos.X, origin.X), Math.Min(pos.Y, origin.Y));
            }

            foreach (var snode in collectionNode.Elements("image"))
            {
                var pos = ParseRectFromViewNode(snode.Element("view"));
                pos = new Rect(pos.X - origin.X, pos.Y - origin.Y, pos.Width, pos.Height);

                var illum = ParseIlluminationSource(snode);
                var fluors = ParseFluors(snode);

                int projectionZ;
                XAttribute projAttr = snode.Element("pixels").Attribute("projectionZ");
                if (projAttr == null || int.TryParse(projAttr.Value, out projectionZ) != true)
                    projectionZ = -1; // No max projection

                var dims = ParseDimensions(pos, snode, fluors, projectionZ);

                bool containsMaxProjection = (projectionZ >= 0) ? true : false;

                SlideImage img = new SlideImage((s++).ToString(), pos, illum, dims, fluors, containsMaxProjection);
                imgList.Add(img);

                if (!p.ContainsMaxProjAtZIndexZero && containsMaxProjection)
                    p.ContainsMaxProjAtZIndexZero = true;
            }

            foreach (var snode in collectionNode.Elements("supplementalImage"))
            {
                XAttribute typeAttr = snode.Attribute("type");
                XAttribute ifdAttr = snode.Attribute("ifd");
                int ifd;
                if (typeAttr != null && ifdAttr != null && int.TryParse(ifdAttr.Value, out ifd) && ifd >= 0)
                {
                    if (typeAttr.Value.Equals("label"))
                        p.LabelImageIdentifier = ifd;
                }
            }

            p.Images = imgList.ToArray();
            if (p.Images.Length > 0)
            {
                p.Bounds = p.Images.First().Position;
                foreach (var i in p.Images.Skip(1))
                    p.Bounds = Rect.Union(p.Bounds, i.Position);
            }

            int biggestZ = 0;
            foreach (var i in imgList)
            {
                foreach (var d in i.Dimensions)
                {
                    biggestZ = Math.Max(biggestZ, d.Z);
                }
            }

            p.MaxIndexZ = biggestZ;
            p.RealOrigin = origin;
            p.ObjectivesUsed = (from c in collectionNode.Descendants("objective")
                                select c.Value).Distinct().ToList();

        }

        private static Dimension[] ParseDimensions(Rect imagePos, XElement imageNode, Fluor[] fluors, int projectionZ)
        {
            return ParseDims(imageNode.Element("pixels").Elements("dimension"), imagePos, fluors, projectionZ);
        }

        private static Dimension[] ParseDims(IEnumerable<XElement> nodes, Rect imagePos, Fluor[] fluors, int projectionZ)
        {
            var dimList = new List<Dimension>();

            foreach (var dimnode in nodes)
            {
                var w = int.Parse(dimnode.Attribute("sizeX").Value, CultureInfo.InvariantCulture);
                var h = int.Parse(dimnode.Attribute("sizeY").Value, CultureInfo.InvariantCulture);
                var r = int.Parse(dimnode.Attribute("r").Value, CultureInfo.InvariantCulture);
                var zn = dimnode.Attribute("z");
                var fln = dimnode.Attribute("c");
                var ifd = int.Parse(dimnode.Attribute("ifd").Value, CultureInfo.InvariantCulture);

                var z = zn == null ? 0 : int.Parse(zn.Value, CultureInfo.InvariantCulture);
                var fl = fln == null ? 0 : int.Parse(fln.Value, CultureInfo.InvariantCulture);

                //WTF? if (z > 0 && r != 0) // only accept multi z stacks when resolution=0  (ie the best quality layer)
                //    continue;

                var cols = (int)Math.Ceiling((double)w / ImageCollection.TilePixelSize);
                var rows = (int)Math.Ceiling((double)h / ImageCollection.TilePixelSize);
                var tileSizeMicrons = new Size(ImageCollection.TilePixelSize / (w / imagePos.Width), ImageCollection.TilePixelSize / (h / imagePos.Height));

                var theFluor = fluors.Length <= fl ? Fluor.Brightfield : fluors[fl];

                // If there is a maximum projection plane, ensure that it has a z index of zero.
                // One reason for doing this is consistency, in case it is stored at different indexes for different images.
                // If it has to be moved to z=0, shift other planes up into the gap
                if (projectionZ >= 0)
                {
                    if (z == projectionZ)
                        z = 0;
                    else if (z < projectionZ)
                        z += 1;
                }

                dimList.Add(new Dimension(r, z, theFluor, cols, rows, ifd, tileSizeMicrons));
            }

            return dimList.ToArray();
        }

        private static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        private static Color ToWpfColor(string p)
        {
            var c = System.Drawing.ColorTranslator.FromHtml(p);
            return Color.FromArgb(c.A, c.R, c.G, c.B);
        }

        private static Rect ParseRectFromViewNode(XElement viewNode)
        {
            var r = new Rect(
                double.Parse(viewNode.Attribute("offsetX").Value, CultureInfo.InvariantCulture) / 1000.0,
                double.Parse(viewNode.Attribute("offsetY").Value, CultureInfo.InvariantCulture) / 1000.0,
                double.Parse(viewNode.Attribute("sizeX").Value, CultureInfo.InvariantCulture) / 1000.0,
                double.Parse(viewNode.Attribute("sizeY").Value, CultureInfo.InvariantCulture) / 1000.0);

            //return new Rect(0, 0, r.Width, r.Height);
            return r;
        }

        private static IlluminationType ParseIlluminationSource(XElement snode)
        {
            var sn = snode.Element("scanSettings");
            if (sn == null)
                return IlluminationType.Brightfield;

            var ch = sn.Element("channelSettings");
            return ch == null ? IlluminationType.Brightfield : IlluminationType.Fluorescent;
        }

        private static Fluor[] ParseFluors(XElement snode)
        {
            var channels = snode.Descendants("channel");
            if (channels == null)
                return null;

            var dims = from cn in channels
                       select new
                       {
                           Index = int.Parse(cn.Attribute("index").Value, CultureInfo.InvariantCulture),
                           Name = ParseFluorName(cn),
                           RenderColor = ToWpfColor(cn.Attribute("rgb").Value)
                       };

            return (from d in dims
                    orderby d.Index
                    select new Fluor(d.Name, d.RenderColor)).ToArray();
        }

        private static string ParseFluorName(XElement cn)
        {
            var n = cn.Attribute("name").Value;
            var exposureNode = cn.Element("exposureTime");

            if (exposureNode != null)
                n += " " + exposureNode.Value;

            return n;
        }
    }
}
