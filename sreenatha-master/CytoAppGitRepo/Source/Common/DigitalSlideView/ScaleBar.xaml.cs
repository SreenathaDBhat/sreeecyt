﻿using System;
using System.Windows;
using System.Windows.Media;

namespace AI
{
    public partial class ScaleBar
    {
        public static readonly DependencyProperty ViewportProperty = DependencyProperty.Register("Viewport", typeof(SlideViewViewport), typeof(ScaleBar), new FrameworkPropertyMetadata(OnViewportChanged));
        public static readonly DependencyProperty PixelsToMicronsScalingFactorProperty = DependencyProperty.Register("PixelsToMicronsScalingFactor", typeof(double), typeof(ScaleBar), new FrameworkPropertyMetadata(OnScaleChanged));
        public static readonly DependencyProperty UnitsProperty = DependencyProperty.Register("Units", typeof(Unit), typeof(ScaleBar), new FrameworkPropertyMetadata(OnUnitChanged));
        private TokenController _controller;

        public ScaleBar()
        {
            _controller = new TokenController();
            DataContext = _controller;
            InitializeComponent();

            Loaded += (s, e) => CompositionTarget.Rendering += _controller.UpdateValues;
            Unloaded += (s, e) => CompositionTarget.Rendering -= _controller.UpdateValues;
        }

        #region Manually crafted MVVM boilerplate. Unacceptable.

        private static void OnViewportChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ScaleBar sb = (ScaleBar)sender;
            sb._controller.Viewport = e.NewValue as SlideViewViewport;
        }
        private static void OnScaleChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ScaleBar sb = (ScaleBar)sender;
            sb._controller.PixelsToMicronsScalingFactor = (double)e.NewValue;
        }
        private static void OnUnitChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ScaleBar sb = (ScaleBar)sender;
            sb._controller.Units = (Unit)e.NewValue;
            sb._controller.UpdateValues(null,null);
        }
        // AND WHY THE FUCK DO I NEED TO DEFINE IT AGAAAIAIIINN??? AGGGGH WPF SUCKS
        public SlideViewViewport Viewport
        {
            get { return (SlideViewViewport)GetValue(ViewportProperty); }
            set { SetValue(ViewportProperty, value); }
        }
        public double PixelsToMicronsScalingFactor
        {
            get { return (double)GetValue(PixelsToMicronsScalingFactorProperty); }
            set { SetValue(PixelsToMicronsScalingFactorProperty, value); }
        }
        public Unit Units
        {
            get { return (Unit)GetValue(UnitsProperty); }
            set { SetValue(UnitsProperty, value); }
        }
        #endregion

        // I HATE MVVM
        private class TokenController : Notifier
        {
            private Unit _unit;

            public SlideViewViewport Viewport { get; set; }
            public double PixelsToMicronsScalingFactor { get; set; }

            public double MeasuredLength { get; private set; }
            public double MeasureBarWidth { get; private set; }
            public Unit Units { get { return _unit; } set { _unit = value; Notify("Units"); } }
            public double Power { get; private set; }

            public void UpdateValues(object sender, EventArgs e)
            {
                if (Viewport == null || PixelsToMicronsScalingFactor == 0)
                    return;

                double micronsPerPixel = 1.0 / (Viewport.Scale);
                double oneHundredPixelsInMicrons = 1.0;
                double measureBarWidth = 0;

                for (int i = 0; i < 20; i++)
                {
                    measureBarWidth = oneHundredPixelsInMicrons / micronsPerPixel;
                    if (measureBarWidth > 150 && measureBarWidth < 500) break;

                    oneHundredPixelsInMicrons *= 2.0;
                    measureBarWidth = oneHundredPixelsInMicrons / micronsPerPixel;
                    if (measureBarWidth > 150 && measureBarWidth < 500) break;

                    oneHundredPixelsInMicrons *= 2.5;
                    measureBarWidth = oneHundredPixelsInMicrons / micronsPerPixel;
                    if (measureBarWidth > 150 && measureBarWidth < 500) break;

                    oneHundredPixelsInMicrons *= 2.0;
                }

                MeasureBarWidth = measureBarWidth;
                MeasuredLength = _unit.ConvertFromMicrons(oneHundredPixelsInMicrons);
                //Power = Viewport.MagnificationPower;
                Power = Math.Round(10 * (Viewport.Scale * 1.04) + 0.0001, 2);

                Notify("MeasuredLength", "MeasureBarWidth", "Power");
            }
        }
    }
}
