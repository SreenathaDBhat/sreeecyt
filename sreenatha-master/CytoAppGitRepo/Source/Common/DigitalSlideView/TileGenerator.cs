﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace AI.Imaging
{
    public static class TileGenerator
    {
        private static IEnumerable<Tile> TileRectsAt(SlideImage image, int resolution, Rect visibleArea, bool isTopLevelTile)
        {
            var dims = image.Dimensions.Where(d => d.Resolution == resolution).ToArray();
            var dimension = dims.FirstOrDefault() as Dimension;

            if (dimension != null)
            {
                var rect = Rect.Intersect(image.Position, visibleArea);
                if (rect != Rect.Empty)
                {
                    int topVisibleRow = (int)Math.Floor((visibleArea.Top - image.Position.Top) / dimension.TileSizeMicrons.Height);
                    int bottomVisibleRow = (int)Math.Ceiling((visibleArea.Bottom - image.Position.Top) / dimension.TileSizeMicrons.Height);
                    int leftVisibleRow = (int)Math.Floor((visibleArea.Left - image.Position.Left) / dimension.TileSizeMicrons.Width);
                    int rightVisibleRow = (int)Math.Ceiling(((visibleArea.Right - image.Position.Left) / dimension.TileSizeMicrons.Width));

                    topVisibleRow = Math.Max(0, topVisibleRow);
                    bottomVisibleRow = Math.Min(dimension.NumberOfTileRows, bottomVisibleRow);
                    leftVisibleRow = Math.Max(0, leftVisibleRow);
                    rightVisibleRow = Math.Min(dimension.NumberOfTileColumns, rightVisibleRow);

                    for (int x = leftVisibleRow; x < rightVisibleRow; ++x)
                    {
                        for (int y = topVisibleRow; y < bottomVisibleRow; ++y)
                        {
                            var t = TileAtPosition(image, dimension, x, y, isTopLevelTile);
                            yield return t;
                        }
                    }
                }
            }
        }

        static double dist2(Point a, Point b)
        {
            return (b.X - a.X) * (b.X - a.X) + (b.Y - a.Y) * (b.Y - a.Y);
        }

        static Point centre(Rect r)
        {
            return new Point(r.X + (r.Width / 2), r.Y + (r.Height / 2));
        }

        public static IEnumerable<Tile> VisibleTiles(this SlideImage image, int resolution, Rect visibleArea)
        {
            var a = TileRectsAt(image, resolution, visibleArea, resolution == image.WorstResolution).ToArray();
            var c = centre(visibleArea);
            Array.Sort(a, (x, y) => dist2(c, centre(x.Position)).CompareTo(dist2(c, centre(y.Position))));
            return a;
        }

        private static Tile TileAtPosition(SlideImage img, Dimension d, int c, int r, bool top)
        {
            double x = img.Position.X + (d.TileSizeMicrons.Width * c);
            double y = img.Position.Y + (d.TileSizeMicrons.Height * r);
            double contentWidth = 1;
            double contentHeight = 1;

            if (x + d.TileSizeMicrons.Width > img.Position.Right)
                contentWidth = (img.Position.Right - x) / d.TileSizeMicrons.Width;

            if (y + d.TileSizeMicrons.Height > img.Position.Bottom)
                contentHeight = (img.Position.Bottom - y) / d.TileSizeMicrons.Height;

            var rect = new Rect(x, y, d.TileSizeMicrons.Width * contentWidth, d.TileSizeMicrons.Height * contentHeight);
            var contentSize = new Size(contentWidth, contentHeight);

            return new Tile(rect, d.Resolution, contentSize, d.NumberOfTileColumns * r + c, top);
        }
    }
}
