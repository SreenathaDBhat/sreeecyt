using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;
using AI.Imaging;
using System.IO;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using System.Collections.Concurrent;
using System.Collections;

// *********************************************************
//
//  THIS HAS BEEN DEPRECATED - REPLACED BY "Tiler".
//  We still use this in SGC, for now, so we don't need to
//  write a compatible rendering provider.
//
// *********************************************************

namespace AI
{

    public class TileRequestQueueItem
    {
        public ISlideImageTileRequest TileRequest { get; set; }
        public DisplayImage DisplayImage { get; set; }
    }

    /// <summary>
    /// TileImager draws the tiles that you can see.
    /// Tell it your viewport (in ideal slide coordinates) and it will serve up tiles via the <see cref="Images"/> property. Bind to that.
    /// </summary>
    public class TileImager : Notifier, IDisposable, ISlideImageTileDelegate
    {
        private Dictionary<SlideImage, Dimension[]> representativeDimensionsFromEachResolutionLevel;
        private Dispatcher uiDispatcher;
        private ISlideImageCollection imageCollection;
        private List<DisplayImage> displayImages;
        private Rect lastPlaceViewportMovedTo;

        private ConcurrentQueue<TileRequestQueueItem> _queue;
        private Task [] _loadTileTask;
        private bool _stopLoadTask = false;
        private bool _trace = false;

        // Add event handler for display changes
        public event EventHandler WhatsOnDisplayChanged;

        private const double ThrowOutWidthRatio = 30;
        private const double TargetPixelRatio = 2;

        public TileImager(Dispatcher uiDispatcher)
        {
            this.uiDispatcher = uiDispatcher;
            
            // Create a concurrent queue to take tile load requests
            _queue = new ConcurrentQueue<TileRequestQueueItem>();
            // create a set of tasks to process request queue, increase array bounds to add more tasks
            _loadTileTask = new Task[4];

            representativeDimensionsFromEachResolutionLevel = new Dictionary<SlideImage, Dimension[]>();
        }

        public TileImager(ISlideImageCollection scn, Dispatcher uiDispatcher)
        {
            this.imageCollection = scn;
            this.uiDispatcher = uiDispatcher;
            this.displayImages = (from i in scn.Images
                                  select new DisplayImage(i)).ToList();

            // Create a concurrent queue to take tile load requests
            _queue = new ConcurrentQueue<TileRequestQueueItem>();
            // create a set of tasks to process request queue, increase array bounds to add more tasks
            _loadTileTask = new Task[4];

            representativeDimensionsFromEachResolutionLevel = new Dictionary<SlideImage,Dimension[]>();

            foreach (var i in scn.Images)
            {
                // get distinct resolutions inside each image
                var distinctResolutions = i.Dimensions.GroupBy(dim => dim.Resolution)
                                                        .Select(g => g.First())
                                                        .ToArray();
                representativeDimensionsFromEachResolutionLevel[i] = distinctResolutions;
            }
        }

        public void SetSCN(ISlideImageCollection scn)
        {
            this.imageCollection = scn;
            this.displayImages = (from i in scn.Images
                                    select new DisplayImage(i)).ToList();

            
            foreach (var i in scn.Images)
            {
                var distinctResolutions = i.Dimensions.GroupBy(dim => dim.Resolution)
                                                        .Select(g => g.First())
                                                        .ToArray();

                representativeDimensionsFromEachResolutionLevel[i] = distinctResolutions;
            }
            Notify("Images");
        }

        public void Dispose()
        {
            // WH, bit of a band aid, basically something has a strong reference to the tiler and its not being GC'ed
            // need to memory profile and try and track down whats got the refernce, my guess is a frameowrk thing such as an eventhandler
            // or binding to a non dependency property, usual shit :(
            StopProcessingRequests();

            foreach( var im in displayImages)
            {
                ((ObservableCollection<DisplayTile>)im.VisibleTiles).Clear();
            }
            displayImages.Clear();
        }

        void _mytrace(string line)
        {
            // trace in debug will slow the display down noticibly
            if (_trace)
                Trace.WriteLine(line);
        }

        private void StartProcessRequests()
        {
            _stopLoadTask = false;

            for (int i = 0; i < _loadTileTask.Count(); i++)
            {
                if(_loadTileTask[i] == null || _loadTileTask[i].IsCompleted)
                {
                    _loadTileTask[i] = Task.Factory.StartNew(() => ProcessRequests(i));
                    _mytrace("Start Process Request " + i);
                }
            }
        }

        private void ProcessRequests(object state)
        {
            int processIndex = (int)state;

            TileRequestQueueItem queueItem;
            while (_stopLoadTask == false && _queue.TryDequeue(out queueItem))
            {
                // load in the image data
                queueItem.TileRequest.LoadTileImage();
                // Its held here for some reason, no need I think, refactor
                var displayTile = (DisplayTile)queueItem.TileRequest.Context;

                uiDispatcher.Invoke((ThreadStart)delegate
                {
                    if (!_stopLoadTask && !IsTilePresent(queueItem.DisplayImage, displayTile.SCNTile))
                        queueItem.DisplayImage.AddTile(displayTile);
                }, DispatcherPriority.Normal);

                _mytrace("ProcessRequest - handled by index " + processIndex );
            }
            _mytrace("ProcessRequest - thread exit - index " + processIndex);
        }

        private void StopProcessingRequests()
        {
            _stopLoadTask = true;
        }

        private void ClearRequestQueue()
        {
            StopProcessingRequests();

            // Clear the queue by consuming the requests
            TileRequestQueueItem item;
            while (_queue.TryDequeue(out item))
            {
                // do nothing
            }
            _mytrace("Cleared Request queue");
        }

        public IEnumerable<DisplayImage> Images
        {
            get { return displayImages; }
        }

        private Rect PreviousVisibleArea
        {
            get;
            set;
        }

        /// <summary>
        /// Updates on the fly with a tile representative of what is currently on disply.
        /// Bind to this to enable controls contextually sensitive to what is visible, analysis for example.
        /// </summary>
        public DisplayOverview WhatsOnDisplay
        {
            get;
            private set;
        }

        private void SendNewWhatsOnDisplay(DisplayOverview disp)
        {
            uiDispatcher.Invoke((ThreadStart)delegate
            {
                WhatsOnDisplay = disp;
                if (WhatsOnDisplayChanged != null)
                    WhatsOnDisplayChanged(this, EventArgs.Empty);

            }, DispatcherPriority.Normal);
        }

        public void UpdateVisibleTiles(Rect actualArea, Rect visibleArea, int displayAreaPixelWidth, int z, bool invalidate)
        {
            if (Thread.CurrentThread != uiDispatcher.Thread)
                throw new Exception("THREADS: KillDeadTiles");

            lastPlaceViewportMovedTo = visibleArea;

            PreviousVisibleArea = visibleArea;

            // Just throw old requests away, get requests for new viewport
            ClearRequestQueue();

            foreach (var scanArea in displayImages)
            {
                if (!scanArea.IsVisible)
                    continue;

                //foreach (var x in scanArea.VisibleTiles)
                //    x.DebugBrush = null;

                var image = scanArea.Image;
                var newRes = ChooseSuitableResolution(image, actualArea, displayAreaPixelWidth);

                scanArea.CurrentResolution = newRes;

                Dimension[] dimensions;
                if (scanArea.Image.Illumination == IlluminationType.Brightfield)
                {
                    dimensions = image.Dimensions.Where(d => d.Resolution == scanArea.CurrentResolution).ToArray();
                }
                else
                {
                    var visibleFluors = (from f in scanArea.Fluors where f.IsVisible select f.Name.ToUpper()).ToArray();
                    dimensions = image.Dimensions.Where(d => d.Resolution == scanArea.CurrentResolution && visibleFluors.Contains(d.Fluor.Name.ToUpper())).ToArray();
                }

                if (dimensions.Length == 0)
                {
                    scanArea.ClearAll(); // no dimensions usual means we have no fluors selected for display, so clear the image
                    continue;
                }

                Dimension bestdimension = dimensions.First();
                foreach (var d in dimensions)
                {
                    if (Math.Abs(d.Z - z) < Math.Abs(bestdimension.Z - z))
                    {
                        bestdimension = d;
                    }
                }

                var tilesize = bestdimension.TileSizeMicrons;
                var tilesVisibleInNewArea = scanArea.Image.VisibleTiles(scanArea.CurrentResolution, visibleArea);

                // Only request tiles that are not already visible or need Refresh
//                tilesVisibleInNewArea = tilesVisibleInNewArea.Where(vt => !IsTilePresent(scanArea, vt));

                foreach (var t in tilesVisibleInNewArea)
                {
                    DisplayTile newTile = GetExistingVisibleTile(t, scanArea);
                    if (newTile == null)
                        newTile = new DisplayTile(scanArea, t, Math.Max(tilesize.Width, tilesize.Height));
                    
                    newTile.Z = z;

                    if (image.Illumination == IlluminationType.Fluorescent)
                    {
                        var planes = (from d in dimensions where d.Z != z select d.Z).Distinct().ToArray();

                        TileLoadInfo[] dims = null;

                        // Different channels can have different focal planes, try to use the closest one
                        int zVal = z;
                        while (zVal >= 0)
                        {
                            dims = (from f in dimensions where f.Z == zVal select TileLoadInfo.LoadRequest(f)).ToArray();
                            if (dims.Length > 0)
                                break;
                            zVal--;
                        }

                        // add a tile request to the queue, the processing thread will add it to the image for display
                        var queueItem = new TileRequestQueueItem
                                                {
                                                    DisplayImage = scanArea,
                                                    TileRequest = imageCollection.ProvideTileImageAsync(t, dims.ToArray(), this, newTile, scanArea.Image.Illumination)
                                                };
                        _queue.Enqueue(queueItem);

                        _mytrace("Added Request " + t.Identifier);

                        if (scanArea.CurrentResolution == 0)
                        {
                            foreach (var zplane in planes)
                            {
                                var otherStackDims = dimensions.Where(d => d.Z == zplane).ToArray();
                            }
                        }
                    }
                    else
                    {
                        var primary = dimensions.ClosestToZ(z);

                        // add a tile request to the queue, the processing thread will add it to the image for display
                        var queueItem = new TileRequestQueueItem
                        {
                            DisplayImage = scanArea,
                            TileRequest = imageCollection.ProvideTileImageAsync(t, new TileLoadInfo[] { TileLoadInfo.LoadRequest(primary) }, this, newTile, scanArea.Image.Illumination)
                        };

                        _queue.Enqueue(queueItem);

                        _mytrace("Added Request " + t.Identifier);
                    }
                    // clear away any higher res tiles that aren't required at our zoom level
                    ClearHigherResTiles(scanArea, newRes);
                }

                // Kill anything outside the approx. visible area
                KillDeadTiles(visibleArea);

                // Start the thread to pull in the required tiles for new viewport
                StartProcessRequests();

                // Update everybody on changed position
                UpdateWhatsOnDisplay();
            }
        }

        private bool IsTilePresent(Tile vt, IEnumerable<DisplayTile> s)
        {
            var potentialTilePosition = vt.Position;

            foreach (var t in s)
            {
                if (t.SCNTile.Resolution == vt.Resolution && t.SCNTile.Identifier == vt.Identifier)
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsTilePresent(DisplayImage scanArea, Tile vt)
        {
            if (IsTilePresent(vt, scanArea.VisibleTiles))
                return true;

            return false;
        }

        private DisplayTile GetExistingVisibleTile(Tile vt, DisplayImage scanArea)
        {
            var displayImage = (from t in scanArea.VisibleTiles
                                where t.SCNTile.Resolution == vt.Resolution && t.SCNTile.Identifier == vt.Identifier
                                select t).FirstOrDefault();
            return displayImage;
        }

        public void SlideImageTileDidFailToLoad(ISlideImageTileRequest req, TileLoadInfo failedDimension)
        {
            Debug.Fail("bad tile?");
        }

        public void SlideImageTileDidLoad(ISlideImageTileRequest req, IEnumerable<TileLoadInfo> loadedDimensions)
        {
            // BRIGHTFIELD
            if (req.Illumination == IlluminationType.Brightfield)
            {
                var dim = loadedDimensions.First();
                var t = (DisplayTile)req.Context;

                var img = UnpackImage(dim.ImageData);
                
                uiDispatcher.Invoke((ThreadStart)delegate
                {
                    t.AbsorbImage(img, dim.Dimension.Z);
                }, DispatcherPriority.Background);
            }
            // FLUORESCENT
            else
            {
                // try to avoid too many buffer allocations in the loop
                byte[] composedImage = new byte[512 * 512 * 4];
                byte[] channelBytes = new byte[512 * 512 * 4];
                var displayTile = (DisplayTile)req.Context;
                int z = -1;

                foreach (var loadedDimension in loadedDimensions)
                {
                    z = loadedDimension.Dimension.Z;
                    var channel = UnpackImage(loadedDimension.ImageData);
                    var channelFluor = loadedDimension.Dimension.Fluor;

                    if (channel != null && channel.Format == PixelFormats.Gray8)
                    {                        
                        channel.CopyPixels(channelBytes, channel.PixelWidth, 0);
                        TileRenderer.MixItIn(composedImage, 512, 512, channelBytes, 512, 512, channelFluor);
                    }
                    else
                    {
                        channel.CopyPixels(channelBytes, channel.PixelWidth * 4, 0);

                        var greyBytes = new byte[channel.PixelHeight * channel.PixelWidth];
                        int j = 0;
                        for (int i = 2; i < channelBytes.Length; i += 4)
                            greyBytes[j++] = channelBytes[i];

                        TileRenderer.MixItIn(composedImage, 512, 512, greyBytes, 512, 512, channelFluor);
                    }
                }

                var outImage = BitmapSource.Create(512, 512, 96, 96, PixelFormats.Bgra32, null, composedImage, 512 * 4);
                outImage.Freeze();
                uiDispatcher.Invoke((ThreadStart)delegate
                {
                    displayTile.AbsorbImage(outImage, z);
//                    displayTile.DebugBrush = Brushes.Blue;
                }, DispatcherPriority.Background);
            }
        }

        private BitmapSource UnpackImage(byte[] p)
        {
            try
            {
                var img = new BitmapImage();
                img.BeginInit();
                img.StreamSource = new MemoryStream(p);
                img.EndInit();
                img.Freeze();
                return img;
            }
            catch(Exception )
            {
                
            }

            byte[] dummy = new byte[512 * 512 * 4];
            var realimg = BitmapImage.Create(512, 512, 96, 96, PixelFormats.Bgra32, null, dummy, 512 * 4);
            realimg.Freeze();
            return realimg;
        }

        private IEnumerable<DisplayTile> AllVisibleTiles()
        {
            foreach (var i in displayImages)
                foreach (var t in i.VisibleTiles)
                    yield return t;
        }

        private int ChooseSuitableResolution(SlideImage image, Rect viewportMicrons, int viewportWidthPixels)
        {
            Dimension level = null;

            var resolutions = representativeDimensionsFromEachResolutionLevel[image];

            // what microns per screen pixel is the viewport at
            var mppScreen = viewportMicrons.Width / viewportWidthPixels;

            // Resolution worst to best
            foreach (var r in resolutions.Reverse())
            {
                level = r;

                var mppTile = r.TileSizeMicrons.Width / 512.0;

                // once the tiles match the viewport resolution or better, we're at the optimum resolution for that viewport
                if (mppTile <= mppScreen)
                    break;
            }
            return level.Resolution;
        }

        /// <summary>
        /// Remove tiles from the scanarea if they are greater than the given resolution
        /// </summary>
        /// <param name="image"></param>
        /// <param name="newResolution"></param>
        private void ClearHigherResTiles(DisplayImage image, int newResolutionIndex)
        {       
            var oc = (ObservableCollection<DisplayTile>)image.VisibleTiles;
            int n = oc.Count - 1;
                    
            for (int i = n; i >= 0; --i)
            {
                var t = oc[i];
                if (t.SCNTile.IsTopLevelTile)
                    continue;

                // Highest res layer is index 0
                if (t.SCNTile.Resolution < newResolutionIndex)
                    oc.RemoveAt(i);
            }
        }

        private void KillDeadTiles(Rect visibleArea)
        {
            if (Thread.CurrentThread != uiDispatcher.Thread)
                throw new Exception("THREADS: KillDeadTiles");

            foreach (var im in displayImages)
            {
                var oc = (ObservableCollection<DisplayTile>)im.VisibleTiles;
                int n = oc.Count - 1;
                    
                // dont be so strict throwing stuff out
                var expandedVisibleArea = Rect.Inflate(visibleArea, visibleArea.Width * 0.5, visibleArea.Height * 0.5);

                for (int i = n; i >= 0; --i)
                {
                    var t = oc[i];
                    if (t.SCNTile.IsTopLevelTile)
                        continue;

                    var widthRatio = visibleArea.Width / t.TileSize;

                    if (widthRatio > ThrowOutWidthRatio)
                    {
                        oc.RemoveAt(i);
                    }
                    else if (Rect.Intersect(expandedVisibleArea, t.Position) == Rect.Empty)
                    {
                        oc.RemoveAt(i);
                    }
                }
            }
        }

        private int lastTileCount = 0;
        private Tile lastRepresentitiveTile;
        private void UpdateWhatsOnDisplay()
        {
            var visibleArea = lastPlaceViewportMovedTo;
            Tile reprasentativeTile = null;
            var centreOfVisibleArea = CenterOf(visibleArea);
            double closestToCentre = double.MaxValue;
            
            var representativeImageInfo = (from i in displayImages
                                           where i.IsVisible && Rect.Intersect(visibleArea, i.Image.Position) != Rect.Empty
                                           select i).LastOrDefault();
            
            // happens when all images are turned off by user
            if (representativeImageInfo == null)
            {
                // Call function to fire display changes event
                SendNewWhatsOnDisplay(DisplayOverview.Nothing);
                return;
            }

            var representativeImage = representativeImageInfo.Image;

            var tiles = representativeImage.VisibleTiles(representativeImageInfo.CurrentResolution, visibleArea);

            foreach(var t in tiles)
            {
                if (reprasentativeTile == null)
                    reprasentativeTile = t;

                var centreOfTile = CenterOf(t.Position);
                var distToCentre = Math.Pow(centreOfTile.X - centreOfVisibleArea.X, 2) + Math.Pow(centreOfTile.Y - centreOfVisibleArea.Y, 2);
                if (distToCentre < closestToCentre)
                {
                    reprasentativeTile = t;
                    closestToCentre = distToCentre;
                }
            }

            // Call function to fire display changes event
            var sameTile = (lastRepresentitiveTile != null) ? reprasentativeTile.Identifier == lastRepresentitiveTile.Identifier : false;
            if ((reprasentativeTile != null && !sameTile) || lastTileCount != tiles.Count())
            {
                SendNewWhatsOnDisplay(new DisplayOverview(representativeImage, reprasentativeTile, visibleArea, 0));
            }

            lastRepresentitiveTile = reprasentativeTile;
            lastTileCount = tiles.Count();
        }

        public void InvalidateDisplay()
        {
            lastRepresentitiveTile = null;
        }

        public static Point CenterOf(Rect rect)
        {
            return new Point(rect.X + (rect.Width / 2), rect.Y + (rect.Height / 2));
        }

        internal void SetZ(int z)
        {
            foreach (var s in displayImages)
            {
                foreach(var t in s.VisibleTiles)
                {
                    t.Z = z;
                }
            }
        }

        // Update fluorescence channels for the current displayed images
        internal void SetFluors(Fluor [] fluors)
        {
            // loop displayed images
            foreach (var s in displayImages)
            {
                // loop on fluorescence channels in the slide settings
                foreach (var f1 in s.Fluors)
                {
                    // loop on fluorescence channels in the UI settings
                    bool visible = false;
                    foreach (Fluor f2 in fluors)
                    {               
                        // search by name
                        if (f1.Name.CompareTo(f2.Name) == 0)
                        {
                            // found
                            visible = true;
                            // modify channel color if needed
                            f1.RenderColor = f2.RenderColor;
                            break;
                        }
                    }
                    // modify channel visibility
                    f1.IsVisible = visible;
                }
            }
        }

        internal void ReloadAll()
        {
                foreach (var i in displayImages)
                    i.ClearAll();
        }
    }

    public static class EXTME
    {
        public static Dimension ClosestToZ(this IEnumerable<Dimension> dims, int z)
        {
            int diff = int.MaxValue;
            Dimension theone = null;

            foreach (var d in dims)
            {
                int delta = Math.Abs(d.Z - z);
                if (delta < diff)
                {
                    diff = delta;
                    theone = d;
                }
            }

            return theone;
        }
    }
}
