﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;
using AI.Imaging;

namespace AI
{
    public sealed class TileSnapshot
    {
        public TileSnapshot(ISlideImageCollection imageCollection, SlideImage image, Tile tile, int z, Fluor[] fluors, IlluminationType illum)
        {
            this.ImageCollection = imageCollection;
            this.Image = image;
            this.Tile = tile;
            this.Z = z;
            this.Fluors = fluors;
            this.Illumination = illum;
        }

        public ISlideImageCollection ImageCollection { get; private set; }
        public SlideImage Image { get; private set; }
        public Tile Tile { get; private set; }
        public int Z { get; private set; }
        public Fluor[] Fluors { get; private set; }

        public IlluminationType Illumination { get; private set; }
    }


    public static class TileRenderer
    {
        public static ImageSource CacheOverviewImage(ISlideImageCollection slideImage)
        {
            double desiredWidth = 100;
            if (slideImage.Bounds.Height > slideImage.Bounds.Width)
            {
                desiredWidth = 100 * (slideImage.Bounds.Height / (double)slideImage.Bounds.Width);
            }

            var image = slideImage.BuildOverviewImage(desiredWidth);
            image.Freeze();

            return image;
        }

        public static BitmapSource Render(ISlideImageCollection imageCollection, SlideImage image, Tile tile, int z)
        {
            return Render(new TileSnapshot(imageCollection, image, tile, z, image.Fluors.ToArray(), image.Illumination));
        }

        public static BitmapSource Render(TileSnapshot tileSnapshot)
        {
            var bfDimension = tileSnapshot.Image.Dimensions.Where(d => d.Resolution == tileSnapshot.Tile.Resolution).First();

            int res = tileSnapshot.Tile.Resolution;
            
            // Brightfield
            if (tileSnapshot.Illumination == IlluminationType.Brightfield)
            {
                var bfDim = tileSnapshot.Image.Dimensions.Where(d => d.Resolution == tileSnapshot.Tile.Resolution);
                var dimensionWithBestZ = ClosestZ(bfDim, tileSnapshot.Z).First();

                var bmpBytes = tileSnapshot.ImageCollection.ProvideTileImage(dimensionWithBestZ.Identifier, tileSnapshot.Tile.Identifier);
                if (bmpBytes == null)
                    return null;

                try
                {
                    BitmapImage bmp = new BitmapImage();
                    bmp.BeginInit();
                    bmp.CacheOption = BitmapCacheOption.OnLoad;
                    bmp.StreamSource = new MemoryStream(bmpBytes);
                    bmp.EndInit();
                    bmp.Freeze();
                    return bmp;
                }
                catch (Exception)
                {
                    return null;
                }
            }

            // Fluorescent
            else
            {
                return RenderFluorescent(tileSnapshot);
            }
        }

        
        private static BitmapSource RenderFluorescent(TileSnapshot tileSnapshot)
        {
            byte[] composedImage = new byte[512 * 512 * 4];

            var potentialDimensions = tileSnapshot.Image.Dimensions.Where(d => d.Resolution == tileSnapshot.Tile.Resolution);
            var dimensionsWithBestZ = ClosestZ(potentialDimensions, tileSnapshot.Z);

            foreach(var dim in dimensionsWithBestZ)
            {
                var bmpBytes = tileSnapshot.ImageCollection.ProvideTileImage(dim.Identifier, tileSnapshot.Tile.Identifier);
                if (bmpBytes == null)
                    continue;

                BitmapImage bmp = null;
                using (var stream = new MemoryStream(bmpBytes))
                {
                    bmp = new BitmapImage();
                    bmp.BeginInit();
                    bmp.CacheOption = BitmapCacheOption.OnLoad;
                    bmp.StreamSource = stream;
                    bmp.EndInit();
                    bmp.Freeze();
                }

                if (bmp != null && bmp.Format == PixelFormats.Gray8)
                {
                    var channelBytes = new byte[bmp.PixelHeight * bmp.PixelWidth];
                    bmp.CopyPixels(channelBytes, bmp.PixelWidth, 0);

                    MixItIn(composedImage, 512, 512, channelBytes, 512, 512, dim.Fluor);
                }
            }

            var outImage = BitmapSource.Create(512, 512, 96, 96, PixelFormats.Bgra32, null, composedImage, 512 * 4);
            outImage.Freeze();
            return outImage;
        }

        private static IEnumerable<Dimension> ClosestZ(IEnumerable<Dimension> potentialDimensions, int z)
        {
            var dim = potentialDimensions.Where(d => d.Z == z).FirstOrDefault();
            if (dim == null)
                dim = potentialDimensions.FirstOrDefault();

            return potentialDimensions.Where(d => d.Z == dim.Z);
        }

        public static unsafe void MixItIn(byte[] outPixels, int ow, int oh, byte[] bmpPixels, int iw, int ih, Fluor f)
        {
            Color renderColor = f.RenderColor;

            fixed (byte* op = &outPixels[0])
            {
                fixed (byte* ip = &bmpPixels[0])
                {
                    byte* i = ip;
                    byte* o = op;
                    int inPitch = iw;
                    int outPitch = ow * 4;

                    Parallel.For(0, oh, y =>
                    // parallel benchmarks at about 2.5x speed, but visually hard to spot difference
                    {
                        byte* inRowPtr = i + (y * inPitch);
                        byte* outRowPtr = o + (y * outPitch);
                        for (int x = 0; x < ow; ++x)
                        {
                            byte v = *inRowPtr++;

                            byte r = (byte)((v * renderColor.R) >> 8);
                            byte g = (byte)((v * renderColor.G) >> 8);
                            byte b = (byte)((v * renderColor.B) >> 8);

                            *(outRowPtr) = (byte)Math.Min(255, (*outRowPtr + b));
                            outRowPtr++;
                            *(outRowPtr) = (byte)Math.Min(255, (*outRowPtr + g));
                            outRowPtr++;
                            *outRowPtr = (byte)Math.Min(255, (*outRowPtr + r));
                            outRowPtr++;
                            *outRowPtr = 255;
                            outRowPtr++;
                        }
                    });
                }
            }
        }
    }
}
