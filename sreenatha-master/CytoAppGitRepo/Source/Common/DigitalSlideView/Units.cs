﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace AI
{
    public sealed class Unit
    {
        private string name, notation;
        private double multiplierFromMicrons;

        private Unit(string name, string notation, double multiplierFromMicrons)
        {
            this.name = name;
            this.notation = notation;
            this.multiplierFromMicrons = multiplierFromMicrons;
        }


        /// <summary>
        /// Formal casing and pluralised for display: Meters, Microns, etc
        /// </summary>
        public string Name { get { return name; } }

        /// <summary>
        /// mm, cm, m, etc
        /// </summary>
        public string Notation { get { return notation; } }
        
        public IEnumerable<Point> ConvertFromMicrons(IEnumerable<Point> points)
        {
            return from p in points select new Point(p.X * multiplierFromMicrons, p.Y * multiplierFromMicrons);
        }

        public double ConvertFromMicrons(double value)
        {
            return value * multiplierFromMicrons;
        }

        public double ConvertToMicrons(double value)
        {
            return value / multiplierFromMicrons;
        }

        #region Presets
        
        private static Unit[] units = 
        {
            new Unit("Microns", "\u00b5m", 1.0),
            new Unit("Millimeters", "mm", 1e-3),
            new Unit("Centimeters", "cm", 1e-4),
        };

        public static Unit Default = units[0];

        public static IEnumerable<Unit> AvailableUnits
        {
            get
            {
                return units;
            }
        }
        #endregion
    }
}
