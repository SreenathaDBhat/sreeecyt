﻿using System;
using System.Windows;
using System.ComponentModel;
using System.Windows.Media;
using System.Windows.Forms;

namespace AI
{
    public sealed class SlideViewViewport : INotifyPropertyChanged, IDisposable
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private Point _lookAt;
        private double _mag;
        private DateTime? lastMove;
        private double _lagX, _lagY, _lagMag;
        private double scaleX, scaleY;

        // Fix Mantis #8934 - SCN images are displaying Thumbnail vertical
        // Fix Mantis #8826: Viewer inconsistencies between SlidePath Gateway and DIH viewer
        // 3. Images are displayed horizontally in DIH and vertically in gateway.
        // Added 90 degrees to default initial orientation (horizontal)
        private double _rotation = 90; 
        private double _baseMagnificationPower;
        private Rect _bounds;

        // HACK alert: in winXP we don't have the luxury of deferred rendering and the OS makes it pretty hard to render anything
        // in a window (ie not FullScreen) at a monitor vsync interval.
        // The spring animation this class performs makes the tearing artifact this all produces stick out like a sore thumb.
        // So, what do we do? Hack it, thats what.
        // In XP, we make the spring effect so strong it may aswel not be there.
        // Ahhhhh yeeaaaaaaa.
        private bool isXP;

        public SlideViewViewport(Rect bounds)
        {
            _baseMagnificationPower = 20;// baseMagnificationPower;
            _bounds = bounds;
            _mag = 1;
            _lagMag = _mag;
            lastMove = DateTime.MinValue;
            scaleX = scaleY = 1;
            UseSprings = true;

            _lookAt = new Point(0, 0);
            _lagX = _lookAt.X;
            _lagY = _lookAt.Y;
            CompositionTarget.Rendering += Move;

            isXP = IsCurrentOSXPOrLess();
            MagicNumber = 0.52;
        }

        public int BaseMagnification
        {
            get { return (int)Math.Round(_baseMagnificationPower); }
        }

        public static bool IsCurrentOSXPOrLess()
        {
            return Environment.OSVersion.Version.Major <= 5 || SystemInformation.TerminalServerSession;
        }

        public void Dispose()
        {
            CompositionTarget.Rendering -= Move;
        }

        void Notify(params string[] p)
        {
            foreach (string s in p)
                Notify(s);
        }

        void Notify(string p)
        {
            if(PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(p));
        }

        public void SetBaseMagnification(double p)
        {
            _baseMagnificationPower = p;
        }

        public bool UseSprings 
        { 
            get; set; 
        }

        public double RotationDegrees 
        { 
            get { return _rotation; }
            set { _rotation = value; Notify("RotationDegrees"); FireAnimatedMove(); FireMove(); } 
        }

        public double FlipScaleX 
        { 
            get { return scaleX; }
            set { scaleX = value; Notify("FlipScaleX"); FireAnimatedMove(); FireMove(); } 
        }

        public double FlipScaleY 
        { 
            get { return scaleY; }
            set { scaleY = value; Notify("FlipScaleY"); FireAnimatedMove(); FireMove(); } 
        }

        public delegate void ViewportDidMoveCallback(SlideViewViewport viewport);
        
        // Called at each aimation frame
        public ViewportDidMoveCallback ViewportDidMoveAnimated;
        // Called once per logical move
        public ViewportDidMoveCallback ViewportDidMove;

        private void Move(object sender, EventArgs e)
        {
            var springStrength = UseSprings ? 0.2 : 0.6;
            if (isXP)
                springStrength = 1;

            bool xMoved = LagMove(ref _lagX, _lookAt.X, 0.1, springStrength);
            bool yMoved = LagMove(ref _lagY, _lookAt.Y, 0.1, springStrength);
            bool magMoved = LagMove(ref _lagMag, _mag, 0.0005, springStrength);

            if (xMoved || yMoved)
            {
                Notify("LaggedLookAt");
                FireAnimatedMove();
            }
            if (magMoved)
            {
                Notify("LaggedScale");
                FireAnimatedMove();
            }
        }

        private bool LagMove(ref double lagX, double target, double significantMoveAmount, double lagFactor)
        {
            double delta = target - lagX;
            lagX += delta * lagFactor;
            return Math.Abs(delta) > significantMoveAmount;
        }

        public Point LaggedLookAt
        {
            get { return new Point(_lagX, _lagY); }
        }

        public double LaggedScale
        {
            get { return Math.Max(0.001, Math.Min(1500, _lagMag)); }
        }

        public Point LookAt
        {
            get { return _lookAt; }
            set 
            {
                var old = _lookAt;
                
                // Sometimes, we know where to look but don't know how big the bounds are yet...
                // Allow it, but don't try to clamp to a 0x0 box!
                // For example, MDT annotations: we know the XY of the annotation but don't know the
                // size of the image it sits on yet.
                _lookAt = value;

                if (Bounds.Width > 0 && Bounds.Height > 0)
                    _lookAt = Clamp(_lookAt);
                
                lastMove = DateTime.Now;
                Notify("LookAt");

                FireMove();
            }
        }

        public double Scale
        {
            get { return _mag; }
            set 
            {
                var old = _mag;
                
                // Don't know the bounds yet? trust the value is good and dont
                // bother range testing..
                if (Bounds.Width > 0 && Bounds.Height > 0)
                {
                    // TODO: work these out properly!
                    var size = Math.Max(_bounds.Width, _bounds.Height);
                    var min = 500.0 / size;
                    var max = 40.0;
                    _mag = Math.Max(min, Math.Min(max, value));
                }
                else
                {
                    _mag = value;
                }
                lastMove = DateTime.Now;
                Notify("Scale", "MagnificationPower");

                FireMove();
            }
        }

        // Note to poor-ass maintainers: 0.52 is default
        // 0.52 is a magic number to make the images on screen appear the same size as
        // they would in the DIH web app. Got .52 by trial and error. Mostly error.
        public double MagicNumber
        {
            get;
            set;
        }


        // TODO: To make this work properly, scale should be based on the actual microns per pixel for an actual pass.
        //       E.g. if MagnificationPower is set to 20, get the microns per pixel for the actual 20x images in the scan
        //       and set the scale so one 20x image pixel is one screen pixel. If there are no 20x images in the scan,
        //       will have to get the scale for images of a different magnificaton and estimate the 20x scales from that.
        //       Remember that the display coordinates are the same as slide coordinates, i.e. microns.
        //       Then we can do away with the klugdy _baseMagnificationPower and MagicNumber!
        public int MagnificationPower
        {
            // Fix Mantis #9214: Image analysis doesn't get enable at 10x for certain images
            // Added 0.5 value to void rounding problems
            get
            {
                return (int)((_mag * _baseMagnificationPower * MagicNumber + 0.5));
            }
            set
            {
                Scale = ScaleForMagPower(value);
            }
        }

        private double ScaleForMagPower(int value)
        {
            return (value / _baseMagnificationPower) / MagicNumber;
        }

		/// <summary>
		/// Returns the base magnification of the slide.
		/// </summary>
	    public int BaseMagnificationPower
	    {
			get { return Convert.ToInt32(_baseMagnificationPower);}
	    }

	    public Rect Bounds
        {
            get { return _bounds; }
            set 
            { 
                _bounds = value; 
                Notify("Bounds");

                if (!_bounds.Contains(_lookAt))
                {
                    _lookAt = Clamp(_lookAt);
                }

                FireMove();
            }
        }

        private Point Clamp(Point lookAt)
        {
            var x = Math.Min(_bounds.Right, Math.Max(_bounds.Left, lookAt.X));
            var y = Math.Min(_bounds.Bottom, Math.Max(_bounds.Top, lookAt.Y));
            return new Point(x, y);
        }

        public void SetScale(double immediateNewMagnification)
        {
            // Fix Mantis #9307: LIMS. Mimimize slide using a launchid which contains a Launchid crashes SGC
            // Avoid to set magnification when a not consistent value is passed
            // It happens when SGC starts minimized, because the "not visible" viewport component returns width==height==0
            if (immediateNewMagnification <= 0.000001)
                return;

            var old = _mag;
            _mag = immediateNewMagnification;
            lastMove = DateTime.Now;
            _lagMag = _mag;
            Notify("Scale", "LaggedScale", "MagnificationPower", "LookAt", "LaggedLookAt");
            FireMove();
        }

        public void SetLookAt(Point point)
        {
            var old = _lookAt;

            _lookAt = point;
            _lagX = point.X;
            _lagY = point.Y;
            Notify("LookAt", "LaggedLookAt");
            FireMove();
        }

        public void SetLookAtAndMagnificationPower(Point lookAt, int mag)
        {
            _lookAt = lookAt;
            _mag = ScaleForMagPower(mag);
            Notify("LookAt", "Scale", "MagnificationPower");
            FireMove();
        }


        internal Matrix TransformMatrix
        {
            get
            {
                Matrix m = new Matrix();
                m.RotateAt(RotationDegrees, _lookAt.X, _lookAt.Y);
                m.ScaleAt(FlipScaleX*Scale, FlipScaleY*Scale, _bounds.Width / 2, _bounds.Height / 2);
                return m;
            }
        }

        public void MoveMagnificationToNextNotch()
        {
            var m = NextMagnificationNotch();

            if (m > 0)
                MagnificationPower = m;
        }

        public int NextMagnificationNotch()
        {
            var m = MagnificationPower;

            if (m < 4)
                m = 4;
            else if (m < 10)
                m = 10;
            else if (m < 20)
                m = 20;
            else if (m < 40)
                m = 40;
            else
                m = -1;

            return m;
        }

        private void FireAnimatedMove()
        {
            if (ViewportDidMoveAnimated != null)
                ViewportDidMoveAnimated(this);
        }
        public void FireMove()
        {
            if (ViewportDidMove != null)
                ViewportDidMove(this);

            FireAnimatedMove();
        }
    }
}
