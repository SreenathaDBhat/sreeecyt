﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CytoApps.DataServer.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class ImageIdsParameter
    {
        [XmlArray("Images")]
        [XmlArrayItem(typeof(ImageIdParameter), ElementName = "Image")]
        public ImageIdParameter[] ImageIds;

        public int[] GetImageIdsAsArray()
        {
            //try
            //{
            var selectIds = ImageIds.Select((x => x.Id));

            return selectIds.ToArray();
            //}
            //catch (Exception ex)
            //{
            return new int[0];
            //}

        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ImageIdParameter
    {
        [XmlElement("Id")]
        public int Id;
    }

    /// <summary>
    /// 
    /// </summary>
    public class SiteServersImagesMappingParameter
    {
        [XmlArray("SiteServerImagesMapping")]
        [XmlArrayItem(typeof(SiteServerImagesMappingParameter), ElementName = "SiteServerImagesMapping")]
        public SiteServerImagesMappingParameter[] SiteServersImagesMapping;
    }

    /// <summary>
    /// 
    /// </summary>
    public class SiteServerImagesMappingParameter
    {
        [XmlElement("SiteId")]
        public int SiteId;

        [XmlElement("ServerId")]
        public int ServerId;

        [XmlElement("EndPointBaseId")]
        public int EndPointBaseId;

        [XmlArray("ImageIds")]
        [XmlArrayItem(typeof(int?[]), ElementName = "ImageIds")]
        public int?[] ImageIds;
    }
    public class ImageDataParameter
    {
        [XmlElement("ImageId")]
        public string ImageId;

        [XmlElement("ImageData")]
        public string ImageData;
    }

    public class CaseParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;
    }

    public class SlideParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;
    }

    public class CellParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("CellId")]
        public string CellId;
    }

    public class CellImageDataParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("CellId")]
        public string CellId;

        [XmlElement("ImageId")]
        public string ImageId;

        [XmlElement("ImageData")]
        public string ImageData;
    }

    public class CaseDetailsParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        /// <summary>
        /// serialized as XML and encoded Base64
        /// </summary>
        [XmlElement("Details")]
        public string Details;
    }


    public class CaseAdvancedParameter
    {
        /// <summary>
        /// serialized as XML and encoded Base64
        /// </summary>
        [XmlElement("Advanced")]
        public string Advanced;
    }

    public class UpdateCaseFlagParameter
    {
        /// <summary>
        /// serialized as XML and encoded Base64
        /// </summary>
        [XmlElement("CaseIds")]
        public string CaseIds;


        [XmlElement("CaseFlag")]
        public string CaseFlag;
    }

    public class CaseLockParameter
    {
        [XmlElement("Case")]
        public string Case;

        [XmlElement("UserDetails")]
        public string UserDetails;
    }

    public class CurrentCaseUsersParameter
    {
        [XmlElement("Case")]
        public string Case;
    }
}
