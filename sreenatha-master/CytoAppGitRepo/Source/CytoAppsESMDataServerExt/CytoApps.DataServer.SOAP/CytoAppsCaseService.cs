﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CytoApps.DataServer.Common;
using CytoApps.Models;

namespace CytoApps.DataServer.SOAP
{
    internal class CytoAppsCaseService
    {
        internal static string GetCaseDetails(CytoAppsConfiguration cfg, CaseParameter caseParm)
        {
            StringBuilder sb = new StringBuilder();
            var caseDAL = new CaseDataAccessLocal.CaseDataAccessLocal(cfg.DatabaseServer, cfg.CasebaseFolder);

            var details = caseDAL.GetCaseDetails(new Models.Case(caseParm.CaseId, caseParm.CaseId));

            // Serialize the scores class to a xml string of retquired format (no xml declaration)
            var xmlString = ObjectSerializer.Serialize<List<Models.CaseDetail>>(details.ToList());

            return xmlString;
        }


        internal static string GetSlides(CytoAppsConfiguration cfg, CaseParameter caseParm)
        {
            StringBuilder sb = new StringBuilder();
            var caseDAL = new CaseDataAccessLocal.CaseDataAccessLocal(cfg.DatabaseServer, cfg.CasebaseFolder);

            var slides = caseDAL.GetSlides(new Models.Case(caseParm.CaseId, caseParm.CaseId));

            // Serialize the slides list to a xml string of required format (no xml declaration)
            var xmlString = ObjectSerializer.Serialize<List<Models.Slide>>(slides.ToList());

            return xmlString;
        }

        internal static string SaveCaseDetails(CytoAppsConfiguration cfg, CaseDetailsParameter caseDetailsParam)
        {
            var caseDAL = new CaseDataAccessLocal.CaseDataAccessLocal(cfg.DatabaseServer, cfg.CasebaseFolder);

            // casedetails serialized as XML and encoded, bit hacky but clean
            var detailsXml = Encoding.Unicode.GetString(Convert.FromBase64String(caseDetailsParam.Details));
            var details = ObjectSerializer.Deserialize<List<CaseDetail>>(detailsXml);

            var ret = caseDAL.SaveCaseDetails(caseDetailsParam.CaseId, details);

            StringBuilder sb = new StringBuilder();
            sb.Append("<Success>");
            sb.Append(ret.ToString());
            sb.AppendLine("</Success>");

            return sb.ToString();
        }

        internal static string GetCaseDetailsFields(CytoAppsConfiguration cfg)
        {
            var caseDAL = new CaseDataAccessLocal.CaseDataAccessLocal(cfg.DatabaseServer, cfg.CasebaseFolder);

            var fields = caseDAL.GetCaseDetailsFields().ToList();
            var xmlString = ObjectSerializer.Serialize<List<CytoApps.Models.Filter>>(fields);

            return xmlString;
        }

        internal static string GetUniqueCaseFlags(CytoAppsConfiguration cfg)
        {
            var caseDAL = new CaseDataAccessLocal.CaseDataAccessLocal(cfg.DatabaseServer, cfg.CasebaseFolder);

            var flags = caseDAL.GetUniqueCaseFlags().ToList();
            var xmlString = ObjectSerializer.Serialize<List<string>>(flags);

            return xmlString;
        }

        internal static string GetAllCaseFlags(CytoAppsConfiguration cfg)
        {
            var caseDAL = new CaseDataAccessLocal.CaseDataAccessLocal(cfg.DatabaseServer, cfg.CasebaseFolder);

            var flags = caseDAL.GetAllCaseFlags().ToList();
            var xmlString = ObjectSerializer.Serialize<List<string>>(flags);

            return xmlString;
        }

        internal static string UpdateCaseFlag(CytoAppsConfiguration cfg, UpdateCaseFlagParameter updateCaseFlagparam)
        {
            var caseDAL = new CaseDataAccessLocal.CaseDataAccessLocal(cfg.DatabaseServer, cfg.CasebaseFolder);

            // CaseIds serialized as XML and encoded, bit hacky but clean
            var caseIdsXml = Encoding.Unicode.GetString(Convert.FromBase64String(updateCaseFlagparam.CaseIds));
            var caseIds = ObjectSerializer.Deserialize<List<string>>(caseIdsXml);

            var ret = caseDAL.UpdateCaseFlag(caseIds, updateCaseFlagparam.CaseFlag);
            StringBuilder sb = new StringBuilder();
            sb.Append("<Success>");
            sb.Append(ret.ToString());
            sb.AppendLine("</Success>");

            return sb.ToString();
        }

        internal static string GetCasesAdvanced(CytoAppsConfiguration cfg, CaseAdvancedParameter advancedParam)
        {
            var caseDAL = new CaseDataAccessLocal.CaseDataAccessLocal(cfg.DatabaseServer, cfg.CasebaseFolder);

            // casedetails serialized as XML and encoded, bit hacky but clean
            var advancedXml = Encoding.Unicode.GetString(Convert.FromBase64String(advancedParam.Advanced));
            var advanced = ObjectSerializer.Deserialize<CytoApps.Models.Criteria>(advancedXml);

            var cases = caseDAL.GetCases(advanced).ToList();

            // Serialize the scores class to a xml string of retquired format (no xml declaration)
            var xmlString = ObjectSerializer.Serialize<List<Case>>(cases);

            return xmlString;
        }

        internal static string CurrentCaseUsers(CytoAppsConfiguration cfg, CurrentCaseUsersParameter caseParam)
        {
            var caseDAL = new CaseDataAccessLocal.CaseDataAccessLocal(cfg.DatabaseServer, cfg.CasebaseFolder);
            var caseXml = Encoding.Unicode.GetString(Convert.FromBase64String(caseParam.Case));
            var caseObj = ObjectSerializer.Deserialize<Case>(caseXml);
            var users = caseDAL.CurrentCaseUsers(caseObj);

            var xmlString = ObjectSerializer.Serialize<List<UserDetails>>(users);
            return xmlString;
        }

        internal static string IsCaseLockedByLegacy(CytoAppsConfiguration cfg, CaseLockParameter caseLockParam)
        {
            var caseDAL = new CaseDataAccessLocal.CaseDataAccessLocal(cfg.DatabaseServer, cfg.CasebaseFolder);

            var caseXml = Encoding.Unicode.GetString(Convert.FromBase64String(caseLockParam.Case));
            var caseObj = ObjectSerializer.Deserialize<Case>(caseXml);
            var userXml = Encoding.Unicode.GetString(Convert.FromBase64String(caseLockParam.UserDetails));
            var userDetails = ObjectSerializer.Deserialize<UserDetails>(userXml);
            LockResult isCaseLegacyLocked = caseDAL.IsCaseLockedByLegacy(caseObj, userDetails);
            var xmlString = ObjectSerializer.Serialize<LockResult>(isCaseLegacyLocked);
            return xmlString;
        }

        internal static void RemoveCaseLocks(CytoAppsConfiguration cfg, CaseLockParameter caseLockParam)
        {
            var caseDAL = new CaseDataAccessLocal.CaseDataAccessLocal(cfg.DatabaseServer, cfg.CasebaseFolder);

            var caseXml = Encoding.Unicode.GetString(Convert.FromBase64String(caseLockParam.Case));
            var caseObj = ObjectSerializer.Deserialize<Case>(caseXml);
            var userXml = Encoding.Unicode.GetString(Convert.FromBase64String(caseLockParam.UserDetails));
            var userDetails = ObjectSerializer.Deserialize<UserDetails>(userXml);
            caseDAL.RemoveCaseLocks(caseObj, userDetails);
        }

        internal static string RemoveCrashLocksByUser(CytoAppsConfiguration cfg, CaseLockParameter caseCrashParam)
        {
            var caseDAL = new CaseDataAccessLocal.CaseDataAccessLocal(cfg.DatabaseServer, cfg.CasebaseFolder);
            var userXml = Encoding.Unicode.GetString(Convert.FromBase64String(caseCrashParam.UserDetails));
            var userDetails = ObjectSerializer.Deserialize<UserDetails>(userXml);
            var ret = caseDAL.RemoveCrashLocksByUser(caseCrashParam.Case, userDetails);

            StringBuilder sb = new StringBuilder();
            sb.Append("<Success>");
            sb.Append(ret.ToString());
            sb.AppendLine("</Success>");

            return sb.ToString();
        }

    }
}
