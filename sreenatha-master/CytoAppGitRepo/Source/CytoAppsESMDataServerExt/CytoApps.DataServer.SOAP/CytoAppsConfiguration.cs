﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace CytoApps.DataServer.SOAP
{
    /// <summary>
    /// various configuration setting for data access
    /// </summary>
    internal class CytoAppsConfiguration
    {
        private const string DefaultDbServer = "localhost";
        private const string DefaultCasebasePath = @"\\localhost\casebase\cases";

        /// <summary>
        /// Load in setting from configuration file, lets use the DataServer config for now
        /// </summary>
        internal static CytoAppsConfiguration LoadConfigSetting()
        {
            var cfg = new CytoAppsConfiguration { CasebaseFolder = DefaultCasebasePath, DatabaseServer = DefaultDbServer };
             if (ConfigurationManager.AppSettings["CytoApps.DatabaseInstance"] != null)
                cfg.DatabaseServer = ConfigurationManager.AppSettings["CytoApps.DatabaseInstance"];
            if (ConfigurationManager.AppSettings["CytoApps.CasebaseFolder"] != null)
                cfg.CasebaseFolder = ConfigurationManager.AppSettings["CytoApps.CasebaseFolder"];
            return cfg;
        }

        /// <summary>
        /// The SQLSERVER instance e.g. Myserver\SQLExpress
        /// </summary>
        internal string DatabaseServer { get; private set; }

        /// <summary>
        /// folder containing the cytovision cases, UNC or absolute
        /// </summary>
        internal string CasebaseFolder { get; private set; }

    }
}
