﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CytoApps.DataServer.Common;
using CVKaryoDAL.Models;

namespace CytoApps.DataServer.SOAP
{
    /// <summary>
    /// Handles accessing CytoApps casebase data via local DAL for legacy Karyotype data. returns expected XML response for request
    /// </summary>
    internal class CytoAppsKaryoService
    {
        internal static string CanAnalyse(CytoAppsConfiguration cfg, SlideParameter slideParm)
        {
            StringBuilder sb = new StringBuilder();
            var karDAL = new CVKaryoDataAccessLocal.CVKaryoDataAccessLocal(cfg.CasebaseFolder);

            bool canAnalyse = karDAL.CanAnalyseSlide(new Models.Slide(slideParm.CaseId, "", slideParm.SlideId));

            sb.Append("<Success>");
            sb.Append(true.ToString());
            sb.AppendLine("</Success>");

            return sb.ToString();
        }

        internal static string GetCells(CytoAppsConfiguration cfg, SlideParameter slideParm)
        {
            StringBuilder sb = new StringBuilder();
            var karDAL = new CVKaryoDataAccessLocal.CVKaryoDataAccessLocal(cfg.CasebaseFolder);

            var cells = karDAL.GetCells(new Models.Slide(slideParm.CaseId, "", slideParm.SlideId));

            // REFACTOR to class serialization
            sb.Append("<CellList>").AppendLine();
            foreach (var c in cells)
            {               
                sb.AppendLine("<Cell>");

                sb.Append("<SlideId>");
                sb.Append(c.Slide.Id);
                sb.AppendLine("</SlideId>");
                sb.Append("<Id>");
                sb.Append(c.Id);
                sb.AppendLine("</Id>");
                sb.Append("<Name>");
                sb.Append(c.Name);
                sb.AppendLine("</Name>");
                sb.Append("<HasRaw>");
                sb.Append(c.HasRaw);
                sb.AppendLine("</HasRaw>");
                sb.Append("<HasMetaphase>");
                sb.Append(c.HasMetaphase);
                sb.AppendLine("</HasMetaphase>");
                sb.Append("<HasKaryotype>");
                sb.Append(c.HasKaryotype);
                sb.AppendLine("</HasKaryotype>");
                sb.Append("<HasProbe>");
                sb.Append(c.HasProbe);
                sb.AppendLine("</HasProbe>");


                sb.AppendLine("</Cell>");
            }
            sb.AppendLine("</CellList>");

            return sb.ToString();
        }

        internal static string GetMetaphaseImageData(CytoAppsConfiguration cfg, CellParameter cellParm)
        {
            StringBuilder sb = new StringBuilder();
            var karDAL = new CVKaryoDataAccessLocal.CVKaryoDataAccessLocal(cfg.CasebaseFolder);

            var imageData = karDAL.GetMetaphaseImageData(new KaryoCell() { Id = cellParm.CellId, Slide = new Models.Slide(cellParm.CaseId, "", cellParm.SlideId)});

            sb.Append("<ImageInfo>");
            sb.AppendLine("<ImageData>");
            sb.AppendLine(Convert.ToBase64String(imageData));
            sb.AppendLine("</ImageData>");
            sb.AppendLine("</ImageInfo>");

            return sb.ToString();
        }

        internal static string GetKaryotypeImageData(CytoAppsConfiguration cfg, CellParameter cellParm)
        {
            StringBuilder sb = new StringBuilder();
            var karDAL = new CVKaryoDataAccessLocal.CVKaryoDataAccessLocal(cfg.CasebaseFolder);

            var imageData = karDAL.GetKaryotypeImageData(new KaryoCell() { Id = cellParm.CellId, Slide = new Models.Slide(cellParm.CaseId, "", cellParm.SlideId)});

            sb.Append("<ImageInfo>");
            sb.AppendLine("<ImageData>");
            sb.AppendLine(Convert.ToBase64String(imageData));
            sb.AppendLine("</ImageData>");
            sb.AppendLine("</ImageInfo>");

            return sb.ToString();
        }

        internal static string GetProbeImageData(CytoAppsConfiguration cfg, CellParameter cellParm)
        {
            StringBuilder sb = new StringBuilder();
            var karDAL = new CVKaryoDataAccessLocal.CVKaryoDataAccessLocal(cfg.CasebaseFolder);

            var imageData = karDAL.GetProbeImageData(new KaryoCell() { Id = cellParm.CellId, Slide = new Models.Slide(cellParm.CaseId, "", cellParm.SlideId) });

            sb.Append("<ImageInfo>");
            sb.AppendLine("<ImageData>");
            sb.AppendLine(Convert.ToBase64String(imageData));
            sb.AppendLine("</ImageData>");
            sb.AppendLine("</ImageInfo>");

            return sb.ToString();
        }


        internal static string PutKaryotypeImageData(CytoAppsConfiguration cfg, CellImageDataParameter input)
        {
            StringBuilder sb = new StringBuilder();

            var karDAL = new CVKaryoDataAccessLocal.CVKaryoDataAccessLocal(cfg.CasebaseFolder);

            karDAL.SaveKaryotypeImageData(new KaryoCell() { Id = input.CellId, Slide = new Models.Slide(input.CaseId, "", input.SlideId) }, input.ImageId, Convert.FromBase64String(input.ImageData));

            sb.Append("<Success>").AppendLine();
            sb.Append(true.ToString());
            sb.AppendLine("</Success>");

            return sb.ToString();
        }
    }
}
