﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;
using CytoApps.DataServer.Common;
using CytoApps.Models;
using PCVImageRenderers.Models;
using ProbeCaseViewDataAccess.Models;

namespace CytoApps.DataServer.SOAP
{
    /// <summary>
    /// Handles accessing CytoApps casebase data via local DAL for legacy PCV FISH data. returns expected XML response for request
    /// </summary>
    internal class CytoAppsPCVService
    {
        internal static string CanAnalyse(CytoAppsConfiguration cfg, SlideParameter slideParm)
        {
            StringBuilder sb = new StringBuilder();
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);
            bool canAnalyse = pcvDAL.CanAnalyseSlide(new Models.Slide(slideParm.CaseId, "", slideParm.SlideId));

            sb.Append("<Success>");
            sb.Append(canAnalyse.ToString());
            sb.AppendLine("</Success>");

            return sb.ToString();
        }

        internal static string GetScanAreas(CytoAppsConfiguration cfg, SlideParameter slideParm)
        {
            StringBuilder sb = new StringBuilder();
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var scanAreas = pcvDAL.GetScanAreas(new Models.Slide(slideParm.CaseId, "", slideParm.SlideId));

            // Serialize the scanareas class to a xml string
            var xmlString = ObjectSerializer.Serialize<List<ScanArea>>(scanAreas.ToList());

            return xmlString;
        }

        internal static string GetSessionScores(CytoAppsConfiguration cfg, PCVSessionParameter sessionParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(sessionParam.CaseId, "", sessionParam.SlideId);
            var scores = pcvDAL.GetScores(new Session(slide, "", sessionParam.SessionId, sessionParam.ScanAreaId));

            // Serialize the scores class to a xml string of retquired format (no xml declaration)
            var xmlString = ObjectSerializer.Serialize<SessionScores>(scores);

            return xmlString;
        }

        internal static string GetScoreStats(CytoAppsConfiguration cfg, PCVSessionParameter sessionParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(sessionParam.CaseId, "", sessionParam.SlideId);
            var stats = pcvDAL.GetScoreStats(new Session(slide, "", sessionParam.SessionId, sessionParam.ScanAreaId));

            // Serialize the scores class to a xml string of retquired format (no xml declaration)
            var xmlString = ObjectSerializer.Serialize<Score[]>(stats);

            return xmlString;
        }


        internal static string GetAssayData(CytoAppsConfiguration cfg, PCVSessionParameter sessionParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(sessionParam.CaseId, "", sessionParam.SlideId);
            var assayData = pcvDAL.GetAssayData(new Session(slide, "", sessionParam.SessionId, sessionParam.ScanAreaId));

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<AssayData>");
            sb.AppendLine(Convert.ToBase64String(assayData));
            sb.AppendLine("</AssayData>");

            return sb.ToString();
        }

        internal static string GetThumbnailData(CytoAppsConfiguration cfg, PCVScoreParameter scoreParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(scoreParam.CaseId, "", scoreParam.SlideId);
            var imageData = pcvDAL.GetThumbnailData(new Session(slide, "", scoreParam.SessionId, scoreParam.ScanAreaId), new Score() { Id = new Guid(scoreParam.ScoreId) });

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<ImageData>");
            sb.AppendLine(Convert.ToBase64String(imageData));
            sb.AppendLine("</ImageData>");

            return sb.ToString();
        }

        internal static string GetThumbnailBitmap(CytoAppsConfiguration cfg, PCVDisplayChannelsParameter scoreParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(scoreParam.CaseId, "", scoreParam.SlideId);
            var session = new Session(slide, "", scoreParam.SessionId, scoreParam.ScanAreaId);
            var score = new Score() { Id = new Guid(scoreParam.ScoreId) };
            var channels = (from c in scoreParam.Channels select new ChannelDisplay { Name = c.Name, HistogramLow = c.Min, HistogramHigh = c.Max, Gamma = 1.0 }).ToList();

            var imageData = pcvDAL.GetThumbnailBitmap(session, score, scoreParam.StackIndex, channels, scoreParam.Attenuate, scoreParam.Invert, false);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<ImageData>");
            sb.AppendLine(Convert.ToBase64String(imageData));
            sb.AppendLine("</ImageData>");

            return sb.ToString();
        }


        internal static string GetOverlayData(CytoAppsConfiguration cfg, PCVOverlayParameter overlayParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(overlayParam.CaseId, "", overlayParam.SlideId);
            var session = new Session(slide, "", overlayParam.SessionId, overlayParam.ScanAreaId);
            var score = new Score() { Id = new Guid(overlayParam.ScoreId) };

            var imageData = pcvDAL.GetOverlayData(session, score, new Overlay() { ID = overlayParam.OverlayId });

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<ImageData>");
            sb.AppendLine(Convert.ToBase64String(imageData));
            sb.AppendLine("</ImageData>");

            return sb.ToString();
        }

        internal static string GetFrames(CytoAppsConfiguration cfg, PCVScanAreaParameter scanAreaParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(scanAreaParam.CaseId, "", scanAreaParam.SlideId);
            var frames = pcvDAL.GetFrames(new ScanArea(slide, "", scanAreaParam.ScanAreaId));

            // Serialize the scores class to a xml string of retquired format (no xml declaration)
            var xmlString = ObjectSerializer.Serialize<List<Frame>>(frames.ToList());

            return xmlString;
        }

        internal static string GetFrameComponentData(CytoAppsConfiguration cfg, PCVFrameComponentParameter frameComponent)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(frameComponent.CaseId, "", frameComponent.SlideId);

            var imageData = pcvDAL.GetFrameComponentData(slide, frameComponent.FrameId, frameComponent.ComponentId);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<ImageData>");
            sb.AppendLine(Convert.ToBase64String(imageData));
            sb.AppendLine("</ImageData>");

            return sb.ToString();
        }

        internal static string GetFrameThumbnailBitmap(CytoAppsConfiguration _cfg, PCVFrameDisplayChannelsParameter frameDisplayParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_cfg.CasebaseFolder);

            var slide = new Slide(frameDisplayParam.CaseId, "", frameDisplayParam.SlideId);

            var frames = pcvDAL.GetFrames(new ScanArea(slide, "", frameDisplayParam.ScanAreaId));
            var frame = (from f in frames where f.Id.ToString() == frameDisplayParam.FrameId select f).FirstOrDefault();

            // V Hacky; currently only a partial Serialization of ComponentLevels (needs reactored, so create here then copy over changable values)
            var componentLevels = (from c in frame.Components select new ComponentLevels(c, ComponentOffset.Zero)).ToList();

            var levelsXml = Encoding.Unicode.GetString(Convert.FromBase64String(frameDisplayParam.Levels));
            var levels = ObjectSerializer.Deserialize<List<ComponentLevels>>(levelsXml).ToArray();

            // we need to use the desired settings for frame display so override read in set with parameter; only need a subset for render as badly designed :)
            if (levels.Length == componentLevels.Count)
            {
                int index = 0;
                foreach (var c in componentLevels)
                {
                    c.Low = levels[index].Low;
                    c.High = levels[index].High;
                    c.IsVisible = levels[index].IsVisible;
                    c.OffsetX = levels[index].OffsetX;
                    c.OffsetY = levels[index].OffsetY;
                    c.SetByUser = levels[index].SetByUser;
                    index++;
                }
            }

            var settingsXml = Encoding.Unicode.GetString(Convert.FromBase64String(frameDisplayParam.Settings));
            var settings = ObjectSerializer.Deserialize<FrameRenderSettings>(settingsXml);

            var imageData = pcvDAL.GetFrameBitmap(slide, frameDisplayParam.ScanAreaId, frame, componentLevels, frameDisplayParam.RequestedZ, settings);

            StringBuilder sb = new StringBuilder();

            var histos = (from h in componentLevels select h.Histogram).ToList();

            // Serialize the scores class to a xml string of retquired format (no xml declaration)
            var histosXml = ObjectSerializer.Serialize<List<Histogram>>(histos);


            sb.AppendLine("<ImageData>");
            sb.AppendLine(Convert.ToBase64String(imageData));
            sb.AppendLine("</ImageData>");
            sb.AppendLine("<HistoData>");
            sb.AppendLine(histosXml.ToString());
            sb.AppendLine("</HistoData>");


            return sb.ToString();
        }

        internal static string CreateThumbnailBitmap(CytoAppsConfiguration cfg, PCVCreateScoreParameter scoreParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(scoreParam.CaseId, "", scoreParam.SlideId);
            var session = new Session(slide, "", scoreParam.SessionId, scoreParam.ScanAreaId);
            var channels = (from c in scoreParam.Channels select new ChannelDisplay { Name = c.Name, HistogramLow = c.Min, HistogramHigh = c.Max, Gamma = 1.0 }).ToList();
            var coords = new Point(scoreParam.PosX, scoreParam.PosY);

            var scoreThumb = new ScoreThumbnail()
            {
                ScoreId = new Guid(scoreParam.ScoreId),
                FrameID = new Guid(scoreParam.FrameId),
                Coordinates = coords,
                Channels = channels,
                StackIndex = scoreParam.StackIndex,
                AttenuateCounterstain = scoreParam.Attenuate,
                InvertCounterstain = scoreParam.Invert,
                RegionId = Guid.Empty
            };

            var imageData = pcvDAL.CreateThumbnailBitmap(scoreThumb, session);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<ImageData>");
            sb.AppendLine(Convert.ToBase64String(imageData));
            sb.AppendLine("</ImageData>");

            return sb.ToString();
        }

        internal static void SaveSession(CytoAppsConfiguration cfg, PCVSaveSessionParameter saveParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(saveParam.CaseId, "", saveParam.SlideId);
            var session = new Session(slide, "", saveParam.SessionId, saveParam.ScanAreaId);

            // bit hacky but we just send the object serialized to xml as encoded string, unpack and deserialize this end
            var settingsXml = Encoding.Unicode.GetString(Convert.FromBase64String(saveParam.ScoreSettingsList));
            var scoresSettings = ObjectSerializer.Deserialize<List<ScoreSettings>>(settingsXml);

            var scoresXml = Encoding.Unicode.GetString(Convert.FromBase64String(saveParam.Scores));
            var scores = ObjectSerializer.Deserialize<SessionScores>(scoresXml);

            pcvDAL.SaveSession(session, scoresSettings, scores);
        }

        internal static string CreateNewSession(CytoAppsConfiguration cfg, PCVNewSessionParameter newSessionParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(newSessionParam.CaseId, "", newSessionParam.SlideId);

            // need to find scanarea, need existing sessions etc
            var existingScanAreas = pcvDAL.GetScanAreas(slide);
            var foundScanArea = existingScanAreas.Where(a => a.Id.ToLower() == newSessionParam.ScanAreaId.ToLower()).FirstOrDefault();

            string xmlString = "";
            if (foundScanArea != null)
            {
                var session = pcvDAL.CreateNewSession(foundScanArea, newSessionParam.SessionId, newSessionParam.UseSpotScores);
                // Serialize to xml (no xml declaration)
                xmlString = ObjectSerializer.Serialize<Session>(session);
            }

            return xmlString;
        }

        internal static void DeleteSession(CytoAppsConfiguration cfg, PCVSessionParameter sessionParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(sessionParam.CaseId, "", sessionParam.SlideId);
            var session = new Session(slide, "", sessionParam.SessionId, sessionParam.ScanAreaId);

            pcvDAL.DeleteSession(session);
        }

        internal static string GetCachedFrameThumbnailBitmap(CytoAppsConfiguration cfg, PCVFrameParameter frameParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var imageData = pcvDAL.GetCachedFrameBitmap(frameParam.CaseId, frameParam.SlideId, null, frameParam.FrameId);
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<ImageData>");
            sb.AppendLine(Convert.ToBase64String(imageData));
            sb.AppendLine("</ImageData>");

            return sb.ToString();
        }

        internal static string GetFrameCellCount(CytoAppsConfiguration cfg, PCVScanAreaFrameParameter frameParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(frameParam.CaseId, "", frameParam.SlideId);
            var scanArea = new ScanArea(slide, "", frameParam.ScanAreaId);

            var count = pcvDAL.GetFrameCellCount(scanArea, frameParam.FrameId);
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<CellCount>");
            sb.AppendLine(count.ToString());
            sb.AppendLine("</CellCount>");

            return sb.ToString();
        }

        internal static void UpdateScores(CytoAppsConfiguration cfg, PCVUpdateScoresParameter scoresParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(scoresParam.CaseId, "", scoresParam.SlideId);
            var session = new Session(slide, "", scoresParam.SessionId, scoresParam.ScanAreaId);

            var framesXml = Encoding.Unicode.GetString(Convert.FromBase64String(scoresParam.FrameIDList));
            var frameIds = ObjectSerializer.Deserialize<List<string>>(framesXml);

            pcvDAL.UpdateScores(session, frameIds);
        }

        internal static string GetSelectedFramesForSession(CytoAppsConfiguration cfg, PCVSessionParameter sessionParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(sessionParam.CaseId, "", sessionParam.SlideId);
            var frames = pcvDAL.GetSelectedFramesForSession(new Session(slide, "", sessionParam.SessionId, sessionParam.ScanAreaId));

            var xmlString = ObjectSerializer.Serialize<List<string>>(frames.ToList());

            return xmlString;
        }

        internal static string GetClassifers(CytoAppsConfiguration _cfg)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_cfg.CasebaseFolder);

            var info = pcvDAL.GetClassifierNamesAndScripts();

            var xmlString = ObjectSerializer.Serialize<AssayInfoWrapper>(info);

            return xmlString;
        }

        internal static string GetCompatibleAssays(CytoAppsConfiguration _cfg, PCVFrameComponentsParameter input)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_cfg.CasebaseFolder);

            var componentsXml = Encoding.Unicode.GetString(Convert.FromBase64String(input.FrameComponents));
            var components = ObjectSerializer.Deserialize<List<FrameComponent>>(componentsXml);

            var assays = pcvDAL.GetCompatibleAssays(components.ToArray());
            var xmlString = ObjectSerializer.Serialize<List<string>>(assays);

            return xmlString;
        }

        internal static string GetAllDefaultAssays(CytoAppsConfiguration _cfg)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_cfg.CasebaseFolder);

            var assays = pcvDAL.GetAllDefaultAssays();

            var xmlString = ObjectSerializer.Serialize<List<string>>(assays);

            return xmlString;
        }

        internal static void SaveAsDefaultAssay(CytoAppsConfiguration _cfg, PCVSaveDefaultAssayParameter input)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_cfg.CasebaseFolder);

            var assayData = Convert.FromBase64String(input.AssayData);

            pcvDAL.SaveAsDefaultAssay(assayData, input.AssayName);
        }

        internal static string GetDefaultAssayData(CytoAppsConfiguration _cfg, PCVAssayNameParameter input)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_cfg.CasebaseFolder);

            var assayData = pcvDAL.GetDefaultAssayData(input.AssayName);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<AssayData>");
            if (assayData != null)
                sb.AppendLine(Convert.ToBase64String(assayData));
            sb.AppendLine("</AssayData>");

            return sb.ToString();
        }

        internal static string GetSlideOverviewImage(CytoAppsConfiguration cfg, PCVSlideOverviewImageParameter input)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(input.CaseId, "", input.SlideId);

            var imageData = pcvDAL.GetSlideOverviewImage(slide, input.ImageName);
            StringBuilder sb = new StringBuilder();

            if (imageData != null)
            {
                sb.AppendLine("<ImageData>");
                sb.AppendLine(Convert.ToBase64String(imageData));
                sb.AppendLine("</ImageData>");
            }

            return sb.ToString();
        }

        internal static string GetFrameOutlines(CytoAppsConfiguration cfg, PCVSlideParameter input)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(input.CaseId, "", input.SlideId);

            var frames = pcvDAL.GetFrameOutlines(slide);

            // Serialize the scores class to a xml string of retquired format (no xml declaration)
            var xmlString = ObjectSerializer.Serialize<List<FrameOutline>>(frames.ToList());

            return xmlString;
        }

        internal static void QueueRegionForAnalysis(CytoAppsConfiguration cfg, PCVAnalysisRegionParameter input)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var analysisRegionXml = Encoding.Unicode.GetString(Convert.FromBase64String(input.AnalysisRegion));
            var analysisRegion = ObjectSerializer.Deserialize<AnalysisRegion>(analysisRegionXml);

            pcvDAL.QueueRegionForAnalysis(analysisRegion);
        }

        internal static string GetAllAnalysisRegions(CytoAppsConfiguration _cfg, PCVGetAnalysisRegionParameter input)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_cfg.CasebaseFolder);

            var sessionXml = Encoding.Unicode.GetString(Convert.FromBase64String(input.Session));
            var session = ObjectSerializer.Deserialize<Session>(sessionXml);

            var frameXml = Encoding.Unicode.GetString(Convert.FromBase64String(input.Frames));
            var frames = ObjectSerializer.Deserialize<List<Frame>>(frameXml);

            var analysisRegions = pcvDAL.GetAllAnalysisRegions(session);
            var xmlString = ObjectSerializer.Serialize<List<AnalysisRegion>>(analysisRegions.ToList());

            return xmlString;
        }


        internal static string GetScoreByRegionAnalysis(CytoAppsConfiguration _cfg, PCVPollScoresParameter input)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_cfg.CasebaseFolder);

            var regionsXml = Encoding.Unicode.GetString(Convert.FromBase64String(input.Regions));
            var analysisRegions = ObjectSerializer.Deserialize<List<AnalysisRegion>>(regionsXml);

            var scores = pcvDAL.GetScoreByRegionAnalysis(analysisRegions);
            var xmlString = ObjectSerializer.Serialize<List<Score>>(scores.ToList());

            return xmlString;
        }

        internal static void DeleteRegions(CytoAppsConfiguration _cfg, PCVDeleteRegionsParameter input)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_cfg.CasebaseFolder);

            var sessionXml = Encoding.Unicode.GetString(Convert.FromBase64String(input.Session));
            var session = ObjectSerializer.Deserialize<Session>(sessionXml);

            var regionsXml = Encoding.Unicode.GetString(Convert.FromBase64String(input.Regions));
            var regions = ObjectSerializer.Deserialize<List<string>>(regionsXml);

            pcvDAL.DeleteRegions(session, regions);
        }

        internal static string GetStoredCommonAnnotations(CytoAppsConfiguration _cfg)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_cfg.CasebaseFolder);
            var annotations = pcvDAL.GetStoredCommonAnnotations();
            var xmlString = ObjectSerializer.Serialize<List<string>>(annotations);
            return xmlString;
        }

        internal static string GetAllFramesAnnotation(CytoAppsConfiguration _cfg, PCVGetAllFramesAnnotationParameter input)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_cfg.CasebaseFolder);

            var sessionXml = Encoding.Unicode.GetString(Convert.FromBase64String(input.Session));
            var session = ObjectSerializer.Deserialize<Session>(sessionXml);

            var frameXml = Encoding.Unicode.GetString(Convert.FromBase64String(input.Frames));
            var frames = ObjectSerializer.Deserialize<List<Frame>>(frameXml);

            var annotations = pcvDAL.GetAllFramesAnnotation(session, frames);
            var xmlString = ObjectSerializer.Serialize<List<AnnotationIdentifier>>(annotations);
            return xmlString;
        }

        internal static string SaveAnnotation(CytoAppsConfiguration _cfg, PCVSaveAnnotationParameter input)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_cfg.CasebaseFolder);
            StringBuilder sb = new StringBuilder();
            var sessionXml = Encoding.Unicode.GetString(Convert.FromBase64String(input.Session));
            var session = ObjectSerializer.Deserialize<Session>(sessionXml);

            var frameAnnotationIdentifiersXml = Encoding.Unicode.GetString(Convert.FromBase64String(input.AnnotationIdentifier));
            var frameAnnotationIdentifiers = ObjectSerializer.Deserialize<List<AnnotationIdentifier>>(frameAnnotationIdentifiersXml);

            bool isAnnotationsSaved = pcvDAL.SaveAnnotation(session, frameAnnotationIdentifiers);
            sb.Append("<Success>");
            sb.Append(isAnnotationsSaved.ToString());
            sb.AppendLine("</Success>");

            return sb.ToString();
        }

        internal static void SaveCommonStoredAnnotation(CytoAppsConfiguration _cfg, PCVSaveCommonStoredAnnotationParameter input)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_cfg.CasebaseFolder);

            var saveCommonStoredAnnotationXml = Encoding.Unicode.GetString(Convert.FromBase64String(input.Annotations));
            var saveCommonStoredAnnotations = ObjectSerializer.Deserialize<List<string>>(saveCommonStoredAnnotationXml);
            pcvDAL.SaveCommonStoredAnnotation(saveCommonStoredAnnotations);
        }

        internal static string GetAssayDataAndCreateBackup(CytoAppsConfiguration cfg, PCVSessionParameter sessionParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(sessionParam.CaseId, "", sessionParam.SlideId);
            var assayData = pcvDAL.GetAssayDataAndCreateBackup(new Session(slide, "", sessionParam.SessionId, sessionParam.ScanAreaId));

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<AssayData>");
            sb.AppendLine(Convert.ToBase64String(assayData));
            sb.AppendLine("</AssayData>");

            return sb.ToString();
        }

        internal static void RestoreAssayData(CytoAppsConfiguration cfg, PCVSessionParameter sessionParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);
            var slide = new Slide(sessionParam.CaseId, "", sessionParam.SlideId);
            var session = new Session(slide, "", sessionParam.SessionId, sessionParam.ScanAreaId);

            pcvDAL.RestoreAssayData(session);
        }

        internal static void SaveAssayData(CytoAppsConfiguration cfg, PCVSaveAssayParameter sessionParam)

        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);
            StringBuilder sb = new StringBuilder();
            var sessionXml = Encoding.Unicode.GetString(Convert.FromBase64String(sessionParam.Session));
            var session = ObjectSerializer.Deserialize<Session>(sessionXml);
            var assayData = Convert.FromBase64String(sessionParam.AssayData);

            pcvDAL.SaveAssayData(assayData, session);

        }

        internal static string CheckKLockExists(CytoAppsConfiguration cfg, PCVLockObjectParameter lockParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder, cfg.DatabaseServer);
            var sessionXml = Encoding.Unicode.GetString(Convert.FromBase64String(lockParam.Session));
            var session = ObjectSerializer.Deserialize<Session>(sessionXml);

            var userDetailsXml = Encoding.Unicode.GetString(Convert.FromBase64String(lockParam.UserDetails));
            var userDetailObj = ObjectSerializer.Deserialize<UserDetails>(userDetailsXml);

            LockResult isSessionLocked = pcvDAL.CheckKLockExists(userDetailObj, lockParam.CaseName, session);
            var xmlString = ObjectSerializer.Serialize<LockResult>(isSessionLocked);
            return xmlString;
        }

        internal static void LockFeatureorObject(CytoAppsConfiguration cfg, PCVLockObjectParameter lockParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder, cfg.DatabaseServer);
            var sessionXml = Encoding.Unicode.GetString(Convert.FromBase64String(lockParam.Session));
            var session = ObjectSerializer.Deserialize<Session>(sessionXml);

            var userDetailsXml = Encoding.Unicode.GetString(Convert.FromBase64String(lockParam.UserDetails));
            var userDetailObj = ObjectSerializer.Deserialize<UserDetails>(userDetailsXml);

            pcvDAL.LockFeatureorObject(userDetailObj, lockParam.CaseName, session);
        }

        internal static void RemoveLock(CytoAppsConfiguration cfg, PCVRemoveLockObjectParameter lockParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder, cfg.DatabaseServer);
            var userDetailsXml = Encoding.Unicode.GetString(Convert.FromBase64String(lockParam.UserDetails));
            var userDetailObj = ObjectSerializer.Deserialize<UserDetails>(userDetailsXml);

            pcvDAL.RemoveLock(userDetailObj, lockParam.CaseName);
        }


        internal static string CanCreateNewSession(CytoAppsConfiguration _cfg, PCVCanCreateNewSessionParameter input)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_cfg.CasebaseFolder);
            StringBuilder sb = new StringBuilder();
            var slideXMl = Encoding.Unicode.GetString(Convert.FromBase64String(input.Slide));
            var slide = ObjectSerializer.Deserialize<Slide>(slideXMl);

            bool canCreateNewSession = pcvDAL.CanCreateNewSession(input.SessionName, slide, input.ScanAreaName);
            sb.Append("<Success>");
            sb.Append(canCreateNewSession.ToString());
            sb.AppendLine("</Success>");

            return sb.ToString();
        }
        internal static string GetSpotFolderNamesForReprocess(CytoAppsConfiguration cfg, SpotFolderParameter spotFolderParameter)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);
            var sessionXml = Encoding.Unicode.GetString(Convert.FromBase64String(spotFolderParameter.Session));
            var session = ObjectSerializer.Deserialize<Session>(sessionXml);

            var spotFolders = pcvDAL.GetSpotFolderNamesForReprocess(session, spotFolderParameter.FrameId);
            var xmlString = ObjectSerializer.Serialize<List<string>>(spotFolders.ToList());
            return xmlString;
        }

        internal static string StartReprocessing(CytoAppsConfiguration cfg, ReprocessParameter reprocessParameter)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);
            var sessionXML = Encoding.Unicode.GetString(Convert.FromBase64String(reprocessParameter.Session));
            var session = ObjectSerializer.Deserialize<Session>(sessionXML);            
            var spotFoldersXml = Encoding.Unicode.GetString(Convert.FromBase64String(reprocessParameter.SpotFolders));
            var spotFolders = ObjectSerializer.Deserialize<List<string>>(spotFoldersXml);
           
            StringBuilder sb = new StringBuilder();
            bool isReProcessStarted = pcvDAL.StartReprocessing(session,spotFolders);
            sb.Append("<Success>");
            sb.Append(isReProcessStarted.ToString());
            sb.AppendLine("</Success>");
            return sb.ToString();            
        }

        internal static string GetScoresFromSpotFoldersProcessed(CytoAppsConfiguration cfg, ReprocessParameter reprocessParameter)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);
            var sessionXML = Encoding.Unicode.GetString(Convert.FromBase64String(reprocessParameter.Session));
            var session = ObjectSerializer.Deserialize<Session>(sessionXML);
            var spotFoldersXml = Encoding.Unicode.GetString(Convert.FromBase64String(reprocessParameter.SpotFolders));
            var spotFolders = ObjectSerializer.Deserialize<List<string>>(spotFoldersXml);

            ReprocessScores reprocessScores = pcvDAL.GetScoresFromSpotFoldersProcessed(session, spotFolders);
            var xmlString = ObjectSerializer.Serialize<ReprocessScores>(reprocessScores);
            return xmlString;
        }

        internal static void RestoreSession(CytoAppsConfiguration cfg, PCVRestoreSessionParameter restoreParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(restoreParam.CaseId, "", restoreParam.SlideId);
            var session = new Session(slide, "", restoreParam.SessionId, restoreParam.ScanAreaId);
            var settingsXml = Encoding.Unicode.GetString(Convert.FromBase64String(restoreParam.ScoreSettingsList));
            var scoresSettings = ObjectSerializer.Deserialize<List<ScoreSettings>>(settingsXml);
            var regionsXml = Encoding.Unicode.GetString(Convert.FromBase64String(restoreParam.Regions));
            var regions = ObjectSerializer.Deserialize<List<string>>(regionsXml);

            pcvDAL.RestoreSession(session, scoresSettings, regions);
        }

        internal static void DeleteNewScoresAndRegionsFromSession(CytoAppsConfiguration cfg, PCVDeleteNewScoreFromSessionParameter deleteScoreParam)
        {
            var pcvDAL = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(cfg.CasebaseFolder);

            var slide = new Slide(deleteScoreParam.CaseId, "", deleteScoreParam.SlideId);
            var session = new Session(slide, "", deleteScoreParam.SessionId, deleteScoreParam.ScanAreaId);
            var settingsXml = Encoding.Unicode.GetString(Convert.FromBase64String(deleteScoreParam.ScoreSettingsList));
            var scoresSettings = ObjectSerializer.Deserialize<List<ScoreSettings>>(settingsXml);
            var regionsXml = Encoding.Unicode.GetString(Convert.FromBase64String(deleteScoreParam.Regions));
            var regions = ObjectSerializer.Deserialize<List<string>>(regionsXml);

            pcvDAL.DeleteNewScoresAndRegionsFromSession(session, scoresSettings, regions);
        }
    }
}
