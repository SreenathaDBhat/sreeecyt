﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Text;


using Core.DataServer.Contracts;
using Core.DataServer.DbObjects;
using Core.DataServer.DbObjects.Custom.Constants;
using Core.DataServer.SOAP;

using CytoApps.DataServer.Common;
using System.IO;


namespace CytoApps.DataServer.SOAP
{
    [DsProxy(Namespace = CytoAppsServicesProxy.Namespace)]
    public partial class CytoAppsServicesProxy : BaseProxy
    {
        public const string Namespace = "/Aperio/CytoAppsServices";

        static readonly Core.DataServer.Common.ILogger logger = Core.DataServer.Common.LogManager.GetLogger(typeof(CytoAppsServicesProxy));

        private CytoAppsConfiguration _cfg;

        [ImportingConstructor]
        public CytoAppsServicesProxy(Core.DataServer.Common.IAppSettings appSettings, RequestInfo request, CompositionContainer container)
            : base(appSettings, request)
        {
            _cfg = CytoAppsConfiguration.LoadConfigSetting();
        }

        /// <summary>
        /// getImage
        /// </summary>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void GetImage(out string retXml)
        {
            StringBuilder sb = new StringBuilder();

            var imageData = encodeBase64ImageFromPath(@"C:\karyotype.png");

            sb.Append("<ImageInfo>").AppendLine();
            sb.AppendLine("<ImageData>");
            sb.AppendLine(imageData);
            sb.AppendLine("</ImageData>");
            sb.AppendLine("</ImageInfo>");

            retXml = sb.ToString();
        }

        /// <summary>
        /// For transit time test
        /// </summary>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void GetHello(out string retXml)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<Str>Hello</Str>");

            retXml = sb.ToString();
        }

        /// <summary>
        /// GetCaseList
        /// </summary>
        /// <param name="input">Optional search pattern</param>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PutImage(ImageDataParameter input, out string retXml)
        {
            StringBuilder sb = new StringBuilder();

            var path = Path.Combine(@"c:\", input.ImageId);
            File.WriteAllBytes(path, Convert.FromBase64String(input.ImageData));

            sb.Append("<Success>").AppendLine();
            sb.Append(true.ToString());
            sb.AppendLine("</Success>");

            retXml = sb.ToString();
        }


        /// <summary>
        /// GetSlideList
        /// </summary>
        /// <param name="input">case id</param>
        /// <param name="retXml">list of slides for given case</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void GetSlideList(CaseParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsCaseService.GetSlides(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:Case", ex.Message));
            }
        }

        /// <summary>
        /// returns a list of case details for a case
        /// </summary>
        /// <param name="input">case id</param>
        /// <param name="retXml">list of casedetails</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void GetCaseDetails(CaseParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsCaseService.GetCaseDetails(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:Case", ex.Message));
            }
        }

        /// <summary>
        /// Save the case details for a case
        /// </summary>
        /// <param name="input">case id</param>
        /// <param name="retXml">list of casedetails</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void SaveCaseDetails(CaseDetailsParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsCaseService.SaveCaseDetails(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:Case", ex.Message));
            }
        }

        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void GetCaseDetailsFields(out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsCaseService.GetCaseDetailsFields(_cfg);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:Case", ex.Message));
            }
        }

        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void GetCasesAdvanced(CaseAdvancedParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsCaseService.GetCasesAdvanced(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:Case", ex.Message));
            }
        }

        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void GetUniqueCaseFlags(out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsCaseService.GetUniqueCaseFlags(_cfg);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:Case", ex.Message));
            }
        }

        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void GetAllCaseFlags(out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsCaseService.GetAllCaseFlags(_cfg);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:Case", ex.Message));
            }
        }


        /// <summary>
        /// update the case status flag
        /// </summary>
        /// <param name="input">case ids and new case flag</param>
        /// <param name="retXml">list of casedetails</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void UpdateCaseFlag(UpdateCaseFlagParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsCaseService.UpdateCaseFlag(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:Case", ex.Message));
            }
        }

        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void CurrentCaseUsers(CurrentCaseUsersParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsCaseService.CurrentCaseUsers(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:Case", ex.Message));
            }
        }

        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void IsCaseLockedByLegacy(CaseLockParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsCaseService.IsCaseLockedByLegacy(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:Case", ex.Message));
            }
        }

        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void RemoveCrashLocksByUser(CaseLockParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsCaseService.RemoveCrashLocksByUser(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:Case", ex.Message));
            }
        }

        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void RemoveCaseLocks(CaseLockParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                CytoAppsCaseService.RemoveCaseLocks(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:Case", ex.Message));
            }
        }

        private string encodeBase64ImageFromPath(string path)
        {
            string encodedImageData = null;

            if (File.Exists(path))
                encodedImageData = Convert.ToBase64String(File.ReadAllBytes(path));
            return encodedImageData;
        }
    }
}
