﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DataServer.DbObjects.Custom.Constants;
using Core.DataServer.SOAP;
using CytoApps.DataServer.Common;

namespace CytoApps.DataServer.SOAP
{
    /// <summary>
    /// PARTIAL class, source file implements Karyotype data Calls
    /// </summary>
    public partial class CytoAppsServicesProxy
    {
        /// <summary>
        /// KaryoCanAnalyse
        /// </summary>
        /// <param name="input">slide information</param>
        /// <param name="retXml">Can the slide be karyotyped</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void KaryoCanAnalyse(SlideParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsKaryoService.CanAnalyse(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps", ex.Message));
            }
        }


        /// <summary>
        /// KaryoGetCellList
        /// </summary>
        /// <param name="input">slide information</param>
        /// <param name="retXml">Can the slide be karyotyped</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void KaryoGetCellList(SlideParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsKaryoService.GetCells(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps", ex.Message));
            }
        }

        /// <summary>
        /// KaryoGetMetaphaseImage
        /// </summary>
        /// <param name="input">cell information</param>
        /// <param name="retXml">Get the metaphas image in bitmap format</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void KaryoGetMetaphaseImage(CellParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsKaryoService.GetMetaphaseImageData(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps", ex.Message));
            }
        }

        /// <summary>
        /// KaryoGetMetaphaseImage
        /// </summary>
        /// <param name="input">cell information</param>
        /// <param name="retXml">Get the metaphas image in bitmap format</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void KaryoKaryotypeImage(CellParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsKaryoService.GetKaryotypeImageData(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps", ex.Message));
            }
        }

        /// <summary>
        /// KaryoGetProbeImage
        /// </summary>
        /// <param name="input">cell information</param>
        /// <param name="retXml">Get the metaphas image in bitmap format</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void KaryoGetProbeImage(CellParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsKaryoService.GetProbeImageData(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps", ex.Message));
            }
        }


        /// <summary>
        /// KaryoGetMetaphaseImage
        /// </summary>
        /// <param name="input">cell information</param>
        /// <param name="retXml">Get the metaphas image in bitmap format</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void KaryoPutKaryotypeImage(CellImageDataParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsKaryoService.PutKaryotypeImageData(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps", ex.Message));
            }
        }
    }
}
