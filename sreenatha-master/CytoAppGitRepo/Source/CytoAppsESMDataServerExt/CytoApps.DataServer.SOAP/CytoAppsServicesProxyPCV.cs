﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DataServer.DbObjects.Custom.Constants;
using Core.DataServer.SOAP;
using CytoApps.DataServer.Common;

namespace CytoApps.DataServer.SOAP
{
    public partial class CytoAppsServicesProxy
    {
        /// <summary>
        /// PCVCanAnalyse
        /// </summary>
        /// <param name="input">slide information</param>
        /// <param name="retXml">Can the slide be karyotyped</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVCanAnalyse(SlideParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.CanAnalyse(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// PCVCGetScanAreas
        /// </summary>
        /// <param name="input">slide information</param>
        /// <param name="retXml">returns a list of scan areas and their sessions</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetScanAreas(SlideParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetScanAreas(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// PCVCGetScanAreas
        /// </summary>
        /// <param name="input">slide information</param>
        /// <param name="retXml">returns a list of scores for the session</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetSessionScores(PCVSessionParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetSessionScores(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// PCVGetScoreStats
        /// </summary>
        /// <param name="input">slide information</param>
        /// <param name="retXml">returns a list of Score stats</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetScoreStats(PCVSessionParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetScoreStats(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// PCVGetAssayData
        /// </summary>
        /// <param name="input">Session</param>
        /// <param name="retXml">returns the assay file as an encode string</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetAssayData(PCVSessionParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetAssayData(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// PCVGetThumbnailData
        /// Uses the local pcv DAL to read in the proprietary thumbnail format (includes stack)
        /// We may have to render on the server
        /// </summary>
        /// <param name="input">indentifies the related cell score for the image</param>
        /// <param name="retXml">xml patload containing image (encoded)</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetThumbnailData(PCVScoreParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetThumbnailData(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// PCVGetThumbnailBitmap
        /// Uses the local pcv DAL to read in the proprietary thumbnail format (includes stack)
        /// and render it locally and return the bitmap
        /// </summary>
        /// <param name="input">indentifies the related cell score for the image</param>
        /// <param name="retXml">xml patload containing image (encoded)</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetThumbnailBitmap(PCVDisplayChannelsParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetThumbnailBitmap(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// PCVCreateThumbnailBitmap
        /// Uses the local pcv DAL to create the proprietary thumbnail format (includes stack)
        /// and render it locally and return the bitmap. Creates it in a temporary folder in case of cancel
        /// </summary>
        /// <param name="input">indentifies the related cell score for the image</param>
        /// <param name="retXml">xml payload containing image (encoded)</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVCreateThumbnailBitmap(PCVCreateScoreParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.CreateThumbnailBitmap(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// PCVGetFrameBitmap
        /// Uses the local pcv DAL to read in the frame files (includes stack)
        /// and render it locally and return the bitmap
        /// </summary>
        /// <param name="input">indentifies the related frame and display options for the image</param>
        /// <param name="retXml">xml patload containing image (encoded)</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetFrameBitmap(PCVFrameDisplayChannelsParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetFrameThumbnailBitmap(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// PCVGetCachedFrameBitmap
        /// Uses the local pcv DAL to read the cached frame jpgs created by scanning / manual scanning
        /// </summary>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetCachedFrameBitmap(PCVFrameParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetCachedFrameThumbnailBitmap(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetFrameCellCount(PCVScanAreaFrameParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetFrameCellCount(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVUpdateScores(PCVUpdateScoresParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                CytoAppsPCVService.UpdateScores(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetSelectedFramesForSession(PCVSessionParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetSelectedFramesForSession(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }



        /// <summary>
        /// PCVGetOverlay
        /// each overlay is a bitmap that is combined in the app, this method retuens the encoded image data
        /// May not be the most efficent and the overlays should be rendered on the server
        /// </summary>
        /// <param name="input">identified the overlay file</param>
        /// <param name="retXml">encoded image data xml payload</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetOverlayData(PCVOverlayParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetOverlayData(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }


        /// <summary>
        /// PCVGetFrames
        /// </summary>
        /// <param name="input">Session</param>
        /// <param name="retXml">returns the frames</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetFrames(PCVScanAreaParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetFrames(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetFrameComponentData(PCVFrameComponentParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetFrameComponentData(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVSaveSession(PCVSaveSessionParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                CytoAppsPCVService.SaveSession(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVCreateNewSession(PCVNewSessionParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.CreateNewSession(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVDeleteSession(PCVSessionParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                CytoAppsPCVService.DeleteSession(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetClassifiers(out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetClassifers(_cfg);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetCompatibleAssays(PCVFrameComponentsParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetCompatibleAssays(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetAllDefaultAssays(out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetAllDefaultAssays(_cfg);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVSaveAsDefaultAssay(PCVSaveDefaultAssayParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                CytoAppsPCVService.SaveAsDefaultAssay(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetDefaultAssayData(PCVAssayNameParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetDefaultAssayData(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetSlideOverviewImage(PCVSlideOverviewImageParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetSlideOverviewImage(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetFrameOutlines(PCVSlideParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetFrameOutlines(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// GetAllAnalysisRegions
        /// </summary>
        /// <param name="input">Session</param>
        /// <param name="input">Frame</param>
        /// <param name="retXml">returns the analysis regions</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetAllAnalysisRegions(PCVGetAnalysisRegionParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetAllAnalysisRegions(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// PCVQueueRegionForAnalysis
        /// </summary>
        /// <param name="input">AnalysisRegion</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVQueueRegionForAnalysis(PCVAnalysisRegionParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                CytoAppsPCVService.QueueRegionForAnalysis(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// this method polls the Slide/Spot folder for new scores.
        /// </summary>
        /// <param name="input">AnalysisRegion</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetScoreByRegionAnalysis(PCVPollScoresParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetScoreByRegionAnalysis(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// this method deletes the Slide/Spot folder.
        /// </summary>
        /// <param name="input">session and list of regions to be deleted</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVDeleteRegions(PCVDeleteRegionsParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                CytoAppsPCVService.DeleteRegions(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetStoredCommonAnnotations(PCVPollScoresParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetStoredCommonAnnotations(_cfg);

            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetAllFramesAnnotation(PCVGetAllFramesAnnotationParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetAllFramesAnnotation(_cfg, input);

            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVSaveAnnotation(PCVSaveAnnotationParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.SaveAnnotation(_cfg, input);

            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVSaveCommonStoredAnnotation(PCVSaveCommonStoredAnnotationParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                CytoAppsPCVService.SaveCommonStoredAnnotation(_cfg, input);

            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// PCVGetAssayData
        /// </summary>
        /// <param name="input">Session</param>
        /// <param name="retXml">returns the assay file as an encode string</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetAssayDataAndCreateBackup(PCVSessionParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetAssayDataAndCreateBackup(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }


        /// <summary>
        /// RestoreAssayData
        /// </summary>
        /// <param name="input">Session</param>
        /// <param name="retXml">returns the assay file as an encode string</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVRestoreAssayData(PCVSessionParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                CytoAppsPCVService.RestoreAssayData(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// RestoreAssayData
        /// </summary>
        /// <param name="input">Session</param>
        /// <param name="retXml">returns the assay file as an encode string</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVSaveAssayData(PCVSaveAssayParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                CytoAppsPCVService.SaveAssayData(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// RestoreAssayData
        /// </summary>
        /// <param name="input">Session</param>
        /// <param name="retXml">returns the assay file as an encode string</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVCheckKLockExists(PCVLockObjectParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.CheckKLockExists(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }
        /// <summary>
        /// RestoreAssayData
        /// </summary>
        /// <param name="input">Session</param>
        /// <param name="retXml">returns the assay file as an encode string</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVLockFeatureorObject(PCVLockObjectParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                CytoAppsPCVService.LockFeatureorObject(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }
        /// <summary>
        /// RestoreAssayData
        /// </summary>
        /// <param name="input">Session</param>
        /// <param name="retXml">returns the assay file as an encode string</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVRemoveLock(PCVRemoveLockObjectParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                CytoAppsPCVService.RemoveLock(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// RestoreAssayData
        /// </summary>
        /// <param name="input">Session</param>
        /// <param name="retXml">returns the assay file as an encode string</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVCanCreateNewSession(PCVCanCreateNewSessionParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.CanCreateNewSession(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }
        /// Get spot folders for reprocess
        /// </summary>
        /// <param name="input">Session</param>
        /// <param name="retXml">returns the assay file as an encode string</param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetSpotFolderNamesForReprocess(SpotFolderParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetSpotFolderNamesForReprocess(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// start reprocessing
        /// </summary>
        /// <param name="input"></param>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVStartReprocessing(ReprocessParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.StartReprocessing(_cfg, input);

            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// start reprocessing
        /// </summary>
        /// <param name="input"></param>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVGetScoresFromSpotFoldersProcessed(ReprocessParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                retXml = CytoAppsPCVService.GetScoresFromSpotFoldersProcessed(_cfg, input);

            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }

        /// <summary>
        /// Restore session 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVRestoreSession(PCVRestoreSessionParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                CytoAppsPCVService.RestoreSession(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }


        /// <summary>
        /// Delete new scores from session 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="retXml"></param>
        [WebMethod]
        [RoleSecurity(Constants.ExtensionName, RoleCommandNames.AccessWorkflow)]
        public void PCVDeleteNewScoresAndRegionsFromSession(PCVDeleteNewScoreFromSessionParameter input, out string retXml)
        {
            retXml = "";
            try
            {
                CytoAppsPCVService.DeleteNewScoresAndRegionsFromSession(_cfg, input);
            }
            catch (Exception ex)
            {
                logger.Log(new Core.DataServer.Common.LogEventInfo(Core.DataServer.Common.LogLevel.Debug, "CytoApps:PCV", ex.Message));
            }
        }
    }
}
