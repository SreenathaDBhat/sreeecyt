﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace CytoApps.DataServer.SOAP
{
    public class ObjectSerializer
    {
        /// <summary>
        /// Serialize to XML without declaration
        /// </summary>
        /// <typeparam name="T">Type of object toSerialize</typeparam>
        /// <param name="toSerialize">object to serialize</param>
        /// <returns></returns>
        public static string Serialize<T>(T toSerialize)
        {
            // Reform xml declaration as we want to add it in another xml doc
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            var xmlSettings = new XmlWriterSettings
            {
                Indent = false,
                OmitXmlDeclaration = true
            };

            // Serialize object into xml using settings
            using (StringWriter stringWriter = new StringWriter())
            using (var xmlWriter = XmlWriter.Create(stringWriter, xmlSettings))
            {
                var namespaces = new XmlSerializerNamespaces();
                namespaces.Add(string.Empty, string.Empty);

                xmlSerializer.Serialize(xmlWriter, toSerialize, namespaces);
                return stringWriter.ToString();
            }
        }

        public static T Deserialize<T>(string toDeserialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            StringReader textReader = new StringReader(toDeserialize);
            return (T)xmlSerializer.Deserialize(textReader);
        }
    }
}
