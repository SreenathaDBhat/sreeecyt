﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using CytoApps.DataServer.Common;

namespace CytoApps.DataServer.SOAP
{
    /// <summary>
    /// input parameter encapsulating data required to find a PCV session
    /// </summary>
    public class PCVSessionParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("ScanAreaId")]
        public string ScanAreaId;

        [XmlElement("SessionId")]
        public string SessionId;
    }

    /// <summary>
    /// input parameter encapsulating data to identify a PCV score
    /// </summary>
    public class PCVScoreParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("ScanAreaId")]
        public string ScanAreaId;

        [XmlElement("SessionId")]
        public string SessionId;

        [XmlElement("ScoreId")]
        public string ScoreId;
    }

    /// <summary>
    /// input parameter encapsulating data to identify a grid image overlay
    /// </summary>
    public class PCVOverlayParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("ScanAreaId")]
        public string ScanAreaId;

        [XmlElement("SessionId")]
        public string SessionId;

        [XmlElement("ScoreId")]
        public string ScoreId;

        [XmlElement("OverlayId")]
        public string OverlayId;
    }

    /// <summary>
    /// input parameter to identify a PCV ScanArea
    /// </summary>
    public class PCVScanAreaParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("ScanAreaId")]
        public string ScanAreaId;
    }

    /// <summary>
    /// input parameter identifying a PCV frame component (channel)
    /// </summary>
    public class PCVFrameComponentParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("FrameId")]
        public string FrameId;

        [XmlElement("ComponentId")]
        public string ComponentId;
    }


    /// <summary>
    /// Set of displayChannels set to grid renderer to control final image generation
    /// </summary>
    public class PCVDisplayChannelsParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("ScanAreaId")]
        public string ScanAreaId;

        [XmlElement("SessionId")]
        public string SessionId;

        [XmlElement("ScoreId")]
        public string ScoreId;

        [XmlElement("StackIndex")]
        public int StackIndex;

        [XmlElement("Attenuate")]
        public bool Attenuate;

        [XmlElement("Invert")]
        public bool Invert;

        [XmlArray("Channels")]
        [XmlArrayItem(typeof(PCVDisplayChannelParameter), ElementName = "Channel")]
        public PCVDisplayChannelParameter[] Channels;
    }

    /// <summary>
    /// input parameter encapsulating data to identify a PCV score
    /// </summary>
    public class PCVCreateScoreParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("ScanAreaId")]
        public string ScanAreaId;

        [XmlElement("SessionId")]
        public string SessionId;

        [XmlElement("ScoreId")]
        public string ScoreId;

        [XmlElement("FrameId")]
        public string FrameId;

        [XmlElement("PosX")]
        public double PosX;

        [XmlElement("PosY")]
        public double PosY;

        [XmlElement("StackIndex")]
        public int StackIndex;

        [XmlElement("Attenuate")]
        public bool Attenuate;

        [XmlElement("Invert")]
        public bool Invert;

        [XmlArray("Channels")]
        [XmlArrayItem(typeof(PCVDisplayChannelParameter), ElementName = "Channel")]
        public PCVDisplayChannelParameter[] Channels;
    }


    /// <summary>
    /// Set of displayChannels set to frame renderer to control final image generation
    /// </summary>
    public class PCVFrameDisplayChannelsParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("ScanAreaId")]
        public string ScanAreaId;

        [XmlElement("FrameId")]
        public string FrameId;

        [XmlElement("RequestedZ")]
        public int RequestedZ;

        [XmlElement("Levels")]
        public string Levels;

        [XmlElement("Settings")]
        public string Settings;
    }

    /// <summary>
    /// Channel display settings used by thumbnail renderer
    /// </summary>
    public class PCVDisplayChannelParameter
    {
        [XmlElement("Name")]
        public string Name;

        [XmlElement("Min")]
        public double Min;

        [XmlElement("Max")]
        public double Max;

        [XmlElement("Visible")]
        public bool Visible;
    }

    /// <summary>
    /// input parameter encapsulating data to save a session
    /// </summary>
    public class PCVSaveSessionParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("ScanAreaId")]
        public string ScanAreaId;

        [XmlElement("SessionId")]
        public string SessionId;

        [XmlElement("ScoreSettingsList")]
        public string ScoreSettingsList;

        [XmlElement("Scores")]
        public string Scores;
    }
    /// <summary>
    /// 
    /// </summary>
    public class PCVNewSessionParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("ScanAreaId")]
        public string ScanAreaId;

        [XmlElement("SessionId")]
        public string SessionId;

        [XmlElement("UseSpotScores")]
        public bool UseSpotScores;
    }
    /// <summary>
    /// 
    /// </summary>
    public class PCVFrameParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("FrameId")]
        public string FrameId;
    }
    /// <summary>
    /// 
    /// </summary>
    public class PCVScanAreaFrameParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("ScanAreaId")]
        public string ScanAreaId;

        [XmlElement("FrameId")]
        public string FrameId;
    }
    /// <summary>
    /// 
    /// </summary>
    public class PCVUpdateScoresParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("ScanAreaId")]
        public string ScanAreaId;

        [XmlElement("SessionId")]
        public string SessionId;

        [XmlElement("FrameIDList")]
        public string FrameIDList;
    }
    /// <summary>
    /// 
    /// </summary>
    public class PCVSaveDefaultAssayParameter
    {
        [XmlElement("AssayData")]
        public string AssayData;

        [XmlElement("AssayName")]
        public string AssayName;
    }
    /// <summary>
    /// 
    /// </summary>
    public class PCVFrameComponentsParameter
    {
        [XmlElement("FrameComponents")]
        public string FrameComponents;
    }
    /// <summary>
    /// 
    /// </summary>
    public class PCVAssayNameParameter
    {
        [XmlElement("AssayName")]
        public string AssayName;
    }

    /// <summary>
    /// slide overview image parameter for low res images capture at scan time and show overview or etch
    /// </summary>
    public class PCVSlideOverviewImageParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("ImageName")]
        public string ImageName;
    }
    /// <summary>
    /// Slide
    /// </summary>
    public class PCVSlideParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;
    }

    /// <summary>
    /// analysis region parameter for saving the Regions. 
    /// </summary>
    public class PCVAnalysisRegionParameter
    {
        [XmlElement("AnalysisRegion")]
        public string AnalysisRegion;
    }

    /// <summary>
    /// analysis region parameter for loading the Regions.  //RVCMT-HA - can we optimize to use sessionid and frameid ?
    /// </summary>
    public class PCVGetAnalysisRegionParameter
    {
        [XmlElement("Session")]
        public string Session;

        [XmlElement("Frames")]
        public string Frames;
    }

    /// <summary>
    /// analysis region parameter for loading the Regions.  //RVCMT-HA - can we optimize to use sessionid and frameid ?
    /// </summary>
    public class PCVPollScoresParameter
    {
        [XmlElement("Regions")]
        public string Regions;
    }

    /// <summary>
    /// analysis region parameter for deleting the Regions.  //RVCMT-HA - can we optimize to use sessionid ?
    /// </summary>
    public class PCVDeleteRegionsParameter
    {
        [XmlElement("Session")]
        public string Session;

        [XmlElement("Regions")]
        public string Regions;
    }

    public class PCVGetAllFramesAnnotationParameter
    {
        [XmlElement("Session")]
        public string Session;

        [XmlElement("Frames")]
        public string Frames;
    }

    public class PCVSaveAnnotationParameter
    {
        [XmlElement("Session")]
        public string Session;

        [XmlElement("AnnotationIdentifiers")]
        public string AnnotationIdentifier;
    }

    public class PCVSaveCommonStoredAnnotationParameter
    {
        [XmlElement("Annotations")]
        public string Annotations;
    }

    public class PCVSaveAssayParameter
    {
        [XmlElement("AssayData")]
        public string AssayData;

        [XmlElement("Session")]
        public string Session;
    }

    public class PCVLockObjectParameter
    {
        [XmlElement("UserDetails")]
        public string UserDetails;

        [XmlElement("CaseName")]
        public string CaseName;

        [XmlElement("Session")]
        public string Session;
    }

    public class PCVRemoveLockObjectParameter
    {
        [XmlElement("UserDetails")]
        public string UserDetails;

        [XmlElement("CaseName")]
        public string CaseName;
    }

    public class PCVCanCreateNewSessionParameter
    {
        [XmlElement("SessionName")]
        public string SessionName;

        [XmlElement("Slide")]
        public string Slide;

        [XmlElement("ScanAreaName")]
        public string ScanAreaName;
    }

    public class SpotFolderParameter
    {
        [XmlElement("Session")]
        public String Session;

        [XmlElement("FrameId")]
        public String FrameId;
    }

    public class ReprocessParameter
    {
        [XmlElement("Session")]
        public String Session;

        [XmlElement("SpotFolders")]
        public String SpotFolders;
    }

    public class PCVRestoreSessionParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("ScanAreaId")]
        public string ScanAreaId;

        [XmlElement("SessionId")]
        public string SessionId;

        [XmlElement("ScoreSettingsList")]
        public string ScoreSettingsList;

        [XmlElement("Regions")]
        public string Regions;
    }

    public class PCVDeleteNewScoreFromSessionParameter
    {
        [XmlElement("CaseId")]
        public string CaseId;

        [XmlElement("SlideId")]
        public string SlideId;

        [XmlElement("ScanAreaId")]
        public string ScanAreaId;

        [XmlElement("SessionId")]
        public string SessionId;

        [XmlElement("ScoreSettingsList")]
        public string ScoreSettingsList;

        [XmlElement("Regions")]
        public string Regions;
    }
}
