﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CaseDataAccessLocal;
using CytoApps.Interfaces;
using CytoApps.Models;

namespace CytoAppsREST
{
    public class CasesController : ApiController
    {
        private string _casebaseFolder = @"\\localhost\Casebase\cases\"; // TODO , pick up from config
        private string _database = "localhost\\SQLEXPRESS"; // TODO , pick up from config

        private ICaseDataAccess CreateDAL()
        {
            return new CaseDataAccessLocal.CaseDataAccessLocal(_database, _casebaseFolder);
        }

        [Route("api/Case/Cases")]
        public IHttpActionResult GetCases()
        {
            ICaseDataAccess dataSource = CreateDAL();

            var cases = dataSource.GetCases();
            return Ok<IEnumerable<Case>>(cases);
        }

        [Route("api/Case/Cases/{caseId}/CaseDetails")]
        public IHttpActionResult GetCaseDetails(string caseId)
        {
            // NOTE: if the caseName has a '.' in it then we have to call it with a trailing '/'
            // e.g //api/case/GetCaseDetails/my.bigcase/  otherwise a 404; something todo with IISExpress rather than REST web-api maybe
            ICaseDataAccess dataSource = CreateDAL();

            var caseDetails = dataSource.GetCaseDetails(new Case(caseId, caseId));
            return Ok<IEnumerable<CaseDetail>>(caseDetails);
        }

        [Route("api/Case/Cases/{caseId}/Slides")]
        public IHttpActionResult GetSlides(string caseId)
        {
            ICaseDataAccess dataSource = CreateDAL();

            var slides = dataSource.GetSlides(new Case(caseId, caseId));
            return Ok<IEnumerable<Slide>>(slides);
        }
    }
}
