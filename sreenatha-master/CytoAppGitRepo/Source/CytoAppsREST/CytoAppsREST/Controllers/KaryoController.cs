﻿using CVKaryoDAL;
using CVKaryoDAL.Models;
using CytoApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CytoAppsREST.Controllers
{
    public class KaryoController : ApiController
    {
        private string _casebaseFolder = @"\\localhost\Casebase\cases\"; // TODO , pick up from config

        private Slide CreateSlide(string caseId, string slideId)
        {
            return new Slide(caseId, slideId, slideId);
        }

        [Route("api/Karyo/Cases/{caseId}/Slides/{slideId}/CanAnalyse")]
        [HttpGet]
        public IHttpActionResult CanAnalyse(string caseId, string slideId)
        {
            ICVKaryoDataAccess dataSource = new CVKaryoDataAccessLocal.CVKaryoDataAccessLocal(_casebaseFolder);

            var slide = CreateSlide(caseId, slideId);
            var canAnalyse = dataSource.CanAnalyseSlide(slide);
            return Ok<bool>(canAnalyse);
        }

        [Route("api/Karyo/Cases/{caseId}/Slides/{slideId}/Cells")]
//        [HttpGet]
        public IHttpActionResult GetCells(string caseId, string slideId)
        {
            ICVKaryoDataAccess dataSource = new CVKaryoDataAccessLocal.CVKaryoDataAccessLocal(_casebaseFolder);

            var slide = CreateSlide(caseId, slideId);
            var cells = dataSource.GetCells(slide);
            return Ok(cells);
        }


        [Route("api/Karyo/Cases/{caseId}/Slides/{slideId}/Cells/{cellId}/Metaphase")]
        public byte[] GetMetaphaseImageData(string caseId, string slideId, string cellId)
        {
            ICVKaryoDataAccess dataSource = new CVKaryoDataAccessLocal.CVKaryoDataAccessLocal(_casebaseFolder);

            var slide = CreateSlide(caseId, slideId);

            var data = dataSource.GetMetaphaseImageData(new KaryoCell { Id = cellId, Slide = slide });
            return data;
        }

        [Route("api/Karyo/Cases/{caseId}/Slides/{slideId}/Cells/{cellId}/Karyotype")]
        public byte[] GetKaryotypeImageData(string caseId, string slideId, string cellId)
        {
            ICVKaryoDataAccess dataSource = new CVKaryoDataAccessLocal.CVKaryoDataAccessLocal(_casebaseFolder);

            var slide = CreateSlide(caseId, slideId);

            var data = dataSource.GetKaryotypeImageData(new KaryoCell { Id = cellId, Slide = slide });
            return data;
        }
    }
}
