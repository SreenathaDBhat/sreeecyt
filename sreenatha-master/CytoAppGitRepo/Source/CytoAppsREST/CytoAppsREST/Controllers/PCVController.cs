﻿using ProbeCaseViewDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CytoApps.Models;
using ProbeCaseViewDataAccess.Models;
using PCVImageRenderers.Models;

namespace CytoAppsREST.Controllers
{
    public class PCVController : ApiController
    {
        private string _casebaseFolder = @"\\localhost\Casebase\cases\"; // TODO , pick up from config

        private Slide CreateSlide(string caseId, string slideId)
        {
            return new Slide(caseId, slideId, slideId);
        }

        [Route("api/PCV/Cases/{caseId}/Slides/{slideId}/CanAnalyse")]
        [HttpGet]
        public IHttpActionResult CanAnalyse(string caseId, string slideId)
        {
            IProbeCaseViewDataAccess dataSource = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_casebaseFolder);

            var slide = CreateSlide(caseId, slideId);
            var canAnalyse = dataSource.CanAnalyseSlide(slide);
            return Ok<bool>(canAnalyse);
        }

        [Route("api/PCV/Cases/{caseId}/Slides/{slideId}/ScanAreas")]
        public IHttpActionResult GetScanAreas(string caseId, string slideId)
        {
            IProbeCaseViewDataAccess dataSource = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_casebaseFolder);

            var slide = CreateSlide(caseId, slideId);
            var scanAreas = dataSource.GetScanAreas(slide);
            return Ok<IEnumerable<ScanArea>>(scanAreas);
        }

        [Route("api/PCV/Cases/{caseId}/Slides/{slideId}/ScanAreas/{scanAreaId}/Sessions/{sessionId}/Scores")]
        public IHttpActionResult GetScores(string caseId, string slideId, string scanAreaId, string sessionId)
        {
            IProbeCaseViewDataAccess dataSource = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_casebaseFolder);

            var slide = CreateSlide(caseId, slideId);
            var session = new Session(slide, sessionId, sessionId, scanAreaId);
            var sessionScores = dataSource.GetScores(session);
            return Ok<SessionScores>(sessionScores);
        }

        [Route("api/PCV/Cases/{caseId}/Slides/{slideId}/ScanAreas/{scanAreaId}/Frames")]
        public IHttpActionResult GetFrames(string caseId, string slideId, string scanAreaId)
        {
            IProbeCaseViewDataAccess dataSource = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_casebaseFolder);

            var slide = CreateSlide(caseId, slideId);
            var scanArea = new ScanArea(slide, scanAreaId, scanAreaId);

            var frames = dataSource.GetFrames(scanArea);
            return Ok<IEnumerable<Frame>>(frames);
        }

        [Route("api/PCV/Cases/{caseId}/Slides/{slideId}/ScanAreas/{scanAreaId}/Sessions/{sessionId}/Scores/{scoreId}/Thumbnail")]
        [HttpGet]
        public byte[] GetThumbnailData(string caseId, string slideId, string scanAreaId, string sessionId, string scoreId)
        {
            IProbeCaseViewDataAccess dataSource = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_casebaseFolder);

            var slide = CreateSlide(caseId, slideId);
            var session = new Session(slide, sessionId, sessionId, scanAreaId);


            // TODO Score.ID is guid???
            var data = dataSource.GetThumbnailData(session, new Score { Id = new Guid(scoreId) });
            return data;
        }

        [Route("api/PCV/Cases/{caseId}/Slides/{slideId}/ScanAreas/{scanAreaId}/Sessions/{sessionId}/Scores/{scoreId}/Overlays/{overlayId}")]
        [HttpGet]
        public byte[] GetOverlayData(string caseId, string slideId, string scanAreaId, string sessionId, string scoreId, string overlayId)
        {
            IProbeCaseViewDataAccess dataSource = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_casebaseFolder);

            var slide = CreateSlide(caseId, slideId);
            var session = new Session(slide, sessionId, sessionId, scanAreaId);
            var score = new Score { Id = new Guid(scoreId) };

            var data = dataSource.GetOverlayData(session, score, new Overlay { ID = overlayId });
            return data;
        }

        [Route("api/PCV/Cases/{caseId}/Slides/{slideId}/ScanAreas/{scanAreaId}/Sessions/{sessionId}/AssayData")]
        [HttpGet]
        public byte[] GetAssayData(string caseId, string slideId, string scanAreaId, string sessionId)
        {
            IProbeCaseViewDataAccess dataSource = new ProbeCaseViewDataAccessLocal.ProbeCaseViewDataAccessLocal(_casebaseFolder);

            var slide = CreateSlide(caseId, slideId);
            var session = new Session(slide, sessionId, sessionId, scanAreaId);
            var data = dataSource.GetAssayData(session);
            return data;
        }
    }
}
