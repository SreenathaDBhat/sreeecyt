﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace CytoAppsREST
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

//            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = true;
//            config.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.All;


            config.Routes.MapHttpRoute(
                                    name: "DefaultApi",
                                    routeTemplate: "api/{controller}/{id}",
                                    defaults: new { id = RouteParameter.Optional });
        }
    }
}