﻿using CytoApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CytoApps.DAL
{
    // the Data Access Layer to handle IHC WSI slides i.e. SCN files
    public interface IIHCViewDataAccess
    {
        bool Initialize(DataSource dataSource);
        bool CanAnalyseSlide(Slide slide);
        string HACKGetImagePath(Slide slide); // hacky method for demo pluggin
    }
}
