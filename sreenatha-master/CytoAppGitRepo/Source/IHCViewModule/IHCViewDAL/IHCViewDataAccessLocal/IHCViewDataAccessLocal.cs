﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CytoApps.Models;
using System.IO;

namespace CytoApps.DAL
{
    /// <summary>
    /// Provide access to WSI .SCN file in casebase folder; no database access required
    /// </summary>
    [DalTypeId(Name = "Local Casebase")]
    public class IHCViewDataAccessLocal : IIHCViewDataAccess, ILocking
    {
        private string _casebaseFolder;

        public IHCViewDataAccessLocal()
        {
        }

        public IHCViewDataAccessLocal(string casebaseFolder)
        {
            _casebaseFolder = casebaseFolder;
        }

        public bool Initialize(DataSource dataSource)
        {
            _casebaseFolder = dataSource.ConnectionParameter[1].Value;
            return true;
        }

        private string GetSlidePath(Slide slide)
        {
            return Path.Combine(_casebaseFolder, slide.CaseId, slide.Id);
        }

        /// <summary>
        /// The module can analyse any SCN file
        /// </summary>
        /// <param name="slide"></param>
        /// <returns></returns>
        public bool CanAnalyseSlide(Slide slide)
        {
            var slideDir = GetSlidePath(slide);
            return Directory.Exists(slideDir) && Directory.EnumerateFileSystemEntries(slideDir, "*.scn").Count() > 0;
        }

        public string HACKGetImagePath(Slide slide)
        {
            var imagePath = Directory.EnumerateFiles(GetSlidePath(slide), "*.scn").FirstOrDefault();
            return imagePath;
        }

        public void LockFeatureorObject(UserDetails userDetails, string caseName, object lockingObject)
        {
            throw new NotImplementedException();
        }

        public void RemoveLock(UserDetails userDetails, string caseName)
        {
            throw new NotImplementedException();
        }

        public LockResult CheckKLockExists(UserDetails userDetails, string caseName, object lockingObject)
        {
            throw new NotImplementedException();
        }
    }
}
