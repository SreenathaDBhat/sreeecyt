﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace IHCViewModule.Converters
{
    public class SizeSlideBorderConverter : IMultiValueConverter
    {
        /// <summary>
        /// Quick way of fitting slides in list to panel, basically width of contain / nItems; also handle any margin
        /// </summary>
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var count = (int)values[0]; // nItems
            var length = (double)values[1]; // Width of container
            var border = (Border)values[2]; // Border of digitalSlideView, handle Margin

            if (count > 0 && length > 0)
            {
                // take into acount any gap between objects, bit hacky
                var totalgap = count * (border.Margin.Left + border.Margin.Right + 2);
                return (length - totalgap) / count;
            }
            return 500;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
