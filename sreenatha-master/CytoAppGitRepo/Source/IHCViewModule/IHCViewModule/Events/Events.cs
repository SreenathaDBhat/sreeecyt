﻿using IHCViewModule.ViewModel;
using Prism.Events;

namespace IHCViewModule.Events
{
    /// <summary>
    /// Event to trigger the IHC main view from the navigation view
    /// </summary>
    public class OpenIHCMainViewEvent : PubSubEvent<IHCNavigationViewModel> { }

    /// <summary>
    /// Open the pseudo breast Panel
    /// </summary>
    public class OpenIHCPanelEvent : PubSubEvent<IHCNavigationViewModel> { }

    /// <summary>
    /// Event called when DigitalSlideViewer is loaded (Legacy hack)
    /// </summary>
    public class LoadedIHCMainViewEvent : PubSubEvent<object> { }

    /// <summary>
    /// Event to navigate back to navigation view from the IHC main view
    /// </summary>
    public class NavigateBackToCaseBrowserEvent : PubSubEvent<object> { }

    /// <summary>
    /// Event to Link slides
    /// </summary>
    public class IHCSlideLinkEvent : PubSubEvent<IHCMainViewModel> { }

}
