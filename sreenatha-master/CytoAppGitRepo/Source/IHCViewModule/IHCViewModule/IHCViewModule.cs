﻿using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;
using System.ComponentModel.Composition;
using IHCViewModule.Presenter;

namespace IHCViewModule
{
    [ModuleExport(typeof(IHCViewModule))]
    class IHCViewModule : IModule
    {
#pragma warning disable 0649, 0169
        [Import]
        private IHCNavigationViewPresenter _navigationPresenter;

        [Import]
        private IHCMainViewPresenter _mainPresenter;

        [Import]
        private IRegionManager _regionManager;
#pragma warning restore 0649, 0169

        public void Initialize()
        {

        }
    }
}
