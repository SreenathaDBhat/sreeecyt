﻿using Prism.Events;
using Prism.Regions;
using IHCViewModule.ViewModel;
using IHCViewModule.Views;
using IHCViewModule.Events;
using System.ComponentModel.Composition;
using CytoApps.Infrastructure.UI;
using CytoApps.DAL;
using CytoApps.Models;
using IHCViewModule.Services;
using AI;
using System.Linq;
using System;
using System.Windows.Input;

/// <summary>
/// This class acts as a main presenter for IHC module
/// Contains methods to view IHC dummy image and a button to navigate back to the CaseBrowser view
/// </summary>
namespace IHCViewModule.Presenter
{
    /// <summary>
    /// Notice its inhrits as IDigitalSlideViewInputDelegate to work with legacy SCN viewer
    /// </summary>
    [Export]
    class IHCMainViewPresenter
    {
        private IEventAggregator _eventAggregator;
        private IRegionManager _regionManager;
        private IHCMainView _ihcMainView;
        private IHCMainViewModel _ihcMainViewModel;
        private IDataService _dataService;

        [ImportingConstructor]
        public IHCMainViewPresenter(IEventAggregator eventAggregator, IRegionManager regionManager, IDataService dataService)
        {
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _dataService = dataService;

            SubscribeEvents();
        }

        private void SubscribeEvents()
        {
            _eventAggregator.GetEvent<OpenIHCMainViewEvent>().Subscribe(OpenIHCWindow);
            _eventAggregator.GetEvent<OpenIHCPanelEvent>().Subscribe(OpenIHCPanel);
            _eventAggregator.GetEvent<LoadedIHCMainViewEvent>().Subscribe(OnDigitalSlideViewLoaded);
            _eventAggregator.GetEvent<NavigateBackToCaseBrowserEvent>().Subscribe(NavigateBackToCaseBrowserView);
            _eventAggregator.GetEvent<IHCSlideLinkEvent>().Subscribe(OnSlideLink);
        }

        /// <summary>
        /// Toggle SlideLink
        /// </summary>
        /// <param name="vm"></param>
        private void OnSlideLink(IHCMainViewModel vm)
        {
            var slide = vm.VisibleSlides.FirstOrDefault();
            var linked = slide != null && slide.UserInput.IsLinked;
            foreach (var s in vm.VisibleSlides)
            {
                if (!linked)
                {
                    s.UserInput.LinkWith((from x in vm.VisibleSlides
                                          where s != x
                                          select x.UserInput).ToArray());
                }
                else
                    s.UserInput.Unlink();
            }
        }

        /// <summary>
        /// As the digital slide view is a bit hacky, we need tp patch some things once its loaded
        /// </summary>
        /// <param name="sv"></param>
        private void OnDigitalSlideViewLoaded(object sv)
        {
            DigitalSlideView slideView = sv as DigitalSlideView;
            IHCSlideModel svm = slideView.DataContext as IHCSlideModel;

            // could also get the DigitalSlideView (SlideView from the Loaded event as a commandParamater)
            AI.SlideViewViewport.ViewportDidMoveCallback updateCall = (vp) =>
            {
                slideView.SendViewportToTiler();
            };
            svm.Viewport.ViewportDidMove += updateCall;

            slideView.Unloaded += (ss, ee) =>
            {
                svm.Viewport.ViewportDidMove -= updateCall;
            };

            slideView.ZoomtoFit(false);
            svm.UserInput.AttachTo(slideView);

            svm.UserInput.Delegate = svm;
            slideView.Content = null;
        }

        /// <summary>
        /// return to casebrowser module
        /// </summary>
        /// <param name="unused"></param>
        private void NavigateBackToCaseBrowserView(object unused)
        {
            RegionManagerExtension.DeactivateViewFromRegion(RegionNames.MainRegion, typeof(IHCMainView));
            _regionManager.RequestNavigate(RegionNames.MainRegion, "CaseBrowserWorkspaceView");
        }

        /// <summary>
        /// Create a new ImageCollection that can be attached to the viewer
        /// </summary>
        /// <param name="selSlide"></param>
        /// <returns></returns>
        private IHCSlideModel CreateSlideVM(Slide selSlide)
        {
            var slide = new IHCSlideModel() { Slide = selSlide };

            var scn = new AI.SCN.ImageCollection(_dataService.IHCViewDAL.HACKGetImagePath(selSlide));

            slide.Viewport = new AI.SlideViewViewport(scn.Bounds);
            slide.UserInput = new AI.DigitalSlideViewInput();
            //_calibration = 1;
            slide.SCNImage = scn;
            slide.Viewport.RotationDegrees = 0;

            return slide;
        }

        /// <summary>
        /// Open selected Slide
        /// </summary>
        /// <param name="navVM"></param>
        private void OpenIHCWindow(IHCNavigationViewModel navVM)
        {
            _ihcMainView = new IHCMainView();
            _ihcMainViewModel = new IHCMainViewModel(_eventAggregator);

            var slide = CreateSlideVM(navVM.Slide);

            _ihcMainViewModel.VisibleSlides.Add(slide);

            _ihcMainView.DataContext = _ihcMainViewModel;
            _regionManager.AddToRegion(RegionNames.MainRegion, _ihcMainView);

            RegionManagerExtension.ActivateViewInRegion(RegionNames.MainRegion, typeof(IHCMainView));
        }

        /// <summary>
        /// Open all IHC slides as a Panel
        /// </summary>
        /// <param name="navVM"></param>
        private void OpenIHCPanel(IHCNavigationViewModel navVM)
        {
            _ihcMainView = new IHCMainView();
            _ihcMainViewModel = new IHCMainViewModel(_eventAggregator);

            foreach (var s in navVM.AllSlides)
            {
                var slide = CreateSlideVM(s);

                _ihcMainViewModel.VisibleSlides.Add(slide);
            }

            _ihcMainView.DataContext = _ihcMainViewModel;
            _regionManager.AddToRegion(RegionNames.MainRegion, _ihcMainView);

            RegionManagerExtension.ActivateViewInRegion(RegionNames.MainRegion, typeof(IHCMainView));
        }
    }
}
