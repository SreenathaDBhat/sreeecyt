﻿using Prism.Events;
using Prism.Regions;
using System.ComponentModel.Composition;
using System.Linq;
using IHCViewModule.Views;
using IHCViewModule.ViewModel;
using CytoApps.Infrastructure.UI;
using CytoApps.Infrastructure.UI.Events;
using CytoApps.Models;
using IHCViewModule.Services;
using System;
using CytoApps.Infrastructure.Helpers;
using System.Windows.Threading;

/// <summary>
/// This class acts as a presenter to IHCNavigationViewModel and IHCNavigationView.
/// Contains method to select only last slide in the navigation slide list contains IHC data enabled
/// and display the views in the correct region via the region manager
/// </summary>
namespace IHCViewModule.Presenter
{
    [Export]
    class IHCNavigationViewPresenter
    {
        private IEventAggregator _eventAggregator;
        private IRegionManager _regionManager;
        private IDataService _dataService;

        [ImportingConstructor]
        public IHCNavigationViewPresenter(IEventAggregator eventAggregator, IRegionManager regionManager, IDataService dataService)
        {
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _dataService = dataService;

            SubscribeEvents();
        }

        private void SubscribeEvents()
        {
            _eventAggregator.GetEvent<SelectedCaseSlidesEvent>().Subscribe(OnSlideLoad);
        }

        /// <summary>
        /// check each slide in the case and generate Navigator UI for any that this module can analyse
        /// Use prism regionmanager to add UI to Navigator view
        /// </summary>
        /// <param name="slideCollection"></param>
        private void OnSlideLoad(CaseDataProxy plugginInfo)
        {
            try
            {
                if (plugginInfo.Slide != null)
                {
                    _dataService.CreateDatasource(plugginInfo.DataSource);

                    if ((plugginInfo.Slide.KnownSlideTypes & (UInt32)KnownSlides.SCNSlide) != 0)
                    {
                        // This is adummy view for now
                        var ihcNavigationView = new IHCNavigationView();
                        var ihcNavigationViewModel = new IHCNavigationViewModel(_eventAggregator);
                        ihcNavigationViewModel.Slide = plugginInfo.Slide;
                        // send all sides in case that we can deal with (for Panel functionality)
                        ihcNavigationViewModel.AllSlides = plugginInfo.AllSlides.Where(a=>(a.KnownSlideTypes & (UInt32)KnownSlides.SCNSlide) != 0);
                        ihcNavigationViewModel.DataSource = plugginInfo.DataSource;

                        ihcNavigationView.DataContext = ihcNavigationViewModel;

                        try
                        {
                            var dispatcher = Dispatcher.CurrentDispatcher;

                            // Current SCN Viewer can only use filepath, so hack for now
                            var si = new AI.SCN.ImageCollection(_dataService.IHCViewDAL.HACKGetImagePath(plugginInfo.Slide));

                            si.LoadThumb().ContinueWith(t =>
                            {
                                var label = si.GetLabel();

                                dispatcher.BeginInvoke((Action)delegate
                                {
                                    ihcNavigationViewModel.OverviewImage = t.Result;
                                    //c.LabelImage = label;
                                    //c.ObjectivesUsed = si.ObjectivesUsed;
                                    //c.Barcode = si.Barcode;
                                    //c.IsFluorescent = si.HasFluorescent;
                                    //c.SetSCNVersion(si.DeviceModel, si.SCNVersion);
                                }, DispatcherPriority.Normal);

                                if (si != null)
                                    si.Release();
                            });
                        }
                        catch { }

                        //Inject naviagtion plug-in view to Slide
                        _regionManager.AddToRegion(plugginInfo.PluginRegionName, ihcNavigationView);
                    }
                }
            }
            // HACK - temp to handle that I haven't written a remote DAL at the momemt
            // issue is that if this throws an exception the Navi UI is broken for other modules
            // so probably have to handle exceptions in here
            catch (Exception ex)
            {
                LogManager.Debug("IHC: probably haven't written the DAL yet : " + ex.Message);
                return;
            }

        }
    }
}
