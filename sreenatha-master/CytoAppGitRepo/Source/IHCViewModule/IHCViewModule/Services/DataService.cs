﻿using CytoApps.DAL;
using CytoApps.Models;
using System;
using System.ComponentModel.Composition;

namespace IHCViewModule.Services
{
    /// <summary>
    /// Provides factory to Create appropriate DAL bases on a give DataSource. Can be imported where required in the analysis module
    /// to access required objects for analysis
    /// </summary>
    [Export(typeof(IDataService))]
    public class DataService : IDataService
    {
        private IIHCViewDataAccess _iHCDAL;

        // all DAL assemblies found that derive from ICVKaryoDataAccess
        private Type[] availIHCDalTypes = null;

        /// <summary>
        /// Create the appropriate instance from the set of available DAL's that implement ICVKaryoDataAccess that match the DataSource
        /// </summary>
        /// <param name="dataSource"></param>
        /// <returns></returns>
        public void CreateDatasource(DataSource dataSource)
        {
            // import karyo DALS using reflection not MEF (ExportFactory) due to lifetime concerns
            if (availIHCDalTypes == null)
                availIHCDalTypes = DalReflectionFinder.GetTypesForInterface<IIHCViewDataAccess>(@".");

            // Create a new instance every time, thread safety concerns
            _iHCDAL = DalReflectionFinder.GetMatchingInstanceOf<IIHCViewDataAccess>(availIHCDalTypes, dataSource);

            // This will pass the connection parameters required (persisted by application per data source pointed at)
            _iHCDAL.Initialize(dataSource);
        }

        public IIHCViewDataAccess IHCViewDAL
        {
            get { return _iHCDAL; }
        }
    }
}
