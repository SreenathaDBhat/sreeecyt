﻿using CytoApps.DAL;
using CytoApps.Models;

namespace IHCViewModule.Services
{
    public interface IDataService
    {
        void CreateDatasource(DataSource dataSource);
        IIHCViewDataAccess IHCViewDAL { get; }
    }
}
