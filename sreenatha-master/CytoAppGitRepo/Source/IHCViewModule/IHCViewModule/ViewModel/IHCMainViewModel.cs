﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System.ComponentModel.Composition;
using IHCViewModule.Events;
using System.Windows.Input;
using CytoApps.Models;
using System.Collections.ObjectModel;

/// <summary>
/// This class implements DelegateCommand to navigate back to CaseBrowser view from IHC main view
/// </summary>
namespace IHCViewModule.ViewModel
{
    public class IHCMainViewModel : BindableBase
    {
        private IEventAggregator _eventAggregator;

        public IHCMainViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }

        private ObservableCollection<IHCSlideModel> _visibleSlides = new ObservableCollection<IHCSlideModel>();
        public ObservableCollection<IHCSlideModel> VisibleSlides
        {
            get { return _visibleSlides; }
            set { _visibleSlides = value;  OnPropertyChanged(); }
        }

        #region Commands
        private DelegateCommand _navigateBackToCaseBrowserCommand;
        public ICommand NavigateBackToCaseBrowserCommand
        {
            get
            {
                return _navigateBackToCaseBrowserCommand ?? (_navigateBackToCaseBrowserCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<NavigateBackToCaseBrowserEvent>().Publish(null);
                }, () => true));
            }
        }

        private DelegateCommand _slideLinkCommand;
        public ICommand SlideLinkCommand
        {
            get
            {
                return _slideLinkCommand ?? (_slideLinkCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<IHCSlideLinkEvent>().Publish(this);
                }, () => true));
            }
        }

        #endregion
    }
}
