﻿using CytoApps.Infrastructure.UI.Utilities;
using CytoApps.Models;
using IHCViewModule.Events;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Linq;

/// <summary>
/// This class implements DelegateCommand to open IHC main view
/// </summary>
namespace IHCViewModule.ViewModel
{
    public class IHCNavigationViewModel : DesignTimeBindableBase
    {
        private IEventAggregator _eventAggregator;
        public IHCNavigationViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }

        public IHCNavigationViewModel()
        {
            if (base.IsInDesignMode)
                DesignTimeData();
        }

        private ImageSource _overviewImage;
        public ImageSource OverviewImage
        {
            get { return _overviewImage; }
            set { _overviewImage = value; base.OnPropertyChanged(); }
        }

        public Slide Slide { get; set; }

        public IEnumerable<Slide> AllSlides { get; set; }

        public DataSource DataSource { get; internal set; }

        public bool IsFirstSlideInMany
        {
            get
            {
                return AllSlides.Count() > 0 && AllSlides.FirstOrDefault() == Slide;
            }
        }

        #region Commands
        private DelegateCommand _iHCOpenMainViewCommand;
        public ICommand IHCOpenMainViewCommand
        {
            get
            {
                return _iHCOpenMainViewCommand ?? (_iHCOpenMainViewCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<OpenIHCMainViewEvent>().Publish(this);
                }, () => true));
            }
        }

        private DelegateCommand _iHCOpenPanelCommand;
        public ICommand IHCOpenPanelCommand
        {
            get
            {
                return _iHCOpenPanelCommand ?? (_iHCOpenPanelCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<OpenIHCPanelEvent>().Publish(this);
                }, () => true));
            }
        }

        #endregion

        private void DesignTimeData()
        {
            var uri = new Uri("pack://application:,,,/IHCViewModule;component/Images/scnlarge.png", UriKind.Absolute);
            OverviewImage = new BitmapImage(uri);
        }
    }
}
