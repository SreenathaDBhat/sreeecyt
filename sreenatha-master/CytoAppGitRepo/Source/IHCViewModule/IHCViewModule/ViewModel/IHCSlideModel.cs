﻿using AI;
using CytoApps.Models;
using IHCViewModule.Events;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

/// <summary>
/// This viewmodel encapsulates the SCN image,  NOTE: it implements AI.IDigitalSlideViewInputDelegate
/// this was a means of catching mouse down / up from the legacy viewer.
/// Also not that the VM provides a Command for the Loaded event for the DigitalSlideViewer
/// This is because the legacy viewer needs to handle certain operations after the control is instantiated and has dimensions
/// (DSV is a bit hacky)
/// </summary>
namespace IHCViewModule.ViewModel
{
    public class IHCSlideModel : BindableBase, AI.IDigitalSlideViewInputDelegate
    {
        private IEventAggregator _eventAggregator;

        public IHCSlideModel()
        {
            _eventAggregator = ServiceLocator.Current.TryResolve<IEventAggregator>();
        }

        private AI.ISlideImageCollection _scnImage = null;
        public AI.ISlideImageCollection SCNImage
        {
            get { return _scnImage; }
            set { _scnImage = value; base.OnPropertyChanged(); }
        }

        public AI.DigitalSlideViewInput UserInput { get; set; }

        private AI.SlideViewViewport _viewport;
        public AI.SlideViewViewport Viewport
        {
            get { return _viewport; }
            set { _viewport = value; base.OnPropertyChanged(); }
        }

        public Slide Slide { get; internal set; }

        private DelegateCommand<object> _loadedCommand;
        public ICommand DigitalSlideViewLoaded
        {
            get
            {
                return _loadedCommand ?? (_loadedCommand = new DelegateCommand<object>((slideview) =>
                {
                    _eventAggregator.GetEvent<LoadedIHCMainViewEvent>().Publish(slideview);
                }, (slideview) => true));
            }
        }

        public void MouseButtonPressed(object sender, MouseButtonEventArgs e)
        {
            //throw new NotImplementedException();
        }

        public void MouseButtonReleased(object sender, DSVMouseButtonEventArgs e)
        {
            //throw new NotImplementedException();
        }


    }
}
