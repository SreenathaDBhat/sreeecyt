﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.IO;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows;

namespace AI
{
    public enum AnnotationUserType
    {
        Technologist,
        Reviewer
    }

    public class Annotation : INotifyPropertyChanged
    {
        private const int defaultContentOffset = 10;

        private string content;
        private double x;
        private double y;
        private double scale = 1;
        private double contentOffsetX = defaultContentOffset;
        private double contentOffsetY = defaultContentOffset;
        private AnnotationUserType userType;
        private Guid caseId;

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        public Annotation(AnnotationUserType userType)
        {
            this.userType = userType;
        }

        public AnnotationUserType UserType
        {
            get { return userType; }
        }

        public bool IsReviewer
        {
            get { return userType == AnnotationUserType.Reviewer; }
        }

        public Guid CaseId
        {
            get { return caseId; }
            set { caseId = value; }
        }

        public string Content
        {
            get { return content; }
            set { content = value; Notify("Content"); }
        }

        public double X
        {
            get { return x; }
            set { x = value; Notify("X"); }
        }

        public double Y
        {
            get { return y; }
            set { y = value; Notify("Y"); }
        }

        public double Scale
        {
            get { return scale; }
            set { scale = value; Notify("Scale"); }
        }

        public double ContentOffsetX
        {
            get { return contentOffsetX; }
            set { contentOffsetX = value; Notify("ContentOffsetX"); Notify("ArrowLinePoints"); }
        }

        public double ContentOffsetY
        {
            get { return contentOffsetY; }
            set { contentOffsetY = value; Notify("ContentOffsetY"); Notify("ArrowLinePoints"); }
        }

        public PointCollection ArrowLinePoints
        {
            get
            {
                PointCollection points = new PointCollection();
                points.Add(new Point(0, 0));
                points.Add(new Point(contentOffsetX + 5, contentOffsetY + 5));
                return points;
            }
        }

        public static void SaveAnnotationsXml(FileInfo file, IEnumerable<Annotation> annotations)
        {
            var xml = new XElement("Annotations", annotations.ToList().ConvertAll<XElement>(a => new XElement("Annotation",
                                                                                                    new XAttribute("UserType", a.UserType.ToString()),
                                                                                                    new XAttribute("X", a.X),
                                                                                                    new XAttribute("Y", a.Y),
                                                                                                    new XAttribute("Scale", a.Scale),
                                                                                                    new XAttribute("ContentOffsetX", a.ContentOffsetX),
                                                                                                    new XAttribute("ContentOffsetY", a.ContentOffsetY),
                                                                                                    new XCData(a.Content))));

            if (file.Exists)
            {
                file.Delete();
            }

            xml.Save(file.FullName);
        }

        public static ObservableCollection<Annotation> LoadXml(FileInfo file)
        {
            if (!file.Exists)
                return new ObservableCollection<Annotation>();

            XElement xml = XElement.Load(file.FullName);

            var annotations = xml.Elements("Annotation").ToList().ConvertAll(x => AnnotationFromNode(x));
            ObservableCollection<Annotation> list = new ObservableCollection<Annotation>(annotations);
            return list;
        }

        private static Annotation AnnotationFromNode(XElement xml)
        {
            AnnotationUserType userType = (AnnotationUserType)Enum.Parse(typeof(AnnotationUserType), Parsing.ValueOrDefault(xml.Attribute("UserType"), "Technologist"));
            Annotation a = new Annotation(userType);
            a.X = Parsing.TryParseDouble(Parsing.ValueOrDefault(xml.Attribute("X"), "0"), 0.0);
            a.Y = Parsing.TryParseDouble(Parsing.ValueOrDefault(xml.Attribute("Y"), "0"), 0.0);
            a.ContentOffsetX = Parsing.TryParseDouble(Parsing.ValueOrDefault(xml.Attribute("ContentOffsetX"), "20"), defaultContentOffset);
            a.ContentOffsetY = Parsing.TryParseDouble(Parsing.ValueOrDefault(xml.Attribute("ContentOffsetY"), "20"), defaultContentOffset);
            a.Scale = Parsing.TryParseDouble(Parsing.ValueOrDefault(xml.Attribute("Scale"), "1"), 1.0);
            a.Content = xml.Value;
            return a;
        }
    }
}
