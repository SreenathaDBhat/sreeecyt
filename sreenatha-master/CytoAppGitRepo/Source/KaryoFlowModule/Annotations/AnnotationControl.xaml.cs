﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Data;
using System.Windows.Media;
using System.ComponentModel;

namespace AI
{
    public partial class AnnotationControl : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(AnnotationControl));
        public static readonly DependencyProperty AnnotationProperty = DependencyProperty.Register("Annotation", typeof(Annotation), typeof(AnnotationControl), new FrameworkPropertyMetadata(OnAnnotationChanged));
 
        private Point? drag;
        private Window rootHack;
        private Brush borderColour;

        public AnnotationControl()
        {
            borderColour = Brushes.CornflowerBlue;
            InitializeComponent();
            rootHack = VisualTreeWalker.FindParentOfType<Window>(this);
        }

        static void OnAnnotationChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            Annotation newAnnotation = (Annotation)e.NewValue;
            AnnotationControl annotationControl = (AnnotationControl)sender;

            if (newAnnotation.UserType == AnnotationUserType.Reviewer)
                annotationControl.BorderColour = new SolidColorBrush(Color.FromRgb(246, 62, 62));
        }

        public Brush BorderColour
        {
            get { return borderColour; }
            private set { borderColour = value; Notify("BorderColour"); }
        }

        public bool IsReadOnly
        {
            get { return (bool)GetValue(IsReadOnlyProperty); }
            set { SetValue(IsReadOnlyProperty, value); }
        }

        public Annotation Annotation
        {
            get { return (Annotation)GetValue(AnnotationProperty); }
            set { SetValue(AnnotationProperty, value); }
        }

        private void OnAnnotationLoadedFocusMe(object sender, RoutedEventArgs e)
        {
            TextBox text = (TextBox)sender;
            if (text.Text == "[annotation]")
            {
                Keyboard.Focus(text);
                text.SelectAll();
            }
        }

        private void OnAnnotationGotFocus(object sender, RoutedEventArgs e)
        {
            TextBox text = (TextBox)sender;
            Keyboard.Focus(text);
            text.SelectAll();
        }


        private void OnLeftMouseDown(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement s = (FrameworkElement)sender;
            s.CaptureMouse();
            drag = e.GetPosition(rootHack);
            e.Handled = true;
        }

        private void OnScaleDrag(object sender, MouseEventArgs e)
        {
            if (drag != null)
            {
                FrameworkElement s = (FrameworkElement)sender;
                Point pos = e.GetPosition(rootHack);
                double delta = pos.Y - drag.Value.Y;
                double scale = Annotation.Scale + delta * -0.03;
                scale = Math.Max(1, Math.Min(4, scale));
                Annotation.Scale = scale;
                drag = pos;
            }
        }

        private void OnScaleMouseUp(object sender, MouseButtonEventArgs e)
        {
            ((FrameworkElement)sender).ReleaseMouseCapture();
            drag = null;
        }

        private void OnPanDrag(object sender, MouseEventArgs e)
        {
            if (drag != null)
            {
                FrameworkElement s = (FrameworkElement)sender;
                Point pos = e.GetPosition(rootHack);
                double deltaY = pos.Y - drag.Value.Y;
                double deltaX = pos.X - drag.Value.X;
                Annotation.ContentOffsetX += deltaX;
                Annotation.ContentOffsetY += deltaY;
                drag = pos;
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        
        private void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }

    public class AnnotationControlControlsVisible : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool a = values[0] == DependencyProperty.UnsetValue ? false : (bool)values[0];
            bool b = values[1] == DependencyProperty.UnsetValue ? false : (bool)values[1];
            bool c = values[2] == DependencyProperty.UnsetValue ? false : (bool)values[2];
            bool readOnly = values[3] == DependencyProperty.UnsetValue ? false : (bool)values[3];

            return (a || b || c) && !readOnly ? Visibility.Visible : Visibility.Hidden;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ModifyAnnotationConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool readOnly = (bool)values[0];
            bool canDeleteExecute = false;
            if (values[1] != DependencyProperty.UnsetValue)
                canDeleteExecute  = (bool)values[1];
            
            return (readOnly || !canDeleteExecute);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
