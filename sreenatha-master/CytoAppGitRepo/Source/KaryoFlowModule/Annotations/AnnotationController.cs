﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Text;
using System.IO;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows;
using System.ComponentModel;
using AI.Connect;

namespace AI
{
    public class AnnotationPlacementHelper
    {
        private Annotation annotationBeingPlaced;
        private Point annotationBeingPlacedDropPoint;
        private AnnotationController controller;

        public AnnotationPlacementHelper(AnnotationController controller)
        {
            this.controller = controller;
        }

        private Annotation AddNewAnnotation(Point position)
        {
            Annotation annotation = new Annotation(controller.AnnotationType);
            annotation.Content = "[annotation]";
            annotation.X = position.X;
            annotation.Y = position.Y;
            annotation.Scale = 2;
            controller.AddAnnotation(annotation);
            return annotation;
        }

        public void StartPlacingAnnotation(object sender, MouseButtonEventArgs e, IInputElement relativeTo)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                e.Handled = true;

                FrameworkElement s = (FrameworkElement)sender;
                s.CaptureMouse();

                Point pos = e.GetPosition(relativeTo);
                annotationBeingPlaced = AddNewAnnotation(pos);
                annotationBeingPlacedDropPoint = e.GetPosition(relativeTo);
            }
        }

        public void MoveAnnotationBeingPlaced(object sender, MouseEventArgs e, IInputElement relativeTo)
        {
            FrameworkElement s = (FrameworkElement)sender;
            if (!s.IsMouseCaptured || annotationBeingPlaced == null)
                return;

            Point pt = e.GetPosition(relativeTo);
            if (Math.Abs(pt.X - annotationBeingPlacedDropPoint.X) > 10 || Math.Abs(pt.Y - annotationBeingPlacedDropPoint.Y) > 10)
            {
                annotationBeingPlaced.ContentOffsetX = pt.X - annotationBeingPlacedDropPoint.X;
                annotationBeingPlaced.ContentOffsetY = pt.Y - annotationBeingPlacedDropPoint.Y;
            }
        }

        public void FinishPlacingAnnotation(object sender, MouseButtonEventArgs e, IInputElement relativeTo)
        {
            FrameworkElement s = (FrameworkElement)sender;
            s.ReleaseMouseCapture();
            annotationBeingPlaced = null;
        }
    }

    public class AnnotationController : INotifyPropertyChanged
    {
        public static RoutedUICommand ToggleAnnotations = new RoutedUICommand("Toggle Annotations", "ToggleAnnotations", typeof(AnnotationController));
        public static RoutedUICommand DeleteAnnotation = new RoutedUICommand("Delete Annotation", "DeleteAnnotation", typeof(AnnotationControl));

        private ObservableCollection<Annotation> annotations;
        private Guid parentId;
        private bool show;
        private AnnotationUserType annotationType;
        private bool modified;
        
        public event PropertyChangedEventHandler PropertyChanged;

        public AnnotationController()
        {  
            annotations = new ObservableCollection<Annotation>();
            modified = false;
        }

        public void Load(Guid id)
        {
            parentId = id;

            var db = new Database();
            annotations = new ObservableCollection<Annotation>(from a in db.Annotations
                                                               where a.parentId == id
                                                               select new Annotation((AnnotationUserType)a.type)
                                                               {
                                                                   Content = a.content,
                                                                   Scale = a.scale,
                                                                   X = a.x,
                                                                   Y = a.y,
                                                                   ContentOffsetX = a.offsetX,
                                                                   ContentOffsetY = a.offsetY
                                                               });

            modified = false;

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("Annotations"));
        }

        public bool IsModified
        {
            get { return modified; }
        }

        public IEnumerable<Annotation> Annotations
        {
            get { return annotations; }
        }

        public AnnotationUserType AnnotationType
        {
            get { return annotationType; }
            set { annotationType = value; }
        }

        public bool Show
        {
            get { return show; }
            set
            {
                show = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Show"));
            }
        }

        public void Save(Guid associatedCase)
        {
            if ((parentId == null) || (associatedCase == Guid.Empty))
                return;

            Database db = new Database();
            try
            {
                var existing = db.Annotations.Where(a => a.parentId == parentId);
                db.Annotations.DeleteAllOnSubmit(existing);

                db.Annotations.InsertAllOnSubmit(from a in annotations
                                                 select new Connect.Annotation
                                                 {
                                                     annotationId = Guid.NewGuid(),
                                                     content = a.Content,
                                                     caseId = associatedCase,
                                                     parentId = parentId,
                                                     scale = a.Scale,
                                                     type = (int)a.UserType,
                                                     x = a.X,
                                                     y = a.Y,
                                                     offsetX = a.ContentOffsetX,
                                                     offsetY = a.ContentOffsetY
                                                 });

                db.SubmitChanges();
                modified = false;
            }
			finally
			{
				db.Dispose();
				db = null;
			}
        }

        public void AddAnnotation(Annotation newAnnotation)
        {
            annotations.Add(newAnnotation);
            modified = true;
            Show = true;
        }

        public void RemoveAnnotation(Annotation annotation)
        {
            annotations.Remove(annotation);
            modified = true;
        }

        public static void DeleteDataForCell(Database db, Guid parentCellId)
        {
            db.Annotations.DeleteAllOnSubmit(db.Annotations.Where(a => a.parentId == parentCellId));
        }
    }
}
