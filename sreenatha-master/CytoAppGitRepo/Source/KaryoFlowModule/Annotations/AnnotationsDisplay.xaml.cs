﻿using System;
using System.Windows.Controls;
using System.Windows;
using System.IO;
using System.Windows.Input;

namespace AI
{
    public partial class AnnotationsDisplay : UserControl
    {
        public static DependencyProperty CaseIdProperty = DependencyProperty.Register("CaseId", typeof(Guid), typeof(AnnotationsDisplay));
        public static DependencyProperty DomainWidthProperty = DependencyProperty.Register("DomainWidth", typeof(double), typeof(AnnotationsDisplay));
        public static DependencyProperty DomainHeightProperty = DependencyProperty.Register("DomainHeight", typeof(double), typeof(AnnotationsDisplay));
        public static DependencyProperty ParentIdProperty = DependencyProperty.Register("ParentId", typeof(Guid), typeof(AnnotationsDisplay), new FrameworkPropertyMetadata(OnParentChanged));
        public static DependencyProperty IsReadOnlyProperty = DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(AnnotationsDisplay));

        private AnnotationController controller;
        private AnnotationPlacementHelper placementHelper;

        public AnnotationsDisplay()
        {
            controller = new AnnotationController();
            placementHelper = new AnnotationPlacementHelper(controller);
            DataContext = this;
            InitializeComponent();
        }

        private static void OnParentChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            AnnotationsDisplay d = (AnnotationsDisplay)sender;
            d.controller.Save(d.CaseId);
            d.controller.Load((Guid)e.NewValue);
        }

        public Guid CaseId
        {
            get { return (Guid)GetValue(CaseIdProperty); }
            set { SetValue(CaseIdProperty, value); }
        }

        public AnnotationController Controller
        {
            get { return controller; }
        }

        public bool IsModified
        {
            get { return controller.IsModified; }
        }

        public AnnotationPlacementHelper PlacementHelper
        {
            get { return placementHelper; }
        }

        public double DomainWidth
        {
            get { return (double)GetValue(DomainWidthProperty); }
            set { SetValue(DomainWidthProperty, value); }
        }

        public double DomainHeight
        {
            get { return (double)GetValue(DomainHeightProperty); }
            set { SetValue(DomainHeightProperty, value); }
        }

        public Guid ParentId
        {
            get { return (Guid)GetValue(ParentIdProperty); }
            set { SetValue(ParentIdProperty, value); }
        }

        public bool IsReadOnly
        {
            get { return (bool)GetValue(IsReadOnlyProperty); }
            set { SetValue(IsReadOnlyProperty, value); }
        }

        private void CanDeleteAnnotationExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void DeleteAnnotationExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            controller.RemoveAnnotation((Annotation)e.Parameter);
        }

        private void CanToggleAnnotationsExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ToggleAnnotationsExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            controller.Show = !controller.Show;
        }
    }
}
