﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace AI
{
    public static class AppData
    {
        public static FileInfo File(Environment.SpecialFolder root, string relativePath)
        {
            string path = Path.Combine(Environment.GetFolderPath(root), relativePath);
            
            FileInfo file = new FileInfo(path);
            DirectoryHelper.MakeFolder(file.Directory.FullName);

            return file;
        }
    }
}
