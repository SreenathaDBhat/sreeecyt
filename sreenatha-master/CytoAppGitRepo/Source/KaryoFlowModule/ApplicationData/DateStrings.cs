﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace AI
{
    public static class DateStrings
    {
        public static string FriendlyDateString(DateTime when, DateTime now)
        {
            var howOld = now - when;

            if (howOld.TotalHours < 12)
            {
                int hours = (int)howOld.TotalHours;
                if (hours > 1)
                    return ((int)howOld.TotalHours) + " hours ago";
                else
                    return Math.Max(1, (int)howOld.TotalMinutes) + " minutes ago";
            }
            else if (now.Year == when.Year && now.DayOfYear == when.DayOfYear)
                return "Today, " + when.ToShortTimeString();
            else if (now.Year == when.Year && now.DayOfYear == when.DayOfYear + 1)
                return "Yesterday, " + when.ToShortTimeString();
            else
                return when.ToLongDateString();
        }
    }

    public class FriendlyDateStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return DateStrings.FriendlyDateString((DateTime)value, DateTime.Now);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
