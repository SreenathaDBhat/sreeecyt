﻿using System;
using System.IO;

namespace AI
{
    public static class DirectoryHelper
    {
        public static DirectoryInfo MakeFolder(string folderPath)
        {
            DirectoryInfo folder = new DirectoryInfo(folderPath);

            CheckExists(folder);

            return folder;
        }

        private static void CheckExists(DirectoryInfo folder)
        {
            if (!folder.Exists)
            {
                CheckExists(folder.Parent);
                folder.Create();
            }
        }

        public static string Combine(params object[] args)
        {
            if(args == null)
                return null;
            
            if(args.Length == 1)
                return args[0].ToString();

            string s = args[0].ToString();
            for (int i = 1; i < args.Length; ++i)
            {
                s = Path.Combine(s, args[i].ToString());
            }

            return s;
        }
    }
}
