﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI
{
	public class ExceptionUtility
	{
		// What should we handle, DB exceptions include
		//	System.Data.SqlClient.SqlException
		//	System.Data.Linq.DuplicateKeyException
		//	System.Data.Linq.ChangeConflictException
		// whose common base class is System.Exception.

		public static void ShowLog( Exception ex, string caption)
		{
			MessageBox.Show(ex.Message + "\n\n" + ex.StackTrace, caption);

			// TODO: Write exception to a file

			// TODO: do different stuff for 
			//	System.Data.SqlClient.SqlException
			//	System.Data.Linq.DuplicateKeyException
			//	System.Data.Linq.ChangeConflictException
		}

	}
}
