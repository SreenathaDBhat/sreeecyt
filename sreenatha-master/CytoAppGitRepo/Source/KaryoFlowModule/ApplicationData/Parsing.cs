﻿using System;
using System.Windows.Media;
using System.Xml.Linq;
using System.Windows.Input;

namespace AI
{
    public static class Parsing
    {
        public static int TryParseInt(string str, int fallback)
        {
            int converted = 0;
            if (int.TryParse(str, out converted))
                return converted;
            else
                return fallback;
        }

        public static double TryParseDouble(string str, double fallback)
        {
            double converted = 0;
            if (double.TryParse(str, out converted))
                return converted;
            else
                return fallback;
        }

        public static float TryParseFloat(string str, float fallback)
        {
            float converted = 0;
            if (float.TryParse(str, out converted))
                return converted;
            else
                return fallback;
        }


        public static bool TryParseBool(string str, bool fallback)
        {
            bool converted = false;
            if (bool.TryParse(str, out converted))
                return converted;
            else
                return fallback;
        }

        public static Color TryParseColor(string str, Color fallback)
        {
            Color converted = Colors.Red;
            if (ColorParse.TryParse(str, out converted))
                return converted;
            else
                return fallback;
        }

        public static string ValueOrDefault(XAttribute attribute, string def)
        {
            if (attribute == null)
                return def;
            else
                return attribute.Value;
        }

        public static string ValueOrDefault(XElement xElement, string def)
        {
            if (xElement == null)
                return def;
            else
                return xElement.Value;
        }

        public static Key TryParseKey(string str, System.Windows.Input.Key fallback)
        {
            try
            {
                return (Key)Enum.Parse(typeof(Key), str);
            }
            catch (Exception)
            {
                return fallback;
            }
        }
    }
}
