﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.IO;
using System.Xml.Linq;

namespace AI
{
    public static class SimpleFileDialog
    {
        public static string Save(Guid identifier, string title, string filter)
        {
            SaveFileDialog fd = new SaveFileDialog();
            fd.AddExtension = true;
            fd.RestoreDirectory = true;
            fd.Title = title;
            fd.Filter = filter;
            fd.RestoreDirectory = true;
            UseRememberedStuff(identifier, fd);
            if (fd.ShowDialog().GetValueOrDefault(false))
            {
                RememberStuff(identifier, fd);
                return fd.FileName;
            }
            else
            {
                return null;
            }
        }

        private static void RememberStuff(Guid identifier, SaveFileDialog fd)
        {
            FileInfo file = AppData.File(Environment.SpecialFolder.LocalApplicationData, "genetix\\filedialog_" + identifier.ToString() + ".settings");
            if (file.Exists)
            {
                file.Delete();
            }

            FileInfo destFile = new FileInfo(fd.FileName);
            XElement xml = new XElement("FileDialog", new XAttribute("FilterIndex", fd.FilterIndex), new XAttribute("Path", destFile.Directory.FullName));
            xml.Save(file.FullName);
        }

        private static void UseRememberedStuff(Guid identifier, SaveFileDialog fd)
        {
            FileInfo file = AppData.File(Environment.SpecialFolder.LocalApplicationData, "genetix\\filedialog_" + identifier.ToString() + ".settings");
            if (file.Exists)
            {
                try
                {
                    XElement xml = XElement.Load(file.FullName);
                    fd.FilterIndex = int.Parse(xml.Attribute("FilterIndex").Value);
                    fd.InitialDirectory = xml.Attribute("Path").Value;
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
