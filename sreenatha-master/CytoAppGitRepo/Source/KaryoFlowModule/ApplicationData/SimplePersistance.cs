﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;

namespace AI
{
    public sealed class SimplePersistance
    {
        private static readonly string fileName = "Genetix\\general.cfg";

        private Dictionary<string, string> items;

        public SimplePersistance()
        {
            items = new Dictionary<string, string>();

            var file = AppData.File(Environment.SpecialFolder.LocalApplicationData, fileName);
            if (file.Exists)
            {
                XElement xml = XElement.Load(file.FullName);

                var xmlItems = from n in xml.Elements("Item")
                               select new
                               {
                                   Key = n.Attribute("Key").Value,
                                   Value = n.Attribute("Value").Value
                               };

                foreach (var i in xmlItems)
                {
                    items.Add(i.Key, i.Value);
                }
            }
        }

        public void Persist()
        {
            XElement xml = new XElement("Items", from i in items.Keys
                                                 select new XElement("Item",
                                                    new XAttribute("Key", i),
                                                    new XAttribute("Value", items[i])));

            var file = AppData.File(Environment.SpecialFolder.LocalApplicationData, fileName);
            if(file.Exists)
                file.Delete();

            xml.Save(file.FullName);
        }

        public void Set(string key, string newValue)
        {
            if (items.ContainsKey(key))
            {
                items[key] = newValue;
            }
            else
            {
                items.Add(key, newValue);
            }
        }

        public void Set(string key, bool newValue)
        {
            Set(key, newValue.ToString());
        }

        public void Set(string key, double newValue)
        {
            Set(key, newValue.ToString());
        }

        private bool Get(string key, out string val)
        {
            if (!items.ContainsKey(key))
            {
                val = string.Empty;
                return false;
            }

            val = items[key];
            return true;
        }

        public string Get(string key, string fallback)
        {
            string v;
            if (Get(key, out v))
                return v;
            else
                return fallback;
        }

        public double GetDouble(string key, double fallback)
        {
            string v;
            if (Get(key, out v))
            {
                double d;
                if (double.TryParse(v, out d))
                    return d;
                else
                    return fallback;
            }
            else
            {
                return fallback;
            }
        }

        public bool GetBool(string key, bool fallback)
        {
            string v;
            if (Get(key, out v))
            {
                bool d;
                if (bool.TryParse(v, out d))
                    return d;
                else
                    return fallback;
            }
            else
            {
                return fallback;
            }
        }
    }
}
