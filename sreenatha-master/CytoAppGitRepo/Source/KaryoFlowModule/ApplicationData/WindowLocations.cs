﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace AI
{
    public class WindowSettings
    {
        private Point location;
        private Size size;
        private bool maximised;
        private string key;

        public WindowSettings()
        {
        }

        public WindowSettings(Window window, string key)
        {
            this.key = key;
            maximised = window.WindowState == WindowState.Maximized;
            location = new Point(window.Left, window.Top);
            size = new Size(window.ActualWidth, window.ActualHeight);
        }

        public string WindowTypeName
        {
            get { return key; }
            set { key = value; }
        }

        public bool Maximised
        {
            get { return maximised; }
            set { maximised = value; }
        }

        public double Left
        {
            get { return location.X; }
            set { location = new Point(value, location.Y); }
        }

        public double Top
        {
            get { return location.Y; }
            set { location = new Point(location.X, value); }
        }

        public double Width
        {
            get { return size.Width; }
            set { size = new Size(value, size.Height); }
        }

        public double Height
        {
            get { return size.Height; }
            set { size = new Size(size.Width, value); }
        }
    }


    public sealed class WindowLocations
    {
        private static List<WindowSettings> windowSettings = new List<WindowSettings>();

        private WindowLocations()
        {
        }

        static WindowLocations()
        {
            FileInfo file = AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\window.locations");
            if (!file.Exists)
                return;

            try
            {
                using (StreamReader reader = new StreamReader(file.OpenRead()))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<WindowSettings>));
                    windowSettings = (List<WindowSettings>)serializer.Deserialize(reader);
                    reader.Close();
                }
            }
            catch(InvalidOperationException)
            {

            }
        }

        public static void Handle(Window window)
        {
            ApplyTo(window);
            window.Closing += (s, e) => Store(window);
        }

        private static void Store(Window window)
        {
            if (window.WindowState == WindowState.Minimized)
                return;

            string key = window.GetType().Name;
            Type windowType = window.GetType();

            for (int i = windowSettings.Count - 1; i >= 0; --i)
            {
                if (windowSettings[i].WindowTypeName == key)
                {
                    windowSettings.RemoveAt(i);
                }
            }
            
            windowSettings.Add(new WindowSettings(window, key));

            FileInfo file = AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\window.locations");

            if (file.Exists)
            {
                while (true)
                {
                    try
                    {
                        file.Delete();
                        break;
                    }
                    catch (IOException ex)
                    {
                        ex.ToString();
                    }
                }
            }

            using (StreamWriter writer = new StreamWriter(file.OpenWrite()))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<WindowSettings>));
                serializer.Serialize(writer, windowSettings);
                writer.Flush();
                writer.Close();
            }
        }

        private static void ApplyTo(Window window)
        {
            //string key = window.GetType().Name;
            //var s = windowSettings.Where(ws => ws.WindowTypeName == key).FirstOrDefault();
            //if (s == null)
            //    return;

            //window.Left = s.Left;
            //window.Top = s.Top;
            //window.Width = s.Width;
            //window.Height = s.Height;

            //if (s.Maximised)
            //{
            //    window.WindowState = WindowState.Maximized;
            //}
            //else
            //{
            //    window.WindowState = WindowState.Normal;
            //}

            window.WindowState = WindowState.Maximized;
        }
    }
}
