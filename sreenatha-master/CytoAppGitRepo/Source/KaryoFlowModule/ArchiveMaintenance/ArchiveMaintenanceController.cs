﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using AI.Connect;

namespace AI
{
    class ArchiveMaintenanceController : Notifier
    {
        private ObservableCollection<Device> devices = new ObservableCollection<Device>();
        private ObservableCollection<String> statuses = new ObservableCollection<String>();
        private ObservableCollection<String> existingDeviceNames = new ObservableCollection<String>();

        private LocalArchiveSettings settings = null;

        public ArchiveMaintenanceController()
        {
            GetDevices();
            GetStatuses();
            settings = DataTransfer.GetArchiveSettings();
        }

        public Boolean PruneData
        {
            get { return settings.PruneData; }
            set
            {
                if (value != settings.PruneData)
                {
                    settings.PruneData = value;
                    Notify("PruneData");
                }
            }
        }

        public Boolean DeleteCase
        {
            get { return settings.DeleteCase;}
            set
            {
                if (value != settings.DeleteCase)
                {
                    settings.DeleteCase = value;
                    Notify("DeleteCase");
                }
            }
        }

        public int TimerInterval
        {
            get { return settings.TimerInterval; }
            set
            {
                if (value != settings.TimerInterval)
                {
                    settings.TimerInterval = value;
                    Notify("TimerInterval");
                }
            }
        }

        public string PreArchiveStatus
        {
            get { return settings.PreArchiveStatus; }
            set
            {
                if (value != settings.PreArchiveStatus)
                {
                    settings.PreArchiveStatus = value;
                    Notify("PreArchiveStatus");
                }
            }
        }

        public string PostArchiveStatus
        {
            get { return settings.PostArchiveStatus; }
            set
            {
                if (value != settings.PostArchiveStatus)
                {
                    settings.PostArchiveStatus = value;
                    Notify("PostArchiveStatus");
                }
            }
        }

        public string ArchiveDevice
        {
            get { return settings.ArchiveDevice; }
            set
            {
                if (value != settings.ArchiveDevice)
                {
                    settings.ArchiveDevice = value;
                    Notify("ArchiveDevice");
                }
            }
        }

        public IEnumerable<Device> Devices
        {
            get { return devices; }
        }

        public IEnumerable<String> ExistingDevices
        {
            get { return existingDeviceNames; }
        }

        public void AddDevice(string path, string label)
        {
            var rec = devices.Where( f => f.Path == path).FirstOrDefault();

            if (rec == null)
            {
                devices.Add( new Device(path,label,false));
                Notify("Devices");
            }
        }

        public void DeleteDevice(string path)
        {
            var rec = devices.Where(f => f.Path == path).FirstOrDefault();

            if (rec != null)
            {
                devices.Remove(rec);
                Notify("Devices");
            }
        }
        
        public IEnumerable<String> Statuses
        {
            get { return statuses; }
        }

        public void Save()
        {
            DataTransfer.SaveArchiveSettings(settings);
            SaveDevices();
        }
        
        private void GetDevices()
        {
            try
            {
                devices.Clear();
                existingDeviceNames.Clear();

                using (Database db = new Database())
                {

                    var existing = db.GetTable<ArchiveDevice>();

                    if (existing.Count() > 0)
                    {
                        foreach (var e in existing)
                        {
                            devices.Add(new Device(e.id, e.path, e.mediaLabel, ((Boolean)e.available)));
                            existingDeviceNames.Add(e.path);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            finally
            {
                base.Notify("Devices");
                base.Notify("ExistingDevices");
            }
        }

        private void GetStatuses()
        {

            try
            {
                using (Database db = new Database())
                {
                    var status = db.GetTable<Status>();

                    if (status.Count() > 0)
                    {
                        foreach (var e in status)
                        {
                            statuses.Add(e.StatusCode);
                        }
                    }
                }
            }
            catch
            {
                statuses.Clear();
                statuses = null;
                statuses = new ObservableCollection<String>();
                base.Notify("Statuses");
            }
        }

        private void SaveDevices()
        {

            Database db = null;

            try
            {
                db = new Database();

                // Delete existing records that are not
                // in the collection
                var existing = db.GetTable<ArchiveDevice>();
                if (existing.Count() > 0)
                {
                    foreach (var e in existing)
                    {
                        var rec = devices.Where(d => d.ID == e.id).FirstOrDefault();

                        if (rec == null)
                        {
                            db.ArchiveDevices.DeleteOnSubmit(e);
                        }
                    }
                }

                //  Iterate through collection, updating existing records
                //  and adding new ones
                if (devices.Count > 0)
                {
                    foreach (var d in devices)
                    {
                        if (d.ID == -1)  // new record
                        {
                            AI.Connect.ArchiveDevice ad = new AI.Connect.ArchiveDevice();
                            ad.mediaLabel = d.MediaLabel;
                            ad.path = d.Path;
                            ad.available = d.Available;
                            db.ArchiveDevices.InsertOnSubmit(ad);
                        }
                        else
                        {   // Update existing record
                            var rec = db.ArchiveDevices.Where(f => f.id == d.ID).First();
                            rec.mediaLabel = d.MediaLabel;
                            rec.path = d.Path;
                            rec.available = d.Available;
                        }
                    }
                }

                db.SubmitChanges();
                db.Dispose();
                db = null;
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }
    }
}
