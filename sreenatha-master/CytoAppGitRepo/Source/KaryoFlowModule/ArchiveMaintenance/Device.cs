﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI
{
    public class Device : Notifier
    {
        private int id = -1;
        private string path = string.Empty;
        private string mediaLabel = string.Empty;
        private Boolean available = false;

        internal int ID
        { get { return this.id; } }

        public string Path
        {
            get { return path; }

            set
            {
                if (value != path)
                {
                    path = value;
                    base.Notify("Path");
                }
            }
        }

        public string MediaLabel
        {
            get { return mediaLabel; }

            set
            {
                if (value != mediaLabel)
                {
                    mediaLabel = value;
                    base.Notify("MediaLabel");
                }
            }
        }

        public Boolean Available
        {
            get { return available; }

            set
            {
                if (value != available)
                {
                    available = value;
                    base.Notify("Available");
                }
            }
        }


        internal Device(int id, string path, string label, Boolean online)
        {
            this.Path = path;
            this.MediaLabel = label;
            this.Available = online;
            this.id = id;
        }

        public Device(string path, string label)
        {
            this.Path = path;
            this.MediaLabel = label;
            this.Available = false;
            this.id = -1;
        }

        public Device(string path, string label, Boolean online)
        {
            this.Path = path;
            this.MediaLabel = label;
            this.Available = online;
            this.id = -1;
        }

    
    }
}
