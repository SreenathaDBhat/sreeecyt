﻿using System;
using System.Windows;
using AI.Connect;
using System.ComponentModel;
using System.Windows.Input;

namespace AI
{
    public partial class NetworkConnect : Window
    {
        public NetworkConnect()
        {
            InitializeComponent();
            DataContext = new NetworkInfo();
        }

        public void OnDragWindow(object sender, MouseEventArgs e)
        {
            DragMove();
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            NetworkInfo info = (NetworkInfo)DataContext;

            Locations.BlobPool = info.BlobPool;
            if (info.WindowsAuthentication)
            {
                Locations.ConnectionString = string.Format("Data Source={0}{1}{2};Integrated Security=True;Initial Catalog=Tubs",
                    info.ServerName,
                    info.InstanceName == string.Empty ? "" : "\\",
                    info.InstanceName == string.Empty ? "" : info.InstanceName);
            }
            else
            {
                Locations.ConnectionString = string.Format("Data Source={0}{1}{2};User ID={3};Password={4};Initial Catalog=Tubs",
                    info.ServerName,
                    info.InstanceName == string.Empty ? "" : "\\",
                    info.InstanceName == string.Empty ? "" : info.InstanceName,
                    info.UserName,
                    info.Password);
            }

            DialogResult = true;
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }

    internal class NetworkInfo : INotifyPropertyChanged
    {
        private string blobPool;
        private string server;
        private string instance;
        private string user;
        private string password;
        private bool windowsAuth;

        public NetworkInfo()
        {
            blobPool = Locations.BlobPool;
            server = Locations.SqlServerName;
            instance = Locations.SqlInstanceName;
            user = Locations.SqlUsername;
            password = Locations.SqlPassword;
            windowsAuth = user == string.Empty;
        }

        public string BlobPool
        {
            get { return blobPool; }
            set { blobPool = value; Notify("BlobPool"); }
        }

        public string ServerName
        {
            get { return server; }
            set { server = value; Notify("ServerName"); }
        }

        public string InstanceName
        {
            get { return instance; }
            set { instance = value; Notify("InstanceName"); }
        }

        public string UserName
        {
            get { return user; }
            set { user = value; Notify("UserName"); }
        }

        public string Password
        {
            get { return password; }
            set { password = value; Notify("Password"); }
        }

        public bool WindowsAuthentication
        {
            get { return windowsAuth; }
            set { windowsAuth = value; Notify("WindowsAuthentication"); }
        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        void Notify(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion
    }
}
