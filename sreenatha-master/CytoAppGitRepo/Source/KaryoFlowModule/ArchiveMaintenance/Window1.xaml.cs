﻿using System;
using System.Management;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.IO;
using AI.Security;
using AI.Logging;
using AI;
using System.Threading;
using System.Windows.Threading;

namespace AI.ArchiveMaintenance
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private ArchiveMaintenanceController amController = null;
        private string error = string.Empty;
        private System.Timers.Timer timer = null;
        private Guid sessionId = Guid.Empty;
        private Logging.Logging logger = null;

        public Window1()
        {
            InitializeComponent();
            Connect.LIMS.SetType(typeof(LimsFallback.LimsBase));

            Loaded += new RoutedEventHandler(Window1_Loaded);
        }

        void Window1_Loaded(object sender, RoutedEventArgs e)
        {

            ThreadPool.QueueUserWorkItem(delegate
            {
                while (!TestConnection())
                {
                    Dispatcher.Invoke((ThreadStart)delegate
                    {
                        NetworkConnect connectScreen = new NetworkConnect();
                        connectScreen.Owner = this;
                        bool result = connectScreen.ShowDialog().GetValueOrDefault(false);

                        if (!result)
                        {
                            DialogResult = false;
                            Close();
                        }
                        else
                        {
                            Connect.Locations.Save();
                        }

                    }, DispatcherPriority.Normal);
                }

                Security.Security.Login();
                sessionId = Security.Security.SessionGuid;

                logger = new AI.Logging.Logging();

                amController = new ArchiveMaintenanceController();
                AI.Device useDevice = amController.Devices.Where(f => f.Path == amController.ArchiveDevice).FirstOrDefault();
                CaseFinder.PropertyChanged += CaseFinderPropertyChanged;

                Dispatcher.Invoke((ThreadStart)delegate
                {
                    this.DataContext = amController;

                    preStatusList.SelectedItem = amController.PreArchiveStatus;
                    postStatusList.SelectedItem = amController.PostArchiveStatus;

                    selectedDevice.SelectedItem = useDevice;
                    archiveFolder.Text = amController.ArchiveDevice;
                    // CaseFinder.Status = amController.PreArchiveStatus;
                    CaseFinder.Refresh();
                    caseList.SelectedIndex = 0;
                });
            });
        }

        private static bool TestConnection()
        {
            return (Connect.Locations.ConfigFile.Exists && AI.Connect.NetworkTest.TestSql());
        }


        void CaseFinderPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Results")
            {
                caseList.SelectedIndex = 0;
            }
        }

        private CaseFinder CaseFinder
        {
            get { return (CaseFinder)FindResource("caseFinder"); }
        }


        private void RefreshCaseList()
        {
            Dispatcher.Invoke((ThreadStart)delegate
            {
                //CaseFinder.Status = amController.PreArchiveStatus;
                CaseFinder.Refresh();
                caseList.SelectedIndex = 0;
            });
        }
        
        private void GetFreeDiskSpace(string selectedPath)
        {
            Boolean mappedDrive = false;
            string driveName = string.Empty;

            DirectoryInfo dir = new DirectoryInfo(selectedPath);

            if (dir.Root.Name.Substring(1, 1) == ":")
            {
                mappedDrive = true;
                driveName = dir.Root.Name.Substring(0, 2);
            }
            else
            {
                //  the path should start with "\\\\"
                //  identifying machine name
                if (selectedPath.StartsWith("\\\\"))
                {
                    driveName = selectedPath.Substring(2, selectedPath.Length - 4);
                    if (driveName.Contains("\\"))
                    {
                        driveName = driveName.Substring(0, driveName.IndexOf("\\"));
                    }
                }
            }

            if (driveName != string.Empty)
            {
                if (mappedDrive)
                {
                    ManagementScope ms = new ManagementScope();
                    ObjectQuery oq = new ObjectQuery("SELECT * FROM Win32_LogicalDisk");
                    ManagementObjectSearcher mos = new ManagementObjectSearcher(ms, oq);
                    ManagementObjectCollection moc = mos.Get();

                    foreach (ManagementObject mo in moc)
                    {
                        string volumeName = mo["Name"].ToString();

                        if (string.Compare(volumeName, driveName, true) == 0)
                        {
                            ulong size = (ulong)mo["FreeSpace"];
                            driveSpace.Content = (size / (1024 * 1024 * 1024)).ToString();
                            break;
                        }
                    }
                }
                else
                {
                    ConnectionOptions oConn = new ConnectionOptions();

                    //oConn.Username = Environment.UserName;
                    //oConn.Password = "Admin54xx";
                    string strNameSpace = @"\\" + driveName + @"\root\cimv2"; ;

                    ManagementScope oMs = new ManagementScope(strNameSpace, oConn);

                    //get Fixed disk state

                    ObjectQuery oQuery = new ObjectQuery("select FreeSpace,Size,Name from Win32_LogicalDisk where DriveType >= 1");

                    //Execute the query
                    ManagementObjectSearcher oSearcher = new ManagementObjectSearcher(oMs, oQuery);

                    //Get the results
                    ManagementObjectCollection oReturnCollection = oSearcher.Get();

                    foreach (ManagementObject oReturn in oReturnCollection)
                    {
                        try
                        {
                            string name = oReturn["Name"].ToString();
                            ulong size = (ulong)oReturn["FreeSpace"];
                            driveSpace.Content = (size / (1024 * 1024 * 1024)).ToString();
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }

        private void browse_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
            dlg.SelectedPath = archiveFolder.Text;

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (ValidArchiveFolder(dlg.SelectedPath))
                    archiveFolder.Text = dlg.SelectedPath;
                else
                    archiveFolder.Text = "INVALID";
            }            
        }

        private void deleteDevice_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;

            if (button.Tag != null)
            {
                string deletePath = ((AI.Device)button.Tag).Path;
                amController.DeleteDevice(deletePath);
            }
        }

        private void addDevice_Click(object sender, RoutedEventArgs e)
        {
            //   Validate the folder
            if (ValidArchiveFolder(archiveFolder.Text))
            {
                amController.AddDevice(archiveFolder.Text, mediaLabel.Text);
            }
            else
            {


            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            amController.Save();
        }

        private void selectedDevice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;

            if (cb != null)
            {
                AI.Device selectedItem = (AI.Device)cb.SelectedItem;
                if (selectedItem != null)
                {
                    string path = selectedItem.Path;
                    amController.ArchiveDevice = path;
                }
            }
        }

        private void preStatusList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            amController.PreArchiveStatus = ((ComboBox)sender).SelectedItem.ToString();
            RefreshCaseList();
        }

        private void postStatusList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            amController.PostArchiveStatus = ((ComboBox)sender).SelectedItem.ToString();
        }

        private void btnArchive_Click(object sender, RoutedEventArgs e)
        {
            Archive();
        }

        private void DisableButtons(Boolean byTimer)
        {
            btnArchive.IsEnabled = false;
            btnStart.IsEnabled = false;
            btnFinish.IsEnabled = byTimer;
        }

        private void EnableButtons()
        {
            Dispatcher.Invoke((ThreadStart)delegate
            {
                btnArchive.IsEnabled = true;
                btnStart.IsEnabled = true;
                btnFinish.IsEnabled = true;
            });
        }

        private void Archive()
        {
            const string EXTENSION = ".Archive";
            error = string.Empty;

            try
            {
                DisableButtons(false);
                // Validate archive location
                string folder = archiveFolder.Text;
                string label = string.Empty;

                if (ValidArchiveFolder(folder, ref label))
                {
                    using (var db = new Connect.Database())
                    {
                        // Which case is selected
                        string caseName = ((Case)caseList.SelectedItem).Name;
                        Guid id = db.CaseIdFor((Case)caseList.SelectedItem);
                        string fileName = folder + "\\" + caseName + EXTENSION;
                        Boolean prune = (Boolean)pruneData.IsChecked;
                        Boolean delete = (Boolean)deleteData.IsChecked;
                        string newStatus = amController.PostArchiveStatus;

                        DirectoryInfo dir = new DirectoryInfo(fileName);

                        DatatransferController controller = (DatatransferController)this.FindResource("xferController");
                        controller.ShowProgressBar = true;

                        ThreadPool.QueueUserWorkItem(delegate
                        {
                            Archiver archiver = new Archiver(controller, folder);
                            archiver.Archive(id, true, "80085");

                            if (Security.Security.IsLoggingEnabled)
                            {
                                if (archiver.LastErrorMessage.Length > 0)
                                    logger.AddLogEntry(Environment.UserName, Environment.MachineName, sessionId, EventType.ArchivedError, id, string.Empty, "Case " + caseName + " error." + archiver.LastErrorMessage);
                                else
                                    logger.AddLogEntry(Environment.UserName, Environment.MachineName, sessionId, EventType.ArchivedCase, id, newStatus, "Case " + caseName + " archived to " + folder + "; Media label = " + label);
                            }

                            archiver.Dispose();
                            archiver = null;
                            controller.ShowProgressBar = false;
                            controller.TotalSteps = 100;
                            controller.CurrentStep = 0;

                            Dispatcher.Invoke((ThreadStart)delegate
                            {
                                EnableButtons();
                                RefreshCaseList();
                            });

                        }, DispatcherPriority.Normal);
                    }
                }
                else
                {
                    Dispatcher.Invoke((ThreadStart)delegate
                    {
                        EnableButtons();
                        RefreshCaseList();
                    });
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                Dispatcher.Invoke((ThreadStart)delegate
                {
                    EnableButtons();
                    RefreshCaseList();
                });
            }
        }

        private Boolean ValidArchiveFolder(string folder)
        {

            Boolean OK = false;

            if ((folder != null) && (folder.Length > 0))
            {

                string tempFolder = folder + "\\" + Guid.NewGuid();
                DirectoryInfo di = new DirectoryInfo(tempFolder);

                try
                {
                    //  Check we have write permissions
                    di.Create();

                    //  Determine disk capacity
                    //GetFreeDiskSpace(tempFolder);
                    OK = true;

                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    OK = false;
                }
                finally
                {
                    if (di.Exists)
                        di.Delete();
                }
            }

            return OK;
        
        }
        
        private Boolean ValidArchiveFolder(string folder, ref string label)
        {
            Boolean OK = false;

            if ((folder != null) && (folder.Length > 0))
            {
                //  Is the folder listed in the devices
                using (var db = new Connect.Database())
                {
                    var location = db.ArchiveDevices.Where(f => f.path == folder).FirstOrDefault();

                    if (location != null)
                    {
                        if (location.mediaLabel.Length > 0)
                        {
                            label = location.mediaLabel;

                            OK = (Boolean)location.available;

                            if (!OK) 
                                error = "Archive device offline.";
                        }
                        else
                            error = "Invalid media label.";
                    }
                    else
                    {
                        error = "Invalid folder.";
                    }
                }

                if (OK)
                {
                    OK = ValidArchiveFolder(folder);
                }

            }
            return OK;
        }


        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            DisableButtons(true);

            if (timer == null)
            {
                timer = new System.Timers.Timer();
                timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
            }

            const int oneHourOfMilliSeconds = 3600000;
            int hours = int.Parse(scanIntervalHours.Text);
            timer.Interval = hours * oneHourOfMilliSeconds;

            if (Security.Security.IsLoggingEnabled)
            {
                logger.AddLogEntry(Environment.UserName, Environment.MachineName, sessionId, EventType.AutoArchiveStarted, Guid.Empty, string.Empty, string.Empty);
            }

            timer.Enabled = true;
        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer.Enabled = false;

            if (Security.Security.IsLoggingEnabled)
            {
                logger.AddLogEntry(Environment.UserName, Environment.MachineName, sessionId, EventType.AutoArchiveStarted, Guid.Empty, string.Empty, string.Empty);
            }

            Dispatcher.Invoke((ThreadStart)delegate
            {
                //CaseFinder.Status = amController.PreArchiveStatus;
                CaseFinder.Refresh();
                caseList.SelectedIndex = 1;
            });

            if (caseList.Items.Count > 0)
            {
                const string EXTENSION = ".Archive";
                error = string.Empty;

                for (int x = 0; x < caseList.Items.Count; x++)
                {
                    string caseName = string.Empty;
                    Guid id = Guid.Empty;
                    string folder = string.Empty;
                    Boolean prune = false;
                    Boolean delete = false;
                    string label = string.Empty;

                    Dispatcher.Invoke((ThreadStart)delegate
                    {
                        using (var db = new Connect.Database())
                        {
                            caseList.SelectedIndex = x;
                            caseName = ((AI.Case)caseList.SelectedItem).Name;
                            id = db.CaseIdFor(((AI.Case)caseList.SelectedItem));
                            folder = archiveFolder.Text;
                            prune = (Boolean)pruneData.IsChecked;
                            delete = (Boolean)deleteData.IsChecked;
                        }
                    });

                    try
                    {
                        if (ValidArchiveFolder(folder,ref  label))
                        {
                            // Which case is selected
                            string fileName = folder + "\\" + caseName + EXTENSION;
                            string newStatus = amController.PostArchiveStatus;

                            DirectoryInfo dir = new DirectoryInfo(fileName);

                            DatatransferController controller = (DatatransferController)this.FindResource("xferController");
                            controller.ShowProgressBar = true;
                            controller.ProcessComplete = false;

                            ThreadPool.QueueUserWorkItem(delegate
                            {
                                //Archiver archiver = new Archiver(controller, folder);
                                //archiver.Archive(dir.Name, id, prune, delete, newStatus, label);

                                //if (Security.Security.IsLoggingEnabled)
                                //{

                                //    if (archiver.LastErrorMessage.Length > 0)
                                //        logger.AddLogEntry(Environment.UserName, Environment.MachineName, sessionId, EventType.ArchivedError, id, string.Empty, "Case " + caseName + " error." + archiver.LastErrorMessage);
                                //    else
                                //        logger.AddLogEntry(Environment.UserName, Environment.MachineName, sessionId, EventType.ArchivedCase, id, newStatus, "Case " + caseName + " archived to " + folder + "; Media label = " + label);
                                //}
                                
                                //archiver.Dispose();
                                //archiver = null;
                                //controller.ShowProgressBar = false;
                                //controller.TotalSteps = 100;
                                //controller.CurrentStep = 0;
                                throw new NotImplementedException();

                            }, DispatcherPriority.Normal);

                            while (!controller.ProcessComplete)
                                Thread.Sleep(100);

                            if(controller.Error.Length > 0)
                            {

                                if (Security.Security.IsLoggingEnabled)
                                {
                                    logger.AddLogEntry(Environment.UserName, Environment.MachineName, sessionId, EventType.ArchivedError, id, string.Empty, controller.Error);
                                }
                                
                                break;
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        Dispatcher.Invoke((ThreadStart)delegate
                        {
                            btnArchive.IsEnabled = true;
                            btnStart.IsEnabled = true;
                        });
                    }
                }  // for each item in caseList
            }

            Dispatcher.Invoke((ThreadStart)delegate
            {
                //CaseFinder.Status = amController.PreArchiveStatus;
                CaseFinder.Refresh();
                caseList.SelectedIndex = 1;
            });
            
            timer.Enabled = true;
        }

        private void btnFinish_Click(object sender, RoutedEventArgs e)
        {
            timer.Enabled = false;
            timer.Dispose();
            timer = null;

            EnableButtons();

            if (Security.Security.IsLoggingEnabled)
            {
                logger.AddLogEntry(Environment.UserName, Environment.MachineName, sessionId, EventType.AutoArchiveFinished, Guid.Empty, string.Empty, string.Empty);
            }
        }
    }
}
