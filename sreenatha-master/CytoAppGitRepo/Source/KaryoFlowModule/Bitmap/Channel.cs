﻿using System;

namespace AI.Bitmap
{
    public sealed class Channel
    {
        private ushort[] pixels;
        private int width;
        private int height;
        private int bitsPerPixel;

        public Channel(int width, int height) : this(width, height, 2)
        {
        }

        public Channel(int width, int height, int bitsPerPixel)
            : this(width, height, bitsPerPixel, new ushort[width * height])
        {
        }


        public Channel(int width, int height, int bitsPerPixel, ushort[] pixels)
        {
            this.width = width;
            this.height = height;
            this.bitsPerPixel = bitsPerPixel;

            if (pixels != null)
            {
				this.pixels = pixels;
            }
            else
            {
				this.pixels = new ushort[width * height];
            }
        }

        public int Width
        {
            get { return width; }
        }

        public int Height
        {
            get { return height; }
        }

        public ushort[] Pixels
        {
            get { return pixels; }
        }

        public int BitsPerPixel
        {
            get { return bitsPerPixel; }
        }

        public Channel Duplicate()
        {
            ushort[] p = new ushort[pixels.Length];
            Array.Copy(pixels, p, pixels.Length);
            return new Channel(width, height, bitsPerPixel, p);
        }
    }
}
