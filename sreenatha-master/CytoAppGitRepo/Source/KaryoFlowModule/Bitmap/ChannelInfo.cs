﻿using System;
using System.ComponentModel;
using System.Windows.Media;

namespace AI
{
    public class Filter
    {
        public int RotorPosition { get; set; }
        public string Name { get; set; }
    }

    public class ChannelInfo : INotifyPropertyChanged
    {
        private Guid id;
        private int exposure;
        private Filter dichroic;
        private Filter excitation;
        private bool isStacked;
        private Color renderColor;
        private bool isCurrent;
        private bool isCounterstain;
        private string displayNameOverride;

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        void Notify(string s)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(s));
        }

        #endregion

        public ChannelInfo()
        {
            displayNameOverride = null;
            id = Guid.NewGuid();
            exposure = 127;
            renderColor = Colors.Red;
            IsStacked = true;
            IsAnalysisOverlay = false;
            Visible = true;
        }

        public ChannelInfo(string displayNameOverride)
            : this()
        {
            this.displayNameOverride = displayNameOverride;
        }

        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }

        public int Exposure
        {
            get { return exposure; }
            set { exposure = value; Notify("Exposure"); }
        }

        public int Gain
        {
            get { return 0; }
        }

        public int Offset
        {
            get { return 128; }
        }

        public Filter Dichroic
        {
            get { return dichroic; }
            set { dichroic = value; Notify("Dichroic"); Notify("DisplayName"); }
        }

        public Filter Excitation
        {
            get { return excitation; }
            set { excitation = value; Notify("Excitation"); Notify("DisplayName"); }
        }

        public bool IsStacked
        {
            get { return isStacked; }
            set { isStacked = value; Notify("IsStacked"); }
        }

        public Color RenderColor
        {
            get { return renderColor; }
            set { renderColor = value; Notify("RenderColor"); }
        }

        public bool IsCurrent
        {
            get { return isCurrent; }
            set { isCurrent = value; Notify("IsCurrent"); }
        }

        public bool IsCounterstain
        {
            get { return isCounterstain; }
            set { isCounterstain = value; Notify("IsCounterstain"); }
        }

        public bool IsAnalysisOverlay { get; set; }

        public bool Visible
        {
            get;
            set;
        }

        public string DisplayName
        {
            get
            {
                if (displayNameOverride != null)
                    return displayNameOverride;

                if (dichroic != null && excitation != null)
                    return dichroic.Name + " & " + excitation.Name;

                if (dichroic != null)
                    return dichroic.Name;

                if (excitation != null)
                    return excitation.Name;

                return string.Empty;
            }
        }

        public ChannelInfo Duplicate()
        {
            var c = new ChannelInfo();
            c.dichroic = dichroic;
            c.displayNameOverride = displayNameOverride;
            c.excitation = excitation;
            c.exposure = exposure;
            c.id = Guid.NewGuid();
            c.isCounterstain = isCounterstain;
            c.isCurrent = false;
            c.isStacked = isStacked;
            c.renderColor = renderColor;
            return c;
        }
    }
}
