﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using AI.Bitmap;

namespace AI
{
    public static class BitmapDebug
    {
        /// <summary>
        /// Converts a channel into a bitmap.
        /// If there are more than 8 bits per pixel the data is scaled down to 8 bits per pixel.
        /// </summary>
        /// <returns></returns>
        public static unsafe BitmapSource ToBitmap(this Channel c)
        {
            int n = c.Width * c.Height;
            byte[] displayBytes = new byte[n * 4];

            fixed (ushort* srcPtr = &c.Pixels[0])
            fixed (byte* destPtr = &displayBytes[0])
            {
                ushort* src = srcPtr;
                byte* dest = destPtr;
                if (c.BitsPerPixel <= 8)
                {
                    for (int i = 0; i < n; i++, src++)
                    {
                        *dest++ = (byte)*src;
                        *dest++ = (byte)*src;
                        *dest++ = (byte)*src;
                        *dest++ = 255;
                    }
                }
                else
                {
                    double scalefactor = 1.0 / Math.Pow(2, c.BitsPerPixel - 8);
                    byte val;
                    for (int i = 0; i < n; i++, src++)
                    {
                        val = (byte)((*src * scalefactor) + 0.5);

                        *dest++ = val;
                        *dest++ = val;
                        *dest++ = val;
                        *dest++ = 255;
                    }
                }
            }

            var s = BitmapSource.Create(c.Width, c.Height, 96, 96, PixelFormats.Bgra32, null, displayBytes, c.Width * 4);
            s.Freeze();
            return s;
        }

        public static void DumpImage(Channel channel, string path)
        {
            DumpImage(channel.ToBitmap(), path);
        }

        public static void DumpImage(BitmapSource img, string path)
        {
            using (var stream = File.OpenWrite(path))
            {
                PngBitmapEncoder enc = new PngBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(img));
                enc.Save(stream);
                stream.Flush();
                stream.Close();
            }
        }
    }
}
