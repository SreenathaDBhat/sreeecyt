﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AI.Bitmap;
using System.Windows.Media;
using System.Windows;

namespace AI
{
    public sealed class HistogramLowHighPair
    {
        public int Low { get; private set; }
        public int High { get; private set; }

        public HistogramLowHighPair(int low, int high)
        {
            Low = low;
            High = high;
        }
    }

    public unsafe class HistogramData
    {
        private int[] rawBins;

        public HistogramData(int[] bins, int bpp)
        {
            if (bins.Length != 256)
                throw new ArgumentException("bins needs to be exactly 256 big");

            rawBins = bins;
            int numberOfGreys = (int)Math.Pow(2, bpp) - 1;
            MaximumGrey = numberOfGreys;
        }

        public int[] Bins
        {
            get { return rawBins; }
        }

        public double MaximumGrey
        {
            get;
            set;
        }

        public double MaximumGreyNormalized
        {
            get { return MaximumGrey / 65535.0; }
        }
    }
}
