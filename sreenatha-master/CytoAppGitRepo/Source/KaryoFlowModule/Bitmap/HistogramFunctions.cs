﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI
{
    public struct HistogramValue
    {
        public double Low { get; set; }
        public double High { get; set; }

        public double LowNormalized
        {
            get { return Low / 65535.0; }
        }

        public double HighNormalized
        {
            get { return High / 65535.0; }
        }
    }

    public static class HistogramFunctions
    {
        public enum HistogramAlgorithm
        {
            Aggressive, // for use when bg subtraction is on
            Default
        }

        public static HistogramValue Estimate(HistogramAlgorithm algorithm, HistogramData hd, int bitsPerPixel, bool isCounterstain)
        {
            if (isCounterstain)
                return CounterstainEstimate(hd, bitsPerPixel);

            if(algorithm == HistogramAlgorithm.Aggressive)
            {
                return Aggressive(hd, bitsPerPixel);
            }
            else
            {
                return TopEndBySignificancePlusHalf(hd, bitsPerPixel);
            }
        }

        private static HistogramValue CounterstainEstimate(HistogramData hd, int bitsPerPixel)
        {
            double maxPixelValue = Math.Pow(2, bitsPerPixel);

            int contentLocation = 0;
            double tinyPercentage = Math.Min(5, hd.NumberOfPixels / 500000);

            for (ushort i = 0; i < maxPixelValue; i++)
            {
                int bin = hd.Bins[i];

                if (bin > 2)
                {
                    contentLocation = i;
                    break;
                }
            }

            return new HistogramValue
            {
                Low = contentLocation,
                High = (int)maxPixelValue
            };
        }

        private static HistogramValue TopEndBySignificancePlusHalf(HistogramData hd, int bitsPerPixel)
        {
            double maxPixelValue = Math.Pow(2, bitsPerPixel);

            int peakLocation = 0;
            int peakValue = 0;
            int endOfSignificanceScale = (int)maxPixelValue;
            int endOfValueScale = (int)maxPixelValue;
            double tinyPercentage = Math.Min(5, hd.NumberOfPixels / 500000);

            for (ushort i = 0; i < maxPixelValue; i++)
            {
                int bin = hd.Bins[i];

                if (bin > 0)
                    endOfValueScale = i;

                if (bin > peakValue)
                {
                    peakValue = bin;
                    peakLocation = i;
                }
            }

            // Fail.
            if (endOfSignificanceScale - endOfValueScale < 5)
            {
                return new HistogramValue
                {
                    Low = 0,
                    High = endOfSignificanceScale
                };
            }

            int halfway = (int)((endOfSignificanceScale + endOfValueScale) / 2);

            return new HistogramValue
            {
                Low = peakLocation,
                High = halfway
            };
        }

        private static HistogramValue Aggressive(HistogramData hd, int bitsPerPixel)
        {
            double maxPixelValue = Math.Pow(2, bitsPerPixel);

            int peakLocation = 0;
            int peakValue = 0;
            int endOfScale = (int)maxPixelValue;
            double tinyPercentage = Math.Min(5, hd.NumberOfPixels / 500000);

            for (ushort i = 0; i < maxPixelValue; i++)
            {
                int bin = hd.Bins[i];

                if (bin > tinyPercentage)
                    endOfScale = i;

                if (bin > peakValue)
                {
                    peakValue = bin;
                    peakLocation = i;
                }
            }

            if (endOfScale < peakLocation + 20)
                endOfScale = peakLocation + 40;


            return new HistogramValue
            {
                Low = peakLocation + 20,
                High = endOfScale
            };
        }
    }
}
