﻿using System;
using AI.Bitmap;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Windows.Media;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using System.Threading;
using System.IO;
using System.Windows;
using System.Threading.Tasks;

namespace AI
{
    public class Snap
    {
        public const int MaximumProjectionZIndex = -1;

        private Channel channel;

        public ChannelInfo ChannelInfo { get; set; }
        public int StackIndex { get; set; }
        public bool IsProjection { get; set; }
        public Guid Id { get; set; }
        public Guid BlobId { get; set; }

        public string ImageDataFile { get; set; }

        public Channel ChannelPixels
        { 
            get
            {
                return channel;
            }
            set
            {
                channel = value;
            }
        }
    }

    public enum ImageType
    {
        /// <summary>
        /// Strait from the camera. Untouched.
        /// </summary>
        Raw = 0,

        /// <summary>
        /// Images of this type have had stuff done to them by something. Result of a fuse merge, for example.
        /// </summary>
        Processed = 1,

        /// <summary>
        /// Deprecated? Only used when importing cases from cytovision.
        /// </summary>
        Fuse = 2
    }

    public class ImageFrame : INotifyPropertyChanged
    {
        private List<Snap> snaps;
        private BitmapSource thumbnail;
        private Guid id;
        private int width;
        private int height;
        private int bpp;
        private Point pos;
        private bool starred;
        private double gamma;

        public ImageFrame()
        {
            snaps = new List<Snap>();
            gamma = 1;
        }

        public void AddSnap(Snap snap)
        {
            snaps.Add(snap);

            Notify("StackSize");
            Notify("StackSizeMinusOne");
            Notify("ImageWidth");
            Notify("ImageHeight");
            Notify("ImageBitsPerPixel");
        }

        public ImageType ImageType
        {
            get;
            set;
        }

        public Point Position
        {
            get { return pos; }
            set { pos = value; }
        }

        public SlidePoint SlideLocation
        {
            get;
            set;
        }

        public bool DisplayInverted
        {
            get;
            set;
        }

        public double Gamma
        {
            get { return gamma; }
            set { gamma = value; Notify("Gamma"); }
        }

        public BitmapSource Thumbnail
        {
            get { return thumbnail; }
            set { thumbnail = value; Notify("Thumbnail"); }
        }

        public bool Starred
        {
            get { return starred; }
            set { starred = value; Notify("Starred"); }
        }

        private static int ChannelComparer(ChannelInfo x, ChannelInfo y)
        {
            if (x.IsCounterstain)
                return -1;
            else if (y.IsCounterstain)
                return 1;
            else if (x.Dichroic != null && y.Dichroic != null)
                return x.DisplayName.CompareTo(y.DisplayName);
            else
                return x.GetHashCode().CompareTo(y.GetHashCode());
        }

        public IEnumerable<ChannelInfo> Channels
        {
            get 
            {
                return (from s in snaps select s.ChannelInfo).Distinct();
            }
        }

        public IEnumerable<Snap> AllSnaps
        {
            get { return snaps; }
        }

        public bool HasProjection()
        {
            bool hasProj = false;

            foreach (var s in snaps)
            {
                if (s.IsProjection == true)
                {
                    hasProj = true;
                    break;
                }
            }

            return hasProj;
        }

        public Snap FindProjectionSnap(ChannelInfo channel, Snap fallbackSnap)
        {
            Snap projectionSnap = snaps.Where(s => s.ChannelInfo == channel && s.IsProjection).FirstOrDefault();
            return projectionSnap == null ? fallbackSnap : projectionSnap;
        }

        public Snap FindSnap(ChannelInfo channel, int stackIndex)
        {
            IEnumerable<Snap> channelSnaps = snaps.Where(s => s.ChannelInfo == channel);

            Snap closestSnap = null;
            int difference = int.MaxValue;

            foreach (var s in channelSnaps)
            {
                int d = Math.Abs(stackIndex - s.StackIndex);
                if (d < difference)
                {
                    difference = d;
                    closestSnap = s;
                }
            }

            return closestSnap;
        }

        public int StackSize
        {
            get
            {
                var counts = from c in Channels
                             select snaps.Count(s => s.ChannelInfo == c && !s.IsProjection);
                
                return counts.Count() == 0 ? 0 : counts.Max();
            }
        }

        public int StackSizeMinusOne
        {
            get { return StackSize - 1; }
        }

        public bool GotStack
        {
            get { return StackSize > 1; }
        }

        public int ImageWidth
        {
            get { return width; }
            set { width = value; }
        }

        public int ImageHeight
        {
            get { return height; }
            set { height = value; }
        }

        /// <summary>
        /// Height / Width
        /// </summary>
        public double AspectRatio
        {
            get { return ImageHeight / (double)ImageWidth; }
        }

        public int ImageBitsPerPixel
        {
            get { return bpp; }
            set { bpp = value; }
        }


        private Snap FirstSnapWithData()
        {
            foreach (var s in snaps)
            {
                if (s.ChannelPixels != null)
                    return s;
            }

            return null;
        }

        private unsafe ushort[] MaxProjection(IList<Snap> snaps)
        {
            ushort[] MaxProj;

            if (snaps.Count > 0)
            {
                int Size = snaps[0].ChannelPixels.Width * snaps[0].ChannelPixels.Height;
                int chunks = Environment.ProcessorCount;
                int splitLen = Size / chunks;

                MaxProj = new ushort[Size];
                Parallel.For(0, chunks, i =>
                {
                    for (int n = 0; n < splitLen; n++)
                    {
                        ushort Max = 0;
                        foreach (var snap in snaps)
                        {
                            if (snap.ChannelPixels.Pixels[n + i * splitLen] > Max)
                                Max = snap.ChannelPixels.Pixels[n + i * splitLen];
                        }
                        MaxProj[n + i * splitLen] = Max;
                    }
                });

                return MaxProj;
            }

            return null;
        }

        public Snap MaxProjection(ChannelInfo channel)
        {
            IList<Snap> ChannelSnaps = new List<Snap>();

            foreach (var snap in snaps)
            {
                if (snap.ChannelInfo == channel && snap.IsProjection == false)
                    ChannelSnaps.Add(snap);
            }

            Snap ProjSnap = new Snap();
            ProjSnap.Id = Guid.NewGuid();
            ProjSnap.ChannelInfo = channel;
            ProjSnap.IsProjection = true;
            ProjSnap.StackIndex = Snap.MaximumProjectionZIndex;
            ProjSnap.ChannelPixels = new Channel(ChannelSnaps[0].ChannelPixels.Width,
                                                 ChannelSnaps[0].ChannelPixels.Height,
                                                 ChannelSnaps[0].ChannelPixels.BitsPerPixel,
                                                 MaxProjection(ChannelSnaps));
            return ProjSnap;
        }

        public void RemoveChannelSnaps(ChannelInfo channel)
        {
            List<Snap> NewSnaps = new List<Snap>();
            foreach (var snap in snaps)
            {
                if (snap.ChannelInfo != channel || snap.IsProjection)
                    NewSnaps.Add(snap);
            }

            snaps = NewSnaps;
        }

        public void ClearAllImageData()
        {
            foreach (var snap in snaps)
            {
                snap.ChannelPixels = null;
            }
        }

        public IEnumerable<Snap> SnapsForChannel(ChannelInfo channel)
        {
            return snaps.Where(c => c.ChannelInfo == channel).OrderBy(s => s.StackIndex);
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void Notify(string p)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(p));
        }

        #endregion

        public IEnumerable<Snap> SnapsForIndex(int p)
        {
            foreach (var ch in Channels)
                yield return FindSnap(ch, p);
        }

		public Guid Id
        {
            get { return id; }
            set { id = value; }
		}

        public void SetThumbNoNotify(BitmapSource thumbnail)
        {
            this.thumbnail = thumbnail;
        }

        public void NotifyImages()
        {
            Notify("Thumbnail");
        }

        public void DumpAllImageData()
        {
            foreach (var s in snaps)
                s.ChannelPixels = null;
        }
    }
}
