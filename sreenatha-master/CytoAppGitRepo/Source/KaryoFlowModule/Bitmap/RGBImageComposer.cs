﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using AI.Bitmap;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Runtime.InteropServices;
using System.IO;

namespace AI
{
    public static class RGBImageComposer
    {
        public static BitmapSource Compose(ImageFrame image)
        {
            return Compose(image, 0);
        }

        public static BitmapSource Compose(ImageFrame image, IEnumerable<Snap> snaps)
        {
            return Compose(image, snaps, false);
        }

        public static BitmapSource Compose(ImageFrame image, int stackIndex)
        {
            var snaps = from c in image.Channels 
                        select image.FindSnap(c, stackIndex);

            return Compose(image, snaps, image.DisplayInverted);
        }

        private static unsafe BitmapSource Compose(ImageFrame image, IEnumerable<Snap> snaps, bool invert)
        {
            if (snaps.Count() == 0)
                return null;

            int len = image.ImageWidth * image.ImageHeight;
            byte[] destImageBytes = new byte[len * 4];

            fixed (byte* bp = &destImageBytes[0])
            {
                Compose(bp, len, image, snaps, invert);
            }

            BitmapSource img = BitmapImage.Create(image.ImageWidth, image.ImageHeight, 96, 96, PixelFormats.Bgr32, null, destImageBytes, image.ImageWidth * 4);
            img.Freeze();
            return img;
        }

        private static unsafe void Compose(byte* destPointer, int length, ImageFrame image, IEnumerable<Snap> snaps, bool invert)
        {
            byte* pOutput = destPointer;

            foreach (var snap in snaps)
            {
                if (snap.ChannelPixels == null)
                    continue;

                Color renderColor = snap.ChannelInfo.RenderColor;
                double maxPixelValue = Math.Pow(2.0, image.ImageBitsPerPixel) - 1;

                double max = Math.Pow(2, image.ImageBitsPerPixel) - 1;
                ushort low = 0;
                ushort high = (ushort)maxPixelValue;
                double gamma = 1.0 / image.Gamma;

                fixed (ushort* pInput = &snap.ChannelPixels.Pixels[0])
                {
                    ushort* pIn = pInput;
                    byte* pOut = pOutput;

                    for (int n = 0; n < length; ++n)
                    {
                        double f = (max / (high - low)) * ((*pIn++) - low) / max;
                        if (invert)
                            f = 1 - f;

                        f = Math.Pow(f, gamma);
                        f = Math.Max(0, Math.Min(1, f));

                        *pOut = (byte)(Math.Min(255, *pOut + renderColor.B * f));
                        pOut++;

                        *pOut = (byte)(Math.Min(255, *pOut + renderColor.G * f));
                        pOut++;

                        *pOut = (byte)(Math.Min(255, *pOut + renderColor.R * f));
                        pOut++;

                        *pOut++ = 255;
                    }
                }
            }
        }

        public static BitmapSource Compose(Channel channel)
        {
            Snap snap = new Snap();
            snap.ChannelPixels = channel;
            snap.ChannelInfo = new ChannelInfo
            {
                RenderColor = Colors.White
            };

            ImageFrame f = new ImageFrame();
            f.ImageWidth = channel.Width;
            f.ImageHeight = channel.Height;
            f.ImageBitsPerPixel = channel.BitsPerPixel;
            f.AddSnap(snap);

            return Compose(f);
        }
    }


    public static unsafe class ThumbnailHistograms
    {
        public static BitmapSource Compose(ImageFrame image)
        {
            int len = image.ImageWidth * image.ImageHeight;
            byte[] destImageBytes = new byte[len * 4];

            fixed (byte* bp = &destImageBytes[0])
            {
                foreach (var s in image.Channels)
                {
                    var fallback = image.FindSnap(s, image.StackSize / 2);
                    var snap = image.FindProjectionSnap(s, fallback);

                    Color renderColor = s.RenderColor;
                    double maxPixelValue = Math.Pow(2.0, image.ImageBitsPerPixel) - 1;

                    double max = Math.Pow(2, image.ImageBitsPerPixel) - 1;
                    ushort low = (ushort)0;
                    ushort high = (ushort)maxPixelValue;

                    fixed (ushort* pInput = &snap.ChannelPixels.Pixels[0])
                    {
                        ushort* pIn = pInput;
                        byte* pOut = bp;

                        for (int n = 0; n < len; ++n)
                        {
                            double f = (max / (high - low)) * ((*pIn++) - low) / max;
                            
                            *pOut = (byte)(Math.Min(255, *pOut + renderColor.B * f));
                            pOut++;

                            *pOut = (byte)(Math.Min(255, *pOut + renderColor.G * f));
                            pOut++;

                            *pOut = (byte)(Math.Min(255, *pOut + renderColor.R * f));
                            pOut++;

                            *pOut++ = 255;
                        }
                    }
                }

                BitmapSource img = BitmapImage.Create(image.ImageWidth, image.ImageHeight, 96, 96, PixelFormats.Bgr32, null, destImageBytes, image.ImageWidth * 4);
                img.Freeze();
                return img;
            }
        }
    }
}
