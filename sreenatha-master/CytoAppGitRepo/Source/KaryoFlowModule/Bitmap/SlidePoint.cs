﻿using System;
using System.Linq;
using System.Globalization;
using System.Windows.Media.Media3D;

namespace AI
{
    public struct SlidePoint
    {
        private double x;
        private double y;
        private double z;
        private string ef;


        public SlidePoint(Point3D pos, string ef)
            : this(pos.X, pos.Y, pos.Z, ef)
        {
        }
            
        public SlidePoint(double x, double y, double z, string ef)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.ef = ef;
        }

        public double IdealX
        {
            get { return x; }
        }

        public double IdealY
        {
            get { return y; }
        }

        public double IdealZ
        {
            get { return z; }
        }

        public string EnglandFinder
        {
            get { return ef; }
        }

        public static string Encode(SlidePoint slidePoint)
        {
            return Encode(CultureInfo.CurrentCulture, slidePoint);
        }

        public static string Encode(IFormatProvider culture, SlidePoint slidePoint)
        {
            return string.Format(culture, "{0:0.000}, {1:0.000}, {2:0.000}, {3}", slidePoint.IdealX, slidePoint.IdealY, slidePoint.IdealZ, slidePoint.EnglandFinder);
        }

        public static SlidePoint Decode(string slidePointString)
        {
            string[] bits = slidePointString.Replace(" ", "").Trim().Split(',');
            if (bits.Length != 4)
            {
                return new SlidePoint(0, 0, 0, "N/A");
            }

            double x = double.Parse(bits[0]);
            double y = double.Parse(bits[1]);
            double z = double.Parse(bits[2]);

            return new SlidePoint(x, y, z, bits[3]);
        }
    }
}
