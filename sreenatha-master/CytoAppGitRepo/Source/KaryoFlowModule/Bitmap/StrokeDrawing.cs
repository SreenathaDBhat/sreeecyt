﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Ink;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;

namespace AI
{
    public static class StrokeDrawing
    {
        public static void RenderStroke(Stroke stroke, Brush strokeBrush, Brush fillBrush, RenderTargetBitmap renderTarget, double offsetX, double offsetY)
        {
            var firstStylus = stroke.StylusPoints.First();
            Point start = new Point(firstStylus.X + offsetX, firstStylus.Y + offsetY);

            var segments = (from p in stroke.StylusPoints.Skip(1)
                            select new LineSegment(new Point(p.X + offsetX, p.Y + offsetY), true)).ToArray();

            var geom = new PathGeometry(new PathFigure[] { new PathFigure(start, (IEnumerable<PathSegment>)segments, fillBrush != null) });

            DrawingVisual visual = new DrawingVisual();
            DrawingContext context = visual.RenderOpen();
            Pen pen = new Pen(strokeBrush, stroke.DrawingAttributes.Width);
            pen.LineJoin = PenLineJoin.Round;
            pen.StartLineCap = PenLineCap.Round;
            pen.EndLineCap = PenLineCap.Round;
            context.DrawGeometry(fillBrush, pen, geom);
            context.Close();

            renderTarget.Render(visual);
        }
    }
}
