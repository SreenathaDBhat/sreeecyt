﻿using System;
using System.Threading;
using AI.Connect;
using System.Windows.Threading;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Concurrent;

namespace AI
{
    //public class BlobStreamData
    //{
    //    public object Tag { get; set; }
    //    public string BlobPath { get; set; }
    //    public Stream BlobStream { get; set; }
    //}

    //internal class BlobLoadQueueItem
    //{
    //    public Guid CaseId { get; set; }
    //    public CacheOption CacheOption { get; set; }
    //    public IEnumerable<BlobStreamData> Blobs { get; set; }
    //    public object Tag { get; set; }
    //}

    //public class BlobLoadedEventArgs : EventArgs
    //{
    //    public Guid CaseId { get; internal set; }
    //    public IEnumerable<BlobStreamData> Blobs { get; internal set; }
    //    public object Tag { get; internal set;  }

    //    public Stream StreamFor(object tag)
    //    {
    //        return (from b in Blobs
    //                where b.Tag == tag
    //                select b.BlobStream).FirstOrDefault();
    //    }
    //}

    //public class BlobLoadQueue
    //{
    //    public event EventHandler<BlobLoadedEventArgs> BlobLoaded;
    //    public enum QueueMode
    //    {
    //        LoadAll,
    //        LoadMostRecentOnly
    //    }


    //    private ConcurrentStack<BlobLoadQueueItem> queue;
    //    private Thread thread;
    //    private Dispatcher dispatcher;
    //    private QueueMode queueMode;        


    //    public BlobLoadQueue(Dispatcher dispatcher) : this(dispatcher, QueueMode.LoadMostRecentOnly)
    //    {
    //    }

    //    public BlobLoadQueue(Dispatcher dispatcher, QueueMode queueMode)
    //    {
    //        this.queueMode = queueMode;
    //        this.dispatcher = dispatcher;
    //        queue = new ConcurrentStack<BlobLoadQueueItem>();
    //    }

    //    public void Enqueue(Guid caseId, CacheOption cacheOption, object tag, string blobPath)
    //    {
    //        Enqueue(caseId, cacheOption, new BlobStreamData[] { new BlobStreamData { Tag = tag, BlobPath = blobPath } }, tag);
    //    }

    //    private void StartThread()
    //    {
    //        if (thread == null)
    //        {
    //            thread = new Thread(new ThreadStart(ThreadMain));
    //            thread.Name = "Image streaming";
    //            thread.Start();
    //        }
    //    }

    //    public void Enqueue(Guid caseId, CacheOption cacheOption, IEnumerable<BlobStreamData> blobs, object tag)
    //    {
    //        queue.Push(new BlobLoadQueueItem
    //        {
    //            CacheOption = cacheOption,
    //            CaseId = caseId,
    //            Blobs = blobs,
    //            Tag = tag
    //        });

    //        StartThread();
    //    }

    //    private void ThreadMain()
    //    {
    //        BlobLoadQueueItem itemToLoad;

    //        while (true)
    //        {
    //            if (queue.TryPop(out itemToLoad))
    //            {
    //                if(queueMode == QueueMode.LoadMostRecentOnly)
    //                    queue.Clear();

    //                var tasks = (from b in itemToLoad.Blobs
    //                             select Task.Factory.StartNew(delegate(object o)
    //                             {
    //                                 var item = (BlobLoadQueueItem)o;
    //                                 b.BlobStream = BlobStream.ReadBlobStream(item.CaseId, b.BlobPath, null, item.CacheOption);

    //                             }, itemToLoad)).ToArray();

    //                Task.WaitAll(tasks);

    //                // if something slid in while we were loading, dont bother sending the dated data.
    //                if (queue.IsEmpty)
    //                {
    //                    object[] args = new object[] { itemToLoad };

    //                    dispatcher.BeginInvoke((WaitCallback)delegate(object state)
    //                    {
    //                        var i = (BlobLoadQueueItem)state;
    //                        BlobLoaded(this, new BlobLoadedEventArgs()
    //                        {
    //                            Blobs = i.Blobs,
    //                            CaseId = i.CaseId,
    //                            Tag = i.Tag
    //                        });
    //                    }, DispatcherPriority.Normal, args);
    //                }
    //            }
    //            else
    //            {
    //                thread = null;
    //                break;
    //            }
    //        }
    //    }

    //    public void Stop()
    //    {
    //        queue.Clear();
    //    }
    //}
}
