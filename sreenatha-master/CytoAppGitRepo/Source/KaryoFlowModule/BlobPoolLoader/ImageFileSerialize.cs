﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Media.Imaging;
using System.IO.Compression;
using AI.Bitmap;
using AI.Connect;

namespace AI
{
    public static class ImageFileSerialize
    {
        public static unsafe Guid WriteSnapDataToDisk(this Snap snap, string relative, IDataSource dataSource)
        {
            string id = snap.Id.ToString() + ".snap";
            if (dataSource.ItemExists(relative, id))
            {
                return snap.Id;
            }

            using (MemoryStream stream = new MemoryStream())
            {
                WriteChannelDataToStream(stream, snap.ChannelPixels);
                dataSource.SaveStream(relative, stream, id);
                return snap.Id;
            }
        }

        public static Guid WriteKaryThumbnail(BitmapSource thumbnail, string relative, Guid cellId, IDataSource dataSource)
        {
            dataSource.SaveThumbnail(relative, thumbnail, cellId.ToString() + ".kar.jpg");
            return cellId;
        }

        public static Guid WriteKaryThumbnailToDisk(BitmapSource thumbnail, Guid caseId, Guid cellId, Database db)
        {
            return WriteThumbnailToDisk(thumbnail, caseId, cellId.ToString() + ".kar.jpg", db);
        }

        public static unsafe void WriteImageDataToDisk(this ImageFrame frame, Guid caseId, Database db)
        {
            string path = frame.Id.ToString() + ".jpg";

            foreach (var snap in frame.AllSnaps)
            {
                snap.WriteSnapDataToDisk(caseId, db);
            }

            if (frame.Thumbnail != null && frame.AllSnaps.Count() > 0)
                WriteThumbnailToDisk(frame.Thumbnail, caseId, path, db);
        }

        public static unsafe Guid WriteThumbnailToDisk(BitmapSource thumbnail, Guid caseId, string path, Database db)
        {
            if (thumbnail != null)
            {
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(thumbnail));

                using (MemoryStream stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    return BlobStream.WriteBlobStream(caseId, path, stream.GetBuffer(), (int)stream.Length);
                }
            }

            return Guid.Empty;
        }

        public static unsafe Guid WriteSnapDataToDisk(this Snap snap, Guid caseId, Database db)
        {
            string path = snap.Id.ToString() + ".snap";
            if (BlobStream.Exists(caseId, path, db))
            {
                return BlobStream.IdOf(caseId, path, db);
            }

            using (MemoryStream stream = new MemoryStream())
            {
                WriteChannelDataToStream(stream, snap.ChannelPixels); 
                return BlobStream.WriteBlobStream(caseId, path, stream, db);
            }
        }

        private static unsafe void WriteChannelDataToStream(Stream stream, Channel channel)
        {
            WriteChannelDataToStream(stream, channel, true);
        }

        public static unsafe void WriteChannelDataToStream(Stream stream, Channel channel, bool zip)
        {
            fixed (ushort* pShort = &channel.Pixels[0])
            {
                byte[] bytes;

                if (channel.BitsPerPixel <= 8)
                {
                    ushort* input = pShort;
                    int len = channel.Pixels.Length;
                    bytes = new byte[len];
                    fixed (byte* pOut = &bytes[0])
                    {
                        byte* p = pOut;
                        for (int i = 0; i < len; ++i)
                        {
                            *p++ = (byte)*input++;
                        }
                    }
                }
                else
                {
                    byte* input = (byte*)pShort;
                    int len = channel.Pixels.Length * 2;
                    bytes = new byte[len];
                    fixed (byte* pOut = &bytes[0])
                    {
                        byte* p = pOut;
                        for (int i = 0; i < len; ++i)
                        {
                            *p++ = *input++;
                        }
                    }
                }

                if(zip)
                    WriteGZip(stream, channel, bytes);
                else
                    WriteRaw(stream, channel, bytes);
            }
        }

        private static unsafe void WriteRaw(Stream stream, Snap snap, byte[] imageDataBytes)
        {
            WriteRaw(stream, snap.ChannelPixels, imageDataBytes);
        }

        private static unsafe void WriteRaw(Stream stream, Channel channel, byte[] imageDataBytes)
        {
            var writer = new BinaryWriter(stream);
            writer.Write(channel.Width);
            writer.Write(channel.Height);
            writer.Write(channel.BitsPerPixel);

            int len = imageDataBytes.Length;
            writer.Write(imageDataBytes, 0, len);
        }

        private static unsafe void WriteGZip(Stream stream, Snap snap, byte[] imageDataBytes)
        {
            WriteGZip(stream, snap.ChannelPixels, imageDataBytes);
        }

        private static unsafe void WriteGZip(Stream wstream, Channel channel, byte[] imageDataBytes)
        {

            using (MemoryStream stream = new MemoryStream()) 
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(80085);
                    writer.Write(channel.Width);
                    writer.Write(channel.Height);
                    writer.Write(channel.BitsPerPixel);
                    byte[] data = stream.ToArray();
                    wstream.Write(data, 0, data.Length);
                }
            }

            using (MemoryStream ms = new MemoryStream())
            {
                GZipStream zs = new GZipStream(ms, CompressionMode.Compress);

                int bytesPerPixel = channel.BitsPerPixel <= 8 ? 1 : 2;
                int len = channel.Pixels.Length * bytesPerPixel;
                zs.Write(imageDataBytes, 0, len);
                zs.Close();
                zs.Dispose();

                byte[] compressed = ms.ToArray();
                wstream.Write(compressed, 0, compressed.Length);
            
            }
        }
    }
}
