﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Text;
using AI.Connect;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Diagnostics;
using System.Xml.Linq;

namespace AI
{
    public static class ImageFrameDbLoad
    {
        public static ImageFrame FromDb(Guid frameId, Guid caseId)
        {
			ImageFrame frame;

			Database db = new Database();
			try
			{
				var newFrame = db.ImageFrames.Where(f => f.imageFrameId == frameId).First();
				frame = FromDb(db, newFrame, caseId);
			}
			finally
			{
				db.Dispose();
			}

			return frame;
        }

        public static ImageFrame FromDb(Database db, Guid frameId, Guid caseId)
        {
            var newFrame = db.ImageFrames.Where(f => f.imageFrameId == frameId).First();
            return FromDb(db, newFrame, caseId);
        }

        public static ImageFrame FromDb(Database db, Connect.ImageFrame dbFrame, Guid caseId)
        {
            var newFrame = new ImageFrame
                            {
                                Id = dbFrame.imageFrameId,
                                ImageBitsPerPixel = dbFrame.bpp,
                                ImageHeight = dbFrame.height,
                                ImageWidth = dbFrame.width,
                                SlideLocation = SlidePoint.Decode(dbFrame.location),
                                ImageType = (ImageType)dbFrame.imageType,
                                Position = new System.Windows.Point(dbFrame.x, dbFrame.y),
                                Gamma = dbFrame.gamma
                            };

            var channels = (from ch in db.ImageComponents
                            where ch.frameId == dbFrame.imageFrameId
                            orderby ch.isAnalysisOverlay ascending
                            select new
                            {
                                Fluor = new ChannelInfo(ch.displayName)
                                {
                                    IsCounterstain = ch.isCounterstain,
                                    IsAnalysisOverlay = ch.isAnalysisOverlay,
                                    RenderColor = ColorParse.HexStringToColor(ch.renderColor)
                                },

                                Id = ch.imageComponentId
                            }).ToArray();

            if ((channels != null) && (channels.Count() > 0))
            {
                foreach (var ci in channels)
                {
                    var snaps = (from s in db.ImageSnaps
                                 where s.componentId == ci.Id
                                 select new
                                 {
                                     Snap = new Snap
                                     {
                                         Id = s.imageSnapId,
                                         StackIndex = s.zLevel,
                                         ImageDataFile = s.imageSnapId.ToString() + ".snap",
                                         IsProjection = s.isProjection
                                     },
                                     ComponentId = s.componentId
                                 }).ToArray();

                    foreach (var snap in snaps)
                    {
                        snap.Snap.ChannelInfo = ci.Fluor;
                        newFrame.AddSnap(snap.Snap);
                    }
                }
            }

            return newFrame;
        }

        public static void ToDb(ImageFrame image, Guid slideId, Guid caseId, ImageType type, Guid groupId)
        {
            Database db = new Database();
			try
			{
				ToDb(db, image, slideId, caseId, type, groupId);
				db.SubmitChanges();
			}
			finally
			{
				db.Dispose();
			}
        }

        public static void ToDb(Database db, ImageFrame image, Guid slideId, Guid caseId, ImageType type, Guid groupId)
        {
            ToDb(db, image, slideId, caseId, type, groupId, null);
        }

        public static ImageFrame FromDataSource(IDataSource dataSource, string relative, Guid frameId)
        {
            var imageFramesXml = dataSource.LoadXml(relative, "images.imageframes");

            if (imageFramesXml == null)
                return null;

            var frameXml = (from f in imageFramesXml.Descendants("ImageFrame")
                            where f.Attribute("imageFrameId").Value == frameId.ToString()
                            select f).FirstOrDefault();

            if (frameXml == null)
                return null;

            var newFrame = new ImageFrame
            {
                Id = frameId,
                ImageBitsPerPixel = int.Parse(frameXml.Attribute("bpp").Value),
                ImageHeight = int.Parse(frameXml.Attribute("height").Value),
                ImageWidth = int.Parse(frameXml.Attribute("width").Value),
                SlideLocation = SlidePoint.Decode(frameXml.Attribute("location").Value),
                ImageType = (ImageType)int.Parse(frameXml.Attribute("imageType").Value),
                Position = new System.Windows.Point(double.Parse(frameXml.Attribute("x").Value), double.Parse(frameXml.Attribute("y").Value)),
                Gamma = double.Parse(frameXml.Attribute("gamma").Value)
            };

            var channels = (from ch in frameXml.Descendants("ImageComponent")
                            orderby ch.Attribute("isAnalysisOverlay").Value ascending
                            select ch).ToArray();

            if ((channels != null) && (channels.Count() > 0))
            {
                foreach (var ch in channels)
                {
                    var fluor = new ChannelInfo(ch.Attribute("displayName").Value)
                                {
                                    IsCounterstain = bool.Parse(ch.Attribute("isCounterstain").Value),
                                    IsAnalysisOverlay = bool.Parse(ch.Attribute("isAnalysisOverlay").Value),
                                    RenderColor = ColorParse.HexStringToColor(ch.Attribute("renderColor").Value)
                                };


                    var snaps = (from s in ch.Descendants("Snap")
                                 select s).ToArray();
       

                    foreach (var s in snaps)
                    {
                        var snap = new Snap
                        {
                            Id = Guid.Parse(s.Attribute("imageSnapId").Value),
                            StackIndex = int.Parse(s.Attribute("zLevel").Value),
                            ImageDataFile = s.Attribute("imageSnapId").Value + ".snap",
                            IsProjection = bool.Parse(s.Attribute("isProjection").Value)
                        };

                        snap.ChannelInfo = fluor;
                        newFrame.AddSnap(snap);
                    }
                }
            }

            return newFrame;
        }


        public static void ToDataSource(ImageFrame image, string slideId, ImageType type, Guid groupId, BitmapSource thumbnail, string relative, IDataSource dataSource)
        {
            Guid blobId = Guid.Empty;
            if (thumbnail != null)
            {
                MemoryStream thumbnailDataStream = new MemoryStream();
                JpegBitmapEncoder jpg = new JpegBitmapEncoder();
                jpg.QualityLevel = 80;
                jpg.Frames.Add(BitmapFrame.Create(thumbnail));
                jpg.Save(thumbnailDataStream);
                var thumbname = image.Id.ToString() + ".jpg";
                blobId = image.Id;
                dataSource.SaveStream(relative, thumbnailDataStream, thumbname);
            }

            var xml = new XElement("ImageFrames");

            var iframeXml = new XElement("ImageFrame",
            new XAttribute("imageFrameId",image.Id),
            new XAttribute("bpp",image.ImageBitsPerPixel),
            new XAttribute("height",image.ImageHeight),
            new XAttribute("width",image.ImageWidth),
            new XAttribute("location",SlidePoint.Encode(image.SlideLocation)),
            new XAttribute("slideId",slideId),
            new XAttribute("imageType",(int)type),
            new XAttribute("groupId",groupId),
            new XAttribute("x",image.Position.X),
            new XAttribute("y",image.Position.Y),
            new XAttribute("gamma",image.Gamma),
            new XAttribute("blobId",blobId));

            foreach (var realChannel in image.Channels)
            {
                var componentXml = new XElement("ImageComponent",
                    new XAttribute("displayName", realChannel.DisplayName),
                    new XAttribute("imageComponentId", Guid.NewGuid()),
                    new XAttribute("isCounterstain", realChannel.IsCounterstain),
                    new XAttribute("renderColor", ColorParse.ToHexString(realChannel.RenderColor)),
                    new XAttribute("isAnalysisOverlay", realChannel.IsAnalysisOverlay));

                    var snapsForThisChannel = image.AllSnaps.Where(s => s.ChannelInfo == realChannel);

                    foreach (var realSnap in snapsForThisChannel)
                    {
                        var snapXml = new XElement("Snap",

                        new XAttribute("imageSnapId",realSnap.Id),
                        new XAttribute("zLevel",realSnap.StackIndex),
                        new XAttribute("isProjection",realSnap.IsProjection),
                        new XAttribute("blobId",realSnap.WriteSnapDataToDisk(relative, dataSource)));

                        // SN Am trying to keep the in-memory realsnap in sync with the db but what if an update following this
                        // method fails? ... realSnap is then out of sync with the db.
                        realSnap.ImageDataFile = realSnap.Id.ToString() + ".snap"; // WH eh?

                        componentXml.Add(snapXml);
                    }

                iframeXml.Add(componentXml);
            }

            xml.Add(iframeXml);
            dataSource.SaveXml(relative, xml, "images.imageframes");
        }

        public static void ToDb(Database db, ImageFrame image, Guid slideId, Guid caseId, ImageType type, Guid groupId, BitmapSource thumbnail)
        {
            var frame = new Connect.ImageFrame();
            frame.imageFrameId = image.Id;
            frame.bpp = image.ImageBitsPerPixel;
            frame.height = image.ImageHeight;
            frame.width = image.ImageWidth;
            frame.location = SlidePoint.Encode(image.SlideLocation);
            frame.slideId = slideId;
            frame.imageType = (int)type;
            frame.groupId = groupId;
            frame.x = image.Position.X;
            frame.y = image.Position.Y;
            frame.gamma = image.Gamma;

            if (thumbnail != null)
            {
                MemoryStream thumbnailDataStream = new MemoryStream();
                JpegBitmapEncoder jpg = new JpegBitmapEncoder();
                jpg.QualityLevel = 80;
                jpg.Frames.Add(BitmapFrame.Create(thumbnail));
                jpg.Save(thumbnailDataStream);
                var thumbname = image.Id.ToString() + ".jpg";
                frame.blobid = Connect.BlobStream.WriteBlobStream(caseId, thumbname, thumbnailDataStream, db);
            }

            db.ImageFrames.InsertOnSubmit(frame);

            foreach (var realChannel in image.Channels)
            {
                var component = new ImageComponent();
                component.displayName = realChannel.DisplayName;
                component.ImageFrame = frame;
                component.imageComponentId = Guid.NewGuid();
                component.isCounterstain = realChannel.IsCounterstain;
                component.renderColor = ColorParse.ToHexString(realChannel.RenderColor);
                component.isAnalysisOverlay = realChannel.IsAnalysisOverlay;
                db.ImageComponents.InsertOnSubmit(component);

                var snapsForThisChannel = image.AllSnaps.Where(s => s.ChannelInfo == realChannel);

                foreach (var realSnap in snapsForThisChannel)
                {
                    var snap = new Connect.ImageSnap();
                    snap.ImageComponent = component;
                    snap.imageSnapId = realSnap.Id;
                    snap.zLevel = realSnap.StackIndex;
                    snap.isProjection = realSnap.IsProjection;
                    snap.blobId = realSnap.WriteSnapDataToDisk(caseId, db);
                    db.ImageSnaps.InsertOnSubmit(snap);

					// SN Am trying to keep the in-memory realsnap in sync with the db but what if an update following this
					// method fails? ... realSnap is then out of sync with the db.
					realSnap.ImageDataFile = realSnap.Id.ToString() + ".snap";
                }
            }
        }

        public static ImageSource ToThumbnail(ImageFrame image, Guid caseId, Database db)
        {
            var thumbBitmap = RGBImageComposer.Compose(image, 0);

            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(thumbBitmap));

            using (var stream = new MemoryStream())
            {
                encoder.Save(stream);
                stream.Flush();

                BlobStream.WriteBlobStream(caseId, image.Id.ToString() + ".jpg", stream);
                stream.Close();
            }

            return thumbBitmap;
        }

		public static void Delete( Guid frameId)
		{
			using (var db = new Database())
			{
				Delete( db, frameId);
				db.SubmitChanges();
			}
		}

		public static void Delete(Database db, Guid frameId)
		{
			// Before deleting the image frame from the database,
			// we need to get rid of the associated files, ie thumbnails and snaps.
			var frame = db.ImageFrames.Where(f => f.imageFrameId == frameId).FirstOrDefault();
			if (frame != null)
			{
				if ((frame.blobid != null) && (frame.blobid != Guid.Empty))
				{
					BlobStream.Delete((Guid)frame.blobid, db);
				}

				db.ImageFrames.DeleteOnSubmit(frame);
			}
		}
	}
}
