﻿using System;
using AI.Bitmap;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Windows.Media;
using System.Linq;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;

namespace AI
{
    public static class ImageFromBitmapSource
    {
        public static Channel Convert(BitmapSource bitmapSource, bool invert)
        {
            if (bitmapSource.Format == PixelFormats.Gray8)
            {
                return LoadGray8(bitmapSource, invert);
            }
            else if (bitmapSource.Format == PixelFormats.Bgra32 || bitmapSource.Format == PixelFormats.Pbgra32 || bitmapSource.Format == PixelFormats.Bgr32)
            {
                return LoadBgra32(bitmapSource, invert);
            }
            else if (bitmapSource.Format == PixelFormats.Indexed8)
            {
                return LoadIndexed8(bitmapSource, invert);
            }

            throw new NotSupportedException();
        }

        private unsafe static Channel LoadIndexed8(BitmapSource bitmapSource, bool invert)
        {
            int len = bitmapSource.PixelWidth * bitmapSource.PixelHeight;
            byte[] pixelData = new byte[len];
            ushort[] outChannel = new ushort[len];
            bitmapSource.CopyPixels(new Int32Rect(0, 0, bitmapSource.PixelWidth, bitmapSource.PixelHeight), pixelData, bitmapSource.PixelWidth, 0);

            fixed (byte* pIn = &pixelData[0])
            fixed (ushort* pOut = &outChannel[0])
            {
                byte* src = pIn;
                ushort* dst = pOut;
                byte v;

                for (int i = 0; i < len; ++i)
                {
                    v = *src++;
                    v = bitmapSource.Palette.Colors[v].R;

                    if (invert)
                        *dst++ = (byte)(255 - v);
                    else
                        *dst++ = v;
                }
            }

            Channel channel = new Channel(bitmapSource.PixelWidth, bitmapSource.PixelHeight, 8, outChannel);
            return channel;
        }

        private unsafe static Channel LoadBgra32(BitmapSource bitmapSource, bool invert)
        {
            int len = bitmapSource.PixelWidth * bitmapSource.PixelHeight;
            byte[] pixelData = new byte[len * 4];
            ushort[] outChannel = new ushort[len];
            bitmapSource.CopyPixels(new Int32Rect(0, 0, bitmapSource.PixelWidth, bitmapSource.PixelHeight), pixelData, bitmapSource.PixelWidth * 4, 0);

            fixed (byte* pIn = &pixelData[0])
            fixed (ushort* pOut = &outChannel[0])
            {
                byte* src = pIn;
                ushort* dst = pOut;

                for (int i = 0; i < len; ++i)
                {
                    if (invert)
                        *dst++ = (byte)(255 - *src);
                    else
                        *dst++ = *src;

                    src += 4;
                }
            }

            Channel channel = new Channel(bitmapSource.PixelWidth, bitmapSource.PixelHeight, 8, outChannel);
            return channel;
        }

        private unsafe static Channel LoadGray8(BitmapSource bitmapSource, bool invert)
        {
            int len = bitmapSource.PixelWidth * bitmapSource.PixelHeight;
            int bytesperpixel = (bitmapSource.Format.BitsPerPixel + 7) / 8;
            byte[] pixelData = new byte[len * bytesperpixel];
            int stride = bitmapSource.PixelWidth * bytesperpixel;
            bitmapSource.CopyPixels(new Int32Rect(0, 0, bitmapSource.PixelWidth, bitmapSource.PixelHeight), pixelData, stride , 0);

            ushort[] oneChannel = new ushort[len];

            fixed (byte* pIn = &pixelData[0])
            fixed (ushort* pOut = &oneChannel[0])
            {
                ushort* dst = pOut;
                int h = bitmapSource.PixelHeight;
                int w = bitmapSource.PixelWidth;

                for (int y = 0; y < h; ++y)
                {
                    // offset to beginning of next row
                    byte* src = pIn + (y * bitmapSource.PixelWidth * bytesperpixel);

                    // process a row of data
                    for (int x = 0; x < w; ++x)
                    {
                        if (invert)
                            *dst++ = (byte)(255 - (*src++));
                        else
                            *dst++ = *src++;
                    }
                }
            }
            Channel channel = new Channel(bitmapSource.PixelWidth, bitmapSource.PixelHeight, 8, oneChannel);
            return channel;
        }

        public unsafe static Channel LoadFromDisk(Guid caseId, string path, Connect.Database db)
        {
            if (!Connect.BlobStream.Exists(caseId, path, db))
                return null;

            using(var stream = Connect.BlobStream.ReadBlobStream(caseId, path, db, Connect.CacheOption.Cache))
                return LoadFromStream(stream);
        }

        unsafe public static Channel LoadFromStream(Stream stream)
        {
            using (BinaryReader reader = new BinaryReader(stream))
            {
                int type = reader.ReadInt32();
                int width = 0;
                bool compressed = false;
                if (type == 80085)
                {
                    compressed = true;
                    width = reader.ReadInt32();
                }
                else
                {
                    width = type;
                }

                int height = reader.ReadInt32();
                int bpp = reader.ReadInt32();

                if (bpp <= 8)
                {
                    return Load8bit(width, height, compressed, stream);
                }
                else
                {
                    return Load16bit(width, height, bpp, compressed, stream);
                }
            }
        }

        private unsafe static Channel Load8bit(int width, int height, bool compressed, Stream stream)
        {
            int len = width * height;
            byte[] bytes = new byte[len];

            if (compressed)
            {
                int n = (int)(stream.Length - stream.Position);
                byte[] compressedBytes = new byte[n];
                stream.Read(compressedBytes, 0, n);

                MemoryStream ms = new MemoryStream(compressedBytes);
                GZipStream zs = new GZipStream(ms, CompressionMode.Decompress);

                int totalRead = 0;
                int bytesRead = 0;
                int need = 0;
                Boolean finished = false;
 
                while (!finished)
                {
                    need = len - totalRead;
                    bytesRead = zs.Read(bytes, totalRead, need);

                    if (bytesRead == 0)
                        finished = true;
                    else
                        totalRead += bytesRead;
                }
            }
            else
            {
                stream.Read(bytes, 0, len);
            }

            ushort[] shortBytes = new ushort[len];
            fixed (ushort* pDest = &shortBytes[0])
            fixed (byte* pSrc = &bytes[0])
            {
                byte* src = pSrc;
                ushort* dst = pDest;

                for (int i = 0; i < len; ++i)
                {
                    *dst++ = (ushort)*src;
                    src += 1;
                }
            }

            Channel ch = new Channel(width, height, 8, shortBytes);
            return ch;
        }

        private unsafe static Channel Load16bit(int width, int height, int bpp, bool compressed, Stream stream)
        {
            int len = width * height;
            byte[] bytes = new byte[len * 2];

            if (compressed)
            {
                int n = (int)(stream.Length - stream.Position);
                byte[] compressedBytes = new byte[n];
                stream.Read(compressedBytes, 0, n);

                MemoryStream ms = new MemoryStream(compressedBytes);
                GZipStream zs = new GZipStream(ms, CompressionMode.Decompress);

                zs.Read(bytes, 0, len * 2);
            }
            else
            {
                stream.Read(bytes, 0, len * 2);
            }

            ushort[] shortBytes = new ushort[len];
            fixed (ushort* pDest = &shortBytes[0])
            fixed (byte* pSrc = &bytes[0])
            {
                byte* src = pSrc;
                ushort* dst = pDest;

                for (int i = 0; i < len; ++i)
                {
                    *dst++ = *(ushort*)src;
                    src += 2;
                }
            }

            Channel ch = new Channel(width, height, bpp, shortBytes);
            return ch;
        }




        public static Channel LoadBitmapFromDisk(string file, bool invert)
        {
            BitmapImage img = new BitmapImage();
            img.BeginInit();
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.UriSource = new Uri(file);
            img.EndInit();
            img.Freeze();

            return ImageFromBitmapSource.Convert(img, invert);
        }

        public static BitmapImage LoadBitmapImageFromDisk(string file)
        {
            FileInfo fileInfo = new FileInfo(file);

            using (var fileStream = File.OpenRead(file))
            {
                long len = fileInfo.Length;
                byte[] bytes = new byte[len];
                BinaryReader reader = new BinaryReader(fileStream);
                reader.Read(bytes, 0, (int)len);

                MemoryStream mem = new MemoryStream(bytes);
                BitmapImage img = new BitmapImage();
                img.BeginInit();
                img.CacheOption = BitmapCacheOption.OnLoad;
                img.StreamSource = mem;
                img.EndInit();
                img.Freeze();

                fileStream.Close();
                return img;
            }
        }


        public static Channel LoadBitmapFromStream(Guid caseId, string path, AI.Connect.Database db)
        {
            throw new NotSupportedException();

            //var stream = Connect.BlobStream.ReadBlobStream(caseId, path, db, Connect.CacheOption.Cache);

            //BitmapImage img = new BitmapImage();
            //img.BeginInit();
            //img.CacheOption = BitmapCacheOption.OnLoad;
            //img.StreamSource = stream;
            //img.EndInit();
            //img.Freeze();

            //return ImageFromBitmapSource.Convert(img, false);
        }

        public static BitmapImage LoadBitmapImageFromStream(Guid caseId, string path, AI.Connect.Database db)
        {
         //   return LoadBitmapImageFromStream(caseId, path, db, AI.Connect.CacheOption.Cache, 0);
            throw new NotSupportedException();
        }

        public static BitmapImage LoadBitmapImageFromStream(Guid caseId, string path, AI.Connect.Database db, Connect.CacheOption cacheOption, int decodeWidth)
        {
            //using (var fileStream = Connect.BlobStream.ReadBlobStream(caseId, path, db, cacheOption))
            //{
            //    return LoadBitmapImageFromStream(decodeWidth, fileStream);
            //}
            throw new NotSupportedException();
        }


        // slowly remove anything with Database above this line

        public static BitmapImage LoadBitmapImageFromStream(Stream stream)
        {
            return LoadBitmapImageFromStream(0, stream);
        }

        public static BitmapImage LoadBitmapImageFromStream(int decodeWidth, Stream stream)
        {
            if (stream == null)
                return null;

            BitmapImage img = new BitmapImage();
            img.BeginInit();
            img.CacheOption = BitmapCacheOption.OnLoad;

            if (decodeWidth > 0)
                img.DecodePixelWidth = decodeWidth;

            img.StreamSource = stream;
            img.EndInit();
            img.Freeze();

            stream.Close();
            return img;
        }
    }
}
