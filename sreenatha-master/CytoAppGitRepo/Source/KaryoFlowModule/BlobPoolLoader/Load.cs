﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using AI.Connect;
using System.Windows.Media.Imaging;

namespace AI
{
    public static class Load
    {
        public static bool GotDataFile(this Snap snap, Guid caseId)
        {
            return BlobStream.Exists(caseId, snap.ImageDataFile, null);
        }

        public static bool LoadDataIfNeeded(this Snap snap, Database db, Guid caseId)
        {
            if (snap.ChannelPixels != null)
                return false;

            if (!BlobStream.Exists(caseId, snap.ImageDataFile, db))
                return false;

            snap.ChannelPixels = ImageFromBitmapSource.LoadFromDisk(caseId, snap.ImageDataFile, db);
            return true;
        }

        public static bool LoadDataIfNeeded(this Snap snap, Guid caseId)
        {
			bool loadOk;

			using (Database db = new Database())
			{
				loadOk = snap.LoadDataIfNeeded( db, caseId);
			}

			return loadOk;
        }

        public static BitmapImage LoadThumbnail(Guid frameId, Guid caseId)
        {
			BitmapImage image;

			using (Database db = new Database())
			{
				image = LoadThumbnail(db, frameId, caseId);
			}

			return image;
        }

        public static BitmapImage LoadThumbnail(Database db, Guid frameId, Guid caseId)
        {
            string path = frameId.ToString() + ".jpg";
            if (!BlobStream.Exists(caseId, path, db))
                return null;

            return ImageFromBitmapSource.LoadBitmapImageFromStream(caseId, path, db);
        }
    }
}
