﻿using System;
using System.Windows.Input;
using System.Windows;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using Microsoft.Win32;
using System.Windows.Media;
using System.Windows.Threading;
using System.Threading;

namespace AI.Brightfield
{
    public partial class AnalysisPage : IClosing
    {
        public static RoutedUICommand ResetFocus = new RoutedUICommand("ResetFocus", "ResetFocus", typeof(AnalysisPage));
        public static RoutedUICommand EditCellComments = new RoutedUICommand("EditCellComments", "EditCellComments", typeof(AnalysisPage));
        public static RoutedUICommand EditCellInfo = new RoutedUICommand("EditCellInfo", "EditCellInfo", typeof(AnalysisPage));
        public static RoutedUICommand EditCaseInfo = new RoutedUICommand("EditCaseInfo", "EditCaseInfo", typeof(AnalysisPage));
        public static RoutedUICommand EditCellResult = new RoutedUICommand("EditCellResult", "EditCellResult", typeof(AnalysisPage));
        public static RoutedUICommand DisplaySettings = new RoutedUICommand("DisplaySettings", "DisplaySettings", typeof(AnalysisPage));

        private WorkflowController controller;
        private StatusWindow statusWindow = null;

        public AnalysisPage(WorkflowController controller)
        {
            this.controller = controller;
            DataContext = controller;

            InitializeComponent();

            imageDisplayOptions.Image = controller.Renderer;
            controller.ImageControl = mainImage;

            controller.ToolChanged += OnToolChanged;
            PreviewKeyDown += OnStealKeyPresses_KeyDown;
            PreviewKeyUp += OnStealKeyPresses_KeyUp;

            controller.Renderer.DisplayChanging += new EventHandler(Renderer_DisplayChanging);
            controller.Renderer.DisplayChanged += new EventHandler(Renderer_DisplayChanged);

            Loaded += (s, e) =>
            {
                if (controller.CurrentCell == null)
                {
                    controller.SetTool(() => new DefaultToolController());
                }

                ScratchPadLoosItemHookerUpper.SetLooseItems(this, new ScratchPadLoosItemHookerUpper(this, controller.Renderer, controller));

                ForceFocus();
            };
            Unloaded += (s, e) =>
            {
                controller.SaveStuff();
                CaseLocking.ReleaseCaseLock(controller.Case);

                if (statusWindow != null)
                    statusWindow.Close();
            };
        }

        void Renderer_DisplayChanged(object sender, EventArgs e)
        {
            ShowStatus(false);
            if (controller.Tool != null)
                controller.Tool.OnDispayChanged();
        }

        void Renderer_DisplayChanging(object sender, EventArgs e)
        {
            ShowStatus(true);
        }

        private void ForceFocus()
        {
            homeButton.Focus();
            Keyboard.Focus(homeButton);
        }

        void OnStealKeyPresses_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.FocusedElement is TextBox)
                return;

            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
                return;

            controller.OnKeyDown(e, Mouse.GetPosition(mainImage));
            if (!e.Handled)
            {
                HandleKey(e);
            }
        }

        void OnStealKeyPresses_KeyUp(object sender, KeyEventArgs e)
        {
            if (!(Keyboard.FocusedElement is TextBox) && !Keyboard.IsKeyDown(Key.LeftCtrl) && !(Keyboard.IsKeyDown(Key.RightCtrl)))
            {
                controller.OnKeyUp(e, Mouse.GetPosition(mainImage));
            }
        }

        private void OnPenSizeWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            if (LooseItems.InLooseCollectionMode && Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                e.Handled = true;
                double d = Math.Sign(e.Delta);
                LooseItems.Collector.PenSize = Math.Max(10, Math.Min(100, LooseItems.Collector.PenSize + (d * 5)));
            }
        }

        private void OnToolChanged(object sender, EventArgs e)
        {
            var iUi = controller.Tool == null ? null : controller.Tool.ImageUi;
            var sUi = controller.Tool == null ? null : controller.Tool.ScreenUi;
            var tUi = controller.Tool == null ? null : controller.Tool.ToolbarUi;

            if (iUi != null)
            {
                StretchOrSizeCanvas.SetSizeMode(iUi, controller.Tool.ImageUiSizeMode);
            }

            imageUi.Child = iUi;
            screenUi.Child = sUi;
            toolbarUi.Child = tUi;

            ForceFocus();

        }

        private void CanNextCellExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.CanNextCellExecute();
        }

        private void NextCellExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            SaveToolUi();
            ClearToolUi();
            controller.NextCellExecuted();
        }

        private void CanPrevCellExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.CanPreviousCellExecute();
        }

        private void PrevCellExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            SaveToolUi();
            ClearToolUi();
            controller.PreviousCellExecuted();
        }

        private void ClearToolUi()
        {
            screenUi.Child = null;
            imageUi.Child = null;
            toolbarUi.Child = null;
        }

        private void SaveToolUi()
        {
            if (controller.Tool != null && (controller.Tool.UndoList == null || controller.Tool.UndoList.HasContent))
                controller.SaveTool();
        }

        private void CanCaptureExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }

        private void CaptureExecuted(object sender, ExecutedRoutedEventArgs e)
        {
        }

        private void CanCountExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller != null && controller.CurrentCell != null && !(controller.Tool is CountController);
        }

        private void CountExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            SaveToolUi();
            ClearToolUi();
            controller.SetTool(() => new CountController());
        }

        private void CanNumberExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller != null && controller.CurrentCell != null && !(controller.Tool is NumberController);
        }

        private void NumberExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            SaveToolUi();
            ClearToolUi();
            controller.SetTool(() => new NumberController(homeButton));
        }

        private void CanKaryotypeExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller != null && controller.CurrentCell != null && !(controller.Tool is KaryotypeController);
        }

        private void KaryotypeExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            SaveToolUi();
            ClearToolUi();
            controller.SetTool(() => new KaryotypeController(controller.Renderer, Dispatcher));
        }

        private void CanUndoExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.CanUndoExecute();
        }

        private void UndoExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            controller.UndoExecuted();
        }

        private void CanDisplaySettingsExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.CurrentCell != null;
        }

        private void DisplaySettingsExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            extraToolsPopup.IsOpen = false;
            cellInfoPopup.IsOpen = false;
            displayPopup.IsOpen = true;
            caseInfoPopup.IsOpen = false;
        }

        private void CanSaveExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.Tool != null && controller.CurrentCell != null;
        }

        private void SaveExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(this));
            controller.SaveTool();
            controller.Totals.Refresh(controller.WorkingCells);
        }

        private void CanReportExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ReportExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            LaunchCellOrganizer(false);
        }

        private void LaunchCellOrganizer(bool organizeMode)
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(this));
            controller.SaveTool();

            CellOrganizer dlg = new CellOrganizer(controller, organizeMode);

            dlg.Owner = VisualTreeWalker.FindParentOfType<Window>(this);
            dlg.ShowDialog();

            ForceFocus();

            controller.WorkingCells = dlg.VisibleCells;

            if (dlg.MoveToSelectedCellOnClose && dlg.SelectedCell != controller.CurrentCell && dlg.SelectedCell != null)
            {
                controller.CurrentCell = dlg.SelectedCell;
            }
        }

        private void CanOrganizeExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void OrganizeExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            LaunchCellOrganizer(true);
        }

        private void OnBack(object sender, RoutedEventArgs e)
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(this));

            if (controller.Tool != null)
            {
                if(controller.Tool.UndoList == null || controller.Tool.UndoList.HasContent)
                    controller.SaveTool();
            }

            //controller.DisplayOptions.Save();
            controller.Persistants.Persist();

            UIStack.For(this).Pop(UIStackResult.Ok);
        }

        public void ShowStatus(bool on)
        {
            if (on)
            {
                if (statusWindow == null)
                    statusWindow = new StatusWindow();
                statusWindow.Show();
            }
            else
            {
                if (statusWindow != null)
                    statusWindow.Hide();
            }
        }

        private void OnShowCellInfo(object sender, RoutedEventArgs e)
        {
            extraToolsPopup.IsOpen = false;
            displayPopup.IsOpen = false;
            cellInfoPopup.IsOpen = true;
            caseInfoPopup.IsOpen = false;
        }

        private void OnShowCaseInfo(object sender, RoutedEventArgs e)
        {
            extraToolsPopup.IsOpen = false;
            cellInfoPopup.IsOpen = false;
            displayPopup.IsOpen = false;
            caseInfoPopup.IsOpen = true;
        }

        private void OnShowToolsSlideout(object sender, RoutedEventArgs e)
        {
            extraToolsPopup.IsOpen = true;
            displayPopup.IsOpen = false;
            cellInfoPopup.IsOpen = false;
            caseInfoPopup.IsOpen = false;
        }

        private void CanRevertExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = mainImage != null && controller.CurrentCell != null;
        }

        private void RevertExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            revertConfirmButton.Visibility = Visibility.Collapsed;
            Revert();
        }

        private void Revert()
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(this));
            ClearToolUi();
            controller.RevertTool();
        }

        private void HandleKey(KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.PageUp:
                    if (controller.CanPreviousCellExecute())
                        controller.PreviousCellExecuted();
                    break;

                case Key.PageDown:
                    if (controller.CanNextCellExecute())
                        controller.NextCellExecuted();
                    break;

                case Key.F2:
                    LaunchScratchPad();
                    e.Handled = true;
                    break;

                case Key.F3:
                    LaunchGrid();
                    e.Handled = true;
                    break;
            }
        }

        private void LaunchGrid()
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(this));
            controller.SaveTool();

            ChromosomeGrid grid = new ChromosomeGrid(controller);
            grid.Owner = VisualTreeWalker.FindParentOfType<Window>(this);
            grid.ShowDialog();

            Revert();
            ForceFocus();
        }

        private void LaunchScratchPad()
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(this));
            controller.SaveTool();

            ScratchPadWindow scratchwnd = new ScratchPadWindow(controller);
            scratchwnd.Owner = VisualTreeWalker.FindParentOfType<Window>(this);
            scratchwnd.Show();
            LooseItems.InLooseCollectionMode = false;
        }

        private void OnLooseScratchItemDrawn(object sender, System.Windows.Controls.InkCanvasStrokeCollectedEventArgs e)
        {
            LooseItems.Collector.CutStroke(controller.Renderer, e.Stroke, controller.CurrentCell.Name, controller.CurrentCell.Slide.Name, controller.CurrentCell.Id);
        }

        public ScratchPadLoosItemHookerUpper LooseItems
        {
            get { return ScratchPadLoosItemHookerUpper.GetLooseItems(this); }
        }

        private void OnEnterQuickCut(object sender, RoutedEventArgs e)
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(this));

            LooseItems.InLooseCollectionMode = true;
            LooseItems.Collector.UpdateDestinationPads(controller.CaseId);
        }

        private void OnGrid(object sender, RoutedEventArgs e)
        {
            LaunchGrid();
        }

        private void OnScratchPad(object sender, RoutedEventArgs e)
        {
            LaunchScratchPad();
        }

        private void CanExecuteTrueWhenGotCell(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller != null && controller.CurrentCell != null;
        }

        private void CanExecuteTrue(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void OnEditCellComments(object sender, ExecutedRoutedEventArgs e)
        {
            if (CellMetadataPopup.EditSingleField(VisualTreeWalker.FindParentOfType<Window>(this), controller.CurrentCell.MetaData, controller.StringTable.Lookup(MetaData.CellFields.DefaultFieldId_Comment), true))
            {
                SaveCellMetaData();
            }
        }

        private void SaveCellMetaData()
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(this));
            MetaData.CellFields.Save(controller.CurrentCell.MetaData, controller.CurrentCell.Id);
            controller.AvailableCellFields.Refresh();
        }

        private void SaveCaseMetaData(IEnumerable<string> newFieldNames)
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(this));

            using (var lims = Connect.LIMS.New())
            {
                lims.SetCaseMetaData(controller.Case, controller.MetaData.ToLims());
            }

            MetaData.CaseFields.UpdateFields(newFieldNames);
            controller.AvailableCaseFields.Refresh();
        }

        private void OnEditCellResult(object sender, ExecutedRoutedEventArgs e)
        {
            if (CellMetadataPopup.EditSingleField(VisualTreeWalker.FindParentOfType<Window>(this), controller.CurrentCell.MetaData, controller.StringTable.Lookup(MetaData.CellFields.DefaultFieldId_Result), false))
            {
                SaveCellMetaData();
            }
        }

        public void Closed()
        {
            CaseLocking.ReleaseCaseLock(controller.Case);
            controller.SaveStuff();
            controller.Persistants.Persist();
            SaveToolUi();
        }

        private void OnRefocusForced(object sender, ExecutedRoutedEventArgs e)
        {
            ForceFocus();
        }

        private void CanPrintExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller != null && controller.CurrentCell != null && controller.Tool != null;
        }

        private void PrintExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var printImage = controller.Tool.PrintImage;
            if (printImage == null)
                printImage = Print.RenderElementWithImage(ImageRendererExport.GetBitmap(controller.Renderer), (FrameworkElement)imageUi.Child,
                                                                imageUi.ActualWidth, imageUi.ActualHeight);

            Print.PrintVisual(printImage, controller.CaseName, controller.CurrentCell.Name, controller.CurrentCell.Slide.Name, DateTime.Now.ToLongDateString(), controller.CurrentCell.MetaData.Get("Result"));
        }

        public static void FireResetFocus(IInputElement sender)
        {
            ResetFocus.Execute(null, sender);
        }

        private void CanResetFocusExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ResetFocusExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ForceFocus();
        }

        private void OnCaptureHyperlink(object sender, MouseButtonEventArgs e)
        {
            MiscCommands.LaunchBrightfieldCapture.Execute(null, this);
        }

        private void OnImportHyperlink(object sender, MouseButtonEventArgs e)
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(this));
            ImportImagesController.DoImport(controller, VisualTreeWalker.FindParentOfType<Window>(this));
        }

        private void OnCopy(object sender, RoutedEventArgs e)
        {
            var printImage = controller.Tool.PrintImage;
            if (printImage == null)
            {
                //printImage = Print.RenderElement(printVisual); // Export wsiwig, below is whole image
                printImage = Print.RenderElementWithImage(ImageRendererExport.GetBitmap(controller.Renderer), 
                                                          (FrameworkElement)imageUi.Child,
                                                          imageUi.ActualWidth, 
                                                          imageUi.ActualHeight);
            }

            ImageRendererExport.CopyToClipboard(printImage);
        }

        private void OnExport(object sender, RoutedEventArgs e)
        {
            var printImage = controller.Tool.PrintImage;
            if (printImage == null)
                printImage = Print.RenderElementWithImage(ImageRendererExport.GetBitmap(controller.Renderer), (FrameworkElement)imageUi.Child,
                                                                imageUi.ActualWidth, imageUi.ActualHeight);
            ImageRendererExport.SaveAs(printImage);
        }

        private void OnShowRevertConfirm(object sender, RoutedEventArgs e)
        {
            revertConfirmButton.Visibility = revertConfirmButton.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }

        private void OnTrackHighlight(object sender, MouseEventArgs e)
        {
            Point pos = e.GetPosition(mainImage);
            Canvas.SetLeft(mouseHighlight, pos.X);
            Canvas.SetTop(mouseHighlight, pos.Y);

        }

        private void OnFudgeMiddleDrag(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Middle)
            {
                var canvas = VisualTreeWalker.FindParentOfType<StretchOrSizeCanvas>((DependencyObject)sender);
                canvas.HijackMouseDown(e);
            }
        }
    }
}
