﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Windows.Input;
using System.Windows.Controls;

namespace AI.Brightfield
{
    public partial class CaseMetadataPopup
    {
        public static DependencyProperty ControllerProperty = DependencyProperty.Register("Controller", typeof(WorkflowController), typeof(CaseMetadataPopup), new FrameworkPropertyMetadata(OnControllerChanged));

        public CaseMetadataPopup()
        {
            InitializeComponent();
        }

        public WorkflowController Controller
        {
            get { return (WorkflowController)GetValue(ControllerProperty); }
            set { SetValue(ControllerProperty, value); }
        }

        private static void OnControllerChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((CaseMetadataPopup)sender).DataContext = e.NewValue;
        }

        private void OnApplyCaseMetadataTemplate(object sender, RoutedEventArgs e)
        {
            EditCaseMetadata(Controller.Template);
        }

        private void OnEditCaseInfo(object sender, ExecutedRoutedEventArgs e)
        {
            EditCaseMetadata(null);
        }

        private void EditCaseMetadata(MetaData.MetaDataTemplate t)
        {
            try
            {
                //blackout.Visibility = Visibility.Visible;
                var title = Controller.StringTable.Lookup("caseMetadata");
                var addDataWindow = new MetaData.MetaDataEditor(Controller, Controller.AvailableCaseFields, title, t);
                addDataWindow.Owner = VisualTreeWalker.FindParentOfType<Window>(this);

                if (addDataWindow.ShowDialog().Value)
                {
                    SaveCaseMetaData(addDataWindow.NewFields);
                }
                //ForceFocus();
            }
            finally
            {
                // blackout.Visibility = Visibility.Collapsed;
            }
        }

        private void OnAddCaseField(object sender, RoutedEventArgs e)
        {
            if (Controller.Case == null)
                return;

            var fieldname = (string)((Button)sender).Tag;
            if (CellMetadataPopup.EditSingleField(VisualTreeWalker.FindParentOfType<Window>(this), Controller.MetaData, fieldname, false))
            {
                SaveCaseMetaData(new string[] { fieldname });
            }
        }

        private void SaveCaseMetaData(IEnumerable<string> newFieldNames)
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(this));

            using (var lims = Connect.LIMS.New())
            {
                lims.SetCaseMetaData(Controller.Case, Controller.MetaData.ToLims());
            }

            MetaData.CaseFields.UpdateFields(newFieldNames);
            Controller.AvailableCaseFields.Refresh();
        }

        private void CanExecuteTrue(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
    }
}
