﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using AI.MetaData;
using System.Windows.Media.Imaging;
using System.IO;
using System.Xml.Linq;

namespace AI.Brightfield
{
    public class CellColor
    {
        public int Index { get; set; }
        public Brush Brush { get; set; }
        public Brush Border { get; set; }
        public bool IsEmptyColor { get { return Index == White.Index; } }

        public static readonly CellColor[] PossibleColors = new CellColor[]
            {
                new CellColor { Index = 0, Brush = Brushes.LimeGreen, Border = Brushes.Transparent },
                new CellColor { Index = 1, Brush = Brushes.Cyan, Border = Brushes.Transparent },
                new CellColor { Index = 2, Brush = Brushes.Red, Border = Brushes.Transparent },
                new CellColor { Index = 3, Brush = Brushes.White, Border = Brushes.LightGray },
            };

        public static CellColor Green { get { return PossibleColors[0]; } }
        public static CellColor Red { get { return PossibleColors[2]; } }
        public static CellColor Blue { get { return PossibleColors[1]; } }
        public static CellColor White { get { return PossibleColors[3]; } }
    }

    public class Cell : Notifier, MetaData.IMetaDataContainer
    {
        private MetaData.MetaData metaData;

        public Cell(Guid cellId)
        {
            id = cellId;
            metaData = new MetaData.MetaData();
        }

        private string name;
        public string Name 
        {
            get { return name; }
            set { name = value; Notify("Name"); }
        }

        private Guid id;
        public Guid Id
        {
            get { return id; }
        }

        public Slide Slide { get; set; }

        /// <summary>
        /// Tubs db slideid, not lims.
        /// </summary>
        public string SlideId { get; set; }
        public String SlideName { get { return Slide.Name; } }


        // We do cell color whacky like this so that we can do performant linq to sql reads like so:
        // from c in db.Cells select new Cell { CellColorIndex = c.color }
        private int cellColorIndex;
        public int CellColorIndex
        {
            get { return cellColorIndex; }
            set { cellColorIndex = value; Notify("Color"); }
        }

        public CellColor Color
        {
            get { return CellColor.PossibleColors[cellColorIndex]; }
        }

        private bool counted;
        public bool IsCounted
        {
            get { return counted; }
            set { counted = value; Notify("IsCounted", "IsProcessed"); }
        }

        private bool numbered;
        public bool IsNumbered
        {
            get { return numbered; }
            set { numbered = value; Notify("IsNumbered", "IsProcessed"); }
        }

        private bool karyotyped;
        public bool IsKaryotyped
        {
            get { return karyotyped; }
            set { karyotyped = value; Notify("IsKaryotyped", "IsProcessed"); }
        }


        public bool IsProcessed
        {
            get { return counted || numbered || karyotyped; }
        }

        private BitmapSource metThumb, karThumb;
        public BitmapSource MetaphaseThumbnail
        {
            get { return metThumb; }
            set { metThumb = value; Notify("MetaphaseThumbnail", "AspectRatio"); }
        }

        public BitmapSource KaryotypeThumbnail
        {
            get { return karThumb; }
            set { karThumb = value; Notify("KaryotypeThumbnail", "KaryotypeAspectRatio"); }
        }

        private Guid karyotypedblobid = Guid.Empty;
        public Guid KaryotypeThumbnailBlobId
        {
            get { return karyotypedblobid; }
            set { karyotypedblobid = value;}
        }

        public string MetaphaseThumbnailPath { get; set; }
        public string KaryotypeThumbnailPath { get; set; }
        public Guid ImageGroupId { get; set; }

        private Guid metId, rawId;
        public Guid MetImageID
        {
            get { return metId; }
            set { metId = value; Notify("IsFused"); }
        }
		public Guid RawImageID
        {
            get { return rawId; }
            set { rawId = value; }
        }

        public Guid ImageID
        {
            get { return IsFused ? metId : rawId; }
        }

        public bool IsFused
        {
            get { return metId != Guid.Empty; }
        }

        public double AspectRatio
        {
            get
            {
                if (metThumb == null || !(metThumb is BitmapSource))
                    return 1;

                return ((BitmapSource)metThumb).PixelHeight / (double)((BitmapSource)metThumb).PixelWidth;
            }
        }

        public double KaryotypeAspectRatio
        {
            get
            {
                if (karThumb == null || !(karThumb is BitmapSource))
                    return 1;

                return ((BitmapSource)karThumb).PixelHeight / (double)((BitmapSource)karThumb).PixelWidth;
            }
        }

        public MetaData.MetaData MetaData
        {
            get { return metaData; }
            internal set { metaData = value; }
        }


        public string Result
        {
            get { return MetaData.Get("Result"); }
            set { MetaData.Set("Result", value); SaveMetaData(); Notify("Result"); }
        }

        public string Comment
        {
            get { return MetaData.Get("Comment"); }
            set { MetaData.Set("Comment", value); SaveMetaData(); Notify("Comment"); }
        }

        public string CellLine
        {
            get { return MetaData.Get("Cell Line"); }
            set { MetaData.Set("Cell Line", value); SaveMetaData(); Notify("CellLine"); }
        }

        public string BandQuality
        {
            get { return MetaData.Get("Band Quality"); }
            set { MetaData.Set("Band Quality", value); SaveMetaData(); Notify("BandQuality"); }
        }

        private void SaveMetaData()
        {
            CellFields.Save(metaData, id);
        }

        internal void NotifyDefaultMetaDataFields()
        {
            Notify("Result");
            Notify("Comment");
            Notify("CellLine");
            Notify("BandQuality");
        }

        public bool AreTemplatesSupported { get { return false; } }
        public MetaDataTemplate Template { get { return null; } set { } }
        public FileInfo TemplateFile { get { return null; } }
        public int KaryotypeThreshold { get; set; }

        public XElement ToXml()
        {
            return new XElement("CellStatus",
                new XAttribute("analysed", IsProcessed),
                new XAttribute("cellId", Id),
                new XAttribute("cellName", Name),
                new XAttribute("color", CellColorIndex),
                new XAttribute("counted", IsCounted),
                new XAttribute("imageGroup", ImageGroupId),
                new XAttribute("karyotyped", IsKaryotyped),
                new XAttribute("karyotypedBlobId", IsKaryotyped ? KaryotypeThumbnailBlobId : Guid.Empty),
                new XAttribute("threshold", KaryotypeThreshold),
                new XAttribute("slideId", SlideId));
        }

        public static Cell FromXml(XElement cell)
        {
            return new Cell(Guid.Parse(cell.Attribute("cellId").Value)) 
            { 
                Name = cell.Attribute("cellName").Value,
                CellColorIndex = int.Parse(cell.Attribute("color").Value),
                IsCounted = bool.Parse(cell.Attribute("counted").Value),
                ImageGroupId =  Guid.Parse(cell.Attribute("imageGroup").Value),
                IsKaryotyped = bool.Parse(cell.Attribute("karyotyped").Value),
                KaryotypeThumbnailBlobId = Guid.Parse(cell.Attribute("karyotypedBlobId").Value),
                KaryotypeThreshold = int.Parse(cell.Attribute("threshold").Value),
//                SlideId = Guid.Parse(cell.Attribute("slideId").Value)
                SlideId = cell.Attribute("slideId").Value
            };
        }
    }
}
