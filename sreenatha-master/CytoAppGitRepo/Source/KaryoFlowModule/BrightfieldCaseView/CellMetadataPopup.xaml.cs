﻿using System;
using System.Linq;
using System.Windows;
using System.Collections.Generic;
using System.Windows.Input;
using System.Windows.Controls;

namespace AI.Brightfield
{
    public partial class CellMetadataPopup
    {
        public static DependencyProperty ControllerProperty = DependencyProperty.Register("Controller", typeof(WorkflowController), typeof(CellMetadataPopup), new FrameworkPropertyMetadata(OnControllerChanged));

        public CellMetadataPopup()
        {
            InitializeComponent();
        }

        public WorkflowController Controller
        {
            get { return (WorkflowController)GetValue(ControllerProperty); }
            set { SetValue(ControllerProperty, value); }
        }

        private static void OnControllerChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((CellMetadataPopup)sender).DataContext = e.NewValue;
        }

        private void CanEditCellInfoExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Controller != null && Controller.CurrentCell != null;
        }

        private void OnEditCellInfo(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            try
            {
                //blackout.Visibility = Visibility.Visible;
                var title = Controller.StringTable.Lookup("metadata") + ": " + Controller.CurrentCell.Name;
                var addDataWindow = new MetaData.MetaDataEditor(Controller.CurrentCell, Controller.AvailableCellFields, title);
                addDataWindow.Owner = VisualTreeWalker.FindParentOfType<Window>(this);

                if (addDataWindow.ShowDialog().Value)
                {
                    SaveCellMetaData();
                }

                //ForceFocus();
            }
            finally
            {
                //blackout.Visibility = Visibility.Collapsed;
            }
        }

        private void SaveCellMetaData()
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(this));
            MetaData.CellFields.Save(Controller.CurrentCell.MetaData, Controller.CurrentCell.Id);
            Controller.AvailableCellFields.Refresh();
        }

        private void CanExecuteTrue(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
 
        private void OnAddCellField(object sender, RoutedEventArgs e)
        {
            if (Controller.CurrentCell == null)
                return;

            var fieldname = (string)((Button)sender).Tag;
            if (CellMetadataPopup.EditSingleField(VisualTreeWalker.FindParentOfType<Window>(this), Controller.CurrentCell.MetaData, fieldname, false))
            {
                SaveCellMetaData();
            }
        }

        public static bool EditSingleField(Window parentWindow, MetaData.MetaData metaData, string fieldname, bool append)
        {
            //blackout.Visibility = Visibility.Visible;

            try
            {
                var field = metaData.Fields.Where(f => f.Field == fieldname).FirstOrDefault();
                bool isNew = false;
                string originalValue = "";

                if (field == null)
                {
                    isNew = true;
                    field = metaData.New();
                    field.Field = fieldname;
                }
                else
                {
                    originalValue = field.Value;
                }

                MetaData.EditSingleMetaDataFieldWindow wnd = new MetaData.EditSingleMetaDataFieldWindow(field, append);
                wnd.Owner = VisualTreeWalker.FindParentOfType<Window>(parentWindow);

                var dialogResult = wnd.ShowDialog().GetValueOrDefault(false);
                if (dialogResult == false)
                {
                    if (isNew)
                    {
                        metaData.Delete(field);
                    }
                    else
                    {
                        field.Value = originalValue;
                    }
                }

                //ForceFocus();
                return dialogResult;
            }
            finally
            {
                //blackout.Visibility = Visibility.Collapsed;
            }
        }
    }
}
