﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Linq;
using System.Windows.Input;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Collections.ObjectModel;
using AI.MetaData;
using System.Threading;
using System.Windows.Threading;
using Microsoft.Win32;

namespace AI.Brightfield
{
    public partial class CellOrganizer
    {
        #region DataContext class

        public class CellMetaDataFilter : Notifier
        {
            private bool filterOn;
            public string Field { get; set; }
            public bool FilterOn { get { return filterOn; } set { filterOn = value; Notify("FilterOn"); } }
        }

        internal class DC : Notifier
        {
            private ObservableCollection<CellMetaDataFilter> metaDataFields = new ObservableCollection<CellMetaDataFilter>();
 
            public DC(WorkflowController controller, bool reportMode)
            {
                Controller = controller;
//                TotalCells = controller.AllCells.Count();
                ReportMode = reportMode;

                if (reportMode)
                {
                    UpdateNumberOfScratchPads();
                }

                UpdateCellMetaData();
            }

            public void UpdateNumberOfScratchPads()
            {
                //using (var db = new Connect.Database())
                //{
                //    NumberOfScratchPads = db.ScratchPads.Count(p => p.caseId == Controller.CaseId);
                //    Notify("NumberOfScratchPads");
                //}
            }

            public int NumberOfScratchPads { get; private set; }
            public bool ReportMode { get; private set; }
            public WorkflowController Controller { get; private set; }
            public CellVisibility Visibility { get { return Controller.CellVisibility; } }
            public IEnumerable<CellMetaDataFilter> MetaDataFilters { get { return metaDataFields; } }

            private Cell selected;
            public Cell SelectedCell
            {
                get { return selected; }
                set { selected = value; Notify("SelectedCell"); }
            }

            public IEnumerable<Cell> FilteredCells
            {
                get 
                {
                    var f = Visibility.FilterCells(Controller.AllCells);
                    NumberVisibleCells = f.Count();
                    Notify("NumberVisibleCells");
                    return f;
                }
            }

            private double gridSize;
            public double GridSize
            {
                get { return gridSize; }
                set { gridSize = value; Notify("GridSize"); }
            }

            public void Refresh()
            {
                Notify("FilteredCells", "NumberVisibleCells", "TotalCells");
            }

            public int NumberVisibleCells { get; set; }
            public int TotalCells { get { return Controller.AllCells.Count(); } }

            public IEnumerable<CellVisibility.VisSlide> Slides
            {
                get
                {
                    return Visibility.Slides;
                }
            }

            public void HideCellsWithoutThisField(string fieldname)
            {
                Visibility.AddMetadataFieldRestraint(fieldname);
            }

            internal void RemoveFieldRestriction(string fieldname)
            {
                Visibility.RemoveMetadataFieldRestraint(fieldname);
            }

            private BitmapSource bigImage;
            public BitmapSource BigImage
            {
                get { return bigImage; }
                set { bigImage = value; Notify("BigImage"); }
            }

            internal void ShowAll()
            {
                foreach (var field in metaDataFields)
                    field.FilterOn = false;

                Visibility.ShowAll();
            }

            internal void UpdateCellMetaData()
            {
                foreach (var field in Controller.AvailableCellFields.FieldNames)
                {
                    if (metaDataFields.Where(mdf => mdf.Field == field).Count() == 0)
                        metaDataFields.Add(new CellMetaDataFilter() { Field = field });
                }
                foreach (var metaField in metaDataFields)
                {
                    metaField.FilterOn = Visibility.IsFieldRequired(metaField.Field);
                }
            }
        }

        #endregion

        private static readonly string GridSizeKey = "GridSize";

        private DC dc;
        private WorkflowController controller;


        public CellOrganizer(WorkflowController controller, bool organizeMode)
        {
            this.controller = controller;
            MoveToSelectedCellOnClose = organizeMode;

            dc = new DC(controller, !organizeMode)
            {
                SelectedCell = controller.CurrentCell,
                GridSize = controller.Persistants.GetDouble(GridSizeKey, 200)
            };

            dc.Refresh();

            DataContext = dc;
            InitializeComponent();

            if (organizeMode)
            {
                mainList.Style = (Style)FindResource("OrganizeStyle");
            }
            else
            {
                mainList.Style = (Style)FindResource("ReportStyle");
            }

            WindowLocations.Handle(this);

            Closing += (s, e) => e.Cancel = deletingProgress.Visibility == Visibility.Visible;
        }

        public IEnumerable<Cell> VisibleCells
        {
            get { return dc.FilteredCells; }
        }

        public Cell SelectedCell
        {
            get { return dc.SelectedCell; }
        }

        private void OnOK(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void OnPrint(object sender, System.Windows.RoutedEventArgs e)
        {
            PrintPreview.Print(controller, dc.FilteredCells.ToList());
        }

        private void OnGetBetterThumb(object sender, System.Windows.RoutedEventArgs e)
        {
            Button b = (Button)sender;
            b.Visibility = Visibility.Collapsed;

            Cell cell = (Cell)b.Tag;
            dc.Controller.LoadFullsizeThumb(cell, Dispatcher);
        }

        private IEnumerable<Cell> SelectedCells
        {
            get
            {
                foreach (object o in mainList.SelectedItems)
                    yield return (Cell)o;
            }
        }

        private IEnumerable<Cell> GetSelectedCellsOrThis(Cell cell)
        {
            var selected = SelectedCells;
            if (selected.Contains(cell))
                return selected;
            else
                return new Cell[] { cell };
        }

        private void OnSetCellGreen(object sender, System.Windows.RoutedEventArgs e)
        {
            dc.Controller.SetCellColor(GetSelectedCellsOrThis(((FrameworkElement)sender).Tag as Cell), CellColor.Green);
        }

        private void OnSetCellRed(object sender, System.Windows.RoutedEventArgs e)
        {
            dc.Controller.SetCellColor(GetSelectedCellsOrThis(((FrameworkElement)sender).Tag as Cell), CellColor.Red);
        }

        private void OnSetCellBlue(object sender, System.Windows.RoutedEventArgs e)
        {
            dc.Controller.SetCellColor(GetSelectedCellsOrThis(((FrameworkElement)sender).Tag as Cell), CellColor.Blue);
        }

        private void OnSetCellWhite(object sender, System.Windows.RoutedEventArgs e)
        {
            dc.Controller.SetCellColor(GetSelectedCellsOrThis(((FrameworkElement)sender).Tag as Cell), CellColor.White);
        }

        private void OnSortGreenFirst(object sender, RoutedEventArgs e)
        {
            ChangeSort(CellSorts.GreenFirst);
        }

        private void OnSortGreenLast(object sender, RoutedEventArgs e)
        {
            ChangeSort(CellSorts.GreenLast);
        }

        private void ChangeSort(Comparison<Cell> sorter)
        {
            dc.Visibility.Sorter = sorter;
            RefreshCells();
            sortPopup.IsOpen = false;
        }

        private void OnSortKaryFirst(object sender, RoutedEventArgs e)
        {
            ChangeSort(CellSorts.KaryotypesFirst);
        }

        private void OnSortKaryLast(object sender, RoutedEventArgs e)
        {
            ChangeSort(CellSorts.KaryotypesLast);
        }

        private void OnShowSortPopup(object sender, RoutedEventArgs e)
        {
            sortPopup.IsOpen = true;
        }

        private void OnSliderChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if(mainList != null)
                mainList.ScrollIntoView(dc.SelectedCell);

            controller.Persistants.Set(GridSizeKey, e.NewValue);
        }

        private void OnSortNumberFirst(object sender, RoutedEventArgs e)
        {
            ChangeSort(CellSorts.NumberedFirst);
        }

        private void OnSortNumberLast(object sender, RoutedEventArgs e)
        {
            ChangeSort(CellSorts.NumberedLast);
        }

        private void OnSortCountFirst(object sender, RoutedEventArgs e)
        {
            ChangeSort(CellSorts.CountedFirst);
        }

        private void OnSortCountLast(object sender, RoutedEventArgs e)
        {
            ChangeSort(CellSorts.CountedLast);
        }

        private void OnSortProcessedFirst(object sender, RoutedEventArgs e)
        {
            ChangeSort(CellSorts.ProcessedFirst);
        }

        private void OnSortProcessedLast(object sender, RoutedEventArgs e)
        {
            ChangeSort(CellSorts.ProcessedLast);
        }

        private void OnSortMetaData(object sender, RoutedEventArgs e)
        {
            var field = (string)((Button)sender).Tag;
            ChangeSort((a, b) => CompareCellMetaData(a, b, field));
        }

        private void OnSortMetaDataDesc(object sender, RoutedEventArgs e)
        {
            var field = (string)((Button)sender).Tag;
            ChangeSort((a, b) => CompareCellMetaData(b, a, field));
        }

        private int CompareCellMetaData(Cell a, Cell b, string field)
        {
            var fieldValueA = a.MetaData.Fields.Where(f => f.Field == field).FirstOrDefault();
            var fieldValueB = b.MetaData.Fields.Where(f => f.Field == field).FirstOrDefault();
            var valueA = (fieldValueA == null || fieldValueA.Value == null) ? null : fieldValueA.Value;
            var valueB = (fieldValueB == null || fieldValueB.Value == null) ? null : fieldValueB.Value;

            if (valueA == null && valueB == null)
            {
                return 0;
            }
            else if (valueA == null)
            {
                return 1;
            }
            else if (valueB == null)
            {
                return -1;
            }
            else
            {
                return string.Compare(valueA, valueB);
            }
        }


        private void OnSortSlide(object sender, RoutedEventArgs e)
        {
            ChangeSort(CellSorts.Slide);
        }

        private void OnSortSlideDesc(object sender, RoutedEventArgs e)
        {
            ChangeSort(CellSorts.SlideDesc);
        }

        private void OnChooseSlides(object sender, RoutedEventArgs e)
        {
            slides.Visibility = slides.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }

        private void OnSlideChanged(object sender, RoutedEventArgs e)
        {
            RefreshCells();
        }

        private void RefreshCells()
        {
            dc.Refresh();
            
            if(mainList != null && dc.FilteredCells.Count() > 0)
                mainList.SelectedIndex = 0;
        }

        DateTime last = DateTime.MinValue;
        object lastSender;
        private void OnDoubleClickMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (dc.ReportMode)
                return;

            if (sender != lastSender)
            {
                last = DateTime.Now;
                lastSender = sender;
                return;
            }

            var now = DateTime.Now;
            if ((now - last).TotalSeconds < 0.5)
            {
                DialogResult = true;
            }

            last = now;
            lastSender = sender;
        }

        private void OnShowAll(object sender, RoutedEventArgs e)
        {
            dc.ShowAll();
            RefreshCells();
        }

        private void OnPreviewMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            if(Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                e.Handled = true;

                var n = dc.GridSize + Math.Sign(e.Delta) * 20;
                dc.GridSize = Math.Max(thumbSizeSlider.Minimum, Math.Min(thumbSizeSlider.Maximum, n));
            }
        }

        private void OnShowScratchPads(object sender, RoutedEventArgs e)
        { 
            ScratchPadWindow scratchwnd = new ScratchPadWindow(dc.Controller);
            scratchwnd.Owner = this;
            scratchwnd.ShowDialog();
            dc.UpdateNumberOfScratchPads();
        }

        private void OnAddMetadataRestraint(object sender, RoutedEventArgs e)
        {
            dc.HideCellsWithoutThisField((string)((FrameworkElement)sender).Tag);
            RefreshCells();
        }

        private void OnRemoveMetadataRestraint(object sender, RoutedEventArgs e)
        {
            dc.RemoveFieldRestriction((string)((FrameworkElement)sender).Tag);
            RefreshCells();
        }

        private void OnEditCaseDetails(object sender, MouseButtonEventArgs e)
        {
            var title = controller.StringTable.Lookup("caseMetadata");
            MetaData.MetaDataEditor ed = new AI.MetaData.MetaDataEditor(dc.Controller, dc.Controller.AvailableCaseFields, title);
            ed.Owner = this;

            if (ed.ShowDialog().Value)
            {
                Connect.ConnectionTest.Test(this);

                using (var lims = Connect.LIMS.New())
                {
                    lims.SetCaseMetaData(dc.Controller.Case, dc.Controller.MetaData.ToLims());
                }

                MetaData.CaseFields.UpdateFields(ed.NewFields);
                dc.Controller.AvailableCaseFields.Refresh();
            }
        }

        public bool MoveToSelectedCellOnClose
        {
            get;
            set;
        }

        private void OnSelectCellForViewing(object sender, MouseButtonEventArgs e)
        {
            mainList.SelectedItem = ((FrameworkElement)sender).Tag;
            MoveToSelectedCellOnClose = true;
            DialogResult = true;
        }

        private void OnDismissBigImage(object sender, MouseButtonEventArgs e)
        {
            dc.BigImage = null;
        }

        private void ShowBigImageKar(object sender, MouseButtonEventArgs e)
        {
            var c = (Cell)((FrameworkElement)sender).Tag;

            var relativeSlideId = controller.CaseName + ";" + c.SlideId;
            var relativeCellId = relativeSlideId + ";" + c.Id;

            var dataSource = DataSourceFactory.GetDataSource();
            c.KaryotypeThumbnailPath = c.Id.ToString() + ".kar.jpg";
            using (var kstream = dataSource.LoadStream(relativeCellId, c.KaryotypeThumbnailPath))
            {
                var bitmap = ImageFromBitmapSource.LoadBitmapImageFromStream(kstream);
                dc.BigImage = bitmap;
            }
        }

        private void ShowBigImageMet(object sender, MouseButtonEventArgs e)
        {
            var c = (Cell)((FrameworkElement)sender).Tag;

            var relativeSlideId = controller.CaseName + ";" + c.SlideId;

            var dataSource = DataSourceFactory.GetDataSource();
            c.MetaphaseThumbnailPath = c.ImageID.ToString() + ".jpg";
            using (var mstream = dataSource.LoadStream(relativeSlideId, c.MetaphaseThumbnailPath))
            {
                var bitmap = ImageFromBitmapSource.LoadBitmapImageFromStream(mstream);
                dc.BigImage = bitmap;
            }
        }

        private void OnEditCellMetadata(object sender, MouseButtonEventArgs e)
        {
            var c = (Cell)((FrameworkElement)sender).Tag;
            var title = controller.StringTable.Lookup("metadata") + ": " + c.Name;
            MetaData.MetaDataEditor ed = new AI.MetaData.MetaDataEditor(c, controller.AvailableCellFields, title);
            ed.Owner = this;
            if (ed.ShowDialog().Value)
            {
                Connect.ConnectionTest.Test(this);

                using (var db = new Connect.Database())
                {
                    MetaData.CellFields.Save(c.MetaData, c.Id);
                    controller.AvailableCellFields.Refresh();
                }

                dc.UpdateCellMetaData();
            }
        }

        private void OnUpload(object sender, RoutedEventArgs e)
        {
            var karyotypedCells = controller.AllCells.Where(c => c.IsKaryotyped);
            var imagePairs = from c in karyotypedCells
                             where c.MetaphaseThumbnail != null && c.KaryotypeThumbnail != null
                             select new CellImagePair
                             {
                                 Name = c.Name,
                                 Slide = c.Slide,
                                 Metaphase = c.MetaphaseThumbnail,
                                 Karyotype = c.KaryotypeThumbnail
                             };

            using (var lims = Connect.LIMS.New())
            {
                lims.CompleteCase(controller.Case, imagePairs);
            }
        }

        private void OnUndoFusing(object sender, RoutedEventArgs e)
        {
            List<Cell> selected = new List<Cell>();
            foreach (Cell c in mainList.SelectedItems)
                selected.Add(c);

            DeleteStuff deleteWindow = new DeleteStuff(() =>
            {
                controller.RollbackCellToRawImage(selected);
            });

            deleteWindow.Owner = this;
            if (deleteWindow.ShowDialog().GetValueOrDefault(false))
            {
                dc.Refresh();
            }
        }

        private void OnDeleteCell(object sender, RoutedEventArgs e)
        {
            List<Cell> selected = new List<Cell>();
            foreach (Cell c in mainList.SelectedItems)
                selected.Add(c);

            deletingProgress.Visibility = Visibility.Visible;

            DeleteStuff deleteWindow = new DeleteStuff(() =>
            {
                controller.DeleteCells(selected);
            });

            deleteWindow.Owner = VisualTreeWalker.FindParentOfType<Window>(this);

            if (deleteWindow.ShowDialog().GetValueOrDefault(false))
            {
                controller.CleanupAfterDeleteCells(selected);
                dc.Refresh();

                if (controller.WorkingCells.Count() == 0)
                {
                    Close();
                }
            }

            deletingProgress.Visibility = Visibility.Collapsed;
            RefreshCells();
        }

        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete && mainList.SelectedItems.Count > 0)
            {
                OnDeleteCell(null, null);
            }
            else if (e.Key == Key.Escape)
            {
                MoveToSelectedCellOnClose = false;
                Close();
            }
        }

        private void OnImport(object sender, RoutedEventArgs e)
        {
            if (ImportImagesController.DoImport(controller, this))
            {
                dc.Refresh();
            }
        }
    }

    public class AspectRatioConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue)
                return 1.0;

            double aspectRatio = (double)values[0];
            double width = (double)values[1];

            if (aspectRatio < 0.1 || width < 0.1)
                return 1.0;

            return Math.Max(70, width * aspectRatio);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }


}
