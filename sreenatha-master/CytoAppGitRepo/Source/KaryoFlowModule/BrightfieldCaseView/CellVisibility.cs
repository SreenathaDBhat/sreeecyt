﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace AI.Brightfield
{
    public static class CellSorts
    {
        public static Comparison<Cell> GreenFirst = (a, b) =>
        {
            if (a.CellColorIndex == b.CellColorIndex)
            {
                return a.Name.CompareTo(b.Name);
            }
            else
            {
                return a.CellColorIndex.CompareTo(b.CellColorIndex);
            }
        };

        public static Comparison<Cell> GreenLast = (a, b) =>
        {
            if (a.CellColorIndex == b.CellColorIndex)
            {
                return a.Name.CompareTo(b.Name);
            }
            else
            {
                return b.CellColorIndex.CompareTo(a.CellColorIndex);
            }
        };

        public static Comparison<Cell> KaryotypesFirst = (a, b) =>
        {
            if (a.IsKaryotyped == b.IsKaryotyped)
            {
                return NumberedFirst(a, b);
            }
            else
            {
                return b.IsKaryotyped.CompareTo(a.IsKaryotyped);
            }
        };

        public static Comparison<Cell> KaryotypesLast = (a, b) =>
        {
            if (a.IsKaryotyped == b.IsKaryotyped)
            {
                return NumberedLast(a, b);
            }
            else
            {
                return a.IsKaryotyped.CompareTo(b.IsKaryotyped);
            }
        };

        public static Comparison<Cell> CountedFirst = (a, b) =>
        {
            if (a.IsCounted == b.IsCounted)
            {
                return GreenFirst(a, b);
            }
            else
            {
                return b.IsCounted.CompareTo(a.IsCounted);
            }
        };

        public static Comparison<Cell> CountedLast = (a, b) =>
        {
            if (a.IsCounted == b.IsCounted)
            {
                return GreenFirst(a, b);
            }
            else
            {
                return a.IsCounted.CompareTo(b.IsCounted);
            }
        };

        public static Comparison<Cell> NumberedFirst = (a, b) =>
        {
            if (a.IsNumbered == b.IsNumbered)
            {
                return CountedFirst(a, b);
            }
            else
            {
                return b.IsNumbered.CompareTo(a.IsNumbered);
            }
        };

        public static Comparison<Cell> NumberedLast = (a, b) =>
        {
            if (a.IsNumbered == b.IsNumbered)
            {
                return CountedLast(a, b);
            }
            else
            {
                return a.IsNumbered.CompareTo(b.IsNumbered);
            }
        };

        public static Comparison<Cell> ProcessedFirst = (a, b) =>
        {
            if (a.IsProcessed == b.IsProcessed)
            {
                return KaryotypesFirst(a, b);
            }
            else
            {
                return b.IsProcessed.CompareTo(a.IsProcessed);
            }
        };

        public static Comparison<Cell> ProcessedLast = (a, b) =>
        {
            if (a.IsProcessed == b.IsProcessed)
            {
                return KaryotypesLast(a, b);
            }
            else
            {
                return a.IsProcessed.CompareTo(b.IsProcessed);
            }
        };

        public static Comparison<Cell> Slide = (a, b) =>
        {
            if (a.Slide.Name == b.Slide.Name)
            {
                return GreenFirst(a, b);
            }
            else
            {
                return a.Slide.Name.CompareTo(b.Slide.Name);
            }
        };

        public static Comparison<Cell> SlideDesc = (a, b) =>
        {
            if (a.Slide.Name == b.Slide.Name)
            {
                return a.Name.CompareTo(b.Name);
            }
            else
            {
                return b.Slide.Name.CompareTo(a.Slide.Name);
            }
        };
    }

    public class CellVisibility : Notifier
    {
        #region bits
        public class VisSlide : Notifier
        {
            private CellVisibility visObject;
            public VisSlide(CellVisibility visibility)
            {
                visObject = visibility;
            }

            public Slide Slide { get; set; }

            private bool visible = true;
            public bool IsVisible
            {
                get { return visible; }
                set { visible = value; Notify("IsVisible"); visObject.NotifyProperties(); }
            }
        }

        #endregion

        private bool showRed, showBlue, showGreen, showWhite;
        private bool requireCounted, requireNumbered, requireKaryotyped, requireProcessed;
        private List<VisSlide> slides;
        private List<string> requiredFields;

        public CellVisibility(IEnumerable<Slide> slides)
        {
            showRed = showGreen = showBlue = showWhite = true;
            requireCounted = requireNumbered = requireKaryotyped = requireProcessed = false;
            requiredFields = new List<string>();
            this.slides = (from s in slides
                           select new VisSlide(this)
                           {
                               Slide = s,
                           }).ToList();
        }

        public Comparison<Cell> Sorter { get; set; }
        public IEnumerable<VisSlide> Slides { get { return slides; } }

        public IEnumerable<Cell> FilterCells(IEnumerable<Cell> allCells)
        {
            if (Sorter != null)
            {
                var list = allCells.Where(c => PassesFilters(c)).ToList();
                list.Sort(Sorter);
                return list;
            }
            else
            {
                return allCells.Where(c => PassesFilters(c));
            }
        }

        private bool PassesFilters(Cell c)
        {
            if (IsRejected(c.Slide))
                return false;

            if (!showRed && c.CellColorIndex == CellColor.Red.Index)
                return false;
            if (!showGreen && c.CellColorIndex == CellColor.Green.Index)
                return false;
            if (!showBlue && c.CellColorIndex == CellColor.Blue.Index)
                return false;
            if (!showWhite && c.CellColorIndex == CellColor.White.Index)
                return false;

            if (requireProcessed && !c.IsKaryotyped && !c.IsCounted && !c.IsNumbered)
                return false;
            if (requireCounted && !c.IsCounted)
                return false;
            if (requireNumbered && !c.IsNumbered)
                return false;
            if (requireKaryotyped && !c.IsKaryotyped)
                return false;

            if (requiredFields.Count != 0)
            {
                foreach (var f in requiredFields)
                {
                    if (c.MetaData.Fields.Where(m => m.Field == f).FirstOrDefault() == null)
                        return false;
                }
            }

            return true;
        }

        private bool IsRejected(Slide slide)
        {
            foreach (var s in slides)
            {
                if (s.Slide == slide)
                {
                    return !s.IsVisible;
                }
            }

            return false;
        }

        public bool ShowRed
        {
            get { return showRed; }
            set { showRed = value; Notify("ShowRed", "AdvancedFilterInEffect"); }
        }

        public bool ShowBlue
        {
            get { return showBlue; }
            set { showBlue = value; Notify("ShowBlue", "AdvancedFilterInEffect"); }
        }

        public bool ShowGreen
        {
            get { return showGreen; }
            set { showGreen = value; Notify("ShowGreen", "AdvancedFilterInEffect"); }
        }

        public bool ShowWhite
        {
            get { return showWhite; }
            set { showWhite = value; Notify("ShowWhite", "AdvancedFilterInEffect"); }
        }

        public bool RequireCounted
        {
            get { return requireCounted; }
            set 
            { 
                requireCounted = value;

                if (requireCounted)
                    requireProcessed = false;

                Notify("RequireCounted", "RequireProcessed", "AdvancedFilterInEffect");
            }
        }

        public bool RequireNumbered
        {
            get { return requireNumbered; }
            set 
            {
                requireNumbered = value;

                if (requireNumbered)
                    requireProcessed = false;

                Notify("RequireNumbered", "RequireProcessed", "AdvancedFilterInEffect");
            }
        }

        public bool RequireKaryotyped
        {
            get { return requireKaryotyped; }
            set 
            {
                requireKaryotyped = value;

                if (requireKaryotyped)
                    requireProcessed = false;

                Notify("RequireKaryotyped", "RequireProcessed", "AdvancedFilterInEffect");
            }
        }

        public bool RequireProcessed
        {
            get { return requireProcessed; }
            set 
            { 
                requireProcessed = value; 
                requireCounted = false;
                requireNumbered = false;
                requireKaryotyped = false;

                Notify("RequireProcessed", "RequireNumbered", "RequireKaryotyped", "RequireCounted", "AdvancedFilterInEffect");
            }
        }

        public void ShowAll()
        {
            showRed = showGreen = showBlue = showWhite = true;
            requireCounted = requireNumbered = requireKaryotyped = requireProcessed = false;
            requiredFields.Clear();
            foreach (var s in slides)
                s.IsVisible = true;

            Notify("ShowRed", "ShowBlue", "ShowGreen", "ShowWhite", "RequireCounted", "RequireNumbered", "RequireKaryotyped", "RequireProcessed", "AdvancedFilterInEffect");
        }

        public void AddMetadataFieldRestraint(string fieldname)
        {
            if (!requiredFields.Contains(fieldname))
            {
                requiredFields.Add(fieldname);
                Notify("AdvancedFilterInEffect");
            }
        }

        public void RemoveMetadataFieldRestraint(string fieldname)
        {
            requiredFields.Remove(fieldname);
            Notify("AdvancedFilterInEffect");
        }

        public bool IsFieldRequired(string fieldName)
        {
            return requiredFields.Contains(fieldName);
        }

        public bool AdvancedFilterInEffect
        {
            get
            {
                return requiredFields.Count > 0 ||
                       RequireCounted || RequireNumbered || RequireKaryotyped || RequireProcessed ||
                       !ShowRed || !ShowGreen || !ShowBlue || !showWhite ||
                       slides.Where(s => !s.IsVisible).Count() > 0;
            }
        }

        public void NotifyProperties()
        {
            Notify("ShowRed", "ShowBlue", "ShowGreen", "ShowWhite", 
                   "RequireCounted", "RequireNumbered", "RequireKaryotyped", "RequireProcessed", 
                   "AdvancedFilterInEffect");
        }
    }
}
