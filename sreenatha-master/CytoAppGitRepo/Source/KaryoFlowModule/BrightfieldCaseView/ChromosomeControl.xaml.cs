﻿using System.Windows.Data;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Effects;

namespace AI.Brightfield
{
    public partial class ChromosomeControl
    {
        public static readonly DependencyProperty ChromosomeProperty = DependencyProperty.Register("Chromosome", typeof(KaryotypeChromosome), typeof(ChromosomeControl), new FrameworkPropertyMetadata(OnChromChanged));

        public ChromosomeControl()
        {
            InitializeComponent();
        }

        static void OnChromChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            //((ChromosomeControl)sender).DataContext = e.NewValue;
        }

        public KaryotypeChromosome Chromosome
        {
            get { return (KaryotypeChromosome)GetValue(ChromosomeProperty); }
            set { SetValue(ChromosomeProperty, value); }
        }

        private void OnMouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Chromosome.IsMouseOver = true;
        }

        private void OnMouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Chromosome.IsMouseOver = false;
        }
    }


    public class ApplyEffectWhenTrue : IValueConverter
    {
        public Effect Effect { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)value ? Effect : null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
