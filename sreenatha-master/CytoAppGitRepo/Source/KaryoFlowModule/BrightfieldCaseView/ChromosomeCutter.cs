﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Ink;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace AI.Brightfield
{
    public static class ChromosomeCutter
    {
        public static KaryotypeChromosome CutStroke(byte[] imageBytes, int imageWidth, int imageHeight, Stroke stroke, Guid cellId)
        {
            int margin = 10;
            var bounds = stroke.GetBounds();
            double offsetX = -(Math.Max(0, bounds.Left) - margin);
            double offsetY = -(Math.Max(0, bounds.Top) - margin);
            bounds.Inflate(margin, margin);

            int left = (int)Math.Max(0, bounds.X);
            int top = (int)Math.Max(0, bounds.Y);
            int right = (int)Math.Min(imageWidth, bounds.Right);
            int Bottom = (int)Math.Min(imageHeight, bounds.Bottom);
            var intBounds = new Int32Rect(left, top, right-left, Bottom-top);
            
            KaryotypeMask mask = new KaryotypeMask(intBounds.Width, intBounds.Height);
            StrokeDrawing.RenderStroke(stroke, Brushes.Black, null, (RenderTargetBitmap)mask.Image, offsetX, offsetY);

            var newItem = KaryotypeChromosome.CreateBare(cellId);
            newItem.DisplayImage = KaryotypeChromosome.MaskBitmap(intBounds, mask, imageBytes, imageWidth);
            newItem.SetSize(intBounds.Width, intBounds.Height);
            newItem.SetDefaultMidPoint(intBounds.Width, intBounds.Height);
            newItem.Bounds = new Int32Rect((int)bounds.X, (int)bounds.Y, (int)bounds.Width, (int)bounds.Height);
            OrientFromStroke(newItem, stroke);

            return newItem;
        }

        private static unsafe void OrientFromStroke(KaryotypeChromosome k, Stroke stroke)
        {
            var points = (from p in stroke.StylusPoints
                          select new Point(p.X, p.Y)).ToArray();

            Point a, b;
            FindMostSeparatedPoints(points, out a, out b);
            double angleToVertical = FindAngleToVertical(a, b);
            double width = FindWidth(points, a, b) + stroke.DrawingAttributes.Width;
            var length = Math.Sqrt(((b.X - a.X) * (b.X - a.X)) + ((b.Y - a.Y) * (b.Y - a.Y))) + stroke.DrawingAttributes.Width;

            k.RotationToVertical = angleToVertical;
            k.SetSize(width, length);
        }

        private static double FindAngleToVertical(Point a, Point b)
        {
            double opposite = Math.Abs(a.Y - b.Y);
            double adjacent = Math.Abs(a.X - b.X);
            double angle = Math.Atan(opposite / adjacent);
            angle /= (Math.PI / 180);

            //which quadrant?
            if (a.X <= b.X && a.Y <= b.Y)
                return 90 - angle;

            if (a.X <= b.X && a.Y > b.Y)
                return 90 + angle;

            if (a.X > b.X && a.Y <= b.Y)
                return 270 + angle;

            return 270 - angle;
        }

        private static void FindMostSeparatedPoints(Point[] points, out Point a, out Point b)
        {
            a = points.First();
            b = points.Last();

            double biggestDistance = 0;

            for (int i = 0; i < points.Length - 2; ++i)
            {
                var pi = points[i];

                for (int j = i + 1; j < points.Length - 1; ++j)
                {
                    var pj = points[j];
                    double distance = ((pi.X - pj.X) * (pi.X - pj.X)) + ((pi.Y - pj.Y) * (pi.Y - pj.Y));
                    if (distance > biggestDistance)
                    {
                        biggestDistance = distance;
                        a = pi;
                        b = pj;
                    }
                }
            }
        }

        private static double FindWidth(Point[] points, Point a, Point b)
        {
            double x1 = a.X;
            double y1 = a.Y;
            double z1 = 0;

            double x2 = b.X;
            double y2 = b.Y;
            double z2 = 0;

            double x3 = a.X;
            double y3 = a.Y;
            double z3 = 1;

            // equation for plane stuff...
            double A = (y1 * (z2 - z3)) + (y2 * (z3 - z1)) + (y3 * (z1 - z2));
            double B = (z1 * (x2 - y3)) + (z2 * (x3 - x1)) + (z3 * (x1 - x2));
            double C = (x1 * (y2 - y3)) + (x2 * (y3 - y1)) + (x3 * (y1 - y2));
            double d = -((x1 * ((y2 * z3) - (y3 * z2)) + (x2 * ((y3 * z1) - (y1 * z3))) + (x3 * ((y1 * z2) - (y2 * z1)))));

            double maxAbovePlane = 0;
            double maxBelowPlane = 0;

            foreach (Point p in points)
            {
                double distance = FindDistanceFromPlane(p, A, B, C, d);
                if (distance > maxAbovePlane)
                    maxAbovePlane = distance;
                if (distance < maxBelowPlane)
                    maxBelowPlane = distance;
            }

            return maxAbovePlane - maxBelowPlane;
        }

        private static double FindDistanceFromPlane(Point p, double A, double B, double C, double d)
        {
            double x0 = p.X;
            double y0 = p.Y;
            double z0 = 0;

            return ((A * x0) + (B * y0) + (C * z0) + d) / Math.Sqrt((A * A) + (B * B) + (C * C));
        }
    }
}
