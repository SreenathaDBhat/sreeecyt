﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Windows.Markup;
using System.Windows.Input;
using AI.Brightfield;
using System.Windows.Data;

namespace AI
{
    public partial class ChromosomeGrid : Window
    {
        private ChromosomeGridDataSource data;
        private ChromosomeGroup destGroup;
        private WorkflowController workflowController;

        public ChromosomeGrid(WorkflowController workflow)
        {
            workflowController = workflow;
            data = new ChromosomeGridDataSource(workflow, Dispatcher);
            DataContext = data;

            InitializeComponent();
            WindowLocations.Handle(this);

            RefreshColumns();
        }

        private void RefreshColumns()
        {
            try
            {
                Cursor = Cursors.Wait;

                grid.Columns.Clear();

                foreach (var cell in data.VisibleCells)
                {
                    var col = new DataGridTemplateColumn();
                    col.IsReadOnly = true;
                    col.CellTemplate = GenerateColumnTemplate(data.IndexOf(cell));
                    col.Header = cell;
                    grid.Columns.Add(col);
                }
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private DataTemplate GenerateColumnTemplate(int index)
        {
            string xaml = "<DataTemplate xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\"\r\n" +
                                        "xmlns:local=\"clr-namespace:AI.Brightfield;assembly=BrightfieldCaseView\">\r\n" +
                                        "<Border Background=\"#01000000\" Margin=\"10,0,10,0\" Tag=\"{Binding Groups[" + index + "]}\">\r\n" +
                                            "<local:ChromosomeGroupControl Group=\"{Binding Groups[" + index + "]}\" Scale=\"{Binding Parent.ChromosomeScale}\" UndoList=\"{Binding UndoList}\">\r\n" +
                                            "<local:ChromosomeGroupControl.LayoutTransform><ScaleTransform ScaleX=\"{Binding Parent.ChromosomeScale}\" ScaleY=\"{Binding Parent.ChromosomeScale}\" /></local:ChromosomeGroupControl.LayoutTransform>\r\n" +
                                            "</local:ChromosomeGroupControl>\r\n" +
                                        "</Border>\r\n" +
                          "</DataTemplate>";

            XmlReader r = new XmlTextReader(xaml, XmlNodeType.Element, null);
            var t = XamlReader.Load(r);
            return t as DataTemplate;
        }

        private void CanShowReassignPopup(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ShowReassignPopup(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            reassignPopup.Tag = ((ChromosomeClickEventArgs)e.Parameter).Chromosome;
            reassignPopup.IsOpen = true;
        }

        private void OnSelectUnclassifiedChromosome(object sender, MouseButtonEventArgs e)
        {
            var chrom = (KaryotypeChromosome)((ChromosomeControl)sender).Chromosome;
            data.UndoList.Add(new Brightfield.KaryotypeUndo.ReclassifyChromosome(chrom, chrom.Group, chrom.Group.IndexOf(chrom)));
            destGroup.Adopt(chrom);
            unclassifiedPopup.IsOpen = false;
        }

        private void OnAddMarker(object sender, RoutedEventArgs e)
        {

        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            Connect.ConnectionTest.Test(this);
            data.Save();
            DialogResult = true;
        }

        private void OnBringInUnclassifieds(object sender, RoutedEventArgs e)
        {
            object tag = ((FrameworkElement)sender).Tag;
            while (!(tag is ChromosomeGroupControl) && tag != null)
            {
                tag = VisualTreeWalker.FindChildOfType<ChromosomeGroupControl>((DependencyObject)tag);
            }

            destGroup = ((ChromosomeGroupControl)tag).Group;
            var unclassified = data.FindUnclassified(destGroup.Cell);

            if (!unclassified.IsEmpty)
            {
                unclassifiedChromosomesOnPopup.ItemsSource = unclassified.Chromosomes;
                unclassifiedPopup.IsOpen = true;
            }
        }

        private void CanUndoExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = data.CanUndo;
        }

        private void UndoExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            data.Undo();
        }

        private void OnCellInfo(object sender, RoutedEventArgs e)
        {
            var cell = (Cell)((FrameworkElement)sender).Tag;
            var title = workflowController.StringTable.Lookup("metadata") + ": " + cell.Name;
            
            var addDataWindow = new MetaData.MetaDataEditor(cell, workflowController.AvailableCellFields, title);
            addDataWindow.Owner = VisualTreeWalker.FindParentOfType<Window>(this);
            if (addDataWindow.ShowDialog().Value)
            {
                cell.NotifyDefaultMetaDataFields();
                data.UpdateCellsHaveCellLines();

                MetaData.CellFields.Save(cell.MetaData, cell.Id);
                workflowController.AvailableCellFields.Refresh();
            }
        }

        private void OnFlipY(object sender, RoutedEventArgs e)
        {
            var chrom = reassignPopup.Tag as KaryotypeChromosome;
            data.UndoList.Add(new Brightfield.KaryotypeUndo.FlipChromosome(chrom, chrom.IsXFlipped, chrom.IsYFlipped));
            chrom.IsYFlipped = !chrom.IsYFlipped;

            reassignPopup.IsOpen = false;
        }

        private void OnFlipX(object sender, RoutedEventArgs e)
        {
            var chrom = reassignPopup.Tag as KaryotypeChromosome;
            data.UndoList.Add(new Brightfield.KaryotypeUndo.FlipChromosome(chrom, chrom.IsXFlipped, chrom.IsYFlipped));
            chrom.IsXFlipped = !chrom.IsXFlipped;

            reassignPopup.IsOpen = false;
        }

        private void OnDeleteChromosome(object sender, RoutedEventArgs e)
        {
            var chrom = reassignPopup.Tag as KaryotypeChromosome;
            data.UndoList.Add(new Brightfield.KaryotypeUndo.DeleteChromosome(chrom, chrom.Group, chrom.Group.IndexOf(chrom)));

            chrom.Group.Remove(chrom);

            reassignPopup.IsOpen = false;
        }

        private void CanReclassifyChrom(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ReclassifyChromExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var chrom = reassignPopup.Tag as KaryotypeChromosome;
            var groupIdentifier = e.Parameter as string;
            var row = data.FindRowFromGroupIdentifier(groupIdentifier);
            var group = row.GroupForCell(chrom.Cell);

            data.UndoList.Add(new Brightfield.KaryotypeUndo.ReclassifyChromosome(chrom, chrom.Group, chrom.Group.IndexOf(chrom)));
            group.Adopt(chrom);
            reassignPopup.IsOpen = false;
        }

        private void OnStartDraggingRow(object sender, MouseButtonEventArgs e)
        {
            DraggableClassRow dr = (DraggableClassRow)((FrameworkElement)sender).Tag;
            rowDraggingBorder.CaptureMouse();
            data.StartDraggingRow(dr);
        }

        private void OnStopDraggingRow(object sender, MouseButtonEventArgs e)
        {
            rowDraggingBorder.ReleaseMouseCapture();
            data.StopDraggingRow();
        }

        private void OnDraggingMouseMove(object sender, MouseEventArgs e)
        {
            Point currentPosition = e.GetPosition(rowDraggingBorder);
            currentPosition = new Point(currentPosition.X, currentPosition.Y - 12);
            data.UpdateDragPosition(currentPosition);
        }

        private void OnChangeToVisibleRows(object sender, RoutedEventArgs e)
        {
            data.UpdateVisibleRows();
        }

        private void OnHideAllRows(object sender, RoutedEventArgs e)
        {
            data.HideAllRows();
        }

        private void OnShowAllRows(object sender, RoutedEventArgs e)
        {
            data.ShowAllRows();
        }

        private void OnDismissMetPreview(object sender, MouseButtonEventArgs e)
        {
            data.HideMetView();
        }

        private void OnViewMetaphase(object sender, RoutedEventArgs e)
        {
            var chrom = reassignPopup.Tag as KaryotypeChromosome;
            reassignPopup.IsOpen = false;
            data.ShowMetWithHighlightedChrom(chrom);
        }

        private void OnViewGroup(object sender, RoutedEventArgs e)
        {
            var chrom = reassignPopup.Tag as KaryotypeChromosome;
            reassignPopup.IsOpen = false;
            data.ShowMetWithHighlightedGroup(chrom);
        }

        private void OnDismissMetImage(object sender, RoutedEventArgs e)
        {
            data.HideMetView();
        }

        private void OnResetZoom(object sender, RoutedEventArgs e)
        {
            data.ChromosomeScale = 1;
            RefreshColumns();
        }

        private void OnResetColumns(object sender, RoutedEventArgs e)
        {
            RefreshColumns();
        }

        private void OnSortByCellLine(object sender, RoutedEventArgs e)
        {
            data.SortByCellLine();
            RefreshColumns();
        }

        private void OnPreviewScaleSliderMouseUp(object sender, MouseButtonEventArgs e)
        {
            RefreshColumns();
        }

        private void CanFlipChromByDoubleClick(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void FlipChromByDoubleClick(object sender, ExecutedRoutedEventArgs e)
        {
            ChromosomeClickEventArgs ea = e.Parameter as ChromosomeClickEventArgs;
            if (ea.ClickCount == 2)
            {
                KaryotypeChromosome chromToFlip = ea.Chromosome;
                chromToFlip.UserRotation += 180;
            }
        }
    }

    public class BringInUnclassifiedVisibilityConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] == null || values[1] == null || values[2] == null)
                return Visibility.Collapsed;

            bool isMouseOverGroup = (bool)values[0];

            FrameworkElement chromGroup = (FrameworkElement)values[1];
            while (!(chromGroup is ChromosomeGroupControl) && chromGroup != null)
            {
                chromGroup = VisualTreeWalker.FindChildOfType<ChromosomeGroupControl>((DependencyObject)chromGroup);
            }

            if (chromGroup == null)
                return Visibility.Collapsed;

            ChromosomeGroup thisGroup = ((ChromosomeGroupControl)chromGroup).Group;

            if (thisGroup.Identifier == ChromosomeGroup.UnclassifiedString)
                return Visibility.Collapsed;

            var cell = thisGroup.Cell;
            ChromosomeGridDataSource ds = (ChromosomeGridDataSource)values[2];
            bool cellHasUnclassifiedChroms = ds.DoesCellHaveUnclassifiedChroms(cell);

            return (isMouseOverGroup && cellHasUnclassifiedChroms) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
