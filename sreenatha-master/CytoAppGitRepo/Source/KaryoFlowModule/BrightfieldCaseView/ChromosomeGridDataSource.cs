﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using System.Threading;
using AI.Brightfield;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Windows.Media;

namespace AI
{
    public class ClassRow
    {
        private List<ChromosomeGroup> groups;

        public IEnumerable<ChromosomeGroup> Groups { get { return groups; } }
        public string ClassIdentifier { get; set; }
        public object Parent { get; set; }

        private Guid caseId;

        public ClassRow(object parent, Guid caseId, string classIdentifier, IEnumerable<Cell> cellIds, IUndoListAdder undo, IEnumerable<Connect.CaseKaryotypeChromosome> caseChroms)
        {
            this.Parent = parent;
            UndoList = undo;
            this.caseId = caseId;
            this.groups = new List<ChromosomeGroup>();
            ClassIdentifier = classIdentifier;

            foreach (var cellId in cellIds)
            {
                ChromosomeGroup g = ChromosomeGroup.GroupForClass(ClassIdentifier, cellId.Id);

                var huh = (from c in caseChroms
                           where c.cellId == cellId.Id && c.chromosomeTag == classIdentifier
                           select c).ToArray();

                foreach (var c in huh)
                {
                    LoadChrom(c, g);
                }

                groups.Add(g);
            }
        }

        private KaryotypeChromosome LoadChrom(Connect.CaseKaryotypeChromosome c, ChromosomeGroup g)
        {
            var k = KaryotypeChromosome.CreateFromDb(c);
            g.AdoptWithoutArrange(k);
            return k;
        }

        public IUndoListAdder UndoList { get; private set; }

        public ChromosomeGroup GroupForCell(Guid guid)
        {
            return groups.Where(g => g.Cell == guid).FirstOrDefault();
        }

        public override string ToString()
        {
            return ClassIdentifier;
        }
    }

    public class DraggableClassRow : Notifier
    {
        private double y;
        private bool displayed;

        public ClassRow Row { get; set; }
        public double Y { get { return y; } set { y = value; Notify("Y"); } }
        public bool Displayed { get { return displayed; } set { displayed = value; Notify("Displayed"); } }
    }

    public class ChromosomeGridDataSource : Notifier
    {
        private static readonly string ChromosomeScaleKey = "Grid_ChromosomeScale";

        private List<Cell> cells, visibleCells;
        private List<ClassRow> rows, visibleRows;
        private List<DraggableClassRow> allRowsInOrder;
        private UndoList undo;
        private bool draggingRow;
        private DraggableClassRow rowBeingDragged;
        private bool showingMet;
        private BitmapSource metImage;
        private ObservableCollection<PointCollection> chromosomeBoundaries = new ObservableCollection<PointCollection>();
        private SimplePersistance persister;
        private WorkflowController workflowController;

        public IEnumerable<Cell> Cells { get { return cells; } }
        public IEnumerable<ClassRow> Rows { get { return rows; } }
        public IEnumerable<Cell> VisibleCells { get { return visibleCells; } }
        public IEnumerable<ClassRow> VisibleRows { get { return visibleRows; } }
        public IEnumerable<DraggableClassRow> AllRowsInOrder { get { return allRowsInOrder; } }
        public Guid CaseId { get; private set; }
        public IUndoListAdder UndoList { get { return undo; } }
        public BitmapSource MetImage { get { return metImage; } private set { metImage = value; Notify("MetImage"); } }
        public IEnumerable<PointCollection> Boundaries { get { return chromosomeBoundaries; } }
        public bool ShowingMet { get { return showingMet; } private set { showingMet = value; Notify("ShowingMet"); } }

        public ChromosomeGridDataSource(WorkflowController controller, Dispatcher uiDispatcher)
        {
            this.workflowController = controller;
            persister = controller.Persistants;
            CaseId = controller.CaseId;
            undo = new UndoList();

            var db = new AI.Connect.Database();

            cells = new List<Cell>(controller.AllCells.Where(c => c.IsKaryotyped));
            visibleCells = new List<Cell>(cells);

            chromosomeScale = persister.GetDouble(ChromosomeScaleKey, 1);

            
            rows = new List<ClassRow>();
            List<ChromosomeGroup> allGroups = new List<ChromosomeGroup>();

            var allTheDamnChromsBaby = (from c in db.CaseKaryotypeChromosomes
                                        where c.caseId == controller.CaseId
                                        orderby c.groupIndex ascending
                                        select c).ToArray();

            foreach (var g in ChromosomeGroup.GroupsForCase(controller.CaseId))
            {
                var row = new ClassRow(this, controller.CaseId, g, cells, undo, allTheDamnChromsBaby);
                rows.Add(row);
                allGroups.AddRange(row.Groups);
            }

            foreach (var someGroup in allGroups)
            {
                Task.Factory.StartNew(state =>
                    {
                        var g = state as ChromosomeGroup;
                        var database = new Connect.Database();

                        foreach (var k in g.Chromosomes)
                        {
                            var img = ImageFromBitmapSource.LoadBitmapImageFromStream(controller.CaseId, KaryotypeChromosome.ChromosomeDataFile(k.Id), database);

                            uiDispatcher.Invoke((ThreadStart)delegate
                            {
                                k.DisplayImage = img;

                            }, DispatcherPriority.Normal);
                        }
                    }, someGroup);
            }

            visibleRows = new List<ClassRow>(rows);
            allRowsInOrder = (from r in rows
                              select new DraggableClassRow()
                              {
                                  Row = r,
                                  Displayed = true
                              }).ToList();
            SetYForDraggableRows();
            UpdateCellsHaveCellLines();
        }

        private double chromosomeScale;
        public double ChromosomeScale
        {
            get { return chromosomeScale; }
            set { chromosomeScale = value; Notify("ChromosomeScale"); persister.Set(ChromosomeScaleKey, value); }
        }

        public ChromosomeGroup FindUnclassified(Guid cellId)
        {
            var unclassified = rows.Where(r => r.ClassIdentifier == ChromosomeGroup.UnclassifiedString).First();
            return unclassified.GroupForCell(cellId);
        }

        private void RefreshRows()
        {
            visibleRows = new List<ClassRow>(visibleRows);
            Notify("VisibleRows");
        }

        public void MoveRowUp(ClassRow classRow)
        {
            var index = visibleRows.IndexOf(classRow);
            if (index > 0)
            {
                visibleRows.Remove(classRow);
                visibleRows.Insert(index - 1, classRow);
                RefreshRows();
            }
        }

        public void MoveRowDown(ClassRow classRow)
        {
            var index = visibleRows.IndexOf(classRow);
            if (index < visibleRows.Count - 1)
            {
                visibleRows.Remove(classRow);
                visibleRows.Insert(index + 1, classRow);
                RefreshRows();
            }
        }

        public void MoveRowBelow(ClassRow classRowToMove, ClassRow destination)
        {
            var indexDest = visibleRows.IndexOf(destination);
            var indexSrc = visibleRows.IndexOf(classRowToMove);

            var delta = indexDest < indexSrc ? 1 : 0;

            visibleRows.Remove(classRowToMove);
            visibleRows.Insert(indexDest + delta, classRowToMove);
            RefreshRows();
        }

        public void MakeOnlyTheseChromosomesVisible(IEnumerable<ClassRow> iEnumerable)
        {
            visibleRows = new List<ClassRow>(iEnumerable);
            Notify("VisibleRows");
        }

        public void MakeOnlyTheseCellsVisible(IEnumerable<Cell> iEnumerable)
        {
            visibleCells = new List<Cell>(iEnumerable);
            Notify("VisibleCells");
        }

        public void Save()
        {
            Parallel.ForEach(cells, cell =>
                {
                    DeleteDeletedChromsForCell(cell);
                });

            Parallel.ForEach(rows, r =>
            {
                var db = new Connect.Database();

                foreach (var g in r.Groups)
                {
                    int index = 0;

                    foreach (var c in g.Chromosomes)
                    {
                        c.SaveNonImageData(db, CaseId, g.Cell, index++);
                    }
                }

                db.SubmitChanges();
            });

            using (var lims = Connect.LIMS.New())
            {
                lims.MarkCaseAsModified(workflowController.Case, DateTime.Now, "Grid");
            }

            SaveKaryotypeThumbnails();
            SetKaryotypedForCells();
        }

        private void SetKaryotypedForCells()
        {
            foreach (var cell in cells)
            {
                var chroms = ChromosomesForCell(cell);
                bool hasClassifications = chroms.Where(c => c.IsClassified).Count() > 0;
                workflowController.SetKaryotyped(cell, hasClassifications);
            }
        }

        private void DeleteDeletedChromsForCell(Cell cell)
        {
            var db = new Connect.Database();
            var existingChroms = db.KaryotypeChromosomes.Where(kc => kc.cellId == cell.Id).ToList();
            var liveChromsForThisCell = ChromosomesForCell(cell);
            var deletedChroms = existingChroms.Where(c => liveChromsForThisCell.Count(k => k.Id == c.karyotypeChromosomeId) == 0);

            db.KaryotypeChromosomes.DeleteAllOnSubmit(deletedChroms);
            db.SubmitChanges();
        }

        private IEnumerable<KaryotypeChromosome> ChromosomesForCell(Cell cell)
        {
            foreach (var row in rows)
                foreach (var group in row.Groups)
                    if (group.Cell == cell.Id)
                        foreach (var chrom in group.Chromosomes)
                            yield return chrom;
        }

        private void SaveKaryotypeThumbnails()
        {
            foreach (var cell in cells)
            {
                var chromosomeGroups = GetGroupsForCell(cell);
                KaryotypeThumbnailGenerator.SaveKaryotypeThumbnail(cell, chromosomeGroups, workflowController.RelativeIdCell, DataSourceFactory.GetDataSource());
            }
        }

        private IEnumerable<ChromosomeGroup> GetGroupsForCell(Cell cell)
        {
            foreach (var row in rows)
            {
                yield return row.GroupForCell(cell.Id);
            }
        }

        public int IndexOf(Cell cell)
        {
            var g = rows.FirstOrDefault();
            if (g == null)
                return -1;

            var groupenum = g.Groups.GetEnumerator();
            int index = 0;
            while (groupenum.MoveNext())
            {
                if (groupenum.Current.Cell == cell.Id)
                    return index;

                index++;
            }

            return -1;
        }

        public void Undo()
        {
            undo.Undo(this);
        }

        public bool CanUndo
        {
            get { return undo.HasContent; }
        }

        internal ClassRow FindRowFromGroupIdentifier(string groupIdentifier)
        {
            foreach (var row in rows)
            {
                if (row.ClassIdentifier == groupIdentifier)
                    return row;
            }
            return null;
        }

        internal bool DoesCellHaveUnclassifiedChroms(Guid cell)
        {
            var unclassifiedRow = rows.Where(r => r.ClassIdentifier == ChromosomeGroup.UnclassifiedString).First();
            var unclassifiedGroup = unclassifiedRow.GroupForCell(cell);
            return unclassifiedGroup.Chromosomes.Count() > 0;
        }

        internal void StartDraggingRow(DraggableClassRow dr)
        {
            draggingRow = true;
            rowBeingDragged = dr;
        }

        internal void StopDraggingRow()
        {
            draggingRow = false;
            rowBeingDragged = null;
            ReOrderDraggableRowsBasedOnY();
            SetYForDraggableRows();
            UpdateVisibleRows();
        }

        internal void UpdateDragPosition(System.Windows.Point currentPosition)
        {
            if (!draggingRow || rowBeingDragged == null)
                return;

            rowBeingDragged.Y = currentPosition.Y;
            ArrangeDraggableRowsMidDrag();
        }

        private void ArrangeDraggableRowsMidDrag()
        {
            ReOrderDraggableRowsBasedOnY();

            double y = 0;
            foreach (var row in allRowsInOrder)
            {
                if (row == rowBeingDragged)
                {
                    y += 30;
                    continue;
                }

                row.Y = y;
                y += 30;
            }
        }

        private void ReOrderDraggableRowsBasedOnY()
        {
            allRowsInOrder.Sort(new RowPositionComparer());
        }

        private void SetYForDraggableRows()
        {
            double y = 0;
            foreach (var row in allRowsInOrder)
            {
                row.Y = y;
                y += 30;
            }
        }

        internal void UpdateVisibleRows()
        {
            List<ClassRow> visibleRowsInOrder = (from r in allRowsInOrder
                                                 where r.Displayed
                                                 select r.Row).ToList();

            MakeOnlyTheseChromosomesVisible(visibleRowsInOrder);
        }

        internal void HideAllRows()
        {
            foreach (var row in allRowsInOrder)
                row.Displayed = false;

            UpdateVisibleRows();
        }

        internal void ShowAllRows()
        {
            foreach (var row in allRowsInOrder)
                row.Displayed = true; ;

            UpdateVisibleRows();
        }

        internal void ShowMetWithHighlightedChrom(KaryotypeChromosome chrom)
        {
            SetMetImageFromChrom(chrom);
            
            chromosomeBoundaries.Clear();
            chromosomeBoundaries.Add(chrom.Outline);
            ShowingMet = true;
        }

        private void SetMetImageFromChrom(KaryotypeChromosome chrom)
        {
            var cell = cells.Where(c => c.Id == chrom.Cell).First();
            var imageId = cell.ImageID;
            var database = new Connect.Database();
            MetImage = ImageFromBitmapSource.LoadBitmapImageFromStream(CaseId, imageId.ToString() + ".jpg", database);
        }

        internal void HideMetView()
        {
            ShowingMet = false;
        }

        internal unsafe void ShowMetWithHighlightedGroup(KaryotypeChromosome chrom)
        {
            SetMetImageFromChrom(chrom);
            
            chromosomeBoundaries.Clear();
            var row = Rows.Where(r => r.ClassIdentifier == chrom.Designation).First();
            var group = row.GroupForCell(chrom.Cell);

            foreach (var kc in group.Chromosomes)
                chromosomeBoundaries.Add(kc.Outline);

            ShowingMet = true;
        }

        internal void SortByCellLine()
        {
            visibleCells.Sort(new Cell_CellLineComparer());
        }

        private bool cellsHaveCellLines;
        public bool CellsHaveCellLines
        {
            get { return cellsHaveCellLines; }
            set { cellsHaveCellLines = value; Notify("CellsHaveCellLines"); }
        }

        public void UpdateCellsHaveCellLines()
        {
            foreach (var cell in visibleCells)
            {
                if (cell.CellLine == null || cell.CellLine == string.Empty)
                    continue;

                CellsHaveCellLines = true;
                return;
            }
            CellsHaveCellLines = false;
        }
    }

    public class RowPositionComparer : IComparer<DraggableClassRow>
    {
        #region IComparer<DraggableClassRow> Members

        public int Compare(DraggableClassRow x, DraggableClassRow y)
        {
            if (x.Y > y.Y)
                return 1;
            if (x.Y < y.Y)
                return -1;
            return 0;
        }

        #endregion
    }

    public class Cell_CellLineComparer : IComparer<Cell>
    {
        #region IComparer<Cell> Members

        public int Compare(Cell x, Cell y)
        {
            if (x.CellLine == null || x.CellLine == string.Empty)
                return 1;

            if (y.CellLine == null || y.CellLine == string.Empty)
                return -1;

            return string.Compare(x.CellLine, y.CellLine);
        }

        #endregion
    }


}
