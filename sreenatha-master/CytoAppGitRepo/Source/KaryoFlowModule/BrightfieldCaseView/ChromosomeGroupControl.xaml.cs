﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows.Input;
using System.Runtime.InteropServices;
using Undo = AI.Brightfield.KaryotypeUndo;
using System.Windows.Documents;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Media.Effects;
using System.Threading;
using System.Windows.Threading;

namespace AI.Brightfield
{
    public partial class ChromosomeGroupControl
    {
        public static readonly DependencyProperty GroupProperty = DependencyProperty.Register("Group", typeof(ChromosomeGroup), typeof(ChromosomeGroupControl), new FrameworkPropertyMetadata(OnGroupChanged));
        public static readonly RoutedUICommand ChromosomeClick = new RoutedUICommand("ChromosomeClick", "ChromosomeClick", typeof(ChromosomeGroupControl));
        public static readonly RoutedUICommand ChromosomeRightClick = new RoutedUICommand("ChromosomeRightClick", "ChromosomeRightClick", typeof(ChromosomeGroupControl));
        public static readonly DependencyProperty UndoListProperty = DependencyProperty.Register("UndoList", typeof(IUndoListAdder), typeof(ChromosomeGroupControl));
        public static readonly DependencyProperty ScaleProperty = DependencyProperty.Register("Scale", typeof(double), typeof(ChromosomeGroupControl));

        #region Implementation Detail

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern int GetDoubleClickTime();
        private DateTime timeOfLastLeftClick = DateTime.MinValue;
        private DateTime timeOfLastRightClick = DateTime.MinValue;


        public class ChromDragData
        {
            public KaryotypeChromosome Chromosome { get; internal set; }
            public AI.Brightfield.ChromosomeGroup Group { get; internal set; }
            public DragChromosomeAdorner DragVisual { get; internal set; }
        }

        #endregion

        public ChromosomeGroupControl()
        {
            InitializeComponent();
        }

        private void FireChromosomeClicked(ChromosomeControl chromosomeControl)
        {
            if (chromosomeControl == null)
                return;

            var chrom = chromosomeControl.Chromosome;

            DateTime now = DateTime.Now;
            var timeSinceLast = now - timeOfLastLeftClick;
            int clickCount = 1;

            if (timeSinceLast.TotalMilliseconds < GetDoubleClickTime())
                clickCount = 2;

            timeOfLastLeftClick = now;

            var e = new ChromosomeClickEventArgs { Chromosome = chrom, ClickCount = clickCount };

            if (ChromosomeClick.CanExecute(e, this))
                ChromosomeClick.Execute(e, this);
        }

        private void FireChromosomeRightClicked(ChromosomeControl chromosomeControl)
        {
            if (chromosomeControl == null)
                return;

            var chrom = chromosomeControl.Chromosome;

            DateTime now = DateTime.Now;
            var timeSinceLast = now - timeOfLastRightClick;
            int clickCount = 1;

            if (timeSinceLast.TotalMilliseconds < GetDoubleClickTime())
                clickCount = 2;

            timeOfLastRightClick = now;

            var e = new ChromosomeClickEventArgs { Chromosome = chrom, ClickCount = clickCount };

            if (ChromosomeRightClick.CanExecute(e, this))
                ChromosomeRightClick.Execute(e, this);
        }

        static void OnGroupChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((ChromosomeGroupControl)sender).chromlist.ItemsSource = ((ChromosomeGroup)e.NewValue).Chromosomes;
        }

        public ChromosomeGroup Group
        {
            get { return (ChromosomeGroup)GetValue(GroupProperty); }
            set { SetValue(GroupProperty, value); }
        }

        public IUndoListAdder UndoList
        {
            get { return (IUndoListAdder)GetValue(UndoListProperty); }
            set { SetValue(UndoListProperty, value); }
        }

        public double Scale
        {
            get { return (double)GetValue(ScaleProperty); }
            set { SetValue(ScaleProperty, value); }
        }

        private void ChromosomeDroppedOnMe(ChromDragData dragData, ChromosomeGroup destinationGroup, Point p)
        {
            // Grid, dont allow drags into other cells
            if(Group.Cell == dragData.Chromosome.Group.Cell)
            {
                var originalGroup = dragData.Group;
                var index = dragData.Group.IndexOf(dragData.Chromosome);

                if (Group == dragData.Chromosome.Group)
                {
                    AddToUndo(new Undo.NudgeChromosome(dragData.Chromosome, dragData.Chromosome.UserOffsetX, dragData.Chromosome.UserOffsetY));
                    dragData.Chromosome.UserOffsetX -= dragStartPt.X - p.X;
                    dragData.Chromosome.UserOffsetY -= dragStartPt.Y - p.Y;
                }
                else
                {
                    Group.Adopt(dragData.Chromosome);
                    dragData.Chromosome.UserOffsetX = 0;
                    dragData.Chromosome.UserOffsetY = 0;

                    AddToUndo(new Undo.ReclassifyChromosome(dragData.Chromosome, originalGroup, index));
                }
            }
        }

        private void ChromosomeDroppedOnAnotherChromosome(ChromDragData dragData, KaryotypeChromosome droppedOn, Point p)
        {
            if (dragData.Chromosome != droppedOn)
            {
                var originalGroup = dragData.Group;
                var index = dragData.Group.IndexOf(dragData.Chromosome);

                // same group: swap positions
                if (droppedOn.Group == dragData.Chromosome.Group)
                {
                    droppedOn.Group.Swap(droppedOn, dragData.Chromosome);

                    AddToUndo(new Undo.SwapChromosomesWithinGroup(droppedOn.Group, dragData.Chromosome, droppedOn));
                }
                // different group. insert
                else if (droppedOn.Group.Cell == dragData.Chromosome.Group.Cell)
                {
                    Group.InsertBefore(dragData.Chromosome, droppedOn);
                    dragData.Chromosome.UserOffsetX = 0;
                    dragData.Chromosome.UserOffsetY = 0;

                    AddToUndo(new Undo.ReclassifyChromosome(dragData.Chromosome, originalGroup, index));
                }
            }
            else
            {
                AddToUndo(new Undo.NudgeChromosome(dragData.Chromosome, dragData.Chromosome.UserOffsetX, dragData.Chromosome.UserOffsetY));
                dragData.Chromosome.UserOffsetX -= dragStartPt.X - p.X;
                dragData.Chromosome.UserOffsetY -= dragStartPt.Y - p.Y;
            }
        }



        #region Mouse Handling
        
        private double previousRotation;
        private Point dragStartPt, lastDragPt;
        private bool outsideDeadzone, leftDown, rightDown, inDragDrop, inRotate;
        private ChromosomeControl mouseCaptureChromosome;

        private void AddToUndo(IUndoItem undoItem)
        {
            if(UndoList != null)
                UndoList.Add(undoItem);
        }

        private void OnDragOver_Group(object sender, DragEventArgs e)
        {
            e.Handled = true;

            var data = e.Data.GetData(typeof(ChromDragData)) as ChromDragData;

            if (data.Chromosome.Cell == Group.Cell)
            {
                data.DragVisual.SetAsValidDrop();
            }
            else
            {
                data.DragVisual.SetAsInvalidDrop();
            }
        }

        private void OnDragLeave_Group(object sender, DragEventArgs e)
        {
            e.Handled = true;

            var data = e.Data.GetData(typeof(ChromDragData)) as ChromDragData;
            data.DragVisual.SetAsInvalidDrop();
        }

        private void OnDragOver_Chromosome(object sender, DragEventArgs e)
        {
            e.Handled = true;

            var data = e.Data.GetData(typeof(ChromDragData)) as ChromDragData;
            var chromUnderneath = (ChromosomeControl)sender;
            
            if (data.Chromosome.Cell == chromUnderneath.Chromosome.Group.Cell)
            {
                data.DragVisual.SetAsValidDrop();
            }
            else
            {
                data.DragVisual.SetAsInvalidDrop();
            }
        }

        private void OnDrop_Group(object sender, DragEventArgs e)
        {
            e.Handled = true;
            Point p = e.GetPosition(this);

            var data = e.Data.GetData(typeof(ChromDragData)) as ChromDragData;
            
            // Chromosome dropped on us.
            if(data != null)
                ChromosomeDroppedOnMe(data, Group, p);
        }

        private void OnDrop_Chromosome(object sender, DragEventArgs e)
        {
            e.Handled = true;
            Point p = e.GetPosition(this);

            var droppedOn = ((ChromosomeControl)sender).Chromosome;
            var data = e.Data.GetData(typeof(ChromDragData)) as ChromDragData;

            ChromosomeDroppedOnAnotherChromosome(data, droppedOn, p);
        }

        private void CheckDeadzone(Point origin, Point pt)
        {
            var dist = ((origin.X - pt.X) * (origin.X - pt.X)) + ((origin.Y - pt.Y) * (origin.Y - pt.Y));
            outsideDeadzone = dist > (3 * 3);
        }

        private void OnChromLeftDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            if (rightDown)
            {
                return;
            }

            outsideDeadzone = false;
            leftDown = true;
            inDragDrop = false;
            inRotate = false;
            dragStartPt = e.GetPosition(this);
            
            var chromosomeControl = sender as ChromosomeControl;
            mouseCaptureChromosome = chromosomeControl;
        }

        private void OnChromLeftUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            leftDown = false;
            FireChromosomeClicked(mouseCaptureChromosome);
        }

        private void OnChromRightDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            if (leftDown)
            {
                return;
            }

            outsideDeadzone = false;
            rightDown = true;
            inDragDrop = false;
            inRotate = false;

            var parentWindow = VisualTreeWalker.FindParentOfType<Window>(this);

            dragStartPt = e.GetPosition(parentWindow);

            var chromosomeControl = sender as ChromosomeControl;
            lastDragPt = dragStartPt = e.GetPosition(parentWindow);
            mouseCaptureChromosome = chromosomeControl;

			previousRotation = mouseCaptureChromosome.Chromosome.UserRotation;
        }

        private void OnChromRightUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            rightDown = false;

            if (inRotate)
            {
                ReleaseMouseCapture();
                if (mouseCaptureChromosome != null)
                    AddToUndo(new Undo.RotateChromosome(mouseCaptureChromosome.Chromosome, previousRotation));
                inRotate = false;
            }
            else
            {
                FireChromosomeRightClicked(mouseCaptureChromosome);
            }
        }

        private void OnChromMouseMove(object sender, MouseEventArgs e)
        {
            e.Handled = true;

            if (inDragDrop)
            {
                return;
            }
            
            var pt = e.GetPosition(this);
            CheckDeadzone(dragStartPt, pt);

            if (leftDown)
            {
                if (outsideDeadzone)
                {
                    StartDragDrop(dragStartPt);
                    ManuallyProdTheCommandBindings();
                }
            }
            else if(rightDown)
            {
                pt = e.GetPosition(VisualTreeWalker.FindParentOfType<Window>(this));
                double delta = pt.X - lastDragPt.X;

                if (!inRotate && delta != 0.0 && outsideDeadzone)
                {
                    inRotate = true;
                    CaptureMouse();
                }

                if (inRotate)
                mouseCaptureChromosome.Chromosome.UserRotation += delta * 0.6;

                lastDragPt = pt;
            }
        }

        private void ManuallyProdTheCommandBindings()
        {
            Dispatcher.Invoke((ThreadStart)delegate
            {
                CommandManager.InvalidateRequerySuggested();

            }, DispatcherPriority.Normal);
        }

        #region Drag Drop Stuff

        private DragChromosomeAdorner chromosomeAdorner = null;
        private AdornerLayer adornerLayer;
        private FrameworkElement dragScope;
        private bool dragHasLeftScope = false;
        
        public FrameworkElement DragScope
        {
            get { return dragScope; }
            set { dragScope = value; }
        }

		/// <summary>
		/// Performs drag'n'drop chromosome operation whilst displaying the chromosome image attached to the mouse.
		/// </summary>
		/// <param name="initialMouseOnChromGroup"> Initial mouse position relative to this chromosome group control.</param>
        private void StartDragDrop( Point initialMouseOnChromGroup)
        {
            DragScope = VisualTreeWalker.FindParentOfType<Window>(this).Content as FrameworkElement;
            System.Diagnostics.Debug.Assert(DragScope != null);

            // We enable Drag & Drop in our scope ...  We are not implementing Drop, so it is OK, but this allows us to get DragOver 
            bool previousDrop = DragScope.AllowDrop;
            DragScope.AllowDrop = true;

            // Let's wire our usual events.. 
            // GiveFeedback just tells it to use no standard cursors..  

            GiveFeedbackEventHandler feedbackHandler = new GiveFeedbackEventHandler(DragSource_GiveFeedback);
            groupControl.GiveFeedback += feedbackHandler;

            // The DragOver event ... 
            DragEventHandler dragHandler = new DragEventHandler(DragOverEventHandler);
            DragScope.PreviewDragOver += dragHandler;

            // Drag Leave is optional, but write up explains why I like it .. 
            DragEventHandler dragLeaveHandler = new DragEventHandler(DragScope_DragLeave);
            DragScope.DragLeave += dragLeaveHandler;

            // QueryContinue Drag goes with drag leave... 
            QueryContinueDragEventHandler queryHandler = new QueryContinueDragEventHandler(DragScope_QueryContinueDrag);
            DragScope.QueryContinueDrag += queryHandler;

			GeneralTransform chromosomeGroupToDragScope = this.TransformToAncestor(DragScope);
			Point initialMouse = chromosomeGroupToDragScope.Transform(initialMouseOnChromGroup);

			GeneralTransform chromosomeToDragScope = mouseCaptureChromosome.TransformToAncestor(DragScope);
			Point originChromosomeControl = chromosomeToDragScope.Transform(new Point(0.0, 0.0));

            //Here we create our adorner...
			chromosomeAdorner = new DragChromosomeAdorner(DragScope, mouseCaptureChromosome.Chromosome, Scale, initialMouse, originChromosomeControl);
            adornerLayer = AdornerLayer.GetAdornerLayer(DragScope as Visual);
            adornerLayer.Add(chromosomeAdorner);

            // Do the dragging...
            inDragDrop = true;
            leftDown = false;
            dragHasLeftScope = false;
            
            var dragData = new ChromDragData
            {
                Chromosome = mouseCaptureChromosome.Chromosome,
                Group = this.Group,
                DragVisual = chromosomeAdorner
            };

            DragDrop.DoDragDrop(mouseCaptureChromosome, dragData, DragDropEffects.Move);

            // Clean up our mess :) 
            DragScope.AllowDrop = previousDrop;
            AdornerLayer.GetAdornerLayer(DragScope).Remove(chromosomeAdorner);
            chromosomeAdorner = null;

            groupControl.GiveFeedback -= feedbackHandler;
            DragScope.DragLeave -= dragLeaveHandler;
            DragScope.QueryContinueDrag -= queryHandler;
            DragScope.PreviewDragOver -= dragHandler;

            inDragDrop = false;
        }

        void DragSource_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            // This code is called when we are using a custom cursor (either a window or adorner)
            e.UseDefaultCursors = false;
            e.Handled = true;
        }

        void DragScope_DragLeave(object sender, DragEventArgs e)
        {
            if (e.OriginalSource == DragScope)
            {
                Point p = e.GetPosition(DragScope);
                Rect r = VisualTreeHelper.GetContentBounds(DragScope);
                if (!r.Contains(p))
                {
                    this.dragHasLeftScope = true;
                    e.Handled = true;
                }
            }
        }

        void DragOverEventHandler(object sender, DragEventArgs args)
        {
            if (chromosomeAdorner != null)
            {
                chromosomeAdorner.LeftOffset = args.GetPosition(DragScope).X;
                chromosomeAdorner.TopOffset = args.GetPosition(DragScope).Y;
            }
        }

        void DragScope_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            if (this.dragHasLeftScope)
            {
                e.Action = DragAction.Cancel;
                e.Handled = true;
            }
        }

        #endregion

        #endregion
    }

    public class DragChromosomeAdorner : Adorner
    {
        protected UIElement child;
		protected Point initialMouse;
		protected Point initialOffset;
        private DropShadowEffect dropFeedbackEffect;
        private double leftOffset;
        private double topOffset;

        public DragChromosomeAdorner(UIElement owner) : base(owner) { }

        public DragChromosomeAdorner(UIElement owner, KaryotypeChromosome chrom, double scale, Point mouse, Point offset)
            : base(owner)
        {
            System.Diagnostics.Debug.Assert(owner != null);

            Rectangle r = new Rectangle();
            r.Fill = new ImageBrush(chrom.DisplayImage);
            r.Width = chrom.DisplayImage.Width;
            r.Height = chrom.DisplayImage.Height;

            double flipXScale = chrom.IsXFlipped ? -1 : 1;
            double flipYScale = chrom.IsYFlipped ? -1 : 1;

            TransformGroup group = new TransformGroup();
			group.Children.Add(new TranslateTransform( -chrom.MidPoint.X, -chrom.MidPoint.Y));
            group.Children.Add(new RotateTransform(chrom.RotationToVertical));
			group.Children.Add(new TranslateTransform( chrom.Width/2.0, chrom.Length/2.0));
            group.Children.Add(new RotateTransform(chrom.UserRotation));
            group.Children.Add(new ScaleTransform(flipXScale, flipYScale));
            group.Children.Add(new ScaleTransform(scale, scale));
            r.RenderTransform = group;

            r.Opacity = 0.7;

			initialMouse = mouse;
			initialOffset = offset;

            child = r;

            dropFeedbackEffect = new DropShadowEffect();
            dropFeedbackEffect.Color = Colors.Transparent;
            dropFeedbackEffect.BlurRadius = 12;
            dropFeedbackEffect.ShadowDepth = 0;
            dropFeedbackEffect.Opacity = 0.6;
            child.Effect = dropFeedbackEffect;
        }

        public void SetAsInvalidDrop()
        {
            dropFeedbackEffect.Color = Colors.Red;
        }

        public void SetAsValidDrop()
        {
            dropFeedbackEffect.Color = Colors.Transparent;
        }
        
        public double LeftOffset
        {
            get { return leftOffset; }
            set
            {
				leftOffset = initialOffset.X + value - initialMouse.X;
                UpdatePosition();
            }
        }
        
        public double TopOffset
        {
            get { return topOffset; }
            set
            {
				topOffset = initialOffset.Y + value - initialMouse.Y;
                UpdatePosition();
            }
        }

        private void UpdatePosition()
        {
            AdornerLayer adorner = (AdornerLayer)this.Parent;
            if (adorner != null)
            {
                adorner.Update(this.AdornedElement);
            }
        }

        protected override Visual GetVisualChild(int index)
        {
            return child;
        }

        protected override int VisualChildrenCount
        {
            get { return 1; }
        }

        protected override Size MeasureOverride(Size finalSize)
        {
            child.Measure(finalSize);
            return child.DesiredSize;
        }
        protected override Size ArrangeOverride(Size finalSize)
        {
            child.Arrange(new Rect(child.DesiredSize));
            return finalSize;
        }

        public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            GeneralTransformGroup result = new GeneralTransformGroup();

            result.Children.Add(base.GetDesiredTransform(transform));
            result.Children.Add(new TranslateTransform(leftOffset, topOffset));
            return result;
        }
    }



    #region Converters

    public class OffsetsToMarginConverter : IMultiValueConverter
    {
        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            KaryotypeChromosome chrom = values[0] as KaryotypeChromosome;

            //return new Thickness(chrom.UserOffsetX, 0, 0, -chrom.UserOffsetY);
            return new Thickness(0, 0, 0, 0);
        }

        public object[] ConvertBack(object value, System.Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }

    #endregion
}
