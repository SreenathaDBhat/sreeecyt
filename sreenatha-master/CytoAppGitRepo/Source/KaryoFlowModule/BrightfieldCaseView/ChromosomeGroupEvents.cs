﻿using System;

namespace AI.Brightfield
{
    public class ChromosomeClickEventArgs : EventArgs
    {
        public KaryotypeChromosome Chromosome { get; internal set; }
        public int ClickCount { get; internal set; }
    }
}
