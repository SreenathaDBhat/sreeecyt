﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Globalization;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace AI.Brightfield
{
    public enum ClearLevel
    {
        NoOverlaps,
        P_Arm,
        Q_Arm,
        NotCleared
    }

    public class ChromosomeClearAction
    {
        public Guid Id { get; set; }
        public Guid CellId { get; set; }
        public string ChromosomeTag { get; set; }
        public ClearLevel ClearLevel { get; set; }
    }

    public class ChromosomeClearStatus : INotifyPropertyChanged
    {
        private bool pClear = false;
        private bool qClear = false;

        public bool PArmCleared 
        {
            get { return pClear; }
            set { pClear = value; NotifyAll(); }
        }

        public bool QArmCleared
        {
            get { return qClear; }
            set { qClear = value; NotifyAll(); }
        }
        
        public bool AtLeastPartiallyCleared
        {
            get { return QArmCleared || PArmCleared; }
        }

        public bool FullyCleared
        {
            get { return QArmCleared && PArmCleared; }
        }

        public bool PArmOnly
        {
            get { return PArmCleared && !QArmCleared; }
        }

        public bool QArmOnly
        {
            get { return QArmCleared && !PArmCleared; }
        }

        public ClearLevel ClearLevel
        {
            get
            {
                if (FullyCleared) return ClearLevel.NoOverlaps;
                else if (PArmOnly) return ClearLevel.P_Arm;
                else if (QArmOnly) return ClearLevel.Q_Arm;
                else return ClearLevel.NotCleared;
            }
            set
            {
                if (value == ClearLevel.NoOverlaps)
                {
                    PArmCleared = true;
                    QArmCleared = true;
                }
                else if (value == ClearLevel.P_Arm)
                {
                    PArmCleared = true;
                    QArmCleared = false;
                }
                else if (value == ClearLevel.Q_Arm)
                {
                    PArmCleared = false;
                    QArmCleared = true;
                }
                else
                {
                    PArmCleared = false;
                    QArmCleared = false;
                }
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyAll()
        {
            Notify("PArmCleared");
            Notify("QArmCleared"); 
            Notify("AtLeastPartiallyCleared"); 
            Notify("FullyCleared");
            Notify("PArmOnly");
            Notify("QArmOnly");
        }

        private void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }

    public class ChromosomeGroup : Notifier
    {
        private ObservableCollection<KaryotypeChromosome> chromosomes; 
        public static readonly string UnclassifiedString = "Unclassified";
        public static readonly string SexStringX = "X";
        public static readonly string SexStringY = "Y";

        private string id;
        private ChromosomeClearStatus clearingStatus = new ChromosomeClearStatus();

        private ChromosomeGroup(string id, Guid cell)
        {
            Cell = cell;
            chromosomes = new ObservableCollection<KaryotypeChromosome>();
            this.id = id;
        }

        public string Identifier
        {
            get { return id; }
        }
        
        public ChromosomeClearStatus ClearingStatus
        {
            get { return clearingStatus; }
        }

        private static IEnumerable<string> HumanGroups
        {
            get
            {
                for (int i = 1; i <= 22; ++i)
                {
                    yield return i.ToString();
                }

                yield return SexStringX;
                yield return SexStringY;
                yield return UnclassifiedString;
            }
        }

        public static IEnumerable<ChromosomeGroup> GenerateHumanBuckets(Guid parentCell)
        {
            return from g in HumanGroups 
                   select new ChromosomeGroup(g, parentCell);
        }

        public static IEnumerable<string> GroupsForCase(Guid caseId)
        {
            return HumanGroups;
        }

        public static ChromosomeGroup GroupForClass(string classId, Guid parentCell)
        {
            return new ChromosomeGroup(classId, parentCell);
        }

        public void AdoptWithoutArrange(KaryotypeChromosome chrom)
        {
            AdoptWithoutArrange(chrom, chromosomes.Count);
        }

        private void AdoptWithoutArrange(KaryotypeChromosome obj, int index)
        {
            MoveChromToThisGroup(obj, index);
        }
        
        public void Adopt(KaryotypeChromosome obj)
        {
            Adopt(obj, chromosomes.Count);
        }

        public void Adopt(KaryotypeChromosome obj, int index)
        {
            MoveChromToThisGroup(obj, index);
            Arrange();
            Notify("IsEmpty");
        }

        private void MoveChromToThisGroup(KaryotypeChromosome obj, int index)
        {
            if (obj.Group != null)
                obj.Group.Remove(obj);

            obj.Group = this;
            chromosomes.Insert(Math.Min(chromosomes.Count, index), obj);
            Notify("IsEmpty");
        }

        public Guid Cell { get; set; }

        public IEnumerable<KaryotypeChromosome> Chromosomes
        {
            get { return chromosomes; }
        }

        public bool IsEmpty
        {
            get { return chromosomes.Count == 0; }
        }

        public void Clear()
        {
            chromosomes.Clear();
            Notify("IsEmpty");
        }

        public void Remove(KaryotypeChromosome chrom)
        {
            if (chromosomes.Contains(chrom))
                chromosomes.Remove(chrom);

            Arrange();
            Notify("IsEmpty");
        }

        public void Swap(KaryotypeChromosome a, KaryotypeChromosome b)
        {
            var ia = chromosomes.IndexOf(a);
            var ib = chromosomes.IndexOf(b);
            

            if (ia < ib)
            {
                chromosomes.RemoveAt(ib);
                chromosomes.RemoveAt(ia);
                
                chromosomes.Insert(ia, b);
                chromosomes.Insert(ib, a);
            }
            else
            {
                chromosomes.RemoveAt(ia);
                chromosomes.RemoveAt(ib);

                chromosomes.Insert(ib, a);
                chromosomes.Insert(ia, b);
            }

            Arrange();
        }

        public void InsertBefore(KaryotypeChromosome chrom, KaryotypeChromosome before)
        {
            if (chrom.Group != null)
                chrom.Group.Remove(chrom);

            var i = chromosomes.IndexOf(before);
            chromosomes.Insert(i, chrom);
            chrom.Group = this;

            Arrange();
            Notify("IsEmpty");
        }

        public void Insert(int index, KaryotypeChromosome chromosome)
        {
            if (chromosome.Group != null)
                chromosome.Group.Remove(chromosome);

            chromosomes.Insert(index, chromosome);
            chromosome.Group = this;

            Arrange();
            Notify("IsEmpty");
        }

        public static void WriteToDb(IEnumerable<ChromosomeGroup> groups, Guid caseId)
        {
            // NYI
        }

        public bool Contains(KaryotypeChromosome chrom)
        {
            foreach (var k in chromosomes)
            {
                if (k == chrom)
                    return true;
            }

            return false;
        }

        public int IndexOf(KaryotypeChromosome karyotypeChromosome)
        {
            return chromosomes.IndexOf(karyotypeChromosome);
        }

        private void Arrange()
        {
            foreach (var chrom in chromosomes)
            {
                chrom.UserOffsetX = 0.0;
                chrom.UserOffsetY = 0.0;
            }

            if (Identifier == UnclassifiedString)
            {
                var sorted = chromosomes.OrderByDescending(c => c.Length).ToArray();
                chromosomes.Clear();

                foreach (var item in sorted)
                    chromosomes.Add(item);
            }
        }
    }
}
