﻿using System;

namespace AI.Brightfield
{
    public partial class CountImageUi
    {
        private CountController counter;

        public CountImageUi(CountController counter)
        {
            this.counter = counter;
            InitializeComponent();
        }

        private void OnPlacePoint(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pt = e.GetPosition(this);
            counter.AddPointAt(pt.X, pt.Y);
        }

        private void OnDeletePoint(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pt = e.GetPosition(this);
            counter.RemovePointAt(pt.X, pt.Y);
        }
    }
}
