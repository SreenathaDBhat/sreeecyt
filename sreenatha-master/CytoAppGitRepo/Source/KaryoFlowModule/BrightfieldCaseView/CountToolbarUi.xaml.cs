﻿using System;

namespace AI.Brightfield
{
    public partial class CountToolbarUi
    {
        private CountController counter;

        public CountToolbarUi(CountController counter)
        {
            this.counter = counter;
            InitializeComponent();
        }

        private void CanClearExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = counter.CanClear;
        }

        private void ClearExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            counter.Clear();
        }
    }
}
