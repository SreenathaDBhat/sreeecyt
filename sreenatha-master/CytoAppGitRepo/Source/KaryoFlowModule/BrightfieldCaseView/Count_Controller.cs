﻿using System;
using System.Linq;
using System.Windows;
using System.Collections.ObjectModel;
using AI.Brightfield.ManualCountImplementation;
using System.Collections.Generic;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace AI.Brightfield
{
    namespace ManualCountImplementation
    {
        public class ChromosomePoint
        {
            public Point Location { get; set; }
        }

        public class UndoPointAdd : IUndoItem
        {
            private ChromosomePoint pointAdded;
            private IList<ChromosomePoint> list;

            public UndoPointAdd(ChromosomePoint pointAdded, IList<ChromosomePoint> list)
            {
                this.list = list;
                this.pointAdded = pointAdded;
            }

            public void Undo(object state)
            {
                list.Remove(pointAdded);
            }
        }

        public class UndoPointRemove : IUndoItem
        {
            private ChromosomePoint pointRemoved;
            private IList<ChromosomePoint> list;

            public UndoPointRemove(ChromosomePoint pointRemoved, IList<ChromosomePoint> list)
            {
                this.list = list;
                this.pointRemoved = pointRemoved;
            }

            public void Undo(object state)
            {
                list.Add(pointRemoved);
            }
        }

        public class UndoClear : IUndoItem
        {
            private List<ChromosomePoint> pointsRemoved;
            private IList<ChromosomePoint> list;

            public UndoClear(IList<ChromosomePoint> list)
            {
                this.list = list;
                pointsRemoved = new List<ChromosomePoint>();
                pointsRemoved.AddRange(list);
            }

            public void Undo(object state)
            {
                foreach (var p in pointsRemoved)
                    list.Add(p);
            }
        }
    }

    public static class CountCommands
    {
        public static RoutedUICommand Clear = new RoutedUICommand("Clear", "Clear", typeof(CountCommands));
    }

    public sealed class CountController : ToolNotifier
    {
        private ObservableCollection<ChromosomePoint> points;
        private Cell cell;
        private bool showPoints;
        private UndoList undoList;
        private WorkflowController controller;

        public CountController()
        {
            showPoints = true;
            undoList = new UndoList();
        }

        public override void Load(Cell cell, WorkflowController controller)
        {
            this.cell = cell;
            this.controller = controller;

            var dataSource = DataSourceFactory.GetDataSource();
            var countXml = dataSource.LoadXml(controller.RelativeIdCell, "cell.count");

            if (countXml != null)
            {
                points = new ObservableCollection<ChromosomePoint>(from ep in countXml.Descendants("Point")
                                                                   select new ChromosomePoint
                                                                   {
                                                                       Location = new Point(double.Parse(ep.Attribute("x").Value), double.Parse(ep.Attribute("y").Value))
                                                                   });
            }
            else
                points = new ObservableCollection<ChromosomePoint>();

        }

        public override UndoList UndoList
        {
            get { return undoList; }
        }

        public override void Save()
        {

            var xml = new XElement("ManualCount", 
                from p in points select new XElement("Point", 
                                                new XAttribute("x", p.Location.X), 
                                                new XAttribute("y", p.Location.Y)));

            var dataSource = DataSourceFactory.GetDataSource();
            dataSource.SaveXml(controller.RelativeIdCell, xml, "cell.count");
//				dbCell.counted = GotPoints;

            //using (var lims = Connect.LIMS.New())
            //{
            //    lims.MarkCaseAsModified(controller.Case, DateTime.Now, "Manual Count");
            //}

			controller.SetCounted(cell, GotPoints);
            undoList.Clear();
        }

        public override FrameworkElement ImageUi
        {
            get { return new CountImageUi(this); }
        }

        public override StretchSizeMode ImageUiSizeMode
        {
            get { return StretchSizeMode.Size; }
        }

        public override FrameworkElement ScreenUi
        {
            get { return null; }
        }

        public override FrameworkElement ToolbarUi
        {
            get { return new CountToolbarUi(this); }
        }

        public bool ShowPoints
        {
            get { return showPoints; }
            set { showPoints = value; Notify("ShowPoints"); }
        }

        public IEnumerable<ChromosomePoint> Points
        {
            get { return points; }
        }

		public bool GotPoints
		{
			get { return points.FirstOrDefault() != null; }
		}

        public void AddPointAt(double x, double y)
        {
            Point newPoint = new Point(x, y);

            ChromosomePoint point = new ChromosomePoint { Location = newPoint };
            points.Add(point);
            undoList.Add(new UndoPointAdd(point, points));

            ShowPoints = true;
        }

        public void RemovePointAt(double x, double y)
        {
            ChromosomePoint point = PointAtLocation(x, y);
            if (point != null)
            {
                points.Remove(point);
                undoList.Add(new UndoPointRemove(point, points));
            }
        }

        private ChromosomePoint PointAtLocation(double x, double y)
        {
            double hitDist = 15 * 15;

            for (int i = points.Count - 1; i >= 0; --i)
            {
                var pt = points[i];
                double d = Math.Pow(pt.Location.X - x, 2) + Math.Pow(pt.Location.Y - y, 2);
                if (d <= hitDist)
                {
                    return pt;
                }
            }

            return null;
        }

        public bool CanClear
        {
            get { return points.Count > 0; }
        }

        public void Clear()
        {
            undoList.Add(new UndoClear(points));
            points.Clear();
        }

        public override void OnKeyDown(ToolKeyEventArgs e)
        {
            
        }

        public override void OnKeyUp(ToolKeyEventArgs e)
        {
            
        }
    }
}
