﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Media.Effects;

namespace AI.Brightfield
{
    public abstract class ToolNotifier : IBrightfieldWorkflowTool
    {
        public abstract void Load(Cell cell, WorkflowController controller);
        public abstract void Save();

        public abstract FrameworkElement ImageUi { get; }
        public abstract FrameworkElement ScreenUi { get; }
        public abstract FrameworkElement ToolbarUi { get; }
        public abstract UndoList UndoList { get; }
        public abstract StretchSizeMode ImageUiSizeMode { get; }

        public abstract void OnKeyDown(ToolKeyEventArgs e);
        public abstract void OnKeyUp(ToolKeyEventArgs e);

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual bool ShowMainImage { get { return true; } }

        public void Notify(params string[] notifications)
        {
            if(PropertyChanged == null)
                return;

            foreach (var n in notifications)
                PropertyChanged(this, new PropertyChangedEventArgs(n));
        }

        public virtual void OnDispayChanged()
        {
        }

        public virtual bool RequiresFullImage { get { return false; } }

        public virtual BitmapSource PrintImage { get { return null; } }

        public virtual bool IsDisplayPopupEnabled { get { return true; } }
    }


    public sealed class DefaultToolController : ToolNotifier
    {
        private UndoList undo = new UndoList();

        public override void Load(Cell cell, WorkflowController controller)
        {
        }

        public override void Save()
        {
        }

        public override FrameworkElement ScreenUi
        {
            get { return null; }
        }

        public override FrameworkElement ImageUi
        {
            get { return null; }
        }

        public override FrameworkElement ToolbarUi
        {
            get { return null; }
        }

        public override StretchSizeMode ImageUiSizeMode
        {
            get { return StretchSizeMode.Stretch; }
        }

        public override UndoList UndoList
        {
            get { return undo; }
        }

        public override void OnKeyDown(ToolKeyEventArgs e)
        {
        }

        public override void OnKeyUp(ToolKeyEventArgs e)
        {
        }
    }
}
