﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace AI
{
    public static class DirtyFrameToBitmap
    {
        public static BitmapSource Convert(Guid frameId, Guid caseId)
        {
            var frame = ImageFrameDbLoad.FromDb(frameId, caseId);

            foreach (var c in frame.Channels)
            {
                frame.FindSnap(c, 0).LoadDataIfNeeded(caseId);
            }

            return RGBImageComposer.Compose(frame);
        }
    }
}
