﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace AI.Brightfield
{
    public static class FocusUtilities
    {
        /// <summary>
        /// A simple iterator method to expose the visual tree to LINQ
        /// </summary>
        /// <param name="start"></param>
        /// <returns></returns>
        public static IEnumerable<DependencyObject> EnumerateVisualTree(this DependencyObject start)
        {
            yield return start;
            foreach (var e in EnumerateChildren(start))
                yield return e;
        }

        private static IEnumerable<DependencyObject> EnumerateChildren(DependencyObject start)
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(start); i++)
            {
                var child = VisualTreeHelper.GetChild(start, i);
                if (child != null)
                {
                    yield return child;
                    foreach (var childOfChild in EnumerateChildren(child))
                        yield return childOfChild;
                }
            }
        }
    }
}
