﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.Brightfield
{
	/// <summary>
	/// When asked to perform a histogram estimate it returns the full range, ie no contrast stretch is performed.
	/// </summary>
	public class FullRangeHistogramEstimator : IHistogramEstimator
	{
		public FullRangeHistogramEstimator()
		{
		}

		public override void Estimate(ImageFrame imageFrame, ChannelRenderParameters channelParams, IEnumerable<RenderPass> passes)
		{
			channelParams.Low = 0.0;
			channelParams.High = 1.0;
		}

	}
}
