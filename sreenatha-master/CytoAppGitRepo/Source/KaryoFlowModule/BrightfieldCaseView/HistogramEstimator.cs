﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;



namespace AI.Brightfield
{
    /// <summary>
    /// The interface methods take an 8 bit histogram of a bimodal distribution and estimate the high and low bins which
    /// provide a "nice" contrast stretch setting the background to white. In other words the bright peak is treated as background.
    /// </summary>
    public unsafe class BiModalHistogramEstimator : IHistogramEstimator
    {
        public BiModalHistogramEstimator()
        {
        }

		public override void Estimate(ImageFrame imageFrame, ChannelRenderParameters channelParams, IEnumerable<RenderPass> passes)
		{
			HistogramValue range = PeakValleyHeuristicEstimate( channelParams);
			//EntropyEstimate( channelParams);

			channelParams.Low = range.Low;
			channelParams.High = range.High;
		}

		public HistogramValue EntropyEstimate(ChannelRenderParameters channelParams)
		{
            // Default estimate to full range
			HistogramValue range = new HistogramValue
			{
				Low = 0,
				High = 0.99
			};

            // Remember all bin values have been scaled to 1000
            int binCount = channelParams.HistogramBins.Length;
			int[] hist = channelParams.HistogramBins;

			// Normalize histogram, that is makes the sum of all bins equal to 1.
			double sum = 0;
			for (int i = 0; i < binCount; ++i)
			{
				sum += hist[i];
			}

			if (sum == 0)
			{
				// This should not normally happen, but...
				Debug.WriteLine( "EntropyEstimate: Empty histogram: sum of all bins is zero.");
				return range;
			}

			double[] normalizedHist = new double[binCount];
			for (int i = 0; i < binCount; i++)
			{
				normalizedHist[i] = hist[i] / sum;
			}

			// The probability mass function values
			double[] pT = new double[binCount];
			pT[0] = normalizedHist[0];
			for (int i = 1; i < binCount; i++)
			{
				pT[i] = pT[i - 1] + normalizedHist[i];
			}

			// Entropy for black and white parts of the histogram
			double epsilon = double.Epsilon;
			double[] hB = new double[binCount];
			double[] hW = new double[binCount];
			for (int t = 0; t < binCount; t++)
			{
				// Black entropy
				if (pT[t] > epsilon)
				{
					double hhB = 0;
					for (int i = 0; i <= t; i++)
					{
						if (normalizedHist[i] > epsilon)
						{
							hhB -= normalizedHist[i] / pT[t] * Math.Log(normalizedHist[i] / pT[t]);
						}
					}
					hB[t] = hhB;
				}
				else
				{
					hB[t] = 0;
				}

				// White  entropy
				double pTW = 1 - pT[t];
				if (pTW > epsilon)
				{
					double hhW = 0;
					for (int i = t + 1; i < binCount; ++i)
					{
						if (normalizedHist[i] > epsilon)
						{
							hhW -= normalizedHist[i] / pTW * Math.Log(normalizedHist[i] / pTW);
						}
					}
					hW[t] = hhW;
				}
				else
				{
					hW[t] = 0;
				}
			}

			// Find histogram index with maximum entropy
			double jMax = hB[0] + hW[0];
			int tMax = 0;
			for (int t = 1; t < binCount; ++t)
			{
				double j = hB[t] + hW[t];
				if (j > jMax)
				{
					jMax = j;
					tMax = t;
				}
			}

			range.High = (((double)tMax) / binCount) * 0.99;
			return range;
		}

		public HistogramValue PeakValleyHeuristicEstimate( ChannelRenderParameters channelParams)
        {
            // Default estimate to full range
            HistogramValue range = new HistogramValue
			{
				Low = 0,
				High = 0.99
			};

            // Remember all bin values have been scaled to 1000
            int binCount = channelParams.HistogramBins.Length;
            int[] smoothHisto = new int[binCount];

            // Smooth histo
            int halfWidth = 4;
            int count = halfWidth * 2 + 1;
            fixed (int* histo = &channelParams.HistogramBins[0])
            fixed (int* result = &smoothHisto[0])
            {
                int* h = histo + halfWidth;
                int* r = result + halfWidth;

                double sum;
                for (int i = halfWidth; i < binCount - halfWidth; i++, h++, r++)
                {
                    sum = 0.0;
                    for (int j = -halfWidth; j < halfWidth; j++)
                    {
                        sum += h[j];
                    }

                    *r = (int)sum / count;
                }
            }

            // From the right end of the histogram find the bright peak and the next valley
            fixed (int* histo = &smoothHisto[0])
            {
                // ignore the end bins on either side of the histogram...usually full of junk
				int junk = halfWidth;
                int* h = histo + smoothHisto.Length - junk;

                // Look for peaks/valleys within the window
                int window = 4;
                bool peakFound = false;
                bool valleyFound = false;
                int peakIndex = 0;
                int valleyIndex = 0;
                for (int i = smoothHisto.Length - junk - 1; i >= junk; i--, h--)
                {
                    if (!peakFound)
                    {
                        if (h[-window] < h[0] && h[window] <= h[0])
                        {
                            peakIndex = i;
                            peakFound = true;
                        }
                    }
                    else if (!valleyFound)
                    {
                        if (h[-window] >= h[0] && h[window] > h[0])
                        {
                            valleyIndex = i;
                            valleyFound = true;
                            break;
                        }
                    }
                }

                if (peakFound && valleyFound)
                {
                    double peakvalleyDistance = peakIndex - valleyIndex;
                    double high = (valleyIndex + ((2.0 * peakvalleyDistance) / 3.0)) / binCount;
                    range.High = high * 0.99;
                }

				return range;
            }
        }

    }
}
