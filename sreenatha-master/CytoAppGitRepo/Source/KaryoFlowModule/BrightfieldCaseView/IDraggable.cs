﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using System.Collections.Generic;

namespace AI
{
    public interface IDraggable
    {
        double TranslateX { get; set; }
        double TranslateY { get; set; }
        double Rotation { get; set; }
    }

    public enum DragType { Translate, Rotate };

    public class ChromosomeDragger
    {
        public delegate void DragFinished(IDraggable chromosomeBeingDragged, DragType dragType);

        private IDraggable objectBeingDragged;
        private Point lastPt, originalPt, offset;
        private DragType dragType;
        private bool outsideDeadzone;
        private FrameworkElement frameworkElement;
        private DragFinished onDragFinished;
        private MouseEventHandler onClick;
        private MouseEventHandler onMouseMove;

        public static ChromosomeDragger StartTranslateDrag(IDraggable chromsome, MouseEventArgs e, FrameworkElement frameworkElement, DragFinished onDragFinished, MouseEventHandler onClick)
        {
            return StartTranslateDrag(chromsome, e, frameworkElement, onDragFinished, onClick, null, new Point());
        }

        public static ChromosomeDragger StartTranslateDrag(IDraggable chromsome, MouseEventArgs e, FrameworkElement frameworkElement, DragFinished onDragFinished, MouseEventHandler onClick, MouseEventHandler onMouseMove, Point offset)
        {
            return new ChromosomeDragger(chromsome, e, DragType.Translate, frameworkElement, onDragFinished, onClick, onMouseMove, offset);
        }

        public static ChromosomeDragger StartRotateDrag(IDraggable chromsome, MouseEventArgs e, FrameworkElement frameworkElement, DragFinished onDragFinished, MouseEventHandler onClick)
        {
            return new ChromosomeDragger(chromsome, e, DragType.Rotate, frameworkElement, onDragFinished, onClick, null, new Point());
        }

        private ChromosomeDragger(IDraggable chromsome, MouseEventArgs e, DragType dragType, FrameworkElement frameworkElement, DragFinished onDragFinished, MouseEventHandler onClick, MouseEventHandler onMouseMove, Point offset)
        {
            outsideDeadzone = false;
            this.frameworkElement = frameworkElement;
            this.dragType = dragType;
            this.onDragFinished = onDragFinished;
            this.onClick = onClick;
            this.onMouseMove = onMouseMove;
            this.offset = offset;

            objectBeingDragged = chromsome;

            frameworkElement.CaptureMouse();

            var startPt = e.GetPosition(frameworkElement);
            e.Handled = true;
            lastPt = startPt;
            originalPt = startPt;

            frameworkElement.MouseMove += MouseMove;
            frameworkElement.MouseUp += MouseUp;
        }

        void MouseUp(object sender, MouseButtonEventArgs e)
        {
            frameworkElement.ReleaseMouseCapture();
            frameworkElement.MouseMove -= MouseMove;
            frameworkElement.MouseUp -= MouseUp;

            if (!outsideDeadzone)
            {
                if (onClick != null)
                    onClick(objectBeingDragged, e);

                return;
            }

            if (onDragFinished != null && objectBeingDragged != null)
                onDragFinished(objectBeingDragged, dragType);

            objectBeingDragged = null;
        }

        private void MouseMove(object sender, MouseEventArgs e)
        {
            if (objectBeingDragged == null)
                return;

            Point curPt = e.GetPosition(frameworkElement);
            curPt = new Point(curPt.X + offset.X, curPt.Y + offset.Y);
            e.Handled = true;

            double dx = curPt.X - lastPt.X;
            double dy = curPt.Y - lastPt.Y;

            if (!outsideDeadzone)
            {
                outsideDeadzone = Math.Max(Math.Abs(curPt.X - originalPt.X), Math.Abs(curPt.Y - originalPt.Y)) > 3;

                if (!outsideDeadzone)
                    return;
            }

            if (dragType == DragType.Translate)
            {
                objectBeingDragged.TranslateX += dx;
                objectBeingDragged.TranslateY += dy;
            }
            else if (dragType == DragType.Rotate)
            {
                objectBeingDragged.Rotation = objectBeingDragged.Rotation - dx;
            }

            if (onMouseMove != null)
            {
                onMouseMove(objectBeingDragged, e);
            }

            lastPt = curPt;
        }
    }
}
