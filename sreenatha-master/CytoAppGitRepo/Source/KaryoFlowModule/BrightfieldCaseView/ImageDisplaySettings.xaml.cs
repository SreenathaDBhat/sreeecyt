﻿using System;
using System.Windows.Data;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;

namespace AI.Brightfield
{
    public partial class ImageDisplaySettings
    {
        public static readonly DependencyProperty ImageProperty = DependencyProperty.Register("Image", typeof(ImageRenderer), typeof(ImageDisplaySettings), new FrameworkPropertyMetadata(OnImageChanged));

        private enum Drag { None, Low, High };
        private Drag drag = Drag.None;

        public ImageDisplaySettings()
        {
            InitializeComponent();
        }

        private static void OnImageChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((ImageDisplaySettings)sender).DataContext = e.NewValue;
        }

        public ImageRenderer Image
        {
            get { return (ImageRenderer)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            var element = (FrameworkElement)sender;
            ((FrameworkElement)sender).CaptureMouse();

            double x = NormalizedX(element, e);
            drag = (Math.Abs(Image.HistogramLow - x) < Math.Abs(Image.HistogramHigh - x)) ? Drag.Low : Drag.High;

            Console.WriteLine(drag);

            DoDrag(element, e);
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var element = (FrameworkElement)sender;
            if (drag == Drag.None)
                return;

            e.Handled = true;
            DoDrag(element, e);
        }

        private void DoDrag(FrameworkElement element, MouseEventArgs e)
        {
            double v = NormalizedX(element, e);
            double minGap = 0.025;

            if (drag == Drag.Low)
            {
                Image.HistogramLow = Math.Max(0, Math.Min(v, Image.HistogramHigh - minGap));
            }
            else if (drag == Drag.High)
            {
                Image.HistogramHigh = Math.Min(1, Math.Max(Image.HistogramLow + minGap, v));
            }
        }

        private void OnMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            drag = Drag.None;
            ((FrameworkElement)sender).ReleaseMouseCapture();
            e.Handled = true;
        }

        private static double NormalizedX(FrameworkElement element, MouseEventArgs e)
        {
            double x = e.GetPosition(element).X;
            double v = x / element.ActualWidth;
            return v;
        }
    }




    #region Converters

    public class BinsToPointsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            PointCollection points = new PointCollection(514);
            if (value == null)
                return points;

            int[] bins = (int[])value;

            points.Add(new Point(0, 50));
            for (int i = 0; i < 256; ++i)
            {
                double b = ((Math.Log(bins[i] + 3, 3) - 1) / 5.3) * 45;
                points.Add(new Point(i, 100 - (b + 50)));
            }
            points.Add(new Point(255, 50));
            for (int i = 255; i >= 0; --i)
            {
                double b = ((Math.Log(bins[i] + 3, 3) - 1) / 5.3) * 45;
                points.Add(new Point(i, b + 50));
            }

            return points;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class HistLowHighToWidthConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue)
                return 0;

            double low = (double)values[0];
            double high = (double)values[1];

            return (high - low) * 256;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class HistLowHighToMarginConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return new Thickness(0, 0, 0, 0);

            double v = (double)value;
            return new Thickness(v * 256, 0, 0, 0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class DarkenColorConverter : IValueConverter
    {
        public double DarkenFactor { get; set; }

        public DarkenColorConverter()
        {
            DarkenFactor = 0.6;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var c = (System.Windows.Media.Color)value;
            return System.Windows.Media.Color.FromArgb(
                (byte)(255),
                (byte)(c.R * DarkenFactor),
                (byte)(c.G * DarkenFactor),
                (byte)(c.B * DarkenFactor));
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    #endregion
}
