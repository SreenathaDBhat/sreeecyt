﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace AI
{
    internal class RememberedImageParameters
    {
        public double HistogramLow { get; set; }
        public double HistogramHigh { get; set; }

        public void Apply(ImageRenderer image)
        {
            image.HistogramLow = HistogramLow;
            image.HistogramHigh = HistogramHigh;
        }
    }

    public class ImageParameterRememberer
    {
        private Dictionary<Guid, RememberedImageParameters> rememberedParameters;
        private Guid caseId;

        public ImageParameterRememberer(Guid caseId)
        {
            this.caseId = caseId;
            rememberedParameters = new Dictionary<Guid, RememberedImageParameters>();
        }

        public Guid CaseId
        {
            get { return caseId; }
        }

        public void Forget(ImageFrame image)
        {
            if (image != null && rememberedParameters.ContainsKey(image.Id))
                rememberedParameters.Remove(image.Id);
        }

        public void Remember(ImageFrame image, ImageRenderer renderer)
        {
            RememberedImageParameters imageParams = null;
            if (rememberedParameters.ContainsKey(image.Id))
            {
                imageParams = rememberedParameters[image.Id];
            }
            else
            {
                imageParams = new RememberedImageParameters();
                rememberedParameters.Add(image.Id, imageParams);
            }

            imageParams.HistogramLow = renderer.HistogramLow;
            imageParams.HistogramHigh = renderer.HistogramHigh;
        }

        public void Apply(ImageFrame frame, ImageRenderer renderer)
        {
            if(rememberedParameters.ContainsKey(frame.Id))
                rememberedParameters[frame.Id].Apply(renderer);
        }

        private FileInfo SaveFile
        {
            get { return AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\DisplayOptions\\" + CaseId.ToString() + ".display.xml"); }
        }

        public void Save()
        {
            var file = SaveFile.FullName;

            if (File.Exists(file))
                File.Delete(file);

            if (rememberedParameters.Count == 0)
                return;

            XElement xml = new XElement("ImageParameters", from imageId in rememberedParameters.Keys
                                                           select ImageXmlNode(imageId));

            xml.Save(file);
        }

        public void Load()
        {
            var file = SaveFile.FullName;

            if(!File.Exists(file))
                return;

            XElement xml = XElement.Load(file);
            foreach (var imageNode in xml.Descendants("Image"))
            {
                RememberedImageParameters imageParams = new RememberedImageParameters();
                Guid key = new Guid(imageNode.Attribute("Id").Value);
                imageParams.HistogramLow = double.Parse(imageNode.Attribute("Low").Value);
                imageParams.HistogramHigh = double.Parse(imageNode.Attribute("High").Value);
                rememberedParameters.Add(key, imageParams);
            }
        }

        private XElement ImageXmlNode(Guid imageId)
        {
            var imageParams = rememberedParameters[imageId];

            XElement xml = new XElement("Image");
            xml.Add(
                new XAttribute("Id", imageId.ToString()),
                new XAttribute("Low", imageParams.HistogramLow),
                new XAttribute("High", imageParams.HistogramHigh));

            return xml;
        }
    }
}
