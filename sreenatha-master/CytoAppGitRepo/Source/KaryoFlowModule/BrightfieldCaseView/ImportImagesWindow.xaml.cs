﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Threading;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.IO;
using Microsoft.Win32;
using System.Windows;
using System.Xml.Linq;

namespace AI.Brightfield
{
    public partial class ImportImagesWindow
    {
        private ImportImagesController importer;
        private IEnumerable<string> filesToImport;
        private WorkflowController workflowController;

        public ImportImagesWindow(IEnumerable<string> filesToImport, WorkflowController workflowController)
        {
            this.workflowController = workflowController;
            this.filesToImport = filesToImport;
            importer = new ImportImagesController(workflowController);
            DataContext = importer;
            InitializeComponent();

            Loaded += (s, e) =>
                {
                    if (workflowController.Slides == null || workflowController.Slides.Count() == 0)
                    {
//                        if (Connect.LIMS.CanCreateCases)
//                        {
                            ImportIntoNewSlide();
//                        }
                    }
                };
        }

        private void OnDragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void OnImportIntoNewSlide(object sender, System.Windows.RoutedEventArgs e)
        {
            ImportIntoNewSlide();
        }

        private void ImportIntoNewSlide()
        {
            NewSlideWindow w = new NewSlideWindow(workflowController.NextAvailableSlideName(), workflowController.StringTable, workflowController.Slides);
            w.Owner = VisualTreeWalker.FindParentOfType<Window>(this);
            if (w.ShowDialog().GetValueOrDefault(false))
            {
                var name = w.NewSlideName;
                var slide = importer.WorkflowController.CreateNewSlide(name);
                importer.TargetSlide = slide;

                Task.Factory.StartNew(() => importer.Import(Dispatcher, filesToImport, OnImportComplete));
            }
        }

        private void OnCancel(object sender, System.Windows.RoutedEventArgs e)
        {
            Close();
        }

        private void OnOK(object sender, System.Windows.RoutedEventArgs e)
        {
            Task.Factory.StartNew(() => importer.Import(Dispatcher, filesToImport, OnImportComplete));
        }

        private void OnImportComplete()
        {
            if (importer.ImportErrors)
            {
                SimpleMessageBox box = new SimpleMessageBox(VisualTreeWalker.FindParentOfType<Window>(this), workflowController.StringTable.Lookup("ImportImageNotSupported"));
                box.ShowDialog();
            }
            Close();
        }
    }



    public class ImportImagesController : Notifier
    {
        private Slide targetSlide;
        private WorkflowController workflowController;

        private static readonly string ImportImagesTitleKey = "ImportImagesDialogTitle";
        public static bool DoImport(WorkflowController controller, Window parentWindow)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = "Image Files|*.jpg;*.png;*.tiff;*.tif;*.bmp;*.raw|All Files|*.*";
            fd.Multiselect = true;
            fd.Title = controller.StringTable.Lookup(ImportImagesTitleKey);
            fd.RestoreDirectory = true;
            if (fd.ShowDialog().GetValueOrDefault(false))
            {
                ImportImagesWindow import = new ImportImagesWindow(fd.FileNames, controller);
                import.Owner = parentWindow;
                import.ShowDialog();
                return true;
            }

            return false;
        }

        public ImportImagesController(WorkflowController workflowController)
        {
            this.workflowController = workflowController;
            targetSlide = workflowController.Slides == null ? null : workflowController.Slides.FirstOrDefault();
        }

        public double Progress { get; private set; }
        public bool InProgress { get; private set; }
        public WorkflowController WorkflowController { get { return workflowController; } }
        public Slide TargetSlide
        {
            get { return targetSlide; }
            set { targetSlide = value; Notify("TargetSlide"); }
        }

        public bool ImportErrors { get; private set; }

        public void Import(Dispatcher uiDispatcher, IEnumerable<string> filesToImport, ThreadStart completeCallback)
        {
            ImportErrors = false;

            uiDispatcher.Invoke((ThreadStart)delegate
                {
                    InProgress = true;
                    Notify("InProgress");

                }, DispatcherPriority.Normal);

            double progressStep = 1.0 / filesToImport.Count();
            Progress = 0;
            var newCells = new List<Cell>();

            foreach (var imagePath in filesToImport)
            {
                Progress += progressStep;

                try
                {
                    var newCell = Import(imagePath, workflowController, targetSlide);
                    newCells.Add(newCell);
                }
                catch
                {
                    ImportErrors = true;
                }

                uiDispatcher.Invoke((ThreadStart)delegate
                {
                    Notify("Progress");

                }, DispatcherPriority.Normal);
            }

            uiDispatcher.Invoke((ThreadStart)delegate
            {
                bool shouldSwitchToNewlyImportedCell = workflowController.CurrentCell == null;
                workflowController.NewCellsCaptured(newCells, shouldSwitchToNewlyImportedCell);
                completeCallback();

            }, DispatcherPriority.Normal);
        }

        private Cell Import(string imagePath, WorkflowController controller, Slide targetSlide)
        {
//            using (var db = new Connect.Database())
//            {
                var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.UriSource = new Uri(imagePath);
                bitmap.EndInit();
                bitmap.Freeze();

                bool invert = string.Compare(Path.GetExtension(imagePath).ToUpper(), ".RAW", StringComparison.CurrentCultureIgnoreCase) == 0;
                var channel = ImageFromBitmapSource.Convert(bitmap, invert);

                var f = new ImageFrame
                {
                    ImageBitsPerPixel = channel.BitsPerPixel,
                    ImageHeight = channel.Height,
                    ImageType = ImageType.Raw,
                    ImageWidth = channel.Width,
                    Thumbnail = null,
                    Id = Guid.NewGuid()
                };

                f.AddSnap(new Snap
                {
                    ChannelInfo = new ChannelInfo("Brightfield") { RenderColor = System.Windows.Media.Colors.White },
                    ChannelPixels = channel,
                    Id = Guid.NewGuid()
                });

                var properthumb = RGBImageComposer.Compose(f);
                f.Thumbnail = properthumb;

                var newCellId = Guid.NewGuid();
                var realSlideId = targetSlide.Id;

                var dataSource = DataSourceFactory.GetDataSource();
                var relativeIdSlide = controller.CaseName + ";" + realSlideId.ToString();
                var relativeIdCell = relativeIdSlide + ";" + newCellId.ToString();

                var imageGroupId = Guid.NewGuid();
                ImageFrameDbLoad.ToDataSource(f, realSlideId, f.ImageType, imageGroupId, properthumb, relativeIdSlide, dataSource); // WH ADDED, save to slide is abit odd?

                var newCell = new Cell(newCellId)
                {
                    CellColorIndex = CellColor.White.Index,
                    IsCounted = false,
                    IsKaryotyped = false,
                    Name = DateTime.Now.ToShortTimeString(),
                    IsNumbered = false,
                    ImageGroupId = imageGroupId,
                    SlideId = realSlideId
                };

                dataSource.SaveXml(relativeIdCell, newCell.ToXml(), "cell.cellstatus");

                //var dbCell = new Connect.Cell
                //{
                //    analysed = false,
                //    cellId = newCellId,
                //    cellName = DateTime.Now.ToShortTimeString(),
                //    color = CellColor.White.Index,
                //    counted = false,
                //    imageGroup = imageGroupId,
                //    karyotyped = false,
                //    slideId = db.SlideIdFor(targetSlide)
                //};

                //db.Cells.InsertOnSubmit(dbCell);
                //db.SubmitChanges();

                newCell.RawImageID = f.Id;
                newCell.MetaphaseThumbnailPath = f.Id.ToString() + ".jpg";
                newCell.MetaphaseThumbnail = properthumb;
                newCell.Slide = targetSlide;
                return newCell;
//            }
        }
    }
}
