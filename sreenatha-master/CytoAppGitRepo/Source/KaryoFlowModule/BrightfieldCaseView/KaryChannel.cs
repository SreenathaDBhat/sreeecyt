﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AI.Bitmap;

namespace AI.Brightfield
{
    /// <summary>
    /// Bunch of extension methods for Channel used by the karyotyper.
    /// Dont want to bloat the Channel class itself.
    /// </summary>
    public static class KaryChannel
    {
        /// <summary>
        /// Factory method for creating mask channels.
        /// The returned channel is 8 bit with its background set to 255.
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public static Channel CreateMask(int width, int height)
        {
            Channel mask = new Channel(width, height, 8);
            mask.SetBackground(255);
            return mask;
        }

        /// <summary>
        /// Factory method for creating inverse mask channels.
        /// The returned channel is 8 bit with its background set to 0.
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public static Channel CreateInverseMask(int width, int height)
        {
            Channel mask = new Channel(width, height, 8);
            return mask;
        }

        private static unsafe ushort[] ConvertToShortArray(byte[] byteArray)
        {
            int len = byteArray.Length;
            ushort[] shortArray = new ushort[len / 2];

            fixed (ushort* pa = &shortArray[0])
            fixed (byte* ba = &byteArray[0])
            {
                byte* end = ba + len;
                int pint, pint1, pint2;
                ushort* p = pa;
                byte* b = ba;

                while (b != end)
                {
                    pint1 = *b++;
                    pint2 = *b++;
                    pint = (pint2 * 255 + pint1);
                    *p++ = (ushort)(1024 - pint);
                }
            }

            return shortArray;
        }

        private static unsafe ushort[] PadToShortArray(byte[] byteArray)
        {
            int len = byteArray.Length;
            ushort[] shortArray = new ushort[len];

            fixed (ushort* pa = &shortArray[0])
            fixed (byte* ba = &byteArray[0])
            {
                ushort* end = pa + len;
                ushort* p = pa;
                byte* b = ba;

                while (p != end)
                {
                    *p++ = *b++;
                }
            }

            return shortArray;
        }

        public static unsafe object Clone(this Channel c)
        {
            int n = c.Width * c.Height;
            ushort[] pixels = new ushort[n];
            fixed (ushort* destPtr = &pixels[0])
            fixed (ushort* srcPtr = &c.Pixels[0])
            {
                ushort* d = destPtr;
                ushort* s = srcPtr;
                for (int i = 0; i < n; ++i)
                {
                    *d++ = *s++;
                }
            }

            return new Channel(c.Width, c.Height, c.BitsPerPixel, pixels);
        }







        public static unsafe Channel Crop(this Channel c, System.Windows.Int32Rect bounds)
        {
            return c.Crop(bounds, (ushort)Math.Pow(2, c.BitsPerPixel));
        }

        /// <summary>
        /// Creates a new Channel by cutting out a rectangle from this Channel.
        /// </summary>
        /// <param name="intBounds"></param>
        /// <returns></returns>
        public static unsafe Channel Crop(this Channel c, System.Windows.Int32Rect bounds, ushort threshold)
        {
            if (bounds.X < 0 || bounds.X + bounds.Width > c.Width) throw new ArgumentException("bounds.X or bounds.Width lie outside the Channel bounds.");
            if (bounds.Y < 0 || bounds.Y + bounds.Height > c.Height) throw new ArgumentException("bounds.Y or bounds.Height lie outside the Channel bounds.");

            int n = bounds.Width * bounds.Height;
            ushort[] result = new ushort[n];
            ushort thresholdedOutValue = (ushort)(Math.Pow(2, c.BitsPerPixel) - 1);
            ushort v;

            fixed (ushort* destPtr = &result[0])
            fixed (ushort* srcPtr = &c.Pixels[c.Width * bounds.Y + bounds.X])
            {
                ushort* d = destPtr;
                ushort* s = srcPtr;

                for (int j = 0; j < bounds.Height; j++)
                {
                    for (int i = 0; i < bounds.Width; i++)
                    {
                        v = *s++;
                        *d++ = v > threshold ? thresholdedOutValue : v;
                    }

                    s += c.Width - bounds.Width;
                }
            }

            return new Channel(bounds.Width, bounds.Height, c.BitsPerPixel, result);
        }

        public static void SetBackgroundWhite(this Channel c)
        {
            ushort background = (ushort)((int)(Math.Pow(2.0, c.BitsPerPixel) + 0.5) - 1);
            c.SetBackground(background);
        }

        private static unsafe void SetBackground(this Channel c, ushort background)
        {
            int n = c.Width * c.Height;
            fixed (ushort* destPtr = &c.Pixels[0])
            {
                ushort* d = destPtr;
                for (int i = 0; i < n; i++)
                {
                    *d++ = background;
                }
            }
        }

        /// <summary>
        /// Inverts the grey levels in this Channel and returns the result in a new Channel.
        /// </summary>
        /// <returns></returns>
        public static unsafe Channel Invert(Channel c)
        {
            int n = c.Width * c.Height;
            ushort[] result = new ushort[n];
            ushort background = (ushort)((int)(Math.Pow(2.0, c.BitsPerPixel) + 0.5) - 1);

            fixed (ushort* destPtr = &result[0])
            fixed (ushort* srcPtr = &c.Pixels[0])
            {
                ushort* d = destPtr;
                ushort* s = srcPtr;
                for (int i = 0; i < n; i++, d++, s++)
                {
                    *d = (ushort)(background - *s);
                }
            }

            return new Channel(c.Width, c.Height, c.BitsPerPixel, result);
        }

        /// <summary>
        /// Sets the value of any pixel whose value is greater than the threshold.
        /// </summary>
        public static unsafe void ApplyThreshold(this Channel c, ushort threshold, ushort newValue)
        {
            int n = c.Width * c.Height;
            fixed (ushort* destPtr = &c.Pixels[0])
            {
                ushort* d = destPtr;
                for (int i = 0; i < n; i++, d++)
                {
                    if (*d > threshold)
                    {
                        *d = newValue;
                    }
                }
            }
        }

        /// <summary>
        /// Creates a new Channel by applying the mask to this Channel.
        /// The pixels ignored by the mask take the maximum value possible for the image bitdepth.
        /// </summary>
        /// <param name="mask"></param>
        /// <returns></returns>
        public static unsafe Channel ApplyMask(this Channel c, Channel mask)
        {
            int n = c.Width * c.Height;
            ushort[] result = new ushort[n];
            ushort background = (ushort)((int)(Math.Pow(2.0, c.BitsPerPixel) + 0.5) - 1);

            fixed (ushort* destPtr = &result[0])
            fixed (ushort* srcPtr = &c.Pixels[0])
            fixed (ushort* maskPtr = &mask.Pixels[0])
            {
                ushort* d = destPtr;
                ushort* s = srcPtr;
                ushort* m = maskPtr;
                for (int i = 0; i < n; i++, d++, m++, s++)
                {
                    *d = (*m == 0) ? *s : background;
                }
            }

            return new Channel(c.Width, c.Height, c.BitsPerPixel, result);
        }

        /// <summary>
        /// Draws a channel into this channel at a specific position.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="channel"></param>
        public static unsafe void DrawInto(this Channel c, int x, int y, Channel source)
        {
            fixed (ushort* srcPtr = &source.Pixels[0])
            fixed (ushort* destPtr = &c.Pixels[0])
            {
                ushort* s = srcPtr;
                ushort* line = destPtr + y * c.Width + x;

                for (int j = 0; j < source.Height; j++)
                {
                    ushort* d = line;

                    for (int i = 0; i < source.Width; i++)
                    {
                        *d++ = *s++;
                    }

                    line += c.Width;
                }
            }
        }


        public static unsafe Channel PixelAnd(Channel channel1, Channel channel2)
        {
            int n = channel1.Width * channel1.Height;
            ushort[] result = new ushort[n];

            fixed (ushort* destPtr = &result[0])
            fixed (ushort* c1Ptr = &channel1.Pixels[0])
            fixed (ushort* c2Ptr = &channel2.Pixels[0])
            {
                ushort* d = destPtr;
                ushort* c1 = c1Ptr;
                ushort* c2 = c2Ptr;
                for (int i = 0; i < n; i++, d++, c1++, c2++)
                {
                    *d = (ushort)(*c1 & *c2);
                }
            }

            return new Channel(channel1.Width, channel1.Height, channel1.BitsPerPixel, result);
        }

        /// <summary>
        /// Bitwise OR the images together and return the result in a new channel.
        /// </summary>
        /// <param name="channel1"></param>
        /// <param name="channel2"></param>
        /// <returns></returns>
        public static unsafe Channel PixelOr(Channel channel1, Channel channel2)
        {
            int n = channel1.Width * channel1.Height;
            ushort[] result = new ushort[n];

            fixed (ushort* destPtr = &result[0])
            fixed (ushort* c1Ptr = &channel1.Pixels[0])
            fixed (ushort* c2Ptr = &channel2.Pixels[0])
            {
                ushort* d = destPtr;
                ushort* c1 = c1Ptr;
                ushort* c2 = c2Ptr;
                for (int i = 0; i < n; i++, d++, c1++, c2++)
                {
                    *d = (ushort)(*c1 | *c2);
                }
            }

            return new Channel(channel1.Width, channel1.Height, channel1.BitsPerPixel, result);
        }
    }
}
