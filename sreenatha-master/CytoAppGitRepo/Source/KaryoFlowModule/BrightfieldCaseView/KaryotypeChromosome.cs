﻿using System;
using System.Windows.Media;
using System.Windows;
using AI.Bitmap;
using System.Windows.Media.Imaging;
using System.IO;
using AI.Connect;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;

namespace AI.Brightfield
{
    internal class KaryotypeChromosomeColoring
    {
        private static Random rand = new Random();
        private static int brushIndex;
        private static Brush[] colors = new Brush[]
        {
            Brushes.Red,
            Brushes.Green,
            Brushes.Blue,
            Brushes.Yellow,
            Brushes.Orange,
            Brushes.Purple,
            Brushes.Cyan,
            Brushes.SaddleBrown,
            Brushes.Turquoise,
            Brushes.DarkViolet,
            Brushes.DarkSlateGray,
            Brushes.CornflowerBlue,
            Brushes.DarkRed,
            Brushes.OliveDrab,
            Brushes.Tomato,
            Brushes.Indigo,
            Brushes.YellowGreen,
            Brushes.OrangeRed,
            Brushes.Aqua,
            Brushes.Sienna,
            Brushes.Firebrick,
            Brushes.Goldenrod,
            Brushes.Gold,
            Brushes.Plum
        };

        public static Brush Random()
        {
            return colors[(brushIndex++) % colors.Length];
        }

        public static Brush Discreet(int index)
        {
            return colors[index % colors.Length];
        }
    }

    public unsafe class KaryotypeChromosome : Notifier, IDraggable
    {
		public const int ObjectDetectionMargin = 10;
        public const int MinArea = 220;
        private BitmapSource displayImage;
        private KaryotypeMask mask;
        private Int32Rect bounds;
        private double rotationToVertical, userRotation;
        private ChromosomeGroup group;
        private ChromosomeMeasurements measurements;
        private double userOffsetX;
        private double userOffsetY;
        private readonly Guid id;
        private uint designationRank;
		private Point midPoint;
        private double width, length;
        private Brush pseudoColour;
        private PointCollection outline = new PointCollection();
        private bool mouseOver, focus;
        private bool imageInvalidated;
        private bool flipX, flipY;
        private Guid blobId = Guid.Empty;

        private KaryotypeChromosome(Guid id, Guid parentCell, Int32Rect bounds, KaryotypeMask mask)
        {
            this.id = id;
            this.mask = mask;
            this.bounds = bounds;
            group = null;
            pseudoColour = KaryotypeChromosomeColoring.Random();
            imageInvalidated = false;

            Cell = parentCell;
        }

        public static KaryotypeChromosome CreateBare(Guid parentCell)
        {
            return new KaryotypeChromosome(Guid.Empty, parentCell, Int32Rect.Empty, null);
        }

        /// <summary>
        /// Create a new chromosome from a Woolz object and a full frame image.
        /// </summary>
        /// <param name="cellId"></param>
        /// <param name="bounds"> The bounds of the chromosome relative to the full frame image.</param>
        /// <param name="w"></param>
        /// <param name="framePixels"> The full frame image.</param>
        /// <param name="frameWidth"> Width of full frame image.</param>
        /// <returns></returns>
        public static KaryotypeChromosome CreateFromWoolz(Guid cellId, Int32Rect bounds, WoolzObject w, byte[] framePixels, int frameWidth)
        {
            KaryotypeChromosome chrom = new KaryotypeChromosome(Guid.NewGuid(), cellId, bounds, new KaryotypeMask(w.Mask));
            chrom.SetMeasurements(w);
            chrom.UpdateDisplayImage(framePixels, frameWidth, bounds);
            chrom.outline = SparseOutline(w.Outline);

            return chrom;
        }

        /// <summary>
        /// Create a new chromosome from a Woolz object and a display image the size of the chromosome bounds.
        /// </summary>
        /// <param name="cellId"></param>
        /// <param name="bounds"> The bounds of the chromosome relative to the full frame image.</param>
        /// <param name="w"></param>
        /// <param name="displayPixels">The display image the size of the chromosome bounds.</param>
        /// <returns></returns>
        public static KaryotypeChromosome CreateFromWoolz(Guid cellId, Int32Rect bounds, WoolzObject w, byte[] displayPixels)
        {
            KaryotypeChromosome chrom = new KaryotypeChromosome(Guid.NewGuid(), cellId, bounds, new KaryotypeMask(w.Mask));
            chrom.SetMeasurements(w);
            chrom.UpdateDisplayImage(displayPixels, bounds.Width, new Int32Rect(0, 0, bounds.Width, bounds.Height));

            Vector offset = new Vector(bounds.X - w.Bounds.X, bounds.Y - w.Bounds.Y);
            chrom.outline = SparseOutline(new PointCollection(from p in w.Outline select p + offset));

            return chrom;
        }

        public static unsafe void InitializeImage(KaryotypeChromosome chrom, Connect.Database db, string relative, IDataSource dataSource)
        {
            if (chrom.displayImage == null)
            {
                using (var stream = dataSource.LoadStream(relative, ChromosomeDataFile(chrom.Id)))
                {
                    chrom.displayImage = ImageFromBitmapSource.LoadBitmapImageFromStream(0, stream);
                }
                chrom.Notify("DisplayImage");
            }
        }

        public static unsafe void InitializeMask(KaryotypeChromosome chrom, string relative, IDataSource dataSource)
        {
            if (chrom.displayImage == null)
            {
                using (var stream = dataSource.LoadStream(relative, ChromosomeDataFile(chrom.Id)))
                {
                    chrom.displayImage = ImageFromBitmapSource.LoadBitmapImageFromStream(0, stream);
                }

                chrom.imageInvalidated = false;
            }

            Channel maskChannel = new Channel(chrom.displayImage.PixelWidth, chrom.displayImage.PixelHeight, 8);
            byte[] displayBytes = new byte[chrom.displayImage.PixelWidth * chrom.displayImage.PixelHeight * 4];
            chrom.displayImage.CopyPixels(displayBytes, chrom.displayImage.PixelWidth * 4, 0);

            fixed (byte* displayPtr = &displayBytes[0])
            fixed (ushort* channelPtr = &maskChannel.Pixels[0])
            {
                byte* d = displayPtr + 3;
                byte* end = displayPtr + chrom.displayImage.PixelWidth * chrom.displayImage.PixelHeight * 4;
                ushort* c = channelPtr;

                while (d < end)
                {
                    *c++ = (ushort)(255 - *d);
                    d += 4;
                }
            }

            chrom.ReplaceMask(maskChannel);
        }

        public static unsafe KaryotypeChromosome CreateFromDb(Guid caseId, Guid chromId, Database db)
        {
            var dbChrom = db.KaryotypeChromosomes.Where(k => k.karyotypeChromosomeId == chromId).First();
            return CreateFromDb(dbChrom);
        }

        public static KaryotypeChromosome CreateFromXml(XElement xml)
        {
            int l = int.Parse(xml.Attribute("boundsLeft").Value);
            int t = int.Parse(xml.Attribute("boundsTop").Value);
            int w = int.Parse(xml.Attribute("boundsWidth").Value);
            int h = int.Parse(xml.Attribute("boundsHeight").Value);
            Int32Rect bounds = new Int32Rect(l, t, w, h);

            Guid chromosomeId = Guid.Parse(xml.Attribute("karyotypeChromosomeId").Value);
            Guid cellId = Guid.Parse(xml.Attribute("cellId").Value);
            KaryotypeChromosome chrom = new KaryotypeChromosome(chromosomeId, cellId, bounds, null);
            chrom.displayImage = null;
            chrom.bounds = bounds;
            chrom.rotationToVertical = double.Parse(xml.Attribute("angleToVertical").Value);
            chrom.userRotation = double.Parse(xml.Attribute("userRotation").Value);
            chrom.width = double.Parse(xml.Attribute("width").Value);
            chrom.length = double.Parse(xml.Attribute("length").Value);
            chrom.UserOffsetX = double.Parse(xml.Attribute("x").Value);
            chrom.UserOffsetY = double.Parse(xml.Attribute("y").Value);
            chrom.outline = GetOutlineFromXML(xml.Descendants("Outline").FirstOrDefault());
            chrom.midPoint = new Point(double.Parse(xml.Attribute("midPointX").Value), double.Parse(xml.Attribute("midPointY").Value));
            chrom.imageInvalidated = false;
            chrom.IsXFlipped = bool.Parse(xml.Attribute("flipX").Value);
            chrom.IsYFlipped = bool.Parse(xml.Attribute("flipY").Value);
            chrom.blobId = Guid.Parse(xml.Attribute("blobId").Value);
            return chrom;
        }



        public static KaryotypeChromosome CreateFromDb(Connect.KaryotypeChromosome dbChrom)
        {
            Int32Rect bounds = new Int32Rect((int)dbChrom.boundsLeft, (int)dbChrom.boundsTop, (int)dbChrom.boundsWidth, (int)dbChrom.boundsHeight);

            KaryotypeChromosome chrom = new KaryotypeChromosome(dbChrom.karyotypeChromosomeId, dbChrom.cellId, bounds, null);
            chrom.displayImage = null;
            chrom.bounds = bounds;
            chrom.rotationToVertical = dbChrom.angleToVertical;
            chrom.userRotation = dbChrom.userRotation;
            chrom.width = dbChrom.width;
            chrom.length = dbChrom.length;
            chrom.UserOffsetX = dbChrom.x;
            chrom.UserOffsetY = dbChrom.y;
            chrom.outline = GetOutlineFromXML(dbChrom.outline);
			chrom.midPoint = new Point( dbChrom.midPointX, dbChrom.midPointY);
            chrom.imageInvalidated = false;
            chrom.IsXFlipped = dbChrom.flipX;
            chrom.IsYFlipped = dbChrom.flipY;
            chrom.blobId = dbChrom.blobId;
            return chrom;
        }

        public static KaryotypeChromosome CreateFromDb(Connect.CaseKaryotypeChromosome dbChrom)
        {
            Int32Rect bounds = new Int32Rect((int)dbChrom.boundsLeft, (int)dbChrom.boundsTop, (int)dbChrom.boundsWidth, (int)dbChrom.boundsHeight);

            KaryotypeChromosome chrom = new KaryotypeChromosome(dbChrom.karyotypeChromosomeId, dbChrom.cellId, bounds, null);
            chrom.displayImage = null;
            chrom.bounds = bounds;
            chrom.rotationToVertical = dbChrom.angleToVertical;
            chrom.userRotation = dbChrom.userRotation;
            chrom.width = dbChrom.width;
            chrom.length = dbChrom.length;
            chrom.UserOffsetX = dbChrom.x;
            chrom.UserOffsetY = dbChrom.y;
            chrom.outline = GetOutlineFromXML(dbChrom.outline);
			chrom.midPoint = new Point(dbChrom.midPointX, dbChrom.midPointY);
            chrom.imageInvalidated = false;
            chrom.IsXFlipped = dbChrom.flipX;
            chrom.IsYFlipped = dbChrom.flipY;
            chrom.blobId = dbChrom.blobId;
            return chrom;
        }

        public static string ChromosomeDataFile(Guid chromosomeId)
        {
            return chromosomeId + ".chromosome";
        }

        public Guid Cell { get; private set; }

        public ChromosomeGroup Group
        {
            get { return group; }
            set 
            { 
                group = value;
                if (group == null)
                {
                    PseudoColour = KaryotypeChromosomeColoring.Random();
                }
                else
                {
                    int index = 0;

                    if (group.Identifier == ChromosomeGroup.SexStringX)
                    {
                        index = 23;
                    }
                    else if (group.Identifier == ChromosomeGroup.SexStringY)
                    {
                        index = 24;
                    }
                    else
                    {
                        if (int.TryParse(group.Identifier, out index) == false)
                            index = 0;
                    }

                    if (index > 0)
                    {
                        PseudoColour = KaryotypeChromosomeColoring.Discreet(index - 1);
                    }
                }
            }
        }

        public bool IsClassified
        {
            get { return Designation != ChromosomeGroup.UnclassifiedString; }
        }

        public Guid Id
        {
            get { return id; }
        }

        public BitmapSource DisplayImage
        {
            get { return displayImage; }
            set { displayImage = value; Notify("DisplayImage"); imageInvalidated = true; }
        }

        public KaryotypeMask Mask
        {
            get { return mask; }
        }

        /// <summary>
        /// In degrees
        /// </summary>
        public double RotationToVertical
        {
            get { return rotationToVertical; }
            set { rotationToVertical = value; }
        }

        public bool IsImageInvalidated { get { return imageInvalidated; } }

        /// <summary>
        /// In degrees
        /// </summary>
        public double UserRotation
        {
            get { return userRotation; }
            set { userRotation = value; Notify("UserRotation", "RotationToVertical"); }
        }

        public ChromosomeMeasurements Measurements
        {
            get { return this.measurements; }
            set { this.measurements = value; }
        }

        public double UserOffsetX
        {
            get { return userOffsetX; }
            set { userOffsetX = value; Notify("UserOffsetX", "TranslateX"); }
        }

        public double UserOffsetY
        {
            get { return userOffsetY; }
            set { userOffsetY = value; Notify("UserOffsetY", "TranslateY"); }
        }

        public string Designation
        {
            get { return group == null ? ChromosomeGroup.UnclassifiedString : group.Identifier; }
        }

        public uint DesignationRank
        {
            get { return designationRank; }
            set { designationRank = value; Notify("DesignationRank"); }
        }

		public Point MidPoint
		{
			get { return midPoint; }
		}

        public double Width
        {
            get { return width; }
        }

        public double Length
        {
            get { return length; }
        }

        /// <summary>
        /// Bounding box (in metaphase coordinate space) of the chromosome
        /// </summary>
        public Int32Rect Bounds
        {
            get { return bounds; }
            set { bounds = value; }
        }

        /// <summary>
        /// A coloured brush used to overlay this chromosome in the UI.
        /// </summary>
        public Brush PseudoColour
        {
            get
            {
                return pseudoColour;
            }
            set
            {
                pseudoColour = value;
                Notify("PseudoColour");
            }
        }

        public PointCollection Outline
        {
            get { return outline; }
        }


        /// <summary>
        /// Reduce the number of points in the outline.
        /// </summary>
        /// <param name="outline"></param>
        /// <returns></returns>
        private static PointCollection SparseOutline(PointCollection outline)
        {
            int pointCount = outline.Count();
            PointCollection sparseOutline = new PointCollection(pointCount);

            for (int i = 0; i < pointCount; i++)
            {
                if (i % 2 == 0)
                {
                    sparseOutline.Add(outline[i]);
                }
            }

            sparseOutline.Freeze();
            return sparseOutline;
        }

        public void SetMeasurements(WoolzObject wzObject)
        {
            length = wzObject.Measurements.LengthVertical;
            width = wzObject.Measurements.WidthVertical;
            rotationToVertical = wzObject.Orientation;
            Measurements = wzObject.Measurements;
			//CentromereX = wzObject.Measurements.CentromereX + ObjectDetectionMargin;
			//CentromereY = wzObject.Measurements.CentromereY + ObjectDetectionMargin;
			midPoint = new Point(wzObject.Measurements.MidPointX + ObjectDetectionMargin, wzObject.Measurements.MidPointY + ObjectDetectionMargin);
            if (Measurements.CentromericIndexByArea > 50)
            {
                rotationToVertical += 180;
            }
        }

        public void DrawIncludeStroke(System.Windows.Ink.Stroke stroke)
        {
            mask.IncudeStroke(stroke);
        }

        public void DrawExcludeStroke(System.Windows.Ink.Stroke stroke)
        {
            mask.ExcludeStroke(stroke);
        }

        public Channel DuplicateMask()
        {
            return mask.ToChannel();
        }

        internal void ReplaceMask(Channel originalMask)
        {
            mask = new KaryotypeMask(originalMask);
        }

        internal void ReplaceMaskData(Channel originalMask, BitmapSource originalDisplayImage)
        {
            mask = new KaryotypeMask(originalMask);
            displayImage = originalDisplayImage;
            imageInvalidated = true;
            Notify("DisplayImage");
        }

        public bool Contains(Point point)
        {
            bool contains = false;

            int maskX = (int)(point.X + 0.5) - bounds.X;
            int maskY = (int)(point.Y + 0.5) - bounds.Y;

            bool xInsideBounds = (0 <= maskX) && (maskX < bounds.Width);
            bool yInsideBounds = (0 <= maskY) && (maskY < bounds.Height);
            if (xInsideBounds && yInsideBounds)
            {
                byte[] maskBytes = new byte[4];
                Int32 stride = (mask.Image.PixelWidth * mask.Image.Format.BitsPerPixel + 7) / 8;
                mask.Image.CopyPixels(new Int32Rect(maskX, maskY, 1, 1), maskBytes, stride, 0);

                // The mask value (255=background, 0=ROI) is in the alpha channel
                contains = maskBytes[0] == 0;
            }

            return contains;
        }

        public void QuietlyUpdateDisplayImage(BitmapSource bitmapSource)
        {
            displayImage = bitmapSource;
            imageInvalidated = true;
        }

        internal unsafe void UpdateDisplayImage(byte[] sourceBytes, int sourceWidth, Int32Rect roi)
        {
            displayImage = MaskBitmap(roi, mask, sourceBytes, sourceWidth);
            imageInvalidated = true;
            Notify("DisplayImage");
        }

        public static BitmapSource MaskBitmap(Int32Rect roi, KaryotypeMask mask, byte[] sourceBytes, int sourceWidth)
        {
            if (roi.Width != mask.Width || roi.Height != mask.Height)
                throw new ArgumentException("roi");

            byte[] displayBytes = new byte[roi.Width * roi.Height * 4];
            byte[] maskBytes = new byte[roi.Width * roi.Height * 4];

            mask.Image.CopyPixels(maskBytes, roi.Width * 4, 0);

            fixed (byte* srcPtr = &sourceBytes[0])
            fixed (byte* destPtr = &displayBytes[0])
            fixed (byte* maskPtr = &maskBytes[0])
            {
                int bottom = roi.Y + roi.Height;
                byte* dest = destPtr;
                byte* m = maskPtr;

                for (int y = roi.Y; y != bottom; ++y)
                {
                    byte* src = srcPtr + y * sourceWidth * 4 + roi.X * 4;
                    byte* end = src + roi.Width * 4;

                    while (src < end)
                    {
                        *dest++ = *src++;
                        *dest++ = *src++;
                        *dest++ = *src++;
                        *dest++ = (byte)(255 - *m);
                        ++src;
                        m += 4;
                    }
                }
            }

            var s = BitmapSource.Create(roi.Width, roi.Height, 96, 96, PixelFormats.Bgra32, null, displayBytes, roi.Width * 4);
            s.Freeze();
            return s;
        }

        public unsafe Guid SaveDisplayImageBlob(string chromFile, byte[] maskBytes, string relative, IDataSource dataSource)
        {
            Guid newId = Guid.Empty;

            byte[] destBytes = new byte[mask.Width * mask.Height * 4];
            byte[] displayBytes = new byte[mask.Width * mask.Height * 4];

            displayImage.CopyPixels(displayBytes, mask.Width * 4, 0);

            fixed (byte* destPtr = &destBytes[0])
            fixed (byte* maskPtr = &maskBytes[0])
            fixed (byte* displayPtr = &displayBytes[0])
            {
                byte* d = destPtr;
                byte* m = maskPtr;
                byte* p = displayPtr;
                byte* end = destPtr + mask.Width * mask.Height * 4;

                while (d != end)
                {
                    *d++ = *p++;
                    *d++ = *p++;
                    *d++ = *p++;
                    *d++ = (byte)(255 - *m);

                    p++;
                    m += 4;
                }
            }

            var bmp = BitmapSource.Create(mask.Width, mask.Height, 96, 96, PixelFormats.Bgra32, null, destBytes, mask.Width * 4);
            using (var stream = new MemoryStream())
            {
                PngBitmapEncoder enc = new PngBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bmp));
                enc.Save(stream);
                stream.Flush();

                dataSource.SaveStream(relative, stream, chromFile);
            }

            imageInvalidated = false;
            this.blobId = newId;
            return newId;
        }

        public XElement SaveNonImageDataXml(Guid cellId, int groupIndex)
        {
            return new XElement("Chromosome",
                new XAttribute("karyotypeChromosomeId", Id.ToString()),
                new XAttribute("cellId", cellId.ToString()),
                new XAttribute("chromosomeTag", Designation),
                new XAttribute("angleToVertical", rotationToVertical),
                new XAttribute("userRotation", userRotation),
                new XAttribute("x", userOffsetX),
                new XAttribute("y", userOffsetY),
                new XAttribute("boundsLeft", Bounds.X),
                new XAttribute("boundsTop", Bounds.Y),
                new XAttribute("boundsWidth", Bounds.Width),
                new XAttribute("boundsHeight", Bounds.Height),
                new XAttribute("width", (int)width),
                new XAttribute("length", (int)length),
                new XAttribute("groupIndex", groupIndex),
			    new XAttribute("midPointX", midPoint.X),
			    new XAttribute("midPointY", midPoint.Y),
                new XAttribute("flipX", flipX),
                new XAttribute("flipY", flipY),
                new XAttribute("blobId", blobId),
                OutlineXML());
        }


        public void SaveNonImageData(Database db, Guid caseId, Guid cellId, int groupIndex, IEnumerable<Connect.KaryotypeChromosome> existingDbChromosomes)
        {
            var existing = existingDbChromosomes.Where(k => k.karyotypeChromosomeId == id).FirstOrDefault();

            if (existing == null)
            {
                existing = new Connect.KaryotypeChromosome
                {
                    karyotypeChromosomeId = Id
                };

                db.KaryotypeChromosomes.InsertOnSubmit(existing);
            }

            UpdateDb(cellId, groupIndex, existing);
        }

        public void SaveNonImageData(Database db, Guid caseId, Guid cellId, int groupIndex)
        {
            var existing = db.KaryotypeChromosomes.Where(c => c.karyotypeChromosomeId == Id).FirstOrDefault();
            if (existing == null)
            {
                existing = new Connect.KaryotypeChromosome
                {
                    karyotypeChromosomeId = Id
                };

                db.KaryotypeChromosomes.InsertOnSubmit(existing);
            }

            UpdateDb(cellId, groupIndex, existing);
        }

        private void UpdateDb(Guid cellId, int groupIndex, AI.Connect.KaryotypeChromosome dbChrom)
        {
            dbChrom.cellId = cellId;
            dbChrom.chromosomeTag = Designation;
            dbChrom.angleToVertical = rotationToVertical;
            dbChrom.userRotation = userRotation;
            dbChrom.x = userOffsetX;
            dbChrom.y = userOffsetY;
            dbChrom.boundsLeft = Bounds.X;
            dbChrom.boundsTop = Bounds.Y;
            dbChrom.boundsWidth = Bounds.Width;
            dbChrom.boundsHeight = Bounds.Height;
            dbChrom.width = (int)width;
            dbChrom.length = (int)length;
            dbChrom.groupIndex = groupIndex;
            dbChrom.outline = OutlineXML();
			dbChrom.midPointX = midPoint.X;
			dbChrom.midPointY = midPoint.Y;
            dbChrom.flipX = flipX;
            dbChrom.flipY = flipY;
            dbChrom.blobId = blobId;
        }

        private XElement OutlineXML()
        {
            XElement outlineXML = new XElement("Outline");
            for (int i = 0; i < Outline.Count(); i++)
            {
                XElement pointXML = new XElement("Point",
                                        new XAttribute("X", Outline[i].X),
                                        new XAttribute("Y", Outline[i].Y));
                outlineXML.Add(pointXML);
            }

            return outlineXML;
        }

        /// <summary>
        /// Extracts the outline from xml.
        /// </summary>
        /// <exception cref="System.FormatException">Couldnt translate attribute to double coordinate</exception>
        /// <exception cref="System.ArgumentNullException">Couldnt find coordinate attribute</exception>
        /// <param name="outlineXML"></param>
        /// <returns></returns>
        public static PointCollection GetOutlineFromXML(XElement outlineXML)
        {
            // TODO : If there are any errors here eg the "X" attribute doesnt exist or its value cannot be converted to double
            // they should be caught in calling method.

            PointCollection outline = new PointCollection(
                from pointXML in outlineXML.Elements("Point")
                select new Point((double)pointXML.Attribute("X"),
                                 (double)pointXML.Attribute("Y")));

            outline.Freeze();
            return outline;
        }

        public bool IsMouseOver
        {
            get { return mouseOver; }
            set { mouseOver = value; Notify("IsMouseOver"); }
        }

        public bool IsFocussed
        {
            get { return focus; }
            set { focus = value; Notify("IsFocussed"); }
        }

        #region IDraggable Members

        public double Rotation
        {
            get
            {
                return UserRotation;
            }
            set
            {
                UserRotation = value;
            }
        }

        public double TranslateX
        {
            get
            {
                return userOffsetX;
            }
            set
            {
                UserOffsetX = value;
            }
        }

        public double TranslateY
        {
            get
            {
                return userOffsetY;
            }
            set
            {
                UserOffsetY = value;
            }
        }

        #endregion

        public void SetSize(double width, double length)
        {
            this.width = width;
            this.length = length;
        }

        public void SetDefaultMidPoint(double width, double length)
        {
            //  To set bounding box correctly a mid point is required.
            //  If the true midpoint is not known via woolz then a
            //  default can be determined based on the width and
            //  length of the object.
            midPoint.X = width / (double)2;
            midPoint.Y = length / (double)2;
        }

        public void SetImageNotInvalidated()
        {
            imageInvalidated = false;
        }

        public bool IsXFlipped
        {
            get { return flipX; }
            set { flipX = value; Notify("IsXFlipped"); }
        }

        public bool IsYFlipped
        {
            get { return flipY; }
            set { flipY = value; Notify("IsYFlipped"); }
        }
    }
}

