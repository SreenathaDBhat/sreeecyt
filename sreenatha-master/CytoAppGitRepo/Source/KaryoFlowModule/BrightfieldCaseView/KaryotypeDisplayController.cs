﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Windows.Media;
using System.Windows.Ink;


namespace AI.Brightfield
{
	public class KaryotypeDisplayController : Notifier
	{
        private static readonly string AddPenSizeKey = "Karyotype_AddModePenSize";
        private static readonly string DeletePenSizeKey = "Karyotype_DeleteModePenSize";
        private static readonly string PersistShowSolidChromosome = "Karyotype_ShowSolidChromosome";
        private static readonly string PersistShowBoundaries = "Karyotype_showBoundaries";


        private SimplePersistance persistants;

		private bool viewingMet;
		private bool detectMode;
		private bool showSolidChromosome;
		private bool showBoundaries;
		private bool addMode;
        private bool editMode;
        private bool deleteMode;
		private double addModePenSize;
        private double deleteModePenSize;
        private double minPenSize;
        private double maxPenSize;
        private KaryotypeChromosome focussedChromosome;

        public KaryotypeDisplayController(SimplePersistance persistants)
		{
            this.persistants = persistants;
			viewingMet = true;
			detectMode = true;
            showSolidChromosome = persistants.GetBool(PersistShowSolidChromosome, false);
            showBoundaries = persistants.GetBool(PersistShowBoundaries, false);
			addMode = false;
            deleteMode = false;
            editMode = true;
			addModePenSize = persistants.GetDouble(AddPenSizeKey, 35);
            deleteModePenSize = persistants.GetDouble(DeletePenSizeKey, 65);
            minPenSize = 10;
            maxPenSize = 100;
            focussedChromosome = null;
		}

		public bool ViewingMet
		{
			get { return viewingMet; }
			set { viewingMet = value; Notify("ViewingMet"); }
		}

		public bool DetectMode
		{
			get { return detectMode; }
			set
			{
				detectMode = value;

				if (value)
				{
					viewingMet = true;
					Notify("ShowSolidChromosomes", "ViewingMet", "EditingChromosome");
				}

				Notify("DetectMode", "CutMode");
			}
		}

		public bool CutMode
		{
			get { return !detectMode; }
			set { DetectMode = !value; }
		}

        public KaryotypeChromosome FocussedChromosome
        {
            get { return focussedChromosome; }
            set
            {
                focussedChromosome = value;
                Notify("FocussedChromosome");

                if (focussedChromosome != null)
                    EditMode = true;
            }
        }

		public bool ShowSolidChromosomes
		{
			get { return showSolidChromosome; }
			set 
            { 
                showSolidChromosome = value;
                persistants.Set(PersistShowSolidChromosome, value);
                Notify( "ShowSolidChromosomes"); 
            }
		}

		public bool ShowBoundaries
		{
			get { return showBoundaries; }
			set 
            { 
                showBoundaries = value;
                persistants.Set(PersistShowBoundaries, value);
                Notify("ShowBoundaries"); 
            }
		}

		public bool AddMode
		{
			get { return addMode; }
			set
			{
				addMode = value;

                if (value)
                {
                    editMode = false;
                    deleteMode = false;
                    focussedChromosome = null;
                }

                Notify("AddMode", "EditMode", "DeleteMode", "DrawingAttributes", "FocussedChromosome");
			}
		}

		public bool EditMode
        {
            get { return editMode; }
            set
            {
                editMode = value;

                if (value)
                {
                    addMode = false;
                    deleteMode = false;
                }

                Notify("AddMode", "EditMode", "DeleteMode", "DrawingAttributes");
            }
        }

        public bool DeleteMode
        {
            get { return deleteMode; }
            set
            {
                deleteMode = value;

                if (value)
                {
                    focussedChromosome = null;
                    editMode = false;
                    addMode = false;
                }
                else
                {
                    editMode = true;
                    addMode = false;
                }

                Notify("AddMode", "EditMode", "DeleteMode", "DrawingAttributes", "FocussedChromosome");
            }
        }

        public double MinPenSize
        {
            get { return minPenSize; }
        }

        public double MaxPenSize
        {
            get { return maxPenSize; }
        }

		public double AddModePenSize
		{
			get { return addModePenSize; }
            set
            {
                addModePenSize = Math.Min(Math.Max(minPenSize, value), maxPenSize);
                persistants.Set(AddPenSizeKey, value);
                Notify("AddModePenSize", "DrawingAttributes");
            }
		}

        public double DeleteModePenSize
        {
            get { return deleteModePenSize; }
            set
            {
                deleteModePenSize = Math.Min(Math.Max(minPenSize, value), maxPenSize);
                persistants.Set(DeletePenSizeKey, value);
                Notify("DeleteModePenSize", "DrawingAttributes");
            }
        }

		public DrawingAttributes AddModeDrawingAttributes
		{
			get
			{
				return new DrawingAttributes
				{
					Color = Color.FromArgb(128, 45, 225, 245),
					IgnorePressure = true,
					FitToCurve = true,
					Height = addModePenSize,
					Width = addModePenSize
				};
			}
		}

		private DrawingAttributes EditModeDrawingAttributes
		{
			get
			{
				return new DrawingAttributes
				{
					Color = Color.FromArgb(172, 0, 228, 228),
					IgnorePressure = true,
					FitToCurve = true,
					Height = 5,
					Width = 5
				};
			}
		}

        private DrawingAttributes DeleteModeDrawingAttributes
        {
            get
            {
                return new DrawingAttributes
                {
                    Color = Color.FromArgb(172, 228, 32, 32),
                    IgnorePressure = true,
                    FitToCurve = true,
                    Height = deleteModePenSize,
                    Width = deleteModePenSize
                };
            }
        }

        public DrawingAttributes DrawingAttributes
        {
            get
            {
                if (addMode)
                    return AddModeDrawingAttributes;
                else if (deleteMode)
                    return DeleteModeDrawingAttributes;
                else
                    return EditModeDrawingAttributes;
            }
        }
	}

}
