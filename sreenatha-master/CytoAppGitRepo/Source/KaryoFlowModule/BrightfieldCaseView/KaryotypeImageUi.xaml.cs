﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Data;

namespace AI.Brightfield
{
    public partial class KaryotypeImageUi
    {
        private bool hackHighlightDisable;

        public KaryotypeImageUi(WorkflowController controller)
        {
            Controller = controller;
            Karyotyper = controller.Tool as KaryotypeController;
            InitializeComponent();
        }

        private WorkflowController Controller { get; set; }
        private KaryotypeController Karyotyper { get; set; }

        private void OnAddModeStroke(object sender, System.Windows.Controls.InkCanvasStrokeCollectedEventArgs e)
        {
            bool unMaskedAdd = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
            Karyotyper.HandleStroke(e.Stroke, Karyotyper.MetImageRenderer, unMaskedAdd);
            ((InkCanvas)sender).Strokes.Clear();
        }

        private void OnHackInkMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            hackHighlightDisable = true;
        }

        private void OnHackInkMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            hackHighlightDisable = false;
        }

        private void OnUpdateMouseHighlight(object sender, MouseEventArgs e)
        {
            //Point pos = e.GetPosition(Karyotyper.MetImageRenderer);
            Point pos = e.GetPosition(this);
            Canvas.SetLeft(mouseHighlightOverlap, pos.X);
            Canvas.SetTop(mouseHighlightOverlap, pos.Y);


            // Dont have mouseover effects while drawing a stroke
            if (hackHighlightDisable)
            {
                foreach (var k in Karyotyper.Chromosomes)
                {
                    k.IsMouseOver = false;
                }
            }
            else if (Karyotyper.GotChromosomes)
            {
                var underMouse = Karyotyper.FindChromosomeContaining(pos);

                foreach (var k in Karyotyper.Chromosomes)
                {
                    k.IsMouseOver = k == underMouse;
                }
            }
        }

        // The inkcanvas swallows middle mouse down, killing panning. We stop that be being even more anti-social than it is.
        private void OnFudgeMiddleDrag(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Middle)
            {
                //var canvas = VisualTreeWalker.FindParentOfType<StretchOrSizeCanvas>(Karyotyper.MetImageRenderer);
                var canvas = VisualTreeWalker.FindParentOfType<StretchOrSizeCanvas>(this);
                canvas.HijackMouseDown(e);
            }
            else if (e.ChangedButton == MouseButton.Right)
            {
                e.Handled = true;
                if (Karyotyper.Display.FocussedChromosome != null)
                    Karyotyper.Display.FocussedChromosome = null;
                else
                    Karyotyper.Display.FocussedChromosome = Karyotyper.ChromosomeUnderMouse;
            }
        }

        private void OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if ((Karyotyper.Display.AddMode || Karyotyper.Display.DeleteMode)  &&  Keyboard.IsKeyDown(Key.LeftCtrl))
            {
                e.Handled = true;
                if (e.Delta > 0)
                    Karyotyper.IncreasePenSize();
                else
                    Karyotyper.DecreasePenSize();
            }
        }
    }

    #region Converters

    [ValueConversion(typeof(double), typeof(double))]
    public class OffsetToCentreConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return -((double)value/2.0) + 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    #endregion
}
