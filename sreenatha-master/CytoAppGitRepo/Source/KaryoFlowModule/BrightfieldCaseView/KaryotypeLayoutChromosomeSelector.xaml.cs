﻿using System;
using System.Windows.Input;
using System.Windows;

namespace AI.Brightfield
{
    public partial class KaryotypeLayoutChromosomeSelector
    {
        public static readonly RoutedUICommand ChromosomeGroupSelect = new RoutedUICommand("ChromosomeGroupSelect", "ChromosomeGroupSelect", typeof(KaryotypeLayoutChromosomeSelector));
        public static DependencyProperty ShowUnclassifiedProperty = DependencyProperty.Register("ShowUnclassified", typeof(bool), typeof(KaryotypeLayoutChromosomeSelector), new FrameworkPropertyMetadata(true));

        public KaryotypeLayoutChromosomeSelector()
        {
            InitializeComponent();
        }

        public bool ShowUnclassified
        {
            get { return (bool)GetValue(ShowUnclassifiedProperty); }
            set { SetValue(ShowUnclassifiedProperty, value); }
        }
    }
}
