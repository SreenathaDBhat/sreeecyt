﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;
using System.IO;
using System.Windows.Ink;
using AI.Bitmap;
using System.Diagnostics;

namespace AI
{
    public class KaryotypeMask
    {
        private RenderTargetBitmap image;
        private int width;
        private int height;

		public KaryotypeMask(int width, int height, Brush backgroundBrush)
		{
			this.width = width;
			this.height = height;

			image = new RenderTargetBitmap(width, height, 96, 96, PixelFormats.Pbgra32);

			DrawingVisual visual = new DrawingVisual();
			var draw = visual.RenderOpen();
			draw.DrawRectangle( backgroundBrush, null, new Rect(0, 0, width, height));
			draw.Close();
			image.Render(visual);
            CachePixels();
		}

        public KaryotypeMask(int width, int height) : this( width, height, Brushes.White)
        {
        }

        public unsafe KaryotypeMask(Channel maskData) : this(maskData.Width, maskData.Height)
        {
            byte[] pixels = new byte[maskData.Width * maskData.Height * 4];

            fixed (byte* pixelPtr = &pixels[0])
            fixed (ushort* srcPtr = &maskData.Pixels[0])
            {
                byte* pixel = pixelPtr;
                ushort* src = srcPtr;
                int n = maskData.Width * maskData.Height;
                byte v;

                for(int i = 0; i < n; ++i)
                {
                    v = (byte)*src;
                    ++src;

                    *pixel++ = v;
                    *pixel++ = v;
                    *pixel++ = v;
                    *pixel++ = 255;
                }
            }

            var img = BitmapSource.Create(maskData.Width, maskData.Height, 96, 96, PixelFormats.Pbgra32, null, pixels, maskData.Width * 4);
            DrawingVisual visual = new DrawingVisual();
            var draw = visual.RenderOpen();
            draw.DrawImage(img, new Rect(0, 0, maskData.Width, maskData.Height));
            draw.Close();

            image.Render(visual);
            CachePixels();
        }

        public BitmapSource Image { get { return image; } }
        public int Width { get { return width; } }
        public int Height { get { return height; } }

        public void IncudeStroke(Stroke stroke)
        {
            DrawStrokeIntoMask(stroke, Brushes.Black);
        }

        public void ExcludeStroke(Stroke stroke)
        {
            DrawStrokeIntoMask(stroke, Brushes.White);
        }

        private void DrawStrokeIntoMask(Stroke stroke, Brush brush)
        {
            StrokeDrawing.RenderStroke(stroke, brush, null, image, 0, 0);
            CachePixels();
        }

		/// <summary>
		/// Creates a mask from an image where the region of interest is every pixel
		/// whose value is less than or equal to the threshold.
		/// </summary>
		/// <remarks>
		/// In the returned Channel pixels in the region of interest have grey value zero and background 255.
		/// </remarks>
		/// <param name="imageBytes"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="threshold"></param>
		/// <returns></returns>
        public static unsafe Channel CreateMask(byte[] imageBytes, int width, int height, ushort threshold)
        {
            byte[] pixels = imageBytes;
            ushort[] channelBytes = new ushort[width * height];

            fixed (byte* fixedPixels = &pixels[0])
            fixed (ushort* channelPixels = &channelBytes[0])
            {
                byte* pixel = fixedPixels + 1; // argb, a is always 255 so skip on to r.
                byte* pixelEnd = fixedPixels + (width * height * 4);
                ushort* channel = channelPixels;

                while (pixel < pixelEnd)
                {
					*channel++ = (ushort)(*pixel > threshold ? 255 : 0);
                    pixel += 4;
                }
            }

            return new Channel(width, height, 8, channelBytes);
        }

        public static unsafe Channel CreateMask(Channel channel, ushort threshold)
        {
            int width = channel.Width;
            int height = channel.Height;
            ushort[] pixels = channel.Pixels;
            ushort[] maskBytes = new ushort[width * height];

            fixed (ushort* fixedPixels = &pixels[0])
            fixed (ushort* channelPixels = &maskBytes[0])
            {
                ushort* pixel = fixedPixels;
                ushort* pixelEnd = fixedPixels + (width * height);
                ushort* mask = channelPixels;
                ushort v;

                while (pixel < pixelEnd)
                {
                    v = *pixel++;
                    v = (ushort)(v > threshold ? 255 : 0);
                    *mask++ = v;
                }
            }

            return new Channel(width, height, 8, maskBytes);
        }


        public void WriteToDisk(FileInfo file)
        {
            using (var s = file.OpenWrite())
            {
                PngBitmapEncoder e = new PngBitmapEncoder();
                e.Frames.Add(BitmapFrame.Create(image));
                e.Save(s);
                s.Flush();
            }
        }

        public unsafe Channel ToChannel()
        {
            return ToChannel(8);
        }

        public unsafe Channel ToChannel(int bitsPerPixel)
        {
            int numPixels = width * height;
            ushort[] channelBytes = new ushort[numPixels];
            byte[] maskBytes = new byte[numPixels * 4];
            image.CopyPixels(maskBytes, width * 4, 0);
            double bppScale = Math.Pow(2.0, (double)bitsPerPixel) / 256;

            fixed (ushort* channelPtr = &channelBytes[0])
            fixed (byte* maskPtr = &maskBytes[0])
            {
                ushort* c = channelPtr;
                byte* m = maskPtr;

                for (int i = 0; i < numPixels; ++i)
                {
                    *c++ = (ushort)(*m * bppScale);
                    m += 4;
                }
            }

            return new Channel(width, height, bitsPerPixel, channelBytes);
        }


        private byte[] pixelCache;
        private void CachePixels()
        {
            var p = new byte[image.PixelWidth * image.PixelHeight * 4];
            image.CopyPixels(p, image.PixelWidth * 4, 0);
            pixelCache = p;
        }

        public byte[] CopyPixels()
        {
            return pixelCache;
        }
    }
}
