﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace AI.Brightfield
{
    public class KaryotypePanel : Panel
    {
        public static readonly DependencyProperty GroupProperty = DependencyProperty.RegisterAttached("Group", typeof(string), typeof(KaryotypePanel));
        private double canvasWidth = 0;
        private double canvasHeight = 0;
        private Dictionary<UIElement, Rect> positions = new Dictionary<UIElement, Rect>();


        public static string GetGroup(UIElement target)
        {
            object r = target.GetValue(GroupProperty);
            return (r == null || r == DependencyProperty.UnsetValue) ? "unknown" : (string)r;
        }

        public static void SetGroup(UIElement target, string value)
        {
            target.SetValue(GroupProperty, value);
        }



        private class Row : List<UIElement>
        {
            internal void AddMatchingGroups(UIElementCollection Children, params string[] matches)
            {
                foreach (UIElement e in Children)
                {
                    var chromGroupElement = VisualTreeHelper.GetChild(e, 0);
                    var groupName = GetGroup((UIElement)chromGroupElement);
                    if (matches.Contains(groupName))
                    {
                        Add(e);
                    }
                }
            }

            public double Width
            {
                get
                {
                    return Count == 0 ? 0 : this.Sum(c => c.DesiredSize.Width);
                }
            }

            public double Height
            {
                get
                {
                    return Count == 0 ? 0 : this.Max(c => c.DesiredSize.Height);
                }
            }

            private bool Contains(UIElement c, Row[] existingGroups)
            {
                foreach (var eg in existingGroups)
                {
                    if (eg.Contains(c))
                        return true;
                }

                return false;
            }

            public void AddRemaining(UIElementCollection allChildren, params Row[] existingGroups)
            {
                foreach (UIElement e in allChildren)
                {
                    if (Contains(e, existingGroups))
                        continue;

                    Add(e);
                }
            }
        }


        protected override Size MeasureOverride(Size availableSize)
        {
            foreach (UIElement child in Children)
            {
                child.Measure(availableSize);
            }

            IEnumerable<Row> rows = InRows(Children);
            canvasHeight = 0;
            canvasWidth = 0;
            foreach (var r in rows)
            {
                canvasWidth = Math.Max(canvasWidth, r.Width);
                canvasHeight += r.Height;
            }

            double x = 0;
            double y = 0;

            foreach (var row in rows)
            {
                int columns = row.Count;

                double w = row.Width;
                x = 0;
                double dx = (canvasWidth - row.Width) / (row.Count - 1);
                foreach (var group in row)
                {
                    positions[group] = new Rect(x, y, group.DesiredSize.Width, row.Height);
                    x += group.DesiredSize.Width + dx;
                }

                y += row.Height;
            }

            return new Size(canvasWidth, canvasHeight);
        }

        private IEnumerable<Row> InRows(UIElementCollection Children)
        {
            Row row1 = new Row();
            row1.AddMatchingGroups(Children, "1", "2", "3", "4", "5");

            Row row2 = new Row();
            row2.AddMatchingGroups(Children, "6", "7", "8", "9", "10", "11", "12");

            Row row3 = new Row();
            row3.AddMatchingGroups(Children, "13", "14", "15", "16", "17", "18");

            Row row4 = new Row();
            row4.AddMatchingGroups(Children, "19", "20", "21", "22", "X", "Y");

            yield return row1;
            yield return row2;
            yield return row3;
            yield return row4;



            // dump the additional, marker, groups at the bottom in 6 column chunks.

            List<UIElement> remaining = new List<UIElement>();
            foreach (UIElement c in Children)
            {
                if (row1.Contains(c) || row2.Contains(c) || row3.Contains(c) || row4.Contains(c))
                    continue;

                remaining.Add(c);
            }

            Row additional = null;

            while (remaining.Count > 0)
            {
                if (additional == null)
                    additional = new Row();

                additional.Add(remaining[0]);
                remaining.RemoveAt(0);

                if (additional.Count == 6)
                {
                    yield return additional;
                    additional = null;
                }
            }

            if (additional != null)
                yield return additional;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            foreach (UIElement c in Children)
            {
                if(positions.ContainsKey(c))
                    c.Arrange(positions[c]);
            }

            positions.Clear();

            return new Size(canvasWidth, canvasHeight);
        }
    }
}
