﻿using System;
using System.Windows.Input;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Data;

namespace AI.Brightfield
{
    public partial class KaryotypeScreenUi
    {
        public KaryotypeScreenUi(WorkflowController controller)
        {
            Controller = controller;
            InitializeComponent();

            Karyotyper.SetPrintElements(karyotypePanel);
        }

        private WorkflowController Controller { get; set; }
        public KaryotypeController Karyotyper { get { return Controller == null ? null : Controller.Tool as KaryotypeController; } }

        private void CanFlipChromByDoubleClick(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        public bool CanGenerateThumbnail()
        {
            return Dispatcher.CheckAccess() && thumbnailSource.IsVisible;
        }

        public BitmapSource GenerateThumbnail()
        {
            RenderTargetBitmap bmp = null;
            int w = (int)thumbnailSource.ActualWidth;
            int h = (int)thumbnailSource.ActualHeight;
            
            bmp = new RenderTargetBitmap(w, h, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(thumbnailSource);
            bmp.Freeze();

            return bmp;
        }

        private void FlipChromByDoubleClick(object sender, ExecutedRoutedEventArgs e)
        {
            ChromosomeClickEventArgs ea = e.Parameter as ChromosomeClickEventArgs;
            if (ea.ClickCount == 2)
            {
                KaryotypeChromosome chromToFlip = ea.Chromosome;
                chromToFlip.UserRotation += 180;
            }
        }

        private void CanShowReassignPopup(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ShowReassignPopup(object sender, ExecutedRoutedEventArgs e)
        {
            chromContextMenu.Tag = ((ChromosomeClickEventArgs)e.Parameter).Chromosome;
            chromContextMenu.IsOpen = true;
        }

        private void CanReclassifyChrom(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ReclassifyChromExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var chrom = chromContextMenu.Tag as KaryotypeChromosome;
            var groupIdentifier = e.Parameter as string;
            var group = Karyotyper.FindGroupFromIdentifier(groupIdentifier);

            Karyotyper.UndoList.Add(new Brightfield.KaryotypeUndo.ReclassifyChromosome(chrom, chrom.Group, chrom.Group.IndexOf(chrom)));

            group.Adopt(chrom);

            chromContextMenu.IsOpen = false;
        }

        private void OnMirror(object sender, RoutedEventArgs e)
        {
            var chrom = chromContextMenu.Tag as KaryotypeChromosome;
            Karyotyper.UndoList.Add(new KaryotypeUndo.FlipChromosome(chrom, chrom.IsXFlipped, chrom.IsYFlipped));
            chrom.IsXFlipped = !chrom.IsXFlipped;

            chromContextMenu.IsOpen = false;
        }

        private void OnCancelFocusMode(object sender, RoutedEventArgs e)
        {
            Karyotyper.Display.FocussedChromosome = null;
        }

        private void OnDeleteChrom(object sender, RoutedEventArgs e)
        {
            var chrom = chromContextMenu.Tag as KaryotypeChromosome;
            Karyotyper.DeleteChromosome(chrom);
            chromContextMenu.IsOpen = false;
        }

        private void OnEditChromosome(object sender, RoutedEventArgs e)
        {
            var chromToFind = chromContextMenu.Tag as KaryotypeChromosome;
            Karyotyper.MoveToChromInMet(chromToFind, VisualTreeWalker.FindParentOfType<StretchOrSizeCanvas>(Controller.ImageControl));
            chromContextMenu.IsOpen = false;
        }

        private void OnChromDroppedOnDeadZone(object sender, DragEventArgs e)
        {
            var data = e.Data.GetData(typeof(ChromosomeGroupControl.ChromDragData)) as ChromosomeGroupControl.ChromDragData;

            var chrom = data.Chromosome;
            var group = Karyotyper.FindGroupFromIdentifier(ChromosomeGroup.UnclassifiedString);

            Karyotyper.UndoList.Add(new Brightfield.KaryotypeUndo.ReclassifyChromosome(chrom, chrom.Group, chrom.Group.IndexOf(chrom)));

            group.Adopt(chrom);
        }
    }


    public class HideEmptyUnclassifieds : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ChromosomeGroup g = (ChromosomeGroup)values[0];
            if (g.Identifier == ChromosomeGroup.UnclassifiedString)
            {
                return g.IsEmpty ? Visibility.Collapsed : Visibility.Visible;
            }
            else
            {
                return Visibility.Visible;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
