﻿using System;

namespace AI.Brightfield
{
    public partial class KaryotypeToolbarUi
    {
        public KaryotypeToolbarUi(WorkflowController controller)
        {
            Controller = controller;
            InitializeComponent();
        }

        private WorkflowController Controller { get; set; }
        private KaryotypeController Karyotyper { get { return Controller == null ? null : Controller.Tool as KaryotypeController; } }

        private void CanSwitchViewsExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Karyotyper == null ? false : Karyotyper.Display.DetectMode == false;
        }

        private void SwitchViewsExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            Karyotyper.SwitchViews();
        }

        private void CanGenerateKaryotypeExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void GenerateKaryotypeExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            if (Karyotyper.Display.ViewingMet)
                Karyotyper.SwitchViews();

            Karyotyper.Classify();
        }

        private void CanThresholdeExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ThresholdExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            Karyotyper.EnterDetectMode();
        }

        private void CanEditModeExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void EditModeExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            Karyotyper.Display.EditMode = true;
        }

        private void CanAddModeExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void AddModeExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            Karyotyper.Display.AddMode = true;
        }

        private void CanDeleteModeExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void DeleteModeExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            Karyotyper.Display.DeleteMode = true;
        }

        private void CanClearKaryotypeExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            if (Karyotyper == null)
                e.CanExecute = false;
            else
                e.CanExecute = Karyotyper.ChromosomeCount > 0;
        }

        private void ClearKaryotypeExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            Karyotyper.UndoList.BeginRecording();
            Karyotyper.ClearChromosomes();
            Karyotyper.UndoList.EndRecording(() => Karyotyper.NotifyGotChromosomes());

            if(Karyotyper.Display.ViewingMet == false)
                Karyotyper.SwitchViews();
        }
    }
}
