﻿using System;
using System.Windows;
using System.Linq;
using System.Windows.Data;
using System.Windows.Controls;

namespace AI.Brightfield
{
    public partial class KaryotypeToolbarUi_Threshold
    {
        public static DependencyProperty ControllerProperty = DependencyProperty.Register("Controller", typeof(WorkflowController), typeof(KaryotypeToolbarUi_Threshold));

        public KaryotypeToolbarUi_Threshold()
        {
            InitializeComponent();
        }

        public WorkflowController Controller
        {
            get { return (WorkflowController)GetValue(ControllerProperty); }
            set { SetValue(ControllerProperty, value); }
        }

        private void OnThresh(object sender, System.Windows.RoutedEventArgs e)
        {
            var k = (KaryotypeController)Controller.Tool;

            k.SetManualThreshold();
            AnalysisPage.FireResetFocus(this);
        }

        private void OnThreshManually(object sender, RoutedEventArgs e)
        {
            var k = (KaryotypeController)Controller.Tool;

            k.CancelThresholdMode();
            AnalysisPage.FireResetFocus(this);
        }
    }
}
