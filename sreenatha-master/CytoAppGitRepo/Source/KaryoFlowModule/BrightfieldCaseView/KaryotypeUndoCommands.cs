﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.Brightfield.KaryotypeUndo
{
    public class DeleteChromosome : IUndoItem
    {
        KaryotypeChromosome chromosome;
        ChromosomeGroup group;
        int index;

        public DeleteChromosome(KaryotypeChromosome chromosome, ChromosomeGroup group, int chromIndexInGroup)
        {
            this.index = chromIndexInGroup;
            this.group = group;
            this.chromosome = chromosome;
        }

        public void Undo(object state)
        {
            group.Adopt(chromosome, index);
        }
    }

    public class AddChromosome : IUndoItem
    {
        KaryotypeChromosome chromosome;
        ChromosomeGroup group;

        public AddChromosome(KaryotypeChromosome chromosome, ChromosomeGroup group)
        {
            this.group = group;
            this.chromosome = chromosome;
        }

        public void Undo(object state)
        {
            group.Remove(chromosome);
        }
    }

    public class RotateChromosome : IUndoItem
    {
        KaryotypeChromosome chromosome;
        double originalAngle;

        public RotateChromosome(KaryotypeChromosome chromosome, double originalAngle)
        {
            this.originalAngle = originalAngle;
            this.chromosome = chromosome;
        }

        public void Undo(object state)
        {
            chromosome.UserRotation = originalAngle;
        }
    }
    
    public class FlipChromosome : IUndoItem
    {
        KaryotypeChromosome chromosome;
        bool x, y;

        public FlipChromosome(KaryotypeChromosome chromosome, bool originalX, bool originalY)
        {
            this.x = originalX;
            this.y = originalY;
            this.chromosome = chromosome;
        }

        public void Undo(object state)
        {
            chromosome.IsXFlipped = x;
            chromosome.IsYFlipped = y;
        }
    }

    public class ReclassifyChromosome : IUndoItem
    {
        KaryotypeChromosome chromosome;
        ChromosomeGroup originalGroup;
        int indexInOriginalGroup;

        public ReclassifyChromosome(KaryotypeChromosome chromosome, ChromosomeGroup originalGroup, int indexInOriginalGroup)
        {
            this.chromosome = chromosome;
            this.indexInOriginalGroup = indexInOriginalGroup;
            this.originalGroup = originalGroup;
        }

        public void Undo(object state)
        {
            originalGroup.Insert(indexInOriginalGroup, chromosome);
        }
    }

    public class SwapChromosomesWithinGroup : IUndoItem
    {
        ChromosomeGroup group;
        KaryotypeChromosome a, b;

        public SwapChromosomesWithinGroup(ChromosomeGroup group, KaryotypeChromosome a, KaryotypeChromosome b)
        {
            this.a = a;
            this.b = b;
            this.group = group;
        }

        public void Undo(object state)
        {
            group.Swap(a, b);
        }
    }

    public class NudgeChromosome : IUndoItem
    {
        double x, y;
        KaryotypeChromosome chrom;

        public NudgeChromosome(KaryotypeChromosome chrom, double originalX, double originalY)
        {
            x = originalX;
            y = originalY;
            this.chrom = chrom;
        }

        public KaryotypeChromosome Chromosome
        {
            get { return chrom; }
        }

        public void Undo(object state)
        {
            chrom.UserOffsetX = x;
            chrom.UserOffsetY = y;
        }
    }

}
