﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Collections.ObjectModel;
using AI.Brightfield.ManualCountImplementation;
using System.Collections.Generic;
using System.Windows.Input;
using AI.Bitmap;
using AI.Connect;
using System.Windows.Ink;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Media.Effects;
using System.Xml.Linq;

namespace AI.Brightfield
{
    public static class KaryotypeCommands
    {
        public static RoutedUICommand ForceInputFocusToRightPlace = new RoutedUICommand("ForceInputFocusToRightPlace", "ForceInputFocusToRightPlace", typeof(KaryotypeCommands));

        public static RoutedUICommand SwitchViews = new RoutedUICommand("SwitchViews", "SwitchViews", typeof(KaryotypeCommands));
        public static RoutedUICommand GenerateKaryotype = new RoutedUICommand("GenerateKaryotype", "GenerateKaryotype", typeof(KaryotypeCommands));
        public static RoutedUICommand Threshold = new RoutedUICommand("Threshold", "Threshold", typeof(KaryotypeCommands));
        public static RoutedUICommand CancelFocusEdit = new RoutedUICommand("CancelFocusEdit", "CancelFocusEdit", typeof(KaryotypeCommands));

        public static RoutedUICommand EditMode = new RoutedUICommand("EditMode", "EditMode", typeof(KaryotypeCommands));
        public static RoutedUICommand AddMode = new RoutedUICommand("AddMode", "AddMode", typeof(KaryotypeCommands));
        public static RoutedUICommand DeleteMode = new RoutedUICommand("DeleteMode", "DeleteMode", typeof(KaryotypeCommands));
        public static RoutedUICommand ClearKaryotype = new RoutedUICommand("ClearKaryotype", "ClearKaryotype", typeof(KaryotypeCommands));
    }

    public sealed class KaryotypeController : ToolNotifier, IClosing
    {
        private Cell cell;
        private UndoList undoList;
        private WorkflowController controller;
        private KaryotypeScreenUi screenUi;
        private FrameworkElement karyotypeCanvas;
        private ThresholdVisualizer thresholdOutlines;
        private Channel masterMask;
        private Dispatcher dispatcher;

        public KaryotypeController(ImageRenderer mainImageRenderer, Dispatcher dispatcher)
        {
            this.dispatcher = dispatcher;
            thresholdOutlines = new ThresholdVisualizer
            {
                Threshold = 0.33
            };

            MetImageRenderer = mainImageRenderer;
            
            undoList = new UndoList();
            undoList.UndoCompleted += (s, e) =>
            {
                if (!Display.ViewingMet && ChromosomeCount == 0)
                    Display.ViewingMet = true;

                Notify("Chromosomes", "ChromosomeCount", "ShowMainImage");
            };
        }

        internal void SetPrintElements(FrameworkElement kary)
        {
            karyotypeCanvas = kary;
        }

        #region IBrightfieldWorkflowTool Implementation

        public override bool RequiresFullImage { get { return true; } }

        // dont allow modification of display image while thresholding
        public override bool IsDisplayPopupEnabled { get { return Display.DetectMode == false; } }
        
        public override UndoList UndoList
        {
            get { return undoList; }
        }

        public override void Load(Cell cell, WorkflowController controller)
        {
            this.cell = cell;
            this.controller = controller;

            display = new KaryotypeDisplayController(controller.Persistants);
            groups = new ObservableCollection<ChromosomeGroup>(from g in ChromosomeGroup.GroupsForCase(controller.CaseId)
                                                               select ChromosomeGroup.GroupForClass(g, cell.Id));

            MetImageRenderer.Threshold = -1;

            LoadChromosomes();
            undoList.Clear();


			if (GotClassifiedChromosomes)
			{
				// Display karyotype
				Display.DetectMode = false;
				Display.ViewingMet = false;
			}
			else if (GotChromosomes)
			{
				// Display metaphase
				Display.DetectMode = false;
				Display.ViewingMet = true;
			}
			else
			{
				EnterDetectMode();
			}

            Notify("ShowMainImage", "IsDisplayPopupEnabled");
        }

        public override void Save()
        {
            var dataSource = DataSourceFactory.GetDataSource();
            dispatcher.Invoke((ThreadStart)delegate
            {
                KaryotypeThumbnailGenerator.SaveKaryotypeThumbnail(cell, groups, controller.RelativeIdCell, dataSource);
            }, DispatcherPriority.Normal);

            var maskPath = MaskPath(cell.Id);


            dataSource.DeleteItem(controller.RelativeIdCell, maskPath);

            var maskChannelStream = new MemoryStream();
            ImageFileSerialize.WriteChannelDataToStream(maskChannelStream, masterMask, true);
            dataSource.SaveStream(controller.RelativeIdCell, maskChannelStream, maskPath);

            var chromsWithInvalidatedImages = from k in Chromosomes where k.IsImageInvalidated select k;
            var tmp = (from k in chromsWithInvalidatedImages
                       select new
                       {
                           Chrom = k,
                           MaskCache = k.Mask.CopyPixels()
                       }).ToArray();

            Parallel.ForEach(tmp, k =>
            {
                k.Chrom.SaveDisplayImageBlob(KaryotypeChromosome.ChromosomeDataFile(k.Chrom.Id), k.MaskCache, controller.RelativeIdCell, dataSource);
            });

            var chromoMetaDataXml = new XElement("ChromesomeMetaData");
            foreach (var g in groups)
            {
                int index = 0;
                foreach (var c in g.Chromosomes)
                {
                    chromoMetaDataXml.Add(c.SaveNonImageDataXml(cell.Id, index++));
                }
            }

            cell.IsKaryotyped = GotChromosomes;

            dataSource.SaveXml(controller.RelativeIdCell, chromoMetaDataXml, "cell.ChromoMetaData");
            dataSource.SaveXml(controller.RelativeIdCell, cell.ToXml(), "cell.cellstatus");


            if (GotNonstandardGroups())
            {
                //ChromosomeGroup.WriteToDb(groups, controller.CaseId); // doesn't do anything?
            }

            //using (var lims = Connect.LIMS.New())
            //{
            //    lims.MarkCaseAsModified(controller.Case, DateTime.Now, "Karyotype");
            //}

            controller.SetKaryotyped(cell, this.GotClassifiedChromosomes);
            undoList.Clear();
        }

        public static string MaskPath(Guid cellId)
        {
            return cellId.ToString() + ".karyotypeMask";
        }

        private void LoadChromosomes()
        {
            var dataSource = DataSourceFactory.GetDataSource();
            var chromosomeXml = dataSource.LoadXml(controller.RelativeIdCell, "cell.ChromoMetaData");

            if (chromosomeXml == null)
                return;

            var kroms = (from k in chromosomeXml.Descendants("Chromosome")
                        select KaryotypeChromosome.CreateFromXml(k)).ToArray();

            foreach (var k in kroms)
            {
                KaryotypeChromosome.InitializeMask(k, controller.RelativeIdCell, dataSource);
                k.SetImageNotInvalidated();

                var tag = chromosomeXml.Descendants("Chromosome").Where(c => c.Attribute("karyotypeChromosomeId").Value == k.Id.ToString()).First().Attribute("chromosomeTag").Value;
                var g = FindGroup(tag);
                AddChromosomeToGroupWithoutArrange(k, g);
            }

            var maskPath = MaskPath(cell.Id);

            using (var stream = dataSource.LoadStream(controller.RelativeIdCell, maskPath))
            {
                masterMask = ImageFromBitmapSource.LoadFromStream(stream);
            }

            if (masterMask == null)
                masterMask = new Channel(controller.CurrentCellImage.ImageWidth, controller.CurrentCellImage.ImageHeight, 8);

            if (masterMask.Width != controller.Renderer.ProcessedChannel.Width)
            {
                masterMask = KaryotypeMask.CreateMask(controller.Renderer.ProcessedChannel, (ushort)cell.KaryotypeThreshold);
            }

            UpdateChromosomeDisplayImages(MetImageRenderer);
        }

        private ChromosomeGroup FindGroup(string p)
        {
            return groups.FirstOrDefault(g => g.Identifier == p);
        }

        public override FrameworkElement ImageUi
        {
            get { return new KaryotypeImageUi(controller); }
        }

        public override StretchSizeMode ImageUiSizeMode
        {
            get { return StretchSizeMode.Size; }
        }

        public override FrameworkElement ScreenUi
        {
            get 
            { 
                screenUi = new KaryotypeScreenUi(controller);
                return screenUi;
            }
        }

        public override FrameworkElement ToolbarUi
        {
            get { return new KaryotypeToolbarUi(controller); }
        }

        #endregion

        public void Closed()
        {
            MetImageRenderer.Threshold = -1;
        }

        private const int OverlapPercentage = 75;
        private ObservableCollection<ChromosomeGroup> groups;
        private KaryotypeDisplayController display;

        public KaryotypeChromosome ChromosomeUnderMouse { get { return Chromosomes.Where(k => k.IsMouseOver).FirstOrDefault(); } }

        public override void OnKeyDown(ToolKeyEventArgs e)
        {
            var chrom = Display.FocussedChromosome;
            if (chrom == null)
                chrom = ChromosomeUnderMouse;
            
            if (chrom != null)
            {
                switch (e.Key)
                {
                    case Key.Escape:
                        e.Handled = true;
                        Display.FocussedChromosome = null;
                        break;
                }
            }

            if(e.Handled)
                return;

            switch (e.Key)
            {
                case Key.V:
                    Display.ShowSolidChromosomes = !Display.ShowSolidChromosomes;
                    e.Handled = true;
                    break;

                case Key.B:
                    Display.ShowBoundaries = !Display.ShowBoundaries;
                    e.Handled = true;
                    break;

                case Key.A:
                    Display.AddMode = true;
                    e.Handled = true;
                    break;

                case Key.D:
                    Display.DeleteMode = true;
                    e.Handled = true;
                    break;

                case Key.Delete:
                    if (chrom != null)
                    {
                        DeleteChromosome(chrom);
                        e.Handled = true;
                    }
                    break;

                case Key.E:
                    Display.EditMode = true;
                    e.Handled = true;
                    break;

                case Key.Tab:
                    if (GotChromosomes)
                    {
                        Display.ViewingMet = !Display.ViewingMet;
                        Notify("ShowMainImage");
                        e.Handled = true;
                    }
                    break;
            }
        }

        public override void OnKeyUp(ToolKeyEventArgs e)
        {
        }

        public KaryotypeDisplayController Display
        {
            get { return display; }
        }

        public ImageRenderer MetImageRenderer
        {
            get;
            private set;
        }

        public IEnumerable<ChromosomeGroup> ChromosomeGroups
        {
            get { return groups; }
        }

        public bool GotChromosomes
        {
            get
            {
                if (groups.Count == 0)
                    return false;

                foreach (var g in groups)
                {
                    if (!g.IsEmpty)
                        return true;
                }

                return false;
            }
        }

		private bool GotClassifiedChromosomes
		{
			get
			{
				foreach (var c in Chromosomes)
				{
					if (c.IsClassified)
					{
						return true;
					}
				}

				return false;
			}
		}

        public IEnumerable<KaryotypeChromosome> Chromosomes
        {
            get
            {
                foreach (var g in groups)
                {
                    foreach (var c in g.Chromosomes)
                        yield return c;
                }
            }
        }

        public void Classify()
        {
            undoList.BeginRecording();
            try
            {
                MeasureChromosomes();

                Woolz.ClassifyMetaphase((from c in Chromosomes select c.Measurements).ToArray());

                var all = Chromosomes.ToArray();
                foreach (KaryotypeChromosome c in all)
                {
                    int classIndex = c.Measurements.Group - 1;
                    var newGroup = classIndex > 23 ? FindUnclassifiedGroup() : ChromosomeGroups.ElementAt(classIndex);
                    var originalGroup = c.Group;
                    var indexInOriginal = originalGroup == null ? 0 : originalGroup.IndexOf(c);
                    undoList.Add(new KaryotypeUndo.ReclassifyChromosome(c, originalGroup, indexInOriginal));
                    newGroup.Adopt(c);
                }

                foreach (var g in groups)
                {
                    var unclassifiedGroup = FindUnclassifiedGroup();

                    if (g.Chromosomes.Count() > 2)
                    {
                        var onesToDump = (from c in g.Chromosomes
                                          orderby c.Measurements.Confidence descending
                                          select c).Skip(2);

                        foreach (var c in onesToDump)
                        {
                            var index = g.IndexOf(c);
                            unclassifiedGroup.Adopt(c);
                            undoList.Add(new KaryotypeUndo.ReclassifyChromosome(c, g, index));
                        }
                    }
                }
            }
            finally
            {
                undoList.EndRecording();
            }

            Notify("GotChromosomes");
        }

        private void DetectFromMask(Channel source, Channel mask, byte[] displayImageBytes)
        {
            undoList.BeginRecording();
            try
            {
                var detectedObjects = Woolz.DetectObjects(source.ApplyMask(mask), 1, KaryotypeChromosome.ObjectDetectionMargin, KaryotypeChromosome.MinArea).ToList();
                detectedObjects.Sort((a, b) => -a.Length.CompareTo(b.Length));

                ClearChromosomes();

                foreach (var w in detectedObjects)
                {
                    KaryotypeChromosome c = KaryotypeChromosome.CreateFromWoolz(cell.Id, w.Bounds, w, displayImageBytes, source.Width);

                    AddChromosomeToGroupWithNotify(c, FindUnclassifiedGroup());
                }
            }
            finally
            {
                undoList.EndRecording();
            }
        }

        private unsafe void AddChromosomeToGroupWithoutArrange(KaryotypeChromosome obj, ChromosomeGroup group)
        {
            if (obj.Group != null)
                obj.Group.Remove(obj);

            group.AdoptWithoutArrange(obj);

            undoList.Add(new KaryotypeUndo.AddChromosome(obj, group));
        }

        private void AddChromosomeToGroup(KaryotypeChromosome obj, ChromosomeGroup group)
        {
            if (obj.Group != null)
                obj.Group.Remove(obj);

            group.Adopt(obj);

            undoList.Add(new KaryotypeUndo.AddChromosome(obj, group));
        }

        private KaryotypeChromosome AddChromosomeToGroupWithNotify(KaryotypeChromosome obj, ChromosomeGroup group)
        {
            AddChromosomeToGroup(obj, group);

            Notify("GotChromosomes", "Chromosomes", "ChromosomeCount");

            return obj;
        }

        private void DeleteModeStroke(Stroke stroke, ImageRenderer metImage)
        {
            IEnumerable<KaryotypeChromosome> chroms = Chromosomes;
            if (Display.FocussedChromosome != null)
                chroms = new KaryotypeChromosome[] { Display.FocussedChromosome };
            
            var intersectingChroms = FindIntersectingChromosomesForStroke(chroms, stroke);
            undoList.BeginRecording();

            foreach (var c in intersectingChroms)
                DeleteChromosome(c);

            undoList.EndRecording();
        }

        private void EditChromosomeFromStroke(Stroke stroke, ImageRenderer metImage)
        {
            Point startPoint = (Point)stroke.StylusPoints.First();
            Point endPoint = (Point)stroke.StylusPoints.Last();
            KaryotypeChromosome startChrom = FindChromosomeContaining(startPoint);
            KaryotypeChromosome endChrom = FindChromosomeContaining(endPoint);

            var focusChrom = Display.FocussedChromosome;
            var chromsomesToHit = Chromosomes;
            if (Display.FocussedChromosome != null)
                chromsomesToHit = new KaryotypeChromosome[] { Display.FocussedChromosome };

            if (focusChrom != null)
            {
                var startHits = Chromosomes.Where(c => c.Contains(startPoint));
                var endHits = Chromosomes.Where(c => c.Contains(endPoint));
                startChrom = startHits.Contains(focusChrom) ? focusChrom : null;
                endChrom = endHits.Contains(focusChrom) ? focusChrom : null;
            }

            if (startChrom == null)
            {
                // Start and end point are outside any chromosomes
                // so try to split a chromosome
                var drawPoints = from p in stroke.StylusPoints select (Point)p;

                var editChroms = FindIntersectingChromosomesForStroke(chromsomesToHit, stroke);
                if (editChroms != null && editChroms.Count() > 0)
                {
                    undoList.BeginRecording();

                    foreach (var editChrom in editChroms)
                        SplitChromosome(editChrom, stroke, metImage);

                    undoList.EndRecording();
                }
            }
            else
            {
                // Start and end in the same chromosome
                // so try to extend the chromosome
                ExtendChromosome(startChrom, stroke, metImage);
            }

            Display.FocussedChromosome = null;
        }

        /// <summary>
        /// Splits a chromosome by a drawn path. If an object produced by the split is too small it is
        /// removed ie trimmed.
        /// </summary>
        /// <param name="editChrom"></param>
        /// <param name="stroke"></param>
        /// <param name="metImage"></param>
        private void SplitChromosome(KaryotypeChromosome editChrom, Stroke stroke, ImageRenderer metImage)
        {
            var RawImage = controller.Renderer.ProcessedChannel;

            // Union of bounds
            Rect drawBounds = Rect.Intersect(stroke.GetBounds(), new Rect(0.0, 0.0, RawImage.Width, RawImage.Height));
            Int32Rect chromBounds = editChrom.Bounds;
            Int32Rect newBounds = Int32Rect.Empty;
            newBounds.X = Math.Min((int)drawBounds.X, chromBounds.X);
            newBounds.Y = Math.Min((int)drawBounds.Y, chromBounds.Y);
            newBounds.Width = Math.Max((int)(drawBounds.X + drawBounds.Width), chromBounds.X + chromBounds.Width) - newBounds.X;
            newBounds.Height = Math.Max((int)(drawBounds.Y + drawBounds.Height), chromBounds.Y + chromBounds.Height) - newBounds.Y;

            // Create chromosome mask
            Channel chromMask = KaryChannel.CreateMask(newBounds.Width, newBounds.Height);
            chromMask.DrawInto(chromBounds.X - newBounds.X, chromBounds.Y - newBounds.Y, editChrom.Mask.ToChannel());

            // Create cut mask but make it an inverse (background black, split line white)
            KaryotypeMask strokeMask = new KaryotypeMask((int)drawBounds.Width, (int)drawBounds.Height, Brushes.Black);
            stroke.DrawingAttributes.Width = 1;
            StrokeDrawing.RenderStroke(stroke, Brushes.White, null, (RenderTargetBitmap)strokeMask.Image, -drawBounds.Left, -drawBounds.Top);
            Channel cutMask = KaryChannel.CreateInverseMask(newBounds.Width, newBounds.Height);
            cutMask.DrawInto((int)drawBounds.X - newBounds.X, (int)drawBounds.Y - newBounds.Y, strokeMask.ToChannel());
            // The white stroke is rendered with aliasing making the cut grey in the cutmask
            // so force the grey cut to be white.
            cutMask.ApplyThreshold(0, 255);

            Channel mask = KaryChannel.PixelOr(chromMask, cutMask);
            Channel source = RawImage.Crop(newBounds);
            Channel clippedCutMask = KaryChannel.PixelOr(chromMask, KaryChannel.Invert(cutMask));

            var objects = Woolz.DetectSplitObjects(source.ApplyMask(mask), clippedCutMask, 1, KaryotypeChromosome.ObjectDetectionMargin, 100);
            if (objects.Count() > 0)
            {
                WoolzObject largestWoolzObject = objects.OrderByDescending((o) => o.Measurements.Area).First();
                bool splitResultedInOneObject = objects.Count() == 1;

                foreach (WoolzObject w in objects)
                {
                    // Redetect object over restored mask
                    Channel restoredMask = w.Mask;
                    Int32Rect restoredBounds = new Int32Rect(newBounds.X + w.Bounds.X, newBounds.Y + w.Bounds.Y, w.Bounds.Width, w.Bounds.Height);
                    Channel restoredSource = RawImage.Crop(restoredBounds);
                    var restoredObjects = Woolz.DetectObjects(restoredSource.ApplyMask(restoredMask), 1, KaryotypeChromosome.ObjectDetectionMargin, KaryotypeChromosome.MinArea);
                    WoolzObject restoredObject = restoredObjects.OrderByDescending((o) => o.Measurements.Area).First();

                    Int32Rect newChromBounds = new Int32Rect(newBounds.X + w.Bounds.X + restoredObject.Bounds.X,
                                                             newBounds.Y + w.Bounds.Y + restoredObject.Bounds.Y,
                                                             restoredObject.Bounds.Width, restoredObject.Bounds.Height);
                    KaryotypeChromosome newChrom = KaryotypeChromosome.CreateFromWoolz(cell.Id, newChromBounds, restoredObject, metImage.CopyPixels(newChromBounds));

                    if (w == largestWoolzObject)
                    {
                        newChrom.PseudoColour = editChrom.PseudoColour;
                    }
                    else
                    {
                        while (newChrom.PseudoColour == editChrom.PseudoColour)
                        {
                            newChrom.PseudoColour = KaryotypeChromosomeColoring.Random();
                        }
                    }

                    if (splitResultedInOneObject)
                    {
                        CopyOrientationAndStuff(editChrom, newChrom);
                        AddChromosomeToGroupWithNotify(newChrom, editChrom.Group);
                    }
                    else
                    {
                        AddChromosomeToGroupWithNotify(newChrom, FindUnclassifiedGroup());
                    }
                }

                DeleteChromosome(editChrom);
            }
        }

        /// <summary>
        /// Extends a chromosome by a drawn path. The chromosome is replaced by a new chromosome
        /// composed of the old chromosome and the filled path.
        /// </summary>
        /// <remarks>
        /// Assume the stroke starts and finishes in the chromosome.
        /// </remarks>
        /// <param name="editChrom"></param>
        /// <param name="stroke"></param>
        /// <param name="metImage"></param>
        private void ExtendChromosome(KaryotypeChromosome editChrom, Stroke stroke, ImageRenderer metImage)
        {
            undoList.BeginRecording();
            var RawImage = controller.Renderer.ProcessedChannel;

            try
            {
                // Union of bounds
                Rect drawBounds = Rect.Intersect(stroke.GetBounds(), new Rect(0.0, 0.0, RawImage.Width, RawImage.Height));
                Int32Rect chromBounds = editChrom.Bounds;
                Int32Rect newBounds = Int32Rect.Empty;
                newBounds.X = Math.Min((int)drawBounds.X, chromBounds.X);
                newBounds.Y = Math.Min((int)drawBounds.Y, chromBounds.Y);
                newBounds.Width = Math.Max((int)(drawBounds.X + drawBounds.Width), chromBounds.X + chromBounds.Width) - newBounds.X;
                newBounds.Height = Math.Max((int)(drawBounds.Y + drawBounds.Height), chromBounds.Y + chromBounds.Height) - newBounds.Y;

                // Create chromosome mask
                Channel chromMask = KaryChannel.CreateMask(newBounds.Width, newBounds.Height);
                chromMask.DrawInto(chromBounds.X - newBounds.X, chromBounds.Y - newBounds.Y, editChrom.Mask.ToChannel());

                // Create draw points mask
                KaryotypeMask strokeMask = new KaryotypeMask((int)drawBounds.Width, (int)drawBounds.Height);
                StrokeDrawing.RenderStroke(stroke, Brushes.Black, Brushes.Black, (RenderTargetBitmap)strokeMask.Image, -drawBounds.Left, -drawBounds.Top);
                Channel drawMask = KaryChannel.CreateMask(newBounds.Width, newBounds.Height);
                drawMask.DrawInto((int)drawBounds.X - newBounds.X, (int)drawBounds.Y - newBounds.Y, strokeMask.ToChannel());

                Channel mask = KaryChannel.PixelAnd(chromMask, drawMask);
                Channel source = RawImage.Crop(newBounds);

                var objects = Woolz.DetectObjects(mask, 1, KaryotypeChromosome.ObjectDetectionMargin, KaryotypeChromosome.MinArea);
                var newChromObj = objects.OrderByDescending((o) => o.Measurements.Area).First();

                Int32Rect newChromBounds = new Int32Rect(newBounds.X + newChromObj.Bounds.X, newBounds.Y + newChromObj.Bounds.Y,
                                                         newChromObj.Bounds.Width, newChromObj.Bounds.Height);
                KaryotypeChromosome newChrom = KaryotypeChromosome.CreateFromWoolz(cell.Id, newChromBounds, newChromObj, metImage.CopyPixels(newChromBounds));
                newChrom.PseudoColour = editChrom.PseudoColour;

                CopyOrientationAndStuff(editChrom, newChrom);

                DeleteChromosome(editChrom);
                AddChromosomeToGroupWithNotify(newChrom, editChrom.Group);
            }
            finally
            {
                undoList.EndRecording();
            }
        }

        private unsafe void CopyOrientationAndStuff(KaryotypeChromosome src, KaryotypeChromosome dest)
        {
            dest.IsXFlipped = src.IsXFlipped;
            dest.IsXFlipped = src.IsYFlipped;
            dest.Rotation = src.Rotation;
            dest.TranslateX = src.TranslateX;
            dest.TranslateY = src.TranslateY;
        }

        private void CreateChromosomeFromStroke(Stroke stroke, ImageRenderer metImage, bool unMaskedAdd)
        {
            undoList.BeginRecording();
            var RawImage = controller.Renderer.ProcessedChannel;

            try
            {
                var bounds = stroke.GetBounds();
                double offsetX = -(bounds.Left - KaryotypeChromosome.ObjectDetectionMargin);
                double offsetY = -(bounds.Top - KaryotypeChromosome.ObjectDetectionMargin);
                bounds.Inflate(KaryotypeChromosome.ObjectDetectionMargin, KaryotypeChromosome.ObjectDetectionMargin);
                bounds = Rect.Intersect(bounds, new Rect(0.0, 0.0, RawImage.Width, RawImage.Height));

                var intBounds = new Int32Rect((int)bounds.Left, (int)bounds.Top, (int)bounds.Width, (int)bounds.Height);

                KaryotypeMask strokeMask = new KaryotypeMask(intBounds.Width, intBounds.Height);
                StrokeDrawing.RenderStroke(stroke, Brushes.Black, null, (RenderTargetBitmap)strokeMask.Image, offsetX, offsetY);

                Channel source;

                if (!unMaskedAdd)
                {
                    source = RawImage.ApplyMask(masterMask);
                    source = source.Crop(intBounds);
                    source = source.ApplyMask(strokeMask.ToChannel(source.BitsPerPixel));
                }
                else
                {
                    source = strokeMask.ToChannel();
                }

                var objects = Woolz.DetectObjects(source, 1, KaryotypeChromosome.ObjectDetectionMargin, 0);

                foreach(var newChromObj in objects)
                {
                    Int32Rect newChromBounds = new Int32Rect(intBounds.X + newChromObj.Bounds.X, intBounds.Y + newChromObj.Bounds.Y,
                                                             newChromObj.Bounds.Width, newChromObj.Bounds.Height);
                    KaryotypeChromosome newChrom = KaryotypeChromosome.CreateFromWoolz(cell.Id, newChromBounds, newChromObj, metImage.CopyPixels(newChromBounds));

                    // Remove overlapping chromosome
                    var chromSamplePoints = from p in stroke.StylusPoints where newChrom.Contains((Point)p) select (Point)p;
                    KaryotypeChromosome[] removedChroms = null;
                    KaryotypeChromosome overlappedChrom = FindOverlappingChromosome(chromSamplePoints);
                    if (overlappedChrom != null)
                    {
                        DeleteChromosome(overlappedChrom);
                        removedChroms = new[] { overlappedChrom };
                    }

                    AddChromosomeToGroupWithNotify(newChrom, FindUnclassifiedGroup());
                }
            }
            finally
            {
                undoList.EndRecording();
            }
        }

        private IEnumerable<KaryotypeChromosome> FindIntersectingChromosomes(Stroke stroke)
        {
            var selected = new List<KaryotypeChromosome>();
            var points = from s in stroke.StylusPoints select new Point(s.X, s.Y);
            foreach (var chrom in Chromosomes)
                foreach (var pt in points)
                    if (chrom.Contains(pt))
                    {
                        selected.Add(chrom);
                        break;
                    }
            return selected;
        }

        private PathGeometry GetPathGeometry(Point [] pts, bool closed)
        {
            PathGeometry pg = new PathGeometry();
            PathFigure pf = new PathFigure();

            pf.IsClosed = closed;
            pf.StartPoint = pts[0];

            PathSegmentCollection LineSegmentCollection = new PathSegmentCollection();

            for (int i = 1; i < pts.Count(); i++)
            {
                LineSegment newSegment = new LineSegment();
                newSegment.Point = pts[i];
                LineSegmentCollection.Add(newSegment);
            }

            pf.Segments = LineSegmentCollection;
            pg.Figures.Add(pf);
            return pg;
        }

        private IEnumerable<KaryotypeChromosome> FindIntersectingChromosomesForStroke(IEnumerable<KaryotypeChromosome> chromosomesToTest, Stroke stroke)
        {
            Geometry strokePath = stroke.GetGeometry();

            var selected = (from c in chromosomesToTest
                            where StrokeIntersectsChromosome(strokePath, c) || StrokeIsEntirelyInsideChromosome(strokePath, c)
                            select c).ToList();

            return selected;
        }

        private bool StrokeIntersectsChromosome(Geometry strokePath, KaryotypeChromosome c)
        {
            var r = strokePath.FillContainsWithDetail(GetPathGeometry(c.Outline.ToArray(), true));
            return r == IntersectionDetail.Intersects || r == IntersectionDetail.FullyContains;
        }

        private bool StrokeIsEntirelyInsideChromosome(Geometry strokePath, KaryotypeChromosome c)
        {
            var chromPathGeometry = GetPathGeometry(c.Outline.ToArray(), true);
            return chromPathGeometry.FillContains(strokePath);
        }

        /// <summary>
        /// Find the chromosome lying over a path.
        /// </summary>
        /// <param name="chromSamplePoints"></param>
        /// <returns></returns>
        private IEnumerable<KaryotypeChromosome> FindOverlayingChromosomes(Rect area)
        {
            var overlaps = from k in Chromosomes
                           where Rect.Intersect(new Rect(k.Bounds.X, k.Bounds.Y, k.Bounds.Width, k.Bounds.Height), area) != Rect.Empty
                           select k;

            return overlaps.ToArray();
        }

        /// <summary>
        /// Find the chromosome overlapping a given chromosome represented by sample points.
        /// </summary>
        /// <param name="chromSamplePoints"></param>
        /// <returns></returns>
        private KaryotypeChromosome FindOverlappingChromosome(IEnumerable<Point> chromSamplePoints)
        {
            KaryotypeChromosome overlappingChrom = null;

            var overlaps = from p in chromSamplePoints group p by FindChromosomeContaining(p);

            var bestOverlap = overlaps.OrderByDescending((g) => g.Count()).FirstOrDefault();
            if (bestOverlap != null)
            {
                int maxCount = bestOverlap.Count();
                if (maxCount > 10 && maxCount > chromSamplePoints.Count() * OverlapPercentage / 100.0)
                {
                    overlappingChrom = bestOverlap.Key;
                }
            }

            return overlappingChrom;
        }

        /// <summary>
        /// Finds the chromosome containing the point.
        /// </summary>
        /// <param name="centre"></param>
        /// <returns>A chromosome containing the point or null if no chromosome found.</returns>
        public KaryotypeChromosome FindChromosomeContaining(Point pos)
        {
            var hits = Chromosomes.Where(c => c.Contains(pos));
            return Closest(hits, pos);
        }

        private KaryotypeChromosome Closest(IEnumerable<KaryotypeChromosome> set, Point pos)
        {
            KaryotypeChromosome closest = null;
            double closestDistance = double.MaxValue;

            foreach (var k in set)
            {
                var centerX = k.Bounds.X + (k.Bounds.Width / 2);
                var centerY = k.Bounds.Y + (k.Bounds.Height / 2);
                var dist = (pos.X - centerX) * (pos.X - centerX) + (pos.Y - centerY) * (pos.Y - centerY);
                if (dist < closestDistance)
                {
                    closestDistance = dist;
                    closest = k;
                }
            }

            return closest;
        }

        private void MeasureChromosomes()
        {
            foreach (KaryotypeChromosome obj in Chromosomes)
            {
                if (obj.Measurements == null)
                {
                    Channel source = controller.Renderer.ProcessedChannel.Crop(obj.Bounds);
                    var objects = Woolz.DetectObjects(source.ApplyMask(obj.Mask.ToChannel()), 1, KaryotypeChromosome.ObjectDetectionMargin, KaryotypeChromosome.MinArea);
                    if (objects.Count() > 0)
                    {
                        obj.SetMeasurements(objects.First());
                    }
                }
            }
        }

        public void ClearChromosomes()
        {
            foreach (var g in groups)
            {
                int n = g.Chromosomes.Count();
                for(int i = 0; i < n; ++i)
                {
                    var k = g.Chromosomes.ElementAt(i);
                    undoList.Add(new KaryotypeUndo.DeleteChromosome(k, g, i));
                }

                g.Clear();
            }

            Notify("GotChromosomes", "Chromosomes", "ChromosomeCount");
        }

        public void DeleteChromosome(KaryotypeChromosome chrom)
        {
            if (chrom != null)
            {
                foreach (var g in groups)
                {
                    if (g.Contains(chrom))
                    {
                        int index = g.IndexOf(chrom);
                        g.Remove(chrom);
                        undoList.Add(new KaryotypeUndo.DeleteChromosome(chrom, g, index));
                        break;
                    }
                }
            }

            if (chrom == Display.FocussedChromosome)
                Display.FocussedChromosome = null;

            if (Chromosomes.Count() == 0)
                Display.ViewingMet = true;

            Notify("GotChromosomes", "Chromosomes", "ChromosomeCount", "ShowMainImage");
        }

        private bool GotNonstandardGroups()
        {
            return groups.Count > 25;
        }

        public void IncreasePenSize()
        {
            if(display.AddMode)
                display.AddModePenSize = display.AddModePenSize + 1;
            else
                display.DeleteModePenSize = display.DeleteModePenSize + 1;
        }

        public void DecreasePenSize()
        {
            if(display.AddMode)
                display.AddModePenSize = display.AddModePenSize - 1;
            else
                display.DeleteModePenSize = display.DeleteModePenSize - 1;
        }

        public void UpdateChromosomeDisplayImages(ImageRenderer metImage)
        {
            byte[] sourceBytes = metImage.CopyPixels();

            foreach (var chrom in Chromosomes)
            {
                chrom.UpdateDisplayImage(sourceBytes, metImage.ImageFrameX.ImageWidth, chrom.Bounds);
            }
        }

        public ChromosomeGroup FindGroupFromIdentifier(string identifier)
        {
            return groups.Where(c => c.Identifier == identifier).First();
        }

        private ChromosomeGroup FindUnclassifiedGroup()
        {
            return FindGroupFromIdentifier(ChromosomeGroup.UnclassifiedString);
        }

        public void AddMarker()
        {
            int n = groups.Count - 25;
            string name = "Marker";

            if (n > 0)
            {
                name += " " + (n + 1).ToString();
            }

            groups.Add(ChromosomeGroup.GroupForClass(name, cell.Id));
        }

        public void HandleStroke(Stroke stroke, ImageRenderer metImage, bool unMaskedAdd)
        {
            if (display.AddMode)
                CreateChromosomeFromStroke(stroke, metImage, unMaskedAdd);
            else if (display.DeleteMode)
                DeleteModeStroke(stroke, metImage);
            else
                EditChromosomeFromStroke(stroke, metImage);
        }

        public void MoveToChromInMet(KaryotypeChromosome chromToFind, StretchOrSizeCanvas metImageCanvas)
        {
            Display.FocussedChromosome = chromToFind;
            Display.ViewingMet = true;
            Notify("ShowMainImage");

            Int32Rect chromBounds = chromToFind.Bounds;
            metImageCanvas.ZoomAndPanTo(chromBounds.X + (chromBounds.Width / 2), chromBounds.Y + (chromBounds.Height / 2), chromToFind.Length * 4);

            if (highlight.Contains(chromToFind))
                highlight.Remove(chromToFind);

            highlight.Add(chromToFind);
        }

        private ObservableCollection<KaryotypeChromosome> highlight = new ObservableCollection<KaryotypeChromosome>();
        public IEnumerable<KaryotypeChromosome> HighlightChromosomes
        {
            get { return highlight; }
        }

        public void SetManualThreshold()
        {
            var renderer = MetImageRenderer;

            var thresh = (ushort)((1 - renderer.Threshold) * 255);
            renderer.Threshold = -1;

			Display.DetectMode = false;
            Notify("IsDisplayPopupEnabled");

			Channel source = controller.Renderer.ProcessedChannel;
            byte[] displayPixels = renderer.CopyPixels();

            masterMask = KaryotypeMask.CreateMask(renderer.ProcessedChannel, thresh);

			DetectFromMask(source, masterMask, displayPixels);
            Notify("Chromosomes", "ChromosomeCount");

            Display.AddMode = false;

            cell.KaryotypeThreshold = thresh;
        }

        public void CancelThresholdMode()
        {
            MetImageRenderer.Threshold = -1;
            Display.DetectMode = false;
            Notify("IsDisplayPopupEnabled");
            Display.AddMode = false;
        }

        public void EnterDetectMode()
        {
            MetImageRenderer.Threshold = 0.33;
            Display.DetectMode = true;
            Notify("IsDisplayPopupEnabled");
        }

        public override void OnDispayChanged()
        {
            UpdateChromosomeDisplayImages(MetImageRenderer);
        }

        public int ChromosomeCount
        {
            get
            {
                return Chromosomes.Count();
            }
        }

        public override bool ShowMainImage
        { 
            get 
            { 
                return Display.ViewingMet;
            } 
        }

        public void SwitchViews()
        {
            Display.ViewingMet = !Display.ViewingMet;
            Notify("ShowMainImage");

            KaryotypeCommands.ForceInputFocusToRightPlace.Execute(null, null);
        }

        public override BitmapSource PrintImage 
        { 
            get 
            {
                if(Display.ViewingMet)
                    return null;

                var img = Print.RenderElement(karyotypeCanvas, Brushes.White);
                return img;
            } 
        }

        public void NotifyGotChromosomes()
        {
            Notify("GotChromosomes");
        }
    }

    public static class KaryotypeThumbnailGenerator
    {
        private static BitmapSource GetThumbnail(IEnumerable<ChromosomeGroup> groups)
        {
            KaryotypePanel panel = new KaryotypePanel();
            panel.Background = new SolidColorBrush(Colors.White);
            foreach (ChromosomeGroup group in groups)
            {
                // Info: KaryotypePanel EXPECTS the Group property (that's an attached property)
                // to be set on the child of each of its Children... It's not the end of the world...

                Border b = new Border();

                Rectangle rect = new Rectangle();
                rect.Fill = Brushes.Black;
                rect.Height = 2;
                rect.Margin = new Thickness(0, 5, 0, 5);

                TextBlock tb = new TextBlock();
                tb.Text = group.Identifier;
                tb.FontSize = 14;
                tb.HorizontalAlignment = HorizontalAlignment.Center;

                b.Margin = new Thickness(15, 10, 15, 5);
                ChromosomeGroupControl groupControl = new ChromosomeGroupControl();
                groupControl.Group = group;

                DockPanel dp = new DockPanel();
                dp.Children.Add(tb);
                dp.Children.Add(rect);
                dp.Children.Add(groupControl);
                DockPanel.SetDock(tb, Dock.Bottom);
                DockPanel.SetDock(rect, Dock.Bottom);
                KaryotypePanel.SetGroup(dp, group.Identifier);

                b.Child = dp;
                panel.Children.Add(b);
            }

            panel.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            panel.Arrange(new Rect(panel.DesiredSize));

            RenderTargetBitmap bmp = new RenderTargetBitmap((int)panel.ActualWidth, (int)panel.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(panel);
            bmp.Freeze();
            return bmp;
        }

        public static void SaveKaryotypeThumbnail(Cell cell, IEnumerable<ChromosomeGroup> groups, string relative, IDataSource dataSource)
        {
            BitmapSource thumbnail = GetThumbnail(groups);

            cell.KaryotypeThumbnail = thumbnail;
            if (thumbnail != null)
            {
                Task.Factory.StartNew(state =>
                {
                    cell.KaryotypeThumbnailBlobId = ImageFileSerialize.WriteKaryThumbnail((BitmapSource)state, relative, cell.Id, dataSource);
                }, thumbnail);
            }
        }
    }
}
