﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Threading;
using System.Windows.Ink;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace AI.Brightfield
{
    public partial class MultiCutWindow
    {
        private MultiCutController controller;
        private List<StretchOrSizeCanvas> allCanvases;

        public MultiCutWindow(WorkflowController caseController, string chromosome)
        {
            allCanvases = new List<StretchOrSizeCanvas>();
            controller = new MultiCutController(caseController.WorkingCells.Where(c => c.IsNumbered), caseController.CaseId, Dispatcher, chromosome);
            DataContext = controller;
            InitializeComponent();

            WindowLocations.Handle(this);
        }

        private void OnMiddleMousePan(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == System.Windows.Input.MouseButton.Middle)
            {
                var c = VisualTreeWalker.FindParentOfType<StretchOrSizeCanvas>((InkCanvas)sender);
                c.HijackMouseDown(e);
            }
        }

        public IEnumerable<ChromosomeCutout> Cutouts { get { return controller.Cutouts; } }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void InkCanvas_StrokeCollected(object sender, InkCanvasStrokeCollectedEventArgs e)
        {
            var inkCanvas = (InkCanvas)sender;
            var cell = (SingleCutout)inkCanvas.Tag;

            while (inkCanvas.Strokes.Count > 1)
                inkCanvas.Strokes.RemoveAt(0);

            var stroke = e.Stroke;
            controller.Cut(cell, stroke);
        }

        private void CenterOnNumberTag(object sender, System.Windows.Data.DataTransferEventArgs e)
        {
            Image i = (Image)sender;
            SingleCutout c = (SingleCutout)i.Tag;
            StretchOrSizeCanvas canvas = VisualTreeWalker.FindParentOfType<StretchOrSizeCanvas>(i);
            canvas.ZoomAndPanTo(c.TagPosition.X, c.TagPosition.Y, 300);
        }

        private void OnPenSizeWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                e.Handled = true;
                double d = Math.Sign(e.Delta);
                controller.PenSize = Math.Max(10, Math.Min(100, controller.PenSize + (d * 5)));
            }
        }

        private void OnRmb(object sender, MouseButtonEventArgs e)
        {
            var ink = (InkCanvas)sender;
            var s = (SingleCutout)ink.Tag;

            ink.Strokes.Clear();
            s.Cutout = null;
        }

        
        private void OnCanvasLoaded(object sender, RoutedEventArgs e)
        {
            allCanvases.Add((StretchOrSizeCanvas)sender);
        }

        private void OnRecenterAll(object sender, MouseButtonEventArgs e)
        {
            foreach (var c in allCanvases)
            {
                var cut = (SingleCutout)c.Tag;
                c.ZoomAndPanTo(cut.TagPosition.X, cut.TagPosition.Y, 300);
            }
        }

        private void InkCanvas_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            e.Handled = true;
        }
    }


    public class ChromosomeCutout
    {
        public Cell Cell { get; internal set; }
        public KaryotypeChromosome Cutout { get; internal set; }
    }

    internal class SingleCutout : Notifier
    {
        public SingleCutout(Cell cell, Point position)
        {
            this.Cell = cell;
            this.TagPosition = position;
        }

        public Cell Cell { get; private set; }
        public BitmapSource Image { get; set; }
        public KaryotypeChromosome Cutout { get; internal set; }
        public Point TagPosition { get; set; }
    }

    internal class MultiCutCell
    {
        public IEnumerable<SingleCutout> Cutouts { get; private set; }

        public MultiCutCell(IEnumerable<SingleCutout> cutouts, BitmapSource img, Cell cell)
        {
            Cutouts = cutouts;

            foreach (var c in cutouts)
                c.Image = img;

            this.Cell = cell;
        }

        public Cell Cell { get; private set; }
    }


    internal class MultiCutController : Notifier
    {
        private ObservableCollection<MultiCutCell> cutCells;
        private double boxSize;
        private double penSize;
        private bool isInkEnabled;

        public MultiCutController(IEnumerable<Cell> cells, Guid caseId, Dispatcher uiThread, string chromosome)
        {
            boxSize = 300;
            penSize = 32;
            isInkEnabled = true;

            cutCells = new ObservableCollection<MultiCutCell>();
            ChromosomeTag = chromosome;

            Task.Factory.StartNew(delegate(object state)
            {
                var db = new Connect.Database();

                foreach (var c in cells)
                {
                    if (c.ImageID == Guid.Empty)
                        continue;

                    var tags = from t in db.NumberingTags
                               where t.cellId == c.Id && t.tag == chromosome
                               select new SingleCutout(c, new Point(t.x, t.y));

                    if(tags.Count() == 0)
                        continue;

                    string path = c.ImageID.ToString() + ".jpg";
                    var bitmap = ImageFromBitmapSource.LoadBitmapImageFromStream(caseId, path, db, Connect.CacheOption.DontCache, 0);
                    var cutCell = new MultiCutCell(tags.ToArray(), bitmap, c);

                    uiThread.BeginInvoke((WaitCallback)delegate(object s)
                    {
                        cutCells.Add((MultiCutCell)s);

                    }, DispatcherPriority.Normal, new object[] { cutCell });
                }
            }, (object)null);
        }

        public string ChromosomeTag { get; private set; }
        public IEnumerable<MultiCutCell> Cells { get { return cutCells; } }
        
        public double BoxSize
        {
            get { return boxSize; }
            set { boxSize = value; Notify("BoxSize"); }
        }
        
        public double PenSize
        {
            get { return penSize; }
            set { penSize = value; Notify("PenSize", "InkCanvasDrawingAttributes"); }
        }
        
        public bool IsInkEnabled
        {
            get { return isInkEnabled; }
            set { isInkEnabled = value; Notify("IsInkEnabled"); }
        }
        
        public IEnumerable<ChromosomeCutout> Cutouts
        {
            get 
            {
                foreach (var c in cutCells)
                {
                    foreach (var cc in c.Cutouts)
                    {
                        if (cc.Cutout != null)
                        {
                            yield return new ChromosomeCutout
                            {
                                Cell = cc.Cell,
                                Cutout = cc.Cutout
                            };
                        }
                    }
                }
            }
        }

        public DrawingAttributes InkCanvasDrawingAttributes
        {
            get
            {
                return new DrawingAttributes
                {
                    Color = Color.FromArgb(100, 0, 200, 30),
                    FitToCurve = true,
                    Height = penSize,
                    IgnorePressure = false,
                    Width = penSize
                };
            }
        }

        public void Cut(SingleCutout cell, Stroke stroke)
        {
            byte[] pixels = new byte[cell.Image.PixelWidth * cell.Image.PixelHeight * 4];
            cell.Image.CopyPixels(pixels, cell.Image.PixelWidth * 4, 0);
            var k = ChromosomeCutter.CutStroke(pixels, cell.Image.PixelWidth, cell.Image.PixelHeight, stroke, cell.Cell.Id);
            cell.Cutout = k;
        }
    }
}
