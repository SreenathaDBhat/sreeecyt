﻿using System;
using System.Linq;
using System.Windows.Input;
using System.Collections.Generic;

namespace AI.Brightfield
{
    public partial class NewSlideWindow
    {
        public static readonly RoutedUICommand CreateSlide = new RoutedUICommand();

        public NewSlideWindow(string initialName, StringTable strings, IEnumerable<Slide> existingSlides)
        {
            var c = new NewSlideWindowController(initialName, strings, existingSlides);
            DataContext = c;
            InitializeComponent();

            Loaded += (s, e) =>
                {
                    Keyboard.Focus(slideName);
                    slideName.SelectAll();
                };
        }

        public string NewSlideName
        {
            get { return ((NewSlideWindowController)DataContext).Name; }
        }

        private void OnDragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void CanCreateSlideExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ((NewSlideWindowController)DataContext).IsValid;
        }

        private void CreateSlideExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Connect.ConnectionTest.Test(this);
            DialogResult = true;
        }
    }

    internal class NewSlideWindowController : Notifier
    {
        private static readonly string NewSlideFail_AlreadyExists = "NewSlideFail_AlreadyExists";
        private static readonly string NewSlideFail_EmptyName = "NewSlideFail_EmptyName";
        private static readonly string NewSlideFail_NameTooLong = "NewSlideFail_NameTooLong";

        public bool IsValid { get; private set; }
        public string FailureReason { get; private set; }
        private IEnumerable<Slide> existingSlides;
        private StringTable strings;

        public NewSlideWindowController(string initialName, StringTable strings, IEnumerable<Slide> existingSlides)
        {
            name = initialName;
            this.strings = strings;
            this.existingSlides = existingSlides;
            Validate();
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; Notify("Name"); Validate(); }
        }

        private void Validate()
        {
            var collision = existingSlides.Where(s => string.Compare(s.Name, name, StringComparison.InvariantCultureIgnoreCase) == 0).FirstOrDefault();
            if (name.Trim().Length == 0)
            {
                FailureReason = strings.Lookup(NewSlideFail_EmptyName);
                IsValid = false;
            }
            else if (name.Length >= 128)
            {
                FailureReason = strings.Lookup(NewSlideFail_NameTooLong);
                IsValid = false;
            }
            else if (collision != null)
            {
                FailureReason = strings.Lookup(NewSlideFail_AlreadyExists);
                IsValid = false;
            }
            else
            {
                FailureReason = string.Empty;
                IsValid = true;
            }

            Notify("IsValid", "FailureReason");
        }
    }
}
