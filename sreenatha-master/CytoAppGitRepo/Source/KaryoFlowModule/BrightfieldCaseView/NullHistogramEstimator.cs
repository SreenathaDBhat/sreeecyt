﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.Brightfield
{
	/// <summary>
	/// When asked to perform a histogram estimate it does nothing, ie there is no change made to the existing histogram.
	/// </summary>
	public class NullHistogramEstimator : IHistogramEstimator
	{
		public NullHistogramEstimator()
		{
		}

		public override void Estimate(ImageFrame imageFrame, ChannelRenderParameters channelParams, IEnumerable<RenderPass> passes)
		{
		}
	}
}
