﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Data;


namespace AI.Brightfield
{
    public partial class NumberImageUi
    {
        private NumberController numberer;

        public NumberImageUi(NumberController numberer)
        {
            this.numberer = numberer;
            InitializeComponent();
        }

        private void OnMouseEnterTag(object sender, MouseEventArgs e)
        {
            NumberingTag tag = (NumberingTag)((FrameworkElement)sender).Tag;
            numberer.HighlightGroup = tag.Tag;
        }

        private void OnMouseLeaveTag(object sender, MouseEventArgs e)
        {
            numberer.HighlightGroup = string.Empty;
        }

        private void OnRightClick(object sender, MouseButtonEventArgs e)
        {
            if (Keyboard.FocusedElement is TextBox)
                return;

            var mousePos = e.GetPosition(this);
            numberer.ShowContextMenu(mousePos);
            e.Handled = true;
        }

        private void OnRightClickDeleteTag(object sender, MouseButtonEventArgs e)
        {
            var mousePos = e.GetPosition(this);
            numberer.MouseDelete(mousePos);
            e.Handled = true;
        }

        private void OnLeftClick(object sender, MouseButtonEventArgs e)
        {
            // if we're in the middle of editing another tag, make sure we dont drop a tag when
            // were just wanting to complete the edit by clicking someplace else.
            if (Keyboard.FocusedElement is TextBox)
            {
                numberer.RevertFocus();
            }
            else
            {
                numberer.MouseDrop(e.GetPosition(this));
            }
        }

        private void CanAddFromContextMenu(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void OnDeleteTextItem(object sender, RoutedEventArgs e)
        {
            NumberingTag tagToDelete = (NumberingTag)((FrameworkElement)sender).Tag;
            numberer.DeleteTag(tagToDelete);
        }
    }

    public class EditTagVisibilityConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isMouseOver = (bool)values[0];
            bool isFocused = (values[1] == DependencyProperty.UnsetValue ? false : (bool)values[1]);

            return (isMouseOver || isFocused) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class NumberTagVisibilityDecider : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string highlightGroup = (values[1] == DependencyProperty.UnsetValue ? string.Empty : (string)values[1]);
            bool showAll = (values[2] == DependencyProperty.UnsetValue ? false : (bool)values[2]);
            bool mouseOver = (bool)values[3];
            bool focused = (values[4] == DependencyProperty.UnsetValue ? false : (bool)values[4]);
            NumberingTagBucket currentBucket = (values[5] == DependencyProperty.UnsetValue ? null : (NumberingTagBucket)values[5]);
            NumberingTag me = (values[6] == DependencyProperty.UnsetValue ? null : (NumberingTag)values[6]);

            if (currentBucket == null || me == null)
                return 0.0;

            bool v = false;

            if (showAll || mouseOver || focused || me.Tag == currentBucket.Tag || me.Tag == highlightGroup)
                v = true;

            return v ? 1.0 : 0.0;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


}
