﻿using System;
using System.Windows;

namespace AI.Brightfield
{
    public partial class NumberScreenUi
    {
        private NumberController tool;

        public NumberScreenUi(NumberController tool)
        {
            this.tool = tool;
            InitializeComponent();
        }

        private void OnDeleteSet(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            NumberingTagBucket bucket = (NumberingTagBucket)((FrameworkElement)sender).Tag;
            tool.DeleteAllOfType(bucket.Tag);
        }

        private void CanAddFromContextMenu(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void AddFromContextMenu(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            tool.ApplyContextMenuSelection((string)e.Parameter);
        }

        private void OnAddMarker(object sender, RoutedEventArgs e)
        {
            tool.AddMarker();
        }
    }
}
