﻿using System;
using System.Windows.Input;
using System.Windows;
using System.Linq;

namespace AI.Brightfield
{
    public partial class NumberToolbarUi
    {
        private NumberController numberer;

        public NumberToolbarUi(NumberController numberer)
        {
            this.numberer = numberer;
            InitializeComponent();
        }

        private void CanClearExecute(object sender, CanExecuteRoutedEventArgs e)
        {
        // Stops number pad input after clear press. No Idea why?           // e.CanExecute = numberer.Tags.Count() > 0;
            e.CanExecute = true;
        }

        private void ClearExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            numberer.ClearTags();
        }
    }
}
