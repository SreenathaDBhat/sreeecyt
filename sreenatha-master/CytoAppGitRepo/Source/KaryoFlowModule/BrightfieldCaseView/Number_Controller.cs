﻿using System;
using System.Linq;
using System.Windows;
using System.Collections.ObjectModel;
using AI.Brightfield.ManualCountImplementation;
using System.Collections.Generic;
using System.Windows.Input;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using System.Xml.Linq;

namespace AI.Brightfield
{
    #region Implementation Detail

    public class NumberingTag : Notifier
    {
        public NumberingTag(string tag)
        {
            this.Tag = tag;
            displayTag = tag;
        }

        private string tag;
        public string Tag
        {
            get { return tag; }
            private set { tag = value; Notify("Tag"); DisplayTag = value; }
        }

        private string displayTag;
        public string DisplayTag
        {
            get { return displayTag; }
            set { displayTag = value; Notify("DisplayTag"); }
        }

        public double X { get; set; }
        public double Y { get; set; }

        public bool IsSex
        {
            get { return IsSexString(Tag); }
        }

        public static bool IsSexString(string s)
        {
            return string.Compare(s, "X", StringComparison.CurrentCultureIgnoreCase) == 0 || string.Compare(s, "Y", StringComparison.CurrentCultureIgnoreCase) == 0;
        }

        public void ChangeTag(string originalTagString)
        {
            this.Tag = originalTagString;
        }

        public void Augment(string keyString)
        {
            Tag = Tag + keyString;
        }
    }

    public class NumberingTagBucket : Notifier
    {
        private string tag;
        public string Tag
        {
            get { return tag; }
            set { tag = value; Notify("Tag"); }
        }

        private int count;
        public int Count
        {
            get { return count; }
            set { count = value; Notify("Count"); }
        }
    }

    public class TagDeletedUndoItem : IUndoItem
    {
        private List<NumberingTag> deleted;
        private IList<NumberingTag> list;

        public TagDeletedUndoItem(IEnumerable<NumberingTag> deletedTags, IList<NumberingTag> list)
        {
            this.list = list;
            this.deleted = new List<NumberingTag>(deletedTags);
        }

        public void Undo(object state)
        {
            foreach (var tag in deleted)
                list.Add(tag);
        }
    }

    public class TagAugmentedUndoItem : IUndoItem
    {
        private NumberingTag tag;
        private string originalTagString;

        public TagAugmentedUndoItem(NumberingTag tag, string originalTagString)
        {
            this.tag = tag;
            this.originalTagString = originalTagString;
        }

        public void Undo(object state)
        {
            tag.ChangeTag(originalTagString);
        }
    }

    public class TagAddedUndoItem : IUndoItem
    {
        private NumberingTag tag;
        private IList<NumberingTag> list;

        public TagAddedUndoItem(NumberingTag tag, IList<NumberingTag> list)
        {
            this.list = list;
            this.tag = tag;
        }

        public void Undo(object state)
        {
            list.Remove(tag);
        }
    }

    public class TagsClearedUndoitem : IUndoItem
    {
        private IEnumerable<NumberingTag> storedTags;
        private IList<NumberingTag> list;

        public TagsClearedUndoitem(IList<NumberingTag> tags)
        {
            storedTags = tags.ToList();
            list = tags;
        }

        public void Undo(object state)
        {
            list.Clear();
            foreach (var st in storedTags)
                list.Add(st);
        }
    }

    public class TagToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = ((string)value).ToUpper();
            if (s == ChromosomeGroup.SexStringX)
            {
                return KaryotypeChromosomeColoring.Discreet(22);
            }
            else if (s == ChromosomeGroup.SexStringY)
            {
                return KaryotypeChromosomeColoring.Discreet(23);
            }
            else if (s == NumberController.MarkerGroupString.ToUpper())
            {
                return Brushes.Orange;
            }
            else
            {
                int i;
                if (int.TryParse(s, out i))
                {
                    return KaryotypeChromosomeColoring.Discreet(i - 1);
                }
                else
                {
                    return Brushes.Black;
                }
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    #endregion

    public static class NumberCommands
    {
        public static RoutedUICommand Clear = new RoutedUICommand("Clear", "Clear", typeof(CountCommands));
    }

    public sealed class NumberController : ToolNotifier
    {
        private static readonly string TagRenderScaleKey = "Number_TagRenderScale";
        private static readonly string ShowTagBarKey = "Number_ShowTagBar";
        private static readonly string ShowAllTagsKey = "Number_ShowAllTags";
        
        public static readonly string MarkerGroupString = "Marker";

        private Cell cell;
        private UndoList undoList;
        private WorkflowController controller;

        private ObservableCollection<NumberingTag> tags;
        private List<NumberingTagBucket> buckets;
        private NumberingTagBucket currentBukkit;
        private DateTime lastKeyboardDropTime;
        private bool showAllTags, showTagBar;
        private double tagRenderScale;
        private UIElement focusDefault;

        public NumberController(UIElement focusDefault)
        {
            undoList = new UndoList();
            this.focusDefault = focusDefault;
        }

        public override void Load(Cell cell, WorkflowController controller)
        {
            this.controller = controller;
            this.cell = cell;

            tagRenderScale = controller.Persistants.GetDouble(TagRenderScaleKey, 2.2);
            showAllTags = controller.Persistants.GetBool(ShowAllTagsKey, false);
            showTagBar = controller.Persistants.GetBool(ShowTagBarKey, false);
            Notify("TagRenderScale", "ShowTagBar", "ShowAllTags");

            CreateHumanBuckets();
            UpdateBucketCounts();
        }

        public override UndoList UndoList
        {
            get { return undoList; }
        }

        public IEnumerable<NumberingTag> Tags
        {
            get { return tags; }
        }

        public bool ShowTagBar
        {
            get { return showTagBar; }
            set { showTagBar = value; Notify("ShowTagBar"); controller.Persistants.Set(ShowTagBarKey, value); }
        }

        public double TagRenderScale
        {
            get { return tagRenderScale; }
            set { tagRenderScale = value; Notify("TagRenderScale"); controller.Persistants.Set(TagRenderScaleKey, value); }
        }

		public bool GotTags
		{
			get { return tags.FirstOrDefault() != null; }
		}

        public override void Save()
        {
            var xml = new XElement("Numbering",
                from pt in tags
                select new XElement("Tag",
                               new XAttribute("txt", pt.Tag),
                               new XAttribute("displayTxt", pt.DisplayTag),
                               new XAttribute("x", pt.X),
                               new XAttribute("y", pt.Y)));

            var dataSource = DataSourceFactory.GetDataSource();
            dataSource.SaveXml(controller.RelativeIdCell, xml, "cell.numbering");

			//dbCell.analysed = GotTags;

            //using (var lims = Connect.LIMS.New())
            //{
            //    lims.MarkCaseAsModified(controller.Case, DateTime.Now, "Chromosome Analysis");
            //}

			controller.SetNumbered(cell, GotTags);
            undoList.Clear();
        }

        public override FrameworkElement ImageUi
        {
            get { return new NumberImageUi(this); }
        }

        public override StretchSizeMode ImageUiSizeMode
        {
            get { return StretchSizeMode.Size; }
        }

        public override FrameworkElement ScreenUi
        {
            get { return new NumberScreenUi(this); }
        }

        public override FrameworkElement ToolbarUi
        {
            get { return new NumberToolbarUi(this); }
        }

        private void CreateHumanBuckets()
        {
            buckets = new List<NumberingTagBucket>();
            for (int i = 1; i <= 22; ++i)
            {
                buckets.Add(new NumberingTagBucket { Tag = i.ToString(CultureInfo.InvariantCulture) });
            }

            buckets.Add(new NumberingTagBucket { Tag = "X" });
            buckets.Add(new NumberingTagBucket { Tag = "Y" });

            buckets.Add(new NumberingTagBucket { Tag = MarkerGroupString });

            currentBukkit = buckets[0];

            var dataSource = DataSourceFactory.GetDataSource();
            var numberXml = dataSource.LoadXml(controller.RelativeIdCell, "cell.numbering");

            if (numberXml != null)
            {
                tags = new ObservableCollection<NumberingTag>(from t in numberXml.Descendants("Tag")
                                                                   select new NumberingTag(t.Attribute("txt").Value)
                                                                   {
                                                                       DisplayTag = t.Attribute("displayTxt").Value,
                                                                       X = double.Parse(t.Attribute("x").Value),
                                                                       Y = double.Parse(t.Attribute("y").Value)
                                                                   });
                UpdateBucketCounts();
            }
            else
                tags = new ObservableCollection<NumberingTag>();

            tags.CollectionChanged += (s, e) => UpdateBucketCounts();
        }

        private void UpdateBucketCounts()
        {
            foreach (var b in buckets)
            {
                b.Count = tags.Count(t => string.Compare(t.Tag, b.Tag) == 0);
            }

            Notify("TagCount");
        }

        public override void OnKeyDown(ToolKeyEventArgs e)
        {
            
        }

        public override void OnKeyUp(ToolKeyEventArgs e)
        {
            if (!IsKeyWeWant(e.Key))
                return;

            string keyString = KeyString(e.Key);
            var time = DateTime.Now;

            NumberingTag workingTag = null;
            if (tags.Count > 0 && !IsSexKey(e.Key) && time.Subtract(lastKeyboardDropTime).TotalMilliseconds < 700)
            {
                var lastTag = tags.Last();
                string join = lastTag.Tag + keyString;

                int n = 0;
                bool isNumber = int.TryParse(join, out n);

                if (!lastTag.IsSex && isNumber && n < 23 && CloseEnough(lastTag, e.MousePositionRelativeToImage))
                    workingTag = lastTag;
            }

            lastKeyboardDropTime = time;

            if (workingTag == null)
            {
                if (keyString != "0")
                {
                    workingTag = new NumberingTag(keyString)
                    {
                        X = e.MousePositionRelativeToImage.X,
                        Y = e.MousePositionRelativeToImage.Y
                    };

                    tags.Add(workingTag);
                    undoList.Add(new TagAddedUndoItem(workingTag, tags));
                }
                else
                {
                    return;
                }
            }
            else
            {
                undoList.Add(new TagAugmentedUndoItem(workingTag, workingTag.Tag));
                workingTag.Augment(keyString);
                UpdateBucketCounts();
            }

            e.Handled = true;
            return;
        }

        public void MouseDrop(Point point)
        {
            NumberingTag tag = new NumberingTag(currentBukkit.Tag)
            {
                X = point.X,
                Y = point.Y
            };

            tags.Add(tag);
            undoList.Add(new TagAddedUndoItem(tag, tags));

            if (currentBukkit.Count == 2)
                MoveToNextBucket();
        }

        public bool MouseDelete(Point point)
        {
            for (int i = tags.Count - 1; i >= 0; --i)
            {
                if (CloseEnough(tags[i], point))
                {
                    DeleteTags(new NumberingTag[] { tags[i] });
                    return true;
                }
            }

            HighlightGroup = string.Empty;
            return false;
        }

        private static bool CloseEnough(NumberingTag tag, Point pt)
        {
            return Math.Sqrt(Math.Pow(tag.X - pt.X, 2) + Math.Pow(tag.Y - pt.Y, 2)) < 10;
        }

        private static string KeyString(Key key)
        {
            if (key == Key.X || key == Key.Add)
                return "X";
            else if (key == Key.Y || key == Key.Subtract)
                return "Y";
            else
                return (key - Key.NumPad0).ToString(CultureInfo.InvariantCulture);
        }

        private static bool IsSexKey(Key key)
        {
            return NumberingTag.IsSexString(key.ToString());
        }

        private static bool IsKeyWeWant(Key key)
        {
            return (key >= Key.NumPad0 && key <= Key.NumPad9) || key == Key.X || key == Key.Y || key == Key.Add || key == Key.Subtract;
        }

        private void MoveToNextBucket()
        {
            int index = buckets.IndexOf(currentBukkit);

            if (index + 1 < buckets.Count)
            {
                var nextBukitt = buckets[index + 1];
                if (nextBukitt.Count == 0)
                    CurrentBucket = nextBukitt;
            }
        }

        public void ClearTags()
        {
            undoList.Add(new TagsClearedUndoitem(tags));

            tags.Clear();

            foreach (var b in buckets)
                b.Count = 0;

            CurrentBucket = buckets.First();
        }

        public bool ShowAllTags
        {
            get { return showAllTags; }
            set
            {
                showAllTags = value;
                Notify("ShowAllTags");
                controller.Persistants.Set(ShowAllTagsKey, value);
            }
        }

        public void DeleteAllOfType(string tag)
        {
            DeleteTags(tags.Where(t => t.Tag == tag));
        }

        private void DeleteTags(IEnumerable<NumberingTag> tagsToDelete)
        {
            undoList.Add(new TagDeletedUndoItem(tagsToDelete, tags));
            for (int i = tags.Count - 1; i >= 0; --i)
            {
                var tag = tags[i];
                if (tagsToDelete.Contains(tag))
                {
                    tags.RemoveAt(i);
                }
            }

            HighlightGroup = string.Empty;
        }

        public IEnumerable<NumberingTagBucket> Buckets
        {
            get { return buckets; }
        }

        public NumberingTagBucket CurrentBucket
        {
            get { return currentBukkit; }
            set { currentBukkit = value; Notify("CurrentBucket"); }
        }


        private Point contextMenuMousePos;
        public void ShowContextMenu(Point point)
        {
            contextMenuMousePos = point;
            IsContextMenuAdderOpen = true;
        }

        private bool isContextMenuAdderOpen;
        public bool IsContextMenuAdderOpen
        {
            get { return isContextMenuAdderOpen; }
            set { isContextMenuAdderOpen = value; Notify("IsContextMenuAdderOpen"); }
        }

        public void ApplyContextMenuSelection(string groupIdentifier)
        {
            IsContextMenuAdderOpen = false;

            var tag = new NumberingTag(groupIdentifier)
            {
                X = contextMenuMousePos.X,
                Y = contextMenuMousePos.Y
            };

            tags.Add(tag);
            undoList.Add(new TagAddedUndoItem(tag, tags));

            CurrentBucket = buckets.Where(b => b.Tag == tag.Tag).First();
        }

        public void RevertFocus()
        {
            focusDefault.Focus();
            Keyboard.Focus(focusDefault);
        }

        public void AddMarker()
        {
            ApplyContextMenuSelection(MarkerGroupString);
        }

        public int TagCount
        {
            get { return tags.Count; }
        }

        internal void DeleteTag(NumberingTag tagToDelete)
        {
            DeleteTags(new NumberingTag[] { tagToDelete });
        }

        private string highlightedGroup;
        public string HighlightGroup
        {
            get { return highlightedGroup; }
            set { highlightedGroup = value; Notify("HighlightGroup"); }
        }
    }
}
