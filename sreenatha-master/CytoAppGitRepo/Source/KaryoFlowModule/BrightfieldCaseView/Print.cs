﻿using System;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows;
using System.Globalization;
using System.Printing;

namespace AI.Brightfield
{
    public static class Print
    {
        public static BitmapSource RenderElementWithImage(BitmapSource image, FrameworkElement printVisual, double w, double h)
        {
            RenderTargetBitmap render = new RenderTargetBitmap(
                   (int)(w),
                   (int)(h),
                   96, 96, PixelFormats.Pbgra32);

            if (image != null)
            {
                Image img = new Image();
                img.Source = image;
                img.Width = w;
                img.Height = h;
                img.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                img.Arrange(new Rect(new Point(0, 0), img.DesiredSize));
                render.Render(img);
            }

            if (printVisual != null)
                render.Render(printVisual);

            return new CroppedBitmap(render, new Int32Rect(0, 0, render.PixelWidth, render.PixelHeight));
        }

        public static BitmapSource RenderElement(FrameworkElement printVisual, Brush background)
        {
            RenderTargetBitmap render = new RenderTargetBitmap(
                   (int)(printVisual.ActualWidth),
                   (int)(printVisual.ActualHeight),
                   96, 96, PixelFormats.Pbgra32);

            if (background != null)
            {
                Border bg = new Border();
                bg.Background = background;
                bg.Width = printVisual.ActualWidth;
                bg.Height = printVisual.ActualHeight;
                bg.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                bg.Arrange(new Rect(new Point(0, 0), bg.DesiredSize));
                render.Render(bg);
            }

            render.Render(printVisual);
            return new CroppedBitmap(render, new Int32Rect(0, 0, render.PixelWidth, render.PixelHeight));
        }

        public static BitmapSource RenderElement(FrameworkElement printVisual)
        {
            return RenderElement(printVisual, null);
        }

        public static void PrintVisual(BitmapSource printImage, string caseName, string cellName, string slideName, string dateString)
        {
            PrintVisual(printImage, caseName, cellName, slideName, dateString, string.Empty);
        }

        public static void PrintVisual(BitmapSource printImage, string caseName, string cellName, string slideName, string dateString, string result)
        {
            try
            {
                double aspect = printImage.PixelHeight / printImage.PixelWidth;

                PrintDialog dlg = new PrintDialog();
				dlg.PrintTicket.PageOrientation = PageOrientation.Landscape;

                if ((bool)dlg.ShowDialog().GetValueOrDefault())
                {
                    Image img = new Image();
                    img.Source = printImage;

                    TextBlock caseText = new TextBlock();
                    caseText.FontSize = 18;
                    caseText.FontWeight = FontWeights.Bold;
                    caseText.Text = caseName;
                    caseText.VerticalAlignment = VerticalAlignment.Center;

                    TextBlock resultText = new TextBlock();
                    resultText.FontSize = 18;
                    resultText.FontWeight = FontWeights.Bold;
                    resultText.Text = result;
                    resultText.VerticalAlignment = VerticalAlignment.Center;

                    DockPanel caseNameResult = new DockPanel();
                    DockPanel.SetDock(caseText, Dock.Top);
                    caseNameResult.Children.Add(caseText);
                    Viewbox vb = new Viewbox();
                    vb.StretchDirection = StretchDirection.DownOnly;
                    vb.Child = resultText;
                    caseNameResult.Children.Add(vb);

                    TextBlock cellText = new TextBlock();
                    cellText.FontSize = 18;
                    cellText.FontWeight = FontWeights.Bold;
                    cellText.TextAlignment = TextAlignment.Right;
                    cellText.Text = cellName;

                    TextBlock slideText = new TextBlock();
                    slideText.FontSize = 16;
                    slideText.FontWeight = FontWeights.Bold;
                    slideText.TextAlignment = TextAlignment.Right;
                    slideText.Text = slideName;

                    StackPanel cellSlide = new StackPanel();
                    DockPanel.SetDock(cellSlide, Dock.Right);
                    cellSlide.Children.Add(cellText);
                    cellSlide.Children.Add(slideText);

                    TextBlock dateText = new TextBlock();
                    dateText.FontSize = 12;
                    dateText.FontWeight = FontWeights.Bold;
                    dateText.Text = dateString;
                    dateText.HorizontalAlignment = HorizontalAlignment.Center;
                    DockPanel.SetDock(dateText, Dock.Bottom);

                    DockPanel topPanel = new DockPanel();
                    topPanel.Children.Add(cellSlide);
                    topPanel.Children.Add(caseNameResult);
                    DockPanel.SetDock(topPanel, Dock.Top);

                    DockPanel dock = new DockPanel();
                    dock.Children.Add(topPanel);
                    dock.Children.Add(dateText);
                    dock.Children.Add(img);
                    dock.Margin = new Thickness(30);
                    dock.Width = dlg.PrintableAreaWidth * 0.9;
                    dock.Height = dlg.PrintableAreaHeight * 0.9;

                    dock.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                    dock.Arrange(new Rect(new Point(0, 0), dock.DesiredSize));

                    dlg.PrintVisual(dock, "Print");
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
