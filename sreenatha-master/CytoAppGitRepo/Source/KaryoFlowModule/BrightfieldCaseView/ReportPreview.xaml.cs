using System;
using System.Windows;
using System.Windows.Documents;
using Microsoft.Win32;
using System.Windows.Media;
using System.IO;
using System.Windows.Controls;
using System.Globalization;
using System.Printing;
using System.Windows.Xps;
using System.Collections.Generic;

namespace AI.Brightfield
{
    public partial class PrintPreview : Window
    {
        public static void Print(WorkflowController controller, List<Cell> filteredCells)
        {
            // put this into a static method for readabilities sake - having implicit functionality performed in a ctor is crap.
            PrintPreview pp = new PrintPreview(controller, filteredCells, false);
        }

        public PrintPreview(WorkflowController controller, List<Cell> filteredCells, Boolean preview)
        {
            InitializeComponent();
            this.DataContext = controller;

            WindowLocations.Handle(this);

            if (!preview)
            {
                this.Hide();
                docReader.Visibility = Visibility.Hidden;
            }

            AddCaseDetails(controller);
            AddCellData(filteredCells);

            if (!preview)
            {
                doc = (FlowDocument)docReader.Document;
                PrintIt();
            }
        }

        private void AddCellData(List<Cell> filteredCells)
        {
            foreach (var cell in filteredCells)
            {
                TableRow row = new TableRow();
                row.Background = Brushes.LightGray;

                //  Cell label
                TableCell cpCell = new TableCell();
                Paragraph cpPara = new Paragraph();
                cpPara.Inlines.Add("Cell:");
                cpCell.Blocks.Add(cpPara);
                row.Cells.Add(cpCell);

                //  Cell name
                TableCell cCell = new TableCell();
                cCell.TextAlignment = TextAlignment.Left;
                Paragraph cPara = new Paragraph();
                cPara.Inlines.Add(cell.Name);
                cCell.Blocks.Add(cPara);
                row.Cells.Add(cCell);

                //  Blank cell
                TableCell bCell = new TableCell();
                row.Cells.Add(bCell);

                // Slide label
                TableCell spCell = new TableCell();
                Paragraph spPara = new Paragraph();
                spPara.Inlines.Add("Slide:");
                spCell.Blocks.Add(spPara);
                row.Cells.Add(spCell);

                // Slide name
                TableCell sCell = new TableCell();
                sCell.TextAlignment = TextAlignment.Left;
                Paragraph sPara = new Paragraph();
                sPara.Inlines.Add(cell.SlideName);
                sCell.Blocks.Add(sPara);
                row.Cells.Add(sCell);

                //  Add slide/cell row to grid
                cellDetailsGroup.Rows.Add(row);

                AddProcessedRow("Counted", cell.IsCounted ? "Yes" : "No");
                AddProcessedRow("Numbered", cell.IsNumbered ? "Yes" : "No");
                AddProcessedRow("Karyotyped", cell.IsKaryotyped ? "Yes" : "No");

                AddBlankRowToCellDetailsGroup();

                foreach (var metaPair in cell.MetaData.Fields)
                {
                    TableRow metarow = new TableRow();
                    //  Blank cell
                    TableCell blnkCell = new TableCell();
                    metarow.Cells.Add(blnkCell);

                    //  Field label
                    TableCell mfCell = new TableCell();
                    Paragraph mfPara = new Paragraph();
                    mfPara.Inlines.Add(metaPair.Field);
                    mfCell.Blocks.Add(mfPara);
                    metarow.Cells.Add(mfCell);

                    //  Field value
                    TableCell mvCell = new TableCell();
                    Paragraph mvPara = new Paragraph();
                    mvPara.Inlines.Add(metaPair.Value);
                    mvCell.Blocks.Add(mvPara);
                    metarow.Cells.Add(mvCell);

                    //  Add meta field row to grid
                    cellDetailsGroup.Rows.Add(metarow);
                }

                AddBlankRowToCellDetailsGroup();
            }
        }

        private void AddBlankRowToCellDetailsGroup()
        {
            TableRow blankkrow = new TableRow();
            TableCell blnkrowCell = new TableCell();
            blankkrow.Cells.Add(blnkrowCell);
            cellDetailsGroup.Rows.Add(blankkrow);
        }

        private void AddProcessedRow(string processName, string processValue)
        {
            TableRow processRow = new TableRow();
            
            TableCell blankCell = new TableCell();
            processRow.Cells.Add(blankCell);

            TableCell processLabel = new TableCell();
            Paragraph mfPara = new Paragraph();
            mfPara.Inlines.Add(processName);
            processLabel.Blocks.Add(mfPara);
            processRow.Cells.Add(processLabel);

            //  Field value
            TableCell processValueCell = new TableCell();
            Paragraph mvPara = new Paragraph();
            mvPara.Inlines.Add(processValue);
            processValueCell.Blocks.Add(mvPara);
            processRow.Cells.Add(processValueCell);

            //  Add meta field row to grid
            cellDetailsGroup.Rows.Add(processRow);
        }

        private void AddCaseDetails(WorkflowController controller)
        {
            foreach (var field in controller.MetaData.Fields)
            {
                TableRow row = new TableRow();

                TableCell fieldCell = new TableCell();
                Paragraph fieldPara = new Paragraph();
                fieldPara.Inlines.Add(field.Field);

                fieldCell.Blocks.Add(fieldPara);
                row.Cells.Add(fieldCell);

                TableCell blankCell = new TableCell();
                row.Cells.Add(blankCell);

                TableCell valueCell = new TableCell();
                Paragraph valuePara = new Paragraph();
                valuePara.Inlines.Add(field.Value);

                valueCell.Blocks.Add(valuePara);
                row.Cells.Add(valueCell);

                caseDetailsGroup.Rows.Add(row);
            }
        }
        
        void OnPrint(object sender, EventArgs e)
        {
            docReader.Print();
        }

        private XpsDocumentWriter GetPrintXpsDocumentWriter(PrintQueue pq)
        {
            XpsDocumentWriter xpsdw = PrintQueue.CreateXpsDocumentWriter(pq);
            return xpsdw;
        }

        void OnDone(object sender, EventArgs e)
        {
            Close();
        }

        private void PrintIt()
        {
            PrintDialog printDialog = null;
            PrintDialog dlg = new PrintDialog();
            if (dlg.ShowDialog() == true)
            {
                printDialog = dlg;
            }
            else
            {
                Close();
                return;
            }

            PrintQueue printQueue = printDialog.PrintQueue;
            XpsDocumentWriter xdwPrint = GetPrintXpsDocumentWriter(printQueue);
            PrintTicket pt = printQueue.UserPrintTicket;

            doc.ColumnWidth = (double)(0.8 * pt.PageMediaSize.Width);

            DocumentPaginator paginator = ((IDocumentPaginatorSource)doc).DocumentPaginator;

            paginator.ComputePageCount();
            int totalpages = paginator.PageCount;

            paginator = new DocumentPaginatorWrapper(paginator, new Size((double)pt.PageMediaSize.Width, (double)pt.PageMediaSize.Height),
                        new Size(48, 48), (double)(0.35 * pt.PageMediaSize.Width), (double)(0.9 * pt.PageMediaSize.Height), totalpages, caseName.Text);

            xdwPrint.Write(paginator);

            Close();
        }
        
        private void OnPrint(object sender, RoutedEventArgs e)
        {
            PrintIt();
        }
    }

    public class DocumentPaginatorWrapper : DocumentPaginator
    {
        Size m_PageSize;
        Size m_Margin;
        DocumentPaginator m_Paginator;
        Typeface m_Typeface;
        double x_footer;
        double y_footer;
        int m_pagestotal;
        string caseName = string.Empty;

        public DocumentPaginatorWrapper(DocumentPaginator paginator, Size pageSize, Size margin, double xfooter, double yfooter, int pagestotal, string caseName)
        {
            this.caseName = caseName;
            m_PageSize = pageSize;
            m_Margin = margin;
            m_Paginator = paginator;

            x_footer = xfooter;
            y_footer = yfooter;
            m_pagestotal = pagestotal;

            m_Paginator.PageSize = new Size(m_PageSize.Width - margin.Width * 2,
                                            m_PageSize.Height - margin.Height * 2);
        }

        Rect Move(Rect rect)
        {
            if (rect.IsEmpty)
            {
                return rect;
            }

            else
            {
                return new Rect(rect.Left + m_Margin.Width, rect.Top + m_Margin.Height,
                                rect.Width, rect.Height);
            }
        }

        public override DocumentPage GetPage(int pageNumber)
        {
            DocumentPage page = m_Paginator.GetPage(pageNumber);

            // Create a wrapper visual for transformation and add extras
            ContainerVisual newpage = new ContainerVisual();
            DrawingVisual title = new DrawingVisual();

            using (DrawingContext ctx = title.RenderOpen())
            {
                if (m_Typeface == null)
                {
                    m_Typeface = new Typeface("Times New Roman");
                }

                //FOOTER
                string footertext = "Page " + (pageNumber + 1) + " of " + m_pagestotal + "      Case: " + caseName;
                FormattedText text = new FormattedText(footertext,
                    System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight,
                    m_Typeface, 14, Brushes.Black);

                ctx.DrawText(text, new Point(x_footer, y_footer)); // 1/4 inch above page content

                //HEADER
                string headertext = " " + DateTime.Now;
                text = new FormattedText(headertext,
                    System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight,
                    m_Typeface, 14, Brushes.Black);

                ctx.DrawText(text, new Point(x_footer, -96 / 4)); // 1/4 inch above 
            }

            ContainerVisual smallerPage = new ContainerVisual();
            smallerPage.Children.Add(page.Visual);

            smallerPage.Transform = new MatrixTransform(0.95, 0, 0, 0.95,
                0.025 * page.ContentBox.Width, 0.025 * page.ContentBox.Height);

            newpage.Children.Add(smallerPage);
            newpage.Children.Add(title);
            newpage.Transform = new TranslateTransform(m_Margin.Width, m_Margin.Height);
            return new DocumentPage(newpage, m_PageSize, Move(page.BleedBox), Move(page.ContentBox));
        }

        public override bool IsPageCountValid
        {
            get { return m_Paginator.IsPageCountValid; }
        }

        public override int PageCount
        {
            get { return m_Paginator.PageCount; }
        }

        public override Size PageSize
        {
            get { return m_Paginator.PageSize; }
            set { m_Paginator.PageSize = value; }
        }

        public override IDocumentPaginatorSource Source
        {
            get { return m_Paginator.Source; }
        }
    }

}
