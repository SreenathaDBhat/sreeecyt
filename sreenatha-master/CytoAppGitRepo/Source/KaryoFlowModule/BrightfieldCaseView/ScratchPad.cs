﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Collections.Specialized;
using System.Windows.Media.Imaging;
using System.Windows.Input;
using System.IO;
using System.Xml.Linq;
using System.Globalization;
using AI.Brightfield;
using System.Collections;

namespace AI
{
    public class ScratchPad : Notifier
    {
        private Guid id;
        private Guid caseId;
        public Guid Id { get { return id; } }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool ShowChromosomeData { get; set; }

        private Case theCase;
        private ObservableCollection<ScratchPadChromosomeItem> chromosomes;
        private ObservableCollection<ScratchPadArrowItem> arrows;
        private ObservableCollection<ScratchPadTextItem> textItems;
        private ObservableCollection<ScratchPadImageItem> imageItems;

        private ObservableCollection<ScratchPadChromosomeItem> deletedChromosomes;
        private ObservableCollection<ScratchPadImageItem> deletedImages;
        
        public ScratchPad(Guid id, Case c, Guid caseId)
        {
            this.id = id;
            this.caseId = caseId;
            this.theCase = c;
            Name = "NA";
            Description = "";

            chromosomes = new ObservableCollection<ScratchPadChromosomeItem>();
            arrows = new ObservableCollection<ScratchPadArrowItem>();
            textItems = new ObservableCollection<ScratchPadTextItem>();
            imageItems = new ObservableCollection<ScratchPadImageItem>();
            deletedChromosomes = new ObservableCollection<ScratchPadChromosomeItem>();
            deletedImages = new ObservableCollection<ScratchPadImageItem>();

            var db = new Connect.Database();
            Load(db);
        }

        public double MaxXChromosomeExtent
        {
            get
            {
                double x = 0;

                foreach (var c in chromosomes)
                {
                    x = Math.Max(x, c.Chrom.UserOffsetX + c.Chrom.Width);
                }

                foreach (var a in arrows)
                {
                    x = Math.Max(x, Math.Max(a.StartX, a.EndX));
                }

                foreach (var t in textItems)
                {
                    x = Math.Max(x, t.TranslateX + 100);
                }

                foreach (var i in imageItems)
                {
                    x = Math.Max(x, i.TranslateX + i.Image.PixelWidth);
                }

                return x + 20;
            }
        }

		public double MaxYChromosomeExtent
		{
			get
			{
				double y = 0;

				foreach (var c in chromosomes)
				{
                    y = Math.Max(y, c.Chrom.UserOffsetY + c.Chrom.Length);
				}

                foreach (var a in arrows)
                {
                    y = Math.Max(y, Math.Max(a.StartY, a.EndY));
                }

                foreach (var t in textItems)
                {
                    y = Math.Max(y, t.TranslateY + 100);
                }

                foreach (var i in imageItems)
                {
                    y = Math.Max(y, i.TranslateY + i.Image.PixelHeight);
                }

				return y + 20;
			}
		}

        public KaryotypeChromosome CurrentChrom
        {
            get;
            set;
        }

        public IEnumerable<ScratchPadChromosomeItem> Chromosomes
        {
            get { return chromosomes; }
            set { chromosomes = new ObservableCollection<ScratchPadChromosomeItem>(value); }
        }

        public void AddChromosome(ScratchPadChromosomeItem c)
        {
            chromosomes.Add(c);
            Notify("MaxXChromosomeExtent");
            Notify("MaxYChromosomeExtent");
        }

        public void AddChromosomes(IEnumerable<ScratchPadChromosomeItem> items)
        {
            ArrangeItems(items);
            foreach (var item in items)
                AddChromosome(item);
        }

        public void RemoveChromosome(ScratchPadChromosomeItem c)
        {
            deletedChromosomes.Add(c);
            chromosomes.Remove(c);

            Notify("MaxXChromosomeExtent");
            Notify("MaxYChromosomeExtent");
        }

        public void ScratchPadItemsHaveMoved()
        {
            foreach (var c in chromosomes)
            {
                c.Chrom.UserOffsetX = Math.Max(0, c.Chrom.UserOffsetX);
                c.Chrom.UserOffsetY = Math.Max(0, c.Chrom.UserOffsetY);
            }

            foreach (var a in arrows)
            {
                a.StartX = Math.Max(0, a.StartX);
                a.StartY = Math.Max(0, a.StartY);
                a.EndX = Math.Max(0, a.EndX);
                a.EndY = Math.Max(0, a.EndY);
            }

            foreach (var t in textItems)
            {
                t.TranslateX = Math.Max(0, t.TranslateX);
                t.TranslateY = Math.Max(0, t.TranslateY);
            }

            foreach (var i in imageItems)
            {
                i.TranslateX = Math.Max(0, i.TranslateX);
                i.TranslateY = Math.Max(0, i.TranslateY);
            }

            Notify("MaxXChromosomeExtent");
            Notify("MaxYChromosomeExtent");
        }

        public void AddKaryotypeChromosomes(string chromsome, Guid caseId)
        {
            var db = new Connect.Database();



            var karChroms = from k in db.CaseKaryotypeChromosomes
                            where k.caseId == caseId && k.chromosomeTag == chromsome
                            orderby k.cellId
                            select k;

            var chroms = new List<ScratchPadChromosomeItem>();
            foreach(var k in karChroms)
            {
                var chrom = KaryotypeChromosome.CreateFromDb(caseId, k.karyotypeChromosomeId, db);
                KaryotypeChromosome.InitializeImage(chrom, db, null, null); // HACK WH - needs a datasource for the cell
                ScratchPadChromosomeItem item = new ScratchPadChromosomeItem();
                item.Id = Guid.NewGuid();
                item.Chrom = chrom;

                var slide = (from s in db.Slides
                             where s.slideId == k.slideId
                             select s).FirstOrDefault();

                var slideName = Connect.LIMS.SlideName(slide.limsRef);

                item.Info = new ChromInfo() { Identifier = k.chromosomeTag, Cell = k.cellName, CellId = k.cellId, Slide = slideName };
                chroms.Add(item);
            }

            ArrangeItems(chroms);

            foreach (var k in chroms)
            {
                AddChromosome(k);
            }
        }

        public void ArrangeItems(IEnumerable<ScratchPadChromosomeItem> itemsToArrange)
        {
            double x = 10;
			double y = MaxYChromosomeExtent;
            Guid previousCell = Guid.Empty;

            foreach (var c in itemsToArrange)
            {
                if (c.Info.CellId == previousCell && previousCell != Guid.Empty)
                    x += 15;
                if (c.Info.CellId != previousCell && previousCell != Guid.Empty)
                    x += 150;

                c.Chrom.UserOffsetX = x;
                c.Chrom.UserOffsetY = y;

                x += c.Chrom.Width;
                previousCell = c.Info.CellId;
            }
        }

        public static ScratchPad FromDb(Guid padId)
        {
            return null;
        }

        private void Load(Connect.Database db)
        {
            var scratchPadItems = from i in db.ScratchPadItems
                                  where i.scratchPadId == Id
                                  select i;

            foreach (var item in scratchPadItems)
            {
                if (item.itemDescription.Name.LocalName == "Chromosome")
                {
                    chromosomes.Add(ScratchPadChromosomeItem.FromDb(item, caseId, db));
                }
                if (item.itemDescription.Name.LocalName == "Arrow")
                {
                    arrows.Add(ScratchPadArrowItem.FromDb(item));
                }
                if (item.itemDescription.Name.LocalName == "Text")
                {
                    textItems.Add(ScratchPadTextItem.FromDb(item));
                }
                if (item.itemDescription.Name.LocalName == "Image")
                {
                    imageItems.Add(ScratchPadImageItem.FromDb(item, caseId, db));
                }
            }
        }

        public void Save(Connect.Database db)
        {
            var dbPad = db.ScratchPads.Where(s => s.scratchPadId == Id).FirstOrDefault();
            if (dbPad == null)
            {
                id = Guid.NewGuid();
                db.ScratchPads.InsertOnSubmit(new Connect.ScratchPad()
                {
                    scratchPadId = id,
                    caseId = caseId,
                    description = Description,
                    name = Name
                });
            }
            else
            {
                DeleteItemsFromDb(db);
            }

            SaveAllItems(db);

            using (var lims = Connect.LIMS.New())
            {
                lims.MarkCaseAsModified(theCase, DateTime.Now, "Scratchpad");
            }
        }

        private void SaveAllItems(Connect.Database db)
        {
            foreach (var chrom in chromosomes)
            {
                chrom.ToDb(db, caseId, this);
            }

            foreach (var arrow in arrows)
            {
                arrow.ToDb(db, this);
            }

            foreach (var text in textItems)
            {
                if (text.Text != string.Empty)
                    text.ToDb(db, this);
            }

            foreach (var image in imageItems)
            {
                image.ToDb(db, caseId, this);
            }
        }

        public void DeleteFromDb()
        {
            var db = new Connect.Database();
            DeleteItemsFromDb(db);
            db.ScratchPads.DeleteAllOnSubmit(db.ScratchPads.Where(p => p.scratchPadId == Id));
            db.SubmitChanges();
        }

        public void AddArrow(ScratchPadArrowItem arrow)
        {
            arrows.Add(arrow);
            ScratchPadItemsHaveMoved();
        }

        public IEnumerable<ScratchPadArrowItem> Arrows
        {
            get { return arrows; }
        }

        public void RemoveArrow(ScratchPadArrowItem arrow)
        {
            arrows.Remove(arrow);
            ScratchPadItemsHaveMoved();
        }

        public IEnumerable<ScratchPadTextItem> TextItems
        {
            get { return textItems; }
        }

        public IEnumerable<ScratchPadImageItem> ImageItems
        {
            get { return imageItems; }
        }

        public void AddImageItem(ScratchPadImageItem image)
        {
            imageItems.Add(image);
            ScratchPadItemsHaveMoved();
        }

        public void RemoveImageItem(ScratchPadImageItem image)
        {
            deletedImages.Add(image);
            imageItems.Remove(image);
            ScratchPadItemsHaveMoved();
        }

        public void DeletePressed()
        {
            foreach (var arrow in arrows)
            {
                if (arrow.IsMouseOver)
                {
                    RemoveArrow(arrow);
                    return;
                }
            }

            foreach (var chromosome in chromosomes)
            {
                if (chromosome.Chrom.IsMouseOver)
                {
                    RemoveChromosome(chromosome);
                    return;
                }
            }
        }

        public void AddTextItem(Point point)
        {
            AddTextItem(point, null);
        }

        public void AddTextItem(Point point, string initialText)
        {
            ScratchPadTextItem text = new ScratchPadTextItem();
            text.TranslateX = point.X;
            text.TranslateY = point.Y;

            if (initialText != null)
                text.Text = initialText;

            textItems.Add(text);
            ScratchPadItemsHaveMoved();
        }

        public void AddTextItem(ScratchPadTextItem textItem)
        {
            textItems.Add(textItem);
            ScratchPadItemsHaveMoved();
        }

        public void RemoveTextItem(ScratchPadTextItem text)
        {
            textItems.Remove(text);
            ScratchPadItemsHaveMoved();
        }

        internal void DeleteItemsFromDb(AI.Connect.Database db)
        {
            foreach (var chrom in deletedChromosomes)
                chrom.PurgeBlob(db, caseId);

            foreach (var chrom in chromosomes)
                chrom.PurgeBlob(db, caseId);

            foreach (var image in deletedImages)
                image.PurgeBlob(db, caseId);

            foreach (var image in imageItems)
                image.PurgeBlob(db, caseId);

            db.ScratchPadItems.DeleteAllOnSubmit(db.ScratchPadItems.Where(i => i.scratchPadId == id));
        }

        internal void FlipXForCurrentChrom()
        {
            if (CurrentChrom != null)
                CurrentChrom.IsXFlipped = !CurrentChrom.IsXFlipped;
        }

        internal void FlipYForCurrentChrom()
        {
            if (CurrentChrom != null)
                CurrentChrom.IsYFlipped = !CurrentChrom.IsYFlipped;
        }

        internal void Rotate180ForCurrentChrom()
        {
            if (CurrentChrom != null)
                CurrentChrom.UserRotation = CurrentChrom.UserRotation + 180;
        }

        public bool IsEmpty
        {
            get { return chromosomes.Count == 0 && textItems.Count == 0 && arrows.Count == 0 && imageItems.Count == 0; }
        }

        internal void DropTextLabelForCurrentChrom()
        {
            if (CurrentChrom != null)
            {
                ScratchPadChromosomeItem chrom = chromosomes.Where(c => c.Chrom == CurrentChrom).FirstOrDefault();

                if (chrom != null)
                {
                    ScratchPadTextItem text = new ScratchPadTextItem();
                    text.Text = chrom.Info.ToolTip;
                    text.TranslateX = CurrentChrom.TranslateX;
                    text.TranslateY = CurrentChrom.TranslateY;
                    textItems.Add(text);
                }
            }
        }

        internal void DeleteCurrentChrom()
        {
            if (CurrentChrom != null)
            {
                ScratchPadChromosomeItem chrom = chromosomes.Where(c => c.Chrom == CurrentChrom).FirstOrDefault();

                if (chrom != null)
                {
                    RemoveChromosome(chrom);
                }
            }
        }
    }



    public class ScratchPadsController : Notifier
    {
        #region ScratchPadNameComparer
        private class PadComparer : IComparer<ScratchPad>
        {
            #region IComparer<ScratchPad> Members

            public int Compare(ScratchPad x, ScratchPad y)
            {
                if (int.Parse(x.Name) < int.Parse(y.Name))
                    return -1;
                else if (int.Parse(x.Name) == int.Parse(y.Name))
                    return 0;
                else
                    return 1;
            }

            #endregion
        }
        #endregion

        public static readonly RoutedUICommand DeleteScratchPadPad = new RoutedUICommand("DeleteScratchPadPad", "DeleteScratchPadPad", typeof(ScratchPadsController));
        public static readonly RoutedUICommand NewScratchPad = new RoutedUICommand("NewScratchPad", "NewScratchPad", typeof(ScratchPadsController));
        public static readonly RoutedUICommand PromoteScratchPad = new RoutedUICommand("PromoteScratchPad", "PromoteScratchPad", typeof(ScratchPadsController));

        private Guid caseId;
        private string caseName;
        private ObservableCollection<ScratchPad> scratchPads = new ObservableCollection<ScratchPad>();
        private ObservableCollection<ScratchPad> deletedPads = new ObservableCollection<ScratchPad>();
        private ScratchPad current;
        private ScratchPad scratch;
        private bool isCurrentScratch;
        private Case theCase;

        public ScratchPadsController(Case theCase, Guid caseId)
        {
            this.caseId = caseId;
            this.theCase = theCase;

            var db = new Connect.Database();
            var list = new List<ScratchPad>(from s in db.ScratchPads
                                            where s.caseId == caseId
                                            orderby s.name
                                            select new ScratchPad(s.scratchPadId, theCase, caseId)
                                            {
                                                Description = s.description,
                                                Name = s.name
                                            });

            SortScratchPadsHack(list);
            scratchPads = new ObservableCollection<ScratchPad>(list);

            using(var lims = Connect.LIMS.New())
            {
                var cse = db.Cases.Where(c => c.caseId == caseId).First();
                var lc = lims.GetCase(cse.limsRef);
                caseName = lc.Name;

                if (scratchPads.Count == 0)
                {
                    AddScratchPad();
                }

                current = scratchPads.LastOrDefault();
            }
        }

        private void SortScratchPadsHack(List<ScratchPad> list)
        {
            PadComparer pc = new PadComparer();
            list.Sort(pc);
        }

        public string CaseName
        {
            get { return caseName; }
        }

        public ScratchPad Current
        {
            get { return isCurrentScratch ? null : current; }
            set { current = value; isCurrentScratch = false; Notify("IsCurrentScratch"); Notify("DisplayPad"); Notify("Current"); }
        }

        public ScratchPad DisplayPad
        {
            get { return isCurrentScratch ? scratch : current; }
        }

        public bool IsCurrentScratch
        {
            get { return isCurrentScratch; }
            set { isCurrentScratch = value; Notify("IsCurrentScratch"); Notify("DisplayPad"); Notify("Current"); }
        }

        public ScratchPad Scratch
        {
            get { return scratch; }
        }

        public IEnumerable<ScratchPad> ScratchPads
        {
            get { return scratchPads; }
        }

        public bool CanAddNewScratchPad
        {
            get { return scratchPads.Count < 10; }
        }

        public bool GotMoreThanOneScratchPad
        {
            get { return scratchPads.Count > 1; }
        }

        public void AddScratchPad()
        {
            var pad = new ScratchPad(Guid.NewGuid(), theCase, caseId);
            int biggestExisting = GetBiggestPadNumber();

            scratchPads.Add(pad);
            pad.Name = (biggestExisting + 1).ToString();
            
            Current = pad;
        }

        private int GetBiggestPadNumber()
        {
            int biggestExisting = 0;

            foreach (var p in scratchPads)
            {
                int n;
                if (int.TryParse(p.Name, out n))
                {
                    biggestExisting = Math.Max(n, biggestExisting);
                }
            }
            return biggestExisting;
        }

        public void SaveAll()
        {
            var db = new Connect.Database();
            foreach (ScratchPad pad in scratchPads)
                pad.DeleteItemsFromDb(db);

            foreach (ScratchPad pad in deletedPads)
                pad.DeleteItemsFromDb(db);

            db.SubmitChanges();

            foreach (ScratchPad pad in scratchPads)
                pad.DeleteFromDb();

            foreach (ScratchPad pad in deletedPads)
                pad.DeleteFromDb();

            foreach (ScratchPad pad in scratchPads)
                pad.Save(db);
           
            db.SubmitChanges();
        }

        public void AddKaryotypeChromosomes(string chrom)
        {
            DisplayPad.AddKaryotypeChromosomes(chrom, caseId);
        }

        public void DeleteItem(ScratchPadChromosomeItem item)
        {
            DisplayPad.RemoveChromosome(item);
        }

        public void DeleteCurrentPad()
        {
            deletedPads.Add(current);
            scratchPads.Remove(current);
            
            if (scratchPads.Count == 0)
            {
                AddScratchPad();
            }

            Current = scratchPads.Last();
        }

        public void ShowScratch(IEnumerable<ScratchPadChromosomeItem> items)
        {
            if (scratch == null)
                scratch = new ScratchPad(Guid.NewGuid(), theCase, caseId);

            scratch.ArrangeItems(items);

            foreach (var i in items)
            {
                scratch.AddChromosome(i);
            }

            Notify("Scratch");
            IsCurrentScratch = true;
        }

        public void PromoteScratch()
        {
            if (TheresOnlyTheDefaultEmptyPad())
            {
                scratch.Name = "1";
                deletedPads.Add(scratchPads[0]);
                scratchPads.Clear();
                scratchPads.Add(scratch);
                Current = scratchPads[0];
            }

            else
            {
                int biggest = GetBiggestPadNumber();
                scratch.Name = (biggest + 1).ToString();
                scratchPads.Add(scratch);
                Current = scratchPads.Last();
            }

            IsCurrentScratch = false;
            scratch = null;
            Notify("Scratch");
        }

        private bool TheresOnlyTheDefaultEmptyPad()
        {
            return scratchPads.Count <= 1 && scratchPads[0].IsEmpty;
        }

        public void AddToExistingPad(IEnumerable<ScratchPadChromosomeItem> items, Guid guid)
        {
            if (scratchPads.Count(sp => sp.Id == guid) > 0)
            {
                Current = scratchPads.Where(sp => sp.Id == guid).FirstOrDefault();
                Current.AddChromosomes(items);
            }
            else
            {
                ShowScratch(items);
            }
        }
    }
}
