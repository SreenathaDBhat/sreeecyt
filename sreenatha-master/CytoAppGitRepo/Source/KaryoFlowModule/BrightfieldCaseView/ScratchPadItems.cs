﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.IO;
using System.Xml.Linq;
using AI.Brightfield;
using System.Windows.Media.Imaging;
using System.Globalization;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Media.Media3D;

namespace AI
{
    public class ChromInfo
    {
        public string Identifier { get; set; }
        public string Cell { get; set; }
        public string Slide { get; set; }
        public Guid CellId { get; set; }

        public string ToolTip
        {
            get
            {
                return "Chromosome: " + Identifier + "\nCell: " + Cell + "\nSlide: " + Slide;
            }
        }
    }

    public class ScratchPadChromosomeItem
    {
        public Guid Id { get; set; }
        public KaryotypeChromosome Chrom { get; set; }
        public ChromInfo Info { get; set; }

        public static ScratchPadChromosomeItem FromDb(Connect.ScratchPadItem item, Guid caseId, Connect.Database db)
        {
            ScratchPadChromosomeItem result = new ScratchPadChromosomeItem();

            var image = ImageFromBitmapSource.LoadBitmapImageFromStream(caseId, ImageFilePath(item.scratchPadItemId), db);
            Guid cellId = new Guid(item.itemDescription.Attribute("CellId").Value);
            var k = AI.Brightfield.KaryotypeChromosome.CreateBare(cellId);
            k.DisplayImage = image;
            k.RotationToVertical = item.angleToVertical;
            k.UserRotation = item.userRotation;
            k.IsXFlipped = item.flipX;
            k.IsYFlipped = item.flipY;
            k.TranslateX = item.x;
            k.TranslateY = item.y;
            k.Bounds = new Int32Rect(int.Parse(item.itemDescription.Attribute("BoundsX").Value, CultureInfo.InvariantCulture),
                                     int.Parse(item.itemDescription.Attribute("BoundsY").Value, CultureInfo.InvariantCulture),
                                     int.Parse(item.itemDescription.Attribute("BoundsWidth").Value, CultureInfo.InvariantCulture),
                                     int.Parse(item.itemDescription.Attribute("BoundsHeight").Value, CultureInfo.InvariantCulture));

            k.SetSize(double.Parse(item.itemDescription.Attribute("Width").Value, CultureInfo.InvariantCulture),
                      double.Parse(item.itemDescription.Attribute("Length").Value, CultureInfo.InvariantCulture));

            k.SetDefaultMidPoint(int.Parse(item.itemDescription.Attribute("BoundsWidth").Value, CultureInfo.InvariantCulture),
                int.Parse(item.itemDescription.Attribute("BoundsHeight").Value, CultureInfo.InvariantCulture));

            ChromInfo info = new ChromInfo();
            info.Identifier = item.itemDescription.Attribute("Identifier").Value;
            info.Cell = item.itemDescription.Attribute("Cell").Value;
            info.Slide = item.itemDescription.Attribute("Slide").Value;

            result.Info = info;
            result.Id = item.scratchPadItemId;
            result.Chrom = k;

            return result;
        }

        internal static string ImageFilePath(Guid itemId)
        {
            return itemId.ToString() + ".scratchPadChromosome.png";
        }

        internal void ToDb(Connect.Database db, Guid caseId, ScratchPad chromosomeScratchPad)
        {
            var scratchPadItem = db.ScratchPadItems.Where(i => i.scratchPadItemId == Id).FirstOrDefault();

            if (scratchPadItem == null)
            {
                scratchPadItem = new AI.Connect.ScratchPadItem();
                Id = Guid.NewGuid();
                scratchPadItem.scratchPadItemId = Id;
                scratchPadItem.scratchPadId = chromosomeScratchPad.Id;
                db.ScratchPadItems.InsertOnSubmit(scratchPadItem);
            }

            scratchPadItem.itemDescription = DescriptionForChromItem();
            scratchPadItem.x = Chrom.TranslateX;
            scratchPadItem.y = Chrom.TranslateY;
            scratchPadItem.angleToVertical = Chrom.RotationToVertical;
            scratchPadItem.userRotation = Chrom.UserRotation;
            scratchPadItem.flipX = Chrom.IsXFlipped;
            scratchPadItem.flipY = Chrom.IsYFlipped;

            var path = ImageFilePath(Id);
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(Chrom.DisplayImage));
            using (var stream = new MemoryStream())
            {
                encoder.Save(stream);
                stream.Flush();

                scratchPadItem.blobId = Connect.BlobStream.WriteBlobStream(caseId, path, stream, db);
                stream.Close();
            }
        }

        private XElement DescriptionForChromItem()
        {
            var xml = new XElement("Chromosome");
            xml.Add(new XAttribute("Identifier", Info.Identifier));
            xml.Add(new XAttribute("Cell", Info.Cell));
            xml.Add(new XAttribute("Slide", Info.Slide));
            xml.Add(new XAttribute("CellId", Info.CellId.ToString()));
            xml.Add(new XAttribute("Width", Chrom.Width));
            xml.Add(new XAttribute("Length", Chrom.Length));

            xml.Add(new XAttribute("BoundsX", Chrom.Bounds.X));
            xml.Add(new XAttribute("BoundsY", Chrom.Bounds.Y));
            xml.Add(new XAttribute("BoundsWidth", Chrom.Bounds.Width));
            xml.Add(new XAttribute("BoundsHeight", Chrom.Bounds.Height));
            return xml;
        }

        internal void PurgeBlob(AI.Connect.Database deleteDb, Guid caseId)
        {
            var items = deleteDb.ScratchPadItems.Where(i => i.scratchPadItemId == Id);

            foreach (var i in items)
            {
                if ((i.blobId != null) && (i.blobId != Guid.Empty))
                {
                    Connect.BlobStream.Delete((Guid)i.blobId, deleteDb);
                }
            }

            deleteDb.ScratchPadItems.DeleteAllOnSubmit(items);
        }
    }

    public class ScratchPadArrowItem : Notifier
    {
        private Guid id;
        private double startX, startY, endX, endY;

        public bool IsMouseOver { get; set; }

        public double StartX { get { return startX; } set { startX = value; Notify("StartX"); Notify("ArrowLine"); } }
        public double StartY { get { return startY; } set { startY = value; Notify("StartY"); Notify("ArrowLine"); } }
        public double EndX { get { return endX; } set { endX = value; Notify("EndX"); Notify("ArrowLine"); } }
        public double EndY { get { return endY; } set { endY = value; Notify("EndY"); Notify("ArrowLine"); } }

        public PointCollection ArrowLine
        {
            get
            {
                Point3D a = new Point3D(startX, startY, 0);
                Point3D b = new Point3D(endX, endY, 0);
                Vector3D v = a - b;
                Vector3D vn = v;
                vn.Normalize();
                double partLen = v.Length - 10;
                Point3D c = b + (vn * partLen);

                Vector3D intoScreen = new Vector3D(0, 0, 1);
                Vector3D perp = Vector3D.CrossProduct(intoScreen, vn);

                Point3D d = c + (perp * 5);
                Point3D e = c - (perp * 5);

                PointCollection points = new PointCollection();
                points.Add(new Point(b.X, b.Y));
                points.Add(new Point(c.X, c.Y));
                points.Add(new Point(e.X, e.Y));
                points.Add(new Point(a.X, a.Y));
                points.Add(new Point(d.X, d.Y));
                points.Add(new Point(c.X, c.Y));

                return points;
            }
        }

        public XElement DescriptionXml()
        {
            var xml = new XElement("Arrow");
            xml.Add(new XAttribute("StartX", startX));
            xml.Add(new XAttribute("StartY", startY));
            xml.Add(new XAttribute("EndX", endX));
            xml.Add(new XAttribute("EndY", endY));
            return xml;
        }

        public static ScratchPadArrowItem FromDb(Connect.ScratchPadItem dbItem)
        {
            ScratchPadArrowItem result = new ScratchPadArrowItem()
            {
                StartX = double.Parse(dbItem.itemDescription.Attribute("StartX").Value, CultureInfo.InvariantCulture),
                StartY = double.Parse(dbItem.itemDescription.Attribute("StartY").Value, CultureInfo.InvariantCulture),
                EndX = double.Parse(dbItem.itemDescription.Attribute("EndX").Value, CultureInfo.InvariantCulture),
                EndY = double.Parse(dbItem.itemDescription.Attribute("EndY").Value, CultureInfo.InvariantCulture)
            };
            result.id = dbItem.scratchPadItemId;
            return result;
        }

        public void ToDb(Connect.Database db, ScratchPad parentScratchPad)
        {
            var scratchPadItem = db.ScratchPadItems.Where(i => i.scratchPadItemId == id).FirstOrDefault();

            if (scratchPadItem == null)
            {
                scratchPadItem = new AI.Connect.ScratchPadItem();
                id = Guid.NewGuid();
                scratchPadItem.scratchPadItemId = id;
                scratchPadItem.scratchPadId = parentScratchPad.Id;
                db.ScratchPadItems.InsertOnSubmit(scratchPadItem);
            }

            scratchPadItem.itemDescription = DescriptionXml();
            scratchPadItem.x = 0;
            scratchPadItem.y = 0;
            scratchPadItem.angleToVertical = 0;
            scratchPadItem.userRotation = 0;
        }
    }

    public class ArrowPlacementHelper
    {
        private ScratchPadArrowItem arrowBeingPlaced;
        private Point arrowBeingPlacedDropPoint;
        ScratchPadsController controller;

        public ArrowPlacementHelper(ScratchPadsController padsController)
        {
            controller = padsController;
        }

        private ScratchPadArrowItem AddNewArrow(Point position)
        {
            ScratchPadArrowItem result = new ScratchPadArrowItem() { StartX = position.X, StartY = position.Y, EndX = position.X, EndY = position.Y };
            controller.DisplayPad.AddArrow(result);
            return result;
        }

        public void StartPlacingArrow(object sender, MouseButtonEventArgs e, IInputElement relativeTo)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                e.Handled = true;

                FrameworkElement s = (FrameworkElement)sender;
                s.CaptureMouse();

                Point pos = e.GetPosition(relativeTo);
                arrowBeingPlaced = AddNewArrow(pos);
                arrowBeingPlacedDropPoint = pos;
            }
        }

        public void MoveArrowBeingPlaced(object sender, MouseEventArgs e, IInputElement relativeTo)
        {
            FrameworkElement s = (FrameworkElement)sender;
            if (!s.IsMouseCaptured || arrowBeingPlaced == null)
                return;

            Point pt = e.GetPosition(relativeTo);
            if (Math.Abs(pt.X - arrowBeingPlacedDropPoint.X) > 10 || Math.Abs(pt.Y - arrowBeingPlacedDropPoint.Y) > 10)
            {
                arrowBeingPlaced.StartX = pt.X;
                arrowBeingPlaced.StartY = pt.Y;
            }
        }

        public void FinishPlacingArrow(object sender, MouseButtonEventArgs e, IInputElement relativeTo)
        {
            FrameworkElement s = (FrameworkElement)sender;
            s.ReleaseMouseCapture();
            arrowBeingPlaced = null;
        }
    }

    public class ScratchPadTextItem : Notifier
    {
        private string text = string.Empty;
        private Guid id;
        private double x;
        private double y;
        private double scale = 1;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public double TranslateX
        {
            get { return x; }
            set { x = value; Notify("TranslateX"); }
        }

        public double TranslateY
        {
            get { return y; }
            set { y = value; Notify("TranslateY"); }
        }

        public double Scale
        {
            get { return scale; }
            set { scale = value; Notify("Scale"); }
        }

        public XElement DescriptionXml()
        {
            var xml = new XElement("Text");
            xml.Add(new XAttribute("Content", text));
            xml.Add(new XAttribute("Scale", scale));
            return xml;
        }

        public static ScratchPadTextItem FromDb(Connect.ScratchPadItem dbItem)
        {
            ScratchPadTextItem item = new ScratchPadTextItem()
            {
                Text = dbItem.itemDescription.Attribute("Content").Value,
                Scale = double.Parse(dbItem.itemDescription.Attribute("Scale").Value, CultureInfo.InvariantCulture),
                TranslateX = dbItem.x,
                TranslateY = dbItem.y
            };
            item.id = dbItem.scratchPadItemId;
            return item;
        }

        public void ToDb(Connect.Database db, ScratchPad parentScratchPad)
        {
            var scratchPadItem = db.ScratchPadItems.Where(i => i.scratchPadItemId == id).FirstOrDefault();

            if (scratchPadItem == null)
            {
                scratchPadItem = new AI.Connect.ScratchPadItem();
                id = Guid.NewGuid();
                scratchPadItem.scratchPadItemId = id;
                scratchPadItem.scratchPadId = parentScratchPad.Id;
                db.ScratchPadItems.InsertOnSubmit(scratchPadItem);
            }

            scratchPadItem.itemDescription = DescriptionXml();
            scratchPadItem.x = TranslateX;
            scratchPadItem.y = TranslateY;
            scratchPadItem.angleToVertical = 0;
            scratchPadItem.userRotation = 0;
        }
    }

    public class ScratchPadImageItem : Notifier
    {
        private double x;
        private double y;
        private double scale = 1;
        
        public Guid Id { get; set; }
        public BitmapSource Image { get; set; }

        public double TranslateX
        {
            get { return x; }
            set { x = value; Notify("TranslateX"); }
        }

        public double TranslateY
        {
            get { return y; }
            set { y = value; Notify("TranslateY"); }
        }

        public double Scale
        {
            get { return scale; }
            set { scale = value; Notify("Scale"); }
        }

        public static ScratchPadImageItem FromDb(Connect.ScratchPadItem item, Guid caseId, Connect.Database db)
        {
            ScratchPadImageItem result = new ScratchPadImageItem();

            var image = ImageFromBitmapSource.LoadBitmapImageFromStream(caseId, ImageFilePath(item.scratchPadItemId), db);
            result.Id = item.scratchPadItemId;
            result.Image = image;
            result.Scale = double.Parse(item.itemDescription.Attribute("Scale").Value, CultureInfo.InvariantCulture);
            result.TranslateX = item.x;
            result.TranslateY = item.y;

            return result;
        }

        internal static string ImageFilePath(Guid itemId)
        {
            return itemId.ToString() + ".scratchPadImage.png";
        }

        internal void ToDb(Connect.Database db, Guid caseId, ScratchPad chromosomeScratchPad)
        {
            var scratchPadItem = db.ScratchPadItems.Where(i => i.scratchPadItemId == Id).FirstOrDefault();

            if (scratchPadItem == null)
            {
                scratchPadItem = new AI.Connect.ScratchPadItem();
                Id = Guid.NewGuid();
                scratchPadItem.scratchPadItemId = Id;
                scratchPadItem.scratchPadId = chromosomeScratchPad.Id;
                db.ScratchPadItems.InsertOnSubmit(scratchPadItem);
            }

            scratchPadItem.itemDescription = DescriptionXml();
            scratchPadItem.x = TranslateX;
            scratchPadItem.y = TranslateY;
            scratchPadItem.angleToVertical = 0;
            scratchPadItem.userRotation = 0;

            var path = ImageFilePath(Id);
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(Image));
            using (var stream = new MemoryStream())
            {
                encoder.Save(stream);
                stream.Flush();

                scratchPadItem.blobId = Connect.BlobStream.WriteBlobStream(caseId, path, stream, db);
                stream.Close();
            }
        }

        private XElement DescriptionXml()
        {
            var xml = new XElement("Image");
            xml.Add(new XAttribute("Scale", scale));
            return xml;
        }

        internal void PurgeBlob(AI.Connect.Database deleteDb, Guid caseId)
        {
            var items = deleteDb.ScratchPadItems.Where(i => i.scratchPadItemId == Id);

            foreach (var i in items)
            {
                if ((i.blobId != null) && (i.blobId != Guid.Empty))
                {
                    Connect.BlobStream.Delete((Guid)i.blobId, deleteDb);
                }
            }

            deleteDb.ScratchPadItems.DeleteAllOnSubmit(items);
        }
    }
}