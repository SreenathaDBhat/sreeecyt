﻿using System;
using System.Windows.Controls;
using System.Windows;

namespace AI
{
    public partial class ScratchPadLooseItemDisplay : UserControl
    {
        public static readonly DependencyProperty CollectorProperty = DependencyProperty.Register("Collector", typeof(ScratchPadLooseItemCollector), typeof(ScratchPadLooseItemDisplay), new FrameworkPropertyMetadata(OnCollectorChanged));

        public ScratchPadLooseItemDisplay()
        {
            InitializeComponent();
        }

        public ScratchPadLooseItemCollector Collector
        {
            get { return (ScratchPadLooseItemCollector)GetValue(CollectorProperty); }
            set { SetValue(CollectorProperty, value); }
        }

        private static void OnCollectorChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((ScratchPadLooseItemDisplay)sender).DataContext = e.NewValue;

            if(e.OldValue is ScratchPadLooseItemCollector)
            {
                ((ScratchPadLooseItemCollector)e.OldValue).ListChanged -= ((ScratchPadLooseItemDisplay)sender).ScratchPadLooseItemDisplay_ListChanged;
            }

            if (e.NewValue is ScratchPadLooseItemCollector)
            {
                ((ScratchPadLooseItemCollector)e.NewValue).ListChanged += ((ScratchPadLooseItemDisplay)sender).ScratchPadLooseItemDisplay_ListChanged;
            }
        }

        void ScratchPadLooseItemDisplay_ListChanged(object sender, EventArgs e)
        {
            scroller.ScrollToBottom();
        }

        private void OnDeleteCut(object sender, RoutedEventArgs e)
        {
            Collector.DeleteItem((ScratchPadChromosomeItem)((FrameworkElement)sender).Tag);
        }
    }
}
