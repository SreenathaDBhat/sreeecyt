﻿using System;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media.Imaging;

namespace AI
{
    public partial class ScratchPadLooseItemInkPad : UserControl
    {
        public static readonly DependencyProperty CollectorProperty = DependencyProperty.Register("Collector", typeof(ScratchPadLooseItemCollector), typeof(ScratchPadLooseItemInkPad), new FrameworkPropertyMetadata(OnCollectorChanged));
        public event EventHandler<InkCanvasStrokeCollectedEventArgs> ItemDrawn;

        public ScratchPadLooseItemInkPad()
        {
            InitializeComponent();
        }

        public ScratchPadLooseItemCollector Collector
        {
            get { return (ScratchPadLooseItemCollector)GetValue(CollectorProperty); }
            set { SetValue(CollectorProperty, value); }
        }

        private void OnStrokeCollected(object sender, InkCanvasStrokeCollectedEventArgs e)
        {
            if (ItemDrawn != null)
                ItemDrawn(this, e);
            ((InkCanvas)sender).Strokes.Clear();
        }

        private static void OnCollectorChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((ScratchPadLooseItemInkPad)sender).DataContext = e.NewValue;
        }
    }
}
