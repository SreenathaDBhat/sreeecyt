﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using System.Windows.Ink;
using System.Collections.Generic;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows;
using System.Linq;
using System.Windows.Controls;
using AI.Brightfield;

namespace AI
{
    public class ScratchPadInfo
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool Temporary { get; set; }
    }

    public class ScratchPadLooseItemCollector : Notifier
    {
        public static readonly RoutedUICommand ShowLooseItemScratchPad = new RoutedUICommand("ShowLooseItemScratchPad", "ShowLooseItemScratchPad", typeof(ScratchPadLooseItemCollector));
        public static readonly RoutedUICommand CloseLooseItemScratchPadCollector = new RoutedUICommand("CloseLooseItemScratchPadCollector", "CloseLooseItemScratchPadCollector", typeof(ScratchPadLooseItemCollector));

        private ObservableCollection<ScratchPadChromosomeItem> items;
        private ObservableCollection<ScratchPadInfo> destinationScratchPads = new ObservableCollection<ScratchPadInfo>();
        public ScratchPadInfo DestinationPad { get; set; }
        private WorkflowController workflowController;
        private double penSize;

        public event EventHandler ListChanged;

        public ScratchPadLooseItemCollector(WorkflowController workflowController)
        {
            penSize = 30;
            this.workflowController = workflowController;
            items = new ObservableCollection<ScratchPadChromosomeItem>();
            UpdateDestinationPads(workflowController.CaseId);
        }

        public void CutStroke(ImageRenderer renderer, Stroke stroke, string cell, string slide, Guid cellId)
        {
            // Slight hack (sorry), but there's a problem saving the scratch pad if "nothing" items get through
            if (stroke.StylusPoints.Count <= 2)
                return;

            var w = renderer.ImageFrameX.ImageWidth;
            var h = renderer.ImageFrameX.ImageHeight;
            byte[] pixels = renderer.CopyPixels();
            var k = AI.Brightfield.ChromosomeCutter.CutStroke(pixels, w, h, stroke, cellId);

            ScratchPadChromosomeItem item = new ScratchPadChromosomeItem
            {
                Chrom = k,
                Id = cellId,
                Info = new ChromInfo
                {
                    Cell = cell,
                    CellId = cellId,
                    Identifier = "Unknown",
                    Slide = slide
                }
            };

            items.Add(item);
            FireListChanged();
        }

        private void FireListChanged()
        {
            if (ListChanged != null)
                ListChanged(this, EventArgs.Empty);
        }

        public double PenSize
        {
            get { return penSize; }
            set { penSize = value; Notify("PenSize", "DrawingAttributes"); }
        }

        public IEnumerable<ScratchPadChromosomeItem> Items
        {
            get { return items; }
        }

        public IEnumerable<ScratchPadInfo> DestinationPads
        {
            get { return destinationScratchPads; }
        }

        public static BitmapSource SourceBitmapFromImageRenderer(ImageRenderer renderer)
        {
            var img = renderer.ImageFrameX;
            return BitmapSource.Create(img.ImageWidth, img.ImageHeight, 96, 96, PixelFormats.Bgra32, null, renderer.CopyPixels(), img.ImageWidth * 4);
        }

        public void ClearItems()
        {
            items.Clear();
        }

        public DrawingAttributes DrawingAttributes
        {
            get
            {
                return new DrawingAttributes
                {
                    Color = Color.FromArgb(100, 80, 255, 80),
                    FitToCurve = true,
                    Height = penSize,
                    IgnorePressure = true,
                    Width = penSize
                };
            }
        }

        public void DeleteItem(ScratchPadChromosomeItem item)
        {
            items.Remove(item);
        }

        public void UpdateDestinationPads(Guid caseId)
        {
            //destinationScratchPads.Clear();
            //Connect.Database db = new AI.Connect.Database();
            //var existingPads = from sp in db.ScratchPads
            //                   where sp.caseId == caseId
            //                   select sp;

            //var temporaryText = workflowController.StringTable.Lookup("TemporaryPad");
            //DestinationPad = new ScratchPadInfo() { Temporary = true, Name = temporaryText };
            //destinationScratchPads.Add(DestinationPad);
            //foreach (var sp in existingPads)
            //    destinationScratchPads.Add(new ScratchPadInfo() { Temporary = false, Id = sp.scratchPadId, Name = sp.name });
            //Notify("DestinationPad");
        }
    }



    public class ScratchPadLoosItemHookerUpper : DependencyObject
    {
        public static DependencyProperty LooseItemsProperty = DependencyProperty.RegisterAttached("LooseItems", typeof(ScratchPadLoosItemHookerUpper), typeof(ScratchPadLoosItemHookerUpper));
        public static DependencyProperty InLooseCollectionModeProperty = DependencyProperty.Register("InLooseCollectionMode", typeof(bool), typeof(ScratchPadLoosItemHookerUpper));

        private FrameworkElement parent;
        private ScratchPadLooseItemCollector collector;
        private ImageRenderer image;
        private WorkflowController workflowController;

        public ScratchPadLoosItemHookerUpper(FrameworkElement parent, ImageRenderer image, WorkflowController workflowController)
        {
            this.workflowController = workflowController;
            this.parent = parent;
            collector = new ScratchPadLooseItemCollector(workflowController);
            this.image = image;

            parent.CommandBindings.Add(new CommandBinding(ScratchPadLooseItemCollector.ShowLooseItemScratchPad, new ExecutedRoutedEventHandler(ShowLooseItemsExecuted), new CanExecuteRoutedEventHandler(CanShowLooseItemsExecute)));
            parent.CommandBindings.Add(new CommandBinding(ScratchPadLooseItemCollector.CloseLooseItemScratchPadCollector, new ExecutedRoutedEventHandler(CloseLooseItemsExecuted), new CanExecuteRoutedEventHandler(CanCloseLooseItemsExecute)));
        }

        public static ScratchPadLoosItemHookerUpper GetLooseItems(DependencyObject who)
        {
            return (ScratchPadLoosItemHookerUpper)who.GetValue(LooseItemsProperty);
        }

        public static void SetLooseItems(DependencyObject who, ScratchPadLoosItemHookerUpper what)
        {
            who.SetValue(LooseItemsProperty, what);
        }

        public ScratchPadLooseItemCollector Collector
        {
            get { return collector; }
        }

        public bool InLooseCollectionMode
        {
            get { return (bool)GetValue(InLooseCollectionModeProperty); }
            set { SetValue(InLooseCollectionModeProperty, value); }
        }

        private void CanShowLooseItemsExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = collector.Items.Count() > 0;
        }

        private void ShowLooseItemsExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(parent));

            var items = collector.Items.ToList();
            var destination = collector.DestinationPad;
            
            ScratchPadWindow w = new ScratchPadWindow(workflowController); // NYI !!!
            w.Owner = VisualTreeWalker.FindParentOfType<Window>(parent);

            if (destination.Temporary)
                w.ShowScratch(items);
            else
                w.AddToExistingPad(items, destination.Id);

            w.ShowDialog();

            collector.UpdateDestinationPads(workflowController.CaseId);
            InLooseCollectionMode = false;
            collector.ClearItems();
        }

        private void CanCloseLooseItemsExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CloseLooseItemsExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            InLooseCollectionMode = false;
            collector.ClearItems();
        }
    }
}
