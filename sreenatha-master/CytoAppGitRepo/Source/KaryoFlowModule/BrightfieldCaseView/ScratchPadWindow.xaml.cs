﻿using System;
using System.Windows;
using System.Windows.Input;
using AI.Brightfield;
using System.Globalization;
using System.Collections.Specialized;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Xml.Linq;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;
using System.Threading;
using System.Windows.Threading;
using System.Diagnostics;

namespace AI
{
    public partial class ScratchPadWindow : Window
    {
        internal enum AddType
        {
            Karyotped = 0,
            Numbered
        }

        private WorkflowController controller;
        private ScratchPadsController padsController;
        private AddType addKaryotypedOrNumbered = AddType.Karyotped;
        private ArrowPlacementHelper arrowPlacementHelper;
        private Point? drag;

        public ScratchPadWindow(WorkflowController controller)
        {
            this.controller = controller;

            padsController = new ScratchPadsController(controller.Case, controller.CaseId);
            arrowPlacementHelper = new ArrowPlacementHelper(padsController);

            DataContext = padsController;

            InitializeComponent();

            WindowLocations.Handle(this);
        }

        private void OnAddKaryotyped(object sender, RoutedEventArgs e)
        {
            addKaryotypedOrNumbered = AddType.Karyotped;
            addStuffPopup.IsOpen = true;
        }

        private void OnAddNumbered(object sender, RoutedEventArgs e)
        {
            addKaryotypedOrNumbered = AddType.Numbered;
            addStuffPopup.IsOpen = true;
        }

        private void OnDragMouseDown(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)sender;
            KaryotypeChromosome item = ((ScratchPadChromosomeItem)element.Tag).Chrom;
            ChromosomeDragger.StartTranslateDrag(item, e, scratchItemsList,
                (obd, dt) =>
                {
                       padsController.DisplayPad.ScratchPadItemsHaveMoved(); 
                },
                (d, me) =>
                {
                    if (e.ClickCount == 2)
                        ((KaryotypeChromosome)d).UserRotation += 180;
                });
        }

        private void OnDropTextLabelForChrom(object sender, RoutedEventArgs e)
        {
            padsController.DisplayPad.DropTextLabelForCurrentChrom();
            chromContextMenu.IsOpen = false;
        }

        private void OnScratchPadItemRightMouseDown(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)sender;
            KaryotypeChromosome item = ((ScratchPadChromosomeItem)element.Tag).Chrom;
            ChromosomeDragger.StartRotateDrag(item, e, scratchItemsList, null, (d, me) => 
            {
                padsController.DisplayPad.CurrentChrom = (KaryotypeChromosome)d;
                chromContextMenu.IsOpen = true;
            });
        }

        private void OnScratchPadArrowRightMouseUp(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)sender;
            ScratchPadArrowItem item = (ScratchPadArrowItem)element.Tag;
            arrowContextMenu.IsOpen = true;
            arrowContextMenu.Tag = item;
        }

        private void OnDeleteItem(object sender, RoutedEventArgs e)
        {
            FrameworkElement element = (FrameworkElement)sender;
            ScratchPadChromosomeItem item = (ScratchPadChromosomeItem)element.Tag;
            padsController.DeleteItem(item);
        }

        private void OnLeftClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                padsController.DisplayPad.AddTextItem(e.GetPosition(scratchItemsList));
            }

            else
            {
                arrowPlacementHelper.StartPlacingArrow(sender, e, scratchItemsList);
            }
        }

        private void OnLeftMouseUp(object sender, MouseButtonEventArgs e)
        {
            arrowPlacementHelper.FinishPlacingArrow(sender, e, scratchItemsList);
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            arrowPlacementHelper.MoveArrowBeingPlaced(sender, e, scratchItemsList);
        }

        internal void ShowScratch()
        {
            Show();
            padsController.Current = padsController.Scratch;
        }

        public void ShowScratch(IEnumerable<ScratchPadChromosomeItem> items)
        {
            padsController.ShowScratch(items);
        }

        internal void AddToExistingPad(List<ScratchPadChromosomeItem> items, Guid guid)
        {
            padsController.AddToExistingPad(items, guid);
        }

        private void CanDeletePadExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !padsController.IsCurrentScratch && padsController.ScratchPads.Count() > 1;
        }

        private void DeletePadExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            DeleteStuff deleteWindow = new DeleteStuff(() =>
            {
                Dispatcher.Invoke((ThreadStart)delegate
                {
                    padsController.DeleteCurrentPad();
                }, DispatcherPriority.Normal);
            });

            deleteWindow.Owner = this;
            deleteWindow.ShowDialog();
        }

        private void CanNewPadExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = padsController.CanAddNewScratchPad;
        }

        private void NewPadExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Connect.ConnectionTest.Test(this);
            padsController.AddScratchPad();
        }

        private void CanPromotePadExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = padsController.CanAddNewScratchPad;
        }

        private void PromotePadExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            padsController.PromoteScratch();
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                e.Handled = true;
                Close();
            }
            else if (e.Key == Key.D || e.Key == Key.Delete)
            {
                padsController.DisplayPad.DeletePressed(); ;
            }
        }

        private void Thumb_DragDeltaStart(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            ScratchPadArrowItem arrow = (ScratchPadArrowItem)((FrameworkElement)sender).Tag;
            arrow.StartX = arrow.StartX + e.HorizontalChange;
            arrow.StartY = arrow.StartY + e.VerticalChange;
        }

        private void Thumb_DragDeltaEnd(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            ScratchPadArrowItem arrow = (ScratchPadArrowItem)((FrameworkElement)sender).Tag;
            arrow.EndX = arrow.EndX + e.HorizontalChange;
            arrow.EndY = arrow.EndY + e.VerticalChange;
        }

        private void OnArrowDragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            padsController.DisplayPad.ScratchPadItemsHaveMoved();
        }

        private void OnMouseEnterArrow(object sender, MouseEventArgs e)
        {
            ScratchPadArrowItem arrow = (ScratchPadArrowItem)((FrameworkElement)sender).Tag;
            arrow.IsMouseOver = true;
        }

        private void OnMouseLeaveArrow(object sender, MouseEventArgs e)
        {
            ScratchPadArrowItem arrow = (ScratchPadArrowItem)((FrameworkElement)sender).Tag;
            arrow.IsMouseOver = false;
        }

        private void TextItemLoaded(object sender, RoutedEventArgs e)
        {
            FrameworkElement textBox = (FrameworkElement)sender;
            textBox.Focus();
        }

        private void OnDeleteTextItem(object sender, RoutedEventArgs e)
        {
            ScratchPadTextItem text = (ScratchPadTextItem)((FrameworkElement)sender).Tag;
            padsController.DisplayPad.RemoveTextItem(text);
        }

        private void OnScaleMouseDown(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement s = (FrameworkElement)sender;
            s.CaptureMouse();
            drag = e.GetPosition(scratchItemsList);
            e.Handled = true;
        }

        private void OnScaleDrag(object sender, MouseEventArgs e)
        {
            if (drag != null)
            {
                ScratchPadTextItem textItem = (ScratchPadTextItem)((FrameworkElement)sender).Tag;

                FrameworkElement s = (FrameworkElement)sender;
                Point pos = e.GetPosition(scratchItemsList);
                double delta = pos.Y - drag.Value.Y;
                double scale = textItem.Scale + delta * -0.03;
                scale = Math.Max(1, Math.Min(4, scale));
                textItem.Scale = scale;
                drag = pos;
            }
        }
        
        private void OnPanDrag(object sender, MouseEventArgs e)
        {
            if (drag != null)
            {
                ScratchPadTextItem textItem = (ScratchPadTextItem)((FrameworkElement)sender).Tag;

                FrameworkElement s = (FrameworkElement)sender;
                Point pos = e.GetPosition(scratchItemsList);
                double deltaY = pos.Y - drag.Value.Y;
                double deltaX = pos.X - drag.Value.X;
                textItem.TranslateX += deltaX;
                textItem.TranslateY += deltaY;
                drag = pos;
            }
        }

        private void OnScaleMouseUp(object sender, MouseButtonEventArgs e)
        {
            ((FrameworkElement)sender).ReleaseMouseCapture();
            drag = null;
            padsController.DisplayPad.ScratchPadItemsHaveMoved();
        }

        private void OnMouseEnterSomething(object sender, MouseEventArgs e)
        {
            focus.Focus();
            Keyboard.Focus(focus);
        }

        private void OnMouseEnterChromosome(object sender, MouseEventArgs e)
        {
            focus.Focus();
            Keyboard.Focus(focus);

            ScratchPadChromosomeItem chrom = (ScratchPadChromosomeItem)((FrameworkElement)sender).Tag;
            chrom.Chrom.IsMouseOver = true;
        }

        private void OnMouseLeaveChromosome(object sender, MouseEventArgs e)
        {
            ScratchPadChromosomeItem chrom = (ScratchPadChromosomeItem)((FrameworkElement)sender).Tag;
            chrom.Chrom.IsMouseOver = false;
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            saving.Visibility = Visibility.Visible;
            Connect.ConnectionTest.Test(this);

            ThreadPool.QueueUserWorkItem((WaitCallback)delegate(object o)
            {
                padsController.SaveAll();

                Dispatcher.Invoke((ThreadStart)delegate
                {
                    Close();

                }, DispatcherPriority.Normal);
            }, null); 
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnPrint(object sender, RoutedEventArgs e)
        {
            PrintDialog printer = new PrintDialog();
            IsHitTestVisible = false;
            
            if ((bool)printer.ShowDialog().GetValueOrDefault())
            {
                Connect.ConnectionTest.Test(this);

                double aspect = scratchBorder.ActualHeight / scratchBorder.ActualWidth;

                RenderTargetBitmap render = new RenderTargetBitmap(
                   (int)(scratchBorder.ActualWidth),
                   (int)(scratchBorder.ActualHeight),
                   96, 96, PixelFormats.Pbgra32);

                render.Render(scratchBorder);

                CroppedBitmap bmp = new CroppedBitmap(render, new Int32Rect(0, 0, render.PixelWidth, render.PixelHeight));
                Image img = new Image();
                img.Source = bmp;

                TextBlock text = new TextBlock();
                text.FontSize = 18;
                text.FontWeight = FontWeights.Bold;
                text.Text = string.Format(CultureInfo.CurrentCulture, "{0}", padsController.CaseName);
                text.Margin = new Thickness(0, 0, 0, 20);
                DockPanel.SetDock(text, Dock.Left);

                TextBlock dateText = new TextBlock();
                dateText.FontSize = 18;
                dateText.FontWeight = FontWeights.Bold;
                dateText.Text = DateTime.Now.ToLongDateString();
                dateText.Margin = new Thickness(0, 0, 0, 20);
                DockPanel.SetDock(dateText, Dock.Right);

                DockPanel topPanel = new DockPanel();
                topPanel.Children.Add(dateText);
                topPanel.Children.Add(text);
                DockPanel.SetDock(topPanel, Dock.Top);

                DockPanel dock = new DockPanel();
                dock.Children.Add(topPanel);
                dock.Children.Add(img);
                dock.Margin = new Thickness(50);
                dock.Width = printer.PrintableAreaWidth - (dock.Margin.Left + dock.Margin.Right);
                dock.Height = dock.Width * aspect;

                dock.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                dock.Arrange(new Rect(new Point(0, 0), dock.DesiredSize));

                printer.PrintVisual(dock, "Scratch Pad Print");
            }
            IsHitTestVisible = true;
        }

        private void OnExport(object sender, RoutedEventArgs e)
        {
            RenderTargetBitmap image = GenerateExportBitmap();
            ImageRendererExport.SaveAs(image);
        }

        private RenderTargetBitmap GenerateExportBitmap()
        {
            RenderTargetBitmap render = new RenderTargetBitmap(
                   (int)(scratchBorder.ActualWidth),
                   (int)(scratchBorder.ActualHeight),
                   96, 96, PixelFormats.Pbgra32);

            Border background = new Border();
            background.Background = Brushes.White;
            background.Width = scratchBorder.ActualWidth;
            background.Height = scratchBorder.ActualHeight;
            background.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            background.Arrange(new Rect(new Point(0, 0), background.DesiredSize));

            render.Render(background);
            render.Render(scratchBorder);
            return render;
        }

        private void OnCopyToClipboard(object sender, RoutedEventArgs e)
        {
            RenderTargetBitmap image = GenerateExportBitmap();
            ImageRendererExport.CopyToClipboard(image);
        }

        private void CanPasteExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);

        private void PasteExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var pad = padsController.DisplayPad;

            if (Clipboard.ContainsImage())
            {
                // Workaround known bug
                System.Drawing.Bitmap img = (System.Drawing.Bitmap)System.Windows.Forms.Clipboard.GetImage();
                IntPtr hBitmap = img.GetHbitmap();
                BitmapSource image = CreateBitmap(hBitmap);
                DeleteObject(hBitmap);

                image.Freeze();
                
                ItemFromImage(pad, image);
            }
            else if (Clipboard.ContainsText())
            {
                pad.AddTextItem(new Point(10, 10), Clipboard.GetText());
            }
        }

        internal BitmapSource CreateBitmap(IntPtr handle)
        {
            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                handle, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
        }


        private static void ItemFromImage(ScratchPad pad, BitmapSource img)
        {
            pad.AddImageItem(new ScratchPadImageItem
            {
                Id = Guid.NewGuid(),
                Image = img,
                TranslateX = 50,
                TranslateY = 50
            });
        }

        private void CanExecuteTrue(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void AddChromsExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            string chromTag = (string)e.Parameter;

            addStuffPopup.IsOpen = false;
            Connect.ConnectionTest.Test(this);

            if (addKaryotypedOrNumbered == AddType.Karyotped)
            {
                padsController.AddKaryotypeChromosomes(chromTag);
            }
            else
            {
                MultiCutWindow w = new MultiCutWindow(controller, chromTag);
                w.Owner = this;
                var result = w.ShowDialog();

                if (result.GetValueOrDefault(false))
                {
                    Connect.ConnectionTest.Test(this);

                    ScratchPad pad = padsController.DisplayPad;

                    var chromsToAdd = from chrom in w.Cutouts
                                      select new ScratchPadChromosomeItem
                                      {
                                          Chrom = chrom.Cutout,
                                          Id = Guid.NewGuid(),
                                          Info = new ChromInfo
                                          {
                                              Cell = chrom.Cell.Name,
                                              CellId = chrom.Cell.Id,
                                              Identifier = chromTag,
                                              Slide = chrom.Cell.Slide.Name
                                          }
                                      };

                    pad.AddChromosomes(chromsToAdd);
                }
            }
        }

        private void OnFlipX(object sender, RoutedEventArgs e)
        {
            padsController.DisplayPad.FlipXForCurrentChrom();
            chromContextMenu.IsOpen = false;
        }

        private void OnDeleteChromFromContextMenu(object sender, RoutedEventArgs e)
        {
            padsController.DisplayPad.DeleteCurrentChrom();
            chromContextMenu.IsOpen = false;
        }

        private void OnDeleteArrowItem(object sender, RoutedEventArgs e)
        {
            ScratchPadArrowItem item = (ScratchPadArrowItem)arrowContextMenu.Tag;
            if (item != null)
            {
                padsController.DisplayPad.RemoveArrow(item);
            }
            arrowContextMenu.IsOpen = false;
            arrowContextMenu.Tag = null;
        }

        private void OnDeleteImageItem(object sender, RoutedEventArgs e)
        {
            ScratchPadImageItem image = (ScratchPadImageItem)((FrameworkElement)sender).Tag;
            padsController.DisplayPad.RemoveImageItem(image);
        }

        private void OnPanDragImage(object sender, MouseEventArgs e)
        {
            if (drag != null)
            {
                ScratchPadImageItem imageItem = (ScratchPadImageItem)((FrameworkElement)sender).Tag;

                FrameworkElement s = (FrameworkElement)sender;
                Point pos = e.GetPosition(scratchItemsList);
                double deltaY = pos.Y - drag.Value.Y;
                double deltaX = pos.X - drag.Value.X;
                imageItem.TranslateX += deltaX;
                imageItem.TranslateY += deltaY;
                drag = pos;
            }
        }

        private void OnScaleDragImage(object sender, MouseEventArgs e)
        {
            if (drag != null)
            {
                ScratchPadImageItem imageItem = (ScratchPadImageItem)((FrameworkElement)sender).Tag;

                FrameworkElement s = (FrameworkElement)sender;
                Point pos = e.GetPosition(scratchItemsList);
                double delta = pos.Y - drag.Value.Y;
                double scale = imageItem.Scale + delta * -0.03;
                scale = Math.Max(0.25, Math.Min(4, scale));
                imageItem.Scale = scale;
                drag = pos;
            }
        }

        private void OnAddIdeogram(object sender, RoutedEventArgs e)
        {
            var exe = new FileInfo("IdeogramEditor.exe");

#if(DEBUG)
            exe = new FileInfo("..\\..\\..\\IdeogramEditor\\PreBaked\\IdeogramEditor.exe");
#endif

            Process p = new Process();
            p.StartInfo = new ProcessStartInfo(exe.FullName)
            {
                WorkingDirectory = exe.Directory.FullName
            };
            p.Start();
        }
    }

    public class MaxTwoDoublesConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[1] == DependencyProperty.UnsetValue)
                return 0;

            double d1 = (double)values[0];
            double d2 = (double)values[1];

            return Math.Max(d1, d2);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class TextBorderVisible : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool a = values[0] == DependencyProperty.UnsetValue ? false : (bool)values[0];
            bool b = values[1] == DependencyProperty.UnsetValue ? false : (bool)values[1];
            bool c = values[2] == DependencyProperty.UnsetValue ? false : (bool)values[2];

            return (a || b || c) ? Visibility.Visible : Visibility.Hidden;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
