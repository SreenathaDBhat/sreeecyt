﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Effects;
using System.Windows.Media;
using System.Windows;

namespace AI
{
    public class ThresholdVisualizer : ShaderEffect
    {
        public static readonly DependencyProperty InputProperty = ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(ThresholdVisualizer), 0);
        public static readonly DependencyProperty ThresholdProperty = DependencyProperty.Register("Threshold", typeof(double), typeof(ThresholdVisualizer), new UIPropertyMetadata(0.7, PixelShaderConstantCallback(0)));

        public ThresholdVisualizer()
        {
            PixelShader = new PixelShader() { UriSource = new Uri(@"pack://application:,,,/BrightfieldCaseView;component/ThresholdVisualizer.ps") };
            UpdateShaderValue(InputProperty);
        }

        public Brush Input
        {
            get { return (Brush)GetValue(InputProperty); }
            set { SetValue(InputProperty, value); }
        }

        public double Threshold
        {
            get { return (double)GetValue(ThresholdProperty); }
            set { SetValue(ThresholdProperty, value); }
        }
    }
}
