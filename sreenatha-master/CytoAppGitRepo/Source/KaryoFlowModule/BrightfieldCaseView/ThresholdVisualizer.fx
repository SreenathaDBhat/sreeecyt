sampler2D implicitInput : register(s0);
float thresh: register(c0);

float4 main(float2 uv : TEXCOORD) : COLOR
{
    float4 c = tex2D(implicitInput, uv);

    if(c.r < thresh)
    {
        c.r = 1;
	c.gb = 0;
    }

    c.a = 1;

    return c;
}