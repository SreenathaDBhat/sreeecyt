﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.Brightfield
{
    public class Totals : Notifier
    {
        public Totals()
        {
        }

        public int Reds { get; private set; }
        public int Greens { get; private set; }
        public int Blues { get; private set; }
        public int Whites { get; private set; }
        public int Counted { get; private set; }
        public int Numbered { get; private set; }
        public int Karyotyped { get; private set; }
        public int Processed { get; private set; }

        public void Refresh(IEnumerable<Cell> cells)
        {
            Reds = 0;
            Greens = 0;
            Blues = 0;
            Whites = 0;
            Counted = 0;
            Numbered = 0;
            Karyotyped = 0;
            Processed = 0;

            if (cells != null)
            {
                foreach (var c in cells)
                {
                    if (c.IsCounted) Counted++;
                    if (c.IsNumbered) Numbered++;
                    if (c.IsKaryotyped) Karyotyped++;
                    if (c.Color.Index == 0) Greens++;
                    if (c.Color.Index == 1) Reds++;
                    if (c.Color.Index == 2) Blues++;
                    if (c.Color.Index == 3) Whites++;
                    if (c.IsCounted || c.IsNumbered || c.IsKaryotyped) Processed++;
                }
            }

            Notify("Reds", "Whites", "Greens", "Blues", "Counted", "Numbered", "Karyotyped", "Processed");
        }
    }
}
