﻿using System;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows;
using System.Windows.Threading;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Media.Effects;
using AI.Connect;
using System.Xml.Linq;


namespace AI.Brightfield
{
    public class ToolKeyEventArgs : EventArgs
    {
        private ToolKeyEventArgs() { }

        public Point MousePositionRelativeToImage { get; set; }
        public Key Key { get; set; }
        public bool Handled { get; set; }

        public static ToolKeyEventArgs Build(Key k, Point mousePos)
        {
            return new ToolKeyEventArgs
            {
                Key = k,
                MousePositionRelativeToImage = mousePos,
                Handled = false
            };
        }
    }

    public interface IBrightfieldWorkflowTool : INotifyPropertyChanged
    {
        void Load(Cell cell, WorkflowController controller);
        void Save();

        UndoList UndoList { get; }

        /// <summary>
        /// Placed inside the main StretchOrSizeCanvas housing the main image.
        /// </summary>
        FrameworkElement ImageUi { get; }
        StretchSizeMode ImageUiSizeMode { get; }
        bool ShowMainImage { get; }
        bool IsDisplayPopupEnabled { get; }

        
        /// <summary>
        /// Will cover the working area of the screen, including the image.
        /// Put toolbar stuff on the bottom left to avoid overlaps
        /// </summary>
        FrameworkElement ScreenUi { get; }

        /// <summary>
        /// 
        /// </summary>
        FrameworkElement ToolbarUi { get; }

        void OnKeyDown(ToolKeyEventArgs e);
        void OnKeyUp(ToolKeyEventArgs e);

        /// <summary>
        /// Called whenever the main image display settings are changed
        /// </summary>
        void OnDispayChanged();

        /// <summary>
        /// If true, the UI will wait for the full fidelity image to be pulled before instantiating the tool.
        /// </summary>
        bool RequiresFullImage { get; }

        BitmapSource PrintImage { get; }
    }

    public static class WorkflowCommands
    {
        public static readonly RoutedUICommand NextCell = new RoutedUICommand("NextCell", "NextCell", typeof(WorkflowCommands));
        public static readonly RoutedUICommand PreviousCell = new RoutedUICommand("PrevCell", "PrevCell", typeof(WorkflowCommands));

        public static readonly RoutedUICommand Count = new RoutedUICommand("Count", "Count", typeof(WorkflowCommands));
        public static readonly RoutedUICommand Number = new RoutedUICommand("Number", "Number", typeof(WorkflowCommands));
        public static readonly RoutedUICommand Karyotype = new RoutedUICommand("Karyotype", "Karyotype", typeof(WorkflowCommands));

        public static readonly RoutedUICommand Organize = new RoutedUICommand("Organize", "Organize", typeof(WorkflowCommands));
        public static readonly RoutedUICommand Report = new RoutedUICommand("Report", "Report", typeof(WorkflowCommands));

        public static readonly RoutedUICommand Revert = new RoutedUICommand("Revert", "Revert", typeof(WorkflowCommands));
        public static readonly RoutedUICommand ToggleAnnotations = new RoutedUICommand("ToggleAnnotations", "ToggleAnnotations", typeof(WorkflowCommands));
    }




    public class WorkflowController : Notifier, MetaData.IMetaDataContainer
    {
        private Case limsCase;
        private Guid caseId;
        private string caseStatus;
        private Dispatcher uiThread;
        private List<Cell> allCells;
        private ObservableCollection<Slide> allSlides;
        private Cell currentCell;
        private Totals totals;
        private CellVisibility cellVisibility;
        private StringTable stringTable;
        private SimplePersistance persistants;
        private ImageRenderer renderer;
        private ImageParameterRememberer displayOptions;
        private Window forParentStuff;

        private MetaData.MetaData caseMetaData;
        private MetaData.AvailableFields availableCaseFields;
        private MetaData.AvailableFields availableCellFields;
        private MetaData.MetaDataTemplate caseDetailsTemplate;

        public string RelativeIdCell { get; private set; }

        public event EventHandler ToolChanged;

        public WorkflowController(Dispatcher uiThread, Case limsCase, Window forParentStuff)
        {
            this.forParentStuff = forParentStuff;
            stringTable = StringTable.Empty;
            persistants = new SimplePersistance();

            this.limsCase = limsCase;
            this.uiThread = uiThread;

            caseMetaData = new MetaData.MetaData();
            toolCreator = () => new DefaultToolController();

            //using (var lims = Connect.LIMS.New())
            //{                
            //    using (var db = new Connect.Database())
            //    {
                    //var caseInfo = db.Cases.Where(c => c.limsRef == limsCase.Id).First();
                    caseId = Guid.Empty;
                    //caseStatus = caseInfo.status;

                    //var limsMetaData = lims.GetCaseMetaData(limsCase);
                    //caseMetaData.AddRange(from m in limsMetaData
                    //                      orderby m.Field
                    //                      select new MetaData.FieldValuePair
                    //                      {
                    //                          Field = m.Field,
                    //                          Value = m.Value
                    //                      });

                    availableCaseFields = new MetaData.AvailableFields(null); //new AI.MetaData.AvailableFields(AI.MetaData.CaseFields.FieldNameFetcher);
                    availableCellFields = new MetaData.AvailableFields(null);// new AI.MetaData.AvailableFields(AI.MetaData.CellFields.FieldNameFetcher);

                    //var defaults = from id in AI.MetaData.CellFields.DefaultFieldsStringTableIds
                    //               select stringTable.Lookup(id);
                    //defaults = from d in defaults
                    //           orderby d
                    //           select d;

                    //availableCellFields.Include(defaults);

                    //availableCaseFields.Refresh();
                    //availableCellFields.Refresh();

                    caseDetailsTemplate = null;// AI.MetaData.MetaDataTemplate.LoadTemplate(TemplateFile.FullName);

                    //LoadCellInfo(db, lims);
                    LoadCellInfo();

                    //GetCaseStatuses(db);
                    GetCaseStatuses(null);
            //    }
            //}

            renderer = new ImageRenderer();

            displayOptions = new ImageParameterRememberer(caseId);
            displayOptions.Load();

            cellVisibility = new CellVisibility(allSlides);

            if (allCells.Count > 0)
            {
                SetCurrentCell(allCells.First());
                LoadThumbnails();
            }

            WorkingCells = cellVisibility.FilterCells(AllCells);

            totals = new Totals();
            totals.Refresh(WorkingCells);
        }

        public DependencyObject ImageControl { get; set; }

        
        private void GetCaseStatuses(AI.Connect.Database db)
        {
            //var existing = (from c in db.Cases
            //                select c.status).Distinct();

            string[] preDefined = new string[]
                        {
                            stringTable.Lookup("CaseStatus_Unassigned"),
                            stringTable.Lookup("CaseStatus_InProgress"),
                            stringTable.Lookup("CaseStatus_ForReview"),
                            stringTable.Lookup("CaseStatus_Completed")
                        };

            var statuses = new List<string>();
//            statuses.AddRange(existing);
            statuses.AddRange(preDefined);
            CaseStatuses = statuses.Distinct().ToArray();
        }

        public ImageRenderer Renderer
        {
            get { return renderer; }
        }

        public StringTable StringTable
        {
            get { return stringTable; }
            set { stringTable = value; }
        }

        public SimplePersistance Persistants
        {
            get { return persistants; }
        }

        public Case Case
        {
            get { return limsCase; }
        }

        public Guid CaseId
        {
            get { return caseId; }
        }

        public Guid Id
        {
            get { return caseId; }
        }

        public string CaseName { get { return limsCase.Name; } }

        public string CaseStatus
        {
            get { return GetCaseStatus(); }
            set { SetCaseStatus(value); Notify("CaseStatus"); }
        }

        public IEnumerable<string> CaseStatuses { get; private set; }

        private void SetCaseStatus(string value)
        {
            using (var db = new Connect.Database())
            {
                caseStatus = value;

                var dbCase = db.Cases.Where(c => c.caseId == caseId).First();
                dbCase.status = caseStatus;
                db.SubmitChanges();
            }
        }

        private string GetCaseStatus()
        {
            return caseStatus;
        }

        public MetaData.MetaData MetaData { get { return caseMetaData; } }
        public bool AreTemplatesSupported { get { return true; } }
        public MetaData.MetaDataTemplate Template { get { return caseDetailsTemplate; } set { caseDetailsTemplate = value; Notify("Template"); } }
        public FileInfo TemplateFile { get { return AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\CaseMetaDataTemplate.xml"); } }

        private IBrightfieldWorkflowTool tool;
        public IBrightfieldWorkflowTool Tool
        {
            get { return tool; }
            private set
            {
                tool = value;
                if (ToolChanged != null)
                    ToolChanged(this, EventArgs.Empty);
                
                Notify("Tool");
            }
        }

        public delegate IBrightfieldWorkflowTool CreateToolDelegate();
        public delegate bool WaitForHiFiImageDelegate();
        private CreateToolDelegate toolCreator;

        public void SetTool(CreateToolDelegate creator)
        {
            Connect.ConnectionTest.Test(forParentStuff);

            toolCreator = creator;

            if (Tool != null && (Tool.UndoList == null || Tool.UndoList.HasContent))
                SaveTool();

            if (Tool != null && Tool is IClosing)
                ((IClosing)Tool).Closed();

            Tool = null;

            CreateTool();
        }

        public void RevertTool()
        {
            if(toolCreator != null)
                CreateTool();
        }

        private void CreateTool()
        {
            IBrightfieldWorkflowTool tool = toolCreator();
            tool.Load(currentCell, this);

            Tool = tool;
            Notify("Tool");
        }

        public Cell CurrentCell
        {
            get { return currentCell; }
            set { SetCurrentCell(value); }
        }

        public ImageFrame CurrentCellImage
        {
            get;
            private set;
        }

        public void SaveStuff()
        {
            if(CurrentCellImage != null)
                displayOptions.Remember(CurrentCellImage, renderer);

            displayOptions.Save();
        }

        public void SetCurrentCell(Cell newCell)
        {
            //Connect.ConnectionTest.Test(forParentStuff);
            
            // If we're in an analysis mode and delete all the cells, we temporarily
            // revert back to the default tool.
            var idealTool = Tool;

            if (Tool is DefaultToolController && toolCreator != null)
                idealTool = toolCreator();

            bool bounceTool = idealTool != null;

            if (bounceTool)
            {
                if (Tool.UndoList != null && Tool.UndoList.HasContent)
                    SaveTool();

                Tool = null;
            }

            currentCell = newCell;
            Notify("CurrentCell", "IndexOfCurrentCell");

            // FolderDataSource is basically a flat folder
            if (currentCell != null)
                RelativeIdCell = CaseName + ";" + currentCell.SlideId.ToString() + ";" + currentCell.Id.ToString();
            else
                RelativeIdCell = null;


            if (CurrentCellImage != null)
            {
                displayOptions.Remember(CurrentCellImage, renderer);
                CurrentCellImage.DumpAllImageData();
            }

            var relativeIdSlide = CaseName + ";" + currentCell.SlideId.ToString();
            var dataSource = DataSourceFactory.GetDataSource();

            CurrentCellImage = newCell == null ? null : ImageFrameDbLoad.FromDataSource(dataSource, relativeIdSlide, newCell.ImageID);
            if (CurrentCellImage != null)
            {
                foreach (var snap in CurrentCellImage.AllSnaps)
                {
                    //var stream = BlobStream.ReadBlobStream(caseId, snap.ImageDataFile, null, CacheOption.DontCache);
                    //snap.ChannelPixels = ImageFromBitmapSource.LoadFromDisk(caseId, snap.ImageDataFile, db); // WH Remove
                    using (var channelStream = dataSource.LoadStream(relativeIdSlide, snap.ImageDataFile)) 
                    {
                        snap.ChannelPixels = ImageFromBitmapSource.LoadFromStream(channelStream);
                    }
                }

                renderer.Image = CurrentCellImage;
                displayOptions.Apply(CurrentCellImage, renderer);
            }
                
            Notify("CurrentCellImage");

            if (newCell == null)
            {
                var defTool = new DefaultToolController();
                defTool.Load(null, this);
                Tool = defTool;
                Notify("Tool");
            }
            else if (bounceTool)
            {
                CreateTool();
            }

            if (Renderer != null)
            {
                if (ImageControl != null)
                {
                    var canvas = VisualTreeWalker.FindParentOfType<StretchOrSizeCanvas>(ImageControl);
                    if (canvas != null)
                        canvas.ResetZoom();
                }
            }
        }

        public IEnumerable<Slide> Slides
        {
            get { return allSlides; }
        }

        public String CurrentDateTime
        {
            get { return DateTime.Now.ToShortDateString(); }
        }

        public IEnumerable<Cell> AllCells
        {
            get { return allCells; }
        }

        private List<Cell> workingCells;
        public IEnumerable<Cell> WorkingCells
        {
            get { return workingCells; }
            set { workingCells = value.ToList(); Notify("TotalCells", "IndexOfCurrentCell"); }
        }

        public Slide CreateNewSlide(string slideName)
        {
            var slide = CaseManagement.CreateSlide(Case, slideName, "", SlideTypes.Metaphase);
            allSlides.Add(slide);
            return slide;
        }

        #region Implementation Detail

        private Guid GetImageIdsXml(XElement frames, Guid groupId, ImageType type)
        {
            if (frames == null)
                return Guid.Empty;

            var g = (from f in frames.Descendants("ImageFrame")
                     where f.Attribute("groupId").Value == groupId.ToString() && f.Attribute("imageType").Value == ((int)type).ToString()
                     select Guid.Parse(f.Attribute("blobId").Value)).FirstOrDefault();
            return g;
        }

        private void LoadCellInfo()
        {
            var dataSource = DataSourceFactory.GetDataSource();
            //var allCellData = db.CaseCells.Where(c => c.caseId == caseId).OrderBy(c => c.cellName).ToArray();
            //var allCellMetaData = (from m in db.CaseCellMetaDatas
            //                       where m.caseId == caseId
            //                       orderby m.field ascending
            //                       select new
            //                       {
            //                           Field = m.field,
            //                           Value = m.value,
            //                           Cell = m.cellId

            //                       }).ToArray();

            allSlides = new ObservableCollection<Slide>();

            allCells = new List<Cell>();
           
            //var tubsDbSlides = (from s in db.Slides
            //                    where s.caseId == caseId
            //                    select s).ToList();


            // kinda funky with the frames at the slide level????
            // HACK need to use DataSource to naviagte case but to get us going HACK
//            var caseFolder = new DirectoryInfo(Path.Combine(DataSourceSettings.Location, CaseName));

            var slides = dataSource.GetSlides(CaseName);

            foreach (var s in slides)
            {
                allSlides.Add(s);

                var relativeSlideId = CaseName +";"+ s.Id;

                var iFramesXml = dataSource.LoadXml(relativeSlideId, "images.imageframes");

                var cellIds = dataSource.GetNodes(relativeSlideId);
                foreach (var c in cellIds)
                {
                    var relativeCellId = relativeSlideId + ";" + c;
                    var cellStatusXml = dataSource.LoadXml(relativeCellId, "cell.cellstatus");
                    if (cellStatusXml != null)
                    {
                        var cell = Cell.FromXml(cellStatusXml);

                        cell.MetImageID = GetImageIdsXml(iFramesXml, cell.ImageGroupId, ImageType.Processed);
                        cell.RawImageID = GetImageIdsXml(iFramesXml, cell.ImageGroupId, ImageType.Raw);
                        allCells.Add(cell);
                    }
                }
            }

            //var caseImages = (from i in db.CaseImageFrames
            //                  where i.caseId == caseId
            //                  select new
            //                  {
            //                      Image = i.imageFrameId,
            //                      Group = i.groupId,
            //                      Type = i.imageType
            //                  }).ToArray();

            //foreach (var c in allCells)
            //{
            //    var tubsSlide = tubsDbSlides.Where(s => s.slideId == c.SlideId).First();
            //    c.Slide = allSlides.Where(s => s.Id == tubsSlide.limsRef).First();

            //    var cellImages = from i in caseImages
            //                     where i.Group == c.ImageGroupId
            //                     select i;

            //    var metaphase = cellImages.Where(i => i.Type == (int)ImageType.Processed).FirstOrDefault();
            //    c.MetImageID = metaphase != null  ?  metaphase.Image : Guid.Empty;

            //    var raw = cellImages.Where(i => i.Type == (int)ImageType.Raw).FirstOrDefault();
            //    c.RawImageID = raw != null  ?  raw.Image : Guid.Empty;

            //    c.MetaData.AddRange(
            //        from m in allCellMetaData
            //        where m.Cell == c.Id
            //        select new MetaData.FieldValuePair
            //        {
            //            Field = m.Field,
            //            Value = m.Value
            //        });
            //}
        }

        private void LoadThumbnails()
        {
            Task.Factory.StartNew(delegate(object state)
            {
                foreach (var c in allCells)
                {
                    if (c.ImageID == Guid.Empty)
                        continue;

                    LoadThumbnail(c);
                }
            }, null);

        }

        private void LoadThumbnail(Cell c)
        {
            var relativeSlideId = CaseName + ";" + c.SlideId;
            var relativeCellId = relativeSlideId + ";" + c.Id;
            var dataSource = DataSourceFactory.GetDataSource();

            c.MetaphaseThumbnailPath = c.ImageID.ToString() + ".jpg";
            c.KaryotypeThumbnailPath = c.Id.ToString() + ".kar.jpg";
            using (var mstream = dataSource.LoadStream(relativeSlideId, c.MetaphaseThumbnailPath)) // this is in the slide folder, I wonder why?
            using (var kstream = dataSource.LoadStream(relativeCellId, c.KaryotypeThumbnailPath))
            {
                // NOTE: for http, we should reduce size on the server ****
                var bitmap = ImageFromBitmapSource.LoadBitmapImageFromStream(250, mstream);
                var karBitmap = ImageFromBitmapSource.LoadBitmapImageFromStream(250, kstream);

                uiThread.BeginInvoke((WaitCallback)delegate(object s)
                {
                    ((Cell)s).MetaphaseThumbnail = bitmap;
                    ((Cell)s).KaryotypeThumbnail = karBitmap;

                }, DispatcherPriority.Normal, new object[] { c });
            }
        }

        #endregion

        public bool CanNextCellExecute()
        {
            return workingCells.IndexOf(currentCell) < workingCells.Count - 1;
        }

        public void NextCellExecuted()
        {
            SetCurrentCell(workingCells[workingCells.IndexOf(currentCell) + 1]);
        }

        public bool CanPreviousCellExecute()
        {
            return workingCells.IndexOf(currentCell) > 0;
        }

        public void PreviousCellExecuted()
        {
            SetCurrentCell(workingCells[workingCells.IndexOf(currentCell) - 1]);
        }

        public bool CanUndoExecute()
        {
            return Tool != null && Tool.UndoList != null && Tool.UndoList.HasContent;
        }

        public void UndoExecuted()
        {
            Tool.UndoList.Undo(null);
        }

        public void SetCounted(Cell cell, bool counted)
        {
            cell.IsCounted = counted;
            totals.Refresh(WorkingCells);
        }

        public void SetNumbered(Cell cell, bool numbered)
        {
            cell.IsNumbered = numbered;
            totals.Refresh(WorkingCells);
        }

        public void SetKaryotyped(Cell cell, bool karyotyped)
        {
            cell.IsKaryotyped = karyotyped;
            totals.Refresh(WorkingCells);
        }

        public Totals Totals
        {
            get { return totals; }
        }

        public CellVisibility CellVisibility
        {
            get { return cellVisibility; }
        }

        public void SetCellColor(Cell cell, CellColor cellColor)
        {
            SetCellColor(new Cell[] { cell }, cellColor);
        }

        public void SetCellColor(IEnumerable<Cell> cells, CellColor cellColor)
        {
            using (var db = new Connect.Database())
            {
                foreach (var c in cells)
                {
                    var dbCell = db.Cells.Where(x => x.cellId == c.Id).FirstOrDefault();
                    if (dbCell != null)
                    {
                        c.CellColorIndex = cellColor.Index;
                        dbCell.color = cellColor.Index;
                    }
                }

                db.SubmitChanges();
            }

            totals.Refresh(allCells);
        }

        public void LoadFullsizeThumb(Cell c, Dispatcher dispatcher)
        {
            ThreadPool.QueueUserWorkItem(delegate
            {
                var relativeSlideId = CaseName + ";" + c.SlideId;
                var relativeCellId = relativeSlideId + ";" + c.Id;
                var dataSource = DataSourceFactory.GetDataSource();

                c.MetaphaseThumbnailPath = c.ImageID.ToString() + ".jpg";
                c.KaryotypeThumbnailPath = c.Id.ToString() + ".kar.jpg";
                using (var mstream = dataSource.LoadStream(relativeSlideId, c.MetaphaseThumbnailPath))
                using (var kstream = dataSource.LoadStream(relativeCellId, c.KaryotypeThumbnailPath))
                {
                    var bitmap = ImageFromBitmapSource.LoadBitmapImageFromStream(mstream);
                    var karBitmap = ImageFromBitmapSource.LoadBitmapImageFromStream(kstream);

                    uiThread.BeginInvoke((WaitCallback)delegate(object s)
                    {
                        ((Cell)s).MetaphaseThumbnail = bitmap;
                        ((Cell)s).KaryotypeThumbnail = karBitmap;

                    }, DispatcherPriority.Normal, new object[] { c });
                }
            });
        }

        public void OnKeyDown(KeyEventArgs e, Point mousePos)
        {
            var k = ToolKeyEventArgs.Build(e.Key, mousePos);
            if (Tool != null)
                Tool.OnKeyDown(k);
            e.Handled = k.Handled;
        }

        public void OnKeyUp(KeyEventArgs e, Point mousePos)
        {
            var k = ToolKeyEventArgs.Build(e.Key, mousePos);
            if (Tool != null)
                Tool.OnKeyUp(k);
            e.Handled = k.Handled;
        }

        public int TotalCells
        {
            get { return workingCells.Count; }
        }

        public string IndexOfCurrentCell
        {
            get 
            { 
                var i = workingCells.IndexOf(currentCell) + 1;
                if (i <= 0)
                    return "???";
                else
                    return i.ToString();
            }
        }


        public static int CreateCellsForFrames(Guid caseId)
        {
            var db = new Connect.Database();

            var rawframeIds = from f in db.CaseImageFrames
                              where (f.caseId == caseId && f.imageType == (int)ImageType.Raw)
                              select f.imageFrameId;

            int cellCount = db.CaseCells.Where(c => c.caseId == caseId).Count();
            int frameCount = rawframeIds.Count();

            if (cellCount >= frameCount)
                return cellCount;

            int i = cellCount;

            foreach (var id in rawframeIds)
            {
                var frame = db.ImageFrames.Where(f => f.imageFrameId == id).First();

                if (db.CaseCells.Count(c => c.caseId == caseId && c.imageGroup == frame.groupId) > 0)
                    continue;

                var cell = new Connect.Cell
                {
                    analysed = false,
                    cellId = Guid.NewGuid(),
                    cellName = frame.location == "n/a" ? (++i).ToString() : frame.location,
                    color = CellColor.White.Index,
                    counted = false,
                    karyotyped = false,
                    slideId = frame.slideId,
                    imageGroup = frame.groupId
                };
                db.Cells.InsertOnSubmit(cell);
            }

            db.SubmitChanges();

            return db.CaseCells.Count(c => c.caseId == caseId);
        }

        public void NewCellCaptured(Cell cell)
        {
            NewCellsCaptured(new Cell[] { cell });
        }

        public void NewCellsCaptured(IEnumerable<Cell> newCells)
        {
            NewCellsCaptured(newCells, true);
        }

        public void NewCellsCaptured(IEnumerable<Cell> newCells, bool switchTo)
        {
            allCells.AddRange(newCells);
            WorkingCells = cellVisibility.FilterCells(AllCells);
            totals.Refresh(workingCells);

            if (switchTo)
            {
                var sel = newCells.LastOrDefault();
                if (sel != null)
                    SetCurrentCell(sel);
                else // no new cells prolly means added fuses to image. refresh anyway.
                    SetCurrentCell(currentCell);
            }
        }

        public static Cell ParseCell(AI.Connect.Cell c)
        {
            return new Cell(c.cellId)
            {
                CellColorIndex = c.color,
                IsCounted = c.counted,
                IsKaryotyped = c.karyotyped,
                Name = c.cellName,
                IsNumbered = c.analysed,
                ImageGroupId = c.imageGroup,
                //SlideId = c.slideId
                SlideId = c.slideId.ToString()
            };
        }
        public static Cell ParseCell(AI.Connect.CaseCell c)
        {
            return new Cell(c.cellId)
            {
                CellColorIndex = c.color,
                IsCounted = c.counted,
                IsKaryotyped = c.karyotyped,
                Name = c.cellName,
                IsNumbered = c.analysed,
                ImageGroupId = c.imageGroup,
//                SlideId = c.slideId,
                SlideId = c.slideId.ToString(),
                KaryotypeThreshold = c.threshold
            };
        }


        
        public event EventHandler CellsDeleted;

        public void DeleteCellsAsync(IEnumerable<Cell> cellsToKill, Dispatcher uiDispatcher, ThreadStart completedCallback)
        {
            ThreadPool.QueueUserWorkItem((WaitCallback)delegate(object state)
            {
                try
                {
                    DeleteCells(cellsToKill);
                }
                finally
                {
                    uiDispatcher.Invoke((ThreadStart)delegate
                    {
                        CleanupAfterDeleteCells(cellsToKill);
                        completedCallback();

                    }, DispatcherPriority.Normal);
                }
            }, null);
        }

        public void DeleteCells(IEnumerable<Cell> cellsToKill)
        {
            using (var db = new Connect.Database())
            {
                foreach (var cell in cellsToKill)
                {
                    DeleteCellUserData(db, cell.Id);

                    var dbCell = db.Cells.Where(c => c.cellId == cell.Id).First();
                    db.Cells.DeleteOnSubmit(dbCell);

                    var assositedFuses = db.ImageFrames.Where(f => f.groupId == cell.ImageGroupId);
                    db.ImageFrames.DeleteAllOnSubmit(assositedFuses);

                    allCells.Remove(cell);
                }

                db.SubmitChanges();
            }

            WorkingCells = cellVisibility.FilterCells(AllCells);
            Totals.Refresh(WorkingCells);
            Notify("TotalCells", "AllCells", "WorkingCells");
        }

        public void CleanupAfterDeleteCells(IEnumerable<Cell> cellsToKill)
        {
            WorkingCells = cellVisibility.FilterCells(AllCells);
            Totals.Refresh(WorkingCells);

            if (cellsToKill.Contains(CurrentCell))
            {
                if (Tool != null && Tool.UndoList != null && Tool.UndoList.HasContent)
                {
                    Tool = null;
                }

                CurrentCell = workingCells.FirstOrDefault();

                if (CellsDeleted != null)
                    CellsDeleted(this, EventArgs.Empty);
            }
        }

        // <summary>
        /// Deletes all data a user generates that depends on an image.
        /// Karyotypes, numbering and count data, fused images.
        /// Does NOT delete cell metadata.
        /// </summary>
        /// <param name="cellId"></param>
        private void DeleteCellUserData(Connect.Database db, Guid cellId)
        {
            var countData = db.ManualCountPoints.Where(p => p.cellId == cellId);
            var numberData = db.NumberingTags.Where(t => t.cellId == cellId);
            var chromosomeData = db.KaryotypeChromosomes.Where(k => k.cellId == cellId);
            
            var dbCell = db.Cells.Where(c => c.cellId == cellId).First();
            var extraImages = db.ImageFrames.Where(f => f.groupId == dbCell.imageGroup && f.imageType == (int)ImageType.Processed);

            foreach (var extraImage in extraImages)
            {
                ImageFrameDbLoad.Delete(extraImage.imageFrameId);
            }

            foreach (var chrom in chromosomeData)
            {
                Connect.BlobStream.Delete(caseId, KaryotypeChromosome.ChromosomeDataFile(chrom.karyotypeChromosomeId), db);
            }

            if (Connect.BlobStream.Exists(caseId, KaryotypeController.MaskPath(cellId), db))
                Connect.BlobStream.Delete(caseId, KaryotypeController.MaskPath(cellId), db);

            db.ManualCountPoints.DeleteAllOnSubmit(countData);
            db.NumberingTags.DeleteAllOnSubmit(numberData);
            db.KaryotypeChromosomes.DeleteAllOnSubmit(chromosomeData);
            dbCell.counted = false;
            dbCell.karyotyped = false;
            dbCell.analysed = false;

            var workflowCell = allCells.Where(c => c.Id == cellId).First();
            workflowCell.IsCounted = false;
            workflowCell.IsKaryotyped = false;
            workflowCell.IsNumbered = false;

            WorkingCells = cellVisibility.FilterCells(allCells);
            totals.Refresh(WorkingCells);
        }

        public MetaData.AvailableFields AvailableCaseFields
        {
            get { return availableCaseFields; }
        }

        public MetaData.AvailableFields AvailableCellFields
        {
            get { return availableCellFields; }
        }

        public void RenameCell(Cell cell, string newName)
        {
            cell.Name = newName.Substring(0, Math.Min(newName.Length, 128));
            using (var db = new Connect.Database())
            {
                var dbCell = db.Cells.Where(c => c.cellId == cell.Id).FirstOrDefault();
                dbCell.cellName = cell.Name;
                db.SubmitChanges();
            }
        }

        public string NextAvailableSlideName()
        {
            //int i = 0;
            //string n = "slide" + i;

            //while (Slides.Where(s => string.Compare(s.Name, n, StringComparison.InvariantCultureIgnoreCase) == 0).FirstOrDefault() != null)
            //{
            //    ++i;
            //    n = "slide" + i;
            //}

            //return n;
            return Guid.NewGuid().ToString();
        }

        public void SetOrReplaceWorkingImageForCell(Cell cell, ImageFrame metImage)
        {
            if (cell.IsFused)
            {
                using (var db = new Connect.Database())
                {
                    var frameToKill = db.ImageFrames.Where(f => f.imageFrameId == cell.MetImageID).FirstOrDefault();

                    if (frameToKill != null)
                    {
                        db.ImageFrames.DeleteOnSubmit(frameToKill);
                        db.SubmitChanges();
                    }
                }
            }

            cell.MetImageID = metImage.Id;
            cell.MetaphaseThumbnail = metImage.Thumbnail;

            if (currentCell != null && cell.Id == currentCell.Id)
            {
                CurrentCellImage = metImage;
                Notify("CurrentCellImage");
            }
        }

        public void RollbackCellToRawImage(IEnumerable<Cell> cellsToRollback)
        {
            using (var db = new Connect.Database())
            {
                foreach (var cell in cellsToRollback)
                {
                    DeleteCellUserData(db, cell.Id);

                    var framesToKill = db.ImageFrames.Where(f => f.groupId == cell.ImageGroupId && f.imageType == (int)ImageType.Processed);
                    db.ImageFrames.DeleteAllOnSubmit(framesToKill);
                    db.SubmitChanges();

                    cell.MetaphaseThumbnailPath = cell.RawImageID.ToString() + ".jpg";
                    var bitmap = ImageFromBitmapSource.LoadBitmapImageFromStream(caseId, cell.MetaphaseThumbnailPath, db, Connect.CacheOption.DontCache, 250);

                    uiThread.Invoke((ThreadStart)delegate
                        {
                            if (Tool.RequiresFullImage)
                            {
                                Tool = null;
                            }
                            else
                            {
                                RevertTool();
                            }

                            cell.MetaphaseThumbnail = bitmap;
                            cell.KaryotypeThumbnail = null;

                            CurrentCellImage = ImageFrameDbLoad.FromDb(db, cell.RawImageID, caseId);
                            Notify("CurrentCellImage");

                            cell.MetImageID = Guid.Empty;

                        }, DispatcherPriority.Normal);
                }
            }
        }

        public void SaveTool()
        {
            if (Tool != null)
                Tool.Save();
        }
    }
}
