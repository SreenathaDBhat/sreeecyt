﻿using System;

namespace AI.Brightfield
{
    public partial class WorkflowPage
    {
        private WorkflowController controller;

        public WorkflowPage(WorkflowController controller)
        {
            this.controller = controller;
            DataContext = controller;
            InitializeComponent();
        }
    }
}
