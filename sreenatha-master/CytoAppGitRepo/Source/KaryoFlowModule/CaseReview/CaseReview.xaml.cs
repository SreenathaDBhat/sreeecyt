﻿using System;
using System.Windows;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using System.Data.Linq;
using System.Linq;
using System.IO;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows.Controls;
using System.Threading;

namespace AI
{
    public partial class CaseReview : UserControl, IDisposable
    {
        private bool disposed = false;

        public static RoutedUICommand ShowWorkSheetControl = new RoutedUICommand("Show Work Sheet Control", "ShowWorkSheetControl", typeof(CaseReview));
        public static RoutedUICommand ShowCellControl = new RoutedUICommand("Show Cell Control", "ShowCellControl", typeof(CaseReview));

        private Guid caseId;
        private CaseReviewController controller;
        private ScratchPadsController scratchPadController;

        public CaseReview(Guid caseId)
        {
            this.caseId = caseId;
            controller = new CaseReviewController(caseId);
            scratchPadController = new ScratchPadsController(caseId);
            InitializeComponent();

            ThreadPool.QueueUserWorkItem(delegate
            {
                controller.LoadThumbs(Dispatcher);
            });

            DataContext = controller;
            ShowWorkSheet();
        }

        private void CanNextCellExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.CanNextCellExecute();
        }

        private void NextCellExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            controller.MoveToNextCell();
        }

        private void CanPreviousCellExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.CanPreviousCellExecute();
        }

        private void PreviousCellExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            controller.MoveToPreviousCell();
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            UIStack.For(this).Pop(UIStackResult.Cancel);
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            scratchPadController.SaveAll();

            controller.SaveChangesToCurrentCell();
            controller.SaveReviewCheckList();
            UIStack.For(this).Pop(UIStackResult.Ok);
        }

        private void CanAddTemporaryToDoItemExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.ReviewersCheckList != null;
        }

        private void AddTemporaryToDoItemExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            newToDoItemPopup.IsOpen = true;
            newToDoItemDescription.Clear();
            newToDoItemDescription.Focus();
        }

        private void OnOK_NewToDoItemPopup(object sender, RoutedEventArgs e)
        {
            controller.ReviewersCheckList.AddToDoItem(new ToDoItem() { Description = newToDoItemDescription.Text, Done = false });
            newToDoItemPopup.IsOpen = false;
            newToDoItemDescription.Clear();
        }

        private void OnCancel_NewToDoItemPopup(object sender, RoutedEventArgs e)
        {
            newToDoItemPopup.IsOpen = false;
            newToDoItemDescription.Clear();
        }

        private void CanShowCellControlExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (mainDisplayBorder != null && mainDisplayBorder.Child is CaseReviewCellControl)
                e.CanExecute = false;
            else
                e.CanExecute = true;
        }

        private void ShowCellControlExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ShowFullScreenCellImage();
        }

        private void ShowFullScreenCellImage()
        {
            CaseReviewCellControl control = new CaseReviewCellControl();
            control.SetController(controller);
            mainDisplayBorder.Child = control;
        }

        private void CanShowWorkSheetControlExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (mainDisplayBorder != null && mainDisplayBorder.Child is CaseReviewWorkSheetControl)
                e.CanExecute = false;
            else
                e.CanExecute = true;
        }

        private void ShowWorkSheetControlExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ShowWorkSheet();
        }

        private void ShowWorkSheet()
        {
            CaseReviewWorkSheetControl workSheetControl = new CaseReviewWorkSheetControl();
            workSheetControl.SetController(controller);
            workSheetControl.CellSelected += new EventHandler(workSheetControl_CellSelected);
            mainDisplayBorder.Child = workSheetControl;
        }

        void workSheetControl_CellSelected(object sender, EventArgs e)
        {
            CaseReviewWorkSheetControl workSheetControl = (CaseReviewWorkSheetControl)sender;
            workSheetControl.CellSelected -= workSheetControl_CellSelected;

            CaseReviewCellControl cellControl = new CaseReviewCellControl();
            cellControl.SetController(controller);
            mainDisplayBorder.Child = cellControl;
        }

        private void OnChromosomeComparison(object sender, RoutedEventArgs e)
        {
            Connect.Database db = new AI.Connect.Database();
            var caseName = (from c in db.Cases
                           where c.caseId == caseId
                           select c.name).First();

            ShowMeThe16sController comparisonController = new ShowMeThe16sController(caseId, caseName, "1");
            ShowMeThe16sControl compairsonControl = new ShowMeThe16sControl(comparisonController, scratchPadController);
            //compairsonControl.AddChromosomesToScratchPad += new EventHandler<ScratchPadEventArgs>(compairsonControl_AddChromosomesToScratchPad);
            mainDisplayBorder.Child = compairsonControl;
        }

        //void compairsonControl_AddChromosomesToScratchPad(object sender, ScratchPadEventArgs e)
        //{
        //    ShowMeThe16sControl control = (ShowMeThe16sControl)sender;
        //    //control.AddChromosomesToScratchPad -= compairsonControl_AddChromosomesToScratchPad;

        //    ShowMeThe16sController controller = control.Controller;
        //    //ObservableCollection<Chromosome> chromosomes = new ObservableCollection<Chromosome>();

        //    ChromosomeScratchPad scratchPad;
        //    if (e.ScratchPad == null)
        //    {
        //        scratchPad = new ChromosomeScratchPad();
        //        scratchPad.Name = "Scratch Pad 01";
        //    }
        //    else
        //    {
        //        scratchPad = e.ScratchPad;
        //    }

        //    Context db = new Context();

        //    foreach (CellChromosomeCollectionCutter collectionCutter in controller.CellChromosomeCollections)
        //    {
        //        foreach (ChromosomeCutter cutter in collectionCutter.NumberedChromosomes)
        //        {
        //            if (cutter.MaskStrokes.Count > 0)
        //            {
        //                MostBasicCell cell = new MostBasicCell() 
        //                {
        //                    CaseId = collectionCutter.Cell.CaseId,
        //                    CellId = collectionCutter.Cell.CellId,
        //                    CellName = collectionCutter.Cell.CellName,
        //                    Culture = collectionCutter.Cell.Culture,
        //                    Slide = collectionCutter.Cell.Slide,
        //                    SlideId = collectionCutter.Cell.SlideId
        //                };

        //                Chromosome chromosome = new Chromosome(cutter.GenerateChromosomeFromMask(), cell.CellId, collectionCutter.ChromosomeTag);
        //                //Chromosome.Save(chromosome, scratchPad.Id, db);
        //                ScratchPadChromosome spChromosome = new ScratchPadChromosome(chromosome);
        //                //chromosomes.Add(chromosome);
        //                scratchPad.AddItem(spChromosome);
        //            }
        //        }
        //    }

        //    db.SubmitChanges();

        //    scratchPadController.AddScratchPad(scratchPad);
        //    ChromosomeComparisonControl comparisonControl = new ChromosomeComparisonControl(scratchPad);
        //    mainDisplayBorder.Child = comparisonControl;
        //}

    
        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    DisposeManagedResources();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.
                DisposeUnManagedResources();

                // Note disposing has been done.
                disposed = true;
            }
        }

        private void DisposeManagedResources()
        {
        }

        private void DisposeUnManagedResources()
        {
        }

        ~CaseReview()
        {
            Dispose(false);
        }

        #endregion
    
    }
}
