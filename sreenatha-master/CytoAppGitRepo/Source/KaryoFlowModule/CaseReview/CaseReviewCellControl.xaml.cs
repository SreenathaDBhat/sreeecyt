﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Linq;
using System.IO;

namespace AI
{
    public partial class CaseReviewCellControl : UserControl
    {
        public static RoutedUICommand LinkCellToCheckListItem = new RoutedUICommand("Link Cell To Check List Item", "LinkCellToCheckListItem", typeof(CaseReviewCellControl));

        private CaseReviewController controller;
        private SharedFiltersPassController imageRenderController;
        private ImageRenderParameters renderParameters;

        public CaseReviewCellControl()
        {
            InitializeComponent();
            annotationsDisplay.Controller.AnnotationType = AnnotationUserType.Reviewer;
        }

        public void SetController(CaseReviewController controller)
        {
            this.controller = controller;
            DataContext = this.controller;

            controller.SetAnnotationsController(annotationsDisplay.Controller);
            renderParameters = new ImageRenderParameters(controller.CellImageFrame);
            image.ImageFrameX = controller.CellImageFrame;
            imageRenderController = new SharedFiltersPassController();
            imageRenderController.AttachRenderer(image, renderParameters);
            imageRenderController.RegisterWithCommands(this);
            imageRenderController.LoadDisplayOptions(KaryotypeWorkflowController.KaryotyperDisplayOptionsString);

            if (renderParameters != null)
            {
                foreach (var snap in controller.CellImageFrame.AllSnaps)
                {
                    snap.ChannelInfo = renderParameters.EquivilentChannel(snap.ChannelInfo);
                }
            }

            if (image != null)
            {
                //image.ForceInvalidateFilters();
                image.Render();
            }
        }

        public ImageRenderParameters RenderParameters
        {
            get { return renderParameters; }
        }

        private void OnAnnotationLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            annotationsDisplay.PlacementHelper.StartPlacingAnnotation(sender, e, image);
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            annotationsDisplay.PlacementHelper.MoveAnnotationBeingPlaced(sender, e, image);
        }

        private void OnLeftMouseUp(object sender, MouseButtonEventArgs e)
        {
            annotationsDisplay.PlacementHelper.FinishPlacingAnnotation(sender, e, image);
        }

        private void CanLinkCellToCheckListItemExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (controller == null)
                return;

            bool validCell = controller.CurrentCell != null;
            bool checkListItems = controller.ReviewersCheckList.AllCheckListItems.Count() > 0;
            e.CanExecute = validCell && checkListItems;
        }
        
        private void LinkCellToCheckListItemExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            checkListLinkPopup.IsOpen = true;
        }

        private void OnCheckListLinkSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ToDoItem item = (ToDoItem)e.AddedItems[0];
            if (item != null)
            {
                item.AddCellReference(controller.CurrentCell);
            }
            checkListLinkPopup.IsOpen = false;
        }
    }
}
