﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.IO;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Windows;
using System.Xml.Linq;
using AI.Karyotyping;
using System.Windows.Threading;
using System.Threading;

namespace AI
{
    public class CaseReviewController : INotifyPropertyChanged
    {
        public static RoutedUICommand NextCell = new RoutedUICommand("Next Cell", "NextCell", typeof(CaseReviewController));
        public static RoutedUICommand PreviousCell = new RoutedUICommand("Previous Cell", "PreviousCell", typeof(CaseReviewController));

        private AnnotationController annotationController;
        private CheckListController checkListController = new CheckListController();
        private Guid caseId;

        public CaseReviewController(Guid caseId)
        {
            this.caseId = caseId;
            Connect.Database db = new Connect.Database();

            Cells = CellsListController.CellsForCase(db, caseId);

            if (Cells.Count() > 0)
            {
                CurrentCell = Cells.First();
            }

            //foreach (Cell cell in Cells)
            //{
            //    cell.LoadManualCountPoints();
            //    cell.LoadNumberingTags();
            //}

            checkListController.LoadCheckList(ReviewCheckListFile(), Cells);
        }

        public CheckListController ReviewersCheckList
        {
            get { return checkListController; }
        }

        public IEnumerable<Cell> Cells { get; set; }
        private Cell currentCell;
        public Cell CurrentCell
        {
            get { return currentCell; }
            set 
            { 
                currentCell = value;
                SetCurrentImageFrame(value);
                Notify("CurrentCell");
                Notify("CellImageFrame");
                
                if(annotationController != null)
                    annotationController.Load(value.Id); }
        }

        private void SetCurrentImageFrame(Cell cell)
        {
            var frame = cell.WorkingImage;
            if (frame == null)
                return;
            cellImageFrame = ImageFrameDbLoad.FromDb(frame.FrameId, caseId);
        }

        private ImageFrame cellImageFrame;
        public ImageFrame CellImageFrame
        {
            get { return cellImageFrame; }
        }

        public IEnumerable<Cell> KaryotypedCells
        {
            get { return Cells.Where(c => c.Karyotyped); }
        }

        internal bool CanNextCellExecute()
        {
            if (Cells.Count() < 1)
                return false;

            return CurrentCell != Cells.Last();
        }

        internal bool CanPreviousCellExecute()
        {
            if (Cells.Count() < 1)
                return false;

            return CurrentCell != Cells.First();
        }

        internal void MoveToNextCell()
        {
            SaveChangesToCurrentCell();
            MoveCurrentCell(+1);
        }

        internal void MoveToPreviousCell()
        {
            SaveChangesToCurrentCell();
            MoveCurrentCell(-1);
        }

        private void MoveCurrentCell(int indexAdjustment)
        {
            int indexOfCurrent = 0;
            foreach (Cell cell in Cells)
            {
                if (cell == CurrentCell)
                    break;
                indexOfCurrent++;
            }
            CurrentCell = Cells.ElementAt(indexOfCurrent + indexAdjustment);
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        internal void SaveChangesToCurrentCell()
        {
            if (annotationController != null)
                annotationController.Save();
        }

        internal void SetAnnotationsController(AnnotationController annotationController)
        {
            this.annotationController = annotationController;
            annotationController.Load(CurrentCell.Id);
        }

        public AnnotationController Annotations
        {
            get { return annotationController; }
        }

        internal void SaveReviewCheckList()
        {
            checkListController.SaveCheckList(ReviewCheckListFile());
        }

        private FileInfo ReviewCheckListFile()
        {
            throw new Exception("This needs to go in the database. Case folders dont exist anymore");
        }

        // DUPLICATED: refactor to remove the duplication
        public void LoadThumbs(Dispatcher dispatcher)
        {
            var db = new Connect.Database();

            foreach (var cell in Cells)
            {
                var img = cell.WorkingImage;
                if (img != null)
                {
                    string path = img.FrameId.ToString() + ".jpg";
                    if (Connect.BlobStream.Exists(caseId, path, db))
                    {
                        var bitmap = ImageFromBitmapSource.LoadBitmapImageFromStream(caseId, path, db);

                        dispatcher.Invoke((ThreadStart)delegate
                        {
                            cell.ThumbnailImage = bitmap;

                        }, DispatcherPriority.Normal);
                    }
                }
            }
        }
    }

    public class CheckListController : INotifyPropertyChanged
    {
        public static RoutedUICommand AddTemporaryToDoItem = new RoutedUICommand("Add Temporary ToDo Item", "AddTemporaryToDoItem", typeof(CheckListController));

        public CheckListController()
        {
            fixedCheckList.Add(new ToDoItem() { Description = "Bands checked on 2 Karyotypes", Done = false });
            fixedCheckList.Add(new ToDoItem() { Description = "Tech analysed 20 cells", Done = false });
            fixedCheckList.Add(new ToDoItem() { Description = "No cell used twice", Done = false });
            fixedCheckList.Add(new ToDoItem() { Description = "All cells have the same sex", Done = false });
            fixedCheckList.Add(new ToDoItem() { Description = "Cell comments match cell counts", Done = false });
            fixedCheckList.Add(new ToDoItem() { Description = "Each chromosome pair cleared twice", Done = false });
        }

        private ObservableCollection<ToDoItem> temporaryToDoList = new ObservableCollection<ToDoItem>();
        public IEnumerable<ToDoItem> TemporaryToDoList
        {
            get { return temporaryToDoList; }
        }

        private ObservableCollection<ToDoItem> fixedCheckList = new ObservableCollection<ToDoItem>();
        public IEnumerable<ToDoItem> FixedCheckList
        {
            get { return fixedCheckList; }
        }

        public IEnumerable<ToDoItem> AllCheckListItems
        {
            get { return fixedCheckList.Union(temporaryToDoList); }
        }

        public void AddToDoItem(ToDoItem item)
        {
            temporaryToDoList.Add(item);
            Notify("AllCheckListItems");
        }

        public void RemoveToDoItem(ToDoItem item)
        {
            temporaryToDoList.Remove(item);
            Notify("AllCheckListItems");
        }

        public void SaveCheckList(FileInfo file)
        {
            XElement xml = new XElement("ReviewCheckList",
                new XElement("FixedCheckListItems", BuildItems(FixedCheckList)),
                new XElement("TemporaryToDoListItems", BuildItems(TemporaryToDoList)));

            if (file.Exists)
            {
                file.Delete();
            }

            xml.Save(file.FullName);
        }

        private IEnumerable<XElement> BuildItems(IEnumerable<ToDoItem> list)
        {
            return from fi in list
                   select new XElement("Item", new XAttribute("Description", fi.Description),
                                                new XAttribute("Done", fi.Done.ToString()),
                                                new XElement("ReferencedCells", from element in fi.ReferencedCells
                                                                                select new XElement("CellId", element.Id.ToString())));
        }

        public void LoadCheckList(FileInfo file, IEnumerable<Cell> reviewCells)
        {
            if (!file.Exists)
                return;

            fixedCheckList.Clear();

            XElement xml = XElement.Load(file.FullName);
            XElement fixedItemsXml = xml.Descendants("FixedCheckListItems").First();

            foreach (XElement item in fixedItemsXml.Descendants("Item"))
            {
                ToDoItem i = new ToDoItem() { Description = item.Attribute("Description").Value, Done = bool.Parse(item.Attribute("Done").Value) };
                foreach (XElement cellId in item.Descendants("CellId"))
                {
                    i.AddCellReference(reviewCells.Where(c => c.Id.ToString() == cellId.Value).First());
                }
                fixedCheckList.Add(i);
            }

            XElement temporaryItemsXml = xml.Descendants("TemporaryToDoListItems").First();

            foreach (XElement item in temporaryItemsXml.Descendants("Item"))
            {
                ToDoItem i = new ToDoItem() { Description = item.Attribute("Description").Value, Done = bool.Parse(item.Attribute("Done").Value) };
                foreach (XElement cellId in item.Descendants("CellId"))
                {
                    i.AddCellReference(reviewCells.Where(c => c.Id.ToString() == cellId.Value).First());
                }
                temporaryToDoList.Add(i);
            }
        }

        #region INotifyPropertyChanged Members
        
        public event PropertyChangedEventHandler PropertyChanged;
        private void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
