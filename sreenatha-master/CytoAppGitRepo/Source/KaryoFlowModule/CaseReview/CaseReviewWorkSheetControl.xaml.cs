﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using AI.Karyotyping;

namespace AI
{
    public partial class CaseReviewWorkSheetControl : UserControl
    {
        public event EventHandler CellSelected;

        private CaseReviewController controller;

        public CaseReviewWorkSheetControl()
        {
            InitializeComponent();
        }

        public void SetController(CaseReviewController controller)
        {
            this.controller = controller;
            DataContext = this.controller;
        }

        private void OnImageDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Cell cellClickedOn = (Cell)((FrameworkElement)sender).Tag;
            controller.CurrentCell = cellClickedOn;
            if (CellSelected != null)
                CellSelected(this, new EventArgs());
        }
    }
}
