﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using System.IO;
using System.Linq;

namespace AI
{
    //public class ReviewCell : INotifyPropertyChanged
    //{
    //    public Connect.Case Case { get; set; }
    //    public Guid CellId { get; set; }
    //    public string Result { get; set; }
    //    public string Comment { get; set; }
    //    public string CellName { get; set; }
    //    public bool Counted { get; set; }
    //    public bool Analysed { get; set; }
    //    public bool Karyotyped { get; set; }
    //    public IEnumerable<Connect.ImageFrame> Images { get; set; }

    //    private List<Point> manualCountPoints = new List<Point>();
    //    public IEnumerable<Point> ManualCountPoints
    //    {
    //        get { return manualCountPoints; }
    //    }

    //    private bool showCountPoints = false;
    //    public bool ShowCountPoints
    //    {
    //        get { return showCountPoints; }
    //        set { showCountPoints = value; Notify("ShowCountPoints"); }
    //    }

    //    private bool showAnalysisNumbers = false;
    //    public bool ShowAnalysisNumbers
    //    {
    //        get { return showAnalysisNumbers; }
    //        set { showAnalysisNumbers = value; Notify("ShowAnalysisNumbers"); }
    //    }

    //    public BitmapSource Metaphase
    //    {
    //        get
    //        {
    //            foreach (Connect.ImageFrame image in Images)
    //            {
    //                if (image.imageType == 0)
    //                    return DirtyFrameToBitmap.Convert(image.imageFrameId, Case.caseId);
    //            }
    //            return null;
    //        }
    //    }

    //    public BitmapSource Karyotype
    //    {
    //        get
    //        {
    //            foreach (Connect.ImageFrame image in Images)
    //            {
    //                if (image.imageType == 1)
    //                    return DirtyFrameToBitmap.Convert(image.imageFrameId, Case.caseId);
    //            }
    //            return null;
    //        }
    //    }

    //    public BitmapSource KaryotypeThumb
    //    {
    //        get { return MetaphaseThumb; }
    //    }

    //    public BitmapSource MetaphaseThumb
    //    {
    //        get
    //        {
    //            foreach (Connect.ImageFrame image in Images)
    //            {
    //                if (image.imageType == 0)
    //                    return DirtyFrameToBitmap.Convert(image.imageFrameId, Case.caseId);
    //            }
    //            return null;
    //        }
    //    }

    //    #region INotifyPropertyChanged Members

    //    public event PropertyChangedEventHandler PropertyChanged;
    //    private void Notify(string propertyName)
    //    {
    //        if (PropertyChanged != null)
    //            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    //    }

    //    #endregion

    //    public void LoadManualCountPoints()
    //    {
    //        Connect.Database db = new Connect.Database();

    //        manualCountPoints = (from m in db.ManualCountPoints
    //                             where m.cellId == CellId
    //                             select new Point
    //                             {
    //                                 X = m.x,
    //                                 Y = m.y
    //                             }).ToList();

    //        Notify("ManualCountPoints");
    //    }

    //    List<NumberingTag> numberingTags = new List<NumberingTag>();
    //    public IEnumerable<NumberingTag> NumberingTags
    //    {
    //        get { return numberingTags; }
    //    }

    //    public void LoadNumberingTags()
    //    {
    //        Connect.Database db = new Connect.Database();
    //        numberingTags = (from n in db.NumberingTags
    //                         where n.cellId == CellId
    //                         select new NumberingTag
    //                         {
    //                             Tag = n.tag,
    //                             X = n.x,
    //                             Y = n.y
    //                         }).ToList();
    //    }
    //}
}
