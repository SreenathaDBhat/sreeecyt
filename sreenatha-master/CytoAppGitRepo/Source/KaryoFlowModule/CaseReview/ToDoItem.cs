﻿using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using AI.Karyotyping;

namespace AI
{
    public class ToDoItem : INotifyPropertyChanged
    {
        public string Description { get; set; }

        private bool done = false;
        public bool Done
        {
            get { return done; }
            set { done = value; Notify("Done"); }
        }

        private ObservableCollection<Cell> referencedCells = new ObservableCollection<Cell>();
        public IEnumerable<Cell> ReferencedCells
        {
            get { return referencedCells; }
        }

        public void AddCellReference(Cell cell)
        {
            referencedCells.Add(cell);
        }

        public void RemoveCellReference(Cell cell)
        {
            referencedCells.Remove(cell);
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}