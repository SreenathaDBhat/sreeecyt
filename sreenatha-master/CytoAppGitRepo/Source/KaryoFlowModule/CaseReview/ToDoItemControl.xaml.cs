﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using AI.Karyotyping;

namespace AI
{
    public partial class ToDoItemControl : UserControl
    {
        public static readonly DependencyProperty ToDoItemProperty = DependencyProperty.Register("ToDoItem", typeof(ToDoItem), typeof(ToDoItemControl));
        public static readonly DependencyProperty ControllerProperty = DependencyProperty.Register("Controller", typeof(CaseReviewController), typeof(ToDoItemControl));

        public ToDoItemControl()
        {
            InitializeComponent();
        }

        public ToDoItem ToDoItem
        {
            get { return (ToDoItem)GetValue(ToDoItemProperty); }
            set { SetValue(ToDoItemProperty, value); }
        }

        public CaseReviewController Controller 
        {
            get { return (CaseReviewController)GetValue(ControllerProperty); }
            set { SetValue(ControllerProperty, value); }
        }

        private void OnSelectReferencedCell(object sender, RoutedEventArgs e)
        {
            Cell cell = (Cell)((FrameworkElement)sender).Tag;
            Controller.CurrentCell = cell;
        }

        private void OnRemoveCellReference(object sender, MouseButtonEventArgs e)
        {
            Cell referencedCell = (Cell)((FrameworkElement)sender).Tag;
            ToDoItem.RemoveCellReference(referencedCell);
        }

        private void OnDoneToggled(object sender, RoutedEventArgs e)
        {
            ToDoItem.Done = !ToDoItem.Done;
        }
    }
}
