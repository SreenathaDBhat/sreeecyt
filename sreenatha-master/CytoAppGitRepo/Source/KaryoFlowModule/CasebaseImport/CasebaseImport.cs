﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using AI.Connect;
using System.Xml.Linq;
using System.Windows.Media.Imaging;
using AI.Bitmap;
using System.Windows.Media;
using System.Windows;
using System.Windows.Threading;
using System.ComponentModel;
using System.Threading;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Xml;

namespace AI
{
    
    public class CaseImport45000to46000 : INotifyPropertyChanged, IDisposable
    {
        private Dispatcher uiDispatcher;
        private double percentDone;
        private ILims lims;
        private Connect.Database db;
        private Guid caseId;
        private string caseName;
        private DirectoryInfo caseFolder;

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        void Notify(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion

        public CaseImport45000to46000(DirectoryInfo caseFolder, string caseName, Dispatcher uiDispatcher)
        {
            this.uiDispatcher = uiDispatcher;
            this.db = new Connect.Database();
            this.lims = Connect.LIMS.New();
            this.percentDone = 0;
            this.caseName = caseName;
            this.caseFolder = caseFolder;
        }

        public void Dispose()
        {
            db.Dispose();
            lims.Dispose();
            db = null;
            lims = null;
        }

        public double PercentComplete
        {
            get { return percentDone; }
        }

        public Guid Import()
        {
            var newCase = lims.FindCase(caseName);
            if(newCase == null)
                newCase = CaseManagement.CreateCase(caseName);

            caseId = db.CaseIdFor(newCase);

            var slideFolders = (from d in caseFolder.GetDirectories()
                                where FolderContainsDotNameFile(d)
                                select d).ToArray();

            double numSlides = slideFolders.Count();
            int curSlide = 0;

            foreach (var slideFolder in slideFolders)
            {
                var slideName = ReadDotNameFile(slideFolder.FullName, slideFolder.Name);
                var newSlide = CaseManagement.CreateSlide(newCase, slideName, "", SlideTypes.Default);

                ImportSlide(newCase, newSlide, slideFolder);

                ++curSlide;
                percentDone = curSlide / numSlides;

                uiDispatcher.Invoke((ThreadStart)delegate
                {
                    Notify("PercentComplete");

                }, DispatcherPriority.Normal);
            }

            return caseId;
        }

        private static bool FolderContainsDotNameFile(DirectoryInfo d)
        {
            return File.Exists(Path.Combine(d.FullName, ".NAME"));
        }

        private static string ReadDotNameFile(string path, string fallback)
        {
            FileInfo file = new FileInfo(Path.Combine(path, ".NAME"));
            if (file.Exists)
            {
                using (var reader = new StreamReader(file.FullName))
                {
                    return reader.ReadLine();
                }
            }
            else
            {
                return fallback;
            }
        }

        private void ImportSlide(Case newCase, Slide newSlide, DirectoryInfo slideFolder)
        {
            foreach (var cellFolder in slideFolder.GetDirectories())
            {
                var slideId = db.Slides.Where(s => s.limsRef == newSlide.Id).First().slideId;
                ImportCell(newCase, newSlide, slideId, cellFolder);
            }
        }

        private void ImportCell(Case newCase, Slide newSlide, Guid newSlideTubsId, DirectoryInfo cellFolder)
        {
            var allFiles = (from f in cellFolder.GetFiles()
                            where string.Compare(f.Extension, ".raw", StringComparison.CurrentCultureIgnoreCase) == 0
                            select f).ToArray();

            if (allFiles.Length == 0)
                return;

            Cell cell = new Cell();
            cell.cellId = Guid.NewGuid();
            cell.slideId = newSlideTubsId;
            cell.cellName = ReadDotNameFile(cellFolder.FullName, cellFolder.Name);
            cell.analysed = false;
            cell.color = 3;
            cell.counted = false;
            cell.karyotyped = false;
            cell.imageGroup = Guid.NewGuid();
            db.Cells.InsertOnSubmit(cell);


            ImageType type = ImageType.Raw;

            foreach (var file in allFiles)
            {
                var channel = ImageFromBitmapSource.LoadBitmapFromDisk(file.FullName, true);
                ImportCellImage(channel, newCase, newSlide, newSlideTubsId, type, cellFolder, cell.imageGroup);

                type = ImageType.Fuse;
            }
        }

        private void ImportCellImage(Channel channel, Case newCase, Slide newSlide, Guid newSlideTubsId, ImageType type, DirectoryInfo cellFolder, Guid imageGroupId)
        {
            Connect.ImageFrame frame = new Connect.ImageFrame();
            frame.imageFrameId = Guid.NewGuid();
            frame.bpp = 8; // TODO
            frame.height = channel.Height;
            frame.location = "n/a";
            frame.slideId = newSlideTubsId;
            frame.width = channel.Width;
            frame.imageType = (int)type;
            frame.groupId = imageGroupId;
            frame.x = frame.y = -1;
            frame.gamma = 1;
            db.ImageFrames.InsertOnSubmit(frame);

            ImageComponent component = new ImageComponent();
            component.ImageFrame = frame;
            component.displayName = "Brightfield";
            component.imageComponentId = Guid.NewGuid();
            component.isCounterstain = false;
            component.renderColor = ColorParse.ToHexString(Colors.White);
            component.isAnalysisOverlay = false;
            db.ImageComponents.InsertOnSubmit(component);

            ImageSnap snap = new ImageSnap();
            snap.ImageComponent = component;
            snap.imageSnapId = Guid.NewGuid();
            snap.zLevel = 0;
            //            db.ImageSnaps.InsertOnSubmit(snap);

            Snap realSnap = new Snap();
            realSnap.Id = snap.imageSnapId;
            realSnap.ChannelPixels = channel;

            snap.blobId = realSnap.WriteSnapDataToDisk(caseId, db);
            db.ImageSnaps.InsertOnSubmit(snap);

            realSnap.ChannelInfo = new ChannelInfo("Brightfield") { RenderColor = Colors.White };

            AI.ImageFrame realFrame = new ImageFrame()
            {
                ImageWidth = frame.width,
                ImageHeight = frame.height,
                ImageBitsPerPixel = frame.bpp
            };

            realFrame.AddSnap(realSnap);
            var bitmapForThumb = RGBImageComposer.Compose(realFrame);
            string thumbFile = frame.imageFrameId.ToString() + ".jpg";

            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapForThumb));
            encoder.QualityLevel = 60;

            using (var stream = new MemoryStream())
            {
                encoder.Save(stream);
                stream.Flush();
                frame.blobid = BlobStream.WriteBlobStream(caseId, thumbFile, stream, db);
                stream.Close();
            }

            db.SubmitChanges();
        }

        private void ImportCellAiPng(Case newCase, Slide newSlide, Guid newSlideTubsId, ImageType imageType, FileInfo imageFile, Guid imageGroupId)
        {
            BitmapImage img = new BitmapImage();
            img.BeginInit();
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.UriSource = new Uri(imageFile.FullName);
            img.EndInit();
            img.Freeze();

            Connect.ImageFrame frame = new Connect.ImageFrame();
            frame.imageFrameId = Guid.NewGuid();
            frame.bpp = img.Format.BitsPerPixel; // TODO
            frame.height = (int)img.Height;
            frame.location = "n/a";
            frame.slideId = newSlideTubsId;
            frame.width = (int)img.Width;
            frame.imageType = (int)imageType;
            frame.groupId = imageGroupId;
            frame.x = frame.y = -1;
            frame.gamma = 1;
            db.ImageFrames.InsertOnSubmit(frame);

            JpegBitmapEncoder jEnc = new JpegBitmapEncoder();
            jEnc.Frames.Add(BitmapFrame.Create(img));
            jEnc.QualityLevel = 90;

            using (var stream = new MemoryStream())
            {
                jEnc.Save(stream);
                stream.Flush();
                frame.blobid = BlobStream.WriteBlobStream(caseId, frame.imageFrameId.ToString() + ".jpg", stream, db);
                stream.Close();
            }
            db.SubmitChanges();
        }

        private static Channel Mask(Channel channel, Channel subtractedMask)
        {
            int len = channel.Width * channel.Height;
            ushort[] pixels = new ushort[len];
            ushort[] input = channel.Pixels;
            ushort[] mask = subtractedMask.Pixels;

            ushort max = (ushort)((int)(Math.Pow(2, channel.BitsPerPixel) + 0.5) - 1);

            for (int i = 0; i < len; ++i)
            {
                if (mask[i] > 100)
                {
                    pixels[i] = (ushort)max;
                }
                else
                {
                    pixels[i] = (ushort)(max - input[i]);
                }
            }

            return new Channel(channel.Width, channel.Height, channel.BitsPerPixel, pixels);
        }
    }
}
