﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Windows.Media;
using AI.Bitmap;

namespace AI.CV45000
{
    public class FrameInfo
    {
        public ImageFrame Image { get; set; }
        public Guid OriginalId { get; set; }
    }

    public static class ImageFrameXmlSerializer
    {
        public static void LoadSnapImageData(Snap snap)
        {
            FileInfo fi = new FileInfo(snap.ImageDataFile);
            if (fi.Exists)
            {
                using (var stream = new FileInfo(snap.ImageDataFile).OpenRead())
                {
                    snap.ChannelPixels = ImageFromBitmapSource.LoadFromStream(stream);
                }
            }
        }

        public static IEnumerable<FrameInfo> FramesFromSlideXml(XElement slideNode, DirectoryInfo rootDirectory)
        {
            List<ChannelInfo> sharedFluorochromes = new List<ChannelInfo>();

            return (from frameNode in slideNode.Descendants("Frame")
                    select BuildFrame(frameNode, rootDirectory, sharedFluorochromes)).ToList(); // Use ToList to force execution of the linq statement now
        }

        private static ChannelInfo ReadFluorochromeChannel(XElement componentNode, List<ChannelInfo> sharedFluorochromes)
        {
            var fluorChannel = new ChannelInfo();
            fluorChannel.Dichroic = new Filter
            {
                Name = Parsing.ValueOrDefault(componentNode.Attribute("Name"), "UNKNOWN"),
                RotorPosition = -1
            };

            fluorChannel.Id = Guid.NewGuid();

            fluorChannel.Exposure = Parsing.TryParseInt(Parsing.ValueOrDefault(componentNode.Attribute("Exposure"), "0.0"), 0);
            fluorChannel.IsStacked = Parsing.TryParseBool(Parsing.ValueOrDefault(componentNode.Attribute("IsStacked"), "False"), false);
            fluorChannel.IsAnalysisOverlay = Parsing.TryParseBool(Parsing.ValueOrDefault(componentNode.Attribute("IsAnalysisOverlay"), "False"), false);
            fluorChannel.IsCounterstain = Parsing.TryParseBool(Parsing.ValueOrDefault(componentNode.Attribute("IsCounterstain"), "False"), false);
            fluorChannel.RenderColor = Parsing.TryParseColor(Parsing.ValueOrDefault(componentNode.Attribute("RenderColor"), "#ff0000"), Colors.Red);

            var existing = sharedFluorochromes.Where(c => c.DisplayName == fluorChannel.DisplayName).FirstOrDefault();
            if (existing != null)
                return existing;

            sharedFluorochromes.Add(fluorChannel);
            return fluorChannel;
        }

        private static FrameInfo BuildFrame(XElement frameNode, DirectoryInfo rootDirectory, List<ChannelInfo> sharedFluorochromes)
        {
            ImageFrame frame = new ImageFrame();

            var originalId = frameNode.Attribute("Id").Value;
            frame.Id = Guid.NewGuid();
            frame.SlideLocation = ParseLocation(frameNode.Attribute("Location"));


            foreach (var componentNode in frameNode.Descendants("Component"))
            {
                ChannelInfo fluorChannel = ReadFluorochromeChannel(componentNode, sharedFluorochromes);

                int stackIndex = 0;
                foreach (var imageNode in componentNode.Descendants("Image"))
                {
                    Snap snap = new Snap();
                    snap.ChannelInfo = fluorChannel;
                    snap.IsProjection = false;
                    snap.StackIndex = stackIndex++;
                    snap.Id = Guid.NewGuid(); //new Guid(Parsing.ValueOrDefault(imageNode.Attribute("ID"), Guid.Empty.ToString()));
                    snap.ImageDataFile = imageNode.Attribute("Path").Value;
                    frame.AddSnap(snap);
                }

                var projection = componentNode.Element("ProjectionImage");
                if (projection != null)
                {
                    Snap snap = new Snap();
                    snap.ChannelInfo = fluorChannel;
                    snap.IsProjection = true;
                    snap.StackIndex = -1;
                    snap.Id = Guid.NewGuid();
                    snap.ImageDataFile = projection.Attribute("Path").Value;
                    frame.AddSnap(snap);
                }
            }

            return new FrameInfo { Image = frame, OriginalId = new Guid(originalId) };
        }

        private static SlidePoint ParseLocation(XAttribute xAttribute)
        {
            if (xAttribute == null)
                return new SlidePoint();

            return SlidePoint.Decode(xAttribute.Value);
        }
    }
}
