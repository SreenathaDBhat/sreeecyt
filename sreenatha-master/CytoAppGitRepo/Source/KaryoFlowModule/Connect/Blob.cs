﻿using System;
using System.IO;
using System.Linq;
using System.Diagnostics;

namespace AI.Connect
{
    public interface IBlobStreamer
    {
        Guid Write(Guid caseId, string path, byte[] data, int length, object state);
        void Write(Guid caseId, Guid blobId, string path, byte[] data, int length, object state);
        byte[] Read(Guid caseId, string path, object state);
        byte[] Read(Guid blobID, object state);
        bool Exists(Guid caseId, string path, object state);
        bool Exists(Guid blobID, object state);
        void Delete(Guid caseId, string path, object state);
        void Delete(Guid blobID, object state);
        Guid IdOf(Guid caseId, string path, Database db);
    }

    internal class DatabaseBlobStreamer : IBlobStreamer
    {
        public Guid Write(Guid caseId, string path, byte[] data, int length, object state)
        {
            Database db = state as Database;
            bool submit = db == null;
            Guid blobID = Guid.Empty;

            if (submit)
                db = new Database();

            var blob = db.Blobs.Where(b => b.caseId == caseId && string.Compare(path, b.path, StringComparison.InvariantCultureIgnoreCase) == 0).FirstOrDefault();

            if (blob == null)
            {
                blobID = Guid.NewGuid();

                blob = new Blob
                {
                    blobId = blobID,
                    caseId = caseId,
                    path = path
                };

                db.Blobs.InsertOnSubmit(blob);
            }
            else blobID = blob.blobId;


            if (data.Length != length)
            {
                byte[] trimmed = new byte[length];
                for (int i = 0; i < length; ++i)
                    trimmed[i] = data[i];

                data = trimmed;
            }

            blob.binaryData = new System.Data.Linq.Binary(data);

            if (submit)
                db.SubmitChanges();

            return blobID;
        }

        public void Write(Guid caseId, Guid blobid, string path, byte[] data, int length, object state)
        {
            Database db = state as Database;
            bool submit = db == null;

            if (submit)
                db = new Database();

            var blob = db.Blobs.Where(b => b.caseId == caseId && string.Compare(path, b.path, StringComparison.InvariantCultureIgnoreCase) == 0).FirstOrDefault();

            if (blob == null)
            {
                blob = new Blob
                {
                    blobId = blobid,
                    caseId = caseId,
                    path = path
                };

                db.Blobs.InsertOnSubmit(blob);
            }
            else blob.blobId = blobid;


            if (data.Length != length)
            {
                byte[] trimmed = new byte[length];
                for (int i = 0; i < length; ++i)
                    trimmed[i] = data[i];

                data = trimmed;
            }

            blob.binaryData = new System.Data.Linq.Binary(data);

            if (submit)
                db.SubmitChanges();
        }


        public byte[] Read(Guid caseId, string path, object state)
        {
            Database db = state as Database;
            if(db == null)
                db = new Database();

            var binary = (from b in db.Blobs
						  where b.caseId == caseId && b.path == path
                          select b.binaryData).FirstOrDefault();

            return binary == null ? null : binary.ToArray();
        }

        public byte[] Read(Guid blobID, object state)
        {
            Database db = state as Database;
            if (db == null)
                db = new Database();

            var binary = (from b in db.Blobs
                          where b.blobId == blobID
                          select b.binaryData).FirstOrDefault();

            return binary == null ? null : binary.ToArray();
        }

        public bool Exists(Guid caseId, string path, object state)
        {
            Database db = state as Database;
            if (db == null)
                db = new Database();

            return db.Blobs.Count(b => b.caseId == caseId && path == b.path) > 0;
        }

        public Guid IdOf(Guid caseId, string path, object state)
        {
            Database db = state as Database;
            if (db == null)
                db = new Database();

            var blb = db.Blobs.Where(b => b.caseId == caseId && path == b.path).FirstOrDefault();
            return blb.blobId;
        }

        public bool Exists(Guid blobID, object state)
        {
            Database db = state as Database;
            if (db == null)
                db = new Database();

            return db.Blobs.Count(b => b.blobId == blobID) > 0;
        }

        public void Delete(Guid caseId, string path, object state)
        {
            bool submit = false;
            Database db = state as Database;

            if (db == null)
            {
                db = new Database();
                submit = true;
            }

            db.Blobs.DeleteAllOnSubmit(db.Blobs.Where(b => b.caseId == caseId && String.Compare(path, b.path, StringComparison.InvariantCultureIgnoreCase) == 0));
            
            if(submit)
                db.SubmitChanges();
        }

        public void Delete(Guid blobID,object state)
        {
            bool submit = false;
            Database db = state as Database;

            if (db == null)
            {
                db = new Database();
                submit = true;
            }

            db.Blobs.DeleteAllOnSubmit(db.Blobs.Where(b => b.blobId == blobID));

            if (submit)
                db.SubmitChanges();
        }

        public Guid IdOf(Guid caseId, string path, Database db)
        {
            var blb = db.Blobs.Where(b => b.caseId == caseId && path == b.path).FirstOrDefault();
            return blb.blobId;
        }
    }

    public static class BlobStream
    {
        private static IBlobStreamer streamer;
        //private static BlobCache cache;

        static BlobStream()
        {
            streamer = new DatabaseBlobStreamer();

            //cache = new BlobCache();
        }

        public static Stream ReadBlobStream(Guid caseId, string path, Database db, CacheOption cacheOption)
        {
            //var cached = cache.Get(caseId, path);
            //if (cached != null)
            //{
              //  return new MemoryStream(cached);
            //}

            var data = streamer.Read(caseId, path, db);
            if (data == null)
                return null;

            //if (cacheOption == CacheOption.Cache)
              //  cache.AddToCache(data, caseId, path);

            return new MemoryStream(data);
        }

        public static Guid WriteBlobStream(Guid caseId, string path, byte[] data)
        {
            return WriteBlobStream(caseId, path, data, data.Length, null);
        }

        public static Guid WriteBlobStream(Guid caseId, string path, byte[] data, Database db)
        {
            return WriteBlobStream(caseId, path, data, data.Length, db);
        }

        public static Guid WriteBlobStream(Guid caseId, string path, MemoryStream stream)
        {
            return WriteBlobStream(caseId, path, stream.GetBuffer(), (int)stream.Length, null);
        }

        public static Guid WriteBlobStream(Guid caseId, string path, MemoryStream stream, Database db)
        {
            return WriteBlobStream(caseId, path, stream.GetBuffer(), (int)stream.Length, db);
        }

        public static Guid WriteBlobStream(Guid caseId, string path, byte[] data, int length)
        {
            return WriteBlobStream(caseId, path, data, length, null);
        }

        public static Guid WriteBlobStream(Guid caseId, string path, byte[] data, int length, Database db)
        {
            return streamer.Write(caseId, path, data, length, db);
        }

        public static void WriteBlobStream(Guid caseId, Guid blobid, string path, byte[] data, int length, Database db)
        {
            streamer.Write(caseId, blobid, path, data, length, db);
        }

        public static void Delete(Guid caseId, string path)
        {
            Delete(caseId, path, null);
        }

        public static void Delete(Guid caseId, string path, Database db)
        {
            streamer.Delete(caseId, path, db);
        }

        public static void Delete(Guid blobId, Database db)
        {
            streamer.Delete(blobId, db);
        }


        public static bool Exists(Guid caseId, string path, Database db)
        {
            return streamer.Exists(caseId, path, db);
        }

        public static Guid IdOf(Guid caseId, string path, Database db)
        {
            return streamer.IdOf(caseId, path, db);
        }
    }
}
