﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace AI.Connect
{
    public enum CacheOption
    {
        Cache,
        DontCache
    }

    internal class BlobCacheItem
    {
        public string Path { get; set; }
        public Guid CaseId { get; set; }
        public byte[] Data { get; set; }
    }

    public sealed class BlobCache
    {
        private const long DefaultMaxCacheSize = 100 * 1024;
        private long maxSizeKb;
        private Dictionary<string, BlobCacheItem> cache;
        private long currentSize;
        private Guid lastCaseId;
        private object sync = new object();

        public BlobCache()
        {
            cache = new Dictionary<string, BlobCacheItem>();
            maxSizeKb = DefaultMaxCacheSize;
        }

        public long MaxCacheSizeKb
        {
            get { return maxSizeKb; }
            set
            {
                maxSizeKb = value;
                Compact();
            }
        }

        private long TotalSizeInBytes
        {
            get
            {
                return currentSize;
            }
        }

        private void Compact()
        {
            long s = maxSizeKb * 1024;
 	        while(TotalSizeInBytes > s)
            {
                //Console.WriteLine("Cache DEL:           {0}", cache.First.Value.Path);

                var itemtoDel = cache.First();
                currentSize -= itemtoDel.Value.Data.Length;
                cache.Remove(itemtoDel.Key);
            }
        }

        public void AddToCache(Stream stream, Guid caseId, string path)
        {
            byte[] data = null;

            if (stream is MemoryStream)
            {
                data = ((MemoryStream)stream).GetBuffer();
            }
            else
            {
                int len = (int)stream.Length;
                data = new byte[len];
                stream.Read(data, 0, len);
            }

            AddToCache(data, caseId, path);
        }

        public void AddToCache(byte[] data, Guid caseId, string path)
        {
            if (caseId != lastCaseId)
            {
                Clear();
            }

            lastCaseId = caseId;

            lock (sync)
            {
                if (!cache.ContainsKey(path))
                {
                    cache.Add(path, new BlobCacheItem
                        {
                            CaseId = caseId,
                            Data = data,
                            Path = path
                        });

                    currentSize += data.Length;

                    Compact();
                }
            }
        }

        public byte[] Get(Guid caseId, string path)
        {
            if (cache.ContainsKey(path))
                return cache[path].Data;

            return null;
        }

        public void Clear()
        {
            cache = new Dictionary<string, BlobCacheItem>();
        }
    }
}
