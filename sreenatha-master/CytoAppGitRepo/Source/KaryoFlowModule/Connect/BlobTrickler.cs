﻿using System;
using System.Threading;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace AI.Connect
{
    public class BlobTrickler
    {
        private class Item
        {
            public Guid CaseId { get; set; }
            public string Path { get; set; }
            public byte[] Blob { get; set; }
        }

        private BlockingCollection<Item> queue;
        private Task trickle;


        public BlobTrickler()
        {
            queue = new BlockingCollection<Item>();
            trickle = Task.Factory.StartNew(ThreadMain);
        }

        public void Enqueue(Guid caseId, string blobPath, byte[] blob)
        {
            queue.Add(new Item
            {
                CaseId = caseId,
                Blob = blob,
                Path = blobPath
            });
        }

        void ThreadMain()
        {
            var enumerable = queue.GetConsumingEnumerable();
            foreach(var item in enumerable)
            {
                var db = new Connect.Database();
                if (Connect.BlobStream.Exists(item.CaseId, item.Path, db))
                    continue;

                Connect.BlobStream.WriteBlobStream(item.CaseId, item.Path, item.Blob, db);

                db.SubmitChanges();
                db.Dispose();
                db = null;
            }
        }

        public void WaitUntilComplete()
        {
            queue.CompleteAdding();
            Task.WaitAll(new Task[] { trickle });
        }
    }
}
