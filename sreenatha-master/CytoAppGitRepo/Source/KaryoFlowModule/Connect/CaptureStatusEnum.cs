﻿namespace AI
{
    public enum CaptureStatus
    {
        Ready,
        Capturing,
        Captured,
        Cancelled,
        FailedFocus
    }
}
