﻿using System;
using System.Collections.Generic;
using System.Linq;
using AI.Connect;
using System.Xml.Linq;
using System.IO;
using System.ComponentModel;

namespace AI
{
    /// <summary>
    /// DEPRECATED
    /// </summary>
    public class CaseFinder : INotifyPropertyChanged
    {
        private string search = string.Empty;
        private IEnumerable<Case> results;
        private int total, resultsLength;

        public CaseFinder()
        {
        }

        public string Search
        {
            get { return search; }
            set 
            { 
                search = value; 
                Notify("Search");
                //while (!DoSearch())
                //{
                //    Connect.ConnectionTest.Test(System.Windows.Application.Current.MainWindow);
                //}
                DoSearch();
            }
        }

        public void RecentCases()
        {
            search = "";
            Notify("Search");

            //using (var lims = Connect.LIMS.New())
            //{
            //    total = lims.CaseCount();
            //    var hits = lims.FindRecentCases(10).ToArray();
            //    results = hits;
            //    resultsLength = hits.Length;
            //    Notify("Results", "TotalNumberOfCases", "ResultsLength");
            //}
        }

        private bool DoSearch()
        {
            var dataSource = DataSourceFactory.GetDataSource();

            total = 0;
//            total = dataSource.GetCases(null).Count(); // bit slack, maybe IDataSource to count nodes would be more effecient
            var r = dataSource.GetCases(search.ToLower()).Take(10).ToArray();
            results = r;
            resultsLength = r.Length;

            Notify("Results", "TotalNumberOfCases", "ResultsLength");
            return true;
        }

        public int TotalNumberOfCases
        {
            get { return total; }
        }

        public int ResultsLength
        {
            get { return resultsLength; }
        }

        public IEnumerable<Case> Results
        {
            get
            {
                return results;
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void Notify(params string[] properties)
        {
            foreach (var propertyName in properties)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        public void Refresh()
        {
            DoSearch();
        }
    }
}
