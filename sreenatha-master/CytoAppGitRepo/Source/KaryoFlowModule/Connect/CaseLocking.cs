﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI
{
    public class CaseLockInfo
    {
        public string LockedBy { get; private set; }
        public DateTime LockedWhen { get; private set; }

        public static CaseLockInfo ForCase(Case c)
        {
            if(c == null)
                return null;

            return null;

            //using (var db = new Connect.Database())
            //{
            //    db.Connection.Open();
            //    db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

            //    var caseId = db.CaseIdFor(c);
            //    if (caseId == Guid.Empty)
            //        return null;

            //    var info = db.CaseLocks.Where(l => l.caseId == caseId).FirstOrDefault();
            //    if (info == null)
            //        return null;

            //    return new CaseLockInfo
            //    {
            //        LockedBy = info.locker,
            //        LockedWhen = info.date
            //    };
            //}
        }

        public static CaseLockInfo ForCase(Guid caseId)
        {
            if (caseId == Guid.Empty)
                return null;

            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                
                var info = db.CaseLocks.Where(l => l.caseId == caseId).FirstOrDefault();
                if (info == null)
                    return null;

                return new CaseLockInfo
                {
                    LockedBy = info.locker,
                    LockedWhen = info.date
                };
            }
        }
    }

    public static class CaseLocking
    {
        public static void AcquireCaseLock(Case c)
        {
            using (var db = new Connect.Database())
            {
                var caseId = db.CaseIdFor(c);

                Connect.CaseLock caseLock = new AI.Connect.CaseLock
                {
                    caseId = caseId,
                    date = DateTime.Now,
                    locker = Environment.UserName
                };

                db.CaseLocks.InsertOnSubmit(caseLock);
                db.SubmitChanges();
            }
        }

        public static void AcquireCaseLock(Guid caseId)
        {
            if (caseId == Guid.Empty) return;

            using (var db = new Connect.Database())
            {
                Connect.CaseLock caseLock = new AI.Connect.CaseLock
                {
                    caseId = caseId,
                    date = DateTime.Now,
                    locker = Environment.UserName
                };

                db.CaseLocks.InsertOnSubmit(caseLock);
                db.SubmitChanges();
            }
        }


        public static void ReleaseCaseLock(Case c)
        {
            //using (var db = new Connect.Database())
            //{
            //    var caseId = db.CaseIdFor(c);
            //    db.CaseLocks.DeleteAllOnSubmit(db.CaseLocks.Where(cl => cl.caseId == caseId));
            //    db.SubmitChanges();
            //}
        }

        public static void ReleaseCaseLock(Guid caseId)
        {
            if (caseId == Guid.Empty) return;

            using (var db = new Connect.Database())
            {
                db.CaseLocks.DeleteAllOnSubmit(db.CaseLocks.Where(cl => cl.caseId == caseId));
                db.SubmitChanges();
            }
        }

    }
}
