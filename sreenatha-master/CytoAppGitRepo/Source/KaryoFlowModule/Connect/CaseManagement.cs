﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using AI.Connect;
using System.IO;

namespace AI
{
    public class CaseManagement
    {
        public static Case CreateCase(string caseName)
        {
            // NOTE: we can assume that this will never be called when using a nondefault lims

            using(Database db = new Database())
            using(ILims lims = Connect.LIMS.New())
            {          
                var lc = new Connect.LMF_Case()
                {
                    id = Guid.NewGuid(),
                    name = caseName,
                    created = DateTime.Now
                };

                db.LMF_Cases.InsertOnSubmit(lc);

                var dc = new Connect.Case()
                {
                    caseId = Guid.NewGuid(),
                    limsRef = lc.id.ToString(),
                    status = "",
                    version = 46000
                };

                db.Cases.InsertOnSubmit(dc);
                db.SubmitChanges();

                return new Case
                {
                    Id = dc.limsRef,
                    Created = DateTime.Now,
                    Name = caseName
                };
            }
        }

        public static void DeleteCase(Case @case)
        {
            using (var db = new Connect.Database())
            {
                var caseToKill = db.Cases.Where(c => c.limsRef == @case.Id).FirstOrDefault();
                db.Cases.DeleteOnSubmit(caseToKill);

                if (LIMS.CanCreateCases)
                {
                    var limsCaseToKill = db.LMF_Cases.Where(c => c.id == new Guid(@case.Id)).First();
                    db.LMF_Cases.DeleteOnSubmit(limsCaseToKill);
                }

                db.SubmitChanges();
            }
        }

        public static Slide CreateSlide(Case c, string name, string barcode, string slideType)
        {
            return CreateSlide(c, name, barcode, DateTime.Now, slideType, "");
        }

        public static Slide CreateSlide(Case lc, string slidename, string slideBarcode, DateTime createTime, string slideType, string culture)
        {
            //using(var db = new Database())
            //{
            //    var dbc = db.Cases.Where(c => c.limsRef == lc.Id).First();
            //    Guid id = new Guid(lc.Id);
            //    var lmf_case = db.LMF_Cases.Where(l => l.id == id).First();

            //    var ls = new Connect.LMF_Slide
            //    {
            //        id = Guid.NewGuid(),
            //        name = slidename,
            //        barcode = slideBarcode,
            //        LMF_Case = lmf_case
            //    };

            //    db.LMF_Slides.InsertOnSubmit(ls);

            //    var dbSlide = new Connect.Slide
            //    {
            //        caseId = dbc.caseId,
            //        limsRef = ls.id.ToString(),
            //        slideId = Guid.NewGuid()
            //    };

            //    db.Slides.InsertOnSubmit(dbSlide);
            //    db.SubmitChanges();

            //    Slide slide = new Slide();
            //    slide.Barcode = slideBarcode;
            //    slide.Id = dbSlide.limsRef;
            //    slide.Name = slidename;

            //    return slide;
            //}
            var dataSource = DataSourceFactory.GetDataSource();
            return dataSource.CreateSlide(lc.Name, slidename);
        }
    }
}
