﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.Connect
{
    public partial class SourceDataContext
    {
        public IEnumerable<Guid> GetTubsId()
        {
            return ExecuteQuery<Guid>(@"select service_broker_guid from sys.databases where name='tempdb'");
        }

        public bool TubsExists()
        {
            return ExecuteQuery<int>(@"select count(*) from master.dbo.sysdatabases where name = 'CytovisionDb'").FirstOrDefault() > 0;
        }
    }

}
