﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace AI.Connect
{
    public static class FileSystemBlobPoolInfo
    {
        public static string GetBlobPath(string connection)
        {
			string blobpath = null;

            Database db = string.IsNullOrEmpty(connection) ? new Database() : new Database(connection);
			using (db)
			{
				var blobpool = db.Configs.Where(c => c.title.ToLower() == "blobpool").FirstOrDefault();
				if (blobpool != null && !string.IsNullOrEmpty(blobpool.text))
				{
					blobpath = blobpool.text;
				}
			}

			return blobpath;
        }

        public static void SetBlobPath(string connection, string value)
        {
            Database db = string.IsNullOrEmpty(connection) ? new Database() : new Database(connection);
			using (db)
			{
				var blobpool = db.Configs.Where(c => c.title.ToLower() == "blobpool").FirstOrDefault();
				if (blobpool == null)
				{
					blobpool = new Config { id = Guid.NewGuid(), title = "blobpool", text = value };
					db.Configs.InsertOnSubmit(blobpool);
				}
				else
					blobpool.text = value;
				db.SubmitChanges();
			}
        }
    }

    internal class FileSystemBlobStreamer : IBlobStreamer
    {
        public static string BlobPoolPath
        {
            get
            {
                return FileSystemBlobPoolInfo.GetBlobPath(null);
            }
            set
            {
                FileSystemBlobPoolInfo.SetBlobPath(null, value);
            }
        }

        static string CaseFolderPath(Guid caseId)
        {
            string blobpool = BlobPoolPath;
            if (string.IsNullOrEmpty(blobpool))
                throw new InvalidDataException("BlobPool must be define in the database");

            var p = Path.Combine(blobpool, caseId.ToString());
            if (!Directory.Exists(p))
                Directory.CreateDirectory(p);

            return p;
        }

        public Guid Write(Guid caseId, string path, byte[] data, int length, object state)
        {
            Database db = state as Database;
            bool submit = db == null;
            Guid blobID = Guid.Empty;

            if (submit)
                db = new Database();

            var blob = db.Blobs.Where(b => b.caseId == caseId && string.Compare(path, b.path, StringComparison.InvariantCultureIgnoreCase) == 0).FirstOrDefault();

            if (blob == null)
            {
                blobID = Guid.NewGuid();

                blob = new Blob
                {
                    blobId = blobID,
                    caseId = caseId,
                    path = path
                };

                db.Blobs.InsertOnSubmit(blob);
            }
            else blobID = blob.blobId;

            if (data.Length != length)
            {
                byte[] trimmed = new byte[length];
                for (int i = 0; i < length; ++i)
                    trimmed[i] = data[i];

                data = trimmed;
            }

            string filePath = Path.Combine(CaseFolderPath(caseId), path);
            WriteBinaryFile(filePath, data);

            if (submit)
                db.SubmitChanges();

            return blobID;
        }

        private void WriteBinaryFile(string path, byte[] data)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(data);
                bw.Close();
                fs.Close();
            }
        }

        private byte[] ReadBinaryFile(string path)
        {
            byte[] data = null;
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                data = new byte[fs.Length];
                fs.Read(data, 0, (int)fs.Length);
                fs.Close();
            }
            return data;
        }

        private void DeleteCaseBlobs(string path)
        {
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }
        }

        private void DeleteBlob(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        public void Write(Guid caseId, Guid blobid, string path, byte[] data, int length, object state)
        {
            Database db = state as Database;
            bool submit = db == null;

            if (submit)
                db = new Database();

            var blob = db.Blobs.Where(b => b.caseId == caseId && string.Compare(path, b.path, StringComparison.InvariantCultureIgnoreCase) == 0).FirstOrDefault();

            if (blob == null)
            {
                blob = new Blob
                {
                    blobId = blobid,
                    caseId = caseId,
                    path = path
                };

                db.Blobs.InsertOnSubmit(blob);
            }
            else blob.blobId = blobid;


            if (data.Length != length)
            {
                byte[] trimmed = new byte[length];
                for (int i = 0; i < length; ++i)
                    trimmed[i] = data[i];

                data = trimmed;
            }

            string filePath = Path.Combine(CaseFolderPath(caseId), path);
            WriteBinaryFile(filePath, data);

            if (submit)
                db.SubmitChanges();
        }


        public byte[] Read(Guid caseId, string path, object state)
        {
            Database db = state as Database;
            if (db == null)
                db = new Database();

            var blobs = from b in db.Blobs
                        where b.caseId == caseId && b.path == path
                        select b.blobId;

            var blob = blobs.FirstOrDefault();
            if (blob == null)
                return null;

            string filePath = Path.Combine(CaseFolderPath(caseId), path);
            return ReadBinaryFile(filePath);
        }

        public byte[] Read(Guid blobID, object state)
        {
            Database db = state as Database;
            if (db == null)
                db = new Database();

            var binary = (from b in db.Blobs
                          where b.blobId == blobID
                          select b).FirstOrDefault();

            string filePath = Path.Combine(CaseFolderPath(binary.caseId), binary.path);
            return ReadBinaryFile(filePath);
        }

        public bool Exists(Guid caseId, string path, object state)
        {
            Database db = state as Database;
            if (db == null)
                db = new Database();

            return db.Blobs.Count(b => b.caseId == caseId && path == b.path) > 0;
        }

        public Guid IdOf(Guid caseId, string path, object state)
        {
            Database db = state as Database;
            if (db == null)
                db = new Database();

            var blb = db.Blobs.Where(b => b.caseId == caseId && path == b.path).FirstOrDefault();
            return blb.blobId;
        }

        public bool Exists(Guid blobID, object state)
        {
            Database db = state as Database;
            if (db == null)
                db = new Database();

            return db.Blobs.Count(b => b.blobId == blobID) > 0;
        }

        public void Delete(Guid caseId, string path, object state)
        {
            bool submit = false;
            Database db = state as Database;

            if (db == null)
            {
                db = new Database();
                submit = true;
            }

            db.Blobs.DeleteAllOnSubmit(db.Blobs.Where(b => b.caseId == caseId && String.Compare(path, b.path, StringComparison.InvariantCultureIgnoreCase) == 0));
            DeleteCaseBlobs(CaseFolderPath(caseId));

            if (submit)
                db.SubmitChanges();
        }

        public void Delete(Guid blobID, object state)
        {
            bool submit = false;
            Database db = state as Database;

            if (db == null)
            {
                db = new Database();
                submit = true;
            }

            var blb = db.Blobs.Where( b => b.blobId == blobID ).FirstOrDefault();

            if (blb != null)
            {
                db.Blobs.DeleteAllOnSubmit(db.Blobs.Where(b => b.blobId == blobID));
                DeleteBlob(blb.path);
            }

            if (submit)
                db.SubmitChanges();
        }

        public Guid IdOf(Guid caseId, string path, Database db)
        {
            var blb = db.Blobs.Where(b => b.caseId == caseId && path == b.path).FirstOrDefault();
            return blb.blobId;
        }
    }

}
