﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.Connect
{
    public static class LIMS
    {
        private static Type limsType;

        public static void SetType(Type type)
        {
            limsType = type;
        }

        public static bool CanCreateCases
        {
            get { return limsType == typeof(LimsFallback.LimsBase); }
        }

        public static ILims New()
        {
            return (ILims)Activator.CreateInstance(limsType);
        }

        public static string CaseName(string limsCaseId)
        {
            using (var lims = New())
            {
                return lims.GetCase(limsCaseId).Name;
            }
        }

        public static AI.Case Case(Guid caseId)
        {
            using (var db = new Connect.Database())
            {
                var c = db.Cases.Where(cc => cc.caseId == caseId).FirstOrDefault();
                if (c == null)
                    return null;

                using (var lims = New())
                {
                    return lims.GetCase(c.limsRef);
                }
            }
        }

        public static string CaseName(Guid cvCaseId)
        {
            var c = Case(cvCaseId);
            return c == null ? "" : c.Name;
        }


        public static AI.Slide Slide(Guid cvSlideId)
        {
            using (var db = new Connect.Database())
            {
                var slide = db.Slides.Where(s => s.slideId == cvSlideId).FirstOrDefault();
                if (slide == null)
                    return null;

                using (var lims = New())
                {
                    var ls = lims.GetSlide(slide.limsRef);
                    if (ls == null)
                        return null;

                    return ls;
                }
            }
        }

        public static string SlideName(string limsSlideId)
        {
            using (var lims = New())
            {
                var ls = lims.GetSlide(limsSlideId);
                return ls == null ? "" : ls.Name;
            }
        }

        public static string SlideName(Guid cvSlideId)
        {
            var ls = Slide(cvSlideId);
            return ls == null ? "" : ls.Name;
        }
    }

}
