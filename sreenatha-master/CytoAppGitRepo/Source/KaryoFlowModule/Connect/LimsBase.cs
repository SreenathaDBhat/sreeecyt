﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.LimsFallback
{
    public class LimsBase : ILims
    {
        public void Dispose()
        {
        }

        public int CaseCount()
        {
            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                return db.LMF_Cases.Count();
            }
        }

        public bool CanCreateCases
        {
            get { return true; }
        }

        public bool CanCreateSlides
        {
            get { return false; }
        }

        public IEnumerable<Case> FindRecentCases(int maxHits)
        {
            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var cases = (from c in db.LMF_Cases
                             orderby c.created descending
                             select new Case
                             {
                                 Id = ConvertId(c.id),
                                 Name = c.name,
                                 Created = c.created

                             }).Take(maxHits);

                return cases.ToArray();
            }
        }

        public IEnumerable<Case> FindCases(string searchTerm)
        {
            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                
                var cases = from c in db.LMF_Cases
                            where c.name.Contains(searchTerm)
                            orderby c.name ascending
                            select new Case 
                            { 
                                Id = ConvertId(c.id), 
                                Name = c.name
                            };

                return cases.ToList();
            }
        }

        private string ConvertId(Guid guid)
        {
            return guid.ToString().ToLower();
        }

        private Guid ConvertId(string guidString)
        {
            return new Guid(guidString);
        }

        public void MarkCaseAsModified(Case c, DateTime modifiedTime, string description)
        {
            // we dont track modifications
        }

        public Case FindCaseForSlide(Slide slide)
        {
            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                
                var slideId = ConvertId(slide.Id);
                var s = db.LMF_Slides.Where(ls => ls.id == slideId).FirstOrDefault();
                if (s == null || s.LMF_Case == null)
                    return null;

                return new Case
                {
                    Id = ConvertId(s.LMF_Case.id),
                    Created = DateTime.Now,
                    Name = s.LMF_Case.name
                };
            }
        }

        public Case CreateCase(string casename)
        {
            var lcase = new Connect.LMF_Case { id = Guid.NewGuid(), name = casename, created = DateTime.Now };

            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                db.LMF_Cases.InsertOnSubmit(lcase);
                db.SubmitChanges();
            }

            return new Case
            {
                Id = ConvertId(lcase.id),
                Created = new DateTime(2000, 1, 1),
                Name = lcase.name
            };
        }

        public Case FindCase(string caseName)
        {
            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var limscase = (from c in db.LMF_Cases
                                where c.name == caseName
                                select new Case { Id = ConvertId(c.id), Name = c.name }).FirstOrDefault();
                return limscase;
            }
        }

        public Case GetCase(string caseId)
        {
            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var limscase = (from c in db.LMF_Cases
                               where c.id == ConvertId(caseId)
                               select new Case { Id = ConvertId(c.id), Name = c.name }).FirstOrDefault();
                return limscase;
            }
        }

        public void DeleteCase(Case c)
        {
            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var caseToDelete = db.LMF_Cases.Where(lmfCase => lmfCase.id == ConvertId(c.Id)).First();
                db.LMF_Cases.DeleteOnSubmit(caseToDelete);
                db.SubmitChanges();
            }
        }

        public IEnumerable<MetaDataItem> GetCaseMetaData(Case c)
        {
            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                
                var caseMetadata = from m in db.LMF_CaseMetaDatas
                                   where m.limsCaseId == ConvertId(c.Id)
                                   select new MetaDataItem { Field = m.field, Value = m.value };
                return caseMetadata.ToList();
            }
        }

        public void SetCaseMetaData(Case c, IEnumerable<MetaDataItem> caseMetadata)
        {
            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var oldMeta = from om in db.LMF_CaseMetaDatas where om.limsCaseId == ConvertId(c.Id) select om;
                db.LMF_CaseMetaDatas.DeleteAllOnSubmit(oldMeta);

                var guid = ConvertId(c.Id);
                var dbCase = db.LMF_Cases.Where(cc => cc.id == guid).First();

                var dbMeta = from m in caseMetadata
                             select new Connect.LMF_CaseMetaData 
                             { 
                                 id = Guid.NewGuid(),
                                 field = m.Field, 
                                 value = m.Value,
                                 limsCaseId = dbCase.id
                             };

                db.LMF_CaseMetaDatas.InsertAllOnSubmit(dbMeta);
                db.SubmitChanges();
            }
        }

        public Slide CreateSlide(Case c, string name)
        {
            var caseId = ConvertId(c.Id);
            var s = new Connect.LMF_Slide { id = Guid.NewGuid(), limsCaseId = caseId, name = name };

            using (var db = new Connect.Database())
            {
                db.LMF_Slides.InsertOnSubmit(s);
                db.SubmitChanges();
            }

            return new Slide
            {
                Barcode = string.Empty,
                Id = ConvertId(s.id),
                Name = s.name,
                Status = string.Empty
            };
        }

        public Slide FindSlide(string barcode)
        {
            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                return (from s in db.LMF_Slides
                        where s.barcode == barcode
                        select new Slide
                        {
                            Barcode = s.barcode,
                            Id = ConvertId(s.id),
                            Name = s.name,
                            Status = ""
                        }).First();
            }
        }

        public void DeleteSlide(Slide slide)
        {
            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                
                var slideToDelete = db.LMF_Slides.Where(lmfSlide => lmfSlide.id == ConvertId(slide.Id)).First();
                db.LMF_Slides.DeleteOnSubmit(slideToDelete);
                db.SubmitChanges();
            }
        }

        public IEnumerable<Slide> GetSlides(Case c)
        {
            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                Guid caseId = ConvertId(c.Id);

                var slides = from s in db.LMF_Slides
                             where s.limsCaseId == caseId
                             orderby s.name
                             select new Slide { Id = ConvertId(s.id), Name = s.name, Barcode = s.barcode, Status = string.Empty };

                return slides.ToList();
            }
        }

        public Slide GetSlide(string slideId)
        {
            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var limsSlide = (from s in db.LMF_Slides
                                 where s.id == ConvertId(slideId)
                                 select new Slide { Id = ConvertId(s.id), Name = s.name, Barcode = s.barcode, Status = string.Empty }).FirstOrDefault();
                
                return limsSlide;
            }
        }

        public IEnumerable<MetaDataItem> GetSlideMetaData(Slide s)
        {
            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var slideMetadata = from m in db.LMF_SlideMetaDatas
                                    where m.limsSlideId == ConvertId(s.Id)
                                    select new MetaDataItem 
                                    { 
                                        Field = m.field, 
                                        Value = m.value
                                    };

                return slideMetadata.ToList();
            }
        }

        public void SetSlideMetaData(Slide s, IEnumerable<MetaDataItem> metaData)
        {
            using (var db = new Connect.Database())
            {
                db.Connection.Open();
                db.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var guid = ConvertId(s.Id);
                
                var oldMeta = from om in db.LMF_SlideMetaDatas where om.limsSlideId == guid select om;
                db.LMF_SlideMetaDatas.DeleteAllOnSubmit(oldMeta);

                var dbSlide = db.LMF_Slides.Where(c => c.id == guid).First();

                var dbMeta = from m in metaData
                             select new Connect.LMF_SlideMetaData
                             {
                                 id = Guid.NewGuid(),
                                 field = m.Field,
                                 value = m.Value,
                                 limsSlideId = dbSlide.id
                             };

                db.LMF_SlideMetaDatas.InsertAllOnSubmit(dbMeta);
                db.SubmitChanges();
            }
        }

        public void CompleteCase(Case c, IEnumerable<CellImagePair> images)
        {
        }
    }
}
