﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Collections;

namespace AI
{
    public class LocalizableString
    {
        private string pluralizedAlternative;

        public string Content { get; set; }
        public string Plural
        {
            get
            {
                if (pluralizedAlternative == null)
                    return Content;

                return pluralizedAlternative;
            }
            set
            {
                pluralizedAlternative = value;
            }
        }
    }


    public class StringTable
    {
        private Dictionary<string, LocalizableString> table;

        public StringTable(ResourceDictionary resources)
        {
            table = new Dictionary<string, LocalizableString>();

            foreach (DictionaryEntry r in resources)
            {
                if (r.Value == null && !(r.Value is LocalizableString))
                    continue;

                table.Add((string)r.Key, (LocalizableString)r.Value);
            }
        }

        private StringTable(int x)
        {
            table = new Dictionary<string, LocalizableString>();
        }

        public static StringTable Empty
        {
            get { return new StringTable(1); }
        }

        public string Lookup(string key)
        {
            if (table.ContainsKey(key))
                return table[key].Content;

            return string.Empty;
        }

        public string LookupPlural(string key)
        {
            if (table.ContainsKey(key))
                return table[key].Plural;

            return string.Empty;
        }

        public string Lookup(string key, int pluralTest)
        {
            if (pluralTest == 1)
                return Lookup(key);
            else
                return LookupPlural(key);

        }
    }
}
