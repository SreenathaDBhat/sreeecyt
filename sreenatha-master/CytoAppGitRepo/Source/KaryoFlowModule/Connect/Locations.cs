﻿using System;
using System.IO;
using Microsoft.Win32;
using System.Xml.Linq;
using System.ComponentModel;
using System.Data.SqlClient;

namespace AI.Connect
{
    public static class Locations
    {
        static Locations()
        {
            SSHUsername = SSHPassword = SSHServer = string.Empty; 
            FileInfo configFile = ConfigFile;
            if(!configFile.Exists)
            {
                UseSSH = false;
                ConnectionString = "Data Source=.\\SQLExpress;Integrated Security=True;Initial Catalog=CytovisionDb";
                BlobPool = "d:\\casebase\\BlobPool";
                return;
            }

            XElement xml = XElement.Load(configFile.FullName);
            ConnectionString = xml.Attribute("ConnectionString").Value;
            BlobPool = xml.Attribute("BlobPool").Value;

            UseSSH = (xml.Attribute("UseSSH")!= null && xml.Attribute("UseSSH").Value == "true");
            SSHPort = (xml.Attribute("SSHPort") != null) ? int.Parse(xml.Attribute("SSHPort").Value) : 0;
            SSHUsername = (xml.Attribute("SSHUsername") != null) ? xml.Attribute("SSHUsername").Value : string.Empty;
            SSHPassword = (xml.Attribute("SSHPassword") != null) ? xml.Attribute("SSHPassword").Value : string.Empty;
            SSHServer = (xml.Attribute("SSHServer") != null) ? xml.Attribute("SSHServer").Value : string.Empty;
        }

        public static void Save()
        {
            XElement xml = new XElement("Network",  new XAttribute("ConnectionString", ConnectionString), 
                                                    new XAttribute("BlobPool", BlobPool),
                                                    new XAttribute("UseSSH", UseSSH),
                                                    new XAttribute("SSHPort", SSHPort),
                                                    new XAttribute("SSHUsername", SSHUsername),
                                                    new XAttribute("SSHPassword", SSHPassword),
                                                    new XAttribute("SSHServer", SSHServer));

            FileInfo configFile = ConfigFile;
            if(configFile.Exists)
                configFile.Delete();

            xml.Save(configFile.FullName);
        }

        public static FileInfo ConfigFile
        {
            get { return AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\network.cfg"); }
        }

        public static string ConnectionString
        {
            get;
            set;
        }

        public static string BlobPool
        {
            get;
            set;
        }

        public static string SSHServer { get; set; }
        public static int SSHPort { get; set; }
        public static string SSHUsername { get; set; }
        public static string SSHPassword { get; set; }
        public static bool UseSSH { get; set; }


        public static bool UsingDatabaseBlobPool
        {
            get { return true; }
        }

        internal static string CaseFolderPath(Guid caseId)
        {
            var p = Path.Combine(BlobPool, caseId.ToString());
            if (!Directory.Exists(p))
                Directory.CreateDirectory(p);

            return p;
        }

        internal static DirectoryInfo CasedFolder(Guid caseId)
        {
            return new DirectoryInfo(CaseFolderPath(caseId));
        }

        public static DirectoryInfo BlobPoolDirectory
        {
            get { return new DirectoryInfo(BlobPool); }
        }

 
        private static string ValueOf(string prop)
        {
            int i = ConnectionString.IndexOf(prop);
            if (i == -1)
                return string.Empty;

            string s = ConnectionString.Substring(i, ConnectionString.Length - i);
            int m = s.IndexOf(';');
            return m == -1 ? s.Substring(prop.Length+1) : s.Substring(prop.Length+1, m - (prop.Length+1));
        }

        public static string SqlServerName
        {
            get
            {
                string s = ValueOf("Data Source");
                int slash = s.IndexOfAny(new char[] { '\\', '/' });

                return slash == -1 ? s : s.Substring(0, slash);
            }
        }

        public static string SqlInstanceName
        {
            get
            {
                string s = ValueOf("Data Source");
                int slash = s.IndexOfAny(new char[] { '\\', '/' });

                return slash == -1 ? string.Empty : s.Substring(slash + 1);
            }
        }

        public static string SqlUsername
        {
            get { return ValueOf("User ID"); }
        }

        public static string SqlPassword
        {
            get { return ValueOf("Password"); }
        }
    }
}
