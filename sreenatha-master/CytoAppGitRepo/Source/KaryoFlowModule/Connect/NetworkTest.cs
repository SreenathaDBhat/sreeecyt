﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace AI.Connect
{
    public static class NetworkTest
    {
        public static bool TestSql()
        {
            if (Locations.UseSSH)
                return TestSSH();

            try
            {
                Database db = new Database();
                db.Cases.Count();
                return true;
            }
            catch (Exception e)
            {
                Debug.Write(e.ToString());
                return false;
            }
        }

        public static bool TestSSH()
        {
            //try
            //{
            //    SSH.ConnectionError += new SSH.ConnectionErrorEvent(SSH_ConnectionError);
            //    SSH.Connect();
            //    Database db = new Database();
            //    db.Cases.Count();
            //    return true;
            //}
            //catch (Exception e)
            //{
            //    Debug.Write(e.ToString());
            //    return false;
            //}
            return false;
        }

        public static bool TestCasebase()
        {
            return Locations.BlobPoolDirectory.Exists;
        }
    }
}
