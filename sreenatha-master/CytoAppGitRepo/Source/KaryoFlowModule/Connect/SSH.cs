﻿using System;
using System.Collections.Generic;
using System.Text;
using Maverick.SSH;
using Maverick.Forwarding;

namespace AI.Connect
{
    public sealed class SSH
    {
        private static SSHConnector connector;
        private static SSHClient client;
        private static ForwardingClient forwarding;
        private static Boolean flagDisconnection = false;

        public delegate void ConnectionErrorEvent();
        public static event ConnectionErrorEvent ConnectionError;

        private SSH()
        {
        }

        public static void AddLicence()
        {
            LicenseManager.AddLicense("----BEGIN 3SP LICENSE----\r\n"
                      + "Product : Maverick .NET\r\n"
                      + "Licensee: Applied Imaging International Ltd\r\n"
                      + "Comments: Gavin Hope\r\n"
                      + "Type    : Professional Developer License\r\n"
                      + "Created : 03-Jul-2008\r\n"
                      + "\r\n"
                      + "37872013AEA13D23A600F3CF9CCEF4C82CADE29D7A743A6C\r\n"
                      + "A4BD7DE56177DD6CF04059076F212CA12AB3EB00D3DF8CFF\r\n"
                      + "71BB53954262DFB009867AB83052D60AC864234639DD7F0D\r\n"
                      + "A3AAEEAD905A69D7E00C22730E4B23CDC61B0F4B6B07C69D\r\n"
                      + "6D601ED891FEC8609A5075503A4E04B0D886529F2958918C\r\n"
                      + "F6643D11F5C71FA68DE9EB67ABD6F439DFD83BC0E385B3C9\r\n"
                      + "----END 3SP LICENSE----\r\n");

        }

        public static void Connect()
        {
            AddLicence();

            Disconnect();

            connector = SSHConnector.Create();

            client = connector.Connect(new TcpClientTransport(Locations.SSHServer, Locations.SSHPort), Locations.SSHUsername, true);
            client.StateChange += new SSHStateListener(client_StateChange);
            client.Authenticate(new PasswordAuthentication(Locations.SSHPassword));

            forwarding = new ForwardingClient(client);
            forwarding.StartLocalForwarding("127.0.0.1", 9999, "127.0.0.1", 1433);
            flagDisconnection = true;
        }

        static void client_StateChange(SSHClient client, SSHState newstate)
        {
            if ((flagDisconnection) && (newstate == SSHState.DISCONNECTED))
            {
                ConnectionError();
                flagDisconnection = false;
            }
        }

        public static void Disconnect()
        {
            flagDisconnection = false;

            if (client != null)
            {
                forwarding.StopAllLocalForwarding(true);
                client.Disconnect();

                client = null;
                forwarding = null;
                connector = null;
            }
        }
    }
}
