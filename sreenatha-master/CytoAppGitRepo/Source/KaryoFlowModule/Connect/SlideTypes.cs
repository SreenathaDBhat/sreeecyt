﻿namespace AI
{
    public static class SlideTypes
    {
        public static readonly string Default = "Not Set";
        public static readonly string Metaphase = "Metaphase";
        public static readonly string IHC = "IHC";
        public static readonly string FISH = "FISH";
    }
}
