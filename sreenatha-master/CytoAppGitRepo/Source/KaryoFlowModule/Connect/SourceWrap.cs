﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.Connect
{
    public class Database : SourceDataContext, IDisposable
    {
        public Database()
            : base(Locations.ConnectionString)
        {
        }

        public Database(string connectionString)
            : base(connectionString)
        {
        }

        public bool GotSlide(AI.Slide limsSlide)
        {
            return Slides.Where(s => s.limsRef == limsSlide.Id).FirstOrDefault() != null;
        }

        public Guid SlideIdFor(AI.Slide limsSlide)
        {
            return Slides.Where(s => s.limsRef == limsSlide.Id).First().slideId;
        }

        public bool GotCase(AI.Slide limsCase)
        {
            return Slides.Where(c => c.limsRef == limsCase.Id).FirstOrDefault() != null;
        }

        public Guid CaseIdFor(AI.Case limsCase)
        {
            if(limsCase == null)
                return Guid.Empty;

            var x = Cases.Where(c => c.limsRef == limsCase.Id).FirstOrDefault();
            if(x == null)
                return Guid.Empty;
            
            return x.caseId;
        }

        public AI.Connect.Case CreateCaseAndSlides(ILims lims, AI.Case limsCase)
        {
            var dbCase = Cases.Where(c => c.limsRef == limsCase.Id).FirstOrDefault();

            if (dbCase == null)
            {
                dbCase = new Case
                {
                    caseId = Guid.NewGuid(),
                    limsRef = limsCase.Id,
                    version = 46000,
                    status = ""
                };

                Cases.InsertOnSubmit(dbCase);
            }

            var dbSlides = Slides.Where(s => s.caseId == dbCase.caseId).ToList();

            foreach (var limsSlide in lims.GetSlides(limsCase))
            {
                var dbslide = dbSlides.Where(s => s.limsRef == limsSlide.Id).FirstOrDefault();
                if (dbslide == null)
                {
                    dbslide = new Slide
                    {
                        caseId = dbCase.caseId,
                        limsRef = limsSlide.Id,
                        slideId = Guid.NewGuid()
                    };

                    Slides.InsertOnSubmit(dbslide);
                }
            }

            SubmitChanges();

            return dbCase;
        }

        public new void Dispose()
        {
            if (base.Connection != null)
                if (base.Connection.State != System.Data.ConnectionState.Closed)
                {
                    base.Connection.Close();
                    base.Connection.Dispose();
                }

            base.Dispose();
        }
    }
}
