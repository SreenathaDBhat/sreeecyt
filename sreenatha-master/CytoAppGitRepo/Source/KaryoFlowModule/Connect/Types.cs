﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI
{
    public enum ImageType
    {
        Raw = 0,
        Fuse = 1,
        Manual = 2,
        Karyotype = 3,
        PreThresholded = 4
    }
}
