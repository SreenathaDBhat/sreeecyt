﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI
{
    public class DataSourceFactory
    {
        public static IDataSource GetDataSource()
        {
            if (DataSourceSettings.Location == null || DataSourceSettings.Location == null)
            {
                if (!DataSourceSettings.SettingsLoad() || DataSourceSettings.Location == null || DataSourceSettings.Location == null)
                    throw new InvalidDataException("FlowDataSource.settings doesn't exist or bad format");
            }

            if (DataSourceSettings.SourceType == "FolderDataSource")
                return new FolderDataSource(DataSourceSettings.Location);
            else if(DataSourceSettings.SourceType == "HttpRestDataSource")
                return new HttpRestDataSource(DataSourceSettings.Location);
            else
                throw new InvalidDataException("FlowDataSource.settings doesn't specify known DataSourceType");

        }

    }

}
