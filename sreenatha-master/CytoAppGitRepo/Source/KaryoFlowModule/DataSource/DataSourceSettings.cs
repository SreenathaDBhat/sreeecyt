﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;


namespace AI
{
    public static class DataSourceSettings
    {
        public static bool SettingsLoad()
        {
            var file = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Genetix\\FlowDataSource.settings");
            if( File.Exists(file))
            {
                var xml = XElement.Load(file);
                Location = (from x in xml.Descendants("DataSourceLocation") select x.Value).FirstOrDefault();
                SourceType = (from x in xml.Descendants("DataSourceType") select x.Value).FirstOrDefault();
                return true;
            }
            return false;
        }

        public static string Location { get; private set;}
        public static string SourceType { get; private set; }
    }

}
