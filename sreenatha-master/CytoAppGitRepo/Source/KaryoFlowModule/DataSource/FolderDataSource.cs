﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.IO;
using System.Xml.Linq;

namespace AI
{
    public class FolderDataSource: IDataSource
    {
        private string _rootPath;

        public FolderDataSource(string rootPath)
        {
            _rootPath = rootPath;
        }

        private string ParseRelativePath(string keys)
        {
            // keys is hierachy seperated by ;
            return keys.Replace(';', '/');
        }

        public bool TestConnection()
        {
            return Directory.Exists(_rootPath);
        }

        public IEnumerable<string> GetNodes(string keys)
        {
            DirectoryInfo dir = new DirectoryInfo(Path.Combine(_rootPath, ParseRelativePath(keys)));
            if (!dir.Exists)
                return new string[0];

            var list = new List<string>();
            foreach (var c in dir.EnumerateDirectories())
                list.Add(c.Name);
            return list;            
        }

        public IEnumerable<Case> GetCases(string search)
        {
            DirectoryInfo dir = new DirectoryInfo(_rootPath);
            if (!dir.Exists)
                return new Case[0];

            var list = new List<Case>();

            var pattern = "*";
            if (!string.IsNullOrWhiteSpace(search))
                pattern = "*" + search + "*";

            foreach (var c in dir.EnumerateDirectories(pattern))
                list.Add( new Case { Name = c.Name, Created = c.CreationTime, Id = Guid.NewGuid().ToString() });

            return list;
        }

        public IEnumerable<Slide> GetSlides(string caseName)
        {
            DirectoryInfo dir = new DirectoryInfo(Path.Combine(_rootPath, caseName));
            if (!dir.Exists)
                return new Slide[0];

            var list = new List<Slide>();

            foreach (var c in dir.EnumerateDirectories())
                list.Add(new Slide { Id = c.Name, Name = c.Name, Barcode = string.Empty, Status = string.Empty });

            return list;
        }

        public Slide CreateSlide(string caseName, string slideId)
        {
            DirectoryInfo dir = new DirectoryInfo(Path.Combine(_rootPath, caseName));
            if (!dir.Exists)
                return null;

            var slideFolder = dir.CreateSubdirectory(slideId);
            if (!slideFolder.Exists)
                return null;

            return new Slide { Name = slideId, Id = slideId, Barcode = string.Empty, Status = string.Empty };
        }


        private void CheckFolder(string keys)
        {
            if (!Directory.Exists(RelativeFullPath(keys)))
                Directory.CreateDirectory(RelativeFullPath(keys));
        }

        private string RelativeFullPath(string keys)
        {
            var path = _rootPath;
            var relativePath = ParseRelativePath(keys);
            if (relativePath != null)
                path = Path.Combine(_rootPath, relativePath);
            return path;
        }

        public void SaveThumbnail(string keys, BitmapSource thumbnail, string imageId)
        {
            if (thumbnail != null)
            {
                CheckFolder(keys);

                var filePath = Path.Combine(RelativeFullPath(keys), imageId);
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(thumbnail));

                using (FileStream stream = new FileStream(filePath, FileMode.Create))
                {
                    encoder.Save(stream);
                }
            }
        }

        public void SaveStream(string keys, Stream stream, string streamId)
        {
            CheckFolder(keys);

            var filePath = Path.Combine(RelativeFullPath(keys), streamId);
            using (FileStream fs = new FileStream(filePath, FileMode.Create))
            {
                stream.Seek(0, SeekOrigin.Begin);
                stream.CopyTo(fs);
            }
        }

        public Stream LoadStream(string keys, string streamId)
        {
            var filePath = Path.Combine(RelativeFullPath(keys), streamId);
            return File.Exists(filePath) ? new FileStream(filePath, FileMode.Open) : null;
        }

        public void SaveXml(string relative, XElement xml, string xmlName)
        {
            CheckFolder(relative);
            var filePath = Path.Combine(RelativeFullPath(relative), xmlName);
            xml.Save(filePath);
        }

        public XElement LoadXml(string keys, string xmlName)
        {
            var filePath = Path.Combine(RelativeFullPath(keys), xmlName);
            if (File.Exists(filePath))
                return XElement.Load(filePath);
            else
                return null;
        }

        public void DeleteItem(string keys, string itemId)
        {
            var filePath = Path.Combine(RelativeFullPath(keys), itemId);
            if (File.Exists(filePath))
                File.Delete(filePath);
        }

        public bool ItemExists(string keys, string itemId)
        {
            var filePath = Path.Combine(RelativeFullPath(keys), itemId);
            return File.Exists(filePath);
        }
    }
}
