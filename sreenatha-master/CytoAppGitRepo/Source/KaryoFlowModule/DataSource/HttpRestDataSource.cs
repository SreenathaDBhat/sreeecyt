﻿using AI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Xml.Linq;

namespace AI
{
    public class HttpRestDataSource : IDataSource
    {
        private string _baseUrl;
        public HttpRestDataSource(string url)
        {
            _baseUrl = url;
        }

        public bool TestConnection()
        {
            return ApiCheckSource();
        }

        public void SaveThumbnail(string relative, System.Windows.Media.Imaging.BitmapSource thumbnail, string imageId)
        {
            if (thumbnail != null)
            {
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(thumbnail));

                using (var ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    ApiSaveStream(relative, ms, imageId);
                }
            }
        }

        public void SaveStream(string relative, System.IO.Stream stream, string streamId)
        {
            stream.Seek(0, SeekOrigin.Begin);
            ApiSaveStream(relative, stream, streamId);
        }

        public void SaveXml(string relative, System.Xml.Linq.XElement xml, string xmlId)
        {
            using (var ms = new MemoryStream())
            {
                xml.Save(ms);
                ms.Seek(0, SeekOrigin.Begin);
                ApiSaveStream(relative, ms, xmlId);
            }
        }

        public System.IO.Stream LoadStream(string relative, string itemId)
        {
            return ApiGetStream(relative, itemId);
        }

        public System.Xml.Linq.XElement LoadXml(string relative, string itemId)
        {
            using (var stream = ApiGetStream(relative, itemId))
            {
                if (stream != null)
                {
                    var xml = XElement.Load(stream);
                    return xml;
                }
                return null;
            }
        }

        public void DeleteItem(string relative, string itemId)
        {
//            throw new NotImplementedException();
        }

        public bool ItemExists(string relative, string itemId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AI.Case> GetCases(string search)
        {
            return ApiGetCases(search);
        }

        public IEnumerable<Slide> GetSlides(string caseName)
        {
            return ApiGetSlides(caseName);
        }

        public IEnumerable<string> GetNodes(string relativeId)
        {
            return ApiGetNodes(relativeId);
        }

        public Slide CreateSlide(string caseName, string slideId)
        {
            throw new NotImplementedException();
        }

        private HttpClient PrepareHttpClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(_baseUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        private IEnumerable<AI.Case> ApiGetCases(string search)
        {
            using (var client = PrepareHttpClient())
            {
                var api = string.Format("api/v1/cases?search={0}", search != null ? search : "");

                // we're blocking by using the result, need to consider how we don't block on the gui, use continuewith and an injected update delegate?
                // or just make these calls block and handle async in the application
                var response = client.GetAsync(api).Result;

                if (response.IsSuccessStatusCode)
                {
                    var Cases = response.Content.ReadAsAsync<IEnumerable<AI.Case>>().Result; // again, blocking
                    return Cases;
                }
                return null;
            }
        }

        private bool ApiCheckSource()
        {
            try
            {
                using (var client = PrepareHttpClient())
                {
                    var api = string.Format("api/v1/connected");

                    // we're blocking by using the result, need to consider how we don't block on the gui, use continuewith and an injected update delegate?
                    // or just make these calls block and handle async in the application
                    var response = client.GetAsync(api).Result;
                    if (response.IsSuccessStatusCode)
                        return response.Content.ReadAsAsync<bool>().Result;
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        private IEnumerable<AI.Slide> ApiGetSlides(string caseId)
        {
            using (var client = PrepareHttpClient())
            {
                var api = string.Format("api/v1/cases/{0}/slides", caseId);
                var response = client.GetAsync(api).Result;

                if (response.IsSuccessStatusCode)
                {
                    var slides = response.Content.ReadAsAsync<IEnumerable<AI.Slide>>().Result;
                    return slides;
                }
                return null;
            }
        }

        private IEnumerable<string> ApiGetNodes(string relative)
        {
            using (var client = PrepareHttpClient())
            {
                var api = string.Format("api/v1/Nodes/{0}", relative);
                var response = client.GetAsync(api).Result;

                if (response.IsSuccessStatusCode)
                {
                    var nodeNames = response.Content.ReadAsAsync<IEnumerable<string>>().Result;
                    return nodeNames;
                }
                return null;
            }
        }


        private MemoryStream ApiGetStream(string relativeId, string itemId)
        {
            using (var client = PrepareHttpClient())
            {
                var api = string.Format("api/v1/data/{0}/items/{1}/stream", relativeId, itemId);
                var response = client.GetAsync(api).Result;

                if (response.IsSuccessStatusCode)
                {
                    var memoryStream = new MemoryStream();
                    response.Content.CopyToAsync(memoryStream).Wait();
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    return memoryStream;
                }
                return null;
            }
        }

        //private void ApiSaveStream(string relativeId, Stream stream, string streamId)
        //{
        //    using (var client = PrepareHttpClient())
        //    using (var content = new MultipartFormDataContent())
        //    {
        //        stream.Seek(0, SeekOrigin.Begin);
        //        var streamContent = new StreamContent(stream);
        //        streamContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = streamId };
        //        content.Add(streamContent);
        //        // Make a call to Web API and wait
        //        var api = string.Format("api/v1/data/{0}", relativeId);
        //        var result = client.PostAsync(api, content).Result;
        //    }
        //}

        private void ApiSaveStream(string relativeId, Stream stream, string streamId)
        {
            var content = new MultipartFormDataContent();
            content.Add(new StreamContent(stream), "file", streamId);

            var api = string.Format("api/v1/data/{0}", relativeId);
            var client = PrepareHttpClient();
            var response = client.PostAsync(api, content).Result; // blocks as we're asking for the result
            if (!response.IsSuccessStatusCode)
            {
                System.Diagnostics.Trace.WriteLine("ApiSaveStream No Success");
            }
        }


    }
}
