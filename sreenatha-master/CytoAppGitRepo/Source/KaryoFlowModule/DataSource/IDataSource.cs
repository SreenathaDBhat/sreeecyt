﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Xml.Linq;

namespace AI
{
    public interface IDataSource
    {
        bool TestConnection();
        // Save
        void SaveThumbnail(string relative, BitmapSource thumbnail, string imageId);
        void SaveStream(string relative, Stream stream, string streamId);
        void SaveXml(string relative, XElement xml, string xmlId);

        // load
        Stream LoadStream(string relative, string streamId);
        XElement LoadXml(string relative, string xmlId);
        void DeleteItem(string relative, string itemId);

        // Manage
        bool ItemExists(string relative, string itemId);

        // Common case management
        IEnumerable<Case> GetCases(string search);
        IEnumerable<Slide> GetSlides(string caseName);
        IEnumerable<string> GetNodes(string relativeId);

        Slide CreateSlide(string caseName, string slideId);
    }
}
