﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AI.Connect;
using System.ComponentModel;
using System.Xml.Linq;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.IO;
using System.Reflection;
using AI.Security;
using System.Threading;
using System.Windows.Threading;

namespace AI
{
    public class Archiver : IDataTransferTask, IDisposable
    {
        private DatatransferController controller = null;

        private string altFileName = string.Empty;
        private string archiveFolder = string.Empty;
        private string password = string.Empty;
        private string caseName = string.Empty;
        private Guid caseID = Guid.Empty;
        private string tempFolder = string.Empty;
        private string archiveFile = string.Empty;
        private int totalRecs = 0;
        private int currentRec = 0;

        
        // flags and options for archive
        private Boolean deleteAfter = false;                    //  delete the case after archive
        private Boolean pruneData = true;                       //  prune image data
        private Boolean incProbeAnalysisData = true;            //  include Probe analysis data
        private Boolean incCellAnalysisData = true;             //  include Cell analysis data
        private Boolean incAnnotationData = true;               //  include annotation data
        private Boolean incScratchPadData = true;               //  include scratch pad data
        private Boolean incDetailsData = true;                  //  include confidential details data
        private Boolean incConfidentialDetailsData = true;      //  include confidential details data
        private Boolean outputForLIMS = false;                  //  dont archive irrelevenat data
        private int archivedStatus = -1;           //  new status of case on successful archive
        private string archivedLabel = string.Empty;            //  label of archive media
        private Boolean exporting = false;                  //  dont archive irrelevenat data
        private Boolean encryptOutput = false;                  //  dont archive irrelevenat data

        private string errMessage = string.Empty;
        
        public Archiver(DatatransferController controller, string arcFolder)
        {
            if (controller != null)
            {
                this.controller = controller;
            }
            
            archiveFolder = arcFolder;

            if (arcFolder.Substring(arcFolder.Length - 1, 1) != @"\")
                archiveFolder += @"\";
        }

        public string LastErrorMessage
        {
            get { return errMessage; }
        }

        public void SetParameters(string caseName, Guid caseId, Boolean pruneData, Boolean incProbeAnalysisData, Boolean incCellAnalysisData,
                        Boolean incAnnotationData, Boolean incScratchPadData, Boolean incDetailsData, Boolean incConfidentialDetailsData,
                        Boolean deleteAfter, Boolean encryptOutput, string password)
        {
            errMessage = string.Empty;
            this.pruneData = pruneData;
            this.incProbeAnalysisData = incProbeAnalysisData;
            this.incCellAnalysisData = incCellAnalysisData;
            this.incAnnotationData = incAnnotationData;
            this.incScratchPadData = incScratchPadData;
            this.incDetailsData = incDetailsData;
            this.incConfidentialDetailsData = incConfidentialDetailsData;
            this.deleteAfter = deleteAfter;
            this.exporting = false;
            this.caseID = caseId;
            this.encryptOutput = encryptOutput;
            this.password = password;
        }

        public void SetExportParameters(string arcFileName, Guid caseId, Boolean pruneData, Boolean incProbeAnalysisData, Boolean incCellAnalysisData,
                        Boolean incAnnotationData, Boolean incScratchPadData,  Boolean incDetailsData, Boolean incConfidentialDetailsData,
                        Boolean encryptOutput, string password)
        {
            this.altFileName = arcFileName;
            this.pruneData = pruneData;
            this.incProbeAnalysisData = incProbeAnalysisData;
            this.incCellAnalysisData = incCellAnalysisData;
            this.incAnnotationData = incAnnotationData;
            this.incScratchPadData = incScratchPadData;
            this.incDetailsData = incDetailsData;
            this.incConfidentialDetailsData = incConfidentialDetailsData;
            this.deleteAfter = false;
            this.archivedStatus = -1;
            this.archivedLabel = "";
            this.exporting = true;
            this.caseID = caseId;
            this.encryptOutput = encryptOutput;
            this.password = password;
        }

        public void SetParameters(string arcFileName, Guid caseId, Boolean prune, Boolean deleteAfter, string archivedStatus, string label)
        {
            this.altFileName = arcFileName;
            this.exporting = false;
            
            SetParameters(caseId, prune, true, true, true, true, true, true, deleteAfter, false, "", archivedStatus, label);
        }

        public void SetParameters(Guid caseId, Boolean prune, Boolean deleteAfter, string archivedStatus, string label)
        {
            this.exporting = false;
            SetParameters(caseId, prune, true, true, true, true, true, true, deleteAfter, false, "", archivedStatus, label);
        }


        public void SetParameters(Guid caseId, Boolean pruneData, Boolean incProbeAnalysisData, Boolean incCellAnalysisData,
                        Boolean incAnnotationData, Boolean incScratchPadData, Boolean incDetailsData, Boolean incConfidentialDetailsData,
                        Boolean deleteAfter, Boolean encryptOutput, string password, string archivedStatus, string mediaLabel)
        {
            errMessage = string.Empty;
            this.pruneData = pruneData;
            this.incProbeAnalysisData = incProbeAnalysisData;
            this.incCellAnalysisData = incCellAnalysisData;
            this.incAnnotationData = incAnnotationData;
            this.incScratchPadData = incScratchPadData;
            this.incDetailsData = incDetailsData;
            this.incConfidentialDetailsData = incConfidentialDetailsData;
            this.deleteAfter = deleteAfter;
            this.archivedStatus = GetStatusCode(archivedStatus);
            this.archivedLabel = mediaLabel;
            this.exporting = false;
            this.caseID = caseId;
            this.encryptOutput = encryptOutput;
            this.password = password;
        }

        private int GetStatusCode(string statusText)
        {
            int retCode = -1;

            using (Database db = new Database())
            {
                var statusrec = db.Status.Where(s => s.StatusCode.ToUpper() == statusText.ToUpper()).FirstOrDefault();

                if (statusrec != null)
                    retCode = statusrec.ID;
                else
                    retCode = -1;
            }

            return retCode;
        }

        private void TotalRecsToProcess(Database db, Guid caseId, ILims lims, Case limsCase)
        {
            var caseSlides = db.Slides.Where(c => c.caseId == caseId);
            if (caseSlides != null && caseSlides.Count() > 0)
            {
                foreach (var slide in caseSlides)
                {
                    var defCaps = db.DeferredCaptures.Where(c => c.slideId == slide.slideId);
                    if (defCaps != null) totalRecs += defCaps.Count();

                    var slideFrames = db.ImageFrames.Where(c => c.slideId == slide.slideId);
                    if (slideFrames != null && slideFrames.Count() > 0)
                    {
                        foreach (var frame in slideFrames)
                        {
                            // Check if there are any FuseCutouts for this frame
							var fuseCutouts = db.FuseCutouts.Where(c => c.cutoutImageFrameId == frame.imageFrameId);
							if (fuseCutouts != null) totalRecs += fuseCutouts.Count();

                            if (!outputForLIMS)  //  LIMS does not need any records below this level
                            {
                                var imageComponents = db.ImageComponents.Where(c => c.frameId == frame.imageFrameId);
                                if (imageComponents != null && imageComponents.Count() > 0)
                                {
                                    foreach (var component in imageComponents)
                                    {
                                        var imageSnaps = db.ImageSnaps.Where(c => c.componentId == component.imageComponentId);
                                        if (imageSnaps != null) totalRecs += imageSnaps.Count();
                                    }
                                }
                            }
                        }
                    }

                    if (!pruneData)
                    {
                        var slideTiles = db.SlideImageTiles.Where(c => c.slideId == slide.slideId);
                        if (slideTiles != null) totalRecs += slideTiles.Count();
                    }

                    if (incProbeAnalysisData)
                    {
                        var pss = db.ProbeScoreSessions.Where(c => c.slideId == slide.slideId);
                        if (pss != null && pss.Count() > 0)
                        {
                            foreach (var ps in pss)
                            {
                                var p = db.ProbeScores.Where(c => c.sessionId == ps.sessionId);
                                if (p != null) totalRecs += p.Count();
                            }
                        }
                    }

                    if (incCellAnalysisData)
                    {
                        var slideCells = db.Cells.Where(c => c.slideId == slide.slideId);

                        if (slideCells != null && slideCells.Count() > 0)
                        {
                            foreach (var cell in slideCells)
                            {
                                var cellTags = db.NumberingTags.Where(c => c.cellId == cell.cellId);
                                if (cellTags != null) totalRecs += cellTags.Count();

                                var mcps = db.ManualCountPoints.Where(c => c.cellId == cell.cellId);
                                if (mcps != null) totalRecs += mcps.Count();

                                var clearedzomes = db.ClearedChromosomes.Where(c => c.cellId == cell.cellId);
                                if (clearedzomes != null) totalRecs += clearedzomes.Count();

                                var typedzomes = db.KaryotypeChromosomes.Where(c => c.cellId == cell.cellId);
                                if (typedzomes != null) totalRecs += typedzomes.Count();

                            }
                        }
                    }
                }
            }

            if (incDetailsData)
            {
                var caseDetails = lims.GetCaseMetaData(limsCase);
                if (caseDetails != null) 
                    totalRecs += caseDetails.Count();
            }

            if (incScratchPadData)
            {
                var scratchPads = db.ScratchPads.Where(c => c.caseId == caseId);
                if (scratchPads != null && scratchPads.Count() > 0)
                {
                    foreach (var sp in scratchPads)
                    {
                        var items = db.ScratchPadItems.Where(c => c.scratchPadId ==  sp.scratchPadId);
                        if (items != null) totalRecs += items.Count();
                    }
                }
            }

            if (incAnnotationData)
            {
                var caseAnnotations = db.Annotations.Where(c => c.caseId == caseId);
                if (caseAnnotations != null) totalRecs += caseAnnotations.Count();
            }
        }
        
        
        public void Archive(Guid caseID, Boolean encryptData, string password)
        {
            errMessage = string.Empty;
            int origStatus = -1;

            // Need to lock the case while exporting
            CaseLockInfo info = CaseLockInfo.ForCase(caseID);
            if (info != null)
            {
                if (controller != null)
                {
                    errMessage = "Case locked by " + info.LockedBy;
                    controller.Error = errMessage;
                    controller.ProcessComplete = true;
                }

                return;
            }

            try
            {
                CaseLocking.AcquireCaseLock(caseID);

                using (ILims lims = Connect.LIMS.New())
                using (Database db = new Database())
                {

                    //  Validation of case
                    AI.Connect.Case caseRec = db.Cases.Where(f => f.caseId == caseID).FirstOrDefault();
                    var limsCase = lims.GetCase(caseRec.limsRef);

                    if (caseRec == null)
                    {
                        errMessage = "CaseID " + caseID.ToString() + " does not exist.";
                        controller.Error = errMessage;
                    }
                    else
                    {
                        origStatus = caseRec.status;
                        caseID = caseRec.caseId;
                        caseName = limsCase.Name;

                        //  Need to set a temporary status 
                        //  so that another user
                        //  can not modify the case while
                        //  it is being processed

                        caseRec.status = -1;
                        db.SubmitChanges();

                        if (controller != null)
                        {
                            controller.Process = "Preparing archive of " + caseName;
                            //  Determine total number of records we will process
                            TotalRecsToProcess(db, caseID, lims, limsCase);
                            currentRec = 0;
                            controller.NewProcess("Extracting data", totalRecs);
                        }

                        XElement xml = GetCase(db, lims, caseID, caseName);

                        if (xml != null)
                        {
                            //  Create the archive in a temporary folder then
                            //  copy it to the correct destination folder.

                            DirectoryInfo di = new DirectoryInfo(tempFolder);

                            if (!di.Exists)  // temporary folder in which all files are collected for compression
                            {
                                di.Create();

                                //  Now to work through the options
                                //  and attach the appropraite requested
                                //  data.
                                XElement slidesXml = GetCaseSlides(db, lims, caseID);
                                if (slidesXml != null)
                                    xml.Add(slidesXml);


                                if (incDetailsData)
                                {
                                    XElement detailsXml = GetLimsCaseMetaData(lims, limsCase);

                                    if (detailsXml != null)
                                    {
                                        var encoded = Zipper.EncryptString(detailsXml.ToString(), password);
                                        xml.Add(new XElement("EncryptedCaseMetaData", encoded));
                                    }
                                }


                                if (incScratchPadData)
                                {
                                    XElement spdXml = GetScratchPads(db, caseID);
                                    if (spdXml != null)
                                        xml.Add(spdXml);
                                }

                                if (incAnnotationData)
                                {
                                    XElement annoXml = GetCaseAnnotations(db, caseID);

                                    if (annoXml != null)
                                        xml.Add(annoXml);
                                }

                                string dataFile = tempFolder + @"\" + caseName + TransferType.DATA_EXTENSION;

                                xml.Save(dataFile);

                                Zipper zip = new Zipper();

                                var files = di.GetFiles();
                                Boolean encryptOutput = false;

                                if (controller != null)
                                    controller.NewProcess("Zipping files", files.Count());

                                int counter = 0;

                                string tempFile = tempFolder + @"\" + Guid.NewGuid().ToString() + ".zip";

                                foreach (var f in files)
                                {

                                    // Only the .DATA file needs encrypting
                                    if (encryptData)
                                        encryptOutput = (f.FullName == dataFile);

                                    zip.AddFileToZip(tempFile, f.FullName, encryptOutput, password);

                                    f.Delete();

                                    if (controller != null)
                                        controller.CurrentStep = ++counter;
                                }

                                if (altFileName.Length == 0)
                                    archiveFile = archiveFolder + caseName + TransferType.ARCHIVE_EXTENSION;
                                else
                                    archiveFile = archiveFolder + altFileName;

                                if (File.Exists(archiveFile))
                                    File.Delete(archiveFile);

                                FileCopy(tempFile, archiveFile);

                                File.Delete(tempFile);

                                di.Delete();

                                //  Do we delete the case from the database
                                //  after archiving
                                if (deleteAfter)
                                {
                                    //  Only delete the Tubs data and not the
                                    //  Lims data.
                                    db.Cases.DeleteOnSubmit(caseRec);
                                    db.SubmitChanges();
                                }
                                else
                                {
                                    if (exporting)
                                        caseRec.status = origStatus;
                                    else
                                    {
                                        caseRec.status = archivedStatus;
                                        caseRec.archiveLabel = archivedLabel;
                                    }

                                    db.SubmitChanges();

                                }
                            }
                            else
                            {
                                errMessage = "Archive case folder " + tempFolder + " already exists.";

                                if (controller != null)
                                    controller.Error = errMessage;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;

                //  Need to reset the status
                using (Database db = new Database())
                {
                    AI.Connect.Case caseRec = db.Cases.Where(f => f.caseId == caseID).FirstOrDefault();

                    if (caseRec != null)
                    {
                        caseRec.status = origStatus;
                        db.SubmitChanges();
                    }
                }

                if (controller != null)
                    controller.Error = errMessage;
            }
            finally
            {
                CaseLocking.ReleaseCaseLock(caseID);
            }

            if (controller != null)
            {
                controller.ProcessComplete = true;
            }

        }

        private XElement GetCaseAnnotations(Database db, Guid caseID)
        {
            XElement annosElement = null;

            var caseAnnotations = db.Annotations.Where(c => c.caseId == caseID);

            if (caseAnnotations != null && caseAnnotations.Count() > 0)
            {

                annosElement = new XElement("Annotations");
                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.Annotation).GetProperties(b);

                foreach (var an in caseAnnotations)
                {
                    XElement anElement = new XElement("Annotation");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(an, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            anElement.Add(new XElement(p.Name, x));
                        }
                    }

                    annosElement.Add(anElement);
                }
            }

            return annosElement;
        }

        private XElement GetLimsCaseMetaData(ILims lims, Case limsCase)
        {
            XElement detailsElement = null;

            var caseDetails = lims.GetCaseMetaData(limsCase);

            if (caseDetails != null && caseDetails.Count() > 0)
            {
                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(MetaDataItem).GetProperties(b);

                foreach (var detail in caseDetails)
                {
                    Boolean incThisDetail = true;

                    if (!incConfidentialDetailsData)
                    {
                        // TODO: CONFIDENTIAL!!!
                        // incThisDetail = !detail.confidential;
                    }

                    if (incThisDetail)
                    {
                        if(detailsElement == null)
                            detailsElement = new XElement("CaseMetaData");
                        
                        XElement detailElement = new XElement("MetaData");

                        foreach (var p in myProps)
                        {
                            object x = p.GetGetMethod().Invoke(detail, null);
                            detailElement.Add(new XElement(p.Name, x));
                        }
                        
                        detailsElement.Add(detailElement);
                    }
                }
            }

            return detailsElement;
        }

        private XElement GetLimsSlideMetaData(ILims lims, AI.Slide limsSlide)
        {
            XElement detailsElement = null;

            var slideDetails = lims.GetSlideMetaData(limsSlide);

            if (slideDetails != null && slideDetails.Count() > 0)
            {
                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(MetaDataItem).GetProperties(b);

                foreach (var detail in slideDetails)
                {
                    if (detailsElement == null)
                        detailsElement = new XElement("SlideMetaData");

                    XElement detailElement = new XElement("MetaData");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(detail, null);
                        detailElement.Add(new XElement(p.Name, x));
                    }

                    detailsElement.Add(detailElement);
                }
            }

            return detailsElement;
        }



        #region Archive Case Image Data

        private XElement GetCase(Database db, ILims lims, Guid caseID, string caseName)
        {
            XElement caseElement = null;

            try
            {
                var caseRec = db.Cases.Where(rec => rec.caseId == caseID).FirstOrDefault();

                tempFolder = Path.GetTempPath() + Guid.NewGuid().ToString();

                caseElement = new XElement("Case");

                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.Case).GetProperties(b);

                foreach (var p in myProps)
                {
                    object x = p.GetGetMethod().Invoke(caseRec, null);
                    object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                    if (info.Length == 1)
                    {
                        caseElement.Add(new XElement(p.Name, x));
                    }
                }

                //  Lims data also
                var limsCase = lims.GetCase(caseRec.limsRef);
                var limsProps = typeof(AI.Case).GetProperties(b);

                foreach (var p in limsProps)
                {
                    object x = p.GetGetMethod().Invoke(limsCase, null);
                    caseElement.Add(new XElement(p.Name, x));
                }
            }
            catch (Exception ex)
            {
                caseElement = null;
                errMessage = ex.Message;
            }
            
            return caseElement;
        }

        private XElement GetCaseSlides(Database db, ILims lims, Guid caseID)
        {
            XElement slidesElement = null;

            var caseSlides = db.Slides.Where(c => c.caseId == caseID);

            if (caseSlides != null && caseSlides.Count() > 0)
            {
                slidesElement = new XElement("Slides");

                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.Slide).GetProperties(b);

                foreach( var slide in caseSlides)
                {

                    XElement slideElement = new XElement("Slide");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(slide, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            slideElement.Add(new XElement(p.Name, x));
                        }
                    }

                    //  Lims data also
                    var limsSlide = lims.GetSlide(slide.limsRef);
                    var limsProps = typeof(AI.Slide).GetProperties(b);

                    foreach (var p in limsProps)
                    {
                        object x = p.GetGetMethod().Invoke(limsSlide, null);
                        slideElement.Add(new XElement(p.Name, x));
                    }

                    XElement slideMetaData = GetLimsSlideMetaData(lims, limsSlide);
                    if (slideMetaData != null)
                        slideElement.Add(slideMetaData);
                    
                    XElement defCapData = GetDeferredCapture(db, slide.slideId);
                    if (defCapData != null)
                        slideElement.Add(defCapData);

                    XElement frameData = GetImageFrames(db, slide.slideId);
                    if(frameData != null)
                       slideElement.Add(frameData);

                    if (!pruneData)
                    {
                        XElement tileData = GetSlideTiles(db, slide.slideId);
                        if (tileData != null)
                            slideElement.Add(tileData);
                    }    
                    
                    if (incProbeAnalysisData)
                    {
                        XElement slideProbes = GetProbeScoreSessions(db, slide.slideId);

                        if (slideProbes != null)
                            slideElement.Add(slideProbes);
                    }
                    
                    if (incCellAnalysisData)
                    {
                        XElement slideCells = GetSlideCells(db, slide.slideId);

                        if(slideCells != null)
                            slideElement.Add(slideCells);
                    }

                    slidesElement.Add(slideElement);
                }
            }

            return slidesElement;
        }

        private XElement GetImageFrames(Database db, Guid slideID)
        {
            XElement framesElement = null;

            var slideFrames = db.ImageFrames.Where(c => c.slideId == slideID);

            if (slideFrames != null && slideFrames.Count() > 0)
            {

                framesElement = new XElement("Frames");

                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.ImageFrame).GetProperties(b);

                foreach (var frame in slideFrames)
                {
                    XElement frameElement = new XElement("Frame");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(frame, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            frameElement.Add(new XElement(p.Name, x));
                        }
                    }   

                    // Check if there are any FuseCutouts for this frame
					var fuseCutouts = db.FuseCutouts.Where(c => c.cutoutImageFrameId == frame.imageFrameId);

					if (fuseCutouts != null && fuseCutouts.Count() > 0)
					{
						XElement fm = GetFuseCutouts(db, frame.imageFrameId);

						if (fm != null)
							frameElement.Add(fm);
					}

                    if (frame.blobid != null && frame.blobid != Guid.Empty)
                    {
                        frameElement.Add(GetBlob(db, (Guid)frame.blobid));
                    }

                    if(!outputForLIMS)  //  Dont need any images below this level
                        frameElement.Add(GetImageComponents(db, frame.imageFrameId));

                    framesElement.Add(frameElement);

                }
            }

            return framesElement;
        }

		private XElement GetFuseCutouts(Database db, Guid imageFrameID)
		{
			XElement fusesElement = null;

			var fuseCutouts = db.FuseCutouts.Where(c => c.cutoutImageFrameId == imageFrameID);

			if (fuseCutouts != null && fuseCutouts.Count() > 0)
			{
				fusesElement = new XElement("FuseCutouts");
				fusesElement.Add(new XAttribute("cutoutImageFrameId", imageFrameID.ToString()));

				BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
				var myProps = typeof(Connect.FuseCutout).GetProperties(b);

				foreach (var fuse in fuseCutouts)
				{

					XElement fuseElement = new XElement("FuseCutout");

					foreach (var p in myProps)
					{
						object x = p.GetGetMethod().Invoke(fuse, null);
						object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
						if (info.Length == 1)
						{
							fuseElement.Add(new XElement(p.Name, x));
						}
					}

					fusesElement.Add(fuseElement);

					if (controller != null)
					{
						controller.CurrentStep = ++currentRec;
					}
				}
			}

			return fusesElement;
		}

        private XElement GetImageComponents(Database db, Guid frameID)
        {
            XElement imageComponentsElement = null;

            var imageComponents = db.ImageComponents.Where(c => c.frameId == frameID);

            if (imageComponents != null && imageComponents.Count() > 0)
            {

                imageComponentsElement = new XElement("ImageComponents");

                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.ImageComponent).GetProperties(b);

                foreach (var component in imageComponents)
                {

                    XElement componentElement = new XElement("ImageComponent");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(component, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            componentElement.Add(new XElement(p.Name, x));
                        }
                    }

                    componentElement.Add(GetImagesnaps(db, component.imageComponentId));
 
                    imageComponentsElement.Add(componentElement);
                }
            }

            return imageComponentsElement;
        }

        private XElement GetImagesnaps(Database db, Guid componentID)
        {
            XElement imageSnapsElement = null;

            var imageSnaps = db.ImageSnaps.Where(c => c.componentId == componentID);

            if (imageSnaps != null && imageSnaps.Count() > 0)
            {

                imageSnapsElement = new XElement("ImageSnaps");

                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.ImageSnap).GetProperties(b);

                foreach (var snap in imageSnaps)
                {
                    // dont need all the Z stack images if pruning, only the -1 zlevel

                    if ((snap.zLevel == -1)  || (!pruneData))
                    {
                        XElement snapElement = new XElement("ImageSnap");

                        foreach (var p in myProps)
                        {
                            object x = p.GetGetMethod().Invoke(snap, null);
                            object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                            if (info.Length == 1)
                            {
                                snapElement.Add(new XElement(p.Name, x));
                            }
                        }

                        snapElement.Add(GetBlob(db, snap.blobId));

                        imageSnapsElement.Add(snapElement);
                    }

                    if (controller != null)
                    {
                        controller.CurrentStep = ++currentRec;
                    }
                }
            }

            return imageSnapsElement;
        }

        private XElement GetBlob(Database db, Guid blobID)
        {
            XElement blobElement = null;

            if (blobID != null && blobID != Guid.Empty)
            {
                var theBlob = db.Blobs.Where(c => c.blobId == blobID).First();

                if (theBlob != null)
                {
                    string filename = tempFolder + @"\" + theBlob.path;

                    try
                    {

                        blobElement = new XElement("Blob",
                           new XElement("blobId", theBlob.blobId.ToString()),
                           new XElement("caseId", theBlob.caseId.ToString()),
                           new XElement("path", theBlob.path)
                       );

                        // Copy the binary data into the file
                        if (!File.Exists(filename))
                        {
                            FileStream fs = File.Create(filename);
                            BinaryWriter bw = new BinaryWriter(fs);
                            bw.Write(theBlob.binaryData.ToArray(), 0, theBlob.binaryData.Length);
                            bw.Close();
                            fs.Close();
                        }
                    }
                    catch (Exception)
                    {
                        blobElement = null;
                    }
                }
            }
            else
            {
                blobElement = new XElement("Blob",
                   new XElement("Invalid", blobID.ToString())
                   );
            }

            return blobElement;
        }

        #endregion

        #region Archive Cell Data

        private XElement GetSlideCells(Database db, Guid slideID)
        {
            XElement cellsElement = null;

            var slideCells = db.Cells.Where(c => c.slideId == slideID);

            if (slideCells != null && slideCells.Count() > 0)
            {
                cellsElement = new XElement("Cells");
                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.Cell).GetProperties(b);

                foreach (var cell in slideCells)
                {
                    XElement cellElement = new XElement("Cell");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(cell, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            cellElement.Add(new XElement(p.Name, x));
                        }
                    }

                    if (cell.karyotypedBlobId != null && cell.karyotypedBlobId != Guid.Empty)
                    {
                        cellElement.Add(GetBlob(db, (Guid)cell.karyotypedBlobId));
                    }

                    cellElement.Add(GetCellNumberingTags(db, cell.cellId),
                        GetCellManualCountPoints(db, cell.cellId),
                        GetCellClearedChromosomes(db, cell.cellId),
                        GetCellMetaData(db, cell.cellId),
                        GetCellKaryotypeChromosomes(db, cell.cellId));

                    cellsElement.Add(cellElement);

                }
            }

            return cellsElement;
        }

        private XElement GetDeferredCapture(Database db, Guid slideID)
        {
            XElement defCapsElement = null;

            var defCaps = db.DeferredCaptures.Where(c => c.slideId == slideID);

            if (defCaps != null && defCaps.Count() > 0)
            {

                defCapsElement = new XElement("DeferredCaptures");
                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.DeferredCapture).GetProperties(b);

                foreach (var defCap in defCaps)
                {
                    XElement capElement = new XElement("DeferredCapture");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(defCap, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            if (p.Name.ToUpper() == "OPTIONS")
                            {
                                XElement assay = new XElement(p.Name);
                                assay.Value = CDATA.CDATA_START + x.ToString() + CDATA.CDATA_END;
                                capElement.Add(assay);
                            }
                            else
                            {
                                capElement.Add(new XElement(p.Name, x));
                            }
                        }
                    }

                    defCapsElement.Add(capElement);

                    if (controller != null)
                    {
                        controller.CurrentStep = ++currentRec;
                    }
                }
            }

            return defCapsElement;
        }


        private XElement GetSlideTiles(Database db, Guid slideID)
        {
            XElement tilesElement = null;

            var slideTiles = db.SlideImageTiles.Where(c => c.slideId == slideID);

            if (slideTiles != null && slideTiles.Count() > 0)
            {
                tilesElement = new XElement("Tiles");
                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.SlideImageTile).GetProperties(b);

                foreach (var tile in slideTiles)
                {
                    XElement tileElement = new XElement("Tile");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(tile, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            tileElement.Add(new XElement(p.Name, x));
                        }
                    }
                    
                    tileElement.Add(GetBlob(db, tile.blobId));
                    
                    tilesElement.Add(tileElement);

                    if (controller != null)
                    {
                        controller.CurrentStep = ++currentRec;
                    }
                }
            }

            return tilesElement;
        }


        private XElement GetCellNumberingTags(Database db, Guid cellID)
        {
            XElement tagsElement = null;

            var cellTags = db.NumberingTags.Where(c => c.cellId == cellID);

            if (cellTags != null && cellTags.Count() > 0)
            {

                tagsElement = new XElement("NumberingTags");

                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.NumberingTag).GetProperties(b);

                foreach (var tag in cellTags)
                {

                    XElement tagElement = new XElement("NumberingTag");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(tag, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            tagElement.Add(new XElement(p.Name, x));
                        }
                    }
                    
                    tagsElement.Add(tagElement);

                    if (controller != null)
                    {
                        controller.CurrentStep = ++currentRec;
                    }

                }
            }

            return tagsElement;
        }

        private XElement GetCellMetaData(Database db, Guid cellID)
        {
            XElement mdsElement = null;

            var cellMetaData = db.CellMetaDatas.Where(c => c.cellId == cellID);

            if (cellMetaData != null && cellMetaData.Count() > 0)
            {

                mdsElement = new XElement("CellMetaDatas");

                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.CellMetaData).GetProperties(b);

                foreach (var md in cellMetaData)
                {

                    XElement mdElement = new XElement("CellMetaData");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(md, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            mdElement.Add(new XElement(p.Name, x));
                        }
                    }

                    mdsElement.Add(mdElement);
                }
            }

            return mdsElement;
        }

        private XElement GetCellManualCountPoints(Database db, Guid cellID)
        {
            XElement mcpsElement = null;
            
            var mcps = db.ManualCountPoints.Where(c => c.cellId == cellID);

            if (mcps != null && mcps.Count() > 0)
            {

                mcpsElement = new XElement("ManualCountPoints");

                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.ManualCountPoint).GetProperties(b);

                foreach (var mcp in mcps)
                {

                    XElement mcpElement = new XElement("ManualCountPoint");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(mcp, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            mcpElement.Add(new XElement(p.Name, x));
                        }
                    }

                    mcpsElement.Add(mcpElement);

                    if (controller != null)
                    {
                        controller.CurrentStep = ++currentRec;
                    }
                }
            }

            return mcpsElement;
        }

        private XElement GetCellClearedChromosomes(Database db, Guid cellID)
        {
            XElement zomesElement = null;
            
            var clearedzomes = db.ClearedChromosomes.Where(c => c.cellId == cellID);

            if (clearedzomes != null && clearedzomes.Count() > 0)
            {
                zomesElement = new XElement("ClearedChromomsomes");

                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.ClearedChromosome).GetProperties(b);

                foreach (var zome in clearedzomes)
                {
                    XElement zomeElement = new XElement("ClearedChromosome");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(zome, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            zomeElement.Add(new XElement(p.Name, x));
                        }
                    }
                    
                    zomesElement.Add(zomeElement);

                    if (controller != null)
                    {
                        controller.CurrentStep = ++currentRec;
                    }

                }
            }

            return zomesElement;
        }

        private XElement GetCellKaryotypeChromosomes(Database db, Guid cellID)
        {
            XElement zomesElement = null;

            var typedzomes = db.KaryotypeChromosomes.Where(c => c.cellId == cellID);

            if (typedzomes != null && typedzomes.Count() > 0)
            {
                zomesElement = new XElement("KaryotypeChromosomes");

                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.KaryotypeChromosome).GetProperties(b);

                foreach (var zome in typedzomes)
                {
                    XElement zomeElement = new XElement("KaryotypeChromosome");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(zome, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            zomeElement.Add(new XElement(p.Name, x));
                        }
                    }
                    
                    if (zome.blobId != null && zome.blobId != Guid.Empty)
                    {
                        zomeElement.Add(GetBlob(db, (Guid)zome.blobId));
                    }
                    
                    zomesElement.Add(zomeElement);

                    if (controller != null)
                    {
                        controller.CurrentStep = ++currentRec;
                    }

                }
            }

            return zomesElement;
        }

        #endregion

        #region ScratchPad Data

        private XElement GetScratchPads(Database db, Guid caseID)
        {
            XElement spElement = null;

            var scratchPads = db.ScratchPads.Where(c => c.caseId == caseID);

            if (scratchPads != null && scratchPads.Count() > 0)
            {

                spElement = new XElement("ScratchPads");

                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.ScratchPad).GetProperties(b);

                foreach (var sp in scratchPads)
                {
                    XElement element = new XElement("ScratchPad");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(sp, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            element.Add(new XElement(p.Name, x));
                        }
                    }

                    element.Add(GetScratchPadItems(db, sp.scratchPadId));
                    
                    spElement.Add(element);
                }
            }

            return spElement;
        }

        private XElement GetScratchPadItems(Database db, Guid spID)
        {
            XElement itemsElement = null;

            var items = db.ScratchPadItems.Where(c => c.scratchPadId == spID);

            if (items != null && items.Count() > 0)
            {
                itemsElement = new XElement("ScratchPadItems");

                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.ScratchPadItem).GetProperties(b);

                foreach (var spi in items)
                {

                    XElement element = new XElement("ScratchPadItem");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(spi, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            if (p.Name.ToUpper() == "ITEMDESCRIPTION")
                            {
                                XElement assay = new XElement(p.Name);
                                assay.Value = CDATA.CDATA_START + x.ToString() + CDATA.CDATA_END;
                                element.Add(assay);
                            }
                            else
                            {
                                element.Add(new XElement(p.Name, x));
                            }
                        }
                    }

                    itemsElement.Add(element);
                }
            }

            return itemsElement;
        }

        #endregion

        #region ProbeScore data

        private XElement GetProbeScoreSessions(Database db, Guid slideID)
        {
            XElement pssElement = null;

            var pss = db.ProbeScoreSessions.Where(c => c.slideId == slideID);

            if (pss != null && pss.Count() > 0)
            {

                pssElement = new XElement("ProbeScoreSessions");

                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.ProbeScoreSession).GetProperties(b);

                foreach (var ps in pss)
                {
                    XElement element = new XElement("ProbeScoreSession");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(ps, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            if(p.Name.ToUpper() == "ASSAY")
                            {
                                XElement assay = new XElement(p.Name);
                                assay.Value =  CDATA.CDATA_START + x.ToString() + CDATA.CDATA_END;
                                element.Add(assay);
                            }
                            else
                            {
                                element.Add(new XElement(p.Name, x));
                            }
                        }
                    }

                    element.Add(GetProbeScore(db, ps.sessionId));

                    pssElement.Add(element);
                }
            }

            return pssElement;
        }

        private XElement GetProbeScore(Database db, Guid sessionID)
        {
            XElement psElement = null;

            var ps = db.ProbeScores.Where(c => c.sessionId == sessionID);

            if (ps != null && ps.Count() > 0)
            {

                psElement = new XElement("ProbeScores");

                BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
                var myProps = typeof(Connect.ProbeScore).GetProperties(b);

                foreach (var score in ps)
                {
                    XElement element = new XElement("ProbeScore");

                    foreach (var p in myProps)
                    {
                        object x = p.GetGetMethod().Invoke(score, null);
                        object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                        if (info.Length == 1)
                        {
                            element.Add(new XElement(p.Name, x));
                        }
                    }

                    element.Add(GetBlob(db, score.blobId));

                    psElement.Add(element);

                    if (controller != null)
                    {
                        controller.CurrentStep = ++currentRec;
                    }

                }
            }

            return psElement;
        }

        #endregion

        private void FileCopy(string sourcefile, string destfile)
        {
               
            using (FileStream source = new FileStream(sourcefile, FileMode.Open))
            {
                int bufferSize = (source.Length <= 1000000)? (int)source.Length : 1000000;
                int numPackets = (int)source.Length / bufferSize;
                int currPacket = 0;

                if (controller != null)
                {
                    controller.NewProcess("Moving archive", numPackets);
                }
                
                byte[] buffer = new byte[bufferSize];
                int bytesRead = 0;

                Boolean eof = false;
                using (FileStream dest = new FileStream(destfile, FileMode.Create))
                {
                    BinaryWriter bw = new BinaryWriter(dest);

                    while (!eof)
                    {
                        bytesRead = source.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0) eof = true;

                        if (!eof)
                            bw.Write(buffer, 0, bytesRead);

                        if (controller != null)
                        {
                            controller.CurrentStep = ++currPacket;
                        }
                    }

                    bw.Close();
                }
                source.Close();
            }
        }


        #region IDisposable Members

        public void Dispose()
        {

        }
    
        #endregion
    
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        #endregion // INotifyPropertyChanged Members

        #region IDataTransferTask Members

        void IDataTransferTask.Transfer()
        {
            Archive(this.caseID, this.encryptOutput, this.password);
        }

        #endregion
    }
}
