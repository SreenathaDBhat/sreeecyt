﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using AI.Security;

namespace AI
{
    internal static class TransferType
    {
        public const string DATA_EXTENSION = @".Data";
        public const string ARCHIVE_EXTENSION = @".Archive";
    }

    internal static class CDATA
    {
        internal const string CDATA_START = "<![CDATA[";
        internal const string CDATA_END = "]]";
    }

    public class LocalArchiveSettings
    {
        public LocalArchiveSettings()
        {
        }
        public Boolean DeleteCase { get; set; }
        public Boolean PruneData { get; set; }
        public int TimerInterval { get; set; }
        public string PreArchiveStatus { get; set; }
        public string PostArchiveStatus { get; set; }
        public string ArchiveDevice { get; set; }
    }
    
    public static class DataTransfer
    {
        private const string ArchiveSettingsFile = "Genetix\\archive.settings";
        private const string ExportSettingsFile = "Genetix\\export.settings";


        public static string GetLastExportLocation()
        {
            string location = @"c:\Archives";

            FileInfo file = AppData.File(Environment.SpecialFolder.LocalApplicationData, ExportSettingsFile);

            if (file.Exists)
            {
                using (StreamReader reader = new StreamReader(file.OpenRead()))
                {
                    location = reader.ReadLine();
                    reader.Close();
                }
            }

            return location;
        }

        public static void SaveExportLocation(string location)
        {

            FileInfo file = AppData.File(Environment.SpecialFolder.LocalApplicationData, ExportSettingsFile);

            if (file.Exists)
                file.Delete();

            using (StreamWriter writer = new StreamWriter(file.OpenWrite()))
            {
                writer.WriteLine(location);
                writer.Flush();
                writer.Close();
            }
        }
        
        public static LocalArchiveSettings GetArchiveSettings()
        {
            LocalArchiveSettings settings = new LocalArchiveSettings();
            settings.PreArchiveStatus = DefaultStatus.ToArchive;
            settings.PostArchiveStatus = DefaultStatus.Archived;
            settings.ArchiveDevice = string.Empty;
            settings.DeleteCase = false;
            settings.PruneData = false;
            settings.TimerInterval = 24;

            FileInfo file = AppData.File(Environment.SpecialFolder.LocalApplicationData, ArchiveSettingsFile);

            if (file.Exists)
            {
                using (StreamReader reader = new StreamReader(file.OpenRead()))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        int indx = line.IndexOf("=");

                        if ((indx != -1)  && (line.Length >= indx + 1))
                        {
                            string flag = line.Substring(0, indx);
                            string value = line.Substring(indx + 1);

                            switch (flag)
                            {   
                                case "DeleteCase":
                                    settings.DeleteCase = value.ToLower()=="true"? true:false;
                                    break;

                                case "PruneData":
                                    settings.PruneData = value.ToLower() == "true" ? true : false;
                                    break;

                                case "TimerInterval":
                                    settings.TimerInterval = int.Parse(value);
                                    break;
                                case "PreArchiveStatus":
                                    settings.PreArchiveStatus = value;
                                    break;
                                case "PostArchiveStatus":
                                    settings.PostArchiveStatus = value;
                                    break;
                                case "ArchiveDevice":
                                    settings.ArchiveDevice = value;
                                    break;
                            }
                        }
                    }

                    reader.Close();
                }
            }

            return settings;
        }

        public static void SaveArchiveSettings(LocalArchiveSettings settings)
        {

            FileInfo file = AppData.File(Environment.SpecialFolder.LocalApplicationData, ArchiveSettingsFile);

            if (file.Exists)
                file.Delete();

            using (StreamWriter writer = new StreamWriter(file.OpenWrite()))
            {
                writer.WriteLine("ArchiveDevice=" + settings.ArchiveDevice.ToString());
                writer.WriteLine("PruneData=" + settings.PruneData.ToString());
                writer.WriteLine("TimerInterval=" + settings.TimerInterval.ToString());
                writer.WriteLine("DeleteCase=" + settings.DeleteCase.ToString());
                writer.WriteLine("PreArchiveStatus=" + settings.PreArchiveStatus.ToString());
                writer.WriteLine("PostArchiveStatus=" + settings.PostArchiveStatus.ToString());
                writer.Flush();
                writer.Close();
            }
        }
    }
}
