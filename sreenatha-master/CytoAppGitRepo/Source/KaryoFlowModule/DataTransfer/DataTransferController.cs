﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI
{
    public class DatatransferController : Notifier
    {
        private string errMessage = string.Empty;
        private string process = string.Empty;  //  process being performed (Archive/Import etc)
        private int totalSteps = 100;
        private int currentStep = 0;
        private double pcentComplete = 0.00;
        private Boolean processComplete = false;
        private Boolean uiVisible = false;

        public DatatransferController()
        {
        }

        public string Process
        {
            get { return process; }
            set 
            {
                if (value != process)
                {
                    process = value;
                    processComplete = false;
                    Notify("Process");
                }
            }
        }

        public string Error
        {
            get { return errMessage; }
            set
            {
                if (value != errMessage)
                {
                    errMessage = value;
                    Notify("Error");
                    ProcessComplete = true;
                }
            }
        }

        public int PercentComplete
        {
            get
            {
                return (int)pcentComplete; 
            }
        }

        public int TotalSteps
        {
            get
            {
                return totalSteps;
            }

            set
            {
                if (value != totalSteps)
                {
                    totalSteps = value;
                    Notify("TotalSteps");
                }
            }
        }

        public int CurrentStep
        {
            get
            {
                return currentStep;
            }

            set
            {
                if (value != currentStep)
                {
                    currentStep = value;

                    if(currentStep != 0)  // dont want div 0
                        pcentComplete = (((double)currentStep/(double)totalSteps)*(double)100);
                    else
                        pcentComplete = (double)0.00;

                    Notify("PercentComplete");
                    Notify("CurrentStep");
                }
            }
        }

        public Boolean ProcessComplete
        {
            get { return processComplete; }
            set
            {
                processComplete = value;
                Notify("ProcessComplete");
            }
        }

        public Boolean ShowProgressBar
        {
            get { return uiVisible; }
            set
            {
                uiVisible = value;
                Notify("ShowProgressBar");
                Notify("AllowInteraction");
            }
        }

        public void NewProcess(string process, int totalsteps)
        {
            Process = process;
            TotalSteps = totalsteps;
            currentStep = 0;
            pcentComplete = 0.00;
            errMessage = string.Empty;
            Notify("Process");
        }
    }
}
