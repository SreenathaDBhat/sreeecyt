﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Data.Linq;
using System.Reflection;
using System.Data.Common;
using System.Data.Linq.Mapping;
using AI.Connect;

namespace AI
{
    public class ErrorEventArgs
    {
        public ErrorEventArgs(int code, string msg) { Code = code; Message = msg; }
        public string Message { get; private set; }
        public int  Code { get; private set; }
    }

    public class Importer : IDataTransferTask,  IDisposable
    {
        private DatatransferController controller = null;

        private string importFolder = string.Empty;
        private string tempFolder = string.Empty;
        private string caseFolder = string.Empty;
        private Guid caseID = Guid.Empty;
        private string errMessage = string.Empty;
        private int totalRecs = 0;
        private int currentRec = 0;
        private string passWord = string.Empty;
        private Boolean encrypted = false;
        private string filename = string.Empty;
        private string caseImported = string.Empty;

        public Importer(DatatransferController controller,   string arcFolder)
        {
            if (controller != null)
                this.controller = controller;

            importFolder = arcFolder;

            if (arcFolder.Substring(arcFolder.Length - 1, 1) != @"\")
                importFolder += @"\";

            tempFolder = Path.GetTempPath() + Guid.NewGuid().ToString();

        }

        public string LastErrorMessage
        {
            get { return errMessage; }
        }

        public string CaseImported
        {
            get { return caseImported; }
        }



        public void SetParameters(string filename)
        {
            this.filename = filename;
            this.encrypted = false;
            this.passWord = string.Empty;
        }

        public void SetParameters(string filename, Boolean encrypted, string password)
        {
            this.filename = filename;
            this.encrypted = encrypted;
            this.passWord = password;
        }

        public void Import(string filename, Boolean encrypted, string password)
        {
            errMessage = string.Empty;
            passWord = password;

            string fullpath = importFolder + filename;
            string datafile = string.Empty;

            caseFolder = importFolder;
            caseImported = string.Empty;

            if (File.Exists(fullpath))
            { 

                if(controller != null)
                    controller.Process = "Preparing to import " + filename;

                try
                {
                    DirectoryInfo di = new DirectoryInfo(tempFolder);
                    if (!di.Exists)
                        di.Create();

                    Zipper zip = new Zipper();
                    zip.UnZip(controller, fullpath, tempFolder, false);

                    var files = di.GetFiles("*" + TransferType.DATA_EXTENSION);

                    datafile = tempFolder + @"\" + files.FirstOrDefault().Name;

                    if (encrypted)
                        zip.DecryptFile(datafile, password);

                    ImportData(datafile);

                }
                catch (Exception ex)
                {
                    errMessage = ex.Message;

                    if (controller != null)
                    {
                        controller.Error = errMessage;
                    }
                }
                finally
                {
                    DirectoryInfo di = new DirectoryInfo(tempFolder);
                    if (di.Exists)
                    {
                        var files = di.GetFiles("*.*");
                        if (files.Count() > 0)
                        {
                            foreach (var file in files)
                            {
                                file.Delete();
                            }
                        }
                        di.Delete();
                        
                    }

                    if (controller != null)
                        controller.ProcessComplete = true;

                }
            }
            else
            {
                errMessage = "File " + fullpath + " does not exist.";

                if (controller != null)
                {
                    controller.Error = errMessage;
                    controller.ProcessComplete = true;
                }
            }
        }

        private void TotalRecsToParse(XElement caseElement)
        {
            totalRecs = 0;

            XElement slides = caseElement.Element("Slides");
            if (slides != null)
            {
                IEnumerable<XElement> slideElements = slides.Elements("Slide");

                foreach (var slide in slideElements)
                {
                    XElement defCaptures = slide.Element("DeferredCaptures");
                    if( defCaptures != null)
                    {
                        IEnumerable<XElement> defCapElements = defCaptures.Elements("DeferredCapture");
                        if( defCapElements != null) totalRecs += defCapElements.Count();
                    }

                    XElement imageFrames = slide.Element("Frames");
                    if (imageFrames != null)
                    {
                        IEnumerable<XElement> frameElements = imageFrames.Elements("Frame");

                        foreach (var frame in frameElements)
                        {
                            XElement imageComponents = frame.Element("ImageComponents");
                            if (imageComponents != null)
                            {
                                IEnumerable<XElement> componentElements = imageComponents.Elements("ImageComponent");

                                foreach (var component in componentElements)
                                {
                                    XElement imageSnaps = component.Element("ImageSnaps");
                                    if (imageSnaps != null)
                                    {
                                        IEnumerable<XElement> snapElements = imageSnaps.Elements("ImageSnap");
                                        if (snapElements != null) totalRecs += snapElements.Count();
                                    }
                                }
                            }
                        }
                    }

                    XElement cells = slide.Element("Cells");
                    if (cells != null)
                    {
                        IEnumerable<XElement> cellElements = cells.Elements("Cell");

                        foreach (var cell in cellElements)
                        {

                            XElement numberingTags = cell.Element("NumberingTags");
                            if (numberingTags != null)
                            {
                                IEnumerable<XElement> tagElements = numberingTags.Elements("NumberingTag");
                                if (tagElements != null) totalRecs += tagElements.Count();
                            }

                            XElement mcps = cell.Element("ManualCountPoints");
                            if (mcps != null)
                            {
                                IEnumerable<XElement> mcpElements = mcps.Elements("ManualCountPoint");
                                if (mcpElements != null) totalRecs += mcpElements.Count();
                            }

                            XElement ccs = cell.Element("ClearedChromomsomes");
                            if (ccs != null)
                            {
                                IEnumerable<XElement> ccElements = ccs.Elements("ClearedChromomsome");
                                if (ccElements != null) totalRecs += ccElements.Count();
                            }

                            XElement kcs = cell.Element("KaryotypedChromomsomes");
                            if (kcs != null)
                            {
                                IEnumerable<XElement> kcElements = kcs.Elements("KaryotypeChromomsome");
                                if (kcElements != null) totalRecs += kcElements.Count();
                            }
                        }
                    
                    }

                    XElement pss = slide.Element("ProbeScoreSessions");
                    if (pss != null)
                    {
                        IEnumerable<XElement> pssElements = pss.Elements("ProbeScoreSession");

                        foreach (var ps in pssElements)
                        {
                            XElement probeScores = ps.Element("ProbeScores");
                            if (probeScores != null)
                            {
                                IEnumerable<XElement> psElements = probeScores.Elements("ProbeScore");
                                if (psElements != null) totalRecs += psElements.Count();
                            }
                        }
                    }

                    XElement tiles = slide.Element("Tiles");
                    if (tiles != null)
                    {
                        IEnumerable<XElement> tileElements = tiles.Elements("Tile");
                        if (tileElements != null) totalRecs += tileElements.Count();
                    }
                }
            }

            XElement annotations = caseElement.Element("Annotations");
            if (annotations != null)
            {
                IEnumerable<XElement> annoElements = annotations.Elements("Annotation");
                if (annoElements != null) totalRecs += annoElements.Count();
            }

            XElement scratchpads = caseElement.Element("ScratchPads");
            if (scratchpads != null)
            {
                IEnumerable<XElement> padElements = scratchpads.Elements("ScratchPad");

                foreach (var pad in padElements)
                {
                    XElement scratchItems = pad.Element("ScratchPadItems");
                    if (scratchItems != null)
                    {
                        IEnumerable<XElement> itemElements = scratchItems.Elements("ScratchPadItem");
                        if (itemElements != null) totalRecs += itemElements.Count();
                    }
                }
            }
        }

        private void ParseLimsCaseDetails(ILims lims, XElement caseMetaData, AI.Case newCase)
        {
            IEnumerable<XElement> metadata = caseMetaData.Elements("MetaData");

            var list = new List<MetaDataItem>();
            foreach (var md in metadata)
            {
/* Doesn't work as ParseElementToRec needs a Column adorner which isn't on the lims interface class members, could add it but just pull it out for now
                AI.MetaDataItem meta = new AI.MetaDataItem();
                ParseElementToRec(typeof(MetaDataItem), md, meta);
 */
                var meta = new MetaDataItem { Field = md.Element("Field").Value, Value = md.Element("Value").Value };
                list.Add(meta);
            }
            lims.SetCaseMetaData(newCase, list);
        }

        private void ParseLimsSlides(Database db, ILims lims, XElement slides, AI.Connect.LMF_Case newCase)
        {
            IEnumerable<XElement> slideElements = slides.Elements("Slide");

            foreach (var slide in slideElements)
            {
                var newSlide = new Slide
                {
                    Id = slide.Element("Id").Value,
                    Barcode = slide.Element("Barcode").Value,
                    Name = slide.Element("Name").Value,
                    Status = slide.Element("Status").Value
                };

                var lmfSlide = new LMF_Slide
                {
                    barcode = newSlide.Barcode,
                    id = new Guid(newSlide.Id),
                    limsCaseId = newCase.id,
                    name = newSlide.Name
                };
                
                db.LMF_Slides.InsertOnSubmit(lmfSlide);

                XElement slideMetaData = slide.Element("SlideMetaData");
                if (slideMetaData != null)
                {
                    IEnumerable<XElement> metadata = slideMetaData.Elements("MetaData");
                    var list = new List<MetaDataItem>();
                    foreach (var md in metadata)
                    {
                        var meta = new MetaDataItem { Field = md.Element("Field").Value, Value = md.Element("Value").Value };
                        list.Add(meta);
                    }
                    lims.SetSlideMetaData(newSlide, list);
                }
            }
        }

        private void CreateFallBackLims(Database db, ILims lims, XElement caseElement, Connect.Case newCase)
        {
            XElement Case = caseElement.Element("Name");
            var limsCase = new Connect.LMF_Case { name = Case.Value, id = new Guid(newCase.limsRef), created = DateTime.Now };
            db.LMF_Cases.InsertOnSubmit(limsCase);
            db.SubmitChanges();

            XElement xcaseMetaData = caseElement.Element("EncryptedCaseMetaData");
            if (xcaseMetaData != null)
            {
                XElement caseMetaData = XDocument.Parse(Zipper.DecryptString(xcaseMetaData.Value, passWord)).Root;
                ParseLimsCaseDetails(lims, caseMetaData, new AI.Case { Id = limsCase.id.ToString(), Name = limsCase.name });
            }

            XElement slides = caseElement.Element("Slides");
            if (slides != null)
                ParseLimsSlides(db, lims, slides, limsCase);

            db.SubmitChanges();
        }

        private void ImportData(string datafile)
        {

            DbTransaction trans = null;

            //  Parse the XML and insert data into database
            XElement caseElement = XElement.Load(datafile);
            using (ILims lims = Connect.LIMS.New())
            using (Database db = new Database())
            {
                db.Connection.Open();

                trans = db.Connection.BeginTransaction();

                try
                {
                    db.Transaction = trans;

                    Connect.Case newCase = new AI.Connect.Case();

                    ParseElementToRec(typeof(Connect.Case), caseElement, newCase);

                    caseID = newCase.caseId;

                    //  If the caseID (GUID) already exists then DO NOT allow import
                    //  as this will overwrite any changes done sine the export/archive
                    var checkCase = db.Cases.Where(f => f.caseId == caseID).FirstOrDefault();
                    if (checkCase != null)
                    {
                        // case already exists
                        if (controller != null)
                        {
                            controller.Error = "Case already exists!";
                            controller.ProcessComplete = true;
                        }
                        return;
                    }

                    caseImported = string.Empty;

                    //  Check if the Lims case data exists
                    var exists = lims.GetCase(newCase.limsRef);

                    if (exists == null)
                    {
                        //  If this is an archive from another system,
                        //  the GUIDs should be different however
                        //  casenames could possible end up duplicated.
                        //  Check if the casename already exist and if it does
                        //  then append a numerical value to differentiate.
                        caseImported = caseElement.Element("Name").Value;
                        exists = lims.FindCase(caseImported);
                        int c = 0;
                        while (exists != null)  // caseName exists
                        {
                            c++;
                            exists = lims.FindCase(caseImported + c.ToString());
                        }

                        if (c > 0)
                            caseElement.Element("Name").Value = caseImported + c.ToString();
                    }


                    //  if case does not exist and it is an internal 
                    //  lims system then we can create it
                    if (exists == null && Connect.LIMS.CanCreateCases)
                    {
                        CreateFallBackLims(db, lims, caseElement, newCase);
                        caseImported = caseElement.Element("Name").Value;
                    }
                    else caseImported = exists.Name;

                    if (exists != null || Connect.LIMS.CanCreateCases)
                    {
                        db.Cases.InsertOnSubmit(newCase);
                        db.SubmitChanges();

                        //  The case can not be locked until it is 
                        //  submitted and can be seen as a dirty-read
                        //  by the AcquireCaseLock routine even though
                        //  the transaction has not been comitted. So
                        //  submit the new case and then acquire the
                        //  lock immediately so that another user can
                        //  not open the case while it is being imported.
                        CaseLocking.AcquireCaseLock(caseID);

                        if (controller != null)
                        {
                            TotalRecsToParse(caseElement);
                            controller.NewProcess("Parsing data for " + caseImported, totalRecs);
                        }

                        XElement slides = caseElement.Element("Slides");
                        if (slides != null)
                            ParseSlides(db, slides);

                        XElement annotations = caseElement.Element("Annotations");
                        if (annotations != null)
                            ParseAnnotations(db, annotations);

                        XElement scratchpads = caseElement.Element("ScratchPads");
                        if (scratchpads != null)
                            ParseScratchpads(db, scratchpads);

                        db.SubmitChanges();
                    }
                    else
                    {
                        // Lims data does not exist
                        if (controller != null)
                        {
                            controller.Error = "Lims data does not exist! Import failed";
                            controller.ProcessComplete = true;
                        }
                    }

                    trans.Commit();

                    CaseLocking.ReleaseCaseLock(caseID);

                }
                catch (Exception ex)
                {
                    if (controller != null)
                    {
                        controller.Error = ex.Message;
                        controller.ProcessComplete = true;
                    }

                    trans.Rollback();

                    CaseLocking.ReleaseCaseLock(caseID);

                }
            }
        }

        private void ParseElementToRec(System.Type recType,  XElement element, object newRec)
        {
            //  Element name is case sensitve when trying to extract a value
            //  and the database table column name may be of a different case,
            //  we therefore use a dictionary to get the correct case for extracting
            //  the appropriate element data.
            Dictionary<string,string> eNames = new Dictionary<string,string>();

            foreach (var e in element.Elements())
                eNames.Add(e.Name.LocalName.ToUpper(), e.Name.LocalName);
            
            BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
            var myProps = recType.GetProperties(b);

            foreach (var p in myProps)
            {
                object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                if (info.Length == 1)
                {
                    object param = null;
                    string dataValue = string.Empty;

                    string elementname = eNames[p.Name.ToUpper()];

                    if (element.Element(elementname).HasElements)  // if table column is an XML field then handle here
                    {
                        dataValue = element.Element(elementname).FirstNode.ToString();
                        param = XElement.Parse(dataValue);
                    }
                    else
                    {
                        dataValue = element.Element(elementname).Value.ToString();

                        if (dataValue.StartsWith(CDATA.CDATA_START))
                        {
                            dataValue = dataValue.Substring(CDATA.CDATA_START.Length, dataValue.Length - (CDATA.CDATA_START.Length + CDATA.CDATA_END.Length));
                            param = XElement.Parse(dataValue);
                        }
                        else
                        {
                            if (p.PropertyType == typeof(Guid) || p.Name.ToUpper().Contains("BLOBID"))
                            {
                                if (dataValue.Length > 0)
                                    param = new Guid(dataValue);
                            }
                            else
                                param = System.Convert.ChangeType(dataValue, p.PropertyType);
                        }
                    }

                    if (param != null)
                        p.GetSetMethod(true).Invoke(newRec, new[] { param });
                }
            }
        }

        private void ParseAnnotations(Database db, XElement annotations)
        {
            IEnumerable<XElement> annoElements = annotations.Elements("Annotation");

            foreach (var anno in annoElements)
            {
                Connect.Annotation newAnno = new Connect.Annotation();

                ParseElementToRec(typeof(Connect.Annotation), anno, newAnno);

                db.Annotations.InsertOnSubmit(newAnno);

                if (controller != null)
                    controller.CurrentStep = ++currentRec;

            }
        }

        private void ParseScratchpads(Database db, XElement scratchpads)
        {
            IEnumerable<XElement> padElements = scratchpads.Elements("ScratchPad");

            foreach (var pad in padElements)
            {
                Connect.ScratchPad newPad = new Connect.ScratchPad();

                ParseElementToRec(typeof(Connect.ScratchPad), pad, newPad);

                db.ScratchPads.InsertOnSubmit(newPad);

                XElement scratchItems = pad.Element("ScratchPadItems");
                if (scratchItems != null)
                    ParseScratchpadItems(db, scratchItems);
            }
        }

        private void ParseScratchpadItems(Database db, XElement scratchItems)
        {
            IEnumerable<XElement> itemElements = scratchItems.Elements("ScratchPadItem");

            foreach (var item in itemElements)
            {
                Connect.ScratchPadItem newItem = new Connect.ScratchPadItem();

                ParseElementToRec(typeof(Connect.ScratchPadItem), item, newItem);

                db.ScratchPadItems.InsertOnSubmit(newItem);

                if (controller != null)
                    controller.CurrentStep = ++currentRec;

            }
        }

        private void ParseSlides(Database db, XElement slides)
        {
            IEnumerable<XElement> slideElements = slides.Elements("Slide");

            foreach (var slide in slideElements)
            {
                Connect.Slide newSlide = new Connect.Slide();

                ParseElementToRec(typeof(Connect.Slide), slide, newSlide);

                db.Slides.InsertOnSubmit(newSlide);

                XElement defCaptures = slide.Element("DeferredCaptures");
                if (defCaptures != null)
                    ParseDeferredCaptures(db, defCaptures);

                XElement imageFrames = slide.Element("Frames");
                if (imageFrames != null)
                    ParseImageFrame(db, imageFrames);

                XElement cells = slide.Element("Cells");
                if (cells != null)
                    ParseCells(db, cells);

                XElement pss = slide.Element("ProbeScoreSessions");
                if (pss != null)
                    ParseProbeScoreSessions(db, pss);

                XElement tiles = slide.Element("Tiles");
                if (tiles != null)
                    ParseSlideTiles(db, tiles);
            }
        }

        private void ParseCells(Database db, XElement cells)
        {
            IEnumerable<XElement> cellElements = cells.Elements("Cell");

            foreach (var cell in cellElements)
            {
                Connect.Cell newCell = new Connect.Cell();

                ParseElementToRec(typeof(Connect.Cell), cell, newCell);
               
                db.Cells.InsertOnSubmit(newCell);

                //  newFrame.blobid is pointer to a thumbnail image
                if (newCell.karyotypedBlobId != null && newCell.karyotypedBlobId != Guid.Empty)
                {
                    XElement blob = cell.Element("Blob");
                    if (blob != null)
                        ParseBlob(db, blob);
                }

                XElement numberingTags = cell.Element("NumberingTags");
                if (numberingTags != null)
                    ParseNumberingTags(db, numberingTags);

                XElement mcps = cell.Element("ManualCountPoints");
                if (mcps != null)
                    ParseManualCountPoints(db, mcps);

                XElement ccs = cell.Element("ClearedChromosomes");
                if (ccs != null)
                    ParseClearedChromosomes(db, ccs);

                XElement kcs = cell.Element("KaryotypeChromosomes");
                if (kcs != null)
                    ParseKaryotypeChromosome(db, kcs);

                XElement mds = cell.Element("CellMetaDatas");
                if (mds != null)
                    ParseCellMetaData(db, mds);

            }
        }

        private void ParseCellMetaData(Database db, XElement mds)
        {
            IEnumerable<XElement> mdElements = mds.Elements("CellMetaData");

            foreach (var md in mdElements)
            {
                Connect.CellMetaData newMetaData = new Connect.CellMetaData();

                ParseElementToRec(typeof(Connect.CellMetaData), md, newMetaData);

                db.CellMetaDatas.InsertOnSubmit(newMetaData);
            }
        }

        private void ParseNumberingTags(Database db, XElement tags)
        {
            IEnumerable<XElement> tagElements = tags.Elements("NumberingTag");

            foreach (var tag in tagElements)
            {
                Connect.NumberingTag newTag = new Connect.NumberingTag();

                ParseElementToRec(typeof(Connect.NumberingTag), tag, newTag);

                db.NumberingTags.InsertOnSubmit(newTag);

                if (controller != null)
                    controller.CurrentStep = ++currentRec;

            }
        }

        
        private void ParseManualCountPoints(Database db, XElement mcps)
        {
            IEnumerable<XElement> mcpElements = mcps.Elements("ManualCountPoint");

            foreach (var mcp in mcpElements)
            {
                Connect.ManualCountPoint newMCP = new Connect.ManualCountPoint();

                ParseElementToRec(typeof(Connect.ManualCountPoint), mcp, newMCP);

                db.ManualCountPoints.InsertOnSubmit(newMCP);

                if (controller != null)
                    controller.CurrentStep = ++currentRec;

            }
        }

        private void ParseClearedChromosomes(Database db, XElement ccs)
        {
            IEnumerable<XElement> ccElements = ccs.Elements("ClearedChromosome");

            foreach (var cc in ccElements)
            {
                Connect.ClearedChromosome newCC = new Connect.ClearedChromosome();

                ParseElementToRec(typeof(Connect.ClearedChromosome), cc, newCC);

                db.ClearedChromosomes.InsertOnSubmit(newCC);

                if (controller != null)
                    controller.CurrentStep = ++currentRec;

            }
        }


        private void ParseProbeScoreSessions(Database db, XElement pss)
        {
            IEnumerable<XElement> pssElements = pss.Elements("ProbeScoreSession");

            foreach (var ps in pssElements)
            {
                Connect.ProbeScoreSession newPSS = new Connect.ProbeScoreSession();

                ParseElementToRec(typeof(Connect.ProbeScoreSession), ps, newPSS);

                db.ProbeScoreSessions.InsertOnSubmit(newPSS);

                XElement probeScores = ps.Element("ProbeScores");
                if (probeScores != null)
                    ParseProbeScores(db, probeScores);
            }
        }

        private void ParseProbeScores(Database db, XElement probeScores)
        {
            IEnumerable<XElement> psElements = probeScores.Elements("ProbeScore");

            foreach (var ps in psElements)
            {
                Connect.ProbeScore newPS = new Connect.ProbeScore();

                ParseElementToRec(typeof(Connect.ProbeScore), ps, newPS);

                db.ProbeScores.InsertOnSubmit(newPS);
            }
        }

        

        private void ParseImageFrame(Database db, XElement frames)
        {
            IEnumerable<XElement> frameElements = frames.Elements("Frame");

            foreach (var frame in frameElements)
            {
                Connect.ImageFrame newFrame = new Connect.ImageFrame();

                ParseElementToRec(typeof(Connect.ImageFrame), frame, newFrame);

                db.ImageFrames.InsertOnSubmit(newFrame);

                //  newFrame.blobid is pointer to a thumbnail image
                if (newFrame.blobid != null && newFrame.blobid != Guid.Empty)
                {
                    XElement blob = frame.Element("Blob");
                    if (blob != null)
                        ParseBlob(db, blob);
                }

                XElement imageComponents = frame.Element("ImageComponents");
                if (imageComponents != null)
                    ParseImageComponents(db, imageComponents);
            }
        }

        private void ParseImageComponents(Database db, XElement components)
        {
            IEnumerable<XElement> componentElements = components.Elements("ImageComponent");

            foreach (var component in componentElements)
            {
                Connect.ImageComponent newComponent = new Connect.ImageComponent();

                ParseElementToRec(typeof(Connect.ImageComponent), component, newComponent);

                db.ImageComponents.InsertOnSubmit(newComponent);

                XElement imageSnaps = component.Element("ImageSnaps");
                if (imageSnaps != null)
                    ParseImageSnaps(db, imageSnaps);

            }
        }

        private void ParseDeferredCaptures(Database db, XElement defCaps)
        {
            IEnumerable<XElement> defCapElements = defCaps.Elements("DeferredCapture");

            foreach (var defcap in defCapElements)
            {
                Connect.DeferredCapture newDefCap = new Connect.DeferredCapture();

                ParseElementToRec(typeof(Connect.DeferredCapture), defcap, newDefCap);

                db.DeferredCaptures.InsertOnSubmit(newDefCap);

                if (controller != null)
                {
                    controller.CurrentStep = ++currentRec;
                }

            }
        }


        private void ParseImageSnaps(Database db, XElement snaps)
        {
            IEnumerable<XElement> snapElements = snaps.Elements("ImageSnap");

            foreach (var snap in snapElements)
            {
                Connect.ImageSnap newSnap = new Connect.ImageSnap();

                ParseElementToRec(typeof(Connect.ImageSnap), snap, newSnap);

                db.ImageSnaps.InsertOnSubmit(newSnap);

                XElement blob = snap.Element("Blob");
                if (blob != null)
                    ParseBlob(db, blob);

                if (controller != null)
                    controller.CurrentStep = ++currentRec;

            }
        }

        private void ParseSlideTiles(Database db, XElement tiles)
        {
            IEnumerable<XElement> tileElements = tiles.Elements("Tile");

            foreach (var tile in tileElements)
            {
                Connect.SlideImageTile newTile = new Connect.SlideImageTile();

                ParseElementToRec(typeof(Connect.SlideImageTile), tile, newTile);

                db.SlideImageTiles.InsertOnSubmit(newTile);

                XElement blob = tile.Element("Blob");
                if (blob != null)
                    ParseBlob(db, blob);

                if (controller != null)
                    controller.CurrentStep = ++currentRec;
            }
        }

        
        private void ParseKaryotypeChromosome(Database db, XElement kcs)
        {
            IEnumerable<XElement> kcElements = kcs.Elements("KaryotypeChromosome");

            foreach (var kc in kcElements)
            {
                Connect.KaryotypeChromosome newKC = new Connect.KaryotypeChromosome();

                ParseElementToRec(typeof(Connect.KaryotypeChromosome), kc, newKC);

                db.KaryotypeChromosomes.InsertOnSubmit(newKC);

                XElement blob = kc.Element("Blob");
                if (blob != null)
                    ParseBlob(db, blob);

                if (controller != null)
                    controller.CurrentStep = ++currentRec;
            }
        }

        private void ParseBlob(Database db, XElement blob)
        {
            Connect.Blob newBlob = new Connect.Blob();
            
            BindingFlags b = BindingFlags.Instance | BindingFlags.Public;
            var myProps = typeof(Connect.Blob).GetProperties(b);

            foreach (var p in myProps)
            {
                object[] info = p.GetCustomAttributes(typeof(ColumnAttribute), true);
                if (info.Length == 1)
                {
                    if (p.Name.ToUpper() != "BINARYDATA"  )
                    {
                        object param = null;

                        if (p.PropertyType == typeof(Guid))
                            param = new Guid(blob.Element(p.Name).Value.ToString());
                        else
                            param = System.Convert.ChangeType(blob.Element(p.Name).Value.ToString(), p.PropertyType);

                        if (param != null)
                            p.GetSetMethod(true).Invoke(newBlob, new[] { param });
                    }
                }
            }
            
            string fullpath = tempFolder + @"\" + newBlob.path;

            if (File.Exists(fullpath))
            {
                using (FileStream source = new FileStream(fullpath, FileMode.Open))
                {
                    long bufferSize = source.Length;
                    byte[] buffer = new byte[bufferSize];
                    int bytesRead = 0;

                    bytesRead = source.Read(buffer, 0, buffer.Length);

                    BlobStream.WriteBlobStream(caseID, newBlob.blobId, newBlob.path, buffer, int.Parse(bufferSize.ToString()), db);
                    
                    source.Close();
                }
                
                File.Delete(fullpath);
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion

        #region IDataTransferTask Members

        void IDataTransferTask.Transfer()
        {
            Import(filename, encrypted, passWord);
        }

        #endregion
    }
}
