﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Threading;
using System.Threading;

namespace AI
{
    /// <summary>
    /// Interaction logic for XferProgress.xaml
    /// </summary>
    public partial class XferProgress : Window
    {
        private DatatransferController controller = null;
        private IDataTransferTask task = null;
        private Boolean allowClose = false;

        public XferProgress(DatatransferController xferctrl, IDataTransferTask xferTask)
        {
            controller = xferctrl;
            DataContext = controller;
            task = xferTask;
            
            controller.PropertyChanged += new PropertyChangedEventHandler(controller_PropertyChanged);
            
            InitializeComponent();

            Closing += new CancelEventHandler(XferProgress_Closing);
            Loaded += new RoutedEventHandler(XferProgress_Loaded);
        }

        void XferProgress_Loaded(object sender, RoutedEventArgs e)
        {
            ThreadPool.QueueUserWorkItem(delegate
            {
                task.Transfer();
            }, DispatcherPriority.Normal);
        }

        public XferProgress(DatatransferController xferctrl)
        {
            controller = xferctrl;
            this.DataContext = controller;

            controller.PropertyChanged += new PropertyChangedEventHandler(controller_PropertyChanged);

            InitializeComponent();

            Closing += new CancelEventHandler(XferProgress_Closing);
        }


        void XferProgress_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = !allowClose;
            controller.PropertyChanged -= controller_PropertyChanged;
        }

        private void OnDragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        void controller_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "ProcessComplete":
                    if (controller.Error.Length == 0)
                    {
                        controller.PropertyChanged -= controller_PropertyChanged;
                        Dispatcher.Invoke((ThreadStart)delegate
                        {
                            allowClose = true;
                            this.Close();
                        }, DispatcherPriority.Normal);
                    }
                    break;
                case "Error":
                    Dispatcher.Invoke((ThreadStart)delegate
                    {
                        //progressBar.Visibility = Visibility.Collapsed;
                        processName.Text = controller.Error;
                        closeButton.Visibility = Visibility.Visible;
                        allowClose = true;
                        //                        errorMsg.Visibility = Visibility.Visible;
                    }, DispatcherPriority.Normal);
                    break;
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
