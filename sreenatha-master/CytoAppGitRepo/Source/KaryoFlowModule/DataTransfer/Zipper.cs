﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Packaging;
using System.Security.Cryptography;

namespace AI
{
    public class Zipper
    {
        private DatatransferController controller = null;

        private const long BUFFER_SIZE = 4096;
        private static byte[] encryptionKey = { 1, 23, 2, 78, 4, 111, 39, 90, 76, 46, 77, 78, 8, 89, 53, 12, 3, 9, 19, 72, 21, 22, 29, 13, 126, 26, 48, 10, 26, 12, 7, 88 };
        private static byte[] encryptionIV = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };

        public void AddFileToZip(string zipFilename, string fileToAdd)
        {
            AddFileToZip(zipFilename, fileToAdd, false, string.Empty);
        }        

        public void AddFileToZip(string zipFilename, string fileToAdd, Boolean encryptFile, string password) 
        {
            if (encryptFile)
            {
                string tempfile = Guid.NewGuid().ToString();

                using (FileStream source = new FileStream(fileToAdd, FileMode.Open))
                {
                    using (FileStream fs = new FileStream(tempfile, FileMode.Create))
                    {
                        CryptoStream dest = GetEncryptionStream(fs, password);

                        long bufferSize = source.Length < BUFFER_SIZE ? source.Length : BUFFER_SIZE;
                        byte[] buffer = new byte[bufferSize];
                        int bytesRead = 0;
                        long bytesWritten = 0;

                        while ((bytesRead = source.Read(buffer, 0, buffer.Length)) != 0)
                        {
                            dest.Write(buffer, 0, bytesRead);
                            bytesWritten += bufferSize;
                        }
                        
                        dest.Close();
                    }
                    source.Close();
                }

                File.Delete(fileToAdd);

                File.Move(tempfile, fileToAdd);
            }
            
            using (Package zip = System.IO.Packaging.Package.Open(zipFilename, FileMode.OpenOrCreate)) 
            {
                string destFilename = ".\\" + Path.GetFileName(fileToAdd); 
                
                Uri uri = PackUriHelper.CreatePartUri(new Uri(destFilename, UriKind.Relative)); 
                
                if (zip.PartExists(uri)) 
                { 
                    zip.DeletePart(uri); 
                } 

                PackagePart part = zip.CreatePart(uri, "", CompressionOption.Normal);

                using (FileStream fileStream = new FileStream(fileToAdd, FileMode.Open, FileAccess.Read)) 
                { 
                    using (Stream dest = part.GetStream()) 
                    {
                        CopyStream(fileStream, dest);
                    } 
                } 
            } 
        }

        public void UnZip(string zipFilename, string toFolder)
        {
            UnZip(zipFilename, toFolder, false);
        }

        public void UnZip(DatatransferController controller, string zipFilename, string toFolder, Boolean deleteZip)
        {
            this.controller = controller;
            UnZip(zipFilename, toFolder, deleteZip);
        }        

        public void UnZip(string zipFilename, string toFolder, Boolean deleteZip)
        {
            using (Package zip = System.IO.Packaging.Package.Open(zipFilename, FileMode.Open))
            {

                PackagePartCollection ppc = zip.GetParts();

                if (controller != null)
                {
                    controller.NewProcess("Unzipping files", ppc.Count());
                }

                int counter = 0;

                foreach (var part in ppc)
                {
                    Uri fileUri = part.Uri;
                    string filename = toFolder + fileUri.OriginalString.Replace("/", @"\");

                    Stream byteStream = part.GetStream();
                    int dataLength = (int) byteStream.Length;
                    byte[] data = new byte[dataLength];
                    int numRead = byteStream.Read(data,(int) 0, (int) dataLength);

                    if (!File.Exists(filename))
                    {
                        FileStream fs = File.Create(filename);
                        BinaryWriter bw = new BinaryWriter(fs);
                        bw.Write(data);
                        bw.Close();
                        fs.Close();
                    }

                    if (controller != null)
                    {
                        controller.CurrentStep = ++counter;
                    }
                    
                }
            }
    
            if(deleteZip)
                File.Delete(zipFilename);

        }

        public void DecryptFile(string filename, string password) 
        {
            var key = GenerateKey(password);
            string tempfile = Guid.NewGuid().ToString();

            using (FileStream source = new FileStream(filename, FileMode.Open))
            {
                using (FileStream fs = new FileStream(tempfile, FileMode.Create))
                {
                    CryptoStream cs = GetDecryptionStream(source, key);

                    long bufferSize = source.Length < BUFFER_SIZE ? source.Length : BUFFER_SIZE;
                    byte[] buffer = new byte[bufferSize];
                    int bytesRead = 0;
                    long bytesWritten = 0;

                    while ((bytesRead = cs.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        fs.Write(buffer, 0, bytesRead);
                        bytesWritten += bufferSize;
                    }

                    cs.Close();
                    fs.Close();
                }
            }

            File.Delete(filename);

            File.Move(tempfile, filename);
        }

        private void CopyStream(System.IO.FileStream inputStream, System.IO.Stream outputStream) 
        { 
            long bufferSize = inputStream.Length < BUFFER_SIZE ? inputStream.Length : BUFFER_SIZE; 
            byte[] buffer = new byte[bufferSize]; 
            int bytesRead = 0; 
            long bytesWritten = 0;

            while ((bytesRead = inputStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                outputStream.Write(buffer, 0, bytesRead);
                bytesWritten += bufferSize;
            }
        }

        private static byte [] GenerateKey(string password)
        {
            byte[] array = new byte[32];
            if (password.Length > 0)
            {
                int len = password.Length <= 32 ? password.Length : 32;
                char[] chararray = password.ToCharArray();

                for (int x = 0; x < encryptionKey.Length; x++)
                {
                    array[x] = encryptionKey[x];
                    if (x < len)
                        array[x] = (byte)chararray[x];
                }
            }
            return array;
        }

        private static CryptoStream GetEncryptionStream(Stream stream, string password)
        {
            var key = GenerateKey(password);
            return GetEncryptionStream(stream, key);
        }

        private static CryptoStream GetEncryptionStream(Stream stream, byte[] key)
        {
            try
            {
                Rijndael RijndaelAlg = Rijndael.Create();
                CryptoStream cStream = new CryptoStream(stream,
                                                        RijndaelAlg.CreateEncryptor(key, encryptionIV),
                                                        CryptoStreamMode.Write);
                return cStream;
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("A Cryptographic error occurred: {0}", e.Message);
            }
            return null;
        }

        private static CryptoStream GetDecryptionStream(Stream stream, byte[] key)
        {
            try
            {
                Rijndael RijndaelAlg = Rijndael.Create();
                CryptoStream cStream = new CryptoStream(stream,
                                                        RijndaelAlg.CreateDecryptor(key, encryptionIV),
                                                        CryptoStreamMode.Read);
                return cStream;
            }
            catch (CryptographicException e)
            {
                Console.WriteLine("A Cryptographic error occurred: {0}", e.Message);
            }
            return null;
        }

        public static string EncryptString(string rawstring, string password)
        {
            MemoryStream ms = new MemoryStream();
            var cryptoStream = GetEncryptionStream(ms, password);

            StreamWriter sw = new StreamWriter(cryptoStream);
            sw.Write(rawstring);
            sw.Flush();
            cryptoStream.FlushFinalBlock();
            return Convert.ToBase64String(ms.GetBuffer(), 0, (int)ms.Length);
        }

        public static string DecryptString(string encoded, string password)
        {
            var key = GenerateKey(password);
            MemoryStream ms = new MemoryStream(Convert.FromBase64String(encoded));
            var cryptoStream = GetDecryptionStream(ms, key);
            StreamReader reader = new StreamReader(cryptoStream);
            return reader.ReadToEnd();
        }
    }
}
