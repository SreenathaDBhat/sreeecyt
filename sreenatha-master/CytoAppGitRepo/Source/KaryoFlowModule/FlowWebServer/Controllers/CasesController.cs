﻿using AI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
/// <summary>
/// Note to self as it was a pain in the arse: to handle large filesyou will have to increase the ASP.net maxRequestLength KB
///     Web.config <httpRuntime targetFramework="4.5" maxRequestLength="2048000" />
/// and the IIS Express maxContentLength Bwhich you can find in
///     %USERPROFILE%\Documents\IIS Express\config\applicationHost.config
/// <system.webServer>
///<security>
///     <security>
///        <requestFiltering>
///            <requestLimits maxAllowedContentLength="524288000"/>
///        </requestFiltering>
///    </security>
/// Also if you host in IIS you will also have to increase the maxContentLength
/// <system.webServer>
///<security>
///  <requestFiltering>
///    <requestLimits maxAllowedContentLength="52428800" /> <!--50MB-->
///  </requestFiltering>
///</security>
///</system.webServer>
///Or in IIS (7):
///
///Select the website you want enable to accept large file uploads.
///In the main window double click 'Request filtering'
///Select "Edit Feature Settings"
///Modify the "Maximum allowed content length (bytes)"
/// </summary>

namespace FlowWebServer.Controllers
{
    [RoutePrefix("api/v1")] // override the default routing
    public class CasesController : ApiController
    {
        // GET api/v1/connected>
        [Route("Connected")]
        [HttpGet]
        public bool Connected()
        {
            var dataSource = GetDataSource();
            return dataSource.TestConnection();
        }

        // GET api/v1/cases>
        [Route("Cases")]
        [HttpGet]
        public IEnumerable<AI.Case> GetCases()
        {
            var dataSource = GetDataSource();
            return dataSource.GetCases(null);
        }

        /// GET api/v1/cases?search=xxx
        [Route("Cases")]
        [HttpGet]
        public IEnumerable<AI.Case> Search(string search)
        {
            var dataSource = GetDataSource();
            return dataSource.GetCases(search);
        }

        // GET api/v1/cases/tutor
        [Route("Cases/{caseId}")]
        [HttpGet]
        public AI.Case GetCase(string caseId)
        {
            var dataSource = GetDataSource();
            var cases = dataSource.GetCases(null);
            var c = cases.Where(a => a.Name.ToLower() == caseId.ToLower()).FirstOrDefault();
            return c;
        }

        // GET api/v1/cases/tutor/slides
        [Route("Cases/{caseId}/Slides")]
        [HttpGet]
        public IEnumerable<AI.Slide> GetSlides(string caseId)
        {
            var dataSource = GetDataSource();
            return dataSource.GetSlides(caseId);
        }

        // GET api/v1/nodes/tutor;slide0
        [Route("Nodes/{keys}")]
        [HttpGet]
        public IEnumerable<string> GetNodes(string keys)
        {
            var dataSource = GetDataSource();
            return dataSource.GetNodes(keys);
        }

        // GET api/v1/data/tutor;slide1;cell1/items/xxxxx.png
        [Route("Data/{keys}/Items/{itemId}/Stream")]
        // Note: I'm tagging the stream part to the end of the url otherwise if the itemId has a '.' IIS will think its an extension
        // and we'll get a 404. Adding another level seems easiest way to get round this.
        [HttpGet]
        public HttpResponseMessage GetDataStream(string keys, string itemId)
        {
            var dataSource = GetDataSource();
            var stream = dataSource.LoadStream(keys, itemId);

            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StreamContent(stream); // this stream will be closed by lower layers of web api for you once the response is completed.
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
            return response;
        }

        [Route("Data/{keys}")]
        [HttpPost]
        public Task<bool> UpdateData(string keys)
        {
            if (Request.Content.IsMimeMultipartContent())
            {
                var tempPath = HttpContext.Current.Server.MapPath("~/app_data/"); // use as a temp area for now

                // need to use this to get a useful name, basically have to write to temp area and move to real place. can't be right !!!
                MyMultipartFormDataStreamProvider streamProvider = new MyMultipartFormDataStreamProvider(tempPath);
                var task = Request.Content.ReadAsMultipartAsync(streamProvider).ContinueWith(t =>
                {
                    if (t.IsFaulted || t.IsCanceled)
                        throw new HttpResponseException(HttpStatusCode.InternalServerError);

                    foreach( var i in streamProvider.FileData)
                    {
                        var f = new FileInfo(i.LocalFileName);
                        // nasty, maybe we don't need the MyMultipartFormDataStreamProvider as we need unique names.
                        //// if casebase is only another volume then move would essential be a copy and we're writing it twice?
                        var newpath = Path.Combine(DataSourceSettings.Location, keys.Replace(";", "\\"), f.Name);
                        if (File.Exists(newpath))
                            File.Delete(newpath);
                        f.MoveTo(newpath);
                    }
                    return true;

                });
                return task;
            }
            else
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotAcceptable, "Invalid Request!"));
            }
        }

        /// <summary>
        /// override the default otherwise you will get a default name for the file
        /// </summary>
        public class MyMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
        {
            public MyMultipartFormDataStreamProvider(string path)
                : base(path)
            {

            }

            public override string GetLocalFileName(System.Net.Http.Headers.HttpContentHeaders headers)
            {
                string fileName;
                if (!string.IsNullOrWhiteSpace(headers.ContentDisposition.FileName))
                {
                    fileName = headers.ContentDisposition.FileName;
                }
                else
                {
                    fileName = Guid.NewGuid().ToString() + ".data";
                }
                return fileName.Replace("\"", string.Empty);
            }
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        private IDataSource GetDataSource()
        {
            // TBD: pick up from a settings file
            return new FolderDataSource("c:\\tubsdataSource");
        }
    }
}