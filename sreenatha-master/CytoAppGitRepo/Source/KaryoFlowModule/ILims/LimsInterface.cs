﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace AI
{
    public class MetaDataItem
    {
        public string Field { get; set; }
        public string Value { get; set; }
    }

    public class Case
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
    }

    public class Slide
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Barcode { get; set; }
        public string Status { get; set; }
    }

    public class CellImagePair
    {
        public string Name { get; set; }
        public Slide Slide { get; set; }
        public BitmapSource Metaphase { get; set; }
        public BitmapSource Karyotype { get; set; }
    }

    public interface ILims : IDisposable
    {
        int CaseCount();
        Case GetCase(string id);
        Case FindCase(string caseName);
        IEnumerable<Case> FindCases(string searchTerm);
        IEnumerable<Case> FindRecentCases(int maxHits);

        IEnumerable<MetaDataItem> GetCaseMetaData(Case c);
        void SetCaseMetaData(Case c, IEnumerable<MetaDataItem> metaData);
        void MarkCaseAsModified(Case c, DateTime modifiedTime, string description);

        Slide GetSlide(string id);
        Slide FindSlide(string barcode);
        IEnumerable<Slide> GetSlides(Case c);
        Case FindCaseForSlide(Slide slide);

        IEnumerable<MetaDataItem> GetSlideMetaData(Slide s);
        void SetSlideMetaData(Slide s, IEnumerable<MetaDataItem> metaData);

        void CompleteCase(Case c, IEnumerable<CellImagePair> resultantImages);
    }
}
    