/*
 * 24bitcanvas.cpp		BP: 23/4/92
 *
 * Probe canvas buffer creation for display.
 *
 * Mods:
 *
 *	21Sep2000	WH: SPR 1568 -Large objects were being truncated due to MAXWIDTH and MAXHEIGHT
 *					being too small. Now 3k by 3k
 *	23Mar2000	JMB	Converted to .cpp, minor mods to reduce warnings,
 *					changed headers included.
 *	dcb	08Jul99		Redefine MAXNINTLINES to 2048 though should be a dynamic value.
 *					Add protection against using a NULL dstintval
 *	KS	15Feb99		New functions set_invert_canvas_BG_col(), get_invert_canvas_BG_col()
 *					set24bitBG(), and mask_8bit_buffer() for background colour stuff.
 *	KS	15Feb99		Changes to draw_overlap_Canvas_buffer() to use new background colour stuff
 *	KS	18Jan99		Added draw_overlap_mask_buffer check for
 *					canavas visibility and if it's OFF then just returns a 0 mask.
 *	KS	14Jan99		Added new functions setCanvasCounterstainAttenuation and
 *					attenuate_24bit_buffer. setCanvasCounterstainAttenuation is
 *					called from padmin to generate local copies of the counterstain
 *					attenuation values. And attenuate_24bit_buffer is called locally
 *					from draw_overlap_Canvas_buffer to do the actual counterstain attenuation.
 *	SN	30Oct98		Fixed draw_overlap_mask_buffer to draw blue components
 *					properly.
 *	MG	15Aug98:	Added canvas visibility check to draw_overlap_Canvas_buffer().
 *	12Mar98 WH:		Windows version of draw_overlap_mask_buffer() that produces
 *					an 8 bit image mask.
 *	19Mar97	BP:		Fixed bogus line at top of object in
 *					draw_overlap_Canvas_buffer().
 *
 *	BP	10/23/96:	Windows version.
 *
 *	MG	22Jul96:	Univision_probe_Canvas_buffer() now recursively searches for
 *				overlap canvases - in case they are at a lower level than
 *				just patches on the main canvas - e.g. CGH met/kar probe overlay
 *				highlight canvases are patches on chromosomes canvases.
 *	BP	3/27/96:	Later that same day... remove reset_probe_frames from
 *					this module! No business here - its up to the application
 *					modules to call reset_probe_frames when required, and
 *					must not be reset whenever the object is redisplayed!
 *	BP	3/27/96:	No more FBORDER - reset_probe_frames has border_width.
 *	BP	10/3/95:	Univision crap back again!
 *	BP	8/23/95:	Allow space for brightfield when calculating position
 *					of Cfetal image buffer.
 *	BP	6/23/95:	Check for outside dog and return NULL. Also added
 *					draw_fetal_Canvas_buffer() which is a much-
 *					abbreviated version of draw_overlapCanvas_buffer().
 *					Also tweaked add_8bit....() to be a tad quicker.
 *	BP	5/11/95:	Use qbuffer (now in woolz).
 *	BP	12/10/94:	New functions draw_overlap_Canvas_buffer() and
 *					draw_overlap_mask_buffer() to replace
 *					display_probe_Canvas() which is now obsolete.
 *					clear_8to24bit_buffer() now also sets upper (unused)
 *					byte of 32-bit image buffer for derivation of mask.
 */



/*
 * The usage of this module c.f. UNIX versions
 * has changed significantly for Windows.
 *
 * These functions are used for all normal Canvas
 * displaying - probe AND grey-scale objects. All
 * Windows object drawing is done through 24-bit
 * DIB buffers (regardless of the depth of the
 * display - Windows drawing functions automatically
 * account for the depth of the display).
 *
 * Utilising the same buffer format for probe and
 * grey-scale objects is less efficient, but has
 * enormous benefits in terms of the clarity and
 * simplicity of the code. And I mean enormous!
 * It makes our code truly, truly, truly platform
 * independent - and allows ALL rendering of objects
 * to go through the SAME code.
 * There is a larger memory requirement (24-bit draw
 * buffer, not 8-bit), and also less efficient
 * rendering (replicate bytes 3 times) but: (a) there
 * is only one draw buffer, so its not much extra
 * memory, and (b) 8-bit would still have to go
 * through LUTs, so there's not a huge performance
 * penalty.
 * The great benefit is that the exact same code is
 * used for probe and grey-scale objects - and there
 * is nothing funky that needs to be done to mix the
 * two together.
 */


#include "canvas.h"
#include "ddgs.h"
#include "woolz.h"

//#include "canvasdll.h"
//#include "/wincv/include/ddgs.h"
//#include "/wincv/include/woolz.h"


#include <stdio.h>
#include <malloc.h>
#include <memory.h>

//#include "ddgs.h"
//#include <wstruct.h>
//#include <common.h>
//#include <canvas.h>
//#include <pcont.h>


/* The following are duplicate of data structures
 * defined in pcont.h - replicated here for
 * convenience. */

/* Type for scanning the canvas
 * RGB luts - which are effectively
 * arrays of 256 of these. */
typedef struct {
	unsigned char r, g, b;
} lut_triple_t;


#pragma pack(2)

/* propertylist data structure
 * for top-level canvas. */
typedef struct {
	SMALL size;
	unsigned char **image;
} compositedata_t;


/* propertylist data dtructure
 * for stain and probe sub-canvases. */
typedef struct {
	SMALL size;
	int id;
	lut_triple_t *lut;
} componentdata_t;


/* propertylist data dtructure
 * for stain and probe sub-canvases
 * in flex which have their own
 * luts. */
typedef struct {
	SMALL size;
	int id;
	lut_triple_t *lut;
	lut_triple_t lutdata[256];
} flex_componentdata_t;

#pragma pack()


// Probe objects all have their own LUTs,
// but grey-scale objects don't. Thus we need
// these to map grey-scale objects into a 24-
// bit buffer.
static lut_triple_t greylut[256], igreylut[256];
static int made_grey_luts = 0;
static int att_factors[999];


/******************************************** clip_8bit_buffer */

/*
 * Clip an objects 8-bit buffer to
 * the limits of the output buffer.
 * The data is reshuffled so the first
 * pixel within the clip bounds is the
 * first value in the buffer. The x, y,
 * w and h of the buffer are tweaked
 * if necessary.
 * The return value is True if at least
 * some of the buffer is within the clip
 * area (in which case the buffer and
 * coordinates will have been modified
 * suitably). False is returned only if
 * the whole buffer is outside the
 * clip area.
 */

#ifdef WIN32
BOOL
clip_8bit_buffer(unsigned char *indata, int *ox, int *oy, int *ow, int *oh, int clipxo, int clipyo, int w, int h)
#endif
#ifdef i386
Boolean
clip_8bit_buffer(indata, ox, oy, ow, oh, clipxo, clipyo, w, h)
unsigned char *indata;
int *ox;
int *oy;
int *ow;
int *oh;
int clipxo;
int clipyo;
int w;
int h;
#endif
{
	register unsigned char *intemp, *outtemp;
	int oldxo, oldyo, oldx1, oldy1;
	int clipx1, clipy1;
	int newxo, newyo, newx1, newy1;
	register int neww, newh, oldw;
	register int j;


/* For convenience, work out some
 * local extremities. */
	oldxo = *ox;
	oldyo = *oy;
	oldx1 = oldxo + *ow - 1;
	oldy1 = oldyo + *oh - 1;

	oldw = *ow;

	clipx1 = clipxo + w - 1;
	clipy1 = clipyo + h - 1;

/* First test for being within the
 * clip area and return if so. */
	if ((oldxo > clipxo) &&
	    (oldyo > clipyo) &&
	    (oldx1 < clipx1) &&
	    (oldy1 < clipy1))
		return(1);

/* Now check for totally outside
 * the clip area. If so return False. */
	if ((oldx1 < clipxo) || 
	    (oldy1 < clipyo) ||
	    (oldxo > clipx1) ||
	    (oldyo > clipy1))
		return(0);

/* If we have got this far we must
 * have an object that crosses the
 * clip boundary. Bugger. This is
 * the tricky bit! */

/* Set the new x extremes to be
 * within the clip widow. */
	if (oldxo < clipxo)
		newxo = clipxo;
	else
		newxo = oldxo;
	if (oldx1 > clipx1)
		newx1 = clipx1;
	else
		newx1 = oldx1;
	neww = newx1 - newxo + 1;

/* Do the same for y. */
	if (oldyo < clipyo)
		newyo = clipyo;
	else
		newyo = oldyo;
	if (oldy1 > clipy1)
		newy1 = clipy1;
	else
		newy1 = oldy1;
	newh = newy1 - newyo + 1;

	if (indata != NULL)
	{
/* Scan through range of new size buffer
 * and read values back in from oversized
 * buffer. Even though input and output
 * buffers are the same we never have a
 * problem with things overlapping since
 * the output data will ALWAYS be smaller
 * than the input - and the valid data is
 * ALWAYS read-back to start at the
 * beginning of the buffer. */
		outtemp = indata;
		intemp = indata + (oldw*(newyo - oldyo) + (newxo - oldxo));
		for (j = 0; j < newh; j++)
		{
			memcpy(outtemp, intemp, neww);

			outtemp += neww;
			intemp += oldw;
		}
	}

	*ox = newxo;
	*oy = newyo;
	*ow = neww;
	*oh = newh;

	return(1);
} /* clip_8bit_buffer */



/******************************************** copy_8to24bit_buffer */

/*
 * Scan through the 8-bit buffer from
 * a woolz object and copy its RGB
 * values into a window on the 24-bit
 * buffer.
 * This is intended for counterstains.
 */

static void
copy_8to24bit_buffer(unsigned char *outdata,
					 int x, int y, int w, int h,
					 unsigned char *indata,
					 int ox, int oy, int ow, int oh,
					 lut_triple_t *rgb)
{
	unsigned char *intemp, *outtemp, *outtempsave;
	register unsigned char thisval;
	register short i, j;
//	int val;

	intemp = indata;
	outtemp = outdata + (4*(w*(oy - y) + (ox - x)));
	for (j = 0; j < oh; j++)
	{
		outtempsave = outtemp;

		for (i = 0; i < ow; i++)
		{
			thisval = *intemp++;

			if (thisval)
			{
				*outtemp++ = rgb[thisval].b;
				*outtemp++ = rgb[thisval].g;
				*outtemp++ = rgb[thisval].r;

				outtemp++;
			}
			else
				outtemp += 4;
		}

		outtemp = outtempsave;
		outtemp += 4*w;
	}
} /* copy_8to24bit_buffer */



/******************************************** clear_8to24bit_buffer */

/*
 * Same as above, but erases stuff
 * from the 24-bit buffer where there
 * are non-zero pixels in the 8-bit
 * buffer.
 * The output buffer (24-bit) is
 * accessed through 32-bit pointers
 * for convenience (and speed ? - maybe).
 * This will be used by intens(0).
 * Note that a value of 0x01000000 is used
 * to indicate that the pixel is clear -
 * this top byte is unused for drawing,
 * but can be detected for deriving clip
 * mask.
 */

#ifdef WIN32
static void
clear_8to24bit_buffer(unsigned *outdata, int x, int y, int w, int h, unsigned char *indata, int ox, int oy, int ow, int oh)
#endif
#ifdef i386
static void
clear_8to24bit_buffer(outdata, x, y, w, h, indata, ox, oy, ow, oh)
unsigned *outdata;
int x;
int y;
int w;
int h;
unsigned char *indata;
int ox;
int oy;
int ow;
int oh;
#endif
{
	unsigned char *intemp;
	unsigned *outtemp, *outtempsave;
	register unsigned char thisval;
	register short i, j;


	intemp = indata;
	outtemp = outdata + (w*(oy - y) + (ox - x));
	for (j = 0; j < oh; j++)
	{
		outtempsave = outtemp;

		for (i = 0; i < ow; i++)
		{
			thisval = *intemp++;

			if (thisval)
				*outtemp++ = 0x01000000;
			else
				outtemp++;
		}

		outtemp = outtempsave;
		outtemp += w;
	}
} /* clear_8to24bit_buffer */



/******************************************** add_8to24bit_buffer */

/*
 * Again, same as above but adds
 * rgb values to the 24-bit buffer.
 * This is a bit more tricky since
 * it needs to check for clipping
 * on the individual colours.
 * This will be used for probes.
 */

#ifdef WIN32
static void
add_8to24bit_buffer(unsigned char *outdata, int x, int y, int w, int h, unsigned char *indata, int ox, int oy, int ow, int oh, lut_triple_t *rgb)
#endif
#ifdef i386
static void
add_8to24bit_buffer(outdata, x, y, w, h, indata, ox, oy, ow, oh, rgb)
unsigned char *outdata;
int x;
int y;
int w;
int h;
unsigned char *indata;
int ox;
int oy;
int ow;
int oh;
lut_triple_t *rgb;
#endif
{
	unsigned char *intemp, *outtemp, *outtempsave;
	register unsigned char oldval, newval, thisval;
	register lut_triple_t *rgbptr;	/* BP - new!! */
	short i, j;	/* BP - new!! Used to be register. */

	intemp = indata;
	outtemp = outdata + (4*(w*(oy - y) + (ox - x)));
	for (j = 0; j < oh; j++)
	{
		outtempsave = outtemp;

		for (i = 0; i < ow; i++)
		{
			thisval = *intemp++;

			if (thisval)
			{
/* Get existing value from image. */
				oldval = *outtemp;

/* Keep pointer to RGB for speed...? */
				rgbptr = &rgb[thisval];		/* BP - new!! */

/* Add new value to existing one.
 * At first glance this < test looks
 * bizarre (x + y < x where x and y
 * are always poitive !) but it will
 * happen when overflow occurs. */

/* BP - new faster version with rgb ptr. */
				if ((newval = oldval + rgbptr->b) < oldval)
					*outtemp++ = 255;
				else
					*outtemp++ = newval;

				oldval = *outtemp;
				if ((newval = oldval + rgbptr->g) < oldval)
					*outtemp++ = 255;
				else
					*outtemp++ = newval;

				oldval = *outtemp;
				if ((newval = oldval + rgbptr->r) < oldval)
					*outtemp++ = 255;
				else
					*outtemp++ = newval;

				outtemp++;
			}
			else
				outtemp += 4;
		}

		outtemp = outtempsave;
		outtemp += 4*w;
	}
} /* add_8to24bit_buffer */


#ifdef WIN32
static void
attenuate_24bit_buffer(unsigned char *outdata, int x, int y, int w, int h, unsigned char *indata, int ox, int oy, int ow, int oh, lut_triple_t *rgb, int att_percent)
#endif
#ifdef i386
static void
attenuate_24bit_buffer(outdata, x, y, w, h, indata, ox, oy, ow, oh, rgb, att_percent)
unsigned char *outdata;
int x;
int y;
int w;
int h;
unsigned char *indata;
int ox;
int oy;
int ow;
int oh;
lut_triple_t *rgb;
int att_percent;
#endif
{
	unsigned char *intemp, *outtemp, *outtempsave;
	register unsigned char oldval, newval, thisval;
	register lut_triple_t *rgbptr;	/* BP - new!! */
	short i, j;	/* BP - new!! Used to be register. */
	int sig_avg, scaled_att_percent;

	intemp = indata;
	outtemp = outdata + (4*(w*(oy - y) + (ox - x)));
	for (j = 0; j < oh; j++)
	{
		outtempsave = outtemp;

		for (i = 0; i < ow; i++)
		{
			thisval = *intemp++;

			if (thisval)
			{
/* Get existing value from image. */
				oldval = *outtemp;

/* Keep pointer to RGB for speed...? */
				rgbptr = &rgb[thisval];		/* BP - new!! */

/* Attenuate existing value. */

/* BP - new faster version with rgb ptr. */

				if ((rgbptr->r >= rgbptr->g) && (rgbptr->r >= rgbptr->b))
					sig_avg = rgbptr->r;
				if ((rgbptr->g >= rgbptr->r) && (rgbptr->g >= rgbptr->b))
					sig_avg = rgbptr->g;
				if ((rgbptr->b >= rgbptr->r) && (rgbptr->b >= rgbptr->g))
					sig_avg = rgbptr->b;

				//if (sig_avg >= 10) sig_avg = 100;
				scaled_att_percent = sig_avg * att_percent / 255;

				if (att_percent >= 0) {
					newval = oldval - (scaled_att_percent * oldval)/100;
					*outtemp++ = newval;
					oldval = *outtemp;
					newval = oldval - (scaled_att_percent * oldval)/100;
					*outtemp++ = newval;
					oldval = *outtemp;
					newval = oldval - (scaled_att_percent * oldval)/100;
					*outtemp++ = newval;
					outtemp++;
				}
				else {
					*outtemp++ = 1;
					*outtemp++ = 1;
					*outtemp++ = 1;
					outtemp++;
				}
			}
			else
				outtemp += 4;
		}

		outtemp = outtempsave;
		outtemp += 4*w;
	}
} /* attenuate_24bit_buffer */



/******************************************** draw_overlap_mask_buffer */

/*
 * Look for non-zero 32-bit values
 * in the image buffer and use these
 * pixels as a mask.
 * The clear.... function actually
 * sets the top (4th, unused) byte
 * for this purpose. The image buffer
 * must be clear (all zero's) before
 * drawing commences.
 *
 * MODS
 *	SN 30Oct98	RGB components are in 0x00FFFFFFF not 0xFFFFFFF00.
 *
 */

#ifdef WIN32
unsigned char *
draw_overlap_mask_buffer(unsigned *canvas_buffer, int w, int h)
#endif
#ifdef i386
unsigned char *
draw_overlap_mask_buffer(canvas_buffer, w, h)
unsigned *canvas_buffer;
int w;
int h;
#endif
{
#if 0
	unsigned char *mask;
	register unsigned char *masktemp, bit;
	unsigned *buffertemp;
	register short i, j, mw;

	mw = 8*((w + 7) >> 3);

	if ((mask = (unsigned char *)ddgs_get_draw_mask(w, h)) == NULL)
		return(NULL);

	bit = 1;
	masktemp = mask;
	buffertemp = canvas_buffer;
	for (j = 0; j < h; j++)
		for (i = 0; i < mw; i++)
		{
			if (i < w)
				if (*buffertemp++)
					*masktemp |= bit;

			bit = bit << 1;
			if (!bit)
			{
				masktemp++;
				bit = 1;
			}
		}

	return(mask);

#endif
	unsigned char *mask;
	register unsigned char *masktemp;
	DWORD *buffertemp;
	register short i, j, mw;

	/* Scanlines need to be DWORD aligned for dibs */
	mw = 4*((w + 3) >> 2);
	/* Allocate memory from ddgs memory pool, mask is precleared to 0 */
	if ((mask = (unsigned char *)ddgs_get_draw_mask(mw, h)) == NULL)
		return(NULL);
	/*
	** Scan through image and map any 24 bit colour values to 8 bit black
	** values in the mask. Any 24 bit black values is mapped to white.
	** Note the canvas_buffer data is in 32 bits.
	*/
	masktemp = mask;
	buffertemp = (DWORD *)canvas_buffer;
	for (j = 0; j < h; j++)
		for (i = 0; i < mw; i++)
		{
			if (i < w)
			{
				//if (*buffertemp & 0xFFFFFF00)/* Any non black value in 1st 24 bits */
				if (*buffertemp & 0x00FFFFFF)  // SN30Oct98
					*masktemp = 0;
				else
					*masktemp = 255;
				
				buffertemp++; /* Incremements by DWORD */
			}
			masktemp++;
		}

	return(mask);

} /* draw_overlap_mask_buffer */


/******************************************** overlap_has_probes */

/*
 * Check if the supplied overlap has any
 * probe sub-canvases (as opposed to stain).
 *
 * BP - no longer check for thiscan->type, so
 * we can use it on any type!
 */

#ifdef WIN32
static int
overlap_has_probes(Canvas *thiscan)
#endif
#ifdef i386
static int
overlap_has_probes(thiscan)
Canvas *thiscan;
#endif
{
	Canvas *subcan/*, *overl, ap*/;

/* Check if there are probes here, and if
 * so return straight away. */
	subcan = thiscan->patch;
	while (subcan)
	{
		if ((subcan->type == Cprobe) ||
			(subcan->type == Cuprobe))
			return(1);

		subcan = subcan->next;
	}

	return(0);
} /* overlap_has_probes */




/******************************************** create_intersect_idom */

/*
 * Create a new idom which consists of the
 * parts of the supplied idom that intersect
 * the bounding rectangle.
 *
 * Return 1 if successful - ie if the object
 * DID intersect, and there are intervals in
 * the new idom. Return 0 if the idom does
 * not actually intersect.
 *
 * A static idom structure is used (to
 * avoid mallocs). The only part of this
 * system which uses malloc is the actual
 * intervals. These are allocated (or reallocated)
 * as needed, but never freed.
 */


/* Allow for objects of 576 lines, plus some
 * more for safe keeping (albeit unnecessary). */
// Whoa, not anymore, we can have very large images
// For now define a much larger number - really should be dynamically allocated
//#define	MAXNINTLINES	580
#define	MAXNINTLINES	2048


static int initialisedtempidom = 0;
static struct intervalline temp_lines[MAXNINTLINES];

/* Keep track of how many intervals have
 * been allocated on each line, so we can
 * re-use without re-mallocing. */
static int nintsallocated[MAXNINTLINES];

static struct intervaldomain temp_idom = {
	1,		/* type */
	1,		/* linkcount */
	NULL,	/* freeptr */
	0, 0, 0, 0,	/* line1, lastln, kol1, lastkl */
	NULL 	 /* interval lines */
};


#ifdef WIN32
static int
create_intersect_idom(struct intervaldomain *idom, short minln, short minkl, short maxln, short maxkl)
#endif
#ifdef i386
static int
create_intersect_idom(idom, minln, minkl, maxln, maxkl)
struct intervaldomain *idom;
short minln;
short minkl;
short maxln;
short maxkl;
#endif
{
	short does_intersect = 0;
	short i, lineindex, thisline;
	register short idom_kol1, startkl, endkl;
	register struct intervalline *srclines, *dstlines;
	struct interval *srcintval, *dstintval;

/* If this is the first call, initialise
 * the array of interval lines to empty.
 * This allows a subsequent "reallocate-when-
 * necessary" scheme to be used. The
 * interval lines will never be freed,
 * hence the use of malloc etc with little
 * 'm'. */
	if (!initialisedtempidom)
	{
		initialisedtempidom = 1;

/* Initially, space for no intervals has
 * been allocated. */
		for (lineindex = 0; lineindex < MAXNINTLINES; lineindex++)
			nintsallocated[lineindex] = 0;

		temp_idom.intvlines = &temp_lines[0];
	}

/* First establish the bounds of the new idom. */
	if (idom->line1 < minln)
		temp_idom.line1 = minln;
	else
		temp_idom.line1 = idom->line1;

	if (idom->lastln > maxln)
		temp_idom.lastln = maxln;
	else
		temp_idom.lastln = idom->lastln;

	if (idom->kol1 < minkl)
		temp_idom.kol1 = minkl;
	else
		temp_idom.kol1 = idom->kol1;

	if (idom->lastkl > maxkl)
		temp_idom.lastkl = maxkl;
	else
		temp_idom.lastkl = idom->lastkl;

	idom_kol1 = idom->kol1;

/* Start pointing at the first relevant line
 * in the source idom. */
	thisline = idom->line1;
	srclines = idom->intvlines;
	while (thisline < temp_idom.line1)
	{
		thisline++;
		srclines++;
	}

/* Start pointing to the first actual line
 * in the destination idom. */
	dstlines = temp_idom.intvlines;
	lineindex = 0;

/* Scan through each line in the resulting
 * interval domain. */
	while (thisline++ <= temp_idom.lastln)
	{
/* No intervals yet. */
		dstlines->nintvs = 0;

/* Look at all the source intervals, to see
 * if any intersect. */
		srcintval = srclines->intvs;
		for (i = 0; i < srclines->nintvs; i++, srcintval++)
		{
			startkl = srcintval->ileft  + idom_kol1;
			endkl = srcintval->iright + idom_kol1;

/* If this interval is out of bounds, skip it. */
			if ((endkl < minkl) || (startkl > maxkl))
				continue;

/* See if we need to allocate more space for
 * this new interval. */
			if (++dstlines->nintvs > nintsallocated[lineindex])
			{
				nintsallocated[lineindex]++;

				if (nintsallocated[lineindex] == 1)
					dstlines->intvs =
						(struct interval *)malloc(sizeof(struct interval));
				else
					dstlines->intvs =
						(struct interval *)realloc(dstlines->intvs,
						nintsallocated[lineindex]*sizeof(struct interval));
			}

			dstintval = &dstlines->intvs[dstlines->nintvs - 1];

/* Add the interval to our new idom.
 * Clip it to the width of the window
 * if necessary. */
			if (dstintval) {
				if (startkl < minkl)
					dstintval->ileft = 0;
				else
					dstintval->ileft = startkl - temp_idom.kol1;
				
				if (endkl > maxkl)
					dstintval->iright = maxkl - temp_idom.kol1;
				else
					dstintval->iright = endkl - temp_idom.kol1;
			}

			does_intersect = 1;
		}

/* Go to next line in both source and output. */
		lineindex++;
		srclines++;
		dstlines++;
	}

	return(does_intersect);
} /* create_intersect_idom */


/******************************************** set_invert_canvas_BG_col */

BOOL set_invert_canvas_BG_col(Canvas *maincan, unsigned int col)
{
	unsigned char r, g, b;

	b = (col & 0x00ff0000)>>16;
	g = (col & 0x0000ff00)>>8;
	r = (col & 0x000000ff)>>0;

	r = r>>3;
	g = g>>3;
	b = b>>3;

	if ((r<1)&&(g<1)&&(b<1))
		maincan->frame.colour = (0x01<<10 | 0x01<<5 | 0x01);
	else
		maincan->frame.colour = (r<<10 | g<<5 | b);

	return TRUE;

}


/******************************************** get_invert_canvas_BG_col */

unsigned int get_canvas_BG_col(Canvas *maincan)
{
	unsigned char r, g, b;

	r = (unsigned char)((maincan->frame.colour & (0x1F<<10)) >> 10);
	g = (unsigned char)((maincan->frame.colour & (0x1F<<5) ) >> 5);
	b = (unsigned char)((maincan->frame.colour & (0x1F<<0) ) >> 0);
	r = r<<3;
	g = g<<3;
	b = b<<3;
	return (r<<16 | g<<8 | b);

}

/******************************************** set24bitBG */

BOOL set24bitBG(unsigned char *vdata, int w, int h,unsigned int col)
{
	DWORD *temp_vdata;
	int i,j;

	if (vdata == NULL) return FALSE;
	temp_vdata = (unsigned long*)vdata;
	for(j=0 ; j < h ; j++)
		for (i=0 ; i < w ; i++)
			*temp_vdata++ = col;
	return TRUE;
}


/******************************************** mask_8bit_buffer */

BOOL mask_8bit_buffer(unsigned char *vdata, unsigned char *mdata, int w, int h)
{
	int i, j,/* k,*/ b, mw/*, masked*/;
	unsigned char *temp_vdata, *temp_mdata;


	if (vdata==NULL || mdata==NULL) return FALSE;
	mw = ((w+7)/8);
	temp_mdata = mdata;
	temp_vdata = vdata;
	for (j=0 ; j<h ; j++) {
		for (i=0 ; i<mw ; i++) {
			for (b=0 ; b<8 ; b++) {
				if ((i*8 + b) < w) {
					if (((*temp_mdata) & (1<<b))>0) {
						if (*temp_vdata <= 1) *temp_vdata = 1; /* could make these 5 but I think its to do with low values here giving 0's from the lut! */
					}
					temp_vdata++;
				}
			}
			temp_mdata++;
		}
	}
	return TRUE;
}


/******************************************** draw_overlap_Canvas_buffer */
/*
 * Draw a probe canvas into a 24-bit buffer.
 * This works on overlaps - the children of
 * the overlap are drawn into the buffer.
 */

#define MAXWIDTH    3074
#define MAXHEIGHT   3074

#ifdef WIN32
unsigned *
draw_overlap_Canvas_buffer(Canvas *thiscan, DDGS *dg, int scale, int *x, int *y, int *w, int *h)
#endif
#ifdef i386
unsigned *
draw_overlap_Canvas_buffer(thiscan, dg, scale, x, y, w, h)
Canvas *thiscan;
DDGS *dg;
int scale;
int *x;
int *y;
int *w;
int *h;
#endif
{
	unsigned *buffer;
	int /*dx, dy, */ox, oy, ow, oh;
	int oldxo = 100000, oldyo = 100000, oldx1 = -1, oldy1 = -1;
	int xo, yo, x1, y1, bigheight;
	int Ive_got_probes/*, He_overlaps_me*/;
	Canvas *overlapcan, *subcan/*, *subsubcan*/;
	unsigned char *temp_buffer;
	lut_triple_t *rgb, counterstain_lut[256], non_vis_signal_lut[256], non_vis_counterstain_lut[256];
	struct pframe *f;
	struct intervaldomain *keep_idom;
	int xoff, yoff;
	int overlapcan_visible=0, subcan_visible=0, i;
	unsigned char *mdata;
	BOOL counterstain_hidden=TRUE;
	unsigned int col ;

	ddgsGetInvertBGColour(&col);
	for (i=0 ; i<256 ; i++) {
		non_vis_counterstain_lut[i].r=(col & 0x00ff0000)>>16;
		non_vis_counterstain_lut[i].g=(col & 0x0000ff00)>>8;
		non_vis_counterstain_lut[i].b=(col & 0x000000ff);
		non_vis_signal_lut[i].r=0x00;
		non_vis_signal_lut[i].g=0x00;
		non_vis_signal_lut[i].b=0x00;
	}
	*x = *y = *w = *h = 0;

/* Do nothing if there's no canvas. */
	if (thiscan->type != Coverlap)
		return(NULL);

	/* To find the bottom-left corner of
	 * the area covered by objects, initially
	 * assume there are none! */
	xoff = thiscan->frame.width;
	yoff = thiscan->frame.height;

/* Find the limits of all the Wobjs. */
	subcan = thiscan->patch;
	while (subcan)
	{
		if ((subcan->type == Cprobe) ||
			(subcan->type == Cuprobe) ||
			(subcan->type == Ccounterstain))
		{
			/* Looking for the bottom-left corner
			 * of objects within the overlap. */
			if (subcan->frame.cpframe.dx < xoff)
				xoff = subcan->frame.cpframe.dx;
			if (subcan->frame.cpframe.dy < yoff)
				yoff = subcan->frame.cpframe.dy;

			if (subcan->Wobj->idom->kol1 < oldxo)
				oldxo = subcan->Wobj->idom->kol1;
			if (subcan->Wobj->idom->lastkl > oldx1)
				oldx1 = subcan->Wobj->idom->lastkl;
			if (subcan->Wobj->idom->line1 < oldyo)
				oldyo = subcan->Wobj->idom->line1;
			if (subcan->Wobj->idom->lastln > oldy1)
				oldy1 = subcan->Wobj->idom->lastln;
		}

		subcan = subcan->next;
	}

	xo = oldxo;
	yo = oldyo;
	x1 = oldx1;
	y1 = oldy1;

	if ((oldx1 < 0) || (oldy1 < 0))
		return(NULL);

	*w = (scale*(oldx1 - oldxo + 1)) >> 3;
	*h = (scale*(oldy1 - oldyo + 1)) >> 3;

	bigheight = dg->maxy - dg->miny;

	f = Canvas_Wobj_pframe(thiscan);
	/* Calculate position of bottom-left of
	 * actual objects within the overlap.
	 * This used to be #define'd - but this
	 * code allows it to be whatever it is! */
	xoff >>= 3;
	yoff >>= 3;
	*x = (f->dx + scale*xoff) >> 3;
	*y = (f->dy + scale*yoff) >> 3;

	// Correct y position (+ 1) to match frame (avoid black line).
	*y = bigheight - *y - *h + 1;
	Free(f);

/* Clip the buffer so that we don't allocate
 * more than we could possibly need! */
	if (*x < 0)
	{
		*w += *x;
		*x = 0;
	}

	if (*y < 0)
	{
		*h += *y;
		*y = 0;
	}

	if (*w > MAXWIDTH)
		*w = MAXWIDTH;

	if (*h > MAXHEIGHT)
		*h = MAXHEIGHT;

	if ((*w < 0) || (*h < 1))
		return(NULL);

/* Do nothing if the frame is outside the limits
 * of the dog. This is a new check - can't explain
 * why it wasn't always there! (something to do
 * with Arthur Negus, perhaps?). */
	if ((*x + *w < 0) || (*y + *h < 0) ||
		(*x > dg->maxx) || (*y > dg->maxy))
		return(NULL);

/* Allocate the required amount of ddgs buffer. */
	if ((buffer = (unsigned *)ddgs_get_draw_buffer(4*(*w)*(*h))) == NULL)
		return(NULL);

	set24bitBG((unsigned char *)buffer, *w, *h, col);

/* If this canvas is attached to something,
 * prepare for a pass through ALL relevant
 * canvases (see notes below). */
	if (thiscan->parent)
		overlapcan = thiscan->parent->patch;
	else
		overlapcan = thiscan;

/* Special case: if THIS canvas has probes we
 * need to refresh ALL overlapping overlaps,
 * else our probes might make other stains
 * disappear! Got that? */
	Ive_got_probes = overlap_has_probes(thiscan);

/*
 * Redraw the COUNTERSTAIN part of the draw
 * buffer.
 */

/* Scan through ALL overlaps attached to this
 * image. This is necessary to avoid disappearing
 * probes when a probe covers more than one stain
 * object, but is (of course) only attached to one
 * of them. If this overlap has any probes, or any
 * other overlaps with probes physically overlap
 * this ones bounding rectange, then refresh add
 * the "other" canvas's objects into the draw buffer. */


	while (overlapcan)
	{
		overlapcan_visible = overlapcan->visible;
		if (overlapcan->type != Coverlap)
		{
			overlapcan = overlapcan->next;
			continue;
		}

		subcan = overlapcan->patch;
		while (subcan)
		{
			subcan_visible = subcan->visible;
			if (subcan->type != Ccounterstain)
			{
				subcan = subcan->next;
				continue;
			}

			keep_idom = NULL;

/* BP - do these extra checks if we are looking
 * at ANOTHER counterstain object. Only want to
 * bother with it if its interval domain is co-
 * incident with any of the probes attached to
 * THIS object. */
			if (overlapcan != thiscan)
			{
/* Firstly check if its not even within the
 * bounding rectangle of this object. */
				if ((subcan->Wobj->idom->kol1 > x1) ||
					(subcan->Wobj->idom->lastkl < xo) ||
					(subcan->Wobj->idom->line1 > y1) ||
					(subcan->Wobj->idom->lastln < yo))
				{
					subcan = subcan->next;
					continue;
				}

/* Now see if the object's idom really
 * intersects with the area. If so, assign
 * the intersecting idom to the object. */
				if (create_intersect_idom(subcan->Wobj->idom, yo, xo, y1, x1))
				{
					keep_idom = subcan->Wobj->idom;
					subcan->Wobj->idom = &temp_idom;
				}
				else
				{
					subcan = subcan->next;
					continue;
				}
			}

			f = Canvas_Wobj_pframe(subcan);
			temp_buffer = (unsigned char *)qbuffer(subcan->Wobj,
					&ox, &oy, &ow, &oh, f, &mdata);
			Free(f);
			mask_8bit_buffer(temp_buffer, mdata, ow, oh);

/* If the object's idom was tweaked for
 * intersection purposes, put the old
 * one back. */
			if (keep_idom)
				subcan->Wobj->idom = keep_idom;

			if (temp_buffer)
			{
				oy = bigheight - oy;

				if (clip_8bit_buffer(temp_buffer,
						&ox, &oy, &ow, &oh, *x, *y, *w, *h))
				{
					rgb = ((componentdata_t *)subcan->data)->lut;
					if (rgb == NULL)
						rgb = &((flex_componentdata_t *)subcan->data)
												->lutdata[0];

					for (i=0 ; i<256 ; i++) {
						if(rgb[i].r==0) counterstain_lut[i].r=1;
						else counterstain_lut[i].r=rgb[i].r;
						if(rgb[i].g==0) counterstain_lut[i].g=1;
						else counterstain_lut[i].g=rgb[i].g;
						if(rgb[i].b==0) counterstain_lut[i].b=1;
						else counterstain_lut[i].b=rgb[i].b;
					}

					rgb = counterstain_lut;
					if (overlapcan_visible == 0)
						rgb = non_vis_counterstain_lut;
					if (subcan_visible == 0) {
						rgb = non_vis_counterstain_lut;
						counterstain_hidden=TRUE;
					}
					else counterstain_hidden=FALSE;

					if (dg->cur_intensity)
						copy_8to24bit_buffer((unsigned char *)buffer, *x, *y, *w, *h,
									temp_buffer, ox, oy, ow, oh, rgb);
					else
						clear_8to24bit_buffer(buffer, *x, *y, *w, *h,
									temp_buffer, ox, oy, ow, oh);
				}
			}
			subcan = subcan->next;
		}

		overlapcan = overlapcan->next;
	}

/*
 * Attenuate the parts of the draw buffer (i.e. the counterstain) which has probe signals.
 */

	if (thiscan->parent)
		overlapcan = thiscan->parent->patch;
	else
		overlapcan = thiscan;

	while (overlapcan)
	{
		overlapcan_visible = overlapcan->visible;
		if ((overlapcan->type != Coverlap)
		|| (!overlapcan->visible))		/* MG 15Aug98 */
		{
			overlapcan = overlapcan->next;
			continue;
		}

		if ((Ive_got_probes == 0) && (overlapcan != thiscan))
			if (overlap_has_probes(overlapcan) == 0)
			{
				overlapcan = overlapcan->next;
				continue;
			}

		subcan = overlapcan->patch;
		while (subcan)
		{
			subcan_visible = subcan->visible;
			if (((subcan->type != Cprobe) && (subcan->type != Cuprobe))
				||(!subcan->visible))
			{
				subcan = subcan->next;
				continue;
			}


			keep_idom = NULL;

/* BP - do these extra checks if we are looking
 * at ANOTHER probe object. Only want to
 * bother with it if its interval domain is co-
 * incident with the counterstain attached to
 * THIS object. */

			if (overlapcan != thiscan)
			{

/* Firstly check if its not even within the
 * bounding rectangle of this object. */
				if ((subcan->Wobj->idom->kol1 > x1) ||
					(subcan->Wobj->idom->lastkl < xo) ||
					(subcan->Wobj->idom->line1 > y1) ||
					(subcan->Wobj->idom->lastln < yo))
				{
					subcan = subcan->next;
					continue;
				}

				if (create_intersect_idom(subcan->Wobj->idom, yo, xo, y1, x1))
				{
					keep_idom = subcan->Wobj->idom;
					subcan->Wobj->idom = &temp_idom;
				}
				else
				{
					subcan = subcan->next;
					continue;
				}
			}

			f = Canvas_Wobj_pframe(subcan);
			temp_buffer = (unsigned char *)qbuffer(subcan->Wobj,
					&ox, &oy, &ow, &oh, f, NULL);
			Free(f);

			if (keep_idom)
				subcan->Wobj->idom = keep_idom;

			if (temp_buffer)
			{
				oy = bigheight - oy;

				if (clip_8bit_buffer(temp_buffer, &ox, &oy, &ow, &oh,
								*x, *y, *w, *h))
				{
					rgb = ((componentdata_t *)subcan->data)->lut;
					if (overlapcan_visible == 0)
						rgb=non_vis_signal_lut;
					if (subcan_visible == 0)
						rgb=non_vis_signal_lut;

					if (rgb == NULL)
						rgb = &((flex_componentdata_t *)subcan->data)
												->lutdata[0];

					if (dg->cur_intensity) {
						if ((overlapcan_visible==1)||(subcan_visible==1)) {
							if (!counterstain_hidden)
								attenuate_24bit_buffer((unsigned char *)buffer, *x, *y, *w, *h,
								temp_buffer, ox, oy, ow, oh, rgb, att_factors[((ComponentData *)subcan->data)->id]);
							else
								attenuate_24bit_buffer((unsigned char *)buffer, *x, *y, *w, *h,
									temp_buffer, ox, oy, ow, oh, rgb, -1);

						}

					}
					else
						clear_8to24bit_buffer(buffer, *x, *y, *w, *h,
									temp_buffer, ox, oy, ow, oh);
				}
			}
			subcan = subcan->next;
		}
		overlapcan = overlapcan->next;
	}

/*
 * Redraw the PROBE part of the draw buffer.
 *
 * This is basically identical to the stain
 * loop abaove, but with add_8to24bit_buffer
 * instead of copy_8to24bit_buffer.
 */


	if (thiscan->parent)
		overlapcan = thiscan->parent->patch;
	else
		overlapcan = thiscan;

	while (overlapcan)
	{
		overlapcan_visible = overlapcan->visible;
		if (overlapcan->type != Coverlap)
		{
			overlapcan = overlapcan->next;
			continue;
		}

		if ((Ive_got_probes == 0) && (overlapcan != thiscan))
			if (overlap_has_probes(overlapcan) == 0)
			{
				overlapcan = overlapcan->next;
				continue;
			}

		subcan = overlapcan->patch;
		while (subcan)
		{
			subcan_visible = subcan->visible;
			if ((subcan->type != Cprobe) && (subcan->type != Cuprobe))
			{
				subcan = subcan->next;
				continue;
			}


			keep_idom = NULL;

/* BP - do these extra checks if we are looking
 * at ANOTHER probe object. Only want to
 * bother with it if its interval domain is co-
 * incident with the counterstain attached to
 * THIS object. */
			if (overlapcan != thiscan)
			{
/* Firstly check if its not even within the
 * bounding rectangle of this object. */
				if ((subcan->Wobj->idom->kol1 > x1) ||
					(subcan->Wobj->idom->lastkl < xo) ||
					(subcan->Wobj->idom->line1 > y1) ||
					(subcan->Wobj->idom->lastln < yo))
				{
					subcan = subcan->next;
					continue;
				}

				if (create_intersect_idom(subcan->Wobj->idom, yo, xo, y1, x1))
				{
					keep_idom = subcan->Wobj->idom;
					subcan->Wobj->idom = &temp_idom;
				}
				else
				{
					subcan = subcan->next;
					continue;
				}
			}

			f = Canvas_Wobj_pframe(subcan);
			temp_buffer = (unsigned char *)qbuffer(subcan->Wobj,
					&ox, &oy, &ow, &oh, f, NULL);
			Free(f);

			if (keep_idom)
				subcan->Wobj->idom = keep_idom;

			if (temp_buffer)
			{
				oy = bigheight - oy;

				if (clip_8bit_buffer(temp_buffer, &ox, &oy, &ow, &oh,
								*x, *y, *w, *h))
				{
					rgb = ((componentdata_t *)subcan->data)->lut;
					if (overlapcan_visible == 0)
						rgb=non_vis_signal_lut;
					if (subcan_visible == 0)
						rgb=non_vis_signal_lut;

					if (rgb == NULL)
						rgb = &((flex_componentdata_t *)subcan->data)
												->lutdata[0];

					if (dg->cur_intensity)
						add_8to24bit_buffer((unsigned char *)buffer, *x, *y, *w, *h,
									temp_buffer, ox, oy, ow, oh, rgb);
				}
			}

			subcan = subcan->next;
		}

		overlapcan = overlapcan->next;
	}

	return(buffer);
} /* draw_overlap_Canvas_buffer */




/******************************************** draw_fetal_Canvas_buffer */

/*
 * Draw a fetal canvas into a 24-bit buffer.
 * This works on "Cfetal"s - the probe children
 * of the Cfetal are drawn into the buffer.
 *
 * This is just a simplified version of
 * draw_overlap_Canvas_buffer(). It differs in
 * that:
 *	(a) there are no Ccounterstain objects,
 *	(b) there is no checking of overlaps with
 *		other canvases (it is assumed they
 *		do not overlap).
 * Note that Cfetals are also drawn without a
 * clip mask (it is assumed they are rectangular).
 * See also display_Wobj() in canvas.c.
 */

#ifdef WIN32
unsigned *
draw_fetal_Canvas_buffer(Canvas *thiscan, DDGS *dg, int scale, int *x, int *y, int *w, int *h)
#endif
#ifdef i386
unsigned *
draw_fetal_Canvas_buffer(thiscan, dg, scale, x, y, w, h)
Canvas *thiscan;
DDGS *dg;
int scale;
int *x;
int *y;
int *w;
int *h;
#endif
{
	unsigned *buffer;
	int /*dx, dy, */ox, oy, ow, oh, first_component = 1;
	int oldxo = 100000, oldyo = 100000, oldx1 = -1, oldy1 = -1;
	int xo, yo, x1, y1, bigheight, bright_offset = 0;
	Canvas *subcan;
	unsigned char *temp_buffer;
	lut_triple_t *rgb;
	struct pframe *f;

	*x = *y = *w = *h = 0;

/* Do nothing if the canvas is of the
 * wrong type, or if it has no probes. */
	if ((thiscan->type != Cfetal) ||
		(overlap_has_probes(thiscan) == 0))
		return(NULL);

/* Find the limits of all the Wobjs. */
	subcan = thiscan->patch;
	while (subcan)
	{
/* Need to find the extent of the probe
 * components, and we are only interested
 * in those that are visible. */
		if ((subcan->type == Cprobe) && subcan->visible)
		{
			if (subcan->Wobj->idom->kol1 < oldxo)
				oldxo = subcan->Wobj->idom->kol1;
			if (subcan->Wobj->idom->lastkl > oldx1)
				oldx1 = subcan->Wobj->idom->lastkl;
			if (subcan->Wobj->idom->line1 < oldyo)
				oldyo = subcan->Wobj->idom->line1;
			if (subcan->Wobj->idom->lastln > oldy1)
				oldy1 = subcan->Wobj->idom->lastln;
		}
		else
/* If there are visible brightfield
 * children, we need to offset this
 * probe image appropriately. */
			if (subcan->visible)
				bright_offset += 8 + subcan->frame.width;

		subcan = subcan->next;
	}

/* Abort if there are no probe components
 * (or if they are not visible). */
	if ((oldx1 < 0) || (oldy1 < 0))
		return(NULL);

	xo = oldxo;
	yo = oldyo;
	x1 = oldx1;
	y1 = oldy1;

	*w = (scale*(oldx1 - oldxo + 1)) >> 3;
	*h = (scale*(oldy1 - oldyo + 1)) >> 3;

	bigheight = dg->maxy - dg->miny;

	f = Canvas_Wobj_pframe(thiscan);
	*x = (f->dx + ((f->scale*bright_offset)/8)) >> 3;
	*y = f->dy >> 3;
	*y = bigheight - *y - *h;
	Free(f);

/* Clip the buffer so that we don't allocate
 * more than we could possibly need! */
	if (*x < 0)
	{
		*w += *x;
		*x = 0;
	}

	if (*y < 0)
	{
		*h += *y;
		*y = 0;
	}

	if (*w > MAXWIDTH)
		*w = MAXWIDTH;

	if (*h > MAXHEIGHT)
		*h = MAXHEIGHT;

	if ((*w < 0) || (*h < 1))
		return(NULL);

/* Do nothing if the frame is outside the limits
 * of the dog. This is a new check - can't explain
 * why it wasn't always there! (something to do
 * with Arthur Negus, perhaps?). */
	if ((*x + *w < 0) || (*y + *h < 0) ||
		(*x > dg->maxx) || (*y > dg->maxy))
		return(NULL);

/* Allocate the required amount of ddgs buffer. */
	if ((buffer = (unsigned *)ddgs_get_draw_buffer(4*(*w)*(*h))) == NULL)
		return(NULL);

/*
 * Add the probe objects into the buffer.
 */
	subcan = thiscan->patch;
	while (subcan)
	{
		if ((subcan->type != Cprobe) || (!subcan->visible))
		{
			subcan = subcan->next;
			continue;
		}

		f = Canvas_Wobj_pframe(subcan);
		temp_buffer = (unsigned char *)qbuffer(subcan->Wobj,
				&ox, &oy, &ow, &oh, f, NULL);
		Free(f);

		if (temp_buffer)
		{
			oy = bigheight - oy;

			if (clip_8bit_buffer(temp_buffer, &ox, &oy, &ow, &oh,
							*x, *y, *w, *h))
				if ((rgb = ((componentdata_t *)subcan->data)->lut) != NULL)
				{
/* Just copy the first one (quicker). */
					if (first_component)
						copy_8to24bit_buffer((unsigned char *)buffer, *x, *y, *w, *h,
									temp_buffer, ox, oy, ow, oh, rgb);
					else
						add_8to24bit_buffer((unsigned char *)buffer, *x, *y, *w, *h,
									temp_buffer, ox, oy, ow, oh, rgb);
					first_component = 0;
				}
		}

		subcan = subcan->next;
	}

	return(buffer);
} /* draw_fetal_Canvas_buffer */


void setCanvasCounterstainAttenuation(int id, int attenuate_cs)
{
	att_factors[id] = attenuate_cs;
}

#if 0


/******************************************** draw_grey_Canvas_buffer */

/*
 * Draw a grey-scale canvas into a buffer.
 * This works on canvases of types:
 *	Cmetblob, CChromosome
 *
 * This is used because its not convenient to
 * use clip masks in windows. Its more efficient
 * to expose all overlapping chroms into a
 * rectangle and blit the rectangle directly.
 * This was already done in earlier versions of
 * CV (3.0 and previous) for probe/fetal images
 * because it was needed to account for the
 * additive nature of probe images.
 */

char *
draw_grey_Canvas_buffer(Canvas *thiscan, DDGS *dg, int scale, int *x, int *y, int *w, int *h)
{
	char *buffer;
	int dx, dy, ox, oy, ow, oh;
	int xo, yo, x1, y1, bigheight;
	Canvas *thatcan;
	lut_triple_t *rgb;
	unsigned char *temp_buffer;
	struct pframe *f;

	if (!made_grey_luts)
	{
		int i;

		for (i = 0; i < 256; i++)
		{
			greylut[i].r = greylut[i].g = greylut[i].b = 255 - i;
			igreylut[i].r = igreylut[i].g = igreylut[i].b = i;
		}

		made_grey_luts = 1;
	}

	*x = *y = *w = *h = 0;

	// If theres no Wobj, its not visible, or
	// its not a type 1 object, do nothing.
	if ((thiscan->Wobj == NULL) ||
		(thiscan->visible == 0) ||
		(thiscan->Wobj->type != 1))
		return(NULL);

	// Get the limits of the Wobj, so we can
	// detect others that coincide with its
	// bounding rectangle.
	xo = thiscan->Wobj->idom->kol1;
	x1 = thiscan->Wobj->idom->lastkl;
	yo = thiscan->Wobj->idom->line1;
	y1 = thiscan->Wobj->idom->lastln;

	*w = (scale*(x1 - xo + 1)) >> 3;
	*h = (scale*(y1 - yo + 1)) >> 3;

	bigheight = dg->maxy - dg->miny;

	f = Canvas_Wobj_pframe(thiscan);
//	*x = (f->dx + ((f->scale*bright_offset)/8)) >> 3;
	*y = f->dy >> 3;
	*y = bigheight - *y - *h;
	Free(f);

	/* Clip the buffer so that we don't allocate
	 * more than we could possibly need! */
	if (*x < 0)
	{
		*w += *x;
		*x = 0;
	}

	if (*y < 0)
	{
		*h += *y;
		*y = 0;
	}

	if (*w > MAXWIDTH)
		*w = MAXWIDTH;

	if (*h > MAXHEIGHT)
		*h = MAXHEIGHT;

	if ((*w < 0) || (*h < 1))
		return(NULL);

	/* Do nothing if the frame is outside the limits
	 * of the dog. This is a new check - can't explain
	 * why it wasn't always there! (something to do
	 * with Arthur Negus, perhaps?). */
	if ((*x + *w < 0) || (*y + *h < 0) ||
		(*x > dg->maxx) || (*y > dg->maxy))
		return(NULL);

	/* Allocate the required amount of ddgs buffer. */
	if ((buffer = (unsigned *)ddgs_get_draw_buffer((*w)*(*h))) == NULL)
		return(NULL);

	// Now find all the other grey objects that
	// coincide with the enclosing rectangle and
	// draw them in.
	// We only do this for immediate siblings of
	// thiscan.
	if (thiscan->parent)
		thatcan = thiscan->parent->patch;
	else
		thatcan = thiscan;

	while (thatcan)
	{
		// Create buffer with this object in it.
		f = Canvas_Wobj_pframe(thiscan);
		temp_buffer = (unsigned char *)qbuffer(thiscan->Wobj,
				&ox, &oy, &ow, &oh, f, NULL);
		Free(f);

		if (clip_8bit_buffer(temp_buffer, &ox, &oy, &ow, &oh,
						*x, *y, *w, *h))
		{
		}

		// Check that the object is real.
		if ((thiscan->Wobj == NULL) ||
			(thiscan->visible == 0) ||
			(thiscan->Wobj->type != 1) ||
			(thatcan->type != Cmetblob))
		{
			thatcan = thatcan->next;
			continue;
		}

		// Check that the object intersects.
		if ((thatcan->Wobj->idom->kol1 > x1) ||
			(thatcan->Wobj->idom->lastkl < xo) ||
			(thatcan->Wobj->idom->line1 > y1) ||
			(thatcan->Wobj->idom->lastln < yo))
		{
			thatcan = thatcan->next;
			continue;
		}

		f = Canvas_Wobj_pframe(thatcan);
		temp_buffer = (unsigned char *)qbuffer(thatcan->Wobj,
				&ox, &oy, &ow, &oh, f, NULL);
		Free(f);

		if (temp_buffer)
		{
			oy = bigheight - oy;

			if (clip_8bit_buffer(temp_buffer,
					&ox, &oy, &ow, &oh, *x, *y, *w, *h))
				copy_8to24bit_buffer(buffer, *x, *y, *w, *h,
						temp_buffer, ox, oy, ow, oh, rgb);
		}

		thatcan = thatcan->next;
	}

	return(buffer);
} /* draw_grey_Canvas_buffer */


/******************** draw_Canvas_buffer */

/*
 * Draw a canvas into a 24-bit buffer.
 *
 * All object types are included, in the
 * following order:
 *	counterstain, probe, greyscale
 * This is because the probes must be
 * added to the counterstain.
 *
 * The function builds a complete sub-
 * image for the rectangle enclosing the
 * supplied canvas' object.
 * This can then be written to the display
 * without clip masks etc, and on any
 * display depth using standard Windows calls.
 *
 * Derived from draw_overlap_Canvas_buffer(),
 * with the addition of greyscale objects and
 * reduction of overlap checking to the
 * enclosing rectangle only.
 */

#define MAXWIDTH    1152
#define MAXHEIGHT   864

unsigned *
draw_Canvas_buffer(Canvas *thiscan, DDGS *dg, int scale,
				   int *x, int *y, int *w, int *h)
{
	unsigned *buffer;
	int dx, dy, ox, oy, ow, oh;
	int oldxo = 100000, oldyo = 100000, oldx1 = -1, oldy1 = -1;
	int xo, yo, x1, y1, bigheight;
	int Ive_got_probes, He_overlaps_me;
	Canvas *overlapcan, *subcan, *subsubcan;
	unsigned char *temp_buffer;
	lut_triple_t *rgb;
	struct pframe *f;
	struct intervaldomain *keep_idom;
	int xoff, yoff;


	*x = *y = *w = *h = 0;

/* Do nothing if there's no canvas. */
	if (thiscan->type != Coverlap)
		return(NULL);

	/* To find the bottom-left corner of
	 * the area covered by objects, initially
	 * assume there are none! */
	xoff = thiscan->frame.width;
	yoff = thiscan->frame.height;

/* Find the limits of all the Wobjs. */
	subcan = thiscan->patch;
	while (subcan)
	{
		if ((subcan->type == Cprobe) ||
			(subcan->type == Cuprobe) ||
			(subcan->type == Ccounterstain))
		{
			/* Looking for the bottom-left corner
			 * of objects within the overlap. */
			if (subcan->frame.cpframe.dx < xoff)
				xoff = subcan->frame.cpframe.dx;
			if (subcan->frame.cpframe.dy < yoff)
				yoff = subcan->frame.cpframe.dy;

			if (subcan->Wobj->idom->kol1 < oldxo)
				oldxo = subcan->Wobj->idom->kol1;
			if (subcan->Wobj->idom->lastkl > oldx1)
				oldx1 = subcan->Wobj->idom->lastkl;
			if (subcan->Wobj->idom->line1 < oldyo)
				oldyo = subcan->Wobj->idom->line1;
			if (subcan->Wobj->idom->lastln > oldy1)
				oldy1 = subcan->Wobj->idom->lastln;
		}

		subcan = subcan->next;
	}

	xo = oldxo;
	yo = oldyo;
	x1 = oldx1;
	y1 = oldy1;

	if ((oldx1 < 0) || (oldy1 < 0))
		return(NULL);

	*w = (scale*(oldx1 - oldxo + 1)) >> 3;
	*h = (scale*(oldy1 - oldyo + 1)) >> 3;

	bigheight = dg->maxy - dg->miny;

	f = Canvas_Wobj_pframe(thiscan);
	/* Calculate position of bottom-left of
	 * actual objects within the overlap.
	 * This used to be #define'd - but this
	 * code allows it to be whatever it is! */
	xoff >>= 3;
	yoff >>= 3;
	*x = (f->dx + scale*xoff) >> 3;
	*y = (f->dy + scale*yoff) >> 3;

	*y = bigheight - *y - *h;
	Free(f);

/* Clip the buffer so that we don't allocate
 * more than we could possibly need! */
	if (*x < 0)
	{
		*w += *x;
		*x = 0;
	}

	if (*y < 0)
	{
		*h += *y;
		*y = 0;
	}

	if (*w > MAXWIDTH)
		*w = MAXWIDTH;

	if (*h > MAXHEIGHT)
		*h = MAXHEIGHT;

	if ((*w < 0) || (*h < 1))
		return(NULL);

/* Do nothing if the frame is outside the limits
 * of the dog. This is a new check - can't explain
 * why it wasn't always there! (something to do
 * with Arthur Negus, perhaps?). */
	if ((*x + *w < 0) || (*y + *h < 0) ||
		(*x > dg->maxx) || (*y > dg->maxy))
		return(NULL);

/* Allocate the required amount of ddgs buffer. */
	if ((buffer = (unsigned *)ddgs_get_draw_buffer((*w)*(*h))) == NULL)
		return(NULL);

/* If this canvas is attached to something,
 * prepare for a pass through ALL relevant
 * canvases (see notes below). */
	if (thiscan->parent)
		overlapcan = thiscan->parent->patch;
	else
		overlapcan = thiscan;

/* Special case: if THIS canvas has probes we
 * need to refresh ALL overlapping overlaps,
 * else our probes might make other stains
 * disappear! Got that? */
	Ive_got_probes = overlap_has_probes(thiscan);

/*
 * Redraw the COUNTERSTAIN part of the draw
 * buffer.
 */

/* Scan through ALL overlaps attached to this
 * image. This is necessary to avoid disappearing
 * probes when a probe covers more than one stain
 * object, but is (of course) only attached to one
 * of them. If this overlap has any probes, or any
 * other overlaps with probes physically overlap
 * this ones bounding rectange, then refresh add
 * the "other" canvas's objects into the draw buffer. */

	while (overlapcan)
	{
		if ((overlapcan->type != Coverlap)
		|| (!overlapcan->visible))		/* MG 8Sep98 */
		{
			overlapcan = overlapcan->next;
			continue;
		}

		subcan = overlapcan->patch;
		while (subcan)
		{
			if ((subcan->type != Ccounterstain) ||
				(!subcan->visible))
			{
				subcan = subcan->next;
				continue;
			}

			keep_idom = NULL;

/* BP - do these extra checks if we are looking
 * at ANOTHER counterstain object. Only want to
 * bother with it if its interval domain is co-
 * incident with any of the probes attached to
 * THIS object. */
			if (overlapcan != thiscan)
			{
/* Firstly check if its not even within the
 * bounding rectangle of this object. */
				if ((subcan->Wobj->idom->kol1 > x1) ||
					(subcan->Wobj->idom->lastkl < xo) ||
					(subcan->Wobj->idom->line1 > y1) ||
					(subcan->Wobj->idom->lastln < yo))
				{
					subcan = subcan->next;
					continue;
				}

/* Now see if the object's idom really
 * intersects with the area. If so, assign
 * the intersecting idom to the object. */
				if (create_intersect_idom(subcan->Wobj->idom, yo, xo, y1, x1))
				{
					keep_idom = subcan->Wobj->idom;
					subcan->Wobj->idom = &temp_idom;
				}
				else
				{
					subcan = subcan->next;
					continue;
				}
			}

			f = Canvas_Wobj_pframe(subcan);
			temp_buffer = (unsigned char *)qbuffer(subcan->Wobj,
					&ox, &oy, &ow, &oh, f, NULL);
			Free(f);

/* If the object's idom was tweaked for
 * intersection purposes, put the old
 * one back. */
			if (keep_idom)
				subcan->Wobj->idom = keep_idom;

			if (temp_buffer)
			{
				oy = bigheight - oy;

				if (clip_8bit_buffer(temp_buffer,
						&ox, &oy, &ow, &oh, *x, *y, *w, *h))
				{
					rgb = ((componentdata_t *)subcan->data)->lut;

					if (rgb == NULL)
						rgb = &((flex_componentdata_t *)subcan->data)
												->lutdata[0];

					if (dg->cur_intensity)
						copy_8to24bit_buffer(buffer, *x, *y, *w, *h,
									temp_buffer, ox, oy, ow, oh, rgb);
					else
						clear_8to24bit_buffer(buffer, *x, *y, *w, *h,
									temp_buffer, ox, oy, ow, oh);
				}
			}

			subcan = subcan->next;
		}

		overlapcan = overlapcan->next;
	}


/*
 * Redraw the PROBE part of the draw buffer.
 *
 * This is basically identical to the stain
 * loop abaove, but with add_8to24bit_buffer
 * instead of copy_8to24bit_buffer.
 */

	if (thiscan->parent)
		overlapcan = thiscan->parent->patch;
	else
		overlapcan = thiscan;

	while (overlapcan)
	{
		if ((overlapcan->type != Coverlap)
		|| (!overlapcan->visible))		/* MG 8Sep98 */
		{
			overlapcan = overlapcan->next;
			continue;
		}

		if ((Ive_got_probes == 0) && (overlapcan != thiscan))
			if (overlap_has_probes(overlapcan) == 0)
			{
				overlapcan = overlapcan->next;
				continue;
			}

		subcan = overlapcan->patch;
		while (subcan)
		{
			if (((subcan->type != Cprobe) && (subcan->type != Cuprobe)) ||
				(!subcan->visible))
			{
				subcan = subcan->next;
				continue;
			}


			keep_idom = NULL;

/* BP - do these extra checks if we are looking
 * at ANOTHER probe object. Only want to
 * bother with it if its interval domain is co-
 * incident with the counterstain attached to
 * THIS object. */
			if (overlapcan != thiscan)
			{
/* Firstly check if its not even within the
 * bounding rectangle of this object. */
				if ((subcan->Wobj->idom->kol1 > x1) ||
					(subcan->Wobj->idom->lastkl < xo) ||
					(subcan->Wobj->idom->line1 > y1) ||
					(subcan->Wobj->idom->lastln < yo))
				{
					subcan = subcan->next;
					continue;
				}

				if (create_intersect_idom(subcan->Wobj->idom, yo, xo, y1, x1))
				{
					keep_idom = subcan->Wobj->idom;
					subcan->Wobj->idom = &temp_idom;
				}
				else
				{
					subcan = subcan->next;
					continue;
				}
			}

			f = Canvas_Wobj_pframe(subcan);
			temp_buffer = (unsigned char *)qbuffer(subcan->Wobj,
					&ox, &oy, &ow, &oh, f, NULL);
			Free(f);

			if (keep_idom)
				subcan->Wobj->idom = keep_idom;

			if (temp_buffer)
			{
				oy = bigheight - oy;

				if (clip_8bit_buffer(temp_buffer, &ox, &oy, &ow, &oh,
								*x, *y, *w, *h))
				{
					rgb = ((componentdata_t *)subcan->data)->lut;

					if (rgb == NULL)
						rgb = &((flex_componentdata_t *)subcan->data)
												->lutdata[0];

					if (dg->cur_intensity)
						add_8to24bit_buffer(buffer, *x, *y, *w, *h,
									temp_buffer, ox, oy, ow, oh, rgb);
					else
						clear_8to24bit_buffer(buffer, *x, *y, *w, *h,
									temp_buffer, ox, oy, ow, oh);
				}
			}

			subcan = subcan->next;
		}

		overlapcan = overlapcan->next;
	}

	return(buffer);
} /* draw_Canvas_buffer */


#endif
