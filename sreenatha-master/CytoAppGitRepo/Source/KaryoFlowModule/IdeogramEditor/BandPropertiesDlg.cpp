// BandPropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IdeogramEditor.h"
#include "BandPropertiesDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBandPropertiesDlg dialog


CBandPropertiesDlg::CBandPropertiesDlg(CIdeoEditCtrl *pEditCtrl /*=NULL*/, CWnd* pParent /*=NULL*/)
	: CDialog(CBandPropertiesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBandPropertiesDlg)
	m_name = _T("");
	m_showName = FALSE;
	m_length = 0.0f;
	m_type = -1;
	//}}AFX_DATA_INIT


	pIdeoEditCtrl = pEditCtrl;


	// Create this as a modeless dialog. To make it visible, call ShowWindow().
	Create(CBandPropertiesDlg::IDD);
}


void CBandPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBandPropertiesDlg)
	DDX_Control(pDX, IDC_SPIN_LENGTH, m_lengthSpin);
	DDX_Control(pDX, IDC_COLOURBOX, m_colourbox);
	DDX_Text(pDX, IDC_EDIT_NAME, m_name);
	DDV_MaxChars(pDX, m_name, 32);
	DDX_Check(pDX, IDC_CHECK_SHOWNAME, m_showName);
	DDX_Text(pDX, IDC_EDIT_LENGTH, m_length);
	DDV_MinMaxFloat(pDX, m_length, 0.f, 1000.f);
	DDX_Radio(pDX, IDC_RADIO_BAND, m_type);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBandPropertiesDlg, CDialog)
	//{{AFX_MSG_MAP(CBandPropertiesDlg)
	ON_BN_CLICKED(IDC_BUTTON_COLOUR, OnButtonColour)
	ON_WM_PAINT()
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_LENGTH, OnDeltaposSpinLength)
	ON_BN_CLICKED(IDC_RADIO_BAND, OnRadioBand)
	ON_BN_CLICKED(IDC_RADIO_STALK, OnRadioStalk)
	ON_BN_CLICKED(IDC_RADIO_SATELLITE, OnRadioSatellite)
	ON_BN_CLICKED(IDC_BUTTON_BLACK, OnButtonBlack)
	ON_BN_CLICKED(IDC_BUTTON_WHITE, OnButtonWhite)
	ON_EN_CHANGE(IDC_EDIT_LENGTH, OnChangeEditLength)
	ON_EN_CHANGE(IDC_EDIT_NAME, OnChangeEditName)
	ON_BN_CLICKED(IDC_CHECK_SHOWNAME, OnCheckShowname)
	ON_BN_CLICKED(IDC_BUTTON_UNDO, OnButtonUndo)
	ON_BN_CLICKED(IDC_BUTTON_UP, OnButtonUp)
	ON_BN_CLICKED(IDC_BUTTON_DOWN, OnButtonDown)
	ON_BN_CLICKED(IDC_RADIO_HETEROCHROMATIC, OnRadioHeterochromatic)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBandPropertiesDlg message handlers


BOOL CBandPropertiesDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here


	// Range for the control this variable is mapped to is set to be greater
	// than 0.1, so make sure control is initialised to a value within this
	// range, or there will be a warning message.
	m_length = 1; UpdateData(FALSE);

	m_lengthSpin.SetRange(1, 1000);
	m_lengthSpin.SetPos((int)m_length);

	FillColourbox();


	Update();

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void CBandPropertiesDlg::ConfigureControls()
{
	UpdateData(TRUE); // Update m_type if selection just changed.


	BOOL disable = FALSE;


	if (!pIdeoEditCtrl || !(pIdeoEditCtrl->pCurrIdeo)) disable = TRUE;
	else
	{
		Band *pSelBand = pIdeoEditCtrl->pCurrIdeo->GetSelectedBand();
		if (!pSelBand) disable = TRUE;

		// Update() sets m_type to -1 if the band type is not editable or unknown.
		if (m_type < 0) disable = TRUE;
	}



	if (disable)
	{
		GetDlgItem(IDC_RADIO_BAND)->EnableWindow(FALSE);
		GetDlgItem(IDC_RADIO_HETEROCHROMATIC)->EnableWindow(FALSE);
		GetDlgItem(IDC_RADIO_STALK)->EnableWindow(FALSE);
		GetDlgItem(IDC_RADIO_SATELLITE)->EnableWindow(FALSE);
		GetDlgItem(IDC_RADIO_CENTROMERIC)->EnableWindow(FALSE);

		GetDlgItem(IDC_EDIT_LENGTH)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_NAME)->EnableWindow(FALSE);
		GetDlgItem(IDC_CHECK_SHOWNAME)->EnableWindow(FALSE);
		GetDlgItem(IDC_COLOURBOX)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_COLOUR)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_BLACK)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_WHITE)->EnableWindow(FALSE);

		// Cannot undo if nothing selected.
		GetDlgItem(IDC_BUTTON_UNDO)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_RADIO_BAND)->EnableWindow(TRUE);
		GetDlgItem(IDC_RADIO_HETEROCHROMATIC)->EnableWindow(TRUE);
		GetDlgItem(IDC_RADIO_STALK)->EnableWindow(TRUE);
		GetDlgItem(IDC_RADIO_SATELLITE)->EnableWindow(TRUE);
		GetDlgItem(IDC_RADIO_CENTROMERIC)->EnableWindow(TRUE);

		GetDlgItem(IDC_EDIT_LENGTH)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_NAME)->EnableWindow(TRUE);
		GetDlgItem(IDC_CHECK_SHOWNAME)->EnableWindow(TRUE);
		GetDlgItem(IDC_COLOURBOX)->EnableWindow(TRUE);
		FillColourbox(); // Colourbox is cleared when disabled - must re-fill.
		
		GetDlgItem(IDC_BUTTON_COLOUR)->EnableWindow(TRUE);



		if (m_type != 4)
		{
		// Cannot change a band to centromeric type - a centromeric band can only
		// be created by adding a centromere. This is neccessary because only this
		// way will it have an adjacent centromeric marker band created.
			GetDlgItem(IDC_RADIO_CENTROMERIC)->EnableWindow(FALSE);
		}
		else // Centromeric band
		{
		// Not allowed to change the type of a centromeric band because only they
		// are supposed to be adjacent to a centromere marker band. If a different
		// type of band is required here, the centromeric band must be deleted, 
		// which will also remove the centromere marker band.
			GetDlgItem(IDC_RADIO_BAND)->EnableWindow(FALSE);
			GetDlgItem(IDC_RADIO_HETEROCHROMATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_RADIO_STALK)->EnableWindow(FALSE);
			GetDlgItem(IDC_RADIO_SATELLITE)->EnableWindow(FALSE);
			GetDlgItem(IDC_RADIO_CENTROMERIC)->EnableWindow(TRUE);
		}	



		if (m_type != 0) 
		{
			GetDlgItem(IDC_COLOURBOX)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_COLOUR)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_BLACK)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_WHITE)->EnableWindow(FALSE);
		}
		else // Regular band
		{
		// Only regular bands have a colour.
			GetDlgItem(IDC_COLOURBOX)->EnableWindow(TRUE);
			FillColourbox(); // Colourbox is cleared when disabled - must re-fill.
			GetDlgItem(IDC_BUTTON_COLOUR)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_BLACK)->EnableWindow(TRUE);
			GetDlgItem(IDC_BUTTON_WHITE)->EnableWindow(TRUE);
		}
	}
}

void CBandPropertiesDlg::OnRadioBand() 
{
	ConfigureControls();	
	Apply();
}

void CBandPropertiesDlg::OnRadioHeterochromatic() 
{
	ConfigureControls();
	Apply();	
}

void CBandPropertiesDlg::OnRadioStalk() 
{
	ConfigureControls();
	Apply();
}

void CBandPropertiesDlg::OnRadioSatellite() 
{
	ConfigureControls();
	Apply();
}


void CBandPropertiesDlg::OnChangeEditLength() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	Apply();

	// Map length to corresponding spin control position, so that next spin
	// goes from here, rather than from where the spin control was when it was
	// last used.
	m_lengthSpin.SetPos((int)m_length);
}

void CBandPropertiesDlg::OnDeltaposSpinLength(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here


	// As m_length is a float, but the spin control only has integer positions,
	// cannot get the spin control to update the edit control automatically, 
	// must map it to a float here.
	m_length = (float)pNMUpDown->iPos;

	UpdateData(FALSE);

	Apply();
	

	*pResult = 0;
}


void CBandPropertiesDlg::OnChangeEditName() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	Apply();	
}

void CBandPropertiesDlg::OnCheckShowname() 
{
	Apply();	
}


void
CBandPropertiesDlg::FillColourbox()
{
	// Fill the colour box with the ideogram colour
	RECT rect;

	CDC *pDC = m_colourbox.GetDC();
	m_colourbox.GetClientRect(&rect);

	pDC->FillSolidRect(&rect, colour);

	m_colourbox.ReleaseDC(pDC);

	// Do need to handle any redraw events as we have just updated it in its entirety
	m_colourbox.ValidateRect(&rect);
}

void CBandPropertiesDlg::OnButtonColour() 
{
	CColorDialog dlg;

	if (dlg.DoModal() == IDOK)
	{
		colour = dlg.GetColor();

		FillColourbox();	

		Apply();
	}
}

void CBandPropertiesDlg::OnButtonBlack() 
{
	colour = RGB(0, 0, 0);
	FillColourbox();
	Apply();
}

void CBandPropertiesDlg::OnButtonWhite() 
{
	colour = RGB(255, 255, 255);
	FillColourbox();
	Apply();
}

void CBandPropertiesDlg::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	FillColourbox();
	
	// Do not call CDialog::OnPaint() for painting messages
}


void CBandPropertiesDlg::OnButtonUp() 
{
	pIdeoEditCtrl->SelectAdjacentBand(FALSE);
	Update();
}

void CBandPropertiesDlg::OnButtonDown() 
{
	pIdeoEditCtrl->SelectAdjacentBand(TRUE);
	Update();
}


void CBandPropertiesDlg::Apply() 
{
	// Apply changes to selected band.
	if (pIdeoEditCtrl && pIdeoEditCtrl->pCurrIdeo)
	{
		Band *pSelBand = pIdeoEditCtrl->pCurrIdeo->GetSelectedBand();
		
		if (pSelBand)
		{
			UpdateData(TRUE);


			switch (m_type)
			{
				case 0:  pSelBand->type = Band::BTband;       break;
				case 1:  pSelBand->type = Band::BTnonCentHet; break;
				case 2:  pSelBand->type = Band::BTstalk;      break;
				case 3:  pSelBand->type = Band::BTsatellite;  break;
				case 4:  pSelBand->type = Band::BTcentHet;    break;
				// Should never get here, but if we do, this isn't a band type
				// that can be modified (e.g. centromeric marker band): abort.
				default: return; 
			}

			pSelBand->length = m_length;
	
			strncpy(pSelBand->name, m_name, sizeof(pSelBand->name));

			pSelBand->showName = m_showName;

			// Extract the RGB from the COLOURREF (the opposite of
			// what the RGB macro does).
			pSelBand->colour[0] =  colour        & 0xFF;
			pSelBand->colour[1] = (colour >> 8)  & 0xFF;
			pSelBand->colour[2] = (colour >> 16) & 0xFF;



			// Update the ideogram display to reflect the changes to the band.
			pIdeoEditCtrl->UpdateCanvas();
			pIdeoEditCtrl->Invalidate();
			pIdeoEditCtrl->UpdateWindow();

			pIdeoEditCtrl->modified = TRUE;


			// Enable Undo button so that any of these changes can be undone
			// (until band selection changes).
			GetDlgItem(IDC_BUTTON_UNDO)->EnableWindow(TRUE);
		}
	}
}



void
CBandPropertiesDlg::Update()
{
	// Update this dialog following a change in the selected band.
	if (pIdeoEditCtrl && pIdeoEditCtrl->pCurrIdeo)
	{
		Band *pSelBand = pIdeoEditCtrl->pCurrIdeo->GetSelectedBand();
		
		if (pSelBand)
		{
			switch (pSelBand->type)
			{
				case Band::BTband:       m_type = 0; break;
				case Band::BTnonCentHet: m_type = 1; break;
				case Band::BTstalk:      m_type = 2; break;
				case Band::BTsatellite:  m_type = 3; break;
				case Band::BTcentHet:    m_type = 4; break;
				// If the band type is not one of the above we must not edit it
				// (e.g. centromere marker band). It should not be possible to
				// select such a band in the first place, but this will catch
				// it if it slips through).
				default:                 m_type = -1; break;
			}

			m_length = pSelBand->length;
			m_lengthSpin.SetPos((int)m_length);

			m_name     = pSelBand->name;
			m_showName = pSelBand->showName;

			colour = RGB(pSelBand->colour[0], 
			             pSelBand->colour[1], 
			             pSelBand->colour[2]);


			UpdateData(FALSE);


			// Disable undo button as any previous changes can no longer be
			// undone now that band selection has changed.
			GetDlgItem(IDC_BUTTON_UNDO)->EnableWindow(FALSE);


			// Save a copy of this band for future Undos.
			backupBand.type      = pSelBand->type;
			backupBand.length    = pSelBand->length;
			backupBand.colour[0] = pSelBand->colour[0];
			backupBand.colour[1] = pSelBand->colour[1];
			backupBand.colour[2] = pSelBand->colour[2];
			strncpy(backupBand.name, pSelBand->name, IBF_STR_MAXLEN);
		}
	}

	ConfigureControls();
}


void CBandPropertiesDlg::OnButtonUndo() 
{
	// Undo any changes to the currently selected band since the band selection
	// last changed (i.e. since the last call to Update()).
	// This assumes that the band selection has not changed since Update() was
	// last called, as the act of changing the band selection should cause
	// Update() to be called.
	if (pIdeoEditCtrl && pIdeoEditCtrl->pCurrIdeo)
	{
		Band *pSelBand = pIdeoEditCtrl->pCurrIdeo->GetSelectedBand();
		
		if (pSelBand)
		{
			pSelBand->type      = backupBand.type;
			pSelBand->length    = backupBand.length;
			pSelBand->colour[0] = backupBand.colour[0];
			pSelBand->colour[1] = backupBand.colour[1];
			pSelBand->colour[2] = backupBand.colour[2];
			strncpy(pSelBand->name, backupBand.name, IBF_STR_MAXLEN);

			// Disable Undo button until more changes are made.
			GetDlgItem(IDC_BUTTON_UNDO)->EnableWindow(FALSE);

			Update();

			// Update the ideogram display to reflect the changes to the band.
			pIdeoEditCtrl->UpdateCanvas();
			pIdeoEditCtrl->Invalidate();
			pIdeoEditCtrl->UpdateWindow();
		}
	}
}



