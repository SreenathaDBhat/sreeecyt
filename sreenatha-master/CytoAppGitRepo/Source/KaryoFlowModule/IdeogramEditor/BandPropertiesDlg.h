#if !defined(AFX_BANDPROPERTIESDLG_H__BE53BDF1_C8DB_11D3_BE2D_00A0C9780849__INCLUDED_)
#define AFX_BANDPROPERTIESDLG_H__BE53BDF1_C8DB_11D3_BE2D_00A0C9780849__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BandPropertiesDlg.h : header file
//

#include "IdeoEditCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CBandPropertiesDlg dialog

class CBandPropertiesDlg : public CDialog
{
private:
	CIdeoEditCtrl *pIdeoEditCtrl;

	COLORREF colour;

	Band backupBand;

	void FillColourbox();
	void ConfigureControls();
	void Apply();

public:
	void Update();


// Construction
public:
	CBandPropertiesDlg(CIdeoEditCtrl *pEditCtrl = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBandPropertiesDlg)
	enum { IDD = IDD_BANDPROPERTIES };
	CSpinButtonCtrl	m_lengthSpin;
	CStatic	m_colourbox;
	CString	m_name;
	BOOL	m_showName;
	float	m_length;
	int		m_type;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBandPropertiesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CBandPropertiesDlg)
	afx_msg void OnButtonColour();
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnDeltaposSpinLength(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRadioBand();
	afx_msg void OnRadioStalk();
	afx_msg void OnRadioSatellite();
	afx_msg void OnButtonBlack();
	afx_msg void OnButtonWhite();
	afx_msg void OnChangeEditLength();
	afx_msg void OnChangeEditName();
	afx_msg void OnCheckShowname();
	afx_msg void OnButtonUndo();
	afx_msg void OnButtonUp();
	afx_msg void OnButtonDown();
	afx_msg void OnRadioHeterochromatic();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BANDPROPERTIESDLG_H__BE53BDF1_C8DB_11D3_BE2D_00A0C9780849__INCLUDED_)
