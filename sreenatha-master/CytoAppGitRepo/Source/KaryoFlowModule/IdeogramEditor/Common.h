#pragma once

class CCommon
{
public:

	CCommon(void)
	{
	}

	//==================================================================================================
	// Helper function to extract a delimited item from a CString.
	// Given a string 'Line' with items delimited with [] enclosure will extract items into string Col.
	// The index is the start position on the string and is update with the next start position for the
	// next item
	//==================================================================================================
	static BOOL ExtractColumn(CString &Line, CString &Col, int *index, char left, char right)
	{
		BOOL bRet = FALSE;
		// Format is [text][Number] i.e Each colum is enclosed with []
		CString tmp;
		int start = *index, end;

		// First char at index must be a '['
		if (start < Line.GetLength() && Line[start] == left)
		{
			start++; // ignore the delimiter
			// Okay Find the ]
			if ((end = Line.Find(right, start)) != -1)
			{
				bRet = TRUE;
				Col = Line.Mid(start, (end - start));
				*index = end +1;
			}
		}
		return bRet;
	}

	~CCommon(void)
	{
	}
};
