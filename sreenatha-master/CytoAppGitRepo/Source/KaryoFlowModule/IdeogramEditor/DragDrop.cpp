//---------------------------DraDrop.cpp-------------------------------
// Implementation of	IDropSource
//						IDropTarget
//						IDataObject for Ole Drag and drop

#include "stdafx.h"

#include "DragDrop.h"
//#include "enumfetc.h"	// Won't be needed once data types are in registry
#include "common.h"



//---------------------------------------------------------------------
//                    CDropSource Constructor
//---------------------------------------------------------------------        
 
CDropSource::CDropSource()
{
    m_refs = 1;  
}   

//---------------------------------------------------------------------
//                    IUnknown Methods
//---------------------------------------------------------------------


STDMETHODIMP
CDropSource::QueryInterface(REFIID iid, void FAR* FAR* ppv) 
{
    if(iid == IID_IUnknown || iid == IID_IDropSource)
    {
      *ppv = this;
      ++m_refs;
      return NOERROR;
    }
    *ppv = NULL;
    return ResultFromScode(E_NOINTERFACE);
}


STDMETHODIMP_(ULONG)
CDropSource::AddRef(void)
{
    return ++m_refs;
}


STDMETHODIMP_(ULONG)
CDropSource::Release(void)
{
    if(--m_refs == 0)
    {
      delete this;
      return 0;
    }
    return m_refs;
}  

//---------------------------------------------------------------------
//                    IDropSource Methods
//---------------------------------------------------------------------  

STDMETHODIMP
CDropSource::QueryContinueDrag(BOOL fEscapePressed, DWORD grfKeyState)
{  
     if (fEscapePressed)
        return ResultFromScode(DRAGDROP_S_CANCEL);
    else if (!(grfKeyState & MK_LBUTTON))
        return ResultFromScode(DRAGDROP_S_DROP);
    else
        return NOERROR;                  
}

STDMETHODIMP
CDropSource::GiveFeedback(DWORD dwEffect)
{
    return ResultFromScode(DRAGDROP_S_USEDEFAULTCURSORS);
}


//---------------------------------------------------------------------
//                    CDataObject Constructor
//---------------------------------------------------------------------        

CDataObject::CDataObject(Canvas * can, DDGS *dog, void (*Func)(DWORD parm), DWORD data, WORD index)
{
   m_refs = 1;  

   // Constructor is passed
   m_can = can;	// The canvas used by CDrawingArea for redraw
   m_dog = dog; // The DDGS * associated with the CDrawingArea
   m_index = index;	// Index of Drawing area source
   CopyFunc = Func;	// Internal copy function
   CopyData = data; // Data required by the internal copy function
  
}   

//---------------------------------------------------------------------
//                    IUnknown Methods
//---------------------------------------------------------------------


STDMETHODIMP
CDataObject::QueryInterface(REFIID iid, void FAR* FAR* ppv) 
{
    if(iid == IID_IUnknown || iid == IID_IDataObject)
    {
      *ppv = this;
      AddRef();
      return NOERROR;
    }
    *ppv = NULL;
    return ResultFromScode(E_NOINTERFACE);
}


STDMETHODIMP_(ULONG)
CDataObject::AddRef(void)
{
    return ++m_refs;
}


STDMETHODIMP_(ULONG)
CDataObject::Release(void)
{
    if(--m_refs == 0)
    {
      delete this;
      return 0;
    }
    return m_refs;
}  

//---------------------------------------------------------------------
//                    IDataObject Methods    
//  
// The following methods are NOT supported for data transfer using the
// clipboard or drag-drop: 
//
//      IDataObject::SetData    -- return E_NOTIMPL
//      IDataObject::DAdvise    -- return OLE_E_ADVISENOTSUPPORTED
//                 ::DUnadvise
//                 ::EnumDAdvise
//      IDataObject::GetCanonicalFormatEtc -- return E_NOTIMPL
//                     (NOTE: must set pformatetcOut->ptd = NULL)
//---------------------------------------------------------------------  
//extern "C" HBITMAP canvas2bitmap(Canvas *canvas, DDGS *dog);

STDMETHODIMP 
CDataObject::GetData(LPFORMATETC pformatetc, LPSTGMEDIUM pmedium) 
{   
	HBITMAP hBitmap;

    pmedium->tymed = NULL;
    pmedium->pUnkForRelease = NULL;
    pmedium->hGlobal = NULL;
	pmedium->hBitmap = NULL;
    
    // This method is called by the drag-drop target to obtain the bitmap
    // that is being dragged.
    if (pformatetc->cfFormat == CF_BITMAP &&
       pformatetc->dwAspect == DVASPECT_CONTENT &&
       pformatetc->tymed == TYMED_GDI)
    {
		// if it has a CopyFunc then its setup to use the internal buffer
		// so convert the buffer Canvases into a bitmap
		if(CopyFunc)
		{
			CopyFunc(CopyData);
//			hBitmap = buffer_to_bitmap(m_dog);
		}
		else
		// Otherwise if we have a main canvas for redraw, use that
			if( m_can)
				hBitmap = canvas2bitmap(m_can, m_dog);

		// Set the STFMEDIUM up, info extracted by target from this
        if (hBitmap)
		{
			pmedium->tymed = TYMED_GDI;
			pmedium->hBitmap = hBitmap; 
 
			return ResultFromScode(S_OK);
		}
		else
		    return ResultFromScode(DATA_E_FORMATETC);
    }

    if (pformatetc->cfFormat == CF_CYTOVISION &&
       pformatetc->dwAspect == DVASPECT_CONTENT &&
       pformatetc->tymed == TYMED_NULL)
    {
		// This is our internal copy format, used only by our CDropTarget, we
		// only need to use the internal copy routine
        pmedium->tymed = TYMED_NULL;

		if(CopyFunc)
			CopyFunc(CopyData);

        return ResultFromScode(S_OK);
    }

    if (pformatetc->cfFormat == CF_CYTOCANVAS &&
       pformatetc->dwAspect == DVASPECT_CONTENT &&
       pformatetc->tymed == TYMED_NULL)
    {
        pmedium->tymed = TYMED_NULL;
		// For CYTOCANVAS pack canvas pointer into hGlobal member of STGMEDIUM
		if (m_can)
		{
			pmedium->hGlobal = (HGLOBAL)m_can;

			return ResultFromScode(S_OK);			
		}
    }

    if (pformatetc->cfFormat == CF_CYTOINDEX &&
       pformatetc->dwAspect == DVASPECT_CONTENT &&
       pformatetc->tymed == TYMED_NULL)
    {
        pmedium->tymed = TYMED_NULL;
		// For CYTOINDEX pack source drawing area index into hGlobal member of STGMEDIUM
		if (m_index >= 0)
		{
			pmedium->hGlobal = (HGLOBAL)m_index;

			return ResultFromScode(S_OK);			
		}
    }

    return ResultFromScode(DATA_E_FORMATETC);
}
   
STDMETHODIMP 
CDataObject::GetDataHere(LPFORMATETC pformatetc, LPSTGMEDIUM pmedium)  
{
    return ResultFromScode(DATA_E_FORMATETC);    
}     

STDMETHODIMP 
CDataObject::QueryGetData(LPFORMATETC pformatetc) 
{   
    // This method is called by the drop target to check whether the source
    // provides data is a format that the target accepts.
    if ((pformatetc->cfFormat == CF_BITMAP 
        && pformatetc->dwAspect == DVASPECT_CONTENT
        && pformatetc->tymed & TYMED_GDI))
        return ResultFromScode(S_OK); 

	if ((pformatetc->cfFormat == CF_CYTOVISION 
        && pformatetc->dwAspect == DVASPECT_CONTENT
        && pformatetc->tymed == TYMED_NULL) )
        return ResultFromScode(S_OK); 

	if ((pformatetc->cfFormat == CF_CYTOCANVAS 
        && pformatetc->dwAspect == DVASPECT_CONTENT
        && pformatetc->tymed == TYMED_NULL) )
        return ResultFromScode(S_OK); 

	if ((pformatetc->cfFormat == CF_CYTOINDEX 
        && pformatetc->dwAspect == DVASPECT_CONTENT
        && pformatetc->tymed == TYMED_NULL) )
        return ResultFromScode(S_OK); 

    
	return ResultFromScode(S_FALSE);
}

STDMETHODIMP 
CDataObject::GetCanonicalFormatEtc(LPFORMATETC pformatetc, LPFORMATETC pformatetcOut)
{ 
    pformatetcOut->ptd = NULL; 
    return ResultFromScode(E_NOTIMPL);
}        

STDMETHODIMP 
CDataObject::SetData(LPFORMATETC pformatetc, STGMEDIUM *pmedium, BOOL fRelease)
{   
    // A data transfer object that is used to transfer data
    //    (either via the clipboard or drag/drop does NOT
    //    accept SetData on ANY format.
    return ResultFromScode(E_NOTIMPL);
}


STDMETHODIMP 
CDataObject::EnumFormatEtc(DWORD dwDirection, LPENUMFORMATETC FAR* ppenumFormatEtc)
{ 
    // A standard implementation is provided by OleStdEnumFmtEtc_Create
    // which can be found in \ole2\samp\ole2ui\enumfetc.c in the OLE 2 SDK.
    // This code from ole2ui is copied to the enumfetc.c file in this sample.
	// Won't need this when data Formats are in registery
    
    SCODE sc = S_OK;
    FORMATETC fmtetc[4];
    
    fmtetc[0].cfFormat = CF_BITMAP;
    fmtetc[0].dwAspect = DVASPECT_CONTENT;
    fmtetc[0].tymed = TYMED_GDI;
    fmtetc[0].ptd = NULL;
    fmtetc[0].lindex = -1;

	fmtetc[1].cfFormat = CF_CYTOVISION;
    fmtetc[1].dwAspect = DVASPECT_CONTENT;
    fmtetc[1].tymed = TYMED_NULL;
    fmtetc[1].ptd = NULL;
    fmtetc[1].lindex = -1;

	fmtetc[2].cfFormat = CF_CYTOCANVAS;
    fmtetc[2].dwAspect = DVASPECT_CONTENT;
    fmtetc[2].tymed = TYMED_NULL;
    fmtetc[2].ptd = NULL;
    fmtetc[2].lindex = -1;

	fmtetc[3].cfFormat = CF_CYTOINDEX;
    fmtetc[3].dwAspect = DVASPECT_CONTENT;
    fmtetc[3].tymed = TYMED_NULL;
    fmtetc[3].ptd = NULL;
    fmtetc[3].lindex = -1;


    if (dwDirection == DATADIR_GET){
//        *ppenumFormatEtc = OleStdEnumFmtEtc_Create(4, fmtetc);
        if (*ppenumFormatEtc == NULL)
            sc = E_OUTOFMEMORY;
/*
	// Do this once Formats are specified in registry
	// see OleRegEnumFormatEtc()
	*ppenumFormatEtc = NULL;
	return ResultFromScode( OLE_S_USEREG );
*/

    } else if (dwDirection == DATADIR_SET){
        // A data transfer object that is used to transfer data
        //    (either via the clipboard or drag/drop does NOT
        //    accept SetData on ANY format.
        sc = E_NOTIMPL;
    
    } else {
        sc = E_INVALIDARG;
    }

    return ResultFromScode(sc);
}

STDMETHODIMP 
CDataObject::DAdvise(FORMATETC FAR* pFormatetc, DWORD advf, 
                       LPADVISESINK pAdvSink, DWORD FAR* pdwConnection)
{ 
    return ResultFromScode(OLE_E_ADVISENOTSUPPORTED);
}
   

STDMETHODIMP 
CDataObject::DUnadvise(DWORD dwConnection)
{ 
    return ResultFromScode(OLE_E_ADVISENOTSUPPORTED);
}

STDMETHODIMP 
CDataObject::EnumDAdvise(LPENUMSTATDATA FAR* ppenumAdvise)
{ 
    return ResultFromScode(OLE_E_ADVISENOTSUPPORTED);
}
//---------------------------------------------------------------------
//                    CDropTarget Constructor
//---------------------------------------------------------------------
CDropTarget::CDropTarget(void (*Func)(DWORD parm), DWORD data, WORD format)
{
   m_refs = 1; 
   m_bAcceptFmt = FALSE;
   m_format = format;
   PasteFunc = Func;
   PasteData = data;
}   

//---------------------------------------------------------------------
//                    IUnknown Methods
//---------------------------------------------------------------------


STDMETHODIMP
CDropTarget::QueryInterface(REFIID iid, void FAR* FAR* ppv) 
{
    if(iid == IID_IUnknown || iid == IID_IDropTarget)
    {
      *ppv = this;
      AddRef();
      return NOERROR;
    }
    *ppv = NULL;
    return ResultFromScode(E_NOINTERFACE);
}


STDMETHODIMP_(ULONG)
CDropTarget::AddRef(void)
{
    return ++m_refs;
}


STDMETHODIMP_(ULONG)
CDropTarget::Release(void)
{
    if(--m_refs == 0)
    {
      delete this;
      return 0;
    }
    return m_refs;
}  

//---------------------------------------------------------------------
//                    IDropTarget Methods
//---------------------------------------------------------------------  

STDMETHODIMP
CDropTarget::DragEnter(LPDATAOBJECT pDataObj, DWORD grfKeyState, POINTL pt, LPDWORD pdwEffect)
{  
    FORMATETC fmtetc;

	// The target will accept CF_CYTOVISION, CF_CYTOINDEX or CF_CYTOCANVAS as specified in m_format
	fmtetc.cfFormat = m_format;
    fmtetc.ptd      = NULL;
    fmtetc.dwAspect = DVASPECT_CONTENT;  
    fmtetc.lindex   = -1;
    fmtetc.tymed    = TYMED_NULL; 
    
    // Does the drag source provide CF_CYTOVISION, which is the only format we accept.    
    m_bAcceptFmt = (NOERROR == pDataObj->QueryGetData(&fmtetc)) ? TRUE : FALSE;    
    
    QueryDrop(grfKeyState, pdwEffect);
    return NOERROR;
}

STDMETHODIMP
CDropTarget::DragOver(DWORD grfKeyState, POINTL pt, LPDWORD pdwEffect)
{
    QueryDrop(grfKeyState, pdwEffect);
    return NOERROR;
}

STDMETHODIMP
CDropTarget::DragLeave()
{   
    m_bAcceptFmt = FALSE;   
    return NOERROR;
}

STDMETHODIMP
CDropTarget::Drop(LPDATAOBJECT pDataObj, DWORD grfKeyState, POINTL pt, LPDWORD pdwEffect)  
{   
    FORMATETC fmtetc;
	STGMEDIUM medium;   
    HRESULT hr;
	// Can we accept this data
    if (QueryDrop(grfKeyState, pdwEffect))
    {      
		if (m_format == CF_CYTOVISION)
		{
			// Call GetData() from the data object, get it the object to do a copy
			// to cytovision internal copy-paste buff
			fmtetc.cfFormat = CF_CYTOVISION;
			fmtetc.ptd      = NULL;
			fmtetc.dwAspect = DVASPECT_CONTENT;  
			fmtetc.lindex   = -1;
			fmtetc.tymed    = TYMED_NULL; 

			// Get the data object to parcel up the data
			// Usually wo would extract the data is the required format from STGMEDIUM 
			// structure, but the IDataObject will do a inetrnal copy, so we only need do
			// an internal paste

			hr = pDataObj->GetData(&fmtetc, &medium);
			// User has dropped on us
			// Do an internal cytovision paste
			if (PasteFunc)
				PasteFunc(PasteData);
		}

		if (m_format == CF_CYTOCANVAS)
		{
			// Want a pointer to the source canvas
			fmtetc.cfFormat = CF_CYTOCANVAS;
			fmtetc.ptd      = NULL;
			fmtetc.dwAspect = DVASPECT_CONTENT;  
			fmtetc.lindex   = -1;
			fmtetc.tymed    = TYMED_NULL; 

			// The pointer is stored in the hGlobal of STGMEDIUM
			hr = pDataObj->GetData(&fmtetc, &medium);
			// Pass the canvas pointer to the Paste function as data
			if (PasteFunc)
				PasteFunc((DWORD)medium.hGlobal);
		}

		if (m_format == CF_CYTOINDEX)
		{
			// Want a pointer to the source canvas
			fmtetc.cfFormat = CF_CYTOINDEX;
			fmtetc.ptd      = NULL;
			fmtetc.dwAspect = DVASPECT_CONTENT;  
			fmtetc.lindex   = -1;
			fmtetc.tymed    = TYMED_NULL; 

			// The index is stored in the hGlobal of STGMEDIUM
			hr = pDataObj->GetData(&fmtetc, &medium);

			DWORD index = (DWORD)medium.hGlobal;
			// Pass the index to the Paste function as data
			if (PasteFunc)
				PasteFunc(index);
		}


    }
    return NOERROR;      
}   

/* OleStdGetDropEffect
** -------------------
**
** Convert a keyboard state into a DROPEFFECT.
**
** returns the DROPEFFECT value derived from the key state.
**    the following is the standard interpretation:
**          no modifier -- Default Drop     (0 is returned)
**          CTRL        -- DROPEFFECT_COPY
**          SHIFT       -- DROPEFFECT_MOVE
**          CTRL-SHIFT  -- DROPEFFECT_LINK
**
**    Default Drop: this depends on the type of the target application.
**    this is re-interpretable by each target application. a typical
**    interpretation is if the drag is local to the same document
**    (which is source of the drag) then a MOVE operation is
**    performed. if the drag is not local, then a COPY operation is
**    performed.
*/
#define OleStdGetDropEffect(grfKeyState)    \
    ( (grfKeyState & MK_CONTROL) ?          \
        ( (grfKeyState & MK_SHIFT) ? DROPEFFECT_LINK : DROPEFFECT_COPY ) :  \
        ( (grfKeyState & MK_SHIFT) ? DROPEFFECT_MOVE : 0 ) )

//---------------------------------------------------------------------
// CDropTarget::QueryDrop: Given key state, determines the type of 
// acceptable drag and returns the a dwEffect. 
//---------------------------------------------------------------------   
STDMETHODIMP_(BOOL)
CDropTarget::QueryDrop(DWORD grfKeyState, LPDWORD pdwEffect)
{  
    DWORD dwOKEffects = *pdwEffect; 
    
    if (!m_bAcceptFmt)
	{
		*pdwEffect = DROPEFFECT_NONE;
		return FALSE;
	}
     
    *pdwEffect = OleStdGetDropEffect(grfKeyState);
    if (*pdwEffect == 0) {
        // No modifier keys used by user while dragging. Try in order: MOVE, COPY.
        if (DROPEFFECT_MOVE & dwOKEffects)
            *pdwEffect = DROPEFFECT_MOVE;
        else if (DROPEFFECT_COPY & dwOKEffects)
            *pdwEffect = DROPEFFECT_COPY; 
        else		
		{
			*pdwEffect = DROPEFFECT_NONE;
			return FALSE;
		}
    } 
    else {
        // Check if the drag source application allows the drop effect desired by user.
        // The drag source specifies this in DoDragDrop
        if (!(*pdwEffect & dwOKEffects))
		{
			*pdwEffect = DROPEFFECT_NONE;
			return FALSE;
		}
        // We don't accept links
        if (*pdwEffect == DROPEFFECT_LINK)
		{
			*pdwEffect = DROPEFFECT_NONE;
			return FALSE;
		}
    }  
    return TRUE;
}   
    
