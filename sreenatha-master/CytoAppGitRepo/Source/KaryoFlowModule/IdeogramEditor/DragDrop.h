//---------------------------DraDrop.h
// Implementation of	IDropSource
//						IDropTarget
//						IDataObject for Ole Drag and drop
#ifndef _DRAGDROP_H
#define _DRAGDROP_H


#include "canvas.h"
#include "ddgs.h"


#include <ole2.h>
//#include "Canvas.h"
//#include "ddgs.h"

// TYPE flags
#define DROP_SOURCE	0x0001
#define DROP_TARGET	0x0002

// STATES
#define NO_DRAG		0	// NOT DRAGGING
#define START_DRAG	1	// DRAG REQUEST
#define DRAG_OFF	2	// DRAG NOT ALLOWED
#define DRAGGING	3	// DRAG IN PROGRESS

// Data Types
#define CF_CYTOVISION	998		// Copy - Paste routine
#define CF_CYTOCANVAS	999		// return Canvas routine
#define CF_CYTOINDEX	1000	// return Box index routine

//------------------------IDropSource implementation------------------------------------
class CDropSource : public IDropSource
{
public:    
    CDropSource();

    /* IUnknown methods */
    STDMETHOD(QueryInterface)(REFIID riid, void FAR* FAR* ppvObj);
    STDMETHOD_(ULONG, AddRef)(void);
    STDMETHOD_(ULONG, Release)(void);

    /* IDropSource methods */
    STDMETHOD(QueryContinueDrag)(BOOL fEscapePressed, DWORD grfKeyState);
    STDMETHOD(GiveFeedback)(DWORD dwEffect);
 
private:
    ULONG m_refs;     
};  

//------------------------IDataObject implementation------------------------------------

class CDataObject : public IDataObject
{
public:
    CDataObject(Canvas * can, DDGS * dog, void (*Func)(DWORD data)= NULL, DWORD data = 0, WORD index = 0);  
   /* IUnknown methods */
    STDMETHOD(QueryInterface)(REFIID riid, void FAR* FAR* ppvObj);
    STDMETHOD_(ULONG, AddRef)(void);
    STDMETHOD_(ULONG, Release)(void);

    /* IDataObject methods */    
     STDMETHOD(GetData)(LPFORMATETC pformatetcIn,  LPSTGMEDIUM pmedium );
    STDMETHOD(GetDataHere)(LPFORMATETC pformatetc, LPSTGMEDIUM pmedium );
    STDMETHOD(QueryGetData)(LPFORMATETC pformatetc );
    STDMETHOD(GetCanonicalFormatEtc)(LPFORMATETC pformatetc, LPFORMATETC pformatetcOut);
    STDMETHOD(SetData)(LPFORMATETC pformatetc, STGMEDIUM FAR * pmedium,
                       BOOL fRelease);
    STDMETHOD(EnumFormatEtc)(DWORD dwDirection, LPENUMFORMATETC FAR* ppenumFormatEtc);
    STDMETHOD(DAdvise)(FORMATETC FAR* pFormatetc, DWORD advf, 
                       LPADVISESINK pAdvSink, DWORD FAR* pdwConnection);
    STDMETHOD(DUnadvise)(DWORD dwConnection);
    STDMETHOD(EnumDAdvise)(LPENUMSTATDATA FAR* ppenumAdvise);
    
private:
    ULONG m_refs;   
    Canvas * m_can;
	DDGS * m_dog;
	void (*CopyFunc)(DWORD parm);
	DWORD CopyData;
	WORD m_index;
};

//------------------------IDropTarget implementation------------------------------------

class CDropTarget : public IDropTarget
{
public:    
    CDropTarget(void (*Func)(DWORD data)= NULL, DWORD data = 0, WORD format = CF_CYTOVISION);

    /* IUnknown methods */
    STDMETHOD(QueryInterface)(REFIID riid, void FAR* FAR* ppvObj);
    STDMETHOD_(ULONG, AddRef)(void);
    STDMETHOD_(ULONG, Release)(void);

    /* IDropTarget methods */
    STDMETHOD(DragEnter)(LPDATAOBJECT pDataObj, DWORD grfKeyState, POINTL pt, LPDWORD pdwEffect);
    STDMETHOD(DragOver)(DWORD grfKeyState, POINTL pt, LPDWORD pdwEffect);
    STDMETHOD(DragLeave)();
    STDMETHOD(Drop)(LPDATAOBJECT pDataObj, DWORD grfKeyState, POINTL pt, LPDWORD pdwEffect); 
    
    /* Utility function to read type of drag from key state*/
    STDMETHOD_(BOOL, QueryDrop)(DWORD grfKeyState, LPDWORD pdwEffect);
 
private:
    ULONG m_refs;  
    BOOL m_bAcceptFmt;
	void (*PasteFunc)(DWORD parm);
	DWORD PasteData;
	WORD m_format;
};

#endif
