// DrawingArea.cpp : implementation file
//
//
//  15Nov99 MC  Mod to ScaleToFitInside(), moved zoomscale = 1 from beginning to end
// ,		    of function, where scaling is actually done. Problem occured with zoomed
//				up normal images (like from Tutor) followed by 'Scale to Fit', which meant
//				next zoom op is 'zoom up', which is correct for large images that have 
//				been scaled down, but wrong for normal images where 'Scale to Fit'
//				has no effect. So now zoomscale not set to 1 for normal images.
//
//	01Nov99	dcb	Add AddCusomMenuItemID() to add menu text by Id rather than compiled in text string
//	04Aug99 WH	Added scaletofit stuff. Scale canvas to fit inside the client area.
//	28Jul99 WH	ScrollWindows doesn't produce the correct update region so do it ourselves.
//				Should mean faster scrolling.
//	28Jul99 WH	Fix bug in scroll in zoom with large canvases.
//	22Jul99	JMB	Turned autoscroll off by default and added method to turn it on
//				or off.
//	21Jul99	JMB	Fixed ZoomCanvas so that it centers correctly for full-screen zoom.
//  30Jun99	WH	SetCanvas nolonger flips can offset so that image beings at top of screen.
//	28Jun99 WH	Modify zoomCanvas such that it remembers scroll position when zoom down
//				on large canvases (i.e have scrollbars already)
//	07May99	dcb	Add SetScrollPositions()
//	26May99	dcb	Add external event handler calls to OnHScroll(), OnVScroll()
//	31Mar99	WH	Add scrollbars if canvas set is larger than screen size.
//	25Feb99 WH	Added OnSetCursor override, prevent parent from changing our cursor
//	12Feb99	WH	Remove inards of OnCaptureChanged - Why releaseCapture, it screws up capturing
//				the mouse elsewhere ??? Should be removed altogether.
//	30Nov98	WH	Activate/Deactivate box selector when cursor enters or leaves window.
//	03Nov98	dcb	Add MouseTracker to detect when mouse leaves drawing area
//	30Nov98 AR	Added ClearArea and CopyArea. Made ExposeArea public.
//

#include "stdafx.h"
#include <commctrl.h>
//#include <Warning.h>	// For GetResourceStr()

#include "DrawingArea.h"
#include "resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Flags for DA capabilities.
#define	DA_BOXSELECT		0x0001	// Selector box
#define	DA_ZOOM				0x0002	// zoom
#define	DA_MENU				0x0004	// menu
#define DA_SELCANVAS		0x0008	// menu over selected canvas
#define DA_AUTOSCROLL		0x0010	// autoscroll

// Resource identifier range for context menu
#define IDC_CUSTOMMENU		800						// ID of first custom menu item
#define IDC_MAX_CUSTOMMENU	IDC_CUSTOMMENU + 100	// ID of last custom menu item (i.e max of 100)
// Scrollbars
#define IDC_VERT			901
#define IDC_HORZ			902
#define SB_WIDTH			20						// Scrollbar width

// Timers for autoscroll
#define AUTOVSCROLL_TIMER	99
#define AUTOHSCROLL_TIMER	98

#define AUTOSCROLL_SPEED	100		// mSeconds

// Want to send meaningful messages back to parent.
//#include "CVMessage.c"

/////////////////////////////////////////////////////////////////////////////
// CDrawingArea

//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
CDrawingArea::CDrawingArea(int set_index)
{
	WNDCLASS c;
	HINSTANCE hinstance = AfxGetInstanceHandle();

	c.style = CS_PARENTDC | CS_DBLCLKS; // Added double clicks style for ideogram editor.
	c.lpfnWndProc = ::DefWindowProc;
	c.cbClsExtra = 0;
	c.cbWndExtra = 0;
	c.hInstance = hinstance;
	c.hIcon = 0;
	c.hCursor = NULL;	// No cursor as we change ours all the time
	c.hbrBackground = 0;
	c.lpszMenuName = NULL;
	c.lpszClassName = "DrawingArea";

	RegisterClass(&c);

	// Initially no interact callbacks are defined.
	externalEH = NULL;
	client_data = 0;
	// No custom Menu event handler
	custommenu = NULL;
	CustomMenuEH = NULL;
	nCustomItems = 0;

	// And there are no attachments.
	dog = NULL;
	can = NULL;
	box = NULL;

	// Autoscroll timers id
	x_timer = y_timer = 0;

	index = set_index;
	flags = 0;

	// Autoscroll is off by default.
	flags &= ~DA_AUTOSCROLL;
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
CDrawingArea::~CDrawingArea()
{
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
BEGIN_MESSAGE_MAP(CDrawingArea, CWnd)
	//{{AFX_MSG_MAP(CDrawingArea)
	ON_WM_ERASEBKGND()
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MBUTTONDOWN()
	ON_WM_MBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_DESTROY()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_SIZE()
	ON_WM_SETCURSOR()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
	ON_WM_CAPTURECHANGED()
	// Catch the custom menu events
	ON_COMMAND_RANGE(IDC_CUSTOMMENU, IDC_MAX_CUSTOMMENU, OnCustomMenu)
END_MESSAGE_MAP()
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
BOOL CDrawingArea::Create(LPCTSTR lpszWindowName,
		short x, short y, short w, short h, CWnd* pParentWnd)
{
	DWORD style;

	style = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

	return CWnd::CreateEx(WS_EX_CLIENTEDGE, "DrawingArea",
		lpszWindowName, style, (int)x, (int)y, (int)w, (int)h,
		pParentWnd->m_hWnd, NULL, NULL);
}

/////////////////////////////////////////////////////////////////////////////
// CDrawingArea message handlers

BOOL CDrawingArea::PreCreateWindow(CREATESTRUCT& cs) 
{
	return CWnd::PreCreateWindow(cs);
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
int CDrawingArea::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Initialize Drag and Drop stuff
	m_drag = 0;
	pDropTarget = NULL;
	pDropSource = NULL;
	pDataObject = NULL;
	// default zoom
	zoomfactor = 2;
	zoomscale = 1;
	m_bScrollEnabled = FALSE;	// Is scrolling in normal scale allowed
	preZoom_dx = preZoom_dy = preZoom_ScrollPosX = preZoom_ScrollPosY = 0;
	scaletofit = 0;				// Scale to fit FALSE

	// Create scrollbars, standard scroll bars don't seem to generate drag events ??
	CRect rect(lpCreateStruct->cx -SB_WIDTH ,0 ,lpCreateStruct->cx -3, lpCreateStruct->cy - SB_WIDTH);
	vert = new CScrollBar();
	vert->Create(WS_CHILD|SBS_VERT|SBS_RIGHTALIGN, rect, this, IDC_VERT);

	rect.SetRect(0, lpCreateStruct->cy - SB_WIDTH , lpCreateStruct->cx -SB_WIDTH, lpCreateStruct->cy -3);
	horz = new CScrollBar();
	horz->Create(WS_CHILD|SBS_HORZ|SBS_BOTTOMALIGN, rect, this, IDC_HORZ);

	// Flag indicating that cursor has left Drawing area
	// Setup bu WM_MOUSELEAVE event
	bLeft = TRUE;

	return 0;
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::OnDestroy() 
{
	ddgsclose(dog);
	dog = NULL;
	can = NULL;
	box = NULL;

	// Tidy up Drag and Drop if used
	SetDropSource(FALSE);
//	SetDropTarget(FALSE);

	delete vert;
	delete horz;

	// Free any event handlers
	POSITION pos;
	for( pos = eventhandlers.GetHeadPosition(); pos != NULL; )
	{
		eventhandler_item * item;
		item = eventhandlers.GetNext( pos );
		if (item)
		{
			delete item;
		}
	}
	eventhandlers.RemoveAll();

	ClearCustomMenu();

	CWnd::OnDestroy();
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::OnSize(UINT nType, int cx, int cy) 
{
	// Handle resizing of window for Full screen etc
	CWnd::OnSize(nType, cx, cy);

	// Create ddgs 
	if(dog)
		ddgsclose(dog);

	dog = ddgsopen(m_hWnd, cx, cy);

	scaletofit = 0;				// Scale to fit FALSE

	// Resize scroll bars
	CRect rect(cx -SB_WIDTH ,0 ,cx, cy - SB_WIDTH);	
	vert->MoveWindow(rect);
	rect.SetRect(0, cy - SB_WIDTH , cx -SB_WIDTH, cy);
	horz->MoveWindow(rect);
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
BOOL CDrawingArea::OnEraseBkgnd(CDC* pDC) 
{
	// No background - assumes ddgs functions.
	return TRUE;
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////
//
//	BOOL CDrawingArea::PreTranslateMessage(MSG* pMsg) 
//	Processing for messages that are not in the message map
//	1) Trap WM_MOUSELEAVE - Mouse is leaving drawing area
//	2) Trap WM_MOUSEHOVER - Mouse has entered drawing area
//
BOOL CDrawingArea::PreTranslateMessage(MSG* pMsg) 
{
	// Special message processing
	if (pMsg->message == WM_MOUSELEAVE) 
	{
		// Deactivate box selector so as to remove blue frame
		if (flags & DA_BOXSELECT)
			deactivate_box_selector(box);

		// Send WM_MOUSELEAVE message to external event handler
		if (externalEH)
			externalEH(pMsg->message, 0, 0, client_data);

		bLeft = TRUE;
	}

	return CWnd::PreTranslateMessage(pMsg);
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::OnCaptureChanged(CWnd *pWnd)
{
	// Don't release capture on drag drop as it screws it up
//	if (m_drag != DRAGGING)
//		ReleaseCapture();

	// Send WM_CAPTURECHANGED message to external event handler
	// in case it does something with it
//	if (externalEH)
//		externalEH(WM_CAPTURECHANGED, 0, 0, client_data);
}

BOOL CDrawingArea::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	// Override this to return FALSE to stop our parent from changing our cursor
	//return CWnd::OnSetCursor(pWnd, nHitTest, message);
	return FALSE;
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////////////////////
//
//	void CDrawingArea::TrackMouse()
//	Set up mouse tracking event or mouse leaving the drawing area
//
void CDrawingArea::TrackMouse()
{
	TRACKMOUSEEVENT tme;

	// Set tracker struct
	tme.cbSize = sizeof(TRACKMOUSEEVENT);
	tme.dwFlags = TME_LEAVE;
	tme.hwndTrack = m_hWnd;

	// Set up tracker event
	if (!_TrackMouseEvent(&tme))
		MessageBox(TEXT("TrackMouseEvent Failed"),TEXT("Mouse Leave"),MB_OK);
}
// Special function members.
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::SetColor(BOOL is_black)
{
	// Obsolete
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::SetCanvas(Canvas *new_can)
{
	can = new_can;

	if (can)
	{

	}
	// May have to add scrollbars if canvas is bigger than clientarea
	SetScreenForCanvas();
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::SetScreenForCanvas()
{
	// Sort out some scrollbars for a canvas that is bigger than the Drawing Area's
	// client area
	CRect rect;
	GetClientRect(rect);
	
	int can_cx, can_cy;	// scaled dimensions
	int can_dx, can_dy;	// scaled offsets

	if (can && m_bScrollEnabled)
	{
		float scale = 8.0/(float)can->frame.cpframe.scale;
		can_cx = (can->frame.width/8)/scale;
		can_cy = (can->frame.height/8)/scale;
		can_dx = (can->frame.cpframe.dx/8)/scale;
		can_dy = (can->frame.cpframe.dy/8)/scale;

		SCROLLINFO scrollinfo;
		scrollinfo.cbSize = sizeof(SCROLLINFO);
		scrollinfo.fMask =	SIF_PAGE |SIF_POS | SIF_RANGE | SIF_DISABLENOSCROLL;

		// Do we need a vertical scrollbar
		if ( can_cy > rect.Height())
		{

			if (zoomscale == 1)
			{
				// Set up scrollbar
				vert->ShowScrollBar(TRUE);

				// VERTICAL
				scrollinfo.nMin = 0;
				scrollinfo.nPage = rect.Height();
				scrollinfo.nMax = can_cy;
				scrollinfo.nPos = 0;

				if (preZoom_ScrollPosY == 0)	// Not zooming down
				{
					// Set the scrollbars to correspond to the dy offset
					scrollinfo.nPos = (scrollinfo.nMax - scrollinfo.nPage) + can_dy;
				}
				else
				{
					can->frame.cpframe.dy = preZoom_dy;
					scrollinfo.nPos = preZoom_ScrollPosY;
				}
				vert->SetScrollInfo(&scrollinfo);
			}
		}			
		else
			vert->ShowScrollBar(FALSE);

		// Do we need a horizontal scrollbar
		if (can_cx > rect.Width())
		{
			if (zoomscale == 1)
			{
				// Set up scrollbar
				horz->ShowScrollBar(TRUE);

				// HORIZONTAL
				scrollinfo.nMin = 0;
				scrollinfo.nPage = rect.Width();
				scrollinfo.nMax = can_cx;
				scrollinfo.nPos = 0;

				if (preZoom_ScrollPosY == 0)	// Not zooming down
				{
					// Set the scrollbars to correspond to the dx offset
					scrollinfo.nPos = -can_dx;
				}
				else
				{
					can->frame.cpframe.dx = preZoom_dx;
					scrollinfo.nPos = preZoom_ScrollPosX;
				}
				horz->SetScrollInfo(&scrollinfo);
			}
		}			
		else
			horz->ShowScrollBar(FALSE);

	}
	else
	{
		vert->ShowScrollBar(FALSE);
		horz->ShowScrollBar(FALSE);
	}
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::ResetToDefault()
{
	// Reset to initial settings
	zoomscale = 1;
	preZoom_dx = preZoom_dy = preZoom_ScrollPosX = preZoom_ScrollPosY = 0;
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::SetSelector(Cselectdata *new_box)
{
	box = new_box;
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::SetEventHandler(void (*func)(UINT event, long x, long y, DWORD ddata), DWORD data)
{
	externalEH = func;
	client_data = data;
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
EH CDrawingArea::GetEventHandler()
{
	return externalEH;
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::ActivateSelector(BOOL state)
{
	if (state)
	{
		flags |= DA_BOXSELECT;
		activate_box_selector(box);
	}
	else
	{
		flags &= ~DA_BOXSELECT;
		deactivate_box_selector(box);
	}
}

// Windows message callbacks.
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::OnPaint() 
{
	RECT rect;

	// Get the exposed rectangle
	if (GetUpdateRect(&rect) == FALSE)
		return;

	if (flags & DA_BOXSELECT && !bLeft)
		deactivate_box_selector(box);

	// Redraw bounded canvases
	ExposeArea(&rect);

	// Send paint event to external event handler incase it does something with it
	if (externalEH)
		externalEH(WM_PAINT, 0, 0, client_data);
	HandleEvents(WM_PAINT, 0, 0);

	ValidateRect(NULL);

	// Force the selection box to re initialize in case it got hit by
	// the redraw
	if (flags & DA_BOXSELECT && !bLeft)
		activate_box_selector(box);
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::ConstrainMouse(BOOL state)
{
	if (state)
		SetCapture();
	else
		ReleaseCapture();
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::OnMouseMove(UINT nFlags, CPoint point) 
{
	if ((nFlags & (MK_LBUTTON | MK_MBUTTON | MK_RBUTTON)) == 0)
		ConstrainMouse(FALSE);

	// Activate mouse tracker
	TrackMouse();

	// Check if we are to drag and drop, LBUTTONDOWM
	if(m_drag == START_DRAG)
		StartDragging();

	// Send x, y to box selector routine if active
	if (flags & DA_BOXSELECT)
	{
		// If the cursor had left the window we have to reset the box selector
		if (bLeft && !ddgsMouseBusy(dog))
		{
			activate_box_selector(box);
			bLeft = FALSE;
		}
	
		box_selectorEH(box, point.x, point.y, FALSE);
	}

	// Send mouse move messages
	if (externalEH)
		externalEH(WM_MOUSEMOVE, point.x, point.y, client_data);
	HandleEvents(WM_MOUSEMOVE, point.x, point.y);

	if (!ddgsMouseMove(dog, point.x, point.y))
	{
		// enable scrollbars (No ddgs function in progress)
		if (!vert->IsWindowEnabled())
			vert->EnableWindow(TRUE);
		if (!horz->IsWindowEnabled())
			horz->EnableWindow(TRUE);

		// Do we need to scroll automatically
		CheckAutoScroll(point);
	}
	else
	{
		// Disable scrollbars (ddgs function in progress)
		if (vert->IsWindowEnabled())
			vert->EnableWindow(FALSE);
		if (horz->IsWindowEnabled())
			horz->EnableWindow(FALSE);
	}
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if (flags & DA_BOXSELECT)
		deactivate_box_selector(box);

	// Indicate we might want to drag and drop if CTRL is dowm
	// and we actual have a drag source interface
	if(pDropSource && m_drag == NO_DRAG && (nFlags & MK_CONTROL))
	{
		m_drag = START_DRAG;
		return;
	}

	ConstrainMouse(TRUE);

	if (externalEH && !(nFlags & MK_CONTROL))
		externalEH(WM_LBUTTONDOWN, point.x, point.y, client_data);
	HandleEvents(WM_LBUTTONDOWN, point.x, point.y);

	if (flags & DA_BOXSELECT && !ddgsMouseBusy(dog))
		activate_box_selector(box);

	CWnd::OnLButtonDown(nFlags, point);
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if (flags & DA_BOXSELECT)
		deactivate_box_selector(box);

	if ((nFlags & (MK_LBUTTON | MK_MBUTTON | MK_RBUTTON)) == 0)
		ConstrainMouse(FALSE);

	// Kill off any autoscrolling
	if (y_timer)
	{
		KillTimer(AUTOVSCROLL_TIMER);
		if (externalEH && !x_timer)	// still scrolling if x_timer exists
			externalEH(DA_STOPAUTOSCROLL, 0, 0, client_data);
		y_timer = 0;
	}
	if (x_timer)
	{
		KillTimer(AUTOHSCROLL_TIMER);
		if (externalEH)
			externalEH(DA_STOPAUTOSCROLL, 0, 0, client_data);
		x_timer = 0;
	}

	// Send event to ddgs if required
	ddgsMouseRelease(dog, point.x, point.y, WM_LBUTTONUP);

	if (externalEH  && !(nFlags & MK_CONTROL))
		externalEH(WM_LBUTTONUP, point.x, point.y, client_data);

	HandleEvents(WM_LBUTTONUP, point.x, point.y);

	if (flags & DA_BOXSELECT && !ddgsMouseBusy(dog))
		activate_box_selector(box);

	// Bit of a frig to redraw the border if a canvas was moved in a zoomed out image
	if (can && scaletofit)
	{
		selectddgs(dog);
		intens(1);
		lwidth(2);
		display_Canvas_frame(can, MARKIM, RED, COPY);
		lwidth(1);
	}

	CWnd::OnLButtonUp(nFlags, point);
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::OnMButtonDown(UINT nFlags, CPoint point) 
{
	if (flags & DA_BOXSELECT)
		deactivate_box_selector(box);

	ConstrainMouse(TRUE);

	// Only zoom if theres a main canvas, zooming
	// is enabled and the other buttons are not down and
	// not in the middle of a ddgsmouse op.
	if (can && (flags & DA_ZOOM) 
			&& !(nFlags & (MK_LBUTTON | MK_RBUTTON))
			&& !ddgsMouseBusy(dog))
		ZoomCanvas(zoomfactor, point.x, point.y);

	if (externalEH)
		externalEH(WM_MBUTTONDOWN, point.x, point.y, client_data);
	HandleEvents(WM_MBUTTONDOWN, point.x, point.y);

	if (flags & DA_BOXSELECT  && !ddgsMouseBusy(dog))
		activate_box_selector(box);

	CWnd::OnLButtonDown(nFlags, point);
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::OnMButtonUp(UINT nFlags, CPoint point) 
{
	if (flags & DA_BOXSELECT)
		deactivate_box_selector(box);

	if ((nFlags & (MK_LBUTTON | MK_MBUTTON | MK_RBUTTON)) == 0)
		ConstrainMouse(FALSE);

	// Send event to ddgs if required
	ddgsMouseRelease(dog, point.x, point.y, WM_MBUTTONUP);

	if (externalEH)
		externalEH(WM_MBUTTONUP, point.x, point.y, client_data);
	HandleEvents(WM_MBUTTONUP, point.x, point.y);

	if (flags & DA_BOXSELECT  && !ddgsMouseBusy(dog))
		activate_box_selector(box);

	CWnd::OnLButtonUp(nFlags, point);
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::OnRButtonDown(UINT nFlags, CPoint point) 
{
	Canvas *selcan = NULL;
	
	if (box)
		selcan = box->selected_canvas;

	ConstrainMouse(TRUE);

	// Check that we can popup the menu if we're over a canvas (option)
	// DA_SELCANVAS set then must be over canvas to popup menu
	// DA_SELCANVAS not set then must not be over canvas to popup menu
	if (flags & DA_MENU && (((flags & DA_SELCANVAS) && selcan) 
							|| !(flags & DA_SELCANVAS) && (selcan == NULL)))
	{
		if (!ddgsMouseBusy(dog)) // no menu if in ddgs draw
		{
			// Require box selector if DA_SELCANVAS set
			if (!(flags & DA_SELCANVAS) && flags & DA_BOXSELECT )
				deactivate_box_selector(box);

			CPoint pt = point;
		
			if (custommenu)
			{
				ConstrainMouse(FALSE);
				ClientToScreen(&pt);
				custommenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,
						pt.x, pt.y, this);
			}
	
			if (!(flags & DA_SELCANVAS) && flags & DA_BOXSELECT)
				activate_box_selector(box);
		}
	}
	else
	{
		if (flags & DA_BOXSELECT)
			deactivate_box_selector(box);

		if (externalEH)
			externalEH(WM_RBUTTONDOWN, point.x, point.y, client_data);
		HandleEvents(WM_RBUTTONDOWN, point.x, point.y);

		if (flags & DA_BOXSELECT  && !ddgsMouseBusy(dog))
			activate_box_selector(box);
	}
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::OnRButtonUp(UINT nFlags, CPoint point) 
{
	if (flags & DA_BOXSELECT)
		deactivate_box_selector(box);

	ConstrainMouse(FALSE);
	// Send event to ddgs if required
	ddgsMouseRelease(dog, point.x, point.y, WM_RBUTTONUP);

	if (externalEH)
		externalEH(WM_RBUTTONUP, point.x, point.y, client_data);
	HandleEvents(WM_RBUTTONUP, point.x, point.y);

	if (flags & DA_BOXSELECT  && !ddgsMouseBusy(dog))
		activate_box_selector(box);
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::Pseudocolour() 
{
	// Show with pseudocolour
	selectddgs(dog);

	if (dog->pseudocolour)
		monodg();
	else
		pseudodg();

	InvalidateRect(NULL);
	PostMessage(WM_PAINT);
}
//----------------------------------------------------------------------------------
//                        Drag and Drop Stuff - Start
//----------------------------------------------------------------------------------
void CDrawingArea::SetDropSource(BOOL source, void (*copyfunc)(DWORD parm), DWORD data)
{
/*  Commented this out for now for DLL version


	// if source is TRUE initialize window as a DROP SOURCE otherwise remove any
	// drop source capability
	if(source)
	{
		if(pDropSource)
			pDropSource->Release();	// deletes object

		// Create an derived IDropSource object
		pDropSource = (LPDROPSOURCE) new CDropSource;

		// Create IDataObject implementation
		if(pDataObject)
			pDataObject->Release();	// deletes object

		pDataObject = (LPDATAOBJECT) new CDataObject(can, dog, copyfunc, data, index);		

		m_drag = NO_DRAG;	// Current state is no drag happening
	}
	else
	{
		if(pDropSource)
		{
			pDropSource->Release();	// deletes object
			pDropSource = NULL;
		}
		if(pDataObject)
		{
			pDataObject->Release();	// deletes object
			pDataObject = NULL;
		}
	
		m_drag = DRAG_OFF;
	}

*/
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::SetDropTarget(BOOL target, void (*pastefunc)(DWORD parm), DWORD data, WORD format)
{
/*  Commented this out for now for DLL version


	// if target is TRUE initialize and register window as a DROP TARGET ( doesn't initiate
	// the drag, thats done by the StartDragging function) otherwise 
	// remove any drop target capability
	if(target)
	{
		if(pDropTarget)
			SetDropTarget(FALSE); // Get rid of the old object
		
		// Create a derived IDropTarget object
		pDropTarget = (LPDROPTARGET) new CDropTarget(pastefunc, data, format); 
		CoLockObjectExternal(pDropTarget, TRUE, TRUE);
		// register the window as a drop target.
		if(RegisterDragDrop(m_hWnd, pDropTarget)!= S_OK)
			MessageBox("Can't register as drop target");
	}
	else
	{
		if(pDropTarget)
		{
			RevokeDragDrop(m_hWnd);
			pDropTarget->Release();  // deletes object
			CoLockObjectExternal(pDropTarget, FALSE, TRUE);
			RevokeDragDrop(m_hWnd);
			pDropTarget = NULL;

		}
	}

*/
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::StartDragging()
{
	// Initiates the drag, Stops the window being its own target until DoDragDrop returns
	if ( m_drag == START_DRAG)
	{
		ConstrainMouse(FALSE);
		// Start drag as outide window
		DWORD dwEffect;
		// Stop it being a target
		if(pDropTarget)
			RevokeDragDrop(m_hWnd);
   
		m_drag = DRAGGING;
		// This drag source only allows copying of data. Move and link is not allowed.
		DoDragDrop(pDataObject, pDropSource, DROPEFFECT_COPY, &dwEffect);     

		// Make it a target again
		if(pDropTarget)
			if(RegisterDragDrop(m_hWnd, pDropTarget)!= S_OK)
				MessageBox("Can't register as drop target");
		
		m_drag = NO_DRAG;

		// Reactivate the box selector
		if (flags & DA_BOXSELECT)
			activate_box_selector(box);
	}
}
//----------------------------------------------------------------------------------
//                        Drag and Drop Stuff - Finish
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//                        Canvas Drawing Stuff - Start
//----------------------------------------------------------------------------------
void CDrawingArea::ActivateZoom(BOOL state, BOOL bHide, BOOL bReset)
{
	// activate zoom
	if (state)
		flags |= DA_ZOOM;
	else
		flags &= ~DA_ZOOM;

	// hide scrollbars if requested
	if (bHide)
	{
		vert->ShowScrollBar(FALSE);
		horz->ShowScrollBar(FALSE);
	}

	// Zoom down if requested (bReset) when deactivate (!state) if zoomed
	if (bReset && !state && (zoomscale != 1 || scaletofit))
	{
		ZoomCanvas(zoomfactor, 0, 0);
		zoomscale = 1;
	}
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::SetZoomFactor(int factor)
{
	// zoom scale is actual used in calculations but isn't set
	// now incase in a zoom state when this is called
	zoomfactor = factor;
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::GetScrollOffsets(int *x, int *y)
{
	// Get the amount scrollable in the x and y direction
	SCROLLINFO info;

	*x =0;
	*y =0;
	// Allow for the scroll bars
	if (horz->IsWindowVisible())
	{
		horz->GetScrollInfo(&info, SIF_ALL);
		*x = info.nMax - info.nPage;
	}
	if (vert->IsWindowVisible())
	{
		vert->GetScrollInfo(&info, SIF_ALL);
		*y = info.nMax - info.nPage;
	}
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::GetScrollPositions(int *x, int *y)
{
	// Get the positions of scrollbars
	SCROLLINFO info;

	*x =0;
	*y =0;
	// Allow for the scroll bars
	if (horz->IsWindowVisible())
	{
		horz->GetScrollInfo(&info, SIF_ALL);
		*x = info.nPos;
	}
	if (vert->IsWindowVisible())
	{
		vert->GetScrollInfo(&info, SIF_ALL);
		*y = info.nPos;
	}
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::SetScrollPositions(int x, int y)
{
	// Allow for the scroll bars
	if (horz->IsWindowVisible())
		horz->SetScrollPos(x);

	if (vert->IsWindowVisible())
		vert->SetScrollPos(y);

}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	int pos, oldpos;
	SCROLLINFO info;
	float scalefactor;	// normal/fullscreen ratio 8/8 or 8/12

	pScrollBar->GetScrollInfo(&info, SIF_ALL);
	pos = info.nPos;

	switch(nSBCode)
	{
		case SB_LEFT: pos -= 1; break;
		case SB_LINELEFT: pos -= 20; break;
		case SB_LINERIGHT: pos += 20; break;
		case SB_PAGELEFT:  pos -= 20; break;
		case SB_PAGERIGHT: pos += 20; break;
		case SB_RIGHT:     pos += 1; break;
		case SB_THUMBTRACK: pos = nPos; break;
		case SB_THUMBPOSITION: pos = nPos; break;
		default: return;
	}

	// Get the Client rect
	RECT rect;
	GetClientRect(&rect);

	// Check we don't exceed boundaries
	if (pos < info.nMin)
		pos = info.nMin;
	else 
		if (pos > info.nMax)
			pos = info.nMax;

	pScrollBar->SetScrollPos(pos);

	oldpos = can->frame.cpframe.dx;
	scalefactor = ((float)(can->frame.cpframe.scale/zoomscale)/8.0);

	// Adjust for canvas scale and zoom factor
	pos = (pos*8)/(can->frame.cpframe.scale/zoomscale);
	// Reset canvas origin to move it

	int scrollamount = 0;
	if(can)
	{
		int can_cx = can->frame.width/8;
		// When we zoom we allow the canvas edge to be centered on screen 
		if (zoomscale > 1)
		{
			// Calculate the margin
			int margin = (((rect.right - rect.left)/2) * 8)/ (can->frame.cpframe.scale/zoomscale);
			// Offset the canvas to new position
			can->frame.cpframe.dx = (margin/zoomscale - pos)*8;
			scrollamount = ((can->frame.cpframe.dx - oldpos)/( 8 / zoomscale)) * scalefactor;
		}
		else // This is the canvas bigger than client area, just scroll incrementally
		{
			can->frame.cpframe.dx = -pos*8;
			// need to round up for scrollwindow to match
			//if ((can->frame.cpframe.dx - oldpos) % can->frame.cpframe.scale)
			//	can->frame.cpframe.dx += 8;

			scrollamount = (float)(((can->frame.cpframe.dx - oldpos) * can->frame.cpframe.scale)/64);
		}
	}

//	TRACE("DeltaX [%d] Scroll [%d]\n\r", (can->frame.cpframe.dx - oldpos)/8, (int)scrollamount);

	// Allow for the scroll bars
	if (vert->IsWindowVisible())
		rect.right -= SB_WIDTH;
	if (horz->IsWindowVisible())
		rect.bottom -= SB_WIDTH;

	// External event handlers
	if ((flags & DA_AUTOSCROLL) && externalEH)
		externalEH(WM_HSCROLL, (int)scrollamount, pos, client_data);
	HandleEvents(WM_HSCROLL, (int)scrollamount, pos);

	// Scroll the client area
	ScrollWindow(scrollamount, 0, &rect, NULL);

	if (scrollamount < 0)
		rect.left = rect.right + (int)scrollamount;
	else
		rect.right = rect.left + (int)scrollamount;
	InvalidateRect(&rect, TRUE);

	// Redraw update region only
	UpdateWindow();
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	int pos, oldpos;
	float scalefactor;	// normal/fullscreen ratio 8/8 or 8/12
	SCROLLINFO info;

	pScrollBar->GetScrollInfo(&info, SIF_ALL);
	pos = info.nPos;

	switch(nSBCode)
	{
		case SB_LEFT: pos -= 1; break;
		case SB_LINELEFT: pos -= 20; break;
		case SB_LINERIGHT: pos += 20; break;
		case SB_PAGELEFT:  pos -= 20; break;
		case SB_PAGERIGHT: pos += 20; break;
		case SB_RIGHT:     pos += 1; break;
		case SB_THUMBTRACK: pos = nPos; break;
		case SB_THUMBPOSITION: pos = nPos; break;
		default: return;
	}
	// Get the Client rect
	CRect rect;
	GetClientRect(&rect);

	// Check we don't exceed boundaries
	if (pos < info.nMin)
		pos = info.nMin;
	else 
		if (pos > info.nMax)
			pos = info.nMax;

	pScrollBar->SetScrollPos(pos);

	oldpos = can->frame.cpframe.dy;
	scalefactor = ((float)(can->frame.cpframe.scale/zoomscale)/8.0);
	float scrollamount = 0.0;

	// Reset canvas origin to move it
	if(can)
	{
		int can_cy = can->frame.height/8;

		// When we zoom we allow the canvas edge to be centered on screen 
		if (zoomscale > 1)
		{
			// Calculate the margin
			int margin = (((rect.bottom - rect.top)/2) * 8)/ (can->frame.cpframe.scale/zoomscale);
			// Adjust for canvas scale and zoom factor
			pos = (pos*8)/(can->frame.cpframe.scale/zoomscale);
			// Offset the canvas to new position (Note: origin flipped )
			can->frame.cpframe.dy = (margin/zoomscale - (can_cy - pos))*8;
			scrollamount = (float)((oldpos - can->frame.cpframe.dy)/( 8 / zoomscale)) * scalefactor;
		}
		else // This is the canvas bigger than client area, just scroll incrementally
		{
			int height = (can_cy * can->frame.cpframe.scale)/8;
			can->frame.cpframe.dy = - (((height - rect.Height() - pos) *64)/can->frame.cpframe.scale);
			// need to round up for scrollwindow to match
			if ((scrollamount = (float)((oldpos - can->frame.cpframe.dy) * can->frame.cpframe.scale)/64) < 0)
				scrollamount-=0.5;
			else
				scrollamount+=0.5;
		}
	}

	// External event handlers
	if ((flags & DA_AUTOSCROLL) && externalEH)
		externalEH(WM_VSCROLL, (int)scrollamount, pos, client_data);
	HandleEvents(WM_VSCROLL, (int)scrollamount, pos);

	// Allow for the scroll bars
	if (vert->IsWindowVisible())
		rect.right -= SB_WIDTH;
	if (horz->IsWindowVisible())
		rect.bottom -= SB_WIDTH;

	// Scroll the client area
	ScrollWindow(0, (int)scrollamount, &rect, NULL);

	// Calculate what we need to redraw as ScrollWindow doesn't do what we want
	if (scrollamount < 0)
		rect.top = rect.bottom + (int)scrollamount;
	else
		rect.bottom = rect.top + (int)scrollamount;
	InvalidateRect(&rect, TRUE);

	// Redraw update region only
	UpdateWindow();
}

void CDrawingArea::OffsetCanvasToMouse(int zoom, int x, int y, BOOL bMargin)
{
	RECT rect;

	// Whats the old and new scales of the canvas
	int normal_scale = can->frame.cpframe.scale;

	// Get Client area of drawing area
	GetClientRect(&rect);

	// Canvas in a non zoomed scale, so zoom
	int height = can->frame.height/8;

	// Calc W and H in same way as X and Y
	int W = (rect.right-rect.left)*8/normal_scale;
	int H = (rect.bottom-rect.top)*8/normal_scale;

	can->frame.cpframe.dx = (W/(2*zoom) - x)*8;
	can->frame.cpframe.dy = (H/(2*zoom) - (height - 1 - y))*8;

	if (!bMargin)	// Don't allow the zoom margin
	{
		if(can->frame.cpframe.dx > 0)
			can->frame.cpframe.dx = 0;
		else if (can->frame.cpframe.dx < ((rect.right-rect.left)*8) - can->frame.width)
			can->frame.cpframe.dx = ((rect.right-rect.left)*8) - can->frame.width;

		if(can->frame.cpframe.dy > 0)
			can->frame.cpframe.dy = 0;
		else if (can->frame.cpframe.dy < ((rect.bottom-rect.top)*8) - can->frame.height)
			can->frame.cpframe.dy = ((rect.bottom-rect.top)*8) - can->frame.height;

	}
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::ZoomCanvas(int zoom, int x, int y)
{
// Would someone please explain what is going on here? -- Why should I, Eh!

	if(!can)
		return;

	// Whats the old and new scales of the canvas
	int normal_scale = can->frame.cpframe.scale;

	// Need to adjust the x, y mouse position with any scrolled position
	int scrollpos_x;
	int scrollpos_y;

	GetScrollPositions(&scrollpos_x, &scrollpos_y);

	x += scrollpos_x/zoomscale;
	y += scrollpos_y/zoomscale;

	// Adjust mouse points according to scale
	int X = x*8/normal_scale;
	int Y = y*8/normal_scale;

	if (scaletofit)
	{
		OffsetCanvasToMouse(2, X, Y, FALSE);
		can->frame.cpframe.scale*=2;
		if (can->frame.cpframe.scale >= scaletofit)
		{
			can->frame.cpframe.scale = scaletofit;
			scaletofit = 0;
		}
		SetScreenForCanvas();
		Invalidate();
		return;
	}

	// Based on the zoom in minteract.c
	// ddgs coordinates used of course, so lots of 8's

	// Now modify canvas scale and add/remove scrollbars
	if (zoomscale == 1)	
	{
		RECT rect;
		SCROLLINFO scrollinfo;
		int scale = normal_scale*zoom;

		zoomscale = zoom;	

		// Get Client area of drawing area
		GetClientRect(&rect);
		// Canvas in a non zoomed scale, so zoom
		int width = can->frame.width/8;
		int height = can->frame.height/8;

		can->frame.cpframe.scale = scale;
		preZoom_dy = can->frame.cpframe.dy;
		preZoom_dx = can->frame.cpframe.dx;

		// Calc W and H in same way as X and Y
		int W = (rect.right-rect.left)*8/normal_scale;
		int H = (rect.bottom-rect.top)*8/normal_scale;

		can->frame.cpframe.dx = (W/(2*zoomscale) - X)*8;
		can->frame.cpframe.dy = (H/(2*zoomscale) - (height - 1 - Y))*8;

		// Set up scrolling for zoom
		vert->ShowScrollBar(TRUE);
		horz->ShowScrollBar(TRUE);

		scrollinfo.cbSize = sizeof(SCROLLINFO);
		scrollinfo.fMask =	SIF_PAGE |SIF_POS | SIF_RANGE | SIF_DISABLENOSCROLL;

		// HORIZONTAL
		horz->GetScrollInfo(&scrollinfo, SIF_ALL);
		scrollinfo.nMin = 0;
		scrollinfo.nPage = ((width * normal_scale)/8)/zoomscale;
		scrollinfo.nMax = (scrollinfo.nPage*zoomscale) + scrollinfo.nPage;
		preZoom_ScrollPosX = scrollinfo.nPos;
		scrollinfo.nPos = x;
		horz->SetScrollInfo(&scrollinfo);

		// VERTICAL
		vert->GetScrollInfo(&scrollinfo, SIF_ALL);
		scrollinfo.nPage = ((height*normal_scale)/8)/zoomscale;
		scrollinfo.nMax = (scrollinfo.nPage*zoomscale) + scrollinfo.nPage;
		preZoom_ScrollPosY = scrollinfo.nPos;
		scrollinfo.nPos = y;
		vert->SetScrollInfo(&scrollinfo);
	}
	else
	{
		// Back to normal scale
		can->frame.cpframe.scale = normal_scale / zoomscale;
		can->frame.cpframe.dx = preZoom_dx;
		can->frame.cpframe.dy = preZoom_dy;

		zoomscale = 1;
		SetScreenForCanvas();
	}

	// Paint handler for redraw
	Invalidate();
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::OnTimer(UINT nIDEvent) 
{
	SCROLLINFO info;
	int pos;

	// Timer function to handlde autoscrolling, Sends scroll messages
	// to the appropriate event handler (OnHScroll/OnVScroll) which do
	// the actual work
	if(nIDEvent == AUTOVSCROLL_TIMER/* && flags & DA_AUTOSCROLL*/)
	{
		if(vert->IsWindowVisible())
		{
			vert->GetScrollInfo(&info, SIF_ALL);

			if (y_direction == SB_LINERIGHT)
			{
				pos = info.nPos + 20;
				if (pos > info.nMax)
					pos = info.nMax;
			}
			else
			{
				pos = info.nPos - 20;
				if (pos < info.nMin)
					pos = info.nMin;
			}

			vert->SetScrollPos(pos);
			SendMessage(WM_VSCROLL, (y_direction)|(pos<<16), (long)vert->m_hWnd);

		}
	}		

	if(nIDEvent == AUTOHSCROLL_TIMER/* && flags & DA_AUTOSCROLL*/)
	{
		if(horz->IsWindowVisible())
		{
			 horz->GetScrollInfo(&info, SIF_ALL);

			if (x_direction == SB_LINERIGHT)
			{
				pos = info.nPos + 20;
				if (pos > info.nMax)
					pos = info.nMax;
			}
			else
			{
				pos = info.nPos - 20;
				if (pos < info.nMin)
					pos = info.nMin;
			}

			horz->SetScrollPos(pos);
			PostMessage(WM_HSCROLL, (x_direction)|(pos<<16), (long)horz->m_hWnd);
		}
	}
	
	CWnd::OnTimer(nIDEvent);
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::ActivateAutoScroll(BOOL state)
{
	if (state) 
		flags |= DA_AUTOSCROLL; // On
	else
		flags &= ~DA_AUTOSCROLL; // Off
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::CheckAutoScroll(CPoint point)
{
	// Check we need to autoscroll depening on mouse pos point
	// Start appropriate action timer and send event to external handler
	if (!(flags & DA_AUTOSCROLL))
		return;

	CRect rect;
	GetClientRect(rect);

	if (vert->IsWindowVisible())
	{
		if (point.y > (rect.bottom - rect.top))
		{
			y_direction = SB_LINERIGHT;
			if (!y_timer)
			{
				y_timer = SetTimer(AUTOVSCROLL_TIMER, AUTOSCROLL_SPEED, NULL);
				if (externalEH && !x_timer)	// Already started
					externalEH(DA_STARTAUTOSCROLL, 0, 0, client_data);
			}
		}
		else if (point.y < 0)
		{
			y_direction = SB_LINELEFT;
			if (!y_timer)
			{
				y_timer = SetTimer(AUTOVSCROLL_TIMER, AUTOSCROLL_SPEED, NULL);
				if (externalEH  && !x_timer)
					externalEH(DA_STARTAUTOSCROLL, 0, 0, client_data);
			}
		}
		else
		{
			if (y_timer)
			{
				KillTimer(AUTOVSCROLL_TIMER);
				if (externalEH  && !x_timer)	// still scrolling if x_timer exists
					externalEH(DA_STOPAUTOSCROLL, 0, 0, client_data);
				y_timer = 0;
			}
		}
	}

	if (horz->IsWindowVisible())
	{
		if (point.x > (rect.right - rect.left))
		{
			x_direction = SB_LINERIGHT;
			if (!x_timer)
			{
				x_timer = SetTimer(AUTOHSCROLL_TIMER, AUTOSCROLL_SPEED, NULL);
				if (externalEH && !y_timer)
					externalEH(DA_STARTAUTOSCROLL, 0, 0, client_data);
			}
		}
		else if (point.x < 0)
		{
			x_direction = SB_LINELEFT;
			if (!x_timer)
			{
				x_timer = SetTimer(AUTOHSCROLL_TIMER, AUTOSCROLL_SPEED, NULL);
				if (externalEH  && !y_timer)
					externalEH(DA_STARTAUTOSCROLL, 0, 0, client_data);

			}
		}
		else
		{
			if (x_timer)
			{
				KillTimer(AUTOHSCROLL_TIMER);
				if (externalEH  && !y_timer)
					externalEH(DA_STOPAUTOSCROLL, 0, 0, client_data);
				x_timer = 0;
			}
		}
	}
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::RECTToDDGS(DDGSframe *dframe, RECT *rect, int scale)
{
	// Convert the RECT coords into DDGSframe coords
 	float normal_scale = ((float)8.0/(float)scale)*(float)8.0;
	float dgg_scale = (float)scale/(float)8.0;
	
	// First converts to canvas relative coords. Exposure rectangle origin top left,
	// canvas origin bottom left.
	int h = (int)((float)((rect->bottom - rect->top) +1)*normal_scale);
	int x = (int)((float)rect->left*normal_scale);
	int y = (int)((float)(dog->maxy - (rect->top - 1))*normal_scale - (float)h);
	int w = (int)((float)((rect->right - rect->left) +1)*normal_scale);

	// then convert these to DDGSframe coords
	dframe->x= (int)((float)x*dgg_scale);
	dframe->y= (int)((float)y*dgg_scale);
	dframe->midx = (int)((float)(x + w/2)*dgg_scale);
	dframe->midy = (int)((float)(y + h/2)*dgg_scale);
	dframe->width = (int)((float)w*dgg_scale);
	dframe->height = (int)((float)h*dgg_scale);
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::ExposeArea(RECT *rect)
{
	DDGSframe dframe;
	RECT drect;
	
	GetClientRect(&drect);

	selectddgs(dog);
	intens(1);
	// If no canvas just clear the screen
	if (can == NULL)
	{
		clear();
		return;
	}

	// Otherwise show the canvas
	// Is the area we talking about the whole thing or just
	// a sub region
	if (memcmp(&rect,&drect, sizeof(RECT)))
	{
		// Sub Region
		// Covert paint RECT to DGGSframe units.
		RECTToDDGS(&dframe, rect, can->frame.cpframe.scale);
		// Clear rectangle		
		if (can->frame.drawmode == COPY)
			erase(dframe.x, dframe.y, dframe.width, dframe.height);
		else
			invert_erase(dframe.x, dframe.y, dframe.width, dframe.height);
		// Redraw any canvas intersecting the rectangle.
		expose_Canvas(can, &dframe);	
	}
	else	
	{
		// Redraw whole canvas if client area == rect (quicker)
		if (can->frame.drawmode==COPY)
			clear();
		else
			invert_clear();

		display_Canvas(can);
	}

	// Image boundary is smaller than client area, draw red frame to point this out
	if (scaletofit)
	{
		intens(1);
		lwidth(2);
		display_Canvas_frame(can, MARKIM, RED, COPY);
		lwidth(1);
	}
}
//----------------------------------------------------------------------------------
//                        Canvas Drawing Stuff - Finish
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//                        Custom Menu Items - Start
//----------------------------------------------------------------------------------
void CDrawingArea::ActivateMenu(BOOL state, BOOL bSelcan)
{
	// Enable/Disable the menu
	if (state)
	{
		if ((flags & DA_MENU) == 0)
				flags |= DA_MENU;
	}
	else
	{
		if (flags & DA_MENU)
			flags &= ~DA_MENU;
	}
	// Select if menu appears if a canvas is underneath
	if (bSelcan)
	{
		if ((flags & DA_SELCANVAS) == 0)
				flags |= DA_SELCANVAS;
	}
	else
	{
		if (flags & DA_SELCANVAS)
			flags &= ~DA_SELCANVAS;
	}
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::ClearCustomMenu()
{
	// Clear these
	if(custommenu)
	{
		custommenu->DestroyMenu();
		delete custommenu;
		custommenu = NULL;
	}
	CustomMenuEH = NULL;
	nCustomItems = 0;
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::AddCustomMenuItem(char *text)
{
	// Create menu if required
	if(!custommenu)
	{
		if(!(custommenu = new CMenu()))
			return;

		if(!custommenu->CreatePopupMenu())
		{
			delete custommenu;
			custommenu = NULL;
			return;
		}
	}

	if(strlen(text))
	{
		// Assign ID of item (first + number of items)
		UINT Id = IDC_CUSTOMMENU + nCustomItems;
		// Record number of items as can't get menu to tell me
		// FYI: GetMenuItemCount() will give the number of items in the specified menu. 
		nCustomItems++;
		// Append it to the menu
		custommenu->InsertMenu(nCustomItems, MF_BYPOSITION|MF_STRING, Id, text);	
	}
	else	// Add seperator
		custommenu->InsertMenu(nCustomItems, MF_BYPOSITION|MF_SEPARATOR, 0, "");	

	
}

//--------------------------------------------------------------------------------------------------
//
//	void CDrawingArea::AddCustomMenuItemID - Insert menu item based on resource ID
//	Note: Pass 0, to insert a separator
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::AddCustomMenuItemID(UINT id)
{
	char str[40];
	int size;

	if (id) {
		size = sizeof(str);
// comment out for DLL version		if (GetResourceStr(id, str, size))
			AddCustomMenuItem(str);
		}
	else if (custommenu)
			custommenu->InsertMenu(nCustomItems, MF_BYPOSITION|MF_SEPARATOR, 0, "");

}

//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::AttachCustomMenuEH(void (*func)(UINT index))
{
	CustomMenuEH = func;
}

void  CDrawingArea::OnCustomMenu(UINT nID)
{
	// Called the attached event handler with 0 based index of
	// custom item (order in  which they were added
	if (CustomMenuEH)
		CustomMenuEH(nID - IDC_CUSTOMMENU);
}
//----------------------------------------------------------------------------------
//                        Custom Menu Items - Finish
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//						  Event handler stuff
//----------------------------------------------------------------------------------
void CDrawingArea::AttachEventHandler(void (*func)(UINT event, long x, long y, DWORD ddata), DWORD data, WORD action)
{
	// action can be DA_INSERT, DA_DELETE, DA_APPEND
	eventhandler_item *item;

	switch(action)
	{
	case DA_INSERT_EH: // Insert handler function at start
		item = new eventhandler_item;
		item ->funcEH = func;
		item->data = data;
		eventhandlers.AddHead(item);
		break;
	case DA_APPEND_EH:	// Append handler function at end
		item = new eventhandler_item;
		item ->funcEH = func;
		item->data = data;
		eventhandlers.AddTail(item);
		break;
	case DA_DELETE_EH: // delete handler function 
		POSITION pos;
		for( pos = eventhandlers.GetHeadPosition(); pos != NULL; )
		{
			item = eventhandlers.GetAt( pos );
			if (item->funcEH == func)
			{
				delete item;
				eventhandlers.RemoveAt( pos );
				break;
			}
			eventhandlers.GetNext( pos );
		}

		break;
	default:
		return;
	}
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::HandleEvents(UINT event, long x, long y)
{
	// Propogate event through list of event handlers
	POSITION pos;
	eventhandler_item *item;

	for( pos = eventhandlers.GetHeadPosition(); pos != NULL; )
	{
		item = eventhandlers.GetNext( pos );
		if (item)
			item->funcEH(event, x, y, item->data);
	}
}
//----------------------------------------------------------------------------------
//						  END - Event handler stuff
//----------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::CopyArea(RECT *srect, RECT *drect)
{
		int width,height;
		RECT rect;

		GetClientRect(&rect);

		selectddgs(dog);
		intens(1);

			// If no canvas just clear the screen
		if (can == NULL) {
				clear();
				return;
			}

		CDC *dc = GetDC();
	
						//keep everything inside the window
		if (srect->bottom > rect.bottom)
				srect->bottom = rect.bottom;
		if (srect->top < rect.top)
				srect->top = rect.top;
		if (srect->right > rect.right)
				srect->right = rect.right;
		if (srect->left < rect.left)
				srect->left = rect.left;
		if (drect->left < rect.left)
				drect->left = rect.left;
		if (drect->right > rect.right)
				drect->right = rect.right;

		width = srect->right - srect->left;
		height = srect->bottom - srect->top;

						//copy area from the source region to the destination
		dc->BitBlt (drect->left,drect->top,width,height,dc,srect->left,srect->top,SRCCOPY);
		
		 ReleaseDC(dc);
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawingArea::ClearArea(RECT *srect)
{
		int width,height;
		RECT rect;

		GetClientRect(&rect);

		selectddgs(dog);
		intens(1);

			// If no canvas just clear the screen
		if (can == NULL) {
				clear();
				return;
			}

		CDC *dc = GetDC();
	
						//keep everything inside the window
		if (srect->bottom > rect.bottom)
				srect->bottom = rect.bottom;
		if (srect->top < rect.top)
				srect->top = rect.top;
		if (srect->right > rect.right)
				srect->right = rect.right;
		if (srect->left < rect.left)
				srect->left = rect.left;
	
		width = srect->right - srect->left;
		height = srect->bottom - srect->top;

						//clear the area
		dc->BitBlt (srect->left,srect->top,width,height,dc,srect->left,srect->top,WHITENESS);
		
		ReleaseDC (dc);
}
//--------------------------------------------------------------------------------------------------
// 
//--------------------------------------------------------------------------------------------------
void CDrawingArea::ScaleToFitInside() 
{
	if (can)
	{
		// whats the normal scale, we need to remember
		if (!scaletofit)
			scaletofit = can->frame.cpframe.scale/zoomscale;

//		zoomscale = 1;	// Normal zoom doesn't apply
//
// zoomscale = 1, means next zoom op. is 'zoom up'.
// If normal image zoomed, before 'Scale to fit' op. then
// if zoom again, it will zoom again which is wrong for
// normal images. MC Nov 99
//
		preZoom_dx = preZoom_dy = preZoom_ScrollPosX = preZoom_ScrollPosY = 0;

		// Ok, whats going to fit inside the display
		RECT rect;
		GetClientRect(&rect);

		// Width
		int len = (rect.right - rect.left) *8;
		int w_factor = can->frame.width / len;

		if (can->frame.width % len)
			w_factor++;

		// Height
		len = (rect.bottom - rect.top) *8;
		int h_factor = can->frame.height / len;

		if (can->frame.height % len)
			h_factor++;

		int newscale;

		if (h_factor > w_factor)
			newscale =  scaletofit / h_factor;
		else
			newscale =  scaletofit / w_factor;

		// Best fit already
		if (newscale == scaletofit)
		{
			scaletofit =0;
			return;
		}

		// Only certain scales are useful
		zoomscale = 1; // do here now MC Nov99

		can->frame.cpframe.scale = newscale;
		can->frame.cpframe.dx = 0;
		can->frame.cpframe.dy = 0;

		SetScreenForCanvas();
		Invalidate();
	}
}
