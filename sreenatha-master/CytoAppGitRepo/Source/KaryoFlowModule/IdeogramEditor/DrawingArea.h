//
// DrawingArea.h
//


#ifndef	_DRAWINGAREA_H
#define	_DRAWINGAREA_H

#include "ddgs.h"
#include "canvas.h"
//#include "ddgs.h"
//#include "canvas.h"
//#include "common.h"
#include "DragDrop.h"	// Drag And Drop
#include <afxtempl.h>

// Event handler insertion
#define DA_INSERT_EH		0
#define DA_APPEND_EH		1
#define DA_DELETE_EH		2

// Pre and post autoscroll user events
#define DA_STARTAUTOSCROLL	WM_USER +1
#define DA_STOPAUTOSCROLL	WM_USER +2

// Event handler typedef
typedef void (*EH)(UINT event, long x, long y, DWORD data);

// Item for eventhandler list
typedef struct
{
	void (*funcEH)(UINT event, long x, long y, DWORD data);
	DWORD data;
} eventhandler_item;


class CDrawingArea : public CWnd
{
// Construction
public:
	CDrawingArea(int set_index = 0);

// Attributes
public:
	DDGS *dog;
	Canvas *can;
	Cselectdata *box;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDrawingArea)
	public:
	virtual BOOL Create(LPCTSTR lpszWindowName, short x, short y, short w, short h, CWnd* pParentWnd);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CDrawingArea();

	void SetColor(BOOL is_black = FALSE);
	void SetCanvas(Canvas *new_can = NULL);
	void SetSelector(Cselectdata *new_box = NULL);
	//void (*SetEventHandler(void (*func)(UINT event, long x, long y, DWORD data) = NULL, DWORD data = 0)) (UINT event, long x, long y, DWORD data);
	void SetEventHandler(void (*func)(UINT event, long x, long y, DWORD data) = NULL, DWORD data = 0);
	void AttachEventHandler(void (*func)(UINT event, long x, long y, DWORD ddata), DWORD data, WORD action);
	EH CDrawingArea::GetEventHandler();

	void ActivateSelector(BOOL state = FALSE);
	void ActivateZoom(BOOL state = FALSE, BOOL bHide = FALSE, BOOL bReset = FALSE);
	void ActivateMenu(BOOL state = FALSE, BOOL bSelcan = FALSE);
	void ActivateManClass(BOOL state = FALSE, void (*func)(UINT index) = NULL);
	void ActivateNormalScroll( BOOL state ) { m_bScrollEnabled = state; SetScreenForCanvas();};
	void ActivateAutoScroll(BOOL state);
	void GetScrollOffsets(int *x, int *y);
	void GetScrollPositions(int *x, int *y);	
	void SetScrollPositions(int x, int y);
	void ScaleToFitInside();

	void ConstrainMouse(BOOL state = FALSE);
	void TrackMouse();

	// Drag and Drop Public functions
	void SetDropSource(BOOL source, void (*copyfunc)(DWORD parm) = NULL, DWORD data = 0);
	void SetDropTarget(BOOL target, void (*pastefunc)(DWORD parm) = NULL, DWORD data = 0, WORD format = CF_CYTOVISION);
	// Custom Context Menu Public functions
	void ClearCustomMenu();
	void AddCustomMenuItem(char *text);
	void AddCustomMenuItemID(UINT id);
	void AttachCustomMenuEH(void (*func)(UINT index));
	// Misc.
	void Pseudocolour() ;
	void SetZoomFactor(int factor);
	void ResetToDefault();
			
	void ExposeArea(RECT *rect);
	void CopyArea (RECT *srect, RECT *drect);
	void ClearArea (RECT *srect);

private:
	void (*externalEH)(UINT event, long x, long y, DWORD data);
	DWORD client_data;
	CMenu *custommenu;					// Context Menu
	void (*CustomMenuEH)(UINT index);	// Custom menu items event handler
	int nCustomItems;					// Number of custom menu items
	int index;							// My DA identifier ("box" number).
	UINT flags;							// State of various options (see DA_XXX)
	BOOL bLeft;							// Cursor has left window
	BOOL m_bScrollEnabled;				// Is scrolling in normal scale allowed

	// Canvas handling stuff
	void OffsetCanvasToMouse(int zoom, int x, int y, BOOL bMargin);
	void ZoomCanvas(int zoom, int x, int y);
	void RECTToDDGS(DDGSframe *dframe, RECT *rect, int scale);
	void SetScreenForCanvas();

	// Scroll
	CScrollBar * vert;
	CScrollBar * horz;
	void CheckAutoScroll(CPoint point);
	UINT x_direction;	// Directions for autoscroll
	UINT x_timer;
	INT y_direction;
	INT y_timer;

	// Drag and Drop stuff
    LPDROPSOURCE pDropSource; 
    LPDROPTARGET pDropTarget; 
	LPDATAOBJECT pDataObject; 
	void StartDragging();
	int m_drag;	

	// Zoom
	int zoomscale;
	int zoomfactor;
	int preZoom_dx;
	int preZoom_dy;
	int preZoom_ScrollPosX;
	int preZoom_ScrollPosY;
	int scaletofit;
	// End drag and drop stuff

	// Event handling stuff
	CTypedPtrList <CPtrList, eventhandler_item * >eventhandlers;
	void HandleEvents(UINT event, long x, long y);

	// Generated message map functions
protected:
	//{{AFX_MSG(CDrawingArea)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnDestroy();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	afx_msg void OnCaptureChanged(CWnd* pWnd);
	afx_msg void OnCustomMenu(UINT nID);
	DECLARE_MESSAGE_MAP()
};


#endif
