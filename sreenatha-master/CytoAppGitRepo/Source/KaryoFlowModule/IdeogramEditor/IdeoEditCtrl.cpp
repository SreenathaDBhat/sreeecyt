// IdeoEditCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "IdeogramEditor.h"
#include "IdeogramEditorDlg.h"
#include "IdeoEditCtrl.h"

#include "NewSetWiz.h"
#include "SetLoadDlg.h"
#include "IdeoSetPropertiesDlg.h"
#include "IdeoPropertiesDlg.h"
#include "BandPropertiesDlg.h"
#include "NewBandDlg.h"
#include "SetSaveDlg.h"

#include "TemplateKaryotype.h"

#include <assert.h>
#include <float.h>

#include <io.h>          // For _sopen() and _locking().
#include <fcntl.h>       // For _sopen().
#include <share.h>       // For _sopen().
#include <sys/stat.h>    // For _sopen().
#include <sys/locking.h> // For _locking().



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



#define	KEYPATH				"Software\\Applied Imaging\\IdeogramEditor"

#define IDEOWIDTH_DEFAULT   30.0f
#define BANDLENGTH_DEFAULT  10.0f 




static 
int
Lock(const char filename[])
{
// Create a lock file whose name is based on the given filename.
// If the file is already locked, the function fails. The function does not
// fail if the file already exists but is not locked, which means that there
// will be no problem if the file is on remote machine which crashes before it
// is removed. When the machine is re-booted, the file will still be there, but
// it will not be locked.
// One advantage of having a separate lockfile rather than locking the data
// file ('filename') itself is that programs which just want to open the data
// file for reading will not be affected by whether someone has opened the file
// in the editor. For example we want people to be able to see ideograms in
// templates, multicell etc. even if someone has them open in an editor. This
// is particularly important for the default human ideograms.
	int lockfileHandle = -1;

	CString lockfileName;
	lockfileName.Format("%s.lock", filename);
	

	// Open the file to be locked.
	lockfileHandle = _sopen((LPCSTR)lockfileName, 
	            // Create the file if it does't exist already. If it does
	            // exist, this will not truncate it. Open it for reading only,
				// as we don't need to write (or read) to it, just lock it.
	            // Create it as a temporary file so that it will be deleted
				// when closed.
	                        _O_CREAT | _O_RDONLY | _O_TEMPORARY,
	            // Allow file to be opened simultaneously elsewhere.
	                        _SH_DENYNO, 
	            // If the file did not already exist, create it with these
	            // permissions.
	                        _S_IREAD | _S_IWRITE);

	if (lockfileHandle != -1)
	{
		// Lock some bytes.
		if (_locking(lockfileHandle, LK_NBLCK, 30L) == -1)
		{
			_close(lockfileHandle);
			lockfileHandle = -1;
		}
	}


	return lockfileHandle;
}

static
BOOL
Unlock(int *pLockfileHandle)
{
	BOOL status = FALSE;

	if (*pLockfileHandle != -1)
	{
		// Must unlock exactly same bytes that were locked.
		if (_locking(*pLockfileHandle, LK_UNLCK, 30L) != -1)
		{
			status = TRUE;
			_close(*pLockfileHandle);
			*pLockfileHandle = -1;
		}
	}

	return status;
}

/*
static
BOOL
IsLocked(const char filename[])
{
// This function checks to see if a file has an associated lock file which is
// locked, without actually trying to leave it locked.
	BOOL isLocked = FALSE;

	CString lockfileName;
	lockfileName.Format("%s.lock", filename);
	

	// Open the file to be locked. Just try to open it for reading, if possible.
	int f = _sopen((LPCSTR)lockfileName, _O_RDONLY, _SH_DENYNO);

	if (f != -1)
	{
		// Try to lock the same bytes as in Lock().
		if (_locking(f, LK_NBLCK, 30L) != -1)
			// Lock worked, but release it as we were only checking.
			// Must unlock exactly same bytes that were locked.
			_locking(f, LK_UNLCK, 30L);
		else 
			// File exists and could not be locked: must already be locked.	
			isLocked = TRUE;

		_close(f);
	}

	return isLocked;
}
*/




/////////////////////////////////////////////////////////////////////////////
// CIdeoEditCtrl

CIdeoEditCtrl::CIdeoEditCtrl()
{
	pSet        = NULL;
	pPolySet    = NULL;
	pIdeoCanvas = NULL;

	pCurrIdeo   = NULL;

	fileName = "";
	setName  = "";

	scale = 1.0;

	readOnly = TRUE; // Safe default!
	modified = FALSE;

	lockfileHandle = -1; // Default is an invalid handle.
}


int CIdeoEditCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDrawingArea::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here

	// Automatically load last loaded set from registry settings.
	char loadFileName[MAX_PATH] = "";
	BOOL loadReadOnly           = TRUE; // Safe default.
	float loadScale             = 1.0f;
	char loadIdeoID[16]         = "";

	HKEY key;
	if (RegOpenKeyEx(HKEY_CURRENT_USER, KEYPATH, 0,
	                 KEY_READ, &key) == ERROR_SUCCESS)
	{
		DWORD type, size, val;
		char scaleStr[32] = "";

		RegQueryValueEx(key, "fileName", NULL, &type, NULL, &size);
		if (type == REG_SZ && size < sizeof(loadFileName))
		{
			RegQueryValueEx(key, "fileName", NULL, NULL,
			                (BYTE*)loadFileName, &size);
		}

		RegQueryValueEx(key, "readOnly", NULL, &type, NULL, &size);
		if (type == REG_DWORD && size == sizeof(DWORD))
		{
			RegQueryValueEx(key, "readOnly", NULL, NULL,
			                (BYTE*)&val, &size);
			if (val) loadReadOnly = TRUE;
			else     loadReadOnly = FALSE;
		}

		RegQueryValueEx(key, "scale", NULL, &type, NULL, &size);
		if (type == REG_SZ && size < sizeof(scaleStr))
		{
			RegQueryValueEx(key, "scale", NULL, NULL,
			                (BYTE*)scaleStr, &size);
			sscanf(scaleStr, "%f", &loadScale);
		}

		RegQueryValueEx(key, "currIdeoID", NULL, &type, NULL, &size);
		if (type == REG_SZ && size < sizeof(loadIdeoID))
		{
			RegQueryValueEx(key, "currIdeoID", NULL, NULL,
			                (BYTE*)loadIdeoID, &size);
		}

		RegCloseKey(key);
	}
	else
	{
		strcat(loadFileName, "Human400G.ideoset");
		loadReadOnly = TRUE;
		loadScale    = 4.0f;
		strcpy(loadIdeoID, "1");
	}



	if (LoadSetFile(loadFileName, loadReadOnly) == IDYES)
	{
		scale = loadScale;
		if (pSet && pSet->pIdeogramList)
		{
			if (strlen(loadIdeoID) < 1 || SelectIdeogram(loadIdeoID) == NULL)
			{
			// If no ID saved, or no ideogram with this ID in set,
			// just load the first in the list.
			// The latter would happen if user created a new ideogram just
			// before exiting, but did not save it, for example.
				SelectIdeogram(pSet->pIdeogramList->id);
			}
		}
	}

	
	return 0;
}




CIdeoEditCtrl::~CIdeoEditCtrl()
{
	// Save the current settings to be loaded on next startup.
	if (pSet)
	{	
		HKEY key;

		if (RegCreateKeyEx(HKEY_CURRENT_USER, KEYPATH, 0, NULL,
		                   REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL,
		                   &key, NULL) == ERROR_SUCCESS)
		{
			RegSetValueEx(key, "fileName", 0, REG_SZ,
			                   (CONST BYTE *)((LPCSTR)fileName),
			                   fileName.GetLength() + 1);

			// Must save read-only flag as that was got from the
			// CTemplateFinder control, which will not be used when
			// auto-loading this file on next startup.
			DWORD val = readOnly;
			RegSetValueEx(key, "readOnly", 0, REG_DWORD,
				          (CONST BYTE *)&val, sizeof(val));

			CString scaleString;
			scaleString.Format("%f", scale);
			RegSetValueEx(key, "scale", 0, REG_SZ,
			                   (CONST BYTE *)((LPCSTR)scaleString),
			                   scaleString.GetLength() + 1);

			if (pCurrIdeo)
			{
				pCurrIdeo->id;
				RegSetValueEx(key, "currIdeoID", 0, REG_SZ,
				                   (CONST BYTE *)pCurrIdeo->id,
				                   strlen(pCurrIdeo->id) + 1);
			}

			RegCloseKey(key);
		}
	}


// Clean up.
	if (pSet != NULL) delete pSet;
	if (pPolySet != NULL) delete pPolySet;
	if (pIdeoCanvas != NULL) destroy_Canvas_and_Wobj(pIdeoCanvas);

	Unlock(&lockfileHandle);
}


BEGIN_MESSAGE_MAP(CIdeoEditCtrl, CDrawingArea)
	//{{AFX_MSG_MAP(CIdeoEditCtrl)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CIdeoEditCtrl message handlers

void
CIdeoEditCtrl::ApplyScale(float s)
{
	scale = s;

	UpdateCanvas();
	Invalidate();
	UpdateWindow();
}


/****************************************************************************
 *  Operations at Ideogram Set level.
 */


int
CIdeoEditCtrl::NewSet()
{
// Returning IDCANCEL tells the calling function to do nothing,
// as the operation was cancelled.
// Returning IDYES or IDNO tells the calling function whether or not this
// function managed to successfully create a new set.
	int status = IDCANCEL;


	CString species;	// Used to pass info from page1 to page2
	CPropertySheet wizard;
	CNewSetWiz1 page1(&species);
	CNewSetWiz2 page2(&species);
	wizard.AddPage(&page1);
	wizard.AddPage(&page2);
	wizard.SetWizardMode();

	if (wizard.DoModal() == ID_WIZFINISH)
	{
		if (SaveIfModifiedOrCancel() != IDCANCEL)
		{
			// Possibilities to cancel have passed, now re-initialise status
			// to failure status: only changed if function succeeeds.
			status = IDNO;

			// Free the previously loaded set, if any.
			if (pSet != NULL) delete pSet;
			pSet      = NULL;
			fileName  = "";
			pCurrIdeo = NULL;
			modified  = FALSE;

			// Unlock the previous set, if any, if locked.
			Unlock(&lockfileHandle);


			CString pathFileName;

			// Want full path name to new ideogram file
			pathFileName =   page2.m_tree.TemplateDirectory + "\\" + species + "\\"
			               + page2.setName + IDEOSET_EXT;


			// Strip off file name to get directory path
			CString path = pathFileName;
			int index = path.ReverseFind('\\');

			path = path.Left(index);


			// Try to create lock file before proceeding: if can't, someone
			// else has this set open already so mustn't overwrite it.
			lockfileHandle = Lock(pathFileName);
			if (lockfileHandle != -1)
			{
				// If file already exists it will be truncated when opened for writing,
				// but user should already have been warned.
				// If file did not exist, creating it immediately will prevent someone
				// else from creating one with the same name (it will appear in their
				// list of current sets).
				FILE *pFile;
				if ((pFile = fopen(pathFileName, "w")) != NULL)
				{
					if ((pSet = new IdeogramSet(species, "", "", IDEOWIDTH_DEFAULT)) != NULL)
					{
						status = IDYES;
					}

					// Must write data, even if it's just one line, 
					// else the file won't be a valid IBF file.
					pSet->WriteIBFFile(pFile);
					fclose(pFile);
	
					fileName = pathFileName; // Assign to member variable

					readOnly = FALSE; // Can't be read only - just created it!
				}
			}
			else AfxMessageBox("Existing set is locked: cannot overwrite!");


			// Refresh display whether or not failed, 
			// as previous set has been deleted.
			UpdateCanvas();
			Invalidate();
			UpdateWindow();
		}
	}

	return status;
}


int 
CIdeoEditCtrl::LoadSetFile(const char pathFileName[], BOOL openReadOnly)
{
// Returning IDCANCEL tells the calling function to do nothing,
// as the operation was cancelled.
// Returning IDYES or IDNO tells the calling function whether or not this
// function managed to successfully load a new set.
	int status = IDCANCEL;


	if (SaveIfModifiedOrCancel() != IDCANCEL)
	{
		// Free the previously loaded set, if any.
		if (pSet != NULL) delete pSet;
		pSet      = NULL;
		fileName  = "";
		pCurrIdeo = NULL;
		modified  = FALSE;

		// Unlock the previous set, if any, if locked.
		Unlock(&lockfileHandle);


		// If the file is only to be opened for reading, don't lock it, so
		// that other people can open it simultaneously.
		BOOL lockFailed = FALSE;
		if (!openReadOnly) 
		{
			lockfileHandle = Lock(pathFileName);

			if (lockfileHandle == -1)
			{
				if (AfxMessageBox("Ideogram set is locked!\n"
				                  "Do you want to open it as read-only?", 
				                   MB_OKCANCEL) != IDOK) 
					lockFailed = TRUE;
				else
					openReadOnly = TRUE;
			}
		}

		if (!lockFailed)
		{
			// Possibilities to cancel have passed, now re-initialise status
			// to failure status: only changed if function succeeeds.
			status = IDNO;


			FILE *pFile;
			if ((pFile = fopen(pathFileName, "r")) != NULL)
			{
				if ((pSet = new IdeogramSet) != NULL)
				{
					if (pSet->ReadIBFFile(pFile)) 
					{
						status = IDYES;
						fileName = pathFileName; // Assign to member variable
						readOnly = openReadOnly;
					}
					else 
					{
						delete pSet;
						pSet = NULL;
					}
				}

				fclose(pFile);
			}

			if (status != IDYES) AfxMessageBox("Failed to load ideogram set!");
		}


		// Refresh display whether or not load failed, 
		// as previous set has been deleted.
		UpdateCanvas();
		Invalidate();//RedrawWindow(); // Invalidate window
		UpdateWindow(); // Update window
	}


	return status;
}


int
CIdeoEditCtrl::LoadSet(BOOL humanOnly /*=FALSE*/)
{
	BOOL status = IDCANCEL;


	CSetLoadDlg dlg(humanOnly);

	if (dlg.DoModal() == IDOK)
	{	
		status = LoadSetFile(dlg.ideosetName, dlg.readOnly);
	}


	return status;
}


void
CIdeoEditCtrl::EditSetProperties()
{
	if (pSet != NULL)
	{
		CIdeoSetPropertiesDlg dlg;	

		dlg.m_species     = pSet->species;
		dlg.m_resolution  = pSet->resolution;
		dlg.m_bandingtype = pSet->bandtype;
		dlg.m_width       = pSet->width;

		if (dlg.DoModal() == IDOK)
		{
			// TODO: Must not modify species once set is created, 
			// as this determines the directory it is saved in.
			pSet->SetProperties((LPCSTR)dlg.m_species, 
			                    (LPCSTR)dlg.m_resolution,
			                    (LPCSTR)dlg.m_bandingtype,
			                    dlg.m_width);

			modified = TRUE;

			UpdateCanvas();
			Invalidate();
			UpdateWindow();
		}	
	}

}


BOOL 
CIdeoEditCtrl::SaveSet()
{
	BOOL status = FALSE;


	if (pSet)
	{
		CSetSaveDlg dlg(readOnly);

		if (dlg.DoModal() == IDOK)
		{
			if (dlg.saveAs)
			{
				// Save as the new species and name specified
				CString species;	// Used to pass info from page1 to page2

				// Initialise species to same as current.
				species = pSet->species;

				CPropertySheet wizard;
				CNewSetWiz1 page1(&species);
				CNewSetWiz2 page2(&species);
				wizard.AddPage(&page1);
				wizard.AddPage(&page2);
				wizard.SetWizardMode();

				if (wizard.DoModal() == ID_WIZFINISH)
				{
					CString pathFileName;

					// Want full path name to new ideogram file
					pathFileName =   page2.m_tree.TemplateDirectory + "\\" + species + "\\"
			                       + page2.setName + IDEOSET_EXT;


					// Strip off file name to get directory path
					CString path = pathFileName;
					int index = path.ReverseFind('\\');

					path = path.Left(index);

					// See if we can create a lock for the new set file.
					int newLockHandle = Lock(pathFileName);
					if (newLockHandle != -1)
					{
						// Try to save set to new file.
						FILE *pFile;
						if ((pFile = fopen(pathFileName, "w")) != NULL)
						{
							if (pSet->WriteIBFFile(pFile)) status = TRUE;

							int closeVal = fclose(pFile);

							if (closeVal != 0) status = FALSE;

							if (status)
							{
								// Set was written to new file okay. Now assign
								// class parameters relating to the set file to
								// the new file.

								// Unlock the old file now we're sure the new
								// file has been created successfully, and
								// assign the handle for the new file's
								// lockfile to this->lockfileHandle.
								Unlock(&lockfileHandle);
								lockfileHandle = newLockHandle;

								// Assign new filename to member variable.
								fileName = pathFileName;

								// Can't be read only - just created it!
								readOnly = FALSE; 

								// Reset modified flag: future modifications
								// are relative to this save.
								modified = FALSE;
							}
						}						

						if (!status) AfxMessageBox("Error saving to file!");
					}
					else AfxMessageBox("Set is locked: cannot overwrite!");
				}
			}
			else // Save to the current file
			{
				// Save button should have been disabled when this set was loaded if
				// it is read-only, but check just to be sure.
				if (readOnly) 
				{
					AfxMessageBox("Can't save - read only (should never see this message!)");
					return status;
				}


				//if (AfxMessageBox("Write to file?", MB_OKCANCEL) != IDOK) return status;


				FILE *pFile;
				if ((pFile = fopen((PCSTR)fileName, "w")) != NULL)
				{
					// If set is written to file okay, status is okay,
					// unless fclose() fails.
					if (pSet->WriteIBFFile(pFile)) status = TRUE;

					int closeVal = fclose(pFile);

					if (closeVal != 0) status = FALSE;

					if (status)
						// Reset modified flag: future modifications
						// are relative to this save.
						modified = FALSE;
				}

				if (!status) AfxMessageBox("Error saving to file!");
			}
		}
	}


	return status;
}


const char * CIdeoEditCtrl::GetSetName()
{
// Set name is just its filename (without path).

	// Extract string remaining after last path delimiter.
	int index = fileName.ReverseFind('\\');
	if (index > -1) setName = fileName.Right(fileName.GetLength() - index - 1);
	else            setName = fileName;

	// Do it for forward slashes too.
	index = setName.ReverseFind('/');
	if (index > -1) setName = setName.Right(setName.GetLength() - index - 1);

	// Remove file extension
	index = setName.ReverseFind('.');
	if (index > -1) setName = setName.Left(index);


	return (LPCSTR)setName;

/*
	static char name[IBF_STR_MAXLEN * 3];

	name[0] = '\0';

	if (pSet) sprintf(name, "%s %s %s", 
	                        pSet->species, pSet->resolution, pSet->bandtype);

	return name;
*/
}


int CIdeoEditCtrl::SaveIfModifiedOrCancel()
{
// Returning IDCANCEL tells the calling function to cancel what it is doing.
// Returning IDYES or IDNO just tells the calling function whether or not this
// function saved the current set.
	int id = IDCANCEL;
	
	if (readOnly) 
	{
		id = IDNO; // Not allowed to save anyway if marked read-only.
	}
	else if (modified)
	{
		CString mesg;
		mesg.Format("Save your changes to %s?", GetSetName());

		if ((id = AfxMessageBox(mesg, MB_YESNOCANCEL)) == IDYES) 
			SaveSet();
	}
	else
	{	// Not readonly, but not modified, so no, didn't need saving. 
		id = IDNO;
	}

	return id;
}


BOOL CIdeoEditCtrl::DeleteSet()
{
	BOOL status = FALSE;


	if (pSet && !readOnly)
	{
		if (AfxMessageBox("Are you sure you want to remove the currently loaded "
		                  "set\nfrom the editor and permanently delete its data file?\n"
		                  "Warning: this operation cannot be undone.", MB_YESNO) == IDYES)
		{
			int numIdeo = 0, numBands = 0;
			Ideogram *pIdeo = pSet->pIdeogramList;
			while (pIdeo != NULL)
			{
				Band *pBand = pIdeo->pBandList;
				while (pBand != NULL)
				{
					numBands++;
					pBand = pBand->pNextBand;
				}
				numIdeo++;
				pIdeo = pIdeo->pNextIdeogram;
			}


			CString mesg;
			mesg.Format("Delete set '%s' (contains %d ideogram(s), %d bands)?",
			            GetSetName(), numIdeo, numBands);

			if (AfxMessageBox(mesg, MB_OKCANCEL) == IDOK)
			{
				if (pSet != NULL) delete pSet;
				pSet      = NULL;
				pCurrIdeo = NULL;
				modified  = FALSE;


				DeleteFile(fileName);


				fileName  = "";
				Unlock(&lockfileHandle);


				UpdateCanvas();
				Invalidate();
				UpdateWindow();

				status = TRUE; // TRUE means operation was not cancelled.
			}
		}
	}

	return status;
}



/****************************************************************************
 *  Operations at Ideogram level.
 */


void
CIdeoEditCtrl::UpdateCanvas()
{
// Call this when something about the current ideogram has changed.
// Note that it also copies the current ideogram to the clipboard.

	if (pCurrIdeo != NULL)
	{
		if (pPolySet != NULL) delete pPolySet;
	
// TODO: Scale should be such that the largest ideogram in the set just fits the edit window.
		pPolySet = new IdeoPolySet(pCurrIdeo, pSet->width, /*4.0*/scale);

		if (pPolySet != NULL)
		{
			int polyBufferSize;
			void *pPolyBuffer = pPolySet->CreateBuffer(&polyBufferSize);

			if (pPolyBuffer != NULL)
			{
				// makeplist is in woolz
				struct propertylist pl;
				// pl.size is only a SHORT!
				// TODO: check we are nowhere near exceeding this: if we are, 
				// try reducing size of variables in IdeoPoly: 
if (polyBufferSize > 16384) AfxMessageBox("Warning: buffer size greater than 16384!");
				// e.g. use shorts rather than ints.
				assert(polyBufferSize + sizeof(pl.size) <= SHRT_MAX);

				struct propertylist *pData = makeplist(polyBufferSize + sizeof(pl.size));


				// Copy buffer to property list.
				// HAVE TO CAST pDATA TO CHAR* here, else structure packing (?)
				// means that 4 bytes are added to pData rather than 2 bytes as
				// might be expected (sizeof(pData->size) is 2). Not sure if
				// this is the best way to handle this: look at other WinCV
				// code that uses property lists.
				memcpy(((char*)pData) + sizeof(pData->size), 
				       pPolyBuffer, polyBufferSize);

				// Can now free buffer
				free(pPolyBuffer);


				// Attach ideogram canvas to DA canvas
//				can->patch = CreateIdeogramCanvas(pData);

				// Note destroy_Canvas_and_Wobj() also destroys property list.
				if (pIdeoCanvas != NULL) destroy_Canvas_and_Wobj(pIdeoCanvas);

				pIdeoCanvas = CreateIdeogramCanvas(pData);


				if (pIdeoCanvas != NULL)
				{
					// Get control dimensions in DDGS scale.
					RECT rect;
					GetWindowRect(&rect);
					int windowWidth  = (rect.right - rect.left) * 8;
					int windowHeight = (rect.bottom - rect.top) * 8;


					// Centre ideogram canvas in drawing area.
					pIdeoCanvas->frame.cpframe.dx = 
					               (windowWidth  - pIdeoCanvas->frame.width)  / 2;
					pIdeoCanvas->frame.cpframe.dy = 
					               (windowHeight - pIdeoCanvas->frame.height) / 2;


					// Attach canvas to drawing area.
					SetCanvas(pIdeoCanvas);


					// Copy buffer to clipboard, for other apps.
					if (OpenClipboard())
					{
						EmptyClipboard();

						HANDLE hData = GlobalAlloc(GMEM_MOVEABLE | GMEM_DDESHARE, pData->size);
						void *pClipboardData = GlobalLock(hData);

						
						memcpy(pClipboardData, pData, pData->size);
/*
Don't scale down - user should get exactly what is in the editor.
If they want it smaller they must csale it down before (or after) pasting it into WinCV.
						// Scale down ideogram in clipboard, as the scale
						// appropriate for the editor is on the large side for
						// annotation.
						float scale, rotAng;
						struct propertylist *pPL = (struct propertylist *)pClipboardData;

						GetValuesFromIdeoPolySetBuffer(((char*)pPL + sizeof(pPL->size)),
						                               &scale, &rotAng);				
						scale /= 2.0F;		

						SetValuesInIdeoPolySetBuffer(((char*)pPL + sizeof(pPL->size)),
						                             scale, rotAng);				
*/

						GlobalUnlock(hData);

						// Must register a clipboard format to use if the data is
						// not in a standard format. 
						// Don't try to use one of the standard types if the data
						// is not standard - it won't work!
						UINT nID = RegisterClipboardFormat(_T("aiideogram"));

						SetClipboardData(nID, hData);
						CloseClipboard();
					}
				}

				// Note: property list is copied to new canvas and to clipboard,
				// so can now free original.
				free(pData);
			}
		}
	}
	else
	{
		// There is no current ideogram pointer. This will be the case, for
		// example, when there is no ideogram set loaded. It will also happen
		// when a new set is created, before any ideograms are added to the
		// set. In this case there may still be an ideogram canvas in the
		// drawing area from the previously loaded set, in which case it needs
		// to be removed until a new one in generated.
		SetCanvas(NULL);
	}
}


Ideogram * CIdeoEditCtrl::SelectIdeogram(const char id[])
{
// Call this to select the ideogram with the given id as pCurrIdeo.

	pCurrIdeo = NULL;

	if (pSet)
	{
		Ideogram *pIdeo = pSet->pIdeogramList;

		while (id != NULL && pIdeo != NULL)
		{
			if (strcmp(id, pIdeo->id) == 0) pCurrIdeo = pIdeo;

			pIdeo = pIdeo->pNextIdeogram;
		}
	}


	UpdateCanvas();
	Invalidate();//RedrawWindow(); // Invalidate window
	UpdateWindow(); // Update window

	return pCurrIdeo;
}


static
BOOL
StrIsInt(const char *pStr)
{
	// atoi() returns 0 either if the string is not a number, 
	// or if it is the number 0 (ASCII 48)!
	if (atoi(pStr) == 0 && *pStr != 48) return FALSE;
	else                                return TRUE;
}

static
BOOL
IdeoIDCompare(const char *pStr0, const char *pStr1)
{
	if (StrIsInt(pStr0))
	{
		if (StrIsInt(pStr1)) return atoi(pStr0) - atoi(pStr1);
		else                 return -1;
	}
	else
	{
		if (StrIsInt(pStr1)) return 1;
		else                 return strcmp(pStr0, pStr1);
	}
}


Ideogram *
CIdeoEditCtrl::NewIdeogram()
{
// New ideogram will be inserted in sorted list.
// Current ideogram pointer then points at new ideogram.
	Ideogram *pNewIdeo = NULL;


	if (pSet)
	{
		CIdeoPropertiesDlg dlg;

		if (dlg.DoModal(pSet) == IDOK)
		{
			pNewIdeo = new Ideogram(dlg.id);

			// Insert new ideogram in list in alpha-numeric order (numbers first).
			// This assumes that the existing members of the list are already sorted,
			// which of course they will be if inserted using this function.
			Ideogram *pIdeo = pSet->pIdeogramList;

			if (   pIdeo == NULL
			    || IdeoIDCompare(pIdeo->id, pNewIdeo->id) > 0)
			{
				// Insert at head of list.
				pNewIdeo->pNextIdeogram = pSet->pIdeogramList;
				pSet->pIdeogramList = pNewIdeo;
			}
			else
			{
				while (   pIdeo->pNextIdeogram != NULL 
				       && IdeoIDCompare(pIdeo->pNextIdeogram->id, pNewIdeo->id) < 0)
				{
					pIdeo = pIdeo->pNextIdeogram;
				}

				// Insert after pIdeo.
				pNewIdeo->pNextIdeogram = pIdeo->pNextIdeogram;	
				pIdeo->pNextIdeogram    = pNewIdeo;
			}


			pCurrIdeo = pNewIdeo;



			UpdateCanvas();
			Invalidate();
			UpdateWindow();



			// Add a band for starters, so something appears on the screen.
			AddBand(FALSE, FALSE);


			modified = TRUE;
		}
	}


	return pNewIdeo;
}


BOOL
CIdeoEditCtrl::DeleteIdeogram()
{
// Delete the current ideogram
	BOOL status = FALSE;

	if (pCurrIdeo)
	{
		CString mesg;
		mesg.Format("Delete ideogram %s?\nAre you sure?", pCurrIdeo->id);

		if (AfxMessageBox(mesg, MB_YESNO) == IDYES)
		{
			// Remove current ideogram from list by attaching previous ideogram
			// to next ideogram.
			Ideogram *pPrevIdeo = pSet->GetPrevIdeogram(pCurrIdeo);

			if (pPrevIdeo) pPrevIdeo->pNextIdeogram = pCurrIdeo->pNextIdeogram;
			else           pSet->pIdeogramList = pCurrIdeo->pNextIdeogram;

			delete pCurrIdeo;
			pCurrIdeo = NULL;

			// Update display (should be blank as pCurrIdeo is NULL)
			// Don't automatically set up a new current ideo here, leave that
			// to the calling code, so that the selection box will be updated
			// properly too.
			UpdateCanvas();
			Invalidate();
			UpdateWindow();

			modified = TRUE;

			status = TRUE;
		}
	}

	return status;
}


/****************************************************************************
 *  Operations at Band level.
 */


Band * CIdeoEditCtrl::GetClickedBand(CPoint point)
{
	Band *pClickedBand = NULL;

	if (pCurrIdeo == NULL) return NULL;
	if (pCurrIdeo->pBandList == NULL) return NULL;
	if (pIdeoCanvas == NULL) return NULL;


//	if (point.x < left || point.x > right) return NULL;

	float scale, rotAng;
	struct propertylist *pPL = pIdeoCanvas->patch->data;

	GetValuesFromIdeoPolySetBuffer(((char*)pPL + sizeof(pPL->size)), 
	                               &scale, &rotAng);


	// Minimum x and y coordinates in an IdeoPolySet are always zero.
	// If we assume that the top end of the ideogram is at the edge of the
	// canvas, then:
	int edgePositions[2];
	// Divide by 8 because canvas is in DDGS coordinates
	edgePositions[0] = pIdeoCanvas->frame.cpframe.dy / pIdeoCanvas->frame.cpframe.scale;

	Band *pCurrBand = pCurrIdeo->pBandList;

	while (pCurrBand != NULL)
	{
		edgePositions[1] = edgePositions[0] + (int)(pCurrBand->length * scale);

		//if (point.y > edgePositions[0] && point.y < edgePositions[1])

		// If co-ordinate is smaller than this it would be earlier in the list,
		// if at all, so no point continuing.
		if (point.y <= edgePositions[0]) 
			break;

		if (point.y < edgePositions[1]) 
			pClickedBand = pCurrBand;


		pCurrBand = pCurrBand->pNextBand;

		edgePositions[0] = edgePositions[1];
	}

	return pClickedBand;
}


void CIdeoEditCtrl::SelectAdjacentBand(BOOL next)
{
	if (pCurrIdeo && pCurrIdeo->pBandList)
	{
		// Get currently selected band, if any.
		Band *pSelBand = pCurrIdeo->GetSelectedBand();

		// Get last band in list
		Band *pLastBand = pCurrIdeo->pBandList;
		while (pLastBand->pNextBand) pLastBand = pLastBand->pNextBand;


		// Deselect all bands
		pCurrIdeo->ClearSelections(); 


		Band *pNewSelBand = pSelBand; // By default keep old selection


		if (pSelBand)
		{
			if (next) 
			{
				if (pSelBand->pNextBand != NULL)
				{
					// Select next band, unless that is a centromere marker,
					// in which case select the band after that.
					if (pSelBand->pNextBand->type != Band::BTcentromere)
						pNewSelBand = pSelBand->pNextBand;
					else if (pSelBand->pNextBand->pNextBand != NULL)
						pNewSelBand = pSelBand->pNextBand->pNextBand;
					else
						pNewSelBand = pCurrIdeo->pBandList; // Wrap back to beginning
				}
				else
					pNewSelBand = pCurrIdeo->pBandList; // Wrap back to beginning
			}
			else // Prev
			{
				Band *pPrevBand = pCurrIdeo->GetPrevBand(pSelBand);

				if (pPrevBand != NULL)
				{
					// Select previous band, unless that is a centromere marker,
					// in which case select the band before that.
					if (pPrevBand->type != Band::BTcentromere)
						pNewSelBand = pPrevBand;
					else 
					{
						Band *pPrevPrevBand = pCurrIdeo->GetPrevBand(pPrevBand);
						if (pPrevPrevBand != NULL)
							pNewSelBand = pPrevPrevBand;
						else
							pNewSelBand = pLastBand; // Go to end.
					}
				}
				else
					pNewSelBand = pLastBand; // Go to end.
			}
		}
		else 
		{
			if (next) 
				pNewSelBand = pCurrIdeo->pBandList; // Select first band.
			else
				pNewSelBand = pLastBand; // Select last band.
		}


		pCurrIdeo->SelectBand(pNewSelBand);



		((CIdeogramEditorDlg*)GetParent())->UpdateBandControls();

		UpdateCanvas();
		Invalidate();
		UpdateWindow();
	}
}


void CIdeoEditCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	if (pCurrIdeo)
	{
		// Get currently selected band, if any.
		Band *pSelectedBand = pCurrIdeo->GetSelectedBand();

		// Deselect all bands
		pCurrIdeo->ClearSelections(); 


		Band *pClickedBand = GetClickedBand(point);

		if (pClickedBand != NULL)
		{
			// If clicked band was not already selected, select it. This means that
			// clicking a selected band will de-select it.
			if (pClickedBand != pSelectedBand) pCurrIdeo->SelectBand(pClickedBand);
		}


		((CIdeogramEditorDlg*)GetParent())->UpdateBandControls();

		UpdateCanvas();

// Refresh the whole dialog window so drawing area is updated too
//GetParent()->Invalidate();
//GetParent()->UpdateWindow();

		Invalidate();//RedrawWindow(); // Invalidate window
		UpdateWindow(); // Update window
	}


	CDrawingArea::OnLButtonDown(nFlags, point);
}
	

void CIdeoEditCtrl::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
// If this function is not being called, it may be because the window class
// was not given the CS_DBLCLKS style in the WNDCLASS structure when it was
// registered. I have added this style in the CDrawingArea constructor, so
// it should work now.

	// TODO: Add your message handler code here and/or call default


	if (pCurrIdeo)
	{
		// If there is a band selected, toggle the visibility of the band
		// properties dialog.
		if (pCurrIdeo->GetSelectedBand())
			((CIdeogramEditorDlg*)GetParent())->ToggleBandProperties();
	}

	
	CDrawingArea::OnLButtonDblClk(nFlags, point);
}


void CIdeoEditCtrl::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	if (pCurrIdeo)
	{
		Band *pClickedBand = GetClickedBand(point);

		if (pClickedBand != NULL)
		{
			// Toggle band name visibility
			if (pClickedBand->showName) pClickedBand->showName = FALSE;
			else                        pClickedBand->showName = TRUE;
		}


		((CIdeogramEditorDlg*)GetParent())->UpdateBandControls();

		UpdateCanvas();

// Refresh the whole dialog window so drawing area is updated too
//GetParent()->Invalidate();
//GetParent()->UpdateWindow();

		Invalidate();//RedrawWindow(); // Invalidate window
		UpdateWindow(); // Update window
	}

	
	CDrawingArea::OnRButtonDown(nFlags, point);
}



void CIdeoEditCtrl::AddBand(BOOL centromere, BOOL above)
{
static int number = 0;

	if (pCurrIdeo != NULL)
	{
		Band *pSelBand = pCurrIdeo->GetSelectedBand();

		if (   pSelBand != NULL
		    || pCurrIdeo->pBandList == NULL) // Band selection not possible as there are no bands
		{
			// Insert either a band or a centromere (two centromeric bands with
			// a centromere marker band between them), above or below the 
			// selected band.
			// If no existing bands just inset default band (as this function
			// is called automatically when a new ideogram is created).
			if (pSelBand && pSelBand->type == Band::BTcentHet)
			{
				Band *pPrevBand = pCurrIdeo->GetPrevBand(pSelBand);

				// Cannot insert a band between a centromeric band and a centromere
				if (   (   pPrevBand 
				        && pPrevBand->type == Band::BTcentromere
				        && above)
				    || (   pSelBand->pNextBand 
				        && pSelBand->pNextBand->type == Band::BTcentromere
				        && !above)                                        )
				{
					AfxMessageBox("Cannot insert a band here!");
					return;
				}
		
			}

			if (!centromere) // BAND
			{
				// Create a new band.
				Band *pNewBand = new Band;

				sprintf(pNewBand->name, "%d", number++);
				pNewBand->type      = Band::BTband;
				// Default colour is white, as this is the most common colour
				// (other than black).
				pNewBand->colour[0] = 255;
				pNewBand->colour[1] = 255;
				pNewBand->colour[2] = 255;
				pNewBand->length    = BANDLENGTH_DEFAULT;

				if (pSelBand != NULL)
				{
					if (above)
					{	// Insert band above selected band
						Band *pPrevBand = pCurrIdeo->GetPrevBand(pSelBand);

						if (pPrevBand)
						{
							pPrevBand->pNextBand = pNewBand;
							pNewBand->pNextBand = pSelBand;
						}
						else
						{	// No previous band: selected band must be at
							// head of list, so insert new band at head.
							pNewBand->pNextBand  = pCurrIdeo->pBandList;
							pCurrIdeo->pBandList = pNewBand;
						}
					}
					else
					{	// Insert band below selected band
						pNewBand->pNextBand = pSelBand->pNextBand;
						pSelBand->pNextBand = pNewBand;
					}
				}
				else
				{	// List is empty.
					pNewBand->pNextBand = pCurrIdeo->pBandList;
					pCurrIdeo->pBandList = pNewBand;
				}

				// Select new band.
				pCurrIdeo->ClearSelections();
				pCurrIdeo->SelectBand(pNewBand);
			}
			else // CENTROMERE
			{
				// Create a centromere (two centromeric
				// bands with a centromere marker band between them)
				Band *pNewBand0 = new Band;
				pNewBand0->type      = Band::BTcentHet;
				pNewBand0->colour[0] = 255;
				pNewBand0->colour[1] = 255;
				pNewBand0->colour[2] = 255;
				pNewBand0->length    = BANDLENGTH_DEFAULT;

				Band *pNewBand1 = new Band;
				pNewBand1->type      = Band::BTcentromere;
				pNewBand1->colour[0] = 255;
				pNewBand1->colour[1] = 255;
				pNewBand1->colour[2] = 255;
				pNewBand1->length    = 0;

				Band *pNewBand2 = new Band;
				pNewBand2->type      = Band::BTcentHet;
				pNewBand2->colour[0] = 255;
				pNewBand2->colour[1] = 255;
				pNewBand2->colour[2] = 255;
				pNewBand2->length    = BANDLENGTH_DEFAULT;


				pNewBand0->pNextBand = pNewBand1;
				pNewBand1->pNextBand = pNewBand2;


				// Now inset this list of three bands in the main band list.
				if (pSelBand != NULL)
				{
					if (above)
					{	// Insert centromere above selected band
						Band *pPrevBand = pCurrIdeo->GetPrevBand(pSelBand);

						if (pPrevBand)
						{
							pPrevBand->pNextBand = pNewBand0;
							pNewBand2->pNextBand = pSelBand;
						}
						else
						{	// No previous band: selected band must be at
							// head of list, so insert centromere at head.
							pNewBand2->pNextBand = pCurrIdeo->pBandList;
							pCurrIdeo->pBandList = pNewBand0;
						}
					}
					else
					{	// Insert centromere below selected band
						pNewBand2->pNextBand = pSelBand->pNextBand;
						pSelBand->pNextBand  = pNewBand0;
					}
				}
				else
				{	// List is empty.
					pNewBand2->pNextBand = pCurrIdeo->pBandList;
					pCurrIdeo->pBandList = pNewBand0;
				}

				// Select one of new bands.
				pCurrIdeo->ClearSelections();
				if (above) pCurrIdeo->SelectBand(pNewBand0);
				else       pCurrIdeo->SelectBand(pNewBand2);
			}


			((CIdeogramEditorDlg*)GetParent())->UpdateBandControls();

			UpdateCanvas();
			Invalidate();
			UpdateWindow();

			modified = TRUE;
		}
	}
}


void CIdeoEditCtrl::DeleteBand()
{
	if (pCurrIdeo != NULL)
	{
		Band *pSelectedBand = pCurrIdeo->GetSelectedBand();

		if (pSelectedBand)
		{
			if (pSelectedBand->type == Band::BTcentHet)
			{
				if (AfxMessageBox("This will also remove the centromere!", MB_OKCANCEL) != IDOK)
					return;
			}

			pCurrIdeo->RemoveSelectedBand();



			((CIdeogramEditorDlg*)GetParent())->UpdateBandControls();

			UpdateCanvas();
			Invalidate();
			UpdateWindow();

			modified = TRUE;
		}
	}
}




