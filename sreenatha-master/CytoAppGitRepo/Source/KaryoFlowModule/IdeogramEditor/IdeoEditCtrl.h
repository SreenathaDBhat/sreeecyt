#if !defined(AFX_IDEOEDITCTRL_H__FC934D91_3095_11D4_BE49_00A0C9780849__INCLUDED_)
#define AFX_IDEOEDITCTRL_H__FC934D91_3095_11D4_BE49_00A0C9780849__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IdeoEditCtrl.h : header file
//


#include "DrawingArea.h"

/////////////////////////////////////////////////////////////////////////////
// CIdeoEditCtrl window

class CIdeoEditCtrl : public CDrawingArea
{
public:
	IdeogramSet *pSet;
	Ideogram *pCurrIdeo;
	BOOL modified;


	void ApplyScale(float s);
	float GetScale(){ return scale; }
	int NewSet();
	int LoadSetFile(const char pathFileName[], BOOL openReadOnly);
	int LoadSet(BOOL humanOnly = FALSE);
	void EditSetProperties();
	BOOL SaveSet();
	const char *GetSetName();
	int SaveIfModifiedOrCancel();
	BOOL IsReadOnly(){ return readOnly; }
	BOOL DeleteSet();

	void UpdateCanvas();
	Ideogram *SelectIdeogram(const char id[]);
	Ideogram *NewIdeogram();
	BOOL DeleteIdeogram();

	Band *GetClickedBand(CPoint point);
	void SelectAdjacentBand(BOOL next);
	void AddBand(BOOL centromere, BOOL above);
	void DeleteBand();

private:
	IdeoPolySet *pPolySet;
	Canvas *pIdeoCanvas;

	CString fileName;
	CString setName;

	float scale;
	BOOL readOnly;
	int lockfileHandle;


// Construction
public:
	CIdeoEditCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIdeoEditCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIdeoEditCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIdeoEditCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IDEOEDITCTRL_H__FC934D91_3095_11D4_BE49_00A0C9780849__INCLUDED_)
