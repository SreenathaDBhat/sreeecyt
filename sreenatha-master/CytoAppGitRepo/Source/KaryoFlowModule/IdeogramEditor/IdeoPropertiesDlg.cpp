// IdeoPropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IdeogramEditor.h"
#include "IdeoPropertiesDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIdeoPropertiesDlg dialog


CIdeoPropertiesDlg::CIdeoPropertiesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIdeoPropertiesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CIdeoPropertiesDlg)
	m_id = _T("");
	m_type = -1;
	m_autosome = _T("");
	//}}AFX_DATA_INIT

	pSet = NULL;
}


void CIdeoPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIdeoPropertiesDlg)
	DDX_Control(pDX, IDC_COMBO_SEX, m_sexListCombo);
	DDX_Control(pDX, IDC_SPIN_AUTOSOME, m_autosomeSpin);
	DDX_Text(pDX, IDC_EDIT_ID, m_id);
	DDV_MaxChars(pDX, m_id, 10);
	DDX_Radio(pDX, IDC_RADIO_AUTOSOME, m_type);
	DDX_Text(pDX, IDC_EDIT_AUTOSOME, m_autosome);
	DDV_MaxChars(pDX, m_autosome, 8);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CIdeoPropertiesDlg, CDialog)
	//{{AFX_MSG_MAP(CIdeoPropertiesDlg)
	ON_BN_CLICKED(IDC_RADIO_AUTOSOME, OnRadioAutosome)
	ON_BN_CLICKED(IDC_RADIO_SEX, OnRadioSex)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIdeoPropertiesDlg message handlers

BOOL CIdeoPropertiesDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	//m_autosome = 1;
	m_autosomeSpin.SetRange(1, 100);

	m_sexListCombo.ResetContent();
	m_sexListCombo.AddString("W");
	m_sexListCombo.AddString("X");
	m_sexListCombo.AddString("Y");
	m_sexListCombo.AddString("Z");
	m_sexListCombo.SetCurSel(1); // Select 'X' by default.

	m_type = 0; // Select autosome radio button by default
	UpdateData(FALSE);
	OnRadioAutosome();

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CIdeoPropertiesDlg::OnRadioAutosome() 
{
	// TODO: Add your control notification handler code here

	GetDlgItem(IDC_EDIT_AUTOSOME)->EnableWindow(TRUE);
	GetDlgItem(IDC_COMBO_SEX)->EnableWindow(FALSE);

	isAutosome = TRUE;
}

void CIdeoPropertiesDlg::OnRadioSex() 
{
	// TODO: Add your control notification handler code here
	
	GetDlgItem(IDC_EDIT_AUTOSOME)->EnableWindow(FALSE);
	GetDlgItem(IDC_COMBO_SEX)->EnableWindow(TRUE);

	isAutosome = FALSE;
}

int CIdeoPropertiesDlg::DoModal(const IdeogramSet *pIdeoSet) 
{
	// TODO: Add your specialized code here and/or call the base class

	pSet = pIdeoSet;
	
	return CDialog::DoModal();
}

void CIdeoPropertiesDlg::OnOK() 
{
	// TODO: Add extra validation here

	UpdateData(TRUE);


	if (isAutosome)	
		// Don't allow any whitespace characters - if there are any,
		// use the string preceeding any whitespace.
		id = m_autosome.SpanExcluding(" \t\n"); //id.Format("%d", m_autosome);
	else
		m_sexListCombo.GetWindowText(id);


	if (id.IsEmpty()) return;


	if (pSet != NULL)
	{
		Ideogram *pIdeo = pSet->pIdeogramList;

		while (pIdeo)
		{
			if (strcmp(pIdeo->id, id) == 0)
			{
				AfxMessageBox("Ideogram already exists!");
				return; // Don't close dialog - let them choose again
			}

			pIdeo = pIdeo->pNextIdeogram;
		}
	}

	
	CDialog::OnOK();
}

