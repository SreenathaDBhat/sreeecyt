#if !defined(AFX_IDEOPROPERTIESDLG_H__0C778101_3226_11D4_BE4D_00A0C9780849__INCLUDED_)
#define AFX_IDEOPROPERTIESDLG_H__0C778101_3226_11D4_BE4D_00A0C9780849__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IdeoPropertiesDlg.h : header file
//

#include "ideogramdll.h"


/////////////////////////////////////////////////////////////////////////////
// CIdeoPropertiesDlg dialog

class CIdeoPropertiesDlg : public CDialog
{
	BOOL isAutosome;
	const IdeogramSet *pSet;

public:
	CString id;


// Construction
public:
	CIdeoPropertiesDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CIdeoPropertiesDlg)
	enum { IDD = IDD_IDEOPROPERTIES };
	CComboBox	m_sexListCombo;
	CSpinButtonCtrl	m_autosomeSpin;
	CString	m_id;
	int		m_type;
	CString	m_autosome;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIdeoPropertiesDlg)
	public:
	virtual int DoModal(const IdeogramSet *pSet); // Manually added argument
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CIdeoPropertiesDlg)
	afx_msg void OnRadioAutosome();
	afx_msg void OnRadioSex();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IDEOPROPERTIESDLG_H__0C778101_3226_11D4_BE4D_00A0C9780849__INCLUDED_)
