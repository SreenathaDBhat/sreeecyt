// IdeoSetPropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IdeogramEditor.h"
#include "IdeoSetPropertiesDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIdeoSetPropertiesDlg dialog


CIdeoSetPropertiesDlg::CIdeoSetPropertiesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIdeoSetPropertiesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CIdeoSetPropertiesDlg)
	m_species = _T("");
	m_resolution = _T("");
	m_bandingtype = _T("");
	m_width = 0.0f;
	//}}AFX_DATA_INIT
}


void CIdeoSetPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIdeoSetPropertiesDlg)
	DDX_Text(pDX, IDC_EDIT_SPECIES, m_species);
	DDV_MaxChars(pDX, m_species, 20);
	DDX_Text(pDX, IDC_EDIT_RESOLUTION, m_resolution);
	DDV_MaxChars(pDX, m_resolution, 20);
	DDX_Text(pDX, IDC_EDIT_BANDINGTYPE, m_bandingtype);
	DDV_MaxChars(pDX, m_bandingtype, 20);
	DDX_Text(pDX, IDC_EDIT_WIDTH, m_width);
	DDV_MinMaxFloat(pDX, m_width, 1.f, 100.f);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CIdeoSetPropertiesDlg, CDialog)
	//{{AFX_MSG_MAP(CIdeoSetPropertiesDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIdeoSetPropertiesDlg message handlers
