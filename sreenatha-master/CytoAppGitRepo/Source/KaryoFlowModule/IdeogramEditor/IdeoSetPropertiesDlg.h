#if !defined(AFX_IDEOSETPROPERTIESDLG_H__87A5DA41_3AD0_11D4_BE4E_00A0C9780849__INCLUDED_)
#define AFX_IDEOSETPROPERTIESDLG_H__87A5DA41_3AD0_11D4_BE4E_00A0C9780849__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IdeoSetPropertiesDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIdeoSetPropertiesDlg dialog

class CIdeoSetPropertiesDlg : public CDialog
{
// Construction
public:
	CIdeoSetPropertiesDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CIdeoSetPropertiesDlg)
	enum { IDD = IDD_SETPROPERTIES };
	CString	m_species;
	CString	m_resolution;
	CString	m_bandingtype;
	float	m_width;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIdeoSetPropertiesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CIdeoSetPropertiesDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IDEOSETPROPERTIESDLG_H__87A5DA41_3AD0_11D4_BE4E_00A0C9780849__INCLUDED_)
