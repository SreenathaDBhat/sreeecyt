// IdeogramEditor.h : main header file for the IDEOGRAMEDITOR application
//

#if !defined(AFX_IDEOGRAMEDITOR_H__C7253CC5_C752_11D3_BE2D_00A0C9780849__INCLUDED_)
#define AFX_IDEOGRAMEDITOR_H__C7253CC5_C752_11D3_BE2D_00A0C9780849__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CIdeogramEditorApp:
// See IdeogramEditor.cpp for the implementation of this class
//

class CIdeogramEditorApp : public CWinApp
{
public:
	CIdeogramEditorApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIdeogramEditorApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CIdeogramEditorApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IDEOGRAMEDITOR_H__C7253CC5_C752_11D3_BE2D_00A0C9780849__INCLUDED_)
