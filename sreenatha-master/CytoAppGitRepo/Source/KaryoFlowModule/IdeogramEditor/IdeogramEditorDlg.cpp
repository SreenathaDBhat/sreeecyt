// IdeogramEditorDlg.cpp : implementation file
//

#include "stdafx.h"
#include <Gdiplus.h>
#include <GdiplusBitmap.h>

#include "IdeogramEditor.h"
#include "IdeogramEditorDlg.h"

//#include <navigator.h>
//#include <casebase.h>

//#include <capabilities.h>

//#include <assert.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Holder for bit flags set by capable_capabilities().
static int capabilities = 0;


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIdeogramEditorDlg dialog

CIdeogramEditorDlg::CIdeogramEditorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIdeogramEditorDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CIdeogramEditorDlg)
	m_setName = _T("");
	m_bandName = _T("");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	pIdeoEditCtrl      = NULL;
//	pTopCan = NULL;
	pNewBandDlg        = NULL;
	pBandPropertiesDlg = NULL;
}

CIdeogramEditorDlg::~CIdeogramEditorDlg()
{
//	if (pTopCan != NULL) destroy_Canvas_and_Wobj(pTopCan);

	if (pIdeoEditCtrl != NULL) 
	{
		pIdeoEditCtrl->DestroyWindow();
		delete pIdeoEditCtrl;
	}

	if (pNewBandDlg) delete pNewBandDlg;
	if (pBandPropertiesDlg) delete pBandPropertiesDlg;


	// Broadcast a message that this program is exiting.
	// I put this here so that WinCV can deactivate ideogram annotation mode,
	// but any app can check for it.
	UINT ideoExitMess = RegisterWindowMessage("AIIdeoEditEXIT");

	::PostMessage(HWND_BROADCAST, ideoExitMess, 0, 0);
}

void CIdeogramEditorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIdeogramEditorDlg)
	DDX_Control(pDX, IDC_COMBO_ZOOM, m_scaleCombo);
	DDX_Control(pDX, IDC_COMBO_IDEO, m_ideoListCombo);
	DDX_Text(pDX, IDC_EDIT_SET_NAME, m_setName);
	DDX_Text(pDX, IDC_EDIT_BAND_NAME, m_bandName);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_COMBO1, setsCombo);
}

BEGIN_MESSAGE_MAP(CIdeogramEditorDlg, CDialog)
	//{{AFX_MSG_MAP(CIdeogramEditorDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_SET_LOAD, OnButtonSetLoad)
	ON_BN_CLICKED(IDC_BUTTON_IDEO_NEW, OnButtonIdeoNew)
	ON_BN_CLICKED(IDC_BUTTON_BAND_NEW, OnButtonBandNew)
	ON_BN_CLICKED(IDC_BUTTON_BAND_DELETE, OnButtonBandDelete)
	ON_BN_CLICKED(IDC_BUTTON_BAND_PROPERTIES, OnButtonBandProperties)
	ON_BN_CLICKED(IDC_BUTTON_SET_NEW, OnButtonSetNew)
	ON_BN_CLICKED(IDC_BUTTON_SET_PROPERTIES, OnButtonSetProperties)
	ON_CBN_SELCHANGE(IDC_COMBO_IDEO, OnSelchangeComboIdeo)
	ON_BN_CLICKED(IDC_BUTTON_IDEO_DELETE, OnButtonIdeoDelete)
	ON_BN_CLICKED(IDC_BUTTON_SET_SAVE, OnButtonSetSave)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_BAND, OnDeltaposSpinBand)
	ON_CBN_SELCHANGE(IDC_COMBO_ZOOM, OnSelchangeComboZoom)
	ON_CBN_EDITCHANGE(IDC_COMBO_ZOOM, OnEditchangeComboZoom)
	ON_BN_CLICKED(IDC_BUTTON_SET_DELETE, OnButtonSetDelete)
	ON_CBN_EDITCHANGE(IDC_COMBO_IDEO, OnEditchangeComboIdeo)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDCOPY, &CIdeogramEditorDlg::OnBnClickedCopy)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CIdeogramEditorDlg::OnCbnSelchangeCombo1)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIdeogramEditorDlg message handlers


BOOL CIdeogramEditorDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here



	setsCombo.AddString("Human400G");
	setsCombo.AddString("Human400R");
	setsCombo.AddString("Human550G");
	setsCombo.AddString("Human550R");
	setsCombo.AddString("Human850G");
	setsCombo.SetCurSel(0);


	// Only particular standard MFC controls can be added using the dialog editor,
	// so such a control has been created in the position and size required for
	// our ideogram control, so that its location can be copied.
	CWnd *pDAPosWnd = GetDlgItem(IDC_IDEOEDIT);
	// Make sure the resource based control is not visible or it will obscure
	// our control completely.
	pDAPosWnd->ShowWindow(SW_HIDE);
	// Get position of control relative to parent window.
	RECT rect;
	pDAPosWnd->GetWindowRect(&rect);
	// Convert to positions relative to the client area of the parent window
	// (i.e. not including title bar, borders etc.).
	ScreenToClient(&rect);


	// Create an DrawingArea based control.
	pIdeoEditCtrl = new CIdeoEditCtrl();
	pIdeoEditCtrl->Create(NULL, (short)rect.left, 
	                            (short)rect.top, 
	                            (short)(rect.right-rect.left), 
	                            (short)(rect.bottom-rect.top),
	                            (CWnd*)this);



/*
// Read a woolz object (e.g. an old-style ideogram image file).
FILE *fp;
WZObj *wobj = NULL;

if (   (fp = fopen("image.wobj", "r")) != NULL
    && ((wobj = readobj(fp)) != NULL)) 
{
	fclose(fp);
}
else
{
	MessageBox("Can't open woolz image file");
	return TRUE;
}
*/


#if 0
	// Create a top level canvas
	Cframe cf;

	/* Set up frame */
	cf.aspect = 100;
	cf.cpframe.type = 60;
	cf.cpframe.scale = 8;
	cf.cpframe.dx = 0;
	cf.cpframe.dy = 0;
	cf.cpframe.ox = 0;
	cf.cpframe.oy = 0;
	cf.cpframe.ix = 1;
	cf.cpframe.iy = 1;
	cf.parent_site = FREE;
	cf.posingrid = 0;
	cf.width = (rect.right-rect.left) * 8;
	cf.height = (rect.bottom-rect.top) * 8;
	cf.clip = 0;
	cf.arena = MARKIM;
	cf.colour = WHITE;
	cf.drawmode = COPY; /* INVERT; */


	short Ctype = Cideogram;
	cf.arena = GREYIM;
	cf.colour = WHITE;
	cf.cpframe.dx = 20;
	cf.cpframe.dy = 20;
	cf.drawmode = COPY;

	pTopCan = create_Canvas(Ctype, &cf, NULL, 1, /*wobj*/NULL, NULL, NULL, NULL, NULL, NULL);


//	pIdeoEditDA->SetCanvas(topcan);
	pIdeoEditCtrl->SetCanvas(pTopCan);
#endif


	// Create modal dialogs for working with bands. These will exist for the
	// duration of the program - show and hide them using ShowWindow().
	pNewBandDlg        = new CNewBandDlg(pIdeoEditCtrl);
	pBandPropertiesDlg = new CBandPropertiesDlg(pIdeoEditCtrl);


	// Load scale combo (values are percentages).
	m_scaleCombo.ResetContent(); // Clear list
	m_scaleCombo.AddString("25");
	m_scaleCombo.AddString("50");
	m_scaleCombo.AddString("100");
	m_scaleCombo.AddString("200");
	m_scaleCombo.AddString("400");
	m_scaleCombo.AddString("800");
	m_scaleCombo.SetCurSel(2); // Should be which ever is 100%



	// Creating the CIdeoditCtrl control may have caused it to load the last
	// set it had loaded, from values stored in the registry, and set the
	// current ideogram and scale also. Need to update the other controls to
	// reflect this.
	// Load ideogram list.
	LoadIdeoListCombo(); // This will just disable it if no set is loaded.
	// Display the ideogram's ID.
	if (pIdeoEditCtrl->pCurrIdeo) m_ideoListCombo.SelectString(-1, pIdeoEditCtrl->pCurrIdeo->id);
	// Display set name.
	m_setName = pIdeoEditCtrl->GetSetName();
	// Display current scale.
	int percentScale = (int)(pIdeoEditCtrl->GetScale() * 100.0f);
	CString scaleString;
	scaleString.Format("%d", percentScale);
	m_scaleCombo.SetWindowText(scaleString);
	// Don't allow set delete if it's read only!
	if (pIdeoEditCtrl->IsReadOnly()) 
		GetDlgItem(IDC_BUTTON_SET_DELETE)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_BUTTON_SET_DELETE)->EnableWindow(TRUE);
	UpdateData(FALSE);



	return TRUE;  // return TRUE  unless you set the focus to a control
}

/*
void CIdeogramEditorDlg::OnActivateApp(BOOL bActive, HTASK hTask) 
{
// The message filter for CIdeogramEditorDlg in ClassWizard was set to 'Dialog' by default.
// I had to change it to 'Window' to add this message handler.
	CDialog::OnActivateApp(bActive, hTask);
	
	// TODO: Add your message handler code here

	if (!bActive)
	{
		UINT ideoDeacMess = RegisterWindowMessage("IdeogramEditorDeActivating");

		::PostMessage(HWND_BROADCAST, ideoDeacMess, 0, 0);
	}
}
*/

void CIdeogramEditorDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CIdeogramEditorDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CIdeogramEditorDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}




void CIdeogramEditorDlg::OnOK() 
{
	// TODO: Add extra validation here


	if (pIdeoEditCtrl->SaveIfModifiedOrCancel() == IDCANCEL) return;

	
	CDialog::OnOK();
}


void CIdeogramEditorDlg::OnCancel() 
{
	// TODO: Add extra cleanup here


	if (pIdeoEditCtrl->SaveIfModifiedOrCancel() == IDCANCEL) return;

	
	CDialog::OnCancel();
}



BOOL CIdeogramEditorDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class


	if (strstr(AfxGetApp()->m_lpCmdLine, "aicv"))
	{
	// If this app was started by WinCV (it has a command-line parameter
	// 'aicv') and gets a message saying that WinCV has shut down, then
	// close down gracefully.
		UINT cvExitMess = RegisterWindowMessage("AICVEXIT");

		if (pMsg->message == cvExitMess)
		{
			OnCancel();
		}
	}

	
	return CDialog::PreTranslateMessage(pMsg);
}



/****************************************************************************
 * Buttons for Ideogram Set
 */


void CIdeogramEditorDlg::OnButtonSetNew() 
{




	if (pIdeoEditCtrl->NewSet() != IDCANCEL)
	{
	// NewSet() returns IDCANCEL if the creation of a new set was cancelled.
	// The following lines of code are valid if either IDYES or IDNO were
	// returned, meaning either that a new set was created, or that the old
	// set was cleared but creating a new set failed (in the latter case the
	// set name control etc. are just cleared).

		// Update set description control
		m_setName = pIdeoEditCtrl->GetSetName();

		LoadIdeoListCombo(); // Clear ideogram selection list.

		UpdateData(FALSE);

		UpdateBandControls();

		// Initialise zoom to 100% for new sets.
		m_scaleCombo.SelectString(-1, "100");
		OnSelchangeComboZoom();
	}
}

void CIdeogramEditorDlg::OnButtonSetLoad() 
{
	
	// The return value from CIdeoEditCtrl::LoadSet() is IDCANCEL if the load
	// of a new set was cancelled. The following lines of code are valid if
	// either IDYES or IDNO are returned, meaning that either a new set was
	// loaded successfully, of that loading a new set failed (in the latter
	// case the set name control etc. are cleared).

		// Don't allow set delete if it's read only!
		if (pIdeoEditCtrl->IsReadOnly()) 
			GetDlgItem(IDC_BUTTON_SET_DELETE)->EnableWindow(FALSE);
		else
			GetDlgItem(IDC_BUTTON_SET_DELETE)->EnableWindow(TRUE);


		// Update set description control
		m_setName = pIdeoEditCtrl->GetSetName();


		LoadIdeoListCombo();
	
		m_ideoListCombo.SetCurSel(0); // Select first ideogram in list.

		OnSelchangeComboIdeo(); // Update display

		UpdateData(FALSE);
	

/*
// Use this code for loading an arbitary file...

	// TODO: Add your control notification handler code here
	CFileDialog dlg(TRUE);

	if (AfxMessageBox("Any changes that have been made to the current ideogram set\n"
	                  "since the last save will be lost if you continue.\n", MB_OKCANCEL) != IDOK)
		return;

#if (WINVER >= 0x0500 && _WIN32_WINNT >= 0x0500) // Check compile-time OS.
	if (_winver < 0x500) // Check execute-time OS.
		dlg.m_ofn.lStructSize = OPENFILENAME_SIZE_VERSION_400;
#endif

	if (dlg.DoModal() == IDOK)
	{
//FILE *pFile = fopen("D:/WinCV/src/IdeogramEditor/Vysis/Human550G.IBF", "r");
		if (pIdeoEditCtrl->LoadSet((PCSTR)dlg.GetPathName()) != TRUE)
		{
			AfxMessageBox("Failed to load ideogram set!");
		}
		else 
		{
			// Update set description control
			m_setName = pIdeoEditCtrl->GetSetName();

			OnButtonIdeoNext(); // Select first ideogram in set and display number.
		}
	}
*/
}

void CIdeogramEditorDlg::OnButtonSetDelete() 
{


	// DeleteSet() returns FALSE if the operation is cancelled.
	if (pIdeoEditCtrl->DeleteSet())
	{
		// Update set description control
		m_setName = pIdeoEditCtrl->GetSetName();

		LoadIdeoListCombo(); // Clear ideogram selection list.

		UpdateData(FALSE);

		UpdateBandControls();
	}
}

void CIdeogramEditorDlg::OnButtonSetProperties() 
{
	pIdeoEditCtrl->EditSetProperties();
}

void CIdeogramEditorDlg::OnButtonSetSave() 
{


	pIdeoEditCtrl->SaveSet();

	// Update set description control, in case user has done a 'Save As...'
	m_setName = pIdeoEditCtrl->GetSetName();

	UpdateData(FALSE);
}

void CIdeogramEditorDlg::LoadIdeoListCombo()
{
// Must call this function if anything changes about the list of ideograms,
// e.g. a new one is added to the list. Can't just insert a new list item if
// adding to the list as don't know where in the list it should be 
// (pIdeoEditCtrl->NewIdeogram() determines the order).

	m_ideoListCombo.ResetContent(); // Clear list

	if (pIdeoEditCtrl->pSet)
	{
		// Add ideogram ID's to drop-down list. 
		// Make sure that combo box does not have the sort style set in the resources
		// - the order should be that of the list (which will be sorted the correct way).
		Ideogram *pIdeo = pIdeoEditCtrl->pSet->pIdeogramList;

		while (pIdeo != NULL)
		{
			m_ideoListCombo.AddString(pIdeo->id);

			pIdeo = pIdeo->pNextIdeogram;
		}
	}

	// If nothing in the list, disable it, so that it is obvious the set is empty.
	if (m_ideoListCombo.GetCount() > 0) 
		GetDlgItem(IDC_COMBO_IDEO)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_COMBO_IDEO)->EnableWindow(FALSE);
}


/****************************************************************************
 * Buttons for Ideogram
 */


void CIdeogramEditorDlg::OnButtonIdeoNew() 
{
	Ideogram *pNewIdeo = pIdeoEditCtrl->NewIdeogram();

	if (pNewIdeo)
	{
		// Reload ideogram list (now with new ideogram).
		LoadIdeoListCombo();

		// Find and select new ideogram in list.
		int index = m_ideoListCombo.FindStringExact(-1, pNewIdeo->id);
		m_ideoListCombo.SetCurSel(index);

		OnSelchangeComboIdeo(); // Update display

		UpdateBandControls();
	}
}

void CIdeogramEditorDlg::OnSelchangeComboIdeo() 
{
	// Get the ideogram id selected in the combo box.

	// If the combo box has the 'dropdown' rather than the 'droplist' style,
	// i.e. its edit box accepts user input, then this function is called after
	// the list selection has changed, but before the edit box has had the
	// selection copied to it. This means that GetWindowText() will return the
	// previous contents of the edit box. To avoid this, use GetCurSel() instead.
	CString selID = "";

	int index = m_ideoListCombo.GetCurSel();

	if (index != CB_ERR) m_ideoListCombo.GetLBText(index, selID);

	// Select this ideogram in the ideo control
	pIdeoEditCtrl->SelectIdeogram(selID);

	UpdateBandControls();
}

void CIdeogramEditorDlg::OnEditchangeComboIdeo() 
{
	CString idString;
	m_ideoListCombo.GetWindowText(idString);

	// Select this ideogram in the ideo control
	pIdeoEditCtrl->SelectIdeogram(idString);

	UpdateBandControls();
}

void CIdeogramEditorDlg::OnButtonIdeoDelete() 
{
	if (pIdeoEditCtrl->DeleteIdeogram())
	{
		// Reload ideogram list (now with one less ideogram).
		LoadIdeoListCombo();

		OnSelchangeComboIdeo(); // Update display

		UpdateBandControls();
	}
}


/****************************************************************************
 * Buttons for Band
 */

void CIdeogramEditorDlg::OnButtonBandNew() 
{
	if (pIdeoEditCtrl->pCurrIdeo) 
	{
		// Toggle dialog visibility.
		if (pNewBandDlg->IsWindowVisible()) pNewBandDlg->ShowWindow(SW_HIDE);
		else                                pNewBandDlg->ShowWindow(SW_SHOW);
	}
}


void CIdeogramEditorDlg::OnButtonBandProperties() 
{
	if (pIdeoEditCtrl->pCurrIdeo)
	{
		// Toggle dialog visibility.
		if (pBandPropertiesDlg->IsWindowVisible())
			pBandPropertiesDlg->ShowWindow(SW_HIDE);
		else
			pBandPropertiesDlg->ShowWindow(SW_SHOW);
	}
}

void CIdeogramEditorDlg::ToggleBandProperties() 
{
	OnButtonBandProperties();
}


void CIdeogramEditorDlg::OnButtonBandDelete() 
{
	pIdeoEditCtrl->DeleteBand();
}


void CIdeogramEditorDlg::OnDeltaposSpinBand(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here

	// It is useful to be able to change the currently selected band using a
	// spin control rather than clicking on the bands themselves, as some of
	// the bands in high resolution ideograms are so short it can be almost
	// impossible to select them at normal scale.

	if (pNMUpDown->iDelta > 0) 
		pIdeoEditCtrl->SelectAdjacentBand(TRUE);
	else if (pNMUpDown->iDelta < 0)
		pIdeoEditCtrl->SelectAdjacentBand(FALSE);

	
	*pResult = 0;
}


void CIdeogramEditorDlg::UpdateBandControls()
{
	Band *pSelBand = NULL;

	if (pIdeoEditCtrl && pIdeoEditCtrl->pCurrIdeo)
	{
		pSelBand = pIdeoEditCtrl->pCurrIdeo->GetSelectedBand();
	}


	if (pSelBand)
		m_bandName = pSelBand->name;
	else
		m_bandName = "";

	// Update control in main window to display name of new selected band.
	UpdateData(FALSE);


	// Update band properties dialog (non-modal).
	pBandPropertiesDlg->Update();
}


/****************************************************************************
 * Zoom stuff
 */

void CIdeogramEditorDlg::ApplyPercentScaleString(const char scaleString[])
{
	// atoi returns zero if the string cannot be converted to an integer, (or
	// if it is the integer zero - which is not a valid scale anyway) in which
	// case ignore it. Also do not accept values less than zero or greater
	// than 5000. Larger scales consume increasing large amounts of memory to
	// display unless the ideogram is very small. Even a 1x1 band looks quite
	// big at a scale of 5000%!
	int percentScale = atoi((LPCSTR)scaleString);
	if (percentScale > 0 && percentScale <= 5000)
	{
		float scale = percentScale / 100.0f;

		pIdeoEditCtrl->ApplyScale(scale);
	}
}

void CIdeogramEditorDlg::OnSelchangeComboZoom() 
{
	// Get the string from the scale combo box. Hopefully this will be an
	// integer value representing the scale as a percentage.

	// If the combo box has the 'dropdown' rather than the 'droplist' style,
	// i.e. its edit box accepts user input, then this function is called after
	// the list selection has changed, but before the edit box has had the
	// selection copied to it. This means that GetWindowText() will return the
	// previous contents of the edit box. To avoid this, use GetCurSel() instead.
	int index = m_scaleCombo.GetCurSel();

	CString scaleString;
	m_scaleCombo.GetLBText(index, scaleString);

	ApplyPercentScaleString(scaleString);
}

void CIdeogramEditorDlg::OnEditchangeComboZoom()
{
	// Get the string typed into the scale combo box. Hopefully this will be
	// an integer value representing the scale as a percentage.
	CString scaleString;
	m_scaleCombo.GetWindowText(scaleString);

	ApplyPercentScaleString(scaleString);
}





using namespace Gdiplus;

int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
   UINT  num = 0;          // number of image encoders
   UINT  size = 0;         // size of the image encoder array in bytes

   ImageCodecInfo* pImageCodecInfo = NULL;

   GetImageEncodersSize(&num, &size);
   if(size == 0)
      return -1;  // Failure

   pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
   if(pImageCodecInfo == NULL)
      return -1;  // Failure

   GetImageEncoders(num, size, pImageCodecInfo);

   for(UINT j = 0; j < num; ++j)
   {
      if( wcscmp(pImageCodecInfo[j].MimeType, format) == 0 )
      {
         *pClsid = pImageCodecInfo[j].Clsid;
         free(pImageCodecInfo);
         return j;  // Success
      }    
   }

   free(pImageCodecInfo);
   return -1;  // Failure
}

void CIdeogramEditorDlg::OnBnClickedCopy()
{
	HBITMAP hBm = canvas2bitmap(pIdeoEditCtrl->can, pIdeoEditCtrl->dog);

	OpenClipboard();
	EmptyClipboard();
	SetClipboardData(CF_BITMAP, hBm);
	CloseClipboard();

	DeleteObject(hBm);
}

void CIdeogramEditorDlg::OnCbnSelchangeCombo1()
{
	int cur = setsCombo.GetCurSel();
	CHAR selected[32];
	setsCombo.GetLBText(cur, selected);
	
	CString file = CString(selected) + ".ideoset";

	pIdeoEditCtrl->LoadSetFile(file, 1);
	OnSelchangeComboIdeo();	
}
