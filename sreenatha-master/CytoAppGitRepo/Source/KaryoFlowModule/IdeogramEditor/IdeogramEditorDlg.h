// IdeogramEditorDlg.h : header file
//

#if !defined(AFX_IDEOGRAMEDITORDLG_H__C7253CC7_C752_11D3_BE2D_00A0C9780849__INCLUDED_)
#define AFX_IDEOGRAMEDITORDLG_H__C7253CC7_C752_11D3_BE2D_00A0C9780849__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CIdeogramEditorDlg dialog

#include "DrawingArea.h"

#include "IdeoEditCtrl.h"
#include "NewBandDlg.h"
#include "BandPropertiesDlg.h"
#include "afxwin.h"


class CIdeogramEditorDlg : public CDialog
{
public:
	void UpdateBandControls();
	void ToggleBandProperties();

private:
	CIdeoEditCtrl *pIdeoEditCtrl;
//	Canvas *pTopCan;
	CNewBandDlg *pNewBandDlg;
	CBandPropertiesDlg *pBandPropertiesDlg;

	void LoadIdeoListCombo();
	void ApplyPercentScaleString(const char scaleString[]);


// Construction
public:
	CIdeogramEditorDlg(CWnd* pParent = NULL);	// standard constructor
	~CIdeogramEditorDlg();

// Dialog Data
	//{{AFX_DATA(CIdeogramEditorDlg)
	enum { IDD = IDD_IDEOGRAMEDITOR_DIALOG };
	CComboBox	m_scaleCombo;
	CComboBox	m_ideoListCombo;
	CString	m_setName;
	CString	m_bandName;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIdeogramEditorDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;




	// Generated message map functions
	//{{AFX_MSG(CIdeogramEditorDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonSetLoad();
	afx_msg void OnButtonIdeoNew();
	afx_msg void OnButtonBandNew();
	afx_msg void OnButtonBandDelete();
	afx_msg void OnButtonBandProperties();
	afx_msg void OnButtonSetNew();
	afx_msg void OnButtonSetProperties();
	afx_msg void OnSelchangeComboIdeo();
	afx_msg void OnButtonIdeoDelete();
	afx_msg void OnButtonSetSave();
	virtual void OnOK();
	afx_msg void OnDeltaposSpinBand(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangeComboZoom();
	virtual void OnCancel();
	afx_msg void OnEditchangeComboZoom();
	afx_msg void OnButtonSetDelete();
	afx_msg void OnEditchangeComboIdeo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCopy();
	CComboBox setsCombo;
	afx_msg void OnCbnSelchangeCombo1();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IDEOGRAMEDITORDLG_H__C7253CC7_C752_11D3_BE2D_00A0C9780849__INCLUDED_)
