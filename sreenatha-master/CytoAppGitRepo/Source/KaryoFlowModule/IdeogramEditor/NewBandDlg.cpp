// NewBandDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IdeogramEditor.h"
#include "NewBandDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNewBandDlg dialog


CNewBandDlg::CNewBandDlg(CIdeoEditCtrl *pEditCtrl /*=NULL*/, CWnd* pParent /*=NULL*/)
	: CDialog(CNewBandDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewBandDlg)
	m_bandType = -1;
	m_bandPos = -1;
	//}}AFX_DATA_INIT


	pIdeoEditCtrl = pEditCtrl;


	// Create this as a modeless dialog. To make it visible, call ShowWindow().
	Create(CNewBandDlg::IDD);
}


void CNewBandDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewBandDlg)
	DDX_Radio(pDX, IDC_RADIO_BAND, m_bandType);
	DDX_Radio(pDX, IDC_RADIO_ABOVE, m_bandPos);
	//}}AFX_DATA_MAP

	//DestroyWindow();
}


BEGIN_MESSAGE_MAP(CNewBandDlg, CDialog)
	//{{AFX_MSG_MAP(CNewBandDlg)
	ON_BN_CLICKED(IDC_BUTTON_INSERT, OnButtonInsert)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewBandDlg message handlers

static int prevBandTypeMode = 0; // Default type is band.
static int prevBandPosMode  = 1; // Default position is below.
static RECT posRect = { -1, -1, -1, -1 };


BOOL CNewBandDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here


	if (posRect.right > 0 && posRect.top >= 0) 
		// Position was saved last time, move to it.
		MoveWindow(&posRect);
	else
	{
/* This doesn't seem to work as expected.
		// This is the first time this dialog has been opened, or it has been
		// moved off screen. Move it somewhat to the right of where it would
		// otherwise open so it does not obscure the ideogram.
		RECT rect;

		GetWindowRect(&rect);

		posRect.left   = rect.right;
		posRect.right  = rect.right + rect.right - rect.left;
		posRect.top    = rect.top;
		posRect.bottom = rect.bottom;

		MoveWindow(&posRect);
*/
	}


	m_bandType = prevBandTypeMode;
	m_bandPos  = prevBandPosMode;

	UpdateData(FALSE);
	
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CNewBandDlg::OnButtonInsert() 
{
	UpdateData(TRUE);

	if (pIdeoEditCtrl)
	{
		BOOL centromere = FALSE, above = FALSE;

		if (m_bandType != 0) centromere = TRUE;
		if (m_bandPos  != 1) above      = TRUE;

		pIdeoEditCtrl->AddBand(centromere, above);
	}
}
