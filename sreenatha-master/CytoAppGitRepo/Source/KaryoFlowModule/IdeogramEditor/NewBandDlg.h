#if !defined(AFX_NEWBANDDLG_H__904E1AD1_4856_11D4_BE59_00A0C9780849__INCLUDED_)
#define AFX_NEWBANDDLG_H__904E1AD1_4856_11D4_BE59_00A0C9780849__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewBandDlg.h : header file
//

#include "IdeoEditCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CNewBandDlg dialog

class CNewBandDlg : public CDialog
{
	CIdeoEditCtrl *pIdeoEditCtrl;


// Construction
public:
	CNewBandDlg(CIdeoEditCtrl *pEditCtrl = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewBandDlg)
	enum { IDD = IDD_NEWBAND };
	int		m_bandType;
	int		m_bandPos;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewBandDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNewBandDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonInsert();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWBANDDLG_H__904E1AD1_4856_11D4_BE59_00A0C9780849__INCLUDED_)
