// NewSetWiz.cpp : implementation file
//

#include "stdafx.h"
#include "IdeogramEditor.h"
#include "NewSetWiz.h"

#include "ideogramdll.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNewSetWiz1 property page

IMPLEMENT_DYNCREATE(CNewSetWiz1, CPropertyPage)

CNewSetWiz1::CNewSetWiz1(CString *pSpeciesString) : CPropertyPage(CNewSetWiz1::IDD)
{
	//{{AFX_DATA_INIT(CNewSetWiz1)
	m_edit = _T("");
	//}}AFX_DATA_INIT

	pStr = pSpeciesString;
}

CNewSetWiz1::~CNewSetWiz1()
{
}

void CNewSetWiz1::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewSetWiz1)
	DDX_Control(pDX, IDC_NEWSETWIZ1_TREE, m_tree);
	DDX_Text(pDX, IDC_NEWSETWIZ1_EDIT, m_edit);
	DDV_MaxChars(pDX, m_edit, 40);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewSetWiz1, CPropertyPage)
	//{{AFX_MSG_MAP(CNewSetWiz1)
	ON_NOTIFY(TVN_SELCHANGED, IDC_NEWSETWIZ1_TREE, OnSelchangedNewsetwiz1Tree)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewSetWiz1 message handlers

BOOL CNewSetWiz1::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	// TODO: Add extra initialization here

	m_tree.LoadSpeciesOnly();


	CString initialSpecies = *pStr;
	HTREEITEM hItem = m_tree.GetNextItem(NULL, TVGN_CHILD);

	while (hItem != NULL)
	{
		if (initialSpecies.CompareNoCase(m_tree.GetItemText(hItem)) == 0)
		{
			m_tree.SelectItem(hItem);
			break;
		}

		hItem = m_tree.GetNextItem(hItem, TVGN_NEXT);
	}

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CNewSetWiz1::OnSetActive() 
{
	// TODO: Add your specialized code here and/or call the base class

	CPropertySheet *pParent = (CPropertySheet *)GetParent();

	if (pParent) pParent->SetWizardButtons(PSWIZB_NEXT);

	
	return CPropertyPage::OnSetActive();
}

void CNewSetWiz1::OnSelchangedNewsetwiz1Tree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here

	HTREEITEM item = m_tree.GetSelectedItem();

	if (item)
	{
		m_edit = m_tree.GetItemText(item);
		UpdateData(FALSE);
	}

	
	*pResult = 0;
}

LRESULT CNewSetWiz1::OnWizardNext() 
{
	// TODO: Add your specialized code here and/or call the base class

	UpdateData(TRUE); // Map controls to member variables
	*pStr = m_edit;
	
	return CPropertyPage::OnWizardNext();
}


/////////////////////////////////////////////////////////////////////////////
// CNewSetWiz2 property page

IMPLEMENT_DYNCREATE(CNewSetWiz2, CPropertyPage)

CNewSetWiz2::CNewSetWiz2(CString *pSpeciesString) : CPropertyPage(CNewSetWiz2::IDD)
{
	//{{AFX_DATA_INIT(CNewSetWiz2)
	m_species = _T("");
	m_name = _T("");
	//}}AFX_DATA_INIT

	pStr = pSpeciesString;
}

CNewSetWiz2::~CNewSetWiz2()
{
}

void CNewSetWiz2::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewSetWiz2)
	DDX_Control(pDX, IDC_NEWSETWIZ2_TREE, m_tree);
	DDX_Text(pDX, IDC_NEWSETWIZ2_EDIT_SPECIES, m_species);
	DDX_Text(pDX, IDC_NEWSETWIZ2_EDIT_NAME, m_name);
	DDV_MaxChars(pDX, m_name, 30);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewSetWiz2, CPropertyPage)
	//{{AFX_MSG_MAP(CNewSetWiz2)
	ON_NOTIFY(TVN_SELCHANGED, IDC_NEWSETWIZ2_TREE, OnSelchangedNewsetwiz2Tree)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewSetWiz2 message handlers

BOOL CNewSetWiz2::OnSetActive() 
{
	// TODO: Add your specialized code here and/or call the base class

	CPropertySheet *pParent = (CPropertySheet *)GetParent();

	if (pParent) pParent->SetWizardButtons(PSWIZB_BACK | PSWIZB_FINISH);


	m_species = *pStr;

	m_tree.LoadIdeosetsOnly(m_species, TRUE);


	UpdateData(FALSE); // Map member variables to controls

	
	return CPropertyPage::OnSetActive();
}

void CNewSetWiz2::OnSelchangedNewsetwiz2Tree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here


	HTREEITEM hItem = m_tree.GetSelectedItem();

	if (hItem)
	{
		m_name = m_tree.GetItemText(hItem);
		UpdateData(FALSE);
	}

	
	*pResult = 0;
}

BOOL CNewSetWiz2::OnWizardFinish() 
{
	// TODO: Add your specialized code here and/or call the base class

	
	BOOL status = FALSE;


	UpdateData(TRUE);


	if (m_name.GetLength() < 1)
	{
		AfxMessageBox("Please enter a name for the new set!");
	}
	else
	{
		// Start by assuming that a set with this name does not already exist
		// (e.g. list from tree is empty).
		status = TRUE; // Okay so far

		HTREEITEM hItem = m_tree.GetNextItem(NULL, TVGN_CHILD);

		while (hItem != NULL)
		{
			if (m_name.CompareNoCase(m_tree.GetItemText(hItem)) == 0)
			{
				// Name matches existing item in list
				status = FALSE;

				CString mesg;

				// Check if this is a ideoset supplied with the system
				// - don't want users overwriting these.
				if (m_tree.IsItemReadOnly(hItem))
				{
					mesg = "Ideogram set " + m_name + " may not be overwritten.\n"
					       "Please choose another name.";

					AfxMessageBox(mesg);
					break;
				}

				mesg = "Existing ideogram set " + m_name + " will be overwritten "
				       "and\nall ideograms within it will be permanently lost!\n"
				       "Are you sure?";

				if (AfxMessageBox((LPCSTR)mesg, MB_YESNO) == IDYES)
				{
					mesg = "Overwrite " + m_name + "?"; // Default mesg if set load fails.

					m_tree.SelectItem(hItem); // So we can use GetSelPath().

					FILE *pFile;
					if ((pFile = fopen((PCSTR)m_tree.GetSelPath(), "r")) != NULL)
					{
						IdeogramSet *pTempSet;

						if ((pTempSet = new IdeogramSet) != NULL)
						{
							if (pTempSet->ReadIBFFile(pFile)) 
							{
								int numIdeo = 0, numBands = 0;
								Ideogram *pCurrIdeo = pTempSet->pIdeogramList;
								while (pCurrIdeo != NULL)
								{
									Band *pCurrBand = pCurrIdeo->pBandList;
									while (pCurrBand != NULL)
									{
										numBands++;
										pCurrBand = pCurrBand->pNextBand;
									}

									numIdeo++;
									pCurrIdeo = pCurrIdeo->pNextIdeogram;
								}

								mesg.Format("Overwrite '%s' (contains %d ideograms, %d bands)?",
								            m_name, numIdeo, numBands);
							}

							delete pTempSet;
						}		

						fclose (pFile);			
					}


					if (AfxMessageBox((LPCSTR)mesg, MB_OKCANCEL) == IDOK) 
					{
						status = TRUE;
					}
				}

				break;
			}

			hItem = m_tree.GetNextItem(hItem, TVGN_NEXT);
		}
	}


	// Why have this setName variable when it's the same as m_name? Because
	// for some reason the function CNewSetWiz2::OnSelchangedNewsetwiz2Tree()
	// gets called AFTER this function and sets m_name to the first item in
	// the list, which may not be the one that was selected.
	setName = m_name;


	if (status)	
		return CPropertyPage::OnWizardFinish();
	else
		return FALSE;
}

