#if !defined(AFX_NEWSETWIZ_H__A4B02726_3C5D_11D4_BE4F_00A0C9780849__INCLUDED_)
#define AFX_NEWSETWIZ_H__A4B02726_3C5D_11D4_BE4F_00A0C9780849__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewSetWiz.h : header file
//


#include "TemplateFinder.h"


/////////////////////////////////////////////////////////////////////////////
// CNewSetWiz1 dialog

class CNewSetWiz1 : public CPropertyPage
{
	DECLARE_DYNCREATE(CNewSetWiz1)

public:
	CString *pStr;


// Construction
public:
	// Compiler will complain that there is no default constructor if there are
	// not default values for the constructor's parameters.
	CNewSetWiz1(CString *pSpeciesString = NULL); 
	~CNewSetWiz1();

// Dialog Data
	//{{AFX_DATA(CNewSetWiz1)
	enum { IDD = IDD_NEWSETWIZ1 };
	CTemplateFinder	m_tree;            // Manually changed from CTreeCtrl
	CString	m_edit;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CNewSetWiz1)
	public:
	virtual LRESULT OnWizardNext();
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CNewSetWiz1)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangedNewsetwiz1Tree(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

/////////////////////////////////////////////////////////////////////////////
// CNewSetWiz2 dialog

class CNewSetWiz2 : public CPropertyPage
{
	DECLARE_DYNCREATE(CNewSetWiz2)

public:
	CString *pStr;

	CString setName;


// Construction
public:
	// Compiler will complain that there is no default constructor if there are
	// not default values for the constructor's parameters.
	CNewSetWiz2(CString *pSpeciesString = NULL); 
	~CNewSetWiz2();

// Dialog Data
	//{{AFX_DATA(CNewSetWiz2)
	enum { IDD = IDD_NEWSETWIZ2 };
	CTemplateFinder	m_tree;            // Manually changed from CTreeCtrl
	CString	m_species;
	CString	m_name; // Don't use this after dialog closes (see note in CNewSetWiz2::OnWizardFinish()).
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CNewSetWiz2)
	public:
	virtual BOOL OnSetActive();
	virtual BOOL OnWizardFinish();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CNewSetWiz2)
	afx_msg void OnSelchangedNewsetwiz2Tree(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWSETWIZ_H__A4B02726_3C5D_11D4_BE4F_00A0C9780849__INCLUDED_)
