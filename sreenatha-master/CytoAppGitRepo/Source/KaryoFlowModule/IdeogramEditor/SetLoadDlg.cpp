// SetLoadDlg.cpp : implementation file
//

// To get the icons to appear in the CTemplateFinder control, which is defined
// in the Template Generator static library, I had to include the resource file
// for the Template Generator project in this project, by adding the line:
// #include "\WinCV\Src\TemplateGenerator\TemplateGenerator.rc"
// to the the end of the 'Compile-time directives' section of the 
// 'Resource Includes' dialog in Visual Studio.


#include "stdafx.h"
#include "IdeogramEditor.h"
#include "SetLoadDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSetLoadDlg dialog


CSetLoadDlg::CSetLoadDlg(BOOL humanOnly /*=FALSE*/, CWnd* pParent /*=NULL*/)
	: CDialog(CSetLoadDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSetLoadDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	this->humanOnly = humanOnly;

	ideosetName = "";
	readOnly = TRUE; // Safe default!
}


void CSetLoadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSetLoadDlg)
	DDX_Control(pDX, IDC_SETLOAD_TREE, m_tree);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSetLoadDlg, CDialog)
	//{{AFX_MSG_MAP(CSetLoadDlg)
	ON_NOTIFY(NM_DBLCLK, IDC_SETLOAD_TREE, OnDblclkSetloadTree)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSetLoadDlg message handlers

BOOL CSetLoadDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	m_tree.LoadSpeciesAndIdeosets(TRUE, humanOnly);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSetLoadDlg::OnOK() 
{
	// TODO: Add extra validation here

	if (m_tree.IsSelIdeoset())
	{
		ideosetName = m_tree.GetSelPath();

		readOnly = m_tree.IsSelReadOnly();

		CDialog::OnOK();
	}
	else
		AfxMessageBox("Please select an Ideogram Set");
}

void CSetLoadDlg::OnDblclkSetloadTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here

	// Double click on ideoset is the same as selection and OK.
	// Double click on any other tree item is ignored.
	if (m_tree.IsSelIdeoset())
		OnOK();
	
	*pResult = 0;
}
