#if !defined(AFX_SETLOADDLG_H__DD188952_3D24_11D4_BE4F_00A0C9780849__INCLUDED_)
#define AFX_SETLOADDLG_H__DD188952_3D24_11D4_BE4F_00A0C9780849__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetLoadDlg.h : header file
//
#include "TemplateFinder.h"

/////////////////////////////////////////////////////////////////////////////
// CSetLoadDlg dialog

class CSetLoadDlg : public CDialog
{
public:
	CString ideosetName;
	BOOL readOnly;

private:
	BOOL humanOnly;


// Construction
public:
	CSetLoadDlg(BOOL humanOnly = FALSE, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSetLoadDlg)
	enum { IDD = IDD_SETLOAD };
	CTemplateFinder	m_tree;         // Manually changes from CTreeCtrl
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetLoadDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSetLoadDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDblclkSetloadTree(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETLOADDLG_H__DD188952_3D24_11D4_BE4F_00A0C9780849__INCLUDED_)
