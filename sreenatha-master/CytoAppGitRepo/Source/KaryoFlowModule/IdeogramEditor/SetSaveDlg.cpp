// SetSaveDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IdeogramEditor.h"
#include "SetSaveDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSetSaveDlg dialog


CSetSaveDlg::CSetSaveDlg(BOOL readOnly /*=FALSE*/, CWnd* pParent /*=NULL*/)
	: CDialog(CSetSaveDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSetSaveDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	this->readOnly = readOnly;
	saveAs = TRUE; // Safe default.
}


void CSetSaveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSetSaveDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSetSaveDlg, CDialog)
	//{{AFX_MSG_MAP(CSetSaveDlg)
	ON_BN_CLICKED(IDC_BUTTON_SAVEAS, OnButtonSaveas)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSetSaveDlg message handlers


BOOL CSetSaveDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	// Disable the save button (the OK button with different text) if the set
	// is read-only, so that is cannot be saved back to file. However, the user
	// can click the 'Save As...' button to save it to a different file.
	if (readOnly) GetDlgItem(IDOK)->EnableWindow(FALSE);


	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSetSaveDlg::OnButtonSaveas() 
{
	saveAs = TRUE;

	// Get DoModal() to return IDOK.
	EndDialog(IDOK);
}

void CSetSaveDlg::OnOK() 
{
	// TODO: Add extra validation here
	saveAs = FALSE;
	
	CDialog::OnOK();
}
