#if !defined(AFX_SETSAVEDLG_H__37294701_5D5F_11D4_BE69_00A0C9780849__INCLUDED_)
#define AFX_SETSAVEDLG_H__37294701_5D5F_11D4_BE69_00A0C9780849__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetSaveDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSetSaveDlg dialog

class CSetSaveDlg : public CDialog
{
public:
	BOOL saveAs;

private:
	BOOL readOnly;

// Construction
public:
	// Compiler insists on a least one constructor with all parameters supplied
	// with defaults (or no parameters).
	CSetSaveDlg(BOOL readOnly = FALSE, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSetSaveDlg)
	enum { IDD = IDD_SETSAVE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetSaveDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSetSaveDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonSaveas();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETSAVEDLG_H__37294701_5D5F_11D4_BE69_00A0C9780849__INCLUDED_)
