#if !defined(AFX_TEMPLATEBROWSE_H__CE61A563_D7A0_11D2_A0B7_006008A8AB47__INCLUDED_)
#define AFX_TEMPLATEBROWSE_H__CE61A563_D7A0_11D2_A0B7_006008A8AB47__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TemplateBrowse.h : header file
//
#include "\WinCV\Src\TemplateGenerator\resource.h"
#include "TemplateFinder.h"


// Some class specfic defines
#define TEMPLATE_CANCEL 0
#define TEMPLATE_OK		1
#define TEMPLATE_ERROR	2
/////////////////////////////////////////////////////////////////////////////
// CTemplateBrowse dialog

class CTemplateBrowse : public CDialog
{
// Construction
public:
	// Constructor, must pass FileName for TEMPLATE_OPEN mode
	CTemplateBrowse(CWnd* pParent = NULL, CString * FileName = NULL);

// Dialog Data
	//{{AFX_DATA(CTemplateBrowse)
	enum { IDD = IDD_TEMPLATE_BROWSE };
	CTemplateFinder	m_tree;
	BOOL	m_showcgh;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTemplateBrowse)
	public:
	virtual int DoModal();
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTemplateBrowse)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnDblclkBrowseTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangedBrowseTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBrowseDelete();
	afx_msg void OnBrowseShowcgh();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg OnToolTipNotify( UINT id, NMHDR * pNMHDR, LRESULT * pResult );
private:
	/////////////////////////////////////////////////////////////////////////////
	// Methods
	/////////////////////////////////////////////////////////////////////////////
	// Add a icon to a button
	void AddIconToButton(UINT ButtonID, UINT iconID);

	/////////////////////////////////////////////////////////////////////////////
	// Data
	/////////////////////////////////////////////////////////////////////////////

	// Image list for tree control
	CImageList	*pimagelist;
	// A pointer to a string passed by a calling module - holds selected template name
	CString *pFileName;
	// Currently selected template
	CString SelTemplate;
	// Return value from DoModal, set by OnCancel() and OnOK()
	UINT retValue;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEMPLATEBROWSE_H__CE61A563_D7A0_11D2_A0B7_006008A8AB47__INCLUDED_)
