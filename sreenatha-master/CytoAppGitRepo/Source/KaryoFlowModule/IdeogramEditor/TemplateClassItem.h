// TemplateClassItem.h : Header file ---------------------------------------------------------------------
//
//	Note not clw created
//
// 23Mar99 WH: Original
// 03Nov99 SN: Set limits for chromosomes per class and class length.
// 29Jun00 SN: Added new CGH flag to indicate inclusion for normalisation
//             calculations.
//--------------------------------------------------------------------------------------------------------
#ifndef _TEMPLATECLASSITEM_H
#define _TEMPLATECLASSITEM_H

#include "Canvas.h"

#include "ideogramdll.h"


// Centromere types masks
#define METACENTRIC		0		// Centromere in the middle of the chromosome
#define SUBMETACENTRIC	1		// One chromosome arm longer than the other
#define ACROCENTRIC		2		// Centromere at the end of the chromosome

// Sex determination type
#define	AUTOSOME		0		// Non sex determining chromosome
#define X_SOME			1		// Female sex determining chromosome	XX
#define Y_SOME			2		// Male sex determining chromosome		XY
#define W_SOME			3		// Female sex determing chromosome		ZW (lizards. birds etc)
#define Z_SOME			4		// Male sex determing chromosome		ZZ (lizards. birds etc)

// Chromosomes per class limits																		//SN03Nov99
#define CHROMOSOMESPERCLASS_MAX		 20
#define CHROMOSOMESPERCLASS_MIN		  1

// Class length limits																				//SN03Nov99
#define CLASSLENGTH_MAX				500
#define CLASSLENGTH_MIN				 10


class CTemplateClassItem
{
public:
	// Constructors and destructor
	CTemplateClassItem(Canvas * parent, UINT index, UINT Type);
	CTemplateClassItem(Canvas * classitem, UINT index);
	~CTemplateClassItem();
	CTemplateClassItem & operator=(CTemplateClassItem &pClass);

	// Label stuff
	void SetLabel(CString & string);
	CString GetLabel();

	// Return array index
	UINT GetIndex(){ return m_index;};

	void SetPosition(INT x, INT y, INT cx, INT cy);
	void SetPosition(CRect & rect);
	void GetPosition(INT *x, INT *y, INT *cx, INT *cy);
	void GetUnscaledPosition(INT *x, INT *y, INT *cx, INT *cy);
	void SetUnscaledPosition(INT x, INT y, INT cx, INT cy);
	void GetScreenPosition(INT *x, INT *y, INT *cx, INT *cy);
	void GetScreenPosition(CRect * rect);
	int GetHeight(BOOL bChromosomes = FALSE);

	// Ideograms
	Ideogram *GetCorrespondingIdeogram(IdeogramSet *pIdeoset);
	Canvas * GetIdeoCanvas(IdeogramSet *pIdeoset, float scale, BOOL bUseDummy = FALSE, int * dummy = NULL);
	//Canvas * GetIdeoCanvas(CString & TemplateDir, float scale, BOOL bUseDummy = FALSE, int * dummy = NULL); // For old ideograms
	BOOL AttachIdeogram(IdeogramSet *pIdeoset, float scale, BOOL bUseDummy = FALSE);
	//BOOL AttachIdeogram(CString & TemplateDir, float scale, BOOL bUseDummy = FALSE); // For old ideograms
	void RemoveIdeogram();
	BOOL HasIdeogram();
	void UpdateState();

	// Centromeres
	float GetCentromericIndex();
	void SetCentromericIndex(float index);
	void SetCentromereType(UINT Type);
	UINT GetCentromereType();
	void SetClassLength(UINT length);
	int GetClassLength();

	// Sex type
	void SetSexType(UINT type);
	UINT GetSexType();

	void SetChromosomesPerClass(UINT nChroms);
	UINT GetChromosomesPerClass();

	void SetUseForCGHNormalisation(BOOL bUseForNorm);									//SN29Jun00
	BOOL GetUseForCGHNormalisation();													//SN29Jun00

	// Ratio graphs
	void AttachRatGraph();
	void RemoveRatGraph();
	BOOL HasRatGraph();

	Canvas *pCanvas;
	Canvas *IdeoCanvas;

	BOOL Selected;

protected:

private:
	// Chromosome class
	UINT ChromoClass;
	// Class Label
	CString Label;
	// Create and attaches a label canvas
	void CreateLabelCanvas(CString & label, Canvas *parent, WORD drawmode);
	// Create a CchromClass canvas
	void CreateClassCanvas(Canvas *parent);
	// Create a Cstack canvas
	void CreateStackCanvas(Canvas *parent);
	// Adjust spacing of chromosomes attached to a Cchromclass canvas.
	void AdjustSingleClass( Canvas * classcanvas);
	// Helper for AdjustSingleClass
	void AdjustClassMembers(Canvas * classcanvas, int chromsep);
	// Helper for AdjustSingleClass
	int ClassWidthByChrom( Canvas * classcanvas, int * nchroms);

	// Dimensions in window coords
	INT X, Y, Width, Height;

	// Centromeric Index
	float fCentromericIndex;

	// Cchromclass, Cstack
	UINT canType;

	// Sex type AUTOSOME, X_SOME, Y_SOME , W_SOME, Z_SOME
	UINT SexType;

	// Centomere type // TELCENTRIC, ACROCENTRIC, METACENTRIC
	UINT CentromereType;

	// CGH specific flag indicating the class should be included in normalisations		//SN29Jun00
	BOOL bUseForCGHNormalisation;

	// Array index
	UINT m_index;
};

#endif	