#if !defined(AFX_TEMPLATEDETAILS_H__0A243A71_DD22_11D2_A0B7_006008A8AB47__INCLUDED_)
#define AFX_TEMPLATEDETAILS_H__0A243A71_DD22_11D2_A0B7_006008A8AB47__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TemplateDetails.h : header file
//

#include "\WinCV\Src\TemplateGenerator\resource.h"
#include "TemplateKaryotype.h"
/////////////////////////////////////////////////////////////////////////////
// CTemplateDetails dialog

class CTemplateDetails : public CDialog
{
// Construction
public:
	CTemplateDetails(CTemplateKaryotype * Kary, CWnd * DrawingArea = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTemplateDetails)
	enum { IDD = IDD_TEMPLATE_DETAILS };
	CListCtrl	m_list;
	CTreeCtrl	m_tree;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTemplateDetails)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTemplateDetails)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnSelchangedDetailsTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkDetailsValues(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CWnd * pDa;
	CTemplateKaryotype * pKary;
	CImageList * pimagelist;

	HTREEITEM AddGroup(HTREEITEM Parent, CString & name, WORD type = 0, WORD index = 0);
	void ShowDetails(BOOL newtemp);
	void AddValue(CString & name, CString & value, UINT Index);
	BOOL UpdateValue(DWORD TreeData, WORD index, CString & value);
	void ShowSelectedValues(DWORD lparam);
};

/////////////////////////////////////////////////////////////////////////////
// CInputValues dialog

class CInputValues : public CDialog
{
// Construction
public:
	CInputValues(CWnd* pParent = NULL);   // standard constructor
	// The value of 'key' should be selected into the pointer 'string'
	void SelectType(CString & key, CString *string);

// Dialog Data
	//{{AFX_DATA(CInputValues)
	enum { IDD = IDD_INPUT_VALUES };
	CString	m_value;
	CString	m_key;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInputValues)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CInputValues)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	// Pointer to string passed by calling function
	CString  * pString;
};
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEMPLATEDETAILS_H__0A243A71_DD22_11D2_A0B7_006008A8AB47__INCLUDED_)
