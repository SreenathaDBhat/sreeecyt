// TemplateFinder.cpp : implementation file -----------------------------------------------
//
//	20May99	WH	Original
//	02Jul99	SN	Add a default human node with AddDefaultHumanTemplate.
//	21Jul99 SN	Moved TEMPLATE_DIR #define to species.h.
//	04Oct99	SN	Client serevr changes.
//				Removed ThisMachine.
//
//				Use cdb_get_machine instead of get_this_machine/get_other_machine.
//					Constructor, LoadAllMachines(), NetDeleteSpecies()
//
//				Remove "////" prefixes to machine paths as dealt with in cdb_get_machine_path calls.
//					FindSpecies(), FindTemplates(), DeleteSpecies()
//
//	11Oct99 SN	Ignore UNIX machines when scanning for templates.
//					See LoadAllMachines(), NetDeleteSpecies()
//	05Jul00	JMB	Added code for finding ideogram set files (new-style ideograms),
//				including various new functions (all have 'Ideosets' in their name).
//				Added a new field 'readOnly' to CItemInfo and a second constructor.
//				Added functions for querying the read-only status of tree items.
//				Created a new function AddItem() which is now called in all places
//				where a new tree item of any sort is added, as well as being used
//				in the new code for ideosets. This has tided up the code somewhat.
//	17Jul00	JMB	Added LoadSpeciesAndTemplatesAndIdeosets() (used by archiving code).
//	21Jul00	JMB	Added an option to LoadDefaultIdeosets() to only load human sets
//				(used if not in flexible karyotyping (GENUS) mode).
//	01Nov00	JMB	Fixed a bug in LoadDefaultIdeosets() whereby the last file
//				found in the directory was not being read (didn't realise that
//				when CFileFind::FindNextFile() returns FALSE, there is still
//				one more file to read. Is that nuts or what).
//-----------------------------------------------------------------------------------------

#include "stdafx.h"
#include "common.h"
#include "species.h"				//SN02Jul99
#include "TemplateFinder.h"
#include "IdeogramDll.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


///////////////////////////////////////////////////////////////////////
//
//	CItemInfo Defination
//

class CItemInfo
{
public:
	CItemInfo();
	CItemInfo(UINT iType, const char *pPath, const char *pItemName, BOOL bReadOnly = FALSE);
	~CItemInfo();

public:
	UINT Type;
	CString Path; // JMB: changed from TemplatePath (applies to ideogram sets too).
	CString ItemName;
	BOOL readOnly; // JMB: Added 04Jul2000: currently only makes sense for ideosets.

protected:

private:

};

// Tree items level identifiers
#define ITEM_IS_SPECIES		0
#define ITEM_IS_TEMPLATE	1
#define ITEM_IS_IDEOSET		2

///////////////////////////////////////////////////////////////////////
//
//	CItemInfo functions
//
CItemInfo::CItemInfo()
{
	Type     = 0;
	Path     = "";
	readOnly = FALSE;
}

CItemInfo::CItemInfo(UINT iType, const char *pPath, const char *pItemName, BOOL bReadOnly /*=FALSE*/)
{
	Type     = iType;
	Path     = pPath;
	ItemName = pItemName;
	readOnly = bReadOnly;
}

CItemInfo::~CItemInfo()
{
}


/////////////////////////////////////////////////////////////////////////////
// CTemplateFinder

// Length of machine name strings													//SN04Oct99
#define TF_MACHNAMELENGTH	20


CTemplateFinder::CTemplateFinder()
{
	selPath = "";
	// Extension search filter (all)
	SetExtFilter(0);
	pimagelist = NULL;
}

CTemplateFinder::~CTemplateFinder()
{
	// Delete Image list
	delete pimagelist;
}

void CTemplateFinder::OnDestroy() 
{
	DeleteAll();
	CTreeCtrl::OnDestroy();
}

void CTemplateFinder::DeleteAll()
{
	HTREEITEM hItem, hRoot;

	hItem = hRoot = GetRootItem();

	// Delete all item info allocations (Recursive), NOTE must do for all siblings
	// of root node and then itself
	while ((hItem = GetNextSiblingItem(hRoot)) != NULL)
		DeleteItemAndInfo(hItem);

	DeleteItemAndInfo(hRoot);
}

BEGIN_MESSAGE_MAP(CTemplateFinder, CTreeCtrl)
	//{{AFX_MSG_MAP(CTemplateFinder)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CTemplateFinder message handlers

void CTemplateFinder::LoadSpeciesAndTemplates()
{
	InitImageList();
	DeleteAll();
	LoadAllMachines(0);
}

void CTemplateFinder::LoadSpeciesOnly()
{
	InitImageList();
	DeleteAll();
	LoadAllMachines(1);
}

void CTemplateFinder::LoadTemplatesOnly(CString & Species)
{
	InitImageList();
	DeleteAll();
	LoadAllMachines(2, &Species);
}

void CTemplateFinder::LoadIdeosetsOnly(CString & Species, BOOL loadDefaultSets /*=FALSE*/)
{
	InitImageList();
	DeleteAll();
	LoadAllMachines(3, &Species);
	if (loadDefaultSets) LoadDefaultIdeosets(&Species);
}

void CTemplateFinder::LoadSpeciesAndIdeosets(BOOL loadDefaultSets /*=FALSE*/,
                                             BOOL humanOnly /*=FALSE*/)
{
	InitImageList();
	DeleteAll();

	CString humanSpecies = "Human";

	if (humanOnly) LoadAllMachines(3, &humanSpecies);
	else           LoadAllMachines(4);

	if (loadDefaultSets) 
	{
		if (humanOnly) LoadDefaultIdeosets(&humanSpecies);
		else           LoadDefaultIdeosets(NULL);
	}
}

void CTemplateFinder::LoadSpeciesAndTemplatesAndIdeosets(BOOL loadDefaultIdeosets /*=FALSE*/,
                                                         BOOL humanIdeosetsOnly /*=FALSE*/)
{
	InitImageList();
	DeleteAll();
	LoadAllMachines(0);
	LoadAllMachines(4);
	if (loadDefaultIdeosets) LoadDefaultIdeosets(NULL, humanIdeosetsOnly);
}


BOOL CTemplateFinder::IsSelSpecies()
{
	HTREEITEM item = GetSelectedItem();

	if (!item)
		return FALSE;

	CItemInfo * info = (CItemInfo *)GetItemData(item);

	ASSERT(info);
	return (info->Type == ITEM_IS_SPECIES);
}

BOOL CTemplateFinder::IsSelTemplate()
{
	HTREEITEM item = GetSelectedItem();

	if (!item)
		return FALSE;

	CItemInfo * info = (CItemInfo *)GetItemData(item);

	ASSERT(info);
	return (info->Type == ITEM_IS_TEMPLATE);
}

BOOL CTemplateFinder::IsSelIdeoset()
{
	HTREEITEM item = GetSelectedItem();

	if (!item) return FALSE;

	CItemInfo *pInfo = (CItemInfo *)GetItemData(item);

	ASSERT(pInfo);
	return (pInfo->Type == ITEM_IS_IDEOSET);
}


CString &CTemplateFinder::GetSelPath()
{
// JMB This replaces GetSelTemplatePath()
	selPath = "";

	HTREEITEM item = GetSelectedItem();
	if (!item) return selPath;

	CItemInfo * info = (CItemInfo *)GetItemData(item);
	ASSERT(info);

	selPath = info->Path;

	return selPath;
}

BOOL CTemplateFinder::IsItemReadOnly(HTREEITEM hItem)
{
	BOOL readOnly = TRUE; // Safe default

	if (hItem)
	{
		CItemInfo *pInfo = (CItemInfo *)GetItemData(hItem);
		ASSERT(pInfo);

		readOnly = pInfo->readOnly;
	}
	
	return readOnly;
}

BOOL CTemplateFinder::IsSelReadOnly()
{
	HTREEITEM hItem = GetSelectedItem();
	return IsItemReadOnly(hItem);
}

void CTemplateFinder::SetExtFilter(int type)
{
	// 0 - All template
	// 1 - karyogram filter
	// 2 - CGH filter

	switch (type)
	{
	case 0: m_extfilter = "*.*template"; break;
	case 1: m_extfilter.Format("*%s", TEMPLATE_EXTKARY); break;
	case 2: m_extfilter.Format("*%s", TEMPLATE_EXTCGH); break;
	default:m_extfilter = "*.*template"; break;
	}
}
//--------------------------------------------------------------------------------------------------------
// Protected + Private functions
//
//--------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------
//
//
//--------------------------------------------------------------------------------------------------------


HTREEITEM CTemplateFinder::AddItem(UINT type, const char *pPath, const char *pName, 
                                   HTREEITEM hParent /*=NULL*/, BOOL bReadOnly /*=FALSE*/)
{
	HTREEITEM hItem = NULL;

	// Construct info structure to give additional info about this item.
	CItemInfo *pInfo = new CItemInfo(type, pPath, pName, bReadOnly);

	// Add Item to tree
	TV_INSERTSTRUCT tvstruct;
	tvstruct.hParent = hParent;
	tvstruct.hInsertAfter = TVI_SORT;
	tvstruct.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;
	tvstruct.item.pszText = pInfo->ItemName.GetBuffer(0); // Nasty hack to get char*
	switch (type)
	{
		case ITEM_IS_SPECIES:  tvstruct.item.iImage = 0; break;
		case ITEM_IS_TEMPLATE: 
		{
			if (pInfo->Path.Find(TEMPLATE_EXTCGH) != -1)
				tvstruct.item.iImage = 2; // CGH
			else
				tvstruct.item.iImage = 1; // KARY
		}
		break;
		case ITEM_IS_IDEOSET:  tvstruct.item.iImage = 3; break;
	}
	tvstruct.item.iSelectedImage = tvstruct.item.iImage;
	tvstruct.item.lParam = (LPARAM)pInfo; // Point to info structure.

	hItem = InsertItem(&tvstruct);


	return hItem;
}


void CTemplateFinder::FindSpecies(CString & machinename, 
                                  BOOL bAndTemplates, BOOL bAndIdeosets)
{
	// Load the tree with the species directory names and child template names
	CFileFind finder;
	char SpeciesName[128];
	HTREEITEM Parent;
	CString label;
	BOOL exists;


	// Find all files in template directory
	CString Species = machinename + "\\" + TEMPLATEDIR + "\\*.*";					//SN04Oct99

	BOOL found = finder.FindFile((LPCTSTR)Species);

	while (found)
	{
		found = finder.FindNextFile();

		// Only intersted in directories
		if (!finder.IsDots() && finder.IsDirectory())
		{
			strncpy(SpeciesName, (LPCTSTR)finder.GetFileName(), sizeof(SpeciesName));

			// Check if we have already created a species node of the same name
			// i.e. exists on another machine in which case we reuse that node
			exists = FALSE;
			Parent = GetRootItem();
			while (Parent && !exists)
			{
				label = GetItemText(Parent);
				if (label.CompareNoCase(SpeciesName) == 0)
				{
					exists = TRUE;
					break;
				}

				Parent = GetNextSiblingItem(Parent);
			}				


			if (!exists)	// Need to add species node
				Parent = AddItem(ITEM_IS_SPECIES, NULL, (PCSTR)SpeciesName);


			if (bAndTemplates)
				// Find any template files in directory and add as children to tree
				FindTemplates(Parent, machinename, finder.GetFileName());

			if (bAndIdeosets)
				// Find any ideogram set files in directory and add as children to tree
				FindIdeosets(Parent, machinename, finder.GetFileName());
		}
	}
	finder.Close();
}

void CTemplateFinder::FindTemplates(HTREEITEM Parent, CString & machinename, CString &Species)
{
	// Load the tree with the template names as children of Parent from
	// the given directory
	CFileFind finder;

	// Find all files of type .template omly
	CString Template = machinename + "\\" + TEMPLATEDIR + "\\" + Species + "\\" + m_extfilter;		//SN04Oct99

	BOOL found = finder.FindFile((PCSTR)Template);

	while (found)
	{
		found = finder.FindNextFile();

		// Add template item to tree. Parent is species node.
		AddItem(ITEM_IS_TEMPLATE, (PCSTR)finder.GetFilePath(), 
		                          (PCSTR)finder.GetFileTitle(), Parent);
	}
	finder.Close();
}


void CTemplateFinder::FindIdeosets(HTREEITEM Parent, CString &machinename, CString &species)
{
	// Load the tree with the ideo set names as children of Parent
	CFileFind finder;

	// Find all files of type .ideoset only, in given species directory on given machine.
	CString ideosetFilter;
	ideosetFilter.Format("*%s", IDEOSET_EXT);
	CString searchString = machinename + "\\" + TEMPLATEDIR + "\\" + species + "\\" + ideosetFilter;

	BOOL found = finder.FindFile((LPCTSTR)searchString);

	while (found)
	{
		found = finder.FindNextFile();

		AddItem(ITEM_IS_IDEOSET, (PCSTR)finder.GetFilePath(), 
		                         (PCSTR)finder.GetFileTitle(), Parent);
	}

	finder.Close();
}


void CTemplateFinder::LoadDefaultIdeosets(CString *pSpeciesStr /*=NULL*/,
                                          BOOL humanOnly /*=FALSE*/)
{
	CString hardcoded[] = 
	{
		"w",
		"fv"
	};

	for(int i = 0; i < 2; ++i)
	{
		// Get filename including path.
		CString fileName;
		fileName = hardcoded[i];

		FILE *pFile;
		if ((pFile = fopen((PCSTR)fileName, "r")) != NULL)
		{
			IdeogramSet *pSet = new IdeogramSet;
			if (pSet && pSet->ReadIBFFile(pFile))
			{
				// Only add this set if no species is specified, or 
				// else if it is the correct species, AND only if flexible
				// karyotyping is enabled, unless this is a human ideogram set.
				if (   (   pSpeciesStr == NULL 
				        || pSpeciesStr->CompareNoCase(pSet->species) == 0)
				    && (   !humanOnly
				        || stricmp(pSet->species, "human") == 0)          )
				{
					HTREEITEM hSpeciesItem;

					if (pSpeciesStr != NULL)
					{
						// If a species has been specified, assume this is
						// because the calling function is LoadIdeosets,
						// which just loads a list of the sets for one
						// species. In this case, all the ideoset items
						// will be root items, they will not be parented
						// by a species item.
						hSpeciesItem = NULL;
					}
					else
					{
						// No species specified, so this is a full tree,
						// showing multiple species at root level. Search
						// through the root level siblings to find if there
						// is already an item for this species. This item
						// will be the parent item for the ideoset items to
						// be added for this species. If there is no such
						// item, add one.
						BOOL exists = FALSE;
						hSpeciesItem = GetRootItem();
						while (hSpeciesItem && !exists)
						{
							CString name = GetItemText(hSpeciesItem);
							if (name.CompareNoCase(pSet->species) == 0)
							{
								exists = TRUE;
								break;
							}

							hSpeciesItem = GetNextSiblingItem(hSpeciesItem);
						}

						if (!exists)
						{
							// Add a root level item for this species.
							hSpeciesItem = AddItem(ITEM_IS_SPECIES, NULL, pSet->species);
						}
					}


					// Add a read-only ideoset item.
					AddItem(ITEM_IS_IDEOSET, fileName, 
					                         fileName, hSpeciesItem,
					                         TRUE); // Read-only
				}

				if (pSet) delete pSet;
			}

			fclose(pFile);
		}
	}

}


void CTemplateFinder::NetDeleteSpecies(CString &Species)
{
	ASSERT(!"killed this method");
}

void CTemplateFinder::DeleteDirectory(CString &Path)
{
	// Delete All file and sub directories
	CFileFind finder;

	// Find all files in species directory and delete them
	CString AllFiles = Path + "\\*";

	BOOL found = finder.FindFile((LPCTSTR)AllFiles);

	while (found)
	{
		found = finder.FindNextFile();

		// Only interested in files
		if (!finder.IsDots())
		{
			if (finder.IsDirectory())
				DeleteDirectory(finder.GetFilePath());	// Recursive
			else
				DeleteFile((LPCTSTR)finder.GetFilePath());
		}
	}
	finder.Close();
	
	// Then Delete the directory
	RemoveDirectory(Path);
}

void CTemplateFinder::DeleteSpecies(CString &Species, CString &machinename)
{
	// Delete the given species directory and its contents on the given machine (machinename)
	CString AllFiles = machinename + "\\" + TEMPLATEDIR + "\\" + Species;			//SN04Oct99

	DeleteDirectory(AllFiles);
}

void CTemplateFinder::InitImageList()
{
	if (pimagelist)
		delete pimagelist;

	// Add Images to Tree
	pimagelist = new CImageList();
	pimagelist->Create(24, 24, TRUE, 6, 4);

	pimagelist->Add(LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_SPECIES)));
	pimagelist->Add(LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_TEMPLATE)));
	pimagelist->Add(LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_CGHTEMPLATE)));
	pimagelist->Add(LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_IDEOSET)));

	SetImageList(pimagelist, TVSIL_NORMAL);
}


BOOL CTemplateFinder::DeleteItemAndInfo(HTREEITEM hitem)
{

	// Recursively deletes child and siblings and itself
	CItemInfo *info;
	HTREEITEM child, sibling;

	if (hitem == NULL)
		return FALSE;

	info = (CItemInfo *)GetItemData(hitem);

	if (ItemHasChildren(hitem))
	{
		// Get rid of its siblings
		child = GetChildItem(hitem);
		while ((sibling = GetNextSiblingItem(child)) != NULL)
			DeleteItemAndInfo(sibling);

		// Delete the child
		DeleteItemAndInfo(child);
	}

	info = (CItemInfo *)GetItemData(hitem);
	delete info;

	// Delete the parent item
	DeleteItem(hitem);

	return TRUE;
}

void CTemplateFinder::LoadAllMachines(UINT type, CString * Species)
{
}


void CTemplateFinder::AddDefaultHumanTemplate()
{
	// Add a default human template to the browser although it doesnt		//SN02Jul99
	// actually exist. This is neccessary so default human layouts and 
	// classifiers may be selected.
	HTREEITEM Parent;
	CString label;
	BOOL exists;

	// Find the Human species in the tree
	exists = FALSE;
	Parent = GetRootItem();
	while (Parent && !exists)
	{
		label = GetItemText(Parent);
		if (label.CompareNoCase(CLSPECIESDEFAULTSTRING) == 0)
		{
			exists = TRUE;
			break;
		}

		Parent = GetNextSiblingItem(Parent);
	}


	// If Human species exists add the default human template
	if (exists)
	{
		// This should be added as a KARY template (not CGH).
		AddItem(ITEM_IS_TEMPLATE,
		        // Prevents template path from being empty - important for SelectTemplateClassifier.
		        TEMPLATE_DEFAULTHUMAN,
		        TEMPLATE_DEFAULTHUMAN,
		        Parent); // Parent to Species Node
	}
}


CString &CTemplateFinder::GetSelRootName()
{
	// Return extensionless path name
	HTREEITEM item = GetSelectedItem();

	selPath = "";

	if (!item)
		return selPath;

	CItemInfo * info = (CItemInfo *)GetItemData(item);

	ASSERT(info);

	selPath = info->Path;

	// Extract Species directory
	int index = selPath.ReverseFind('.');

	if (index != -1)
		selPath = selPath.Left(index);
	else
		selPath = "";

	return selPath;
}

CString &CTemplateFinder::GetSelSpeciesDir()
{
	// Return extensionless path name
	HTREEITEM item = GetSelectedItem();

	selPath = "";

	if (!item)
		return selPath;

	CItemInfo * info = (CItemInfo *)GetItemData(item);

	ASSERT(info);

	selPath = info->Path;

	// Extract Species directory
	int index = selPath.ReverseFind('\\');

	if (index != -1)
		selPath = selPath.Left(index);
	else
		selPath = "";

	return selPath;
}
