#if !defined(AFX_TEMPLATEFINDER_H__8DEAC175_ED94_11D2_A0BD_006008A8AB47__INCLUDED_)
#define AFX_TEMPLATEFINDER_H__8DEAC175_ED94_11D2_A0BD_006008A8AB47__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TemplateFinder.h : header file
//
//	SN	04Oct99	Removed ThisMachine as part of client server changes.
//
#define TEMPLATE_EXTCGH		".cghtemplate"
#define TEMPLATE_EXTKARY	".karytemplate"

#define IDEOSET_EXT         ".ideoset"

#define TEMPLATE_DEFAULTHUMAN	"NormalHuman"			//SN02Jul99

//#include "\WinCV\Src\TemplateGenerator\resource.h"
/////////////////////////////////////////////////////////////////////////////
// CTemplateFinder window

class CTemplateFinder : public CTreeCtrl
{
// Construction
public:
	CTemplateFinder();
	CString TemplateDirectory;
// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTemplateFinder)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTemplateFinder();
	void LoadSpeciesAndTemplates();
	void LoadSpeciesOnly();
	void LoadTemplatesOnly(CString & Species);
	void LoadIdeosetsOnly(CString & Species, BOOL loadDefaultSets = FALSE);
	void LoadSpeciesAndIdeosets(BOOL loadDefaultSets = FALSE,
	                            BOOL humanOnly = FALSE);
	void LoadSpeciesAndTemplatesAndIdeosets(BOOL loadDefaultIdeosets = FALSE,
	                                        BOOL humanIdeosetsOnly = FALSE);
	void AddDefaultHumanTemplate();
	BOOL IsSelSpecies();
	BOOL IsSelTemplate();
	BOOL IsSelIdeoset();
	BOOL IsSelReadOnly();
	BOOL IsItemReadOnly(HTREEITEM hItem);
	CString &GetSelPath();
	CString &GetSelTemplatePath(){ return GetSelPath(); } // For old code
	CString &GetSelRootName();
	CString &GetSelSpeciesDir();
	void NetDeleteSpecies(CString &Species);
	void DeleteSpecies(CString &Species, CString &machinename);
	void SetExtFilter(int type);


	// Generated message map functions
protected:
	//{{AFX_MSG(CTemplateFinder)
	afx_msg void OnDestroy();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

private:
	HTREEITEM AddItem(UINT type, const char *pPath, const char *pName, 
                      HTREEITEM hParent = NULL, BOOL bReadOnly = FALSE);
	void FindSpecies(CString & machinename, BOOL bAndTemplates, BOOL bAndIdeosets);
	void FindTemplates(HTREEITEM Parent, CString & machinename, CString &Species);
	void FindIdeosets(HTREEITEM Parent, CString &machinename, CString &species);
	void LoadAllMachines(UINT type, CString * Species = NULL);
	void LoadDefaultIdeosets(CString *pSpeciesStr = NULL, BOOL humanOnly = FALSE);
	void InitImageList();
	void DeleteDirectory(CString &Path);
	void DeleteAll();
	BOOL DeleteItemAndInfo(HTREEITEM hitem);


	// Image list for tree control
	CImageList	*pimagelist;
	// Path of selected item
	CString selPath;  // JMB Used to be SelTemplate
	// The extension filter string for search puposes
	CString m_extfilter;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.



#endif // !defined(AFX_TEMPLATEFINDER_H__8DEAC175_ED94_11D2_A0BD_006008A8AB47__INCLUDED_)
