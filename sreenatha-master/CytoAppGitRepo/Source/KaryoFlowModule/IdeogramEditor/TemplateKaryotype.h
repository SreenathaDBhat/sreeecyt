// TemplateKaryotype.h : Header file ---------------------------------------------------------------------
//
//	Note not clw created
//
// 23Mar99 WH: Original
//--------------------------------------------------------------------------------------------------------
#ifndef _TEMPLATEKARYOTYPE_H
#define _TEMPLATEKARYOTYPE_H

// For C++ Templates
#include <afxtempl.h>
#include "common.h"
#include "Canvas.h"
#include "TemplateClassItem.h"
#include "ideogramdll.h"


// These define the scale of ideograms in the template editor.
#define KARYIDEOSCALE	0.5f		// Default scale for an ideogram for karyograms
#define CGHIDEOSCALE	0.75f		// Default scale for an ideogram for cgh


class CTemplateKaryotype
{
public:
	CTemplateKaryotype(int type = Ckaryogram, int draw_mode = COPY);
	~CTemplateKaryotype();

	void SetTemplateName(CString & string);
	CString GetTemplateName();

	void SetSysTemplateName(CString & string) { SysTemplateName = string;};
	CString GetSysTemplateName() { return SysTemplateName; };

	void SetSpeciesName(CString & string);
	CString GetSpeciesName();

	void SetIdeosetName(CString & string);
	CString GetIdeosetName();

	BOOL NewTopCanvas(Canvas * Top, Canvas** canarray, UINT numClasses, BOOL bMakeStack = FALSE);
	void SetNumberOfClasses(Canvas ** canarray, UINT Classes);
	void SetNumberOfClasses(UINT Classes);
	UINT GetNumberOfClasses();

	void SetScreenHeight(UINT height);
	UINT GetScreenHeight();

	CTemplateKaryotype &operator=(CTemplateKaryotype &pTemplate);

	int GetType() {return Type;};

	void DefaultLayout();

	CTemplateClassItem * GetClassItem(UINT index);
	int FindClassCanvasIndex(Canvas * can);
	void CreateStack(Canvas * stackcanvas);
	void CreateStack(UINT height);
	int GetStackItem();
	UINT GetNumberOfItems();

	IdeogramSet *GetIdeogramSet(BOOL forceRead = FALSE);
	Canvas * GetClassIdeoCanvas(UINT ClassIndex, float scale, int * dummy = NULL);
	//BOOL ShowIdeograms(BOOL bShow, float scale = 1.0f, BOOL bUseDummy = FALSE);
	BOOL ShowIdeograms(BOOL bShow, int fitToClassIndex = -1, BOOL bUseDummy = TRUE);

	BOOL Saveas(LPCTSTR filename);
	BOOL Save( BOOL bIsSystemTemplate = TRUE);
	BOOL Open(LPCTSTR filename, BOOL CanPersist = FALSE, BOOL bReread = FALSE);
	BOOL NewTemplate();
	BOOL CreateCGHTemplate( UINT screenheight, CString speciesname, CString templatepath, UINT Classes);

	// Flag that can be set to indicate the template is local or in the species directory
	void SetLocal( BOOL b) { bLocal = b;};
	BOOL IsLocal() { return bLocal; };
	// Flag modified
	void SetModified( BOOL b) { bModified = b;};
	BOOL IsModified() { return bModified; };

	// Lock File
	BOOL Lock();
	void Unlock();
	BOOL HaveLock();

	// Array of CTemplateClass Items
	CTypedPtrArray <CPtrArray, CTemplateClassItem * > ClassItems;
	Canvas * pTopCanvas;

protected:
	void CreateTopCanvas();
	BOOL CreateDirectories(const char * dir); // Should be a utility program somewhere
	int FitInRow(int first, int last, int gap, int *width, int *height);
	void AttachRatioGraphs();
	float Scale();
	void CreateCGHKeyLabels(Canvas *parent, int dx, int dy);
	CString &GetRootPath();

private:
	// Do we or don't we destroy the canvases we have created
	BOOL bPersistCanvas;
	// A flag that can be set to indicate the template is local or in the species directory
	BOOL bLocal;
	// Ckaryogram, cghratkar
	int Type;
	// Full path of this template file
	CString TemplateName;
	// Full path of the System template file i.e \\MACHINE\\Templates\\species\\filename
	// from which this file was derived (may be the same as template file)
	CString SysTemplateName;
	// The Species name 
	CString SpeciesName;
	// The name of the Ideogram Set to use with this template (without path or extension).
	CString ideosetName;
	// The following two variables, nameOfLoadedSet and pIdeoset, should only
	// be used by function GetIdeogramSet().
	// The name of the ideogram set pointed to by pIdeoset (without path or extension).
	// This may be different from ideosetName if GetIdeogramSet() has not been
	// called since the set name for the template was changed.
	CString nameOfLoadedIdeoset;
	// The ideogram set loaded from file nameOfLoadedSet.
	IdeogramSet *pIdeoset; 
	// The number of class
	UINT nClasses;
	// The number of items usually nClasses + 1 for the stack
	UINT nItems;
	// Comment string
	CString Comment;
	// ScreenHeight
	UINT nScreenHeight;
	UINT nScreenWidth;
	// Modified flag
	BOOL bModified;
	// drawmode
	int DrawMode;
	// Lock file handle
	int lock_fh;
	// For general buffer use
	CString BuffStr;
};

#endif	