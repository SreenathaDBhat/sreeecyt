#if !defined(AFX_TEMPLATESIZE_H__FB3B3691_2A19_11D3_8485_00104BAD7499__INCLUDED_)
#define AFX_TEMPLATESIZE_H__FB3B3691_2A19_11D3_8485_00104BAD7499__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TemplateSize.h : header file
//
#include "\WinCV\Src\TemplateGenerator\resource.h"
#include "DialogPopup.h"
/////////////////////////////////////////////////////////////////////////////
// CTemplateSize dialog

class CTemplateSize : public CDialogPopup
{
// Construction
public:
	CTemplateSize(int *size, int maxsize, CWnd* pParent = NULL);   // standard constructor
	void SetApplyFunc(void (*func)(int height));
// Dialog Data
	//{{AFX_DATA(CTemplateSize)
	enum { IDD = IDD_TEMPLATE_SIZE };
	CString	m_text;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTemplateSize)
	public:
	virtual BOOL Create(CWnd* pParent, CWnd ** pFlag);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTemplateSize)
	afx_msg void OnClose();
	virtual BOOL OnInitDialog();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	virtual void OnOK();
	afx_msg void OnSizeApply();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void DrawPage(CDC * dc, int height, BOOL on);
	void (*applyFunc)(int height);
	
	int * m_ret_size;
	int m_height;
	int m_maxheight;
	int m_minheight;
	BOOL m_action;
	CRect m_rect;
	BOOL bOnLine;
	BOOL bCreated;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEMPLATESIZE_H__FB3B3691_2A19_11D3_8485_00104BAD7499__INCLUDED_)
