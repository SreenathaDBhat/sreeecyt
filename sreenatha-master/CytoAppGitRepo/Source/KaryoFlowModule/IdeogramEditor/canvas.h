/*
 * canvas.h
 *
 * Defines the external interface to the new woolz DLL.
 * JMB March 2000
 *
 *
 * Generated header file.
 * Created from translation of UNIX source.
 *
 * Copyright Applied Imaging corp. 1996
 * Created: Tue Oct 15 15:29:31 1996
 *
 * Original source directory: "canvas"
 *
 * Mods:
 *
 *
 *	11Dec02 MC	    Moved Cblobdata propertylist dec. from redkar.h - needed in Xcanvas.cpp [canvas]
 *					and DrawingArea.cpp [CVcontrol] for access to new 'permanent' variable used im met. annotation
 *					Don't want to put redkar.h in these files.
 *	29Nov02 MC		Added Cmetanno tag for metaphase annotation
 *	01Sep99	MG:		Added nothresh_fscale_Canvas() and mode_fscale_Canvas()
 *	07Oct98	JMB:	Added SaveTiffCanvas.
 *	12Jan97	BP:		Add validateWobj().
 *	BP	10/15/96:	Merge new-style function declarations
 *					with original types and defines.
 *
 */


#ifndef CANVAS_H
#define CANVAS_H


#include "ddgs.h"
#include "woolz.h"


//#include "/wincv/include/ddgs.h"
//#include "/wincv/include/woolz.h"

//#include <wstruct.h>
//#include "ddgs.h"
//#include <settings.h>


/****************************************
 * Begin visual studio generated code ...
 */


// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the CANVAS_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// CANVAS_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef CANVAS_EXPORTS
#define CANVAS_API
#else
#define CANVAS_API
#endif

#ifdef __cplusplus 
// This class is exported from the canvas.dll
class CANVAS_API CCanvas {
public:
	CCanvas(void);
	// TODO: add your methods here.
};
#endif

extern CANVAS_API int nCanvas;

CANVAS_API int fnCanvas(void);


/*
 * ... end visual studio generated code.
 ***************************************/




/**** THIS SECTION FROM THE ORIGINAL HEADER ****/

/*
 * CANVAS site flags - 	used in parent_site field of Cframe structure below.
 *
 *	If FREE then canvas is sited at cpframe.dx,dy which is
 *	considered relative to the parent canvas cpframe.dx,dy. 
 *
 *	Otherwise the flag acts as a parent canvas origin modifier
 *
 *	e.g. If RIGHT then cpframe.dx is assumed relative to the
 *	parent canvas right edge i.e. parent cpframe.dx + width.
 *
 *	If CENTRE+TOP then cpframe.dx is relative to parent canvas
 *	centre and cpframe.dy is relative to parent canvas top.
 *
 *	If GRID then the canvas assumes the position posingrid in the 
 *	parent canvas patchgrid. Again cpframe.dx and dy are offsets
 *	from this grid position. CENTRE, RIGHT, TOP etc. now apply
 *	to the canvas relative to its grid position.
 *			
 *	Note FREE is the same as BOTTOM | LEFT
 */
#define	FREE		0	/* parent_site flags modify effective position of */
#define LEFT		1	/* parent cpframe.dx dy */
#define RIGHT		2
#define	TOP		4	/* OR flags together for combination */
#define BOTTOM		8	
#define	CENTRE		16
#define GRID		32


/*** Woolz object drawing arena flags - for Cframe.arena ***/
#define GREYIM		1	/* grey image */
#define OVLYIM		2	/* overlay plane */
#define MARKIM 		3	/* destructive markers */
#define COLOURIM	4	/* 24bit coulour image */

/*** Canvas drawmode - modifies X drawing modes ***/
#define COPY		1	/* use for brightfield image - just draw it */
#define INVERT		2	/* use for fluorescence image - invert it */

// Met Anno Labelling
#define NOLABEL		0
#define	TEMPLABEL	1
#define PERMLABEL	2

/*
 * CANVAS display frame structure
 */
typedef struct Canvas_frame_type{
	int aspect;				/* 0-200 = y:x ratio as pcent e.g 75 for FIP */
	struct pframe cpframe;	/* canvas scale & origin, object origin */
	short parent_site;		/* parent origin modifier - see flags above */
	short posingrid;		/* used if parent_site is GRID, starts at 0 */
	int width;				/* in ddgs coordinates ...*/
	int height;				/* .... typicaly  0..4095 */
	short clip;				/* clip Wobj to Cframe */
	short arena;			/* GREYIM OVLYIM or MARKIM */
	short colour;			/* for GREYIM or MARKIM */
	short drawmode;			/* COPY, INVERT etc */
} Cframe;


/*
 * CANVAS type structure
 */
typedef struct Canvas_type{
	short type;				/* user defined type */
	Cframe frame;			/* display frame */
	struct pgrid patchgrid;	/* layout for patches in this canvas */
	short visible;			/* display or not */
	struct object *Wobj;	/* woolz object */
	struct Canvas_type *parent;	/* parent canvas */
	struct Canvas_type *previous;/* previous canvas in chain */
	struct Canvas_type *next;	/* next canvas in chain */
	struct Canvas_type *patch;	/* patches on this canvas */
	struct propertylist *data;	/* user defined properties */
} Canvas;


/*
 * DDGSframe structure  -  derived display coordinates
 *
 *	used in get_Canvas_ddgsframe and disply_Canvas_frame
 */
typedef struct DDGSframe_type{
	int x;					/* origin */
	int y;
	int midx;				/* midpoint */
	int midy;
	int width;				/* dimesions */
	int height;
} DDGSframe;


/**** END OF ORIGINAL HEADER ****/


/****************************************************************************
 * CANVAS types - originally from common.h
 */
#define Cmetaphase	1
#define Ckaryogram	2
#define Cchromclass	3
#define Cmetblob	4
#define Cchromosome	5
#define Cstack		6
#define Cannotation	7
#define	Cideogram	8
#define Cflex		9
#define Cprint		10
#define Ccomposite	11
#define Cgraphic	12
#define Cprobe		13
#define Coverlap	14
#define Ccounterstain	15
#define Craw		16
#define Ctag		17
#define Cgraphelement	18
#define Cuprobe		19
#define Cratiohilite	20
#define Ccentromere	21
#define Caxis		22
#define Ccghratkar	23
#define Cchromprof	24
#define Ccellprof	25
#define Cslideprof	26
#define Cchromcount	27
#define Cconf95prof	28
#define Cconf99prof	29
#define Cfetal		30
#define Cmetblobplus	31	/* metblob with highlights attached to canvas patch */
#define Cchromosomeplus	32	/* chromosome with highlights attached to canvas patch */
#define Cboundary 33		/* metaphase chromosome boundary */
#define Cmetanno  34
/* End of stuff from common.h **********************************************/





/**** STRUCTURE FROM Xcanvas.h ****/

/* Beware if you change this then CGH size will change and hence .cgh file size */
#define	MAXSELCANTYPES	10

#pragma pack(2)
/* canvas selection data type */
typedef struct Cselectdata_type {
	DDGS *dg;				/* DDGS display structure */
	Canvas *main_canvas;	/* displayed canvas structure */
	short types[MAXSELCANTYPES];	/* canvas types to look for e.g. Cchromosome */
	int ntypes;				/* number of types in types array */
	Canvas *selected_canvas;/* canvas currently highlighted when user
 							/* presses mouse button - could be NULL*/
} Cselectdata;
#pragma pack()

/**** END OF BIT FROM Xcanvas.h ****/



/****************************************************************************
 * Stuff originally from pcont.h
 */

/*	LUT triples are RGBRGBRGB etc. */
#ifndef LUTTRIPPPPPL
#define LUTTRIPPPPPL
#pragma pack(2)
typedef struct {
	unsigned char r, g, b;
} LUT_Triple;
#pragma pack()
#endif


#pragma pack(2)

/*
 *	Component data structure to tag to canvasses
 */
typedef struct componentData {
	SMALL	size;
	int		id;			/* The probe id */
	LUT_Triple *lut;	/* Colourisation LUT pointer */
	short selected;
	short oo_index;
} ComponentData;

typedef struct TempcomponentData {
	SMALL	size;
	int		id;			/* The probe id */
	LUT_Triple *lut;	/* Colourisation LUT pointer */
	short selected;
	short oo_index;
	short was_selected;
} TempComponentData;
	

/*
 * Component data structure to tag to
 * object sub-canvases (in place of
 * their existing one) to retain the
 * colourmap - used by flex and cgh met/kar canvases
 * The first 3 fields of this structure
 * must remain IDENTICAL to structure
 * ComponentData as defined in pcont.h.
 * The actual LUT values are copied from
 * the pcontrol buffer into the new
 * ComponentData buffer. The lut pointer
 * is then set to point to the new, local
 * data table.
 */

typedef struct {
	SMALL size;
	int id;
	LUT_Triple *lut;
	LUT_Triple lutdata[256];	/* New bit - LUT data. */
} PasteComponentData;


/*
 *	Overlap data structure to tag to canvasses
 */
typedef struct overlapData {
	SMALL	size;
	short selected;
	short deleted;
	short modified;
} OverlapData;

typedef struct TempoverlapData {
	SMALL	size;
	short selected;
	short deleted;
	short modified;
	short was_selected;
} TempOverlapData;


/*
 *	Composite data structure to tag to top level canvas
 */
typedef struct compositeData {
	SMALL	size;
	unsigned char **image;	/* Pointer to pc->arthur_negus */
} CompositeData;

/*
 * CANVAS data property list for Cmetblob type - see comment as to why this has been moved into here MC 11/Dec/02
 */
typedef struct Cblobdata_type{
    SMALL	size;				/* size of this structure in bytes */
    short	onum;				/* index to object list */
	short	label;				/* Metaphase Annotation label (no/temp/perm) */
    char	tag[10];			/* tag field */
} Cblobdata;

#pragma pack()

/* End of stuff from pcont.h ***********************************************/





//#ifdef WIN32

#ifdef __cplusplus
extern "C" {
#endif


/****** 24bitcanvas.cpp ******/

CANVAS_API unsigned int get_canvas_BG_col(Canvas *maincan);
CANVAS_API BOOL set_invert_canvas_BG_col(Canvas *maincan, unsigned int col);
CANVAS_API unsigned char *draw_overlap_mask_buffer(unsigned *canvas_buffer, int w, int h);
CANVAS_API unsigned char *draw_allzero_mask_buffer(unsigned *canvas_buffer, int w, int h);
CANVAS_API unsigned *draw_overlap_Canvas_buffer(Canvas *thiscan, DDGS *dg, int scale, int *x, int *y, int *w, int *h);
CANVAS_API unsigned *draw_fetal_Canvas_buffer(Canvas *thiscan, DDGS *dg, int scale, int *x, int *y, int *w, int *h);
CANVAS_API void Univision_counterstain_to_buffer(unsigned *buffer, Canvas *canvas, int w, int h);
CANVAS_API void Univision_probes_to_buffer(unsigned *buffer, Canvas *canvas, int w, int h);
CANVAS_API void Univision_probe_Canvas_buffer(unsigned *buffer, Canvas *thiscan);
CANVAS_API BOOL clip_8bit_buffer(unsigned char *indata, int *ox, int *oy, int *ow, int *oh, int clipxo, int clipyo, int w, int h);
CANVAS_API void setCanvasCounterstainAttenuation(int id, int attenuate_cs);


/****** Xcanvas.cpp ******/

CANVAS_API void box_selectorEH(Cselectdata *select_box, int x, int y, BOOL click);
CANVAS_API Canvas *box_selector(DDGS *dg, Canvas *canvas, short *canvas_types, int ntypes);
CANVAS_API void activate_box_selector(Cselectdata *select_box);
CANVAS_API void deactivate_box_selector(Cselectdata *select_box);
CANVAS_API struct polygondomain *get_lassoo_poly(struct pframe *f, DDGS *dg);
CANVAS_API void recursive_lassoo(Cselectdata *select_box, Canvas *canvas, Canvas * * *canvas_list, int *nselected, int *nallowed, struct object *lassoo_obj, int lassoo_dx, int lassoo_dy);
CANVAS_API Canvas * *lassoo_select_Canvas(short linestyle, Cselectdata *select_box, int *nselected);
CANVAS_API BOOL MetaphaseImage(Canvas *selcan);
CANVAS_API Canvas *GetMetblobcanvas(Canvas *canvas);
CANVAS_API Canvas *GetMetannocanvas(Canvas *canvas);
CANVAS_API BOOL IsMetAnnoLabelPermanent(Canvas *canvas);


/****** canvas.cpp ******/

CANVAS_API Canvas *create_Canvas(short type, Cframe *frame, struct pgrid *patchgrid, short visible, WZObj *Wobj,
	Canvas *parent, Canvas *previous, Canvas *next, Canvas *patch, struct propertylist *data);
CANVAS_API void debug_Canvas(char *title, Canvas *canvas);
CANVAS_API void destroy_Canvas_chain_kernel(Canvas *canvas, short destroy_Wobj);
CANVAS_API void destroy_Canvas_chain(Canvas *canvas);
CANVAS_API void destroy_Canvas_chain_and_Wobj(Canvas *canvas);
CANVAS_API void destroy_Canvas_kernel(Canvas *canvas, short destroy_Wobj);
CANVAS_API void destroy_Canvas(Canvas *canvas);
CANVAS_API void destroy_Canvas_and_Wobj(Canvas *canvas);
CANVAS_API void cut_Canvas(Canvas *canvas);
CANVAS_API void paste_Canvas(Canvas *canvas, Canvas *new_parent);
CANVAS_API void attach_Canvas(Canvas *canvas, Canvas *new_parent);
CANVAS_API void swap_Canvas(Canvas *canvasA, Canvas *canvasB);
CANVAS_API Canvas *dup_Canvas(Canvas *canvas, Canvas *parent, short copyWobj);
CANVAS_API Canvas *copy_Canvas(Canvas *canvas);
CANVAS_API Canvas *copy_Canvas_and_Wobj(Canvas *canvas);
CANVAS_API void modify_origin(int *newX, int *newY, Canvas *canvas);
CANVAS_API void reset_probe_frames(Canvas *thiscan, int border_width);
CANVAS_API void display_Wobj(Canvas *canvas, int _X, int _Y, int S, int A, Canvas *topcanvas);
CANVAS_API void recursive_display(Canvas *canvas, int X, int Y, int S, int A, int V, Canvas *topcanvas);
CANVAS_API void display_Canvas_chain(Canvas *canvas);
CANVAS_API BOOL display_Canvas(Canvas *canvas);
CANVAS_API void fscale_probe_canvas(Canvas *canvas, double scale, int keep_center);
CANVAS_API void fscale_Canvas_Wobj(Canvas *canvas, double scale);
CANVAS_API void recursive_fscale(Canvas *canvas, double scale);
CANVAS_API void fscale_Canvas_chain(Canvas *canvas, double scale);
CANVAS_API void fscale_Canvas(Canvas *canvas, double scale);
CANVAS_API void nothresh_fscale_Canvas(Canvas *canvas, double scale);
CANVAS_API void mode_fscale_Canvas(Canvas *canvas, double scale);
CANVAS_API void get_Canvas_DDGSframe(Canvas *canvas, DDGSframe *dgf);
CANVAS_API struct pframe *Canvas_Wobj_pframe(Canvas *canvas);
CANVAS_API BOOL display_Canvas_frame(Canvas *canvas, short arena, short col, int drawmode);
CANVAS_API void recursive_find(Canvas *canvas, int X, int Y, int S, int A, short *types, int ntypes, int fx, int fy, int closest);
CANVAS_API Canvas *select_Canvas(Canvas *canvas, short *types, int ntypes, int fx, int fy);
CANVAS_API Canvas *closest_Canvas(Canvas *canvas, short *types, int ntypes, int fx, int fy);
CANVAS_API void recursive_expose(Canvas *canvas, int X, int Y, int S, int A, DDGSframe *dgf, Canvas *topcanvas);
CANVAS_API BOOL expose_Canvas(Canvas *canvas, DDGSframe *dgf);
CANVAS_API void hide_Canvas_type(Canvas *canvas, short type);
CANVAS_API void unhide_Canvas_type(Canvas *canvas, short type);
CANVAS_API int canvas_has_probes(Canvas *can);
CANVAS_API void setFLEXprobeframes(int i);
CANVAS_API int getFLEXprobeframes();
CANVAS_API void EnableCanvasImageDisplay(BOOL enableDraw, BOOL enableErase, BOOL enableExpose);
CANVAS_API void EnableCanvasFrameDisplay(BOOL enableDraw, BOOL enableErase);


/****** filecanvas.cpp ******/

CANVAS_API Canvas *load_Canvas(FILE *fp);
CANVAS_API int save_Canvas(FILE *fp, Canvas *canvas);
CANVAS_API void validateWObj(Canvas *can);


/****** printcanvas.cpp ******/

CANVAS_API void create_PrintShell();
CANVAS_API Canvas *make_border(Canvas *canvas);
CANVAS_API int background_print(char *buf, int old_sig);


/****** rastercanvas.cpp ******/

//char *canvas2raster(/*Canvas *canvas, int *format, int width, int height, colour_lut *clut, colour_lut *olut, DDGS *dog*/);
CANVAS_API HBITMAP canvas2bitmap(Canvas *canvas, DDGS *dog);


/****** simplecanvas.cpp ******/

CANVAS_API Canvas *create_main_Canvas(short type, int dx, int dy, int width, int height);
CANVAS_API Canvas *create_grid_Canvas(short type, int dx, int dy, int width, int height, int vertical, int spacing);
CANVAS_API Canvas *create_object_Canvas(short type, int dx, int dy, struct object *Wobj, short arena, short colour, short drawmode);
CANVAS_API Canvas *create_object_in_grid_Canvas(short type, int dx, int dy, struct object *Wobj, short arena, short colour, short drawmode, short posingrid, short parent_site);


/****** ideocanvas.cpp ******/

#ifdef __cplusplus
#include "ideogramdll.h"
CANVAS_API void DisplayIdeogram(IdeoPolySet *pPolySet, int scale, int dx, int dy, BOOL backgroundIsBlack);
CANVAS_API struct object *CreateIdeogramWoolzObject(IdeoPolySet *pPolySet);
CANVAS_API Canvas *CreateIdeogramCanvasFromIdeogram(Ideogram *pIdeo, float width, float scale);
#endif
CANVAS_API Canvas *CreateIdeogramCanvas(struct propertylist *pData);
CANVAS_API BOOL IsCanvasNewIdeogram(Canvas *pCanvas);
CANVAS_API Canvas *RotateScaleNewIdeogramCanvas(Canvas *pOldCanvas, float rads, float scale);




#ifdef __cplusplus
}
#endif

//#endif

#endif




