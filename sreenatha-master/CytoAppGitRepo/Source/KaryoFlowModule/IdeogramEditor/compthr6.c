/*
 * compthr6.c		BP	11/20/95
 *
 * Copyright etc Applied Imaging corp. 1995
 *
 * Threshold calculation from histogram.
 * New algorithm which tries to find the lowest
 * reasonable threshold, not necessarily the
 * "best" for object segmentation! (but useful
 * for other purposes, I hope).
 *
 * Mods:
 *
 */

#include "woolz.h"

#include <stdio.h>
//#include <wstruct.h>


/*
 * Compute threshold as first point in histogram
 * that is less than a certain percentage of the
 * highest peak, but comes after the peak.
 * Result is a threshold [hopefully] that will
 * cause pretty much every object to be segmented
 * (but will probably also pick up some crap).
 * Can be used in situations where a minimum
 * sensible threshold is required.
 */


#ifdef WIN32
int
compthr6(struct object *histo)
#endif
#ifdef i386
int
compthr6(histo)
struct object *histo;
#endif
{
	int i, maxh, maxi, histmax, *pf;
	int fraction;
	struct histogramdomain *hdom;

	if (woolz_check_obj(histo, "compthr6") != 0)
		return(0);

	hdom = (struct histogramdomain *)histo->idom;
	pf = hdom->hv;
	histmax = hdom->npoints;

/* Find highest peak in histogram. */
	maxh = 0;
	for (i = 0; i < histmax; i++)
		if (pf[i] > maxh)
		{ 
			maxh = pf[i];	
			maxi = i;	
		}

/* If there was nothing there - or the peak is
 * at the end - return zero. */
	if ((maxh < 1) || (maxi > histmax - 2))
		return(0);

/* Start with 10%, and try to find a point
 * following maxi that is fraction% of maxh.
 * This is in a loop so we can reduce the
 * strictness of the test if such a point is
 * not found straight away. */
	fraction = 10;
	while (1)
	{
		for (i = maxi + 1; i < histmax; i++)
			if (pf[i] < maxh/fraction)
				break;
/* If we broke the loop, we have found
 * our threshold! */
		if (i < histmax)
			break;

/* If we have reached a silly point, quit. */
		if (--fraction < 2)
		{
			i = histmax;
			break;
		}
	}

/* If we reached the end without finding
 * anything, return zero. */
	if (i >= histmax)
		return(0);

	return(i);
}

