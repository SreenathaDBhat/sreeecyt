/*
 * curvat.c		Jim Piper		21 November 1984
 *
 * Compute the curvature of a polygon which is assumed to be
 * the boundary of a connected object, hence:
 *	(1) closed - first vertex = last vertex
 *	(2) adjacent vertices are 8-connected neighbours
 *
 * The output is produced as a histogram which is assumed to
 * map the polygon, thus the first and last points are the same.
 *
 * changes:
 *
 *	10 Mar 2000		JMB		Changed headers included, as part of conversion to DLL.
 *							Also, made minor mods to stop compiler warnings and removed
 *							standard C parameter lists (were '#ifdef i386' only).
 *  LJ 10-03-91  when boundary points less than 3, return NULL
 *	Ji Liang 7 Mar 86, '-' missing from '->' in polygon scanning code
 */

#include "woolz.h"

#include <stdio.h>
//#include <wstruct.h>

/*
 * "directions" encodes the 16 possible directions of a vector between
 * two next-but-adjacent points in the boundary, on the basis of the
 * difference in their line and column coordinates.
 * Note that if the inner square is used (will be a very rare
 * configuration, we hope) then the curvature coded in curv() below
 * will not be suitable.  I intend to ignore this on the grounds
 * that it will be a very rare event, and the error will be small.
 */
#define DIRECTION(i,j)	directions[i+2][j+2]
static int directions[5][5] = {
	{14,15, 0, 1, 2},
	{13,14, 0, 2, 3},
	{12,12,-1, 4, 4},
	{11,10, 8, 6, 5},
	{10, 9, 8, 7, 6}
};

/*
 * CURVATURE encodes the curvature between back and forward two-point
 * vectors from point k.  It is coded as 10*(degrees per unit length),
 * and is computed by a program whose source is ../aux/curvat.f
 */
static int CURVATURE[4][16] = {
	{0,51,93,153,225,255,280,357,450,-357,-280,-255,-225,-153,-93,-51},
	{-51,0,43,93,153,186,215,280,357,373,-300,-280,-255,-186,-129,-93},
	{-93,-43,0,43,93,129,159,215,280,300,318,-300,-280,-215,-159,-129},
	{-153,-93,-43,0,51,93,129,186,255,280,300,373,-357,-280,-215,-186}
};


struct object *
curvat(struct object *ubound)
{
	struct object *curv, *makemain();
	struct polygondomain *pdom;
	struct histogramdomain *hdom, *makehistodmn();
	register struct ivertex *utx, *vtx, *wtx;
	int backdir, forwdir;
	register int i;
	int *hv;

	/*
	 * check for closed polygon.
	 * should really check vertex adjacency,
	 * but this would take a long time.
	 */
	pdom = (struct polygondomain *)ubound->idom;
	vtx = pdom->vtx;
	wtx = vtx + pdom->nvertices - 1;
	if (vtx->vtX != wtx->vtX || vtx->vtY != wtx->vtY) {
		fprintf(stderr,"CURVAT: warning - not a closed polygon %d %d %d %d\n", vtx->vtX, wtx->vtX, vtx->vtY, wtx->vtY);
	}
	if(pdom->nvertices < 4)
		return(NULL);

	/*
	 * make a histogram
	 */
	hdom = makehistodmn(1,pdom->nvertices);
	hv = hdom->hv;
	hdom->r = 1;
	hdom->k = 0;
	hdom->l = 0;
	curv = makemain(13, (struct intervaldomain *)hdom, NULL, NULL, NULL);

	/*
	 * scan the polygon, computing direction vector from point
	 * two points behind current point (16 values), and direction vector to
	 * point two points ahead (16 values).  Normalise backward direction
	 * to lie in just one quadrant (4 values), adjust forward direction
	 * for compatibility, look up curvature in 4 by 16 table.
	 * Fill in the histogram.
	 */
	wtx -= 2;
	utx = vtx + 2;
	for (i=0; i<pdom->nvertices; i++) {

		/* handle the wrap-round */
		if (i == 2)
			wtx = pdom->vtx;
		if (i == pdom->nvertices-3)
			utx = pdom->vtx;

		/* compute raw directons */
		backdir = DIRECTION(vtx->vtX-wtx->vtX, vtx->vtY-wtx->vtY);

		/* Ji Liang missing '-' in utx->vtX below (prev utx>vtX) */
		forwdir = DIRECTION(utx->vtX-vtx->vtX, utx->vtY-vtx->vtY);

		/* normalise the directions */
		while (backdir > 3) {
			backdir -= 4;
			forwdir -= 4;
		}
		if (forwdir < 0)
			forwdir += 16;

		/* fill in the curvature */
		*hv++ = CURVATURE[backdir][forwdir];

		/* increment things */
		utx++;
		vtx++;
		wtx++;
	}
	return(curv);
}
