/*
 *	D D G S . C P P    X-windows/DDGS
 *
 * 			Mark Gregson
 *			Applied Imaging
 *
 *	Date:		3rd August 1992
 *
 * Mods:
 *
 *	09Mar2000	JMB	Converted to .cpp.
 *	07Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *	26Jul99		MG	Add ddgs NULL checking at start of a number of routines
 *	03Jul99		WH:	Need to increase the dimension limits for the dggd in ddgs_core()
 *					as we now have large canvases and we require a ddgs for the
 *					full size in printing (added DDGS_MAXWIDTH + HEIGHT)
 *	15Feb99		KS:	New functions to Set and Get the normal and inverted
 *					background colour, now part of the ddgs structure. Plus
 *					additions to clear() invert_clear() erase() and invert_erase()
 *					to use the new background colour stuff.
 *	24Apr97		WH:	Afraid we do so, clipping is back by popular demand
 *	20Mar97		BP:	No clip region... no need?
 *	18Mar97		BP:	Initialise mask BITMAPINFO structure now in
 *					DDGS struct,
 *	10Jan97		BP:	Initialise ddgs_xxxx colour values and palette handle
 *					to something rational.
 *					Only free draw buffer on #ifdef DDGSBUFFERED in ddgsclose!
 *
 *	BP	1/3/97:		No setfont() - ddgstext takes care of everything.
 *	BP	12/24/96:	Only include ddgsImageXXXX (ie buffered display)
 *					based on DDGSBUFFERED define.
 *	BP	9/24/96:	Windows version.
 *
 *	BP	5/28/96:	Make pseudo-color background brighter on 24-bit
 *					displays (using new PSEUDOBGRGB value).
 *	BP	8/24/95:	Allow for NULL window. Note that this does not apply
 *					to event handlers etc - only drawing primitives.
 *	BP	5/10/95:	Provide option to use 8-bit dog on Univision
 *					systems for printing. New function ddgspropen_8bit
 *					and new parameter to ddgs_core().
 *
 *	9/14/94	BP	Assign depth in ddgsopen (for 24-bit).
 *				Initialise curdg to NULL, and add checks.
 *				Don't assign LUTs if 24-bit.
 *				Alls orts of stuff for 24-bit.
 *
 *	4 Mar 1993	MG	fixed bug in ddgsclose added invert_clear()
 *     19 Jan 1993	MG	VGA cannot XStoreColors
 *     16 Dec 1992 	MG	added check for unmapped ddgs window - ddgsismapped()
 *     13 Oct 1992	MG	intens(2) gives exclusive-or drawing - for overlays	
 *	2 Sep 1992	MG	Marker distructive overlays added
 *	8 Aug 1992	MG	DDGS code broken into several modules
 *	5 Aug 1992	MG	Overlay plane stuff added - ddgsovly
 *
 */


#include "ddgs.h"
#include "ddgsdefs.h"

#include <stdio.h>
//#include <wstruct.h>
//#include "ddgs.h"



#define DDGS_MAXWIDTH	2024
#define DDGS_MAXHEIGHT	2024

/*
 * Global palette variables for use by
 * the application.
 */
HPALETTE ddgs_palette = NULL;
COLORREF ddgs_cornsilk = RGB(255, 255, 255);
COLORREF ddgs_darkslategrey = RGB(128, 129, 128);
COLORREF ddgs_lightslategrey = RGB(192, 192, 192);
COLORREF ddgs_banana = RGB(255, 255, 0);


/*
 * Globals accessible throughout DDGS
 * (declared exte4rn in ddgsdefs.h)
 */
DDGS *curdg = NULL;		/* current ddgs in use */

static DDGSLOGPALETTE pseudo_LUT;	/* Basic pseudocolour display LUT. */
static DDGSLOGPALETTE grey_LUT;	/* Basic grey-scale display LUT. */


/*
 *	Marker RGB values. Used to be in common.c!!!
 */
static BYTE marker_RGB[][3] = {
	{0, 0, 0},			/* BLACK */
	{0, 0, 255},		/* BLUE */
	{0, 220, 0},		/* GREEN */
	{0, 220, 220},		/* CYAN */
	{255, 0, 0},		/* RED */
	{255, 0, 255},		/* MAGENTA */
	{220, 220, 0},		/* YELLOW */
	{255, 255, 255},	/* WHITE */
	{127, 127, 127},	/* MIDGREY */
	{0, 160, 0},		/* DARKGREEN */
	{208, 32, 144},		/* VIOLETRED */
	{205, 149, 12},		/* DARKGOLDENROD2 */
	{0, 127, 255},		/* VARCOLOUR */
	{255, 255, 191},	/* VARCOLOUR2 */
	{255, 255, 127},	/* spare */
	{255, 255, 63}		/* spare */
};
#define	NMARKERCOLOURS	(sizeof(marker_RGB)/3)


/*
 *  GUI RGB values. Not used directly by DDGS,
 * but DDGS is responsible for managing the app's
 * palette.
 */
static BYTE GUI_RGB[][3] = {
	{0xff, 0xf8, 0xdc},	/* CORNSILK */
	{0x35, 0x66, 0x8a},	/* DARKSLATEGREY */
	{0x45, 0x82, 0xb7},	/* LIGHTSLATEGREY */
	{0xff, 0xff, 0}		/* BANANA */
};
#define	NGUICOLOURS		(sizeof(GUI_RGB)/3)


/* 
 * 	C M A P I N I T
 *
 * 	Internal DDGS routine -- initialises global cmap for display.
 *	We need colour cells for pixel values 64-127  -
 *  these pixel values are required if proper
 *	pixel logical ops ( OR, XOR, AND )
 *	are to be performed on pixmap and window data.
 * 	So dump exisiting colormap but retain colors up to pixel value 63.
 *
 * BP - do nothing if we are 24-bit.
 * NOTE: also added check for NULL curdg.
 */

void
ddgsInit()
{
	int i, j;
	static BOOL initialised = FALSE;
	DDGSLOGPALETTE lut;

	// Only do this once.
	if (initialised)
		return;
	initialised = TRUE;

	// Create grey-scale LUT with 256 entries.
	grey_LUT.version = 0x300;
	grey_LUT.numEntries = 256;
	for (i = 0; i < 256; i++)
	{
		grey_LUT.val[i].peRed = i;
		grey_LUT.val[i].peGreen = i;
		grey_LUT.val[i].peBlue = i;
		grey_LUT.val[i].peFlags = 0;
	}

	// Create pseudo-colour LUT with 256 entries.
	pseudo_LUT.version = 0x300;
	pseudo_LUT.numEntries = 256;
	for (i = 0; i < 52; i++)
	{
		j = (int)((double)i*255.0/51.0);
		pseudo_LUT.val[i].peRed = 0;
		pseudo_LUT.val[i].peGreen = j;
		pseudo_LUT.val[i].peBlue = 255;
		pseudo_LUT.val[i].peFlags = 0;
	}
	for ( ; i < 103; i++)
	{
		j = (int)((103.0 - (double)i)*255.0/51.0);
		pseudo_LUT.val[i].peRed = 0;
		pseudo_LUT.val[i].peGreen = 255;
		pseudo_LUT.val[i].peBlue = j;
		pseudo_LUT.val[i].peFlags = 0;
	}
	for ( ; i < 154; i++)
	{
		j = (int)(((double)i - 103.0)*255.0/51.0);
		pseudo_LUT.val[i].peRed = j;
		pseudo_LUT.val[i].peGreen = 255;
		pseudo_LUT.val[i].peBlue = 0;
		pseudo_LUT.val[i].peFlags = 0;
	}
	for ( ; i < 205; i++)
	{
		j = (int)((205.0 - (double)i)*255.0/51.0);
		pseudo_LUT.val[i].peRed = 255;
		pseudo_LUT.val[i].peGreen = j;
		pseudo_LUT.val[i].peBlue = 0;
		pseudo_LUT.val[i].peFlags = 0;
	}
	for ( ; i < 256; i++)
	{
		j = (int)(((double)i - 205.0)*255.0/51.0);
		pseudo_LUT.val[i].peRed = 255;
		pseudo_LUT.val[i].peGreen = 0;
		pseudo_LUT.val[i].peBlue = j;
		pseudo_LUT.val[i].peFlags = 0;
	}

	// Create the actual display palette.
	lut.version = 0x300;
	lut.numEntries = 0;
	i = 0;
	// This has:
	// 64 greyscale entries...
	for ( ; i < lut.numEntries + 64; i++)
		lut.val[i] = grey_LUT.val[4*i];
	// ...plus 64 pseudocolour entries...
	lut.numEntries = i;
	for ( ; i < lut.numEntries + 64; i++)
		lut.val[i] = pseudo_LUT.val[4*(i - lut.numEntries)];
	// ...plus the markers....
	lut.numEntries = i;
	for ( ; i < lut.numEntries + (int)NMARKERCOLOURS; i++)
	{
		lut.val[i].peRed = marker_RGB[i - lut.numEntries][0];
		lut.val[i].peGreen = marker_RGB[i - lut.numEntries][1];
		lut.val[i].peBlue = marker_RGB[i - lut.numEntries][2];
		lut.val[i].peFlags = 0;
	}
	// ...plus some GUI colours.
	ddgs_cornsilk = PALETTEINDEX(i);
	ddgs_darkslategrey = PALETTEINDEX(i + 1);
	ddgs_lightslategrey = PALETTEINDEX(i + 2);
	ddgs_banana = PALETTEINDEX(i + 3);
	lut.numEntries = i;
	for ( ; i < lut.numEntries + (int)NGUICOLOURS; i++)
	{
		lut.val[i].peRed = GUI_RGB[i - lut.numEntries][0];
		lut.val[i].peGreen = GUI_RGB[i - lut.numEntries][1];
		lut.val[i].peBlue = GUI_RGB[i - lut.numEntries][2];
		lut.val[i].peFlags = 0;
	}
	lut.numEntries = i;

	ddgs_palette = CreatePalette((LOGPALETTE *)&lut);

	// Make sure all the fonts are loaded up, and
	// select the default. BP: actually does nothing now!
	init_fonts();
}


/*
 * ddgsExposeWindow
 */

void
ddgsExposeWindow(DDGS *dg)
{
}


/*
 * ddgsColormapChange
 */

void
ddgsColormapChange()
{
}


/*
 * ddgsSetBGColour
 */

void
ddgsSetNormalBGColour(unsigned int col)
{
	if (!curdg)
		return;

	curdg->normalBG= col;
}

void
ddgsSetInvertBGColour(unsigned int col)
{
	if (!curdg)
		return;

	curdg->invertBG= col;
}


/*
 * ddgsGetBGColour
 */

void
ddgsGetNormalBGColour(unsigned int *col)
{
	if (!curdg)
		return;

	*col = curdg->normalBG;
}

void
ddgsGetInvertBGColour(unsigned int *col)
{
	if (!curdg)
		return;

	*col = curdg->invertBG;
}


/******************************************** ddgs_core */

/*
 * Create and initialise core of DOGS structure.
 * This is required by ddgsopen() and ddgspropen(),
 * so this function to avoids code duplication.
 */

static DDGS *
ddgs_core(HWND window, int width, int height)
{
	DDGS *temp;
	RECT rect;
	int i;

	/* Allocate DDGS struct. */
	if ((temp = (DDGS *)Calloc(sizeof(DDGS), 1)) == NULL)
	{
		fprintf(stderr, "ddgs: error allocating DDGS structure.\n");
		return(NULL);
	}

	/* Make sure its a sensible size. */
	if ((width > DDGS_MAXWIDTH) || (width < 1))
		width = DDGS_MAXWIDTH;
	if ((height > DDGS_MAXHEIGHT) || (height < 1))
		height = DDGS_MAXHEIGHT;	

	/* Set up ddgs structure */
	temp->window		= window;
	temp->dc			= NULL;
	temp->minx			= 0;
	temp->miny			= 0;
	temp->maxx			= width - 1;
	temp->maxy			= height - 1;
	temp->invert		= FALSE;
	temp->backup		= FALSE;
	temp->ovlycol		= RGB(0, 0, 255);
	temp->pseudocolour	= OFF;
	temp->shape			= 0;
	temp->map			= NULL;
	temp->xshift		= 3;
	temp->yshift		= 3;
	temp->himage		= NULL;	// Used only for buffered display.
	temp->invertBG		= 0x00000000;
	temp->normalBG		= 0x00ffffff;

	/* Assign the marker colours, and create
	 * the marker pens. */
	temp->cur_lwidth = 0;
	for (i = 0; i < NMARKERCOLOURS; i++)
	{
		// Note that the PALETTERGB macro is used
		// in preference to RGB (which would apply
		// the nearest color from the 20 base colours
		// on an 8-bit display), and PALETTEINDEX
		// (which may [??] not work on 24-bit
		// displays, where palettes are ignored).
		// PALETTERGB means the nearest colour
		// will be found in the DC's whole palette.
		temp->markcol[i] = PALETTERGB(marker_RGB[i][0], marker_RGB[i][1], marker_RGB[i][2]);
		temp->markpen[i] = CreatePen(PS_SOLID, temp->cur_lwidth, temp->markcol[i]);
	}

	temp->ovlypen = CreatePen(PS_SOLID, temp->cur_lwidth,
		DOG_OVLYMASK(temp));

	temp->blackpen = CreatePen(PS_SOLID, temp->cur_lwidth,
		PALETTERGB(0, 0, 0));
	temp->whitepen = CreatePen(PS_SOLID, temp->cur_lwidth,
		PALETTERGB(255, 255, 255));

	temp->cur_marker = 0;
	temp->cur_pixelmode = 1;

	/* Set up ddgs clipping window to entire
	 * window, and create the clip region. */
	if (window)
	{
		GetClientRect(window, &rect);
		if (rect.left + width < rect.right)
			rect.right = rect.left + width;
		if (rect.top + height < rect.bottom)
			rect.bottom = rect.top + height;

		temp->cliprgn = CreateRectRgn(rect.left, rect.top,
			rect.right, rect.bottom);
	}
	else
	{
		rect.left = 0;
		rect.top = 0;
		rect.right = rect.left + width;
		rect.bottom = rect.top + height;

		temp->cliprgn = NULL;
	}


	/* Setup the BITMAPINFO structure, used
	 * for DIB drawing. The w, h & bits fields
	 * will be modified on the fly, as required. */
	temp->binfo.head.biSize = sizeof(BITMAPINFOHEADER);
	temp->binfo.head.biWidth = width;
	temp->binfo.head.biHeight = -height;
	temp->binfo.head.biPlanes = 1;
	temp->binfo.head.biBitCount = 0;		// Let ddgsImageDepth set this up.
	temp->binfo.head.biCompression = BI_RGB;
	temp->binfo.head.biSizeImage = 0;
	temp->binfo.head.biXPelsPerMeter = 1000;
	temp->binfo.head.biXPelsPerMeter = 1000;
	temp->binfo.head.biClrUsed = 0;
	temp->binfo.head.biClrImportant = 0;

	// Initialise binfo colourmap. Only used by
	// 8-bit drawing (ie ignored when binfo.bits
	// is 24, which will be the case for probes).
	for (i = 0; i < 256; i++)
		temp->binfo.col[i].rgbReserved = 0;

	// Copy initial settings into the mask image.
	temp->minfo = temp->binfo;

	// The mask has 255 as colour[0] and 0
	// in all other entries. These are used for
	// mask drawing using boolean operators (see
	// ddgspix.c).
	temp->minfo.col[0].rgbRed = 255;
	temp->minfo.col[0].rgbGreen = 255;
	temp->minfo.col[0].rgbBlue = 255;
	for (i = 1; i < 255; i++)
	{
		temp->minfo.col[i].rgbRed = 0;
		temp->minfo.col[i].rgbGreen = 0;
		temp->minfo.col[i].rgbBlue = 0;
	}

#ifdef	DDGSBUFFERED
	// Initially just 8-bit. Call this here to
	// ensure buffer is allocated. Note that
	// binfo.bits must be set to 0 above so this
	// call will do something.
	ddgsImageDepth(temp, 8);
#endif

	return(temp);
} /* ddgs_core */


/*
 * ddgsopen
 *
 * Takes display widget from caller and returns ID of ddgs structure
 * All future WOOLZ/DDGS display output will be sent to the display widget's
 * window. This routine clears the window, sets up a linear LUT, 
 * and links a colour map change event handler to the display widget. 
 * If the function is successful it returns a pointer to a DDGS structure
 * otherwise NULL.
 * The width and height specify the maximum dimensions of the display area.
 * The display widget can be resized but the display area remains the same.
 * This routine doesn't create a backup pixmap (call backup(TRUE) for this).
 */

DDGS *
ddgsopen(HWND window, int width, int height)
{
	/* Make sure all pre-initialisation has been done. */
	ddgsInit();

	/* Create the basic structure. */
	if ((curdg = ddgs_core(window, width, height)) == NULL)
		return(NULL);

	/* Set some defaults. */
	colour(255);

	return(curdg);
} /* ddgsopen */


/*
 * ddgspropen - Special version of ddgsopen for printing.
 *
 * Same as ddgsopen, except it accepts an HDC instead of
 * a HWND. The dc member of DDGS is used in place of
 * GetDC(HWND) if the window is NULL.
 */

DDGS *
ddgspropen(HDC dc, int width, int height)
{
	/* Make sure all pre-initialisation has been done. */
	ddgsInit();

	/* Create the basic structure. */
	if ((curdg = ddgs_core(NULL, width, height)) == NULL)
		return(NULL);

	/* Attach the dc. */
	curdg->dc = dc;

	/* Make sure the dc has the correct palette. */
	SelectPalette(dc, ddgs_palette, 0);
	RealizePalette(dc);

	/* Set some defaults. */
	colour(255);

	return(curdg);
} /* ddgspropen */


/*
 *	D D G S I S M A P P E D
 *
 *	Returns true when ddgs window is mapped - must test for
 *	this before using routines such as wsave(), and pixline
 *	and pixel when pixelmode=3 - as these routines call XGetImage.
 *	If no window is mapped XGetImage fails and crashes the program.
 */

BOOL
ddgsismapped()
{
	if (curdg == NULL)
		return(FALSE);

	/* If its a printer dog, pretend its always
	 * visible. */
	if (curdg->window == NULL)
		return TRUE;

	return(IsWindowVisible(curdg->window));
}


/* 
 * 	S E L E C T D D G S 
 *
 * 	Select ddgs window to work on - all future operations apply
 *	to this ddgs window. The window, graphics context, backup
 *	pixmap etc. are passed in the DDGS structure.
 */ 

void
selectddgs(DDGS *dg)
{
	HDC dc;

	curdg = dg;

	if (!curdg)		// MG 26Jul99
		return;

	dc = DOG_DC(curdg);
	SelectPalette(dc, ddgs_palette, 0);
	RealizePalette(dc);
	DOG_RDC(curdg);
}


/*
 * 	D D G S D U P  --  original DDGS routine, effectively
 *	same function as selectddgs
 */

DDGS *
ddgsdup(DDGS *dg)
{
	selectddgs(dg);
	return(dg);
}


/* 	G E T C U R D G -- returns pointer to current ddgs structure
 *
 */
DDGS *
getcurdg()
{
	return(curdg);
}


/*
 * ddgsclose
 *	
 * 	Destroy the pixmap and graphics context associated with a DDGS and free
 *      objects with allocated memory
 *
 */

void
ddgsclose(DDGS *dg)
{
	int i;
	DDGS *tempdg;
	
	/* deallocate backup pixmap and switch off expose event handler */
	tempdg = curdg;
	curdg = dg;
	backup(FALSE);
	curdg = tempdg;

	// Delete the clip region.
	if (dg->cliprgn)
		DeleteObject(dg->cliprgn);

	// Free memory allocated for mousedraw.
	if (dg->mousepts)
		free(dg->mousepts);

	// Delete all the marker pens.
	for (i = 0; i < NMARKERCOLOURS; i++)
		DeleteObject(dg->markpen[i]);
	DeleteObject(dg->ovlypen);
	DeleteObject(dg->blackpen);
	DeleteObject(dg->whitepen);

#ifdef	DDGSBUFFERED
	GlobalFree(dg->himage);
#endif

	// Protect continued access.
	if (curdg == dg)
		curdg = NULL;

	Free((char *)dg);
}


/*
 * backup
 *	If status is FALSE all ddgs operations only
 *	effect the curdg window - this saves time.
 *	When status is TRUE backup initially copies
 *	window to the curdg backup pixmap, thereafter
 *	all ddgs operations affect the backup pixmap
 *	and the window. An event handler is switched
 *	on to handle copying of backup map data to
 *	the window when expose events occur.
 */

void
backup(int status)
{
// Not used.
}


/*
 * ifill
 *
 *	Fills window area, ignores current clip area set by window()
 *	and drawing mode set by intens() - these remain unchanged
 */

void
ifill(int x, int y, int w, int h, int val)
{
	HBRUSH brush;
	RECT rect;
	HDC dc;

	if (curdg == NULL)
		return;

	rect.left = x >> 3;
	rect.top = curdg->maxy - ((y + h) >> 3);
	rect.right = (x + w) >> 3;
	rect.bottom = rect.top + (h >> 3);

	// Create a brush that nominally maps
	// its colour from the 64-level grey
	// range at the base of the palette.
	brush = CreateSolidBrush(PALETTEINDEX(val/4));
	dc = DOG_DC(curdg);
	FillRect(dc, &rect, brush);
	DOG_RDC(curdg);
	DeleteObject(brush);
}


/*
 * erase
 * New function - erase area of dog. This
 * works just like clear(), but on a specific
 * area. Intended for use with erasing canvas
 * display - application must ensure expose
 * events are installed so that clearing this
 * area doesn't wipe out other objects.
 * See also invert_erase().
 */

void
erase(int x, int y, int w, int h)
{
	RECT rect;
	HBRUSH brush;
	HDC dc;
	unsigned char r,g,b;

	if (curdg == NULL)
		return;

	rect.left = x >> 3;
	rect.top = curdg->maxy - ((y + h - 3) >> 3);
	rect.right = (x + w) >> 3;
	rect.bottom = rect.top + ((h + 3) >> 3);

	dc = DOG_DC(curdg);
	if (curdg->pseudocolour == ON)
	{
		SelectPalette(dc, ddgs_palette, 0);
		RealizePalette(dc);
		brush = CreateSolidBrush(PSEUDOBGRGB);
	}
	else {
		r = (unsigned char)((curdg->normalBG & 0x00ff0000)>>16);
		g = (unsigned char)((curdg->normalBG & 0x0000ff00)>>8);
		b = (unsigned char)((curdg->normalBG & 0x000000ff));
		brush = CreateSolidBrush( RGB( r, g, b) ); /* KS GBCOL */
	}
//		brush = GetStockObject(WHITE_BRUSH);
	FillRect(dc, &rect, brush);
	DOG_RDC(curdg);
	DeleteObject(brush);

#ifdef	DDGSBUFFERED
	ddgsImageErase(curdg, 0, &rect);
#endif
} /* erase */



/*
 * clear
 * Clear grey image and overlay - sets plane 6
 */

void
clear()
{
	RECT rect;
	HBRUSH brush;
	HDC dc;
	unsigned char r,g,b;

	if (curdg == NULL)
		return;

	dc = DOG_DC(curdg);
	if (curdg->pseudocolour == ON)
	{
		SelectPalette(dc, ddgs_palette, 0);
		RealizePalette(dc);
		brush = CreateSolidBrush(PSEUDOBGRGB);
	}
	else
	{
		r = (unsigned char)((curdg->normalBG & 0x00ff0000)>>16);
		g = (unsigned char)((curdg->normalBG & 0x0000ff00)>>8);
		b = (unsigned char)((curdg->normalBG & 0x000000ff));
		brush = CreateSolidBrush( RGB(r, g, b) ); /* KS GBCOL */
	}
//		brush = GetStockObject(WHITE_BRUSH);
	rect.left = curdg->minx;
	rect.right = curdg->maxx + 1;
	rect.top = curdg->miny;
	rect.bottom = curdg->maxy + 1;
	FillRect(dc, &rect, brush);
	DOG_RDC(curdg);
	DeleteObject(brush);

#ifdef	DDGSBUFFERED
	ddgsImageClear(curdg, 0);
#endif
}


/*
 * invert_erase
 * New function - erase area of dog.
 * See also erase().
 */

void
invert_erase(int x, int y, int w, int h)
{
	RECT rect;
	HBRUSH brush;
	HDC dc;
	unsigned char r,g,b;

	if (curdg == NULL)
		return;

	rect.left = x >> 3;
	rect.top = curdg->maxy - ((y + h - 3) >> 3);
	rect.right = (x + w) >> 3;
	rect.bottom = rect.top + ((h + 3) >> 3);

	dc = DOG_DC(curdg);
	if (curdg->pseudocolour == ON)
	{
		SelectPalette(dc, ddgs_palette, 0);
		RealizePalette(dc);
		brush = CreateSolidBrush(PSEUDOBGRGB);
	}
	else
	{
		r = (unsigned char)((curdg->invertBG & 0x00ff0000)>>16);
		g = (unsigned char)((curdg->invertBG & 0x0000ff00)>>8);
		b = (unsigned char)((curdg->invertBG & 0x000000ff));
		brush = CreateSolidBrush( RGB(r, g, b) ); /* KS GBCOL */
	}
//		brush = GetStockObject(BLACK_BRUSH);
	FillRect(dc, &rect, brush);
	DOG_RDC(curdg);
	DeleteObject(brush);

#ifdef	DDGSBUFFERED
	ddgsImageErase(curdg, 0, &rect);
#endif
} /* invert_erase */




/*
 * invert_clear
 * Set grey image, clear overlay, set plane 6
 *
 */

void
invert_clear()
{
	RECT rect;
	HBRUSH brush;
	HDC dc;
	unsigned char r,g,b;

	if (curdg == NULL)
		return;

	dc = DOG_DC(curdg);
	if (curdg->pseudocolour == ON)
	{
		SelectPalette(dc, ddgs_palette, 0);
		RealizePalette(dc);
		brush = CreateSolidBrush(PSEUDOBGRGB);
	}
	else
	{
		r = (unsigned char)((curdg->invertBG & 0x00ff0000)>>16);
		g = (unsigned char)((curdg->invertBG & 0x0000ff00)>>8);
		b = (unsigned char)((curdg->invertBG & 0x000000ff));
		brush = CreateSolidBrush( RGB(r, g, b) ); /* KS GBCOL */
	}
//		brush = GetStockObject(BLACK_BRUSH);
	rect.left = curdg->minx;
	rect.right = curdg->maxx + 1;
	rect.top = curdg->miny;
	rect.bottom = curdg->maxy + 1;
	FillRect(dc, &rect, brush);
	DOG_RDC(curdg);
	DeleteObject(brush);

#ifdef	DDGSBUFFERED
	ddgsImageClear(curdg, 0);
#endif
}


/*
 * intens
 * Set whether to draw (intens(1)) or erase (intens(0))
 */

void
intens(int val)
{
	int i;

	if (curdg == NULL)
		return;

	// Do nothing if no change (to save time).
	if ((curdg->invert == FALSE) && (curdg->cur_intensity == val))
		return;
	curdg->invert = FALSE;
	curdg->cur_intensity = val;

	if (curdg->pseudocolour)
	{
		curdg->binfo.col[0].rgbRed = PSEUDOBGRGB & 0xff;
		curdg->binfo.col[0].rgbGreen = (PSEUDOBGRGB & 0xff00) >> 8;
		curdg->binfo.col[0].rgbBlue = (PSEUDOBGRGB & 0xff0000) >> 16;
		for (i = 1; i < 256; i++)
		{
			curdg->binfo.col[i].rgbRed = pseudo_LUT.val[255 - i].peRed;
			curdg->binfo.col[i].rgbGreen = pseudo_LUT.val[255 - i].peGreen;
			curdg->binfo.col[i].rgbBlue = pseudo_LUT.val[255 - i].peBlue;
		}
	}
	else
	{
		for (i = 0; i < 256; i++)
		{
			curdg->binfo.col[i].rgbRed = grey_LUT.val[255 - i].peRed;
			curdg->binfo.col[i].rgbGreen = grey_LUT.val[255 - i].peGreen;
			curdg->binfo.col[i].rgbBlue = grey_LUT.val[255 - i].peBlue;
		}
	}
}


/*
 * invert_intens
 * Create an linear grey mapping LUT for
 * drawing inverted grey-scale objects.
 */

void
invert_intens()
{
	int i;

	if (curdg == NULL)
		return;

	curdg->invert = TRUE;

	if (curdg->pseudocolour)
	{
		curdg->binfo.col[0].rgbRed = PSEUDOBGRGB & 0xff;
		curdg->binfo.col[0].rgbGreen = (PSEUDOBGRGB & 0xff00) >> 8;
		curdg->binfo.col[0].rgbBlue = (PSEUDOBGRGB & 0xff0000) >> 16;
		for (i = 1; i < 256; i++)
		{
			curdg->binfo.col[i].rgbRed = pseudo_LUT.val[i].peRed;
			curdg->binfo.col[i].rgbGreen = pseudo_LUT.val[i].peGreen;
			curdg->binfo.col[i].rgbBlue = pseudo_LUT.val[i].peBlue;
		}
	}
	else
	{
		for (i = 0; i < 256; i++)
		{
			curdg->binfo.col[i].rgbRed = grey_LUT.val[i].peRed;
			curdg->binfo.col[i].rgbGreen = grey_LUT.val[i].peGreen;
			curdg->binfo.col[i].rgbBlue = grey_LUT.val[i].peBlue;
		}
	}
}


/*
 * restore_intens
 * restore current ddgs drawing mode
 */

void
restore_intens()
{
	if (curdg == NULL)
		return;

	intens(curdg->cur_intensity);
}


/*
 * Switch pseudocolour on.
 * Must be followed by an intens() etc to
 * take effect.
 */

void
pseudodg()
{
	if (curdg == NULL)
		return;

	// If it used to be off, need to
	// recreate black/white pens.
	if (curdg->pseudocolour == OFF)
	{
		DeleteObject(curdg->whitepen);
		DeleteObject(curdg->blackpen);
		curdg->blackpen = CreatePen(PS_SOLID, curdg->cur_lwidth,
			PSEUDOBGRGB);
		curdg->whitepen = CreatePen(PS_SOLID, curdg->cur_lwidth,
			PSEUDOBGRGB);
	}

	curdg->pseudocolour = ON;

	// Force re-assignment of LUTs.
	curdg->cur_intensity = -1;
	intens(1);
}


/*
 * Switch pseudocolour off.
 * Must be followed by an intens() etc to
 * take effect.
 */

void
monodg()
{
	if (curdg == NULL)
		return;

	// If it used to be on, need to
	// recreate black/white pens.
	if (curdg->pseudocolour == ON)
	{
		DeleteObject(curdg->whitepen);
		DeleteObject(curdg->blackpen);
		curdg->blackpen = CreatePen(PS_SOLID, curdg->cur_lwidth,
			PALETTERGB(0, 0, 0));
		curdg->whitepen = CreatePen(PS_SOLID, curdg->cur_lwidth,
			PALETTERGB(255, 255, 255));
	}

	curdg->pseudocolour = OFF;

	// Force re-assignment of LUTs.
	curdg->cur_intensity = -1;
	intens(1);
}


/*
 * colour
 * Select bit planes to draw into.
 * Not normally used.
 */
void
colour(int col)
{
	if (curdg == NULL)
		return;

	curdg->cur_colour = col;
}


/*
 * window
 * Set clipping area for current ddgs window
 */
void
window(int xl, int xr, int yb, int yt)
{
	RECT rect;

	if (curdg == NULL)
		return;

	rect.left = xl >> 3;
	rect.top = curdg->maxy - (yt >> 3);
	rect.right = xr >> 3;
	rect.bottom = curdg->maxy - (yb >> 3);

	if (rect.right <= rect.left)
		rect.right = rect.left + 1;
	if (rect.bottom <= rect.top)
		rect.bottom = rect.top + 1;

	// Do nothing if this is a non-window (ie
	// printer) DC.
	if (curdg->cliprgn)
	{
		DeleteObject(curdg->cliprgn);
		curdg->cliprgn = CreateRectRgn(rect.left, rect.top,
			rect.right, rect.bottom);
	}
}
