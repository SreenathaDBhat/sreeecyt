/*
 *	ddgsimage.c			BP	10/24/96
 *
 * Copyright etc Applied Imaging corp. 1996
 *
 * Mods:
 *
 *	09Mar2000	JMB	Converted to .cpp.
 *	07Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *	24Dec96		BP:	Include ddgsdefs.h, and do nothing unless
 *					DDGSBUFFERED is defined (ie support for un-
 *					buffered display).
 */



#include "ddgsdefs.h"


//#include "ddgs.h"
//#include <ddgsdefs.h>


#ifdef	DDGSBUFFERED


/*
 * ddgsImageDepth
 *
 * Allocate the appropriate size buffer, and
 * setup the DIB.
 * If there is already a buffer, it is freed.
 * The ddgs->depth is taken into account, and
 * must therefore be set before calling this!
 */

void ddgsImageDepth(DDGS *dg, short depth)
{
	if (dg == NULL)
		return;

	// Do nothing if theres no change.
	if (dg->binfo.head.biBitCount == depth)
		return;

	if (dg->himage)
		GlobalFree(dg->himage);

	dg->binfo.head.biBitCount = depth;

	dg->himage = GlobalAlloc(GMEM_MOVEABLE,
		(depth/8)*dg->binfo.head.biWidth*abs(dg->binfo.head.biHeight));
}


/*
 * ddgsImageClear
 * ddgsImageErase
 *
 * Set region of a ddgs image buffer to
 * a specific grey-level.
 */

void ddgsImageClear(DDGS *dg, BYTE g)
{
	BYTE *ptr;

	if (dg == NULL)
		return;

	if ((ptr = (BYTE *)GlobalLock(dg->himage)) == NULL)
		return;

	switch (dg->binfo.head.biBitCount) {
	case 8:
		memset(ptr, g, dg->binfo.head.biWidth*abs(dg->binfo.head.biHeight));
		break;

	case 24:
		memset(ptr, g, 3*dg->binfo.head.biWidth*abs(dg->binfo.head.biHeight));
		break;
	}

	GlobalUnlock(dg->himage);
}

void ddgsImageErase(DDGS *dg, BYTE g, RECT *rect)
{
	BYTE *ptr;
	int x, y, w, h, j;

	if (dg == NULL)
		return;

	if ((x = rect->left) < 0)
		x = 0;
	if (x >= dg->binfo.head.biWidth)
		return;
	if ((y = rect->top) < 0)
		y = 0;
	if (y >= abs(dg->binfo.head.biHeight))
		return;
	if ((w = rect->right - x + 1) < 0)
		return;
	if ((h = rect->bottom - y + 1) < 0)
		return;

	if (x + w >= dg->binfo.head.biWidth)
		w = dg->binfo.head.biWidth - x;
	if (y + h >= abs(dg->binfo.head.biHeight))
		h = abs(dg->binfo.head.biHeight) - y;

	if ((ptr = (BYTE *)GlobalLock(dg->himage)) == NULL)
		return;

	switch (dg->binfo.head.biBitCount) {
	case 8:
		ptr += y*dg->binfo.head.biWidth;
		ptr += x;
		for (j = 0; j < h; j++)
		{
			memset(ptr, g, w);
			ptr += dg->binfo.head.biWidth;
		}
		break;

	case 24:
		ptr += 3*y*dg->binfo.head.biWidth;
		ptr += 3*x;
		for (j = 0; j < h; j++)
		{
			memset(ptr, g, 3*w);
			ptr += 3*dg->binfo.head.biWidth;
		}
		break;
	}

	GlobalUnlock(dg->himage);
}


/*
 * ddgsImageDrawGrey
 *
 * Draw 8-bit (grey-scale) data from a
 * supplied buffer into the ddgs image
 * buffer.
 * Works with either 8-bit or 24-bit
 * depth dogs (unlike the color drawing
 * versions which only work with 24-bit
 * buffers).
 */
void ddgsImageDrawGrey(DDGS *dg, BYTE *src, RECT *rect)
{
	void *ptr;
	int x, y, w, h;
	int i, j, pitch;
	BYTE g;

	if (dg == NULL)
		return;

	pitch = rect->right - rect->left + 1;

	if ((x = rect->left) < 0)
	{
		src -= rect->left;
		x = 0;
	}
	if (x >= dg->binfo.head.biWidth)
		return;
	if ((y = rect->top) < 0)
	{
		src -= rect->top*pitch;
		y = 0;
	}
	if (y >= abs(dg->binfo.head.biHeight))
		return;
	if ((w = rect->right - x + 1) < 0)
		return;
	if ((h = rect->bottom - y + 1) < 0)
		return;

	if (x + w >= dg->binfo.head.biWidth)
		w = dg->binfo.head.biWidth - x;
	if (y + h >= abs(dg->binfo.head.biHeight))
		h = abs(dg->binfo.head.biHeight) - y;

	if ((ptr = (void *)GlobalLock(dg->himage)) == NULL)
		return;

	switch (dg->binfo.head.biBitCount) {
	case 8:
		(BYTE *)ptr += y*dg->binfo.head.biWidth;
		(BYTE *)ptr += x;
		for (j = 0; j < h; j++)
		{
			for (i = 0; i < w; i++)
				if (g = *src++)
					*((BYTE *)ptr)++ = g;
				else
					((BYTE *)ptr)++;

			(BYTE *)ptr += dg->binfo.head.biWidth - w;
			src += pitch - w;
		}
		break;

	case 24:
		(DDGSRGB *)ptr += y*dg->binfo.head.biWidth;
		(DDGSRGB *)ptr += x;
		for (j = 0; j < h; j++)
		{
			for (i = 0; i < w; i++)
			{
				if (g = *src++)
				{
					((DDGSRGB *)ptr)->r = g;
					((DDGSRGB *)ptr)->g = g;
					((DDGSRGB *)ptr)->b = g;
				}

				((DDGSRGB *)ptr)++;
			}

			(DDGSRGB *)ptr += dg->binfo.head.biWidth - w;
			src += pitch - w;
		}
		break;
	}

	GlobalUnlock(dg->himage);
}


/*
 * ddgsImageDrawColor
 *
 * Draw 24-bit data from a supplied 8-bit
 * buffer and mapping LUT into the ddgs image
 * buffer.
 * Fails (does nothing) if ddgs depth isnt 24.
 */

void ddgsImageDrawColor(DDGS *dg, BYTE *src, RECT *rect, DDGSRGB *lut, BOOL addmode)
{
	DDGSRGB *ptr;
	int x, y, w, h;
	int i, j, pitch;
	BYTE g;
	short acc;

	if (dg == NULL)
		return;

	// If its not a 24-bit dog, just draw
	// it in greyscale (this will only happen
	// if the app screws up).
	if (dg->binfo.head.biBitDepth != 24)
	{
		ddgsImageDrawGrey(dg, src, rect);
		return;
	}

	pitch = rect->right - rect->left + 1;

	if ((x = rect->left) < 0)
	{
		src -= rect->left;
		x = 0;
	}
	if (x >= dg->binfo.head.biWidth)
		return;
	if ((y = rect->top) < 0)
	{
		src -= rect->top*pitch;
		y = 0;
	}
	if (y >= abs(dg->binfo.head.biHeight))
		return;
	if ((w = rect->right - x + 1) < 0)
		return;
	if ((h = rect->bottom - y + 1) < 0)
		return;

	if (x + w >= dg->binfo.head.biWidth)
		w = dg->binfo.head.biWidth - x;
	if (y + h >= abs(dg->binfo.head.biHeight))
		h = abs(dg->binfo.head.biHeight) - y;

	if ((ptr = (DDGSRGB *)GlobalLock(dg->himage)) == NULL)
		return;

	ptr += y*dg->binfo.head.biWidth;
	ptr += x;
	for (j = 0; j < h; j++)
	{
		if (addmode)
		{
			for (i = 0; i < w; i++)
			{
				if (g = *src++)
				{
					acc = (short)ptr->r;
					if ((acc += (short)lut[g].r) > 255)
						acc = 255;
					ptr->r = (BYTE)acc;

					acc = (short)ptr->g;
					if ((acc += (short)lut[g].g) > 255)
						acc = 255;
					ptr->g = (BYTE)acc;

					acc = (short)ptr->b;
					if ((acc += (short)lut[g].b) > 255)
						acc = 255;
					ptr->b = (BYTE)acc;
				}

				ptr++;
			}
		}
		else
			for (i = 0; i < w; i++)
				if (g = *src++)
					*ptr++ = lut[g];

		ptr += dg->binfo.head.biWidth - w;
		src += pitch - w;
	}

	GlobalUnlock(dg->himage);
}


#endif
