/************************************************************************
 *                                                                      *
 *                              details.c                               *
 *                                                                      *
 ************************************************************************
 *
 *      Function to preint out all the details of a woolz object.
 *      Go down the structure a level at a time passing single pointer
 *      to the next routine.
 *
 *      Structure of code taken from write obj by J Piper
 *
 *      Graham John Page  Shandon southern Products Ltd 5 Dec 1985
 *
 * Modifications
 *
 *	10 Mar 2000		JMB		Changed headers included, as part of conversion to DLL.
 *							Also, made minor mods to stop compiler warnings and removed
 *							standard C parameter lists (were '#ifdef i386' only).
 *  9/19/96     BP:     Fix function return type.
 *	13 Sep 94		BP		Show corrent number of intervals.
 *	22 Jun 1992		mc		changed stderr to stdout
 *	18 Jan 1991		CAS/GJP		Added interval count
 *	22 Nov 1988		dcb			Added param to woolz_exit to say where called from
 *	17 Nov 1987		CAS			Combined two versions..... (TEST + BASIC)
 *	16 Jun 1987		CAS			Print out histogram properly..
 *	 2 Mar 1987		GJP			Woolz_exit
 *	 3 Dec 1986		CAS			Print real property list instead of random
 *								memory
 *	13 Sep 1986		CAS			Hand modified to long names
 *
 */

#include "woolz.h"

#include <stdio.h>
//#include <wstruct.h>

#define SCREENWIDTH 80

#ifdef WIN32
void
typeprint(int i)
#endif
#ifdef i386
void
typeprint(i)
int i;
#endif
{fprintf(stdout,"( type=%d )\n",i);}

/*
 *	B Y T E S T R E A M P R I N T
 *
 */
#ifdef WIN32
void
bytestreamprint(char *pl, int si)
#endif
#ifdef i386
void
bytestreamprint(pl, si)
char *pl;
int si;
#endif
{
	int i;
	int l=0;
	for (i=0; i<si;++i) {
		fprintf(stdout,"%4d",(*pl++)&255);
		l += 4 ;					/* size of %d above*/ 
		if (l>=SCREENWIDTH) {
		/*	putc('\n',stdout); */
			fprintf(stdout,"\n");
			l=0;
		}
	}
/*	putc('\n',stdout); */
	fprintf(stdout,"\n");
}

/*
 *	G R E Y S T R E A M P R I N T
 *
 */
#ifdef WIN32
void
greystreamprint(GREY *pl, int si)
#endif
#ifdef i386
void
greystreamprint(pl, si)
GREY *pl;
int si;
#endif
{
	int i;
	int l=0;
	for (i=0; i<si;++i) {
		fprintf(stdout,"%6d",*pl++);
		l += 6;
		if (l>=SCREENWIDTH) {
		/*	putc('\n',stdout); */
			fprintf(stdout,"\n");
			l=0;
		}
	}
	/* putc('\n',stdout); */
	fprintf(stdout,"\n");
}

/*
 *	I N T S T R E A M P R I N T
 *
 */
#ifdef WIN32
void
intstreamprint(int *pl, int si)
#endif
#ifdef i386
void
intstreamprint(pl, si)
int *pl;
int si;
#endif
{
	int i;
	int l=0;
	for (i=0; i<si;++i)	{
		fprintf(stdout,"%10d",*pl++);
		l += 10;
		if (l>=SCREENWIDTH) {
		/*	putc('\n',stdout); */
			fprintf(stdout,"\n");
			l=0;
		}
	}
	/* putc('\n',stdout); */
	fprintf(stdout,"\n");
}

/*
 *	F L O A T S T R E A M P R I N T
 *
 */
#ifdef WIN32
void
floatstreamprint(float *pl, int si)
#endif
#ifdef i386
void
floatstreamprint(pl, si)
float *pl;
int si;
#endif
{
	int i;
	int l=0;
	for (i=0; i<si;++i) {
		fprintf(stdout,"%4e",*pl++);
		l += 10 ;
		if (l>=SCREENWIDTH) {
		/*	putc('\n',stdout); */
			fprintf(stdout,"\n");
			l=0;
		}
	}
/* 	putc('\n',stdout); */
	fprintf(stdout,"\n");
}

#ifdef WIN32
void
printinterval(struct interval *intv)
#endif
#ifdef i386
void
printinterval(intv)
struct interval *intv;
#endif
{
	fprintf ( stdout,"[%5d,%5d] ",intv->ileft,intv->iright);
}

INTSIZE = 14 ;

#ifdef WIN32
void
printintervalline(struct intervalline *ivln, int nlines)
#endif
#ifdef i386
void
printintervalline(ivln, nlines)
struct intervalline *ivln;
int nlines;
#endif
{
	int i, j, l=0, totint=1;
	struct interval *intvsptr;

	for (i=0; i<=nlines; ivln++,i++) {
		fprintf ( stdout, "** intervalline %d **. %d intervals in line (tot int %d)\n",
					i,ivln->nintvs,totint);
		for (j=0,intvsptr=ivln->intvs; j<ivln->nintvs; j++,intvsptr++) {
			totint++;
			printinterval(intvsptr);
			l += INTSIZE ;
			if ( j > SCREENWIDTH ) {
				/* putc('\n',stdout); */
				fprintf(stdout,"\n");
				l=0;
			}
		}
		if (l != 0)
		/*	putc('\n',stdout); */
			fprintf(stdout,"\n");

	};
}

#ifdef WIN32
void
printintervaldomain(struct intervaldomain *itvl)
#endif
#ifdef i386
void
printintervaldomain(itvl)
struct intervaldomain *itvl;
#endif
{
	register int /*i, j,*/ nlines;

	fprintf(stdout,"** intervaldomain  ** \n ");
	if (itvl == NULL)
		fprintf(stdout,"NULL\n");
	else {
		typeprint(itvl->type);
		fprintf(stdout,"First line = %d , Last line = %d ;\
						First column = %d , Last column = %d\n",
						itvl->line1,itvl->lastln,itvl->kol1,itvl->lastkl);
		switch (itvl->type) {
		case 1:
				nlines = itvl->lastln - itvl->line1;
/* BP - show the correct number! (+1) */
				fprintf(stdout, "There are %d interval lines\n", nlines + 1);
				printintervalline(itvl->intvlines,nlines);
				break;
		case 2:
				break;
		default:
				fprintf(stdout,"Invalid domain type %d\n",itvl->type);
				woolz_exit(51, "printintervaldomain");
		}
	}
}



#ifdef WIN32
static void
printpropertylist(struct propertylist *plist)
#endif
#ifdef i386
static void
printpropertylist(plist)
struct propertylist *plist;
#endif
{
	fprintf ( stdout , "** propertylist ** ");
	if (plist == NULL)
		fprintf(stdout,"NULL\n");	   
	else {
		fprintf ( stdout ,"Property list is %d bytes long\n",plist->size);
		bytestreamprint((char *)plist, plist->size);
	}
}



#ifdef WIN32
static void
printvaluetable(struct object *obj)
#endif
#ifdef i386
static void
printvaluetable(obj)
struct object *obj;
#endif
{
	struct iwspace iwsp;
	struct gwspace gwsp;
	int packing, type;
	register int i;
	register GREY *g, min, max;
//	short sht;
	int reccount=0;
	fprintf(stdout,"** valuetable ** ");

	if (obj->vdom == NULL) 
		fprintf ( stdout ,"NULL\n");
	else {
		typeprint(type=obj->vdom->type);
		switch (type) {
		case 1:
		case 11:
		case 21:
				min = 0;
				max = 255;
				initgreyscan(obj,&iwsp,&gwsp);
				while (nextgreyinterval(&iwsp) == 0) {
					g = gwsp.grintptr;
					for (i=0; i<iwsp.colrmn; i++) {
						if (*g < min)
							min = *g;
						else if (*g > max)
							max = *g;
						g++;
					}
				}
				if (min == 0 && max == 255)
					packing = 3;
				else if (min > -32768 && max < 32768)
					packing = 2;
				else
					packing = 1;
				fprintf(stdout,"packing = %d ",packing);
				fprintf(stdout," background value = %d\n",(obj->vdom->bckgrnd));
				initgreyscan(obj,&iwsp,&gwsp);
				while(nextgreyinterval(&iwsp) == 0) {
					fprintf(stdout,"Interval record %d\n",++reccount);
					greystreamprint(gwsp.grintptr,iwsp.colrmn); 
				}
				break;
				
		case 2:
		case 12:
		case 22:
				fprintf(stdout,"Float grey tables are invalid\n");
				woolz_exit(53, "printvaluetable.1");
		default:
				fprintf(stdout,"Invalid grey table type %d\n",type);
				woolz_exit(53, "printvaluetable.2");
		}
	}
}

#ifdef WIN32
void
printintvtx(struct ivertex *vtx, int nverticies)
#endif
#ifdef i386
void
printintvtx(vtx, nverticies)
struct ivertex *vtx;
int nverticies;
#endif
{
int i;
		for (i=0;i<nverticies;vtx++,i++)
				fprintf(stdout,"X vertex %d Y vertex %d\n",vtx->vtX,vtx->vtY);
}

#ifdef WIN32
void
printfloatvtx(struct fvertex *vtx, int nverticies)
#endif
#ifdef i386
void
printfloatvtx(vtx, nverticies)
struct fvertex *vtx;
int nverticies;
#endif
{
int i;
		for (i=0;i<nverticies;vtx++,i++)
				fprintf(stdout,"X vertex %e Y vertex %e\n",vtx->vtX,vtx->vtY);
}

#ifdef WIN32
static void
printpolygon(struct polygondomain *poly)
#endif
#ifdef i386
static void
printpolygon(poly)
struct polygondomain *poly;
#endif
{
		register int nvertices;

		fprintf ( stdout ,"** polygon ** ");
		typeprint(poly->type);
		fprintf(stdout,"%d vertices out of a maximum of %d.\n"
				,nvertices = poly->nvertices,poly->maxvertices);
		switch (poly->type) 
		{
		case 1:
				printintvtx(poly->vtx,nvertices);
				break;

		case 2:
				printfloatvtx((struct fvertex *)poly->vtx, nvertices);
				break;
		
		default:
				fprintf(stdout,"Illegal polygon type %d\n",poly->type);
				woolz_exit(51, "printpolygon");
		}
}


#ifdef WIN32
static void
printboundlist(struct boundlist *blist)
#endif
#ifdef i386
static void
printboundlist(blist)
struct boundlist *blist;
#endif
{
		fprintf(stdout,"** boundlist ** ");
		if (blist == NULL)
				fprintf(stdout,"NULL\n");
		else 
				{
				typeprint(blist->type);
				fprintf(stdout,"Next boundlist\n");
				printboundlist(blist->next);
				fprintf(stdout,"Down boundlist\n");
				printboundlist(blist->down);
				fprintf(stdout,"Wrap has a value of %d\n",blist->wrap);
				printpolygon(blist->poly);
				}
}

						
#ifdef WIN32
static void
printintpoint(struct ipoint *pnt)
#endif
#ifdef i386
static void
printintpoint(pnt)
struct ipoint *pnt;
#endif
{
		fprintf(stdout,"** integer point ** ");
		fprintf(stdout,"k = %d  l = %d  style = %d\n",
						pnt->k, pnt->l,pnt->style);
}

#ifdef WIN32
static void
printfloatpoint(struct fpoint *pnt)
#endif
#ifdef i386
static void
printfloatpoint(pnt)
struct fpoint *pnt;
#endif
{
		fprintf(stdout,"** floating point ** ");
				fprintf(stdout,"k = %e  l = %e style = %e\n",
						pnt->k, pnt->l,pnt->style);
}
						
#ifdef WIN32
static void
printintvector(struct ivector *vec)
#endif
#ifdef i386
static void
printintvector(vec)
struct ivector *vec;
#endif
{
		fprintf(stdout,"** integer vector ** ");
		fprintf(stdout,"k1 = %d  l1 = %d  k2 = %d  l2 = %d  style = %d\n",
						vec->k1, vec->l1, vec->k2, vec->l2,vec->style);
}

#ifdef WIN32
static void
printfloatvector(struct fvector *vec)
#endif
#ifdef i386
static void
printfloatvector(vec)
struct fvector *vec;
#endif
{
		fprintf(stdout,"** floating vector ** ");
				fprintf(stdout,"k1 = %e  l1 = %e  k2 = %e  l2 = %e  style = %e\n",
						vec->k1, vec->l1, vec->k2, vec->l2,vec->style);
}
						

#ifdef WIN32
static void
printhisto(struct histogramdomain *h)
#endif
#ifdef i386
static void
printhisto(h)
struct histogramdomain *h;
#endif
{
int type;
		fprintf(stdout,"** histogram ** ");
		typeprint(type=h->type);
		fprintf(stdout,"style = %d k = %d l = %d number of points = %d\n",
				h->r,h->k,h->l,h->npoints);
		switch (h->type) 
		{
		case 1:
				intstreamprint(h->hv,h->npoints);
				break;
		case 2:
				floatstreamprint((float *)h->hv, h->npoints);
				break;
		default:
				fprintf(stdout,"Invalid histogram type\n");
		}
}

#ifdef WIN32
static void
printtext(struct textobj *tobj)
#endif
#ifdef i386
static void
printtext(tobj)
struct textobj *tobj;
#endif
{
	struct textdomain *tdom;
	char *tstring,c;
	int i;

	fprintf(stdout,"** text ** ");  
	tstring = tobj->text;
	tdom = tobj->tdom;
	for(i = 0;i <= tdom->stringlen; i++) {
		c = tstring[i];
	/*	putc(c,stdout); */
		fprintf(stdout," %c \n",c);
	}
}


/*
 *	D E T A I L S
 *
 */
#ifdef WIN32
void
details(struct object *obj)
#endif
#ifdef i386
void
details(obj)
struct object *obj;
#endif
{
	fprintf(stdout,"** object **\n");
	typeprint (obj->type);

	switch (obj->type) {
	case 1:
		printintervaldomain(obj->idom);
		printvaluetable(obj);
		printpropertylist(obj->plist);
		break;

	case 10:
		printpolygon((struct polygondomain *)obj->idom);
		break;

	case 11:
		printboundlist((struct boundlist *)obj->idom);
		break;

	case 13:
		printhisto((struct histogramdomain *)obj->idom);
		break;

	case 20:
/*
		printrect(obj->idom);
*/
		fprintf(stdout,"later\n");
		break;

	case 30:
		printintvector((struct ivector *)obj);
		break;

	case 31:
		printfloatvector((struct fvector *)obj);
		break;

	case 40:
		printintpoint((struct ipoint *)obj);
		break;

	case 41:
		printfloatpoint((struct fpoint *)obj);
		break;

	case 70:
		printtext((struct textobj *)obj);
		break;

	case 110:
		printpropertylist(obj->plist);
		break;

	default:
		fprintf(stdout,"Invalid object ");
		typeprint(obj->type);
		woolz_exit(51, "details");
	}
}

