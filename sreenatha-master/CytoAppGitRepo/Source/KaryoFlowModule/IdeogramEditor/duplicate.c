
/*
 *	duplicate.c	2/3/93	M.Gregson
 *
 *	routines for duplicating woolz objects 
 *
 * Modifications:
 *
 *	10Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *					Also, made minor mods to stop compiler warnings and removed
 *					standard C parameter lists (were '#ifdef i386' only).
 *	KS	30Jul99:	NULL object check in duplicate_obj
 *	WH	29Apr98:	Can't reference a member in a NULL pointer in WIN32
 *	BP	10/13/94:	Protection against NULL malloc.
 *
 */

#include "woolz.h"

#include <stdio.h>
//#include <memory.h>
#include <malloc.h> // For malloc
#include <string.h> // For memcpy
//#include <fbio.h>
//#include <wstruct.h>


struct object *
duplicate_obj(struct object *srcobj)
{
	int type, spc;
	struct object *obj,*makemain(),*dup_vector(),*dup_point(),*dup_frame();
	struct object *dup_text(), *dup_circle();
//	struct intervaldomain *idmn, *copydomain();
//	struct propertylist *ipp;
	struct polygondomain *poly, *dup_polygon();
	struct boundlist *blist, *dup_boundlist();
	struct cvhdom *cd, *dup_cvhdom();
	struct irect *rect, *dup_rect();
	struct histogramdomain *hdom, *dup_histo();
//	struct pframe *rframe;
	struct valuetable *copyvaluetb();
	struct interval *spareitv;

	if (srcobj == NULL) return NULL;

	type = srcobj->type;

	switch (type) {
	case 0:
		return(NULL);

	case 1: /*
		duplicates a Woolz object - returned object has new idom and vdom
		but shares plist with source object
		*/

		// SN 18Oct98 Added straight from UNIX CV
		/* check if this is a type 1 object with rectangular vdom and
		   type 2 idom - i.e. a rectangular grey object */
		if (srcobj->idom->type == 2) {

			int line1, lastln, kol1, lastkl, nvalues;
			GREY *values;
			struct rectvaltb *rvtb;


			line1=srcobj->idom->line1;
			lastln=srcobj->idom->lastln;
			kol1=srcobj->idom->kol1;
			lastkl=srcobj->idom->lastkl;
			nvalues=(lastln-line1+1) * (lastkl-kol1+1);

			/* allocate space for obj grey values */
			values=(GREY *)Malloc( nvalues );

			/* copy original object values */
			rvtb=(struct rectvaltb *)srcobj->vdom;
			memcpy(values, rvtb->values, nvalues);

			/* create new object - NOTE plist is SHARED */
			obj=(struct object *)makerect(line1, lastln, kol1, lastkl, values, 
							srcobj->vdom->bckgrnd, srcobj->plist, srcobj);

			return(obj);
		}


		/* allocate space for obj */
		obj=(struct object *) Malloc(sizeof(struct object));

		obj->type = srcobj->type;

		obj->idom = (struct intervaldomain *)copydomain(srcobj->idom, &spareitv, &spc);
		obj->idom->linkcount = 1;

#ifdef i386
		obj->vdom=copyvaluetb((struct object *)srcobj,srcobj->vdom->type,srcobj->vdom->bckgrnd);
#endif
#ifdef WIN32
		obj->vdom=copyvaluetb((struct object *)srcobj,
			(srcobj->vdom) ? srcobj->vdom->type : 0,
			(srcobj->vdom) ? srcobj->vdom->bckgrnd : 0);
#endif

		/* share srcobj plist with obj */
		obj->plist=srcobj->plist;

		/* associate obj with srcobj */
		obj->assoc=(struct object *)srcobj;

		return(obj);

	case 10:
		poly = dup_polygon(srcobj->idom);
		obj = makemain(type, (struct intervaldomain *)poly, NULL, NULL, NULL);
		return(obj);

	case 11:
		blist=dup_boundlist(srcobj->idom);
		obj= makemain(type, (struct intervaldomain *)blist, NULL, NULL, NULL);
		return(obj); 

	case 12:
		poly = dup_polygon(srcobj->idom);
		cd = dup_cvhdom(srcobj->vdom);
		obj=makemain(type, (struct intervaldomain *)poly, (struct valuetable *)cd, NULL, NULL);
		return(obj);

	case 13:
		hdom = dup_histo(srcobj->idom);
		obj=makemain(type, (struct intervaldomain *)hdom, NULL, NULL, NULL);
		return(obj);
	
	case 20:
		rect=dup_rect(srcobj->idom);
		obj=makemain(20, (struct intervaldomain *)rect, NULL, NULL, NULL);
		return(obj);
		
	case 30:
	case 31:
		return(dup_vector(srcobj));

	case 40:
	case 41:
		return(dup_point(srcobj));
	case 60:
		return(dup_frame(srcobj));
		
	case 70:
		return(dup_text(srcobj));

	case 100:
		return(dup_circle(srcobj));

	default:
		fprintf(stderr,"Can't duplicate object type %d\n",type);
		return(NULL);

	}
}




/*
 *	DUPLICATE_OBJ_PLUS
 *
 *	duplicates a Woolz object - returned object has new idom, vdom and plist
 *	- straight copies of source object components
 */
struct object *
duplicate_obj_plus(struct object *srcobj)
{
	int psize;
	struct object *destobj;


	destobj = duplicate_obj(srcobj);

/* Except for text objects (type 70) objects
 * above type 20 do not have property lists */
	if ((srcobj->type >= 20) && (srcobj->type != 70))
		return(destobj);

/* BP - moved from before the above condition
 * so that objects without plists don't have
 * theit plist set to NULL!!!! WHO WROTE THIS
 * CODE????? */
	destobj->plist = NULL;

/* If there was a plist, copy it. */
	if (srcobj->plist)
		if ((psize = srcobj->plist->size) > 0)
		{
			if (destobj->plist = (struct propertylist *)Malloc(psize))
				memcpy(destobj->plist, srcobj->plist, psize);
			else
				destobj->plist = NULL;
		}
		else
			fprintf(stderr, "duplicate_obj_plus: bad plist!\n");

	return(destobj);
}


static struct polygondomain *
dup_polygon(struct polygondomain *srcpoly)
{
	int type;
	int nvertices, maxvertices;
	struct ivertex *vertices;
	struct polygondomain *poly, *makepolydmn();

	type = srcpoly->type;
	nvertices = srcpoly->nvertices;
	maxvertices = srcpoly->maxvertices;
	vertices =  srcpoly->vtx;

	/* create polygondomain */
	poly = makepolydmn(type, (short *)vertices, nvertices, maxvertices, 1);

	return(poly);
}



static struct boundlist *
dup_boundlist(struct boundlist *srcblist)
{
	struct boundlist *blist;

	if (srcblist==NULL)
		return(NULL);

	blist = (struct boundlist *) Malloc (sizeof(struct boundlist));
	
	blist->type=srcblist->type;

	blist->up=NULL;

	blist->next=dup_boundlist(srcblist->next);

	blist->down=dup_boundlist(srcblist->down);

	if (blist->down!=NULL)
		blist->down->up=blist;

	blist->wrap = srcblist->wrap;

	blist->poly = dup_polygon(srcblist->poly);
	
	return(blist);
}


static struct cvhdom *
dup_cvhdom(struct cvhdom *src_cvhdom)
{
	int i;
	struct cvhdom *c;
	

	c = (struct cvhdom *) Malloc(sizeof( struct cvhdom) + 
		 src_cvhdom->nchords * sizeof( struct chord));


	c->type = src_cvhdom->type;
	c->nchords = src_cvhdom->nchords;
	c->nsigchords =src_cvhdom->nsigchords;
	c->mdlin = src_cvhdom->mdlin;
	c->mdkol = src_cvhdom->mdkol;
	c->ch=(struct chord *) (c+1);

	for (i=0;i<c->nchords;i++)
		c->ch[i]=src_cvhdom->ch[i];

	return(c);
}


static struct irect *
dup_rect(struct irect *srcrect)
{
	register type;
	struct irect *ir;
	struct frect *fr;

	type=srcrect->type;
	
	switch (type) {
	case 1:
		ir = (struct irect *) Malloc (sizeof(struct irect));
		*ir=*srcrect;
		return((struct irect *)ir);
	case 2:
		fr = (struct frect *) Malloc (sizeof(struct frect));
		*fr= *((struct frect *)srcrect);
		return((struct irect *)fr);

	}
}


static struct object *
dup_circle(struct circle *circ)
{
	struct circle *c;

	c = (struct circle *) Malloc (sizeof(struct circle));
	*c=*circ;
	return((struct object *)c);
}


static struct object *
dup_vector(struct ivector *srcvect)
{
	struct ivector *iv;
	struct fvector *fv;

	switch (srcvect->type) {
	case 30:
		iv = (struct ivector *) Malloc (sizeof(struct ivector));
		*iv=*srcvect;
		return((struct object *)iv);
	case 31:
		fv = (struct fvector *) Malloc (sizeof(struct fvector));
		*fv=*((struct fvector *)srcvect);
		return((struct object *)fv);
	}
}


static struct object *
dup_point(struct ipoint *srcpoint)
{
	struct ipoint *iv;
	struct fpoint *fv;

	switch (srcpoint->type) {
	case 40:
		iv = (struct ipoint *) Malloc (sizeof(struct ipoint));
		*iv=*srcpoint;
		return((struct object *)iv);
	case 41:
		fv = (struct fpoint *) Malloc (sizeof(struct fpoint));
		*fv=*((struct fpoint *)srcpoint);
		return((struct object *)fv);
	}
}


static struct histogramdomain *
dup_histo(struct histogramdomain *srchisto)
{
	int i;
	struct histogramdomain *h;
	
	h = (struct histogramdomain *) Malloc (sizeof(struct histogramdomain) +
		srchisto->npoints * (srchisto->type == 1? sizeof(int): sizeof(float)));


	h->type = srchisto->type;
	h->npoints=srchisto->npoints;
	h->r=srchisto->r;
	h->k=srchisto->k;
	h->l=srchisto->l;
	h->hv=(int *) (h+1);

	for (i=0;i<h->npoints;i++)
		h->hv[i]=srchisto->hv[i];
	
	return(h);
}


static struct object *
dup_frame(struct pframe *srcframe)
{
	struct pframe *rf;

	rf = (struct pframe *) Malloc(sizeof(struct pframe));
	*rf=*srcframe;

	return((struct object *)rf);
}


static struct object *
dup_text(struct textobj *srctext)
{
	struct textobj *tobj;
	struct textdomain *tdom;
	char *tstring;
	int i;
	
	tdom = (struct textdomain *) Malloc(sizeof(struct textdomain));
	*tdom=*(srctext->tdom);

	tstring = (char *) Malloc(srctext->tdom->stringlen+1);

	tobj = (struct textobj *)makemain(srctext->type, (struct intervaldomain *)tdom, 
	                                  (struct valuetable *)tstring, NULL, NULL);

	for(i = 0;i <= tdom->stringlen; i++)
		tstring[i] = srctext->text[i];

	
	return((struct object *) tobj);
}

