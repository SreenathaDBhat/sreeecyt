/*
 * edgegv.c	Jim Piper	July 1983
 *
 * Modifications
 *
 *	14Aug98			MG		Stopped set_edgegv() drawin one too many
 *							lines at the top of an object
 *	18 Nov 1988		dcb		woolz_check_obj() instead of wzcheckobj()
 *	06 May 1987		BDP		protection against null or empty objs
 *	13 Sep 1986		CAS		Includes
 */

#include "woolz.h"

#include <stdio.h>
//#include <wstruct.h>

/*
 * compute mean grey value of edge points.
 * protection against null objects added, this will just cause zero to 
 * be returned. For an empty object the same result will occur, previously
 * an empty object would give a divide by zero error. An extra variable
 * has been used allowing easier handling of the empty case an giving
 * just one point of return
 */
#ifdef WIN32
int
edgegv(struct object *obj)
#endif
#ifdef i386
int
edgegv(obj)
struct object *obj;
#endif
{
	struct iwspace iwsp;
	struct gwspace gwsp;
	register count,gtot,gv;

	gv = 0;
	if (woolz_check_obj(obj, "edgegv") == 0)
		if ( ( wzemptyidom(obj->idom) == 0 ) && (obj->type == 1) )
		{
			initgreyscan(obj,&iwsp,&gwsp);
			gtot = 0;
			count = 0;
			while (nextgreyinterval(&iwsp) == 0) {
				count++;
				gtot += *gwsp.grintptr;
				if (iwsp.colrmn > 1) {
					count++;
					gtot += *(gwsp.grintptr + iwsp.colrmn -1);
				}
			}
			if (count > 0)
				gv = gtot/count;
		}
	return(gv);
}


/*
 *	set edge grey value of object
 *	useful for post filter tidying of edge pixels
 */
#ifdef WIN32
int
set_edgegv(struct object *obj, GREY val, int width)
#endif
#ifdef i386
int
set_edgegv(obj, val, width)
struct object *obj;
GREY val;
int width;
#endif
{
	struct iwspace iwsp;
	struct gwspace gwsp;
	register int left,w;
	GREY *g;

	if (woolz_check_obj(obj, "set_edgegv") == 0)
		if ( ( wzemptyidom(obj->idom) == 0 ) && (obj->type == 1) )
		{
			initgreyscan(obj,&iwsp,&gwsp);
			while (nextgreyinterval(&iwsp) == 0) {
				g=gwsp.grintptr;
				left=iwsp.colrmn;

				/* fill top & bottom edges */
				if ((iwsp.linpos-iwsp.linbot<width)
				|| (iwsp.linrmn<width))				// MG 14Aug98
					w=left;
				else
					w=width;

				/* left edge */
				while ((w>0) && (left > 0)) {
					*g=val;
					g++;
					w--;
					left--;
				}
					
				/* right edge */	
				if (iwsp.colrmn > 1) {
					g=gwsp.grintptr + iwsp.colrmn -1;
					w=width;
					left=iwsp.colrmn-width;
					while ((w>0) && (left > 0)) {
						*g=val;
						g--;
						w--;
						left--;
					}
				}
			}
		}
}

