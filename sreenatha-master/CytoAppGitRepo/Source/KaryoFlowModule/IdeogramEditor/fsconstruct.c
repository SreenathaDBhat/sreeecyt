/* 
 *      F S C O N S T R U C T . C   --  Construct woolz object from framestore
 *
 *		assumes a BYTE structure store (ie VIRTUOSO)
 *
 *
 *  Written: B. Pinnington
 *           Image Recognition Systems
 *           720 Birchwood Boulevard
 *           Birchwood Science Park
 *           Warrington
 *           Cheshire
 *           WA3 7PX
 *
 *	Copyright (c) and intellectual property rights Image Recognition Systems (1988)
 *
 *  Date:    
 *
 *	Description:	Uses two passes through the frame store, one to find
 *					the number of intervals required, the other to fill
 *					them out. The object returned is interval structured
 *					with a completed interval structured grey table.
 *
 *  Modifications
 *
 *	4 Jul  2000		MC		Add further potential value for fsinvert flag (2) - means
 *							pixels should be included in the object which are less than
 *							the threshold (just like fsinvert = 1) but DON'T actually 
 *							invert grey levels. This is needed for new PROBE BRIGHTFIELD 
 *							mode (BRfluors).
 *
 *	10 Mar 2000		JMB		Changed headers included, as part of conversion to DLL.
 *							Also, made minor mods to stop compiler warnings and removed
 *							standard C parameter lists (were '#ifdef i386' only).
 *	9/19/96		BP:		Change COORD to WZCOORD.
 *						fsthresh parameter to fsconstruct changed to FSCHAR.
 *						Move comments from function header.
 *	 4 Aug 1993	MG		stop fsconstruct from ignoring 1st and last lines
 *					capconstruct() performs in the same way as old fsconstruct
 *	20 May 1988		CAS		Tidy up somewhat + make slightly faster
 *  12 Aug 1987		BDP		Some minor performance tuning
 *	10 Aug 1987		BDP		Added memory failure checking and handling
 *	29 Jul 1987		BDP		Ignore top and bottom  lines of frame
 *	22 Jun 1987		BDP		Cope with either inverted or normal memory
 *
 */

#include "woolz.h"

#include <stdio.h>
#include <malloc.h> // For malloc, calloc, free
//#include <wstruct.h>

#define TRUE 1
#define FALSE 0
typedef unsigned char FSCHAR;

/* flag to make fsconstruct miss 1st and last lines - if set */
static short FSCAPTURE = 0;



int cthiscanbuild(),cthiscancount();
int ctlowscanbuild(),ctlowscancount();
int woolzexitfn(),fsmemerror(),fscheckerror();
struct intervaldomain *makedomain();
struct object *ctmakeobj(), *makemain();
static int memerror = 0;



/*
 *	F S C O N S T R U C T  --  Construct woolz obj from framestore
 *
 *	Uses two passes through the frame store , firstly to find out how
 *	many pixels will be in the object and how many intervals will
 *	contain them, and the second pass will fill out the extents of the
 *	intervals, and fill out the grey values. The object returned contains a
 *	type 21 interval structured grey table. This is filled out at the same
 *	time as the interval records themselves.
 *
 *	Input:	fsaddr - Address of the frame store.
 *			fslines, fscols	- the number of lines and columns in the frame store
 *			fsthresh - and the threshold value
 *			fsinvert (1) - indicates whether to invert grey values so white is zero.
 *			fsinvert (2) - grey values included in object < than threshold MC 4Jul 2000
 *			               see header.
 *
 *	Returns:	Object pointer
 */
struct object *
fsconstruct(FSCHAR *fsaddr, WZCOORD fslines, WZCOORD fscols, FSCHAR fsthresh, int fsinvert)
{
	struct object *obj;
	int intcount, greycount, invertgreys = 1;
	
	/* for version which steps over first line and finishes before the last
	need to move memory address on by one line . dont alter lines values
	as domains must remain accurate */

	if (FSCAPTURE)	{
		fsaddr += fscols;
		fslines-=2;
	}
		
	
	if (fsinvert)
		greycount = ctlowscancount(fsaddr,fslines,fscols,fsthresh,&intcount);
	else
		greycount = cthiscancount(fsaddr,fslines,fscols,fsthresh,&intcount);
/*	fprintf(stderr,"fsconstruct, first scan complete greycount %d, intcount %d\n",greycount,intcount); */

	if (greycount > 0) {
		obj = ctmakeobj(fslines,fscols,intcount,greycount);
		if (obj != NULL) {
			if (fsinvert)
			{
				if (fsinvert == 1)
					invertgreys = 1;
				else
					invertgreys = 0;

				ctlowscanbuild(obj,fsaddr,fsthresh,invertgreys);
			}
			else
			{
				cthiscanbuild(obj,fsaddr,fsthresh);
			}
		}
	} else
		obj = makemain(1,makedomain(1,1,0,1,0),NULL,NULL,NULL);
	return(obj);
}					

/*
 *	C T M A K E O B J  --  Build up object data structure
 *
 *	Obtains space for interval domain and grey table and builds object
 *
 *	Inputs:	lines, cols - number of line and colums (although bruce didn't say what of)
 *			intcount - number of intervals there will be
 *			greycount - no of pixels
 *
 *	Returns:	Object pointer
 */
struct object *
ctmakeobj(int lines, int cols, int intcount, int greycount)
{
	struct ivtable *ctivtable;
	struct intervaldomain *idom;

	ctivtable = (struct ivtable *) Calloc( sizeof(struct ivtable) +
									lines  * sizeof(struct valintline) +
									intcount * sizeof(struct valueline) , 1);
	if (fscheckerror())
		return(NULL);
	ctivtable->type = 21;
	ctivtable->freeptr = (char *) Calloc(greycount, sizeof(GREY) );
	ctivtable->linkcount = 0;		/* will be incremented by a makemain */
	if (fscheckerror()) {
		Free(ctivtable);
		return(NULL);
	}

	if (FSCAPTURE) {
		ctivtable->line1 = 1;
		ctivtable->lastln = lines;
	}
	else {
		ctivtable->line1 = 0;
		ctivtable->lastln = lines-1;
	}

	ctivtable->bckgrnd = 0;
	ctivtable->vil = (struct valintline *) (ctivtable + 1);
	ctivtable->original = NULL;

	idom = makedomain(1, ctivtable->line1, ctivtable->lastln, 0, cols - 1);
	idom->freeptr = (char *) Malloc( intcount * sizeof(struct interval));
	idom->linkcount = 0;			/* will be incremented by a makemain */
	if (fscheckerror()) {
		Free(idom);
		Free(ctivtable->freeptr);
		Free(ctivtable);
		return(NULL);
	}
	return( makemain(1, idom, (struct valuetable *)ctivtable, NULL, NULL) );
}

/*
 *	C T H I S C A N C O U N T  --  count intervals and above threshold pixels
 *
 *	Looks through the specified area of the framestore counting the
 *	number of pixels which are above or equal to the threshold value,
 *	and counting the number of intervals (series of above threshold pixels).
 *	Designed for 8 bit operation.
 *
 */
int
cthiscancount(FSCHAR *fsaddr, WZCOORD lines, WZCOORD cols, register FSCHAR thresh, int *intcountptr)
{
	register int colno, totcount, ininterval;
	int lineno, intcount;
	register FSCHAR *chr;
	
	intcount = 0;
	totcount = 0;
	chr = fsaddr;				/* to help speed up the search */
	
	lineno = lines;
	while (lineno) {
		ininterval = FALSE;		/* initialise to false at line start */
		colno = cols;
		while (colno) {
			if (*chr++ >= thresh) {
				totcount++;
				ininterval = TRUE;
			} else {
/*
 * if we were in an interval but current is below thresh, then one more interval
 */
				if (ininterval)
					intcount++;
				ininterval = FALSE;
			}
			colno--;
		}
/* at end of line see if an interval is in progress, if so inc count */
		if (ininterval)
			intcount++;
		lineno--;
	}
	*intcountptr = intcount;	/* return interval count at supplied ptr */
	return(totcount);
}

/*
 *	C T L O W S C A N C O U N T  --  count intervals and below threshold pixels
 *
 *	direct equivalent of cthiscancount but for use when the invert flag 
 *	in the call to fsconstruct is set. In this case white is high(255)
 *	and so below threshold values are searched for
 */
int
ctlowscancount(FSCHAR *fsaddr, WZCOORD lines, WZCOORD cols, register FSCHAR thresh, int *intcountptr)
{
	register int colno, totcount, ininterval;
	int intcount, lineno;
	register FSCHAR *chr;
	
	intcount = 0;
	totcount = 0;
	chr = fsaddr;				/* to help speed up the search */
	
	lineno = lines;
	while (lineno) {
		ininterval = FALSE;		/* initialise to false at line start */
		colno = cols;
		while (colno) {
			if (*chr++ < thresh) {
				totcount++;
				ininterval = TRUE;
			} else {
/*
 * if we were in an interval but current is below thresh, then one more interval
 */
				if (ininterval)
					intcount++;
				ininterval = FALSE;
			}
			colno--;
		}
/* at end of line see if an interval is in progress, if so inc count */
		if (ininterval)
			intcount++;
		lineno--;
	}
	*intcountptr = intcount;	/* return interval count at supplied ptr */
	return(totcount);
}


/*
 *	C T H I S C A N B U I L D  --  mark interval end points and collect grey values
 *
 *	Description:	performs the second pass of the frame store, to fill
 *					all the grey values in the grey table, and the
 *					coordinate positions of the interval structures for
 *					both the interval and  values domain.  The values
 *					domain could be more easily but less efficiently
 *					filled out at the end of the scan by makeivtable.
 *
 *	Entry:			object pointer. the object contains a suitably sized
 *					interval domain, and value domain, but with none of
 *					the relevant line entries filled out. the interval
 *					domain also contains a valid but unused interval
 *					list, and the value domain contains a suitable
 *					length grey table.
 *					The threshold value is also resupplied
 *
 *	Exit:			All remaining object fields are completed
 *
 */
int
cthiscanbuild(struct object *obj, FSCHAR *fsaddr, register FSCHAR thresh)
{
	struct intervaldomain *idom;
//	struct intervalline *intvline;
	register struct interval *intv;
	struct interval *intvlinestart;
	struct ivtable *sbivtable;		/* grey version of idom */
	struct valintline *sbvil;		/* grey version of intvline */
	struct valueline *sbval;		/* grey version of intv */
	register GREY *g;
	register FSCHAR *chr;
	register int colno,lastabove,intvcount;
	int lineno, lastln,lastkl;
	int cols;
	
	/* set up data structures from supplied object . This routine is assumed 
	to be internal to this module, so no explicit object checks are necessary */
	
	idom = obj->idom;
	lastln = idom->lastln;
	lastkl = idom->lastkl;
	intv = (struct interval *) idom->freeptr;
	sbivtable = (struct ivtable *) obj->vdom;
	sbvil = sbivtable->vil;
	sbval = (struct valueline *) (sbvil + lastln - idom->line1 + 1);
	g = (GREY *) sbivtable->freeptr;
	cols = lastkl - idom->kol1 + 1;
	chr = fsaddr + (lastln - idom->line1) * cols;	/* this is to speed up the scan */
		
	/*	The outer loop below is the line loop, and will result in the 
	intervalline structure being completed and incremented once. The same
	applies to the valintline structure. */
	
	for (lineno = idom->line1; lineno <= lastln; lineno++ ) {
		sbvil->vtbint = sbval;		/* grey interval line start */
		intvlinestart = intv;		/* record this for makeinterval(line) */
		intvcount = 0;
		lastabove = FALSE;			/* cant be mid interval at line start */
		
		for (colno = idom->kol1; colno <= lastkl; colno++ ) {
			if (*chr >= thresh) {		/* is current above threshold */
			/* if previous was below then mark new interval start, else
				just add current grey value to grey table */
				
				if (lastabove == FALSE) {
					intv->ileft = colno;
					sbval->vkol1 = colno;
					sbval->values = g;		/* record this at start of each interval */
					lastabove = TRUE;
				}
				*g++ = *chr;
			} else {					/* current below threshold */
				/* if previous was above then that was the end of an interval
				so mark it up, otherwise just ignore the current value */
				if (lastabove) {
					intv->iright = sbval->vlastkl = colno - 1;
					lastabove = FALSE;
					sbval++;		/* incr to next grey interval */
					intv++;			/* incr to next interval +*/
					intvcount++;	/* total for current line */
				}
			}
			chr++;
		}
		/* now at the end of line , if an interval is current at this point
		then terminate it, this being the second interval end condition */
			
		if (lastabove == TRUE) {
			colno--;
			intv->iright = colno ;
			sbval->vlastkl = colno ;
			colno++;
			sbval++;
			intv++;
			intvcount++;
		}
		/* can now complete the interval line and valintline structures and
		increment to next line */
		
		sbvil->nintvs = intvcount;
		makeinterval(lineno, idom, intvcount, intvlinestart);
		sbvil++;		/* only need to incr valintline, other is recalculated eacc h line */

		/* because line numbers increment up the screen, but frame memeory
		increments down the screen, to get to beginning of the next line we
		have to subtract two lines from the frame pointer */
		
		chr -= cols * 2;
	}	
}

/************************************************************************/
/*																		*/
/*	Function:		ctlowscanbuild										*/
/*																		*/
/*	Purpose:		Mark interval coordinates and collect grey values	*/
/*																		*/
/*	Description:	performs the second pass of the frame store, to fill*/
/*					all the grey values in the grey table, and the		*/
/*					coordinate positions of the interval structures for */
/*					both the interval and  values domain.  The values	*/
/*					domain could be more easily but less efficiently	*/
/*					filled out at the end of the scan by makeivtable.	*/
/*					N.B. this version is for use when the invert flag is*/
/*					specified and will invert the grey values, and also */
/*					will compare for values below threshold				*/
/*																		*/
/*	Entry:			object pointer. the object contains a suitably sized*/
/*					interval domain, and value domain, but with none of */
/*					the relevant line entries filled out. the interval  */
/*					domain also contains a valid but unused interval 	*/
/*					list, and the value domain contains a suitable 		*/
/*					length grey table.									*/
/*					The threshold value is also resupplied				*/
/*																		*/
/*	Exit:			All remaining object fields are completed			*/
/*																		*/
/************************************************************************/


int
ctlowscanbuild(struct object *obj, FSCHAR *fsaddr, register FSCHAR thresh, int invertgreys)
{
	struct intervaldomain *idom;
//	struct intervalline *intvline;
	register struct interval *intv;
	struct interval *intvlinestart;
	struct ivtable *sbivtable;		/* grey version of idom */
	struct valintline *sbvil;		/* grey version of intvline */
	struct valueline *sbval;		/* grey version of intv */
	register GREY *g;
	register FSCHAR *chr;
	register int lineno,colno,lastbelow,intvcount;
	int cols,lastln,lastkl;
	
	/* set up data structures from supplied object . This routine is assumed 
	to be internal to this module, so no explicit object checks are necessary */
	
	idom = obj->idom;
	lastln = idom->lastln;
	lastkl = idom->lastkl;
	intv = (struct interval *) idom->freeptr;
	sbivtable = (struct ivtable *) obj->vdom;
	sbvil = sbivtable->vil;
	sbval = (struct valueline *) (sbvil + lastln - idom->line1 + 1);
	g = (GREY *) sbivtable->freeptr;
	cols = lastkl - idom->kol1 + 1;
	chr = fsaddr + (lastln - idom->line1) * cols;	/* this is to speed up the scan */
		
	/*	The outer loop below is the line loop, and will result in the 
	intervalline structure being completed and incremented once. The same
	applies to the valintline structure. */
	
	for (lineno = idom->line1; lineno <= lastln; lineno++ ) {
		sbvil->vtbint = sbval;		/* grey interval line start */
		intvlinestart = intv;		/* record this for makeinterval(line) */
		intvcount = 0;
		lastbelow = FALSE;			/* cant be mid interval at line start */
		
		for (colno = idom->kol1; colno <= lastkl; colno++ ) {
			if ( *chr < thresh ) {	/* is current below threshold */
				/* if previous was below then mark new interval start, else
				just add current grey value to grey table */
				
				if (lastbelow == FALSE) {
					intv->ileft = colno;
					sbval->vkol1 = colno;
					sbval->values = g;		/* record this at start of each interval */
					lastbelow = TRUE;
				}
				if (invertgreys)			// NEW MC 4Jul2000 see header for rationale
					*g++ = 255 - *chr;
				else
					*g++ = *chr;
			} else {					/* current above threshold */
				/* if previous was below then that was the end of an interval
				so mark it up, otherwise just ignore the current value */
				
				if (lastbelow) {
					colno--;
					intv->iright = colno ;	/* previous char was the end */
					sbval->vlastkl = colno ;
					colno++;
					lastbelow = FALSE;
					sbval++;		/* incr to next grey interval */
					intv++;			/* incr to next interval +*/
					intvcount++;	/* total for current line */
				}
			}
			chr++;
		}
		/* now at the end of line , if an interval is current at this point
		then terminate it, this being the second interval end condition */
			
		if (lastbelow) {
			colno--;
			intv->iright = colno ;
			sbval->vlastkl = colno ;
			colno++;
			sbval++;
			intv++;
			intvcount++;
		}
		/* can now complete the interval line and valintline structures and
		increment to next line */
		
		sbvil->nintvs = intvcount;
		makeinterval(lineno, idom, intvcount, intvlinestart);
		sbvil++;		/* only need to incr valintline, other is recalculated eacc h line */
		
		/* because line numbers increment up the screen, but frame memeory
		increments down the screen, to get to beginning of the next line we
		have to subtract two lines from the frame pointer */
		
		chr -= cols * 2;
	}	
}

/*
 *	F S M E M E R R O R  --  Error routine to give to 'Alloc' to
 *							 trap memory allocation failures
 */
int
fsmemerror(int code, char *estr)
{
	memerror = 1;
}


int
fscheckerror()
{
	int err;
	
	err = memerror;
	memerror = 0;	/* reset this ready for next request */
	return(err);
}


/*
 *	CAPCONSTRUCT
 *
 *	Works same as old fsconstruct - misses first and last line
 */
struct object *
capconstruct(FSCHAR *fsaddr, WZCOORD fslines, WZCOORD fscols, int fsthresh, int fsinvert)
{
	struct object *obj;

	FSCAPTURE=1;
	obj = fsconstruct(fsaddr, fslines, fscols, (FSCHAR)fsthresh, fsinvert);
	FSCAPTURE=0;
	return(obj);
}


