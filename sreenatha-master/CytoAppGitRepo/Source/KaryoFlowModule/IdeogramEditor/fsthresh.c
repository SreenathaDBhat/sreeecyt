
/*
 *	FSTHRESH.C	Mark Gregson	21/4/94
 *
 *	Splendid routines for automatically thresholding images
 *
 *	Mods:
 *
 *	10Mar2000	JMB		Changed headers included, as part of conversion to DLL.
 *						Also, made minor mods to stop compiler warnings and removed
 *						standard C parameter lists (were '#ifdef i386' only).
 *	9/20/96		BP:		Use FSCHAR, not Uchar.
 *						Include wstruct.h.
 *	MG	16Jul96		Made BackgroundThresh() even better !
 *	MG	12Jul96		Added new BackgroundThresh() routine
 */

#include "woolz.h"

#include <math.h>
//#include <wstruct.h>


static double round(num)	/* can you believe this doesn't exist in C ??? */
double num;
{
	double up,down;

	up=ceil(num);
	down=floor(num);

	if (num - down < 0.5 )
		return(down);
	else
		return(up);
}	
	



/* 	gradthresh
 *	
 *	automatic threshold determination by evaluation of grey level
 *	gradients in the image
 */
int
gradthresh(unsigned char *image, int cols, int lines)
{
	register unsigned char *imptr, *leftptr, *rightptr, *upptr, *downptr;
	register int line, col;
	int size, grad, g, val, thresh, cutoff, npixels;
	float SE, SG, SE1, SG1, fcutoff;
	
	float C = (float)0.62665707;	/* const C = sqrt(2*PI)/4 */


	size=cols*lines;
	
	imptr=image+cols;
	leftptr=imptr-1;
	rightptr=imptr+1;
	upptr=imptr-cols;
	downptr=imptr+cols;

	SE=SG=0.0;
	SE1=SG1=0.0;

	/* ignore first and last lines of image */

	for (line=1;line<lines-1;line++) {

		/* ignore first column of image */
		imptr++;
		leftptr++;
		rightptr++;
		upptr++;
		downptr++;

		for (col=1;col<cols-1;col++) {

			val=*imptr++;
			
			/* calculate max gradient around pixel */
			grad=abs(val-*leftptr++);

			g=abs(val-*rightptr++);
			if (g>grad) grad=g;

			g=abs(val-*upptr++);
			if (g>grad) grad=g;

			g=abs(val-*downptr++);
			if (g>grad) grad=g;

			/* calc product of grey level and max gradient */
			val*=grad;

			/* sum max gradients and grey level products */
			if (grad > 1) {
				SE+=grad;
				SG+=val;
			}

			/* calc sums ignoring small gradients */
			if (grad > 8) {
				SE1+=grad;
				SG1+=val;
			}
		}

		/* ignore last column of image */
		imptr++;
		leftptr++;
		rightptr++;
		upptr++;
		downptr++;
	}

	/* determine if noise adjustment required */

	/* number of pixels evaluated */
	npixels=(lines-2)*(cols-2);

	if (npixels<=1) 
		return(0);

	/* Determine gradient variation due to noise : */
	fcutoff = (float)((SE * C )/npixels - 1.0);

	/* round the number */
	cutoff=(int)round((double)fcutoff);

	/* calculate threshold */
	if (cutoff > 0) {
		if (SE1 == 0)
			thresh=0;
		else
			thresh=(int)(SG1/SE1);	/* noisy image */
	}
	else {
		if (SE == 0)
			thresh=0;
		else
			thresh=(int)(SG/SE);	/* normal image */
	}

	return(thresh);

}


/* 	gradthresh2
 *	
 *	automatic threshold determination by evaluation of grey level
 *	gradients in the image
 */
int
gradthresh2(unsigned char *image, int cols, int lines)
{
	register unsigned char *imptr, *leftptr, *rightptr, *upptr, *downptr;
	register int line, col;
	int size, grad, g, val, thresh, cutoff, npixels;
	float SE, SG, /*SE1, SG1,*/ fcutoff;
	
	float C = (float)0.62665707;	/* const C = sqrt(2*PI)/4 */


	size=cols*lines;
	
	imptr=image+cols;
	leftptr=imptr-1;
	rightptr=imptr+1;
	upptr=imptr-cols;
	downptr=imptr+cols;

	SE=SG=0.0;
	/* ignore first and last lines of image */

	for (line=1;line<lines-1;line++) {

		/* ignore first column of image */
		imptr++;
		leftptr++;
		rightptr++;
		upptr++;
		downptr++;

		for (col=1;col<cols-1;col++) {

			val=*imptr++;
			
			/* calculate max gradient around pixel */
			grad=abs(val-*leftptr++);

			g=abs(val-*rightptr++);
			if (g>grad) grad=g;

			g=abs(val-*upptr++);
			if (g>grad) grad=g;

			g=abs(val-*downptr++);
			if (g>grad) grad=g;

			/* calc product of grey level and max gradient */
			val*=grad;

			/* sum max gradients and grey level products */
			SE+=grad;
			SG+=val;

		}

		/* ignore last column of image */
		imptr++;
		leftptr++;
		rightptr++;
		upptr++;
		downptr++;
	}

	/* determine noise adjustment required */

	/* number of pixels evaluated */
	npixels=(lines-2)*(cols-2);

	if (npixels<=1) 
		return(0);

	/* Determine gradient variation due to noise : */
	fcutoff = (float)(6.0 * (SE * C )/npixels);

	/* round the number */
	cutoff=(int)round((double)fcutoff);

	imptr=image+cols;
	leftptr=imptr-1;
	rightptr=imptr+1;
	upptr=imptr-cols;
	downptr=imptr+cols;

	SE=SG=0.0;
	/* ignore first and last lines of image */

	for (line=1;line<lines-1;line++) {

		/* ignore first column of image */
		imptr++;
		leftptr++;
		rightptr++;
		upptr++;
		downptr++;

		for (col=1;col<cols-1;col++) {

			val=*imptr++;
			
			/* calculate max gradient around pixel */
			grad=abs(val-*leftptr++);

			g=abs(val-*rightptr++);
			if (g>grad) grad=g;

			g=abs(val-*upptr++);
			if (g>grad) grad=g;

			g=abs(val-*downptr++);
			if (g>grad) grad=g;

			/* calc product of grey level and max gradient */
			val*=grad;

			/* sum max gradients and grey level products */
			if (grad > cutoff) {
				SE+=grad;
				SG+=val;
			}

		}

		/* ignore last column of image */
		imptr++;
		leftptr++;
		rightptr++;
		upptr++;
		downptr++;
	}

	if (SE == 0)
		return(0);

	/* calculate threshold */
	thresh=(int)(SG/SE);

	return(thresh);

}

/***************************************************************** smooth_hist */

/* 
 
	Efficent technique to smooth a histogram ( 256 integers ) by N passes
	of a (1,1,1,1,1,1,1) filter
*/

static void
smooth_hist(int *hist, int passes)
{
	
	int i, pass, count;
	int exphist[256+50];	/* expanded histogram */
	int temp[4];
	long total;
	int *oldptr, *newptr, *maxhistptr;

	/* zero expanded hist - used to allow
	   smoothing around endpoints of hist */
	for (i=0;i<256+50;i++)
		exphist[i]=0;

	/* copy hist to expanded hist */
	for (i=0;i<256;i++)
		exphist[i+25]=hist[i];

	maxhistptr=exphist+256+50;

	/* for passes */
	for (pass=0;pass<passes;pass++) {

		total=0;
		oldptr=newptr=exphist;

		for (i=0;i<7;i++) {
			total+=*newptr;
			newptr++;
		}

		count=0;
		temp[count]=(total/7);

		/* smooth exphist with 7x1 mean filter */
		while (newptr<maxhistptr) {
			total-=*oldptr;
			total+=*newptr;

			count++;
			if (count>3) {
				*oldptr=temp[0];
				temp[0]=temp[1];
				temp[1]=temp[2];
				temp[2]=temp[3];
				temp[3]=(total/7);
			}
			else
				temp[count]=(total/7);

			oldptr++;
			newptr++;
		}
	}

	/* store result back to hist */
	for (i=0;i<256;i++)
		hist[i]=exphist[i+25];


}

/**********************************************************************************/
/*
 *	BackgroundThresh determines the threshold of an image by
 *	first calculating the approximate background level in the image
 *	and then analysing the greylevel gradients just above this level
 *	i.e. it analyses the contours of the objects.
 */

#define SMALLGRAD 	8	/* ignore gradients less than this */


int
BackgroundThresh(FSCHAR *image, int cols, int lines)
{
	FSCHAR *imptr, *leftptr, *rightptr, *upptr, *downptr;
	int size, grad, g, val, nval, thresh, max, mode;
	int i, lo, hi, valmax, range, hist[256];
	int line, col;
	int SE, SG;

	/* Let's not be silly */
	if ((!image) || (cols < 10) || (lines < 10))
		return(0);

	/* zero image histogram */
	for (i=0;i<256;i++)
		hist[i]=0;

	/* histogram image */
	imptr=image;
	size=cols*lines;
	while (size--)
		hist[*imptr++]++;

	/* smooth histogram */
	smooth_hist(hist,3);

	/* determine mode of histogram - approximate value of background */
	mode=0; max=0;
	for (i=0;i<256;i++) {
		if (hist[i]>max) {
			max=hist[i];
			mode=i;
		}
	}

	/* determine range of histogram */
	lo=0;
	while (hist[lo]==0)
		lo++;

	hi=255;
	while (hist[hi]==0)
		hi--;

	range=hi-lo;

	/* Let's not be silly */
	if (range < 10)
		return(0);

	/* set max value we are interested in to 
	   1/5 of the range above the mode - i.e.
	   around the contours of the chromosomes */
	valmax=range/5;

	/* peform gradient analysis on contours of objects
	   - data below mode and darker data inside objects
	   are ignored. */	
	imptr=image+cols;
	leftptr=imptr-1;
	rightptr=imptr+1;
	upptr=imptr-cols;
	downptr=imptr+cols;

	SE=1;	/* Sum of gradients : avoid divide by zero */
	SG=0;	/* Sum of gradient * grey val */

	/* ignore first and last lines of image */

	for (line=1;line<lines-1;line++) {

		/* ignore first column of image */
		imptr++;
		leftptr++;
		rightptr++;
		upptr++;
		downptr++;

		for (col=1;col<cols-1;col++) {

			val=*imptr++ - mode;

			if ((val < 0) || (val > valmax)) {

				/* ignore dark areas inside objects
				   and background (below mode) */
				leftptr++;
				rightptr++;
				upptr++;
				downptr++;
				continue;
			}
	
			/* calculate max gradient around pixel */
			nval=*leftptr++ - mode;

			if (nval < 0) nval=0;
			grad=abs(val-nval);

			nval=*rightptr++ - mode;

			if (nval < 0) nval=0;
			g=abs(val-nval);
			if (g>grad) grad=g;

			nval=*upptr++ - mode;

			if (nval < 0) nval=0;
			g=abs(val-nval);
			if (g>grad) grad=g;

			nval=*downptr++ - mode;

			if (nval < 0) nval=0;
			g=abs(val-nval);
			if (g>grad) grad=g;
			
			/* ignore snall gradients */
			if (grad > SMALLGRAD) {
				/* sum max gradients */
				SE+=grad;
				SG+=val*grad;
			}
		}

		/* ignore last column of image */
		imptr++;
		leftptr++;
		rightptr++;
		upptr++;
		downptr++;
	}

	/* threshold initially set at background value */
	thresh=mode;

	/* then increased based on gradients around objects */
	thresh += ((float)SG/(float)SE);
		
	return(thresh);
}

/**********************************************************************************/

