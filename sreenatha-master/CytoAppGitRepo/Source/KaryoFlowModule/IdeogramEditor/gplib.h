/*
 * Generated header file.
 * Created from translation of UNIX source.
 *
 * Copyright Applied Imaging corp. 1996
 * Created: Fri Sep 20 20:07:02 1996
 *
 * Original source directory: "/usr2/bp/cv/src/woolz/gplib"
 *
 *
 */


#ifdef WIN32


/****** fbio.c ******/

int fbread(register char *p, register int s, register int n, register FILE *fp);
int fbwrite(register char *p, register int s, register int n, register FILE *fp);


/****** misc.c ******/

int strntcpy(char *s1, char *s2, int n);
int strcasecmp(char *s1, char *s2);
int strncasecmp(char *s1, char *s2, int len);
int axtoi(char *ptr);
char *str2cmp(char *s, char *s1, char *s2);
int touch(char *name);
int bcopy(char *b1, char *b2, int length);
int instring(char *s1, char *s2);
int inbuffer(char *s1, int l1, char *s2, int l2);


#endif
