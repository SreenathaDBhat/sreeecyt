
/*
 * gstretch.c
 *
 * Implement the grey-stretching used in normalisation,
 * used to be coded in assembler - gstretch.a.  We assume that
 * none of the parameters exceed 255.
 *
 * Modifications
 *
 *	MG	25 Jun 92: 	dumped 68000 assembler version and built
 *				this from gstretch.proto
 *	BP	12/13/94:	Rewrite of gstretch to work with any size
 *				GREY values (bytes etc).
 *				Also new functions gtabcontrast() and gmmcontrast()
 *				which are sensible versions of gstretch() working
 *				on whole objects.
 */

#include "woolz.h"

//#include <wstruct.h>
#include <stdio.h>


#ifdef WIN32
void
gstretch(register GREY *g, int count, register GREY newmax, register GREY min, register GREY range)
#endif
#ifdef i386
void
gstretch(g, count, newmax, min, range)
register GREY *g;
int count;
register GREY newmax;
register GREY min;
register GREY range;
#endif
{
	register unsigned int i, val;


	if (range < 1)
	{
		fprintf(stderr, "gstretch: range < 1\n");
		return;
	}

	for (i = 0; i < count; i++)
	{
		val = (newmax*((int)(*g) - min))/range;
		*g++ = (GREY)val;
	}
}




/* Table used by gtabcontrast() and gcontrast() */
static GREY contrast_table[256];




/*************************************************** gtabcontrast */

/*
 * Apply contrast range to object grey values.
 *
 * A LUT of float values is used to perform
 * the translation.
 */

#ifdef WIN32
void
gtabcontrast(struct object *obj, float *floatlut)
#endif
#ifdef i386
void
gtabcontrast(obj, floatlut)
struct object *obj;
float *floatlut;
#endif
{
	register int i;
	register GREY *g;
	struct iwspace iwsp;
	struct gwspace gwsp;

/* Values less than min become zero. */
	for (i = 0; i < 256; i++)
		contrast_table[i] = (GREY)floatlut[i];

/* Traverse object grey table and apply
 * LUT to values. */
	initgreyscan(obj, &iwsp, &gwsp);
	while (nextgreyinterval(&iwsp) == 0)
	{
		g = gwsp.grintptr;
		for (i = iwsp.lftpos; i <= iwsp.rgtpos; i++)
		{
			*g = contrast_table[*g];
			g++;
		}
	}
}




/*************************************************** gmmcontrast */

/*
 * Apply contrast range to object grey values.
 *
 * The supplied min and max values are used to
 * perform linear contrast stretch via a LUT.
 */

#ifdef WIN32
void
gmmcontrast(struct object *obj, GREY min, GREY max)
#endif
#ifdef i386
void
gmmcontrast(obj, min, max)
struct object *obj;
GREY min;
GREY max;
#endif
{
    register int i, val;
    register GREY *g, range;
    struct iwspace iwsp;
    struct gwspace gwsp;

/* Do nothing if range is invalid. */
    if (min >= max)
        return;

    if (min < 0)
        min = 0;
    if (max > 255)
        max = 255;

    range = max - min;

/* Values less than min become zero. */
    for (i = 0; i < min; i++)
        contrast_table[i] = 0;

/* Values within range (including min and
 * max) are modified by linear table. */
    for ( ; i <= max; i++)
    {
        val = 255*(i - min);
        contrast_table[i] = val/range;
    }

/* Values more than max become 255. */
    for ( ; i < 256; i++)
        contrast_table[i] = 255;

/* Traverse object grey table and apply
 * LUT to values. */
    initgreyscan(obj, &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0)
    {
        g = gwsp.grintptr;
        for (i = iwsp.lftpos; i <= iwsp.rgtpos; i++)
        {
            *g = contrast_table[*g];
            g++;
        }
    }
}

