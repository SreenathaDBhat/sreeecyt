
/*
 * ideogram.cpp
 * JMB January 2000
 */

#include "stdafx.h"

#include "ideogramdll.h"

#include <stdlib.h> // For atof




static
void MakeStringParseable(char *pStr)
{
	// The sscanf function, used to parse fields when reading in lines from an
	// ideogram set file, will not recognise a string field that consists only
	// of what it considers to be whitespace characters, i.e. space ' ', 
	// tab '\t' or newline '\n', or is of zero length. If this happens, it will
	// then incorrectly assign the next string it finds to the field. To prevent
	// this, if string is zero-length, replace it by a single question mark.
	// Otherwise, replace any whitepace characters by a dash, otherwise strings
	// with spaces in them will be parsed as more than one string field.
	if (strlen(pStr) < 1) strcpy(pStr, "?");
	else 
	{
		char *pWhtSpc;
		while ((pWhtSpc = strpbrk(pStr, " \t\n")) != NULL) 
		{
			*pWhtSpc = '-'; // Replace whitespace with a dash.
		}
	}
}


/*
 *****************************************************************************
 * Band
 *****************************************************************************
 */

Band::Band()
{
	type             = BTundefined;
	colour[0] = colour[1] = colour[2] = 0;
	name[0]          = '\0';
//	cumulativeLength = 0.0;
	length           = 0.0;
	pNextBand        = NULL;
	showName         = FALSE;
	selected         = FALSE;
}

BOOL
Band::ReadFromIBFBuffer(char buf[], float *pPrevCumLen)
{
	switch (buf[0])
	{
		case 'B': // Band
		{
			char id[IBF_STR_MAXLEN];
			char tmp[IBF_STR_MAXLEN];
			char cumLen[IBF_STR_MAXLEN]; // Cumulative length
			int rgb;
			// Note the 'rgb' field is an extension to the original format, 
			// and will not appear in old files.
			//sscanf(buf, "B:%*[ \t]%50[^ \t]%*[ \t]%20[^ \t]%*[ \t]%20[^ \t]%*[ \t]%20[^ \t]", scn, name, tmp, len);
//TODO: This doesn't work if the band doesn't have a name - presumably sscanf
//doesn't carry on reading the rest of the fields. Presumably this may be a
//potential problem in other areas of the code where string fields are empty - sort this out.
			int numFields = sscanf(buf, "B: %"IBF_STR_MAXLEN_STR"s"
			                               "%"IBF_STR_MAXLEN_STR"s"
			                               "%"IBF_STR_MAXLEN_STR"s"
			                               "%"IBF_STR_MAXLEN_STR"s"
			                               "%d", id, name, tmp, cumLen, &rgb);

			// Default colour is black
			colour[0] = colour[1] = colour[2] = 0;

			if      (strcmp (tmp, "CH") == 0) type = BTcentHet;
			else if (strcmp (tmp, "NH") == 0) type = BTnonCentHet;
			else if (strcmp (tmp, "SH") == 0) type = BTsatellite;
			else if (strcmp (tmp, "ST") == 0) type = BTstalk;
			else                      
			{        
				type = BTband;

				if (numFields > 4)
				{	// rgb field is present in record - use this instead of grey value.
					// Extract the R, G, B components.
					colour[0] =  rgb        & 0xFF;
					colour[1] = (rgb >> 8)  & 0xFF;
					colour[2] = (rgb >> 16) & 0xFF;
				}
				else
				{
					// Get grey value from 0.0 to 1.0 (white to black)
					double fGrey = 1.0 - atof(tmp);
					int iGrey = (int)(255.0 * fGrey);
					colour[0] = colour[1] = colour[2] = iGrey;
				}
			}

			length = (float)atof(cumLen) - *pPrevCumLen;
			*pPrevCumLen = (float)atof(cumLen);

			if (length < 0.0) return FALSE;
		}
		break;

		case 'C':
		{
			type = BTcentromere;
		}
		break;

		default: return FALSE; // Error
	}

	return TRUE;
}

BOOL
Band::WriteToIBFFile(FILE *pFile, const char ideoID[], float cumLen)
{
	BOOL status = FALSE;


	MakeStringParseable(name);


	if (type != BTcentromere)
	{
		char tmpStr[IBF_STR_MAXLEN];
		float fGrey;

		switch (type)
		{
			case BTnonCentHet: strcpy(tmpStr, "NH"); break;
			case BTstalk:      strcpy(tmpStr, "ST"); break;
			case BTsatellite:  strcpy(tmpStr, "SH"); break;
			case BTcentHet:    strcpy(tmpStr, "CH"); break;
			case BTband:
			{
				// Get grey value from 0.0 to 1.0 (white to black)
				int iGrey = (colour[0] + colour[1] + colour[2]) / 3;

				fGrey = 1.0F - (float)iGrey / 255.0F;
			}
			break;
			default: return FALSE; // Unknown band type.
		}


		// Pack the rgb values into a single number.
		// I think this is actually the same format as a Win32 COLORREF, so I
		// could have used the RGB() macro.
		int rgb = colour[0] | ((int)colour[1] << 8) | ((int)colour[2] << 16);


		// Write the band record.
		if (type != BTband)
		{
			if (fprintf(pFile, "B: %s %s\t%s\t%.3f\n", 
			                        ideoID, name, tmpStr, cumLen) >= 0) 
				status = TRUE;
		}
		else
		{
			if (colour[1] != colour[0] || colour[2] != colour[0])
			{ 
				// This band is coloured. This must be described with an extra
				// field, in an extension to the file format.
				if (fprintf(pFile, "B: %s %s\t%.2f\t%.3f\t%d\n", 
				                        ideoID, name, fGrey, cumLen, rgb) >= 0) 
					status = TRUE;
			}
			else
			{
				if (fprintf(pFile, "B: %s %s\t%.2f\t%.3f\n", 
				                        ideoID, name, fGrey, cumLen) >= 0) 
					status = TRUE;
			}
		}
	}
	else
	{
		if (fprintf(pFile, "C: %s cen\n", ideoID) >= 0)
			status = TRUE;
	}


	return status;
}

/*
 *****************************************************************************
 * Ideogram
 *****************************************************************************
 */

Ideogram::Ideogram()
{
	pBandList      = NULL;
	id[0]          = '\0';
	relativeLength = 0.0;
	pNextIdeogram  = NULL;
}

Ideogram::Ideogram(const char i[])
{
	pBandList      = NULL;
	strncpy(id, i, IBF_STR_MAXLEN);	
	relativeLength = 0.0;
	pNextIdeogram  = NULL;
}

Ideogram::~Ideogram()
{
	Band *pCurrBand = pBandList;

	while (pCurrBand != NULL)
	{
		Band *pNextBand = pCurrBand->pNextBand;

		delete pCurrBand;

		pCurrBand = pNextBand;
	}
}

BOOL
Ideogram::ReadFromIBFFile(FILE *pFile)
{
// Read a sequence of IBF lines representing one ideogram. 
// The first line found is expected to be an 'N:' (start of chromosome) record,
// the last line read is an 'E:' (end of chromosome) record. In between there
// may be a number of 'B:' (band) records and normally just one 'C:'
// (centromere) record.
// Normally, band intensity reflects 'ink' - 0.0 is white, 1.0 is black;  
// if invert is true, reverse this.
	BOOL status = FALSE;
	char buf[IBF_LINE_MAXLEN];
	char name[IBF_STR_MAXLEN];

	// Skip lines until a record line is found.
	buf[0] = '\0';
	while (fgets(buf, IBF_LINE_MAXLEN, pFile) != NULL) 
		if (buf[0] != '\0' && buf[1] == ':') break;

	if (buf[0] != 'N') 
	{
		// We have reached end of file: there are no more ideograms defined in
		// this file. Calling function can check for this by seeing 
		// if strlen(pIdeogram->id) is zero.
		if (feof(pFile)) return TRUE;

		// File error, or record other than N record is not first record found
		// (error).
		return FALSE; 
	}

	// Parse ideogram parameters from line.
	// e.g. "N: 1 pter	100.0"
	//sscanf (buf, "N: %50[^ \t]%*[ \t]%20[^ \t]%f\n", scn, name, &makeThisLength);
	sscanf(buf, "N: %"IBF_STR_MAXLEN_STR"s"
	               "%"IBF_STR_MAXLEN_STR"s%f", id, name, &relativeLength);


	pBandList = NULL;
	Band *pPrevBand = NULL;

	float cumulativeLength = 0.0;

	// Now read in the bands, until an 'E:' record is found.
	while (fgets(buf, IBF_LINE_MAXLEN, pFile) != NULL) 
	{
		// Allow blank and comment lines
		if (buf[0] != '\0' && buf[1] == ':')
		{
			if (buf[0] == 'B' || buf[0] == 'C')
			{
				Band *pNewBand = new Band;
				if (pNewBand) status = pNewBand->ReadFromIBFBuffer(buf, &cumulativeLength);
				else
				{
					status = FALSE;
					break;
				}

				if (pBandList == NULL) pBandList = pNewBand; // First band
				else if (pPrevBand != NULL) pPrevBand->pNextBand = pNewBand;

				pPrevBand = pNewBand;
			}
			else if (buf[0] == 'E') 
			{ 
				pPrevBand->pNextBand = NULL; // Null-terminate list 
				break; 
			}
			else
			{
				// An unknown record type, or one which shouldn't be here.
				return FALSE;
			}
		}
	}

	return status;
}

BOOL 
Ideogram::WriteToIBFFile(FILE *pFile, float comparisonLength)
{
// Write a sequence of IBF lines representing one ideogram. 
// The first line is an 'N:' (start of chromosome) record,
// the last line is an 'E:' (end of chromosome) record. In between there
// may be a number of 'B:' (band) records and normally just one 'C:'
// (centromere) record.

	// Spacing line.
	if (fputs("\n", pFile) < 0) return FALSE; 


	MakeStringParseable(id);


	// Work out comparison length as a percentage, although I'm not sure it
	// needs to be a percentage, according to the original Vysis spec the units
	// were arbitary, though it did seem to be a de-facto rule that they were a
	// percentage in practice.
	if (comparisonLength > 0.0f)
		relativeLength = (GetSumOfBandLengths() / comparisonLength) * 100;
	else
		relativeLength = 0.0f;


	if (fprintf(pFile, "N: %s ter\t%.3f\n", id, relativeLength) < 0) return FALSE;

	float cumLen = 0.0;

	Band *pCurrBand = pBandList;
	while (pCurrBand)
	{
		cumLen += pCurrBand->length;

		if (!pCurrBand->WriteToIBFFile(pFile, id, cumLen)) return FALSE;

		pCurrBand = pCurrBand->pNextBand;
	}


	if (fprintf(pFile, "E: %s ter\n", id) < 0) return FALSE;


	return TRUE;
}

Band * 
Ideogram::GetPrevBand(Band *pBand)
{
	Band *pPrevBand = NULL;

	if (pBand != pBandList) // Band is at head: no previous band.
	{
		Band *pCurrBand  = pBandList;
		while (pCurrBand)
		{
			if (pCurrBand->pNextBand == pBand)
			{
				pPrevBand = pCurrBand;
				break;
			}
			pCurrBand = pCurrBand->pNextBand;
		}
	}

	return pPrevBand;
}

void 
Ideogram::ClearSelections()
{
	Band *pCurrBand = pBandList;
	while (pCurrBand != NULL)
	{
		pCurrBand->selected = FALSE;
		pCurrBand = pCurrBand->pNextBand;
	}
}

void 
Ideogram::SelectBand(Band *pBand)
{
	ClearSelections();
	
	if (pBand) pBand->selected = TRUE;
}

Band *
Ideogram::GetSelectedBand()
{
	Band *pSelectedBand = NULL;
	Band *pCurrBand = pBandList;
	while (pCurrBand != NULL)
	{
		if (pCurrBand->selected) pSelectedBand = pCurrBand;
		pCurrBand = pCurrBand->pNextBand;
	}
	return pSelectedBand;
}

void 
Ideogram::RemoveSelectedBand()
{
	Band *pSelBand = GetSelectedBand();

	if (pSelBand != NULL)
	{
		Band *pPrevBand = GetPrevBand(pSelBand);

		if (pSelBand->type == Band::BTcentHet)
		{	// Band is a centromeric hetrochromatin band. If this is adjacent
			// to a centromere marker band (as it always should be), remove the
			// marker band also, as a centromere must have a centromeric
			// hetrochromatin band either side to be valid, and cannot be 
			// removed directly as it is not visible due to being zero-length.
			if (pPrevBand && pPrevBand->type == Band::BTcentromere)
			{	// There is a centromere marker band above selected band.
				Band *pPrevPrevBand = GetPrevBand(pPrevBand);
				delete pPrevBand;
				pPrevBand = pPrevPrevBand;

				// If the centromere marker was preceeded by another
				// centromeric hetrochromatin band (as it should have been)
				// that must be changed to a non-centromeric hetrochromatin
				// band, as there is no centromere any more.
				if (pPrevBand && pPrevBand->type == Band::BTcentHet)
					pPrevBand->type = Band::BTnonCentHet;
			}
			
			if (pSelBand->pNextBand && pSelBand->pNextBand->type == Band::BTcentromere)
			{	// There is a centromere marker band below selected band.
				Band *pNextNextBand = pSelBand->pNextBand->pNextBand;
				delete pSelBand->pNextBand;
				pSelBand->pNextBand = pNextNextBand;

				// If the centromere marker was followed by another
				// centromeric hetrochromatin band (as it should have been)
				// that must be changed to a non-centromeric hetrochromatin
				// band, as there is no centromere any more.
				if (pSelBand->pNextBand && pSelBand->pNextBand->type == Band::BTcentHet)
					pSelBand->pNextBand->type = Band::BTnonCentHet;
			}
		}


		// Remove the selected band freom the list.
		if (pPrevBand != NULL)
			pPrevBand->pNextBand = pSelBand->pNextBand;
		else
			// Selected band was head of list.
			pBandList = pSelBand->pNextBand;

		// Delete the selected band.
		delete pSelBand;
	}
}

float
Ideogram::GetSumOfBandLengths()
{
	float total = 0.0;

	Band *pCurrBand = pBandList;
	while (pCurrBand != NULL)
	{
		total += pCurrBand->length;
		pCurrBand = pCurrBand->pNextBand;
	}

	return total;
}

float
Ideogram::GetCentromericIndex()
{
// The centromeric index, for human chromosomes at least, is defined in ISCN
// (1995) as the ratio of the length of the shorter arm to the whole length.
// As ideograms are conventionally drawn with the shorter arm uppermost, this
// will be assumed.
// If there is no centromere, an index of zero is returned. If there is more
// than one centromere, the index of the centromere furthest from the top will
// be returned.
	float index = 0.0f;
	float total = 0.0f;
	float centromeric = 0.0f;

	Band *pCurrBand = pBandList;
	while (pCurrBand != NULL)
	{
		total += pCurrBand->length;

		if (pCurrBand->type == Band::BTcentromere) centromeric = total;

		pCurrBand = pCurrBand->pNextBand;
	}

	if (total > 0.0) index = centromeric / total;
	else             index = 0.0;

	return index;
}

/*
 *****************************************************************************
 * IdeogramSet
 *****************************************************************************
 */

IdeogramSet::IdeogramSet()
{
	pComments     = NULL;
	pIdeogramList = NULL;
	species[0]    = '\0';
	resolution[0] = '\0';
	bandtype[0]   = '\0';
	width         = 0.0;
}

IdeogramSet::IdeogramSet(const char s[], const char r[], const char b[], 
                         const float w)
{
	pComments     = NULL;
	pIdeogramList = NULL;
	SetProperties(s, r, b, w);
}

IdeogramSet::~IdeogramSet()
{
	Ideogram *pCurrIdeo = pIdeogramList;

	while (pCurrIdeo != NULL)
	{
		Ideogram *pNextIdeo = pCurrIdeo->pNextIdeogram;

		delete pCurrIdeo;

		pCurrIdeo = pNextIdeo;
	}

	free(pComments);
}

Ideogram * 
IdeogramSet::GetPrevIdeogram(Ideogram *pIdeogram)
{
	Ideogram *pPrevIdeo = NULL;

	if (pIdeogram != pIdeogramList) // Band is at head: no previous band.
	{
		Ideogram *pCurrIdeo = pIdeogramList;
		while (pCurrIdeo)
		{
			if (pCurrIdeo->pNextIdeogram == pIdeogram)
			{
				pPrevIdeo = pCurrIdeo;
				break;
			}
			pCurrIdeo = pCurrIdeo->pNextIdeogram;
		}
	}

	return pPrevIdeo;
}

Ideogram *
IdeogramSet::Find(const char id[])
{
	Ideogram *pIdeo = pIdeogramList;

	while (pIdeo)
	{
		// Case INsensitive comparison for letters.
		if (stricmp(pIdeo->id, id) == 0) break;

		pIdeo = pIdeo->pNextIdeogram;
	}

	// Will be NULL if got to end of list without a match, or if list empty.
	return pIdeo; 
}

void 
IdeogramSet::SetProperties(const char s[], const char r[], const char b[],
                           const float w)
{
	strncpy(species, s, IBF_STR_MAXLEN);
	strncpy(resolution, r, IBF_STR_MAXLEN);
	strncpy(bandtype, b, IBF_STR_MAXLEN);
	width = w;
}

BOOL
IdeogramSet::ReadIBFFile(FILE *pFile)
{
// Read set from Ideogram Band Format (IBF) file.
	BOOL status = FALSE;
	char buf[IBF_LINE_MAXLEN];
	int invert = 0;

	// Lines before the set definition record are comments: read into a buffer.
	size_t commentsSize = 1; // Want a null terminator at least.
	buf[0] = '\0';
	while (fgets(buf, IBF_LINE_MAXLEN, pFile) != NULL)
	{
		if (buf[0] == 'S' && buf[1] == ':') break;

		// strlen doesn't include the null terminator, but we don't want it for
		// every line anyway (just at the end of the buffer).
		commentsSize += strlen(buf); 
		pComments = (char *)realloc(pComments, commentsSize);
		// Don't use strcpy as we don't want a null for each line.
		memcpy(pComments + commentsSize - strlen(buf) - 1, buf, strlen(buf));

		pComments[commentsSize - 1] = '\0'; // Ensure comments buffer is null-terminated.
	}
	
	if (buf[0] != 'S' || buf[1] != ':') return FALSE; // S record not found (could be file error).


	// Parse set parameters from line.
	// e.g. "S: Human 400 G-band 9.0" (invert parameter is often missing).
	//sscanf (buf, "S: %50[^ \t]%*[ \t]%50[^ \t]%*[ \t]%50[^ \t]%f%d",
	//             species, resolution, bandtype, &suggestedWidth, &invert);
	sscanf(buf, "S: %"IBF_STR_MAXLEN_STR"s"
	               "%"IBF_STR_MAXLEN_STR"s"
	               "%"IBF_STR_MAXLEN_STR"s"
	               "%f%d",
	               species, resolution, bandtype, &width, &invert);


#ifdef GET_RID_OF_THIS_OLD_CODE_WHEN_YOURE_SURE_THE_NEW_CODE_WORKS
/*
	Ideogram *pNewIdeogram;
	pNewIdeogram = new Ideogram;
	if (pNewIdeogram) status = pNewIdeogram->ReadFromIBFFile(pFile);
	else              status = FALSE;

	pIdeogramList = pNewIdeogram;

	while (pNewIdeogram && status)
	{
		pNewIdeogram->pNextIdeogram = new Ideogram;
		if (pNewIdeogram->pNextIdeogram)
			status = pNewIdeogram->pNextIdeogram->ReadFromIBFFile(pFile);
		else status = FALSE;

		if (status && strlen(pNewIdeogram->pNextIdeogram->id) < 1)
		{
			// Status is okay but no ideogram id read. This means
			// end-of-file has been reached and there are no more ideograms
			// defined in this set.
			// Delete unused Ideogram object and null-terminate list.
			delete pNewIdeogram->pNextIdeogram;
			pNewIdeogram->pNextIdeogram = NULL;
			break;
		}

		pNewIdeogram = pNewIdeogram->pNextIdeogram;
	}
*/
#else

	Ideogram *pPrevIdeogram = NULL;

	status = FALSE;
	do	
	{
		Ideogram *pNewIdeogram = new Ideogram;

		if (pNewIdeogram) 
		{
			if ((status = pNewIdeogram->ReadFromIBFFile(pFile)) != FALSE)
			{
				if (strlen(pNewIdeogram->id) < 1)
				{
					// Read status is okay but no ideogram id read. This means
					// end-of-file has been reached and there are no more 
					// ideograms defined in this set.
					// Delete unused Ideogram object and null-terminate list.
					delete pNewIdeogram;
					if (pPrevIdeogram) pPrevIdeogram->pNextIdeogram = NULL;
					else               pIdeogramList = NULL;

					break; // Break do-while(status) loop.
				}
				else
				{
					if (pPrevIdeogram) 
						pPrevIdeogram->pNextIdeogram = pNewIdeogram;
					else
						pIdeogramList = pNewIdeogram;
					
					pPrevIdeogram = pNewIdeogram;
				}
			}
			else 
				delete pNewIdeogram;
		}
	}
	while (status);

#endif


	return status;
}

BOOL
IdeogramSet::WriteIBFFile(FILE *pFile)
{
	// Write comments lines (if any).	
	if (pComments) 
		if (fputs(pComments, pFile) < 0) return FALSE;


	// Write set definition record.
// TODO: these fields should be tab delimited so that they can have spaces in them, 
// however I'm not sure what the reading routine would make of that. Must be able to read
// without tabs, else old files won't work (unless we change the format - may need to).
	MakeStringParseable(species);
	MakeStringParseable(resolution);
	MakeStringParseable(bandtype);

	if (fprintf(pFile, "S: %s\t%s\t%s\t%.3f\n", 
	                       species, resolution, bandtype, width) < 0) return FALSE;


	// Get length of first chromosome: all chromosomes' relative lengths will
	// be given as a percentage of this.
	float comparisonLength = 0.0;
	if (pIdeogramList)
	{
		comparisonLength = pIdeogramList->GetSumOfBandLengths();
	}


	// Write ideogram records.
	Ideogram *pCurrIdeo = pIdeogramList;
	while (pCurrIdeo)
	{
		if (!pCurrIdeo->WriteToIBFFile(pFile, comparisonLength)) return FALSE;

		pCurrIdeo = pCurrIdeo->pNextIdeogram;
	}


	// I like to have a blank line at the end of a file.
	if (fputs("\n", pFile) < 0) return FALSE; 


	return TRUE;
}

