
/*
 * ideogramdll.h
 * JMB January 2000
 * Would be called ideogram.h, but there is currently already an ideogram.h in WinCV, 
 * for handling the old-style ideograms. If that is got rid of in the course of things,
 * then rename this to ideogram.h.
 */

#ifndef IDEOGRAM_H
#define IDEOGRAM_H


#include <stdio.h> // For FILE
#include <string.h>



/****************************************
 * Begin visual studio generated code ...
 */

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the IDEOGRAM_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// IDEOGRAM_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef IDEOGRAM_EXPORTS
#define IDEOGRAM_API
#else
#define IDEOGRAM_API
#endif

#ifdef __cplusplus
// This class is exported from the ideogram.dll
class IDEOGRAM_API CIdeogram {
public:
	CIdeogram(void);
	// TODO: add your methods here.
};
#endif

extern IDEOGRAM_API int nIdeogram;

IDEOGRAM_API int fnIdeogram(void);

/*
 * ... end visual studio generated code.
 ***************************************/



//////////////////////////////////////////////////////////////////////////////


// Limits for Ideogram Band Format file.
#define  IBF_LINE_MAXLEN     256
#define  IBF_STR_MAXLEN       50
#define  IBF_STR_MAXLEN_STR  "50" // Same as IBF_STR_MAXLEN, but as a string


#ifdef __cplusplus


class IDEOGRAM_API Band
{
public:
	typedef enum
	{
		BTundefined,
		BTband,      // Normal band
		BTstalk,     // Stalk band (colour not valid)
		BTsatellite, // Satellite band (colour not valid)
		BTnonCentHet,// Non-centromeric heterochromatin band (colour not valid) 
		BTcentHet,   // Centromeric heterochromatin band (colour not valid)
		BTcentromere // Zero-length marker
	} 
	BandType;

	//float cumulativeLength; // As read from IBF file.

	BandType type;
	int colour[3];
	char name[IBF_STR_MAXLEN];
	float length; // Arbitary units.
	              // Note: this bears no relationship to the relative
	              // length value for the whole chromosome.
	Band *pNextBand;


	BOOL showName; // Not stored in IBF file
	BOOL selected; // Not stored in IBF file


	Band();
	BOOL ReadFromIBFBuffer(char buf[], float *pPrevCumLen);
	BOOL WriteToIBFFile(FILE *pFile, const char ideoID[], float cumLen);
};

class IDEOGRAM_API Ideogram
{
public:
	Band *pBandList;

	char id[IBF_STR_MAXLEN]; // Ideogram 'number'. Must be a string as it can
	                         // be letters too, e.g. "X", "Y".

	float relativeLength; // Arbitary units: relative to other chromosomes in set.
	                      // Note: this bears no relationship to the length
	                      // measurements for the bands.

	Ideogram *pNextIdeogram;

	Ideogram();
	Ideogram(const char i[]);
	~Ideogram();
	BOOL ReadFromIBFFile(FILE *pFile);
	BOOL WriteToIBFFile(FILE *pFile, float comparisonLength);
	Band * GetPrevBand(Band *pBand);
	void ClearSelections();
	void SelectBand(Band *pBand);
	Band *GetSelectedBand();
	void RemoveSelectedBand();
	float GetSumOfBandLengths();
	float GetCentromericIndex();
};

class IDEOGRAM_API IdeogramSet
{
public:
	char *pComments;

	char species[IBF_STR_MAXLEN];
	char resolution[IBF_STR_MAXLEN];
	char bandtype[IBF_STR_MAXLEN];

	float width; // Width of chromosomes: in SAME (arbitary) units as length
	             // measurements for bands?

	Ideogram *pIdeogramList;

	IdeogramSet();
	IdeogramSet(const char s[], const char r[], const char b[], 
                const float w);
	~IdeogramSet();
	BOOL ReadIBFFile(FILE *pFile);
	BOOL WriteIBFFile(FILE *pFile);
	Ideogram *GetPrevIdeogram(Ideogram *pIdeogram);
	Ideogram *Find(const char id[]);
	void SetProperties(const char s[], const char r[], const char b[],
                       const float w);
};


//////////////////////////////////////////////////////////////////////////////


class IDEOGRAM_API IdeoPolyText
{
public:
	float x1;
	float y1;
	float x2;
	float y2;
	char text[256];
	IdeoPolyText *pNextText;

	IdeoPolyText(float a, float b, float c, float d, char *s)
	             { x1 = a; y1 = b; x2 = c; y2 = d; 
	               strncpy(text, s, 256);
	               pNextText = NULL; }
};

class IDEOGRAM_API IdeoPolyVertex
{
public:
	float x;
	float y;
	IdeoPolyVertex *pNextVertex;

	IdeoPolyVertex(){ x = y = 0.0f; pNextVertex = NULL; }
	IdeoPolyVertex(float a, float b){ x = a; y = b; pNextVertex = NULL; }
};


// Bits set for IdeoPoly properties
#define IDEOPOLY_LRHATCH  0x01 // Poly is filled using hatching
#define IDEOPOLY_RLHATCH  0x02
#define IDEOPOLY_VHATCH   0x04
#define IDEOPOLY_SELECT   0x80 // Poly is a selection rectangle

class IDEOGRAM_API IdeoPoly
{
public:
	int numVertices;
	IdeoPolyVertex *pVertices;

	unsigned char colour[3];
	unsigned char properties;

	IdeoPoly *pNextPoly;

	IdeoPolyVertex *AddVertex(float x, float y);
	IdeoPolyVertex *GetVertex(int index);

	void SetColour(int r, int g, int b){ colour[0] = r; colour[1] = g; colour[2] = b; }

	IdeoPoly(){ numVertices = 0; pVertices = NULL; pNextPoly = NULL; properties = 0;}
	~IdeoPoly();
};

class IDEOGRAM_API IdeoPolySet
{
public:
	int numPolys;
	IdeoPoly *pPolys;

	float scale;
	float rotation; 

	int numTexts;
	IdeoPolyText *pTexts; 

	void AddPolysFromBand(Band *pBand, Band *pPrevBand, 
	                      float top, float bottom, float width);
	IdeoPoly *AddPoly();
	IdeoPoly *GetPoly(int index);

	IdeoPolyText *AddText(float x, float y, float w, float h, char s[]);
	IdeoPolyText *GetText(int index);

	void *CreateBuffer(int *pBufSize);

	void GetExtents(float *pXmin, float *pXmax, float *pYmin, float *pYmax);

	IdeoPolySet(Ideogram *pIdeo, float width, float initialScale);
	IdeoPolySet(void *pBuffer);
	~IdeoPolySet();
};


#endif  // __cplusplus


#ifdef __cplusplus
extern "C" {
#endif

IDEOGRAM_API void GetValuesFromIdeoPolySetBuffer(void *pBuffer, float *pScale, float *pRotAng);
IDEOGRAM_API void SetValuesInIdeoPolySetBuffer(void *pBuffer, float scale, float rotAng);

#ifdef __cplusplus
}
#endif

//////////////////////////////////////////////////////////////////////////////



#endif
