/*
 * ideopoly.cpp
 *
 * JMB April - August 2000
 *
 * Mods:
 *
 */

#include "stdafx.h"

#include "ideogramdll.h"

#include <stdlib.h> // For malloc()
#include <math.h>
#include <float.h>  // For FLT_MAX
#include <limits.h> // For INT_MAX
#include <assert.h>


#define BUFFER_VERSION  1



IdeoPolyVertex*
IdeoPoly::GetVertex(int index)
{
	IdeoPolyVertex *pCurrVertex = pVertices;

	while (index-- > 0 && pCurrVertex != NULL)
	{
		pCurrVertex = pCurrVertex->pNextVertex;
	}

	return pCurrVertex;
}

IdeoPolyVertex*
IdeoPoly::AddVertex(float x, float y)
{
	IdeoPolyVertex *pNewVertex = new IdeoPolyVertex(x, y);

	if (pNewVertex)
	{
/*
		// Insert at head of list
		pNewVertex->pNextVertex = pVertices;
		pVertices = pNewVertex;
*/
		// Insert at tail of list, so that the list preserves the order in
		// which the vertices were added.
		pNewVertex->pNextVertex = NULL;
		if (pVertices != NULL) 
		{
			// Find last element in list...
			IdeoPolyVertex *pCurrVertex = pVertices;
			while (pCurrVertex->pNextVertex != NULL)
			{
				pCurrVertex = pCurrVertex->pNextVertex;
			}

			// ...add new vertex after it.
			pCurrVertex->pNextVertex = pNewVertex;
		}
		else // List is empty
		{
			pVertices = pNewVertex;			
		}

		numVertices++;
	}

	return pNewVertex;
}

IdeoPoly::~IdeoPoly()
{
	while (pVertices != NULL)
	{
		IdeoPolyVertex *pCurrVertex = pVertices;
		pVertices = pVertices->pNextVertex;
		delete pCurrVertex;
	}

	numVertices = 0;
}


void
GetValuesFromIdeoPolySetBuffer(void *pBuffer, float *pScale, float *pRotAng)
{
	char *pByte = (char*)pBuffer;

	pByte += sizeof(int); // Skip version number
	memcpy(pScale,  pByte, sizeof(*pScale));  pByte += sizeof(*pScale);
	memcpy(pRotAng, pByte, sizeof(*pRotAng)); pByte += sizeof(*pRotAng);
}

void
SetValuesInIdeoPolySetBuffer(void *pBuffer, float scale, float rotAng)
{
	char *pByte = (char*)pBuffer;

	pByte += sizeof(int); // Skip version number
	memcpy(pByte, &scale,  sizeof(scale));  pByte += sizeof(scale);
	memcpy(pByte, &rotAng, sizeof(rotAng)); pByte += sizeof(rotAng);
}


void 
IdeoPolySet::GetExtents(float *pXmin, float *pXmax, float *pYmin, float *pYmax)
{
// Get extents (before scaling and rotating).
	*pXmin = *pYmin = +FLT_MAX;
	*pXmax = *pYmax = -FLT_MAX;

	IdeoPoly *pCurrPoly = pPolys;
	while (pCurrPoly != NULL)
	{
		IdeoPolyVertex *pCurrVertex = pCurrPoly->pVertices;
		while (pCurrVertex != NULL)
		{
			float scaledX = pCurrVertex->x/* * scale*/;
			float scaledY = pCurrVertex->y/* * scale*/;

			if (scaledX < *pXmin) *pXmin = scaledX;
			if (scaledX > *pXmax) *pXmax = scaledX;
			if (scaledY < *pYmin) *pYmin = scaledY;
			if (scaledY > *pYmax) *pYmax = scaledY;

			pCurrVertex = pCurrVertex->pNextVertex;
		}

		pCurrPoly = pCurrPoly->pNextPoly;
	}
}



void*
IdeoPolySet::CreateBuffer(int *pBufSize)
{
	void *pBuffer = NULL;
	*pBufSize = 0;

	const int version = BUFFER_VERSION; // Buffer format version number

/////////////////////////////////////////////////////////////////////////////
// Calculate buffer size
	int bufSize =   sizeof(version) 
	              + sizeof(scale) + sizeof(rotation) 
	              + sizeof(numPolys) + sizeof(numTexts);

	IdeoPoly *pCurrPoly = pPolys;
	while (pCurrPoly != NULL)
	{
		IdeoPolyVertex aVertex(0,0);
		bufSize +=   sizeof(pCurrPoly->colour) 
		           + sizeof(pCurrPoly->properties)

		           + sizeof(pCurrPoly->numVertices)
		           + pCurrPoly->numVertices * (  sizeof(aVertex.x) 
		                                       + sizeof(aVertex.y) );
		pCurrPoly = pCurrPoly->pNextPoly;
	}

	IdeoPolyText *pCurrText = pTexts;
	while (pCurrText != NULL)
	{
		bufSize +=   sizeof(pCurrText->x1) + sizeof(pCurrText->y1)
		           + sizeof(pCurrText->x2) + sizeof(pCurrText->y2)

		           + sizeof(int) // Stores string length for text
		           + strlen(pCurrText->text);

		pCurrText = pCurrText->pNextText;
	}

/////////////////////////////////////////////////////////////////////////////
// Put data in buffer
	if ((pBuffer = malloc(bufSize)) != NULL)
	{
		char *pByte = (char*)pBuffer;

		memcpy(pByte, &version,  sizeof(version));  pByte += sizeof(version);
		memcpy(pByte, &scale,    sizeof(scale));    pByte += sizeof(scale);
		memcpy(pByte, &rotation, sizeof(rotation)); pByte += sizeof(rotation);
		memcpy(pByte, &numPolys, sizeof(numPolys)); pByte += sizeof(numPolys);
		memcpy(pByte, &numTexts, sizeof(numTexts)); pByte += sizeof(numTexts);

		pCurrPoly = pPolys;
		while (pCurrPoly != NULL)
		{
			memcpy(pByte, &(pCurrPoly->colour), sizeof(pCurrPoly->colour)); 
			pByte += sizeof(pCurrPoly->colour);

			memcpy(pByte, &(pCurrPoly->properties), sizeof(pCurrPoly->properties)); 
			pByte += sizeof(pCurrPoly->properties);

			memcpy(pByte, &(pCurrPoly->numVertices), sizeof(pCurrPoly->numVertices)); 
			pByte += sizeof(pCurrPoly->numVertices);


			IdeoPolyVertex *pCurrVertex = pCurrPoly->pVertices;
			while (pCurrVertex != NULL)
			{
				memcpy(pByte, &(pCurrVertex->x), sizeof(pCurrVertex->x)); 
				pByte += sizeof(pCurrVertex->x);				
				memcpy(pByte, &(pCurrVertex->y), sizeof(pCurrVertex->y)); 
				pByte += sizeof(pCurrVertex->y);				

				pCurrVertex = pCurrVertex->pNextVertex;
			}

			pCurrPoly = pCurrPoly->pNextPoly;
		}


		IdeoPolyText *pCurrText = pTexts;
		while (pCurrText != NULL)
		{
			memcpy(pByte, &(pCurrText->x1), sizeof(pCurrText->x1)); 
			pByte += sizeof(pCurrText->x1);
			memcpy(pByte, &(pCurrText->y1), sizeof(pCurrText->y1)); 
			pByte += sizeof(pCurrText->y1);
			memcpy(pByte, &(pCurrText->x2), sizeof(pCurrText->x2)); 
			pByte += sizeof(pCurrText->x2);
			memcpy(pByte, &(pCurrText->y2), sizeof(pCurrText->y2)); 
			pByte += sizeof(pCurrText->y2);

			int textLength = strlen(pCurrText->text);
			memcpy(pByte, &textLength, sizeof(textLength)); 
			pByte += sizeof(textLength);

			memcpy(pByte, pCurrText->text, textLength); 
			pByte += textLength;

			pCurrText = pCurrText->pNextText;
		}


		assert(pByte == (char*)pBuffer + bufSize);

		*pBufSize = bufSize;
	}

	return pBuffer;
}


IdeoPolySet::IdeoPolySet(void *pBuffer)
{
	numPolys = 0;
	pPolys   = NULL;
	scale    = 1.0;//initialScale;
	rotation = 0.0;
	numTexts = 0;
	pTexts   = NULL;

	char *pByte = (char*)pBuffer;

	int version;
	memcpy(&version,  pByte, sizeof(version));  pByte += sizeof(version);

assert(version == BUFFER_VERSION);

	memcpy(&scale,    pByte, sizeof(scale));    pByte += sizeof(scale);
	memcpy(&rotation, pByte, sizeof(rotation)); pByte += sizeof(rotation);
	// Don't read number of polys stored in buffer into numPolys, as this is
	// incremented by AddPoly().
	int polyCount;
	assert(sizeof(polyCount) == sizeof(numPolys));
	memcpy(&polyCount, pByte, sizeof(polyCount)); pByte += sizeof(polyCount);
	// Don't read number of texts stored in buffer into numTexts, as this is
	// incremented by AddText().
	int textCount;
	assert(sizeof(textCount) == sizeof(numTexts));
	memcpy(&textCount, pByte, sizeof(textCount)); pByte += sizeof(textCount);

	while (polyCount-- > 0)
	{
		IdeoPoly *pCurrPoly = AddPoly();

		memcpy(&(pCurrPoly->colour), pByte, sizeof(pCurrPoly->colour)); 
		pByte += sizeof(pCurrPoly->colour);

		memcpy(&(pCurrPoly->properties), pByte, sizeof(pCurrPoly->properties)); 
		pByte += sizeof(pCurrPoly->properties);

		// Don't read number of vertices stored in buffer into numVertices, 
		// as this is incremented by AddVertex().
		int vertexCount;
		assert(sizeof(vertexCount) == sizeof(pCurrPoly->numVertices));
		memcpy(&vertexCount, pByte, sizeof(vertexCount)); 
		pByte += sizeof(vertexCount);


		while (vertexCount-- > 0)
		{
			IdeoPolyVertex *pCurrVertex = pCurrPoly->AddVertex(0,0);

			memcpy(&(pCurrVertex->x), pByte, sizeof(pCurrVertex->x)); 
			pByte += sizeof(pCurrVertex->x);
			memcpy(&(pCurrVertex->y), pByte, sizeof(pCurrVertex->y)); 
			pByte += sizeof(pCurrVertex->y);	
		}
	}

	while (textCount-- > 0)
	{
		IdeoPolyText *pCurrText = AddText(0,0,0,0,"");

		memcpy(&(pCurrText->x1), pByte, sizeof(pCurrText->x1)); 
		pByte += sizeof(pCurrText->x1);
		memcpy(&(pCurrText->y1), pByte, sizeof(pCurrText->y1)); 
		pByte += sizeof(pCurrText->y1);
		memcpy(&(pCurrText->x2), pByte, sizeof(pCurrText->x2)); 
		pByte += sizeof(pCurrText->x2);
		memcpy(&(pCurrText->y2), pByte, sizeof(pCurrText->y2)); 
		pByte += sizeof(pCurrText->y2);

		int textLength;
		memcpy(&textLength, pByte, sizeof(textLength)); 
		pByte += sizeof(textLength);

		memcpy(pCurrText->text, pByte, min(textLength, sizeof(pCurrText->text))); 
		pByte += textLength;
	}

	//assert(pByte == (char*)pBuffer + bufSize);

// Verify this function and CreateBuffer() by using CreateBuffer() to pack the
// data just extracted into another buffer, then checking it exactly matches
// the first buffer.
// TODO: make sure this is commented out when testing is complete.
///*
int bufSize;
void *pBuf2 = CreateBuffer(&bufSize);
assert(pByte - (char*)pBuffer == bufSize);
assert(memcmp(pBuffer, pBuf2, bufSize) == 0);
free(pBuf2);
//*/
}





IdeoPolyText*
IdeoPolySet::GetText(int index)
{ 
	IdeoPolyText *pCurrText = pTexts;

	while (index-- > 0 && pCurrText != NULL)
	{
		pCurrText = pCurrText->pNextText;
	}

	return pCurrText;
}


IdeoPolyText*
IdeoPolySet::AddText(float x, float y, float w, float h, char s[])
{
	IdeoPolyText *pNewText = new IdeoPolyText(x, y, w, h, s);

	if (pNewText)
	{
		// Insert at tail of list, so that the list preserves the order in
		// which the texts were added.
		pNewText->pNextText = NULL;
		if (pTexts != NULL) 
		{
			// Find last element in list...
			IdeoPolyText *pCurrText = pTexts;
			while (pCurrText->pNextText != NULL)
			{
				pCurrText = pCurrText->pNextText;
			}

			// ...add new element after it.
			pCurrText->pNextText = pNewText;
		}
		else // List is empty
		{
			pTexts = pNewText;			
		}

		numTexts++;
	}

	return pNewText;
}




IdeoPoly*
IdeoPolySet::GetPoly(int index)
{ 
	IdeoPoly *pCurrPoly = pPolys;

	while (index-- > 0 && pCurrPoly != NULL)
	{
		pCurrPoly = pCurrPoly->pNextPoly;
	}

	return pCurrPoly;
}


IdeoPoly*
IdeoPolySet::AddPoly()
{
	IdeoPoly *pNewPoly = new IdeoPoly;

	if (pNewPoly)
	{
/*
		// Insert at head of list
		pNewPoly->pNextPoly = pPolys;
		pPolys = pNewPoly;
*/
		// Insert at tail of list, so that the list preserves the order in
		// which the polys were added.
		pNewPoly->pNextPoly = NULL;
		if (pPolys != NULL) 
		{
			// Find last element in list...
			IdeoPoly *pCurrPoly = pPolys;
			while (pCurrPoly->pNextPoly != NULL)
			{
				pCurrPoly = pCurrPoly->pNextPoly;
			}

			// ...add new poly after it.
			pCurrPoly->pNextPoly = pNewPoly;
		}
		else // List is empty
		{
			pPolys = pNewPoly;			
		}

		numPolys++;
	}

	return pNewPoly;
}





static 
void CalcPolyArcOffsets(int segmentNumber, int numSegments, 
                        float arcRadius, float arcAngle, float arcHeight, 
                        float *pX, float *pY)
{
// Calculate the x and y-offsets, relative to the top or bottom extent of the
// band (depending on whether the curved edge is at the bottom or top of the
// band), of the vertex at the end of the line segment 'segmentNumber' of a
// polygonal approximation of 'numSegments' line segments to a circular arc.
	float a = arcAngle / 2.0f;
	float b = ((float)segmentNumber * a)/((float)numSegments);
	float c = 2.0f * arcRadius * (float)sin(b);

	*pX = c * (float)cos(a - b);
	*pY = arcHeight - (c * (float)sin(a - b));
}



void 
IdeoPolySet::AddPolysFromBand(Band *pBand, Band *pPrevBand, 
                              float top, float bottom, float width)
{
// 3May2000: left should always be zero (as should top). 
// Offsets are added only by code that renders.
	float xMid  = width / 2.0f; // x-offset of central axis of ideogram
	float left  = xMid - width/2.0f; // x-offset of left-hand edge
	float right = xMid + width/2.0f; // x-offset of right-hand edge

//	float half = (top + bottom)/2.0;
/*
	float curveRadius = width/7.0;
	// Curve must not extend to adjacent band
	if (curveRadius > (bottom - top)) curveRadius = bottom - top;
*/
/*
float rOneMinusCos225 = 0.08*curveRadius; // r * (1 - cos(22.5))
float rOneMinusSin225 = 0.62*curveRadius; // r * (1 - sin(22.5))
float rOneMinusCos450 = 0.29*curveRadius; // r * (1 - cos(45.0))
				pCurrPoly->AddVertex(left + rOneMinusCos225, top + rOneMinusSin225);
				pCurrPoly->AddVertex(left + rOneMinusCos450, top + rOneMinusCos450);
				pCurrPoly->AddVertex(left + rOneMinusSin225, top + rOneMinusCos225);
*/
/*
	float rOneMinusCos30 = 0.13*curveRadius; // r * (1 - cos(30))
	float rOneMinusSin30 = 0.50*curveRadius; // r * (1 - sin(30))
				// 'Curved' top edge, made from three lines fitting to
				// a circular arc (vertices at 30 degree angles).
				pCurrPoly->AddVertex(left,                  top + curveRadius);
				pCurrPoly->AddVertex(left + rOneMinusCos30, top + rOneMinusSin30);
				pCurrPoly->AddVertex(left + rOneMinusSin30, top + rOneMinusCos30);
				pCurrPoly->AddVertex(left + curveRadius,    top);

				pCurrPoly->AddVertex(right - curveRadius,    top);
				pCurrPoly->AddVertex(right - rOneMinusSin30, top + rOneMinusCos30);
				pCurrPoly->AddVertex(right - rOneMinusCos30, top + rOneMinusSin30);
				pCurrPoly->AddVertex(right,                  top + curveRadius);
/*
				// 'Curved' bottom edge, similar to 'curved' top edge.
				pCurrPoly->AddVertex(right,                  bottom - curveRadius);
				pCurrPoly->AddVertex(right - rOneMinusCos30, bottom - rOneMinusSin30);
				pCurrPoly->AddVertex(right - rOneMinusSin30, bottom - rOneMinusCos30);
				pCurrPoly->AddVertex(right - curveRadius,    bottom);

				pCurrPoly->AddVertex(left + curveRadius,    bottom);
				pCurrPoly->AddVertex(left + rOneMinusSin30, bottom - rOneMinusCos30);
				pCurrPoly->AddVertex(left + rOneMinusCos30, bottom - rOneMinusSin30);
				pCurrPoly->AddVertex(left,                  bottom - curveRadius);
*/


// Parameters for arcs. Arcs are circular arcs, symmetrical about the vertical
// axis of the ideogram (i.e. the centre of the arc lies on the axis), which do
// not pass outside the horizontal extents of the ideogram.
// The arcHeight is the vertical extent of the arc.
	float bandHeight = bottom - top;
	float arcRadius, arcHeight;

	if (pBand->type != Band::BTcentHet)
	{
	// An arc with a small and constant arc angle, whatever the band width.
		arcRadius = width * 1.5f;
		arcHeight = arcRadius - (float)sqrt(arcRadius*arcRadius - (width*width/4.0f));
	}
	else
	{
	// An arc with as large an arc angle as possible, limited by the band height,
	// unless this band is at one end of the ideogram, in which case don't allow
	// the arc to extend beyond half the band height, as both ends of the band
	// are rounded.
		if (pPrevBand != NULL && pBand->pNextBand != NULL)
		{
			if (bandHeight < width/2.0f) arcHeight = bandHeight;
			else                         arcHeight = width/2.0f;
		}
		else
		{
			if (bandHeight/2.0f < width/2.0f) arcHeight = bandHeight/2.0f;
			else                              arcHeight = width/2.0f;
		}		

		arcRadius = (width*width + 4.0f*(arcHeight*arcHeight))/(8.0f*arcHeight);
	}


	float arcAngle  = 2.0f * (float)asin(width / (2.0f * arcRadius));

	// Number of line segments making up the arc.
	int numSegments = ((int)arcAngle)*2 + 5; // Should be 5, 7 or 9


	switch (pBand->type)
	{
		// Draw the sides of the band.
		case Band::BTband:       // Ordinary Band
		case Band::BTnonCentHet: // Non-centromeric heterochromatin band (colour not valid) 
		case Band::BTcentHet:    // Centromeric band
		case Band::BTsatellite:  // Satellite band
		{
			// If band is very small, or zero length, don't create an IdeoPoly
			// for it at all. 
			// The ideogram editor forces all centromeres to have a centromeric
			// heterocromatin band on each side. However, there are some
			// instances when we don't want this, for example telocentric
			// centromeres, or centromeres without a hetrochromatin band on one
			// or both sides. In these instances, if the user sets one or both
			// of the heterochromatin bands to zero length, then they won't be
			// drawn. If we didn't put this check in, there would always be at
			// least a horizontal line across the full width of the ideogram
			// where the zero length band was, which would not look right for
			// telocentric centromeres.
			// Note that the selection rectangle will still be drawn for such
			// bands if they are selected.
			if (bottom - top < 0.000001f) break;


			IdeoPoly *pCurrPoly = AddPoly();


			pCurrPoly->SetColour(pBand->colour[0], pBand->colour[1], pBand->colour[2]);


			switch (pBand->type)
			{
				case Band::BTsatellite:
				case Band::BTnonCentHet:
					pCurrPoly->properties |= IDEOPOLY_LRHATCH; // Left-to-right (from top) angled hatch
					break;
				case Band::BTcentHet:
					pCurrPoly->properties |= IDEOPOLY_RLHATCH; // Right-to-left (from top) angled hatch
					break;
			}


			//pCurrPoly->AddVertex(left, half);

			if (   pPrevBand == NULL    // Top edge of ideogram
				|| pPrevBand->type == Band::BTcentromere // Bottom part of centromere
			    || pPrevBand->type == Band::BTstalk  // Adjacent to Stalk
			    /*|| pPrevBand->type == 4*/) // Adjacent to Stalk
			{ 

				int i;
				float x, y;
				for (i=0; i <= numSegments; i++)
				{
					CalcPolyArcOffsets(i, numSegments, arcRadius, arcAngle, arcHeight, &x, &y);
					//pCurrPoly->AddVertex(left + x, top + y);
					// Make sure arc does not go below bottom extent of band.
					if (top + y < bottom) pCurrPoly->AddVertex(left + x, top + y);
					else                  pCurrPoly->AddVertex(left + x, bottom);
				}
			}
			else
			{ 
				// Default: edge between two bands: full width line.
				pCurrPoly->AddVertex(left, top);
				pCurrPoly->AddVertex(right, top);
			}

			//pCurrPoly->AddVertex(right, half);

			if (   pBand->pNextBand == NULL    // Bottom edge of ideogram
				|| pBand->pNextBand->type == Band::BTcentromere // Bottom part of centromere
			    || pBand->pNextBand->type == Band::BTstalk  // Adjacent to Stalk
			    /*|| pBand->pNextBand->type == 4*/) // Adjacent to Stalk
			{ 
				int i;
				float x, y;
				for (i=0; i <= numSegments; i++)
				{
					CalcPolyArcOffsets(i, numSegments, arcRadius, arcAngle, arcHeight, &x, &y);
					//pCurrPoly->AddVertex(right - x, bottom - y);
					// Make sure arc does not go above top extent of band.
					if (bottom - y > top) pCurrPoly->AddVertex(right - x, bottom - y);
					else                  pCurrPoly->AddVertex(right - x, top);
				}
			}
			else
			{ 
				pCurrPoly->AddVertex(right, bottom);
				pCurrPoly->AddVertex(left, bottom);
			}

			//pCurrPoly->AddVertex(left, half);
		}
		break;

		case Band::BTstalk:	// Stalk (Type 1)		
		{
			float third      = top + (bottom - top)/3.0f;
			float twoThirds  = top + ((bottom - top) * 2.0f)/3.0f;
			float stalkLeft  = left + (right - left)/4.0f;
			float stalkRight = right - (right - left)/4.0f;

			IdeoPoly *pCurrPoly = AddPoly();
			pCurrPoly->SetColour(pBand->colour[0], pBand->colour[1], pBand->colour[2]);

			pCurrPoly->AddVertex(stalkLeft,  third);
			pCurrPoly->AddVertex(stalkRight, third);

			pCurrPoly = AddPoly();
			pCurrPoly->SetColour(pBand->colour[0], pBand->colour[1], pBand->colour[2]);

			pCurrPoly->AddVertex(stalkLeft,  twoThirds);
			pCurrPoly->AddVertex(stalkRight, twoThirds);
		}
		break;

	}

	if (pBand->selected)
	{
		// Create an additional poly (following the other poly(s) for the band
		// in the list, so that it is drawn on top) as a selection rectangle
		// for this band.
		IdeoPoly *pCurrPoly = AddPoly();

		pCurrPoly->properties |= IDEOPOLY_SELECT;

		pCurrPoly->AddVertex(right, bottom);
		pCurrPoly->AddVertex(left, bottom);
		pCurrPoly->AddVertex(left, top);
		pCurrPoly->AddVertex(right, top);
	}




//	pDC->TextOut(xMax + 10, top, pBand->name, strlen(pBand->name));
	if (pBand->showName && strlen(pBand->name) > 0)
	{
		// Coordinate pairs are for a line perpendicular to the ideogram,
		// leading from the centre of the edge of the band to the position
		// of the text. 
		AddText(left, (top + bottom)/2, left - width/3, (top + bottom)/2, pBand->name);

		// text on left (after 180 degree rotation) AddText(right, (top + bottom)/2, right + width/3, (top + bottom)/2, pBand->name);
		
		//AddText(right + width/2, (top + bottom)/2, width /2, (bottom - top)/2, pBand->name);
	}
}






// 'draw' a PolyIdeogram from an Ideogram




IdeoPolySet::IdeoPolySet(Ideogram *pIdeo, float width, float initialScale)
{
	numPolys = 0;
	pPolys   = NULL;
	scale    = initialScale;
	rotation = 0.0;
	numTexts = 0;
	pTexts   = NULL;

	if (pIdeo)
	{
		// Iterate through linked list of bands.

		//Ideogram *pIdeo = pSet->pIdeogramList;
		//for (int i = 1; i < 1/*Ideo number*/; i++) pIdeo = pIdeo->pNextIdeogram;


		Band *pCurrBand = pIdeo->pBandList;

		Band *pPrevBand = NULL;


		float top = 0.0;

		while (pCurrBand != NULL)
		{
			float bottom = top + pCurrBand->length;

			AddPolysFromBand(pCurrBand, pPrevBand, top, bottom, width);

			pPrevBand = pCurrBand;

			pCurrBand = pCurrBand->pNextBand;

			top = bottom;
		}


	}


}

IdeoPolySet::~IdeoPolySet()
{
	while (pPolys != NULL)
	{
		IdeoPoly *pCurrPoly = pPolys;
		pPolys = pPolys->pNextPoly;
		delete pCurrPoly;
	}

	numPolys = 0;

	while (pTexts != NULL)
	{
		IdeoPolyText *pCurrText = pTexts;
		pTexts = pTexts->pNextText;
		delete pCurrText;
	}

	numTexts = 0;
}






/*
Test code from canvas.cpp:

case Cideogram:
	getcurdg()->cur_colour = 0; /*color(0);* // Set ddgs colour
	moveto(0,0);
	lineto(800,800);
break;
*/
