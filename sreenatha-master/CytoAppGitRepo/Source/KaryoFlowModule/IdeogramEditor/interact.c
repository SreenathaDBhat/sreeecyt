/*
 * interact.c	18/8/92	Mark Gregson
 *
 * construct woolz objects interactively with the mouse
 *
 *	Modifications:
 *
 *	10Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *					Also, made minor mods to stop compiler warnings and removed
 *					standard C parameter lists (were '#ifdef i386' only).
 *	12Jan98	WH:	Set mousepts to NULL after free in mousepoint().
 *	20Dec96	BP:	Set mousepts to NULL to avoid repeated free().
 *	9/19/96	BP:	Changed POINT to DDGSPOINT and same for CIRCLE,
 *				POLYLINE etc.
 *
 *	27/3/93 MG	Fixed small shift problem in mousepoly
 *	 1/2/93 MG	Added single point capability to mousepoly
 *	11/1/93	MG	Fixed dx,dy bug in mousepoly
 */

#include "woolz.h"

#include "ddgs.h"

#include <stdio.h>
#include <math.h>
//#include <wstruct.h>
//#include "ddgs.h"




/*
 *	M A K E P O L Y C I R C L E
 *
 *	Constructs a circular polygon object from a given centre point and radius
 */
#ifdef WIN32
struct object *
makepolycircle(struct ivertex centre, int radius)
#endif
#ifdef i386
struct object *
makepolycircle(centre, radius)
struct ivertex centre;
int radius;
#endif
{
	struct object *circpoly, *makemain();
	register struct polygondomain *circpdom;
	struct polygondomain *makepolydmn();
	struct ivertex *tempvtx;
	int /*temp,*/i, x, y, nvtx;
	int error, err2, stepx, stepy, diffx, diffy;
	int quad, q0, q1, q2, q3;


	/* create temp vertex list and allow for more vertices than
           a circle will require i.e 8*radius */

	tempvtx=(struct ivertex *)Malloc(sizeof(struct ivertex) * 8*radius);

	/* calculate the vertices for the first quadrant of the circle */

	x=centre.vtX;
	y=centre.vtY-radius;
    	tempvtx[0].vtX=x;
     	tempvtx[0].vtY=y;
	nvtx=1;

	error=0;
	diffx=1;
	diffy=1- (2 * radius);
 
	do {
		stepx=1;
		stepy=0;
		error = error + diffx;
		err2 = error + diffy;

		if ( abs(err2) < abs (error) )
		{
			stepy = 1;
			error = err2;
		}

		err2 = err2 - diffx;

		if ( abs(err2) < abs (error) )
		{
			stepx = 0;
			stepy = 1;
			error = err2;
		}

		diffx = diffx+ 2*stepx;
		diffy = diffy+ 2*stepy;
		
		y = y + stepy;
		x = x - stepx;
				
    		tempvtx[nvtx].vtX=x;
     		tempvtx[nvtx].vtY=y;

		nvtx++;
		if (nvtx>2*radius) {
			fprintf(stderr,"makecircle allocation fault\n");
			exit(1);
		}
	
	} while (y < centre.vtY);

	quad=nvtx-1;

	
	/* calculate true number of polygon vertices, generate new circpoly
	   object and copy vertices from tempvtx */	


	/* generate the vertices for the other 3 quadrants */
	q0=1;
	q1=q0+quad;
	q2=q1+quad;
	q3=q2+quad;

	diffx=diffy=0;

	for (i=0; i<quad; i++) {
		diffx+=tempvtx[q0].vtX-tempvtx[i].vtX;
		diffy+=tempvtx[q0].vtY-tempvtx[i].vtY;
		q0++;
		tempvtx[q1].vtX=centre.vtX-radius+diffy;
		tempvtx[q1].vtY=centre.vtY-diffx;
		q1++;
		tempvtx[q2].vtX=centre.vtX-diffx;
		tempvtx[q2].vtY=centre.vtY+radius-diffy;
		q2++;
		tempvtx[q3].vtX=centre.vtX+radius-diffy;
		tempvtx[q3].vtY=centre.vtY+diffx;
		q3++;
	}
	
	circpdom = makepolydmn(1, (short *)tempvtx, quad*4+1, quad*4+1, 1);
	circpoly = makemain(10, (struct intervaldomain *)circpdom, NULL, NULL, NULL);

	circpdom->nvertices = quad*4+1;

	Free(tempvtx);
	return(circpoly);	

}   




/*
 *	M O U S E P O L Y 
 *
 *	Creates a polygon object from the shape drawn by the user
 *
 *	shape can be :
 *			DDGSPOINT
 *		 	DDGSLINE - freehand line 
 *			DDGSPOLYLINE - polygon vertices
 *			DDGSRECTANGLE
 *			DDGSCIRCLE
 *
 *	All drawing takes place in the overlay plane of the window held
 *      in the DDGS structure dg.
 *
 *	pframe f ensures that polygon coordinates can be matched
 *	to object coordinates in the image
 *
 *	closed is a boolean used with DDGSLINE and DDGSPOLYLINE
 *	if non-zero the line is closed back to the first point on the line.
 *
 */

#ifdef WIN32
struct object *
mousepoly(struct pframe *f, DDGS *dg, int shape, int closed)
#endif
#ifdef i386
struct object *
mousepoly(f, dg, shape, closed)
struct pframe *f;
DDGS *dg;
int shape;
int closed;
#endif
{
	struct object *poly, *makemain();
	register struct polygondomain *pdom;
	struct polygondomain *makepolydmn();
	int i,xs,ys,xo,yo,radius,npts;
	struct ivertex centre,edge;
	double dx,dy;

	/* protect against illegal shapes */
	if ((shape<DDGSPOINT) || (shape>DDGSCIRCLE))
		shape=DDGSPOINT;

	mousedraw(shape, dg);	/* draw polygon with mouse - ddgs routine */

	xs=f->ix * f->scale;
	ys=f->iy * f->scale;
	xo=f->dx - f->ox * xs;
	yo=f->dy - f->oy * ys;

	switch (shape) {
		
		case DDGSLINE:
		case DDGSPOLYLINE:
			npts=dg->nmousepts;
			if (closed)
				npts++;
			break;
		
		case DDGSRECTANGLE:
			npts=5;
			break;

		case DDGSCIRCLE:
			npts=2; /* initially */  /* always! */
			break;
		default:
		case DDGSPOINT:
			npts=1;
			break;
	}

	/* create woolz polygon object */
	pdom = makepolydmn(1, NULL, npts, npts, 1);
	poly = makemain(10, (struct intervaldomain *)pdom, NULL, NULL, NULL);

	/* convert frame coordinates to object coordinates */
	for (i=0; i<dg->nmousepts; i++) {

		pdom->vtx[i].vtX=((dg->mousepts[i].x) * 8 - xo) / xs;
		pdom->vtx[i].vtY=((dg->maxy-dg->mousepts[i].y) * 8 - yo) / ys;
	}

	free(dg->mousepts);
	/* Don't allow re-free! */
	dg->mousepts = NULL;

	switch (shape) {
		
		case DDGSLINE:
		case DDGSPOLYLINE:
			if (closed) {	/* make new end point = first point */
				pdom->vtx[pdom->nvertices-1]=pdom->vtx[0];
			}
			break;

		case DDGSRECTANGLE:

			/* mousedraw returns rectangle opposite corner points in vtx[0] and vtx[1]
			   however the y positions are swapped */

			pdom->vtx[3]=pdom->vtx[1];         /* 1-------2 */
			pdom->vtx[1]=pdom->vtx[0];         /* |       | */
			pdom->vtx[2].vtX=pdom->vtx[3].vtX; /* |       | */
			pdom->vtx[2].vtY=pdom->vtx[1].vtY; /* |       | */
			pdom->vtx[0].vtY=pdom->vtx[3].vtY; /* 0-------3 */
			pdom->vtx[4]=pdom->vtx[0];         /* 4         */
			break;
			
		case DDGSCIRCLE: 
			centre=pdom->vtx[0];
			edge=pdom->vtx[1];
			dx=(double)(edge.vtX-centre.vtX);
			dy=(double)(edge.vtY-centre.vtY);
			radius=(int) sqrt( dx*dx + dy*dy);

			freeobj(poly);

			/* make circle object - type 100 - not polyobj */
			poly=(struct object *)makecircle(centre.vtX, centre.vtY, radius);

			break;

		default:
			break;
	}

	return(poly);
}


/*
 *	MOUSEPOINT
 *
 *	All drawing takes place in the overlay plane of the window held
 *      in the DDGS structure dg.
 *
 *	pframe f ensures that point coordinates can be matched
 *	to object coordinates in the image
 *
 *	returns woolz ipoint object - coordinates in object space
 */
#ifdef WIN32
struct ipoint
mousepoint(struct pframe *f, DDGS *dg)
#endif
#ifdef i386
struct ipoint
mousepoint(f, dg)
struct pframe *f;
DDGS *dg;
#endif
{
	int /*i,*/xs, ys, xo, yo;
	struct ipoint pt;

	mousedraw(DDGSPOINT, dg);	/* draw point with mouse - ddgs routine */

	xs=f->ix * f->scale;
	ys=f->iy * f->scale;
	xo=f->dx - f->ox * xs + abs(xs)/2;
	yo=f->dy - f->oy * ys + abs(ys)/2;

	/* set up woolz integer point object */
	pt.type=40;
	pt.style=1;

	/* convert frame coordinates to object coordinates */
	pt.k=((dg->mousepts[0].x + 1) * 8 - xo) / xs;
	pt.l=((dg->maxy-dg->mousepts[0].y + 1) * 8 - yo) / ys;
	
	free(dg->mousepts);
	dg->mousepts = NULL;

	return(pt);
}


