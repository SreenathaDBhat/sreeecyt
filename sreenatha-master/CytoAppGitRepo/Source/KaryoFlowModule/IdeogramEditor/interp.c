/*
 * interp.c
 *
 * linear interpolation between 4 points
 *
 * Modifications:
 *
 *	9/20/96		BP:		Replace register with register int!!!!!
 *
 */

#include "woolz.h"

#include <stdio.h>
//#include <wstruct.h>

/*
	MG	25/6/92		made from interp.proto , cannot use 
				68000 assembler interp.a
*/
#ifdef WIN32
GREY
interp(register GREY *g4, register int fractline, register int fractkol)
#endif
#ifdef i386
GREY
interp(g4, fractline, fractkol)
register GREY *g4;
register int fractline;
register int fractkol;
#endif
{
	register int mfractline, mfractkol;
	GREY g;
	mfractline = 256 - fractline;
	mfractkol = 256 - fractkol;
	g = (g4[0] * mfractline*mfractkol
	   + g4[1] * mfractline*fractkol
	   + g4[2] * fractline*mfractkol
	   + g4[3] * fractline*fractkol
	   + 32767) / 65536;
	return(g);
}
