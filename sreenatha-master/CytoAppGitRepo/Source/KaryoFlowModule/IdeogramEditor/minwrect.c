/*
 * minwrect.c	Jim Piper	January 1984
 *
 * Construct minimum width rectangle from convex hull
 * of type 1 object.
 *
 * Modifications
 *
 *	10 Mar 2000		JMB		Changed headers included, as part of conversion to DLL.
 *							Also, made minor mods to stop compiler warnings and removed
 *							standard C parameter lists (were '#ifdef i386' only).
 *	01Dec98		RH_31	Bugfix: Rotation angle near -98 degree were by a bug made positive. 
 *	9/20/96		BP:		Return type of intersect() is void.
 *	13 Jul 1992		MG		corrected intersect() div by 0 test (dork!)
 *	18 Nov 1988		dcb		woolz_check_obj() instead of wzcheckobj()
 *	25 Nov 1987		BDP		protection against single point objects
 *	08 May 1987		BDP		protection against null or empty objs
 *	13 Sep 1986		CAS		Includes
 */

#include "woolz.h"

#include <stdio.h>
#include <math.h>
#include <malloc.h> // For malloc, calloc, free
//#include <wstruct.h>

/*
 * extract minimum width rectangle from convex hull
 * (it is relatively obvious that minimum width rectangle long
 * side must be parallel to a chord of convex hull, and all
 * sides must have at least one vertex of convex hull lying
 * within them).
 * Protection just checks object for existence and returns Null object
 * if any probelm found.  bdp 8/5/87 
 */
struct object *
minwrect(struct object *cvh)
{
	struct object *makemain(), *robj, *makemain();
	register struct polygondomain *pdom;
	struct polygondomain *makepolydmn();
	register struct frect *rdom;
	struct chord *ch;
	register struct cvhdom *cdom;
	struct chord chl[4], chperp;
	register int i;
	int gap();
	int s,c,np[4];
	

	if (woolz_check_obj(cvh, "minwrect") != 0)
		return(NULL);
	pdom = (struct polygondomain *)cvh->idom;
	cdom = (struct cvhdom *)cvh->vdom;
	if ( (pdom == NULL) || (cdom == NULL))
		return(NULL);
	if ( cdom->nchords < 1 )
		return(NULL);
	rdom = (struct frect *) Malloc(sizeof(struct frect));
	rdom->type = 2;		/* float */
	robj = makemain(20, (struct intervaldomain *)rdom, NULL, NULL, cvh->assoc);
	
	mwrangle(cvh,&ch,np,np+2,&s,&c);

									/* RH_31 */
	/* Below line is replaced with the corresponding
	   source used in the function vertical (chromlib/vertical.c).
	   The problem in the line is that angles close to -89 degrees 
	   are made positive
	rdom->rangle = atan(c!=0? (double)s/(double)c: 10000.0);
	*/
	if (!c) {
		c = 1;
		s*= 1000;
	}
	rdom->rangle = (float)(atan((double)s/(double)c));

	/*
	 * construct two "pseudo-chords" parallel to min-width
	 * rectangle at opposite sides of hull.
	 */
	for (i=0; i<=2; i+=2) {
		chl[i].acon = ch->acon;
		chl[i].bcon = ch->bcon;
		chl[i].ccon = (pdom->vtx[np[i]].vtX - cdom->mdkol)*ch->acon -
				(pdom->vtx[np[i]].vtY - cdom->mdlin)*ch->bcon;
		chl[i].cl = ch->cl;
	}
	/*
	 * construct a "pseudo-chord" perpendicular to min-width
	 * rectangle, find opposites sides, construct two more "pseudo-chords"
	 */
	chperp.acon = -ch->bcon;
	chperp.bcon = ch->acon;
	chperp.cl = ch->cl;
	gap(&chperp,pdom,np+1,np+3);
	for (i=1; i<=3; i+=2) {
		chl[i].acon = chperp.acon;
		chl[i].bcon = chperp.bcon;
		chl[i].ccon = (pdom->vtx[np[i]].vtX - cdom->mdkol)*chperp.acon -
				(pdom->vtx[np[i]].vtY - cdom->mdlin)*chperp.bcon;
		chl[i].cl = chperp.cl;
	}
	/*
	 * compute the intersections of consecutive pairs of these
	 *  pseudo-chords, giving the rectangle vertices.
	 */
	for (i=0; i<=3; i++) {
		intersect(&chl[i],&chl[i==3? 0: i+1],rdom->frk+i,rdom->frl+i);
		rdom->frk[i] = rdom->frk[i] + cdom->mdkol;
		rdom->frl[i] = rdom->frl[i] + cdom->mdlin;
	}

	return(robj);
}


/*
 * compute intersection of two chords (which will be
 * returned in object-central coordinates)
 */
void
intersect(register struct chord *ch1, register struct chord *ch2, register float *x, register float *y)
{
	double div;
	
	*y = *x = 0;
	div = (ch2->acon*ch1->bcon - ch2->bcon*ch1->acon);
	if (div != 0){
		*y = (float)(((double)(ch1->acon*ch2->ccon - ch2->acon*ch1->ccon))/div);
		*x = (float)(((double)(ch1->bcon*ch2->ccon - ch2->bcon*ch1->ccon))/div);
	}
}
