/*
 * picframe.c	Jim Piper	10 August 1983
 *
 * Draw an object in DIGS.
 *
 * Mods:
 *
 *	KS	30Jun00		picrawim now handles 12bit RawIm structures, i.e. converts the
 *					12bit data to 8bits for display.
 *	WH 09May97:		BP has changed Fastpixobj, so do approp changes to Picrawim.
 *					Is this going to be okay on UNIX ?
 *	BP	19Dec96:	Proper cast all types - domain/obj and float/int - to
 *					eliminate compiler warnings.
 *	WH	17Dec96:	PicFrame returns 1 if NULL obj
 *	9/19/96		BP:		Change COORD to WZCOORD. Text functions do nothing.
 *						Add forward declarations.
 *						Fix bug - text_gc is a pointer!
 *
 *	BP	5/2/96:		Removed all refernces to grab buffer (already commented
 *					but was ugly).
 *	BP	7/18/95:	Use temp int values for Picpoly to avoid overflow.
 *	BP	6/15/95:	Two tweaks to Picpoly: now uses ddgs_aux_buffer for
 *					the array of points (no more malloc), and special
 *					hack for Univision to reduce broken lines.
 *	BP	5/10/95:	Another tweak to txtobj() which now has to make sure
 *					the static pixmap it uses is of the right depth -
 *					multiple depth dogs are now permissable for Univision
 *					printing.
 *					Also now use qbuffer() for regular object drawing.
 *					This is faster than picbuffer.
 *
 *	BP	12/10/94	Rehash of txtobj() to use fixed pixmap and don't call
 *					XGetWindowAttributes each time. Also removed call to
 *					threshold(). Result is faster drawing that is clearer
 *					when scaled down.
 *	BP	9/20/94		No more malloc/grab_buffer stuff. Picintobj now
 *					uses ddgs buffers.
 *	 6 Jan 1993		MG	picintobj now uses fill instead of line drawing
 *	 2 Sep 1992		MG 	modified to allow all routines to draw into
 *					either the grey, overlay or marker images
 *					as defined by ddgs. Generic routines begin
 *					with capital P - specific routines call these :
 *
 *					picpoly, opicpoly and mpicpoly all call Picpoly
 *
 *	11/6/92			mc	commented out call to arrowhead()
 *					will be removed eventully
 *
 *	 7 Feb 1991		CAS		voids
 *	02 Dec 1988		SCG		rewrite picpoly to be more stack efficient
 *  18 Nov 1988		dcb		woolz_check_obj() instead of wzcheckobj()
 *	20 Aug 1987	jimp@IRS	arrowhead() call for arrow cursor.
 *					CHECK that this mod matches the MRC
 *					version of 04-03-87.
 *	06 May 1987		BDP		protection against null or empty objs
 *	12 Sep 1986		CAS		Includes
 */

#include "ddgs.h"

#include "woolz.h"

#include <stdio.h>
//#include <wstruct.h>
//#include "ddgs.h"

#define ABS(i)	(i>=0? i: -i)

			/* where to draw graphics or text */
#define GREYIM 	0	/* grey image planes */
#define	OVLYIM	1	/* overlay plane */
#define MARKIM	2	/* marker image planes */

#ifdef PICDEBUG
extern FILE *pic_chan;
#endif



/* Forward declarations. */
static void Picintobj();
//static void oldPicintobj();
static void Picrawim();



old_style_picintobj = 0;		/* picintobj method - normally 0 */
int dont_use_clipmask = 0;

extern unsigned char *picbuff;		/* picbuffer.c */
extern unsigned char *picmask;		/* picbuffer.c */



/*
 * Protection added to this module assumes that picframe function is the
 * only public function in this module.  The only checking necessary is
 * for the object existing. If the object is null immediate return is
 * effected. All routines should copy with an empty object, either by
 * their own for loops , or by protection added to  the interval scanning
 * routines.  bdp 6/5/87 
 */

/************************************************************************/

#ifdef WIN32
int
Picframe(struct object *obj, struct pframe *f, SMALL where)
#endif
#ifdef i386
int
Picframe(obj, f, where)
struct object *obj;
struct pframe *f;
SMALL where;
#endif
{
	/* Return fail on NULL obj */
	if (!obj)
		return(1);

	if (woolz_check_obj(obj, "picframe") != 0)
		return(1);		/* return a failed status to anyone interested */
		
	/* switch on object type */
	switch(obj->type) {
	case 0 :		/* interval/grey-table object */
	case 1 :
		Picintobj(obj, f, where);
		break;

	case 10 :	/* polygon */
	case 12 :	/* convexhull */
		Picpoly((WZPoly *)obj->idom, f, where);
		break;

	case 11 :	/* boundary list */
		Picbound((WZBound *)obj->idom, f, where);
		break;

	case 13:	/* histogram */
		Pichisto((WZHisto *)obj->idom, f, where);
		break;

	case 20 :	/* rectangle - vertices specified */
		Picrect((WZIrect *)obj->idom, f, where);
		break;

	case 30 :	/* vectors */
	case 31 :
		Picvec((WZIvector *)obj, f, where);
		break;

	case 40 :	/* point (cursor) */
	case 41 :
		Picpoint((WZIpoint *)obj, f, where);
		break;

	case 70 :	/* text object */
		Pictext((WZTextObj *)obj, f, where);
		break;

	case 90:
		Picrawim(obj,f,where);
		break;

	case 100:
		Piccircle((WZCircle *)obj, f, where);
		break;

	default:
		break;
	}
	return(0);	/* operation completed ok */
}


/* versions to work in grey, overlay or marker image planes */

#ifdef WIN32
void
picframe(struct object *obj, struct pframe *f)
#endif
#ifdef i386
void
picframe(obj, f)
struct object *obj;
struct pframe *f;
#endif
{
	Picframe (obj, f, GREYIM);
}

#ifdef WIN32
void
opicframe(struct object *obj, struct pframe *f)
#endif
#ifdef i386
void
opicframe(obj, f)
struct object *obj;
struct pframe *f;
#endif
{
	Picframe (obj, f, OVLYIM);
}

#ifdef WIN32
void
mpicframe(struct object *obj, struct pframe *f)
#endif
#ifdef i386
void
mpicframe(obj, f)
struct object *obj;
struct pframe *f;
#endif
{
	Picframe (obj, f, MARKIM);
}


#if 0


/**************************************************************************/

/* old style - slow - picintobj - has capability to draw at funny scales */

#ifdef WIN32
static void
oldPicintobj(struct object *obj, register struct pframe *f, SMALL where)
#endif
#ifdef i386
static void
oldPicintobj(obj, f, where)
struct object *obj;
register struct pframe *f;
SMALL where;
#endif
{
	struct iwspace iwsp;
	struct gwspace gwsp;
	register xo,yo,xs,ys;
	int intlength;

	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	xo = f->dx - xs*f->ox;
	yo = f->dy - ys*f->oy;
	if ((obj->vdom == NULL) || (where!=GREYIM)) {
		/* scanning routines will protect against empty objects */

		/* modify fill rectangle origin for -ve xs or ys */
		if (xs<0) xo=xo+xs;
		if (ys<0) yo=yo+ys;

		initrasterscan(obj,&iwsp,0);
		while (nextinterval(&iwsp) == 0) {
			intlength = xs*iwsp.colrmn;

			switch (where) {
			case OVLYIM:
				ofill(xo + xs*iwsp.lftpos, yo + ys*iwsp.linpos,
				      abs(intlength), abs(ys));
				break;
			case MARKIM: 
				mfill(xo + xs*iwsp.lftpos, yo + ys*iwsp.linpos,
				      abs(intlength), abs(ys));
				break;
			case GREYIM:
			default:			
				fill(xo + xs*iwsp.lftpos, yo + ys*iwsp.linpos,
				     abs(intlength), abs(ys));
				break;
			}
		}
	}
	else
	{
		initgreyscan(obj,&iwsp,&gwsp);
		while (nextgreyinterval(&iwsp) == 0) {
			moveto(xo + xs*iwsp.lftpos, yo + ys*iwsp.linpos);
			pixline(iwsp.colrmn,ABS(xs),ABS(ys),gwsp.grintptr,
				(f->ix>0? 1: -1));
		}
	}
}


#endif


/************************************************************************/

#ifdef WIN32
static void
Picintobj(struct object *obj, register struct pframe *f, SMALL where)
#endif
#ifdef i386
static void
Picintobj(obj, f, where)
struct object *obj;
register struct pframe *f;
SMALL where;
#endif
{
	int x, y, w, h;
	unsigned char *vdata;

	if ((vdata = qbuffer(obj, &x, &y, &w, &h, f, NULL)) != NULL)
		fastpixobj(x, y, w, h, vdata, NULL, where);


#if 0
	int x, y, w, h;
	char *srcdata, *vdata, *mdata;

	if (dont_use_clipmask)
	{
		if (srcdata = (char *)qbuffer(obj, &x, &y, &w, &h, f, NULL))
		{
			if ((vdata = (char *)ddgs_get_draw_buffer(w*h)) != NULL)
				fastpixobj(x, y, w, h, vdata, NULL, where, srcdata);
		}
	}
	else
	{
		if (srcdata = (char *)qbuffer(obj, &x, &y, &w, &h, f, &mdata))
		{
			if ((vdata = (char *)ddgs_get_draw_buffer(w*h)) != NULL)
				fastpixobj(x, y, w, h, vdata, mdata, where, srcdata);
		}
	}
#endif
}


/* versions to work in grey, overlay or marker image planes */

#ifdef WIN32
void
picintobj(struct object *obj, register struct pframe *f)
#endif
#ifdef i386
void
picintobj(obj, f)
struct object *obj;
register struct pframe *f;
#endif
{
//	if (old_style_picintobj) 
//		oldPicintobj(obj,f,GREYIM);
//	else
		Picintobj(obj,f,GREYIM);
}

#ifdef WIN32
void
opicintobj(struct object *obj, register struct pframe *f)
#endif
#ifdef i386
void
opicintobj(obj, f)
struct object *obj;
register struct pframe *f;
#endif
{
//	if (old_style_picintobj) 
//		oldPicintobj(obj,f,OVLYIM);
//	else
		Picintobj(obj,f,OVLYIM);
}


#ifdef WIN32
void
mpicintobj(struct object *obj, register struct pframe *f)
#endif
#ifdef i386
void
mpicintobj(obj, f)
struct object *obj;
register struct pframe *f;
#endif
{
	if (old_style_picintobj) 
		Picintobj(obj,f,MARKIM);
	else
		Picintobj(obj,f,MARKIM);
}


/**********************************************************************/

#ifdef WIN32
static void
Picrawim(RawIm *im, register struct pframe *f, SMALL where)
#endif
#ifdef i386
static void
Picrawim(im, f, where)
RawIm *im;
register struct pframe *f;
SMALL where;
#endif
{
	int x, y, w, h;//, i;
	char *srcdata, *mdata;
	//unsigned char  *cptr;
	//unsigned short *sptr, min, max, dif;
	RawIm *temp_im;


	if (im->bits == 16)
	{
		temp_im = (RawIm *)Calloc(1, sizeof(RawIm));
		temp_im->type = 90;
		temp_im->width = im->width;
		temp_im->height = im->height;
		temp_im->bits = 16;
		temp_im->values = (unsigned char *)Malloc(2 * temp_im->width * temp_im->height * sizeof(unsigned char));
		memcpy(temp_im->values, im->values, 2 * temp_im->width * temp_im->height);


		fs12to8(temp_im, TRUE);

		/* Fastpixobj now uses 1 less param, ddgs_get_draw_buffer no longer needed. WH */
		if ((srcdata = (char *)picrawbuffer(temp_im, &x, &y, &w, &h, f, &mdata)) != NULL)
		{
			fastpixobj(x, y, w, h, srcdata, mdata, where);
			#if 0
			if ((vdata = (char *)ddgs_get_draw_buffer(w*h)) != NULL)
				fastpixobj(x, y, w, h, vdata, mdata, where, srcdata);
			#endif
		}

		Free(temp_im->values);
		Free(temp_im);
	}
	else
	{
	/* Fastpixobj now uses 1 less param, ddgs_get_draw_buffer no longer needed. WH */
		if ((srcdata = (char *)picrawbuffer(im, &x, &y, &w, &h, f, &mdata)) != NULL)
		{
			fastpixobj(x, y, w, h, srcdata, mdata, where);
			#if 0
			if ((vdata = (char *)ddgs_get_draw_buffer(w*h)) != NULL)
				fastpixobj(x, y, w, h, vdata, mdata, where, srcdata);
			#endif
		}
	}
}


/* versions to work in grey, overlay or marker image planes */

#ifdef WIN32
void
picrawim(RawIm *im, register struct pframe *f)
#endif
#ifdef i386
void
picrawim(im, f)
RawIm *im;
register struct pframe *f;
#endif
{
	Picrawim(im,f,GREYIM);
}

#ifdef WIN32
void
opicrawim(RawIm *im, register struct pframe *f)
#endif
#ifdef i386
void
opicrawim(im, f)
RawIm *im;
register struct pframe *f;
#endif
{
	Picrawim(im,f,OVLYIM);
}

#ifdef WIN32
void
mpicrawim(RawIm *im, register struct pframe *f)
#endif
#ifdef i386
void
mpicrawim(im, f)
RawIm *im;
register struct pframe *f;
#endif
{
	Picrawim(im,f,MARKIM);
}




/***********************************************************************/

/*
 * Picpoly - display a polygon domain.
 */

#ifdef WIN32
void
Picpoly(struct polygondomain *pdom, struct pframe *f, SMALL where)
#endif
#ifdef i386
void
Picpoly(pdom, f, where)
struct polygondomain *pdom;
struct pframe *f;
SMALL where;
#endif
{
	struct fvertex *fverts;
	struct ivertex *iverts;
	int npoints, i, xo, yo, xs, ys;
	register int xdum, ydum;
	DDGSPoint *xp;
	DDGS *dg;

/* Do nothing if nothing to do. */
	if (pdom->nvertices < 2)
		return;

/*
 * BP - now use ddgs buffer instead.
	xp = (XPoint *)Malloc(sizeof(XPoint)*pdom->nvertices);
*/
	xp = (DDGSPoint *)ddgs_get_aux_buffer(sizeof(DDGSPoint)*pdom->nvertices);

	dg = getcurdg();

	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	xo = f->dx - xs*f->ox + ABS(xs)/2;	/* to line up centrally with pixels */
	yo = f->dy - ys*f->oy + ABS(ys)/2;

/* Convert the polygon coordinates into sensible ones. */
	switch (pdom->type)
	{
	case 1 :		/* Integer poly domain. */
		iverts = pdom->vtx;

		for (i = 0; i < pdom->nvertices; i++)
		{
/* Calculate via int values to avoid overflow
 * at large scales. */
			xdum = xo + xs*(int)iverts->vtX;
			ydum = yo + ys*(int)iverts->vtY;
			xp[i].x = (short)(xdum >> 3);
			xp[i].y = (short)(dg->maxy - (ydum >> 3));
/* Fix?
			xp[i].y = (short)(dg->maxy - ((ydum + 4) >> 3));
*/
			iverts++;
		}
		break;

	case 2 :		/* Float poly domain (hopefully not used at all!) */
		fverts = (struct fvertex *)pdom->vtx;

		for (i = 0; i < pdom->nvertices; i++)
		{
			xdum = xo + (int)((float)xs*fverts->vtX);
			ydum = yo + (int)((float)ys*fverts->vtY);
			xp[i].x = xdum >> 3;
			xp[i].y = dg->maxy - (ydum >> 3);
			fverts++;
		}

	default:
		break;
	}

#if 0

/* If Univision */
	if (flip_probe_colours)
	{
		register short xdiff, ydiff, j;

/* Run through all the points to see if
 * any of them are adjacent. These will
 * be ignored. */
		if ((npoints = pdom->nvertices) > 1)
			for (i = 0; i < npoints - 1; i++)
			{
				xdiff = xp[i].x - xp[i + 1].x;
				ydiff = xp[i].y - xp[i + 1].y;

/* If the next point is adjacent to this
 * one, skip it. */
				if ((xdiff >= -1) && (xdiff <= 1) &&
					(ydiff >= -1) && (ydiff <= 1))
				{
/* Pull all the remaining points back
 * by one place, so the list of points
 * is still cointiguous. */
					for (j = i + 1; j < npoints; j++)
						xp[j] = xp[j + 1];

/* Now we have one less point. */
					npoints--;
				}
			}
	}
	else
#endif

		npoints = pdom->nvertices;		/* Normally just use them all. */

	switch (where)
	{			
//		case GREYIM: drawpoly(xp, npoints); break;
		case OVLYIM: odrawpoly(xp, npoints); break;
		case MARKIM: mdrawpoly(xp, npoints); break;
	}

/*
 * BP - don't free: now using ddgs buffer.
	Free(xp);
 */
}




/* versions to work in grey, overlay or marker image planes */

#ifdef WIN32
void
picpoly(struct polygondomain *pdom, struct pframe *f)
#endif
#ifdef i386
void
picpoly(pdom, f)
struct polygondomain *pdom;
struct pframe *f;
#endif
{
	Picpoly(pdom,f,GREYIM);
}

#ifdef WIN32
void
opicpoly(struct polygondomain *pdom, struct pframe *f)
#endif
#ifdef i386
void
opicpoly(pdom, f)
struct polygondomain *pdom;
struct pframe *f;
#endif
{
	Picpoly(pdom,f,OVLYIM);
}

#ifdef WIN32
void
mpicpoly(struct polygondomain *pdom, struct pframe *f)
#endif
#ifdef i386
void
mpicpoly(pdom, f)
struct polygondomain *pdom;
struct pframe *f;
#endif
{
	Picpoly(pdom,f,MARKIM);
}


/*************************************************************************/

/*
 * display a circle object
 */
#ifdef WIN32
void
Piccircle(struct circle *circ, struct pframe *f, SMALL where)
#endif
#ifdef i386
void
Piccircle(circ, f, where)
struct circle *circ;
struct pframe *f;
SMALL where;
#endif
{
	int xo,yo,xs,ys;
	int x,y,r;


	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	xo = f->dx - xs*f->ox + ABS(xs)/2;	/* to line up centrally with pixels */
	yo = f->dy - ys*f->oy + ABS(ys)/2;

	x = xo+xs*circ->centre.vtX;
	y = yo+ys*circ->centre.vtY;
	r = xs*circ->radius;
	
	switch (where) {			
//		case GREYIM:	drawcircle(x,y,r); break;
		case OVLYIM:	odrawcircle(x,y,r); break;
		case MARKIM:	mdrawcircle(x,y,r); break;
		default: 	break;
	}

}

/* versions to work in grey, overlay or marker image planes */

#ifdef WIN32
void
piccircle(struct circle *circ, struct pframe *f)
#endif
#ifdef i386
void
piccircle(circ, f)
struct circle *circ;
struct pframe *f;
#endif
{
	Piccircle(circ,f,GREYIM);
}

#ifdef WIN32
void
opiccircle(struct circle *circ, struct pframe *f)
#endif
#ifdef i386
void
opiccircle(circ, f)
struct circle *circ;
struct pframe *f;
#endif
{
	Piccircle(circ,f,OVLYIM);
}

#ifdef WIN32
void
mpiccircle(struct circle *circ, struct pframe *f)
#endif
#ifdef i386
void
mpiccircle(circ, f)
struct circle *circ;
struct pframe *f;
#endif
{
	Piccircle(circ,f,MARKIM);
}

/**********************************************************************/

	

/*
 * display boundary list of polygons recursively
 */
#ifdef WIN32
void
Picbound(struct boundlist *bound, struct pframe *f, SMALL where)
#endif
#ifdef i386
void
Picbound(bound, f, where)
struct boundlist *bound;
struct pframe *f;
SMALL where;
#endif
{
	struct boundlist *bp;

	/*
	 * This code has been replaced with code below as the latter is more
	 * stack efficient
	 *	if (bound != NULL) {
	 *		picpoly(bound->poly,f);
	 *		picbound(bound->next,f);
	 *		picbound(bound->down,f);
	 *	}
	 */	
		
	if((bp = bound) != NULL) {
		do {
			Picpoly(bp->poly, f, where);
			Picbound(bp->down, f, where);
		} while((bp = bp->next) != NULL);
	}
}

/* versions to work in grey, overlay or marker image planes */

#ifdef WIN32
void
picbound(struct boundlist *bound, struct pframe *f)
#endif
#ifdef i386
void
picbound(bound, f)
struct boundlist *bound;
struct pframe *f;
#endif
{
	Picbound(bound,f,GREYIM);
}

#ifdef WIN32
void
opicbound(struct boundlist *bound, struct pframe *f)
#endif
#ifdef i386
void
opicbound(bound, f)
struct boundlist *bound;
struct pframe *f;
#endif
{
	Picbound(bound,f,OVLYIM);
}

#ifdef WIN32
void
mpicbound(struct boundlist *bound, struct pframe *f)
#endif
#ifdef i386
void
mpicbound(bound, f)
struct boundlist *bound;
struct pframe *f;
#endif
{
	Picbound(bound,f,MARKIM);
}


/*************************************************************************/


/*
 * display a vector
 */
#ifdef WIN32
void
Picvec(struct ivector *vec, struct pframe *f, SMALL where)
#endif
#ifdef i386
void
Picvec(vec, f, where)
struct ivector *vec;
struct pframe *f;
SMALL where;
#endif
{
	register struct fvector *fvec;
	register xo,yo,xs,ys;
	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	xo = f->dx - xs*f->ox + ABS(xs)/2;	/* to line up centrally with pixels */
	yo = f->dy - ys*f->oy + ABS(ys)/2;
	switch (vec->type) {
	case 30 :
		moveto(xo+xs*vec->k1, yo+ys*vec->l1);
		switch (where) {
		case OVLYIM:
			olineto(xo+xs*vec->k2, yo+ys*vec->l2);
			break;
		case MARKIM:
			mlineto(xo+xs*vec->k2, yo+ys*vec->l2);
			break;
		case GREYIM:
		default:			
//			lineto(xo+xs*vec->k2, yo+ys*vec->l2);
			break;
		}
		break;
	case 31 :
/**/
		fvec = (struct fvector *) vec;
		moveto(xo+(int)(xs*fvec->k1), yo+(int)(ys*fvec->l1));
		switch (where) {
		case OVLYIM:
			olineto(xo+(int)(xs*fvec->k2), yo+(int)(ys*fvec->l2));
			break;
		case MARKIM:
			mlineto(xo+(int)(xs*fvec->k2), yo+(int)(ys*fvec->l2));
			break;
		case GREYIM:
		default:			
//			lineto(xo+(int)(xs*fvec->k2), yo+(int)(ys*fvec->l2));
			break;
		}
/**/
		break;
	default:
		break;
	}
}

/* versions to work in grey, overlay or marker image planes */

#ifdef WIN32
void
picvec(struct ivector *vec, struct pframe *f)
#endif
#ifdef i386
void
picvec(vec, f)
struct ivector *vec;
struct pframe *f;
#endif
{
	Picvec(vec,f,GREYIM);
}

#ifdef WIN32
void
opicvec(struct ivector *vec, struct pframe *f)
#endif
#ifdef i386
void
opicvec(vec, f)
struct ivector *vec;
struct pframe *f;
#endif
{
	Picvec(vec,f,OVLYIM);
}

#ifdef WIN32
void
mpicvec(struct ivector *vec, struct pframe *f)
#endif
#ifdef i386
void
mpicvec(vec, f)
struct ivector *vec;
struct pframe *f;
#endif
{
	Picvec(vec,f,MARKIM);
}


/************************************************************************/

/*
 * display a point (in future, in a variety of styles; just a cross for now)
 */
#ifdef WIN32
void
Picpoint(struct ipoint *point, struct pframe *f, SMALL where)
#endif
#ifdef i386
void
Picpoint(point, f, where)
struct ipoint *point;
struct pframe *f;
SMALL where;
#endif
{
	register struct fpoint *fpoint;
	register xo,yo,xs,ys;
	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	xo = f->dx - xs*f->ox + ABS(xs)/2;	/* to line up centrally with pixels */
	yo = f->dy - ys*f->oy + ABS(ys)/2;
	switch (point->type) {
	case 40 :
		moveto(xo+xs*point->k, yo+ys*point->l);
		Piccursor(abs(xs),point->style,where);
		break;
	case 41 :
/**/
		fpoint = (struct fpoint *) point;
		moveto(xo+(int)(xs*fpoint->k), xo+(int)(xs*fpoint->l));
		Piccursor(abs(xs),fpoint->style,where);
/**/
		break;
	default:
		break;
	}
}
/* versions to work in grey, overlay or marker image planes */

#ifdef WIN32
void
picpoint(struct ipoint *point, struct pframe *f)
#endif
#ifdef i386
void
picpoint(point, f)
struct ipoint *point;
struct pframe *f;
#endif
{
	Picpoint(point,f,GREYIM);
}

#ifdef WIN32
void
opicpoint(struct ipoint *point, struct pframe *f)
#endif
#ifdef i386
void
opicpoint(point, f)
struct ipoint *point;
struct pframe *f;
#endif
{
	Picpoint(point,f,OVLYIM);
}

#ifdef WIN32
void
mpicpoint(struct ipoint *point, struct pframe *f)
#endif
#ifdef i386
void
mpicpoint(point, f)
struct ipoint *point;
struct pframe *f;
#endif
{
	Picpoint(point,f,MARKIM);
}


/***********************************************************************/

#ifdef WIN32
void
Piccursor(int scale, int style, SMALL where)
#endif
#ifdef i386
void
Piccursor(scale, style, where)
int scale;
int style;
SMALL where;
#endif
{
	switch (style) {
	default :
	case 0:				/* constant sized cross */
		scale = 16;
	case 1:				/* scaled cross */
		scale = scale*4;
		switch (where) {
		case OVLYIM:
			moveby(-scale/2,-scale/2);
			olineby(scale,scale);
			moveby(0,-scale);
			olineby(-scale,scale);
			break;
		case MARKIM:
			moveby(-scale/2,-scale/2);
			mlineby(scale,scale);
			moveby(0,-scale);
			mlineby(-scale,scale);
			break;
		case GREYIM:
		default:			
			moveby(-scale/2,-scale/2);
//			lineby(scale,scale);
			moveby(0,-scale);
//			lineby(-scale,scale);
			break;
		}

		return;
	case 2:				/* constant sized blob */
		switch (where) {
		case OVLYIM:
			moveby(-8,-8);
			olineby(16,0);
			moveby(0,8);
			olineby(-16,0);
			moveby(0,8);
			olineby(16,0);
			break;
		case MARKIM:
			moveby(-8,-8);
			mlineby(16,0);
			moveby(0,8);
			mlineby(-16,0);
			moveby(0,8);
			mlineby(16,0);
			break;
		case GREYIM:
		default:			
			moveby(-8,-8);
//			lineby(16,0);
			moveby(0,8);
//			lineby(-16,0);
			moveby(0,8);
//			lineby(16,0);
			break;
		}
		break;

	case 4:				/* constant sized hollow square */
		scale = 32;
		lwidth(2);
	case 5:				/* scaled hollow square */
		switch (where) {
		case OVLYIM:
			moveby(-scale,-scale);
			scale *= 2;
			olineby(scale,0);
			olineby(0,scale);
			olineby(-scale,0);
			olineby(0,-scale);
			break;
		case MARKIM:
			moveby(-scale,-scale);
			scale *= 2;
			mlineby(scale,0);
			mlineby(0,scale);
			mlineby(-scale,0);
			mlineby(0,-scale);
			break;
		case GREYIM:
		default:			
			moveby(-scale,-scale);
			scale *= 2;
//			lineby(scale,0);
//			lineby(0,scale);
//			lineby(-scale,0);
//			lineby(0,-scale);
			break;
		}
		if (style == 4)
			lwidth(0);
		break;

	case 6:				/* tilted arrow head */
		/* arrowhead(32); mc 11/6/92 (in DDGS) */
		break;

	case 8:	lwidth(3);	/* FAT version of  */
	case 7: 		/* + shaped cross  */
		scale = scale*8;
		switch (where) {
		case OVLYIM:
			moveby(-scale/2,0);
			olineby(scale,0);
			moveby(-scale/2,-scale/2);
			olineby(0,scale);
			break;
		case MARKIM:
			moveby(-scale/2,0);
			mlineby(scale,0);
			moveby(-scale/2,-scale/2);
			mlineby(0,scale);
			break;
		case GREYIM:
		default:			
			moveby(-scale/2,0);
//			lineby(scale,0);
			moveby(-scale/2,-scale/2);
//			lineby(0,scale);
			break;
		}

		if (style==8)
			lwidth(0);
		break;
	}
}

/* versions to work in grey, overlay or marker image planes */

#ifdef WIN32
void
piccursor(int scale, int style)
#endif
#ifdef i386
void
piccursor(scale, style)
int scale;
int style;
#endif
{
	Piccursor(scale,style,GREYIM);
}

#ifdef WIN32
void
opiccursor(int scale, int style)
#endif
#ifdef i386
void
opiccursor(scale, style)
int scale;
int style;
#endif
{
	Piccursor(scale,style,OVLYIM);
}

#ifdef WIN32
void
mpiccursor(int scale, int style)
#endif
#ifdef i386
void
mpiccursor(scale, style)
int scale;
int style;
#endif
{
	Piccursor(scale,style,MARKIM);
}


/**********************************************************************/

#ifdef WIN32
void
Picrect(struct irect *rdom, struct pframe *f, SMALL where)
#endif
#ifdef i386
void
Picrect(rdom, f, where)
struct irect *rdom;
struct pframe *f;
SMALL where;
#endif
{
	register i, j;
	WZCOORD *k, *l;
	float *fk, *fl;
	struct frect *fdom;
	register xo,yo,xs,ys;
	int xdum, ydum;
	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	xo = f->dx - xs*f->ox + ABS(xs)/2;	/* to line up centrally with pixels */
	yo = f->dy - ys*f->oy + ABS(ys)/2;
	switch(rdom->type) {
	case 1 :
		k = rdom->irk;
		l = rdom->irl;
		moveto(xo+xs*k[0], yo+ys*l[0]);
		switch (where) {
		case OVLYIM:
			for (i=1; i<=4; i++) {
				j = i==4? 0: i;
				olineto(xo+xs*k[j], yo+ys*l[j]);
			}
			break;
		case MARKIM:
			for (i=1; i<=4; i++) {
				j = i==4? 0: i;
				mlineto(xo+xs*k[j], yo+ys*l[j]);
			}
			break;
		case GREYIM:
		default:			
			for (i=1; i<=4; i++) {
				j = i==4? 0: i;
//				lineto(xo+xs*k[j], yo+ys*l[j]);
			}
			break;
		}
		break;

	case 2 :
		fdom = (struct frect *) rdom;
		fk = fdom->frk;
		fl = fdom->frl;
		xdum = (int)((float)xs*fk[0]);
		ydum = (int)((float)ys*fl[0]);
		moveto(xo+xdum, yo+ydum);
		switch (where) {
		case OVLYIM:
			for (i=1; i<=4; i++) {
				j = i==4? 0: i;
				xdum = (int)((float)xs*fk[j]);
				ydum = (int)((float)ys*fl[j]);
				olineto(xo+xdum, yo+ydum);
			}
			break;
		case MARKIM:
			for (i=1; i<=4; i++) {
				j = i==4? 0: i;
				xdum = (int)((float)xs*fk[j]);
				ydum = (int)((float)ys*fl[j]);
				mlineto(xo+xdum, yo+ydum);
			}
			break;
		case GREYIM:
		default:			
			for (i=1; i<=4; i++) {
				j = i==4? 0: i;
				xdum = (int)((float)xs*fk[j]);
				ydum = (int)((float)ys*fl[j]);
//				lineto(xo+xdum, yo+ydum);
			}
			break;
		}
		break;
	}
}

/* versions to work in grey, overlay or marker image planes */

#ifdef WIN32
void
picrect(struct irect *rdom, struct pframe *f)
#endif
#ifdef i386
void
picrect(rdom, f)
struct irect *rdom;
struct pframe *f;
#endif
{
	Picrect(rdom,f,GREYIM);
}

#ifdef WIN32
void
opicrect(struct irect *rdom, struct pframe *f)
#endif
#ifdef i386
void
opicrect(rdom, f)
struct irect *rdom;
struct pframe *f;
#endif
{
	Picrect(rdom,f,OVLYIM);
}

#ifdef WIN32
void
mpicrect(struct irect *rdom, struct pframe *f)
#endif
#ifdef i386
void
mpicrect(rdom, f)
struct irect *rdom;
struct pframe *f;
#endif
{
	Picrect(rdom,f,MARKIM);
}

/**********************************************************************/


#ifdef WIN32
void
Pichisto(struct histogramdomain *h, struct pframe *f, SMALL where)
#endif
#ifdef i386
void
Pichisto(h, f, where)
struct histogramdomain *h;
struct pframe *f;
SMALL where;
#endif
{
	int *iv;
	float *fv;
	register i, k, l;
	register xs,ys;
	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	k = f->dx - xs*(f->ox - h->k) + xs/2;
	l = f->dy - ys*(f->oy - h->l) + ys/2;

	moveto(k, l);
	switch (h->type) {
	default:
	case 1:
		iv = h->hv;
		switch (h->r) {
		default:
		case 0:
			switch (where) {
			case OVLYIM:
				for (i=0; i<h->npoints; i++) {
					olineto(k+xs* (*iv), l);
					l += ys;
					iv++;
				}
				olineto(k, l-ys);
				break;
			case MARKIM:
				for (i=0; i<h->npoints; i++) {
					mlineto(k+xs* (*iv), l);
					l += ys;
					iv++;
				}
				mlineto(k, l-ys);
				break;
			case GREYIM:
			default:			
				for (i=0; i<h->npoints; i++) {
//					lineto(k+xs* (*iv), l);
					l += ys;
					iv++;
				}
//				lineto(k, l-ys);
				break;
			}
			break;

		case 1:
			switch (where) {
			case OVLYIM:
				for (i=0; i<h->npoints; i++) {
					olineto(k, l+ys* (*iv));
					k += xs;
					iv++;
				}
				olineto(k-xs, l);
				break;
			case MARKIM:
				for (i=0; i<h->npoints; i++) {
					mlineto(k, l+ys* (*iv));
					k += xs;
					iv++;
				}
				mlineto(k-xs, l);
				break;
			case GREYIM:
			default:			
				for (i=0; i<h->npoints; i++) {
//					lineto(k, l+ys* (*iv));
					k += xs;
					iv++;
				}
//				lineto(k-xs, l);
				break;
			}
			return;
		}

	case 2:
/**/
		fv = (float *) h->hv;
		switch (h->r) {
		default:
		case 0:
			switch (where) {
			case OVLYIM:
				for (i=0; i<h->npoints; i++) {
					olineto(k+ (int)(xs* *fv), l);
					l += ys;
					fv++;
				}
				olineto(k, l-ys);
				break;
			case MARKIM:
				for (i=0; i<h->npoints; i++) {
					mlineto(k+ (int)(xs* *fv), l);
					l += ys;
					fv++;
				}
				mlineto(k, l-ys);
				break;
			case GREYIM:
			default:			
				for (i=0; i<h->npoints; i++) {
//					lineto(k+ (int)(xs* *fv), l);
					l += ys;
					fv++;
				}
//				lineto(k, l-ys);
				break;
			}
			return;

		case 1:
			switch (where) {
			case OVLYIM:
				for (i=0; i<h->npoints; i++) {
					olineto(k, l+ (int)(ys* *fv));
					k += xs;
					fv++;
				}
				olineto(k-xs, l);
				break;
			case MARKIM:
				for (i=0; i<h->npoints; i++) {
					mlineto(k, l+ (int)(ys* *fv));
					k += xs;
					fv++;
				}
				mlineto(k-xs, l);
				break;
			case GREYIM:
			default:			
				for (i=0; i<h->npoints; i++) {
//					lineto(k, l+ (int)(ys* *fv));
					k += xs;
					fv++;
				}
//				lineto(k-xs, l);
				break;
			}
			return;
		}
/**/
		break;
	}
}

/* versions to work in grey, overlay or marker image planes */

#ifdef WIN32
void
pichisto(struct histogramdomain *h, struct pframe *f)
#endif
#ifdef i386
void
pichisto(h, f)
struct histogramdomain *h;
struct pframe *f;
#endif
{
	Pichisto(h,f,GREYIM);
}

#ifdef WIN32
void
opichisto(struct histogramdomain *h, struct pframe *f)
#endif
#ifdef i386
void
opichisto(h, f)
struct histogramdomain *h;
struct pframe *f;
#endif
{
	Pichisto(h,f,OVLYIM);
}

#ifdef WIN32
void
mpichisto(struct histogramdomain *h, struct pframe *f)
#endif
#ifdef i386
void
mpichisto(h, f)
struct histogramdomain *h;
struct pframe *f;
#endif
{
	Pichisto(h,f,MARKIM);
}




/**************************************************************************/

/*
 * Text object.
 *
 * Completely rewritten, because UNIX version was
 * full of X calls....
 * Now passes string, position and scale to ddgs.
 *
 * .... but this is only adequate for single lines - hence its rename (MG)
 */



#ifdef WIN32
void
OnelinePictext(struct textobj *tobj, struct pframe *f, SMALL where)
#endif
#ifdef i386
void
OnelinePictext(tobj, f, where)
struct textobj *tobj;
struct pframe *f;
SMALL where;
#endif
{
	char *str;
	int xo, yo, xs, ys;
	int x, y;

	str = tobj->text;

	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	xo = f->dx - xs*f->ox + ABS(xs)/2;	/* to line up centrally with pixels */
	yo = f->dy - ys*f->oy + ABS(ys)/2;

	x = xo + xs*tobj->tdom->k;
	y = yo + ys*tobj->tdom->l;

	setfont(tobj->tdom->font);

	switch (where) {			
//		case GREYIM:	text(x, y, str, f->scale); break;
		case OVLYIM:	otext(x, y, str, f->scale); break;
		case MARKIM:	mtext(x, y, str, f->scale); break;
		default: 	break;
	}
}

/*
 * text object - original version from UNIX (MG)
 * a quick and dirty fix here for text with new-line characters
 */
#ifdef WIN32
void
Pictext(struct textobj *tobj, struct pframe *f, SMALL where)
#endif
#ifdef i386
void
Pictext(tobj, f, where)
struct textobj *tobj;
struct pframe *f;
SMALL where;
#endif
{
	char 	*c;
	char 	*keepstr;
	int 	i,len,newlines,txtheight,txtwidth;
	int		savedy;
	char	s[]="TestString";

	savedy=f->dy;

	c = keepstr = tobj->text;

	if (c==NULL)
		return;

	/* get number of newlines in text */
	newlines=0;
	len=strlen(c);

	if (len==0)
		return;

	for (i=0; i<len; i++) {
		if (c[i]=='\n')
		newlines++;
	}
	
	setfont(tobj->tdom->font);

	/* deteremine height of one line of text */
	textsize(s, &txtwidth, &txtheight);
	txtheight=(txtheight * f->scale) >> 3;
	/* set position of first line of text */
	f->dy+=newlines*txtheight;

	while (*c != '\0') {
		if (*c == '\n') {
			*c = '\0';

			if (strlen(tobj->text)>0) {
				OnelinePictext(tobj, f, where);
			}

			*c = '\n';
			tobj->text = c+1;

			/* update text position */
			f->dy-=txtheight;
		}
		c++;
	}
	if (c != tobj->text) {
		if (strlen(tobj->text)>0) {
			OnelinePictext(tobj, f, where);
		}
	}

	tobj->text = keepstr;

	/* restore frame pointer */
	f->dy=savedy;
}



/* versions to work in grey, overlay or marker image planes */

#ifdef WIN32
void
pictext(struct textobj *tobj, struct pframe *f)
#endif
#ifdef i386
void
pictext(tobj, f)
struct textobj *tobj;
struct pframe *f;
#endif
{
	Pictext(tobj,f,GREYIM);
}

#ifdef WIN32
void
opictext(struct textobj *tobj, struct pframe *f)
#endif
#ifdef i386
void
opictext(tobj, f)
struct textobj *tobj;
struct pframe *f;
#endif
{
	Pictext(tobj,f,OVLYIM);
}

#ifdef WIN32
void
mpictext(struct textobj *tobj, struct pframe *f)
#endif
#ifdef i386
void
mpictext(tobj, f)
struct textobj *tobj;
struct pframe *f;
#endif
{
	Pictext(tobj,f,MARKIM);
}

