/*
 *	POLYTOBLOB converts polygon into solid filled object
 *
 *	Mark Gregson 26/8/93
 *
 * Modifications
 *
 *	10Mar2000	JMB		Changed headers included, as part of conversion to DLL.
 *						Also, made minor mods to stop compiler warnings and removed
 *						standard C parameter lists (were '#ifdef i386' only).
 *	9/20/96		BP:		Make get_boundary_point_type static because it
 *						uses locally-defines structures!
 *
 */

#include "woolz.h"

#include <stdio.h>
#include <malloc.h> // For malloc, calloc, free
//#include <wstruct.h>

typedef struct bound_pt_type{
		int x;
		int y;
	}bound_pt;

typedef struct xpos_type{
		int x;
		struct xpos_type *next;
	}xptype;

/*
 *	Determine boundary point type based on vectors in and out of the point
 *
 *	0 = on a line
 *	1 = start or end of a line
 *	2 = start and end of a line
 */
static short
get_boundary_point_type(bound_pt pt1, bound_pt pt2, bound_pt pt3)
{
	static short vec[3][3]= {	1,0,7,
			  		2,8,6,
					3,4,5 };

	short vecin,vecout,btype;


	/* get vector in and out of pt2 */			
	vecin=vec[pt2.x-pt1.x+1][pt2.y-pt1.y+1];
	vecout=vec[pt3.x-pt2.x+1][pt3.y-pt2.y+1];
	
	btype=0;

	vecout=(vecout + 3) & 7;
	
	if (vecin > 3) {
		if (vecout < 4)
			btype=1;	
		else 
			if (vecout < vecin)
				btype=2;
	}
	else {
		if (vecout > 3)
			btype=1;
		else
			if (vecout < vecin)
				btype=2;
	}

	return(btype);
}
	

/*
 *	polytoblob	Mark Gregson 30/7/93
 *
 *	Create solid filled object from polygon - object has NULL vdom
 */	
struct object *
polytoblob(struct polygondomain *pdom)
{
	int			i, /*x, y,*/ minx, miny, maxx, maxy/*, nobjs*/;
	int			width, height, /*area, length,*/holes;
	struct object		*obj,*bobj,*uobj;
	struct boundlist	*blist;
	xptype			*xptr, *prevxptr, *newx, *newx2, **ypts;
	bound_pt		bpt, lastbpt, nextbpt;
	short			btype;
	struct interval		*intvs, *intbase;
	int 			nints;
	struct intervaldomain	*idom;
	int 			offset;


mal_comment("start of polytoblob");
	/* convert polygon to object */
	obj =(struct object *)polytoobj(pdom);

	/* discard object vdom - we dont need it */
	freevaluetable((struct intervaldomain *)obj->vdom);
	obj->vdom=NULL;

	/* check if it is a closed loop - will have holes inside */
	bobj=(struct object *)ibound(obj,1);
	blist=(struct boundlist *)bobj->idom;
	if (blist->down == NULL)
		holes=0;
	else
		holes=1;
	

	if (holes==0) {
		/* no holes to fill return obj */
		freeobj(bobj);
		return(obj);
	}

	/* if we get this far we need to fill the object */

	/* get size of object */
	minx=obj->idom->kol1;
	maxx=obj->idom->lastkl;
	miny=obj->idom->line1;
	maxy=obj->idom->lastln;
	width=maxx-minx+1;
	height=maxy-miny+1;

	/* get unit boundary of object */
	uobj=(struct object *)unitbound(bobj);
	freeobj(bobj);
	freeobj(obj);

	
	/* allocate y position array and initialise all pointers to NULL */
	ypts=(xptype **) Calloc(height,sizeof(xptype *));

	pdom = (struct polygondomain *) uobj->idom;

	/* note last points in boundary may = first point so */
	offset=1;
	while (( pdom->vtx[pdom->nvertices-offset].vtX == pdom->vtx[0].vtX )
	&& ( pdom->vtx[pdom->nvertices-offset].vtY == pdom->vtx[0].vtY ))
		offset++;

	lastbpt.x=pdom->vtx[pdom->nvertices-offset].vtX;
	lastbpt.y=pdom->vtx[pdom->nvertices-offset].vtY;

	nints=0;
	/* scan round boundary and create xy table of interval end points */
	for (i=0;i<pdom->nvertices-offset+1;i++) {
		bpt.x=pdom->vtx[i].vtX;
		bpt.y=pdom->vtx[i].vtY;
		nextbpt.x=pdom->vtx[i+1].vtX;
		nextbpt.y=pdom->vtx[i+1].vtY;

		btype=get_boundary_point_type(lastbpt,bpt,nextbpt);
		
		if (btype!=0){
			/* btype=1 start or finish of an interval */

			xptr=ypts[bpt.y-miny];

			newx=(xptype *)Calloc(1,sizeof(xptype));
			newx->x=bpt.x;
			newx->next=NULL;
			nints++;

			prevxptr=NULL;
			while (xptr!=NULL) {
			   	if (xptr->x >= bpt.x) 
					break;
				else{
					prevxptr=xptr;
					xptr=xptr->next;
				}
			}

			if (prevxptr==NULL)
				ypts[bpt.y-miny]=newx;
			else
				prevxptr->next=newx;

			newx->next=xptr;

			/* btype=2 start and finish of an interval */
			if (btype==2) {
				newx2=(xptype *)Calloc(1,sizeof(xptype));
				*newx2=*newx;
				newx->next=newx2;
				nints++;
			}
		}

		lastbpt=bpt;
	}
	nints=nints/2;	/* nomber of intervals */

	/* free unit boundary */
	freeobj(uobj);			

	/* generate new object interval table */			
	intbase=(struct interval *)Calloc(nints,sizeof(struct interval));
	idom=(struct intervaldomain *)makedomain(1,miny,maxy,minx,maxx);
	idom->freeptr = (char *) intbase;
	obj=(struct object *)makemain(1,idom,NULL,NULL,NULL);					
		
	intvs=intbase;
	for (i=miny;i<=maxy;i++) {

		xptr=ypts[i-miny];

		nints=0;
		intbase=intvs;

		while (xptr!=NULL) {
			newx=xptr;
			intvs->ileft=xptr->x-minx;
			xptr=xptr->next;
			Free(newx);

			newx=xptr;
			intvs->iright=xptr->x-minx;
			xptr=xptr->next;
			Free(newx);

			nints++;
			intvs++;
		}

		makeinterval(i,idom,nints,intbase);
	}
				
	/* free off interval endpoints table */	
	Free(ypts);

mal_comment("end of polytoblob");

	/* return filled object */
	return(obj);
}

/*
 *
 *	Create solid filled object from polygon - object has NULL vdom
 */	
struct object *bigboundtoblob(struct ivertex *ivtx, int nvertices)
{
	int					i, minx, miny, maxx, maxy;
	int					width, height;
	struct object		*obj;
	xptype				*xptr, *prevxptr, *newx, *newx2, **ypts;
	bound_pt			bpt, lastbpt, nextbpt;
	short				btype;
	struct interval		*intvs, *intbase;
	int 				nints;
	struct intervaldomain	*idom;
	int 				offset;
	int					unvertices;
	struct ivertex		*uvtx;
	int					left, right;


	if ((nvertices <= 0) || ( ivtx == NULL))
		return NULL;

	/* get size of input object */
	minx=maxx=ivtx[0].vtX;
	miny=maxy=ivtx[0].vtY;

	for (i=1; i<nvertices;i++)
	{
		if (ivtx[i].vtX < minx)
			minx=ivtx[i].vtX;
		if (ivtx[i].vtY < miny)
			miny=ivtx[i].vtY;
		if (ivtx[i].vtX >maxx)
			maxx=ivtx[i].vtX;
		if (ivtx[i].vtY > maxy)
			maxy=ivtx[i].vtY;
	}

	width=maxx-minx+1;
	height=maxy-miny+1;

	uvtx = unitbigbound(ivtx, nvertices, &unvertices);

	if ((unvertices <= 0) || ( uvtx == NULL))
		return NULL;

	/* if we get this far we need to fill the object */

	/* get size of object */
	minx=maxx=uvtx[0].vtX;
	miny=maxy=uvtx[0].vtY;

	for (i=1; i<unvertices;i++)
	{
		if (uvtx[i].vtX < minx)
			minx=uvtx[i].vtX;

		if (uvtx[i].vtY < miny)
			miny=uvtx[i].vtY;

		if (uvtx[i].vtX >maxx)
			maxx=uvtx[i].vtX;

		if (uvtx[i].vtY > maxy)
			maxy=uvtx[i].vtY;
	}

	width=maxx-minx+1;
	height=maxy-miny+1;

	/* allocate y position array and initialise all pointers to NULL */
	ypts=(xptype **) Calloc(height,sizeof(xptype *));

	/* note last points in boundary may = first point so */
	offset=1;
	while (( uvtx[unvertices-offset].vtX == uvtx[0].vtX )
	&& ( uvtx[unvertices-offset].vtY == uvtx[0].vtY ))
		offset++;

	lastbpt.x=uvtx[unvertices-offset].vtX;
	lastbpt.y=uvtx[unvertices-offset].vtY;

	nints=0;
	/* scan round boundary and create xy table of interval end points */
	for (i=0;i<unvertices-offset+1;i++) {
		bpt.x=uvtx[i].vtX;
		bpt.y=uvtx[i].vtY;
		nextbpt.x=uvtx[i+1].vtX;
		nextbpt.y=uvtx[i+1].vtY;

		btype=get_boundary_point_type(lastbpt,bpt,nextbpt);
		
		if (btype!=0){
			/* btype=1 start or finish of an interval */

			xptr=ypts[bpt.y-miny];

			newx=(xptype *)Calloc(1,sizeof(xptype));
			newx->x=bpt.x;
			newx->next=NULL;
			nints++;

			prevxptr=NULL;
			while (xptr!=NULL) {
			   	if (xptr->x >= bpt.x) 
					break;
				else{
					prevxptr=xptr;
					xptr=xptr->next;
				}
			}

			if (prevxptr==NULL)
				ypts[bpt.y-miny]=newx;
			else
				prevxptr->next=newx;

			newx->next=xptr;

			/* btype=2 start and finish of an interval */
			if (btype==2) {
				newx2=(xptype *)Calloc(1,sizeof(xptype));
				*newx2=*newx;
				newx->next=newx2;
				nints++;
			}
		}

		lastbpt=bpt;
	}
	nints=nints/2;	/* nomber of intervals */

	/* free unit boundary */
	free(uvtx);			

	/* generate new object interval table */			
	intbase=(struct interval *)Calloc(nints,sizeof(struct interval));
	idom=(struct intervaldomain *)makedomain(1,miny,maxy,minx,maxx);
	idom->freeptr = (char *) intbase;
	obj=(struct object *)makemain(1,idom,NULL,NULL,NULL);					
		
	intvs=intbase;
	for (i=miny;i<=maxy;i++) {

		xptr=ypts[i-miny];

		nints=0;
		intbase=intvs;

		while (xptr!=NULL) {

			newx=xptr;
			left=xptr->x-minx;
			xptr=xptr->next;
			Free(newx);

			if (xptr!=NULL)
			{
				newx=xptr;
				right=xptr->x-minx;
				xptr=xptr->next;
				Free(newx);

				nints++;
				intvs->ileft=left;
				intvs->iright=right;
				intvs++;
			}
		}

		makeinterval(i,idom,nints,intbase);
	}
				
	/* free off interval endpoints table */	
	Free(ypts);

	/* return filled object */
	return(obj);
}


