/*
 * rastercanvas.cpp	M.Gregson
 *
 *	Routines to convert a canvas into a monochrome or RGB raster
 *	- used for filing or printing
 *
 *	Mods:
 *
 *	23Mar2000	JMB	Converted to .cpp, minor mods to reduce warnings,
 *					changed headers included.
 *	19Mar97	BP:		Complete rewrite for Windows. Single new function
 *					to draw image into a HBITMAP.
 *	BP	5/11/95:	Call XGetImage on chunks of 32 lines.
 */


#include "canvas.h"
#include "ddgs.h"


//#include "canvasdll.h"
//#include "/wincv/include/ddgs.h"


#include <stdio.h>
#include <memory.h>
//#include "ddgs.h"
//#include <common.h>
//#include <canvas.h>


/*
 * Create rectangular bitmap containing canvas image.
 */

HBITMAP canvas2bitmap(Canvas *canvas, DDGS *dog)
{
	HDC dc, memdc;
	DDGS *memdog;
	HBITMAP bmp;
	HGDIOBJ oldobj;
	int w, h, scale;
	int origx, origy;

	//w = dog->maxx + 1;
	//h = dog->maxy + 1;
	w = canvas->frame.width/8;
	h = canvas->frame.height/8;
	scale = canvas->frame.cpframe.scale;
	origx = canvas->frame.cpframe.dx;
	origy = canvas->frame.cpframe.dy;

	// Set to normal scale and origin
	canvas->frame.cpframe.scale = 8;
	canvas->frame.cpframe.dx = 0;
	canvas->frame.cpframe.dy = 0;
										
	// Create a new memory DC which we can
	// draw into invisibly.
	dc = GetDC(dog->window);

	if ((memdc = CreateCompatibleDC(dc)) == NULL)
	{
		ReleaseDC(dog->window, dc);
		MessageBox(dog->window, "Create DC failed", "canvas2bitmap error", MB_OK);
		return NULL;
	}
	if ((bmp = CreateCompatibleBitmap(dc, w, h)) == NULL)
	{
		ReleaseDC(dog->window, dc);
		MessageBox(dog->window, "Create Bitmap failed", "canvas2bitmap error", MB_OK);
		return NULL;
	}
	ReleaseDC(dog->window, dc);

	// Select the bitmap into the memory DC.
	oldobj = SelectObject(memdc, bmp);

	// Create a temporary dog, and draw the
	// canvas into it.
	memdog = ddgspropen(memdc, w, h);
	memdog->invertBG = 0x00000000;
	memdog->normalBG = 0x00ffffff;
	if (canvas->type == Ccomposite)
		memdog->invertBG = get_canvas_BG_col(canvas);

	selectddgs(memdog);
	intens(1);
	if (canvas->frame.drawmode == INVERT)
		invert_clear();
	else
		clear();
	display_Canvas(canvas);
	ddgsclose(memdog);

	// Restore scale
	canvas->frame.cpframe.scale = scale;
	canvas->frame.cpframe.dx = origx;
	canvas->frame.cpframe.dy = origy;

	// Finished with the DC.
	SelectObject(memdc, oldobj);
	DeleteDC(memdc);

	return bmp;
}
