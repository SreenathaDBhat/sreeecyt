/*
 *	M.Castling	7/7/92
 *
 *	These files were from ../mrcwoolz/woolzsrc and remove need
 *	for _DCalloc = calloc etc in Makefile. Not using original
 *	Calloc etc because to complex to change to work on UNIX PCs
 *
 * Mods:
 *
 *	29Jan97	BP:		WIN32 Malloc etc don't exist. Instead these are
 *					#define'd to malloc etc (proper debugger, so
 *					don't need this crap).
 *	Tidied up MG/DCB 21/7/93
 */

#include "woolz.h"

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <errno.h>


//extern int errno;

static FILE *maldebug=NULL;

#ifdef WIN32
void
init_maldebug(FILE *fp)
#endif
#ifdef i386
void
init_maldebug(fp)
FILE *fp;
#endif
{
#ifndef WIN32
	/* initialise malloc debug file pointer */
	maldebug=fp;
#endif
}

#ifdef WIN32
void
mal_comment(char *s)
#endif
#ifdef i386
void
mal_comment(s)
char *s;
#endif
{
#ifndef WIN32
	char *c;

	c=s;

	/* write a comment to malloc debug file */
	if (maldebug!=NULL) {
		/* replace spaces */
		while (*c!='\0') {
			if (*c==' ') 
				*c='_';
			c++;
		}
		fprintf(maldebug,"# %s\n",s);
	}
#endif
}


#ifndef WIN32

char *
Malloc(n)
int n;
{
	char *m;

	if( (m = (char *)malloc(n)) == NULL)
		printf("Malloc: failed to allocate %d bytes\n", n);

	if (maldebug!=NULL)
		fprintf(maldebug,"M %x\n",m);


	return(m);
	
}

char *
Calloc(n, s)
unsigned n;
unsigned s;
{
	char *c;

	if ((c = (char *)calloc(n, s)) == NULL)
		  printf("Calloc: failed to allocate %d bytes\n", n * s);

	if (maldebug!=NULL)
		fprintf(maldebug,"C %x\n",c);

	return(c);
}


char *
Realloc(m, n)
char *m;
unsigned int n;
{
	char *r;

	if( (r = (char *)realloc(m,n)) == NULL)
		printf("Realloc: failed to allocate %d bytes\n", n);

	if (maldebug!=NULL) {
		fprintf(maldebug,"F %x\n",m);
		fprintf(maldebug,"R %x\n",r);
	}


	return(r);
	
}


int
Free(x)
char *x;
{
	if (x==NULL)
		printf("Free: given a NULL pointer - refusing to free\n");
	else
	{
		if (maldebug!=NULL)
			fprintf(maldebug,"F %x\n",x);

	 	free(x);
	}
}


#endif
