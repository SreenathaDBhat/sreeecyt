//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by IdeogramEditor.rc
//
#define IDCOPY                          3
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_IDEOGRAMEDITOR_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDD_BANDPROPERTIES              129
#define IDD_IDEOPROPERTIES              130
#define IDD_SETPROPERTIES               131
#define IDD_NEWSETWIZ1                  132
#define IDD_NEWSETWIZ2                  136
#define IDD_SETLOAD                     137
#define IDD_NEWBAND                     139
#define IDB_BITBAND                     142
#define IDB_BITCLASS                    143
#define IDB_BITSET                      144
#define IDD_SETSAVE                     145
#define IDI_CGHTEMPLATE                 147
#define IDI_ICON2                       148
#define IDI_TEMPLATE                    149
#define IDI_IDEOSET                     150
#define IDI_SPECIES                     151
#define IDC_IMAGE                       1000
#define IDC_IDEOGRAM                    1011
#define IDC_RADIO_ADDBAND               1015
#define IDC_RADIO_REMOVEBAND            1017
#define IDC_RADIO_BAND_PROPERTIES       1019
#define IDC_RADIO_TYPE                  1020
#define IDC_RADIO2                      1021
#define IDC_RADIO_HETROCHROMATIC        1021
#define IDC_RADIO_HETEROCHROMATIC       1021
#define IDC_RADIO3                      1022
#define IDC_RADIO_STALK                 1022
#define IDC_EDIT_NAME                   1023
#define IDC_RADIO4                      1025
#define IDC_RADIO_CENTROMERIC           1025
#define IDC_RADIO1                      1026
#define IDC_RADIO_SATELLITE             1026
#define IDC_BUTTON_COLOUR               1027
#define IDC_EDIT_IDEO_ID                1028
#define IDC_EDIT_SET_NAME               1029
#define IDC_BUTTON_IDEO_NEXT            1030
#define IDC_BUTTON_IDEO_DELETE          1031
#define IDC_BUTTON_IDEO_PROPERTIES      1032
#define IDC_BUTTON4                     1033
#define IDC_BUTTON_IDEO_NEW             1033
#define IDC_BUTTON_SET_RESTORE          1034
#define IDC_BUTTON_SET_PROPERTIES       1035
#define IDC_BUTTON7                     1036
#define IDC_BUTTON_SET_SAVE             1036
#define IDC_BUTTON8                     1037
#define IDC_BUTTON_SET_DELETE           1037
#define IDC_BUTTON_SET_NEW              1038
#define IDC_BUTTON_SET_LOAD             1039
#define IDC_BUTTON1                     1042
#define IDC_BUTTON_INSERT               1042
#define IDC_BUTTON_APPLY                1042
#define IDC_BUTTON_SAVEAS               1042
#define IDC_BUTTON_UNDO                 1042
#define IDC_SCROLLBAR1                  1043
#define IDC_IDEOEDITDA                  1045
#define IDC_IDEOEDIT                    1045
#define IDC_EDIT2                       1047
#define IDC_EDIT_ID                     1047
#define IDC_EDIT_BAND_NAME              1047
#define IDC_EDIT3                       1048
#define IDC_EDIT_RELATIVELENGTH         1048
#define IDC_CHECK_SHOWNAME              1049
#define IDC_BUTTON_BAND_NEW             1052
#define IDC_BUTTON_BAND_DELETE          1053
#define IDC_BUTTON_BAND_PROPERTIES      1054
#define IDC_EDIT_SPECIES                1056
#define IDC_EDIT_WIDTH                  1057
#define IDC_EDIT_RESOLUTION             1058
#define IDC_EDIT_BANDINGTYPE            1059
#define IDC_EDIT_LENGTH                 1060
#define IDC_NEWSETWIZ1_TREE             1061
#define IDC_NEWSETWIZ1_EDIT             1062
#define IDC_NEWSETWIZ2_EDIT_SPECIES     1063
#define IDC_SETLOAD_TREE                1064
#define IDC_NEWSETWIZ2_TREE             1065
#define IDC_NEWSETWIZ2_EDIT_NAME        1067
#define IDC_COMBO_SEX                   1069
#define IDC_SPIN_AUTOSOME               1070
#define IDC_EDIT_AUTOSOME               1071
#define IDC_COMBO_IDEO                  1072
#define IDC_RADIO_AUTOSOME              1073
#define IDC_RADIO_SEX                   1074
#define IDC_SPIN_LENGTH                 1075
#define IDC_COLOURBOX                   1076
#define IDC_RADIO_BAND                  1077
#define IDC_RADIO_CENTROMERE            1078
#define IDC_RADIO_ABOVE                 1079
#define IDC_RADIO_BELOW                 1080
#define IDC_SPIN_BAND                   1081
#define IDC_COMBO_ZOOM                  1084
#define IDC_BUTTON_BLACK                1085
#define IDC_BUTTON_WHITE                1086
#define IDC_BUTTON_UP                   1088
#define IDC_BUTTON_DOWN                 1089
#define IDC_COMBO1                      1090

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        152
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1091
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
