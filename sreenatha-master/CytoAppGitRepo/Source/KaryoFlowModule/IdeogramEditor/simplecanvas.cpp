/*
 *	simplecanvas.cpp	M.Gregson	12/2/93
 *
 *	convenience routines for creating basic canvases
 *
 *	Modifications:
 *
 *	23Mar2002	MC	Added missing 'Wobj' NULL check for create_object_Canvas()
 *					
 *	23Mar2000	JMB	Converted to .cpp, minor mods to reduce warnings,
 *					changed headers included.
 *	11/26/96	BP:	Windows version.
 *					Manually add create_main_Canvas and
 *					create_grid_Canvas to public header.
 *
 *	 9 Mar 1995	MG	added ipoint and fpoint to create_object_Canvas()
 *	 4 Mar 1993	MG	added drawmode to canvas creation routines
 */


#include "canvas.h"
#include "ddgs.h"
#include "woolz.h"


//#include "canvasdll.h"
//#include "/wincv/include/ddgs.h"
//#include "/wincv/include/woolz.h"


#include <stdio.h>
//#include "ddgs.h"
//#include <wstruct.h>
//#include <canvas.h>


/*
 *	CREATE_MAIN_CANVAS
 *
 *	create very basic canvas specifying only origin, width and height
 */
#ifdef WIN32
Canvas *
create_main_Canvas(short type, int dx, int dy, int width, int height)
#endif
#ifdef i386
Canvas *
create_main_Canvas(type, dx, dy, width, height)
short type;
int dx,dy,width,height;
#endif
{
	Cframe cf;

	cf.aspect=100;
	cf.cpframe.scale=8;
	cf.cpframe.dx=dx;
	cf.cpframe.dy=dy;
	cf.cpframe.ox=0;
	cf.cpframe.oy=0;
	cf.cpframe.ix=1;
	cf.cpframe.iy=1;
	cf.parent_site=FREE;
	cf.posingrid=0;
	cf.width=width;
	cf.height=height;
	cf.clip=0;
	cf.arena=GREYIM;
	cf.colour=255;
	cf.drawmode=COPY;

	return(create_Canvas(type,&cf,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL));
}


/*
 *	CREATE_GRID_CANVAS
 *
 *	create canvas with patchgrid 
 *	if vertical=1 vertical grid else horizontal
 *	spacing specifies patch canvas spacing
 */
#ifdef WIN32
Canvas *
create_grid_Canvas(short type, int dx, int dy, int width, int height, int vertical, int spacing)
#endif
#ifdef i386
Canvas *
create_grid_Canvas(type, dx, dy, width, height, vertical, spacing)
short type;
int dx,dy,width,height;
int vertical;
int spacing;
#endif
{
	Canvas *canvas;

	canvas=create_main_Canvas(type, dx, dy, width, height);

	canvas->patchgrid.spacing=spacing;
	canvas->patchgrid.style=vertical;

	return(canvas);
}

//
//	Find x,y limits of a polygon
//
void polylimits(struct polygondomain *pdom, int *minx, int *maxx, int *miny, int *maxy)
{
	int i;
	struct fvertex *fverts;
	struct ivertex *iverts;

	if (!pdom)
		return;

	switch (pdom->type) {
	case 1 :
		iverts = pdom->vtx;
		*minx = *maxx=iverts->vtX;
		*miny = *maxy=iverts->vtY;
		for (i=0;i<pdom->nvertices;i++) {
			if (iverts->vtX < *minx) *minx = iverts->vtX;
			else if (iverts->vtX > *maxx) *maxx = iverts->vtX;
			if (iverts->vtY < *miny) *miny = iverts->vtY;
			else if (iverts->vtY > *maxy) *maxy = iverts->vtY;
			iverts++;
		}
		break;

	case 2 :
		fverts =(struct fvertex *) pdom->vtx;
		*minx = *maxx = (int)(fverts->vtX);
		*miny = *maxy = (int)(fverts->vtY);
		for (i=0;i<pdom->nvertices;i++) {
			if (fverts->vtX < *minx) *minx = (int)(fverts->vtX);
			else if (fverts->vtX > *maxx) *maxx = (int)(fverts->vtX);
			if (fverts->vtY < *miny) *miny = (int)(fverts->vtY);
			else if (fverts->vtY > *maxy) *maxy = (int)(fverts->vtY);
			fverts++;
		}
	}
}


//
//	Find x,y limits of a boundlimts - should only be called by boundlimits()
//
void recursive_boundlimits(struct boundlist *bound, int *minx, int *maxx, int *miny, int *maxy)
{
	int pminx, pmaxx, pmaxy, pminy;

	if (!bound)
		return;

	do {
		polylimits(bound->poly, &pminx, &pmaxx, &pminy, &pmaxy);

		if (pminx < *minx) *minx = pminx;
		if (pmaxx > *maxx) *maxx = pmaxx;
		if (pminy < *miny) *miny = pminy;
		if (pmaxy > *maxy) *maxy = pmaxy;

		recursive_boundlimits(bound->down, minx, maxx, miny, maxy);

	} while((bound = bound->next) != NULL);
}


//
//	Find x,y limits of a boundlist
//
void boundlimits(struct boundlist *bound, int *minx, int *maxx, int *miny, int *maxy)
{
	if (!bound)
		return;

	// initialise limits to limits of top or first poly in boundlist
	polylimits(bound->poly, minx, maxx, miny, maxy);

	// drop down tree updating limits
	recursive_boundlimits(bound, minx, maxx, miny, maxy);
}



/*
 *	CREATE_OBJECT_CANVAS
 *
 *	creates a canvas for any woolz object assumes FREE parent_site and not
 *	in grid, calculates canvas frame size to hold object
 */

#ifdef WIN32
Canvas *
create_object_Canvas(short type, int dx, int dy, struct object *Wobj, short arena, short colour, short drawmode)
#endif
#ifdef i386
Canvas *
create_object_Canvas(type, dx, dy, Wobj, arena, colour, drawmode)
short type;
int dx;
int dy;
struct object *Wobj;
short arena;
short colour;
short drawmode;
#endif
{
	Canvas *canvas;
	int width,height;
	struct intervaldomain *idom;
	struct polygondomain *pdom;
	struct boundlist *bound;
	struct histogramdomain *h;
	struct irect *rdom;
	struct frect *fdom;
	struct ivector *ivec;
	struct fvector *fvec;
	struct textobj *textobj;
	struct circle *circ;
	struct ipoint *ipt;
	struct fpoint *fpt;
	int minx,maxx,miny,maxy,i,maxval;
	int *iv;
	float *fv;

	if (Wobj == NULL)
		return (NULL);

	idom=Wobj->idom;

	switch (Wobj->type) {
	case 0:		/* interval/grey-table object */
	case 1:
		width=(idom->lastkl - idom->kol1 + 1) * 8;
		height=(idom->lastln - idom->line1 + 1) * 8;
		canvas=create_main_Canvas(type, dx, dy, width, height);
		canvas->frame.cpframe.ox=idom->kol1;
		canvas->frame.cpframe.oy=idom->line1;
		break;

	case 11 :	/* boundary list */
		bound=(struct boundlist *)idom;
		boundlimits(bound, &minx, &maxx, &miny, &maxy);
		width=(maxx - minx + 1) * 8;
		height=(maxy - miny + 1) * 8;
		canvas=create_main_Canvas(type, dx, dy, width, height);
		canvas->frame.cpframe.ox=minx;
		canvas->frame.cpframe.oy=miny;
		break;
		
	case 10 :	/* polygon */
	case 12 :	/* convexhull */
		pdom=(struct polygondomain *)idom;
		polylimits(pdom, &minx, &maxx, &miny, &maxy);
		width=(maxx - minx + 1) * 8;
		height=(maxy - miny + 1) * 8;
		canvas=create_main_Canvas(type, dx, dy, width, height);
		canvas->frame.cpframe.ox=minx;
		canvas->frame.cpframe.oy=miny;
		break;


	case 13:	/* histogram */
		h=(struct histogramdomain *)idom;
		switch (h->type) {
		default:
		case 1:			/* integer values */
			iv=h->hv;
			maxval=0;
			for (i=0; i<h->npoints; i++) {
				if ((*iv)>maxval) maxval=(*iv);
				iv++;
			}
			break;
		case 2:			/* float values */
			fv = (float *) h->hv;
			maxval=0;
			for (i=0; i<h->npoints; i++) {
				if ((*fv)>maxval) maxval = (int)(*fv);
				fv++;
			}
			break;
		}
		switch (h->r) {
		default:
		case 0:		/* vertical */
			height=(h->npoints)*8;
			width=(maxval+1)*8;
			break;
		case 1:		/* horizontal */
			width=(h->npoints)*8;
			height=(maxval+1)*8;
			break;
		}
		canvas=create_main_Canvas(type, dx, dy, width, height);
		canvas->frame.cpframe.ox=h->k;
		canvas->frame.cpframe.oy=h->l;
		break;
		

	case 20 :	/* rectangle - vertices specified */
		rdom=(struct irect *)idom;
		switch(rdom->type) {
		default:
		case 1 :		/* integer values */
			minx=maxx=rdom->irk[0];
			miny=maxy=rdom->irl[0];
			for (i=1; i<=4; i++) {
				if (rdom->irk[i]<minx) minx=rdom->irk[i];
				else if (rdom->irk[i]>maxx) maxx=rdom->irk[i];
				if (rdom->irl[i]<miny) miny=rdom->irl[i];
				else if (rdom->irl[i]>maxy) maxy=rdom->irl[i];
			}
			break;
		case 2:			/* float values */
			fdom = (struct frect *) rdom;
			minx = maxx = (int)(fdom->frk[0]);
			miny = maxy = (int)(fdom->frl[0]);
			for (i=1; i<=4; i++) {
				if (fdom->frk[i]<minx) minx = (int)(fdom->frk[i]);
				else if (fdom->frk[i]>maxx) maxx = (int)(fdom->frk[i]);
				if (fdom->frl[i]<miny) miny = (int)(fdom->frl[i]);
				else if (fdom->frl[i]>maxy) maxy = (int)(fdom->frl[i]);
			}
			break;
		}
		width=(maxx - minx + 1) * 8;
		height=(maxy - miny + 1) * 8;
		canvas=create_main_Canvas(type, dx, dy, width, height);
		canvas->frame.cpframe.ox=minx;
		canvas->frame.cpframe.oy=miny;
		break;

	case 30 :	/* vectors */
		ivec=(struct ivector *)Wobj;
		minx=(ivec->k2<ivec->k1) ? ivec->k2 : ivec->k1;
		maxx=(ivec->k2>ivec->k1) ? ivec->k2 : ivec->k1;
		miny=(ivec->l2<ivec->l1) ? ivec->l2 : ivec->l1;
		maxy=(ivec->l2>ivec->l1) ? ivec->l2 : ivec->l1;
		width=(maxx - minx + 1) * 8;
		height=(maxy - miny + 1) * 8;
		canvas=create_main_Canvas(type, dx, dy, width, height);
		canvas->frame.cpframe.ox=minx;
		canvas->frame.cpframe.oy=miny;
		break;

	case 31 :
		fvec=(struct fvector *)Wobj;
		minx = (int)((fvec->k2<fvec->k1) ? fvec->k2 : fvec->k1);
		maxx = (int)((fvec->k2>fvec->k1) ? fvec->k2 : fvec->k1);
		miny = (int)((fvec->l2<fvec->l1) ? fvec->l2 : fvec->l1);
		maxy = (int)((fvec->l2>fvec->l1) ? fvec->l2 : fvec->l1);
		width=(maxx - minx + 1) * 8;
		height=(maxy - miny + 1) * 8;
		canvas=create_main_Canvas(type, dx, dy, width, height);
		canvas->frame.cpframe.ox=minx;
		canvas->frame.cpframe.oy=miny;
		break;

	case 40 :	/* points */
		/* create canvas to centre cross at given dx,dy */
		ipt=(struct ipoint *)Wobj;
		dx=dx-4*8;
		dy=dy-4*8;
		canvas=create_main_Canvas(type, dx, dy, 9*8, 9*8);
		canvas->frame.cpframe.ox=ipt->k-4;
		canvas->frame.cpframe.oy=ipt->l-4;
		break;
		
	case 41 :
		/* create canvas to centre cross at given dx,dy */
		fpt=(struct fpoint *)Wobj;
		dx=dx-4*8;
		dy=dy-4*8;
		canvas=create_main_Canvas(type, dx, dy, 9*8, 9*8);
		canvas->frame.cpframe.ox = (short)(fpt->k-4);
		canvas->frame.cpframe.oy = (short)(fpt->l-4);
		break;
		
	case 70 :	/* text object */
		textobj=(struct textobj *)Wobj;
		setfont(textobj->tdom->font);
		textsize(textobj->text,&width,&height);
		canvas=create_main_Canvas(type, dx, dy, width, height);
		break;
	
	case 100:	/* circle object */
		circ=(struct circle *)Wobj;
		minx=circ->centre.vtX - circ->radius;
		miny=circ->centre.vtY - circ->radius;
		height=width=(circ->radius*2+1)*8;
		canvas=create_main_Canvas(type, dx, dy, width, height);
		canvas->frame.cpframe.ox=minx;
		canvas->frame.cpframe.oy=miny;
		break;
		
	default:
		break;
	}

	canvas->Wobj=Wobj;
	canvas->frame.arena=arena;
	canvas->frame.colour=colour;
	canvas->frame.drawmode=drawmode;

	return(canvas);
}


/*
 *	CREATE_OBJECT_IN_GRID_CANVAS
 *
 *	creates a canvas for a woolz object and sets its position in the parent 
 *	canvas grid and its parent_site e.g. GRID|TOP|LEFT
 */
#ifdef WIN32
Canvas *
create_object_in_grid_Canvas(short type, int dx, int dy, struct object *Wobj, short arena, short colour, short drawmode, short posingrid, short parent_site)
#endif
#ifdef i386
Canvas *
create_object_in_grid_Canvas(type, dx, dy, Wobj, arena, colour, drawmode, posingrid, parent_site)
short type;
int dx;
int dy;
struct object *Wobj;
short arena;
short colour;
short drawmode;
short posingrid;
short parent_site;
#endif
{
	Canvas *canvas;

	canvas=create_object_Canvas(type, dx, dy, Wobj, arena, colour, drawmode);

	canvas->frame.posingrid=posingrid;		/* position in grid */
	canvas->frame.parent_site=parent_site|GRID;	/* modified position */

	return(canvas);
}



