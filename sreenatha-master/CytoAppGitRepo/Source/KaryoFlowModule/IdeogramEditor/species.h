/*****************************************************************************
               Copyright (c) 1999 Applied Imaging International 
 
	Source file:    species.h
 
	Function        Header file for WinCV,
					use wherever species string to be used.

	Package:        WinCV

	Modification history:
	Author      Date        Description
	SN          03Jun99     Initial implementation

Mods:
	10Aug2000	JMB	Added LOCALIDEOSETFILENAME.

 ****************************************************************************/

#ifndef SPECIES_H
#define SPECIES_H

/** MACROS ******************************************************************/

#define CLSPECIESNAMELENGTH		80				// Length of species string
												// MUST agree with limit set in TemplateGenerator class wizard.

#define CLSPECIESDEFAULTSTRING	"Human"			// Default species string literal

#define TEMPLATEDIR				"Templates"		// Template share name

#define KTEMPLATEFILENAME		"\\local.ktmpl"	// Filename of local cell karyotype template
#define CTEMPLATEFILENAME		"\\local.ctmpl"	// Filename of local cell cgh template

#define LOCALIDEOSETFILENAME	"\\local.ideoset"	// Filename of local ideogram set (actual name held in template).


/** TYPEDEFS, STRUCTS *******************************************************/

/** PUBLIC FUNCTION PROTOTYPES **********************************************/

/** PUBLIC (extern) DECLARATIONS ********************************************/


#endif
/*****************************************************************************
               Copyright (c) 1999 Applied Imaging International 
 ****************************************************************************/
