/*
 * stubs.cpp
 *
 * The Ideogram Editor uses the TemplateGenerator library from WinCV
 * (TempGen.lib). This library references code in other WinCV libraries, which
 * therefore also need to be linked to. However, some of these references are
 * not used for our purposes, or are not used by the TemplateGenerator library
 * at all (they come from code which shares the same source files in the other
 * libraries as code which is used). To minimise the number of other libraries
 * that need to be linked to, some of the unused references are resolved by
 * including them in this file, as stub functions.
 * Libraries which still need to linked to to resolve references brought in by
 * linking to tempgen.lib are: cvcontrol.lib common.lib casebase.lib lms.lib
 *                             ISCFS.lib
 */


#include "windows.h"

/*
 * Cytovision\MacroHotkey.cpp
 */
unsigned int  CVMacroNumber = 99;
unsigned int  CVMacroFlags = 99;

char const * _cdecl MacroFilePrefix(void)
{
	return "A";
}



// Functions whose real versions are defined in C sources or whose prototypes
// are defined as extern "C" in their headers must be wrapped in extern "C" so
// that their naming convention appears the same to the linker as the real 
// functions.

extern "C"
{

int captureMode;

/*
 * cvadmin/db_admin.cpp
 */
BOOL clear_group_unless_modified(int groupID)
{
	return FALSE;
}


/*
 * cgh/cghcanvas.cpp
 */
typedef struct
{
	int dummy;
} Canvas;

void
create_ratgraph_axes(Canvas *ratgraph, float minratio, float maxratio, int show_axes_labels)
{
}


/*
 * fcadmin
 */

typedef struct
{
	int dummy;
} FCcontrol;

int fccont_save(FCcontrol *cont, char *name)
{
	return -1;
}

FCcontrol *fccont_create()
{
	return NULL;
}

FCcontrol *fccont_load(char *name)
{
	return NULL;
}

void fccont_clear(FCcontrol *cont)
{
}


BOOL StartManualSpotCounting(BOOL LaunchCVScanApp)
{
	return FALSE;
}

void admin_SelectPanel(void *pcont, int mode, BOOL show, int type)
{
	return;
}

void get_fluolist_from_cell(char *cellname)
{
	return;
}

void WINAPI UpdateCaptureSelectPanel(int type)
{
	return;
}

int get_slide_type_from_nav()
{
	return 1;
}

} // extern "C"

