
/*
 * textimage.c    Dr. Bill
 */

#include "woolz.h"



#include <stdio.h>




#define NTEXTFONTS	10

static char *text_fonts[NTEXTFONTS];

char *text_font_names[NTEXTFONTS] = {
	"helv.med.11",
	"helv.med.14",
	"helv.med.20",
	"helv.med.25",
	"helv.med.34",
	"helv.bold.11",
	"helv.bold.14",
	"helv.bold.20",
	"helv.bold.25",
	"helv.bold.34"
};




/**************************************************** load_text_fonts */

/*
 * Load the text fonts from file.
 */

#ifdef WIN32
int
load_text_fonts()
#endif
#ifdef i386
int
load_text_fonts()
#endif
{
	FILE *f;
	int i, size;


	for (i = 0; i < NTEXTFONTS; i++)
	{
		if ((f = fopen(text_font_names[i], "rb")) == NULL)
		{
			printf("Error opening font file %s.\n",
							text_font_names[i]);
			exit(1);
		}

		fseek(f, 0, SEEK_END);
		size = ftell(f);
		rewind(f);

		text_fonts[i] = (char *)malloc(size);

		fread(text_fonts[i], 1, size, f);
	}

	return(1);
} /* load_text_fonts */




/**************************************************** create_bitmap_text */

/*
 * Draw a text string into a rectangular
 * char buffer.
 * The size of the buffer is calculated
 * here and returned through outw & outh.
 * A pointer to the buffer (which is
 * allocated here) is returned and must
 * be freed when finished with.
 */

#ifdef WIN32
char *
create_bitmap_text(char *str, int *outw, int *outh, char colour, char bg, int font)
#endif
#ifdef i386
char *
create_bitmap_text(str, outw, outh, colour, bg, font)
char *str;
int *outw;
int *outh;
char colour;
char bg;
int font;
#endif
{
	char *outdata, *outtemp, *outtempsave, *c;
	char *fontptr, *fonttemp;
	int *offsetptr;
	register char thisbit;
	register short i, j, w, h, totw = 0, toth = 0;


	if ((font < 0) ||
	    (font >= NTEXTFONTS))
		fontptr = text_fonts[1];
	else
		fontptr = text_fonts[font];

	c = str;

	while (*c != 0)
	{
		offsetptr = (int *)fontptr + (*c & 127);
		fonttemp = fontptr + *offsetptr;

		w = *fonttemp++;
		h = *fonttemp++;

		if (h > toth)
			toth = h;

		totw += w;

		c++;
	}

	*outw = totw;
	*outh = toth;

	if (totw*toth < 1)
		return(NULL);

	outdata = (char *)malloc(totw*toth);

	memset(outdata, bg, totw*toth);

	outtempsave = outdata;

	c = str;

	while (*c != 0)
	{
		offsetptr = (int *)fontptr + (*c & 127);
		fonttemp = fontptr + *offsetptr;

		w = *fonttemp++;
		h = *fonttemp++;
		thisbit = 1;

		for (j = 0; j < h; j++)
		{
			outtemp = outtempsave + (totw*j);

			for (i = 0; i < w; i++)
			{
				if (*fonttemp & thisbit)
					*outtemp = colour;

				outtemp++;

				thisbit = thisbit << 1;
				if (thisbit == 0)
				{
					thisbit = 1;
					fonttemp++;
				}
			}
		}

		outtempsave += w;
		c++;
	}

	return(outdata);
} /* create_bitmap_text */

