
/*
 *	vdom11to1 	Mike Castling	29/6/93
 *
 *	Convert object with type 11 vdom to object with type 1 vdom
 *
 * Mods:
 *
 *	10Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *					Also, made minor mods to stop compiler warnings and removed
 *					standard C parameter lists (were '#ifdef i386' only).
 */

#include "woolz.h"

#include <stdio.h>
#include <malloc.h>
//#include <wstruct.h>



void
vdom11to1(struct object *obj)
{
	int i, kstart, vtblinearea;
	struct iwspace iwsp;
	struct gwspace gwsp;
	struct valuetable *oldvdom, *newvdom, *makevaluetb();
	GREY /*bckgrnd,*/ *v;
	register GREY *newg, *oldg;

	/* protect against silly objects */
	if (obj == NULL)
		{
		return;
		}

	if (obj->vdom == NULL)
		{
		return;
		}

	if (obj->vdom->type != 11)
		{
		return;
		}

	
	oldvdom = obj->vdom;
	obj->vdom = NULL;

	/* generate new type 1 vdom */
	newvdom = makevaluetb(1, obj->idom->line1, 
				obj->idom->lastln,
				oldvdom->bckgrnd,
				obj);
	obj->vdom = newvdom;

	vtblinearea = linearea(obj);

	if (vtblinearea > 0)
		{
		v = (GREY *) Malloc(vtblinearea * sizeof(GREY));
		newvdom->linkcount = 1;
		}
	else 
		{
		v = NULL;
		newvdom->linkcount = 0;
		}

	newvdom->freeptr = (char *)v;

	obj->vdom = oldvdom;

	initgreyscan(obj,&iwsp,&gwsp);
	while(nextgreyinterval(&iwsp) == 0)
		{
		if (iwsp.nwlpos)
			kstart = iwsp.lftpos;

		newg = v+iwsp.lftpos-kstart;

		oldg = gwsp.grintptr;

		for (i=0; i<iwsp.colrmn; i++) {
			*newg++ = *oldg++;
		}

		if (iwsp.intrmn == 0) {
			makevalueline(newvdom, iwsp.linpos, kstart, iwsp.rgtpos, v);
			v += (iwsp.rgtpos - kstart + 1);
		}
	}

	obj->vdom=newvdom;

	freevaluetable((struct intervaldomain *)oldvdom);
}	