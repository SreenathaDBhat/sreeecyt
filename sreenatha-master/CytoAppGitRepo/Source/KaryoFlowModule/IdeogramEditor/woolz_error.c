/*
 *      W O O L Z _ E R R O R        Error exit defined by user
 *
 *
 *  Written: Graham John Page
 *           Image Recognition Systems
 *           720 Birchwood Boulevard
 *           Birchwood Science Park
 *           Warrington
 *           Cheshire
 *           WA3 7PX
 *
 *	Copyright (c) and intellectual property rights Image Recognition Systems
 *
 *  Date:    3rd March 1987
 *
 *  Modifications
 *
 *	29 Jul 1992		MC		inserted missing %s in woolz_exit 
 *
 *	 1 Dec 1988		CAS		woolz_exit really exits if no user routine setup
 *							Can't use woolzsig generally - most programs don't
 *							know aboutit..
 *	22 Nov 1988		dcb		woolz_exit now has an additional parameter to
 *							indicate where it was called from. Note that:
 *							1) woolz_exit must be called with this parameter
 *							2) The woolz exit function must also cope
 *	18 Nov 1988		SCG		modified woolz_exit to return the error status
 *	18 Nov 1988		dcb		woolz_exit() outputs message string, & kills process
 *							with signal WOOLZSIG, instead of just exiting.
 */

#include "woolz.h"


#include <stdio.h>
//# include <wtcheck.h>
#include "wtcheck.h"

static int (*woolz_error)() = NULL ;

/*
 *
 *		S E T _ W O O L Z _ E X I T
 *
 */
#ifdef WIN32
void
set_woolz_exit(int (*exit_fn)())
#endif
#ifdef i386
void
set_woolz_exit(exit_fn)
int (*exit_fn)();
#endif
{
	woolz_error=exit_fn;
}

/*
 *
 *		W O O L Z _ E X I T
 *
 */
#ifdef WIN32
int
woolz_exit(int err_no, char *called_from)
#endif
#ifdef i386
int
woolz_exit(err_no, called_from)
int err_no;
char *called_from;
#endif
{
	if (woolz_error == NULL) {
		fprintf(stdout, "Woolz error %s, called from %s\n",
			woolz_err_msg(err_no), called_from);
		exit(err_no);
	} else {
		return(woolz_error(err_no, called_from));
	}
}

