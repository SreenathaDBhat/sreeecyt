﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Windows;
using System.Xml.Linq;

namespace AI
{
    public class AdaptiveBackgroundSubtractionFilterTemplate : IRenderPassTemplate
	{
		private static readonly Guid id = new Guid("60926727-10DD-46f0-A7D3-C0A8BD0A9BD3");
		private AdaptiveBackgroundSubtractionFilterParameters parms;

		public AdaptiveBackgroundSubtractionFilterTemplate()
		{
			parms = new AdaptiveBackgroundSubtractionFilterParameters();
		}

        public RenderPass CreatePass(IntPtr device, ImageFrame image, ImageFrameRenderer r, int textureBpp)
		{
			return new AdaptiveBackgroundSubtractionFilter(parms, this, device, image.ImageWidth, image.ImageHeight);
		}

		public Guid Id
		{
			get { return id; }
		}

		public string DisplayName
		{
            get { return "Subtract Background"; }
		}

		public UIElement UI
		{
            get
            {
                return null;
            }
            set
            {
            }
		}

        public event PropertyChangedEventHandler PropertyChanged;
        bool isActive;
        public bool IsActive
        {
            get { return isActive; }
            set
            {
                isActive = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("IsActive"));
            }
        }

		public void FromXml(XElement filterNode)
		{
			// Read in filter parameters here
		}

		public void ToXml(XElement filterNode)
		{
			// Write filter parameters here
		}
	}
}
