﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Windows;
using System.Xml.Linq;

namespace AI
{
    public class BackgroundSubtractionFilterTemplate : IRenderPassTemplate
	{
        private static readonly Guid id = new Guid("ADDF0032-6539-45e7-83DA-E0C32456743C");
        private BackgroundSubtractionFilterParameters parms;
		public event PropertyChangedEventHandler PropertyChanged;

		public BackgroundSubtractionFilterTemplate()
        {
            parms = new BackgroundSubtractionFilterParameters();
        }

		public RenderPass CreatePass(IntPtr device, ImageFrame image, ImageFrameRenderer r, int textureBpp)
        {
            return new BackgroundSubtractionFilter(parms, this, device, image.ImageWidth, image.ImageHeight);
        }

        bool isActive;
        public bool IsActive
        {
            get { return isActive; }
            set 
            {
                isActive = value; 
                if(PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("IsActive"));
            }
        }

		public int Passes
		{
			get
			{
				return parms.Passes;
			}
			set
			{
				parms.Passes = value;
               
                if(PropertyChanged != null)
				    PropertyChanged(this, new PropertyChangedEventArgs("Passes"));
			}
		}

        public Guid Id
        {
            get { return id; }
        }

        public string DisplayName
        {
            get { return "Subtract Background"; }
        }

        public UIElement UI
        {
            get { return new BackgroundSubtractionFilterUi(); }
            set { }
        }

        public void FromXml(XElement filterNode)
        {
            Passes = Parsing.TryParseInt(Parsing.ValueOrDefault(filterNode.Attribute("Passes"), "4"), 4);
        }

        public void ToXml(XElement filterNode)
        {
            filterNode.Add(new XAttribute("Passes", Passes));
        }
	};
}
