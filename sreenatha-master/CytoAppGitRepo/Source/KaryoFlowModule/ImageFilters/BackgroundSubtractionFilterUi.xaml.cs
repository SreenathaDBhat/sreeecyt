﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace AI
{
    public partial class BackgroundSubtractionFilterUi : UserControl
    {
        static BackgroundSubtractionFilterUi()
        {
            DataContextProperty.OverrideMetadata(typeof(BackgroundSubtractionFilterUi), new FrameworkPropertyMetadata(OnDataContextChanged));
        }

        public BackgroundSubtractionFilterUi()
        {
            InitializeComponent();
        }

        private static void OnDataContextChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            BackgroundSubtractionFilterUi t = (BackgroundSubtractionFilterUi)sender;

            BackgroundSubtractionFilterTemplate bg = e.NewValue as BackgroundSubtractionFilterTemplate;
            if (bg == null)
                return;

            switch (bg.Passes)
            {
                case 2:
                    t.bgRadio0.IsChecked = true;
                    break;

                case 4:
                    t.bgRadio1.IsChecked = true;
                    break;

                case 10:
                    t.bgRadio2.IsChecked = true;
                    break;

                case 18:
                    t.bgRadio3.IsChecked = true;
                    break;

                case 40:
                    t.bgRadio4.IsChecked = true;
                    break;
            }
        }

        private void FireInvalidateFilter()
        {
            if (ImageRenderParameters.InvalidateFilters.CanExecute(null, this))
                ImageRenderParameters.InvalidateFilters.Execute(null, this);
        }

        private void OnBackgroundSubtraction0(object sender, System.Windows.RoutedEventArgs e)
        {
            ((BackgroundSubtractionFilterTemplate)DataContext).Passes = 2;
            FireInvalidateFilter();
        }

        private void OnBackgroundSubtraction1(object sender, System.Windows.RoutedEventArgs e)
        {
            ((BackgroundSubtractionFilterTemplate)DataContext).Passes = 4;
            FireInvalidateFilter();
        }

        private void OnBackgroundSubtraction2(object sender, System.Windows.RoutedEventArgs e)
        {
            ((BackgroundSubtractionFilterTemplate)DataContext).Passes = 10;
            FireInvalidateFilter();
        }

        private void OnBackgroundSubtraction3(object sender, System.Windows.RoutedEventArgs e)
        {
            ((BackgroundSubtractionFilterTemplate)DataContext).Passes = 18;
            FireInvalidateFilter();
        }

        private void OnBackgroundSubtraction4(object sender, System.Windows.RoutedEventArgs e)
        {
            ((BackgroundSubtractionFilterTemplate)DataContext).Passes = 40;
            FireInvalidateFilter();
        }
    }
}
