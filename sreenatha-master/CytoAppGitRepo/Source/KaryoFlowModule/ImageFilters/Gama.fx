float4x4 worldViewProj : WorldViewProjection;
texture source;
float onePixelX;
float onePixelY;
float maxPixelValue;
float gamma;
float bitsPerPixelMultiplier;

sampler sourceSampler = sampler_state
{
	Texture   = (source);
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

struct VS_INPUT
{
    float3 position	: POSITION;
	float2 texture0 : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4 hposition : POSITION;
	float2 texture0  : TEXCOORD0;
};

struct PS_OUTPUT
{
	float4 color : COLOR;
};


VS_OUTPUT vs(VS_INPUT IN)
{
    VS_OUTPUT OUT;

	OUT.hposition = mul(worldViewProj, float4(IN.position, 1));
	OUT.texture0 = IN.texture0 + float2(onePixelX/2, onePixelY/2);

	return OUT;
}

PS_OUTPUT ps(VS_OUTPUT IN)
{
	float s = tex2D(sourceSampler, IN.texture0).r * bitsPerPixelMultiplier;
	s = pow(s, gamma);
	s = max(0, min(1, s));
	
    PS_OUTPUT o = (PS_OUTPUT)0;
    o.color = s / bitsPerPixelMultiplier;
    o.color.a = 1;
    return o;
}

technique T0
{
    pass Pass0
    {
		Lighting = FALSE;

		VertexShader = compile vs_2_0 vs();
		PixelShader  = compile ps_2_0 ps();
    }
}