﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using System.ComponentModel;
using System.Windows.Data;

namespace AI
{
    public class GammaFilter : FilterBase, INotifyPropertyChanged
    {
        private double gamma;
        private ImageFrame image;

        public GammaFilter(object parent, IntPtr device, ImageFrame image, int textureBpp)
            : base("Gama.fx", parent, device, image.ImageWidth, image.ImageHeight)
        {
            this.image = image;
            this.gamma = image.Gamma;
            SetFloat("gamma", 1 / gamma);
            SetFloat("bitsPerPixelMultiplier", 65536.0f / Math.Pow(2.0f, (float)textureBpp));
            SetTechnique("T0");
        }

        public override bool RunsOn(ChannelInfo channelInfo)
        {
            return true;
        }

        public double Gamma
        {
            get { return gamma; }
            set 
            {
                gamma = value;
                image.Gamma = value;
                SetFloat("gamma", 1 / gamma);

                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Gamma"));

                IsInvalidated = true;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
