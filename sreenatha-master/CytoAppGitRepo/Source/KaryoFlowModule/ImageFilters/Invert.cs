﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using System.ComponentModel;

namespace AI
{
    public class InvertFilter : FilterBase
    {
        public InvertFilter(object parent, IntPtr device, int textureWidth, int textureHeight, int textureBpp)
            : base("invert.fx", parent, device, textureWidth, textureHeight)
        {
            SetFloat("maxPixelValue", Math.Pow(2, textureBpp) / 65536.0);
        }

        public override bool RunsOn(ChannelInfo channelInfo)
        {
            return true;
        }
    }

    public class InvertFilterTemplate : IRenderPassTemplate
    {
        private static readonly Guid id = new Guid("5FD3B8DF-F175-40c8-9989-88ABA34963CC");

        public RenderPass CreatePass(IntPtr device, int textureWidth, int textureHeight, int textureBpp)
        {
            return new InvertFilter(this, device, textureWidth, textureHeight, textureBpp);
        }

        public UIElement CreateUi()
        {
            TextBlock text = new TextBlock();
            text.Text = DisplayName;
            text.VerticalAlignment = VerticalAlignment.Center;
            return text;
        }

        public string DisplayName
        {
            get { return "Invert"; }
        }

        public Guid Id
        {
            get { return id; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        bool isActive;
        public bool IsActive
        {
            get { return isActive; }
            set
            {
                isActive = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("IsActive"));
            }
        }

        public void FromXml(XElement filterNode)
        {
        }

        public void ToXml(XElement filterNode)
        {
        }
    }
}
