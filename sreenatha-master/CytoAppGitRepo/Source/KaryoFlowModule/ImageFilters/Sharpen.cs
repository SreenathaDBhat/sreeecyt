﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;

namespace AI
{
    public class SharpenFilter : FilterBase
    {
        public SharpenFilter(object parent, IntPtr device, int textureWidth, int textureHeight)
            : base("sharpenFilter.fx", parent, device, textureWidth, textureHeight)
        {
        }

        public override bool RunsOn(ChannelInfo channelInfo)
        {
            return true;
        }
    }

    public class SharpenFilterTemplate : IRenderPassTemplate
    {
        private static readonly Guid id = new Guid("02D55E70-ACC2-4c74-BA17-F4D94A3FF8F4");

        public RenderPass CreatePass(IntPtr device, int textureWidth, int textureHeight, int textureBpp)
        {
            return new SharpenFilter(this, device, textureWidth, textureHeight);
        }

        public UIElement CreateUi()
        {
            TextBlock text = new TextBlock();
            text.Text = DisplayName;
            text.VerticalAlignment = VerticalAlignment.Center;
            return text;
        }

        public string DisplayName
        {
            get { return "Sharpen"; }
        }

        public Guid Id
        {
            get { return id; }
        }

        public void FromXml(XElement filterNode)
        {
        }

        public void ToXml(XElement filterNode)
        {
        }
    }
}
