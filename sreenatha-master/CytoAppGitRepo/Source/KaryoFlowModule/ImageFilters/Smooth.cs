﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using System.ComponentModel;

namespace AI
{
    public class SmoothFilter : FilterBase
    {
        public SmoothFilter(object parent, IntPtr device, int textureWidth, int textureHeight)
            : base("smoothFilter.fx", parent, device, textureWidth, textureHeight)
        {
            SetTechnique("Smooth");
        }

        public override bool RunsOn(ChannelInfo channelInfo)
        {
            return true;
        }
    }

    public class SmoothFilterTemplate : IRenderPassTemplate
    {
        private static readonly Guid id = new Guid("3FCEC9B8-427C-46ac-AA9C-09A45C6819DA");

        public RenderPass CreatePass(IntPtr device, int textureWidth, int textureHeight, int textureBpp)
        {
            return new SmoothFilter(this, device, textureWidth, textureHeight);
        }

        public UIElement CreateUi()
        {
            TextBlock text = new TextBlock();
            text.Text = DisplayName;
            text.VerticalAlignment = VerticalAlignment.Center;
            return text;
        }

        public string DisplayName
        {
            get { return "Smooth"; }
        }

        public void FromXml(XElement filterNode)
        {
        }

        public void ToXml(XElement filterNode)
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;
        bool isActive;
        public bool IsActive
        {
            get { return isActive; }
            set
            {
                isActive = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("IsActive"));
            }
        }

        public Guid Id
        {
            get { return id; }
        }
    }
}
