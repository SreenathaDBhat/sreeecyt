﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Windows;
using System.Xml.Linq;
using System.Windows.Controls;

namespace AI
{
    public class UnsharpMaskFilterTemplate : IRenderPassTemplate, INotifyPropertyChanged
	{
        private static readonly Guid id = new Guid("9BB56560-79AF-4707-98D7-B68FAA3ACFC1");
        private UnsharpMaskParameters parms;
        private string displayName;
		public event PropertyChangedEventHandler PropertyChanged;

		public UnsharpMaskFilterTemplate(float sigma, int filterHalfWidth, float amount, string displayName)
        {
            this.displayName = displayName;

            SimplePersistance persist = new SimplePersistance();

            parms = new UnsharpMaskParameters()
            {
                FilterHalfWidth = Parsing.TryParseInt(persist.Get(DisplayName + "FilterHalfWidth", filterHalfWidth.ToString()), filterHalfWidth),
                Sigma = Parsing.TryParseFloat(persist.Get(DisplayName + "Sigma", sigma.ToString()), sigma),
                Amount = Parsing.TryParseFloat(persist.Get(DisplayName + "Amount", amount.ToString()), amount)
            };
        }

        bool isActive;
        public bool IsActive
        {
            get { return isActive; }
            set
            {
                isActive = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("IsActive"));
            }
        }

        public RenderPass CreatePass(IntPtr device, ImageFrame image, ImageFrameRenderer r, int textureBpp)
        {
            return new UnsharpMask(parms, this, device, image.ImageWidth, image.ImageHeight);
        }

		public float Sigma
		{
			get
			{
				return parms.Sigma;
			}
			set
			{
				parms.Sigma = value;
               
                if(PropertyChanged != null)
				    PropertyChanged(this, new PropertyChangedEventArgs("Sigma"));
			}
		}

        public int FilterHalfWidth
        {
            get
            {
                return parms.FilterHalfWidth;
            }
            set
            {
                parms.FilterHalfWidth = value;

                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("FilterHalfWidth"));
            }
        }

        public Guid Id
        {
            get { return id; }
        }

        public string DisplayName
        {
            get { return displayName; }
        }

        public UIElement UI
        {
            get { return null; }
            set { }
        }

        public void FromXml(XElement filterNode)
        {

        }

        public void ToXml(XElement filterNode)
        {

        }
	};
}
