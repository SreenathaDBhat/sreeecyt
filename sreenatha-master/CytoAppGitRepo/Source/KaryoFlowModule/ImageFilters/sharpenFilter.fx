float4x4 worldViewProj : WorldViewProjection;
texture source;
float onePixelX;
float onePixelY;

sampler sourceSampler = sampler_state
{
	Texture   = (source);
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

struct VS_INPUT
{
    float3 position	: POSITION;
	float2 texture0 : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4 hposition : POSITION;
	float2 texture0  : TEXCOORD0;
};

struct PS_OUTPUT
{
	float4 color : COLOR;
};


VS_OUTPUT vs(VS_INPUT IN)
{
    VS_OUTPUT OUT;

	OUT.hposition = mul(worldViewProj, float4(IN.position, 1));
	OUT.texture0 = IN.texture0 + float2(onePixelX/2, onePixelY/2);

	return OUT;
}

PS_OUTPUT ps(VS_OUTPUT IN)
{
	float strength = 0.4;
	
	float s11 = tex2D(sourceSampler, IN.texture0 + float2(-onePixelX,	-onePixelY)).r;
	float s12 = tex2D(sourceSampler, IN.texture0 + float2(0,			-onePixelY)).r;
	float s13 = tex2D(sourceSampler, IN.texture0 + float2(onePixelX,	-onePixelY)).r;
	float s21 = tex2D(sourceSampler, IN.texture0 + float2(-onePixelX,	0)).r;
	float s22 = tex2D(sourceSampler, IN.texture0 + float2(0,			0)).r;
	float s23 = tex2D(sourceSampler, IN.texture0 + float2(onePixelX,	0)).r;
	float s31 = tex2D(sourceSampler, IN.texture0 + float2(-onePixelX,	onePixelY)).r;
	float s32 = tex2D(sourceSampler, IN.texture0 + float2(0,			onePixelY)).r;
	float s33 = tex2D(sourceSampler, IN.texture0 + float2(onePixelX,	onePixelY)).r;	
	
	float v =	s11 * -1 + 
				s12 * -1 + 
				s13 * -1 +
				s21 * -1 + 
				s22 *  9 + 
				s23 * -1 +
				s31 * -1 + 
				s32 * -1 + 
				s33 * -1;
	v = v * strength + (1.0 - strength) * s22;
				
			
    PS_OUTPUT o = (PS_OUTPUT)0;
    o.color = float4(clamp(v, 0, 1), 0, 0, 1);
    return o;
}

technique Sharpen
{
    pass Pass0
    {
		Lighting = FALSE;

		VertexShader = compile vs_2_0 vs();
		PixelShader  = compile ps_2_0 ps();
    }
}