#include "StdAfx.h"
#include "AdaptiveBackgroundSubtraction.h"
#include "Bits.h"
using namespace System::IO;
using namespace System::Runtime::InteropServices;
using namespace System::Reflection;
using namespace AI::Bitmap;

namespace AI
{
	AdaptiveBackgroundSubtractionFilter::AdaptiveBackgroundSubtractionFilter( AdaptiveBackgroundSubtractionFilterParameters^ parameters, Object^ parent, IntPtr devicePtr, int w, int h)
		: RenderPass(parent)
	{
		this->params = parameters;
		this->w = w;
		this->h = h;

		HRESULT hr;
		LPDIRECT3DDEVICE9 device = (LPDIRECT3DDEVICE9)devicePtr.ToPointer();

		LPDIRECT3DSURFACE9 surfaceTmp = 0;
		LPDIRECT3DTEXTURE9 texTmp = 0;
		hr = device->CreateTexture(w, h, 1, D3DUSAGE_DYNAMIC, D3DFMT_G16R16, D3DPOOL_SYSTEMMEM, &texTmp, 0);
		hr = texTmp->GetSurfaceLevel(0, &surfaceTmp);

		textureResult = texTmp;
		surfaceResult = surfaceTmp;
	}

	void AdaptiveBackgroundSubtractionFilter::Release()
	{
		surfaceResult->Release();
		textureResult->Release();
	}

	bool AdaptiveBackgroundSubtractionFilter::RunsOn(ChannelInfo^ channelInfo) 
	{
		return !channelInfo->IsCounterstain;
	}

	void AdaptiveBackgroundSubtractionFilter::Render(IntPtr devicePtr, Snap^ snap, IntPtr inputTexturePtr, IQuadRenderer^ quadRenderer)
	{
		// Get image out of snap
		Channel^ channel = snap->ChannelPixels;

		// Process image
		int n = w * h;
		//unsigned short * destPtr = new unsigned short[n];
		//pin_ptr<unsigned short> srcPtr = &channel->Pixels[0];
		//unsigned short * s = srcPtr;
  //      unsigned short * d = destPtr;
  //      for (int i = 0; i < n; i++)
  //      {
  //          *d++ = *s++ - 50;
  //      }

		Channel^ backSubChannel = SubtractBackground::Adaptive( channel);
		pin_ptr<unsigned short> destPtr = &backSubChannel->Pixels[0];

		// Put result into a surface
		HRESULT hr;
		LPDIRECT3DDEVICE9 device = (LPDIRECT3DDEVICE9)devicePtr.ToPointer();
		LPDIRECT3DSURFACE9 parentRenderTarget;
		hr = device->GetRenderTarget(0, &parentRenderTarget);

		D3DLOCKED_RECT lock;
		hr = surfaceResult->LockRect(&lock, 0, 0);

		unsigned short * surface = (unsigned short*)lock.pBits;
		unsigned short * r = destPtr;
        for (int j = 0; j < n; j++)
        {
            *surface++ = *r;
			*surface++ = *r++;
        }

		hr = surfaceResult->UnlockRect();

		// Output surface 
		hr = device->UpdateSurface( surfaceResult, 0, parentRenderTarget, 0);

		// Cleanup
		//delete[] destPtr;
	}
}
