#pragma once
using namespace System::ComponentModel;

#include "Bits.h"

namespace AI
{
	public ref class AdaptiveBackgroundSubtractionFilterParameters
	{

	public:
		explicit AdaptiveBackgroundSubtractionFilterParameters()
		{
		}

	};

	public ref class AdaptiveBackgroundSubtractionFilter : RenderPass
	{
	private:
		LPDIRECT3DTEXTURE9 textureResult;
		LPDIRECT3DSURFACE9 surfaceResult;
		int w, h;
		AdaptiveBackgroundSubtractionFilterParameters^ params;

	public:
		explicit AdaptiveBackgroundSubtractionFilter(AdaptiveBackgroundSubtractionFilterParameters^ parameters, Object^ parent, IntPtr devicePtr, int w, int h);
		virtual void Render(IntPtr device, Snap^ snap, IntPtr inputTexturePtr, IQuadRenderer^ quadRenderer) override;
		virtual void Release() override;
		virtual bool RunsOn(ChannelInfo^ channelInfo) override;

	private:

	};
}