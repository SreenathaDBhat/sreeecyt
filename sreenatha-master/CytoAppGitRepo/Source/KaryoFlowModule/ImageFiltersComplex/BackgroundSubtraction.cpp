#include "StdAfx.h"
#include "BackgroundSubtraction.h"
#include "Bits.h"
using namespace System::IO;
using namespace System::Runtime::InteropServices;
using namespace System::Reflection;
using namespace AI::Bitmap;

namespace AI
{
	BackgroundSubtractionFilter::BackgroundSubtractionFilter(BackgroundSubtractionFilterParameters^ parameters, Object^ parent, IntPtr devicePtr, int w, int h) : RenderPass(parent)
	{
		this->params = parameters;
		this->w = w;
		this->h = h;

		HRESULT hr;
		LPDIRECT3DDEVICE9 device = (LPDIRECT3DDEVICE9)devicePtr.ToPointer();

		LPD3DXEFFECT tempEffect;
		LPD3DXBUFFER pBufferErrors = NULL;

#ifdef localFile
		D3DXCreateEffectFromFile(device, L"bgSubtraction.fx", NULL, NULL, 0, NULL, &tempEffect, &pBufferErrors);
#else
		Stream ^ stream = Assembly::GetExecutingAssembly()->GetManifestResourceStream("bgSubtraction.fx");
		System::String^ fx = nullptr;

		if (stream != nullptr)
		{
			StreamReader ^ sr = gcnew StreamReader(stream);
			fx = sr->ReadToEnd();
			stream->Close();
		}
		
		char* fxs = (char*)(void*)Marshal::StringToHGlobalAnsi(fx);
		D3DXCreateEffect(device, (void*)fxs, strlen(fxs), NULL, NULL, 0, NULL, &tempEffect, &pBufferErrors);

		Marshal::FreeHGlobal((IntPtr)fxs);
#endif
		effect = tempEffect;
		Impl::SetWorldViewProjectionMatrix(IntPtr(effect), w, h);
		effect->SetFloat("onePixelX", 1.0f / w);
		effect->SetFloat("onePixelY", 1.0f / h);

		LPDIRECT3DTEXTURE9 texTmp = 0;
		LPDIRECT3DSURFACE9 surfaceTmp = 0;

		hr = device->CreateTexture(w, h, 1, D3DUSAGE_RENDERTARGET, D3DFMT_G16R16, D3DPOOL_DEFAULT, &texTmp, 0);
		texTmp->GetSurfaceLevel(0, &surfaceTmp);
		renderTextureA = texTmp;
		renderSurfaceA = surfaceTmp;
		
		hr = device->CreateTexture(w, h, 1, D3DUSAGE_RENDERTARGET, D3DFMT_G16R16, D3DPOOL_DEFAULT, &texTmp, 0);
		texTmp->GetSurfaceLevel(0, &surfaceTmp);
		renderTextureB = texTmp;
		renderSurfaceB = surfaceTmp;
	}

	void BackgroundSubtractionFilter::Release()
	{
		effect->Release();
		renderSurfaceA->Release();
		renderTextureA->Release();
		renderSurfaceB->Release();
		renderTextureB->Release();
	}

	bool BackgroundSubtractionFilter::RunsOn(ChannelInfo^ channelInfo) 
	{
		return !channelInfo->IsCounterstain;
	}

	void BackgroundSubtractionFilter::RenderQuad(IQuadRenderer^ quadRenderer)
	{
		HRESULT hr;
		UINT shaderPasses = 0;
		hr = effect->Begin(&shaderPasses, 0);
		for(UINT sp = 0; sp < shaderPasses; ++sp)
		{
			hr = effect->BeginPass(sp);
			hr = quadRenderer->RenderQuad();
			hr = effect->EndPass();
		}
		hr = effect->End();
	}

	void BackgroundSubtractionFilter::Render(IntPtr devicePtr, Snap^ snap, IntPtr inputTexturePtr, IQuadRenderer^ quadRenderer)
	{
		HRESULT hr;
		LPDIRECT3DDEVICE9 device = (LPDIRECT3DDEVICE9)devicePtr.ToPointer();

		LPDIRECT3DSURFACE9 parentRenderTarget;
		device->GetRenderTarget(0, &parentRenderTarget);

		device->SetRenderTarget(0, renderSurfaceA);
		device->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_ARGB(0, 0, 0, 0), 1.0f, 0);
		
		LPDIRECT3DSURFACE9 nextSurface = renderSurfaceB;
		UINT shaderPasses = 0;
		LPDIRECT3DTEXTURE9 input = (LPDIRECT3DTEXTURE9)inputTexturePtr.ToPointer();
		
		int numPasses = params->Passes;
		int totalPasses = numPasses * 2;
		
		for(int i = 0; i < totalPasses; ++i)
		{
			if(i < numPasses)
				hr = effect->SetTechnique(i % 2 == 0 ? "Erode" : "ErodeDiagonal");
			else
				hr = effect->SetTechnique(i % 2 == 0 ? "Dilate" : "DilateDiagonal");

			hr = effect->SetTexture("source", input);
			RenderQuad(quadRenderer);

			if(nextSurface == renderSurfaceB)
			{
				input = renderTextureA;
				device->SetRenderTarget(0, renderSurfaceB);
				nextSurface = renderSurfaceA;
			}
			else
			{
				input = renderTextureB;
				device->SetRenderTarget(0, renderSurfaceA);
				nextSurface = renderSurfaceB;
			}
		}

		device->SetRenderTarget(0, parentRenderTarget);
		hr = effect->SetTexture("background", nextSurface == renderSurfaceA ? renderTextureA : renderTextureB);
		hr = effect->SetTexture("source", (LPDIRECT3DTEXTURE9)inputTexturePtr.ToPointer());
		hr = effect->SetTechnique("Subtract");
		RenderQuad(quadRenderer);
	}
	
}