#pragma once
using namespace System::ComponentModel;

#include "Bits.h"

namespace AI
{
	public ref class BackgroundSubtractionFilterParameters
	{
	private:
		int passes;

	public:
		explicit BackgroundSubtractionFilterParameters()
		{
			passes = 4;
		}

		property int Passes
		{
			int get() { return passes; }
			void set(int value) { passes = value; }
		}
	};

	public ref class BackgroundSubtractionFilter : RenderPass
	{
	private:
		LPD3DXEFFECT effect;
		LPDIRECT3DSURFACE9      renderSurfaceA;
		LPDIRECT3DTEXTURE9      renderTextureA;
		LPDIRECT3DSURFACE9      renderSurfaceB;
		LPDIRECT3DTEXTURE9      renderTextureB;
		int w, h;
		BackgroundSubtractionFilterParameters^ params;

	public:
		explicit BackgroundSubtractionFilter(BackgroundSubtractionFilterParameters^ parameters, Object^ parent, IntPtr devicePtr, int w, int h);
		virtual void Render(IntPtr device, Snap^ snap, IntPtr inputTexturePtr, IQuadRenderer^ quadRenderer) override;
		virtual void Release() override;
		virtual bool RunsOn(ChannelInfo^ channelInfo) override;

	private:
		void RenderQuad(IQuadRenderer^ quadRenderer);
	};
}