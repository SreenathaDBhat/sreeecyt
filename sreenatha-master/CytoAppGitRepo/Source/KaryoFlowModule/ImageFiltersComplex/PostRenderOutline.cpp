#include "StdAfx.h"
#include "PostRenderOutline.h"

using namespace System::IO;
using namespace System::Runtime::InteropServices;
using namespace System::Reflection;

#define SafeRelease(x) {if(x) { x->Release(); } x=0; }

namespace AI
{
	PostRenderOutline::PostRenderOutline()
	{
		effect = 0;
	}

	void PostRenderOutline::SetThresh(double thresh)
	{
		threshold = thresh;
	}

	void PostRenderOutline::Render(IntPtr devicePtr, IntPtr sourceTexture, IntPtr destRenderSurface, IQuadRenderer^ quadRenderer, int w, int h)
	{
		HRESULT hr;
		LPDIRECT3DDEVICE9 device = (LPDIRECT3DDEVICE9)devicePtr.ToPointer();

		if(effect == 0)
		{
			LPD3DXEFFECT tempEffect;
			LPDIRECT3DTEXTURE9 texTmp = 0;
			LPDIRECT3DSURFACE9 surfaceTmp = 0;
			LPD3DXBUFFER pBufferErrors = NULL;

#ifdef localFile
			hr = D3DXCreateEffectFromFile(device, L"outline.fx", NULL, NULL, 0, NULL, &tempEffect, &pBufferErrors);
#else
		Stream ^ stream = Assembly::GetExecutingAssembly()->GetManifestResourceStream("outline.fx");
		System::String^ fx = nullptr;

		if (stream != nullptr)
		{
			StreamReader ^ sr = gcnew StreamReader(stream);
			fx = sr->ReadToEnd();
			stream->Close();
		}
		
		char* fxs = (char*)(void*)Marshal::StringToHGlobalAnsi(fx);
		hr = D3DXCreateEffect(device, (void*)fxs, strlen(fxs), NULL, NULL, 0, NULL, &tempEffect, &pBufferErrors);

		Marshal::FreeHGlobal((IntPtr)fxs);
#endif
			effect = tempEffect;
			Impl::SetWorldViewProjectionMatrix(IntPtr(effect), w, h);
			hr = effect->SetFloat("onePixelX", 1.0f / w);
			hr = effect->SetFloat("onePixelY", 1.0f / h);

			hr = device->CreateTexture(w, h, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &texTmp, 0);
			texTmp->GetSurfaceLevel(0, &surfaceTmp);
			threshSurf = surfaceTmp;
			threshTex = texTmp;

			hr = device->CreateTexture(w, h, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &texTmp, 0);
			texTmp->GetSurfaceLevel(0, &surfaceTmp);
			dilatedSurf = surfaceTmp;
			dilatedTex = texTmp;
		}

		effect->SetFloat("threshold", (FLOAT)threshold);

		hr = device->SetRenderTarget(0, threshSurf);
		hr = effect->SetTexture("inputTex", (LPDIRECT3DTEXTURE9)sourceTexture.ToPointer());
		hr = effect->SetTechnique("Thresh");
		RenderQuad(quadRenderer);

		hr = device->SetRenderTarget(0, dilatedSurf);
		hr = effect->SetTexture("inputTex", threshTex);
		hr = effect->SetTechnique("Dilate");
		RenderQuad(quadRenderer);

			
		hr = device->SetRenderTarget(0, (LPDIRECT3DSURFACE9)destRenderSurface.ToPointer());
		hr = effect->SetTexture("dilatedTex", dilatedTex);
		hr = effect->SetTexture("thresholdedTex", threshTex);
		hr = effect->SetTexture("inputTex", (LPDIRECT3DTEXTURE9)sourceTexture.ToPointer());
		hr = effect->SetTechnique("Subtract");
		RenderQuad(quadRenderer);
	}

	void PostRenderOutline::RenderQuad(IQuadRenderer^ quadRenderer)
	{
		HRESULT hr;
		UINT shaderPasses = 0;
		hr = effect->Begin(&shaderPasses, 0);
		for(UINT sp = 0; sp < shaderPasses; ++sp)
		{
			hr = effect->BeginPass(sp);
			hr = quadRenderer->RenderQuad();
			hr = effect->EndPass();
		}
		hr = effect->End();
	}

	void PostRenderOutline::Release()
	{
		if(effect == 0)
			return;

		SafeRelease(threshSurf);
		SafeRelease(threshTex);
		SafeRelease(dilatedSurf);
		SafeRelease(dilatedTex);
		SafeRelease(effect);
	}
}