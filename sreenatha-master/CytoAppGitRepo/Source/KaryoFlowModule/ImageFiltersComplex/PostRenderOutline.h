#pragma once

namespace AI
{
	public ref class PostRenderOutline : public IPostRenderEffect
	{
	public:
		PostRenderOutline();

		virtual void Render(IntPtr device, IntPtr sourceTexture, IntPtr destRenderSurface, IQuadRenderer^ quadRenderer, int w, int h);
		virtual void Release();

		property double Threshold
		{
			double get() { return threshold; }
			void set(double v) { SetThresh(v); }
		}

	private:
		void RenderQuad(IQuadRenderer^ quadRenderer);
		void SetThresh(double thresh);

		double threshold;
		LPD3DXEFFECT effect;
		LPDIRECT3DSURFACE9 threshSurf;
		LPDIRECT3DTEXTURE9 threshTex;
		LPDIRECT3DSURFACE9 dilatedSurf;
		LPDIRECT3DTEXTURE9 dilatedTex;
	};
}