#include "StdAfx.h"
#include "UnsharpMask.h"
#include "Bits.h"
using namespace System::IO;
using namespace System::Runtime::InteropServices;
using namespace System::Reflection;

namespace AI
{
	VOID WINAPI FillTexel(D3DXVECTOR4* pOut, const D3DXVECTOR2* pTexCoord, const D3DXVECTOR2* pTexelSize, LPVOID pData)
	{	// used to fix texture directly via D3DXFillTexture
		
		TexFillData FD = *(TexFillData *) pData;
		// NOTE these texture coordinates range from 0 to 1
		int x = (int)(pTexCoord->x * FD.W);
		int y = (int)(pTexCoord->y * FD.H);

		D3DXVECTOR4 *pV = FD.pV;
		pV += (x + y * FD.H);
	   *pOut = D3DXVECTOR4(pV->x, pV->y, pV->z, pV->w);
	}

	UnsharpMask::UnsharpMask(UnsharpMaskParameters^ parameters, Object^ parent, IntPtr devicePtr, int w, int h) : RenderPass(parent)
	{
		this->params = parameters;
		this->w = w;
		this->h = h;

		HRESULT hr;
		LPDIRECT3DDEVICE9 device = (LPDIRECT3DDEVICE9)devicePtr.ToPointer();

		LPD3DXEFFECT tempEffect;
		LPD3DXBUFFER pBufferErrors = NULL;
		filterArray = NULL;

#ifdef localFile
		D3DXCreateEffectFromFile(device, L"unsharpMask.fx", NULL, NULL, 0, NULL, &tempEffect, &pBufferErrors);
#else
		Stream ^ stream = Assembly::GetExecutingAssembly()->GetManifestResourceStream("unsharpMask.fx");
		System::String^ fx = nullptr;

		if (stream != nullptr)
		{
			StreamReader ^ sr = gcnew StreamReader(stream);
			fx = sr->ReadToEnd();
			stream->Close();
		}
		
		char* fxs = (char*)(void*)Marshal::StringToHGlobalAnsi(fx);
		D3DXCreateEffect(device, (void*)fxs, strlen(fxs), NULL, NULL, 0, NULL, &tempEffect, &pBufferErrors);

		Marshal::FreeHGlobal((IntPtr)fxs);
#endif
		effect = tempEffect;
		Impl::SetWorldViewProjectionMatrix(IntPtr(effect), w, h);
		effect->SetFloat("onePixelX", 1.0f / w);
		effect->SetFloat("onePixelY", 1.0f / h);

		LPDIRECT3DTEXTURE9 texTmp = 0;
		LPDIRECT3DSURFACE9 surfaceTmp = 0;

		hr = device->CreateTexture(w, h, 1, D3DUSAGE_RENDERTARGET, D3DFMT_G16R16, D3DPOOL_DEFAULT, &texTmp, 0);
		texTmp->GetSurfaceLevel(0, &surfaceTmp);
		swapTexture = texTmp;
		swapSurface = surfaceTmp;
		
		hr = device->CreateTexture(w, h, 1, D3DUSAGE_RENDERTARGET, D3DFMT_G16R16, D3DPOOL_DEFAULT, &texTmp, 0);
		texTmp->GetSurfaceLevel(0, &surfaceTmp);
		swapTexture2 = texTmp;
		swapSurface2 = surfaceTmp;

        hr = device->CreateTexture(Size, 1, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A16B16G16R16F, D3DPOOL_DEFAULT, &texTmp, 0);
		filterTexture = texTmp;
	}

	void UnsharpMask::Release()
	{
		effect->Release();
		swapSurface->Release();
		swapTexture->Release();
		swapSurface2->Release();
		swapTexture2->Release();
		filterTexture->Release();
		if (filterArray != NULL)
			delete filterArray;
	}

	bool UnsharpMask::RunsOn(ChannelInfo^ channelInfo) 
	{
		return true;
	}

	void UnsharpMask::RenderQuad(IQuadRenderer^ quadRenderer)
	{
		HRESULT hr;
		UINT shaderPasses = 0;
		hr = effect->Begin(&shaderPasses, 0);
		for(UINT sp = 0; sp < shaderPasses; ++sp)
		{
			hr = effect->BeginPass(sp);
			hr = quadRenderer->RenderQuad();
			hr = effect->EndPass();
		}
		hr = effect->End();
	}

	void UnsharpMask::Render(IntPtr devicePtr, Snap^ snap, IntPtr inputTexturePtr, IQuadRenderer^ quadRenderer)
	{
		LPDIRECT3DDEVICE9 device = (LPDIRECT3DDEVICE9)devicePtr.ToPointer();

		LPDIRECT3DSURFACE9 parentRenderTarget;
		device->GetRenderTarget(0, &parentRenderTarget);

		GenerateFilter();
		ApplyGaussian(devicePtr, inputTexturePtr, quadRenderer, parentRenderTarget);
		ApplyUnsharpMask(devicePtr, inputTexturePtr, quadRenderer, parentRenderTarget);
	}

	void UnsharpMask::GenerateFilter()
	{
        float Total = 0.0f;

		if (filterArray != NULL)
			delete filterArray;
        filterArray = new D3DXVECTOR4[Size];

        // Calculate our filter coefficients - NOTE we are doing a vertical and a horizontal pass over
        // the image so we only need a 1D texture for the coefficients
        for (int i = 0; i < Size; i++)
        {
            int j = i - params->FilterHalfWidth;
            filterArray[i].x = (float)j / (float)w;
            filterArray[i].y = (float)j / (float)h;
            filterArray[i].z = 0.0f;

            // Gaussian filter
            float ep = -(j * j) / (2.0f * params->Sigma * params->Sigma);
            // NOTE 2.50662827 = sqrt(2 * pi)
            // NOTE if this was applied in both directions this would be 2.50662827 * Sigma * Sigma
            filterArray[i].w = (FLOAT) (exp(ep) / (2.50662827 * params->Sigma));    
            Total += filterArray[i].w;   
        }

        // Normalise them
        for (int i = 0; i < Size; i++)
            filterArray[i].w /= Total;                 // Multiply divisor by 2 as must apply in X & Y direction

	    TexFillData m_FillData;
		m_FillData.pV = filterArray;
		m_FillData.W = Size;
		m_FillData.H = 1;
		m_FillData.P = 0;
		// Fill the texture using D3DXFillTexture
		HRESULT hr = D3DXFillTexture(filterTexture, (LPD3DXFILL2D)FillTexel, (void *) &m_FillData);
	}

	void UnsharpMask::ApplyGaussian(IntPtr devicePtr, IntPtr inputTexturePtr, IQuadRenderer^ quadRenderer, LPDIRECT3DSURFACE9 parentRenderTarget)
	{
  		UINT cPasses;
		HRESULT hr;

		LPDIRECT3DDEVICE9 device = (LPDIRECT3DDEVICE9)devicePtr.ToPointer();
		LPDIRECT3DTEXTURE9 source = (LPDIRECT3DTEXTURE9)inputTexturePtr.ToPointer();

		hr = effect->SetTechnique("gaussian");

		hr = effect->Begin(&cPasses, 0);
		hr = device->SetRenderTarget( 0, swapSurface);			// process original verticals into swap buffer
	    
		hr = effect->SetTexture("SourceImageTexture", source);
		hr = effect->SetInt("Size", Size);  // Filter Size
		hr = effect->SetTexture("CoefficientsTexture", filterTexture);
		        
		hr = effect->BeginPass(0);	 
		hr = effect->CommitChanges();
		hr = quadRenderer->RenderQuad();
		hr = effect->EndPass();

		hr = device->SetRenderTarget( 0, swapSurface2);			// process swap buffer horizontals into 2nd swap buffer, ready to apply as mask
//hr = device->SetRenderTarget( 0, parentRenderTarget);

		hr = effect->SetTexture("SourceImageTexture", swapTexture);
		hr = effect->BeginPass(1);	 
		hr = effect->CommitChanges();
		hr = quadRenderer->RenderQuad();
		hr = effect->EndPass();
		hr = effect->End();
	}

	void UnsharpMask::ApplyUnsharpMask(IntPtr devicePtr, IntPtr inputTexturePtr, IQuadRenderer^ quadRenderer, LPDIRECT3DSURFACE9 parentRenderTarget)
	{
		LPDIRECT3DDEVICE9 device = (LPDIRECT3DDEVICE9)devicePtr.ToPointer();
		LPDIRECT3DTEXTURE9 source = (LPDIRECT3DTEXTURE9)inputTexturePtr.ToPointer();

		HRESULT hr;
		UINT cPasses;
		
		hr = effect->SetTechnique("UnsharpMask");
		hr = effect->Begin(&cPasses, 0);
		hr = device->SetRenderTarget( 0, parentRenderTarget);

		hr = effect->SetTexture("SourceImageTexture", source);			// orignal image
		hr = effect->SetTexture("SourceImage2Texture", swapTexture);	// copy of the image blurred by gaussian filter
		hr = effect->SetFloat("Amount", params->Amount);

		hr = effect->BeginPass(0);
		hr = effect->CommitChanges();
		hr = quadRenderer->RenderQuad();
		hr = effect->EndPass();
		hr = effect->End();
	}
}