#pragma once
using namespace System::ComponentModel;

#include "Bits.h"

namespace AI
{
	public ref class UnsharpMaskParameters
	{
	private:
		int filterHalfWidth;
		float amount;
		float sigma;

	public:
		explicit UnsharpMaskParameters()
		{
			filterHalfWidth = 4;
			sigma = 4.0;
			amount = (float)0.75;
		}

		property int FilterHalfWidth
		{
			int get() { return filterHalfWidth; }
			void set(int value) { filterHalfWidth = value; }
		}

		property float Sigma
		{
			float get() { return sigma; }
			void set(float value) { sigma = value; }
		}

		property float Amount
		{
			float get() { return amount; }
			void set(float value) { amount = value; }
		}

	};

	public ref class UnsharpMask : RenderPass
	{
	private:
		LPD3DXEFFECT effect;
		LPDIRECT3DSURFACE9      swapSurface;
		LPDIRECT3DTEXTURE9      swapTexture;
		LPDIRECT3DSURFACE9      swapSurface2;
		LPDIRECT3DTEXTURE9      swapTexture2;
		LPDIRECT3DTEXTURE9      filterTexture;
		D3DXVECTOR4*			filterArray;
		int w, h;
		UnsharpMaskParameters^ params;

	public:
		explicit UnsharpMask(UnsharpMaskParameters^ parameters, Object^ parent, IntPtr devicePtr, int w, int h);
		virtual void Render(IntPtr device, Snap^ snap, IntPtr inputTexturePtr, IQuadRenderer^ quadRenderer) override;
		virtual void Release() override;
		virtual bool RunsOn(ChannelInfo^ channelInfo) override;

	private:
		void RenderQuad(IQuadRenderer^ quadRenderer);
		void ApplyGaussian(IntPtr device, IntPtr inputTexturePtr, IQuadRenderer^ quadRenderer, LPDIRECT3DSURFACE9 parentRenderTarget);
		void ApplyUnsharpMask(IntPtr device, IntPtr inputTexturePtr, IQuadRenderer^ quadRenderer, LPDIRECT3DSURFACE9 parentRenderTarget);
		void GenerateFilter();

		property int Size
		{
			int get() { return params->FilterHalfWidth * 2 + 1; }
		}
	};

	typedef struct
	{
		int W, H, P;
		D3DXVECTOR4 *pV;
	} TexFillData;
}