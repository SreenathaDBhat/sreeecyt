float4x4 worldViewProj : WorldViewProjection;
texture source;
texture background;
float onePixelX;
float onePixelY;

sampler sourceSampler = sampler_state
{
	Texture   = (source);
    ADDRESSU = MIRROR;
    ADDRESSV = MIRROR;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};
sampler backgroundSampler = sampler_state
{
	Texture   = (background);
    ADDRESSU = MIRROR;
    ADDRESSV = MIRROR;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

struct VS_INPUT
{
    float3 position	: POSITION;
	float2 texture0 : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4 hposition : POSITION;
	float2 texture0  : TEXCOORD0;
};

struct PS_OUTPUT
{
	float4 color : COLOR;
};


VS_OUTPUT vs(VS_INPUT IN)
{
    VS_OUTPUT OUT;

	OUT.hposition = mul(worldViewProj, float4(IN.position, 1));
	OUT.texture0 = IN.texture0 + float2(onePixelX/2, onePixelY/2);

	return OUT;
}

PS_OUTPUT DilatePs(VS_OUTPUT IN)
{
	float s12 = tex2D(sourceSampler, IN.texture0 + float2(0,			-onePixelY)).r;
	float s21 = tex2D(sourceSampler, IN.texture0 + float2(-onePixelX,	0)).r;
	float s22 = tex2D(sourceSampler, IN.texture0 + float2(0,			0)).r;
	float s23 = tex2D(sourceSampler, IN.texture0 + float2(onePixelX,	0)).r;
	float s32 = tex2D(sourceSampler, IN.texture0 + float2(0,			onePixelY)).r;
	
	float v = max(s12, max(s21, max(s22, max(s23, s32)))); 
	
    PS_OUTPUT o = (PS_OUTPUT)0;
    o.color = float4(v, 0, 0, 1);
    return o;
}

PS_OUTPUT DilateDiagonalPs(VS_OUTPUT IN)
{
	float s11 = tex2D(sourceSampler, IN.texture0 + float2(-onePixelX,	-onePixelY)).r;
	float s13 = tex2D(sourceSampler, IN.texture0 + float2(onePixelX,	-onePixelY)).r;
	float s22 = tex2D(sourceSampler, IN.texture0 + float2(0,			0)).r;
	float s31 = tex2D(sourceSampler, IN.texture0 + float2(-onePixelX,	onePixelY)).r;
	float s33 = tex2D(sourceSampler, IN.texture0 + float2(onePixelX,	onePixelY)).r;	
	
	float v = max(s11, max(s13, max(s22, max(s31, s33))));
	
    PS_OUTPUT o = (PS_OUTPUT)0;
    o.color = float4(v, 0, 0, 1);
    return o;
}

technique Dilate
{
    pass Pass0
    {
		Lighting = FALSE;

		VertexShader = compile vs_2_0 vs();
		PixelShader  = compile ps_2_0 DilatePs();
    }
}
technique DilateDiagonal
{
    pass Pass0
    {
		Lighting = FALSE;

		VertexShader = compile vs_2_0 vs();
		PixelShader  = compile ps_2_0 DilateDiagonalPs();
    }
}


PS_OUTPUT ErodePs(VS_OUTPUT IN)
{
	float s12 = tex2D(sourceSampler, IN.texture0 + float2(0,			-onePixelY)).r;
	float s21 = tex2D(sourceSampler, IN.texture0 + float2(-onePixelX,	0)).r;
	float s22 = tex2D(sourceSampler, IN.texture0 + float2(0,			0)).r;
	float s23 = tex2D(sourceSampler, IN.texture0 + float2(onePixelX,	0)).r;
	float s32 = tex2D(sourceSampler, IN.texture0 + float2(0,			onePixelY)).r;
	
	float v = min(s12, min(s21, min(s22, min(s23, s32)))); 
	
    PS_OUTPUT o = (PS_OUTPUT)0;
    o.color = float4(v, 0, 0, 1);
    return o;
}

PS_OUTPUT ErodeDiagonalPs(VS_OUTPUT IN)
{
	float s11 = tex2D(sourceSampler, IN.texture0 + float2(-onePixelX,	-onePixelY)).r;
	float s13 = tex2D(sourceSampler, IN.texture0 + float2(onePixelX,	-onePixelY)).r;
	float s22 = tex2D(sourceSampler, IN.texture0 + float2(0,			0)).r;
	float s31 = tex2D(sourceSampler, IN.texture0 + float2(-onePixelX,	onePixelY)).r;
	float s33 = tex2D(sourceSampler, IN.texture0 + float2(onePixelX,	onePixelY)).r;	
	
	float v = min(s11, min(s13, min(s22, min(s31, s33))));
	
    PS_OUTPUT o = (PS_OUTPUT)0;
    o.color = float4(v, 0, 0, 1);
    return o;
}

technique Erode
{
    pass Pass0
    {
		Lighting = FALSE;

		VertexShader = compile vs_2_0 vs();
		PixelShader  = compile ps_2_0 ErodePs();
    }
}
technique ErodeDiagonal
{
    pass Pass0
    {
		Lighting = FALSE;

		VertexShader = compile vs_2_0 vs();
		PixelShader  = compile ps_2_0 ErodeDiagonalPs();
    }
}


PS_OUTPUT SubtractPs(VS_OUTPUT IN)
{
	float s = tex2D(sourceSampler, IN.texture0).r;
	float b = tex2D(backgroundSampler, IN.texture0).r;
	float v = s - (b);
	
    PS_OUTPUT o = (PS_OUTPUT)0;
    o.color = float4(v, 0, 0, 1);
    return o;
}

technique Subtract
{
    pass Pass0
    {
		Lighting = FALSE;

		VertexShader = compile vs_2_0 vs();
		PixelShader  = compile ps_2_0 SubtractPs();
    }
}