float4x4 worldViewProj : WorldViewProjection;
float onePixelX;
float onePixelY;
texture inputTex;
texture dilatedTex;
texture thresholdedTex;
float threshold;

sampler inputTexSampler = sampler_state
{
	Texture   = (inputTex);
    ADDRESSU = MIRROR;
    ADDRESSV = MIRROR;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};
sampler dilatedTexSampler = sampler_state
{
	Texture   = (dilatedTex);
    ADDRESSU = MIRROR;
    ADDRESSV = MIRROR;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};
sampler thresholdedTexSampler = sampler_state
{
	Texture   = (thresholdedTex);
    ADDRESSU = MIRROR;
    ADDRESSV = MIRROR;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};


struct VS_INPUT
{
    float3 position	: POSITION;
	float2 texture0 : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4 hposition : POSITION;
	float2 texture0  : TEXCOORD0;
};

struct PS_OUTPUT
{
	float4 color : COLOR;
};


VS_OUTPUT vs(VS_INPUT IN)
{
    VS_OUTPUT OUT;

	OUT.hposition = mul(worldViewProj, float4(IN.position, 1));
	OUT.texture0 = IN.texture0 + float2(onePixelX/2, onePixelY/2);

	return OUT;
}

PS_OUTPUT DilatePs(VS_OUTPUT IN)
{
	float s11 = tex2D(inputTexSampler, IN.texture0 + float2(-onePixelX,	-onePixelY)).r;
	float s12 = tex2D(inputTexSampler, IN.texture0 + float2(0,			-onePixelY)).r;
	float s13 = tex2D(inputTexSampler, IN.texture0 + float2(onePixelX,	-onePixelY)).r;
	float s21 = tex2D(inputTexSampler, IN.texture0 + float2(-onePixelX,	0)).r;
	float s22 = tex2D(inputTexSampler, IN.texture0 + float2(0,			0)).r;
	float s23 = tex2D(inputTexSampler, IN.texture0 + float2(onePixelX,	0)).r;
	float s31 = tex2D(inputTexSampler, IN.texture0 + float2(-onePixelX,	onePixelY)).r;
	float s32 = tex2D(inputTexSampler, IN.texture0 + float2(0,			onePixelY)).r;
	float s33 = tex2D(inputTexSampler, IN.texture0 + float2(onePixelX,	onePixelY)).r;
	
	float v = max(s11, max(s12, max(s13, max(s21, max(s22, max(s23, max(s31, max(s32, s33)))))))); 
	
    PS_OUTPUT o = (PS_OUTPUT)0;
    o.color = float4(v, 0, 0, 1);
    return o;
}
technique Dilate
{
    pass Pass0
    {
		Lighting = FALSE;

		VertexShader = compile vs_2_0 vs();
		PixelShader  = compile ps_2_0 DilatePs();
    }
}

PS_OUTPUT ThreshPs(VS_OUTPUT IN)
{
	float v = tex2D(inputTexSampler, IN.texture0).r;
		
    PS_OUTPUT o = (PS_OUTPUT)0;
    if(v < threshold)
		o.color = float4(1,1,1,1);
	else
		o.color = float4(0,0,0,0);
		
    return o;
}
technique Thresh
{
    pass Pass0
    {
		Lighting = FALSE;

		VertexShader = compile vs_2_0 vs();
		PixelShader  = compile ps_2_0 ThreshPs();
    }
}

PS_OUTPUT SubtractPs(VS_OUTPUT IN)
{
	float t = tex2D(thresholdedTexSampler, IN.texture0).r;
	float d = tex2D(dilatedTexSampler, IN.texture0).r;
	float s = d - t;
	float4 i = tex2D(inputTexSampler, IN.texture0);
	
	float4 outlineColor = float4(0.17f,0.7f,0.42f,1);
		
    PS_OUTPUT o = (PS_OUTPUT)0;
    if(s > 0.1f)
		o.color = outlineColor;
	else
		o.color = i;
		
	return o;
}

technique Subtract
{
    pass Pass0
    {
		Lighting = FALSE;

		VertexShader = compile vs_2_0 vs();
		PixelShader  = compile ps_2_0 SubtractPs();
    }
}