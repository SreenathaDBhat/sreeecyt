// Ported from Karls GIP filter
float4x4 worldViewProj : WorldViewProjection;
float onePixelX;
float onePixelY;
texture SourceImageTexture;             // Source
texture SourceImage2Texture;            // Blurred copy of source
texture CoefficientsTexture;			// Stores the filter coefficients 
float	Amount;
int		Size;


sampler SourceImage = sampler_state
{
	Texture   = (SourceImageTexture);
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

sampler SourceImage2 = sampler_state
{
    Texture = (SourceImage2Texture);
    ADDRESSU = MIRROR;            // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;            //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler CoeffImage = sampler_state
{
    Texture = (CoefficientsTexture);
    ADDRESSU = MIRROR;            
    ADDRESSV = MIRROR;            
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

struct VS_INPUT
{
    float3 position	: POSITION;
	float2 texture0 : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4 hposition : POSITION;
	float2 texture0  : TEXCOORD0;
};

VS_OUTPUT vs(VS_INPUT IN)
{
    VS_OUTPUT OUT;

	OUT.hposition = mul(worldViewProj, float4(IN.position, 1));
	OUT.texture0 = IN.texture0 + float2(onePixelX/2, onePixelY/2);

	return OUT;
}

float4 PixelConvolveH(float Filt, float2 Pos)
{
	float4 C;
	float4 S;
	float4 R;
	float2 TexS;
	
	// Look up the pixel offsets (x, y component of texture) and filter coefficients (w component of texture)
	C = tex1D(CoeffImage, Filt);
	
	// Adjust sampler coords 
	TexS.x = C.x + Pos.x;
	TexS.y = Pos.y;
	
	// Multiply by the w component in the texture i.e. the filter coefficients
	S = tex2D(SourceImage, TexS);
	R = mul(S, C.w);
	
	return R;
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Given the texture coordinate into the filter coefficients texture, look up the 
// float4 value. The x,y components are sample offsets into the image we are applying the
// filter to and the w component is the filter coefficient itself.
////////////////////////////////////////////////////////////////////////////////////////////////

float4 PixelConvolveV(float Filt, float2 Pos)
{
	float4 C;
	float4 S;
	float4 R;
	float2 TexS;
	
	// Look up the pixel offsets (x, y component of texture) and filter coefficients (w component of texture)
	C = tex1D(CoeffImage, Filt);
	
	// Adjust sampler coords 
	TexS.x = Pos.x;
	TexS.y = C.y + Pos.y;
	
	// Multiply by the w component in the texture i.e. the filter coefficients
	S = tex2D(SourceImage, TexS);
	R = mul(S, C.w);
	
	return R;
}

float4 GaussH(VS_OUTPUT Input) : COLOR0
{
	int i;
	float TexC;
	float4 Result = 0.0;
	float4 R = 0.0;
		
    TexC = 0.5 / Size;							// Centre of the coefficients texel
   			
	for (i = 0; i < 7; i++)
	{
		R = PixelConvolveH(TexC, Input.texture0);
		Result += R;
		TexC += 1.0 / Size;						// Next coefficient
	}
	
	return Result;
}

float4 GaussV(VS_OUTPUT Input) : COLOR0
{
	int i;
	float TexC;
	float4 Result = 0.0;
		
    TexC = 0.5 / Size;							// Centre of the coefficients texel
   			
	for (i = 0; i < 7; i++)
	{
		Result += PixelConvolveV(TexC, Input.texture0);
		TexC += 1.0 / Size;						// Next coefficient
	}
	
	return Result;
}

float4 Umask(VS_OUTPUT Input) : COLOR0    
{ 
	float4 outp;
	outp=(tex2D(SourceImage,Input.texture0)-Amount*tex2D(SourceImage2,Input.texture0))/(1.-Amount);
    return outp;
}

technique gaussian
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 vs();
        PixelShader = compile ps_2_0 GaussH();
    }
    
    pass P1
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 vs();
        PixelShader = compile ps_2_0 GaussV();
    }
}

technique UnsharpMask
{
    pass Pass0
    {
		Lighting = FALSE;

		VertexShader = compile vs_2_0 vs();
		PixelShader  = compile ps_2_0 Umask();
    }
}