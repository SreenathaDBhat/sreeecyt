#include "stdafx.h"
#include "Bits.h"

namespace
{
	#pragma warning(disable:4793)
	HRESULT Matrixes(LPD3DXEFFECT effect, int screenWidth, int screenHeight)
	{
		D3DXMATRIXA16 matWorld;
		D3DXMatrixIdentity(&matWorld);

		D3DXMATRIXA16 matView;
		D3DXVECTOR3 vEyePt(0.0f, 0.0f,-4.0f);
		D3DXVECTOR3 vLookatPt(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 vUpVec(0.0f, 1.0f, 0.0f);
		D3DXMatrixLookAtLH(&matView, &vEyePt, &vLookatPt, &vUpVec);

		D3DXMATRIXA16 matProj;
		D3DXMatrixOrthoLH(&matProj, (FLOAT)screenWidth, (FLOAT)screenHeight, 0, 100);

		D3DXMATRIX worldViewProjection = matWorld * matView * matProj;
		D3DXMatrixTranspose(&worldViewProjection, &worldViewProjection);

		return effect->SetMatrix("worldViewProj", &worldViewProjection);
	}	

	HRESULT Matrixes(LPD3DXEFFECT effect, int screenWidth, int screenHeight, double translateX, double translateY, double angle, double scaleX, double scaleY)
	{
		D3DXMATRIX trans;
		D3DXMatrixTranslation(&trans, (FLOAT)translateX, (FLOAT)translateY, 0);
		D3DXMATRIX rot;
		D3DXMatrixRotationZ(&rot, (FLOAT)angle);
		D3DXMATRIX scale;
		D3DXMatrixScaling(&scale, (FLOAT)scaleX, (FLOAT)scaleY, 1);

		D3DXMATRIX matWorld;
		D3DXMatrixIdentity(&matWorld);
		D3DXMatrixMultiply(&matWorld, &matWorld, &scale);	
		D3DXMatrixMultiply(&matWorld, &matWorld, &rot);	
		D3DXMatrixMultiply(&matWorld, &matWorld, &trans);	

		D3DXMATRIXA16 matView;
		D3DXVECTOR3 vEyePt(0.0f, 0.0f,-4.0f);
		D3DXVECTOR3 vLookatPt(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 vUpVec(0.0f, 1.0f, 0.0f);
		D3DXMatrixLookAtLH(&matView, &vEyePt, &vLookatPt, &vUpVec);

		D3DXMATRIXA16 matProj;
		D3DXMatrixOrthoLH(&matProj, (FLOAT)screenWidth, (FLOAT)screenHeight, 0, 100);

		D3DXMATRIX worldViewProjection = matWorld * matView * matProj;
		D3DXMatrixTranspose(&worldViewProjection, &worldViewProjection);

		return effect->SetMatrix("worldViewProj", &worldViewProjection);
	}	
	#pragma warning(default:4793)
}

namespace AI
{
	void Impl::SetWorldViewProjectionMatrix(IntPtr effect, int screenWidth, int screenHeight)
	{
		Matrixes((LPD3DXEFFECT)effect.ToPointer(), screenWidth, screenHeight);
	}

	void Impl::SetWorldViewProjectionMatrix(IntPtr effect, int screenWidth, int screenHeight, double translateX, double translateY, double angle, double scaleX, double scaleY)
	{
		Matrixes((LPD3DXEFFECT)effect.ToPointer(), screenWidth, screenHeight, translateX, translateY, angle, scaleX, scaleY);
	}
}