#pragma once

namespace AI
{
	public ref class Impl
	{
	public:
		static void SetWorldViewProjectionMatrix(IntPtr effect, int screenWidth, int screenHeight);
		static void SetWorldViewProjectionMatrix(IntPtr effect, int screenWidth, int screenHeight, double translateX, double translateY, double angle, double scaleX, double scaleY);
	};
}