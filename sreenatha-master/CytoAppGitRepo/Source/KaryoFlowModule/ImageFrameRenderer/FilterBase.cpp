#include "stdafx.h"
#include "FilterBase.h"
#include <vcclr.h>
using namespace System::IO;
using namespace System::Runtime::InteropServices;
using namespace System::Reflection;

namespace AI
{
	FilterBase::FilterBase(String^ fxFilePath, Object^ parent, IntPtr devicePtr, int w, int h) : RenderPass(parent)
	{
		LPDIRECT3DDEVICE9 device = (LPDIRECT3DDEVICE9)devicePtr.ToPointer();

		LPD3DXEFFECT tempEffect;
		LPD3DXBUFFER pBufferErrors = NULL;

		Stream ^ stream = Assembly::GetCallingAssembly()->GetManifestResourceStream("AI." + fxFilePath);
		System::String^ fx = nullptr;

		if (stream != nullptr)
		{
			StreamReader ^ sr = gcnew StreamReader(stream);
			fx = sr->ReadToEnd();
			stream->Close();
		}

		char* fxs = (char*)(void*)Marshal::StringToHGlobalAnsi(fx);
		D3DXCreateEffect(device, (void*)fxs, strlen(fxs), NULL, NULL, 0, NULL, &tempEffect, &pBufferErrors);
		Marshal::FreeHGlobal((IntPtr)fxs);

		effect = tempEffect;
		Impl::SetWorldViewProjectionMatrix(IntPtr(effect), w, h);

		effect->SetFloat("onePixelX", 1.0f / w);
		effect->SetFloat("onePixelY", 1.0f / h);
	}

	void FilterBase::SetFloat(String^ parameter, double value)
	{
		char* t = (char*)(void*)Marshal::StringToHGlobalAnsi(parameter);
		if(effect == 0)
			return;

		HRESULT hr = effect->SetFloat(t, (float)value);
		hr;
		Marshal::FreeHGlobal((IntPtr)t);
	}

	void FilterBase::SetTechnique(String^ technique)
	{
		char* t = (char*)(void*)Marshal::StringToHGlobalAnsi(technique);
		effect->SetTechnique(t);
		Marshal::FreeHGlobal((IntPtr)t);
	}

	void FilterBase::Render(IntPtr device, Snap^ snap, IntPtr inputTexturePtr, IQuadRenderer^ quadRenderer)
	{
		HRESULT hr;
		LPDIRECT3DTEXTURE9 inputTexture = (LPDIRECT3DTEXTURE9)inputTexturePtr.ToPointer();

		hr = effect->SetTexture("source", inputTexture);

		UINT passes = 0;
		hr = effect->Begin(&passes, 0);
		for(UINT pass = 0; pass < passes; ++pass)
		{
			hr = effect->BeginPass(pass);
			hr = quadRenderer->RenderQuad();
			hr = effect->EndPass();
		}
		hr = effect->End();
	}

	void FilterBase::Release()
	{
		if(effect)
		{
			effect->Release();
			effect = 0;
		}
	}
}