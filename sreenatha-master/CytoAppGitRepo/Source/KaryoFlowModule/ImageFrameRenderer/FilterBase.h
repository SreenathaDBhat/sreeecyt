#pragma once
#include "RenderParameters.h"
#include "Bits.h"

namespace AI
{
	public ref class FilterBase abstract : RenderPass
	{
	private:
		LPD3DXEFFECT effect;

	protected:
		explicit FilterBase(String^ fxFilePath, Object^ parent, IntPtr devicePtr, int w, int h);
		void SetTechnique(String^ technique);
		void SetFloat(String^ parameter, double value);

	public:
		virtual void Render(IntPtr device, Snap^ snap, IntPtr inputTexturePtr, IQuadRenderer^ quadRenderer) override;
		virtual void Release() override;
	};
}