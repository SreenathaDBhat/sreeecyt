#pragma once

namespace AI
{
	public interface class IQuadRenderer
	{
	public:
		virtual HRESULT RenderQuad() = 0;
	};
}