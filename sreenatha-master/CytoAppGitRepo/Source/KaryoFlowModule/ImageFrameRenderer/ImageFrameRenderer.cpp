#include "stdafx.h"
#include "ImageFrameRenderer.h"
#include "Bits.h"

using namespace System::IO;
using namespace System::Runtime::InteropServices;
using namespace System::Reflection;

LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    return DefWindowProc(hWnd, msg, wParam, lParam);
}
WNDCLASSEX g_wc= { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L, GetModuleHandle(NULL), NULL, NULL, NULL, NULL, L"AI", NULL };

HRESULT hr;


namespace AI
{
	ImageFrameRenderer::ImageFrameRenderer()
	{
		d3d = 0;
		device = 0;
		renderSurfaceScreen = 0;
		quadVertexBuffer = 0;
		effect = 0;
		channelsInvalidated = false;
		channelsInvalidated = false;
		SetValue(IsLoFiPropertyKey, true);
		loadMode = ImageLoadingModes::Staggered;
		noSixteenBits = false;

		loFiQueue = gcnew BlobLoadQueue(this->Dispatcher);
		hiFiQueue = gcnew BlobLoadQueue(this->Dispatcher);
		loFiQueue->BlobLoaded += gcnew EventHandler<BlobLoadedEventArgs^>(this, &ImageFrameRenderer::LoFiLoaded);
		hiFiQueue->BlobLoaded += gcnew EventHandler<BlobLoadedEventArgs^>(this, &ImageFrameRenderer::HiFiLoaded);

		renderChannels = gcnew List<RenderChannel^>;

		d3dImage = gcnew D3DImage;
		d3dImage->IsFrontBufferAvailableChanged += gcnew DependencyPropertyChangedEventHandler(this, &ImageFrameRenderer::OnIsFrontBufferAvailableChanged);

		filterPasses = gcnew List<RenderPass^>;
		filterPassTemplates = gcnew List<IRenderPassTemplate^>;

		this->Source = d3dImage;
		Initialize();
	}

	void ImageFrameRenderer::Initialize()
	{
		RegisterClassEx(&g_wc);
		HWND hWnd = CreateWindow(L"AI", L"AI", WS_OVERLAPPEDWINDOW, 0, 0, 0, 0, NULL, NULL, g_wc.hInstance, NULL);
		window = System::IntPtr(hWnd);

		d3d = Direct3DCreate9(D3D_SDK_VERSION);
	}

	void ImageFrameRenderer::Release()
	{
		ReleaseDeviceDependants();
		SafeRelease(d3d);

		DestroyWindow((HWND)(void*)window);
	}

	void ImageFrameRenderer::OnIsFrontBufferAvailableChanged(Object^ o, DependencyPropertyChangedEventArgs e)
    {
		bool available = (bool)e.NewValue;
		
		if(available)
		{
			Initialize();
			InitializeDeviceDependants(ImageFrameX);
			Render();
		}
		else
		{
			Release();
		}        
    }

	void ImageFrameRenderer::OnImageFrameChanged(DependencyObject^ o, DependencyPropertyChangedEventArgs e)
    {
		ImageFrameRenderer^ r = (ImageFrameRenderer^)o;
		ImageFrame^ newFrame = (ImageFrame^)e.NewValue;
		ImageFrame^ oldFrame = (ImageFrame^)e.OldValue;

		r->channelsInvalidated = true;

		if(newFrame != nullptr)
		{
			r->SetValue(RenderParametersPropertyKey, gcnew ImageRenderParameters(newFrame));

			r->imageWidth = newFrame->ImageWidth;
			r->imageHeight = newFrame->ImageHeight;
		}

		if(r->d3d == 0)
			return;

		bool needToInitializeImageDependants = true;

		if(newFrame != nullptr && (oldFrame == nullptr || newFrame->ImageWidth != oldFrame->ImageWidth || newFrame->ImageHeight != oldFrame->ImageHeight))
		{
			r->ReleaseDeviceDependants();
		}

		if(r->device == 0)
		{
			r->InitializeDeviceDependants(newFrame);
			needToInitializeImageDependants = false; // (init DEVICE dependants calls init IMAGE dependants. dont want to do it twice...
		}

		if(newFrame != nullptr && needToInitializeImageDependants)
		{
			r->ReleaseImageDependants();
			r->InitializeImageDependants(newFrame);
		}
	}

	void ImageFrameRenderer::OnRenderParametersChanged(DependencyObject^ o, DependencyPropertyChangedEventArgs e)
	{
		ImageFrameRenderer^ r = (ImageFrameRenderer^)o;
		r->channelsInvalidated = true;
		r->Render();
	}

	void ImageFrameRenderer::InitializeDeviceDependants(ImageFrame^ image)
	{
		D3DCAPS9 caps;
		DWORD dwVertexProcessing = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
		if (SUCCEEDED(d3d->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &caps)))
		{
			if ((caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) == D3DDEVCAPS_HWTRANSFORMANDLIGHT)
			{
				dwVertexProcessing = D3DCREATE_HARDWARE_VERTEXPROCESSING;
			}

			
		}

		D3DPRESENT_PARAMETERS d3dpp;
		ZeroMemory(&d3dpp, sizeof(d3dpp));
		d3dpp.Windowed = TRUE;
		d3dpp.BackBufferHeight = 1;
		d3dpp.BackBufferWidth = 1;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		d3dpp.BackBufferFormat = D3DFMT_A8R8G8B8;

		LPDIRECT3DDEVICE9 tmpdevice;
		HWND hWnd = (HWND)window.ToPointer();
		hr = d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, dwVertexProcessing | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE, &d3dpp, &tmpdevice);
		device = tmpdevice;

		if(hr != S_OK)
			return;

		hr = device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
		hr = device->SetRenderState(D3DRS_LIGHTING, 0);
		hr = device->SetRenderState(D3DRS_ZENABLE, 0);

		LPDIRECT3DTEXTURE9 texTmp = 0;
		LPDIRECT3DSURFACE9 surfaceTmp = 0;
		hr = device->CreateRenderTarget(image->ImageWidth, image->ImageHeight, D3DFMT_A8R8G8B8, D3DMULTISAMPLE_NONE, 0, 1, &surfaceTmp, 0);		
		renderSurfaceScreen = surfaceTmp;

		hr = device->CreateTexture(image->ImageWidth, image->ImageHeight, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &texTmp, 0);		
		texTmp->GetSurfaceLevel(0, &surfaceTmp);
		renderTexture8 = texTmp;
		renderSurface8 = surfaceTmp;

		D3DFORMAT textureFormat = D3DFMT_G16R16;
		hr = device->CreateTexture(image->ImageWidth, image->ImageHeight, 1, D3DUSAGE_RENDERTARGET, textureFormat, D3DPOOL_DEFAULT, &texTmp, 0);		
		if(hr != S_OK)
		{
			textureFormat = D3DFMT_A8R8G8B8;
			noSixteenBits = true;
			hr = device->CreateTexture(image->ImageWidth, image->ImageHeight, 1, D3DUSAGE_RENDERTARGET, textureFormat, D3DPOOL_DEFAULT, &texTmp, 0);		
		}

		texTmp->GetSurfaceLevel(0, &surfaceTmp);
		renderTextureA16 = texTmp;
		renderSurfaceA16 = surfaceTmp;
		hr = device->CreateTexture(image->ImageWidth, image->ImageHeight, 1, D3DUSAGE_RENDERTARGET, textureFormat, D3DPOOL_DEFAULT, &texTmp, 0);		
		texTmp->GetSurfaceLevel(0, &surfaceTmp);
		renderTextureB16 = texTmp;
		renderSurfaceB16 = surfaceTmp;

		float hw = (float)image->ImageWidth / 2.0f;
		float hh = (float)image->ImageHeight / 2.0f;

		Vertex quadVertices[] =
		{
			{-hw, hh, 0.0f,  0.0f,0.0f,  0.0f,0.0f },
			{ hw, hh, 0.0f,  1.0f,0.0f,  1.0f,0.0f },
			{-hw,-hh, 0.0f,  0.0f,1.0f,  0.0f,1.0f },
			{ hw,-hh, 0.0f,  1.0f,1.0f,  1.0f,1.0f }
		};

		LPDIRECT3DVERTEXBUFFER9 tmpvb;
		hr = device->CreateVertexBuffer(6*sizeof(Vertex), 0, VertexFormat, D3DPOOL_DEFAULT, &tmpvb, NULL);
		quadVertexBuffer = tmpvb;

		VOID* pVertices;
		hr = quadVertexBuffer->Lock(0, 6*sizeof(Vertex), (void**)&pVertices, 0);
		memcpy(pVertices, quadVertices, 6*sizeof(Vertex));
		hr = quadVertexBuffer->Unlock();

		LPD3DXEFFECT tempEffect;
		LPD3DXBUFFER pBufferErrors = NULL;

#ifdef localFile
		hr = D3DXCreateEffectFromFile(device, L"imageFrameRenderer.fx", NULL, NULL, 0, NULL, &tempEffect, &pBufferErrors);
#else
		Stream ^ stream = Assembly::GetExecutingAssembly()->GetManifestResourceStream("imageFrameRenderer.fx");
		System::String^ fx = nullptr;

		if (stream != nullptr)
		{
			StreamReader ^ sr = gcnew StreamReader(stream);
			fx = sr->ReadToEnd();
			stream->Close();
		}
		
		char* fxs = (char*)(void*)Marshal::StringToHGlobalAnsi(fx);
		hr = D3DXCreateEffect(device, (void*)fxs, strlen(fxs), NULL, NULL, 0, NULL, &tempEffect, &pBufferErrors);

		Marshal::FreeHGlobal((IntPtr)fxs);
#endif
		effect = tempEffect;
		Impl::SetWorldViewProjectionMatrix(IntPtr(effect), image->ImageWidth, image->ImageHeight);

		hr = device->CreateTexture(image->ImageWidth, image->ImageHeight, 1, D3DUSAGE_DYNAMIC, textureFormat, D3DPOOL_SYSTEMMEM, &texTmp, 0);
		texTmp->GetSurfaceLevel(0, &surfaceTmp);
		swapSurface16 = surfaceTmp;
		swapTexture16 = texTmp;

		hr = device->CreateTexture(image->ImageWidth, image->ImageHeight, 1, D3DUSAGE_DYNAMIC, D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM, &texTmp, 0);
		texTmp->GetSurfaceLevel(0, &surfaceTmp);
		swapSurface8 = surfaceTmp;
		swapTexture8 = texTmp;

		histogramLow = effect->GetParameterByName(0, "low");
		histogramHigh = effect->GetParameterByName(0, "high");
		offsetX = effect->GetParameterByName(0, "offsetX");
		offsetY = effect->GetParameterByName(0, "offsetY");
		sourceChannelTexture = effect->GetParameterByName(0, "channelTexture");
		destinationCompositeTexture = effect->GetParameterByName(0, "destinationCompositeTexture");
		renderColor = effect->GetParameterByName(0, "renderColor");

		InitializeImageDependants(image);
	}

	void ImageFrameRenderer::ReleaseDeviceDependants()
	{
		if(filterPasses != nullptr)
		{
			for each(RenderPass^ pass in filterPasses)
				pass->Release();

			filterPasses->Clear();
		}

		ReleaseImageDependants();

		if(PostRenderEffect != nullptr)
			PostRenderEffect->Release();

		SafeRelease(effect);
		SafeRelease(swapSurface16);
		SafeRelease(swapTexture16);
		SafeRelease(swapSurface8);
		SafeRelease(swapTexture8);
		SafeRelease(renderSurfaceScreen);
		SafeRelease(renderSurfaceA16);
		SafeRelease(renderSurfaceB16);
		SafeRelease(renderTextureA16);
		SafeRelease(renderTextureB16);
		SafeRelease(renderTexture8);
		SafeRelease(renderSurface8);
		SafeRelease(quadVertexBuffer);
		SafeRelease(device);
	}

	void ImageFrameRenderer::InitializeImageDependants(ImageFrame^ image)
	{
		if(noSixteenBits)
			hr = effect->SetFloat("bitsPerPixelMultiplier", 1);
		else
			hr = effect->SetFloat("bitsPerPixelMultiplier", 65536.0f / pow(2.0f, (float)image->ImageBitsPerPixel));
			
		hr = effect->SetFloat("onePixelX", (float)(1.0 / image->ImageWidth));
		hr = effect->SetFloat("onePixelY", (float)(1.0 / image->ImageHeight));
		hr = effect->SetTechnique("DefaultTechnique");

		SetValue(IsLoadingPropertyKey, true);
		SetValue(IsLoFiPropertyKey, true);
		
		if(ImageLoadingMode != ImageLoadingModes::HiFiOnly)
		{
			SetValue(IsAnythingOnDisplayPropertyKey, false);

			String^ thumb = image->Id.ToString() + ".jpg";
			loFiQueue->Enqueue(CaseId, Connect::CacheOption::Cache, image, thumb);			
		}
		
		if(ImageLoadingMode != ImageLoadingModes::LoFiOnly)
		{
			List<BlobStreamData^>^ blobsToLoad = gcnew List<BlobStreamData^>;
			for each(ChannelInfo^ channel in image->Channels)
			{
				BlobStreamData^ load = gcnew BlobStreamData;
				Snap^ snap = image->FindSnap(channel, -1);
				load->BlobPath = snap->ImageDataFile;
				load->Tag = snap;
				blobsToLoad->Add(load);
			}

			hiFiQueue->Enqueue(CaseId, Connect::CacheOption::Cache, blobsToLoad, image);
		}
	}

	void ImageFrameRenderer::LoFiLoaded(Object^ sender, BlobLoadedEventArgs^ e)
	{
		ImageFrame^ loadedFrame = (ImageFrame^)e->Tag;
		if(loadedFrame != ImageFrameX || IsLoFi == false)
			return;

		if(renderChannels->Count != 0)
			throw gcnew ApplicationException("ERROR: should have no channels loaded (LOFI)");

		SetValue(IsLoFiPropertyKey, true);
		Source = ImageFromBitmapSource::LoadBitmapImageFromStream(-1, e->StreamFor(ImageFrameX));

		SetValue(IsAnythingOnDisplayPropertyKey, true);
		if(ImageLoadingMode == ImageLoadingModes::LoFiOnly)
			SetValue(IsLoadingPropertyKey, false);
	}

	void ImageFrameRenderer::HiFiLoaded(Object^ sender, BlobLoadedEventArgs^ e)
	{
		ImageFrame^ loaded = (ImageFrame^)e->Tag;
		if(device == 0 || loaded != ImageFrameX)
		{
			loaded->DumpAllImageData();
			return;
		}

		if(renderChannels->Count != 0)
			throw gcnew ApplicationException("ERROR: should have no channels loaded (HIFI)");

		for each(ChannelInfo^ channel in ImageFrameX->Channels)
		{
			AI::Snap^ snap = ImageFrameX->FindSnap(channel, -1);
			snap->ChannelPixels = ImageFromBitmapSource::LoadFromStream(e->StreamFor(snap));
			RenderChannel^ newChannel = gcnew RenderChannel(snap, IntPtr(device), IntPtr(swapSurface16), noSixteenBits);
			renderChannels->Add(newChannel);
		}

		channelsInvalidated = true;
		RecreateFiltersForNewImage();
		Render();

		SetValue(IsLoFiPropertyKey, false);
		SetValue(IsAnythingOnDisplayPropertyKey, true);
		SetValue(IsLoadingPropertyKey, false);
		Source = d3dImage;

		HiFiImageLoaded(this, EventArgs::Empty);
	}

	void ImageFrameRenderer::ReleaseImageDependants()
	{
		if (effect)
		{
			effect->SetTexture(sourceChannelTexture, 0);
			effect->SetTexture(destinationCompositeTexture, 0);
		}

		for each(RenderChannel^ channel in renderChannels)
		{
			channel->Release();
		}

		renderChannels->Clear();
	}

	void ImageFrameRenderer::RefreshImage()
	{
		ReleaseImageDependants();
		ReleaseDeviceDependants();
		InitializeDeviceDependants(ImageFrameX);
		Render();
	}

	void ImageFrameRenderer::OnPostRenderEffectChanged(DependencyObject^ o, DependencyPropertyChangedEventArgs e)
	{
		if(e.OldValue != nullptr)
			((IPostRenderEffect^)e.OldValue)->Release();

		((ImageFrameRenderer^)o)->Render();
	}

	void ImageFrameRenderer::OnZStackIndexChanged(DependencyObject^ o, DependencyPropertyChangedEventArgs e)
	{
		int z = (int)e.NewValue;
		((ImageFrameRenderer^)o)->UpdateZ();
	}

	void ImageFrameRenderer::FilterChannel(RenderChannel^ channel, ChannelRenderParameters^ channelParams)
	{
		if(filterPasses == nullptr)
			return;

		int passesToExecute = 0;
		for each(RenderPass^ pass in filterPasses)
		{
			if(pass->RunsOn(channel->ImageSnap->ChannelInfo))
				++passesToExecute;
		}

		if(passesToExecute > 0)
		{
			LPDIRECT3DSURFACE9 surf = channel->Filter(filterPasses, this, device, swapSurface16, renderSurfaceA16, renderTextureA16, renderSurfaceB16, renderTextureB16);
			UpdateHistogram(channelParams, surf);
		}
		else
		{
			channel->ResetSurface(device, swapSurface16);
			UpdateHistogram(channelParams, swapSurface16);
		}
	}

	void ImageFrameRenderer::UpdateZ()
	{
		bool needsRender = false;

		for each(RenderChannel^ rc in renderChannels)
		{
			ChannelInfo^ ci = rc->ImageSnap->ChannelInfo;
			AI::Snap^ snap = ImageFrameX->FindSnap(ci, ZStackIndex);
			if(snap != rc->ImageSnap)
			{
				Load::LoadDataIfNeeded(snap, CaseId);
				rc->ImageSnap = snap;
				//rc->ResetSurface(device, swapSurface16);
				needsRender = true;
			}
		}

		if(needsRender)
		{
			channelsInvalidated = true;
			Render();
		}
	}

	HRESULT ImageFrameRenderer::RenderQuad()
	{
		hr = device->SetStreamSource(0, quadVertexBuffer, 0, sizeof(Vertex));
		if(!SUCCEEDED(hr)) return hr;
		hr = device->SetFVF(VertexFormat);
		if(!SUCCEEDED(hr)) return hr;
		hr = device->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
		if(!SUCCEEDED(hr)) return hr;

		return ERROR_SUCCESS;
	}

	void ImageFrameRenderer::Render()
	{
		Render(true);
	}

	void ImageFrameRenderer::Render(bool updateD3DImage)
	{
		if(ImageFrameX == nullptr || device == 0 || RenderParameters == nullptr)
			return;

		device->BeginScene();

		if(!channelsInvalidated)
		{
			for each(RenderPass^ pass in filterPasses)
			{
				if(pass->IsInvalidated)
				{
					channelsInvalidated = true;
				}

				pass->IsInvalidated = false;
			}
		}

		if(channelsInvalidated)
		{
			channelsInvalidated = false;

			for each(RenderChannel^ channel in renderChannels)
			{
				ChannelRenderParameters^ channelParams = nullptr;
				channelParams = RenderParameters->ParametersForChannel(channel->ImageSnap->ChannelInfo);
				if (channelParams)
				{
					FilterChannel(channel, channelParams);
					HistogramInvalidated->Execute(channelParams, this);
				}
			}
		}

		device->Clear(0, 0, D3DCLEAR_TARGET, 0, 1, 0);

		hr = effect->SetTechnique("Set");
		hr = effect->SetInt("forceLowBitDepth", ForceLowBitDepth ? 3 : 0);
		hr = device->SetRenderTarget(0, renderSurfaceScreen);
		hr = effect->SetTexture(destinationCompositeTexture, renderTexture8);

		int numRenderChannels = renderChannels->Count;
		int numVisibleChannels = 0;
		for(int i = 0; i < numRenderChannels; ++i)
		{
			RenderChannel^ channel = renderChannels[i];
			ChannelRenderParameters^ channelParams = channelParams = RenderParameters->ParametersForChannel(channel->ImageSnap->ChannelInfo);
			if(channelParams->IsVisible)
				++numVisibleChannels;
		}

		for(int i = 0; i < numRenderChannels; ++i)
		{
			RenderChannel^ channel = renderChannels[i];
			ChannelRenderParameters^ channelParams = channelParams = RenderParameters->ParametersForChannel(channel->ImageSnap->ChannelInfo);
			if(channelParams == nullptr || !channelParams->IsVisible)
				continue;

			if(numVisibleChannels > 1 && AttenuateCounterstain && channelParams->ChannelInfoX->IsCounterstain)
			{
				continue;
			}

			RenderSingleChannel(channelParams, channel);
			hr = effect->SetTechnique("Add");
		}

		if(numVisibleChannels > 1 && AttenuateCounterstain)
		{
			hr = effect->SetTechnique("Attenuate");

			for(int i = 0; i < numRenderChannels; ++i)
			{
				RenderChannel^ channel = renderChannels[i];
				ChannelRenderParameters^ channelParams = channelParams = RenderParameters->ParametersForChannel(channel->ImageSnap->ChannelInfo);

				if(channelParams == nullptr || !channelParams->IsVisible || !channelParams->ChannelInfoX->IsCounterstain)
					continue;

				RenderSingleChannel(channelParams, channel);
			}
		}


		if(PostRenderEffect != nullptr)
		{
			hr = device->GetRenderTargetData(renderSurfaceScreen, swapSurface8);
			hr = device->UpdateSurface(swapSurface8, 0, renderSurface8, 0);
			PostRenderEffect->Render(IntPtr(device), IntPtr(renderTexture8), IntPtr(renderSurfaceScreen), this, ImageFrameX->ImageWidth, ImageFrameX->ImageHeight);
		}



		hr = device->EndScene();

		if(updateD3DImage)
		{
			try
			{
			d3dImage->Lock();
			d3dImage->SetBackBuffer(System::Windows::Interop::D3DResourceType::IDirect3DSurface9, IntPtr(renderSurfaceScreen));
			d3dImage->AddDirtyRect(Int32Rect(0, 0, ImageFrameX->ImageWidth, ImageFrameX->ImageHeight));
			d3dImage->Unlock();
			}
			catch(ArgumentException^)
			{
				//System::Diagnostics::Debug::Fail("Backbuffer");
				ReleaseDeviceDependants();
			}
		}
	}

	void ImageFrameRenderer::RenderSingleChannel(ChannelRenderParameters^ channelParams, RenderChannel^ channel)
	{
		UINT effectPasses;
		hr = effect->SetFloat(histogramLow, (FLOAT)channelParams->Low);
		hr = effect->SetFloat(histogramHigh, (FLOAT)channelParams->High);
	
		System::Windows::Media::Color color = channel->ImageSnap->ChannelInfo->RenderColor;
		float c[] = 
		{ 
			(float)color.R / 255.0f, 
			(float)color.G / 255.0f, 
			(float)color.B / 255.0f, 
			1.0f 
		};

		hr = effect->SetFloat(offsetX, (float)channelParams->OffsetX);
		hr = effect->SetFloat(offsetY, -(float)channelParams->OffsetY);
		hr = effect->SetFloatArray(renderColor, c, 4);
		hr = effect->SetTexture(sourceChannelTexture, (LPDIRECT3DTEXTURE9)channel->Texture.ToPointer());
		hr = effect->Begin(&effectPasses, 0);

		for(UINT pass = 0; pass < effectPasses; ++pass)
		{
			hr = effect->BeginPass(pass);
			RenderQuad();
			hr = effect->EndPass();
		}

		hr = effect->End();

		hr = device->GetRenderTargetData(renderSurfaceScreen, swapSurface8);
		hr = device->UpdateSurface(swapSurface8, 0, renderSurface8, 0);
	}

	array<unsigned char>^ ImageFrameRenderer::CopyPixels()
	{
		return CopyPixels(Int32Rect(0, 0, ImageFrameX->ImageWidth, ImageFrameX->ImageHeight));
	}

	array<unsigned char>^ ImageFrameRenderer::CopyPixels(Int32Rect roi)
	{
		int roiLeft = max(0, roi.X);
		int roiTop = max(0, roi.Y);
		int roiRight = min(imageWidth, roi.X + roi.Width);
		int roiBottom = min(imageHeight, roi.Y + roi.Height);

		int negX = roi.X < 0 ? -roi.X : 0;
		int negY = roi.Y < 0 ? -roi.Y : 0;

		hr = device->GetRenderTargetData(renderSurfaceScreen, swapSurface8);
		
		D3DLOCKED_RECT lock;
		hr = swapSurface8->LockRect(&lock, 0, 0);

		array<unsigned char>^ bytes = gcnew array<unsigned char>(roi.Width * roi.Height * 4);
		pin_ptr<unsigned char> dest = &bytes[0];
		unsigned char* src = (unsigned char*)lock.pBits;
		int w = imageWidth;

		for(int y = roi.Y; y < roiBottom; ++y)
		{
			src = ((unsigned char*)lock.pBits) + (w * (y + negY) * 4) + ((roi.X + negX) * 4);
			unsigned char* end = src + roi.Width * 4;

			while(src != end)
			{
				*dest++ = *src++;
			}
		}

		swapSurface8->UnlockRect();

		return bytes;
	}

	void ImageFrameRenderer::UpdateHistogram(ChannelRenderParameters^ channelParams, LPDIRECT3DSURFACE9 surface)
	{
		HRESULT hr;

		D3DLOCKED_RECT lock;
		hr = surface->LockRect(&lock, 0, 0);
		array<int>^ bins = gcnew array<int>(256);
		int maxCount = 0;

		if(noSixteenBits)
		{
			unsigned char* data16 = (unsigned char*)lock.pBits;
			unsigned char* pixel = data16;
			unsigned char* endOfPixels = data16 + (ImageFrameX->ImageWidth * ImageFrameX->ImageHeight) * 2;
			double maxPixelValue = 255;
			
			while(pixel != endOfPixels)
			{
				short r = *pixel++;
				pixel++; // g is not used

				int v = max(0, min(255, (int)((r / maxPixelValue) * 256)));
				bins[v] = bins[v] + 1;
				if(v < 255)
					maxCount = max(maxCount, bins[v]);
			}
		}
		else
		{
			short* data16 = (short*)lock.pBits;
			short* pixel = data16;
			short* endOfPixels = data16 + (ImageFrameX->ImageWidth * ImageFrameX->ImageHeight) * 2;
			double maxPixelValue = pow(2.0, ImageFrameX->ImageBitsPerPixel) - 1;

			while(pixel != endOfPixels)
			{
				short r = *pixel++;
				pixel++; // g is not used

				int v = max(0, min(255, (int)((r / maxPixelValue) * 256)));
				bins[v] = bins[v] + 1;
				if(v < 255)
					maxCount = max(maxCount, bins[v]);
			}
		}

		
		for(int i = 0; i < 256; ++i)
		{
			int v = (int)((((double)bins[i]) / maxCount) * 1000);
			v = Math::Min(1000, v);
			bins[i] = v;
		}
		
		hr = swapSurface16->UnlockRect();
		channelParams->HistogramBins = bins;
	}

	void ImageFrameRenderer::ForceInvalidateFilters()
	{
		channelsInvalidated = true;
	}

	void ImageFrameRenderer::OnCaseIdChanged(DependencyObject^ o, DependencyPropertyChangedEventArgs e)
	{
		((ImageFrameRenderer^)o)->caseId = (Guid)e.NewValue;
	}

	void ImageFrameRenderer::OnImageLoadingModeChanged(DependencyObject^ o, DependencyPropertyChangedEventArgs e)
	{
		((ImageFrameRenderer^)o)->loadMode = (ImageLoadingModes)e.NewValue;
	}



	void ImageFrameRenderer::ActivateFilter(IRenderPassTemplate^ filter)
	{
		filter->IsActive = true;
		int bpp = noSixteenBits ? 8 : ImageFrameX->ImageBitsPerPixel;
		RenderPass^ pass = filter->CreatePass(IntPtr(device), ImageFrameX, this, bpp);
		filterPasses->Add(pass);

		if(!filterPassTemplates->Contains(filter))
			filterPassTemplates->Add(filter);
		channelsInvalidated = true;
	}

	void ImageFrameRenderer::DeactivateFilter(IRenderPassTemplate^ filter)
	{
		filter->IsActive = false;
		for(int i = filterPasses->Count-1; i >= 0; --i)
		{
			if((IRenderPassTemplate^)filterPasses[i]->Parent == filter)
			{
				filterPasses[i]->Release();
				filterPasses->RemoveAt(i);
				channelsInvalidated = true;
			}
		}

		filterPassTemplates->Remove(filter);
	}

	void ImageFrameRenderer::ClearFilters()
	{
		if(filterPasses != nullptr)
		{
			for each(RenderPass^ pass in filterPasses)
				pass->Release();

			filterPasses->Clear();
		}
	}

	void ImageFrameRenderer::RecreateFiltersForNewImage()
	{
		int bpp = noSixteenBits ? 8 : ImageFrameX->ImageBitsPerPixel;

		for(int i = 0; i < filterPasses->Count; ++i)
		{
			filterPasses[i]->Release();
		}

		filterPasses->Clear();

		
		for each(IRenderPassTemplate^ t in filterPassTemplates)
		{
			RenderPass^ pass = t->CreatePass(IntPtr(device), ImageFrameX, this, bpp);
			filterPasses->Add(pass);
		}

		channelsInvalidated = true;
	}

	bool ImageFrameRenderer::IsPassActive(IRenderPassTemplate^ filter)
	{
		for each(RenderPass^ pass in filterPasses)
		{
			if((IRenderPassTemplate^)pass->Parent == filter)
				return true;
		}

		return false;
	}
}
