#pragma once
#include "RenderParameters.h"
#include "IQuadRenderer.h"
#include "RenderObject.h"
#include "RenderChannel.h"
using namespace System::Windows::Input;

namespace AI
{
	enum class ImageLoadingModes
	{
		LoFiOnly,
		HiFiOnly,
		Staggered
	};

	public interface class IPostRenderEffect
	{
	public:
		virtual void Render(IntPtr device, IntPtr sourceTexture, IntPtr destRenderSurface, IQuadRenderer^ quadRenderer, int w, int h) = 0;
		virtual void Release() = 0;
	};

	public ref class IHistogramEstimator
	{
	public:
		virtual void Estimate(ImageFrame^ imageFrame, ChannelRenderParameters^ channelParams, IEnumerable<RenderPass^>^ passes)
		{
		}
	};



	public ref class ImageFrameRenderer : public Image, public IQuadRenderer
	{
	private:
		D3DXHANDLE histogramLow;
		D3DXHANDLE histogramHigh;
		D3DXHANDLE renderColor;
		D3DXHANDLE sourceChannelTexture;
		D3DXHANDLE destinationCompositeTexture;
		D3DXHANDLE offsetX;
		D3DXHANDLE offsetY;

		static DependencyPropertyKey^ IsLoFiPropertyKey = DependencyProperty::RegisterReadOnly("IsLoFi", bool::typeid, ImageFrameRenderer::typeid, gcnew FrameworkPropertyMetadata(false));
		static DependencyPropertyKey^ IsLoadingPropertyKey = DependencyProperty::RegisterReadOnly("IsLoading", bool::typeid, ImageFrameRenderer::typeid, gcnew FrameworkPropertyMetadata(false));
		static DependencyPropertyKey^ IsAnythingOnDisplayPropertyKey = DependencyProperty::RegisterReadOnly("IsAnythingOnDisplay", bool::typeid, ImageFrameRenderer::typeid, gcnew FrameworkPropertyMetadata(false));
		static DependencyPropertyKey^ RenderParametersPropertyKey = DependencyProperty::RegisterReadOnly("RenderParameters", ImageRenderParameters::typeid, ImageFrameRenderer::typeid, gcnew FrameworkPropertyMetadata((ImageRenderParameters^)nullptr));

	public:
		static RoutedUICommand^ HistogramInvalidated = gcnew RoutedUICommand("HistogramInvalidated", "HistogramInvalidated", ImageFrameRenderer::typeid);

		static DependencyProperty^ AvailableFiltersProperty = DependencyProperty::Register("AvailableFilters", IEnumerable<IRenderPassTemplate^>::typeid, ImageFrameRenderer::typeid, gcnew FrameworkPropertyMetadata((IEnumerable<IRenderPassTemplate^>^)nullptr));
		static DependencyProperty^ CaseIdProperty = DependencyProperty::Register("CaseId", Guid::typeid, ImageFrameRenderer::typeid, gcnew FrameworkPropertyMetadata(gcnew PropertyChangedCallback(&OnCaseIdChanged)));
		static DependencyProperty^ ImageFrameXProperty = DependencyProperty::Register("ImageFrameX", ImageFrame::typeid, ImageFrameRenderer::typeid, gcnew FrameworkPropertyMetadata(gcnew PropertyChangedCallback(&OnImageFrameChanged)));
		static DependencyProperty^ PostRenderEffectProperty = DependencyProperty::Register("PostRenderEffect", IPostRenderEffect::typeid, ImageFrameRenderer::typeid, gcnew FrameworkPropertyMetadata(gcnew PropertyChangedCallback(&OnPostRenderEffectChanged)));
		static DependencyProperty^ ZStackIndexProperty = DependencyProperty::Register("ZStackIndex", int::typeid, ImageFrameRenderer::typeid, gcnew FrameworkPropertyMetadata(gcnew PropertyChangedCallback(&OnZStackIndexChanged)));
		static DependencyProperty^ HistogramEstimatorProperty = DependencyProperty::Register("HistogramEstimator", IHistogramEstimator::typeid, ImageFrameRenderer::typeid, gcnew FrameworkPropertyMetadata(gcnew IHistogramEstimator));
		static DependencyProperty^ ImageLoadingModeProperty = DependencyProperty::Register("ImageLoadingMode", ImageLoadingModes::typeid, ImageFrameRenderer::typeid, gcnew FrameworkPropertyMetadata(ImageLoadingModes::Staggered, gcnew PropertyChangedCallback(&OnImageLoadingModeChanged)));
		static DependencyProperty^ AttenuateCounterstainProperty = DependencyProperty::Register("AttenuateCounterstain", bool::typeid, ImageFrameRenderer::typeid, gcnew FrameworkPropertyMetadata(false));
		static DependencyProperty^ ForceLowBitDepthProperty = DependencyProperty::Register("ForceLowBitDepth", bool::typeid, ImageFrameRenderer::typeid, gcnew FrameworkPropertyMetadata(false));

		static DependencyProperty^ IsLoFiProperty = IsLoFiPropertyKey->DependencyProperty;
		static DependencyProperty^ IsLoadingProperty = IsLoadingPropertyKey->DependencyProperty;
		static DependencyProperty^ IsAnythingOnDisplayProperty = IsAnythingOnDisplayPropertyKey->DependencyProperty;
		static DependencyProperty^ RenderParametersProperty = RenderParametersPropertyKey->DependencyProperty;

		event EventHandler^ HiFiImageLoaded;
		event EventHandler^ LoFiImageLoaded;

		ImageFrameRenderer();

		void Initialize();
		void Release();
		void Render();
		void Render(bool updateD3DImage);
		void RefreshImage();
		void ForceInvalidateFilters();
		void RecreateFiltersForNewImage();
		void ClearFilters();

		void ActivateFilter(IRenderPassTemplate^ filter);
		void DeactivateFilter(IRenderPassTemplate^ filter);
		bool IsPassActive(IRenderPassTemplate^ filter);

		array<unsigned char>^ CopyPixels();
		array<unsigned char>^ CopyPixels(Int32Rect roi);

		virtual HRESULT RenderQuad();

		property ImageFrame^ ImageFrameX // bastard c++ wont let me use ImageFrame. its a type name. So stick an X on the end for now.
		{
			ImageFrame^ get() { return (ImageFrame^)GetValue(ImageFrameXProperty); }
			void set(ImageFrame^ value) { SetValue(ImageFrameXProperty, value); }
		}

		property IEnumerable<IRenderPassTemplate^>^ AvailableFilters
		{
			IEnumerable<IRenderPassTemplate^>^ get() { return (IEnumerable<IRenderPassTemplate^>^)GetValue(AvailableFiltersProperty); } 
			void set(IEnumerable<IRenderPassTemplate^>^ v) { SetValue(AvailableFiltersProperty, v); }
		}

		property IPostRenderEffect^ PostRenderEffect
		{
			IPostRenderEffect^ get() { return (IPostRenderEffect^)GetValue(PostRenderEffectProperty); }
			void set(IPostRenderEffect^ value) { SetValue(PostRenderEffectProperty, value); }
		}

		property Guid CaseId
		{
			Guid get() { return (Guid)GetValue(CaseIdProperty); }
			void set(Guid value) { SetValue(CaseIdProperty, value); }
		}

		property ImageRenderParameters^ RenderParameters
		{
			ImageRenderParameters^ get() { return (ImageRenderParameters^)GetValue(RenderParametersProperty); }
		}

		property int ZStackIndex
		{
			int get() { return (int)GetValue(ZStackIndexProperty); }
			void set(int value) { SetValue(ZStackIndexProperty, value); }
		}

		property bool IsLoFi
		{
			bool get() { return (bool)GetValue(IsLoFiProperty); }
		}

		property bool IsLoading
		{
			bool get() { return (bool)GetValue(IsLoadingProperty); }
		}

		property bool AttenuateCounterstain
		{
			bool get() { return (bool)GetValue(AttenuateCounterstainProperty); }
			void set(bool v) { SetValue(AttenuateCounterstainProperty, v); }
		}

		property bool ForceLowBitDepth
		{
			bool get() { return (bool)GetValue(ForceLowBitDepthProperty); }
			void set(bool v) { SetValue(ForceLowBitDepthProperty, v); }
		}

		property bool IsAnythingOnDisplay
		{
			bool get() { return (bool)GetValue(IsAnythingOnDisplayProperty); }
		}

		property ImageLoadingModes ImageLoadingMode
		{
			ImageLoadingModes get() { return (ImageLoadingModes)GetValue(ImageLoadingModeProperty); }
			void set(ImageLoadingModes mode) { SetValue(ImageLoadingModeProperty, mode); }
		}

		property IEnumerable<RenderPass^>^ Filters
		{
			IEnumerable<RenderPass^>^ get() { return filterPasses; }
		}

		property IHistogramEstimator^ HistogramEstimator
		{
			IHistogramEstimator^ get() { return (IHistogramEstimator^)GetValue(HistogramEstimatorProperty); }
			void set(IHistogramEstimator^ value) { SetValue(HistogramEstimatorProperty, value); }
		}

	private:
		void InitializeDeviceDependants(ImageFrame^);
		void ReleaseDeviceDependants();
		void InitializeImageDependants(ImageFrame^);
		void ReleaseImageDependants();
		void FilterChannel(RenderChannel^ channel, ChannelRenderParameters^ channelParams);
		void UpdateHistogram(ChannelRenderParameters^ channelParams, LPDIRECT3DSURFACE9 sourceTexture);
		void UpdateZ();
		void RenderSingleChannel(ChannelRenderParameters^ channelParams, RenderChannel^ channel);
		
		void OnIsFrontBufferAvailableChanged(Object^ o, DependencyPropertyChangedEventArgs e);
		static void OnImageFrameChanged(DependencyObject^ o, DependencyPropertyChangedEventArgs e);
		static void OnCaseIdChanged(DependencyObject^ o, DependencyPropertyChangedEventArgs e);
		static void OnPostRenderEffectChanged(DependencyObject^ o, DependencyPropertyChangedEventArgs e);
		static void OnZStackIndexChanged(DependencyObject^ o, DependencyPropertyChangedEventArgs e);
		static void OnRenderParametersChanged(DependencyObject^ o, DependencyPropertyChangedEventArgs e);
		static void OnImageLoadingModeChanged(DependencyObject^ o, DependencyPropertyChangedEventArgs e);
		

		IntPtr window;

		D3DImage^ d3dImage;
		LPDIRECT3D9             d3d;
		LPDIRECT3DDEVICE9       device;
		LPDIRECT3DSURFACE9      renderSurfaceScreen; //8bpp
		LPDIRECT3DSURFACE9      renderSurfaceA16;//16bpp
		LPDIRECT3DTEXTURE9      renderTextureA16;//16bpp
		LPDIRECT3DSURFACE9      renderSurfaceB16;//16bpp
		LPDIRECT3DTEXTURE9      renderTextureB16;//16bpp
		LPDIRECT3DVERTEXBUFFER9 quadVertexBuffer;
		LPD3DXEFFECT			effect;
		LPDIRECT3DTEXTURE9		swapTexture16;
		LPDIRECT3DSURFACE9		swapSurface16;
		LPDIRECT3DTEXTURE9		swapTexture8;
		LPDIRECT3DSURFACE9		swapSurface8;
		LPDIRECT3DSURFACE9		renderSurface8;
		LPDIRECT3DTEXTURE9		renderTexture8;

		List<RenderChannel^>^	renderChannels;
		List<RenderPass^>^		filterPasses;
		List<IRenderPassTemplate^>^	filterPassTemplates;
		bool					channelsInvalidated, noSixteenBits;


		System::Threading::Thread^ loadThread;
		ImageFrame^ frameToLoad;
		Guid caseId;
		ImageLoadingModes loadMode;
		int imageWidth, imageHeight;

		BlobLoadQueue^ loFiQueue;
		BlobLoadQueue^ hiFiQueue;

		void LoFiLoaded(Object^ sender, BlobLoadedEventArgs^ e);
		void HiFiLoaded(Object^ sender, BlobLoadedEventArgs^ e);
	};
}
