#pragma once

namespace AI
{
	public ref class RenderChannel
	{
	private:
		LPDIRECT3DTEXTURE9 texture;
		LPDIRECT3DSURFACE9 surface;
		Snap^ snap;
		bool force8bits;
		double gamma;

		void SeedSurface(LPDIRECT3DDEVICE9 device, LPDIRECT3DSURFACE9 swapSurface)
		{
			HRESULT hr;

			D3DLOCKED_RECT lock;
			hr = swapSurface->LockRect(&lock, 0, 0);
			int w = snap->ChannelPixels->Width;
			int h = snap->ChannelPixels->Height;
			int len = w * h;
			double maxPixelValue = pow(2.0, (double)snap->ChannelPixels->BitsPerPixel);
			
			if(force8bits)
			{
				double downSample = 256.0 / maxPixelValue;
				unsigned char* dest = (unsigned char*)lock.pBits;
				pin_ptr<unsigned short> original = &snap->ChannelPixels->Pixels[0];
				unsigned short* src = original;
				unsigned short* srcEnd = src + (w * h);

				while(src != srcEnd)
				{
					unsigned short s = *src++;
					double v = s * downSample;
					unsigned char c = (unsigned char)(v);
					*dest++ = c;
					*dest++ = c;
					*dest++ = c;
					*dest++ = c;
				}
			}
			else
			{
				unsigned short* dest = (unsigned short*)lock.pBits;
				pin_ptr<unsigned short> original = &snap->ChannelPixels->Pixels[0];
				unsigned short* src = original;
				unsigned short* srcEnd = src + (w * h);

				while(src != srcEnd)
				{
					unsigned short s = *src++;
					*dest++ = s;
					//*dest++ = (*src++);
					dest++;
				}
			}

			hr = swapSurface->UnlockRect();
			hr = device->UpdateSurface(swapSurface, 0, surface, 0);
		}

	public:
		RenderChannel(Snap^ snap, IntPtr devicePtr, IntPtr swapSurfacePtr, bool force8bits)
		{
			this->snap = snap;
			this->force8bits = force8bits;

			LPDIRECT3DDEVICE9 device = (LPDIRECT3DDEVICE9)devicePtr.ToPointer();
			LPDIRECT3DSURFACE9 swap = (LPDIRECT3DSURFACE9)swapSurfacePtr.ToPointer();

			D3DFORMAT texFormat = force8bits ? D3DFMT_A8R8G8B8 : D3DFMT_G16R16;

			LPDIRECT3DTEXTURE9 texTmp = 0;
			LPDIRECT3DSURFACE9 surfaceTmp = 0;
			HRESULT hr = device->CreateTexture(snap->ChannelPixels->Width, snap->ChannelPixels->Height, 1, D3DUSAGE_RENDERTARGET, texFormat, D3DPOOL_DEFAULT, &texTmp, 0);
			texTmp->GetSurfaceLevel(0, &surfaceTmp);
			texture = texTmp;
			surface = surfaceTmp;

			SeedSurface(device, swap);
		}

		void ResetSurface(LPDIRECT3DDEVICE9 device, LPDIRECT3DSURFACE9 swapSurface)
		{
			SeedSurface(device, swapSurface);
		}

		LPDIRECT3DSURFACE9 Filter(IEnumerable<RenderPass^>^ filters, IQuadRenderer^ quadRenderer, LPDIRECT3DDEVICE9 device, LPDIRECT3DSURFACE9 swapSurface, LPDIRECT3DSURFACE9 renderSurfaceA, LPDIRECT3DTEXTURE9 renderTextureA, LPDIRECT3DSURFACE9 renderSurfaceB, LPDIRECT3DTEXTURE9 renderTextureB)
		{
			SeedSurface(device, swapSurface);

			Channel^ channel = snap->ChannelPixels;
			HRESULT hr = device->SetRenderTarget(0, renderSurfaceA);
			LPDIRECT3DSURFACE9 nextSurface = renderSurfaceB;

			LPDIRECT3DTEXTURE9 input = texture;

			for each(RenderPass^ pass in filters)
			{
				pass->Render(IntPtr(device), snap, IntPtr(input), quadRenderer);

				if(nextSurface == renderSurfaceB)
				{
					input = renderTextureA;
					hr = device->SetRenderTarget(0, renderSurfaceB);
					nextSurface = renderSurfaceA;
				}
				else
				{
					input = renderTextureB;
					hr = device->SetRenderTarget(0, renderSurfaceA);
					nextSurface = renderSurfaceB;
				}
			}

			LPDIRECT3DSURFACE9 filtered = nextSurface == renderSurfaceB ? renderSurfaceB : renderSurfaceA;
			hr = device->GetRenderTargetData(filtered, swapSurface);
			hr = device->UpdateSurface(swapSurface, 0, surface, 0);
			return swapSurface;
		}

		void Release()
		{
			surface->Release();
			texture->Release();
			surface = 0;
			texture = 0;
		}

		property IntPtr Texture
		{
			IntPtr get() { return IntPtr(texture); }
		}

		property Snap^ ImageSnap
		{
			Snap^ get() { return snap; }
			void set(Snap^ v) { snap = v; }
		}
	};
}