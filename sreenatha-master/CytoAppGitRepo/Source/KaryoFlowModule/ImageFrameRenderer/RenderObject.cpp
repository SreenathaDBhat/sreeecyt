#include "StdAfx.h"
#include "RenderObject.h"
#include "Bits.h"

namespace AI
{
	RenderObject::RenderObject()
	{
		translateX = 0;
		translateY = 0;
		angle = 0;
		scaleX = 1;
		scaleY = 1;

		vertexBuffer = 0;
		maskTexture = 0;
		pointsInvalidated = true;
		maskInvalidated = true;
		mouseOver = false;
	}

	void RenderObject::InvalidateMask()
	{
		maskInvalidated = true;
	}

	void RenderObject::BuildMaskTexture(IntPtr devicePtr)
	{
		SafeRelease(maskTexture);

		if(mask == nullptr)
			return;

		int w = mask->Width;
		int h = mask->Height;

		LPDIRECT3DDEVICE9 device = (LPDIRECT3DDEVICE9)devicePtr.ToPointer();
		LPDIRECT3DTEXTURE9 tex = 0;
		LPDIRECT3DSURFACE9 surface = 0;
		LPDIRECT3DSURFACE9 swapSurface = 0;
		HRESULT hr = device->CreateTexture(w, h, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &tex, 0);
		hr = tex->GetSurfaceLevel(0, &surface);
		maskTexture = tex;

		hr = device->CreateTexture(w, h, 1, D3DUSAGE_DYNAMIC, D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM, &tex, 0);
		hr = tex->GetSurfaceLevel(0, &swapSurface);
		
		D3DLOCKED_RECT lock;
		hr = swapSurface->LockRect(&lock, 0, 0);
		
		int len = w * h;
		unsigned char* dest = (unsigned char*)lock.pBits;
		pin_ptr<unsigned short> original = &mask->Pixels[0];
		unsigned short* src = original;
		unsigned short* srcEnd = src + (w * h);

		// TODO: remove assumption that its an 8bit input channel
		while(src != srcEnd)
		{
			*dest++ = (unsigned char)*src;
			*dest++ = (unsigned char)*src;
			*dest++ = (unsigned char)*src;
			*dest++ = (unsigned char)*src++;
		}

		dest = (unsigned char*)lock.pBits;

		hr = swapSurface->UnlockRect();
		hr = device->UpdateSurface(swapSurface, 0, surface, 0);

		SafeRelease(swapSurface);
		SafeRelease(tex);
		SafeRelease(surface);
	}

	void RenderObject::Render(IntPtr devicePtr, IntPtr effectPtr, UINT numEffectPasses, int imageWidth, int imageHeight)
	{
		LPDIRECT3DDEVICE9 device = (LPDIRECT3DDEVICE9)devicePtr.ToPointer();
		LPD3DXEFFECT effect = (LPD3DXEFFECT)effectPtr.ToPointer();
		HRESULT hr = 0;

		if(vertexBuffer == 0 || pointsInvalidated)
		{
			Release();

			LPDIRECT3DVERTEXBUFFER9 tmp = 0;			
			hr = device->CreateVertexBuffer(TrianglePoints->Length * sizeof(Vertex), 0, VertexFormat, D3DPOOL_DEFAULT, &tmp, NULL);
			vertexBuffer = tmp;

			Vertex* vert = 0;
			vertexBuffer->Lock(0, TrianglePoints->Length * sizeof(Vertex), (void**)&vert, 0);
			Vertex* end = vert + TrianglePoints->Length;
			FLOAT hw = bounds.Width / 2.0f;
			FLOAT hh = bounds.Height / 2.0f;
			pin_ptr<Point> srcPtr = &TrianglePoints[0];
			Point* src = srcPtr;

			while(vert != end)
			{
				vert->tu = (FLOAT)(src->X / imageWidth);
				vert->tv = (FLOAT)(src->Y / imageHeight);
				vert->x = (FLOAT)(src->X - bounds.X) - hw;
				vert->y = (FLOAT)(((src->Y - bounds.Y) - hh) * -1);
				vert->z = 0.0f;
				vert->maskTu = ((FLOAT)src->X - bounds.X) / (bounds.Width);
				vert->maskTv = ((FLOAT)src->Y - bounds.Y) / (bounds.Height);
				vert++;
				src++;
			}

			vertexBuffer->Unlock();

			pointsInvalidated = false;
		}

		if(maskInvalidated)
		{
			maskInvalidated = false;
			BuildMaskTexture(devicePtr);
		}

		Impl::SetWorldViewProjectionMatrix(IntPtr(effect), imageWidth, imageHeight, translateX - imageWidth / 2, translateY - imageHeight / 2, angle, scaleX, scaleY);
		hr = effect->SetTexture("maskTexture", maskTexture);

		for(UINT pass = 0; pass < numEffectPasses; ++pass)
		{
			hr = effect->BeginPass(pass);

			hr = device->SetStreamSource(0, vertexBuffer, 0, sizeof(Vertex));
			hr = device->SetFVF(VertexFormat);
			hr = device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, TrianglePoints->Length / 3);

			hr = effect->EndPass();
		}
	}

	void RenderObject::Release()
	{
		SafeRelease(vertexBuffer);
		SafeRelease(maskTexture);
	}

	RenderObject^ RenderObject::Duplicate()
	{
		RenderObject^ obj = gcnew RenderObject;
		obj->translateX = translateX;
		obj->translateY = translateY;
		obj->scaleX = scaleX;
		obj->scaleY = scaleY;
		obj->angle = angle;
		obj->bounds = bounds;

		if(tris != nullptr && tris->Length > 0)
		{
			array<Point>^ triPoints = gcnew array<Point>(tris->Length);
			for(int i = 0; i < tris->Length; ++i) triPoints[i] = tris[i];
			obj->TrianglePoints = triPoints;
		}

		if(mask != nullptr)
		{
			int len = mask->Width * mask->Height;
			pin_ptr<unsigned short> src = &mask->Pixels[0];
			array<unsigned short>^ pixels = gcnew array<unsigned short>(len);
			for(int i = 0; i < len; ++i) pixels[i] = src[i];
			obj->Mask = gcnew Channel(mask->Width, mask->Height, mask->BitsPerPixel, pixels);
		}

		return obj;
	}
}