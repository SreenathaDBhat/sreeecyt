#pragma once
using namespace System::ComponentModel;
using namespace System::Collections::Generic;
using namespace System::Windows::Media;
using namespace AI::Bitmap;

namespace AI
{
	public ref class RenderObject : public INotifyPropertyChanged
	{
	private:
		array<Point>^ tris;
		double translateX, translateY;
		double angle;
		double scaleX, scaleY;
		LPDIRECT3DVERTEXBUFFER9 vertexBuffer;
		LPDIRECT3DTEXTURE9 maskTexture;
		int numPrmitives;
		bool pointsInvalidated;
		bool maskInvalidated;
		Channel^ mask;
		Int32Rect bounds;
		bool mouseOver;

		void BuildMaskTexture(IntPtr devicePtr);
	
	public:
		virtual event PropertyChangedEventHandler^ PropertyChanged;

		RenderObject();

		property bool IsMouseOver
		{
			bool get() { return mouseOver; }
			void set(bool b) { mouseOver = b; PropertyChanged(this, gcnew PropertyChangedEventArgs("IsMouseOver")); }
		}

		property Channel^ Mask
		{
			Channel^ get() { return mask; }
			void set(Channel^ value) { mask = value; maskInvalidated = true; SafeRelease(maskTexture); }
		}

		property array<Point>^ TrianglePoints
		{
			array<Point>^ get() { return tris; }
			void set(array<Point>^ value) { tris = value; pointsInvalidated = true; }
		}

		property double TranslateX
		{
			double get() { return translateX; }
			void set(double v) { translateX = v; PropertyChanged(this, gcnew PropertyChangedEventArgs("TranslateX")); }
		}

		property double TranslateY
		{
			double get() { return translateY; }
			void set(double v) { translateY = v; PropertyChanged(this, gcnew PropertyChangedEventArgs("TranslateY")); }
		}

		property double Angle
		{
			double get() { return angle; }
			void set(double v) { angle = v; PropertyChanged(this, gcnew PropertyChangedEventArgs("Angle")); PropertyChanged(this, gcnew PropertyChangedEventArgs("AngleDegrees")); }
		}

		property double AngleDegrees
		{
			double get() { return angle / 0.01745; }
			void set(double v) { angle = v * 0.01745; PropertyChanged(this, gcnew PropertyChangedEventArgs("Angle")); PropertyChanged(this, gcnew PropertyChangedEventArgs("AngleDegrees")); }
		}

		property double ScaleX
		{
			double get() { return scaleX; }
			void set(double v) { scaleX = v;PropertyChanged(this, gcnew PropertyChangedEventArgs("ScaleX")); }
		}

		property double ScaleY
		{
			double get() { return scaleY; }
			void set(double v) { scaleY = v; PropertyChanged(this, gcnew PropertyChangedEventArgs("ScaleY")); }
		}

		property Int32Rect Bounds
		{
			Int32Rect get() { return bounds; }
			void set(Int32Rect v) { bounds = v; PropertyChanged(this, gcnew PropertyChangedEventArgs("Bounds")); }
		}

		void InvalidateMask();
		RenderObject^ Duplicate();
		
	internal:
		void Render(IntPtr device, IntPtr effect, UINT numEffectPasses, int imageWidth, int imageHeight);
		void Release();
	};
}