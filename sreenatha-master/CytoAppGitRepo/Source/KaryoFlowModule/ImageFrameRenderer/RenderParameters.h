#pragma once
#include "IQuadRenderer.h"
using namespace System::ComponentModel;

namespace AI
{
	ref class ImageFrameRenderer;


	public ref class RenderPass abstract
	{
	private:
		Object^ parent;
		bool invalidated;

	public:
		RenderPass(Object^ parent) 
		{ 
			this->parent = parent; 
			invalidated = false;
		}

		virtual void Render(IntPtr device, Snap^ snap, IntPtr inputTexture, IQuadRenderer^ quadRenderer) = 0;
		virtual void Release() = 0;
		virtual bool RunsOn(ChannelInfo^ channelInfo) = 0;

		property Object^ Parent { Object^ get() { return parent; } }
		property bool IsInvalidated 
		{ 
			bool get() { return invalidated; } 
			void set(bool b) { invalidated = b; }
		}
	};

	public interface class IRenderPassTemplate : System::ComponentModel::INotifyPropertyChanged
	{
		RenderPass^ CreatePass(IntPtr device, ImageFrame^ image, ImageFrameRenderer^ targetRenderer, int bpp);
		
		property String^ DisplayName { String^ get(); }
		property Guid Id { Guid get(); }
		property bool IsActive { bool get(); void set(bool); }

		property UIElement^ UI 
		{ 
			UIElement^ get(); 
			void set(UIElement^);  
		}
	};



	public ref class ChannelRenderParameters : public INotifyPropertyChanged
	{
	private:
		double low, high;
		bool visible;
		ChannelInfo^ channelInfo;
		array<int>^ histogramBins;
		double offsetX, offsetY;

	public:
		virtual event PropertyChangedEventHandler^ PropertyChanged;

		ChannelRenderParameters(ChannelInfo^ channelInfo)
		{
			low = 0;
			high = 1;
			visible = true;
			this->channelInfo = channelInfo;
		}

		// low = 0..1
		property double Low 
		{ 
			double get() { return low; } 
			void set(double v) 
			{ 
				low = v; 
				PropertyChanged(this, gcnew PropertyChangedEventArgs("Low"));
				PropertyChanged(this, gcnew PropertyChangedEventArgs("High"));
				PropertyChanged(this, gcnew PropertyChangedEventArgs("Range")); 
			} 
		}
		
		// high = 0..1
		property double High 
		{ 
			double get() { return high; } 
			void set(double v) 
			{ 
				high = v; 
				PropertyChanged(this, gcnew PropertyChangedEventArgs("Low"));
				PropertyChanged(this, gcnew PropertyChangedEventArgs("High")); 
				PropertyChanged(this, gcnew PropertyChangedEventArgs("Range")); 
			} 
		}

		property double OffsetX
		{
			double get() { return offsetX; }
			void set(double v)
			{
				offsetX = v;
				PropertyChanged(this, gcnew PropertyChangedEventArgs("OffsetX"));
			}
		}

		property double OffsetY
		{
			double get() { return offsetY; }
			void set(double v)
			{
				offsetY = v;
				PropertyChanged(this, gcnew PropertyChangedEventArgs("OffsetY"));
			}
		}

		property double Range
		{
			double get() { return High - Low; }
		}

		property bool IsVisible 
		{ 
			bool get() { return visible; } 
			void set(bool v) 
			{ 
				visible = v; 
				PropertyChanged(this, gcnew PropertyChangedEventArgs("IsVisible")); 
			} 
		}

		property ChannelInfo^ ChannelInfoX
		{ 
			ChannelInfo^ get() { return channelInfo; } 
		}

		// Always 256 bins, values normalised to 0..100
		property array<int>^ HistogramBins
		{
			array<int>^ get() { return histogramBins; }
			void set(array<int>^ bins) 
			{
				histogramBins = bins;
				PropertyChanged(this, gcnew PropertyChangedEventArgs("HistogramBins"));
			}

		}
	};



	public ref class ImageRenderParameters : DependencyObject
	{
	public:
		static RoutedUICommand^ InvalidateRender = gcnew RoutedUICommand("Invalidate Render", "InvalidateRender", ImageRenderParameters::typeid);
		static RoutedUICommand^ InvalidateFilters = gcnew RoutedUICommand("Invalidate Filters", "InvalidateFilters", ImageRenderParameters::typeid);
		static DependencyProperty^ ChannelParametersProperty = DependencyProperty::Register("ChannelParameters", IEnumerable<ChannelRenderParameters^>::typeid, ImageRenderParameters::typeid);

		ImageRenderParameters(ImageFrame^ image)
		{
			List<ChannelRenderParameters^>^ list = gcnew List<ChannelRenderParameters^>;

			for each(ChannelInfo^ ch in image->Channels)
			{
				list->Add(gcnew ChannelRenderParameters(ch));
			}

			ChannelParameters = list;
		}

		ChannelRenderParameters^ ParametersForChannel(ChannelInfo^ channel)
		{
			for each(ChannelRenderParameters^ c in ChannelParameters)
			{
				if(c->ChannelInfoX == channel)
					return c;
			}

			return nullptr;
		}

		property IEnumerable<ChannelRenderParameters^>^ ChannelParameters
		{
			IEnumerable<ChannelRenderParameters^>^ get() { return (IEnumerable<ChannelRenderParameters^>^)GetValue(ChannelParametersProperty); }
			void set(IEnumerable<ChannelRenderParameters^>^ v) { return SetValue(ChannelParametersProperty, v); }
		}

		void Reset()
		{
			for each(ChannelRenderParameters^ c in ChannelParameters)
			{
				c->Low = 0;
				c->High = 1;
			}
		}
	};
}