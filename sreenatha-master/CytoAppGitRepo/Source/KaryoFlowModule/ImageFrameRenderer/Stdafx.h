#pragma once
#include <Windows.h>
#include <mmsystem.h>
#include <d3dx9.h>

using namespace System;
using namespace System::Windows;
using namespace System::Windows::Input;
using namespace System::Windows::Controls;
using namespace System::Windows::Interop;
using namespace System::Windows::Threading;
using namespace System::Windows::Data;
using namespace System::Windows::Media;
using namespace System::Windows::Media::Imaging;
using namespace System::Collections::Generic;
using namespace System::Xml::Linq;
using namespace System::Collections::Specialized;
using namespace System::Collections::ObjectModel;

struct Vertex
{
    float x, y, z;
    float tu, tv;
	float maskTu, maskTv;
};

#define VertexFormat (D3DFVF_XYZ|D3DFVF_TEX1|D3DFVF_TEX2)

#define SafeRelease(x) {if(x) { x->Release(); } x=0; }