float4x4 worldViewProj : WorldViewProjection;
float bitsPerPixelMultiplier;
float onePixelX;
float onePixelY;
float offsetX;
float offsetY;

float low;
float high;
float4 renderColor;

int forceLowBitDepth;

texture channelTexture;
texture destinationCompositeTexture;

sampler sourceSampler = sampler_state { Texture = (channelTexture); MipFilter = LINEAR; MinFilter = LINEAR; MagFilter = LINEAR; };
sampler destSampler = sampler_state { Texture = (destinationCompositeTexture); MipFilter = LINEAR; MinFilter = LINEAR; MagFilter = LINEAR; };

struct VS_INPUT
{
    float3 position	: POSITION;
	float2 tc : TEXCOORD0;
	float2 maskTc : TEXCOORD1;
};

struct VS_OUTPUT
{
    float4 hposition : POSITION;
	float2 tc  : TEXCOORD0;
	float2 maskTc  : TEXCOORD1;
};

struct PS_OUTPUT
{
	float4 color : COLOR;
};


VS_OUTPUT vs(VS_INPUT IN)
{
    VS_OUTPUT OUT;

	float3 p = IN.position;
	p.x += offsetX;
	p.y += offsetY;

	OUT.hposition = mul(worldViewProj, float4(p, 1));
	OUT.tc = IN.tc + float2(onePixelX/2, onePixelY/2);
	OUT.maskTc = IN.maskTc + float2(onePixelX/2, onePixelY/2);

	return OUT;
}

float scaleToHistogram(float v)
{
	float r = v * bitsPerPixelMultiplier;
	r = clamp((r - low) / (high - low), 0, 1);
	return r;
}

float modifyBitDepth(float v, int depth)
{
	int maxColours = pow (2, depth);
	float r = round(v * bitsPerPixelMultiplier * maxColours);
	return r / (bitsPerPixelMultiplier * maxColours);
}

PS_OUTPUT ps_set(VS_OUTPUT IN)
{
	float4 sourcePixel = tex2D(sourceSampler, IN.tc);
	float pixel = sourcePixel.r;
	if (forceLowBitDepth > 0)
		pixel = modifyBitDepth(pixel, forceLowBitDepth);
	
	pixel = scaleToHistogram(pixel);
	float4 stretched = pixel * renderColor;

    PS_OUTPUT o;
    o.color = clamp(stretched, 0, 1);
    o.color.a = 1;
    return o;
}

PS_OUTPUT ps_add(VS_OUTPUT IN)
{
	float4 sourcePixel = tex2D(sourceSampler, IN.tc);
	float4 existingPixel = tex2D(destSampler, IN.tc);
	float4 stretched = scaleToHistogram(sourcePixel.r) * renderColor;

    PS_OUTPUT o;
    o.color = clamp(stretched + existingPixel, 0, 1);
    o.color.a = 1;
    return o;
}

PS_OUTPUT ps_attenuate(VS_OUTPUT IN)
{
	float4 sourcePixel = tex2D(sourceSampler, IN.tc);
	float4 existingPixel = tex2D(destSampler, IN.tc);
	float e = max(existingPixel.r, max(existingPixel.g, existingPixel.b));
	float f = scaleToHistogram(sourcePixel.r) * (1 - e);
	float4 stretched = f * renderColor;

    PS_OUTPUT o;
    o.color = clamp(stretched + existingPixel, 0, 1);
    o.color.a = 1;
    return o;
}

technique Add
{
    pass Pass0
    {
		Lighting = false;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_2_0 vs();
		PixelShader  = compile ps_2_0 ps_add();
    }
}

technique Set
{
    pass Pass0
    {
		Lighting = false;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_2_0 vs();
		PixelShader  = compile ps_2_0 ps_set();
    }
}

technique Attenuate
{
    pass Pass0
    {
		Lighting = false;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_2_0 vs();
		PixelShader  = compile ps_2_0 ps_attenuate();
    }
}