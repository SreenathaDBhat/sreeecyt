﻿using System;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows;

namespace AI
{
    public class BinsToPointsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            PointCollection points = new PointCollection(514);
            if (value == null)
                return points;

            int[] bins = (int[])value;

            points.Add(new Point(0, 50));
            for (int i = 0; i < 256; ++i)
            {
                double b = ((Math.Log(bins[i] + 3, 3) - 1) / 5.3) * 45;
                points.Add(new Point(i, 100 - (b + 50)));
            }
            points.Add(new Point(255, 50));
            for (int i = 255; i >= 0; --i)
            {
                double b = ((Math.Log(bins[i] + 3, 3) - 1) / 5.3) * 45;
                points.Add(new Point(i, b + 50));
            }

            return points;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class HistLowHighToWidthConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue)
                return 0;

            double low = (double)values[0];
            double high = (double)values[1];

            return (high - low) * 256;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class HistLowHighToMarginConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return new Thickness(0, 0, 0, 0);

            double v = (double)value;
            return new Thickness(v * 256, 0, 0, 0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
