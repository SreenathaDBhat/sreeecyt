﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows;
using System.Windows.Data;

namespace AI
{
    public partial class ChannelSlider : UserControl
    {
        public static readonly DependencyProperty ShowChannelNameProperty = DependencyProperty.Register("ShowChannelName", typeof(bool), typeof(ChannelSlider), new FrameworkPropertyMetadata(true));
        public static readonly DependencyProperty ChannelParametersProperty = DependencyProperty.Register("ChannelParameters", typeof(ChannelRenderParameters), typeof(ChannelSlider), new FrameworkPropertyMetadata(OnChannelParametersChanged));
        public event EventHandler HistogramSliderChanged;

        private enum Drag { None, Low, High };
        private Drag drag = Drag.None;

        public ChannelSlider()
        {
            InitializeComponent();
        }

        public bool ShowChannelName
        {
            get { return (bool)GetValue(ShowChannelNameProperty); }
            set { SetValue(ShowChannelNameProperty, value); }
        }

        public ChannelRenderParameters ChannelParameters
        {
            get { return (ChannelRenderParameters)GetValue(ChannelParametersProperty); }
            set { SetValue(ChannelParametersProperty, value); }
        }

        private static void OnChannelParametersChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var s = (ChannelSlider)sender;
            s.DataContext = e.NewValue;
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            var element = (FrameworkElement)sender;
            ((FrameworkElement)sender).CaptureMouse();

            double x = NormalizedX(element, e);
            ChannelRenderParameters renderParams = (ChannelRenderParameters)element.Tag;
            drag = (Math.Abs(renderParams.Low - x) < Math.Abs(renderParams.High - x)) ? Drag.Low : Drag.High;


            DoDrag(element, e);
        }

        private void OnMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var element = (FrameworkElement)sender;
            if (drag == Drag.None)
                return;

            e.Handled = true;
            DoDrag(element, e);
        }

        private void DoDrag(FrameworkElement element, MouseEventArgs e)
        {
            double v = NormalizedX(element, e);
            double minGap = 0.025;

            ChannelRenderParameters renderParams = (ChannelRenderParameters)element.Tag;
            if (drag == Drag.Low)
            {
                renderParams.Low = Math.Max(0, Math.Min(v, renderParams.High - minGap));
            }
            else if (drag == Drag.High)
            {
                renderParams.High = Math.Min(1, Math.Max(renderParams.Low + minGap, v));
            }

            FireHistoChange();
        }

        private void FireHistoChange()
        {
            if (HistogramSliderChanged != null)
                HistogramSliderChanged(this, EventArgs.Empty);
        }

        private void OnMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            drag = Drag.None;
            ((FrameworkElement)sender).ReleaseMouseCapture();
            e.Handled = true;

            FireHistoChange();
        }

        private static double NormalizedX(FrameworkElement element, MouseEventArgs e)
        {
            double x = e.GetPosition(element).X;
            double v = x / element.ActualWidth;
            return v;
        }

        private void OnInvalidateRender(object sender, RoutedEventArgs e)
        {
            FireHistoChange();
        }
    }



    public class DarkenColorConverter : IValueConverter
    {
        public double DarkenFactor { get; set; }

        public DarkenColorConverter()
        {
            DarkenFactor = 0.6;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var c = (System.Windows.Media.Color)value;
            return System.Windows.Media.Color.FromArgb(
                (byte)(255),
                (byte)(c.R * DarkenFactor),
                (byte)(c.G * DarkenFactor),
                (byte)(c.B * DarkenFactor));
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


}
