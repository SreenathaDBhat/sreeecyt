﻿using System;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows;

namespace AI
{
    public partial class GammaDisplayOptions : UserControl
    {
        ImageFrame image;
        ImageFrameRenderer renderer;
        GammaFilter gammaFilter;
        ImageParameterRememberer displayOptions;

        public GammaDisplayOptions(GammaFilter gammaFilter, ImageFrame image, ImageFrameRenderer renderer, ImageParameterRememberer displayOptions)
        {
            this.gammaFilter = gammaFilter;
            this.image = image;
            this.renderer = renderer;
            this.displayOptions = displayOptions;
            DataContext = image;
            InitializeComponent();

            slider.ValueChanged += OnSliderSlid;
        }

        private void OnSliderSlid(object sender, System.Windows.RoutedPropertyChangedEventArgs<double> e)
        {
            gammaFilter.Gamma = e.NewValue;
            displayOptions.Remember(image, renderer.RenderParameters);
            renderer.ForceInvalidateFilters();
            renderer.Render();
        }
    }



    public class GammaFilterTemplate : IRenderPassTemplate
    {
        private static readonly Guid id = new Guid("363226D3-2662-478b-81BA-EA0920BD343E");
        private ImageParameterRememberer displayOptions;

        public GammaFilterTemplate(ImageParameterRememberer displayOptions)
        {
            this.displayOptions = displayOptions;
        }

        public RenderPass CreatePass(IntPtr device, ImageFrame image, ImageFrameRenderer r, int textureBpp)
        {
            var f = new GammaFilter(this, device, image, textureBpp);
            //UI = new GammaDisplayOptions(f, image, r, displayOptions);
            return f;
        }

        private UIElement ui;
        public UIElement UI
        {
            get { return null; }
            set
            {
                ui = value;

                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("UI"));
            }
        }

        public string DisplayName
        {
            get { return "Gamma"; }
        }

        public Guid Id
        {
            get { return id; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        bool isActive;
        public bool IsActive
        {
            get { return isActive; }
            set
            {
                isActive = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("IsActive"));
            }
        }
    }
}
