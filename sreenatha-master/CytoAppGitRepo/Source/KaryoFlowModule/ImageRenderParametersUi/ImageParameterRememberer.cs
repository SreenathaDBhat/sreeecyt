﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace AI
{
    internal class RememberedChannelParameters
    {
        public string ChannelDisplayName { get; set; }
        public double HistogramLow { get; set; }
        public double HistogramHigh { get; set; }
    }

    internal class RememberedImageParameters
    {
        public IEnumerable<RememberedChannelParameters> ChannelParameters { get; set; }
        public double Gamma { get; set; }

        public void Apply(ImageFrame image, ImageRenderParameters renderParameters)
        {
            image.Gamma = Gamma;

            foreach (var rp in renderParameters.ChannelParameters)
            {
                var matchingChannelInfo = ChannelParameters.Where(c => c.ChannelDisplayName == rp.ChannelInfoX.DisplayName).FirstOrDefault();
                if (matchingChannelInfo == null)
                    continue;

                rp.High = matchingChannelInfo.HistogramHigh;
                rp.Low = matchingChannelInfo.HistogramLow;
            }
        }
    }

    public class ImageParameterRememberer
    {
        private Dictionary<Guid, RememberedImageParameters> rememberedParameters;
        private Guid caseId;

        public ImageParameterRememberer(Guid caseId)
        {
            this.caseId = caseId;
            rememberedParameters = new Dictionary<Guid, RememberedImageParameters>();
        }

        public Guid CaseId
        {
            get { return caseId; }
        }

        public void Forget(ImageFrame image)
        {
            if (image != null && rememberedParameters.ContainsKey(image.Id))
                rememberedParameters.Remove(image.Id);
        }

        public void Remember(ImageFrame image, ImageRenderParameters renderParameters)
        {
            RememberedImageParameters imageParams = null;
            if (rememberedParameters.ContainsKey(image.Id))
            {
                imageParams = rememberedParameters[image.Id];
            }
            else
            {
                imageParams = new RememberedImageParameters();
                rememberedParameters.Add(image.Id, imageParams);
            }

            imageParams.Gamma = image.Gamma;
            imageParams.ChannelParameters = (from crp in renderParameters.ChannelParameters
                                             select new RememberedChannelParameters
                                             {
                                                 ChannelDisplayName = crp.ChannelInfoX.DisplayName,
                                                 HistogramHigh = crp.High,
                                                 HistogramLow = crp.Low
                                             }).ToArray();
        }

        public void Apply(ImageFrame frame, ImageRenderParameters renderParameters)
        {
            if(rememberedParameters.ContainsKey(frame.Id))
                rememberedParameters[frame.Id].Apply(frame, renderParameters);
        }

        private FileInfo SaveFile
        {
            get { return AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\DisplayOptions\\" + CaseId.ToString() + ".xml"); }
        }

        public void Save()
        {
            var file = SaveFile.FullName;

            if (File.Exists(file))
                File.Delete(file);

            if (rememberedParameters.Count == 0)
                return;

            XElement xml = new XElement("ImageParameters", from imageId in rememberedParameters.Keys
                                                           select ImageXmlNode(imageId));

            xml.Save(file);
        }

        public void Load()
        {
            var file = SaveFile.FullName;

            if(!File.Exists(file))
                return;

            XElement xml = XElement.Load(file);
            foreach (var imageNode in xml.Descendants("Image"))
            {
                RememberedImageParameters imageParams = new RememberedImageParameters();
                imageParams.Gamma = double.Parse(imageNode.Attribute("Gamma").Value);

                imageParams.ChannelParameters = (from c in imageNode.Elements("Channel")
                                                 select new RememberedChannelParameters
                                                 {
                                                     ChannelDisplayName = c.Attribute("Name").Value,
                                                     HistogramLow = double.Parse(c.Attribute("Low").Value),
                                                     HistogramHigh = double.Parse(c.Attribute("High").Value),

                                                 }).ToArray();

                Guid key = new Guid(imageNode.Attribute("Id").Value);
                rememberedParameters.Add(key, imageParams);
            }
        }

        private XElement ImageXmlNode(Guid imageId)
        {
            var imageParams = rememberedParameters[imageId];

            XElement xml = new XElement("Image");
            xml.Add(
                new XAttribute("Id", imageId.ToString()),
                new XAttribute("Gamma", imageParams.Gamma));

            foreach (var cp in imageParams.ChannelParameters)
            {
                xml.Add(new XElement("Channel",
                    new XAttribute("Name", cp.ChannelDisplayName),
                    new XAttribute("Low", cp.HistogramLow),
                    new XAttribute("High", cp.HistogramHigh)));
            }

            return xml;
        }
    }
}
