﻿using System;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Input;
using System.Collections.Generic;
using System.Windows.Data;
using System.Linq;
using System.ComponentModel;
using System.Collections;

namespace AI
{
    public partial class ImageRenderParametersUi : INotifyPropertyChanged
    {
        public static RoutedUICommand AddFilter = new RoutedUICommand("Add Filter", "AddFilter", typeof(ImageRenderParametersUi));
        public static RoutedUICommand RemoveFilter = new RoutedUICommand("Remove Filter", "RemoveFilter", typeof(ImageRenderParametersUi));
        public static RoutedUICommand ModifiedByUser = new RoutedUICommand("Modified by User", "ModifiedByUser", typeof(ImageRenderParametersUi));
        public static RoutedUICommand CopyImageToClipboard = new RoutedUICommand("Copy to Clipboad", "CopyClipboard", typeof(ImageRenderParametersUi));
        public static RoutedUICommand ExportImageToFile = new RoutedUICommand("Export Image", "ExportImage", typeof(ImageRenderParametersUi));

        public static readonly DependencyProperty BusyLoadingProperty = DependencyProperty.Register("BusyLoading", typeof(bool), typeof(ImageRenderParametersUi));
        public static readonly DependencyProperty RendererProperty = DependencyProperty.Register("Renderer", typeof(ImageFrameRenderer), typeof(ImageRenderParametersUi));
        public static readonly DependencyProperty DisplayOptionsProperty = DependencyProperty.Register("DisplayOptions", typeof(ImageParameterRememberer), typeof(ImageRenderParametersUi));

        public event EventHandler DisplayChanged;
        public event EventHandler DisplayChanging;

        public ImageRenderParametersUi()
        {
            InitializeComponent();
        }

        public bool ShowCounterstainAttenuation
        {
            get { return GotMultipleChannels && RenderParameters.ChannelParameters.Where(r => r.ChannelInfoX.IsCounterstain).FirstOrDefault() != null; }
        }

        private ImageRenderParameters RenderParameters
        {
            get { return Renderer == null ? null : Renderer.RenderParameters; }
        }

        public bool GotMultipleChannels
        {
            get { return RenderParameters != null && RenderParameters.ChannelParameters.Count() > 1; }
        }

        public ImageFrameRenderer Renderer
        {
            get { return (ImageFrameRenderer)GetValue(RendererProperty); }
            set { SetValue(RendererProperty, value); }
        }

        public ImageParameterRememberer DisplayOptions
        {
            get { return (ImageParameterRememberer)GetValue(DisplayOptionsProperty); }
            set { SetValue(DisplayOptionsProperty, value); }
        }

        public bool BusyLoading
        {
            get { return (bool)GetValue(BusyLoadingProperty); }
            set { SetValue(BusyLoadingProperty, value); }
        }

        private void OnDragFinished(object sender, EventArgs e)
        {
            FireDisplayChanged();
            DisplayOptions.Remember(Renderer.ImageFrameX, Renderer.RenderParameters);
        }

        private void FireDisplayChanged()
        {
            Renderer.Render();
            if (DisplayChanged != null)
                DisplayChanged(this, null);
        }

        private void CanCopyClipboardExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Renderer != null;
        }

        private void CopyClipboardExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ImageRendererExport.CopyToClipboard(Renderer);
        }

        private void CanExportImageToFileExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Renderer != null;
        }

        private void ExportImageToFileExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ImageRendererExport.SaveAs(Renderer);
        }

        private void ActivateFilter(IRenderPassTemplate filter)
        {
            if (Renderer == null || Renderer.ImageFrameX == null)
                return;

            DisplayOptions.Forget(Renderer.ImageFrameX);
            RenderParameters.Reset();

            if (DisplayChanging != null)
                DisplayChanging(this, null);

            Renderer.ActivateFilter(filter);
        }

        private void OnAddFilter(object sender, RoutedEventArgs e)
        {
            var t = (IRenderPassTemplate)((FrameworkElement)sender).Tag;
            if (!Renderer.IsPassActive(t))
            {
                ActivateFilter(t);

                Renderer.Render();
                FireDisplayChanged();
            }
        }

        private void OnRemoveFilter(object sender, RoutedEventArgs e)
        {
            var t = (IRenderPassTemplate)((FrameworkElement)sender).Tag;
            if (Renderer.IsPassActive(t))
            {
                DisplayOptions.Forget(Renderer.ImageFrameX);
                RenderParameters.Reset();

                t.UI = null;

                if (DisplayChanging != null)
                    DisplayChanging(this, null);

                Renderer.DeactivateFilter(t);
                Renderer.Render();
                FireDisplayChanged();
            }
        }

        public void AddInitialFilters()
        {
            Renderer.ClearFilters();

            foreach (var f in Renderer.AvailableFilters)
            {
                if (f.IsActive)
                {
                    ActivateFilter(f);
                }
            }

            Renderer.Render();
            FireDisplayChanged();
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void Notify(string param)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(param));
        }

        #endregion
    }


    public class ChannelSorter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var list = ((IEnumerable<ChannelRenderParameters>)value).ToList();
            list.Sort((a, b) =>
                {
                    if ((a.ChannelInfoX.IsCounterstain && b.ChannelInfoX.IsCounterstain) || (!a.ChannelInfoX.IsCounterstain && !b.ChannelInfoX.IsCounterstain))
                        return a.ChannelInfoX.DisplayName.CompareTo(b.ChannelInfoX.DisplayName);
                    else if (a.ChannelInfoX.IsCounterstain)
                        return -1;
                    else
                        return 1;
                });

            return list;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
