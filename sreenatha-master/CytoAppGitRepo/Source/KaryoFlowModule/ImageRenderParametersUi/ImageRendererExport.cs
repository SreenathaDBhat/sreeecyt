﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;
using System.Xml.Linq;
using System.Windows;

namespace AI
{
    public static class ImageRendererExport
    {
        private static readonly Guid ImageRenderExportIdentifier = new Guid("95634C55-368F-4789-8F34-BBBF70B3013E");


        public static void SaveAs(ImageFrameRenderer renderer)
        {
            var file = SimpleFileDialog.Save(ImageRenderExportIdentifier, "Export Image", "JPEG Image|*.jpg;*.jpeg|PNG Image|*.png");
            if (file != null)
            {
                SaveAs(renderer, file);
            }
        }

        public static void SaveAs(ImageFrameRenderer renderer, string filename)
        {
            var image = GetBitmap(renderer);
            SaveAs(image, filename);
        }

        public static void SaveAs(BitmapSource image)
        {
            var file = SimpleFileDialog.Save(ImageRenderExportIdentifier, "Export Image", "JPEG Image|*.jpg;*.jpeg|PNG Image|*.png");
            if (file != null)
            {
                SaveAs(image, file);
            }
        }

        public static void SaveAs(BitmapSource image, string filename)
        {
            using (var stream = File.OpenWrite(filename))
            {
                if (string.Compare(Path.GetExtension(filename), ".png", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    PngBitmapEncoder enc = new PngBitmapEncoder();
                    enc.Frames.Add(BitmapFrame.Create(image));
                    enc.Save(stream);
                    stream.Flush();
                }
                else
                {
                    JpegBitmapEncoder enc = new JpegBitmapEncoder();
                    enc.QualityLevel = 60;
                    enc.Frames.Add(BitmapFrame.Create(image));
                    enc.Save(stream);
                    stream.Flush();
                }
            }
        }

        public static BitmapSource GetBitmap(ImageFrameRenderer renderer)
        {
            renderer.Render();
            byte[] pixels = renderer.CopyPixels();
            var image = BitmapSource.Create(renderer.ImageFrameX.ImageWidth, renderer.ImageFrameX.ImageHeight, 96, 96, PixelFormats.Pbgra32, null, pixels, renderer.ImageFrameX.ImageWidth * 4);
            return image;
        }

        public static void CopyToClipboard(ImageFrameRenderer renderer)
        {
            var image = GetBitmap(renderer);
            Clipboard.SetImage(image);
        }

        public static void CopyToClipboard(BitmapSource image)
        {
            Clipboard.SetImage(image);
        }
    }
}
