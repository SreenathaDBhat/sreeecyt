﻿using System;
using System.Collections.Generic;

namespace AI
{
    public static class FilterTypes
    {
        public static IEnumerable<IRenderPassTemplate> BrightfieldTypes(ImageParameterRememberer displayOptions)
        {
            return new IRenderPassTemplate[]
            {
                new GammaFilterTemplate(displayOptions) { IsActive = true },
                new AdaptiveBackgroundSubtractionFilterTemplate(),
                new UnsharpMaskFilterTemplate(20, 3, 0.4f, "Sharpen"),
                new UnsharpMaskFilterTemplate(20, 3, 0.6f, "Sharpen More")
            };
        }

        public static IEnumerable<IRenderPassTemplate> FluorescentTypes
        {
            get
            {
                return new IRenderPassTemplate[]
                {
                    new UnsharpMaskFilterTemplate(4, 4, 0.75f, "Sharpen"),
                    new BackgroundSubtractionFilterTemplate()
                };
            }
        }
    }
}
