﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AI.Bitmap;

namespace AI
{
    public static class Gamma
    {
        public static unsafe Channel ApplyGamma(Channel img, double gamma)
        {
            int n = img.Width * img.Height;
            ushort[] result = new ushort[n];
            double maxPixelValue = Math.Pow(2, img.BitsPerPixel);
            gamma = 1 / gamma;

            fixed (ushort* pixels = &img.Pixels[0])
            fixed (ushort* resultPixels = &result[0])
            {
                for (int i = 0; i < n; ++i)
                {
                    double v = pixels[i] / maxPixelValue;
                    v = Math.Pow(v, gamma);
                    v = Math.Min(1, v);
                    v = Math.Max(0, v);
                    v = maxPixelValue * v;
                    resultPixels[i] = (ushort)v;
                }
            }

            return new Channel(img.Width, img.Height, img.BitsPerPixel, result);
        }
    }
}
