sampler2D implicitInput : register(s0);
float low : register(c0);
float high : register(c1);
float thresh : register(c2);

float4 main(float2 uv : TEXCOORD) : COLOR
{
    float4 color = tex2D(implicitInput, uv);
    
    if((1-color.r) < thresh)
    {
		float4 red = float4(1,0,0,1);
		return (red * 0.3) + (color * 0.7);
    }
    
    float r = clamp(color.r, low, high) - low;
    r = r / (high-low);

    float4 col;
    col = r;
    col.a = 1;

    return col;
}