﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Effects;
using System.Windows.Media;
using System.Windows;

namespace AI
{
    public class HistogramRenderEffect : ShaderEffect
    {
        public static readonly DependencyProperty InputProperty = ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(HistogramRenderEffect), 0);
        public static readonly DependencyProperty LowProperty = DependencyProperty.Register("Low", typeof(double), typeof(HistogramRenderEffect), new UIPropertyMetadata(0.0, PixelShaderConstantCallback(0)));
        public static readonly DependencyProperty HighProperty = DependencyProperty.Register("High", typeof(double), typeof(HistogramRenderEffect), new UIPropertyMetadata(1.0, PixelShaderConstantCallback(1)));
        public static readonly DependencyProperty ThresholdProperty = DependencyProperty.Register("Threshold", typeof(double), typeof(HistogramRenderEffect), new UIPropertyMetadata(-1.0, PixelShaderConstantCallback(2)));

        public HistogramRenderEffect()
        {
            PixelShader = new PixelShader() { UriSource = new Uri(@"pack://application:,,,/ImageRenderer;component/HistogramRender.ps") };
            UpdateShaderValue(InputProperty);
            UpdateShaderValue(LowProperty);
            UpdateShaderValue(HighProperty);
            UpdateShaderValue(ThresholdProperty);
        }

        public Brush Input
        {
            get { return (Brush)GetValue(InputProperty); }
            set { SetValue(InputProperty, value); }
        }

        public double Low
        {
            get { return (double)GetValue(LowProperty); }
            set { SetValue(LowProperty, value); }
        }

        public double High
        {
            get { return (double)GetValue(HighProperty); }
            set { SetValue(HighProperty, value); }
        }

        public double Threshold
        {
            get { return (double)GetValue(ThresholdProperty); }
            set { SetValue(ThresholdProperty, value); }
        }
    }
}
