﻿using System;
using System.Linq;
using System.Windows.Media;
using System.ComponentModel;
using System.Windows.Media.Effects;
using AI.Bitmap;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using System.Windows;

namespace AI
{
    public class ImageRenderer : INotifyPropertyChanged
    {
        private ImageFrame image;
        private BitmapSource display;
        private HistogramRenderEffect effect;
        private bool backgroundSubtract, gamma, sharpen, sharpenMore;
        private Channel channelUsedForRender;
        private byte[] copiedPixels;
        private HistogramData histogram;
        private int[] histogramBins;
        private double low, high;

        public event EventHandler DisplayChanged;
        public event EventHandler DisplayChanging;

        public ImageRenderer()
        {
            gamma = true;
            low = 0;
            high = 1;

            effect = new HistogramRenderEffect();
        }

        public Effect Effect
        {
            get { return effect; }
        }

        public ImageFrame Image
        {
            get { return image; }
            set 
            {
                HistogramLow = 0;
                HistogramHigh = 1;
                image = value; 
                RebuildDisplayImage(); 
            }
        }

        public bool BackgroundSubtraction
        {
            get { return backgroundSubtract; }
            set 
            {
                backgroundSubtract = value; 
                Notify("BackgroundSubtraction"); 
                RebuildDisplayImage();
                HistogramLow = 0;
                HistogramHigh = 1;
            }
        }

        public bool Gamma
        {
            get { return gamma; }
            set { gamma = value; Notify("Gamma"); RebuildDisplayImage(); }
        }

        public bool Sharpen
        {
            get { return sharpen; }
            set 
            { 
                sharpen = value; 
                if(value)
                    sharpenMore = false; 

                Notify("Sharpen", "SharpenMore"); 
                RebuildDisplayImage();
            }
        }

        public bool SharpenMore
        {
            get { return sharpenMore; }
            set 
            { 
                sharpenMore = value; 
                if(value)
                    sharpen = false; 

                Notify("Sharpen", "SharpenMore"); 
                RebuildDisplayImage(); }
        }

        public ImageFrame ImageFrameX
        {
            get { return image; }
        }

        public ImageSource DisplayImage
        {
            get { return display; }
        }

        private void RebuildDisplayImage()
        {
            if (DisplayChanging != null)
                DisplayChanging(this, null);

            var snap = image.AllSnaps.First();
            var channel = snap.ChannelPixels.Duplicate();

            if (gamma)
            {
                channel = AI.Gamma.ApplyGamma(channel, image.Gamma);
            }
            if (backgroundSubtract)
            {
//                channel = SubtractBackground.Adaptive(channel); // 32 bit only
            }
            if (sharpen)
            {
                channel = SharpenFilter.Sharpen(channel, 3, 20.0f, 0.4f);
            }
            if (sharpenMore)
            {
                channel = SharpenFilter.Sharpen(channel, 3, 20.0f, 0.6f);
            }

            channelUsedForRender = channel;

            var bmp = RGBImageComposer.Compose(channel);
            copiedPixels = new byte[bmp.PixelWidth * bmp.PixelHeight * 4];
            bmp.CopyPixels(copiedPixels, bmp.PixelWidth * 4, 0);
            display = bmp;
            Notify("DisplayImage");

            UpdateHistogram(channel);

            if (DisplayChanged != null)
                DisplayChanged(this, null);
        }

        public Channel ProcessedChannel
        {
            get { return channelUsedForRender; }
        }

        private unsafe void UpdateHistogram(Channel pixels)
        {
            histogramBins = new int[256];
            int maxCount = 0;

            fixed (ushort* data16 = &pixels.Pixels[0])
            {
                ushort* pixel = data16;
                ushort* endOfPixels = data16 + (pixels.Width * pixels.Height);
                double maxPixelValue = Math.Pow(2.0, pixels.BitsPerPixel) - 1;

                while (pixel != endOfPixels)
                {
                    ushort r = *pixel++;
                    pixel++; // g is not used

                    int v = Math.Max(0, Math.Min(255, (int)((r / maxPixelValue) * 256)));
                    histogramBins[v] = histogramBins[v] + 1;
                    if (v < 255)
                        maxCount = Math.Max(maxCount, histogramBins[v]);
                }
            }

            for(int i = 0; i < 256; ++i)
		    {
                int v = (int)((((double)histogramBins[i]) / maxCount) * 1000);
			    v = Math.Min(1000, v);
                histogramBins[i] = v;
		    }

            Notify("Histogram", "HistogramLow", "HistogramHigh");
        }

        public IEnumerable<int> Histogram
        {
            get { return histogramBins; }
        }

        public double HistogramLow
        {
            get { return low; }
            set 
            {
                low = value; 
                Notify("HistogramLow"); 
                effect.Low = low;
                if (DisplayChanged != null)
                    DisplayChanged(this, null);
            }
        }

        public double HistogramHigh
        {
            get { return high; }
            set 
            { 
                high = value; 
                Notify("HistogramHigh"); 
                effect.High = high;
                if (DisplayChanged != null)
                    DisplayChanged(this, null);
            }
        }

        public double Threshold
        {
            get { return effect.Threshold; }
            set { effect.Threshold = value; Notify("Threshold"); }
        }


        public byte[] CopyPixels()
        {
            byte[] p = new byte[copiedPixels.Length];
            Array.Copy(copiedPixels, p, copiedPixels.Length);
            ApplyHistogramInSoftware(p, image.ImageWidth);
            return p;
        }

        public byte[] CopyPixels(Int32Rect roi)
        {
            var cb = new CroppedBitmap(display, roi);
            var p = new byte[roi.Width * roi.Height * 4];
            cb.CopyPixels(p, roi.Width * 4, 0);
            ApplyHistogramInSoftware(p, roi.Width);
            return p;
        }

        private void ApplyHistogramInSoftware(byte[] pixels, int width)
        {
            int height = (pixels.Length / 4) / width;
            byte low = (byte)(HistogramLow * 255);
            byte high = (byte)(HistogramHigh * 255);
            double range = high-low;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    int i = ((y * width) + x) * 4;
                    byte g = pixels[i + 1];
                    double d = (Math.Min(high, Math.Max(low, g)) - low) / range;
                    g = (byte)(d * 255);
                    pixels[i] = g;
                    pixels[i+1] = g;
                    pixels[i+2] = g;
                }
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        void Notify(params string[] ps)
        {
            if (PropertyChanged != null)
            {
                foreach (var s in ps)
                    PropertyChanged(this, new PropertyChangedEventArgs(s));
            }
        }

        #endregion
    }
}
