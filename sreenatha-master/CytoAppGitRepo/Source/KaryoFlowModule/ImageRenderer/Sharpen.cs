﻿using System;
using AI.Bitmap;

namespace AI
{
    public unsafe static class SharpenFilter
    {

        public unsafe static Channel Sharpen(Channel input, int halfWidth, float sigma, float amount)
        {
            int n = input.Width * input.Height;
            ushort[] gauss = new ushort[n];
            ushort[] result = new ushort[n];

            double [] filterArray = GenerateFilter(halfWidth, sigma);

            fixed (ushort* resultPixels = &result[0])
            fixed (ushort* pixels = &input.Pixels[0])
            fixed (ushort* gaussPixels = &gauss[0])
            {
                for (int i = 0; i < input.Width; ++i)
                {
                    for (int j = 0; j < input.Height; j++)
                    {
                        int pixel = j * input.Width + i;
                        gaussPixels[pixel] = GaussianV(filterArray, pixels, i, j, input.Width, input.Height );
                    }
                }

                for (int i = 0; i < input.Width; ++i)
                {
                    for (int j = 0; j < input.Height; j++)
                        gaussPixels[i] = GaussianH(filterArray, gaussPixels, i, j, input.Width, input.Height);
                }

                for (int i = 0; i < n; ++i)
                {
                    resultPixels[i] = Umask(pixels[i], gauss[i], amount, input.BitsPerPixel); 
                }
            }
            return new Channel(input.Width, input.Height, input.BitsPerPixel, result);
        }

        private static double [] GenerateFilter(int halfWidth, float sigma)
	    {
            double Total = 0;

            double [] filterArray = new double [Size(halfWidth)];
 
            for (int i = 0; i < filterArray.Length; i++)
            {
                int j = i - halfWidth;

                double ep = -(j * j) / (2.0 * sigma * sigma);
                double x = Math.Exp(ep) / (2.50662827 * sigma);
                filterArray[i] = x;    
                Total += filterArray[i];   
            }
            for (int i = 0; i < filterArray.Length; i++)
                filterArray[i] /= Total;                 

            return filterArray;
	    }

        private static int Size(int halfWidth)
        {
            return halfWidth * 2 + 1;
        }

        private static unsafe ushort GaussianV(double [] filter,  ushort * pixels, int col, int line, int w, int h)
        {
            double p = 0;

            int newline = line - (filter.Length / 2) + 1;

            for (int i= 0; i < filter.Length; i++)
            {
                int pixel = newline * w + col;

                if (newline < 0 || newline >= h)
                {
                    p += 0;
                }
                else
                {
                    p += filter[i] * pixels[pixel];
                }
                newline++;
            }
            return (ushort)p;
        }

        private static unsafe ushort GaussianH(double[] filter, ushort* pixels, int col, int line, int w, int h)
        {
            double p = 0;

            int newcol = col - (filter.Length / 2) + 1;

            for (int i = 0; i < filter.Length; i++)
            {
                int pixel = newcol * h + line;
                if (newcol < 0 || newcol >= w)
                {
                    p += 0;
                }
                else
                {
                    p += filter[i] * pixels[pixel];
                }
                newcol++;
            }
            return (ushort)p;
        }

        private static ushort Umask(ushort source, ushort gauss, float amount, int bpp)
        {
            double maxVal = Math.Pow(2, bpp);
	        double x = (source - amount*gauss)/(1.0-amount);
            if (x > maxVal)
                x = maxVal;
            else if (x < 0)
                x = 0;

            return (ushort)x;
        }
    }
}
