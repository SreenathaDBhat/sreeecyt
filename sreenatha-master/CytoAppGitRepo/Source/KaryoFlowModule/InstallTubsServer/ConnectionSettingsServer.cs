﻿using System;
using System.Windows;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.Linq;

namespace AI
{
    public class ConnectionSettingsServer : Notifier
    {
        public const int RequiredDbVersion = 46000; // 4.60.00

        SqlServer sql = new SqlServer();

        public SqlServer Sql
        {
            get { return sql; }
            set { sql = value; Notify("Sql"); Notify("IsValid"); }
        }

        public ICaseConverter RunDatabaseScript()
        {
            string scriptText = string.Empty;
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("AI.database.sql"))
            {
                StreamReader reader = new StreamReader(stream);
                scriptText = reader.ReadToEnd();
            }

            return RunDatabaseScript(scriptText);
        }

        public void RunFileStreamScript(string filestreamPath)
        {
            string scriptText = string.Empty;
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("AI.TubsToFS2008.sql"))
            {
                StreamReader reader = new StreamReader(stream);
                string rs = reader.ReadToEnd();
                scriptText = string.Format(rs, filestreamPath.TrimEnd('\\'));
            }
            RunDbScript(scriptText);
        }
           

        public ICaseConverter RunDatabaseScript(string sqlScriptText)
        {
            ICaseConverter result = null;

            int currentVersion = FindVersion();

            if (currentVersion < RequiredDbVersion)
            {
                try
                {
                    if (currentVersion >= 0)
                    {
                    }


                    RunDbScript(sqlScriptText);

                    if (currentVersion == 1)
                    {
                        result = new TubsConverter1();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Database creation failed. Check you have sufficient access.", "Database Creation / Upgrade", MessageBoxButton.OK, MessageBoxImage.Error);
                    return result;
                }
            }

            return result;
        }

        private int FindVersion()
        {
            int ver = -1;
            try
            {
                ver = sql.GetTubsVersion();
            }
            catch 
            {
                ver = -1;
            }
            return ver;
        }

        public void RunDbScript(string sqlScriptText)
        {
            string connectionString = sql.BuildConnectionString(false);
            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                Server server = new Server(new ServerConnection(connection));
                server.ConnectionContext.ExecuteNonQuery(sqlScriptText);
            }
            finally
            {
                connection.Close();
            }
        }

        public bool IsSqlServer2008
        {
            get
            {
                string connectionString = sql.BuildConnectionString(false);
                SqlConnection connection = new SqlConnection(connectionString);
                try
                {
                    connection.Open();
                    Server server = new Server(new ServerConnection(connection));
                    return server.VersionMajor == 10;

                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public bool IsFileStream
        {
            get
            {
                string connectionString = sql.BuildConnectionString(false);
                SqlConnection connection = new SqlConnection(connectionString);
                try
                {
                    connection.Open();
                    Server server = new Server(new ServerConnection(connection));
                    if (server.FilestreamLevel == FileStreamEffectiveLevel.TSqlFullFileSystemAccess )
                        return true;
                    else
                        return false;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public static bool IsConfiguredForFileStream(string connectionString)
        {
            bool ret = false;

            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                Server server = new Server(new ServerConnection(connection));
                Database database = server.Databases["Tubs"];
                Table blob = database.Tables["Blob"];
                if (blob.FileStreamFileGroup == "TUBSFILEGROUP")
                    ret = true;
            }
            catch { }
            finally
            {
                connection.Close();
            }
            return ret;
        }


    }
}
