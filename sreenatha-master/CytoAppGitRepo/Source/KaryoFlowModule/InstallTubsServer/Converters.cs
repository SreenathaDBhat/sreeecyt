﻿using System.Windows.Data;
using System;
using System.Windows.Media;
using System.Globalization;
using System.Windows;

namespace AI
{
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class InvertVisibilityConverter : IValueConverter
    {
        private Visibility valueWhenInvisible = Visibility.Collapsed;

        public Visibility ValueWhenInvisible
        {
            get { return valueWhenInvisible; }
            set { valueWhenInvisible = value; }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility v = (Visibility)value;
            return (v == Visibility.Visible) ? valueWhenInvisible : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    public class BoolToColourConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool b = (bool)value;
            return b ? Brushes.Green : Brushes.DarkRed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class SqlStatusToServerInstallMessage : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SqlServerInstall state = (SqlServerInstall)value;
            switch (state)
            {
                case SqlServerInstall.Unverified: return "Unverified";
                case SqlServerInstall.InvalidConnection: return "Server Not Set";
                case SqlServerInstall.ValidServer_NoCvDatabase: return "No Tubs DB";
                case SqlServerInstall.ValidServer_UnableToRunScript: return "Cannot Run Script";
                case SqlServerInstall.ValidServer_CvDatabaseOutOfDate: return "Invalid Version";
                case SqlServerInstall.ValidServer_CvDataFullyUpToDate: return "Confirmed";
                default: return "Invalid";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class SqlInstallStatusToRunUpdateVis : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SqlServerInstall state = (SqlServerInstall)value;
            return (state == SqlServerInstall.ValidServer_CvDatabaseOutOfDate) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class SqlInstallOKVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SqlServerInstall state = (SqlServerInstall)value;
            return (state == SqlServerInstall.ValidServer_CvDataFullyUpToDate) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }


    public class SqlInstallStatusToRunCreateOrUpdateVis : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SqlServerInstall state = (SqlServerInstall)value;
            bool visibile = (state == SqlServerInstall.ValidServer_CvDatabaseOutOfDate) || (state == SqlServerInstall.ValidServer_NoCvDatabase);
            return visibile ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class SqlInstallStatusToScriptMsg : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SqlServerInstall state = (SqlServerInstall)value;
            return state == SqlServerInstall.ValidServer_NoCvDatabase ? "Install DB" : "Update DB";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class SqlInstallStatusToServerConnectMsg : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SqlServerInstall state = (SqlServerInstall)value;
            return state == SqlServerInstall.InvalidConnection ? "Connect" : "Change";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class SqlInstallStatusToBoolConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SqlServerInstall state = (SqlServerInstall)value;
            return state == SqlServerInstall.ValidServer_CvDataFullyUpToDate;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }   

    public class SqlStatusToConnectMessage : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SqlServerInstall state = (SqlServerInstall)value;
            switch (state)
            {
                case SqlServerInstall.Unverified: return "Unverified";
                case SqlServerInstall.InvalidConnection: return "Invalid";
                case SqlServerInstall.SqlException: return "Sql Error";
                case SqlServerInstall.ValidServer_NoCvDatabase: return "Valid Server. No Tubs Database";
                case SqlServerInstall.ValidServer_UnableToRunScript: return "Valid Server. Tubs Database Found. Access Problem";
                case SqlServerInstall.ValidServer_CvDatabaseOutOfDate: return "Valid Server. Tubs Database Found. Version Invalid";
                case SqlServerInstall.ValidServer_CvDataFullyUpToDate: return "Server and Database Confirmed";
                default: return "Invalid";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class SqlStatusToConnectColour : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SqlServerInstall state = (SqlServerInstall)value;
            return state == SqlServerInstall.ValidServer_CvDataFullyUpToDate ? Brushes.Green : Brushes.DarkRed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class SqlInstallStatusToAccessProblemVis : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SqlServerInstall state = (SqlServerInstall)value;
            bool visible = (state == SqlServerInstall.ValidServer_UnableToRunScript) || (state == SqlServerInstall.SqlException);
            return visible ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }



}