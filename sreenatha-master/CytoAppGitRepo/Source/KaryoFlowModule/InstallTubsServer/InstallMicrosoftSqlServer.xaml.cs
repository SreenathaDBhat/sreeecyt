﻿using System;
using System.Windows;
using System.Threading;
using System.IO;
using Microsoft.Win32;
using System.ServiceProcess;
using System.Diagnostics;

namespace AI
{
    public partial class InstallMicrosoftSqlServer : Window
    {
        public InstallMicrosoftSqlServer()
        {
            InitializeComponent();
        }

        private void OnCloseClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void OnOk(object sender, RoutedEventArgs e)
        {
            InstallSqlServer();
            RestartSqlService();
            DialogResult = true;
        }

        private void RestartSqlService()
        {
            string instance = newInstanceName.Text;
            if (instance == string.Empty)
                instance = "MSSQLSERVER";

            ServiceController service = new ServiceController(string.Format("SQL Server ({0})", instance));
            service.Stop();
            while (service.Status == ServiceControllerStatus.StopPending)
            {
                Thread.Sleep(100);
                service.Refresh();
            }

            service.Start();

            while (service.Status == ServiceControllerStatus.StartPending)
            {
                Thread.Sleep(100);
                service.Refresh();
            }
        }

        private void InstallSqlServer()
        {
            string path = dbPath.Text;
            string instance = newInstanceName.Text;

            DirectoryInfo dir = new DirectoryInfo(path);
            if (instance.Length > 0)
            {
                ProcessLaunch.RunExeAndWait("SQL Server 2008\\SQLEXPRWT_x86_ENU.exe", "/qb INSTANCENAME=" + instance + " SQLCOLLATION=Latin1_General_CI_AS ADDLOCAL=All SECURITYMODE=SQL INSTALLSQLDATADIR=" + dir.FullName);
            }
            else
            {
                ProcessLaunch.RunExeAndWait("SQL Server 2008\\SQLEXPRWT_x86_ENU.exe", "/qb SQLCOLLATION=Latin1_General_CI_AS ADDLOCAL=All SECURITYMODE=SQL INSTALLSQLDATADIR=" + dir.FullName);
            }
//            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Microsoft SQL Server\\MSSQL.1\\MSSQLServer", "LoginMode", 2);
          }
    }

    public static class ProcessLaunch
    {
        public static void RunExeAndWait(string exePath, string args)
        {
            Process process = new Process();
            process.StartInfo = new ProcessStartInfo(exePath, args);
            process.Start();
            process.WaitForExit();
        }

        public static Process RunExe(string exePath, string args)
        {
            Process process = new Process();
            process.StartInfo = new ProcessStartInfo(exePath, args);
            process.Start();
            return process;
        }
    }
}