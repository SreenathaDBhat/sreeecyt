﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using AI.Connect;
using System.Windows.Threading;

namespace AI
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class MainScreen : Window
    {
        ConnectionSettingsServer connection = null;

        public MainScreen()
        {
            InitializeComponent();

            connection = new ConnectionSettingsServer();
            DataContext = connection;

            sqlBusy.IsBusy = true;

            ThreadPool.QueueUserWorkItem(delegate
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate
                {
                    SqlServer sql = InitialiseSqlFromRegistry(Environment.MachineName);
                    connection.Sql = sql;
                    sqlBusy.IsBusy = false;
                    IsEnabled = true;
                });
             });
         }

        public void OnDragWindow(object sender, MouseEventArgs e)
        {
            DragMove();
        }

        private SqlServer InitialiseSqlFromRegistry(string casebaseMachineName)
        {
            SqlServer sql = new SqlServer();

            if (Locations.ConfigFile.Exists/* && AI.Connect.NetworkTest.TestSql()*/)
            {
                if (sql.ParseConnectionString(Locations.ConnectionString))
                {
                    sql.TestConnection();
                    return sql;
                }
            }

            sql.Username = "cv";
            sql.Password = "OldBoOt46XX";

            if (sql.InstanceName == null)
                sql.InstanceName = "";

            sql.TestConnection();
            return sql;
        }



        private void OnInstallMicrosoftSqlServer(object sender, RoutedEventArgs e)
        {
            InstallMicrosoftSqlServer dlg = new InstallMicrosoftSqlServer();
            dlg.ShowDialog();
        }

        private void OnRunDatabaseScript(object sender, RoutedEventArgs e)
        {

        }

        private void Refresh()
        {
            DataContext = null;
            DataContext = connection;
        }

        private void OnConfigureSql(object sender, RoutedEventArgs e)
        {
            SqlConfiguration config = new SqlConfiguration(connection);
            config.Owner = this;
            if (config.ShowDialog().GetValueOrDefault(false))
            {
                connection.Sql = config.Sql;
                Refresh();
            }
        }

        private void OnCloseClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
