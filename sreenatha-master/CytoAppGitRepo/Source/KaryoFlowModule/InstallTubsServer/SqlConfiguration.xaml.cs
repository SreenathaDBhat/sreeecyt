﻿using System;
using System.Windows;
using System.Threading;
using System.Windows.Threading;
using System.Windows.Controls;
using System.IO;
using Microsoft.Win32;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Smo;
using System.Reflection;
using Microsoft.SqlServer.Management.Common;
//using System.ServiceProcess;

namespace AI
{
    public partial class SqlConfiguration : Window
    {
        private ConnectionSettingsServer originalConnection;
        private SqlServer sql;

        public SqlConfiguration(ConnectionSettingsServer connection)
        {
            originalConnection = connection;

            sql = new SqlServer();
            sql.CopyFrom(connection.Sql);

            InitializeComponent();
            DataContext = sql;
            if (!sql.WindowsAuthentication)
                sqlServerCheck.IsChecked = true;

            password.Password = sql.Password;

            sql.TestConnection();
            fileStreamStore.IsChecked = true;
            sql.OkToRunScript = false;
        }

        public void CheckBlobStore()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate
            {
                if (sql.IsConfiguredForBlobpool())
                    blobpoolStore.IsChecked = true;
                else if (ConnectionSettingsServer.IsConfiguredForFileStream(sql.BuildConnectionString(false)))
                    fileStreamStore.IsChecked = true;
                else
                    databaseStore.IsChecked = true;
            });
        }

        public SqlServer Sql
        {
            get { return sql; }
        }

        private void OnCloseClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void OnOk(object sender, RoutedEventArgs e)
        {
            SqlServer sql = (SqlServer)DataContext;
            sql.SaveConnection();

            DialogResult = true;
        }

        private void OnTestConnection(object sender, RoutedEventArgs e)
        {
            testConnectionBusy.IsBusy = true;
            IsEnabled = false;
            SqlServer sql = (SqlServer)DataContext;
            
            Thread thread = new Thread((ThreadStart)delegate
            {
                sql.TestServerConnection();
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate
                {
                    testConnectionBusy.IsBusy = false;
                    IsEnabled = true;
                });
            });

            thread.Start();
        }

        private void SqlAuthChecked(object sender, RoutedEventArgs e)
        {
            SqlServer sql = (SqlServer)DataContext;
            sql.WindowsAuthentication = false;
        }

        private void WindowsAuthChecked(object sender, RoutedEventArgs e)
        {
            SqlServer sql = (SqlServer)DataContext;
            sql.WindowsAuthentication = true;
        }

        private void OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            SqlServer sql = (SqlServer)DataContext;
            sql.Password = password.Password;
        }

        private bool WaitForSettle()
        {
            bool ret = false;
            int tries = 0;
            while (!ret && tries++ < 20)
            {
                try
                {
                    AI.Connect.BlobPool.GetBlobPath(sql.BuildConnectionString(true));
                    ret = true;
                }
                catch 
                {
                    System.Diagnostics.Trace.WriteLine(tries.ToString());
                    System.Threading.Thread.Sleep(500);
                }
            }
            return ret;
        }

        private void OnRunDatabaseScript(object sender, RoutedEventArgs e)
        {
            ConnectionSettingsServer tempConnection = new ConnectionSettingsServer();
            SqlServer sql = (SqlServer)DataContext;
            tempConnection.Sql = sql;

            // put this on a thread with busy display?
            ICaseConverter caseConverter = tempConnection.RunDatabaseScript();
/*
            if (caseConverter != null)
            {
                ConversionProgressWindow prog = new ConversionProgressWindow(caseConverter);
                prog.Owner = VisualTreeWalker.FindParentOfType<Window>(this);
                prog.ShowDialog();
            }
 */
            WaitForSettle();
            
            if (sql.IsConfigStorageValid)
            {
                if (fileStreamStore.IsChecked == true)
                {
                    if (!ConnectionSettingsServer.IsConfiguredForFileStream(sql.BuildConnectionString(false)))
                        tempConnection.RunFileStreamScript(sql.FileStreamPath);
                }
                else if (blobpoolStore.IsChecked == true)
                {
                    sql.BlobpoolPathDb = sql.BlobpoolPath;
                }
            }

            sql.TestConnection();
        }

        private void OnDragWindow(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void OnFileStreamChecked(object sender, RoutedEventArgs e)
        {
            SqlServer sql = (SqlServer)DataContext;
            sql.OkToRunScript = false;
            string name = ((RadioButton)sender).Name;

            switch (name)
            {
                case "fileStreamStore":
                    {
                        sql.ValidateFileStream();
                    }
                    break;
                case "blobpathStore":
                    {
                        sql.ValidateBlobpool();
                    }
                    break;
                case "databaseStore":
                    {
                        sql.OkToRunScript = true;
                    }
                    break;
            }
        }

        private void OnCheckFSPath(object sender, RoutedEventArgs e)
        {
            SqlServer sql = (SqlServer)DataContext;
            sql.ValidateFileStream();
        }

        private void OnCheckBlobpoolPath(object sender, RoutedEventArgs e)
        {
            SqlServer sql = (SqlServer)DataContext;
            sql.ValidateBlobpool();
        }
    }
}
