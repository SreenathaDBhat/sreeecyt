﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Windows.Data;
using System.Windows;
using System.Windows.Media;
using System.Linq;
using AI.Connect;
using System.IO;

namespace AI
{
    public enum SqlServerInstall
    {
        Unverified,
        InvalidConnection,
        SqlException,
        ValidServer_NoCvDatabase,
        ValidServer_UnableToRunScript,
        ValidServer_CvDatabaseOutOfDate,
        ValidServer_CvDataFullyUpToDate
    }

    public class ConnectionSettings : Notifier
    {
        SqlServer sql = new SqlServer();

        public ConnectionSettings()
        {
            sql.IsValidChanged += new EventHandler(sql_IsValidChanged);
        }

        void sql_IsValidChanged(object sender, EventArgs e)
        {
            Notify("IsValid");
        }

        public SqlServer Sql
        {
            get { return sql; }
            set { sql = value; Notify("Sql"); Notify("IsValid"); }
        }

        public bool IsValid
        {
            get { return sql.IsValid; }
        }
    }

    public class SqlServer : Notifier
    {
        public event EventHandler IsValidChanged;

        private const string CVUsername = "cv";
        private const string CVPassword = "OldBoOt46XX";

        public void CopyFrom(SqlServer other)
        {
            validated = other.validated;
            windowsAuthentication = other.windowsAuthentication;
            machine = other.machine;
            username = other.username;
            password = other.password;
            instanceName = other.instanceName;
            dbVersion = other.dbVersion;
            sqlServerStatus = other.sqlServerStatus;
            sqlInstallStatus = other.sqlInstallStatus;
            fileStreamPath = other.fileStreamPath;
            blobpoolPath = other.blobpoolPath;
        }

        private bool validated = true;
        public bool IsValid
        {
            get { return validated; }
        }

        private void NotifyChanges()
        {
            Notify("CurrentVersion");
            Notify("SqlServerStatus");
            Notify("SqlInstallStatus");
            Notify("SqlException");
            Notify("IsValid");
            if (IsValidChanged != null)
                IsValidChanged(this, new EventArgs());
        }

        private SqlServerInstall sqlInstallStatus = SqlServerInstall.ValidServer_CvDataFullyUpToDate;
        public SqlServerInstall SqlInstallStatus
        {
            get { return sqlInstallStatus; }
        }

        private string sqlException = string.Empty;
        public string SqlException
        {
            get { return sqlException; }
        }

        private string sqlServerStatus = "Unverified";
        public string SqlServerStatus
        {
            get { return sqlServerStatus; }
        }

        private string machine;
        public string Machine
        {
            get
            {
                if (machine == null)
                    return null;

                return machine == "." || machine.ToLower() == "localhost" ? Environment.MachineName : machine;
            }
            set
            {
                machine = value;
                validated = false;
                sqlServerStatus = "Unverified";
                sqlInstallStatus = SqlServerInstall.Unverified;
                Notify("Machine");
                NotifyChanges();
            }
        }

        private bool windowsAuthentication;
        public bool WindowsAuthentication
        {
            get { return windowsAuthentication; }
            set
            {
                windowsAuthentication = value;
                validated = false;
                sqlServerStatus = "Unverified";
                sqlInstallStatus = SqlServerInstall.Unverified;
                Notify("WindowsAuthentication");
                NotifyChanges();
            }
        }

        private string fileStreamPath;
        public string FileStreamPath
        {
            get { return fileStreamPath; }
            set
            {
                fileStreamPath = value;
                OkToRunScript = false;
            }
        }

        private string blobpoolPath;
        public string BlobpoolPath
        {
            get { return blobpoolPath; }
            set
            {
                blobpoolPath = value;
                OkToRunScript = false;
            }
        }

        private string username = CVUsername;
        public string Username
        {
            get { return username; }
            set
            {
                username = value;
                validated = false;
                sqlServerStatus = "Unverified";
                sqlInstallStatus = SqlServerInstall.Unverified;
                Notify("Username");
                NotifyChanges();
            }
        }

        private string password = CVPassword;
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                validated = false;
                sqlServerStatus = "Unverified";
                sqlInstallStatus = SqlServerInstall.Unverified;
                Notify("Password");
                NotifyChanges();
            }
        }

        private string instanceName;
        public string InstanceName
        {
            get { return instanceName; }
            set
            {
                instanceName = value;
                validated = false;
                sqlServerStatus = "Unverified";
                sqlInstallStatus = SqlServerInstall.Unverified;
                Notify("InstanceName");
                NotifyChanges();
            }
        }

        private int dbVersion;
        public int CurrentVersion
        {
            get { return dbVersion; }
            set { dbVersion = value; Notify("Version"); }
        }

        public string Version
        {
            get
            {
                if (dbVersion == -1)
                    return "Not available";

                return dbVersion.ToString();
            }
        }

        public bool TestTubsExists()
        {
            Database db = new Database(BuildConnectionString(false));
            return db.TubsExists();
        }

        public int GetTubsVersion()
        {
            int ver = -1;
            Database db = new Database(BuildConnectionString(true));
            var v = db.Versions.FirstOrDefault();

            if (v != null)
            {
                ver = (int)v.number;
            }
            return ver;
        }

        public bool IsConfigStorageValid
        {
            get { return sqlInstallStatus == SqlServerInstall.ValidServer_NoCvDatabase; }
        }

        public void TestConnection()
        {
            try
            {
                if (TestTubsExists())
                {
                    dbVersion = GetTubsVersion();

                    if (dbVersion != ConnectionSettingsServer.RequiredDbVersion)
                    {
                        sqlServerStatus = "Db Confirmed. Version Invalid";
                        sqlInstallStatus = SqlServerInstall.ValidServer_CvDatabaseOutOfDate;
                        validated = false;
                    }
                    else
                    {
                        sqlServerStatus = "Confirmed";
                        sqlInstallStatus = SqlServerInstall.ValidServer_CvDataFullyUpToDate;
                    }

                }
                else
                {
                    sqlServerStatus = "Invalid";
                    sqlInstallStatus = SqlServerInstall.ValidServer_NoCvDatabase;
                    validated = false;
                    dbVersion = -1;
                }
            }
            catch (SqlException e)
            {
                string x = e.Message;
                sqlServerStatus = "Invalid";
                sqlInstallStatus = SqlServerInstall.InvalidConnection;
                validated = false;
            }
            catch (InvalidOperationException)
            {
                sqlServerStatus = "Invalid";
                sqlInstallStatus = SqlServerInstall.InvalidConnection;
                validated = false;
            }

            NotifyChanges();
            //return IsValid;
        }

        public void TestServerConnection()
        {
            try
            {
                if (TestTubsExists())
                {
                    try
                    {
                        dbVersion = GetTubsVersion();

                        if (dbVersion != ConnectionSettingsServer.RequiredDbVersion)
                        {
                            sqlServerStatus = "Database Confirmed. Version Invalid";
                            sqlInstallStatus = SqlServerInstall.ValidServer_CvDatabaseOutOfDate;
                            validated = false;
                        }
                        else
                        {
                            sqlServerStatus = "Confirmed";
                            sqlInstallStatus = SqlServerInstall.ValidServer_CvDataFullyUpToDate;
                        }
                    }
                    catch (SqlException e)
                    {
                        sqlInstallStatus = SqlServerInstall.ValidServer_UnableToRunScript;
                        sqlException = e.Message;
                    }
                }
                else
                {
                    sqlServerStatus = "Invalid";
                    sqlInstallStatus = SqlServerInstall.ValidServer_NoCvDatabase;
                    validated = false;
                    dbVersion = -1;
                }
            }
            catch (SqlException e)
            {
                sqlException = e.Message;
                sqlInstallStatus = SqlServerInstall.SqlException;
                validated = false;
            }
            catch (InvalidOperationException)
            {
                sqlServerStatus = "Invalid";
                sqlInstallStatus = SqlServerInstall.InvalidConnection;
                validated = false;
            }

            NotifyChanges();
        }

        private bool okToRunScript = true;
        public bool OkToRunScript
        {
            get { return okToRunScript; }
            set { okToRunScript = value; Notify("OkToRunScript");}
        }

        public void ValidateFileStream()
        {
            OkToRunScript = CheckFSPathExists();
        }

        private bool CheckFSPathExists()
        {
            bool ret = false;
            if (Directory.Exists(FileStreamPath))
            {
                ret = true;
                if (!Directory.Exists(FileStreamPath))
                    ret = false;
            }
            return ret;
        }

        public void ValidateBlobpool()
        {
            OkToRunScript = CheckBlobpoolExist();
        }

        private bool CheckBlobpoolExist()
        {
            // check unc
            if (string.IsNullOrEmpty(BlobpoolPath) || BlobpoolPath[0] != '\\')
                return false;

            BlobpoolPath.TrimEnd('\\', '/');

            if (!Directory.Exists(BlobpoolPath))
                return false;
            // check accessible
            return true;
        }

        public bool IsConfiguredForBlobpool()
        {
            if (IsValid)
                return BlobpoolPathDb != null;
            return false;
        }

        public string BlobpoolPathDb
        {
            get
            {
                return AI.Connect.BlobPool.GetBlobPath(BuildConnectionString(true));
            }
            set
            {
                AI.Connect.BlobPool.SetBlobPath(BuildConnectionString(true), value);
            }
        }

        public void SaveConnection()
        {
            Locations.ConnectionString = BuildConnectionString();
            Locations.Save();
        }

        public string BuildConnectionString()
        {
            return BuildConnectionString(true);
        }

        public string BuildConnectionString(bool includeInitialCatalog)
        {
            string instance = (instanceName != null && instanceName.Length > 0) ? "\\" + instanceName : "";

            if (windowsAuthentication)
            {
                return string.Format(CultureInfo.CurrentUICulture, "Data Source={0}{1};Integrated Security=True;{2}", machine, instance, includeInitialCatalog ? "Initial Catalog=Tubs" : "");
            }
            else
            {
                return string.Format(CultureInfo.CurrentUICulture, "Data Source={0}{1};User ID={2};Password={3};{4}", machine, instance, username, password, includeInitialCatalog ? "Initial Catalog=Tubs" : "");
            }
        }

        public bool ParseConnectionString(string connectionString)
        {
            string[] bits = connectionString.Split(';');
            string dataSource = GetValue(bits, "Data Source");
            if (dataSource == null)
                return false;

            string[] dataSourceBits = dataSource.Split('\\');
            Machine = dataSourceBits[0];

            if (dataSourceBits.Length > 1)
                InstanceName = dataSourceBits[1];

            string integratedSecurity = GetValue(bits, "Integrated Security");
            if (integratedSecurity == null || integratedSecurity.ToUpper() != "TRUE")
            {
                WindowsAuthentication = false;
                Username = GetValue(bits, "User ID");
                Password = GetValue(bits, "Password");
            }
            else
            {
                WindowsAuthentication = true;
            }

            return true;
        }

        private string GetValue(string[] bits, string valueName)
        {
            valueName = valueName.ToUpper();

            foreach (string bit in bits)
            {
                string[] keyValuePair = bit.Split('=');
                if (keyValuePair == null || keyValuePair.Length != 2)
                    continue;

                if (keyValuePair[0].Trim().ToUpper() == valueName)
                {
                    return keyValuePair[1].Trim();
                }
            }

            return null;
        }
    }
}
