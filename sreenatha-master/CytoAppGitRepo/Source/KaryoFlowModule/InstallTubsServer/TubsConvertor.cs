﻿using System;
using System.Windows.Threading;
using System.ComponentModel;
using System.Threading;

namespace AI
{

    public interface ICaseConverter : INotifyPropertyChanged
    {
        void Convert(Dispatcher dispatcher);

        event EventHandler ConversionComplete;

        int TotalCases { get; }
        int CurrentCaseNumber { get; }
    }

    public class TubsConverter1 : ICaseConverter
    {
        private int currentCase = 0;
        private int totalCases = 0;

        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler ConversionComplete;

        public TubsConverter1()
        {

        }

        public int TotalCases
        {
            get { return totalCases; }
        }

        public int CurrentCaseNumber
        {
            get { return currentCase; }
        }


        public void Convert(Dispatcher dispatcher)
        {
            ThreadPool.QueueUserWorkItem(delegate
            {
                // Do Conversion

                dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate
                {
                    if (ConversionComplete != null)
                        ConversionComplete(this, EventArgs.Empty);

                });
            });
        }

        private void SetTotalCases(Dispatcher dispatcher, int count)
        {
            totalCases = count;
            dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("TotalCases"));

            });
        }

        private void SetCurrentCase(Dispatcher dispatcher, int cur)
        {
            currentCase = cur;
            dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("CurrentCaseNumber"));

            });
        }
    }
}