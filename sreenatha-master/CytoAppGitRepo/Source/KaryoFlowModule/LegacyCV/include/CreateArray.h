/*****************************************************************************
               Copyright (c) 1999 Applied Imaging International 
 
	Source file:    CreateArray.h
 
	Function        Header file for CreateArray.cpp

	Package:        (The application/library to which this file belongs)

	Modification history:
	Author      Date        Description
	SN          15Apr99     Initial implementation

 ****************************************************************************/

#ifndef CREATEARRAY_H
#define CREATEARRAY_H

/** MACROS ******************************************************************/

/** TYPEDEFS, STRUCTS *******************************************************/

/** PUBLIC FUNCTION PROTOTYPES **********************************************/


#ifdef __cplusplus
extern "C" {
#endif

void * CreateArray1D( size_t num, size_t size, LPCTSTR  szfnname, LPCTSTR  szvarname, int * pfarrayOK);
void DestroyArray1D( void * pv);

double ** CreateArray2Ddouble( size_t num1, size_t num2, LPCTSTR  szfnname, LPCTSTR  szvarname, int * pfarrayOK);
void DestroyArray2Ddouble(     size_t num1, size_t num2, double ** ppd);

double *** CreateArray3Ddouble( size_t num1, size_t num2, size_t num3, LPCTSTR  szfnname, LPCTSTR  szvarname, int * pfarrayOK);
void DestroyArray3Ddouble(      size_t num1, size_t num2, size_t num3, double *** pppd);

int ** CreateArray2Dint( size_t num1, size_t num2, LPCTSTR  szfnname, LPCTSTR  szvarname, int * pfarrayOK);
void DestroyArray2Dint(  size_t num1, size_t num2, int ** ppn);

#ifdef __cplusplus
}
#endif


/** PUBLIC (extern) DECLARATIONS ********************************************/


#endif
/*****************************************************************************
               Copyright (c) 1999 Applied Imaging International 
 ****************************************************************************/
