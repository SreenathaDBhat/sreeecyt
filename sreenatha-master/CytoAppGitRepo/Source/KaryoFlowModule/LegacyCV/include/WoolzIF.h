////////////////////////////////////////////////////////////////////////////////////////
//
// Interface to the WOOLZ Library
//
// Written By Karl Ratcliff 22082001
// 
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//


#ifndef __WOOLZIF_H
#define __WOOLZIF_H

#ifdef UTS_EXPORTS
#define UTS_API
#else
#define UTS_API
#endif

#define BYTE unsigned char
#define BOOL int



#define PPOINT POINT*

#include <list>

/////////////////////////////////////////////////////////////////////////////
// Define the maximum number of objects allowed at any one time
/////////////////////////////////////////////////////////////////////////////

#define MAX_WOOLZ_OBJECTS               20000
#define MAX_HOLES                       50
#define MAX_ANGS                        150
#define	ANGTHRES                        -146

////////////////////////////////////////////////////////////////////////////////////////
// Type Definitions
////////////////////////////////////////////////////////////////////////////////////////
//struct for floodfill seed
typedef struct structFloodSeed
{
	struct structFloodSeed * prev;
	int x, y;
}
FLOODSEED;

struct structPoint
{
	int x, y;
};

#define PPOINT structPoint*


typedef struct structMergeObj
{
	struct structMergeObj* Parent;
	struct structMergeObj* N;
	struct structMergeObj* S;
	struct structMergeObj* E;
	struct structMergeObj* W;
	struct structMergeObj* Child1;
	struct structMergeObj* Child2;
	struct structMergeObj* Next;
	BYTE		Level;
	BYTE		type;
	int			col;
	int			line;
	long		val;
}
MERGEOBJ;

// Structure holding a number of measurements

typedef struct {
	long		EnclosedArea;	    // Object area (filled does not take into account holes)
    long        Area;               // Actual Area 
	long		BBLeft, BBRight, 
				BBTop, BBBottom;	// Bounding box
	double	    Perimeter;			// Perimeter length
	double		Circularity;		// Circularity measurement
	double		AxisRatio;			// Ratio of min width rectangle width and height
	double		Compactness;
	double		COGx, COGy;
	double		MaxRadii, MinRadii; // Max and min radii
    double      Orientation;
    long        Width;              // True width and height (min width rectangle)
    long        Height; 
} WoolzMeasurements;


////////////////////////////////////////////////////////////////////////////////////////
// Class definition
////////////////////////////////////////////////////////////////////////////////////////

class UTS_API CWoolzIP 
{
    // Structure linking two objects together - these are just pointers objects

    typedef struct 
    {
        int     ObjI;           // Pointer to the first object
        int     ObjJ;           // Pointer to the second object 
        BOOL    Measured;
        BOOL    GMeasured;
        BOOL    MaskCreated;
    } MergeObj;

public:

    // Return values for measure merged object routine
    enum 
    {
        NotMergedObject = 1,                    // Object not a merged object
        MergedObject = 2,                       // Object is a merged object
        NullObject = 0                          // Object no longer exists
    };

    CWoolzIP();
    ~CWoolzIP();

    BOOL Initialise(void);
    // Kill the list of merged objects
    void KillMergedObjects(void);

    // Woolz functions
    int GradThresh2(BYTE *image, int cols, int lines);
    int BackgroundThreshold(BYTE *image, int cols, int lines);
    BYTE *FSMin(BYTE *image, int cols, int lines, int filtsize);
    BYTE *FSMax(BYTE *image, int cols, int lines, int filtsize);
    BYTE *FSOpen(BYTE *image, int cols, int lines, int filtsize);
    BYTE *FSClose(BYTE *image, int cols, int lines, int filtsize);
    BYTE *FSTopHat(BYTE *image, int cols, int lines, int filtsize);
	BYTE *FSExtract(BYTE *image, int cols, int lines, int filtsize, int noisefiltsize);
	long FSClahe(BYTE *image, int cols, int lines, int Min, int Max, int uiNrX, int uiNrY, int uiNrBins, int fCliplimit);
    BYTE *FSMin84(BYTE *image, int cols, int lines, int filtsize);
    BYTE *FSMax84(BYTE *image, int cols, int lines, int filtsize);
    BYTE *FSOpen84(BYTE *image, int cols, int lines, int filtsize);
    BYTE *FSClose84(BYTE *image, int cols, int lines, int filtsize);
	BYTE *FSExtract84(BYTE *image, int cols, int lines, int filtsize, int noisefiltsize);
	BYTE *FSMedian(BYTE *image, int cols, int lines, int filtsize);
	BYTE *FSGaussian(BYTE *image, int cols, int lines, float radius);
	BYTE *FSUnsharpMask(BYTE *image, int cols, int lines, float radius, float depth);
	BYTE *FSSpotExtract(BYTE *image1, int width, int height, int StructSize, int MinArea);

    // Pathvysion routines
    // Spot extraction probe image processing
    BYTE *PathVysion(BYTE *image, int width, int height, int SpotWidth, int ClusterWidth, int MinArea);
    // Cell extraction from tissue
    BYTE *PathOlogical(BYTE *image, int width, int height, int CellWidth);

    // Clear the current list of objects in memory
    void FreeObjects(void);

    // Object shape measurement stuff
    int FindObjects(BYTE *image, int cols, int lines, int Thresh, int MinArea);

    // Object shape measurement stuff
    int FindObjectsEx(BYTE *image, int cols, int lines, int Thresh, int MinArea, int MaxArea, int Border);

    // Find objects and merge objects within merge object distance of one another
    int FindObjectsAndMerge(BYTE *image, int W, int H, int Thresh, int AbsMin, int MinArea, int MaxArea, int BorderWidth, int MergeDist);

    // Get data about a specific object
    BOOL GetObjectArea(int ObjectIndex, int *WSD);

    // Get the mass of an object = sum of its grey levels
    BOOL GetObjectMass(int ObjectIndex, int *Mass);

    // Determine the number of convex points on an object
    int NumberVertices(int ObjectIndex, BOOL troughs, int ThreshLevel);

    // Find the min width rectangle for an object + its orientation
    BOOL MinWidthRectangle(short ObjectIndex, float *angle, PPOINT V1, PPOINT V2, PPOINT V3, PPOINT V4);

	// Find the bounding box for an object
	void BoundingBox(short ObjectIndex, int *Xmin, int *Ymin, int *Xmax, int *Ymax);

    // Recreate the object in an image
    void DrawObject(short ObjectIndex, BYTE *image, int image_width, int image_height);

    // Fill the object with a certain value in an image
    void FillObject(short ObjectIndex, BYTE *image, int image_width, int image_height, int value);
	void FillObjectXY(short ObjectIndex, BYTE *image, int x, int y, int image_width, int image_height, int value);

    // Create a mask from a woolz object
    BOOL CreateMask(short ObjectIndex, long MaskOut);

    // Create an 8 bit binary mask from a woolz object
    BOOL CreateMaskFast(short ObjectIndex, long BinIm);

    // Create an 8 bit binary mask from a list of merged objects
    BOOL CreateMaskFromMerged(short ObjectIndex, long Pitch, long H, BYTE *Image);

    // Same but for all objects 
    BOOL CreateMaskFromAllObjects(long MaskOut);
    
    // Same once again but only for those objects with a matching label
    BOOL CreateMaskFromLabelledObjects(long Label, long MaskOut);

    // Get basic grey level info about a woolz object
    void ObjectGreyData(short ObjectIndex, long *MinGrey, long *MaxGrey, long *MeanGrey);

    // Get basic grey level data about a woolz object from ANY image
    void ObjectGreyDataInImage(short ObjectIndex, BYTE *image, long W, long H, long *MinGrey, long *MaxGrey, long *MeanGrey);

    // Get basic grey level data about a merged woolz object from ANY image
    void MergedObjectGreyDataInImage(short ObjectIndex, BYTE *image, long W, long H, long *MinGrey, long *MaxGrey, long *MeanGrey);

    // Label an object
    void LabelObject(short ObjectIndex, long LabelValue);

    // Get the label for an object
    long GetObjectLabel(short ObjectIndex);

    // Count labelled objects only
    long CountLabelledObjects(void);

	void GetPoint(int ObjectIndex, long *X, long *Y);

	// Gradient thresholding routine for woolz objects
	int Objgradthresh(struct object *obj);
	
	// Draw an object in image if its grey levels exceed the threshold
	void ThreshValues(struct object *obj, BYTE *image, int image_width, int image_height, int thresh);

	// Take various measurements on an object perimeter
	BOOL MeasureObject(short ObjectIndex, WoolzMeasurements *WM);

    // Measure merged objects found with FindObjectsAndMerge
    int MeasureMergedObject(short ObjectIndex, WoolzMeasurements *WM);

    // Boundary operations - returns an array of boundary points
    int MeasureBoundary(short ObjectIndex, long **Boundary);

    // Fractal boundary measurement
    double FractalMeasureBoundary(short ObjectIndex, int NSamples);

    // Fill an object in an image
    void WzFillObject(struct object *Obj, BYTE *image, int image_width, int image_height, int value);
	BOOL WzFillObjectSeed(struct object *Obj, BYTE *image, int cols, int lines, int col, int line,int value);

    // Split a mask up
    void WoolzSplitMask(long RangeImage,  long MinMask,   long MaxMask,     
					    long MinArea,     long MaxArea, 
					    long *NumInRange, long *NumInMin, long *NumInMax);
    long WoolzSplitMask(long RangeImage, long MinArea, long MaxArea);

    // Remove objects touching the edge of a mask
    long WoolzRemoveEdgeObjects(long RangeImage, long BorderWidth);

    // Determine the number of components in an image and the largest area of the component
    long WoolzNumberOfComponents(long RangeImage, long *MaxArea);

    // Get the pointer to a particular object
    void *GetObjectPtr(short ObjectIndex);

 	// Convert an image into a suitable format for Woolz and vice versa
	void UnpadImage(long SrcHandle);
	void PadImage(long SrcHandle);
	long MakePaddedImage(long SrcHandle, BYTE *ImageAddr);

	// Texture metaphase finder
	BYTE* TextureMetFind(BYTE *image, int width, int height, int maxFeatureWidth);

	// Split touching objects
	long WoolzDeagglomerate(long SrcHandle, long DstHandle);

	// Remove objects which look like coverslip
	long WoolzRemoveCoverslipObjects();

    // Remove an object from the list
    BOOL RemoveObject(int index);

    void RangeThreshold(long ImageHandle, long ImageMask, short LowThreshold, short HighThreshold);

    void DrawIdom(short ObjectIndex, BYTE *image, int image_width, int image_height, int value);

    BOOL MaskFill(long R, long G, long B, long Rmask, long Gmask, long Bmask, long Rclass, long Gclass, long Bclass, long RegionMask, long GatedImage, int seed_col, int seed_line,  int thresh);

    long TreeWalk(structMergeObj* seed, structMergeObj* leaf_list[], BOOL init);

    void ColourCost(structMergeObj** leaf_list, int leaf_count, long* val, long* cost); 

    void ShapeMoments(structMergeObj** leaf_list, int leaf_count, double ShapeMoments[10]);

    BOOL MergeObjects();

	void MaskMinDistHSI(long H, long S, long I, long MultiMask,
						short posH, short posS, short posI, 
						short negH, short negS, short negI, 
						short neginposH, short neginposS, short neginposI, 
						short backgroundH, short backgroundS, short backgroundI);

    void WoolzRegionGrowing(long SrcImage);
    // Variables
    // Keep hold of the current number of objects currently found
    int     m_nNumberObjects;   

private:

    // Various utility routines 
    int curvsmooth2(struct object *histo, int iterations);
    void clear_values(struct object *obj, BYTE *image, int image_width, int image_height, int value);
    int number_of_features(struct object *obj, int troughs);
    int feature_pos(struct object *obj, int troughs, struct ipoint **pointlist, int **position, int *npoints);
    struct object *closeObj(struct object *obj, int passes);
    void splitObjects(BYTE *image, int width, int height, int thresh);

    struct  object                  **m_pObjectList;            // Current list of woolz objects
    long                            *m_pLabelList;              // Labels associated with each object
	std::list<MergeObj *>			m_MergeObjects;             // Holds a list of connected objects
    BOOL                            *m_pMergeTable;             // Keeps track of which objects are merged and which are not merged
    BOOL                            *m_pMeasuredObject;         // Keeps track of those objects that have been measured
    BOOL                            *m_pMaskObjectCreated;      // Keeps track of those objects that have had a mask created
    BOOL                            *m_pObjectGreys;            // Keeps track of those objects that have had grey level measurement sampling
};


// typical stack class
class UTS_API FloodStack
{
	int numStack;

	FLOODSEED * floodSeed;

public:
	FloodStack();
	int getNum();
	int peek(int *, int *); //pointer to x,y
	int pop();
	int push(int, int); //x,y
};

// typical stack class
class UTS_API FloodFiller
{
public:
	int dimX;
	int dimY;
	FloodStack floodStack;

public:
	int floodfill(int x, int y, BYTE *buffer, BYTE fill_col, BYTE stop_col);
	int floodseed(int x, int y, BYTE *buffer, BYTE fill_col, BYTE stop_col);

};

#endif                  // #ifndef __WOOLZIF_H
