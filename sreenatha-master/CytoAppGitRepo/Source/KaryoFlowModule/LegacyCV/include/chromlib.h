/*
 * Generated header file.
 * Created from translation of UNIX source.
 *
 * Copyright Applied Imaging corp. 1996
 * Created: Tue Oct 15 15:27:36 1996
 *
 * Original source directory: "chromlib"
 *
 *	06May99	SN	Moved mahalanobis.c to traincl.
 *	09Apr99 SN	Many prototypes removed as private to mahalanobis.c
 *				idistvar removed as fastidist is not in build any more.
 *	08Apr99 SN	Changed return of normvec, applynorm and selectedfeatures.
 *				norm_a_d_l removed as private to featvect.c.
 *				adisttrans removed as private to skelsup.c.
 *				disttrans removed as private to skelsupport.c.
 *	28Apr98	dcb	readdatum() Change 'class' parameter to chromclass for C++
 *
 */


#ifdef WIN32


#ifdef __cplusplus
extern "C" {
#endif


/****** axisslope.c ******/

int axisslope(struct polygondomain *p, int lf, int lr, int *sinsl, int *cossl);


/****** axissmooth.c ******/

void axissmooth(register struct polygondomain *poly, int iterations);


/****** axoutside.c ******/

int axoutside(struct object *mpol, struct object *obj);


/****** centpoly.c ******/

struct object *centpoly(struct object *mpol, int cpos);


/****** centvector.c ******/

struct ivector *centvector(struct object *mpol, int cpos);


/****** chrom_error.c ******/

int chrom_exit(int errno);


/****** chromaxis.c ******/

struct object *chromosomeaxis(struct object *obj);


/****** chromaxis2.c ******/

struct object *chromaxis2(struct object *obj);


/****** cindexa.c ******/

int cindexa(struct object *obj, int l);


/****** cindexl.c ******/

int cindexl(struct object *mpol, register int l);


/****** cindexm.c ******/

int cindexm(struct object *obj, int l);


/****** cmerefeat.c ******/

int centromerefeatures(struct chromosome *obj, struct object *mpol, struct object *prof);


/****** cmlocation.c ******/

int cmerelocation(struct object *prof, int quantum, int mirror);


/****** featvect.c ******/

//int getvec(register struct chromplist *plist, register double *v);
void getvec(register struct chromplist *plist, register double *v);
//int getdim(int print);
int getdim( void);
//int getclass(int print);
int normvec(struct chromosome * *objs, int number, struct normaliser *vn);
//int norm_a_d_l(register struct chromosome * *objs, int number, register struct normaliser *vn);
//int applynorm(register double *fvec,register double *svec,int *sfv,register struct normaliser *vn, int ndim, register int nsfv);
void applynorm(register double *fvec,register double *svec,int *sfv,register struct normaliser *vn, register int nsfv);
//int selectedfeatures(int *sfv, int *nsfv);
void selectedfeatures( int * sfv, int * nsfv);



/****** fipconvhull.c ******/

struct object *fipconvhull(struct object *obj);
int cvhfipsqueeze(struct polygondomain *pdom);


/****** getcellinfo.c ******/

struct cellid *getinformation();


/****** ideogram.c ******/

int ideogram_debug(FILE *chan);
struct chrom_data *get_ideogram(FILE *fp, char chromosome, int band_type);
//int select_band(FILE *fp, struct chrom_data *chrom_ptr, int band_type, char buffer);
//int readbands(FILE *fp, struct chrom_data *chrom_ptr, char buffer);
//int end_of_band_data(FILE *fp, struct chrom_data *chrom_ptr, struct band_data *band_ptr, char buffer);
//int read_sat_data(struct chrom_data *chrom_ptr, char buffer);
int draw_ideogram(struct chrom_data *chrom_ptr, int x_orig, int y_orig, int scale);
int put_bands(struct chrom_data *chrom_ptr, int arm, int x, int y_orig, int scale);
int draw_sat(int x, int y, int length, int width, int grey_color, int scale, int bandtype);
int greycells(struct band_data *band_ptr, int col, int row);
int satel_color(int row, int col, int density);
int label_ideogram(struct chrom_data *chrom_ptr, int x_orig, int y_orig, int scale);
struct band_data *line(char l, int arm, struct band_data *band_ptr, struct chrom_data *chrom_ptr, int scale);
int put_caption(struct band_data *band_ptr, int pix_x, int pix_y, int scale);
int draw_line();
char *skipband(char *b, int i);
int skipchrom(FILE *fp, char *b, char id);
int free_ideogram(struct chrom_data *chrom_ptr);
int greycheck(struct band_data *band_ptr, int density);
int convert(float decimal, char *s1, char *s2);


#if 0
/****** mahalanobis.c ******/

//int int1alloc(int * *p, int n);
//int int2alloc(int * * *p, int m, int n);
//int initmahalanobis(int dim);
//int findclass(double *fvec, double *priorp, struct cllik *liklv, int nclass, int ndim);
int findclass(double *fvec, double *priorp, struct cllik *liklv, int nclasses, int nfeatures);
//int classmahal(double *fvec, double *priorp, struct cllik *liklv, int nclass, int ndim);
//int classrawmahal(double *fvec, struct cllik *liklv, int nclass, int ndim);
//double distvar(register double *fv, register double *mv, register double *cliv, int dim);
//int classdist(double *fvec, double *lpriorp, register struct cllik *liklv, int nclass, int ndim);
//int classrawdist(double *fvec, struct cllik *liklv, int nclass, int ndim);
//int iclassdist(double *fvec, double *priorp, struct cllik *liklv, int nclass, int ndim);
//FILE *open_classfile(char *classfile);
FILE * open_classfile(char *classfile, int *pnclasses, int *pnfeatures);
//int readclassparams(FILE *c, register int *nclass, register int *ndim, register int *sf, register int *nsf);
//int liksort(register struct cllik *a, register struct cllik *b);
//int readdatum(FILE *fp, int *chromclass, double *fv, int nclasses, int ndim);
//int make_cholesky_decomps(int nclass, int nsfv);
//int vcv_cholesky(double *a, double *b, int n);
//int read_class_params2(FILE *c, int *nclasses, int *ndims, int *sfeatvec, int *nfeatures, double *lpp);
//int read_c_params(FILE *c, int *nclasses, int *ndims, int *sfeatvec, int *nfeatures, double *lpp);
void read_c_params( FILE *c, int nclasses, int *sfeatvec, int nfeatures, double *lpp);
//int set_def_params(register int *nclasses, register int *ndims, register int *sfeatvec, register int *nfeatures, double *lpp);
void set_def_params(register int *nclasses, register int *sfeatvec, register int *nfeatures, double *lpp);
#endif


/****** massdiff.c ******/

int massdiff(struct chromosome *obj);


/****** mfeatures.c ******/

struct chromosome *chromfeatures(struct chromosome *obj, int basic, double xscale, double yscale);
int basicfeatures(struct chromosome *obj);
void new_centromere(struct chromosome *obj);


/****** midptspoly.c ******/

struct object *midptspoly(struct object *obj);


/****** mirrorprof.c ******/

struct object *mirrorprof(struct object *p, register int *o, int *e);


/****** modifytips.c ******/

int modifytips(struct object *p);


/****** mprofiles.c ******/

void multiprofiles(struct chromosome *obj, struct object *mpol, struct object * *prof, int *moments, int nmoments);


/****** ncentromere.c ******/

struct ipoint *ncentromere(struct object *mpol, struct object *prof);


/****** nprofile.c ******/

struct object *nprofile(struct chromosome *obj, struct object *mpol, int moment);


/****** pmsaxis.c ******/

struct object *pmsaxis(struct object *obj, int tip, int step, int tipstraight);


/****** probotype.c ******/

int peaks(struct object *obj, int interv);
int probotype(struct chromosome *obj);


/****** profslice.c ******/

int profslice(struct object *obj, int x2, int y2, int sin2, int cos2, int halfwidth, GREY *g);


/****** proplist.c ******/

struct propertylist *makechromplist();


/****** rearrange.c ******/

int rearrange(struct chromcl *chrcl, struct classcont *clcontents, int numobj, int *clwn, int nclass);
struct chchain *followchains(struct chchain * *chn, struct chromcl *chrcp, double *paccum, int depth, struct classcont *clcontents, int nclass, int *clwn);


/****** removeblips.c ******/

int removeblips(struct object *p);


/****** setnextpoint.c ******/

int setnextpoint(struct polygondomain *p, int *xx, int *yy, int *sini, int *cosi, int sln);


/****** shorten.c ******/

int shorten(struct object *mpol, struct object *prof0, struct object *prof2);


/****** skelaxis.c ******/

struct object *skelaxis(struct object *obj);


/****** skelsup.c ******/

int afindtipandbranch(register struct object *sk, int *tipx, int *tipy);
//int adisttrans(register struct object *sk, register k, register l);
struct object *apolyfromskel(struct object *skobj, int tipx, int tipy);


/****** skelsupport.c ******/

int findtipandbranch(register struct object *sk);
//int disttrans(register struct object *sk, register k, register l);
struct object *polyfromskel(struct object *skobj);


/****** straight.c ******/

struct object *straighten(struct object *obj, struct object *poly, int demo, struct pframe *frame);


/****** uspaxis8.c ******/

struct polygondomain *uspaxis8(register struct polygondomain *poly1);


/****** vertical.c ******/

struct chromosome *vertical(struct chromosome *obj, double xcale, double yscale);


/****** wdd.c ******/

int wdd(struct object *histo, register short *v);
int cvdd(struct object *histo);
int nssd(struct object *histo);
struct object *gradprof(struct object *histo);


/****** wddfeatures.c ******/

int wddfeatures(struct chromosome *obj, struct object *prof0, struct object *prof2);


/****** xexcursion.c ******/

int xexcursion(struct object *pol);


/****** xrange.c ******/

int xrange(struct object *pol);


/****** centroaxis.c ******/

void centro_axis_features(struct chromosome *obj, struct object *mpol);


/****** cholesky.c ******/

void dchdc(double *a, int *lda, int *p, double *work, int *jpvt, int *job, int *info);
void daxpy(int n, double *da, double *dx, int incx, double *dy, int incy);
void dswap(int n, double *dx, int incx, double *dy, int incy);


/****** countpeaks.c ******/

int countpeaks(struct object *prof, int quantum, int profpc);


#ifdef __cplusplus
}
#endif

#endif

