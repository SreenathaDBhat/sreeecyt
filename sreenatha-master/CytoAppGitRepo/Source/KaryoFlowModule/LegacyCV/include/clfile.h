/*****************************************************************************
               Copyright (c) 1999 Applied Imaging International 
 
	Source file:    clfile.h
 
	Function        Header file for clfile.cpp

	Package:        traincl.lib, part of WinCV

	Modification history:
	Author      Date        Description
	SN          06May99     Initial implementation
	SN			11May99		Moved tdcellinfo and tdlist types from
							trainclassCB.cpp to here.
	SN			08Jun99		Moved CLheader declaration to trainkarcl.h for
							use by barcode project.
	SN			07Apr00		Added defines for string lengths in tdcellinfo
							and tdlist.

 ****************************************************************************/

#ifndef CLFILE_H
#define CLFILE_H

/** MACROS ******************************************************************/

// String lengths for tdcellinfo and tdlist structures	//SN07Apr00
#define TDCASELENGTH	20
#define TDSLIDELENGTH	20
#define TDCELLLENGTH	20
#define TDDATELENGTH	20

/** TYPEDEFS, STRUCTS *******************************************************/

struct tdcellinfo{
	TCHAR casename[TDCASELENGTH], slidename[TDSLIDELENGTH], cellname[TDCELLLENGTH];
};


struct tdlist{
	struct tdcellinfo ident;
	TCHAR dateadded[TDDATELENGTH];
	int nobjs;
	struct tdlist *nextcell;
};

/** PUBLIC FUNCTION PROTOTYPES **********************************************/

int  cl_read_class_header(  FILE * fp, int * pclassid, CLheader * pheader);
void cl_write_class_header( FILE * fp, int classid,    CLheader header);

//int  cl_read_data_header(   FILE * fp, int * pclassid, CLheader * pheader);
void cl_write_data_header(  FILE * fp, int classid,    CLheader header);
int  cl_copy_data_header(   FILE * fsrc, FILE * fdst);
int  cl_skip_data_header(   FILE * fp);

void cl_read_all_cell_headers( FILE *fp, int * pcellcount, struct tdlist *firstcell);
void cl_write_cell_header(     FILE *fp, int datacount, struct tdcellinfo *cellinfo);

int  cl_read_datum(  FILE *fp, int *cclass, double *fv, int ndim);
void cl_write_datum( FILE *fp, int cclass,  double *fv, int ndim);


/** PUBLIC (extern) DECLARATIONS ********************************************/

extern const TCHAR szspeciesdefault[CLSPECIESNAMELENGTH];
extern const TCHAR szValidClassToken[];




#endif
/*****************************************************************************
               Copyright (c) 1999 Applied Imaging International 
 ****************************************************************************/
