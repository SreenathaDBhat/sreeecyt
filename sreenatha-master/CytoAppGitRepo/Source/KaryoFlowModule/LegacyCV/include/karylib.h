/*
 * Generated header file.
 * Created from translation of UNIX source.
 *
 * Copyright Applied Imaging corp. 1996
 * Created: Wed Jan 15 17:00:43 1997
 *
 * Original source directory: "."
 *
 *
 */

#pragma once

//#ifdef WIN32
//#include <common.h>
//#include <settings.h>
//#include <canvas.h>
#include <ddgs.h>


#ifdef __cplusplus
extern "C" {
#endif


#define EXT_KARYHEIGHT  800
#define NORM_KARYHEIGHT  576
#define NORM_KARYWIDTH  768

/****** annomod.c ******/

//void annodraw(Canvas *canvas);
//void annoerase(Canvas *canvas);
//void move_anno(DDGS *dg, int drawmode, Canvas *canvas, int newdx, int newdy);
//void vert_flip_poly(struct polygondomain *pdom);
//void invert_anno(DDGS *dg, int drawmode, Canvas *canvas);
//void rotate_scale_anno(DDGS *dg, int drawmode, Canvas *canvas, float rads, double newscale);
//void rotate_anno(DDGS *dg, int drawmode, Canvas *canvas, float rads);
//void scale_anno(DDGS *dg, int drawmode, Canvas *canvas, double newscale);
//
//
///****** announdo.c ******/
//
//void anno_display_selectframe(DDGS *dg, int drawmode, Canvas *canvas, Canvas *parent, int state, int expose);
//void annokeep(Canvas *canvas, DDGS *dg, int drawmode);
//void free_annokeep(Canvas *anno_canvas);
//void restore_canvases(Canvas *canvas);
//void announdo(DDGS *dg, Canvas *anno_canvas);
//
//
///****** autosplit.c ******/
//
//int autosplit(struct kcontrol *kcont, int onum, short TYPE);
//void endsplit_cgh(struct kcontrol *kcont, int onum, int first, int last);
//void endsplit_barcode(struct kcontrol *kcont, int onum, int first, int last);
//void endsplit_mfish(struct kcontrol *kcont, int onum, int first, int last);
//int endsplit(struct kcontrol *kcont, int onum, int TYPE, int AUTO);
//
//
///****** correctaxis.c ******/
//
//void MetCorrectAxis(struct kcontrol *kcont);
//void KarCorrectAxis(struct kcontrol *kcont);
//
//
///****** enhance.c ******/
//
//struct object *enhance(struct object *obj);
//struct object *varenhance(struct object *obj, struct enhancement *enh);
//
//static void adjustFont(struct kcontrol * kcont, Canvas *canvas);
///****** karcanvas.c ******/
//int getPointSize(struct kcontrol * kcont);
//Canvas *create_tagpatch(LPCTSTR tagptr, Canvas *parent, short drawmode);
////Canvas *create_fusetagpatch(LPCTSTR tagptr, Canvas *parent, short drawmode);
//Canvas *create_tagpatchExt(LPCTSTR tagptr, Canvas *parent, short drawmode, int pointsize);
//void adjustClassFontSize(struct kcontrol * kcont);
//void create_class_tagpatch(struct kcontrol *kcont, int chromclass);
//void modsexclasslabel(struct kcontrol *kcont, int chromclass);
//void kar_add_cgh_ratio_highlight(struct kcontrol *kcont, int onum);
//void add_colour_to_CGH_probe_highlight(Canvas *canvas, int probeid);
//void add_colour_to_probe_highlight(struct kcontrol *kcont, Canvas *canvas, int fluor);
//void kar_add_cgh_probe_highlight(struct kcontrol *kcont, int onum);
//void kar_add_mfish_info_tags(struct kcontrol *kcont);
//void kar_show_info_tags(struct kcontrol *kcont, BOOL show);
//void kar_add_mfish_pseudo_highlight(struct kcontrol *kcont, int onum);
//void kar_add_probe_highlight(struct kcontrol *kcont, int onum);
//void kar_add_pkar_pseudo_highlight(struct kcontrol *kcont, int onum);
//void kar_attach_probe_luts(struct kcontrol *kcont);
//void create_kar_axis_highlight(struct kcontrol *kcont, int onum);
//int get_special_centromere_offset(struct kcontrol *kcont, int onum, int *dx, int *dy);
//int get_centromere_offset(struct kcontrol *kcont, int onum, int *dx, int *dy);
//int offset_in_chromosome(struct kcontrol *kcont, int onum, int dx, int dy);
//int get_centromere_from_offset(struct kcontrol *kcont, int onum, int dx, int dy, int *cx, int *cy);
//void create_kar_centromere_highlight(struct kcontrol *kcont, int onum);
//void default_karyogram_canvas(struct kcontrol *kcont);
//void create_karyogram_canvas(struct kcontrol *kcont);
//void newchromcanvas(int onum, int chromclass, int classpos, struct kcontrol *kcont);
//void DefaultKaryLayout(struct kcontrol *kcont);
//int row_height(struct kcontrol *kcont, int first, int last);
//int row_width(struct kcontrol *kcont, int first, int last, int *nchroms);
//Canvas* create_square_canvas(LUT_Triple colour, int dx, int dy, int w, int h);
//
//int get_centromere_leftX_rightX(struct kcontrol *kcont, int onum, int *lx, int *rx);
//
////void adjust_class_width(  struct kcontrol *kcont, int chromclass, int *newx, int chromsep);
////void adjust_class_members(struct kcontrol *kcont, int chromclass, int chromsep);
////void adjust_single_class( struct kcontrol *kcont, int chromclass);
//int class_width_by_chrom( Canvas * classcanvas, int * nchroms);
//void adjust_class_width(  Canvas * classcanvas, int chromclass, int *newx, int chromsep);
//void adjust_class_members(struct kcontrol *kcont, int chromclass, Canvas * classcanvas, int chromsep);
//void adjust_single_class( Canvas * classcanvas);
//
//void adjust_row(          struct kcontrol *kcont, int first, int last);
//void adjust_class_height( struct kcontrol *kcont, int first, int last, int newy, int newht);
//void MaximiseLayout(struct kcontrol *kcont, BOOLEAN rowswap, BOOLEAN autoscale);
//void AutoKaryLayout(struct kcontrol *kcont);
//void realign(struct kcontrol *kcont, int centalign);
//void read_disppos(int chromclass, int *col,int *lin,int *maxobjs,int *width,int *height, LPTSTR *tag);
//void	set_class_RGB_probe_objects_intensities(LUT_Triple *classLUT, struct object * spun_dapi, struct object * spun_pseudo,  struct object * red, struct object *green, struct object *blue, BOOL transparent, float gamma);
//void SetRampLut(Canvas * canvas, LUT_Triple *rgb, BOOL invert, BOOL MFISH); 
//
///****** karcreate.c ******/
//
struct kcontrol *init_kcontrol(DDGS *metdg, DDGS *kardg);
void destroy_kcontrol(struct kcontrol *kcont);
//void kary(struct kcontrol *kcont);
//void modmaxchroms(struct kcontrol *kcont);
//void fillobjclass(struct kcontrol *kcont, int *objclass);
//int minconfval(char *pc1, char *pc2);
void karyogram(struct kcontrol *kcont);
//void newCpgroup(int onum, int chromclass, struct kcontrol *kcont);
//void newpgroup(int onum, int chromclass, struct kcontrol *kcont);
//void firstclass(int onum, struct kcontrol *kcont);
//int checkpos(int chromclass, int pos, struct kcontrol *kcont);
//void reposition(int onum, int chromclass, struct kcontrol *kcont);
//void newclass(int onum, int chromclass, struct kcontrol *kcont);
//void check_classification(struct kcontrol *kcont);
//void initial_flip_thru_180(struct kcontrol *kcont, int onum);
//void initialorientation(int onum, struct kcontrol *kcont);
int set_kcontrol_NumOfClasses(struct kcontrol *kcont, int numofclasses);
//void *kcontReCalloc(void *list, size_t oldnum, size_t newnum, size_t size );
int increase_kcontrol_MaxObjs(struct kcontrol *kcont, int numofobjects);
//
//
///****** karmod.c ******/
//
//int need_to_display(DDGSframe *expdgf, DDGSframe *dispdgf);
//int kar_premod(struct kcontrol *kcont, int onum, Canvas * *chromcanvas, DDGSframe *expdgf);
//int kar_postmod(struct kcontrol *kcont, int onum, DDGSframe *expdgf);
//void reset_obj_frame(struct kcontrol *kcont, Canvas *chromcanvas, BOOL centromere_adjust);
//void adjust_chrom(struct kcontrol *kcont, int onum, int newdx, int newdy);
//void correct_centromere_chrom(struct kcontrol *kcont, int onum, int cx, int cy);
//void correct_axis_chrom(struct kcontrol *kcont, int onum);
//void invert_chrom(struct kcontrol *kcont, int onum);
//void straight_kernel(struct kcontrol *kcont, int onum, int AUTO);
//void manual_straight_chrom(struct kcontrol *kcont, int onum);
//void auto_straight_chrom(struct kcontrol *kcont, int onum);
//void trim_chrom(struct kcontrol *kcont, int onum, int npolys);
//void enhance_chrom(struct kcontrol *kcont, int onum);
//void reflect_chrom(struct kcontrol *kcont, int onum, long);
//void contrast_stretch_chrom(struct kcontrol *kcont);
//void contrast_chrom(struct kcontrol *kcont);
//void restore_chrom(struct kcontrol *kcont, int onum);
//void rotate_scale_chrom(struct kcontrol *kcont, int onum, float rads, double newxscale, double newyscale, BOOL perform_karykeep);
//void rotate_chrom(struct kcontrol *kcont, int onum, float rads);
//void scale_chrom(struct kcontrol *kcont, int onum, double newxscale, double newyscale);
//void swapclass(struct kcontrol *kcont, int onumA, int onumB);
//void compact_stack(int posingrid, struct kcontrol *kcont);
//void remove_chromcanvas(int onum, struct kcontrol *kcont);
//void delete_chrom(int onum, struct kcontrol *kcont);
//void retype_chrom(struct kcontrol *kcont, int onum, int newtype);
//void manclass_chrom(struct kcontrol *kcont, int onum, int chromclass);
//void split_chrom(struct kcontrol *kcont, int onum);
//void join_chrom(struct kcontrol *kcont);
//void new_tagpatch(struct kcontrol *kcont, Canvas *chromcanvas, int chromclass);
//int compress_class(struct kcontrol *kcont, int chromclass, int posremoved);
//void moveclass(int onum, int chromclass, struct kcontrol *kcont);
//void reuse_if_poss(int chromclass, struct kcontrol *kcont);
//void tidyclass(int chromclass, struct kcontrol *kcont);
//void tidykary(struct kcontrol *kcont);
//float getChromCurvature(struct chromosome *obj);
//void expand_class(struct kcontrol *kcont, int chromclass, int newonum);
//void AdjustChromSpacing(struct kcontrol *kcont, int chromclass, int onum);
//
//
///****** karpaste.c ******/
//void PasteKar(struct kcontrol * kcont);
//
//
///****** karundo.c ******/
//
void karykeep(int onum, struct kcontrol *kcont);
void free_karykeep(struct kcontrol *kcont);
//void remove_splitchrom(struct kcontrol *kcont);
//void remove_joinedchrom(struct kcontrol *kcont);
//void karyundo(struct kcontrol *kcont);
//void EnableMaximisLayoutInUndo();
//void DisableMaximisLayoutInUndo();
//
/****** kchromclass.c ******/

#include <tchar.h>

void kclass_def();
int kclass_read(LPCTSTR classfile, int default_human, int NumOfClasses);

void kchromclass( struct chromosome ** chrlist, int numchr);

//void reclassify_obj(        struct kcontrol *kcont, int objnum);
//void kchromclass_by_length( struct kcontrol *kcont);


///****** kchromfeat.c ******/
//
//int keepmeas(struct kcontrol *kcont, int onum);
//void measobj(struct kcontrol *kcont, int onum);
//int check_emol_list(struct kcontrol *kcont);
//
//
///****** khighlight.c ******/
//
//void paste_highlight_inorder(Canvas *highlight, Canvas *parent);
//void reorder_highlights(Canvas *parent);
//void highlight_canvas_object(Canvas *canvas, struct object *highlight, int arena, int col, int onoff);
//void create_obtype_highlight(int atype, struct kcontrol *kcont, int onum);
//void set_obtype_canvas(int onum, struct kcontrol *kcont);
//void set_obtype_canvases(struct kcontrol *kcont);
//void recursive_set_drawmode(Canvas *canvas, int drawmode);
//void set_highlights_visibility(struct kcontrol *kcont, Canvas *canvas, int objects, int centromeres, int axes, int boundaries, int CGHratios, int CGHprobes, int MFISHpseudo, int MFISHprobes, int classlabels, int set_highlights_visibility);
//void set_met_highlights_visibility(struct kcontrol *kcont, int objects, int centromeres, int axes, int boundaries, int CGHratios, int CGHprobes, int MFISHpseudo, int MFISHprobes, int classlabels, int show_coloured_boundaries);
//void set_kar_highlights_visibility(struct kcontrol *kcont, int objects, int centromeres, int axes, int CGHratios, int CGHprobes, int MFISHpseudo, int MFISHprobes, int classlabels);
//void set_canvas_background(Canvas *canvas, DDGS *dg);
//void show_met_highlights(struct kcontrol *kcont);
//void show_kar_highlights(struct kcontrol *kcont);
//void show_metkar_highlights(struct kcontrol *kcont);
//void recursive_set_probe_visibility(Canvas *canvas, SpectraChrome *fc);
//void set_metkar_probe_visibility(struct kcontrol *kcont, SpectraChrome *fc);
//void kcont_updateSelectorPanel(struct kcontrol *kcont);
//void kcont_get_probe_visibility(struct kcontrol *kcont); // Only for multicell - elsewhere use kcont_get_current_probe_visibility()
//void kcont_get_current_probe_visibility(struct kcontrol *kcont);
//void kcont_hideSelectorPanel(struct kcontrol *kcont);
//
//
///****** kinteract.c ******/
//
//void free_keeplists(struct kcontrol *kcont);
//void display_boundary(struct kcontrol *kcont, Canvas *chromcanvas, int onoff);
//void multibox_selector(struct kcontrol *kcont, Canvas * *chromcanvas, Canvas * *classcanvas, Canvas *movingcan, long x, long y);
//void correct_centromere(struct kcontrol *kcont, int onum, int cx, int cy);
//void ActivateKinteract(struct kcontrol *kcont);
//void DeactivateKinteract(struct kcontrol *kcont);
//void remove_invert_flag(struct kcontrol *kcont, int onum);
//void SelectAll(struct kcontrol *kcont);
//void SelectAll(struct kcontrol *kcont);
//void KarLassooSelect(struct kcontrol *kcont);
//void DeselectAll(struct kcontrol *kcont);
//void Undo(struct kcontrol *kcont);
//void DeleteSelected(struct kcontrol *kcont);
//void AutoStraightenSelected(struct kcontrol *kcont);
//void ManStraightenSelected(struct kcontrol *kcont);
//void TrimSelected(struct kcontrol *kcont);
//int GetScaleOfSelected(struct kcontrol *kcont);
//void ScaleSelected(struct kcontrol *kcont, long intscale);
//void EnhanceSelected(struct kcontrol *kcont);
//void ReflectSelected(struct kcontrol *kcont, long mode);
//void ContrastSelected(struct kcontrol *kcont);
//void RestoreSelected(struct kcontrol *kcont);
//void TidyKary(struct kcontrol *kcont);
//void KinteractCallback(UINT event, long x, long y, struct kcontrol *kcont);
//void CopyKar(struct kcontrol *kcont);
//void flip_and_twist(struct kcontrol *kcont, BOOL redraw);
//
///****** kselect.c ******/
//
//void access_metdisplay(struct kcontrol *kcont);
//void access_kardisplay(struct kcontrol *kcont);
//void redraw_kar(struct kcontrol *kcont);
//void redraw_met(struct kcontrol *kcont);
//void redraw_metandkar(struct kcontrol *kcont);
//void kar_display_selectframe(struct kcontrol *kcont, int onum, int state, int expose);
//void met_display_selectframe(struct kcontrol *kcont, int onum, int state, int expose);
//void display_selectframe(struct kcontrol *kcont, int onum, int state, int expose);
//void metanno_select_state(struct kcontrol *kcont, int state, int expose);
//void karanno_select_state(struct kcontrol *kcont, int state, int expose);
//void anno_select_state(struct kcontrol *kcont, int state, int expose);
//void kar_show_selected(struct kcontrol *kcont);
//void met_show_selected(struct kcontrol *kcont);
//void show_selected(struct kcontrol *kcont);
//void kar_hide_selected(struct kcontrol *kcont);
//void met_hide_selected(struct kcontrol *kcont);
//void hide_selected(struct kcontrol *kcont);
//
//
///****** manstraight.c ******/
//
//struct pframe *pre_kardraw(struct kcontrol *kcont, int onum);
//void post_kardraw(struct kcontrol *kcont, struct pframe *f);
//void invert_poly(struct polygondomain *pdom);
//void drawstraightpoly(struct kcontrol *kcont, int onum);
//struct chromosome *manualstraight(struct kcontrol *kcont, int onum);
//
//
///****** manualcount.c ******/
//
//Canvas *make_cross_canvas(struct kcontrol *kcont, int dx, int dy);
//void mc_redraw_image(struct kcontrol *kcont);
//void show_crosses(struct kcontrol *kcont);
//void show_count(int count);
//void ResetManualCount(void *kcont);
//void DeactivateManualCount(void *kcont);
//
//void ManualCountCallback(UINT event, long x, long y, DWORD cont);
//void ActivateManualCount(void *kc, HWND wnd, HWND manwnd, BOOL *Numbering, BOOL *Visible);
//void ShowHideCount (void *kc, BOOL bShow);
//void SetNumberColour(struct kcontrol *kcont,Canvas *canvmain, int CurrentChromClass, LPTSTR class_txt, Canvas *cross_canvas);
//LPTSTR ChromosomeNumbering (struct kcontrol *kcont, int *pChromClass, int *pChromCount, BOOL *pChnangeColour);
//void DeleteChromosomeNumber (struct kcontrol *kcont, Canvas *canvas);
//int GetClassCount(int chrom_number_class);
//int GetClassCountFromLabel(LPTSTR class_lbl);
//int GetClassCountFromCanvas(struct kcontrol *kcont,Canvas *canvas);
//BOOL SexOrMarker (LPTSTR class_txt, Canvas *cross_canvas);
//void IncrementChromClass (struct kcontrol *kcont,Canvas *canvas);
//BOOL StopMarkerEdit (struct kcontrol *kcont,Canvas *markercanvas);
//void StartMarkerEdit (long X, long Y);
//void GetCoordinates (Canvas * canvas1, int X, int Y, int *ddx, int *ddy);
//void DeleteCanvas (struct kcontrol *kcont,Canvas *cptr);
//BOOL AutoScrolling (UINT event);
//void MoveNumber (UINT event, struct kcontrol *kcont, Canvas *cptr, BOOL scroll, int MouseRoot_x, int MouseRoot_y);
//BOOL MarkerAnnotation (UINT event, struct kcontrol *kcont, Canvas **canvas1, int x, int y);
//BOOL DeleteNumber (UINT event, struct kcontrol *kcont, int x,int y);
//BOOL MovingNumber (UINT event, struct kcontrol *kcont, Canvas **canvas1, int x,int y, int *selected_exisiting, int scroll);
//void ManualCount_KcontAdmin(struct kcontrol * kcont);
//
///****** manualsplit.c ******/
//
//struct pframe *pre_metdraw(struct kcontrol *kcont, int onum);
//void post_metdraw(struct kcontrol *kcont, struct pframe *f);
//void drawsplitpoly(struct kcontrol *kcont, int onum, int shape, int closed);
//int draw_npolys(struct kcontrol *kcont, int onum, int shape, int show_bound);
//void draw_fourpts(struct kcontrol *kcont, int onum);
//int single_manover(struct kcontrol *kcont, int onum);
//int axes_split(struct kcontrol *kcont, int onum, int naxes);
//int manualsplit(struct kcontrol *kcont, int onum, int npolys);
//
//void bdrawEH(UINT event, long x, long y, DWORD cont);
//void init_colouroverlayList();
//void clear_colouroverlayList();
//void next_colouroverlay();
//void display_colouroverlayList(Canvas *canvas);
//void display_colouroverlayCanvas(Canvas * canvas, struct object *obj);
//
///****** metcanvas.c ******/
//
//void add_cgh_probe_highlight(struct kcontrol *kcont, int onum);
//void add_mfish_pseudo_highlight(struct kcontrol *kcont, int onum);
//void update_rmfish_met_pseudo(struct kcontrol *kcont);
//void update_mfish_met_pseudo(struct kcontrol *kcont);
//void add_probe_highlight(struct kcontrol *kcont, int onum);
//void attach_probe_luts(struct kcontrol *kcont);
//void add_cgh_ratio_highlight(struct kcontrol *kcont, int onum);
//void create_met_axis_highlight(struct kcontrol *kcont, int onum);
//void create_met_centromere_highlight(struct kcontrol *kcont, int onum);
//int offset_in_metblob(struct kcontrol *kcont, int onum, int dx, int dy);
//void get_met_centromere_from_offset(struct kcontrol *kcont, int onum, int dx, int dy, int *cx, int *cy);
//void add_boundary_highlight(struct kcontrol *kcont, int onum);
//void create_metblob_canvas(struct kcontrol *kcont, int onum);
//void create_metaphase_canvas(struct kcontrol *kcont);
//
///****** metcreate.c ******/
//
//void get_chromwidth(struct kcontrol *kcont);
//void read_Cytoscan_met(struct kcontrol *kcont, FILE *f);
//struct kcontrol *load_Cytoscan_metaphase(DDGS *metdg, LPCTSTR filename, int filegroupid, int imageid);
//struct kcontrol *load_psi_metaphase(DDGS *metdg, LPCTSTR filename, int filegroupid, int imageid);
//struct kcontrol *load_sat_metaphase(DDGS *metdg, LPCTSTR filename, int filegroupid, int imageid, int fluorescent);
//struct kcontrol *create_metaphase(struct chromosome * *objlist, short fluorescent, DDGS *metdg, int filegroupid, int imageid);
//struct kcontrol *load_metaphase(struct kcontrol *oldkcont, DDGS *metdg, LPCTSTR filename, int filegroupid, int imageid);
//int save_metaphase(struct kcontrol *kcont, LPCTSTR filename);
struct kcontrol *load_karyogram(struct kcontrol *oldkcont, DDGS *kardg, LPCTSTR filename, int filegroupid, int imageid);
//int save_karyogram(struct kcontrol *kcont, LPCTSTR filename);
//
//int boundary_colorize(struct kcontrol *kcont, int onum, int col);
////void colorize_met_boundaries(struct kcontrol *kcont);
//void colorize_met_boundaries(struct kcontrol *kcont, int particularcolour);
//
//
///****** metmod.c ******/
//
//void remove_metblobcanvas(int onum, struct kcontrol *kcont);
//void delete_metblob(int onum, struct kcontrol *kcont);
//void move_metblob(struct kcontrol *kcont, int onum, int x, int y);
//void enhance_metblob(struct kcontrol *kcont, int onum);
//void contrast_metblob(struct kcontrol *kcont);
//void contrast_stretch_metblob(struct kcontrol *kcont);
//void new_split_canvases(struct kcontrol *kcont, int newobjs);
//void split_kernel(struct kcontrol *kcont, int onum, int TYPE, int AUTO, int showBndsOnly);
//void autosplit_metblob(struct kcontrol *kcont, int onum, short TYPE, int showBndsOnly);
//void manualsplit_metblob(struct kcontrol *kcont, int onum, int npolys);
//void single_manover_metblob(struct kcontrol *kcont, int onum);
//void multi_manover_metblob(struct kcontrol *kcont, int onum, int naxes);
//void join_metblob(struct kcontrol *kcont);
//void correct_centromere_metblob(struct kcontrol *kcont, int onum, int cx, int cy);
//void correct_axis_metblob(struct kcontrol *kcont, int onum);
//void restore_metblob(struct kcontrol *kcont, int onum);
//void scrap_prob_noise(struct kcontrol *kcont);
//void retype_metblob(struct kcontrol *kcont, int onum, int newtype);
//
//
///****** metpaste.c ******/
//
//void fuse_metblob(struct kcontrol *kcont, int buffindex);
//void PasteMet(struct kcontrol *kcont);
//
//
///****** metundo.c ******/
//
//void metkeep(int onum, struct kcontrol *kcont);
//void free_metkeep(struct kcontrol *kcont);
//void cgh_remove_split_join(struct kcontrol *kcont, int onum);
//void mfish_remove_split_join(struct kcontrol *kcont, int onum);
//void barcode_remove_split_join(struct kcontrol *kcont, int onum);
//int remove_splitmetblob(struct kcontrol *kcont);
//int remove_joinedmetblob(struct kcontrol *kcont);
//void metundo(struct kcontrol *kcont);
//void destroy_zombies(struct kcontrol *kcont);
//
//
///****** minteract.c ******/
//void zoom_met(struct kcontrol *kcont, int X, int Y);
//void remove_temporary_highlights(struct kcontrol *kcont);
//void ActivateMinteract(struct kcontrol *kcont);
//void DeactivateMinteract(struct kcontrol *kcont);
//void RetypeSelected(struct kcontrol *kcont, int newtype);
//void ManualClass(int onum, int chromclass, struct kcontrol *kcont);
//int CountChromes (struct kcontrol *kcont);
//void UpdateCount(struct kcontrol *kcont);
//void ActivateMeastimeCallback(struct kcontrol *kcont);
//void DeactivateMeastimeCallback(struct kcontrol *kcont);
//void fixfuse(struct kcontrol *kcont);
//
//#define CLASSIFY_FEATURES	0
//#define CLASSIFY_LENGTH		1
//void Create_Karyogram(      struct kcontrol *kcont, int classify_mode);
//void Create_Empty_Karyogram(struct kcontrol *kcont);
//void CollectAbnormals(      struct kcontrol *kcont);
//
//void MinteractCallback(UINT event, long x, long y, struct kcontrol *kcont);
//void MetLassooSelect(struct kcontrol *kcont);
//void AutosplitSelected(struct kcontrol *kcont);
//void AutodisoverSelected(struct kcontrol *kcont);
//void ManualdisoverSelected(struct kcontrol *kcont);
//void ManaxesOverlap(struct kcontrol *kcont);
//void ManualsplitSelected(struct kcontrol *kcont);
//void JoinSelected(struct kcontrol *kcont);
//void MakeSelectedChromosome(struct kcontrol *kcont);
//void MakeSelectedNoise(struct kcontrol *kcont);
//void MakeSelectedComposite(struct kcontrol *kcont);
//void MakeSelectedOverlap(struct kcontrol *kcont);
//void MakeSelectedNucleus(struct kcontrol *kcont);
//void Colourise_Metaphase(struct kcontrol *kcont);
//void CALLBACK MeastimeCallback(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);
//void SaveMetCanvas(struct kcontrol *kcont);
//void LoadMetCanvas(struct kcontrol *kcont);
//void CopyMet(struct kcontrol *kcont);
//BOOL classify_but_no_karyotype(struct kcontrol *kcont);
//
///****** psi.c ******/
///*
//These are in psi.h:
//struct psidata *psiopen(char *name);
//int psiread(struct psidata *psi, unsigned char *image);
//int psiclose(struct psidata *psi);
//*/
//int psidgread(struct psidata *psi, DDGS *dg);
//int psidgread(struct psidata *psi, DDGS *dg);
//
///****** trimstuff.c ******/
//
//int draw_nareas(struct kcontrol *kcont, int onum, int shape);
//struct chromosome *trim(struct kcontrol *kcont, int onum, int npolys);
//
///****** klabel.cpp ******/
//void newMarkerTag(struct kcontrol *kcont, Canvas *tagcanvas, LPCTSTR tagname);
//void startTagEdit(long x, long y);
//BOOL stopTagEdit(struct kcontrol *kcont, Canvas *tagcanvas);
//
///******* metanno.cpp ******/
//Canvas *create_met_annotation(struct kcontrol *kcont, int onum, int metclass, short labeltype, short position);
//void calc_centromere_pos(struct kcontrol *kcont, int onum, Canvas *METcanvas);
//void get_centromere_pos(struct kcontrol *kcont, int onum, int *centX, int *centY);
//void AddMetAnnotation(struct kcontrol *kcont, int onum, short labeltype);
//void RemoveMetAnnotation(struct kcontrol *kcont, int onum);
//void relabel_visible_met_annotation(struct kcontrol *kcont, int onum, int metclass);
//void reset_met_annotation_after_undo(struct kcontrol *kcont, int onum);
//void display_MetAnno_canvas(struct kcontrol *kcont, Canvas *METcanvas, int display, BOOL expose);
//void expose_Metblob_canvas(struct kcontrol *kcont, Canvas *METcanvas);
//void destroy_MetAnno_canvas(Canvas *METcanvas);
//void delete_temporary_MetAnno_canvas(struct kcontrol *kcont);
//void move_metanno(struct kcontrol *kcont, int onum, int newdx, int newdy);
//
///******* trim eraser ******/
//
//
//void TrimEraseSelected(struct kcontrol *kcont);
//void TrimEraserCallback (UINT event, long x, long y, DWORD cont);
//void TrimErase (struct kcontrol *kcont,long x, long y, int onum);
//void TrimEraseChrom (struct kcontrol *kcont, int onum);
//void TrimUnerase (struct kcontrol *kcont,long x, long y, int onum);
//void TrimEraser (struct kcontrol *kcont,long x, long y, int onum);
//void TrimEraseDisplay (struct kcontrol *kcont);
//void RecursiveModifyOrigin (int *X,int *Y, Canvas *cptr);
//int CanvasHitTest (Canvas *cptr,long fx, long fy);
//int CanvasHitTestAll (Canvas *cptr,long fx, long fy);
//struct chromosome *TrimChrom(struct kcontrol *kcont, int onum, int npolys);


#define CENTRMPOS 0
#define REALPOS 1
#define UNDOPOS 2

#ifdef __cplusplus
}
#endif

//#endif

