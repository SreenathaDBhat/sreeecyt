
/*
	rawtypes.h	Mark Gregson	19/9/94

	Raw image types - constants stored in raw image file TIFF headers
	as part of the image description. See rawIO.c in libcvadmin.a

	
*/

/* Modifications:
	9Jun95	MG	Added satcap rawtypes and extended rawplist to
 			store satcap case slide cell info... this could
			be used later for all raw images. Also modified
			rawIO.c in libcvadmin to andle these new rawtypes.
*/


#ifndef RAWTYPES_H

#define RAW_PROBE 				0
#define RAW_COUNTERSTAIN			1
#define RAW_CGH_TEST				2
#define RAW_CGH_REFERENCE			3
#define RAW_BRIGHTFIELD				4
#define RAW_FLUORESCENT				5
#define RAW_SATCAP_BRIGHTFIELD			6
#define RAW_SATCAP_BRIGHTFIELD_THRESHOLDED	7
#define RAW_SATCAP_FLUORESCENT			8
#define RAW_SATCAP_FLUORESCENT_THRESHOLDED	9


struct rawplist {
	SMALL size;		/* of this structure - ESSENTIAL for I/O */
	short rawtype;		/* one of the above types */
	short probeid;		/* fluorochrome id if rawtype = 0, 1, 2 or 3 */
	TCHAR name[64];		/* fluorochrome name or "Raw Brightfield" */
				/*		     or "Raw Fluorescent" */

	TCHAR casename[20];	/* These are for satcap only - at present */
	TCHAR slidename[20];
	TCHAR cellname[20];
	int fusenum;		/* 0 if main metaphase */
};

/* example raw image descriptions :	Raw FITC WHEEL 762 2 		*/
/*			      		Raw Brightfield 0 4 		*/

/* SATCAP :	Raw Brightfield casename slidename cellname 0 0 6 	*/
/*							    ^	  	*/
/*							    Fusenum 	*/
/*							    0 = met	*/
/*							    1 = fuse 1	*/
/*							    2 = fuse 2  */
/*							    etc.	*/


#define RAWTYPES_H
#endif /* RAWTYPES_H */
