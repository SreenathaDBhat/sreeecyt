    
    /*
     * redkar.h	26/8/92		Mark Gregson
     *
     * Structures and routines for control of karyotyping on RED
     *******************************************************************
     *
     * Modifications :
     *
     *	11Dec02     MC	    Moved Cblobdata propertylist dec. from redkar.h - needed in Xcanvas.cpp [canvas]
     *					and DrawingArea.cpp [CVcontrol] for access to new 'permanent' variable used im met. annotation
     *					Don't want to put redkar.h in these files.
     *
	 *	13 Apr 1999	MG	Removed old i386 stuff, added new flexible karyotyper fields
	 *					and incremented met and kar file versions to 2
     *	16 Jul 1997	MG	Made unified assoc_type to replace cghflag and mfishflag
     *				and assoc_ptr to point to the assoicated cgh
     *				or mfish structure. See constants below for assoc_type
     *	
     *      26Mar97 BP:             Replace class with chromclass (prep for C++ version).
     *	17 May 1996	MG	Added mfish flag and stucture pointer
     *	17 Feb 1995	MG	Moved classifier ids to chromanal.h
     *	10 Oct 1993	MG	Removed unused bol[] list
     *	25 Sep 1994 	MG	Magic numbers and new kcontrol structure 
     *	 1 Apr 1993	MG	added colourised and modified flags
     *	29 Mar 1993	MG	added filegroupid and imageids
     *	 4 Mar 1993	MG	added drawmode to handle fluorescent images
     *	25 Feb 1993	MG	classifier identifiers
     *	16 Sep 1992	MG	create_metaphase_canvas
     */
    
    #ifndef REDKAR_H
    
    #include <ddgs.h>
    #include <wstruct.h>
    #include <chromanal.h>
    #include <canvas.h>
 //   #include <common.h>
    
    struct enhancement {
    	int white;					/* percentage white for htnormalise */
    	int black;					/* percentage black for htnormalise */
    	struct convolution *conv;
    	int matrix[25];
    };
    
    typedef struct count_cross_type{
    	short x;
    	short y;
    }crosspt;

  
    /* file constants */
    #define METMAGIC	0xfadedace	/* metaphase magic number */
    #define KARMAGIC	0xacefaded	/* karyotype magic number */
    
	// New met and kar versions for flexible karyotyper   
    #define METVERSION	3			/* met file version number */
    #define KARVERSION	3			/* kar file version number */
    
    /* constants used to define associated structures - CGH, MFISH etc */
    #define ASSOC_NONE		0
    #define ASSOC_CGH		1
    #define ASSOC_MFISH		2
    #define ASSOC_BARCODE	3
    #define ASSOC_PKAR		4

    #define UNDEFINED_LAYOUT_ROW        -1

    // constants used for the action of chromosome numbering
	#define CHROMOSOME_NUMBERING_OFF	-2
	#define CHROMOSOME_NUMBERING_INACTIVE -1

    // Keep track of karyotype layout
    typedef enum
    {
        DefaultLayout = 0,
        UnstackLayout = 1,
        AutoLayout = 2
    } KaryoLayout;

    struct kcontrol {
    	TCHAR	FIP;				/* FIP ,TV,  FLUOR digitisation ? */
    	SMALL 	maxnumber;			/* current maximum number of objects */
    	SMALL	measnumber;			/* current object to measure */
    
    /* ------ general purpose display parameters and frames ------ */
    	int 	current_obj;		/* used in mouse interaction */
    	SMALL 	identify_set;		/* Which set of objects to be identified */
    	struct 	seg_interact *SI;	/* segmentation control structure */
    	int		chromwidth;			/* chromosome width for overlap axes */
        KaryoLayout Layout;         // Current layout
        BOOL    Autoscale;

    /*
     *--------  Classifier   --------
     */
    	SMALL 	classifier;			/* classifier SHORT_CLASS, LONG_CLASS, etc */
    
    /*
     *------ particular lists for karyotyping ------
     */
    	struct chromosome **ool;	/* original objects pointer */
    	struct chromosome **eool;	/* enhanced original objects pointer */
    	struct chromosome **mol;	/* measured/rotated/classified objects pointer */
    	struct chromosome **emol;	/* enhanced classified objects pointer */
    	struct object	  **bndool;	/* original object boundary list */
    	struct object	  **bndmol;	/* measured/rotated object boundary list */
    	SMALL 		*acl;			/* feature measurement activity list */
    	SMALL		*selected;		/* currently selected objects */

    	struct chromosome **keepeool;	 /* old eool object pointers for undo */
    	struct object	  **keepbndool;	 /* old bndool boundary pointers for undo */
    	struct chromosome **keepemol;	 /* old emol object pointers for undo */
//TRYIT
    	struct chromosome **keepmol;	 /* old mol object pointers for undo - introduced CV3.6 for reflected, trimmed or straightened chroms */
//TRYIT
    	struct object	  **keepbndmol;	 /* old bndmol boundary pointers for undo */
    	struct chromplist **keepsharedplist;/* old sharedplist contents for undo */
    
    	SMALL		  *keepclassmax;/* old class canvas maxchroms for undo */

    /*
     *----- pointers to metaphase and karyogram canvases -------
     */
    	Canvas *met_canvas;
    	Canvas *kar_canvas;
    	Canvas *metanno_canvas;
    	Canvas *karanno_canvas;
    
    /*
     * Short cut lists for quick access to individual class and chromomsome canvases
     */
    	Canvas **classptr;
    	Canvas **kar_chromptr;
    	Canvas **met_chromptr;
    
    /* 
     * Data structures for box selector event handlers 
     */
    	Cselectdata	metbox, karbox;
    
    /*
     *------ pointers to associated structures ------
     */
    	DDGS	*kardg;			/* ddgs structure for karyogram */
    	DDGS	*metdg;			/* ddgs structure for metaphase */
    
    	SMALL 	karactive;		/* activity flags used by kcont_admin */
    	SMALL 	metactive;
    
    /*
     *	filegroup and image identifiers - from casbase - used for copy & paste
     */
    	int filegroupid;		
    	int met_imageid;
    	int kar_imageid;
    
    /*
     *------ canvas display zoom factor -----------------------------
     */
    	int 	zoom_factor;		/* ddgs scale i.e  16 => x2 */
    /* 
     *------ canvas drawmode - COPY for brightfield INVERT for fluorescent -------
     */
    
    	SMALL 	drawmode;
    
    /*
     *------- colourised metaphase flag ------------------------------
    */
    	SMALL 	colourised;
    
    /*
     *-------- modification flag - check if met/kar have been updated ------
     */
    	SMALL	modified;
    
    /*
     *----- timer measurements ---------------------------------------
     */
    	UINT meastime;			/* time out id for MeasCallback - object measurement */
    
    /*
     *----- manual count info ----------------------------------------------
     */
    	int manual_count;		/* object count */
    	crosspt *count_cross;	/* cross position of counted object */

					//chromosome numbering
		short current_chrom_number_class;
    /*
     *----- align on centromere if this flag set ----------------------------
     */
    	SMALL	centalign;		/* align on centromere if set */
    
    
    /*
     *------ CGH / MFISH / BARCODE flag and pointer to structure ------------
     */
    	int 	assoc_type;		/* ASSOC_NONE, ASSOC_CGH, ASSOC_MFISH .. */
    	void	*assoc_ptr;		/* NULL      , *cgh     , *mfish         */
    
	// New fields for flexible karyotyper - METVERSION=2, KARVERSION=2
		int NumOfClasses;		// current number of classes allocated
		int MaxObjs;			// Current number of objects allocated 
		void *TemplatePtr;		// Species template pointer

		float temp_mfish_gamma;	// NOTE: this doesn't get written to the file, just a temp placement holder for livetime of met.kar
   };
    
    
    /*
     * per-object activity bits 
     */
    #define ACTIVE		1	/* still of interest */
    #define DEAD		2	/* zombie waiting to be destroyed */
    
    
    /*********** RED Karyotyping Canvas stuff ***********************/
    
    /*
     * CANVAS data property list for Cchromclass type
     */
    typedef struct Cclassdata_type{
    	SMALL	size;		/* size of this structure in bytes */
    	short	chromclass;		/* chromsome class */
    	short	nchroms;	/* current no of chromosomes in class canvas */
    	short	maxchroms;	/* no of chromosomes allowed in class canvas */
    	TCHAR	tag[10];	/* tag field for class */
    } Cclassdata;
    
    /*
     * CANVAS data property list for Cchromosome type
     */
    typedef struct Cchromdata_type{
    	SMALL	size;		/* size of this structure in bytes */
    	short	chromclass;		/* chromsome class */
    	short	posinclass;	/* position in class canvas */
    	short	onum;		/* index to object list */
    	TCHAR	tag[10];	/* tag field for class */
    } Cchromdata;
    
#if 0
    /*
     * CANVAS data property list for Cmetblob type
     */
    typedef struct Cblobdata_type{
    	SMALL	size;		/* size of this structure in bytes */
    	short	onum;		/* index to object list */
    	char	tag[10];	/* tag field */
    } Cblobdata;
#endif // see 11Dec comment MC

    /*
     * CANVAS data property list for Cstack type
     */
    typedef struct Cstackdata_type{
    	SMALL	size;		/* size of this structure in bytes */
    	short	nchroms;	/* current no of chromosomes in stack canvas */
    } Cstackdata;
    
    
    /*
     * CANVAS data property list for annotation
     */
    typedef struct Cannodata_type{
    	SMALL	size;		/* size of this structure in bytes */
    	int	selected;	/* canvas selected or not */
    	int	state;		/* -1=delete if freekeep, 0=OK, 1=delete if undo */
    	double 	scale;		/* current scale factor */
    } Cannodata;
    
    /************************ ROUTINES in redkar.lib *******************************/
    
    
    #define REDKAR_H
    #endif  /* REDKAR_H */
