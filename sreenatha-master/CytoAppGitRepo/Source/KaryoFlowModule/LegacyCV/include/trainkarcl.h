/* 
** Classifier trainer (trainkarcl.h)
**
** This is the only header needed for external code using the traincl library.
**
** Mod's
**
**	22Mar00 SN	Removed CP_COPYPATH and CP_BACKUPPATH as get_classifier_path()
**				no longer uses them.
**	21Oct99	MG	Changed CGHDAPI_CLASSNAME from "CGH-Dapi" to "Inverted DAPI"
**	17Sep99	SN	New #defines for Data and Class file extensions, CLUSERDATA and CLUSERCLASS.
**	09Sep99	JMB	Moved prototype for get_classifier_path and its associated
**				defines here from traincl/cllist.h.
**	21Jun99	MG	Now pass kcont thru to generate_classifier() for access to attached 
**				species template
**	06May99	SN	New file clfile.cpp.
**				Moved mahalanobis.c to traincl.
**	26Mar99	SN	Removed training_select_file as unused anywhere.
**	25Mar99	SN	Removed cytovcv.c, vcvstatsel.c and vcvtrain.c function prototypes
**				to their own header residing in traincl directory. Pointless keeping
**				them here as they arent used outside traincl.lib.
**	07Apr98	WH:	Add Prototypes for C++. Moved machine_ident to traincl.c
*/

#include <stdio.h>
//#include <common.h>

// Ignore warnings on _wfopen and fwscanf like "This function or variable may be unsafe. Consider using _wfopen_s instead."
#pragma warning(disable : 4996)

#ifndef TRAINKARCL_H
#define TRAINKARCL_H


//#ifdef __cplusplus
//extern "C" {
//#endif

// Used for classifier Data and Class file headers
typedef struct t_CLheader {
	int nclass;									// Number of classes
	int nselected_feature;						// Number of selected features in Class file
	int nfeature;								// Number of features in Data file
	int data_version;							// Data file version
	int class_version;							// Class file version
	TCHAR szspecies[CLSPECIESNAMELENGTH + 1];	// Species name
} CLheader;


typedef enum e_CLloadclass_type {
	LOADCLASS_ERROR, LOADCLASS_OK, LOADCLASS_SHORT, LOADCLASS_BARCODE_DEF, LOADCLASS_CGHDAPI
} CLLoadClass;


// Type of features trained
typedef enum e_CLfeature_type {											//SN30Mar00
	FEATURE_STANDARD, FEATURE_BARCODE, FEATURE_CGHSTDREF
} CLFeature;


/****** cllist.cpp ******/

// To indicate no classifier name
#define NO_CLASSNAME				_T("")

// Default classifier names, one per default classifier id, see chromanal.h
#define SHORT_CLASSNAME				_T("G-band short")
#define LONG_CLASSNAME				_T("G-band long")
#define RBAND_CLASSNAME				_T("R-band")
#define CGHDAPI_CLASSNAME			_T("Inverted DAPI")
#define BARCODE_DEF_CLASSNAME		_T("RxFISH default")
#define CGH_DEF_STDREFNAME			_T("CGH Standard Ref default")
#define CGH_MALE_STDREFNAME			_T("CGH Standard Ref Male")
#define CGH_FEMALE_STDREFNAME		_T("CGH Standard Ref Female")

// Classifier data and class file extensions
#define CLUSERDATA				_T("dat")
#define CLUSERCLASS				_T("cla")

// CGH Ratio profiles (data) and Standard references (class) file extensions				//SN10Apr00
#define CLCGHRATPROF			_T("pro")			// MUST be same length as CLUSERDATA string
#define CLCGHSTDREF				_T("ref")			// MUST be same length as CLUSERCLASS string

// Parameters for routine get_classifier_path and get_new_classifier_path
#define CP_DATAFILE				0
#define CP_CLASSFILE			1

//#define CP_OLD					0
//#define CP_NEW					1

#define CP_ACTUALPATH			0
//#define CP_COPYPATH			1
//#define CP_BACKUPPATH			2
#define CP_TEMPPATH				3


void get_classifier_path(		LPTSTR szclasspath, int ffiletype, int fpathtype, 
                              LPCTSTR szclassname, LPCTSTR szspecies);
void get_new_classifier_path( LPTSTR szclasspath, int ffiletype, 
							  LPCTSTR szclassname, LPCTSTR szspecies,
							  CLFeature clfeature);
	
void    init_usr_class_list();
void destroy_usr_class_list();
void  update_usr_class_list();

BOOL update_cl_list(LPCTSTR szclasspath);

TCHAR * find_filename(int classid);
CLFeature find_classfeature(LPCTSTR filename, LPCTSTR species);

int  is_default_human( void *TemplatePtr, int NumOfClasses);
int  is_classid_consistent_with_image( int classid, int isbarcodedata, void *TemplatePtr);
BOOL is_default_classifier(LPCTSTR name);


/****** clfile.cpp ******/
	
int  cl_read_data_header(   FILE * fp, int * pclassid, CLheader * pheader);


/***** classboxCB.cpp ******/

CLLoadClass load_classifier(struct kcontrol *kcont, int * pclassid);

void add_user_classifiers(LPCTSTR species, HWND hWnd);

// Note add_classifiers is overloaded, the prototype for the original version
// is in traincl\classboxCB.h (only used internally to traincl).
void add_classifiers(/*(CTemplateKaryotype *)*/void *pkarytemplate, HWND hWnd);

int  add_CGHStdRefs( /*(CTemplateKaryotype *)*/void *pcghtemplate, LPTSTR szCGHStdref, HWND hWnd);
void add_CGHStdRefs( /*(CTemplateKaryotype *)*/void *pkarytemplate, HWND hWnd);
/****** mahalanobis.cpp ******/

int findclass(double *fvec, double *priorp, struct cllik *liklv, int nclasses, int nfeatures);
FILE * open_classfile(LPCTSTR classfile, int *pnclasses, int *pnfeatures);
void read_c_params( FILE *c, int nclasses, int *sfeatvec, int nfeatures, double *lpp);
void set_def_params(register int *nclasses, register int *sfeatvec, register int *nfeatures, double *lpp);


//#ifdef __cplusplus
//}
//#endif



#endif /* TRAINKARCL_H */
