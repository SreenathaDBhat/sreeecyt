/*
 * woolz.h
 *
 * Created on the 2nd day of February in the year 2000.
 * Defines the external interface to the new woolz DLL.
 */

#pragma warning(disable:4945)

#ifndef WOOLZ_H
#define WOOLZ_H

#include <tchar.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

/****************************************
 * Begin visual studio generated code ...
 */

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the WOOLZ_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// WOOLZ_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef WOOLZ_EXPORTS
#define WOOLZ_API 
#else
#define WOOLZ_API
#endif

#ifdef __cplusplus
// This class is exported from the woolz.dll
class WOOLZ_API CWoolz {
public:
	CWoolz(void);
	// TODO: add your methods here.
};
#endif

extern WOOLZ_API int nWoolz;

WOOLZ_API int fnWoolz(void);

/*
 * ... end visual studio generated code.
 ***************************************/



/*************************************************************
 * This section is from machdef.h (which used to be #included)
 */

typedef unsigned char	GREY;		/* pixel values */
typedef short	WZCOORD;		/* integer coordinate */
typedef short	SMALL;		/* small integer value */
typedef unsigned char UBYTE;
#define uiNR_OF_GREY (256)

/*
 * End of section from machdef.h
 *******************************/





/***********************************
 * This section used to be wstruct.h
 */

/*
 * structure definitions for object structures for woolz system
 *
 * Load system dependant definitions
 *
 * Modifications
 *
 *	BP	19Dec96:	Added more structure aliases.
 *	BP	9/19/96:	Remove Polygon and Circle definitions.
 *					Include basic.h for formal declarations with WIN32.
 *
 *	BP	7/17/95:	pframe dx and dy now int, to avoid overflow. Also
 *					some nice typedefs.
 *	27 Aug 1992		MG	Added missing pgrid structure
 *	 2 Jul 1992		MG 	Added mrcwoolz convolution structure - much nicer,mmmm
 *	23 Oct 1990	jimp@MRC	Added compound objects (from MRC Woolz)
 *	 7 Oct 1986		CAS				Moved wmachdef to /dd/defs/machdef.h
 *	 6 Oct 1986		CAS (GJP)		Added a link count to some of the
 *									more 'shared' structures, to enable
 *									us to only free them when everyone
 *									has finished using them. (Done under
 *									protest from GJP). (Sorry Jim)
 */


//#include <machdef.h>
//#include "machdef.h"


/* main object structure.  By use of pointers to sub-structures, these
   do not necessarily need to be of the type specified, but can be re-typed
   to be any pointer-to-structure type. */

struct object {
	SMALL type;	/* object type : in the following ranges :
				1-9 - interval/grey table objects :
					 1 - "conventional" object
				10-19 - polygons, boundaries, etc. :
					10 - polygon
					11 - boundlist - tree of polygon domains
					12 - convex hull
					13 - histogram or profile
				20-29 - rectangles
					20 - rectangle specified by vertices
				30-39 - lines :
					30 - vector (integer vertices)
					31 - vector (float vertices)
				40-49 - points :
					40 - point (integer vertices)
					41 - point (float vertices)
				50-59 - convolution :
					50 - int space-domain convolution
					51 - float space-domain convolution
				60-69 - miscellaneous
					60 - display frame
				70-79 - text and labels
					70 - text
				80-89 - compound objects
					80 - compound array (all elements same type)
					81 - compound array (elements may be different)
					82 - compound linked list (same types)
					83 - compound linked list (different types)
				90    - raw image
				100   - circle
			*/
	struct intervaldomain *idom;	/* interval domain */
	struct valuetable *vdom;	/* value domain */
	struct propertylist *plist;	/* property domain */
	struct object *assoc;		/* associated object */
};

struct rawimage{
	SMALL type;	/* 90 */
	int width;
	int height;
	int bits;	/* 8,16,24 */
	char *values;
};


/* standard interval list structure for pixel-objects */

struct intervaldomain {
	SMALL type;			/* type :
					  1 - conventional with interval list
					  2 - rectangular (NULL interval list)
					*/
	int		linkcount;		/* Number of times data is used */
	char *freeptr;			/* If != NULL, can be used to free intervals table */
	WZCOORD line1;			/* first line coordinate */
	WZCOORD lastln;			/* last    "     "	 */
	WZCOORD kol1;			/* first column  "	 */
	WZCOORD lastkl;			/* last    "     "	 */
	struct intervalline *intvlines;	/* array of intervals in a line structures
					   note "intervals in a line" must be contiguous*/
};

/* structure  of intervals in a line */

struct intervalline {
	SMALL nintvs;			/* number of intervals in this line */
	struct interval *intvs;		/* array of intervals (must be contiguous)*/
};

/* interval structure */

struct interval {
	WZCOORD ileft;	/* offset from domain->kol1 of interval left end */
	WZCOORD iright;	/* offset from domain->kol1 of interval right end */
};

/* standard value table for pixel objects */

struct valuetable {
	SMALL type;			/* type :
						1 : GREY pixel values
						2 : float values (not implemented)
					*/
	int		linkcount;		/* Number of times data used */
	char *freeptr;			/* If != NULL, can be used to free grey table */
	WZCOORD line1;			/* first line */
	WZCOORD lastln;			/* last line */
	GREY bckgrnd;			/* background value for points not in object */
	struct valueline **vtblines;	/* array of pointers to structures of grey table lines */
	struct object *original;	/* pointer to originating object */
};

/* structure of grey values in a line */
struct valueline {
	WZCOORD vkol1;			/* left end */
	WZCOORD vlastkl;			/* right end */
	GREY *values;			/* array of values */
};


/* rectangular values table structure */
struct rectvaltb {
	SMALL type;	/* type
				11 : GREY pixel values
				12 : float pixel values (not implemented)
			*/
	int		linkcount;		/* Number of times data used */
	char *freeptr;
	WZCOORD line1;
	WZCOORD lastln;
	WZCOORD kol1;
	WZCOORD width;	/* more use than lastkl */
	GREY bckgrnd;
	GREY *values;	/* contiguous array of values */
	struct object *original;
};

/* interval-structured value table for pixel objects */

struct ivtable {
	SMALL type;			/* type :
						21 : GREY pixel values
						22 : float values (not implemented)
					*/
	int		linkcount;		/* Number of times data used */
	char *freeptr;			/* If != NULL, can be used to free grey table */
	WZCOORD line1;			/* first line */
	WZCOORD lastln;			/* last line */
	GREY bckgrnd;			/* background value for points not in object */
	struct valintline *vil;		/* pointers to structures of grey table lines */
	struct object *original;	/* pointer to originating object */
};

/* structure for one line's worth of grey-intervals */

struct valintline {
	SMALL nintvs;			/* number of grey-intervals */
	struct valueline *vtbint;	/* pointer to grey intervals */
};


/* polygon domain structure */

struct polygondomain {
	SMALL type;			/*
					1 for int vertices
					2 for float vertices
					*/
	SMALL nvertices;
	SMALL maxvertices;
	struct ivertex *vtx;
};

struct ivertex {
	WZCOORD   vtY;
	WZCOORD   vtX;
};

struct fvertex {
	float vtY;
	float vtX;
};

/* boundary list structure - domain of boundary object type 11 */

struct boundlist {
	SMALL type;			/*	0 - piece
						1 - hole */
	struct boundlist *up;		/* the containing hole or piece, NULL
				   	if the universal hole (very top) */
	struct boundlist *next;		/* next hole or piece at same level and
				   	lying within same piece or hole, NULL
				   	if no more at this level */
	struct boundlist *down;		/* first enclosed structure, NULL if none */
	SMALL wrap;			/* wrap number - number of points of
				   	boundary included both at start and end
				   	of polygon representation */
	struct polygondomain *poly;	/* polygon representation of boundary */
};

/*
 * convex hull parameters structure (the vertices as such are stored
 * in a conventional polygon domain).  This is placed in the "vdom"
 * position in an object type 12.
 */

struct cvhdom {
	SMALL type;
	SMALL nchords;	/* number of chords */
	SMALL nsigchords;	/* number of significant chords */
	WZCOORD mdlin;	/* mid-line of enclosed originating object */
	WZCOORD mdkol;	/* mid-column of enclosed originating object */
	struct chord *ch;
};

struct chord {
	SMALL sig;	/* non-zero if judged to be significant */
	int acon;	/* chord equation is acon*x - bcon*y = ccon */
	int bcon;	/* in object-central coordinates */
	int ccon;
	int cl;		/* chord length, *8 */
	WZCOORD bl;	/* line number of bay bottom or bulge top */
	WZCOORD bk;	/* column number of bay bottom or bulge top */
	int barea;	/* bay or bulge area, *8 */
	int bd;		/* bay maximum depth or bulge max height, *8 */
};


/* profile or histogram.  Here only one dimension is specified; the
   other is assumed to have unit increment.  An origin is included
   mostly for display purposes */

struct histogramdomain {
	SMALL type;	/* 1 for integer values, 2 for floats */
	SMALL r;	/* 0 - display with vertical base,
			   1 - display with horizontal base */
	WZCOORD k;	/* column origin (for display) */
	WZCOORD l;	/* line origin */
	SMALL npoints;	/* length */
	int *hv;	/* pointer to array of values - floats if type 2 */
};


/* rectangle domain structures */

struct irect {
	SMALL type;		/* 1 - integer vertices */
	WZCOORD irk[4];		/* column vertex coordinates */
	WZCOORD irl[4];		/* line vertex coordinates */
				/* side from (l[0],k[0]) to (l[1],k[1])
				   is a long side.  Vertices are cyclic */
	float rangle;		/* angle of long side to vertical (radians) */
};

struct frect {
	SMALL type;		/* 2 - real vertices */
	float frk[4];		/* column vertex coordinates */
	float frl[4];		/* line vertex coordinates */
				/* side from (l[0],k[0]) to (l[1],k[1])
				   is a long side.  Vertices are cyclic */
	float rangle;		/* angle of long side to vertical (radians) */
};



/*
 * vector structure.  Type 30 and 31 objects.
 */

struct ivector {
	SMALL type;		/* 30 */
	SMALL style;
	WZCOORD k1;
	WZCOORD l1;
	WZCOORD k2;
	WZCOORD l2;
};

struct fvector {
	SMALL type;		/* 31 */
	SMALL style;
	float k1;
	float l1;
	float k2;
	float l2;
};


/*
 * points (for cursors, etc).    Objects type 40 and 41.
 */

struct ipoint {
	SMALL type;		/* 40 */
	SMALL style;
	WZCOORD k;
	WZCOORD l;
};

struct fpoint {
	SMALL type;		/* 41 */
	SMALL style;
	float k;
	float l;
};


/*
 * convolution mask structure.  Main structure for a type 50 object.
 */
struct convolution {
	SMALL type;	/* 50 integer valued
			   51 real valued */
	SMALL linkcount;/* number of times data used */
	SMALL xsize, ysize; /* size of mask (must be odd) */
	int *cv;	/* size*size convolution mask elements
			   To reduce computational cost at the expense of data
			   storage, complete convolution must be specified even
			   if highly symmetrical */
	int divscale;   /* Divide by this after forming the convolution */
	int offset;     /* ... then add this offset */
	SMALL	modflag;/* ... and take the modulus if this is non-zero. */
};


/* standard workspace structure for interval objects */

struct iwspace {
	struct object *objaddr;		/* the current object */
	SMALL dmntype;			/* domain type */
	SMALL lineraster;		/* line scan direction as follows :
					  1 increasing rows
					  -1 decreasing rows */
	SMALL colraster;		/* column scan direction as follows :
					  1 increasing columns
					  -1 decreasing columns */
	struct intervaldomain *intdmn;	/* pointer to interval structure */
	struct intervalline *intvln;	/* pointer to current line of intervals */
	struct interval	*intpos;	/* pointer to current interval -
						in the case of domain type = 2
						(rectangle) this is set up to
						point to the column bounds in
						the intervaldomain structure */
	WZCOORD colpos;			/* column position */
	SMALL colrmn;			/* columns remaining */
	WZCOORD linbot;			/* first line */
	WZCOORD linpos;			/* line position */
	SMALL linrmn;			/* lines rnemaining */
	SMALL intrmn;			/* intervals remaining in line */
	WZCOORD lftpos;			/* left end of interval */
	WZCOORD rgtpos;			/* right end of interval */
	SMALL nwlpos;			/* non-zero if new line, counts line
					   increment since last interval */
	struct gwspace *gryptr;		/* pointer to grey table workspace */
};

/* standard workspace for grey-table manipulations */

struct gwspace {
	SMALL gvio;			/* grey value i/o switch :
					   0 = input to object only
					   1 = output from object only
					   Only relevant if tranpl set, as all
					   grey-tables are unpacked. */
	SMALL tranpl;			/* if non-zero, transplant values to a
					   buffer whose address is grintptr.
					   Direction of transplant in gvio */
	struct valuetable *gtable;	/* grey table */
	struct valueline **gline;	/* pointer to current grey table line pointer */
	struct iwspace *intptr;		/* pointer to interval table workspace */
	GREY *grintptr;			/* pointer to interval grey table.
					   Always points to lowest order column,
					   whatever the value of raster */
};

/*
 * trivial property list
 * propertylist must ALWAYS commence with "size" which MUST be
 * initialised to its size in bytes
 */

struct propertylist {
	SMALL size;
};

struct graphicpropertylist {
	SMALL size;
	SMALL type;
	long linewidth;
	long linestyle;
	long colour;
	long graphicstyle;
};

struct stringpropertylist {
    SMALL size;
    TCHAR str;
};

/*
 * frame for object display
 *
 * Programming note - the only really sensible fields
 * are scale, dx and dy. All the other fields are
 * supported by the code, but should be set as noted.
 * Hopefully someday we can take them out completely.
 */
struct pframe {
	SMALL	type;	/* ------ 60 ----- */
	SMALL	scale;
	int		dx;
	int		dy;

	WZCOORD	ox;		/* These should always be idom->kol1/line1 */
	WZCOORD	oy;
	SMALL	ix;		/* These should always be 1 */
	SMALL	iy;
};


/* BP - old style pframe.
 * This has short dx and dy which can overflow.
 * Note that this old version IS USED FOR FILING
 * so that no new file format is required. */
struct old_pframe {
	SMALL	type;	/* ------ 60 ----- */
	SMALL	scale;	/* object coordinates to DIGS coordinates.
			   NOTE - positive only (see ix, iy below) */
	WZCOORD		dx;	/* DIGS coordinates x/column origin */
	WZCOORD		dy;	/* DIGS coordinates y/line origin */
	WZCOORD	ox;	/* object coordinates x origin - column in object
			   space to be located at DIGS column origin dx */
	WZCOORD	oy;	/* object coordinates y origin - line in object
			   space to be located at DIGS line origin dy */
	SMALL	ix;	/* x increment factor - can be used to set x-scale
			   different to yscale, or to reverse plotting
			   direction, etc..   Usually +1 or -1 but can
			   be varied for special effects. */
	SMALL	iy;	/* yscale factor */

	int (*func)();	/* transform function passed to picture routine,
			   where if not NULL then applied to each pixel.
			   This facility only applies to type 1 objects.
			   ----- NOT YET IMPLEMENTED -----
			   Note - no parameters permitted */
};



struct pgrid{
	SMALL type;	/* ------ 61 ------ */
	SMALL style;	/* 0=horizontal , 1=vertical */
	WZCOORD k1;	/* limits of grid */
	WZCOORD l1;
	WZCOORD k2;
	WZCOORD l2;
	WZCOORD spacing;	/* grid spacing, if <=0 don't draw grid */
};

/*
 * text object
 * holds text associated with the object
 * for screen display but will also be written to file
 * with the object
 */
struct textobj {
	SMALL type;			/* 70 for text*/
	struct textdomain *tdom;	/* details about the text*/
	TCHAR *text;			/* where the text is held */
	char *plist;		/*NULL*/
	char *assoc;		/*NULL*/
};

struct textdomain {
	SMALL type;
	WZCOORD k;			/* origin w.r.t. object coords */
	WZCOORD l;
	SMALL orientation;
	SMALL spacing;		
	SMALL stringdir;		/*string direction 1-4*/
	SMALL boldness;		
	SMALL font;
	SMALL stringlen;		/* length of the text string */
};

/*
 * compound objects, implemented as either ARRAYS or LINKED LISTS
 * of other objects.  There is a distinction between an compound
 * of the same type (e.g. resulting from a labelling) and a
 * compound of different types (e.g. resulting from a range of
 * image processes from a single original object).
 */
struct compounda {
	SMALL type;		/* 80: components all same type */
				/* 81: different types permitted */
	SMALL linkcount;
	SMALL otype;		/* the permitted type if type==80 */
	int n;			/* maximum number of objects (array length) */
	struct object **o;	/* the list of woolz object pointers */
	struct propertylist *p;	/* a non-specific property list */
	struct object *assoc;
};


struct compoundl{
	SMALL type;		/* 82: components all same type */
				/* 83: different types permitted */
	SMALL linkcount;
	SMALL otype;		/* the permitted type if type==82 */
	/*
	 *******UNDETERMINED LINKED LIST STRUCTURE
	 */
	struct plist *p;	/* a non-specific property list */
	struct object *assoc;
};

typedef struct numbering_struct{
		SMALL size;
    	short x;
    	short y;
		short max_chroms_in_class;
		short chrom_number_class;
		TCHAR chrom_numbering_class_lbl[100];	//chromosome numbering class label
    }chrome_numbering;


#pragma pack(2)

struct circle{
	SMALL type;	/* 100 */
	struct ivertex centre;
	int radius;
};
	
#pragma pack()


typedef struct object WObject;
typedef struct rawimage RawIm;


#ifndef FSCHAR
typedef unsigned char FSCHAR;
#endif

/*
 * Some convenient typedefs for
 * objects etc...
 */

typedef struct object Obj;

typedef struct intervaldomain Idom;
typedef struct intervalline Intervalline;
typedef struct interval IntervalW;

typedef struct valuetable Vdom;
typedef struct rectvaltb Vdom11;
typedef struct ivtable Vdom21;
typedef struct valueline Vline;

typedef struct pframe Pframe;
typedef struct propertylist Plist;
typedef struct graphicpropertylist GraphicPlist;


// These are the preferred aliases.
// The above are for compatibility only!
typedef struct object WZObj, *pWZObj;
typedef struct textobj WZTextObj, *pWZTextObj;
typedef struct circle WZCircle, *pWZCircle;
typedef struct intervaldomain WZIdom, *pWZIdom;
typedef struct polygondomain WZPoly, *pWZPoly;
typedef struct textdomain WZText, *pWZText;
typedef struct boundlist WZBound, *pWZBound;
typedef struct histogramdomain WZHisto, *pWZHisto;
typedef struct propertylist WZPlist, *pWZPlist;
typedef struct valuetable WZVdom, *pWZVdom;
typedef struct pframe WZPframe, *pWZPframe;

// Lord only knows what these are.....
typedef struct irect WZIrect, *pWZIrect;
typedef struct ivector WZIvector, *pWZIvector;
typedef struct ipoint WZIpoint, *pWZIpoint;



void* docalloc(int size);

// This lot used to be in subs.h
#define	Malloc	malloc
#define	Calloc	calloc
#define	Realloc	realloc
#define	Free	free


/*
 * End of wstruct.h section
 **************************/


/*********************************
 * This section used to be basic.h
 */

#include "ddgs.h" // For functions in interact.c

#include <stdio.h>


#ifdef __cplusplus
extern "C" {
#endif

/****** details.c ******/

WOOLZ_API void typeprint(int i);
WOOLZ_API void bytestreamprint(char *pl, int si);
WOOLZ_API void greystreamprint(GREY *pl, int si);
WOOLZ_API void intstreamprint(int *pl, int si);
WOOLZ_API void floatstreamprint(float *pl, int si);
WOOLZ_API void printinterval(struct interval *intv);
WOOLZ_API void printintervalline(struct intervalline *ivln, int nlines);
WOOLZ_API void printintervaldomain(struct intervaldomain *itvl);
WOOLZ_API void printintvtx(struct ivertex *vtx, int nverticies);
WOOLZ_API void printfloatvtx(struct fvertex *vtx, int nverticies);
WOOLZ_API void details(struct object *obj);

/****** gstretch.c ******/

WOOLZ_API void gstretch(register GREY *g, int count, register GREY newmax, register GREY min, register GREY range);
WOOLZ_API void gtabcontrast(struct object *obj, float *floatlut);
WOOLZ_API void gmmcontrast(struct object *obj, GREY min, GREY max);

/****** add.c ******/

WOOLZ_API int add(struct object *o1, struct object *o2, struct object *o3);

/****** area.c ******/

WOOLZ_API int area(struct object *obj);

/****** construct.c ******/

WOOLZ_API struct object *construct(unsigned char *addr, int xsize, int ysize, int pitch, unsigned char thresh, int invert);
WOOLZ_API int count_high(register unsigned char *addr, int xsize, int ysize, int pitch, register unsigned char thresh, int *intcount);
WOOLZ_API int count_low(register unsigned char *addr, int xsize, int ysize, int pitch, register unsigned char thresh, int *intcount);

/****** copy.c ******/

WOOLZ_API struct valuetable *copyvaluetb(struct object *obj, int type, int bckgrnd);

/****** copydomain.c ******/

WOOLZ_API struct intervaldomain *copydomain(struct intervaldomain *idom, struct interval * *spareitv, int *spc);

/****** diffdom.c ******/

WOOLZ_API struct object *diffdom(struct object *obj1, struct object *obj2);

/****** dispnum.c ******/

WOOLZ_API void dispnum(int num, int x, int y);

/****** dispstring.c ******/

WOOLZ_API int dispstring(LPCTSTR s, int x, int y);

/****** duplicate.c ******/

WOOLZ_API struct object *duplicate_obj(struct object *srcobj);
WOOLZ_API struct object *duplicate_obj_plus(struct object *srcobj);

/****** framedigs.c ******/

WOOLZ_API int frkdis(register int x, register struct pframe *f);
WOOLZ_API int frldis(register int y, register struct pframe *f);
WOOLZ_API int frkvdis(register int x, register struct pframe *f);
WOOLZ_API int frlvdis(register int y, register struct pframe *f);
WOOLZ_API int showframe(struct pframe *fr);
WOOLZ_API int frdigsxpos(register int x, register struct pframe *f);
WOOLZ_API int frdigsypos(register int y, register struct pframe *f);
WOOLZ_API short frinvxdig(register int xv, struct pframe *f);
WOOLZ_API short frinvydig(register int yv, struct pframe *f);
WOOLZ_API int limxdigfrm(register int x, register struct pframe *f);
WOOLZ_API int limydigfrm(register int y, register struct pframe *f);

/****** frameobjnum.c ******/

WOOLZ_API int frameobjnum(int num, struct object *obj, struct pframe *f);
WOOLZ_API int frameobjstr(char *str, struct object *obj, struct pframe *f);
WOOLZ_API int frameobjnnum(int num, struct object *obj, struct pframe *f, int position);

/****** freespace.c ******/

WOOLZ_API int freeobj_debug(FILE *chan);
WOOLZ_API int freeobj(struct object *obj);
WOOLZ_API int freedomain(struct intervaldomain *idom);
WOOLZ_API int freevaluetable(struct intervaldomain *vdom);
WOOLZ_API int freeplist(struct propertylist *plist);
WOOLZ_API int freehistodmn(struct histogramdomain *h);
WOOLZ_API int freeconvhull(struct cvhdom *c);
WOOLZ_API int freerectdmn(struct irect *r);
WOOLZ_API int freepolydmn(struct polygondomain *poly);
WOOLZ_API int freeboundlist(struct boundlist *b);
WOOLZ_API int freetextdomain(struct textdomain *tdom);

/****** grab_buffer.c ******/

WOOLZ_API unsigned char *init_grab_buffer(int nblocks, int w, int h);
WOOLZ_API unsigned char *grab_buffer(int start_block, int nblocks);
WOOLZ_API unsigned char *ungrab_buffer(int start_block, int nblocks);

/****** grey4val.c ******/

WOOLZ_API int grey4val(struct object *obj, int l, int k, register GREY *g4);

/****** greyscan.c ******/

WOOLZ_API void initgreyscan(struct object *obj, struct iwspace *iwsp, struct gwspace *gwsp);
WOOLZ_API void initgreyrasterscan(struct object *obj, struct iwspace *iwsp, struct gwspace *gwsp, int raster, int tranpl);
WOOLZ_API int initgreywsp(struct object *obj, register struct iwspace *iwsp, register struct gwspace *gwsp, int tranpl);
WOOLZ_API int nextgreyinterval(register struct iwspace *iwsp);
WOOLZ_API void greyinterval(register struct iwspace *iwsp);

/****** greyval.c ******/

WOOLZ_API GREY *greyval(struct object *obj, register int l, register int k);

/****** intcount.c ******/

WOOLZ_API int intcount(struct intervaldomain *idom);

/****** intdomscan.c ******/

WOOLZ_API int initrasterscan(struct object *obj, register struct iwspace *iwsp, int raster);
WOOLZ_API int nextinterval(register struct iwspace *iwsp);

/****** intendpoints.c ******/

WOOLZ_API int nextintervalendpoint(register struct iwspace *iwsp);
WOOLZ_API struct interval *firstintptr(struct object *obj, int line);
WOOLZ_API struct interval *lastintptr(struct object *obj, int line);

/****** interact.c ******/

WOOLZ_API struct object *makepolycircle(struct ivertex centre, int radius);
WOOLZ_API struct object *roimousepoly(struct pframe *f, DDGS *dg, int shape, int closed, int backupsteps); //Quick and dirty fix
WOOLZ_API struct object *mousepoly(struct pframe *f, DDGS *dg, int shape, int closed, int backupsteps);
WOOLZ_API struct ipoint mousepoint(struct pframe *f, DDGS *dg);

/****** interp.c ******/

WOOLZ_API GREY interp(register GREY *g4, register int fractline, register int fractkol);

/****** invertobj.c ******/

WOOLZ_API struct object *invertobject(struct object *obj);
WOOLZ_API struct intervaldomain *invidom(struct intervaldomain *idom, int *co_ords);
WOOLZ_API struct valuetable *invvdom(struct object *invobj, struct object *obj, int *co_ords);

/****** invertobjhor.c ******/

WOOLZ_API struct intervaldomain *invidom_hor(struct intervaldomain *idom, int *co_ords);
WOOLZ_API struct valuetable *invvdom_hor(struct object *invobj, struct object *obj, int *co_ords);
WOOLZ_API struct object *invertobject_hor(struct object *obj);

/****** invertobjver.c ******/

WOOLZ_API struct intervaldomain *invidom_ver(struct intervaldomain *idom, int *co_ords);
WOOLZ_API struct valuetable *invvdom_ver(struct object *invobj, struct object *obj, int *co_ords);
WOOLZ_API struct object *invertobject_ver(struct object *obj);

/****** linearea.c ******/

WOOLZ_API int linearea(struct object *obj);

/****** makecompound.c ******/

WOOLZ_API struct compounda *makecompounda(int type, int mode, int n, struct object * *ol, int otype);

/****** makeivtable.c ******/

WOOLZ_API struct ivtable *makeivtable(int type, struct object *obj, int bckgrnd);

/****** makestructs.c ******/

WOOLZ_API void Makestruct_debug(FILE *chan);
WOOLZ_API struct intervaldomain *makedomain(int type, int l1, int ll, int k1, int kl);
WOOLZ_API struct object *makemain(int type, struct intervaldomain *domain, struct valuetable *values, struct propertylist *plist, struct object *assoc);
WOOLZ_API struct valuetable *makevaluetb(int type, int l1, int ll, int bckgrnd, struct object *orig);
WOOLZ_API struct rectvaltb *makerectvaltb(int type, int line1, int lastln, int kol1, int width, GREY bckgrnd, GREY *values);
WOOLZ_API void makeinterval(int line, struct intervaldomain *domain, int nints, struct interval *intptr);
WOOLZ_API void makevalueline(struct valuetable *vtb, int line, int k1, int kl, GREY *greyptr);
WOOLZ_API struct polygondomain *makepolydmn(int type, WZCOORD *vertices, int n, int maxv, int copy);
WOOLZ_API struct histogramdomain *makehistodmn(int type, int n);
WOOLZ_API struct object *newgrey(struct object *iobj);
WOOLZ_API struct valuetable *newvaluetb(struct object *obj, int type, int bckgrnd);
WOOLZ_API struct intervaldomain *newidomain(struct intervaldomain *idom);
WOOLZ_API struct propertylist *makeplist(int size);
WOOLZ_API struct pframe *makepframe(int scale, int dx, int dy, int ox, int oy);
WOOLZ_API struct textobj *maketext(LPTSTR string, int l, int k, int stringlen);
WOOLZ_API struct textdomain *maketdomain(int l, int k, int stringlen);
WOOLZ_API struct object *makeirect();
WOOLZ_API struct object *makecircle(int cx, int cy, int radius);
WOOLZ_API struct object * makerect( int line1, int lastln, int kol1, int lastkl,
						  GREY * values, GREY bckgrnd, struct propertylist * prop, struct object * assoc_obj);

/****** move.c ******/

WOOLZ_API void moveidom(struct intervaldomain *idom, int x, int y);
WOOLZ_API void movevdom(struct valuetable *vdom, int x, int y);
WOOLZ_API struct object *moveobject(struct object *obj, int x, int y);

/****** obj2buffer.c ******/

WOOLZ_API unsigned char *set_object_buffer(struct object *obj, int *xout, int *yout, int *wout, int *hout, struct pframe *f);
WOOLZ_API unsigned char *set_object_mask(struct object *obj, int *xout, int *yout, int *wout, int *hout, struct pframe *f);

/****** picbuffer.c ******/

WOOLZ_API unsigned char *picbuffer(struct object *obj, int *xout, int *yout, int *wout, int *hout, struct pframe *f, unsigned char * *maskbuffer);
WOOLZ_API unsigned char *picrawbuffer(struct rawimage *obj, int *xout, int *yout, int *wout, int *hout, struct pframe *f, unsigned char * *maskbuffer);

/****** picframe.c ******/

WOOLZ_API int Picframe(struct object *obj, struct pframe *f, SMALL where);
WOOLZ_API void picframe(struct object *obj, struct pframe *f);
WOOLZ_API void opicframe(struct object *obj, struct pframe *f);
WOOLZ_API void mpicframe(struct object *obj, struct pframe *f);

WOOLZ_API void Picintobj(struct object *obj, register struct pframe *f, SMALL where);
WOOLZ_API void picintobj(struct object *obj, register struct pframe *f);
WOOLZ_API void opicintobj(struct object *obj, register struct pframe *f);
WOOLZ_API void mpicintobj(struct object *obj, register struct pframe *f);

WOOLZ_API void Picrawim(RawIm *im, register struct pframe *f, SMALL where);
WOOLZ_API void picrawim(RawIm *im, register struct pframe *f);
WOOLZ_API void opicrawim(RawIm *im, register struct pframe *f);
WOOLZ_API void mpicrawim(RawIm *im, register struct pframe *f);

WOOLZ_API void Picpoly(struct polygondomain *pdom, struct pframe *f, SMALL where);
WOOLZ_API void picpoly(struct polygondomain *pdom, struct pframe *f);
WOOLZ_API void opicpoly(struct polygondomain *pdom, struct pframe *f);
WOOLZ_API void mpicpoly(struct polygondomain *pdom, struct pframe *f);

WOOLZ_API void Piccircle(struct circle *circ, struct pframe *f, SMALL where);
WOOLZ_API void piccircle(struct circle *circ, struct pframe *f);
WOOLZ_API void opiccircle(struct circle *circ, struct pframe *f);
WOOLZ_API void mpiccircle(struct circle *circ, struct pframe *f);

WOOLZ_API void Picbound(struct boundlist *bound, struct pframe *f, SMALL where);
WOOLZ_API void picbound(struct boundlist *bound, struct pframe *f);
WOOLZ_API void opicbound(struct boundlist *bound, struct pframe *f);
WOOLZ_API void mpicbound(struct boundlist *bound, struct pframe *f);

WOOLZ_API void Picvec(struct ivector *vec, struct pframe *f, SMALL where);
WOOLZ_API void picvec(struct ivector *vec, struct pframe *f);
WOOLZ_API void opicvec(struct ivector *vec, struct pframe *f);
WOOLZ_API void mpicvec(struct ivector *vec, struct pframe *f);

WOOLZ_API void Picpoint(struct ipoint *point, struct pframe *f, SMALL where);
WOOLZ_API void picpoint(struct ipoint *point, struct pframe *f);
WOOLZ_API void opicpoint(struct ipoint *point, struct pframe *f);
WOOLZ_API void mpicpoint(struct ipoint *point, struct pframe *f);

WOOLZ_API void Piccursor(int scale, int style, SMALL where);
WOOLZ_API void piccursor(int scale, int style);
WOOLZ_API void opiccursor(int scale, int style);
WOOLZ_API void mpiccursor(int scale, int style);

WOOLZ_API void Picrect(struct irect *rdom, struct pframe *f, SMALL where);
WOOLZ_API void picrect(struct irect *rdom, struct pframe *f);
WOOLZ_API void opicrect(struct irect *rdom, struct pframe *f);
WOOLZ_API void mpicrect(struct irect *rdom, struct pframe *f);

WOOLZ_API void Pichisto(struct histogramdomain *h, struct pframe *f, SMALL where);
WOOLZ_API void pichisto(struct histogramdomain *h, struct pframe *f);
WOOLZ_API void opichisto(struct histogramdomain *h, struct pframe *f);
WOOLZ_API void mpichisto(struct histogramdomain *h, struct pframe *f);

WOOLZ_API void Pictext(struct textobj *tobj, struct pframe *f, SMALL where);
WOOLZ_API void pictext(struct textobj *tobj, struct pframe *f);
WOOLZ_API void opictext(struct textobj *tobj, struct pframe *f);
WOOLZ_API void mpictext(struct textobj *tobj, struct pframe *f);

/****** qreadobj.c ******/


/****** readobj.c ******/

WOOLZ_API struct object *readobj(FILE *fp);

/****** seqsize.c ******/

WOOLZ_API int seqsize(struct object *obj);

/****** setval.c ******/

WOOLZ_API int setval(struct object *obj, int val);

/****** showobjbox.c ******/

WOOLZ_API int bothbox(struct object *obj, struct pframe *fr, int tint, int mode);
WOOLZ_API int boxobj(struct object *obj, struct pframe *f, int tint);
WOOLZ_API int backlight(struct object *obj, struct pframe *fr, int tint);
WOOLZ_API void unbacklight();
WOOLZ_API void unbox();

/****** standardidom.c ******/

WOOLZ_API int standardidom(register struct intervaldomain *idom);

/****** textimage.c ******/

WOOLZ_API int load_text_fonts();
WOOLZ_API TCHAR *create_bitmap_text(LPTSTR str, int *outw, int *outh, char colour, char bg, int font);

/****** txtobj.c ******/

WOOLZ_API struct object *txtobj(LPTSTR s, int fontid, int scale);

/****** vdom11to1.c ******/

WOOLZ_API void vdom11to1(struct object *obj);

/****** vdom21to1.c ******/

WOOLZ_API void vdom21to1(struct object *obj);

/****** vreadobj.c ******/


/****** vwriteobj.c ******/


/****** writeobj.c ******/

WOOLZ_API int writeobj(FILE *fp, struct object *obj);

/****** qbuffer.c ******/

WOOLZ_API unsigned char *qbuffer(struct object *obj, int *xout, int *yout, int *wout, int *hout, struct pframe *f, unsigned char * *maskbuffer);


#ifdef __cplusplus
}
#endif

/*
 * end basic.h section.
 **********************/


/*******************************
 * This section used to be fip.h
 */

#ifdef __cplusplus
extern "C" {
#endif


/****** fsconstruct.c ******/

WOOLZ_API struct object *fsconstruct(FSCHAR *fsaddr, WZCOORD fslines, WZCOORD fscols, FSCHAR fsthresh, int fsinvert);
WOOLZ_API struct object *ctmakeobj(int lines, int cols, int intcount, int greycount);
WOOLZ_API int cthiscancount(FSCHAR *fsaddr, WZCOORD lines, WZCOORD cols, register FSCHAR thresh, int *intcountptr);
WOOLZ_API int ctlowscancount(FSCHAR *fsaddr, WZCOORD lines, WZCOORD cols, register FSCHAR thresh, int *intcountptr);
WOOLZ_API int cthiscanbuild(struct object *obj, FSCHAR *fsaddr, register FSCHAR thresh);
WOOLZ_API int ctlowscanbuild(struct object *obj, FSCHAR *fsaddr, register FSCHAR thresh);
WOOLZ_API int fsmemerror(int code, char *estr);
WOOLZ_API int fscheckerror();
WOOLZ_API struct object *capconstruct(FSCHAR *fsaddr, WZCOORD fslines, WZCOORD fscols, int fsthresh, int fsinvert);

/****** fsframehst.c ******/

WOOLZ_API struct object *fsframehisto(register unsigned char *frame, int ppl, int lpf, int maxg);

/****** fslinfilt.c ******/

WOOLZ_API FSCHAR *linfilt(FSCHAR *image1, int width, int height, struct convolution *filter);

/****** fsmorph.c ******/

WOOLZ_API FSCHAR *fsMin(FSCHAR *frame, int cols, int lines, int halfwidth);
WOOLZ_API FSCHAR *fsMax(FSCHAR *frame, int cols, int lines, int halfwidth);
WOOLZ_API FSCHAR *fsOpen(FSCHAR *frame, int cols, int lines, int halfwidth);
WOOLZ_API FSCHAR *fsClose(FSCHAR *frame, int cols, int lines, int halfwidth);
WOOLZ_API FSCHAR *fsExtract(FSCHAR *frame, int cols, int lines, int halfwidth, int noise_halfwidth);
WOOLZ_API void fsFreeWoolz(FSCHAR *im);

WOOLZ_API FSCHAR *fsMin84(FSCHAR *frame, int cols, int lines, int halfwidth);
WOOLZ_API FSCHAR *fsMax84(FSCHAR *frame, int cols, int lines, int halfwidth);
WOOLZ_API FSCHAR *fsOpen84(FSCHAR *frame, int cols, int lines, int halfwidth);
WOOLZ_API FSCHAR *fsClose84(FSCHAR *frame, int cols, int lines, int halfwidth);
WOOLZ_API FSCHAR *fsExtract84(FSCHAR *frame, int cols, int lines, int halfwidth, int noise_halfwidth);
WOOLZ_API FSCHAR *fsTophat(FSCHAR *frame, int cols, int lines, int halfwidth);
WOOLZ_API FSCHAR *fsSep(FSCHAR *frame, int cols, int lines, int halfwidth);
WOOLZ_API void fsReconstruct(FSCHAR *input, FSCHAR *reference, int cols, int lines, int count);
WOOLZ_API FSCHAR *fsMultires(FSCHAR *frame, int cols, int lines, int size_max, int size_min);
WOOLZ_API FSCHAR *fsMedian(FSCHAR *frame, int cols, int lines, int halfwidth);
WOOLZ_API FSCHAR *fsUnsharpMask(FSCHAR *frame, int cols, int lines, float radius, float depth);
WOOLZ_API FSCHAR *fsGaussian(FSCHAR *frame, int cols, int lines, float radius);

/****** fssecthst.c ******/

WOOLZ_API struct object *fssecthisto(register unsigned char *image, int width, int height, int loffset, int maxg);

/****** fsstretch.c ******/

WOOLZ_API void fsStretch(FSCHAR *frame, int cols, int lines);
WOOLZ_API void fs12to8(RawIm *im, BOOL stretch);
WOOLZ_API long fsClahe(FSCHAR *pImage, int uiXRes, int uiYRes, FSCHAR Min, FSCHAR Max, int uiNrX, int uiNrY, int uiNrBins, int fCliplimit);
static void ClipHistogram (unsigned long*, unsigned int, unsigned long);
static void MakeHistogram (FSCHAR*, unsigned int, unsigned int, unsigned int,
		unsigned long*, unsigned int, FSCHAR*);
static void MapHistogram (unsigned long*, FSCHAR, FSCHAR,
	       unsigned int, unsigned long);
static void MakeLut (FSCHAR*, FSCHAR, FSCHAR, unsigned int);
static void Interpolate (FSCHAR*, int, unsigned long*, unsigned long*,
	unsigned long*, unsigned long*, unsigned int, unsigned int, FSCHAR*);

/****** fsthresh.c ******/

WOOLZ_API int gradthresh(unsigned char *image, int cols, int lines);
WOOLZ_API int gradthresh2(unsigned char *image, int cols, int lines);
WOOLZ_API int BackgroundThresh(FSCHAR *image, int cols, int lines);

/****** fsxyshift.c ******/

WOOLZ_API void shift_image(char *im, int width, int height, int xshift, int yshift, char background);

#ifdef __cplusplus
}
#endif

/*
 * end fip.h section.
 ********************/


/*********************************
 * This section used to be gplib.h
 */

/****** fbio.c ******/

WOOLZ_API int fbread(register char *p, register int s, register int n, register FILE *fp);
WOOLZ_API int fbwrite(register char *p, register int s, register int n, register FILE *fp);

/****** misc.c ******/

WOOLZ_API int strntcpy(char *s1, char *s2, int n);
WOOLZ_API int strcasecmp(char *s1, char *s2);
WOOLZ_API int strncasecmp(char *s1, char *s2, int len);
WOOLZ_API int axtoi(char *ptr);
WOOLZ_API char *str2cmp(char *s, char *s1, char *s2);
WOOLZ_API int touch(char *name);
WOOLZ_API int bcopy(char *b1, char *b2, int length);
WOOLZ_API int instring(char *s1, char *s2);
WOOLZ_API int inbuffer(char *s1, int l1, char *s2, int l2);

/*
 * end gplib.h section.
 **********************/


/********************************
 * This section used to be subs.h
 */

/*
** the cplusplus wrap can be temporary until kcont_admin stops
** calling these functions !
*/
#ifdef __cplusplus
extern "C" {
#endif
/****** wtcheck.c ******/

WOOLZ_API int woolz_check_obj(struct object *obj, char *called_from);
WOOLZ_API int woolz_check_idom(register struct intervaldomain *idom, char *called_from);
WOOLZ_API int woolz_check_pdom(register struct polygondomain *pdom, char *called_from);
WOOLZ_API int woolz_check_vdom(struct object *vobj, char *called_from);
WOOLZ_API int woolz_check_cvhdom(struct cvhdom *cdom, char *called_from);
WOOLZ_API int woolz_check_rdom(struct irect *rdom, char *called_from);
WOOLZ_API int woolz_check_hdom(struct histogramdomain *hdom, char *called_from);
WOOLZ_API int woolz_check_blist(struct boundlist *blist, char *called_from);
WOOLZ_API int woolz_check_tdom(struct text *tobj, char *called_from);
WOOLZ_API int woolz_check_debug(FILE *fp);
WOOLZ_API int woolz_check_level(int level);
WOOLZ_API int woolzexitfn(int err_no, char *called_from);
WOOLZ_API int woolz_check_exit(int err_no, char *mesg, char *ptr);

/****** woolz_error.c ******/

WOOLZ_API void set_woolz_exit(int (*exit_fn)());
WOOLZ_API int woolz_exit(int err_no, char *called_from);

/****** hypot8.c ******/

WOOLZ_API int hypot8(register int i, register int j);

/****** isqrt.c ******/

WOOLZ_API int isqrt(int x);

/****** isqrt8.c ******/

WOOLZ_API int isqrt8(int x);

/****** redmem.c ******/

WOOLZ_API void init_maldebug(FILE *fp);
WOOLZ_API void mal_comment(char *s);

/*
Moved this lot to wstruct.h as it's only for internal code
#ifdef WIN32
#define	Malloc	malloc
#define	Calloc	calloc
#define	Realloc	realloc
#define	Free	free
#else
char *Malloc(int n);
char *Calloc(unsigned n, unsigned s);
char *Realloc(char *m, unsigned int n);
int Free(char *x);
#endif
*/

/****** wzcheck.c ******/

WOOLZ_API int wzcheckobj(struct object *obj);
WOOLZ_API int wzemptyidom(struct intervaldomain *idom);
WOOLZ_API int wzemptypdom(struct polygondomain *pdom);

/****** wzerror.c ******/

WOOLZ_API char *woolz_err_msg(int err_no);
WOOLZ_API int woolz_fatal(int err_no);

#ifdef __cplusplus
}
#endif

/*
 * end subs.h section.
 *********************/


/*********************************
 * This section used to be trans.h
 */

#ifdef __cplusplus
extern "C" {
#endif


/****** spin.c ******/

WOOLZ_API struct object *spin(struct object *obj, double radians);
WOOLZ_API struct object *spinsqueeze(struct object *obj, double *radians, double *xs, double *ys);
WOOLZ_API struct object *nothresh_spinsqueeze(struct object *obj, double *radians, double *xs, double *ys);
WOOLZ_API struct object *mode_spinsqueeze(struct object *obj, double *radians, double *xs, double *ys);
WOOLZ_API struct object *squeeze(struct object *obj, double xs, double ys);
WOOLZ_API struct object *rottrans(struct object *obj, double radians);
WOOLZ_API struct object *spin_offset(struct object *obj, int ycent, int xcent, double radians);
WOOLZ_API struct object *rottrans_offset(struct object *obj, int ycent, int xcent, double radians);

/****** compthr3.c ******/

WOOLZ_API int compthr3(struct object *histo);

/****** convhull.c ******/

WOOLZ_API struct object *convhull(struct object *obj);
WOOLZ_API struct object *cvhpoly(struct object *obj);
WOOLZ_API int chlpars(struct object *cvh, struct object *obj);

/****** convolve.c ******/

WOOLZ_API int convolve(int cv_long);
WOOLZ_API int applyconvolution_ow(struct object *obj, struct convolution *conv);
WOOLZ_API int applyconvolution_new(struct object *obj, struct convolution *conv);
WOOLZ_API int convsum(struct convolution *conv);
WOOLZ_API int onedx_conv(struct convolution *conv);
WOOLZ_API int normalise_conv(struct convolution *conv);
WOOLZ_API int modulus_conv(struct convolution *conv);

/****** curvat.c ******/

WOOLZ_API struct object *curvat(struct object *ubound);

/****** curvsmooth.c ******/

WOOLZ_API void curvsmooth(struct object *histo, int iterations);

/****** dilation.c ******/

WOOLZ_API struct object *dilation(struct object *obj);

/****** dilation4.c ******/

WOOLZ_API struct object *dilation4(struct object *obj);

/****** edgegv.c ******/

WOOLZ_API int edgegv(struct object *obj);
WOOLZ_API int set_edgegv(struct object *obj, GREY val, int width);

/****** erosion.c ******/

WOOLZ_API struct object *erosion(struct object *obj);

/****** erosion4.c ******/

WOOLZ_API struct object *erosion4(struct object *obj);

/****** extractov.c ******/

WOOLZ_API int Extractov_debug(FILE *chan);
WOOLZ_API struct object *extractov(struct object *obj, struct object *poly, int w);

/****** extremum.c ******/

WOOLZ_API int extremum(int size);

/****** greyrange.c ******/

WOOLZ_API int greyrange(struct object *obj, GREY *min, GREY *max);

/****** h256norm.c ******/

WOOLZ_API int histo256normalise(struct object *obj);

/****** histinvert.c ******/

WOOLZ_API int histinvert(struct object *h);

/****** histo.c ******/

WOOLZ_API struct object *histo(struct object *obj);

/****** histonorm.c ******/

WOOLZ_API int histonormalise(struct object *obj);

/****** histosmooth.c ******/

WOOLZ_API int histosmooth(struct object *histo, int iterations);

/****** histthsmooth.c ******/

WOOLZ_API int histothreshsmooth(struct object *histo);

/****** htnormalise.c ******/

WOOLZ_API int htnormalise(struct object *obj, float toppc, float botpc);

/****** hullarea.c ******/

WOOLZ_API int hullarea(struct object *cvh, register int *perim);

/****** ibound.c ******/

WOOLZ_API struct object *ibound(struct object *obj, int wrap);
WOOLZ_API struct ivertex *bigbound(struct object *obj, int wrap, int *nvtx);
WOOLZ_API struct bound_interval *makeboundints(struct object *obj, int icount);
WOOLZ_API int nextunprocessed(register struct bound_interval *bi, struct bound_interval *bitop, register struct bound_point *bp);
WOOLZ_API int bnd_link(register struct boundlist *bl, register struct boundlist *bp);
WOOLZ_API int newbpoint(register struct bound_point *cp, register struct bound_interval *bi, register struct ivertex * *vtx, int *nv, int dir);
WOOLZ_API int extranewbpoint(register struct bound_point *cp, struct bound_interval *bi, register struct ivertex * *vtx, int *nv, int dir);
WOOLZ_API int vertcompress(struct ivertex * *vtx, int *nv);
WOOLZ_API int wraparound(register struct polygondomain *pdom, register int wrap);
WOOLZ_API struct object *create_dummy_poly();

/****** intersect2.c ******/

WOOLZ_API struct object *intersect2(struct object *obj1, struct object *obj2);

/****** intersectn.c ******/

WOOLZ_API struct object *intersectn(struct object * *objs, int n, int uvt);

/****** intrp8.c ******/

WOOLZ_API struct polygondomain *intrp8(struct polygondomain *p1);

/****** invert.c ******/

WOOLZ_API void invert(struct object *obj);
WOOLZ_API void invert_direct(struct object *obj);
WOOLZ_API void fsinvertimage(FSCHAR *frame, int cols, int lines);

/****** itrpoly.c ******/

WOOLZ_API struct polygondomain *inttorealpoly(struct polygondomain *p1);

/****** iuspace.c ******/

WOOLZ_API struct polygondomain *iuspace(struct polygondomain *pdom);
WOOLZ_API int ipolylength(struct polygondomain *pdom);

/****** label.c ******/

WOOLZ_API int label(struct object *obj, int *mm, struct object * *objlist, int nobj, int ignlns);
WOOLZ_API int label_area(struct object *obj, int *mm, struct object * *objlist, int nobj, int ignlns);
WOOLZ_API int label_maxdim(struct object *obj, int *mm, struct object * *objlist, int nobj, int ignlns);
WOOLZ_API int showlinks(struct link *mlist, int all, char *tag);

/****** mass.c ******/

WOOLZ_API int mass(struct object *obj);

/****** maxdiam.c ******/

WOOLZ_API int maxdiam(struct object *obj);

/****** median.c ******/

WOOLZ_API int median(int size);

/****** minwrect.c ******/

WOOLZ_API struct object *minwrect(struct object *cvh);
WOOLZ_API void intersect(register struct chord *ch1, register struct chord *ch2, register float *x, register float *y);

/****** mwrangle.c ******/

WOOLZ_API int mwrangle(struct object *cvh, struct chord * *chr, int *nn1, int *nn2, int *s, int *c);
WOOLZ_API int gap(struct chord *ch, register struct polygondomain *pdom, register int *nummin, int *nummax);

/****** normalise.c ******/

WOOLZ_API void normalise(struct object *obj);

/****** octagon.c ******/

WOOLZ_API struct object *octagon(struct object *obj, int tip);

/****** polyinvert.c ******/

WOOLZ_API int polyinvert(struct polygondomain *p);

/****** polylength.c ******/

WOOLZ_API double polylength(struct polygondomain *p);

/****** polymult.c ******/

WOOLZ_API int polymult(register struct polygondomain *poly, register int m);

/****** polysmooth.c ******/

WOOLZ_API int polysmooth(register struct polygondomain *poly, int iterations);

/****** polysplit.c ******/

WOOLZ_API int polysplit(struct object *obj, struct object *poly, struct object * *objlist, register int *numobj);

/****** polysqueeze.c ******/

WOOLZ_API int polysqueeze(struct polygondomain *pdom, double xs, double ys);

/****** polytoblob.c ******/

WOOLZ_API struct object *polytoblob(struct polygondomain *pdom);
WOOLZ_API struct object *bigboundtoblob(struct ivertex *ivtx, int nvertices);

/****** polytoobj.c ******/

WOOLZ_API struct object *polytoobj(struct polygondomain *plgd);
WOOLZ_API int countpoly(struct polygondomain *plgd);
WOOLZ_API int line_int_order(register struct line_int *lp1, register struct line_int *lp2);
WOOLZ_API struct object *bigboundtoobj(struct ivertex *ivtx, int nvertices);

/****** rsupoly.c ******/

WOOLZ_API struct object *realsmoothunitpoly(struct object *polyobj, int iter);

/****** seqpar.c ******/

WOOLZ_API struct object *seqpar(struct object *iobj,int notovr,int seqent,int raster,int brdrsz,
			int backgr, int sparam, int (*extsub)(int));

/****** shift.c ******/

WOOLZ_API struct object *shift(struct object *obj, int lshift, int kshift);

/****** skeleton.c ******/

WOOLZ_API struct object *skeleton(struct object *obj, int smoothpasses);

/****** test.c ******/

/*int convolve(struct convolution *cvl);*/

/****** threshlabel.c ******/

WOOLZ_API int threshlabel(struct object *obj, int thresh, int *mm, struct object * *objlist, int nobj, int ignlns);

/****** threshold.c ******/

WOOLZ_API struct object *threshold(struct object *obj, register int thresh);

/****** threshset.c ******/

WOOLZ_API void topset(int jjdqt);
WOOLZ_API void hilow(int hi);

/****** union2.c ******/

WOOLZ_API struct object *union2(struct object *obj1, struct object *obj2);

/****** unionn.c ******/

WOOLZ_API struct object *unionn(struct object * *objs, int n, int uvt);
WOOLZ_API struct object *unionn_grey_from_first(struct object * *objs, int n, int uvt);

/****** unitbound.c ******/

WOOLZ_API struct object *unitbound(struct object *bound);
WOOLZ_API struct ivertex *unitbigbound(struct ivertex *bigbound, int nvertices, int *unvertices);

/****** unitspace.c ******/

WOOLZ_API struct polygondomain *unitspace(struct polygondomain *p1);

/****** compthr6.c ******/

WOOLZ_API int compthr6(struct object *histo);


#ifdef __cplusplus
}
#endif

/*
 * end trans.h section.
 **********************/


WOOLZ_API struct object *deagglomerate(struct object *AgglomObj);
WOOLZ_API long rec_deagglomerate(struct object *AgglomObj, struct object **DeAgglomObjList, BOOL init);


/* 
 * This has to be here else when this header is included before WinCV's
 * split.h header (e.g. in WinCV's splitlib library), there are compile errors
 * apparently because variables called 'small' are seen by the compiler as
 * 'char', as though 'small' was typedef'd or #defined as char. Although I
 * can't see how this is happening, it seems to do with this header, so I
 * undefine 'small' here.
 */
#undef small


#endif // #ifndef WOOLZ_H
