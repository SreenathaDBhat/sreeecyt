/*
 *	XCANVAS.CPP	Mark Gregson	22/9/92
 *
 *	X routines for interacting with canvases
 *
 * Mods:
 *
 *	Dec/Jan 02/03 MC Mods for Metaphase Annotation
 *	23Mar2000	JMB	Converted to .cpp, minor mods to reduce warnings,
 *					changed headers included.
 *	21Jan97	BP:		Completed Windows version. ACtually not
 *					much change.
 *					Don't free mousepts - done by mousepoly.
 *	31 Mar 1993	MG	Fixed Lassoo to work for anny type woolz object - uses frame if not type 1
 *
 */

#include "canvas.h"

//#include "canvasdll.h"

//#include <canvas.h>
#include <math.h>
#include <stdio.h>

/* globals used by box_selector */
static BOOL active = FALSE;


/* 
 *	BOX_SELECTOREH
 *
 *	event handler - monitors position of mouse curson over canvas display
 *	and draws box in overlay plane to mark nearest canvas frame
 *
 *	select_box is a Cselectdata structure
 *
 */
void
box_selectorEH(Cselectdata *select_box, int x, int y, BOOL click)
{
	Canvas *newcanvas, *oldcanvas;

	if (select_box == NULL)
		return;

	if (select_box->dg == NULL)
		return;

	if (active == FALSE)
		return;

	x *= 8;
	y *= 8;

	oldcanvas = select_box->selected_canvas;
	newcanvas = select_Canvas(select_box->main_canvas,
			                  select_box->types, select_box->ntypes, x, y);

	if (newcanvas == oldcanvas)
		return;

	select_box->selected_canvas = newcanvas;
	selectddgs(select_box->dg);

	intens(1);

	// Toggle the old one OFF.
	if (oldcanvas)
		display_Canvas_frame(oldcanvas, OVLYIM, 1, COPY);


	// Toggle the new one ON.
	if (newcanvas)
		display_Canvas_frame(newcanvas, OVLYIM, 1, COPY);
}


/* 	
 *	BOX_SELECTOR
 *
 *	Monitors position of mouse cursor over canvas displayed in
 *	current DDGS structure dg and draws a box in the overlay plane 
 *      to mark the selected canvas frame.
 *	Only canvases of the correct canvas_types are highlighted.
 *	Returns pointer to highlighted canvas when mouse button pressed
 *	- this can be NULL if no canvas is highlighted.
 *	event handler - monitors position of mouse curson over canvas display
 *	and draws box in overlay plane to mark nearest canvas frame
 */	
Canvas *
box_selector(DDGS *dg, Canvas *canvas, short *canvas_types, int ntypes)
{
	// I sincerely hope that this function is
	// never used (especially in the X/UNIX version!)
	return NULL;
}


void
activate_box_selector(Cselectdata *select_box)
{
	if (select_box == NULL)
		return;

	active = TRUE;

	select_box->selected_canvas = NULL;

	// Twiddle the mouse to make sure we see
	// anything already underneath.
	mouse_event(MOUSEEVENTF_MOVE, 0, 5, 0, 0);
	mouse_event(MOUSEEVENTF_MOVE, 0, (DWORD)-5, 0, 0);
}


void
deactivate_box_selector(Cselectdata *select_box)
{
	active = FALSE;

	if (select_box == NULL)
		return;

	// Remove selection box.
	if (select_box->selected_canvas)
		display_Canvas_frame(select_box->selected_canvas, OVLYIM, 1, COPY);
}


/************************** LASSOO SELECT STUFF AND NONSENSE *****************************/

/*
 *	G E T _ L A S S O O _ P O L Y 
 *
 *	Creates a polygon object from the shape drawn by the user
 *
 *	shape can be :	LINE - freehand line 
 *			POLYLINE - polygon vertices
 *
 *	All drawing takes place in the overlay plane of the window held
 *      in the DDGS structure dg.
 *
 *	pframe f ensures that polygon coordinates can be matched
 *	to object coordinates in the image
 *
 */

#ifdef WIN32
struct polygondomain *
get_lassoo_poly(struct pframe *f, DDGS *dg)
#endif
#ifdef i386
struct polygondomain *
get_lassoo_poly(f, dg)
struct pframe *f;
DDGS *dg;
#endif
{
	struct polygondomain *pdom/*, *makepolydmn()*/;
	int i,xs,ys,xo,yo,npts;
	int xp,yp;
	double xd,yd;

	xs=f->ix * f->scale;
	ys=f->iy * f->scale;
	xo=f->dx - f->ox * xs + abs(xs)/2;
	yo=f->dy - f->oy * ys + abs(ys)/2;

	npts=dg->nmousepts;

	/* create woolz polygon domain */
	pdom = makepolydmn(1,NULL,npts,npts,1);

	/* convert frame coordinates to object coordinates */
	for (i=0; i<dg->nmousepts; i++) {

			//convert mouse points from screen points to image points
		xd = (double)dg->mousepts[i].x;
		yd = (double)dg->mousepts[i].y;
		screentoimage (&xd,&yd);
		xp = (int) xd;
		yp = (int) yd;

		pdom->vtx[i].vtX=((xp) * 8 - xo) / xs;
		pdom->vtx[i].vtY=((yp) * 8 - yo) / ys;
		}

	return(pdom);
}



/*
 *	recursive_lassoo
 *
 *	Internal called by lassoo_select_Canvas - performs recursive canvas
 *	searching to test if a canvas object lies on/in the lassoo and if
 *	so adds the canvas pointer to the canvas_list
 */
#ifdef WIN32
void
recursive_lassoo(Cselectdata *select_box, Canvas *canvas,
				 Canvas ***canvas_list, int *nselected,
				 int *nallowed, struct object *lassoo_obj,
				 int lassoo_dx, int lassoo_dy)
#endif
#ifdef i386
void
recursive_lassoo(select_box, canvas, canvas_list, nselected, nallowed, lassoo_obj, lassoo_dx, lassoo_dy)
Cselectdata *select_box;
Canvas *canvas;
Canvas ***canvas_list;
int *nselected;
int *nallowed;
struct object *lassoo_obj;
int lassoo_dx;
int lassoo_dy;
#endif
{
	struct pframe *f;
	int t, right_type;
	struct object *obj, *hit/*, *intersect2(), *makerect()*/, *duplicate_obj_plus();
	int obj_dx, obj_dy, xs, ys, offsetx, offsety;
	int made_up_object;
	

	if (canvas!=NULL) {

		/* search siblings */
		recursive_lassoo(select_box,
				 canvas->next,
				 canvas_list,
				 nselected,
				 nallowed,
				 lassoo_obj,
				 lassoo_dx,
				 lassoo_dy );

		/* only search canvas if its visible */
		if (canvas->visible==1) {

			/* search children */
			recursive_lassoo(select_box,
					 canvas->patch,
					 canvas_list,
					 nselected,
					 nallowed,
					 lassoo_obj,
					 lassoo_dx,
					 lassoo_dy );

			/* only check canvas of correct type */
			t=0;
			right_type=0;
			while ((t<select_box->ntypes) && (!right_type)) {
				right_type=(select_box->types[t]==canvas->type);
				t++;
			}

			if ((right_type) && (canvas->Wobj!=NULL)) {

				/* get woolz object pframe */
				f=Canvas_Wobj_pframe( canvas );				

				/* calculate coordinate shift to move object 
				   into same coordinate space as the lassoo */
				xs=f->ix*f->scale;
				ys=f->iy*f->scale;
				obj_dx=f->dx/abs(xs) - f->ox;
				obj_dy=f->dy/abs(ys) - f->oy;

				/* if not type 1 object - make a rectangular one 
				   the size of the object's frame */
				obj=canvas->Wobj;

				if (obj->type != 1) {
					obj=makerect(	f->oy, f->oy + canvas->frame.height/abs(ys)-1,
							f->ox, f->ox + canvas->frame.width/abs(xs)-1,
							NULL,NULL,NULL,NULL);
					made_up_object=1;
				}				
				else
					made_up_object=0;
				     
				
				/* deal with inverted objects - badly - object's
				   idom should really be inverted as in picframe */
				if (xs<0) obj_dy+=(obj->idom->lastln - obj->idom->line1);
				if (ys<0) obj_dx+=(obj->idom->lastkl - obj->idom->kol1);

				offsetx=obj_dx - lassoo_dx;
				offsety=obj_dy - lassoo_dy;

				/* move object */
				moveidom(obj->idom, offsetx, offsety);

				/* check object intersects lassoo */
				hit=intersect2(lassoo_obj, obj);

				if (wzemptyidom(hit->idom)==0) {

					/* store ptr to canvas in canvas_list */
					(*canvas_list)[(*nselected)]=canvas;
					(*nselected)++;

					/* allocate more space if canvas_list is full */
					if (*nselected == *nallowed ) {
						(*nallowed)+=50;

						*canvas_list = (Canvas **)Realloc((char *)(*canvas_list), sizeof(canvas)*(*nallowed));

						if (*canvas_list==NULL) {
							fprintf(stderr,"lassoo_select_Canvas allocation failure \n");
							exit(1);
						}
					}
				}

				freeobj(hit);

				if (made_up_object != 0)
					freeobj(obj);
				else
					/* move object back */
					moveidom(obj->idom, -offsetx, -offsety);

				Free((char *)f);

			}
		}
	}
}


/*
 *	LASSOO_SELECT_CANVAS
 *
 *	Allows the user to draw a LINE or POLYLINE through or around a group of
 *	objects he wants to select. If the line goes through the object it is selected
 *	or if the line is drawn as a lassoo (i.e. a closed loop) all objects on or
 *	inside of the lassoo line are selected.
 *	
 *	linestyle can be LINE or POLYLINE
 *
 *	select_box contains the ddgs structure, a list of types of canvas to select
 *	and the number of types in the types list.
 *
 *	The routine returns a list of pointers to selected canvases - NOTE that it
 *	is down to the user to free this list when he is finished with it.
 *	The nselected variable returns the number of canvases in the list.
 *
 */
#ifdef WIN32
Canvas * *
lassoo_select_Canvas(short linestyle, Cselectdata *select_box, int *nselected)
#endif
#ifdef i386
Canvas * *
lassoo_select_Canvas(linestyle, select_box, nselected)
short linestyle;
Cselectdata *select_box;
int *nselected;
#endif
{
	Canvas **canvas_list, *canvas, *parent;
	struct polygondomain *lassoo_poly;
	struct object *lassoo_obj;
	struct pframe *mainf;
	int visible;
	int nallowed=50;
	int lassoo_dx, lassoo_dy;
	int xs,ys;


	/* get search canvas from select_box structure */	
	canvas=select_box->main_canvas;

	/* allocate space for array of pointers to selected canvases */
	canvas_list=(Canvas **)Malloc(sizeof(canvas) * nallowed);

	/* initialise count */
	*nselected=0;
	
	
	if ((canvas!=NULL) && (canvas->visible==1)) {

		/* check ancestor visibility - if parent invisible so is canvas */
		visible=canvas->visible;

		parent=canvas->parent;

		while (parent!=NULL) {
			visible=parent->visible;
			parent=parent->parent;
		}
	
		if (visible==1) {
			/* protect against illegal linestyles */
			if ((linestyle != DDGSLINE) && (linestyle != DDGSPOLYLINE))
				linestyle = DDGSLINE;

//			show_time_cursor(select_box->dg->displaywidget);

			/* draw lassoo with mouse - ddgs routine */
			mousedraw(linestyle, select_box->dg);

			/* get canvas pframe */
			mainf=Canvas_Wobj_pframe( canvas );				

			/* get lassoo poly for this pframe */
			lassoo_poly=get_lassoo_poly(mainf,select_box->dg);

			/* convert to solid object */
			if (lassoo_poly != NULL)
				lassoo_obj=(struct object *)polytoblob(lassoo_poly);
			else 
				lassoo_obj=NULL;

			select_box->dg->nmousepts = 0;
			if (select_box->dg->mousepts)
			{
				free(select_box->dg->mousepts);
				select_box->dg->mousepts = NULL;	
			}

			if (lassoo_obj != NULL) {

				/* get lassoo offset */
				xs=mainf->ix*mainf->scale;
				ys=mainf->iy*mainf->scale;
				lassoo_dx=mainf->dx/abs(xs) - mainf->ox;
				lassoo_dy=mainf->dy/abs(ys) - mainf->oy;

				/* look for canvases inside lassoo */
				recursive_lassoo(select_box,
						canvas,
						&canvas_list,
						nselected,
						&nallowed,
						lassoo_obj,
						lassoo_dx,
						lassoo_dy);

				freeobj(lassoo_obj);
			}
			
			if (lassoo_poly != NULL)
				freepolydmn(lassoo_poly);

			Free((char *)mainf);

//			restore_cursor();
		}
	}

	/* return list of selected canvases */
	return(canvas_list);
}

//
// Functions for Metaphase annotation
//
BOOL MetaphaseImage(Canvas *selcan)
{
	Canvas *canvas = selcan;

	while (canvas != NULL)
	{
		if (canvas->type == Cmetaphase)
			return(TRUE);

		canvas = canvas->parent;
	}

	return(FALSE);
}

//
// If custom analysis 'object type' on, METcanvas = CGraphic, else METcanvas = CMetblob
//
// So get metblob canvas in currently selected canvas ('vertical' search)
//
Canvas *GetMetblobcanvas(Canvas *canvas)
{
	Canvas *METcanvas = canvas;

	while (METcanvas != NULL)
	{
		if (METcanvas->type == Cmetblob)
			return(METcanvas);

		METcanvas = METcanvas->parent;
	}

	return(NULL);
}

//
// So get any metanno canvas in chain
//
Canvas *GetMetannocanvas(Canvas *canvas)
{
	Canvas *METcanvas = canvas;

	if (METcanvas == NULL)
		return(NULL);

	if (METcanvas->type != Cmetblob)
		return(NULL);

	METcanvas = METcanvas->patch;
	while (METcanvas != NULL)
	{
		if (METcanvas->type == Cmetanno)
			return(METcanvas);

		METcanvas = METcanvas->next;
	}

	return(NULL);
}

BOOL IsMetAnnoLabelPermanent(Canvas *canvas)
{
	Canvas *METcanvas, *METannocanvas;
	Cblobdata *blobdata;

	if (MetaphaseImage(canvas))
	{
		if ((METcanvas = GetMetblobcanvas(canvas)) == NULL)
			return(FALSE);

		if ((METannocanvas = GetMetannocanvas(METcanvas)) == NULL)
			return(FALSE);

		if ((blobdata = (Cblobdata *)METcanvas->data) == NULL)
			return(FALSE);

		if (METannocanvas->visible && (blobdata->label == PERMLABEL))
			return(TRUE);
	}

	return(FALSE);
}
