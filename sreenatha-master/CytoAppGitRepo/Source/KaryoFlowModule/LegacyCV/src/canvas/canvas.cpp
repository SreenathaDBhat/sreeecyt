
/*
 *	CANVAS.CPP	26th August 1992 	Mark Gregson
 *
 * Basic Canvas manipulation routines
 *
 * Mods:
 *
 *	06May2003	JMB	Added EnableCanvasImageDisplay() and EnableCanvasFrameDisplay().
 *					These were created to selectively disable types of canvas
 *					display when (re)using particular canvas manipulation functions
 *					(specifically, the contrast, sharpen and undo routines) in a 
 *					different manner from normal. The display_Canvas(),
 *					expose_Canvas() and display_Canvas_frame() functions now return
 *					a boolean value indicating whether they have failed (perhaps
 *					due to disabling of particular sorts of display).
 *	05Sep2000	JMB	Modified recursive_fscale() to work with new ideograms.
 *	25May2000	JMB	Added code to display_wobj() to draw new-style ideograms.
 *					This code can be disabled by defining USE_OLD_IDEOGRAMS.
 *	11Apr2000	KS	Added new variable show_FLEXprobeframes with access functions
 *					used in display_Wobj to show (or not) frames around probe objects in flex screens
 *	23Mar2000	JMB	Converted to .cpp, minor mods to reduce warnings,
 *					changed headers included.
 *	MG	01Sep99		Added canvas_spinsqueeze_method to augment fscale_Canvas()
 *					- required to prevent breakdown of MFISH pseudocolour objects
 *					when scaled in multicell - but this will be useful elesewhere
 *					New functions added: mode_fscale_Canvas() & nothresh_fscale_Canvas()
 *	KS	10Aug99		Support for hidden canvases
 *	MG	4Aug98		Removed fscale_probe_canvas() which simply did not work
 *					and fscale_Canvas was more than adequate ( when the call
 *					to fscale_probe_canvas() was removed.
 *	MG	4Aug98:		Modified display_Wobj() to stop erasing the area under
 *					a MARKIM object uneccessarily - was causing problems when
 *					displaying CGH amp and del patches on chromosomes in flex.
 *  WH	20Mar98		ix/iy - new plan - leave ix/iy as it was, but we will invert any
 *					canvas flagged as inverted on load so that it should never get called.
 *	WH	11Mar98		Put draw_overlap_mask_buffer() back in as we need to mask image.
 *	dcb	3Oct97		Use ix, iy in display_Wobj()
 *	WH	08May97		display_Wobj() now nolonger uses the cpframe ix/iy flags for
 *					inversion of objects. They are now physically inverted. However
 *					an exception is type 90 (raws) which use the iy flags to invert the
 *					raw image in woolz/basic picrawbuffer.c
 *	WH	05Mar97:	Check if canvas->parent is NULL before reference in cut_canvas()
 *	MG	23Jul96:	Modified display_Wobj() to erase area under Coverlap
 *				if dog is not probe_capable - to fix problems with
 *				Univision printing which mixes an 8bit dog with a
 *				24bit buffer - normal 24bit and 8bit systems will
 *				be unaffected.
 *	WH	12Jun96:	Erase background for text objects (type 70) in display_Wobj()
 *				as Picframe doesn't	handle these via mtext() function therefore 
 *				foreground may not be correct at intens 0 in pseudocolour.
 *	BP	3/27/96:	No more FBORDER. reset_probe_frames now has border_width.
 *	BP	3/26/96:	Remove unnecessary botch to hack probe borders in fscale.
 *	BP	11/7/95:	Botch to avoid white shadows on black image.
 *	BP	11/1/95:	New function attach_Canvas() to paste canvas on to END of
 *					existing chain.
 *	BP	8/21/95:	Slight speed improvement in recursive_find() by
 *					avoiding division. Major improvement by not looking
 *					at children of Cfetal!
 *	BP	7/19/95:	Use int for temp_dx in swap_Canvas().
 *	BP	6/28/95:	Added handling of Cfetal canvas type (mostly similar to
 *					Coverlap).
 *	MC	15jun 95	Added extra test to display_Wobj() 'erase' section 
 *				(Wobj->type == 1) so that d'ont erase if its a canvas 
 *				for a polygon (from highlight_canvas_object), erase for
 *				this case done the 'old' way. BP has added further test	
 *				canvas->type == Coverlap, I believe for probes in flex screen.
 *				(The purpose of these mods is to remove 'red' split lines,
 *				outside canvas after split.)
 *	MG	16Mar95		Modified expose canvas to correctly expose sub canvases
 *				of an exposed woolz object canvas - also check that a
 *				canvas has a wool object on it before displaying.
 *	BP	12/10/94:	Erasing of objects done with XClearArea() for speed.
 *					New probe display inserted in display_Wobj().
 *					No more calls to display_probe_Canvas().
 *	 4 Mar 1993	MG	added drawmode to display_Wobj() and
 *					display_Canvas_frame().
 *	25 Feb 1993	MG	destroy_Canvas_and_Wobj and chain version added
 *	22 Jan 1993	MG	reversed canvas tree drawing - now top down 
 *	 4 Jan 1993	MG	added backward visibility check to recursive_display()
 *	21 Dec 1992	MG	replaced global CANVAS_ASPECT with canvas->frame.aspect
 *	21 Sep 1992	MG	added canvas_Select for interactive canvas selection 
 *	16 Sep 1992	MG 	CANVAS_ASPECT to handle FIP digitised metaphases
 *	15 Sep 1992 	MG	added copy_Canvas
 *	14 Sep 1992 	MG	added previous canvas pointer for chain handling
 *				and swap_Canvas routine
 *	 3 Sep 1992	MG	added woolz object display arena to recursive display
 */



//#define USE_OLD_IDEOGRAMS



#include "canvas.h"
#include <ddgs.h>
#include <woolz.h>
#include <ideogramdll.h>

#include <stdio.h>
#include <memory.h>
//#include <wstruct.h>
//#include <common.h>
#include "chromanal.h"


/* globals for select_Canvas */
#define OUTSIDE 0
#define INSIDE 1

// spinsqueeze modes - used by fscale_Canvas_Wobj()
#define SS_NORMAL	0
#define SS_NOTHRESH	1
#define SS_MODE		2
static int SS_method = SS_NORMAL;

int min_dist;
Canvas *selected_canvas;
int previous_best;
int show_FLEXprobeframes=1;
int show_forInterpolation=0; // 1 = image data only, 2 = graphics, 0 = all

static BOOL canvasImageDrawEnabled   = TRUE; // display_Canvas() with intens(1)
static BOOL canvasImageEraseEnabled  = TRUE; // display_Canvas() with intens(0)
static BOOL canvasImageExposeEnabled = TRUE; // expose_Canvas()
static BOOL canvasFrameDrawEnabled   = TRUE; // display_Canvas_frame() with intens(1)
static BOOL canvasFrameEraseEnabled  = TRUE; // display_Canvas_frame() with intens(0)


void EnableCanvasImageDisplay(BOOL enableDraw, BOOL enableErase, BOOL enableExpose)
{
	canvasImageDrawEnabled   = enableDraw;
	canvasImageEraseEnabled  = enableErase;
	canvasImageExposeEnabled = enableExpose;
}

void EnableCanvasFrameDisplay(BOOL enableDraw, BOOL enableErase)
{
	canvasFrameDrawEnabled   = enableDraw;
	canvasFrameEraseEnabled  = enableErase;
}



#ifdef DEBUG

/*************************************************** debug_Canvas */

/*
 * BP - new function (CV 2.0) to display canvas
 * data.
 * Printfs info to stderr.
 *
 * Display is in format:

type: XXXXX
Wobj (type): XXXX (nn)
Visible: Yes/No
Plist: Yes/No

        parent ptr
           |
prev ptr - * - next ptr
           |
        patch ptr

 *
 */

static char *cantype_str[Caxis] = {
	"Cmetaphase",
	"Ckaryogram",
	"Cchromclass",
	"Cmetblob",
	"Cchromosome",
	"Cstack",
	"Cannotation",
	"Cideogram",
	"Cflex",
	"Cprint",
	"Ccomposite",
	"Cgraphic",
	"Cprobe",
	"Coverlap",
	"Ccounterstain",
	"Craw",
	"Ctag",
	"Cgraphelement",
	"Cuprobe",
	"Cratiohilite",
	"Ccentromere",
	"Caxis"
};


void
debug_Canvas(char *title, Canvas *canvas)
{
	fprintf(stderr, "\n", title);

	if (title)
		fprintf(stderr, "%s\n", title);

	fprintf(stderr, "Canvas (type): %X", canvas);
	if ((canvas->type < 1) || (canvas->type > Caxis))
		fprintf(stderr, " (%d UNKNOWN)\n", canvas->type);
	else
		fprintf(stderr, " (%s)\n", cantype_str[canvas->type - 1]);

	if (canvas->Wobj)
		fprintf(stderr, "Wobj (type): %X (%d)\n",
					canvas->Wobj, canvas->Wobj->type);
	else
		fprintf(stderr, "Wobj: NONE\n");

	fprintf(stderr, "Visible: %s\n", canvas->visible ? "Yes" : "No");
	fprintf(stderr, "Plist: %s\n", canvas->data ? "Yes" : "No");

	if (canvas->parent)
		fprintf(stderr, "        %X\n", canvas->parent);
	fprintf(stderr, "           |\n");

	if (canvas->previous)
		fprintf(stderr, "%8X", canvas->previous);
	else
		fprintf(stderr, "        ");
	fprintf(stderr, " - * - ");
	if (canvas->next)
		fprintf(stderr, "%X\n", canvas->next);
	else
		fprintf(stderr, "\n");

	fprintf(stderr, "           |\n");
	if (canvas->patch)
		fprintf(stderr, "        %X\n", canvas->patch);
} /* debug_Canvas */

#endif



/*
 * 	CREATE_CANVAS
 *
 *	Basic canvas creation routine
 *	
 *	Allocates memory for Canvas structure copies in user
 *	supplied values and returns pointer to Canvas.
 *
 *	Note that frame and patchgrid structures are copied into
 *	the Canvas structure - not just their pointers.
 *
 *	Also data is a user defined structure ( variable size )
 *	So memory is allocated for this and memcpy() is used to 
 *	transfer the contents. This will only work if the user
 *	sets up data->size correctly - sizeof(user defined structure).
 *
 */

Canvas *
create_Canvas(short type, Cframe *frame, struct pgrid *patchgrid, short visible, WZObj *Wobj,
			Canvas *parent, Canvas *previous, Canvas *next, Canvas *patch, struct propertylist *data)
{
	Canvas *canvas;

	/* Allocate memory for canvas */
	canvas=(Canvas *) Malloc(sizeof(Canvas));
    memset(canvas, 0, sizeof(Canvas));

	/* copy details to canvas structure */

	canvas->type=type;

	/* check for NULL frame - naughty, naughty, naughty */
	if (!frame)
	{
		fprintf(stderr,"create_Canvas(): NULL frame!.\n");
		return(NULL);
/* exit(1); I don't think so. Mark! */
	}
	else 	/* otherwise copy user supplied frame */
		canvas->frame=*frame;

	/* If NULL patchgrid just fill with dummy values */
	if (!patchgrid)
	{
		canvas->patchgrid.type=61;
		canvas->patchgrid.style=0;
		canvas->patchgrid.k1=0;
		canvas->patchgrid.l1=0;
		canvas->patchgrid.k2=canvas->frame.width-1;
		canvas->patchgrid.l2=canvas->frame.height-1;
		canvas->patchgrid.spacing=1;
	}
	else	/* otherwise copy user supplied patchgrid */
		canvas->patchgrid=*patchgrid;

	canvas->visible=visible;
	canvas->Wobj=Wobj;
	canvas->parent=parent;
	canvas->previous=previous;
	canvas->next=next;
	canvas->patch=patch;

/* if no data then just set canvas data ptr to NULL */
	if (!data)
		canvas->data=NULL;
	else
	{
		/* otherwise allocate space for data and copy */
		canvas->data=(struct propertylist *) Malloc(data->size);

		/* data is user defined structure so memcpy used for copy */
		memcpy((char *)canvas->data, (char *)data, data->size);
	}

	/* return pointer to canvas */
	return(canvas);
}


/* 
 *	DESTROY_CANVAS_CHAIN_KERNEL
 *
 *	Total recursive destruction of canvas - destroys its patches
 *	AND any canvas attached to next canvas pointer !
 *	AND woolz object connected to canvas if destroy_Wobj is True
 *	Does not destroy parent, or previous canvasses in a chain.
 */

void
destroy_Canvas_chain_kernel(Canvas *canvas, short destroy_Wobj)
{
	if (!canvas)
		return;

	destroy_Canvas_chain_kernel( canvas->next, destroy_Wobj );
	destroy_Canvas_chain_kernel( canvas->patch, destroy_Wobj );

	if ((destroy_Wobj) && (canvas->Wobj))
		freeobj(canvas->Wobj);

	if (canvas->data)
		Free(canvas->data);

/* if this is first canvas in a chain of patches
 * reset the parent canvas patch field */
	if (canvas->parent && (canvas->parent->patch == canvas))
		canvas->parent->patch=NULL;
	else
/* otherwise reset the previous canvas next field */
		if (canvas->previous)
			canvas->previous->next=NULL;

	Free(canvas);
}	



/* 
 *	DESTROY_CANVAS_CHAIN
 *
 *	Total recursive destruction of canvas - destroys its patches
 *	AND any canvas attached to next canvas pointer !
 *	BUT not attached woolz object
 *	Does not destroy parent, or previous canvasses in a chain.
 */

void
destroy_Canvas_chain(Canvas *canvas)
{
	destroy_Canvas_chain_kernel(canvas, 0);
}


/* 
 *	DESTROY_CANVAS_CHAIN_AND_WOBJ
 *
 *	Total recursive destruction of canvas - destroys its patches
 *	AND any canvas attached to next canvas pointer !
 *	AND not attached woolz object
 *	Does not destroy parent, or previous canvasses in a chain.
 */

void destroy_Canvas_chain_and_Wobj(Canvas *canvas)
{
	destroy_Canvas_chain_kernel(canvas, 1);
}


/*
 *	DESTROY_CANVAS_KERNEL
 *
 *	Recursive canvas destruction - destroys canvas and its patches.
 *	Does not destroy canvas parent or any canvases attached to top
 *	level previous or next canvas pointers.
 */
 
void destroy_Canvas_kernel(Canvas *canvas, short destroy_Wobj)
{
	if (!canvas)
		return;
		
/* if this is first canvas in a chain of patches
   set the parent canvas patch field to point to
   the next canvas in the chain */

	if (canvas->parent && (canvas->parent->patch == canvas))
	{
		canvas->parent->patch=canvas->next;

		if (canvas->next)
			canvas->next->previous=NULL;
	}
	else
	{
/* otherwise modify canvas chain links */
		if (canvas->previous)
			canvas->previous->next=canvas->next;
		
		if (canvas->next)
			canvas->next->previous=canvas->previous;
	}

/* protect canvas siblings */
	canvas->previous=NULL;
	canvas->next=NULL;

/* destroy canvas and its children */
	destroy_Canvas_chain_kernel( canvas, destroy_Wobj );
}




/*
 *	DESTROY_CANVAS
 *
 *	Recursive canvas destruction - destroys canvas and its patches,
 *	BUT not attached woolz objects.
 *	Does not destroy canvas parent or any canvases attached to top
 *	level previous or next canvas pointers.
 */

void destroy_Canvas(Canvas *canvas)
{
	destroy_Canvas_kernel(canvas, 0);
}



/*
 *	DESTROY_CANVAS_AND_WOBJ
 *
 *	Recursive canvas destruction - destroys canvas and its patches,
 *	AND attached woolz objects.
 *	Does not destroy canvas parent or any canvases attached to top
 *	level previous or next canvas pointers.
 */

void destroy_Canvas_and_Wobj(Canvas *canvas)
{
	destroy_Canvas_kernel(canvas, 1);
}



/*
 *	CUT_CANVAS
 *
 *	cut canvas from canvas structure
 *
 *	- canvas modified :
 *		parent, previous and next fields become NULL
 *		
 *	- updates links in canvas structure from which the canvas 
 *	  was cut  i.e. closes the hole
 *	  
 */

void cut_Canvas(Canvas *canvas)
{
	if (!canvas)
		return;

	if (canvas->previous)
		canvas->previous->next=canvas->next;

	if (canvas->next)
		canvas->next->previous=canvas->previous;

	if (canvas->parent && canvas->parent->patch==canvas)
		canvas->parent->patch=canvas->next;

	canvas->parent=NULL;
	canvas->previous=NULL;
	canvas->next=NULL;
}


/*
 *	PASTE_CANVAS
 *
 *	paste a canvas into a canvas structure
 *
 *	- canvas modified:
 *		new parent field supplied by caller
 *		previous field becomes NULL - canvas pasted to parent.patch
 *		next field becomes original parent.patch
 */			

void paste_Canvas(Canvas *canvas, Canvas *new_parent)
{
	if (!canvas)
		return;

	canvas->parent=new_parent;
	canvas->previous=NULL;

	if (new_parent)
	{
		canvas->next=new_parent->patch;
		if (canvas->next)
			canvas->next->previous=canvas;

		new_parent->patch=canvas;
	}
	else
		canvas->next=NULL;
}


/*
 *	attach_Canvas
 *
 * This is derived from paste_Canvas and behaves
 * in exactly the same way except that the new
 * child is pated onto the end of the parent's
 * chain (paste_Canvas puts it at the beginning).
 *
 *	- canvas modified:
 *		new parent field supplied by caller
 *		next field becomes NULL
 *		previous field becomes last in chain
 */

void
attach_Canvas(Canvas *canvas, Canvas *new_parent)
{
	if (!canvas)
		return;

	canvas->parent = new_parent;
	canvas->next = NULL;

	if (new_parent)
	{
		Canvas *child;

/* If there is no chain, just attach the
 * new canvas to the parent. */
		if ((child = new_parent->patch) == NULL)
		{
			new_parent->patch = canvas;
			canvas->previous = NULL;
		}
		else
		{
/* There is a chain (of at least one) so
 * find the end of it. */
			while (child->next)
				child = child->next;

/* At this point, "child" is the last
 * canvas in the existing chain. Add
 * ours to the end. */
			child->next = canvas;
			canvas->previous = child;
		}
	}
	else
		canvas->previous = NULL;
}




/*
 *	SWAP_CANVAS
 *
 *	swaps the position of two canvases in a canvas structure
 *	 - breaks and rejoins canvas parent/previous/next links
 *	 - and swaps canvas.frame parent relative positions
 * 	   i.e. parent_site, posingrid, cpframe.dx and cpframe.dy
 */

void
swap_Canvas(Canvas *canvasA, Canvas *canvasB)
{
	Canvas *parentA, *parentB;
	short temp_parent_site, temp_posingrid;
	int temp_dx, temp_dy;


	/* check for NULL canvases - cannot swap */
	if ((canvasA==NULL) || (canvasB==NULL))
		return;

	/* note A and B's parents */
	parentA=canvasA->parent;
	parentB=canvasB->parent;

	/* cut A and B from canvas structure */
	cut_Canvas(canvasA);
	cut_Canvas(canvasB);

	/* swap A and B's parent relative origin data */
	temp_parent_site=canvasA->frame.parent_site;
	temp_posingrid=canvasA->frame.posingrid;
	temp_dx=canvasA->frame.cpframe.dx;
	temp_dy=canvasA->frame.cpframe.dy;

	canvasA->frame.parent_site=canvasB->frame.parent_site;
	canvasA->frame.posingrid=canvasB->frame.posingrid;
	canvasA->frame.cpframe.dx=canvasB->frame.cpframe.dx;
	canvasA->frame.cpframe.dy=canvasB->frame.cpframe.dy;

	canvasB->frame.parent_site=temp_parent_site;
	canvasB->frame.posingrid=temp_posingrid;
	canvasB->frame.cpframe.dx=temp_dx;
	canvasB->frame.cpframe.dy=temp_dy;

	/* reparent A and B */
	paste_Canvas(canvasA,parentB);
	paste_Canvas(canvasB,parentA);
}



/*
 *	COPY_CANVAS
 *
 *	creates a copy of a canvas structure including its patches 
 *      - BUT DOES NOT COPY THE ATTACHED WOOLZ OBJECTS - just their pointers
 *	
 *	- canvas returned :
 *		parent, previous and next fields are NULL
 *		frame - including parent_site and posingrid are unaltered
 *			user may need to change these manually
 */

Canvas *
dup_Canvas(Canvas *canvas, Canvas *parent, short copyWobj)
{
	Canvas *temp_canvas;
//	struct object *duplicate_obj_plus();

	if (!canvas)
		return(NULL);

	temp_canvas=create_Canvas(canvas->type, &canvas->frame,
						&canvas->patchgrid, canvas->visible,
						canvas->Wobj, parent,
						NULL,			/* previous */
						NULL,			/* next */
						NULL,			/* patch */
						canvas->data);

	if (copyWobj)
		if (canvas->Wobj)
			temp_canvas->Wobj = duplicate_obj_plus(canvas->Wobj);

	temp_canvas->next=dup_Canvas(canvas->next,parent,copyWobj);

	if (temp_canvas->next)
		temp_canvas->next->previous=temp_canvas;

	temp_canvas->patch=dup_Canvas(canvas->patch,temp_canvas,copyWobj);

	return(temp_canvas);
}


Canvas *copy_Canvas(Canvas *canvas)
{
	Canvas *temp_canvas, *temp_parent, *temp_previous, *temp_next;

	if (!canvas)
		return(NULL);

	temp_parent=canvas->parent;	canvas->parent=NULL;
	temp_previous=canvas->previous;	canvas->previous=NULL;
	temp_next=canvas->next;		canvas->next=NULL;

	temp_canvas=dup_Canvas( canvas, NULL, 0 );

	canvas->parent=temp_parent;
	canvas->previous=temp_previous;
	canvas->next=temp_next;

	return( temp_canvas );
}


/*
 * 	COPY_CANVAS_AND_WOBJ
 *
 *	same as copy_Canvas but also duplicates all Wobj's too
 */

Canvas *copy_Canvas_and_Wobj(Canvas *canvas)
{
	Canvas *temp_canvas, *temp_parent, *temp_previous, *temp_next;

	if (!canvas)
		return(NULL);

	temp_parent=canvas->parent;	canvas->parent=NULL;
	temp_previous=canvas->previous;	canvas->previous=NULL;
	temp_next=canvas->next;		canvas->next=NULL;

	temp_canvas=dup_Canvas( canvas, NULL, 1 );

	canvas->parent=temp_parent;
	canvas->previous=temp_previous;
	canvas->next=temp_next;

	return( temp_canvas );
}




/*
 *	MODIFY_ORIGIN
 *
 *	modify display origin based on canvas frame parent_site
 */

void
modify_origin(int *newX, int *newY, Canvas *canvas)
{
	Cframe cf, pf;
	struct pgrid pg;
	int X, Y;
	int pxcent,pycent,pwidth,pheight;
	int cxcent,cycent,cwidth,cheight;


	// if its a text canvas, update the width as the scale may have changed and the font is never scaled linearly by _makefont
	// need to do it here as the grid position is based on width, which we may change here
	if (canvas->Wobj && canvas->Wobj->type == 70)
	{
		setfont(((struct textdomain *)canvas->Wobj->idom)->font);
		setTrueFont(((struct textdomain *)canvas->Wobj->idom)->orientation);
		textsizeScale(((WZTextObj *)canvas->Wobj)->text, &canvas->frame.width, &canvas->frame.height, 8);

		if (canvas->frame.cpframe.scale != 8)
		{
			// this is a kludge to handle text that has been scaled via the reflection and scaling. It uses the canvas->frame.cpframe.scale value to do this.
			// not very nice but can't think of other way of doing it
			canvas->frame.width = (canvas->frame.width * canvas->frame.cpframe.scale * 1.1)/ 8;
			canvas->frame.height = (canvas->frame.height * canvas->frame.cpframe.scale)/ 8;
		}
	}

	X=*newX;
	Y=*newY;

	/* update display origin based on canvas origin */
	cf=canvas->frame;

	X+=cf.cpframe.dx;
	Y+=cf.cpframe.dy;

	/* update display origin based on parent_site */
	if (canvas->parent!=NULL) {

		pf=canvas->parent->frame;
		pg=canvas->parent->patchgrid;

		pxcent=pf.width/2;
		pycent=pf.height/2;
		pwidth=pf.width-1;
		pheight=pf.height-1;
		cxcent=cf.width/2;
		cycent=cf.height/2;
		cwidth=cf.width-1;
		cheight=cf.height-1;

		if (cf.parent_site & GRID) {
			if (pg.style==0) {	/* horizontal grid */
				X+=cf.posingrid * pg.spacing + pg.k1;
				Y+=pg.l1;
				pxcent=pg.spacing/2;
				pwidth=pg.spacing;
			}
			else {			/* vertical grid */
				X+=pg.k1;
				Y+=cf.posingrid * pg.spacing + pg.l1;
				pycent=pg.spacing/2;
				pheight=pg.spacing;
			}
		}

		if (cf.parent_site & RIGHT)
			X+=pwidth-cwidth;

		if (cf.parent_site & TOP)
			Y+=pheight-cheight;

		if (cf.parent_site & CENTRE) {
			X+=pxcent-cxcent;
			Y+=pycent-cycent;

			if (cf.parent_site & (LEFT|RIGHT))
				X-=pxcent-cxcent;
	
			if (cf.parent_site & (BOTTOM|TOP))
				Y-=pycent-cycent;
				
		}

	}

	/* return update values */
	*newX=X;
	*newY=Y;

}




/**************** reset_probe_frames() moved from pint *****************/

/*
 * reset_probe_frames
 *
 * Convenience routine to set the
 * frame parameters (size, origin etc)
 * to valid values based on the objects
 * contained within the canvas.
 * The canvas parameter must be a
 * Coverlap type.
 * The border_width specifies the amount that
 * the Coverlap canvas is bigger than the area
 * covered by its children. Normally set to
 * zero - but is non-zero for pasting probes
 * into a flex screen (provides a black border
 * around the objects).
 */

void
reset_probe_frames(Canvas *thiscan, int border_width)
{
	Canvas *subcan;
	struct object *subobj;
	Cframe *f;
	int minx, miny, maxx, maxy;

	if (thiscan == NULL)
		return;

	if ((thiscan->type != Coverlap) ||
		(thiscan->patch == NULL))
		return;

/* Get a list of all the child (probe
 * and counterstain) woolz objects. */
	subcan = thiscan->patch;

	if (subcan->Wobj == NULL)
		return;

	minx = subcan->Wobj->idom->kol1;
	miny = subcan->Wobj->idom->line1;
	maxx = subcan->Wobj->idom->lastkl;
	maxy = subcan->Wobj->idom->lastln;

	while (subcan = subcan->next)
		if (subcan->Wobj)
		{
			if (subcan->Wobj->idom->kol1 < minx)
				minx = subcan->Wobj->idom->kol1;
			if (subcan->Wobj->idom->line1 < miny)
				miny = subcan->Wobj->idom->line1;
			if (subcan->Wobj->idom->lastkl > maxx)
				maxx = subcan->Wobj->idom->lastkl;
			if (subcan->Wobj->idom->lastln > maxy)
				maxy = subcan->Wobj->idom->lastln;
		}

/* First set the frame for the Coverlap canvas. */
	f = &thiscan->frame;

	f->width = 8*(maxx - minx + 1 + 2*border_width);
	f->height = 8*(maxy - miny + 1 + 2*border_width);
	f->aspect = 100;
	f->parent_site = FREE;
	f->clip = 0;
	f->arena = GREYIM;
	f->colour = 255;
	f->drawmode = INVERT;

	f->cpframe.dx = 8*(minx - border_width);
	f->cpframe.dy = 8*(miny - border_width);
	f->cpframe.ox = minx - border_width;
	f->cpframe.oy = miny - border_width;
	f->cpframe.ix = 1;
	f->cpframe.iy = 1;

#if 0
printf("\n*** reset_probe_frames (border = %d)\n", border_width);
printf("\tCoverlap w/h = %d/%d, x/y = %d/%d\n",
	f->width, f->height, f->cpframe.dx, f->cpframe.dy);
#endif

/* Now scan through the sub-canvases and set
 * their frames based on their own objects
 * and offset from the origin of the parent
 * frame. */
	subcan = thiscan->patch;
	while (subcan)
	{
		f = &subcan->frame;

		if (subobj = subcan->Wobj)
		{
			f->width = 8*(subobj->idom->lastkl - subobj->idom->kol1 + 1);
			f->height = 8*(subobj->idom->lastln - subobj->idom->line1 + 1);
			f->aspect = 100;
			f->parent_site = FREE;
			f->clip = 0;
			f->arena = GREYIM;
			f->colour = 255;
			f->drawmode = INVERT;

			f->cpframe.dx = 8*(subobj->idom->kol1 - minx + border_width);
			f->cpframe.dy = 8*(subobj->idom->line1 - miny + border_width);
			f->cpframe.ox = subobj->idom->kol1;
			f->cpframe.oy = subobj->idom->line1;
			f->cpframe.ix = 1;
			f->cpframe.iy = 1;
			f->cpframe.scale = 8;
			f->cpframe.type = 60;

#if 0
printf("\t\tsubcan w/h = %d/%d, x/y = %d/%d\n",
	f->width, f->height, f->cpframe.dx, f->cpframe.dy);
#endif
		}

		subcan = subcan->next;
	}
} /* reset_probe_frames */


void setFLEXprobeframes(int i)
{
	show_FLEXprobeframes=i;
}
int getFLEXprobeframes()
{
	return show_FLEXprobeframes;
}



static bool OkForInterpolation(Canvas* can)
{
	switch (can->type)
	{
	case Cmetaphase:
	case  Ckaryogram:
	case  Cchromclass:
	case  Cmetblob:
	case  Cchromosome:
	case  Cstack:
//	case  Cannotation:
//	case  Cideogram:
	case  Cflex:
	case  Cprint:
	case  Ccomposite:
//	case  Cgraphic:
	case  Cprobe:
	case  Coverlap:
	case  Ccounterstain:
	case  Craw:
//	case  Ctag:
//	case  Cgraphelement:
	case  Cuprobe:
//	case  Cratiohilite:
//	case  Ccentromere:
//	case  Caxis:
//	case  Ccghratkar:
//	case  Cchromprof:
//	case  Ccellprof:
//	case  Cslideprof:
//	case  Cchromcount:
//	case  Cconf95prof:
//	case  Cconf99prof:
//	case  Cfetal:
	case  Cmetblobplus:
	case  Cchromosomeplus:
//	case  Cboundary:
//	case  Cmetanno:
//	case  CnumberingMain:
//	case  Cnumbering:
//	case  Ccross:
//	case  CColorBoundary:
//	case  CCentromereTargetLeft:
//	case  CCentromereTargetRight:
		return true;
	default: return false;
	}
}

/*
 *	DISPLAY_WOBJ
 *
 *	internal routine - called by recursive_display() and recursive_expose()
 *	to draw woolz object held in canvas.
 *
 * BP 9/15/94	Accomodate 'proper' drawing of probe objects. This is done
 * by separating the stages which fill the draw buffer and display the
 * draw buffer. It is assumed that only Coverlaps will be displayed - no
 * other means of displaying a probe image is supported (ie it is not
 * possible to display probe/counterstain objects separately).
 *
 * BP 10/2/94	Provide for probes on bright flex by filling rectangle
 * to white if appropriate.
 *
 * BP - note: the whole canvas business could be speeded up considerably
 * if erasing just clears a rectangle surrounding the object (we already
 * have exposure events in most places to refresh other objects close to
 * an erased one). Its crazy that erasing objects from the screen takes a
 * percievable amount of time (in fact, its crazy that DRAWING them does -
 * but thats another story). We could lose all this 'plane 6', 'invert_
 * intens()' stuff, too.....
 */

void
display_Wobj(Canvas *canvas, int _X, int _Y, int S, int A, Canvas *topcanvas)
{
	struct pframe f;
	Cframe cf;
	DDGS *dg;
//	int oldmarker;
	short topdraw;
	int x, y, w, h;
	char *can_buffer = NULL;
	char *can_mask = NULL;

	if (show_forInterpolation== 1 && !OkForInterpolation(canvas))
		return;
	else if (show_forInterpolation== 2 && OkForInterpolation(canvas))
		return;

	unsigned char *vdata, *mdata;
	int dispx = 0;
	int dispy = 0;
	int dispw = 0;
	int disph = 0;
	double dispscale, sc;
	unsigned char *maskbuf = NULL;

	cf = canvas->frame;

	if(canvas->type == CColorBoundary)
	{
		int kk = 0;
		lwidth(2);
	}

	/* Special consideration for probe images
	 * in GREYIM - these are drawn via their
	 * Coverlap. In MARKIM (colourised) probe
	 * objects are drawn as usual. Also check
	 * for NULL object. */
	switch (cf.arena) {
	case OVLYIM: 
	case MARKIM:
		if ((canvas->type == Coverlap) ||
			(canvas->type == Cfetal))
			return;
		break;

	case GREYIM:
		/* If its a discrete probe object canvas, do
		 * nothing - only Coverlaps can be displayed
		 * (their children are dealt with in one go). */
		if ((canvas->type == Ccounterstain) ||
			(canvas->type == Cprobe) ||
			(canvas->type == Cuprobe))
			return;

		/* Check for NULL Wobj - note that this doesn't
		 * apply for Coverlaps (its their children we
		 * are interested in, kinda like Michael J!). */
		if ((canvas->type != Coverlap) && (canvas->type != Cfetal))
			if (canvas->Wobj == NULL)
				return;
		break;
	}


	if (canvas->Wobj &&canvas->Wobj->type == 70)
	{
		/* Set ddgs font for text object. */
		setfont(((struct textdomain *)canvas->Wobj->idom)->font);
		setTrueFont(((struct textdomain *)canvas->Wobj->idom)->orientation);
		// text is now scaled via scaling and reflection by setting the canvas->frame.cpframe.scale, so use the canvas scale here
		f.scale = canvas->frame.cpframe.scale;
	}
	else
		f.scale = S; // otherwise use the top level scale for all other objects

	/* set object coordinates origin */
	f.ox = cf.cpframe.ox;
	f.oy = cf.cpframe.oy;


	/* derived ddgs coordinates origin */
	f.dx = (_X*S + 4) >> 3;
	f.dy = (_Y*S + 4) >> 3;

	/* invert object if requested (should never happen now i.e ix/iy always 1) */
	f.ix = cf.cpframe.ix;
	f.iy = cf.cpframe.iy;

	dg = getcurdg();
	topdraw = topcanvas->frame.drawmode;
	if(topcanvas->type == Ccghratkar)
	{
		dg->ddgsImageFormat.MaxX = topcanvas->frame.width / 8;
		dg->ddgsImageFormat.MaxY = topcanvas->frame.height / 8;
		selectddgs(dg);
	}

	x = f.dx;
	y = f.dy;
	w = (canvas->frame.width*S + 4) >> 3,
	h = (canvas->frame.height*S + 4) >> 3;



	/* BP - only need to clear area of window to
	 * background colour if we are erasing... */
	if ( (dg->cur_intensity == 0) && (cf.arena != OVLYIM) &&
		 ((canvas->type == Coverlap) ||
		  (canvas->Wobj && ((canvas->Wobj->type == 1) ||
		  (canvas->Wobj->type == 70)))) )
	{
		if (topdraw == INVERT)
			invert_erase(x, y, w, h);
		else
			erase(x, y, w, h);
		return;
	}

	if (cf.clip)
		window(_X*S/8, (_X + cf.width - 1)*S/8,
				_Y*S/8, (_Y + cf.height - 1)*S/8);

	/* Handle drawing of brightfield on fluorescent and vice-versa. */
	if ((dg->cur_intensity == 1) && (cf.arena == GREYIM))
		if ((canvas->type == Coverlap) || (canvas->type == Cfetal) ||
			(canvas->Wobj && (canvas->Wobj->type == 1)))
		{
			if ((topdraw == INVERT) && (cf.drawmode == COPY))
			{
				if (canvas->type != Cideogram)
					{
					if (show_FLEXprobeframes==0)
						{ /* black out object background */
						invert_erase(x, y, w, h);
						}
					else		
						{ /* white out object background */
						erase(x, y, w, h);
						}
					}
			}

			if ((topdraw == COPY) && (cf.drawmode == INVERT))
			{
				/* black out object background */
				if ((canvas->type != Cideogram) || ((canvas->type == Coverlap) && (topcanvas->type == Cflex)))
					{
					if (show_FLEXprobeframes==0)
						{ /* white out object background */
						erase(x, y, w, h);
						}
					else  /* black out object background */
						{
						invert_erase(x, y, w, h);
						}
					}
			}
		}
	/* Make sure we draw inverted objects
	 * correctly, AND erase stuff on inverted
	 * images correctly. */
	if ( (cf.drawmode == INVERT) ||
		 ((topdraw == INVERT) && (dg->cur_intensity == 0)) )
		invert_intens();

	/* Now do the actual drawing. */
	switch (cf.arena) {
	case OVLYIM: 
		opicframe(canvas->Wobj,&f);
		break;

	case MARKIM:
		mcolour(cf.colour);
		mpicframe(canvas->Wobj,&f);
		break;

	case GREYIM:
		switch (canvas->type) {
		case Coverlap :
			{
				// get the background colour, used to generate the canvas image data and to threshold out the display mask from the image data
				unsigned int col;
				ddgsGetInvertBGColour(&col);

				if ((can_buffer = (char *)draw_overlap_Canvas_buffer(canvas, dg, S, &x, &y, &w, &h, col)) != NULL)
				{
					/* mask image */
					if ((can_mask = (char *)draw_overlap_mask_buffer((unsigned int *)can_buffer, w, h, col)) != NULL)
					{
						dispx = x;
						dispy = dg->ddgsImageFormat.MaxY - y; //AK - carefull here, - to display probe images properly requires fliping in Y direction
						dispw = w;
						disph = h;
						//this takes care of scaling and translation (scrolling)
						imagetoscreen (&dispx,&dispy,&dispw,&disph,&dispscale, &sc,FALSE);
						x = y = 0;

						nfastpixobj24(x,y, w, h, dispx,dispy,dispw,disph,(unsigned char *)can_buffer, (unsigned char *)can_mask, GREYIM);
					}
				}
			}
			break;

		case Cfetal :
			if ((can_buffer = (char *)draw_fetal_Canvas_buffer(canvas,
					dg, S, &x, &y, &w, &h)) != NULL)
				fastpixobj24(x, dg->maxy - y, w, h, (unsigned char *)can_buffer, NULL);
			break;

#ifndef USE_OLD_IDEOGRAMS
		case Cideogram:
		{
			if (IsCanvasNewIdeogram(canvas)) 
			{
			// Draw the ideogram from the data in the patch canvas's property
			// list rather than from the Wobj, which is there for backward
			// compatibility.
// Show the underlying Wobj as well, at least while testing.
//picframe(canvas->Wobj,&f);
//break;
				struct propertylist *pPL = canvas->patch->data;

				IdeoPolySet *pPolySet = new IdeoPolySet((char*)pPL + sizeof(pPL->size));

				BOOL backgroundIsBlack = FALSE;
				// For probe images, a frame colour of 0x421 is black - see set_invert_canvas_BG_col().
				//if (topcanvas->frame.colour == 0x421) backgroundIsBlack = TRUE;
				if (topdraw == INVERT) backgroundIsBlack = TRUE;


				DisplayIdeogram(pPolySet, f.scale/*canvas->frame.cpframe.scale*/,
				                          x + w/2, y + h/2, backgroundIsBlack);

				delete pPolySet;
			}
			else picframe(canvas->Wobj, &f);
		}
		break;
#endif

		default :
			picframe(canvas->Wobj,&f);
			break;
		}

		break;

	default:
		break;
	}

	/* Restore current ddgs drawing mode */
	restore_intens();
	if(canvas->type == CColorBoundary)
	{
		int kkk = 0;
		lwidth(1);
	}

	if (cf.clip)
		window(dg->minx << dg->xshift, dg->maxx << dg->xshift,
				dg->miny << dg->yshift, dg->maxy << dg->yshift);
} /* display_Wobj */



/* 
 *	RECURSIVE_DISPLAY
 *
 *	Guts of canvas display - internal routine for canvas.c
 *      Users should always call display_Canvas() or display_Canvas_chain()
 */

void
recursive_display(Canvas *canvas, int X, int Y, int S, int A, int V, Canvas *topcanvas)
{
	int oldX, oldY;
//	DDGS *dg;

/* Keep X and Y for canvas siblings */
	oldX = X;
	oldY = Y;

/* Do nothing if NULL canvas. */
	if (!canvas)
		return;

/* Only display canvas if it is visible. */
	if ((canvas->visible & 1) && (V & 1))
	{
		/* update display origin based on parent_site */
		modify_origin(&X, &Y, canvas);

		/* display this canvas Wobj : */
		if (!(canvas->visible & 2)){

			display_Wobj(canvas, X, Y, S, A, topcanvas);
			}

		/* display canvas children */
		recursive_display(canvas->patch, X, Y, S, A, V, topcanvas);
	}

/* display canvas siblings */
	recursive_display(canvas->next, oldX, oldY, S, A, V, topcanvas);
} /* recursive_display */




/*
 *	DISPLAY_CANVAS_CHAIN
 *
 *	Total recursive display of canvas - displays its patches
 *	AND any canvas attached to next canvas pointer !
 *	Does not display parent.
 */

void display_Canvas_chain(Canvas *canvas)
{
	Canvas*	parent;
	int X,Y,S,A,V;
	Canvas *topcanvas;

	if (!canvas)
		return;

/* offset canvas origin depending on ancestor canvases 
   and maintain highest canvas scale factor and aspect */

	X=Y=0;
	S=canvas->frame.cpframe.scale;
	A=canvas->frame.aspect;
	V=canvas->visible;
	topcanvas=canvas;

	parent=canvas->parent;
	

	while (parent)
	{
/* update display origin based on parent_site */
		modify_origin(&X,&Y,parent);

		S=parent->frame.cpframe.scale;
		A=parent->frame.aspect;
		V=parent->visible;
		topcanvas=parent;

		parent=parent->parent;
	}


/* display the canvas */
	recursive_display(canvas,X,Y,S,A,V,topcanvas);
}



/*
 *	DISPLAY_CANVAS
 *
 *	Recursive canvas display - displays canvas and its patches.
 *	Does not display canvas parent or any canvases attached to top
 *	level next canvas pointer.
 */

BOOL display_Canvas(Canvas *canvas)
{
	DDGS *pDG = getcurdg();
	if (pDG && pDG->cur_intensity != 0)
	{	// Return if canvas drawing is disabled
		if (!canvasImageDrawEnabled) return FALSE;
	}
	else
	{	// We are erasing, so return if this is disabled
		if (!canvasImageEraseEnabled) return FALSE;
	}


	Canvas *savenext;

	if (!canvas)
		return FALSE;

	savenext=canvas->next;		/* break next link so    */
	canvas->next=NULL;		/* we dont display siblings */

	display_Canvas_chain(canvas);	/* display all children  */

	canvas->next=savenext;		/* restore next link     */

	return TRUE;
}



/*
 *	FSCALE_CANVAS_WOBJ
 *
 *	Floating point scale of canvas object
 *
 *	All objects scaled about their midpoints
 *	Uses Woolz squeeze() routine but also handles convex hulls
 *	rectangles and text.
 *
 *	NOTE1 text objects are converted permanently into type 1 grey objects
 *
 *	NOTE2 object propertylists are not maintained
 */
#ifdef WIN32
void
fscale_Canvas_Wobj(Canvas *canvas, double scale)
#endif
#ifdef i386
void
fscale_Canvas_Wobj(canvas, scale)
Canvas *canvas;
double scale;
#endif
{
//	Canvas *parent;
	struct object *scaled_Wobj;
	struct irect *rdom;
	int i, type, cx, cy;
	double rads=0.0;	// no rotation - just scaling


	if (!canvas)
		return;

	if (!canvas->Wobj)
		return;
		
	switch (type = canvas->Wobj->type) {
		case 1:	 /* grey object */

			switch (SS_method)
			{
			// scale object - allow different techniques to be used 
			// - specified by global canvas_spinsqueeze_method
			default:
			case SS_NORMAL:
				scaled_Wobj=(struct object *)squeeze(canvas->Wobj, scale, scale);
				break;
			case SS_NOTHRESH:
				// used to stop spin_squeeze removing pixels whose grey
				// values reduce to zero when averaged - e.g. perimeter pixels
				scaled_Wobj=(struct object *)nothresh_spinsqueeze(canvas->Wobj, &rads, &scale, &scale);
				break;
			case SS_MODE:
				// used on single colour objects e.g. mfish pseudocolour objects
				// - this prevents breakdown of colours at edges when averaged
				scaled_Wobj=(struct object *)mode_spinsqueeze(canvas->Wobj, &rads, &scale, &scale);
				break;
			}

			/* restore object type if necessary */
			canvas->Wobj->type=type;

			/* free original object */	
			freeobj(canvas->Wobj);

			/* attach scaled object to canvas */
			canvas->Wobj=scaled_Wobj;
			break;

		case 12: /* convex hull - has polygon idom so treat as polygon */
			canvas->Wobj->type=10;

		case 10: /* polygon */
		case 11: /* boundary list */
		case 100:/* circle */

			/* scale object */
			scaled_Wobj=(struct object *)squeeze(canvas->Wobj,scale,scale);

			/* restore object type if necessary */
			canvas->Wobj->type=type;

			/* free original object */	
			freeobj(canvas->Wobj);

			/* attach scaled object to canvas */
			canvas->Wobj=scaled_Wobj;
			break;

		case 20: /* rectangle */
			rdom=(struct irect *)canvas->Wobj->idom;

			cx=(rdom->irk[0] + rdom->irk[2])/2;
			cy=(rdom->irl[0] + rdom->irl[1])/2;

			for (i=0;i<4;i++) {
				rdom->irk[i] = (short)(cx - ((double)(cx - rdom->irk[i]) * scale));
				rdom->irl[i] = (short)(cy - ((double)(cy - rdom->irl[i]) * scale));
			}
			break;
			
		case 70: /* text object */

			/* No longer convert text to type 1 object,
			 * because we can arbitrarily font.
			 * I would hope that fscale_Canvas is never,
			 * ever used anyway.... */
			canvas->frame.cpframe.scale = (int)(scale*(double)canvas->frame.cpframe.scale);

#if 0
			/* convert text to type 1 grey object */
			scaled_Wobj=(struct object *)string2obj(canvas->Wobj);

			freeobj(canvas->Wobj);

			/* scale text */
			if (scaled_Wobj)
			{
				canvas->Wobj=
					(struct object *)squeeze(scaled_Wobj,scale,scale);
				freeobj(scaled_Wobj);
			}
			else
				canvas->Wobj=NULL;

			parent = canvas->parent;
			while (parent->parent)
				parent = parent->parent;
			canvas->frame.drawmode = parent->frame.drawmode;
#endif

			break;

		default:
			break;
	}
}




/* 
 *	RECURSIVE_FSCALE
 *
 *	Guts of floating point canvas scaling - internal routine for canvas.c
 *      Users should always call fscale_Canvas() or fscale_Canvas_chain()
 */

#ifdef WIN32
void
recursive_fscale(Canvas *canvas, double scale)
#endif
#ifdef i386
void
recursive_fscale(canvas, scale)
Canvas *canvas;
double scale;
#endif
{
//	DDGS *dg;
	Cframe *pf;
	struct pgrid *pg;

	if (!canvas)
		return;


	if (IsCanvasNewIdeogram(canvas))
	{
		// Keep existing rotation, but need to find out what it is first.
		float oldIdeoScale, rotAng;
		struct propertylist *pPL = canvas->patch->data;

		GetValuesFromIdeoPolySetBuffer(((char*)pPL + sizeof(pPL->size)), 
		                               &oldIdeoScale, &rotAng);


		// Note that inside RotateScaleNewIdeogramCanvas(), 'scale' is
		// multiplied by the existing ideogram scale to get the new ideogram scale.
		Canvas *pNewCanvas = RotateScaleNewIdeogramCanvas(canvas, (float)rotAng, (float)scale);


		// RotateScaleNewIdeogram() scales the ideogram around its centre.
		// However, recursive_fscale() seems to scale around the frame origin,
		// so compensate for this.
		pNewCanvas->frame.cpframe.dx = canvas->frame.cpframe.dx * scale;
		pNewCanvas->frame.cpframe.dy = canvas->frame.cpframe.dy * scale;


		// New canvas needs to be inserted in the canvas heirachy in place of
		// the old canvas, for the recursion to continue successfully. It can't
		// just be pasted in. It has a different address though, so need to
		// sort out the pointers.
		if (canvas->parent)   canvas->parent->patch  = pNewCanvas;
		if (canvas->previous) canvas->previous->next = pNewCanvas;
		if (canvas->next)     canvas->next->previous = pNewCanvas;

		pNewCanvas->parent   = canvas->parent;
		pNewCanvas->previous = canvas->previous;
		pNewCanvas->next     = canvas->next;


		destroy_Canvas_and_Wobj(canvas);

		canvas = pNewCanvas;
	}
	else
	{
		/* scale this canvas */
		pf=&canvas->frame;
		pg=&canvas->patchgrid;

		if (canvas->Wobj)
		{
			/* scale canvas object */
			fscale_Canvas_Wobj( canvas, scale );

			/* Handle the fact that objects are scaled around their mid-points.
			   NOTE - this code relies on the user setting up the canvas frame
			   and cpframe dimensions correctly. In the following lines we /8
			   for ddgs scale up and /2 for centre adjust => /16 overall */

			pf->cpframe.ox += pf->cpframe.ix * (pf->width * (1.0 - scale)) / 16;
			pf->cpframe.oy += pf->cpframe.iy * (pf->height * (1.0 - scale)) / 16;
		}
	
		/* scale canvas size -  add 16 to handle floating
		   point errors in ox, oy, width and height */
		pf->width  = (int)(pf->width * scale + 16);
		pf->height = (int)(pf->height * scale + 16);

		/* scale canvas position */
		pf->cpframe.dx *= scale;
		pf->cpframe.dy *= scale;

		pg->spacing*=scale;
		pg->k1*=scale;
		pg->k2*=scale;
		pg->l1*=scale;
		pg->l2*=scale;

		/* scale canvas children */
		recursive_fscale(canvas->patch, scale );
	}

	/* scale canvas siblings */
	recursive_fscale(canvas->next, scale );
}




/*
 *	FSCALE_CANVAS_CHAIN
 *
 *	Total recursive floating point scale of canvas - scales its patches
 *	AND any canvas attached to next canvas pointer !
 *	Does not scale parent.
 */

#ifdef WIN32
void
fscale_Canvas_chain(Canvas *canvas, double scale)
#endif
#ifdef i386
void
fscale_Canvas_chain(canvas, scale)
Canvas *canvas;
double scale;
#endif
{
	if (!canvas)
		return;

/* scale the canvas */
	recursive_fscale(canvas, scale );
}




/*
 *	FSCALE_CANVAS
 *
 *	Recursive floating point canvas scaling - scales canvas and its patches.
 *	Does not scale canvas parent or any canvases attached to top
 *	level next canvas pointer.
 */

#ifdef WIN32
void
fscale_Canvas(Canvas *canvas, double scale)
#endif
#ifdef i386
void
fscale_Canvas(canvas, scale)
Canvas *canvas;
double scale;
#endif
{
	Canvas *savenext;

	if (!canvas)
		return;

	savenext=canvas->next;		/* break next link so    */
	canvas->next=NULL;		/* we dont scale siblings */

	fscale_Canvas_chain(canvas, scale);	/* scale all children  */

	canvas->next=savenext;		/* restore next link     */
}



void nothresh_fscale_Canvas(Canvas *canvas, double scale)
{
	SS_method = SS_NOTHRESH;
	
	fscale_Canvas(canvas,scale);

	SS_method = SS_NORMAL;
}


void mode_fscale_Canvas(Canvas *canvas, double scale)
{
	SS_method = SS_MODE;
	
	fscale_Canvas(canvas,scale);

	SS_method = SS_NORMAL;
}


/*
 *		C O N V E R T D G F
 *		-- introduced to accommodate large format cameras
 */
void convertDGF(DDGSframe *dgf)
{
	DDGS *dg;
	//int xoffset,yoffset;
	dg = getcurdg();
	int x,y,w,h;
	double scale, sc;

	x = dgf->x >> 3;
	y = (dgf->y >> 3);
	w = dgf->width >> 3;
	h = dgf->height >> 3;

	imagetoscreen (&x,&y,&w,&h,&scale, &sc,TRUE);	//get new coordinates

	dgf->x = x << 3;
	dgf->y = y << 3;
//	dgf->width = w << 3; // rounding errors this way
	dgf->width = (dgf->width * scale);
//	dgf->height = h << 3;
	dgf->height = (dgf->height * scale);

	dgf->midx = dgf->x + dgf->width / 2;
	dgf->midy = dgf->y + dgf->height / 2;
}

/*
 *
 *	GET_CANVAS_DDGSFRAME
 *
 *	returns canvas frame DDGS coordinates for an individual canvas :
 *
 *		x , y , midx, midy, width, height
 */

#ifdef WIN32
void
get_Canvas_DDGSframe(Canvas *canvas, DDGSframe *dgf)
#endif
#ifdef i386
void
get_Canvas_DDGSframe(canvas, dgf)
Canvas *canvas;
DDGSframe *dgf;
#endif
{
	Canvas*	parent;
	Cframe cf;
	int X,Y,S,A;

	if (!canvas)
		return;

/* offset canvas origin depending on ancestor canvases 
 * and maintain highest canvas scale factor and aspect */

	X=Y=0;
	S=canvas->frame.cpframe.scale;
	A=canvas->frame.aspect;

	parent=canvas->parent;

	while (parent)
	{
/* update display origin based on parent_site */
		modify_origin(&X,&Y,parent);

		S=parent->frame.cpframe.scale;
		A=parent->frame.aspect;

		parent=parent->parent;
	}

/* update display origin based on parent_site */
	modify_origin(&X,&Y,canvas);

	cf=canvas->frame;

	/* Handle strange aspect ratios */
	dgf->x= X*S/8;
	dgf->y= Y*S/8;
	dgf->midx= (X+cf.width/2)*S/8;
	dgf->midy= (Y+cf.height/2)*S/8;
	dgf->width= (cf.width)*S/8;
	dgf->height= (cf.height)*S/8;
}



/*
 *
 *	CANVAS_WOBJ_PFRAME
 *
 *	returns derived pframe struct of canvas object :
 *	- this could be used by routines such as picframe
 *
 *	struct pframe *f;
 *	Canvas *canvas;
 *
 *		f=Canvas_Wobj_pframe(canvas);
 *		if (f!=NULL)
 *			picframe(canvas->Wobj,f);
 *
 */

#ifdef WIN32
struct pframe *
Canvas_Wobj_pframe(Canvas *canvas)
#endif
#ifdef i386
struct pframe *
Canvas_Wobj_pframe(canvas)
Canvas *canvas;
#endif
{
	Canvas*	parent;
	Cframe cf;
	int X,Y,S,A;
	struct pframe *f;

	if (!canvas)
		return(NULL);

	cf = canvas->frame;

	/* offset canvas origin depending on ancestor canvases 
	 * and maintain highest canvas scale factor and aspect */

	X=Y=0;
	S=canvas->frame.cpframe.scale;
	A=canvas->frame.aspect;

	parent=canvas->parent;

	while (parent)
	{
		/* update display origin based on parent_site */
		modify_origin(&X,&Y,parent);

		S=parent->frame.cpframe.scale;
		A=parent->frame.aspect;

		parent=parent->parent;
	}

	/* update display origin based on parent_site */
	modify_origin(&X,&Y,canvas);

	cf = canvas->frame;

	/* create pframe f for Wobj based on canvas->frame */

	/* allocate structure and initialise */
	f = makepframe(8,0,0,0,0);

	/* set object coordinates origin */
	f->ox = cf.cpframe.ox;
	f->oy = cf.cpframe.oy;

	/* set ddgs scale factor */	
	f->scale = S;

	/* derived ddgs coordinates origin */
	f->dx = X*S/8;
	f->dy = Y*S/8;

	/* invert object if requested */
	f->ix = cf.cpframe.ix;
	f->iy = cf.cpframe.iy;

	return(f);
}



/*
 *	DISPLAY_CANVAS_FRAME
 *
 *	display individual canvas frame
 *
 *	arena is either GREYIM, OLVLYIM or MARKIM
 *
 *	col is    0-255 for GREYIM
 *		  0-15  for MARKIM
 *		ignored for OVLYIM
 *
 *	drawmode is usually COPY for brightfield images
 *			    INVERT for fluorescent
 *			    others will appear for COLOURIM
 */

BOOL
display_Canvas_frame(Canvas *canvas, short arena, short col, int drawmode)
{
	DDGS *pDG = getcurdg();
	if (pDG && pDG->cur_intensity != 0)
	{	// Return if frame drawing is disabled
		if (!canvasFrameDrawEnabled) return FALSE;
	}
	else
	{	// We are erasing, so return if this is disabled
		if (!canvasFrameEraseEnabled) return FALSE;
	}


	if (!canvas)
		return FALSE;

	DDGSframe dgf;
	get_Canvas_DDGSframe(canvas, &dgf);

		//changes introduced to accomodate large format cameras
	convertDGF (&dgf);	//image to screen

	moveto(dgf.midx, dgf.midy);
		
/* set drawing mode */
	if (drawmode==INVERT)
		invert_intens();
			
/* draw the frame */
	switch (arena) {
	case OVLYIM: 
		obox(dgf.width, dgf.height);
		break;

	case MARKIM:
		mcolour(col);
		mbox(dgf.width, dgf.height);
		break;

	case GREYIM:
	default:
		colour(col);
		box(dgf.width, dgf.height);
		break;
	}

	restore_intens();

	return TRUE;
}



/*
 * SELECT_CANVAS
 *
 *	Searches given canvas structure looking for the canvas closest to
 *	the supplied pointer coordinates fx, fy ( ddgs coordinates ).
 *	select_Canvas only checks canvases of the given types - supplied in
 *	an array of ntypes.
 *	If fx,fy is not inside any canvas select_Canvas returns NULL.
 *	If fx,fy is inside a number of overlapping canvases, the canvas with
 *	mid point closest to fx,fy is returned.
 *
 *	select_Canvas uses the recusive_find routine to traverse the canvas tree.
 */


void
recursive_find(Canvas *canvas, int X, int Y, int S, int A, short *types, int ntypes, int fx, int fy, int closest)
{
	Cframe cf;
	int minx,midx,maxx,distx;
	int miny,midy,maxy,disty;
	int distsq;
	int t, right_type;
	BOOL inside;
	

	if (!canvas)
		return;

/* search siblings */
	recursive_find(canvas->next, X, Y, S, A, types, ntypes, fx, fy, closest);

/* only search canvas if its visible */
	if (!(canvas->visible & 1))
		return;

/* update display origin based on parent_site */
	modify_origin(&X,&Y,canvas);

/* search children */
	if (canvas->type != Cfetal)
		recursive_find(canvas->patch, X, Y, S, A, types, ntypes, fx, fy, closest);

/* only check canvas of correct type */
	t=0;
	right_type=0;
	while ((t<ntypes) && (!right_type))
	{
		right_type=(types[t]==canvas->type);
		t++;
	}

	if (!right_type)
		return;

/* if closest flag is not set then
 * check fx,fy are inside canvas frame.
 * Also look for canvas closest to 
 * teh frame centre in  the
 * case of overlapping canvases */
	cf=canvas->frame;

/*
	minx= X*S/8;
  	miny= Y*S*A/800;
	maxx= minx + (cf.width)*S/8;
	maxy= miny + (cf.height)*S*A/800;
*/
/* BP - ignore aspect and use shift instead
 * of divide in attempt to make faster! */
	minx = (X*S) >> 3;
	miny = (Y*S) >> 3;
	maxx = minx + ((cf.width*S) >> 3);
	maxy = miny + ((cf.height*S) >> 3);

	inside=0;
	if (fx>=minx)
	  	if (fy>=miny)
    		if (fx<maxx)
      			if (fy<maxy)
					inside = 1;

	if (inside)
	{
/*
		midx= (minx+maxx)/2;
		midy=(miny+maxy)/2;
*/
/* Use shift instead of divide. */
		midx = (minx + maxx) >> 1;
		midy = (miny + maxy) >> 1;


		distx=(fx-midx);
		disty=(fy-midy);
		distsq=distx*distx+disty*disty;
		if ((distsq<min_dist) || (previous_best==OUTSIDE))
		{
/* nearest canvas so far */
			min_dist=distsq;
			selected_canvas=canvas;
			previous_best=INSIDE;
		}
	}
	else
		if ((closest) && (previous_best==OUTSIDE))
		{
			distx=0;
			if (fx<minx)
				distx=fx-minx;
			else
				if (fx>maxx)
					distx=fx-maxx;

			disty=0;
			if (fy<miny)
				disty=fy-miny;
			else 
				if (fy>maxy)
					disty=fy-maxy;
							
			distsq=distx*distx+disty*disty;
			if (distsq < min_dist)
			{
/* nearest canvas so far */
				min_dist=distsq;
				selected_canvas=canvas;
			}
		}					
}




Canvas *
select_Canvas(Canvas *canvas, short *types, int ntypes, int fx, int fy)
{
	Canvas*	parent;
	int X,Y,S,A;

	/* initialise global canvas select data */
	min_dist=200000000; /* very large 32 bit integer */
	selected_canvas=NULL;
	previous_best=OUTSIDE;

	if (!canvas)
		return(NULL);

/* offset canvas origin depending on ancestor canvases 
 * and maintain highest canvas scale factor and aspect */

	X=Y=0;
	S=canvas->frame.cpframe.scale;
	A=canvas->frame.aspect;

	parent=canvas->parent;

	while (parent)
	{
/* update display origin based on parent_site */
		modify_origin(&X,&Y,parent);

		S=parent->frame.cpframe.scale;
		A=parent->frame.aspect;

		parent=parent->parent;
	}

/* look for canvas */
	recursive_find(canvas,X,Y,S,A,types,ntypes,fx,fy, 0);

	return(selected_canvas);
}




/*
 * CLOSEST_CANVAS
 *
 *	Searches given canvas structure looking for the canvas closest to
 *	the supplied pointer coordinates fx, fy ( ddgs coordinates ).
 *	select_Canvas only checks canvases of the given types - supplied in
 *	an array of ntypes.
 *	If fx,fy is inside a number of overlapping canvases, the canvas with
 *	mid point closest to fx,fy is returned.
 *
 *	select_Canvas uses the recusive_find routine to traverse the canvas tree.
 */

Canvas *
closest_Canvas(Canvas *canvas, short *types, int ntypes, int fx, int fy)
{
	Canvas*	parent;
	int X,Y,S,A;

	/* initialise global canvas select data */
	min_dist=200000000; /* very large 32 bit integer */
	selected_canvas=NULL;
	previous_best=OUTSIDE;

	if (!canvas)
		return(NULL);

/* offset canvas origin depending on ancestor canvases 
 * and maintain highest canvas scale factor and aspect */

	X=Y=0;
	S=canvas->frame.cpframe.scale;
	A=canvas->frame.aspect;

	parent=canvas->parent;

	while (parent)
	{
/* update display origin based on parent_site */
		modify_origin(&X,&Y,parent);

		S=parent->frame.cpframe.scale;
		A=parent->frame.aspect;

		parent=parent->parent;
	}

/* look for canvas */
	recursive_find(canvas,X,Y,S,A,types,ntypes,fx,fy,1);

	return(selected_canvas);
}



/*
 * EXPOSE_CANVAS
 *
 *	Searches given canvas structure looking for any canvas containing a
 *	woolz object. If the frame of the Wobj overlaps the given DDGSframe dgf
 *	then the Wobj is redrawn. This routine is usually called after routines
 *	which have erased an object from a canvas display to tidy up holes
 *	left in overlapped objects.
 *
 *	expose_Canvas() calls the internal routine recursive_expose()
 *
 *	NOTE: This will only work if the woolz objects are totally contained
 *	      by the canvas frames.
 */

void
recursive_expose(Canvas *canvas, int X, int Y, int S, int A, DDGSframe *dgf, Canvas *topcanvas)
{
	Cframe cf;
	int minx,maxx;
	int miny,maxy;
	int oldX, oldY;
	DDGSframe obj_dgf, *combined_dgf;


/* Save X and Y for peer canvases. */
	oldX = X;	
	oldY = Y;

/* Do nothing if NULL canvas. */
	if (!canvas)
		return;

/* Only expose canvas if its visible. */
	if (canvas->visible & 1)
	{
		/* update display origin based on parent_site */
		modify_origin(&X,&Y,canvas);

		/* Set combined_dgf to dgf initially
		   If a woolz object is exposed - then the combined_dgf
		   will be the dgf combined with the woolz object dgf 
		   - this combined_dgf is used only to expose the children 
		   of this canvas */

		combined_dgf=(DDGSframe *)Malloc(sizeof(DDGSframe));
		*combined_dgf=*dgf;

		/* check for overlap between dgf frame and woolz object. */
		cf = canvas->frame;

		if ((canvas->Wobj != NULL) ||
			(canvas->type == Coverlap) || (canvas->type == Cfetal)) {			
			minx = X*S/8;
			if ((dgf->x) + (dgf->width) > minx)
			{
				miny = Y*S*A/800;
				if ((dgf->y) + (dgf->height) > miny)
				{
					maxx = minx + (cf.width)*S/8;
					if (dgf->x < maxx)
					{
						maxy = miny + (cf.height)*S*A/800;
				 	   	if (dgf->y < maxy) 
						{
							if (!(canvas->visible & 2))
								display_Wobj(canvas, X, Y, S, A, topcanvas);
						
							if ((canvas->type != Coverlap) &&
								(canvas->type != Cfetal))
							{

								/* generate combined dgf for exposing this
							   	object's canvas children - Coverlaps are
								a special case they have no Wobj */
	
								get_Canvas_DDGSframe(canvas, &obj_dgf);

								minx=dgf->x;
								miny=dgf->y;
								maxx=minx + dgf->width - 1;
								maxy=miny + dgf->height - 1;
						
								if (obj_dgf.x  < minx)
									minx=obj_dgf.x;
								if (obj_dgf.x + obj_dgf.width > maxx)
									maxx=obj_dgf.x + obj_dgf.width;
								if (obj_dgf.y < miny)
									miny=obj_dgf.y;
								if (obj_dgf.y + obj_dgf.height > maxy)
									maxy=obj_dgf.y + obj_dgf.height;

								combined_dgf->x=minx;
								combined_dgf->y=miny;
								combined_dgf->width=maxx-minx+1;
								combined_dgf->height=maxy-miny+1;
								combined_dgf->midx=(minx+maxx)/2;
								combined_dgf->midy=(miny+maxy)/2;
							}
						}					
					}
				}
			}
		}

		/* Expose children. */
		recursive_expose(canvas->patch, X, Y, S, A, combined_dgf, topcanvas);

		/* Dispose of combined DDGSframe */
		Free(combined_dgf);
	}

/* Expose siblings. */
	recursive_expose(canvas->next, oldX, oldY, S, A, dgf, topcanvas);
} /* recursive_expose */




BOOL expose_Canvas(Canvas *canvas, DDGSframe *dgf)
{

	if (!canvasImageExposeEnabled)
		return FALSE;

	Canvas*	parent;
	int X,Y,S,A;
	Canvas *topcanvas;


	if (!canvas)
		return FALSE;

/* offset canvas origin depending on ancestor canvases 
 * and maintain highest canvas scale factor and aspect */

	X=Y=0;
	S=canvas->frame.cpframe.scale;
	A=canvas->frame.aspect;
	topcanvas=canvas;

	parent=canvas->parent;

	while (parent)
	{
/* update display origin based on parent_site */
		modify_origin(&X,&Y,parent);

		S=parent->frame.cpframe.scale;
		A=parent->frame.aspect;
		topcanvas=parent;

		parent=parent->parent;
	}

/* expose overlapped canvases */
	intens(1);
	recursive_expose(canvas,X,Y,S,A,dgf,topcanvas);

	return TRUE;
} /* expose_Canvas */


/*
 *	H I D E _ C A N V A S _ T Y P E
 *
 *	Makes canvases of the given type invisible, the next time they are
 *	displayed, by oring the canvas->visible flag with 2. Normally
 *	canvases are made visible by setting canvas->visible.
 *	hide_Canvas_type can be undone by anding canvas->visible with 1,
 *	since previously visible canvases will revert to 1 and previously
 *	invisible canvases will revert to 0. This is precisely what
 *	unhide_Canvas_type does.
 */

void hide_Canvas_type(Canvas *canvas, short type)
{
	if (!canvas)
		return;

	hide_Canvas_type(canvas->next,type);

	hide_Canvas_type(canvas->patch,type);

	if (canvas->type == type)
		canvas->visible=(canvas->visible | 2);
}


void unhide_Canvas_type(Canvas *canvas, short type)
{
	if (!canvas)
		return;

	unhide_Canvas_type(canvas->next,type);

	unhide_Canvas_type(canvas->patch,type);

	if (canvas->type == type)
		canvas->visible=(canvas->visible & 1);
}


/*
 * canvas_has_probes
 * Seeing if there are any probe bits in a
 * canvas and its sub-tree.
 * This is used to decide whether its necessary
 * to have a 24-bit rendering buffer, or if 8
 * bits will do (which is quicker).
 */

int canvas_has_probes(Canvas *can)
{
	Canvas *next;
	int got_one = 0;

	if (!can)
		return(got_one);

	next = can;
	while (next)
	{
		if ((next->type == Ccomposite) ||
			(next->type == Coverlap) ||
			(next->type == Ccounterstain) ||
			(next->type == Cprobe) ||
			(next->type == Cuprobe) ||
			(next->type == Cfetal))
		{
			got_one = 1;
			break;
		}

		if (got_one = canvas_has_probes(next->patch))
			break;

		next = next->next;
	}

	return(got_one);
}


void RectToDDGSFrame(DDGSframe *dframe, RECT *r)
{
	//convert from screen to image coordinates
	RECT rect = *r;
	double x1,y1,x2,y2;
	x1 = (double)rect.left;
	y1 = (double)rect.top;
	x2 = (double)rect.right;
	y2 = (double)rect.bottom;
	screentoimage (&x1,&y1);
	screentoimage (&x2,&y2);
	rect.left = (long)x1;
	rect.top = (long)y2;
	rect.right = (long)x2;
	rect.bottom = (long)y1;

		//6.11.2006
		//these scales are kept constant
		//to accomodate all format cameras
	float normal_scale = 8.0;

		//convert these to DDGSframe coords
	int h = (int)((float)((rect.bottom - rect.top) +1)*normal_scale);
	int x = (int)((float)rect.left*normal_scale);
	int w = (int)((float)((rect.right - rect.left) +1)*normal_scale);
	int y = (int)(rect.top * normal_scale);

	dframe->x= x;
	dframe->y= y;
	dframe->midx = x + w/2;
	dframe->midy = y + h/2;
	dframe->width = w;
	dframe->height = h;
}

