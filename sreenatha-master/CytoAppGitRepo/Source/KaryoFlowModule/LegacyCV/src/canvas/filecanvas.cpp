/*
 *
 *	filecanvas.cpp	M.Gregson 18/12/92
 *
 *	Generic routines for saving and loading canvas structures from disk
 *
 *	Mods:
 *
 *	23Mar2000	JMB	Converted to .cpp, minor mods to reduce warnings, changed
 *					headers included. Changed occurences of variable 'this' to
 *					'thiscanvas', as 'this' is a reserved word in C++.
 *	WH  30Mar98:		validateWObj() - This invertobject stuff doesn't handle the complexity of some image
 *						types like CGH which can have complex groups attached. Actual inversion on load will now
 *						happen in the loadbox stuff in cvadmin. (need the everthing loaded before we start flipping 
 *						things.
 *	BP	03Dec96:		Check for NULL file in save/load.
 */


#include "canvas.h"
#include <ddgs.h>
#include <woolz.h>

//#include "canvasdll.h"
//#include "/wincv/include/ddgs.h"
//#include "/wincv/include/woolz.h"


#include <stdio.h>
#include <fcntl.h>
#include <io.h>
//#include <ddgs.h>
//#include <wstruct.h>
//#include <canvas.h>



// For compatibility with UNIX version of CV
// compiled with Zp2 throughout, we need to
// compile this segment with same. Fortunately,
// it is isolated to this small segment!
#pragma pack(2)


/**************************************
 Old format canvas structure for filing.
 The only difference is the old_pframe
 structure (new pframe has int positions
 instead of short).
***************************************/

typedef struct {
	int aspect;
	struct old_pframe cpframe;
	short parent_site;
	short posingrid;
	int width;
	int height;
	short clip;
	short arena;
	short colour;
	short drawmode;
} OldCframe;

typedef struct {
	short type;
	OldCframe frame;
	struct pgrid patchgrid;
	short visible;
	struct object *Wobj;
	struct Canvas_type  *parent;
	struct Canvas_type  *previous;
	struct Canvas_type  *next;
	struct Canvas_type  *patch;
	struct propertylist *data;
} OldCanvas;


static int
fwrite_canvas(Canvas *can, FILE *f)
{
	OldCanvas fcan;

	fcan.type = can->type;
	fcan.frame.aspect = can->frame.aspect;
	fcan.frame.cpframe.type = can->frame.cpframe.type;
	fcan.frame.cpframe.scale = can->frame.cpframe.scale;
	fcan.frame.cpframe.dx = (WZCOORD)can->frame.cpframe.dx;
	fcan.frame.cpframe.dy = (WZCOORD)can->frame.cpframe.dy;
	fcan.frame.cpframe.ox = can->frame.cpframe.ox;
	fcan.frame.cpframe.oy = can->frame.cpframe.oy;
	fcan.frame.cpframe.ix = can->frame.cpframe.ix;
	fcan.frame.cpframe.iy = can->frame.cpframe.iy;
	fcan.frame.parent_site = can->frame.parent_site;
	fcan.frame.posingrid = can->frame.posingrid;
	fcan.frame.width = can->frame.width;
	fcan.frame.height = can->frame.height;
	fcan.frame.clip = can->frame.clip;
	fcan.frame.arena = can->frame.arena;
	fcan.frame.colour = can->frame.colour;
	fcan.frame.drawmode = can->frame.drawmode;
	fcan.patchgrid = can->patchgrid;
	fcan.visible = can->visible;
	fcan.Wobj = can->Wobj;
	fcan.parent = can->parent;
	fcan.previous = can->previous;
	fcan.next = can->next;
	fcan.patch = can->patch;
	fcan.data = can->data;

	if (fwrite(&fcan, sizeof(OldCanvas), 1, f) != 1)
		return(-1);
	return(0);
}


static int
fread_canvas(Canvas *can, FILE *f)
{
	OldCanvas fcan;

	if (fread(&fcan, sizeof(OldCanvas), 1, f) != 1)
		return(-1);

	can->type = fcan.type;
	can->frame.aspect = fcan.frame.aspect;
	can->frame.cpframe.type = fcan.frame.cpframe.type;
	can->frame.cpframe.scale = fcan.frame.cpframe.scale;
	can->frame.cpframe.dx = (int)fcan.frame.cpframe.dx;
	can->frame.cpframe.dy = (int)fcan.frame.cpframe.dy;
	can->frame.cpframe.ox = fcan.frame.cpframe.ox;
	can->frame.cpframe.oy = fcan.frame.cpframe.oy;
	can->frame.cpframe.ix = fcan.frame.cpframe.ix;
	can->frame.cpframe.iy = fcan.frame.cpframe.iy;
	can->frame.parent_site = fcan.frame.parent_site;
	can->frame.posingrid = fcan.frame.posingrid;
	can->frame.width = fcan.frame.width;
	can->frame.height = fcan.frame.height;
	can->frame.clip = fcan.frame.clip;
	can->frame.arena = fcan.frame.arena;
	can->frame.colour = fcan.frame.colour;
	can->frame.drawmode = fcan.frame.drawmode;
	can->patchgrid = fcan.patchgrid;
	can->visible = fcan.visible;
	can->Wobj = fcan.Wobj;
	can->parent = fcan.parent;
	can->previous = fcan.previous;
	can->next = fcan.next;
	can->patch = fcan.patch;
	can->data = fcan.data;

	return(0);
}


// Back to whatever the normal packing is.
#pragma pack()


/*
 *	recursive_save of canvas - called by save_Canvas
 */
static int
recursive_save(FILE *fp, Canvas *canvas, int *CN, int parent, int previous)
{
	Canvas *oldparent, *oldnext, *oldprevious, *oldpatch;
	int thiscanvas,next,patch;
	int NOT_OK;
	char *ch;
	SMALL si;

	if (canvas!=NULL) {
		(*CN)++;	/* increment canvas count */
	
		thiscanvas=*CN;	/* this canvas number */

		if (canvas->next!=NULL) {
			next=(*CN) + 1;
			NOT_OK=recursive_save( fp, canvas->next, CN, parent, thiscanvas );
			if (NOT_OK) {
				fprintf(stderr,"save_Canvas failed writing ->next\n");
				return(-1);
			}
		}
		else next=0;

		if (canvas->patch!=NULL) {
			patch=(*CN) + 1;
			NOT_OK=recursive_save( fp, canvas->patch, CN, thiscanvas, 0 );
			if (NOT_OK) {
				fprintf(stderr,"save_Canvas failed writing ->patch\n");
				return(-1);
			}
		}
		else patch=0;

		/* Save this canvas and its Wobj */

		/* save current canvas number */
		if (fwrite(&thiscanvas,sizeof(thiscanvas),1,fp) != 1) {
			fprintf(stderr,"save_Canvas failed writing canvas number\n");
			return(-1);
		}

		/* protect canvas pointers */
		oldparent=canvas->parent;
		oldprevious=canvas->previous;
		oldnext=canvas->next;
		oldpatch=canvas->patch;

		/* assign canvas numbers to canvas pointers */	
		canvas->parent=(Canvas *)parent;
		canvas->previous=(Canvas *)previous;
		canvas->next=(Canvas *)next;
		canvas->patch=(Canvas *)patch;

		/* then save canvas structure */;
		if (fwrite_canvas(canvas, fp) != 0)
		{
/*
		if (fwrite(canvas,sizeof(Canvas),1,fp) != 1) {
*/
			fprintf(stderr,"save_Canvas failed writing canvas structure\n");
			return(-1);
		}

		/* restore canvas pointers */
		canvas->parent=oldparent;
		canvas->previous=oldprevious;
		canvas->next=oldnext;
		canvas->patch=oldpatch;

		/* save canvas->data propertylist */
		if (canvas->data!=NULL) {
			si=canvas->data->size;

			if (fwrite(&si,sizeof(SMALL),1,fp) != 1) {
				fprintf(stderr,"save_Canvas failed writing canvas->data->size\n");
				return(-1);
			}
			ch = (char *) canvas->data;
			ch += sizeof(SMALL);
			si -= sizeof(SMALL);
			if (fwrite(ch,si,1,fp) != 1) {
				fprintf(stderr,"save_Canvas failed writing canvas->data\n");
				return(-1);
			}
		}	
		
		/* save canvas woolz object */
		if (canvas->Wobj!=NULL) {
			if (writeobj(fp, canvas->Wobj) != 0) {
				fprintf(stderr,"save_Canvas failed writing canvas->Wobj\n");
				return(-1);
			}
		}
	}

	return(0);

}


static void
recursive_count(Canvas *canvas, int *CN)
{
	if (canvas!=NULL) {
		(*CN)++;	/* increment canvas count */
		recursive_count( canvas->next, CN );
		recursive_count( canvas->patch, CN );
	}
}


/*
 *	SAVE_CANVAS
 *
 *	Recursive canvas file save - saves a canvas structure to file.
 *	Saves canvas structure and any attached woolz objects.
 *	Returns 0 if successful.
 */
int save_Canvas(FILE *fp, Canvas *canvas)
{
	int CN, OK;

	if ((canvas == NULL) || (fp == NULL))
		return -1;

	// Make sure the file is in binary mode.
	_setmode(_fileno(fp), _O_BINARY);

	CN=0;						/* initialise canvas count */

	recursive_count( canvas , &CN );		/* count number of canvases */

	if (fwrite(&CN,sizeof(CN),1,fp) != 1)		/* and save to file */
		return(-1);

	CN=0;						/* initialise canvas number */

	OK=recursive_save( fp, canvas, &CN, 0, 0 );	/* save canvas structure */

	return(OK);					/* canvas saved */
}



/*
 * Make sure the woolz object and canvas components
 * are valid. This may be necessary when reading an
 * old-platform file.
 *
 * Currently consists of two checks:
 *	1)	no negative ix/iy (explicitly flip object).
 *	2)	resize text frame (fonts not exactly the same size).
 */
void validateWObj(Canvas *can)
{
	if (can->Wobj == NULL)
		return;


#if 0
// BELOW NOT IN USE
// This invertobject stuff doesn't handle the complexity of some image
// types like CGH which can have complex groups attached. Actual inversion on load will now
// happen in the loadbox stuff in cvadmin. (need the everthing loaded before we start flipping 
// things.
	/* First check type 1 objects for negative
	 * ix/iy, and flip objects if necessary. */
	if (can->Wobj->type == 1)
	{
		WZObj *newobj;
		WZPlist *plist;

		if ((can->frame.cpframe.ix < 0) &&
			(can->frame.cpframe.iy < 0))
		{
			if (newobj = invertobject(can->Wobj))
			{
				plist = can->Wobj->plist;
				can->Wobj->plist = NULL;
				freeobj(can->Wobj);
				can->Wobj = newobj;
				can->Wobj->plist = plist;
			}
				
			can->frame.cpframe.ix = 1;
			can->frame.cpframe.iy = 1;

			can->frame.cpframe.ox += can->Wobj->idom->kol1;
			can->frame.cpframe.oy += can->Wobj->idom->line1;
//			can->frame.cpframe.dx += can->frame.width;
//			can->frame.cpframe.dy += can->frame.height;
		}
		else if (can->frame.cpframe.ix < 0)
		{
			if (newobj = invertobject_hor(can->Wobj))
			{
				plist = can->Wobj->plist;
				can->Wobj->plist = NULL;
				freeobj(can->Wobj);
				can->Wobj = newobj;
				can->Wobj->plist = plist;
			}

			can->frame.cpframe.ix = 1;

			can->frame.cpframe.ox += can->Wobj->idom->kol1;
//			can->frame.cpframe.dx += can->frame.width;
		}
		else if (can->frame.cpframe.iy < 0)
		{
			if (newobj = invertobject_hor(can->Wobj))
			{
				plist = can->Wobj->plist;
				can->Wobj->plist = NULL;
				freeobj(can->Wobj);
				can->Wobj = newobj;
				can->Wobj->plist = plist;
			}

			can->frame.cpframe.iy = 1;

			can->frame.cpframe.oy += can->Wobj->idom->line1;
//			can->frame.cpframe.dy += can->frame.height;
		}
	}
// ABOVE NOT IN USE
#endif

	/* Now check text objects for suitable
	 * canvas frame size. */
	if (can->Wobj->type == 70)
	{
		int w, h;

		/* Only makes sense if theres a DOG. */
		if (getcurdg())
		{
			setfont(((WZText *)can->Wobj->idom)->font);
			textsize(((WZTextObj *)can->Wobj)->text, &w, &h);

			/* We are only concerned if the frame
			 * is too small, and we don't want to
			 * reduce the frame in case the image
			 * is later loaded back into old software. */
			if (w > can->frame.width)
				can->frame.width = w;
			if (h > can->frame.height)
				can->frame.height = h;
		}
	}
}


/*
 *	LOAD_CANVAS
 *
 *	Recursive canvas file load - loads a canvas structure from file.
 *	Loads canvas structure and any attached woolz objects.
 *	Returns pointer to canvas if successful, NULL otherwise.
 */
Canvas *load_Canvas(FILE *fp)
{
	int CN, thiscanvas, cnum;
	Canvas **cptr_table;
	Canvas *canvas;
	char *ch;
	SMALL si;

	if (fp == NULL)
		return NULL;

	// Make sure the file is in binary mode.
	_setmode(_fileno(fp), _O_BINARY);

	CN=0;						/* initialise canvas count */

	if (fread(&CN,sizeof(CN),1,fp) != 1) {		/* and read from file */
		fprintf(stderr,"load_Canvas failed reading number of canvases\n");
		return(NULL);
	}

	/* allocate space for canvas ptr table - this is
 	   used to relink the canvas tree structure */
	cptr_table=(Canvas **)Malloc((CN+1)*sizeof(Canvas *));

	cptr_table[0]=NULL;

	for (cnum=1; cnum<=CN; cnum++) {

		/* Load canvas and its Wobj */

		/* load current canvas number */
		if (fread(&thiscanvas,sizeof(thiscanvas),1,fp) != 1) {
			fprintf(stderr,"load_Canvas failed reading canvas id number\n");
			return(NULL);
		}

		/* Allocate memory for canvas */
		canvas=(Canvas *) Malloc(sizeof(Canvas));

		/* link canvas number to canvas */
		cptr_table[thiscanvas]=canvas;

		/* then read in canvas structure */;
		if (fread_canvas(canvas, fp) != 0)
		{
			fprintf(stderr,"load_Canvas failed reading canvas structure\n");
			return(NULL);
		}

		/* read canvas->data propertylist */
		if (canvas->data != NULL)
		{
			if (fread(&si,sizeof(SMALL),1,fp) != 1) {
				fprintf(stderr,"load_Canvas failed reading sizeof canvas->data\n");
				return(NULL);
			}
			canvas->data=(struct propertylist *) Malloc(si);
			canvas->data->size = si;
			ch = (char *)canvas->data;
			ch += sizeof(SMALL);
			si -= sizeof(SMALL);
			if (fread(ch,si,1,fp) != 1) {
				fprintf(stderr,"load_Canvas failed reading canvas->data\n");
				return(NULL);
			}
		}	
		
		/* read canvas woolz object */
		if (canvas->Wobj != NULL)
		{
			if ((canvas->Wobj = readobj(fp)) == NULL)
			{
				fprintf(stderr,"load_Canvas failed reading canvas->Wobj\n");
				return(NULL);
			}

			// Make sure any platform-dependence is
			// accounted for.
			validateWObj(canvas);
		}
	}


	/* relink canvas using canvas ptr table */
	for (cnum=1; cnum<=CN; cnum++)
	{
		canvas=cptr_table[cnum];
		canvas->parent	=cptr_table[(int)canvas->parent];
		canvas->next	=cptr_table[(int)canvas->next];
		canvas->previous=cptr_table[(int)canvas->previous];
		canvas->patch	=cptr_table[(int)canvas->patch];
	}
	
	/* set canvas pointing to root of canvas tree structure */
	canvas=cptr_table[1];

	/* dispose of canvas number table */
	Free(cptr_table);

	/* return pointer to loaded canvas */
	return(canvas);
}
