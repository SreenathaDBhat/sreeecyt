
/*
 * ideocanvas.cpp
 * 
 * JMB May - Aug 2000
 *
 * Mods:
 *
 *
 */

#include <canvas.h>

#include <math.h> // For sin, cos
#include <limits.h>



// Must define pi to enough decimal places that sin(PI) is exactly zero and
// cos(PI) is exactly -1, otherwise rotations by pi radians (applied to all
// ideograms with a rotation of zero, to invert - see code) will not always
// yield exactly straight vertical and horizontal lines in some circumstances.
#define PI  3.1415926535897932384626433832795


#define TEXT_SCALE_MULTIPLIER  0.65f
#define TEXT_SCALE_MIN         1.0f
#define TEXT_SCALE_MAX         2.6f


// Float values are truncated when cast to int. Using this macro instead of
// casting to int gives the rounded equivalent instead. Using truncated values
// rather than rounded ones gives poor visual results as, for example, the
// endpoints of a line after rotation may have y-coordinates of 7.99 and 8.00
// respectively, and so the line should appear horoizontal, but after
// truncation these values become 7 and 8, resulting in a line which does not
// look horizontal.
#define ROUND(x) ((x < 0) ? ((int)((x) - 0.5f)) : ((int)((x) + 0.5f)))



static
IdeoPolyVertex
RotateVertex(const IdeoPolyVertex *pVertex, 
             float ox, float oy, float sinAng, float cosAng)
{
// Translates an IdeoPolyVertex to a coordinate system with origin (ox, oy),
// then rotates about the origin.
// The original vertex is not modified.
	IdeoPolyVertex rotVtx(0, 0);

	// Convert to coordinate system with origin (ox, oy)
	float transx = pVertex->x - ox;
	float transy = pVertex->y - oy;

	// Rotate around the origin
	rotVtx.x = transx * cosAng - transy * sinAng;
	rotVtx.y = transx * sinAng + transy * cosAng;

	return rotVtx;
}


static
void
UpdateExtents(int x, int y, int *pXmin, int *pXmax, int *pYmin, int *pYmax)
{
	if (x < *pXmin) *pXmin = x;	
	if (x > *pXmax) *pXmax = x;
	if (y < *pYmin) *pYmin = y;	
	if (y > *pYmax) *pYmax = y;
}

static
void
GetIdeoPolySetDisplayExtents(IdeoPolySet *pPolySet, int s, 
                             int *pXmin, int *pXmax, int *pYmin, int *pYmax,
                             BOOL unRotate)
{
// Gets the dimensions of the smallest rectangle (with horizontal and vertical
// edges) which will enclose the given IdeoPolySet (including its text), as
// drawn by DisplayIdeogram().
// If unRotate is TRUE, the rectangle is rotated back (see notes below).
// The extents are in DDGS units.
// This function must match the calculations done in DisplayIdeogram() and
// DrawIdeoPoly(), except that here the origin for rotation is (0, 0) and there
// are no offsets (dx, dy) applied, both of which make the code simpler.
// However, this means that (if unRotate is FALSE) the absolute positions of
// the extents are not much use, only the dimensions are useful
// (e.g. xMin - xMax). If unRotate is TRUE, the extents rectangle is rotated by
// the reverse of the rotation applied to the IdeoPolySet, then the extents are
// calculated from this. This gives a different result from just getting the
// extents from the IdeoPolySet after rotating zero degrees, because the text
// items have different relative orientations to the polygons at different
// rotations since they are always horizontal. Also, more importantly, the
// extents of the unRotated extent rectangle are not the extents of the
// unRotated ideogram.
	int xMin = INT_MAX, xMax = INT_MIN, yMin = INT_MAX, yMax = INT_MIN;

	if (pPolySet)
	{
		float scale = s * pPolySet->scale;

		float sinAng, cosAng;
		// Note: 180 degrees (PI radians) is always added to the rotation
		// because ideogram coordinates are 'upside down' compared with DDGS
		// coordinates.
		sinAng = (float)sin((double)pPolySet->rotation + PI);
		cosAng = (float)cos((double)pPolySet->rotation + PI);


		// Calculate the coordinates of each vertex in each poly,
		// after rotation and scaling.
		int i = 0;
		IdeoPoly *pPoly;
		while ((pPoly = pPolySet->GetPoly(i++)) != NULL)
		{
			IdeoPolyVertex *pVertex, rotVertex(0,0);

			int i = 0;
			while ((pVertex = pPoly->GetVertex(i++)) != NULL)
			{
				rotVertex = RotateVertex(pVertex, 0, 0, sinAng, cosAng);

				int x = ROUND(rotVertex.x * scale);
				int y = ROUND(rotVertex.y * scale);

				// Adjust overall extents if neccessary.
				UpdateExtents(x, y, &xMin, &xMax, &yMin, &yMax);
			}
		}


		// Calculate the coordinates of the rectangle enclosing the text item,
		// after rotation and scaling.
		i = 0;
		IdeoPolyText *pPolyText;
		while ((pPolyText = pPolySet->GetText(i++)) != NULL)
		{
			IdeoPolyVertex textPos1(pPolyText->x1, pPolyText->y1);
			textPos1 = RotateVertex(&textPos1, 0, 0, sinAng, cosAng);

			IdeoPolyVertex textPos2(pPolyText->x2, pPolyText->y2);
			textPos2 = RotateVertex(&textPos2, 0, 0, sinAng, cosAng);

			setfont(0);
			setTrueFont(0); // use font id only


			float polyTextScale = pPolySet->scale * TEXT_SCALE_MULTIPLIER;
			if (polyTextScale < TEXT_SCALE_MIN) polyTextScale = TEXT_SCALE_MIN; 
			if (polyTextScale > TEXT_SCALE_MAX) polyTextScale = TEXT_SCALE_MAX; 
			int textScale = polyTextScale * s;


			int w, h;
			textsize(pPolyText->text, &w, &h);

			w = (w * textScale) / 8;
			h = (h * textScale) / 8;


			IdeoPolyVertex textVertex1, textVertex2, textVertex3, textVertex4;
			int scaledX = ROUND(textPos2.x * scale);
			int scaledY = ROUND(textPos2.y * scale);
			int halfH = h / 2;
			// text() function takes coordinates of bottom left corner of text
			if (textPos1.x > textPos2.x)
			{
			// Text is to the left of textPos2
				// Get coordinates for four corners of text rectangle.
				// Bottom left vertex.
				textVertex1.x = (float)(scaledX - w);
				textVertex1.y = (float)(scaledY - halfH);
				// Bottom right vertex.
				textVertex2.x = (float) scaledX;
				textVertex2.y = (float)(scaledY - halfH);
				// Top right vertex.
				textVertex3.x = (float) scaledX;
				textVertex3.y = (float)(scaledY + halfH);
				// Top left vertex.
				textVertex4.x = (float)(scaledX - w);
				textVertex4.y = (float)(scaledY + halfH);
			}
			else
			{
			// Text is to the right of textPos2
				// Get coordinates for four corners of text rectangle.
				// Bottom left vertex.
				textVertex1.x = (float) scaledX;
				textVertex1.y = (float)(scaledY - halfH);
				// Bottom right vertex.
				textVertex2.x = (float)(scaledX + w);
				textVertex2.y = (float)(scaledY - halfH);
				// Top right vertex.
				textVertex3.x = (float)(scaledX + w);
				textVertex3.y = (float)(scaledY + halfH);
				// Top left vertex.
				textVertex4.x = (float) scaledX;
				textVertex4.y = (float)(scaledY + halfH);
			}


			UpdateExtents(ROUND(textVertex1.x), ROUND(textVertex1.y),
			              &xMin, &xMax, &yMin, &yMax);
			UpdateExtents(ROUND(textVertex2.x), ROUND(textVertex2.y),
			              &xMin, &xMax, &yMin, &yMax);
			UpdateExtents(ROUND(textVertex3.x), ROUND(textVertex3.y),
			              &xMin, &xMax, &yMin, &yMax);
			UpdateExtents(ROUND(textVertex4.x), ROUND(textVertex4.y),
			              &xMin, &xMax, &yMin, &yMax);
		}



		if (unRotate)
		{
		// Get the extents rectangle and rotate it by the reverse of the
		// original rotation, then re-calculate the extents from the extents
		// of the rotated rectangle.
			IdeoPolyVertex extentVertex1((float)xMin, (float)yMin);
			IdeoPolyVertex extentVertex2((float)xMax, (float)yMin);
			IdeoPolyVertex extentVertex3((float)xMax, (float)yMax);
			IdeoPolyVertex extentVertex4((float)xMin, (float)yMax);

			// Using -sinAng, cosAng should reverse the original rotation.
			extentVertex1 = RotateVertex(&extentVertex1, 0, 0, -sinAng, cosAng);
			extentVertex2 = RotateVertex(&extentVertex2, 0, 0, -sinAng, cosAng);
			extentVertex3 = RotateVertex(&extentVertex3, 0, 0, -sinAng, cosAng);
			extentVertex4 = RotateVertex(&extentVertex4, 0, 0, -sinAng, cosAng);

			// Recalculate the extents from just these vertices.
			xMin = INT_MAX; xMax = INT_MIN; yMin = INT_MAX; yMax = INT_MIN;
			UpdateExtents(ROUND(extentVertex1.x), ROUND(extentVertex1.y),
			              &xMin, &xMax, &yMin, &yMax);
			UpdateExtents(ROUND(extentVertex2.x), ROUND(extentVertex2.y),
			              &xMin, &xMax, &yMin, &yMax);
			UpdateExtents(ROUND(extentVertex3.x), ROUND(extentVertex3.y),
			              &xMin, &xMax, &yMin, &yMax);
			UpdateExtents(ROUND(extentVertex4.x), ROUND(extentVertex4.y),
			              &xMin, &xMax, &yMin, &yMax);
		}
	}


	*pXmin = xMin;
	*pXmax = xMax;
	*pYmin = yMin;
	*pYmax = yMax;
}



static
void DrawIdeoPoly(IdeoPoly *pPoly, float ox, float oy, float sinAng, float cosAng, 
                                   float scale, float dx, float dy/*,
                                   BOOL backgroundIsBlack*/)
{
	IdeoPolyVertex *pVertex, rotVertex(0,0);

	int x1,y1;
	double ss, sc;

		//handle all format images and display magnifications
	x1 = (int)(dx/8.0);
	y1 = (int)(dy/8.0);
	imagetoscreen_pt_scale (&x1,&y1,&ss, &sc,TRUE);
	dx = (float)x1 * 8;
	dy = (float)y1 * 8;

	scale = scale * ss;

	int i = 0;
	pVertex = pPoly->GetVertex(i++);

	rotVertex = RotateVertex(pVertex, ox, oy, sinAng, cosAng);

	DDGSBeginIdeoPoly(ROUND((rotVertex.x * scale) + dx), 
	                  ROUND((rotVertex.y * scale) + dy));

	while ((pVertex = pPoly->GetVertex(i++)) != NULL)
	{
		rotVertex = RotateVertex(pVertex, ox, oy, sinAng, cosAng);

		DDGSIdeoPolyVertex(ROUND((rotVertex.x * scale) + dx), 
		                   ROUND((rotVertex.y * scale) + dy));
	}

	pVertex = pPoly->GetVertex(0);
	rotVertex = RotateVertex(pVertex, ox, oy, sinAng, cosAng);
	DDGSIdeoPolyVertex(ROUND((rotVertex.x * scale) + dx), 
	                   ROUND((rotVertex.y * scale) + dy));


	// Translate IdeoPoly properties to DDGS ideo properties.
	// TODO: These need to be adjusted based on the angle of rotation,
	// otherwise at certain angles the hatching will be perpendicular to the
	// axis of the ideogram, which will make it look like a series of small bands.
	unsigned char properties = 0;
	if (pPoly->properties & IDEOPOLY_LRHATCH) properties |= DDGSIDEO_LRHATCH;
	if (pPoly->properties & IDEOPOLY_RLHATCH) properties |= DDGSIDEO_RLHATCH;
	if (pPoly->properties & IDEOPOLY_VHATCH) properties  |= DDGSIDEO_VHATCH;
	if (pPoly->properties & IDEOPOLY_SELECT) properties  |= DDGSIDEO_SELECT;


	DDGSEndIdeoPoly(pPoly->colour[0], pPoly->colour[1], pPoly->colour[2], 
	                properties/*, backgroundIsBlack*/);
}



void DisplayIdeogram(IdeoPolySet *pPolySet, int s, int dx, int dy, BOOL backgroundIsBlack)
 {
// Display ideogram after scaling and rotating and centering on (dx,dy).
// 's' is the scaling factor for converting to DDGS screen coordinates,
// dx and dy are in DDGS coordinates already.

//getcurdg()->cur_colour = 0;
//moveto(0,0);
//lineto(100,100);
	int x1, y1;
	double sca, sc;

	if (pPolySet && pPolySet->scale > 0.0f && s > 0)
	{
		float scale = s * pPolySet->scale;
		// Note: 180 degrees (PI radians) is always added to the rotation
		// because ideogram coordinates are 'upside down' compared with DDGS
		// coordinates.
		float sinAng = (float)sin((double)pPolySet->rotation + PI);
		float cosAng = (float)cos((double)pPolySet->rotation + PI);

/*
		// Note that extents are NOT scaled or rotated,
		float xMin, xMax, yMin, yMax;
		pPolySet->GetExtents(&xMin, &xMax, &yMin, &yMax);

		float xMid = (xMin + xMax) / 2.0f;
		float yMid = (yMin + yMax) / 2.0f;
*/
		int xMin, xMax, yMin, yMax;
		GetIdeoPolySetDisplayExtents(pPolySet, s, &xMin, &xMax, &yMin, &yMax, TRUE);

		// Remove scaling from dimensions (equivalent to 'scale').
		float xMid = ((xMin + xMax) / 2.0f) / scale;
		float yMid = ((yMin + yMax) / 2.0f) / scale;


		DDGSBeginIdeo(); // Begin adding to boundary region.

		int i = 0;
		IdeoPoly *pPoly;
		while ((pPoly = pPolySet->GetPoly(i++)) != NULL)
		{
			DrawIdeoPoly(pPoly, xMid, yMid, sinAng, cosAng,
			                    scale, (float)dx, (float)dy/*,
			                    backgroundIsBlack*/);
		}

		DDGSEndIdeo(backgroundIsBlack); // Draw boundary .


		// Put DDGS into draw mode (intens(0) is erase mode (default)).
		intens(1);

		if (backgroundIsBlack) colour(254);
		else                   colour(0); // Set DDGS current colour to black.



	x1 = (int)(dx/8.0);
	y1 = (int)(dy/8.0);
	imagetoscreen_pt_scale (&x1,&y1,&sca, &sc,TRUE);
	dx = (float)x1 * 8;
	dy = (float)y1 * 8;

	scale = s * pPolySet->scale * sca;

		i = 0;
		IdeoPolyText *pPolyText;
		while ((pPolyText = pPolySet->GetText(i++)) != NULL)
		{
		// Display text.
			IdeoPolyVertex textPos1(pPolyText->x1, pPolyText->y1);
			textPos1 = RotateVertex(&textPos1, xMid, yMid, sinAng, cosAng);

			IdeoPolyVertex textPos2(pPolyText->x2, pPolyText->y2);
			textPos2 = RotateVertex(&textPos2, xMid, yMid, sinAng, cosAng);

			moveto(ROUND(textPos1.x * scale)  + dx , ROUND(textPos1.y * scale) + dy);
			lineto(ROUND(textPos2.x * scale)  + dx , ROUND(textPos2.y * scale) + dy);


			// Use the smallest available font (though the scale factor passed
			// to text() will affect its size). There is some information in 
			// function set_anno_font() in anno\annotate.cpp about the ddgs
			// fonts: apparently there are currently only four fonts available,
			// 0, 1, 2 and 3, in increasing order of size, which can also be
			// made bold.
			setfont(0);
			setTrueFont(0); 


			// Get the scale factor for the text. This is proportional to the
			// IdeoPolySet scale, within limits.
			float polyTextScale = pPolySet->scale * TEXT_SCALE_MULTIPLIER; // Multiplier is empirical

			// A text scale of less than the minimum is pointless as it cannot be read.
			if (polyTextScale < TEXT_SCALE_MIN) polyTextScale = TEXT_SCALE_MIN; 
			// Similarly, a text scale of more than the maximum is unnecessarily large.
			if (polyTextScale > TEXT_SCALE_MAX) polyTextScale = TEXT_SCALE_MAX; 

			// Now calculate the DDGS text scale. This must be calulated after
			// the upper and lower limits have been applied, so that it is
			// proportional to the main DDGS scale. This is because the canvas
			// extents are proportional to the DDGS scale and so the text scale
			// must be also, otherwise it will not fit the extents when they
			// are scaled.
			int textScale = polyTextScale * s;


			// The textsize() function returns the size of the text in the
			// current font, at a scale of 8.
			int w, h;
			textsize(pPolyText->text, &w, &h);

			// Scale text dimensions for textScale.
			w = (w * textScale) / 8;
			h = (h * textScale) / 8;



			int scaledX = ROUND(textPos2.x * scale) + dx;
			int scaledY = ROUND(textPos2.y * scale) + dy;
			int halfH = h / 2;

			LPTSTR lp = pPolyText->text;
			// text() function takes coordinates of bottom left corner of text
			// Need to subtract half of text hight so that vertical edge of
			// text is centered on our coordinates.
			if (textPos1.x > textPos2.x)
			{
			// Draw text to the left of textPos2
				text(scaledX - w, scaledY - halfH, lp, textScale);
/*
				moveto(scaledX - w, scaledY - halfH);
				lineto(scaledX,     scaledY - halfH);
				lineto(scaledX,     scaledY + halfH);
				lineto(scaledX - w, scaledY + halfH);
*/
			}
			else
			{
			// Draw text to the right of textPos2
				text(scaledX,     scaledY - halfH, lp, textScale);
/*
				moveto(scaledX,     scaledY - halfH);
				lineto(scaledX + w, scaledY - halfH);
				lineto(scaledX + w, scaledY + halfH);
				lineto(scaledX,     scaledY + halfH);
*/
			}
		}

	}
}




struct object *
CreateIdeogramWoolzObject(IdeoPolySet *pPolySet, int *pWidth, int *pHeight)
{
	struct object *pObj = NULL;
/*
	float xMin, xMax, yMin, yMax;
	pPolySet->GetExtents(&xMin, &xMax, &yMin, &yMax);

	int width  = (int)((xMax - xMin) * pPolySet->scale + 1.0f);
	int height = (int)((yMax - yMin) * pPolySet->scale + 1.0f);
*/
	// Get a better estimate from GetIdeoPolySetDisplayExtents(), when there is
	// text, if a large scale is passed to it, probably something to do with
	// integer arithmetic. I think 24 is the maximum possible scale in WinCV:
	// I think it is the scale when zoomed and fullscreen.
	// This assumes that the text scale is proportionate to the overall scale. 
	int xMin, xMax, yMin, yMax;
	GetIdeoPolySetDisplayExtents(pPolySet, 24, &xMin, &xMax, &yMin, &yMax, FALSE);

	// Divide by the scale passed to GetIdeoPolySetDisplayExtents() to get the
	// dimensions at a scale of 1.
	int width  = ROUND((float)(xMax - xMin) / 24.0f);
	int height = ROUND((float)(yMax - yMin) / 24.0f);


	// Width or height may be zero due to scaling, or both may be zero because
	// the IdeogramSet is empty (e.g. just created). In this case create a
	// nominal 1x1 pixmap.
	if (width < 1)  width  = 1;
	if (height < 1) height = 1;


	// Save the current DDGS so it can be restored at the end.
	// Note that presumably the old DDGS may be using memory DC too (e.g. if
	// printing or saving to file), in which case it may not have a window
	// handle associated with it.
	DDGS *pOldDDGS = getcurdg();


// If this lot doesn't work, try comparing with canvas2bitmap() in canvas
// Also see probeRawMerge() in isolate\isocall.c


	// Get a handle to the screen device context, as this will have the correct
	// resolution for display.
// TODO: why doesn't pOldDDGS->dc work? This may cause problems when printing or saving to file??
	HDC hScreenDC = GetDC(NULL);

	// Create a memory device context which is compatible with the display,
	// for drawing off-screen.
	HDC hMemDC = CreateCompatibleDC(hScreenDC);
	//int cap = GetDeviceCaps(hMemDC, BITSPIXEL);
	// Create a bitmap for use as the drawing surface in the memory device context.
	// Note that this must be compatible with the same device context the
	// memory device context is compatible with, not with the memory device
	// context itself.
	HBITMAP hBitmap = CreateCompatibleBitmap(hScreenDC, width, height);

	// Release the screen DC now it's finished with.
	ReleaseDC(NULL, hScreenDC);

	// Put the bitmap into the memory device context.
	HGDIOBJ hOldObj = SelectObject(hMemDC, hBitmap);


	// Make sure we are drawing on a white background. Any pixels that remain
	// white will be subtracted from the final Woolz object.
	RECT rect;
	rect.left = rect.top = 0;
	rect.right = width + 1;
	rect.bottom = height + 1;
	FillRect(hMemDC, &rect, (HBRUSH)GetStockObject(WHITE_BRUSH));


	// Create a DDGS context based on the memory device context (this is what
	// ddgspropen does: despite its name it doesn't have to be used for printing).
	DDGS *pMemDDGS = ddgspropen(hMemDC, width, height);


	// Now draw the IdeoPolySet (into the memory context).
	DisplayIdeogram(pPolySet, 8, (width * 8) / 2, (height * 8) / 2, FALSE);


	// Get the bitmap bits from the bitmap

	// Each scan line in the DI bitmap will be padded up to a LONG (4 byte)
	// boundary. Each pixel is 3 bytes (24 bit), so without padding the
	// number of bytes in the scan line for 'width' pixels will be:
	int scanWidthBytes = width * 3;

	// If this number of bytes is not exactly divisible by 4,
	// pad it upto a 4 byte boundary.
	int remainder = scanWidthBytes % 4;
	if (remainder > 0) scanWidthBytes = scanWidthBytes - remainder + 4;


	unsigned char *p24BitBuffer = (unsigned char *)malloc(scanWidthBytes * (height + 1));

	// Must select the bitmap out of the DC before calling GetDIBits.
	SelectObject(hMemDC, hOldObj);

	BITMAPINFO bitmapInfo;
	bitmapInfo.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
	bitmapInfo.bmiHeader.biWidth       = width;
	bitmapInfo.bmiHeader.biHeight      = (LONG)height; // Negate height to invert.
	bitmapInfo.bmiHeader.biPlanes      = 1; // Always 1.
	bitmapInfo.bmiHeader.biBitCount    = 24; // 24 bits means no palette
	bitmapInfo.bmiHeader.biCompression = BI_RGB;

	int n = GetDIBits(hMemDC, hBitmap, 0, height, p24BitBuffer, &bitmapInfo, DIB_RGB_COLORS);


	// Convert 24 bit colour buffer to nearest grey values.
	GREY *pGreyBuffer = (GREY*)calloc(width * (height + 1), sizeof(GREY)); /* Woolz grey levels */

	GREY *pGreyPixel = pGreyBuffer;

	unsigned char *pScanLine = p24BitBuffer;

	int w, h;
	for (h = 0; h < height; h++)
	{
		unsigned char *p24BitPixel = pScanLine;

		for (w = 0; w < width; w++)
		{
			// Average out R, G and B from 24BitPixel to get greyscale equivalent.
			int rgbAverage = (p24BitPixel[0] + p24BitPixel[1] + p24BitPixel[2]) / 3;

			*pGreyPixel = (255 - rgbAverage);  // Have to invert values: don't know why

			pGreyPixel += sizeof(GREY);
			p24BitPixel += 3;
		}

		pScanLine += scanWidthBytes;
	}



	// Take grey value buffer and create a woolz object of the required
	// configuration from it. I don't know whether calling makerect() then
	// threshold is the best way to do this, but this is what was done by the
	// the old programs makewideo2.c and makecghideo.c, which created the old
	// set of ideogram woolz object files, so doing this should at least create
	// the same sort of woolz object as before.

	// Makerect creates a type 1 (conventional, interval domain,
	// grey table object) with a type 2 interval domain (rectangular) and a
	// type 11 valuetable (rectangular, integer).
	//pObj = makerect(1, height, 1, width, pGreyBuffer, 0, NULL, NULL);
	pObj = makerect(0, height - 1, 0, width - 1, pGreyBuffer, 0, NULL, NULL);

	// Threshold takes the object and creates a similar object from it, but
	// with a type 1 interval domain (arbitarily shaped), that is thresholded.
	// The old pObj is pointed to by the assoc pointer of the new pObj.
	// The thresholded object is constucted of an arbitary number of
	// 'intervals' of pixel data per scanline, with gaps in between
	// corresponding to where pixels in the rectangular object which fell below
	// the threshold were. Thus, the boundary of such an object is easy to
	// obtain as it corresponds with the beginning of the first interval and
	// the end of the last interval on each scanline.
	// Note that the grey values in the object are inverted, so white is 0,
	// black is 255. If the background colour in the rectangular object is
	// white, then a threshold of 1 will result in an object which has gaps
	// where white pixels were. This means that if there are any white bands in
	// the ideogram, they must be drawn slightly off-white so they do not get
	// subtracted as background when thresholded - this is done by the DDGS
	// function DDGSEndIdeoPoly(), which is called by DrawIdeoPoly().
	pObj = threshold(pObj, 1);



	// Clean up.
	// Don't free this: it becomes part of object(?)  free(pGreyBuffer);
	free(p24BitBuffer);
	ddgsclose(pMemDDGS);
	selectddgs(pOldDDGS);
	DeleteObject(hBitmap);
	DeleteDC(hMemDC);


	*pWidth  = width;
	*pHeight = height;


	return pObj;
}


Canvas *
CreateIdeogramCanvas(struct propertylist *pData)
{
	Canvas *pCanvas = NULL;

	if (!pData) return NULL;


// HAVE TO CAST pDATA TO CHAR* here, else structure packing (?) means that 4 bytes
// are added to pData rather than 2 bytes as might be expected (sizeof(pData->size) is 2).
// Not sure if this is the best way to handle this: look at other WinCV code that uses property lists.
	IdeoPolySet *pPolySet = new IdeoPolySet((char*)pData + sizeof(pData->size));

	// Create a woolz bitmap object from the IdeoPolySet
	int width, height;
	struct object *ideoobj = CreateIdeogramWoolzObject(pPolySet, &width, &height);

//	delete pPolySet;



	short Ctype = Cideogram; // Canvas type
	Cframe cf;

	// Create ideogram canvas
	struct intervaldomain *idom = (struct intervaldomain *) ideoobj->idom;

	/* set display frame - ddgs coordinates relative to parent */
	cf.aspect = 100;
	cf.cpframe.scale = 8; //ideo_scale; // 8;
	cf.cpframe.dx = 0;//(valx + wid/2 - 20) * 8; /* 20 is half original ideo woolz object width */
	cf.cpframe.dy = 0;//BASE_OFFSET;
	cf.cpframe.ox = idom->kol1;    /*(int)xMin;*/ 
	cf.cpframe.oy = idom->line1;   /*(int)yMin;*/ 
	cf.cpframe.ix = 1; // Always 1 (see woolz.h)
	cf.cpframe.iy = 1; // Always 1 (see woolz.h)
	cf.parent_site = FREE;
	cf.posingrid = 0;
	// Use the width and height values returned by CreateIdeogramWoolzObject()
	// as the canvas dimensions, not the dimensions of the woolz object,
	// because if the ideogram has text, the woolz object will not include
	// parts of the text extents which do not have text in them, but
	// DisplayIdeogram() will center the ideogram in the canvas based on the
	// assumption that it is large enough to include the text extents (as
	// calculated by GetIdeoPolySetDisplayExtents()).
	cf.width  = (width  + 3) * 8;  /*(idom->lastkl - idom->kol1 + 1) * 8; */  /*(int)(xMax - xMin + 1) * 8;*/ 
	cf.height = (height + 3) * 8;  /*(idom->lastln - idom->line1 + 1) * 8;*/  /*(int)(yMax - yMin + 1) * 8;*/
	cf.clip = 0;
	cf.arena = GREYIM;
	cf.colour = 255;
	cf.drawmode = COPY;

	pCanvas = create_Canvas(Ctype, &cf, NULL, 1, /* visible */
	                        ideoobj, NULL, NULL, NULL, NULL, NULL);


// Create patch canvas for annotation canvas. This will hold actual ideogram
// data in propertylist (parent propertylist is used for other purposes
// - e.g. paste_to_flex() in flexpaste.cpp replaces any existing data in the
// parent canvas's propertylist with a Cflexdata structure).
	// Frame is not used, so zero it.
	memset(&cf, 0, sizeof(Cframe));

	Canvas *patchCan = create_Canvas(Ctype, &cf, NULL, 0, /* Not visible */
	                                 NULL, NULL, NULL, NULL, NULL, pData);

	pCanvas->patch = patchCan;



	delete pPolySet;


	return pCanvas;
}


Canvas *
CreateIdeogramCanvasFromIdeogram(Ideogram *pIdeo, float width, float scale)
{
	Canvas *pCanvas = NULL;

	IdeoPolySet *pPolys = new IdeoPolySet(pIdeo, width, scale);

	if (pPolys != NULL)
	{
		int polyBufferSize;
		void *pPolyBuffer = pPolys->CreateBuffer(&polyBufferSize);

		if (pPolyBuffer)
		{		
			struct propertylist pl, *pData = makeplist(polyBufferSize + sizeof(pl.size));

			// Copy buffer to property list.
			// HAVE TO CAST pDATA TO CHAR* here, else structure packing (?)
			// means that 4 bytes are added to pData rather than 2 bytes as
			// might be expected (sizeof(pData->size) is 2). 
			memcpy(((char*)pData) + sizeof(pData->size), pPolyBuffer, polyBufferSize);


			pCanvas = CreateIdeogramCanvas(pData);


			// Can now free buffer
			free(pPolyBuffer);
		}

		delete pPolys;
	}

	return pCanvas;
}



BOOL IsCanvasNewIdeogram(Canvas *pCanvas)
{
// For compatibility, a new ideogram looks like an old ideogram, to old code,
// but new code can use extra information in its patch canvas to draw a new
// format ideogram.
// An old format ideogram is just a Canvas with type Cideogram and with a
// type 1 Wobj. A new format ideogram Canvas is like an old format ideogram
// Canvas, plus its path pointer points to another Cideogram canvas. The second
// Canvas always has a NULL Wobj and a non-NULL property list. The property
// list ('data' pointer) for the second ideogram contains the data needed to
// draw a new format ideogram.
	if (   pCanvas->type == Cideogram
	    && pCanvas->patch != NULL 
	    && pCanvas->patch->type == Cideogram 
	    && pCanvas->patch->data != NULL     ) 
		return TRUE;
	else
		return FALSE;
}



Canvas *
RotateScaleNewIdeogramCanvas(Canvas *pOldCanvas, float newRads, float newScale)
{
	Canvas *pNewCanvas = NULL;

	if (IsCanvasNewIdeogram(pOldCanvas))
	{
		float scale, rotAng;
		struct propertylist *pPL = pOldCanvas->patch->data;

		GetValuesFromIdeoPolySetBuffer(((char*)pPL + sizeof(pPL->size)), 
		                               &scale, &rotAng);

		scale *= newScale; // Note newScale is applied to existing scale, rather than replacing it.
		//rotAng += (float)rads;
		rotAng = newRads;

		SetValuesInIdeoPolySetBuffer(((char*)pPL + sizeof(pPL->size)), 
		                             scale, rotAng);

		// Create a new ideogram canvas based on the rotated property list for
		// the old canvas. Do this before destroying the old canvas as that
		// would destroy the property list before we've used it!
		pNewCanvas = CreateIdeogramCanvas(pPL);


		// Adjust new canvas position so that its centre is where the centre of
		// the old canvas was.
		Cframe *pOldFrame = &(pOldCanvas->frame);
		Cframe *pNewFrame = &(pNewCanvas->frame);		

		pNewFrame->cpframe.dx =   pOldFrame->cpframe.dx
		                        + (pOldFrame->width - pNewFrame->width)/2;
		pNewFrame->cpframe.dy =   pOldFrame->cpframe.dy
		                        + (pOldFrame->height - pNewFrame->height)/2;

		// Actual scale may not be default scale for newly created canvas.
		pNewFrame->cpframe.scale = pOldFrame->cpframe.scale;


		// Copy old canvas's property list, if any
		// (usually holds Cflexdata or Cannodata structure etc.).
		if (pOldCanvas->data)
		{
			pNewCanvas->data = makeplist(pOldCanvas->data->size);
			memcpy(pNewCanvas->data, pOldCanvas->data, pOldCanvas->data->size);
		}


		// Move canvas's wobj so that it doesn't jump if split.
		// Move it to match the canvas.
		// Copied this from add_ideo() in annotate.cpp. I think this is needed
		// because create_flexible_canvas() in flexcanvas.cpp, which is called
		// to create the canvases resulting from a split, gets the canvas
		// offsets from the split wobjs.
		int oldox, oldoy, newox, newoy, deltax, deltay;
		WObject *moveobj;

		oldox = pNewCanvas->Wobj->idom->kol1;
		oldoy = pNewCanvas->Wobj->idom->line1;
		newox = pNewFrame->cpframe.dx / 8;
		newoy = pNewFrame->cpframe.dy / 8;
		deltax = newox - oldox;
		deltay = newoy - oldoy;

		moveobj = (struct object *)moveobject(pNewCanvas->Wobj, deltax, deltay);
		freeobj(pNewCanvas->Wobj);
		pNewCanvas->Wobj = moveobj;

		pNewFrame->cpframe.ox = moveobj->idom->kol1;
		pNewFrame->cpframe.oy = moveobj->idom->line1;
	}

	return pNewCanvas;
}






/*
	IdeoPolyVertex *pVertex;

	int i = 0;
	pVertex = pPoly->GetVertex(i++);

	moveto((pVertex->x * scale) + dx, (pVertex->y * scale) + dy);

	while ((pVertex = pPoly->GetVertex(i++)) != NULL)
	{
		lineto((pVertex->x * scale) + dx, (pVertex->y * scale) + dy);
	}

	pVertex = pPoly->GetVertex(0);
	lineto((pVertex->x * scale) + dx, (pVertex->y * scale) + dy);
*/

