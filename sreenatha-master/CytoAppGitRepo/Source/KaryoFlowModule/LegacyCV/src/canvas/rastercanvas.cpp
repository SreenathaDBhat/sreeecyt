/*
 * rastercanvas.cpp	M.Gregson
 *
 *	Routines to convert a canvas into a monochrome or RGB raster
 *	- used for filing or printing
 *
 *	Mods:
 *
 *	23Mar2000	JMB	Converted to .cpp, minor mods to reduce warnings,
 *					changed headers included.
 *	19Mar97	BP:		Complete rewrite for Windows. Single new function
 *					to draw image into a HBITMAP.
 *	BP	5/11/95:	Call XGetImage on chunks of 32 lines.
 */


#include "canvas.h"
#include <ddgs.h>


//#include "canvasdll.h"
//#include "/wincv/include/ddgs.h"


#include <stdio.h>
#include <memory.h>
//#include <ddgs.h>
//#include <common.h>
//#include <canvas.h>
#include <gdiplus.h>
using namespace Gdiplus;

extern int show_forInterpolation; // 1 = image data only, 2 = graphics, 0 = all


/*
 * Create rectangular bitmap containing canvas image.
 */

HBITMAP canvas2bitmap(Canvas *canvas, DDGS *dog, int scale)
{
	HDC dc, memdc;
	DDGS *memdog;
	HBITMAP bmp;
	HGDIOBJ oldobj;
	int w, h;
	int origx, origy;

	//w = dog->maxx + 1;
	//h = dog->maxy + 1;
	w = canvas->frame.width * scale/64;
	h = canvas->frame.height * scale/64;
	int oldscale = canvas->frame.cpframe.scale;
	origx = canvas->frame.cpframe.dx;
	origy = canvas->frame.cpframe.dy;

	// Set to normal scale and origin
	canvas->frame.cpframe.scale = scale;
	canvas->frame.cpframe.dx = 0;
	canvas->frame.cpframe.dy = 0;
										
	// Create a new memory DC which we can
	// draw into invisibly.
	dc = GetDC(dog->window);

	if ((memdc = CreateCompatibleDC(dc)) == NULL)
	{
		ReleaseDC(dog->window, dc);
		MessageBox(dog->window, _T("Create DC failed"), _T("canvas2bitmap error"), MB_OK);
		return NULL;
	}
	if ((bmp = CreateCompatibleBitmap(dc, w, h)) == NULL)
	{
		ReleaseDC(dog->window, dc);
		MessageBox(dog->window, _T("Create Bitmap failed"), _T("canvas2bitmap error"), MB_OK);
		return NULL;
	}
	ReleaseDC(dog->window, dc);

	// Select the bitmap into the memory DC.
	oldobj = SelectObject(memdc, bmp);

	// Create a temporary dog, and draw the
	// canvas into it.
	memdog = ddgspropen(memdc, w, h);
	memdog->invertBG = 0x00000000;
	memdog->normalBG = 0x00ffffff;

	memdog->ddgsImageFormat.MaxX = w;
	memdog->ddgsImageFormat.MaxY = h;
	memdog->ddgsImageFormat.Magnification = 1;

	if (canvas->type == Ccomposite)
		memdog->invertBG = get_canvas_BG_col(canvas);

	selectddgs(memdog);
	intens(1);
	if (canvas->frame.drawmode == INVERT)
		invert_clear();
	else
		clear();
	display_Canvas(canvas);
	ddgsclose(memdog);

	// Restore scale
	canvas->frame.cpframe.scale = oldscale;
	canvas->frame.cpframe.dx = origx;
	canvas->frame.cpframe.dy = origy;

	// Finished with the DC.
	SelectObject(memdc, oldobj);
	DeleteDC(memdc);

	return bmp;
}

//-----------------------------------------------------------------------------
// MG: Draw canvas into offscreen ddgs, then blit it to the destination DDGS
// - very quick, flickerless display of canvas change without having to 
// erase old canvas or worry about expose events
BOOL BltCanvasToDDGS(Canvas *canvas, DDGS *destdg, RECT *rect)
{
	if ((canvas->type > 41)||(canvas->type < 1)) return FALSE;

	BOOL status = FALSE;

	selectddgs (destdg);	//make sure the dg is selected

	// Build new image in an offscreen bitmap
	// and copy the whole bitmap over the current image - to prevent flicker
	HDC destdc = GetDC(destdg->window);
	HDC memdc = CreateCompatibleDC(destdc);

	if (!memdc)
		return FALSE;

	// initialize for the whole of the drawing area
	int x = 0, y = 0;
	int w = destdg->maxx - destdg->minx + 1;
	int h = destdg->maxy - destdg->miny + 1;
	int sw =w, sh = h;

	DDGSframe dframe;
	if (rect) // update rect
	{
		RectToDDGSFrame(&dframe, rect);
		x = rect->left;
		y = rect->top;
		sw = rect->right - rect->left;
		sh = rect->bottom - rect->top;
	}

	HBITMAP hBitmap = CreateCompatibleBitmap(destdc, w, h);

	if (hBitmap)
	{
		// Select the bitmap into the memory DC.
		HGDIOBJ oldobj;
		oldobj = SelectObject(memdc, hBitmap);

		// Create a temporary dog, and draw the
		// canvas into it.
		DDGS *memdog = ddgspropen(memdc, w, h); // has to be the size of the canvas for the offsets to be the same

				//copy settings from destdg
		memdog->invertBG = destdg->invertBG;
		memdog->normalBG = destdg->normalBG;
		memdog->ddgsImageFormat = destdg->ddgsImageFormat;
		memdog->pseudocolour = destdg->pseudocolour;
		memcpy ((void*)&memdog->binfo,(void*)&destdg->binfo, sizeof (DDGSBITMAPINFO));
		memcpy ((void*)&memdog->minfo,(void*)&destdg->minfo, sizeof (DDGSBITMAPINFO));

		selectddgs(memdog);
		intens(1);
		// blt the canvas into the destination 
		if (true)
		{
			if (canvas->frame.drawmode==COPY) // clear all of the offscreen buffer first
				clear();
			else 
				invert_clear();

			if (rect) // expose update rect only
			{
				expose_Canvas(canvas, &dframe);
			}
			else // do the lot
			{
				display_Canvas(canvas);
			}

			SetStretchBltMode(destdc,COLORONCOLOR);
			StretchBlt(destdc, x, y, sw, sh, memdc, x, y, sw, sh, SRCCOPY); // only copy the bit that has been updated
			SelectObject(memdc, oldobj);
		}
		else // this code is attempting to interpolate image, MUST be using fastpixobj and fastpixobj 24 not n versions.
			// rearranged.
		{
			StartGDIplus();
			show_forInterpolation=1;
			display_Canvas(canvas);

			SelectObject(memdc, oldobj);

			Bitmap *bm = new Bitmap (hBitmap,/*, ddgs_palette*/NULL);

			// scale image based on zoom????????
			int scale_w = w / 4;
			int scale_h = h / 4;
			Bitmap scalebitmap((INT)scale_w, (INT)scale_h, bm->GetPixelFormat());
			Graphics *new_g = Graphics::FromImage((Image*)&scalebitmap);
			new_g->SetSmoothingMode(SmoothingModeHighQuality);
			new_g->SetInterpolationMode(InterpolationModeHighQualityBicubic);
			new_g->DrawImage(bm, 0, 0, scalebitmap.GetWidth(), scalebitmap.GetHeight());
		    delete new_g;

			// okay now draw to screen, with interpolation
			Graphics graphics(destdc);
			graphics.SetSmoothingMode(SmoothingModeHighSpeed);
			graphics.SetInterpolationMode(InterpolationModeHighQualityBicubic);
			graphics.SetPixelOffsetMode(PixelOffsetModeHalf);
			Rect r(0, 0, w, h);
			graphics.DrawImage(&scalebitmap,r,0,0,scale_w,scale_h,UnitPixel,NULL);
			delete bm;

			// now draw in the graphics direct to display dc
			selectddgs(destdg);
			show_forInterpolation=2;
			display_Canvas(canvas);

			show_forInterpolation=0;
			StopGDIplus();
		}

		DeleteObject(hBitmap);

		//copy back the image format parameter values
		destdg->ddgsImageFormat = memdog->ddgsImageFormat;

		// dump dog
		ddgsclose(memdog);

		status = TRUE;

		selectddgs (destdg);	//make sure the destination dg is selected
	}

	// Finished with mem DC.
	DeleteDC(memdc);
	ReleaseDC (destdg->window, destdc);

	return status;
}
//-----------------------------------------------------------------------------

