/*****************************************************************************
              Copyright (c) 1999 Applied Imaging International
 
	Source file:    CreateArray.cpp
 
	Function:       Create and destroy 1D, 2D and 3D dynamic arrays.

	Package:        sharedutils.lib part of WinCV
 
	Dependencies:   (Any non-portable dependencies)
 
	Usage:          (How to use this file)
 
	Modification history:
	Author      Date        Description
	SN          15Apr99     Initial implementation
 
 ****************************************************************************/

/** STANDARD LIBRARY HEADERS ************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>

#include <tchar.h>
#include <windows.h>
/** OTHER HEADER FILES ******************************************************/
#include <CreateArray.h>


/** MACROS ******************************************************************/
// Malloc/Calloc redefines
#define	Malloc	malloc
#define	Calloc	calloc
#define	Realloc	realloc
#define	Free	free


/** TYPEDEFS, STRUCTS *******************************************************/

/** PRIVATE FUNCTION PROTOTYPES *********************************************/

/** PUBLIC DECLARATIONS (and externs) ***************************************/

/** PRIVATE DECLARATIONS (statics) ******************************************/

/** DEBUG only DECLARATIONS and FUNCTIONS ***********************************/


/** IMPLEMENTATION **********************************************************/


/***************************************************** CreateArray1D
 
	CREATED:                    SN      15Apr99

	FUNCTION:                   
		Creates a 1 dimensional array of any type.

		If the error flag value passed in (*pfarrayOK) is 0 no array is created.
		If successful the value passed out is 1 otherwise 0.
		This means a number of arrays can be created with a number of calls to
		CreateArray1D but the error flag need only be checked once, after the
		final CreateArray1D call.

		If array creation fails an error message containing	the calling
		function's name and	the variable name of the array is sent to stderr.

	PARAMETERS:                 
		num:		read: number of elements of array
		size:		read: size of one element of array
		szfnname:	read: calling function's name
		szvarname:	read: variable name assigned to array
		pfarrayOK:	update: pointer to flag of error status. 0 - error, 1 - ok.
 
	RETURN:
		Pointer to array memory.
 
	EXTERNS:
		(Definition and usage of external or static variables used)
 
	MODS:
 
 ***************************************************************************/
void * CreateArray1D( size_t num, size_t size, LPCTSTR szfnname, LPCTSTR szvarname, int * pfarrayOK)
{
	void * pv;


	pv = NULL;


	// Check error status is ok before trying to create array
	if (*pfarrayOK)
	{
		// Allocate memory for array
		pv = Malloc( num * size);

		// Check allocation status
		if ( pv == NULL )
		{
			// Notify error in debug version
			assert(0);

			// Failed to create array so print message and set flag.
			fprintf( stderr, "ERROR: CreateArray1D: Called from %s: Variable %s\n", szfnname, szvarname);
			*pfarrayOK = 0;
		}
	}

	return( pv);
}


/*********************************************************** DestroyArray1D
 
	CREATED:                    SN      15Apr99

	FUNCTION:                   Destroys array created by CreateArray1D.

	PARAMETERS:                 pv: read: pointer to array memory
 
	RETURN:                     none
 
	EXTERNS:                    (Definition and usage of external or static
                                variables used)
 
	MODS:
 
 ***************************************************************************/
void DestroyArray1D( void * pv)
{
	Free( pv);
}


/*********************************************************** CreateArray2Ddouble
 
	CREATED:                    SN      19Apr99

	FUNCTION:                   
		Creates a 2 dimensional array of doubles.

		If the error flag value passed in (*pfarrayOK) is 0 no array is created.
		If successful the value passed out is 1 otherwise 0.
		This means a number of arrays can be created with a number of calls to
		CreateArray routines but the error flag need only be checked once, after
		the final CreateArray call.

		If array creation fails an error message containing	the calling
		function's name and	the variable name of the array is sent to stderr.

	PARAMETERS:                 
		num1:		read: number of elements for first index of array
		num2:		read: number of elements for second index of array
		szfnname:	read: calling function's name
		szvarname:	read: variable name assigned to array
		pfarrayOK:	update: pointer to flag of error status. 0 - error, 1 - ok.
 
	RETURN:
		Address of 2D array. NULL if there is any error.
 
	EXTERNS:
		(Definition and usage of external or static variables used)
 
	MODS:
 
 ***************************************************************************/
double ** CreateArray2Ddouble( size_t num1, size_t num2, LPCTSTR szfnname, LPCTSTR szvarname, int * pfarrayOK)
{
	size_t index1;
	double ** ppd;


	ppd = NULL;


	// Check error status is ok before trying to create array
	if (*pfarrayOK)
	{
		// Allocate memory for array
		ppd = (double **) Malloc( num1 * sizeof(double *) );

		// Check allocation status
		if ( ppd == NULL )
		{
			// Notify error in debug version
			assert(0);

			// Failed to create array so print message and set error status
			fprintf( stderr, "ERROR: CreateArray2Ddouble: Called from %s: Variable %s\n", szfnname, szvarname);
			*pfarrayOK = 0;
		}
		else
		{
			// No error so create second array index
			for (index1=0; index1<num1; index1++)
			{
				ppd[index1] = (double *) CreateArray1D( num2, sizeof(double), szfnname, szvarname, pfarrayOK);
			}

			// Cleanup memory if there are any errors
			if ( !(*pfarrayOK) )
			{
				DestroyArray2Ddouble( num1, num2, ppd);
				ppd = NULL;
				fprintf( stderr, "ERROR: CreateArray2Ddouble: Called from %s: Variable %s\n", szfnname, szvarname);
			}
		}
	}

	return( ppd);
}



/****************************************************** DestroyArray2Ddouble
 
	CREATED:                    SN      19Apr99

	FUNCTION:
		Destroys 2D array created by CreateArray2Ddouble.

	PARAMETERS:
		num1:		read: number of elements for first index of array
		num2:		read: number of elements for second index of array
		ppd:		read: address of 2D array

 
	RETURN:                     none
 
	EXTERNS:                    (Definition and usage of external or static
                                variables used)
 
	MODS:
 
 ***************************************************************************/
void DestroyArray2Ddouble( size_t num1, size_t num2, double ** ppd)
{
	size_t index1;


	if (ppd != NULL)
	{
		// Free second array index
		for ( index1=0; index1<num1; index1++)
			DestroyArray1D( ppd[index1]);

		// Free first array index
		Free( ppd);
	}
}


/*********************************************************** CreateArray3Ddouble
 
	CREATED:                    SN      20Apr99

	FUNCTION:                   
		Creates a 3 dimensional array of doubles.

		If the error flag value passed in (*pfarrayOK) is 0 no array is created.
		If successful the value passed out is 1 otherwise 0.
		This means a number of arrays can be created with a number of calls to
		CreateArray routines but the error flag need only be checked once, after
		the final CreateArray call.

		If array creation fails an error message containing	the calling
		function's name and	the variable name of the array is sent to stderr.

	PARAMETERS:                 
		num1:		read: number of elements for first index of array
		num2:		read: number of elements for second index of array
		num3:		read: number of elements for third index of array
		szfnname:	read: calling function's name
		szvarname:	read: variable name assigned to array
		pfarrayOK:	update: pointer to flag of error status. 0 - error, 1 - ok.
 
	RETURN:
		Address of 3D array. NULL if there is any error.
 
	EXTERNS:
		(Definition and usage of external or static variables used)
 
	MODS:
 
 ***************************************************************************/
double *** CreateArray3Ddouble( size_t num1, size_t num2, size_t num3,
							   LPCTSTR szfnname, LPCTSTR szvarname, int * pfarrayOK)
{
	size_t index1;
	double *** pppd;


	pppd = NULL;


	// Check error status is ok before trying to create array
	if (*pfarrayOK)
	{
		// Allocate memory for array
		pppd = (double ***) Malloc( num1 * sizeof(double **) );

		// Check allocation status
		if ( pppd == NULL )
		{
			// Notify error in debug version
			assert(0);

			// Failed to create array so print message and set error status
			fprintf( stderr, "ERROR: CreateArray3Ddouble: Called from %s: Variable %s\n", szfnname, szvarname);
			*pfarrayOK = 0;
		}
		else
		{
			// No error so create second and third array indicies
			for (index1=0; index1<num1; index1++)
			{
				pppd[index1] = (double **) CreateArray2Ddouble( num2, num3, szfnname, szvarname, pfarrayOK);
			}

			// Cleanup memory if there are any errors
			if ( !(*pfarrayOK) )
			{
				DestroyArray3Ddouble( num1, num2, num3, pppd);
				pppd = NULL;
				fprintf( stderr, "ERROR: CreateArray3Ddouble: Called from %s: Variable %s\n", szfnname, szvarname);
			}
		}
	}

	return( pppd);
}


/****************************************************** DestroyArray3Ddouble
 
	CREATED:                    SN      20Apr99

	FUNCTION:
		Destroys 3D array created by CreateArray3Ddouble.

	PARAMETERS:
		num1:		read: number of elements for first index of array
		num2:		read: number of elements for second index of array
		num3:		read: number of elements for third index of array
		ppd:		read: address of 3D array

 
	RETURN:                     none
 
	EXTERNS:                    (Definition and usage of external or static
                                variables used)
 
	MODS:
 
 ***************************************************************************/
void DestroyArray3Ddouble( size_t num1, size_t num2, size_t num3, double *** pppd)
{
	size_t index1;


	if (pppd != NULL)
	{
		// Free second and third array indicies
		for ( index1=0; index1<num1; index1++)
			DestroyArray2Ddouble( num2, num3, pppd[index1]);

		// Free first array index
		Free( pppd);
	}
}


/*********************************************************** CreateArray2Dint
 
	CREATED:                    SN      29Apr99

	FUNCTION:                   
		Creates a 2 dimensional array of ints.

		If the error flag value passed in (*pfarrayOK) is 0 no array is created.
		If successful the value passed out is 1 otherwise 0.
		This means a number of arrays can be created with a number of calls to
		CreateArray routines but the error flag need only be checked once, after
		the final CreateArray call.

		If array creation fails an error message containing	the calling
		function's name and	the variable name of the array is sent to stderr.

	PARAMETERS:                 
		num1:		read: number of elements for first index of array
		num2:		read: number of elements for second index of array
		szfnname:	read: calling function's name
		szvarname:	read: variable name assigned to array
		pfarrayOK:	update: pointer to flag of error status. 0 - error, 1 - ok.
 
	RETURN:
		Address of 2D array. NULL if there is any error.
 
	EXTERNS:
		(Definition and usage of external or static variables used)
 
	MODS:
 
 ***************************************************************************/
int ** CreateArray2Dint( size_t num1, size_t num2, LPCTSTR szfnname, LPCTSTR szvarname, int * pfarrayOK)
{
	size_t index1;
	int ** ppn;


	ppn = NULL;


	// Check error status is ok before trying to create array
	if (*pfarrayOK)
	{
		// Allocate memory for array
		ppn = (int **) Malloc( num1 * sizeof(int *) );

		// Check allocation status
		if ( ppn == NULL )
		{
			// Notify error in debug version
			assert(0);

			// Failed to create array so print message and set error status
			fprintf( stderr, "ERROR: CreateArray2Dint: Called from %s: Variable %s\n", szfnname, szvarname);
			*pfarrayOK = 0;
		}
		else
		{
			// No error so create second array index
			for (index1=0; index1<num1; index1++)
			{
				ppn[index1] = (int *) CreateArray1D( num2, sizeof(int), szfnname, szvarname, pfarrayOK);
			}

			// Cleanup memory if there are any errors
			if ( !(*pfarrayOK) )
			{
				DestroyArray2Dint( num1, num2, ppn);
				ppn = NULL;
				fprintf( stderr, "ERROR: CreateArray2Dint: Called from %s: Variable %s\n", szfnname, szvarname);
			}
		}
	}

	return( ppn);
}



/****************************************************** DestroyArray2Dint
 
	CREATED:                    SN      29Apr99

	FUNCTION:
		Destroys 2D array created by CreateArray2Dint.

	PARAMETERS:
		num1:		read: number of elements for first index of array
		num2:		read: number of elements for second index of array
		ppn:		read: address of 2D array

 
	RETURN:                     none
 
	EXTERNS:                    (Definition and usage of external or static
                                variables used)
 
	MODS:
 
 ***************************************************************************/
void DestroyArray2Dint( size_t num1, size_t num2, int ** ppn)
{
	size_t index1;


	if (ppn != NULL)
	{
		// Free second array index
		for ( index1=0; index1<num1; index1++)
			DestroyArray1D( ppn[index1]);

		// Free first array index
		Free( ppn);
	}
}







/*****************************************************************************
              Copyright (c) 1999 Applied Imaging International
 ****************************************************************************/
