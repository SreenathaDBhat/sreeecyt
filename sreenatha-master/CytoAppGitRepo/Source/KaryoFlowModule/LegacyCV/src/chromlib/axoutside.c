    /*
     * axoutside.c		26-05-86		Jim Piper
     *
     * find if any polygon vertex except first or last lies to left of
     * left-most interval of object, or to right of right-most interval
     *
     * Modifications
     *
	 *	22 Oct 1998 MG		Put in checks to make sure we deal
	 *						with silly axes that are outside the
	 *						y-range of the object. And did a general
	 *						tidy up of the code.
	 *
     *	05 Jan 1998	MG		Changed to check if vertex does
     *						not lie on any interval on a given
     *						line - this forces chromaxis2() to
     *						use skeleton on U shaped chromosomes.
	 *
     *	23 Sep 1987	jimp	Remove the interesting message !
	 *
     *	11 Sep 1986	CAS		Includes
     */
    
    #include <stdio.h>
    #include <woolz.h>
    
    #ifdef WIN32
    int
    axoutside(struct object *mpol, struct object *obj)
    #endif
    #ifdef i386
    int
    axoutside(mpol, obj)
    struct object *mpol;
    struct object *obj;
    #endif
    {
    	struct polygondomain *pdom;
    	struct ivertex *vtx;
    	struct intervaldomain *idom;
    	struct intervalline *itvln;
  		int i, j, outside, n, nints, vx, vy;
    
    	outside = 0;

    	idom = obj->idom;
    	pdom = (struct polygondomain *)mpol->idom;
    	vtx = pdom->vtx + 2;
    	n = pdom->nvertices - 4;

    	for (i=0; i<n; i++, vtx++) 
		{
			
			/* check if the axis vertex lies within the object y-range */
			vy = vtx->vtY;
			
			if (vy < idom->line1 || vy > idom->lastln)
			{
				outside=1;
				break;		/* from i loop */
			}

			/* get a pointer to the list of intervals for this line */
    		itvln = idom->intvlines + vy  - idom->line1;

    		/* check for no intervals on this line - possibly two joined objects, ignore this anomoly */
			if ((nints = itvln->nintvs) == 0)
    			continue;
    		
			/* now check if the axis vertex lies on any of the lintervals */ 
			vx = vtx->vtX - idom->kol1;
    		outside = 1;

 			for (j=0; j<nints; j++) 
			{
 				if (vx >= itvln->intvs[j].ileft && vx <= itvln->intvs[j].iright) 
				{
 					outside = 0;
 					break;	/* from j loop */
 				}
 			}
 
			/* if it didn't we're out of here */
  			if (outside)
  				break;		/* from i loop */
    	}

    	return(outside);
    }
