
/*
 * 	centroaxis.c		21/3/95		M.Gregson
 *
 *
 *	Based on mfeatures.c in chromlib
 *
 *	void centro_axis_features(obj,mpol)
 *
 *	modifies:
 *		obj->plist->length
 *		obj->plist->nbands
 *		obj->plist->nbindex
 *		obj->plist->wdd[]
 *		obj->plist->mwdd[]
 *		obj->plist->gwdd[]
 *		obj->plist->cvdd
 *		obj->plist->nssd
 *		obj->plist->cindexa
 *		obj->plist->cindexm
 *		obj->plist->cindexl
 *		obj->plist->mdra
 *
 *	And possibly :
 *		obj->plist->cx
 *		obj->plist->cy
 *
 *	Called when user modifies chromosome axis or centromere
 *	to re-measure the object axis and centromere dependent features.
 *	This will improve classification and chromosome orientation if
 *	called prior to karyotyping.
 *
 *	User can optionally supply a new axis in mpol - if NULL the
 *	axis is recalculated by chromaxis2() and Free'd at the end.
 *	If user has modified the centromere position this should
 *	be stored in the plist Ccx, Ccy fields. If these are both
 *	zero centromerefeatures() will calculate plist->cx and plist->cy
 *	and use this for centromere related features.
 *
 *	NOTE: 
 *
 *	The object supplied should be vertical for the centromere
 *	based features to work correctly - especially since cindexa
 *	may be used later for orientation decisions i.e. which end
 *	of the vertical object is centromere closest to.
 */

#include <stdio.h>
#include <woolz.h>
#include <chromanal.h>
#define PROFSMOOTH 1

/*
 * select which profiles should be used, one for band parameters,
 * one for centromere location.  We already know that profile "0" (density)
 * is better than profile "-1" (mean density)  or profile "-2" (max
 * density) for wdd features.
 */
static int whichprof[2] = {0,2};


#ifdef WIN32
void
centro_axis_features(struct chromosome *obj, struct object *mpol)
#endif
#ifdef i386
void
centro_axis_features(obj, mpol)
struct chromosome *obj;
struct object *mpol;
#endif
{
	struct object *chromaxis2(), *prof[2];
	struct histogramdomain *h;
	int freempol=0;

	/* if no axis supplied calculate one and
	   make sure we free it at the end */

	if (mpol == NULL) {
		mpol = chromaxis2(obj);
		freempol=1;
	}

	/*
	 * find density and moment profiles, and shorten mid-points polygon
	 * and profile if tips lie outside object.  Then chromosome length
	 * is resulting profile length.
	 */
	multiprofiles(obj,mpol,prof,whichprof,2);
	shorten(mpol,prof[0],prof[1]);
	obj->plist->length = ((struct polygondomain *)(mpol->idom))->nvertices;

	/* set tip values to zero */
	h = (struct histogramdomain *)prof[1]->idom;
	h->hv[0] = h->hv[h->npoints-1] = 0;
	/*
	 * Smoothing possibly improves the power of wdd features,
	 * so perhaps we should smooth more strongly here.
	 */
	histosmooth(prof[0], PROFSMOOTH);
	histosmooth(prof[1], PROFSMOOTH+2);
	/*
	 * find centromere, measure features
	 * use corrected centromere if it exists
	 */
	centromerefeatures(obj,mpol,prof[1]);
	/* 
	 * Number of bands and band index
	 */
    	obj->plist->nbands = 2 + countpeaks(prof[0],1,100); /* 1% quantum, full length */
    	obj->plist->nbindex = ((1 + countpeaks(prof[0],1,50)) * 100 + obj->plist->nbands/2) / obj->plist->nbands;
                                                         /* 1% quantum, half length */
    	if (obj->plist->nbindex > 50)
		obj->plist->nbindex = 100 - obj->plist->nbindex;

	/*
	 * profile density distribution features
	 */
	wddfeatures(obj,prof[0],prof[1]);

	if (freempol)
		freeobj(mpol);

	freeobj(prof[0]);
	freeobj(prof[1]);
}

