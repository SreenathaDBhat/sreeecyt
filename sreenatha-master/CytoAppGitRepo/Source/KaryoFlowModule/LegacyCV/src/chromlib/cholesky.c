



/*..BEGIN PROLOGUE  DCHDC
...DATE WRITTEN   790319   (YYMMDD)
...REVISION DATE  820801   (YYMMDD)
...CATEGORY NO.  D2B1B
...KEYWORDS  CHOLESKY DECOMPOSITION,DOUBLE PRECISION,LINEAR ALGEBRA,
             LINPACK,MATRIX,POSITIVE DEFINITE
...AUTHOR  DONGARRA, J., (ANL)
           STEWART, G. W., (U. OF MARYLAND)
...PURPOSE  Computes the Cholesky decomposition of a POSITIVE DEFINITE
            matrix.  A pivoting option allows the user to estimate the
            condition of a positive definite matrix or determine the
            rank of a POSITIVE SEMIDEFINITE matrix.
...DESCRIPTION

     DCHDC computes the Cholesky decomposition of a positive definite
     matrix.  A pivoting option allows the user to estimate the
     condition of a positive definite matrix or determine the rank
     of a positive semidefinite matrix.

     On Entry

         A      DOUBLE PRECISION(LDA,P).
                A contains the matrix whose decomposition is to
                be computed.  Only the upper half of A need be stored.
                The lower part of the array A is not referenced.

         LDA    INTEGER.
                LDA is the leading dimension of the array A.

         P      INTEGER.
                P is the order of the matrix.

         WORK   DOUBLE PRECISION.
                WORK is a work array.

         JPVT   INTEGER(P).
                JPVT contains integers that control the selection
                of the pivot elements, if pivoting has been requested.
                Each diagonal element A(K,K)
                is placed in one of three classes according to the
                value of JPVT(K).

                   If JPVT(K) .GT. 0, then X(K) is an initial
                                      element.

                   If JPVT(K) .EQ. 0, then X(K) is a free element.

                   If JPVT(K) .LT. 0, then X(K) is a final element.

                Before the decomposition is computed, initial elements
                are moved by symmetric row and column interchanges to
                the beginning of the array A and final
                elements to the end.  Both initial and final elements
                are frozen in place during the computation and only
                free elements are moved.  At the K-th stage of the
                reduction, if A(K,K) is occupied by a free element
                it is interchanged with the largest free element
                A(L,L) with L .GE. K.  JPVT is not referenced if
                JOB .EQ. 0.

        JOB     INTEGER.
                JOB is an integer that initiates column pivoting.
                If JOB .EQ. 0, no pivoting is done.
                If JOB .NE. 0, pivoting is done.

     On Return

         A      A contains in its upper half the Cholesky factor
                of the matrix A as it has been permuted by pivoting.

         JPVT   JPVT(J) contains the index of the diagonal element
                of a that was moved into the J-th position,
                provided pivoting was requested.

         INFO   contains the index of the last positive diagonal
                element of the Cholesky factor.

     For positive definite matrices INFO = P is the normal return.
     For pivoting with positive semidefinite matrices INFO will
     in general be less than P.  However, INFO may be greater than
     the rank of A, since rounding error can cause an otherwise zero
     element to be positive.  Indefinite systems will always cause
     INFO to be less than P.

     LINPACK.  This version dated 03/19/79 .
     J.Dongarra J. and Stewart G. W., Argonne National Laboratory and
     University of Maryland.


     BLAS DAXPY,DSWAP
     Fortran DSQRT
...REFERENCES  DONGARRA J.J., BUNCH J.R., MOLER C.B., STEWART G.W.,
                 *LINPACK USERS  GUIDE*, SIAM, 1979.
...ROUTINES CALLED  DAXPY,DSWAP
...END PROLOGUE  DCHDC
*/

#include <stdio.h>
 
#ifdef WIN32
void
dchdc(double *a, int *lda, int *p, double *work, int *jpvt, int *job, int *info)
#endif
#ifdef i386
void
dchdc(a, lda, p, work, jpvt, job, info)
double *a;
int *lda;
int *p;
double *work;
int *jpvt;
int *job;
int *info;
#endif
{
	double sqrt();
	void daxpy();
	void dswap();
	
	int pu, pl, plp1, j, k, l; 
	int jp, jt, kb, km1, kp1, maxl;
	int copyp, a_size;
	double temp;
	double maxdia;

/*	LOGICAL SWAPK,NEGK   */
	int swapk, negk;

	pl = 1;
	pu = 0;
	*info = *p;
	if (job != 0) {
/*
        PIVOTING HAS BEEN REQUESTED. REARRANGE THE
        THE ELEMENTS ACCORDING TO JPVT.
*/

		copyp = *p;
		a_size = *lda;
		for (k=1; k<=copyp; k++){
			swapk = jpvt[k]>0 ? 1 : 0;
			negk = jpvt[k]<0 ? 1 : 0;
			jpvt[k] = k;
            if (negk) 
				jpvt[k] = -jpvt[k];
			if (swapk){ 
				if (k != pl){ 
					dswap(pl-1,&a[1 * a_size + k],1,&a[1 * a_size + pl],1);
					temp = a[k * a_size + k];
					a[k * a_size + k] = a[pl * a_size + pl];
					a[pl * a_size + pl] = temp;
					plp1 = pl + 1;

					if (copyp >= plp1){
						for (j=plp1; j<copyp; j++){
							if (j<k){
								temp = a[pl * a_size + j];
								a[pl * a_size + j] = a[j * a_size + k];
								a[j * a_size + k] = temp;
							}
							else if (j>k){ 
								temp = a[k * a_size + j];
								a[k * a_size + j] = a[pl * a_size + j];
								a[pl * a_size + j] = temp;
							}
						}
					}
					jpvt[k] = jpvt[pl];
					jpvt[pl] = k;
				}
				pl = pl + 1;
			}
		}

		pu = *p;
		copyp = *p;
		if (copyp >= pl){ 
			for (kb=pl; kb<=copyp; kb++){

				k = copyp - kb + pl;
				if (jpvt[k]<0){
					jpvt[k] = -jpvt[k];
					if (pu!=k){
						dswap(k-1,&a[1 * a_size + k],1,&a[1 * a_size + pu],1);
						temp = a[k * a_size + k];
						a[k * a_size + k] = a[pu * a_size + pu];
						a[pu * a_size + pu] = temp;
						kp1 = k + 1;
						if (copyp>=kp1){
							for (j=kp1; j<=copyp; j++){
								if (j<pu){
									temp = a[k * a_size + j];
									a[k * a_size + j] = a[j * a_size + pu];
									a[j * a_size + pu] = temp;
								}
								else if (j>pu){
									temp = a[k * a_size + j];
									a[k * a_size + j] = a[pu * a_size + j];
									a[pu * a_size + j] = temp;
								}
							}
						}
						jt = jpvt[k];
						jpvt[k] = jpvt[pu];
						jpvt[pu] = jt;
					}
					pu = pu - 1;
				}
			}
		}
	}

	copyp = *p;
	for (k=1; k<=copyp; k++){ 
/*
        REDUCTION LOOP.
*/
		maxdia = a[k * a_size + k];
		kp1 = k + 1;
		maxl = k;
/*
        DETERMINE THE PIVOT ELEMENT.
*/
		if ((k>=pl)&&(k<pu)){
			for (l=kp1; l<=pu; l++){
				if (a[l * a_size + l]>maxdia){
					maxdia = a[l * a_size + l];
					maxl = l;
				}
			}
		}
/*
        QUIT IF THE PIVOT ELEMENT IS NOT POSITIVE.
*/	
		if (maxdia<=0){
			if (k!=maxl){
/*
           START THE PIVOTING AND UPDATE JPVT.
*/
				km1 = k - 1;
				dswap(km1,&a[1 * a_size + k],1,&a[1 * a_size + maxl],1);
				a[maxl * a_size + maxl] = a[k * a_size + k];
				a[k * a_size + k] = maxdia;
				jp = jpvt[maxl];
				jpvt[maxl] = jpvt[k];
				jpvt[k] = jp;
			}
/*
        REDUCTION STEP. PIVOTING IS CONTAINED ACROSS THE ROWS.
*/
			work[k] = sqrt(a[k * a_size + k]);
			a[k * a_size + k] = work[k];
			if (copyp>=kp1){
				for (j=kp1; j<=copyp; j++){
					if (k!=maxl){
						if (j<maxl){
							temp = a[k * a_size + j];
							a[k * a_size + j] = a[j * a_size + maxl];
							a[j * a_size + maxl] = temp;
						}
						else if (j>maxl){
							temp = a[k * a_size + j];
							a[k * a_size + j] = a[maxl * a_size + j];
							a[maxl * a_size + j] = temp;
						}
					}
					a[k * a_size + j] = a[k * a_size + j]/work[k];
					work[j] = a[k * a_size + j];
					temp = -a[k * a_size + j];
					daxpy(j-k,temp,work[kp1],1,a[kp1 * a_size + j],1);
				}
			}
		}
	}
}


/*		DAXPY	*/


/*..BEGIN PROLOGUE  DAXPY
...DATE WRITTEN   791001   (YYMMDD)
...REVISION DATE  820801   (YYMMDD)
...CATEGORY NO.  D1A7
...KEYWORDS  BLAS,DOUBLE PRECISION,LINEAR ALGEBRA,TRIAD,VECTOR
...AUTHOR  LAWSON, C. L., (JPL)
           HANSON, R. J., (SNLA)
           KINCAID, D. R., (U. OF TEXAS)
           KROGH, F. T., (JPL)
...PURPOSE  D.P computation y = a*x + y
...DESCRIPTION

                B L A S  Subprogram
    Description of Parameters

     --Input--
        N  number of elements in input vector(s)
       DA  double precision scalar multiplier
       DX  double precision vector with N elements
     INCX  storage spacing between elements of DX
       DY  double precision vector with N elements
     INCY  storage spacing between elements of DY

     --Output--
       DY  double precision result (unchanged if N .LE. 0)

     Overwrite double precision DY with double precision DA*DX + DY.
     For I = 0 to N-1, replace  DY(LY+I*INCY) with DA*DX(LX+I*INCX) +
       DY(LY+I*INCY), where LX = 1 if INCX .GE. 0, else LX = (-INCX)*N
       and LY is defined in a similar way using INCY.
...REFERENCES  LAWSON C.L., HANSON R.J., KINCAID D.R., KROGH F.T.,
                 *BASIC LINEAR ALGEBRA SUBPROGRAMS FOR FORTRAN USAGE*,
                 ALGORITHM NO. 539, TRANSACTIONS ON MATHEMATICAL
                 SOFTWARE, VOLUME 5, NUMBER 3, SEPTEMBER 1979, 308-323
...ROUTINES CALLED  (NONE)
...END PROLOGUE  DAXPY

*/


#ifdef WIN32
void
daxpy(int n, double *da, double *dx, int incx, double *dy, int incy)
#endif
#ifdef i386
void
daxpy(n, da, dx, incx, dy, incy)
int n;
double *da;
double *dx;
int incx;
double *dy;
int incy;
#endif
{
	int i;
	int ix, iy;
	int m, ns, mp1;

	if ((n>0) && (da!=0)) {
		if (incx == incy)
			if (incx-1 < 0){
/*
        CODE FOR NONEQUAL OR NONPOSITIVE INCREMENTS.
*/
				ix = 1;
				iy = 1;
				if (incx < 0)
					ix = (1-n)*incx + 1;
				if (incy < 0)
					iy = (1-n)*incy + 1;

				for (i=1; i<=n; i++) {
					dy[iy] = dy[iy] + *da * dx[ix];
					ix += incx;
					iy += incy;
				}
			}
			else if (incx-1 == 0){
/*
        CODE FOR BOTH INCREMENTS EQUAL TO 1


        CLEAN-UP LOOP SO REMAINING VECTOR LENGTH IS A MULTIPLE OF 4.
*/
				m = n % 4;
				if (m!=0){
					for (i=1; i<=m; i++)
						dy[i] += (*da * dx[i]);
					if (n<4)
						return;
				}
				mp1 = m + 1;
				for (i=mp1; i<n; i+=4){
					dy[i] +=  *da * dx[i];
					dy[i+1] +=  *da * dx[i+1];
					dy[i+2] +=  *da * dx[i+2];
					dy[i+3] +=  *da * dx[i+3];
				}
			}
			else{  /* > 1 */
/*
        CODE FOR EQUAL, POSITIVE, NONUNIT INCREMENTS.
*/
				ns = n * incx;
				for (i=1; i<=ns; i+=incx)
					dy[i] += *da * dx[i];
			}
	}
}

/*   	DSWAP  */

/*..BEGIN PROLOGUE  DSWAP
...DATE WRITTEN   791001   (YYMMDD)
...REVISION DATE  820801   (YYMMDD)
...CATEGORY NO.  D1A5
...KEYWORDS  BLAS,DOUBLE PRECISION,INTERCHANGE,LINEAR ALGEBRA,VECTOR
...AUTHOR  LAWSON, C. L., (JPL)
           HANSON, R. J., (SNLA)
           KINCAID, D. R., (U. OF TEXAS)
           KROGH, F. T., (JPL)
...PURPOSE  Interchange d.p. vectors
...DESCRIPTION

                B L A S  Subprogram
    Description of Parameters

     --Input--
        N  number of elements in input vector(s)
       DX  double precision vector with N elements
     INCX  storage spacing between elements of DX
       DY  double precision vector with N elements
     INCY  storage spacing between elements of DY

     --Output--
       DX  input vector DY (unchanged if N .LE. 0)
       DY  input vector DX (unchanged if N .LE. 0)

     Interchange double precision DX and double precision DY.
     For I = 0 to N-1, interchange  DX(LX+I*INCX) and DY(LY+I*INCY),
     where LX = 1 if INCX .GE. 0, else LX = (-INCX)*N, and LY is
     defined in a similar way using INCY.
...REFERENCES  LAWSON C.L., HANSON R.J., KINCAID D.R., KROGH F.T.,
                 *BASIC LINEAR ALGEBRA SUBPROGRAMS FOR FORTRAN USAGE*,
                 ALGORITHM NO. 539, TRANSACTIONS ON MATHEMATICAL
                 SOFTWARE, VOLUME 5, NUMBER 3, SEPTEMBER 1979, 308-323
...ROUTINES CALLED  (NONE)
...END PROLOGUE  DSWAP
*/

#ifdef WIN32
void
dswap(int n, double *dx, int incx, double *dy, int incy)
#endif
#ifdef i386
void
dswap(n, dx, incx, dy, incy)
int n;
double *dx;
int incx;
double *dy;
int incy;
#endif
{
	double dtemp1,dtemp2,dtemp3;
	int ix, iy;
	int i, m, ns, mp1;

	if (n>0){
		if (incx == incy){
			if (incx-1 < 0){
/*
       CODE FOR UNEQUAL OR NONPOSITIVE INCREMENTS.
*/
				ix = 1;
				iy = 1;
				if (incx<0)
					ix = (1-n) * incx + 1;
				if (incy<0)
					iy = (1-n) * incy + 1;
				for (i=1; i<=n; i++){
					dtemp1 = dx[ix];
					dx[ix] = dy[iy];
					dy[iy] = dtemp1;
					ix += incx;
					iy += incy;
				}
			}
			else if (incx-1 == 0){
/*
       CODE FOR BOTH INCREMENTS EQUAL TO 1


       CLEAN-UP LOOP SO REMAINING VECTOR LENGTH IS A MULTIPLE OF 3.
*/
				m = n % 3;
				if (m!=0){
					for (i=1; i<=m; i++){
						dtemp1 = dx[i];
						dx[i] = dy[i];
						dy[i] = dtemp1;
					}
					if (n<3)
						return;
				}
				mp1 =m + 1;
				for (i=mp1; i<=n; i+=3){
					dtemp1 = dx[i];
					dtemp2 = dx[i+1];
					dtemp3 = dx[i+2];
					dx[i] = dy[i];
					dx[i+1] = dy[i+1];
					dx[i+2] = dy[i+2];
					dy[i] = dtemp1;
					dy[i+1] = dtemp2;
					dy[i+2] = dtemp3;
				}
			}
			else {
/*
     CODE FOR EQUAL, POSITIVE, NONUNIT INCREMENTS.
*/
				ns = n*incx;
				for (i=1; i<=ns; i+=incx){
					dtemp1 = dx[i];
					dx[i] = dy[i];
					dy[i] = dtemp1;
				}
			}
		}
	}
}
