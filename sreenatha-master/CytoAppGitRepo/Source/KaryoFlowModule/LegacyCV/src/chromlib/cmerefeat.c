
/*
 * cmerefeat.c		Jim Piper		20-05-86
 *		Modifications
 *
 *	24 Sep 1986		GJP		change free to Free
 *	15 Jul 1997		WH		Check point isn't NULL before using it
 */

#include <stdio.h>
#include <woolz.h>
#include <chromanal.h>

#ifdef WIN32
int
centromerefeatures(struct chromosome *obj, struct object *mpol, struct object *prof)
#endif
#ifdef i386
int
centromerefeatures(obj, mpol, prof)
struct chromosome *obj;
struct object *mpol;
struct object *prof;
#endif
{
	struct ipoint *point, *ncentromere();
	struct chromplist *plist;
	

	plist = obj->plist;

	/* check if corrected centromere position exists
	   - use it of it does */

	if ((plist->Ccx == 0) && (plist->Ccy == 0)){
		/*
		* find centromere from second moment profile
		*/
		point = ncentromere(mpol,prof);
		if (point)
		{
			plist->cx = point->k;
			plist->cy = point->l;
		}
		else
		{
			plist->cx = 0;
			plist->cy = 0;
		}
	}
	else {
		point=(struct ipoint *)Malloc(sizeof(struct ipoint));
		point->k=plist->Ccx;
		point->l=plist->Ccy;
	}

	if (point != NULL) {
		plist->cangle = 0;	/* since chromosome vertical */
		plist->cindexa = cindexa(obj,point->l);
		plist->cindexm = cindexm(obj,point->l);
		plist->cindexl = cindexl(mpol,point->l);
		plist->mdra = plist->cindexa > 50 ?
			100*(100-plist->cindexm)/(101-plist->cindexa):
			100*plist->cindexm/(1+plist->cindexa);
		Free(point);
	} else {
		plist->cindexa = -1;	/* failed flag */
	}
}
