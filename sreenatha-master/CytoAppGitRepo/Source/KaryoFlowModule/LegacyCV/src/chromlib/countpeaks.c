/*
 * countpeaks.c		Jim Piper	04-10-89
 *
 * Count peaks in a density profile.
 *
 * MODIFICATIONS
 * -------------
 * 24-10-89	jimp	Add percentage of profile to be scanned (for nbindex)
 */
#include <stdio.h>
#include <woolz.h>

#define PEMAX	300		/* maximum number of extrema in a profile */

#ifdef WIN32
int
countpeaks(struct object *prof, int quantum, int profpc)
#endif
#ifdef i386
int
countpeaks(prof, quantum, profpc)
struct object *prof;
int quantum;
int profpc;
#endif
{
	register int *hv;
	register i,m,ne,max;
	int curmin, curmax, pt;
	struct histogramdomain *hdom;
	
	struct profextremum {
		int pos;	/* position */
		int minmax;	/* 1 if max, -1 if min */
		int val;	/* profile value */
	} pe[PEMAX];

	hdom = (struct histogramdomain *)prof->idom;
	/*
	 * find max value and use to determine quantisation of profile
	 */
	max = 0;
	hv = hdom->hv;
	for (i=0; i<hdom->npoints; i++) {
		if (*hv > max)
			max = *hv;
		hv++;
	}
	quantum = max*quantum/100;
	/*
	 * search profile for extrema
	 */
	profpc = profpc*hdom->npoints / 100;
	m = 1;
	ne = 0;
	curmax = 0;
	hv = hdom->hv;
	for (i=0; i<profpc && ne < PEMAX; i++) {
		switch (m) {
		case 1:			/* looking for maximum */
			if (*hv > curmax) {
				curmax = *hv;
				pt = i;
			} else if (*hv < curmax - quantum) {
				pe[ne].pos = pt;
				pe[ne].minmax = m;
				pe[ne].val = curmax;
				m *= -1;
				ne++;
				curmin = *hv;
				pt = i;
			}
			break;
		case -1:		/* looking for minimum */
			if (*hv < curmin) {
				curmin = *hv;
				pt = i;
			} else if (*hv > curmin + quantum) {
				pe[ne].pos = pt;
				pe[ne].minmax = m;
				pe[ne].val = curmin;
				m *= -1;
				ne++;
				curmax = *hv;
				pt = i;
			}
			break;
		}
		hv++;
	}
	return(ne);
}
