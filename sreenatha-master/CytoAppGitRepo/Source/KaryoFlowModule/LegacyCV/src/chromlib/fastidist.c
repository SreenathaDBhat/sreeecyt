/*
 * fastidist.c	Jim Piper	22/11/85
 *
 * prototype C file for assembly coded computation of
 * integer-variance-corrected distance for classification.
 */

#include <stdio.h>

/*
 * compute distance of feature vector from class mean vector,
 * corrected by per-class feature variances, IN INTEGER ARITHMETIC.
 */
#ifdef WIN32
int
idistvar(register int *fv, register int *mv, register int *cliv, register int dim)
#endif
#ifdef i386
int
idistvar(fv, mv, cliv, dim)
register int *fv;
register int *mv;
register int *cliv;
register int dim;
#endif
{
	register int d,dv;
	register int i;

	d = 0;
	for (i=0; i<dim; i++) {
		/* difference between sample and class mean */
		dv = *fv++ - *mv++;
		/* compute generalised Euclidean distance */
		dv *= dv;
		/*
		 * on a 68000, we can use the MULU instruction if
		 * dv*dv is less than 2**16, since we know that *cliv
		 * is positive and less than 2**16.
		 */
		if (dv < 65536)
			d += dv * *cliv++;	/* using MULU in assembler hack */
		else
			d += dv * *cliv++;	/* using C-multiply otherwise */
		/* check for overflow */
		if (d < 0)
			return(2000000000);	/* near enough 2**31 */
	}
	return(d);
}
