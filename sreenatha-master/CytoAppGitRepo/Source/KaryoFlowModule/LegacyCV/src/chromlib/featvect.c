#define register
/*
 * featvect.c	
 *
 * Get feature vector from object.
 * Also get classifier dimension,
 * and normaliser vectors.
 *
 * List of features with indicies running from 0.
 *
 *		 0: area normalised by median object area
 *		 1: length normalised by median object length
 *		 2: normalised density (cell mean = 100)
 *		 3: area centromeric index
 *		 4: ratio of mass c.i. to area c.i.
 *		 5: normalised cvdd
 *		 6: normalised nssd
 *		 7: normalised ratio mass to area c.i.
 *		 8-13: normalised wdds
 *		 14-19: normalised mwdds
 *		 20-25: normalised gwdds
 *		 26: length centromeric index
 *		 27: hull perimeter
 *		 28: number of bands
 *		 29: index based on nbands
 *
 *
 * Modifications
 *
 *	24 Jun 1999 SN	Removed dividi by zero error in norm_a_d_l.
 *	23 Apr 1999 SN	Added dynamic arrays.
 *					Disabled getclass.
 *					Removed NEWJP #define.
 *	08 Apr 1999 SN	Remove i386/WIN32 compile directives.
 *					Add some private function prototypes.
 *					Removed unreferenced variables.
 *					Added stdlib.h and chromlib.h inlcudes.
 *					Swapped int return value for void in normvec, norm_a_d_l and applynorm.
 *					Removed fp, redundant extern.
 *					Updated all function definitions to ANSI.
 *					Removed OSK #ifndefs, they must be redundant now, surely.
 *	17 Nov 1998 SN	Remove i386 compile directive in norm_hp for WIN32 version norm_hp()
 *	27 Aug 1998	WH	Remove i386 comile directive in ormvec for WIN32 version normvec()
 *  17 Jun 1994 CAJ changed for floating point maths & 30 features
 *	 6 Aug 1992	MC	added ifdef i386 statement in normvec() and norm_hp
 *	 6 Aug 1992	MC	debug o/p
 *	 7 Feb 1991	CAS	remove Mizar defs -> osk
 *	02 Dec 1988	SCG/dcb	avoid dividing fss as this can sometimes be zero
 *	16 Sep 1988	SCG	Modified norm_a_d_l and normhp so that derived
 *				objects are included in normalisation (history[0] > 0)
 *	13 Nov 1986	jimp	And yet one more additional small number fix,
 *				so that non-zero vn->A values are returned if
 *				zero or one objects input.
 *	15 Oct 1986	CAS/SCG	Further attempts to deal with silly numbers
 *				of chroms (0,1 etc) in norm_a_d_l, standardnorm
 *	19 Sep 1986	SCG/CAS	Fix standardnorm so it doesn't crash with
 *				only one object
 *	26 Nov 1985	Jimp	modified for scaled integer arithmetic
 */

//#include <stdio.h>
#include <stdlib.h>			//SN08Apr99 needed for qsort
#include <woolz.h>
#include <chromanal.h>
#include <math.h>
#include <chromlib.h>		//SN08Apr99	needed for public prototypes
#include <CreateArray.h>	//SN23Apr99 Create dynamic arrays

#undef MAXOBJS
#undef MAXDIM
#undef MAXCLASS


#define SCALEFACTOR 256.0


// SN 17Nov98 Private Function prototypes
int norm_hp( register struct chromosome **objs, int number, register struct normaliser *vn);
int norm_a_d_l(register struct chromosome * *objs, int number, register struct normaliser *vn);
void standardnorm( register struct chromosome **objs, int n, int off, register struct normaliser *vn);
int compare( const void * i, const void *j);


//extern FILE *fp;			//SN08Apr99	Redundant extern



/*
 * get feature vector
 *
 *	MODS
 *	SN	23Apr99	Return value never used so removed.
 */
//int getvec(register struct chromplist *plist, register double *v)
void getvec(register struct chromplist *plist, register double *v)
{
	register int i;


	/* area */
	v[0] = (double)plist->area;

	/* length */
	v[1] = (double)plist->length;

	/* relative density */
	v[2] = (double)plist->mass/plist->area;
	
/* centromeric index by area, ratio of mass c.i. to area c.i. */
	v[3] = (double)plist->cindexa;
	v[4] = (double)plist->cindexm;
	v[26] = (double)plist->cindexl;
	v[27] = (double)plist->hullperim;
	/* Modify c.i. appropriately if object upside-down */
	if (v[3] > 50.0) {
		v[3] = 100.0 - v[3];
		v[4] = 100.0 - v[4];
		v[26] = 100.0 - v[26];
	}
	/* Convert v[4] to ratio - watch for division by zero */
	if (v[3] < 1.0) {
		v[3] = 1.0;
		v[4] = 1.0;
	}
	v[4] = 100.0 * v[4] / v[3];

	/* cvdd, nsdd, ddm1, mdra */
	v[5] = (double)plist->cvdd;
	v[6] = (double)plist->nssd;
	v[7] = (double)plist->mdra;

	/* wdd of density profile */
	/* wdd of 2nd moment of density profile */
	/* wdd of gradient of density profile */
	for (i=0; i<6; i++) {
		v[8+i] = (double)plist->wdd[i];
		v[14+i] = (double)plist->mwdd[i];
		v[20+i] = (double)plist->gwdd[i];
	}


    v[28] = (double)plist->nbands;
    v[29] = (double)plist->nbindex;

	//return(30);											//SN23Apr99
}


//
// getdim
//
//	MODS
//	SN	23Apr99	Remove input paramter.
//
//int getdim(int print)
int getdim( void)
{
	//return(30);
	return(NUMOFFEATURES);
}


//SN23Apr99 No longer used anywhere in WinCV
#if 0
int getclass(int print)
{
	if (print)
		printf("\n24 classes (22 autosomes, X = 23, Y = 24)\n");
	return(24);
}
#endif


/*
 * Compute a normalisation vector for classifier - compute pairs
 * of constants A and B such that X = (Ax + B) / SCALEFACTOR.
 *
 * THE VECTOR OF NORMALISERS SHOULD CORRESPOND TO THE VECTOR OF FEATURES
 * IN getvec()
 *
 * MODS
 * SN 08Apr99	Swapped int return value for void.
 * SN 23Apr99	Return int, 0 - memory error, 1 - ok.
 *
 */
//void normvec(struct chromosome * *objs, int number, struct normaliser *vn)
int normvec(struct chromosome * *objs, int number, struct normaliser *vn)
{
	int i, nOK;
	struct chromplist plist;


	/* area, density/mass, length */
	nOK = norm_a_d_l(objs,number,&vn[0]);
	if (!nOK)
		return(0);								//SN23Apr99 memory error

	/* hull perimeter */
	nOK = norm_hp(objs,number,&vn[27]);			
	if (!nOK)
		return(0);								//SN23Apr99 memory error


	/* FOLLOWING CONVERSION EQUIVALENT TO "fstand1.c" ON VAX */
	vn[3].A = 10.0 * SCALEFACTOR;
	vn[3].B = -250.0 * SCALEFACTOR;
	vn[4].A = SCALEFACTOR;
	vn[4].B = 0.0;

	vn[26].A = SCALEFACTOR; vn[26].B = 0.0;		/* length c.i. */

#ifdef DEBUG
	fprintf(stderr,"vn[3] A is %f, B is %f scalefactor is %f\n",vn[3].A,vn[3].B,SCALEFACTOR);
	fprintf(stderr,"vn[26] A is %f, B is %f scalefactor is %f\n",vn[26].A,vn[26].B,SCALEFACTOR);
#endif
	/* cvdd, nssd, ddm1, mdra */
	standardnorm(objs,number,((char *)&plist.cvdd - (char *)&plist.size),&vn[5]);
	standardnorm(objs,number,((char *)&plist.nssd - (char *)&plist.size),&vn[6]);
	standardnorm(objs,number,((char *)&plist.mdra - (char *)&plist.size),&vn[7]);
	standardnorm(objs,number,((char *)&plist.nbands - (char *)&plist.size),&vn[28]);
	standardnorm(objs,number,((char *)&plist.nbindex - (char *)&plist.size),&vn[29]);

	/* wdd distributions normalised to standard mean 0, s.d. 100 */
	for (i=0; i<6; i++) {
		standardnorm(objs,number,((char *)&plist.wdd[i] - (char *)&plist.size),&vn[8+i]);
		standardnorm(objs,number,((char *)&plist.mwdd[i] - (char *)&plist.size),&vn[14+i]);
		standardnorm(objs,number,((char *)&plist.gwdd[i] - (char *)&plist.size),&vn[20+i]);
	}
#ifdef DEBUG
	fprintf(stderr,"vn[26] A is %f, B is %f\n",vn[26].A,vn[26].B);
#endif

	// Everything's ok											//SN23Apr99
	return(1);
}


/*
 * normalise area so that object median area = 500
 * normalise cell mass so that mean relative density is 100
 * normalise length so that object median length = 50
 *
 * MODS
 * SN 08Apr99	Swapped int return value for void.
 * SN 23Apr99	Create dynamic arrays.
 *				Return int, 0 - memory error, 1 - ok.
 * SN 24Jun99	Avoid divide by zero problems. Occurs for median length when many small objects present.
 *
 */
int norm_a_d_l(register struct chromosome * *objs, int number, register struct normaliser *vn)
{
	register struct chromplist *p;
	register int i,nchr;
	int nchr2;
	int farrayOK;
	int *areas = NULL;				// number array					//SN23Apr99
	int *lengths = NULL;			// number array					//SN23Apr99
	int median_area, median_length;
	//int areas[MAXOBJS],nchr2;
	//int lengths[MAXOBJS];
	TCHAR szfnname[] = _T("norm_a_d_l");
    double tarea, tmass;


	// Create arrays												//SN23Apr99
	farrayOK = 1;
	areas = (int *) CreateArray1D( number, sizeof(int), szfnname, _T("areas"), &farrayOK);
	lengths = (int *) CreateArray1D( number, sizeof(int), szfnname, _T("lengths"), &farrayOK);
	if (!farrayOK)
	{
		DestroyArray1D( areas);
		DestroyArray1D( lengths);
		return(0);
	}

	// Count number of objects of interest, nchr
	tmass = 0.0;
	tarea = 0.0;
	nchr = 0;	//0.0;		//SN08Apr99 Removed bizarre initialisation of integer variable.
	for (i=0; i<number; i++) {
		/*
		 * check object history - we accept underived objects only,
		 * giving worst-case least-assumption normalisation of size
		 */
		p = objs[i]->plist;
/*		if (p->otype <= 3 && p->history[0] == 0) { */
		if(p->otype <= 1) {
			tarea += (double)p->area;
			tmass += (double)p->mass;
			areas[nchr] = p->area;
			lengths[nchr++] = p->length;
		}
	}


	// Calculate normalising coefficients
	if (nchr) {
		qsort(areas,nchr,sizeof(int),compare);
		qsort(lengths,nchr,sizeof(int),compare);

		/* take mean of central 2 (if even number) */
		// And dont forget to check for divide by zero					//SN24Jun99
		nchr2 = nchr>>1;
		if ((nchr&1) == 0) {
			median_area = areas[nchr2-1]+areas[nchr2];
			if (median_area != 0)		vn[0].A = 1000.0 * SCALEFACTOR/(double)median_area;
			else						vn[0].A = 1;

			median_length = lengths[nchr2-1]+lengths[nchr2];
			if (median_length != 0)		vn[1].A = 100.0  * SCALEFACTOR/(double)median_length;
			else						vn[1].A = 1;
		} else {
			median_area = areas[nchr2];
			if (median_area != 0)		vn[0].A = 500.0  * SCALEFACTOR/(double)median_area;
			else						vn[0].A = 1;

			median_length = lengths[nchr2];
			if (median_length != 0)		vn[1].A = 50.0   * SCALEFACTOR/(double)median_length;
			else						vn[1].A = 1;
		}
		vn[0].B = 0.0;
		vn[1].B = 0.0;


        vn[7].A = vn[1].A;    /* length   a j.p. */
        vn[7].B = vn[1].B;

		/*
		 * the aim of the following is to normalise the mean density
		 * to 100.  The following assumes that the area has been
		 * extracted RAW by getvec() for this feature !!!!!
		 */
		vn[2].A = 100.0 * tarea * SCALEFACTOR / tmass;
		vn[2].B = 0.0;

	} else {
		vn[0].A = 1;
		vn[1].A = 1;
		vn[2].A = 1;
		vn[0].B = 0;
		vn[1].B = 0;
		vn[2].B = 0;
	}


	// Destroy arrays											//SN23Apr99
	DestroyArray1D( areas);
	DestroyArray1D( lengths);

	// Everything's ok											//SN23Apr99
	return(1);
}


//compare(i,j)
//register int *i,*j;
int compare( const void * i, const void *j)		//SN08Apr99 Make parameters compatible with qsort definition.
{
	//return(*j - *i);
	return( *((int *) j) - *((int *) i) );
}


/*
 * standardise short integer member of property list to mean zero, s.d. 100.
 * BUT IGNORE NON-CHROMOSOMES WHEN COMPUTING THE NORMALISERS.
 * "off" is (byte) offset in propertylist to value in question.
 */
void standardnorm( register struct chromosome **objs, int n, int off, register struct normaliser *vn)
{
	register struct chromplist *plist;
	register int i,v;
	int nc;
	double s, ss;
	short *o;


	s = 0.0;
	ss = 0.0;
	nc = 0;
	for (i=0; i<n; i++) {
		plist = objs[i]->plist;
		if (plist->otype <= 1) {
			nc++;
			o = (short*) ((char *)plist + off);
			v = *o;
			s += v;
			ss += v*v;
		}
	}
	if (nc <= 1) {
		vn->A = 1.0;
		vn->B = 0.0;
	} else {
        s /= nc;
        ss = sqrt((ss - nc*s*s)/(nc-1));
		/*
		 * In certain rare occassions ss can be zero; avoid division
		 */
		if(ss != 0)
			vn->A = 100.0 * SCALEFACTOR/ss;
		else
			vn->A = 100.0;
		vn->B = - (s * vn->A);
	}
}


/*
 * specify which features are to be used for classification.
 * Note that as usual the features are numbered from zero.
 * Program "vcv" REQUIRES SELECTED FEATURES TO BE IN ASCENDING ORDER !
 */
void selectedfeatures( int * sfv, int * nsfv)
{
/*
	*nsfv = 15;
	sfv[0] = 0;
	sfv[1] = 2;
	sfv[2] = 3;
	sfv[3] = 5;
	sfv[4] = 6;
	sfv[5] = 8;
	sfv[6] = 9;
	sfv[7] = 11;
	sfv[8] = 12;
	sfv[9] = 15;
	sfv[10] = 17;
	sfv[11] = 20;
	sfv[12] = 21;
	sfv[13] = 23;
	sfv[14] = 24;
*/
	*nsfv = 3;
	sfv[0] = 0;
	sfv[1] = 9;
	sfv[2] = 15;
}


/*
 * normalise hull perimeter by median
 *
 *	MODS
 * SN 17Nov98	No return specified in definition so made void.
 *				Also updated definition.
 * SN 23Apr99	Create dynamic arrays.
 *				Return int, 0 - memory error, 1 - ok.
 */
//void norm_hp( register struct chromosome **objs, int number, register struct normaliser *vn)
int norm_hp( register struct chromosome **objs, int number, register struct normaliser *vn)
{
	register struct chromplist *p;
	register int i,nchr,nchr2;
    double medianh;
	int farrayOK;
	int *hullp = NULL;					// number array			//SN23Apr99
	//int hullp[MAXOBJS]; //,compare();							//SN08Apr99 Superceded by prototype


	// Create array												//SN23Apr99
	farrayOK = 1;
	hullp = (int *) CreateArray1D( number, sizeof(int), _T("norm_hp"), _T("hullp"), &farrayOK);
	if (!farrayOK)
		return(0);

	// Count number of objects of interest, nchr
	nchr = 0;
	for (i=0; i<number; i++) {
		/*
		 * check object history - we accept underived objects only,
		 * giving worst-case least-assumption normalisation of size
		 */
		p = objs[i]->plist;
/*		if (p->otype <= 3 && p->history[0] == 0) */
		if(p->otype <= 1)
			hullp[nchr++] = p->hullperim;
	}

	// Calculate hull perimeter normalising coefficients, vn
	qsort(hullp,nchr,sizeof(int),compare);
	/* take mean of central 2 (if even number) */
	nchr2 = nchr>>1;
	if ((nchr&1) == 0)
		medianh = hullp[nchr2-1]+hullp[nchr2];
	else 
		medianh = 2*hullp[nchr2];
	if (medianh == 0)
		vn->A = SCALEFACTOR;
	else
		vn->A = 1000.0 * SCALEFACTOR / medianh;

	/* FOLLOWING CONVERSION EQUIVALENT TO "fstand1.c" ON VAX */
	vn->B = -500.0 * SCALEFACTOR;

	// Destroy array											//SN23Apr99
	DestroyArray1D( hullp);

	// Everythings ok											//SN23Apr99
	return(1);
}


/*
 * normalise feature vector 
 *
 * MODS
 * SN	26Apr99	Removed ndim, redundant parameter.
 */
//void applynorm(register double *fvec,register double *svec,int *sfv,register struct normaliser *vn, int ndim, register int nsfv)
void applynorm(register double *fvec,register double *svec,int *sfv,register struct normaliser *vn, register int nsfv)
{
	register int i,j;
    double f;


/* FOLLOWING IS EQUIVALENT TO "fstand1.c" ON VAX */
	fvec[0] = (vn[0].A * fvec[0] + vn[0].B) / SCALEFACTOR;
	fvec[1] = (vn[1].A * fvec[1] + vn[1].B) / SCALEFACTOR;
	/* mean of length and area */

	fvec[1] = 10.0*fvec[1] + fvec[0];
	fvec[0] -= 500.0;
	fvec[1] = (fvec[1] - 1000.0)/3.0;
/* OTHER "fstand1.c" CONVERSIONS PERFORMED IN "featvect.c"  ON OSK */

	for (i=0; i<nsfv; i++){
		j = *sfv++;
		f = fvec[j];
		/* normalise feature vector if not already done above */
		if (j > 1)
			f = (f * vn[j].A + vn[j].B) / SCALEFACTOR;

#ifdef DEBUG4
fprintf(stderr,"\n%2d %2d %8f",i,*(sfv-1),f);
#endif /* DEBUG4 */

		*svec++ = f;

#ifdef DEBUG4
fprintf(stderr," %8d",*(ivec-1));
#endif /* DEBUG4 */

	}

#ifdef DEBUG4
fprintf(stderr," apply normaliser");
#endif 
}
