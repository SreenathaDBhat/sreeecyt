
/*
 * mfeatures.c		7/3/84		Jim Piper
 *
 * compute features of chromosome objects
 *
 * Who		when		Significant modification
 * ---		----		------------------------
 *	MG		7Apr99		xcale and yscale (needed for vertical()) now passed into chromfeatures
 *  CAJ     20/6/94     extra features nbands and nbindex added.
 *	18 Nov 1987	SCG	Do not allow higher feature measurement
 *				on one pixel wide objects.  This would
 *				produce a zero profile and subsequent zero
 *				division in wdd. (This is now corrected).
 *  JP		29/10/85	obj->plist no longer set to be NULL.
 *  JP		14/05/86	"fipconvhull" used for FIP digitised objects
 *  JP		20/05/86	split into smaller modules
 *	JP		07/07/86	test for small objects moved to AFTER rotation
 *	11 Sep 1986		CAS		Includes
 */

#include <stdio.h>
#include <woolz.h>
#include <chromanal.h>
#define PROFSMOOTH 1

/*
 * select which profiles should be used, one for band parameters,
 * one for centromere location.  We already know that profile "0" (density)
 * is better than profile "-1" (mean density)  or profile "-2" (max
 * density) for wdd features.
 */
static int whichprof[2] = {0,2};


#ifdef WIN32
struct chromosome *
chromfeatures(struct chromosome *obj, int basic, double xscale, double yscale)
#endif
#ifdef i386
struct chromosome *
chromfeatures(obj, basic, xscale, yscale)
struct chromosome *obj;
int basic;
double xscale;
double yscale;
#endif
{
	struct chromosome *robj, *vertical();
	struct object *mpol, *chromaxis2(), *prof[2];
/*
    struct object *cobj, *cleanbinobj(); * added for future possible mod 
                                            see j.p.version *
*/
	struct object *cvh, *convhull();
	struct histogramdomain *h;

	if (basic != 0) {
		basicfeatures(obj);
		robj = vertical(obj, xscale, yscale);
		/*
		 * Small objects can cause trouble e.g. in axis finding.
		 * Don't do any further processing of anything that is
		 * too small here.  Other feature values will be zero,
		 * but since this cannot be a chromosome this does not matter.
		 *
 		 * To keep things tidy set the centromere to the mid point
 		 * of the object.
		 */
		if ((robj->idom->lastln - robj->idom->line1) < 8 ||
				(robj->idom->lastkl - robj->idom->kol1) < 8) {
			
			robj->plist->cx = (robj->idom->lastkl + robj->idom->kol1) / 2;
			robj->plist->cy = (robj->idom->lastln + robj->idom->line1) / 2;
			return(robj);
		}
	} else {
		cvh = convhull(obj);
		obj->plist->hullarea = hullarea(cvh, &obj->plist->hullperim);
		freeobj(cvh);
		robj = obj;
	}

/*  future possible improvement to axis finding, see j.p. version */
/*  cobj = cleanbinobj (robj);
    mpol = chromaxis2(cobj);
    freeobj(cobj);
*/
	mpol = chromaxis2(robj);
	/*
	 * find density and moment profiles, and shorten mid-points polygon
	 * and profile if tips lie outside object.  Then chromosome length
	 * is resulting profile length.
	 */
	multiprofiles(robj,mpol,prof,whichprof,2);
	shorten(mpol,prof[0],prof[1]);
	robj->plist->length = ((struct polygondomain *)(mpol->idom))->nvertices;
	/* set tip values to zero */
	h = (struct histogramdomain *)prof[1]->idom;
	h->hv[0] = h->hv[h->npoints-1] = 0;
	/*
	 * Smoothing possibly improves the power of wdd features,
	 * so perhaps we should smooth more strongly here.
	 */
	histosmooth(prof[0], PROFSMOOTH);
	histosmooth(prof[1], PROFSMOOTH+2);
	/*
	 * find centromere, measure features
	 */
	centromerefeatures(robj,mpol,prof[1]);
	/* 
	 * Number of bands and band index
	 */
    robj->plist->nbands = 2 + countpeaks(prof[0],1,100); /* 1% quantum, full length */
    robj->plist->nbindex = ((1 + countpeaks(prof[0],1,50)) * 100 + robj->plist->nbands/2) / robj->plist->nbands;
                                                         /* 1% quantum, half length */
    if (robj->plist->nbindex > 50)
		robj->plist->nbindex = 100 - robj->plist->nbindex;

	/*
	 * profile density distribution features
	 */
	wddfeatures(robj,prof[0],prof[1]);
	freeobj(mpol);
	freeobj(prof[0]);
	freeobj(prof[1]);
	return (robj);
}


#ifdef WIN32
int
basicfeatures(struct chromosome *obj)
#endif
#ifdef i386
int
basicfeatures(obj)
struct chromosome *obj;
#endif
{
	struct chromplist *plist;
	plist = obj->plist;
	plist->area = area(obj);
	plist->mass = mass(obj);
}


/* 
 * new centromere should be called after rotation or straighten of object
 */
#ifdef WIN32
void
new_centromere(struct chromosome *obj)
#endif
#ifdef i386
void
new_centromere(obj)
struct chromosome *obj;
#endif
{
	struct chromosome *vertical();
	struct object *mpol, *chromaxis2(), *prof[2];
	struct object *convhull();
	struct histogramdomain *h;
	struct ipoint *point, *ncentromere();
	struct chromplist *plist;

	mpol = chromaxis2(obj);

	/*
	 * find density and moment profiles, and shorten mid-points polygon
	 * and profile if tips lie outside object.
	 */
	multiprofiles(obj,mpol,prof,whichprof,2);
	shorten(mpol,prof[0],prof[1]);

	/* set tip values to zero */
	h = (struct histogramdomain *)prof[1]->idom;
	h->hv[0] = h->hv[h->npoints-1] = 0;
	histosmooth(prof[1], PROFSMOOTH+2);
	/*
	 * find centromere
	 */
	point = ncentromere(mpol,prof[1]);
	plist = obj->plist;
	if (point != NULL) {
		plist->cx = point->k;
		plist->cy = point->l;
		Free(point);
	} else {
		plist->cindexa = -1;	/* failed flag */
	}

	freeobj(mpol);
	freeobj(prof[0]);
	freeobj(prof[1]);
}

