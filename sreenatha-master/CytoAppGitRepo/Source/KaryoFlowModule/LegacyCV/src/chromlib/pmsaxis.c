/*
 * pmsaxis.c		Jim Piper @ AUC			11 May 1986
 *
 * Make an axis using a modification of the poor man's skeleton
 * procedure.  I.e. assume chromosome vertical, and find mid-points
 * of horizontal slices through chromosome, with the following
 * modifications:
 *	(1) Sample at vertical intervals separated by "step"
 *	(2) Start sampling at distance "tip" from top and bottom.
 *	(3) Force first "tipstraight" samples to lie on straight
 *		line, by skipping the intermediate sample points and
 *		then filling them in by linear interpolation.
 *	(4) make the tip values by linear interpolation from the
 *		straight tip section.
 * The resulting axis can be smoothed as usual by "axissmooth()".
 *
 * Modifications
 *
 *	14 Oct 1996		MG	KS divide by zero test was ignoring the fact that
 *					dy could legitimately be negative - fixed
 *					Note: due to the way the vertices of the polygon
 *					are generated it is very unlikely (impossible ?)
 *					that a divide by zero could ever occur.
 *					Also modified KS fix for small objects - this now
 *					generates a true 3 point vertical mid-point point
 *					poly based on the original object's centre of idom.
 *
 *	23 Jul 1996		KS	Cleaned up two places where this crashed :
 *					1) if nl <= 3 return pdom as first line,col to last line/col, and
 *					2) a possible divide by zero
 *	 7 Dec 1988		CAS		Only copy back if there is a gap..
 *	05 Dec 1988		SCG/CAS	Dont interpolate intermediate undefined points; better
 *							to copy back and reduce pdom->nvertices
 *	02 Dec 1988		SCG/dcb	Rewritten the code in the main loop to cope
 *							with joined objects
 *	11 Sep 1986		CAS		Includes
 */

#include <stdio.h>
#include <woolz.h>

#ifdef WIN32
struct object *
pmsaxis(struct object *obj, int tip, int step, int tipstraight)
#endif
#ifdef i386
struct object *
pmsaxis(obj, tip, step, tipstraight)
struct object *obj;
int tip;
int step;
int tipstraight;
#endif
{
	struct polygondomain *pdom, *makepolydmn();
	register struct ivertex *vtx,*wtx,*xtx;
	struct intervaldomain *idom;
	struct interval *itvl;
	struct object *ax, *makemain();
	register int i, l, l1, l2, n;
	int nl, nl2, ts, dy, count, gap;
	int midx, midy;

	idom = obj->idom;
	nl = 3 + (idom->lastln - idom->line1 - 2*tip + step/2)/step;
	if (nl <= 3) {
/*
KS replaced ...
	if (nl <= 3)
		nl = 4;
with ...
*/
/* MG modified to create true 3 point mid-point vertical axis */

		pdom = makepolydmn(1,NULL,0,3,1);
		pdom->nvertices = 3;
		ax = makemain(10,pdom,NULL,NULL,NULL);

		midx=(idom->kol1 + idom->lastkl)/2;
		midy=(idom->line1 + idom->lastln)/2;

		pdom->vtx[0].vtX = midx;
		pdom->vtx[0].vtY = midy-1;
		pdom->vtx[1].vtX = midx;
		pdom->vtx[1].vtY = midy;
		pdom->vtx[2].vtX = midx;
		pdom->vtx[2].vtY = midy+1;
		
		return(ax);
	}

	/*
	 * if the object is rather short, don't permit a middle point
	 */
	if (nl < 2*(tipstraight+1))
		nl &= 0xfffe;
	nl &= 0xfffffffe;
	pdom = makepolydmn(1,NULL,0,nl,1);
	pdom->nvertices = nl;
	vtx = pdom->vtx + 1;
	ax = makemain(10,pdom,NULL,NULL,NULL);
	/*
	 * loop, filling axis polygon
	 */
	nl2 = nl/2 - 1;
	wtx = vtx + nl - 3;
	for (i=0; i<nl2; i++) {
		l1 = tip + i*step; 
		l2 = idom->lastln - idom->line1 - l1;
		/*
		 * If this interval is not empty define this vertex in the polygon
		 * and step forward
		 */
		if ((n = idom->intvlines[l1].nintvs) != 0) {
			itvl = idom->intvlines[l1].intvs;
			vtx->vtX = idom->kol1 + (itvl->ileft + itvl[n-1].iright)/2;
			vtx->vtY = l1 + idom->line1;
			vtx++;
		}
		/*
		 * If this interval is not empty define this vertex in the polygon
		 * and step back
		 */
		if ((n = idom->intvlines[l2].nintvs) != 0) {
			itvl = idom->intvlines[l2].intvs;
			wtx->vtX = idom->kol1 + (itvl->ileft + itvl[n-1].iright)/2;
			wtx->vtY = l2 + idom->line1;
			wtx--;
		}
		/*
		 * force skipping over the "tip-straight" section
		 */
		if (i == 0) {
			i += tipstraight-1;
			vtx += tipstraight-1;
			wtx -= tipstraight-1;
		}
	}
	/*
	 * remove intermediate undefined points by copying back 
	 * reduce pdom->nvertices accordingly
	 */
	count = pdom->vtx + pdom->nvertices - wtx - 1;
	gap = wtx - vtx + 1;
	if (gap > 0) {
		pdom->nvertices -= gap;
		wtx++;
		while (count--) {
			vtx->vtX = wtx->vtX;
			vtx->vtY = wtx->vtY;
			vtx++;
			wtx++;
		}
	}
	nl = pdom->nvertices;
	nl2 = nl/2 - 1;
	/*
	 * Fill in the end-points and the "tip-straight" section
	 * by linear interpolation.
	 */
	ts = tipstraight;
	if (nl2 < tipstraight)
		ts = nl2;
	vtx = pdom->vtx;
	wtx = vtx+1;
	xtx = wtx+tipstraight;
	if (nl2 <= tipstraight)
		xtx = wtx+nl-3;
	for (i = -1; i<ts; i++,vtx++) {
		l = i<0? idom->line1-2: wtx->vtY+i*step;
/* MG check for div by 0 - probably not needed */
		dy=(wtx->vtY-xtx->vtY);

		if (dy == 0) 
			dy=1;

		vtx->vtX = wtx->vtX +
			(l-wtx->vtY) * (wtx->vtX-xtx->vtX) / dy;
		vtx->vtY = l;
	}
	vtx = pdom->vtx + nl - 1;
	wtx = vtx-1;
	xtx = wtx-tipstraight;
	if (nl2 <= tipstraight)
		xtx = wtx-nl+3;
	for (i = -1; i<ts; i++,vtx--) {
		l = i<0? idom->lastln+2: wtx->vtY-i*step;
/* MG check for div by 0 - probably not needed */
		dy=(wtx->vtY-xtx->vtY);

		if (dy == 0) 
			dy=1;

		vtx->vtX = wtx->vtX +
			(l-wtx->vtY) * (wtx->vtX-xtx->vtX) / dy;
		vtx->vtY = l;
	}

	return(ax);
}
