/*
 * probotype.c		M.Gregson
 *
 *
 * Use simple tests to decide probable type of object,
 * currently: NOISE, CHROMOSOME or COMPOSITE on basis of areas and curvature
 *
 *
 *	Modifications:
 *		
 *	10Apr96		MG	Added protection to peaks.
 */
#include <stdio.h>
#include <woolz.h>
#include <chromanal.h>



#ifdef WIN32
int
peaks(struct object *obj, int interv)
#endif
#ifdef i386
int
peaks(obj, interv)
struct object *obj;
int interv;
#endif
{
	/* analyse curvature of object and count the number of convex peaks */
	struct object *h, *bobj, *uobj;
	struct histogramdomain *hdom;
	int *pt;
	int i, n, start, end, peaks;
	int min, max;

	if (!obj)
		return(0);

	bobj = ibound(obj,1);
	if (!bobj)
		return(0);

	uobj= unitbound(bobj);
	freeobj(bobj);

	if (!uobj) 
		return(0);


	h = curvat(uobj);
	freeobj(uobj);

	if (!h) 
		return(0);

	curvsmooth(h,9);

	hdom=(struct histogramdomain *)h->idom;
	n=hdom->npoints;
	pt=hdom->hv;

	start=0;
	end=0;
	peaks=0;
	min=0;
	max=0;

	for (i=1; i<n; i++) {
		if (*pt < -20 ) {
			if (start>0)
				end=i;
			else
				start=i;
		}
		else {
			if (end >0) {
				if (end-start>interv)
					peaks++;

				start=0;
				end=0;
			}
		}
		if (*pt<min)
			min=*pt;
		if (*pt>max)
			max=*pt;
		pt++;
	}

	freeobj(h);

	return(peaks);
}




#ifdef WIN32
int
probotype(struct chromosome *obj)
#endif
#ifdef i386
int
probotype(obj)
struct chromosome *obj;
#endif
{
	struct chromplist *plist = obj->plist;
	struct chromosome *hull;
	float ratio;
	int tips;
	int minjunk_area = 100;
	int maxjunk_area = 10000;
	double scale = 1;

	/*
	 * Otype already set with high confidence ??
	 * In which case, the type of guessing done
	 * here is NOT required !!
	 */
	if (plist->Cotype != 0 || plist->otconf == 100)
		return(0);

	/* Area known ? */
	if (plist->area == 0)
		plist->area = area(obj);
	
	//if original image width and height has been set then calculate size of min and max junk
	if((plist->pixel_X_micronsize > 0) && (plist->pixel_Y_micronsize > 0))
	{
			//junk area adjusted for imaging system (camera pixel size and microscope magnification)
			//normalised to a Cohu camera (pixel size 8.6x8.3) and 100x object with a .7x adaptor
		minjunk_area = 100 * (COHU_PIXEL_WIDTH * COHU_PIXEL_HEIGHT) / (plist->pixel_X_micronsize * plist->pixel_Y_micronsize);
		maxjunk_area = 10000 * (COHU_PIXEL_WIDTH * COHU_PIXEL_HEIGHT) / (plist->pixel_X_micronsize * plist->pixel_Y_micronsize);
		scale = COHU_PIXEL_WIDTH  / plist->pixel_X_micronsize ;
		if((scale > 0.9) && (scale < 1))
			scale = 1; /// cohu images will give scale approximately 0.98 or 0.99 so just make it 1
	}

	/* small area => noise */
	if (plist->area < minjunk_area) {
		plist->otype = NOISE;
		plist->otconf = 20;
		return(NOISE);
	}

	/* very large area => composite */
	if (plist->area > maxjunk_area) {
			plist->otype = COMPOSITE;
			plist->otconf = 20;
			return(COMPOSITE);
	}

	if (plist->hullarea == 0) {
		hull=convhull(obj);
		plist->hullarea=hullarea(hull,&plist->hullperim);
		freeobj(hull);
	}

	ratio=(float)plist->area/(float)plist->hullarea;

	/* ratio of hull area to area < 0.75 => possible composite */
	if (ratio < 0.80 ) {
		/* look for chromsomsome tips - greater than two => composite */
		tips=peaks(obj, 4 * scale);
		
		if (tips>2) {
			plist->otype = COMPOSITE;
			plist->otconf = 20;
			return(COMPOSITE);
		}
	}

	plist->otype = CHROMOSOME;
	plist->otconf = 20;
	return(CHROMOSOME);	/* the default */

}


