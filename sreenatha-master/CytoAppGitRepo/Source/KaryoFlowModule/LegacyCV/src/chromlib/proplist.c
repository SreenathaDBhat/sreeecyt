/*
 * proplist.c
 *
 * Modifications
 *
 *	11 Sep 1986		CAS		Includes
 */

#include <stdio.h>
#include <woolz.h>
#include <chromanal.h>


#ifdef WIN32
struct propertylist *
makechromplist()
#endif
#ifdef i386
struct propertylist *
makechromplist()
#endif
{
	struct propertylist *p;
	p = (struct propertylist *) Calloc(sizeof(struct chromplist),1);
	p->size = sizeof(struct chromplist);
	return(p);
}
