/*
 * rearrange.c		Jim Piper	31/5/84
 *
 * re-arrange an initial maximum likelihood classification in attempt
 * to move objects from over-subscribed to under-subscribed classes.
 *
 * VERSION OF 2 May 1985 FOR LOG-LIKELIHOODS
 *
 * THIS PROGRAM IS RECURSIVE AND THE CODE IS IMPENETRABLE - 
 * TAKE GREAT CARE BEFORE MODIFYING !!!!!!
 *
 ************************************************************************
 * OUTSTANDING BUG - 
 *	If the number of chromosomes in a class exceeds MAXCHILD
 *	then only the first MAXCHILD should be considered as part
 *	of a chain out of the class.  But this does not yet work
 *	and so MAXCHILD is set to 10, which is ridiculously high.
 ************************************************************************
 *
 * Modifications
 *
 *	29 Nov 1990		CAS/IH	Fix some strange struct/array refs for compiler
 *  12 Jul 1989		SEAJ	Check on classcontp->n now >= MAXCHILD.
 *	23 Feb 1989		SCG/CAS	maxchn true count of where chn is; not high water mark
 *	 1 Apr 1987		CAS		Rermoved 'too many chains' message..
 *	24 Sep 1986		GJP		free to Free
 *	11 Sep 1986		CAS		Includes
 */


#include <stdio.h>
#include <woolz.h>
#include <chromanal.h>

#define NCHAINS 100
static int maxchn, maxchflag;


/*
 * Iterate the following procedure:
 * (1)	For every chromosome in an oversubscribed class,
 *	compute best possible move to an undersubscribed class for
 *	which the likelihood is one of the first "CUTOFF" values.
 *	use cost = P(i|ch) - P(k|ch), i.e.
 *	reduction in overall likelihood
 * (2)	Implement the overall best move
 * (3)	Iterate until no oversubscribed classes.
 */
#ifdef WIN32
int
rearrange(struct chromcl *chrcl, struct classcont *clcontents, int numobj, int *clwn, int nclass)
#endif
#ifdef i386
int
rearrange(chrcl, clcontents, numobj, clwn, nclass)
struct chromcl *chrcl;
struct classcont *clcontents;
int numobj;
int *clwn;
int nclass;
#endif
{
	struct classcont *classcontp;
	struct chchain *chchn;
	int i,j,nj;
	int tobemoved;
	double minmcost,iaccum;
	struct chromcl *chrcp;
	struct chchain *chn, *chnj, *followchains(), *chainval(), *bestchain;

	chchn = (struct chchain *) Calloc(NCHAINS,sizeof(struct chchain));
	maxchflag = 0;
	/*
	 * stage (1) :
	 */
	do {
#ifdef DEBUG
		fprintf(stderr,"REARRANGE - STAGE 1\n");
#endif /* DEBUG */
		tobemoved = 0;
		chn = chchn;
		maxchn = 0;
		for (i=0;  i<nclass;  i++) {
			classcontp = &clcontents[i];
#ifdef DEBUG
			fprintf(stderr,"class %d n %d t %d\n",i,classcontp->n,clwn[i]);
#endif /* DEBUG */
			/*
			 * oversubscribed ?
			 */
			if (classcontp->n > clwn[i]) {
				/*
				 * look at each chromosome in turn
				 */
				nj = classcontp->n;
				if (nj >= MAXCHILD) {
					if (maxchflag == 0) {
						maxchflag = 1;
						fprintf(stderr,"REARRANGE: too many children\n");
					}
					nj = MAXCHILD;
				}
				for (j=0;  j<nj;  j++) {
					chrcp = classcontp->chcl[j];
					iaccum = 0.0;
					chnj = followchains(&chn,chrcp,&iaccum,MAXCHAIN,clcontents,nclass,clwn);
					if (chnj != NULL) {
						chrcp->moveval = chainval(chnj,MAXCHAIN)->accum_cost;
						chrcp->movechain = chnj;
						tobemoved++;
					} 
					else {
					    minmcost = 2000000.0;
					    chrcp->moveval = minmcost;
					}
				}
			}
		}
		/*
		 * stage (2a) - find best move this iteration.
		 */
#ifdef DEBUG
		fprintf(stderr,"REARRANGE - STAGE 2\n");
#endif /* DEBUG */
		minmcost = 1000000.0;
		if (tobemoved > 0) {
			for (i=0;  i<nclass;  i++) {
				classcontp = &clcontents[i];
				if (classcontp->n > clwn[i]) {
					nj = classcontp->n;
					if (nj > MAXCHILD)
						nj = MAXCHILD;
					for (j=0;  j<nj;  j++) {
						chrcp = classcontp->chcl[j];
						if (chrcp->moveval <= minmcost) {
							bestchain = chrcp->movechain;
							minmcost = chrcp->moveval;
						}
					}
				}
			}
			/*
			 * stage (2b) - implement the move
			 */
#ifdef DEBUG1
			fprintf(stderr,"---------------IMPLEMENT A CHAIN-------------------\n");
#endif /* DEBUG1  */
			movechain(bestchain,clcontents);
#ifdef DEBUG1
			fprintf(stderr,"\n---------------------------------------------------\n");
#endif /* DEBUG1 */
		}
	}  while (tobemoved > 0);
	/* how many chchain structures were used ?? */
#ifdef DEBUG
	fprintf(stderr,"REARRANGE: %d chains required (%d available)\n",
		maxchn, NCHAINS);
#endif /* DEBUG */
	Free (chchn);
}

/*
 * recursively construct a re-arrangement chain, passing
 * the accumulated cost forward.
 *
 * This function is called with a chromosome class pointer chrcp.
 * The aim is to measure the loss of likelihood if this chromosome
 * is assigned to another class (subject to constraints).  There
 * are then two cases:
 *	(i) the assigned class has too few chromosomes.  Stop the chain.
 *	(ii) the assigned class has enough or more than enough chromosomes.
 *	     If the maximum chain length has been reached, prohibit this
 *	     chain, otherwise try to extend by moving a chromosome from
 *	     this class to another by calling followchains recursively.
 */
#ifdef WIN32
struct chchain *
followchains(struct chchain * *chn, struct chromcl *chrcp, double *paccum, int depth, struct classcont *clcontents, int nclass, int *clwn)
#endif
#ifdef i386
struct chchain *
followchains(chn, chrcp, paccum, depth, clcontents, nclass, clwn)
struct chchain * *chn;
struct chromcl *chrcp;
double *paccum;
int depth;
struct classcont *clcontents;
int nclass;
int *clwn;
#endif
{
	register struct chchain *chcn, *chcnj;
	struct chromcl *chrcj;
	struct classcont *classcontp;
	register struct cllik *cllak;
	register int i,j,nj;
	
	double cost,lik;
	double iaccum;
	double accum = *paccum;

#ifdef DEBUG
	for (d=0; d<MAXCHAIN-depth; d++)
		fprintf(stderr,"      ");
	fprintf(stderr,"depth %d cur class %d chcn %o\n",depth,chrcp->newclass,(unsigned)*chn);
#endif /* DEBUG */
	chcn = *chn;
	(*chn)++;
	maxchn++;
	chcn->chrcl = chrcp;
	chcn->nchildren = 0;
	chcn->chromclass = chrcp->newclass;
	chcn->child[0] = NULL;
	chcn->accum_cost = accum;
	if (maxchn >= NCHAINS) {
		maxchn--;
		(*chn)--;
		return(NULL);
	}
	if (depth > 0) {
		/*
		 * since the class-likelihoods have been sorted
		 * we have to search for the current likelihood.
		 */
		cllak = chrcp->cllika;
		for (i=0;  i<CUTOFF;  i++) {
			if (cllak->chromclass == chrcp->newclass) {
				lik = cllak->lval;
				/*
				 * consider each possible re-assignment class
				 */
				for (i=0;  i<nclass;  i++) {
					if (chrcp->newclass != i) {
					    /*
					     * find the likelihood for the re-assigmnent
					     * and check for conformance with conditions
					     * CUTOFF and LRATIO
					     */
						cllak = chrcp->cllika;
						for (j=0;  j<CUTOFF;  j++) {
							if (cllak->chromclass == i) {
								cost = cllak->lval;
#ifdef C_COMPILER_PROBLEM
								if (cost > LRATIO + chrcp->cllika->lval) {
#else
								if (cost > LRATIO + chrcp->cllika[0].lval) {
#endif
									cost = lik - cost;
#ifdef DEBUG
									for (d=0;  d<MAXCHAIN-depth;  d++)
										fprintf(stderr,"      ");
									fprintf(stderr,"      class %d cost %f",i,cost);
#endif /* DEBUG */
									/*
									 * we can move "chrcp" to this class with an increment
									 * to the overall likelihood of "cost".  If this is
									 * an under-subscribed class, stop here, else
									 * consider the effect of moving each of the current
									 * members of the class to yet another class.
									 */
									if (clcontents[i].n < clwn[i]) {
#ifdef DEBUG
										fprintf(stderr," undersubscribed\n");
#endif /* DEBUG */
										chcnj = (*chn)++;
										maxchn++;
										chcnj->accum_cost = accum+cost;
										chcnj->nchildren = 0;
										chcnj->chromclass = i;
										chcnj->child[0] = NULL;
										chcn->child[chcn->nchildren++] = chcnj;
										if (maxchn >= NCHAINS) {
											maxchn--;
											(*chn)--;
											return(NULL);
										}
									} 
									else {
#ifdef DEBUG
										fprintf(stderr,"\n");
#endif /* DEBUG */
										classcontp = &clcontents[i];
										nj = classcontp->n;
										if (nj > MAXCHILD) {
											if (maxchflag == 0) {
												maxchflag = 1;
												fprintf(stderr,"REARRANGE: too many children\n");
											}
											nj = MAXCHILD;
										}
										for (j=0;  j<nj;  j++) {
											chrcj = classcontp->chcl[j];
											iaccum = accum+cost;
											chcnj = followchains(chn,chrcj,&iaccum,depth-1,clcontents,nclass,clwn);
											if (chcnj != NULL)
												chcn->child[chcn->nchildren++] = chcnj;
										}
									}
								}
								break;
							}
							cllak++;
						}
					}
				}
				break;
			}
			cllak++;
		}
		if (chcn->nchildren == 0) {
			(*chn)--;
			maxchn--;
			return(NULL);
		} 
		else
		    return(chcn);
	} 
	else {
#ifdef DEBUG
		for (d=0;  d<MAXCHAIN-depth;  d++)
			fprintf(stderr,"      ");
		fprintf(stderr,"too deep\n");
#endif /* DEBUG */
		return(NULL);
	}
}



/*
 * recursively pass back the re-arrangement chain with minimum accumulated cost
 */
struct chchain * chainval(chnj,depth)
register struct chchain *chnj;
{
	register int i, mi;
	register struct chchain *c;
	register double mcost;
	if (chnj->nchildren == 0) {
#ifdef DEBUG
		for (i=0;  i<MAXCHAIN-depth;  i++)
			fprintf(stderr,"      ");
		fprintf(stderr,"chainval: no children. class %d cost %f\n",chnj->class,
		chnj->accum_cost);
#endif /* DEBUG */
		return (chnj);
	} 
	else {
		for (i=0;  i<chnj->nchildren;  i++) {
			c = chainval(chnj->child[i],depth-1);
			if (i == 0 || c->accum_cost < mcost) {
				mcost = c->accum_cost;
				mi = i;
			}
		}
#ifdef DEBUG 
		for (i=0;  i<MAXCHAIN-depth;  i++)
			fprintf(stderr,"      ");
		fprintf(stderr,"chainval: best child %d cost %f (%f). class %d\n",mi,mcost,
		chnj->child[mi]->accum_cost,chnj->class);
#endif /* DEBUG */
		chnj->bestchild = mi;
		chnj->accum_cost = mcost;
		/*
		 * this strange return is used subsequently
		 * when implementing a chain move.
		 */
		return(chnj->child[mi]);
	}
}



/*
 * recursively implement a move as specified in a chain.
 * The chromosome in chn->chrcl is moved to chn->class, and
 * movechain is applied to the subchain chn->child[chn->bestchild].
 */
movechain(chn,clcontents)
register struct chchain *chn;
register struct classcont *clcontents;
{
	register int newc,oldc,i;
	struct chromcl *chrcp;
	register struct classcont *clc;
#ifdef DEBUG1
	fprintf(stderr,"%d  ",chn->class);
#endif /* DEBUG1 */
	if (chn->nchildren > 0) {
		/*
		 * relevant per-chromosome class structure
		 */
		chrcp = chn->chrcl;
		/*
		 * current and new classes
		 */
		oldc = chn->chromclass;
		newc = chn->child[chn->bestchild]->chromclass;
		chrcp->newclass = newc;
		/*
		 * locate chromosome in relevant per-class structure
		 * and take it out.   decrement clss count.
		 */
		clc = clcontents + oldc;
		for (i=0;  i<clc->n;  i++)
			if (chrcp == clc->chcl[i])
				break;
		for (i++;  i<clc->n;  i++)
			clc->chcl[i-1] = clc->chcl[i];
		clc->n--;
		/*
		 * add chromosome to new per-class structure
		 * and increment count.
		 */
		clc = clcontents + newc;
		clc->chcl[clc->n] = chrcp;
		clc->n++;
		/*
		 * go to next level of chain and recursively rearrange
		 */
		movechain(chn->child[chn->bestchild],clcontents);
	}
}
