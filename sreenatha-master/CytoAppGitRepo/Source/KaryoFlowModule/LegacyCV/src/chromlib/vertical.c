/*
 * vertical.c		Jim Piper		20-05-86
 *
 *
 * Modifications
 *
 *  07 Apr 99		MG		Removed global xscale and yscale - they are now passed in
 *							- used to be some tacky code in karylib that accessed these
 *							via externs (kchromfeat.c).
 *
 *	11 Sep 1986		CAS		Includes
 *  JP		14/05/86	"fipconvhull" used for FIP digitised objects
 */

#include <stdio.h>
#include <math.h>
#include <woolz.h>
#include <chromanal.h>
#define RADIANS 57.296


#ifdef WIN32
struct chromosome *
vertical(struct chromosome *obj, double xscale, double yscale)
#endif
#ifdef i386
struct chromosome *
vertical(obj, xscale, yscale)
double xscale, yscale;
struct chromosome *obj;
#endif
{
	struct object *cvh, *convhull(), *fipconvhull();
	struct chromosome *robj;
	struct chromplist *plist;
	
	struct chord *ch;
	int n1,n2,s,c;
	double os9crud;
	if (xscale == yscale)
		cvh = convhull(obj);
	else
		cvh = fipconvhull(obj);
	obj->plist->hullarea = hullarea(cvh, &obj->plist->hullperim);
	mwrangle(cvh,&ch,&n1,&n2,&s,&c);
	if (c == 0) {
		c = 1;
		s *= 1000;
	}
	os9crud = atan((double)s / c);
	robj = spinsqueeze(obj,&os9crud,&xscale,&yscale);
	/*
	 * attach property list from original to rotated object
	 */
	robj->plist = obj->plist;
	/*
	 * fill in the property list
	 */
	plist = robj->plist;
	os9crud = RADIANS*os9crud;	/* angle through which I have been rotated */
	plist->rangle = (int) os9crud;
	freeobj(cvh);
	return(robj);
}
