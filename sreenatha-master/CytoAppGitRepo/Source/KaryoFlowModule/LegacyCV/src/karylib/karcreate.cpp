/*
 *	KARCREATE.CPP		9/2/93		Mark Gregson
 *
 *	routines for creating red karyogram canvas
 *
 *  	Modifications :
 *
 *	Dec/Jan 02/03 MC Mods for Metaphase Annotation
 *
 *	22 Sep 1999	MG	Fixed initial_flip_thru_180() scaling - it was always using a scale
 *					of 1 which resulted in karyotyped chromosomes reverting to their
 *					original scale when re-karyotyped.... oops!
 *
 *	24 Jul 1999 SN	Order stack objects by length in karyogram.
 *	27 may 1999 WH	Destroy the species template in destroy_kcont.
 *	14 Apr 1999 MG	Replaced all references to MAXOBJS and MAXCLASS with 
 *					kcont->MaxObjs and kcont->NumOfClasses respectively
 *					and tidied up modmaxchroms() to work with any species.
 *
 *	 6 Apr 1999 MG	CPP, and general tidy up - removal of #ifdef i386 etc.
 *
 *	 7 Dec 1998 MG	No mod in here but restore_chrom() in karmod.c now keeps the object's 
 *					current orientation - it does NOT use initialorientation() any more
 *					
 *	12 Nov 1998	MG	Took previous mod back out and corrected restore_chrom() to do
 *					an initialorientation() after resetting plist->dispor=0
 *	26 Oct 1998	MG	Modified initial_flip_thru_180() to add 180deg to rangle
 *					calculated by measobj. This ensures objects are displayed
 *					in the correct orientation after restore_chrom().
 *	13 Oct 1997 SN  For display, order chromosomes by confidence level.
 *	16 Jul 1997	MG	Suuport for BARCODE fish and unification
 *					of flags into assoc_type & assoc_ptr.
 *	4/17/97		BP:	Replace class with chromclass.
 *	25 Sep 1996	MG	MFISH flag and pointer added to kcontrol
 *
 *	25 Sep 1994	MG	CGH flag and pointer added to kcontrol
 *					obsolete kcont->bol[] list removed
 *	 4 Mar 1993	MG	added init of kcont->drawmode
 */

//#include <stdafx.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
//#include <fileheaders.h>
#include <redkar.h>
#include <seg_control.h>
#include <karylib.h>
//#include <cgh.h>
//#include <common.h>
//#include <warning.h>
//#include <TemplateKaryotype.h>
//#include <capabilities.h>
//#include <mfish.h>
//#include <mfwin.h>
//#include <pkar.h>
//#include <barcode.h>
//#include <barwin.h>

int UNIXCompatible = 0;

#define DIGTV		2	/* TV camera digitised (pixels are square) */

//-----------------------------------------------------------------------------------

/*
 *	Realloc a list to a newsize and clear any new elemets 
 *
 *	NOTE: realloc returns a void pointer to the reallocated (and possibly moved) memory block. 
 *	The return value is NULL if the size is zero and the buffer argument is not NULL, or if there
 *	is not enough available memory to expand the block to the given size. In the first case, the 
 *	original block is freed. In the second, the original block is unchanged - i.e. non-destructive.
 */
void *ReCalloc(void *list, size_t oldnum, size_t newnum, size_t size )
{
	void* newptr=NULL;
	char* clist=NULL;
	
	newptr = (void *)Realloc(list, newnum * size);

	// if the realloc was successful clear the new elements
	if ((newptr) && (newnum > oldnum)) {
		clist=(char *)newptr;
		clist += oldnum * size;
		memset(clist, 0, (newnum - oldnum) * size);
	}

	return newptr;

}

/*SN13Oct97 For odering chromosomes by confidence */
typedef struct confidence_record {
	int onum;
	int conf;
} confrec;



/*
 *	Update kcontrol number of classes - may be differnet from human 24 (DEFAULT_NUMOFCLASSES)
 *	
 *	Will fail if flexible karyotyping is disabled and numofclasses is anything but human
 *
 *	Called by init_kcontrol() , when a karyotype is loaded and when a cell is karyotyped 
 *	- prior to that we only have the metaphase and the number of classes is unimportant. 
 *	Also when we karyotype we may have selected a different species.
 *
 *	This routine free's  and then allocates space for the two arrays 
 *	Realloc is unecessary since the previous sets of data are redundant.
 *	Note that numofclasses is allowed to be smaller or larger than DEFAULT_NUMOFCLASSES
 *
 *	Returns 1 if successful, 0 otherwise.
 */
int set_kcontrol_NumOfClasses(struct kcontrol *kcont, int numofclasses)
{
	if ((!kcont) || (numofclasses <= 0))
		return 0;	// Failed

	// if no change in number of classes just return
	// NOTE: init_kcontrol() sets kcont->NumOfClasses = 0 before calling this
	if (kcont->NumOfClasses == numofclasses)
		return 1;	// No change OK

	if (kcont->classptr)		Free(kcont->classptr);
	if (kcont->keepclassmax)	Free(kcont->keepclassmax);

	// ***NOTE*** the use of (numofclasses + 1) - the +1 is to handle the STACK canvas
	kcont->classptr		= (Canvas **)Calloc(numofclasses + 1, sizeof(kcont->classptr[0]));	// ptrs to chromosome class canvases
	kcont->keepclassmax	= (SMALL *)Calloc(numofclasses + 1, sizeof(kcont->keepclassmax[0]));// old class maxchroms for undo

	// check allocation
	if (!kcont->classptr || !kcont->keepclassmax)
		return 0;

	// update associated structure's dynamic arrays 
	// - if this fails we should not update kcont->MaxObjs either

	// make sure structure has been attached first
	// Theoretically the following should NEVER HAPPEN
	// since CGH and MFISH structures are created with
	// the correct number of classes and CANNOT be rekaryotyped
	// to a different species 
	// NOTE RxFISH and PKAR have no class dependent structures

/* 
	if (kcont->assoc_ptr)
	{
		switch (kcont->assoc_type)	
		{
		case ASSOC_CGH:
			if (!set_cgh_NumOfClasses((CGH *)kcont->assoc_ptr, numofclasses)) 
					return 0;
			break;

		case ASSOC_MFISH:	
			if (!set_mfish_NumOfClasses((MFISH *)kcont->assoc_ptr, numofclasses)) 
				return 0;	
			break;

		default: break;
		}
	}
*/
	// New fields for flexible karyotyper - added in METVERSION=2, KARVERSION=2
	kcont->NumOfClasses	= numofclasses;

	return 1;		// Changed OK
}



/*
 *	INIT_KCONTROL
 *
 *	Initialise a kcontrol structure 
 *	This maintains lists of chromosome objects on the metaphase and karyogram
 *	and their associated canvas display structures.
 *	A pointer to a DDGS structure dg is used for canvas display manipulation
 */
struct kcontrol *init_kcontrol(DDGS *metdg, DDGS *kardg)
{
	struct kcontrol *kcont;
	int i;


	// allocate space for kcont structure
	kcont = (struct kcontrol *) Calloc (1, sizeof (struct kcontrol));

	// New fields for flexible karyotyper 
	//- saved and loaded in METVERSION=2, KARVERSION=2
	kcont->NumOfClasses		= 0;					// Initially - see call to set_kcontrol_NumOfClasses() below
	kcont->MaxObjs			= DEFAULT_MAXOBJS;		// 250 - UNIX compatibility
	kcont->TemplatePtr		= NULL;					// Species template pointer

    kcont->maxnumber		= 0;					// total number of objects in karyogram
	kcont->measnumber		= 0;					// number of objects measured in metaphase

	kcont->classifier		= SHORT_CLASS;			// default classifier

    kcont->FIP				= DIGTV;				// digitisation method

	kcont->drawmode			= COPY;					// assume brightfield draw mode
    kcont->Autoscale        = FALSE;                // Display params
    kcont->Layout           = DefaultLayout;

	kcont->metdg			= metdg;				// metaphase DDGS display structure
	kcont->kardg			= kardg;				// karyogram DDGS display structure

	kcont->zoom_factor		= 16;					// canvas zoom_factor, i.e. DDGS scale

	kcont->met_canvas		= NULL;					// metaphase canvas
	kcont->kar_canvas		= NULL;					// karyogram canvas
	kcont->metanno_canvas	= NULL;					// metaphase annotation canvas
	kcont->karanno_canvas	= NULL;					// karyogram annotation canvas

	kcont->metactive		= FALSE;				// metaphase active - used by kcont_admin
	kcont->karactive		= FALSE;				// karyogram active - used by kcont_admin

	kcont->filegroupid		= NULL;					// filegroup and image IDs for copy and paste
	kcont->met_imageid		= NULL;
	kcont->kar_imageid		= NULL;

	kcont->colourised		= 0;					// whether met is colourised or not
	kcont->modified			= 0;					// indicates if met/kar have been modified

	kcont->manual_count		= 0;					// manual count of chromosomes

	// Flexible karyotyper introduces DYNAMIC ARRAYS - allocate them here
	// NOTE the use of Calloc to make sure all pointers are effectively NULL
	kcont->ool				= (struct chromosome **)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->ool[0]));		// original metaphase objects
	kcont->eool				= (struct chromosome **)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->eool[0]));	// enhanced metaphase objects
	kcont->mol				= (struct chromosome **)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->mol[0]));		// measured/rotated objects
	kcont->emol				= (struct chromosome **)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->emol[0]));	// enhanced measured objects
	kcont->bndool			= (struct object **)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->bndool[0]));		// boundaries of ool objects
	kcont->bndmol			= (struct object **)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->bndmol[0]));		// boundaries of mol objects

	kcont->keepeool			= (struct chromosome **)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->keepeool[0]));// old eool objects for undo
	kcont->keepbndool		= (struct object **)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->keepbndool[0]));	// old ool boundaries for undo
	kcont->keepemol			= (struct chromosome **)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->keepemol[0]));// old emol objects for undo
	kcont->keepmol			= (struct chromosome **)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->keepmol[0]));	// old mol objects for undo when reflect, trim or straightened objects are involved - new in CV3.6
	kcont->keepbndmol		= (struct object **)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->keepbndmol[0]));		// old mol boundaries for undo
	kcont->keepsharedplist	= (struct chromplist **)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->keepsharedplist[0]));	// old shared plist contents for undo

	kcont->acl				= (SMALL *)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->acl[0]));					// activity list
	kcont->selected			= (SMALL *)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->selected[0]));				// selected list

	kcont->met_chromptr		= (Canvas **)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->met_chromptr[0]));		// ptrs to metaphase object canvases
	kcont->kar_chromptr		= (Canvas **)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->kar_chromptr[0]));		// ptrs to karyogram object canvases

	kcont->count_cross		= (crosspt *)Calloc(DEFAULT_MAXOBJS, sizeof(kcont->count_cross[0]));		// manual count cross locations

	// check allocation
	if (!kcont->ool
	|| !kcont->eool
	|| !kcont->mol
	|| !kcont->emol
	|| !kcont->bndool
	|| !kcont->bndmol
	|| !kcont->keepeool
	|| !kcont->keepbndool
	|| !kcont->keepemol
	|| !kcont->keepmol
	|| !kcont->keepbndmol
	|| !kcont->keepsharedplist
	|| !kcont->acl
	|| !kcont->selected
	|| !kcont->met_chromptr
	|| !kcont->kar_chromptr
	|| !kcont->count_cross	) { 
		destroy_kcontrol(kcont);
		return(NULL);
	}

	// allocate space for kcont->classptr & kcont->keepclassmax lists, and set kcont->NumOfClasses
	if (!set_kcontrol_NumOfClasses(kcont, DEFAULT_NUMOFCLASSES)) // 24 - Human
	{
		destroy_kcontrol(kcont);
		return(NULL);
	}
		
	// allocate space for seg_interact structure - NOTE this is NOT DYNAMIC
	// - we can easily adjust MAXSPLITS whenever we like with no affect on other code or file formats
	kcont->SI=(struct seg_interact *) Calloc ( 1, sizeof(struct seg_interact));
	if (!kcont->SI) {
		destroy_kcontrol(kcont);
		return(NULL);
	}


	kcont->SI->polyobj		= NULL;					// the splitting polygon
	kcont->SI->framewas		= NULL;					// when frames are swapped  - not used

	for (i=0;i<MAXSPLITS;i++) {
		kcont->SI->objl[i]	= NULL;					// split off objects
		kcont->SI->boundlist[i]=NULL;				// and their boundaries
		kcont->SI->axes[i]	= NULL;					// overlap axes polygons
	}

	kcont->chromwidth		= 8;					// chromosome width for axes overlap

	kcont->centalign		= NULL;					// don't align on centromere

	kcont->assoc_type		= ASSOC_NONE;			// not a cgh/mfish/barcode counterstain image
	kcont->assoc_ptr		= NULL;					// no associated structure

	kcont->temp_mfish_gamma = 1.0; // NOTE: note stored to file, just placement holder

	return(kcont);
}




/*
 *	Reallocate all kcontrol object based lists
 *
 *	Returns 1 if successful:
 *			kcont->MaxObjs = kcont->MaxObjs + numofobjects
 *			All new elements are zeroed (as if created by Calloc)
 *			All old elements are untouched.
 *
 *	Returns 0 if failure:
 *			kcont->MaxObjs is unchanged 
 *			But some arrays may contain MORE elements than MaxObjs
 *			- this is OK since a Free(kcont->listname) will release them
 *			and their existence will not interfere with kcont operations
 *			since these will be limited to the current kcont->MaxObjs
 *
 *			NOTE this function ALWAYS FAILS if UNIXCompatible is set
 *			In which case no reallocation takes place and MaxOvjs is unchanged.
 */
int increase_kcontrol_MaxObjs(struct kcontrol *kcont, int numofobjects)
{
	void* newptr=NULL;
	int oldmax, newmax;

	// check we are allowed to do this 
	// we can't if we are trying to maintain UNIX compatibility
	if (UNIXCompatible)
		return 0;

	// check structures are OK
	if ((!kcont) || (numofobjects <= 0))
		return 0;

	oldmax = kcont->MaxObjs;
	newmax = oldmax + numofobjects;

	newptr = (void *)ReCalloc(kcont->ool, oldmax, newmax, sizeof(kcont->ool[0]));
	if (newptr) kcont->ool = (struct chromosome **)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->eool, oldmax, newmax, sizeof(kcont->eool[0]));
	if (newptr) kcont->eool = (struct chromosome **)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->mol, oldmax, newmax, sizeof(kcont->mol[0]));
	if (newptr) kcont->mol = (struct chromosome **)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->emol, oldmax, newmax, sizeof(kcont->emol[0]));
	if (newptr) kcont->emol = (struct chromosome **)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->bndool, oldmax, newmax, sizeof(kcont->bndool[0]));
	if (newptr) kcont->bndool = (struct object **)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->bndmol, oldmax, newmax, sizeof(kcont->bndmol[0]));
	if (newptr) kcont->bndmol = (struct object **)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->keepeool, oldmax, newmax, sizeof(kcont->keepeool[0]));
	if (newptr) kcont->keepeool = (struct chromosome **)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->keepbndool, oldmax, newmax, sizeof(kcont->keepbndool[0]));
	if (newptr) kcont->keepbndool = (struct object **)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->keepemol, oldmax, newmax, sizeof(kcont->keepemol[0]));
	if (newptr) kcont->keepemol = (struct chromosome **)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->keepmol, oldmax, newmax, sizeof(kcont->keepmol[0]));
	if (newptr) kcont->keepmol = (struct chromosome **)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->keepbndmol, oldmax, newmax, sizeof(kcont->keepbndmol[0]));
	if (newptr) kcont->keepbndmol = (struct object **)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->keepsharedplist, oldmax, newmax, sizeof(kcont->keepsharedplist[0]));
	if (newptr) kcont->keepsharedplist = (struct chromplist **)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->acl, oldmax, newmax, sizeof(kcont->acl[0]));
	if (newptr) kcont->acl = (SMALL *)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->selected, oldmax, newmax, sizeof(kcont->selected[0]));
	if (newptr) kcont->selected = (SMALL *)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->met_chromptr, oldmax, newmax, sizeof(kcont->met_chromptr[0]));
	if (newptr) kcont->met_chromptr = (Canvas **)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->kar_chromptr, oldmax, newmax, sizeof(kcont->kar_chromptr[0]));
	if (newptr) kcont->kar_chromptr = (Canvas **)newptr; else return 0;

	newptr = (void *)ReCalloc(kcont->count_cross, oldmax, newmax, sizeof(kcont->count_cross[0]));
	if (newptr) kcont->count_cross = (crosspt *)newptr; else return 0;

	
	// update associated structure's dynamic arrays 
	// - if this fails we should not update kcont->MaxObjs either

	// make sure structure has been attached first
/*
	if (kcont->assoc_ptr)
	{
		switch (kcont->assoc_type)	
		{
		case ASSOC_CGH:		if (!increase_cgh_MaxObjs((CGH *)kcont->assoc_ptr, numofobjects)) return 0;	break;
		case ASSOC_MFISH:	if (!increase_mfish_MaxObjs((MFISH *)kcont->assoc_ptr, numofobjects)) return 0;	break;
		case ASSOC_BARCODE:	if (!increase_barcode_MaxObjs((BARCODE *)kcont->assoc_ptr, numofobjects)) return 0;break;
		case ASSOC_PKAR:	if (!increase_pkar_MaxObjs((PKAR *)kcont->assoc_ptr, numofobjects)) return 0;	break;
		default: break;
		}
	}
*/
	// New field for flexible karyotyper - METVERSION=2, KARVERSION=2
	kcont->MaxObjs = newmax;


	return 1;	// OK
}
// HACK NEED REAL ONES
void destroy_zombies(struct kcontrol *kcont) {} 
void free_annokeep(Canvas *anno_canvas) {}
void free_metkeep(struct kcontrol *kcont) {}
// END HACK

/*
 *	FREE_KEEPLISTS
 *
 *	free metaphase and karyogram object keeplists
 */
void free_keeplists(struct kcontrol *kcont)
{

	if (kcont->kar_canvas!=NULL) {
		free_karykeep(kcont);
		free_annokeep(kcont->karanno_canvas);
	}
	if (kcont->met_canvas!=NULL) {
		free_metkeep(kcont);
		free_annokeep(kcont->metanno_canvas);
	}

	/* destroy the undead or they will return to haunt you */
	destroy_zombies(kcont);
}



/*
 *	DESTROY_KCONTROL
 *
 *	destroys a kcontrol structure and frees up all allocated space
 */
void destroy_kcontrol(struct kcontrol *kcont)
{
	int onum;
	Canvas *canvas;

	if (!kcont)
		return;

	/* clear ddgs pointers - stops free_keeplists drawing objects */
	kcont->metdg=NULL;
	kcont->kardg=NULL;

	/* free all keeplists */
	free_keeplists(kcont);

	/* free seg_interact structure */
	if (kcont->SI)
	{
		freeobj(kcont->SI->polyobj);	// note freeobj handles NULL pointers OK
		for (onum=0;onum<MAXSPLITS;onum++) {
			freeobj((struct object *)kcont->SI->objl[onum]);
			freeobj(kcont->SI->boundlist[onum]);
			freeobj(kcont->SI->axes[onum]);
		}
		Free(kcont->SI);
	}

	/* remove object list pointers from metaphase canvas before destroying */
	for (onum=0; onum<kcont->maxnumber; onum++) {
		canvas=kcont->met_chromptr[onum];

		if (canvas != NULL) {
			/* remove object pointer */
			canvas->Wobj=NULL;

			/* and highlight object pointer if its a bndool[] */
			canvas=canvas->patch;

			/* search for boundary highlight first */
			while (canvas!=NULL) {
				if (canvas->type == Cgraphic) {
					if (canvas->Wobj == kcont->bndool[onum])
						canvas->Wobj=NULL;
					break;
				}
				canvas=canvas->next;
			}
		}
	}
	/* destroy metaphase canvas - pointer not set NULL - needed below */
	destroy_Canvas_and_Wobj(kcont->met_canvas);

	/* remove object list pointers from karyogram canvas before destroying */
	for (onum=0; onum<kcont->maxnumber; onum++) {
		canvas=kcont->kar_chromptr[onum];

		if (canvas != NULL) {
			/* remove object pointer */
			canvas->Wobj=NULL;
		}
	}

	/* destroy karyogram canvas - pointer not set NULL - needed below */
	destroy_Canvas_and_Wobj(kcont->kar_canvas);


	/* free off object lists */
	for (onum=0;onum<kcont->MaxObjs;onum++) {
		/* first free shared plists */
		if ((kcont->met_canvas !=NULL ) && (kcont->ool[onum] != NULL))
			freeplist((struct propertylist *)kcont->ool[onum]->plist);
		else
			if ((kcont->kar_canvas != NULL) && (kcont->mol[onum] != NULL))
			 	freeplist((struct propertylist *)kcont->mol[onum]->plist);

		/* then free objects */
		if (kcont->ool[onum] != NULL) {
			kcont->ool[onum]->plist=NULL;
			freeobj((struct object *)kcont->ool[onum]);
		}
		if (kcont->eool[onum] != NULL) {
			kcont->eool[onum]->plist=NULL;
			freeobj((struct object *)kcont->eool[onum]);
		}
		if (kcont->mol[onum] != NULL) {
			kcont->mol[onum]->plist=NULL;
			freeobj((struct object *)kcont->mol[onum]);
		}
		if (kcont->emol[onum] != NULL) {
			kcont->emol[onum]->plist=NULL;
			freeobj((struct object *)kcont->emol[onum]);
		}

		/* then boundary objects */
		freeobj(kcont->bndool[onum]);
		freeobj(kcont->bndmol[onum]);
	}
	
	// Free off Calloc'd list structures
	if (kcont->ool)				Free(kcont->ool);
	if (kcont->eool)			Free(kcont->eool);
	if (kcont->mol)				Free(kcont->mol);
	if (kcont->emol)			Free(kcont->emol);
	if (kcont->bndool)			Free(kcont->bndool);
	if (kcont->bndmol)			Free(kcont->bndmol);
	if (kcont->keepeool)		Free(kcont->keepeool);
	if (kcont->keepbndool)		Free(kcont->keepbndool);
	if (kcont->keepemol)		Free(kcont->keepemol);
	if (kcont->keepmol)			Free(kcont->keepmol);
	if (kcont->keepbndmol)		Free(kcont->keepbndmol);
	if (kcont->keepsharedplist)	Free(kcont->keepsharedplist);
	if (kcont->acl)				Free(kcont->acl);
	if (kcont->selected)		Free(kcont->selected);
	if (kcont->met_chromptr)	Free(kcont->met_chromptr);
	if (kcont->kar_chromptr)	Free(kcont->kar_chromptr);
	if (kcont->count_cross)		Free(kcont->count_cross);
	if (kcont->classptr)		Free(kcont->classptr);
	if (kcont->keepclassmax)	Free(kcont->keepclassmax);

/*
	if (kcont->TemplatePtr)
		delete ((CTemplateKaryotype *)kcont->TemplatePtr);
*/

	/* bye-bye */
	if (kcont) Free(kcont);
}


/*
 * 	C H E C K _ C L A S S I F I C A T I O N
 *
 * 	Check all active objects ,if object not classified make it abnormal.
 *
 */
void check_classification(struct kcontrol *kcont)
{
	register struct chromplist *plist;
	register int i;
	register int ac;
	
	for (i=0; i<kcont->maxnumber; i++) {
		ac = kcont->acl[i];
		ac &= ACTIVE;
		if (ac != 0) {
			plist = kcont->emol[i]->plist;
			if (plist->otype <= CHROMOSOME) {
				if (plist->pgroup <= 0) {
					fprintf(stderr,"unclassified CHR %d called ABNORMAL\n",i);
					plist->pgroup = kcont->NumOfClasses + 1;
				}
			} else {
				/*
				 * This is likely to be a composite or noise as determined
				 * by the feature measurement but not confirmed by operator
				 */
				plist->pgroup = kcont->NumOfClasses + 1;
			}
		}
	}
}


/* produce initial karyogram but do not display */
#if 0
void kary(struct kcontrol *kcont)
{
	struct chromosome *obj;
	struct chromplist *plist;
	int i,ac;

	/* read objects and check orientation */
	for (i=0; i<kcont->maxnumber; i++) {
		ac = kcont->acl[i];
		ac &= ACTIVE;
		if (ac != 0) {
			obj = kcont->emol[i];	
			plist = obj->plist;
			if (obj->type != 1 || plist == NULL) {
				fprintf(stderr,"%d: wrong type or no property list\n",i);
				kcont->acl[i] &= ~ACTIVE;
			} else
				/* check orientation */
				initialorientation(i,kcont);
		}
	}

	/* check classification */
	check_classification(kcont);

	/* produce initial karyogram */ 
	karyogram(kcont);

}
#endif

/*
 *  MODMAXCHROMS -- nicked & mangled from Cytoscan code /kmr/karysubs1.c
 *
 *  Modify the maxchroms allowed in each class depending on the number of 
 *  active chromosomes in the cell - aim to cater for triploid, tetraploid,
 *  and prevent stack overflow.
 *
 *	MG. 14Apr99 modified this to work with any species 
 *	i.e. any number of chromosome classes
 */
void modmaxchroms(struct kcontrol *kcont)
{
	int 		onum, extras, active, chromclass, chromthresh;
	Canvas 		*classcanvas;
	Cclassdata	*classdata;

	active = 0;
	for(onum = 0; onum < kcont->maxnumber; onum++) 
		if((ACTIVE & kcont->acl[onum]) != 0)
			active++;

	// Set an 'extra chromosomes' threshold of 3/4 of the expected number of chromosomes
	// in a metaphase i.e. for human this will be 3/4 of 46 = 36
	chromthresh = 3 * (kcont->NumOfClasses * 2) / 4;

	// calculate the total extra chromosomes as the number exceeding this threshold
	extras = active - chromthresh;	

	if(extras < 0)
		extras = 0;

	// the number of extra chromosomes in each class is then calculated by
	// dividing the extras by the  number of classes
	extras /= kcont->NumOfClasses;

	for (chromclass = 1; chromclass <= kcont->NumOfClasses; chromclass++) {
		classcanvas=kcont->classptr[chromclass-1];
		classdata=(Cclassdata *) classcanvas->data;
	
		/* update allowed number of chromsomes */
		classdata->maxchroms+=extras;

		/* recalculate chromosome spacing in class */
		classcanvas->patchgrid.spacing=classcanvas->frame.width 
						/ classdata->maxchroms;
	}
		
}


/*
 *	FILLOBJCLASS
 *
 *	SN	13Oct97
 *
 * 	Fill array of MAXOBJS elements with each active object's class
 *
 */
void fillobjclass(struct kcontrol *kcont, int *objclass)
{
	int onum;
	int chromclass;

	/* Scan all active objects */
	for (onum=0; onum<kcont->maxnumber; onum++) {
		if ((ACTIVE & kcont->acl[onum]) != 0) {

			/* Get chromosome class*/
			if (kcont->emol[onum]->plist->Cpgroup)
				chromclass = kcont->emol[onum]->plist->Cpgroup;
			else
				chromclass = kcont->emol[onum]->plist->pgroup;

			/* Make class Abnormal if silly class */
			if ((chromclass < 1) || (chromclass > kcont->NumOfClasses + 1))
				chromclass = kcont->NumOfClasses + 1;

			objclass[onum] = chromclass;
		}
	}	
}


/*
 *	MINCONFVAL
 *
 *	SN	13Oct97
 *
 * 	Given two confrec structures, returns positive value if
 *	conf in first struct is less than that in second.
 *
 */
int minconfval(const void *pc1, const void *pc2)
{
	confrec * pcr1 = (confrec *) pc1;
	confrec * pcr2 = (confrec *) pc2;

	return( pcr2->conf - pcr1->conf );
}


/*
 *	F I R S T C L A S S --
 *
 * 	This routine looks to see if data is previusly karyotyped and if
 *	so tries to put things back in the same place
 */
#if 0
void firstclass(int onum, struct kcontrol *kcont)
{
	int chromclass;
	
	/* get chromosome class*/

	if (kcont->emol[onum]->plist->Cpgroup)
		chromclass = kcont->emol[onum]->plist->Cpgroup;
	else
		chromclass = kcont->emol[onum]->plist->pgroup;


	/* if karyotyped before put chromosome back in same place */

	if (kcont->emol[onum]->plist->disppos > 0)
		reposition(onum,chromclass,kcont);
	else
		newclass(onum,chromclass,kcont);
}
#endif


/*
 *	KARYOGRAM
 *
 * 	construct initial karyogram from classified objects
 *
 *  MODS:
 *	SN	130ct97	Order chromosomes by confidence value. 
 *	SN	24Jul99	Order stack objects by length.
 */
#if 0
void karyogram(struct kcontrol *kcont)
{
	int onum, chromclass;
	Cclassdata *classdata;
	Cstackdata *stackdata;
	int i, nclasscnt;				//SN13Oct97
	int *objclass;					//SN13Oct97 , changed to dynamic array MG 13Apr99 
	confrec *conf;					//SN13Oct97, changed to dynamic array MG 13Apr99 

	
	/* initialise class canvas data fields - use kcont classptr array 
           to access canvases - set up in default_karyogram */
	for (chromclass=0; chromclass<kcont->NumOfClasses; chromclass++) {

		classdata=(Cclassdata *) kcont->classptr[chromclass]->data;

		if ( classdata!=NULL)
			/* zero chromosome count in each class */
			classdata->nchroms = 0;
	}

	stackdata=(Cstackdata *) kcont->classptr[kcont->NumOfClasses]->data;

	if ( stackdata!=NULL)
		/* zero chromosome count in stack */
		stackdata->nchroms = 0;

	/*
	 * Modify classdata->maxchroms values dependent on number of 
	 * active objects.
	 */
	modmaxchroms(kcont);

	// Allocate space for confidence data
	objclass = (int *)Calloc(kcont->MaxObjs, sizeof(objclass[0]));
	conf = (confrec *)Calloc(kcont->MaxObjs, sizeof(conf[0]));

	// If allocation fails .....
	if ((!objclass) || (!conf)) 
	{
		if (objclass) Free(objclass);
		if (conf) Free(conf);

		// use OLD CODE - without confidence ordering
		for (onum=0; onum<kcont->maxnumber; onum++) {
			if ((ACTIVE & kcont->acl[onum]) != 0) {
				firstclass(onum,kcont);
			}
		}

		return;
	}
	

	/* Get class of every active object */
	fillobjclass( kcont, objclass);

	/* Reorder objects within classes 1 to NumOfClasses + 1 (=stack class) */
	for (chromclass=1; chromclass <= (kcont->NumOfClasses + 1); chromclass++)
	{
		/* Zero conf array */
		for (i=0; i<kcont->MaxObjs; i++) {
			conf[i].onum = 0;
			conf[i].conf = 0;
		}

		/* For each active object in this class fill conf */
		/* and count objects belonging to class */
		nclasscnt = 0;
		for (onum=0; onum<kcont->maxnumber; onum++) {
			if (((ACTIVE & kcont->acl[onum]) != 0) &&
				(objclass[onum] == chromclass)	) {

				conf[nclasscnt].onum = onum;
				if (chromclass == kcont->NumOfClasses + 1)						//SN24Jul99
					conf[nclasscnt].conf = kcont->mol[onum]->plist->length;		// Order stack objects by length
				else
					conf[nclasscnt].conf = kcont->mol[onum]->plist->pgconf;		// Order classified objects by confidence
				nclasscnt++;
			}
		}

		if (nclasscnt == 0)
			continue;

		/* Sort conf array by conf field */
		qsort( (char *) conf, nclasscnt, sizeof( confrec), minconfval);

#ifdef DEBUG
		for (i=0; i<nclasscnt; i++)
			printf( "%d, onum = %d, conf = %d\n", chromclass, conf[i].onum, conf[i].conf);
#endif

		if (chromclass != (kcont->NumOfClasses + 1))
		{
			// MG 8May03 now arrange objects in classcanvas based on curvature of chromosome
			// only interested in chromosomes in classcanvas - not those in stack
			classdata=(Cclassdata *) kcont->classptr[chromclass]->data;

			if (classdata->maxchroms > 1)
			{
				int chromsinclass = nclasscnt;

				if (classdata->maxchroms < chromsinclass)
					chromsinclass = classdata->maxchroms;

				for (i=0; i<chromsinclass; i++)
					conf[i].conf = int (-getChromCurvature(kcont->emol[conf[i].onum]) * 100);

				qsort( (char *) conf, chromsinclass, sizeof( confrec), minconfval);
			}
		}


		/* Add all chromosomes to karyotype in order */
		for (i=0; i<nclasscnt; i++)
			firstclass( conf[i].onum,kcont);
	}

	// free confidence data
	if (objclass) Free(objclass);
	if (conf) Free(conf);

}
#endif

/*
 * 	N E W C P G R O U P
 */
void newCpgroup(int onum, int chromclass, struct kcontrol *kcont)
{
	if ((chromclass < 0) || (chromclass > kcont->NumOfClasses + 1))
		chromclass = kcont->NumOfClasses + 1;			/* Abnormal if silly class */

	kcont->emol[onum]->plist->Cpgroup = chromclass;
}

/*
 * N E W P G R O U P
 */
void newpgroup(int onum, int chromclass, struct kcontrol *kcont)
{
	if ((chromclass < 0) || (chromclass > kcont->NumOfClasses + 1)) 
		chromclass = kcont->NumOfClasses + 1;			/* Abnormal if silly class */

	kcont->emol[onum]->plist->pgroup = chromclass;
}




/*
 *	C H E C K P O S
 *
 *	if position in class is occupied return 1 else 0
 *	- checks class, then stack for chroms that could not fit in class canvas
 */
int checkpos(int chromclass, int pos, struct kcontrol *kcont)
{	
	Canvas *chromcanvas;
	Cchromdata *chromdata;
	int found;
	
	found=0;

	if (chromclass == kcont->NumOfClasses + 1)
		return(found);	/* dont bother checking abnormals */

	chromcanvas=kcont->classptr[chromclass-1]->patch;
	
	/* first check class canvas */
	while ( (chromcanvas!=NULL) && (!found) ){

		if ( (chromcanvas->type==Cchromosome)
		   && (chromcanvas->data!=NULL) )  {

			chromdata=(Cchromdata *)chromcanvas->data;
	
		 	found=(chromdata->posinclass==pos);
		}
		chromcanvas=chromcanvas->next;
 	}

	/* then check stack canvas */
	chromcanvas=kcont->classptr[kcont->NumOfClasses]->patch;

	while ( (chromcanvas!=NULL) && (!found) ){

		if ( (chromcanvas->type==Cchromosome)
		   && (chromcanvas->data!=NULL) )  {
		
			chromdata=(Cchromdata *)chromcanvas->data;

			if (chromdata->chromclass==chromclass) {
			 	found=(chromdata->posinclass==pos);
			}
		}

		chromcanvas=chromcanvas->next;
 	}

	return(found);

}



/*
 *	R E P O S I T I O N  --
 *
 * re-insert object from previously classified file in specified class
 * at specified position
 */
#if 0
void reposition(int onum, int chromclass, struct kcontrol *kcont)
{
	int classpos;
	struct chromosome *obj;


	/* get pointer to chromosome object */
	obj=kcont->emol[onum];

	/* Make class Abnormal if silly class */
	if ((chromclass < 1) || (chromclass > kcont->NumOfClasses + 1))
		chromclass = kcont->NumOfClasses + 1;

	/* get previously classified position */
	classpos=obj->plist->disppos;
	if (classpos<1) classpos=1;

	/* if position in class is already occupied */
	if ( checkpos(chromclass,classpos,kcont) ) {
           	/* pass chromosome to newclass to handle */
		newclass(onum,chromclass,kcont);
	}
	else {
		/* otherwise create new chromosome canvas at specified position */
		newchromcanvas(onum,chromclass,classpos,kcont);
	}
}
#endif


/*
 *	N E W C L A S S -- insert object in specified class
 */
#if 0
void newclass(int onum, int chromclass, struct kcontrol *kcont)
{
	int classpos;
	struct chromosome *obj;
	Cclassdata *classdata;
	Cstackdata *stackdata;


	/* get pointer to chromosome object */
	obj=kcont->emol[onum];

	/* Make class Abnormal if silly class */
	if ((chromclass < 1) || (chromclass > kcont->NumOfClasses + 1))
		chromclass = kcont->NumOfClasses + 1;

	/* add to new class */
	if (chromclass != kcont->NumOfClasses + 1) { 	/* not abnormal */

		/* get ptr to class canvas data  */
		classdata=(Cclassdata *) kcont->classptr[chromclass-1]->data;

		if (classdata->nchroms==0) { /* no chromosomes in class */
			classpos=1;
		} 	
		else {	/* look for first free position in class */
			classpos=1;
			
			while ( checkpos(chromclass,classpos,kcont) ) {
				classpos++;
			}	
		}
	}
	else {
		/* abnormals go to end of stack */
		stackdata=(Cstackdata *)kcont->classptr[kcont->NumOfClasses]->data;
		classpos=stackdata->nchroms+1;
	}

	/* create new chromosome canvas */
	newchromcanvas(onum,chromclass,classpos,kcont);

	/* save canvas class position in object plist */
	kcont->emol[onum]->plist->disppos = classpos;

	relabel_visible_met_annotation(kcont, onum, chromclass);
}
#endif


void initial_flip_thru_180(struct kcontrol *kcont, int onum)
{
	struct chromplist *plist;
	struct chromosome *obj, *rotobj;
	double scale, drads;

	obj = kcont->eool[onum];
	plist = obj->plist;


	if (plist->fscale<=0.0)		/* object not scaled before */
		scale=1.0;
	else
		scale=plist->fscale;	/* use existing scale for rotation */

	// Set initial corrected angle if necessary
	if (plist->Cangle == 0)				// object not been rotated
		plist->Cangle = plist->rangle;	// use measobj() rotation

	// add 180deg flip to corrected angle
	plist->Cangle += 180;

	// set display orientation to non-inverted
	plist->dispor=1;

	// create new object flipped thru 180
	drads = plist->Cangle * 3.1415926/180.0;


	rotobj=(struct chromosome *) spinsqueeze( (struct object *)obj,
							&drads,
							&scale,
							&scale);

	// destroy old emol object
	obj=kcont->emol[onum];
	obj->plist=NULL;
	freeobj((pWZObj)obj);

	// reset kcont object pointers to new emol object
	kcont->emol[onum]=rotobj;

	// copy original object plist
	rotobj->plist = plist;

	/* associate objects */
	kcont->emol[onum]->assoc=(pWZObj)kcont->eool[onum];

	/* destroy old boundary object */
	freeobj(kcont->bndmol[onum]);

	/* and create new one */
	if (((pWZObj)kcont->bndmol[onum] = (pWZObj)ibound((pWZObj)kcont->emol[onum], 1)) == NULL)
		kcont->bndmol[onum] = (pWZObj)create_dummy_poly();
}



/*
 * initial orientation :
 * invert if area c.i. > 50 (should really be length c.i.),
 * or if c.i. unknown then if mwdd[0] positive
 */
void initialorientation(int onum, struct kcontrol *kcont)
{
	register struct chromplist *plist;
	struct chromosome *obj;


	obj = kcont->emol[onum];
	plist = obj->plist;

	if (plist->otype != CHROMOSOME)
		return;

	if (plist->dispor != 1 && plist->dispor != -1) {
		if (plist->cindexa > 50)
			plist->dispor = -1;
		else if (plist->cindexa == -1 && plist->mwdd[0] > 0)
			plist->dispor = -1;
		else
			plist->dispor = 1;
	}
	if (plist->dispor==0)
		fprintf(stderr,"onum=%d dispor=0\n",onum);

	/* initialise corrected centromere location - used throughout karyotype */
	if ((plist->Ccy==0 ) && (plist->Ccx==0)){
		if (plist->cindexa==-1) {	
			/* centromere location failed - set to mid point */
			plist->Ccy=(obj->idom->lastln + obj->idom->line1)/2;
			plist->Ccx=(obj->idom->lastkl + obj->idom->kol1)/2;
		}
		else {
			/* set to measured value */
			plist->Ccy=plist->cy;
			plist->Ccx=plist->cx;
		}
	}


	/* Prevent any inverted chroms getting into karyotype
	   by rotating them thru 180 degrees instead  - this 
	   is so we don't get any horrible ix=iy=-1 stuff when
	   diplaying canvases */

	if (plist->dispor == -1)
		initial_flip_thru_180(kcont,onum);

}



	
