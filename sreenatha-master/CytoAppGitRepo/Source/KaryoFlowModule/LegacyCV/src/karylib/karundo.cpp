/*
 *	KARUNDO.CPP 		9/2/93		Mark Gregson
 *
 *	routines for undoing modifications to red karyogram canvas
 *
 *  	Modifications :
 *
 *	Dec/Jan 02/03 MC Mods for Metaphase Annotation
 *	MG	16Apr99		Replaced MAXCLASS in free_karykeep() & karyundo() with (kcont->NumOfClasses + 1)
 *  BP	11/7/94:	Dont always make Undo select objects.
 *
 */
//#include <stdafx.h>
#include <stdio.h>
#include <string.h>
#include <woolz.h>
#include <chromlib.h>
#include <sys/types.h>
//#include <fileheaders.h>
#include <redkar.h>
#include <karylib.h>
//#include <cvadmin.h>


/* flag set if karyotype goes thru MaximiseLayout in karcanvas.c */
struct kcontrol *kary_reformatted;			// MG 3.6 no longer uses these three, but will leave them for future possibilities
KaryoLayout lastLayout = DefaultLayout;
short lastAlignment = 0;

extern int show_boundaries;			/* metaphase chromosome boundaries */
extern int show_coloured_boundaries;

//================================================================================================
static BOOL MaximiseLayoutInUndo = TRUE;	// Can switch this off during intensive operations like contrast changing 
											// in Preview Enhnacemnet mode - where no layout changes are required
void EnableMaximisLayoutInUndo()
{
	MaximiseLayoutInUndo = TRUE;
}

void DisableMaximisLayoutInUndo()
{
	MaximiseLayoutInUndo = FALSE;
}
//================================================================================================


/*
 *	KARYKEEP
 *
 *	save karyogram object , boundary and plist in keep lists 
 *	temporarily - in case of undo
 */
void karykeep(int onum, struct kcontrol *kcont)
{
	/* store ptr to original object temporarily in keepemol */
	kcont->keepemol[onum]=kcont->emol[onum];

	/* store ptr to original boundary temporarily in keepbndmol */
	kcont->keepbndmol[onum]=kcont->bndmol[onum];

	// CV3.6 MG - now store mol too so we can handle scale and rotation of reflected, trimmed, straightened objects correctly
	// store ptr to original mol object temporarily in keepmol 
	kcont->keepmol[onum]=kcont->mol[onum];

	/* store a copy of the original object's plist contents in keepsharedplist
	   unless metkeep has already done so - i.e. keepsharedplist!=NULL */
	if (kcont->keepsharedplist[onum] == NULL) {
		kcont->keepsharedplist[onum]=(struct chromplist *)makechromplist();
		*(kcont->keepsharedplist[onum])=*(kcont->mol[onum]->plist);
	}

	/* mark karyogram as modified */
	kcont->modified=1;
}



/*
 *	FREE_KARYKEEP
 *
 *	Clears karyogram keep lists - hold pointers to objects
 *	modified by last operation - for single step karyundo 
 *	Usually called in kinteract.c prior to implementing an operation on
 *	a chromosome.
 */
void free_karykeep(struct kcontrol *kcont)
{
	int onum, chromclass;

	for (chromclass=1;chromclass < (kcont->NumOfClasses + 1);chromclass++)
		kcont->keepclassmax[chromclass-1]=0;

	for (onum=0;onum<kcont->maxnumber;onum++) {

		/* free kept object */
		if ( (kcont->keepemol[onum]!=kcont->emol[onum])
		  && (kcont->keepemol[onum]!=NULL) ){
			kcont->keepemol[onum]->plist=NULL;
			freeobj((struct object *)kcont->keepemol[onum]);
		}
		kcont->keepemol[onum]=NULL;

		/* free kept boundary */
		if ( (kcont->keepbndmol[onum]!=kcont->bndmol[onum])
		  && (kcont->keepbndmol[onum]!=NULL) ) {
			freeobj(kcont->keepbndmol[onum]);
		}
		kcont->keepbndmol[onum]=NULL;

		/* free kept mol object  - introduced in CV3.6 to handle reflected, trimmed or straightened objects*/
		if ( (kcont->keepmol[onum]!=kcont->mol[onum])
		  && (kcont->keepmol[onum]!=NULL) ){
			kcont->keepmol[onum]->plist=NULL;
			freeobj((struct object *)kcont->keepmol[onum]);
		}
		kcont->keepmol[onum]=NULL;

		/* free kept shared plist - unless free_metkeep has already done so */
		if ( kcont->keepsharedplist[onum]!=NULL ) {
			freeplist((struct propertylist *)kcont->keepsharedplist[onum]);
		}
		kcont->keepsharedplist[onum]=NULL;
	}
}


/*
 * remove_splitchrom called by karyundo to remove split-off child
 * chromosomes when a composite or overlap chromosome is restored
 */
#if 0
void remove_splitchrom(struct kcontrol *kcont)
{
	int onum,parent;

	for (onum=0;onum<kcont->maxnumber;onum++) {
		if (kcont->mol[onum]!=NULL) {
			/* check object is result of split */
			if ((kcont->mol[onum]->plist->history[0]==8)
			|| (kcont->mol[onum]->plist->history[0]==1)) {

				/* check if its parent is about to reactivate */
				parent=kcont->mol[onum]->plist->pnumber-1;

				if (parent>=0)
				if (kcont->keepemol[parent] != NULL) {

					remove_chromcanvas(onum, kcont);
				
					/* metundo will delete ool, eool
				  	 and bndool, and update kcont->maxnumber 
				 	  karyundo is called before metundo */

					kcont->mol[onum]->plist=NULL;
					freeobj((struct object *)kcont->mol[onum]);
					kcont->mol[onum]=NULL;

					kcont->emol[onum]->plist=NULL;
					freeobj((struct object *)kcont->emol[onum]);
					kcont->emol[onum]=NULL;

					freeobj(kcont->bndmol[onum]);
					kcont->bndmol[onum]=NULL;

					/* dont free plist leave it to metundo */
				}

			}
		}
	}
}
#endif

/*
 * remove_joinedchrom called by karyundo to remove joined object
 * chromosomes when its component chromosomes are restored
 */
#if 0
void remove_joinedchrom(struct kcontrol *kcont)
{
	int onum,parent;
	struct chromplist *join_plist;

	/* joined object will be last object created */
	onum=kcont->maxnumber-1;

	if (kcont->mol[onum]!=NULL) {
		/* check object is result of join */
		if (kcont->mol[onum]->plist->history[0]==4) {

			/* check if one of its parents is about to reactivate */
			parent=kcont->mol[onum]->plist->pnumber-1;

			if (parent>=0)
			if (kcont->keepemol[parent] != NULL) {

				/* remove chromosome canvas from karyogram */
				remove_chromcanvas(onum, kcont);

				join_plist=kcont->mol[onum]->plist;
				
				/* metundo will delete ool, eool
				   and bndool, and update kcont->maxnumber 
				   karyundo is called before metundo */

				kcont->mol[onum]->plist=NULL;
				freeobj((struct object *)kcont->mol[onum]);
				kcont->mol[onum]=NULL;

				kcont->emol[onum]->plist=NULL;
				freeobj((struct object *)kcont->emol[onum]);
				kcont->emol[onum]=NULL;

				freeobj(kcont->bndmol[onum]);
				kcont->bndmol[onum]=NULL;

				/* dont free plist leave it to metundo */

			}
		}
	}
}
#endif

#if 0
/*
 *	KARYUNDO
 *
 *	undo last operation - restores objects in keeplist and removes their
 * 	existing counterparts
 */
void karyundo(struct kcontrol *kcont)
{
	int onum, chromclass, olddx, olddy;
	Canvas *classcanvas;
	Cclassdata *classdata;
	struct chromosome *oldchrom, *oldmol;
	struct chromplist *oldplist;
	struct object *oldbndry;
	int was_selected = 0;		/* BP */

	/* first remove chrom canvases from last modification */
	for (onum=0;onum<kcont->maxnumber;onum++) {
		oldchrom=kcont->keepemol[onum];

		if (oldchrom!=NULL)
		{
/* BP - keep track if any objects were
 * selected. */
			was_selected |= kcont->selected[onum];

			if (kcont->kar_chromptr[onum]!=NULL) { /* object not deleted */
				/* remove current object canvas */
				remove_chromcanvas(onum, kcont);
			}
		}
	}


	/* restore any classes reformatted by tidyclass */

	for (chromclass=1; chromclass < (kcont->NumOfClasses + 1); chromclass++) {
		if (kcont->keepclassmax[chromclass-1]>0) {
	
			classcanvas=kcont->classptr[chromclass-1];
			classdata=(Cclassdata *) classcanvas->data;

			/* restore num chroms allowed in class */
			classdata->maxchroms=kcont->keepclassmax[chromclass-1];
		
			/* and recalculate chromosome spacing */
			classcanvas->patchgrid.spacing=classcanvas->frame.width 
							/ classdata->maxchroms;
			/* clear keep list */
			kcont->keepclassmax[chromclass-1]=0;
		}
	}



	/* check if any joined object's parents will be revived by Undo
	   and if so remove the joined object */
	remove_joinedchrom(kcont);

	/* check if any split objects parent will be revived by Undo
	   and if so remove the split off objects */
	remove_splitchrom(kcont);


	/* then restore chrom canvases for old objects */
	for (onum=0;onum<kcont->maxnumber;onum++) {

		oldchrom=kcont->keepemol[onum];
		if (oldchrom!=NULL) {
			oldbndry=kcont->keepbndmol[onum];
			oldplist=kcont->keepsharedplist[onum];

			/* restore old object if it has changed */
			if (kcont->emol[onum]!=oldchrom) 
			{
				if(kcont->emol[onum] != NULL)
				{
					kcont->emol[onum]->plist=NULL;
					freeobj((struct object *)kcont->emol[onum]);
				}
				kcont->emol[onum]=oldchrom;
			}
			/* and clear its keep list entry */
			kcont->keepemol[onum]=NULL;

			/* restore old mol object if it has changed - introduced in CV3.6 for reflected, trimmed or stratightened objects */
			oldmol=kcont->keepmol[onum];
			if (kcont->mol[onum]!=oldmol) {
				kcont->mol[onum]->plist=NULL;
				freeobj((struct object *)kcont->mol[onum]);
				kcont->mol[onum]=oldmol;
			}
			/* and clear its keep list entry */
			kcont->keepmol[onum]=NULL;


			/* restore old shared plist data unless metundo 
			   has already done so, i.e. oldplist=NULL */
			if (oldplist!=NULL) {
				*(kcont->emol[onum]->plist)=*(oldplist);

				/* and clear its keep list entry */
				freeplist((struct propertylist *)kcont->keepsharedplist[onum]);
				kcont->keepsharedplist[onum]=NULL;
			}
						
			/* restore old boundary if it has changed */
			if (kcont->bndmol[onum]!=oldbndry) {
				freeobj(kcont->bndmol[onum]);
				/* restore old boundary */
				kcont->bndmol[onum]=oldbndry;
			}
			/* and clear its keep list entry */
			kcont->keepbndmol[onum]=NULL;
	
			oldplist=(struct chromplist *) kcont->emol[onum]->plist;

			/* Only restore previous object canvas if it was a
			   CHROMOSOME , COMPOSITE or OVERLAP */			

			if ((oldplist->otype!=NOISE) && (oldplist->otype!=NUCLEUS)) {

				/* activate object in case it was deleted */
				kcont->acl[onum]=ACTIVE;

				/* re-select object */
/* BP - but only if it was before. */
				if (was_selected)
					kcont->selected[onum] = 1;

				/* save old adjusted position before firstclass,
        	                   which calls newchromcanvas, modifies it */
				olddx=oldplist->offsetx;
				olddy=oldplist->offsety;
				short oldparentsite =  oldplist->keep_parent_site;

				/* reposition chrom back to the class from whence it came */
				firstclass(onum,kcont);

				/* and restore its old adjusted position */
				kcont->kar_chromptr[onum]->frame.cpframe.dx=oldplist->offsetx=olddx;
				kcont->kar_chromptr[onum]->frame.cpframe.dy=oldplist->offsety=olddy;
				kcont->kar_chromptr[onum]->frame.parent_site =  oldplist->keep_parent_site = oldparentsite; // need to make sure we have the parent site as this specifies the origin of the offsets

				if (show_classlabels)
				{
					access_metdisplay(kcont);
					reset_met_annotation_after_undo(kcont, onum);
					access_kardisplay(kcont);
				}

				/* redisplay old chrom */
				intens(1);
				display_Canvas(kcont->kar_chromptr[onum]);
			}
		}
	}

	if (MaximiseLayoutInUndo)
	{
		// Unless Specifically switched off (e.g. in contrast dialog for fast updates)
		// always auto layout and auto scale chroms to fit karyogram
		// - position/scale/orientation may have changed
		AutoKaryLayout(kcont);
	}


	int colour =0;
	if(show_coloured_boundaries && show_boundaries)
	{
		colorize_met_boundaries(kcont, -1);
		//show_selected(kcont);
		return;
	}
	if((show_boundaries) && (!show_coloured_boundaries))
	{
		/* set colour based on brightfield, fluo, CGH or MFISH */
		colour = ((kcont->drawmode == COPY) || (kcont->assoc_type != ASSOC_NONE)) ? BLUE : YELLOW;
		colorize_met_boundaries(kcont, colour);
		//return;
	}
}
#endif