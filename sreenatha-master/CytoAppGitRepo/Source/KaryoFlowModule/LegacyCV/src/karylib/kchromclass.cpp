/*
 * kchromclass.cpp  M.Castling	28/8/92
 *
 * Normalise feature measurements, then
 * classify chromosomes using distance from class means.
 * Also classify using re-arrangement from over-subscribed classes.
 *
 * Optionally either write entire classified cell for karyogramming,
 * or just write summary output in format suitable for constructing
 * confusion matrix (if flag /c specified).
 *
 * This program should be run on individual cells.
 *
 * NOTE THIS PROGRAM WORKS WITH CLASS NUMBERS 0 - MAXCLASS-1.
 *
 **********************************************************************
 * NORMALISATION REQUIRES ALL CHROMOSOMES, COMPOSITES AND OVERLAPS !!!!
 *
 * Apparently, this is no longer the case  MG 11/12/92
 **********************************************************************
 *
 * Modifications
 * 
 * 24Feb03	JMB	Now uses CVGetCVDirectory() in kchromclass(), rather than
 *				using hard-coded install directory path.
 * 19Oct99	SN	Added classify by size routine, kchromclass_by_length().
 * 16Aug99	SN	Disabled classifier_is_locked (traincl.lib) as it does nothing.
 * 30Jun99	KS	Classifier file locking re-introduced
 * 24Jun99	SN	Fixed array overrun in kchromclass.
 * 25May99	SN	kclass_read returns error flag.
 * 27Apr99	SN	lpriorp and clwn are now pointers.
 * 26Apr99	SN	Private function prototypes
 *				Removed redundant includes, #define, extern, statics and function dispclass.
 *				Removed ndim static from all functions and function calls, as redundant.
 *				Replaced MAXDIM with NUMOFFEATURES for the number of elements of array sfv,
 *					 the list of selected feature indicies.
 * 15Apr99	MG	(kcont->NumOfClasses + 1) is now passed into fixclass() to replace MAXCLASS
 * 15Apr99	MG	Replaced fixed sized arrays based on MAXOBJS and MAXCLASS with 
 *				dynamic arrays based on kcont->MaxObjs & kcont->NumOfClasses in:
 *					kchromclass()
 *					reclassify_obj()
 *
 * 07Apr99	MG	CPP and general tidy up - remove #ifdef i386 etc.
 * 06Apr98	WH	class goes to chromclass to keep C++ compiler happy
 * 28thFeb95	MG	Modified to use UNIX file locking.
 * 	 /8/94	CAJ	Modified to provide new classification technique
 * 21/9/92 	MG	commented out DEBUG, placed further ifdef DEBUG statements
 * 11/12/92 	MG 	only nomalise and classify CHROMOSOME types
 */


#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

//#include <redkar.h>
#include <machdef.h>
#include <chromanal.h>
#include <chromlib.h>
#include <karylib.h>
//#include <admin_call.h>
//#include <cvadmin.h>
//#include <fileheaders.h> 
//#include <seg_control.h>
//#include <message.h>
//#include <warning.h>
//#include <common.h>
#include <assert.h>
#include "species.h"
#include <trainkarcl.h>

#include <CreateArray.h>		// For dynamic arrays				//SN26Apr99

#include <io.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/locking.h>
#include <share.h>
#include <fcntl.h>



#undef MAXOBJS
#undef MAXDIM
#undef MAXCLASS


#define MAXPROB 2000000000.0

// For ordering chromosomes by length										//SN19Oct99
typedef struct length_record {
	int onum;
	int length;
} lenrec;

// Default human information
/* logs of prior probabilities */
//static double lpriorp_default_human[MAXCLASS] = {
static double lpriorp_default_human[DEFAULT_NUMOFCLASSES] = {
0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
0.0, 0.0, 0.0, -0.5};

// Expected number of chromosomes in each class
//static int clwn_default_human[MAXCLASS] = {
static int clwn_default_human[DEFAULT_NUMOFCLASSES] = {
2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
2, 2, 2, 1};


static double *lpriorp = NULL;										//SN26Apr99 Module data
static int *clwn = NULL;											//SN26Apr99 Module data


//static int ndim,sfv[MAXDIM],nsfv,nclass;							//SN26Apr99 Removed ndim.
																	//			Replaced MAXDIM with NUMOFFEATURES.
static int sfv[NUMOFFEATURES],nsfv,nclass;


//
// PRIVATE PROTOTYPES
//
int fixclass(struct chromcl *chp, int numchr);
void determine_lpriorp( int nclass, int default_human);
void determine_clwn( int nclass, int default_human);
static int minlength_record(const void *pv1, const void *pv2);


/*********************************************************************/

void kclass_def()
{
	// It is implicitly assumed that by calling this function default
	// human data is to be classified. Hence it is up to the calling
	// function to check this.

	// Point to log prior probabilities and expected chroms arrays	//SN26Apr99
	determine_lpriorp( DEFAULT_NUMOFCLASSES, TRUE);
	determine_clwn(    DEFAULT_NUMOFCLASSES, TRUE);

	//set_def_params(&nclass,&ndim,sfv,&nsfv,lpriorp);
	set_def_params( &nclass, sfv, &nsfv, lpriorp); /* a MC */		//SN26Apr99 Removed ndim
}

/*********************************************************************/

/* read class mean vectors and variance-covariance matrix */

//void kclass_read(char *classfile)
//void kclass_read(char *classfile, int default_human, int NumOfClasses)	//SN14May99
int kclass_read(LPCTSTR classfile, int default_human, int NumOfClasses)		//SN25May99
{
	FILE *fclass;
	//DIR *classdir = NULL;
	TCHAR classdirname[_MAX_PATH];
	TCHAR *chptr;
	int retval;
	TCHAR lockname[_MAX_PATH];
	int classdir_fh=0;


	// Create log prior probabilities and expected chroms arrays	//SN26Apr99
	determine_lpriorp( NumOfClasses, default_human);
	determine_clwn(    NumOfClasses, default_human);
	
	/* strip classifier filename from classifier directory name */
	_tcscpy(classdirname,classfile);
	chptr=classdirname+_tcslen(classdirname)-1;
	while ((chptr > classdirname) && (*chptr != '/'))
		chptr--;

	*chptr='\0';

	/* Must loop before reading classifier file if another
	   user is generating a new version of this classifier 
	   - this will normally be for a very short time */
	//while (classifier_is_locked(classdirname)){
	//}

	/* lock the classifier directory to read_only - while reading */
	retval = TRUE;
//	if (lock_open_classifier(classdirname, &classdir) == 0) {

	// KS CREATE A LOCK FILE FOR THIS CLASSIFIER FILE
	_tcscpy(lockname, classfile);
	_tcscat(lockname, _T(".lock"));
	classdir_fh = _tsopen( lockname, _O_CREAT | _O_TEMPORARY, _SH_DENYNO, _S_IREAD | _S_IWRITE );
	if( classdir_fh != -1 )
	{ // opened OK
		if( _locking( classdir_fh, LK_NBLCK, 1L ) != -1 )
		{ // locked OK

			fclass = open_classfile(classfile, &nclass, &nsfv);			//SN26Apr99 Added classes and features.

			if (fclass != NULL)
			{
				assert( NumOfClasses == nclass );						//SN26Apr99 Sanity check!
				read_c_params( fclass, nclass, sfv, nsfv, lpriorp);		//SN26Apr99 Removed ndim. Pass classes + features as int
				fclose(fclass);
			}
			else
			{
				fprintf(stderr,"Cannot open classifier parameter file %s\n",classfile);
				retval = FALSE;
			}

			/* unlock the classifier directory */
//			closedir(classdir);

			// KS RELEASE THE LOCK FILE
			_locking( classdir_fh, LK_UNLCK, 1L );
			_close( classdir_fh );
			classdir_fh=0;
		}
		else
		{
			_close( classdir_fh );
			classdir_fh=0;
			assert(false);
			fprintf(stderr,"Cannot lock classifier parameter lock file %s\n",classfile);
			retval = FALSE;
		}
	}
	else
	{
		classdir_fh=0;
		assert(false);
		fprintf(stderr,"Cannot open classifier parameter lock file %s\n",classfile);
		retval = FALSE;
	}

	return( retval);
}


//
//	determine_lpriorp	created SN	26Apr99
//
//	Sets lpriorp to an array of the log prior probabilities
//	for each class.
//
void determine_lpriorp( int nclass, int default_human)
{
	int i;
	int farrayOK;


	// Clear away last lpriorp array, whether dynamic array or pointer to static
	if ( lpriorp != lpriorp_default_human )
		DestroyArray1D( lpriorp);
	lpriorp = NULL;

	// If default human data required then set known human log prior probabilities...
	if (default_human)
	{
		lpriorp = lpriorp_default_human;
		return;
	}

	// ...otherwise create dynamic array for non-default human data
	farrayOK = 1;
	lpriorp = (double *) CreateArray1D( nclass, sizeof(double), _T("determine_lpriorp"), _T("lpriorp"), &farrayOK);
	if (!farrayOK)
		return;

	// TODO **** Assign log prior probabilities, currently set to 0.0 as for most human chrom classes.
	for (i=0; i<nclass; i++)
		lpriorp[i] = 0.0;
}


//
//	determine_clwn	created SN	27Apr99
//
//	Sets clwn to an array of the log prior probabilities
//	for each class.
//
void determine_clwn( int nclass, int default_human)
{
	int i;
	int farrayOK;


	// Clear away last clwn array, whether dynamic array or pointer to static
	if ( clwn != clwn_default_human )
		DestroyArray1D( clwn);
	clwn = NULL;

	// If default human data required then set to known human expected number of chromsomes in each class...
	if (default_human)
	{
		clwn = clwn_default_human;
		return;
	}

	// ...otherwise create dynamic array for non-default human data
	farrayOK = 1;
	clwn = (int *) CreateArray1D( nclass, sizeof(int), _T("determine_clwn"), _T("clwn"), &farrayOK);
	if (!farrayOK)
		return;

	// TODO **** Assign expected number of chromsomes, currently set to 0.0 as for most human chrom classes.
	for (i=0; i<nclass; i++)
		clwn[i] = 2;
}



/********************************************************************************/


//SN26Apr99 Not used anywhere in WinCV, could be useful for debugging though at some stage though ...
#if 0
/*
 * D I S P C L A S S --- display classification info
 *
 */
int dispclass(struct chromcl *chp, int numchr)
{
	int i, j;

	/*
	 * for all chromosomes
	 */
	for (i = 0; i < numchr; i++) {
		printf("%d ",i);
		for(j = 0; j < CUTOFF; j++) {
			printf("cl=%d  p=%f ",
				chp->cllika[j].chromclass+1,
				chp->cllika[j].lval);
		}
		printf("\n");
		chp++;
	}

	return(1);
}
#endif


/********************************************************************************/

void kchromclass( struct chromosome ** chrlist, int numchr)
{
	assert( chrlist != NULL);
	assert( numchr > 0);

	// nclass,		the number of classes understood by the classifier and loaded by kclass_def or kclass_read.
	// ndim,		the number of chromosome features measured
	// numchr,		the number of chromosomes to classify
	int ndim = getdim();

	// allocate list space based on nchromcount
	int farrayOK = 1;
	TCHAR szfnname[] = _T("kchromclass");
	struct chromcl * chrcl			= (struct chromcl *)	CreateArray1D( numchr, sizeof(chrcl[0]), szfnname, _T("chrcl"), &farrayOK);

	// allocate list space based on ndim
	double * fvec					= (double *)			CreateArray1D( ndim, sizeof(fvec[0]), szfnname, _T("fvec"), &farrayOK);
	double * svec					= (double *)			CreateArray1D( ndim, sizeof(svec[0]), szfnname, _T("svec"), &farrayOK);
	struct normaliser * vn			= (struct normaliser *)	CreateArray1D( ndim, sizeof(vn[0]), szfnname, _T("vn"), &farrayOK);

	// allocate list space based on nclass - note +1 for stack
	struct classcont * clcontents	= (struct classcont *)	CreateArray1D( nclass + 1, sizeof(clcontents[0]), szfnname, _T("clcontents"), &farrayOK);

	// check allocation
	if (!farrayOK)
	{
		// Free allocated lists
		DestroyArray1D( chrcl);
		DestroyArray1D( fvec);
		DestroyArray1D( svec);
		DestroyArray1D( vn);
		DestroyArray1D( clcontents);
		return;
	}

	// Initialise arrays
	int i;
	for (i = 0; i < numchr; i++)
	{
		memset( &chrcl[i], 0, sizeof(chrcl[0]) );
	}

	// clear class contents structure, including final element of clcontents array used for stack, hence the +1
	for (i = 0; i < nclass + 1; i++)
	{
		clcontents[i].n = 0;
	}

	/* compute cell normaliser */
	// TODO: make it accept it chromplist arrays instead
	normvec( chrlist, numchr, vn);

	// Classify by likelihood
	struct chromosome *obj = NULL;
	struct classcont *classcontp = NULL;
	struct chromcl *chp = chrcl;
	int j, chromclass;
	for (j=0; j<numchr; j++)
	{
		obj = chrlist[j];
		getvec(obj->plist,fvec); /* feature vector */

		applynorm(fvec,svec,sfv,vn,nsfv); /* normalised feature vector */
		
		/* likelihood classifier */
		chromclass = findclass(svec,lpriorp,&(chp->cllika[0]),nclass,nsfv); 
		chp->trueclass = obj->plist->Cpgroup - 1;
		chp->rawclass = chromclass;
		chp->newclass = chromclass;
		classcontp = &clcontents[chromclass];

		// Add chrom to class contents if there is room							//SN24Jun99
		if (classcontp->n < MAXCHILD)
		{
			classcontp->chcl[classcontp->n] = chp;
			classcontp->n++;
		}
		chp++;
	} /* 'numchr' for loop */

	// TODO : Need a switch for rearrangement classifier
	/* re-arrange so as to move objects from over-subscribed
	classes to under-subscribed as far as plausibility allows */
	rearrange(chrcl,clcontents,numchr,clwn,nclass);

	/* write the data */
	for (i=0; i<numchr; i++)
	{
		chrlist[i]->plist->pgroup = chrcl[i].newclass+1;
/* printf("%d class=%d",i,chrcl[i].newclass+1);*/

		for (j=0;j<CUTOFF;j++)
		{
			if (chrcl[i].newclass==chrcl[i].cllika[j].chromclass)
			{
				chrlist[i]->plist->pgconf=(short)chrcl[i].cllika[0].lval;
/* printf(" likelihood=%d",chrlist[i]->plist->pgconf);*/
				break;
			}
		}
/* printf("\n"); */
	}

	// Free allocated lists
	DestroyArray1D( chrcl);
	DestroyArray1D( fvec);
	DestroyArray1D( svec);
	DestroyArray1D( vn);
	DestroyArray1D( clcontents);
}


///*
// *	RECLASSIFY_OBJ
// *
// *	reclassifies a single object based on its feature measurements
// */
//void reclassify_obj(struct kcontrol *kcont, int objnum)
//{
//	int *temponum = NULL;		// kcont->maxnumber array
//	int *tempclass = NULL;		// kcont->maxnumber array
//	struct chromosome *obj;
//	int onum, numchr, keepclass, classid;
//	TCHAR szfnname[] = _T("reclassify_obj");
//	int farrayOK;
//
//	if (!kcont)
//		return;
//
//	// Load a suitable classifier and classify otherwise clasifiy as abnormal							//SN10JUn99
//	classid = kcont->classifier;
//	if ( load_classifier( kcont, &classid) == LOADCLASS_ERROR )
//	{
//		kcont->mol[objnum]->plist->pgroup = kcont->NumOfClasses + 1;	//abnormal
//		return;
//	}
//
//	// Allocate dynamic arrays
//	farrayOK = 1;
//	temponum	= (int *)CreateArray1D( kcont->maxnumber, sizeof(temponum[0]), szfnname, _T("temponum"), &farrayOK);
//	tempclass	= (int *)CreateArray1D( kcont->maxnumber, sizeof(tempclass[0]), szfnname, _T("tempclass"), &farrayOK);
//	
//	// check allocation
//	if (!farrayOK)
//	{
//		DestroyArray1D( temponum);
//		DestroyArray1D( tempclass);
//		return;
//	}
//
//	/* save previous classification in temp array */
//	numchr=0;
//	for (onum = 0; onum < kcont->maxnumber; onum++) {
//		
//		if (kcont->acl[onum] & ACTIVE ) {
//			obj = kcont->mol[onum];
//
//			if (obj->type==1) {
//				if (obj->plist->otype <= 1) { /* chromosome ? */
//					temponum[numchr]=onum;
//					tempclass[numchr]=obj->plist->pgroup;
//					numchr++;
//				}
//			}
//		}
//	}
//
//	/* reclassify all objects */
//	kchromclass(kcont);
//
//	/* keep chosen object's new classification */
//	keepclass=kcont->mol[objnum]->plist->pgroup;
//	if (keepclass<=0)
//		keepclass=kcont->NumOfClasses+1;	// stack
//
//	/* restore previous classifcation to all objects */
//	while (numchr>0) {
//		numchr--;
//		obj=kcont->mol[temponum[numchr]];
//		obj->plist->pgroup=tempclass[numchr];
//	}
//
//	/* restore chosen object's new classification */
//	kcont->mol[objnum]->plist->pgroup=keepclass;
//
//	// free allocation
//	DestroyArray1D( temponum);
//	DestroyArray1D( tempclass);
//}	
//
//
///*
// *	minlength_record	SN	18Oct99
// *
// * 	Given two lenrec structures, returns positive value if
// *	lenrec in first struct is less than that in second.
// *
// */
//static int minlength_record(const void *pv1, const void *pv2)
//{
//	lenrec * pl1 = (lenrec *) pv1;
//	lenrec * pl2 = (lenrec *) pv2;
//
//	return( pl2->length - pl1->length );
//}
//
//
///*
// *	kchromclass_by_length		SN	21Oct99
// *
// *	Classifies all chromosome objects by their length. First their lengths are
// *	ranked then the ranking is converted to a classification. The objects are
// *	equally distibuted between all class positions UNLESS there are fewer objects
// *	than positions. In this case the objects are placed (one per position) until
// *	all objects are allocated.
// *
// */
//void kchromclass_by_length(struct kcontrol *kcont)
//{
//	int onum, i;
//	int MaxNumOfObjs, NumOfObjs, NumOfPosns, NumOfClasses;
//	lenrec * lenlist;
//	double dposn;
//	CTemplateKaryotype * ptemplate;
//	struct chromosome ** mol;
//	SMALL * acl;
//
//
//	// Useful shorthand
//	MaxNumOfObjs = kcont->maxnumber;
//	mol          = kcont->mol;
//	acl          = kcont->acl;
//	ptemplate    = (CTemplateKaryotype *) kcont->TemplatePtr;
//
//	// Get number of classes
//	if (ptemplate == NULL)		NumOfClasses = DEFAULT_NUMOFCLASSES;
//	else						NumOfClasses = ptemplate->GetNumberOfClasses();
//
//	// Get number of class box positions
//	//  - from template
//	//****SIMPLE DEFAULT****************
//	NumOfPosns = NumOfClasses;				
//
//	// Construct position to class mapping
//	//****ENHANCEMENT*********************
//
//	// Make list of object lengths
//	lenlist = (lenrec *) Calloc( MaxNumOfObjs, sizeof(lenlist[0]));
//
//	// If memory allocation fails then dont classify and leave
//	if (lenlist == NULL)
//		return;
//
//	// Fill list of object lengths with only CHROMOSOME objects
//	NumOfObjs = 0;
//	for (onum=0; onum<MaxNumOfObjs; onum++)
//	{
//		if ( (acl[onum] & ACTIVE)            &&         // Active object
//			 (mol[onum]->type == 1)         &&			// CHROMOSOME object
//			 (mol[onum]->plist->otype <= 1)    )		// really really a CHROMOSOME object!!! or UNKOWN????
//		{
//			lenlist[NumOfObjs].onum   = onum;
//			lenlist[NumOfObjs].length = mol[onum]->plist->length;
//			NumOfObjs++;
//		}
//	}
//
//	// Order chrom objects list by length
//	qsort( lenlist, NumOfObjs, sizeof( lenrec), minlength_record);
//
//
//	if (NumOfObjs <= NumOfPosns)
//	{
//		// Fewer objects than positions for them.
//		// Copes with NumOfObjs == 0 as loop is not entered.
//		// Copes with NumOfObjs == 1 placing only object in 1st class and NumOfPosns >= 1 always.
//		for (i=0; i<NumOfObjs; i++)
//		{
//			mol[ lenlist[i].onum ]->plist->pgroup = i + 1;
//		}
//	}
//	else 
//	{
//		// More objects than positions
//		for (i=0; i<NumOfObjs; i++)
//		{
//			// Map list index to position: dposn real range [0.0, (NumOfPosns-1)]
//			dposn = (double) i * (NumOfPosns-1) / (NumOfObjs-1);
//
//			// Round position index and map to class value (ie add +1 for this simple implementation)
//			mol[ lenlist[i].onum ]->plist->pgroup = ((short) (dposn + 0.5)) + 1;
//		}
//	}
//
//	// Free list of lengths
//	Free( lenlist);
//}
