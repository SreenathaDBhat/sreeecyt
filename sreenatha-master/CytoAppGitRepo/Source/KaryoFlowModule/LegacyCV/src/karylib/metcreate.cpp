/*
 *
 *	metcreate.cpp	Mark Gregson 	18/2/93
 *
 *	routines for creating metaphase and karyogram canvases from files
 *
 * Mods:
 *
 *	Dec/Jan 02/03 MC Mods for Metaphase Annotation
 * 27Jul99		MG	Make sure all annotation canvases are created with same diemnsions as parent
 * 23Jun99		MG	save_metaphase() and save_karyogram() now return status of save
 * 14May99		dcb	get_chromwidth() Increase size of widths[] array to 2k (May need to be variable)
 * 15Apr99		MG	flexkar mods - MAXOBJS replaced with kcont->MaxObjs and dynamic arrays
 *					The following will also attempt to allocate more objects to the kcontrol
 *					object lists if not limited by UNIX compatibility.
 *						load_sat_metaphase()
 *						create_metaphase()
 *
 *					Only small changes to load_metaphase() and save_metaphase() required to read
 *					and write additional kcont->MaxObjs and kcont->NumOfClasses() fields, 
 *					and to read/write the whole	of the kcont->acl[] list based on kcont->MaxObjs
 *					instead of MAXOBJS.
 *
 *					load_metaphase() and load_karyogram() will try to reallocate kcontrol object lists
 *					if the saved metaphase has a non default kcont->MaxObjs - if this fails,
 *					possibly because of UNIX compatibility, it stops the image load and informs
 *					the user
 *
 *					load_metaphase() and load_karyogram() will try to reallocate lists based on 
 *					kcont->NumOfClasses	(for different species) if the saved metaphase is not default human
 *					- this should not fail - if it does it stops the image load and informs
 *					the user
 *
 *
 *	8Apr99		MG	CPP and general tidy up - remove #ifdef i386 etc.
 *	7Apr99		MG	xscale and yscale are no passed thru to chromfeatures()
 *					instead of using tacky externs (in chromlib\vertical.c).
 *	6Feb99		MC added NULL test for rewind(f) in load_metaphase() and load_karyogram()
 *				   following crash that occured when the corresponding met. image to a loaded
 *				   cgh image was about to be loaded. The fopen() failed because filename was corrupted
 *				   However this cgh case contained 2 metaphase images hence it was a
 *				   corrupt cgh case. But none the less need to protect rewind().
 *
 *  3Feb99		MC Sorted out Warning dialog messages for met & kar load/save functions
 *
 *	 6Aug98		MG	Added SpeedUpMalloc() to file loading routines
 *	16Jul97		MG	Dumped cghflag and mfishflag in favour of
 *				of assoc_type.
 *
 *   4/17/97        	BP:     Replace class with chromclass.
 *
 *	18Aug98	MC	Added further 'port' mods for wincv - for case import

 *				'load_sat_metaphase()'

 *	18Jun97	WH	Added Chromosome boundary stuff (MG's)
 *	6Jul96		MG	Removed set_met/kar_highlights_visibility()
 *				from load_metaphase() and load_karyogram()
 *				coz this is done in kcont_admin() anyway
 *				and so we could stop it happening if the
 *				image was gonna be displayed in casebase.	
 *
 *	5/2/96	BP:		Use GRAB_BUFFER (no more FRAME_BUFFER).
 *	21Mar96		MG	Added CGHprobes highlight display capability
 *
 *	6Nov95		MG	Use attach_Canvas instead of paste_Canvas when
 *				creating annotation canvases.
 *
 *	9Jun95		MG	Handle new satcap2 TIFF images 
 *
 *	8Mar95		MG	Set visibility of met and kar highlights
 *				when met/kar loaded/created
 *
 *	11/9/95		MG	Now handles CGH metaphase and karyotypes
 *
 * 	3/8/94 		CAJ 	Modified load_metaphase to measure 2 new features on 
 * 				earlier files. New features automatically measured now.
 *
 *	1/9/94		MG	Added more error checking, and made sure image
 *				loading routines return NULL pointer if they fail.
 */

//#include <stdafx.h>
#include <stdio.h>
//#include <cvadmin.h>
#include <woolz.h>
#include <chromlib.h>
#include <redkar.h>
//#include <interact.h>
//#include <message.h>
//#include <fileheaders.h>
//#include <warning.h>
//#include <psi.h>
#include <rawtypes.h>
#include <karylib.h>
//#include <graph.h>
//#include <capabilities.h>

int save_tries = 0;	//number of tries at saving 

extern int show_boundaries;			/* metaphase chromosome boundaries */
extern int show_coloured_boundaries;

/************************************************************************************/
/*
 *	This function pastes object highlights onto a canvas chain
 *	in order of their canvas type (numeric) which maintains the display
 *	order when more tha one highlight type visible.i.e. axes appeear on top
 *	of centromeres on top of CGH ratios on top of CGH probes.
 */
// WH START FROM KHIGHLIGHTS to get load_karyogram compiling
void paste_highlight_inorder(Canvas *highlight, Canvas *parent)
{
	Canvas *subcan;

	if (!parent || !highlight) 
		return;


	subcan=parent->patch;

	if (!subcan) {
		paste_Canvas(highlight, parent);
		return;
	}

	while ((subcan->next) && (subcan->type <= highlight->type))
		subcan=subcan->next;

	if (subcan->type > highlight->type) {
		highlight->next=subcan;
		highlight->previous=subcan->previous;
		highlight->parent=parent;

		if (highlight->previous != NULL)
			highlight->previous->next=highlight;

		subcan->previous=highlight;
		
		if (parent->patch == subcan)
			parent->patch=highlight;
	}
	else {
		highlight->next=subcan->next;
		highlight->previous=subcan;
		highlight->parent=parent;

		if (highlight->next != NULL)
			highlight->next->previous = highlight;

		subcan->next=highlight;
	}
}

/************************************************************************************/
/*
 *	Routine to take a canvas patch chain and reorder al canvases
 *	based on their canvas type
 */
void reorder_highlights(Canvas *parent)
{
	Canvas *canvas, **sublist;
	int count, i;
	
	if (!parent)
		return;

	canvas=parent->patch;
	if (!canvas)
		return;

	count=1;
	while (canvas->next) {
		count++;
		canvas=canvas->next;
	}

	if (count==1)
		return;

	/* create temp list of pointers to canvases */
	sublist=(Canvas **)Malloc(sizeof(Canvas *) * count);

	if (!sublist)
		return;
	
	/* cut canvases off parent patch and store pointers in sublist */
	for (i=0;i<count;i++) {
		canvas=parent->patch;
		cut_Canvas(canvas);
		sublist[i]=canvas;
	}

	/* stick canvases back on to parent in correct order */
	for (i=0;i<count;i++)
		paste_highlight_inorder(sublist[i],parent);

	/* free temporary list of pointers */
	Free(sublist);
}
// WH END FROM KHIGHLIGHTS

#if 0
/*
 *	GET_CHROMWIDTH
 *
 *	Analyses all chromosomes in a metaphase to determine average
 *	chromosome width - used by draw-axes to split composites
 *	Note: kcont->chromwidth is a half width.
 */
void get_chromwidth(struct kcontrol *kcont)
{
    int i, w, type;
    int widths[2048];	// May need to be variable
    struct chromplist *plist;
    struct iwspace iwsp;
    struct chromosome *obj;
    float sum,n;
	float scf = 1;
	

    if (kcont==NULL)
    	return;

    for (i = 0; i < 2048; i++)
		widths[i] = 0;

		for (i=0; i<kcont->maxnumber; i++) 
		{

    		if (kcont->acl[i]==ACTIVE) 
			{
    			obj=kcont->ool[i];

    			if (obj!=NULL) {
    				plist=obj->plist;

    				type=plist->Cotype;
    				if (type==0)
    					type=plist->otype;

    				if ((type==CHROMOSOME) || (type==COMPOSITE)) {
    					initrasterscan((struct object *)obj, &iwsp, 0);
    					while (nextinterval(&iwsp)==0)
    						widths[iwsp.colrmn]+=1;
    				}
    			}
    		}
		}

		if (kcont->ool[0] != NULL && kcont->ool[0]->plist != NULL)
		{
 			if((kcont->ool[0]->plist->pixel_Y_micronsize <= 0) || (kcont->ool[0]->plist->pixel_X_micronsize <= 0))
 				scf=(float)(kcont->met_canvas->frame.height / 8)/(float)COHU_IMAGE_HEIGHT;
			else
				scf = (float)(COHU_PIXEL_WIDTH)/(float)kcont->ool[0]->plist->pixel_Y_micronsize;///COHU_IMAGE_HEIGHT;
		}
		else
 			scf=(float)(kcont->met_canvas->frame.height / 8)/(float)COHU_IMAGE_HEIGHT;

		sum=0.0;
		n=0.0;

		for (i = 6; (i < 40 * scf) && (i < 2048); i++) 
		{
    		sum=sum+(i*widths[i]);
    		n=n+widths[i];
		}


		/* if sensible number of intervals available */
		if (n > 100) 
		{	
    		w=(int)(sum/(n*2.0)) - 1;

    		if (w<3)
    			w=3;
    		if (w>20)
    			w=20;

    		kcont->chromwidth = w;//*scf;

		}
		else
			kcont->chromwidth=(int)(8*scf);	/* default width */

}
    

/*
 * read objects from Cytoscan metaphase file.
 * set them active so that feature measurement may proceed
 * and generate their boundaries
 */
void read_Cytoscan_met(struct kcontrol *kcont, FILE *f)
{
    struct chromosome *obj, *robj;
    struct cell_header *ch;
    struct cell_header_1 *ch1;
    struct cell_header_2 *ch2;
    int version;
    int onum;
    int yshift;
    double xscale,yscale,rads;


    if (kcont==NULL)
    	return;
    
    while ((obj=(struct chromosome *)readobj(f)) != NULL) {

    	if (obj->type == 110) {

    		/* special to read cytoscan created file headers,
    		   with integer words flipped - hence >>16 */

    		ch=(struct cell_header *) obj->plist;
    		version=ch->version >> 16;
    		switch (version) {
    		case 1:
    			ch1=(struct cell_header_1 *) obj->plist;
    			kcont->FIP=ch1->dig_type >> 16;
    			break;
    		case 2:
    		case 3:
    			ch2=(struct cell_header_2 *) obj->plist;
    			kcont->FIP=ch2->dig_type >> 16;
    			break;
    		default:
    			kcont->FIP=ch->dig_type >> 16;
    			break;
    		}


    		if (kcont->FIP==0)
    			kcont->FIP=DIGFIP;

    		/* this is a frig FIP should have been 10 */
    		if (kcont->FIP==42)
    			kcont->FIP=DIGTV | DIGFLUOR;
    		
    		if (kcont->FIP & DIGFLUOR )
    			kcont->drawmode=INVERT;

    		freeobj((struct object *)obj);
    	}
    	if (obj->type == 1) {
    		/* bugger it ! - lets make sure all objects 
    		   have 1:1 aspect ratio */
    		if (kcont->FIP & DIGFIP) {
    			yshift= - (obj->idom->line1 / 4);
    			yscale = 0.75;
    			xscale = 1.0;
    			rads = 0.0;
    			robj = (struct chromosome *)spinsqueeze((struct object *)obj, &rads, &xscale, &yscale);

    			yshift=yshift-(robj->idom->line1-obj->idom->line1);
    			moveidom(robj->idom,0,yshift);
    			movevdom(robj->vdom,0,yshift);
    			robj->plist=obj->plist;
    			obj->plist=NULL;
    			freeobj((struct object *)obj);
    			obj=robj;
    		}

    		if (obj->plist != NULL)
    			freeplist((struct propertylist *)obj->plist);

    		obj->plist = (struct chromplist *)makechromplist(); /* plist zeroed */
    		obj->plist->number = kcont->maxnumber+1;

    		kcont->ool[kcont->maxnumber] = obj;
    		kcont->acl[kcont->maxnumber] = ACTIVE;
    		kcont->bndool[kcont->maxnumber]=(struct object *)ibound((struct object *)obj,1);
    		kcont->maxnumber++;
    	}
    }
    /*************************************/
    /****** always 1:1 aspect ratio ******/
    /*************************************/
    kcont->FIP=(kcont->FIP & DIGFLUOR) | DIGTV;

    /* duplicate ool in eool - enhanced original object list */
    for (onum=0; onum<kcont->maxnumber; onum++)		
    	kcont->eool[onum] = (struct chromosome *)duplicate_obj((struct object *)kcont->ool[onum]);

}


/*
 *	LOAD_CYTOSCAN_METAPHASE
 *	
 *	loads metaphase objects from a metaphase file f
 *	initialises kcontrol structure - which it returns
 *	loads default classifer - for now
 *	creates initial metaphase canvas
 *
 *	Does not display canvas - this is responsiblity of kcont_admin()
 */
struct kcontrol *load_Cytoscan_metaphase(DDGS *metdg, LPCTSTR filename, int filegroupid, int imageid)
{
    FILE *f;
    struct kcontrol *kcont=NULL;
    int i, ok;


    if ((f = _tfopen(filename, _T("rb"))) == NULL) {
    	wait_warning(Mess(_T("Eopencytomet"), IDS_KARY_ERR_OPENCYTOMET));
    	ok=0;
    }
    else
    	ok=1;

    if (ok) {
    	show_message(Mess(_T("loadingmet"), IDM_ADM_LOADINGMET));

    	/* initialise kcontrol structure */
    	kcont=init_kcontrol(metdg,NULL);

    	if (kcont==NULL) {
    		fclose(f);
    		return(NULL);
    	}

    	/* assign file and image ID's */
    	kcont->filegroupid=filegroupid;
    	kcont->met_imageid=imageid;

    	/* read in metaphase object file to display */
    	read_Cytoscan_met(kcont,f);

    	/**** NEED TO KNOW KCONT->CLASSIFIER ******/
    	/* this is not held currently in Cytoscan */
    	/* file headers                           */
    	/******************************************/

    	/* create initial metaphase canvas  - create individual object
      	    canvases and fill met_chromptr quick canvas access array */
    	create_metaphase_canvas(kcont);

    	/* do a quick guess at probable object types before displaying */
    	for (i=0; i<kcont->maxnumber; i++)
    		probotype(kcont->ool[i]);
    
    	/* add object type highlights to metaphase canvas 
    	   i.e. hulls around composites, boxes around noise */
    	set_obtype_canvases(kcont);

    	/* show/hide object highlights based on current global flags */
    	set_met_highlights_visibility(	kcont,
    					show_object_highlights,
    					show_centromeres,
    					show_axes,
    					show_boundaries,
    					show_CGHratios,
    					show_CGHprobes,
    					show_MFISHpseudo,
    					show_MFISHprobes,	
						show_classlabels,
						show_coloured_boundaries);

    	/* determine chromosome width */
    	get_chromwidth(kcont);

    	show_message(Mess(_T("nowt"), IDS_NOWT));

    }
    else
    	kcont=NULL;

    if (f)
    	fclose(f);

    return(kcont);
}



/*
 *	LOAD_PSI_METAPHASE
 *	
 *	loads metaphase objects from raw psi image
 *	initialises kcontrol structure - which it returns
 *	loads default classifer - for now
 *	creates initial metaphase canvas
 *
 *	Does not display canvas - this is responsiblity of kcont_admin()
 */
struct kcontrol *load_psi_metaphase(DDGS *metdg, LPCTSTR filename, int filegroupid, int imageid)
{
	return NULL;

}



/*
 *	LOAD_SAT_METAPHASE
 *	
 *	loads metaphase objects from thresolded satellite image
 *	initialises kcontrol structure - which it returns
 *	loads default classifer - for now
 *	creates initial metaphase canvas
 *
 *	Does not display canvas - this is responsiblity of kcont_admin()
 *
 * 	BP - now make sure we don't get too many objects, and
 * 	discard the small crappy ones...
 *
 *	MG - now handle satcap2 TIFF style images as well as old satcap1 targa images
 *
 *	MG 15Apr99 - if flexkar is enabled and not UNIX compatible  - will handle more 
 *				 than MAXOBJS (250) objects 
 */

struct kcontrol *load_sat_metaphase(DDGS *metdg, LPCTSTR filename, int filegroupid, int imageid, int fluorescent)
{
    struct kcontrol *kcont=NULL;
    int i, n;
    int minobjarea;
    unsigned char *frame;
    struct chromosome *bigobj, **objlist;
    FILE *fp;
    struct rawplist rpl;
    Canvas *rawCanvas;
    RawIm *rawim;
    int width=768;
    int height=576;
    int mustfreeframe=0;
	int local_maxobjs;


	/* check if this is NEW satcap2 TIFF raw image */

    if (get_tiff_rawplist(filename, &rpl)) {
    	/* it is - so just load image */

    	rawCanvas=(Canvas *)loadRawCanvas(filename);

    	if (rawCanvas !=NULL) {
    		/* Satcap brightfield images must be inverted to suit sad 
    		   old Woolz - also done in caseimportCB.c for raw images */

    		if (rpl.rawtype == RAW_SATCAP_BRIGHTFIELD_THRESHOLDED)
    			invertRaw(rawCanvas); /* rawIO.c in libcvadmin */


    		rawim=(RawIm *)rawCanvas->Wobj;

    		/* get image width and height */
    		width=rawim->width;
    		height=rawim->height;

    		/* set frame pointing at image and chuck the canvas */
    		frame=(unsigned char *)rawim->values;
    		rawim->values = NULL;
    		destroy_Canvas_and_Wobj(rawCanvas);

    		/* we must free off this frame later */
    		mustfreeframe=1;
    	}
    	else {
    		wait_warning(Mess(_T("Eopenmet"), IDS_KARY_ERR_OPENMET));
    		return(NULL);
    	}
    }
    else {	/* old satcap1 matrox targa file image */

    	if ((fp=_tfopen(filename, _T("rb")))==NULL) {
    		wait_warning(Mess(_T("Eopenmet"), IDS_KARY_ERR_OPENMET));
    		return(NULL);
    	}

//		frame=(char *)read_matrox_file(fp,fluorescent);

    	/* the above uses grab buffer */
    	mustfreeframe=0;
    	fclose(fp);
    }

    if (frame!=NULL) {
    	
    	bigobj=(struct chromosome *)fsconstruct(frame, height, width, 1, 0);

		// allocate temp list of object pointers 
		objlist=(struct chromosome **)Calloc(DEFAULT_MAXOBJS, sizeof(objlist[0]));

		if (!objlist) 
		{
    		if (mustfreeframe)
    			Free(frame);
    		else
    			ungrab_buffer(GRAB_BUFFER,1);

			return NULL;
		}

		/* Get the object list - but make sure we
		 * ignore small ones, and don't get too many. */
		n=0;
    	minobjarea = 9;
		local_maxobjs = DEFAULT_MAXOBJS;

    	while (minobjarea <= 45)
    	{
    		label_area((struct object *)bigobj, &n, (struct object **)objlist, local_maxobjs - 50, minobjarea);

    		if (n < local_maxobjs - 50)
				break;	// we have our object list - lets get out of here
    		
			// If there are too many objects, free them and try to allocate more
    		for (i = 0; i < n; i++)
    		{
    			freeobj((struct object *)objlist[i]);
    			objlist[i]=NULL;
    		}

			// If we are UNIX compatible (limited to 250 objects) we must 
			// retry with increased object size
			// Otherwise we can try to allocate more objects

			if (UNIXCompatible)
				minobjarea += 9;
			else
 			{
				// reallocate objlist
				Free(objlist);

				local_maxobjs += 250;
				objlist=(struct chromosome **)Calloc(local_maxobjs, sizeof(objlist[0]));

				// if this fails we must give up
				if (!objlist) 
				{
    				if (mustfreeframe)
    					Free(frame);
    				else
    					ungrab_buffer(GRAB_BUFFER,1);

					return NULL;
				}
			}
		} // while


    	objlist[n]=NULL;

		/* If the object count is OK... */
    	if (n < local_maxobjs - 50)
    		kcont = create_metaphase(objlist, fluorescent, metdg, filegroupid, imageid);

    	for (i=0;i<n;i++)
    		if (objlist[i])
    			freeobj((struct object *)objlist[i]);

		Free(objlist);

		freeobj((struct object *)bigobj);
   	
    	if (mustfreeframe)
    		Free(frame);
    	else
    		ungrab_buffer(GRAB_BUFFER,1);
    }

	/* kcont will be NULL if we were unable to get
	 * a suitable object count. */
    return(kcont);
}


/*
 *	CREATE_METAPHASE
 *
 *	Creates a new kcontrol structure and fills it with the supplied
 *	object list. This is usually called after capturing an image
 *	and thresholding it i.e. via db_capture() in db_admin.c
 *
 *	Does not display canvas - this is responsiblity of kcont_admin()
 */
struct kcontrol *create_metaphase(struct chromosome **objlist, short fluorescent, DDGS *metdg, int filegroupid, int imageid)
{
    struct kcontrol *kcont;
    struct chromosome *obj;
    int onum;

    show_message(Mess(_T("creatingmet"), IDS_ADM_CREATEMET));

    /* initialise kcontrol structure */
    kcont=init_kcontrol(metdg,NULL);

    if (kcont==NULL)
    	return(NULL);

	if (objlist == NULL)
		return NULL;

	
	/* set filegroup and image ids */
	kcont->filegroupid=filegroupid;
	kcont->met_imageid=imageid;

	/* check for brightfield or fluorescent image */
	if (fluorescent!=0) {
		kcont->FIP=DIGTV | DIGFLUOR;
		kcont->drawmode=INVERT;
	}
	else {
		kcont->FIP=DIGTV;
		kcont->drawmode=COPY;
	}

	// Check the current allocated object lists are large enough to 
	// contain the supplied object list
	onum=0;
	while (objlist[onum]!=NULL)
		onum++;

	if (onum > kcont->MaxObjs)
	{
		// we can try to allocate more objects - this may fail
		//if we are not UNIX compatible (limited to 250 objects)
		increase_kcontrol_MaxObjs(kcont, (onum - kcont->MaxObjs) + 50);
	}

	/* duplicate supplied object list into kcont structure */
	onum=0;
	while ((objlist[onum] != NULL) && (kcont->maxnumber < kcont->MaxObjs)) {

		obj=(struct chromosome *)duplicate_obj((struct object *)objlist[onum]);

		if (obj->type == 1) {
			obj->plist = (struct chromplist *)makechromplist(); /* plist zeroed */
			obj->plist->number = kcont->maxnumber+1;

			/* convert object vdom if necessary */
			if (obj->vdom->type == 21) {
				vdom21to1((struct object *)obj);
			}
			kcont->ool[kcont->maxnumber] = obj;
			kcont->acl[kcont->maxnumber] = ACTIVE;
			kcont->bndool[kcont->maxnumber]=ibound((struct object *)obj,1);
			kcont->maxnumber++;
		}
		else
		{
			freeobj((struct object *)obj);
			OutputDebugString(_T("METCREATE NULL OBJECT\r\n"));
			obj = NULL;
		}

		onum++;
	}

	/* duplicate ool in eool - enhanced original object list */
	for (onum=0; onum<kcont->maxnumber; onum++)		
		kcont->eool[onum] = (struct chromosome *)duplicate_obj((struct object *)kcont->ool[onum]);


	/* create initial metaphase canvas  - create individual object
		   canvases and fill met_chromptr quick canvas access array */
	create_metaphase_canvas(kcont);

	///* do a quick guess at probable object types before displaying */
	//for (onum=0; onum<kcont->maxnumber; onum++)
	//	probotype(kcont->ool[onum]);



	/* add object type highlights to metaphase canvas 
	   i.e. hulls around composites, boxes around noise */
	//set_obtype_canvases(kcont);

	///* show/hide object highlights based on current global flags */
	set_met_highlights_visibility(	kcont,
					show_object_highlights,
					show_centromeres,
					show_axes,
					show_boundaries,
					show_CGHratios,
					show_CGHprobes,
					show_MFISHpseudo,
					show_MFISHprobes,
					show_classlabels,
					show_coloured_boundaries);

	/* determine chromosome width */
	get_chromwidth(kcont);

	/* set modification flag - save me */
	kcont->modified=1;


    show_message(Mess(_T("nowt"), IDS_NOWT));

    return(kcont);
}



/*
 *	LOAD_METAPHASE
 *	
 *	Loads metaphase objects from a metaphase file and
 *	initialises kcontrol structure - which it returns
 *
 *	Returns NULL if image load fails
 *
 *	Does not display canvas - this is responsiblity of kcont_admin()
 */
struct kcontrol *load_metaphase(struct kcontrol *oldkcont, DDGS *metdg, LPCTSTR filename, int filegroupid, int imageid)
{
    FILE *f;
    short canvas_type;
    Canvas *canvas;
    Cblobdata *blobdata;
    int onum, ok;
    struct chromosome *tempobj;
    struct kcontrol *kcont=NULL;
    int magic=0;
    int version=0;
	int tmpMaxObjs, tmpNumOfClasses;
	double xscale, yscale;
	BOOL loadcanv = TRUE;
	BOOL Load = TRUE;
	int cnum = 0;

    int BASIC = 1;

    if ((f = _tfopen(filename, _T("rb"))) == NULL) {
    	wait_warning(Mess(_T("Eopenmet"), IDS_KARY_ERR_OPENMET));
    	ok=0;
    }
    else
    	ok=1;

    if (ok)	show_message(Mess(_T("loadingmet"), IDM_ADM_LOADINGMET));

    if (ok) ok=fread(&magic,sizeof(magic),1,f);
    
    /* check for new image type magic number */
    if (magic == METMAGIC) {

    	/* read file version number */
    	if (ok) ok=fread(&version,sizeof(version),1,f);
    }		
    else {
    	
    	/* old image type so rewind file */
		if (f) // test added MC 6/Feb/99
    		rewind(f);

    	magic=0;
    }
    	
    /* check this software recognises the file version
       - may be using old CV to read new file format */	
    if ((ok) && (version > METVERSION)) {
   		wait_warning(Mess(_T("Enewmetversion"), IDS_KARY_ERR_FUTUREMET));
    	ok=0;
    }
    	
    if (ok) ok=fread(&canvas_type,sizeof(canvas_type),1,f);

    /* check this is a metaphase file */
    if ((ok) && (canvas_type != Cmetaphase)) {
    	wait_warning(Mess(_T("Enotmetfile"), IDS_KARY_ERR_NOTMETFILE));
    	ok=0;
    }


    /* initialise kcontrol structure unless already
       done by loading associated karyogram */
    if (ok) {
    	if (oldkcont == NULL) {
    		kcont=init_kcontrol(metdg,NULL);
    		ok=(kcont != NULL);
    	}
    	else
    		kcont=oldkcont;

    	if (ok) {
    		/* assign file and image ID's */
    		kcont->filegroupid=filegroupid;
    		kcont->met_imageid=imageid;
    	}
    }

    /* load kcontrol info */
    if (ok) ok=fread(&kcont->FIP,sizeof(kcont->FIP),1,f);
    if (ok) ok=fread(&kcont->maxnumber,sizeof(kcont->maxnumber),1,f);
    if (ok) ok=fread(&kcont->measnumber,sizeof(kcont->measnumber),1,f);
    if (ok) ok=fread(&kcont->classifier,sizeof(kcont->classifier),1,f);
    if (ok) ok=fread(&kcont->drawmode,sizeof(kcont->drawmode),1,f);
    if (ok) ok=fread(&kcont->colourised,sizeof(kcont->colourised),1,f);
    if (ok) ok=fread(&kcont->manual_count,sizeof(kcont->manual_count),1,f);
    if (ok) ok=fread(&kcont->centalign,sizeof(kcont->centalign),1,f);

    /* new metaphase files keep chromwidth and cgh info */
    if (version>0) {
    	if (ok) ok=fread(&kcont->chromwidth,sizeof(kcont->chromwidth),1,f);
    	if (ok) ok=fread(&kcont->assoc_type,sizeof(kcont->assoc_type),1,f);

    	/* if assoc_type set check the classifier - old files used to
    	   have this set to RBAND_CLASS in demo version - if so then
    	   change it to CGHDAPI_CLASS - you would never use RBAND on CGH */
    	if ((ok)
    	&& (kcont->assoc_type)
    	&& (kcont->classifier == RBAND_CLASS)) {
    		kcont->classifier=CGHDAPI_CLASS;
    	}

    }

	// version 2 onwards -  metaphases contain additional features for flexible karyotyping
	if (version>1) {
		// read in the MaxObjs value for this cell
		if (ok) ok=fread(&tmpMaxObjs, sizeof(kcont->MaxObjs),1,f);

		if (ok) {
			if (tmpMaxObjs != kcont->MaxObjs)
			{
				// We must reallocate the kcontrol object list space to fit this cell
				// If it succeeds it will automatically set the new kcont->MaxObjs value
 				ok = increase_kcontrol_MaxObjs(kcont, tmpMaxObjs -  kcont->MaxObjs);

				if (!ok) {
					// This may fail if we are UNIX compatible i.e. limited to 250 objects
					// in which case we cannot load this image - let the user know
   					wait_warning(IDS_KARY_ERR_NOTUNIXCOMPATIBLE);

					// We haven't loaded any objects at this point
					// so reset kcont->maxnumber (which may exceed 250)
					kcont->maxnumber=0;
				}
			}
			
		}
		
		// Read in species number of classes
		if (ok) ok=fread(&tmpNumOfClasses, sizeof(kcont->NumOfClasses),1,f);

		if (ok) {
			if (tmpNumOfClasses != kcont->NumOfClasses)
			{
				ok = set_kcontrol_NumOfClasses(kcont, tmpNumOfClasses);

				if (!ok)
				// This may fail if we are not flexkar compliant
				// and the cell is non-human
    			wait_warning(IDS_KARY_ERR_NOTFLEXKAR);
			}
		}
	}


	
    /* read manual count crosses */
    if (ok)
    for (onum=0;onum<kcont->manual_count;onum++) {
    	if (ok)
    		ok=fread(&kcont->count_cross[onum].x,
    			sizeof(kcont->count_cross[onum].x),1,f);
    	else
    		break;
    	if (ok)
    		ok=fread(&kcont->count_cross[onum].y,
    			sizeof(kcont->count_cross[onum].y),1,f);
    	else
    		break;
    }
    
    /* read activity list - version 2 not based on MAXOBJS */
    if (ok) ok=fread(kcont->acl,sizeof(kcont->acl[0])*kcont->MaxObjs, 1,f);
    
    /* read in object lists */
    if (ok)
    for (onum=0;onum<kcont->maxnumber;onum++) {

    	kcont->ool[onum]=(struct chromosome *)readobj(f);
    	if (kcont->ool[onum]==NULL) {
    		ok=0;
    		break;
    	}


    	kcont->eool[onum]=(struct chromosome *)readobj(f);
    	if (kcont->eool[onum]==NULL) {
    		ok=0;
    		break;
    	}
    	kcont->bndool[onum]=(struct object *)readobj(f);
    	if (kcont->bndool[onum]==NULL) {
    		ok=0;
    		break;
    	}

		// resize plist if its an old version
		if(version < METVERSION)
		{
			kcont->ool[onum]->plist = (chromplist*)realloc(kcont->ool[onum]->plist, sizeof(struct chromplist));
			kcont->ool[onum]->plist->size = sizeof(struct chromplist);
			kcont->ool[onum]->plist->pixel_X_micronsize = 0;
			kcont->ool[onum]->plist->pixel_Y_micronsize = 0;
		}

    	/* If any measurements have been made and its an old (pre 1.7)
    	 * format file, remeasure so that nbands & nbindex have values.
    	 */
    	if ((kcont->measnumber>0)
    	&& (kcont->ool[onum]->plist->otype <= CHROMOSOME) 
    	&& (kcont->ool[onum]->plist->nbands==0) 
    	&& (kcont->ool[onum]->plist->nbindex==0)) {

    		/* Force centromere plist->cx, cy to be remeasured */
    		kcont->ool[onum]->plist->Ccx=0;
    		kcont->ool[onum]->plist->Ccy=0;

			if (kcont->FIP & DIGFIP) {
				yscale = 0.75;
				xscale = 1.0;
			}
			else {
				yscale = 1.0;
				xscale = 1.0;
			}

    		tempobj = chromfeatures(kcont->ool[onum],BASIC, xscale, yscale);

    		/* set corrected centromere to calculated value */
    		kcont->ool[onum]->plist->Ccx = kcont->ool[onum]->plist->cx;
    		kcont->ool[onum]->plist->Ccy = kcont->ool[onum]->plist->cy;




    		tempobj->plist=NULL;
    		freeobj((struct object *)tempobj);

    	}

    }



    /* load metaphase canvas */
    if (ok) {
    	kcont->met_canvas=load_Canvas(f);
    	ok = (kcont->met_canvas != NULL);
    }

    if (f)
    	fclose(f);

    if (ok) {

/* BP - make sure all graph canvases are
 * OK - old-style graphs had bad frames. */
    	repair_graph_canvas_frames(kcont->met_canvas);

    	/* place objects in metaphase canvas :
    
    	   scan thru metaphase canvas for Cmetblob canvases
    	   get onum from canvas->data field and attach Wobj
    	   from eool, then set up short cut ptr to canvas */
    	   
    	canvas=kcont->met_canvas->patch;
    	while (canvas!=NULL) {
    		if (canvas->type == Cmetblob) {
    			if ((blobdata=(Cblobdata *)canvas->data) != NULL)
				{
					Canvas *METannocanvas;

					if ( (version < 2) || ((METannocanvas = GetMetannocanvas(canvas)) == NULL) )
						blobdata->label = NOLABEL;

    				/* attach correct Wobj */
    				canvas->Wobj=(struct object *)kcont->eool[blobdata->onum];

    				/* set up short cut list */
    				kcont->met_chromptr[blobdata->onum]=canvas;
				}
    		}
    		else {
    			if (canvas->type == Cflex) {
    				/* the annotation canvas */
    				kcont->metanno_canvas=canvas;
    			}
    		}
    
    		canvas=canvas->next;
    	}

    	/* if annotation canvas is NULL create one */
    	if (kcont->metanno_canvas==NULL) {
    		/* create flex canvas for annotation */
			kcont->metanno_canvas=create_main_Canvas(Cflex,0,0,kcont->met_canvas->frame.width, kcont->met_canvas->frame.height);

    		/* and paste to end of met_canvas */
    		attach_Canvas(kcont->metanno_canvas, kcont->met_canvas);
    	}

    	
    		
    	/* make sure ool,eool,mol and emol share the same plist */
    	for (onum=0; onum<kcont->maxnumber; onum++) {
    		freeplist((struct propertylist *)kcont->eool[onum]->plist);

    		kcont->eool[onum]->plist=kcont->ool[onum]->plist;
    		if (kcont->kar_canvas != NULL) {




				// CV3.6 MG - if we loaded the kary first we may have changed some plist data
				// check if we have updated trimmed, reflected straightened objects in kary
				// if we have we need to remember a few things
				if (kcont->mol[onum]->plist->history[4] == 2)
				{
					kcont->ool[onum]->plist->history[4] = 2;
					kcont->ool[onum]->plist->Cangle = kcont->mol[onum]->plist->Cangle;
					kcont->ool[onum]->plist->fixedptscale = kcont->mol[onum]->plist->fixedptscale;
				}



    			freeplist((struct propertylist *)kcont->mol[onum]->plist);
    			kcont->mol[onum]->plist=kcont->ool[onum]->plist;
    			kcont->emol[onum]->plist=kcont->ool[onum]->plist;

    			/* associate objects */
    			kcont->emol[onum]->assoc=(struct object *)kcont->eool[onum];

    		}				
    	}
    


    	/* add object type highlights to metaphase canvas 
    	   i.e. hulls around composites, boxes around noise */
    	set_obtype_canvases(kcont);





    	/* add object boundary highlights - replaces if they already exist */
 		for (onum=0;onum<kcont->maxnumber;onum++) 
		{
			add_boundary_highlight(kcont,onum);
			//colorize_met_boundaries(kcont);
		}

		//If selcted Border and Color options then colorize it
		if(show_coloured_boundaries && show_boundaries)
			colorize_met_boundaries(kcont, -1);


	if(version < METVERSION)
		get_chromwidth(kcont);



    	/* reorder any highlights attached to chromosome canvases
    	   to make sure they are displayed in the correct order */
    	for (onum=0; onum<kcont->maxnumber; onum++)
		{
    		reorder_highlights(kcont->met_chromptr[onum]);

			// check if karyotype canvas already loaded
    		if (kcont->kar_canvas != NULL) 
			{
				// make sure that plist->keep_parent_site is set up same as canvas - must do this in here as well as load_karyogram()
				// becuase we can load met and kar in either order
				kcont->emol[onum]->plist->keep_parent_site = (kcont->kar_chromptr[onum] != NULL)  ? kcont->kar_chromptr[onum]->frame.parent_site : 0;
			}
		}

    	/* if old style metaphase determine chromosome width */
    	if (version==0) {
    		get_chromwidth(kcont);
    	}
    }
    else {
    	wait_warning(Mess(_T("Ereadmet"), IDS_KARY_ERR_READMET));

    	/* destroy kcontrol structure if first image of met
    	   - kar pair and image load fails */

    	if ((kcont!=NULL) && (kcont!=oldkcont))
    		destroy_kcontrol(kcont);

    	kcont=NULL;
    }
    		


    show_message(Mess(_T("nowt"), IDS_NOWT));


    return(kcont);
}



/*
 *	SAVE_METAPHASE
 *	
 *	saves metaphase objects and canvas to file 
 *	and clears modified flag
 *
 */
int save_metaphase(struct kcontrol *kcont, LPCTSTR filename)
{

    FILE *f;
    short canvas_type;
    Canvas *canvas;
    int onum, ok;
    struct object **highlight = NULL;
    SMALL savemeas;
    int magic=METMAGIC;
    int version=METVERSION;	

	if (!kcont)
		return(0);

    if ((f = _tfopen(filename, _T("wb"))) == NULL) {
    	wait_warning(Mess(_T("Eopenmet"), IDS_KARY_ERR_OPENMET));
    	ok=0;
    }
    else
    	ok=1;

    if (ok)	show_message(Mess(_T("savingmet"), IDS_ADM_SAVINGMET));


    /* clear keeplists & kill zombies */
    free_keeplists(kcont);

	// Allocate space for temp pointers to highlights
	if (ok) {
		highlight = (struct object **)Calloc(kcont->maxnumber, sizeof(highlight[0]));
		ok = (highlight != NULL);
	}
	

    /* force met to be remeasured if saved without the
       karyogram - otherwise we will have missing mol
       and emol objects when we resume measuring */
    if (kcont->kar_canvas == NULL)
    	savemeas=0;
    else
    	savemeas=kcont->measnumber;

    /* save magic number */
    if (ok) ok=fwrite(&magic,sizeof(magic),1,f);

    /* save version number */
    if (ok) ok=fwrite(&version,sizeof(version),1,f);
    
    /* save file type */
    canvas_type=Cmetaphase;
    if (ok) ok=fwrite(&canvas_type,sizeof(canvas_type),1,f);

    /* save kcontrol info */
    if (ok) ok=fwrite(&kcont->FIP,sizeof(kcont->FIP),1,f);
    if (ok) ok=fwrite(&kcont->maxnumber,sizeof(kcont->maxnumber),1,f);
    if (ok) ok=fwrite(&savemeas,sizeof(kcont->measnumber),1,f);
    if (ok) ok=fwrite(&kcont->classifier,sizeof(kcont->classifier),1,f);
    if (ok) ok=fwrite(&kcont->drawmode,sizeof(kcont->drawmode),1,f);
    if (ok) ok=fwrite(&kcont->colourised,sizeof(kcont->colourised),1,f);
    if (ok) ok=fwrite(&kcont->manual_count,sizeof(kcont->manual_count),1,f);
    if (ok) ok=fwrite(&kcont->centalign,sizeof(kcont->centalign),1,f);
    if (ok) ok=fwrite(&kcont->chromwidth,sizeof(kcont->chromwidth),1,f);
    if (ok) ok=fwrite(&kcont->assoc_type,sizeof(kcont->assoc_type),1,f);

	// New to version 2 - fields to support flexible karyotyping
	if (version > 1) 
	{
		if (ok) ok=fwrite(&kcont->MaxObjs,sizeof(kcont->MaxObjs),1,f);
		if (ok) ok=fwrite(&kcont->NumOfClasses,sizeof(kcont->NumOfClasses),1,f);
	}

    /* write manual count crosses */
    if (ok)
    for (onum=0;onum<kcont->manual_count;onum++) {
    	if (ok)
    		ok=fwrite(&kcont->count_cross[onum].x,
    			sizeof(kcont->count_cross[onum].x),1,f);
    	else
    		break;
    	if (ok)
    		ok=fwrite(&kcont->count_cross[onum].y,
    			sizeof(kcont->count_cross[onum].y),1,f);
    	else
    		break;
    }

    /* save activity list - from version 2 onwards no longer base on MAXOBJS */
    if (ok)	ok=fwrite(kcont->acl,sizeof(kcont->acl[0])*kcont->MaxObjs, 1,f);
    
    /* write out object lists */
    if (ok)
    for (onum=0;onum<kcont->maxnumber;onum++) {
    	if (writeobj(f,(struct object *)kcont->ool[onum] ) != 0) {
    		ok=0;
    		break;
    	}
    	if (writeobj(f, (struct object *)kcont->eool[onum] ) != 0) {
    		ok=0;
    		break;
    	}

    	if (writeobj(f, (struct object *)kcont->bndool[onum] ) != 0) {
    		ok=0;
    		break;
    	}

    }


    /* save metaphase canvas - which may contain ideograms 
       and annotation as well as the metaphase chromosomes */
    if (ok) {

    	/* strip off chromosomes and their object highlights from the
    	   canvas before saving the canvas  - these are held in the 
    	   object lists already saved */

    	for (onum=0;onum<kcont->maxnumber;onum++) {
    		canvas=kcont->met_chromptr[onum];
    		if (canvas != NULL) {
    			/* remove metblob */
    			canvas->Wobj=NULL;

    			/* and its highlight - coz this may be bndool[] 
    			   but dont touch CGH ratio highlights or Axes
    			   or Centromeres */

    			canvas=canvas->patch;
    			while (canvas != NULL) {
    				if (canvas->type == Cgraphic) {
    					highlight[onum]=canvas->Wobj;
    					canvas->Wobj=NULL;
    					break;
    				
    				}
    				else
    					canvas=canvas->next;
    			}
    		}
    	}

    	if (save_Canvas(f, kcont->met_canvas) != 0)
    		ok = 0;

    	/* restore objects and their highlights to
    	   allow continued processing of the canvas */	
    		
    	for (onum=0;onum<kcont->maxnumber;onum++) {
    		canvas=kcont->met_chromptr[onum];
    		if (canvas != NULL) {
    			/* restore metblob */
    			canvas->Wobj=(struct object *)kcont->eool[onum];

    			/* and its highlight */
    			canvas=canvas->patch;
    			while (canvas != NULL) {
    				if (canvas->type == Cgraphic) {
    					canvas->Wobj=highlight[onum];
    					break;					
    				}
    				else
    					canvas=canvas->next;
    			}
    		}
    	}
    }

    if (f)
    	fclose(f);

	if (highlight)
		Free(highlight);

    if (ok) {
    	/* reset modification flag */
    	kcont->modified=0;
		save_tries = 0;
		}
    else {
		if (save_tries < 3){
			save_tries++;
			Sleep (5000);
			ok = save_metaphase(kcont,filename); //try again
			}
		else {
			save_tries = 0;
    		wait_warning(Mess(_T("Esavemet"), IDS_KARY_ERR_SAVEMET));
			}
		}

    show_message(Mess(_T("nowt"), IDS_NOWT));

	return(ok);

}

#endif

/*
 *	LOAD_KARYOGRAM
 *	
 *	Loads karyogram objects and canvas from a karyogram file
 *	and initialises kcontrol structure - which it returns.
 *
 *	Returns NULL if image load fails.
 *
 *	Does not display canvas - this is responsiblity of kcont_admin().
 */
struct kcontrol *load_karyogram(struct kcontrol *oldkcont, DDGS *kardg, LPCTSTR filename, int filegroupid, int imageid)
{
    FILE *f;
    short canvas_type;
    Canvas *classcanvas, *chromcanvas;
    Cclassdata *classdata;
    Cchromdata *chromdata;
    int onum, chromclass, ok;
    struct kcontrol *kcont=NULL;
    int magic=0;
    int version=0;
	int tmpMaxObjs, tmpNumOfClasses;
	BOOL kary_err = FALSE;

    if ((f = _tfopen(filename, _T("rb"))) == NULL) {
    	ok=0;
    }
    else
    	ok=1;

    if (ok) ok=fread(&magic,sizeof(magic),1,f);
    
    /* check for new image type magic number */
    if (magic == KARMAGIC) {

    	/* read file version number */
    	if (ok) ok=fread(&version,sizeof(version),1,f);
    }		
    else {
    	
    	/* old image type so rewind file */
		if (f) // test added MC 6/Feb/99
    		rewind(f);

    	magic=0;
    }
    	
    /* check this software recognises the file version
       - may be using old CV to read new file format */	
    if ((ok) && (version > KARVERSION)) {
    	ok=0;
    }
    	
    if (ok) ok=fread(&canvas_type,sizeof(canvas_type),1,f);

    /* check this is a karyogram file */
    if ((ok) && (canvas_type != Ckaryogram)) {
    	ok=0;
    }

    if (ok) {
    	/* initialise kcontrol structure unless already
    	 done by loading associated metaphase */
    	if (oldkcont == NULL) {
    		kcont=init_kcontrol(NULL,kardg);
    		ok = (kcont != NULL);
    	}
    	else 
    		kcont=oldkcont;
    	
    	if (ok) {
    		/* deactivate measurement timer in case metaphase loaded
    		first - does nothing if karyogram loaded first */
    		// DeactivateMeastimeCallback(kcont);

    		/* assign file and image ID's */
    		kcont->filegroupid=filegroupid;
    		kcont->met_imageid=imageid;
    	}
    }

	if(kcont == NULL)
		return(kcont);

    /* load kcontrol info */
    if (ok) ok=fread(&kcont->FIP,sizeof(kcont->FIP),1,f);
    if (ok) ok=fread(&kcont->maxnumber,sizeof(kcont->maxnumber),1,f);
    if (ok) ok=fread(&kcont->measnumber,sizeof(kcont->measnumber),1,f);
    if (ok) ok=fread(&kcont->classifier,sizeof(kcont->classifier),1,f);
    if (ok) ok=fread(&kcont->drawmode,sizeof(kcont->drawmode),1,f);
    if (ok) ok=fread(&kcont->colourised,sizeof(kcont->colourised),1,f);
    if (ok) ok=fread(&kcont->manual_count,sizeof(kcont->manual_count),1,f);
    if (ok) ok=fread(&kcont->centalign,sizeof(kcont->centalign),1,f);

    /* new karyotype files keep chromwidth and cgh info */
    if (version>0) {
    	if (ok) ok=fread(&kcont->chromwidth,sizeof(kcont->chromwidth),1,f);
    	if (ok) ok=fread(&kcont->assoc_type,sizeof(kcont->assoc_type),1,f);

    	/* if assoc_type set check the classifier - old files used to
    	   have this set to RBAND_CLASS in demo version - if so then
    	   change it to CGHDAPI_CLASS - you would never use RBAND on CGH */
    	if ((ok)
    	&& (kcont->assoc_type)
    	&& (kcont->classifier == RBAND_CLASS)) {
    		kcont->classifier=CGHDAPI_CLASS;
    	}
    }

	// version 2 onwards -  karyotypes contain additional features for flexible karyotyping
	if (version>1) {
		// read in the MaxObjs value for this cell
		if (ok) ok=fread(&tmpMaxObjs,sizeof(kcont->MaxObjs),1,f);

		if (ok) {
			if (tmpMaxObjs != kcont->MaxObjs)
			{
				// We must reallocate the kcontrol object list space to fit this cell
				// If it succeeds it will automatically set the new kcont->MaxObjs value
 				ok = increase_kcontrol_MaxObjs(kcont, tmpMaxObjs -  kcont->MaxObjs);

				if (!ok) {
					// This may fail if we are UNIX compatible i.e. limited to 250 objects
					// in which case we cannot load this image - let the user know

					// We haven't loaded any objects at this point
					// so reset kcont->maxnumber (which may exceed 250)
					kcont->maxnumber=0;
				}
			}
			
		}
		
		// Read in species number of classes
		if (ok) ok=fread(&tmpNumOfClasses,sizeof(kcont->NumOfClasses),1,f);

		if (tmpNumOfClasses != kcont->NumOfClasses)
		{
			ok = set_kcontrol_NumOfClasses(kcont, tmpNumOfClasses);
		}
	}

    /* read manual count crosses */
    if (ok)
    for (onum=0;onum<kcont->manual_count;onum++) {
    	if (ok)
    		ok=fread(&kcont->count_cross[onum].x,
    			sizeof(kcont->count_cross[onum].x),1,f);
    	else
    		break;
    	if (ok)
    		ok=fread(&kcont->count_cross[onum].y,
    			sizeof(kcont->count_cross[onum].y),1,f);
    	else
    		break;
    }
    
    /* read activity list - version 2 not based on MAXOBJS */
    if (ok) ok=fread(kcont->acl,sizeof(kcont->acl[0])*kcont->MaxObjs, 1,f);

    /* read in object lists */
    if (ok){
		for (onum=0;onum<kcont->maxnumber;onum++) {
    		kcont->mol[onum]=(struct chromosome *)readobj(f);
    		if (kcont->mol[onum]==NULL) {
    			ok=0;
    			break;
    		}
    		kcont->emol[onum]=(struct chromosome *)readobj(f);
    		if (kcont->emol[onum]==NULL) {
    			ok=0;
    			break;
    		}
    		kcont->bndmol[onum]=(struct object *)readobj(f);
    		if (kcont->bndmol[onum]==NULL) {
					//boundary may be null (due to a bug somewhere!),
					//so identify the object so the user may take action.  
				kcont->selected[onum]= 1;	//select the problem object
				kary_err = TRUE;
    		}
		}
	}	

    /* load karyogram canvas */
    if (ok) {
    	kcont->kar_canvas=load_Canvas(f);
    	ok = (kcont->kar_canvas != NULL);
    }

    if (f)
    	fclose(f);

    if (ok) {

/* BP - make sure all graph canvases are
 * OK - old-style graphs had bad frames. */
    	//repair_graph_canvas_frames(kcont->kar_canvas);


    	/* place objects in karyogram canvas :

    	   scan thru kar_canvas for Cchromclass canvases get
    	   class from canvas->data field and set up short cut
    	   ptr to class canvas then ...
    
    	   scan thru Cchromclass canvas for Cchromosome canvases
    	   get onum from canvas->data field and attach Wobj
    	   from emol, then set up short cut ptr to canvas */
    	   
    	classcanvas=kcont->kar_canvas->patch;

    	while (classcanvas!=NULL) {
    		if ( (classcanvas->type == Cchromclass)
    		|| (classcanvas->type == Cstack ) ) {

    			if (classcanvas->type == Cstack)
    				chromclass=kcont->NumOfClasses;
    			else {
    				classdata=(Cclassdata *)classcanvas->data;
    				chromclass=classdata->chromclass-1;
    			}
    			
    			/* set up short cut list */
    			kcont->classptr[chromclass]=classcanvas;

    			chromcanvas=classcanvas->patch;

    			while (chromcanvas!=NULL) {
    				if (chromcanvas->type == Cchromosome) {
    					chromdata=(Cchromdata *)chromcanvas->data;

    					/* attach correct Wobj */
    					chromcanvas->Wobj=(struct object *)kcont->emol[chromdata->onum];

    					/* set up short cut list */
    					kcont->kar_chromptr[chromdata->onum]=chromcanvas;
    				}
    
    				chromcanvas=chromcanvas->next;
    			}
    		}
    		else {
    			if (classcanvas->type == Cflex) {
    				/* the annotation canvas */
    				kcont->karanno_canvas=classcanvas;
    			}
    		}

    		classcanvas=classcanvas->next;
    	}
    		
    	/* if annotation canvas is NULL create one */
    	if (kcont->karanno_canvas==NULL) {
    		/* create flex canvas for annotation */
			kcont->karanno_canvas=create_main_Canvas(Cflex,0,0,kcont->kar_canvas->frame.width, kcont->kar_canvas->frame.height);

    		/* and paste to end of kar_canvas */
    		attach_Canvas(kcont->karanno_canvas, kcont->kar_canvas);
    	}
		

    	/* make sure ool,eool,mol and emol share the same plist */
    	for (onum=0; onum<kcont->maxnumber; onum++) {
    		freeplist((struct propertylist *)kcont->emol[onum]->plist);
    		kcont->emol[onum]->plist=kcont->mol[onum]->plist;
    		if (kcont->met_canvas != NULL) {
    			freeplist((struct propertylist *)kcont->mol[onum]->plist);
    			kcont->mol[onum]->plist=kcont->ool[onum]->plist;
    			kcont->emol[onum]->plist=kcont->ool[onum]->plist;

    			/* associate objects */
    			kcont->emol[onum]->assoc=(struct object *)kcont->eool[onum];
    		}
    	}
    	
    	/* reorder any highlights attached to chromosome canvases
    	   to make sure they are displayed in the correct order */
    	for (onum=0; onum<kcont->maxnumber; onum++)
		{
    		reorder_highlights(kcont->kar_chromptr[onum]);
			// also make sure that plist->keep_parent_site is set up same as canvas
			kcont->emol[onum]->plist->keep_parent_site = (kcont->kar_chromptr[onum] != NULL)  ? kcont->kar_chromptr[onum]->frame.parent_site : 0;
		}

		// CV3.6 onwards - check for reflected, trimmed or straightened objects
		// for these objects we now keep a copy of the emol object in mol and a
		// copy of the scale it was created at in fixedptscale in plist
		// So we know this history[4] has been incremented from 1 to 2
		// Here we deal with old karyotypes with history[4] = 1
    	for (onum=0; onum<kcont->maxnumber; onum++)
		{
			if (kcont->mol[onum]->plist->history[4] == 1)
			{
				kcont->mol[onum]->plist = NULL;
				freeobj((struct object *)kcont->mol[onum]);
				kcont->mol[onum ]= (struct chromosome *)duplicate_obj((struct object *)kcont->emol[onum]);
				kcont->mol[onum]->plist = kcont->emol[onum]->plist;
				// keep track of current scale in fixedptscale - rotate_scale_chrom() will need this
				// note fixedptscale is fixed point, max scale is 2.0 and min scale is 0.25
				// why fixed point? - we only had an int free to play with in plist
				kcont->mol[onum]->plist->fixedptscale =  (int)(16384 * (kcont->mol[onum]->plist->fscale - 0.25));
				// reset corrected angle 
				kcont->mol[onum]->plist->Cangle=0;
				// so we don't need to do this again...
				kcont->mol[onum]->plist->history[4] = 2;
			}
		}

		// Base font size on size of karcanvas
		//adjustClassFontSize(kcont);
    }
    else {

    	/* destroy kcontrol structure if first image of met
    	   - kar pair and image load fails */

    	if ((kcont!=NULL) && (kcont!=oldkcont))
    		destroy_kcontrol(kcont);

    	kcont=NULL;
    }
    return(kcont);
}

#if 0
int verify_kcont (struct kcontrol *kcont)
{
	int ok, onum,kary_err;

	ok = 1;

	DeselectAll (kcont);

	for (onum=0;onum<kcont->maxnumber;onum++) {
  		if (kcont->bndmol[onum] == NULL){
    			//boundary is null (due to a bug somewhere!),
				//so identify the object so the user may take action.  
			kcont->selected[onum]= 1;	//select the problem object
			kary_err = TRUE;
			ok = 0;
    		}
		}

	if (kary_err == TRUE){
		show_selected (kcont);
		wait_warning(Mess(_T("Esavekar"), IDS_KARY_CORRUPT_OBJECT));
		}

	return ok;
}

/*
 *	SAVE_KARYOGRAM
 *	
 *	saves karyogram objects and canvas to file 
 *	and clears modified flag
 *
 */
int save_karyogram(struct kcontrol *kcont, LPCTSTR filename)
{
    FILE *f;
    short canvas_type;
    Canvas *canvas;
    int onum, ok;
    int magic=KARMAGIC;
    int version=KARVERSION;	
	BOOL kary_err = FALSE;

	if (!kcont)
		return(0);

    if ((f = _tfopen(filename, _T("wb"))) == NULL) {
    	wait_warning(Mess(_T("Eopenkar"), IDS_KARY_ERR_OPENKAR));
    	ok=0;
    }
    else
    	ok=1;


    if (ok) show_message(Mess(_T("savingkar"), IDS_ADM_SAVINGKAR));


    /* clear keeplists & kill zombies */
    free_keeplists(kcont);

    /* save magic number */
    if (ok) ok=fwrite(&magic,sizeof(magic),1,f);

    /* save version number */
    if (ok) ok=fwrite(&version,sizeof(version),1,f);
    
    /* save file type */
    canvas_type=Ckaryogram;
    if (ok) ok=fwrite(&canvas_type,sizeof(canvas_type),1,f);

    /* save kcontrol info */
    if (ok) ok=fwrite(&kcont->FIP,sizeof(kcont->FIP),1,f);
    if (ok) ok=fwrite(&kcont->maxnumber,sizeof(kcont->maxnumber),1,f);
    if (ok) ok=fwrite(&kcont->measnumber,sizeof(kcont->measnumber),1,f);
    if (ok) ok=fwrite(&kcont->classifier,sizeof(kcont->classifier),1,f);
    if (ok) ok=fwrite(&kcont->drawmode,sizeof(kcont->drawmode),1,f);
    if (ok) ok=fwrite(&kcont->colourised,sizeof(kcont->colourised),1,f);
    if (ok) ok=fwrite(&kcont->manual_count,sizeof(kcont->manual_count),1,f);
    if (ok) ok=fwrite(&kcont->centalign,sizeof(kcont->centalign),1,f);
    if (ok) ok=fwrite(&kcont->chromwidth,sizeof(kcont->chromwidth),1,f);
    if (ok) ok=fwrite(&kcont->assoc_type,sizeof(kcont->assoc_type),1,f);

	// New to version 2 - fields to support flexible karyotyping
	if (version > 1) 
	{
		if (ok) ok=fwrite(&kcont->MaxObjs,sizeof(kcont->MaxObjs),1,f);
		if (ok) ok=fwrite(&kcont->NumOfClasses,sizeof(kcont->NumOfClasses),1,f);
	}

    /* write manual count crosses */
    for (onum=0;onum<kcont->manual_count;onum++) {
    	if (ok)
    		ok=fwrite(&kcont->count_cross[onum].x,
    			sizeof(kcont->count_cross[onum].x),1,f);
    	else
    		break;
    	if (ok)
    		ok=fwrite(&kcont->count_cross[onum].y,
    			sizeof(kcont->count_cross[onum].y),1,f);
    	else
    		break;
    }

    /* save activity list - from version 2 this is not based on MAXOBJS */
    if (ok)	ok=fwrite(kcont->acl,sizeof(kcont->acl[0])*kcont->MaxObjs, 1,f);
    
    /* write out object lists */
    if (ok) {

		DeselectAll (kcont);

		for (onum=0;onum<kcont->maxnumber;onum++) {
				if (kcont->mol[onum] != NULL) {
    				if (writeobj(f,(struct object *)kcont->mol[onum] ) != 0) {
    					ok=0;
    					break;
    					}
					}
				else {
					fputc(0,f);
						//boundary is null (due to a bug somewhere!),
						//so identify the object so the user may take action.  
					kcont->selected[onum]= 1;	//select the problem object
					kary_err = TRUE;
					}

				if (kcont->emol[onum] != NULL) {
    				if (writeobj(f, (struct object *)kcont->emol[onum] ) != 0) {
    					ok=0;
    					break;
    					}
					}
				else {
					fputc(0,f);
						//boundary is null (due to a bug somewhere!),
						//so identify the object so the user may take action.  
					kcont->selected[onum]= 1;	//select the problem object
					kary_err = TRUE;
					}
				
				if (kcont->bndmol[onum] != NULL){
    				if (writeobj(f, (struct object *)kcont->bndmol[onum] ) != 0) {
    					ok=0;
    					break;
    					}
					}
				else {
					fputc(0,f);
						//boundary is null (due to a bug somewhere!),
						//so identify the object so the user may take action.  
					kcont->selected[onum]= 1;	//select the problem object
					kary_err = TRUE;
    			}
			}

		if (kary_err == TRUE){
			show_selected (kcont);
			wait_warning(Mess(_T("Esavekar"), IDS_KARY_CORRUPT_OBJECT));
			}

		}
 
    /* save karyogram canvas - which may contain ideograms 
       and annotation as well as the karyogram chromosomes */
    if (ok) {

    	/* strip off chromosomes and from the
    	   canvas before saving the canvas  - these
    	   are held in the object lists already saved */

    	for (onum=0;onum<kcont->maxnumber;onum++) {
    		canvas=kcont->kar_chromptr[onum];
    		if (canvas != NULL) {
    			/* remove chromosome */
    			canvas->Wobj=NULL;
    		}
    	}

    	if (save_Canvas(f, kcont->kar_canvas) != 0)
    		ok = 0;

    	/* restore chromosome objects to canvas to
    	   allow continued processing of the canvas */	
    		
    	for (onum=0;onum<kcont->maxnumber;onum++) {
    		canvas=kcont->kar_chromptr[onum];
    		if (canvas != NULL) {
    			/* restore chromosome */
    			canvas->Wobj=(struct object *)kcont->emol[onum];
    		}
    	}
    }

    if (f)
    	fclose(f);

    show_message(Mess(_T("nowt"), IDS_NOWT));

    if (ok) {
    	/* reset modification flag */
    	kcont->modified=0;
		save_tries = 0;
		}
    else {
		if (save_tries < 3){
			save_tries++;
			Sleep (5000);
			ok = save_karyogram(kcont,filename); //try again
			}
		else {
			save_tries = 0;
			wait_warning(Mess(_T("Esavekar"), IDS_KARY_ERR_SAVEKAR));
			}
		}

	return(ok);    
}

#endif

    
