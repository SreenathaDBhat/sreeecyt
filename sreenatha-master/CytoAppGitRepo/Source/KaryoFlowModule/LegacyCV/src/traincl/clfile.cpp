/*****************************************************************************
              Copyright (c) 1999 Applied Imaging International
 
	Source file:    clfile.cpp
 
	Function:       File read/write for classifier Class and Data files.

	Package:        traincl.lib, part of WinCV
 
	Dependencies:   (Any non-portable dependencies)
 
	Usage:          (How to use this file)
 
	Modification history:
	Author      Date        Description
    RH-01		  Apr00	    Added CGH Std Refs support to training files for High Resolution CGH.
	SN          06May99     Initial implementation
	SN			08Oct99		Set the version variable in cl_read_class_header
							and cl_read_data_header.
 -------------------------------------v2.51-----------------------------------
	SN			06Apr00		Stop cl_read_all_cell_headers() being recursive as
							it crashed with a training file of approximately
							160 cells.
 
 ****************************************************************************/

/** STANDARD LIBRARY HEADERS ************************************************/
#include <stdio.h>
#include <time.h>
#include <string.h>


/** OTHER HEADER FILES ******************************************************/
//#include <stdafx.h>			// Needed for CNavigator class
							// somehow included in by barcode.h->pcont.h->navigator.h
//#include <navigator.h>		// Needed for CNavigator reference in casebase.h for .cpp sources.
//#include <casebase.h>
#include <woolz.h>
#include <chromanal.h>		// For DEFAULT_MAXDIM only.
#include "species.h"
#include <trainkarcl.h>		// For CLheader definition
//#include <barcode.h>
//#include <barwin.h>

//#include <species.h>		//SN03Jun99 Needed for clfile.h
#include "clfile.h"


/** MACROS ******************************************************************/
#define CLVARIABLE_MAXLINELENGTH	(CLSPECIESNAMELENGTH + 20)


/** TYPEDEFS, STRUCTS *******************************************************/
typedef enum e_CLclassifier_type {
	CLASSIFIER_UNKOWN, CLASSIFIER_STD, CLASSIFIER_BARCODE, CLASSIFIER_CGH_STDREF		// RH-01
} CLclassifier_type;



/** PRIVATE FUNCTION PROTOTYPES *********************************************/

/** PUBLIC DECLARATIONS (and externs) ***************************************/

/** PRIVATE DECLARATIONS (statics) ******************************************/

const TCHAR szspeciesdefault[CLSPECIESNAMELENGTH] = CLSPECIESDEFAULTSTRING;		// TODO: May need to be moved in time!!!
const TCHAR szSTDCLASSTOKEN[] = _T("Vcv");
const TCHAR szValidClassToken[] = _T("ValidClassMapping");


/** DEBUG only DECLARATIONS and FUNCTIONS ***********************************/


/** IMPLEMENTATION **********************************************************/



//////////////////////////////////////////////////////////////////////////////
//
//	CLASS FILE HANDLING
//
//////////////////////////////////////////////////////////////////////////////

/***************************************************** cl_read_class_header
 
	CREATED:                    SN      06May99

	FUNCTION:
		Reads the header of a classifier Class file.

		STD VERSION 0 HEADER - "Vcv 107 0 0 0 10 24"
		STD VERSION 1 HEADER - "Vcv 107 0 0 0 10 21 1 Mouse"

		BARCODE VERSION 0 HEADER - "Barcode 550"
		BARCODE VERSION 1 HEADER - "Barcode 550 1 21 Mouse"

   		CGH STDREF VERSION 1 HEADER - "CGH 800 1 21 Mouse"					// RH-01


PARAMETERS:
		fp: read: Classifier Class file, must already be open.
		pclassid: write: Classifier id.
		pheader: write: Information in Class file header.
 
	RETURN:
		int. The error status flag.
		0 - error, 1 - ok.
 
	EXTERNS:
		(Definition and usage of external or static variables used)
 
	MODS:
	SN	08Oct99	Set class_version in the header struct passed out for
				standard classifiers.
    RH-01	  Apr00	Added CGH Std Refs support to training files for High Resolution CGH.
	
 ***************************************************************************/
int cl_read_class_header( FILE * fp, int * pclassid, CLheader * pheader)
{
	TCHAR sztype[20] = {0};
	int classid;
	int nval;
	CLclassifier_type cltype;
	int nselected_feature;
	int nclass;
	int class_version;
	TCHAR szspecies[CLSPECIESNAMELENGTH + 1] = {0};
	TCHAR szline[CLVARIABLE_MAXLINELENGTH + 1] = {0};
	int dum1, dum2, dum3;


	// Initialise header elements to be written
	pheader->nclass				= 0;
	pheader->nselected_feature	= 0;
	pheader->class_version		= 0;
	memset( pheader->szspecies, 0, (CLSPECIESNAMELENGTH + 1) * sizeof(TCHAR));


	// While first string is not recognised continue reading
	// This loop is only really necc. for longclass and Rbandclass
	// default human classifier Class files which have loads
	// of text before the "Vcv" keyword.
	cltype = CLASSIFIER_UNKOWN;
   	do
	{
       	nval = _ftscanf( fp, _T("%s"), sztype);
		if (_tcscmp( sztype, szSTDCLASSTOKEN) == 0)	cltype = CLASSIFIER_STD;
   	}
	while( (cltype == CLASSIFIER_UNKOWN) && (nval != EOF) );

	// If classifier unkown then error so return
	if (cltype == CLASSIFIER_UNKOWN)
	{
		fprintf( stderr, "ERROR: cl_read_class_header: Not a Class file.\n");
		return(0);
	}

	// Read classifier id
	nval = _ftscanf( fp, _T(" %d"), &classid);
	if (nval != 1)
	{
		fprintf( stderr, "ERROR: cl_read_class_header: Reading classifier id.\n");
		return(0);
	}

	// Read rest of header
	if (cltype == CLASSIFIER_STD)
	{
		//
		// VERSION 0 header
		//
    	nval = _ftscanf( fp, _T(" %d %d %d %d %d"), &dum1, &dum2, &dum3, &nselected_feature, &nclass);
		if (nval != 5)
		{
			fprintf( stderr, "ERROR: cl_read_class_header: Reading version 0 header.\n");
			return(0);
		}

		// Read rest of line
		if (_fgetts( szline, (CLVARIABLE_MAXLINELENGTH + 1), fp ) == NULL)
		{
			fprintf( stderr, "ERROR: cl_read_class_header: Reading version 0 header.\n");
			return(0);
		}

		// Try reading the version number and species name
		nval = _stscanf( szline, _T(" %d %s"), &class_version, szspecies);
		switch (nval)
		{
			case 2:
				// VERSION 1 or more header
				*pclassid = classid;
				pheader->nclass				= nclass;
				pheader->nselected_feature	= nselected_feature;
				pheader->class_version		= class_version;							//SN08Oct99		
				_tcsncpy( pheader->szspecies, szspecies, CLSPECIESNAMELENGTH);
				break;

			case EOF:
				// VERSION 0 header
				*pclassid = classid;
				pheader->nclass				= nclass;
				pheader->nselected_feature	= nselected_feature;
				pheader->class_version		= 0;
				_tcsncpy( pheader->szspecies, szspeciesdefault, CLSPECIESNAMELENGTH);
				break;

			default:
				fprintf( stderr, "ERROR: cl_read_class_header: Reading version 1 header.\n");
				return(0);
				break;
		}
	}

	return(1);
}


/***************************************************** cl_write_class_header
 
	CREATED:                    SN      07May99

	FUNCTION:
		Writes the header of a classifier Class file.
		This is only written for the standard classifier in version 1 format.

		STD VERSION 1 HEADER - "Vcv 107 0 0 0 10 21 1 Mouse"

	PARAMETERS:
		fp: read: Classifier Class file, must already be open.
		classid: read: Classifier id.
		header: read: Information in Class file header.
 
	RETURN:
		None.
		
	EXTERNS:
		szSTDCLASSTOKEN is a string token identifying the file as a
		standard classifier file.
 
	MODS:
 
 ***************************************************************************/
void cl_write_class_header( FILE * fp, int classid, CLheader header)
{
	_ftprintf( fp, _T("%s %d %d %d %d %d %d %d %s\n"),
		szSTDCLASSTOKEN, classid, 0, 0, 0, header.nselected_feature, header.nclass,		// VERSION 0 header
		header.class_version, header.szspecies);										// VERSION 1 header
}



//////////////////////////////////////////////////////////////////////////////
//
//	DATA FILE HANDLING
//
//////////////////////////////////////////////////////////////////////////////

/***************************************************** cl_read_data_header
 
	CREATED:                    SN      10May99

	FUNCTION:
		Reads the header of a classifier Data file.

		STD & BARCODE VERSION 0 HEADER - "% 107"
		STD & BARCODE VERSION 1 HEADER - "% 107 1 30 21 Mouse"

	PARAMETERS:
		fp: read: Classifier Data file, must already be open.
		pclassid: write: Classifier id.
		pheader: write: Information in Data file header.
 
	RETURN:
		int. The error status flag.
		0 - error, 1 - ok.
 
	EXTERNS:
		(Definition and usage of external or static variables used)
 
	MODS:
	SN	08Oct99	Set data_version in the header struct passed out for
				both standard and barcode classifiers.
 
 ***************************************************************************/
int cl_read_data_header( FILE * fp, int * pclassid, CLheader * pheader)
{
	int nval;
	int nfeature, nclass, classid, data_version;
	int fchar;
	TCHAR szspecies[CLSPECIESNAMELENGTH + 1] = {0};
	TCHAR szline[CLVARIABLE_MAXLINELENGTH + 1] = {0};


	*pclassid = 0;

	// Initialise header elements to be written
	pheader->nclass				= 0;
	pheader->nfeature			= 0;
	pheader->data_version		= 0;
	memset( pheader->szspecies, 0, (CLSPECIESNAMELENGTH + 1) * sizeof(TCHAR));


	// While first char is not '%' continue reading.
	// This loop is only really necc. to ignore 
	// any text before the header.
	do 
	{
		fchar = _fgettc( fp);
	}
	while ((fchar != _T('%')) && (fchar != EOF));

	// If no header found then error so return
	if (fchar == EOF)
	{
		fprintf( stderr, "ERROR: cl_read_data_header: Not a Data file.\n");
		return(0);
	}

	// Read classifier id
	nval = _ftscanf( fp, _T(" %d"), &classid);
	if (nval != 1)
	{
		fprintf( stderr, "ERROR: cl_read_data_header: Reading classifier id.\n");
		return(0);
	}

	// Read rest of header
	if (_fgetts( szline, (CLVARIABLE_MAXLINELENGTH + 1), fp ) == NULL)
	{
		fprintf( stderr, "ERROR: cl_read_data_header: Reading version 0 header.\n");
		return(0);
	}

	//
	// VERSION 1 header
	//
	// Try reading the version number, number of features, number of classes and species name
	nval = _stscanf( szline, _T(" %d %d %d %s"), &data_version, &nfeature, &nclass, szspecies);
	switch (nval)
	{
		case 4:
			// VERSION 1 header
			*pclassid = classid;
			pheader->nclass			= nclass;
			pheader->nfeature		= nfeature;
			pheader->data_version	= data_version;									//SN08Oct99
			_tcsncpy( pheader->szspecies, szspecies, CLSPECIESNAMELENGTH);
			break;

		case EOF:
			// VERSION 0 header specific to humans
			*pclassid = classid;
			pheader->nclass			= DEFAULT_NUMOFCLASSES;
			pheader->nfeature		= DEFAULT_MAXDIM;
			pheader->data_version	= 0;
			_tcsncpy( pheader->szspecies, szspeciesdefault, CLSPECIESNAMELENGTH);
			break;

		default:
			fprintf( stderr, "ERROR: cl_read_data_header: Reading version 1 header.\n");
			return(0);
			break;
	}


	return(1);
}


/***************************************************** cl_write_data_header
 
	CREATED:                    SN      11May99

	FUNCTION:
		Writes the header of a classifier Data file, in version 1
		format only.

		STD & BARCODE VERSION 1 HEADER - "% 107 1 30 21 Mouse"

	PARAMETERS:
		fp:      read: Classifier Data file, must already be open.
		classid: read: Classifier id.
		header:  read: Data file header.
 
	RETURN:
		None.
		
	EXTERNS:
		(Definition and usage of external or static variables used)
 
	MODS:
 
 ***************************************************************************/
void cl_write_data_header( FILE * fp, int classid, CLheader header)
{
	fprintf( fp, "%% %d %d %d %d %s\n",
		classid, header.data_version, header.nfeature, header.nclass, header.szspecies);
}



/***************************************************** cl_copy_data_header
 
	CREATED:                    SN      12May99

	FUNCTION:
		A convenience function copying the header of one Data file
		to another.
		Reads  STD & BARCODE version 0 and version 1 header formats.
		Writes STD & BARCODE version 1 header format.

	PARAMETERS:
		fpsrc: read: Source classifier Data file, must already be open.
		fpdst: read: Destination classifier Data file, must already be open.
 
	RETURN:
		int. The error status flag.
		0 - error, 1 - ok.
		
	EXTERNS:
		(Definition and usage of external or static variables used)
 
	MODS:
 
 ***************************************************************************/
int cl_copy_data_header( FILE * fsrc, FILE * fdst)
{
	CLheader header;
	int classid, nrtn;


	memset( &header, 0, sizeof(header));
	if (cl_read_data_header( fsrc, &classid, &header))
	{
		header.data_version = CLASSVERSION;
		cl_write_data_header( fdst, classid, header);
		nrtn = 1;
	}
	else
	{
		nrtn = 0;
	}

	return( nrtn);
}


/***************************************************** cl_skip_data_header
 
	CREATED:                    SN      12May99

	FUNCTION:
		A convenience function moving the file pointer passed the
		Data file header.
		Reads STD & BARCODE version 0 and version 1 header formats.

	PARAMETERS:
		fp: read: Classifier Data file, must already be open.
 
	RETURN:
		int. The error status flag.
		0 - error, 1 - ok.
 
	EXTERNS:
		(Definition and usage of external or static variables used)
 
	MODS:
 
 ***************************************************************************/
int cl_skip_data_header( FILE * fp)
{
	CLheader header;
	int classid;

	return( cl_read_data_header( fp, &classid, &header));
}


/*************************************************** cl_read_all_cell_headers
 
	CREATED:                    SN      ??Jan99

	FUNCTION:
		Read cell headers from Classifier Data file (training file)
		into cell linked list. The cell header consists of

		"% casename slidename cellname dd/mm/yy count"

		Moved from trainclassCB.c wheer it was called get_file_data.

	PARAMETERS:
		(Definition of parameters, types, and usage i.e. read, write, or update)
 
	RETURN:
		None.
 
	EXTERNS:
		(Definition and usage of external or static variables used)
 
	MODS:
	SN    28Aug98	Added support for barcode data files which are not
					fixed length, by disabling file pointer move.
	SN    06Apr00	Replace recursion with a do-while loop.
	SN    07Apr00	Use _tcsncpy instead of strcpy for safety.
					Increase size of buf for safety.
	SN    10Apr00	Only copy (size of destination buffer - 1) to allow
					room for terminator in destination.
					Use Calloc instead of Malloc so terminator need not be
					explicitly set after _tcsncpy.
 
 ***************************************************************************/
//void cl_read_all_cell_headers( FILE *fp, int * pcellcount, struct tdlist *thiscell)
void cl_read_all_cell_headers( FILE *fp, int * pcellcount, struct tdlist *firstcell)
{
	struct tdlist *thiscell;															//SN06Apr00
	//char buf[20], fchar;
	TCHAR buf[101], fchar;																//SN07Apr00
	//int nobjects, bytestojump;
	int nobjects;


	// Set the start of the linked list													//SN06Apr00
	thiscell = firstcell;

	// Read all cell headers until we reach the end of the file							//SN06Apr00
	do {

		// Move file pointer to next cell header
		do {
			fchar = fgetc( fp);
		} while ((fchar != EOF) && (fchar != '%'));


		if (fchar == '%') {

			// Increment cell count
			//cellcount++;
			(*pcellcount)++;

			_ftscanf( fp, _T("%s"),buf);		/* case  */
			_tcsncpy(thiscell->ident.casename,  buf, sizeof(thiscell->ident.casename) - 1 );	//SN10Apr00

			_ftscanf( fp, _T("%s"),buf);		/* slide */
			_tcsncpy(thiscell->ident.slidename, buf, sizeof(thiscell->ident.slidename) - 1 );//SN10Apr00

			_ftscanf( fp, _T("%s"),buf);		/* cell  */
			_tcsncpy(thiscell->ident.cellname,  buf, sizeof(thiscell->ident.cellname) - 1 );	//SN10Apr00

			_ftscanf( fp, _T("%s"),buf);		/* date  */
			_tcsncpy(thiscell->dateadded,       buf, sizeof(thiscell->dateadded) - 1 );		//SN10Apr00

			_ftscanf( fp, _T("%d"),&nobjects);  /* number of objects  */
			thiscell->nobjs = nobjects;

			//thiscell->nextcell = (struct tdlist *) Malloc(sizeof(struct tdlist));
			thiscell->nextcell = (struct tdlist *) Calloc( 1, sizeof(struct tdlist));		//SN10Apr00
			thiscell->nextcell->nextcell = NULL;

			//bytestojump = 333 * nobjects;

			//if (fseek(ftraindata, bytestojump, SEEK_CUR) == 0)
				//get_file_data(ftraindata, thiscell->nextcell);
			//cl_read_all_cell_headers( fp, pcellcount, thiscell->nextcell);			//SN06Apr00 Removed
			thiscell = thiscell->nextcell;												//SN06Apr00
		}
	} while (fchar != EOF);
}


/************************************************** cl_write_data_cell_header
 
	CREATED:                    SN      11May99

	FUNCTION:
		Writes a cell header to a classifier Data file.
		Moved from trainclassCB.c where it was add_cell_header.

	PARAMETERS:
		(Definition of parameters, types, and usage i.e. read, write, or update)
 
	RETURN:
		None.
 
	EXTERNS:
		(Definition and usage of external or static variables used)
 
	MODS:
 
 ***************************************************************************/
void cl_write_cell_header(FILE *fp, int datacount, struct tdcellinfo *cellinfo)
{
	struct tm *thetime;
	time_t clock;

	clock = time(NULL);
	thetime = localtime(&clock);

	fprintf( fp, "%% %s %s %s",cellinfo->casename,cellinfo->slidename,cellinfo->cellname);
	fprintf( fp," %02d/%02d/%02d %4d\n",thetime->tm_mday,thetime->tm_mon+1,thetime->tm_year % 100,datacount);
}



/***************************************************** cl_read_datum
 
	CREATED:                    SN      10May99

	FUNCTION:
		Read a single line from Data file, skipping comments
		(line beginning in # or %). Return EOF when appropriate.

		Used to be function readdatum originally in
		karylib\mahalanobis.c.

	PARAMETERS:
		fp:			read: Classifier Data file, must already be open.
		cclass:		write: Class of an object.
		fv:			write: Array of ndim object features.
		ndim:		read: Number of features.
 
	RETURN:
		int. The error status flag.
		0 - ok,
		1 - error, ran out of data or hit non-numeric data
		EOF - prematurely hit end of file.
 
	EXTERNS:
		(Definition and usage of external or static variables used)
 
	MODS:
		SN	04Aug98	Inserted new error code and removed exits
					that produced mysterious crashes.
		SN	09Apr99	Replaced class with cclass.
		SN	11May99	Remove redundant nclasses parameter.
 
 ***************************************************************************/
int cl_read_datum(FILE *fp, int *cclass, double *fv, int ndim)
{
	int n, f;
	TCHAR buf[60];


	n=_ftscanf(fp, _T("%60s"),buf);

	if (n==EOF) return (EOF);

	if ((buf[0] == '%') || (buf[0] == '#')){ /* Comment */
		_ftscanf (fp, _T("%*[^\n]"));  /* Throw away rest of line */
		return (cl_read_datum(fp, cclass, fv, ndim));
	}

	if (!isdigit (buf[0]) && buf[0] != '-') { /* Quick check */
		fprintf (stderr, "ERROR:cl_read_datum:Non-numeric data: \n%s\n", buf);
		return( 1);
	}
	_stscanf(buf, _T("%d"), cclass);

	for (f=0; f<ndim; f++) {
		n=_ftscanf(fp, _T("%lf"), &fv[f]);
		if ((n==EOF) || (n!=1)) {
			fprintf (stderr, "ERROR:cl_read_datum:Incorrect number of features.\n");
			return( 1);
		}
	}
	return (0);
}


/***************************************************** cl_write_datum
 
	CREATED:                    SN      11May99

	FUNCTION:
		Write a single line of features to Data file.

		Used to be code in trainclassCB.c function normalise_data.

	PARAMETERS:
		fp:			read: Classifier Data file, must already be open.
		cclass:		read: Chromosome Class of an object.
		fv:			write: Array of ndim object features.
		ndim:		read: Number of features.
 
	RETURN:
		None.
		
	EXTERNS:
		(Definition and usage of external or static variables used)
 
	MODS:
 
 ***************************************************************************/
void cl_write_datum( FILE *fp, int cclass, double *fv, int ndim)
{
	int i;


	fprintf( fp, "%3d ", cclass);			// Write chromosome class

	for (i=0; i<ndim; i++)					// Write ndim object features
#ifdef i386
		fprintf( fp, "%10.3e ", fv[i]);
#endif
#ifdef WIN32
		fprintf( fp, "%11.3e ", fv[i]);
#endif
		
	fprintf( fp, "\n");						// Write newline
}

/*****************************************************************************
              Copyright (c) 1999 Applied Imaging International
 ****************************************************************************/

