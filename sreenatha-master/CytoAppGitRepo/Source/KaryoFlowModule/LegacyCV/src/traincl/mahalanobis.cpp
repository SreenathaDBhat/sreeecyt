/*
 * mahalanobis.c	Jim Piper	27/3/84
 *
 * Compute Mahalanobis' generalised distance between two vectors
 * using a given inverted variance-covariance matrix.
 *
 * Use it for maximum likelihood classifier.
 *
 * Modifications
 *
 * 23Jun99	SN	For new classifiers generated with this software
 *				read in a valid class mapping from the CLASS file
 *				mapping actual class indecies to valid class indecies.
 *				Valid classes are those that had enough chromosomes in them.
 *				See readclassparams and classmahal.
 *	07May99	SN	New Class file reading routines.
 *	06May99 SN	Moved from chromlib to traincl.
 *	29Apr99 SN	Updated set_def_params with DEFAULT_NUMOFCLASSES instead of MAXCLASS.
 *				Moved readdatum to traincl\cytovcv.cpp, the only place it is used.
 *	28Apr99 SN	Use dynamic allocation for class parameters. See functions
 *				create_class_params and destroy_class_parameters.
 *	27Apr99	SN	Removed all cholesky method references as unused.
 *				In particular function cholesky_mahalanobis and
 *				statics chol_vcv, chol_clvcv and cholesky_decomp.
 *	26Apr99 SN	Removed ndim parameter from readclassparams, read_class_params2
 *				and read_c_params.
 *				Moved reading of number of classes and featires to open_classfile.
 *	09Apr99 SN	Added malloc.h, stdlib.h and chromlib.h includes.
 *				Removed unreferenced variables.
 *				Removed OSK #ifndefs, they must be redundant now, surely.
 *				Private prototypes.
 *				Replaced class with cclass in findclass.
 *				Disabled unused functions classdist, classrawmahal,
 *					classrawdist, make_cholesky_decomps, vcv_cholesky.
 *	04Aug98	SN	Remove exits in readdatum and replace with new return value.
 *	07Apr98 WH	read_class_params2 - use %LE for _ftscanf in WIN32 version. Should be
 *				that for UNIX as well.
 *	2/1/97	WH	Changed ifdef i386 to ifndef OSK. Also note that Cytovision
 *				uses the idistvar() declared here not fastidist.c which doesn't 
 *				work.
 *  7/9/94 CAJ	modified for new classifier format files, including alot of
 *		initialisation. Both original and new format classification
 *		techniques can be used. Type of classifier is recognised by
 *		by the no. in the header >= CGHDAPI_CLASS (103) for new type 
 *		as read in by read_class_params2.
 *  4/3/93 MG	tidied up read_class_params2
 * 12/9/92 MC	Mods to read_class_params2() (1) to Free data (2) to enable
 *		'cultures' type files with any no. of header lines to be read in
 *
 * 11/8/92 MC	Incorporated function set_def_params() to initialise classifier
 *		arrays with default data. Modified to be OSK and INTARITH defined.
 * 14/7/92 MC	In readclassparams() replaced invvarsqrtdet (bug)
 *		and replaced  with clinvvarsqrtdet,
 *		also removed & (unnecessary)
 *	
 * 7/2/91 CAS	remove Mizar defs -> osk
 */

#include <stdio.h>
#include <stdlib.h>			//SN09Apr99 Needed for qsort
#include <malloc.h>			//SN09Apr99 Needed for calloc
#include <math.h>
#include <float.h>			//SN23Jun99 Needed for max double value
#include <string.h>
#include <ctype.h>
#include <woolz.h>
#include <chromanal.h>
#include "species.h"
#include <trainkarcl.h>		//SN06May99 Needed for public prototypes

#include <CreateArray.h>	//SN28Apr99 For dynamic arrays
#include "clfile.h"			//SN07May99 For Class file reading routines


#undef MAXOBJS
#undef MAXDIM
#undef MAXCLASS


/* #define DEBUG    /* a MC to class_out */
#ifdef DEBUG
extern FILE *fp; /* a MC for class_out */
#endif

#define TRUEMAXLIK

#define INTARITH

#define OLD_C 0
#define NEW_C 1

#define VALIDCLASS_MAXLINELENGTH	400

// Number of upper triangular elements of a square matrix of dimension x					//SN28Apr99
// This is used for the number of elements of vcv and clvcv
#define NUPPERTRIANGULAR(x)			( ((x) * ((x)+1)) / 2 )


//
// PRIVATE FUNCTION PROTOTYPES
//
int liksort( const void * a, const void * b);
void int1alloc(int * *p, int n);
void int2alloc(int * * *p, int m, int n);
void initmahalanobis(int dim);
double mahalanobis( double *fv, double *mv, register double *vcv, int dim);
//double cholesky_mahalanobis( double *fv, double *mv, double *c_sqrt_vcv, int dim);		//SN27Apr99 Removed
int classmahal(double *fvec, double *priorp, struct cllik *liklv, int nclass, int ndim);
double distvar(register double *fv, register double *mv, register double *cliv, int dim);
int idistvar(double *fv, register int *mv, register int *cliv, register int dim);
int iclassdist(double *fvec, double *priorp, struct cllik *liklv, int nclass, int ndim);
//void readclassparams(FILE *c, register int *nclass, register int *ndim, register int *sf, register int *nsf);
void readclassparams(FILE *c, int nclass, int *sf, int nsf);
//void read_class_params2( FILE *c,		/* for 'cultures' files */
//						register int *nclasses,	register int *ndims,
//						register int *sfeatvec,	register int *nfeatures,
//						double *lpp);
int read_class_params2( FILE *c, int nclasses, int *sfeatvec, int nfeatures, double *lpp);
int create_class_params( int nclass, int ndim);
void destroy_class_parameters( void);



//
// STATIC VARIABLES
//

// Switches
int POOL = 0;
int classifier_format=OLD_C;


// For dynamic arrays of class parameters
int current_nclass = 0;								// Number of chromosome classes, for use with dynamic arrays only
int current_ndim = 0;								// Number of selected features, for use with dynamic arrays only


//double invvarsqrtdet; /* c jp */								//SN28Apr99 Only used in redundant function classdist
double sqrtdet;
double *invvar			= NULL;	/* c jp */			// ndim array
double **classmeans		= NULL;						// nclass x ndim array
double **classinvvars	= NULL;						// nclass x ndim array
double *clsqrtdet		= NULL;						// nclass array
double *vcv				= NULL;						// (ndim*(ndim+1))/2 array
double **clvcv			= NULL;						// nclass x (ndim*(ndim+1))/2 array
double *clinvvarsqrtdet	= NULL;						// nclass array
int    *validclass      = NULL;						// nclass array

/* for cholesky decomposition */    /* c jp */
//int cholesky_decomp = 0;										//SN27Apr99 All cholesky methods unused so ditch this.
//double chol_vcv[MAXDIM*(MAXDIM+1)/2];							//SN27Apr99 All cholesky methods unused so ditch this.
//double chol_clvcv[MAXCLASS][MAXDIM*(MAXDIM+1)/2];				//SN27Apr99 All cholesky methods unused so ditch this.
double **i_c_correl		= NULL;						// nclass x ndim array
double max_sqrt_det;

#ifdef INTARITH
//int jpvt[MAXDIM];												//SN28Apr99 All cholesky methods unused so ditch this.
int **iclassmeans		= NULL;						// nclass x ndim array
int **iclassinvvars		= NULL;						// nclass x ndim array
double *conversionf		= NULL;						// nclass array
#endif /* INTARITH */

int **tabmahal;



/*
 * initialize Mahalanobis vcv lookup table
 */
/* Copied from typealloc.c because only routines used */
void int1alloc(int * *p, int n)
{
	if (n < 1)
		_ftprintf(stderr, _T("int1alloc: invalid array size\n"));

	if ((*p = (int *) calloc(n, sizeof(int))) == NULL)
		_ftprintf(stderr, _T("int1alloc: calloc failed\n"));
}


void int2alloc(int * * *p, int m, int n)
{
	int i, *dump;

	if (m < 1 || n < 1)
		_ftprintf(stderr, _T("int2alloc: invalid array size\n"));

	int1alloc(&dump, m * n);

	if ((*p = (int **) calloc(m, sizeof(int *))) == NULL)
		_ftprintf(stderr, _T("int2alloc: calloc failed\n"));

	for (i = 0; i < m; i++) {
		(*p)[i] = dump;
		dump += n;
	}
}


void initmahalanobis(int dim)
{
	register int i, j, *tabmahali;

	int2alloc(&tabmahal, dim, dim);

	for (i = 0; i < dim; i++) {
		tabmahali = tabmahal[i];
		for (j = 0; j < dim; j++)
			tabmahali[j] = j < i ? i * (i + 1) / 2 + j : j * (j + 1) / 2 + i;
	}
}


/* vcv is upper triangular portion only */
//
// MODS
// SN	29Apr99 Replace MAXDIM with successor NUMOFFEATURES.
//
double mahalanobis( double *fv, double *mv, register double *vcv, int dim)
{
	register int j, i;
	register double t, d;
	register int *tabmahali;
	//double dv[MAXDIM];
	double dv[NUMOFFEATURES];
	

	/* difference vector between sample and class mean */
	for (i = 0; i < dim; i++)
		dv[i] = fv[i] - mv[i];

	/* generalised distance */
	d = 0.0;
	for (i = 0; i < dim; i++) {
		t = 0.0;
		tabmahali = tabmahal[i];
		for (j = 0; j < dim; j++)
			t += dv[j] * vcv[*tabmahali++];
		d += dv[i] * t;
	}
	return(d);
}


//SN27Apr99 Removed references to cholesky method as unused
#if 0
/* c_sqrt_vcv is upper trianglular portion only */
double cholesky_mahalanobis( double *fv, double *mv, double *c_sqrt_vcv, int dim)
{
	int j, i;
	double /*t,*/ d;			//SN09Apr99 Removed unreferenced variable
	double dv[MAXDIM], cv;

	d = 0.0;
	for (i = 0; i < dim; i++){
		/* difference vector between sample and class mean */
#ifdef PIVOT
		j = jpvt[i]-1;
		dv[i] = fv[j] - mv[j];
#else /* PIVOT */
		dv[i] = fv[i] - mv[i];
#endif /* PIVOT */
	}

	for (i = 0; i < dim; i++) {
		cv = 0.0;
		for (j=i; j<dim; j++)
			cv += dv[j] * c_sqrt_vcv[tabmahal[j][i]];
		d += cv * cv;
	}
	return(d);
}
#endif


/* Classify using either full vcv facilities or integer technique 
 * depending on which format classifier being used.
 *
 * MODS
 * SN09Apr99	Replaced class with cclass.
 * SN29Apr99	Changed names of function parameters for consistency with read_c_params.
 *
 */
//int findclass(double *fvec, double *priorp, struct cllik *liklv, int nclass, int ndim)
int findclass(double *fvec, double *priorp, struct cllik *liklv, int nclasses, int nfeatures)
{
	int cclass;

	if (classifier_format == NEW_C)
		//cclass = classmahal(fvec,priorp,&liklv[0],nclass,ndim) - 1;
		cclass = classmahal(fvec,priorp,&liklv[0],nclasses,nfeatures) - 1;
	else
        //cclass = iclassdist(fvec,priorp,&liklv[0],nclass,ndim) - 1;
		cclass = iclassdist(fvec,priorp,&liklv[0],nclasses,nfeatures) - 1;

	return(cclass);
}


/*
 * classify by maximum likelihood.
 * compute mahalanobis distance from class mean vectors,
 * convert to likelihood using square root of determinant
 * of variance - covariance matrix,
 * multiply by prior probability of group to give probability.
 * fill in per-class table of likelihod values for subsequent re-arrangement
 *
 * SN	29Apr99	Dynamically allocate liks array.
 * SN	23Jun99	Only classify into valid classes. ie those that have stats associated with them.
 */
int classmahal(double *fvec, double *priorp, struct cllik *liklv, int nclass, int ndim)
{
	int i,maxc;
	double d, lik, maxl;
	//struct cllik liks[MAXCLASS], *liklvp;
	struct cllik *liks = NULL;					// nclass array
	struct cllik *liklvp;
	int farrayOK;


	// Create liks array storing liklihood values
	farrayOK = 1;
	liks = (struct cllik *) CreateArray1D( nclass, sizeof(struct cllik), _T("classmahal"), _T("liks"), &farrayOK);
	if (!farrayOK)
		return( nclass);			// On error return abnormal classification


	// Classify based on mahalanobis distance from class mean vectors
	liklvp = liks;

	for (i=0; i<nclass; i++) {

		if (0 <= validclass[i])
		{
			//SN27Apr99 Removed cholesky method as unused
			//if (cholesky_decomp)	/* c jp */
			//	d = cholesky_mahalanobis(fvec,&classmeans[i][0],(POOL != 0? chol_vcv: &chol_clvcv[i][0]),ndim);
			//else
			d = mahalanobis(fvec,&classmeans[validclass[i]][0],(POOL != 0? vcv: &clvcv[validclass[i]][0]),ndim);

/* original AI 
			lik = exp(-d/2.0);
#ifdef TRUEMAXLIK
			lik /= clsqrtdet[i];
			lik *= priorp[i];
#endif */ /* TRUEMAXLIK */

			lik = -d/2;
			lik += (POOL != 0? sqrtdet: clsqrtdet[validclass[i]]-max_sqrt_det);
			lik += priorp[i];	/* c jp */
		}
		else
		{	// Invalid class so set log liklihood very large and negative.
			lik = -DBL_MAX;		
		}

		liklvp->lval = lik;
		liklvp->chromclass = i;
		liklvp++;
		if (i == 0 || lik > maxl) {
			maxl = lik;
			maxc = i;
		}
	}
	/*
	 * sort likelihoods into order and copy first CUTOFF
	 * values into class likelihood structure
	 */
	qsort(liks,nclass,sizeof(struct cllik),liksort);
	liklvp = liks;
	for (i=0; i<CUTOFF; i++) {
		liklv->lval = liklvp->lval;
		liklv->chromclass = liklvp->chromclass;
		liklv++;
		liklvp++;
	}

	// Clear liks
	DestroyArray1D( liks);

	return (maxc+1);
}


//SN09Apr99 classrawmahal is completely unused by WinCV.
#if 0
int classrawmahal(double *fvec, struct cllik *liklv, int nclass, int ndim)
{
	int i, maxc;
	double d, lik, maxl; //, mahalanobis();		//SN09Apr99 Superceded by prototype

	for (i=0; i<nclass; i++){
		if (cholesky_decomp)
			d = cholesky_mahalanobis(fvec,&classmeans[i][0],(POOL? chol_vcv: &chol_clvcv[i][0]),ndim);
		else
			d = mahalanobis(fvec,&classmeans[i][0],(POOL? vcv: &clvcv[i][0]),ndim);

		/*
		 * If covariance matrix was not positive definite, may have problems !!
		 */
		lik = -sqrt(d);
		liklv->lval = lik;
		liklv->chromclass = 1;
		liklv++;
		if (i == 0 || lik > maxl) {
			maxl = lik;
			maxc = i;
		}
	}
	return (maxc+1);
}
#endif


/*
 * compute distance of feature vector from class mean vector,
 * corrected by per-class feature variances.
 */
double distvar(register double *fv, register double *mv, register double *cliv, int dim)
{
	register double d;
	double dv;
	register int i;

	d = 0.0;
	for (i=0; i<dim; i++) {
		/* difference vector between sample and class mean */
		dv = *fv++ - *mv++;
		/* generalised Euclidean distance */
		d += (dv * dv * (*cliv++));
	}
	return(d);
}


//SN09Apr99 classdist is completely unused by WinCV.
#if 0
/*
 * classify by distance (variance-corrected), retain in form
 * compatible with log-likelihood.
 */
int classdist(double *fvec, double *lpriorp, register struct cllik *liklv, int nclass, int ndim)
{
	register int i,maxc;
	//int liksort();					//SN09Apr99 Superceded by prototype.
	double d, lik, maxl, distvar();
	struct cllik liks[MAXCLASS];
	register struct cllik *liklvp;
	liklvp = liks;
	for (i=0; i<nclass; i++) {
		d = distvar(fvec,&classmeans[i][0],(POOL != 0? invvar: &classinvvars[i][0]),ndim);
		lik = -d/2.0;
		lik += (POOL != 0? invvarsqrtdet: clinvvarsqrtdet[i]);
		lik += lpriorp[i];	/* log of prior probability */
		liklvp->lval = lik;
		liklvp->chromclass = i;
		liklvp++;
		if (i == 0 || lik > maxl) {
			maxl = lik;
			maxc = i;
		}
	}
	/*
	 * sort likelihoods into order and copy first CUTOFF
	 * values into class likelihood structure
	 */
	qsort(liks,nclass,sizeof(struct cllik),liksort);
	liklvp = liks;
	for (i=0; i<CUTOFF; i++) {
		liklv->lval = liklvp->lval;
		liklv->chromclass = liklvp->chromclass;
		liklv++;
		liklvp++;
	}
	return (maxc+1);
}
#endif


//SN09Apr99 classrawdist is completely unused by WinCV.
#if 0
int classrawdist(double *fvec, struct cllik *liklv, int nclass, int ndim)
{
	register int i, maxc;
	double d, lik, maxl;

	for (i=0; i<nclass; i++){
		d = distvar(fvec,&classmeans[i][0],(POOL != 0? invvar: &classinvvars[i][0]), ndim);
		lik = -sqrt(d);
		liklv->lval = lik;
		liklv->chromclass = i;
		liklv++;
		if (i== 0 || lik > maxl){
			maxl = lik;
			maxc = i;
		}
	}
	return (maxc+1);
}
#endif


#ifdef INTARITH
/* #ifdef NOASSEMBLER c MC/


/* BP - use version in fastidist.c */
/* WH #if 0 Cytovision uses this one, the fastivar.c version doen't work */


/*
 * compute distance of feature vector from class mean vector,
 * corrected by per-class feature variances, IN INTEGER ARITHMETIC.
 *
 * THIS ROUTINE NOW REPLACED BY ASSEMBLER CODED VERSION IN "fastidist.a"
 */
int idistvar(double *fv, register int *mv, register int *cliv, register int dim)
{
	register int d,dv;
	register int i;
	//register int c, f,m; /* a MC for file o/p only */	//SN09Apr99 Removed unreferenced variables

	d = 0;
	for (i=0; i<dim; i++) {
		/* difference between sample and class mean */
#ifdef DEBUG
		f = *fv; /* a MC */
		m = *mv; /* a MC */
		c = *cliv; /* a MC */
#endif /* DEBUG */
		dv = *fv++ - *mv++;
		/* generalised Euclidean distance */
		d += dv * dv * *cliv++;
		if (d < 0) {
#ifdef DEBUG
			/* _ftprintf(stderr,"IDISTVAR : overflow\n"); */
			_ftprintf(fp,"IDISTVAR : overflow ");
_ftprintf(fp,"i= %d d= %d *fv= %d *mv= %d dv= %d *cliv= %d\n",i,d,f,m,dv,c);
#endif /* DEBUG */
			return(2000000000);	/* near enough 2**31 */
		}
	}
	return(d);
}
/* #endif NOASSEMBLER c MC */

/* BP - use version in fastidist.c */
/* WH #endif use this one*/



/*
 * classify by distance (variance-corrected) IN INTEGER ARITHMETIC,
 * computing log-likelihoods
 */
#define LARGENEG -9999999999999.0

/*
 * SN	29Apr99	Dynamically allocate liks array.
 */
int iclassdist(double *fvec, double *priorp, struct cllik *liklv, int nclass, int ndim)
{
	register int d,i,maxc;
	double lik, maxl;
	//struct cllik liks[MAXCLASS], *liklvp;
	struct cllik *liks = NULL;					// nclass array
	struct cllik *liklvp;
	int farrayOK;


	// Create liks array storing liklihood values
	farrayOK = 1;
	liks = (struct cllik *) CreateArray1D( nclass, sizeof(struct cllik), _T("iclassdist"), _T("liks"), &farrayOK);
	if (!farrayOK)
		return( nclass);			// On error return abnormal classification


	liklvp = liks;
	for (i=0; i<nclass; i++) {
		d = idistvar(fvec,&iclassmeans[i][0],&iclassinvvars[i][0],ndim);
		d = (-d/2);
		lik = (double)d/conversionf[i];
#ifdef TRUEMAXLIK
		lik += clinvvarsqrtdet[i];
		lik += priorp[i];
#endif /* TRUEMAXLIK */
		liklvp->lval = lik;
		liklvp->chromclass = i;
		liklvp++;
#ifdef DEBUG
_ftprintf(fp,"ICLASSDIST : class = %d dist = %d lik = %f\n",i,d,lik);
#endif /* DEBUG */
	} /* for loop */

	/*
	 * sort likelihoods into order.
	 * copy first CUTOFF values into class likelihood structure
	 */

	maxc = 0; /* a MC prevent v.l maxc values */
	for (i=0; i<CUTOFF; i++) {
		liklvp = liks;
		maxl = LARGENEG;
		for (d=0; d<nclass; d++) {
			if (liklvp->lval > maxl) {
				maxl = liklvp->lval;
				maxc = d;
			}
			liklvp++;
		}
		liks[maxc].lval = LARGENEG;
		liklv[i].lval = maxl;
		liklv[i].chromclass = maxc;
	}
	
	// Clear liks
	DestroyArray1D( liks);

	return (liklv->chromclass+1);
}
#endif /* INTARITH */


/* Open and read the header of the specified classifier, check the header to
 * see if it is new or old format classifier.
 *
 * TODO
 * Check return of _ftscanf and values for *pnclasses and *pnfeatures.
 *
 * MODS
 * SN 07May99 New Class header reading routine.
 * SN 26Apr99 Also read number of classes and number of features, instead of
 *            read_c_params().
 */
//FILE * open_classfile(char *classfile)
FILE * open_classfile(LPCTSTR classfile, int *pnclasses, int *pnfeatures)
{
	FILE *fclass;
	SMALL whichclassifier;
    int nok, id;
	CLheader clheader;


    fclass = _tfopen(classfile, _T("r"));
	if (fclass != NULL) {

		// If there is any error reading the class file header
		// then close the file and return NULL.
		memset( &clheader, 0, sizeof(clheader));
		nok = cl_read_class_header( fclass, &id, &clheader);

		if (!nok)
		{
			fclose( fclass);
			_ftprintf( stderr, _T("ERROR: open_classfile: Reading %s classifier Class file.\n"), classfile);
			fflush( stderr);
			return( NULL);
		}

		whichclassifier = (short) id;
		*pnfeatures = clheader.nselected_feature;
		*pnclasses  = clheader.nclass;

		if ((whichclassifier == CGHDAPI_CLASS)
		|| (whichclassifier >= USER_CLASS))

			classifier_format = NEW_C;
		else
			classifier_format = OLD_C;
	}

	return(fclass);
}


//
// create_class_params	SN	28Apr99
//
// Creates those class parameters which need dynamic arrays. These were
// previously declared as static fixed length arrays. The statics
//
//		current_nclass and current_ndim
//
// maintain the dimensions of the dynamic arrays so that they may be 
// correctly destroyed. They are updated here.
//
// Return 0 - error, 1 - OK
//
// MODS
// SN	23Jun99	Create validclass array for mapping actual classes to valid classes
//				that the classifier has stats on.
//
int create_class_params( int nclass, int ndim)
{
	int farrayOK;
	TCHAR szfnname[] = _T("create_class_params");


	// If class parameters already exist then destroy them
	if ((current_nclass > 0) && (current_ndim > 0))
		destroy_class_parameters();

	// Create dynamic arrays for class parameters
	farrayOK = 1;

	// 1D arrays
	vcv				= (double *) CreateArray1D( NUPPERTRIANGULAR(ndim), sizeof(double), szfnname, _T("vcv"), &farrayOK);
	invvar			= (double *) CreateArray1D( ndim,   sizeof(double), szfnname, _T("invvar"),          &farrayOK);
	clsqrtdet		= (double *) CreateArray1D( nclass, sizeof(double), szfnname, _T("clsqrtdet"),       &farrayOK);
	clinvvarsqrtdet = (double *) CreateArray1D( nclass, sizeof(double), szfnname, _T("clinvvarsqrtdet"), &farrayOK);
	conversionf		= (double *) CreateArray1D( nclass, sizeof(double), szfnname, _T("conversionf"),     &farrayOK);
	validclass      = (int *)    CreateArray1D( nclass, sizeof( int),   szfnname, _T("validclass"),      &farrayOK);


	// 2D arrays
	clvcv			= CreateArray2Ddouble( nclass, NUPPERTRIANGULAR(ndim), szfnname, _T("clvcv"), &farrayOK);
	classmeans		= CreateArray2Ddouble( nclass, ndim, szfnname, _T("classmeans"),    &farrayOK);
	classinvvars	= CreateArray2Ddouble( nclass, ndim, szfnname, _T("classinvvars"),  &farrayOK);
	i_c_correl		= CreateArray2Ddouble( nclass, ndim, szfnname, _T("i_c_correl"),    &farrayOK);
	iclassmeans		= CreateArray2Dint(    nclass, ndim, szfnname, _T("iclassmeans"),   &farrayOK);
	iclassinvvars	= CreateArray2Dint(    nclass, ndim, szfnname, _T("iclassinvvars"), &farrayOK);

	// Check error status
	if (!farrayOK)
	{
		destroy_class_parameters();
		current_nclass = 0;
		current_ndim = 0;
	}
	else
	{
		current_nclass = nclass;
		current_ndim = ndim;
	}

	return( farrayOK);
}


//
// destroy_class_parameters	SN	28Apr99
//
// Destroy those class parameters defined as dynamic arrays using
// the statics current_nclass and current_ndim. See create_class_params.
//
void destroy_class_parameters( void)
{
	// Dispose of 1D arrays
	DestroyArray1D( vcv);
	DestroyArray1D( invvar);
	DestroyArray1D( clsqrtdet);
	DestroyArray1D( clinvvarsqrtdet);
	DestroyArray1D( conversionf);
	DestroyArray1D( validclass);

	// Dispose of 2D arrays
	DestroyArray2Ddouble( current_nclass, NUPPERTRIANGULAR(current_ndim), clvcv);
	DestroyArray2Ddouble( current_nclass, current_ndim, classmeans);
	DestroyArray2Ddouble( current_nclass, current_ndim, classinvvars);
	DestroyArray2Ddouble( current_nclass, current_ndim, i_c_correl);
	DestroyArray2Dint(    current_nclass, current_ndim, iclassmeans);
	DestroyArray2Dint(    current_nclass, current_ndim, iclassinvvars);
}


/* Based on the file header read in the relevant classifier format file */
//
// SN26Apr99 removed redundant ndims
//           Passed nclasses and nfeatures as ints not pointers to int.
// SN28Apr99 Create arrays for class params.
//
//void read_c_params( FILE *c,												/* for 'cultures' files */
//				   register int *nclasses, register int *ndims,
//				   register int *sfeatvec, register int *nfeatures,
//				   double *lpp)
void read_c_params( FILE *c,												/* for 'cultures' files */
				   int nclasses, int *sfeatvec, int nfeatures, double *lpp)

{
	create_class_params( nclasses, nfeatures);

	if (classifier_format == NEW_C){
		readclassparams(c,nclasses,sfeatvec,nfeatures);
	}
	else {
		read_class_params2(c,nclasses,sfeatvec,nfeatures,lpp);
	}
}


//
// MODS
// SN 26Apr99 Removed redundant ndim.
//            Passed nclass and nsf as ints not pointers to int.
//
//	MODS
//	SN	23Jun99	Read in the valid class mapping if it exists.
//				This indicates how many classes there are real stats on in the class file.
//
void readclassparams(FILE *c, int nclass, int *sf, int nsf)
{
	int i,j;
	int n, nvalidclass;
	TCHAR sztoken[31] = {0};


	// Read a string
	if (_ftscanf( c, _T(" %s"), sztoken) != 1)
	{
		_ftprintf( stderr, _T("ERROR: readclassparams: Reading token from Class file.\n"));
		return;
	}

	// Does it contain the valid class mapping?
	if (_tcscmp( szValidClassToken, sztoken) == 0)
	{
		// Read the numberof valid classes
		_ftscanf( c, _T("%d"), &nvalidclass);

		// Read the valid class mapping, mapping actual classes (nclass) to valid ones (nvalidclass).
		for (i=0; i<nclass; i++)
			_ftscanf( c, _T("%d"), &validclass[i]);

		// Read the number of selected features
		for (i=0; i<nsf; i++)
			_ftscanf(c, _T("%d"),&sf[i]);
	}
	else
	{
		// NO it doesnt so all classes are valid
		nvalidclass = nclass;

		// Create a default valid mapping
		for (i=0; i<nvalidclass; i++)
			validclass[i] = i;

		// Read selected features
		_stscanf( sztoken, _T("%d"), &sf[0]);
		for (i=1; i<nsf; i++)
			_ftscanf(c, _T("%d"), &sf[i]);
	}
	

/* class means */
	for (i=0; i<nvalidclass; i++)
		for (j=0; j<nsf; j++)
			_ftscanf(c, _T("%lE"), &classmeans[i][j]);

/* pooled inverted variance-covariance matrix */
	_ftscanf(c, _T("%lE"), &sqrtdet);
	for (i=0; i<nsf*(nsf+1)/2; i++)
		_ftscanf(c, _T("%lE"), &vcv[i]);

/* per-class inverted variance-covariance matrices */
	max_sqrt_det = -100000000.0;
	for (j=0; j<nvalidclass; j++) {
		_ftscanf(c, _T("%lE"),&clsqrtdet[j]);
		clsqrtdet[j] = -log(clsqrtdet[j]);
		if (clsqrtdet[j] > max_sqrt_det)
			max_sqrt_det = clsqrtdet[j];
		for (i=0; i<nsf*(nsf+1)/2; i++)
			_ftscanf(c, _T("%lE"), &clvcv[j][i]);
	}

/* pooled inverted variances */
	for (i=0; i<nsf; i++) {
		_ftscanf(c, _T("%lE"),&invvar[i]);
	}
	_ftscanf(c, _T("%lE"),clinvvarsqrtdet); /* MC 14/7/92 see notes */

/* per class inverted variances */
	for (i=0; i<nvalidclass; i++) {

		for (j=0; j<nsf; j++)
			_ftscanf(c, _T("%lE"),&classinvvars[i][j]);

		_ftscanf(c, _T("%lE"), &clinvvarsqrtdet[i]);
	}
	
#ifdef INTARITH
/* class integer means */
	for (i=0; i<nvalidclass; i++)
		for (j=0; j<nsf; j++)
			_ftscanf(c, _T("%d"), &iclassmeans[i][j]);

/* per class integer inverted variances */
	for (i=0; i<nvalidclass; i++) {
		_ftscanf(c, _T("%lE"), &conversionf[i]);
		for (j=0; j<nsf; j++)
			n = _ftscanf(c, _T("%d"), &iclassinvvars[i][j]);
	}

/* intra-class correlations */
	/* The saving of these is user dependant so check that this is here */
	if (n != EOF) {
		for (i=0; i<nvalidclass; i++)
			for (j=0; j<nsf; j++)
				_ftscanf(c, _T("%lE"), &i_c_correl[i][j]);
	}
#endif /* INTARITH */

	initmahalanobis(nsf);
/* This is one of the possibilities with jp's new stuff
	cholesky_decomp = 0;
*/
}


//SN09Apr99 New function definition to agree with qsort prototype.
//int liksort(register struct cllik *a, register struct cllik *b)		
int liksort( const void * a, const void * b)
{
	struct cllik * pa = (struct cllik *) a;
	struct cllik * pb = (struct cllik *) b;


	if (pa->lval > pb->lval)
		return(-1);
	else if (pa->lval < pb->lval)
		return(1);
	else
	    return(0);
}


//SN09Apr99 make_cholesky_decomps is completely unused by WinCV.
#if 0
int make_cholesky_decomps(int nclass, int nsfv)
{
	int i; /*, j, k;*/					//SN09Apr99 Removed unreferenced variables
	vcv_cholesky(vcv, chol_vcv, nsfv);
	for (i=0; i<nclass; i++)
		vcv_cholesky(clvcv[i], &chol_clvcv[i][0], nsfv);
	cholesky_decomp = 1;
}
#endif


//SN09Apr99 vcv_cholesky is completely unused by WinCV.
#if 0
/*
 * Compute the cholesky decomposition b of input
 * positive definite symmetric matrix a, both in
 * our standard upper-triangular form
 */
int vcv_cholesky(double *a, double *b, int n)
{
	double A[MAXDIM*MAXDIM], W[MAXDIM];
	int LDA, P, JOB, INFO;
	int i, j, k;
	
	k = 0;
	for (i=0; i<n; i++) {
		jpvt[i] = 0;
		for (j=0; j<=1; j++,k++)
			A[i*MAXDIM+j] = a[k];
	}
	LDA = MAXDIM;
	P = n;
#ifdef PIVOT
	JOB = 1;
#else /* PIVOT */
	JOB = 0;
#endif /* PIVOT */
	dchdc(A, &LDA, &P, W, jpvt, &JOB, &INFO);
	k = 0;
	for (i=0; i<n; i++)
		for (j=0; j<=i; j++,k++)
			b[k] = A[i*MAXDIM+j];
}
#endif


/*
 *
 * READ_CLASS_PARAMS2
 *
 * reads input files (such as cultures1 or 2)
 *
 * Created by M.Castling	11/8/92
 * SN 26Apr99 Removed ndims redundant parameter.
 *            nclasses and nfeatures are now int and not pointer to int.
 * SN 29Apr99 Dynamically alloctae arrays.
 *            Return 0 - error, 1 - ok.
 *
 */
//void read_class_params2( FILE *c,		/* for 'cultures' files */
//						register int *nclasses,	register int *ndims,
//						register int *sfeatvec,	register int *nfeatures,
//						double *lpp)
int read_class_params2( FILE *c, int nclasses, int *sfeatvec, int nfeatures, double *lpp)
{
double *logsqrtdet = NULL;			// nclass array
int **featmean     = NULL;			// nfeatures x nclasses array
int **featsd       = NULL;			// nfeatures x nclasses array
//double logsqrtdet[MAXCLASS - 1];
//int featmean[MAXDIM][MAXCLASS - 1]; /* max of 30 features */
//int featsd[MAXDIM][MAXCLASS - 1];
int farrayOK;
TCHAR szfnname[] = _T("read_class_params2");

register int i,j;
//int dum1,dum2,dum3,dum4,noofstr = 0;			//SN09Apr99 Remove unreferenced variables
	
/* read selected classifier,tissue,stain and dimensions */
/*_ftscanf(c,"%d %d %d %d %d %d",&dum1,&dum2,&dum3,&dum4,nfeatures,nclasses);*/
//_ftscanf(c,"%d %d",nfeatures,nclasses);			//SN26Apr99 Read number of classes and features moved to open_classfile.

												//SN26Apr99 Removed ndims as redundant and confusing.
//*ndims = *nfeatures; /* ndims (ndim) in chromclass is defined but not used,
//		      included for completeness */


// Create arrays								//SN29Apr99	
farrayOK = 1;
logsqrtdet = (double *) CreateArray1D( nclasses, sizeof(double), szfnname, _T("logsqrtdet"), &farrayOK);
featmean = CreateArray2Dint( nfeatures, nclasses, szfnname,_T("featmean"), &farrayOK);
featsd   = CreateArray2Dint( nfeatures, nclasses, szfnname, _T("featsd"), &farrayOK);
if (!farrayOK)
{
	DestroyArray1D( logsqrtdet);
	DestroyArray2Dint( nfeatures, nclasses, featmean);
	DestroyArray2Dint( nfeatures, nclasses, featsd);
	return(0);
}


for (i=0; i<nclasses; i++)
	_ftscanf(c, _T("%LE"),&logsqrtdet[i]);

for (i=0; i<nclasses; i++)
	_ftscanf(c, _T("%LE"),&conversionf[i]);

/* per class means and inverted variances */
for (i=0; i<nfeatures; i++)
	{
	_ftscanf(c, _T("%d"),&sfeatvec[i]);
	for (j=0; j<nclasses; j++)
		{
		_ftscanf(c, _T("%d"), &featmean[i][j]);
		}
	for (j=0; j<nclasses; j++)
		{
		_ftscanf(c, _T("%d"), &featsd[i][j]);
		}
	}

/* transpose */
for(i = 0; i < nfeatures; i++) 
	{
	for(j = 0; j < nclasses; j++)
		{
		iclassmeans[j][i] = featmean[i][j];
		iclassinvvars[j][i] = featsd[i][j];
		}
	}

/*
 * per class inverted variances. Add log of prior probability
 * for computational efficiency
 */
for (i = 0; i < nclasses; i++)
	{
	clinvvarsqrtdet[i] = lpp[i] + logsqrtdet[i];
	}



#ifdef DEBUG
_ftprintf(fp, _T(" FILE PARAMETERS \n\n"));
_ftprintf(fp," no. of class = %d ",nclasses);
_ftprintf(fp," no. of features = %d\n",nfeatures);
//_ftprintf(fp," no. of features used = %d\n",*ndims);

_ftprintf(fp," selected feature numbers ");
for(i = 0; i < nfeatures; i++)
	_ftprintf(fp,"%3d",sfeatvec[i]);

_ftprintf(fp,"\n\n\t iclassmeans[i][j] \t class mean feature vectors \n");
for (i = 0; i < nclasses; i++)
	{
	_ftprintf(fp,"\n class %d ",i);
	for (j = 0; j < nfeatures; j++)
		_ftprintf(fp,"%5d",iclassmeans[i][j]);
	}
	
_ftprintf(fp,"\n\n\t iclassinvvars[i][j] \t class variances \n");
for (i = 0; i < nclasses; i++)
	{
	_ftprintf(fp,"\n class %d ",i);
	for (j = 0; j < nfeatures; j++)
		_ftprintf(fp,"%5d",iclassinvvars[i][j]);
	}

_ftprintf(fp,"\n\n class  prior probability \t clinvvarsqrtdet[i] \t conversionf[i]");						
for (i = 0; i < nclasses; i++)
_ftprintf(fp,"\n  %d \t %f \t\t %f \t\t %f",i,lpp[i],clinvvarsqrtdet[i],conversionf[i]);								
# endif /* DEBUG */

// Destroy arrays
DestroyArray1D( logsqrtdet);
DestroyArray2Dint( nfeatures, nclasses, featmean);
DestroyArray2Dint( nfeatures, nclasses, featsd);

// Everythings ok												//SN29Apr99
return(1);
}


/*
 * S E T_D E F_P A R A M S  --
 * This program initialises the classifier arrays with default data
 *
 * Modifications
 *
 * Created from newdentify.c and kclass.c (setparams)	3/8/92	MC
 * Removed redundant parameter ndims					26Apr99	SN
 * Replaced 'MAXCLASS - 1' with 'DEFAULT_NUMOFCLASSES'	29Apr99 SN
 * Also replaced remaining 'MAXCLASS' with 'DEFAULT_NUMOFCLASSES'
 */
//void set_def_params(register int *nclasses, register int *ndims, register int *sfeatvec, register int *nfeatures, double *lpp)
void set_def_params(register int *nclasses, register int *sfeatvec, register int *nfeatures, double *lpp)
{

/*
 * defaults for vcv classifier
 */
#define DEFVCVFEATS 10 /* in original version this was defined elsewhere */

int featmean[DEFAULT_NUMOFCLASSES][DEFVCVFEATS];		//int featmean[MAXCLASS - 1][DEFVCVFEATS];
int featsd[DEFAULT_NUMOFCLASSES][DEFVCVFEATS];			//int featsd[MAXCLASS - 1][DEFVCVFEATS];
int i,j;

static int featids[DEFVCVFEATS] = {
	1, 3, 9, 10, 11, 12, 13, 17, 19, 21
};

static int vcvfeatm[DEFVCVFEATS][DEFAULT_NUMOFCLASSES] = {
    {292, 275, 177, 149, 123, 109, 66, 28, 12, 4, -1, -3, -54, -76, -78, -114, -130, -123, -194, -167, -218, -208, 58, -181},
    {201, 134, 159, -2, 6, 54, 141, 50, 133, 55, 173, 52, -89, -96, -76, 98, 52, -31, 55, 199, -122, -36, 120, -95},
    {-88, -128, -83, -55, -36, -19, -105, -21, -120, -16, -140, -9, 57, 84, 92, 55, -47, 66, 146, 8, 178, 109, -80, 104},
    {-113, 14, -80, -14, 19, 33, 31, 68, 81, -1, 48, 120, 45, -107, -102, 98, -17, 23, -85, -70, 7, -67, 55, 27},
    {103, 52, -46, -63, -96, -57, 44, -17, 101, -22, 232, 35, 0, -52, -100, -41, 133, -68, -68,  82, -107, -14, 15,-31},
    {-126, -50, -20, -1, -48, 103, -17, 49, -8, 31, 22, -22, 65, 23, -46, 35, 67, 102, -63, -76, 25, -56, -72, 82},
    {-80, 3, 141, 37, 40, 14, 69, 93, 76, 113, -53, -95, -92, -18, 8, 69, -10, 70, -96, -33, -99, -94, 83, -69},
    {91, 11, 4, -132, -149, -57, 29, -47, 64, -51, 136, -37, -31, -72, -92, 67, 48, -54, 64, 167, 16, 55, 6, -6},
    {-185, -36, -10, -2, 12, 26, 52, 76, 39, 85, -12, -4, -113, -62, -57, 107, 53, 56, 26, 38, -19, 15, 52, -27},
    {151, 2, -47, -119, -61, -59, -1, -35, -13, 8, 35, 75, -20, 23, 39, 76, -69, -5, 65, -128, 82, -45, -5, -40}
};

static int vcvfeatsd[DEFVCVFEATS][DEFAULT_NUMOFCLASSES] = {
    {1000, 755, 941, 607, 510, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000},
    {295, 523, 150, 946, 1000, 176, 554, 336, 467, 296, 230, 88, 49, 46, 74, 125, 57, 121, 27, 49, 34, 26, 809, 47},
    {946, 1000, 1000, 1000, 554, 671, 442, 398, 146, 566, 167, 268, 276, 408, 263, 84, 64, 211, 169, 74, 78, 106, 700, 204},
    {263, 432, 602, 404, 259, 343, 252, 188, 117, 152, 87, 73, 55, 55, 53, 71, 44, 61, 24, 26, 21, 27, 299, 83},
    {722, 579, 310, 542, 336, 633, 373, 393, 120, 339, 119, 90, 276, 124, 152, 41, 68, 132, 97, 46, 62, 71, 464, 194},
    {270, 309, 227, 266, 120, 130, 185, 150, 125, 116, 85, 72, 58, 64, 65, 70, 27, 52, 38, 20, 24, 21, 242, 52},
    {368, 471, 128, 330, 153, 341, 182, 173, 124, 129, 61, 53, 174, 67, 89, 51, 48, 55, 116, 38, 62, 62, 308, 122},
    {351, 305, 321, 244, 196, 217, 138, 136, 111, 137, 89, 75, 132, 104, 130, 68, 68, 103, 128, 63, 86, 91, 222, 131},
    {317, 200, 138, 186, 120, 199, 149, 121, 90, 87, 90, 59, 90, 67, 70, 57, 51, 47, 68, 33, 29, 39, 215, 73},
    {356, 445, 274, 245, 186, 238, 151, 173, 126, 105, 151, 78, 68, 80, 52, 34, 71, 53, 42, 22, 17, 29, 276, 72}
};

static double logsqrtdet[DEFAULT_NUMOFCLASSES] = {
	-4.17364e+01, -3.84634e+01, -4.09258e+01, -3.73258e+01, -3.78843e+01,
	-3.92105e+01, -3.79475e+01, -3.78642e+01, -4.02520e+01, -3.85448e+01,
	-3.93441e+01, -3.97798e+01, -3.86768e+01, -3.95346e+01, -3.94818e+01,
	-4.08774e+01, -4.05505e+01, -3.85535e+01, -4.34407e+01, -4.04939e+01,
	-4.18571e+01, -4.21680e+01, -3.79024e+01, -3.92196e+01
};

static double scalef[DEFAULT_NUMOFCLASSES] = {
	1.80785e+06, 1.00025e+06, 1.12641e+06, 7.09317e+05, 5.24467e+05,
	8.17324e+05, 5.42356e+05, 4.73699e+05, 5.28679e+05, 4.67304e+05,
	3.60817e+05, 3.06241e+05, 3.03608e+05, 3.03883e+05, 3.10881e+05,
	2.92945e+05, 2.37440e+05, 2.32728e+05, 4.96306e+05, 1.72364e+05,
	2.34341e+05, 2.80429e+05, 7.60205e+05, 3.08781e+05
};

static double vcvmeans[DEFAULT_NUMOFCLASSES][DEFVCVFEATS] = {
	{2.709800e+02,  1.985000e+02, -1.397535e+02, -9.132985e+01, 5.182115e+01,
 	-8.724692e+01, -1.470910e+02,  2.325229e+01, -1.661504e+02, 8.523500e+01},
 	{2.586300e+02,  1.195000e+02, -1.093265e+02,  7.999900e+00,-1.884110e+01,
	-4.946763e+01, -2.405045e+01, -5.620630e+01, -4.580030e+01, 3.353798e+01},
 	{1.681250e+02,  2.040000e+02, -1.031205e+02, -7.229800e+01,-2.006866e+01,
 	-3.067895e+01,  5.185960e+01,  4.672175e+01, -1.495000e+02, 3.373900e+01},
 	{1.316370e+02,  3.500000e+01, -6.017750e+01, -3.821370e+01,-5.163181e+01,
 	-5.209325e+01,  6.550650e+01, -1.221800e+02,  2.144055e+01,-3.9034150e+01}, 
 	{1.059405e+02,  2.600000e+01, -5.017000e+01,  3.316700e+00,-1.0742300e+02,
	-5.578105e+01,  7.559050e+01, -1.414380e+02,  2.123385e+01,-5.8645705e+01},
 	{9.945895e+01,  9.631579e+01, -5.329374e+01, -4.462368e+00,-4.7964737e+01,
 	 5.815032e+01, -6.640947e+00, -5.816579e+00, -5.716632e+00, 1.9973895e+01},
 	{6.358300e+01,  1.160000e+02, -8.725200e+01,  1.332285e+01, 5.1729200e+01,
 	-7.073380e+01,  4.532200e+01, -2.085300e+00,  3.818115e+01, 6.9471500e+01},
 	{2.789895e+01,  8.250000e+01, -2.881240e+01,  8.025200e+01,-2.7270745e+01,
  	3.3473500e+01,  1.081305e+02, -4.145275e+01,  9.892250e+01,-1.5138150e+01},
 	{2.509235e+01,  1.000000e+02, -5.231550e+01,  8.590350e+01, 2.3511700e+01,
	 2.084485e+01,  1.155410e+02,  1.819745e+01,  4.400950e+01,-4.9775685e+01},
 	{9.404520e+00,  6.150000e+01, -3.585126e+01,  1.898135e+01,-1.2262470e+01,
	 5.375210e+01,  1.002310e+02, -2.555900e+01,  9.967415e+01, 3.5553300e+01},
 	{1.220285e+01,  1.465000e+02, -9.125065e+01, -1.985720e+01, 1.9768650e+02,
	-4.691225e+01, -1.443095e+02,  2.443755e+01,  1.119700e+01, 4.8108500e+01},
 	{6.964650e+00,  4.650000e+01, -2.087115e+01,  6.779300e+01, 5.2740650e+00,
 	-9.637067e+01, -5.040429e+01, -6.016613e+01,  6.835475e+01, 6.8299900e+01},
   {-4.715100e+01, -1.330000e+02,  9.110600e+01,  1.839255e+01, 1.2195500e+01,
 	 6.370168e+01, -1.378960e+02, -8.023780e+01, -1.537390e+02, 1.2852300e+01},
   {-5.614400e+01, -1.010000e+02,  9.607750e+01, -6.069100e+01,-6.6360500e+01,  
	 1.396895e+02,  1.105928e+01, -8.511800e+01, -5.131700e+01, 3.6174750e+01}, 
   {-6.632400e+01, -9.250000e+01,  8.118950e+01, -1.071335e+02,-9.5823000e+01,
    -2.992220e+01, -5.898210e+00, -4.962030e+01, -4.998940e+01,-3.5130350e+01}, 
   {-8.575850e+01,  1.605000e+02,  2.761755e+01,  2.264366e+01, 9.9513000e+01,
	-3.693695e+01,  6.571130e+01,  1.364030e+02,  2.271795e+01, 2.3603450e+01}, 
   {-1.076420e+02,  5.250000e+01,  1.687465e+01,  4.673400e+01, 1.5650650e+02,  
	 1.215440e+02, -7.678900e+01, -2.962300e+00,  9.388045e+01,-5.2387150e+00}, 
   {-1.136660e+02,  1.800000e+01,  3.914045e+01,  2.060350e+01,-5.1136500e+01,  
	 1.126289e+02,  1.250339e+02, -1.883550e+01,  7.526800e+01,-4.8469750e+01}, 
   {-1.403900e+02,  1.570000e+02,  7.890930e+01, -1.311910e+02,-1.2950850e+01,
 	-8.864700e+01, -8.827845e-01,  1.396500e+02,  2.811565e+01,-1.1071850e+02}, 
   {-1.445000e+02,  1.890000e+02,  2.610050e+00, -3.268690e+01, 1.3550500e+02,
	-8.763600e+01,  2.528950e+01,  2.074900e+02, -3.480680e+01,-1.7512650e+02}, 
   {-1.890000e+02, -1.100000e+01,  2.563400e+02,  1.242225e+02,-2.0116200e+02,
	 1.133280e+02, -6.795940e+01,  8.128557e+01,  1.518890e+01, 2.2491200e+02}, 
   {-1.742550e+02, -1.850000e+01,  1.429260e+02,  3.672117e+01,-4.7271600e+01,
	 5.448140e+01, -1.296660e+02,  9.173950e+00,  6.825500e+00,-3.5779300e+01}, 
 	{4.388700e+01,  1.220000e+02, -8.062400e+01, -1.688600e+01, 2.3152570e+01,
	-1.213050e+02,  4.937230e+01, -4.216900e+00,  2.106620e+01,-6.1436000e+01}, 
   {-1.665900e+02,  6.000000e+00,  7.410100e+01,  3.837890e+01, 2.4906000e+01,
	 4.875700e+01, -4.546700e+01,  1.375040e+01,  2.349000e+00,-1.7328300e+02}
   };

static double vcvclsqrt[DEFAULT_NUMOFCLASSES] = {								//Changed MAXCLASS to DEFAULT_NUMOFCLASSES
	9.208149e+06,  2.528092e+05,  1.423006e+05,  6.964356e+03,  3.868379e+03, 
	2.236590e+05,  3.002366e+05,  3.895257e+04,  8.475570e+04,  7.207331e+04, 
	3.742933e+05,  1.101304e+05,  8.721790e+04,  1.144563e+05,  2.886913e+05, 
	3.455148e+04,  1.254209e+05,  6.378002e+05,  5.347656e+04,  1.094608e+04, 
	6.746683e+05,  6.591438e+05,  6.363548e-04,  4.342510e-03
	};

static double vcvclvcv[DEFAULT_NUMOFCLASSES][DEFVCVFEATS*(DEFVCVFEATS+1)/2] = { //Changed MAXCLASS to DEFAULT_NUMOFCLASSES
{3.418351e-03,  9.016834e-05,  9.436625e-04,  4.877350e-04,  5.315273e-04, 
 1.268276e-03, -1.531511e-04, -1.409623e-04, -3.054797e-04,  4.817537e-04, 
 9.239590e-04,  6.731008e-04,  1.076698e-03,  1.152735e-05,  1.966490e-03, 
 5.460554e-04,  1.589115e-04,  3.733480e-04, -6.450707e-04, -2.005658e-04, 
 1.119494e-03, -1.641968e-04, -1.285946e-04,  2.863648e-04, -3.851899e-05, 
 4.892584e-05, -7.864391e-06,  4.502013e-04, -5.882001e-04, -4.494649e-04, 
-3.474730e-04,  1.279870e-04, -5.681715e-04, -2.359854e-04,  1.842464e-04, 
 5.265034e-04,  4.695919e-04,  4.999496e-04,  5.426058e-05,  6.075447e-05, 
 4.240555e-04, -5.532339e-05, -2.734082e-04, -3.212960e-04,  6.769768e-04, 
-1.150626e-03, -8.805627e-04, -9.705559e-04,  3.711262e-04, -1.124198e-03, 
-5.111034e-04,  1.373188e-04,  5.517141e-04, -4.507608e-04,  1.546435e-03}, 

{2.847402e-03,  4.503483e-04,  1.322194e-03, -1.037556e-03,  2.518389e-04, 
 2.853569e-03, -1.825000e-03, -9.474502e-04,  1.157643e-03,  3.738296e-03, 
-3.908380e-04, -5.668033e-04,  5.873860e-04,  5.346857e-04,  1.286878e-03, 
 1.858659e-03,  6.169797e-04, -1.646955e-03, -3.551807e-03, -6.489913e-04, 
 3.811041e-03, -7.984001e-04, -1.219585e-03,  4.275071e-04,  1.389612e-03, 
 1.084773e-03, -1.294070e-03,  2.417182e-03,  1.973679e-04, -3.112279e-04, 
-7.616712e-05, -2.556285e-04, -2.012880e-05,  1.990316e-04,  4.024724e-04, 
 8.486207e-04, -2.348308e-04,  3.763850e-05, -4.742297e-06, -3.912636e-05, 
-2.462134e-04,  8.307038e-05, -8.344997e-04, -2.328142e-04,  8.081511e-04, 
-1.089472e-04,  8.163140e-05,  1.140165e-04, -1.403508e-04,  3.194895e-04, 
 4.465179e-05,  2.287601e-05,  7.839434e-05, -7.380836e-05,  7.269822e-04}, 

{1.535148e-02,  1.413254e-03,  1.213150e-03,  1.012672e-03,  1.064245e-03, 
 3.318716e-03,  6.980542e-05, -8.261581e-04,  4.228498e-05,  2.085300e-03, 
 8.870478e-04, -3.272514e-04, -5.514441e-04,  9.068369e-05,  1.367000e-03, 
-1.508786e-03,  9.478756e-04,  1.666577e-04, -1.753117e-03, -5.711932e-04, 
 2.352794e-03,  5.852455e-04, -3.530237e-04, -4.096906e-05,  9.998481e-04, 
 5.219725e-04, -8.963977e-04,  1.351536e-03, -2.796062e-03, -9.404806e-05, 
 5.451604e-04, -2.242444e-04, -9.096654e-04,  6.475812e-04, -6.385879e-04, 
 1.654608e-03, -1.163481e-03,  2.651425e-04,  2.351273e-04, -3.600037e-04, 
-3.580015e-04,  5.982478e-04, -1.979167e-04,  5.013078e-04,  5.225466e-04, 
-2.225516e-03, -3.473902e-04, -3.295395e-04,  1.255847e-04, -6.652676e-05, 
 1.707357e-04,  2.189038e-04,  3.946222e-04,  2.210148e-04,  7.444970e-04}, 

{3.657733e-03,  2.138632e-03,  8.260977e-03, -3.730462e-04,  2.882994e-03, 
 7.520205e-03,  4.015008e-03,  4.388673e-03, -2.673615e-03,  1.252025e-02, 
-2.082428e-03, -1.934490e-03, -8.479282e-04, -3.439063e-03,  3.565390e-03, 
-1.843043e-03, -1.771713e-03,  1.313704e-04, -4.680838e-03,  2.072911e-03, 
 3.026972e-03, -1.319781e-03,  1.168781e-03,  2.404037e-03, -2.155945e-03, 
 3.865197e-04,  7.330890e-04,  2.584304e-03,  4.724949e-04, -1.717500e-04, 
-4.696938e-04,  1.875448e-03, -1.064450e-03, -5.914597e-04, -2.109654e-04, 
 1.131352e-03,  8.559335e-05,  8.071645e-05,  4.549537e-04,  1.878673e-05, 
-5.041604e-04, -1.626684e-04, -1.693030e-04,  2.061538e-05,  5.469572e-04, 
-9.298567e-04, -1.065846e-03,  3.367001e-04, -2.186212e-03,  6.646718e-04, 
 7.467989e-04,  2.042039e-04, -1.959163e-04, -2.772190e-05,  1.120035e-03}, 

{5.330797e-03,  1.076452e-04,  1.190459e-02, -6.105714e-04,  9.851420e-04, 
 4.177284e-03,  8.570519e-04,  1.888437e-03,  3.053734e-04,  4.126958e-03, 
-2.860677e-04,  1.639679e-03,  1.040031e-03,  1.563197e-03,  4.832187e-03, 
-5.446219e-04, -1.207122e-03,  2.405124e-04, -2.546698e-03, -2.185335e-04, 
 2.323238e-03, -7.046422e-04,  3.480558e-04, -6.315520e-04, -2.963222e-04, 
-1.105030e-03,  2.753775e-04,  1.604198e-03, -1.913317e-04,  4.211953e-04, 
-1.455911e-03,  1.329814e-03, -9.699104e-04, -1.606975e-03, -3.681476e-05, 
 2.978617e-03, -3.313399e-04, -1.942134e-03,  3.714812e-04, -8.588061e-04, 
-4.667934e-04,  7.788466e-04,  2.559989e-04, -9.149791e-04,  1.274336e-03, 
-5.038038e-04, -1.476719e-04, -6.849885e-04, -1.383407e-03, -7.807822e-04, 
 1.001647e-03,  2.417371e-04, -6.668486e-05, -1.693418e-05,  1.328316e-03}, 

{1.256446e-02,  3.425280e-04,  4.566171e-04, -1.570028e-03,  5.680265e-04, 
 8.079672e-03,  1.851861e-04,  2.211243e-04,  2.890875e-03,  2.574489e-03, 
-1.314660e-03,  6.877230e-04, -6.927247e-05,  1.011924e-04,  2.667617e-03, 
 7.846101e-04, -4.161922e-04, -2.135918e-03, -2.214894e-03, -1.089859e-03, 
 2.579080e-03, -9.162235e-04, -6.619515e-05, -1.563332e-04, -5.234372e-04, 
 1.584420e-04,  2.214598e-04,  9.371520e-04,  1.901061e-03,  4.241419e-05, 
 1.874484e-03,  6.979319e-04, -1.473956e-03,  1.634960e-04, -3.950334e-04, 
 2.259829e-03, -1.154828e-04,  2.124310e-04,  1.185959e-04, -4.656970e-04, 
 1.147909e-04,  4.658123e-04, -2.241188e-04,  2.634918e-04,  6.881480e-04, 
-1.016492e-03,  5.189637e-05,  3.671332e-04,  2.684347e-04,  4.932952e-05, 
-2.221015e-04, -3.859640e-05,  3.931732e-05,  7.197008e-05,  3.569272e-04}, 

{2.804131e-03,  4.516391e-04,  7.913060e-04, -1.041228e-03,  7.356335e-04, 
 4.643228e-03,  5.120083e-04,  4.203824e-04,  6.700296e-05,  2.358037e-03, 
-1.051305e-04, -4.878370e-04,  5.044034e-04,  5.098861e-04,  2.541023e-03, 
-4.278851e-04, -7.562248e-04, -4.833325e-04, -1.683401e-03,  7.254464e-04, 
 2.114278e-03, -9.786980e-04, -2.478531e-04,  2.165993e-03,  4.036451e-04, 
 1.181139e-03,  8.400834e-05,  2.469336e-03,  1.857228e-04,  7.869085e-05, 
-5.883346e-04, -1.176224e-04, -8.658325e-04, -2.225835e-04, -8.712631e-04, 
 8.109620e-04,  9.780049e-04,  4.846197e-04, -8.150769e-04,  5.032351e-04, 
-6.746399e-04, -7.665744e-04, -1.404799e-03,  5.812228e-04,  1.393719e-03, 
-1.074742e-05, -7.861035e-05, -9.168807e-04,  2.816871e-04,  3.819610e-04, 
 2.673178e-04, -3.334867e-04, -7.654525e-05,  7.727946e-05,  9.290438e-04}, 

{1.061245e-02,  7.723429e-03,  1.428432e-02, -5.044711e-04, -2.128537e-03, 
 2.659765e-03,  1.796809e-04, -1.533411e-03,  5.575770e-04,  7.936359e-04, 
-2.278727e-03, -3.431187e-03,  1.092449e-03,  1.011870e-04,  2.088125e-03, 
-4.040456e-03, -3.780249e-03,  1.342519e-03, -1.044681e-04,  1.108791e-03, 
 3.881094e-03,  1.981703e-04,  1.151596e-03, -3.050299e-04, -9.487191e-05, 
-4.149584e-04, -2.735425e-04,  1.027351e-03,  7.284939e-04, -3.957474e-05, 
-1.838093e-04,  1.867676e-06, -4.544345e-04, -1.824066e-04, -7.706454e-05, 
 7.100819e-04,  4.043219e-04,  8.937829e-05,  1.372034e-04,  1.774806e-04, 
 1.438334e-04, -4.169442e-04, -3.037497e-04, -1.636959e-04,  6.869265e-04, 
-1.034738e-05,  6.492121e-04, -1.267746e-03, -2.806665e-04,  1.534002e-04, 
-1.041592e-03,  2.794664e-05, -1.054081e-04,  1.501153e-04,  1.391529e-03}, 

{6.496458e-03,  2.821246e-03,  3.563734e-03, -1.920858e-04,  3.247798e-06, 
 2.797792e-03, -4.902184e-04, -7.834030e-05,  1.296067e-04,  1.009031e-03, 
-6.541192e-04, -7.251571e-04,  3.723985e-04, -4.061016e-04,  2.737967e-03, 
 6.259047e-04,  3.370081e-04, -1.455587e-04, -1.476645e-03,  1.125399e-03, 
 2.810258e-03, -2.279967e-03, -1.160162e-03,  1.028091e-03, -2.472362e-05, 
 9.075653e-04,  1.104800e-04,  2.328652e-03, -5.464925e-04, -9.213051e-04, 
 9.309282e-05,  3.371761e-04, -1.969017e-04, -3.979076e-04, -7.885288e-05, 
 9.980759e-04,  2.987645e-04, -4.266487e-05, -2.133840e-04,  1.951133e-04, 
-2.189793e-05, -1.208365e-04, -4.738738e-04,  3.189843e-04,  4.494256e-04, 
 4.630134e-04,  4.219718e-05, -6.964431e-04, -5.900174e-04,  1.088238e-04, 
 8.360459e-04, -4.083024e-04, -7.925302e-05,  2.209099e-05,  8.378257e-04}, 

{1.827923e-02, -1.957306e-03,  9.593369e-04, -2.574552e-03,  9.691912e-06, 
 3.463694e-03, -1.336151e-03,  4.136326e-05, -3.191502e-04,  1.244033e-03, 
 1.208583e-03,  3.449694e-05, -1.597495e-03,  8.863407e-04,  5.580377e-03, 
-2.508959e-03,  7.272268e-04, -1.540511e-04, -7.329853e-05, -9.269799e-05, 
 2.335797e-03,  9.203691e-04, -1.768287e-04, -1.579100e-04,  5.096400e-05, 
 1.347071e-03, -5.002035e-05,  1.472852e-03, -5.652581e-04,  4.153310e-04, 
-6.960702e-04,  2.607225e-04,  5.905686e-04,  4.306075e-04,  4.809581e-08, 
 5.267804e-04,  7.761192e-04, -1.603699e-04,  1.312537e-04, -1.184602e-04, 
 4.105333e-04, -3.961489e-04, -1.106515e-04, -9.413845e-05,  3.474860e-04, 
-7.984623e-04,  1.314119e-04, -2.734227e-04,  4.969299e-05,  3.859243e-04, 
 2.846859e-05, -1.534778e-04,  2.070719e-04,  2.090779e-05,  4.938343e-04}, 

{1.094713e-02,  2.572005e-04,  1.763660e-03, -2.575517e-03, -3.755374e-04, 
 5.036649e-03,  2.590974e-05,  1.403546e-04,  2.345573e-04,  2.143206e-04, 
-1.452519e-03, -1.983599e-04,  1.126522e-03,  9.951572e-05,  9.426555e-04, 
-6.639524e-04, -5.952820e-06,  1.035408e-03, -8.048012e-05,  7.588587e-05, 
 8.272091e-04, -3.580118e-05,  3.414972e-04, -1.773380e-04, -1.029399e-05, 
 1.886934e-04, -1.355702e-04,  5.339420e-04,  4.394740e-04,  6.917765e-05, 
-1.271945e-04, -4.786964e-05, -3.567737e-04,  1.374420e-04, -2.727314e-05, 
 5.888494e-04,  9.116450e-04,  1.158768e-04, -7.951230e-05, -7.551728e-05, 
-1.993936e-04,  9.739683e-05, -2.904576e-05,  1.518512e-04,  3.151161e-04, 
-1.197390e-03,  6.532620e-04, -1.343129e-03, -1.607301e-04,  4.590325e-05, 
-6.487010e-05,  5.911287e-04,  3.105275e-04,  5.052427e-06,  2.429194e-03}, 

{9.514126e-03,  7.015564e-04,  1.148216e-03, -1.349575e-04,  9.240292e-05, 
 7.357733e-03,  3.902208e-04, -1.509713e-04,  7.822011e-04,  8.065413e-04, 
-8.373607e-04, -6.239145e-04,  1.534553e-04, -2.614594e-04,  1.670877e-03, 
-1.282659e-03, -1.696942e-04, -6.870081e-04, -1.106647e-03,  9.573190e-04, 
 2.568874e-03, -1.463755e-04, -6.428389e-04, -9.718918e-04, -4.594257e-04, 
 8.276336e-04,  9.019806e-04,  1.898815e-03,  5.831323e-04, -2.114712e-04, 
-1.839269e-03, -5.014921e-04,  1.855320e-04,  1.057877e-03,  5.172014e-04, 
 1.424597e-03,  2.705067e-04,  4.002407e-04,  1.256751e-05,  1.229272e-05, 
-3.524617e-04, -4.143702e-04, -3.734024e-04, -2.254986e-04,  4.229223e-04, 
-9.620072e-04, -1.287067e-04, -7.821008e-05, -5.863719e-04,  4.226405e-04, 
 1.211267e-03,  1.031933e-03,  3.062329e-04, -1.906465e-04,  1.182845e-03}, 

{8.657212e-03, -2.591440e-03,  3.185553e-03, -1.520107e-03,  6.877692e-04, 
 1.996746e-03,  2.344665e-03, -2.519928e-03, -1.258372e-03,  4.386192e-03, 
-4.095504e-03,  2.318378e-03,  1.456382e-03, -2.618327e-03,  3.733280e-03, 
-3.732956e-03,  2.896266e-03,  1.358381e-03, -3.959846e-03,  3.345369e-03, 
 4.486856e-03, -2.318505e-03,  1.284461e-03,  5.060065e-04, -1.809930e-03, 
 2.240193e-03,  2.419782e-03,  2.778474e-03, -1.428540e-03,  1.035469e-03, 
 5.435371e-04, -7.689026e-04,  8.922046e-04,  1.250343e-03,  3.493175e-04, 
 1.083591e-03,  2.086301e-03, -1.964376e-03, -5.512762e-04,  2.427592e-03, 
-2.274138e-03, -2.713514e-03, -2.134513e-03, -5.198113e-04,  2.386049e-03, 
-3.503320e-03,  2.131825e-03,  8.279558e-04, -2.975320e-03,  2.862286e-03, 
 3.497710e-03,  2.645871e-03,  8.705508e-04, -2.513263e-03,  3.622904e-03}, 

{1.314428e-02,  2.281506e-04,  8.828877e-04,  1.948917e-03,  6.805167e-04, 
 3.123732e-03, -2.089059e-03,  6.345224e-05,  2.797426e-04,  8.099284e-04, 
 1.449828e-04, -2.339158e-04,  1.041934e-03,  2.166607e-04,  3.035904e-03, 
 1.051098e-03,  4.609692e-04, -2.693050e-04, -5.506390e-04, -6.467109e-04, 
 2.268107e-03,  1.198868e-03,  1.533870e-04,  6.258580e-04, -3.856143e-04, 
 5.952498e-04,  6.062027e-04,  1.075876e-03,  1.328985e-03,  4.152858e-05, 
 2.123159e-04, -3.246058e-04, -4.136128e-04,  3.830717e-04,  3.666415e-04, 
 1.024245e-03,  6.129773e-04, -2.021465e-04,  2.984755e-04, -6.880002e-05, 
 5.552129e-04, -2.484957e-04,  4.950110e-05,  6.870495e-05,  4.312347e-04, 
-1.733492e-04, -9.383242e-05, -5.477473e-04, -9.331271e-05,  4.977877e-04, 
-4.200762e-04,  1.044421e-04,  1.180358e-04,  1.199188e-04,  9.189158e-04}, 

{8.937030e-03, -1.545385e-04,  1.018700e-03,  1.317634e-03, -2.331078e-04, 
 2.826049e-03,  5.012867e-04, -1.316510e-05,  5.015764e-04,  1.102913e-03, 
-6.553496e-04,  3.719712e-04,  5.424194e-04, -1.143424e-04,  1.328862e-03, 
-1.501158e-03,  3.170529e-04, -1.690952e-04, -6.697000e-04,  2.853097e-04, 
 1.121655e-03,  7.801929e-04, -3.376581e-04,  9.918910e-04,  2.776247e-04, 
-2.495588e-04, -3.632493e-04,  1.217577e-03,  2.477128e-05,  3.205990e-04, 
-3.849626e-04,  2.961794e-05, -2.211087e-04,  9.419430e-05, -1.997460e-04, 
 4.680646e-04, -2.011685e-04, -1.882766e-04, -6.628703e-05, -2.886681e-04, 
 1.620135e-04,  1.172930e-04, -1.211720e-04, -1.181927e-04,  3.414340e-04, 
-1.918171e-03,  2.094812e-04, -1.010249e-03, -6.385279e-04,  2.305905e-04, 
 6.594942e-04, -5.367472e-04,  1.546614e-04,  2.168502e-04,  1.315643e-03}, 

{1.481018e-02,  1.876563e-03,  6.283057e-03, -2.140926e-03,  1.890594e-03, 
 3.869629e-03,  3.432696e-04, -9.522016e-04, -7.141184e-04,  1.146574e-03, 
-2.468655e-03,  3.147331e-05,  1.388314e-03, -1.883268e-04,  2.283865e-03, 
-1.644370e-03,  2.740253e-03,  2.462533e-03, -2.124853e-03,  7.330994e-04, 
 5.664998e-03, -1.783932e-03,  2.282048e-04,  1.647863e-03,  5.434147e-05, 
 5.222546e-04,  4.638715e-04,  1.465110e-03, -6.662406e-04,  1.100327e-03, 
 6.798178e-04, -6.630693e-04, -2.679007e-04,  1.579411e-03,  1.230989e-04, 
 1.021542e-03,  1.888474e-03,  2.483519e-04, -6.295649e-04,  1.997947e-04, 
-2.250630e-04, -8.899326e-04, -5.628843e-04, -2.624573e-04,  7.416136e-04, 
 5.825289e-05, -1.856960e-04, -1.119581e-03,  1.767113e-05, -7.328687e-04, 
-4.182304e-04, -5.916452e-04,  2.250921e-04,  9.392332e-05,  9.612006e-04}, 

{1.190381e-02,  5.212192e-04,  7.481227e-04, -1.266015e-03, -7.424823e-04, 
 4.741749e-03, -1.708759e-03, -3.855387e-04,  1.936390e-03,  1.465797e-03, 
-1.031614e-03, -6.888986e-04,  4.688636e-03,  1.812226e-03,  5.256638e-03, 
-4.668617e-04,  2.954350e-04, -7.328481e-04, -2.972639e-04, -7.853649e-04, 
 9.382728e-04,  3.173467e-05, -3.360029e-04,  2.096866e-03,  7.637641e-04, 
 2.177969e-03, -7.771609e-04,  1.750445e-03, -3.746634e-04, -6.674243e-04, 
 1.564880e-04,  2.440799e-04,  2.421731e-04, -5.271778e-04,  2.015907e-04, 
 1.728216e-03,  1.510648e-03, -3.458021e-04,  1.399413e-04, -4.325211e-04, 
 3.915294e-04, -4.603084e-04,  3.047101e-04,  9.867191e-04,  1.586826e-03, 
-2.333234e-04,  5.273959e-05, -1.428052e-03, -5.231446e-04, -1.581397e-03, 
-6.031535e-06, -4.933228e-04,  2.888800e-04,  3.726932e-05,  1.027162e-03}, 

{1.490715e-02,  1.527964e-03,  2.109154e-03, -1.805686e-03, -5.225098e-05, 
 1.863528e-03, -1.600748e-03, -3.305205e-04,  1.692429e-04,  6.811074e-04, 
-1.279586e-03, -8.222481e-04,  4.012446e-04, -6.367333e-06,  1.189063e-03, 
 1.637708e-03,  4.441847e-04,  1.772078e-05, -6.447272e-04,  5.310095e-05, 
 9.096086e-04, -6.622097e-04, -3.429954e-04,  6.858616e-04, -4.947973e-05, 
 7.153416e-04,  8.092577e-05,  8.306184e-04,  1.675421e-04, -1.503105e-04, 
 1.843659e-04, -4.435774e-06, -1.225534e-04,  2.232997e-04, -3.770470e-05, 
 8.644179e-04,  3.286901e-04, -4.331593e-04,  1.118130e-04, -3.214449e-05, 
-7.553726e-05, -1.390335e-05, -5.083563e-05,  1.574227e-04,  3.786637e-04, 
-2.084375e-03,  2.494689e-04, -3.504863e-04,  6.396669e-05,  1.514627e-04, 
-1.420600e-04, -1.210437e-04,  6.744281e-05, -3.583141e-04,  1.285347e-03}, 

{2.387966e-02, -8.322094e-03,  4.922157e-03,  3.315776e-03, -2.539031e-03, 
 7.955407e-03, -4.085972e-03,  1.520732e-03, -1.215207e-03,  1.387415e-03, 
-4.257193e-04,  4.164340e-04,  9.461262e-04, -1.989322e-04,  8.565748e-04, 
 2.250296e-03, -5.973094e-04, -9.705372e-04, -4.424982e-04,  2.664509e-04, 
 1.866088e-03,  1.476482e-03, -2.426204e-04,  3.069638e-03, -1.195089e-03, 
 2.880891e-04, -1.188146e-03,  4.080425e-03,  9.713835e-03, -5.426255e-03, 
 5.376108e-03, -2.201024e-03, -3.057682e-04, -2.347331e-05,  2.561038e-03, 
 8.246388e-03,  1.702064e-03, -2.858962e-04, -7.469739e-04, -4.035286e-04, 
 5.896092e-05,  6.521970e-04, -2.964826e-04,  1.951587e-04,  7.376319e-04, 
-1.429601e-03,  5.382970e-04, -6.773431e-04,  3.509549e-04,  9.713387e-05, 
-1.559785e-05, -4.529889e-04, -7.053575e-04, -1.500089e-05,  4.431838e-04}, 

{2.095906e-02, -1.173966e-02,  2.695177e-02, -2.209103e-03,  5.732490e-03, 
 3.274156e-03,  2.448057e-03, -1.784808e-03, -5.087530e-04,  1.773630e-03, 
 3.310860e-03, -2.717659e-03, -5.061724e-04,  1.175172e-03,  1.735668e-03, 
-1.939333e-03,  1.263624e-03,  2.743589e-04, -1.407265e-03, -1.104844e-03, 
 2.032580e-03,  1.724326e-03, -4.341137e-03, -1.126261e-04, -5.003985e-04, 
 5.843323e-06,  1.290694e-04,  2.206159e-03, -4.438291e-03,  8.538988e-03, 
 2.554597e-03, -1.156948e-03, -1.513721e-03,  9.202119e-04, -7.151049e-04, 
 3.965116e-03, -1.120826e-03,  3.105997e-03,  3.726012e-04, -3.284295e-05, 
-2.619699e-04,  6.777519e-05, -7.517706e-04,  8.323946e-04,  7.794104e-04, 
-2.330893e-03,  3.246199e-03,  9.523549e-06, -4.181707e-04, -6.534452e-04, 
 5.074209e-04, -7.946691e-04,  8.747013e-04,  3.896263e-04,  1.287419e-03}, 

{1.494096e-02, -6.810396e-04,  6.727023e-04,  3.544302e-03,  2.930658e-04, 
 3.580586e-03,  4.080678e-04,  8.252190e-05,  4.013994e-04,  2.810961e-04, 
 5.379478e-04,  1.119044e-04,  9.381123e-05,  2.457475e-04,  2.873899e-03, 
-9.655258e-04, -3.189170e-04, -9.489048e-04, -3.655866e-04,  1.917897e-04, 
 8.302743e-04,  1.002628e-03,  3.801443e-04, -8.600259e-05,  1.934012e-04, 
 2.480582e-03,  1.369934e-04,  2.945926e-03, -1.090577e-03, -5.342623e-04, 
-1.051100e-03, -2.191776e-04,  5.875120e-05,  6.702984e-04, -1.305588e-04, 
 1.039589e-03,  9.814283e-04,  7.842974e-05,  5.654681e-04,  2.282309e-05, 
-1.741049e-04, -2.707343e-04,  5.938587e-06, -3.040146e-04,  6.769951e-04, 
-5.507206e-04,  3.901755e-04, -9.231694e-04, -5.886939e-05,  2.393938e-03, 
 3.843191e-04,  2.387320e-03,  8.284963e-05, -5.717279e-05,  2.984831e-03}, 

{2.151398e-02, -1.194123e-03,  2.070269e-03, -2.072317e-03,  1.164630e-03, 
 2.040216e-03,  3.213132e-03, -1.499400e-03, -8.490003e-04,  2.188251e-03, 
-1.114006e-03,  1.279771e-03,  8.379483e-04, -1.695518e-03,  2.446684e-03, 
-3.004468e-03,  9.219298e-04,  5.957200e-04, -1.699988e-03,  1.599905e-03, 
 1.622916e-03, -7.967372e-04, -7.158760e-04, -2.207378e-04,  3.393817e-05, 
-9.939216e-06,  2.962378e-04,  9.328815e-04,  3.068121e-04, -1.865748e-03, 
-7.164664e-04,  1.452380e-03, -1.427707e-03, -1.001024e-03,  6.454364e-04, 
 2.096618e-03,  6.635750e-05, -1.280491e-03, -6.047477e-04,  1.012480e-03, 
-7.366508e-04, -5.769038e-04,  3.630034e-04,  1.251059e-03,  1.142350e-03, 
 4.708420e-04,  4.623000e-05, -2.552677e-04, -2.228557e-04,  3.127529e-04, 
 1.508926e-04,  1.137846e-04, -1.080763e-04, -1.847107e-04,  4.679161e-04}, 

{2.652096e+11,  1.950266e+10,  1.434163e+09,  4.448675e+11,  3.271412e+10, 
 7.462291e+11, -6.640592e+10, -4.883277e+09, -1.113905e+11,  1.662740e+10, 
 3.163427e+11,  2.326282e+10,  5.306391e+11, -7.920915e+10,  3.773343e+11, 
-2.204812e+11, -1.621348e+10, -3.698393e+11,  5.520637e+10, -2.629905e+11, 
 1.832964e+11, -2.091890e+11, -1.538309e+10, -3.508976e+11,  5.237892e+10, 
-2.495212e+11,  1.739087e+11,  1.650018e+11, -3.524437e+11, -2.591757e+10, 
-5.911957e+11,  8.824850e+10, -4.203957e+11,  2.930030e+11,  2.779966e+11, 
 4.683713e+11, -2.280547e+11, -1.677041e+10, -3.825433e+11,  5.710271e+10, 
-2.720243e+11,  1.895926e+11,  1.798825e+11,  3.030677e+11,  1.961051e+11, 
 5.154697e+10,  3.790598e+09,  8.646585e+10, -1.290686e+10,  6.148535e+10, 
-4.285342e+10, -4.065864e+10, -6.850206e+10, -4.432543e+10,  1.001883e+10}, 

{8.286860e+10, -3.393511e+10,  1.389660e+10,  1.951882e+10, -7.993055e+09, 
 4.597451e+09,  4.500985e+10, -1.843176e+10,  1.060159e+10,  2.444698e+10, 
 1.139463e+10, -4.666158e+09,  2.683884e+09,  6.188963e+09,  1.566789e+09, 
-7.014262e+10,  2.872375e+10, -1.652135e+10, -3.809777e+10, -9.644778e+09, 
 5.937095e+10, -1.847518e+10,  7.565679e+09, -4.351633e+09, -1.003474e+10, 
-2.540382e+09,  1.563798e+10,  4.118959e+09, -2.555338e+08,  1.046424e+08, 
-6.018828e+07, -1.387925e+08, -3.513651e+07,  2.162920e+08,  5.697011e+07, 
 7.879647e+05,  3.421372e+10, -1.401069e+10,  8.058678e+09,  1.858309e+10, 
 4.704468e+09, -2.895958e+10, -7.627794e+09, -1.055015e+08,  1.412572e+10, 
-1.427836e+10,  5.847060e+09, -3.363116e+09, -7.755252e+09, -1.963309e+09, 
 1.208566e+10,  3.183296e+09,  4.402878e+07, -5.895065e+09,  2.460179e+09}, 
};

static double vcvsqrtdet = 4.61350068805394230e+06;
 

static double initialvcv[DEFVCVFEATS*(DEFVCVFEATS+1)/2] = {
	3.4274257416e-03, -6.0045810178e-05,  3.0429112325e-04, -1.1494549810e-04,
	1.0761568254e-04,  1.2691777508e-03,  6.4990176802e-05, -1.9980293074e-06, 
 	8.0546654951e-05,  3.2165043510e-04, -1.1177290345e-04,  7.7210047813e-06, 
 	3.7970403855e-04,  5.8860524539e-06,  6.8039199236e-04, -8.0141835458e-05,
	2.3930937445e-06, -4.6486304183e-06, -2.6425459960e-04,  7.2014820616e-05,  
 	4.7456401648e-04, -4.7024565712e-05,  1.4854529533e-05,  2.6460430398e-04, 
   -3.9263082221e-06,  1.5517348386e-04,  3.1147793049e-05,  4.9289640160e-04,
   -5.3040920765e-05, -8.7873986152e-05, -7.3551684149e-05,  1.6990724134e-05,
   -1.6940941765e-04,  1.1134091338e-05,  2.4284176748e-06,  3.9205465664e-04, 
 	2.1431067613e-05, -1.3664113973e-05, -9.0201536423e-06, -2.2391820925e-05,
   -2.0441512084e-05,  2.9469552950e-07, -1.1229470545e-04,  2.9349290176e-05, 
 	2.5926481639e-04, -3.5760255349e-04, -8.2877497000e-06, -1.8487987060e-04, 
   -4.8179072399e-05,  3.0154628793e-05,  3.2272577607e-05,  3.4331075806e-05,
	2.9716915723e-05, -1.2415210347e-05,  3.7671108905e-04 
};

/* ------------------------------------*/
//nc = MAXCLASS;
//*nclasses = nc - 1;
*nclasses = DEFAULT_NUMOFCLASSES;
*nfeatures = DEFVCVFEATS;

create_class_params( *nclasses, *nfeatures);						//SN28Apr99

//for(i = 0; i < MAXDIM; i++) 
for(i = 0; i < *nfeatures; i++) 
	{
	sfeatvec[i] = 0;
	}

//for(i = 0; i < MAXCLASS; i++) 
for(i = 0; i < *nclasses; i++) 
	{
	conversionf[i] = 0.0;
	clinvvarsqrtdet[i] = 0.0;
	}

//for(i = 0; i < MAXCLASS; i++) 
for(i = 0; i < *nclasses; i++) 
	{
//	for(j = 0; j < MAXDIM; j++)
	for(j = 0; j < *nfeatures; j++)
		{
		iclassmeans[i][j] = 0;
		iclassinvvars[i][j] = 0;
		}
	}
	
for(i = 0; i < *nfeatures; i++) 
	{
	sfeatvec[i] = featids[i];

	}

for(i = 0; i < *nfeatures; i++) 
	{
	for(j = 0; j < *nclasses; j++)
		{
		featmean[j][i] = vcvfeatm[i][j];
		featsd[j][i] = vcvfeatsd[i][j];
		}
	}
	
/*
 * per class inverted variances. Add log of prior probability
 * for computational efficiency
 */
for (i = 0; i < *nclasses; i++)
	{
	clinvvarsqrtdet[i] = lpp[i] + logsqrtdet[i];
	}
	
/*
 * class integer means
 */
for (i = 0; i < *nclasses; i++)
	for (j = 0; j < *nfeatures; j++)
		iclassmeans[i][j] = featmean[i][j];
/*
 * per class integer inverted variances
 */
for (i = 0; i < *nclasses; i++) {
	conversionf[i] = scalef[i];
	for (j = 0; j < *nfeatures; j++)
		iclassinvvars[i][j] = featsd[i][j];
	}
classifier_format = OLD_C;

/*
 * vcv
 */
for (i=0; i< *nclasses; i++)
	for (j=0; j < *nfeatures; j++)
		classmeans[i][j] = (double)vcvmeans[i][j];

for (i=0; i<(DEFVCVFEATS*(DEFVCVFEATS+1)/2); i++)
	vcv[i] = initialvcv[i];

max_sqrt_det = -100000000.0;
for (i=0; i< *nclasses; i++){
	clsqrtdet[i] = -log(vcvclsqrt[i]);
	if (clsqrtdet[i] > max_sqrt_det)
		max_sqrt_det = clsqrtdet[i];
	for(j=0; j<(DEFVCVFEATS*(DEFVCVFEATS+1)/2); j++)
		clvcv[i][j] = vcvclvcv[i][j];
}

sqrtdet = vcvsqrtdet;

initmahalanobis(DEFVCVFEATS);

#ifdef DEBUG
_ftprintf(fp," DEFAULT PARAMETERS \n\n");
_ftprintf(fp," no. of class = %d",*nclasses);
_ftprintf(fp," no. of features = %d",*nfeatures);
_ftprintf(fp," no. of features used = %d\n\n",*ndims);

_ftprintf(fp," selected feature numbers ");
for(i = 0; i < *nfeatures; i++)
	_ftprintf(fp,"%3d",sfeatvec[i]);

_ftprintf(fp,"\n\n\t iclassmeans[i][j] \t class mean feature vectors \n");
for (i = 0; i < *nclasses; i++)
	{
	_ftprintf(fp,"\n class %d ",i);
	for (j = 0; j < *nfeatures; j++) 
		{
		_ftprintf(fp,"%5d",iclassmeans[i][j]);
		}
	}
	
_ftprintf(fp,"\n\n\t iclassinvvars[i][j] \t class variances \n");
for (i = 0; i < *nclasses; i++)
	{
	_ftprintf(fp,"\n class %d ",i);
	for (j = 0; j < *nfeatures; j++) 
		{
		_ftprintf(fp,"%5d",iclassinvvars[i][j]);
		}
	}

_ftprintf(fp,"\n\n class  prior probability \t clinvvarsqrtdet[i] \t conversionf[i]");						
for (i = 0; i < *nclasses; i++)
_ftprintf(fp,"\n  %d \t %f \t\t %f \t\t %f",i,lpp[i],clinvvarsqrtdet[i],conversionf[i]);						
#endif /* DEBUG */
}



