////////////////////////////////////////////////////////////////////////////////////////
// Woolz Interface Class 
//
// Written By karl Ratcliff 22082001
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
// MODS:
//	23Jun2002	JMB	Images allocated with 'new', in both versions of
//					CWoolzIP::WoolzSplitMask(), are now initialised with zeros
//					after creation.

#include "StdAfx.h"
#include <math.h>
#include <float.h>

//#include "HRTimer.h"
//#include "vcmemrect.h"
//#include "uts.h"
//#include "vcutsfile.h"


#undef _DO_NOT_DEFINE_FREE
#include "woolz.h"
#include "woolzif.h"

#define MIN(x,y) (x < y ? x : y)
#define MAX(x,y) (x > y ? x : y)
#define GETXY(x, y)		((dimX * (y)) + (x))

#define ONE_BIT_PER_PIXEL               1
#define EIGHT_BITS_PER_PIXEL            8
#define NBINS_EIGHT						256
#define TWENTYFOUR_BITS_PER_PIXEL       24
#define THIRTYTWO_BITS_PER_PIXEL        32
#define TRACE1(x)

/*
 * "directions" encodes the 16 possible directions of a vector between
 * two next-but-adjacent points in the boundary, on the basis of the
 * difference in their line and column coordinates.
 * Note that if the inner square is used (will be a very rare
 * configuration, we hope) then the curvature coded in curv() below
 * will not be suitable.  I intend to ignore this on the grounds
 * that it will be a very rare event, and the error will be small.
 */
#define DIRECTION(i,j)	directions[i+2][j+2]
static int directions[5][5] = {
	{14,15, 0, 1, 2},
	{13,14, 0, 2, 3},
	{12,12,-1, 4, 4},
	{11,10, 8, 6, 5},
	{10, 9, 8, 7, 6}
};

/*
 * CURVATURE encodes the curvature between back and forward two-point
 * vectors from point k.  It is coded as 10*(degrees per unit length),
 * and is computed by a program whose source is ../aux/curvat.f
 */
static int CURVATURE[4][16] = {
	{0,51,93,153,225,255,280,357,450,-357,-280,-255,-225,-153,-93,-51},
	{-51,0,43,93,153,186,215,280,357,373,-300,-280,-255,-186,-129,-93},
	{-93,-43,0,43,93,129,159,215,280,300,318,-300,-280,-215,-159,-129},
	{-153,-93,-43,0,51,93,129,186,255,280,300,373,-357,-280,-215,-186}
};


////////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
////////////////////////////////////////////////////////////////////////////////////////

CWoolzIP::CWoolzIP()
{
    int i;
	m_pMeasuredObject = 0;
	m_pMaskObjectCreated = 0;
	m_pObjectGreys = 0;

    // Allocate some space for an object list
    m_pObjectList = (object **) new object[MAX_WOOLZ_OBJECTS];

    // Allocate some space for the labels
    m_pLabelList = new long[MAX_WOOLZ_OBJECTS];

    // Go through and make sure all elements are NULL
    for (i = 0; i < MAX_WOOLZ_OBJECTS; i++)
    {
        m_pObjectList[i] = NULL;
        m_pLabelList[i] = 0;
    }

    // No objects currently found
    m_nNumberObjects = 0;

    // No merged obects
    m_pMergeTable = NULL;

}

CWoolzIP::~CWoolzIP()
{
    // Clear up 
    if (m_pObjectList)
    {
        FreeObjects();              // Clear objects in the list
        delete [] m_pObjectList;    // Delete the list itself
    }

    if (m_pLabelList)
        delete [] m_pLabelList;

    KillMergedObjects();            // Tidy up any merged objects
}

////////////////////////////////////////////////////////////////////////////////////////
// Initialise the woolz interface
// Returns FALSE if not successful
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::Initialise(void)
{
    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////
// Initialise the woolz interface
// Returns FALSE if not successful
////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::KillMergedObjects(void)
{
    MergeObj *pM;

	std::list<MergeObj*>::iterator P = m_MergeObjects.begin();
    while (P != m_MergeObjects.end())
    {
        pM = *P;
        delete pM;
        P++;
    }

    m_MergeObjects.clear();

    if (m_pMergeTable)
    {
        delete m_pMergeTable;
        m_pMergeTable = NULL;
    }

    if (m_pMeasuredObject)
    {
        delete m_pMeasuredObject;
        m_pMeasuredObject = NULL;
    }

    if (m_pMaskObjectCreated)
    {
        delete m_pMaskObjectCreated;
        m_pMaskObjectCreated = NULL;
    }

    if (m_pObjectGreys)
    {
        delete m_pObjectGreys;
        m_pObjectGreys = NULL;
    }
}

////////////////////////////////////////////////////////////////////////////////////////
//	
//	automatic threshold determination by evaluation of grey level
//	gradients in the image, returns >=0 if o.k., UTS_ERROR otherwise
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::GradThresh2(BYTE *image, int cols, int lines)
{
    return gradthresh2(image, cols, lines);
}

////////////////////////////////////////////////////////////////////////////////////////
//
//	BackgroundThresh determines the threshold of an image by
//	first calculating the approximate background level in the image
//	and then analysing the greylevel gradients just above this level
//	i.e. it analyses the contours of the objects.
//  Returns >=0 if o.k., UTS_ERROR otherwise
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::BackgroundThreshold(BYTE *image, int cols, int lines)
{
    return BackgroundThresh(image, cols, lines);
}

////////////////////////////////////////////////////////////////////////////////////////
//
//		F S M I N	( erode grey image )
//
//	Inputs address of frame store, lines, cols , filter half width
//		frame - Pointer to begining of frame store
//		cols  - Number of pixels in a line
//		lines - Number of lines in a frame
//		halfwidth - Filter halfwidth i.e. 1 => 3x3, 2 => 5x5 etc
//
//	Outputs : new frame store containing result of NxN minimum filter
//		  where N = 2*halfwidth + 1
//
//	Uses local histogram ( held in rack[] ) to perform a very efficient
//	running calculation of the local minimum without sorting values.
////////////////////////////////////////////////////////////////////////////////////////
    
BYTE *CWoolzIP::FSMin(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsMin(image, cols, lines, filtsize);
}

////////////////////////////////////////////////////////////////////////////////////////
//
//		F S M A X	( dilate grey image )
//
//	Inputs address of frame store, lines, cols , filter half width
//			frame - Pointer to begining of frame store
//			cols  - Number of pixels in a line
//			lines - Number of lines in a frame
//			halfwidth - Filter halfwidth i.e. 1 => 3x3, 2 => 5x5 etc
//
//	Outputs : new frame store containing result of NxN maximum filter
//		  where N = 2*halfwidth + 1
//
//	Uses local histogram ( held in rack[] ) to perform a very efficient
//	running calculation of the local maximum without sorting values.
////////////////////////////////////////////////////////////////////////////////////////
 
BYTE *CWoolzIP::FSMax(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsMax(image, cols, lines, filtsize);
}

BYTE *CWoolzIP::FSOpen(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsOpen(image, cols, lines, filtsize);
}

BYTE *CWoolzIP::FSClose(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsClose(image, cols, lines, filtsize);
}

BYTE *CWoolzIP::FSTopHat(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsTophat(image, cols, lines, filtsize);
}

BYTE *CWoolzIP::FSMedian(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsMedian(image, cols, lines, filtsize);
}

BYTE *CWoolzIP::FSGaussian(BYTE *image, int cols, int lines, float radius)
{
    return (BYTE *) fsGaussian(image, cols, lines, radius);
}

BYTE *CWoolzIP::FSUnsharpMask(BYTE *image, int cols, int lines, float radius, float depth)
{
    return (BYTE *) fsUnsharpMask(image, cols, lines, radius, depth);
}

BYTE *CWoolzIP::FSExtract(BYTE *image, int cols, int lines, int filtsize, int noisefiltsize)
{
    return (BYTE *) fsExtract(image, cols, lines, filtsize, noisefiltsize);
}

long CWoolzIP::FSClahe(BYTE *image, int cols, int lines, int Min, int Max, int uiNrX, int uiNrY, int uiNrBins, int fCliplimit)
{
	return (long)fsClahe(image, cols, lines, Min, Max, uiNrX, uiNrY, uiNrBins, fCliplimit);
}

////////////////////////////////////////////////////////////////////////////////////////
// Variations of the above using alternate 8 and 4 neighbourhoods on different passes
////////////////////////////////////////////////////////////////////////////////////////
    
BYTE *CWoolzIP::FSMin84(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsMin84(image, cols, lines, filtsize);
}
 
BYTE *CWoolzIP::FSMax84(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsMax84(image, cols, lines, filtsize);
}

////////////////////////////////////////////////////////////////////////////////////////
// Opening and Closing ops with an 84 structuring element
////////////////////////////////////////////////////////////////////////////////////////

BYTE *CWoolzIP::FSOpen84(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsOpen84(image, cols, lines, filtsize);
}

BYTE *CWoolzIP::FSClose84(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsClose84(image, cols, lines, filtsize);
}
	
////////////////////////////////////////////////////////////////////////////////////////
// FSExtract with an 84 structuring element
////////////////////////////////////////////////////////////////////////////////////////

BYTE *CWoolzIP::FSExtract84(BYTE *image, int cols, int lines, int filtsize, int noisefiltsize)
{
    return (BYTE *) fsExtract84(image, cols, lines, filtsize, noisefiltsize);
}

#define MAXOBJ		100000

////////////////////////////////////////////////////////////////////////////////////////
// A Spot processing routine by MG, replaces image1 with a background subtracted version
////////////////////////////////////////////////////////////////////////////////////////

BYTE *CWoolzIP::FSSpotExtract(BYTE *image1, int width, int height, int StructSize, int MinArea)
{
	FSCHAR *image2, *im1ptr, *im2ptr;
	int i, min, diff, thresh;

	if (!image1)
		return NULL;
    
	// find background using large structure element
	image2 = FSOpen84(image1, width, height, StructSize);

	im1ptr = image1;
	im2ptr = image2;

	for (i=0; i< width * height; i++)
	{
		min = (*im2ptr < *im1ptr) ? *im2ptr : *im1ptr;
		diff = *im1ptr - min;

		//thresh = *im1ptr >> 2; // threshold is image value/4 i.e. 25%
		thresh = *im2ptr >> 3; // threshold is image value/4 i.e. 12.5%
		if (thresh < 8)
			thresh = 8;

		if (diff > thresh)
			*im2ptr = diff;
		else
			*im2ptr = 0;

		im1ptr++;
		im2ptr++;
	}


	// Store the background subtracted image back in image 1
	memcpy(image1, image2, width * height);

	// search for objects
	struct object *bigobj, *objlist[MAXOBJ];
	int n;

	bigobj=(struct object *)fsconstruct(image2, height, width, 1, 0);

	for (i = 0; i < MAXOBJ; i++)
		objlist[i]=NULL;

	int retobjs = label_area(bigobj, &n, objlist, MAXOBJ, MinArea);						//minimum area of 5
	
    // If the return value from label_area is zero then there may be zero objects !
    // Therefore check that the number of returned objects is not zero.
    if (retobjs == 0 && n != 0)
        return NULL;

	for (i = 0; i < n; i++)
		vdom21to1(objlist[i]);
	
	freeobj(bigobj);

	// clear the image2 
	im2ptr = image2;
	for (i = 0; i < width * height; i++)
		*im2ptr++ = 0;

	// draw thresholded spots into image2
	im2ptr = image2;

	for (i = 0; i < n; i++)
	{
		int thresh = Objgradthresh(objlist[i]);

		if (thresh >0)
			ThreshValues(objlist[i], im2ptr, width, height, thresh);
	}

	// dump all objects
	for (i = 0; i < n; i++) 
	{
		freeobj(objlist[i]);
		objlist[i]=NULL;
	}

	return image2;
}

////////////////////////////////////////////////////////////////////////////////////////
//--------------------------------------------------------------------------------------
// PathVysion Image processing routines
//--------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////
//	clear data under idom in an image for a given object with certain value
    
void CWoolzIP::clear_values(struct object *obj, FSCHAR *image, int image_width, int image_height, int value)
{
    FSCHAR *imptr, *imend;
    struct iwspace iwsp;
    struct gwspace gwsp;
    register int k;

    if (wzcheckobj(obj)) {
    	fprintf(stderr,"Bad obj sent to clear_values\n");
    	return;
    }

    if (wzemptyidom(obj->idom)) {
    	fprintf(stderr, "Empty idom sent to clear_values\n");
    	return;
    }

    if (obj->vdom == NULL) {
    	fprintf(stderr, "Null vdom sent to clear_values\n");
    	return;
    }

    if (obj->vdom->type != 1) {
    	fprintf(stderr, "vdom type = %d\n",obj->vdom->type);
    	if (obj->vdom->type == 21)
    		vdom21to1(obj);
    	else
    		return;
    }
    	
    imend=image + image_width * (image_height - 1);
    	
    initgreyscan(obj,&iwsp,&gwsp);
    while (nextgreyinterval(&iwsp) == 0) {

    	imptr=imend - iwsp.linpos * image_width + iwsp.lftpos;

    	for (k=iwsp.lftpos; k<=iwsp.rgtpos; k++) {
    		*imptr=value;
    		imptr++;
    	}
    }

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PathVysion routines
// Process amplified probe images
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BYTE *CWoolzIP::PathVysion(BYTE *image, int width, int height, int SpotWidth, int ClusterWidth, int MinArea)
{
	FSCHAR *image1, *image2, *image3, *imptr1, *imptr2, *imptr3;
	int i;

	if ((!image) || (width<3) || (height<3) || (SpotWidth < 2) || (ClusterWidth < 2))
		return NULL;

	imptr1 = image1 = image;

	imptr2 = image2 = fsExtract(image1, width, height,SpotWidth/2,2);

	imptr3 = image3 = fsExtract(image1, width, height,ClusterWidth/2,2);

	for (i=0; i<width * height; i++)
	{
		int val =0;

		if (*imptr2 > 16)
			val=*imptr2;

		if (*imptr3 > 64)
			val=*imptr3;

		*imptr1++ = val;		// Store background subtracted result in image1

		imptr2++;
		imptr3++;
	}
	free(image3);

	// search for objects
	struct object *bigobj, *objlist[MAX_WOOLZ_OBJECTS];
	int n;

	bigobj=(struct object *)fsconstruct(image1, height, width, 1, 0);

	for (i=0;i<MAX_WOOLZ_OBJECTS;i++)
		objlist[i]=NULL;

   	label_area(bigobj,&n,objlist, MAX_WOOLZ_OBJECTS, MinArea);						//minimum spot area
	for (i = 0; i < n; i++)
		vdom21to1(objlist[i]);
	
	freeobj(bigobj);

	// clear the image2 
	imptr2 = image2;
	for (i=0; i<width * height; i++)
		*imptr2++ = 0;

	// draw thresholded spots into image2
	imptr2 = image2;

	for (i = 0; i < n; i++)
	{
		int thresh = Objgradthresh(objlist[i]);

		if (thresh >0)
			ThreshValues(objlist[i], imptr2, width, height, thresh);
	}

	// dump all objects
	for (i = 0; i < n; i++) 
	{
		freeobj(objlist[i]);
		objlist[i]=NULL;
	}

	return image2;

}
//--------------------------------------------------------------------------------------


/****************************************************************** number_of_tips */
/*
 *	Analyse the curvature of an object and count the number of major peaks
 *	This should roughly represent the number of visible chromosome tips.
 *	If troughs=1 it looks for troughs instead, which are potential cut points.
 */

int CWoolzIP::number_of_features(struct object *obj, int troughs)
{
	struct object *bobj, *uobj;
	struct object *h;
	struct histogramdomain *hdom;
	struct polygondomain *pdom;
	int *pt;
	int i, n, start, end, mid, wrap, feature, nfeatures;
	int threshold;

	if (troughs)
		threshold=5;		/* trough threshold */
	else
		threshold = -5;	/* peak threshold */

	nfeatures=0;
	
	if (obj == NULL)
		return(0);

	if (obj->type != 1)
		return(0);

	bobj = ibound(obj,1);
	if (bobj == NULL)
		return(0);

	uobj= unitbound(bobj);
	if (uobj == NULL) {
		freeobj(bobj);
		return(0);
	}

	pdom = (struct polygondomain *)uobj->idom;
	h = curvat(uobj);
	if (h==NULL) {
		freeobj(bobj);
		freeobj(uobj);
		return(0);
	}

	curvsmooth2(h,10);

	hdom=(struct histogramdomain *)h->idom;
	n=hdom->npoints;
	pt=hdom->hv;

	start=0;
	end=0;

	wrap=20;
	if (wrap>n/2)
		wrap=n/2;

	/* scan for features - wrap round to catch end feature */
	for (i=0; i<n+wrap; i++) {

		if (troughs)
			feature=(*pt >= threshold);
		else
			feature=(*pt <= threshold);

		if (feature) {
			if (start>0)
				end=i;
			else
				start=i;
		}
		else {
			if ((end >0) && (start>1)) {

				if (end-start>5) {
					mid=(end+start)/2;

					if (mid > n-1)
						mid=mid-n;

					nfeatures++;

				}						
			}

			start=0;
			end=0;
					
		}
		if (i==n-1)
			pt=hdom->hv;
		else
			pt++;
	}

	freeobj(h);
	freeobj(uobj);
	freeobj(bobj);

	return(nfeatures);
}


/****************************************************************** feature_pos */
/*
 *	Same operation as number_of_features routine above
 *	but returns the positions of the features in a point list.
 */
#define MAXSPLITS 50

int CWoolzIP::feature_pos(struct object *obj, int troughs, struct ipoint **pointlist, int **position, int *npoints)
{
	struct object *bobj, *uobj;
	struct object *h;
	struct histogramdomain *hdom;
	struct polygondomain *pdom;
	int *pt;
	int i, n, start, end, mid, wrap, feature, nfeatures;
	int threshold;


	if (troughs)
		threshold=5;		/* trough threshold */
	else
		threshold = -5;	/* peak threshold */

	nfeatures=0;
	
	if (obj == NULL)
		return(0);

	if (obj->type != 1)
		return(0);

	/* malloc space for point list
	 - pointers to positions of possible features */
	*pointlist=(struct ipoint *)Malloc(sizeof(struct ipoint)*MAXSPLITS);
	*position = (int *)Malloc(sizeof(int)*MAXSPLITS);

	if (*pointlist == NULL)
		return(0);


	bobj = ibound(obj,1);
	if (bobj == NULL)
		return(0);


	uobj= unitbound(bobj);
	if (uobj == NULL) {
		freeobj(bobj);
		return(0);
	}

	pdom = (struct polygondomain *)uobj->idom;
	h = curvat(uobj);
	if (h==NULL) {
		freeobj(bobj);
		freeobj(uobj);
		return(0);
	}

	curvsmooth2(h,10);


	hdom=(struct histogramdomain *)h->idom;
	n=hdom->npoints;
	pt=hdom->hv;

	start=0;
	end=0;

	wrap=20;
	if (wrap>n/2)
		wrap=n/2;

	// return number of points
	*npoints = n;

	/* scan for features - wrap round to catch end feature */
	for (i=0; i<n+wrap; i++) {

		if (troughs)
			feature=(*pt >= threshold);
		else
			feature=(*pt <= threshold);

		if (feature) {
			if (start>0)
				end=i;
			else
				start=i;
		}
		else {
			if ((end >0) && (start>1)) {

				if (end-start>5) {
					mid=(end+start)/2;

					if (mid > n-1)
						mid=mid-n;

					(*position)[nfeatures] = mid;

					(*pointlist)[nfeatures].type=40;
					(*pointlist)[nfeatures].k=pdom->vtx[mid].vtX;
					(*pointlist)[nfeatures].l=pdom->vtx[mid].vtY;
					(*pointlist)[nfeatures].style=1;	
							
					nfeatures++;

				}						
			}

			start=0;
			end=0;
					
		}
		if (i==n-1)
			pt=hdom->hv;
		else
			pt++;

		if (nfeatures == MAXSPLITS)
			break;
	}

	/* check if any feature point positions to return */
	if (nfeatures==0)
	{
		Free(*pointlist);
		*pointlist = NULL;

		Free(*position);
		*position = NULL;

		*npoints=NULL;
	}

	freeobj(h);
	freeobj(uobj);
	freeobj(bobj);

	return(nfeatures);
}

//--------------------------------------------------------------------------------------
// binary closing on object

struct object *CWoolzIP::closeObj(struct object *obj, int passes)
{
	struct object *dobj, *nobj;
	int i;

	if ((obj == NULL) || (passes < 1))
		return obj;

	dobj=obj;

	for (i=0; i<passes; i++) {
		nobj = dobj;

		/* alternate dilation structuring element */
		if (i & 1)
			dobj = dilation(nobj);
		else
			dobj = dilation4(nobj);

		if (i>0)
			freeobj(nobj);
	}

	for (i=0; i<passes; i++) {
		nobj = dobj;

		/* alternate dilation structuring element */
		if (i & 1)
			dobj = erosion(nobj);
		else
			dobj = erosion4(nobj);

		freeobj(nobj);
	}

	return dobj;
}

//--------------------------------------------------------------------------------------

void CWoolzIP::splitObjects(FSCHAR *image, int width, int height, int thresh)
{
	FSCHAR *imptr;

	// search image for objects
	struct object *bigobj, *objlist[5000];
	int n, i;

    bigobj=(struct object *)fsconstruct(image,height,width,thresh,0);

    for (i=0;i<5000;i++)
    	objlist[i]=NULL;

    label_area(bigobj,&n,objlist,5000,100);									//minimum area of 100, max no objects is 5000
    for (i=0;i<n;i++)
    	vdom21to1(objlist[i]);
   
    freeobj(bigobj);

	// now transfer all objects which are no larger than a certain size and smoothness to
	// the keeplist - and remove them from the binary image 
	struct ipoint *pointlist=NULL;
	int *position=NULL, npoints;
	int dist, distx, disty, x, y, count, sep1, sep2;
	float xinc, yinc, fx, fy;

	for (i=0;i<n;i++)
	{
		// search for troughs in objects and locate their positions on boundary of object
		position=NULL;
		pointlist=NULL;
		npoints=0;

		int features = feature_pos(objlist[i], 1, &pointlist, &position, &npoints);

		// try to split complex objects - but only work on ones with a few features
		// - do splitting directly on blobs in image2, we will re find them again later
		if ((features > 1) && (features < 50))
		{
			// find two points which are close enough to each other to split the objects
			for (int feat1=0; feat1<features-1; feat1++)
			{
				for (int feat2=1; feat2<features; feat2++)
				{
					// first determine seperation around boundary of object
					sep1 = position[feat2] - position[feat1];
					sep2 = npoints -  position[feat2] + position[feat1];

					// ignore if points are too close to each other
					if ((sep1 < 30) || (sep2 < 30))
						continue;

					// then check linear distance between points
					distx = pointlist[feat2].k - pointlist[feat1].k;
					disty = pointlist[feat2].l - pointlist[feat1].l;
					dist = distx*distx + disty*disty;

					// split object if dist between points across object is less than 20;
					if (dist > 400)
						continue;

					// draw splitline through blob
					if (abs(distx) > abs(disty))
					{
                        y = pointlist[feat1].l;
						fy = (float)y;

						xinc = (float)distx / (float)abs(distx);
						yinc = (float)disty / (float)abs(distx);
						count=0;
						for (x=pointlist[feat1].k; count<=abs(distx); x+=(int)xinc)
						{
							imptr = image + x + width*(height - y);

							*imptr = 0;
							if ((x>1) && (x<width-1) && (y>1) && (y<height-1))
							{
								// nice fat line
								imptr--; *imptr=0;
								imptr-=width; *imptr++=0;
								*imptr++=0;
								*imptr=0; imptr+=width; 
								*imptr=0; imptr+=width;
								*imptr--=0;
							}	*imptr--=0;

							fy+= yinc;
							y=(int)fy;
							count++;
						}
					}
					else
					{
                        x = pointlist[feat1].k;
						fx = (float)x;
						
						if (disty == 0)
							yinc = 0;
						else
							yinc = disty / abs(disty);

						xinc = (float)distx / abs(disty);
						count = 0;
						for (y=pointlist[feat1].l;  count<=abs(disty); y+=yinc)
						{
							imptr = image + x + width*(height - y);

							*imptr = 0;
							if ((x>1) && (x<width-1) && (y>1) && (y<height-1))
							{
								// nice fat line
								imptr--; *imptr=0;
								imptr-=width; *imptr++=0;
								*imptr++=0;
								*imptr=0; imptr+=width; 
								*imptr=0; imptr+=width;
								*imptr--=0;
							}	*imptr--=0;

							fx+= xinc;
							x=(int)fx;
							count++;
						}
					}

				}
			}
		}

		if (pointlist)
		{
			Free(pointlist);
			pointlist=NULL;
		}


		if (position)
		{
			Free(position);
			position=NULL;
		}
	}

	// dump objects
    for (i=0;i<n;i++) {
    	freeobj(objlist[i]);
    	objlist[i]=NULL;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Routine to segment binary mask of cells from Tissue Image
////////////////////////////////////////////////////////////////////////////////////////////////

BYTE *CWoolzIP::PathOlogical(BYTE *image, int width, int height, int CellWidth)
{
	FSCHAR *image1, *image2, *image3, *imptr1, *imptr2, *imptr3;
	int i;

	if ((!image) || (width <3) || (height<3) || (CellWidth < 2) || (CellWidth > width) || (CellWidth > height))
		return NULL;

	imptr1 = image1 = image;

	imptr2 = image2 = fsExtract(image1, width, height, CellWidth * 3, 2);	// ***** subtract BG 

	if (!image2)
		return NULL;

	imptr3 = image3 = fsOpen84(image2, width, height, CellWidth / 2);		// ***** open using cell halfwidth

	for (i=0; i<width * height; i++)										// invert image 3 into image 2
		*imptr2++ = 255 - *imptr3++;

	imptr2 = fsExtract(image2, width, height,2,2);							// subtract BG to find troughs between cells
	free(image2);
	image2 = imptr2;

	for (i=0; i<width * height; i++)										// thresold with a value of 1 *** image2 is troughs image ***
		*imptr2++ = (*imptr2 > 1) ? 255 : 0;


	int thresh=BackgroundThresh(image3, width, height);						// automatic threshold determination
																
	imptr3 = image3;
	for (i=0; i<width * height; i++)										// *** image3 is thresholded blob image ***
		*imptr3++ = (*imptr3 > thresh) ? 255 : 0;

	// subtract troughs image from blobs image and store in image3
	imptr2 = image2;
	imptr3 = image3;
	for (i=0; i<width * height; i++)
	{
		if (*imptr3 != 0)
			*imptr3 -= *imptr2;

		imptr2++;
		imptr3++;
	}

	// open 5 to break small attachments - result in image2
	free(image2);
	image2 = fsOpen84(image3, width, height, 5);				// ***** 5


	// split objects by looking for pinched-in regions
	splitObjects(image2, width, height, thresh);


	// search for objects again - now they have been split
	struct object *bigobj, *objlist[5000];
	int n;

	bigobj=(struct object *)fsconstruct(image2,height,width,thresh,0);

    for (i=0;i<5000;i++)
    	objlist[i]=NULL;

    label_area(bigobj,&n,objlist,5000,100);									//***** minimum area of 100
    for (i=0;i<n;i++)
    	vdom21to1(objlist[i]);
   
    freeobj(bigobj);

	// clear the final image 
	imptr2 = image2;
	for (i=0; i<width * height; i++)
		*imptr2++ = 0;

	// look for only single cell objects
	for (i=0;i<n;i++)
	{
		// count troughs in objects 
		int features = number_of_features(objlist[i], 1);

		// keep only smooth objects - draw them into final mask image
		if (features < 2)
		{	
			struct object *fillobj = closeObj(objlist[i], 3);

			// requires a value table for clear_values
			fillobj->vdom = newvaluetb(fillobj,1,255);
    		fillobj->vdom->linkcount++;

			clear_values(fillobj, image2, width, height,255);

			freeobj(fillobj);
		}
	}

	// dump all objects
    for (i=0;i<n;i++) {
    	freeobj(objlist[i]);
    	objlist[i]=NULL;
    }


	// dump temporary images
	free(image3);

	return image2;	// *****  this image contains the cell mask  *****
}

//--------------------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////////////
// Woolz object stuff
////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
// Clears the current list of objects in memory
////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::FreeObjects(void)
{
    int i;

    // Reset the number of objects
    m_nNumberObjects = 0;
    for (i = 0; i < MAX_WOOLZ_OBJECTS; i++)
    {
        // If previously an object then free it
    	if (m_pObjectList[i])
        {
            freeobj(m_pObjectList[i]);
            m_pObjectList[i] = NULL;
        }
        // Reset the label anyway
        m_pLabelList[i] = 0;
    }
}

////////////////////////////////////////////////////////////////////////////////////////
// Find objects 
// Finds a series of Woolz objects and keeps a list internally. 
// Parameters:
//      *image is a pointer to an 8 bit image
//      cols is the number of columns in the image
//      lines is the number of rows in the image
//      Thresh is threhold for the image
//      MinArea is the minimum size of object to look for
// Returns:
//      The number of objects found
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::FindObjects(BYTE *image, int cols, int lines, int Thresh, int MinArea)
{
    int         i;

    m_nNumberObjects = 0;
    
    // search for objects 
	struct object *bigobj;

	bigobj=(struct object *) fsconstruct(image, lines, cols, Thresh, 0);

    // Clear current list of objects
    FreeObjects();

    label_area(bigobj, &m_nNumberObjects, m_pObjectList, MAX_WOOLZ_OBJECTS, MinArea);		
    for (i = 0; i < m_nNumberObjects; i++)
    	vdom21to1(m_pObjectList[i]);
   
    freeobj(bigobj);

    // Send back number of objects found
    return m_nNumberObjects;
}

////////////////////////////////////////////////////////////////////////////////////////
// Find objects Extended
// Finds a series of Woolz objects and keeps a list internally. 
// Parameters:
//      *image is a pointer to an 8 bit image
//      cols is the number of columns in the image
//      lines is the number of rows in the image
//      Thresh is threhold for the image
//      MinArea is the minimum size of object to look for
//      MaxArea is the maxumum size of object to look for
//      Border objects touching the border will be ignored
// Returns:
//      The number of objects found
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::FindObjectsEx(BYTE *image, int W, int H, int Thresh, int MinArea, int MaxArea, int BorderWidth)
{
    int     i, j;
    struct  object **TempObjects;
    int     A;
    int     N;
    struct  iwspace iwsp;
    struct  gwspace gwsp;
    long    Xmin, Xmax, Ymin, Ymax;

    m_nNumberObjects = 0;
    
    // search for objects 
	struct object *bigobj;

	bigobj=(struct object *) fsconstruct(image, H, W, Thresh, 0);

    // Clear current list of objects
    FreeObjects();

    // Find objects >= MinArea
    label_area(bigobj, &N, m_pObjectList, MAX_WOOLZ_OBJECTS, MinArea);	

    // Go through and reject those objects that are too big
    TempObjects = (object **) new object[N];
    for (i = 0; i < N; i++)
    {
    	vdom21to1(m_pObjectList[i]);
        A = area(m_pObjectList[i]);
        // Under the max size limit
        if (A <= MaxArea)
        {
   	        Xmin = LONG_MAX;
	        Ymin = LONG_MAX;
	        Xmax = LONG_MIN;
	        Ymax = LONG_MIN;

            // Determine the bounding box for this object
            initgreyscan(m_pObjectList[i], &iwsp, &gwsp);
            while (nextgreyinterval(&iwsp) == 0) 
            {
		        if (iwsp.linpos < Ymin) Ymin = iwsp.linpos;
		        if (iwsp.linpos > Ymax) Ymax = iwsp.linpos;
		        if (iwsp.lftpos < Xmin) Xmin = iwsp.lftpos;
		        if (iwsp.rgtpos > Xmax) Xmax = iwsp.rgtpos;
            }
            
            // Keep objects not touching the border
            if ((Ymin > BorderWidth) && (Xmin > BorderWidth) && (Xmax < (W - BorderWidth)) && (Ymax < (H - BorderWidth)))
                TempObjects[i] = m_pObjectList[i];
            else
            {
                freeobj(m_pObjectList[i]);
                TempObjects[i] = NULL;
            }
        }
        else
        {
            freeobj(m_pObjectList[i]);
            TempObjects[i] = NULL;
        }
    }

    for (i = 0; i < N; i++)
        m_pObjectList[i] = NULL;

    j = 0;
    // Copy the object pointers back into the object array
    for (i = 0; i < N; i++)
    {
        if (TempObjects[i])
            m_pObjectList[j++] = TempObjects[i];
    }

    delete TempObjects;

    freeobj(bigobj);

    m_nNumberObjects = j;

    // Send back number of objects found
    return m_nNumberObjects;
}

////////////////////////////////////////////////////////////////////////////////////////
// Find objects and merge them
// Finds a series of Woolz objects and keeps a list internally. 
// Parameters:
//      *image is a pointer to an 8 bit image
//      cols is the number of columns in the image
//      lines is the number of rows in the image
//      Thresh is threhold for the image
//      AbsMin is the absolute min size of object to merge
//      MinArea is the minimum size of object to look for (after merge)
//      MaxArea is the maxumum size of object to look for
//      Border objects touching the border will be ignored
//      MD is the merge distance 
// Returns:
//      The number of objects found
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::FindObjectsAndMerge(BYTE *image, int W, int H, int Thresh, int AbsMin, int MinArea, int MaxArea, int BorderWidth, int MD)
{
    int     i, j;
    struct  object **TempObjects;
    int     N;
    struct  iwspace iwsp;
    struct  gwspace gwsp;
    long    *Xmin, *Xmax, *Ymin, *Ymax, *Area;
    BOOL    AlreadyMerged, *Merged;
    MergeObj *pM;

    m_nNumberObjects = 0;
    
    // search for objects 
	struct object *bigobj;

	bigobj=(struct object *) fsconstruct(image, H, W, Thresh, 0);

    // Clear current list of objects
    FreeObjects();

    // Find objects >= MinArea
    label_area(bigobj, &N, m_pObjectList, MAX_WOOLZ_OBJECTS, AbsMin);	

    // Go through and reject those objects that are too big
    TempObjects = (object **) new object[N];
    Xmin = new long[N];
    Xmax = new long[N];
    Ymin = new long[N];
    Ymax = new long[N];
    Area = new long[N];

    for (i = 0; i < N; i++)
    {
    	vdom21to1(m_pObjectList[i]);

        Xmin[i] = LONG_MAX;
        Ymin[i] = LONG_MAX;
        Xmax[i] = LONG_MIN;
        Ymax[i] = LONG_MIN;

        // Get the area
        Area[i] = 0;

        // Determine the bounding box for this object
        initgreyscan(m_pObjectList[i], &iwsp, &gwsp);
        while (nextgreyinterval(&iwsp) == 0) 
        {
            if (iwsp.linpos < Ymin[i]) Ymin[i] = iwsp.linpos;
            if (iwsp.linpos > Ymax[i]) Ymax[i] = iwsp.linpos;
            if (iwsp.lftpos < Xmin[i]) Xmin[i] = iwsp.lftpos;
            if (iwsp.rgtpos > Xmax[i]) Xmax[i] = iwsp.rgtpos;
            Area[i] += iwsp.rgtpos - iwsp.lftpos;                   // Object area
        }

        if (Area[i] > MaxArea)                 // Out of area range stuff
        {
            freeobj(m_pObjectList[i]);
            TempObjects[i] = NULL;
        }
        else
        {
            // Keep objects not touching the border
            if ((Ymin[i] > BorderWidth) && (Xmin[i] > BorderWidth) && (Xmax[i] < (W - BorderWidth)) && (Ymax[i] < (H - BorderWidth)))
                TempObjects[i] = m_pObjectList[i];
            else
            {
                freeobj(m_pObjectList[i]);
                TempObjects[i] = NULL;
            }
        }
    }

    for (i = 0; i < N; i++)
        m_pObjectList[i] = NULL;

    j = 0;
    // Copy the object pointers back into the object array removing NULL entries
    for (i = 0; i < N; i++)
    {
        if (TempObjects[i])
        {
            Xmin[j] = Xmin[i];
            Xmax[j] = Xmax[i];
            Ymin[j] = Ymin[i];
            Ymax[j] = Ymax[i];
            Area[j] = Area[i];
            m_pObjectList[j++] = TempObjects[i];
        }
    }

    N = j;

    // Now produce a list of merge objects
    KillMergedObjects();
    Merged = new BOOL[N * N];
    for (i = 0; i < (N * N); i++) Merged[i] = FALSE;
    m_pMergeTable = new BOOL[N];
    m_pMeasuredObject = new BOOL[N];
    m_pMaskObjectCreated = new BOOL[N];
    m_pObjectGreys = new BOOL[N];

    for (i = 0; i < N; i++)
    {
        m_pMergeTable[i] = FALSE;
        m_pMeasuredObject[i] = FALSE;
        m_pMaskObjectCreated[i] = FALSE;
        m_pObjectGreys[i] = FALSE;
    }

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            // Don't use the same object and don't use objects touching the border
            if (i != j && m_pObjectList[i] && m_pObjectList[j])
            {
                // Have we already merged the objects ?
                AlreadyMerged = Merged[i * N + j] || Merged[j * N + i];
                // No
                if (!AlreadyMerged)
                {
                    // Check the merge conditions - this is based on bounding boxes overlapping or within
                    // Merge dist of one another
                    if (Xmin[i] > Xmin[j] - MD && Xmin[i] < Xmax[j] + MD && Ymin[i] > Ymin[j] - MD && Ymin[i] < Ymax[j] + MD)
                    {
                        Merged[i * N + j] = TRUE;
                        pM = new MergeObj;
                        pM->ObjI = i;
                        pM->ObjJ = j;
                        pM->Measured = FALSE;
                        pM->GMeasured = FALSE;
                        pM->MaskCreated = FALSE;
                        m_MergeObjects.push_back(pM);
                        m_pMergeTable[i] = TRUE;
                        m_pMergeTable[j] = TRUE;
                    }
                    else
                    if (Xmin[i] > Xmin[j] - MD && Xmin[i] < Xmax[j] + MD && Ymax[i] > Ymin[j] - MD && Ymax[i] < Ymax[j] + MD)
                    {
                        Merged[i * N + j] = TRUE;
                        pM = new MergeObj;
                        pM->ObjI = i;
                        pM->ObjJ = j;
                        pM->Measured = FALSE;
                        pM->GMeasured = FALSE;
                        pM->MaskCreated = FALSE;
                        m_MergeObjects.push_back(pM);
                        m_pMergeTable[i] = TRUE;
                        m_pMergeTable[j] = TRUE;
                    }
                    else
                    if (Xmax[i] > Xmin[j] - MD && Xmax[i] < Xmax[j] + MD && Ymin[i] > Ymin[j] - MD && Ymin[i] < Ymax[j] + MD)
                    {
                        Merged[i * N + j] = TRUE;
                        pM = new MergeObj;
                        pM->ObjI = i;
                        pM->ObjJ = j;
                        pM->Measured = FALSE;
                        pM->GMeasured = FALSE;
                        pM->MaskCreated = FALSE;
                        m_MergeObjects.push_front(pM);
                        m_pMergeTable[i] = TRUE;
                        m_pMergeTable[j] = TRUE;
                    }
                    else
                    if (Xmax[i] > Xmin[j] - MD && Xmax[i] < Xmax[j] + MD && Ymax[i] > Ymin[j] - MD && Ymax[i] < Ymax[j] + MD)
                    {
                        Merged[i * N + j] = TRUE;
                        pM = new MergeObj;
                        pM->ObjI = i;
                        pM->ObjJ = j;
                        pM->Measured = FALSE;
                        pM->GMeasured = FALSE;
                        pM->MaskCreated = FALSE;
                        m_MergeObjects.push_front(pM);
                        m_pMergeTable[i] = TRUE;
                        m_pMergeTable[j] = TRUE;
                    }
                }
            }
        }
    }


    // Delete objects < MinArea that are not part of a merge
    for (i = 0; i < N; i++)
    {
        if (Area[i] < MinArea && !m_pMergeTable[i])
        {
            freeobj(m_pObjectList[i]);
            m_pObjectList[i] = NULL;
        }
    }

    delete TempObjects;
    delete Xmin;
    delete Ymin;
    delete Xmax;
    delete Ymax;
    delete Area;
    delete Merged;

    freeobj(bigobj);

    m_nNumberObjects = N;

    // Send back number of objects found
    return m_nNumberObjects;
}

////////////////////////////////////////////////////////////////////////////////////////
// Gets the area a woolz object
// Parameters:
//      ObjectIndex is the index of the object to get data about
//      *WSD is the area for that object
// Returns:
//      TRUE if the object specified by index is in the list, FALSE otherwise
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::GetObjectArea(int ObjectIndex, int *WSD)
{
    BOOL Found = FALSE;

    // Check to make sure the object index is not silly
    if (ObjectIndex >= 0 && ObjectIndex < m_nNumberObjects && ObjectIndex < MAX_WOOLZ_OBJECTS)
    {
        // Now check to make sure the object is in the object list
        if (m_pObjectList[ObjectIndex])
        {
            // Do some object measurements and fill out the data structure
            *WSD = area(m_pObjectList[ObjectIndex]);

            // Return the object found
            Found = TRUE;
        }
    }

    return Found;
}

////////////////////////////////////////////////////////////////////////////////////////
// Gets the mass a woolz object (mass = sum of grey levels in the object)
// Parameters:
//      ObjectIndex is the index of the object to get data about
//      *WSD is the area for that object
// Returns:
//      TRUE if the object specified by index is in the list, FALSE otherwise
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::GetObjectMass(int ObjectIndex, int *Mass)
{
    BOOL Found = FALSE;

    // Check to make sure the object index is not silly
    if (ObjectIndex >= 0 && ObjectIndex < m_nNumberObjects && ObjectIndex < MAX_WOOLZ_OBJECTS)
    {
        // Now check to make sure the object is in the object list
        if (m_pObjectList[ObjectIndex])
        {
            // Do some object measurements and fill out the data structure
            *Mass = mass(m_pObjectList[ObjectIndex]);

            // Return the object found
            Found = TRUE;
        }
    }

    return Found;
}

////////////////////////////////////////////////////////////////////////////////////////
// Smooth a histogram by applying a low pass filter.
// It is assumed to be a curvature histogram, which has last point
// and first point referring to same boundary location,
// so we wrap round at the ends.
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::curvsmooth2(struct object *histo, int iterations)
{
	register int i;
	register int *iv;
	register int k, km2, km1, kp1, kp2, n;
	struct histogramdomain *hdom;

	if (histo == NULL)
		return 0;

	hdom = (struct histogramdomain *) histo->idom;
	//
	// iterate 1-1-1-1-1 smoothing
	//
	for (n=0; n<iterations; n++) 
    {
		iv = hdom->hv;
		km2 = *(iv + hdom->npoints - 3);
		km1 = *(iv + hdom->npoints - 2);
		kp1 = *(iv);
		kp2 = *(iv + 1);
		for (i = 0; i < hdom->npoints - 2; i++) 
        {
			k = *iv;
			*iv = (km2 + km1 + k + *(iv + 1) + *(iv + 2)) / 5;
			iv++;
			km2 = km1;
			km1 = k;
		}
		k = *iv;
		*iv = (km2 + km1 + *iv + *(iv + 1) + kp1) / 5;

		iv++;
		km2 = km1;
		km1 = k;
		*iv = (km2 + km1 + *iv + kp1 + kp2) / 5;

	}
	return 1;
}

////////////////////////////////////////////////////////////////////////////////////////
//	Analyse the curvature of an object and count the number of major peaks
//	This should roughly represent the number of visible chromosome tips.
//	If troughs=1 it looks for troughs instead, which are potential cut points.
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::NumberVertices(int ObjectIndex, BOOL troughs, int ThreshLevel)
{
	struct object *bobj, *uobj;
	struct object *h;
	struct histogramdomain *hdom;
	struct polygondomain *pdom;
	int *pt;
	int i, n, start, end, mid, wrap, feature, nfeatures;
	int threshold;

	if (troughs)
		threshold = ThreshLevel;		// trough threshold  
	else
		threshold = -ThreshLevel;	    // peak threshold  

	nfeatures = 0;
	
    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return 0;


    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return 0;


	if (m_pObjectList[ObjectIndex]->type != 1)
		return 0;

	bobj = ibound(m_pObjectList[ObjectIndex], 1);
	if (bobj == NULL)
		return 0;

	uobj= unitbound(bobj);
	if (uobj == NULL) 
    {
		freeobj(bobj);
		return 0;
	}

	pdom = (struct polygondomain *)uobj->idom;
	h = curvat(uobj);
	if (h==NULL) 
    {
		freeobj(bobj);
		freeobj(uobj);
		return 0;
	}

	curvsmooth2(h, 10);

	hdom = (struct histogramdomain *)h->idom;
	n = hdom->npoints;
	pt = hdom->hv;

	start = 0;
	end = 0;

	wrap = 20;
	if (wrap > n / 2)
		wrap = n / 2;

	/* scan for features - wrap round to catch end feature */
	for (i = 0; i < n + wrap; i++) 
    {

		if (troughs)
			feature = (*pt >= threshold);
		else
			feature = (*pt <= threshold);

		if (feature) 
        {
			if (start > 0)
				end = i;
			else
				start = i;
		}
		else 
        {
			if ((end > 0) && (start > 1)) 
            {

				if (end - start > 5) 
                {
					mid = (end + start)/ 2;

					if (mid > n - 1)
						mid = mid - n;

					nfeatures++;
				}						
			}

			start = 0;
			end = 0;
					
		}
		if (i == n - 1)
			pt = hdom->hv;
		else
			pt++;
	}

	freeobj(h);
	freeobj(uobj);
	freeobj(bobj);

	return nfeatures;
}

////////////////////////////////////////////////////////////////////////////////////////
// MinWidthRectangle
//      Finds the min width rectangle that fits an object
// Finds the rectangle and its orientation
// Returns TRUE if successful
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::MinWidthRectangle(short ObjectIndex, float *angle, PPOINT V1, PPOINT V2, PPOINT V3, PPOINT V4)
{
    struct object *rect, *cvh;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return FALSE;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return FALSE;

	if (m_pObjectList[ObjectIndex]->type != 1)
		return FALSE;

    // Convex hull for the object
	cvh = (struct object *)convhull(m_pObjectList[ObjectIndex]);

    // Calc the rectangle
	rect = (struct object *)minwrect(cvh);

    // Get the rectangle vertices
    V1->x = (int) ((struct frect *)rect->idom)->frk[0];
    V2->x = (int) ((struct frect *)rect->idom)->frk[1];
    V3->x = (int) ((struct frect *)rect->idom)->frk[2];
    V4->x = (int) ((struct frect *)rect->idom)->frk[3];
    V1->y = (int) ((struct frect *)rect->idom)->frl[0];
    V2->y = (int) ((struct frect *)rect->idom)->frl[1];
    V3->y = (int) ((struct frect *)rect->idom)->frl[2];
    V4->y = (int) ((struct frect *)rect->idom)->frl[3];

    // Calc orientation of the rectangle
	*angle = ((struct frect *)rect->idom)->rangle;

    // Free the temp objects
    freeobj(rect);
    freeobj(cvh);

    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////
// Find the bounding box for the object - not the same as the min width rectangle
////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::BoundingBox(short ObjectIndex, int *Xmin, int *Ymin, int *Xmax, int *Ymax)
{
    struct iwspace iwsp;
    struct gwspace gwsp;

	*Xmin = INT_MAX;
	*Ymin = INT_MAX;
	*Xmax = INT_MIN;
	*Ymax = INT_MIN;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return;

	if (m_pObjectList[ObjectIndex]->type != 1)
		return;

    if (wzcheckobj(m_pObjectList[ObjectIndex])) 
    	return;

    if (wzemptyidom(m_pObjectList[ObjectIndex]->idom)) 
    	return;

    if (m_pObjectList[ObjectIndex]->vdom == NULL) 
    	return;

    if (m_pObjectList[ObjectIndex]->vdom->type != 1) 
    {
    	if (m_pObjectList[ObjectIndex]->vdom->type == 21)
    		vdom21to1(m_pObjectList[ObjectIndex]);
    	else
    		return;
    }
    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
		if (iwsp.linpos < *Ymin) *Ymin = iwsp.linpos;
		if (iwsp.linpos > *Ymax) *Ymax = iwsp.linpos;
		if (iwsp.lftpos < *Xmin) *Xmin = iwsp.lftpos;
		if (iwsp.rgtpos > *Xmax) *Xmax = iwsp.rgtpos;
    }
}

////////////////////////////////////////////////////////////////////////////////////////
// draw object values under idom into an image 
////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::DrawObject(short ObjectIndex, FSCHAR *image, int image_width, int image_height)
{
    FSCHAR *imptr;
    struct iwspace iwsp;
    struct gwspace gwsp;
    register GREY *g;
    register int k;
    FSCHAR val;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return;

	if (m_pObjectList[ObjectIndex]->type != 1)
		return;

    if (wzcheckobj(m_pObjectList[ObjectIndex])) 
    	return;

    if (wzemptyidom(m_pObjectList[ObjectIndex]->idom)) 
    	return;

    if (m_pObjectList[ObjectIndex]->vdom == NULL) 
    	return;

    if (m_pObjectList[ObjectIndex]->vdom->type != 1) 
    {
    	if (m_pObjectList[ObjectIndex]->vdom->type == 21)
    		vdom21to1(m_pObjectList[ObjectIndex]);
    	else
    		return;
    }
    	    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	g = gwsp.grintptr;

    	imptr = (FSCHAR *) image + iwsp.linpos * image_width + iwsp.lftpos;

    	for (k=iwsp.lftpos; k<=iwsp.rgtpos; k++) 
        {
    		val = (FSCHAR)*g;
    		*imptr = val;
    		g++; 
            imptr++;
    	}
    }
}

////////////////////////////////////////////////////////////////////////////////////////
//	clear data under idom in an image for a given object with certain value 
////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::FillObjectXY(short ObjectIndex, FSCHAR *image, int x, int y, int image_width, int image_height, int value)
{
    FSCHAR *imptr;
    struct iwspace iwsp;
    struct gwspace gwsp;
    register int k;

     // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return;

	if (m_pObjectList[ObjectIndex]->type != 1)
        return;

    if (wzcheckobj(m_pObjectList[ObjectIndex])) 
    	return;

    if (wzemptyidom(m_pObjectList[ObjectIndex]->idom)) 
    	return;

    if (m_pObjectList[ObjectIndex]->vdom == NULL) 
    	return;

    if (m_pObjectList[ObjectIndex]->vdom->type != 1) 
    {
    	if (m_pObjectList[ObjectIndex]->vdom->type == 21)
    		vdom21to1(m_pObjectList[ObjectIndex]);
    	else
    		return;
    }
    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	imptr = (FSCHAR *) image + (y + (iwsp.linpos - m_pObjectList[ObjectIndex]->idom->line1)) * image_width + (x + (iwsp.lftpos - m_pObjectList[ObjectIndex]->idom->kol1));

    	for (k = iwsp.lftpos; k <= iwsp.rgtpos; k++) 
        {
    		*imptr = value;
    		imptr++;
    	}
    }
}

void CWoolzIP::FillObject(short ObjectIndex, FSCHAR *image, int image_width, int image_height, int value)
{
    FSCHAR *imptr;
    struct iwspace iwsp;
    struct gwspace gwsp;
    register int k;

     // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return;

	if (m_pObjectList[ObjectIndex]->type != 1)
        return;

    if (wzcheckobj(m_pObjectList[ObjectIndex])) 
    	return;

    if (wzemptyidom(m_pObjectList[ObjectIndex]->idom)) 
    	return;

    if (m_pObjectList[ObjectIndex]->vdom == NULL) 
    	return;

    if (m_pObjectList[ObjectIndex]->vdom->type != 1) 
    {
    	if (m_pObjectList[ObjectIndex]->vdom->type == 21)
    		vdom21to1(m_pObjectList[ObjectIndex]);
    	else
    		return;
    }
    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	imptr = (FSCHAR *) image + iwsp.linpos * image_width + iwsp.lftpos;

    	for (k = iwsp.lftpos; k <= iwsp.rgtpos; k++) 
        {
    		*imptr = value;
    		imptr++;
    	}
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Calculate grey level data about a woolz object
// GreyData returns an array of grey level values
// GreyData[0] = Min Grey Level
// GreyData[1] = Max Grey Level
// GreyData[2] = Mean Grey Level
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
void CWoolzIP::ObjectGreyData(short ObjectIndex, long *MinGrey, long *MaxGrey, long *MeanGrey)
{
    struct      iwspace iwsp;
    struct      gwspace gwsp;
    register    GREY *g;
    register    int k;
    BYTE        val;
    long        n = 0, total = 0;
    BYTE        Max, Min;

    Max = 0;
    Min = (BYTE) NBINS_EIGHT - 1;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return;

	if (m_pObjectList[ObjectIndex]->type != 1)
		return;

    if (wzcheckobj(m_pObjectList[ObjectIndex])) 
    	return;

    if (wzemptyidom(m_pObjectList[ObjectIndex]->idom)) 
    	return;

    if (m_pObjectList[ObjectIndex]->vdom == NULL) 
    	return;

    if (m_pObjectList[ObjectIndex]->vdom->type != 1) 
    {
    	if (m_pObjectList[ObjectIndex]->vdom->type == 21)
    		vdom21to1(m_pObjectList[ObjectIndex]);
    	else
    		return;
    }
    	    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	g = gwsp.grintptr;

    	for (k = iwsp.lftpos; k<=iwsp.rgtpos; k++) 
        {
    		val = (BYTE) *g;
            if (val > Max) Max = val;
            else
            if (val < Min) Min = val;
            total += val;
    		g++; 
            n++;
    	}
    }

    *MaxGrey = Max;
    *MinGrey = Min;
    if (n)
        *MeanGrey = total / n;
    else
        *MeanGrey = 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Calculate grey level data about a woolz object - for any UTS image
// GreyData returns an array of grey level values
// GreyData[0] = Min Grey Level
// GreyData[1] = Max Grey Level
// GreyData[2] = Mean Grey Level
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
void CWoolzIP::ObjectGreyDataInImage(short ObjectIndex, BYTE *image, long W, long H, long *MinGrey, long *MaxGrey, long *MeanGrey)
{
    struct      iwspace iwsp;
    struct      gwspace gwsp;
    register    int k;
    BYTE        val;
    long        n = 0, total = 0;
    BYTE        Max, Min;
    BYTE        *imptr, *imend;

    Max = 0;
    Min = (BYTE) NBINS_EIGHT - 1;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return;

	if (m_pObjectList[ObjectIndex]->type != 1)
		return;

    if (wzcheckobj(m_pObjectList[ObjectIndex])) 
    	return;

    if (wzemptyidom(m_pObjectList[ObjectIndex]->idom)) 
    	return;

    if (m_pObjectList[ObjectIndex]->vdom == NULL) 
    	return;

    if (m_pObjectList[ObjectIndex]->vdom->type != 1) 
    {
    	if (m_pObjectList[ObjectIndex]->vdom->type == 21)
    		vdom21to1(m_pObjectList[ObjectIndex]);
    	else
    		return;
    }
    	    	
    imend = image + W * (H - 1);
    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	imptr = image + iwsp.linpos * W + iwsp.lftpos;

    	for (k = iwsp.lftpos; k<=iwsp.rgtpos; k++) 
        {
    		val = (BYTE) *imptr++;
            if (val > Max) 
                Max = val;
            else
            if (val < Min) 
                Min = val;
            total += val;
            n++;
    	}
    }

    *MaxGrey = Max;
    *MinGrey = Min;
    if (n)
        *MeanGrey = total / n;
    else
        *MeanGrey = 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Calculate grey level data about a woolz object - for any UTS image
// GreyData returns an array of grey level values
// GreyData[0] = Min Grey Level
// GreyData[1] = Max Grey Level
// GreyData[2] = Mean Grey Level
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
void CWoolzIP::MergedObjectGreyDataInImage(short ObjectIndex, BYTE *image, long W, long H, long *MinGrey, long *MaxGrey, long *MeanGrey)
{
    struct      iwspace iwsp;
    struct      gwspace gwsp;
    register    int k;
    BYTE        val;
    long        n = 0, total = 0, TotObj = 0;
    BYTE        Max, Min;
    BYTE        *imptr, *imend;
    MergeObj    *pM;
    long        TempMean, TempMax, TempMin;
    long        TotalMean, TotalMax, TotalMin;

    TotalMean = 0;
    TotalMax = 0;
    TotalMin = 0;

    Max = 0;
    Min = (BYTE) NBINS_EIGHT - 1;

	std::list<MergeObj*>::iterator P = m_MergeObjects.begin();
    while (P != m_MergeObjects.end())
    {
        pM = *P;
		P++;

        if (!m_pObjectGreys[ObjectIndex])
        {
            if (pM->ObjI == ObjectIndex && !pM->GMeasured)
            {
                pM->GMeasured = TRUE;
                MergedObjectGreyDataInImage(pM->ObjJ, image, W, H, &TempMin, &TempMax, &TempMean);
                TotalMin += TempMin;
                TotalMax += TempMax;
                TotalMean += TempMean;
                TotObj++;
                m_pObjectGreys[pM->ObjJ] = TRUE;
            }
            else
            if (pM->ObjJ == ObjectIndex && !pM->GMeasured)
            {
                pM->GMeasured = TRUE;
                MergedObjectGreyDataInImage(pM->ObjI, image, W, H, &TempMin, &TempMax, &TempMean);
                TotalMin += TempMin;
                TotalMax += TempMax;
                TotalMean += TempMean;
                TotObj++;
                m_pObjectGreys[pM->ObjI] = TRUE;
            }
        }
    }
    	    	
    imend = image + W * (H - 1);
    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	imptr = image + iwsp.linpos * W + iwsp.lftpos;

    	for (k = iwsp.lftpos; k<=iwsp.rgtpos; k++) 
        {
    		val = (BYTE) *imptr++;
            if (val > Max) 
                Max = val;
            else
            if (val < Min) 
                Min = val;
            total += val;
            n++;
    	}
    }

    if (TotObj)
    {
        TotalMin = (Min + TotalMin / TotObj) / 2;
        TotalMax = (Max + TotalMax / TotObj) / 2;
        
        *MaxGrey = TotalMax;
        *MinGrey = TotalMin;
        if (n)
            *MeanGrey = ((total / n) + TotalMean / TotObj) / 2;
        else
            *MeanGrey = TotalMean / TotObj;
    }
    else
    {
        *MaxGrey = Max;
        *MinGrey = Min;
        if (n)
            *MeanGrey = total / n;
        else
            *MeanGrey = 0;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Label an object
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::LabelObject(short ObjectIndex, long LabelValue)
{
    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return;

    m_pLabelList[ObjectIndex] = LabelValue;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Get the label for an object, returns 0 if invalid for some reason
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

long CWoolzIP::GetObjectLabel(short ObjectIndex)
{
    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return 0;

    return m_pLabelList[ObjectIndex];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Count separately labelled objects within the object list
// i.e. if two objects are labelled with "1" and object is labelled with "2" then the 
// total count will be 2 as two obejcts have the same label
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

long CWoolzIP::CountLabelledObjects(void)
{
    int i, j;
    long Total = 0;
    long InList[MAX_WOOLZ_OBJECTS];         // Keep a list of all labels counted so far
    BOOL AlreadyThere;

    // Clear the list of counted labels
    for (i = 0; i < MAX_WOOLZ_OBJECTS; i++) InList[i] = 0;

    // For all objects in the list
    for (i = 0; i < m_nNumberObjects; i++)
    {
        // Is this a valid object
        if (m_pObjectList[i])
        {
            AlreadyThere = FALSE;
            // Check to make sure the label has not already been counted
            for (j = 0; j < m_nNumberObjects; j++)
                if (InList[j] == m_pLabelList[i])
                {
                    AlreadyThere = TRUE;
                    break;
                }

            // Not already in our list so add to the list and inc the total number of objects
            if (!AlreadyThere)
            {
                for (j = 0; j < m_nNumberObjects; j++)
                {
                    // Found a blank place in the list so add it and quit the loop
                    if (InList[j] == 0)
                    {
                        InList[j] = m_pLabelList[i];
                        Total++;                        // Inc the total
                        break;
                    }
                }
            }
        }
    }

    return Total;
}

void CWoolzIP::GetPoint(int ObjectIndex, long *X, long *Y)
{
    struct      iwspace iwsp;
    struct      gwspace gwsp;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return;

	if (m_pObjectList[ObjectIndex]->type != 1)
		return;

    if (wzcheckobj(m_pObjectList[ObjectIndex])) 
    	return;

    if (wzemptyidom(m_pObjectList[ObjectIndex]->idom)) 
    	return;

    if (m_pObjectList[ObjectIndex]->vdom == NULL) 
    	return;

    if (m_pObjectList[ObjectIndex]->vdom->type != 1) 
    {
    	if (m_pObjectList[ObjectIndex]->vdom->type == 21)
    		vdom21to1(m_pObjectList[ObjectIndex]);
    	else
    		return;
    }
    	    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
	nextgreyinterval(&iwsp);

    *X = iwsp.lftpos; 
	*Y = iwsp.linpos;
}

//////////////////////////////////////////////////////////////////////////////////
// Some spot processing routines - courtesy of MG
//////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::Objgradthresh(struct object *obj)
{
    struct iwspace iwsp;
    struct gwspace gwsp;
    register GREY *g;
    register int k;
    FSCHAR val;

    int SE=0;
    int SG=0;

    initgreyscan(obj, &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
	{
    	g = gwsp.grintptr;

		int preval = -1;
    	
		for (k=iwsp.lftpos; k<=iwsp.rgtpos; k++) 
		{
    		val = (FSCHAR)*g;

			if (preval>=0)
			{
				int grad = abs(val - preval);
				if (grad > 4)
				{
					SG+= grad;
					SE+= val*grad;
				}
			}

			preval=val;

    		g++; 
    	}
    }

    if (SG == 0)
		return 0;
    else
		return SE/(SG+1);
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Draw an object in image if its grey levels exceed the threshold
////////////////////////////////////////////////////////////////////////////////////////////////
    
void CWoolzIP::ThreshValues(struct object *obj, FSCHAR *image, int image_width, int image_height, int thresh)
{
    BYTE *imptr, *imend;
    struct iwspace iwsp;
    struct gwspace gwsp;
    register GREY *g;
    register int k;
    BYTE val;

    if (wzcheckobj(obj)) 
	{
    	fprintf(stderr,"Bad obj sent to set_values\n");
    	return;
    }

    if (wzemptyidom(obj->idom)) 
	{
    	fprintf(stderr, "Empty idom sent to set_values\n");
    	return;
    }

    if (obj->vdom == NULL) 
	{
    	fprintf(stderr, "Null vdom sent to set_values\n");
    	return;
    }

    if (obj->vdom->type != 1) 
	{
    	fprintf(stderr, "vdom type = %d\n",obj->vdom->type);
    	if (obj->vdom->type == 21)
    		vdom21to1(obj);
    	else
    		return;
    }
    	
    imend=image + image_width * (image_height - 1);
    	
    initgreyscan(obj, &iwsp, &gwsp);
    
	while (nextgreyinterval(&iwsp) == 0) 
	{
    	g = gwsp.grintptr;

    	imptr=imend - iwsp.linpos * image_width + iwsp.lftpos;

    	for (k=iwsp.lftpos; k<=iwsp.rgtpos; k++) 
		{
    		val = (BYTE) *g;
			if (val>thresh)
    			*imptr = 255;
			else
				*imptr = 0;

    		g++; imptr++;
    	}
    }
}

////////////////////////////////////////////////////////////////////////////////////////
// Do a selection of measurements on an object found using the find objects above
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::MeasureObject(short ObjectIndex, WoolzMeasurements *WM)
{
	register struct ivertex *vtx, *wtx;
	register int i;
	double Perim, MinDist, MaxDist, Dist, D;
	long dx, dy;
	long Area;
	long Xmin, Ymin, Xmax, Ymax;
	long SumCOGx, SumCOGy;
    int N, NVertices;
    struct ivertex *ivtx, *UnitObj;
    POINT  V1, V2, V3, V4;
    struct object *rect, *cvh;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return FALSE;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return FALSE;

	if (m_pObjectList[ObjectIndex]->type != 1)
		return FALSE;

    if (wzcheckobj(m_pObjectList[ObjectIndex])) 
    	return FALSE;

    if (wzemptyidom(m_pObjectList[ObjectIndex]->idom)) 
    	return FALSE;

    if (m_pObjectList[ObjectIndex]->vdom == NULL) 
    	return FALSE;

    if (m_pObjectList[ObjectIndex]->vdom->type != 1) 
    {
    	if (m_pObjectList[ObjectIndex]->vdom->type == 21)
    		vdom21to1(m_pObjectList[ObjectIndex]);
    	else
    		return FALSE;
    }
         
	// Convert the object to a unit boundary with wrap to beginning
	ivtx = (ivertex *)bigbound(m_pObjectList[ObjectIndex], 1, &N);
	if (ivtx == NULL)
		return FALSE;

	// Calc the boundary points
	UnitObj = (ivertex *)unitbigbound((ivertex *)ivtx, N, &NVertices);
	if (UnitObj == NULL) 
    {
		free(ivtx);
		return FALSE;
	}

    // Actual area of object
    WM->Area = area(m_pObjectList[ObjectIndex]);

	// check for closed polygon.
	// should really check vertex adjacency,
	// but this would take a long time.	
	vtx = UnitObj + NVertices - 1;
	wtx = UnitObj;
	if (vtx->vtX != wtx->vtX || vtx->vtY != wtx->vtY) 
	{
		TRACE1("OBJECT %ld NOT A CLOSED POLYGON\r\n", ObjectIndex);
		free(UnitObj);
		free(ivtx);
		return FALSE;
	}

	if (NVertices < 2)
	{
		free(UnitObj);
		free(ivtx);
		return FALSE;
	}

    // Point on boundary for reconstruction in UTS
    //WM->SeedReconX = wtx->vtX;
    //WM->SeedReconY = wtx->vtY;
    //GetPoint(ObjectIndex, &WM->SeedReconX, &WM->SeedReconY);

	Perim = 0.0;
	Area = 0;
	Xmin = LONG_MAX;
	Xmax = LONG_MIN;
	Ymin = LONG_MAX;
	Ymax = LONG_MIN;
	SumCOGx = 0;
	SumCOGy = 0;
	MinDist = DBL_MAX;
	MaxDist = 0.0;

	wtx = UnitObj;
	vtx = wtx + 1;

	for (i = 0; i < NVertices; i++) 
	{
		if (i == NVertices - 1)
			vtx = UnitObj;
		
		// Bounding box
		if (vtx->vtX > Xmax) 
			Xmax = vtx->vtX;
		if (vtx->vtX < Xmin)
			Xmin = vtx->vtX;
		if (vtx->vtY > Ymax)
			Ymax = vtx->vtY;
		if (vtx->vtY < Ymin)
			Ymin = vtx->vtY;

		// Calculate perimeter
		dx = vtx->vtX - wtx->vtX;
		dy = vtx->vtY - wtx->vtY;
        // Lengthy but accurate enough
		//if (dx != 0 || dy != 0)
		//	Perim += sqrt(dx * dx + dy * dy);
        // This saves 100 microsecs / object on a 100 x DAPI cell !
        if (dx != 0)
        {
            if (dy == 0)
                Perim += 1.0;
            else
                Perim += 1.4142;        // sqrt(2)
        }
        else
        {
            if (dy != 0)
                Perim += 1.0;
        }

		// Calculate Area 
		Area += (vtx->vtX + wtx->vtX) * dy;

		// COG
		SumCOGx += vtx->vtX;
		SumCOGy += vtx->vtY;

		vtx++;
		wtx++;
	}
	Area /= 2;

	// Get the approximate centre of gravity
	WM->COGx = SumCOGx / NVertices;
	WM->COGy = SumCOGy / NVertices;
	
	vtx = UnitObj;

	for (i = 0; i < NVertices; i++) 
	{		
		// Calculate max and min dist from COG to perimeter
		dx = vtx->vtX - (long) WM->COGx;
		dy = vtx->vtY - (long) WM->COGy;

        // NOTE dont sqrt it here, this saves precious microsecs
        Dist = dx * dx + dy * dy;
		if (Dist < MinDist) 
			MinDist = Dist;
		if (Dist > MaxDist) 
			MaxDist = Dist;
	
		vtx++;
	}

	// Min and max radii, axis ratio
	WM->MinRadii = sqrt(MinDist + 1.);
	WM->MaxRadii = sqrt(MaxDist + 1.);

    // Take the max diameter to be twice the distance from
    // the COG to the furthest point on the boundary. This
    // is not really quite right but gives nice results for
    // compactness and circularity measures.
    D = WM->MaxRadii + WM->MaxRadii;
	// Axis ratio and circularity
	WM->AxisRatio = WM->MinRadii / WM->MaxRadii;

	WM->Circularity = (4.0 * Area) / (3.142 * D * D);
    // Make sure circularity stays in range sometimes a wee bit over 1.00
    if (WM->Circularity > 1.0000) WM->Circularity = 1.0;

	// Area and perimeter
	WM->EnclosedArea = Area;
	WM->Perimeter = Perim;
	if (Area > 0 && D > 0)
		WM->Compactness = sqrt(1.273 * Area) / D;			// NB 1.273 = 4/pi
	else
		WM->Compactness = 0.0;
	
	// Bounding box
	WM->BBLeft   = Xmin;
	WM->BBRight  = Xmax;
	WM->BBTop	 = Ymin;
	WM->BBBottom = Ymax;
    
    // Convex hull for the object
	cvh = (struct object *)convhull(m_pObjectList[ObjectIndex]);

    // Calc the rectangle
	rect = (struct object *)minwrect(cvh);

    // Get the rectangle vertices
    V1.x = (int) ((struct frect *)rect->idom)->frk[0];
    V2.x = (int) ((struct frect *)rect->idom)->frk[1];
    V3.x = (int) ((struct frect *)rect->idom)->frk[2];
    V4.x = (int) ((struct frect *)rect->idom)->frk[3];
    V1.y = (int) ((struct frect *)rect->idom)->frl[0];
    V2.y = (int) ((struct frect *)rect->idom)->frl[1];
    V3.y = (int) ((struct frect *)rect->idom)->frl[2];
    V4.y = (int) ((struct frect *)rect->idom)->frl[3];

    // Calc orientation of the rectangle
	WM->Orientation = ((struct frect *)rect->idom)->rangle;

    // Calc Height and Width of the rectangle
    dx = V1.x - V2.x;
    dy = V1.y - V2.y;
    WM->Width = (long) sqrt(dx * dx + dy * dy + 0.1);
    dx = V2.x - V3.x;
    dy = V2.y - V3.y;
    WM->Height = (long) sqrt(dx * dx + dy * dy + 0.1);		// Get array info

    // Free the temp objects
    freeobj(rect);
    freeobj(cvh);

	free(UnitObj);
    free(ivtx);

	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////
// Do a selection of measurements on an object found using the find objects above
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::MeasureMergedObject(short ObjectIndex, WoolzMeasurements *WM)
{
    int N;
    BOOL MergedObj = FALSE;
    WoolzMeasurements WMerged, WTemp;
    MergeObj *pM;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS || m_pMeasuredObject[ObjectIndex] || !m_pObjectList[ObjectIndex])
        return NullObject;

    // Is this a merged object ?
    if (m_pMergeTable)
    {
        MergedObj = m_pMergeTable[ObjectIndex];
        // No it isn't so call the standard measurement routine and exit
        if (!MergedObj)
        {
            MeasureObject(ObjectIndex, WM);             // Standard measurements
            m_pMeasuredObject[ObjectIndex] = TRUE;      // Mark as measured
            return NotMergedObject;                     // Object not a merged object
        }
    }

    // Zero our total measurements
    memset(&WMerged, 0, sizeof(WoolzMeasurements));
    WMerged.BBLeft   = LONG_MAX;
    WMerged.BBRight  = LONG_MIN;
    WMerged.BBTop    = LONG_MAX;
    WMerged.BBBottom = LONG_MIN;

    N = 1;
	std::list<MergeObj*>::iterator P = m_MergeObjects.begin();
    while (P != m_MergeObjects.end())
    {
        pM = *P;
		P++;

        // Do we have an object connected to this one ? If so measure it
        if (pM->ObjI == ObjectIndex && !m_pMeasuredObject[pM->ObjJ] && !pM->Measured)
        {
            pM->Measured = TRUE;
            MeasureMergedObject(pM->ObjJ, &WTemp);
            N++;

            WMerged.Area += WTemp.Area;
            WMerged.AxisRatio += WTemp.AxisRatio;
            WMerged.Circularity += WTemp.Circularity;
            WMerged.Compactness += WTemp.Compactness;
            WMerged.COGx += WTemp.COGx;
            WMerged.COGy += WTemp.COGy;
            WMerged.EnclosedArea += WTemp.EnclosedArea;
            WMerged.MaxRadii += WTemp.MaxRadii;
            WMerged.MinRadii += WTemp.MinRadii;
            WMerged.Perimeter += WTemp.Perimeter;
            WMerged.Orientation += WTemp.Orientation;
            WMerged.BBLeft = MIN(WMerged.BBLeft, WTemp.BBLeft);
            WMerged.BBRight = MAX(WMerged.BBRight, WTemp.BBRight);
            WMerged.BBTop = MIN(WMerged.BBTop, WTemp.BBTop);
            WMerged.BBBottom = MAX(WMerged.BBBottom, WTemp.BBBottom);
            WMerged.Width += WTemp.Width;
            WMerged.Height += WTemp.Height;
            m_pMeasuredObject[pM->ObjJ] = TRUE;
        }
        else
        if (pM->ObjJ == ObjectIndex && !m_pMeasuredObject[pM->ObjI] && !pM->Measured)
        {
            pM->Measured = TRUE;
            MeasureMergedObject(pM->ObjI, &WTemp);
            N++;

            WMerged.Area += WTemp.Area;
            WMerged.AxisRatio += WTemp.AxisRatio;
            WMerged.Circularity += WTemp.Circularity;
            WMerged.Compactness += WTemp.Compactness;
            WMerged.COGx += WTemp.COGx;
            WMerged.COGy += WTemp.COGy;
            WMerged.EnclosedArea += WTemp.EnclosedArea;
            WMerged.MaxRadii += WTemp.MaxRadii;
            WMerged.MinRadii += WTemp.MinRadii;
            WMerged.Perimeter += WTemp.Perimeter;
            WMerged.Orientation += WTemp.Orientation;
            WMerged.BBLeft = MIN(WMerged.BBLeft, WTemp.BBLeft);
            WMerged.BBRight = MAX(WMerged.BBRight, WTemp.BBRight);
            WMerged.BBTop = MIN(WMerged.BBTop, WTemp.BBTop);
            WMerged.BBBottom = MAX(WMerged.BBBottom, WTemp.BBBottom);
            WMerged.Width += WTemp.Width;
            WMerged.Height += WTemp.Height;
            m_pMeasuredObject[pM->ObjI] = TRUE;
        }
    }
        
    // Now measure this object
    MeasureObject(ObjectIndex, &WTemp);
    m_pMeasuredObject[ObjectIndex] = TRUE;

    WMerged.Area += WTemp.Area;
    WMerged.AxisRatio += WTemp.AxisRatio;
    WMerged.Circularity += WTemp.Circularity;
    WMerged.Compactness += WTemp.Compactness;
    WMerged.COGx += WTemp.COGx;
    WMerged.COGy += WTemp.COGy;
    WMerged.EnclosedArea += WTemp.EnclosedArea;
    WMerged.MaxRadii += WTemp.MaxRadii;
    WMerged.MinRadii += WTemp.MinRadii;
    WMerged.Perimeter += WTemp.Perimeter;
    WMerged.Orientation += WTemp.Orientation;
    WMerged.BBLeft = MIN(WMerged.BBLeft, WTemp.BBLeft);
    WMerged.BBRight = MAX(WMerged.BBRight, WTemp.BBRight);
    WMerged.BBTop = MIN(WMerged.BBTop, WTemp.BBTop);
    WMerged.BBBottom = MAX(WMerged.BBBottom, WTemp.BBBottom);
	WMerged.Width += WTemp.Width;
	WMerged.Height += WTemp.Height;

    WM->Area = WMerged.Area;
    WM->AxisRatio = WMerged.AxisRatio / N;
    WM->Circularity = WMerged.Circularity / N;
    WM->COGx = WMerged.COGx / N;
    WM->COGy = WMerged.COGy / N;
    WM->Compactness = WMerged.Compactness / N;
    WM->EnclosedArea = WMerged.EnclosedArea;
    WM->MaxRadii = WMerged.MaxRadii;
    WM->MinRadii = WMerged.MinRadii;
    WM->Perimeter = WMerged.Perimeter;
    WM->BBLeft = WMerged.BBLeft;
    WM->BBRight = WMerged.BBRight;
    WM->BBTop = WMerged.BBTop;
    WM->BBBottom = WMerged.BBBottom;
    WM->Orientation = WMerged.Orientation / N;
	WM->Width = WMerged.BBRight - WMerged.BBLeft;
	WM->Height = WMerged.BBBottom - WMerged.BBTop;

	return MergedObject;
}

////////////////////////////////////////////////////////////////////////////////////////
// Measures the boundary of an object
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::MeasureBoundary(short ObjectIndex, long **Boundary)
{
	struct ivertex *UnitObj, *IObj;
	register struct ivertex *vtx, *wtx;
	register int i;
    int N, NVertices;
    long *Bptr;

    // Free any previous array of points
    if ((*Boundary) != NULL)
        delete [] (*Boundary);
    
    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return 0;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return 0;

	if (m_pObjectList[ObjectIndex]->type != 1)
		return 0;

    if (wzcheckobj(m_pObjectList[ObjectIndex])) 
    	return 0;

    if (wzemptyidom(m_pObjectList[ObjectIndex]->idom)) 
    	return 0;

    if (m_pObjectList[ObjectIndex]->vdom == NULL) 
    	return 0;

    if (m_pObjectList[ObjectIndex]->vdom->type != 1) 
    {
    	if (m_pObjectList[ObjectIndex]->vdom->type == 21)
    		vdom21to1(m_pObjectList[ObjectIndex]);
    	else
    		return 0;
    }
         
	// Convert the object to a unit boundary with wrap to beginning
	IObj = bigbound(m_pObjectList[ObjectIndex], 1, &N);
	if (IObj == NULL)
		return 0;

	// Calc the boundary points
	UnitObj = unitbigbound(IObj, N, &NVertices);
	if (UnitObj == NULL) 
    {
		free(IObj);
		return 0;
	}

	// check for closed polygon.
	// should really check vertex adjacency,
	// but this would take a long time.	
	vtx = UnitObj;
	wtx = vtx + NVertices - 1;
	if (vtx->vtX != wtx->vtX || vtx->vtY != wtx->vtY) 
	{
		TRACE1("OBJECT %ld NOT A CLOSED POLYGON\r\n", ObjectIndex);
		return 0;
	}

	if (NVertices < 2)
		return 0;

    *Boundary = (long *) new long[NVertices * 2];

    Bptr = (*Boundary);
	wtx = UnitObj;

	for (i = 0; i < NVertices; i++) 
	{
		*Bptr++ = (long) wtx->vtX;       // Fill in the boundary point array     
        *Bptr++ = (long) wtx->vtY;
		wtx++;
	}
	
	// Get the approximate centre of gravity
	free(UnitObj);
	free(IObj);

	return i;             // Return the number of points in the boundary
}

////////////////////////////////////////////////////////////////////////////////////////
// Fractal measure the boundary of an object
// NSamples is the max sample spacing around the perimeter. 1 to NSamples iterations
// will be done
////////////////////////////////////////////////////////////////////////////////////////

double CWoolzIP::FractalMeasureBoundary(short ObjectIndex, int NSamples)
{
	struct ivertex *UnitObj, *IObj;
	register struct ivertex *vtx, *wtx;
	int i, j, s, dx, dy;
    int N, NVertices;
    double *Y, *X, Result, *XY, *XX, SX, SXX, SY, SXY;

    // Pointless for less than 3 samples
    if (NSamples < 3) 
        return 0.0;

    
    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return 0;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return 0;

	if (m_pObjectList[ObjectIndex]->type != 1)
		return 0;

    if (wzcheckobj(m_pObjectList[ObjectIndex])) 
    	return 0;

    if (wzemptyidom(m_pObjectList[ObjectIndex]->idom)) 
    	return 0;

    if (m_pObjectList[ObjectIndex]->vdom == NULL) 
    	return 0;

    if (m_pObjectList[ObjectIndex]->vdom->type != 1) 
    {
    	if (m_pObjectList[ObjectIndex]->vdom->type == 21)
    		vdom21to1(m_pObjectList[ObjectIndex]);
    	else
    		return 0;
    }
         
	// Convert the object to a unit boundary with wrap to beginning
	IObj = bigbound(m_pObjectList[ObjectIndex], 1, &N);
	if (IObj == NULL)
		return 0;

	// Calc the boundary points
	UnitObj = unitbigbound(IObj, N, &NVertices);
	if (UnitObj == NULL) 
    {
		free(IObj);
		return 0;
	}

	// check for closed polygon.
	// should really check vertex adjacency,
	// but this would take a long time.	
	vtx = UnitObj;
	wtx = vtx + NVertices - 1;
	if (vtx->vtX != wtx->vtX || vtx->vtY != wtx->vtY) 
	{
		TRACE1("OBJECT %ld NOT A CLOSED POLYGON\r\n", ObjectIndex);
		return 0.0;
	}

	if (NVertices < 2)
		return 0.0;

    // Arrays to hold the data in
    X = new double[NSamples];
    Y = new double[NSamples];
    XX = new double[NSamples];
    XY = new double[NSamples];

#if 0
    // Square root table - should be faster than calculating
    // a square root everytime unless NSamples is small
    Roots = new double[(NSamples + 1) * (NSamples + 1)];

    for (i = 0; i <= NSamples; i++)
    {
        for (j = 0; j <= NSamples; j++)
            Roots[i * NSamples + j] = sqrt(i * i + j * j + 0.000000001);
    }
#endif

    // Iterate over the boundary
    for (s = 1; s <= NSamples; s++)
    {
	    wtx = UnitObj;
        vtx = UnitObj;
        wtx += s;
        i = NSamples;
        Result = 0.0;
	    while (i < NVertices)
	    {
            dx = abs(wtx->vtX - vtx->vtX);
            dy = abs(wtx->vtY - vtx->vtY);
            Result += sqrt((double)(dx * dx + dy * dy));//Roots[dx * NSamples + dy];
            i += s;
            vtx += s;
            wtx += s;
        }

        X[s - 1] = log10(Result);
        Y[s - 1] = log10((double)s);
        XY[s - 1] = X[s - 1] * Y[s - 1];
        XX[s - 1] = X[s - 1] * X[s - 1];
    }

    // Use linear regression to find the best line fit though the points 
    // calculated above. The slope is then the fractal measure
    SX = 0.0;
    SY = 0.0;
    SXX = 0.0;
    SXY = 0.0;
    for (s = 0; s < NSamples; s++)
    {
        SX += X[s];
        SY += Y[s];
        SXX += XX[s];
        SXY += XY[s];
    }

    Result = fabs(NSamples * SXY - SX * SY) / (NSamples * SXX - SX * SX);

    // Clear up
    delete X;
    delete Y;
    delete XX;
    delete XY;
    //delete Roots;
	
	free(UnitObj);
	free(IObj);

	return Result;             // Return the number of points in the boundary
}

////////////////////////////////////////////////////////////////////////////////////////
// Simply returns the pointer to an object with a specific object index
////////////////////////////////////////////////////////////////////////////////////////

void *CWoolzIP::GetObjectPtr(short ObjectIndex)
{
    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return NULL;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return FALSE;

	if (m_pObjectList[ObjectIndex]->type != 1)
		return FALSE;

    return m_pObjectList[ObjectIndex];
}

////////////////////////////////////////////////////////////////////////////////////////
// Remove an object from the list
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::RemoveObject(int index)
{
	BOOL status = FALSE;

    // First check to make sure the object index is not silly
    if (index >= 0 && index < m_nNumberObjects)
	{
		if (m_pObjectList[index])
		{
			freeobj(m_pObjectList[index]);
			m_pObjectList[index] = NULL;
		}

		m_nNumberObjects--;

		// Shuffle down objects to close gap where object was removed.
		for (int i = index; i < m_nNumberObjects; i++)
			m_pObjectList[i] = m_pObjectList[i + 1];

		// Must clear the last object that was shuffled down, as FreeObjects()
		// frees everything up to MAX_WOOLZ_OBJECTS rather than just m_nNumberObjects.
		m_pObjectList[m_nNumberObjects] = NULL;

		status = TRUE;
	}

	return status;
}

////////////////////////////////////////////////////////////////////////////////////////
//	Fill an object in an image 
////////////////////////////////////////////////////////////////////////////////////////
   
void CWoolzIP::WzFillObject(struct object *Obj, BYTE *image, int image_width, int image_height, int value)
{
    FSCHAR *imptr, *imend;
    struct iwspace iwsp;
    struct gwspace gwsp;
    register int k;
    	
    imend = image + image_width * (image_height - 1);
    	
    initgreyscan(Obj, &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	imptr=imend - iwsp.linpos * image_width + iwsp.lftpos;

    	for (k = iwsp.lftpos; k <= iwsp.rgtpos; k++) 
        {
    		*imptr = value;
    		imptr++;
    	}
    }
}



long CWoolzIP::WoolzRemoveCoverslipObjects()
{
	long obj, num_vert;
    long *BPoints;
    long *Bptr1, *Bptr2;
	long i,x1,y1,x2,y2;
	double dx,dy,ang,angs[11];
	BOOL coverslip_objects[MAX_WOOLZ_OBJECTS];

    // For all objects in the list
    for (obj = 0; obj < m_nNumberObjects; obj++)
    {
		coverslip_objects[obj] = FALSE;
		for (i=0; i<=10; i++) angs[i]=0.0;
		BPoints = NULL;
	    num_vert = (long) MeasureBoundary((short) obj, &BPoints);

		if (num_vert > 100)
		{
			// Fill in the boundary point array
			Bptr1 = BPoints;
			Bptr2 = BPoints + 50;

			for (i = 0; i < num_vert-50; i++)
			{
				x1 = *Bptr1++;
				y1 = *Bptr1++;
				x2 = *Bptr2++;
				y2 = *Bptr2++;

				dx = fabs((float)(x1 - x2));
				dy = fabs((float)(y1 - y2));

				if (dx != 0.0)
					ang = 10.0*2.0*atan(dy/dx)/3.1415926535897932384626433832795;
				else
					ang = 10.0;

				long rounded_ang;
				if ((ang - (int)ang) > 0.5)
					rounded_ang = (int)ang + 1;
				else
					rounded_ang = (int)ang;
				angs[rounded_ang] += 1;
			}

			delete BPoints;

			// check if 
			long horiz_plus_vert = angs[0] + angs[10];
			long total = 0;
			for (i=0; i<=10; i++)
				total += angs[i];

			if ( ((100*horiz_plus_vert)/total) > 40 )
				coverslip_objects[obj] = TRUE;
			
		}
	}

    for (obj = 0; obj < m_nNumberObjects; obj++)
	{
		if (coverslip_objects[obj])
		{
			freeobj(m_pObjectList[obj]);
			m_pObjectList[obj] = NULL;
		}
	}

	long keep_objects=0;
    for (obj = 0; obj < m_nNumberObjects; obj++)
	{
		if (!coverslip_objects[obj])
			m_pObjectList[keep_objects++] = m_pObjectList[obj];
	}
    for (obj = keep_objects; obj < m_nNumberObjects; obj++)
		m_pObjectList[obj] = NULL;

	m_nNumberObjects = keep_objects;
	return m_nNumberObjects;
}





////////////////////////////////////////////////////////////////////////////////////////
// draw idom into an image using supplied val
////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::DrawIdom(short ObjectIndex, BYTE *image, int image_width, int image_height, int value)
{
    BYTE *imptr;
    struct iwspace iwsp;
    //struct gwspace gwsp;
    register int k;

     // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return;

	if (m_pObjectList[ObjectIndex]->type != 1)
        return;

    if (wzcheckobj(m_pObjectList[ObjectIndex])) 
    	return;

    if (wzemptyidom(m_pObjectList[ObjectIndex]->idom)) 
    	return;

    	
	initrasterscan(m_pObjectList[ObjectIndex],&iwsp,0);
	while (nextinterval(&iwsp) == 0)
	{
    	imptr = (FSCHAR *) image + iwsp.linpos * image_width + iwsp.lftpos;
		for (k = iwsp.lftpos; k <= iwsp.rgtpos; k++) 
		{
			*imptr = value;
			imptr++;
		}
	}



}

////////////////////////////////////////////////////////////////////////////////////////
//	Fill an object in an image if seed is inside object 
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::WzFillObjectSeed(struct object *obj, BYTE *image, int cols, int lines, int col, int line,int value)
{
    FSCHAR *imptr, *imend;
    struct iwspace iwsp;
    register int k;
	BOOL seedinobj=FALSE;

	initrasterscan(obj,&iwsp,0);
	
	while (nextinterval(&iwsp) == 0)
    	if (iwsp.linpos == (lines-line-1))
    		if ((iwsp.lftpos <= col) && (iwsp.rgtpos >= col))
			{
				seedinobj=TRUE;
				break;
			}
    	
    imend = image + cols * (lines - 1);

	if (seedinobj)
	{
		initrasterscan(obj,&iwsp,0);
		while (nextinterval(&iwsp) == 0)
		{
			imptr=imend - iwsp.linpos * cols + iwsp.lftpos;
			for (k = iwsp.lftpos; k <= iwsp.rgtpos; k++) 
			{
				*imptr = value;
				imptr++;
			}
		}
	}

	return seedinobj;
}





BOOL CWoolzIP::MergeObjects()
{
	for (int i=0; i<m_nNumberObjects; i++)
	{
		struct object *tmp_obj = dilation(m_pObjectList[i]);
		struct object *dilated_obj_i = dilation4(tmp_obj);
		freeobj(tmp_obj);	
		
		for (int j=i+1; j<m_nNumberObjects; j++)
		{

			struct object *tmp_obj = dilation(m_pObjectList[j]);
			struct object *dilated_obj_j = dilation4(tmp_obj);
			freeobj(tmp_obj);	

			struct object *intersect_obj = intersect2(dilated_obj_i, dilated_obj_j);
			int a = area(intersect_obj);
			if (a > 0)
			{
				int b = area(m_pObjectList[j]);
				if ((100*a/b) > 50)
				{
					struct object *ObjList[3];
					ObjList[0] = intersect_obj;
					ObjList[1] = m_pObjectList[i];
					ObjList[2] = m_pObjectList[j];

					struct object *union_obj = unionn(ObjList, 3, 0);
					//struct object *union_obj = duplicate_obj(intersect_obj);
					// Free the original objects
					freeobj(m_pObjectList[i]);
					freeobj(m_pObjectList[j]);
					// Replace original with union object
					m_pObjectList[i] = union_obj;
					// move everything down
					for (int k=j; k<m_nNumberObjects-1; k++)
					{
						m_pObjectList[k] = m_pObjectList[k+1];
					}
					m_pObjectList[m_nNumberObjects-1] = NULL;
					m_nNumberObjects--;

					freeobj(intersect_obj);
					freeobj(dilated_obj_i);
					freeobj(dilated_obj_j);
					return TRUE;
				}
			}
			freeobj(dilated_obj_j);	
			freeobj(intersect_obj);
		}
		freeobj(dilated_obj_i);
	}
	return FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////
// Texture metaphase finder
//					 Clumpy bone	Blood + Wide Spreads
//	maxFeatureWidth		11					20				typical values
////////////////////////////////////////////////////////////////////////////////////////

//BYTE* CWoolzIP::TextureMetFind(BYTE *image, int width, int height, int maxFeatureWidth)
BYTE* CWoolzIP::TextureMetFind(BYTE *, int , int , int )
{
	//if ((image == NULL) || (width < 5) || (height < 5))
	//	return NULL;

	//int minContrast = 30;	// we may make this a parameter in future

	//BYTE *textureIm = (BYTE *)Calloc(width * height, 1);

	//// imptr1 points to source image data
	//BYTE *imptr1= image;

	//int max1 = 0;
	//int max2 = 0;
	//int min = 255;
	//int level = 0;
	//int max1x  =0;
	//int max2x = 0;
	//int minx = 0;
	//
	//imptr1= image;

	//// first pass -  perform analysis horizontally
	//for (int row=0; row<height; row++)
	//{
	//	imptr1= image + row * width;
	//	max1 = max2 = 0;
	//	min = 255;
	//	max1x  =0;
	//	max2x = 0;
	//	minx = 0;

	//	for (int col=0;col<width;col++)
	//	{
	//		int val=*imptr1++;

	//		val = 255 - val;	// Woolz

	//		if (min==255) // looking for max1
	//		{
	//			if (val > max1+8)
	//			{
	//				max1 = val;
	//				max1x = col;
	//				level = 0;
	//			}
	//			else
	//				if (val < max1-8)
	//				{
	//					max1x += level/2;		// choose mid point of level peak
	//					min = val;
	//					minx = col;
	//					level = 0;
	//				}
	//				else
	//					level++;				// travelling across plateau
	//		}
	//		else
	//		{
	//			if (max2 == 0)	// looking for min
	//			{
	//				if (val < min-8)
	//				{
	//					min = val;
	//					minx = col;
	//					level = 0;
	//				}
	//				else
	//					if (val > min+8)
	//					{
	//						minx += level/2;
	//						max2 = val;
	//						max2x = col;
	//						level = 0;
	//					}
	//					else
	//						level++;
	//			}
	//			else	// looking for max2
	//			{
	//				if (val > max2+8)
	//				{
	//					max2 = val;
	//					max2x = col;
	//					level =0;
	//				}
	//				else
	//					if (val < max2-8)
	//					{
	//						max2x += level/2;

	//						// calculate roughness value and store in textureIm
	//						int v1 = max1 - min;
	//						int h1 = minx - max1x;
	//						int v2 = max2 - min;
	//						int h2 = max2x - minx;
	//						// check roughness has good minContrast - ignore noise
	//						if ((v1+v2 > 2*minContrast) && (h1+h2 < maxFeatureWidth))
	//						{
	//							// check ratio is greater than 0.6
	//							if ((float)min(v1,v2)/(float)max(v1,v2) > 0.6)
	//							{
	//								int ratio = 8*(v1 + v2)/(h1+h2);
	//								if (ratio>255)
	//									ratio = 255;
	//								FSCHAR *textureImptr = minx + row * width + textureIm;
	//								*textureImptr = ratio;
	//							}
	//						}

	//						// and start again
	//						max1 = max2;
	//						max1x = max2x;
	//						max2 = 0;
	//						min = 255;
	//						level = 0;
	//						max2x = 0;
	//						minx = 0;
	//					}
	//					else
	//						level++;
	//			}
	//		}
	//	}	// next col
	//}	// next row


	//// second pass - perform analysis vertically
	//imptr1= image;

	//for (int col=0;col<width;col++)
	//{
	//	imptr1= image + col;
	//	max1 = max2 = 0;
	//	min = 255;
	//	max1x  =0;
	//	max2x = 0;
	//	minx = 0;

	//	for (int row=0; row<height; row++)
	//	{
	//		int val=*imptr1;
	//		val = 255 - val;	// Woolz

	//		imptr1+=width;

	//		if (min==255) // looking for max1
	//		{
	//			if (val > max1+8)
	//			{
	//				max1 = val;
	//				max1x = row;
	//				level = 0;
	//			}
	//			else
	//				if (val < max1-8)
	//				{
	//					max1x += level/2;		// choose mid point of level peak
	//					min = val;
	//					minx = row;
	//					level = 0;
	//				}
	//				else
	//					level++;				// travelling across plateau
	//		}
	//		else
	//		{
	//			if (max2 == 0)	// looking for min
	//			{
	//				if (val < min-8)
	//				{
	//					min = val;
	//					minx = row;
	//					level = 0;
	//				}
	//				else
	//					if (val > min+8)
	//					{
	//						minx += level/2;
	//						max2 = val;
	//						max2x = row;
	//						level = 0;
	//					}
	//					else
	//						level++;
	//			}
	//			else	// looking for max2
	//			{
	//				if (val > max2+8)
	//				{
	//					max2 = val;
	//					max2x = row;
	//					level =0;
	//				}
	//				else
	//					if (val < max2-8)
	//					{
	//						max2x += level/2;

	//						// calculate roughness value and store in textureIm
	//						int v1 = max1 - min;
	//						int h1 = minx - max1x;
	//						int v2 = max2 - min;
	//						int h2 = max2x - minx;
	//						// check rough has good minContrast - ignore noise
	//						if ((v1+v2 > 2*minContrast) && (h1+h2 < maxFeatureWidth))
	//						{
	//							// check ratio is greater than 0.6
	//							if ((float)min(v1,v2)/(float)max(v1,v2) > 0.6)
	//							{
	//								int ratio = 8*(v1 + v2)/(h1+h2);
	//								if (ratio>255)
	//									ratio = 255;
	//								FSCHAR *textureImptr = col + minx * width + textureIm;
	//								if (*textureImptr < ratio)	// keep horizontal value if greater
	//									*textureImptr = ratio;
	//							}
	//						}

	//						// and start again
	//						max1 = max2;
	//						max1x = max2x;
	//						max2 = 0;
	//						min = 255;
	//						level = 0;
	//						max2x = 0;
	//						minx = 0;
	//					}
	//					else
	//						level++;
	//			}
	//		}
	//	}	// next row
	//}	// next col

	//return textureIm;

return 0;
}
	

long CWoolzIP::TreeWalk(structMergeObj* seed, structMergeObj* leaf_list[], BOOL init)
{
	static long leaf_count=0;

	if (init==TRUE) leaf_count=0;

	if (seed==NULL) return leaf_count;

	if (seed->Child1 || seed->Child2)
	{
		TreeWalk(seed->Child1, leaf_list, FALSE);
		TreeWalk(seed->Child2, leaf_list, FALSE);
	}
	else
	{
		 leaf_list[leaf_count++] = seed;
	}
	return leaf_count;
}


void CWoolzIP::ColourCost(structMergeObj** leaf_list, int leaf_count, long* val, long* cost)
{
	long r=0; long g=0; long b=0;
	long avg_r=0; long  avg_g=0; long  avg_b=0;

	for (int i=0; i<leaf_count; i++)
	{
		long v = leaf_list[i]->val;
		r += (unsigned char) (v >> 16);
		g += (unsigned char) (v >>  8);
		b += (unsigned char) (v >>  0);
	}

	avg_r = r/leaf_count;
	avg_g = g/leaf_count;
	avg_b = b/leaf_count;

	long d=0;
	for (int i=0; i<leaf_count; i++)
	{
		long v = leaf_list[i]->val;
		BYTE r = (unsigned char) (v >> 16);
		BYTE g = (unsigned char) (v >>  8);
		BYTE b = (unsigned char) (v >>  0);
		d += sqrt((double)((r-avg_r)*(r-avg_r)+(g-avg_g)*(g-avg_g)+(b-avg_b)*(b-avg_b)));
	}

	*cost = d/leaf_count;
	*val = ((avg_r<<16)+(avg_g<<8)+avg_b);
}

void CWoolzIP::ShapeMoments(structMergeObj** leaf_list, int leaf_count, double ShapeMoments[10])
{
	double Moment[4][4];
	double xc, yc;

	Moment[0][0] = (double)leaf_count;

	// xc and yc
	double xsum=0.0;
	double ysum=0.0;
	for (int i=0; i<leaf_count; i++)
	{
		xsum += (double)leaf_list[i]->col+1;
		ysum += (double)leaf_list[i]->line+1;
	}
	xc = xsum/Moment[0][0];
	yc = ysum/Moment[0][0];

	for (int p=0; p<4; p++)
	{
		for (int q=0; q<4; q++)
		{
			if (p==0 && q==0)
				continue;
			double s=0.0;
			for (int i=0; i<leaf_count; i++)
			{
				double t1 = (double)(leaf_list[i]->col+1 - xc);
				double t2 = (double)(leaf_list[i]->line+1 - yc);
				t1 = pow(t1, p);
				t2 = pow(t2, q);
				s += t1*t2;
			}
			Moment[p][q] = s/Moment[0][0];
		}
	}

	double Compactness=0.0;
	double Eccentricity=1.0;
	double divi = (Moment[2][0] + Moment[0][2]);
	if (divi != 0.0)
	{
		Compactness = Moment[0][0] / (Moment[2][0] + Moment[0][2]);
		Eccentricity = sqrt(((Moment[2][0] - Moment[0][2])*(Moment[2][0] - Moment[0][2]))+4.0*(Moment[1][1]*Moment[1][1]))/(Moment[2][0] + Moment[0][2]);
	}

	ShapeMoments[0] = xc;
	ShapeMoments[1] = yc;
	ShapeMoments[2] = Moment[0][0];
	ShapeMoments[3] = Compactness;
	ShapeMoments[4] = Eccentricity;

}




/****************************************************************
 Call this function to start colouring!
 ****************************************************************/
int FloodFiller::floodfill(int x, int y, BYTE *buffer, BYTE fill_col, BYTE stop_col)
{
	int newx, newy;
	int area=0;

	floodStack.push(x, y);
	while (floodStack.getNum())
	{
		floodStack.peek(&newx, &newy);
		floodStack.pop();
		area += floodseed (newx, newy, buffer, fill_col, stop_col);
	}
	return area;
}

/****************************************************************
 Floodfill seed funtion.  This function is continuously called
 until there's no more seed left in the stack.
 ****************************************************************/
int FloodFiller::floodseed(int x, int y, BYTE * buffer, BYTE fill_col, BYTE stop_col)
{
	int area=0;
	//if error case, exit
	if (x < 0 || x >= dimX || y < 0 || y >= dimY)
		return area;
	
	//if already fill coloured exit
	if (buffer [GETXY(x, y)] == fill_col)
		return area;

	//if already stop coloured exit
	if (buffer [GETXY(x, y)] == stop_col)
		return area;


	//see what's the original colour first.
	BYTE orgCol = buffer[GETXY(x, y)];


	//scan x
	int spotX = x;
	bool bUpClose, bDownClose;

	//check initial up/down open condition
	if (y != 0)
	{
		if (buffer [GETXY(spotX, y - 1)] == orgCol)
			bUpClose = false;
		else bUpClose = true;
	}
	if (y != dimY - 1)
	{
		if (buffer [GETXY(spotX, y + 1)] == orgCol)
			bDownClose = false;
		else bDownClose = true;
	}

	//well, start colouring! 
	buffer[GETXY(x, y)] = fill_col;
	area++;

	//scan left
	while (true)
	{
		spotX --;

		//check exit condition
		if (spotX < 0 || buffer [GETXY(spotX, y)] != orgCol)
		{
			//check diagonal!!!

			// check upper right diagonal
			if (y != 0)
			{
				if (buffer [GETXY(spotX + 1, y - 1)] == orgCol)
					floodStack.push (spotX + 1, y - 1);
			}
			// check lower right diagonal
			if (y != dimY - 1)
			{
				if (buffer [GETXY(spotX + 1, y + 1)] == orgCol)
					floodStack.push (spotX + 1, y + 1);
			}
			break;
		}

		//check upper open->close
		if (y != 0)
		{
			//if suddenly closed
			if (buffer [GETXY(spotX, y - 1)] != orgCol && !bUpClose)
			{
				//plant seed
				floodStack.push (spotX + 1, y - 1);

				bUpClose = true;
			}
			else if (buffer [GETXY(spotX, y - 1)] == orgCol)
				bUpClose = false;
		}
		//check lower open->close
		if (y != dimY - 1)
		{
			//if suddenly closed
			if (buffer [GETXY(spotX, y + 1)] != orgCol && !bDownClose)
			{
				//plant seed
				floodStack.push (spotX + 1, y + 1);

				bDownClose = true;
			}
			else if (buffer [GETXY(spotX, y + 1)] == orgCol)
				bDownClose = false;
		}

		buffer [GETXY(spotX, y)] = fill_col;
		area++;
	}

	spotX = x;

	//check initial up/down open condition
	if (y != 0)
	{
		if (buffer [GETXY(spotX, y - 1)] == orgCol)
			bUpClose = false;
		else bUpClose = true;
	}
	if (y != dimY - 1)
	{
		if (buffer [GETXY(spotX, y + 1)] == orgCol)
			bDownClose = false;
		else bDownClose = true;
	}

	//scan right
	while (true)
	{
		spotX ++;

		//check exit condition
		if (spotX >= dimX || buffer [GETXY(spotX, y)] != orgCol)
		{
			//check diagonal!!!

			// check upper left diagonal
			if (y != 0)
			{
				if (buffer [GETXY(spotX - 1, y - 1)] == orgCol)
					floodStack.push (spotX - 1, y - 1);
			}
			// check lower left diagonal
			if (y != dimY - 1)
			{
				if (buffer [GETXY(spotX - 1, y + 1)] == orgCol)
					floodStack.push (spotX - 1, y + 1);
			}
			break;
		}


		//check upper open->close
		if (y != 0)
		{
			//if suddenly closed
			if (buffer [GETXY(spotX, y - 1)] != orgCol && !bUpClose)
			{
				//plant seed
				floodStack.push (spotX - 1, y - 1);

				bUpClose = true;
			}
			else if (buffer [GETXY(spotX, y - 1)] == orgCol)
				bUpClose = false;
		}
		//check lower open->close
		if (y != dimY - 1)
		{
			//if suddenly closed
			if (buffer [GETXY(spotX, y + 1)] != orgCol && !bDownClose)
			{
				//plant seed
				floodStack.push (spotX - 1, y + 1);

				bDownClose = true;
			}
			else if (buffer [GETXY(spotX, y + 1)] == orgCol)
				bDownClose = false;
		}


		buffer [GETXY(spotX, y)] = fill_col;
		area++;
	}

	return area;
}

//flood stack constructor
FloodStack::FloodStack()
{
	numStack = 0;
	floodSeed = NULL;
}


int FloodStack::peek(int * pX, int * pY)
{
	if (!numStack)
		return 1;

	*pX = floodSeed->x;
	*pY = floodSeed->y;

	return 0;
}


int FloodStack::pop()
{
	if (floodSeed == NULL)
		return 1;

	FLOODSEED * tempSeed;
	tempSeed = floodSeed;
	floodSeed = floodSeed->prev;

	delete tempSeed;
	numStack --;

	return 0;
}


int FloodStack::push(int ix, int iy)
{
	FLOODSEED * tempSeed;
	tempSeed = floodSeed;

	floodSeed = new FLOODSEED;

	floodSeed->prev = tempSeed;
	floodSeed->x = ix;
	floodSeed->y = iy;
	
	numStack ++;

	return 0;
}

int FloodStack::getNum()
{
	return numStack;
}
