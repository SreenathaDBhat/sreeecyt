/*
 *      D D G S . H	    ddgs -EXTERNAL- defintions
 *
 *
 *  Written: Graham J Page
 *           Image Recognition Systems
 *           720 Birchwood Boulevard
 *           Birchwood Science Park
 *           Warrington
 *           Cheshire
 *           WA3 7PX
 *
 * Copyright (c) and intellectual property rights Image Recognition Systems
 * Copyright etc Applied Imaging 1995, 1996
 *
 * Date:    10th June 1987
 *
 * Mods:
 *
 *	07Feb2000	JMB	Added DDGS_API statements. This header now defines the
 *					external interface to the new ddgs DLL.
 *					Also added prototype for oneline_textsize().
 *
 *	18Mar97	BP:	Add mask BITMAPINFO to DDGS structure.
 *	17Mar97	BP:	More "normal" BITMAPINFO structure.
 *				Remove UNIX fonts.
 *	9/26/96	BP:		Added DDGSPoint for platform independence (is
 *						used in woolz).
 *	9/19/96	BP:		New Windows version.
 *	BP	7/20/95:	Added ddgspropen_8bit, and some tidying (needs more!)
 *	BP	9/14/94:	Added depth and visual to DDGS structure for 24-bit.
 *
 *	 4 Mar 1993		MG		added invert_clear(), invert_intens()
 *	 1 Feb 1993		MG		added POINT to mousedraw() shapes
 *	18 Dec 1992		MG		added ddgspropen()
 *	16 Dec 1992		MG		added ddgsismapped()
 *	17 Sep 1992		MG		get_mousept added
 *	 3 Sep 1992		MG		text routines added
 *	 2 Sep 1992		MG		destuctive markers
 *	11 Aug 1992		MG		mouse drawing stuff
 *	 5 Aug 1992		MG		X overlay plane stuff added
 *	31 Jul 1992		MG		X-Windows additions to ddgs struct
 *	11 Jun 1992 	MG		New X Windows version
 *	 4 Apr 1990		CAS		Declare ddgsopen + dup
 *	30 Jan 1990		dcb		Added look up table structure
 *	 3 Mar 1989		CAS		Added a name to DDGS structure
 *	 4 Feb 1988		CAS		Added lutcopy field
 *	 2 Sep 1987		CAS		Added devno field
 * 	12 Jun 1987		GJP		Added center of display coordinates
 */



#ifndef DDGS_H
#define DDGS_H


#include <stdio.h>
#include <windows.h>


/****************************************
 * Begin visual studio generated code ...
 */

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the DDGS_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// DDGS_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef DDGS_EXPORTS
#define DDGS_API 
#else
#define DDGS_API 
#endif

// This class is exported from the ddgs.dll
#ifdef __cplusplus
class DDGS_API CDdgs {
public:
	CDdgs(void);
	// TODO: add your methods here.
};
#endif

extern DDGS_API int nDdgs;

DDGS_API int fnDdgs(void);

/*
 * ... end visual studio generated code.
 ***************************************/


/*************************************
 * This section used to be in common.c 
 */
/*************************** DDGS marker colours ***************************/
/* these are initialised by calling the common.c function SetColours(ddgs) */

#define BLACK		0
#define BLUE		1
#define GREEN		2
#define CYAN		3
#define RED		4
#define MAGENTA		5
#define	YELLOW		6
#define	WHITE		7

#define	MIDGREY		8
#define DARKGREEN	9
#define	VIOLETRED	10
#define DARKGOLDENROD2	11
#define VARCOLOUR	12	/* variable colour */
#define VARCOLOUR2	13

/*
 * End of common.c section
 **************************/



#define ON 			1		/* overlay state */
#define OFF 		0
#define MAXMARKER	32		/* number of destructive marker colours */

/* Increased this from 2048 to accomodate
 * extreme case of 768x4(zoom)x4(24-bit). */
#define MAXDATALENGTH	12288
#define PLANEMASK	63		/* masks grey image planes, not overlays */

/* ddgs mousedraw shapes */
#define DDGSPOINT		1
#define DDGSLINE		2
#define DDGSPOLYLINE	3
#define DDGSRECTANGLE	4
#define DDGSCIRCLE		5
#define DDGSLINE_ONRELEASE 6
#define DDGS_TRIM_ERASER_RECT	7	//draw rectangle for trim eraser operation
#define DDGS_TRIM_ERASER_CIRC	8	//draw circle for trim eraser operation

#define  DDGS_SHAPEACTION_NORMAL 0
#define DDGS_SHAPEACTION_ONRELEASE 1

/* ddgs fonts 0-3 normal, 4-7 bold */
#define MAXFONT		8

 
/*
 *	Look up table structure
 */
typedef struct {
	WORD version;
	WORD numEntries;
	PALETTEENTRY val[256];
} DDGSLOGPALETTE;


/*
 *	Point structure.
 * Note that this is defined the same as
 * the windows POINT structure so can be
 * cast as such.
 */
typedef struct {
	LONG x;
	LONG y;
} DDGSPoint;


/*
 *	Sensible version of BITMAPINFO
 */
typedef struct {
	BITMAPINFOHEADER head;
	RGBQUAD col[256];	// Defines linear of invert grey mapping.
} DDGSBITMAPINFO;


/*
 * Packed (3-byte) RGB triplet.
 */
typedef struct {
	BYTE b, g, r;
} DDGSRGB;

typedef struct {
			int MaxX;					//image x dimension
			int MaxY;					//image y dimension
			float Magnification;			//magnification factor
			double XMousePt;			//x mouse point in image 
			double YMousePt;			//y mouse point in image
			double XOffset;				//x offset of display origin from image  origin
			double YOffset;				//y offset of display origin from image  origin
			double Height;				//display(magnified) image height
			double Width;				//display(magnified) image width
			BOOL pixel_interpolation;	//switch for pixel interpolation
			BOOL XCohu;					//set to 1 to exclude cohu scale, set to 0 to include cohu scale
			} DDGSImageFormat;

/*
 *	DDGS structure
 */
typedef struct ddgs_control {
	int pseudocolour;		/* 1=pseudocolour display, 0=monochrome */
	int minx;				/* Physical frame store pixel minx */
	int maxx;				/* Physical frame store pixel maxx */
	int miny;				/* Physical frame store pixel miny */
	int maxy;				/* Physical frame store pixel maxy */

	int xshift;					/* Scaling - always 3! */
	int yshift;

	HWND      	window;			/* Window handle */
	HDC 		dc;				/* Device context id */
	HBITMAP		map;			/* window backup bitmap */
	HRGN		cliprgn;		/* clip rectangle defined by window */
	int 		cur_colour;		/* current colour */
	int			cur_intensity;	/* current intensity */
	int 		cur_pixelmode;	/* current pixel mode */
	int 		cur_x;			/* DDGS current x/y pos */
	int 		cur_y;
	BOOL 		invert;
	BOOL		backup;			/* True => ddgs ops backed up to pixmap */
	COLORREF	ovlycol;		/* overlay plane colour */
	HPEN		ovlypen;
	HPEN		blackpen;
	HPEN		whitepen;
	COLORREF	markcol[MAXMARKER];	/* RGB colours for destructive markers */
	HPEN		markpen[MAXMARKER];
	int			cur_marker;		/* current destructive marker */
	int			cur_lwidth;
	int			shape;			/* mouse drawing shape */
	int			shapeaction;	/* extended functions for shape drawing, 0 is as normal */
	int			undo_npixels; /* undo on mousepoly draw*/
	int			nmousepts;		/* num of mouse pts drawn */
	DDGSPoint	*mousepts;		/* ptr to list of mouse pts */
	DDGSBITMAPINFO binfo;		/* DIB drawing structure */
	DDGSBITMAPINFO minfo;		/* DIB mask structure */
	HFONT		font;			/* Currently selected font. */
	HANDLE		himage;			/* DIB drawing buffer (only used with DDGSBUFFERED) */
	unsigned int  invertBG;
	unsigned int  normalBG;
	HBRUSH fillbrush;
	int			TrimEraserSize;	//size of trim eraser
	double		TrimEraserScale;
	DDGSImageFormat 	ddgsImageFormat;			//scale/zoom/pan image and fit to display window
} DDGS;


/*
 * DDGS Routines
 */

#ifdef __cplusplus
extern "C" {
#endif


/**** ddgs.c ****/

DDGS_API void ddgsInit();
DDGS_API void ddgsSetNormalBGColour(unsigned int col);
DDGS_API void ddgsSetInvertBGColour(unsigned int col);
DDGS_API void ddgsGetNormalBGColour(unsigned int *col);
DDGS_API void ddgsGetInvertBGColour(unsigned int *col);
DDGS_API DDGS *ddgsopen(HWND window, int width, int height);
DDGS_API DDGS *ddgsreopen(DDGS *existingdg, HWND window, int width, int height);
DDGS_API DDGS *ddgspropen(HDC dc, int width, int height);
DDGS_API BOOL ddgsismapped();
DDGS_API void selectddgs(DDGS *dg);
DDGS_API void ddgsclose(DDGS *dg);
DDGS_API DDGS *ddgsdup(DDGS *dg);
DDGS_API DDGS *getcurdg();
DDGS_API void backup(int status);
DDGS_API void colour(int col);
DDGS_API void clear();
DDGS_API void invert_clear();
DDGS_API void erase(int x, int y, int w, int h);
DDGS_API void invert_erase(int x, int y, int w, int h);
DDGS_API void intens(int val);
DDGS_API void invert_intens();
DDGS_API void restore_intens();
DDGS_API void pseudodg();
DDGS_API void monodg();

// Event callbacks.
DDGS_API void ddgsExposeWindow(DDGS *dg);
DDGS_API void ddgsColormapChange();


/**** ddgsline.c ****/

DDGS_API void lwidth(int w);
DDGS_API void moveto(int x, int y);
DDGS_API void moveby(int x, int y);
DDGS_API void fillcolour(int colour);


/**** ddgspix.c ****/

DDGS_API unsigned char *ddgs_get_aux_buffer(int size);
DDGS_API unsigned char *ddgs_get_draw_buffer(int size);
DDGS_API unsigned char *ddgs_get_draw_mask(int w, int h);
DDGS_API void ddgs_free_buffers();

DDGS_API void window(int xl, int xr, int yb, int yt);
DDGS_API void setrgb(int col, int r, int g, int b);
DDGS_API void fastpixobj(int vx, int vy, int w, int h, unsigned char *vdata, unsigned char *mdata, short where);
DDGS_API void fastpixobj24(int vx, int vy, int w, int h, unsigned char *vdata, unsigned char *mdata);
DDGS_API int  pixelmode(int m);
DDGS_API void nfastpixobj(int vx, int vy, int w, int h, int dx,int dy, int dw, int dh, unsigned char *vdata, unsigned char *mdata, short where);

DDGS_API void display_image (int vx, int vy, int w, int h,int dispx,int dispy, int dispw, int disph,unsigned char *vdata, unsigned char *mdata, DWORD maskop, DWORD imageop,short where);
DDGS_API void display_image_GDIplus (int vx, int vy, int w, int h,int dispx,int dispy, int dispw, int disph,unsigned char *vdata, unsigned char *mdata,DWORD maskop, DWORD imageop, short where);

DDGS_API void nfastpixobj24(int vx, int vy, int w, int h,int dispx,int dispy, int dispw, int disph,unsigned char *vdata, unsigned char *mdata,short where);

DDGS_API void nfastpixobj(int vx, int vy, int w, int h,
							int dispx,int dispy, int dispw, int disph, 
							unsigned char *vdata, unsigned char *mdata, short where);

DDGS_API void StartGDIplus();
DDGS_API void StopGDIplus();

/**** ddgstext.c ****/

DDGS_API void init_fonts();
DDGS_API void free_fonts();
DDGS_API void setfont(int fontnum);
DDGS_API void setTrueFont(short pointsize);
DDGS_API void oneline_textsize(LPCTSTR s, int *width, int *height, int scale);
DDGS_API void textsize(LPCTSTR s, int *width, int *height);
DDGS_API void textsizeScale(LPCTSTR s, int *width, int *height, int scale);
DDGS_API HFONT _makefont(short id, short scale);


/**** ddgsovly.c ****/

DDGS_API void ddgsoverlay(int state);
DDGS_API void oclear();
DDGS_API void osetrgb(int r, int g, int b);

DDGS_API void olineto(int x, int y);
DDGS_API void odrawpoly(DDGSPoint *xpt, int npts, int fill);
DDGS_API void odrawcircle(int x, int y, int r);
DDGS_API void olineby(int x, int y);
DDGS_API void obox(int x, int y);
DDGS_API void ofill(int x, int y, int width, int height);
DDGS_API void otext(int x, int y, LPCTSTR s, short scale);

DDGS_API void mcolour(int marker);
DDGS_API void msetrgb(int marker, int r, int g, int b);
DDGS_API void load_markers(DDGS *dg);

DDGS_API void mlineto(int x, int y);
DDGS_API void mdrawpoly(DDGSPoint *xpt, int npts, int fill);
DDGS_API void mdrawcircle(int x, int y, int r);
DDGS_API void mlineby(int x, int y);
DDGS_API void mbox(int x, int y);
DDGS_API void mfill(int x, int y, int width, int height);
DDGS_API void mtext(int x, int y, LPCTSTR s, short scale);

DDGS_API void lineto(int x, int y);
DDGS_API void drawpoly(DDGSPoint *xpt, int npts);
DDGS_API void drawcircle(int x, int y, int r);
DDGS_API void lineby(int x, int y);
DDGS_API void box(int x, int y);
DDGS_API void fill(int x, int y, int width, int height);
DDGS_API void text(int x, int y, LPCTSTR s, short scale);


/**** ddgsmouse.c ****/

// Callbacks. Return TRUE if dogs is busy (eg
// drawing a shape).
DDGS_API BOOL ddgsMouseMove(DDGS *dg, long x, long y);
DDGS_API BOOL ddgsMouseRelease(DDGS *dg, long x, long y, short button);
DDGS_API BOOL ddgsMouseBusy(DDGS *dg);
DDGS_API BOOL ddgsTrimEraserOverlay (DDGS *dg);

// OK, same as UNIX/X version - return when finished.
DDGS_API void mousedraw(int shape, DDGS *dg);


/**** ddgsimage.c ****/

DDGS_API void ddgsImageDepth(DDGS *dg, short depth);
DDGS_API void ddgsImageClear(DDGS *dg, BYTE g);
DDGS_API void ddgsImageErase(DDGS *dg, BYTE g, RECT *rect);
DDGS_API void ddgsImageDrawGrey(DDGS *dg, BYTE *src, RECT *rect);
DDGS_API void ddgsImageDrawColor(DDGS *dg, BYTE *src, RECT *rect, DDGSRGB *lut, BOOL addmode);


/**** ddgsideo.c ****/

#define DDGSIDEO_LRHATCH  0x01
#define DDGSIDEO_RLHATCH  0x02
#define DDGSIDEO_VHATCH   0x04
#define DDGSIDEO_SELECT   0x80

DDGS_API void DDGSBeginIdeo();
DDGS_API void DDGSEndIdeo(BOOL backgroundIsBlack);
DDGS_API BOOL DDGSBeginIdeoPoly(int x, int y);
DDGS_API void DDGSIdeoPolyVertex(int x, int y);
DDGS_API void DDGSEndIdeoPoly(unsigned char r, unsigned char g, unsigned char b,
                              unsigned char properties/*, BOOL backgroundIsBlack*/);


// Palette definitions, so applications can use
// the ddgs palette.
DDGS_API extern HPALETTE ddgs_palette;
DDGS_API extern COLORREF ddgs_cornsilk;
DDGS_API extern COLORREF ddgs_darkslategrey;
DDGS_API extern COLORREF ddgs_lightslategrey;
DDGS_API extern COLORREF ddgs_banana;


/**** ddgsimageformat.cpp ****/
DDGS_API BOOL imagetoscreen(int *x, int *y, int *w, int *h, double *scale,double *scale_cohu, BOOL invert_);
DDGS_API void screentoimage(double *x, double *y);
DDGS_API double to_cohu_scale();
DDGS_API BOOL imagetoscreen_pt_scale(int *x, int *y, double *scale,double *scale_cohu, BOOL invert_);
DDGS_API BOOL imagetoscreen_pt(int *x, int *y, BOOL invert_);
DDGS_API void imagescales (double *xscale, double *yscale);

#ifdef __cplusplus
}
#endif


#endif












