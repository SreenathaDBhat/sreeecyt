/*
 * ddgsideo.cpp
 *
 * JMB May - Aug 2000
 *
 * Functions for drawing ideogram bands.
 *
 * Mods:
 *
 *	08Sep2000	JMB	Always draw band outlines in the same shade of grey,
 *					whatever the background colour. This means that the band
 *					lengths do not appear to change as the background changes,
 *					by the outlines appearing to be a part of different bands,
 *					depending on its colour. Also the outline is less likely to
 *					appear to be part of a band if it is not one of the common
 *					band colours (black or white). This means that the problem
 *					of small bands seeming to disappear at small scales because
 *					their outline is taking up the entire length of the band
 *					(if it is 2 pixels or less) and the adjacent bands are the
 *					outline colour (e.g. a small white band with a black outline
 *					with adjacent black bands) is less likely.
 *					I think the biggest potential problem with this is that the
 *					line making up a stalk band will not be visible on a
 *					background which is the same grey as the outline colour.
 */

#include "stdafx.h"
#include "ddgs.h"
#include "ddgsdefs.h"


static HDC dc = NULL;
static HRGN hOutlineRgn = 0;



// DDGSBeginIdeo() and DDGSEndIdeo() and associated changes to DDGSEndIdeoPoly()
// are for drawing an outline around a whole ideogram, instead of drawing an
// outline around each band, as this was leading to	undesirable	effects such as
// small white bands seeming to disappear at small scales, as the black outline
// was taking up all of	the	band's pixels, making it appear black. Even at
// larger scales, white bands appeared to be smaller than they were, because
// a black outline could appear to be part of an adjacent black band (making
// the black band appear larger also). What's more, inverting the outline
// colour to white when on a black background had the opposite effect - making
// the white bands appear larger and the black bands smaller - so the bands
// appeared to change in size when changing the background.
// Unfortunately for some reason DDGSEndIdeo() is slow and flickery at angles
// of rotation that are not close to vertical or horizontal! This can get very
// slow at large scales. Also, it is still necessary to have outlines around
// hatched bands, but these outlines are not coincident with the overall
// outline, meaning that the overall outline appears thicker around hatched bands.
// These problems mean I have decided not to use this code for now.



void
DDGSBeginIdeo()
{
/*
	if (hOutlineRgn != 0)
	{
		DeleteObject(hOutlineRgn);
		hOutlineRgn = 0;
	}
*/
}

void
DDGSEndIdeo(BOOL backgroundIsBlack)
{
/*
	if (dc = DOG_DC(curdg))
	{
		if (backgroundIsBlack)
			FrameRgn(dc, hOutlineRgn, (HBRUSH)GetStockObject(WHITE_BRUSH), 1, 1);
		else
			FrameRgn(dc, hOutlineRgn, (HBRUSH)GetStockObject(BLACK_BRUSH), 1, 1);

		DOG_RDC(curdg);
	}
*/
}



BOOL
DDGSBeginIdeoPoly(int x, int y)
{
	if (curdg == NULL) return FALSE;

	if (dc = DOG_DC(curdg))
	{
		BeginPath(dc);

		// DDGS coordinates are scaled by 8 and inverted, 
		// relative to screen coordinates.
		MoveToEx(dc, x >> 3, curdg->maxy - (y >> 3), NULL); 
	}
	
	if (dc != NULL) return TRUE;
	else            return FALSE;
}


void
DDGSIdeoPolyVertex(int x, int y)
{
	if (curdg == NULL) return;

	// DDGS coordinates are scaled by 8 and inverted, 
	// relative to screen coordinates.
	LineTo(dc, x >> 3, curdg->maxy - (y >> 3)); 
}


void
DDGSEndIdeoPoly(unsigned char r, unsigned char g, unsigned char b, 
                unsigned char properties/*, BOOL backgroundIsBlack*/)
{
// Note that this function never draws anything in white (255,255,255), because
// the Canvas function CreateIdeogramWoolzObject() defines that as the
// background colour which is subtracted when creating ideogram woolz objects.
// Instead, any occurence of (255,255,255) is adjusted to (254, 254, 254).
// Note that if this function ever needs to fill or stroke the path more than
// once, only the first fill/stroke will succeed unless the device context is
// saved (with SaveDC()) before the fill/stroke then restored before the next
// one, as filling/stroking seems to do something to the path (destroys it?).
	if (curdg == NULL) return;

	if (dc != NULL)
	{
		EndPath(dc);
/*
		//
		// Add path for this band to outline region for entire ideogram,
		// as drawn by DDGSEndIdeo().
		// Save DC before converting path to region, as that will discard it
		// from the DC, and we would be able to use it again to fill it etc.
		//
		int savedDC = SaveDC(dc);

		HRGN hNewRgn = PathToRegion(dc);
		if (hOutlineRgn != 0)
		{
			CombineRgn(hOutlineRgn, hOutlineRgn, hNewRgn, RGN_OR);
			DeleteObject(hNewRgn);				
		}
		else
			hOutlineRgn = hNewRgn;

		RestoreDC(dc, savedDC);
*/

		COLORREF cref;
		HBRUSH hBrush    = NULL;
		HBRUSH hOldBrush = NULL;
		HPEN   hPen      = NULL;
		HPEN   hOldPen   = NULL;

		if (properties & DDGSIDEO_SELECT)
		{
		// This is a selection rectangle - draw in red.
			cref = RGB(255, 0, 0);
			hPen = CreatePen(PS_SOLID, 2, cref);
			hOldPen = (HPEN)SelectObject(dc, hPen);

			StrokePath(dc);
		}
		else if (properties) // Only other properties are hatch types.
		{
		// TODO: To improve hatching, e.g. arbitary angles and thickness,
		// define the path as a clip path (with SelectClipPath()) then draw
		// lines, e.g. (0,1)->(1,0), (0,2)->(2,0) etc, to create the hatching.
			// Want black hatching on a white background, unless hatching
			// type is DDGSIDEO_RLHATCH, in which case we want white
			// hatching on a black background. The latter is used for
			// centromeric heterochromatin bands. White is intentionally
			// (254, 254, 254) - see comment at top of function.
/*
Don't do this - it's not very clear at small scales.
			if (     properties & DDGSIDEO_RLHATCH
			    && !(properties & DDGSIDEO_LRHATCH))
			{
				SetBkColor(dc, RGB(0, 0, 0));
				cref = RGB(254, 254, 254);
			}
			else
*/
			{
				SetBkColor(dc, RGB(254, 254, 254));
				//cref = RGB(0, 0, 0);
				// Do hatching in 50% grey, like the outline.
				cref = RGB(128, 128, 128);
			}


			// Create hatching brush.
			if (   (properties & DDGSIDEO_LRHATCH)
			    && (properties & DDGSIDEO_RLHATCH))
				hBrush = CreateHatchBrush(HS_DIAGCROSS, cref); // Diag crosshatch
			else if (properties & DDGSIDEO_LRHATCH)
				hBrush = CreateHatchBrush(HS_FDIAGONAL, cref);
			else if (properties & DDGSIDEO_RLHATCH)
				hBrush = CreateHatchBrush(HS_BDIAGONAL, cref);
			else if (properties & DDGSIDEO_VHATCH)
				hBrush = CreateHatchBrush(HS_VERTICAL,  cref);
/*
// scanlines must be word aligned
UINT bitArray[8];
bitArray[0] = 0xAAAAAAAA;
for (int i=1; i < 8; i++) bitArray[i] = bitArray[i-1] << 1;
HBITMAP hbmp = CreateBitmap(8, 8, 1, 1, bitArray);
hBrush = CreatePatternBrush(hbmp);
*/
			hOldBrush = (HBRUSH)SelectObject(dc, hBrush);

			// Do outline of hatched area in same colour as hatching.
			hPen = CreatePen(PS_SOLID, 0, cref);
			hOldPen = (HPEN)SelectObject(dc, hPen);
			
			// Draw outline of path with current pen and fill with current brush.
			StrokeAndFillPath(dc);
		}
		else
		{
			// Adjust if white - see comment at top of function.
			if (r == 255 && g == 255 && b == 255) cref = RGB(254, 254, 254);
			else                                  cref = RGB(r, g, b);

			hBrush = CreateSolidBrush(cref); 
			hOldBrush = (HBRUSH)SelectObject(dc, hBrush);


/*
			// Outline is black unless background is black, in which case it is white.
			// White is intentionally (254, 254, 254) - see comment at top of function.
			if (backgroundIsBlack) cref = RGB(254, 254, 254);
			else                   cref = RGB(0, 0, 0);
			hPen = CreatePen(PS_SOLID, 0, cref);
			hOldPen = SelectObject(dc, hPen);
*/
// Outline is 50% grey.	This shows up on black or white backgrounds
// (but not 50% grey backgrounds, though this will only be a problem if there are 50% grey bands too).
// It also seems to give the appearence of solving the 'disappearing small bands' phenomenon, so long as neither of the bands either side of the small band are 50% grey.
			cref = RGB(128, 128, 128);
			hPen = CreatePen(PS_SOLID, 0, cref);
			hOldPen = (HPEN)SelectObject(dc, hPen);


			// Fill path with current brush.
			//FillPath(dc);
			StrokeAndFillPath(dc);
		}

		// Clean up
		SelectObject(dc, hOldBrush);
		DeleteObject(hBrush);

		SelectObject(dc, hOldPen);
		DeleteObject(hPen);


		DOG_RDC(curdg);
	}
}





