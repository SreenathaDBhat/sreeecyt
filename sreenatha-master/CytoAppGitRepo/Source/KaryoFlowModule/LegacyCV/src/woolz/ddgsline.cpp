/*
 * 	ddgsline.c	: Mark Gregson 11/8/92
 *
 *	X-Windows line drawing, box and filling routines
 *
 * Modifications:
 *
 *	09Mar2000	JMB	Converted to .cpp.
 *	JMB	07Mar2000	Changed headers included, as part of conversion to DLL.
 *	MG	26Jul99		Add ddgs NULL checking at start of a number of routines 
 *	BP	11/29/95:	Fill in gaps in polygon on Univision systems.
 *	BP	8/24/95:	Allow for NULL window to draw in pixmap only.
 *	BP	12/10/94:	No actual changes here, but XDrawLines() will now
 *					work properly in 24-bit 'overlay' because of change
 *					to dogs gc cap_style parameter to CapNotLast.
 */
#include "stdafx.h"
#include "ddgs.h"
#include "ddgsdefs.h"


/****************** PUBLIC FUNCTIONS ******************/

void 
fillcolour(int colour) /* -1 removes fill */
{
	if (curdg == NULL)
		return;

	if (curdg->fillbrush)
	{
		DeleteObject(curdg->fillbrush);
		curdg->fillbrush = NULL;
	}

	if (colour >= 0)
		curdg->fillbrush = CreateSolidBrush(curdg->markcol[colour]);
}
/*
 * lwidth
 * set lineto and lineby pixel drawing widths
 * This is done by re-creating the marker and
 * overlay pens. Note that the greyscale pens
 * are created on the fly anyway.
 */

void
lwidth(int w)
{
	int i;

	if (curdg == NULL)
		return;

	// Do nothing if the width is already
	// the same. Important to do this because
	// changing width requires destruction and
	// re-creation of the pens.
	if (curdg->cur_lwidth == w)
		return;

	curdg->cur_lwidth = w;

	// Re-create all the marker pens.
	for (i = 0; i < MAXMARKER; i++)
	{
		DeleteObject(curdg->markpen[i]);
		curdg->markpen[i] = CreatePen(PS_SOLID|PS_ENDCAP_FLAT | PS_JOIN_MITER, curdg->cur_lwidth, curdg->markcol[i]);
	}

	// Re-create the overlay pen.
	DeleteObject(curdg->ovlypen);
	curdg->ovlypen = CreatePen(PS_SOLID|PS_ENDCAP_FLAT | PS_JOIN_MITER, curdg->cur_lwidth,
		DOG_OVLYMASK(curdg));

	// And the black/white BG pens (which
	// may be pseudocolour).
	DeleteObject(curdg->whitepen);
	DeleteObject(curdg->blackpen);
	if (curdg->pseudocolour)
	{
		curdg->blackpen = CreatePen(PS_SOLID|PS_ENDCAP_FLAT | PS_JOIN_MITER, curdg->cur_lwidth,
			PSEUDOBGRGB);
		curdg->whitepen = CreatePen(PS_SOLID|PS_ENDCAP_FLAT | PS_JOIN_MITER, curdg->cur_lwidth,
			PSEUDOBGRGB);
	}
	else
	{
		curdg->blackpen = CreatePen(PS_SOLID|PS_ENDCAP_FLAT | PS_JOIN_MITER, curdg->cur_lwidth,
			PALETTERGB(0, 0, 0));
		curdg->whitepen = CreatePen(PS_SOLID|PS_ENDCAP_FLAT | PS_JOIN_MITER, curdg->cur_lwidth,
			PALETTERGB(255, 255, 255));
	}
}

	
/*
 * moveto
 */
void
moveto(int x, int y)
{
	if (curdg == NULL)
		return;

	curdg->cur_x = x;
	curdg->cur_y = y;
}


/*
 * moveby
 */
void
moveby(int x, int y)
{
	if (curdg == NULL)
		return;

	curdg->cur_x += x;
	curdg->cur_y += y;
}


/****************** INTERNAL FUNCTIONS ******************/


/*
 * lineto
 */
void
_lineto(HDC dc, int x, int y)
{
	if (curdg == NULL)
		return;

	MoveToEx(dc, curdg->cur_x >> 3, curdg->maxy - (curdg->cur_y >> 3), NULL); 
	LineTo(dc, x >> 3, curdg->maxy - (y >> 3)); 

	curdg->cur_x = x;
	curdg->cur_y = y;
}


/*
 * drawcircle
 */
void
_drawcircle(HDC dc, int x, int y, int r)
{
	if (curdg == NULL)
		return;

	x = x >> 3;
	y = curdg->maxy - (y >> 3);

	if ((r = r >> 3) < 1)
		r = 1;

	Arc(dc, x - r, y - r, x + r, y + r,
		x, y - r, x, y - r);
}


/*
 * drawpoly
 *
 * Note that DDGSPoint is the same as the
 * windows POINT strucrture, so we can cast
 */
void
_drawpoly(HDC dc, DDGSPoint *xpt, int npts)
{
	// Probably need to do some coordinate
	// conversion in here.....
	Polyline(dc, (POINT *)xpt, npts);
}

void
_drawfillpoly(HDC dc, DDGSPoint *xpt, int npts)
{
	// will use the fillbrush
	Polygon(dc, (POINT *)xpt, npts);
}


/*
 * lineby
 */
void
_lineby(HDC dc, int x, int y)
{
	if (curdg == NULL)
		return;

	MoveToEx(dc, curdg->cur_x >> 3, curdg->maxy - (curdg->cur_y >> 3), NULL); 

	curdg->cur_x += x;
	curdg->cur_y += y;

	LineTo(dc, curdg->cur_x >> 3, curdg->maxy - (curdg->cur_y >> 3)); 

}


/*
 * box
 *
 * Box centred on current position.
 */
void
_box(HDC dc, int w, int h)
{
	int w_2, h_2;

	if (curdg == NULL)
		return;

	w_2 = (w + 1) >> 1;
	h_2 = (h + 1) >> 1;

	moveby(-w_2, -h_2);
	_lineby(dc, 0, h);
	_lineby(dc, w, 0);
	_lineby(dc, 0, -h);
	_lineby(dc, -w, 0);
	moveby(w_2, h_2);
}


/*
 * fill
 *
 * fills window area with current colour using current 
 * drawing mode set by intens().
 */
void
_fill(HDC dc, int x, int y, int width, int height)
{
	int fw, fh;
	int i;

	if (curdg == NULL)
		return;

	fw = (width + 4 ) >> 3; 	/* + 4 forces round up, XSHIFT always 3 */
	fw = (fw > 0) ? fw : 1;
	fh = (height + 4 ) >> 3;	/* + 4 forces round up, YSHIFT always 3 */
	fh = (fh > 0) ? fh : 1;

	// Currently fill by using lines, so
	// that we use the current pen. Not the
	// most efficient way - we really should
	// be using FillRect with a brush.
	// Probably better to have special ifill,
	// ofill and mfill functions instead of
	// this generic one, where we could create
	// brushes on the fly.

	x >>= 3;
	y >>= 3;
	for (i = 0; i < fh; i++)
	{
		moveto(x, curdg->maxy - y - i);
		_lineto(dc, x + fw, curdg->maxy - y - i);
	}
}

