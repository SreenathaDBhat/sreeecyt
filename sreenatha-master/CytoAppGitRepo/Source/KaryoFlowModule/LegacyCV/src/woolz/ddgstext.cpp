/*
 * 	ddgstext.c	: Mark Gregson 11/8/92
 *
 *	X-Windows text display routines
 *
 * Mods:
 *
 *	09Mar2000	JMB	Converted to .cpp.
 *	07Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *	26Jul99	MG		Add ddgs NULL checking at start of a number of routines 
 *	14Oct98	SN		Made _makefont nonstatic function.
 *	10Jan97	BP:		Rework for Windows with proportianal fonts etc.
 *					See comments in body. New free_fonts() function.
 *	BP	12/10/94:	Just cleanup - no functional changes.
 * 	 3 Sep 1992	MG 	textsize, loadfont routines added
 *	20 Oct 1993 MG	textsize modified to handle newlines
 */
#include "stdafx.h"
#include "ddgs.h"
#include "ddgsdefs.h"

#include <string.h>
#include <tchar.h>


#define	DDGSFONTNAME	"MS Sans Serif"


// This is our current font. There is only one,
// which is re-created every time a text draw
// request comes in with a different font size.
// This font resource is only re-allocated when
// the required size changes.
static HFONT font = NULL;


// Pair of fonts used to calculate text canvas
// sizes - one for bold and one for normal.
// These are based on the largest pointsize (31), and
// other-sized text sizes are calculated by scaling
// the result.
static HFONT font_norm = NULL;
static HFONT font_bold = NULL;


// The current font ID. This is used to detect
// when the font size changes and a new font
// needs to be created.
static short fontid = 2;
static short gFontPoint = 0;


// These should be OK - but point sizes don't seem
// to be the same as in X, so I scale them by about 30%...
//static short pointsize[4] = {10, 12, 16, 24};
static short pointsize[4] = {12, 15, 21, 31};
static short ext_pointsize[] = {1,  48, 72}; // note that index 0 isn't used for extended font size
static TCHAR *ext_fontnames[] = {_T("MS Sans Serif"),
								_T("Fixedsys"),
								_T("Arial"), 
								_T("Comic Sans MS"), 
								_T("Courier New"), 
								_T("Impact"), 
								_T("Georgia"), 
								_T("Times New Roman"), 
								_T("Tahoma"),
								_T("Trebuchet MS"),
								_T("Verdana")};

/*
 * Initialise the font system, by loading up
 * all the required fonts.
 * Retained for compatibility only.
 */
void init_fonts()
{
	return;
}

// ====================================== Get current display scale
static double getScale()
{
	int x, y;
	double ss, sc;
	imagetoscreen_pt_scale (&x,&y,&ss,&sc, TRUE);
	return ss;
}
/*
 * Free the globally-allocated font resources.
 */
void free_fonts()
{
	if (font)
	{
		DeleteObject(font);
		font = NULL;
	}

	if (font_norm)
	{
		DeleteObject(font_norm);
		font_norm = NULL;
	}

	if (font_bold)
	{
		DeleteObject(font_bold);
		font_bold = NULL;
	}
}


/*
 * Create a new font resource. Its up to the
 * caller to decide whether we need to create
 * a new one or reuse the current one - this
 * function just does it, and does not have
 * memory (other than the LOGFONT contents).
 *
 *	SN 14Oct98	MAde nonstatic function so
 *				mfish\mflabel.cpp has access to it.
 *
 */
//static HFONT _makefont(short id, short scale)
HFONT _makefont(short id, short scale)
{
return 0;
}

//=============================== Using GDI funcs, fast and pretty good =================
static void CalculateClassicDimensions(int & lWidth, int & lHeight, HFONT hFont, const TCHAR* szText)
{

	HDC hDCScreen=GetDC(NULL);
	HFONT hFontOld=(HFONT)SelectObject(hDCScreen,hFont);

	//calculate the width of the string using the 'classic' way
	SIZE sizeText;
	GetTextExtentPoint32(hDCScreen, szText, lstrlen(szText), &sizeText);

	lHeight=sizeText.cy;//lHeight==0 if the text is empty, so try GetTextMetrics below!
	lWidth=sizeText.cx;

	TEXTMETRIC tm;
	GetTextMetrics(hDCScreen,&tm);//this helps if the text is empty!
	lHeight=tm.tmHeight;//no space between the lines, doesn't need +tm.tmExternalLeading;

	if(0==lstrlen(szText))
	{
		SelectObject(hDCScreen,hFontOld);
		ReleaseDC(NULL,hDCScreen);	
		return;
	}

	double dOverhangTrailing=0;
	ABCFLOAT WidthsABC[256];

	//on Windows 98, GetCharABCWidthsFloat fails
	//for this operating system, use GetCharABCWidths instead

	BOOL bResult=GetCharABCWidthsFloat(hDCScreen,0,255,WidthsABC);

	if(bResult)
	{
		dOverhangTrailing=WidthsABC[szText[lstrlen(szText)-1]].abcfC;
		if(dOverhangTrailing<0)
		{
			lWidth-=(int)dOverhangTrailing; // Last character's overhang
        }
	}

	SelectObject(hDCScreen,hFontOld);
	ReleaseDC(NULL,hDCScreen);	
}
//=============================== Using a raster scan, pixel perfect but too slow =================
static void CalculateSmartDimensions(int &lWidth, int& lHeight, HFONT hFont,const TCHAR* szText)
{
	HDC hDCScreen=GetDC(NULL);
	HDC hDCMem=CreateCompatibleDC(hDCScreen);
	ReleaseDC(NULL,hDCScreen);

	HFONT hFontOld=(HFONT)SelectObject(hDCMem,hFont);

	//calculate the width of the string using the classic way
	SIZE sizeText;
	GetTextExtentPoint32(hDCMem, szText, lstrlen(szText), &sizeText);

	lHeight=sizeText.cy;//lHeight==0 if the text is empty, so try GetTextMetrics below!
	lWidth=sizeText.cx;

	if(NULL==szText || _T('\0')==szText[0])
    {
		TEXTMETRIC tm;
		GetTextMetrics(hDCMem,&tm);//this helps if the text is empty!
		lHeight=tm.tmHeight;//no space between the lines, doesn't need +tm.tmExternalLeading;

		SelectObject(hDCMem,hFontOld);
		DeleteDC(hDCMem);
		return;
	}

	SIZE sizeLastCharacter;
    GetTextExtentPoint32(hDCMem, &szText[-1+lstrlen(szText)], 1, &sizeLastCharacter);

      //enough to fit the text
    RECT rect={0,0,lWidth+sizeLastCharacter.cx,lHeight};
	
	HBITMAP hBitmap= CreateCompatibleBitmap ( hDCMem, rect.right-rect.left, rect.bottom-rect.top);

	HBITMAP hOldBitmap=(HBITMAP)SelectObject ( hDCMem, hBitmap );

	//fill with white
	FillRect(hDCMem,&rect,(HBRUSH)GetStockObject(WHITE_BRUSH));
	DrawText(hDCMem,szText,-1,&rect,DT_LEFT|DT_TOP|DT_SINGLELINE|DT_NOPREFIX);

	int iXmax=0;
	BOOL bFound=FALSE;
	for(int x=rect.right-1;x>=0 && !bFound ;x--)	
	{
		for (int y=0;y<=rect.bottom-1 && !bFound;y++)	
		{
			COLORREF rgbColor=GetPixel(hDCMem,x,y);

			if(rgbColor!=RGB(255,255,255))
			{
				//found a non-white pixel, save the horizontal position
				//and exit the loop. Job finished.
				iXmax=x;
				bFound=TRUE;
			}
		}//endfor
	}//endfor	
	
	//here we have the width of the painted text
	lWidth=iXmax+1;//+1 because we use 0-based indexes

	SelectObject(hDCMem,hFontOld);
	SelectObject(hDCMem,hOldBitmap);
	DeleteObject(hBitmap);
	DeleteDC(hDCMem);
}

/*
 * text
 * print text at current position, using current colour
 * and intensity mode current position is treated as
 * bottom left corner of bounding box for the string
 */
void
_text(HDC dc, int x, int y, LPCTSTR s, short scale)
{
	HGDIOBJ oldfont;
	static short prev_fontid = -1;
	static short prev_scale = -1;

	if (!curdg)
		return;

	if (font)
		DeleteObject(font);
	font = _makefont(fontid, scale);
	curdg->font = font;

	x = x >> 3;
	y = curdg->maxy - (y >> 3);
	oldfont = SelectObject(dc, font);
	SetTextAlign(dc, TA_BOTTOM + TA_LEFT);
	SetBkMode(dc, TRANSPARENT);

//DEBUG for checking font/ string dimeensions are correct
//	int w, h;
//	CalculateClassicDimensions(w, h, font , s);
//	::Rectangle(dc, x, y -h , x + w , y);

	TextOut(dc, x, y, s, _tcslen(s));

	SelectObject(dc, oldfont);
}


/* 
 * setfont
 * set font to one of ddgs preloaded fonts
 */
void
setfont(int fontnum)
{
	// if its not the old 0 -3 font or the new 0x0F - 0xFF font sizes set to 0
//	if (!((fontnum >= 0 && fontnum < 4)  || (fontnum > 0x0F && fontnum < 0xFFFF)))
//		fontnum = 0;

	fontid = (short)fontnum;
}

void setTrueFont(short pointsize)
{
	gFontPoint = pointsize;
}

/*
 * oneline_textsize
 * returns width and height of a string WITH NO LINEFEEDS in current font
 * scaled to DDGS coordinates
 */
void
oneline_textsize(LPCTSTR s, int *width, int *height, int scale)
{
	int w, h;

	if (curdg == NULL)
	{
		*width = *height = 32;
		return;
	}

	// create currently selected font, only used to calculate font size
	HFONT f = _makefont(fontid, scale);

	CalculateClassicDimensions(w, h, f, s);

	DeleteObject(f);

	double ds = getScale();

	*width = w * 8/ ds; 
	*height = h * 8/ ds;
}


/*
 *	T E X T S I Z E		
 *				returns width and height of string in current font
 *				scaled to DDGS coordinates
 *				HANDLES LINEFEEDS (MG)
 */

void textsize(LPCTSTR s, int *width, int *height)
{
	textsizeScale(s, width, height, 8);
}

void textsizeScale(LPCTSTR s, int *width, int *height, int scale)
{
	int l,newlines,maxwidth;
	int lwidth, lheight;
	TCHAR *c, *d;

	if ((s==NULL) || (_tcslen(s)==0))
	{
		*width=32;
		*height=32;
		return;
	}


	c = d = (TCHAR *)s;
	newlines=1;
	maxwidth=1;
	while (*c != _T('\0'))
	{
		if (*c == _T('\n'))
		{
			*c = _T('\0');
			newlines++;
			l = _tcslen(d);

			if (l>0)
			{
				oneline_textsize(d, &lwidth, &lheight, scale); 
				if (lwidth>maxwidth)
					maxwidth=lwidth;
			}

			*c = _T('\n');
			d = c+1;
		}
		c++;
	}

	if (c!=d)
	{
		l = _tcslen(d);

		if (l>0)
		{
			oneline_textsize(d, &lwidth, &lheight, scale); 
			if (lwidth>maxwidth)
				maxwidth=lwidth;
		}
	}

//	oneline_textsize(teststr, &lwidth, &lheight); 
	oneline_textsize(s, &lwidth, &lheight, scale); 
	*height = lheight * newlines/* * to_cohu_scale()*/; // and scale for large format images
	*width = maxwidth/* * to_cohu_scale()*/;			// and scale for large format images
}

