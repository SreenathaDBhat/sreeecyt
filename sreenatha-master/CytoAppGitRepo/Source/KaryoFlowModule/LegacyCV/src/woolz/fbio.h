/*
 *      F B I O . H  --  version written by m.castling 
 *
 *	necessary because original version was created for OS9 operating system
 *
 *	Copyright (c) and intellectual property rights Image Recognition Systems (1988)
 *
 *  Date:    16/6/92
 */

#define mgetc(f) getc(f)
#define mputc(c,f) putc(((unsigned char)(c)),f)

