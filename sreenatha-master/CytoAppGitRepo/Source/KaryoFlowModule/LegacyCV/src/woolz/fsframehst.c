/*
 *	F S F R A M E H I S T O . C
 * 		Create a histogram from byte orientated frame store
 *
 *		Graham John Page Image recognition Systems
 *			Copyright IPR etc
 *									18 Jun 1987
 *
 *	10Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *					Also, made minor mods to stop compiler warnings and removed
 *					standard C parameter lists (were '#ifdef i386' only).
 *	9/19/96		BP:		Move comments outside function declaration.
 *	20/12/93	dcb	Removed unused register variable 'k'
 */

#include "woolz.h"

#include <stdio.h>
//#include <wstruct.h>


/*
 *
 *		F S F R A M E H I S T O
 *
 *	Inputs addres of frame store, lines, cols and max grey
 *

 frame - Pointer to begining of frame store
 ppl   - Number of pixels in a line
 lpf   - Number of lines in a frame
 maxg  - Max grey value required to determin size

 *	Outputs pointer to object of type histogram
 *
 */
 

struct object *
fsframehisto(register unsigned char *frame, int ppl, int lpf, int maxg)
{
	struct object *hist, *makemain();
	struct histogramdomain *hdom, *makehistodmn();
	register int *h , size=ppl*lpf;

	/* make empty histogram */
	hdom = makehistodmn(1,maxg+1);
	hist = makemain(13, (struct intervaldomain *)hdom, NULL, NULL, NULL);
	/* fill histogram */
	h = hdom->hv;
	do {
			h[*frame]++;
			frame++;
	}while (--size);
	return(hist);
}

