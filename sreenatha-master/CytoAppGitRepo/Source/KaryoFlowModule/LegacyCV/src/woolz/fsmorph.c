
/*	FSMORPH.C 	-	Mark Gregson	5/4/94
 *
 *	Grey level morphology for 8 bit frame stores
 *
 * Modifications:
 *
 *	08Nov2001	MG		Added fsMin84, fsMax84, fsOpen84,...etc.c 
 *						These are variants of fsMin etc. tha use alternating square and cross
 *						structuring elemnets to give more orgainc shapes that match the original
 *						object more closely. Also added fsMedian() for performing gfast NxN median filter
 *	10Mar2000	JMB		Changed headers included, as part of conversion to DLL.
 *						Also, made minor mods to stop compiler warnings and removed
 *						standard C parameter lists (were '#ifdef i386' only).
 *	5Oct99		dcb		In all functions: 2 * halfwidth must be < cols or lines
 *	9/20/96		BP:		Use FSCHAR instead of Uchar, remove local
 *						definition of Uchar.
 *						Move comments from function headers.
 *
 */

#include "woolz.h"

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h> // For expf()
//#include <wstruct.h>


//--------------------------------------------------------------------------------------

/*
 *
 *		F S M I N	( erode grey image )
 *
 *	Inputs address of frame store, lines, cols , filter half width
 *		frame - Pointer to begining of frame store
 *		cols  - Number of pixels in a line
 *		lines - Number of lines in a frame
 *		halfwidth - Filter halfwidth i.e. 1 => 3x3, 2 => 5x5 etc
 *
 *	Outputs : new frame store containing result of NxN minimum filter
 *		  where N = 2*halfwidth + 1
 *
 *	Uses local histogram ( held in rack[] ) to perform a very efficient
 *	running calculation of the local minimum without sorting values.
 */
FSCHAR *
fsMin(FSCHAR *frame, int cols, int lines, int halfwidth)
{
	register FSCHAR *fptr, *oldfptr, *hfptr, *vfptr;
	register int col,line;
	FSCHAR min;
	FSCHAR *hframe, *vframe;
	int size, fw;
	short rack[256];
	
	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	size=lines*cols;

	/* allocate temporary frame */
	hframe=(FSCHAR *)Malloc(size);

	if (hframe==NULL) {
		fprintf(stderr,"\nfsmin: could not allocate new frame\n");
		return(NULL);
	}

	/* allocate result frame */
	vframe=(FSCHAR *)Malloc(size);

	if (vframe==NULL) {
		Free(hframe);
		fprintf(stderr,"\nfsmin: could not allocate new frame\n");
		return(NULL);
	}

/* PASS1 */

	/* perform horizontal min on frame and save in hframe */

	for (line=0; line<lines; line++) {	/* for each image line */

		/* clear rack */
		memset(rack,0,sizeof(rack[0])*256);

		/* initialise min */
		min=255;

		/* initialise frame pointers */
		oldfptr = fptr = frame + cols*line;
		hfptr = hframe + cols*line;

		/* put first halfwidth+1 points into rack and find min */
		for (col=0; col<halfwidth+1; col++) {
			if (*fptr<min)
				min=*fptr;

			rack[*fptr]++;
	
			fptr++;
		}


		/* deal with pixels within half filter 
		   width of left edge of image */

		for (col=0;col<halfwidth;col++) {
			/* store min in hframe */
			*hfptr++=min;

			/* update rack counts */
			rack[*fptr]++;

			/* check if newval is the min */
			if (*fptr<min)
				min=*fptr;

			fptr++;
		}
			
		/* deal with pixels in main body of image */

		for (col=halfwidth; col<cols-halfwidth-1; col++) {

			/* store min in hframe */
			*hfptr++=min;
		
			/* remove oldval from rack, insert newval
		   	   and determine new min value */

			if (*oldfptr!=*fptr) {

				/* update rack counts */
				rack[*fptr]++;
				rack[*oldfptr]--;

				/* check if newval is the min */
				if (*fptr<min)
					min=*fptr;

				/* check if oldval was the min
				   and if so find new min */
				while (rack[min] == 0)
					min++;
			}

			fptr++;
			oldfptr++;
		}

		/* deal with pixels within half filter 
		   width of right edge of image */

		for (col=cols-halfwidth-1; col<cols; col++) {
			*hfptr++=min;
		
			/* update rack counts */
			rack[*oldfptr]--;

			/* check if oldval was the min
		   	   and if so find new min */
			while (rack[min] == 0)
				min++;

			oldfptr++;
		}
	}

/* PASS2 */

	/* perform vertical min on hframe and save in vframe */

	for (col=0; col<cols; col++) {	/* for each image column */

		/* clear rack */
		memset(rack,0,sizeof(rack[0])*256);

		min=255;
		oldfptr = fptr = hframe+col;
		vfptr = vframe+col;


		/* put first halfwidth+1 points into rack and find min */
		for (line=0; line<halfwidth+1; line++) {
			if (*fptr<min)
				min=*fptr;

			rack[*fptr]++;
	
			fptr+=cols;
		}

		/* deal with pixels within half filter 
		   width of bottom edge of image */

		for (line=0; line<halfwidth; line++) {
			/* store min in vframe */
			*vfptr=min;
			vfptr+=cols;

			/* update rack counts */
			rack[*fptr]++;

			/* check if newval is the min */
			if (*fptr<min)
				min=*fptr;

			fptr+=cols;
		}

		/* deal with pixels in main body of image */

		for (line=halfwidth; line<lines-halfwidth-1; line++) {

			/* store min in hframe */
			*vfptr=min;
			vfptr+=cols;
		
			/* remove oldval from rack, insert newval
		   	   and determine new min value */

			if (*oldfptr!=*fptr) {

				/* update rack counts */
				rack[*fptr]++;
				rack[*oldfptr]--;

				/* check if newval is the min */
				if (*fptr<min)
					min=*fptr;

				/* check if oldval was the min
				   and if so find new min */
				while (rack[min] == 0)
					min++;
			}

			fptr+=cols;
			oldfptr+=cols;
		}

		/* deal with pixels within half filter 
		   width of top edge of image */

		for (line=lines-halfwidth-1; line<lines; line++) {
			*vfptr=min;
			vfptr+=cols;
		
			/* update rack counts */
			rack[*oldfptr]--;

			/* check if oldval was the min
		   	   and if so find new min */
			while (rack[min] == 0)
				min++;

			oldfptr+=cols;
		}
	}

	/* free off unwanted hframe */
	Free(hframe);
	
	/* vframe now contains 2 dimensional min */
	return(vframe);
}

//--------------------------------------------------------------------------------------
//
// Same as fsMin but uses alternating 8,4 connectivity (cross, square) 
// This gives more organic operation - which matches original object shapes a little closer
// but takes much longer than fsMin - which is highly optimised
//

FSCHAR *fsMin84(FSCHAR *frame, int cols, int lines, int halfwidth)
{
	int passes = halfwidth;
	int binwidth = cols+2;
	int binheight = lines+2;
	int binwidthplus1 = binwidth+1;
	int binwidthx2 = binwidth *2;
	int row, col;
	int i, p, val;
	FSCHAR *binim1, *binim2, *imptr1, *binptr1, *binptr2;
	FSCHAR *temp, *image1;

	// 2 * halfwidth must be < cols or lines
	int fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	binim1 = (FSCHAR *)Calloc(binwidth * binheight, 1);
	binim2 = (FSCHAR *)Calloc(binwidth * binheight, 1);

	imptr1= frame;
	binptr1= binim1;
	binptr2= binim2;

	// drop image into clear larger image binim1 to allow fow 3x3 nhood operators
	binptr1 += binwidth;
	for (row=0; row<lines; row++)
	{
		binptr1++;
		for (col=0;col<cols;col++)
		{
			*binptr1++ = *imptr1++;
		}
		binptr1++;
	}

	// duplicate top and bottom rows and left and right sides so min and max not affected at edges
	binptr1 = binim1;
	binptr2 = binim1 + binwidth;
	memcpy(binptr1, binptr2, binwidth);

	binptr1 = binim1 + lines*binwidth;
	binptr2 = binim1 + (lines+1)*binwidth;
	memcpy(binptr2, binptr1, binwidth);

	binptr1=binim1; binptr2=binim1+binwidth-1;
	for (i=0; i<binheight; i++)
	{
		*binptr1 = *(binptr1+1); binptr1+=binwidth;
		*binptr2 = *(binptr2-1); binptr2+=binwidth;
	}

	// now process binary image, alternating between cross and square structures
	for (p=0; p<passes; p++)
	{
		binptr1 = binim1 + binwidth;	// in one line
		binptr2 = binim2 + binwidth;

		if (p & 1) //cross - use slow version
		{
			for (row=0; row<lines; row++)
			{
				binptr1++;				// in one col
				binptr2++;
				for (col=0;col<cols;col++)
				{
					val = *binptr1--;							// centre
					val = (*binptr1 < val) ? *binptr1 : val;	// left
					binptr1++;binptr1++;
					val = (*binptr1 < val) ? *binptr1 : val;	// right
					binptr1-=binwidthplus1;
					val = (*binptr1 < val) ? *binptr1 : val;	// top
					binptr1+=binwidthx2;
					val = (*binptr1 < val) ? *binptr1 : val;	// bottom
					binptr1-=binwidth;	// back to centre

					binptr1++;			// next point
					*binptr2++ = val;
				}
				binptr1++;
				binptr2++;
			}
		}
		else // square - so use fast fsMin() routine
		{
			free(binim2);
			binim2 = fsMin(binim1, binwidth, binheight, 1);
		}
		
		//prepare for next pass - so the result is always in in binim1
		temp = binim1;
		binim1 = binim2;
		binim2 = temp;
	}

	// place result back into an image the size of the original
	image1 = (FSCHAR *)Calloc(cols*lines, 1);

	imptr1= image1;
	binptr1= binim1;

	binptr1 += binwidth;
	for (row=0; row<lines; row++)
	{
		binptr1++;
		for (col=0;col<cols;col++)
		{
			*imptr1++ = *binptr1++;
		}
		binptr1++;
	}

	// destroy temp binims
	Free(binim1);
	Free(binim2);

	return image1;
}
							
//--------------------------------------------------------------------------------------

/*
 *
 *		F S M A X	( dilate grey image )
 *
 *	Inputs address of frame store, lines, cols , filter half width
 *			frame - Pointer to begining of frame store
 *			cols  - Number of pixels in a line
 *			lines - Number of lines in a frame
 *			halfwidth - Filter halfwidth i.e. 1 => 3x3, 2 => 5x5 etc
 *
 *	Outputs : new frame store containing result of NxN maximum filter
 *		  where N = 2*halfwidth + 1
 *
 *	Uses local histogram ( held in rack[] ) to perform a very efficient
 *	running calculation of the local maximum without sorting values.
 */
 
FSCHAR *
fsMax(FSCHAR *frame, int cols, int lines, int halfwidth)
{
	register FSCHAR *fptr, *oldfptr, *hfptr, *vfptr;
	register int col,line;
	FSCHAR max;
	FSCHAR *hframe, *vframe;
	int size, fw;
	short rack[256];
	
	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	size=lines*cols;

	/* allocate temporary frame */
	hframe=(FSCHAR *)Malloc(size);

	if (hframe==NULL) {
		fprintf(stderr,"\nfsmax: could not allocate new frame\n");
		return(NULL);
	}

	/* allocate result frame */
	vframe=(FSCHAR *)Malloc(size);

	if (vframe==NULL) {
		Free(hframe);
		fprintf(stderr,"\nfsmax: could not allocate new frame\n");
		return(NULL);
	}

/* PASS1 */

	/* perform horizontal max on frame and save in hframe */

	for (line=0; line<lines; line++) {	/* for each image line */

		/* clear rack */
		memset(rack,0,sizeof(rack[0])*256);

		/* initialise max */
		max=0;

		/* initialise frame pointers */
		oldfptr = fptr = frame + cols*line;
		hfptr = hframe + cols*line;

		/* put first halfwidth+1 points into rack and find max */
		for (col=0; col<halfwidth+1; col++) {
			if (*fptr>max)
				max=*fptr;

			rack[*fptr]++;
	
			fptr++;
		}


		/* deal with pixels within half filter 
		   width of left edge of image */

		for (col=0;col<halfwidth;col++) {
			/* store max in hframe */
			*hfptr++=max;

			/* update rack counts */
			rack[*fptr]++;

			/* check if newval is the max */
			if (*fptr>max)
				max=*fptr;

			fptr++;
		}
			
		/* deal with pixels in main body of image */

		for (col=halfwidth; col<cols-halfwidth-1; col++) {

			/* store max in hframe */
			*hfptr++=max;
		
			/* remove oldval from rack, insert newval
		   	   and determine new max value */

			if (*oldfptr!=*fptr) {

				/* update rack counts */
				rack[*fptr]++;
				rack[*oldfptr]--;

				/* check if newval is the max */
				if (*fptr>max)
					max=*fptr;

				/* check if oldval was the max
		   		   and if so find new max */
				while (rack[max] == 0)
					max--;
			}

			fptr++;
			oldfptr++;
		}

		/* deal with pixels within half filter 
		   width of right edge of image */

		for (col=cols-halfwidth-1; col<cols; col++) {
			*hfptr++=max;
		
			/* update rack counts */
			rack[*oldfptr]--;

			/* check if oldval was the max
		   	   and if so find new max */
			while (rack[max] == 0)
				max--;

			oldfptr++;
		}
	}

/* PASS2 */

	/* perform vertical max on hframe and save in vframe */

	for (col=0; col<cols; col++) {	/* for each image column */

		/* clear rack */
		memset(rack,0,sizeof(rack[0])*256);

		max=0;
		oldfptr = fptr = hframe+col;
		vfptr = vframe+col;


		/* put first halfwidth+1 points into rack and find max */
		for (line=0; line<halfwidth+1; line++) {
			if (*fptr>max)
				max=*fptr;

			rack[*fptr]++;
	
			fptr+=cols;
		}

		/* deal with pixels within half filter 
		   width of bottom edge of image */

		for (line=0; line<halfwidth; line++) {
			/* store max in vframe */
			*vfptr=max;
			vfptr+=cols;

			/* update rack counts */
			rack[*fptr]++;

			/* check if newval is the max */
			if (*fptr>max)
				max=*fptr;

			fptr+=cols;
		}

		/* deal with pixels in main body of image */

		for (line=halfwidth; line<lines-halfwidth-1; line++) {

			/* store max in hframe */
			*vfptr=max;
			vfptr+=cols;
		
			/* remove oldval from rack, insert newval
		   	   and determine new max value */

			if (*oldfptr!=*fptr) {

				/* update rack counts */
				rack[*fptr]++;
				rack[*oldfptr]--;

				/* check if newval is the max */
				if (*fptr>max)
					max=*fptr;

				/* check if oldval was the max
		   		   and if so find new max */
				while (rack[max] == 0)
					max--;
			}

			fptr+=cols;
			oldfptr+=cols;
		}

		/* deal with pixels within half filter 
		   width of top edge of image */

		for (line=lines-halfwidth-1; line<lines; line++) {
			*vfptr=max;
			vfptr+=cols;
		
			/* update rack counts */
			rack[*oldfptr]--;

			/* check if oldval was the max
		   	   and if so find new max */
			while (rack[max] == 0)
				max--;

			oldfptr+=cols;
		}
	}

	/* free off unwanted hframe */
	Free(hframe);
	
	/* vframe now contains 2 dimensional max */
	return(vframe);
}

//--------------------------------------------------------------------------------------
//
// Same as fsMax but uses alternating 8,4 connectivity (cross, square) 
// This gives more organic operation - which matches original object shapes a little closer
// but takes much longer than fsMax - which is highly optimised
//


FSCHAR *fsMax84(FSCHAR *frame, int cols, int lines, int halfwidth)
{
	int passes = halfwidth;
	int binwidth = cols+2;
	int binheight = lines+2;
	int binwidthplus1 = binwidth+1;
	int binwidthx2 = binwidth *2;
	int row, col;
	int i, p, val;
	FSCHAR *binim1, *binim2, *imptr1, *binptr1, *binptr2;
	FSCHAR *temp, *image1;

	// 2 * halfwidth must be < cols or lines
	int fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	binim1 = (FSCHAR *)Calloc(binwidth * binheight, 1);
	binim2 = (FSCHAR *)Calloc(binwidth * binheight, 1);

	imptr1= frame;
	binptr1= binim1;
	binptr2= binim2;

	// drop image into clear larger image binim1 to allow fow 3x3 nhood operators
	binptr1 += binwidth;
	for (row=0; row<lines; row++)
	{
		binptr1++;
		for (col=0;col<cols;col++)
		{
			*binptr1++ = *imptr1++;
		}
		binptr1++;
	}

	// duplicate top and bottom rows and left and right sides so min and max not affected at edges
	binptr1 = binim1;
	binptr2 = binim1 + binwidth;
	memcpy(binptr1, binptr2, binwidth);

	binptr1 = binim1 + lines*binwidth;
	binptr2 = binim1 + (lines+1)*binwidth;
	memcpy(binptr2, binptr1, binwidth);

	binptr1=binim1; binptr2=binim1+binwidth-1;
	for (i=0; i<binheight; i++)
	{
		*binptr1 = *(binptr1+1); binptr1+=binwidth;
		*binptr2 = *(binptr2-1); binptr2+=binwidth;
	}

	// now process binary image, alternating between cross and square structures
	for (p=0; p<passes; p++)
	{
		binptr1 = binim1 + binwidth;
		binptr2 = binim2 + binwidth;

		if (p & 1) //cross - use slow version
		{
			for (row=0; row<lines; row++)
			{
				binptr1++;
				binptr2++;
				for (col=0;col<cols;col++)
				{
					val = *binptr1--;							// centre
					val = (*binptr1 > val) ? *binptr1 : val;	// left
					binptr1++;binptr1++;
					val = (*binptr1 > val) ? *binptr1 : val;	// right
					binptr1-=binwidthplus1;
					val = (*binptr1 > val) ? *binptr1 : val;	// top
					binptr1+=binwidthx2;
					val = (*binptr1 > val) ? *binptr1 : val;	// bottom
					binptr1-=binwidth;	// back to centre

					binptr1++;			// next point
					*binptr2++ = val;
				}
				binptr1++;
				binptr2++;
			}
		}
		else // square - so use fast fsMax() routine
		{
			free(binim2);
			binim2 = fsMax(binim1, binwidth, binheight, 1);
		}
		
		//prepare for next pass - so the result is always in in binim1
		temp = binim1;
		binim1 = binim2;
		binim2 = temp;
	}

	// place result back into an image the size of the original
	image1 = (FSCHAR *)Calloc(cols*lines, 1);

	imptr1= image1;
	binptr1= binim1;

	binptr1 += binwidth;
	for (row=0; row<lines; row++)
	{
		binptr1++;
		for (col=0;col<cols;col++)
		{
			*imptr1++ = *binptr1++;
		}
		binptr1++;
	}

	// destroy temp binims
	Free(binim1);
	Free(binim2);

	return image1;
}
							

//--------------------------------------------------------------------------------------

/* 
 *	F S O P E N
 *
 *	Perform morphological opening by NxN square structuring element on frame store image
 */
FSCHAR *
fsOpen(FSCHAR *frame, int cols, int lines, int halfwidth)
{
	int fw;
	FSCHAR *f1=NULL;
	FSCHAR *f2=NULL;

	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	/* Erode image */
	f1=fsMin(frame,cols,lines,halfwidth);
	
	if (f1!=NULL) {

		/* Dilate image */
		f2=fsMax(f1,cols,lines,halfwidth);
		Free(f1);
	}

	return(f2);
}

//--------------------------------------------------------------------------------------

FSCHAR *fsOpen84(FSCHAR *frame, int cols, int lines, int halfwidth)
{
	int fw;
	FSCHAR *f1=NULL;
	FSCHAR *f2=NULL;

	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	/* Erode image */
	f1=fsMin84(frame,cols,lines,halfwidth);
	
	if (f1!=NULL) {

		/* Dilate image */
		f2=fsMax84(f1,cols,lines,halfwidth);
		Free(f1);
	}

	return(f2);
}


//--------------------------------------------------------------------------------------

/* 
 *	F S C L O S E
 *
 *	Perform morphological closing by NxN square structuring element on frame store image
 */
FSCHAR *
fsClose(FSCHAR *frame, int cols, int lines, int halfwidth)
{
	int fw;
	FSCHAR *f1=NULL;
	FSCHAR *f2=NULL;

	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	/* Dilate image */
	f1=fsMax(frame,cols,lines,halfwidth);
	
	if (f1!=NULL) {

		/* Erode image */
		f2=fsMin(f1,cols,lines,halfwidth);
		Free(f1);
	}

	return(f2);
}

//--------------------------------------------------------------------------------------

FSCHAR *fsClose84(FSCHAR *frame, int cols, int lines, int halfwidth)
{
	int fw;
	FSCHAR *f1=NULL;
	FSCHAR *f2=NULL;

	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	/* Dilate image */
	f1=fsMax84(frame,cols,lines,halfwidth);
	
	if (f1!=NULL) {

		/* Erode image */
		f2=fsMin84(f1,cols,lines,halfwidth);
		Free(f1);
	}

	return(f2);
}

//--------------------------------------------------------------------------------------

/* 
 *	F S T O P H A T
 *
 *	Perform morphological opening by NxN square structuring element on frame store image
 *	Then subtract the result from the original image to remove background variation
 */
FSCHAR *
fsTophat(FSCHAR *frame, int cols, int lines, int halfwidth)
{
	register FSCHAR *imptr, *im2ptr;
	int register size;
	int fw;
	FSCHAR *f1=NULL;

	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	size=lines*cols;

	/* Open image */
	f1=fsOpen(frame,cols,lines,halfwidth);
	
	if (f1!=NULL) {

		/* subtract opened image from original */
		imptr=frame;
		im2ptr=f1;
		do {
			*im2ptr=*imptr++ - *im2ptr;
			im2ptr++;
		} while (--size);
	}

	return(f1);
}

//--------------------------------------------------------------------------------------

/* 
 *	F S S E P
 *
 *	Perform morphological closing by NxN square structuring element on frame store image
 *	Then subtract the result from the original image to remove background variation
 */
FSCHAR *
fsSep(FSCHAR *frame, int cols, int lines, int halfwidth)
{
	register FSCHAR *imptr, *im2ptr;
	int register size;
	int fw;
	FSCHAR *f1=NULL;

	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	size=lines*cols;

	/* Open image */
	f1=fsClose(frame,cols,lines,halfwidth);
	
	if (f1!=NULL) {

		/* subtract opened image from original */
		imptr=frame;
		im2ptr=f1;
		do {
			*im2ptr=*im2ptr - *imptr++;
			im2ptr++;
		} while (--size);
	}

	return(f1);
}

//--------------------------------------------------------------------------------------

/* 
 *	F S E X T R A C T
 *
 *	Perform morphological closing by NxN square structuring element on frame store image
 *	Perform morphological opening by NxN square structuring element on result
 *	Get Min of result and original image
 *	Then subtract the result from the original image to extract image without background
 *	variation or noise
 */
FSCHAR *
fsExtract(FSCHAR *frame, int cols, int lines, int halfwidth, int noise_halfwidth)
{
	register int size;
	FSCHAR register *imptr, *im2ptr, min/*, max*/;
	FSCHAR *f1=NULL;
	FSCHAR *f2=NULL;
	int fw;

	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	size=lines*cols;

	/* Close image */
	f1=fsClose(frame,cols,lines,noise_halfwidth);

	if (f1!=NULL) {

		/* Open resultant image */
		f2=fsOpen(f1,cols,lines,halfwidth);

		/* free intermediate image */
		Free(f1);

		if (f2!=NULL) {

			imptr=frame;
			im2ptr=f2;
			do {
				/* find min of original and processed images */
				min=*imptr < *im2ptr ? *imptr : *im2ptr;

				/* subtract min from original image */
				*im2ptr++=*imptr++ - min;

			} while (--size);
		}
	}

	return(f2);
}

////////////////////////////////////////////////////////////////////////////////
//
//	This function is provided to free memory allocated by fsExtract() etc
//	since freeing it in an external DLL seems to cause problems - not sure why - dcb
//
void
fsFreeWoolz(FSCHAR *im)
{
	if (im)
		Free(im);
}


//--------------------------------------------------------------------------------------

FSCHAR *fsExtract84(FSCHAR *frame, int cols, int lines, int halfwidth, int noise_halfwidth)
{
	register int size;
	FSCHAR register *imptr, *im2ptr, min/*, max*/;
	FSCHAR *f1=NULL;
	FSCHAR *f2=NULL;
	int fw;

	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	size=lines*cols;

	/* Close image */
	f1=fsClose84(frame,cols,lines,noise_halfwidth);

	if (f1!=NULL) {

		/* Open resultant image */
		f2=fsOpen84(f1,cols,lines,halfwidth);

		/* free intermediate image */
		Free(f1);

		if (f2!=NULL) {

			imptr=frame;
			im2ptr=f2;
			do {
				/* find min of original and processed images */
				min=*imptr < *im2ptr ? *imptr : *im2ptr;

				/* subtract min from original image */
				*im2ptr++=*imptr++ - min;

			} while (--size);
		}
	}

	return(f2);
}

//--------------------------------------------------------------------------------------


void
fsReconstruct(FSCHAR *input, FSCHAR *reference, int cols, int lines, int count)
{

	register FSCHAR *f2ptr, *refptr, *inptr;
	register int size;
	int activity;
	FSCHAR *f2=NULL;

	do {
		activity=0;

		f2=fsMax(input,cols,lines,3);
		
		if (f2==NULL)
			return;

		size=cols*lines;

		refptr=reference;
		f2ptr=f2;
		inptr=input;

		do {
			if (*refptr < *f2ptr)
				*f2ptr=*refptr;

			if (*inptr != *f2ptr)
				activity++;

			*inptr++=*f2ptr++;

			refptr++;
			
		} while (--size);

		Free(f2);

		count-=3;

	} while ((activity >500) && (count>0));

}

	
//--------------------------------------------------------------------------------------

FSCHAR *
fsMultires(FSCHAR *frame, int cols, int lines, int size_max, int size_min)
{
	register FSCHAR *f2ptr, *f1ptr;
	register int size;
	FSCHAR *f1, *f2/*, *open1, *open2*/;

	f1=fsOpen(frame,cols,lines,size_min);
	if (f1==NULL)
		return(NULL);

	fsReconstruct(f1,frame,cols,lines,size_max/2);

	f2=fsOpen(f1,cols,lines,size_max);
	if (f2==NULL)
		return(NULL);

	fsReconstruct(f2,f1,cols,lines,size_max/2);

	size = cols*lines;
	f2ptr=f2;
	f1ptr=f1;
	do {
		if (*f2ptr < *f1ptr)
			*f2ptr=*f1ptr-*f2ptr;
		else
			*f2ptr=0;
		f1ptr++;
		f2ptr++;
	} while (--size);

	Free(f1);

	return(f2);
}


												
//--------------------------------------------------------------------------------------
/*
 *
 *		F S M E D I A N	
 *
 *	Inputs address of frame store, lines, cols , filter half width
 *		frame - Pointer to begining of frame store
 *		cols  - Number of pixels in a line
 *		lines - Number of lines in a frame
 *		halfwidth - Filter halfwidth i.e. 1 => 3x3, 2 => 5x5 etc
 *
 *	Outputs : new frame store containing result of NxN median filter
 *		  where N = 2*halfwidth + 1
 *
 *	Uses local histogram ( held in rack[] ) to perform a very efficient
 *	running calculation of the local median without sorting values.
 */
FSCHAR *fsMedian(FSCHAR *frame, int cols, int lines, int halfwidth)
{
	register FSCHAR *fptr, *oldfptr, *hfptr, *vfptr;
	register int col,line;
	FSCHAR min, median;
	FSCHAR *hframe, *vframe;
	int size, fw, count;
	short rack[256];
	
	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	size=lines*cols;

	/* allocate temporary frame */
	hframe=(FSCHAR *)Malloc(size);

	if (hframe==NULL) {
		fprintf(stderr,"\nfsMedian: could not allocate new frame\n");
		return(NULL);
	}

	/* allocate result frame */
	vframe=(FSCHAR *)Malloc(size);

	if (vframe==NULL) {
		Free(hframe);
		fprintf(stderr,"\nfsMedian: could not allocate new frame\n");
		return(NULL);
	}

/* PASS1 */

	/* perform horizontal min on frame and save in hframe */

	for (line=0; line<lines; line++) {	/* for each image line */

		/* clear rack */
		memset(rack,0,sizeof(rack[0])*256);

		/* initialise min*/
		min=255;

		/* initialise frame pointers */
		oldfptr = fptr = frame + cols*line;
		hfptr = hframe + cols*line;

		/* put first halfwidth+1 points into rack and find min */
		for (col=0; col<halfwidth+1; col++) {
			if (*fptr<min)
				min=*fptr;

			rack[*fptr]++;
	
			fptr++;
		}

		// calc median
		count = halfwidth;
		for (median=min; median<255; median++)
		{
			count -= rack[median];
			if (count<0)
				break;
		}

		/* deal with pixels within half filter 
		   width of left edge of image */

		for (col=0;col<halfwidth;col++) {
			/* store median in hframe */
			*hfptr++=median;

			/* update rack counts */
			rack[*fptr]++;

			/* check if newval is the min */
			if (*fptr<min)
				min=*fptr;
			
			// recalc median
			if (*fptr<=median)
			{
				count = halfwidth;
				for (median=min; median<255; median++)
				{
					count -= rack[median];
					if (count<0)
						break;
				}
			}

			fptr++;
		}
			
		/* deal with pixels in main body of image */

		for (col=halfwidth; col<cols-halfwidth-1; col++) {

			/* store median in hframe */
			*hfptr++=median;
		
			/* remove oldval from rack, insert newval
		   	   and determine new min value */

			if (*oldfptr!=*fptr) {

				/* update rack counts */
				rack[*fptr]++;
				rack[*oldfptr]--;

				/* check if newval is the min */
				if (*fptr<min)
					min=*fptr;

				/* check if oldval was the min
				   and if so find new min */
				while (rack[min] == 0)
					min++;

				// recalc median
				if ((*fptr<=median) || (*oldfptr<=median))
				{
					count = halfwidth;
					for (median=min; median<255; median++)
					{
						count -= rack[median];
						if (count<0)
							break;
					}
				}

			}

			fptr++;
			oldfptr++;
		}

		/* deal with pixels within half filter 
		   width of right edge of image */

		for (col=cols-halfwidth-1; col<cols; col++) {
			*hfptr++=median;
		
			/* update rack counts */
			rack[*oldfptr]--;

			/* check if oldval was the min
		   	   and if so find new min */
			while (rack[min] == 0)
				min++;

			// recalc median
			if (*oldfptr<=median)
			{
				count = halfwidth;
				for (median=min; median<255; median++)
				{
					count -= rack[median];
					if (count<0)
						break;
				}
			}


			oldfptr++;
		}
	}

/* PASS2 */

	/* perform vertical median on hframe and save in vframe */

	for (col=0; col<cols; col++) {	/* for each image column */

		/* clear rack */
		memset(rack,0,sizeof(rack[0])*256);

		min=255;
		oldfptr = fptr = hframe+col;
		vfptr = vframe+col;


		/* put first halfwidth+1 points into rack and find min */
		for (line=0; line<halfwidth+1; line++) {
			if (*fptr<min)
				min=*fptr;

			rack[*fptr]++;
	
			fptr+=cols;
		}


		// calc median
		count = halfwidth;
		for (median=min; median<255; median++)
		{
			count -= rack[median];
			if (count<0)
				break;
		}

		/* deal with pixels within half filter 
		   width of bottom edge of image */

		for (line=0; line<halfwidth; line++) {
			/* store median in vframe */
			*vfptr=median;
			vfptr+=cols;

			/* update rack counts */
			rack[*fptr]++;

			/* check if newval is the min */
			if (*fptr<min)
				min=*fptr;

			// recalc median
			if (*fptr<=median)
			{
				count = halfwidth;
				for (median=min; median<255; median++)
				{
					count -= rack[median];
					if (count<0)
						break;
				}
			}


			fptr+=cols;
		}

		/* deal with pixels in main body of image */

		for (line=halfwidth; line<lines-halfwidth-1; line++) {

			/* store median in hframe */
			*vfptr=median;
			vfptr+=cols;
		
			/* remove oldval from rack, insert newval
		   	   and determine new min value */

			if (*oldfptr!=*fptr) {

				/* update rack counts */
				rack[*fptr]++;
				rack[*oldfptr]--;

				/* check if newval is the min */
				if (*fptr<min)
					min=*fptr;

				/* check if oldval was the min
				   and if so find new min */
				while (rack[min] == 0)
					min++;

				// recalc median
				if ((*fptr<=median) || (*oldfptr<=median))
				{
					count = halfwidth;
					for (median=min; median<255; median++)
					{
						count -= rack[median];
						if (count<0)
							break;
					}
				}

			}

			fptr+=cols;
			oldfptr+=cols;
		}

		/* deal with pixels within half filter 
		   width of top edge of image */

		for (line=lines-halfwidth-1; line<lines; line++) {
			*vfptr=median;
			vfptr+=cols;
		
			/* update rack counts */
			rack[*oldfptr]--;

			/* check if oldval was the min
		   	   and if so find new min */
			while (rack[min] == 0)
				min++;

			// recalc median
			if (*oldfptr<=median)
			{
				count = halfwidth;
				for (median=min; median<255; median++)
				{
					count -= rack[median];
					if (count<0)
						break;
				}
			}

			oldfptr+=cols;
		}
	}

	/* free off unwanted hframe */
	Free(hframe);
	
	/* vframe now contains 2 dimensional median */
	return(vframe);
}


/*
 * This is used by both fsGaussian and fsUnsharpMask.
 */
#define ROUND(r) ((int)floor((r) + 0.5f))

//--------------------------------------------------------------------------------------
/*
 *		F S G A U S S I A N
 *
 *	Inputs address of frame store, lines, cols, filter half width
 *		frame - Pointer to begining of frame store
 *		cols  - Number of pixels in a line
 *		lines - Number of lines in a frame
 *		halfwidth - Filter halfwidth i.e. 1 => 3x3, 2 => 5x5 etc
 *
 *	Outputs : new frame store containing result of NxN gaussian filter
 *		  where N = 2*halfwidth + 1
 *
 * Based on 'QGaussFilter', (c) 2003, Sjaak Priester, Amsterdam (sjaak@sjaakpriester.nl)
 */
FSCHAR *fsGaussian(FSCHAR *frame, int cols, int lines, float radius)
{
	int *pFilterVector = NULL;
	unsigned int filterVectorLength = 0;
	int filterVectorDenominator = 0;

	FSCHAR *hframe, *vframe;
	int passNum;


	/* allocate temporary frame */
	hframe = (FSCHAR *)Malloc(lines * cols);
	if (hframe == NULL) {
		fprintf(stderr, "\nfsGaussian: could not allocate new frame\n");
		return(NULL);
	}

	/* allocate result frame */
	vframe = (FSCHAR *)Malloc(lines * cols);
	if (vframe == NULL) {
		Free(hframe);
		fprintf(stderr,"\nfsGaussian: could not allocate new frame\n");
		return(NULL);
	}


/*
 * Calculate one-dimensional convolution matrix for use in each pass of
 * two-pass gaussian blur.
 */
	// filterVectorLength is the effective diameter in pixels; all weight factors outside d are assumed
	// to be zero. The factor 5.0 is somewhat arbitrary; a value between 4.0 and 6.0
	// is generally recommended.
	filterVectorLength = ROUND(5.0f * radius + 1.0f) | 1; // must be odd
	if (filterVectorLength > 0) // radius 0 is acceptable; effectively no convolution
	{
		unsigned int i;
		unsigned int d = filterVectorLength / 2;
		float num = 2 * radius * radius;
		float f = exp(d * d / num); // Should be expf(), but that wouldn't build!

		pFilterVector = Malloc(sizeof(int) * filterVectorLength);
		if (!pFilterVector)
		{
			Free(hframe);
			Free(vframe);
			fprintf(stderr,"\nfsGaussian: could not allocate vector\n");
			return(NULL);
		}

		filterVectorDenominator = ROUND(f);

		pFilterVector[d] = filterVectorDenominator;

		for (i = 1; i <= d; i++)
		{
			int i2 = - (int)(i * i);
			int v = ROUND(f * exp(i2 / num)); // Should be expf(), but that wouldn't build!
			pFilterVector[d - i] = v;
			pFilterVector[d + i] = v;
			filterVectorDenominator += 2 * v;
		}
	}


/*
 * Perform filtering operation with two passes
 */
	for (passNum = 1; passNum <= 2; passNum++)
	{
		unsigned int d = filterVectorLength / 2;
		FSCHAR *pStartSrc;
		FSCHAR *pStartDest;
		unsigned int nLines;		// number of lines (horizontal or vertical)
		unsigned int nPixels;		// number of pixels per line
		unsigned int dPixelSrc;		// pixel step in source
		unsigned int dPixelDest;	// pixel step in destination
		unsigned int dLineSrc;		// line step in source
		unsigned int dLineDest;		// line step in destination

		if (passNum == 1) // Horizontal pass - filter frame and save in hframe
		{
			pStartSrc  = frame;
			pStartDest = hframe;
			nLines  = lines;
			nPixels = cols;
			dPixelSrc  = 1;
			dPixelDest = 1;
			dLineSrc  = cols;
			dLineDest = cols;
		}
		else if (passNum == 2) // Vertical pass - filter hframe and save in vframe
		{
			pStartSrc  = hframe;
			pStartDest = vframe;
			nLines  = cols;
			nPixels = lines;
			dPixelSrc  = cols;
			dPixelDest = cols;
			dLineSrc  = 1;
			dLineDest = 1;
		}
		else
			continue; // Unknown pass


		// Avoid overrun in small bitmaps.
		if (d > nPixels / 2) d = nPixels / 2;

		if (pStartSrc && pStartDest)
		{
			FSCHAR *pLineSrc  = pStartSrc;
			FSCHAR *pLineDest = pStartDest;
			unsigned int line;
			for (line = 0; line < nLines; line++) // loop through lines
			{
				FSCHAR *pPixelDest = pLineDest;
				unsigned int pxl;
				for (pxl = 0; pxl < d; pxl++) // loop through pixels in left/top margin
				{
					int *pFactors = pFilterVector + d - pxl;
					int denom = 0;
					int sum = 0;
					FSCHAR *pPixelSrc = pLineSrc;
					unsigned int x;
					unsigned int xEnd = pxl + d;
					if (xEnd > nPixels) xEnd = nPixels;

					for (x = 0; x < xEnd; x++)
					{
						denom += *pFactors;
						sum += *pFactors++ * *pPixelSrc;
						pPixelSrc += dPixelSrc;
					}

					if (denom) sum /= denom;
					*pPixelDest = (FSCHAR)sum;

					pPixelDest += dPixelDest;
				}

				for (pxl = d; pxl < nPixels - d; pxl++)	// loop through pixels in main area
				{
					int *pFactors = pFilterVector;
					int sum = 0;
					unsigned int xBegin = pxl - d;
					FSCHAR *pPixelSrc = & pLineSrc[xBegin * dPixelSrc];
					unsigned int x;

					for (x = xBegin; x <= pxl + d; x++)
					{
						sum += *pFactors++ * *pPixelSrc;
						pPixelSrc += dPixelSrc;
					}

					if (filterVectorDenominator) sum /= filterVectorDenominator;
					*pPixelDest = (FSCHAR)sum;

					pPixelDest += dPixelDest;
				}

				for (pxl = nPixels - d; pxl < nPixels; pxl++) // loop through pixels in right/bottom margin
				{
					int *pFactors = pFilterVector;
					int denom = 0;
					int sum = 0;
					int xBegin = (pxl - d) < 0 ? 0 : (pxl - d);
					FSCHAR *pPixelSrc = & pLineSrc[xBegin * dPixelSrc];
					unsigned int x;
					for (x = xBegin; x < nPixels; x++)
					{
						denom += *pFactors;
						sum += *pFactors++ * *pPixelSrc;
						pPixelSrc += dPixelSrc;
					}

					if (denom) sum /= denom;

					* pPixelDest = (FSCHAR) sum;

					pPixelDest += dPixelDest;
				}

				pLineSrc += dLineSrc;
				pLineDest += dLineDest;
			}	// next line
		}
	}


	/* free hframe and pFilterVector */
	Free(hframe);
	if (pFilterVector) Free(pFilterVector);
	
	/* vframe now contains 2 dimensional result */
	return vframe;
}


//--------------------------------------------------------------------------------------
/*
 *		F S U N S H A R P M A S K
 *
 *	Inputs address of frame store, lines, cols, filter half width
 *		frame - Pointer to begining of frame store
 *		cols  - Number of pixels in a line
 *		lines - Number of lines in a frame
 *		halfwidth - Filter halfwidth i.e. 1 => 3x3, 2 => 5x5 etc
 *
 *	Outputs : new frame store containing result of NxN unsharp mask
 *		  where N = 2*halfwidth + 1
 *
 * Based on 'QGaussFilter', (c) 2003, Sjaak Priester, Amsterdam (sjaak@sjaakpriester.nl)
 */
FSCHAR *fsUnsharpMask(FSCHAR *frame, int cols, int lines, float radius, float depth)
{
	FSCHAR *pResult = NULL;

	if (radius <= 0.0 || depth <= 0.0) 
	{
			//do no processing, just return a copy of the image
			//the calling function may need this 
		pResult = (FSCHAR *)Malloc(lines * cols);
		if (pResult)
			memcpy (pResult,frame,sizeof (FSCHAR) * cols * lines);
		return pResult;
	}


	// Start with blur
	pResult = fsGaussian(frame, cols, lines, radius);
	if (pResult)
	{
		// Subtract blurred bitmap from original to get Unsharp Mask
		int denom = 10000;	// use an arbitrary denominator, not too small
		int dpt = ROUND((float) denom * depth);
		int dptplus = dpt + denom;

		FSCHAR *pPixelSrc = frame;
		FSCHAR *pPixelResult = pResult;

		unsigned int line;
		for (line = 0; line < lines; line++)	// loop through lines
		{
			unsigned int pxl;
			for (pxl = 0; pxl < cols; pxl++)	// loop through pixels
			{
				int v = dptplus * *pPixelSrc - dpt * *pPixelResult;
				v /= denom;

				// Clipping is very essential here. for large values of depth
				// (> 5.0f) more than half of the pixel values are clipped.
				if (v > 255) v = 255;
				if (v < 0)   v = 0;

				*pPixelResult = (FSCHAR)v;
				pPixelSrc++;
				pPixelResult++;
			}
		}
	}

	return pResult;
}


//--------------------------------------------------------------------------------------
