/*
 *	F S S E C T H I S T O . C
 *
 *	Create a histogram on image section (Assumed in byte format)
 *
 *	Mods:
 *
 *	10Mar2000	JMB		Changed headers included, as part of conversion to DLL.
 *						Also, made minor mods to stop compiler warnings and removed
 *						standard C parameter lists (were '#ifdef i386' only).
 *	9/19/96		BP:		Move comments from function declaration.
 *
 */

#include "woolz.h"

#include <stdio.h>
//#include <wstruct.h>

/*
 *	f s s e c t h i s t o --
 *
 *	Input:	image	- Start address of byte image (top left)
 *			width	- Image section width
 *			height	-	Image section height
 *			loffset	- Line to line offset
 *			maxg	- Max grey value expected - determines size of histogram
 *					  (Usually 255)
 *	Output:	Pointer to object of type histogram
 *
 *	This function copes with non-contiguous images, for contiguous images
 *	fsframehisto may be quicker
 */

struct object *
fssecthisto(register unsigned char *image, int width, int height, int loffset, int maxg)
{
	struct object *hist, *makemain();
	struct histogramdomain *hdom, *makehistodmn();
	register unsigned char *im;
	register int i, *h, line;

	/* make empty histogram */
	hdom = makehistodmn(1, maxg+1);
	hist = makemain(13, (struct intervaldomain *)hdom, NULL, NULL, NULL);

	/* fill histogram */
	h = hdom->hv;

	for (line = 0; line < height; line++) {
		im = image + (line * loffset);
		for (i=0; i < width; i++) {
			h[*im]++;
			im++;
		}
	}

	return(hist);
}

