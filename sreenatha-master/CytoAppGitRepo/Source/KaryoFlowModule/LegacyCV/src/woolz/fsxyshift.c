
/* 	fsxyshift.c	M.Gregson 23/6/94

	shift_image :

	Routine to shift an image in x and y directions within its frame buffer.
	Edges of the image are filled with the supplied 'background' colour.
*/

#include "woolz.h"

#include <stdio.h>
#include <string.h>
//#include <wstruct.h>


/********************************************************************* shift_image */


#ifdef WIN32
static void
shift_line(char *im1, char *im2, int width, int xshift, char background)
#endif
#ifdef i386
static void
shift_line(im1, im2, width, xshift, background)
char *im1;
char *im2;
int width;
int xshift;
char background;
#endif
{
	int xcount;

	if (xshift <= 0) {

		im1-=xshift;
	
		xcount=width+xshift;
		while (xcount--) {
			*im2++ = *im1++;
		}

		xcount=-xshift;
		while (xcount--) {
			*im2++ = background;
		}
	}
	else {
		im2+=(width-1);
		im1+=(width-1-xshift);
		
		xcount=width-xshift;
		while (xcount--) {
			*im2-- = *im1--;
		}

		xcount=xshift;
		while (xcount--) {
			*im2-- = background;
		}
	}
}	



#ifdef WIN32
void
shift_image(char *im, int width, int height, int xshift, int yshift, char background)
#endif
#ifdef i386
void
shift_image(im, width, height, xshift, yshift, background)
char *im;
int width;
int height;
int xshift;
int yshift;
char background;
#endif
{
	int ycount;
	char *im1, *im2;


	if (yshift <= 0) {
		
		im1=im - yshift*width;
		im2=im;

		ycount=height+yshift;
		while (ycount--) {
			shift_line(im1, im2, width, xshift, background);
			im2+=width;
			im1+=width;
		}

		ycount=-width*yshift;
		while (ycount--) {
			*im2++=background;
		}
	}
	else {
		im2=im + width*(height-1);
		im1=im + width*(height-1-yshift);
		
		ycount=height-yshift;
		while (ycount--) {
			shift_line(im1, im2, width, xshift, background);
			im2-=width;
			im1-=width;
		}

		ycount=width*yshift;
		im2=im;
		while (ycount--) {
			*im2++=background;
		}
	}
}

/********************************************************************* shift_image */



