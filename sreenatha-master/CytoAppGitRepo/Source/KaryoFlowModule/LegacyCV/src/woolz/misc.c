/*
 *      M I S C . C  --  General nondescript bits and pieces
 *
 *
 *  Written: Clive A Stubbings
 *           Image Recognition Systems
 *           720 Birchwood Boulevard
 *           Birchwood Science Park
 *           Warrington
 *           Cheshire
 *           WA3 7PX
 *
 *
 *  Date:    2nd June 1987
 *
 *  Modifications
 *
 *	 1 Nov 1991		CAS			Cock ups in str2cmp
 *	20 Jul 1990		CAS_H		Bug in strncasecmp (went 1 char too far)
 *	18 Sep 1988		CAs			Added inbuffer, instring
 *	 6 Apr 1988		CAS			Added bcopy
 *	31 Mar 1988		CAS			Added strncasecmp
 *	15 Mar 1988		CAS			Added str2cmp
 *	23 Jul 1987		CAS			Added 'touch'
 */

#include "woolz.h"


/*
 *	S T R N T C P Y  --  Copy a string up to n char or a char with the top
 *						bit set;
 */
#ifdef WIN32
int
strntcpy(char *s1, char *s2, int n)
#endif
#ifdef i386
int
strntcpy(s1, s2, n)
char *s1;
char *s2;
int n;
#endif
{
	while (*s2 && (n-- > 0)) {
		*s1++ = *s2 & 0x7f;
		if (*s2++ & 0x80)
			break;
	}
	*s1 = '\0';
}

/*
 *	S T R C A S E C M P  --  Compare two strings but ignore case
 *							 differences and top bit differences
 */
#ifdef WIN32
int
strcasecmp(char *s1, char *s2)
#endif
#ifdef i386
int
strcasecmp(s1, s2)
char *s1;
char *s2;
#endif
{
	while (*s1 && *s2) {
		if (toupper(*s1 & 0x7f) != toupper(*s2 & 0x7f))
			break;
		s1++; s2++;
	}
	return(toupper(*s1 & 0x7f) - toupper(*s2 & 0x7f));
}

/*
 *	S T R N C A S E C M P  --  Compare two strings but ignore case
 *								 differences and top bit differences
 */
#ifdef WIN32
int
strncasecmp(char *s1, char *s2, int len)
#endif
#ifdef i386
int
strncasecmp(s1, s2, len)
char *s1;
char *s2;
int len;
#endif
{
	while (*s1 && *s2 && (--len > 0)) {
		if (toupper(*s1 & 0x7f) != toupper(*s2 & 0x7f))
			break;
		s1++; s2++;
	}
	return(toupper(*s1 & 0x7f) - toupper(*s2 & 0x7f));
}

/*
 *	A X T O I  --  Ascii hex to integer convert
 *
 */
#ifdef WIN32
int
axtoi(char *ptr)
#endif
#ifdef i386
int
axtoi(ptr)
char *ptr;
#endif
{
	int		num;
	num = 0;
	while (*ptr) {
		if ((*ptr >= '0') && (*ptr <= '9'))
			num = num *16 + *ptr - '0';
		else if ((*ptr >= 'A') && (*ptr <= 'F'))
			num = num * 16 + *ptr - 'A' + 10;
		else if ((*ptr >= 'a') && (*ptr <= 'f'))
			num = num * 16 + *ptr - 'a' + 10;
		else
			break;
		ptr++;
	}
	return(num);
}

/*
 *	S T R 2 C M P  --  Compare a muddled string against its 2 components
 *
 *	Returns:	Pointer to mismatch or NULL if OK
 */
#ifdef WIN32
char *
str2cmp(char *s, char *s1, char *s2)
#endif
#ifdef i386
char *
str2cmp(s, s1, s2)
char *s;
char *s1;
char *s2;
#endif
{
	char	*s1mat, *s2mat;
	s1mat = s1; s2mat = s2;
	while (*s) {
		if (*s1 == *s2) {
			if (*s != *s1)
				return(s);
			else {
				s1++;
				s2++;
			}
		} else {
			if (*s == *s1) {
				s1++; s++;
				s1mat = s1;
				s2 = s2mat;
			} else if (*s == *s2) {
				s2++; s++;
				s2mat = s2;
				s1 = s1mat;
			} else
				return(s);
		}
	}
	if ((*s1 == '\0') && (*s2 == '\0'))
		return(0);
	else
		return(s);
}

/*
 *	T O U C H
 *
 */
#ifdef WIN32
int
touch(char *name)
#endif
#ifdef i386
int
touch(name)
char *name;
#endif
{
	int		fd;
	if ((fd = creat(name,2)) == -1)
		return(0);
	close(fd);
	return(1);
}


/*
 *	B C O P Y  --
 *
 */
#ifdef WIN32
int
bcopy(char *b1, char *b2, int length)
#endif
#ifdef i386
int
bcopy(b1, b2, length)
char *b1;
char *b2;
int length;
#endif
{
	while (length--)
		*b2++ = *b1++;
}

/*
 *	I N S T R I N G  --  Is string s2 in string s1 ??
 *
 *	Returns position if yes, -1 otherwise
 */
#ifdef WIN32
int
instring(char *s1, char *s2)
#endif
#ifdef i386
int
instring(s1, s2)
char *s1;
char *s2;
#endif
{
	return(inbuffer(s1,strlen(s1),s2,strlen(2)));
}

/*
 *	I N B U F F E R  --  Is string s2 in string s1 ??
 *
 *	Returns position if yes, -1 otherwise
 */
#ifdef WIN32
int
inbuffer(char *s1, int l1, char *s2, int l2)
#endif
#ifdef i386
int
inbuffer(s1, l1, s2, l2)
char *s1;
int l1;
char *s2;
int l2;
#endif
{
	char	*p1,*p2,*restart;
	char	*e1, *e2;
	p1 = s1; p2 = s2;
	e1 = s1 + l1; e2 = s2 + l2;
	while (p2 < e2) {
		while (*p1 != *p2) {
			if (++p1 >= e1)
				return(-1);
		}
		restart = ++p1;	p2++;
		while (*p1++ == *p2)
			if (++p2 >= e2)
				return((p1-s1)-l2);
		p1 = restart;
		p2 = s2;
	}
	return((p1-s1)-l2);
}

