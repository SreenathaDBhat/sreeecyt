
/*
 * normalise.c	Jim Piper	July 1983
 *
 * Modifications
 *
 *	9/20/96		BP:		void function return.
 *
 *	08 May 1987		BDP		protection against null or empty objs
 *	13 Sep 1986		CAS		Includes
 *	BP	12/12/94:	Check range is valid before proceeding.
 *	BP	12/13/94:	Now use [much faster] new function gmmcontrast().
 */

#include "woolz.h"

#include <stdio.h>
//#include <wstruct.h>

/*
 * normalise grey-table of object to lie in range 0-255
 * Protection against null and empty objects is given to this module by
 * protection in greyrange and the scanning routines. If any problem exists
 * in the supplied object then the while loop will fail at its first attempt.
 * no other changes have been added therefore.  bdp 8/5/87
 */
#ifdef WIN32
void
normalise(struct object *obj)
#endif
#ifdef i386
void
normalise(obj)
struct object *obj;
#endif
{
	GREY min, max;

	greyrange(obj, &min, &max);
	gmmcontrast(obj, min, max);


#if 0

	GREY min, max, range;
	register GREY *g;
	register int i;
	struct iwspace iwsp;
	struct gwspace gwsp;

	greyrange(obj, &min, &max);

/* BP */
	if ((range = max - min) < 1)
		range = 1;

	initgreyscan(obj,&iwsp,&gwsp);
	while (nextgreyinterval(&iwsp) == 0)
	{
		g = gwsp.grintptr;
		gstretch(g, iwsp.colrmn, 255, min, range);
	}

#endif

}
