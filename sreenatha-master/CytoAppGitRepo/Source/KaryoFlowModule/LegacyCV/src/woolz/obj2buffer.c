
/*
 * obj2buffer.c		BP: 13/5/93
 *
 * Modifications:
 *
 *	10Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *					Also, made minor mods to stop compiler warnings and removed
 *					standard C parameter lists (were '#ifdef i386' only).
 * BP	9/21/94		Now uses ddgs aux buffer rather than malloc.
 *					No mask generated. Since this function is only
 *					used for probes, there is a much more convenient
 *					(and fast) way of deriving the mask (24bitcanvas.c).
 */




/*
 * Functions to draw a type 1 object into a
 * rectangular buffer and create a single-
 * plane mask buffer from same.
 * This file supercedes an older version with
 * the same name which didn't work properly
 * with some objects (not my fault - woolz's).
 */



#include "woolz.h"

#include "ddgs.h"

#include <stdio.h>
#include <memory.h>

//#include <wstruct.h>




#define ABS(i)	(i >= 0 ? i : -i)




/****************************************************** set_object_buffer */

/*
 * Traverse the object and draw it into an
 * 8-bit buffer.
 * Must be type 1 object.
 * The buffer is NOT malloc'd here (a ddgs
 * draw buffer is used), and must NOT be
 * freed when finished with.
 * The return values are in screen pixel
 * coordinates, with origin at the top left.
 */

unsigned char *
set_object_buffer(struct object *obj, int *xout, int *yout, int *wout, int *hout, struct pframe *f)
{
	struct iwspace iwsp;
	struct gwspace gwsp;
	GREY *temp;
	unsigned char *outbuffer = NULL;
	unsigned char *outtemp, *outtempstart;
	unsigned outbufferend;
	int x, y, w, h, fullw, fullh;
	int abs_xs, abs_ys, xs, ys, lasty, lastlinpos;
	register short i, j;
	register int offset_inc, offset_big;
	struct valuetable *vdom;
	int retval;


	vdom = obj->vdom;

	xs = f->ix*f->scale;
	ys = f->iy*f->scale;

	abs_xs = ABS(xs);
	abs_ys = ABS(ys);

/* Calculate the width and height of the
 * required buffer. These are defined by
 * the limits of the idom. */
	fullw = (abs_xs*(1 + obj->idom->lastkl - obj->idom->kol1)) >> 3;
	fullh = (abs_ys*(1 + obj->idom->lastln - obj->idom->line1)) >> 3;

/* Output the position of the TOP-LEFT
 * corner of the rectangle in SCALED-DOWN
 * dogs coordinates. */
	*xout = (f->dx - xs*f->ox + xs*obj->idom->kol1) >> 3;
	*yout = (f->dy - ys*f->oy + ys*obj->idom->line1) >> 3;

	if (ys > 0)
		*yout += fullh;

	if (xs < 0)
		*xout -= fullw;

/* Output width and height. */
	*wout = fullw;
	*hout = fullh;


/* BP */
/* Now use ddgs aux buffer. */

	outbuffer = (unsigned char *)ddgs_get_aux_buffer(fullw*(fullh + 1));
	outbufferend = (unsigned)outbuffer + fullw*(fullh + 1);

/* Fixed-point incrementer for scaling. */
	offset_inc = 64/xs;

/* Pretend we have started just before
 * the beginning (???). This keeps the
 * loop happy. */
	lasty = -(abs_ys/8);
	lastlinpos = obj->idom->line1 - 1;

/* Scan through object on a interval-by-
 * interval basis. */
	initrasterscan(obj, &iwsp, 0);

	if (vdom!=NULL) {
		initgreywsp(obj, &iwsp, &gwsp, 0);
		retval=nextgreyinterval(&iwsp);
	}
	else
		retval=nextinterval(&iwsp);

	while (retval == 0)
	{
/* Height "above" first line. */
		y = (abs_ys*(iwsp.linpos - obj->idom->line1)) >> 3;

/* Height depends on scale.
 * Don't recalculate if interval is
 * on same line as last one. */
		if (iwsp.linpos > lastlinpos) {
			if (iwsp.linpos - lastlinpos > 1) {
				/* handle empty intervals */
				lasty=(abs_ys*(iwsp.linpos - 1 - obj->idom->line1)) >> 3;
			}

			h = y - lasty;
		}
				
/* Height may be 0 when object
 * is scaled-down. In which case
 * ignore this interval. */
		if (h < 1) {
			if (vdom!=NULL) 
				retval=nextgreyinterval(&iwsp);
			else
				retval=nextinterval(&iwsp);
			continue;
		}

/* Find start point in buffer. This
 * depends on the sign of yscale. If
 * negative, start from top of buffer
 * if positive, start from end and work
 * back up. These XXXXtempstart pointers
 * are used for convenience and point to
 * the first location in the particular
 * line in the buffer. X offsets are
 * therfore always relative to these. */
		if (ys < 0)
			outtempstart = outbuffer + (y*fullw);
		else
			outtempstart = outbuffer + (fullw*(fullh - 1 - y));

/* Calculate distance of start of
 * interval from edge of buffer, and
 * the scaled width of the interval. */
		x = (abs_xs*(iwsp.lftpos - obj->idom->kol1)) >> 3;
		w = (abs_xs*iwsp.colrmn) >> 3;

/* If xscale is negative we scan from
 * the end of the interval backwards. */
		if (xs < 0)
		{
			offset_big = (iwsp.colrmn << 3) - 1;
			x = fullw - x - w;
		}
		else
			offset_big = 0;

/* If this interval is not on same
 * line as last one reset these bits. */
		if (iwsp.linpos > lastlinpos)
		{
			lasty = (abs_ys*(iwsp.linpos - obj->idom->line1)) >> 3;
			lastlinpos = iwsp.linpos;
		}

/* Point to first required location
 * in buffer. */
		outtemp = outtempstart + x;

/* Quick check that we're not going
 * beyond the buffer. Probably unnecessary. */
		if ((unsigned)outtemp < (unsigned)outbuffer)
{
/* fprintf(stderr, "set_object_buffer: underflow\n"); */
			return(outbuffer);
}

		if ((unsigned)outtemp >= outbufferend - w)
{
/* fprintf(stderr, "set_object_buffer: overflow\n"); */
			return(outbuffer);
}

/* For each point along SCALED interval. */
		for (i = 0; i < w; i++)
		{
			if (vdom != NULL)
			{
/* Get pointer to grunt value. Note
 * fixed-point scaling is x8. */
				temp = gwsp.grintptr + (offset_big >> 3);

				*outtemp++ = *temp;

/* Increment offset by 8x required
 * offset. */
				offset_big += offset_inc;
			}
		}

/* Increment pointers ready for
 * next line in buffer. */
		if (ys < 0)
			outtempstart += fullw;
		else
			outtempstart -= fullw;

/* If h was > 1, (ie we are scaled-up)
 * there are a number of lines which
 * exact copies of what we just did. */
		j = 1;
		while (j < h)
		{
			j++;

/* Repeated lines are AFTER the last. */
			if (ys < 0)
			{
				if ((unsigned)outtempstart >= outbufferend - fullw)
{
/* fprintf(stderr, "set_object_buffer: overflow 2\n"); */
					return(outbuffer);
}

				if (vdom != NULL)
					memcpy(outtempstart,
						outtempstart - fullw, fullw);

				outtempstart += fullw;
			}
			else
/* Repeated lines are BEFORE the last. */
			{
				if ((unsigned)outtempstart < (unsigned)outbuffer)
{
/* fprintf(stderr, "set_object_buffer: underflow 2\n"); */
					return(outbuffer);
}

				if (vdom != NULL)
					memcpy(outtempstart,
						outtempstart + fullw, fullw);

				outtempstart -= fullw;
			}
		}
		if (vdom!=NULL) 
			retval=nextgreyinterval(&iwsp);
		else
			retval=nextinterval(&iwsp);

	}

	return(outbuffer);
} /* set_object_buffer */




/****************************************************** set_object_mask */

/*
 * Traverse the object and generate the mask
 * only. This was derived from set_object_buffer.
 * Must be type 1 object.
 * The return values are in screen pixel
 * coordinates, with origin at the top left.
 */

unsigned char *
set_object_mask(struct object *obj, int *xout, int *yout, int *wout, int *hout, struct pframe *f)
{
	struct iwspace iwsp;
	unsigned char *maskbuffer = NULL;
	unsigned char *masktemp, *masktempstart;
	int x, y, w, h, fullw, fullh, maskw;
	int abs_xs, abs_ys, xs, ys, lasty, lastlinpos;
	register short i, j;
	register unsigned char maskbyte, maskbit;


	xs = f->ix*f->scale;
	ys = f->iy*f->scale;

	abs_xs = ABS(xs);
	abs_ys = ABS(ys);

/* Calculate the width and height of the
 * required buffer. These are defined by
 * the limits of the idom. */
	fullw = (abs_xs*(1 + obj->idom->lastkl - obj->idom->kol1)) >> 3;
	fullh = (abs_ys*(1 + obj->idom->lastln - obj->idom->line1)) >> 3;

/* Output the position of the TOP-LEFT
 * corner of the rectangle in SCALED-DOWN
 * dogs coordinates. */
	*xout = (f->dx - xs*f->ox + xs*obj->idom->kol1) >> 3;
	*yout = (f->dy - ys*f->oy + ys*obj->idom->line1) >> 3;

	if (ys > 0)
		*yout += fullh;

	if (xs < 0)
		*xout -= fullw;

/* Output width and height. */
	*wout = fullw;
	*hout = fullh;

/* Get the necessary buffer from ddgs. */
	maskw = (fullw + 7) >> 3;
	maskbuffer = (unsigned char *)ddgs_get_draw_mask(fullw, fullh + 1);

/* Pretend we have started just before
 * the beginning (???). This keeps the
 * loop happy. */
	lasty = -(abs_ys/8);
	lastlinpos = obj->idom->line1 - 1;

/* Scan through object on a interval-by-
 * interval basis. */
	initrasterscan(obj, &iwsp, 0);

	while (nextinterval(&iwsp) == 0)
	{
/* Height "above" first line. */
		y = (abs_ys*(iwsp.linpos - obj->idom->line1)) >> 3;

/* Height depends on scale.
 * Don't recalculate if interval is
 * on same line as last one. */
		if (iwsp.linpos > lastlinpos)
		{
/* Handle empty intervals. */
			if (iwsp.linpos - lastlinpos > 1)
				lasty = (abs_ys*(iwsp.linpos - 1 - obj->idom->line1)) >> 3;

			h = y - lasty;
		}
				
/* Height may be 0 when object
 * is scaled-down. In which case
 * ignore this interval. */
		if (h < 1)
			continue;

/* Find start point in buffer. This
 * depends on the sign of yscale. If
 * negative, start from top of buffer
 * if positive, start from end and work
 * back up. These XXXXtempstart pointers
 * are used for convenience and point to
 * the first location in the particular
 * line in the buffer. X offsets are
 * therfore always relative to these. */
		if (ys < 0)
			masktempstart = maskbuffer + (y*maskw);
		else
			masktempstart = maskbuffer + (maskw*(fullh - 1 - y));

/* Calculate distance of start of
 * interval from edge of buffer, and
 * the scaled width of the interval. */
		x = (abs_xs*(iwsp.lftpos - obj->idom->kol1)) >> 3;
		w = (abs_xs*iwsp.colrmn) >> 3;

/* If xscale is negative we scan from
 * the end of the interval backwards. */
		if (xs < 0)
			x = fullw - x - w;

/* If this interval is not on same
 * line as last one reset these bits. */
		if (iwsp.linpos > lastlinpos)
		{
			lasty = (abs_ys*(iwsp.linpos - obj->idom->line1)) >> 3;
			lastlinpos = iwsp.linpos;
		}

/* Point to first required location
 * in buffer. */
		masktemp = masktempstart + (x >> 3);
		maskbit = 1 << (x & 0x07);
		maskbyte = *masktemp;

/* For each point along SCALED interval. */
		for (i = 0; i < w; i++)
		{
			maskbyte = maskbyte | maskbit;
			maskbit = maskbit << 1;

/* If we have just done the top bit
 * we need to go to next byte and
 * start again from LSB. */
			if (maskbit == 0)
			{
				*masktemp++ = maskbyte;
				maskbit = 1;
				maskbyte = 0;
			}
		}

/* Just make sure we put end of
 * interval into mask (remember these
 * were only put in when the byte was
 * full). */
		if (maskbyte != 0)
			*masktemp = maskbyte;

/* Increment pointers ready for
 * next line in buffer. */
		if (ys < 0)
			masktempstart += maskw;
		else
			masktempstart -= maskw;

/* If h was > 1, (ie we are scaled-up)
 * there are a number of lines which
 * exact copies of what we just did. */
		j = 1;
		while (j < h)
		{
			j++;

/* Repeated lines are AFTER the last. */
			if (ys < 0)
			{
				memcpy(masktempstart, masktempstart - maskw, maskw);
				masktempstart += maskw;
			}
			else
/* Repeated lines are BEFORE the last. */
			{
				memcpy(masktempstart, masktempstart + maskw, maskw);
				masktempstart -= maskw;
			}
		}
	}

	return(maskbuffer);
} /* set_object_mask */

