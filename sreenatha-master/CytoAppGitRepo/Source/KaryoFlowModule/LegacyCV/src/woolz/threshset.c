/*
 * threshset.c
 *
 * set label topology and threshold under/over
 *
 * Modifications
 *
 *	 7 Feb 1991		CAS		voids
 *	13 Sep 1986		CAS		Includes
 */

#include "woolz.h"

#include <stdio.h>
//#include <wstruct.h>

SMALL jdqt = 0, highlow = 1;
/*
 * set connection topology - 
 * diagonal if jdqt = 0,
 * square if jdqt = 1.
 */
#ifdef WIN32
void
topset(int jjdqt)
#endif
#ifdef i386
void
topset(jjdqt)
int jjdqt;
#endif
{ 
	jdqt = jjdqt;
	return;
} 

/*
 * set mode for selection of pixels - 
 * >= threshold if hi != 0,
 * < threshold if hi == 0;
 */
#ifdef WIN32
void
hilow(int hi)
#endif
#ifdef i386
void
hilow(hi)
int hi;
#endif
{ 
	highlow = hi;
	return;
} 
