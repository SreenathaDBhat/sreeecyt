/*
 *      W T C H E C K . C  --  woolz object checking routines (modified version)
 *
 *
 *  Written: B. Pinnington (original version)
 *           Image Recognition Systems
 *           720 Birchwood Boulevard
 *           Birchwood Science Park
 *           Warrington
 *           Cheshire
 *           WA3 7PX
 *
 * Copyright(c) and intellectual property rights Image Recognition Systems 1988
 * Copyright etc Applied Imaging 1995
 *
 *  Date:    10th January 1988
 *
 *	Description
 *		Several routines are provided for checking a whole
 *		object, or just checking a domain. In all cases
 *		the result is returned as zero for correct, and
 *		non zero for any error. A summary of each object
 *		is sent to wz_db_fp, plus highlighted error messages.
 *
 *		This program is a modified version of the original wtcheck.c
 *		
 *		The original version is in this directory and is called wtcheck_old.c.
 *		I have removed the original modification list see wtcheck_old for this
 *
 *  Modifications
 *
 *	10Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *					Also, made minor mods to stop compiler warnings and removed
 *					standard C parameter lists (were '#ifdef i386' only).
 *	WH	06Feb98:	woolz_check_obj handles objects of type 90, Raw Canvas.
 *	WH	11Jan98:	Attach the default error handler if woolz_check_level()
 *					sets a level. For Windows version as we need to test for NULLs.
 *	BP	7/20/95:	Fixed endif compiler warning
 *	18 Jul 1992	MC	Removed all Alloc_size() calls and associated code, also
 *				removed all subsequent code in each function. freemem() and
 *				associated code	also removed.
 *				REASON : At this stage in the RED project it is 
 *				considered that it will be to time consuming to modify alloc.c
 *				(which contains Alloc_size()) to work on these UNIX PCs.
 *
 *				freemem() removed because it is not needed, also no source code
 *				available. freemem() is an OS9 C library function.
 *
 *				code after tests using allocsize has been removed, this
 *				of course was not strictly necesary. However simple removal
 *				was the simplest solution, otherwise time would need to be spent
 *				in making sure which was kept and which removed. If code removed
 *				(or not removed when it should have been(, could end up getting
 *				error messages when no error exists !
 *
 *				No changes done to woolz_check_obj() or woolz_check_tdom()
 */

#include "woolz.h"

#include <stdio.h>
//#include <wstruct.h>
//#include <wtcheck.h>
#include "wtcheck.h"
//#include <wtcklims.h>
#include "wtcklims.h"

/*
 * statics
 */
static FILE *wz_db_fp;		/* set in woolz_debug */
static int check_level;		/* level of checking */

/* 
 * general space checking variables 
 */
static int	ecount, 	/* calculated count of elements of type being tested */
		acount;		/* count of allocated elements of type being tested */



static int ckcoord(WZCOORD line1, WZCOORD lastln, WZCOORD kol1, WZCOORD lastkl);
static int ckfcoord(float line1, float lastln, float kol1, float lastkl);


/*
 *
 *	Function:		woolz_check_obj
 *
 *	Purpose:		check consistency of supplied object
 *
 *	Description:	Checks the object for being null, or for its first
 *				domain pointer being null. Also checks that type is
 *				legal. Any failure is reported with unique status
 *				Function can be further context expanded via the
 *				the case table established. Logging of errors and
 *				handling of fatal errors may be added later
 *
 *	Entry:			object to be checked
 *
 *	Exit:			0 for correct,  non-zero for error status
 *
 */

int
woolz_check_obj(struct object *obj, char *called_from)
{
	register int objstatus = 0;
	
/*----*/if(check_level < 1) return(0);

	
	if (obj == NULL)
		objstatus = CKNULL;
	else {
		switch (obj->type) {
			case 1:		/* interval structured object */
			case 2:
				if ((objstatus = woolz_check_idom(obj->idom, called_from))==0) {
					if (obj->vdom != NULL)
						objstatus =  woolz_check_vdom(obj, called_from);
				}
				break;
			case 10:	/* polygon object */
				if (obj->idom == NULL)
					objstatus =  PDMNULL;
				else
					objstatus =  woolz_check_pdom((struct polygondomain *)obj->idom, called_from);
				break;
			case 11:	/* Boundary list object */
				if ((obj->vdom != NULL) || (obj->plist != NULL))
					objstatus =  BDLEDPTR;
				else {
					objstatus =  woolz_check_blist((struct boundlist *)obj->idom, called_from);
				}
				break;
			case 12:
				if (obj->idom == NULL)
					objstatus =  PDMNULL;
				else {
					objstatus =  woolz_check_pdom((struct polygondomain *)obj->idom, called_from);
					if ( objstatus == 0 ) {
						if (obj->vdom == NULL)
							objstatus = CVDNULL;
						else
							objstatus = woolz_check_cvhdom((struct cvhdom *)obj->vdom, called_from);
					}
				}
				break;
			case 13:
				if (obj->vdom != NULL)
					objstatus = HDMEDNULL;
				else
					if (obj->idom == NULL)
						objstatus = HDMNULL;
					else
						objstatus = woolz_check_hdom((struct histogramdomain *)obj->idom, called_from);
				break;
			case 20:
				if (obj->idom == NULL)
					objstatus = RDMNULL;
				else
					objstatus = woolz_check_rdom((struct irect *)obj->idom, called_from);
				break;
			case 30:
			case 31:
			case 40:
			case 41:
				break;		/* no check implemented yet for these cases */
			case 70:
				objstatus = woolz_check_tdom((struct text *)obj, called_from);
				break;
			case 90:		/* Raw */
				break;		/* No check as yet */
			case 100:
				break;		// No check for circles
			case 110:
				if ((obj->idom != NULL) || (obj->vdom != NULL))
					objstatus = HDREDPTR;
				break;
			default:
				objstatus = CKINVTYPE;
		}
	}
	if(objstatus)
		return(woolz_check_exit(objstatus, called_from, (char *)obj));
	else
		return(0);
}
/*
 *
 *	Function:		woolz_check_idom
 *
 *	Purpose:		perform totally rigorous validation of interval dom
 *
 *	Description:	Performs a series of complex tests on the
 *			interval domain and its associated structures.
 *				tests:
 *				1. Check for NULL or empty domain
 *				2. Type
 *				3. Linkcount range
 *				4. Domain extent
 *				5. Free ptr == first interval line ptr ( not mand.)
 *	Entry:			Interval domain pointer
 *
 *	Exit:			Exit status of first error detected or 0 if o.k.
 *
 */
int
woolz_check_idom(register struct intervaldomain *idom, char *called_from)
{

/*----*/if(check_level < 1) return(0);

	/* first check that its not empty */
	if (idom == NULL)
		return(woolz_check_exit(IDMNULL, called_from, (char *)idom));
	else
		if (idom->line1 > idom->lastln)
			return(woolz_check_exit(CKEMPTYDMN, called_from, (char *)idom));

/*----*/if(check_level < 2) return(0);

	/* check type */
	if ((idom->type != 1) && (idom->type != 2) )
		return(woolz_check_exit(IDMETYPE, called_from, (char *)idom));

	return(0);			/* no problems */	
}

/*
 *
 *	Function:		woolz_check_pdom
 *
 *	Purpose:		Check a polygon domain data structure exhaustively
 *
 *	Description:	Performs a number of data integrity checks on the
 *					domain and returns the first encountered error.
 *					tests:
 *					1. Domain exists
 *					2. Type
 *					3. Empty
 *					4. Vertices count < max count
 *					5. vertices count not negative or > predefined limit
 *					6. Vertex coordinates ranges
 *
 *	Entry:			Polygon domain pointer
 *
 *	Exit:			Status of first detected error or 0 if o.k.
 *
 */
int
woolz_check_pdom(register struct polygondomain *pdom, char *called_from)
{
	register struct ivertex *vx;
	register struct fvertex *fx;
	register int i;
	int vertexsize;
		
/*----*/if(check_level < 1) return(0);

	if (pdom == NULL)
		return(woolz_check_exit(PDMNULL, called_from, (char *)pdom));

/*----*/if(check_level < 2) return(0);
	
	if ((pdom->type != 1) && (pdom->type != 2))
		return(woolz_check_exit(PDMETYPE, called_from, (char *)pdom));
	
	if (pdom->nvertices == 0)
		return(woolz_check_exit(CKEMPTYDMN, called_from, (char *)pdom));
	
	/* check the running count is less than the allocated count */
	if (pdom->nvertices > pdom->maxvertices)
		return(woolz_check_exit(PDMECNT, called_from, (char *)pdom));
	
	/* check that the counts are sensible anyway */
	if ( (pdom->nvertices < 0) || (pdom->nvertices > CKPDMNVXMAX) )
		return(woolz_check_exit(PDMEVCNT, called_from, (char *)pdom));

/*----*/if(check_level < 3) return(0);
		
	/* now check that all the vertex coodinates are sensible */
	switch(pdom->type)
	{
	case 1:
		vx = pdom->vtx;
		for ( i=0; i<pdom->nvertices; i++ ) {
			if ( ckcoord(vx->vtY, 0, vx->vtX, 0) )
				return(woolz_check_exit(PDMECOORD, called_from, (char *)pdom));
			vx++;
		}
		vertexsize = sizeof(struct ivertex);	/* for memory check later */
		ecount = pdom->vtx - (struct ivertex *)(pdom + 1); /* see if any skipped */
		break;
		
	case 2:
		fx = (struct fvertex *) pdom->vtx;
		for ( i=0; i<pdom->nvertices; i++ ) {
			if ( ckfcoord(vx->vtY, 0, vx->vtX, 0) )
				return(woolz_check_exit(PDMECOORD, called_from, (char *)pdom));
			fx++;
		}
		vertexsize = sizeof(struct fvertex);	/* for memory check later */
		ecount = ((struct fvertex *)pdom->vtx) - ((struct fvertex *)(pdom + 1)); /* see if any skipped */
		break;
	
	}	/* end switch */
	
	/* check that allocated domain memory is not exceeded */
	if (ecount > 0)
		if(wz_db_fp)
			fprintf(wz_db_fp, "CHECK WARNING: %d vertices skipped\n",ecount);	

	return(0);			/* everything looks o.k. */
}

/*
 *
 *	Function:		woolz_check_vdom
 *
 *	Purpose:		Validate a value table as far as possible
 *
 *	Description:	Performs extensive checking across all three types
 *					of values domain structure.
 *						Test the following:
 *					1. Linkcount
 *
 *	Entry:			N.B. an OBJECT pointer is supplied here
 *
 *	Exit:			Zero if o.k. or status of first detected error
 *
 */
int
woolz_check_vdom(struct object *vobj, char *called_from)
{
	register struct valuetable *vdom;
	struct intervaldomain *idom;
		
/*----*/if(check_level < 1) return(0);

	vdom = vobj->vdom;
	idom = vobj->idom;

	if(idom == NULL)
		return(woolz_check_exit(IDMNULL, called_from, (char *)idom));
	
	if(vdom == NULL)
		return(woolz_check_exit(VDMNULL, called_from, (char *)vdom));
		
/*----*/if(check_level < 2) return(0);
	
	/* Check linkcount */
	if ((vdom->linkcount < 0 ) || (vdom->linkcount > CKVDMNLCMAX))
		return(woolz_check_exit(VDMELC, called_from, (char *)vdom));

	return(0);		/* successfully tested */
}

/*
 *
 *	Function:		woolz_check_cvhdom
 *
 *	Purpose:		Exhaustive validation of convex hull domain
 *
 *	Description:	Performs following tests:
 *					1. Mid coordinates are in range
 *					2. No of sig chords <= chords
 *					3. No of chords > 0 and <= predefined maximum
 *
 *	Entry:			Convex hull domain pointer
 *
 *	Exit:			Error status of first detected error, or zero if ok
 *
 */
int
woolz_check_cvhdom(struct cvhdom *cdom, char *called_from)
{

/*----*/if(check_level < 1) return(0);
	
	if(cdom == NULL)
		return(woolz_check_exit(CVDNULL, called_from, (char *)cdom));
		
/*----*/if(check_level < 2) return(0);

	/* 1. check mid coordinates a ok */
	if( ckcoord( cdom->mdlin, 0, cdom->mdkol, 0) )
		return(woolz_check_exit(CVDECOORD, called_from, (char *)cdom));
	
	/* 2. check sigcount is not greater than count */

	if( cdom->nsigchords > cdom->nchords)
		return(woolz_check_exit(CVDECNT, called_from, (char *)cdom));
	
	/* 3. check chord count sensible */

	if( (cdom->nchords > CKCDMNCHMAX) || (cdom->nchords < 0) )
		return(woolz_check_exit(CVDECCNT, called_from, (char *)cdom));
		
	/* 
	 * check that allocated domain memory is not exceeded 
	 */
	ecount = cdom->ch  - ( (struct chord *)(cdom + 1)) ;
	if (ecount > 0)
		if(wz_db_fp)
			fprintf(wz_db_fp, "CHECK WARNING: %d chords skipped\n",ecount);

	return(0);		/* successfully completed all tests */
}

/*
 *
 *	Function:		woolz_check_rdom
 *
 *	Purpose:		Check a rectangle domain
 *
 *	Description:	Checks domain boundaries for int or float rects
 *					tests:
 *					1. Type
 *					2. Check coordinates
 *
 *	Entry:			rectangle domain pointer
 *
 *	Exit:			Zero is successful, or non-zero error status
 *
 */

int
woolz_check_rdom(struct irect *rdom, char *called_from)
{
	struct frect *frdom;

/*----*/if(check_level < 1) return(0);

	if(rdom == NULL)
		return(woolz_check_exit(RDMNULL, called_from, (char *)rdom));
		
/*----*/if(check_level < 2) return(0);

	/* 1. Check type */
	/* 2. Check rectangle coordinates range */
	switch(rdom->type) {
		case 1:
			if (ckcoord(rdom->irl[0], rdom->irl[1], rdom->irk[0], rdom->irk[1]))
				return(woolz_check_exit(RDMECOORD, called_from, (char *)rdom));
			if (ckcoord(rdom->irl[2], rdom->irl[3], rdom->irk[2], rdom->irk[3]))
				return(woolz_check_exit(RDMECOORD, called_from, (char *)rdom));
			break;
		case 2:
			frdom = (struct frect *) rdom;
			if (ckfcoord(frdom->frl[0], frdom->frl[1], frdom->frk[0], frdom->frk[1]))
				return(woolz_check_exit(RDMECOORD, called_from, (char *)frdom));
			if (ckfcoord(frdom->frl[2], frdom->frl[3], frdom->frk[2], frdom->frk[3]))
				return(woolz_check_exit(RDMECOORD, called_from, (char *)frdom));
			break;
		default:
			return(woolz_check_exit(RDMETYPE, called_from, (char *)rdom));
	}

	return(0);
}

/*
 *
 *	Function:		woolz_check_hdom
 *
 *	Purpose:		Exhaustively validate a histogram domain
 *
 *	Description:	Test the following:
 *					1. Type
 *					2. R, vertical or horizontal indicator
 *					3. Number of points range
 *					4. Row column of output, range
 *
 *	Entry:			Histogram domain pointer
 *
 *	Exit:			Error status of first error or zero if o.k.
 *
 */
int
woolz_check_hdom(struct histogramdomain *hdom, char *called_from)
{
//	register int hdomsize, inout;

/*----*/if(check_level < 1) return(0);

	if(hdom == NULL)
		return(woolz_check_exit(HDMNULL, called_from, (char *)hdom));
		
/*----*/if(check_level < 2) return(0);
	
	/* 
	 * 1,2. first check that the type field and 'r' field each contain expected
	 * values
	 */
	if((hdom->type != 1) && (hdom->type != 2))
		return(woolz_check_exit(HDMETYPE, called_from, (char *)hdom));

	if( (hdom->r != 1) && (hdom->r != 2) )
		return(woolz_check_exit(HDMER, called_from, (char *)hdom));
		
	/* 
	 * 3. check that number of points is within reason 
	 */
	if( (hdom->npoints < 0) || (hdom->npoints > CKHDMNPTMAX) )
		return(woolz_check_exit(HDMECNT, called_from, (char *)hdom));
		
	/* 
	 * 4. check output coordintates 
	 */
	if( ckcoord( hdom->l, 0, hdom->k, 0) )
		return(woolz_check_exit(HDMECOORD, called_from, (char *)hdom));
	/* 
	 * 5,6. finally check that for the size of this domain there aren't too many pointes 
	 */
	ecount = hdom->hv  - ( (int *)(hdom + 1) );
	
	if (ecount > 0)
		if(wz_db_fp)
			fprintf(wz_db_fp, "CHECK WARNING: %d points skipped\n",ecount);
	return(0);
}

/*
 *
 *	Function:		woolz_check_blist
 *
 *	Purpose:		Check a boundary lists integrity
 *
 *	Description:	Performs just a few checks, to check that the tree
 *					structure of this domain is traversable in both
 *					directions, and then uses the check polygon domain
 *					routine to validate all associated polygons
 *					To test tree structure this routine calls itself
 *					recursively	. To stop itself getting lost in an
 *					erroneous tree, it checks its up route from the
 *					next level down before going to that level
 *					Tests:
 *					1. not now done
 *					2. Type
 *					3. Polygon using ckpdom
 *
 *	Entry:			Pointer to boundary list
 *
 *	Exit:			Error status of first detected error, or zero if ok
 *
 */
int
woolz_check_blist(struct boundlist *blist, char *called_from)
{
	static int blevel = 0;
	register  pstat;

/*----*/if(check_level < 1) return(0);

	if(blist == NULL && !blevel)
		return(woolz_check_exit(BDLNULL, called_from, (char *)blist));
		
/*----*/if(check_level < 2) return(0);
		
	/* 
	 * 1. stack check (not done freemem() removed) MC 8/7/92
	 */

	/* 
	 * At each level check the type and the polygon
	 */
	if( (blist->type != 0) && (blist->type != 1) )
		return(woolz_check_exit(BDLETYPE, called_from, (char *)blist));
	
	if( blist->poly != NULL )
		if ( (pstat = woolz_check_pdom(blist->poly, called_from)) > 0)
			return(woolz_check_exit(pstat, called_from, (char *)blist));		
	if(wz_db_fp)
		blevel--;		/* recursion counter */

	return(0);		/* ok for this level */
}

/*
 *
 *	Function:		woolz_check_tdom
 *
 *	Purpose:		Validates a text OBJECT as much as possible
 *
 *	Description:	Performs consistency checks on both the text domain
 *					structure and the associated text .
 *					1. Output coordinate range
 *					2. String length sensible
 *					3. No non ascii characters contained
 *
 *	Entry:			N.B. This routine takes an OBJECT not the domain
 *
 *	Exit:			Zero if successful, or error status of first error.
 *
 */
int
woolz_check_tdom(struct textobj *tobj, char *called_from)
{
	struct textdomain *tdom;
	register TCHAR *chr;
	register int i,len,errcnt;

/*----*/if(check_level < 1) return(0);

	if(tobj == NULL)
		return(woolz_check_exit(TDMNULL, called_from, (char *)tobj));
		
/*----*/if(check_level < 2) return(0);
	
	tdom = tobj->tdom;
	chr = tobj->text;
	
	/* 1. first check the coordinates look sensible */
	if( ckcoord( tdom->l, 0, tdom->k, 0) )
		return(woolz_check_exit(TDMECOORD, called_from, (char *)tdom));
	
	/* 2. Check length is plausible */
	len = tdom->stringlen;
	if( (len < 0) || (len > CKTDMNSTMAX) )
		return(woolz_check_exit(TDMECNT, called_from, (char *)tdom));
		
/*----*/if(check_level < 3) return(0);

	/* 3. Whizz through the text */
	errcnt = 0;
	for ( i = 0; i < len; i++ )
	{
		if( (*chr < 0x20) || (*chr > 0x7f) ) {
			if(wz_db_fp)
				fprintf(wz_db_fp, "CHECK WARNING: tdom char %x encountered\n",*chr);
				
			if (++errcnt > 4)
				i = len;		/* dont list out a massive load of errors */
		}
		chr++;
	}
	
	if(errcnt > 0)
		return(woolz_check_exit(TDMEASC, called_from, (char *)tdom));
	
	if(wz_db_fp)
		fprintf(wz_db_fp, "CHECK: text object O.K.\n");

	return(0);					/* otherwise alls well */
}

/*
 *
 *	Functions:		ckcoord, ckfcoord
 *
 *	Purpose:		Make sure that coordinates are in a plausible range
 *
 *	Description:	Checks each of the parameters are within the max
 *					likely extent, even for a large rotated object, if
 *					not then object corruption is likely to have
 *					occured	. Integer and float routines included
 *
 *	Entry:			The four coordinate extents
 *
 *	Exit:			ZERO = success.   ONE = error status.
 *
 */
static int
ckcoord(WZCOORD line1, WZCOORD lastln, WZCOORD kol1, WZCOORD lastkl)
{
	if ( (line1  < CKLINEMIN) || (line1  > CKLINEMAX) ||
		 (lastln < CKLINEMIN) || (lastln > CKLINEMAX) ||
		 (kol1   < CKKOLMIN)  || (kol1   > CKKOLMAX)  ||
		 (lastkl < CKKOLMIN)  || (lastkl > CKKOLMAX)  ) {
		if(wz_db_fp)
			fprintf(wz_db_fp, "line1: %d  lastln: %d  kol1: %d  lastkl: %d\n",
					line1, lastln, kol1, lastkl);
		return(1);
	}
	return(0);		/* coordinates o.k. */
}


static int
ckfcoord(float line1, float lastln, float kol1, float lastkl)
{
	if ( (line1  < CKLINEFLMIN) || (line1  > CKLINEMAX) ||
		 (lastln < CKLINEFLMIN) || (lastln > CKLINEMAX) ||
		 (kol1   < CKKOLFLMIN)  || (kol1   > CKKOLMAX)  ||
		 (lastkl < CKKOLFLMIN)  || (lastkl > CKKOLMAX)  ) {
		if(wz_db_fp)
			fprintf(wz_db_fp, "line1: %f  lastln: %f  kol1: %f  lastkl: %f\n",
					line1, lastln, kol1, lastkl);
		return(1);
	}
	return(0);		/* coordinates o.k. */
}

/*
 * W O O L Z _ C H E C K _ D E B U G ---
 *
 */
int
woolz_check_debug(FILE *fp)
{
	wz_db_fp = fp;
}

/*
 * W O O L Z _ C H E C K _ L E V E L ---
 *
 */
int
woolz_check_level(int level)
{
	check_level = level;
	/* WH set the default error handler*/
	if (level > 0)
		set_woolz_exit(woolzexitfn);
}

/* WH for WINDOWS version we do */
/*#ifdef IDONTTHINKWENEEDTHIS */


/*
 * W O O L Z _ E X I T _ F N ---
 * Purpose: 1) Output message to debug file (if debug on)
 *				or, output message to stdout (if debug is off)
 *			2) Determine if the error is fatal by calling woolz_fatal()
 *			3) If the error is fatal, the process will log the message in
 *			   the system error log file (using errlog()), and kill itself with
 *			   signal WOOLZSIG
 *			NOTE: errlog() is not used at present.
 *			4) If the error is non-fatal it returns the value of the error no.
 */ 
int
woolzexitfn(int err_no, char *called_from)
{
	char buf[100];

	sprintf(buf, "Woolz error no. %d [%s] detected, called from %s\n",
		err_no, woolz_err_msg(err_no), called_from);
	if(wz_db_fp)
		fprintf(wz_db_fp, "%s", buf);
	else
		fprintf(stdout, "%s", buf);
	if (woolz_fatal(err_no)) {
#ifdef i386
		kill(getpid(),WOOLZSIG);
#endif
#ifdef WIN32
	/* exit(errno); */
#endif
/*		errorlog(buf);*/ /* Can't call errorlog from here ... yet */
	}
	return(err_no);
}

/*#endif*/

	
/*
 * W O O L Z _ C H E C K _ E X I T ---
 *
 */
int
woolz_check_exit(int err_no, char *mesg, char *ptr)
{
	
	if(wz_db_fp)
		fprintf(wz_db_fp, "Checking pointer 0x%x\n", ptr);
		
	return(woolz_exit(err_no, mesg));
}
