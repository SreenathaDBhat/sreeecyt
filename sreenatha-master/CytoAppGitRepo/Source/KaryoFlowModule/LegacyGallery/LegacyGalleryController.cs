﻿using System;
using System.Linq;
using System.Windows.Threading;
using System.Threading;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Windows.Media;

namespace AI
{

    public class LegacyCellImage : Notifier
    {
        public ImageType Type { get; set; }
        public string Name { get; set; }
        public Guid SlideId { get; set; }

        private ImageSource thumb;
        public ImageSource ThumbnailImage
        {
            get { return thumb; }
            set { thumb = value; Notify("ThumbnailImage"); }
        }
    }


    public class LegacyGalleryController : Notifier
    {
        private List<Karyotyping.Slide> slides;
        private List<Karyotyping.Cell> cells;
        private List<LegacyCellImage> allImages = new List<LegacyCellImage>();
        private ObservableCollection<LegacyCellImage> selImages = new ObservableCollection<LegacyCellImage>();
        private Guid caseId;


        public LegacyGalleryController(Guid caseId)
        {
            this.caseId = caseId;

            using (var db = new Connect.Database())
            {
                LoadCase(db, caseId);
            }
        }

        private void LoadCase(Connect.Database db, Guid caseId)
        {
            using(var lims = Connect.LIMS.New())
            {
                cells = (from c in db.CaseCells
                            where c.caseId == caseId
                         select new Karyotyping.Cell
                            {
                                Id = c.cellId,
                                Name = c.cellName,
                                Counted = c.counted,
                                Numbered = c.analysed,
                                Karyotyped = c.karyotyped,
                                ColorIndex = c.color,
                                ImageGroup = c.imageGroup,
                                SlideId = c.slideId,
                            }).ToList();

                var dbc = db.Cases.Where(c => c.caseId == caseId).First();

                var lc = lims.GetCase(dbc.limsRef);

                slides = (from s in db.Slides
                          where s.caseId == caseId
                          select new AI.Karyotyping.Slide
                          {
                              Id = s.slideId,
                              Name = lims.GetSlide(s.limsRef).Name,
                              Selected = false
                          }).ToList();

                foreach (var c in cells)
                {
                    c.Slide = slides.Where(s => s.Id == c.SlideId).First();
                }

                var first = slides.First();
                if (first != null)
                    first.Selected = true;
            }
        }

        public Case Case { get; set; }

        public IEnumerable<Karyotyping.Cell> Cells
        {
            get { return cells.Where(c => c.Slide.Selected).OrderBy(i => i.Name); }
        }

        public IEnumerable<Karyotyping.Slide> Slides
        {
            get { return slides; }
        }

        public void NotifySlides()
        {
            Notify("Cells");
            Notify("LegacyImages");
        }

        public ObservableCollection<LegacyCellImage> LegacyImages
        {
            get 
            { 
                selImages = new ObservableCollection<LegacyCellImage>();
                var slides = Slides.Where( i => i.Selected == true).OrderBy( o => o.Name);
                foreach (var slide in slides)
                {
                    var images = allImages.Where(s => s.SlideId == slide.Id).OrderBy(o => o.Name);
                    foreach (var image in images)
                        selImages.Add(image);
                }
                return selImages;
            }
        }

        public int IndexOfCurrentImage
        {
            get 
            {
                return (selImages == null) ? -1 : selImages.ToList().IndexOf(selectedImage); 
            }
        }

        public LegacyCellImage NextImage()
        {
            return selImages[IndexOfCurrentImage + 1];
        }

        public bool CanGoNextImage()
        {
            return IndexOfCurrentImage + 1 < selImages.Count;
        }

        public LegacyCellImage PrevImage()
        {
            return selImages[IndexOfCurrentImage - 1];
        }

        public bool CanGoPrevImage()
        {
            return IndexOfCurrentImage - 1 >= 0;
        }

        private LegacyCellImage selectedImage;
        public LegacyCellImage SelectedLegacyImage
        {
            get
            {
                return selectedImage;
            }
            set
            {
                selectedImage = value; 
                Notify("SelectedLegacyImage");
            }
        }

        public static Karyotyping.CellImage[] AiPngsForCell(Connect.Database db, Guid cellImageGroupId, Guid caseId)
        {
            var images = from i in db.ImageFrames
                         where i.groupId == cellImageGroupId && i.imageType == (int)ImageType.AiPng
                         select new Karyotyping.CellImage
                         {
                             Type = (ImageType)i.imageType,
                             FrameId = i.imageFrameId

                         };

            return images.ToArray();
        }

        public static bool CaseHasLegacyImages(Guid caseId)
        {
            var db = new Connect.Database();

            var legacy = from f in db.CaseImageFrames
                              where (f.caseId == caseId && f.imageType == (int)ImageType.AiPng)
                              select f.imageFrameId;

            return legacy.Count() > 0;
        }


/*
                public void LoadThumbs(Dispatcher dispatcher)
                {
                    allImages = new List<LegacyCellImage>();
                    Parallel.ForEach(cells, c =>
                    {
                        var db = new Connect.Database();
                        CellImage [] images = AiPngsForCell(db, c.ImageGroup, Case.Id);

                        foreach (var img in images)
                        {
                            if (img != null)
                            {
                                string path = img.FrameId.ToString() + ".jpg";
                                var bitmap = ImageFromBitmapSource.LoadBitmapImageFromStream(Case.Id, path, db, Connect.CacheOption.DontCache, 0);

                                dispatcher.BeginInvoke((ThreadStart)delegate
                                {
                                    allImages.Add(new LegacyCellImage { Name = c.Name, ThumbnailImage = bitmap, SlideId = c.SlideId });
                                    Notify("LegacyImages");

                                }, DispatcherPriority.Normal);
                            }
                        }
                    });
                }

*/

        public void LoadThumbs(Dispatcher dispatcher)
        {
            var db = new Connect.Database();

            allImages = new List<LegacyCellImage>();

            foreach (var c in cells)
            {
                Karyotyping.CellImage[] images = AiPngsForCell(db, c.ImageGroup, caseId);

                foreach (var img in images)
                {
                    if (img != null)
                    {
                        string path = img.FrameId.ToString() + ".jpg";
                        var bitmap = ImageFromBitmapSource.LoadBitmapImageFromStream(caseId, path, db, Connect.CacheOption.DontCache, 0);
						LegacyCellImage image = new LegacyCellImage() { Name = c.Name, ThumbnailImage = bitmap, SlideId = c.SlideId };

                        dispatcher.BeginInvoke((ThreadStart)delegate
                        {
                            allImages.Add( image);
                            Notify("LegacyImages");
                        }, DispatcherPriority.Normal);
                    }
                }
            }
        }
    }
}
