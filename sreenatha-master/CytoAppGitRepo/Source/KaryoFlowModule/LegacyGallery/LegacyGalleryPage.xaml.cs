﻿using System;
using System.Windows.Controls;
using System.Threading;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows;
using System.Windows.Media;
using System.Linq;
using System.Windows.Controls.Primitives;
using AI.Security;
using System.Collections.Generic;

namespace AI
{
    public partial class LegacyGalleryPage : UserControl
    {
        public static RoutedUICommand NextCell = new RoutedUICommand("Next Cell", "NextCell", typeof(LegacyGalleryPage));
        public static RoutedUICommand PrevCell = new RoutedUICommand("Previous Cell", "PrevCell", typeof(LegacyGalleryPage));
        public static RoutedUICommand ToggleCellDisplay = new RoutedUICommand("Toggle Cell Display", "ToggleCellDisplay", typeof(LegacyGalleryPage));
        public static RoutedUICommand ShowCaseDetails = new RoutedUICommand("Show Case Details", "ShowCaseDetails", typeof(LegacyGalleryPage));

        public static DependencyProperty GridSizeProperty = DependencyProperty.Register("GridSize", typeof(double), typeof(LegacyGalleryPage), new FrameworkPropertyMetadata(200.0));
        public static DependencyProperty ShowImageQualityButtonProperty = DependencyProperty.Register("ShowImageQualityButton", typeof(bool), typeof(LegacyGalleryPage));


        private LegacyGalleryController controller;
        private Guid caseId;

        public LegacyGalleryPage( Guid caseId)
        {
            GridSize = 150;
            this.caseId = caseId;

            controller = new LegacyGalleryController(caseId);
            DataContext = controller;

            InitializeComponent();

            Loaded += (s, e) => 
            {
                ThreadPool.QueueUserWorkItem(delegate
                {
                    controller.LoadThumbs(Dispatcher);
                });

                Keyboard.Focus(this); 

                if (controller.Slides.Count() > 15)
                {
                    embeddedSlideList.Visibility = Visibility.Collapsed;
                    slidesPopupButton.Visibility = Visibility.Visible;
                }
                else
                {
                    embeddedSlideList.Visibility = Visibility.Visible;
                    slidesPopupButton.Visibility = Visibility.Collapsed;
                }
            };

            Unloaded += (s, e) =>
            {
            };
        }

        public double GridSize
        {
            get { return (double)GetValue(GridSizeProperty); }
            set { SetValue(GridSizeProperty, value); }
        }

        public bool ShowImageQualityButton
        {
            get { return (bool)GetValue(ShowImageQualityButtonProperty); }
            set { SetValue(ShowImageQualityButtonProperty, value); }
        }

        private void CanNextCellExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.CanGoNextImage();
        }

        private void NextCellExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            controller.SelectedLegacyImage = controller.NextImage();
            ScrollToTop();
        }

        private void CanPrevCellExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.CanGoPrevImage();
        }

        private void PrevCellExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            controller.SelectedLegacyImage = controller.PrevImage();
            ScrollToTop();
        }

        private void ScrollToTop()
        {
            thumbList.ScrollIntoView(thumbList.SelectedItem);
        }


        private void OnListKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;

                if (ToggleCellDisplay.CanExecute(this, this))
                {
                    ToggleCellDisplay.Execute(this, this);
                }
            }
        }

        private void OnShowPopup(object sender, RoutedEventArgs e)
        {
            ((Popup)((Button)sender).Tag).IsOpen = true;
        }

        private void OnSlideChecked(object sender, RoutedEventArgs e)
        {
            controller.NotifySlides();
            ScrollToTop();
        }

        private void OnSlideUnChecked(object sender, RoutedEventArgs e)
        {
            controller.NotifySlides();
            ScrollToTop();
        }

        private void OnShowSlidesPopup(object sender, RoutedEventArgs e)
        {
            slidesPopup.IsOpen = true;
        }

        private void OnBack(object sender, RoutedEventArgs e)
        {
            UIStack.For(this).Pop(UIStackResult.Cancel);
        }
    }
}
