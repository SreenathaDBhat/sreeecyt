﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.IO;
using System.ComponentModel;
using System.Globalization;
using AI.Connect;
using System.Windows.Media.Imaging;

namespace AI
{
    namespace Karyotyping
    {
        public class Case
        {
            public string Name { get; set; }
            public Guid Id { get; set; }
        }

        public class Slide
        {
            public string Name { get; set; }
            public Guid Id { get; set; }
            public string Comment { get; set; }
            public bool Selected { get; set; }
        }

        public class CellImage : INotifyPropertyChanged
        {
            public ImageType Type { get; set; }
            public string Name { get { return Type.ToString(); } }
            public Guid FrameId { get; set; }

            private ImageSource thumb;
            public ImageSource ThumbnailImage
            {
                get { return thumb; }
                set { thumb = value; Notify("ThumbnailImage"); }
            }


            #region INotifyPropertyChanged Members

            public event PropertyChangedEventHandler PropertyChanged;
            private void Notify(string s)
            {
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(s));
            }

            #endregion
        }

        public class CellColor
        {
            public int Index { get; set; }
            public Brush Brush { get; set; }
            public Brush Border { get; set; }

            public static readonly CellColor[] PossibleColors = new CellColor[]
            {
                new CellColor { Index = 0, Brush = Brushes.LimeGreen, Border = Brushes.Transparent },
                new CellColor { Index = 1, Brush = Brushes.Red, Border = Brushes.Transparent },
                new CellColor { Index = 2, Brush = Brushes.Cyan, Border = Brushes.Transparent },
                new CellColor { Index = 3, Brush = Brushes.Transparent, Border = Brushes.Transparent },
            };

            public static CellColor Green { get { return PossibleColors[0]; } }
            public static CellColor Red { get { return PossibleColors[1]; } }
            public static CellColor Blue { get { return PossibleColors[2]; } }
            public static CellColor White { get { return PossibleColors[3]; } }
        }

        public class Cell : INotifyPropertyChanged
        {
            public string Name { get; set; }
            public Guid Id { get; set; }
            public IEnumerable<CellImage> Images { get; set; }
            public int ColorIndex { get; set; }
            public Guid ImageGroup { get; set; }
            public Guid SlideId { get; set; }
            public Slide Slide { get; set; }

            public CellImage WorkingImage
            {
                get
                {
                    if (Images == null)
                        return null;

                    CellImage rawImage = null;

                    foreach (var img in Images)
                    {
						//if (img.Type == ImageType.Manual)
						//    return img;
						//else if (img.Type == ImageType.PreThresholded)
						//    return img;

                        if (rawImage == null && img.Type == ImageType.Raw)
                            rawImage = img;
                    }

                    return rawImage;
                }
            }

            public CellColor Color
            {
                get { return CellColor.PossibleColors[ColorIndex]; }
                set { ColorIndex = IndexOf(CellColor.PossibleColors, value); Notify("Color"); }
            }

            private bool counted;
            public bool Counted
            {
                get { return counted; }
                set { counted = value; Notify("Counted"); }
            }

            private bool numbered;
            public bool Numbered
            {
                get { return numbered; }
                set { numbered = value; Notify("Numbered"); }
            }
            private bool karyotyped;
            public bool Karyotyped
            {
                get { return karyotyped; }
                set { karyotyped = value; Notify("Karyotyped"); }
            }

            private string result;
            public string Result
            {
                get { return result; }
                set { result = value; Notify("Result"); }
            }

			private bool objectsDetected;
			public bool ObjectsDetected
			{
				get { return objectsDetected; }
				set { objectsDetected = value; Notify("ObjectsDetected"); }
			}

			private int threshold;
			public int Threshold
			{
				get { return threshold; }
				set { threshold = value; Notify("Threshold"); }
			}

			private int histoLow;
			public int HistoLow
			{
				get { return histoLow; }
				set { histoLow = value; Notify("HistoLow"); }
			}

			private int histoHigh;
			public int HistoHigh
			{
				get { return histoHigh; }
				set { histoHigh = value; Notify("HistoHigh"); }
			}

            private ImageSource thumb;
            public ImageSource ThumbnailImage
            {
                get { return thumb; }
                set { thumb = value; Notify("ThumbnailImage"); }
            }

            private int IndexOf(CellColor[] cellColor, CellColor value)
            {
                for (int i = 0; i < cellColor.Length; ++i)
                {
                    if (cellColor[i] == value)
                        return i;
                }

                return -1;
            }

            public CellImage KaryotypeImage
            {
                get { return Images.Where(i => i.Type == ImageType.Karyotype).FirstOrDefault(); }
            }

            public CellImage RawImage
            {
                get { return Images.Where(i => i.Type == ImageType.Raw).FirstOrDefault(); }
            }

            public bool GotFuses
            {
                get { return Images == null ? false : Images.Count(i => i.Type == ImageType.Fuse) > 0; }
            }

            public bool GotMergedImage
            {
                get { return Images == null ? false : Images.Count(c => c.Type == ImageType.Metaphase) > 0; }
            }

            internal void NotifyMergedImage()
            {
                Notify("GotMergedImage");
            }

            internal void NotifyImages()
            {
                Notify("ThumbnailImage");
                Notify("Images");
                Notify("GotFuses");
                Notify("GotMergedImage");
            }

            #region INotifyPropertyChanged Members

            public event PropertyChangedEventHandler PropertyChanged;
            private void Notify(string s)
            {
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(s));
            }

            #endregion

            internal void RemoveManualImages()
            {
                Images = Images.Where(i => i.Type != ImageType.Metaphase);
                Notify("GotMergedImage");
            }

            internal void SetThumbNoNotify(BitmapImage bitmap)
            {
                thumb = bitmap;
            }

            internal ImageFrame LoadImage(Database db, Guid caseId)
            {
                var f = ImageFrameDbLoad.FromDb(db, WorkingImage.FrameId, caseId);
                //f.Thumbnail = thumb as BitmapSource;
                return f;
            }

			public void Update( CellStatus status)
			{
				ObjectsDetected = status.ObjectsDetected;
				Threshold = status.Threshold;
				HistoLow = status.HistoLow;
				HistoHigh = status.HistoHigh;
			}
        }

		public class CellStatus
		{
			public bool ObjectsDetected { get; set; }
			public int Threshold { get; set; }
			public int HistoLow { get; set; }
			public int HistoHigh { get; set; }

			public CellStatus(Cell cell)
			{
				ObjectsDetected = cell.ObjectsDetected;
				Threshold = cell.Threshold;
				HistoLow = cell.HistoLow;
				HistoHigh = cell.HistoHigh;
			}
		}

        public class CellTotals : Notifier
        {
            private IEnumerable<Cell> cells;
            private int reds;
            private int greens;
            private int blues;
            private int whites;
            private int counted;
            private int numbered;
            private int karyotyped;
            private int usedForClearing;
            private int processed;


            public CellTotals(IEnumerable<Cell> cells, Database db, Guid caseId)
            {
                this.cells = cells;
                Refresh(db, caseId);
            }

            public void Refresh(Database db, Guid caseId)
            {
                reds = 0;
                greens = 0;
                blues = 0;
                whites = 0;
                counted = 0;
                numbered = 0;
                karyotyped = 0;
                usedForClearing = 0;
                processed = 0;

                if (cells != null)
                {
                    foreach (var c in cells)
                    {
                        if (c.Counted) counted++;
                        if (c.Numbered) numbered++;
                        if (c.Karyotyped) karyotyped++;
                        if (c.Color.Index == 0) greens++;
                        if (c.Color.Index == 1) reds++;
                        if (c.Color.Index == 2) blues++;
                        if (c.Color.Index == 3) whites++;
                        if (c.Counted || c.Numbered || c.Karyotyped /*|| c.UsedForClearing*/) processed++;
                    }
                }

                var ccIds = (from c in db.CaseClearedChromosomes
                             where c.caseId == caseId
                             select c.clearedChromosomeId).ToList();

                usedForClearing = (from c in db.ClearedChromosomes
                                   where ccIds.Contains(c.clearedChromosomeId)
                                   select c.cellId).Distinct().Count();

                Notify("Reds", "Whites", "Greens", "Blues");
                Notify("Counted", "Numbered", "Karyotyped", "UsedForClearing", "Processed", "GotColors");
            }

            public int Reds
            {
                get { return reds; }
            }
            public int Blues
            {
                get { return blues; }
            }
            public int Greens
            {
                get { return greens; }
            }
            public int Whites
            {
                get { return whites; }
            }
            public int Counted
            {
                get { return counted; }
                set { counted = value; Notify("Counted"); }
            }
            public int Numbered
            {
                get { return numbered; }
                set { numbered = value; Notify("Numbered"); }
            }
            public int Karyotyped
            {
                get { return karyotyped; }
                set { karyotyped = value; Notify("Karyotyped"); }
            }
            public int UsedForClearing
            {
                get { return usedForClearing; }
            }
            public int Processed
            {
                get { return processed; }
            }
            public bool GotColors
            {
                get { return greens > 0 || reds > 0 || blues > 0; }
            }

            public void SetColor(Cell cell, CellColor color)
            {
                if (color == cell.Color)
                    return;

                switch (cell.ColorIndex)
                {
                    case 0: greens--; break;
                    case 1: reds--; break;
                    case 2: blues--; break;
                    case 3: whites--; break;
                }

                cell.Color = color;
                switch(cell.ColorIndex)
                {
                    case 0: greens++; break;
                    case 1: reds++; break;
                    case 2: blues++; break;
                    case 3: whites++; break;
                }

                Notify("Reds", "Greens", "Blues", "Whites", "GotColors");
            }
        }
    }
}
