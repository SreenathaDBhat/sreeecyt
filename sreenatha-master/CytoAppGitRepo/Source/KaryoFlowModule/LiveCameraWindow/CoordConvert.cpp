#include "stdafx.h"
#include "CoordConvert.h"
#include "Conversion.h"

namespace AI
{
	System::String^ CoordConvert::IdealToEnglandFinder(double idealX, double idealY)
	{
		CConversion c;
		c.create_ideal_to_EF();

		char efOutput[64];
		c.convert_idealcoords_to_EF((float)idealX, (float)idealY, efOutput);

		return gcnew System::String(efOutput);
	}
}