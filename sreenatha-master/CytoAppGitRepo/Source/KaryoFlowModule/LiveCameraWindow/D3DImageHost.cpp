#include "stdafx.h"
#include "D3DImageHost.h"
using namespace System::Threading;
using namespace System::Windows::Threading;

LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    return DefWindowProc(hWnd, msg, wParam, lParam);
}

WNDCLASSEX g_wc= { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L, GetModuleHandle(NULL), NULL, NULL, NULL, NULL, L"AI", NULL };



namespace AI
{
	D3DImageHost::D3DImageHost()
	{
		Renderer = nullptr;

		g_pd3dSurface = 0;
		d3dImage = gcnew System::Windows::Interop::D3DImage;

		d3dImage->IsFrontBufferAvailableChanged += gcnew System::Windows::DependencyPropertyChangedEventHandler(this, &D3DImageHost::OnIsFrontBufferAvailableChanged);

		image = gcnew System::Windows::Controls::Image;
		image->Source = d3dImage;

		Child = image;

		SizeChanged += gcnew System::Windows::SizeChangedEventHandler(this, &D3DImageHost::OnSizeChanged);

		//System::Windows::Media::CompositionTarget::Rendering += gcnew System::EventHandler(this, &D3DImageHost::CompositionTargetRendering);
	}

	void D3DImageHost::CompositionTargetRendering(System::Object^ sender, System::EventArgs^ e)
	{
		Render();
	}

	void D3DImageHost::OnRendererChanged(System::Windows::DependencyObject^ sender, System::Windows::DependencyPropertyChangedEventArgs e)
	{
		D3DImageHost^ t = (D3DImageHost^)sender;

		if(t->g_pd3dDevice == nullptr)
			t->InitializeScene();

		if(e.OldValue != nullptr)
		{
			ID3DRenderer^ oldRenderer = (ID3DRenderer^)(e.OldValue);
			oldRenderer->ReleaseScene();
		}
		if(e.NewValue != nullptr)
		{
			ID3DRenderer^ newRenderer = (ID3DRenderer^)(e.NewValue);
			newRenderer->InitializeScene(System::IntPtr(t->g_pd3dDevice));
			newRenderer->SetHost(t);
		}

		t->Render();
	}

	System::Windows::Controls::Image^ D3DImageHost::GetWpfImage()
	{
		return (System::Windows::Controls::Image^)Child;
	}

	void D3DImageHost::InitializeScene()
	{
		D3DPRESENT_PARAMETERS d3dpp;
		ZeroMemory(&d3dpp, sizeof(d3dpp));
		d3dpp.Windowed = TRUE;
		d3dpp.BackBufferHeight = 1;
		d3dpp.BackBufferWidth = 1;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		d3dpp.BackBufferFormat = D3DFMT_A8R8G8B8;

		RegisterClassEx(&g_wc);
		HWND hWnd = CreateWindow(L"AI", L"AI", WS_OVERLAPPEDWINDOW, 0, 0, 0, 0, NULL, NULL, g_wc.hInstance, NULL);
		window = System::IntPtr(hWnd);

		InitializeD3D(hWnd, d3dpp);

		g_pd3dDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
		g_pd3dDevice->SetRenderState(D3DRS_LIGHTING, 0);
		g_pd3dDevice->SetRenderState(D3DRS_ZENABLE, 0);
	}

	void D3DImageHost::ResizeRenderTarget(int width, int height)
	{
		if(g_pd3dSurface)
			g_pd3dSurface->Release();

		LPDIRECT3DSURFACE9 tmpsurf;
		g_pd3dDevice->CreateRenderTarget(width, height, 
			D3DFMT_A8R8G8B8, D3DMULTISAMPLE_NONE, 0, 
			1,
			&tmpsurf, NULL);
		g_pd3dSurface = tmpsurf;
	}

	void D3DImageHost::RenderScene()
	{
	}

	void D3DImageHost::ReleaseScene()
	{
		if(Renderer != nullptr)
			Renderer->ReleaseScene();

		if(g_pd3dDevice == 0)
			return;

		g_pd3dDevice->Release();
		g_pD3D->Release();
		g_pd3dSurface->Release();

		g_pd3dDevice = 0;
		g_pD3D = 0;
		g_pd3dSurface = 0;

		DestroyWindow((HWND)(void*)window);
	}



	void D3DImageHost::InitializeD3D(HWND hWnd, D3DPRESENT_PARAMETERS d3dpp)
	{
		if (NULL == (g_pD3D = Direct3DCreate9(D3D_SDK_VERSION)))
		{
			return;
		}

		DWORD dwVertexProcessing = GetVertexProcessingCaps();

		LPDIRECT3DDEVICE9 tmpdevice;
		g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
			dwVertexProcessing | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE,
			&d3dpp, &tmpdevice);
		g_pd3dDevice = tmpdevice;
	}

	DWORD D3DImageHost::GetVertexProcessingCaps()
	{
		D3DCAPS9 caps;
		DWORD dwVertexProcessing = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
		if (SUCCEEDED(g_pD3D->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &caps)))
		{
			if ((caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) == D3DDEVCAPS_HWTRANSFORMANDLIGHT)
			{
				dwVertexProcessing = D3DCREATE_HARDWARE_VERTEXPROCESSING;
			}
		}
		return dwVertexProcessing;
	}

	void D3DImageHost::OnRendering(System::Object^ sender, System::EventArgs^ e)
    {
        Render();
    }



	void D3DImageHost::RenderOnUiThread()
	{
		if (d3dImage != nullptr && d3dImage->IsFrontBufferAvailable && Renderer != nullptr)
		{
			System::Windows::Int32Rect r = System::Windows::Int32Rect(0, 0, (int)ActualWidth, (int)ActualHeight);
			if(r.Width > 0 && r.Height > 0)
			{
				g_pd3dDevice->SetRenderTarget(0, g_pd3dSurface);
				d3dImage->Lock();
				d3dImage->SetBackBuffer(System::Windows::Interop::D3DResourceType::IDirect3DSurface9, System::IntPtr(g_pd3dSurface));
				Renderer->RenderScene(System::IntPtr(g_pd3dDevice), (int)ActualWidth, (int)ActualHeight);
				d3dImage->AddDirtyRect(r);
				d3dImage->Unlock();
			}
		}
	}

	void D3DImageHost::Render()
	{
		if(Thread::CurrentThread == Dispatcher->Thread)
			RenderOnUiThread();
		else
			Dispatcher->Invoke(DispatcherPriority::Normal, gcnew ThreadStart(this, &D3DImageHost::RenderOnUiThread));
	}

	void D3DImageHost::OnSizeChanged(System::Object^ o, System::Windows::SizeChangedEventArgs^ e)
	{
		ResizeRenderTarget((int)e->NewSize.Width, (int)e->NewSize.Height);
		Render();
	}

	void D3DImageHost::OnIsFrontBufferAvailableChanged(System::Object ^o, System::Windows::DependencyPropertyChangedEventArgs e)
    {
		bool available = (bool)e.NewValue;
		
		if(available)
		{
			ReleaseScene();
			InitializeScene();

			ResizeRenderTarget((int)ActualWidth, (int)ActualHeight);
			if(Renderer != nullptr)
				Renderer->InitializeScene(System::IntPtr(g_pd3dDevice));

			Render();
		}
		else
		{
			if(Renderer != nullptr)
				Renderer->ReleaseScene();

			ReleaseScene();
		}        
    }
}