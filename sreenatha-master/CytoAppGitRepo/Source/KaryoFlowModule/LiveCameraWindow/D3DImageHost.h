#pragma once

namespace AI
{
	public interface class ID3DRenderer
	{
		void InitializeScene(System::IntPtr d3dDevice);
		void RenderScene(System::IntPtr d3dDevice, int screenWidth, int screenHeight);
		void ReleaseScene();
		void SetHost(System::Object^ host);
	};


	public ref class D3DImageHost : System::Windows::Controls::Decorator
	{
	private:
		static void OnRendererChanged(System::Windows::DependencyObject^ sender, System::Windows::DependencyPropertyChangedEventArgs e);
		static System::Windows::DependencyProperty^ RendererProperty = System::Windows::DependencyProperty::Register(
																			L"Renderer", 
																			ID3DRenderer::typeid, 
																			D3DImageHost::typeid,
																			gcnew System::Windows::FrameworkPropertyMetadata(gcnew System::Windows::PropertyChangedCallback(&OnRendererChanged)));
		
		void RenderOnUiThread();

	public:
		D3DImageHost();

		property ID3DRenderer^ Renderer
		{
			ID3DRenderer^ get()
			{
				return (ID3DRenderer^)GetValue(RendererProperty);
			}
			void set(ID3DRenderer^ value)
			{
				SetValue(RendererProperty, value);
			}
		}

	private:
		void InitializeScene();
		void RenderScene();
		void ReleaseScene();
		void InitializeD3D(HWND hWnd, D3DPRESENT_PARAMETERS d3dpp);
		DWORD GetVertexProcessingCaps();
		void ResizeRenderTarget(int width, int height);
		void OnSizeChanged(System::Object^ sender, System::Windows::SizeChangedEventArgs^ e);
		void CompositionTargetRendering(System::Object^ sender, System::EventArgs^ e);
		System::IntPtr window;
	
	public: 
		void OnRendering(System::Object^ sender, System::EventArgs^ e);
		void OnIsFrontBufferAvailableChanged(System::Object ^o ,System::Windows::DependencyPropertyChangedEventArgs e);
		void Render();

		System::Windows::Controls::Image^ GetWpfImage();
	
	private:

		//typedef HRESULT (WINAPI *DIRECT3DCREATE9EX)(UINT, IDirect3D9Ex**);

		//DIRECT3DCREATE9EX       g_pfnCreate9Ex;
		//bool                    g_is9Ex;
		LPDIRECT3D9             g_pD3D;
		//LPDIRECT3D9EX           g_pD3DEx;
		LPDIRECT3DDEVICE9       g_pd3dDevice;
		//LPDIRECT3DDEVICE9EX     g_pd3dDeviceEx;
		LPDIRECT3DSURFACE9      g_pd3dSurface;

		System::Windows::Interop::D3DImage^ d3dImage;
		System::Windows::Controls::Image^ image;
	};
}