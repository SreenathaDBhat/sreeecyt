#include "stdafx.h"
#include "LiveCameraWindow.h"
#include <vcclr.h>

using namespace System::Collections::Generic;
using namespace System::IO;
using namespace System::Runtime::InteropServices;
using namespace System::Reflection;

unsigned short* potetialGhost = 0;
gcroot<AI::LiveCameraWindow^> globalLive;

struct Vertex
{
    float x, y, z;
    float tu, tv;
};
#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ|D3DFVF_TEX1)


namespace AI
{
	LiveCameraWindow::LiveCameraWindow()
	{
		globalLive = this;
		lastHistUpdate = DateTime::MinValue;
	}

	void LiveCameraWindow::SetHost(Object^ host)
	{
		this->host = (D3DImageHost^)host;
	}

	void LiveCameraWindow::InitializeScene(System::IntPtr d3dDevicePtr)
	{
		LPDIRECT3DDEVICE9 d3dDevice = (LPDIRECT3DDEVICE9)d3dDevicePtr.ToPointer();
		d3d = d3dDevice;

		Vertex quadVertices[] =
		{
			{-796,	596, 0.0f,  0.0f,0.0f },
			{796,	596, 0.0f,  1.0f,0.0f },
			{-796,	-596, 0.0f,  0.0f,1.0f },
			{796,	-596, 0.0f,  1.0f,1.0f }
		};

		LPDIRECT3DVERTEXBUFFER9 tmpvb;
		d3dDevice->CreateVertexBuffer(6*sizeof(Vertex), 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT, &tmpvb, NULL);
		g_pVB = tmpvb;

		// fill vertex buffer
		VOID* pVertices;
		g_pVB->Lock(0, sizeof(quadVertices), (void**)&pVertices, 0);
		memcpy(pVertices, quadVertices, sizeof(quadVertices));
		g_pVB->Unlock();

		HRESULT hr;
		LPD3DXBUFFER pBufferErrors = NULL;

		//
		// *** TODO *** load the fx file from embedd resources???
		//
		LPD3DXEFFECT tempEffect;
#ifdef localFolder
		System::String^ folder = System::Reflection::Assembly::GetExecutingAssembly()->Location;
		folder = System::IO::Path::GetDirectoryName(folder);
		folder = System::IO::Path::Combine(folder, L"fx.fx");
		System::IntPtr ptr = System::Runtime::InteropServices::Marshal::StringToHGlobalUni(folder);

		hr = D3DXCreateEffectFromFile( d3dDevice, 
									   (LPCWSTR)ptr.ToPointer(),
									   NULL, 
									   NULL, 
									   0, 
									   NULL, 
									   &tempEffect, 
									   &pBufferErrors );

		System::Runtime::InteropServices::Marshal::FreeHGlobal(ptr);
#else
		Stream ^ stream = Assembly::GetExecutingAssembly()->GetManifestResourceStream("fx.fx");
		System::String^ fx = nullptr;

		if (stream != nullptr)
		{
			StreamReader ^ sr = gcnew StreamReader(stream);
			fx = sr->ReadToEnd();
			stream->Close();
		}
		
		char* fxs = (char*)(void*)Marshal::StringToHGlobalAnsi(fx);
		hr = D3DXCreateEffect(d3dDevice, (void*)fxs, strlen(fxs), NULL, NULL, 0, NULL, &tempEffect, &pBufferErrors);

		Marshal::FreeHGlobal((IntPtr)fxs);
#endif
		effect = tempEffect;
	}

	void LiveCameraWindow::CreateTextures(int width, int height)
	{
		LPDIRECT3DTEXTURE9 imageTexTmp = 0;
		LPDIRECT3DSURFACE9 imageSurfaceTmp = 0;
		d3d->CreateTexture(width, height, 1, D3DUSAGE_RENDERTARGET, D3DFMT_G16R16, D3DPOOL_DEFAULT, &imageTexTmp, 0);		
		imageTexTmp->GetSurfaceLevel(0, &imageSurfaceTmp);
		imageTexture = imageTexTmp;
		imageSurface = imageSurfaceTmp;

		d3d->CreateTexture(width, height, 1, D3DUSAGE_RENDERTARGET, D3DFMT_G16R16, D3DPOOL_DEFAULT, &imageTexTmp, 0);		
		imageTexTmp->GetSurfaceLevel(0, &imageSurfaceTmp);
		ghostTexture = imageTexTmp;
		ghostSurface = imageSurfaceTmp;

		LPDIRECT3DTEXTURE9 swapTextureTmp = 0;
		LPDIRECT3DSURFACE9 swapSurfaceTmp = 0;
		d3d->CreateTexture(width, height, 1, D3DUSAGE_DYNAMIC, D3DFMT_G16R16, D3DPOOL_SYSTEMMEM, &swapTextureTmp, 0);
		swapTextureTmp->GetSurfaceLevel(0, &swapSurfaceTmp);
		swapSurface = swapSurfaceTmp;
		swapTexture = swapTextureTmp;

		d3d->CreateTexture(width, height, 1, D3DUSAGE_DYNAMIC, D3DFMT_G16R16, D3DPOOL_SYSTEMMEM, &swapTextureTmp, 0);
		swapTextureTmp->GetSurfaceLevel(0, &swapSurfaceTmp);
		ghostSwapSurface = swapSurfaceTmp;
		ghostSwapTexture = swapTextureTmp;
	}

	#pragma warning(disable:4793)
	void Matrixes(LPDIRECT3DDEVICE9 g_pd3dDevice, LPD3DXEFFECT effect, LPDIRECT3DTEXTURE9 imageTexture, LPDIRECT3DTEXTURE9 ghostTexture, float bppFactor)
	{
		// Set up rotation matrix
		D3DXMATRIXA16 matWorld;
		D3DXMatrixIdentity(&matWorld);
		
		// Set up view matrix
		D3DXVECTOR3 vEyePt(0.0f, 0.0f,-4.0f);
		D3DXVECTOR3 vLookatPt(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 vUpVec(0.0f, 1.0f, 0.0f);
		D3DXMATRIXA16 matView;
		D3DXMatrixLookAtLH(&matView, &vEyePt, &vLookatPt, &vUpVec);

		// Set up projection matrix
		D3DXMATRIXA16 matProj;
		//D3DXMatrixPerspectiveFovLH(&matProj, D3DX_PI/4, 1.0f, 1.0f, 100.0f);
		D3DXMatrixOrthoLH(&matProj, 1592, 1192, 0, 100);


		// Send worldViewProj to effect
		D3DXMATRIX worldViewProjection = matWorld * matView * matProj;
		D3DXMatrixTranspose(&worldViewProjection, &worldViewProjection );
		effect->SetMatrix("worldViewProj", &worldViewProjection);
		effect->SetTexture("liveImage", imageTexture);
		effect->SetTexture("ghostImage", ghostTexture);

		D3DXHANDLE handle = effect->GetParameterByName(0, "bppFactor");
		effect->SetFloat(handle, bppFactor);
	}



	// lame. but temp.
	LPDIRECT3DSURFACE9 gimageSurface;
	LPDIRECT3DSURFACE9 gswapSurface;
	LPDIRECT3DDEVICE9 gd3d;
	gcroot<D3DImageHost^> d3dImage;
	bool gPaused;

	void LiveCameraWindow::RenderScene(IntPtr d3dDevicePtr, int screenWidth, int screenHeight)
	{
		LPDIRECT3DDEVICE9 d3dDevice = (LPDIRECT3DDEVICE9)d3dDevicePtr.ToPointer();
		d3d = d3dDevice;
		gPaused = paused;

		gimageSurface = imageSurface;
		gswapSurface = swapSurface;
		gd3d = d3d;
		d3dImage = host;

		// render the scene
		d3dDevice->BeginScene();

		d3dDevice->Clear(0, NULL, /*D3DCLEAR_TARGET |*/  D3DCLEAR_ZBUFFER, D3DCOLOR_ARGB(255, 0, 0, 0), 1.0f, 0);
		effect->SetTechnique( "Technique0" );

		Matrixes(d3dDevice, effect, imageTexture, ghostTexture, 64);//(float)(65565.0 / pow(2.0, 8)));

		UINT uPasses;
		effect->Begin( &uPasses, 0 );
    
		for( UINT uPass = 0; uPass < uPasses; ++uPass )
		{
			effect->BeginPass( uPass );

			d3dDevice->SetStreamSource(0, g_pVB, 0, sizeof(Vertex));
			d3dDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
			d3dDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

			effect->EndPass();
		}

		 effect->End();

		// end the scene
		d3dDevice->EndScene();
	}

	void LiveCameraWindow::ReleaseScene()
	{
		g_pVB->Release();
	}

	void LiveCameraWindow::UpdateHistogram(unsigned short* src, unsigned short* srcEnd)
	{
		DateTime now = DateTime::Now;
		TimeSpan time = lastHistUpdate - now;
		if(time.TotalSeconds < 0.5)
			return;

		lastHistUpdate = now;

		unsigned short* s = src;

		pin_ptr<int> hist = &histogram[0];
		int n = histogram->Length;
		memset(hist, 0, sizeof(int) * n);

		while(s != srcEnd)
		{
			hist[*s++]++;
		}

		array<int>^ dup = gcnew array<int>(n);
		pin_ptr<int> newHist = &dup[0];
		memcpy(newHist, hist, sizeof(int)*n);
		HistogramChanged(this, gcnew HistogramChangedEventArgs(dup));
	}

	void LiveHook(unsigned short* data, int bpp, int w, int h)
	{
		if(!gimageSurface)
			return;

		if(gPaused)
		{
			D3DLOCKED_RECT lock;
			gswapSurface->LockRect(&lock, 0, D3DLOCK_DISCARD);
			
			int len = w * h;
			memset(lock.pBits, 0, w * h * sizeof(unsigned short));

			gswapSurface->UnlockRect();

			gd3d->UpdateSurface(gswapSurface, 0, gimageSurface, 0);
		}
		else
		{
			D3DLOCKED_RECT lock;
			if(gswapSurface->LockRect(&lock, 0, D3DLOCK_DISCARD) != 0)
				return;
			
			int len = w * h;
			unsigned short* dest = (unsigned short*)lock.pBits;
			unsigned short* src = data;
			unsigned short* srcEnd = src + (w * h);

			while(src != srcEnd)
			{
				*dest++ = *src++;
				*dest++ = 0;
			}

			//globalLive->UpdateHistogram(data, srcEnd);

			if(potetialGhost == 0)
				potetialGhost = new unsigned short[w * h];

			memcpy(potetialGhost, data, w * h * sizeof(unsigned short));

			gswapSurface->UnlockRect();

			gd3d->UpdateSurface(gswapSurface, 0, gimageSurface, 0);
		}

		d3dImage->Render();
	}

	void LiveCameraWindow::UseGhost()
	{
		D3DLOCKED_RECT lock;
		LPDIRECT3DSURFACE9 swap = ghostSwapSurface;
		swap->LockRect(&lock, 0, D3DLOCK_DISCARD);
		
		D3DSURFACE_DESC d;
		int len = swap->GetDesc(&d);

		unsigned short* dest = (unsigned short*)lock.pBits;
		unsigned short* src = (unsigned short*)potetialGhost;
		unsigned short* srcEnd = src + (d.Width * d.Height);

		while(src != srcEnd)
		{
			*dest++ = *src++;
			*dest++ = 0;
		}

		swap->UnlockRect();

		d3d->UpdateSurface(swap, 0, ghostSurface, 0);
	}

	void LiveCameraWindow::ClearGhost()
	{
		D3DLOCKED_RECT lock;
		LPDIRECT3DSURFACE9 swap = ghostSwapSurface;
		swap->LockRect(&lock, 0, D3DLOCK_DISCARD);

		D3DSURFACE_DESC d;
		int len = swap->GetDesc(&d);
		
		memset(lock.pBits, 0, sizeof(unsigned short) * d.Width * d.Height * 2);

		swap->UnlockRect();
		d3d->UpdateSurface(swap, 0, ghostSurface, 0);
	}

	void LiveCameraWindow::SetGhostColor(Color^ ghostColor)
	{
		D3DXHANDLE handle = effect->GetParameterByName(0, "ghostColor");
		float col[] = 
		{ 
			(float)ghostColor->R / 255, 
			(float)ghostColor->G / 255, 
			(float)ghostColor->B / 255, 
			(float)ghostColor->A / 255 
		};

		effect->SetFloatArray(handle, col, 4);
	}

	void LiveCameraWindow::HookLiveImage(Camera^ camera)
	{
		if(imageTexture != 0)
		{
			imageSurface->Release();
			imageTexture->Release();
			ghostSurface->Release();
			ghostTexture->Release();
			swapSurface->Release();
			swapTexture->Release();
		}

		if(camera->ImageWidth == 0 || camera->ImageHeight == 0)
			return;

		CreateTextures(camera->ImageWidth, camera->ImageHeight);
		ClearGhost();

		camera->HookLiveImage((int)LiveHook);

		cameraBitDepth = camera->ImageBitsPerPixel;
		histogram = gcnew array<int>((int)pow(2.0, cameraBitDepth));
	}
}