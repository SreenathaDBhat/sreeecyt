#pragma once

using namespace System;
using namespace System::Windows::Media;

#include "D3DImageHost.h"

namespace AI
{
	public ref class HistogramChangedEventArgs : public EventArgs
	{
	private:
		array<int>^ bins;

	public:
		HistogramChangedEventArgs(array<int>^ bins)
		{
			this->bins = bins;
		}

		property array<int>^ Bins
		{
			array<int>^ get()
			{
				return bins;
			}
		}
	};

	public ref class LiveCameraWindow : public ID3DRenderer
	{
	private:
		LPDIRECT3DDEVICE9 d3d;
		LPDIRECT3DVERTEXBUFFER9 g_pVB;

		LPDIRECT3DTEXTURE9 imageTexture;
		LPDIRECT3DSURFACE9 imageSurface;
		LPDIRECT3DTEXTURE9 ghostTexture;
		LPDIRECT3DSURFACE9 ghostSurface;
		LPDIRECT3DTEXTURE9 swapTexture;
		LPDIRECT3DSURFACE9 swapSurface;
		LPDIRECT3DTEXTURE9 ghostSwapTexture;
		LPDIRECT3DSURFACE9 ghostSwapSurface;

		LPD3DXEFFECT effect;
		D3DImageHost^ host;
		bool paused;
		array<int>^ histogram;
		int cameraBitDepth;
		DateTime lastHistUpdate;

		void CreateTextures(int width, int height);

	public:
		LiveCameraWindow();

		event EventHandler<HistogramChangedEventArgs^>^ HistogramChanged;

		virtual void InitializeScene(IntPtr d3dDevice);
		virtual void RenderScene(IntPtr d3dDevicePtr, int screenWidth, int screenHeight);
		virtual void ReleaseScene();
		virtual void SetHost(Object^ host);

		void HookLiveImage(AI::Camera^ cam);

		void UseGhost();
		void ClearGhost();
		void SetGhostColor(Color^ ghostColor);

		void UpdateHistogram(unsigned short* src, unsigned short* srcEnd);

		property bool Paused
		{
			bool get()
			{
				return paused;
			}
			void set(bool p)
			{
				paused = p;
			}
		}
	};
}
