float4x4 worldViewProj : WorldViewProjection;
float bppFactor = 1 / 0.015625f; // data is really n bit, not neccessarily 16, so max intensity coming could will be < 1.
texture liveImage;
texture ghostImage;
float4 ghostColor = float4(1,1,1,1);

sampler liveImageSampler = sampler_state
{
	Texture   = (liveImage);
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

sampler ghostImageSampler = sampler_state
{
	Texture   = (ghostImage);
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};


struct VS_INPUT
{
    float3 position	: POSITION;
	float2 texture0 : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4 hposition : POSITION;
	float2 texture0  : TEXCOORD0;
};

struct PS_OUTPUT
{
	float4 color : COLOR;
};


VS_OUTPUT myvs( VS_INPUT IN )
{
    VS_OUTPUT OUT;

	OUT.hposition = mul( worldViewProj, float4(IN.position, 1) );
	OUT.texture0 = 1-IN.texture0;
	//OUT.texture0 = IN.texture0;

	return OUT;
}

PS_OUTPUT myps( VS_OUTPUT IN )
{
    PS_OUTPUT OUT;
    
    float4 c = tex2D( liveImageSampler, IN.texture0 );
	OUT.color = c.r * bppFactor;
	
	float4 g = tex2D( ghostImageSampler, IN.texture0 );
	OUT.color += (g.r * bppFactor) * ghostColor;
	
	OUT.color.a = 1;
    return OUT;
}

//-----------------------------------------------------------------------------
// Simple Effect (1 technique with 1 pass)
//-----------------------------------------------------------------------------

technique Technique0
{
    pass Pass0
    {
		Lighting = FALSE;

		Sampler[0] = (liveImageSampler);
		Sampler[0] = (ghostImageSampler);

		VertexShader = compile vs_2_0 myvs();
		PixelShader  = compile ps_2_0 myps();
    }
}

