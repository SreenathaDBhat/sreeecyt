#pragma once
using namespace System;
using namespace System::Windows;

namespace AI
{

	public static class Interop
	{
	public:
		static void WriteToD3DSurface(array<ushort>^ pixels, int w, int h, IntPtr surfacePtr)
		{
			D3DLOCKED_RECT lock;
			LPDIRECT3DSURFACE9 surface = (LPDIRECT3DSURFACE9)surfacePtr.ToPointer();
			if(surface->LockRect(&lock, 0, D3DLOCK_DISCARD) != 0)
				return;
			
			int len = w * h;
			unsigned short* dest = (unsigned short*)lock.pBits;
			unsigned short* src = data;
			unsigned short* srcEnd = src + (w * h);

			while(src != srcEnd)
			{
				*dest++ = *src++;
				*dest++ = 0;
			}

			surface->UnlockRect();
		}
	};
}