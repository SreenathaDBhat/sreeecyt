#include "stdafx.h"
#include "LiveImageRenderer.h"
using namespace System::IO;
using namespace System::Reflection;
using namespace System::Runtime::InteropServices;
using namespace System::Threading;

namespace
{
#define SAFE_RELEASE(x) if(x) { x->Release(); x = 0; }
LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    return DefWindowProc(hWnd, msg, wParam, lParam);
}
WNDCLASSEX g_wc= { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L, GetModuleHandle(NULL), NULL, NULL, NULL, NULL, L"AI", NULL };

struct Vertex
{
    float x, y, z;
    float tu, tv;
};
#define VertexFormat (D3DFVF_XYZ|D3DFVF_TEX1)

#pragma warning(disable:4793)
HRESULT Matrixes(LPD3DXEFFECT effect)
{
	D3DXMATRIXA16 matWorld;
	D3DXMatrixIdentity(&matWorld);

	D3DXMATRIXA16 matView;
	D3DXVECTOR3 vEyePt(0.0f, 0.0f,-4.0f);
	D3DXVECTOR3 vLookatPt(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 vUpVec(0.0f, 1.0f, 0.0f);
	D3DXMatrixLookAtLH(&matView, &vEyePt, &vLookatPt, &vUpVec);

	D3DXMATRIXA16 matProj;
	D3DXMatrixOrthoLH(&matProj, (FLOAT)1, (FLOAT)1, 0, 100);

	D3DXMATRIX worldViewProjection = matWorld * matView * matProj;
	D3DXMatrixTranspose(&worldViewProjection, &worldViewProjection);

	return effect->SetMatrix("worldViewProj", &worldViewProjection);
}	
#pragma warning(default:4793)

void SetWorldViewProjectionMatrix(IntPtr effect)
{
	Matrixes((LPD3DXEFFECT)effect.ToPointer());
}
}


namespace AI
{
	LiveImageRenderer::LiveImageRenderer(System::Windows::Interop::D3DImage^ d3dImage, int surfaceWidth, int surfaceHeight, int surfaceBpp)
	{
		uiThread = System::Windows::Threading::Dispatcher::CurrentDispatcher;
		this->surfaceWidth = surfaceWidth;
		this->surfaceHeight = surfaceHeight;
		this->surfaceBpp = surfaceBpp;
		this->d3dImage = d3dImage;
		this->gamma = 1;

		d3dImage->IsFrontBufferAvailableChanged += gcnew DependencyPropertyChangedEventHandler(this, &LiveImageRenderer::OnIsFrontBufferAvailableChanged);
		
		d3d = 0;
		device = 0;
		vertexBuffer = 0;
		renderTexture = 0;
		renderSurface = 0;
		swapTexture = 0;
		swapSurface = 0;
		sourceTexture = 0;
		sourceSurface = 0;
		effect = 0;
		window = IntPtr::Zero;

		if(d3dImage->IsFrontBufferAvailable)
		{
			InitScene();
		}
	}

	LiveImageRenderer::~LiveImageRenderer()
	{
		ReleaseScene();
	}

	void LiveImageRenderer::OnIsFrontBufferAvailableChanged(Object ^sender, DependencyPropertyChangedEventArgs e)
    {
		bool available = (bool)e.NewValue;

		ReleaseScene();

		if(available)
		{
			InitScene();
		}
	}

	void LiveImageRenderer::SetGamma(double ng)
	{
		gamma = ng;
		if(effect != nullptr)
			effect->SetFloat("gamma", (FLOAT)ng);
	}

	void LiveImageRenderer::InitScene()
	{
		D3DPRESENT_PARAMETERS d3dpp;
		ZeroMemory(&d3dpp, sizeof(d3dpp));
		d3dpp.Windowed = TRUE;
		d3dpp.BackBufferHeight = 1;
		d3dpp.BackBufferWidth = 1;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		d3dpp.BackBufferFormat = D3DFMT_A8R8G8B8;

		RegisterClassEx(&g_wc);
		HWND hWnd = CreateWindow(L"AI", L"AI", WS_OVERLAPPEDWINDOW, 0, 0, 0, 0, NULL, NULL, g_wc.hInstance, NULL);
		window = System::IntPtr(hWnd);

		HRESULT hr;

		d3d = Direct3DCreate9(D3D_SDK_VERSION);
		if(d3d == 0)
			return;

		D3DCAPS9 caps;
		DWORD dwVertexProcessing = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
		d3d->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &caps);
		if ((caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) == D3DDEVCAPS_HWTRANSFORMANDLIGHT)
		{
			dwVertexProcessing = D3DCREATE_HARDWARE_VERTEXPROCESSING;
		}

		LPDIRECT3DDEVICE9 tmpdevice;
		hr = d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, dwVertexProcessing | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE, &d3dpp, &tmpdevice);
		device = tmpdevice;

		if(device == 0)
		{
			ReleaseScene();
			return;
		}

		hr = device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
		hr = device->SetRenderState(D3DRS_LIGHTING, 0);
		hr = device->SetRenderState(D3DRS_ZENABLE, 0);



		Vertex quadVertices[] =
		{
			{-0.5f,	 0.5f, 0.0f,  0.0f,0.0f },
			{ 0.5f,	 0.5f, 0.0f,  1.0f,0.0f },
			{-0.5f,	-0.5f, 0.0f,  0.0f,1.0f },
			{ 0.5f,	-0.5f, 0.0f,  1.0f,1.0f }
		};

		LPDIRECT3DVERTEXBUFFER9 tmpvb;
		hr = device->CreateVertexBuffer(6*sizeof(Vertex), 0, VertexFormat, D3DPOOL_DEFAULT, &tmpvb, NULL);
		vertexBuffer = tmpvb;

		// fill vertex buffer
		VOID* pVertices;
		vertexBuffer->Lock(0, sizeof(quadVertices), (void**)&pVertices, 0);
		memcpy(pVertices, quadVertices, sizeof(quadVertices));
		vertexBuffer->Unlock();

		Stream^ stream = Assembly::GetExecutingAssembly()->GetManifestResourceStream("liveImage.fx");
		StreamReader ^ sr = gcnew StreamReader(stream);
		String^ fx = sr->ReadToEnd();
		stream->Close();
		
		char* fxs = (char*)(void*)Marshal::StringToHGlobalAnsi(fx);
		LPD3DXEFFECT tempEffect;
		LPD3DXBUFFER pBufferErrors;
		hr = D3DXCreateEffect(device, (void*)fxs, strlen(fxs), NULL, NULL, 0, NULL, &tempEffect, &pBufferErrors);
		Marshal::FreeHGlobal((IntPtr)fxs);
		effect = tempEffect;
		SetWorldViewProjectionMatrix(IntPtr(effect));

		hr = effect->SetFloat("bitsPerPixelMultiplier", (FLOAT)(65536 / (Math::Pow(2.0, surfaceBpp))));
		hr = effect->SetTechnique("Default");

		LPDIRECT3DTEXTURE9 imageTexTmp = 0;
		LPDIRECT3DSURFACE9 imageSurfaceTmp = 0;
		device->CreateTexture(surfaceWidth, surfaceHeight, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &imageTexTmp, 0);
		imageTexTmp->GetSurfaceLevel(0, &imageSurfaceTmp);
		renderTexture = imageTexTmp;
		renderSurface = imageSurfaceTmp;

		imageTexTmp = 0;
		imageSurfaceTmp = 0;
		device->CreateTexture(surfaceWidth, surfaceHeight, 1, D3DUSAGE_DYNAMIC, D3DFMT_G16R16, D3DPOOL_SYSTEMMEM, &imageTexTmp, 0);
		imageTexTmp->GetSurfaceLevel(0, &imageSurfaceTmp);
		swapSurface = imageSurfaceTmp;
		swapTexture = imageTexTmp;

		imageTexTmp = 0;
		imageSurfaceTmp = 0;
		device->CreateTexture(surfaceWidth, surfaceHeight, 1, D3DUSAGE_DYNAMIC, D3DFMT_G16R16, D3DPOOL_DEFAULT, &imageTexTmp, 0);
		imageTexTmp->GetSurfaceLevel(0, &imageSurfaceTmp);
		sourceSurface = imageSurfaceTmp;
		sourceTexture = imageTexTmp;
	}

	void LiveImageRenderer::ReleaseScene()
	{
		if(d3dImage != nullptr)
			d3dImage->IsFrontBufferAvailableChanged -= gcnew DependencyPropertyChangedEventHandler(this, &LiveImageRenderer::OnIsFrontBufferAvailableChanged);

		d3dImage = nullptr;

		SAFE_RELEASE(sourceSurface);
		SAFE_RELEASE(sourceTexture);
		SAFE_RELEASE(swapSurface);
		SAFE_RELEASE(swapTexture);
		SAFE_RELEASE(renderSurface);
		SAFE_RELEASE(renderTexture);
		SAFE_RELEASE(effect);
		SAFE_RELEASE(vertexBuffer);
		SAFE_RELEASE(device);
		SAFE_RELEASE(d3d);
		DestroyWindow((HWND)window.ToPointer());
		UnregisterClass(g_wc.lpszClassName, g_wc.hInstance);
	}

	void LiveImageRenderer::Render()
	{
		if(device == nullptr)
			return;

		uiThread->VerifyAccess();

		HRESULT hr = device->BeginScene();

		hr = device->UpdateSurface(swapSurface, 0, sourceSurface, 0);

		//device->Clear(0, 0, D3DCLEAR_TARGET, 0, 1, 0);
		
		hr = device->SetRenderTarget(0, renderSurface);
		hr = effect->SetTexture("sourceTexture", sourceTexture);
		hr = effect->SetFloat("gamma", (float)(1 / gamma));
		hr = effect->SetFloat("numColors", pow(2.0f, (float)surfaceBpp));

		UINT effectPasses;
		hr = effect->Begin(&effectPasses, 0);
		for(UINT pass = 0; pass < effectPasses; ++pass)
		{
			hr = effect->BeginPass(pass);

			hr = device->SetStreamSource(0, vertexBuffer, 0, sizeof(Vertex));
			hr = device->SetFVF(VertexFormat);
			hr = device->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

			hr = effect->EndPass();
		}

		device->EndScene();

		try
		{
			d3dImage->Lock();
			d3dImage->SetBackBuffer(System::Windows::Interop::D3DResourceType::IDirect3DSurface9, System::IntPtr(renderSurface));
			d3dImage->AddDirtyRect(Int32Rect(0, 0, surfaceWidth, surfaceHeight));
			d3dImage->Unlock();
		}
		catch(ArgumentException^)
		{
			//System::Diagnostics::Debug::Fail("Backbuffer?");
			ReleaseScene();
			InitScene();
		}
	}
}