#pragma once
using namespace System;
using namespace System::Windows;

namespace AI
{

	public ref class LiveImageRenderer : public Hardware::ILiveImageRenderer
	{
	public:
		LiveImageRenderer(System::Windows::Interop::D3DImage^ d3dImage, int surfaceWidth, int surfaceHeight, int surfaceBpp);
		virtual ~LiveImageRenderer();

		virtual void Render();

		virtual property Hardware::Direct3DSurface^ Direct3DSurface 
		{ 
			Hardware::Direct3DSurface^ get()
			{
				if(d3dImage == nullptr)
					return nullptr;

				return AI::Hardware::Direct3DSurface::Create(d3dImage->Dispatcher, IntPtr(swapSurface));
			}
		}

		virtual property double Gamma
		{
			double get()
			{
				return gamma;
			}
			void set(double ng)
			{
				SetGamma(ng);
			}
		}

	private:
		System::Windows::Threading::Dispatcher^ uiThread;
		System::Windows::Interop::D3DImage^ d3dImage;
		int surfaceWidth;
		int surfaceHeight;
		int surfaceBpp;
		double gamma;

		LPDIRECT3D9 d3d;
		LPDIRECT3DDEVICE9 device;
		LPDIRECT3DVERTEXBUFFER9 vertexBuffer;
		LPDIRECT3DTEXTURE9 renderTexture;
		LPDIRECT3DSURFACE9 renderSurface;
		LPDIRECT3DTEXTURE9 swapTexture;
		LPDIRECT3DSURFACE9 swapSurface;
		LPDIRECT3DTEXTURE9 sourceTexture;
		LPDIRECT3DSURFACE9 sourceSurface;
		LPD3DXEFFECT effect;
		IntPtr window;


		void InitScene();
		void ReleaseScene();
		void OnIsFrontBufferAvailableChanged(Object ^sender, DependencyPropertyChangedEventArgs e);
		void SetGamma(double ng);
	};
}
