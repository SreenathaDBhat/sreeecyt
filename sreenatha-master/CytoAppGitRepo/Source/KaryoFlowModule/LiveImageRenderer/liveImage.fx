float4x4 worldViewProj : WorldViewProjection;
float bitsPerPixelMultiplier;
float onePixelX;
float onePixelY;
float gamma;
float numColors;

texture sourceTexture;
sampler sourceSampler = sampler_state { Texture = (sourceTexture); MipFilter = LINEAR; MinFilter = LINEAR; MagFilter = LINEAR; };

struct VS_INPUT
{
    float3 position	: POSITION;
	float2 tc : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4 hposition : POSITION;
	float2 tc  : TEXCOORD0;
};

struct PS_OUTPUT
{
	float4 color : COLOR;
};


VS_OUTPUT vs(VS_INPUT IN)
{
    VS_OUTPUT OUT;

	float3 p = IN.position;
	
	OUT.hposition = mul(worldViewProj, float4(p, 1));
	OUT.tc = IN.tc + float2(onePixelX/2, onePixelY/2);

	return OUT;
}

PS_OUTPUT ps(VS_OUTPUT IN)
{
	float sourcePixel = tex2D(sourceSampler, IN.tc).r;
	float p = sourcePixel.r * bitsPerPixelMultiplier;
	p = pow(p, gamma);
	p = clamp(p, 0, 1);
	

    PS_OUTPUT o;
    if(p < 0.05)
		o.color = clamp(0, 1, (p * 0.7) + (float4(0,0,1,1) * 0.3));
	else if(p > 0.95)
		o.color = clamp(0, 1, (p * 0.7) + (float4(1,0,0,1) * 0.3));
    else
		o.color = p;
		
    o.color.a = 1;
    return o;
}

technique Default
{
    pass Pass0
    {
		Lighting = false;
		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_2_0 vs();
		PixelShader  = compile ps_2_0 ps();
    }
}