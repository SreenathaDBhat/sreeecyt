cd..
set SOLUTIONDIR=%CD%
set APPLICATIONDIR=%5
set LOCDIR=%CD%\Localization
set PROJECTNAME=%1
set BINDIR=%SOLUTIONDIR%\%APPLICATIONDIR%\bin\%3%

set DESTLANG=%2
set WORKINGTRANSLATION=%LOCDIR%\%PROJECTNAME%_%DESTLANG%.csv
set PERFORCE="c:\Program Files\Perforce\p4.exe"
set P4SERVER=buildfs:1666
set P4CLIENT=%4
cd %LOCDIR%

copy "%LOCDIR%\LocBaml.exe" "%BINDIR%"
cd "%BINDIR%"
LocBaml.exe /parse en-US/%PROJECTNAME%.resources.dll /out:out.csv

%PERFORCE% -p %P4SERVER% -c %P4CLIENT% edit "%WORKINGTRANSLATION%"

if exist "%WORKINGTRANSLATION%" goto :gotTransFile
	copy out.csv "%WORKINGTRANSLATION%
	%PERFORCE% -p %P4SERVER% -c %P4CLIENT% add "%WORKINGTRANSLATION%"
	goto :afterMerge
:gotTransFile

"%LOCDIR%\MergeLocalizationCsv.exe" "%WORKINGTRANSLATION%" out.csv
:afterMerge

if not exist "%BINDIR%\%DESTLANG%" md "%BINDIR%\%DESTLANG%"
LocBaml.exe /generate en-US\%PROJECTNAME%.resources.dll "/trans:%WORKINGTRANSLATION%" /cul:%DESTLANG% "/out:%BINDIR%\%DESTLANG%"

%PERFORCE% -p %P4SERVER% -c %P4CLIENT% revert -a "%WORKINGTRANSLATION%"
%PERFORCE% -p %P4SERVER% -c %P4CLIENT% submit -d merged "%WORKINGTRANSLATION%"

cd %LOCDIR%