set PERFORCE="c:\Program Files\Perforce\p4.exe"
set P4SERVER=buildfs:1666
set P4CLIENT=%1

if x%1x == xx goto :usage

%PERFORCE% -p %P4SERVER% -c %P4CLIENT% edit //...xaml

call UpdateProjectUids.bat ..\BrightfieldCaseView\BrightfieldCaseView.csproj
call UpdateProjectUids.bat ..\TubsLite\TubsLite.csproj

%PERFORCE% -p %P4SERVER% -c %P4CLIENT% revert -a //....xaml
%PERFORCE% -p %P4SERVER% -c %P4CLIENT% submit -d uids //....xaml
goto :end

:usage
echo usage:
echo UpdateUids.bat [ClientSpecName]
echo -----------------------------------------------
echo example:
echo UpdateUids.bat TubsJamesXP

:end