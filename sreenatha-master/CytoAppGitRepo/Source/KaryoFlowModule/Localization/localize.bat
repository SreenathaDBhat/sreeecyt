set BUILDCONFIG=%1
set CLIENTSPEC=%2
set APPLICATIONDIR=%3

if x%BUILDCONFIG%x == xx set BUILDCONFIG=release
if x%CLIENTSPEC%x == xx set CLIENTSPEC=cvbuildTubs
if x%APPLICATIONDIR%x == xx set APPLICATIONDIR=tubs

rem *******************************************************
rem ENGLISH GB
rem *******************************************************

call LocalizeSingleProject.bat BrightfieldCaseView en-GB %BUILDCONFIG% %CLIENTSPEC% %APPLICATIONDIR%
call LocalizeSingleProject.bat TubsLite en-GB %BUILDCONFIG% %CLIENTSPEC% %APPLICATIONDIR%

rem ************************
rem CHINESE (HONG KONG SAR)
rem ************************

call LocalizeSingleProject.bat BrightfieldCaseView zh-hk %BUILDCONFIG% %CLIENTSPEC% %APPLICATIONDIR%
call LocalizeSingleProject.bat TubsLite zh-hk %BUILDCONFIG% %CLIENTSPEC% %APPLICATIONDIR%