﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Data.Linq;
using System.Text;
using AI.Connect;


namespace AI.Logging
{
    public class Logging : IDisposable
    {
        private const int NOERROR = 0;
        private const int ERROR = 1;

        public Logging()
        {
        }

        public int AddLogEntry(string username, string machinename, Guid sessionId, int eventType, Guid caseID, string caseStatus, string eventMsg)
        {
            int errCode = NOERROR;

            Database db = null;

            try
            {
                db = new Database();

                Connect.EventLog newevent = new EventLog();
                newevent.DateTime = DateTime.Now;
                newevent.UserName = username;
                newevent.MachineName = machinename;
                newevent.SessionID = sessionId;
                newevent.EventType = eventType;
                newevent.CaseID = caseID;
                newevent.CaseStatus = caseStatus;
                newevent.Description = eventMsg;

                db.EventLogs.InsertOnSubmit(newevent);
                db.SubmitChanges();
            }
            catch
            {
                errCode = ERROR;
            }
            finally
            {
                db.Dispose();
                db = null;
            }

            return errCode;
        }

        #region IDisposable Members

        public void Dispose()
        {

        }

        #endregion
    }
}
