﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.Logging
{
    public static class EventType
    {
        // 0..99 general codes
        public const int Login = 1;
        public const int Logout = 2;
        public const int UnhandledException = 3;

        //  100+  Capture specific codes
        public const int ManualCapture = 100;
        public const int ScanStart = 101;
        public const int ScanError = 102;
        public const int ScanFinished = 103;

        //  200+  Case specific codes
        public const int CaseCreated = 200;
        public const int CaseOpened = 201;
        public const int CaseDeleted = 202;
        public const int CaseModified = 203;
        public const int CaseArchived = 204;
        public const int CaseImported = 205;
        public const int CaseExported = 206;
        public const int CasePrinted = 207;
        public const int ImageDeleted = 208;
        public const int SlideAdded = 209;
        public const int SlideStatusChange = 210;
        public const int CaseStatusChange = 211;

        //  300+ Archive codes
        public const int ArchivedCase = 300;
        public const int ArchivedError = 301;
        public const int AutoArchiveStarted = 302;
        public const int AutoArchiveFinished = 303;
    }
}
