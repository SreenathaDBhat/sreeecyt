#include "stdafx.h"
#include "../LegacyCV/include/Woolz.h"

static int get_mean_diff(FSCHAR *image, FSCHAR *image1, FSCHAR *image2, int cols, int lines)
{
       FSCHAR *imptr, *imptr1, *imptr2;
       int val, sum, count, mean_diff, imsize;
 
       // Find sum of all differences between image1 and image2
       // only where the difference between image1 and the original
       // is greater than a noise threshold of 8
       // This should ignore most areas inside nucleii or clusters
       // and outside of chromosomes */
 
       imptr=image;
       imptr1=image1;
       imptr2=image2;
       imsize=cols*lines;
 
       sum=0; count=1;
 
       while (imsize--) {
 
              // subtract image1 from original image
              val= *imptr - *imptr1;
 
              // if greater than a noise threshold of 8
              // sum the difference between the images generated
              // from the different structure elements
              // note - only interested in where image1 < image
              if ((val > 8)) {
                     sum += *imptr1 - *imptr2;
                     count++;
              }
 
              imptr++;
              imptr1++;
              imptr2++;
       }
 
       // find maximum difference allowed as the mean of differences
       mean_diff=sum/count;
 
       return(mean_diff);
}
static void subtract_mean_diff(FSCHAR *image, FSCHAR *image1, FSCHAR *image2, int cols, int lines, int mean_diff)
{
       FSCHAR *imptr, *imptr1, *imptr2;
       int val, imsize;
 
       // Store mean_diff limited background in image 1
       imptr=image;
       imptr1=image1;
       imptr2=image2;
       imsize=cols*lines;
 
       while (imsize--) {
 
              val= *imptr2 + mean_diff;
 
              if (val < *imptr1)
                     *imptr1=val;
 
              // find min of original image and chromwidth structure
              // element processed image limited by max_diff with
              // large structure element processed image and store result
              // in image1 - only need to do this on first pass
 
              if (*imptr < *imptr1)
                     *imptr1 = *imptr;
 
              imptr++;
              imptr1++;
              imptr2++;
       }
}
static void subtract_mean_diff2(FSCHAR *image1, FSCHAR *image2, int cols, int lines, int mean_diff)
{
       FSCHAR *imptr1, *imptr2;
       int val, imsize;
 
       // Store mean_diff limited background in image 1
       imptr1=image1;
       imptr2=image2;
       imsize=cols*lines;
 
       while (imsize--) {
              val= *imptr2 + mean_diff;
 
              if (val < *imptr1)
                     *imptr1=val;
 
              imptr1++;
              imptr2++;
       }
}

FSCHAR *AdaptiveSubtract(FSCHAR *image, int cols, int lines)
{
       FSCHAR *bgimage=NULL, *bgimage2=NULL;
       FSCHAR *noisy_image, *noisless_image;
       FSCHAR *imptr, *bgimptr;
       int mean_diff, imsize;
       int firstpass;
 
       if (!image)
              return NULL;
 
       int noise_structure_size=2;      
       int min_structure_size=16;                     
       int max_structure_size=256;
       int structure_size = min_structure_size;
 
       // max structure size must work with image
       while (max_structure_size * 2 >= cols)
              max_structure_size/= 2;
       while (max_structure_size * 2 >= lines)
              max_structure_size/= 2;
      
       // Close image - remove bright spot noise
       noisy_image=(FSCHAR *)fsClose(image,cols,lines,noise_structure_size);
       if (noisy_image == NULL)
              return NULL;
 
       // we need to handle big white objects - which are brighter than the background
       // for example bubbles - becuase these mess up the bgimages created by fsOpen
       // So we force these to be the same color as the mode of the noisy_image
 
       // histogram noisy image
       int hist[256];
       for (int i=0;i<256;i++)
              hist[i]=0;
 
       imptr=noisy_image;
       imsize=cols*lines;
 
       while (imsize--)
              hist[*imptr++]++;
 
       // determine mode of histogram - approximate value of background
       int mode=0;
       int max=0;
       for (int i=0;i<256;i++) {
              if (hist[i]>max) {
                     max=hist[i];
                     mode=i;
              }
       }
 
       // set any pixels below mode to mode - handles large white spots e.g. bubbles
       imptr=noisy_image;
       imsize=cols*lines;
 
       while (imsize--)
       {
              if (*imptr<mode)
                     *imptr=mode;
 
              imptr++;
       }
 
 
 
       // Open image - remove dark spot noise
       noisless_image=(FSCHAR *)fsOpen(noisy_image,cols,lines,noise_structure_size);
       Free(noisy_image);
       if (noisless_image == NULL)
              return NULL;
      
       // Open noise-free image with object size element
       // to create background image in bgimage
       bgimage=(FSCHAR *)fsOpen(noisless_image,cols,lines,structure_size);
       if (bgimage == NULL) {
              Free(noisless_image);
              return NULL;
       }
 
       firstpass=1;
       while (structure_size < max_structure_size) {
      
              /* double size of structuring element */
              structure_size *= 2;
             
              if (bgimage2)
                     Free(bgimage2);
 
              // create new background image based on larger structure
              bgimage2=(FSCHAR *)fsOpen(noisless_image,cols,lines,structure_size);
 
              if (!bgimage2) {
                     Free(bgimage);
                     Free(noisless_image);
                     return NULL;
              }
      
              // get mean difference between this and bgimage
              mean_diff=get_mean_diff(image, bgimage, bgimage2, cols, lines);
 
              // limit bgimage with mean difference
              if (firstpass) {
                     subtract_mean_diff(image, bgimage, bgimage2, cols, lines, mean_diff);
                     firstpass=0;
              }
              else
                     subtract_mean_diff2(bgimage, bgimage2, cols, lines, mean_diff);
       }
 
       if (bgimage2)
              Free(bgimage2);
 
       if (noisless_image)
              Free(noisless_image);
 
       // Subtract mean_diff limited background image from original
       // and store result in bgimage
 
       imptr = image;
       bgimptr =  bgimage;
       imsize = cols*lines;
 
       while (imsize--) {
              *bgimptr =  *imptr - *bgimptr;
 
              imptr++;
              bgimptr++;
       }
 
       // success
       return bgimage;
}