#include "StdAfx.h"
#include <math.h>
#include "../LegacyCV/include/Woolz.h"
#include "../LegacyCV/include/chromanal.h"
#include "ChromosomeServices.h"
#include "../LegacyCV/include/chromlib.h"
#include "../LegacyCV/include/CreateArray.h"
#include "../LegacyCV/include/karylib.h"
#include "../LegacyCV/include/species.h"
#include "../LegacyCV/include/trainkarcl.h"
#include "../LegacyCV/include/clfile.h"
#include <assert.h>

using namespace System;
using namespace System::Reflection;

const int BASIC = 1;
const double XSCALE = 1.0;
const double YSCALE = 1.0;


ChromosomeServices::ChromosomeServices(void)
{
}

ChromosomeServices::~ChromosomeServices(void)
{
}


void ChromosomeServices::DumpChromMeas( struct chromplist& chromMeas)
{
	WCHAR msg[256];
	swprintf( msg, L"\nDumpChromMeas()");					OutputDebugString( msg);
	swprintf( msg, L"otype = %d\n", chromMeas.otype);		OutputDebugString( msg);
	swprintf( msg, L"area = %d\n", chromMeas.area);			OutputDebugString( msg);
	swprintf( msg, L"length = %d\n", chromMeas.length);		OutputDebugString( msg);
	swprintf( msg, L"mass = %d\n", chromMeas.mass);			OutputDebugString( msg);
	swprintf( msg, L"cindexa = %d\n", chromMeas.cindexa);	OutputDebugString( msg);
	swprintf( msg, L"cindexm = %d\n", chromMeas.cindexm);	OutputDebugString( msg);
	swprintf( msg, L"cindexl = %d\n", chromMeas.cindexl);	OutputDebugString( msg);
	swprintf( msg, L"hullperim = %d\n", chromMeas.hullperim);	OutputDebugString( msg);
	swprintf( msg, L"cvdd = %d\n", chromMeas.cvdd);			OutputDebugString( msg);
	swprintf( msg, L"nssd = %d\n", chromMeas.nssd);			OutputDebugString( msg);
	swprintf( msg, L"mdra = %d\n", chromMeas.mdra);			OutputDebugString( msg);
	swprintf( msg, L"wdd[0] = %d\n", chromMeas.wdd[0]);		OutputDebugString( msg);
	swprintf( msg, L"wdd[1] = %d\n", chromMeas.wdd[1]);		OutputDebugString( msg);
	swprintf( msg, L"wdd[2] = %d\n", chromMeas.wdd[2]);		OutputDebugString( msg);
	swprintf( msg, L"wdd[3] = %d\n", chromMeas.wdd[3]);		OutputDebugString( msg);
	swprintf( msg, L"wdd[4] = %d\n", chromMeas.wdd[4]);		OutputDebugString( msg);
	swprintf( msg, L"wdd[5] = %d\n", chromMeas.wdd[5]);		OutputDebugString( msg);
	swprintf( msg, L"mwdd[0] = %d\n", chromMeas.mwdd[0]);	OutputDebugString( msg);
	swprintf( msg, L"mwdd[1] = %d\n", chromMeas.mwdd[1]);	OutputDebugString( msg);
	swprintf( msg, L"mwdd[2] = %d\n", chromMeas.mwdd[2]);	OutputDebugString( msg);
	swprintf( msg, L"mwdd[3] = %d\n", chromMeas.mwdd[3]);	OutputDebugString( msg);
	swprintf( msg, L"mwdd[4] = %d\n", chromMeas.mwdd[4]);	OutputDebugString( msg);
	swprintf( msg, L"mwdd[5] = %d\n", chromMeas.mwdd[5]);	OutputDebugString( msg);
	swprintf( msg, L"gwdd[0] = %d\n", chromMeas.gwdd[0]);	OutputDebugString( msg);
	swprintf( msg, L"gwdd[1] = %d\n", chromMeas.gwdd[1]);	OutputDebugString( msg);
	swprintf( msg, L"gwdd[2] = %d\n", chromMeas.gwdd[2]);	OutputDebugString( msg);
	swprintf( msg, L"gwdd[3] = %d\n", chromMeas.gwdd[3]);	OutputDebugString( msg);
	swprintf( msg, L"gwdd[4] = %d\n", chromMeas.gwdd[4]);	OutputDebugString( msg);
	swprintf( msg, L"gwdd[5] = %d\n", chromMeas.gwdd[5]);	OutputDebugString( msg);
	swprintf( msg, L"nbands = %d\n", chromMeas.nbands);		OutputDebugString( msg);
	swprintf( msg, L"nbindex = %d\n", chromMeas.nbindex);	OutputDebugString( msg);
}


bool ChromosomeServices::GetChromMeasurements( struct object * pWoolzObj, struct chromplist& chromMeas )
{
	bool bGotMeas = false;

	// Convert woolz object to chromosome and measure
	struct ::chromosome chromObj;

	chromObj.type = pWoolzObj->type;
	chromObj.idom = pWoolzObj->idom;
	chromObj.vdom = pWoolzObj->vdom;
	chromObj.plist = (struct chromplist *) makechromplist();
	chromObj.assoc = NULL;

	if (chromObj.plist->otype <= CHROMOSOME )
	{
		struct ::chromosome * measobj = (struct ::chromosome *) chromfeatures( &chromObj, BASIC, XSCALE, YSCALE);
		if (measobj != NULL)
		{
			// Bitwise copy all properties of chromsome
			chromMeas = *measobj->plist;
			bGotMeas = true;

			// Length and width in metaphase so need original object
			chromMeas.widthInMetaphase = chromObj.idom->lastkl - chromObj.idom->kol1 + 1;
			chromMeas.lengthInMetaphase = chromObj.idom->lastln - chromObj.idom->line1 + 1;

			// Length and width measured from original object rotated to vertical
			chromMeas.widthVertical  = measobj->idom->lastkl - measobj->idom->kol1 + 1;
			chromMeas.lengthVertical = measobj->idom->lastln - measobj->idom->line1 + 1;

			// Get centromere position in woolz metaphase image coords
			float midptx=(chromObj.idom->kol1 + chromObj.idom->lastkl)/2.0f;
			float midpty=(chromObj.idom->line1 + chromObj.idom->lastln)/2.0f;

			/* get degree of rotation */
			float angle= 0.0f - chromMeas.rangle;

			angle*=3.1415926f/180.0f;

			float x1=chromMeas.cx-midptx;
			float y1=chromMeas.cy-midpty;
			float x2=(float)(x1*cos(angle) - y1*sin(angle));
			float y2=(float)(x1*sin(angle) + y1*cos(angle));
			chromMeas.cx=(int)(x2+midptx+0.5);			/* 0.5 handles rounding */ 
			chromMeas.cy=(int)(y2+midpty+0.5);

			// Now transform coordinates
			// from woolz image   - origin bottom left, x increasing to the right and y increasing upwards
			// to tubs chrom mask - origin top left,    x increasing to the right and y increasing downwards
			chromMeas.cx = chromMeas.cx - chromObj.idom->kol1;
			short extentY = chromObj.idom->lastln - chromObj.idom->line1;
			chromMeas.cy = extentY - (chromMeas.cy - chromObj.idom->line1);


			// Get mid point of vertical object relative to tubs chrom mask
			float vmidptx = (measobj->idom->kol1 + measobj->idom->lastkl)/2.0;
			float vmidpty = (measobj->idom->line1 + measobj->idom->lastln)/2.0;
			x1=vmidptx-midptx;
			y1=vmidpty-midpty;
			x2=(float)(x1*cos(angle) - y1*sin(angle));
			y2=(float)(x1*sin(angle) + y1*cos(angle));
			vmidptx=(int)(x2+midptx+0.5);			/* 0.5 handles rounding */ 
			vmidpty=(int)(y2+midpty+0.5);

			chromMeas.dmidPointX = vmidptx - chromObj.idom->kol1;
			chromMeas.dmidPointY = extentY - (vmidpty - chromObj.idom->line1);


//			DumpChromMeas( chromMeas);

			freeobj( (struct object *) measobj);
			chromObj.plist = NULL;
		}
	}


	if (chromObj.plist)
	{
		free(chromObj.plist);
		chromObj.plist = NULL;
	}
	
	return bGotMeas;
}


bool ChromosomeServices::LoadClassifier()
{
	return LoadClassifier( SHORT_CLASS);
}

bool ChromosomeServices::LoadClassifier( int classid)
{
	bool bLoadOk = false;

	// Get root exe dir and set for debug and release
	String^ folder = Assembly::GetExecutingAssembly()->Location;
	folder = System::IO::Path::GetDirectoryName(folder);
	IntPtr ptr = System::Runtime::InteropServices::Marshal::StringToHGlobalUni(folder);

	TCHAR fullname[_MAX_PATH] = {0};
	int default_human	= 1;
	int fbarcodedata	= 0;
	CLLoadClass retval	= LOADCLASS_ERROR;
	int NumOfClasses	= DEFAULT_NUMOFCLASSES;
	LPCTSTR species		= szspeciesdefault;


	// Try to load classifier based on the classifier id
	// If a classifier is loaded successfully set retval
	switch ( classid )
	{
		case NO_CLASS:		// Indicates some previous problem loading a classifier
			break;

		case MAN_CLASS:		// Manual classification
			if (default_human)
			{
				kclass_def();
				//*pclassid = SHORT_CLASS;
				retval    = LOADCLASS_SHORT;
			}// otherwise let calling routine handle other species
			break;

		case SHORT_CLASS:
			kclass_def();	
			retval = LOADCLASS_OK;	// *pclassid already set correctly
			break;

		case LONG_CLASS:	
			_stprintf(fullname, _T("%s\\classifiers\\longclass"), (LPCTSTR)ptr.ToPointer());
			kclass_read( fullname, default_human, NumOfClasses);
			retval = LOADCLASS_OK;	// *pclassid already set correctly
			break;

		case RBAND_CLASS:	
			_stprintf(fullname, _T("%s\\classifiers\\Rbandclass"), (LPCTSTR)ptr.ToPointer());
			kclass_read( fullname, default_human, NumOfClasses);
			retval = LOADCLASS_OK;	// *pclassid already set correctly
			break;


		default:			// Load user classifier
			// load_user_classifier will set the return value and classifier id correctly.
			// Also if the user classifier couldnt be loaded and the data is defaulthuman
			// it will try to load alternatives...
			// ...otherwise let calling routine handle other species or error
			//retval = load_user_classifier( &classid, default_human, NumOfClasses, species, fbarcodedata);
			break;
	} // end of switch

	System::Runtime::InteropServices::Marshal::FreeHGlobal(ptr);

	// If no classifier was loaded successfully then set the classid to invalid
	//if (retval == LOADCLASS_ERROR)
	//	*pclassid = NO_CLASS;


	return bLoadOk;
}

//
//	Classify
//	Based on kchromclass() in kchromclass.cpp in cytovision.
//
void ChromosomeServices::Classify( ptrcplist* rawChromosomes, int nchromcount)
{
	assert( rawChromosomes != NULL);

	if (rawChromosomes && nchromcount > 0)
	{
		// TODO: set somewhere else, as this should be set once only not once per metaphase
		LoadClassifier();
		//LoadClassifier( LONG_CLASS);

		// Create fake chromsome objects for classification stowing the plist from the rawchromosomes inside
		ptrchrom* chrlist = new ptrchrom[nchromcount];
		int numchr = 0;
		for (int i = 0; i < nchromcount; i++)
		{
			// TODO: if (kcont->acl[i] & ACTIVE) {
			if (rawChromosomes[i]->otype <= 1)
			{
				struct chromosome * pfakeChrom = new struct chromosome();
				pfakeChrom->plist = rawChromosomes[i];
				chrlist[numchr++] = pfakeChrom;
			}
		}

		if (numchr > 0)
		{
			// TODO: pass in chromplist array instead
			kchromclass( chrlist, numchr);

			// Free fake chromosomes without destroying the chromplists
			for (int i=0;i<numchr;i++)
			{
				chrlist[i]->plist = NULL;
				freeobj( (struct object *) chrlist[i]);
			}
		}

		delete[] chrlist;
	}
}

