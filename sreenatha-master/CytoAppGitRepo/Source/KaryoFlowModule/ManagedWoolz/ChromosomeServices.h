#pragma once

typedef struct chromplist* ptrcplist;
typedef struct chromosome* ptrchrom;

class ChromosomeServices
{
public:
	ChromosomeServices(void);
	~ChromosomeServices(void);

public:
	static void DumpChromMeas( struct chromplist& chromMeas);
	static bool GetChromMeasurements( struct object * pWoolzObj, struct chromplist& chromMeas );
	static bool LoadClassifier();
	static bool LoadClassifier( int classid);
	static void Classify( ptrcplist* rawChromosomes, int nchromcount);
};
