#include "stdafx.h"
#include <stdio.h>
#include "ChromsomeMeasurements.h"
#include "..\LegacyCV\include\chromlib.h"

namespace AI
{
	const int MOMENTPARAMCOUNT = 6;

	ChromosomeMeasurements::ChromosomeMeasurements(void)
	{
		wdd = gcnew array<short>(MOMENTPARAMCOUNT);
		mwdd = gcnew array<short>(MOMENTPARAMCOUNT);
		gwdd = gcnew array<short>(MOMENTPARAMCOUNT);
	}

	ChromosomeMeasurements::ChromosomeMeasurements( struct ::chromplist& meas)
	{
		// Object type
		otype = meas.otype;

		// Measurements
		area = meas.area;
		length = meas.length;
		width = meas.width;
		mass = meas.mass;
		cindexa = meas.cindexa;
		cindexm = meas.cindexm;
		cindexl = meas.cindexl;
		hullperim = meas.hullperim;
		cvdd = meas.cvdd;
		nssd = meas.nssd;
		mdra = meas.mdra;
		wdd  = gcnew array<short>(MOMENTPARAMCOUNT);
		mwdd = gcnew array<short>(MOMENTPARAMCOUNT);
		gwdd = gcnew array<short>(MOMENTPARAMCOUNT);
		for (int i=0; i<MOMENTPARAMCOUNT; i++)
		{
			wdd[i] = meas.wdd[i];
			mwdd[i] = meas.mwdd[i];
			gwdd[i] = meas.gwdd[i];
		}

		nbands = meas.nbands;
		nbindex = meas.nbindex;

		// Classification
		pgroup = meas.pgroup;
		pgconf = meas.pgconf;

		// Centromere
		cx = meas.cx;
    	cy = meas.cy;

		// Orientation
    	rangle = meas.rangle;

		// Non-classification measurements
		lengthInMetaphase = meas.lengthInMetaphase;
		widthInMetaphase  = meas.widthInMetaphase;
		lengthVertical    = meas.lengthVertical;
		widthVertical     = meas.widthVertical;
		dmidPointX        = meas.dmidPointX;
		dmidPointY        = meas.dmidPointY;
	}

	struct ::chromplist * ChromosomeMeasurements::GetRawChromosome()
	{
		struct ::chromplist * pchromplist = (struct chromplist *) makechromplist();
		if (pchromplist)
		{
			// Object type
			pchromplist->otype = otype;

			// Measurements
			pchromplist->area = area;
			pchromplist->length = length;
			pchromplist->width = width;
			pchromplist->mass = mass;
			pchromplist->cindexa = cindexa;
			pchromplist->cindexm = cindexm;
			pchromplist->cindexl = cindexl;
			pchromplist->hullperim = hullperim;
			pchromplist->cvdd = cvdd;
			pchromplist->nssd = nssd;
			pchromplist->mdra = mdra;
			for (int i=0; i<MOMENTPARAMCOUNT; i++)
			{
				pchromplist->wdd[i] = wdd[i];
				pchromplist->mwdd[i] = mwdd[i];
				pchromplist->gwdd[i] = gwdd[i];
			}

			pchromplist->nbands = nbands;
			pchromplist->nbindex = nbindex;

			// Classification
			pchromplist->pgroup = pgroup;
			pchromplist->pgconf = pgconf;

			// Centromere
			pchromplist->cx = cx;
    		pchromplist->cy = cy;

			// Orientation
    		pchromplist->rangle = rangle;

			// Non-classification measurements
			pchromplist->lengthInMetaphase = lengthInMetaphase;
			pchromplist->widthInMetaphase = widthInMetaphase;
			pchromplist->lengthVertical = lengthVertical;
			pchromplist->widthVertical = widthVertical;
			pchromplist->dmidPointX    = dmidPointX;
			pchromplist->dmidPointY    = dmidPointY;
		}

		return pchromplist;
	}

	void ChromosomeMeasurements::SetClass( struct ::chromplist& meas)
	{
		pgroup = meas.pgroup;
		pgconf = meas.pgconf;
	}
}
