#pragma once

#include "..\LegacyCV\include\machdef.h"				// TODO : try to remove in future. Needed for chromanal.h
#include "..\LegacyCV\include\chromanal.h"

using namespace System;
using namespace System::Windows;
using namespace System::Collections::Generic;

namespace AI
{
	public ref class ChromosomeMeasurements
	{
	public:
		ChromosomeMeasurements(void);
		ChromosomeMeasurements( struct ::chromplist& meas);

	public:
		property int Area
		{
			int get() { return area; }
		}

		property short Length
		{
			short get() { return length; }
		}

		property short LengthInMetaphase
		{
			short get() { return lengthInMetaphase; }
		}

		property short LengthVertical
		{
			short get() { return lengthVertical; }
		}

		property short Width
		{
			short get() { return width; }
		}

		property short WidthInMetaphase
		{
			short get() { return widthInMetaphase; }
		}

		property short WidthVertical
		{
			short get() { return widthVertical; }
		}

		property short CentromericIndexByArea
		{
			short get() { return cindexa; }
		}
		
		property short CentromericIndexByMass
		{
			short get() { return cindexm; }
		}
		
		property short CentromericIndexByLength
		{
			short get() { return cindexl; }
		}

		property short Group
		{
			short get() { return pgroup; }
		}

		property short Confidence
		{
			short get() { return pgconf; }
		}

		property short CentromereX
		{
			short get() { return cx; }
		}

		property short CentromereY
		{
			short get() { return cy; } 
		}

		property double MidPointX
		{
			double get() { return dmidPointX; }
		}

		property double MidPointY
		{
			double get() { return dmidPointY; }
		}

		property short Type
		{
			short get() { return otype; } 
		}

		struct ::chromplist * GetRawChromosome();

		void SetClass( struct ::chromplist& meas);

	private:

		// Object type
    	short otype;	/* object type :
    					1	chromosome
    					2	composite
    					3	overlap
    					4	piece
    					5	nucleus
    					6	spot noise (small unknown)
    					7	blob noise (large unknown) */

		// Measurements
    	int area;
    	short length;
		short width;
    	int mass;
    	short cindexa;		/* centromeric index (%) - area */
    	short cindexm;		/*      "        "    "  - mass */
    	short cindexl;		/*      "        "    "  - length */
    	int hullperim;		/* c.h. perimeter */
    	short cvdd;			/* coefficient of variation of density profile */
		short nssd;			/* Granum's NSSD of density profile */
    	short mdra;			/* ratio of mass c.i. to area c.i. */
    	array<short>^ wdd;	/* 6 weighted density distribution parameters */
    	array<short>^ mwdd;	/* 6 wdd of 2nd moment profile parameters */
    	array<short>^ gwdd;	/* 6 wdd of profile of absolute gradient of dd */
        short nbands;       /* number of extrema in density profile */
        short nbindex;      /* ratio of smaller number of extrema in */
							/* half density profile to nbands (%) */

		// Classification
    	short pgroup;		/* paris group/class */	// One based class number
    	short pgconf;

		// Centromere
    	short cx;			/* centromere parameters */
    	short cy;

		// Orientation
    	short rangle;		/* m.w. rectangle orientation */

		// Non-classification measurements
		short lengthInMetaphase;
		short widthInMetaphase;
		short lengthVertical;
		short widthVertical;
		double dmidPointX;
		double dmidPointY;

	};
}
