#include "StdAfx.h"
#include "stdio.h"
#include "ImageDebug.h"


#pragma warning(disable : 4996)


ImageDebug::ImageDebug()
{
}

ImageDebug::~ImageDebug()
{
}


void ImageDebug::DumpImage( const char * szfilename, void * pImage, size_t sizeofType, size_t count)
{
	FILE * fp = fopen( szfilename, "wb");
	if (fp != NULL)
	{
		fwrite( pImage, sizeofType, count, fp);
		fclose( fp);
	}
}
