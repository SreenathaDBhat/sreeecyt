#pragma once

class ImageDebug
{

public:
	ImageDebug();
	~ImageDebug();

	static void DumpImage( const char * szfilename, void * pImage, size_t sizeofType, size_t count);

};
