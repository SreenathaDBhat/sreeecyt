#include "stdafx.h"
#include <math.h>
#include "../LegacyCV/include/Woolz.h"
#include "ImageDebug.h"
#include "ImageProcessingSW.h"


FSCHAR *AdaptiveSubtract(FSCHAR *image, int cols, int lines);

namespace AI
{
	Bitmap::Channel^ SubtractBackground::Adaptive(Channel^ image)
	{
		unsigned char* bits = new unsigned char[image->Pixels->Length];
		pin_ptr<unsigned short> sixteenBit = &image->Pixels[0];
		unsigned short* src = sixteenBit;
		unsigned short* srcEnd = sixteenBit + image->Pixels->Length;
		unsigned char* dest = bits;
		unsigned char v;
		double maxPixel = pow(2.0, image->BitsPerPixel);

		while(src != srcEnd)
		{
			*dest++ = 255 - (unsigned char) ((*src++ / maxPixel) * 256);
		}

		//ImageDebug::DumpImage( "D:\\Adaptive.before.raw", bits, sizeof(FSCHAR), image->Pixels->Length);

		FSCHAR* newImage = AdaptiveSubtract(bits, image->Width, image->Height);

		//ImageDebug::DumpImage( "D:\\Adaptive.after.raw", newImage, sizeof(FSCHAR), image->Pixels->Length);

		int len = image->Width * image->Height;
		array<unsigned short>^ bitmapData = gcnew array<unsigned short>(len);
		pin_ptr<unsigned short> bitmapDataPtr = &bitmapData[0];
		unsigned short* bmpPtr = bitmapDataPtr;
		unsigned char* thresh = newImage;
		for(int i = 0; i < len; ++i)
		{
			*bmpPtr++ = 255 - *thresh++;
		}

		Free(newImage);
		delete[] bits;

		return gcnew Bitmap::Channel(image->Width, image->Height, 8, bitmapData);
	}

	Bitmap::Channel^ Threshold::SubtractBackground(Channel^ image)
	{
		unsigned char* bits = new unsigned char[image->Pixels->Length];
		pin_ptr<unsigned short> sixteenBit = &image->Pixels[0];
		unsigned short* src = sixteenBit;
		unsigned short* srcEnd = sixteenBit + image->Pixels->Length;
		unsigned char* dest = bits;
		unsigned char v;
		double maxPixel = pow(2.0, image->BitsPerPixel);

		while(src != srcEnd)
		{
			*dest++ = 255 - (unsigned char) ((*src++ / maxPixel) * 256);
		}

		FSCHAR* newImage = AdaptiveSubtract(bits, image->Width, image->Height);

		int len = image->Width * image->Height;
		array<unsigned short>^ bitmapData = gcnew array<unsigned short>(len);
		pin_ptr<unsigned short> bitmapDataPtr = &bitmapData[0];
		unsigned short* bmpPtr = bitmapDataPtr;
		unsigned char* thresh = newImage;
		for(int i = 0; i < len; ++i)
		{
			*bmpPtr++ = *thresh++;
		}

		Free(newImage);
		delete[] bits;

		return gcnew Bitmap::Channel(image->Width, image->Height, 8, bitmapData);
	}

	Bitmap::Channel^ Threshold::GenerateMask(Channel^ image, int postSubractThresh)
	{
		unsigned char* bits = new unsigned char[image->Pixels->Length];
		pin_ptr<unsigned short> sixteenBit = &image->Pixels[0];
		unsigned short* src = sixteenBit;
		unsigned short* srcEnd = sixteenBit + image->Pixels->Length;
		unsigned char* dest = bits;
		unsigned char v;
		double maxPixel = pow(2.0, image->BitsPerPixel);

		while(src != srcEnd)
		{
			*dest++ = 255 - (unsigned char) ((*src++ / maxPixel) * 256);
		}

		//ImageDebug::DumpImage( "D:\\GenMask.src.raw", bits, sizeof(FSCHAR), image->Pixels->Length);

		FSCHAR* newImage = AdaptiveSubtract(bits, image->Width, image->Height);

		//ImageDebug::DumpImage( "D:\\GenMask.backsub.raw", newImage, sizeof(FSCHAR), image->Pixels->Length);

		int len = image->Width * image->Height;
		array<unsigned short>^ bitmapData = gcnew array<unsigned short>(len);
		pin_ptr<unsigned short> bitmapDataPtr = &bitmapData[0];
		unsigned short* bmpPtr = bitmapDataPtr;
		unsigned char* thresh = newImage;
		for(int i = 0; i < len; ++i)
		{
			v = *thresh++;
			v = v >= postSubractThresh ? 0 : 255;
			*bmpPtr++ = v;
		}

		Free(newImage);
		delete[] bits;

		return gcnew Bitmap::Channel(image->Width, image->Height, 8, bitmapData);
	}

	int Threshold::GetBackgroundThresholdValue( Channel^ image)
	{
		unsigned char* bits = new unsigned char[image->Pixels->Length];
		pin_ptr<unsigned short> sixteenBit = &image->Pixels[0];
		unsigned short* src = sixteenBit;
		unsigned short* srcEnd = sixteenBit + image->Pixels->Length;
		unsigned char* dest = bits;
		double maxPixel = pow(2.0, image->BitsPerPixel);

		while(src != srcEnd)
		{
			*dest++ = 255 - (unsigned char) ((*src++ / maxPixel) * 256);
		}

		//ImageDebug::DumpImage( "D:\\GetBackThreshValue.raw", bits, sizeof(FSCHAR), image->Pixels->Length);

		// Add one (groan) as we thresh everything less than or equal to a thresh but CV thresholds everything less than a thresh
		int threshold = BackgroundThresh( bits, image->Width, image->Height) + 1;

		delete[] bits;

		return max( 255 - threshold, 0);
	}

}