#pragma once
using namespace System;
using namespace System::Windows::Media::Imaging;
using namespace AI::Bitmap;

namespace AI
{
	public ref class SubtractBackground
	{
	public:
		static Bitmap::Channel^ Adaptive(Channel^ image);
	};

	public ref class Threshold
	{
	public:
		static Bitmap::Channel^ GenerateMask(Channel^ image, int postSubractThresh);
		static Bitmap::Channel^ SubtractBackground(Channel^ image);
		static int GetBackgroundThresholdValue( Channel^ image);
	};

}
