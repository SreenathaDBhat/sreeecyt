#include "stdafx.h"
#include "assert.h"
#include "..\LegacyCV\include\WoolzIF.h"
#include "Woolz.h"
#include "ChromosomeServices.h"
#include "math.h"
#include "ImageDebug.h"

namespace
{
	CWoolzIP woolz;
}

namespace AI
{
	WoolzObject::WoolzObject(FSCHAR* maskData, WoolzMeasurements* m, ChromosomeMeasurements^ meas, PointCollection^ boundary, int imageWidth, int imageHeight, int margin)
	{
		int boundsLeft = Math::Max(0, m->BBLeft - margin);
		int boundsTop = Math::Max(0, m->BBTop - margin);
		int boundsRight = Math::Min(imageWidth, m->BBRight + margin);
		int boundsBottom = Math::Min(imageHeight, m->BBBottom + margin);
		bounds = Int32Rect(boundsLeft, boundsTop, boundsRight - boundsLeft, boundsBottom - boundsTop);

		array<UInt16>^ channelBytes = gcnew array<UInt16>(bounds.Width * bounds.Height);
		pin_ptr<UInt16> destPtr = &channelBytes[0];
		for(int i = 0; i < channelBytes->Length; ++i) channelBytes[i] = 255;

		for(int y = 0; y < bounds.Height; ++y)
		{
			int sourceY = y + boundsTop;
			FSCHAR* source = maskData + (sourceY * imageWidth) + boundsLeft;

			int destY = (bounds.Height - y) - 1;
			UInt16* dest = destPtr + bounds.Width * destY;

			for(int x = 0; x < bounds.Width; ++x)
				*dest++ = *source++;
		}


		minRadii = m->MinRadii;
		maxRadii = m->MaxRadii;
		orientation = -m->Orientation * (180 / Math::PI);
		mask = gcnew Channel(bounds.Width, bounds.Height, 8, channelBytes);
		bounds.Y = imageHeight - bounds.Y - bounds.Height;
		
		Matrix rotMatrix;
		rotMatrix.Rotate(orientation);

		array<Point>^ rotatedPoints = gcnew array<Point>(4);
		double bw = bounds.Width / 2.0;
		double bh = bounds.Height / 2.0;
		rotatedPoints[0] = (rotMatrix.Transform(Point(-bw, -bh)));
		rotatedPoints[1] = (rotMatrix.Transform(Point( bw, -bh)));
		rotatedPoints[2] = (rotMatrix.Transform(Point( bw,  bh)));
		rotatedPoints[3] = (rotMatrix.Transform(Point(-bw,  bh)));

		length = 0;
		for(int i = 0; i < 3; ++i)
		{
			for(int j = i; j < 4; ++j)
			{
				double ySep = Math::Abs(rotatedPoints[i].Y - rotatedPoints[j].Y);
				length = Math::Max(length, ySep);
			}
		}

		measurements = meas;
		outline = boundary;
	}

	int Woolz::CountObjects(Channel^ image, UInt16 threshold, int margin, int minArea)
	{
		CWoolzIP woolz;
		FSCHAR* imageWoolz = ChannelToWoolz(image);
		int numberOfObjects = woolz.FindObjects(imageWoolz, image->Width, image->Height, threshold, minArea);
		Free(imageWoolz);
		
		return numberOfObjects;
	}

	IEnumerable<WoolzObject^>^ Woolz::DetectSplitObjects(Channel^ image, Channel^ cutMask, UInt16 threshold, int margin, int minArea)
	{	
		List<WoolzObject^>^ woolzObjects = gcnew List<WoolzObject^>;

		if (image->Width == cutMask->Width &&
			image->Height == cutMask->Height)
		{
			int imageLen = image->Width * image->Height;
			FSCHAR* compositeMaskImage = new FSCHAR[imageLen];
			memset( compositeMaskImage, 255, imageLen);
			woolzObjects = DetectObjects( image, threshold, margin, minArea, compositeMaskImage);

			if (woolzObjects->Count > 1)
			{
				pin_ptr<unsigned short> cutMaskImage = &cutMask->Pixels[0];
				RestoreCutPixels( cutMask, compositeMaskImage, image->Width, image->Height, woolzObjects);
			}

			Free( compositeMaskImage);
		}
		else
		{
			assert( false);		// image and cutMask are different sizes
		}

		return woolzObjects;
	}

	void Woolz::RestoreCutPixels( Channel^ cutMask, FSCHAR* compositeMaskImage, int width, int height, List<WoolzObject^>^ woolzObjects)
	{
		_cutPixelUnassigned = 0;

		int halfWidth = 2;

		int binCount = 256;
		int* pHisto = new int[binCount];
		memset( pHisto, 0, binCount);

		pin_ptr<unsigned short> cutMaskImage = &cutMask->Pixels[0];
		unsigned short* pCutMaskRow = cutMaskImage + halfWidth*width + halfWidth;
		for (int j=halfWidth; j<height-halfWidth; j++)
		{
			unsigned short* c = pCutMaskRow;

			for (int i=halfWidth; i<width-halfWidth; i++, c++)
			{
				// Is point (i,j) in the cut?
				if (*c == 0)
				{
					// Find distribution of neighbouring points in composite mask over woolz objects
					memset( pHisto, 0, binCount);
					FSCHAR* pCompMaskRow = compositeMaskImage + (j-halfWidth)*width + (i-halfWidth);
					for (int n=-halfWidth; n<halfWidth; n++)
					{
						FSCHAR* s = pCompMaskRow;

						for (int m=-halfWidth; m<halfWidth; m++, s++)
						{
							pHisto[*s]++;
						}

						pCompMaskRow += width;
					}

					// Find max index k in histo and assign this point, (i,j), of kth woolzObject mask to ON
					int woolzObjectIndex = -1;
					int maxCount = 0;
					for (int histoIndex = 0; histoIndex<woolzObjects->Count; histoIndex++)
					{
						if (pHisto[histoIndex] > maxCount)
						{
							maxCount = pHisto[histoIndex];
							woolzObjectIndex = histoIndex;
						}
					}

					SetObjectMaskON( woolzObjects, woolzObjectIndex, i, j);
				}
			}

			pCutMaskRow += width;
		}

		delete[] pHisto;

		System::Diagnostics::Debug::Write( "_cutPixelUnassigned = ");
		System::Diagnostics::Debug::WriteLine( _cutPixelUnassigned.ToString() );
	}

	void Woolz::SetObjectMaskON( List<WoolzObject^>^ woolzObjects, int woolzObjectIndex, int x, int y)
	{
		if (woolzObjectIndex >= 0 && woolzObjectIndex < woolzObjects->Count)
		{
			WoolzObject^ w = woolzObjects[woolzObjectIndex];

			int maskX = x - w->Bounds.X;
			int maskY = y - w->Bounds.Y;

			if (maskX >= 0 && maskX < w->Bounds.Width &&
				maskY >= 0 && maskY < w->Bounds.Height)
			{
				w->Mask->Pixels[ maskY*w->Bounds.Width + maskX ] = 0;
			}
			else
			{
				assert( false);		// mask X, Y out of range
			}
		}
		else
		{
			assert( woolzObjectIndex == -1 );		// woolzObjectIndex out of range
			_cutPixelUnassigned++;
		}
	}

	IEnumerable<WoolzObject^>^ Woolz::DetectObjects(Channel^ image, UInt16 threshold, int margin, int minArea)
	{
		return DetectObjects( image, threshold, margin, minArea, NULL);
	}

	List<WoolzObject^>^ Woolz::DetectObjects(Channel^ image, UInt16 threshold, int margin, int minArea, FSCHAR* compositeObjectMask)
	{
		List<WoolzObject^>^ objects = gcnew List<WoolzObject^>;

		FSCHAR* imageWoolz = ChannelToWoolz(image);
		
		int imageLen = image->Width * image->Height;
		FSCHAR* maskBuffer = new FSCHAR[imageLen];
		memset(maskBuffer, 255, imageLen);

		struct chromplist rawChromosome;
		int compositeIndex = 0;
		int numberOfObjects = woolz.FindObjects(imageWoolz, image->Width, image->Height, threshold, minArea);
		for(int i = 0; i < numberOfObjects; ++i)
		{
			WoolzMeasurements m;

			if(!woolz.MeasureObject(i, &m))
			{
				continue;
			}

			int w = m.BBRight - m.BBLeft;
			int h = m.BBBottom - m.BBTop;
			if (w * h < 200)
			{
				continue;
			}

			if (ChromosomeServices::GetChromMeasurements( (struct object *) woolz.GetObjectPtr(i), rawChromosome))
			{
				if (compositeObjectMask)
				{
					woolz.FillObject( i, compositeObjectMask, image->Width, image->Height, compositeIndex++);
				}

				woolz.FillObject( i, maskBuffer, image->Width, image->Height, 0);

				objects->Add(gcnew WoolzObject(maskBuffer, &m, gcnew ChromosomeMeasurements( rawChromosome), GetBoundary(i, image), image->Width, image->Height, margin));

				memset(maskBuffer, 255, imageLen);
			}
		}

		if (compositeObjectMask)
		{
			ReflectInXAxis( compositeObjectMask, image->Width, image->Height);
		}

		Free(imageWoolz);
		Free(maskBuffer);
		return objects;
	}

	PointCollection^ Woolz::GetBoundary(int i, Channel^ image)
	{
		PointCollection^ boundary = gcnew PointCollection;

		long* pBoundary = NULL;
		int vertexCount = woolz.MeasureBoundary( i, &pBoundary);
		if (vertexCount > 0)
		{
			long* pVertex=pBoundary;
			long ptX = 0;
			long ptY = 0;
			for (int i=0; i<vertexCount; i++)
			{
				ptX = *pVertex++;
				ptY = *pVertex++;

				boundary->Add( Point( ptX, image->Height - 1 - ptY));
			}
		}

		if (pBoundary != NULL)
		{
			delete[] pBoundary;
			pBoundary = NULL;
		}

		return boundary;
	}

	void Woolz::ReflectInXAxis( FSCHAR* image, int width, int height)
	{
		int i;
		FSCHAR * src;
		FSCHAR * dest;
		FSCHAR * temp = new FSCHAR[width];

		FSCHAR* row = image;
		FSCHAR* reflected = image + width*height - width;
		for (int j=0; j<height/2; j++)
		{
			src = row;
			dest = temp;
			for (i=0; i<width; i++)	{ *dest++ = *src++; }

			src = reflected;
			dest = row;
			for (i=0; i<width; i++)	{ *dest++ = *src++; }

			src = temp;
			dest = reflected;
			for (i=0; i<width; i++)	{ *dest++ = *src++; }

			row += width;
			reflected -= width;
		}

		delete[] temp;
	}

	void Woolz::ClassifyMetaphase( array<ChromosomeMeasurements^>^ chromosomes)
	{
		int chromCount = chromosomes->GetLength(0);
		ptrcplist* rawChromosomes = new ptrcplist[chromCount];

		// Extract raw measurements
		for (int i=0; i<chromCount; i++)
		{
			assert( chromosomes[i]);	// This chromosome has not been measured!
			rawChromosomes[i] = chromosomes[i]->GetRawChromosome();
		}

		ChromosomeServices::Classify( rawChromosomes, chromCount);

		// Store classifications in chromosomes
		for (int i=0; i<chromCount; i++)
		{
			if (rawChromosomes[i])
			{
				chromosomes[i]->SetClass( *rawChromosomes[i]);

				free( rawChromosomes[i]);
				rawChromosomes[i] = NULL;
			}
		}

		delete[] rawChromosomes;
	}

	FSCHAR* Woolz::ChannelToWoolz(Channel^ channel)
	{
		unsigned char* bits = new unsigned char[channel->Pixels->Length];
		pin_ptr<unsigned short> sixteenBit = &channel->Pixels[0];
		unsigned short* src = sixteenBit;
		unsigned short* srcEnd = sixteenBit + channel->Pixels->Length;
		unsigned char* dest = &bits[0];
		double maxPixel = pow(2.0, channel->BitsPerPixel);

		//ImageDebug::DumpImage( "D:\\src.raw", src, sizeof(unsigned short), channel->Pixels->Length);

		while(src != srcEnd)
		{
			*dest++ = 255 - (unsigned char) ((*src++ / maxPixel) * 256);
		}

		//ImageDebug::DumpImage( "D:\\dest.raw", bits, sizeof(unsigned char), channel->Pixels->Length);

		return bits;
	}

#pragma warning(disable : 4996)

	void Woolz::DumpWoolzImages( const char * szrootfilename, List<WoolzObject^>^ woolzObjects)
	{
		for (int i=0; i<woolzObjects->Count; i++)
		{
			char szFilename[256] = {0};
			sprintf( szFilename, "%s%02d.raw", szrootfilename, i);
			pin_ptr<unsigned short> sixteenBit = &woolzObjects[i]->Mask->Pixels[0];
			ImageDebug::DumpImage( szFilename, sixteenBit, sizeof(unsigned short), woolzObjects[i]->Mask->Pixels->Length);
		}
	}

}
