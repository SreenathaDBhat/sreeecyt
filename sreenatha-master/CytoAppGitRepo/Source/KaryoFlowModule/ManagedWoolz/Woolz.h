#pragma once

#include "ChromsomeMeasurements.h"

using namespace System;
using namespace System::Windows;
using namespace System::Windows::Media;
using namespace AI::Bitmap;
using namespace System::Collections::Generic;

namespace AI
{
	public ref class WoolzObject
	{
	private:
		Channel^ mask;
		Int32Rect bounds;
		double minRadii, maxRadii, orientation, length;
		ChromosomeMeasurements^ measurements;
		PointCollection^ outline;

	internal:
		WoolzObject(FSCHAR* maskData, WoolzMeasurements* m, ChromosomeMeasurements^ meas, PointCollection^ boundary, int imageWidth, int imageHeight, int margin);

	public:
		property Channel^ Mask
		{
			Channel^ get() { return mask; }
		}

		property Int32Rect Bounds
		{
			Int32Rect get() { return bounds; }
		}

		property double Orientation
		{
			double get() { return orientation; }
		}

		property double MaxRadius
		{
			double get() { return maxRadii; }
		}

		property double MinRadius
		{
			double get() { return minRadii; }
		}

		property double Length
		{
			double get() { return length; }
		}

		property ChromosomeMeasurements^ Measurements
		{
			ChromosomeMeasurements^ get() { return measurements; }
		}

		property PointCollection^ Outline
		{
			PointCollection^ get() { return outline; }
		}

	};



	public ref class Woolz
	{
	public:
		static IEnumerable<WoolzObject^>^ DetectObjects(Channel^ image, UInt16 threshold, int margin, int minArea);
		static IEnumerable<WoolzObject^>^ DetectSplitObjects(Channel^ image, Channel^ cutMask, UInt16 threshold, int margin, int minArea);
		static int CountObjects(Channel^ image, UInt16 threshold, int margin, int minArea);

		static void ClassifyMetaphase( array<ChromosomeMeasurements^>^ chromosomes);

		static FSCHAR* ChannelToWoolz(Channel^ channel);

	private:
		static int _cutPixelUnassigned;
		static List<WoolzObject^>^ DetectObjects(Channel^ image, UInt16 threshold, int margin, int minArea, FSCHAR* compositeObjectMask);
		static void RestoreCutPixels( Channel^ cutMask, FSCHAR* compositeMaskImage, int width, int height, List<WoolzObject^>^ woolzObjects);
		static void SetObjectMaskON( List<WoolzObject^>^ woolzObjects, int woolzObjectIndex, int x, int y);
		static void ReflectInXAxis( FSCHAR* image, int width, int height);
		static PointCollection^ GetBoundary(int i, Channel^ image);
		static void DumpWoolzImages( const char * szrootfilename, List<WoolzObject^>^ woolzObjects);
	};
}
