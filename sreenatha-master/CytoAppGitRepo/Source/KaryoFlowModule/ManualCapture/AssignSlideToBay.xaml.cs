﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Data;
using System.Linq;
using System.Collections.Generic;

namespace AI
{
    public partial class AssignSlideToBay : Window
    {
        public static RoutedUICommand AssignSlide = new RoutedUICommand("AssignSlide", "AssignSlide", typeof(AssignSlideToBay));
        public static RoutedUICommand CreateSlide = new RoutedUICommand("CreateSlide", "CreateSlide", typeof(AssignSlideToBay));
        private AssignSlideToBayController controller;
        
        public AssignSlideToBay(int bayNumber, IEnumerable<FishSlide> slidesOnStage)
        {
            controller = new AssignSlideToBayController(slidesOnStage);
            DataContext = controller;

            InitializeComponent();
            Title += " " + bayNumber;

            Loaded += (s, e) => Keyboard.Focus(txt);
            Closing += (s, e) => controller.Remember();
        }

        private void OnDragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        public Guid SelectedSlide
        {
            get { return controller.SelectedSlide.Id; }
        }

        public string SelectedSlideName
        {
            get { return controller.SelectedSlide.Name; }
        }

        public Case SelectedCase
        {
            get { return controller.SelectedCase; }
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void CanAssignExeceute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.CanOkExecute();
        }

        private void AssignSlideExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CanCreateSlideExeceute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.SelectedCase != null;
        }

        private void CreateSlideSlideExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            CreateSlideWindow wnd = new CreateSlideWindow(controller.SelectedCase);
            wnd.Owner = this;
            if (wnd.ShowDialog().GetValueOrDefault(false))
            {
                CaseManagement.CreateSlide(controller.SelectedCase, wnd.NewSlideName, wnd.NewSlideBarcode, SlideTypes.FISH);
                controller.RefreshSlides();
                controller.SelectSlide(wnd.NewSlideName);
                DialogResult = true;
            }
        }
    }

    public class AssignSlideToBayController : Notifier
    {
        private static string rememberedSearch = "";
        private static string rememberedCaseSelection = "";

        private IEnumerable<FishSlide> slidesOnStage;
        private Case selectedCase;
        private AssignableFishSlide selectedSlide;
        private CaseFinder finder;
        private IEnumerable<AssignableFishSlide> slides;

        public AssignSlideToBayController(IEnumerable<FishSlide> slidesOnStage)
        {
            this.slidesOnStage = slidesOnStage;
            finder = new CaseFinder();
            finder.Search = rememberedSearch;

            finder.PropertyChanged += (s, e) => SelectedCase = finder.Results.FirstOrDefault();
            RefreshSlides();
        }

        public class AssignableFishSlide
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public bool AlreadyOnTray { get; set; }
        }

        public CaseFinder CaseFinder
        {
            get { return finder; }
        }

        public Case SelectedCase
        {
            get { return selectedCase; }
            set { selectedCase = value; Notify("SelectedCase"); RefreshSlides(); }
        }

        public AssignableFishSlide SelectedSlide
        {
            get { return selectedSlide; }
            set { selectedSlide = value; Notify("SelectedSlide", "WhyOkIsDisabled"); }
        }

        public IEnumerable<AssignableFishSlide> Slides
        {
            get
            {
                return slides;
            }
        }

        private bool IsOnTray(Guid guid)
        {
            foreach (var s in slidesOnStage)
            {
                if (s.SlideId == guid)
                    return true;
            }

            return false;
        }

        public string WhyOkIsDisabled
        {
            get
            {
                if (selectedCase == null)
                    return "No case selected";
                if (selectedSlide == null)
                    return "No slide selected";
                if (selectedSlide.AlreadyOnTray)
                    return "Slide already loaded";
                
                return "";
            }
        }

        public bool CanOkExecute()
        {
            return selectedCase != null && selectedSlide != null && selectedSlide.AlreadyOnTray == false;
        }

        public void Remember()
        {
            rememberedSearch = finder.Search;
            if (SelectedCase != null)
                rememberedCaseSelection = SelectedCase.Id;
            else
                rememberedCaseSelection = "";
        }

        public void RefreshSlides()
        {
            if (selectedCase == null)
                return;

            var db = new Connect.Database();
            slides = (from x in db.Slides
                      where x.limsRef == selectedCase.Id
                      select new AssignableFishSlide
                      {
                          Id = x.slideId,
                          Name = Connect.LIMS.SlideName(x.slideId),
                          AlreadyOnTray = IsOnTray(x.slideId)
                      }).ToArray();


            Notify("Slides", "WhyOkIsDisabled");
        }

        public void SelectSlide(string slideName)
        {
            SelectedSlide = Slides.Where(s => s.Name == slideName).FirstOrDefault();
        }
    }
}
