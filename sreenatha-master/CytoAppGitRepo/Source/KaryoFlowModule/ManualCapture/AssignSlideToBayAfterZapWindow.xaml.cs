﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;

namespace AI
{
    public partial class AssignSlideToBayAfterZapWindow : INotifyPropertyChanged
    {
        private IEnumerable<FishSlide> bays;
        private SlideStatusInfo slideInfo;
        private BarcodeZapper zapReader;

        public event PropertyChangedEventHandler PropertyChanged;


        public AssignSlideToBayAfterZapWindow(SlideStatusInfo slideInfo, IEnumerable<FishSlide> slidesOnStage)
        {
            this.bays = slidesOnStage;
            this.slideInfo = slideInfo;
            DataContext = this;
            InitializeComponent();

            Loaded += (s, e) => 
            { 
                Keyboard.Focus(top); 
                top.Focus();
                
                zapReader = new BarcodeZapper();
                zapReader.Zap += AnotherBarcodeZapped;
            };
        }

        public IEnumerable<FishSlide> Bays
        {
            get { return bays; }
        }

        public SlideStatusInfo ZappedSlideInfo
        {
            get { return slideInfo; }
            private set 
            { 
                slideInfo = value; 
            
                if(PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ZappedSlideInfo")); 
            }
        }

        private void OnDragWindow(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void OnCancel(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = false;
        }

        public FishSlide SelectedBay { get; set; }

        private void OnSelectBay(object sender, RoutedEventArgs e)
        {
            SelectedBay = (FishSlide)((FrameworkElement)sender).Tag;
            DialogResult = true;
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            zapReader.CharacterRead(e);
        }

        private void AnotherBarcodeZapped(object sender, BarcodeZapEventArgs e)
        {
            var newSlide = SlideStatusInfo.FromBarcode(e.Barcode);
            if (newSlide == null)
                return;

            ZappedSlideInfo = newSlide;
        }
    }
}
