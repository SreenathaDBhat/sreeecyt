﻿using System;
using System.Collections.Generic;
using System.Linq;
using HW = AI.HardwareController;
using System.Threading.Tasks;
using System.Threading;
using AI.Bitmap;
using System.ComponentModel;
using System.Collections.Concurrent;

namespace AI
{
    public class AutoFocus
    {
        public event EventHandler<CancelEventArgs> CancelCheck;

        private Camera camera;
        private HW.HardwareController hw;

        public enum FocusInfo
        {
            SaturatedLowEnd,
            SaturatedHighEnd,
            NotSaturated
        };

        public class ZSnap
        {
            public double Z { get; set; }
            public Channel Image { get; set; }

            public double Low { get; set; }
            public double High { get; set; }
            public double Quality { get; set; }
            public FocusInfo Info { get; set; }
            public int SaturatedPixelCount { get; set; }

            public ZSnap()
            {
                Info = FocusInfo.NotSaturated;
            }
        }

        public AutoFocus(Camera camera, HW.HardwareController hw)
        {
            this.camera = camera;
            this.hw = hw;
        }

        private double GetSlideDelta(double Mag)
        {
            double DOF;
            double M = Math.Floor(Mag * 100.0) / 100.0;

            DOF = 4.0;
            if (M >= 1.00 && M < 5.00)
                DOF = 200.0 - 150.0 * (M - 1.0) / 4.0;

            if (M >= 5.00 && M < 10.00)
                DOF = 40.0 - 24.0 * (M - 5.0) / 5.0;

            if (M >= 10.00 && M < 12.00)
                DOF = 16.0 - 2.5 * (M - 10.0) / 2.0;

            if (M >= 12.00 && M < 16.00)
                DOF = 13.5 - 3.5 * (M - 12.0) / 4.0;

            if (M >= 16.00 && M < 20.00)
                DOF = 10.0 - 2.0 * (M - 16.0) / 4.0;

            if (M >= 20.00 && M < 40.00)
                DOF = 8.0 - 4.5 * (M - 20.0) / 20.0;

            if (M >= 40.00 && M < 60.00)
                DOF = 3.5 - 1.0 * (M - 40.0) / 20.0;

            if (M >= 60.00 && M < 63.00)
                DOF = 2.0 - 0.5 * (M - 60.0) / 3.0;

            if (M >= 63.00 && M < 100.00)
                DOF = 2.0 - 1.0 * (M - 63.0) / 37.0;

            if (M >= 100.00)
                DOF = 1.0;

            return DOF;
        }

        public double LongEnvelope(double startZ)
        {
            double sliceDelta = GetSlideDelta(hw.ObjectivePower);

            double coarse = Focus(startZ, sliceDelta * 2, 20);
            if (hw.ObjectivePower >= 40)
            {
                if (coarse <= 0)
                    return -1;

                return Focus(coarse, sliceDelta, 10);
            }

            return coarse;
        }

        public double Envelope(double startZ)
        {
            double sliceDelta = GetSlideDelta(hw.ObjectivePower);

            double coarse = Focus(startZ, sliceDelta * 2, 10);
            if (hw.ObjectivePower > 41)
            {
                if (coarse <= 0)
                    return -1;

                return Focus(coarse, sliceDelta, 10);
            }

            return coarse;
        }

        private double Focus(double hintZ, double slideDelta, int slices)
        {
            double z = hintZ + slideDelta * slices;
            double zEnd = hintZ - slideDelta * slices;

            BlockingCollection<ZSnap> stack = new BlockingCollection<ZSnap>();
            ConcurrentQueue<ZSnap> allSnaps = new ConcurrentQueue<ZSnap>();
            bool cancelled = false;

            Task.Factory.StartNew((o) =>
            {
                while (z > zEnd)
                {
                    if (CancelCheck != null)
                    {
                        CancelEventArgs e = new CancelEventArgs(false);
                        CancelCheck(this, e);
                        if (e.Cancel)
                        {
                            cancelled = true;
                            break;
                        }
                    }

                    hw.MoveStageToPosZ((float)z);
                    hw.MicWaitZ();
                    ushort[] snap = camera.Capture();

                    var zsnap = new ZSnap
                    {
                        Z = z,
                        Image = new Channel(camera.ImageWidth, camera.ImageHeight, camera.ImageBitsPerPixel, snap)
                    };

                    stack.Add(zsnap);
                    allSnaps.Enqueue(zsnap);

                    z -= slideDelta;
                }

                stack.CompleteAdding();
            }, null);

            if (cancelled)
                return -1;

            Parallel.ForEach(stack.GetConsumingEnumerable(), (snap) =>
            {
                MeasureImageForFocus.Measure(snap);
            });

            var best = DecideWhichIsBest(allSnaps, hintZ);
            return best;

        }

        private double DecideWhichIsBest(IEnumerable<ZSnap> all, double originalZ)
        {
            var imagesProcessed = all.ToArray();
            
            // now do something with the measurements
            double focus = originalZ;
            int FirstImage = -1, MaxImage = -1;
            double FirstPeak = 0.0;
            //double SecondPeak = 0.0;
            double MaxFocusMeas = 0.0;
            double MaxFocusZ = focus;
            int MaxSaturated = -1;
            int MinSaturated = imagesProcessed.Length + 1;
            int SaturationCount = 0;
            double threshold = 0;
            bool TooManySaturated = false;

	        // If we are not doing ultrafine focus then background subtract and find the peak
	        // else find the maximum 

        
	        Console.WriteLine("Type == Fine-----------------------------------------------------------------\r\n");

	        MaxFocusMeas = 0.0;

	        // Get max
	        for (int i = 0; i < imagesProcessed.Length; i++)
	        {
		        if (imagesProcessed[i].Quality > MaxFocusMeas && imagesProcessed[i].Quality > threshold)
		        {
			        MaxFocusMeas = imagesProcessed[i].Quality;
			        MaxImage = i;
			        MaxFocusZ = imagesProcessed[i].Z;
		        }
	        }

            for (int i = 0; i < imagesProcessed.Length; i++)
	        {
		        if (imagesProcessed[i].Quality == MaxFocusMeas && imagesProcessed[i].Info == FocusInfo.SaturatedHighEnd)
		        {
			        SaturationCount++;
			        if (i > MaxSaturated)
				        MaxSaturated = i;
			        if (i < MinSaturated)
				        MinSaturated = i;
		        }
	        }

	        if (MaxSaturated - MinSaturated > 1)
	        {
		        if (MaxSaturated - MinSaturated <= 5)
		        {
			        MaxImage = (MaxSaturated + MinSaturated) / 2;
			        MaxFocusZ = imagesProcessed[MaxImage].Z;
		        }
		        else
		        {
			        TooManySaturated = true;
		        }
	        }

	        // first check the measurements are any good given our threshold
	        // Look for a first peak position
	        FirstPeak = 0.0;
            for (int i = 1; i < imagesProcessed.Length - 1; i++)
	        {
		        if ((((imagesProcessed[i].Quality > imagesProcessed[i - 1].Quality &&
			        imagesProcessed[i].Quality > imagesProcessed[i + 1].Quality) || 
			        (MaxFocusMeas == imagesProcessed[i].Quality))) && 
			        (imagesProcessed[i].Quality > FirstPeak) && 
			        (imagesProcessed[i].Quality > threshold))
		        {
			        FirstPeak = imagesProcessed[i].Quality;
			        FirstImage = i;
			        focus = imagesProcessed[i].Z;
			        break;
		        }
	        }	

	        FirstPeak = MaxFocusMeas;
	        FirstImage = MaxImage;
	        focus = MaxFocusZ;

	        OutputDebugInfo(imagesProcessed, FirstImage, -1, MaxFocusMeas, MaxImage);

	        if (MaxFocusMeas > 10)
	        {
		        //if (FirstImage <= 1 && imagesProcessed[FirstImage + 1].Quality < MaxFocusMeas)
			        //FocusStatus = AtBottomOfRange;
		        //if (FirstImage >= info.imagesProcessed - 2 && imagesProcessed[FirstImage - 1].Quality < MaxFocusMeas)
			      //  FocusStatus = AtTopOfRange;
	        }

        	

	        // Have we gone past the threshold if not then fail
	        if  (FirstPeak < threshold)
	        {
		        Console.WriteLine("FOCUS FAIL THRESH = {0:0.00} MEASUREMENT = {1}", threshold, FirstPeak);
		        return -1;
	        }

	        if (TooManySaturated)
	        {
		        Console.WriteLine("FOCUS FAILED TOO MUCH SATURATION");
                return -2;
	        }

            Console.WriteLine("Z = {0:0.000}");
	        Console.WriteLine("ENVELOPE FOCUS END===========================================================================");

	        return focus;
        }

        void OutputDebugInfo(ZSnap[] info, int FirstImage, int SecondImage, double MaxFocusMeas, int MaxImage)
        {
	        Console.WriteLine("BACKGROUND SUBTRACTED-----------------------------------------------------------------");
	        string dbg;

	        for (int n = 0; n < info.Length; n++)
	        {
                dbg = String.Format("{0:0.000}, {1:0.000}, L={2}, H={3}, S={4}", 
                    info[n].Z, 
                    info[n].Quality,
                    info[n].Low,
                    info[n].High,
                    info[n].SaturatedPixelCount);
    			
		        if (info[n].Quality == MaxFocusMeas)
			        dbg += " <<<< MAX";

		        if (n == MaxImage)
			        dbg += " <<<< FOCUS";

		        if (n == FirstImage)
			        dbg += " <<<< FIRST PEAK";

		        if (n == SecondImage)
			        dbg += " <<<< SECOND PEAK";

		        Console.WriteLine(dbg);
	        }
	        Console.WriteLine("END BACKGROUND SUBTRACTED-------------------------------------------------------------");
        }
    }





    public static unsafe class MeasureImageForFocus
    {
        public static void Measure(AutoFocus.ZSnap snap)
        {
            ImageQuantiles16(snap);

            double maxContrast = Math.Pow(2, snap.Image.BitsPerPixel);
            snap.Quality = (255.0 * (snap.High - snap.Low)) / maxContrast;
        }


        private static void ImageQuantiles16(AutoFocus.ZSnap snap)
        {
            int[] pHisto = Histogram16(snap);
            double PCBelow = 0.01;
            double PCAbove = 0.01;
            int ngreys = (int)Math.Pow(2, snap.Image.BitsPerPixel);
		    
            // Work out the number of pixels constituting PCBelow% of the total
            double npix = snap.Image.Width * snap.Image.Height;
            double pc, totsofar;
            pc = npix * PCBelow / 100.0;

            // Now calc the level at which 5% of the pixels are below
            // and the level at which 95% of the pixels are below
            totsofar = 0.0;
            for (int i = 0; i < ngreys; i++)
            {
                totsofar += pHisto[i];
                if (totsofar > pc)
                {
                    snap.Low = i;
                    break;
                }
            }

            // Work out the number of pixels constituting (100 - PCAbove)% of the total
            pc = npix * PCAbove / 100.0;

	        totsofar = 0.0;
            for (int i = ngreys - 1; i >= 0; i--)
            {
                totsofar += pHisto[i];
                if (totsofar > pc)
                {
                    snap.High = i;
                    break;
                }
            }

            if (snap.Low == 0)
                snap.Info = AutoFocus.FocusInfo.SaturatedLowEnd;
            else if (snap.High == ngreys - 1)
                snap.Info = AutoFocus.FocusInfo.SaturatedHighEnd;
        }

        private static int[] Histogram16(AutoFocus.ZSnap snap)
        {
 	        fixed(ushort* pixelsPtr = &snap.Image.Pixels[0])
            {
                ushort* p = pixelsPtr;
                ushort* end = pixelsPtr + (snap.Image.Width * snap.Image.Height);
                int greys = (int)Math.Pow(2, snap.Image.BitsPerPixel);
                int[] histo = new int[greys];

                while(p != end)
                {
                    ushort v = *p++;
                    histo[v]++;
                    if (v == greys - 1)
                        snap.SaturatedPixelCount++;
                }

                return histo;
            }
        }
    }
}

