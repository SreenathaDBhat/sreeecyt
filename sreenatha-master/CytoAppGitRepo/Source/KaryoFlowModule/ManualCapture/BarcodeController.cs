﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace AI
{
    public class BarcodeController : Notifier
    {
        protected BarcodeZapper zapper = new BarcodeZapper();
        protected string zappedBarcode = string.Empty;

        public BarcodeController()
        {
            zapper.Zap += new EventHandler<BarcodeZapEventArgs>(zapper_Zap);
        }

        protected virtual void zapper_Zap(object sender, BarcodeZapEventArgs e)
        {
            zappedBarcode = e.Barcode;
        }

        public void CharacterRead(KeyEventArgs e)
        {
            zapper.CharacterRead(e);
        }
    }

}
