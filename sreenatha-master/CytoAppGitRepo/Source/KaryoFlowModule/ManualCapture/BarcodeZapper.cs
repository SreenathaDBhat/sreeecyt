﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace AI
{
    public class BarcodeZapper
    {
        public event EventHandler<BarcodeZapEventArgs> Zap;

        string barcodeString = "";
        bool recordingBarcode;
        Key startSignal;
        Key stopSignal;

        public BarcodeZapper()
        {
            recordingBarcode = false;
            startSignal = Key.F23;
            stopSignal = Key.F24;
        }

        public void CharacterRead(KeyEventArgs keyPress)
        {
            Key key = keyPress.Key;
            if (!recordingBarcode && key != startSignal)
                return;

            if (key == startSignal)
            {
                recordingBarcode = true;
                barcodeString = "";
                return;
            }

            if (key == stopSignal && recordingBarcode)
            {
                recordingBarcode = false;
                OnZap(barcodeString);
                return;
            }

            if (recordingBarcode)
            {
                char c = 'Z';
                if (KeyToChar(key, ref c))
                    barcodeString += c;
            }
        }

        private bool KeyToChar(Key key, ref char c)
        {
            bool returnval = false;
            if (key >= Key.D0 && key <= Key.D9)
            {
                c = (char)(((int)'0') + (key - Key.D0));
                return true;
            }
            if (key >= Key.A && key <= Key.Z)
            {
                c = (char)(((int)'A') + (key - Key.A));
                return true;
            }

            switch (key)
            {
                case Key.Multiply:
                    c = (char)'*';
                    returnval = true;
                    break;
                case Key.Add:
                    c = (char)'+';
                    returnval = true;
                    break;
                case Key.Subtract:
                    c = (char)'-';
                    returnval = true;
                    break;
                case Key.OemSemicolon:
                    c = (char)';';
                    returnval = true;
                    break;
                case Key.OemMinus:
                    c = (char)'-';
                    returnval = true;
                    break;
                case Key.OemPeriod:
                    c = (char)'.';
                    returnval = true;
                    break;
                case Key.OemQuestion:
                    c = (char)'?';
                    returnval = true;
                    break;
                case Key.OemOpenBrackets:
                    c = (char)'(';
                    returnval = true;
                    break;
                case Key.OemComma:
                    c = (char)',';
                    returnval = true;
                    break;
                case Key.OemCloseBrackets:
                    c = (char)')';
                    returnval = true;
                    break;
                case Key.OemQuotes:
                    c = (char)'"';
                    returnval = true;
                    break;
                case Key.OemBackslash:
                    c = (char)'\\';
                    returnval = true;
                    break;
                default:
                    returnval = false;
                    break;
            }


            return returnval;
        }

        private void OnZap(string barcode)
        {
            if (Zap != null)
            {
                Zap(this, new BarcodeZapEventArgs(barcode));
            }
        }
    }

    public class BarcodeZapEventArgs : EventArgs
    {
        private string barcode;

        public BarcodeZapEventArgs(string barcode)
        {
            this.barcode = barcode;
        }

        public string Barcode
        {
            get { return barcode; }
        }
    }

}
