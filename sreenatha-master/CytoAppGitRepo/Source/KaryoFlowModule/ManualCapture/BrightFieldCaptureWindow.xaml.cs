﻿using System;
using System.Windows;
using System.Windows.Interop;
using System.Diagnostics;
using System.Threading;
using System.Linq;
using System.Windows.Media;
using System.Windows.Threading;
using Microsoft.Win32;
using System.IO;
using System.Windows.Controls;
using System.Reflection;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows.Navigation;
using AI.Security;

namespace AI
{
    public partial class BrightFieldCaptureWindow : UserControl, IDisposable
    {
        private bool disposed = false;

        private AI.HardwareController.HardwareController hardwareController = null;
        private CaptureController controller = null;
        private bool hardwareFailure = false;
        private AI.HardwareController.HotKeyStageData stagedata = null;
        private AI.HardwareController.HardwareStatus hs = null;
        private string userID = Environment.UserName;
        private Guid caseId;
        private DateTime? roiLmbDownHack;
        private static LampGaugeViewModel lgvm;
        private bool canDeleteCapturedCell = false;

        public BrightFieldCaptureWindow(Guid caseId, HardwareController.HardwareController hc, Camera cam)
        {
			try
			{
				this.caseId = caseId;
				this.hardwareController = hc;
				controller = new CaptureController(Dispatcher, hardwareController, cam, caseId, true);
				InitializeComponent();

				Loaded += (s, e) =>
				{
					hardwareController.ApplyObjectiveOffsets(true);

					USBJoystick.LaunchProcess();

					LiveCameraWindow live = (LiveCameraWindow)FindResource("renderer");
					live.HookLiveImage(controller.Camera);

					controller.MoveToBrightfieldPositions();
					controller.Camera.SetExposure(40);

					lgvm = new LampGaugeViewModel(hardwareController, controller.Camera, Dispatcher);
					lgvm.StartContinuous();

					Dispatcher.Invoke((ThreadStart)delegate
					{
						DataContext = controller;
						LampGauge.DataContext = lgvm;

						ConfigureOptions();
						StartHotkeys();
					}, DispatcherPriority.Normal);
				};

				Unloaded += (s, e) =>
				{
					StopHotkeys();

					if (cam != null)
						cam.StopLive();

					if (lgvm != null)
					{
						lgvm.EndContinuous();
						lgvm.Dispose();
						lgvm = null;
					}
				};
			}
			catch (Exception ex)
			{
				ExceptionUtility.ShowLog(ex, "BrightFieldCaptureWindow");
			}
        }

        public CaptureController CaptureController
        {
            get { return controller; }
        }

        public bool HardwareFailure
        {
            get { return hardwareFailure; }
        }

        private void OnAbortConnection(object sender, RoutedEventArgs e)
        {
            UIStack.For(this).Pop(UIStackResult.Cancel);
        }

        private void StartHotkeys()
        {
            IntPtr handle = new System.Windows.Interop.WindowInteropHelper(Application.Current.MainWindow).Handle;
            int err = AI.HardwareController.HotKeys.Start(handle, true, hardwareController, true, true);
            if (err == 0)
            {
                hs = AI.HardwareController.HotKeys.StatusNotifier;

                if (hs != null)
                    hs.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(hs_PropertyChanged);
            }
            else
                MessageBox.Show("Error starting Hot Keys. " + AI.HardwareController.HotKeys.LastError());
        }

        private void StopHotkeys()
        {
            if (stagedata != null)
            {
                stagedata.PreventClosing(false);
                stagedata.Close();
                stagedata = null;
            }

            if (hs != null)
            {
                hs.PropertyChanged -= hs_PropertyChanged;
                hs.Dispose();
            }

            AI.HardwareController.HotKeys.Stop();
        }


        private void ConfigureOptions()
        {
        }

        void hs_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "ShowStatusWindow":
                    Dispatcher.Invoke((ThreadStart)delegate
                    {
                        if (hs.ShowStatusWindow)
                        {
                            if (stagedata == null)
                            {
                                stagedata = new AI.HardwareController.HotKeyStageData(hs, null);
                                stagedata.Owner = VisualTreeWalker.FindParentOfType<Window>(this);
                                stagedata.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                                stagedata.Show();
                            }

                            IntPtr handle = new System.Windows.Interop.WindowInteropHelper(stagedata).Handle;
                            AI.HardwareController.HotKeys.RegisterTempWindow(handle);
                            Application.Current.MainWindow.Focus();
                        }
                        else
                        {
                            AI.HardwareController.HotKeys.UnRegisterTempWindow();
                            Application.Current.MainWindow.Focus();

                            if (stagedata != null)
                            {
                                stagedata.PreventClosing(false);
                                stagedata.Close();
                                stagedata = null;
                            }
                        }

                    }, DispatcherPriority.Normal);
                    break;
            }
        }

        private void CanCaptureExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller != null && controller.ReadyToCaptureBrightfield;
        }

        private void CaptureExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Capture(CaptureOptions.Default);
        }

        private void Capture(CaptureOptions options)
        {
            LiveCameraWindow live = (LiveCameraWindow)FindResource("renderer");
            live.ClearGhost();
            host.Visibility = Visibility.Hidden;

            capturingMessage.Visibility = Visibility.Visible;
            bool fuse = (options & CaptureOptions.Fuse) == CaptureOptions.Fuse;

            ThreadPool.QueueUserWorkItem((WaitCallback)delegate
            {
                ImageFrame img = controller.BrightfieldCapture(options);

                Dispatcher.Invoke((ThreadStart)delegate
                {
                    if(fuse)
                        controller.AddFuseImage(img);
                    else
                        controller.AddCapturedImage(img);

                    capturingMessage.Visibility = Visibility.Collapsed;
                    host.Visibility = Visibility.Visible;

                }, DispatcherPriority.Normal);

            }, DispatcherPriority.Normal);
        }

        private void CanCaptureFuseExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller != null && controller.ReadyToCaptureBrightfield;
        }

        private void CaptureFuseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Capture(CaptureOptions.Default | CaptureOptions.Fuse);
        }

        private void MakeCurrent(object sender, RoutedEventArgs e)
        {
            ChannelInfo fl = (ChannelInfo)((FrameworkElement)sender).Tag;
            CaptureController.MakeCurrent(fl);
        }

        private void OnDeleteCapturedImage(object sender, RoutedEventArgs e)
        {
			try
			{
				if (canDeleteCapturedCell)
				{
					CaptureFrame image = (CaptureFrame)((FrameworkElement)sender).Tag;

					if (Security.Security.IsLoggingEnabled)
					{
						string imageIdentifier = controller.Case.Name + "/" + controller.Slide.Name + "/" + image.FrameId.ToString();
						Security.Security.AddLogEntry(Logging.EventType.ImageDeleted, controller.Case.Id, "", imageIdentifier);
					}

					CaptureController.KillCapturedImage(image);
				}

				if (CaptureController.CurrentCapturedImage == null)
					capturedCellsPopup.IsOpen = false;
			}
			catch (Exception ex)
			{
				ExceptionUtility.ShowLog(ex, "Delete Image");
			}
        }

        private void OnDeleteFuseImage(object sender, RoutedEventArgs e)
        {
			try
			{
				CaptureFrame image = (CaptureFrame)((FrameworkElement)sender).Tag;
				CaptureController.KillFuseImage(image, controller.CurrentCapturedImage);
			}
			catch (Exception ex)
			{
				ExceptionUtility.ShowLog(ex, "Delete Fuse Image");
			}
        }

        private void OnStackChange(object sender, RoutedEventArgs e)
        {
            controller.StacksChanged();
        }

        private void OpenFromCommandLine(object sender, ExecutedRoutedEventArgs e)
        {
            foreach (string arg in Environment.GetCommandLineArgs())
            {
                Console.WriteLine(arg);
            }
        }

        private void CanNewSlideExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller != null && controller.Case != null;
        }

        private void NewSlideExecuted(object sender, ExecutedRoutedEventArgs e)
        {
			try
			{
				controller.CreateNewSlide();

				if (Security.Security.IsLoggingEnabled)
				{
					string slideIdentifier = controller.Case.Name + "/" + controller.Slide.Name;
					Security.Security.AddLogEntry(Logging.EventType.SlideAdded, controller.Case.Id, "", slideIdentifier);
				}
			}
			catch (Exception ex)
			{
				ExceptionUtility.ShowLog(ex, "New Slide");
			}
        }

        private void OnLoadSlide(object sender, RoutedEventArgs e)
        {
            //int numberofslidespertray = 0;
            //int numberoftrays = 0;
            //Boolean[] trayPresent;
            //hardwareController.GetNumberOfTraysAndSlidesPerTray(ref numberoftrays, ref numberofslidespertray);

            ////  Get occupied tray positions
            //if (numberoftrays > 1)
            //{
            //    trayPresent = new Boolean[numberoftrays];
            //    hardwareController.GetOccupiedTrayPositions(numberoftrays, ref trayPresent);
            //}

            //LoadSlideWindow slide = new LoadSlideWindow(numberoftrays, numberofslidespertray);

            LoadSlideWindow slide = new LoadSlideWindow(hardwareController);

            slide.Owner = VisualTreeWalker.FindParentOfType<Window>(this);

            if (slide.ShowDialog().Value)
            {
                int slidenumber = slide.SelectedSlideIndex;
                hardwareController.LoadSlide_N(slidenumber);
            }
        }

        private void OnUnloadSlide(object sender, RoutedEventArgs e)
        {
            hardwareController.UnloadSlide_N(0);
        }

        private void OnRoiDropLeftMouseDown(object sender, MouseButtonEventArgs e)
        {
            roiLmbDownHack = DateTime.Now;

        }

        private void OnRoiDropped(object sender, MouseButtonEventArgs e)
        {
            if (roiLmbDownHack == null || DateTime.Now.Subtract(roiLmbDownHack.Value).TotalSeconds > 1)
                return;

            Point p = e.GetPosition((IInputElement)sender);
            roiEllipseTransform.X = p.X;
            roiEllipseTransform.Y = p.Y;
            roiBlackCircleTransform.X = p.X;
            roiBlackCircleTransform.Y = p.Y;
            controller.AutoCameraSetupViaRoi(p);
        }

        private void OnRoiCancel(object sender, MouseButtonEventArgs e)
        {
            controller.Camera.CancelAutoExpose();
            controller.ShowRoi = false;
        }

        private void LightMeter_Click(object sender, RoutedEventArgs e)
        {
            if (lgvm != null)
            {
                lgvm.EndContinuous();
                controller.Camera.SetExposure(40);
                lgvm.StartContinuous();
            }

            controller.ShowRoi = false;

        }


        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    DisposeManagedResources();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.
                DisposeUnManagedResources();

                // Note disposing has been done.
                disposed = true;
            }
        }

        private void DisposeManagedResources()
        {
            StopHotkeys();

            if (lgvm != null)
            {
                lgvm.EndContinuous();
                lgvm.Dispose();
                lgvm = null;
            }

            
            if (controller != null)
            {
                controller.Camera.StopLive();
                controller.Camera.Shutdown();
                controller.Dispose();
                controller = null;
            }
        }

        private void DisposeUnManagedResources()
        {
            
        }

        ~BrightFieldCaptureWindow()
        {
            Dispose(false);
        }

        #endregion

        private void OnBack(object sender, RoutedEventArgs e)
        {
            UIStack.For(this).Pop(UIStackResult.Cancel);
        }
    }
}
