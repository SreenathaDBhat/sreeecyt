﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Threading;
using System.Windows;
using AI.Bitmap;
using System.Threading;
using System.Windows.Media.Media3D;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Threading.Tasks;

namespace AI
{
    public class CaptureStackingParameters
    {
        public enum StackingMode
        {
            FullStack,
            MaximumProjection
        }

        public StackingMode Mode { get; set; }

        /// <summary>
        /// Number of images taken on the Z axis
        /// </summary>
        public int Slices { get; set; }

        /// <summary>
        /// Distance (in microns) between the z slices.
        /// </summary>
        public double Gap { get; set; }


        public CaptureStackingParameters()
        {
            Mode = StackingMode.MaximumProjection;
            Slices = 1;
            Gap = 10;
        }
    }

    public class CaptureAutoSetupParameters
    {
        public Point AutoSetupCenter { get; set; }
    }

    internal class ScalingDistances
    {
        private Size scale;

        public static ScalingDistances FromObjective(int objective, Size cameraPixelSize)
        {
            return new ScalingDistances(objective, cameraPixelSize);
        }

        private ScalingDistances(int objective, Size cameraPixelSize)
        {
            scale = MicroscopeFile.ImageScale(objective);
            FrameSizePixels = cameraPixelSize;
        }

        /// <summary>
        /// Size of a frame in pixels
        /// </summary>
        public Size FrameSizePixels { get; private set; }

        /// <summary>
        /// Microns
        /// </summary>
        public double FrameWidth { get { return FrameSizePixels.Width * scale.Width; } }

        /// <summary>
        /// Microns
        /// </summary>
        public double FrameHeight { get { return FrameSizePixels.Width * scale.Width; } }

        /// <summary>
        /// Microns
        /// </summary>
        public Size FrameSize { get { return new Size(FrameWidth, FrameHeight); } }
    }

    
    public class Capture : Notifier
    {
        public delegate void ChannelSnappedDelegate(CaptureQueue parentQueue, SnapRecord snap);
        public delegate void ImageCapturedDelegate(CaptureQueue parentQueue, FrameRecord frame);
        public delegate void CaptureCompletedDelegate(CaptureQueue parentQueue, bool cacelled);

        private HardwareController.HardwareController hw;
        private Camera camera;
        private int progressCurrent, progressTarget;
        private Dispatcher dispatcher;
        private int cancelled;
        private CaptureQueue queue;
        private CaptureRecord record;
        private IEnumerable<CaptureRegion> specificRegions;

        public Capture(CaptureQueue queue, HardwareController.HardwareController hw, Camera camera, Dispatcher dispatcher)
        {
            this.record = new CaptureRecord();
            this.queue = queue;
            this.hw = hw;
            this.camera = camera;

            this.dispatcher = dispatcher;
            StackingParameters = new CaptureStackingParameters();
            progressTarget = 1;
        }


        public CaptureRecord Record { get { return record; } }
        public CaptureStackingParameters StackingParameters { get; set; }
        public CaptureAutoSetupParameters AutoSetupParameters { get; set; }
        public int Objective { get; set; }
        public Fluorochrome Fluorochrome { get; set; }

        // Notififcations
        public ImageCapturedDelegate ImageCaptured { get; set; }
        public ChannelSnappedDelegate ChannelSnapped { get; set; }
        public CaptureCompletedDelegate Completed { get; set; }

        public IEnumerable<CaptureRegion> RegionsToCapture
        {
            get { return specificRegions == null ? queue.CaptureRegions : specificRegions; }
            set { specificRegions = value; }
        }

        public double Progress
        {
            get 
            {
                return progressCurrent / (double)progressTarget;
            }
        }

        public void Go(int startingSlideIndex)
        {
            if (!TryReallyHardToGetLock())
                throw new ApplicationException("Could not get mic lock");

            try
            {
                var regionsToCapture = RegionsToCapture;

                var firstFrame = regionsToCapture.First();
                var scales = ScalingDistances.FromObjective(Objective, camera.ImageSize);
                int slideLoaderSlideIndex = startingSlideIndex;
                SwitchBays(ref slideLoaderSlideIndex, firstFrame.SlideIndex);
                double rootZ = hw.StagePosition.Z;

                hw.ApplyObjectiveOffsets(true);

                int totalNumFrames = 0;
                foreach (var r in regionsToCapture)
                {
                    r.GenerateFrames(camera.ImageSize, rootZ, Objective);
                    totalNumFrames += r.Frames.Count();
                }

                int numFluorChannels = Fluorochrome.Channels.Count();
                progressTarget = numFluorChannels * totalNumFrames;

                dispatcher.Invoke((ThreadStart)delegate
                {
                    Notify("Progress");
                }, DispatcherPriority.Normal);


                var distinctSlides = (from f in regionsToCapture select f.Slide).Distinct();
                hw.MoveToObjective(Objective - 1, true);

                foreach (var slide in distinctSlides)
                {
                    var regionsForThisSlide = from r in regionsToCapture
                                              where r.Slide == slide
                                              select r;

                    SwitchBays(ref slideLoaderSlideIndex, regionsForThisSlide.First().SlideIndex);

                    foreach (var regionInfo in regionsForThisSlide)
                    {
                        double zToUse = rootZ;
                        double focusZ = -1;

                        if (regionInfo.ShouldAutoFocus)
                        {
                            SetStateString("Focussing...");

                            AutoFocus focus = new AutoFocus(camera, hw);
                            focus.CancelCheck += (s, e) => e.Cancel = cancelled != 0;

                            MoveToCounterstain();

                            hw.MoveStageToPosX(regionInfo.RegionCenter.X);
                            hw.MoveStageToPosY(regionInfo.RegionCenter.Y);
                            hw.WaitAxisStationary();
                            focusZ = focus.LongEnvelope(focusZ < 0 ? hw.StagePosition.Z : focusZ);

                            if (focusZ < 0)
                            {
                                focusZ = hw.StagePosition.Z;
                                regionInfo.Status = CaptureStatus.FailedFocus;
                                regionInfo.Modified = DateTime.Now;
                                continue;
                            }

                            hw.MoveStageToPosZ((float)focusZ);
                            hw.WaitAxisStationary();
                            zToUse = focusZ;
                        }

                        foreach (var framePosition in regionInfo.Frames)
                        {
                            hw.MoveStageTo(framePosition);
                            hw.WaitAxisStationary();

                            if (RespondToCancel(regionInfo))
                                return;

                            ImageFrame image = new ImageFrame();
                            image.ImageBitsPerPixel = camera.ImageBitsPerPixel;
                            image.ImageWidth = camera.ImageWidth;
                            image.ImageHeight = camera.ImageHeight;
                            image.Position = new Point(framePosition.X, framePosition.Y);
                            image.SlideLocation = new SlidePoint(framePosition, "EF NYI");

                            foreach (var channel in Fluorochrome.Channels)
                            {
                                SetStateString(channel.DisplayName);

                                if (channel.Dichroic != null)
                                    hw.MoveToDichroic(channel.Dichroic.RotorPosition - 1, true);
                                if (channel.Excitation != null)
                                    hw.MoveToExcitation(channel.Excitation.RotorPosition - 1, true);

                                camera.SetExposure(channel.Exposure);

                                if (AutoSetupParameters != null)
                                {
                                    hw.MoveStageToPosZ((float)zToUse);
                                    var pt = AutoSetupParameters.AutoSetupCenter;
                                    camera.AutoExpose(pt.X, pt.Y, 100, false);
                                    channel.Exposure = camera.GetExposure();
                                }

                                if (!CaptureChannel(regionInfo, channel, image, zToUse))
                                {
                                    return; // cancelled
                                }
                                else
                                {
                                    IncrementProgress();
                                }
                            }

                            image.Thumbnail = ThumbnailHistograms.Compose(image);
                            image.Id = Guid.NewGuid();

                            regionInfo.Status = CaptureStatus.Captured;
                            regionInfo.Modified = DateTime.Now;

                            var r = record.AddFrame(regionInfo, image);

                            if (ImageCaptured != null)
                                ImageCaptured(queue, r);

                            IncrementProgress();
                        }
                    }
                }

                if (Completed != null)
                    Completed(queue, false);
            }
            finally
            {
                hw.SetMicLock(false);
            }
        }

        private void MoveToCounterstain()
        {
            var coutnerstain = Fluorochrome.Channels.Where(c => c.IsCounterstain).FirstOrDefault();
            if (coutnerstain != null)
            {
                if (coutnerstain.Dichroic != null)
                    hw.MoveToDichroic(coutnerstain.Dichroic.RotorPosition - 1, true);
                if (coutnerstain.Excitation != null)
                    hw.MoveToExcitation(coutnerstain.Excitation.RotorPosition - 1, true);
            }
        }

        private bool TryReallyHardToGetLock()
        {
            DateTime start = DateTime.Now;

            while ((DateTime.Now - start).TotalSeconds < 5)
            {
                if (hw.SetMicLock(true) == 0)
                    return true;

                Thread.Sleep(100);
            }

            return false;
        }

        private bool CaptureChannel(CaptureRegion regionInfo, ChannelInfo channel, ImageFrame image, double focalZ)
        {
            List<Snap> stack = new List<Snap>();
            var stacking = StackingParameters;

            if (channel.IsStacked && stacking != null && !channel.IsCounterstain)
            {
                if (RespondToCancel(regionInfo))
                    return false;

                double stackGapMicrons = stacking.Gap;
                int stackSize = stacking.Slices;
                double z = focalZ - stackGapMicrons * (stackSize / 2.0f);

                for (int i = 0; i < stackSize; ++i)
                {
                    hw.MoveStageToPosZ((float)z);
                    hw.MicWaitZ();
                    z += stackGapMicrons;

                    Channel cap = Snap();
                    var snap = CreateSnap(cap, channel, i);
                    
                    stack.Add(snap);
                }

                int w = camera.ImageWidth;
                int h = camera.ImageHeight;
                int bpp = camera.ImageBitsPerPixel;

                Snap maximumProjection = new Snap
                {
                    ChannelInfo = channel,
                    ChannelPixels = new Channel(w, h, bpp, MaxProjection(stack)),
                    Id = Guid.NewGuid(),
                    IsProjection = true,
                    StackIndex = AI.Snap.MaximumProjectionZIndex
                };
                image.AddSnap(maximumProjection);

                var mpr = record.AddSnap(regionInfo, maximumProjection);
                if (ChannelSnapped != null)
                    ChannelSnapped(queue, mpr);
               
                if(stacking.Mode == CaptureStackingParameters.StackingMode.FullStack)
                {
                    foreach (var s in stack)
                    {
                        var sr = record.AddSnap(regionInfo, s);

                        if (ChannelSnapped != null)
                            ChannelSnapped(queue, sr);
                        
                        image.AddSnap(s);
                    }
                }
            }
            else
            {
                if (RespondToCancel(regionInfo))
                    return false;

                Channel cap = Snap();
                var capSnap = CreateSnap(cap, channel, AI.Snap.MaximumProjectionZIndex);
                image.AddSnap(capSnap);

                var sr = record.AddSnap(regionInfo, capSnap);
                if (ChannelSnapped != null)
                    ChannelSnapped(queue, sr);
            }

            return true;
        }

        private bool RespondToCancel(CaptureRegion region)
        {
            if (cancelled == 1)
            {
                region.Status = CaptureStatus.Cancelled;

                if(Completed != null)
                    Completed(queue, true);

                return true;
            }

            return false;
        }

        private void IncrementProgress()
        {
            dispatcher.Invoke((ThreadStart)delegate
            {
                progressCurrent++;
                Notify("Progress");

            }, DispatcherPriority.Normal);
        }

        private Channel Snap()
        {
            ushort[] pixels = camera.Capture();
            return new Channel(camera.ImageWidth, camera.ImageHeight, camera.ImageBitsPerPixel, pixels);
        }

        private static Snap GenerateMaximumProjection(List<Snap> snaps)
        {
            var s = snaps[0];

            Snap projection = new Snap();
            projection.Id = Guid.NewGuid();
            projection.ChannelInfo = s.ChannelInfo;
            projection.IsProjection = true;
            projection.StackIndex = AI.Snap.MaximumProjectionZIndex;
            projection.ChannelPixels = new Channel(s.ChannelPixels.Width, s.ChannelPixels.Height, s.ChannelPixels.BitsPerPixel, MaxProjection(snaps));
            return projection;
        }

        private static unsafe ushort[] MaxProjection(IList<Snap> snaps)
        {
            ushort[] MaxProj;

            if (snaps.Count > 0)
            {
                int Size = snaps[0].ChannelPixels.Width * snaps[0].ChannelPixels.Height;
                int chunks = Environment.ProcessorCount;
                int splitLen = Size / chunks;

                MaxProj = new ushort[Size];
                Parallel.For(0, chunks, i =>
                {
                    for (int n = 0; n < splitLen; n++)
                    {
                        ushort Max = 0;
                        foreach (var snap in snaps)
                        {
                            if (snap.ChannelPixels.Pixels[n + i * splitLen] > Max)
                                Max = snap.ChannelPixels.Pixels[n + i * splitLen];
                        }
                        MaxProj[n + i * splitLen] = Max;
                    }
                });

                return MaxProj;
            }

            return null;
        }

        private Snap CreateSnap(Channel cap, ChannelInfo channel, int z)
        {
            return new Snap()
            {
                Id = Guid.NewGuid(),
                ChannelInfo = channel,
                ChannelPixels = cap,
                StackIndex = z,
            };            
        }

        private void SwitchBays(ref int slideLoaderSlideIndex, int newIndex)
        {
            if (newIndex != -1 && slideLoaderSlideIndex != newIndex)
            {
                slideLoaderSlideIndex = newIndex;
                hw.LoadSlide_N(slideLoaderSlideIndex);
                hw.WaitOnSlideLoader();
            }
        }

        public string StateString { get; set; }
        
        private void SetStateString(string s)
        {
            dispatcher.Invoke((ThreadStart)delegate
            {
                StateString = s;
                Notify("StateString");

            }, DispatcherPriority.Normal);
        }

        public void Cancel()
        {
            SetStateString("Cancelling...");
            Interlocked.Exchange(ref cancelled, 1);
        }
    }
}
