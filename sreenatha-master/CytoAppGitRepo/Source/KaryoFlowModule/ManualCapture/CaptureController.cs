﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Media;
using AI.Bitmap;
using AI.HardwareController;
using System.Threading;
using System.IO;
using System.Xml.Linq;
using System.Windows.Threading;
using System.Windows;
using System.Globalization;
using System.Windows.Media.Imaging;
using AI.Connect;

namespace AI
{
    public class HardwareFailureException : Exception
    {
    }

    [Flags]
    public enum CaptureOptions
    {
        Default = 1,
        WithAutoSetupPerChannel = 2,
        Fuse = 4
    }

    public class CaptureSlide
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class CaptureCase
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class CaptureFrame : Notifier, IDisposable
    {
        public Guid FrameId { get; set; }
        public ImageSource Thumbnail { get; set; }
        public Guid Group { get; set; }

        private ObservableCollection<CaptureFrame> fuses = new ObservableCollection<CaptureFrame>();
        public IEnumerable<CaptureFrame> FuseImages { get { return fuses; } }
        public bool GotFuses { get { return fuses.Count > 0; } }

        public bool IsDeletable { get; set; }

        public void AddFuse(CaptureFrame frame)
        {
            fuses.Add(frame);
            Notify("GotFuses");
        }

        public void RemoveFuse(CaptureFrame fuse)
        {
            fuses.Remove(fuse);
            Notify("GotFuses");
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (fuses.Count > 0)
            {
                foreach (CaptureFrame f in fuses)
                    f.Dispose();
                fuses.Clear();
                fuses = null;
            }
        }

        #endregion
    }

    public class CaptureController : ViewModelBase
    {
        private Fluorochrome currentFluorochrome;
        private HardwareController.HardwareController hw;
        private ObservableCollection<CaptureFrame> capturedImages = null;
        private ObservableCollection<CaptureSlide> slides = null;
        private ObservableCollection<Filter> dichroics = new ObservableCollection<Filter>();
        private ObservableCollection<Filter> excitationFilters = new ObservableCollection<Filter>();
        private ObservableCollection<Fluorochrome> fluorochromes = new ObservableCollection<Fluorochrome>();
        private string captureProgressText = string.Empty;
        private Dispatcher dispatcher = null;
        private int stackSize = 0;
        private double stackGapMicrons;
        private Camera camera = null;
        private CaptureSlide curSlide = null;
        private CaptureCase theCase = null;
        private bool autoExposing = false;
        private bool SaveTheStacks = false;
        private bool showRoi = false;
        private Point lastRoiPt;
        private bool brightfield;
        private CaptureFrame currentFrame;
        private ChannelInfo brightfieldFluor;
        private Case limsCase;

        public CaptureController(Dispatcher dispatcher, HardwareController.HardwareController hw, Camera cam, Guid caseId)
            : this(dispatcher, hw, cam, caseId, false)
        {
        }

        public CaptureController(Dispatcher dispatcher, HardwareController.HardwareController hw, Camera cam, Guid caseId, bool brightfieldMode)
        {
            limsCase = Connect.LIMS.Case(caseId);

            this.dispatcher = dispatcher;
            this.hw = hw;
            this.camera = cam;
            camera.GoLive();
            camera.AutoExposed += new EventHandler(camera_AutoExposed);
            camera.AutoExposing += new EventHandler<AutoExposingEventArgs>(camera_AutoExposing);
            capturedImages = new ObservableCollection<CaptureFrame>();

			using (var db = new Connect.Database())
			{
				theCase = (from c in db.Cases
						   where c.caseId == caseId
						   select new CaptureCase
						   {
							   Id = c.caseId,
							   Name = Connect.LIMS.CaseName(c.caseId),
						   }).First();

				slides = new ObservableCollection<CaptureSlide>(from s in db.Slides
																where s.caseId == theCase.Id
																select new CaptureSlide
																{
																	Id = s.slideId,
																	Name = Connect.LIMS.SlideName(s.slideId)
																});

				Slide = slides.LastOrDefault();
				brightfield = brightfieldMode;
				stackGapMicrons = 0.5;
				stackSize = 10;

				PopulateDichroics();
				PopulateExcitationFilters();

				LoadGlobalZStackInfo();
				LoadFluorochromes();
				SetCurrentFluorochrome();

				brightfieldFluor = new ChannelInfo("Brightfield")
				{
					IsCounterstain = false,
					IsStacked = false,
					RenderColor = Colors.White,
					Visible = true,
					Exposure = 40
				};

				if (!IsBrightfield)
					ProtectSampleWithBlankFilterAndToggleShutterToMakeSureMySampleDoesntBleachTooMuchWhenImNotExpectingItTo();
			}
        }
        
        public void MoveToBrightfieldPositions()
        {
            camera.SetExposure(brightfieldFluor.Exposure);

            string[] dichroics = null;
            int num = 0;
            hw.GetDichroics(ref num, ref dichroics);

            for(int i = 0; i < num; ++i)
            {
                if (string.Compare(dichroics[i], "clear", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    hw.MoveToDichroic(i, true);
                }
            }
        }

        public bool IsBrightfield
        {
            get { return brightfield; }
            set { brightfield = value; base.OnPropertyChanged("IsBrightfield"); }
        }

        public IEnumerable<CaptureSlide> Slides
        {
            get
            {
                return slides;
            }
        }

        public CaptureFrame CurrentCapturedImage
        {
            get { return currentFrame; }
            set { currentFrame = value; base.OnPropertyChanged("CurrentCapturedImage"); }
        }

        public void ProtectSampleWithBlankFilterAndToggleShutterToMakeSureMySampleDoesntBleachTooMuchWhenImNotExpectingItTo()
        {
            if (!IsBrightfield)
            {
                hw.MoveToBlank();
                hw.SetBrightfieldLamp(0, false);
                hw.SetFluorescenceLampShutter((int)ShutterState.Closed, true);
            }
            NoCurrentChannel();
            base.OnPropertyChanged("CurrentChannel");
        }

        private void SetCurrentFluorochrome()
        {
            FileInfo file = AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\capturecontroller.capturesettings");

            if (file.Exists)
            {
                XElement xml = XElement.Load(file.FullName);
                currentFluorochrome = Fluorochrome.FromXml(xml.Descendants("Flourochrome").FirstOrDefault(), dichroics, excitationFilters);
            }
            else if (fluorochromes.Count > 0)
            {
                currentFluorochrome = fluorochromes[0];
            }
            else
            {
                CreateNewFluorochrome();
            }
        }

        private void CreateNewFluorochrome()
        {
            currentFluorochrome = new Fluorochrome();
            ChannelInfo dapi = new ChannelInfo();
            currentFluorochrome.AddChannel(dapi);
            MakeCurrent(currentFluorochrome.Channels.First());
            camera.GoLive();
        }

        private void LoadFluorochromes()
        {
            string localAppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            DirectoryInfo genetixDirectory = new DirectoryInfo(Path.Combine(localAppDataPath, "Genetix"));

            fluorochromes.Clear();

            foreach (FileInfo file in genetixDirectory.GetFiles("*.fluorochrome"))
            {
                XElement xml = XElement.Load(file.FullName);
                fluorochromes.Add(Fluorochrome.FromXml(xml, dichroics, excitationFilters));
            }
        }

        private void PopulateExcitationFilters()
        {
            int num = 0;
            string[] hardwareFilters = null;
            hw.GetExcitationFilters(ref num, ref hardwareFilters);
            PopulateFilterList(hardwareFilters, excitationFilters);
        }

        private void PopulateDichroics()
        {
            int num = 0;
            string[] hardwareFilters = null;
            hw.GetDichroics(ref num, ref hardwareFilters);
            PopulateFilterList(hardwareFilters, dichroics);
        }

        private void PopulateFilterList(string[] hardwareFilters, ObservableCollection<Filter> filters)
        {
            int i = 0;
            if (hardwareFilters != null)
            {
                foreach (var name in hardwareFilters)
                {
                    filters.Add(new Filter
                    {
                        Name = name,
                        RotorPosition = i++
                    });
                }
            }
        }

        public ObservableCollection<Fluorochrome> Fluorochromes
        {
            get
            {
                LoadFluorochromes();
                return fluorochromes;
            }
        }

        public Camera Camera
        {
            get { return camera; }
        }

        public CaptureCase Case
        {
            get { return theCase; }
        }

        public CaptureSlide Slide
        {
            get { return curSlide; }
            set
            {
                ShowRoi = false;

                curSlide = value;
                base.OnPropertyChanged("Slide");
                base.OnPropertyChanged("GotSlide");

                if (curSlide == null)
                {
                    CurrentCapturedImage = null;
                    return;
                }

                capturedImages.Clear();

				using (var db = new Connect.Database())
				{
					if (db.ImageFrames.Count(f => f.slideId == value.Id && f.imageType == (int)ImageType.Raw) == 0)
						return;

					var frames = from f in db.ImageFrames
								 where f.slideId == value.Id && f.imageType == (int)ImageType.Raw
								 select new CaptureFrame
								 {
									 FrameId = f.imageFrameId,
									 Group = f.groupId,
									 Thumbnail = LoadThumb(f.imageFrameId.ToString() + ".jpg", db)
								 };

					foreach (var frame in frames)
					{
						capturedImages.Add(frame);

						var fuses = from f in db.ImageFrames
									where f.groupId == frame.Group && f.imageType == (int)ImageType.Fuse
									select new CaptureFrame
									{
										FrameId = f.imageFrameId,
										Group = f.groupId,
										Thumbnail = LoadThumb(f.imageFrameId.ToString() + ".jpg", db)
									};

						foreach (var fuse in fuses)
							frame.AddFuse(fuse);
					}

					CurrentCapturedImage = capturedImages.FirstOrDefault();
				}
            }
        }

        private ImageSource LoadThumb(string url, Database db)
        {
            Stream stream = BlobStream.ReadBlobStream(Case.Id, url, db, Connect.CacheOption.Cache);
            if (stream == null)
                return null;
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.StreamSource = stream;
            image.EndInit();
            image.Freeze();
            return image;
        }

        private void LoadGlobalZStackInfo()
        {
            FileInfo file = AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\capturecontroller.capturesettings");
            if (file.Exists)
            {
                XElement xml = XElement.Load(file.FullName);
                stackSize = Convert.ToInt32(xml.Attribute("ZStackSize").Value);
                stackGapMicrons = Convert.ToDouble(xml.Attribute("ZStackGap").Value);
                SaveTheStacks = Convert.ToBoolean(xml.Attribute("SaveStacks").Value);
            }
        }

        public void SaveDefaultFluorochromeAndSettings()
        {
            FileInfo file = AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\capturecontroller.capturesettings");
            SaveFluorochromeAsXml(file);
        }

        private void SaveFluorochromeAsXml(FileInfo file)
        {
            if (file.Exists)
                file.Delete();

            XElement xml = new XElement("CaptureController",
                new XAttribute("ZStackSize", StackSize),
                new XAttribute("ZStackGap", StackIncrement),
                new XAttribute("SaveStacks", SaveTheStacks),
                new XAttribute("IsBrightfield", IsBrightfield),
                currentFluorochrome.ToXml());

            xml.Save(file.FullName);
        }

        public bool ReadyToCapture
        {
            get
            {
                if (curSlide == null)
                    return false;

                if (hw == null)
                    return false;

                if (currentFluorochrome.Channels.Count() == 0)
                    return false;

                foreach (var c in currentFluorochrome.Channels)
                {
                    if (c.Dichroic == null)
                        return false;
                }

                return true;
            }
        }

        public bool ReadyToCaptureBrightfield
        {
            get
            {
                if (curSlide == null)
                    return false;

                if (hw == null)
                    return false;

                return true;
            }
        }

        public bool ShowZStackOptions
        {
            get
            {
                foreach (var c in currentFluorochrome.Channels)
                {
                    if (c.IsStacked)
                        return true;
                }

                return false;
            }
        }

        public void StacksChanged()
        {
            base.OnPropertyChanged("ShowZStackOptions");
        }

        public Fluorochrome Fluorochrome
        {
            get { return currentFluorochrome; }
        }

        public IEnumerable<CaptureFrame> CapturedImages
        {
            get { return capturedImages; }
        }

        public IEnumerable<Filter> Dichroics
        {
            get { return dichroics; }
        }

        public IEnumerable<Filter> ExcitationFilters
        {
            get { return excitationFilters; }
        }

        public string CaptureProgressText
        {
            get { return captureProgressText; }
            set { captureProgressText = value; base.OnPropertyChanged("CaptureProgressText"); }
        }

        public int StackSize
        {
            get { return stackSize; }
            set { stackSize = value; base.OnPropertyChanged("StackSize"); }
        }

        public bool SaveStacks
        {
            get { return SaveTheStacks; }
            set { SaveTheStacks = value; base.OnPropertyChanged("SaveStacks"); }
        }


        public double StackIncrement
        {
            get { return stackGapMicrons; }
            set { stackGapMicrons = value; base.OnPropertyChanged("StackIncrement"); }
        }

        public void AddFluorochromeChannel()
        {
            ChannelInfo f = new ChannelInfo();
            currentFluorochrome.AddChannel(f);
            MakeCurrent(f);
            camera.GoLive();
            base.OnPropertyChanged("ReadyToCapture");
            base.OnPropertyChanged("ShowZStackOptions");
        }

        public void RemoveFluorochromeChannel(ChannelInfo fluor)
        {
            currentFluorochrome.RemoveChannel(fluor);
            base.OnPropertyChanged("ReadyToCapture");
            base.OnPropertyChanged("ShowZStackOptions");
            base.OnPropertyChanged("GotCounterstain");
        }

        public ImageFrame Capture(CaptureOptions options)
        {
            ClearCaptureProgressString();

            float x = 0;
            float y = 0;
            float z = 0;
            hw.GetStagePosition(ref x, ref y, ref z);

            bool stacking = currentFluorochrome.Channels.Where(f => f.IsStacked).Count() > 0;

            ImageFrame image = new ImageFrame();
            image.ImageBitsPerPixel = camera.ImageBitsPerPixel;
            image.ImageWidth = camera.ImageWidth;
            image.ImageHeight = camera.ImageHeight;
            image.SlideLocation = new SlidePoint(x, y, z, "EF NYI");

            camera.StopLive();
            foreach (var fluor in currentFluorochrome.Channels)
            {
                MakeCurrent(fluor);

                if ((options & CaptureOptions.WithAutoSetupPerChannel) == CaptureOptions.WithAutoSetupPerChannel)
                {
                    hw.MoveStageToPosZ(z);
                    AutoCameraSetupViaRoi(lastRoiPt);
                    WaitForAutoSetupToComplete();
                }
                ApplyFluorochrome(fluor);
                CaptureChannel(fluor, image, z);
            }

            ProtectSampleWithBlankFilterAndToggleShutterToMakeSureMySampleDoesntBleachTooMuchWhenImNotExpectingItTo();
            camera.GoLive();

            image.Thumbnail = ThumbnailHistograms.Compose(image);
            image.Id = Guid.NewGuid();

            return image;
        }

        public ImageFrame BrightfieldCapture(CaptureOptions options)
        {
            ClearCaptureProgressString();

            float x = 0;
            float y = 0;
            float z = 0;
            hw.GetStagePosition(ref x, ref y, ref z);

            ImageFrame image = new ImageFrame();
            image.ImageBitsPerPixel = camera.ImageBitsPerPixel;
            image.ImageWidth = camera.ImageWidth;
            image.ImageHeight = camera.ImageHeight;
            image.SlideLocation = new SlidePoint(x, y, z, "EF NYI");

            camera.StopLive();

            if ((options & CaptureOptions.WithAutoSetupPerChannel) == CaptureOptions.WithAutoSetupPerChannel)
            {
                AutoCameraSetupViaRoi(lastRoiPt);
                WaitForAutoSetupToComplete();
            }
            
            CaptureChannel(brightfieldFluor, image, z);

            camera.GoLive();

            image.Thumbnail = ThumbnailHistograms.Compose(image);
            image.Id = Guid.NewGuid();

            return image;
        }

        private void WaitForAutoSetupToComplete()
        {
            while (AutoExposing)
            {
                Thread.Sleep(50);
            }
        }

        public void AddCapturedImage(ImageFrame image)
        {
			using (var db = new Database())
			{
				var newFrame = new CaptureFrame { FrameId = image.Id, Thumbnail = image.Thumbnail };
				newFrame.Group = Guid.NewGuid();
				newFrame.IsDeletable = true;
				capturedImages.Add(newFrame);

				CurrentCapturedImage = newFrame;

				ImageFrameDbLoad.ToDb(db, image, Slide.Id, Case.Id, ImageType.Raw, newFrame.Group);
				image.ClearAllImageData();

				db.SubmitChanges();
			}
        }

        public void AddFuseImage(ImageFrame image)
        {
            var newFrame = new CaptureFrame { FrameId = image.Id, Thumbnail = image.Thumbnail };
            newFrame.Group = CurrentCapturedImage.Group;
            newFrame.IsDeletable = true;
            CurrentCapturedImage.AddFuse(newFrame);

			using (var db = new Database())
			{
				image.WriteImageDataToDisk(Case.Id, db);
				ImageFrameDbLoad.ToDb(db, image, Slide.Id, Case.Id, ImageType.Fuse, newFrame.Group);

				db.SubmitChanges();
			}

            image.ClearAllImageData();
        }

        private Channel Snap()
        {
            ushort[] pixels = camera.Capture();
            return new Channel(camera.ImageWidth, camera.ImageHeight, camera.ImageBitsPerPixel, pixels);
        }

        private void CaptureChannel(ChannelInfo fluorChannel, ImageFrame image, float focalZ)
        {
            List<Channel> capturedChannels = new List<Channel>();

            AddToCaptureString(fluorChannel.DisplayName);
            AddToCaptureString("...");

            if (fluorChannel.IsStacked)
            {
                double z = focalZ - stackGapMicrons * (stackSize / 2.0f);

                for (int i = 0; i < stackSize; ++i)
                {
                    hw.MoveStageToPosZ((float)z);
                    hw.MicWaitZ();
                    z += stackGapMicrons;

                    AddToCaptureString((i + 1).ToString() + "..");
                    Channel cap = Snap();
                    AddSnapToImage(cap, fluorChannel, image, i);
                }

                Snap ProjSnap = image.MaxProjection(fluorChannel);

                image.AddSnap(ProjSnap);

                if (SaveTheStacks == false)
                    image.RemoveChannelSnaps(fluorChannel);

                hw.MoveStageToPosZ(focalZ);
            }
            else
            {
                Channel cap = Snap();
                AddSnapToImage(cap, fluorChannel, image, 0);
            }

            AddToCaptureString("Done.\n");
        }

        private void AddSnapToImage(Channel cap, ChannelInfo fluorChannel, ImageFrame image, int i)
        {
            Snap snap = new Snap();
            snap.Id = Guid.NewGuid();
            snap.ChannelInfo = fluorChannel;
            snap.ChannelPixels = cap;
            snap.StackIndex = i;
            image.AddSnap(snap);
        }

        private void AddToCaptureString(string p)
        {
            dispatcher.Invoke((ThreadStart)delegate
            {
                CaptureProgressText += p;

            }, DispatcherPriority.Normal);
        }

        private void ClearCaptureProgressString()
        {
            dispatcher.Invoke((ThreadStart)delegate
            {
                CaptureProgressText = "";

            }, DispatcherPriority.Normal);
        }

        private void ApplyFluorochrome(ChannelInfo fl)
        {
            if (fl.Dichroic != null)
                hw.MoveToDichroic(fl.Dichroic.RotorPosition, true);

            if (fl.Excitation != null)
                hw.MoveToExcitation(fl.Excitation.RotorPosition, true);

            camera.SetExposure(fl.Exposure);
        }

        public void MakeCurrent(ChannelInfo fl)
        {
            NoCurrentChannel();
            fl.IsCurrent = true;
            ApplyFluorochrome(fl);
            hw.SetFluorescenceLampShutter(0, true);
            base.OnPropertyChanged("CurrentChannel");
        }

        private void NoCurrentChannel()
        {
            if (currentFluorochrome != null)
            {
                foreach (var channel in currentFluorochrome.Channels)
                    channel.IsCurrent = false;
            }
        }

        public void KillCapturedImage(CaptureFrame image)
        {
            capturedImages.Remove(image);

			using (var db = new Connect.Database())
			{
				foreach (var fuse in image.FuseImages)
					DeleteImage(fuse.FrameId, db);

				DeleteImage(image.FrameId, db);
				db.SubmitChanges();
			}
        }

        public void KillFuseImage(CaptureFrame fuse, CaptureFrame parentFrame)
        {
            parentFrame.RemoveFuse(fuse);

			using (var db = new Connect.Database())
			{
				DeleteImage(fuse.FrameId, db);
				db.SubmitChanges();
			}
        }

        private void DeleteImage(Guid frameId, Database db)
        {
            //  Before deleting the image frame from the database, we net to get all of the
            //  filenames associated with the image frame in the blobpool for deletion.
            var snapfiles = db.ImageFrameFiles.Where(f => f.imageFrameId == frameId);
            Guid caseid = snapfiles.First().caseId;
            var frame = db.ImageFrames.Where(f => f.imageFrameId == frameId).First();

            if (frame != null)
            {
                //   get the thumbnail file name if it exists
                if ((frame.blobid != null) && (frame.blobid != Guid.Empty))
                    BlobStream.Delete((Guid)frame.blobid, db);

                foreach (var sf in snapfiles)
                {
                    BlobStream.Delete(sf.blobId, db);
                }
                
                db.ImageFrames.DeleteOnSubmit(frame);
            }
        }

        public void ToggleShutter()
        {
            int shutter = 0;
            if (hw.GetFluorescenceLampShutter(ref shutter) == 0)
            {
                hw.SetFluorescenceLampShutter((int)((shutter == (int)ShutterState.Open)?ShutterState.Closed:ShutterState.Open), true);
            }
        }

        public bool HasDichroics
        {
            get { return dichroics.Count > 0; }
        }

        public bool HasExcitationFilters
        {
            get { return excitationFilters.Count > 0; }
        }

        public bool NoFiltersConfugred
        {
            get { return !HasDichroics && !HasExcitationFilters; }
        }

        public ChannelInfo CurrentChannel
        {
            get { return currentFluorochrome == null ? null : currentFluorochrome.Channels.Where(c => c.IsCurrent).FirstOrDefault(); }
        }

        public void CreateNewSlide()
        {
			using (var db = new Connect.Database())
			{
				int n = db.Slides.Where(s => s.caseId == Case.Id).Count();
				string name = (n + 1).ToString(CultureInfo.InvariantCulture);

                var id = CaseManagement.CreateSlide(limsCase, name, "", SlideTypes.Metaphase);

				CaptureSlide newSlide = new CaptureSlide
				{
					Id = db.SlideIdFor(id),
					Name = name
				};

				slides.Add(newSlide);
				Slide = newSlide;
			}
        }

        public bool AutoExposing
        {
            get { return autoExposing; }
            set
            {
                autoExposing = value;
                base.OnPropertyChanged("AutoExposing");

                if (value)
                    ShowRoi = true;
            }
        }

        public bool ShowRoi
        {
            get { return showRoi; }
            set { showRoi = value; base.OnPropertyChanged("ShowRoi"); }
        }

        public void AutoCameraSetupViaRoi(Point pt)
        {
            AutoExposing = true;
            lastRoiPt = pt;

            ThreadPool.QueueUserWorkItem((WaitCallback)delegate
            {
                camera.AutoExpose(pt.X, pt.Y, 100, IsBrightfield);
            });
        }

        private void camera_AutoExposing(object sender, AutoExposingEventArgs e)
        {
            dispatcher.BeginInvoke((ThreadStart)delegate
            {
                if (CurrentChannel != null)
                    CurrentChannel.Exposure = e.Exposure;

            }, DispatcherPriority.Normal);
        }

        private void camera_AutoExposed(object sender, EventArgs e)
        {
            dispatcher.BeginInvoke((ThreadStart)delegate
            {
                AutoExposing = false;

            }, DispatcherPriority.Normal);
        }

        public void SetCounterstain(ChannelInfo fluorochromeChannel)
        {
            foreach (var ch in currentFluorochrome.Channels)
            {
                ch.IsCounterstain = ch == fluorochromeChannel;
            }

            base.OnPropertyChanged("GotCounterstain");
        }

        public bool GotCounterstain
        {
            get { return currentFluorochrome.Channels.Where(c => c.IsCounterstain).Count() != 0; }
        }

        public bool GotSlide
        {
            get { return (curSlide == null)? false : true; }
        }


        public void SaveCurrentFluorochrome()
        {
            FileInfo file = GetFluorochromeFile(currentFluorochrome);
            DeleteFluorochromeFile(file);
            XElement xml = currentFluorochrome.ToXml();
            xml.Save(file.FullName);

            base.OnPropertyChanged("Fluorochromes");
        }

        internal void LoadFluorochrome(Fluorochrome fluorochrome)
        {
            currentFluorochrome = fluorochrome;
            NotifyAfterFluorochromeChange();
        }

        internal void DeleteFluorochrome(Fluorochrome fluorochrome)
        {
            FileInfo fluorochromeFile = GetFluorochromeFile(fluorochrome);
            DeleteFluorochromeFile(fluorochromeFile);
            base.OnPropertyChanged("Fluorochrome");
            base.OnPropertyChanged("Fluorochromes");
        }

        private static void DeleteFluorochromeFile(FileInfo fluorochromeFile)
        {
            if (fluorochromeFile.Exists)
                fluorochromeFile.Delete();
        }

        private FileInfo GetFluorochromeFile(Fluorochrome fluorochrome)
        {
            string localAppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            DirectoryInfo genetixDirectory = new DirectoryInfo(Path.Combine(localAppDataPath, "Genetix"));
            string fileName = fluorochrome.Id.ToString() + ".fluorochrome";
            return new FileInfo(Path.Combine(genetixDirectory.FullName, fileName));
        }

        internal void NewCurrentFluorochrome()
        {
            CreateNewFluorochrome();
            NotifyAfterFluorochromeChange();
        }

        internal void CopyCurrentFluorochrome()
        {
            Fluorochrome duplicate = currentFluorochrome.Duplicate();
            duplicate.Name = currentFluorochrome.Name + " (copy)";
            currentFluorochrome = duplicate;
            base.OnPropertyChanged("Fluorochrome");
        }

        private void NotifyAfterFluorochromeChange()
        {
            base.OnPropertyChanged("Fluorochrome");
            base.OnPropertyChanged("ShowZStackOptions");
            base.OnPropertyChanged("ReadyToCapture");
            base.OnPropertyChanged("GotCounterstain");
        }

        protected override void DisposeManagedResources()
        {
            camera.AutoExposed -= camera_AutoExposed;
            camera.AutoExposing -= camera_AutoExposing;
            camera = null;

            if (capturedImages != null)
            {
                if (capturedImages.Count > 0)
                {
                    foreach (CaptureFrame c in CapturedImages)
                        c.Dispose();

                    capturedImages.Clear();
                    capturedImages = null;
                }
            }

            if (slides != null)
            {
                if (slides.Count > 0)
                {
                    slides.Clear();
                    slides = null;
                }
            }

            if (dichroics != null)
            {
                if (dichroics.Count > 0)
                {
                    dichroics.Clear();
                    dichroics = null;
                }
            }

            if (dichroics != null)
            {
                if (dichroics.Count > 0)
                {
                    dichroics.Clear();
                    dichroics = null;
                }
            }

            if (excitationFilters != null)
            {
                if (excitationFilters.Count > 0)
                {
                    excitationFilters.Clear();
                    excitationFilters = null;
                }
            }

            if (fluorochromes != null)
            {
                if (fluorochromes.Count > 0)
                {
                    fluorochromes.Clear();
                    fluorochromes = null;
                }
            }
        }

        protected override void DisposeUnManagedResources()
        {
        }
    }
}
