﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Media3D;
using System.Collections.ObjectModel;

namespace AI
{
    public sealed class CaptureQueue : Notifier
    {
        private List<CaptureRegion> regions;

        public CaptureQueue()
        {
            regions = new List<CaptureRegion>();
        }

        public void Enqueue(CaptureRegion captureRegion)
        {
            regions.Add(captureRegion);
            Refresh();
        }

        public void EnqueueAll(IEnumerable<CaptureRegion> regions)
        {
            foreach (var r in regions)
                this.regions.Add(r);

            Refresh();
        }

        public void RemoveQueue(Guid slideId)
        {
            regions = regions.Where(r => r.Slide != slideId).ToList();
            Refresh();
        }

        public void Refresh()
        {
            Notify("CaptureRegions", "GotRegions", "Length", "CompletedRegions");
        }

        public IEnumerable<CaptureRegion> AllRegions
        {
            get { return regions; }
        }

        public IEnumerable<CaptureRegion> CaptureRegions
        {
            get { return regions.Where(r => r.Status == CaptureStatus.Ready); }
        }

        public IEnumerable<CaptureRegion> CompletedRegions
        {
            get { return regions.Where(r => r.Status != CaptureStatus.Ready); }
        }

        public void Clear()
        {
            regions.Clear();
            Refresh();
        }

        public bool GotRegions
        {
            get { return regions.Count > 0; }
        }

        public int Length 
        { 
            get { return regions.Count; }
        }
    }
}
