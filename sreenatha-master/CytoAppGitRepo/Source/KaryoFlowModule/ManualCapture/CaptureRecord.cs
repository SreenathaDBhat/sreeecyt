﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Media3D;
using System.Windows;

namespace AI
{
    public class SnapRecord : EventArgs
    {
        public CaptureRegion Region { get; internal set; }
        public Snap Snap { get; internal set; }
    }

    public class FrameRecord : EventArgs
    {
        public CaptureRegion Region { get; internal set; }
        public ImageFrame Frame { get; internal set; }
    }

    public class CaptureRecord
    {
        private List<SnapRecord> snaps;
        private List<FrameRecord> frames;

        public CaptureRecord()
        {
            snaps = new List<SnapRecord>();
            frames = new List<FrameRecord>();
        }

        public SnapRecord AddSnap(CaptureRegion region, Snap snap)
        {
            var s = new SnapRecord() { Region = region, Snap = snap };
            snaps.Add(s);
            return s;
        }

        public FrameRecord AddFrame(CaptureRegion region, ImageFrame frame)
        {
            var r = new FrameRecord() { Region = region, Frame = frame };
            frames.Add(r);
            return r;
        }

        public IEnumerable<SnapRecord> Snaps { get { return snaps; } }
        public IEnumerable<FrameRecord> Frames { get { return frames; } }
    }
}
