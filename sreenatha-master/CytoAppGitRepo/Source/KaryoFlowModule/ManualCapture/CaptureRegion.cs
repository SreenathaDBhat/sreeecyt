﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Media3D;
using System.Windows;

namespace AI
{
    public abstract class CaptureRegion : Notifier
    {
        public abstract void GenerateFrames(Size cameraImageSize, double z, int objective);
        public abstract IEnumerable<Point3D> Frames { get; }
        public abstract bool ShouldAutoFocus { get; }

        public Guid Slide { get; set; }
        public Guid Case { get; set; }
        public string Identifier { get; set; }

        /// <summary>
        /// Index of slide in slideloader. 1..119 for GSL120, 1..8 for 8bay stage, etc.
        /// </summary>
        public int SlideIndex { get; set; }

        public Guid DeferredCaptureId { get; set; }
        public string Owner { get; set; }
        public string Comment { get; set; }
        public DateTime Modified { get; set; }

        private CaptureStatus status;
        public CaptureStatus Status
        {
            get { return status; }
            set { status = value; Notify("Status"); }
        }

        public abstract Point RegionCenter { get; }
    }

    public class DiscreetFramesRegion : CaptureRegion
    {
        private bool grid;
        private Point3D center;
        private List<Point3D> frames;

        public static CaptureRegion Generate1x1(Point3D center, Guid caseId, Guid slideId, int slideIndex)
        {
            var c = new DiscreetFramesRegion();
            c.grid = false;
            c.center = center;
            c.Case = caseId;
            c.Slide = slideId;
            c.SlideIndex = slideIndex;
            return c;
        }

        public static CaptureRegion Generate3x3(Point3D center, Guid caseId, Guid slideId, int slideIndex)
        {
            var c = new DiscreetFramesRegion();
            c.grid = true;
            c.center = center;
            c.Case = caseId;
            c.Slide = slideId;
            c.SlideIndex = slideIndex;
            return c;
        }

        public override Point RegionCenter
        {
            get { return new Point(center.X, center.Y); }
        }

        private DiscreetFramesRegion()
        {
        }

        public override void GenerateFrames(Size cameraImageSize, double currentZ, int objective)
        {
            frames = new List<Point3D>();
            if (grid)
            {
                frames.Add(new Point3D(center.X - cameraImageSize.Width, center.Y - cameraImageSize.Height, center.Z));
                frames.Add(new Point3D(center.X - cameraImageSize.Width, center.Y, center.Z));
                frames.Add(new Point3D(center.X - cameraImageSize.Width, center.Y + cameraImageSize.Height, center.Z));
                frames.Add(new Point3D(center.X, center.Y + cameraImageSize.Height, center.Z));
                frames.Add(new Point3D(center.X, center.Y, center.Z));
                frames.Add(new Point3D(center.X, center.Y - cameraImageSize.Height, center.Z));
                frames.Add(new Point3D(center.X + cameraImageSize.Width, center.Y - cameraImageSize.Height, center.Z));
                frames.Add(new Point3D(center.X + cameraImageSize.Width, center.Y, center.Z));
                frames.Add(new Point3D(center.X + cameraImageSize.Width, center.Y + cameraImageSize.Height, center.Z));

            }
            else
            {
                frames.Add(center);
            }
        }

        public override IEnumerable<Point3D> Frames
        {
            get
            {
                return frames;
            }
        }

        public override bool ShouldAutoFocus
        {
            get
            {
                return false;
            }
        }
    }

    public class ArbitraryRegion : CaptureRegion
    {
        private Rect area;
        private IEnumerable<Point3D> frames;

        public ArbitraryRegion(Rect area)
        {
            this.area = area;
        }

        public override void GenerateFrames(Size cameraImageSize, double z, int objective)
        {
            frames = FramesForArea(area, cameraImageSize, objective, z);
        }

        public override IEnumerable<Point3D> Frames
        {
            get { return frames; }
        }

        public override bool ShouldAutoFocus
        {
            get { return true; }
        }

        public override Point RegionCenter
        {
            get { return new Point(area.Left + (area.Width / 2), area.Top + (area.Height / 2)); }
        }

        public Rect Area { get { return area; } }

        /// <summary>
        /// Return optimized list of frames to cover a given area
        /// </summary>
        /// <param name="scanArea">Area to capture. Ideal coordinates.</param>
        /// <param name="imageSize">Pixel size of the capturing camera.</param>
        /// <param name="objective">Index (1 based) of the capturing objective</param>
        /// <param name="z">Why the hell not.</param>
        /// <returns>Take a guess.</returns>
        public static IEnumerable<Point3D> FramesForArea(Rect scanArea, Size cameraImageSize, int objective, double z)
        {
            Size imageScale = MicroscopeFile.ImageScale(objective);
            Size frameSizeMicrons = new Size(cameraImageSize.Width * imageScale.Width, cameraImageSize.Height * imageScale.Height);
            double halfFrameWidth = frameSizeMicrons.Width / 2;
            double halfFrameHeight = frameSizeMicrons.Height / 2;

            double x = scanArea.Left + halfFrameWidth;
            double y = scanArea.Top + halfFrameHeight;
            bool goingDown = true;

            for (; x < scanArea.Right; x += frameSizeMicrons.Width)
            {
                while (true)
                {
                    yield return new Point3D(x, y, z);

                    if (goingDown)
                    {
                        if (y + frameSizeMicrons.Height < scanArea.Bottom)
                            y += frameSizeMicrons.Height;
                        else
                            break;
                    }
                    else
                    {
                        if (y - frameSizeMicrons.Height > scanArea.Top)
                            y -= frameSizeMicrons.Height;
                        else
                            break;
                    }
                }

                goingDown = !goingDown;
            }
        }
    }
}
