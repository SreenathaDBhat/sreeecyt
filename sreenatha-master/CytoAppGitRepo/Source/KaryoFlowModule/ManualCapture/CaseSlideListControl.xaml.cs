﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace AI
{
    public class SlideInfo
    {
        public string Name { get; set; }
        public string Culture { get; set; }
        public string Barcode { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public byte StatusId { get; set; }
        public Guid Id { get; set; }
        public DateTime Modified { get; set; }
    }

    public partial class CaseSlideListControl
    {
        public static readonly DependencyProperty CasesProperty = DependencyProperty.Register("Cases", typeof(IEnumerable<Case>), typeof(CaseSlideListControl));
        public static readonly DependencyProperty SelectedCaseProperty = DependencyProperty.Register("SelectedCase", typeof(Case), typeof(CaseSlideListControl), new FrameworkPropertyMetadata(OnSelectedCaseChanged));
        public static readonly DependencyProperty SlidesProperty = DependencyProperty.Register("Slides", typeof(IEnumerable<SlideInfo>), typeof(CaseSlideListControl));
        public static readonly DependencyProperty SelectedSlideProperty = DependencyProperty.Register("SelectedSlide", typeof(SlideInfo), typeof(CaseSlideListControl));

        public CaseSlideListControl()
        {
            DataContext = this;
            InitializeComponent();
        }

        public IEnumerable<Case> Cases
        {
            get { return (IEnumerable<Case>)GetValue(CasesProperty); }
            set { SetValue(CasesProperty, value); }
        }

        public Case SelectedCase
        {
            get { return (Case)GetValue(SelectedCaseProperty); }
            set { SetValue(SelectedCaseProperty, value); }
        }

        public IEnumerable<SlideInfo> Slides
        {
            get { return (IEnumerable<SlideInfo>)GetValue(SlidesProperty); }
            set { SetValue(SlidesProperty, value); }
        }

        public SlideInfo SelectedSlide
        {
            get { return (SlideInfo)GetValue(SelectedSlideProperty); }
            set { SetValue(SelectedSlideProperty, value); }
        }

        private static void OnSelectedCaseChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((CaseSlideListControl)sender).RefreshSlides();
        }

        private void RefreshSlides()
        {
            if (SelectedCase == null)
            {
                Slides = null;
                SelectedSlide = null;
                return;
            }

            var db = new Connect.Database();
            var slides = (from s in db.Slides
                          where s.limsRef == SelectedCase.Id
                          select s);

            var list = new List<SlideInfo>();
            using(var lims = Connect.LIMS.New())
            {
                foreach (var s in slides)
                {
                    var ls = lims.GetSlide(s.limsRef);

                    list.Add(new SlideInfo
                              {
                                  Id = s.slideId,
                                  Modified = s.statusChanged,
                                  StatusId = s.slideStatus,
                                  Type = s.slideType,
                                  Status = db.SlideStatus.Where(t => t.sequence == s.slideStatus).First().description,
                                  Name = ls.Name,
                                  Barcode = ls.Barcode
                              });
                }

                Slides = list;
            }
        }

        private void OnLmbUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1)
            {
                MessageBox.Show("ndklnvkd");
            }
        }
    }
}
