﻿using System;
using System.Windows.Input;
using System.Linq;
using System.Windows.Controls;
using System.Collections.Generic;

namespace AI
{
    public partial class ChooseSlideWindow
    {
        public static readonly RoutedUICommand CreateSlide = new RoutedUICommand("CreateSlide", "CreateSlide", typeof(ChooseSlideWindow));
        public static readonly RoutedUICommand SelectSlide = new RoutedUICommand("SelectSlide", "SelectSlide", typeof(ChooseSlideWindow));
        public static readonly RoutedUICommand SelectSlideAndCreate = new RoutedUICommand("SelectSlideAndCreate", "SelectSlideAndCreate", typeof(ChooseSlideWindow));


        public ChooseSlideWindow()
        {
            InitializeComponent();

            Loaded += (s, e) => Keyboard.Focus(searchText);
        }

        private void OnDragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void CanCreateSlideExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateSlideExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var nc = (NewCaseAndSlide)FindResource("new");
            var c = (Case)e.Parameter;
            nc.Case = c.Name;
            tabs.SelectedIndex = 1;

            slideNameTextbox.Focus();
            Keyboard.Focus(slideNameTextbox);
        }

        private void CanSelectSlideExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = e.Parameter != null && e.Parameter is SlideInfo;
        }

        private void SelectSlideExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            SelectedSlide = e.Parameter as SlideInfo;
            DialogResult = true;
        }

        public SlideInfo SelectedSlide { get; private set; }



        private void CanSelectCreateSlideExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = IsInputOk();
        }

        private void SelectCreatedSlideExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var nc = (NewCaseAndSlide)FindResource("new");
            using (var lims = Connect.LIMS.New())
            {
                var theCase = lims.FindCase(nc.Case);

                if (theCase == null)
                {
                    theCase = CaseManagement.CreateCase(nc.Case);
                }

                var type = (typeCombo.SelectedItem) == null ? "Unknown" : ((ComboBoxItem)typeCombo.SelectedItem).Content as string;
                var slide = CaseManagement.CreateSlide(theCase, nc.Slide.Name, nc.Slide.Barcode, DateTime.Now, type, nc.Slide.Culture);

                using (var db = new Connect.Database())
                {
                    SelectedSlide = nc.Slide;
                    SelectedSlide.Id = db.SlideIdFor(slide);
                    DialogResult = true;
                }
            }
        }

        private void UpdateErrors(object sender, TextChangedEventArgs e)
        {
            IsInputOk();
        }

        private bool IsInputOk()
        {
            var nc = (NewCaseAndSlide)FindResource("new");
            using (var lims = Connect.LIMS.New())
            {
                bool caseEmpty = nc.Case == null || nc.Case.Length == 0;
                var c = lims.FindCase(nc.Case);
                
                bool caseExists = caseEmpty ? false : c != null;
                bool slideEmpty = nc.Slide.Name == null || nc.Slide.Name.Length == 0;

                var slides = c == null ? null : lims.GetSlides(c);
                bool slideExists = (caseEmpty || !caseExists || slideEmpty) ? false : slides.Where(n => n.Name == nc.Slide.Name).FirstOrDefault() != null;

                // case name empty
                if (caseEmpty)
                {
                    nc.IsNewCase = false;
                    nc.SlideExists = false;
                    return false;
                }

                nc.IsNewCase = !caseExists;

                if (slideEmpty)
                    nc.SlideExists = false;
                else
                    nc.SlideExists = slideExists;

                return !slideEmpty && !slideExists;
            }
        }

        private void OnTabChanged(object sender, SelectionChangedEventArgs e)
        {
            IsInputOk();
        }
    }

    public class NewCaseAndSlide : Notifier
    {
        private SlideInfo slide = new SlideInfo();
        private string caseInfo;

        public SlideInfo Slide
        {
            get { return slide; }
            set { slide = value; Notify("Slide"); }
        }

        public string Case
        {
            get { return caseInfo; }
            set { caseInfo = value; Notify("Case"); }
        }

        private bool isNewCase;
        public bool IsNewCase
        {
            get { return isNewCase; }
            set { isNewCase = value; Notify("IsNewCase"); }
        }

        private bool slideExists;
        public bool SlideExists
        {
            get { return slideExists; }
            set { slideExists = value; Notify("SlideExists"); }
        }
    }
}
