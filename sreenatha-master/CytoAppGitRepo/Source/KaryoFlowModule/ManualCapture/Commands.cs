﻿using System.Windows.Input;

namespace AI
{
    internal static class Commands
    {
        public static RoutedUICommand CaptureImages = new RoutedUICommand("CaptureImages", "CaptureImages", typeof(Commands));
        public static RoutedUICommand CaptureImagesWithAutoSetup = new RoutedUICommand("CaptureImagesWithAutoSetup", "CaptureImagesWithAutoSetup", typeof(Commands));
        public static RoutedUICommand CaptureFuse = new RoutedUICommand("CaptureFuse", "CaptureFuse", typeof(Commands));
        public static RoutedUICommand CaptureFuseWithAutoSetup = new RoutedUICommand("CaptureFuseWithAutoSetup", "CaptureFuseWithAutoSetup", typeof(Commands));
    }
}
