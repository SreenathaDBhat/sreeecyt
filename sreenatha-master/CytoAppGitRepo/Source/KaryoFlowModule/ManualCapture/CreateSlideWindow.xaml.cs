﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Input;

namespace AI
{
    public partial class CreateSlideWindow
    {
        CreateSlideWindowContoller controller;

        public CreateSlideWindow(Case c)
        {
            controller = new CreateSlideWindowContoller(c);
            DataContext = controller;
            InitializeComponent();

            Loaded += (s, e) => Keyboard.Focus(nameText);
        }

        private void OnDragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void CanOkExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.Valid;
        }

        private void OkExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void OnCancel(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = false;
        }

        public string NewSlideName
        {
            get { return controller.Name; }
        }

        public string NewSlideBarcode
        {
            get { return controller.Barcode; }
        }
    }


    public class CreateSlideWindowContoller : Notifier
    {
        private string name, barcode;
        private List<string> existingSlides;

        public CreateSlideWindowContoller(Case c)
        {
            using (var lims = Connect.LIMS.New())
            {
                existingSlides = (from s in lims.GetSlides(c)
                                  select s.Name).ToList();
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                Notify("Name", "Valid", "FailureReason");
            }
        }

        public string Barcode
        {
            get { return barcode; }
            set
            {
                barcode = value;
                Notify("Barcode", "Valid", "FailureReason");
            }
        }

        public string FailureReason
        {
            get; set;
        }

        public bool Valid
        {
            get 
            {
                if (name == null || name.Length == 0)
                {
                    FailureReason = "Enter slide name";
                    return false;
                }

                foreach(var s in existingSlides)
                {
                    if(string.Compare(name, s, StringComparison.CurrentCultureIgnoreCase) == 0)
                    {
                        FailureReason = "Duplicate slide name";
                        return false;
                    }
                }

                if(barcode != null && barcode.Length > 0)
                {
                    using (var lims = Connect.LIMS.New())
                    {
                        if(lims.FindSlide(barcode) != null)
                        {
                            FailureReason = "Duplicate barcode";
                            return false;
                        }
                    }
                }
                
                return true;
            }
        }
    }
}
