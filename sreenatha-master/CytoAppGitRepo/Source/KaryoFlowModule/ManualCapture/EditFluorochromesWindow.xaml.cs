﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media;
using System.Windows;

namespace AI
{
    public partial class EditFluorochromesWindow
    {
        private FluorochromeEditorController controller;

        public EditFluorochromesWindow(IEnumerable<Fluorochrome> fluors, IEnumerable<Filter> dichroics, IEnumerable<Filter> excitations)
        {
            controller = new FluorochromeEditorController(fluors, dichroics, excitations);
            DataContext = controller;
            InitializeComponent();

            Loaded += (s, e) => SelectName();
        }

        public IEnumerable<Fluorochrome> Fluorochromes { get { return controller.Fluorochromes; } }
        public Fluorochrome SelectedFluorochrome { get { return controller.CurrentFluorochrome; } }

        private void SelectName()
        {
            Keyboard.Focus(nameBox);
            nameBox.SelectAll();
        }

        private void OnDragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void OnOK(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void OnCancel(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void OnAddFluor(object sender, System.Windows.RoutedEventArgs e)
        {
            controller.NewFluorochrome();
            SelectName();
        }

        private void OnAddChannel(object sender, System.Windows.RoutedEventArgs e)
        {
            controller.NewChannel();
        }

        private void OnSetRenderColor(object sender, System.Windows.RoutedEventArgs e)
        {
            ChannelInfo fl = controller.CurrentChannel;
            System.Windows.Forms.ColorDialog dlg = new System.Windows.Forms.ColorDialog();
            dlg.SolidColorOnly = true;
            dlg.Color = System.Drawing.Color.FromArgb(fl.RenderColor.A, fl.RenderColor.R, fl.RenderColor.G, fl.RenderColor.B);
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fl.RenderColor = Color.FromArgb(dlg.Color.A, dlg.Color.R, dlg.Color.G, dlg.Color.B);
            }
        }

        private void OnDeleteFluor(object sender, RoutedEventArgs e)
        {
            controller.DeleteFluorochrome();
        }

        private void OnDuplicateFluor(object sender, RoutedEventArgs e)
        {
            controller.DuplicateFluorochrome();
        }

        private void CheckForMultipleCounterstains(object sender, RoutedEventArgs e)
        {
            controller.TestForCounterstains();
        }

        private void OnDeleteChannel(object sender, RoutedEventArgs e)
        {
            controller.DeleteChannel();
        }

        private void OnDichroicChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            controller.TryToGuessCurrentChannelRenderColor();
        }
    }



    public class FluorochromeEditorController : Notifier
    {
        private IEnumerable<Filter> dichroics, excitations;
        private ObservableCollection<Fluorochrome> fluorochromes;
        private ChannelInfo currentChannel;
        private Fluorochrome current;

        private static readonly Color DefaultColor = Colors.Orange;


        public FluorochromeEditorController(IEnumerable<Fluorochrome> fluors, IEnumerable<Filter> dichroics, IEnumerable<Filter> excitations)
        {
            this.dichroics = dichroics;
            this.excitations = excitations;
            this.fluorochromes = new ObservableCollection<Fluorochrome>(fluors);

            if (fluorochromes.Count == 0)
            {
                NewFluorochrome();
            }
            else
            {
                CurrentFluorochrome = fluorochromes.FirstOrDefault();
            }
        }

        public bool GotDichroicFilters
        {
            get { return dichroics != null && dichroics.Count() > 0; }
        }

        public IEnumerable<Filter> DichroicFilters
        {
            get { return dichroics; }
        }

        public bool GotExcitationFilters
        {
            get { return excitations != null && excitations.Count() > 0; }
        }

        public IEnumerable<Filter> ExcitationFilers
        {
            get { return excitations; }
        }

        public IEnumerable<Fluorochrome> Fluorochromes 
        { 
            get { return fluorochromes; } 
        }

        public Fluorochrome CurrentFluorochrome
        {
            get { return current; }
            set 
            { 
                current = value;
                Notify("CurrentFluorochrome", "IsCounterstainSelected", "MultipleCounterstainsSelected");

                CurrentChannel = current == null ? null : current.Channels.FirstOrDefault();
            }
        }

        public ChannelInfo CurrentChannel
        {
            get { return currentChannel; }
            set { currentChannel = value; Notify("CurrentChannel", "IsCounterstainSelected", "MultipleCounterstainsSelected"); }
        }

        public void NewChannel()
        {
            var newChannel = new ChannelInfo
            {
                Excitation = excitations == null ? null : excitations.FirstOrDefault(),
                Exposure = 10,
                Id = Guid.NewGuid(),
                IsCounterstain = false,
                IsStacked = true,
                RenderColor = DefaultColor,
                Visible = true
            };

            var dapi = dichroics.Where(d => string.Compare(d.Name, "dapi", StringComparison.CurrentCultureIgnoreCase) == 0).FirstOrDefault();
            if (dapi != null)
            {
                newChannel.Dichroic = dapi;
                newChannel.RenderColor = Colors.Blue;
            }
            else
            {
                newChannel.Dichroic = dichroics.FirstOrDefault();
            }

            current.AddChannel(newChannel);
            CurrentChannel = newChannel;
        }

        public void NewFluorochrome()
        {
            string name = "New Fluorochrome";

            int n = 2;
            while (FluorExists(name))
            {
                name = "New Fluorochrome " + (n++);
            }

            var f = Fluorochrome.Empty;
            f.Name = name;

            fluorochromes.Add(f);
            CurrentFluorochrome = f;

            NewChannel();
            CurrentChannel = CurrentFluorochrome.Channels.First();
            CurrentChannel.IsCounterstain = true;
        }

        private bool FluorExists(string name)
        {
            return fluorochromes.Count(f => f.Name == name) > 0;
        }

        public void DeleteFluorochrome()
        {
            if (CurrentFluorochrome != null)
            {
                var f = CurrentFluorochrome;
                CurrentFluorochrome = null;
                fluorochromes.Remove(f);
                CurrentFluorochrome = fluorochromes.FirstOrDefault();
            }

            CurrentFluorochrome = fluorochromes.FirstOrDefault();
        }

        public void DeleteChannel()
        {
            if (CurrentFluorochrome != null && CurrentChannel != null)
            {
                current.RemoveChannel(currentChannel);
            }

            CurrentChannel = current == null ? null : current.Channels.FirstOrDefault();
        }

        public void DuplicateFluorochrome()
        {
            if(CurrentFluorochrome == null)
                return;

            string name = CurrentFluorochrome.Name;

            while (FluorExists(name))
            {
                name += " (copy)";
            }

            var f = CurrentFluorochrome.Duplicate();
            f.Name = name;
            fluorochromes.Add(f);
            CurrentFluorochrome = f;
        }

        public void TestForCounterstains()
        {
            Notify("IsCounterstainSelected", "MultipleCounterstainsSelected");
        }

        public bool IsCounterstainSelected
        {
            get
            {
                if (current == null)
                    return false;

                int counterstains = current.Channels.Count(c => c.IsCounterstain);
                return counterstains != 0;
            }
        }

        public bool MultipleCounterstainsSelected
        {
            get
            {
                if (current == null)
                    return false;

                int counterstains = current.Channels.Count(c => c.IsCounterstain);
                return counterstains > 1;
            }
        }

        private delegate bool ComparerFunc(string a, string b);

        public void TryToGuessCurrentChannelRenderColor()
        {
            if (currentChannel == null)// || currentChannel.RenderColor != DefaultColor)
                return;

            var channelName = currentChannel.DisplayName.ToLower();
            
            ComparerFunc compare = (a, b) => string.Compare(a, b, StringComparison.CurrentCultureIgnoreCase) == 0;

            if (compare(channelName, "dapi") || compare(channelName, "blue") || compare(channelName, "b"))
            {
                currentChannel.RenderColor = Colors.Blue;
            }
            else if (compare(channelName, "spectrum orange") || compare(channelName, "red") || compare(channelName, "r"))
            {
                currentChannel.RenderColor = Colors.Red;
            }
            else if (compare(channelName, "spectrum green") || compare(channelName, "green") || compare(channelName, "g"))
            {
                currentChannel.RenderColor = Colors.LimeGreen;
            }
        }
    }
}
