﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

namespace AI
{
    public class ExposureSlider : Control
    {
        public static DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(double), typeof(ExposureSlider), new FrameworkPropertyMetadata(1.0, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, new PropertyChangedCallback(OnValueChanged)));
        public static DependencyProperty IncrementProperty = DependencyProperty.Register("Increment", typeof(double), typeof(ExposureSlider), new FrameworkPropertyMetadata(1.0));
        public event RoutedPropertyChangedEventHandler<double> ValueChanged;

        private FrameworkElement thumb;
        private FrameworkElement track;
        private bool drag;

        static ExposureSlider()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ExposureSlider), new FrameworkPropertyMetadata(typeof(ExposureSlider)));
        }

        public ExposureSlider()
        {
            SizeChanged += new SizeChangedEventHandler(ExposureSlider_SizeChanged);
        }

        void ExposureSlider_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            OnRangeChanged(this, new DependencyPropertyChangedEventArgs());
        }

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, Math.Max(Minimum, Math.Min(Maximum, value))); }
        }

        public double Minimum
        {
            get { return 1; }
        }

        public double Maximum
        {
            get { return 5000; }
        }

        public double Increment
        {
            get { return (double)GetValue(IncrementProperty); }
            set { SetValue(IncrementProperty, value); }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            RepeatButton decrement = (RepeatButton)GetTemplateChild("PART_Decrement");
            if(decrement != null)
                decrement.Click += new RoutedEventHandler(decrement_Click);

            RepeatButton increment = (RepeatButton)GetTemplateChild("PART_Increment");
            if(increment != null)
                increment.Click += new RoutedEventHandler(increment_Click);
            
            thumb = (FrameworkElement)GetTemplateChild("PART_Thumb");
            track = (FrameworkElement)GetTemplateChild("PART_Track");
            if (track != null)
            {
                track.MouseLeftButtonDown += new MouseButtonEventHandler(track_MouseLeftButtonDown);
                track.MouseLeftButtonUp += new MouseButtonEventHandler(track_MouseLeftButtonUp);
                track.MouseMove += new MouseEventHandler(track_MouseMove);
            }
        }

        void track_MouseMove(object sender, MouseEventArgs e)
        {
            if (!drag)
                return;

            FrameworkElement feSender = (FrameworkElement)sender;
            Point pos = e.GetPosition(feSender);

            double p = (pos.X / track.ActualWidth);

            if (p <= 0.25)
            {
                Value = (p * 4) * 10;
            }
            else if (p <= 0.5)
            {
                Value = ((p - 0.25) * 4) * 70 + 10;
            }
            else
            {
                Value = ((p - 0.5) * 2) * 4920 + 80;
            }
        }

        void track_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ((FrameworkElement)sender).ReleaseMouseCapture();
            drag = false;
        }

        void track_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ((FrameworkElement)sender).CaptureMouse();
            drag = true;

            track_MouseMove(sender, e);
        }

        static void OnRangeChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ExposureSlider nud = sender as ExposureSlider;
            if (nud.thumb == null)
                return;

            if (nud.Value <= 10)
            {
                nud.thumb.Width = (nud.Value / 10) * (nud.track.ActualWidth * 0.25);
            }
            else if (nud.Value <= 80)
            {
                nud.thumb.Width = ((nud.Value - 10) / 70) * (nud.track.ActualWidth * 0.25) + (nud.track.ActualWidth * 0.25);
            }
            else
            {
                nud.thumb.Width = ((nud.Value - 80) / 4920) * (nud.track.ActualWidth / 2) + (nud.track.ActualWidth / 2);
            }

            
        }

        static void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ExposureSlider nud = sender as ExposureSlider;
            double old = nud.Value;

            OnRangeChanged(sender, e);

            if (nud.ValueChanged != null)
            {
                nud.ValueChanged(nud, new RoutedPropertyChangedEventArgs<double>(old, (double)e.NewValue));
            }
        }

        void increment_Click(object sender, RoutedEventArgs e)
        {
            Value = Math.Max(Minimum, Math.Min(Maximum, Value + Increment));
        }

        void decrement_Click(object sender, RoutedEventArgs e)
        {
            Value = Math.Max(Minimum, Math.Min(Maximum, Value - Increment));
        }
    }
}
