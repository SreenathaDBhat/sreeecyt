﻿using System;
using HW = AI.HardwareController;
using System.Windows.Media;
using System.Windows;
using System.Linq;
using System.Threading;
using System.Windows.Controls;
using System.Diagnostics;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows.Threading;

namespace AI
{
    public partial class FishCapture : IClosing
    {
        private FishCaptureController controller;
        private AI.HardwareController.HotKeyStageData stagedata = null;
        private AI.HardwareController.HardwareStatus hs;
        private BarcodeZapper zapReader;
        private HW.HardwareController hw = null;

        public FishCapture(HW.HardwareController hw, Camera cam, Guid initialSlideId)
        {
            this.hw = hw;
            controller = new FishCaptureController(hw, cam, Dispatcher);
            DataContext = controller;
            InitializeComponent();

            var ssw = (SlideStatusWatcher)FindResource("ssw");
            ssw.CheckForChanges();
            ssw.ResetChanges();

            ssw.NotificationsAdded += (s, e) =>
                {
                    notificationsBaloonThing.Visibility = Visibility.Visible;
                    ssw.ResetChanges();
                };

            Loaded += (s, e) =>
                {
                    LiveCameraWindow live = (LiveCameraWindow)FindResource("renderer");
                    live.HookLiveImage(controller.Camera);

                    zapReader = new BarcodeZapper();
                    zapReader.Zap += OnBarcodeZapped;
                    
                    StartHotkeys();

                    //if(controller.IsScopeAutomated)
                        USBJoystick.LaunchProcess();

                    Keyboard.Focus(this);


                    var slide = SlideStatusInfo.FromSlideId(initialSlideId);
                    if (slide != null)
                        HandleSlideSelection(slide);
                };

            Unloaded += (s, e) =>
                {
                    StopHotkeys();

                    if(controller != null)
                        controller.Unloaded();
                };

            CompositionTarget.Rendering += (s, e) =>
                {
                    liveHost.Render();
                };
        }

        private void StopHotkeys()
        {
            if (stagedata != null)
            {
                stagedata.PreventClosing(false);
                stagedata.Close();
                stagedata = null;
            }

            if (hs != null)
            {
                hs.PropertyChanged -= new System.ComponentModel.PropertyChangedEventHandler(hs_PropertyChanged);
                hs.Dispose();
            }

            AI.HardwareController.HotKeys.Stop();
        }

        private void StartHotkeys()
        {
            IntPtr handle = new System.Windows.Interop.WindowInteropHelper(Application.Current.MainWindow).Handle;
            int err = AI.HardwareController.HotKeys.Start(handle, true, hw, true, true);

            if (err == 0)
            {
                hs = AI.HardwareController.HotKeys.StatusNotifier;

                if (hs != null)
                    hs.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(hs_PropertyChanged);
            }
            else
                MessageBox.Show("Error starting Hot Keys. " + AI.HardwareController.HotKeys.LastError());
        }

        void hs_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "ShowStatusWindow":
                    Dispatcher.Invoke((ThreadStart)delegate
                    {
                        if (hs.ShowStatusWindow)
                        {
                            if (stagedata == null)
                            {
                                stagedata = new AI.HardwareController.HotKeyStageData(hs,zapReader);
                                stagedata.Owner = VisualTreeWalker.FindParentOfType<Window>(this);
                                stagedata.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                                stagedata.Show();
                            }

                            IntPtr handle = new System.Windows.Interop.WindowInteropHelper(stagedata).Handle;
                            AI.HardwareController.HotKeys.RegisterTempWindow(handle);
                            Application.Current.MainWindow.Focus();
                        }
                        else
                        {
                            AI.HardwareController.HotKeys.UnRegisterTempWindow();
                            Application.Current.MainWindow.Focus();

                            if (stagedata != null)
                            {
                                stagedata.PreventClosing(false);
                                stagedata.Close();
                                stagedata = null;
                            }
                        }

                    }, DispatcherPriority.Normal);
                    break;
            }
        }

        private void OnBack(object sender, System.Windows.RoutedEventArgs e)
        {
            UIStack.For(this).Pop(UIStackResult.Ok);
        }

        private void CanCaptureExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            if (controller != null)
            {
                e.CanExecute =
                    controller.CurrentSlide != null &&
                    controller.CurrentSlide.ValidCase &&
                    controller.CurrentSlide.ValidSlide &&
                    controller.CurrentFluorochrome != null &&
                    controller.CurrentFluorochrome.Channels.Count() > 0;
            }
            else e.CanExecute = false;
        }

        private void CaptureExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            if (queueCheck.IsChecked.GetValueOrDefault(false))
            {
                controller.CaptureLater(int.Parse(e.Parameter as string) > 1);
            }
            else
            {
                controller.CaptureNow(int.Parse(e.Parameter as string) > 1);
            }
        }

        private void OnExposureChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double v = e.NewValue;
            controller.Camera.SetExposure((int)v);
        }

        private void OnTraySelected(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            string tray = (string)((ComboBox)sender).SelectedItem;

            int iTray = (tray == "empty") ? -1 : int.Parse(tray);

            ThreadPool.QueueUserWorkItem(delegate
            {
                controller.SelectTray(iTray, Dispatcher);
            });
        }

        private void OnShowZStackOptions(object sender, RoutedEventArgs e)
        {
            stackingOptions.IsOpen = true;
        }

        private void OnCancelCapture(object sender, RoutedEventArgs e)
        {
            controller.CaptureInProgress.Cancel();
        }

        private void CanClearTrayAssignmentsExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            if(controller != null)
                e.CanExecute = !controller.NeedToLoadTray;
            else e.CanExecute = false;
        }

        private void ClearTrayAssignmentsExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            controller.ClearTraySlideAssignments();
        }


        private DateTime lastMouseDown;

        private void OnDropAutoSetupDown(object sender, MouseButtonEventArgs e)
        {
            lastMouseDown = DateTime.Now;
        }

        private void OnDropAutoSetup(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if ((DateTime.Now - lastMouseDown).TotalSeconds > 1)
                return;

            var pt = e.GetPosition(liveHost);
            controller.AutoCameraSetupViaRoi(pt);
        }

        private void OnCancelAutoSetup(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            controller.CancelAutoSetup();
        }

        public void Closed()
        {
            StopHotkeys();

            if (controller != null)
            {
                controller.Unloaded();
                controller.Dispose();
                controller = null;
            }
        }

        public bool Cleanup()
        {
            controller.Unloaded();
            return true;
        }

        private void OnEnableStacking(object sender, RoutedEventArgs e)
        {
            controller.StackingEnabled = true;
        }

        private void OnDisableStacking(object sender, RoutedEventArgs e)
        {
            controller.StackingEnabled = false;
            stackingOptions.IsOpen = false;
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
            zapReader.CharacterRead(e);
        }

        private void OnScanBarcodes(object sender, RoutedEventArgs e)
        {
			try
			{
				ScanBarcodesWindow window = new ScanBarcodesWindow(controller.Tray);
				if (window.ShowDialog().GetValueOrDefault(false))
					controller.CurrentSlide = controller.Tray.First();
			}
			catch (Exception ex)
			{
				ExceptionUtility.ShowLog(ex, "Scan Barcodes");
			}
        }

        private void OnAssignSlide(object sender, RoutedEventArgs e)
        {
			try
			{
				var slide = (FishSlide)((FrameworkElement)sender).Tag;
				var wnd = new ChooseSlideWindow();
				wnd.Owner = VisualTreeWalker.FindParentOfType<Window>(this);
				if (wnd.ShowDialog().GetValueOrDefault(false))
				{
					controller.AssignCase(slide, wnd.SelectedSlide.Id);
				}
			}
			catch (Exception ex)
			{
				ExceptionUtility.ShowLog(ex, "Assign Case");
			}
        }

        private void OnMovetoBayClick(object sender, RoutedEventArgs e)
        {
			try
			{
				controller.CurrentSlide = (FishSlide)((FrameworkElement)sender).Tag;
			}
			catch (Exception ex)
			{
				ExceptionUtility.ShowLog(ex, "Move to bay");
			}
        }

        private void OnClearSlideAssignment(object sender, RoutedEventArgs e)
        {
            controller.ClearTraySlideAssignment((FishSlide)((FrameworkElement)sender).Tag);
        }

        private void OnToggleShutter(object sender, RoutedEventArgs e)
        {
            controller.ToggleShutter();
        }

        private void OnUnloadTray(object sender, RoutedEventArgs e)
        {
            controller.UnloadTray();
        }

        private void OnClearAutoSetupRoi(object sender, MouseButtonEventArgs e)
        {
            controller.ClearAutoSetupRoi();
        }

        private void OnClearAutoSetupRoiClick(object sender, RoutedEventArgs e)
        {
            controller.ClearAutoSetupRoi();
        }

        private void OnCaptureQueue(object sender, RoutedEventArgs e)
        {
            controller.BeginCapturingQueue(controller.CaptureQueue);
        }

        private void OnClearQueue(object sender, RoutedEventArgs e)
        {
            controller.CaptureQueue.Clear();
            queueCheck.IsChecked = false;
        }

        private void OnSetObjective(object sender, RoutedEventArgs e)
        {
            string obj = (string)((FrameworkElement)sender).Tag;
            var all = controller.Objectives.ToList();
            int index = all.IndexOf(obj);
            controller.CurrentObjective = index;
        }

        private void OnRelocateToMarkupRegion(object sender, RoutedEventArgs e)
        {
            var r = (CaptureRegion)((FrameworkElement)sender).Tag;
            var center = r.RegionCenter;
            controller.MoveStageTo(center);
            controller.WholeSlideLocation.MoveTo(center);
            controller.WholeSlideLocation.ZoomTo(15);
        }

        private void OnCaptureMarkupRegion(object sender, RoutedEventArgs e)
        {
            var r = (CaptureRegion)((FrameworkElement)sender).Tag;
            controller.BeginCapturingSpecificRegion(controller.MarkupCaptureQueue, r);
        }

        private void OnWholeSlideClicked(object sender, MouseButtonEventArgs e)
        {
            var pos = ((WholeSlideView)sender).TranslateMouseIntoSlideCoords(e);
            controller.MoveStageTo(pos);
        }

        private void OnEditFluorochromes(object sender, RoutedEventArgs e)
        {
            controller.WaitingForHardware = true;

            var duplicates = from f in controller.Fluorochromes select f.Duplicate();
            
            var wnd = new EditFluorochromesWindow(duplicates, controller.Dichroics, controller.Excitations);
            wnd.Owner = VisualTreeWalker.FindParentOfType<Window>(this);

            if (wnd.ShowDialog().GetValueOrDefault(false))
            {
                Fluorochrome.SaveAll(wnd.Fluorochromes);
            }

            controller.LoadFluorochromes();

            if (wnd.SelectedFluorochrome != null)
            {
                controller.CurrentFluorochrome = wnd.SelectedFluorochrome;
            }
            controller.WaitingForHardware = false;
        }

        private void CanExecuteTrue(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RespondToReadyForCapture(object sender, ExecutedRoutedEventArgs e)
        {
			try
			{
				var slide = (SlideStatusInfo)e.Parameter;
				HandleSlideSelection(slide);

                var ssw = (SlideStatusWatcher)FindResource("ssw");
                if (ssw.Slides.Count() == 1)
                {
                    liveImageRadio.IsChecked = true;
                }
			}
			catch (Exception ex)
			{
				ExceptionUtility.ShowLog(ex, "Ready for capture");
			}
        }

        private void OnFilterNotifications(object sender, RoutedEventArgs e)
        {
            var ssw = (SlideStatusWatcher)FindResource("ssw");
            Notifications n = new Notifications(ssw);
            n.Owner = VisualTreeWalker.FindParentOfType<Window>(this);
            n.ShowDialog();

            ssw.ResetChanges();
            ssw.SaveStatusMonitorSettings();
            ssw.CheckForChanges();
        }

        void OnBarcodeZapped(object sender, BarcodeZapEventArgs e)
        {
			try
			{
                var slide = SlideStatusInfo.FromBarcode(e.Barcode);
                if(slide != null)
                    HandleSlideSelection(slide);
			}
			catch( Exception ex)
			{
				ExceptionUtility.ShowLog(ex, "Barcode read");
			}
        }

        private void HandleSlideSelection(SlideStatusInfo slide)
        {
            if (hw.NumberOfTrays == 1 && hw.NumberOfSlidesPerTray == 1)
            {
                controller.AssignCase(controller.Tray.First(), slide.SlideId);
                return;
            }

            var wnd = new AssignSlideToBayAfterZapWindow(slide, controller.Tray);
            wnd.Owner = VisualTreeWalker.FindParentOfType<Window>(this);

            if (wnd.ShowDialog().GetValueOrDefault(false))
            {
                controller.AssignCase(wnd.SelectedBay, wnd.ZappedSlideInfo.SlideId);
            }
        }

        private void OnSlideStatusComboChanged(object sender, SelectionChangedEventArgs e)
        {
            controller.SaveCurrentSlideStatus(((string)((ComboBox)sender).SelectedItem));
            ((SlideStatusWatcher)FindResource("ssw")).CheckForChanges();
        }

        private void OnShowNotifications(object sender, RoutedEventArgs e)
        {
            notificationsBaloonThing.Visibility = Visibility.Collapsed;
        }

        private void OnDeleteRecentCapture(object sender, RoutedEventArgs e)
        {
            var image = (ImageFrame)((FrameworkElement)sender).Tag;
            controller.DeleteRecentCapture(image);
        }

        private void OnDeleteAll(object sender, RoutedEventArgs e)
        {
            controller.DeleteAllRecentCaptures();
        }

        private void OnEnterRecentActivity(object sender, RoutedEventArgs e)
        {
            ((SlideStatusWatcher)FindResource("ssw")).SetUserMarkToNow();
        }

        private void OnToggleMarkupMode(object sender, RoutedEventArgs e)
        {
            controller.ToggleMarkupMode();
        }

        private void OnDone(object sender, RoutedEventArgs e)
        {
            UIStack.For(this).Pop(UIStackResult.Ok);
        }

        private void manualLoad_Click(object sender, RoutedEventArgs e)
        {
            if (manualLoad.Content.ToString() == "Manual Load")
            {
                controller.ManualTrayLoadStart();
                manualLoad.Content = "Return Stage";
            }
            else
            {
                controller.ManualTrayLoadEnd();
                manualLoad.Content = "Manual Load";
            }
        }
    }

}
