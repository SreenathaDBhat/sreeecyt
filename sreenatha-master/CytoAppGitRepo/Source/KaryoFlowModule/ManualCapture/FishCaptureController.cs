﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HW = AI.HardwareController;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using System.IO;
using System.Windows.Input;
using System.Threading;
using System.Windows.Media;
using System.Windows.Threading;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
namespace AI
{
    public class FishSlide :  Notifier
    {
        private string caseName, slideName, slideStatus;
        private Guid caseId, slideId;

        public Guid SlideId { get { return slideId; } set { slideId = value; Notify("SlideId", "ValidSlide", "GotCaseButNoSlide"); } }
        public string SlideName { get { return slideName; } set { slideName = value; Notify("SlideName"); } }
        public string SlideStatus 
        { 
            get { return slideStatus; } 
            set 
            { 
                slideStatus = value; 
                Notify("SlideStatus"); 
            } 
        }
        public string CaseName { get { return caseName; } set { caseName = value; Notify("CaseName"); } }
        public int BayIndex { get; set; }
        public int SlideIndex { get; set; }
        public double BayIndexZeroBased { get { return BayIndex - 1; } }
        public Guid CaseId 
        { 
            get { return caseId; } 
            set 
            {
                if (value != caseId)
                {
                    caseId = value;
                    Notify("CaseId", "ValidCase", "GotCaseButNoSlide");
                }
            } 
        }

        public bool ValidCase { get { return CaseId != Guid.Empty; } }
        public bool ValidSlide { get { return SlideId != Guid.Empty; } }
        public bool GotCaseButNoSlide { get { return CaseId != Guid.Empty && SlideId == Guid.Empty; } }

        private bool gotRegions;
        public bool GotMarkedUpRegions
        {
            get { return gotRegions; }
            set { gotRegions = value; Notify("GotMarkedUpRegions"); }
        }
    }

    public class FishCaptureController : Notifier, IDisposable
    {
        public static RoutedUICommand Capture = new RoutedUICommand("Capture", "Capture", typeof(FishCaptureController));
        public static RoutedUICommand EnqueueCapture = new RoutedUICommand("EnqueueCapture", "EnqueueCapture", typeof(FishCaptureController));
        public static RoutedUICommand AddFluorChannel = new RoutedUICommand("AddFluorChannel", "AddFluorChannel", typeof(FishCaptureController));
        public static RoutedUICommand ClearTrayAssignments = new RoutedUICommand("ClearTrayAssignments", "ClearTrayAssignments", typeof(FishCaptureController));
        
        private List<FishSlide> tray;
        private FishSlide currentSlide;
        private HW.HardwareController hw;
        private Camera cam;
        private int numSlidesPerTray, numTrays, trayIndex;
        private int stackSlices;
        private double stackGap;
        private IEnumerable<Fluorochrome> fluorochromes;
        private Fluorochrome currentFluorochrome;
        private ChannelInfo currentFluorochromeChannel;
        private List<Filter> dichroics, excitations;
        private string[] objectives;
        private bool waitingForHardware, captureFullStack, stackingEnabled, trayOnStage;
        private Capture captureInProgress;
        private Dispatcher dispatcher;
        private Connect.BlobTrickler trickler;
        private CaptureQueue manualCaptureQueue;
        private CaptureQueue markedUpCaptureQueue;
        private Thread monitorSlidePosition;
        private ObservableCollection<ImageFrame> recentCaptures;
        private Boolean[] trayPresent = null;
        private bool markupCaptureMode;
        private bool moveToBayRequired = true;

        public FishCaptureController(HW.HardwareController hw, Camera cam, Dispatcher uiDispatcher)
        {
            this.dispatcher = uiDispatcher;
            this.hw = hw;
            this.cam = cam;
            cam.GoLive();

            recentCaptures = new ObservableCollection<ImageFrame>();
            
            cam.AutoExposed += new EventHandler(camera_AutoExposed);
            cam.AutoExposing += new EventHandler<AutoExposingEventArgs>(camera_AutoExposing);

            manualCaptureQueue = new CaptureQueue();

            int n = 0;
            hw.GetObjectives(ref n, ref objectives);
            IsScopeAutomated = false; // TODO: decide based on hardware!!!!!
            PopulateFilterList(hw.ExcitationFilters, ref excitations);
            PopulateFilterList(hw.DichroicFilters, ref dichroics);

            LoadFluorochromes();
            LoadPrefs();

            using (var db = new Connect.Database())
            {
                SlideStatuses = (from s in db.SlideStatus
                                 select s.description).ToArray();
            }

            trickler = new Connect.BlobTrickler();

            numSlidesPerTray = hw.NumberOfSlidesPerTray;
            numTrays = hw.NumberOfTrays;

            trayIndex = -1;

            if (numTrays == 1)
                trayIndex = 1;
            else
                hw.GetOccupiedTrayPositions(numTrays, ref trayPresent);

            TrayOnStage = hw.IsSlideOnStage();

            tray = new List<FishSlide>(from s in Enumerable.Range(0, numSlidesPerTray)
                                       select new FishSlide
                                       {
                                           BayIndex = s + 1,
                                           SlideIndex = s + 1,
                                           CaseName = "",
                                           SlideName = ""
                                       });
            
            moveToBayRequired = false;
            CurrentSlide = tray.First();

            StartMonitoringSlidePosition();

        }

        private void StartMonitoringSlidePosition()
        {
            monitorSlidePosition = new Thread(SlidePositionThreadMain);
             monitorSlidePosition.IsBackground = true;
            monitorSlidePosition.Name = "Monitor Slide Position Thread";
            monitorSlidePosition.Start();
        }

        private void StopMonitoringSlidePosition()
        {
            if (monitorSlidePosition != null && monitorSlidePosition.IsAlive)
            {
                monitorSlidePosition.Abort();
            }
        }

        void SlidePositionThreadMain(object state)
        {
            while (true)
            {
                if (hw.SetMicLock(true) != 0)
                    continue;

                try
                {
                    float x = 0, y = 0, z = 0;
                    if (hw.GetStagePosition(ref x, ref y, ref z) == 0)
                    {
                        dispatcher.Invoke((ThreadStart)delegate
                        {
                            SlidePositionX = x;
                            SlidePositionY = y;

                        }, DispatcherPriority.Normal);
                    }
                }
                finally
                {
                    hw.SetMicLock(false);
                }

                Thread.Sleep(500);
            }
        }

        private void LoadPrefs()
        {
            FileInfo file = AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\capturecontroller.capturesettings");
            if (file.Exists)
            {
                XElement xml = XElement.Load(file.FullName);
                StackingEnabled = Parsing.TryParseBool(Parsing.ValueOrDefault(xml.Attribute("StackingEnabled"), "False"), false);
                StackSlices = Parsing.TryParseInt(Parsing.ValueOrDefault(xml.Attribute("ZStackSize"), "10"), 10);
                StackGap = Parsing.TryParseDouble(Parsing.ValueOrDefault(xml.Attribute("ZStackGap"), "5.0"), 5.0);
                CaptureFullStack = Parsing.TryParseBool(Parsing.ValueOrDefault(xml.Attribute("SaveStacks"), "False"), false);

                var fluor = xml.Attribute("Fluorochrome");
                
                // look for old style fluor
                if (fluor == null)
                {
                    var fluorNode = xml.Element("Fluorochrome");
                    if (fluorNode != null)
                        fluor = fluorNode.Attribute("Id");
                }

                if (fluor != null)
                {
                    Guid fluorId = new Guid(fluor.Value);
                    foreach (var f in fluorochromes)
                    {
                        if (f.Id == fluorId)
                        {
                            CurrentFluorochrome = f;
                            break;
                        }
                    }
                }
            }

            if (currentFluorochrome != null && currentFluorochrome.Channels.Count() > 0)
                MakeCurrentChannel(currentFluorochrome.Channels.First());
        }

        private void SavePrefs()
        {
            FileInfo file = AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\capturecontroller.capturesettings");
            if (file.Exists)
                file.Delete();

            XElement xml = new XElement("CaptureController",
                new XAttribute("StackingEnabled", stackingEnabled),
                new XAttribute("ZStackSize", stackSlices),
                new XAttribute("ZStackGap", stackGap),
                new XAttribute("SaveStacks", captureFullStack),
                new XAttribute("Fluorochrome", currentFluorochrome == null ? Guid.Empty : currentFluorochrome.Id));

            xml.Save(file.FullName);
        }

        public void Unloaded()
        {
            WaitingForHardware = true;
            cam.StopLive();
            SavePrefs();
            WaitingForHardware = false;
            StopMonitoringSlidePosition();
        }

        private void PopulateFilterList(string[] hardwareFilters, ref List<Filter> filters)
        {
            if (hardwareFilters == null || hardwareFilters.Length == 0)
                return;

            int i = 1;
            filters = new List<Filter>(from f in hardwareFilters
                                       select new Filter
                                       {
                                           Name = f,
                                           RotorPosition = i++
                                       });
        }

        public void LoadFluorochromes()
        {
            fluorochromes = Fluorochrome.LoadFluorochromes(dichroics, excitations);
            currentFluorochrome = fluorochromes.FirstOrDefault();
            Notify("Fluorochromes", "CurrentFluorochrome");
        }

        public Camera Camera
        {
            get { return cam; }
        }

        public HW.HardwareController Hardware
        {
            get { return hw; }
        }
        
        public int TrayIndex
        {
            get { return trayIndex; }
        }

        public bool GotMoreThanOneTray
        {
            get { return numTrays > 1; }
        }

        public WholeSlideDisplayLocation WholeSlideLocation
        {
            get;
            private set;
        }

        public IEnumerable<WholeSlide> WholeSlideImages
        {
            get;
            private set;
        }

        public IEnumerable<Filter> Dichroics { get { return dichroics; } }
        public IEnumerable<Filter> Excitations { get { return excitations; } }
        public IEnumerable<string> Objectives
        { 
            get 
            { 
                return objectives; 
            } 
        }
        public IEnumerable<Fluorochrome> Fluorochromes { get { return fluorochromes; } }


        public SolidColorBrush ObjectiveStripColorFluff
        {
            get
            {
                var cur = Objectives.ElementAt(CurrentObjective);
                switch(cur)
                {
                    case "1.25X":
                        return Brushes.Black;

                    case "5.00X":
                        return Brushes.Red;

                    case "10.00X":
                        return Brushes.Yellow;

                    case "20.00X":
                        return Brushes.LimeGreen;

                    case "40.00X":
                        return Brushes.Cyan;

                    case "60.00X":
                        return Brushes.Blue;

                    case "100.00X":
                        return Brushes.White;

                    default:
                        return Brushes.White;
                }
            }
        }

        public int CurrentObjectiveBase1
        {
            get
            {
                return hw.Objective + 1;
            }
            set
            {
                hw.MoveToObjective(value - 1, true);
                Notify("ObjectiveStripColorFluff");
            }
        }
        public int CurrentObjective
        {
            get
            {
                return hw.Objective;
            }
            set
            {
                hw.MoveToObjective(value, true);
                Notify("ObjectiveStripColorFluff");
            }
        }

        public bool IsScopeAutomated { get; private set; }

        public void MakeCurrentChannel(ChannelInfo fl)
        {
            if (currentFluorochrome != null)
            {
                foreach (var c in currentFluorochrome.Channels)
                    c.IsCurrent = false;
            }

            currentFluorochromeChannel = fl;

            if (fl != null)
            {
                fl.IsCurrent = true;

                if (fl.Dichroic != null)
                    hw.MoveToDichroic(fl.Dichroic.RotorPosition - 1, true);

                if (fl.Excitation != null)
                    hw.MoveToExcitation(fl.Excitation.RotorPosition - 1, true);

                cam.SetExposure(fl.Exposure);
            }

            //hw.SetFluorescenceLampShutter(0, true);
            Notify("CurrentChannel");
        }

        public ChannelInfo CurrentChannel
        {
            get { return currentFluorochromeChannel; }
            set 
            { 
                MakeCurrentChannel(value);
            }
        }
        
        public Fluorochrome CurrentFluorochrome
        {
            get { return currentFluorochrome; }
            set 
            { 
                currentFluorochrome = value; 
                Notify("CurrentFluorochrome");

                if(currentFluorochrome != null && currentFluorochrome.Channels.Count() > 0)
                    MakeCurrentChannel(currentFluorochrome.Channels.First());
            }
        }

        public bool CaptureFullStack
        {
            get { return captureFullStack; }
            set { captureFullStack = value; Notify("CaptureFullStack"); }
        }

        public bool StackingEnabled
        {
            get { return stackingEnabled; }
            set { stackingEnabled = value; Notify("StackingEnabled"); }
        }

        public int StackSlices
        {
            get { return stackSlices; }
            set { stackSlices = value; Notify("StackSlices"); }
        }

        public double StackGap
        {
            get { return stackGap; }
            set { stackGap = value; Notify("StackGap"); }
        }

        public IEnumerable<FishSlide> Tray { get { return tray; } }
        public FishSlide CurrentSlide
        {
            get { return currentSlide; }
            set
            {
                bool differentCase = (currentSlide == null) || currentSlide.CaseId != value.CaseId;

                currentSlide = value;
                Notify("CurrentSlide");

                if(moveToBayRequired)
                    WaitingForHardware = true;

                if (differentCase)
                {
                    LoadWholeImages(value.CaseId);
                }

                LoadDefferedCaptures(value);
                MarkupCaptureMode = (markedUpCaptureQueue != null && markedUpCaptureQueue.GotRegions);

                if (moveToBayRequired)
                {
                    ThreadPool.QueueUserWorkItem(delegate
                    {
                        if (hw.MoveToBay(value.BayIndex) != 0)
                        {
                            int errCode = 0;
                            string errMsg = string.Empty;
                            hw.GetLastError(ref errCode, ref errMsg);
                        }

                        dispatcher.Invoke((ThreadStart)delegate
                        {
                            WaitingForHardware = false;
                        }, DispatcherPriority.Normal);
                    });
                }
                
                // reset for next CurrentSlide
                // assignment
                moveToBayRequired = true;

            }
        }

        private void LoadWholeImages(Guid caseId)
        {
			using (Connect.Database db = new Connect.Database())
			{
				var slides = db.Slides.Where(s => s.caseId == caseId);

				List<WholeSlide> all = new List<WholeSlide>();
				WholeSlideLocation = new WholeSlideDisplayLocation();
				WholeSlideLocation.ZoomBy(-0.05);

				foreach (var slide in slides)
				{
					if (db.SlideImageTiles.Where(t => t.slideId == slide.slideId).Count() > 0)
					{
						var w = new WholeSlide(caseId, slide.slideId);
						w.DisplayLocation = WholeSlideLocation;
						all.Add(w);
					}
				}

				if (all.Count > 0)
				{
					WholeSlideImages = all;
					Notify("WholeSlideImages", "WholeSlideLocation");
				}
				else
				{
					NullOutWholeImageDisplay();
				}
			}
        }

        public Capture CaptureInProgress
        {
            get { return captureInProgress; }
            set { captureInProgress = value; Notify("CaptureInProgress"); }
        }

        public void AssignCase(FishSlide slide, Guid slideId)
        {
            using (var lims = Connect.LIMS.New())
            using (var db = new Connect.Database())
            {
                var dbSlide = db.Slides.Where(s => s.slideId == slideId).First();
                var dbCase = db.Cases.Where(c => c.caseId == dbSlide.caseId).First();
                var ls = lims.GetSlide(dbSlide.limsRef);
                var lc = lims.GetCase(dbCase.limsRef);

                Guid oldSlide = slide.SlideId;
                bool differentCase = slide.CaseId != dbCase.caseId;

                bool differentSlide = slide.SlideId != slideId;
                if (differentSlide && oldSlide != Guid.Empty)
                    CaptureQueue.RemoveQueue(oldSlide);

                var status = db.SlideStatus.Where(s => s.sequence == dbSlide.slideStatus).First();

                slide.CaseId = dbCase.caseId;
                slide.CaseName = lc.Name;
                slide.SlideId = slideId;
                slide.SlideName = ls.Name;
                slide.SlideStatus = status.description;
                slide.GotMarkedUpRegions = db.DeferredCaptures.Where(d => d.slideId == slideId).Count() > 0;

                if (slide == CurrentSlide)
                {
                    LoadDefferedCaptures(slide);
                    MarkupCaptureMode = (markedUpCaptureQueue != null && markedUpCaptureQueue.GotRegions);

                    if (differentCase)
                        LoadWholeImages(dbCase.caseId);
                }

                if (differentSlide)
                {
                    recentCaptures.Clear();
                }
            }
        }

        public Guid CreateSlide(Guid caseId, string slideName)
        {
            using (var db = new Connect.Database())
            using (var lims = Connect.LIMS.New())
            {
                var dbCase = db.Cases.Where(c => c.caseId == caseId).First();
                var limsCase = lims.GetCase(dbCase.limsRef);
                var newSlide = CaseManagement.CreateSlide(limsCase, slideName, "", SlideTypes.FISH);
                var dbSlide = db.Slides.Where(s => s.limsRef == newSlide.Id).First();
                return dbSlide.slideId;
            }
        }

        private void CaptureFrames(CaptureQueue queue, IEnumerable<CaptureRegion> allFrames, Dispatcher dispatcher)
        {
            CaptureAutoSetupParameters autoSetup = null;
            if (AutoSetupCenter != null)
            {
                autoSetup = new CaptureAutoSetupParameters
                {
                    AutoSetupCenter = this.AutoSetupCenter.Value
                };
            }

            CaptureStackingParameters stacking = null;
            if (StackingEnabled)
            {
                stacking = new CaptureStackingParameters
                {
                    Gap = StackGap,
                    Slices = StackSlices,
                    Mode = CaptureFullStack ? CaptureStackingParameters.StackingMode.FullStack : CaptureStackingParameters.StackingMode.MaximumProjection
                };
            }

            CaptureInProgress = new Capture(queue, hw, cam, dispatcher)
            {
                Fluorochrome = currentFluorochrome,
                Objective = CurrentObjectiveBase1,
                AutoSetupParameters = autoSetup,
                StackingParameters = stacking,
                RegionsToCapture = allFrames
            };

            CaptureInProgress.ChannelSnapped = (q, snapInfo) =>
            {
                MemoryStream stream = new MemoryStream();
                ImageFileSerialize.WriteChannelDataToStream(stream, snapInfo.Snap.ChannelPixels, true);
                trickler.Enqueue(snapInfo.Region.Case, snapInfo.Snap.Id + ".snap", stream.GetBuffer());
            };

            CaptureInProgress.ImageCaptured = (q, captureInfo) =>
            {
                dispatcher.Invoke((ThreadStart)delegate
                {
					try
					{
						SaveImageFrameInfoToDb(captureInfo);

						AddRecentCapture(captureInfo.Frame);

						foreach (var snap in captureInfo.Frame.AllSnaps)
							snap.ChannelPixels = null;
					}
					catch (Exception ex)
					{
						ExceptionUtility.ShowLog(ex, "Image Captured");
					}
                }, DispatcherPriority.Normal);
            };

            captureInProgress.Completed = (q, isCancelled) =>
            {
                dispatcher.Invoke((ThreadStart)delegate
                {
					try
					{
						if (isCancelled)
						{
							DeleteHalfCapturedQueue(captureInProgress);
						}

						AutoSetupCenter = null;
						Notify("AutoSetupCenter");

						CaptureInProgress = null;

						MakeCurrentChannel(currentFluorochrome.Channels.First());
						cam.GoLive();
					}
					catch (Exception ex)
					{
						ExceptionUtility.ShowLog(ex, "Capture completed");
					}
                }, DispatcherPriority.Normal);
            };


            captureInProgress.Go(currentSlide.SlideIndex);
        }

        private void AddRecentCapture(ImageFrame imageFrame)
        {
            recentCaptures.Add(imageFrame);

            while (recentCaptures.Count > 30)
                recentCaptures.RemoveAt(0);
        }

        public IEnumerable<ImageFrame> RecentCaptures
        {
            get { return recentCaptures; }
        }

        private void DeleteHalfCapturedQueue(Capture cap)
        {
			using (var db = new Connect.Database())
			{
				foreach (var snap in cap.Record.Snaps)
				{
                    Connect.BlobStream.Delete(snap.Snap.BlobId, db);
				}

				foreach (var frame in cap.Record.Frames)
				{
					var dbFrame = db.ImageFrames.Where(f => f.imageFrameId == frame.Frame.Id).First();

                    if((dbFrame.blobid != null) && (dbFrame.blobid != Guid.Empty))
                        Connect.BlobStream.Delete((Guid)dbFrame.blobid, db);					    
                        
                    db.ImageFrames.DeleteOnSubmit(dbFrame);

					if (recentCaptures.Contains(frame.Frame))
						recentCaptures.Remove(frame.Frame);
				}

				foreach (var r in cap.RegionsToCapture)
				{
					var def = db.DeferredCaptures.Where(d => d.regionId == r.DeferredCaptureId).First();
					def.status = (int)r.Status;
				}

				db.SubmitChanges();
			}
        }

        private FishSlide FindSlide(Guid slideId)
        {
            return tray.Where(s => s.SlideId == slideId).First();
        }

        private void SaveImageFrameInfoToDb(FrameRecord frameInfo)
        {
			using (var db = new Connect.Database())
			{
				ImageFrameDbLoad.ToDb(db, frameInfo.Frame, frameInfo.Region.Slide, frameInfo.Region.Case, ImageType.Raw, Guid.NewGuid());

				if (frameInfo.Region.DeferredCaptureId != Guid.Empty)
				{
					var defCap = db.DeferredCaptures.Where(d => d.regionId == frameInfo.Region.DeferredCaptureId).First();
					defCap.modified = frameInfo.Region.Modified;
					defCap.status = (int)frameInfo.Region.Status;
				}

				db.SubmitChanges();

				if (frameInfo.Frame.Thumbnail != null && frameInfo.Frame.AllSnaps.Count() > 0)
				{
					JpegBitmapEncoder encoder = new JpegBitmapEncoder();
					encoder.Frames.Add(BitmapFrame.Create(frameInfo.Frame.Thumbnail));

					MemoryStream stream = new MemoryStream();
					encoder.Save(stream);

					string path = frameInfo.Frame.Id.ToString() + ".jpg";
					trickler.Enqueue(frameInfo.Region.Case, path, stream.GetBuffer());
				}
			}
        }

        /// <summary>
        /// Switch to tray. trayIndex is 1 based. Naturally.
        /// </summary>
        /// <param name="trayIndex"></param>
        public void SelectTray(int trayIndex, Dispatcher dispatcher)
        {
            if (trayIndex == -1)
            {
                Notify("NeedToLoadTray");
                return;
            }

            CaptureQueue.Clear();
            this.ClearTraySlideAssignments();

            foreach (var slide in tray)
            {
                slide.SlideIndex = (trayIndex - 1) * numSlidesPerTray + slide.BayIndex;
            }

            dispatcher.Invoke((ThreadStart)delegate
            {
                WaitingForHardware = true;
            }, DispatcherPriority.Normal);

            int n = trayIndex - 1;

            //  This will swap slides if there is a slide on the tray
            //  that was put there from a previous load. It also calls a
            //  move to first bay on the micserver.
            int errCode = hw.LoadSlide_N((numSlidesPerTray * n) + 1, true);

            if (errCode == 0)
            {
                this.trayIndex = trayIndex;

                PopulateSlidesFromBarcodes();

                // Dont need to perfrom a MoveTobay
                //  after a LoadSlide_N as the objective
                //  is always positioned at first bay
                //  on the slide.
                moveToBayRequired = false;
                CurrentSlide = Tray.First();
            }
            else
            {
                string errMsg = string.Empty;
                hw.GetLastError(ref errCode, ref errMsg);
                if (errCode == -5058)
                    TrayOnStage = true;
            }
            
            dispatcher.Invoke((ThreadStart)delegate
            {
				try
				{
					LoadWholeImages(CurrentSlide.CaseId);
					LoadDefferedCaptures(CurrentSlide);

					WaitingForHardware = false;
					Notify("TrayIndex", "NeedToLoadTray");
				}
				catch (Exception ex)
				{
					ExceptionUtility.ShowLog(ex, "Select Tray");
				}
            }, DispatcherPriority.Normal);
        }

        private void PopulateSlidesFromBarcodes()
        {
            int bayNumber = 1;
            foreach (FishSlide slide in Tray)
            {
                string barcode = hw.GetBarcodeFromCurrentTray(bayNumber);
                bayNumber++;
                ScanBarcodeController.PopulateSlideDetails(slide, barcode);
            }
        }

        //public IEnumerable<int> Trays
        //{
        //    get
        //    {
        //        for (int i = numTrays; i >= 1; --i)
        //            yield return i;
        //    }
        //}

        public IEnumerable<String> Trays
        {
            get
            {
                if (numTrays > 1)
                {
                    for (int i = numTrays; i >= 1; --i)
                    {   
                        if(!trayPresent[i-1])
                            yield return "empty";
                        else
                            yield return i.ToString();
                    }
                }
                else
                    yield return numTrays.ToString();
            }
        }


        public bool NeedToLoadTray
        {
            get { return numTrays > 1 && trayIndex == -1 && (!TrayOnStage); }
        }

        public bool WaitingForHardware
        {
            get { return waitingForHardware; }
            set { waitingForHardware = value; Notify("WaitingForHardware"); }
        }

        public bool TrayOnStage
        {
            get { return trayOnStage; }
            set 
            { 
                trayOnStage = value; 
                Notify("TrayOnStage");
                Notify("NeedToLoadTray");
            }
        }

        public void ClearTraySlideAssignments()
        {
            NullOutWholeImageDisplay();

            foreach (var s in tray)
            {
                ClearTraySlideAssignment(s);
            }
        }

        private void NullOutWholeImageDisplay()
        {
            WholeSlideLocation = new WholeSlideDisplayLocation();
            WholeSlideLocation.ZoomBy(-0.05);
            var img = new WholeSlide[] { WholeSlide.Null };
            img[0].DisplayLocation = WholeSlideLocation;
            WholeSlideImages = img;
            Notify("WholeSlideImages", "WholeSlideLocation");
        }

        public void ClearTraySlideAssignment(FishSlide slide)
        {
            dispatcher.Invoke((ThreadStart)delegate
            {
                CaptureQueue.RemoveQueue(slide.SlideId);

                slide.CaseId = Guid.Empty;
                slide.CaseName = "";
                slide.SlideId = Guid.Empty;
                slide.SlideName = "";

                if (slide == CurrentSlide)
                {
                    markedUpCaptureQueue = null;
                    Notify("MarkupCaptureQueue");
                    MarkupCaptureMode = false;

                    recentCaptures.Clear();
                }
            }, DispatcherPriority.Normal);
        }


        public Point? AutoSetupCenter { get; set; }

        public void AutoCameraSetupViaRoi(Point pt)
        {
            AutoSetupCenter = pt;
            WaitingForHardware = true;
            Notify("AutoSetupCenter");

            ThreadPool.QueueUserWorkItem((WaitCallback)delegate
            {
                cam.AutoExpose(pt.X, pt.Y, 100, false);
            });
        }

        public void CancelAutoSetup()
        {
            cam.CancelAutoExpose();
            AutoSetupCenter = null;
            WaitingForHardware = false;
            Notify("AutoSetupCenter");
        }

        public void ClearAutoSetupRoi()
        {
            AutoSetupCenter = null;
            Notify("AutoSetupCenter");
        }

        private void camera_AutoExposing(object sender, AutoExposingEventArgs e)
        {
            if (currentFluorochrome == null)
                return;

            dispatcher.Invoke((ThreadStart)delegate
            {
                foreach (var c in currentFluorochrome.Channels)
                {
                    if (c.IsCurrent)
                    {
                        c.Exposure = e.Exposure;
                    }
                }

            }, DispatcherPriority.Normal);
        }

        private void camera_AutoExposed(object sender, EventArgs e)
        {
            dispatcher.Invoke((ThreadStart)delegate
            {
                WaitingForHardware = false;

            }, DispatcherPriority.Normal);
        }

        public void MoveStageTo(Point point)
        {
            //Size scale = MicroscopeFile.ImageScale(CurrentObjective);
            //Size imgSize = new Size(cam.ImageWidth, cam.ImageHeight);
            //hw.MoveStageTo(point.X + ((imgSize.Width * scale.Width) / 2), point.Y - ((imgSize.Height * scale.Height)));

            hw.MoveStageTo(point.X, point.Y);
        }

        public void ToggleShutter()
        {
            int v = 0;
            hw.GetFluorescenceLampShutter(ref v);
            hw.SetFluorescenceLampShutter(v == 0 ? 1 : 0, true);
        }

        public void UnloadTray()
        {
            CaptureQueue.Clear();

            if (hw.IsSlideOnStage())
            {
                int err = hw.UnloadSlide_N(trayIndex * numSlidesPerTray + currentSlide.BayIndex);
                TrayOnStage = hw.IsSlideOnStage();  // Check again incase unload failed
            }

            if (!TrayOnStage)
            {
                trayIndex = -1;
                Notify("TrayIndex", "NeedToLoadTray", "WholeSlideImages", "WholeSlideLocation");
            }
        }

        public void ManualTrayLoadEnd()
        {
            hw.SlideLoader_ManualTrayLoadDone();
            TrayOnStage = hw.IsSlideOnStage();

            if (!TrayOnStage)
            {
                trayIndex = -1;
                Notify("TrayIndex", "NeedToLoadTray", "WholeSlideImages", "WholeSlideLocation");
            }
        }

        public void ManualTrayLoadStart()
        {
            hw.SlideLoader_ManualTrayLoad();
        }

        public void CaptureLater(bool grid)
        {
            var region = BuildGrid(grid);
            manualCaptureQueue.Enqueue(region);
        }

        public void CaptureNow(bool grid)
        {
            var region = BuildGrid(grid);

            cam.StopLive();
            ThreadPool.QueueUserWorkItem(delegate(object state)
            {
                var disp = (Dispatcher)state;
                CaptureQueue q = new CaptureQueue();
                q.Enqueue(region);
                CaptureFrames(q, q.CaptureRegions, disp);

                var center = hw.StagePosition;
                hw.MoveStageTo(center);

            }, Dispatcher.CurrentDispatcher);
        }

        private CaptureRegion BuildGrid(bool grid3x3)
        {
            var center = hw.StagePosition;
            Guid group = Guid.NewGuid();

            if(grid3x3)
                return DiscreetFramesRegion.Generate3x3(center, CurrentSlide.CaseId, CurrentSlide.SlideId, CurrentSlide.SlideIndex);
            else
                return DiscreetFramesRegion.Generate1x1(center, CurrentSlide.CaseId, CurrentSlide.SlideId, CurrentSlide.SlideIndex);
        }

        public CaptureQueue CaptureQueue
        {
            get { return manualCaptureQueue; }
        }

        public CaptureQueue MarkupCaptureQueue
        {
            get { return markedUpCaptureQueue; }
        }

        public void BeginCapturingQueue(CaptureQueue queue)
        {
            BeginCapturingQueue(queue, queue.CaptureRegions);
        }

        public void BeginCapturingSpecificRegion(CaptureQueue parentQueue, CaptureRegion region)
        {
            BeginCapturingQueue(parentQueue, new CaptureRegion[] { region });
        }

        public void BeginCapturingQueue(CaptureQueue queue, IEnumerable<CaptureRegion> specificRegions)
        {
            var dispatcher = Dispatcher.CurrentDispatcher;
            ThreadPool.QueueUserWorkItem(delegate
            {
                CaptureFrames(queue, specificRegions, dispatcher);

                dispatcher.Invoke((ThreadStart)delegate
                {
                    queue.Refresh();
                    
                }, DispatcherPriority.Normal);
            });
        }

        private void LoadDefferedCaptures(FishSlide slide)
        {
			using (var db = new Connect.Database())
			{
				var deferred = db.DeferredCaptures.Where(s => s.slideId == slide.SlideId);
				var theGroup = Guid.NewGuid();

				markedUpCaptureQueue = null;
				if (deferred.Count() > 0)
				{
					markedUpCaptureQueue = new CaptureQueue();
                    int i = 1;

                    foreach (var d in deferred)
                    {
                        markedUpCaptureQueue.Enqueue(new ArbitraryRegion(new Rect(d.left, d.top, d.right - d.left, d.bottom - d.top))
                                                     {
                                                         Case = slide.CaseId,
                                                         Slide = slide.SlideId,
                                                         SlideIndex = slide.SlideIndex,
                                                         DeferredCaptureId = d.regionId,
                                                         Owner = d.owner,
                                                         Comment = d.comment,
                                                         Modified = d.modified,
                                                         Status = (CaptureStatus)d.status,
                                                         Identifier = (i++).ToString()
                                                     });
                    }
				}
			}

            Notify("MarkupCaptureQueue");
        }

        private double slidePositionX;
        public double SlidePositionX
        {
            get { return slidePositionX; }
            private set { slidePositionX = value; Notify("SlidePositionX"); }
        }

        private double slidePositionY;
        public double SlidePositionY
        {
            get { return slidePositionY; }
            private set { slidePositionY = value; Notify("SlidePositionY"); }
        }

        public IEnumerable<string> SlideStatuses { get; private set; }

        public void SaveCurrentSlideStatus(string status)
        {
            if (CurrentSlide != null && CurrentSlide.SlideId != Guid.Empty && CurrentSlide.ValidSlide)
            {
				using (var db = new Connect.Database())
				{
					var dbSlide = db.Slides.Where(s => s.slideId == CurrentSlide.SlideId).First();
					var dbStatus = db.SlideStatus.Where(s => s.description == status).First();
					dbSlide.slideStatus = dbStatus.sequence;
                    dbSlide.statusChanged = DateTime.Now;
					db.SubmitChanges();
				}
            }
        }

        public void DeleteRecentCapture(ImageFrame image)
        {
			using (var db = new Connect.Database())
			{
				var dbFrame = db.ImageFrames.Where(f => f.imageFrameId == image.Id).FirstOrDefault();
				if (dbFrame != null)
				{
                    if ((dbFrame.blobid != null) && (dbFrame.blobid != Guid.Empty))
                        Connect.BlobStream.Delete((Guid)dbFrame.blobid, db);					    

					db.ImageFrames.DeleteOnSubmit(dbFrame);
					db.SubmitChanges();

					recentCaptures.Remove(image);
				}
			}
        }

        internal void DeleteAllRecentCaptures()
        {
			using (var db = new Connect.Database())
			{
				foreach (var c in recentCaptures)
				{
					var dbFrame = db.ImageFrames.Where(f => f.imageFrameId == c.Id).FirstOrDefault();
					if (dbFrame != null)
					{

                        if ((dbFrame.blobid != null) && (dbFrame.blobid != Guid.Empty))
                            Connect.BlobStream.Delete((Guid)dbFrame.blobid, db);					    

						db.ImageFrames.DeleteOnSubmit(dbFrame);
					}
				}

				db.SubmitChanges();
				recentCaptures.Clear();
			}
        }

        public void Dispose()
        {
            cam.AutoExposed -= camera_AutoExposed;
            cam.AutoExposing -= camera_AutoExposing;
            cam = null;
        }

        public bool MarkupCaptureMode
        {
            get { return markupCaptureMode; }
            private set { markupCaptureMode = value; Notify("MarkupCaptureMode"); }
        }

        internal void ToggleMarkupMode()
        {
            MarkupCaptureMode = !MarkupCaptureMode;
        }
    }
}
