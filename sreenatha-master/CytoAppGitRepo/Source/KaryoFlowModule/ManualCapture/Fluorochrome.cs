﻿using System;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.IO;

namespace AI
{
    public class Fluorochrome : Notifier
    {
        private ObservableCollection<ChannelInfo> channels;
        private Guid id;
        private string name;

        public static Fluorochrome Empty
        {
            get
            {
                return new Fluorochrome(false);
            }
        }

        private Fluorochrome(bool wantId)
        {
            channels = new ObservableCollection<ChannelInfo>();
            Name = "New Fluorochrome List";
            if (wantId)
                id = Guid.NewGuid();
        }

        public Fluorochrome() : this(true)
        {
        }

        public IEnumerable<ChannelInfo> Channels
        {
            get { return channels; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; Notify("Name"); }
        }

        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }

        public void AddChannel(ChannelInfo channel)
        {
            channels.Add(channel);
        }

        public void RemoveChannel(ChannelInfo channel)
        {
            channels.Remove(channel);
        }

        public static Fluorochrome FromXml(XElement xml, IEnumerable<Filter> availableDichroics, IEnumerable<Filter> availableExcitationFilters)
        {
            if (xml == null)
                return null;

            Fluorochrome fl = new Fluorochrome();
            fl.Name = xml.Attribute("Name").Value;
            fl.id = new Guid(xml.Attribute("Id").Value);

            foreach (var ch in xml.Descendants("Channel"))
            {
                ChannelInfo channel = new ChannelInfo();
                channel.Dichroic = FindFilter(availableDichroics, Parsing.ValueOrDefault(ch.Attribute("Dichroic"), string.Empty));
                channel.Excitation = FindFilter(availableExcitationFilters, Parsing.ValueOrDefault(ch.Attribute("Excitation"), string.Empty));
                channel.Exposure = Convert.ToInt32(ch.Attribute("Exposure").Value);
                channel.IsStacked = Parsing.TryParseBool(Parsing.ValueOrDefault(ch.Attribute("Stacked"), "False"), false);
                channel.IsCounterstain = Parsing.TryParseBool(Parsing.ValueOrDefault(ch.Attribute("IsCounterstain"), "False"), false);

                byte r = Convert.ToByte(ch.Attribute("RenderColorR").Value);
                byte g = Convert.ToByte(ch.Attribute("RenderColorG").Value);
                byte b = Convert.ToByte(ch.Attribute("RenderColorB").Value);
                channel.RenderColor = Color.FromArgb((byte)255, r, g, b);

                fl.AddChannel(channel);
            }

            return fl;
        }

        private static Filter FindFilter(IEnumerable<Filter> availableFilters, string filterName)
        {
            return availableFilters == null ? null : availableFilters.Where(d => d.Name == filterName).FirstOrDefault();
        }

        public XElement ToXml()
        {
            XElement xml = new XElement("Flourochrome",
                new XAttribute("Name", Name),
                new XAttribute("Id", id),
                new XElement("Channels", channels.ToList().ConvertAll<XElement>(c => BuildChannelNode(c))));

            return xml;
        }

        private XElement BuildChannelNode(ChannelInfo c)
        {
            return new XElement("Channel",
                new XAttribute("Dichroic", c.Dichroic == null ? "Not Set" : c.Dichroic.Name),
                new XAttribute("Excitation", c.Excitation == null ? "Not Set" : c.Excitation.Name),
                new XAttribute("Exposure", c.Exposure),
                new XAttribute("Stacked", c.IsStacked),
                new XAttribute("IsCounterstain", c.IsCounterstain),
                new XAttribute("RenderColorR", c.RenderColor.R),
                new XAttribute("RenderColorG", c.RenderColor.G),
                new XAttribute("RenderColorB", c.RenderColor.B));
        }

        public Fluorochrome Duplicate()
        {
            var f = new Fluorochrome();
            f.channels = new ObservableCollection<ChannelInfo>(from c in channels
                                                               select c.Duplicate());
            f.id = Guid.NewGuid();
            f.Name = Name;
            return f;
        }

        public static IEnumerable<Fluorochrome> LoadFluorochromes(IEnumerable<Filter> dichroics, IEnumerable<Filter> excitations)
        {
            string localAppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            DirectoryInfo genetixDirectory = new DirectoryInfo(Path.Combine(localAppDataPath, "Genetix"));

            var list = new List<Fluorochrome>();

            foreach (FileInfo file in genetixDirectory.GetFiles("*.fluorochrome"))
            {
                XElement xml = XElement.Load(file.FullName);
                list.Add(Fluorochrome.FromXml(xml, dichroics, excitations));
            }

            list.Sort((a, b) => a.Name.CompareTo(b.Name));
            return list;
        }

        public static void SaveAll(IEnumerable<Fluorochrome> fluors)
        {
            string localAppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            DirectoryInfo genetixDirectory = new DirectoryInfo(Path.Combine(localAppDataPath, "Genetix"));

            foreach (var f in genetixDirectory.GetFiles())
            {
                if (string.Compare(f.Extension, ".fluorochrome", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    f.Delete();
                }
            }

            foreach (var f in fluors)
            {
                var xml = f.ToXml();
                xml.Save(Path.Combine(genetixDirectory.FullName, Guid.NewGuid().ToString() + ".fluorochrome"));
            }
        }
    }
}
