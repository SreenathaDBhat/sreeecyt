﻿using System;
using System.Windows;
using System.Windows.Input;

namespace AI
{
    public partial class LoadSlideWindow : Window
    {
        private string EMPTY = "empty";
        private int slidesPerTray;
        private int numberTrays = 0;

        private Boolean slideOnStage = false;
        private int selectedSlideIndex;
        private static int lastTray = -1;
        private Boolean[] trayPresent = null ;
        private HardwareController.HardwareController hc;
        
        public LoadSlideWindow(HardwareController.HardwareController hc)
        {
            this.hc = hc;

            // InitSlideLoader will not do anything
            // unless cassette has been parked or 
            // door has been opened.
            int errCode = hc.InitSlideLoader();

            slideOnStage = hc.IsSlideOnStage();

            slidesPerTray = hc.NumberOfSlidesPerTray;
            numberTrays = hc.NumberOfTrays;

            if (numberTrays > 1)
            {
                hc.GetOccupiedTrayPositions(numberTrays, ref trayPresent);
            }

            bool sl50 = numberTrays == 10 && slidesPerTray == 5;

            DataContext = new
            {
                Slides = Generate(slidesPerTray),

                // On an SL50 tray 1 is at the top,
                // on the GSL10 and 120 it is at the bottom
                Trays = sl50 ? GenerateBays(numberTrays) : GenerateBackwardBays(numberTrays),
            };

            InitializeComponent();

            UserPrompt.Text = slideOnStage ? "Remove slide from stage." : "Double-click to load a slide.";

            if (lastTray != -1)
            {
                trayList.SelectedIndex = lastTray;
            }
        }

        private int[] Generate(int n)
        {
            int[] slides = new int[n];

            for (int i = 0; i < n; ++i)
                slides[i] = i + 1;

            return slides;
        }

        private string[] GenerateBays(int n)
        {
            string[] bays = new string[n];

            for (int i = 0; i < n; ++i)
            {
                if (trayPresent != null)
                    bays[i] = trayPresent[i] ? (i + 1).ToString() : EMPTY;
                else
                    bays[i] = (i + 1).ToString();
            }

            return bays;
        }


        private string[] GenerateBackwardBays(int n)
        {
            string[] bays = new string[n];

            for (int i = 0; i < n; ++i)
            {
                if (trayPresent != null)
                    bays[i] = trayPresent[(n-1) - i] ? (n - i).ToString() : EMPTY;
                else
                    bays[i] = (n - i).ToString();
            }

            return bays;
        }

        private void OnDragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void OnDoubleClickSlide(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

            string tray = trayList.SelectedValue.ToString();
            if (tray != EMPTY)
            {
                int selectedTray = int.Parse(tray) - 1;
                int selectedSlide = (int)((FrameworkElement)sender).Tag;
                selectedSlideIndex = selectedTray * slidesPerTray + selectedSlide;

                lastTray = trayList.SelectedIndex;
                DialogResult = true;
            }
        }

        public int SelectedSlideIndex
        {
            get 
            {
                return selectedSlideIndex;
            }
        }

        private void OnCloseClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void ScanCassette_Click(object sender, RoutedEventArgs e)
        {
            // InitSlideLoader will not do anything
            // unless cassette has been parked or 
            // door has been opened.
            int errCode = hc.InitSlideLoader();

            slideOnStage = hc.IsSlideOnStage();

            hc.GetOccupiedTrayPositions(numberTrays, ref trayPresent);

            bool sl50 = numberTrays == 10 && slidesPerTray == 5;

            DataContext = new
            {
                Slides = Generate(slidesPerTray),

                // On an SL50 tray 1 is at the top,
                // on the GSL10 and 120 it is at the bottom
                Trays = sl50 ? GenerateBays(numberTrays) : GenerateBackwardBays(numberTrays),
            };

            UserPrompt.Text = slideOnStage ? "Remove slide from stage." : "Double-click to load a slide.";

        }
    }
}
