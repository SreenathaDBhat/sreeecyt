﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace AI
{
    public partial class SaveFluorochromeAs
    {
        private SaveFluorochromeAsController controller;

        public SaveFluorochromeAs(IEnumerable<Fluorochrome> existingFluors, Fluorochrome fluor)
        {
            controller = new SaveFluorochromeAsController(fluor, existingFluors);
            controller.Name = fluor.Name;

            DataContext = controller;
            InitializeComponent();

            controller.SetErrorStringsResources(Resources.MergedDictionaries[1]);

            Loaded += (s, e) =>
                {
                    Keyboard.Focus(text);
                    text.SelectAll();
                };
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void CanSave(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.IsValid;
        }

        private void Save(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }

        public string FluorochromeName
        {
            get { return controller.Name; }
        }

        private void DragMeAllOver(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }


    internal class SaveFluorochromeAsController : Notifier
    {
        private string name, fail;
        private IEnumerable<Fluorochrome> existingFluors;
        private ResourceDictionary resourcesForStrings;
        private Fluorochrome fluorBeingSaved;

        public SaveFluorochromeAsController(Fluorochrome fluorBeingSaved, IEnumerable<Fluorochrome> existingFluors)
        {
            this.fluorBeingSaved = fluorBeingSaved;
            this.existingFluors = existingFluors;
            name = string.Empty;
        }

        public string Name
        {
            get { return name; }
            set { name = value; Notify("Name", "IsVaid"); }
        }

        public string Fail
        {
            get { return fail; }
            private set { fail = value; Notify("Fail"); }
        }

        public bool IsValid
        {
            get
            {
                if (name == null)
                {
                    Fail = Error("failureNoName");
                    return false;
                }

                if (name.Length == 0)
                {
                    Fail = Error("failureNoName");
                    return false;
                }

                foreach (var f in existingFluors)
                {
                    if (f == fluorBeingSaved)
                        continue;

                    if (string.Compare(f.Name, name, StringComparison.CurrentCultureIgnoreCase) == 0)
                    {
                        Fail = Error("failureDuplicate");
                        return false;
                    }
                }

                bool gotNonWhitespace = false;
                foreach (var c in name)
                {
                    if (char.IsLetterOrDigit(c))
                    {
                        gotNonWhitespace = true;
                        break;
                    }
                }

                if (gotNonWhitespace == false)
                {
                    Fail = Error("failureBadName");
                    return false;
                }

                return true;
            }
        }

        private string Error(string id)
        {
            return (string)resourcesForStrings[id];
        }

        internal void SetErrorStringsResources(ResourceDictionary resources)
        {
            resourcesForStrings = resources;
        }
    }
}
