﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Collections.ObjectModel;

namespace AI
{
    public partial class ScanBarcodesWindow : Window
    {
        private ScanBarcodeController controller;

        public ScanBarcodesWindow(IEnumerable<FishSlide> bayOfSlides)
        {
            controller = new ScanBarcodeController(bayOfSlides);
            DataContext = controller;
            InitializeComponent();
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            controller.CharacterRead(e);
        }

        private void OnDragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }

    public class ScanBarcodeController : BarcodeController
    {
        private ObservableCollection<FishSlide> slides = new ObservableCollection<FishSlide>();
        private FishSlide currentSlide;

        public IEnumerable<FishSlide> Slides { get { return slides; } }
        
        public FishSlide CurrentSlide 
        {
            get { return currentSlide; }
            set { currentSlide = value; Notify("CurrentSlide"); }
        }

        public ScanBarcodeController(IEnumerable<FishSlide> slides)
        {
            foreach (FishSlide s in slides)
                this.slides.Add(s);

            if (slides.Count() > 0)
                CurrentSlide = Slides.First();
        }

        protected override void zapper_Zap(object sender, BarcodeZapEventArgs e)
        {
            base.zapper_Zap(sender, e);

            PopulateSlideDetails(CurrentSlide, zappedBarcode);
            MoveToNextSlide();
        }

        public static void PopulateSlideDetails(FishSlide fishSlide, string barcode)
        {
            using(var db = new Connect.Database())
            using (var lims = Connect.LIMS.New())
            {
                var slide = lims.FindSlide(barcode);
                if (slide != null)
                {
                    var theCase = lims.FindCaseForSlide(slide);
                    var dbc = db.Cases.Where(c => c.limsRef == theCase.Id).First();
                    var dbs = db.Slides.Where(c => c.limsRef == slide.Id).First();

                    fishSlide.CaseId = dbc.caseId;
                    fishSlide.CaseName = theCase.Name;
                    fishSlide.SlideId = dbs.slideId;
                    fishSlide.SlideName = slide.Name;

                    string status = db.SlideStatus.Where(f => f.sequence == dbs.slideStatus).First().description;

                    if (status == null)
                    {
                        AI.Connect.SlideStatus newRec = new AI.Connect.SlideStatus();
                        newRec.sequence = 0;
                        newRec.description = "Unassigned";
                        db.SlideStatus.InsertOnSubmit(newRec);
                        db.SubmitChanges();
                    }
                    else
                        fishSlide.SlideStatus = status;
                }
            }
        }

        private void MoveToNextSlide()
        {
            if (CurrentSlide != slides.Last())
                CurrentSlide = slides[slides.IndexOf(CurrentSlide) + 1];
        }
    }

    //public class ScanBarcodeController : Notifier
    //{
    //    private BarcodeZapper zapper = new BarcodeZapper();
    //    private ObservableCollection<FishSlide> slides = new ObservableCollection<FishSlide>();
    //    private FishSlide currentSlide;

    //    public IEnumerable<FishSlide> Slides { get { return slides; } }

    //    public FishSlide CurrentSlide
    //    {
    //        get { return currentSlide; }
    //        set { currentSlide = value; Notify("CurrentSlide"); }
    //    }

    //    public ScanBarcodeController(IEnumerable<FishSlide> slides)
    //    {
    //        foreach (FishSlide s in slides)
    //            this.slides.Add(s);

    //        if (slides.Count() > 0)
    //            CurrentSlide = Slides.First();

    //        zapper.Zap += new EventHandler<BarcodeZapEventArgs>(zapper_Zap);
    //    }

    //    void zapper_Zap(object sender, BarcodeZapEventArgs e)
    //    {
    //        string barcode = e.Barcode;
    //        PopulateSlideDetails(CurrentSlide, barcode);
    //        MoveToNextSlide();
    //    }

    //    private void MoveToNextSlide()
    //    {
    //        if (CurrentSlide != slides.Last())
    //            CurrentSlide = slides[slides.IndexOf(CurrentSlide) + 1];
    //    }

    //    public static void PopulateSlideDetails(FishSlide fishSlide, string barcode)
    //    {
    //        var db = new Connect.Database();
    //        var slide = (from s in db.Slides
    //                     where s.barcode == barcode
    //                     select s).FirstOrDefault();

    //        if (slide != null)
    //        {
    //            var theCase = (from c in db.Cases
    //                           where c.caseId == slide.caseId
    //                           select c).First();

    //            fishSlide.CaseId = slide.caseId;
    //            fishSlide.CaseName = theCase.name;
    //            fishSlide.SlideId = slide.slideId;
    //            fishSlide.SlideName = slide.slideName;

    //            byte findStatus = (slide.slideStatus == null) ? (byte)0 : (byte)slide.slideStatus;
    //            fishSlide.SlideStatus = db.SlideStatus.Where(f => f.sequence == findStatus).First().description;
    //        }
    //    }

    //    internal void CharacterRead(KeyEventArgs e)
    //    {
    //        zapper.CharacterRead(e);
    //    }
    //}


    //public class BarcodeZapper
    //{
    //    public event EventHandler<BarcodeZapEventArgs> Zap;

    //    string barcodeString = "";
    //    bool recordingBarcode;
    //    Key startSignal;
    //    Key stopSignal;

    //    public BarcodeZapper()
    //    {
    //        recordingBarcode = false;
    //        startSignal = Key.F23;
    //        stopSignal = Key.F24;
    //    }

    //    public void CharacterRead(KeyEventArgs keyPress)
    //    {
    //        Key key = keyPress.Key;
    //        if (!recordingBarcode && key != startSignal)
    //            return;

    //        if (key == startSignal)
    //        {
    //            recordingBarcode = true;
    //            barcodeString = "";
    //            return;
    //        }

    //        if (key == stopSignal && recordingBarcode)
    //        {
    //            recordingBarcode = false;
    //            OnZap(barcodeString);
    //            return;
    //        }

    //        if (recordingBarcode)
    //        {
    //            char c = 'Z';
    //            if (KeyToChar(key, ref c))
    //                barcodeString += c;
    //        }
    //    }

    //    private bool KeyToChar(Key key, ref char c)
    //    {
    //        bool returnval = false;
    //        if (key >= Key.D0 && key <= Key.D9)
    //        {
    //            c = (char)(((int)'0') + (key - Key.D0));
    //            return true;
    //        }
    //        if (key >= Key.A && key <= Key.Z)
    //        {
    //            c = (char)(((int)'A') + (key - Key.A));
    //            return true;
    //        }

    //        switch (key)
    //        {
    //            case Key.Multiply:
    //                c = (char)'*';
    //                returnval = true;
    //                break;
    //            case Key.Add:
    //                c = (char)'+';
    //                returnval = true;
    //                break;
    //            case Key.Subtract:
    //                c = (char)'-';
    //                returnval = true;
    //                break;
    //            case Key.OemSemicolon:
    //                c = (char)';';
    //                returnval = true;
    //                break;
    //            case Key.OemMinus:
    //                c = (char)'-';
    //                returnval = true;
    //                break;
    //            case Key.OemPeriod:
    //                c = (char)'.';
    //                returnval = true;
    //                break;
    //            case Key.OemQuestion:
    //                c = (char)'?';
    //                returnval = true;
    //                break;
    //            case Key.OemOpenBrackets:
    //                c = (char)'(';
    //                returnval = true;
    //                break;
    //            case Key.OemComma:
    //                c = (char)',';
    //                returnval = true;
    //                break;
    //            case Key.OemCloseBrackets:
    //                c = (char)')';
    //                returnval = true;
    //                break;
    //            case Key.OemQuotes:
    //                c = (char)'"';
    //                returnval = true;
    //                break;
    //            case Key.OemBackslash:
    //                c = (char)'\\';
    //                returnval = true;
    //                break;
    //            default:
    //                returnval = false;
    //                break;
    //        }


    //        return returnval;
    //    }

    //    private void OnZap(string barcode)
    //    {
    //        if (Zap != null)
    //        {
    //            Zap(this, new BarcodeZapEventArgs(barcode));
    //        }
    //    }
    //}

    //public class BarcodeZapEventArgs : EventArgs
    //{
    //    private string barcode;

    //    public BarcodeZapEventArgs(string barcode)
    //    {
    //        this.barcode = barcode;
    //    }

    //    public string Barcode
    //    {
    //        get { return barcode; }
    //    }
    //}
}
