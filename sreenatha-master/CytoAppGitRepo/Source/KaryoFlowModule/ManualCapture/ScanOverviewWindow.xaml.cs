﻿using System;
using System.Windows;
using System.Threading;
using System.Collections.Generic;
using System.Windows.Media;
using AI.Bitmap;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using System.IO;

namespace AI
{
    public partial class ScanOverviewWindow
    {
        private ScanOverviewController controller;
        private HardwareController.HardwareController hw;
        private Camera cam;

        public ScanOverviewWindow(HardwareController.HardwareController hw, Camera cam)
        {
            this.hw = hw;
            this.cam = cam;

            controller = new ScanOverviewController();
            DataContext = controller;
            InitializeComponent();
        }

        private void OnCancelScan(object sender, RoutedEventArgs e)
        {
            controller.CancelScan();
        }

        private void OnScan(object sender, RoutedEventArgs e)
        {
            controller.Scan(hw, cam, Dispatcher);
        }
    }


    public class ScanOverviewSnap
    {
        public ImageSource Image;
        public Rect Position;
    }

    public class ScanOverviewController : Notifier
    {
        private bool isScanning;
        private HardwareController.HardwareController hw;
        private Camera camera;
        private Thread scanThread;
        private Dispatcher uiDispatcher;
        private ObservableCollection<ScanOverviewSnap> snaps;
        private RenderTargetBitmap tiledImage;

        public ScanOverviewController()
        {
            tiledImage = new RenderTargetBitmap(1250, 3250, 96, 96, PixelFormats.Pbgra32);
            snaps = new ObservableCollection<ScanOverviewSnap>();
        }

        public ImageSource SlideImage
        {
            get { return tiledImage; }
        }

        public bool IsScanning
        {
            get { return isScanning; }
            set { isScanning = value; Notify("IsScanning"); }
        }

        public void Scan(HardwareController.HardwareController hw, Camera camera, Dispatcher uiDispatcher)
        {
            this.uiDispatcher = uiDispatcher;
            this.hw = hw;
            this.camera = camera;

            scanThread = new Thread(ScanThread);
            scanThread.Start();

            IsScanning = true;
        }

        public void ScanThread()
        {
            ChannelInfo channelInfo = new ChannelInfo()
            {
                RenderColor = Colors.White
            };

            try
            {
                Size frameSize = new Size(3000, 2000);

                double x = frameSize.Width / 2;
                double y = 40000;
                double dx = frameSize.Width;

                hw.SetBrightfieldLamp(17, true);

                while (y < 65000)
                {
                    hw.MoveStageToPosY((float)y);

                    while (true)
                    {
                        hw.MoveStageToPosX((float)x);
                        hw.WaitAxisStationary();

                        TakeSnap(new Rect(x - frameSize.Width / 2, y - frameSize.Height / 2, frameSize.Width, frameSize.Height), channelInfo);

                        Increment(ref x, dx);
                        
                        if (!InRange(x, dx))
                        {
                            dx *= -1;
                            y += frameSize.Height;
                            break;
                        }
                    }
                }
            }
            finally
            {
                uiDispatcher.Invoke((ThreadStart)delegate
                {
                    IsScanning = false;

                }, DispatcherPriority.Normal);
            }
        }

        private void TakeSnap(Rect pos, ChannelInfo channelInfo)
        {
            ushort[] pixels = camera.Capture();
            var ch = new Channel(camera.ImageWidth, camera.ImageHeight, camera.ImageBitsPerPixel, pixels);
            var frame = new ImageFrame()
            {
                ImageBitsPerPixel = ch.BitsPerPixel,
                ImageHeight = ch.Height,
                ImageWidth = ch.Width,
                ImageType = ImageType.Raw
            };

            frame.AddSnap(new Snap
            {
                ChannelInfo = channelInfo,
                ChannelPixels = ch
            });

            var img = RGBImageComposer.Compose(frame, null);

            ScanOverviewSnap snap = new ScanOverviewSnap()
            {
                Image = img,
                Position = pos
            };

            uiDispatcher.BeginInvoke((ThreadStart)delegate
            {
                snaps.Add(snap);

                DrawingVisual visual = new DrawingVisual();
                var draw = visual.RenderOpen();
                double s = 0.05;

                //var transform = new ScaleTransform(1, -1);
                //transform.CenterX = pos.X + pos.Width / 2;
                //transform.CenterY = pos.Y + pos.Height / 2;

                //draw.PushTransform(transform);
                draw.DrawImage(snap.Image, new Rect(snap.Position.X * s, snap.Position.Y * s, snap.Position.Width * s, snap.Position.Height * s));
                draw.Close();

                tiledImage.Render(visual);

            }, DispatcherPriority.Normal);
        }

        private void Increment(ref double x, double dx)
        {
            x += dx;
        }

        private bool InRange(double x, double dx)
        {
            if (dx > 0)
                return x < 25000;
            else
                return x > 0;
        }

        public void CancelScan()
        {
            scanThread.Abort();
            scanThread.Join(5000);
        }
    }
}
