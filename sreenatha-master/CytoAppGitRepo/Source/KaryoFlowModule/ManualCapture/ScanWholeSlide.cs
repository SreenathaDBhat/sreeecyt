﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;
using System.IO;
using AI.Bitmap;
using HW = AI.HardwareController;
using System.Windows.Threading;
using System.Threading;
using System.Windows.Media.Media3D;

namespace AI
{
    public class ScanWholeSlide
    {
        public event EventHandler WholeSlideScanned;
        private HW.HardwareController hw;
        private Camera camera;
        private Dispatcher disp;

        public ScanWholeSlide(HW.HardwareController hw, Camera camera)
        {
            this.hw = hw;
            this.camera = camera;
            disp = Dispatcher.CurrentDispatcher;
        }

        public void Scan(Guid caseId, Guid slideId, int objective, Fluorochrome fluorochrome)
        {
            hw.SetMicLock(true);

            try
            {
                PyramidGenerator pyramid = new PyramidGenerator(caseId, slideId, 4);

                pyramid.Completed += (s, e) =>
                {
                    if (WholeSlideScanned != null)
                    {
                        disp.Invoke((ThreadStart)delegate
                        {
                            WholeSlideScanned(this, EventArgs.Empty);

                        }, DispatcherPriority.Normal);
                    }
                };

                Rect scanArea = new Rect(10000, 45000, 3000, 3000);

                int currentRotorPosition = fluorochrome.Channels.First().Dichroic.RotorPosition - 1;
                hw.MoveToDichroic(currentRotorPosition, true);

                Size imageScale = MicroscopeFile.ImageScale(objective);
                Size pixelSize = new Size(camera.ImageWidth, camera.ImageHeight);

                CaptureQueue q = new CaptureQueue();
                q.Enqueue(new ArbitraryRegion(scanArea)
                           {
                               Case = caseId,
                               Slide = slideId,
                               SlideIndex = -1
                           });

                Capture capture = new Capture(q, hw, camera, Dispatcher.CurrentDispatcher)
                {
                    Objective = objective,
                    Fluorochrome = fluorochrome
                };

                capture.ImageCaptured = (qq, cap) => pyramid.AddFrame(cap.Frame, cap.Frame.Position, camera.ImageSize, imageScale);
                capture.Go(-1);
                pyramid.NoMoreFrames();
            }
            finally
            {
                hw.SetMicLock(false);
            }
        }
    }
}
