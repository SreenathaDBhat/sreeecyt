﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using AI.Connect;
using AI.Logging;

namespace AI
{
    public class StatusSequence : Notifier
    {
        private byte sequence = 0;
        private string description = string.Empty;

        public byte Sequence 
        {   get { return sequence; } 
            set 
            {
                sequence = value;
                Notify("Sequence");
            } 
        }

        public String Description
        {
            get { return description; }
            set
            {
                description = value;
                Notify("Description");
            }
        }

        public StatusSequence(byte sequence, String description)
        {
            Sequence = sequence;
            Description = description;
        }
    }

    public class SlideInfoController : BarcodeController
    {
        public event EventHandler<BarcodeZapEventArgs> Zap;

        private ObservableCollection<String> events = new ObservableCollection<String>();
        private ObservableCollection<StatusSequence> statuses = new ObservableCollection<StatusSequence>();

        private string caseName = string.Empty;
        private string caseStatus = string.Empty;
        private string slideName = string.Empty;
        private string initialStatus = string.Empty;
        private string slideStatus = string.Empty;
        private string slideType = string.Empty;
        private byte slideSeq = 0;
        private Boolean dirty = false;
        private string barcode = string.Empty;
        private Guid caseId = Guid.Empty;
        private Guid slideId = Guid.Empty;
        private Boolean proceed = false;

        public Boolean ProceedToTask
        {
            get { return proceed; }
            set { proceed = value; }
        }
        
        public Guid SlideId
        {
            get { return slideId; }
            set { slideId = value; Notify("SlideId"); }
        }

        public string SlideName
        {
            get { return slideName; }
            set { slideName = value; Notify("SlideName"); }
        }

        public string Barcode
        {
            get { return barcode; }
            set { barcode = value; Notify("Barcode"); }
        }

        public Boolean Dirty
        {
            get { return dirty; }
        }

        public string SlideType
        {
            get { return slideType; }
            set { slideType = value; Notify("SlideType"); }
        }

        public string SlideStatus
        {
            get { return slideStatus; }
            set
            {
                slideStatus = value;
                slideSeq = statuses.Where(f => f.Description == slideStatus).First().Sequence;
                Notify("SlideStatus");
                Notify("SlideStatusSequence");

                dirty = (slideStatus != initialStatus);
                Notify("Dirty");
            }
        }

        public byte SlideStatusSequence
        {
            get { return slideSeq; }
            set
            {
                slideSeq = value;

                slideStatus = statuses.Where(f => f.Sequence == slideSeq).First().Description;
                Notify("SlideStatusSequence");
                Notify("SlideStatus");


                dirty = (slideStatus != initialStatus);
                Notify("Dirty");
            }
        }

        public IEnumerable<String> Events
        {
            get { return events; }
        }

        public IEnumerable<StatusSequence> Statuses
        {
            get { return statuses; }
        }

        public string CaseName
        {
            get { return caseName; }
            set { caseName = value; Notify("CaseName"); }
        }

        public Guid CaseId
        {
            get { return caseId; }
            set
            {
                if (value != caseId)
                {
                    caseId = value;
                    Notify("CaseId");
                }
            }
        }

        public SlideInfoController()
        {
            GetStatuses();
        }

        private void GetStatuses()
        {
            statuses.Clear();

            using (Database db = new Database())
            {
                var stats = from stat in db.SlideStatus
                            orderby stat.sequence
                            select stat;

                foreach( var stat in stats)
                    statuses.Add(new StatusSequence(stat.sequence, stat.description));
            }
        }

        public void SaveStatus()
        {
			using (var db = new Connect.Database())
			{
				var slide = (from s in db.Slides
							 where s.slideId == slideId
							 select s).FirstOrDefault();

				if (slide != null)
				{
					slide.slideStatus = slideSeq;

					if (Security.Security.IsLoggingEnabled)
					{
						string msg = slideName + " [" + slideStatus + "]";
						Security.Security.AddLogEntry(Logging.EventType.SlideStatusChange, caseId, caseStatus, msg);
					}

					db.SubmitChanges();

					initialStatus = slideStatus;
					dirty = false;
					Notify("Dirty");

					CheckForSlideEvents();
				}
			}
        }
        


        protected override void zapper_Zap(object sender, BarcodeZapEventArgs e)
        {
            base.zapper_Zap(sender, e);
            LookupSlideDetails();

            if (Zap != null)
            {
                Zap(this, new BarcodeZapEventArgs(zappedBarcode));
            }
        }

        public void LookupSlideDetails()
        {
            CaseId = Guid.Empty;
            CaseName = string.Empty;
            caseStatus = string.Empty;
            SlideId = Guid.Empty;
            SlideName = string.Empty;
            SlideType = string.Empty;
            Barcode = zappedBarcode;
            slideSeq = 0;
            slideStatus = string.Empty;
            Notify("SlideStatus");
            events.Clear();

			using (var db = new Connect.Database())
            using (var lims = Connect.LIMS.New())
			{
                var slide = lims.FindSlide(zappedBarcode);
				if (slide != null)
					LookupSlideDetails(db, slide, lims);
			}
        }

        private void LookupSlideDetails(Database db, Slide slide, ILims lims)
        {
            if (slide != null)
            {
                var theCase = lims.FindCaseForSlide(slide);
                var dbc = db.Cases.Where(c => c.limsRef == theCase.Id).First();
                var dbs = db.Slides.Where(c => c.limsRef == slide.Id).First();

                CaseId = dbc.caseId;
                CaseName = theCase.Name;
                //caseStatus = dbc.S;
                SlideId = dbs.slideId;
                SlideName = slide.Name;
                SlideType = dbs.slideType;
                Barcode = slide.Barcode;

                //  Do it this way so dirty is not
                //  set on initial assignment
                slideSeq = dbs.slideStatus;
                slideStatus = db.SlideStatus.Where(f => f.sequence == slideSeq).First().description;
                initialStatus = slideStatus;
                dirty = false;
                Notify("SlideStatus");
            }

            //  Get some history on the slide
            CheckForSlideEvents();
        }

        public void CheckForSlideEvents()
        {
            events.Clear();

            using (Database db = new Database())
            {
                var caseHistory = from eventRec in db.EventLogs
                                where (eventRec.CaseID == CaseId)
                                orderby eventRec.ID descending
                                select eventRec;

                foreach (var newEvent in caseHistory)
                {
                    switch(newEvent.EventType)
                    {
                        case EventType.CaseCreated:
                            events.Add(newEvent.DateTime.ToShortDateString() + " - Case created");
                            break;
                        case EventType.SlideAdded:
                            //  The slide name and status are stored in the description field
                            //  which should be of the format      slidename [slidestatus]
                            if (newEvent.Description.Length > 0)
                            {
                                if (newEvent.Description.StartsWith(SlideName))
                                    events.Add(newEvent.DateTime.ToShortDateString() + " - Slide added");
                            }
                            break;

                        case EventType.SlideStatusChange:
                            if (newEvent.Description.Length > 0)
                            {
                                if (newEvent.Description.Contains("[") && newEvent.Description.Contains("]"))
                                {
                                    int pos1 = newEvent.Description.IndexOf("[");
                                    int pos2 = newEvent.Description.IndexOf("]");
                                    if (newEvent.Description.Substring(0, pos1).Trim() == SlideName)
                                        events.Add(newEvent.DateTime.ToShortDateString() + "  " +
                                            newEvent.DateTime.ToShortTimeString() + " - Slide status set to "
                                            + newEvent.Description.Substring(pos1 + 1, pos2 - (pos1 + 1)));

                                }
                            }

                            break;

                        case EventType.CaseStatusChange:
                            events.Add(newEvent.DateTime.ToShortDateString() + " - Case status set to " + newEvent.CaseStatus);
                            break;
                    }
                }
            }

            Notify("Events");

        }
    }
}
