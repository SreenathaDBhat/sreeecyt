﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.IO;
using AI.Connect;
using System.Windows.Input;
using System.Windows.Threading;
using System.Threading;


namespace AI
{
    public static class SlideNotificationCommands
    {
        public static readonly RoutedUICommand ReadyForMarkup = new RoutedUICommand("Markup", "Markup", typeof(SlideNotificationCommands));
        public static readonly RoutedUICommand ReadyForCapture = new RoutedUICommand("Capture", "Capture", typeof(SlideNotificationCommands));
        public static readonly RoutedUICommand ReadyForAnalysis = new RoutedUICommand("Analyse", "Analyse", typeof(SlideNotificationCommands));
    }

    public class StatusOption : Notifier
    {
        private Boolean watch = false;
        private byte sequence = 0;
        private string description = string.Empty;

        public Boolean Monitor
        {
            get 
            {
                return watch;
            } 
            set 
            {
                watch = value; 
                Notify("Monitor");
            }
        }

        public Byte Sequence 
        {
            get
            {
                return sequence;
            }
            set
            {
                sequence = value;
                Notify("Sequence");
            }
        }

        public String Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
                Notify("Description");
            }
        }
    }

    public class SlideStatusInfo : Notifier
    {
        private string caseName = string.Empty;
        private string slideName = string.Empty;
        private string slideStatus = string.Empty;
        private byte slideStatusSequence = 0;
        private DateTime whenChanged;
        private Guid caseId = Guid.Empty;
        private Guid slideId = Guid.Empty;

        public static SlideStatusInfo FromBarcode(string barcode)
        {
            using (var lims = Connect.LIMS.New())
            {
                var slide = lims.FindSlide(barcode);
                using (var db = new Connect.Database())
                {
                    var dbs = db.Slides.Where(s => s.limsRef == slide.Id).FirstOrDefault();
                    var dbc = dbs == null ? null : db.Cases.Where(c => c.caseId == dbs.caseId).First();
                    Case lc = dbc == null ? null : lims.GetCase(dbc.limsRef);
                    var stat = dbs == null ? "" : (from ss in db.SlideStatus where ss.sequence == dbs.slideStatus select ss.description).First();
                    
                    return new SlideStatusInfo
                    {
                        CaseId = dbs.caseId,
                        CaseName = lc == null ? "" : lc.Name,
                        SlideId = dbs == null ? Guid.Empty : dbs.slideId,
                        SlideName = slide.Name,
                        StatusSequence = dbs == null ? (byte)0 : dbs.slideStatus,
                        SlideStatus = stat,
                        WhenChanged = dbs == null ? DateTime.Now : dbs.statusChanged
                    };
                }
            }
        }

        public static SlideStatusInfo FromSlideId(Guid slideId)
        {
            using (var db = new Connect.Database())
            {
                var slide = db.Slides.Where(s => s.slideId == slideId).FirstOrDefault();
                return FromDb(slide, db);
            }
        }

        private static SlideStatusInfo FromDb(Connect.Slide slide, Database db)
        {
            if (slide == null)
                return null;

            var cas = db.Cases.Where(c => c.caseId == slide.caseId).First();
            var stat = db.SlideStatus.Where(s => s.sequence == slide.slideStatus).First();

            using (var lims = Connect.LIMS.New())
            {
                var lc = lims.GetCase(cas.limsRef);
                var ls = lims.GetSlide(slide.limsRef);

                return new SlideStatusInfo
                {
                    CaseId = cas.caseId,
                    CaseName = lc.Name,
                    SlideId = slide.slideId,
                    SlideName = ls.Name,
                    SlideStatus = stat.description,
                    StatusSequence = slide.slideStatus,
                    WhenChanged = slide.statusChanged
                };
            }
        }

        public Guid SlideId
        {
            get { return slideId; }
            set { slideId = value; Notify("SlideId"); }
        }

        public string SlideName
        {
            get { return slideName; }
            set { slideName = value; Notify("SlideName"); }
        }

        public string SlideStatus
        {
            get { return slideStatus; }
            set
            {
                slideStatus = value;
                Notify("SlideStatus");
            }
        }

        public byte StatusSequence
        {
            get { return slideStatusSequence; }
            set
            {
                slideStatusSequence = value;
                Notify("SlideStatus");
            }
        }

        public RoutedCommand RecommendedCommand
        {
            get
            {
                switch(slideStatusSequence)
                {
                    case 2: // ReadyForMarkup
                        return SlideNotificationCommands.ReadyForMarkup;

                    case 3: // ReadyForCapture
                        return SlideNotificationCommands.ReadyForCapture;

                    case 4:
                        return SlideNotificationCommands.ReadyForAnalysis;

                    default:
                        return null;
                }
            }
        }

        public string CaseName
        {
            get { return caseName; }
            set { caseName = value; Notify("CaseName"); }
        }

        public Guid CaseId
        {
            get { return caseId; }
            set
            {
                if (value != caseId)
                {
                    caseId = value;
                    Notify("CaseId");
                }
            }
        }

        public DateTime WhenChanged
        {
            get { return whenChanged; }
            set { whenChanged = value; Notify("WhenChanged", "ChangeDuration"); }
        }

        public string ChangeDuration
        {
            get
            {
                return DateStrings.FriendlyDateString(whenChanged, DateTime.Now);
            }
        }
    }

    public class GroupedSlides
    {
        public Guid CaseId { get; set; }
        public string CaseName { get; set; }

        private IEnumerable<SlideStatusInfo> slides;
        public IEnumerable<SlideStatusInfo> Slides
        {
            get { return slides; }
            set
            {
                slides = value;

                if (slides.Count() > 0)
                {
                    Date = (from s in Slides select s.WhenChanged).Max();
                }
            }
        }

        public DateTime Date { get; private set; }

        public static GroupedSlides ForCase(Connect.Database db, Guid caseId, IEnumerable<SlideStatusInfo> slides)
        {
            var caseSlides = from s in slides
                             where s.CaseId == caseId
                             select s;


            var dbCase = (from c in db.Cases
                          where c.caseId == caseId
                          select c).First();


            return new GroupedSlides
            {
                CaseId = caseId,
                CaseName = Connect.LIMS.CaseName(dbCase.limsRef),
                Slides = caseSlides
            };
        }
    }

    public class SlideStatusWatcher : Notifier, IDisposable
    {
        private const string settingsFile = "Genetix\\SlideStatusesOptions.settings";

        private ObservableCollection<StatusOption> options = new ObservableCollection<StatusOption>();
        private ObservableCollection<SlideStatusInfo> matchingSlides = new ObservableCollection<SlideStatusInfo>();
        private int timerInterval;
        private int modifiedSinceLastRead;
        private int addedSinceLastRead;
        private int removedSinceLastRead;
        private int changesSinceLastRead;
        private System.Timers.Timer timer;
        private Dispatcher uiThread;
        private DateTime userMark;
        public event EventHandler NotificationsAdded;


        public SlideStatusWatcher()
        {
            uiThread = Dispatcher.CurrentDispatcher;
            userMark = DateTime.MinValue;

            timerInterval = 30000; // 30secs
            GetStatuses();
        }

        private void GetStatuses()
        {
            options.Clear();

            using (Database db = new Database())
            {
                var statuses = db.GetTable<SlideStatus>();

                if ((statuses != null) && (statuses.Count() > 0))
                {
                    foreach (var status in statuses)
                    {
                        StatusOption so = new StatusOption();
                        so.Description = status.description;
                        so.Sequence = status.sequence;
                        so.Monitor = false;

                        options.Add(so);
                    }

                    GetLastStatusMonitorSettings();
                }
            }

            Notify("Options");

            Start();
        }

        private void GetLastStatusMonitorSettings()
        {
            FileInfo file = AppData.File(Environment.SpecialFolder.LocalApplicationData, settingsFile);

            if (file.Exists)
            {
                using (StreamReader reader = new StreamReader(file.OpenRead()))
                {
                    while (reader.Peek() >= 0)
                    {
                        string line = reader.ReadLine();
                        if (line.Contains("="))
                        {
                            string sequence = line.Substring(0, line.IndexOf("="));
                            string value = line.Substring(line.IndexOf("=") + 1);

                            var opt = options.Where(f => f.Sequence == byte.Parse(sequence)).FirstOrDefault();

                            if (opt != null)
                            {
                                opt.Monitor = Boolean.Parse(value);
                            }
                        }
                    }

                    reader.Close();
                }
            }
        }

        public void SaveStatusMonitorSettings()
        {
            FileInfo file = AppData.File(Environment.SpecialFolder.LocalApplicationData, settingsFile);

            if (file.Exists)
                file.Delete();

            using (StreamWriter writer = new StreamWriter(file.OpenWrite()))
            {
                foreach (var opt in options)
                {
                    writer.WriteLine(opt.Sequence.ToString() + "=" + opt.Monitor.ToString());
                }
                writer.Flush();
                writer.Close();
            }
        }

        public bool GotNotifications
        {
            get { return matchingSlides.Count > 0; }
        }

        public IEnumerable<StatusOption> Options
        {
            get
            {
                return options;
            }
        }

        public void ResetChanges()
        {
            changesSinceLastRead = 0;
            modifiedSinceLastRead = 0;
            addedSinceLastRead = 0;
            removedSinceLastRead = 0;

            NotifyChanges();
        }
        
        public int Changes
        {
            get 
            { 
                return changesSinceLastRead; 
            }
        }

        public int Added
        {
            get
            {
                return addedSinceLastRead;
            }
        }

        public int Removed
        {
            get
            {
                return removedSinceLastRead;
            }
        }

        public int Modified
        {
            get
            {
                return modifiedSinceLastRead;
            }
        }

        public IEnumerable<SlideStatusInfo> Slides
        {
            get 
            {
                return matchingSlides; 
            }
        }

        public void Start()
        {
            CheckForChanges();

            if (timer == null)
            {
                timer = new System.Timers.Timer();
                timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
            }

            timer.Interval = (double)timerInterval;

            timer.Enabled = true;
        }

        public void Finish()
        {
            timer.Enabled = false;
            timer.Dispose();
            timer = null;

            SaveStatusMonitorSettings();
        
        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer.Enabled = false;
            CheckForChanges();
            timer.Enabled = true;
        }

        private void NotifyChanges()
        {
            Notify("Changes");
            Notify("Added");
            Notify("Removed");
            Notify("Modified");
        }

        public void CheckForChanges()
        {
            if (options.Count() > 0)
            {

                using (Database db = new Database())
                {
                    IEnumerable<byte> statusesToWatch = from opt in options
                                                        where opt.Monitor == true
                                                        select opt.Sequence;

                    var latest = from slide in db.Slides
                                 where statusesToWatch.Contains(slide.slideStatus)
                                 select slide;


                    SlideStatusInfo[] oldSlides = new SlideStatusInfo[matchingSlides.Count()];
                    matchingSlides.CopyTo(oldSlides, 0);

                    ObservableCollection<SlideStatusInfo> newMatchingSlides = new ObservableCollection<SlideStatusInfo>();

                    foreach (var latestrec in latest)
                    {
                        //  is new slide in list
                        var existingRec = matchingSlides.Where(f => f.SlideId == latestrec.slideId).FirstOrDefault();

                        //  if its a new matching record or an old one but status has changed
                        if (existingRec == null)
                        {
                            changesSinceLastRead++;
                            addedSinceLastRead++;
                        }
                        else 
                        {
                            if (existingRec.StatusSequence != latestrec.slideStatus)
                            {
                                modifiedSinceLastRead++;
                                changesSinceLastRead++;
                            }
                        }

                        
                        SlideStatusInfo ssi = new SlideStatusInfo();
                        ssi.SlideId = latestrec.slideId;
                        ssi.SlideName = Connect.LIMS.SlideName(latestrec.slideId);
                        ssi.StatusSequence = (byte)latestrec.slideStatus;
                        ssi.SlideStatus = options.Where(f => f.Sequence == latestrec.slideStatus).First().Description;
                        ssi.CaseId = latestrec.caseId;
                        ssi.WhenChanged = latestrec.statusChanged;
                        ssi.CaseName = Connect.LIMS.CaseName(latestrec.caseId);

                        newMatchingSlides.Add(ssi);
                    }

                    foreach (var slide in oldSlides)
                    {
                        //  if old slide not in new list then it is classed as another change
                        var match = latest.Where(f => f.slideId == slide.SlideId).FirstOrDefault();

                        //  Record not meeting criteria so count as change
                        if (match == null)
                        {
                            removedSinceLastRead++;
                            changesSinceLastRead++;
                        }
                    }

                    matchingSlides = newMatchingSlides;

                    uiThread.Invoke((ThreadStart)delegate
                    {
                        Notify("Slides", "SlidesByCase", "GotNotifications", "NewSlideCount");
                        NotifyChanges();

                        if (addedSinceLastRead > 0 && NotificationsAdded != null)
                            NotificationsAdded(this, EventArgs.Empty);

                    }, DispatcherPriority.Normal);
                }
            }
        }

        public void Dispose()
        {
            SaveStatusMonitorSettings();
        }

        public IEnumerable<GroupedSlides> SlidesByCase
        {
            get
            {
                var cases = (from s in Slides select s.CaseId).Distinct();
                var db = new Connect.Database();

                var result = from c in cases
                             select GroupedSlides.ForCase(db, c, Slides.ToArray());

                return from r in result orderby r.Date descending select r;
            }
        }

        /// <summary>
        /// Call this function to mark all the slides currently in the notify list as 'seen'.
        /// This allows the ui to distinguish proper new notifications from ones that have been lying around for a while.
        /// </summary>
        public void SetUserMarkToNow()
        {
            userMark = DateTime.Now;
            Notify("NewSlideCount");
        }

        public int NewSlideCount
        {
            get
            {
                return Slides.Count(s => s.WhenChanged > userMark);
            }
        }
    }
}
