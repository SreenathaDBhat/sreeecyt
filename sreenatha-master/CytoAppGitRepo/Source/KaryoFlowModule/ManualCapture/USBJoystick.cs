﻿using System;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Reflection;

namespace AI
{
    public static class USBJoystick
    {
        private static Process process;

        public static void LaunchProcess()
        {
            process = Process.GetProcessesByName("USBJoystickController").FirstOrDefault();
            if (process != null)
                return;

            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "USBJoystickController.exe");
            if (!File.Exists(path))
                path = @"c:\Program Files\Applied Imaging\CytoVision\USBJoystickController.exe";

            if (!File.Exists(path))
                return;
            
            process = new Process();
            process.StartInfo = new ProcessStartInfo(path);
            process.Start();
        }

        public static void KillProcess()
        {
            if(process != null && !process.HasExited)
                process.Kill();

            process = null;
        }
    }
}
