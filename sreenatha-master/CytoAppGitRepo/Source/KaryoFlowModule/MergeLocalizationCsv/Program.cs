﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace AI
{
    public class SingleRow
    {
        public string BamlName { get; set; }
        public string ResourceKey { get; set; }
        public string Category { get; set; }
        public string Readable { get; set; }
        public string Modifiable { get; set; }
        public string Comment { get; set; }
        public string Value { get; set; }

        public string WholeLine
        {
            get
            {
                return
                    BamlName + "," +
                    ResourceKey + "," +
                    Category + "," +
                    Readable + "," +
                    Modifiable + "," +
                    Comment + "," +
                    Value;
            }
        }

        public static SingleRow Parse(string line)
        {
            if (line == null || line.Length == 0)
                return null;

            string[] columns = line.Split(',');
            if (columns.Length != 7)
                return null;

            return new SingleRow
            {
                BamlName = columns[0],
                ResourceKey = columns[1],
                Category = columns[2],
                Readable = columns[3],
                Modifiable = columns[4],
                Comment = columns[5],
                Value = columns[6]
            };
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            TestArgs(args);

            FileInfo translationFile = new FileInfo(args[0]);
            FileInfo liveFile = new FileInfo(args[1]);

            List<SingleRow> existingTranslations = ReadFile(translationFile);
            List<SingleRow> liveUsList = ReadFile(liveFile);

            foreach (var row in liveUsList)
            {
                var matchingExistingRow = FindMatchingRow(row, existingTranslations);
                if (matchingExistingRow != null)
                {
                    row.Value = matchingExistingRow.Value;
                }
            }

            var writer = new StreamWriter(translationFile.FullName);
            try
            {
                foreach (var row in liveUsList)
                {
                    writer.WriteLine(row.WholeLine);
                }
            }
            finally
            {
                writer.Flush();
                writer.Close();
            }
        }

        private static SingleRow FindMatchingRow(SingleRow row, IEnumerable<SingleRow> existing)
        {
            foreach (var existingRow in existing)
            {
                if (row.BamlName == existingRow.BamlName && row.ResourceKey == existingRow.ResourceKey)
                    return existingRow;
            }

            return null;
        }

        private static List<SingleRow> ReadFile(FileInfo file)
        {
            List<SingleRow> rows = new List<SingleRow>();
            using (StreamReader reader = new StreamReader(file.FullName))
            {
                while (reader.Peek() != -1)
                {
                    var line = reader.ReadLine();
                    var row = SingleRow.Parse(line);
                    if(row != null)
                        rows.Add(row);
                }
            }

            return rows;
        }

        private static void TestArgs(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: MergeLocalizationCsv.exe trans.csv live.csv");
                Console.WriteLine("where:");
                Console.WriteLine("trans.csv = file with your translations in. This will be UPDATED with any new elements from LIVE.CSV");
                Console.WriteLine("live.csv = most recent csv dump from LocBaml.");
            }
        }
    }
}
