﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AI;

namespace AI.MetaData
{
    public class AvailableFields : Notifier
    {
        public delegate IEnumerable<string> FieldNameFetcher();
        
        private FieldNameFetcher fetcher;
        private IEnumerable<string> fieldNames;
        private IEnumerable<string> defaultOnes;

        public AvailableFields(FieldNameFetcher fetcher)
        {
            this.fieldNames = new string[0];
            this.defaultOnes = new string[0];
            this.fetcher = fetcher;
        }

        public IEnumerable<string> FieldNames
        {
            get 
            {
                if (defaultOnes == null)
                    return fieldNames;

                if (fieldNames == null)
                    return defaultOnes;

                var allofem = defaultOnes.ToList();
                allofem.AddRange(fieldNames);

                return allofem.Distinct();
            }
        }

        public IEnumerable<string> TopFew
        {
            get { return FieldNames.Take(8); }
        }

        public void Refresh()
        {
            fieldNames = fetcher();
            Notify("FieldNames", "TopFew");
        }

        public void Include(IEnumerable<string> defaultOnes)
        {
            this.defaultOnes = defaultOnes;
        }
    }
}
