﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AI.MetaData
{
    public static class CaseFields
    {
        public static void UpdateFields(IEnumerable<string> newFieldNames)
        {
            using (var db = new Connect.Database())
            {
                var existingFields = from m in db.CaseMetaDataFields
                                     orderby m.field
                                     select m;
                
                var existingFieldsList = (from m in existingFields select m).ToArray();

                foreach (var existing in existingFieldsList)
                {
                    if (newFieldNames.Contains(existing.field))
                    {
                        existing.tally++;
                    }
                }

                var properlyNewFields = (from m in newFieldNames
                                         where !Contains(existingFieldsList, m)
                                         select m).ToArray();

                db.CaseMetaDataFields.InsertAllOnSubmit(from f in properlyNewFields
                                                        select new Connect.CaseMetaDataField
                                                        {
                                                            caseMetaDataFieldId = Guid.NewGuid(),
                                                            field = f.Substring(0, Math.Min(f.Length, 255)),
                                                            tally = 1
                                                        });

                db.SubmitChanges();
            }
        }

        public static bool Contains(IEnumerable<Connect.CaseMetaDataField> fields, string oneToFind)
        {
            foreach (var f in fields)
            {
                if (string.Compare(f.field, oneToFind, StringComparison.CurrentCultureIgnoreCase) == 0)
                    return true;
            }

            return false;
        }

        public static IEnumerable<string> FieldNameFetcher()
        {
            using (var db = new Connect.Database())
            {
                var fields = (from f in db.CaseMetaDataFields
                              orderby f.tally descending
                              select f.field).ToArray();
                
                return fields;
            }
        }
    }
}
