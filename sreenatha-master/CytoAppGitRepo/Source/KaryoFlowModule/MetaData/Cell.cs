﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace AI.MetaData
{
    public static class CellFields
    {
        public static readonly string DefaultFieldId_Comment = "CellField_Comment";
        public static readonly string DefaultFieldId_Result = "CellField_Result";
        public static readonly string DefaultFieldId_BandQuality = "CellField_BandQuality";
        public static readonly string DefaultFieldId_CellLine = "CellField_CellLine";

        public static IEnumerable<string> DefaultFieldsStringTableIds
        {
            get
            {
                return new string[]
                {   
                    DefaultFieldId_BandQuality,
                    DefaultFieldId_CellLine,
                    DefaultFieldId_Comment,
                    DefaultFieldId_Result
                };
            }
        }

        public static IEnumerable<string> FieldNameFetcher()
        {
            using (var db = new Connect.Database())
            {

                var dbFields = (from f in db.CellMetaDatas
                                orderby f.field ascending
                                select f.field).Distinct();

                var newFields = from f in dbFields
                                select new
                                {
                                    Field = f,
                                    Tally = db.CellMetaDatas.Count(c => c.field == f)
                                };

                var theFields = from nf in newFields
                                orderby nf.Tally descending
                                select nf.Field;

                return theFields.ToArray();
            }
        }

        public static void Save(MetaData cellData, Guid cell)
        {
            using (var db = new Connect.Database())
            {
                cellData.Sort();

                var allForCell = db.CellMetaDatas.Where(m => m.cellId == cell).ToArray();
                db.CellMetaDatas.DeleteAllOnSubmit(allForCell);
                db.SubmitChanges();

                db.CellMetaDatas.InsertAllOnSubmit(from m in cellData.Fields
                                                   select new Connect.CellMetaData
                                                   {
                                                       cellId = cell,
                                                       cellMetaDataId = Guid.NewGuid(),
                                                       confidential = false,
                                                       field = m.Field.Substring(0, Math.Min(m.Field.Length, 255)),
                                                       value = m.Value
                                                   });
                db.SubmitChanges();
            }
        }
    }
}
