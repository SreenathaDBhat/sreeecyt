﻿using System;
using System.Windows.Input;
using System.Windows;

namespace AI.MetaData
{
    public partial class EditSingleMetaDataFieldWindow
    {
        public EditSingleMetaDataFieldWindow(FieldValuePair field, bool append)
        {
            if (field.Value == null)
                field.Value = string.Empty;

            if (append && field.Value.Trim().Length > 0)
                field.Value += ", ";

            DataContext = field;
            InitializeComponent();

            Loaded += (s, e) =>
                {
                    valueTextBox.Focus();
                    Keyboard.Focus(valueTextBox);

                    if (append)
                    {
                        valueTextBox.CaretIndex = field.Value.Length;
                    }
                    else
                    {
                        
                        valueTextBox.SelectAll();
                    }
                };
        }

        private void OnMoveWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
