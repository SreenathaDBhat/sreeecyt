﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Linq;
using System.Diagnostics;

namespace AI.MetaData
{
    public class FieldValuePair : Notifier
    {
        private string field = "";
        public string Field 
        { 
            get { return field; }
            set { field = value; Notify("Field"); }
        }

        private string val = "";
        public string Value
        { 
            get { return val; }
            set { val = value; Notify("Value", "IsEmpty"); }
        }

        public bool IsEmpty
        {
            get { return val == null || val.Length == 0; }
        }
    }


    public class MetaData : Notifier
    {
        private ObservableCollection<FieldValuePair> fields;

        public MetaData()
        {
            fields = new ObservableCollection<FieldValuePair>();
        }

        public FieldValuePair New()
        {
            var n = new FieldValuePair();
            fields.Add(n);

            Notify("IsEmpty");
            return n;
        }

        public void Delete(FieldValuePair val)
        {
            fields.Remove(val);
            Notify("IsEmpty");
        }

        public IEnumerable<FieldValuePair> Fields
        {
            get { return fields; }
        }

        public void AddRange(IEnumerable<FieldValuePair> items)
        {
            foreach (var i in items)
                fields.Add(i);

            Notify("IsEmpty");
        }

        public bool IsEmpty
        {
            get
            {
                if (fields.Count == 0)
                    return true;

                foreach (var f in fields)
                {
                    if (f.Value != null)
                        return false;
                }

                return true;
            }
        }

        public bool IsFieldPresent(string fieldName)
        {
            return fields.Where(f => f.Field == fieldName).FirstOrDefault() != null;
        }

        public void Sort()
        {
            var list = new List<FieldValuePair>(fields);
            list.Sort((a, b) => string.Compare(a.Field, b.Field));
            fields = new ObservableCollection<FieldValuePair>(list);
            Notify("Fields");
        }

        public string Get(string field)
        {
            return Get(field, string.Empty);
        }

        public string Get(string field, string fallback)
        {
            var f = fields.Where(a => a.Field == field).FirstOrDefault();
            if (f == null)
                return string.Empty;

            return f.Value;
        }

        public void Set(string field, string value)
        {
            var f = fields.Where(a => a.Field == field).FirstOrDefault();
            if (f == null)
            {
                f = new FieldValuePair()
                {
                    Field = field,
                    Value = value
                };

                fields.Add(f);
            }
            else
            {
                f.Value = value;
            }
        }

        public IEnumerable<MetaDataItem> ToLims()
        {
            return from f in fields
                   select new MetaDataItem
                   {
                       Field = f.Field,
                       Value = f.Value
                   };
        }

        public void ReplaceData(IEnumerable<FieldValuePair> replacementMetaData)
        {
            fields.Clear();
            foreach (var pair in replacementMetaData)
                fields.Add(pair);
        }

        public void ApplyTemplate(MetaDataTemplate metaDataTemplate)
        {
            foreach (var templateField in metaDataTemplate.Fields)
            {
                if (IsFieldPresent(templateField) == false)
                {
                    fields.Add(new FieldValuePair()
                    {
                        Field = templateField,
                        Value = string.Empty
                    });
                }
            }
        }

        public MetaData Clone()
        {
            MetaData clone = new MetaData();
            foreach (var pair in fields)
            {
                FieldValuePair fvp = new FieldValuePair();
                fvp.Field = pair.Field;
                fvp.Value = pair.Value;
                clone.fields.Add(fvp);
            }
            return clone;
        }
    }

    public class MetaDataTemplate
    {
        public IEnumerable<string> Fields { get; private set; }

        public static MetaDataTemplate LoadTemplate(string filePath)
        {
            if(!File.Exists(filePath))
                return null;

            try
            {
                XElement xml = XElement.Load(filePath);
                return new MetaDataTemplate
                {
                    Fields = (from f in xml.Descendants("Field")
                              select f.Attribute("Name").Value).ToArray()
                };
            }
            catch(Exception e)
            {
                Debug.WriteLine(e.ToString());
                return null;
            }
        }

        public static void SaveTemplate(string filePath, MetaDataTemplate metaTemplate)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);

            XElement xml = new XElement("MetaDataTemplate", from f in metaTemplate.Fields
                                                            select new XElement("Field", new XAttribute("Name", f)));

            xml.Save(filePath);
        }

        public static MetaDataTemplate FromMetaData(MetaData metaData)
        {
            return new MetaDataTemplate()
            {
                Fields = (from m in metaData.Fields
                          select m.Field).ToArray()
            };
        }
    }

    public interface IMetaDataContainer
    {
        MetaData MetaData { get; }
        Guid Id { get; }

        bool AreTemplatesSupported { get; }
        MetaDataTemplate Template { get; set; }
        FileInfo TemplateFile { get; }
    }
}
