﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Collections.Generic;
using System.Windows.Data;
using System.Windows.Media.Animation;

namespace AI.MetaData
{
    public partial class MetaDataEditor
    {
        public static readonly RoutedUICommand AddField = new RoutedUICommand("AddField", "AddField", typeof(MetaDataEditor));

        private IMetaDataContainer parent;
        private string nullComboText;
        private List<string> addedFields;
        private MetaData originalMetaData;

        public MetaDataEditor(IMetaDataContainer parent, AvailableFields fieldNames, string dialogTitle) : this(parent, fieldNames, dialogTitle, null)
        {
        }

        public MetaDataEditor(IMetaDataContainer parent, AvailableFields fieldNames, string dialogTitle, MetaDataTemplate initialTemplate)
        {
            this.parent = parent;
            DataContext = parent;
            originalMetaData = parent.MetaData.Clone();

            if (initialTemplate != null)
                parent.MetaData.ApplyTemplate(initialTemplate);

            addedFields = new List<string>();

            FieldNames = fieldNames;
            MetaDataTitle = dialogTitle;

            InitializeComponent();

            nullComboText = (string)FindResource("initialComboText");

            Loaded += (s, e) =>
                {
                    addCombo.SelectAll();
                    addCombo.Focus();
                    Keyboard.Focus(addCombo);
                };
        }

        public AvailableFields FieldNames { get; set; }
        public string MetaDataTitle { get; set; }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            Connect.ConnectionTest.Test(this);
            DialogResult = true;
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            parent.MetaData.ReplaceData(originalMetaData.Fields);
        }

        private void OnDelete(object sender, RoutedEventArgs e)
        {
            var field = (FieldValuePair)((FrameworkElement)sender).Tag;
            addedFields.Remove(field.Field);
            parent.MetaData.Delete(field);
        }

        private void OnMoveWindow(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void OnStealFocus(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).Focus();
            Keyboard.Focus((TextBox)sender);
        }

        private void CanAddFieldExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = addCombo != null && addCombo.Text != null && addCombo.Text.Length > 0 && addCombo.Text != nullComboText;
        }

        private void AddFieldExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var n = parent.MetaData.New();
            n.Field = addCombo.Text.Substring(0, Math.Min(addCombo.Text.Length, 255));

            addCombo.Text = nullComboText;

            addedFields.Add(n.Field);
        }

        private void OnKeyDownStealEnter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                e.Handled = true;

                if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
                {
                    Connect.ConnectionTest.Test(this);
                    DialogResult = true;
                }
                else
                {
                    AddField.Execute(null, null);
                }
            }
        }

        private void OnAddCellField(object sender, RoutedEventArgs e)
        {
            var fieldname = (string)((Button)sender).Tag;

            var n = parent.MetaData.New();
            n.Field = fieldname;
        }

        private void OnWhatever(object sender, RoutedEventArgs e)
        {
            addCombo.Focus();
            Keyboard.Focus(addCombo);
        }

        public IEnumerable<string> NewFields
        {
            get
            {
                return addedFields.Distinct();
            }
        }

        private void OnSaveTemplate(object sender, RoutedEventArgs e)
        {
            var anim = ((Storyboard)FindResource("templateSavedAnimation"));
            templateSavedIndicator.BeginStoryboard(anim);

            var t = MetaDataTemplate.FromMetaData(parent.MetaData);
            MetaDataTemplate.SaveTemplate(parent.TemplateFile.FullName, t);
            parent.Template = t;
        }
    }

    public class FieldTooLongConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int length = (int)value;
            return length < 256 ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
