﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Xml;
using System.Xml.Linq;

namespace AI.Packaging
{
    public static class Encryptor
    {
        private static readonly byte[] encryptionKey = { 1, 23, 2, 78, 4, 111, 39, 90, 76, 46, 77, 78, 8, 89, 53, 12, 3, 9, 19, 72, 21, 22, 29, 13, 126, 26, 48, 10, 26, 12, 7, 88 };
        private static readonly byte[] encryptionIV = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };

        public static XCData ToBase64(XElement source)
        {
            var stream = new MemoryStream();
            var enc = Rijndael.Create();
            enc.Padding = PaddingMode.PKCS7;
            CryptoStream cStream = new CryptoStream(stream,
                                                    enc.CreateEncryptor(encryptionKey, encryptionIV),
                                                    CryptoStreamMode.Write);

            int len;

            using (var writer = new StreamWriter(cStream, Encoding.Default))
            {
                source.Save(writer);
                writer.Flush();
                cStream.FlushFinalBlock();

                len = (int)stream.Length;
            }

            cStream.Close();
            var encryptedBytes = stream.GetBuffer();
            var base64 = System.Convert.ToBase64String(encryptedBytes, 0, len);
            var cdata = new XCData(base64);

            return cdata;
        }

        public static XElement XElementFromBase64(string base64)
        {
            byte[] data = Convert.FromBase64String(base64);
            MemoryStream stream = new MemoryStream(data);

            var enc = Rijndael.Create();
            enc.Padding = PaddingMode.PKCS7;
            CryptoStream cStream = new CryptoStream(stream,
                                                    enc.CreateDecryptor(encryptionKey, encryptionIV),
                                                    CryptoStreamMode.Read);

            StreamReader reader = new StreamReader(cStream);
            var content = reader.ReadToEnd();
            
            using (var stringReader = new StringReader(content))
            {
                XElement xml = XElement.Load(stringReader);
                return xml;
            }
        }
    }
}
