﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Packaging;
using System.Windows.Threading;
using System.IO;
using System.Net.Mime;
using System.Xml.Linq;
using System.Xml;

namespace AI.Packaging
{
    public interface IPackager
    {
        void Export(Package package, ILims lims, Case c, Connect.Database sourceDb);
        void Import(Package package, ILims lims, Case c, Connect.Database destDb);
    }

    #region VersionPacket
    internal class VersionPacket : IPackager
    {
        private static string Unknown = "Unknown version: version.xml not found in archive";
        private static Uri partUri = new Uri("/version.xml", UriKind.Relative);
        private string version;

        public VersionPacket(string version)
        {
            this.version = version;
        }

        public void Export(Package package, ILims lims, Case c, AI.Connect.Database sourceDb)
        {
            XElement versionXml = new XElement("version", new XAttribute("value", version));
            Packager.AddPartFromXml(package, partUri, versionXml);
        }

        public void Import(Package package, ILims lims, Case c, AI.Connect.Database destDb)
        {
            var ver = ReadVersion(package);

            if (ver != this.version)
            {
                throw new NotSupportedException("Version mismatch, expected: " + version + ", got: " + ver);
            }
        }

        public string ReadVersion(Package package)
        {
            var xml = Packager.ReadXmlPart(package, partUri);
            if (xml == null)
                return Unknown;

            return xml.Attribute("value").Value;
        }
    }
    #endregion

    public static class Packager
    {
        public static string GetVersion(Package p)
        {
            VersionPacket v = new VersionPacket(null);
            return v.ReadVersion(p);
        }

        public static void ExportCase(ILims lims, Case c, string destination, IEnumerable<IPackager> parts, Dispatcher uiDispatcher, string version)
        {
            using (var db = new Connect.Database())
            {
                Package package = Package.Open(destination);
                try
                {
                    var ver = new VersionPacket(version);
                    ver.Export(package, lims, c, db);

                    foreach (var p in parts)
                    {
                        p.Export(package, lims, c, db);
                        package.Flush();
                    }
                }
                finally
                {
                    package.Flush();
                    package.Close();
                }
            }
        }

        public static void ImportCase(Package package, ILims lims, Case c, IEnumerable<IPackager> parts, Dispatcher uiDispatcher, string version)
        {
            using (var db = new Connect.Database())
            {
                var dbCase = db.CreateCaseAndSlides(lims, c);
                CaseLocking.AcquireCaseLock(c);
                try
                {
                    var ver = new VersionPacket(version);
                    ver.Import(package, lims, c, db);

                    foreach (var p in parts)
                    {
                        p.Import(package, lims, c, db);
                        db.SubmitChanges();
                    }
                }
                finally
                {
                    CaseLocking.ReleaseCaseLock(c);
                }
            }
        }


        public static void AddPartFromXml(Package destPackage, Uri uri, XElement xml)
        {
            var part = destPackage.CreatePart(uri, MediaTypeNames.Text.Xml);
            var stream = part.GetStream();

            using (var writer = new XmlTextWriter(stream, Encoding.Default))
            {
                xml.Save(writer);
                writer.Flush();
            }
        }

        public static XElement ReadXmlPart(Package package, Uri uri)
        {
            if (!package.PartExists(uri))
                return null;

            var part = package.GetPart(uri);

            using (var stream = part.GetStream())
            {
                using (var reader = new XmlTextReader(stream))
                {
                    return XElement.Load(reader);
                }
            }
        }
    }
}
