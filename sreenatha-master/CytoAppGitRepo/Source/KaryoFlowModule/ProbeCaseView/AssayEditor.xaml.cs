﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;

namespace AI
{
    public partial class AssayEditor : UserControl
    {
        public static DependencyProperty AssayProperty = DependencyProperty.Register("Assay", typeof(ProbeScoringAssay), typeof(AssayEditor), new FrameworkPropertyMetadata(OnAssayChanged));
        public static DependencyProperty ScoresProperty = DependencyProperty.Register("Scores", typeof(IEnumerable<ProbeScore>), typeof(AssayEditor));

        private String duplicateName = string.Empty;
        public AssayEditor()
        {
            InitializeComponent();
        }

        public ProbeScoringAssay Assay
        {
            get { return (ProbeScoringAssay)GetValue(AssayProperty); }
            set { SetValue(AssayProperty, value); }
        }

        public IEnumerable<ProbeScore> Scores
        {
            get { return (IEnumerable<ProbeScore>)GetValue(ScoresProperty); }
            set { SetValue(ScoresProperty, value); }
        }

        static void OnAssayChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((AssayEditor)sender).DataContext = (ProbeScoringAssay)e.NewValue;
        }

        private void OnDeleteClass(object sender, RoutedEventArgs e)
        {
            ProbeScoringClass c = (ProbeScoringClass)((FrameworkElement)sender).Tag;
            Assay.DeleteClass(c);
        }

        private void CanNewClassExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (Assay != null) && (Assay.Classes.Count() < 10) && (Assay.DuplicateClassName == false);
        }

        private void NewClassExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            ProbeScoringClass newClass = new ProbeScoringClass()
            {
                ShortcutKey = Assay.NextAvailableShortcut(),
                Text = Assay.NextAvailableName()
            };

            Assay.AddClass(newClass);
        }

        private void OnSelectColor(object sender, RoutedEventArgs e)
        {
            var c = (ProbeScoringClass)((FrameworkElement)sender).Tag;
            System.Windows.Forms.ColorDialog dialog = new System.Windows.Forms.ColorDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var sel = dialog.Color;
                c.Color = Color.FromArgb(sel.A, sel.R, sel.G, sel.B);
            }
        }
    }



    public class DeleteButtonVisibilityConverter : IMultiValueConverter
    {
        // Bound to IsEnabled property of button
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue)
                return true;

            IEnumerable<ProbeScore> scores = (IEnumerable<ProbeScore>)values[0];
            ProbeScoringClass assayClass = (ProbeScoringClass)values[1];

            if (scores == null)
                return true;

            foreach (var s in scores)
            {
                if (s.Class == assayClass.Text)
                    return false;
            }

            return true;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
