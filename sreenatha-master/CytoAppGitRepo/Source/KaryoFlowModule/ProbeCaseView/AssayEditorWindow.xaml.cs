﻿using System;
using System.Windows;
using System.Collections.Generic;

namespace AI
{
    public partial class AssayEditorWindow : Window
    {
        private ProbeScoringAssay assay = null;

        public AssayEditorWindow(ProbeScoringAssay assay, IEnumerable<ProbeScore> scores)
        {
            this.assay = assay;
            Scores = scores;
            DataContext = assay;
            InitializeComponent();
        }

        public ProbeScoringAssay Assay
        {
            get { return (ProbeScoringAssay)DataContext; }
        }

        public IEnumerable<ProbeScore> Scores
        {
            get;
            set;
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void OnDragMove(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if(e.ChangedButton == System.Windows.Input.MouseButton.Left)
                DragMove();
        }

        private void CanAcceptAssayExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (assay != null) && (assay.DuplicateClassName == false);
        }

        private void AcceptAssayExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
