﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
namespace AI
{
    public partial class AssaySelector : Window
    {
        private AssayController assays;

        public AssaySelector(IEnumerable<ProbeScoringAssay> assays)
        {
            this.assays = new AssayController(assays);
            DataContext = this.assays;
            InitializeComponent();
        }

        public ProbeScoringAssay SelectedAssay
        {
            get { return assays.CurrentAssay; }
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void OnDragMove(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if(e.ChangedButton == System.Windows.Input.MouseButton.Left)
                DragMove();
        }

        private void OnNewAssay(object sender, RoutedEventArgs e)
        {
            assays.CurrentAssay = assays.CreateNew();
        }

        private void OnDuplicateAssay(object sender, RoutedEventArgs e)
        {
            assays.Duplicate(assays.CurrentAssay);
        }

        private void CanAcceptAssayExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (assays.CurrentAssay != null) && (assays.CurrentAssay.DuplicateClassName == false) && (assays.DuplicateAssayNames == false);
        }

        private void AcceptAssayExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            assays.Save();
            DialogResult = true;
        }

        private void DeleteAssayButton_Click(object sender, RoutedEventArgs e)
        {
            assays.Delete(assays.CurrentAssay);
        }
    }


    public class AssayController : Notifier
    {
        private ObservableCollection<ProbeScoringAssay> assays;
        private List<Guid> deletedAssays;
        private ProbeScoringAssay currentAssay;

        public AssayController(IEnumerable<ProbeScoringAssay> assays)
        {
            this.assays = new ObservableCollection<ProbeScoringAssay>(assays);
            deletedAssays = new List<Guid>();
        }

        public void Save()
        {
            var db = new Connect.Database();
            foreach (var deadId in deletedAssays)
            {
                var dbAssay = db.ProbeAssays.Where(a => a.assayId == deadId).First();
                db.ProbeAssays.DeleteOnSubmit(dbAssay);
            }

            foreach (var a in assays)
            {
                var dbAssay = db.ProbeAssays.Where(dba => dba.assayId == a.Id).FirstOrDefault();

                if (dbAssay == null)
                {
                    dbAssay = new Connect.ProbeAssay
                    {
                        assayId = Guid.NewGuid()
                    };

                    a.Id = dbAssay.assayId;
                    db.ProbeAssays.InsertOnSubmit(dbAssay);
                }

                dbAssay.assay = a.ToXml();
                dbAssay.name = a.Name;
            }

            db.SubmitChanges();
        }

        public IEnumerable<ProbeScoringAssay> Assays
        {
            get { return assays; }
        }

        public Boolean DuplicateAssayNames
        {
            get
            {
                Boolean dupes = false;

                foreach (var a in assays)
                {
                    int c = 0;
                    foreach (var b in assays)
                    {
                        if (a.Name == b.Name) c++;
                    }

                    if (c > 1)
                    {
                        dupes = true;
                        break;
                    }
                }

                return dupes;
            }
        }

        public ProbeScoringAssay CurrentAssay
        {
            get { return currentAssay; }
            set { currentAssay = value; Notify("CurrentAssay"); }
        }

        public ProbeScoringAssay CreateNew()
        {
            ProbeScoringAssay assay = new ProbeScoringAssay()
            {
                Name = "Unnamed"
            };

            string name = assay.Name;
            int n = 1;
            while (AssayAlreadyExists(assay))
            {
                assay.Name = name + " " + n++;
            }

            assays.Add(assay);
            return assay;
        }

        private bool AssayAlreadyExists(ProbeScoringAssay assay)
        {
            return assays.Count(a => string.Compare(a.Name, assay.Name, StringComparison.CurrentCultureIgnoreCase) == 0) > 0;
        }

        public void Delete(ProbeScoringAssay assay)
        {
            if(assay.Id != Guid.Empty)
                deletedAssays.Add(assay.Id);

            assays.Remove(assay);
        }

        public void Delete(Guid id)
        {
            if (id != Guid.Empty)
            {
                ProbeScoringAssay found = null;

                foreach (ProbeScoringAssay a in assays)
                {
                    if (a.Id == id)
                    {
                        found = a;
                        break;
                    }
                }

                if (found != null)
                {
                    Delete(found);
                }
            }
        }


        public void Duplicate(ProbeScoringAssay probeScoringAssay)
        {
            var clone = probeScoringAssay.Duplicate();
            assays.Add(clone);
            CurrentAssay = clone;
        }
    }
}
