﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace AI
{
	public class CurrentFrameChangingEventArgs : CancelEventArgs
	{
		public CurrentFrameChangingEventArgs(ImageFrame oldFrame, ImageFrame newFrame)
		{
			OldFrame = oldFrame;
			NewFrame = NewFrame;
		}

		public ImageFrame OldFrame { get; set; }
		public ImageFrame NewFrame { get; set; }
	}
}
