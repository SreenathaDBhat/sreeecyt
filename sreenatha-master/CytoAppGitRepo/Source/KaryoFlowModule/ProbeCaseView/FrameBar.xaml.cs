﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AI
{
    public partial class FrameBar : UserControl
    {
        public static RoutedUICommand HideFrameBar = new RoutedUICommand("Hide Frame Bar", "HideFrameBar", typeof(FrameBar));


        public FrameBar()
        {
            InitializeComponent();
        }

        public static DependencyProperty ItemTemplateProperty = DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(FrameBar), new FrameworkPropertyMetadata(new PropertyChangedCallback(OnItemTemplateChanged)));
        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        private static void OnItemTemplateChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            
        }
    }
}
