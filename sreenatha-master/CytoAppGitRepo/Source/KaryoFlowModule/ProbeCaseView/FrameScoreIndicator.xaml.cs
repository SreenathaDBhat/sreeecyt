﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace AI
{
    public partial class FrameScoreIndicator : UserControl
    {
        public FrameScoreIndicator()
        {
            InitializeComponent();
        }

        public static DependencyProperty ClassesProperty = DependencyProperty.Register("Classes", typeof(IEnumerable<Color>), typeof(FrameScoreIndicator));
        public static DependencyProperty ScoredInOtherSessionProperty = DependencyProperty.Register("ScoredInOtherSession", typeof(bool), typeof(FrameScoreIndicator));
        public static DependencyProperty FrameStarredProperty = DependencyProperty.Register("FrameStarred", typeof(bool), typeof(FrameScoreIndicator));
        public static DependencyProperty FrameHasRatioScoresProperty = DependencyProperty.Register("FrameHasRatioScores", typeof(bool), typeof(FrameScoreIndicator));

        public IEnumerable<Color> Classes
        {
            get { return (IEnumerable<Color>)GetValue(ClassesProperty); }
            set { SetValue(ClassesProperty, value); }
        }

        public bool ScoredInOtherSession
        {
            get { return (bool)GetValue(ScoredInOtherSessionProperty); }
            set { SetValue(ScoredInOtherSessionProperty, value); }
        }

        public bool FrameStarred
        {
            get { return (bool)GetValue(FrameStarredProperty); }
            set { SetValue(FrameStarredProperty, value); }
        }

        public bool FrameHasRatioScores
        {
            get { return (bool)GetValue(FrameHasRatioScoresProperty); }
            set { SetValue(FrameHasRatioScoresProperty, value); }
        }
    }

    public class FrameScoreConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            IScorerAugmentedFrame r = (IScorerAugmentedFrame)value;
            IEnumerable<ProbeScore> results = (IEnumerable<ProbeScore>)r.AllScores;
            ImageFrame frame = r.Frame;

            var all = results.Where(s => s.Frame == frame).ToList().ConvertAll(s => s.Color);
            var distinct = all.Distinct();
            return distinct;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class FrameScoredByOtherConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            IScorerAugmentedFrame r = (IScorerAugmentedFrame)value;
            IEnumerable<Guid> otherFrames = (IEnumerable<Guid>)r.AllFramesTouchedInOtherSessions;
            ImageFrame frame = r.Frame;

            return otherFrames.Where(ps => ps == frame.Id).Count() > 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class FrameHasRatioScoresConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            IScorerAugmentedFrame r = (IScorerAugmentedFrame)value;
            IEnumerable<CountedCell> countedCells = (IEnumerable<CountedCell>)r.AllScores;
            ImageFrame frame = (ImageFrame)r.Frame;

            return countedCells.Where(c => c.Frame == frame).Count() > 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
