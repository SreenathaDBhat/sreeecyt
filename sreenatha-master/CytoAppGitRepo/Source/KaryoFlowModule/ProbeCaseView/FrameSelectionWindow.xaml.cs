﻿using System;
using System.Linq;
using System.Windows;
using System.Collections.Generic;

namespace AI
{
    public partial class FrameSelectionWindow
    {
        public FrameSelectionWindow(IScorer scoreTool, DataTemplate itemTemplate, ImageFrame initialSelection)
        {
            ItemTemplate = itemTemplate;
            DataContext = scoreTool;
            InitializeComponent();

            Loaded += (s, e) =>
            {
                int i = 0;
                foreach (var f in scoreTool.FramesForSelectionWindow)
                {
                    if (f.Frame == initialSelection)
                        frameListBox.SelectedIndex = i;

                    ++i;
                }
            };
        }

        public DataTemplate ItemTemplate
        {
            get;
            private set;
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        public ImageFrame SelectedFrame
        {
            get { return (frameListBox.SelectedItem as IScorerAugmentedFrame).Frame; }
        }

        DateTime last = DateTime.MinValue;
        private void OnDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var now = DateTime.Now;
            if ((now - last).TotalSeconds < 0.3)
            {
                DialogResult = true;
            }

            last = now;
        }

        private void OnMouseDownDrag(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
