﻿using System;
using System.Windows.Controls;
using System.Linq;
using System.Windows;

namespace AI
{
    public partial class FrameThumbnailScoreSummary : UserControl
    {
        public static DependencyProperty ScoreToolProperty = DependencyProperty.Register("ScoreTool", typeof(ScoringTool), typeof(FrameThumbnailScoreSummary), new FrameworkPropertyMetadata(OnScoreToolChanged));

        public FrameThumbnailScoreSummary()
        {
            InitializeComponent();
        }

        public ScoringTool ScoreTool
        {
            get { return (ScoringTool)GetValue(ScoreToolProperty); }
            set { SetValue(ScoreToolProperty, value); }
        }

        static void OnScoreToolChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            FrameThumbnailScoreSummary tis = (FrameThumbnailScoreSummary)sender;

            object frame = tis.DataContext;
            ScoringTool tool = (ScoringTool)e.NewValue;

            tis.theList.ItemsSource = tool.ScoresForFrame(null);
        }
    }
}
