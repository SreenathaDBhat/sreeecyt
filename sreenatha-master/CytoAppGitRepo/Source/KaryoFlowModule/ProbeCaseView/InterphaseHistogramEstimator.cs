﻿using System;
using System.Collections.Generic;

namespace AI
{
    public class InterphaseHistogramEstimator : IHistogramEstimator
    {
        public override void Estimate(ImageFrame imageFrame, ChannelRenderParameters channelParams, IEnumerable<RenderPass> passes)
        {
            if (channelParams.ChannelInfoX.IsCounterstain)
            {
                channelParams.Low = 0;
                channelParams.High = 1;
                return;
            }

            if (IsBackgroundSubtractionTurnedOn(passes))
            {
                EstimateForBgSubtracted(channelParams);
            }
            else
            {
                Estimate(channelParams);
            }
        }

        private static bool IsBackgroundSubtractionTurnedOn(IEnumerable<RenderPass> passes)
        {
            foreach (var pass in passes)
            {
                if (pass is BackgroundSubtractionFilter)
                    return true;
            }

            return false;
        }

        private unsafe void EstimateForBgSubtracted(ChannelRenderParameters channelParams)
        {
            int peakLocation = 0;
            int peakValue = 0;
            int endOfScale = 255;
            double threshold = 10; // remember: histograms are normalized, 0..1000

            fixed (int* start = &channelParams.HistogramBins[0])
            {
                // ignore the 10 bins on either end...usually full of junk???????
                int* bin = start;
                int* end = start + channelParams.HistogramBins.Length;


                while (bin != end)
                {
                    int value = *bin;

                    if (value > threshold)
                        endOfScale = (int)(bin - start);

                    if (value > peakValue)
                    {
                        peakValue = value;
                        peakLocation = (int)(bin - start);
                    }

                    ++bin;
                }
            }

            if (endOfScale < peakLocation + 20)
                endOfScale = peakLocation + 20;


            channelParams.Low = (peakLocation + 2) / 255.0;
            channelParams.High = endOfScale / 255.0;
        }

        private unsafe void Estimate(ChannelRenderParameters channelParams)
        {
            int peakLocation = 0;
            int peakValue = 0;
            int endOfValueScale = 255;
            int endOfSignificanceScale = 0;
            double threshold = 10;

            fixed (int* start = &channelParams.HistogramBins[0])
            {
                // ignore the 10 bins on either end...usually full of junk???????
                int* bin = start + 10;
                int* end = start + (channelParams.HistogramBins.Length - 10);


                while (bin != end)
                {
                    int value = *bin;

                    if (value > threshold)
                        endOfSignificanceScale = (int)(bin - start);

                    if (value > peakValue)
                    {
                        peakValue = value;
                        peakLocation = (int)(bin - start);
                    }

                    ++bin;
                }
            }

            int halfway = (int)((endOfSignificanceScale + endOfValueScale) / 2);


            double adjustedPeak = peakLocation + 5;

            channelParams.Low = adjustedPeak / 255.0;
            channelParams.High = halfway / 255.0;
        }
    }
}
