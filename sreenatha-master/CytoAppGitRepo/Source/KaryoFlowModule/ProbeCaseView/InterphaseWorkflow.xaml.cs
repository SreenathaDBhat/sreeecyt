﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Threading;
using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace AI
{
    public partial class InterphaseWorkflow
    {
        public static RoutedUICommand MarkupSlide = new RoutedUICommand("MarkupSlide", "MarkupSlide", typeof(InterphaseWorkflow));
        //public static RoutedUICommand ShowCaseDetails = new RoutedUICommand("Show Case Details", "ShowCaseDetails", typeof(InterphaseWorkflow));

        private InterphaseWorkflowController controller;
        private Guid caseId;
        private string caseName;
        private Guid highlightSlide;
        private Case limsCase;

        public InterphaseWorkflow(Guid caseId, string caseName, Guid highlightSlide)
        {
            using (var lims = Connect.LIMS.New())
            using (var db = new Connect.Database())
            {
                var dbc = db.Cases.Where(c => c.caseId == caseId).First();
                limsCase = lims.GetCase(dbc.limsRef);
            }

            this.highlightSlide = highlightSlide;
            this.caseId = caseId;
            this.caseName = caseName;
            InitializeComponent();

            Loaded += new RoutedEventHandler(InterphaseWorkflow_Loaded);
        }

        void InterphaseWorkflow_Loaded(object sender, RoutedEventArgs e)
        {
            controller = new InterphaseWorkflowController(caseId, caseName, highlightSlide, Dispatcher);
            DataContext = controller;
			this.resultsCheckBox.IsChecked = controller.ScoreVisible;
            captureNewSlidePanel.Visibility = Visibility.Collapsed;
        }

        private void OnBack(object sender, System.Windows.RoutedEventArgs e)
        {
            UIStack.For(this).Pop(UIStackResult.Cancel);
        }

		private void OnShowResults(object sender, RoutedEventArgs e)
		{
			controller.ScoreVisible = !controller.ScoreVisible;
			((CheckBox)e.Source).IsChecked = controller.ScoreVisible;
		}

        private void OnScore(object sender, System.Windows.RoutedEventArgs e)
        {
            InterphaseSlide slide = (InterphaseSlide)((FrameworkElement)sender).Tag;
            InterphaseScoreSession newSession = new InterphaseScoreSession() { Id = Guid.NewGuid(), SlideId = slide.Id, Owner = Environment.UserName };
            string assayName = controller.GetAssayOfMostRecentSession(slide);
            LaunchProbeScoring(newSession, assayName);
        }

        private void OnRatioScore(object sender, System.Windows.RoutedEventArgs e)
        {
            InterphaseSlide slide = (InterphaseSlide)((FrameworkElement)sender).Tag;
            InterphaseScoreSession newSession = new InterphaseScoreSession() { Id = Guid.NewGuid(), SlideId = slide.Id, Owner = Environment.UserName };
            LaunchRatioScoring(newSession);
        }

        private void OnReview(object sender, RoutedEventArgs e)
        {
            doSomethingToResultsPopup.IsOpen = false;
            InterphaseScoreSession session = (InterphaseScoreSession)((FrameworkElement)sender).Tag;
            UIStack.For(this).Push(new ReviewScoresPage(session.Id, controller.CaseId));
        }

        private void OnEdit(object sender, RoutedEventArgs e)
        {
            doSomethingToResultsPopup.IsOpen = false;
            InterphaseScoreSession existingSession = (InterphaseScoreSession)((FrameworkElement)sender).Tag;
            if (existingSession.Type == ScoreSessionType.ClassScoring)
                LaunchProbeScoring(existingSession);
            else
                LaunchRatioScoring(existingSession);
        }

        private void LaunchRatioScoring(InterphaseScoreSession session)
        {
            var ratioScoringToolUi = new RatioScoringToolUi(new RatioScoringTool(controller.CaseId, session));
            UIStack.For(this).Push(ratioScoringToolUi);
        }

        private void LaunchProbeScoring(InterphaseScoreSession scoreSession)
        {
            var scoringToolUi = new ScoringToolUi(new ScoringTool(controller.CaseId, scoreSession));
            UIStack.For(this).Push(scoringToolUi);
        }

        private void LaunchProbeScoring(InterphaseScoreSession newSession, string assayName)
        {
            var scoringToolUi = new ScoringToolUi(new ScoringTool(controller.CaseId, newSession, assayName));
            UIStack.For(this).Push(scoringToolUi);
        }

        private void OnDelete(object sender, RoutedEventArgs e)
        {
            doSomethingToResultsPopup.IsOpen = false;
            InterphaseScoreSession existingSession = (InterphaseScoreSession)((FrameworkElement)sender).Tag;
            controller.DeleteScoreSession(existingSession);
        }

        private void OnShowOpsPopup(object sender, RoutedEventArgs e)
        {
            var session = (InterphaseScoreSession)((FrameworkElement)sender).Tag;
            doSomethingToResultsPopup.Tag = session;

            if (session.Type == ScoreSessionType.ClassScoring && ((ProbeScoringResults)session.Results).HasResults)
                reviewButton.Visibility = Visibility.Visible;
            else
                reviewButton.Visibility = Visibility.Collapsed;

            doSomethingToResultsPopup.IsOpen = true;
        }

        private void CanMarkupSlideExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            InterphaseSlide slide = e.Parameter as InterphaseSlide;
            if (slide == null)
                return;
         
            e.CanExecute = slide.GotWholeSlideImage && slide.IsFishSlide;
        }

        private void MarkupSlideExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            InterphaseSlide slide = (InterphaseSlide)e.Parameter;
            MarkupSlideForCapture markup = new MarkupSlideForCapture(slide.Id);
            UIStack.For(this).Push(markup);
        }

        private void OnSlideStatusChanged(object sender, SelectionChangedEventArgs e)
        {
            var combo = (ComboBox)sender;
            var slide = (InterphaseSlide)combo.Tag;
            var db = new Connect.Database();
            var status = (string)combo.SelectedItem;

            var dbSlide = db.Slides.Where(s => s.slideId == slide.Id).First();
            var dbStatus = db.SlideStatus.Where(s => s.description == status).First();
            dbSlide.slideStatus = dbStatus.sequence;
            dbSlide.statusChanged = DateTime.Now;
            db.SubmitChanges();
        }

        private void OnCapNewSlide(object sender, RoutedEventArgs e)
        {
            captureNewSlidePanel.Visibility = Visibility.Visible;
            Keyboard.Focus(newSlideName);
            newSlideName.Text = GenerateUniqueSlideName();
            newSlideName.Focus();
            newSlideName.SelectAll();
        }

        private void OnCancelNewSlide(object sender, RoutedEventArgs e)
        {
            captureNewSlidePanel.Visibility = Visibility.Collapsed;
            newSlideName.Text = "";
        }

        private void OnAcceptNewSlide(object sender, RoutedEventArgs e)
        {
            var slideId = CaseManagement.CreateSlide(Connect.LIMS.Case(caseId), newSlideName.Text, "", SlideTypes.FISH);
            MiscCommands.LaunchFishCapture.Execute(slideId, this);
        }

        
        private IEnumerable<string> existingNames;

        private string GenerateUniqueSlideName()
        {
            EnsureWeGotSlideNames();
            string possibleSlideName = "slide 1";
            int i = 1;

            while (existingNames.Contains(possibleSlideName))
            {
                ++i;
                possibleSlideName = "slide " + i;
            }

            return possibleSlideName;
        }

        private void ValidateNewSlideName(object sender, TextChangedEventArgs e)
        {
            EnsureWeGotSlideNames();

            string typed = newSlideName.Text;
            if (typed.Length == 0 || existingNames.Contains(typed.ToLower()))
            {
                newSlideOkButton.IsEnabled = false;
            }
            else
            {
                newSlideOkButton.IsEnabled = true;
            }
        }

        private void EnsureWeGotSlideNames()
        {
            if (existingNames == null)
            {
                using (var lims = Connect.LIMS.New())
                {
                    existingNames = from s in lims.GetSlides(limsCase)
                                    select s.Name;
                }
            }
        }
    }

    public enum ScoreSessionType
    {
        ClassScoring,
        RatioScoring
    }

    public class InterphaseScoreSession
    {
        public Guid Id { get; set; }
        public Guid SlideId { get; set; }
        public string Owner { get; set; }
        public string Assay { get; set; }
        public ScoreSessionType Type { get; set; }
        public DateTime Modified { get; set; }
        public object Results { get; set; }

        public string HiddenResultsText
        {
            get
            {
                if (Type == ScoreSessionType.RatioScoring)
                    return "Ratio Scoring";
                return "Class Scoring\nAssay: " + Assay;
            }
        }
    }

    public class InterphaseSlide : Notifier
    {
        private ObservableCollection<InterphaseScoreSession> scoringSessions = new ObservableCollection<InterphaseScoreSession>();

        public string Name { get; set; }
        public Guid Id { get; set; }
        public IEnumerable<ImageFrame> Frames { get; set; }
        public ImageSource Thumbnail { get; set; }
        public bool GotFrames { get; set; }
        public bool IsFishSlide { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public byte StatusInt { get; set; }
        public bool Highlight { get; set; }

        public IEnumerable<InterphaseScoreSession> ScoringSessions
        {
            get { return scoringSessions; }
            set
            {
                scoringSessions = new ObservableCollection<InterphaseScoreSession>(value);
                Notify("ScoringSessions");
                Notify("GotResults");
            }
        }

        public void RemoveScoringSession(InterphaseScoreSession session)
        {
            scoringSessions.Remove(session);
            Notify("GotResults");
        }

        public void ForceNotify(string property)
        {
            Notify(property);
        }

        public bool GotResults
        {
            get { return scoringSessions.Count > 0; }
        }

        public bool GotWholeSlideImage
        {
            get;
            set;
        }
    }

    public class ScoringResultUiSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var session = (InterphaseScoreSession)item;
            var ui = (InterphaseWorkflow)VisualTreeWalker.FindParentOfType<InterphaseWorkflow>(container);
            var templateName = session.Type == ScoreSessionType.ClassScoring ? "classResultsUi" : "ratioResultsUi";
            var template = (DataTemplate)ui.FindResource(templateName);
            return template;
        }
    }

    public class InterphaseWorkflowController : Notifier
    {
		private bool scoreVisible;

        public string CaseName { get; set; }
        public Guid CaseId { get; set; }
        public IEnumerable<InterphaseSlide> Slides { get; set; }

        public InterphaseWorkflowController(Guid caseId, string caseName, Guid highlightSlide, Dispatcher uiDispatcher)
        {
            CaseId = caseId;
            uiDispatcher = Dispatcher.CurrentDispatcher;
            CaseName = caseName;
            ScoreVisible = false;

            using (var db = new Connect.Database())
            using (var lims = Connect.LIMS.New())
            {
                var dbc = db.Cases.Where(c => c.caseId == caseId).First();
                var lc = lims.GetCase(dbc.limsRef);
                var allslides = lims.GetSlides(lc);

                var list = new List<InterphaseSlide>();
                foreach (var s in allslides)
                {
                    var dbs = db.Slides.Where(sl => sl.limsRef == s.Id).First();

                    list.Add(new InterphaseSlide
                      {
                          Id = dbs.slideId,
                          Name = s.Name,
                          IsFishSlide = dbs.slideType == SlideTypes.FISH,
                          Type = dbs.slideType,
                          StatusInt = dbs.slideStatus,
                          Highlight = highlightSlide == dbs.slideId
                      });
                }
                
                Slides = list;
                SlideStatuses = (from s in db.SlideStatus
                                 select s.description).ToArray();

                foreach (var s in Slides)
                {
                    s.GotWholeSlideImage = db.SlideImageTiles.Where(t => t.slideId == s.Id).Count() > 0;
                    s.Status = db.SlideStatus.Where(t => t.sequence == s.StatusInt).First().description;
                }

                Notify("GotResults");

                foreach (var s in Slides)
                {
                    s.GotFrames = db.ImageFrames.Where(f => f.slideId == s.Id).Count() > 0; // don't need this on the thread...
                    LoadSlideInfo(s, uiDispatcher); // async
                }
            }
        }

        public IEnumerable<string> SlideStatuses
        {
            get;
            private set;
        }

		public bool ScoreVisible
		{
			get
			{
				return scoreVisible;
			}
			set
			{
				scoreVisible = value;
				Notify("ScoreVisible");
			}
		}

		public bool GotResults
		{
			get
			{
				int resultsCount = 0;

				foreach (InterphaseSlide s in Slides)
				{
					if (s.GotResults)
					{
						resultsCount++;
					}
				}

				return resultsCount > 0;
			}
		}

        private void LoadSlideInfo(InterphaseSlide slide, Dispatcher uiDispatcher)
        {
            ThreadPool.QueueUserWorkItem((WaitCallback)delegate
            {
                var db = new Connect.Database();

				try
				{
					// scoring results
					var results = from s in db.ProbeScoreSessions
								  where s.slideId == slide.Id
								  orderby s.modified
								  select new InterphaseScoreSession
								  {
									  Id = s.sessionId,
									  Owner = s.ownerUser,
									  SlideId = s.slideId,
									  Assay = s.assay.Attribute("Name").Value,
									  Type = (ScoreSessionType)s.type,
									  Modified = s.modified
								  };

					if (results != null)
					{
						var sessions = results.ToList();

						foreach (var session in sessions)
						{
							if (session.Type == ScoreSessionType.ClassScoring)
								LoadClassScoringResults(session, db);
							else
								LoadRatioScoringResults(session, db);
						}

						uiDispatcher.Invoke((ThreadStart)delegate
						{
							slide.ScoringSessions = sessions;
							slide.ForceNotify("ScoringSessions");
							Notify("GotResults");
						}, DispatcherPriority.Normal);
					}


					var rand = new Random();
					var frameIds = from f in db.ImageFrames
								   where f.slideId == slide.Id
								   select f.imageFrameId;

					int count = frameIds.Count();
					if (count > 0)
					{
						int n = rand.Next(count);
						var frameId = frameIds.ToArray()[n];
						var path = frameId.ToString() + ".jpg";

						var stream = Connect.BlobStream.ReadBlobStream(CaseId, path, db, AI.Connect.CacheOption.DontCache);
						if (stream != null)
						{
							BitmapImage img = new BitmapImage();
							img.BeginInit();
							img.StreamSource = stream;
							img.EndInit();
							img.Freeze();

							uiDispatcher.Invoke((ThreadStart)delegate
							{
								slide.Thumbnail = img;
								slide.ForceNotify("Thumbnail");

							}, DispatcherPriority.Normal);
						}
					}
				}
				catch (Exception ex)
				{
					uiDispatcher.Invoke((ThreadStart)delegate
					{
						ExceptionUtility.ShowLog(ex, "LoadSlideInfo thread");
					}, DispatcherPriority.Normal);
				}
				finally
				{
					db.Dispose();
				}

            }, uiDispatcher);
        }

        private void LoadRatioScoringResults(InterphaseScoreSession session, AI.Connect.Database db)
        {
            var countedCells = from s in db.ProbeScores
                               where s.sessionId == session.Id
                               select RatioScoringTool.CountedCellFromDatabase(s);

            RatioScoringResults results = new RatioScoringResults();
            foreach (CountedCell cell in countedCells)
                results.Add(cell);

            session.Results = results;
        }

        private void LoadClassScoringResults(InterphaseScoreSession session, AI.Connect.Database db)
        {
            var scores = from s in db.ProbeScores
                         where s.sessionId == session.Id
                         select new ProbeScore
                         {
                             Class = s.content,
                             Color = ColorParse.HexStringToColor(s.color),
                             Id = s.scoreId,
                             SpotMeasurements = "",
                             X = s.x,
                             Y = s.y
                         };

            session.Results = ProbeScoringResultsGenerator.GenerateResultsForSlide(scores);
        }

        internal void DeleteScoreSession(InterphaseScoreSession existingSession)
        {
            var db = new Connect.Database();

            var allTypesOfProbeScores = from ps in db.ProbeScores
                                        where ps.sessionId == existingSession.Id
                                        select ps;

            foreach (var probeScore in allTypesOfProbeScores)
            {
                Connect.BlobStream.Delete(probeScore.blobId, db);
            }

            db.ProbeScoreSessions.DeleteOnSubmit(db.ProbeScoreSessions.Where(ss => ss.sessionId == existingSession.Id).First());
            db.SubmitChanges();

            var slide = Slides.Where(s => s.Id == existingSession.SlideId).First();
            slide.RemoveScoringSession(existingSession);
			Notify("GotResults");
        }

        internal string GetAssayOfMostRecentSession(InterphaseSlide slide)
        {
            InterphaseScoreSession lastSession = null;
            if (slide.GotResults)
            {
                DateTime mostRecent = DateTime.MinValue;

                foreach (InterphaseScoreSession session in slide.ScoringSessions)
                {
                    if (session.Type == ScoreSessionType.ClassScoring && session.Modified > mostRecent)
                    {
                        mostRecent = session.Modified;
                        lastSession = session;
                    }
                }
            }
            if (lastSession == null)
                return string.Empty;
            else
                return lastSession.Assay;
        }
    }
}
