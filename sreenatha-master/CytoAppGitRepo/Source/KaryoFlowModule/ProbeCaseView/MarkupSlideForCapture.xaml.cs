﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows;
using System.Xml.Linq;
using System.Windows.Input;

namespace AI
{
    public partial class MarkupSlideForCapture
    {
        private CaptureMarkupController controller;

        public MarkupSlideForCapture(Guid slide)
        {
            controller = new CaptureMarkupController(slide);
            DataContext = controller;
            InitializeComponent();

            var statuses = new List<string>();
            statuses.Add((string)FindResource("dontChangeStatus"));
            statuses.AddRange(controller.SlideStatuses);

            statusCombo.ItemsSource = statuses;
            statusCombo.SelectedIndex = Math.Min(statuses.Count - 1, Math.Max(0, RememberedStatusIndex()));

            if (controller.OtherSlideImagesFromCase.Count() > 0)
            {
                theGrid.Columns = 2;

                MarkupSlideForCaptureOtherSlides m = new MarkupSlideForCaptureOtherSlides();
                m.DataContext = controller.OtherSlideImagesFromCase;
                theGrid.Children.Add(m);
            }
            else
            {
                theGrid.Columns = 1;
            }
        }

        private int RememberedStatusIndex()
        {
            var file = AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\markup.status");
            if (file.Exists)
            {
                XElement xml = XElement.Load(file.FullName);
                return int.Parse(xml.Attribute("Index").Value);
            }

            return 0;
        }

        private void RememberStatusIndex(int index)
        {
            var file = AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\markup.status");
            if (file.Exists)
                file.Delete();

            XElement xml = new XElement("Markup", new XAttribute("Index", index));
            xml.Save(file.FullName);
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            RememberStatusIndex(statusCombo.SelectedIndex);
            controller.Save(statusCombo.SelectedIndex - 1);
            UIStack.For(this).Pop(UIStackResult.Cancel);
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            UIStack.For(this).Pop(UIStackResult.Cancel);
        }

        private void OnStartBoxDrag(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            e.Handled = true;
            FrameworkElement ui = (FrameworkElement)sender;
            ui.CaptureMouse();

            controller.StartDrawingBox(e.GetPosition(ui));
        }

        private void OnContinueBoxDrag(object sender, System.Windows.Input.MouseEventArgs e)
        {
            e.Handled = true;
            FrameworkElement ui = (FrameworkElement)sender;
            controller.ContinueDrawingBox(e.GetPosition(ui));
        }

        private void OnFinnishBoxDrag(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            e.Handled = true;
            FrameworkElement ui = (FrameworkElement)sender;
            ui.ReleaseMouseCapture();

            controller.FinishDrawing();
            Keyboard.Focus(commentBox);
        }

        private void OnDeleteRegion(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            controller.DeleteRegion((CaptureMarkupController.DrawnRegion)((FrameworkElement)sender).Tag);
        }

        private void OnSelectRegion(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            controller.SelectRegion((CaptureMarkupController.DrawnRegion)((FrameworkElement)sender).Tag);
        }
    }



    public class CaptureMarkupController : Notifier
    {
        public class DrawnRegion : Notifier
        {
            private Rect rect;

            public DrawnRegion()
            {
            }

            public DrawnRegion(Point topLeft)
            {
                rect = new Rect(topLeft, new Point(topLeft.X + 1, topLeft.Y + 1));
            }

            public Guid Id { get; set; }
            public double X 
            { 
                get { return rect.Left; } 
                set { rect = new Rect(value, Y, Width, Height); } 
            }
            public double Y 
            { 
                get { return rect.Top; } 
                set { rect = new Rect(X, value, Width, Height); } 
            }
            public double Width 
            { 
                get { return rect.Width; } 
                set { rect = new Rect(X, Y, value, Height); } 
            }
            public double Height
            {
                get { return rect.Height; }
                set { rect = new Rect(X, Y, Width, value); }
            }

            public DateTime Modified { get; set; }
            public CaptureStatus Status { get; set; }

            public bool IsTooBig
            {
                get { return Width > 10 || Height > 10; }
            }

            public void MoveBottomRightTo(Point bottomRight)
            {
                rect = new Rect(rect.Left, rect.Top, Math.Max(1, bottomRight.X - rect.Left), Math.Max(1, bottomRight.Y - rect.Top));
                Notify("Width", "Height", "IsTooBig");
            }

            public string Owner { get; set; }

            private string comment;
            public string Comment
            {
                get { return comment; }
                set { comment = value; Notify("Comment"); }
            }

            private bool selected;
            public bool IsSelected
            {
                get { return selected; }
                set { selected = value; Notify("IsSelected"); }
            }
        }

        private Guid slide;
        private ObservableCollection<WholeSlide> otherImagesFromCase;
        private WholeSlideDisplayLocation sharedDisplay;
        private DrawnRegion regionBeingDrawn;
        private ObservableCollection<DrawnRegion> regions;
        private WholeSlide markupSlide;

        public CaptureMarkupController(Guid slide)
        {
            this.slide = slide;
            var db = new Connect.Database();

            var slideInfo = db.Slides.Where(s => s.slideId == slide).First();

            otherImagesFromCase = new ObservableCollection<WholeSlide>();
            regions = new ObservableCollection<DrawnRegion>();

            sharedDisplay = new WholeSlideDisplayLocation();

            regions = new ObservableCollection<DrawnRegion>(from d in db.DeferredCaptures
                                                            where d.slideId == slide
                                                            select new DrawnRegion
                                                            {
                                                                Id = d.regionId,
                                                                
                                                                X = d.left / 100,
                                                                Y = d.top / 100,
                                                                Width = (d.right - d.left) / 100,
                                                                Height = (d.bottom - d.top) / 100,
                                                                Comment = d.comment,
                                                                Owner = d.owner,
                                                                Modified = d.modified,
                                                                Status = (CaptureStatus)d.status
                                                            });


            SlideStatuses = (from s in db.SlideStatus orderby s.sequence select s.description).ToArray();

            markupSlide = new WholeSlide(slideInfo.caseId, slideInfo.slideId);
            markupSlide.DisplayLocation = sharedDisplay;

            var caseSlides = db.Slides.Where(s => s.caseId == slideInfo.caseId);
            foreach (var s in caseSlides)
            {
                if(s.slideId != slide && db.SlideImageTiles.Where(t => t.slideId == s.slideId).Count() > 0)
                {
                    var w = new WholeSlide(slideInfo.caseId, s.slideId);
                    w.DisplayLocation = sharedDisplay;
                    otherImagesFromCase.Add(w);
                }
            }
        }

        public IEnumerable<string> SlideStatuses
        {
            get;
            private set;
        }

        public WholeSlide MarkupSlide
        {
            get { return markupSlide; }
        }

        public IEnumerable<WholeSlide> OtherSlideImagesFromCase
        {
            get { return otherImagesFromCase; }
        }

        public IEnumerable<DrawnRegion> Regions
        {
            get { return regions; }
        }

        public void Save(int status)
        {
            var db = new Connect.Database();
            
            db.DeferredCaptures.DeleteAllOnSubmit(db.DeferredCaptures.Where(d => d.slideId == slide));
            db.SubmitChanges();

            db.DeferredCaptures.InsertAllOnSubmit(from r in regions
                                                  select new Connect.DeferredCapture
                                                  {
                                                      left = r.X * 100,
                                                      right = (r.X + r.Width) * 100,
                                                      top = r.Y * 100,
                                                      bottom = (r.Y + r.Height) * 100,
                                                      options = new XElement("Options"),
                                                      regionId = r.Id == Guid.Empty ? Guid.NewGuid() : r.Id,
                                                      slideId = slide,
                                                      owner = r.Owner,
                                                      modified = r.Modified,
                                                      status = (int)r.Status,
                                                      comment = r.Comment
                                                  });

            if (status >= 0)
            {
                var dbSlide = db.Slides.Where(s => s.slideId == slide).First();
                dbSlide.slideStatus = (byte)status;
                dbSlide.statusChanged = DateTime.Now;
            }

            db.SubmitChanges();
        }

        public DrawnRegion RegionBeingDrawn
        {
            get { return regionBeingDrawn; }
        }

        public void StartDrawingBox(Point mousePos)
        {
            regionBeingDrawn = new DrawnRegion(mousePos)
            {
                Modified = DateTime.Now,
                Owner = Environment.UserName,
                Comment = string.Empty
            };

            Notify("RegionBeingDrawn");
        }

        public void ContinueDrawingBox(Point mousePos)
        {
            if(regionBeingDrawn != null)
                regionBeingDrawn.MoveBottomRightTo(mousePos);
        }

        public void FinishDrawing()
        {
            if (regionBeingDrawn == null)
                return;

            if (!regionBeingDrawn.IsTooBig)
            {
                regions.Add(regionBeingDrawn);
            }

            SelectRegion(regionBeingDrawn);
            regionBeingDrawn = null;
            Notify("RegionBeingDrawn");
        }

        public void DeleteRegion(DrawnRegion drawnRegion)
        {
            regions.Remove(drawnRegion);
            Notify("SelectedRegion");
        }

        public void SelectRegion(DrawnRegion oneToSelect)
        {
            foreach (var d in regions)
            {
                d.IsSelected = d == oneToSelect;
            }

            Notify("SelectedRegion");
        }

        public DrawnRegion SelectedRegion
        {
            get { return regions.Where(r => r.IsSelected).FirstOrDefault(); }
        }
    }
}
