﻿using System;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace AI
{
    public class ProbeScore : Notifier
    {
        public Guid Id { get; set; }
        public ImageFrame Frame { get; set; }

        private string className;
        public string Class { get { return className; } set { className = value; Notify("Class"); } }

        private Color color;
        public Color Color { get { return color; } set { color = value; Notify("Color"); } }
        
        public double X { get; set; }
        public double Y { get; set; }
        
        public string SpotMeasurements { get; set; }

        private ImageSource thumb;
        public ImageSource Thumbnail { get { return thumb; } set { thumb = value; Notify("Thumbnail"); } }
    }
}
