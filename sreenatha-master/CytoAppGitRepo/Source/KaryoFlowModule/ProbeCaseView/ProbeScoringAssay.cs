﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Windows.Input;
using System.Xml.Linq;
using System.Linq;
using System.Windows.Media;

namespace AI
{
    public class ProbeScoringClass : Notifier
    {
        private string text;
        private Color color;

        public ProbeScoringClass()
        {
            ShortcutKey = Key.Back;
            Text = string.Empty;
            color = RandomColor();
        }

        private static int nextColor = 0;
        private static Color[] colors = new Color[]
        {
            Colors.Red,
            Colors.Green,
            Colors.Blue,
            Colors.Cyan,
            Colors.Magenta,
            Colors.Yellow,
            Colors.Goldenrod,
            Colors.LimeGreen,
            Colors.Beige,
            Colors.Plum
        };
        
        public static Color RandomColor()
        {
            return colors[nextColor++ % 10];
        }

        public Color Color
        {
            get { return color; }
            set { color = value; Notify("Color"); }
        }

        public Key ShortcutKey
        {
            get;
            set;
        }

        public string Text
        {
            get { return text; }
            set { text = value; Notify("Text"); }
        }
    }


    public class ProbeScoringAssay : Notifier
    {
        private ObservableCollection<ProbeScoringClass> classes;
        private string name;
        private Boolean duplicateClassNames = false;

        public static RoutedUICommand AcceptAssay = new RoutedUICommand("Accept Assay Classes", "AcceptAssayClasses", typeof(ProbeScoringAssay));

        public ProbeScoringAssay()
        {
            classes = new ObservableCollection<ProbeScoringClass>();
            name = "Unnamed";
        }

        public string Name
        {
            get { return name; }
            set { name = value; Notify("Name"); }
        }

        public Guid Id
        {
            get;
            set;
        }

        public IEnumerable<ProbeScoringClass> Classes
        {
            get { return classes; }
            set 
            {
                classes = new ObservableCollection<ProbeScoringClass>(value);

                foreach (ProbeScoringClass p in classes)
                {
                    p.PropertyChanged += newClass_PropertyChanged;
                }
            }
        }

        public Boolean DuplicateClassName
        {
            get { return duplicateClassNames; }

            internal set
            {
                if (duplicateClassNames != value)
                {
                    duplicateClassNames = value;
                    Notify("DuplicateClassName");
                }
            }
        }

        private void CheckDuplicates()
        {

            Boolean duplicates = false;

            foreach (ProbeScoringClass ass in classes)
            {
                int count = 0;
                foreach (ProbeScoringClass a in classes)
                {
                    if (ass.Text == a.Text)
                    {
                        count++;
                    }
                }

                if (count > 1)
                {
                    duplicates = true;
                    break;
                }
            }

            DuplicateClassName = duplicates;
                    
        }


        
        public void AddClass(ProbeScoringClass newClass)
        {
            for (int i = 0; i < classes.Count; ++i)
            {
                if (newClass.ShortcutKey < classes[i].ShortcutKey)
                {
                    classes.Insert(i, newClass);
                    CheckDuplicates();
                    return;
                }
            }

            newClass.PropertyChanged += new PropertyChangedEventHandler(newClass_PropertyChanged);

            classes.Add(newClass);

            CheckDuplicates();
        }

        void newClass_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Text":
                    CheckDuplicates();
                    break;

            }
        }


        public void Save(Connect.Database db)
        {
            var assay = db.ProbeAssays.Where(a => a.assayId == Id).FirstOrDefault();
            if (assay == null)
            {
                assay = new AI.Connect.ProbeAssay()
                {
                    assayId = Guid.NewGuid()
                };

                db.ProbeAssays.InsertOnSubmit(assay);
            }

            assay.name = Name;
            assay.assay = ToXml();
        }

        public XElement ToXml()
        {
            var xml = new XElement("Assay", from c in classes
                                            select new XElement("Class",
                                                    new XAttribute("Color", ColorParse.ToHexString(c.Color)),
                                                    new XAttribute("Text", c.Text),
                                                    new XAttribute("Shortcut", c.ShortcutKey.ToString())));

            xml.Add(new XAttribute("Name", Name));

            return xml;
        }

        public static IEnumerable<ProbeScoringClass> ClassesFromXml(XElement assayNode)
        {
            return from c in assayNode.Elements("Class")
                   select new ProbeScoringClass
                   {
                       Color = ColorParse.HexStringToColor(c.Attribute("Color").Value),
                       ShortcutKey = (Key)Enum.Parse(typeof(Key), c.Attribute("Shortcut").Value),
                       Text = c.Attribute("Text").Value
                   };
        }

        public Key NextAvailableShortcut()
        {
            int key = (int)Key.F1;
            while (true)
            {
                if (classes.Where(c => c.ShortcutKey == (Key)key).Count() == 0)
                {
                    return (Key)key;
                }
                else
                {
                    ++key;
                }
            }
        }

        public void DeleteClass(ProbeScoringClass probeScoringClass)
        {
            classes.Remove(probeScoringClass);
            probeScoringClass.PropertyChanged -= newClass_PropertyChanged;

            CheckDuplicates();
        }

        public ProbeScoringAssay Duplicate(Boolean withCopyAppended)
        {
            return new ProbeScoringAssay
            {
                Classes = new ObservableCollection<ProbeScoringClass>(classes),
                Name = ((withCopyAppended == true) ? name + " (copy)" : name)
            };
        }

        public ProbeScoringAssay Duplicate()
        {
            return Duplicate(true);
        }


        internal string NextAvailableName()
        {
            int numUnnamedClasses = classes.Where(c => c.Text.StartsWith("Unnamed Class")).Count();
            if (numUnnamedClasses == 0)
                return "Unnamed Class";
            else
            {
                string tryThis = "Unnamed Class (" + numUnnamedClasses.ToString() + ")";
                while (classes.Where(c => c.Text == tryThis).Count() > 0)
                {
                    numUnnamedClasses++;
                    tryThis = "Unnamed Class (" + numUnnamedClasses.ToString() + ")";
                }
                return tryThis;
            }
        }
    }
}
