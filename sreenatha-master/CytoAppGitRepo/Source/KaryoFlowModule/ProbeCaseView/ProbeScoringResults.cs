﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Linq;

namespace AI
{
    public class ClassTotal : INotifyPropertyChanged
    {
        private string scoringClass;
        private int count;
        private double percentage;

        public ClassTotal(ProbeScore score)
        {
            this.scoringClass = score.Class;
            Color = score.Color;
        }

        public string ScoringClass
        {
            get { return scoringClass; }
        }

        public int Count
        { 
            get { return count; }
            set { count = value; Notify("Count"); } 
        }

        public double Percentage 
        {
            get { return percentage; }
            set { percentage = value; Notify("Percentage"); } 
        }

        public Color Color
        {
            get;
            set;
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }

    public static class ProbeScoringResultsGenerator
    {
        public static ProbeScoringResults GenerateResultsForSlide(IEnumerable<ProbeScore> allProbeScores)
        {
            List<ClassTotal> scoredClassTotals = new List<ClassTotal>();

            foreach (ProbeScore score in allProbeScores)
            {
                AddScore(score, scoredClassTotals);
            }

            scoredClassTotals.Sort((a, b) => a.Count.CompareTo(b.Count));

            return new ProbeScoringResults(scoredClassTotals);
        }

        private static void AddScore(ProbeScore score, List<ClassTotal> scoredClassTotals)
        {
            bool found = false;
            foreach (ClassTotal total in scoredClassTotals)
            {
                if (total.ScoringClass == score.Class)
                {
                    found = true;
                    total.Count++;
                }
            }

            if (!found)
            {
                ClassTotal t = new ClassTotal(score);
                t.Count = 1;
                scoredClassTotals.Add(t);
            }
        }
    }

    public class ProbeScoringResults
    {
        private IEnumerable<ClassTotal> scoredClassTotals;
        private int totalScoredCells;

        public ProbeScoringResults(IEnumerable<ClassTotal> scoredClassTotals)
        {
            var sortedScores = scoredClassTotals.ToList();
            sortedScores.Sort((a, b) => a.ScoringClass.CompareTo(b.ScoringClass));

            this.scoredClassTotals = sortedScores;

            CalculateTotalScoredCells();
            CalculatePercentages();
        }

        public IEnumerable<ClassTotal> ScoredClassTotals
        {
            get { return scoredClassTotals; }
        }

        public int TotalScoredCells
        {
            get { return totalScoredCells; }
        }

        public bool HasResults
        {
            get { return totalScoredCells > 0; }
        }

        private void CalculatePercentages()
        {
            foreach (ClassTotal total in scoredClassTotals)
            {
                total.Percentage = (total.Count / (double)totalScoredCells) * 100;
            }
        }

        private void CalculateTotalScoredCells()
        {
            totalScoredCells = 0;
            foreach (ClassTotal t in scoredClassTotals)
            {
                totalScoredCells += t.Count;
            }
        }
    }
}
