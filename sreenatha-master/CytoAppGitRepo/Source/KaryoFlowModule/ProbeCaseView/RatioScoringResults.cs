﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Windows.Media;

namespace AI
{
    public class RatioScoringResults : INotifyPropertyChanged
    {
        private ObservableCollection<CountedCell> countedCells = new ObservableCollection<CountedCell>();

        public void Add(CountedCell cell)
        {
            cell.PropertyChanged += new PropertyChangedEventHandler(cell_PropertyChanged);
            countedCells.Add(cell);
            NotifyAllPropertiesChanged();
        }

        public void Remove(CountedCell countedCell)
        {
            countedCells.Remove(countedCell);
            NotifyAllPropertiesChanged();
        }

        internal void Clear()
        {
            countedCells.Clear();
            NotifyAllPropertiesChanged();
        }

        private void NotifyAllPropertiesChanged()
        {
            Notify("TotalCountedCells");
            Notify("TotalCountOfReds");
            Notify("TotalCountOfGreens");
            Notify("RatioRedToGreen");
            Notify("Bin1Count");
            Notify("Bin2Count");
            Notify("Bin3Count");
            Notify("Bin1HeightFraction");
            Notify("Bin2HeightFraction");
            Notify("Bin3HeightFraction");
        }

        private void cell_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            NotifyAllPropertiesChanged();
        }

        public int TotalCountedCells
        {
            get { return countedCells.Count; }
        }

        public int TotalCountOfReds
        {
            get { return CountAllRedSpots(); }
        }

        public double RatioRedToGreen
        {
            get
            {
                if (TotalCountOfGreens == 0)
                    return 0;

                return (double)TotalCountOfReds / (double)TotalCountOfGreens;
            }
        }

        public int TotalCountOfGreens
        {
            get
            {
                int countgreen = 0;
                foreach (CountedCell cc in countedCells)
                {
                    countgreen = countgreen + cc.GreenCount;
                }
                return countgreen;
            }
        }

        private int CountAllRedSpots()
        {
            int countred = 0;
            foreach (CountedCell cc in countedCells)
            {
                countred = countred + cc.RedCount;
            }
            return countred;
        }

        public int NumberOfCountedCells
        {
            get { return countedCells.Count; }
        }

        public IEnumerable<CountedCell> CountedCells
        {
            get { return countedCells; }
        }

        public int Bin1Count
        {
            get
            {
                int result = 0;
                foreach (CountedCell countedCell in countedCells)
                {
                    double ratio = (double)countedCell.RedCount / (double)countedCell.GreenCount;
                    if (ratio < 1.8)
                        result++;
                }
                return result;
            }
        }

        public int Bin2Count
        {
            get
            {
                int result = 0;
                foreach (CountedCell countedCell in countedCells)
                {
                    double ratio = (double)countedCell.RedCount / (double)countedCell.GreenCount;
                    if (ratio >= 1.8 && ratio <= 2.2)
                        result++;
                }
                return result;
            }
        }

        public int Bin3Count
        {
            get
            {
                int result = 0;
                foreach (CountedCell countedCell in countedCells)
                {
                    double ratio = (double)countedCell.RedCount / (double)countedCell.GreenCount;
                    if (ratio > 2.2)
                        result++;
                }
                return result;
            }
        }

        private int MaxBinCount()
        {
            int maxHeight = Math.Max(Bin1Count, Bin2Count);
            maxHeight = Math.Max(maxHeight, Bin3Count);
            return maxHeight;
        }

        private double GetHeightFractionForBinCount(int binCount)
        {
            // since this generates numbers used for display only, return a minimum of 0.1 so that we dont get empty displays...
            int maxHeight = MaxBinCount();
            if (maxHeight == 0)
                return 0.1;
            return Math.Max(0.1, (double)binCount / (double)maxHeight);
        }

        public double Bin1HeightFraction
        {
            get { return GetHeightFractionForBinCount(Bin1Count); }
        }

        public double Bin2HeightFraction
        {
            get { return GetHeightFractionForBinCount(Bin2Count); }
        }

        public double Bin3HeightFraction
        {
            get { return GetHeightFractionForBinCount(Bin3Count); }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}