﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using System.Threading;
using AI.Security;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Ink;
using System.Collections;

namespace AI
{
    public class RatioScoringTool : INotifyPropertyChanged, IScorer
    {
        public class SlideInfo
        {
            public string Name { get; set; }
            public Guid Id { get; set; }
        }

        public static RoutedUICommand ToggleScores = new RoutedUICommand("Toggle scores", "ToggleScores", typeof(ScoringTool));
        public static RoutedUICommand ToggleAnnotations = new RoutedUICommand("Toggle annotations", "ToggleAnnotations", typeof(ScoringTool));
        public static RoutedUICommand ClearScoresFromFrame = new RoutedUICommand("Clear frame scores", "ClearScoresFromFrame", typeof(ScoringTool));
        public static RoutedUICommand ClearScoresFromSlide = new RoutedUICommand("Clear slide scores", "ClearScoresFromSlide", typeof(ScoringTool));
        public static RoutedUICommand NextFrame = new RoutedUICommand("Next Frame", "NextFrame", typeof(ScoringTool));
        public static RoutedUICommand PrevFrame = new RoutedUICommand("Previous Frame", "PrevFrame", typeof(ScoringTool));

		public delegate void CurrentFrameChangingEventHandler(object sender, CurrentFrameChangingEventArgs e);

		public event CurrentFrameChangingEventHandler CurrentFrameChanging;
        public event EventHandler CurrentFrameChanged;

        private CountedCell currentCountCell;
        private List<CountedCell> counts;
        private List<Guid> framesTouchedInOtherSessions;
        private List<CountedCell> potentialOrphans;
        private RatioScoringResults results;
        private Connect.BlobTrickler trickler;
        
        private bool showScores;
        private double scoreSize = 150;
        private bool showAnnotations = true;
        private ObservableCollection<Annotation> annotations;
        private List<ImageFrame> frames;
        private ImageFrame currentFrame;
        private Guid caseId;
        private string caseName;
        private List<SlideInfo> slides;
        private SlideInfo currentSlide;

        private InterphaseScoreSession currentScoringSession;

        public RatioScoringTool(Guid caseId, InterphaseScoreSession scoreSession)
        {
            Guid slideId = scoreSession.SlideId;
            currentScoringSession = scoreSession;

            using(var db = new Connect.Database())
            using(var lims = Connect.LIMS.New())
            {
                this.caseId = caseId;

                var allslides = db.Slides.Where(s => s.caseId == caseId);
                slides = new List<SlideInfo>();
                foreach (var s in allslides)
                {
                    var ls = lims.GetSlide(s.limsRef);
                    slides.Add(new SlideInfo
                                {
                                    Id = s.slideId,
                                    Name = ls.Name
                                });
                }

                this.currentSlide = slides.First();
                frames = (from f in db.ImageFrames
                          where f.slideId == currentSlide.Id
                          select ImageFrameDbLoad.FromDb(db, f, caseId)).ToList();

                annotations = new ObservableCollection<Annotation>();
                counts = new List<CountedCell>();
                framesTouchedInOtherSessions = new List<Guid>();
                results = new RatioScoringResults();
                trickler = new Connect.BlobTrickler();

                LoadSettings(db);
                LoadScoringData(db);
                LoadImageFrameStars(db);
                LoadScoringDataFromOtherSessions(db);
                LoadThumbs(Dispatcher.CurrentDispatcher);

                potentialOrphans = counts.ToList();
                CurrentFrame = frames.FirstOrDefault();

                var dbc = db.Cases.Where(c => c.caseId == caseId).First();
                var lc = lims.GetCase(dbc.limsRef);
                caseName = lc.Name;
            }
        }

        public RatioScoringResults Results
        {
            get { return results; }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        void Notify(string s)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(s));
        }

        #endregion

        public ImageFrame CurrentFrame
        {
            get { return currentFrame; }
            set
            {
                if (value != null)
                {
					CurrentFrameChangingEventArgs args = new CurrentFrameChangingEventArgs(currentFrame, value);
					if (CurrentFrameChanging != null)
						CurrentFrameChanging(this, args);

					if (args.Cancel == false)
					{
						currentFrame = value;
						if (CurrentFrameChanged != null)
							CurrentFrameChanged(this, EventArgs.Empty);

						if (currentFrame.Thumbnail == null)
							currentFrame.Thumbnail = Load.LoadThumbnail(currentFrame.Id, CaseId);

						// CHECK THESE...
						Notify("CurrentFrame");
						Notify("CountedCellsForCurrentFrame");
                        Notify("CountedCellsForCurrentSlide");
						Notify("IndexOfCurrentFrame");
						Notify("FrameScoredInOtherSession");
					}
                }
            }
        }

        public IEnumerable<ImageFrame> Frames
        {
            get { return frames; }
        }

        public SlideInfo Slide
        {
            get { return currentSlide; }
        }

        public string CaseName { get { return caseName; } }

        public IEnumerable<Annotation> Annotations
        {
            get { return annotations; }
        }

        public bool ShowAllCounts
        {
            get { return showScores; }
            set { showScores = value; Notify("ShowAllCounts"); }
        }

        public bool ShowAnnotations
        {
            get { return showAnnotations; }
            set { showAnnotations = value; Notify("ShowAnnotations"); }
        }

        public double ScoreSize
        {
            get { return scoreSize; }
            set { scoreSize = value; Notify("ScoreSize"); Notify("ScoreSizeUiOffset"); }
        }

        public double ScoreSizeUiOffset
        {
            get { return ScoreSize / -2; }
        }

        public IEnumerable<CountedCell> CountedCellsForFrame(ImageFrame frame)
        {
            return counts.Where(s => s.Frame == frame);
        }

        public IEnumerable<CountedCell> AllCountedCells
        {
            get { return counts; }
        }

        public IEnumerable<Guid> AllFramesTouchedInOtherSessions
        {
            get { return framesTouchedInOtherSessions; }
        }

		public bool FrameScoredInOtherSession
		{
			get
			{
				return framesTouchedInOtherSessions.Where(f => f == currentFrame.Id).Count() > 0;
			}
		}

        public IEnumerable<CountedCell> CountedCellsForCurrentFrame
        {
            get
            {
                if (currentFrame == null)
                    return new CountedCell[0];

                return CountedCellsForFrame(currentFrame);
            }
        }

        public IEnumerable<CountedCell> CountedCellsForCurrentSlide
        {
            get { return counts; }
        }

        #region adding counted stuff
        
        public void AddCountedCell(Point center, ImageFrameRenderer image)
        {
            if (CurrentCountCell != null && !CurrentCountCell.IsValid)
            {
                RemoveCountedCell(CurrentCountCell);
            }

            CountedCell newCell = new CountedCell(new CellScore(0, 0));
            newCell.X = center.X;
            newCell.Y = center.Y;
            newCell.Frame = currentFrame;
            newCell.Id = Guid.NewGuid();
            ScoreThumbnailGenerator.EnqueueProbeScoreThumb(trickler, caseId, newCell.Id, ScoreThumbnailGenerator.CutThumb(center, image));

            counts.Add(newCell);
            results.Add(newCell);
            CurrentCountCell = newCell;
            isModified = true;

            // need others?
            Notify("CountedCellsForCurrentFrame");
            Notify("CountedCellsForCurrentSlide");
        }

        internal void RemoveCountedCell(CountedCell countedCell)
        {
            counts.Remove(countedCell);
            results.Remove(countedCell);
            potentialOrphans.Add(countedCell);

            if (CurrentCountCell == countedCell)
                CurrentCountCell = null;

            isModified = true;

            // need others?
            Notify("CountedCellsForCurrentFrame");
            Notify("CountedCellsForCurrentSlide");
        }

        internal void RemoveCountedCellAt(Point mousePosition)
        {
            // the last one in the list was the last one added, so use lastmost overlapping cell
            CountedCell last = counts.Where(c => c.HitTestContainsPoint(mousePosition)).LastOrDefault();
            if (last != null)
                RemoveCountedCell(last);
        }

        public void AppendToCountString(char ch)
        {
            if (CurrentCountCell != null)
                CurrentCountCell.AppendToCountString(ch);
        }

        public void AppendBackspaceToCountString()
        {
            if (CurrentCountCell != null)
                CurrentCountCell.AppendBackspaceToCountString();
        }

        public void AbortCurrentCount()
        {
            if (CurrentCountCell != null)
                RemoveCountedCell(CurrentCountCell);
        }

        public CountedCell CurrentCountCell
        {
            get { return currentCountCell; }
            set { currentCountCell = value; Notify("CurrentCountCell"); }
        }

        #endregion        

        #region Scoring Tool Serialisation

        private void LoadScoringData(Connect.Database db)
        {
            this.counts = new List<CountedCell>();
            var session = db.ProbeScoreSessions.Where(s => s.sessionId == currentScoringSession.Id).FirstOrDefault();

            if (session == null)
            {
                return;
            }

            var rawCounts = CountedCellsForSession(session.sessionId, db);
            this.counts = rawCounts.ToList();

            foreach (CountedCell cell in counts)
                results.Add(cell);
        }

        private List<CountedCell> CountedCellsForSession(Guid sessionId, Connect.Database db)
        {
            var countedCells = (from s in db.ProbeScores
                                where s.sessionId == sessionId
                                select new
                                {
                                    CountedCell = CountedCellFromDatabase(s),
                                    FrameId = s.frameId
                                }).ToArray();

            foreach (var countedCell in countedCells)
            {
                countedCell.CountedCell.Frame = frames.Where(f => f.Id == countedCell.FrameId).FirstOrDefault();
            }

            var rawCountedCells = from c in countedCells select c.CountedCell;
            return rawCountedCells.ToList();
        }

        public static CountedCell CountedCellFromDatabase(AI.Connect.ProbeScore s)
        {
            CountedCell cell = new CountedCell(new CellScore(0, 0));
            cell.Id = s.scoreId;
            cell.X = s.x;
            cell.Y = s.y;
            string content = s.content;
            foreach (char c in content)
                cell.AppendToCountString(c);
            return cell;
        }

        private void LoadScoringDataFromOtherSessions(AI.Connect.Database db)
        {
            framesTouchedInOtherSessions.Clear();

            var sessionIds = (from s in db.ProbeScoreSessions
                              where s.slideId == currentSlide.Id && s.sessionId != currentScoringSession.Id
                              select s.sessionId).ToList();

            foreach (var id in sessionIds)
            {
                var someCounts = CountedCellsForSession(id, db);
                foreach (CountedCell cell in someCounts)
                    framesTouchedInOtherSessions.Add(cell.Frame.Id);
            }
        }

        private void LoadImageFrameStars(AI.Connect.Database db)
        {
            var stars = (from s in db.ImageFrameStars
                         where s.sessionId == currentScoringSession.Id
                         select s).ToList();

            foreach (var star in stars)
            {
                ImageFrame frame = frames.Where(f => f.Id == star.imageFrameId).FirstOrDefault();
                if (frame != null)
                    frame.Starred = true;
            }
        }

        public void SaveData()
        {
            SaveSettings();
            SaveScores();
            SaveImageFrameStars();
            isModified = false;
        }

        private void SaveImageFrameStars()
        {
            var db = new Connect.Database();

            db.ImageFrameStars.DeleteAllOnSubmit(db.ImageFrameStars.Where(star => star.sessionId == currentScoringSession.Id));
            db.SubmitChanges();

            foreach (var frame in frames)
            {
                if (frame.Starred)
                {
                    db.ImageFrameStars.InsertOnSubmit(new AI.Connect.ImageFrameStar()
                    {
                        starId = Guid.NewGuid(),
                        sessionId = currentScoringSession.Id,
                        imageFrameId = frame.Id
                    });
                } 
            }

            db.SubmitChanges();
        }

        private void SaveScores()
        {
            var db = new Connect.Database();

			try
			{
				db.ProbeScoreSessions.DeleteAllOnSubmit(db.ProbeScoreSessions.Where(s => s.sessionId == currentScoringSession.Id));
				db.SubmitChanges();

				var xml = new XElement("Assay");
				xml.Add(new XAttribute("Name", "Empty"));

				var session = new Connect.ProbeScoreSession
				{
					assay = xml,
					ownerUser = currentScoringSession.Owner,
					slideId = this.currentSlide.Id,
					sessionId = Guid.NewGuid(),
					type = 1,
					modified = DateTime.Now
				};

				db.ProbeScoreSessions.InsertOnSubmit(session);

				db.ProbeScores.InsertAllOnSubmit(from s in counts
												 select new Connect.ProbeScore
												 {
													 color = ColorParse.ToHexString(Colors.Red),
													 content = s.CountString,
													 frameId = s.Frame.Id,
													 measurements = "",
													 scoreId = s.Id,
													 sessionId = session.sessionId,
													 x = s.X,
													 y = s.Y
												 });

				db.SubmitChanges();

				var orphanScores = potentialOrphans.Where(s => !counts.Contains(s));
				foreach (var orphan in orphanScores)
				{
					var path = orphan.Id.ToString() + ScoredCell.ScoreThumbExtension;
					Connect.BlobStream.Delete(caseId, path, db);
				}

				db.SubmitChanges();

				// TODO : Need to keep currentScoringSession here but what do we do about InterphaseSlide.ScoringSessions
				// in InterphaseWorkflow.xaml.cs which has a session collection which is now out of date!
				currentScoringSession = new InterphaseScoreSession()
				{
					Id = session.sessionId,
					Owner = session.ownerUser,
					SlideId = session.slideId,
					Assay = session.assay.Attribute("Name").Value,
					Type = (ScoreSessionType)session.type,
					Modified = session.modified
				};
			}
			finally
			{
				db.Dispose();
			}
        }

        private void LoadSettings(Connect.Database db)
        {
            FileInfo settingsFile = AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\ratio_scoring.settings");
            settingsFile.Refresh();
            if (settingsFile.Exists)
            {
                XElement xml = XElement.Load(settingsFile.FullName);
                ShowAllCounts = Parsing.TryParseBool(Parsing.ValueOrDefault(xml.Attribute("ShowScores"), "False"), false);
                ScoreSize = Parsing.TryParseDouble(Parsing.ValueOrDefault(xml.Attribute("ScoreCircleSize"), "100"), 100);
                ShowAnnotations = Parsing.TryParseBool(Parsing.ValueOrDefault(xml.Attribute("ShowAnnotations"), "False"), false);
            }
        }

        private void SaveSettings()
        {
            XElement xml = new XElement("RatioScoringTool",
                new XAttribute("ShowScores", ShowAllCounts),
                new XAttribute("ScoreCircleSize", ScoreSize),
                new XAttribute("ShowAnnotations", ShowAnnotations));

            FileInfo settingsFile = AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\ratio_scoring.settings");
            settingsFile.Refresh();
            if (settingsFile.Exists)
                settingsFile.Delete();

            xml.Save(settingsFile.FullName);
        }

        #endregion

        public void DeleteCountedCellsForFrame(ImageFrame frame)
        {
            for (int i = counts.Count - 1; i >= 0; --i)
            {
                if (counts[i].Frame == frame)
                {
                    CountedCell cell = counts[i];
                    counts.RemoveAt(i);
                    results.Remove(cell);
                    potentialOrphans.Add(cell);
                    if (cell == currentCountCell)
                        CurrentCountCell = null;
                }
            }
            Notify("CountedCellsForCurrentFrame");
            Notify("CountedCellsForCurrentSlide");
        }

        public void DeleteCountedCellsForSlide()
        {
            counts.Clear();
            results.Clear();
            Notify("CountedCellsForCurrentFrame");
            Notify("CountedCellsForCurrentSlide");
        }

        public int IndexOfCurrentFrame
        {
            get { return frames.IndexOf(currentFrame) + 1; }
        }

        public bool CanNextFrameExecute
        {
            get { return currentFrame != null && frames.IndexOf(currentFrame) < frames.Count - 1; }
        }

        public bool CanPrevFrameExecute
        {
            get { return currentFrame != null && frames.IndexOf(currentFrame) > 0; }
        }

        public void MoveToPrevFrame()
        {
            CurrentFrame = frames[frames.IndexOf(currentFrame) - 1];
        }

        public void MoveToNextFrame()
        {
            CurrentFrame = frames[frames.IndexOf(currentFrame) + 1];
        }

        private bool isModified = false;
        public bool IsModified
        {
            get { return isModified; }
        }

        public void LoadThumbs(Dispatcher dispatcher)
        {
            var db = new Connect.Database();
            var notifyList = new List<ImageFrame>();

            foreach (var frame in frames)
            {
                try
                {
                    string path = frame.Id.ToString() + ".jpg";
                    var bitmap = ImageFromBitmapSource.LoadBitmapImageFromStream(caseId, path, db, Connect.CacheOption.DontCache, 100);
                    frame.SetThumbNoNotify(bitmap);

                    notifyList.Add(frame);

                    if (notifyList.Count == 10)
                    {
                        var list = notifyList;
                        notifyList = new List<ImageFrame>();

                        dispatcher.BeginInvoke((ThreadStart)delegate
                        {
                            foreach (var c in list)
                            {
                                c.NotifyImages();
                            }
                        }, DispatcherPriority.Normal);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("EXCEPTION: LoadThumbs, Cell: {0}", frame.Id);
                    Console.WriteLine(frame.ToString());
                    Console.WriteLine(e.ToString());
                }
            }

            dispatcher.Invoke((ThreadStart)delegate
            {
                foreach (var c in notifyList)
                {
                    c.NotifyImages();
                }
            }, DispatcherPriority.Normal);
        }

        public Guid CaseId
        {
            get { return caseId; }
        }

        public IEnumerable<IScorerAugmentedFrame> FramesForSelectionWindow
        {
            get
            {
                return from f in frames
                       select new IScorerAugmentedFrame
                       {
                           Frame = f,
                           AllFramesTouchedInOtherSessions = AllFramesTouchedInOtherSessions,
                           AllScores = AllCountedCells
                       };
            }
        }
    }

    public class CountedCell : INotifyPropertyChanged
    {
        private Point position;
        private int redCount;
        private int greenCount;
        private string countString;
        
        public CountedCell(CellScore cellScore)
        {
            redCount = cellScore.RedCount;
            greenCount = cellScore.GreenCount;
            countString = "";
        }

        public ImageFrame Frame { get; set; }
        public Guid Id { get; set; }
        public BitmapSource Thumb { get; set; }

        public int RedCount
        {
            get { return redCount; }
            set { redCount = value; Notify("RedCount"); Notify("IsValid"); }
        }

        public int GreenCount
        {
            get { return greenCount; }
            set { greenCount = value; Notify("GreenCount"); Notify("IsValid"); }
        }

        public double X
        {
            get { return position.X; }
            set { position.X = value; Notify("X"); }
        }

        public double Y
        {
            get { return position.Y; }
            set { position.Y = value; Notify("Y"); }
        }

        public bool IsValid
        {
            get { return redCount > 0 || greenCount > 0; }
        }

        public string CountString
        {
            get { return countString; }
        }

        public bool HitTestContainsPoint(Point pt)
        {
            double dx = X - pt.X;
            double dy = Y - pt.Y;
            return (80 * 80) > (dx * dx) + (dy * dy);
        }

        public void AppendToCountString(char ch)
        {
            if (ch == '\t')
                ch = '\n';

            // ignore successive separators
            if (ch == '\n' && countString.Length > 0 && countString[countString.Length - 1] == '\n')
            {
                return;
            }

            countString += ch;

            ParseCountString();
        }

        private void ParseCountString()
        {
            int red = 0;
            int green = 0;

            if (countString.Length == 0 || CountStringParser.Parse(countString, out red, out green))
            {
                RedCount = red;
                GreenCount = green;
            }
        }

        public void AppendBackspaceToCountString()
        {
            if (countString.Length > 0 && char.IsWhiteSpace(countString[countString.Length - 1]))
            {
                while (countString.Length > 0 && char.IsWhiteSpace(countString[countString.Length - 1]))
                {
                    countString = countString.Substring(0, countString.Length - 1);
                }
            }

            if (countString.Length > 0)
            {
                countString = countString.Substring(0, countString.Length - 1);
            }

            ParseCountString();
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }

    public class CellScore
    {
        private int redCount;
        private int greenCount;

        public CellScore(int redCount, int greenCount)
        {
            this.redCount = redCount;
            this.greenCount = greenCount;
        }

        public int RedCount
        {
            get { return redCount; }
            set { redCount = value; }
        }

        public int GreenCount
        {
            get { return greenCount; }
            set { greenCount = value; }
        }
    }

    public static class CountStringParser
    {
        public static bool Parse(string input, out int redCount, out int greenCount)
        {
            redCount = 0;
            greenCount = 0;

            if (input == null || input.Length == 0)
            {
                return false;
            }

            input = input.TrimStart();
            input = input.Replace(" ", "\n");
            input = input.Replace("\t", "\n");

            while (input.IndexOf("\n\n") >= 0)
                input = input.Replace("\n\n", "\n");

            redCount = 0;
            greenCount = 0;

            string[] realBits = input.Split('\n');

            if (realBits.Length == 0)
                return false;

            int red = 0;
            if (!Int32.TryParse(realBits[0], out red))
                return false;

            int green = 0;
            if (realBits.Length > 1)
            {
                if (realBits[1].Length == 0) // consider string: 33[enter]. you want 33,0
                {
                    green = 0;
                }
                else
                {
                    if (!Int32.TryParse(realBits[1], out green))
                        return false;
                }
            }

            redCount = red;
            greenCount = green;

            return true;
        }
    }
}