﻿using System;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Globalization;
using System.Windows.Data;
using System.Threading;
using System.ComponentModel;

namespace AI
{
    public partial class RatioScoringToolUi : UserControl, INotifyPropertyChanged, IClosing
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private RatioScoringTool controller;
        private IEnumerable<SlidingDrawer> popups;
        private string caseStatus = string.Empty;
        private bool isMouseButtonDown = false;
        private bool firstTime = true;

        public RatioScoringToolUi(RatioScoringTool tool)
        {
            this.controller = tool;
            DataContext = tool;

            InitializeComponent();

            frameImage.AvailableFilters = FilterTypes.FluorescentTypes;
            frameImage.HistogramEstimator = new InterphaseHistogramEstimator();

            popups = new SlidingDrawer[]
            {
                channelPopup,
                resultsPopup,
                annotationsPopup
            };

            annotationsDisplay.ParentId = controller.CurrentFrame.Id;

            ThreadPool.QueueUserWorkItem(delegate
            {
                tool.LoadThumbs(Dispatcher);
            });


            Loaded += (s, e) =>
            {
                Keyboard.Focus(this);

                frameImage.Initialize();

                if (!firstTime)
                    frameImage.RefreshImage();

                firstTime = false;
                frameImage.Render();

				tool.CurrentFrameChanging += CurrentFrameChanging;
				tool.CurrentFrameChanged += CurrentFrameChanged;
            };

            Unloaded += (s, e) =>
            {
				tool.CurrentFrameChanging -= CurrentFrameChanging;
                tool.CurrentFrameChanged -= CurrentFrameChanged;
                frameImage.Release();
            };
        }

		private void CurrentFrameChanging(object sender, CurrentFrameChangingEventArgs e)
		{
			if (annotationsDisplay.IsModified)
			{
				e.Cancel = !SaveAnnotations();
			}
		}

        private void CurrentFrameChanged(object sender, EventArgs e)
        {
            annotationsDisplay.ParentId = controller.CurrentFrame.Id;
            PropertyChanged(this, new PropertyChangedEventArgs("Image"));
        }

        public ImageFrame Image
        {
            get { return controller.CurrentFrame; }
        }

        public FrameworkElement UiRoot
        {
            get { return this; }
        }

        public void MakeCurrent()
        {
            frameImage.Render();
        }

        private void ShowPopupOrClose(SlidingDrawer popup)
        {
            foreach (var p in popups)
            {
                if (p != popup && p.IsOpen)
                    p.IsOpen = false;
            }
        }

        private void ToggleChannelPopup(object sender, RoutedEventArgs e)
        {
            ShowPopupOrClose(channelPopup);
        }

        private void ToggleResultsPopup(object sender, RoutedEventArgs e)
        {
            ShowPopupOrClose(resultsPopup);
        }

        private void ToggleAnnotationPopup(object sender, RoutedEventArgs e)
        {
            ShowPopupOrClose(annotationsPopup);
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            if (controller.IsModified)
            {
                cancelMessage.IsOpen = true;
            }
            else
            {
                UIStack.For(this).Pop(UIStackResult.Cancel);
            }
        }

        private void OnCancelMessageOk(object sender, RoutedEventArgs e)
        {
            UIStack.For(this).Pop(UIStackResult.Cancel);
        }

        private void OnCancelMessageCancel(object sender, RoutedEventArgs e)
        {
            cancelMessage.IsOpen = false;
        }

        private void Grid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (isMouseButtonDown)
            {
                Point mousePosition = e.GetPosition(countsItemsControl);
                controller.AddCountedCell(new Point(mousePosition.X, mousePosition.Y), frameImage);
                isMouseButtonDown = false;
            }
            isMouseButtonDown = false;
        }

        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isMouseButtonDown = true;
        }

        private void Grid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            Point mousePosition = e.GetPosition(countsItemsControl);
            mousePosition = new Point(mousePosition.X, mousePosition.Y);
            controller.RemoveCountedCellAt(mousePosition);
        }

        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            if (controller == null)
                return;

            Point mousePos = Mouse.GetPosition(countsItemsControl);
            mousePos = new Point(mousePos.X, mousePos.Y);

            if (e.Key == Key.Return || e.Key == Key.Enter || e.Key == Key.Tab)
            {
                controller.AppendToCountString('\n');
                e.Handled = true;
            }
            else if (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
            {
                controller.AppendToCountString((char)('0' + (int)(e.Key - Key.NumPad0)));
                e.Handled = true;
            }
            else if (e.Key >= Key.D0 && e.Key <= Key.D9)
            {
                controller.AppendToCountString((char)('0' + (int)(e.Key - Key.D0)));
                e.Handled = true;
            }
            else if (e.Key == Key.Back)
            {
                controller.AppendBackspaceToCountString();
            }
            else if (e.Key == Key.Escape)
            {
                controller.AbortCurrentCount();
            }
        }

        private void OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                e.Handled = true;
                zStacker.Controller.MoveZ(Math.Sign(e.Delta));
            }
            else
            {
                e.Handled = false;
            }
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            Closed();
	        UIStack.For(this).Pop(UIStackResult.Ok);
        }

        private void OnAnnotationLeftMouseButtonDown(object sender, MouseButtonEventArgs e)
        {
            annotationsDisplay.PlacementHelper.StartPlacingAnnotation(sender, e, frameImage);

            if (e.Handled)
                return;

            Keyboard.Focus(this);
        }

        private void OnAnnotationMouseMove(object sender, MouseEventArgs e)
        {
            annotationsDisplay.PlacementHelper.MoveAnnotationBeingPlaced(sender, e, frameImage);

        }

        private void OnAnnotationLeftMouseButtonUp(object sender, MouseButtonEventArgs e)
        {
            annotationsDisplay.PlacementHelper.FinishPlacingAnnotation(sender, e, frameImage);
        }

        private void CanToggleScoresExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller != null;
        }

        private void ToggleScoresExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            controller.ShowAllCounts = !controller.ShowAllCounts;
        }

        private void CanClearScoresFromFrameExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller != null && controller.CountedCellsForCurrentFrame.Count() > 0;
        }

        private void ClearScoresFromFrameExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            controller.DeleteCountedCellsForFrame(controller.CurrentFrame);
        }

        private void CanClearScoresFromSlideExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller != null && controller.CountedCellsForCurrentSlide.Count() > 0;
        }

        private void ClearScoresFromSlideExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            controller.DeleteCountedCellsForSlide();
        }

        private void OnSetCircleSize(object sender, RoutedEventArgs e)
        {
            if (controller != null)
            {
                double size = (double)((FrameworkElement)sender).Tag;
                controller.ScoreSize = size;
            }
        }

        private void CanPrintExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller != null;
        }

        private void PrintExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                PrintDialog dlg = new PrintDialog();

                if ((bool)dlg.ShowDialog().GetValueOrDefault())
                {
                    double aspect = printVisual.ActualHeight / printVisual.ActualWidth;

                    RenderTargetBitmap render = new RenderTargetBitmap(
                       (int)(printVisual.ActualWidth),
                       (int)(printVisual.ActualHeight),
                       96, 96, PixelFormats.Pbgra32);
                    render.Render(printVisual);

                    CroppedBitmap bmp = new CroppedBitmap(render, new Int32Rect(0, 0, render.PixelWidth, render.PixelHeight));
                    Image img = new Image();
                    img.Source = bmp;

                    TextBlock text = new TextBlock();
                    text.FontSize = 18;
                    text.FontWeight = FontWeights.Bold;
                    text.Text = string.Format(CultureInfo.CurrentCulture, "{0}, {1}", controller.CaseName, controller.Slide.Name);
                    text.Margin = new Thickness(0, 0, 0, 20);

                    TextBlock dateText = new TextBlock();
                    dateText.FontSize = 18;
                    dateText.FontWeight = FontWeights.Bold;
                    dateText.Text = DateTime.Now.ToLongDateString();
                    dateText.Margin = new Thickness(0, 0, 0, 20);
                    DockPanel.SetDock(dateText, Dock.Right);


                    DockPanel topPanel = new DockPanel();
                    topPanel.Children.Add(dateText);
                    topPanel.Children.Add(text);
                    DockPanel.SetDock(topPanel, Dock.Top);

                    DockPanel dock = new DockPanel();
                    dock.Children.Add(topPanel);
                    dock.Children.Add(img);
                    dock.Margin = new Thickness(50);
                    dock.Width = dlg.PrintableAreaWidth - (dock.Margin.Left + dock.Margin.Right);
                    dock.Height = dock.Width * aspect;

                    dock.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                    dock.Arrange(new Rect(new Point(0, 0), dock.DesiredSize));

                    dlg.PrintVisual(dock, "Print");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Print failed", "Print", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void CanNextFrameExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller != null && controller.CanNextFrameExecute;
        }

        private void CanPrevFrameExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller != null && controller.CanPrevFrameExecute;
        }

        private void NextFrameExecute(object sender, ExecutedRoutedEventArgs e)
        {
			try
			{
				controller.MoveToNextFrame();
			}
			catch (Exception ex)
			{
				ExceptionUtility.ShowLog(ex, "Next Frame");
			}
        }

        private void PrevFrameExecute(object sender, ExecutedRoutedEventArgs e)
        {
			try
			{
				controller.MoveToPrevFrame();
			}
			catch (Exception ex)
			{
				ExceptionUtility.ShowLog(ex, "Previous Frame");
			}
        }

		#region IClosing Members

		public void Closed()
		{
			annotationsDisplay.Controller.Save(controller.CaseId);
			controller.SaveData();
		}

		#endregion

		private bool SaveAnnotations()
		{
			bool saveOk = false;

			try
			{
				annotationsDisplay.Controller.Save(controller.CaseId);
				saveOk = true;
			}
			catch (Exception ex)
			{
				ExceptionUtility.ShowLog(ex, "Save annotations error");
			}

			return saveOk;
		}

		private void CanToggleAnnotations(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = annotationsDisplay != null && annotationsDisplay.Controller != null;
		}

		private void ToggleAnnotationsExecuted(object sender, ExecutedRoutedEventArgs e)
		{
			annotationsDisplay.Controller.Show = !annotationsDisplay.Controller.Show;
		}

        private void OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            //  If control with focus is editable then dont process the keypress
            UIElement elementWithFocus = Keyboard.FocusedElement as UIElement;
            if (elementWithFocus != null)
            {
                string elementType = elementWithFocus.GetType().ToString();
                if ((elementType == "System.Windows.Controls.TextBox") || (elementType == "System.Windows.Controls.RichTextBox"))
                {
                    e.Handled = false;
                    return;
                }
            }

            if (e.Key == Key.Space)
            {
                e.Handled = true;
                OnShowFrameSelection(null, null);
                return;
            }

            e.Handled = false;
        }

        private void OnShowFrameSelection(object sender, RoutedEventArgs e)
        {
            FrameSelectionWindow w = new FrameSelectionWindow(controller, (DataTemplate)FindResource("frameOverlay"), controller.CurrentFrame);
            if (w.ShowDialog().GetValueOrDefault(false))
            {
                controller.CurrentFrame = w.SelectedFrame;
            }
        }
	}

    public class IsCurrentCountOrMouseOverConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue || values[2] == DependencyProperty.UnsetValue || values[3] == DependencyProperty.UnsetValue)
                return Visibility.Hidden;

            CountedCell currentCell = (CountedCell)values[0];
            CountedCell cell = (CountedCell)values[1];
            bool isMouseOver = (bool)values[2];
            bool showAll = (bool)values[3];

            bool same = currentCell == cell;
            return (same || isMouseOver || showAll) ? Visibility.Visible : Visibility.Hidden;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
