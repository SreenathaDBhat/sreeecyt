﻿using System;
using System.Windows;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Media;
using System.Threading;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace AI
{
    public partial class ReviewScoresPage : IClosing
    {
        private ReviewScoresController controller;
        private Point addScorePoint;
        private bool firstTime = true;
        
        public ReviewScoresPage(Guid scoringSession, Guid caseId)
        {
            controller = new ReviewScoresController(scoringSession, caseId);
            controller.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(controller_PropertyChanged);
            DataContext = controller;

            InitializeComponent();

            image.AvailableFilters = FilterTypes.FluorescentTypes;
            image.HistogramEstimator = new InterphaseHistogramEstimator();

            Loaded += OnLoaded;
        }

        void controller_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "CurrentCell")
                stretchOrSizeCanvas.ResetZoom();
        }

        void OnLoaded(object sender, RoutedEventArgs e)
        {
            ThreadPool.QueueUserWorkItem(delegate
            {
                controller.Load(Dispatcher, image);

                Dispatcher.Invoke((ThreadStart)delegate
                {
                    loadingScores.Visibility = Visibility.Collapsed;

                    image.Initialize();

                    if (!firstTime)
                    {
                        image.RefreshImage();
                    }

                    firstTime = false;
                    SetImageBinding();

                }, DispatcherPriority.Normal);
            });
        }

        private void SetImageBinding()
        {
            var bind = new Binding("DisplayFrame");
            bind.Source = controller;
            image.SetBinding(ImageFrameRenderer.ImageFrameXProperty, bind);
            image.Render();
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
			try
			{
				controller.Save();
				UIStack.For(this).Pop(UIStackResult.Cancel);
			}
			catch (Exception ex)
			{
				ExceptionUtility.ShowLog( ex, "Error");
			}
        }

        private void OnCancelMessageOk(object sender, RoutedEventArgs e)
        {
            UIStack.For(this).Pop(UIStackResult.Cancel);
        }

        private void OnCancelMessageCancel(object sender, RoutedEventArgs e)
        {
            cancelMessage.IsOpen = false;
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            if (controller.IsModified)
            {
                cancelMessage.IsOpen = true;
            }
            else
            {
                UIStack.For(this).Pop(UIStackResult.Cancel);
            }
        }

        private void OnGridSize0(object sender, RoutedEventArgs e)
        {
            controller.GridSize = 70;
        }

        private void OnGridSize1(object sender, RoutedEventArgs e)
        {
            controller.GridSize = 120;
        }

        private void OnGridSize2(object sender, RoutedEventArgs e)
        {
            controller.GridSize = 200;
        }

        private void OnGridSize3(object sender, RoutedEventArgs e)
        {
            controller.GridSize = 300;
        }

        private void OnClassTurnedOn(object sender, RoutedEventArgs e)
        {
            ClassTotal ct = (ClassTotal)((FrameworkElement)sender).Tag;
            controller.Show(ct.ScoringClass);
        }

        private void OnShowAllChecked(object sender, RoutedEventArgs e)
        {
            controller.ShowAll();
        }

        private void ToggleChannelPopup(object sender, RoutedEventArgs e)
        {
            channelPopup.IsOpen = ((CheckBox)sender).IsChecked.Value;
        }

        private void CanReclassifyCellExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = e.Parameter != null;
        }

        private void ReclassifyCellExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            reclassifyPopup.Tag = e.Parameter;
            reclassifyPopup.IsOpen = true;

            cellsGrid.EnsureSelected((ScoredCell)e.Parameter);
        }


        private void OnReclassifyClicked(object sender, RoutedEventArgs e)
        {
            ProbeScoringClass classs = (ProbeScoringClass)((FrameworkElement)sender).Tag;
            controller.Reclassify(cellsGrid.SelectedCells, classs);
            reclassifyPopup.IsOpen = false;
        }

        private void OnReclassifyToOther(object sender, RoutedEventArgs e)
        {
            ReclassifyToOther();
        }

        private void ReclassifyToOther()
        {
            string newClassName = reclassifyOther.Text;

            if (newClassName.Length > 0)
            {
                ProbeScoringClass existingClass = controller.Assay.Classes.Where(c => c.Text == newClassName).FirstOrDefault();

                if (existingClass != null)
                {
                    controller.Reclassify(cellsGrid.SelectedCells, existingClass);
                }
                else
                {
                    Key nextShortcut = controller.Assay.NextAvailableShortcut();
                    ProbeScoringClass newClass = new ProbeScoringClass { Color = ProbeScoringClass.RandomColor(), Text = newClassName, ShortcutKey = nextShortcut };
                    controller.Reclassify(cellsGrid.SelectedCells, newClass);
                    controller.Assay.AddClass(newClass);
                }
            }

            reclassifyPopup.IsOpen = false;
        
        }

        private void OnReclassifyToOtherWithReturnKey(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Return)
            {
                e.Handled = true;
                ReclassifyToOther();
            }
        }

        private void OnKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Delete)
            {
                e.Handled = true;
                controller.DeleteCells(cellsGrid.SelectedCells);
            }
            else if (e.Key == Key.Tab)
            {
                ToggleBigImage();
            }
        }

        private void OnClickBigImage(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            addScorePopup.IsOpen = true;
            addScorePoint = e.GetPosition(image);
        }

        private void OnAddScoreClassSelected(object sender, RoutedEventArgs e)
        {
            controller.AddScore((ProbeScoringClass)((FrameworkElement)sender).Tag, addScorePoint, image);
            addScorePopup.IsOpen = false;
        }

        private void OnDeleteIndividualScore(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var cell = (ScoredCell)((FrameworkElement)sender).Tag;
            controller.DeleteCells(new[] { cell });
        }

        private void ToggleBigImage()
        {
            controller.ShowBigImage = !controller.ShowBigImage;
        }

        private void OnShowImageChecked(object sender, RoutedEventArgs e)
        {
            image.Render();
        }

        private void OnShowImageUnchecked(object sender, RoutedEventArgs e)
        {
            if (channelPopup.IsOpen)
                channelPopup.IsOpen = false;
        }

        private void CanZoomToCellExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = e.Parameter != null;
        }

        private void ZoomToCellExecuted(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            ScoredCell cellToZoomTo = (ScoredCell)e.Parameter;

            if (cellToZoomTo.Frame != controller.DisplayFrame)
            {
                controller.CurrentCell = cellToZoomTo;
            }

            stretchOrSizeCanvas.ZoomAndPanTo(cellToZoomTo.X, cellToZoomTo.Y, cellToZoomTo.Thumbnail.Width);
        }

        private void OnPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Add && cellsGrid.SelectedCells.Count() == 1)
            {
                ScoredCell cellToZoomTo = controller.CurrentCell;
                stretchOrSizeCanvas.ZoomAndPanTo(cellToZoomTo.X, cellToZoomTo.Y, cellToZoomTo.Thumbnail.Width);
                e.Handled = true;
            }
        }

		public void Closed()
		{
			controller.Save();
		}

        private void OnSortBySignal(object sender, RoutedEventArgs e)
        {
            SignalInfo signal = (SignalInfo)((FrameworkElement)sender).Tag;
            controller.SortBySignal(signal);
        }
    }


    public class ScoreForegroundConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length != 3 || values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue || values[2] == DependencyProperty.UnsetValue)
                return Brushes.White;

            var cell = (ScoredCell)values[0];
            var selectedCell = (ScoredCell)values[1];
            var isMouseOver = (bool)values[2];

            if (isMouseOver)
                return Brushes.Yellow;
            else if (cell == selectedCell)
                return Brushes.Orange;
            else
                return Brushes.White;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    public class ReviewScoresController : Notifier
    {
        private Guid scoringSession;
        private ObservableCollection<ScoredCell> scoredCells;
        private List<ScoredCell> deletedScores;
        private List<ScoredCell> addedScores;
        private Guid caseId;
        private ScoredCell currentCell;
        private double gridSize;
        private bool showBigImage;
        private string classNameFilter;
        private bool showScoresOnImage;
        private double scoreSize;
        private bool isModified;
        private Connect.BlobTrickler trickler;
        private ImageFrame displayFrame;
        private ProbeScoringAssay assay;
        private List<SignalInfo> imageChannels;
        private bool gotAnyCounts;

        public ReviewScoresController(Guid sessionId, Guid caseId)
        {
            scoringSession = sessionId;
            this.caseId = caseId;
            ShowBigImage = false;
            scoreSize = 100;
            showScoresOnImage = true;
            trickler = new Connect.BlobTrickler();

            Dictionary<Guid, ImageFrame> framePool = new Dictionary<Guid, ImageFrame>();

            using(var db = new Connect.Database())
            using (var lims = Connect.LIMS.New())
            {
                var firstFrame = db.ProbeScores.Where(p => p.sessionId == sessionId).FirstOrDefault();
                if (firstFrame != null)
                {
                    var dbFrame = db.ImageFrames.Where(f => f.imageFrameId == firstFrame.frameId).First();
                    imageChannels = (from c in db.ImageComponents
                                     where c.frameId == firstFrame.frameId && !c.isCounterstain && !c.isAnalysisOverlay
                                     select new SignalInfo
                                     {
                                         SignalName = c.displayName,
                                         Color = ColorParse.HexStringToColor(c.renderColor)
                                     }).ToList();
                }

                deletedScores = new List<ScoredCell>();
                addedScores = new List<ScoredCell>();

                var cells = (from s in db.ProbeScores
                             where s.sessionId == scoringSession
                             select new ScoredCell
                             {
                                 Id = s.scoreId,
                                 Frame = LoadFromPool(framePool, db, s.frameId, caseId),
                                 X = s.x,
                                 Y = s.y,
                                 Class = s.content,
                                 Color = ColorParse.HexStringToColor(s.color),
                                 SpotMeasurements = s.measurements
                             }).ToList();

                cells.Sort((a, b) =>
                {
                    int classComp = a.Class.CompareTo(b.Class);
                    if (classComp != 0)
                    {
                        return classComp;
                    }

                    return a.Frame.Id.CompareTo(b.Frame.Id);
                });

                scoredCells = new ObservableCollection<ScoredCell>(cells);
                ParseMeasurements();

                CurrentCell = scoredCells[0];

                gridSize = 200;

                var dbc = db.Cases.Where(c => c.caseId == caseId).First();
                var lc = lims.GetCase(dbc.limsRef);

                CaseName = lc.Name;

                var session = (from s in db.ProbeScoreSessions
                               where s.sessionId == sessionId
                               select s).First();

                var ls = lims.GetSlide(session.Slide.limsRef);
                SlideName = ls.Name;
                OwnerName = session.ownerUser;

                Assay = new ProbeScoringAssay()
                {
                    Classes = ProbeScoringAssay.ClassesFromXml(session.assay),
                    Name = session.assay.Attribute("Name").Value
                };
            }
        }

        private void ParseMeasurements()
        {
            if (imageChannels == null)
                return;

            foreach (var score in scoredCells)
            {
                if (score.ParseMeasurements(imageChannels))
                    gotAnyCounts = true;
            }
        }

        private ImageFrame LoadFromPool(Dictionary<Guid, ImageFrame> framePool, AI.Connect.Database db, Guid frameId, Guid caseId)
        {
            if(framePool.ContainsKey(frameId))
                return framePool[frameId];

            var n = ImageFrameDbLoad.FromDb(db, frameId, caseId);
            framePool[frameId] = n;
            return n;
        }

        public IEnumerable<SignalInfo> Signals 
        { 
            get { return imageChannels; } 
        }

        public string SlideName 
        { 
            get; 
            private set; 
        }
        
        public Guid CaseId 
        { 
            get { return caseId; } 
        }
        
        public string CaseName 
        { 
            get; 
            private set; 
        }
        
        public IEnumerable<ScoredCell> Scores 
        { 
            get { return scoredCells; } 
        }

        public string AssayName
        {
            get { return assay.Name; }
        }

        public ProbeScoringAssay Assay
        {
            get { return assay; }
            set { assay = value; Notify("Classes"); Notify("AssayName"); }
        }

        public string OwnerName
        {
            get;
            private set;
        }

        public IEnumerable<ProbeScoringClass> Classes
        {
            get { return assay.Classes; }
        }
        
        public double GridSize 
        { 
            get { return gridSize; } 
            set { gridSize = value; Notify("GridSize"); } 
        }
        
        public bool ShowScoresOnImage 
        { 
            get { return showScoresOnImage; } 
            set { showScoresOnImage = value; Notify("ShowScoresOnImage"); } 
        }
        
        public double ScoreSize 
        { 
            get { return scoreSize; } 
            set { scoreSize = value; Notify("ScoreSize"); } 
        }
        
        public bool IsModified 
        { 
            get { return isModified; } 
        }
        
        public bool ShowBigImage 
        {
            get { return showBigImage; }
            set 
            { 
                showBigImage = value; 
                Notify("ShowBigImage");

                if (value)
                    DisplayFrame = currentCell.Frame;
            } 
        }

        public ScoredCell CurrentCell
        {
            get { return currentCell; }
            set 
            { 
                currentCell = value;

                if (value != null)
                {
                    Notify("CurrentCell", "ScoresForCurrentFrame");

                    if (showBigImage)
                        DisplayFrame = currentCell.Frame;
                }
            }
        }

        public ImageFrame DisplayFrame
        {
            get { return displayFrame; }
            private set { displayFrame = value; Notify("DisplayFrame"); }
        }

        internal void Load(Dispatcher uiDispatcher, ImageFrameRenderer image)
        {
            var db = new Connect.Database();
            var firstScore = scoredCells.First();
            var testPath = firstScore.Id.ToString() + ScoredCell.ScoreThumbExtension;
            bool gotThumbs = Connect.BlobStream.Exists(caseId, testPath, db);

            if (gotThumbs)
            {
                LoadThumbs(scoredCells, db, uiDispatcher);
            }
            else
            {
                ScoreThumbnailGenerator.GenerateThumbs(caseId, uiDispatcher, scoredCells, image, new Connect.BlobTrickler());
            }
        }

        private void LoadThumbs(IEnumerable<ScoredCell> scoredCells, Connect.Database db, Dispatcher uiDispatcher)
        {
            ThreadPool.QueueUserWorkItem(delegate(object state)
            {
                foreach (var score in scoredCells)
                {
                    var path = score.Id.ToString() + ScoredCell.ScoreThumbExtension;
                    var stream = Connect.BlobStream.ReadBlobStream(caseId, path, db, Connect.CacheOption.DontCache);

                    if (stream == null)
                        continue;

                    var thumb = new BitmapImage();
                    thumb.BeginInit();
                    thumb.StreamSource = stream;
                    thumb.EndInit();
                    thumb.Freeze();

                    uiDispatcher.Invoke((ThreadStart)delegate
                    {
                        score.Thumbnail = thumb;

                    }, DispatcherPriority.Normal);
                }
            });
        }

        public ProbeScoringResults Totals
        {
            get { return ProbeScoringResultsGenerator.GenerateResultsForSlide(from s in scoredCells select (ProbeScore)s); }
        }

        public IEnumerable<ScoredCell> OrderedScores
        {
            get 
            { 
                if(classNameFilter == null || classNameFilter.Length == 0)
                    return scoredCells;

                return scoredCells.Where(s => s.Class == classNameFilter);
            }
        }

        public IEnumerable<ScoredCell> ScoresForCurrentFrame
        {
            get
            {
                if (currentCell == null)
                    return null;

                var imageId = currentCell.Frame.Id;
                return OrderedScores.Where(s => s.Frame.Id == imageId);
            }
        }

        public int NumberOfScores
        {
            get { return scoredCells.Count; }
        }

        public void Show(string className)
        {
            classNameFilter = className;
            Notify("OrderedScores");
        }

        public void ShowAll()
        {
            classNameFilter = string.Empty;
            Notify("OrderedScores");
        }

        public void Save()
        {
            var modifiedCells = from s in scoredCells
                                where s.IsModified
                                select s;

			using (var db = new Connect.Database())
			{
				bool somethingChanged = false;

				if (modifiedCells.Count() > 0)
				{
					foreach (var s in modifiedCells)
					{
						var dbScore = db.ProbeScores.Where(c => c.scoreId == s.Id).First();
						dbScore.color = ColorParse.ToHexString(s.Color);
						dbScore.content = s.Class;
					}

					db.SubmitChanges();
					somethingChanged = true;
				}

				if (deletedScores.Count > 0)
				{
					foreach (var p in deletedScores)
					{
						// This query could return null if someone else has also deleted the same cell
						var dbCell = db.ProbeScores.Where(s => s.sessionId == scoringSession && s.scoreId == p.Id).FirstOrDefault();
						if (dbCell != null)
						{
							if ((dbCell.blobId != null) && (dbCell.blobId != Guid.Empty))
								Connect.BlobStream.Delete(dbCell.blobId, db);

							db.ProbeScores.DeleteOnSubmit(dbCell);
						}
					}

					db.SubmitChanges();
					somethingChanged = true;
				}

				if (addedScores.Count > 0)
				{
					db.ProbeScores.InsertAllOnSubmit(from s in addedScores
													 select new Connect.ProbeScore
													 {
														 color = ColorParse.ToHexString(s.Color),
														 content = s.Class,
														 frameId = s.Frame.Id,
														 measurements = "",
														 scoreId = s.Id,
														 sessionId = scoringSession,
														 x = s.X,
														 y = s.Y
													 });
					db.SubmitChanges();
					somethingChanged = true;
				}

				if (somethingChanged)
				{
					var session = db.ProbeScoreSessions.Where(s => s.sessionId == scoringSession).First();
					session.assay = assay.ToXml();
					session.modified = DateTime.Now;
					db.SubmitChanges();
				}
			}
        }

        private bool IsDeleted(Guid scoreId)
        {
            foreach (var s in deletedScores)
                if (s.Id == scoreId)
                    return true;

            return false;
        }

        public void Reclassify(IEnumerable<ScoredCell> cells, ProbeScoringClass classs)
        {
            foreach (var cell in cells)
            {
                if (cell.Class == classs.Text)
                    continue;

                cell.Reclassify(classs);
                isModified = true;
            }

            Notify("IsModified", "Totals");
        }

        public void DeleteCells(IEnumerable<ScoredCell> cellsToDelete)
        {
            var curCell = currentCell;

            foreach (var cell in cellsToDelete)
            {
                scoredCells.Remove(cell);

				if (addedScores.Contains(cell))
				{
					addedScores.Remove(cell);
				}
				else
				{
					deletedScores.Add(cell);
				}
            }

            isModified = true;

            // select a random cell from the current frame.
            if (curCell != null)
            {
                foreach (var c in scoredCells)
                {
                    if (c.Frame.Id == curCell.Frame.Id)
                    {
                        currentCell = c;
                        break;
                    }
                }
            }

            Notify("OrderedScores", "ScoresForCurrentFrame", "Totals", "CurrentCell", "NumberOfScores");
        }

        public void AddScore(ProbeScoringClass c, Point addScorePoint, ImageFrameRenderer image)
        {
            var thumb = ScoreThumbnailGenerator.CutThumb(addScorePoint, image);

            ScoredCell newScore = new ScoredCell
            {
                Class = c.Text,
                Color = c.Color,
                Frame = CurrentCell.Frame,
                Id = Guid.NewGuid(),
                SpotMeasurements = "",
                Thumbnail = thumb,
                X = addScorePoint.X,
                Y = addScorePoint.Y
            };

            scoredCells.Add(newScore);
            addedScores.Add(newScore);

            ScoreThumbnailGenerator.EnqueueProbeScoreThumb(trickler, caseId, newScore.Id, thumb);
            isModified = true;

            Notify("OrderedScores", "ScoresForCurrentFrame", "Totals", "NumberOfScores");
        }

        internal void Reclassify(IEnumerable<ScoredCell> cells, System.Windows.Input.Key key)
        {
            if (key >= Key.NumPad1 && key <= Key.NumPad9)
                key = Key.F1 + (key - Key.NumPad1);

            else if (key == Key.NumPad0)
                key = Key.F10;

            var c = Classes.Where(cl => cl.ShortcutKey == key).FirstOrDefault();
            
            if (c == null)
                return;

            Reclassify(cells, c);              
        }

        public bool ShowSortingControls
        {
            get { return imageChannels.Count > 0 && gotAnyCounts; }
        }

        public void SortBySignal(SignalInfo signal)
        {
            var list = scoredCells.ToList();
            list.Sort((a, b) => -a.CompareTo(b, signal));

            scoredCells = new ObservableCollection<ScoredCell>(list);
            Notify("OrderedScores", "ScoresForCurrentFrame", "Totals", "CurrentCell", "NumberOfScores");
        }
    }

    public class ShowIndividualCountsConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool showSortingControls = (bool)values[0];
            double gridSize = (double)values[1];

            if (showSortingControls && gridSize >= 200.0)
                return true;
            
            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
