﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml.Linq;
using System.Drawing;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Data;

namespace AI
{
    public partial class ScoreCircle : UserControl
    {
        public static DependencyProperty DiameterProperty = DependencyProperty.Register("Diameter", typeof(double), typeof(ScoreCircle));
        public static DependencyProperty CountedCellProperty = DependencyProperty.Register("CountedCell", typeof(CountedCell), typeof(ScoreCircle), new FrameworkPropertyMetadata(OnCountedCellChanged));
        public event EventHandler Removed;

        public ScoreCircle()
        {
            InitializeComponent();
        }

        public CountedCell CountedCell
        {
            get { return (CountedCell)GetValue(CountedCellProperty); }
            set { SetValue(CountedCellProperty, value); }
        }

        public double Diameter
        {
            get { return (double)GetValue(DiameterProperty); }
            set { SetValue(DiameterProperty, value); }
        }

        private static void OnCountedCellChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((ScoreCircle)sender).DataContext = ((CountedCell)e.NewValue);
        }

        private void OnRemoveMe(object sender, EventArgs e)
        {
            if (Removed != null)
            {
                Removed(this, EventArgs.Empty);
            }
        }
    }
}
