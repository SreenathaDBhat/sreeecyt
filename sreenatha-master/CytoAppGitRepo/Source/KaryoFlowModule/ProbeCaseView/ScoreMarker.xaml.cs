﻿using System;
using System.Windows;

namespace AI
{
    public partial class ScoreMarker
    {
        public static DependencyProperty ScoreProperty = DependencyProperty.Register("Score", typeof(object), typeof(ScoreMarker));
        public static DependencyProperty DiameterProperty = DependencyProperty.Register("Diameter", typeof(double), typeof(ScoreMarker));

        public ScoreMarker()
        {
            DataContext = this;
            InitializeComponent();
        }

        public double Diameter
        {
            get { return (double)GetValue(DiameterProperty); }
            set { SetValue(DiameterProperty, value); }
        }

        public object Score
        {
            get { return GetValue(ScoreProperty); }
            set { SetValue(ScoreProperty, value); }
        }
    }
}
