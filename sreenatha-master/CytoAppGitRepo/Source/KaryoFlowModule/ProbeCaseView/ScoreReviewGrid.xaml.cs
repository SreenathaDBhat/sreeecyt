﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Data;

namespace AI
{
    public partial class ScoreReviewGrid
    {
        public static DependencyProperty SelectedCellProperty = DependencyProperty.Register("SelectedCell", typeof(ScoredCell), typeof(ScoreReviewGrid), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty CellsProperty = DependencyProperty.Register("Cells", typeof(IEnumerable<ScoredCell>), typeof(ScoreReviewGrid), new FrameworkPropertyMetadata(OnCellsChanged));
        public static DependencyProperty GridSizeProperty = DependencyProperty.Register("GridSize", typeof(double), typeof(ScoreReviewGrid), new FrameworkPropertyMetadata(200.0));
        public static DependencyProperty ShowIndividualCountsProperty = DependencyProperty.Register("ShowIndividualCounts", typeof(bool), typeof(ScoreReviewGrid));
        public static readonly RoutedUICommand ReclassifyCell = new RoutedUICommand("ReclassifyCell", "ReclassifyCell", typeof(ScoreReviewGrid));
        public static readonly RoutedUICommand ZoomToCell = new RoutedUICommand("ZoomToCell", "ZoomToCell", typeof(ScoreReviewGrid));

        public ScoreReviewGrid()
        {
            InitializeComponent();
        }

        public IEnumerable<ScoredCell> Cells
        {
            get { return (IEnumerable<ScoredCell>)GetValue(CellsProperty); }
            set { SetValue(CellsProperty, value); }
        }

        public ScoredCell SelectedCell
        {
            get { return (ScoredCell)GetValue(SelectedCellProperty); }
            set { SetValue(SelectedCellProperty, value); }
        }

        public bool ShowIndividualCounts
        {
            get { return (bool)GetValue(ShowIndividualCountsProperty); }
            set { SetValue(ShowIndividualCountsProperty, value); }
        }

        public double GridSize
        {
            get { return (double)GetValue(GridSizeProperty); }
            set { SetValue(GridSizeProperty, value); }
        }

        public IEnumerable<ScoredCell> SelectedCells
        {
            get 
            {
                var copy = new List<ScoredCell>();
                foreach (var s in list.SelectedItems)
                    copy.Add((ScoredCell)s);

                return copy;
            }
        }

        private static void OnCellsChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((ScoreReviewGrid)sender).SelectedCell = ((IEnumerable<ScoredCell>)e.NewValue).FirstOrDefault();
        }

        private void OnMouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ((ScoredCell)((FrameworkElement)sender).Tag).IsMouseOver = true;
        }

        private void OnMouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            ((ScoredCell)((FrameworkElement)sender).Tag).IsMouseOver = false;
        }

		private void OnKeyDown(object sender, KeyEventArgs e)
		{
			// Force the left/right arrow keys to wrap moves at the end of a row
			if (list.Items.Count > 0)
			{
				switch (e.Key)
				{
					case Key.Right:
					{
						if (!list.Items.MoveCurrentToNext())
						{
							list.Items.MoveCurrentToFirst();
						}

						e.Handled = true;
						ListBoxItem lbi = (ListBoxItem)list.ItemContainerGenerator.ContainerFromItem(list.SelectedItem);
						lbi.Focus();
						break;
					}

					case Key.Left:
					{
						if (!list.Items.MoveCurrentToPrevious())
						{
							list.Items.MoveCurrentToLast();
						}

						e.Handled = true;
						ListBoxItem lbi = (ListBoxItem)list.ItemContainerGenerator.ContainerFromItem(list.SelectedItem);
						lbi.Focus();
						break;
					}

					default:
					{
						break;
					}
				}
			}
		}

        internal void EnsureSelected(object content)
        {
            var cont = list.ItemContainerGenerator.ContainerFromItem(content) as ListBoxItem;
            if (cont.IsSelected)
                return;

            list.SelectedItems.Clear();
            list.SelectedItems.Add(content);
        }
    }


    public class SignalInfo
    {
        public string SignalName { get; set; }
        public Color Color { get; set; }
    }

    public class SignalCount
    {
        public SignalInfo SignalInfo { get; set; }
        public int Count { get; set; }
    }

    public class ScoredCell : ProbeScore
    {
        public static readonly string ScoreThumbExtension = ".scorethumb";

        private bool isMouseOver;
        public bool IsMouseOver
        {
            get { return isMouseOver; }
            set { isMouseOver = value; Notify("IsMouseOver"); }
        }

        public void Reclassify(ProbeScoringClass classs)
        {
            Class = classs.Text;
            Color = classs.Color;
            IsModified = true;
        }

        public bool IsModified { get; set; }


        
        
        private List<SignalCount> counts = new List<SignalCount>();
        public IEnumerable<SignalCount> Counts { get { return counts; } }
        
        public bool ParseMeasurements(IEnumerable<SignalInfo> imageChannels)
        {
            // measurements field in db looks like this:
            // DAPI=6 Spectrum Orange=3
            // Given the list of channel names, we need to parse out the numbers. Easy.

            bool gotAnyCounts = false;
            var meas = SpotMeasurements;

            foreach (var channel in imageChannels)
            {
                var index = meas.IndexOf(channel.SignalName);
                if (index == -1)
                {
                    counts.Add(new SignalCount { SignalInfo = channel, Count = 0 });
                    continue;
                }
                else
                {
                    int positionOfNumber = index + channel.SignalName.Length + 1;
                    int positionOfWhitespaceFollowingNumber = meas.IndexOf(' ', positionOfNumber);

                    if (positionOfWhitespaceFollowingNumber == -1)
                        positionOfWhitespaceFollowingNumber = meas.Length;

                    string number = meas.Substring(positionOfNumber, positionOfWhitespaceFollowingNumber - positionOfNumber);
                    int count;

                    if (int.TryParse(number, out count))
                    {
                        counts.Add(new SignalCount { SignalInfo = channel, Count = count });
                        gotAnyCounts = true;
                    }
                    else
                    {
                        counts.Add(new SignalCount { SignalInfo = channel, Count = 0 });
                    }
                }
            }

            return gotAnyCounts;
        }

        public int CompareTo(ScoredCell other, SignalInfo signal)
        {
            int myCount = counts.Where(c => c.SignalInfo == signal).First().Count;
            int theirCount = other.counts.Where(c => c.SignalInfo == signal).First().Count;

            return myCount.CompareTo(theirCount);
        }
    }


    public class MouseOverOpacityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((bool)value) ? 1 : 0.3;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
