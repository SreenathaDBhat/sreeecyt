﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace AI
{
    public partial class ScoreSessionSelectionWindow : Window
    {
        private ScoreSessionController controller;

        public ScoreSessionSelectionWindow(IEnumerable<UserScoringSession> otherScoringSessions)
        {
            controller = new ScoreSessionController(otherScoringSessions);
            DataContext = controller;
            InitializeComponent();
        }

        public ScoreSessionController Controller
        {
            get { return controller; }
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }

    public class ScoreSessionController : Notifier
    {
        private UserScoringSession selectedSession;
        
        public IEnumerable<UserScoringSession> ScoringSessions { get; set; }
        
        public bool HasSelectedSession 
        {
            get { return selectedSession != null; }
        }
        
        public UserScoringSession SelectedSession
        {
            get { return selectedSession; }
            set { selectedSession = value; Notify("HasSelectedSession"); }
        }
        
        public ScoreSessionController(IEnumerable<UserScoringSession> otherScoringSessions)
        {
            ScoringSessions = otherScoringSessions;
        }
    }
}
