﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Windows.Media;
using System.Threading;
using System.IO;
using System.Collections.Concurrent;

namespace AI
{
    public static class ScoreThumbnailGenerator
    {

        public static BitmapSource CutThumb(Point center, ImageFrameRenderer image)
        {
            int w = 300;
            var pixels = image.CopyPixels(new Int32Rect(
                                            (int)(center.X - w / 2),
                                            (int)(center.Y - w / 2),
                                            w, w));

            var thumb = BitmapSource.Create(w, w, 96, 96, PixelFormats.Bgra32, null, pixels, w * 4);
            thumb.Freeze();
            return thumb;
        }

        public static void EnqueueProbeScoreThumb(Connect.BlobTrickler trickler, Guid caseId, Guid probeScoreId, BitmapSource image)
        {
            var path = probeScoreId.ToString() + ScoredCell.ScoreThumbExtension;

            JpegBitmapEncoder enc = new JpegBitmapEncoder();
            enc.QualityLevel = 75;
            enc.Frames.Add(BitmapFrame.Create(image));

            var stream = new MemoryStream();
            enc.Save(stream);

            trickler.Enqueue(caseId, path, stream.GetBuffer());
        }

        public static void GenerateThumbs(Guid caseId, Dispatcher uiDispatcher, IEnumerable<ScoredCell> scoredCells, ImageFrameRenderer image, Connect.BlobTrickler trickler)
        {
            var distinctFrames = (from s in scoredCells select s.Frame).Distinct();
            BlockingCollection<ImageFrame> frames = new BlockingCollection<ImageFrame>();

            ThreadPool.QueueUserWorkItem(delegate
            {
                var loadTasks = (from f in distinctFrames
                                 select System.Threading.Tasks.Task.Factory.StartNew(o =>
                                 {
                                     var db = new Connect.Database();
                                     foreach (var channel in f.Channels)
                                     {
                                         var snap = f.FindProjectionSnap(channel, f.FindSnap(channel, -1));
                                         snap.LoadDataIfNeeded(db, caseId);
                                     }

                                     frames.Add(f);
                                 }, f)).ToArray();

                System.Threading.Tasks.Task.WaitAll(loadTasks);
                frames.CompleteAdding();
            });

            foreach (var frame in frames.GetConsumingEnumerable())
            {
                var cells = scoredCells.Where(c => c.Frame.Id == frame.Id);

                foreach (var cell in cells)
                {
                    uiDispatcher.Invoke((ThreadStart)delegate
                    {
                        image.ImageFrameX = cell.Frame;

                    }, DispatcherPriority.Normal);

                    WaitForImageToLoad(image, uiDispatcher);

                    uiDispatcher.Invoke((ThreadStart)delegate
                    {
                        image.Render(false);
                    }, DispatcherPriority.Normal);

                    cell.Thumbnail = CutThumb(new Point(cell.X, cell.Y), image);

                    EnqueueProbeScoreThumb(trickler, caseId, cell.Id, cell.Thumbnail as BitmapSource);
                }

                frame.DumpAllImageData();
            }
        }

        private static void WaitForImageToLoad(ImageFrameRenderer image, Dispatcher uiDispatcher)
        {
            while (true)
            {
                bool stillLoading = true;
                uiDispatcher.Invoke((ThreadStart)delegate
                {
                    stillLoading = image.IsLoading;

                }, DispatcherPriority.Normal);

                if (!stillLoading)
                    break;

                Thread.Sleep(100);
            }
        }
    }
}
