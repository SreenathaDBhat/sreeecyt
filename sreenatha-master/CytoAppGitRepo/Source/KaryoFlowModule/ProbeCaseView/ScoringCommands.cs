﻿using System;
using System.Windows.Input;

namespace AI
{
    internal static class ScoringCommands
    {
        public static RoutedUICommand AddClass = new RoutedUICommand("Add class", "AddClass", typeof(ScoringCommands));
        public static RoutedUICommand ClearClasses = new RoutedUICommand("Clear classes", "ClearClasses", typeof(ScoringCommands));
        public static RoutedUICommand ToggleCountDisplay = new RoutedUICommand("Show / hide counts", "ToggleCountsDisplay", typeof(ScoringCommands));
        public static RoutedUICommand OneStackDown = new RoutedUICommand("One Stack Down", "OneStackDown", typeof(ScoringCommands));
        public static RoutedUICommand OneStackUp = new RoutedUICommand("One Stack Up", "OneStackUp", typeof(ScoringCommands));
    }
}
