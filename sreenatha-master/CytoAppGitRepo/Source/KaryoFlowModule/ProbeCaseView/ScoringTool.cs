﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using System.Threading;
using AI.Security;
using System.Threading.Tasks;
using System.Collections;

namespace AI
{
    public class IScorerAugmentedFrame
    {
        public ImageFrame Frame { get; set; }
        public IEnumerable<Guid> AllFramesTouchedInOtherSessions { get; set; }
        public IEnumerable AllScores { get; set; }
        public bool Starred { get; set; }
    }

    public interface IScorer
    {
        IEnumerable<IScorerAugmentedFrame> FramesForSelectionWindow { get; }
        ImageFrame CurrentFrame { get; }
    }

    public class ScoringTool : INotifyPropertyChanged, IScorer
    {
        public class SlideInfo
        {
            public string Name { get; set; }
            public Guid Id { get; set; }
        }      

        public static readonly string ScoringDisplayOptionsString = "ProbeScoringDisplay";

        public static RoutedUICommand CreateNewAssay = new RoutedUICommand("Create Assay", "CreateAssay", typeof(ScoringTool));
        public static RoutedUICommand DeleteAssay = new RoutedUICommand("Delete Assay", "DeleteAssay", typeof(ScoringTool));
        public static RoutedUICommand DuplicateAssay = new RoutedUICommand("Duplicate Assay", "DuplicateAssay", typeof(ScoringTool));
        public static RoutedUICommand CreateClass = new RoutedUICommand("Create Assay", "CreateAssay", typeof(ScoringTool));
        public static RoutedUICommand DeleteClass = new RoutedUICommand("Delete class", "DeleteClasses", typeof(ScoringTool));
        public static RoutedUICommand ToggleScores = new RoutedUICommand("Toggle scores", "ToggleScores", typeof(ScoringTool));
        public static RoutedUICommand ToggleAnnotations = new RoutedUICommand("Toggle annotations", "ToggleAnnotations", typeof(ScoringTool));
        public static RoutedUICommand ClearScoresFromFrame = new RoutedUICommand("Clear frame scores", "ClearScoresFromFrame", typeof(ScoringTool));
        public static RoutedUICommand ClearScoresFromSlide = new RoutedUICommand("Clear slide scores", "ClearScoresFromSlide", typeof(ScoringTool));
        public static RoutedUICommand NextFrame = new RoutedUICommand("Next Frame", "NextFrame", typeof(ScoringTool));
        public static RoutedUICommand PrevFrame = new RoutedUICommand("Previous Frame", "PrevFrame", typeof(ScoringTool));

		public delegate void CurrentFrameChangingEventHandler(object sender, CurrentFrameChangingEventArgs e);

		public event CurrentFrameChangingEventHandler CurrentFrameChanging;
        public event EventHandler CurrentFrameChanged;

        private ObservableCollection<ProbeScoringAssay> assays;
        private ProbeScoringAssay currentAssay;
        private List<ProbeScore> scores;
        private List<Guid> framesTouchedInOtherSessions;
        private List<ProbeScore> potentialOrphans;
        private List<ImageFrame> frames;
        private bool showScores;
        private double scoreSize = 150;
        private bool showAnnotations = true;
        private ObservableCollection<Annotation> annotations;
        private ImageFrame currentFrame;
        private Guid caseId;
        private string caseName;
        private List<SlideInfo> slides;
        private SlideInfo currentSlide;
        private InterphaseScoreSession currentScoringSession;
        private Connect.BlobTrickler trickler;

        public ScoringTool(Guid caseId, InterphaseScoreSession scoreSession, string assay)
            : this(caseId, scoreSession)
        {
            SelectAssay(assay);
        }

        public ScoringTool(Guid caseId, InterphaseScoreSession scoreSession)
        {
            Guid slideId = scoreSession.SlideId;
            currentScoringSession = scoreSession;

            using(var db = new Connect.Database())
            using (var lims = Connect.LIMS.New())
            {
                trickler = new Connect.BlobTrickler();

                this.caseId = caseId;

                var allslides = db.Slides.Where(s => s.caseId == caseId);
                slides = new List<SlideInfo>();
                foreach (var s in allslides)
                {
                    var ls = lims.GetSlide(s.limsRef);
                    slides.Add(new SlideInfo
                    {
                        Id = s.slideId,
                        Name = ls.Name
                    });
                }

                this.currentSlide = slides.First();

                frames = (from f in db.ImageFrames
                          where f.slideId == currentSlide.Id
                          select ImageFrameDbLoad.FromDb(db, f, caseId)).ToList();

                annotations = new ObservableCollection<Annotation>();
                assays = new ObservableCollection<ProbeScoringAssay>();
                scores = new List<ProbeScore>();
                framesTouchedInOtherSessions = new List<Guid>();

                LoadSettings(db);
                LoadScoringData(db);
                LoadImageFrameStars(db);
                LoadScoringDataFromOtherSessions(db);
                LoadThumbs(Dispatcher.CurrentDispatcher);

                potentialOrphans = scores.ToList();

                CurrentFrame = frames.FirstOrDefault();

                var dbc = db.Cases.Where(c => c.caseId == caseId).First();
                var lc = lims.GetCase(dbc.limsRef);
                caseName = lc.Name;
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        void Notify(string s)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(s));
        }

        #endregion

        public ImageFrame CurrentFrame
        {
            get { return currentFrame; }
            set
            {
                if (value != null)
                {
					CurrentFrameChangingEventArgs args = new CurrentFrameChangingEventArgs(currentFrame, value);
					if (CurrentFrameChanging != null)
						CurrentFrameChanging(this, args);

					if (args.Cancel == false)
					{
						currentFrame = value;

						if (CurrentFrameChanged != null)
							CurrentFrameChanged(this, EventArgs.Empty);

						if (currentFrame.Thumbnail == null)
							currentFrame.Thumbnail = Load.LoadThumbnail(currentFrame.Id, CaseId);

						Notify("CurrentFrame");
						Notify("AllScores");
						Notify("ScoresForCurrentFrame");
						Notify("ResultsForCurrentSlide");
						Notify("IndexOfCurrentFrame");
						Notify("FrameScoredInOtherSession");
					}
                }
            }
        }

        public IEnumerable<ImageFrame> Frames
        {
            get { return frames; }
        }

        public SlideInfo Slide
        {
            get { return currentSlide; }
        }

        public string CaseName { get { return caseName; } }
        
        public IEnumerable<ProbeScoringAssay> Assays
        {
            get { return assays; }
        }

        public IEnumerable<Annotation> Annotations
        {
            get { return annotations; }
        }

        public ProbeScoringAssay CurrentAssay
        {
            get { return currentAssay; }
            set { currentAssay = value; Notify("CurrentAssay"); }
        }

        public bool ShowAllScores
        {
            get { return showScores; }
            set { showScores = value; Notify("ShowAllScores"); }
        }

        public bool ShowAnnotations
        {
            get { return showAnnotations; }
            set { showAnnotations = value; }
        }

        public double ScoreSize
        {
            get { return scoreSize; }
            set { scoreSize = value; Notify("ScoreSize"); Notify("ScoreSizeUiOffset"); }
        }

        public double ScoreSizeUiOffset
        {
            get { return ScoreSize / -2; }
        }

        public IEnumerable<ProbeScore> ScoresForFrame(ImageFrame frame)
        {
            return scores.Where(s => s.Frame == frame);
        }

        public IEnumerable<ProbeScore> AllScores
        {
            get { return scores; }
        }

        public IEnumerable<Guid> AllFramesTouchedInOtherSessions
        {
            get { return framesTouchedInOtherSessions; }
        }

		public bool FrameScoredInOtherSession
		{
			get
			{
				return framesTouchedInOtherSessions.Where(f => f == currentFrame.Id).Count() > 0;
			}
		}

        public IEnumerable<ProbeScore> ScoresForCurrentFrame
        {
            get 
            {
                if (currentFrame == null)
                    return new ProbeScore[0];

                return ScoresForFrame(currentFrame); 
            }
        }

        public IEnumerable<ProbeScore> ScoresForCurrentSlide
        {
            get { return scores; }
        }

        public bool AddScoreUsingShortcut(Key key, Point mousePos, ImageFrameRenderer image)
        {
            if (key >= Key.NumPad1 && key <= Key.NumPad9)
                key = Key.F1 + (key - Key.NumPad1);
            else if (key == Key.NumPad0)
                key = Key.F10;

            var c = currentAssay.Classes.Where(cl => cl.ShortcutKey == key).FirstOrDefault();
            if (c == null)
            {
                return false;
            }

            var score = new ProbeScore
            {
                Id = Guid.NewGuid(),
                Class = c.Text,
                Frame = currentFrame,
                X = mousePos.X,
                Y = mousePos.Y,
                Color = c.Color,
            };

            ScoreThumbnailGenerator.EnqueueProbeScoreThumb(trickler, caseId, score.Id, ScoreThumbnailGenerator.CutThumb(mousePos, image));
            scores.Add(score);
            isModified = true;


            Notify("AllScores");
            Notify("ScoresForCurrentFrame");
            Notify("ResultsForCurrentSlide");
            return true;
        }

        public void KillClass(ProbeScoringClass probeScoringClass)
        {
            currentAssay.DeleteClass(probeScoringClass);
        }

        public void DeleteScore(ProbeScore score)
        {
            scores.Remove(score);
            potentialOrphans.Add(score);
            isModified = true;

            Notify("AllScores");
            Notify("ResultsForCurrentSlide");
            Notify("ScoresForCurrentFrame");
        }

        public ProbeScoringResults ResultsForCurrentSlide
        {
            get { return ProbeScoringResultsGenerator.GenerateResultsForSlide(scores); }
        }

        #region Scoring Tool Serialisation

        private void LoadScoringData(Connect.Database db)
        {
            this.scores = new List<ProbeScore>();
            var session = db.ProbeScoreSessions.Where(s => s.sessionId == currentScoringSession.Id).FirstOrDefault();
            
            if (session == null)
            {
                return;
            }

            ProbeScoringAssay assay = new ProbeScoringAssay
            {
                Classes = ProbeScoringAssay.ClassesFromXml(session.assay),
                Name = session.assay.Attribute("Name").Value
            };

            foreach (var c in assay.Classes)
            {
                if (c.ShortcutKey == Key.Escape)
                    c.ShortcutKey = assay.NextAvailableShortcut();
            }

            CurrentAssay = assay;
            var rawScores = ProbeScoresForSession(session.sessionId, db);
            this.scores = rawScores.ToList();
        }

        private List<ProbeScore> ProbeScoresForSession(Guid sessionId, Connect.Database db)
        {
            var scores = (from s in db.ProbeScores
                          where s.sessionId == sessionId
                          select new
                          {
                              ProbeScore = new ProbeScore
                              {
                                  Class = s.content,
                                  Color = ColorParse.HexStringToColor(s.color),
                                  SpotMeasurements = s.measurements,
                                  X = s.x,
                                  Y = s.y,
                                  Id = s.scoreId
                              },

                              FrameId = s.frameId
                          }).ToArray();

            foreach (var score in scores)
            {
                score.ProbeScore.Frame = frames.Where(f => f.Id == score.FrameId).FirstOrDefault();
            }

            var rawScores = from s in scores select s.ProbeScore;
            return rawScores.ToList();
        }

        private void LoadScoringDataFromOtherSessions(AI.Connect.Database db)
        {
            framesTouchedInOtherSessions.Clear();

            var sessionIds = (from s in db.ProbeScoreSessions
                              where s.slideId == currentSlide.Id && s.sessionId != currentScoringSession.Id
                              select s.sessionId).ToList();

            foreach (var id in sessionIds)
            {
                var someScores = ProbeScoresForSession(id, db);
                foreach (ProbeScore cell in someScores)
                    framesTouchedInOtherSessions.Add(cell.Frame.Id);
            }
        }

        private void LoadImageFrameStars(AI.Connect.Database db)
        {
            var stars = (from s in db.ImageFrameStars
                         where s.sessionId == currentScoringSession.Id
                         select s).ToList();

            foreach (var star in stars)
            {
                ImageFrame frame = frames.Where(f => f.Id == star.imageFrameId).FirstOrDefault();
                if (frame != null)
                    frame.Starred = true;
            }
        }

        private void SelectAssay(string assayName)
        {
            foreach (var assay in assays)
            {
                if (string.Compare(assayName, assay.Name, StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    CurrentAssay = assay;
                    break;
                }
            }
        }

        private bool isModified = false;
        public bool IsModified
        {
            get { return isModified;}
        }

        public void SaveData()
        {
            SaveSettings();
            SaveScores();
            SaveImageFrameStars();
            isModified = false;
        }

        private void SaveImageFrameStars()
        {
            var db = new Connect.Database();

            db.ImageFrameStars.DeleteAllOnSubmit(db.ImageFrameStars.Where(star => star.sessionId == currentScoringSession.Id));
            db.SubmitChanges();

            foreach (var frame in frames)
            {
                if (frame.Starred)
                {
                    db.ImageFrameStars.InsertOnSubmit(new AI.Connect.ImageFrameStar()
                    {
                        starId = Guid.NewGuid(),
                        sessionId = currentScoringSession.Id,
                        imageFrameId = frame.Id
                    });
                }
            }

            db.SubmitChanges();
        }

        private void SaveScores()
        {
            if (currentAssay == null)
                return;

			var db = new Connect.Database();

			try
			{
				db.ProbeScoreSessions.DeleteAllOnSubmit(db.ProbeScoreSessions.Where(s => s.sessionId == currentScoringSession.Id));
				db.SubmitChanges();

				var session = new Connect.ProbeScoreSession
				{
					assay = currentAssay.ToXml(),
					ownerUser = currentScoringSession.Owner,
					slideId = this.currentSlide.Id,
					sessionId = Guid.NewGuid(),
					type = 0,
					modified = DateTime.Now
				};

				db.ProbeScoreSessions.InsertOnSubmit(session);

				db.ProbeScores.InsertAllOnSubmit(from s in scores
												 select new Connect.ProbeScore
												 {
													 color = ColorParse.ToHexString(s.Color),
													 content = s.Class,
													 frameId = s.Frame.Id,
													 measurements = s.SpotMeasurements == null ? "" : s.SpotMeasurements,
													 scoreId = s.Id,
													 sessionId = session.sessionId,
													 x = s.X,
													 y = s.Y
												 });

				db.SubmitChanges();


				var orphanScores = potentialOrphans.Where(s => !scores.Contains(s));
				foreach (var orphan in orphanScores)
				{
					var path = orphan.Id.ToString() + ScoredCell.ScoreThumbExtension;
					Connect.BlobStream.Delete(caseId, path, db);
				}

				db.SubmitChanges();

				// TODO : Need to keep currentScoringSession here but what do we do about InterphaseSlide.ScoringSessions
				// in InterphaseWorkflow.xaml.cs which has a session collection which is now out of date!
				currentScoringSession = new InterphaseScoreSession()
				{
					Id = session.sessionId,
					Owner = session.ownerUser,
					SlideId = session.slideId,
					Assay = session.assay.Attribute("Name").Value,
					Type = (ScoreSessionType)session.type,
					Modified = session.modified
				};
			}
			finally
			{
				db.Dispose();
			}
        }

        private void LoadSettings(Connect.Database db)
        {
            FileInfo settingsFile = AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\scoring.settings");
            settingsFile.Refresh();
            if (settingsFile.Exists)
            {
                XElement xml = XElement.Load(settingsFile.FullName);
                ShowAllScores = Parsing.TryParseBool(Parsing.ValueOrDefault(xml.Attribute("ShowScores"), "False"), false);
                ScoreSize = Parsing.TryParseDouble(Parsing.ValueOrDefault(xml.Attribute("ScoreCircleSize"), "100"), 100);
                ShowAnnotations = Parsing.TryParseBool(Parsing.ValueOrDefault(xml.Attribute("ShowAnnotations"), "False"), false);
            }

            RefreshAssays();
        
        }

        private void RefreshAssays(Connect.Database db)
        {
            assays = new ObservableCollection<ProbeScoringAssay>(from a in db.ProbeAssays
                                                                 select new ProbeScoringAssay()
                                                                 {
                                                                     Classes = ProbeScoringAssay.ClassesFromXml(a.assay),
                                                                     Id = a.assayId,
                                                                     Name = a.name
                                                                 });
        }

        public void RefreshAssays()
        {
            var db = new Connect.Database();
            RefreshAssays(db);
            db.Dispose();
            db = null;
        }

        private void SaveSettings()
        {
            XElement xml = new XElement("ScoringTool",
                new XAttribute("CurrentAssay", currentAssay == null ? "" : currentAssay.Name),
                new XAttribute("ShowScores", ShowAllScores),
                new XAttribute("ScoreCircleSize", ScoreSize),
                new XAttribute("ShowAnnotations", ShowAnnotations));

            FileInfo settingsFile = AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\scoring.settings");
            settingsFile.Refresh();
            if (settingsFile.Exists)
                settingsFile.Delete();

            xml.Save(settingsFile.FullName);
        }

        #endregion

        public void DeleteScoresForFrame(ImageFrame frame)
        {
            for (int i = scores.Count - 1; i >= 0; --i)
            {
                if (scores[i].Frame == frame)
                {
                    potentialOrphans.Add(scores[i]);
                    scores.RemoveAt(i);                    
                }
            }

            Notify("AllScores");
            Notify("ScoresForCurrentFrame");
            Notify("ResultsForCurrentSlide");
        }

        public void DeleteScoresForSlide()
        {
            potentialOrphans.AddRange(scores);
            scores.Clear();

            Notify("AllScores");
            Notify("ScoresForCurrentFrame");
            Notify("ResultsForCurrentSlide");
        }

        public int IndexOfCurrentFrame
        {
            get { return frames.IndexOf(currentFrame) + 1; }
        }

        public bool CanNextFrameExecute
        {
            get { return currentFrame != null && frames.IndexOf(currentFrame) < frames.Count - 1; }
        }

        public bool CanPrevFrameExecute
        {
            get { return currentFrame != null && frames.IndexOf(currentFrame) > 0; }
        }

        public void MoveToPrevFrame()
        {
            CurrentFrame = frames[frames.IndexOf(currentFrame) - 1];
        }

        public void MoveToNextFrame()
        {
            CurrentFrame = frames[frames.IndexOf(currentFrame) + 1];
        }

        public void LoadThumbs(Dispatcher dispatcher)
        {
            var db = new Connect.Database();
            var notifyList = new List<ImageFrame>();

            foreach (var frame in frames)
            {
                try
                {
                    string path = frame.Id.ToString() + ".jpg";
                    var bitmap = ImageFromBitmapSource.LoadBitmapImageFromStream(caseId, path, db, Connect.CacheOption.DontCache, 100);
                    frame.SetThumbNoNotify(bitmap);

                    notifyList.Add(frame);

                    if (notifyList.Count == 10)
                    {
                        var list = notifyList;
                        notifyList = new List<ImageFrame>();

                        dispatcher.BeginInvoke((ThreadStart)delegate
                        {
                            foreach (var c in list)
                            {
                                c.NotifyImages();
                            }
                        }, DispatcherPriority.Normal);
                    }
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine("EXCEPTION: LoadThumbs, Cell: {0}", frame.Id);
                    Console.WriteLine(frame.ToString());
                    Console.WriteLine(e.ToString());
                }
            }

            db.Dispose();
            db = null;

            dispatcher.Invoke((ThreadStart)delegate
            {
                foreach (var c in notifyList)
                {
                    c.NotifyImages();
                }
            }, DispatcherPriority.Normal);
        }

        public Guid CaseId
        {
            get { return caseId; }
        }

        public void WaitForImageWrites()
        {
            while (trickler.IsBusy)
            {
                Thread.Sleep(100);
            }
        }

        public IEnumerable<IScorerAugmentedFrame> FramesForSelectionWindow
        {
            get
            {
                return (from f in frames
                        select new IScorerAugmentedFrame
                        {
                            Frame = f,
                            AllFramesTouchedInOtherSessions = AllFramesTouchedInOtherSessions,
                            AllScores = AllScores
                        }).ToArray();
            }
        }

        public void Export(string destPath)
        {
            if (scores.Count == 0)
                return;

            StringBuilder exportString = new StringBuilder();
            var last = scores.Last();

            foreach (var s in scores)
            {
                exportString.Append(s.X);
                exportString.Append(",");
                exportString.Append(s.Y);
                exportString.Append(",");
                exportString.Append(s.Class);
                if (s.SpotMeasurements != null && s.SpotMeasurements.Length > 0)
                {
                    exportString.Append(",");
                    exportString.Append(s.SpotMeasurements);
                }

                if(s != last)
                    exportString.Append(Environment.NewLine);
            }

            using(StreamWriter writer = new StreamWriter(destPath))
            {
                writer.Write(exportString.ToString());
                writer.Flush();
                writer.Close();
            }
        }
    }



    
}
