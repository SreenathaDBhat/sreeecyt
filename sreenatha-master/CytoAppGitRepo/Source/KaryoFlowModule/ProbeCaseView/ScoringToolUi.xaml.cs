﻿using System;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Globalization;
using System.Windows.Data;
using System.Threading;
using System.ComponentModel;
using System.Windows.Threading;

namespace AI
{
    public partial class ScoringToolUi : UserControl, INotifyPropertyChanged, IClosing
    {
        private ScoringTool tool;
        private IEnumerable<SlidingDrawer> popups;
        private string caseStatus = string.Empty;
        private bool firstTime = true;

        public event PropertyChangedEventHandler PropertyChanged;


        public ScoringToolUi(ScoringTool tool)
        {
            this.tool = tool;
            DataContext = tool;

            ImageFrame frameWithMostChannels = tool.CurrentFrame;
            int mostChannels=0;
            foreach (ImageFrame f in tool.Frames)
            {
                if (f.Channels.Count() > mostChannels)
                {
                    mostChannels = f.Channels.Count();
                    frameWithMostChannels = f;
                }
            }

            InitializeComponent();

            frameImage.HistogramEstimator = new InterphaseHistogramEstimator();
            frameImage.AvailableFilters = FilterTypes.FluorescentTypes;

            popups = new SlidingDrawer[]
            {
                channelPopup,
                resultsPopup,
                annotationsPopup
            };

            annotationsDisplay.ParentId = tool.CurrentFrame.Id;
            annotationsDisplay.Controller.Show = tool.ShowAnnotations;

            ThreadPool.QueueUserWorkItem(delegate
            {
                tool.LoadThumbs(Dispatcher);
            });


            Loaded += (s, e) => 
            {
                Keyboard.Focus(this);

                frameImage.Initialize();

                if (!firstTime)
                {
                    frameImage.RefreshImage();
                }

                firstTime = false;

                frameImage.Render();
				tool.CurrentFrameChanging += CurrentFrameChanging;
                tool.CurrentFrameChanged += CurrentFrameChanged;
            };
            Unloaded += (s, e) =>
            {
				tool.CurrentFrameChanging -= CurrentFrameChanging;
                tool.CurrentFrameChanged -= CurrentFrameChanged;
                frameImage.Release();
            };
        }

		private void CurrentFrameChanging(object sender, CurrentFrameChangingEventArgs e)
		{
			if (annotationsDisplay.IsModified)
			{
				e.Cancel = !SaveAnnotations();
			}
		}

        private void CurrentFrameChanged( object sender, EventArgs e)
        {
           //RenderParameters.ApplyChannelParametersTo(tool.CurrentFrame);
            annotationsDisplay.CaseId = tool.CaseId;
            annotationsDisplay.ParentId = tool.CurrentFrame.Id;
            PropertyChanged(this, new PropertyChangedEventArgs("Image"));
        }

        public ImageFrame Image
        {
            get { return tool.CurrentFrame; }
        }

        public FrameworkElement UiRoot
        {
            get { return this; }
        }

        public void MakeCurrent()
        {
            frameImage.Render();
        }

        private void ShowPopupOrClose(SlidingDrawer popup)
        {
            foreach (var p in popups)
            {
                if (p != popup && p.IsOpen)
                    p.IsOpen = false;
            }
        }

        private void ToggleChannelPopup(object sender, RoutedEventArgs e)
        {
            ShowPopupOrClose(channelPopup);
        }

        private void ToggleResultsPopup(object sender, RoutedEventArgs e)
        {
            ShowPopupOrClose(resultsPopup);
        }

        private void ToggleAnnotationPopup(object sender, RoutedEventArgs e)
        {
            ShowPopupOrClose(annotationsPopup);
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            if (tool.IsModified)
            {
                cancelMessage.IsOpen = true;
            }
            else
            {
                UIStack.For(this).Pop(UIStackResult.Cancel);
            }
        }

        private void OnCancelMessageOk(object sender, RoutedEventArgs e)
        {
            UIStack.For(this).Pop(UIStackResult.Cancel);
        }

        private void OnCancelMessageCancel(object sender, RoutedEventArgs e)
        {
            cancelMessage.IsOpen = false;
        }


        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (tool.CurrentAssay != null)
            {
                Point mousePos = Mouse.GetPosition(countsItemsControl);
                mousePos = new Point(mousePos.X, mousePos.Y);

                if (e.Key == Key.System)
                    e.Handled = tool.AddScoreUsingShortcut(e.SystemKey, mousePos, frameImage);
                else
                    e.Handled = tool.AddScoreUsingShortcut(e.Key, mousePos, frameImage);
            }

            if (e.Handled == false)
                base.OnKeyDown(e);
            else
                Keyboard.Focus(this);
        }

        private void OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                e.Handled = true;
                zStacker.Controller.MoveZ(Math.Sign(e.Delta));
            }
            else
            {
                e.Handled = false;
            }
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            savingMessage.Visibility = Visibility.Visible;
            IsEnabled = false;

            ThreadPool.QueueUserWorkItem(delegate(object state)
            {
                Closed();
				Dispatcher.Invoke((ThreadStart)delegate
				{
					UIStack.For(this).Pop(UIStackResult.Ok);

				}, DispatcherPriority.Normal);
            });
        }

        private void OnAnnotationLeftMouseButtonDown(object sender, MouseButtonEventArgs e)
        {
            annotationsDisplay.PlacementHelper.StartPlacingAnnotation(sender, e, frameImage);

            if (e.Handled)
                return;

            Keyboard.Focus(this);
        }

        private void OnAnnotationMouseMove(object sender, MouseEventArgs e)
        {
            annotationsDisplay.PlacementHelper.MoveAnnotationBeingPlaced(sender, e, frameImage);
        }

        private void OnAnnotationLeftMouseButtonUp(object sender, MouseButtonEventArgs e)
        {
            annotationsDisplay.PlacementHelper.FinishPlacingAnnotation(sender, e, frameImage);
        }

        private void DeleteScore(object sender, MouseButtonEventArgs e)
        {
            ProbeScore score = (ProbeScore)((FrameworkElement)sender).Tag;
            tool.DeleteScore(score);
        }

        private void CanToggleScoresExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = tool != null;
        }

        private void ToggleScoresExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            tool.ShowAllScores = !tool.ShowAllScores;
        }

        private void CanClearScoresFromFrameExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = tool != null && tool.ScoresForCurrentFrame.Count() > 0;
        }

        private void ClearScoresFromFrameExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            tool.DeleteScoresForFrame(tool.CurrentFrame);
        }

        private void CanClearScoresFromSlideExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = tool != null && tool.ScoresForCurrentSlide.Count() > 0;
        }

        private void ClearScoresFromSlideExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            tool.DeleteScoresForSlide();
        }

        private void OnSetCircleSize(object sender, RoutedEventArgs e)
        {
            if (tool != null)
            {
                double size = (double)((FrameworkElement)sender).Tag;
                tool.ScoreSize = size;
            }
        }

        private void CanPrintExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = tool != null;
        }

        private void PrintExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                PrintDialog dlg = new PrintDialog();

                if ((bool)dlg.ShowDialog().GetValueOrDefault())
                {
                    double aspect = printVisual.ActualHeight / printVisual.ActualWidth;

                    RenderTargetBitmap render = new RenderTargetBitmap(
                       (int)(printVisual.ActualWidth),
                       (int)(printVisual.ActualHeight),
                       96, 96, PixelFormats.Pbgra32);

                    render.Render(printVisual);

                    CroppedBitmap bmp = new CroppedBitmap(render, new Int32Rect(0, 0, render.PixelWidth, render.PixelHeight));
                    Image img = new Image();
                    img.Source = bmp;
                    
                    TextBlock text = new TextBlock();
                    text.FontSize = 18;
                    text.FontWeight = FontWeights.Bold;
                    text.Text = string.Format(CultureInfo.CurrentCulture, "{0}, {1}", tool.CaseName, tool.Slide.Name);
                    text.Margin = new Thickness(0, 0, 0, 20);

                    TextBlock dateText = new TextBlock();
                    dateText.FontSize = 18;
                    dateText.FontWeight = FontWeights.Bold;
                    dateText.Text = DateTime.Now.ToLongDateString();
                    dateText.Margin = new Thickness(0, 0, 0, 20);
                    DockPanel.SetDock(dateText, Dock.Right);


                    DockPanel topPanel = new DockPanel();
                    topPanel.Children.Add(dateText);
                    topPanel.Children.Add(text);
                    DockPanel.SetDock(topPanel, Dock.Top);

                    DockPanel dock = new DockPanel();
                    dock.Children.Add(topPanel);
                    dock.Children.Add(img);
                    dock.Margin = new Thickness(50);
                    dock.Width = dlg.PrintableAreaWidth - (dock.Margin.Left + dock.Margin.Right);
                    dock.Height = dock.Width * aspect;

                    dock.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                    dock.Arrange(new Rect(new Point(0, 0), dock.DesiredSize));

                    dlg.PrintVisual(dock, "Print");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Print failed", "Print", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void CanNextFrameExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = tool != null && tool.CanNextFrameExecute;
        }

        private void CanPrevFrameExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = tool != null && tool.CanPrevFrameExecute;
        }

        private void NextFrameExecute(object sender, ExecutedRoutedEventArgs e)
        {
			tool.MoveToNextFrame();
        }

        private void PrevFrameExecute(object sender, ExecutedRoutedEventArgs e)
        {
			tool.MoveToPrevFrame();
        }

        private void OnSelectAssay(object sender, RoutedEventArgs e)
        {
            if (tool.ScoresForCurrentSlide.Count() > 0)
            {
                loseScoresMessage.IsOpen = true;
            }
            else
            {
                SelectAssay();
            }
        }

        private void OnEditAssay(object sender, RoutedEventArgs e)
        {
            ProbeScoringAssay original = tool.CurrentAssay.Duplicate(false);
            
            AssayEditorWindow editor = new AssayEditorWindow(tool.CurrentAssay, tool.ScoresForCurrentSlide);

            editor.Owner = VisualTreeWalker.FindParentOfType<Window>(this);

            if (editor.ShowDialog().GetValueOrDefault(false))
            {
                tool.CurrentAssay = editor.Assay;
            }
            else
            {
                tool.CurrentAssay = original;
            }
        }

        private void OnLoseScoresOk(object sender, RoutedEventArgs e)
        {
            loseScoresMessage.IsOpen = false;
            SelectAssay();
        }

        private void SelectAssay()
        {
            tool.RefreshAssays();
            AssaySelector selector = new AssaySelector(tool.Assays);
            selector.Owner = VisualTreeWalker.FindParentOfType<Window>(this);
            if (selector.ShowDialog().GetValueOrDefault(false) && selector.SelectedAssay != null)
            {
                tool.DeleteScoresForSlide();
                tool.CurrentAssay = selector.SelectedAssay.Duplicate(false);
            }
        }

        private void OnLoseScoresCancel(object sender, RoutedEventArgs e)
        {
            loseScoresMessage.IsOpen = false;
        }

		public void Closed()
		{
			tool.WaitForImageWrites();
			annotationsDisplay.Controller.Save(tool.CaseId);
			tool.SaveData();
		}

		private bool SaveAnnotations()
		{
			bool saveOk = false;

			try
			{
				annotationsDisplay.Controller.Save(tool.CaseId);
				saveOk = true;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message + "\n\n" + ex.StackTrace, "Save annotations error");
			}

			return saveOk;
		}

		private void CanToggleAnnotations(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = annotationsDisplay != null && annotationsDisplay.Controller != null;
		}

		private void ToggleAnnotationsExecuted(object sender, ExecutedRoutedEventArgs e)
		{
			annotationsDisplay.Controller.Show = !annotationsDisplay.Controller.Show;
		}

        private void OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            //  If control with focus is editable then dont process the keypress
            UIElement elementWithFocus = Keyboard.FocusedElement as UIElement;
            if (elementWithFocus != null)
            {
                string elementType = elementWithFocus.GetType().ToString();
                if ((elementType == "System.Windows.Controls.TextBox") || (elementType == "System.Windows.Controls.RichTextBox"))
                {
                    e.Handled = false;
                    return;
                }
            }

            if (e.Key == Key.Space)
            {
                OnShowFrameSelection(null, null);
                e.Handled = true;
                return;
            }

            e.Handled = false;
        }

        private void OnShowFrameSelection(object sender, RoutedEventArgs e)
        {
            FrameSelectionWindow w = new FrameSelectionWindow(tool, (DataTemplate)FindResource("frameSelectionItemTemplate"), tool.CurrentFrame);
            if (w.ShowDialog().GetValueOrDefault(false))
            {
                tool.CurrentFrame = w.SelectedFrame;
            }
        }
	}


    public class ScoreVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool showAll = values[0] == DependencyProperty.UnsetValue ? false : (bool)values[0];
            bool mouseOver = values[1] == DependencyProperty.UnsetValue ? false : (bool)values[1];

            return (showAll || mouseOver) ? Visibility.Visible : Visibility.Hidden;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    internal class SaveChangesHandler
    {
        public RoutedEventHandler Save { get; set; }
        public RoutedEventHandler Discard { get; set; }
        public RoutedEventHandler Cancel { get; set; }
    }
}
