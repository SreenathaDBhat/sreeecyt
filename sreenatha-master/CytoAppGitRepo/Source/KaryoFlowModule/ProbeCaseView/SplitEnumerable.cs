﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI
{
    public static class SplitEnumerable
    {
        public static IEnumerable<T[]> Split<T>(this IEnumerable<T> input, int bitSize)
        {
            List<T> list = new List<T>();
            List<T[]> lists = new List<T[]>();

            foreach (var i in input)
            {
                list.Add(i);

                if (list.Count == bitSize)
                {
                    lists.Add(list.ToArray());
                    list = new List<T>();
                }
            }

            if (lists.Count == 0 && list.Count > 0)
                lists.Add(list.ToArray());

            return lists;
        }
    }
}
