﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;

namespace AI
{
    /// <summary>
    /// Interaction logic for ZStackControl.xaml
    /// </summary>
    public partial class ZStackControl : UserControl
    {
        public static DependencyProperty ImageFrameProperty = DependencyProperty.Register("ImageFrame", typeof(ImageFrame), typeof(ZStackControl), new FrameworkPropertyMetadata(OnImageChanged));
        public static DependencyProperty ImageRendererProperty = DependencyProperty.Register("ImageRenderer", typeof(ImageFrameRenderer), typeof(ZStackControl), new FrameworkPropertyMetadata(OnImageRendererChanged));

        ZStackController controller;

        public ZStackControl()
        {
            controller = new ZStackController();
            DataContext = controller;
            InitializeComponent();

            Unloaded += (s, e) => controller.StopAnimating();
        }

        public ZStackController Controller
        {
            get { return controller; }
        }

        public ImageFrame ImageFrame
        {
            get { return (ImageFrame)GetValue(ImageFrameProperty); }
            set { SetValue(ImageFrameProperty, value); }
        }

        public ImageFrameRenderer ImageRenderer
        {
            get { return (ImageFrameRenderer)GetValue(ImageRendererProperty); }
            set { SetValue(ImageRendererProperty, value); }
        }

        private static void OnImageChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((ZStackControl)sender).Controller.ImageFrame = (ImageFrame)e.NewValue;
        }

        private static void OnImageRendererChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((ZStackControl)sender).Controller.ImageRenderer = (ImageFrameRenderer)e.NewValue;
        }
    }


    public class ZStackController : Notifier
    {
        private bool animating;
        private bool projection;
        private ImageFrameRenderer renderer;
        private ImageFrame frame;
        private Thread animatorThread;
        private int direction;

        public ZStackController()
        {
            animating = false;
            projection = true;
            direction = 1;
        }

        public bool IsAnimating
        {
            get { return animating; }
            set 
            { 
                animating = value; 
                Notify("IsAnimating");

                if (animating)
                    StartAnimating(Dispatcher.CurrentDispatcher);
                else
                    StopAnimating();
            }

        }

        public bool IsProjection
        {
            get { return projection; }
            set 
            { 
                projection = value; 
                Notify("IsProjection");

                if (projection)
                {
                    IsAnimating = false;
                    renderer.ZStackIndex = -1;
                }
                else
                {
                    renderer.ZStackIndex = frame.StackSize / 2;
                }

                Notify("ZDouble");
            }
        }

        public double ZDouble
        {
            get { return ImageRenderer == null ? 0 : ImageRenderer.ZStackIndex; }
            set { ImageRenderer.ZStackIndex = (int)value; }
        }

        public ImageFrameRenderer ImageRenderer
        {
            get { return renderer; }
            set { renderer = value; }
        }

        public ImageFrame ImageFrame
        {
            get { return frame; }
            set { frame = value; Notify("ImageFrame"); }
        }

        public void StopAnimating()
        {
            if (animatorThread != null)
            {
                animatorThread.Abort();
                animatorThread.Join(1000);
                animatorThread = null;
            }
        }

        private void StartAnimating(Dispatcher dispatcher)
        {
            if (animatorThread != null)
            {
                throw new InvalidOperationException("animator already running");
            }

            animatorThread = new Thread(() =>
            {
                while (true)
                {
                    dispatcher.Invoke((ThreadStart)delegate
                    {
                        int newIndex = renderer.ZStackIndex + direction;
                        if (renderer.ZStackIndex <= 0)
                            direction = 1;
                        else if (renderer.ZStackIndex >= renderer.ImageFrameX.StackSizeMinusOne)
                            direction = -1;

                        renderer.ZStackIndex += direction;

                        Notify("ZDouble");

                    }, DispatcherPriority.Normal);

                    Thread.Sleep(200);
                }
            });

            animatorThread.Start();
        }

        public void MoveZ(int d)
        {
            ImageRenderer.ZStackIndex = Math.Max(0, Math.Min(ImageFrame.StackSizeMinusOne, ImageRenderer.ZStackIndex + d));
            Notify("ZDouble");
        }
    }
}
