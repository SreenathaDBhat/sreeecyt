﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using System.Threading;
using System.Windows.Threading;
using System.IO;
using AI.Connect;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace AI
{
    public sealed class PyramidGenerator
    {
        private const int DefaultSubdivisionLevels = 2;
        private const int TilePixelSize = 512;

        #region Implementation Detail
        private class InputFrame
        {
            public ImageFrame Frame { get; set; }
            public string FrameOnDisk { get; set; }
            public Point CenterPoint { get; set; }
            public Size Scale { get; set; }
            public Size PixelDimensions { get; set; }
            public Rect PositionRectIdeal
            {
                get
                {
                    double w = PixelDimensions.Width * Scale.Width;
                    double h = PixelDimensions.Height * Scale.Height;
                    return new Rect(CenterPoint.X - (w / 2), CenterPoint.Y - (h / 2), w, h);
                }
            }
        }
        private class Tile
        {
            public Tile(int level, Rect position, int maxSubdivisions)
            {
                Level = level;
                PositionRect = position;

                if (level < maxSubdivisions)
                {
                    double half = position.Width / 2;

                    Children = new Tile[4]
                    {
                        new Tile(level + 1, new Rect(position.X, position.Y, half, half), maxSubdivisions),
                        new Tile(level + 1, new Rect(position.X + half, position.Y, half, half), maxSubdivisions),
                        new Tile(level + 1, new Rect(position.X, position.Y + half, half, half), maxSubdivisions),
                        new Tile(level + 1, new Rect(position.X + half, position.Y + half, half, half), maxSubdivisions)
                    };
                }
            }

            public void CreateTargetImage()
            {
                Image = new RenderTargetBitmap(TilePixelSize, TilePixelSize, 96, 96, PixelFormats.Pbgra32);
            }

            public Rect PositionRect { get; set; }
            public RenderTargetBitmap Image { get; set; }
            public bool Used { get; set; }
            public Tile[] Children { get; set; }
            public bool GotChildren { get { return Children != null; } }
            public int Level { get; set; }
        }
        #endregion

        private BlockingCollection<InputFrame> inputFrames;
        private Tile[] tiles;
        private Guid caseId, slideId;
        private int complete;

        public event EventHandler Completed;


        public PyramidGenerator(Guid caseId, Guid slideId) : this(caseId, slideId, true)
        {
        }

        public PyramidGenerator(Guid caseId, Guid slideId, int subdivisionLevels)
            : this(caseId, slideId, subdivisionLevels, true)
        {
        }

        public PyramidGenerator(Guid caseId, Guid slideId, bool startProcessingAsynchronously)
            : this(caseId, slideId, DefaultSubdivisionLevels, startProcessingAsynchronously)
        {
        }

        public PyramidGenerator(Guid caseId, Guid slideId, bool startProcessingAsynchronously, int subdivisionLevels)
            : this(caseId, slideId, subdivisionLevels, startProcessingAsynchronously)
        {
        }

        public PyramidGenerator(Guid caseId, Guid slideId, int subdivisionLevels, bool startProcessingAsynchronously)
        {
            this.caseId = caseId;
            this.slideId = slideId;
            inputFrames = new BlockingCollection<InputFrame>();

            tiles = new Tile[3]
            {
                new Tile(0, new Rect(0, 0, 25000, 25000), subdivisionLevels),
                new Tile(0, new Rect(0, 25000, 25000, 25000), subdivisionLevels),
                new Tile(0, new Rect(0, 50000, 25000, 25000), subdivisionLevels)
            };

            if (startProcessingAsynchronously == true)
                System.Threading.Tasks.Task.Factory.StartNew(BuilderThread, null);
        }

        public void AddFrame(string frameFile, Point centerPoint, Size pixelDimensions, Size scale)
        {
            inputFrames.Add(new InputFrame
            {
                CenterPoint = centerPoint,
                FrameOnDisk = frameFile,
                Scale = scale,
                PixelDimensions = pixelDimensions
            });
        }

        public void AddFrame(ImageFrame frame, Point centerPoint, Size pixelDimensions, Size scale)
        {
            inputFrames.Add(new InputFrame
            {
                CenterPoint = centerPoint,
                Frame = frame,
                Scale = scale,
                PixelDimensions = pixelDimensions
            });
        }

        public void NoMoreFrames()
        {
            inputFrames.CompleteAdding();
        }

        public void ProcessFramesSynchronous()
        {
            ProcessFrames(inputFrames);
        }

        private void ProcessFrames(IEnumerable<InputFrame> frames)
        {
            foreach (var frame in frames)
            {
                RenderFrame(frame);
            }

            foreach (var t in tiles)
            {
                Freeze(t);
            }

            Save();
        }
     
        private void BuilderThread(object state)
        {
            ProcessFrames(inputFrames.GetConsumingEnumerable());
            Interlocked.Increment(ref complete);

            if(Completed != null)
            {
                Completed(this, EventArgs.Empty);
            }
        }

        private void Freeze(Tile tile)
        {
            if (tile.Used)
            {
                tile.Image.Freeze();

                if (tile.GotChildren)
                {
                    foreach (var t in tile.Children)
                        Freeze(t);
                }
            }
        }


        private void RenderFrame(InputFrame frame)
        {
            BitmapSource img;

            if (frame.Frame != null)
            {
                img = RGBImageComposer.Compose(frame.Frame, 0);
                frame.Frame = null; // let it die, dont need it anymore
            }
            else
            {
                var bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.CacheOption = BitmapCacheOption.OnLoad;
                bmp.UriSource = new Uri(frame.FrameOnDisk);
                bmp.EndInit();
                img = bmp;
            }

            RenderIntoTile(img, frame, tiles[0]);
            RenderIntoTile(img, frame, tiles[1]);
            RenderIntoTile(img, frame, tiles[2]);
        }

        private void RenderIntoTile(ImageSource frameImage, InputFrame frame, Tile tile)
        {
 	        if (tile.PositionRect.IntersectsWith(frame.PositionRectIdeal))
            {
                if (tile.Image == null)
                    tile.CreateTargetImage();

                tile.Used = true;
                Render(frameImage, frame, tile);

                if(tile.GotChildren)
                {
                    foreach(var t in tile.Children)
                        RenderIntoTile(frameImage, frame, t);
                }
            }
        }

        private void Render(ImageSource frameImage, InputFrame frame, Tile tile)
        {
            double convertScaleX = TilePixelSize / tile.PositionRect.Width;
            double convertScaleY = TilePixelSize / tile.PositionRect.Height;

            DrawingVisual visual = new DrawingVisual();
            using(var r = visual.RenderOpen())
            {
                var frameRect = frame.PositionRectIdeal;
                double relativeX = frameRect.X - tile.PositionRect.X;
                double relativeY = frameRect.Y - tile.PositionRect.Y;
                relativeX *= convertScaleX;
                relativeY *= convertScaleY;

                if (frame.FrameOnDisk == null)
                {
                    Matrix m = new Matrix();
                    m.Scale(convertScaleX, convertScaleY);
                    m.RotateAt(180, (frame.PositionRectIdeal.Width / 2) * convertScaleX, (frame.PositionRectIdeal.Height / 2) * convertScaleY);
                    m.Translate(relativeX, relativeY);
                    r.PushTransform(new MatrixTransform(m));
                    r.DrawImage(frameImage, new Rect(0, 0, frame.PositionRectIdeal.Width, frame.PositionRectIdeal.Height));
                }
                else
                {
                    r.DrawImage(frameImage, new Rect(relativeX, relativeY, frame.PositionRectIdeal.Width * convertScaleX, frame.PositionRectIdeal.Height * convertScaleY));
                }
 	        }

            tile.Image.Render(visual);
        }

        public void Save()
        {
            var db = new Connect.Database();
            var existingTiles = db.SlideImageTiles.Where(c => c.slideId == slideId);

            db.SlideImageTiles.DeleteAllOnSubmit(existingTiles);
            db.SubmitChanges();

            Parallel.ForEach(tiles, tile =>
            {
                WriteTileToDatabase(new Connect.Database(), tile);
            });
        }

        private void WriteTileToDatabase(Connect.Database db, Tile tile)
        {
            if (!tile.Used)
                return;

            MemoryStream stream = new MemoryStream();

            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            //encoder.Frames.Add(BitmapFrame.Create(RemoveBlack(tile.Image)));
            encoder.Frames.Add(BitmapFrame.Create(tile.Image));
            encoder.QualityLevel = 75;
            encoder.Save(stream);

            stream.Flush();

            Guid tileGuid = Guid.NewGuid();
            string file = string.Format(tileGuid.ToString() + ".tile.jpg", tile.Level, tile.PositionRect.X, tile.PositionRect.Y);

            var dbTile = new Connect.SlideImageTile
            {
                tileId = tileGuid,
                positionH = tile.PositionRect.Height,
                positionW = tile.PositionRect.Width,
                positionX = tile.PositionRect.X,
                positionY = tile.PositionRect.Y,
                slideId = this.slideId,
                subdivisionLevel = (short)tile.Level,
                blobId = Connect.BlobStream.WriteBlobStream(caseId, file, stream, db)
            };

            db.SlideImageTiles.InsertOnSubmit(dbTile);
            db.SubmitChanges();

            if (tile.GotChildren)
            {
                foreach(var t in tile.Children)
                    WriteTileToDatabase(db, t);
            }
        }

        private unsafe BitmapSource RemoveBlack(RenderTargetBitmap renderTargetBitmap)
        {
            int len = renderTargetBitmap.PixelWidth * renderTargetBitmap.PixelHeight * 4;
            byte[] pixels = new byte[len];
            renderTargetBitmap.CopyPixels(pixels, renderTargetBitmap.PixelWidth * 4, 0);

            fixed (byte* pixelsPtr = &pixels[0])
            {
                byte* p = pixelsPtr;
                byte* end = pixelsPtr + len;

                for ( ; p < end; p += 4)
                {
                    int t = *p + *(p + 1) + *(p + 2);
                    if (t == 0)
                        *(p + 3) = 0;
                }
            }

            return BitmapSource.Create(renderTargetBitmap.PixelWidth, renderTargetBitmap.PixelHeight, 96, 96, PixelFormats.Pbgra32, null, pixels, renderTargetBitmap.PixelWidth * 4);
        }
    }
}
