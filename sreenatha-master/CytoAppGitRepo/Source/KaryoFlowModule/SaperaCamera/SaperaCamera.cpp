#include "stdafx.h"
#include "SaperaCamera.h"
using namespace System::Xml::Linq;

namespace Sapera
{
	#pragma warning(disable:4538)

	Camera::Camera(System::Windows::Threading::Dispatcher^ dispatcher) : BasicCamera(dispatcher)
	{
		isConnected = false;
		components = gcnew System::Collections::Generic::List<AI::Hardware::IHardwareComponent^>;
		components->Add(this);

		frameWidth = 0;
		frameHeight = 0;
		bitsPerPixel = 0;
		cameraName = "Sapera";
		configFilePath = "C:\\DALSA\\Sapera\\CamFiles\\User\\a10.ccf";
	}

	#pragma warning(default:4538)

	Camera::~Camera()
	{
		delete callbackContext;
		callbackContext = 0;
		Disconnect();
	}

	void GlobalXferCallback(SapXferCallbackInfo* pInfo)
	{
		gcroot<Sapera::Camera^>* pcam = (gcroot<Sapera::Camera^>*)pInfo->GetContext();
		(*pcam)->XferCallback(pInfo);
	}

	void Camera::Connect()
	{
		int serverCount = SapManager::GetServerCount(SapManager::ResourceAcq);
		if(serverCount == 0)
		{
			failureReason = FaultDescription::FromStrings("No sapera camera servers are running.", "SapManager.GetServerCount: 0");
			return;
		}

		SapLocation serverLocation = SapLocation(1, 0);

		int resourceCount = SapManager::GetResourceCount(1, SapManager::ResourceAcq);
		if(resourceCount == 0)
		{
			failureReason = FaultDescription::FromStrings("No sapera camera servers are running.", "SapManager.GetServerCount: 0");
			return;
		}

		if(!SapManager::IsResourceAvailable(serverLocation, 0))
		{
			failureReason = FaultDescription::FromStrings("Camera unavailable, already in use?", "SapManager.IsResourceAvailable: false");
			return;
		}

		callbackContext = new gcroot<Camera^>(this);

		System::IntPtr pathPtr = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(configFilePath);
		try
		{
			char* path = (char*)pathPtr.ToPointer();
			m_Acq = new SapAcquisition(serverLocation, path);
		}
		finally
		{		
			System::Runtime::InteropServices::Marshal::FreeHGlobal(pathPtr);
		}

		if(!m_Acq->Create())
		{
			failureReason = FaultDescription::FromStrings("Could not create acquisition interface. Check camera file is correct.", "SapAcquisition.Create: false");
			Disconnect();
			return;
		}

		m_Buffers = new SapBufferWithTrash(2, m_Acq);
		if(!m_Buffers->Create())
		{
			failureReason = FaultDescription::FromStrings("Could not create transfer buffers.", "SapBufferWithTrash.Create: false");
			Disconnect();
			return;
		}
		
		m_Buffers->Clear();

		m_Xfer = new SapAcqToBuf(m_Acq, m_Buffers, GlobalXferCallback, callbackContext);
		
		if(!m_Xfer->Create())
		{
			failureReason = FaultDescription::FromStrings("Could not create transfer.", "SapAcqToBuf.Create: false");
			Disconnect();
			return;
		}

		frameWidth = m_Buffers->GetWidth();
		frameHeight = m_Buffers->GetHeight();
		bitsPerPixel = m_Buffers->GetPixelDepth();
		
		int len = frameWidth * frameHeight;
		snap = gcnew array<unsigned short>(len);

		isConnected = m_Xfer->Grab() != 0;
		
		if(isConnected)
		{
			failureReason = nullptr;
		}
		else
		{
			failureReason = FaultDescription::FromStrings("Unknown error", "isConnected: false");
		}
	}

	void Camera::Disconnect()
	{
		if (m_Xfer && *m_Xfer) 
			m_Xfer->Destroy();

		if (m_Buffers && *m_Buffers) 
			m_Buffers->Destroy();

		if (m_Acq && *m_Acq) 
			m_Acq->Destroy();

		delete m_Xfer; 
		m_Xfer = 0;
		
		delete m_Buffers; 
		m_Buffers = 0;
		
		delete m_Acq; 
		m_Acq = 0;
	}

	void Camera::XferCallback(SapXferCallbackInfo* info)
	{
		if(!IsStreaming)
			return;

		void* data = 0;
		if(!m_Buffers->GetAddress(&data))
			return;

		int len = frameWidth * frameHeight;
		pin_ptr<unsigned short> p = &snap[0];
		m_Buffers[0].GetAddress((void**)&data);

		if(bitsPerPixel == 8)
		{
			unsigned char* buf = (unsigned char*)data;
			for(int i = 0; i < len; ++i)
			{
				*p++ = *buf++;
			}
		}
		else
		{
			memcpy(p, data, len * 2);
		}

		HandleFrame(snap);
	}

	System::Collections::Generic::IEnumerator<AI::Hardware::IHardwareComponent^>^ Camera::GetEnumerator()
	{
		return components->GetEnumerator();
	}

	bool Camera::GetIsConnected()
	{
		return isConnected;
	}

	System::Xml::Linq::XElement^ Camera::SaveCofiguration()
	{
		XElement^ root = gcnew XElement("Config");

		XAttribute^ configPathNode = gcnew XAttribute("ConfigFile", ConfigFilePath);
		root->Add(configPathNode);

		return root;
	}

	void Camera::LoadConfiguration(System::Xml::Linq::XElement^ configNode)
	{
		XAttribute^ configPathNode = configNode->Attribute("ConfigFile");
		if(configPathNode != nullptr)
		{
			ConfigFilePath = configPathNode->Value;
		}
	}
}