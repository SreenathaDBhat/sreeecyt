#pragma once
using namespace System;
using namespace System::Windows::Threading;
using namespace AI::Hardware;
#include <vcclr.h>

namespace Sapera
{
	[Device("Sapera Compatible Camera")]
	[ConfigurationUi("SaperaCameraConfig", "SaperaCameraConfigUi.dll")]
	public ref class Camera : public BasicCamera
	{
	public:
		Camera(System::Windows::Threading::Dispatcher^ dispatcher);
		virtual ~Camera();

		virtual void Connect() override;
		void Disconnect();
		virtual property bool IsConnected { bool get() override { return GetIsConnected(); } }
		virtual property FaultDescription^ FailureReason { FaultDescription^ get() override { return failureReason; } }

		virtual property System::String^ Name { System::String^ get() override { return cameraName; } }
		virtual property int FrameWidth { int get() override { return frameWidth; } }
		virtual property int FrameHeight { int get() override { return frameHeight; } }
		virtual property int BitsPerPixel { int get() override { return bitsPerPixel; } }
        
		virtual System::Collections::Generic::IEnumerator<AI::Hardware::IHardwareComponent^>^ GetEnumerator() override;
		virtual System::Xml::Linq::XElement^ SaveCofiguration() override;
        virtual void LoadConfiguration(System::Xml::Linq::XElement^ deviceNode) override;

		property System::String^ ConfigFilePath
		{
			System::String^ get() { return configFilePath; }
			void set(System::String^ path) { configFilePath = path; }
		}

	internal:
		void XferCallback(SapXferCallbackInfo* info);

	private:
		FaultDescription^ failureReason;

		System::Collections::Generic::List<IHardwareComponent^>^ components;

		SapAcquisition* m_Acq;
		SapBuffer* m_Buffers;
		SapTransfer* m_Xfer;
		gcroot<Camera^>* callbackContext;
		array<unsigned short>^ snap;

		int frameWidth;
		int frameHeight;
		int bitsPerPixel;
		bool isConnected;

		System::String^ cameraName;
		System::String^ configFilePath;

		bool GetIsConnected();
	};
}
