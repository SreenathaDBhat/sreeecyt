﻿using System;
using AI.Hardware;
using Microsoft.Win32;

namespace Sapera
{
    public partial class SaperaCameraConfig
    {
        private Sapera.Camera cam;

        public SaperaCameraConfig(IHardwareDevice cam)
        {
            this.cam = (Sapera.Camera)cam;
            DataContext = cam;

            InitializeComponent();
        }

        private void OnBrowse(object sender, System.Windows.RoutedEventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.RestoreDirectory = true;
            fd.Filter = "Camera Files (*.ccf)|*.ccf|All Files|*.*";
            if (fd.ShowDialog().GetValueOrDefault(false))
            {
                cam.ConfigFilePath = fd.FileName;
                configTextBox.Text = fd.FileName;
            }
        }
    }
}
