﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.Security
{
    public class FlagViewModel : ViewModelBase
    {
        private int id = -1;
        private string flagname = string.Empty;
        private Boolean defaultValue = false;

        internal int ID
        { get { return this.id; } }

        public string FlagName
        {
            get { return flagname; }

            set
            {
                if (value != flagname)
                {
                    flagname = value;
                    base.OnPropertyChanged("FlagName");
                }
            }
        }

        public Boolean DefaultValue
        {
            get { return defaultValue; }

            set
            {
                if (value != defaultValue)
                {
                    defaultValue = value;
                    base.OnPropertyChanged("DefaultValue");
                }
            }
        }

        internal FlagViewModel(int id, string flagname, Boolean defaultvalue)
        {
            this.DefaultValue = defaultvalue;
            this.FlagName = flagname;
            //  Set the ID last so
            //  that the code can 
            //  be assigned
            this.id = id;
        }

        public FlagViewModel(string flagname)
        {
            this.FlagName = flagname;
            this.DefaultValue = false;
            this.id = -1;
        }

        public FlagViewModel(string flagname, Boolean defaultvalue)
        {
            this.FlagName = flagname;
            this.DefaultValue = defaultvalue;
            this.id = -1;
        }

        protected override void DisposeManagedResources()
        {
        }

        protected override void DisposeUnManagedResources()
        {
        }
    }


}
