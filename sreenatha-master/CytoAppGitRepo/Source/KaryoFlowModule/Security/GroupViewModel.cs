﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.Security
{
    public class GroupViewModel : ViewModelBase
    {
        private int id = -1;
        private string name = string.Empty;

        internal int ID
        { get { return this.id; } }

        public string Name
        {
            get { return name; }

            set
            {
                if (value != name)
                {
                    name = value;
                    base.OnPropertyChanged("Name");
                }
            }
        }

        internal GroupViewModel(int id, string name)
        {
            this.Name = name;
            this.id = id;
        }

        public GroupViewModel(string name)
        {
            this.id = -1;
            this.Name = name;
        }

        protected override void DisposeManagedResources()
        {
        }

        protected override void DisposeUnManagedResources()
        {
        }
    }

}
