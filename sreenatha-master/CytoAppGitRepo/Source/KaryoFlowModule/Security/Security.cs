﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Data.Linq;
using System.Text;
using AI.Connect;
using AI.Logging;

namespace AI.Security
{
    /*
     *          S E C U R I T Y
     * 
     *  Main business object for accessing
     *  the security settings/access rights
     *  of system users. 
     *  
     */

    public static class Security 
    {
        #region  Constants

        private const int NOERROR = 0;
        private const int ERROR = 1;
        private const int GLOBALTYPE = 0;
        private const int FEATURETYPE = 1;
        private const string USERACCESSRIGHTS = "UserAccessRights";
        private const string USERLOGGING = "UserLogging";

        private const string TUBS = "Tubs";


        #endregion

        #region Variables

        private static Boolean _userLogging = false;
        private static Boolean _checkAccess = false;
        private static Boolean masterEnabled = false;

        private static Boolean loggedin = false;
        private static string username = string.Empty;
        private static string machinename = string.Empty;
        private static Guid sessionGuid = Guid.Empty;
        private static string app = string.Empty;

        private static AI.Logging.Logging logger = null;
        private static Boolean initialized = false;

        #endregion

        #region Properties

        public static Boolean IsAccessRightsEnabled
        {
            get { return _checkAccess; }
        }

        public static Boolean IsLoggingEnabled
        {
            get { return _userLogging; }
        }

        #endregion

        public static int Init()
        {
            username = Environment.UserName;
            machinename = Environment.MachineName;
            sessionGuid = Guid.NewGuid();

            app = System.IO.Path.GetFileName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

            if (app.Contains("."))
            {
                app = app.Substring(0, app.IndexOf("."));
            }

            switch (app.ToUpper())
            {
                case "ARCHIVEMAINTENANCE":
                    app = TUBS;
                    break;
                case "TUBS":
                    app = TUBS;
                    break;
            }

            int errCode = ReadGlobalSettings();

            if (errCode == ERROR)
            {
                //  Any errors then disable global options
                //  to allow application to continue running
                _userLogging = false;
                _checkAccess = false;

                masterEnabled = true;  // means any query about a users access always returns enabled
            }

            initialized = true;

            return errCode;
        }

        public static Guid SessionGuid
        {
            get { return sessionGuid; }
        }

        public static string UserName
        {
            get { return username; }
        }

        public static string MachineName
        {
            get { return machinename; }
        }
        
        private static int ReadGlobalSettings()
        {
            /*
             *  If any of the global settings
             *  do not exist then create them
             *  with a default value of disabled.
             *  Administrator has to explicitly
             *  set them.
             */ 

            int errCode = ERROR;
            Database db = null;

            try
            {

                db = new Database();

               int numRecs = db.Flags.Count();

                var flag = db.Flags.Where(f => f.FlagName == USERACCESSRIGHTS).FirstOrDefault();

                if (flag != null)
                    _checkAccess = (Boolean)flag.Enabled;
                else
                {
                    //  If record does not exist then
                    //  create it as a global setting
                    _checkAccess = false;
                    Connect.Flag f = new Connect.Flag();
                    f.FlagName = USERACCESSRIGHTS;
                    f.FlagType = GLOBALTYPE;
                    f.Enabled = _checkAccess;
                    db.Flags.InsertOnSubmit(f);
                    db.SubmitChanges();
                }


                flag = db.Flags.Where(f => f.FlagName == USERLOGGING).FirstOrDefault();

                if (flag != null)
                    _userLogging = (Boolean)flag.Enabled;
                else
                {
                    //  If record does not exist then
                    //  create it as a global setting
                    _userLogging = false;
                    Connect.Flag f = new Connect.Flag();
                    f.FlagName = USERLOGGING;
                    f.FlagType = GLOBALTYPE;
                    f.Enabled = _userLogging;
                    db.Flags.InsertOnSubmit(f);
                    db.SubmitChanges();
                }

                errCode = CheckDefaultStatuses();

                if (errCode == NOERROR)
                    errCode = CheckDefaultFlags();
            }
            catch (Exception)
            {
                _checkAccess = false;
                _userLogging = false;
                errCode = ERROR;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            //  One way or the other 
            //  we now have global settings

            if ((_userLogging) && (logger == null))
            {
                logger = new AI.Logging.Logging();
            }

            return errCode;
        }

        private static int CheckDefaultStatuses()
        {
            int errCode = ERROR;
            Database db = null;

            try
            {
                db = new Database();

                foreach (string s in DefaultStatus.Defaults)
                {
                    var rec = db.GetTable<Connect.Status>().Where(r => r.StatusCode == s).FirstOrDefault();

                    if (rec == null)
                    {
                        Connect.Status n = new Connect.Status();
                        n.StatusCode = s.ToString();
                        db.Status.InsertOnSubmit(n);
                    }
                }

                db.SubmitChanges();

                errCode = NOERROR;
            }
            catch
            {
                errCode = ERROR;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return errCode;
        }

        private static int CheckDefaultFlags()
        {
            int errCode = ERROR;
            Database db = null;

            try
            {
                db = new Database();

                //  Check there is an entry in the flags 
                //  table for each of the defaults
                foreach (string s in DefaultFlags.Defaults)
                {
                    Connect.Flag rec = db.GetTable<Connect.Flag>().Where(f => f.FlagName == s).FirstOrDefault();

                    if (rec == null)
                    {
                        Connect.Flag n = new Connect.Flag();
                        n.FlagType = FEATURETYPE;
                        n.FlagName = s.ToString();
                        n.Enabled = false;
                        db.Flags.InsertOnSubmit(n);

                    }
                }
                
                db.SubmitChanges();

                errCode = NOERROR;

            }
            catch
            {
                errCode = ERROR;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return errCode;
        }

        private static int ReadSecuritySettings()
        {
            int errCode = ReadGlobalSettings();

            if (errCode == ERROR)
            {
                //  Any errors then disable global options
                _userLogging = false;
                _checkAccess = false;

                masterEnabled = true;  // means any query about a users access always returns enabled
            }

            return errCode;
        }

        public static int Login()
        {
            int errCode = NOERROR;

            if (!initialized)
                errCode = Init();

            if (errCode == NOERROR)
            {
                errCode = AddLogEntry(EventType.Login, app);

                loggedin = (errCode == NOERROR);
            }

            return errCode;
        }

        public static int Logout()
        {
            int errCode = NOERROR;

            if (loggedin)
                errCode = AddLogEntry(EventType.Logout, app);

            if (errCode == NOERROR)
                loggedin = false;

            if (logger != null)
            {
                logger.Dispose();
                logger = null;
            }

            initialized = false;

            return errCode;
        }

        
        public static Boolean UserOptionEnabled(string userID, string status, string flag)
        {
            if (!initialized) Init();

            if (masterEnabled)
                return true;   //  No configuration data available

            if (!_checkAccess)
                return true;

            if ((userID.Length <= 0) || (status.Length <= 0) || (flag.Length <= 0))
                return false;

            Boolean enabled = false;
            Database db = null;

            try
            {
                db = new Database();

                // get the user record and determine if they are in a group
                var user = db.Users.Where(u => u.Name == userID).FirstOrDefault();

                if (user != null)
                {
                    string username = string.Empty;

                    if (user.GroupID != -1)
                    {
                        int groupid = ((int)user.GroupID);
                        user = db.Users.Where(u => u.ID == groupid).FirstOrDefault();

                        if (user != null)
                            username = user.Name;
                    }
                    else username = user.Name;

                    //  We now have the Name for the correct 
                    //  set of flags to query.
                    var flagrecord = db.UserStatusFlagViews.Where(ufv => (ufv.Name == username)
                                && (ufv.StatusCode == status) && (ufv.FlagName == flag)).FirstOrDefault();

                    if (flagrecord != null)
                        enabled = flagrecord.Enabled;
                }
            }
            catch
            {
                enabled = false;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return enabled;
        }

        public static int AddLogEntry( int eventType, Guid caseID, string caseStatus, string eventMsg)
        {
            int errCode = NOERROR;

            if (!initialized) 
                Init();

            if (_userLogging)
            {
               errCode = logger.AddLogEntry(UserName, MachineName, SessionGuid, eventType, caseID, caseStatus, eventMsg);
            }

            return errCode;
        }

        public static int AddLogEntry(int eventType)
        {
            return AddLogEntry(eventType, Guid.Empty, string.Empty, app);
        }
        
        public static int AddLogEntry(int eventType, string msg)
        {
            return AddLogEntry(eventType, Guid.Empty, string.Empty, msg);
        }

    }
}
