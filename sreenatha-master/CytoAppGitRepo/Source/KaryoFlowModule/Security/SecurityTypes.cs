﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.Security
{
    public class Constants
    {
        public const int TRUE = 1;
        public const int FALSE = 0;
        public const int NOERROR = 0;
        public const int ERROR = 1;
    }

    public static class DefaultStatus
    {
        public const string InProgress = "InProgress";
        public const string ForReview = "ForReview";
        public const string Completed = "Completed";
        public const string ToArchive = "ToArchive";
        public const string Archived = "Archived";

        public static string[] Defaults = { InProgress, ForReview, Completed, ToArchive, Archived };
    }
    
    public static class DefaultFlags
    {
        public const string OpenCase = "OpenCase";
        public const string Delete = "Delete";
        public const string CreateNewImage = "CreateNewImage";
        public const string ModifyChange = "ModifyChange";
        public const string AllowToSetFlags = "AllowToSetFlags";
        public const string Print = "Print";
        public const string Archive = "Archive";
        public const string CreateCase = "CreateCase";
        public const string DirectorsReview = "DirectorsReview";

        public static string[] Defaults = { OpenCase, Delete, CreateNewImage, ModifyChange, AllowToSetFlags,
                                            Print, Archive, CreateCase, DirectorsReview};
    
    }
}
