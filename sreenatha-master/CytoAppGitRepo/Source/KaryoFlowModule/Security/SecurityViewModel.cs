﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace AI.Security
{
    /*    All business logic retained in the Security class.
     *    The ViewModel is purely used for monitoring data
     *    state.
     */
    
    public class SecurityViewModel: ViewModelBase
    {
        private Security sbo = null;
        private Boolean _userLogging = false;
        private Boolean _checkAccess = false;

        private ObservableCollection<StatusViewModel> statuses = new ObservableCollection<StatusViewModel>();
        private ObservableCollection<GroupViewModel> groups = new ObservableCollection<GroupViewModel>();
        private ObservableCollection<UserViewModel> users = new ObservableCollection<UserViewModel>();
        private ObservableCollection<FlagViewModel> flags = new ObservableCollection<FlagViewModel>();

        private List<string> groupsToDelete = new List<string>();
        private List<string> usersToDelete = new List<string>();
        private List<string> statusesToDelete = new List<string>();
        private List<string> flagsToDelete = new List<string>();


        internal List<string> GroupsToDelete
        {
            get { return groupsToDelete; }
        }

        internal List<string> UsersToDelete
        {
            get { return usersToDelete; }
        }

        internal List<string> StatusesToDelete
        {
            get { return statusesToDelete; }
        }

        internal List<string> FlagsToDelete
        {
            get { return flagsToDelete; }
        }

        public Boolean IsLoggingEnabled
        {
            get { return _userLogging;}
            set
            {
                if (_userLogging != value)
                {
                    _userLogging = value;
                    base.OnPropertyChanged("IsLoggingEnabled");
                }
            }
        }

        public Boolean IsAccessRightsEnabled
        {
            get { return _checkAccess; }
            set
            {
                if (_checkAccess != value)
                {
                    _checkAccess = value;
                    base.OnPropertyChanged("IsAccessRightsEnabled");
                }
            }
        }

        public IEnumerable<StatusViewModel> Statuses
        {
            get { return statuses; }
            set
            {
                if (statuses != value)
                {
                    statuses.Clear();
                    statuses = null;
                    statuses = new ObservableCollection<StatusViewModel>(value.OrderBy(s => s.Code));
                    base.OnPropertyChanged("Statuses");
                }
            }
        }

        public IEnumerable<GroupViewModel> Groups
        {
            get { return groups; }
            set
            {
                if (groups != value)
                {
                    groups.Clear();
                    groups = null;
                    groups = new ObservableCollection<GroupViewModel>(value.OrderBy(g => g.Name));
                    base.OnPropertyChanged("Groups");
                }
            }
        }

        public IEnumerable<UserViewModel> Users
        {
            get { return users; }
            set
            {
                if (users != value)
                {
                    users.Clear();
                    users = null;
                    users = new ObservableCollection<UserViewModel>(value.OrderBy( u => u.Name));
                    base.OnPropertyChanged("Users");
                }
            }
        }

        public IEnumerable<FlagViewModel> Flags
        {
            get { return flags; }
            set
            {
                if (flags != value)
                {
                    flags.Clear();
                    flags = null;
                    flags = new ObservableCollection<FlagViewModel>(value.OrderBy( f => f.FlagName));
                    base.OnPropertyChanged("Flags");
                }
            }
        }

        // Can only be instantiated through request to Security Factory
        internal SecurityViewModel(Security securityBO)
        {
            sbo = securityBO;
        }

        protected override void DisposeManagedResources()
        {

        
        }

        protected override void DisposeUnManagedResources()
        {

        
        }

        public int SaveSettings()
        {
            return sbo.SaveSecurityViewModel(this);
        }

        public int AddGroup(string newGroup)
        {
            int errCode = Constants.ERROR;

            if (!GroupExists(newGroup))
            {
                // Ok to add new group
                groups.Add(new GroupViewModel(newGroup));

                groupsToDelete.Remove(newGroup);

                base.OnPropertyChanged("Groups");

                errCode = Constants.NOERROR;
            }

            return errCode;
        }

        public Boolean CanDeleteGroup(string group)
        {
            Boolean ok = true;

            if (groups.Count() > 0)
            {
                //  Check none of the users are assigned to the group
                var found = users.Where(g => g.MemberOfGroup == group).FirstOrDefault();

                if (found != null)
                {
                    ok = false;
                }
            }

            return ok;
        }
        
        public int DeleteGroup(string group)
        {
            int errCode = Constants.NOERROR;

            if (groups.Count() > 0)
            {
                var found = groups.Where(g => g.Name == group).FirstOrDefault();

                if (found != null)
                {
                    errCode = groups.Remove(found) ? Constants.NOERROR :  Constants.ERROR;

                    if (errCode == Constants.NOERROR)
                    {
                        groupsToDelete.Add(group);
                    }
                }
            }

            return errCode;
        }

        public int AddUser(string newUser, string memberofgroup)
        {
            int errCode = Constants.ERROR;

            if (!UserExists(newUser))
            {
                Boolean ok = true;

                if (memberofgroup.Length > 0)
                {
                    ok = GroupExists(memberofgroup);
                }
                
                // Ok to add new user
                if (ok)
                {
                    users.Add(new UserViewModel(newUser,memberofgroup));

                    usersToDelete.Remove(newUser);

                    base.OnPropertyChanged("Users");

                    errCode = Constants.NOERROR;
                }
            }

            return errCode;
        }

        public int DeleteUser(string user)
        {
            int errCode = Constants.NOERROR;

            if (users.Count() > 0)
            {
                var found = users.Where(g => g.Name == user).FirstOrDefault();

                if (found != null)
                {
                    errCode = users.Remove(found) ? Constants.NOERROR : Constants.ERROR;

                    if (errCode == Constants.NOERROR)
                    {
                        usersToDelete.Add(user);
                    }
                }
            }

            return errCode;
        }

        public int AddStatus(string newCode)
        {
            int errCode = Constants.ERROR;

            if (!StatusExists(newCode))
            {
                statuses.Add(new StatusViewModel(newCode));

                statusesToDelete.Remove(newCode);
                
                base.OnPropertyChanged("Statuses");

                errCode = Constants.NOERROR;
            }

            return errCode;
        }

        public int DeleteStatus(string status )
        {
            int errCode = Constants.NOERROR;

            if (statuses.Count() > 0)
            {
                var found = statuses.Where(g => g.Code == status).FirstOrDefault();

                if (found != null)
                {
                    errCode = statuses.Remove(found) ? Constants.NOERROR : Constants.ERROR;

                    if (errCode == Constants.NOERROR)
                    {
                        statusesToDelete.Add(status);
                    }
                }
            }

            return errCode;
        }

        public int AddFlag(string newFlag)
        {
            int errCode = Constants.ERROR;

            if (!FlagExists(newFlag))
            {
                flags.Add(new FlagViewModel(newFlag, false));

                flagsToDelete.Remove(newFlag);

                base.OnPropertyChanged("Flags");

                errCode = Constants.NOERROR;
            }

            return errCode;
        }

        public int DeleteFlag(string flag)
        {
            int errCode = Constants.NOERROR;

            if (flags.Count() > 0)
            {
                var found = flags.Where(g => g.FlagName == flag).FirstOrDefault();

                if (found != null)
                {
                    errCode = flags.Remove(found) ? Constants.NOERROR : Constants.ERROR;

                    if (errCode == Constants.NOERROR)
                    {
                        flagsToDelete.Add(flag);
                    }
                }
            }

            return errCode;
        }

        public Boolean GroupExists(string group)
        {
            if (groups.Count() > 0)
            {
                var found = groups.Where(g => g.Name == group).FirstOrDefault();

                if (found != null)
                    return true;
            }

            return false;
        }

        private Boolean UserExists(string user)
        {
            if (users.Count() > 0)
            {
                var found = users.Where(u => u.Name == user).FirstOrDefault();

                if (found != null)
                    return true;
            }

            return false;
        }

        private Boolean StatusExists(string status)
        {
            if (statuses.Count() > 0)
            {
                var found = statuses.Where(s => s.Code == status).FirstOrDefault();

                if (found != null)
                    return true;
            }

            return false;
        }

        private Boolean FlagExists(string flag)
        {
            if (flags.Count() > 0)
            {
                var found = flags.Where(f => f.FlagName == flag).FirstOrDefault();

                if (found != null)
                    return true;
            }
            return false;
        }

        public void Refresh()
        {
            sbo.RefreshSecurityViewModel();
        }
    }

}
