﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.Security
{
    public class StatusViewModel : ViewModelBase
    {
        private int id = -1;
        private string code = string.Empty;

        internal int ID
        { get { return this.id; } }
       
        public string Code
        {
            get { return code; }

            set
            {
                if (value != code)
                {
                    code = value;
                    base.OnPropertyChanged("Code");
                }
            }
        }

        internal StatusViewModel(int id, string code)
        {
            this.Code = code;

            //  Set the ID last so
            //  that the code can 
            //  be assigned
            this.id = id;
        }

        public StatusViewModel(string code)
        {
            this.Code = code;
            this.id = -1;
        }

        protected override void DisposeManagedResources()
        {
        }

        protected override void DisposeUnManagedResources()
        {
        }
    }
}
