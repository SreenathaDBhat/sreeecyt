﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using AI.Connect;

namespace AI.Security
{

    public class FlagOption : ViewModelBase
    {
        private int id = -1;
        private string flagname = string.Empty;
        private Boolean flagValue = false;
        private Boolean canBeModified = false;

        public int ID
        { get { return this.id; } }
        
        public string FlagName
        {
            get { return flagname; }
        }
        
        public Boolean Enabled
        {
            get { return flagValue; }

            set
            {
                if (value != flagValue)
                {
                    flagValue = value;
                    base.OnPropertyChanged("Enabled");
                 }
            }
        }

        public Boolean Modifyable
        { 
            get { return canBeModified; }

            internal set
            {
                canBeModified = value;
            }
        }

        internal FlagOption(int id, string flagname, Boolean initialValue, Boolean canmodify)
        {
            this.id = id;
            this.flagname = flagname;
            this.Enabled = initialValue;
            this.canBeModified = canmodify;
        }

        internal FlagOption(int id, string flagname, Boolean initialValue)
        {
            this.id = id;
            this.flagname = flagname;
            this.Enabled = initialValue;
            this.canBeModified = true;
        }

        protected override void DisposeManagedResources()
        {
        }

        protected override void DisposeUnManagedResources()
        {
        }
    }

    public class StatusCode : ViewModelBase
    {
        private int id = -1;
        private string status = string.Empty;

        private ObservableCollection<FlagOption> flagOptions = new ObservableCollection<FlagOption>();

        public int ID
        { get { return this.id; } }


        internal StatusCode(int id, string status)
        {
            this.status = status;
            this.id = id;
            CreateStatusFlags();
        }

        public string Status
        {
            get { return status; }
        }

        public IEnumerable<FlagOption> Flags
        {
            get { return flagOptions; }
            //set
            //{
            //    if (flagOptions != value)
            //    {
            //        flagOptions.Clear();
            //        flagOptions = null;
            //        flagOptions = new ObservableCollection<FlagOption>(value);
            //        base.OnPropertyChanged("Flags");
            //    }
            //}
        }

        private void CreateStatusFlags()
        {
            Database db = null;

            try
            {
                db = new Database();

                var flags = db.GetTable<Flag>().Where(f => f.FlagType == 1);

                if (flags.Count() > 0)
                {
                    foreach (Flag f in flags)
                    {
                        FlagOption flg = new FlagOption(f.ID, f.FlagName, ((Boolean)f.Enabled), true);
                        flagOptions.Add(flg);
                    }
                }
            }
            catch
            {
                flagOptions.Clear();
                flagOptions = null;
                flagOptions = new ObservableCollection<FlagOption>();
                base.OnPropertyChanged("Flags");
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }
        
        protected override void DisposeManagedResources()
        {
            flagOptions.Clear();
            flagOptions = null;
        }

        protected override void DisposeUnManagedResources()
        {
        }
    }

    public class UserStatusFlagsViewModel : ViewModelBase
    {
        private int id = -1;
        private string username = string.Empty;
        private string groupname = string.Empty;
        private Boolean isGroup = false;
        private int groupId = -1;
        private Boolean isDirty = false;

        private ObservableCollection<StatusCode> settings = new ObservableCollection<StatusCode>();

        public int ID
        { get { return this.id; } }

        public Boolean IsDirty
        {
            get { return isDirty; }
        }
        
        internal UserStatusFlagsViewModel(string username)
        {
            this.username = username;

            //  Now get the existing settings for this user
            GetExistingSettings();
        }
        
        public IEnumerable<StatusCode> Settings
        {
            get { return settings; }
            //set
            //{
            //    if (settings != value)
            //    {
            //        settings.Clear();
            //        settings = null;
            //        settings = new ObservableCollection<StatusCode>(value);
            //        base.OnPropertyChanged("Settings");
            //    }
            //}
        }

        public Boolean IsGroup
        {
            get { return isGroup; }

            set
            {
                if (value != isGroup)
                {
                    isGroup = value;
                    //  Need to get settings again
                    //  for the user
                    GetExistingSettings();
                }
            }
        }

        private void ClearUsersSettings()
        {
            if (settings != null)
            {
                foreach (StatusCode x in settings)
                {
                    x.Dispose();
                }

                settings.Clear();
            }
            else
                settings = new ObservableCollection<StatusCode>();
        }

        private int GetExistingSettings()
        {
            int errCode = Constants.ERROR;

            Database db = null;

            try
            {
                ClearUsersSettings();

                db = new Database();

                // Is this a new user record 
                var user = db.Users.Where(u => u.Name == this.username).FirstOrDefault();

                if (user != null)
                {
                    this.id = user.ID;
                    groupname = string.Empty;
                    isGroup = ((Boolean)user.GroupFlag);
                    groupId = ((int)user.GroupID);

                    //  Is this user assigned to group settings
                    if (groupId != -1)
                    {
                        user = db.Users.Where(u => u.ID == groupId).FirstOrDefault();

                        if ((user != null) && (user.ID == groupId))
                        {
                            //  user is a mamber of this group
                            groupname = user.Name;
                        }
                        else
                        {
                            //   No valid group record so reset
                            //  this value for next stage
                            groupId = -1;
                        }
                    }

                    if (groupId != -1)
                        // Getting the group settings to display
                        // so these can not be modified
                        GetStatusFlagSettings(groupId, false);
                    else
                        GetStatusFlagSettings(id, true);
                }
                else
                {
                    //  its a new user/group record
                    id = -1;
                    groupname = string.Empty;
                    isGroup = false;
                    groupId = -1;
                    ClearUsersSettings();
                }
            }
            catch
            {
                errCode = Constants.ERROR;
                ClearUsersSettings();
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return errCode;
        }

        public void Refresh()
        {
            GetExistingSettings();
        }

        private int GetStatusFlagSettings(int userid, Boolean modifyable)
        {
            int errCode = Constants.ERROR;

            Database db = null;

            try
            {
                db = new Database();

                var statuses = db.GetTable<Status>();

                if (statuses.Count() > 0)
                {

                    //  Create a record for every flag and status.
                    //  This is done because there may have been
                    //  new statuses or flags entered since the last 
                    //  time this users rights were ammended.
                    foreach (Status s in statuses)
                    {
                        settings.Add(new StatusCode(s.ID, s.StatusCode));
                    }

                    // now get all of the defaults so set the 
                    // enabled flags for the user

                    foreach (StatusCode f in settings)
                    {
                        foreach (FlagOption o in f.Flags)
                        {
                            var found = new List<FlagOption>(from uvf in db.UserStatusFlagViews
                                        where (uvf.ID == userid) && (uvf.StatusCode == f.Status) && (uvf.FlagName == o.FlagName)
                                                             select new FlagOption(uvf.ID, uvf.FlagName, uvf.Enabled));

                            if (found.Count() == 1)
                            {
                                o.Enabled = found.FirstOrDefault().Enabled;
                                o.Modifyable = modifyable;
                            }

                            o.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(FlagOption_PropertyChanged);                        
                        }                        
                    }
                
                }
                
                errCode = Constants.NOERROR;
            }
            catch
            {
                errCode = Constants.ERROR;
                ClearUsersSettings();
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }

                isDirty = false;
            }

            return errCode;
        }

        void FlagOption_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            isDirty = true;
            base.OnPropertyChanged("IsDirty");
        }

        protected override void  DisposeManagedResources()
        {
            ClearUsersSettings();
            settings = null;
        }

        protected override void  DisposeUnManagedResources()
        {
        }
    }
}
