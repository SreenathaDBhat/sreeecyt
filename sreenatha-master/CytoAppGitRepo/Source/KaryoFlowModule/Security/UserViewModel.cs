﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI.Security
{
    public class UserViewModel : ViewModelBase
    {
        private int id = -1;
        private string name = string.Empty;
        private string groupname = string.Empty;

        internal int ID 
        { get {return this.id;} }

        public string MemberOfGroup
        {
            get { return groupname; }

            set
            {
                if (value != groupname)
                {
                    groupname = value;
                    base.OnPropertyChanged("MemberOfGroup");
                }
            }
        }

        public string Name
        {
            get { return name; }
            
            set
            {
                if (value != name)
                {
                    name = value;
                    base.OnPropertyChanged("Name");
                }
            }
        }

        internal UserViewModel(int id, string name, string groupName)
        {
            this.Name = name;
            this.MemberOfGroup = groupName;
            this.id = id;
        }

        public UserViewModel(string name)
        {
            this.id = -1;
            this.Name = name;
            this.groupname = string.Empty;
        }

        public UserViewModel(string name, string memberof)
        {
            this.id = -1;
            this.Name = name;
            this.MemberOfGroup = memberof;
        }

        protected override void DisposeManagedResources()
        {
        }

        protected override void DisposeUnManagedResources()
        {
        }
    }
}
