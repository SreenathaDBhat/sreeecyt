﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace AI.Security
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.ExceptionObject.ToString());
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            bool useCulture = true;

#if DEBUG
            // In DEBUG dont use current culture use default en-US, so that xaml changes dont break execution.
            // Can be overridden by /L command line arguement if debugging languages but you must have language folders,
            // eg copy from build server Tubs/bin/release.
            useCulture = false;
            foreach (string arg in e.Args)
            {
                switch (arg.ToUpper())
                {
                    case "/L": useCulture = true; break;
                    default: break;
                }
            }
#endif
            if (useCulture)
            {
                // Control Panel "Regional and Language Options" applet sets CurrentCulture below BUT you have to run the 
                // app twice before it sees the change when running with the debugger - dont know why. If running without 
                // debugging it sees the change straight off.
                System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture;
            }

            Window1 wnd = new Window1();
            wnd.ShowDialog();
        }
    }
}
