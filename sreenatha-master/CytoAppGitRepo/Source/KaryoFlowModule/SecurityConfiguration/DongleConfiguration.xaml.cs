﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ServiceProcess;
using System.Runtime.InteropServices;
using System.IO;
using AI.Authentication;


namespace AI
{
    /// <summary>
    /// Interaction logic for DongleConfiguration.xaml
    /// </summary>
    public partial class DongleConfiguration : UserControl
    {
        const string extension = ".v2c";
        const string filefilter = "Dongle Update Files (*.v2c)|*";

        public DongleConfiguration()
        {
            InitializeComponent();
        }

        private void OnApply(object sender, RoutedEventArgs e)
        {
            Cursor oldCursor = Cursor;
            Cursor = Cursors.Wait;

            ((DongleController)DataContext).UpdateIniFile();

            Cursor = oldCursor;
        }

        private void OnImport(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog fd = new System.Windows.Forms.OpenFileDialog();
            fd.Filter = filefilter + extension;
            if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ((DongleController)DataContext).ImportUpdateFile(fd.FileName);
                
            }
        }
    }

    public class DongleController : Notifier
    {
        [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileString")]
        private static extern int GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);
        [DllImport("kernel32.dll", EntryPoint = "WritePrivateProfileString")]
        private static extern bool WritePrivateProfileString(string lpAppName, string lpKeyName, string lpString, string lpFileName);
        
        private string serverIP;
        private bool isRemoteEnabled = false;
        
        public DongleController()
        {
            DongleServerIP = ReadServerAddress();
            IsRemoteEnabled = DongleServerIP.Trim() != string.Empty;

            DongleInfo = NetDongle.ParseInfoString(NetDongle.GetInfo());
        }

        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; Notify("Status"); }
        }

        private IEnumerable<DongleProductInfo> dongleInfo;
        public IEnumerable<DongleProductInfo> DongleInfo
        {
            get { return dongleInfo; }
            set { dongleInfo = value; Notify("DongleInfo"); }
        }

        private bool isRestarting = false;
        public bool IsRestarting
        {
            get { return isRestarting; }
            set { isRestarting = value; Notify("IsRestarting"); }
        }

        public void ImportUpdateFile(string filename)
        {
            Status = string.Empty;
            if (NetDongle.Update(filename))
                DongleInfo = NetDongle.ParseInfoString(NetDongle.GetInfo());
            else
                Status = NetDongle.StatusString;
        }

        public bool IsRemoteEnabled
        {
            get { return isRemoteEnabled; }
            set { isRemoteEnabled = value; Notify("IsRemoteEnabled"); }
        }

        public string DongleServerIP
        {
            get { return serverIP; }
            set { serverIP = value; Notify("DongleServerIP"); }
        }

        private string ReadServerAddress()
        {
            int chars = 256;
            StringBuilder buffer = new StringBuilder(chars);

            if (GetPrivateProfileString("REMOTE", "serveraddr", "", buffer, chars, NetDongleSettings.IniFile) == 0)
                return string.Empty;

            return buffer.ToString();
        }

        public void UpdateIniFile()
        {
            if (!File.Exists(NetDongleSettings.IniFile))
                File.WriteAllText(NetDongleSettings.IniFile, "; Created by CVFlow");

            if (IsRemoteEnabled)
            {
                WritePrivateProfileString("REMOTE", "broadcastsearch", "0", NetDongleSettings.IniFile);
                WritePrivateProfileString("REMOTE", "serveraddr", DongleServerIP, NetDongleSettings.IniFile);
            }
            else
            {
                WritePrivateProfileString("REMOTE", "broadcastsearch", "1", NetDongleSettings.IniFile);
                WritePrivateProfileString("REMOTE", "serveraddr", "", NetDongleSettings.IniFile);
            }
            RestartHaspService();

            DongleInfo = NetDongle.ParseInfoString(NetDongle.GetInfo());
        }

        public void RestartHaspService()
        {
            IsRestarting = true;

            double timeoutMilliseconds = 20000;
            ServiceController service = new ServiceController("HASP License Manager");
            try
            {
                int millisec1 = Environment.TickCount;
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);

                int millisec2 = Environment.TickCount;
                timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1));

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch {}
            finally
            {
                IsRestarting = false;
            }
        }

    }
}
