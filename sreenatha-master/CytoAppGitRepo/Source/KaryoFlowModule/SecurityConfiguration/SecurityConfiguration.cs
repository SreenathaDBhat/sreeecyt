﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Data.Linq;
using System.Text;
using AI.Connect;

namespace AI.Security
{
    class SecurityConfiguration : IDisposable
    {
        #region  Constants

        private const int NOERROR = 0;
        private const int ERROR = 1;
        private const int GLOBALTYPE = 0;
        private const int FEATURETYPE = 1;
        private const string USERACCESSRIGHTS = "UserAccessRights";
        private const string USERLOGGING = "UserLogging";

        #endregion

        #region Variables

        private SecurityViewModel svm = null;
        private List<StatusViewModel> statuses = null;
        private List<GroupViewModel> groups = null;
        private List<UserViewModel> users = null;
        private List<FlagViewModel> flags = null;

        private Boolean _userLogging = false;
        private Boolean _checkAccess = false;

        #endregion

        #region Properties

        public Boolean IsAccessRightsEnabled
        {
            get { return _checkAccess; }
        }

        public Boolean IsLoggingEnabled
        {
            get { return _userLogging; }
        }

        #endregion

        public SecurityConfiguration()
        {
        }

        private int ReadGlobalSettings()
        {

            int errCode = ERROR;
            Database db = null;

            try
            {
                db = new Database();

               int numRecs = db.Flags.Count();

                var flag = db.Flags.Where(f => f.FlagName == USERACCESSRIGHTS).FirstOrDefault();

                if (flag != null)
                    _checkAccess = (Boolean)flag.Enabled;
                else
                {
                    _checkAccess = false;
                }


                flag = db.Flags.Where(f => f.FlagName == USERLOGGING).FirstOrDefault();

                if (flag != null)
                    _userLogging = (Boolean)flag.Enabled;
                else
                {
                    _userLogging = false;
                }

                errCode = NOERROR;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                _checkAccess = false;
                _userLogging = false;
                errCode = ERROR;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return errCode;
        }

        private int ReadUsersAndGroups()
        {
            int errCode = ERROR;
            Database db = null;

            try
            {
                db = new Database();

                var user = db.GetTable<Connect.User>();

                //  Need to get the groups first as the ID's are required 
                //  for looking up groups for a user
                var groupquery = from g in user where ((Boolean)g.GroupFlag) == true select g;

                foreach (Connect.User newgroup in groupquery)
                {
                    groups.Add(new GroupViewModel(newgroup.ID, newgroup.Name));
                }

                var userquery = from u in user where ((Boolean)u.GroupFlag) == false select u;

                foreach (Connect.User newuser in userquery)
                {
                    string grpName = string.Empty;

                    if (newuser.GroupID > 0)
                    {
                        var found = groups.Where(g => g.ID == newuser.GroupID).First();

                        if (found != null)
                        {
                            grpName = found.Name;
                        }
                    }

                    users.Add(new UserViewModel(newuser.ID, newuser.Name, grpName));
                }

                errCode = NOERROR;
            }
            catch
            {
                errCode = ERROR;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return errCode;
        }
        
        private int ReadStatuses()
        {
            int errCode = ERROR;
            Database db = null;

            try
            {
                db = new Database();
                var s = db.GetTable<Connect.Status>();

                foreach (Connect.Status u in s)
                {
                    statuses.Add(new StatusViewModel(u.ID, u.StatusCode));
                }

                errCode = NOERROR;
            }
            catch
            {
                errCode = ERROR;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return errCode;
        }

        public List<string> GetStatusList()
        {
            List<string> thisList = new List<string>();
            Database db = null;

            try
            {
                db = new Database();

                var s = db.GetTable<Connect.Status>();

                foreach (Connect.Status u in s)
                {
                    thisList.Add(u.StatusCode);
                }
            }
            catch
            {
                thisList.Clear();            
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return thisList;
        }

        public List<string> GetFlagList()
        {
            List<string> thisList = new List<string>();

            Database db = null;

            try
            {
                db = new Database();

                var s = db.GetTable<Connect.Flag>();

                foreach (Connect.Flag u in s)
                {
                    thisList.Add(u.FlagName);
                }

                thisList.Sort();
            }
            catch
            {
                thisList.Clear();
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
            return thisList;
        }

        private int ReadUserFlags()
        {
            int errCode = ERROR;

            Database db = null;

            try
            {
                db = new Database(); 
                
                var uf = db.GetTable<Connect.Flag>();

                foreach (Connect.Flag f in uf)
                {
                    if (f.FlagType == FEATURETYPE)
                        flags.Add(new FlagViewModel(f.ID, f.FlagName, ((Boolean)f.Enabled)));
                }

                errCode = NOERROR;
            }
            catch
            {
                flags.Clear();
                errCode = ERROR;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return errCode;
        }

        private void CreateSecurityViewModel()
        {
            if (svm == null)
            {
                statuses = new List<StatusViewModel>();
                groups = new List<GroupViewModel>();
                users = new List<UserViewModel>();
                flags = new List<FlagViewModel>();

                int errCode = ReadUsersAndGroups();

                if (errCode == NOERROR)
                    errCode = ReadStatuses();

                if (errCode == NOERROR)
                    errCode = ReadUserFlags();

                svm = new SecurityViewModel(this);

                svm.Statuses = statuses;
                svm.Groups = groups;
                svm.Users = users;
                svm.Flags = flags;

                svm.IsAccessRightsEnabled = _checkAccess;
                svm.IsLoggingEnabled = _userLogging;
            }
        }

        internal void RefreshSecurityViewModel()
        {
            if (svm != null)
            {
                statuses.Clear();
                groups.Clear();
                users.Clear();
                flags.Clear();

                int errCode = ReadUsersAndGroups();

                if (errCode == NOERROR)
                    errCode = ReadStatuses();

                if (errCode == NOERROR)
                    errCode = ReadUserFlags();

                svm.Statuses = statuses;
                svm.Groups = groups;
                svm.Users = users;
                svm.Flags = flags;

                svm.IsAccessRightsEnabled = _checkAccess;
                svm.IsLoggingEnabled = _userLogging;
            }
        }

        private int SaveGlobalSettings()
        {
            int errCode = ERROR;

            Database db = null;

            try
            {
                db = new Database();
                var flag = db.Flags.Where(f => f.FlagName == USERACCESSRIGHTS).FirstOrDefault();
                flag.Enabled = _checkAccess;
                db.SubmitChanges();

                flag = db.Flags.Where(f => f.FlagName == USERLOGGING).FirstOrDefault();
                flag.Enabled = _userLogging;
                db.SubmitChanges();

                errCode = NOERROR;
            }
            catch
            {
                errCode = ERROR;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return errCode;
        }

        private int SaveGroups(IEnumerable<GroupViewModel> saveGVM, IEnumerable<string> groupsToDelete)
        {
            int errCode = ERROR;
            Database db = null;

            try
            {
                db = new Database();

                //  Insert and update what we have been given
                foreach (GroupViewModel g in saveGVM)
                {
                    if (g.ID == -1)   // its a new group
                    {
                        Connect.User u = new Connect.User();
                        u.GroupFlag = true;
                        u.Name = g.Name;
                        u.GroupID = -1;
                        db.Users.InsertOnSubmit(u);
                    }
                    else
                    {
                        var grp = db.Users.Where(f => f.ID == g.ID).FirstOrDefault();
                        grp.Name = g.Name;
                    }
                }

                //  Remove deletions from table
                foreach (string s in groupsToDelete)
                {
                    var delete = db.Users.Where(f => (f.Name == s) && (f.GroupFlag == true)).FirstOrDefault();

                    if (delete != null)
                    {
                        // Need to delete any linked records before deletion
                        var linkedRecs = from usf in db.UserStatusFlags where usf.UserID == delete.ID select usf;

                        foreach (var rec in linkedRecs)
                        {
                            db.UserStatusFlags.DeleteOnSubmit(rec);
                        }

                        db.Users.DeleteOnSubmit(delete);
                    }
                }
                
                db.SubmitChanges();

                errCode = NOERROR;
            }
            catch
            {
                errCode = ERROR;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return errCode;
        }

        private int GetGroupID(string groupName)
        {
            int id = -1;

            Database db = null;

            try
            {
                db = new Database();

                if (groupName.Length > 0)
                {
                    var grp = db.Users.Where(f => f.Name == groupName).FirstOrDefault();
                    if (grp != null)
                        id = grp.ID;
                }
            }
            catch
            {
                id = -1;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return id;
        }

        private int SaveUsers(IEnumerable<UserViewModel> saveUVM, IEnumerable<string> usersToDelete)
        {
            int errCode = ERROR;
            Database db = null;

            try
            {
                db = new Database();

                foreach (UserViewModel g in saveUVM)
                {
                    if (g.ID == -1)   // its a new user
                    {
                        Connect.User u = new Connect.User();
                        u.GroupFlag = false;
                        u.Name = g.Name;
                        u.GroupID = GetGroupID(g.MemberOfGroup);
                        db.Users.InsertOnSubmit(u);
                    }
                    else
                    {
                        var usr = db.Users.Where(f => f.ID == g.ID).FirstOrDefault();
                        usr.Name = g.Name;
                        usr.GroupID = GetGroupID(g.MemberOfGroup);
                    }
                }

                //  Remove deletions from table
                foreach (string s in usersToDelete)
                {
                    var deleteUser = db.Users.Where(f => (f.Name == s) && (f.GroupFlag == false)).FirstOrDefault();

                    if (deleteUser != null)
                    {
                        // Need to delete any linked records before deletion
                        var linkedRecs = from usf in db.UserStatusFlags where usf.UserID == deleteUser.ID select usf;

                        foreach (var rec in linkedRecs)
                        {
                            db.UserStatusFlags.DeleteOnSubmit(rec);
                        }

                        db.Users.DeleteOnSubmit(deleteUser);
                    }
                }

                db.SubmitChanges();

                errCode = NOERROR;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                errCode = ERROR;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return errCode;
        }

        private int SaveStatuses(IEnumerable<StatusViewModel> saveSVM, IEnumerable<string> statusesToDelete)
        {
            int errCode = ERROR;
            Database db = null;

            try
            {
                db = new Database();

                foreach (StatusViewModel g in saveSVM)
                {
                    if (g.ID == -1)   // its a new Status
                    {
                        Connect.Status u = new Connect.Status();
                        u.StatusCode = g.Code;
                        db.Status.InsertOnSubmit(u);
                    }
                    else
                    {
                        var stat = db.Status.Where(f => f.ID == g.ID).FirstOrDefault();
                        stat.StatusCode = g.Code;
                    }
                }

                //  Remove deletions from table
                foreach (string s in statusesToDelete)
                {
                    var delete = db.Status.Where(f => f.StatusCode == s).FirstOrDefault();

                    if (delete != null)
                    {
                        // Need to delete any linked records before deletion
                        var linkedRecs = from usf in db.UserStatusFlags where usf.StatusID == delete.ID select usf;

                        foreach (var rec in linkedRecs)
                        {
                            db.UserStatusFlags.DeleteOnSubmit(rec);
                        }

                        db.Status.DeleteOnSubmit(delete);
                    }
                }

                db.SubmitChanges();

                errCode = NOERROR;
            }
            catch
            {
                errCode = ERROR;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return errCode;
        }

        private int SaveFlags(IEnumerable<FlagViewModel> saveFVM, IEnumerable<string> flagsToDelete)
        {
            int errCode = ERROR;
            Database db = null;

            try
            {
                db = new Database();

                foreach (FlagViewModel g in saveFVM)
                {
                    if (g.ID == -1)   // its a new Flag
                    {
                        Connect.Flag u = new Connect.Flag();
                        u.FlagName = g.FlagName;
                        u.FlagType = FEATURETYPE;
                        u.Enabled = g.DefaultValue;
                        db.Flags.InsertOnSubmit(u);
                    }
                    else
                    {
                        var flg = db.Flags.Where(f => f.ID == g.ID).FirstOrDefault();
                        flg.FlagName = g.FlagName;
                        flg.Enabled = g.DefaultValue;
                    }
                }

                //  Remove deletions from table
                foreach (string s in flagsToDelete)
                {
                    var delete = db.Flags.Where(f => f.FlagName == s).FirstOrDefault();

                    if (delete != null)
                    {
                        // Need to delete any linked records before deletion
                        var linkedRecs = from usf in db.UserStatusFlags where usf.FlagID == delete.ID select usf;

                        foreach (var rec in linkedRecs)
                        {
                            db.UserStatusFlags.DeleteOnSubmit(rec);
                        }

                        db.Flags.DeleteOnSubmit(delete);
                    }
                }
                
                db.SubmitChanges();

                errCode = NOERROR;
            }
            catch
            {
                errCode = ERROR;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return errCode;
        }

        internal int SaveSecurityViewModel(SecurityViewModel objSVM)
        {
            //  Saves the assigned groups, users, statuses and flags

            int errCode = ERROR;

            try
            {

                _checkAccess = objSVM.IsAccessRightsEnabled;
                _userLogging = objSVM.IsLoggingEnabled;
                errCode = SaveGlobalSettings();

                //  Save the groups first incase of 
                //  user dependancies needing ID
                if (errCode == Constants.NOERROR)
                    errCode = SaveGroups(objSVM.Groups, objSVM.GroupsToDelete);

                if (errCode == Constants.NOERROR)
                    errCode = SaveUsers(objSVM.Users, objSVM.UsersToDelete);

                if (errCode == Constants.NOERROR)
                    errCode = SaveStatuses(objSVM.Statuses, objSVM.StatusesToDelete);

                if (errCode == Constants.NOERROR)
                    errCode = SaveFlags(objSVM.Flags, objSVM.FlagsToDelete);

            }
            catch
            {
                errCode = ERROR;
            }

            return errCode;
        }

        public int ReadSecuritySettings()
        {
            int errCode = ERROR;

            try
            {
                errCode = ReadGlobalSettings();
            }
            catch
            {
                //  Any errors then disable global options
                _userLogging = false;
                _checkAccess = false;
                errCode = ERROR;
            }
            finally
            {
                if (errCode == ERROR)
                {
                    //  Any errors then disable global options
                    _userLogging = false;
                    _checkAccess = false;
                }
            }

            return errCode;
        }
        
        public SecurityViewModel GetSecurityViewModel()
        {
            CreateSecurityViewModel();
            return svm;
        }

        public UserStatusFlagsViewModel GetUserStatusFlagSettings(string username)
        {
            return new UserStatusFlagsViewModel(username); 
        }

        public int SaveUserStatusFlagSettings(UserStatusFlagsViewModel userSettings)
        {
            int errCode = ERROR;

            if (userSettings == null)
                return errCode;

            Database db = null;

            try
            {
                db = new Database();

                int userID = userSettings.ID;
                
                var recs = db.GetTable<Connect.UserStatusFlag>();

                var userrecs = from usr in recs where (usr.UserID == userID) select usr;

                Boolean allinserts = false;

                if (userrecs.Count() <= 0)
                {
                    allinserts = true;
                }

                foreach (StatusCode sc in userSettings.Settings)
                {
                    foreach (FlagOption fg in sc.Flags)
                    {
                        //  Do we update or insert?

                        if (allinserts)
                        {
                            Connect.UserStatusFlag newrec = new Connect.UserStatusFlag();
                            newrec.UserID = userID;
                            newrec.StatusID = sc.ID;
                            newrec.FlagID = fg.ID;
                            newrec.Enabled = fg.Enabled;
                            db.UserStatusFlags.InsertOnSubmit(newrec);
                        }
                        else
                        {
                            var userrec = (from usr in recs where (usr.UserID == userID) && (usr.StatusID == sc.ID) 
                                       && (usr.FlagID == fg.ID) select usr).FirstOrDefault();

                            if (userrec ==  null)
                            {
                                //  INSERT
                                Connect.UserStatusFlag newrec = new Connect.UserStatusFlag();
                                newrec.UserID = userID;
                                newrec.StatusID = sc.ID;
                                newrec.FlagID = fg.ID;
                                newrec.Enabled = fg.Enabled;
                                db.UserStatusFlags.InsertOnSubmit(newrec);
                            }
                            else
                            {
                                //  UPDATE
                                userrec.Enabled = fg.Enabled;
                            }
                        }
                    }
                }

                db.SubmitChanges();

                userSettings.IsDirty = false;

                errCode = NOERROR;
            }
            catch
            {
                errCode = ERROR;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }

            return errCode;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (statuses != null)
            {
                statuses.Clear();
                statuses = null;
            }

            if (groups != null)
            {
                groups.Clear();
                groups = null;
            }

            if (users != null)
            {
                users.Clear();
                users = null;
            }

            if (flags != null)
            {
                flags.Clear();
                flags = null;
            }

            if (svm != null)
            {
                svm.Dispose();
                svm = null;
            }
        }

        #endregion
    }
}
