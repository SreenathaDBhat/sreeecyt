﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;
using AI.Connect;
using AI.Security;

namespace AI.Security
{
    /// <summary> 
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private const string GROUP_SUFFIX = " (G)";
        private const string GROUPMEMBER_SUFFIX = " (M)";

        private SecurityConfiguration sc = null;
        private SecurityViewModel svm = null;
        private UserStatusFlagsViewModel usfs = null;
        private UserViewModel popupGroup = null;
        private Boolean tabDataChanged = false;

        private TabItem currentPage = null;
        private string currentUser = string.Empty;

        public Window1()
        {
            InitializeComponent();

            WindowLocations.Handle(this);

            configSettings.Visibility = Visibility.Collapsed;

            Loaded += new RoutedEventHandler(SecurityConfig_Loaded);

            Closing += (s, e) =>
            {
                if (sc != null)
                {
                    sc.Dispose();
                    sc = null;
                }
            };

        }

        void SecurityConfig_Loaded(object sender, RoutedEventArgs e)
        {

            ThreadPool.QueueUserWorkItem(delegate
            {
                while (!TestConnection())
                {
                    Dispatcher.Invoke((ThreadStart)delegate
                    {
                        NetworkConnect connectScreen = new NetworkConnect();
                        connectScreen.Owner = this;
                        bool result = connectScreen.ShowDialog().GetValueOrDefault(false);

                        if (!result)
                        {
                            DialogResult = false;
                            Close();
                        }
                        else
                        {
                            Locations.Save();
                        }

                    }, DispatcherPriority.Normal);
                }

                //  Check the defaults exist
                if (Security.Init() != 0)
                {
                    this.Close();
                }
                
                sc = new SecurityConfiguration();
                sc.ReadSecuritySettings();

                svm = sc.GetSecurityViewModel();
                svm.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(svm_PropertyChanged);

                Dispatcher.Invoke((ThreadStart)delegate
                {
                    networkBusy.Visibility = Visibility.Collapsed;
                    configSettings.Visibility = Visibility.Visible;

                    UserRights.DataContext = svm;
                    Logging.DataContext = svm;
                    GroupList.DataContext = svm.Groups;
                    UserList.DataContext = svm.Users;
                    StatusList.DataContext = svm.Statuses;
                    FlagsList.DataContext = svm.Flags;
                    DongleConfig.DataContext = new DongleController();

                    currentPage = ((TabItem)configSettings.SelectedItem);

                });

            });
        }

        void svm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Groups":
                    break;
                case "Users":
                    break;

            }
        }

        private void RefreshConfigSettings()
        {
            svm.Refresh();

            UserRights.DataContext = svm;
            Logging.DataContext = svm;
            GroupList.DataContext = svm.Groups;
            UserList.DataContext = svm.Users;
            StatusList.DataContext = svm.Statuses;
            FlagsList.DataContext = svm.Flags;

        }
        
        private static bool TestConnection()
        {
            return (Locations.ConfigFile.Exists && AI.Connect.NetworkTest.TestSql());
        }

        private void SaveSettings_Click(object sender, RoutedEventArgs e)
        {
            if (svm != null)
            {
                int errCode = Constants.NOERROR;

                //  D A T A     V A L I D A T I O N    F I R S T
                
                
                // Ensure there are no empty records
                foreach (GroupViewModel gvm in svm.Groups)
                {
                    if (gvm.Name.Length <= 0)
                    {
                        MessageBox.Show("Empty Group names not allowed.");
                        errCode = Constants.ERROR;
                        break;
                    }
                }

                if (errCode == Constants.NOERROR)
                {
                    //  Confirm that each of the users groups exist
                    foreach (UserViewModel uvm in svm.Users)
                    {
                        if (uvm.Name.Length > 0)
                        {
                            if (uvm.MemberOfGroup.Length > 0)
                            {
                                GroupViewModel gvm = svm.Groups.Where(f => f.Name == uvm.MemberOfGroup).FirstOrDefault();
                                if (gvm == null)
                                {
                                    // We have an error
                                    MessageBox.Show("Invalid Group assigned to " + uvm.Name);
                                    errCode = Constants.ERROR;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Empty User names not allowed.");
                            errCode = Constants.ERROR;
                            break;
                        }
                    }
                }

                if (errCode == Constants.NOERROR)
                {
                    foreach (StatusViewModel status in svm.Statuses)
                    {
                        if (status.Code.Length <= 0)
                        {
                            MessageBox.Show("Empty Status codes not allowed.");
                            errCode = Constants.ERROR;
                            break;
                        }
                    }
                }

                if (errCode == Constants.NOERROR)
                {
                    foreach (FlagViewModel flg in svm.Flags)
                    {
                        if (flg.FlagName.Length <= 0)
                        {
                            MessageBox.Show("Empty Flag names not allowed.");
                            errCode = Constants.ERROR;
                            break;
                        }
                    }
                }

                if (errCode == Constants.NOERROR)
                {
                    errCode = svm.SaveSettings();
                    if (errCode == Constants.NOERROR)
                    {
                        RefreshConfigSettings();
                         MessageBox.Show("Settings saved succesfully.");
                    }
                    else
                    {
                        MessageBox.Show("Error saving Settings.");
                    }
                }

                tabDataChanged = false;
            
            }
        }

        private void AddGroupButton_Click(object sender, RoutedEventArgs e)
        {
            string newGroup = NewGroup.Text;
            int errCode = svm.AddGroup(newGroup);
            if (errCode != Constants.NOERROR)
                MessageBox.Show("Error adding Group");
            else
            {
                NewGroup.Text = string.Empty;
                tabDataChanged = true;
            }
        }

        private void AddUserButton_Click(object sender, RoutedEventArgs e)
        {
            string newUser = NewUserName.Text;

            int errCode = svm.AddUser(newUser, string.Empty);

            if (errCode != Constants.NOERROR)
                MessageBox.Show("Error adding User");
            else
            {
                NewUserName.Text = string.Empty;
                tabDataChanged = true;
            }
        }

        private void AddStatusButton_Click(object sender, RoutedEventArgs e)
        {
            string newCode = NewStatusCode.Text;

            int errCode = svm.AddStatus(newCode);

            if (errCode != Constants.NOERROR)
                MessageBox.Show("Error adding Status");
            else
            {
                NewStatusCode.Text = string.Empty;
                tabDataChanged = true;
            }
        }

        private void AddFlagButton_Click(object sender, RoutedEventArgs e)
        {
            string newFlag = NewFlag.Text;

            int errCode = svm.AddFlag(newFlag);

            if (errCode != Constants.NOERROR)
                MessageBox.Show("Error adding Flag");
            else
            {
                NewFlag.Text = string.Empty;
                tabDataChanged = true;
            }
        }

        private void UserMemberOf_DropDownOpened(object sender, EventArgs e)
        {
            ComboBox cbUserGroup = sender as ComboBox;

            if (cbUserGroup != null)
            {
                cbUserGroup.Items.Clear();
                cbUserGroup.Items.Add(" ");

                foreach (GroupViewModel g in svm.Groups)
                {
                    cbUserGroup.Items.Add(g.Name);
                }

                cbUserGroup.IsDropDownOpen = true;
            }
        }

        private void configSettings_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (sender == e.OriginalSource)
            {
                //  Disable event temporarily
                configSettings.SelectionChanged -= this.configSettings_SelectionChanged;
                
                Boolean swap = true;

                if (tabDataChanged)
                {
                    if (MessageBox.Show("Data has not been saved and will be lost.", "Are you sure?", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                    {
                        swap = false;
                    }
                }

                // We are already on the new tabpage
                TabItem tabpg = ((sender as TabControl).SelectedItem as TabItem);

                if (swap)
                {
                    if (tabpg.Name.ToUpper() == "USERSETTINGS")
                    {
                        if (tabDataChanged)
                            svm.Refresh();

                        if (usfs != null)
                        {
                            usfs.Dispose();
                            usfs = null;
                        }

                        // Populate the User dropdown
                        PopulateSelectUser();
                    }
                    else
                    {
                        svm.Refresh();
                        UserRights.DataContext = svm;
                        Logging.DataContext = svm;
                        GroupList.DataContext = svm.Groups;
                        UserList.DataContext = svm.Users;
                        StatusList.DataContext = svm.Statuses;
                        FlagsList.DataContext = svm.Flags;
                    }

                    tabDataChanged = false;

                    currentPage = tabpg;
                }
                else
                {
                    configSettings.SelectedItem = currentPage;
                }

                configSettings.SelectionChanged += this.configSettings_SelectionChanged;

                e.Handled = true;

            }
        }

        
        private void PopulateSelectUser()
        {
            List<string> names = new    List<string>();
            names.Add(" ");

            foreach (GroupViewModel grp in svm.Groups)
            {
                names.Add(grp.Name + GROUP_SUFFIX);
            }

            foreach (UserViewModel g in svm.Users)
            {
                if (g.MemberOfGroup.Length > 0)
                    names.Add(g.Name + GROUPMEMBER_SUFFIX);
                else
                    names.Add(g.Name);
            }

            names.Sort();

            SelectUser.ItemsSource = names;
            SelectUser.SelectedItem = " ";
        }

        private void SelectUser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (sender == e.OriginalSource)
            {
                SelectUser.SelectionChanged -= this.SelectUser_SelectionChanged;
                
                Boolean swap = true;

                if (usfs != null)
                {
                    if (usfs.IsDirty)
                    {
                        if (MessageBox.Show("Data has not been saved and will be lost.", "Are you sure?", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                        {
                            swap = false;
                        }
                    }
                }

                if (swap)
                {
                    string username = ((string)(sender as ComboBox).SelectedItem);

                    UserSettingsSave.IsEnabled = false;

                    if ((username != null) && (username.Trim().Length > 0))
                    {

                        if (usfs != null)
                        {
                            usfs.Dispose();
                            usfs = null;
                        }

                        string lookup = username;

                        if (lookup.EndsWith(GROUP_SUFFIX))
                        {
                            lookup = lookup.Substring(0, username.Length - GROUP_SUFFIX.Length).Trim();
                            UserSettingsSave.IsEnabled = true;
                        }
                        else
                        {
                            if (lookup.EndsWith(GROUPMEMBER_SUFFIX))
                            {
                                lookup = lookup.Substring(0, username.Length - GROUPMEMBER_SUFFIX.Length).Trim();
                            }
                            else
                            {
                                UserSettingsSave.IsEnabled = true;
                            }
                        }

                        usfs = sc.GetUserStatusFlagSettings(lookup);
                        usfs.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(usfs_PropertyChanged);
                        tabDataChanged = false;

                        StatusCodeList.DataContext = usfs.Settings;
                        UserSettingsSave.DataContext = usfs;
                        currentUser = username;
                    }

                }
                else
                {
                    (sender as ComboBox).SelectedItem = currentUser;
                }

                SelectUser.SelectionChanged += this.SelectUser_SelectionChanged;

                e.Handled = true;
            }        
        }

        void usfs_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsDirty":
                    tabDataChanged = true;
                    break;
            }
        }

        private void UserSettingsSave_Click(object sender, RoutedEventArgs e)
        {
            if (usfs != null)
            {
                int err = sc.SaveUserStatusFlagSettings(usfs);

                if (err == Constants.NOERROR)
                {
                    tabDataChanged = false;
                    MessageBox.Show("User Settings saved succesfully.");
                }
                else
                {
                    MessageBox.Show("Error saving User Settings.");
                }
            }
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            //  Validation required on some fields
            TextBox tb = sender as TextBox;

            if (tb.Tag != null)
            {
                string objType = tb.Tag.ToString().ToUpper();
                switch (objType)
                {
                    case "AI.SECURITY.USERVIEWMODEL":
                        if (tb.Name.ToUpper() == "USERMEMBEROF")
                        {
                            // Validate the group exists in current list
                            if (!svm.GroupExists(tb.Text))
                                MessageBox.Show("Invalid Group assigned");
                        }
                        break;
                }

            }


            ((TextBox)sender).Background = Brushes.Transparent;
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).Background = Brushes.White;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;

            if (tb.Tag != null)
            {
                string objType = tb.Tag.ToString().ToUpper();
                switch (objType)
                {
                    case "AI.SECURITY.GROUPVIEWMODEL":
                        if (tb.Name.ToUpper() == "GROUPNAME")
                        {
                            ((GroupViewModel)tb.Tag).Name = tb.Text;
                        }
                        break;
                    case "AI.SECURITY.USERVIEWMODEL":
                        if (tb.Name.ToUpper() == "USERNAME")
                        {
                            ((UserViewModel)tb.Tag).Name = tb.Text;
                        }
                        else
                        {
                            if (tb.Name.ToUpper() == "USERMEMBEROF")
                            {
                                ((UserViewModel)tb.Tag).MemberOfGroup = tb.Text;
                            }
                        }
                        break;
                    case "AI.SECURITY.STATUSVIEWMODEL":
                        if (tb.Name.ToUpper() == "STATUSCODE")
                        {
                            ((StatusViewModel)tb.Tag).Code = tb.Text;
                        }
                        break;
                    case "AI.SECURITY.FLAGVIEWMODEL":
                        if (tb.Name.ToUpper() == "FLAGNAME")
                        {
                            ((FlagViewModel)tb.Tag).FlagName = tb.Text;
                        }
                        break;
                }

            }
        }

        private void DeleteGroupButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;

            if (button.Tag != null)
            {
                string deleteGroup = button.Tag.ToString();

                if (svm.CanDeleteGroup(deleteGroup))
                {
                    svm.DeleteGroup(deleteGroup);
                    tabDataChanged = true;
                }
                else
                    MessageBox.Show("Error - Users assigned to group");
            }
        }

        private void DeleteUserButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;

            if (button.Tag != null)
            {
                string deleteUser = button.Tag.ToString();

                svm.DeleteUser(deleteUser);
                tabDataChanged = true;
            }

        }

        private void DeleteStatusButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;

            if (button.Tag != null) 
            {
                string deleteStatus = button.Tag.ToString();

                svm.DeleteStatus(deleteStatus);
                tabDataChanged = true;
            }
        }

        private void DeleteFlagButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;

            if (button.Tag != null)
            {
                string deleteFlag = button.Tag.ToString();

                svm.DeleteFlag(deleteFlag);
                tabDataChanged = true;
            }

        }

        private void UserGroupButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            popupGroup = ((UserViewModel)button.Tag);
            PopupGroupList.DataContext = svm.Groups;
            groupsPopup.IsOpen = true;
        }

        private void popupGroupName_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;

            if (button.Tag != null)
            {
                string group = ((GroupViewModel)button.Tag).Name;
                popupGroup.MemberOfGroup = group;
                groupsPopup.IsOpen = false;
                tabDataChanged = true;
            }
        }

        private void DeleteUserGroupButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;

            if (button.Tag != null)
            {
                ((UserViewModel)button.Tag).MemberOfGroup = string.Empty;
                tabDataChanged = true;
            }
        }
    }
}
