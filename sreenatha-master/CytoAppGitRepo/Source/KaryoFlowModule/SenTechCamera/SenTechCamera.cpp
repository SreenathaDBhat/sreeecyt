#include "stdafx.h"
#include "SenTechCamera.h"

namespace SenTech
{
	#pragma warning(disable:4538)

	Camera::Camera(System::Windows::Threading::Dispatcher^ disp) : BasicCamera(disp)
	{
		isConnected = false;
		callbackContext = new gcroot<Camera^>(this);	
		
		components = gcnew System::Collections::Generic::List<IHardwareComponent^>;
		components->Add(this);
	}

	#pragma warning(default:4538)

	Camera::~Camera()
	{
		Disconnect();

		delete callbackContext;
		callbackContext = 0;
	}

	void WINAPI RawCallbackFunc(PBYTE pbyteBuffer,DWORD dwBufferSize, DWORD dwWidth, DWORD dwHeight, DWORD, WORD, DWORD dwTransferBitsPerPixel, LPVOID lpContext, LPVOID)
	{
		gcroot<Camera^>* root = (gcroot<Camera^>*)lpContext;
		(*root)->LiveCallback(pbyteBuffer, dwBufferSize, dwWidth, dwHeight, dwTransferBitsPerPixel);
	}

	void Camera::DescribeFailure()
	{
		DWORD err = StCam_GetLastError(handle);
		if(err == 0)
		{
			failureReason = nullptr;
			return;
		}

		switch(err)
		{
		case ERRST_NOT_FOUND_CAMERA:
			failureReason = FaultDescription::FromStrings("Camera not found.", String::Empty);
			break;

		case ERRST_ALL_CAMARA_OPENED:
			failureReason = FaultDescription::FromStrings("Camera is already in use.", String::Empty);
			break;

		default:
			failureReason = FaultDescription::FromStrings("Connection Failed", String::Format("Error: {0}", err));
			break;
		};
	}

	void Camera::Connect()
	{
		handle = StCam_Open(0);
		if(handle == 0)
		{
			DescribeFailure();
			return;
		}

		DWORD n = 0;
		StCam_AddRawCallback(handle, &RawCallbackFunc, callbackContext, &n);


		DWORD sizeMode, offsetX, offsetY, w, h, bppmode;
		WORD scanMode;
		if(!StCam_GetImageSize(handle, &sizeMode, &scanMode, &offsetX, &offsetY, &w, &h))
		{
			DescribeFailure();
			Disconnect();
			return;
		}

		frameWidth = w;
		frameHeight = h;

		int len = frameWidth * frameHeight;
		snap = gcnew array<unsigned short>(len);

		StCam_GetTransferBitsPerPixel(handle, &bppmode);
		bitsPerPixel = bppmode == STCAM_TRANSER_BITS_PER_PIXEL_08 ? 8 : 16;
		if(bitsPerPixel != 8)
		{
			DescribeFailure();

			// does this camera even support >8 bits?
			// not sure so explicit fail.
			Disconnect();
			return;
		}

		WORD g;
		StCam_GetGain(handle, &g);
		StCam_SetGain(handle, 0);

		StCam_StartTransfer(handle);
		isConnected = true;
	}
	
	void Camera::Disconnect()
	{
		StopStreaming();
		StCam_StopTransfer(handle);
		StCam_Close(handle);
		handle = 0;
	}

	void Camera::LiveCallback(PBYTE pbyteBuffer, DWORD /*dwBufferSize*/, DWORD , DWORD , DWORD)
	{
		if(IsStreaming == false)
			return;

		
		pin_ptr<unsigned short> p = &snap[0];
		PBYTE buf = pbyteBuffer;

		int len = frameWidth * frameHeight;
		for(int i = 0; i < len; ++i)
		{
			*p++ = *buf++;
		}

		HandleFrame(snap);
	}


	System::Collections::Generic::IEnumerator<IHardwareComponent^>^ Camera::GetEnumerator()
	{
		return components->GetEnumerator();
	}

	System::Xml::Linq::XElement^ Camera::SaveCofiguration()
	{
		return gcnew System::Xml::Linq::XElement("SenTechCamera");
	}

	void Camera::LoadConfiguration(System::Xml::Linq::XElement^ /*deviceNode*/)
	{
	}
}