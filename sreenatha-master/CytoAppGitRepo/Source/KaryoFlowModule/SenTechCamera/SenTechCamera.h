#pragma once
using namespace System;
using namespace AI::Hardware;

#pragma warning(disable:4538)

namespace SenTech
{
	[Device("Sentech Camera")]
	public ref class Camera : public BasicCamera
	{
	public:
		Camera(System::Windows::Threading::Dispatcher^ disp);
		virtual ~Camera();

		virtual void Connect() override;
		void Disconnect();
		virtual property bool IsConnected { bool get() override { return isConnected; } }
		virtual property FaultDescription^ FailureReason { FaultDescription^ get() override { return failureReason; } }

		virtual property System::String^ Name { System::String^ get() override { return "SenTech"; } }
		virtual property int FrameWidth { int get() override { return frameWidth; } }
		virtual property int FrameHeight { int get() override { return frameHeight; } }
		virtual property int BitsPerPixel { int get() override { return bitsPerPixel; } }
        
		virtual System::Collections::Generic::IEnumerator<IHardwareComponent^>^ GetEnumerator() override;

		virtual System::Xml::Linq::XElement^ SaveCofiguration() override;
        virtual void LoadConfiguration(System::Xml::Linq::XElement^ deviceNode) override;

	internal:
		void LiveCallback(PBYTE pbyteBuffer, DWORD dwBufferSize, DWORD dwWidth, DWORD dwHeight, DWORD dwTransferBitsPerPixel);

	private:
		void DescribeFailure();
		FaultDescription^ failureReason;
		array<unsigned short>^ snap;

		HANDLE handle;
		gcroot<Camera^>* callbackContext;

		System::Collections::Generic::List<IHardwareComponent^>^ components;
		

		bool isConnected;
		int frameWidth;
		int frameHeight;
		int bitsPerPixel;
	};
}

#pragma warning(default:4538)
