﻿using System;
using System.Linq;
using System.Windows.Input;
using AI;
using System.Threading;
using System.Windows.Threading;
using System.Windows;

namespace AI
{
    public partial class ConnectionTestWindow
    {
        public ConnectionTestWindow()
        {
            InitializeComponent();
        }

        private void OnDragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void OnRetry(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                var db = new Connect.Database();
                db.Versions.Count();
                db.Dispose();

                DialogResult = true;
            }
            catch (Exception)
            {

            }
        }

        private void OnCancel(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
