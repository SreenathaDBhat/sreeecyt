﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace AI
{
    public static class DateStrings
    {
        private static readonly string DateString_HoursAgo = "DateString_HoursAgo";
        private static readonly string DateString_MinsAgo = "DateString_MinsAgo";
        private static readonly string DateString_Today = "DateString_Today";
        private static readonly string DateString_Yesterday = "DateString_Yesterday";
        private static readonly string DateString_Fallback = "DateString_Fallback";
        private static StringTable strings;

        public static void PrimeFromStringTable(StringTable strings)
        {
            DateStrings.strings = strings;
        }

        public static string FriendlyDateString(DateTime when, DateTime now)
        {
            var howOld = now - when;

            if (howOld.TotalHours < 12)
            {
                int hours = (int)howOld.TotalHours;
                if (hours > 0)
                {
                    return string.Format(strings.Lookup(DateString_HoursAgo, hours), hours);
                }
                else
                {
                    int mins = Math.Max(1, (int)howOld.TotalMinutes);
                    return string.Format(strings.Lookup(DateString_MinsAgo, mins), mins);
                }
            }
            else if (now.Year == when.Year && now.DayOfYear == when.DayOfYear)
            {
                return string.Format(strings.Lookup(DateString_Today), when);
            }
            else if (now.Year == when.Year && now.DayOfYear == when.DayOfYear + 1)
            {
                return string.Format(strings.Lookup(DateString_Yesterday), when);
            }
            else
            {
                return string.Format(strings.Lookup(DateString_Fallback), when);
            }
        }
    }

    public class FriendlyDateStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return DateStrings.FriendlyDateString((DateTime)value, DateTime.Now);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
