﻿using System;
using System.Linq;
using System.Windows.Input;
using AI;
using System.Threading;
using System.Windows.Threading;

namespace AI
{
    public partial class DeleteStuff
    {
        private DeleteStuffController controller;
        private ThreadStart deleteCode;

        public DeleteStuff(ThreadStart deleteCode)
        {
            this.deleteCode = deleteCode;
            controller = new DeleteStuffController();
            DataContext = controller;
            InitializeComponent();
        }

        private void OnDragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void OnConfirm(object sender, System.Windows.RoutedEventArgs e)
        {
            controller.ConfirmMode = false;
            Connect.ConnectionTest.Test(this);

            ThreadPool.QueueUserWorkItem((state) =>
                {
                    deleteCode();

                    Dispatcher.Invoke((ThreadStart)delegate
                    {
                        DialogResult = true;

                    }, DispatcherPriority.Normal);

                }, null);
        }

        private void OnCancel(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = false;
        }


        private class DeleteStuffController : Notifier
        {
            private bool confirmMode;

            public DeleteStuffController()
            {
                confirmMode = true;
            }

            public bool ConfirmMode
            {
                get { return confirmMode; }
                set { confirmMode = value; Notify("ConfirmMode"); }
            }
        }
    }
}
