﻿using System;
using System.Windows;
using AI.Connect;
using System.ComponentModel;
using System.Windows.Input;

namespace AI
{
    public partial class NetworkConnect : Window
    {
        public NetworkConnect()
        {
            InitializeComponent();
            DataContext = new NetworkInfo();

            sqlPassword.Password = ((NetworkInfo)DataContext).Password;
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            NetworkInfo info = (NetworkInfo)DataContext;
            info.Password = sqlPassword.Password;
            Locations.BlobPool = info.BlobPool;
            Locations.UseSSH = info.SSH;
            Locations.SSHServer = info.SSHServer;
            Locations.SSHUsername = info.SSHUsername;
            Locations.SSHPassword = info.SSHPassword;
            Locations.SSHPort = info.SSHPort;

            if (info.SSH)
            {
                Locations.ConnectionString = string.Format("Data Source=127.0.0.1,9999;Initial Catalog=CytovisionDb;User Id=cv;Password=OldBoOt46XX");
            }
            else if (info.WindowsAuthentication)
            {
                Locations.ConnectionString = string.Format("Data Source={0}{1}{2};Integrated Security=True;Initial Catalog=CytovisionDb",
                    info.ServerName,
                    info.InstanceName == string.Empty ? "" : "\\",
                    info.InstanceName == string.Empty ? "" : info.InstanceName);
            }
            else
            {
                Locations.ConnectionString = string.Format("Data Source={0}{1}{2};User ID={3};Password={4};Initial Catalog=CytovisionDb",
                    info.ServerName,
                    info.InstanceName == string.Empty ? "" : "\\",
                    info.InstanceName == string.Empty ? "" : info.InstanceName,
                    info.UserName,
                    info.Password);
            }

            DialogResult = true;
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void OnDragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }

    internal class NetworkInfo : INotifyPropertyChanged
    {
        private string blobPool;
        private string server;
        private string instance;
        private string user;
        private string password;
        private bool windowsAuth;
        
        private bool ssh;
        private string sshPassword;
        private string sshUsername;
        private string sshServer;
        private int sshPort;

        public NetworkInfo()
        {
            blobPool = Locations.BlobPool;
            server = Locations.SqlServerName;
            instance = Locations.SqlInstanceName;
            user = Locations.SqlUsername;
            password = Locations.SqlPassword;
            windowsAuth = user == string.Empty;

            ssh = Locations.UseSSH;
            sshPassword = Locations.SSHPassword;
            sshServer = Locations.SSHServer;
            sshPort = Locations.SSHPort;
            sshUsername = Locations.SSHUsername;
        }

        public string BlobPool
        {
            get { return blobPool; }
            set { blobPool = value; Notify("BlobPool"); }
        }

        public string ServerName
        {
            get { return server; }
            set { server = value; Notify("ServerName"); }
        }

        public string InstanceName
        {
            get { return instance; }
            set { instance = value; Notify("InstanceName"); }
        }

        public string UserName
        {
            get { return user; }
            set { user = value; Notify("UserName"); }
        }

        public string Password
        {
            get { return password; }
            set { password = value; Notify("Password"); }
        }

        public bool WindowsAuthentication
        {
            get { return windowsAuth; }
            set { windowsAuth = value; Notify("WindowsAuthentication"); }
        }

        public bool SSH
        {
            get { return ssh; }
            set { ssh = value; Notify("SSH"); }
        }

        public string SSHServer
        {
            get { return sshServer; }
            set { sshServer = value; Notify("SSHServer"); }
        }

        public string SSHUsername
        {
            get { return sshUsername; }
            set { sshUsername = value; Notify("SSHUsername"); }
        }

        public string SSHPassword
        {
            get { return sshPassword; }
            set { sshPassword = value; Notify("SSHPassword"); }
        }

        public int SSHPort
        {
            get { return sshPort; }
            set { sshPort = value; Notify("SSHPort"); }
        }


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        void Notify(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion
    }
}
