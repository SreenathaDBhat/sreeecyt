﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Linq;
using System.Data.Linq;
using System.IO;

namespace AI
{
    public partial class NewCaseWindow : Window
    {
        public static RoutedUICommand CreateCase = new RoutedUICommand("Create Case", "CreateCase", typeof(NewCaseWindow));

        private Case myCase;
        private StringTable strings;
        
        #region Validation class
        private class CaseDataContext : Notifier
        {
            private ILims lims;
            private Case myCase;
            private bool valid;
            private string reason;

            public CaseDataContext(Case myCase)
            {
                valid = true;
                lims = Connect.LIMS.New();
                this.myCase = myCase;
            }

            public bool Validate(Window parent, StringTable strings)
            {
                Connect.ConnectionTest.Test(parent);

                if (myCase.Name == null || myCase.Name.Trim().Length == 0)
                {
                    valid = false;
                    reason = strings.Lookup("newCaseFail_NoName");
                }
                else if (myCase.Name.Length > 250)
                {
                    valid = false;
                    reason = strings.Lookup("newCaseFail_TooLong");
                }
                else if (ContainsInvalidChars())
                {
                    valid = false;
                    reason = strings.Lookup("newCaseFail_BadChars");
                }
                else
                {
                    var existing = lims.FindCase(myCase.Name);
                    if (existing == null)
                    {
                        valid = true;
                        reason = "";
                    }
                    else
                    {
                        valid = false;
                        reason = strings.Lookup("newCaseFail_AlreadyExists");
                    }
                }

                Notify("IsValid");
                Notify("FailureReason");
                return valid;
            }

            private bool ContainsInvalidChars()
            {
                char[] alsoBad = { '~' };
                var invalidChars = Path.GetInvalidFileNameChars();

                foreach (char ch in myCase.Name)
                {
                    if (invalidChars.Contains(ch))
                        return true;
                    if (alsoBad.Contains(ch))
                        return true;
                }

                return false;
            }

            public bool IsValid
            {
                get { return valid; }
            }

            public string FailureReason
            {
                get { return reason; }
            }

            public Case Case
            {
                get { return myCase; }
            }
        }
        #endregion

        public NewCaseWindow(string startingName, StringTable strings)
        {
            this.strings = strings;
            myCase = new Case();
            myCase.Name = startingName;
            DataContext = new CaseDataContext(myCase);
            
            InitializeComponent();

            Loaded += (s, e) => { Keyboard.Focus(caseName); caseName.SelectAll(); };
        }

        public Case Case
        {
            get { return myCase; }
        }

        private void OnDragWindow(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                e.Handled = true;
                DialogResult = false;
            }
        }

        private void CanCreateCaseExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateCaseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (((CaseDataContext)DataContext).Validate(this, strings))
            {
                DialogResult = true;
            }
        }
    }
}
