﻿using System;
using System.Windows.Input;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Threading;
using System.Windows;

namespace AI
{
    public partial class ProgressDisplay
    {
        public ProgressDisplay(string displayText, ProgressTracker progress, Action action)
        {
            DataContext = new
                {
                    DisplayText = displayText,
                    Progress = progress,
                    IsCancellable = false
                };

            InitializeComponent();

            
            var task = Task.Factory.StartNew(action);
            task.ContinueWith((t) =>
                {
                    Dispatcher.Invoke((ThreadStart)delegate
                    {
                        DialogResult = true;

                    }, DispatcherPriority.Normal);
                });

            progress.ProgressChanged += (s, e) =>
                {
                    Dispatcher.Invoke((ThreadStart)delegate
                    {
                        progressDisplay.Value = e.ProgressPercentage;

                    }, DispatcherPriority.Normal);
                };
        }

        public static bool? Run(string displayText, ProgressTracker progress, Action action, Window owner)
        {
            var d = new ProgressDisplay(displayText, progress, action);
            d.Owner = owner;
            return d.ShowDialog();
        }

        private void OnDragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
