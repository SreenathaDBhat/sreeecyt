﻿using System;
using System.ComponentModel;
using System.Collections.Generic;

namespace AI
{
    public class ProgressNode
    {
        private ProgressTracker parent;
        private int steps, currentStep;

        internal ProgressNode(ProgressTracker parent, double weight)
        {
            this.parent = parent;
            steps = 0;
            currentStep = 0;
            Weight = weight;
        }

        public double Weight
        {
            get; private set;
        }

        public int Steps
        {
            get { return steps; }
            set
            {
                steps = value;
                parent.Notify();
            }
        }

        public int CurrentStep
        {
            get { return currentStep; }
            set { currentStep = value; parent.Notify(); }
        }

        public void Initialize(int totalSteps, int currentStep)
        {
            steps = totalSteps;
            this.currentStep = currentStep;
            parent.Notify();
        }
    }

    public class ProgressTracker
    {
        public delegate void ProgressChangedDelegate(object sender, ProgressChangedEventArgs e);
        public event ProgressChangedDelegate ProgressChanged;
        private List<ProgressNode> nodes;

        public ProgressTracker()
        {
            nodes = new List<ProgressNode>();
        }

        private bool isNotifying;
        public bool IsNotifyEnabled
        {
            get { return isNotifying; }
            set
            {
                isNotifying = value;
                if (isNotifying)
                    Notify();
            }
        }

        public ProgressNode CreateNode(double weight)
        {
            var n = new ProgressNode(this, weight);
            nodes.Add(n);
            Notify();
            return n;
        }

        internal void Notify()
        {
            if (!isNotifying || ProgressChanged == null)
                return;

            double total = 0;
            double current = 0;

            foreach (var n in nodes)
            {
                total += n.Steps * n.Weight;
                current += n.Weight * n.CurrentStep;
            }

            int p = (int)((current / total) * 100);
            ProgressChanged(this, new ProgressChangedEventArgs(p, null));
        }
    }
}
