﻿using System;
using System.Windows.Controls;
using System.Windows;

namespace AI
{
    public class BlackWindow : ContentControl
    {
        static BlackWindow()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(BlackWindow), new FrameworkPropertyMetadata(typeof(BlackWindow)));
        }

        protected override void OnMouseDown(System.Windows.Input.MouseButtonEventArgs e)
        {
            //if (e.ChangedButton == System.Windows.Input.MouseButton.Left)
            //{
            //    MoveWindow();
            //}

            base.OnMouseDown(e);
        }

        private void MoveWindow()
        {
            Window parent = VisualTreeWalker.FindParentOfType<Window>(this);
            if (parent != null)
                parent.DragMove();
        }
    }
}
