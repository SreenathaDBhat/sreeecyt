﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

namespace AI
{
    public class BrushAlternator : IValueConverter
    {
        private int n = 0;
        private Brush brush1 = Brushes.Transparent;
        private Brush brush2 = new SolidColorBrush(Color.FromArgb(32, 255, 255, 255));

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return n++ % 2 == 0 ? brush1 : brush2;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public Brush Even
        {
            get { return brush1; }
            set { brush1 = value; }
        }

        public Brush Odd
        {
            get { return brush2; }
            set { brush2 = value; }
        }
    }
}
