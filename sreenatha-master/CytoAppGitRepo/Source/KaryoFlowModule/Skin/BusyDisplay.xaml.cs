﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace AI
{
    public class BusyDisplay : UserControl
    {
        public static DependencyProperty ColorProperty = DependencyProperty.Register("Color", typeof(Brush), typeof(BusyDisplay), new FrameworkPropertyMetadata(Brushes.Black));

        public Brush Color
        {
            get { return (Brush)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }
    }
}
