﻿using System;
using System.Windows.Input;

namespace AI
{
    public static class MiscCommands
    {
        public static readonly RoutedUICommand SaveImage = new RoutedUICommand("SaveImage", "SaveImage", typeof(MiscCommands));
        public static readonly RoutedUICommand SaveImageWithOverlays = new RoutedUICommand("SaveImageWithOverlays", "SaveImageWithOverlays", typeof(MiscCommands));

        public static RoutedUICommand LaunchFishCapture = new RoutedUICommand("LaunchFishCapture", "LaunchFishCapture", typeof(MiscCommands));
        public static RoutedUICommand LaunchBrightfieldCapture = new RoutedUICommand("LaunchFishCapture", "LaunchFishCapture", typeof(MiscCommands));
    }
}
