﻿using System;
using System.Windows.Data;
using System.Windows;

namespace AI
{
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVisibilityConverter : IValueConverter
    {
        private Visibility valueWhenTrue = Visibility.Visible;
        private Visibility valueWhenFalse = Visibility.Collapsed;

        public Visibility ValueWhenTrue
        {
            get { return valueWhenTrue; }
            set { valueWhenTrue = value; }
        }

        public Visibility ValueWhenFalse
        {
            get { return valueWhenFalse; }
            set { valueWhenFalse = value; }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return valueWhenFalse;

            bool b = (bool)value;
            return b ? valueWhenTrue : valueWhenFalse;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility v = (Visibility)value;
            return v == Visibility.Visible;
        }
    }

    [ValueConversion(typeof(int), typeof(Visibility))]
    public class IntToVisibilityConverter : IValueConverter
    {
        private Visibility valueWhenZero = Visibility.Visible;
        private Visibility valueWhenNonZero = Visibility.Collapsed;

        public Visibility ValueWhenZero
        {
            get { return valueWhenZero; }
            set { valueWhenZero = value; }
        }

        public Visibility ValueWhenNonZero
        {
            get { return valueWhenNonZero; }
            set { valueWhenNonZero = value; }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((int)value == 0) ? valueWhenZero : valueWhenNonZero;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(int), typeof(bool))]
    public class IntToBoolConverter : IValueConverter
    {
        private bool valueWhenZero = false;
        private bool valueWhenNonZero = true;

        public bool ValueWhenZero
        {
            get { return valueWhenZero; }
            set { valueWhenZero = value; }
        }

        public bool ValueWhenNonZero
        {
            get { return valueWhenNonZero; }
            set { valueWhenNonZero = value; }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((int)value == 0) ? valueWhenZero : valueWhenNonZero;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(bool), typeof(bool))]
    public class BoolInvertConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !((bool)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !((bool)value);
        }
    }

    [ValueConversion(typeof(object), typeof(Visibility))]
    public class NullToVisibilityConverter : IValueConverter
    {
        private Visibility valueWhenNull;
        private Visibility valueWhenNotNull;

        public Visibility ValueWhenNull
        {
            get { return valueWhenNull; }
            set { valueWhenNull = value; }
        }
        
        public Visibility ValueWhenNotNull
        {
            get { return valueWhenNotNull; }
            set { valueWhenNotNull = value; }
        }



        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value == null ? valueWhenNull : valueWhenNotNull;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    [ValueConversion(typeof(object), typeof(Visibility))]
    public class NullToBoolConverter : IValueConverter
    {
        public bool ValueWhenNull { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value == null ? ValueWhenNull : !ValueWhenNull;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ObjectSameToVisibilityConverter : IMultiValueConverter
    {
        public Visibility ValueWhenSame { get; set; }
        public Visibility ValueWhenDifferent { get; set; }

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length < 2)
                return ValueWhenDifferent;

            return values[0] == values[1] ? ValueWhenSame : ValueWhenDifferent;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class StringToBoolConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string s = (string)value;
            string trimmedString = s.Trim();
            return trimmedString != string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class StringToVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members
        private Visibility valueWhenZero = Visibility.Hidden;
        private Visibility valueWhenNonZero = Visibility.Visible;

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return valueWhenZero;

            string s = (string)value;
            string trimmedString = s.Trim();
            return (trimmedString.Length > 0) ? valueWhenNonZero : valueWhenZero;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class StringLengthWarningVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public int MaxStringLength { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return Visibility.Hidden;

            string s = (string)value;
            return (s.Length > MaxStringLength) ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }


    public class DoubleConstantMultiplierConverter : IValueConverter
    {
        public double Multiplier { get; set; }

        public DoubleConstantMultiplierConverter()
        {
            Multiplier = 1;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if(value is int)
                return ((int)value) * Multiplier;
            else
                return ((double)value) * Multiplier;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class DoubleMultiplier : IMultiValueConverter
    {
        public DoubleMultiplier()
        {
            
        }

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double v = 1;

            foreach (object o in values)
            {
                if (o == DependencyProperty.UnsetValue)
                    return 0;

                v *= (double)o;
            }

            return v;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class AndTwoBoolsMultiConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)values[0] && (bool)values[1];
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }


    public class RandomNumberGenerator : IValueConverter
    {
        public double Minimum { get; set; }
        public double Maximum { get; set; }
        private Random rand = new Random();

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (rand.NextDouble() * (Maximum - Minimum)) + Minimum;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    public class BooleanOrConverter : IMultiValueConverter
    {
        public Visibility ValueWhenTrue { get; set; }
        public Visibility ValueWhenFalse { get; set; }

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            foreach (object o in values)
            {
                if(!(o is bool))
                    return ValueWhenFalse;

                if ((bool)o == true)
                    return ValueWhenTrue;
            }

            return ValueWhenFalse;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BooleanAndConverter : IMultiValueConverter
    {
        public Visibility ValueWhenTrue { get; set; }
        public Visibility ValueWhenFalse { get; set; }

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            foreach (object o in values)
            {
                if (!(o is bool))
                    return ValueWhenFalse;

                if ((bool)o == false)
                    return ValueWhenFalse;
            }

            return ValueWhenTrue;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToDoubleConverter : IValueConverter
    {
        private double valueWhenTrue = 1;
        private double valueWhenFalse = 0;

        public double ValueWhenTrue
        {
            get { return valueWhenTrue; }
            set { valueWhenTrue = value; }
        }

        public double ValueWhenFalse
        {
            get { return valueWhenFalse; }
            set { valueWhenFalse = value; }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool b = (bool)value;
            return b ? valueWhenTrue : valueWhenFalse;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }



    [ValueConversion(typeof(bool), typeof(double))]
    public class PickADoubleConverter : IValueConverter
    {
        private double valueWhenTrue = 1;
        private double valueWhenFalse = 0;

        public double ValueWhenTrue
        {
            get { return valueWhenTrue; }
            set { valueWhenTrue = value; }
        }

        public double ValueWhenFalse
        {
            get { return valueWhenFalse; }
            set { valueWhenFalse = value; }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool b = (bool)value;
            return b ? valueWhenTrue : valueWhenFalse;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility v = (Visibility)value;
            return v == Visibility.Visible;
        }
    }


    public class InverseVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (Visibility)value == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
