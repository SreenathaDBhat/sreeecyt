﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;

namespace AI
{
	/// <summary>
	/// Allows a UIStack item to save changes
	/// in response to the OnClosing main window event.
	/// </summary>
	public interface IClosing
	{
		void Closed();
	}
}
