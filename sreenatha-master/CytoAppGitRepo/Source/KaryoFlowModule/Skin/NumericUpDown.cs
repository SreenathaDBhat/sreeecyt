﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Globalization;

namespace AI
{
    public class NumericUpDown : Control
    {
        public static DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(double), typeof(NumericUpDown), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, new PropertyChangedCallback(OnValueChanged)));
        public static DependencyProperty MinimumProperty = DependencyProperty.Register("Minimum", typeof(double), typeof(NumericUpDown), new FrameworkPropertyMetadata(0.0));
        public static DependencyProperty MaximumProperty = DependencyProperty.Register("Maximum", typeof(double), typeof(NumericUpDown), new FrameworkPropertyMetadata(1.0));
        public static DependencyProperty IncrementProperty = DependencyProperty.Register("Increment", typeof(double), typeof(NumericUpDown), new FrameworkPropertyMetadata(1.0));
        private TextBlock valueText;

        static NumericUpDown()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NumericUpDown), new FrameworkPropertyMetadata(typeof(NumericUpDown)));
        }

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public double Minimum
        {
            get { return (double)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }

        public double Maximum
        {
            get { return (double)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        public double Increment
        {
            get { return (double)GetValue(IncrementProperty); }
            set { SetValue(IncrementProperty, value); }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            RepeatButton decrement = (RepeatButton)GetTemplateChild("PART_Decrement");
            decrement.Click += new RoutedEventHandler(decrement_Click);

            RepeatButton increment = (RepeatButton)GetTemplateChild("PART_Increment");
            increment.Click += new RoutedEventHandler(increment_Click);

            valueText = (TextBlock)GetTemplateChild("PART_ValueText");
            valueText.Text = Value.ToString(); 
        }

        static void OnValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            NumericUpDown nud = sender as NumericUpDown;
            if(nud.valueText != null)
                nud.valueText.Text = ((double)e.NewValue).ToString();
        }

        void increment_Click(object sender, RoutedEventArgs e)
        {
            Value = Math.Max(Minimum, Math.Min(Maximum, Value + Increment));
        }

        void decrement_Click(object sender, RoutedEventArgs e)
        {
            Value = Math.Max(Minimum, Math.Min(Maximum, Value - Increment));
        }
    }
}
