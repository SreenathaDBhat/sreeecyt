﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AI
{
    public enum SimpleMessageResult
    {
        OK = 0,
        Cancel = 1
    }

	public partial class SimpleMessageBox : Window
	{
		private SimpleMessageResult result;
        private bool showCancel;

        public string Message { get; set; }

		public SimpleMessageBox( Window owner, string message)
        {
		    InitSimpleMessageBox(owner, message, false);
        }

		public SimpleMessageBox( Window owner, string message, bool showCancel)
        {
		    InitSimpleMessageBox(owner, message, showCancel);
        }

		public void InitSimpleMessageBox( Window owner, string message, bool showCancel)
		{
            this.showCancel = showCancel;
            Message = message;

			InitializeComponent();

			if (owner != null)
			{
				Owner = owner;
				Left = owner.Left + (owner.ActualWidth - Width) / 2;
				Top = owner.Top + owner.ActualHeight * 0.2;
			}
            DataContext = this;
		}

        public Visibility ShowCancel
        {
            get { return (showCancel) ? Visibility.Visible : Visibility.Collapsed; }
        }

		public SimpleMessageResult DoModal()
		{
			result = SimpleMessageResult.Cancel;

			if (Owner != null) 
                Owner.Opacity = 0.5;
			ShowDialog(); 
			if (Owner != null) 
                Owner.Opacity = 1.0;

			return result;
		}


		private void OnOK(object sender, RoutedEventArgs e)
		{
            result = SimpleMessageResult.OK;
			Close();
		}

		private void OnCancel(object sender, RoutedEventArgs e)
		{
            result = SimpleMessageResult.Cancel;
			Close();
		}
	}
}
