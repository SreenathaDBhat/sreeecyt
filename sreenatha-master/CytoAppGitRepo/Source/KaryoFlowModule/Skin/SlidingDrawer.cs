﻿using System;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Input;

namespace AI
{
    public class SlidingDrawer : ContentControl
    {
        public static DependencyProperty IsOpenProperty = DependencyProperty.Register("IsOpen", typeof(bool), typeof(SlidingDrawer), new FrameworkPropertyMetadata(OnIsOpenChanged));
        public event EventHandler Opened;

        private FrameworkElement PART_SlidingPanel;

        static SlidingDrawer()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SlidingDrawer), new FrameworkPropertyMetadata(typeof(SlidingDrawer)));
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            MouseLeave += (s, e) => IsOpen = false;

            PART_SlidingPanel = (FrameworkElement)GetTemplateChild("PART_SlidingPanel");
            PART_SlidingPanel.MouseLeftButtonUp += new MouseButtonEventHandler(PART_SlidingPanel_MouseLeftButtonUp);
        }

        private void PART_SlidingPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            return;
        }

        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty, value); }
        }

        static void OnIsOpenChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue.Equals(e.OldValue))
                return;

            SlidingDrawer tis = (SlidingDrawer)sender;

            bool v = (bool)e.NewValue;
            if (v)
            {
                Storyboard sb = (Storyboard)tis.PART_SlidingPanel.FindResource("extend");
                sb.Begin(tis.PART_SlidingPanel);
                sb.Completed += (ss, ee) =>
                {
                    if (tis.Opened != null)
                        tis.Opened(tis, EventArgs.Empty);
                };
            }
            else
            {
                Storyboard sb = (Storyboard)tis.PART_SlidingPanel.FindResource("retract");
                sb.Begin(tis.PART_SlidingPanel);
            }
        }
    }
}
