﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Media;

namespace AI
{
    public class SortaMessageBox : ContentControl
    {
        public static DependencyProperty IsOpenProperty = DependencyProperty.Register("IsOpen", typeof(bool), typeof(SortaMessageBox), new FrameworkPropertyMetadata(false, OnIsOpenChanged));
        private FrameworkElement mainPanel;
        private TranslateTransform mainPanelTransform;
        private DoubleAnimationUsingKeyFrames wobbleAnim;

        static SortaMessageBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SortaMessageBox), new FrameworkPropertyMetadata(typeof(SortaMessageBox)));
        }

        public SortaMessageBox()
        {
            wobbleAnim = new DoubleAnimationUsingKeyFrames();
            TimeSpan time = new TimeSpan();
            int n = 10;

            for (int i = 0; i < n; ++i)
            {
                wobbleAnim.KeyFrames.Add(new DiscreteDoubleKeyFrame((n - i) * 2, KeyTime.FromTimeSpan(time)));
                time = time.Add(TimeSpan.FromMilliseconds(10));

                wobbleAnim.KeyFrames.Add(new DiscreteDoubleKeyFrame(-(n - i) * 2, KeyTime.FromTimeSpan(time)));
                time = time.Add(TimeSpan.FromMilliseconds(10));
            }

            wobbleAnim.KeyFrames.Add(new DiscreteDoubleKeyFrame(0, KeyTime.FromTimeSpan(time)));
        }

        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty, value); }
        }

        private static void OnIsOpenChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var panel = ((SortaMessageBox)sender).mainPanel;
            if(panel != null)
                panel.Visibility = ((bool)e.NewValue) ? Visibility.Visible : Visibility.Collapsed;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            mainPanel = (FrameworkElement)GetTemplateChild("PART_main");
            mainPanelTransform = (TranslateTransform)GetTemplateChild("PART_mainTransform");
            mainPanel.Visibility = IsOpen ? Visibility.Visible : Visibility.Collapsed;

            mainPanel.MouseDown += new MouseButtonEventHandler(mainPanel_MouseDown);
        }

        void mainPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            mainPanelTransform.BeginAnimation(TranslateTransform.XProperty, wobbleAnim);
        }
    }
}
