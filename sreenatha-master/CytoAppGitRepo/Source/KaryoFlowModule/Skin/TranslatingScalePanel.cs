﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;

namespace AI
{
    public class TranslatingScalePanel : Panel
    {
        public static readonly DependencyProperty DomainWidthProperty = DependencyProperty.Register("DomainWidth", typeof(double), typeof(TranslatingScalePanel), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));
        public static readonly DependencyProperty DomainHeightProperty = DependencyProperty.Register("DomainHeight", typeof(double), typeof(TranslatingScalePanel), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender));

        public static readonly DependencyProperty DomainXProperty = DependencyProperty.RegisterAttached("DomainX", typeof(double), typeof(TranslatingScalePanel), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender, new PropertyChangedCallback(OnChildMoved)));
        public static readonly DependencyProperty DomainYProperty = DependencyProperty.RegisterAttached("DomainY", typeof(double), typeof(TranslatingScalePanel), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsRender, new PropertyChangedCallback(OnChildMoved)));

        public TranslatingScalePanel()
        {
            SizeChanged += new SizeChangedEventHandler(TranslatingScalePanel_SizeChanged);
        }

        void TranslatingScalePanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            InvalidateArrange();
        }

        public double DomainWidth
        {
            get { return (double)GetValue(DomainWidthProperty); }
            set { SetValue(DomainWidthProperty, value); }
        }

        public double DomainHeight
        {
            get { return (double)GetValue(DomainHeightProperty); }
            set { SetValue(DomainHeightProperty, value); }
        }

        public static double GetDomainX(UIElement target)
        {
            object r = target.GetValue(DomainXProperty);
            return (r == null || r == DependencyProperty.UnsetValue) ? 0.0 : (double)r;
        }

        public static void SetDomainX(UIElement target, double value)
        {
            target.SetValue(DomainXProperty, value);
        }

        public static double GetDomainY(UIElement target)
        {
            object r = target.GetValue(DomainYProperty);
            return (r == null || r == DependencyProperty.UnsetValue) ? 0.0 : (double)r;
        }

        public static void SetDomainY(UIElement target, double value)
        {
            target.SetValue(DomainYProperty, value);
        }


        protected override Size MeasureOverride(Size availableSize)
        {
            foreach (UIElement child in Children)
                child.Measure(availableSize);

            return base.MeasureOverride(availableSize);
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            double scaleX = ActualWidth / DomainWidth;
            double scaleY = ActualHeight / DomainHeight;
            var flow = (FlowDirection)GetValue(FlowDirectionProperty);
            bool rightToLeft = flow == FlowDirection.RightToLeft;

            foreach (UIElement child in Children)
            {
                UIElement c = child;

                if (child is ContentPresenter || child is ListBoxItem)
                    c = (UIElement)VisualTreeHelper.GetChild(child, 0);

                double domainX = GetDomainX(c);
                double domainY = GetDomainY(c);

                if (rightToLeft)
                {
                    domainX = DomainWidth - domainX;
                }

                double screenX = domainX * scaleX;
                double screenY = domainY * scaleY;

                child.Arrange(new Rect(screenX, screenY, child.DesiredSize.Width, child.DesiredSize.Height));
            }

            return base.ArrangeOverride(finalSize);
        }

        private static void OnChildMoved(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            TranslatingScalePanel parent = VisualTreeWalker.FindParentOfType<TranslatingScalePanel>(sender);
            if (parent != null)
            {
                parent.InvalidateArrange();
            }
        }
    }
}
