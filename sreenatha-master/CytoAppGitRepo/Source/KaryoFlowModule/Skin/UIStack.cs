﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;

namespace AI
{
    public class UIStackCurrentChangedEventArgs : EventArgs
    {
        public UIStackCurrentChangedEventArgs(UIElement oldValue, UIElement newValue, UIStackAction action)
        {
            OldValue = oldValue;
            NewValue = newValue;
            Action = action;
        }

        public UIElement OldValue { get; private set; }
        public UIElement NewValue { get; private set; }
        public UIStackAction Action { get; private set; }
    }

    public enum UIStackResult
    {
        Ok, Cancel
    }

    public enum UIStackAction
    {
        Push, Pop
    }

    public class UIStack : DependencyObject
    {
        public static DependencyProperty StackProperty = DependencyProperty.RegisterAttached("Stack", typeof(UIStack), typeof(UIStack));
        public static DependencyProperty CurrentProperty = DependencyProperty.Register("Current", typeof(UIElement), typeof(UIStack));

        public delegate void PoppedDelegate(UIStackResult result, object state);
        public event EventHandler<UIStackCurrentChangedEventArgs> CurrentChanged;

        private class StackItem
        {
            public UIElement Element { get; set; }
            public PoppedDelegate Popped { get; set; }
            public object State { get; set; }
        }

        private List<StackItem> elements = new List<StackItem>();

        public static UIStack For(UIElement element)
        {
            if (element is Window)
                return GetStack(element);
            else
                return GetStack(VisualTreeWalker.FindParentOfType<Window>(element));
        }

        public void Push(UIElement element)
        {
            Push(element, null, null);
        }

        public void Push(UIElement element, object state, PoppedDelegate popped)
        {
            var old = Current;
            elements.Add(new StackItem
            {
                Element = element,
                Popped = popped,
                State = state
            });

            Current = element;

            if (CurrentChanged != null)
                CurrentChanged(this, new UIStackCurrentChangedEventArgs(old, element, UIStackAction.Push));
        }

        public void Pop(UIStackResult result)
        {
            var old = Current;

            if (elements.Count == 0)
            {
                return;
            }
            else if (elements.Count == 1)
            {
                ExecutePoppedMethod(elements[0], result);
                elements.RemoveAt(0);
                Current = null;
            }
            else
            {
                ExecutePoppedMethod(elements[elements.Count - 1], result);
                elements.RemoveAt(elements.Count - 1);
                Current = elements[elements.Count - 1].Element;
            }

            if (CurrentChanged != null)
                CurrentChanged(this, new UIStackCurrentChangedEventArgs(old, Current, UIStackAction.Pop));
        }

        private void ExecutePoppedMethod(StackItem item, UIStackResult result)
        {
            if (item.Popped != null)
            {
                item.Popped(result, item.State);
            }
        }

        public void SwapCurrent(UIStackResult resultForExisting, UIElement element)
        {
            SwapCurrent(resultForExisting, element, null, null);
        }

        public void SwapCurrent(UIStackResult resultForExisting, UIElement element, object state, PoppedDelegate popped)
        {
            //  Pop the current page and 
            //  push the new element on the stack
            var old = Current;
            var item = elements.Where(e => e.Element == old).First();
            
            elements.Remove(item);

            ExecutePoppedMethod(item, resultForExisting);

            if (CurrentChanged != null)
                CurrentChanged(this, new UIStackCurrentChangedEventArgs(old, Current, UIStackAction.Pop));

            elements.Add(new StackItem
            {
                Element = element,
                Popped = popped,
                State = state
            });

            Current = element;

            if (CurrentChanged != null)
                CurrentChanged(this, new UIStackCurrentChangedEventArgs(old, Current, UIStackAction.Push));
        }

        public UIElement Current
        {
            get { return (UIElement)GetValue(CurrentProperty); }
            private set { SetValue(CurrentProperty, value); }
        }

        public static UIStack GetStack(UIElement target)
        {
            object r = target.GetValue(StackProperty);
            return (r == null || r == DependencyProperty.UnsetValue) ? null : (UIStack)r;
        }

        public static void SetStack(UIElement target, UIStack value)
        {
            target.SetValue(StackProperty, value);
        }

        public int Count
        {
            get { return elements.Count; }
        }

        public IEnumerable<UIElement> WholeStack
        {
            get
            {
                var items = from e in elements select e.Element;
                return items.Reverse();
            }
        }
    }
}
