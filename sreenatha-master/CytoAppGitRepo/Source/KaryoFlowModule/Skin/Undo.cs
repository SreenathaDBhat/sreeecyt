﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace AI
{
    public interface IUndoItem
    {
        void Undo(object state);
    }

    public interface IUndoListAdder
    {
        void Add(IUndoItem item);
    }

    public class UndoList : Notifier, IUndoListAdder
    {
        private const int maxSize = 100;
        private readonly List<IUndoItem> undoList;
        private List<IUndoItem> record;

        public event EventHandler UndoCompleted;


        public UndoList()
        {
            undoList = new List<IUndoItem>();
        }

        public void Add(IUndoItem item)
        {
            if (record != null)
            {
                record.Add(item);
            }
            else
            {
                undoList.Add(item);

                while (undoList.Count > maxSize)
                    undoList.RemoveAt(0);

                Notify("HasContent");
            }
        }

        public delegate bool RepetitionTest<T>(T item);

        public bool TestLastItem<T>(RepetitionTest<T> test)
        {
            var last = undoList.LastOrDefault();
            if(record != null)
                last = record.LastOrDefault();

            if (last == null || last.GetType() != typeof(T))
                return false;

            return test((T)last);
        }

        public void Undo(object state)
        {
            IUndoItem item = undoList.Last();
            undoList.Remove(item);
            item.Undo(state);

            Notify("HasContent");

            if (UndoCompleted != null)
                UndoCompleted(this, EventArgs.Empty);
        }

        public void BeginRecording()
        {
            record = new List<IUndoItem>();
        }

        public void EndRecording()
        {
            EndRecording(null);
        }

        public void EndRecording(ThreadStart undone)
        {
            if (record.Count > 0)
            {
                undoList.Add(new CompositeUndoItem(record, undone));
            }

            record = null;
        }

        public void Clear()
        {
            record = null;
            undoList.Clear();
        }

        public bool HasContent
        {
            get { return undoList.Count > 0; }
        }
    }

    public class CompositeUndoItem : IUndoItem
    {
        private IEnumerable<IUndoItem> list;
        private ThreadStart callback;

        public CompositeUndoItem(IEnumerable<IUndoItem> list, ThreadStart callback)
        {
            this.list = list;
            this.callback = callback;  
        }

        public void Undo(object state)
        {
            foreach (var item in list.Reverse())
            {
                item.Undo(state);
            }

            if(callback != null)
                callback();
        }
    }
}
