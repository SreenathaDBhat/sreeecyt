﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AI
{
	/// <summary>
	/// Modal dialog prompting user with a message.
	/// </summary>
	public partial class UserPrompt : Window
	{
		enum UserPromptResult
		{
			Save = 0,
			Discard = 1,
			Cancel = 2
		}

		private UserPromptResult result;

		public UserPrompt( Window owner)
		{
			InitializeComponent();

			if (owner != null)
			{
				Owner = owner;
				Left = owner.Left + (owner.ActualWidth - Width) / 2;
				Top = owner.Top + owner.ActualHeight * 0.1;
			}
		}

		public int DoModal()
		{
			result = UserPromptResult.Cancel;

			if (Owner != null) Owner.Opacity = 0.5;
			ShowDialog(); // this "blocks" until window is closed
			if (Owner != null) Owner.Opacity = 1.0;

			return (int) result;
		}

		private void OnSave(object sender, RoutedEventArgs e)
		{
			result = UserPromptResult.Save;
			Close();
		}

		private void OnDiscard(object sender, RoutedEventArgs e)
		{
			result = UserPromptResult.Discard;
			Close();
		}

		private void OnCancel(object sender, RoutedEventArgs e)
		{
			result = UserPromptResult.Cancel;
			Close();
		}
	}
}
