﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Collections.Generic;

namespace AI
{
    public sealed class VisualTreeWalker
    {
        private VisualTreeWalker()
        {
        }

        public static IEnumerable<DependencyObject> GetChildren(DependencyObject parent)
        {
            int n = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < n; ++i)
            {
                yield return VisualTreeHelper.GetChild(parent, i);
            }
        }

        public static T FindChildOfType<T>(DependencyObject parent) where T : DependencyObject
        {
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; ++i)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                if (child.GetType() == typeof(T))
                    return (T)child;

                T foundChild = FindChildOfType<T>(child);
                if (foundChild != null)
                    return foundChild;
            }

            return default(T);
        }

        public delegate bool FindChildDelegate(DependencyObject child);

        public static DependencyObject FindChildWhere(DependencyObject parent, FindChildDelegate cd)
        {
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; ++i)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                if(cd(child))
                    return child;

                var foundChild = FindChildWhere(child, cd);
                if (foundChild != null)
                    return foundChild;
            }

            return null;
        }


        public static T FindChildOfType<T>(DependencyObject parent, string elementName) where T : UIElement
        {
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; ++i)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                if (child.GetType() == typeof(T))
                {
                    T ch = (T)child;
                    string name = (string)ch.GetValue(Control.NameProperty);
                    if (name == elementName)
                        return ch;
                }

                T foundChild = FindChildOfType<T>(child);
                if (foundChild != null)
                    return foundChild;
            }

            return default(T);
        }

        public static T FindParentOfType<T>(DependencyObject obj) where T : DependencyObject
        {
            DependencyObject parent = VisualTreeHelper.GetParent(obj);
            while (parent != null)
            {
                if (parent.GetType() == typeof(T) || parent.GetType().IsSubclassOf(typeof(T)))
                    return (T)parent;
                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return null;
        }
    }
}
