﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using System.IO;
using AI.Connect;
using System.Threading;
using System.Windows.Threading;
using System.ComponentModel;
using System.Windows.Data;
using System.Collections.Generic;
using System.Windows.Controls;

namespace AI
{
    public partial class CaseListPage : INotifyPropertyChanged
    {
        internal class CaseInfo : GroupedSlides
        {
            public string Status { get; set; }

            public static new CaseInfo ForCase(Connect.Database db, Guid caseId, IEnumerable<SlideStatusInfo> slides)
            {
                var caseSlides = from s in slides
                                 where s.CaseId == caseId
                                 select s;


                var dbCase = (from c in db.Cases
                              where c.caseId == caseId
                              select c).First();

                return new CaseInfo
                {
                    CaseId = caseId,
                    CaseName = Connect.LIMS.CaseName(dbCase.caseId),
                    Slides = caseSlides,
                    Status = ""
                };
            }
        }

        public static readonly RoutedUICommand LaunchKaryotypeWorkflow = new RoutedUICommand("Launch Karyotype Workflow", "LaunchKaryotypeWorkflow", typeof(CaseListPage));
        public static readonly RoutedUICommand LaunchScoringWorkflow = new RoutedUICommand("Launch Scoring Workflow", "LaunchScoringWorkflow", typeof(CaseListPage));
        public static readonly RoutedUICommand LaunchReviewersWorkflow = new RoutedUICommand("Launch Reviewers Workflow", "LaunchReviewersWorkflow", typeof(CaseListPage));
        public static readonly RoutedUICommand LaunchManualCapture = new RoutedUICommand("Launch Manual Capture", "LaunchManualCapture", typeof(CaseListPage));
        public static readonly RoutedUICommand LaunchReviewWorkflow = new RoutedUICommand("Launch Review Workflow", "LaunchReviewWorkflow", typeof(CaseListPage));
        public static readonly RoutedUICommand ImportCase = new RoutedUICommand("Import", "ImportExecute", typeof(CaseListPage));
        public static readonly RoutedUICommand CytovisionImport = new RoutedUICommand("CytovisionImport", "CytovisionImportExecute", typeof(CaseListPage));

        public static readonly DependencyProperty SelectedCaseProperty = DependencyProperty.Register("SelectedCase", typeof(CaseInfo), typeof(CaseListPage));

        public event PropertyChangedEventHandler PropertyChanged;
        private CaseImport45000to46000 importer;
        private Boolean modifyRights = false;
        private SlideInfoController controller = null;
        private Boolean sendToController = false;

        public CaseListPage()
        {
            InitializeComponent();

            CaseFinder.PropertyChanged += CaseFinderPropertyChanged;
            CaseFinder.Refresh();

            controller = new SlideInfoController();
            controller.Zap += new EventHandler<BarcodeZapEventArgs>(controller_BarcodeZapped);

            Loaded += (s, e) =>
            {
                CaseFinder.Refresh();
                Keyboard.Focus(searchText); 
                searchText.SelectAll(); 

                SlideStatusWatcher.CheckForChanges();
            };
        }

        internal CaseInfo SelectedCase
        {
            get { return (CaseInfo)GetValue(SelectedCaseProperty); }
            set { SetValue(SelectedCaseProperty, value); }
        }

        void controller_BarcodeZapped(object sender, BarcodeZapEventArgs e)
        {
            SlideInformation dlg = new SlideInformation(controller);
            dlg.ShowDialog();
            if (controller.ProceedToTask)
            {
                switch (controller.SlideStatusSequence)
                {
                    case 0: // Unassigned
                        break;
                    case 1:
                    case 3: // Ready for scan/capture
                        MiscCommands.LaunchFishCapture.Execute(null, this);
                        break;
                    case 2: // Ready for Markup
                        UIStack.For(this).Push(new MarkupSlideForCapture(controller.SlideId));
                        break;

                    case 4:  // Ready for analysis
                        UIStack.For(this).Push(new InterphaseWorkflow(controller.CaseId, controller.CaseName, controller.SlideId));
                        break;
                }
            }
        }

        void CaseFinderPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Results")
            {
                if (CaseFinder.Results.Count() == 1)
                {
                    var c = CaseFinder.Results.First();

                    SelectCase(c);
                }
                else
                {
                    caseInfo = null;
                }
            }
        }

        private void ConfigureOptions()
        {
            //  By default, all options are disabled and we start switching them on in
            //  sequence.  Note that the PRINT option is independant of the others.

            //  The sequence is check MODIFY status first, and if that is disabled then
            //  everything else remains disabled.  If it is enabled then check if the
            //  user has CAPTURE rights.

            //  Disable buttons according to user access rights
            newCaseButton.IsEnabled = false;

            if (Security.Security.IsAccessRightsEnabled)
            {
                modifyRights = Security.Security.UserOptionEnabled(Environment.UserName, Security.DefaultStatus.InProgress, Security.DefaultFlags.ModifyChange);
            }
            else
                modifyRights = true;

            if (modifyRights)
            {
                if (Security.Security.UserOptionEnabled(Environment.UserName, Security.DefaultStatus.InProgress, Security.DefaultFlags.CreateCase))
                {
                    //  Enable Capture buttons
                    newCaseButton.IsEnabled = true;
                }
            }
        }

        private CaseFinder CaseFinder
        {
            get { return (CaseFinder)FindResource("caseFinder"); }
        }

        private SlideStatusWatcher SlideStatusWatcher
        {
            get { return (SlideStatusWatcher)FindResource("ssw"); }
        }


        internal CaseInfo caseInfo
        {
            get { return (CaseInfo)GetValue(SelectedCaseProperty); }
            set { SetValue(SelectedCaseProperty, value); }
        }

        public CaseImport45000to46000 Importer
        {
            get { return importer; }
            set { importer = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Importer")); } }
        }

        private void CaseImport(DirectoryInfo dir, string caseName)
        {
            Importer = new CaseImport45000to46000(dir, caseName, Dispatcher);

            ThreadPool.QueueUserWorkItem(delegate
            {
                try
                {
                    var caseId = Importer.Import();
                    Database db = new Database();
                    var dbCase = db.Cases.Where(c => c.caseId == caseId).First();
                    
                    Security.Security.AddLogEntry(AI.Logging.EventType.CaseImported, caseId, /*dbCase.status*/ "", Connect.LIMS.CaseName(dbCase.caseId));

                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        Importer.Dispose();
                        Importer = null;
                        CaseFinder.Refresh();

                    }, DispatcherPriority.Normal);
                }
                catch (Exception ex)
                {
                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        MessageBox.Show(ex.ToString());
                    }, DispatcherPriority.Normal);
                }
            });
        }

        private void CanNewCaseExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //  Security check
            if (Security.Security.IsAccessRightsEnabled)
            {
                e.CanExecute = Security.Security.UserOptionEnabled(Environment.UserName, Security.DefaultStatus.InProgress, Security.DefaultFlags.CreateCase);
            }
            else
                e.CanExecute = true;
        }

        private void NewCaseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            CreateCase("");
        }

        private Case CreateCase(string startingName)
        {
            genericBlackout.Visibility = Visibility.Visible;
            try
            {
                NewCaseWindow wnd = new NewCaseWindow(startingName, ((TubsApp)Application.Current).Strings)
                {
                    Owner = VisualTreeWalker.FindParentOfType<Window>(this)
                };
                if (!wnd.ShowDialog().Value)
                    return null;

                CaseManagement.CreateCase(wnd.Case.Name);
                CaseFinder.Refresh();

                //Security.Security.AddLogEntry(AI.Logging.EventType.CaseCreated, wnd.Case.Id, /*wnd.Case.Status*/"", wnd.Case.Name);

                return wnd.Case;
            }
            finally
            {
                genericBlackout.Visibility = Visibility.Collapsed;
            }
        }

        private void OnSearchKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Escape)
                return;

            e.Handled = true;
            CaseFinder.Search = "";
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {

            //  Because we have barcode capability on this
            //  screen we need to check for redirection 
            //  of the keys.

            if (e.Key == Key.F23)  // special char identifying start of barcode characters
                sendToController = true;

            if (e.Key == Key.F24)  // special char identifying end of barcode characters
            {
                // this char still needs to goto to the controller
                controller.CharacterRead(e);
                e.Handled = true;
                sendToController = false;
            }

            if (sendToController)
            {
                controller.CharacterRead(e);
                e.Handled = true;
            }
        }



        private void CanImportExportExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            ////  Security check
            if (Security.Security.IsAccessRightsEnabled)
            {
                e.CanExecute = Security.Security.UserOptionEnabled(Environment.UserName, Security.DefaultStatus.InProgress, Security.DefaultFlags.Archive);
            }
            else
                e.CanExecute = true;
        }

        private void CytovisionImportExecute(object sender, ExecutedRoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DirectoryInfo dir = new DirectoryInfo(dlg.SelectedPath);
                string name = dir.Name;

                if (CaseExists(name))
                {
                    var newCase = CreateCase(name);
                    if (newCase == null)
                        return;

                    name = newCase.Name;
                }

                CaseImport(dir, name);
            }
        }

        private void ImportExecute(object sender, ExecutedRoutedEventArgs e)
        {
            const string EXTENSION = ".Archive";

            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            dlg.Filter = "Archive files (*.Archive)|*" + EXTENSION;
            dlg.RestoreDirectory = true;
            dlg.InitialDirectory = DataTransfer.GetLastExportLocation(); 

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DirectoryInfo dir = new DirectoryInfo(dlg.FileName);
                DataTransfer.SaveExportLocation(dir.FullName);
                string filename = dir.Name;
                string folder = dlg.FileName.Substring(0, dlg.FileName.Length - filename.Length);

                DatatransferController dtController = new DatatransferController();
                XferProgress prog = new XferProgress(dtController);
                prog.Show();

                Importer importer = new Importer(dtController, folder);

                ThreadPool.QueueUserWorkItem(delegate
                {
                    importer.Import(filename, true, "80085");

                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        CaseFinder.Refresh();
                    }, DispatcherPriority.Normal);


                }, DispatcherPriority.Normal);
            }
        }


        private static bool CaseExists(string caseName)
        {
            using (var lims = Connect.LIMS.New())
            {
                return lims.FindCase(caseName) != null;
            }
        }

        private void CanLaunchExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var caseInfo = (CaseInfo)e.Parameter;

            if (Security.Security.IsAccessRightsEnabled)
            {
                e.CanExecute = Security.Security.UserOptionEnabled(Environment.UserName, caseInfo.Status, Security.DefaultFlags.OpenCase);
            }
            else
                e.CanExecute = true;
        }

        private void LaunchKaryotypeExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var caseInfo = (CaseInfo)e.Parameter;

            if (Brightfield.WorkflowController.CreateCellsForFrames(caseInfo.CaseId) == 0)
            {
                return;
            }

            Security.Security.AddLogEntry(AI.Logging.EventType.CaseOpened, caseInfo.CaseId, caseInfo.Status, caseInfo.CaseName + " - Karyotyping");

            var controller = new Brightfield.WorkflowController(Dispatcher, Connect.LIMS.Case(caseInfo.CaseId));
            UIStack.For(this).Push(new Brightfield.AnalysisPage(controller, null));

        }

        private void LaunchScoringExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var caseInfo = (CaseInfo)e.Parameter;
            Security.Security.AddLogEntry(AI.Logging.EventType.CaseOpened, caseInfo.CaseId, caseInfo.Status, caseInfo.CaseName + " - Scoring");
            UIStack.For(this).Push(new InterphaseWorkflow(caseInfo.CaseId, caseInfo.CaseName, Guid.Empty));
        }

        private void CanLaunchReviewExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var caseInfo = (CaseInfo)e.Parameter;

            //  Security check
            if ((Security.Security.IsAccessRightsEnabled) && (caseInfo != null))
            {
                e.CanExecute = Security.Security.UserOptionEnabled(Environment.UserName, caseInfo.Status, Security.DefaultFlags.OpenCase);
            }
            else
                e.CanExecute = true;
        }

        private void LaunchReviewExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var caseInfo = (CaseInfo)e.Parameter;
            Security.Security.AddLogEntry(AI.Logging.EventType.CaseOpened, caseInfo.CaseId, caseInfo.Status, caseInfo.CaseName + " - Review");            
        }

        private void CanLaunchManualCapture(object sender, CanExecuteRoutedEventArgs e)
        {
            var caseInfo = (CaseInfo)e.Parameter;

            if ((Security.Security.IsAccessRightsEnabled) && (caseInfo != null))
            {
                e.CanExecute = Security.Security.UserOptionEnabled(Environment.UserName, caseInfo.Status, Security.DefaultFlags.OpenCase);
            }
            else
                e.CanExecute = true;
        }

        private void LaunchManualCaptureExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var caseInfo = (CaseInfo)e.Parameter;

            var cam = new Camera();
            bool connected = HardwareController.ConnectHardwareWindow.Connect(Hardware.Instance, cam);
            if (connected)
            {
                Security.Security.AddLogEntry(AI.Logging.EventType.CaseOpened, caseInfo.CaseId, caseInfo.Status, caseInfo.CaseName + " - BrightField Capture");
                UIStack.For(this).Push(new BrightFieldCaptureWindow(caseInfo.CaseId, Hardware.Instance, cam), null, (r, s) => cam.Shutdown());
            }
        }

        private void AlwaysExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void OnChangeNetwork(object sender, RoutedEventArgs e)
        {
            genericBlackout.Visibility = Visibility.Visible;
            try
            {
                string revert = Locations.ConnectionString;

                while (true)
                {
                    NetworkConnect connectScreen = new NetworkConnect
                    {
                        Owner = VisualTreeWalker.FindParentOfType<Window>(this)
                    };
                    bool result = connectScreen.ShowDialog().GetValueOrDefault(false);

                    if (!result)
                    {
                        Locations.ConnectionString = revert;
                        Locations.Save();
                        break;
                    }

                    Locations.Save();

                    if (AI.Connect.NetworkTest.TestSql())
                    {
                        CaseFinder.Refresh();
                        break;
                    }
                }
            }
            finally
            {
                genericBlackout.Visibility = Visibility.Collapsed;
            }
        }

        private void btnNotifications_Click(object sender, RoutedEventArgs e)
        {
            if (SlideStatusWatcher != null)
            {
                Notifications n = new Notifications(SlideStatusWatcher);
                n.Owner = VisualTreeWalker.FindParentOfType<Window>(this);
                n.ShowDialog();

                SlideStatusWatcher.ResetChanges();
                SlideStatusWatcher.SaveStatusMonitorSettings();
                SlideStatusWatcher.CheckForChanges();
            }
        }

        private void CanExecuteTrue(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RespondToReadyForMarkup(object sender, ExecutedRoutedEventArgs e)
        {
            var slide = (SlideStatusInfo)e.Parameter;
            UIStack.For(this).Push(new MarkupSlideForCapture(slide.SlideId));
        }

        private void RespondToReadyForCapture(object sender, ExecutedRoutedEventArgs e)
        {
            MiscCommands.LaunchFishCapture.Execute(null, this);
        }

        private void RespondToReadyForAnalysis(object sender, ExecutedRoutedEventArgs e)
        {
            var slide = (SlideStatusInfo)e.Parameter;
            UIStack.For(this).Push(new InterphaseWorkflow(slide.CaseId, slide.CaseName, slide.SlideId));
        }

        private void SelectCase(Case c)
        {
            if (c == null)
            {
                caseInfo = null;
                return;
            }

            using (var db = new Connect.Database())
            using (var lims = Connect.LIMS.New())
            {
                var dbCase = db.Cases.Where(dbc => dbc.limsRef == c.Id).FirstOrDefault();
                if (dbCase != null)
                {
                    var slides = from slide in db.Slides
                                 where slide.caseId == dbCase.caseId
                                 select slide;

                    var statuses = (from s in db.SlideStatus
                                    orderby s.sequence
                                    select s.description).ToArray();

                    var slideInfos = (from s in slides
                                      select new SlideStatusInfo
                                      {
                                          SlideId = s.slideId,
                                          SlideName = lims.GetSlide(s.limsRef).Name,
                                          StatusSequence = (byte)s.slideStatus,
                                          CaseId = s.caseId,
                                          WhenChanged = s.statusChanged,
                                          CaseName = c.Name,
                                      }).ToArray();

                    foreach (var s in slideInfos)
                        s.SlideStatus = statuses[s.StatusSequence];

                    caseInfo = CaseInfo.ForCase(db, dbCase.caseId, slideInfos);
                }
                else
                {
                    caseInfo = new CaseInfo
                    {
                        CaseId = Guid.Empty,
                        CaseName = c.Name

                    };
                }                
            }
        }

        private void OnSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 1)
            {
                SelectCase(e.AddedItems[0] as Case);
            }

            caseList.SelectedItem = null;
        }

        private void OnBackToSearchResults(object sender, RoutedEventArgs e)
        {
            SelectCase(null);
        }

        private void OnShowMoreCaseOptions(object sender, RoutedEventArgs e)
        {
            ((Button)sender).Focus();
            ((Button)sender).ContextMenu.IsOpen = true;
        }

        private void OnExportCase(object sender, RoutedEventArgs e)
        {
            var selectedCase = (CaseInfo)((FrameworkElement)sender).Tag;

            const string EXTENSION = ".Archive";

            System.Windows.Forms.SaveFileDialog dlg = new System.Windows.Forms.SaveFileDialog();
            dlg.Filter = "Archive files (*.Archive)|*" + EXTENSION;
            dlg.RestoreDirectory = true;
            dlg.InitialDirectory = DataTransfer.GetLastExportLocation();

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DirectoryInfo dir = new DirectoryInfo(dlg.FileName);
                string casename = dir.Name.Substring(0, dir.Name.Length - EXTENSION.Length);
                string folder = dlg.FileName.Substring(0, dlg.FileName.Length - (casename.Length + EXTENSION.Length));
                DataTransfer.SaveExportLocation(dir.FullName);

                DatatransferController controller = (DatatransferController)this.FindResource("xferController");
                controller.ShowProgressBar = true;
                Guid id = caseInfo.CaseId;

                ThreadPool.QueueUserWorkItem(delegate
                {
                    //Archiver archiver = new Archiver(controller, folder);
                    //archiver.Export(dir.Name, id, false, true, true, true, true, true, true, false, string.Empty);
                    //archiver.Dispose();
                    //archiver = null;
                    //controller.ShowProgressBar = false;
                    //controller.TotalSteps = 100;
                    //controller.CurrentStep = 0;
                    throw new NotImplementedException();

                }, DispatcherPriority.Normal);
            }
        }

        private void OnLaunchLegacyGallery(object sender, RoutedEventArgs e)
        {
            if (LegacyGalleryController.CaseHasLegacyImages(caseInfo.CaseId))
                UIStack.For(this).Push(new LegacyGalleryPage(caseInfo.CaseId));
        }
    }
}
