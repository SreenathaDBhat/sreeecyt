SET VERSION=1.0.0.%CCNetLabel%
SET DEPLOY=Deployment
SET DEPLOYBIN=%DEPLOY%\%VERSION%
SET MAGE="C:\Program Files\Microsoft SDKs\Windows\v6.0A\bin\mage.exe"
SET MSBUILD="C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\msbuild.exe"

SET TUBSDEPLOYSHARE=\\whughes\CVFlowDeploy\Tubs\

ECHO %CCNetLabel%

rd %DEPLOY% /Q /S
md %DEPLOY%
md %DEPLOYBIN%

xcopy bin\release\* %DEPLOYBIN% /Y /E
copy tubs.ico %DEPLOYBIN% /Y

del %DEPLOYBIN%\*.pdb
del %DEPLOYBIN%\locbaml.exe
del %DEPLOYBIN%\tubs.vshost.*
del %DEPLOYBIN%\tubs.application
del %DEPLOYBIN%\tubs.exe.manifest

%MAGE% -New Application -ToFile %DEPLOYBIN%\tubs.exe.manifest -Name "Tubs" -Version %VERSION% -FromDirectory %DEPLOYBIN% -IconFile "tubs.ico"

%MAGE% -Sign %DEPLOYBIN%\tubs.exe.manifest -CertFile Tubs_TemporaryKey.pfx

%MAGE% -New Deployment -ToFile %DEPLOY%\Tubs.application -Install true -Name "Tubs" -Publisher "Genetix" -Version %VERSION% -AppManifest %DEPLOYBIN%\tubs.exe.manifest -ProviderUrl file:%TUBSDEPLOYSHARE%Tubs.application

%MAGE% -Sign %DEPLOY%\Tubs.application -CertFile Tubs_TemporaryKey.pfx

%MSBUILD% tubsbootstrap.msbuild

rd %TUBSDEPLOYSHARE% /Q /S
xcopy %DEPLOY%\* %TUBSDEPLOYSHARE% /Y /E



