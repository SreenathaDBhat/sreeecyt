﻿using System;
using System.Windows;
using System.IO;
using AI.Connect;
using System.Windows.Input;
using System.Threading;
using System.Windows.Threading;
using System.Windows.Interop;
using System.Diagnostics;
using System.Windows.Controls;
using System.Linq;
using AI.Authentication;

namespace AI
{
    public partial class RootWindow : Window
    {

        public RootWindow()
        {
            var stack = new UIStack();
            stack.CurrentChanged += (s, e) =>
                {
                    //  This assignment will trigger the 
                    //  Unloaded event on the OldValue page
                    //  and the Loaded event on the NewValue
                    //  page.
                    navFrame.Child = e.NewValue;

                    
                    //  If finished with page then
                    //  dispose of it so GC can tidy up.
                    if (e.Action == UIStackAction.Pop)
                    {
                        var disposable = e.OldValue as IDisposable;
                        if (disposable != null)
                        {
                            ((IDisposable)(e.OldValue)).Dispose();
                        }
                    }
                };

            UIStack.SetStack(this, stack);

            Dispatcher.ShutdownStarted += Dispatcher_ShutdownStarted;

            InitializeComponent();
           
            WindowLocations.Handle(this);

            Loaded += new RoutedEventHandler(RootWindow_Loaded);

			DataContext = this;
        }

		public string ApplicationVersion
		{
			get { return Application.Current == null ? "" : ((TubsApp)Application.Current).ApplicationVersion; }
		}

        private bool firstTimeClosing = true;
		protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
		{
            // Anything setting e.Cancel to true will prevent the app closing
            // JR:......is that what 'e.Cancel = true' means then? Good to know.
            base.OnClosing(e);

            savingChanges.Visibility = Visibility.Visible;

            // When we hit the big red x we cancel the initial close request so that we can
            // put up a save progress display, then tell all the pages in the stack to save.
            // Only then do we fire off another close request, which sails right through 
            // (because firstTimeClosing is now false).
            var stack = UIStack.For(this);
            if (firstTimeClosing)
            {
                firstTimeClosing = false;
                e.Cancel = true;

                ThreadPool.QueueUserWorkItem(state =>
                {
                    KillStuff((UIStack)state);

                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        Close();

                    }, DispatcherPriority.Normal);
                }, stack);
            }
            else
            {
                KillStuff(stack);
            }
		}

        private void KillStuff(UIStack stack)
        {
            foreach (var item in stack.WholeStack)
            {
                if(item is IClosing)
                    ((IClosing)item).Closed();
            }

            Hardware.Instance.ShutDown();
            Security.Security.Logout();
        }

        void Dispatcher_ShutdownStarted(object sender, EventArgs e)
        {
            Hardware.Instance.ShutDown();

            while (UIStack.For(this).Count > 0)
            {
                UIStack.For(this).Pop(UIStackResult.Cancel);
            }
        }

        void RootWindow_Loaded(object sender, RoutedEventArgs e)
        {

            WindowInteropHelper helper = new WindowInteropHelper(this);
            int hWnd = helper.Handle.ToInt32();

            uint processID = (uint)Process.GetCurrentProcess().Id;
            uint threadID = (uint)Thread.CurrentThread.ManagedThreadId;

            if (!CheckLicence())
            {
                DialogResult = false;
                Close();
            }
            
            ThreadPool.QueueUserWorkItem(delegate
            {
                while (!TestConnection())
                {
                    Dispatcher.Invoke((ThreadStart)delegate
                    {
                        NetworkConnect connectScreen = new NetworkConnect();
                        connectScreen.Owner = this;
                        bool result = connectScreen.ShowDialog().GetValueOrDefault(false);

                        if (!result)
                        {
                            DialogResult = false;
                            Close();
                        }
                        else
                        {
                            Locations.Save();
                        }

                    }, DispatcherPriority.Normal);
                }


                Dispatcher.Invoke((ThreadStart)delegate
                {
					try
					{
						Hardware.Instance.Init(hWnd, processID, threadID);

						networkBusy.Visibility = Visibility.Collapsed;
						navFrame.Focus();
						Keyboard.Focus(navFrame);

						Security.Security.Login();

						UIStack.For(this).Push(new CaseListPage());
					}
					catch (Exception ex)
					{
						ExceptionUtility.ShowLog(ex, "CytoVision");
					}
                });

            });
        }

        private bool CheckLicence()
        {
#if DEBUG
            return true;
#else

#if checkdongle
            SimpleMessageResult result = SimpleMessageResult.OK;
            if (!NetDongle.Initiate(NetDongleFeatures.CVFlow))
            {
                SimpleMessageBox box = new SimpleMessageBox(this, LicenceMessage + "\n\n" + (string)FindResource("Contact"));
                result = box.DoModal();
                return false;
            }
            else
            {
                NetDongle.DongleEvent += new EventHandler<NetDongleEventArgs>(MonitorLicence);
                NetDongle.StartMonitor();
                return true;
            }
#else
            return true;
#endif
#endif

        }

        private string LicenceMessage
        {
            get
            {
                string message = string.Empty;

                if (NetDongle.ValidLicense)
                    message = "Authenticated";
                else if (NetDongle.TooManyUsers)
                    message = (string)FindResource("LicenceExceeded");
                else if (NetDongle.NoDongle)
                    message = (string)FindResource("NoLicence");
                else
                    message = NetDongle.StatusString;

                return message;
            }
        }

        private void MonitorLicence(object o, NetDongleEventArgs args)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (System.Threading.ThreadStart)delegate
            {
				this.Title = "CytoVision 4.6 Alpha:  " + LicenceMessage;

                if (!args.IsValid)
                {
                    NetDongle.DongleEvent -= new EventHandler<NetDongleEventArgs>(MonitorLicence);

                    SimpleMessageBox box = new SimpleMessageBox(this, LicenceMessage + "\n\n" + (string)FindResource("Contact"));
                    SimpleMessageResult result = box.DoModal();
                    if (result == SimpleMessageResult.OK)
                        NetDongle.Initiate(NetDongleFeatures.CVFlow);

                    NetDongle.DongleEvent += new EventHandler<NetDongleEventArgs>(MonitorLicence);
                }
            });
        }

        private static bool TestConnection()
        {
            return (Locations.ConfigFile.Exists && AI.Connect.NetworkTest.TestSql());
        }

        private void CanFishCaptureExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void FishCaptureExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Camera cam = new Camera();
            bool connected = HardwareController.ConnectHardwareWindow.Connect(Hardware.Instance, cam);

            if (connected)
            {
                Guid slideId = (e.Parameter == null || !(e.Parameter is Guid)) ? Guid.Empty : (Guid)e.Parameter;
                UIStack.For(this).Push(new FishCapture(Hardware.Instance, cam, slideId), null, (r, s) => cam.Shutdown());
            }
        }
    }
}
