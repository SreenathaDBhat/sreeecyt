﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AI
{
    /// <summary>
    /// Interaction logic for SlideInfo.xaml
    /// </summary>
    public partial class SlideInformation : Window
    {
        private SlideInfoController controller;

        public SlideInformation(SlideInfoController sic)
        {
            controller = sic;
            this.DataContext = controller;
            InitializeComponent();
        }

        private void statusName_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;

            if (button.Tag != null)
            {
                //byte seq = ((StatusSequence)button.Tag).Sequence;
                controller.SlideStatusSequence = ((StatusSequence)button.Tag).Sequence;
                //string newStatus = ((StatusSequence)button.Tag).Description;
                statusPopup.IsOpen = false;
            }

        }

        private void StatusChangeButton_Click(object sender, RoutedEventArgs e)
        {
            statusList.DataContext = controller.Statuses;
            statusPopup.IsOpen = true;
        }

        private void saveStatus_Click(object sender, RoutedEventArgs e)
        {
			try
			{
				controller.SaveStatus();
			}
			catch (Exception ex)
			{
				ExceptionUtility.ShowLog(ex, "Slide Information");
			}
        }

        private void proceedToNextTask_Click(object sender, RoutedEventArgs e)
        {
            controller.ProceedToTask = true;
            this.Close();
        }
    }
}
