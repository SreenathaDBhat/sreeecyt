﻿using System;
using System.Linq;
using System.Windows;
using System.IO;
using Microsoft.Win32;
using System.Windows.Input;
using System.Windows.Interop;
using System.Reflection;
using AI.Security;
using System.Diagnostics;

namespace AI
{
    public partial class TubsApp : Application
    {
		private const string baseAppVersion = "Build ";
		private string appVersion;
        private StringTable strings;

        public TubsApp()
        {
            Connect.LIMS.SetType(typeof(LimsFallback.LimsBase));

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

			Assembly exeAssembly = Assembly.GetExecutingAssembly();
			object[] fileversions = exeAssembly.GetCustomAttributes(typeof(AssemblyFileVersionAttribute), true);

			appVersion = baseAppVersion + ((AssemblyFileVersionAttribute)fileversions[0]).Version;

            KillCytovisionExecutables();
        }

        private void KillCytovisionExecutables()
        {
            KillProcess("GrabServer");
            KillProcess("MicServer");
        }

        private void KillProcess(string p)
        {
            var process = Process.GetProcessesByName(p).FirstOrDefault();
            if (process != null)
            {
                process.Kill();
            }
        }

		public string ApplicationVersion
		{
			get { return appVersion; }
		}

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.ExceptionObject.ToString());

            if (e.IsTerminating)
            {
                Security.Security.AddLogEntry(AI.Logging.EventType.UnhandledException, Guid.Empty, string.Empty, string.Empty);
                Clearup();
            }
        }

        private void Clearup()
        {
            Security.Security.Logout();
            Authentication.NetDongle.Finish();
        }

        public StringTable Strings
        {
            get { return strings; }

        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            strings = new StringTable(Resources);

//			bool useCulture = true; // Issue with ClickOnce picking up language culture at moment
			bool useCulture = false;


#if DEBUG
			// In DEBUG dont use current culture use default en-US, so that xaml changes dont break execution.
			// Can be overridden by /L command line arguement if debugging languages but you must have language folders,
			// eg copy from build server Tubs/bin/release.
			useCulture = false;
			foreach (string arg in e.Args)
			{
				switch (arg.ToUpper())
				{
					case "/L": useCulture = true; break;
					default : break;
				}
			}
#endif
			if (useCulture)
			{
				// Control Panel "Regional and Language Options" applet sets CurrentCulture below BUT you have to run the 
				// app twice before it sees the change when running with the debugger - dont know why. If running without 
				// debugging it sees the change straight off.
				System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture;
			}

            RootWindow wnd = new RootWindow();
            wnd.ShowDialog();
        }

        protected override void OnExit(ExitEventArgs e)
        {

            Clearup();

            base.OnExit(e);
        }

    }
}
