﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.ComponentModel;
using System.Windows.Media;

namespace AI
{
    public class InstallController
    {
        private Settings settings = new Settings();

        public Settings Settings
        {
            get { return settings; }
        }

        public void Install()
        {
            if (Validate())
            {
                if (!DropTubs())
                {
                    Settings.SetMessage("Can't drop existing CytovisionDb database", true);
                    return;
                }

                if (!RunDatabaseScript())
                {
                    Settings.SetMessage("Can't create new CytovisionDb database", true);
                    return;
                }

                if (!RunFileStreamScript())
                {
                    Settings.SetMessage("FAILED to configure for Filestream. Check SqlServer running as localsystem or above", true);
                    return;
                }

                Settings.SetMessage("Successful Install", false);
            }
        }

        public bool TestDbServer()
        {
            bool ret = false;
            if (!IsSqlServer2008)
                Settings.SetMessage("No 2008 Sql Server Instance on " + Settings.ServerName, true);
            else if (!IsFileStream)
                Settings.SetMessage("Sql Server not configured for FILESTREAM", true);
            else
            {
                Settings.SetMessage("Found SqlServer 2008 database instance " + Settings.ServerName, false);
                ret = true;
            }
            return ret;
        }

        private bool DropTubs()
        {
            string drop =   "USE master \r\n" +
                            "GO \r\n" +
                            "IF EXISTS(SELECT * FROM SYS.DATABASES WHERE NAME = 'CYTOVISIONDB') \r\n" +
                            "BEGIN " +
                            "ALTER DATABASE CYTOVISIONDB SET SINGLE_USER WITH ROLLBACK IMMEDIATE \r\n" +
                            "DROP DATABASE CYTOVISIONDB \r\n" +
                            "END \r\n" +
                            "GO \r\n";
            return RunDbScript(drop);            
        }

        private bool RunDatabaseScript()
        {
            string scriptText = string.Empty;
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("AI.database.sql"))
            {
                StreamReader reader = new StreamReader(stream);
                scriptText = reader.ReadToEnd();
            }
            return RunDbScript(scriptText);
        }

        private bool RunFileStreamScript()
        {
            string scriptText = string.Empty;
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("AI.TubsToFS2008.sql"))
            {
                StreamReader reader = new StreamReader(stream);
                string rs = reader.ReadToEnd();
                scriptText = string.Format(rs, Settings.BlobPool.TrimEnd('\\'));
            }
            return RunDbScript(scriptText);
        }

        private bool Validate()
        {
            if (!TestDbServer())
                return false;

            if (!Directory.Exists(Settings.BlobPool))
            {
                Settings.SetMessage("Folder ( " + Settings.BlobPool + " ) does not exist", true);
                return false;
            }
            return true;
        }

        private bool RunDbScript(string sqlScriptText)
        {
            bool ret = false;
            SqlConnection connection = new SqlConnection(Settings.ConnectionStringNoCatalog);
            try
            {
                connection.Open();
                Server server = new Server(new ServerConnection(connection));
                server.ConnectionContext.ExecuteNonQuery(sqlScriptText);
                ret = true;
            }
            catch (Exception e)
            {
                Settings.Error = true; Settings.InfoMessage = e.Message;
            }
            finally
            {
                connection.Close();
            }
            return ret;
        }

        private bool IsSqlServer2008
        {
            get
            {
                try
                {
                    Server server = new Server(Settings.ServerName);
                    return server.VersionMajor == 10;

                }
                catch { return false; }
            }
        }

        private bool IsFileStream
        {
            get
            {
                try
                {
                    Server server = new Server(Settings.ServerName);
                    if (server.FilestreamLevel == FileStreamEffectiveLevel.TSqlFullFileSystemAccess || server.FilestreamLevel == FileStreamEffectiveLevel.TSqlLocalFileSystemAccess)
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
            }
        }

    }

    public class Settings : INotifyPropertyChanged
    {
        public Settings()
        {
            Error = false;
            ServerName = ".\\SQLEXPRESS";
        }

        public string ConnectionString
        {
            get { return string.Format("Data Source={0};Integrated Security=True;Connection Timeout=5;Initial Catalog=CytovisionDb", ServerName); }
        }

        public string ConnectionStringNoCatalog
        {
            get { return string.Format("Data Source={0};Integrated Security=True;Connection Timeout=5;", ServerName); }
        }

        private bool error;
        public bool Error
        {
            get { return error; }
            set { error = value; Notify("TextColour"); }
        }

        private string servername;
        public string ServerName
        {
            get { return servername; }
            set { servername = value; Notify("ServerName"); }
        }

        private string blobpool;
        public string BlobPool
        {
            get { return blobpool; }
            set { blobpool = value; Notify("BlobPool"); }
        }

        public void SetMessage(string message, bool error)
        {
            Error = error;
            InfoMessage = message;
        }

        private string infoMessage;
        public string InfoMessage
        {
            get { return infoMessage; }
            set { infoMessage = value; Notify("InfoMessage"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void Notify(string prop)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public SolidColorBrush TextColour
        {
            get 
            {
                Color colour = Error ? Color.FromArgb(255, 74, 19, 0) : Colors.LimeGreen;
                return new SolidColorBrush(colour);
            }
        }
    }

}
