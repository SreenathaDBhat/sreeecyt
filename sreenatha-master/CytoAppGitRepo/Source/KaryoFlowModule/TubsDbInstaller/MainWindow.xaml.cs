﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Threading;
using System.Windows.Threading;

namespace AI
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private InstallController controller = new InstallController();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = controller;
        }

        private void OnInstall(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            IsEnabled = false;
            Thread thread = new Thread((ThreadStart)delegate
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate
                {
                    controller.Install();
                    IsEnabled = true;
                    this.Cursor = Cursors.Arrow;
                });
            });
            thread.Start();
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OnTestConnection(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            IsEnabled = false;

            Thread thread = new Thread((ThreadStart)delegate
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate
                {
                    controller.TestDbServer();
                    IsEnabled = true;
                    this.Cursor = Cursors.Arrow;
                });
            });
            thread.Start();
        }

        private void OnBrowseFolder(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
            dlg.SelectedPath = "c:\\";

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                controller.Settings.BlobPool = dlg.SelectedPath;
            }            
        }
    }
}
