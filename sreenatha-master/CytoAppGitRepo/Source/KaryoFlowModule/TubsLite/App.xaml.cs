﻿using System;
using System.Reflection;
using System.Windows;
using AI;
using System.Linq;
using System.Diagnostics;

namespace TubsLite
{
    public partial class App : System.Windows.Application
    {
        private string appVersion;
        private StringTable strings;

        public App()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

//            AI.Connect.LIMS.SetType(typeof(AI.LimsFallback.LimsBase));

            Assembly exeAssembly = Assembly.GetExecutingAssembly();
            appVersion = "build " + exeAssembly.GetName().Version.ToString();

            IDataSource dataSource = null;
            try
            {
                dataSource = DataSourceFactory.GetDataSource();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Temp warning: Error in FlowDataSourceSettings\n\r" + ex.Message);
                var p = Process.GetCurrentProcess();
                p.Kill();

            }

            if (!dataSource.TestConnection())
            {
                MessageBox.Show("Temp warning: No valid data source. if you are using http then check web server is running. Check that FlowDataSource location is good at both ends");
                var p = Process.GetCurrentProcess();
                p.Kill();
            }

            //AI.Connect.ConnectionTest.ConnectionTester = (parent) =>
            //    {
            //        Dispatcher.VerifyAccess();

            //        try
            //        {
            //            var db = new AI.Connect.Database();
            //            db.Versions.Count();
            //            db.Dispose();
            //        }
            //        catch (Exception)
            //        {
            //            var wnd = new ConnectionTestWindow();
            //            wnd.Owner = parent;
            //            if (wnd.ShowDialog().GetValueOrDefault(false) == false)
            //            {
            //                var p = Process.GetCurrentProcess();
            //                p.Kill();
            //            }
            //        }
            //    };
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            strings = new StringTable(Resources);
            DateStrings.PrimeFromStringTable(strings);
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.ExceptionObject.ToString());
        }

        public string ApplicationVersion
        {
            get { return appVersion; }
        }

        public StringTable Strings
        {
            get { return strings; }

        }
    }
}
