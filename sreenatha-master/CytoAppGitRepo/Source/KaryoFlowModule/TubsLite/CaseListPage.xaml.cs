﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Data;
using System.ComponentModel;
using System.Windows.Controls;
using System.Threading;
using System.Windows.Threading;
using System.Windows.Media;
using AI.Brightfield;
using Microsoft.Win32;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO.Packaging;

namespace AI
{
    public partial class CaseListPage : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public static readonly RoutedUICommand ImportCase = new RoutedUICommand("Import", "ImportExecute", typeof(CaseListPage));
        public static readonly RoutedUICommand CytovisionImport = new RoutedUICommand("CytovisionImport", "CytovisionImportExecute", typeof(CaseListPage));
        private CaseImport45000to46000 cytoimporter;

        private static readonly string archiveFilter = "Archives (*.archive)|*.archive|All Files|*.*";
        private static readonly string archiveVersion = "Cytovision4.5.3.0";


        public CaseListPage()
        {
            DataContext = this;

            InitializeComponent();

#if(DEBUG)
            cytoImportButton.Visibility = Visibility.Visible;
#endif

            ((CaseFinder)FindResource("caseFinder")).PropertyChanged += HijackCaseFinderResults;
            ((CaseFinder)FindResource("caseFinder")).RecentCases();

            Loaded += (s, e) =>
                {
                    RefreshMru();

                    RefreshSelectedCase();

                    searchText.Focus();
                    Keyboard.Focus(searchText);
                };
        }

        private void RefreshMru()
        {
            var recent = MostRecentCases.RecentCases;
            mru.ItemsSource = recent;
        }

        private void HijackCaseFinderResults(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Results")
            {
                var cf = (CaseFinder)sender;
                if (cf.Results.Count() == 1)
                {
                    SelectedCase = cf.Results.ElementAt(0);
                }
            }
        }

		public string ApplicationVersion
		{
			get { return Application.Current == null ? "" : ((TubsLite.App)Application.Current).ApplicationVersion; }
		}

        public CaseLockInfo SelectedCaseLockInfo
        {
            get;
            private set;
        }

        private Case selectedCase;
        public Case SelectedCase
        {
            get { return selectedCase; }
            set 
            { 
                selectedCase = value;
                PropertyChanged(this, new PropertyChangedEventArgs("SelectedCase"));

                SelectedCaseLockInfo = CaseLockInfo.ForCase(selectedCase);
                PropertyChanged(this, new PropertyChangedEventArgs("SelectedCaseLockInfo"));
            }
        }

        private void CanNewCaseExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void NewCaseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            CreateCase("");
        }

        private Case CreateCase(string initialCaseName)
        {
            genericBlackout.Visibility = Visibility.Visible;
            try
            {
                NewCaseWindow wnd = new NewCaseWindow(initialCaseName, Strings)
                {
                    Owner = VisualTreeWalker.FindParentOfType<Window>(this)
                };
                if (!wnd.ShowDialog().Value)
                    return null;

                var newCase = CaseManagement.CreateCase(wnd.Case.Name);
                ((CaseFinder)FindResource("caseFinder")).Refresh();

                SelectedCase = newCase;
                return newCase;
            }
            finally
            {
                genericBlackout.Visibility = Visibility.Collapsed;
            }
        }

        private void CanImportExportExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ImportExecute(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                genericBlackout.Visibility = Visibility.Visible;

                OpenFileDialog fd = new OpenFileDialog();
                fd.Filter = archiveFilter;
                fd.Title = Strings.Lookup("ImportCase_FileDialogTitle");
                fd.RestoreDirectory = true;
                if (fd.ShowDialog().GetValueOrDefault(false) != false)
                {
                    using (var lims = Connect.LIMS.New())
                    {
                        var fileName = fd.FileName;

                        using (var package = Package.Open(fileName))
                        {
                            var version = AI.Packaging.Packager.GetVersion(package);
                            if (version != archiveVersion)
                            {
                                MessageBox.Show("Invalid archive: wrong version number", "Import", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }

                            var caseName = Packaging.CaseInfoPackager.ParseCaseName(package);

                            Case existingCase = lims.FindCase(caseName);
                            if (existingCase == null)
                            {
                                existingCase = CaseManagement.CreateCase(caseName);
                            }
                            else
                            {
                                MessageBox.Show(Strings.Lookup("ImportCase_AlreadyExists"));
                                SelectedCase = existingCase;
                                return;
                            }

                            var progress = new ProgressTracker();
                            var packagers = CreatePackagers(progress);
                            progress.IsNotifyEnabled = true;

                            Action importAction = () =>
                                {
                                    Packaging.Packager.ImportCase(package, lims, existingCase, packagers, Dispatcher, archiveVersion);
                                };

                            ProgressDisplay.Run(Strings.Lookup("ImportCase_WindowText"), progress, importAction, VisualTreeWalker.FindParentOfType<Window>(this));

                            ((CaseFinder)FindResource("caseFinder")).Search = existingCase.Name;

                            CaseLocking.ReleaseCaseLock(existingCase);
                        }
                    }
                }
            }
            finally
            {
                genericBlackout.Visibility = Visibility.Collapsed;
            }
        }

        private void CytovisionImportExecute(object sender, ExecutedRoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DirectoryInfo dir = new DirectoryInfo(dlg.SelectedPath);
                string name = dir.Name;

                bool caseExists = CaseExists(name);

                if (!caseExists && !Connect.LIMS.CanCreateCases)
                {
                    MessageBox.Show("Case not found in LIMS");
                    return;
                }

                // already exists, ask for a new name.
                if (caseExists)
                {
                    var newCase = CreateCase(name);
                    if (newCase == null)
                        return;

                    name = newCase.Name;
                }

                CaseImport(dir, name);
            }
        }

        public CaseImport45000to46000 Importer
        {
            get { return cytoimporter; }
            set { cytoimporter = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Importer")); } }
        }

        private void CaseImport(DirectoryInfo dir, string caseName)
        {
            Importer = new CaseImport45000to46000(dir, caseName, Dispatcher);

            ThreadPool.QueueUserWorkItem(delegate
            {
                try
                {
                    var caseId = Importer.Import();
                    var db = new Connect.Database();
                    var dbCase = db.Cases.Where(c => c.caseId == caseId).First();

                    //Security.Security.AddLogEntry(AI.Logging.EventType.CaseImported, caseId, /*dbCase.status*/ "", Connect.LIMS.CaseName(dbCase.caseId));

                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        Importer.Dispose();
                        Importer = null;

                        searchText.Text = caseName;

                        ((CaseFinder)FindResource("caseFinder")).Refresh();
                    }, DispatcherPriority.Normal);
                }
                catch (Exception ex)
                {
                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        MessageBox.Show(ex.ToString());
                    }, DispatcherPriority.Normal);
                }
            });
        }

        private static bool CaseExists(string caseName)
        {
            using (var lims = Connect.LIMS.New())
            {
                return lims.FindCase(caseName) != null;
            }
        }

        private void OnChangeNetwork(object sender, RoutedEventArgs e)
        {
            genericBlackout.Visibility = Visibility.Visible;
            try
            {
                string revert = Connect.Locations.ConnectionString;

                while (true)
                {
                    NetworkConnect connectScreen = new NetworkConnect
                    {
                        Owner = VisualTreeWalker.FindParentOfType<Window>(this)
                    };
                    bool result = connectScreen.ShowDialog().GetValueOrDefault(false);

                    if (!result)
                    {
                        Connect.Locations.ConnectionString = revert;
                        Connect.Locations.Save();
                        break;
                    }

                    Connect.Locations.Save();

                    if (AI.Connect.NetworkTest.TestSql())
                    {
                        ((CaseFinder)FindResource("caseFinder")).Refresh();
                        break;
                    }
                }
            }
            finally
            {
                ((CaseFinder)FindResource("caseFinder")).Search = "";
                RefreshMru();
                genericBlackout.Visibility = Visibility.Collapsed;
            }
        }

        private void ShowMyContextMenu(object sender, RoutedEventArgs e)
        {
            ((Button)sender).Focus();
            ((Button)sender).ContextMenu.IsOpen = true;
        }

        private void OnDeleteCase(object sender, RoutedEventArgs e)
        {
            if (SelectedCase == null)
                return;

            RefreshCaseLock();
            if (SelectedCaseLockInfo != null)
                return;

            if (CaseNoLongerExists())
            {
                ClearSelectedCase();
                return;
            }

            genericBlackout.Visibility = Visibility.Visible;
            try
            {
                MostRecentCases.Remove(SelectedCase.Name);

                DeleteStuff deleteWindow = new DeleteStuff((ThreadStart)delegate
                {
                    CaseManagement.DeleteCase(SelectedCase);
                });

                deleteWindow.Owner = VisualTreeWalker.FindParentOfType<Window>(this);

                if (deleteWindow.ShowDialog().GetValueOrDefault(false))
                {
                    ((CaseFinder)FindResource("caseFinder")).Refresh();
                    SelectedCase = null;
                }

                RefreshMru();
                OnShowRecent(null, null);
            }
            finally
            {
                genericBlackout.Visibility = Visibility.Collapsed;
            }
        }

        private void ClearSelectedCase()
        {
            ((CaseFinder)FindResource("caseFinder")).Refresh();
            SelectedCase = null;
        }

        private bool CaseNoLongerExists()
        {
            //using (var db = new Connect.Database())
            //{
            //    return db.Cases.Where(c => c.limsRef == SelectedCase.Id).FirstOrDefault() == null;
            //}
            return false;
        }

        private Guid GetRealCaseId(string limsId)
        {
            Guid id = Guid.Empty;
            using (var db = new Connect.Database())
            {
                var caseInfo = db.Cases.Where(c => c.limsRef == limsId).First();
                id = caseInfo.caseId;
            }
            return id;
        }

        private void OnExportCase(object sender, RoutedEventArgs e)
        {
            if (SelectedCase == null)
                return;

            genericBlackout.Visibility = Visibility.Visible;

            SaveFileDialog fd = new SaveFileDialog();
            fd.Filter = archiveFilter;
            fd.RestoreDirectory = true;
            fd.FileName = SelectedCase.Name;
            fd.Title = Strings.Lookup("ExportCase_FileDialogTitle");
            fd.OverwritePrompt = true;
            if(fd.ShowDialog().GetValueOrDefault(false) == false)
            {
                genericBlackout.Visibility = Visibility.Collapsed;
                return;
            }

            var c = SelectedCase;
            RefreshCaseLock();

            if (CaseNoLongerExists())
            {
                ClearSelectedCase();
                genericBlackout.Visibility = Visibility.Collapsed;
                return;
            }

            CaseLocking.AcquireCaseLock(c);
            try
            {
                if (File.Exists(fd.FileName))
                    File.Delete(fd.FileName);

                var progress = new ProgressTracker();
                var packageParts = CreatePackagers(progress);
                progress.IsNotifyEnabled = true;

                Action exportAction = () =>
                {
                    using (var lims = Connect.LIMS.New())
                    {
                        Packaging.Packager.ExportCase(lims, c, fd.FileName, packageParts, Dispatcher, archiveVersion);
                    }
                };

                ProgressDisplay.Run(Strings.Lookup("ExportCase_WindowText"), progress, exportAction, VisualTreeWalker.FindParentOfType<Window>(this));
            }
            finally
            {
                genericBlackout.Visibility = Visibility.Collapsed;
                CaseLocking.ReleaseCaseLock(c);
            }
        }

        private IEnumerable<Packaging.IPackager> CreatePackagers(ProgressTracker progress)
        {
            return new Packaging.IPackager[]
                    {
                        new Packaging.CaseInfoPackager(),
                        new Packaging.BlobPackager(progress.CreateNode(15)),
                        new Packaging.SlidesAndCellsPackager(progress.CreateNode(5)),
                        new Packaging.ImageFramePackager(progress.CreateNode(1)),
                        new Packaging.ScratchPadPackager(progress.CreateNode(2))
                    };
        }

        private void OnOpenCase(object sender, RoutedEventArgs e)
        {
            var c = (Case)((FrameworkElement)sender).Tag;
            RefreshCaseLock();
            if(SelectedCaseLockInfo != null)
                return;

            if (CaseNoLongerExists())
            {
                ClearSelectedCase();
                return;
            }

            MostRecentCases.Add(c.Name);
            OpenCase(c);
        }

        private StringTable Strings
        {
            get { return ((TubsLite.App)TubsLite.App.Current).Strings; }
        }

        private void OpenCase(Case c)
        {
            //using (var db = new Connect.Database())
            //using (ILims lims = Connect.LIMS.New())
            //{
            //    db.CreateCaseAndSlides(lims, c);
            //    CaseLocking.AcquireCaseLock(c);
            //}

            Brightfield.WorkflowController controller = new AI.Brightfield.WorkflowController(Dispatcher, c, VisualTreeWalker.FindParentOfType<Window>(this));
            controller.StringTable = Strings;

            UIStack.For(this).Push(new Brightfield.AnalysisPage(controller));
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (selectedCase != null && e.Key == Key.Enter && CaseLockInfo.ForCase(selectedCase) == null)
            {
                OpenCase(selectedCase);
            }
        }

        private void OnDeleteSlide(object sender, RoutedEventArgs e)
        {
            RefreshCaseLock();
            if (SelectedCaseLockInfo != null)
                return;

            if (CaseNoLongerExists())
            {
                ClearSelectedCase();
                return;
            }

            genericBlackout.Visibility = Visibility.Visible;
            try
            {
                AI.SlideInfoGenerator.SlideInfo slide = (AI.SlideInfoGenerator.SlideInfo)((FrameworkElement)sender).Tag;

                if (SlideNoLongerExists(slide))
                {
                    RefreshSelectedCase();
                    return;
                }

                DeleteStuff deleteWindow = new DeleteStuff((ThreadStart)delegate
                {
                    using (var db = new Connect.Database())
                    using (ILims lims = Connect.LIMS.New())
                    {
                        var limsSlide = lims.GetSlide(slide.LimsRef);
                        var dbSlide = db.Slides.Where(s => s.limsRef == slide.LimsRef).FirstOrDefault();
                        if (dbSlide != null)
                        {
                            // Default LIMS. Kill the slide for reals.
                            if (Connect.LIMS.CanCreateCases)
                            {
                                var limsId = new Guid(limsSlide.Id);
                                var defLimsSlide = db.LMF_Slides.Where(ls => ls.id == limsId).First();
                                db.LMF_Slides.DeleteOnSubmit(defLimsSlide);
                            }

                            db.Slides.DeleteOnSubmit(dbSlide);
                            db.SubmitChanges();
                        }
                    }
                });

                deleteWindow.Owner = VisualTreeWalker.FindParentOfType<Window>(this);

                if (deleteWindow.ShowDialog().GetValueOrDefault(false))
                {
                    RefreshSelectedCase();
                }
            }
            finally
            {
                genericBlackout.Visibility = Visibility.Collapsed;
            }
        }

        private void RefreshSelectedCase()
        {
            var sc = selectedCase;
            SelectedCase = null;
            SelectedCase = sc;
        }

        private bool SlideNoLongerExists(SlideInfoGenerator.SlideInfo slide)
        {
            using (var db = new Connect.Database())
            {
                return db.Slides.Where(s => s.limsRef == slide.LimsRef).FirstOrDefault() == null;
            }
        }

        private void OnSelectFromMru(object sender, RoutedEventArgs e)
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(this));

            var mruEntry = (CaseMruEntry)((FrameworkElement)sender).Tag;
            using (var lims = Connect.LIMS.New())
            {
                var c = lims.FindCase(mruEntry.CaseName);
                if (c == null)
                {
                    MostRecentCases.Remove(mruEntry.CaseName);
                    RefreshMru();
                }
                else
                {
                    SelectedCase = c;
                }
            }

            mruPopup.IsOpen = false;
        }

        private void OnShowMru(object sender, RoutedEventArgs e)
        {
            mruPopup.IsOpen = true;
        }

        private void OnHardwareChange(object sender, RoutedEventArgs e)
        {
        }

        private void OnClearCaseLock(object sender, RoutedEventArgs e)
        {
            if (SelectedCase == null)
                return;

            genericBlackout.Visibility = Visibility.Visible;
            try
            {
                ConfirmCaseLockRemoval w = new ConfirmCaseLockRemoval();
                w.Owner = VisualTreeWalker.FindParentOfType<Window>(this);
                if (w.ShowDialog().GetValueOrDefault(false))
                {
                    CaseLocking.ReleaseCaseLock(SelectedCase);
                    RefreshCaseLock();
                }
            }
            finally
            {
                genericBlackout.Visibility = Visibility.Collapsed;
            }
        }

        private void RefreshCaseLock()
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(this));
            SelectedCaseLockInfo = CaseLockInfo.ForCase(SelectedCase);
            PropertyChanged(this, new PropertyChangedEventArgs("SelectedCaseLockInfo"));
        }

        private void OnShowRecent(object sender, RoutedEventArgs e)
        {
            Connect.ConnectionTest.Test(VisualTreeWalker.FindParentOfType<Window>(this));
            ((CaseFinder)FindResource("caseFinder")).RecentCases();
            mruPopup.IsOpen = false;
        }
    }
}