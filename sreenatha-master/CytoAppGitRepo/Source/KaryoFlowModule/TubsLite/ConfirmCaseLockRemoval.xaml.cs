﻿using System;
using System.Windows.Input;
using System.Windows;

namespace AI
{
    public partial class ConfirmCaseLockRemoval
    {
        public ConfirmCaseLockRemoval()
        {
            InitializeComponent();
        }

        private void OnDragWindow(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void OnConfirm(object sender, RoutedEventArgs e)
        {
            Connect.ConnectionTest.Test(this);
            DialogResult = true;
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
