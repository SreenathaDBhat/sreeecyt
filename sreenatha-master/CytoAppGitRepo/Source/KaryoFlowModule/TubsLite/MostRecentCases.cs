﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace AI
{
    public class CaseMruEntry
    {
        public CaseMruEntry(string name, DateTime timeOpened)
        {
            CaseName = name;
            TimeLastOpened = timeOpened;
        }

        public string CaseName { get; private set; }
        public DateTime TimeLastOpened { get; private set; }
    }

    public static class MostRecentCases
    {
        public static IEnumerable<CaseMruEntry> RecentCases
        {
            get
            {
                var file = AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\case.mru");
                if (!file.Exists)
                    return new CaseMruEntry[0];

                try
                {
                    var xml = XElement.Load(file.FullName);

                    // mru is invalidated if we switched databases!
                    var connection = xml.Attribute("Connection").Value;
                    if (string.Compare(connection, Connect.Locations.ConnectionString, StringComparison.CurrentCultureIgnoreCase) != 0)
                        return new CaseMruEntry[0];

                    return from c in xml.Descendants("Case")
                           select new CaseMruEntry(c.Attribute("Name").Value, DateTime.Parse(c.Attribute("When").Value));
                }
                catch (Exception)
                {
                    file.Delete();
                    throw;
                }
            }
        }

        public static void Add(string caseName)
        {
            var all = (from c in RecentCases
                       where string.Compare(c.CaseName, caseName, StringComparison.CurrentCultureIgnoreCase) != 0
                       select c).ToList();

            all.Insert(0, new CaseMruEntry(caseName, DateTime.Now));
            Save(all);

            
        }

        private static void Save(IEnumerable<CaseMruEntry> all)
        {
            var file = AppData.File(Environment.SpecialFolder.LocalApplicationData, "Genetix\\case.mru");
            if (!file.Exists)
                file.Delete();

            List<XObject> content = new List<XObject>();
            content.Add(new XAttribute("Connection", Connect.Locations.ConnectionString));
            content.AddRange((from n in all
                              select new XElement("Case", new XAttribute("Name", n.CaseName), new XAttribute("When", n.TimeLastOpened))).ToArray());

            XElement xml = new XElement("CaseMru", content);
            xml.Save(file.FullName);
        }

        public static void Remove(string caseName)
        {
            var all = (from c in RecentCases
                       where string.Compare(c.CaseName, caseName, StringComparison.CurrentCultureIgnoreCase) != 0
                       select c).ToList();

            Save(all);
        }
    }
}
