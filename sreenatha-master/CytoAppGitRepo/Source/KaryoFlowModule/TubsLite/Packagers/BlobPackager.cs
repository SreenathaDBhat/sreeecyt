﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mime;
using System.IO.Packaging;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.IO;

namespace AI.Packaging
{
    public class BlobPackager : IPackager
    {
        private static readonly Uri partUri = new Uri("/blobinfo.xml", UriKind.Relative);
        private ProgressNode progress;

        public BlobPackager(ProgressNode progress)
        {
            this.progress = progress;
        }

        public void Export(System.IO.Packaging.Package package, ILims lims, Case c, AI.Connect.Database sourceDb)
        {
            var caseId = sourceDb.CaseIdFor(c);

            var blobs = from b in sourceDb.Blobs
                        where b.caseId == caseId
                        select new
                        {
                            Id = b.blobId,
                            Path = b.path
                        };

            progress.Initialize(blobs.Count() + 1, 0);

            foreach (var blob in blobs)
            {
                var uri = new Uri("/blob/" + blob.Id.ToString(), UriKind.Relative);
                var part = package.CreatePart(uri, MediaTypeNames.Application.Zip);
                var stream = part.GetStream();
                
                var wholeBlob = sourceDb.Blobs.Where(b => b.blobId == blob.Id).First();
                var binary = wholeBlob.binaryData.ToArray();

                int len = wholeBlob.binaryData.Length;
                stream.Write(binary, 0, len);

                package.Flush();
                progress.CurrentStep++;
            }


            var info = new XElement("Blobs", from b in blobs
                                             select new XElement("Blob", new XAttribute("Path", b.Path), new XAttribute("Id", b.Id)));
            
            Packager.AddPartFromXml(package, partUri, info);
            progress.CurrentStep++;
        }



        public void Import(Package package, ILims lims, Case c, Connect.Database sourceDb)
        {

            var xml = Packager.ReadXmlPart(package, partUri);
            var blobs = from b in xml.Descendants("Blob")
                        select new
                        {
                            Path = b.Attribute("Path").Value,
                            Id = new Guid(b.Attribute("Id").Value)
                        };

            var caseId = sourceDb.CaseIdFor(c);
            progress.Initialize(blobs.Count(), 0);

            foreach(var blob in blobs)
            {
                var blobPart = package.GetPart(new Uri("/blob/" + blob.Id.ToString(), UriKind.Relative));
                var stream = blobPart.GetStream();

                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, (int)stream.Length);

                using (var db = new Connect.Database())
                {
                    Connect.Blob dbblob = new AI.Connect.Blob()
                    {
                        binaryData = new System.Data.Linq.Binary(buffer),
                        blobId = blob.Id,
                        caseId = caseId,
                        path = blob.Path
                    };

                    db.Blobs.InsertOnSubmit(dbblob);
                    db.SubmitChanges();
                    progress.CurrentStep++;
                }
            }
        }
    }
}
