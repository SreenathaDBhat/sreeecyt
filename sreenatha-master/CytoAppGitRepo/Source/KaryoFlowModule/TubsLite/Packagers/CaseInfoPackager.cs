﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Packaging;
using System.Net.Mime;
using System.Xml.Linq;
using System.IO;
using System.Xml;

namespace AI.Packaging
{
    public class CaseInfoPackager : IPackager
    {
        private static readonly Uri partUri = new Uri("/caseinfo.xml", UriKind.Relative);

        public void Export(Package package, ILims lims, Case c, Connect.Database sourceDb)
        {
            XElement caseXml = new XElement("Case",
                new XAttribute("Name", c.Name),
                new XAttribute("Created", c.Created),
                new XElement("MetaData", PackageCaseMetaData(lims, c)));

            Packager.AddPartFromXml(package, partUri, caseXml);
        }

        public void Import(Package package, ILims lims, Case c, Connect.Database sourceDb)
        {
            // The actual case object is already created

            if (Connect.LIMS.CanCreateCases)
            {
                var xml = Packager.ReadXmlPart(package, partUri);
                var encryptedNode = xml.Element("MetaData");
                var encrypted = encryptedNode.Value;

                if (encrypted != null && encrypted.Length > 0)
                {
                    var metaDataXml = Encryptor.XElementFromBase64(encrypted);

                    var metaData = from f in metaDataXml.Descendants("Field")
                                   select new MetaDataItem
                                   {
                                       Field = f.Attribute("Name").Value,
                                       Value = f.Attribute("Value").Value
                                   };

                    lims.SetCaseMetaData(c, metaData);
                }
            }
        }

        public static string ParseCaseName(Package package)
        {
            var part = package.GetPart(partUri);
            var stream = part.GetStream();
            var reader = new XmlTextReader(stream);
            XElement xml = XElement.Load(reader);
            return xml.Attribute("Name").Value;
        }



        private XCData PackageCaseMetaData(ILims lims, Case c)
        {
            var metaData = lims.GetCaseMetaData(c);
            if (metaData.Count() == 0)
                return null;

            XElement meta = new XElement("MetaData", from m in metaData
                                                     select new XElement("Field", new XAttribute("Name", m.Field), new XAttribute("Value", m.Value)));

            return Encryptor.ToBase64(meta);
        }
    }
}
