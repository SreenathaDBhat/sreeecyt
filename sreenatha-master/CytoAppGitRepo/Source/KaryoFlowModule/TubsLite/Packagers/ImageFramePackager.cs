﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO.Packaging;

namespace AI.Packaging
{
    public class ImageFramePackager : IPackager
    {
        private static readonly Uri partUri = new Uri("/imageframes.xml", UriKind.Relative);

        private ProgressNode progress;

        public ImageFramePackager(ProgressNode progress)
        {
            this.progress = progress;
        }

        public void Export(Package package, ILims lims, Case c, AI.Connect.Database sourceDb)
        {
            var caseId = sourceDb.CaseIdFor(c);
            var slides = sourceDb.Slides.Where(s => s.caseId == caseId).ToArray();

            XElement xml = new XElement("ImageFrames");
            progress.Initialize(slides.Count(), 0);

            foreach (var s in slides)
            {
                var frames = sourceDb.ImageFrames.Where(f => f.slideId == s.slideId);
                var frameNodes = from f in frames
                                 select new XElement("ImageFrame",
                                     new XAttribute("id", f.imageFrameId),
                                     new XAttribute("slideId", f.slideId),
                                     new XAttribute("blobid", f.blobid == null ? Guid.Empty : f.blobid),
                                     new XAttribute("bpp", f.bpp),
                                     new XAttribute("gamma", f.gamma),
                                     new XAttribute("groupId", f.groupId),
                                     new XAttribute("height", f.height),
                                     new XAttribute("imageType", f.imageType),
                                     new XAttribute("location", f.location),
                                     new XAttribute("width", f.width),
                                     new XAttribute("x", f.x),
                                     new XAttribute("y", f.y),

                                     
                                     from m in sourceDb.ImageComponents
                                     where m.frameId == f.imageFrameId
                                     select new XElement("Component",
                                         new XAttribute("name", m.displayName),
                                         
                                         from n in sourceDb.ImageSnaps
                                         where n.componentId == m.imageComponentId
                                         select new XElement("Snap",
                                             new XAttribute("id", n.imageSnapId),
                                             new XAttribute("blob", n.blobId),
                                             new XAttribute("isProjection", n.isProjection),
                                             new XAttribute("z", n.zLevel))));
                
                XElement slideNode = new XElement("Slide", new XAttribute("slide", s.slideId));
                slideNode.Add(frameNodes);
                xml.Add(slideNode);

                progress.CurrentStep++;
            }

            Packager.AddPartFromXml(package, partUri, xml);
        }

        public void Import(Package package, ILims lims, Case c, Connect.Database db)
        {
            var xml = Packager.ReadXmlPart(package, partUri);
            var slides = from s in xml.Descendants("Slide")
                         select s;

            progress.Initialize(slides.Count(), 0);

            foreach (var slideNode in slides)
            {
                var id = new Guid(slideNode.Attribute("slide").Value);
                var dbSlide = db.Slides.Where(dbs => dbs.slideId == id).First();

                foreach (var frameNode in slideNode.Descendants("ImageFrame"))
                {
                    var dbFrame = new Connect.ImageFrame
                    {
                        imageFrameId = new Guid(frameNode.Attribute("id").Value),
                        Slide = dbSlide,
                        width = int.Parse(frameNode.Attribute("width").Value),
                        height = int.Parse(frameNode.Attribute("height").Value),
                        bpp = int.Parse(frameNode.Attribute("bpp").Value),
                        gamma = double.Parse(frameNode.Attribute("gamma").Value),
                        groupId = new Guid(frameNode.Attribute("groupId").Value),
                        blobid = new Guid(frameNode.Attribute("blobid").Value),
                        imageType = int.Parse(frameNode.Attribute("imageType").Value),
                        location = frameNode.Attribute("location").Value
                    };

                    db.ImageFrames.InsertOnSubmit(dbFrame);
                    db.SubmitChanges();

                    foreach (var componentNode in frameNode.Descendants("Component"))
                    {
                        var dbComponent = new Connect.ImageComponent
                        {
                            displayName = componentNode.Attribute("name").Value,
                            frameId = dbFrame.imageFrameId,
                            imageComponentId = Guid.NewGuid(),
                            isAnalysisOverlay = false,
                            isCounterstain = false,
                            renderColor = "#ffffff"
                        };

                        db.ImageComponents.InsertOnSubmit(dbComponent);
                        db.SubmitChanges();

                        foreach (var snapNode in componentNode.Descendants("Snap"))
                        {
                            var dbSnap = new Connect.ImageSnap
                            {
                                blobId = new Guid(snapNode.Attribute("blob").Value),
                                componentId = dbComponent.imageComponentId,
                                imageSnapId = new Guid(snapNode.Attribute("id").Value),
                                isProjection = bool.Parse(snapNode.Attribute("isProjection").Value),
                                zLevel = int.Parse(snapNode.Attribute("z").Value)
                            };

                            db.ImageSnaps.InsertOnSubmit(dbSnap);
                            db.SubmitChanges();
                        }
                    }
                }

                progress.CurrentStep++;
            }
        }
    }
}
