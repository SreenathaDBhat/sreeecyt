﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO.Packaging;
using System.Xml.Linq;

namespace AI.Packaging
{
    public class ScratchPadPackager : IPackager
    {
        private static readonly Uri partUri = new Uri("/scratchpads.xml", UriKind.Relative);
        private ProgressNode progress;

        public ScratchPadPackager(ProgressNode progress)
        {
            this.progress = progress;
        }

        public void Export(Package package, ILims lims, Case c, AI.Connect.Database sourceDb)
        {
            var caseId = sourceDb.CaseIdFor(c);
            var scratchPads = sourceDb.ScratchPads.Where(p => p.caseId == caseId);

            if(scratchPads.Count() == 0)
                return;

            progress.Initialize(2, 0);

            var xml = new XElement("ScratchPads", from s in scratchPads
                                                  select new XElement("ScratchPad",
                                                      new XAttribute("id", s.scratchPadId),
                                                      new XAttribute("name", s.name),
                                                      new XAttribute("description", s.description),

                                                      from i in s.ScratchPadItems
                                                      select new XElement("ScratchPadItem",
                                                          new XAttribute("id", i.scratchPadItemId),
                                                          new XAttribute("angleToVertical", i.angleToVertical),
                                                          new XAttribute("blobId", i.blobId == null ? Guid.Empty : i.blobId),
                                                          new XAttribute("flipX", i.flipX),
                                                          new XAttribute("flipY", i.flipY),
                                                          new XAttribute("userRotation", i.userRotation),
                                                          new XAttribute("x", i.x),
                                                          new XAttribute("y", i.y),
                                                          i.itemDescription)));

            progress.CurrentStep++;
            
            Packager.AddPartFromXml(package, partUri, xml);

            progress.CurrentStep++;
        }

        public void Import(Package package, ILims lims, Case c, Connect.Database sourceDb)
        {
            var xml = Packager.ReadXmlPart(package, partUri);
            if (xml == null)
                return;

            var caseId = sourceDb.CaseIdFor(c);
            var scratchPadNodes = xml.Descendants("ScratchPad");
            progress.Initialize(scratchPadNodes.Count(), 0);

            foreach (var padNode in scratchPadNodes)
            {
                var pad = new Connect.ScratchPad
                {
                    caseId = caseId,
                    description = padNode.Attribute("description").Value,
                    name = padNode.Attribute("name").Value,
                    scratchPadId = new Guid(padNode.Attribute("id").Value)
                };

                sourceDb.ScratchPads.InsertOnSubmit(pad);
                sourceDb.SubmitChanges();

                var items = from i in padNode.Descendants("ScratchPadItem")
                            select new Connect.ScratchPadItem
                            {
                                angleToVertical = double.Parse(i.Attribute("angleToVertical").Value),
                                blobId = new Guid(i.Attribute("blobId").Value),
                                flipX = bool.Parse(i.Attribute("flipX").Value),
                                flipY = bool.Parse(i.Attribute("flipY").Value),
                                itemDescription = DescriptionFor(i),
                                scratchPadId = pad.scratchPadId,
                                scratchPadItemId = new Guid(i.Attribute("id").Value),
                                userRotation = double.Parse(i.Attribute("userRotation").Value),
                                x = double.Parse(i.Attribute("x").Value),
                                y = double.Parse(i.Attribute("y").Value)
                            };

                sourceDb.ScratchPadItems.InsertAllOnSubmit(items);
                sourceDb.SubmitChanges();
                progress.CurrentStep++;
            }            
        }

        private XElement DescriptionFor(XElement i)
        {
            var d = i.Descendants();
            if (d == null)
                return null;

            return d.FirstOrDefault();
        }
    }
}
