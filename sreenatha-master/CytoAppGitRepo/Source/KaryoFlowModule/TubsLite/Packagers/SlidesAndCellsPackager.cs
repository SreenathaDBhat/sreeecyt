﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.Net.Mime;
using System.IO.Packaging;

namespace AI.Packaging
{
    public class SlidesAndCellsPackager : IPackager
    {
        private static readonly Uri partUri = new Uri("/cells.xml", UriKind.Relative);
        private ProgressNode progress;

        public SlidesAndCellsPackager(ProgressNode progress)
        {
            this.progress = progress;
        }
        
        public void Export(System.IO.Packaging.Package package, ILims lims, Case c, AI.Connect.Database sourceDb)
        {
            var dbCase = sourceDb.Cases.Where(lc => lc.limsRef == c.Id).First();

            var slides = dbCase.Slides;
            progress.Initialize(2, 0);
            
            var xml = new XElement("Case",
                            from s in slides
                            select new XElement("Slide", 
                                new XAttribute("name", lims.GetSlide(s.limsRef).Name),
                                new XAttribute("barcode", ValueOrEmpty(lims.GetSlide(s.limsRef).Barcode)),
                                new XAttribute("limsRef", s.limsRef),
                                new XAttribute("id", s.slideId),
                                CellData(s)));

            progress.CurrentStep++;

            var part = package.CreatePart(partUri, MediaTypeNames.Text.Xml);
            var stream = part.GetStream();
            using (var writer = new XmlTextWriter(stream, Encoding.Default))
            {
                xml.Save(writer);
                writer.Flush();
            }

            progress.CurrentStep++;
        }

        private XElement CellData(Connect.Slide slide)
        {
            XElement cellNode = new XElement("Cells", from c in slide.Cells select CellNode(c));
            return cellNode;
        }

        private XElement CellNode(AI.Connect.Cell c)
        {
            var node = new XElement("Cell",
                    new XAttribute("cellName", c.cellName),
                    new XAttribute("analysed", c.analysed),
                    new XAttribute("cellId", c.cellId),
                    new XAttribute("color", c.color),
                    new XAttribute("counted", c.counted),
                    new XAttribute("imageGroup", c.imageGroup),
                    new XAttribute("karyotyped", c.karyotyped),
                    new XAttribute("karyotypedBlobId", c.karyotypedBlobId == null ? Guid.Empty : c.karyotypedBlobId),
                    new XAttribute("threshold", c.threshold));

            if (c.counted)
            {
                node.Add(AddManualCountPoints(c));
            }
            if (c.analysed)
            {
                node.Add(AddNumberingTags(c));
            }
            if (c.karyotyped)
            {
                node.Add(AddChromosomes(c));
            }
            if (c.CellMetaDatas.Count > 0)
            {
                node.Add(AddMetaData(c));
            }

            return node;
        }

        private XElement AddManualCountPoints(Connect.Cell c)
        {
            return new XElement("ManualCountPoints", from p in c.ManualCountPoints
                                                     select new XElement("CountPoint", new XAttribute("x", p.x), new XAttribute("y", p.y)));
        }

        private XElement AddNumberingTags(Connect.Cell c)
        {
            return new XElement("AnalysisTags", from p in c.NumberingTags
                                                select new XElement("AnalysisTag", 
                                                           new XAttribute("x", p.x), 
                                                           new XAttribute("y", p.y), 
                                                           new XAttribute("display", p.display),
                                                           new XAttribute("tag", p.tag)));
        }

        private XElement AddChromosomes(Connect.Cell c)
        {
            return new XElement("Chromosomes", from p in c.KaryotypeChromosomes
                                               select new XElement("Chromosome",
                                                          new XAttribute("angleToVertical", p.angleToVertical),
                                                          new XAttribute("blobId", p.blobId),
                                                          new XAttribute("boundsHeight", p.boundsHeight),
                                                          new XAttribute("boundsLeft", p.boundsLeft),
                                                          new XAttribute("boundsTop", p.boundsTop),
                                                          new XAttribute("boundsWidth", p.boundsWidth),
                                                          new XAttribute("chromosomeTag", p.chromosomeTag),
                                                          new XAttribute("flipX", p.flipX),
                                                          new XAttribute("flipY", p.flipY),
                                                          new XAttribute("groupIndex", p.groupIndex),
                                                          new XAttribute("karyotypeChromosomeId", p.karyotypeChromosomeId),
                                                          new XAttribute("length", p.length),
                                                          new XAttribute("midPointX", p.midPointX),
                                                          new XAttribute("midPointY", p.midPointY),
                                                          new XAttribute("userRotation", p.userRotation),
                                                          new XAttribute("width", p.width),
                                                          new XAttribute("x", p.x),
                                                          new XAttribute("y", p.y),
                                                          p.outline));
        }

        private XCData AddMetaData(Connect.Cell c)
        {
            XElement meta = new XElement("MetaData",
                from m in c.CellMetaDatas
                select new XElement("Field", new XAttribute("Name", m.field), new XAttribute("Value", m.value), new XAttribute("Confidential", m.confidential)));

            return Encryptor.ToBase64(meta);
        }

        private string ValueOrEmpty(string i)
        {
            return i == null ? string.Empty : i;
        }

        
        
        
        public void Import(Package package, ILims lims, Case c, Connect.Database db)
        {
            var xml = Packager.ReadXmlPart(package, partUri);
            var slides = xml.Descendants("Slide");
            var caseId = db.CaseIdFor(c);

            progress.Initialize(slides.Count(), 0);

            foreach (var slide in slides)
            {
                var limsRef = slide.Attribute("limsRef").Value;
                var limsSlide = lims.GetSlide(limsRef);
                
                if (limsSlide == null)
                {
                    if (!Connect.LIMS.CanCreateCases)
                    {
                        throw new NotSupportedException("Slide not found: " + limsRef);
                    }
                    else
                    {
                        var lmfCase = db.LMF_Cases.Where(lc => lc.id == new Guid(c.Id)).First();

                        var lmfSlide = new Connect.LMF_Slide()
                        {
                            barcode = slide.Attribute("barcode").Value,
                            id = Guid.NewGuid(),
                            name = slide.Attribute("name").Value,
                            limsCaseId = lmfCase.id
                        };

                        db.LMF_Slides.InsertOnSubmit(lmfSlide);
                        db.SubmitChanges();

                        limsSlide = lims.GetSlide(lmfSlide.id.ToString());
                    }
                }

                var dbSlide = new Connect.Slide
                {
                    caseId = caseId,
                    limsRef = limsSlide.Id,
                    slideId = new Guid(slide.Attribute("id").Value)
                };

                db.Slides.InsertOnSubmit(dbSlide);
                db.SubmitChanges();

                ImportCellsForSlide(slide, db, dbSlide);
                progress.CurrentStep++;
            }
        }

        private void ImportCellsForSlide(XElement slide, Connect.Database db, Connect.Slide dbSlide)
        {
            var cellNodes = slide.Descendants("Cell");
            var newCells = from cellNode in cellNodes
                           select new
                           {
                               DbCell = new Connect.Cell
                               {
                                   analysed = bool.Parse(cellNode.Attribute("analysed").Value),
                                   counted = bool.Parse(cellNode.Attribute("counted").Value),
                                   karyotyped = bool.Parse(cellNode.Attribute("karyotyped").Value),
                                   cellId = new Guid(cellNode.Attribute("cellId").Value),
                                   cellName = cellNode.Attribute("cellName").Value,
                                   color = int.Parse(cellNode.Attribute("color").Value),
                                   imageGroup = new Guid(cellNode.Attribute("imageGroup").Value),
                                   karyotypedBlobId = new Guid(cellNode.Attribute("karyotypedBlobId").Value),
                                   slideId = dbSlide.slideId,
                                   threshold = int.Parse(cellNode.Attribute("threshold").Value)
                               },

                               Node = cellNode
                           };

            db.Cells.InsertAllOnSubmit(from c in newCells select c.DbCell);
            db.SubmitChanges();

            foreach (var c in newCells)
            {
                ReadMetaData(db, c.DbCell, c.Node);
                ReadManualCountPoints(db, c.DbCell, c.Node);
                ReadNumberingTags(db, c.DbCell, c.Node);
                ReadChromosomes(db, c.DbCell, c.Node);
            }
        }

        private void ReadMetaData(AI.Connect.Database db, AI.Connect.Cell cell, XElement cellNode)
        {
            var content = cellNode.Value;
            if (content == null || content.Trim().Length == 0)
                return;

            var xml = Encryptor.XElementFromBase64(content);

            var fieldValuePairs = xml.Descendants("Field");
            var rows = from f in fieldValuePairs
                       select new Connect.CellMetaData
                       {
                           cellId = cell.cellId,
                           cellMetaDataId = Guid.NewGuid(),
                           confidential = false,
                           field = f.Attribute("Name").Value,
                           value = f.Attribute("Value").Value
                       };

            db.CellMetaDatas.InsertAllOnSubmit(rows);
            db.SubmitChanges();
        }

        private void ReadManualCountPoints(Connect.Database db, AI.Connect.Cell cell, XElement cellNode)
        {
            var points = cellNode.Descendants("CountPoint");
            var dbPoints = from p in points
                           select new Connect.ManualCountPoint
                           {
                               cellId = cell.cellId,
                               manualCountPointId = Guid.NewGuid(),
                               x = double.Parse(p.Attribute("x").Value),
                               y = double.Parse(p.Attribute("y").Value)
                           };

            db.ManualCountPoints.InsertAllOnSubmit(dbPoints);
            db.SubmitChanges();
        }

        private void ReadChromosomes(Connect.Database db, AI.Connect.Cell cell, XElement cellNode)
        {
            var chroms = cellNode.Descendants("Chromosome");
            var dbChroms = from p in chroms
                           select new Connect.KaryotypeChromosome
                           {
                               angleToVertical = double.Parse(p.Attribute("angleToVertical").Value),
                               blobId = new Guid(p.Attribute("blobId").Value),
                               boundsHeight = int.Parse(p.Attribute("boundsHeight").Value),
                               boundsLeft = int.Parse(p.Attribute("boundsLeft").Value),
                               boundsTop = int.Parse(p.Attribute("boundsTop").Value),
                               boundsWidth = int.Parse(p.Attribute("boundsWidth").Value),
                               cellId = cell.cellId,
                               chromosomeTag = p.Attribute("chromosomeTag").Value,
                               flipX = bool.Parse(p.Attribute("flipX").Value),
                               flipY = bool.Parse(p.Attribute("flipY").Value),
                               groupIndex = int.Parse(p.Attribute("groupIndex").Value),
                               karyotypeChromosomeId = new Guid(p.Attribute("karyotypeChromosomeId").Value),
                               length = int.Parse(p.Attribute("length").Value),
                               midPointX = double.Parse(p.Attribute("midPointX").Value),
                               midPointY = double.Parse(p.Attribute("midPointY").Value),
                               outline = p.Descendants("Outline").First(),
                               userRotation = double.Parse(p.Attribute("userRotation").Value),
                               width = int.Parse(p.Attribute("width").Value),
                               x = double.Parse(p.Attribute("x").Value),
                               y = double.Parse(p.Attribute("y").Value),

                           };

            db.KaryotypeChromosomes.InsertAllOnSubmit(dbChroms);
            db.SubmitChanges();
        }

        private void ReadNumberingTags(Connect.Database db, AI.Connect.Cell cell, XElement cellNode)
        {
            var tags = cellNode.Descendants("AnalysisTag");
            var dbPoints = from p in tags
                           select new Connect.NumberingTag
                           {
                               cellId = cell.cellId,
                               numberingTagId = Guid.NewGuid(),
                               x = double.Parse(p.Attribute("x").Value),
                               y = double.Parse(p.Attribute("y").Value),
                               display = p.Attribute("display").Value,
                               tag = p.Attribute("tag").Value
                           };

            db.NumberingTags.InsertAllOnSubmit(dbPoints);
            db.SubmitChanges();
        }
    }
}
