﻿using System;
using System.Linq;
using System.Windows.Data;
using System.Windows;

namespace AI
{
    public class SlideInfoGenerator : IValueConverter
    {
        internal class SlideInfo
        {
            public string SlideName { get; internal set; }
            public string SlideStatus { get; internal set; }
            public string LimsRef { get; internal set; }
            public string Info { get; internal set; }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var c = value as Case;
            if (c == null)
                return null;

            var dataSource = DataSourceFactory.GetDataSource();
            return (from s in dataSource.GetSlides(c.Name)
                    orderby s.Name
                    select CreateSlideInfo(s)).ToArray();
        }

        private SlideInfo CreateSlideInfo(Slide s)
        {
            var slide = new SlideInfo
            {
                SlideName = s.Name,
                SlideStatus = s.Status,
                LimsRef = s.Id,
                Info = "whatever"
            };

            var strings = ((TubsLite.App)Application.Current).Strings;

            //using (var db = new Connect.Database())
            //{
            //    var cells = db.Cells.Where(c => c.Slide.limsRef == s.Id);

            //    int count = cells.Count();
            //    int kcount = cells.Where(c => c.karyotyped).Count();
            //    slide.Info = string.Format("{0} {1}, ({2} {3})", kcount, strings.Lookup("karyotype", kcount), count, strings.Lookup("cell", count));
            //}

            return slide;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
