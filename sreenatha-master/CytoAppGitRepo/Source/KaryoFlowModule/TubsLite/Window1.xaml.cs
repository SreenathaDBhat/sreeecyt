﻿using System;
using System.Windows;
using System.Linq;
using System.Threading;
using System.Windows.Threading;
using System.Windows.Interop;
using System.Diagnostics;
using System.Windows.Input;
using System.IO;
using System.Reflection;

namespace AI
{
    public partial class Window1
    {
        public Window1()
        {
            var stack = new UIStack();
            stack.CurrentChanged += (s, e) =>
            {
                Connect.ConnectionTest.Test(this);
                navFrame.Child = e.NewValue;
            };

            UIStack.SetStack(this, stack);

            Dispatcher.ShutdownStarted += Dispatcher_ShutdownStarted;

            InitializeComponent();

            WindowLocations.Handle(this);

            Loaded += new RoutedEventHandler(RootWindow_Loaded);

            DataContext = this;
        }

        public string ApplicationVersion
        {
            get { return Application.Current == null ? "" : ((TubsLite.App)Application.Current).ApplicationVersion; }
        }

        private StringTable Strings
        {
            get { return ((TubsLite.App)TubsLite.App.Current).Strings; }
        }

        private bool firstTimeClosing = true;
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            // Anything setting e.Cancel to true will prevent the app closing
            // JR:......is that what 'e.Cancel = true' means then? Good to know.
            base.OnClosing(e);

//            Connect.ConnectionTest.Test(this);

            savingChanges.Visibility = Visibility.Visible;

            // When we hit the big red x we cancel the initial close request so that we can
            // put up a save progress display, then tell all the pages in the stack to save.
            // Only then do we fire off another close request, which sails right through 
            // (because firstTimeClosing is now false).
            var stack = UIStack.For(this);
            if (firstTimeClosing)
            {
                firstTimeClosing = false;
                e.Cancel = true;

                ThreadPool.QueueUserWorkItem(state =>
                {
                    KillStuff((UIStack)state);

                    Dispatcher.BeginInvoke((ThreadStart)delegate
                    {
                        Close();

                    }, DispatcherPriority.Normal);
                }, stack);
            }
            else
            {
                KillStuff(stack);
            }
        }

        private void KillStuff(UIStack stack)
        {
            foreach (var item in stack.WholeStack.Reverse())
            {
                if (!(item is IClosing))
                    continue;

                ((IClosing)item).Closed();
            }
        }

        void Dispatcher_ShutdownStarted(object sender, EventArgs e)
        {
            while (UIStack.For(this).Count > 0)
            {
                UIStack.For(this).Pop(UIStackResult.Cancel);
            }
        }

        void RootWindow_Loaded(object sender, RoutedEventArgs e)
        {
            ThreadPool.QueueUserWorkItem(delegate
            {
                //while (!TestConnection())
                //{
                //    Dispatcher.Invoke((ThreadStart)delegate
                //    {
                //        NetworkConnect connectScreen = new NetworkConnect();
                //        connectScreen.Owner = this;
                //        bool result = connectScreen.ShowDialog().GetValueOrDefault(false);

                //        if (!result)
                //        {
                //            Process.GetCurrentProcess().Kill();
                //        }
                //        else
                //        {
                //            Connect.Locations.Save();
                //        }

                //    }, DispatcherPriority.Normal);
                //}


                Dispatcher.Invoke((ThreadStart)delegate
                {
                    try
                    {
                        networkBusy.Visibility = Visibility.Collapsed;
                        navFrame.Focus();
                        Keyboard.Focus(navFrame);

                        UIStack.For(this).Push(new CaseListPage());
                    }
                    catch (Exception ex)
                    {
                        ExceptionUtility.ShowLog(ex, "CytoVision");
                    }
                });
            });
        }

        private static bool TestConnection()
        {
            return (Connect.Locations.ConfigFile.Exists && AI.Connect.NetworkTest.TestSql());
        }

        private void CanBfCaptureExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
    }
}
