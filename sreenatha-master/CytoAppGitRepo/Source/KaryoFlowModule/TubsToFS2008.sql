USE CytovisionDb

EXEC sp_configure filestream_access_level, 2;
GO
RECONFIGURE;
GO

ALTER DATABASE CytovisionDb ADD
FILEGROUP TUBSFILEGROUP CONTAINS FILESTREAM;
GO

ALTER DATABASE CytovisionDb ADD FILE (
	NAME = TUBSGROUPFILE,
	FILENAME = '{0}\Blobs')
TO FILEGROUP TUBSFILEGROUP;
GO	

/* START Modify Existing 2005 CytovisionDb database */
IF NOT EXISTS (SELECT * FROM dbo.syscolumns WHERE id=object_id(N'[dbo].[Blob]') AND name='binaryDataId')
	ALTER TABLE dbo.Blob ADD [binaryDataId] [uniqueidentifier] NOT NULL UNIQUE ROWGUIDCOL DEFAULT NEWID();
GO	
	
ALTER TABLE dbo.Blob
SET (FILESTREAM_ON = TUBSFILEGROUP)
GO	

ALTER TABLE dbo.Blob
ADD binaryDataFS varbinary(MAX) FILESTREAM NULL;
GO

UPDATE dbo.blob SET [binaryDataFS] = [binaryData];
GO

ALTER TABLE dbo.Blob 
DROP COLUMN binaryData;
GO

EXEC sp_rename 'CytovisionDb.dbo.Blob.binaryDataFS', 'binaryData', 'COLUMN';
GO
/* END Modify Existing 2005 CytovisionDb database */

/* Create table from scratch in 2008
CREATE TABLE [Blob](
	[blobId] [uniqueidentifier] NOT NULL,
	[caseId] [uniqueidentifier] NOT NULL,
	[path] [nvarchar] (MAX) NOT NULL,
	[binaryData] [varbinary] (max) FILESTREAM NOT NULL,
	[binaryDataId] [uniqueidentifier] NOT NULL UNIQUE ROWGUIDCOL DEFAULT NEWID(),	
CONSTRAINT [PK_blob] PRIMARY KEY CLUSTERED([blobId] ASC))
FILESTREAM_ON [TUBSFILEGROUP]
*/
