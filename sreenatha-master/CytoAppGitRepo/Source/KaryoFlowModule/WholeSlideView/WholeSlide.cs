﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Windows;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Threading;
using System.Windows.Threading;

namespace AI
{
    public class WholeSlideDisplayLocation : Notifier
    {
        private Point lookAt, displayLookAt;
        private double zoom, displayZoom;
        private static object sync = new object();
        private DateTime lastMove;

        public WholeSlideDisplayLocation()
        {
            lastMove = DateTime.Now;
            zoom = displayZoom = 1;
            lookAt = displayLookAt = new Point(0.5, 0.5);
            CompositionTarget.Rendering += FrameMove;
        }

        public double Zoom
        {
            get { lock (sync) { return displayZoom; } }
        }

        public Point LookAt
        {
            get { lock (sync) { return displayLookAt; } }
        }

        public DateTime LastMove
        {
            get { lock (sync) { return lastMove; } }
        }

        private void FrameMove(object sender, EventArgs e)
        {
            double x, y, z;
            bool bx = Creep(lookAt.X, displayLookAt.X, 0.0001, out x);
            bool by = Creep(lookAt.Y, displayLookAt.Y, 0.0001, out y);
            bool bz = Creep(zoom, displayZoom, 0.0075, out z);

            lock (sync)
            {
                displayLookAt = new Point(x, y);
                displayZoom = z;
                lastMove = DateTime.Now;
            }

            if (bx || by)
            {
                Notify("LookAt");
            }
            if (bz)
            {
                Notify("Zoom");
            }
        }

        private bool Creep(double target, double current, double epsilon, out double p)
        {
            double delta = target - current;
            double bitMoreThanEpsilon = epsilon * 1.1;
            double speedFactor = 0.05;

            if (Math.Abs(delta) > epsilon)
            {
                double n = current + delta * speedFactor;
                delta = target - n;

                p = Math.Abs(delta) < bitMoreThanEpsilon ? target : n;
            }
            else
            {
                p = current;
            }

            return true;
        }

        public void ZoomBy(double dz)
        {
            zoom += dz;
            zoom = Math.Max(0.95, Math.Min(100, zoom));
        }

        public void ZoomTo(double z)
        {
            zoom = z;
            zoom = Math.Max(0.95, Math.Min(100, zoom));
        }

        public void MoveBy(double dx, double dy)
        {
            lookAt = new Point(
                Math.Max(0.1, Math.Min(0.9, lookAt.X + dx)),
                Math.Max(0.1, Math.Min(0.9, lookAt.Y + dy))
            );
        }

        public void MoveTo(Point point)
        {
            lookAt = new Point(
                Math.Max(0.1, Math.Min(0.9, point.X / 25000.0)),
                Math.Max(0.1, Math.Min(0.9, point.Y / 75000.0))
            );
        }
    }

    public class WholeSlide : Notifier
    {
        #region Implementation Details
        public class Tile : Notifier
        {
            public Rect Position { get; set; }
            public ImageSource Image { get; set; }
            public int Level { get; set; }
            public string BlobPath { get; set; }
            public void PushNotify(string property)
            {
                Notify(property);
            }
        }
        public class SlideClickEventArgs : EventArgs
        {
            public Point Position { get; set; }
        }
        #endregion

        private List<Tile> tiles;
        private ObservableCollection<Tile> visibleTiles;
        private Guid caseId, slideId;
        private WholeSlideDisplayLocation location;
        private Thread displayThread;
        private object sync = new object();
        private Dispatcher dispatcher;
        private int maxDisplayLevel;
        private Point lastDragPoint;
        private Point? dragStart;
        private bool outsideDeadzone;
        private string slideType, slideName, slideStatus;

        public event EventHandler<SlideClickEventArgs> SlideClick;

        public WholeSlide(Guid caseId, Guid slideId)
        {
            dispatcher = Dispatcher.CurrentDispatcher;
            visibleTiles = new ObservableCollection<Tile>();

            this.caseId = caseId;
            this.slideId = slideId;

            var db = new Connect.Database();
            tiles = new List<Tile>(from t in db.CaseSlideTiles
                                   where t.slideId == slideId
                                   orderby t.subdivisionLevel
                                   select new Tile
                                   {
                                       BlobPath = t.path,
                                       Level = t.subdivisionLevel,
                                       Position = new Rect(
                                           t.positionX, t.positionY,
                                           t.positionW, t.positionH)
                                   });

            using (var lims = Connect.LIMS.New())
            {
                var slideInfo = db.Slides.Where(s => s.slideId == slideId).First();
                var limsSlide = lims.GetSlide(slideInfo.limsRef);
                slideName = limsSlide.Name;
                slideType = slideInfo.slideType;
                slideStatus = db.SlideStatus.Where(s => s.sequence == slideInfo.slideStatus).First().description;

                maxDisplayLevel = tiles.Count == 0 ? 0 : tiles.Max(t => t.Level);
                EnsureThreadIsAlive();
            }
        }

        private WholeSlide()
        {
            
        }

        public static WholeSlide Null
        {
            get { return new WholeSlide(); }
        }

        public bool IsNull
        {
            get { return tiles == null; }
        }

        public IEnumerable<Tile> VisibleTiles
        {
            get { return visibleTiles; }
        }

        public string SlideStatus { get { return slideStatus; } }
        public string SlideName { get { return slideName; } }
        public string SlideType { get { return slideType; } }
        public Guid Case { get { return caseId; } }

        public WholeSlideDisplayLocation DisplayLocation
        {
            get { return location; }
            set { location = value; }
        }

        public void MouseWheel(int mouseWheelDelta)
        {
            double scaling = Math.Max(1, DisplayLocation.Zoom) * 2;
            DisplayLocation.ZoomBy(mouseWheelDelta * 0.001 * scaling);
        }

        
        public void DisplayThread()
        {
            while (true)
            {
                if (DisplayLocation != null)
                {
                    Point look = DisplayLocation.LookAt;
                    double z = DisplayLocation.Zoom;

                    int desiredLevel = Math.Min((int)((z + 0.1) / 2.0), maxDisplayLevel);
                    bool loadedSomething = false;

                    foreach (var t in tiles)
                    {
                        if (t.Level == desiredLevel && t.Image == null)
                        {
                            loadedSomething = true;
                            var img = LoadBlob(new Connect.Database(), caseId, t.BlobPath);
                            PushImage(t, img);
                            break;
                        }
                    }

                    if (loadedSomething)
                        continue;

                    RemoveUselessTiles(desiredLevel);
                }

                Thread.Sleep(100);
            }
        }

        private ImageSource LoadBlob(Connect.Database db, Guid caseId, string p)
        {
            using (var stream = Connect.BlobStream.ReadBlobStream(caseId, p, db, Connect.CacheOption.Cache))
            {
                BitmapImage img = new BitmapImage();
                img.BeginInit();
                img.CacheOption = BitmapCacheOption.OnLoad;
                img.StreamSource = stream;
                img.EndInit();
                img.Freeze();

                stream.Close();
                return img;
            }
        }

        private void RemoveUselessTiles(int desiredLevel)
        {
            dispatcher.Invoke((ThreadStart)delegate
            {
                for (int i = visibleTiles.Count - 1; i >= 0; --i)
                {
                    if (visibleTiles[i].Level != desiredLevel)
                    {
                        visibleTiles[i].Image = null;
                        visibleTiles.RemoveAt(i);
                    }
                }
            }, DispatcherPriority.Normal);
        }

        private void PushImage(Tile t, ImageSource img)
        {
            dispatcher.Invoke((ThreadStart)delegate
            {
                t.Image = img;
                t.PushNotify("Image");
                visibleTiles.Add(t);

            }, DispatcherPriority.Normal);
        }

        public void MouseDown(Point point)
        {
            lastDragPoint = point;
            dragStart = point;
            outsideDeadzone = false;
        }

        public void MouseMove(Point point)
        {
            if (dragStart != null)
            {
                double zoom = Math.Min(3, DisplayLocation.Zoom);
                double scaling = 1 / (Math.Max(1, zoom) * 0.2);
                double pdx = point.X - lastDragPoint.X;
                double pdy = point.Y - lastDragPoint.Y;
                double dx = pdx * -0.01 * scaling;
                double dy = pdy * -0.0033 * scaling;
                lastDragPoint = point;
                DisplayLocation.MoveBy(dx, dy);
                
                if (!outsideDeadzone && (Math.Abs(point.X - dragStart.Value.X) > 3 || Math.Abs(point.Y - dragStart.Value.Y) > 3))
                    outsideDeadzone = true;
            }
        }

        public void EnsureThreadIsAlive()
        {
            if (displayThread == null || !displayThread.IsAlive)
            {
                displayThread = new Thread(DisplayThread);
                displayThread.Name = "Tile Selection Thread";
                displayThread.IsBackground = true;
                displayThread.Start();
            }
        }

        public void MouseUp(Point point)
        {
            dragStart = null;

            if (!outsideDeadzone && SlideClick != null)
            {
                SlideClick(this, new SlideClickEventArgs
                {
                    Position = new Point(point.X * 100, point.Y * 100)
                });
            }
        }
    }
}
