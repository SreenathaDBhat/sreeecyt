﻿using System;
using System.Windows;
using AI;
using System.Windows.Input;
using System.Windows.Controls;

namespace AI
{
    public class WholeSlideView : ContentControl
    {
        public static DependencyProperty WholeSlideProperty = DependencyProperty.Register("WholeSlide", typeof(WholeSlide), typeof(WholeSlideView), new FrameworkPropertyMetadata(OnWholeSlideChanged));
        public event EventHandler<WholeSlide.SlideClickEventArgs> Click;
        private FrameworkElement topLevel, zoomer;

        public WholeSlideView()
        {
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            topLevel = (FrameworkElement)GetTemplateChild("PART_TopLevel");
            zoomer = (FrameworkElement)GetTemplateChild("PART_Zoomer");
            topLevel.MouseWheel += OnMouseWheel;
            topLevel.MouseDown += OnMouseDown;
            topLevel.MouseMove += OnMouseMove;
            topLevel.MouseUp += OnMouseUp;
            topLevel.DataContext = WholeSlide;
        }

        public WholeSlide WholeSlide
        {
            get { return (WholeSlide)GetValue(WholeSlideProperty); }
            set { SetValue(WholeSlideProperty, value); topLevel.DataContext = value; }
        }

        private static void OnWholeSlideChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            WholeSlideView me = (WholeSlideView)sender;

            if (e.OldValue != null)
                ((WholeSlide)e.OldValue).SlideClick -= me.OnSlideClick;
            if(e.NewValue != null)
                ((WholeSlide)e.NewValue).SlideClick += me.OnSlideClick;
        }

        private void OnSlideClick(object sender, WholeSlide.SlideClickEventArgs e)
        {
            if (Click != null)
                Click(sender, e);
        }

        private void OnMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            WholeSlide.MouseWheel(e.Delta);
        }

        private void OnMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            e.Handled = false;

            if (e.ChangedButton == MouseButton.Middle)
            {
                e.Handled = true;
                topLevel.CaptureMouse();
                WholeSlide.MouseDown(e.GetPosition(zoomer));
            }
        }

        private void OnMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            WholeSlide.MouseMove(e.GetPosition(zoomer));
        }

        private void OnMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            topLevel.ReleaseMouseCapture();

            if(e.ChangedButton == MouseButton.Middle)
            {
                WholeSlide.MouseUp(e.GetPosition(zoomer));
            }
        }

        public Point TranslateMouseIntoSlideCoords(MouseEventArgs e)
        {
            var p = e.GetPosition(zoomer);
            return new Point(p.X * 100, p.Y * 100);
        }
    }
}
