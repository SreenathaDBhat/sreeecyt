SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
if not exists (select * from master.dbo.sysdatabases where name = 'CytoVisionDb')
CREATE DATABASE CytoVisionDb
GO

USE CytoVisionDb

CREATE TABLE [Slide](
	[slideId] [uniqueidentifier] NOT NULL,
	[caseId] [uniqueidentifier] NOT NULL,
	[limsRef] [nvarchar](256) NOT NULL,
	[slideStatus] [tinyint] NOT NULL DEFAULT(0),
	[statusChanged] [datetime] NOT NULL DEFAULT(getdate()),
	[slideType] [nvarchar](256) NOT NULL DEFAULT('Not Set')
CONSTRAINT PK_Slides PRIMARY KEY CLUSTERED([slideId] ASC));

CREATE TABLE [Case](
	[caseId] [uniqueidentifier] NOT NULL,
	[limsRef] [nvarchar](256) NOT NULL,
	[version] int NOT NULL DEFAULT(0),
	[archiveLabel] [nvarchar](50) NULL,	 
	[status] [int] NOT NULL CONSTRAINT [DF_Case_status]  DEFAULT ((1)),
CONSTRAINT PK_Cases PRIMARY KEY CLUSTERED([caseId] ASC))

CREATE TABLE [CaseLock](
	[caseId] [uniqueidentifier] NOT NULL,
	[locker] [nvarchar](max) NOT NULL,
	[date] [datetime] NOT NULL,
 CONSTRAINT [PK_CaseLock] PRIMARY KEY CLUSTERED([caseId] ASC))

CREATE TABLE [Cell](
	[cellId] [uniqueidentifier] NOT NULL,
	[slideId] [uniqueidentifier] NOT NULL,
	[cellName] [nvarchar](128) NOT NULL,
	[color] [int] NOT NULL DEFAULT(0),
	[counted] [bit] NOT NULL DEFAULT(0),
	[analysed] [bit] NOT NULL DEFAULT(0),
	[karyotyped] [bit] NOT NULL DEFAULT(0),
	[karyotypedBlobId] [uniqueidentifier] NULL,
	[imageGroup] [uniqueidentifier] NOT NULL,
	[threshold] int NOT NULL default(0)
CONSTRAINT PK_Cells PRIMARY KEY CLUSTERED([cellId] ASC))

CREATE TABLE [CellMetaData](
	[cellMetaDataId] [uniqueidentifier] NOT NULL,
	[cellId] [uniqueidentifier] NOT NULL,
	[field] [nvarchar](255) NOT NULL,
	[value] [nvarchar](max) NOT NULL,
	[confidential] [bit] NOT NULL
CONSTRAINT PK_CellMetaData PRIMARY KEY CLUSTERED([cellMetaDataId] ASC))

CREATE TABLE [CaseMetaDataField](
	[caseMetaDataFieldId] [uniqueidentifier] NOT NULL,
	[field] [nvarchar](255) NOT NULL,
	[tally] [bigint] NOT NULL
CONSTRAINT PK_CaseMetaDataField PRIMARY KEY CLUSTERED([caseMetaDataFieldId] ASC))

CREATE TABLE [ManualCountPoint](
	[manualCountPointId] [uniqueidentifier] NOT NULL,
	[cellId] [uniqueidentifier] NOT NULL,
	[x] [float] NOT NULL,
	[y] [float] NOT NULL
CONSTRAINT [PK_manualCountPoints] PRIMARY KEY CLUSTERED ([manualCountPointId] ASC));

CREATE TABLE [NumberingTag](
	[numberingTagId] [uniqueidentifier] NOT NULL,
	[cellId] [uniqueidentifier] NOT NULL,
	[tag] [nvarchar](16) NOT NULL,
	[display] [nvarchar](max) NOT NULL,
	[x] [float] NOT NULL,
	[y] [float] NOT NULL
CONSTRAINT [PK_numberingTags] PRIMARY KEY CLUSTERED ([numberingTagId] ASC));

CREATE TABLE [FuseCutout](
	[fuseCutoutId] [uniqueidentifier] NOT NULL,
	[cellId] [uniqueidentifier] NOT NULL,
	[cutoutImageFrameId] [uniqueidentifier] NOT NULL,
	[fuseImageFrameId] [uniqueidentifier] NOT NULL,
	[boundsLeft] [int] NOT NULL,
	[boundsTop] [int] NOT NULL,
	[boundsWidth] [int] NOT NULL,
	[boundsHeight] [int] NOT NULL,
CONSTRAINT [PK_FuseCutout] PRIMARY KEY CLUSTERED ([fuseCutoutId] ASC));

CREATE TABLE [ImageFrame](
	[imageFrameId] [uniqueidentifier] NOT NULL,
	[slideId] [uniqueidentifier] NOT NULL,
	[width] [int] NOT NULL,
	[height] [int] NOT NULL,
	[bpp] [int] NOT NULL,
	[location] [nvarchar](256) NOT NULL DEFAULT('N/A'),
	[imageType] [int] NOT NULL,
	[groupId] [uniqueidentifier] NOT NULL,
	[blobid] [uniqueidentifier] NULL,
	[x] [float] NOT NULL,
	[y] [float] NOT NULL,
	[gamma] [float] NOT NULL default(1)
CONSTRAINT [PK_imageFrames] PRIMARY KEY CLUSTERED ([imageFrameId] ASC));

CREATE TABLE [ImageComponent](
	[imageComponentId] [uniqueidentifier] NOT NULL,
	[frameId] [uniqueidentifier] NOT NULL,
	[isCounterstain] [bit] NOT NULL,
	[displayName] [nvarchar](256) NOT NULL,
	[renderColor] [nvarchar](16) NOT NULL,
	[isAnalysisOverlay] [bit] NOT NULL
CONSTRAINT [PK_imageComponents] PRIMARY KEY CLUSTERED ([imageComponentId] ASC));

CREATE TABLE [ImageSnap](
	[imageSnapId] [uniqueidentifier] NOT NULL,
	[componentId] [uniqueidentifier] NOT NULL,
	[zLevel] [int] NOT NULL,
	[isProjection] [bit] NOT NULL,
	[blobId] [uniqueidentifier] NOT NULL,	
CONSTRAINT [PK_imageSnaps] PRIMARY KEY CLUSTERED ([componentId] ASC, [imageSnapId] ASC));

CREATE TABLE [Annotation](
	[annotationId] [uniqueidentifier] NOT NULL,
	[parentId] [uniqueidentifier] NOT NULL,
	[x] [float] NOT NULL,
	[y] [float] NOT NULL,
	[content] [ntext] NOT NULL,
	[type] [int] NOT NULL,
	[scale] [float] NOT NULL,
	[offsetX] [float] NOT NULL,
	[offsetY] [float] NOT NULL,
	[caseId] [uniqueidentifier] NOT NULL,
CONSTRAINT [PK_annotations] PRIMARY KEY CLUSTERED ([annotationId] ASC));

CREATE TABLE [ProbeAssay](
	[assayId] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](max) NOT NULL,
	[assay] [xml] NOT NULL
CONSTRAINT PK_ProbeAssay PRIMARY KEY CLUSTERED([assayId] ASC))

CREATE TABLE [ProbeScoreSession](
	[sessionId] [uniqueidentifier] NOT NULL,
	[slideId] [uniqueidentifier] NOT NULL,
	[assay] [xml] NOT NULL,
	[ownerUser] [nvarchar](max) NOT NULL,
	[type] [int] NOT NULL,
	[modified] [datetime] NOT NULL,
CONSTRAINT PK_ProbeScoreSession PRIMARY KEY CLUSTERED([sessionId] ASC))

CREATE TABLE [ProbeScore](
	[scoreId] [uniqueidentifier] NOT NULL,
	[frameId] [uniqueidentifier] NOT NULL,
	[sessionId] [uniqueidentifier] NOT NULL,
	[x] [float] NOT NULL,
	[y] [float] NOT NULL,
	[content] [nvarchar](256) NOT NULL,
	[color] [nvarchar](16) NOT NULL,
	[measurements] [ntext] NOT NULL,
	[blobId] [uniqueidentifier] NOT NULL
CONSTRAINT [PK_ProbeScore] PRIMARY KEY CLUSTERED ([scoreId] ASC));

CREATE TABLE [ScratchPad](
	[scratchPadId] [uniqueidentifier] NOT NULL,
	[caseId] [uniqueidentifier] NOT NULL,
	[name] [nvarchar] (50) NOT NULL,
	[description] [nvarchar] (max) NOT NULL
CONSTRAINT [PK_scratchPad] PRIMARY KEY CLUSTERED([scratchPadId] ASC));

CREATE TABLE [ScratchPadItem](
	[scratchPadItemId] [uniqueidentifier] NOT NULL,
	[scratchPadId] [uniqueidentifier] NOT NULL,
	[itemDescription] [xml] NOT NULL,
	[x] [float] NOT NULL,
	[y] [float] NOT NULL,
	[angleToVertical] [float] NOT NULL,
	[userRotation] [float] NOT NULL,
	[blobId] [uniqueidentifier] NULL,
	[flipX] [bit] NOT NULL DEFAULT(0),
	[flipY] [bit] NOT NULL DEFAULT(0)
CONSTRAINT [PK_scrathPadChromosome] PRIMARY KEY CLUSTERED([scratchPadItemId] ASC));

CREATE TABLE [KaryotypeChromosome](
	[karyotypeChromosomeId] [uniqueidentifier] NOT NULL,
	[x] [float] NOT NULL,
	[y] [float] NOT NULL,
	[angleToVertical] [float] NOT NULL,
	[userRotation] [float] NOT NULL,
	[cellId] [uniqueidentifier] NOT NULL,
	[chromosomeTag] [nvarchar](16) NOT NULL,
	[groupIndex] [int] NOT NULL,
	[boundsLeft] [float] NOT NULL,
	[boundsTop] [float] NOT NULL,
	[boundsWidth] [float] NOT NULL,
	[boundsHeight] [float] NOT NULL,
	[width] [int] NOT NULL,
	[length] [int] NOT NULL,
	[blobId] [uniqueidentifier] NOT NULL,
	[outline] [xml] NOT NULL,
	[midPointX] [float] NOT NULL,
	[midPointY] [float] NOT NULL,
	[flipX] [bit] NOT NULL DEFAULT(0),
	[flipY] [bit] NOT NULL DEFAULT(0)
CONSTRAINT [PK_karyotypeChromosome] PRIMARY KEY CLUSTERED([karyotypeChromosomeId] ASC));

CREATE TABLE [ClearedChromosome](
	[clearedChromosomeId] [uniqueidentifier] NOT NULL,
	[cellId] [uniqueidentifier] NOT NULL,
	[chromosomeTag] [nvarchar] (50) NOT NULL,
	[clearedLevel] [int] NOT NULL
CONSTRAINT [PK_clearedChromosome] PRIMARY KEY CLUSTERED([clearedChromosomeId] ASC));

CREATE TABLE [Blob](
	[blobId] [uniqueidentifier] NOT NULL,
	[caseId] [uniqueidentifier] NOT NULL,
	[path] [nvarchar] (128) NOT NULL,
	[binaryData] [image] NOT NULL
CONSTRAINT [PK_blob] PRIMARY KEY CLUSTERED([blobId] ASC));

CREATE TABLE Version([number] [int]);

CREATE TABLE [Status](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StatusCode] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_StatusID (Clustered)] PRIMARY KEY CLUSTERED ([ID] ASC));

CREATE TABLE [Flag](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FlagType] [int] NULL,
	[FlagName] [nvarchar](20) NOT NULL,
	[Enabled] [bit] NULL,
 CONSTRAINT [PK_FlagID] PRIMARY KEY CLUSTERED([ID] ASC)); 

CREATE TABLE [User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[GroupFlag] [bit] NULL,
	[GroupID] [int] NULL CONSTRAINT [DF_User_GroupID]  DEFAULT ((-1)),
 CONSTRAINT [PK_UserID (Clustered)] PRIMARY KEY CLUSTERED ([ID] ASC));

CREATE TABLE [UserStatusFlag](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[FlagID] [int] NOT NULL,
	[Enabled] [bit] NOT NULL,
 CONSTRAINT [PK_UserStatusFlagID] PRIMARY KEY CLUSTERED ([ID] ASC));
 
 CREATE TABLE [EventLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[MachineName] [nvarchar](50) NOT NULL,
	[SessionID] [uniqueidentifier] NOT NULL,
	[EventType] [int] NOT NULL,
	[CaseID] [uniqueidentifier] NULL,
	[CaseStatus] [nvarchar](20) NULL,
	[Description] [nvarchar](128) NULL,
 CONSTRAINT [PK_EventLog] PRIMARY KEY CLUSTERED ([ID] ASC));


CREATE TABLE [SlideImageTile](
	[tileId] [uniqueidentifier] NOT NULL,
	[slideId] [uniqueidentifier] NOT NULL,
	[positionX] [float] NOT NULL,
	[positionY] [float] NOT NULL,
	[positionW] [float] NOT NULL,
	[positionH] [float] NOT NULL,
	[subdivisionLevel] [smallint] NOT NULL,
	[blobId] [uniqueidentifier] NOT NULL
CONSTRAINT [PK_SlideImageTile] PRIMARY KEY CLUSTERED ([tileId] ASC));

CREATE TABLE [DeferredCapture](
	[regionId] [uniqueidentifier] NOT NULL,
	[slideId] [uniqueidentifier] NOT NULL,
	[left] [float] NOT NULL,
	[right] [float] NOT NULL,
	[top] [float] NOT NULL,
	[bottom] [float] NOT NULL,
	[options] [xml] NOT NULL,
	[owner] [nvarchar](MAX) NOT NULL,
	[comment] [ntext] NOT NULL,
	[modified] [datetime] NOT NULL,
	[status] [int] NOT NULL
CONSTRAINT [PK_DeferredCapture] PRIMARY KEY CLUSTERED ([regionId] ASC));

CREATE TABLE [Config](
	[id] [uniqueidentifier] NOT NULL,
	[title] [nvarchar](128) NOT NULL,
	[text] [nvarchar](max) NULL,
CONSTRAINT [PK_Config] PRIMARY KEY CLUSTERED ([id] ASC));

CREATE TABLE [ArchiveDevice](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[path] [nvarchar](max) NOT NULL,
	[mediaLabel] [nvarchar](50) NULL,
	[available] [bit] NULL
CONSTRAINT [PK_ArchiveDevice] PRIMARY KEY CLUSTERED ([id] ASC));

CREATE TABLE [ChromosomeGroup](
	[chromosomeGroupId] [uniqueidentifier] NOT NULL,
	[classIdentifier] [nvarchar](max) NOT NULL,
	[caseId] [uniqueidentifier] NOT NULL,
	[index] [int] NOT NULL
CONSTRAINT [PK_ChromosomeGroup] PRIMARY KEY CLUSTERED ([chromosomeGroupId] ASC));

CREATE TABLE [SlideStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[sequence] [tinyint] NOT NULL,
	[description] [nvarchar](50)  NOT NULL
 CONSTRAINT [PK_SlideStatus (Clustered)] PRIMARY KEY CLUSTERED ([ID] ASC));

CREATE TABLE [dbo].[ImageFrameStar](
	[starId] [uniqueidentifier] NOT NULL,
	[sessionId] [uniqueidentifier] NOT NULL,
	[imageFrameId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_ImageFrameStar] PRIMARY KEY CLUSTERED ([starId] ASC));
 



/* CREATE TABLE INDEXES */
CREATE UNIQUE NONCLUSTERED INDEX [CaseLimsRef] ON [Case] ([limsRef] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [SlideLimsRef] ON [Slide] ([limsRef] ASC);
CREATE NONCLUSTERED INDEX [ImageType] ON [ImageFrame] ([imageType] ASC);
CREATE NONCLUSTERED INDEX [GroupId] ON [ImageFrame] ([groupId] ASC);
CREATE NONCLUSTERED INDEX [CaseMetadataFieldTally] ON [CaseMetaDataField] ([tally] DESC);
CREATE NONCLUSTERED INDEX [CellMetadataField] ON [CellMetaData] ([field] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [StatusCode] ON [Status] ([StatusCode] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [FlagName] ON [Flag] ([FlagName] ASC);
CREATE NONCLUSTERED INDEX [GroupFlag] ON [User] ([GroupFlag] ASC);
CREATE NONCLUSTERED INDEX [Name] ON [User] ([Name] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [UserStatusFlags] ON [UserStatusFlag] ([UserID] ASC,[StatusID] ASC,[FlagID] ASC);
CREATE NONCLUSTERED INDEX [CaseID] ON [EventLog] ([CaseID] ASC);
CREATE NONCLUSTERED INDEX [DateTime] ON [EventLog] ([DateTime] ASC);
CREATE NONCLUSTERED INDEX [EventType] ON [EventLog] ([EventType] ASC);
CREATE NONCLUSTERED INDEX [UserName] ON [EventLog] ([UserName] ASC);
CREATE NONCLUSTERED INDEX [caseId+path] ON [Blob] ([caseId] ASC, [path] ASC);
CREATE NONCLUSTERED INDEX [CellId+ChromosomeId] ON [KaryotypeChromosome] ([cellId] ASC,	[karyotypeChromosomeId] ASC);
CREATE NONCLUSTERED INDEX [CellId] ON [ClearedChromosome] ([cellId] ASC, [clearedChromosomeId] ASC);
CREATE NONCLUSTERED INDEX [CellId] ON [ManualCountPoint] ([cellId] ASC);
CREATE NONCLUSTERED INDEX [CellID] ON [NumberingTag] ([cellId] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [FrameId+ComponentId] ON [ImageComponent] ([frameId] ASC,[imageComponentId] ASC);
CREATE NONCLUSTERED INDEX [FrameID] ON [ProbeScore] ([frameId] ASC);
CREATE NONCLUSTERED INDEX [SessionID] ON [ProbeScore] ([sessionId] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [ComponentId+SnapId] ON [ImageSnap] ([componentId] ASC,[imageSnapId] ASC);
CREATE NONCLUSTERED INDEX [ScratchPadId] ON [ScratchPadItem] ([scratchPadId] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [SlideId+ImageFrameId] ON [ImageFrame] ([slideId] ASC,	[imageFrameId] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [SlideID+CellId] ON [Cell] ([slideId] ASC,	[cellId] ASC);
CREATE NONCLUSTERED INDEX [SlideId+SessionID] ON [ProbeScoreSession] ([slideId] ASC,[sessionId] ASC);
CREATE NONCLUSTERED INDEX [CaseId] ON [ScratchPad] ([caseId] ASC);
CREATE NONCLUSTERED INDEX [CaseId] ON [Annotation] ([caseId] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [CaseId+SlideId] ON [Slide] ([caseId] ASC,[slideId] ASC);
CREATE NONCLUSTERED INDEX [blobId] ON [KaryotypeChromosome] ([blobId] ASC);
CREATE NONCLUSTERED INDEX [blobId] ON [ImageSnap] ([blobId] ASC);
CREATE NONCLUSTERED INDEX [blobId] ON [ProbeScore] ([blobId] ASC);
CREATE NONCLUSTERED INDEX [SlideID] ON [SlideImageTile] ([slideId] ASC);
CREATE NONCLUSTERED INDEX [blobId] ON [SlideImageTile] ([blobId] ASC);
CREATE NONCLUSTERED INDEX [SlideID] ON [DeferredCapture] ([slideId] ASC);
CREATE NONCLUSTERED INDEX [ArchiveLabel] ON [Case] ([archiveLabel] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [StatusSequence] ON [SlideStatus] ([sequence] ASC );

/* CREATE TABLE RELATIONSHIPS */
ALTER TABLE [Blob]					WITH CHECK ADD CONSTRAINT [FK_Blob_Case] FOREIGN KEY([caseId]) REFERENCES [Case] ([caseId]) ON DELETE CASCADE;
ALTER TABLE [Cell]					WITH CHECK ADD CONSTRAINT [FK_Cell_Slide] FOREIGN KEY([slideId]) REFERENCES [Slide] ([slideId]) ON DELETE CASCADE;
ALTER TABLE [CellMetaData]			WITH CHECK ADD CONSTRAINT [FK_Meta_Cell] FOREIGN KEY([cellId]) REFERENCES [Cell] ([cellId]) ON DELETE CASCADE;
ALTER TABLE [ChromosomeGroup]		WITH CHECK ADD CONSTRAINT [FK_Group_Case] FOREIGN KEY([caseId]) REFERENCES [Case] ([caseId]) ON DELETE CASCADE;
ALTER TABLE [ClearedChromosome]		WITH CHECK ADD CONSTRAINT [FK_ClearedChromosome_Cell] FOREIGN KEY([cellId]) REFERENCES [Cell] ([cellId])ON DELETE CASCADE;
ALTER TABLE [DeferredCapture]		WITH CHECK ADD CONSTRAINT [FK_Dcap_Slide] FOREIGN KEY([slideId]) REFERENCES [Slide] ([slideId]) ON DELETE CASCADE;
ALTER TABLE [ImageComponent]		WITH CHECK ADD CONSTRAINT [FK_ImageComponent_ImageFrame] FOREIGN KEY([frameId]) REFERENCES [ImageFrame] ([imageFrameId]) ON DELETE CASCADE;
ALTER TABLE [ImageFrame]			WITH CHECK ADD CONSTRAINT [FK_ImageFrame_Slide] FOREIGN KEY([slideId]) REFERENCES [Slide] ([slideId]) ON DELETE CASCADE;
ALTER TABLE [ImageSnap]				WITH CHECK ADD CONSTRAINT [FK_ImageSnap_ImageComponent] FOREIGN KEY([componentId]) REFERENCES [ImageComponent] ([imageComponentId]) ON DELETE CASCADE;
ALTER TABLE [KaryotypeChromosome]	WITH CHECK ADD CONSTRAINT [FK_KaryotypeChromosome_Cell] FOREIGN KEY([cellId]) REFERENCES [Cell] ([cellId]) ON DELETE CASCADE;
ALTER TABLE [ManualCountPoint]		WITH CHECK ADD CONSTRAINT [FK_ManualCountPoint_Cell] FOREIGN KEY([cellId]) REFERENCES [Cell] ([cellId]) ON DELETE CASCADE;
ALTER TABLE [NumberingTag]			WITH CHECK ADD CONSTRAINT [FK_NumberingTag_Cell] FOREIGN KEY([cellId]) REFERENCES [Cell] ([cellId]) ON DELETE CASCADE;
ALTER TABLE [FuseCutout]  			WITH CHECK ADD CONSTRAINT [FK_FuseCutout_Cell] FOREIGN KEY([cellId]) REFERENCES [Cell] ([cellId]) ON DELETE CASCADE;
ALTER TABLE [FuseCutout]  			WITH CHECK ADD CONSTRAINT [FK_FuseCutout_ImageFrame_cutout] FOREIGN KEY([cutoutImageFrameId]) REFERENCES [ImageFrame] ([imageFrameId]);
ALTER TABLE [FuseCutout]  			WITH CHECK ADD CONSTRAINT [FK_FuseCutout_ImageFrame_fuse] FOREIGN KEY([fuseImageFrameId]) REFERENCES [ImageFrame] ([imageFrameId]);
ALTER TABLE [ProbeScore]			WITH CHECK ADD CONSTRAINT [FK_ProbeScore_SessionID] FOREIGN KEY([sessionId]) REFERENCES [ProbeScoreSession] ([sessionId]) ON DELETE CASCADE;
ALTER TABLE [ProbeScore]			WITH CHECK ADD CONSTRAINT [FK_ProbeScore_FrameId] FOREIGN KEY([frameId]) REFERENCES [ImageFrame] ([imageFrameId]) ON DELETE NO ACTION;
ALTER TABLE [ProbeScoreSession]		WITH CHECK ADD CONSTRAINT [FK_ProbeScoreSession_Slide] FOREIGN KEY([slideId]) REFERENCES [Slide] ([slideId]) ON DELETE CASCADE;
ALTER TABLE [ScratchPad]			WITH CHECK ADD CONSTRAINT [FK_ScratchPad_Case] FOREIGN KEY([caseId]) REFERENCES [Case] ([caseId]) ON DELETE CASCADE;
ALTER TABLE [ScratchPadItem]		WITH CHECK ADD CONSTRAINT [FK_ScratchPadItem_ScratchPad] FOREIGN KEY([scratchPadId]) REFERENCES [ScratchPad] ([scratchPadId]) ON DELETE CASCADE;
ALTER TABLE [Slide]					WITH CHECK ADD CONSTRAINT [FK_Slide_Case] FOREIGN KEY([caseId]) REFERENCES [Case] ([caseId]) ON DELETE CASCADE;
ALTER TABLE [SlideImageTile]		WITH CHECK ADD CONSTRAINT [FK_SlideImageTile_Slide] FOREIGN KEY([slideId]) REFERENCES [Slide] ([slideId]) ON DELETE CASCADE;
ALTER TABLE [UserStatusFlag]        WITH CHECK ADD CONSTRAINT [FK_UserStatusFlag_Flag] FOREIGN KEY([FlagID]) REFERENCES [Flag] ([ID]);
ALTER TABLE [UserStatusFlag]        WITH CHECK ADD CONSTRAINT [FK_UserStatusFlag_Status] FOREIGN KEY([StatusID]) REFERENCES [Status] ([ID]);
ALTER TABLE [UserStatusFlag]        WITH CHECK ADD CONSTRAINT [FK_UserStatusFlag_User] FOREIGN KEY([UserID]) REFERENCES [User] ([ID]);
ALTER TABLE [Annotation]			WITH CHECK ADD CONSTRAINT [FK_Annotation_Case] FOREIGN KEY([caseId])REFERENCES [Case] ([caseId]) ON DELETE CASCADE;
ALTER TABLE [ImageFrameStar]		WITH CHECK ADD CONSTRAINT [FK_ImageFrameStar_ImageFrame] FOREIGN KEY([imageFrameId]) REFERENCES [dbo].[ImageFrame] ([imageFrameId]);
ALTER TABLE [ImageFrameStar]		WITH CHECK ADD CONSTRAINT [FK_ImageFrameStar_ProbeScoreSession] FOREIGN KEY([sessionId]) REFERENCES [dbo].[ProbeScoreSession] ([sessionId]) ON DELETE CASCADE;

INSERT INTO [Version] VALUES(46000)

GO

CREATE VIEW [UserStatusFlagView]
AS
SELECT     [User].Name, [User].GroupID, Status.StatusCode, Flag.FlagName, UserStatusFlag.Enabled, [User].ID
FROM         UserStatusFlag INNER JOIN
                      [User] ON UserStatusFlag.UserID = [User].ID INNER JOIN
                      Status ON UserStatusFlag.StatusID = Status.ID INNER JOIN
                      Flag ON UserStatusFlag.FlagID = Flag.ID

GO

CREATE VIEW [CaseKaryotypeChromosome] AS
SELECT	[Case].caseId, 
		Slide.slideId, 
		Cell.cellName, 
		KaryotypeChromosome.karyotypeChromosomeId, 
		KaryotypeChromosome.x, 
		KaryotypeChromosome.y, 
		KaryotypeChromosome.angleToVertical,
		KaryotypeChromosome.userRotation, 
		KaryotypeChromosome.cellId, 
		KaryotypeChromosome.chromosomeTag, 
		KaryotypeChromosome.groupIndex, 
		KaryotypeChromosome.boundsLeft, 
		KaryotypeChromosome.boundsTop, 
		KaryotypeChromosome.boundsWidth,
		KaryotypeChromosome.boundsHeight,
		KaryotypeChromosome.width, 
		KaryotypeChromosome.outline,
		KaryotypeChromosome.length, 
		KaryotypeChromosome.blobId,
		KaryotypeChromosome.midPointX, 
		KaryotypeChromosome.midPointY,
		KaryotypeChromosome.flipX,
		KaryotypeChromosome.flipY

FROM	KaryotypeChromosome INNER JOIN
		Cell ON KaryotypeChromosome.cellId = Cell.cellId INNER JOIN
		Slide ON Cell.slideId = Slide.slideId INNER JOIN
		[Case] ON Slide.caseId = [Case].caseId
GO

CREATE VIEW [CaseClearedChromosomes]
AS
SELECT     [Case].caseId, ClearedChromosome.cellId, ClearedChromosome.chromosomeTag, ClearedChromosome.clearedLevel, 
                      ClearedChromosome.clearedChromosomeId
FROM         ClearedChromosome INNER JOIN
                      Cell ON ClearedChromosome.cellId = Cell.cellId INNER JOIN
                      Slide ON Cell.slideId = Slide.slideId INNER JOIN
                      [Case] ON Slide.caseId = [Case].caseId
GO

CREATE VIEW [dbo].[CaseCell]
AS
SELECT     [Case].caseId, Cell.cellId, Slide.slideId, Cell.cellName, 
                      Cell.imageGroup, Cell.counted, Cell.analysed, Cell.karyotyped, Cell.color, Cell.threshold
FROM         [Case] INNER JOIN
                      Slide ON [Case].caseId = Slide.caseId INNER JOIN
                      Cell ON Slide.slideId = Cell.slideId
GO

CREATE VIEW [dbo].[CaseCellMetaData]
AS
SELECT     [CellMetaData].cellMetaDataId, [Case].caseId, [Cell].cellId, [CellMetaData].[field], [CellMetaData].[value], [CellMetaData].[confidential]
FROM       [Case] INNER JOIN
            Slide ON [Case].caseId = Slide.caseId INNER JOIN
            Cell ON Slide.slideId = Cell.slideId INNER JOIN
            CellMetaData ON Cell.cellId = CellMetaData.cellId
GO

GO
CREATE VIEW [dbo].[ImageFrameFiles]
AS
SELECT     Blob.blobId, Blob.path, ImageFrame.imageFrameId, [Case].caseId
FROM         ImageFrame INNER JOIN
                      ImageComponent ON ImageFrame.imageFrameId = ImageComponent.frameId INNER JOIN
                      ImageSnap ON ImageComponent.imageComponentId = ImageSnap.componentId INNER JOIN
                      Blob ON ImageSnap.blobId = Blob.blobId INNER JOIN
                      Slide ON ImageFrame.slideId = Slide.slideId INNER JOIN
                      [Case] ON Slide.caseId = [Case].caseId
GO

CREATE VIEW [dbo].[CaseImageFrames]
AS
SELECT     [Case].caseId, ImageFrame.imageType, ImageFrame.groupId, ImageFrame.imageFrameId
FROM         [Case] INNER JOIN
                      Slide ON [Case].caseId = Slide.caseId INNER JOIN
                      ImageFrame ON Slide.slideId = ImageFrame.slideId
GO

CREATE VIEW [dbo].[CaseSlideTiles]
AS
SELECT     dbo.Slide.slideId, dbo.[Case].caseId, dbo.SlideImageTile.positionX, 
                      dbo.SlideImageTile.positionY, dbo.SlideImageTile.tileId, dbo.SlideImageTile.positionW, dbo.SlideImageTile.positionH, 
                      dbo.SlideImageTile.subdivisionLevel, dbo.Blob.blobId, dbo.Blob.path, dbo.Blob.binaryData
FROM         dbo.SlideImageTile INNER JOIN
                      dbo.Slide ON dbo.SlideImageTile.slideId = dbo.Slide.slideId INNER JOIN
                      dbo.Blob ON dbo.SlideImageTile.blobId = dbo.Blob.blobId INNER JOIN
                      dbo.[Case] ON dbo.Slide.caseId = dbo.[Case].caseId

GO




 /* LIMS FALLBACK TABLES */
 CREATE TABLE [LMF_Cases](
	[id] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](256) NOT NULL,
	[created] [datetime] NOT NULL
CONSTRAINT [PK_LMFCases] PRIMARY KEY CLUSTERED ([id] ASC));

CREATE TABLE [LMF_CaseMetaData](
	[id] [uniqueidentifier] NOT NULL,
	[limsCaseId] [uniqueidentifier] NOT NULL,
	[field] [nvarchar](255) NOT NULL,
	[value] [nvarchar](max) NOT NULL,
CONSTRAINT [PK_LMFCaseMetaData] PRIMARY KEY CLUSTERED ([id] ASC));
 
CREATE TABLE [LMF_Slides](
	[id] [uniqueidentifier] NOT NULL,
	[limsCaseId] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](256) NOT NULL,
	[barcode] [nvarchar](512) NULL,
CONSTRAINT [PK_LMFSlides] PRIMARY KEY CLUSTERED ([id] ASC));
 
CREATE TABLE [LMF_SlideMetaData](
	[id] [uniqueidentifier] NOT NULL,
	[limsSlideId] [uniqueidentifier] NOT NULL,
	[field] [nvarchar](256) NOT NULL,
	[value] [nvarchar](max) NOT NULL,
CONSTRAINT [PK_LMFSlideMetaData] PRIMARY KEY CLUSTERED ([id] ASC));

ALTER TABLE [LMF_CaseMetaData]  WITH CHECK ADD  CONSTRAINT [FK_LMF_CaseMetaData_Cases] FOREIGN KEY([limsCaseId])REFERENCES [LMF_Cases] ([id]) ON DELETE CASCADE;
ALTER TABLE [LMF_Slides]  WITH CHECK ADD  CONSTRAINT [FK_LMF_Slides_Cases] FOREIGN KEY([limsCaseId])REFERENCES [LMF_Cases] ([id]) ON DELETE CASCADE;
ALTER TABLE [LMF_SlideMetaData]  WITH CHECK ADD  CONSTRAINT [FK_LMF_SlideMetaData_Slides] FOREIGN KEY([limsSlideId]) REFERENCES [LMF_Slides] ([id]) ON DELETE CASCADE;




INSERT INTO [SlideStatus] ([sequence], [description]) values ( 0, 'Unassigned')
INSERT INTO [SlideStatus] ([sequence], [description]) values ( 1, 'Ready for scan')
INSERT INTO [SlideStatus] ([sequence], [description]) values ( 2, 'Ready for markup')
INSERT INTO [SlideStatus] ([sequence], [description]) values ( 3, 'Ready for capture')
INSERT INTO [SlideStatus] ([sequence], [description]) values ( 4, 'Ready for analysis')

INSERT INTO [Flag] ([FlagType], [FlagName], [Enabled]) values ( 0,'UserAccessRights', 0)
INSERT INTO [Flag] ([FlagType], [FlagName], [Enabled]) values ( 0,'UserLogging', 0)

CREATE NONCLUSTERED INDEX [CaseDate] ON [LMF_Cases] ([created] ASC);

GO
