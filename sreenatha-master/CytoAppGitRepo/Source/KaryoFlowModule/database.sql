SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
if not exists (select * from master.dbo.sysdatabases where name = 'CytoVisionDb')
CREATE DATABASE CytoVisionDb
GO

USE CytoVisionDb

CREATE TABLE [Slide](
	[slideId] [uniqueidentifier] NOT NULL,
	[caseId] [uniqueidentifier] NOT NULL,
	[limsRef] [nvarchar](256) NOT NULL
CONSTRAINT PK_Slides PRIMARY KEY CLUSTERED([slideId] ASC));

CREATE TABLE [Case](
	[caseId] [uniqueidentifier] NOT NULL,
	[limsRef] [nvarchar](256) NOT NULL,
	[status] [nvarchar](256) NOT NULL,
	[version] int NOT NULL DEFAULT(0)
CONSTRAINT PK_Cases PRIMARY KEY CLUSTERED([caseId] ASC))

CREATE TABLE [CaseLock](
	[caseId] [uniqueidentifier] NOT NULL,
	[locker] [nvarchar](max) NOT NULL,
	[date] [datetime] NOT NULL,
 CONSTRAINT [PK_CaseLock] PRIMARY KEY CLUSTERED([caseId] ASC))

CREATE TABLE [Cell](
	[cellId] [uniqueidentifier] NOT NULL,
	[slideId] [uniqueidentifier] NOT NULL,
	[cellName] [nvarchar](128) NOT NULL,
	[color] [int] NOT NULL DEFAULT(0),
	[counted] [bit] NOT NULL DEFAULT(0),
	[analysed] [bit] NOT NULL DEFAULT(0),
	[karyotyped] [bit] NOT NULL DEFAULT(0),
	[karyotypedBlobId] [uniqueidentifier] NULL,
	[imageGroup] [uniqueidentifier] NOT NULL,
	[threshold] int NOT NULL default(0)
CONSTRAINT PK_Cells PRIMARY KEY CLUSTERED([cellId] ASC))

CREATE TABLE [CellMetaData](
	[cellMetaDataId] [uniqueidentifier] NOT NULL,
	[cellId] [uniqueidentifier] NOT NULL,
	[field] [nvarchar](255) NOT NULL,
	[value] [nvarchar](max) NOT NULL,
	[confidential] [bit] NOT NULL
CONSTRAINT PK_CellMetaData PRIMARY KEY CLUSTERED([cellMetaDataId] ASC))

CREATE TABLE [CaseMetaDataField](
	[caseMetaDataFieldId] [uniqueidentifier] NOT NULL,
	[field] [nvarchar](255) NOT NULL,
	[tally] [bigint] NOT NULL
CONSTRAINT PK_CaseMetaDataField PRIMARY KEY CLUSTERED([caseMetaDataFieldId] ASC))

CREATE TABLE [ManualCountPoint](
	[manualCountPointId] [uniqueidentifier] NOT NULL,
	[cellId] [uniqueidentifier] NOT NULL,
	[x] [float] NOT NULL,
	[y] [float] NOT NULL
CONSTRAINT [PK_manualCountPoints] PRIMARY KEY CLUSTERED ([manualCountPointId] ASC));

CREATE TABLE [NumberingTag](
	[numberingTagId] [uniqueidentifier] NOT NULL,
	[cellId] [uniqueidentifier] NOT NULL,
	[tag] [nvarchar](16) NOT NULL,
	[display] [nvarchar](max) NOT NULL,
	[x] [float] NOT NULL,
	[y] [float] NOT NULL
CONSTRAINT [PK_numberingTags] PRIMARY KEY CLUSTERED ([numberingTagId] ASC));

CREATE TABLE [ImageFrame](
	[imageFrameId] [uniqueidentifier] NOT NULL,
	[slideId] [uniqueidentifier] NOT NULL,
	[width] [int] NOT NULL,
	[height] [int] NOT NULL,
	[bpp] [int] NOT NULL,
	[location] [nvarchar](256) NOT NULL DEFAULT('N/A'),
	[imageType] [int] NOT NULL,
	[groupId] [uniqueidentifier] NOT NULL,
	[blobid] [uniqueidentifier] NULL,
	[x] [float] NOT NULL,
	[y] [float] NOT NULL,
	[gamma] [float] NOT NULL default(1)
CONSTRAINT [PK_imageFrames] PRIMARY KEY CLUSTERED ([imageFrameId] ASC));

CREATE TABLE [ImageComponent](
	[imageComponentId] [uniqueidentifier] NOT NULL,
	[frameId] [uniqueidentifier] NOT NULL,
	[isCounterstain] [bit] NOT NULL,
	[displayName] [nvarchar](256) NOT NULL,
	[renderColor] [nvarchar](16) NOT NULL,
	[isAnalysisOverlay] [bit] NOT NULL
CONSTRAINT [PK_imageComponents] PRIMARY KEY CLUSTERED ([imageComponentId] ASC));

CREATE TABLE [ImageSnap](
	[imageSnapId] [uniqueidentifier] NOT NULL,
	[componentId] [uniqueidentifier] NOT NULL,
	[zLevel] [int] NOT NULL,
	[isProjection] [bit] NOT NULL,
	[blobId] [uniqueidentifier] NOT NULL,	
CONSTRAINT [PK_imageSnaps] PRIMARY KEY CLUSTERED ([componentId] ASC, [imageSnapId] ASC));

CREATE TABLE [ScratchPad](
	[scratchPadId] [uniqueidentifier] NOT NULL,
	[caseId] [uniqueidentifier] NOT NULL,
	[name] [nvarchar] (50) NOT NULL,
	[description] [nvarchar] (max) NOT NULL
CONSTRAINT [PK_scratchPad] PRIMARY KEY CLUSTERED([scratchPadId] ASC));

CREATE TABLE [ScratchPadItem](
	[scratchPadItemId] [uniqueidentifier] NOT NULL,
	[scratchPadId] [uniqueidentifier] NOT NULL,
	[itemDescription] [xml] NOT NULL,
	[x] [float] NOT NULL,
	[y] [float] NOT NULL,
	[angleToVertical] [float] NOT NULL,
	[userRotation] [float] NOT NULL,
	[blobId] [uniqueidentifier] NULL,
	[flipX] [bit] NOT NULL DEFAULT(0),
	[flipY] [bit] NOT NULL DEFAULT(0)
CONSTRAINT [PK_scrathPadChromosome] PRIMARY KEY CLUSTERED([scratchPadItemId] ASC));

CREATE TABLE [KaryotypeChromosome](
	[karyotypeChromosomeId] [uniqueidentifier] NOT NULL,
	[x] [float] NOT NULL,
	[y] [float] NOT NULL,
	[angleToVertical] [float] NOT NULL,
	[userRotation] [float] NOT NULL,
	[cellId] [uniqueidentifier] NOT NULL,
	[chromosomeTag] [nvarchar](16) NOT NULL,
	[groupIndex] [int] NOT NULL,
	[boundsLeft] [float] NOT NULL,
	[boundsTop] [float] NOT NULL,
	[boundsWidth] [float] NOT NULL,
	[boundsHeight] [float] NOT NULL,
	[width] [int] NOT NULL,
	[length] [int] NOT NULL,
	[blobId] [uniqueidentifier] NOT NULL,
	[outline] [xml] NOT NULL,
	[midPointX] [float] NOT NULL,
	[midPointY] [float] NOT NULL,
	[flipX] [bit] NOT NULL DEFAULT(0),
	[flipY] [bit] NOT NULL DEFAULT(0)
CONSTRAINT [PK_karyotypeChromosome] PRIMARY KEY CLUSTERED([karyotypeChromosomeId] ASC));

CREATE TABLE [Blob](
	[blobId] [uniqueidentifier] NOT NULL,
	[caseId] [uniqueidentifier] NOT NULL,
	[path] [nvarchar] (128) NOT NULL,
	[binaryData] [image] NOT NULL
CONSTRAINT [PK_blob] PRIMARY KEY CLUSTERED([blobId] ASC));

CREATE TABLE Version([number] [int]);
 
CREATE TABLE [EventLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[MachineName] [nvarchar](50) NOT NULL,
	[SessionID] [uniqueidentifier] NOT NULL,
	[EventType] [int] NOT NULL,
	[CaseID] [uniqueidentifier] NULL,
	[CaseStatus] [nvarchar](20) NULL,
	[Description] [nvarchar](128) NULL,
CONSTRAINT [PK_EventLog] PRIMARY KEY CLUSTERED ([ID] ASC)); 

CREATE TABLE [LMF_Cases](
	[id] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](256) NOT NULL,
	[created] [datetime] NOT NULL
CONSTRAINT [PK_LMFCases] PRIMARY KEY CLUSTERED ([id] ASC));

CREATE TABLE [LMF_CaseMetaData](
	[id] [uniqueidentifier] NOT NULL,
	[limsCaseId] [uniqueidentifier] NOT NULL,
	[field] [nvarchar](255) NOT NULL,
	[value] [nvarchar](max) NOT NULL,
CONSTRAINT [PK_LMFCaseMetaData] PRIMARY KEY CLUSTERED ([id] ASC));
 
CREATE TABLE [LMF_Slides](
	[id] [uniqueidentifier] NOT NULL,
	[limsCaseId] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](256) NOT NULL,
	[barcode] [nvarchar](512) NULL,
CONSTRAINT [PK_LMFSlides] PRIMARY KEY CLUSTERED ([id] ASC));
 
CREATE TABLE [LMF_SlideMetaData](
	[id] [uniqueidentifier] NOT NULL,
	[limsSlideId] [uniqueidentifier] NOT NULL,
	[field] [nvarchar](256) NOT NULL,
	[value] [nvarchar](max) NOT NULL,
CONSTRAINT [PK_LMFSlideMetaData] PRIMARY KEY CLUSTERED ([id] ASC));


/* CREATE TABLE INDEXES */
CREATE UNIQUE NONCLUSTERED INDEX [CaseLimsRef] ON [Case] ([limsRef] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [SlideLimsRef] ON [Slide] ([limsRef] ASC);
CREATE NONCLUSTERED INDEX [ImageType] ON [ImageFrame] ([imageType] ASC);
CREATE NONCLUSTERED INDEX [GroupId] ON [ImageFrame] ([groupId] ASC);
CREATE NONCLUSTERED INDEX [CaseDate] ON [LMF_Cases] ([created] ASC);
CREATE NONCLUSTERED INDEX [CaseMetadataFieldTally] ON [CaseMetaDataField] ([tally] DESC);
CREATE NONCLUSTERED INDEX [CellMetadataField] ON [CellMetaData] ([field] ASC);
CREATE NONCLUSTERED INDEX [CaseID] ON [EventLog] ([CaseID] ASC);
CREATE NONCLUSTERED INDEX [DateTime] ON [EventLog] ([DateTime] ASC);
CREATE NONCLUSTERED INDEX [EventType] ON [EventLog] ([EventType] ASC);
CREATE NONCLUSTERED INDEX [UserName] ON [EventLog] ([UserName] ASC);
CREATE NONCLUSTERED INDEX [caseId+path] ON [Blob] ([caseId] ASC, [path] ASC);
CREATE NONCLUSTERED INDEX [CellId+ChromosomeId] ON [KaryotypeChromosome] ([cellId] ASC,	[karyotypeChromosomeId] ASC);
CREATE NONCLUSTERED INDEX [CellId] ON [ManualCountPoint] ([cellId] ASC);
CREATE NONCLUSTERED INDEX [CellID] ON [NumberingTag] ([cellId] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [FrameId+ComponentId] ON [ImageComponent] ([frameId] ASC,[imageComponentId] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [ComponentId+SnapId] ON [ImageSnap] ([componentId] ASC,[imageSnapId] ASC);
CREATE NONCLUSTERED INDEX [ScratchPadId] ON [ScratchPadItem] ([scratchPadId] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [SlideId+ImageFrameId] ON [ImageFrame] ([slideId] ASC,	[imageFrameId] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [SlideID+CellId] ON [Cell] ([slideId] ASC,	[cellId] ASC);
CREATE NONCLUSTERED INDEX [CaseId] ON [ScratchPad] ([caseId] ASC);
CREATE UNIQUE NONCLUSTERED INDEX [CaseId+SlideId] ON [Slide] ([caseId] ASC,[slideId] ASC);
CREATE NONCLUSTERED INDEX [blobId] ON [KaryotypeChromosome] ([blobId] ASC);
CREATE NONCLUSTERED INDEX [blobId] ON [ImageSnap] ([blobId] ASC);


/* CREATE TABLE RELATIONSHIPS */
ALTER TABLE [Blob]					WITH CHECK ADD CONSTRAINT [FK_Blob_Case] FOREIGN KEY([caseId]) REFERENCES [Case] ([caseId]) ON DELETE CASCADE;
ALTER TABLE [Cell]					WITH CHECK ADD CONSTRAINT [FK_Cell_Slide] FOREIGN KEY([slideId]) REFERENCES [Slide] ([slideId]) ON DELETE CASCADE;
ALTER TABLE [CellMetaData]			WITH CHECK ADD CONSTRAINT [FK_Meta_Cell] FOREIGN KEY([cellId]) REFERENCES [Cell] ([cellId]) ON DELETE CASCADE;
ALTER TABLE [ImageComponent]		WITH CHECK ADD CONSTRAINT [FK_ImageComponent_ImageFrame] FOREIGN KEY([frameId]) REFERENCES [ImageFrame] ([imageFrameId]) ON DELETE CASCADE;
ALTER TABLE [ImageFrame]			WITH CHECK ADD CONSTRAINT [FK_ImageFrame_Slide] FOREIGN KEY([slideId]) REFERENCES [Slide] ([slideId]) ON DELETE CASCADE;
ALTER TABLE [ImageSnap]				WITH CHECK ADD CONSTRAINT [FK_ImageSnap_ImageComponent] FOREIGN KEY([componentId]) REFERENCES [ImageComponent] ([imageComponentId]) ON DELETE CASCADE;
ALTER TABLE [KaryotypeChromosome]	WITH CHECK ADD CONSTRAINT [FK_KaryotypeChromosome_Cell] FOREIGN KEY([cellId]) REFERENCES [Cell] ([cellId]) ON DELETE CASCADE;
ALTER TABLE [LMF_CaseMetaData]		WITH CHECK ADD  CONSTRAINT [FK_LMF_CaseMetaData_Cases] FOREIGN KEY([limsCaseId])REFERENCES [LMF_Cases] ([id]) ON DELETE CASCADE;
ALTER TABLE [LMF_Slides]			WITH CHECK ADD  CONSTRAINT [FK_LMF_Slides_Cases] FOREIGN KEY([limsCaseId])REFERENCES [LMF_Cases] ([id]) ON DELETE CASCADE;
ALTER TABLE [LMF_SlideMetaData]		WITH CHECK ADD  CONSTRAINT [FK_LMF_SlideMetaData_Slides] FOREIGN KEY([limsSlideId]) REFERENCES [LMF_Slides] ([id]) ON DELETE CASCADE;
ALTER TABLE [ManualCountPoint]		WITH CHECK ADD CONSTRAINT [FK_ManualCountPoint_Cell] FOREIGN KEY([cellId]) REFERENCES [Cell] ([cellId]) ON DELETE CASCADE;
ALTER TABLE [NumberingTag]			WITH CHECK ADD CONSTRAINT [FK_NumberingTag_Cell] FOREIGN KEY([cellId]) REFERENCES [Cell] ([cellId]) ON DELETE CASCADE;
ALTER TABLE [ScratchPad]			WITH CHECK ADD CONSTRAINT [FK_ScratchPad_Case] FOREIGN KEY([caseId]) REFERENCES [Case] ([caseId]) ON DELETE CASCADE;
ALTER TABLE [ScratchPadItem]		WITH CHECK ADD CONSTRAINT [FK_ScratchPadItem_ScratchPad] FOREIGN KEY([scratchPadId]) REFERENCES [ScratchPad] ([scratchPadId]) ON DELETE CASCADE;
ALTER TABLE [Slide]					WITH CHECK ADD CONSTRAINT [FK_Slide_Case] FOREIGN KEY([caseId]) REFERENCES [Case] ([caseId]) ON DELETE CASCADE;

INSERT INTO [Version] VALUES(46000)
GO


CREATE VIEW [CaseKaryotypeChromosome] AS
SELECT [Case].caseId, 
	   Slide.slideId, 
	   Cell.cellName, 
	   KaryotypeChromosome.karyotypeChromosomeId, 
	   KaryotypeChromosome.x, 
	   KaryotypeChromosome.y, 
	   KaryotypeChromosome.angleToVertical,
	   KaryotypeChromosome.userRotation, 
	   KaryotypeChromosome.cellId, 
	   KaryotypeChromosome.chromosomeTag, 
	   KaryotypeChromosome.groupIndex, 
	   KaryotypeChromosome.boundsLeft, 
	   KaryotypeChromosome.boundsTop, 
	   KaryotypeChromosome.boundsWidth,
	   KaryotypeChromosome.boundsHeight,
	   KaryotypeChromosome.width, 
	   KaryotypeChromosome.outline,
	   KaryotypeChromosome.length, 
	   KaryotypeChromosome.blobId,
	   KaryotypeChromosome.midPointX, 
	   KaryotypeChromosome.midPointY,
	   KaryotypeChromosome.flipX,
	   KaryotypeChromosome.flipY

FROM   KaryotypeChromosome INNER JOIN
	   Cell ON KaryotypeChromosome.cellId = Cell.cellId INNER JOIN
	   Slide ON Cell.slideId = Slide.slideId INNER JOIN
	   [Case] ON Slide.caseId = [Case].caseId
GO


CREATE VIEW [dbo].[CaseCell] AS
SELECT [Case].caseId, 
	   Cell.cellId, Slide.slideId, 
	   Cell.cellName, 
	   Cell.imageGroup, 
	   Cell.counted, 
	   Cell.analysed, 
	   Cell.karyotyped, 
	   Cell.color, 
	   Cell.threshold
FROM   [Case] INNER JOIN
       Slide ON [Case].caseId = Slide.caseId INNER JOIN
       Cell ON Slide.slideId = Cell.slideId
GO

CREATE VIEW [dbo].[CaseCellMetaData] AS
SELECT [CellMetaData].cellMetaDataId, 
       [Case].caseId, 
       [Cell].cellId, 
       [CellMetaData].[field], 
       [CellMetaData].[value], 
       [CellMetaData].[confidential]
FROM   [Case] INNER JOIN
       Slide ON [Case].caseId = Slide.caseId INNER JOIN
       Cell ON Slide.slideId = Cell.slideId INNER JOIN
       CellMetaData ON Cell.cellId = CellMetaData.cellId
GO

CREATE VIEW [dbo].[CaseImageFrames] AS
SELECT [Case].caseId, 
       ImageFrame.imageType, 
       ImageFrame.groupId, 
       ImageFrame.imageFrameId
FROM   [Case] INNER JOIN
       Slide ON [Case].caseId = Slide.caseId INNER JOIN
       ImageFrame ON Slide.slideId = ImageFrame.slideId
GO
