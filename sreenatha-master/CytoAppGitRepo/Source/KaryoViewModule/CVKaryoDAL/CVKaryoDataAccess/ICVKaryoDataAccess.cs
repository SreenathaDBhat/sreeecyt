﻿using CVKaryoDAL.Models;
using CytoApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVKaryoDAL
{
    public interface ICVKaryoDataAccess
    {
        bool Initialize(DataSource dataSource);
        bool CanAnalyseSlide(Slide slide);
        IEnumerable<KaryoCell> GetCells(Slide slide);
        byte[] GetMetaphaseImageData(KaryoCell cell);
        byte[] GetKaryotypeImageData(KaryoCell cell);
        byte[] GetProbeImageData(KaryoCell cell);
        void SaveKaryotypeImageData(KaryoCell cell, string imageId, byte[] data);
    }
}
