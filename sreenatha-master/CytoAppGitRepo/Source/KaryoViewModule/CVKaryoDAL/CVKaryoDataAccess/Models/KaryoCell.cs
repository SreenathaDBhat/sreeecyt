﻿using CytoApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CVKaryoDAL.Models
{
    [DataContract]
    public class KaryoCell
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public Slide Slide { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool HasRaw { get; set; }
        [DataMember]
        public bool HasMetaphase { get; set; }
        [DataMember]
        public bool HasKaryotype { get; set; }
        [DataMember]
        public bool HasProbe { get; set; }

    }
}
