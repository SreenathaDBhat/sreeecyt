﻿using CVKaryoDAL;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CVKaryoDAL.Models;
using CytoApps.Models;
using System.IO;
using System;

namespace CVKaryoDataAccessLocal
{
    [DalTypeId(Name = "Local Casebase")]
    public class CVKaryoDataAccessLocal : ICVKaryoDataAccess
    {
        private string _casebaseFolder;

        public CVKaryoDataAccessLocal()
        {
        }

        public CVKaryoDataAccessLocal(string casebaseFolder)
        {
            _casebaseFolder = casebaseFolder;
        }

        public bool Initialize(DataSource dataSource)
        {
            _casebaseFolder = dataSource.ConnectionParameter[1].Value;
            return true;
        }

        private string GetSlidePath(Slide slide)
        {
            return Path.Combine(_casebaseFolder, slide.CaseId, slide.Id);
        }

        private string GetCellRealName(string cellfolder)
        {
            var cellDir = new DirectoryInfo(cellfolder);
            string cellname = cellDir.Name;
            FileInfo nameFile = new FileInfo(Path.Combine(cellfolder, ".name"));
            if (nameFile.Exists)
            {
                using (var reader = new StreamReader(nameFile.FullName, Encoding.Default))
                    cellname = reader.ReadToEnd();
            }
            return cellname;
        }

        public bool CanAnalyseSlide(Slide slide)
        {
            var slideFolder = GetSlidePath(slide);
            return Directory.Exists(slideFolder) && Directory.EnumerateFileSystemEntries(slideFolder, "Cell*").Count() > 0;
        }

        public IEnumerable<KaryoCell> GetCells(Slide slide)
        {
            var slideDir = new DirectoryInfo(GetSlidePath(slide));

            if (!slideDir.Exists)
                return new KaryoCell[0];

            var cellFolders = slideDir.EnumerateDirectories("Cell*").ToArray();

            var cells = (from cellfolder in cellFolders
                         select GetCell(slide, cellfolder)).ToArray();
            return cells;
        }

        public byte[] GetMetaphaseImageData(KaryoCell cell)
        {
            return GetImageData(cell, "*met.aipng");
        }

        public byte[] GetKaryotypeImageData(KaryoCell cell)
        {
            return GetImageData(cell, "*kar.aipng");
        }

        public byte[] GetProbeImageData(KaryoCell cell)
        {
            return GetImageData(cell, "*probe.aipng");
        }


        private KaryoCell GetCell(Slide slide, DirectoryInfo cellFolder)
        {
            if (cellFolder.Exists)
            {
                var name = GetCellRealName(cellFolder.FullName);
                var files = Directory.EnumerateFiles(cellFolder.FullName).ToList();
                var hasMet = files.Where(f => f.ToLower().Contains(".met") || f.ToLower().Contains(".fmet")).Count() > 0;
                var hasKar = files.Where(f => f.ToLower().Contains(".kar") || f.ToLower().Contains(".fkar")).Count() > 0;
                var hasRaw = files.Where(f => f.ToLower().Contains(".raw")).Count() > 0;
                var hasProbe = files.Where(f => f.ToLower().Contains(".probe")).Count() > 0;

                return new KaryoCell { Name = name, Id = cellFolder.Name, HasKaryotype = hasKar, HasMetaphase = hasMet, HasRaw = hasRaw, HasProbe = hasProbe, Slide = slide };
            }
            return null;
        }

        private byte[] GetImageData(KaryoCell cell, string imagePattern)
        {
            var cellFolder = new DirectoryInfo(Path.Combine(GetSlidePath(cell.Slide), cell.Id));
            if (cellFolder.Exists)
            {
                var file = Directory.EnumerateFiles(cellFolder.FullName, imagePattern).Select(a => new FileInfo(a)).FirstOrDefault();
                //                return (file != null ? ImageLoader.GetImageBytes(file.FullName, 200, 200) : null);
                return (file != null ? ImageLoader.GetImageBytes(file.FullName, 50) : null);
            }
            else
                return null;
        }

        private void PutImageData(KaryoCell cell, string name, byte[] data)
        {
            var cellFolder = new DirectoryInfo(Path.Combine(GetSlidePath(cell.Slide), cell.Id));
            var filePath = Path.Combine(cellFolder.FullName, name + ".kar.aipng");
            if (cellFolder.Exists)
            {
                File.WriteAllBytes(filePath, data);
            }
        }

        public void SaveKaryotypeImageData(KaryoCell cell, string imageId, byte[] data)
        {
            PutImageData(cell, imageId, data);
        }


    }
}
