﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace CVKaryoDataAccessLocal
{
    public class ImageLoader
    {
        public static byte[] GetImageBytes(string path, int quality)
        {
            if (!File.Exists(path))
                return null;

            var image = LoadImage(path);
            return GetBitmapJpegBytes(image, quality);
        }

        public static byte[] GetImageBytes(string path, int maxwidth, int maxheight)
        {
            if (!File.Exists(path))
                return null;

            return ResizeImage(path, maxwidth, maxheight);
        }

        private static byte[] ResizeImage(string path, int NewWidth, int MaxHeight, bool flip = false)
        {
            var fullsizeImage = System.Drawing.Image.FromFile(path);

            if (flip)
            {
                fullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
                fullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            }

            if (fullsizeImage.Width <= NewWidth)
            {
                NewWidth = fullsizeImage.Width;
            }

            int NewHeight = fullsizeImage.Height * NewWidth / fullsizeImage.Width;
            if (NewHeight > MaxHeight)
            {
                NewWidth = fullsizeImage.Width * MaxHeight / fullsizeImage.Height;
                NewHeight = MaxHeight;
            }

            System.Drawing.Image NewImage = fullsizeImage.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);
            fullsizeImage.Dispose();

            using (MemoryStream ms = new MemoryStream())
            {
                NewImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return ms.GetBuffer();
            }
        }

        public static BitmapSource LoadImage(string path, int maxwidth = 0)
        {
            if (File.Exists(path))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                if (maxwidth > 0)
                    bmp.DecodePixelWidth = maxwidth;
                bmp.CacheOption = BitmapCacheOption.OnLoad;
                bmp.UriSource = new Uri(path);
                bmp.EndInit();
                bmp.Freeze();
                return bmp;
            }
            else
            {
                return null;
            }
        }

        public static byte[] GetBitmapJpegBytes(BitmapSource bitmap, int quality)
        {
            using (var s = new MemoryStream())
            {
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmap));
                encoder.QualityLevel = quality;
                encoder.Save(s);
                s.Flush();
                return s.ToArray();
            }
        }
    }
}
