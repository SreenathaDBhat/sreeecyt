﻿using CVKaryoDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CVKaryoDAL.Models;
using CytoApps.Models;
using System.Net.Http;
using System.Net.Http.Headers;

namespace CVKaryoDataAccessRemote
{
    public class CVKaryoDataAccessRemote : ICVKaryoDataAccess
    {
        private string _baseUrl;

        public CVKaryoDataAccessRemote(string baseUrl)
        {
            _baseUrl = baseUrl;
        }

        public bool CanAnalyseSlide(Slide slide)
        {
            using (var client = ConfigureClient())
            {
                string restUrl = string.Format("api/Karyo/Cases/{0}/Slides/{1}/CanAnalyse",
                    slide.CaseId, slide.Id);

                HttpResponseMessage response = client.GetAsync(restUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var canAnalyse = response.Content.ReadAsAsync<bool>().Result;
                    return canAnalyse;
                }
            }
            return false;
        }

        public IEnumerable<KaryoCell> GetCells(Slide slide)
        {
            using (var client = ConfigureClient())
            {
                string restUrl = string.Format("api/Karyo/Cases/{0}/Slides/{1}/Cells",
                    slide.CaseId, slide.Id);

                HttpResponseMessage response = client.GetAsync(restUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var cells = response.Content.ReadAsAsync<IEnumerable<KaryoCell>>().Result;
                    return cells;
                }
            }
            return null;
        }

        public byte[] GetKaryotypeImageData(KaryoCell cell)
        {
            using (var client = ConfigureClient())
            {
                string restUrl = string.Format("api/Karyo/Cases/{0}/Slides/{1}/Cells/{2}/Karyotype",
                    cell.Slide.CaseId, cell.Slide.Id, cell.Id);

                HttpResponseMessage response = client.GetAsync(restUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var imageData = response.Content.ReadAsAsync<byte[]>().Result;
                    return imageData;
                }
            }
            return null;
        }

        public byte[] GetProbeImageData(KaryoCell cell)
        {
            throw new NotImplementedException();        }


        public byte[] GetMetaphaseImageData(KaryoCell cell)
        {
            using (var client = ConfigureClient())
            {
                string restUrl = string.Format("api/Karyo/Cases/{0}/Slides/{1}/Cells/{2}/Metaphase",
                    cell.Slide.CaseId, cell.Slide.Id, cell.Id);

                HttpResponseMessage response = client.GetAsync(restUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var imageData = response.Content.ReadAsAsync<byte[]>().Result;
                    return imageData;
                }
            }
            return null;

        }

        public void SaveKaryotypeImageData(KaryoCell cell, string imageId, byte[] data)
        {
            throw new NotImplementedException();
        }

        private HttpClient ConfigureClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(_baseUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
            //                             BearerToken.AccessToken);

            return client;
        }

        public bool Initialize(DataSource dataSource)
        {
            throw new NotImplementedException();
        }
    }
}
