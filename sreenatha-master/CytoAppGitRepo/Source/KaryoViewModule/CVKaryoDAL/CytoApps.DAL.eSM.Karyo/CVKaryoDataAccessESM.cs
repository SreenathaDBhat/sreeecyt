﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CytoApps.Models;
using Core.DataServer.SoapClient;
using CVKaryoDAL;
using CVKaryoDAL.Models;

namespace CytoApps.DAL.eSM
{
    /// <summary>
    /// Provides the ICVKaryoDataAccess implementation when the remote casebase is accessed via the CytoApps
    /// eSlideManager dataserver extension
    /// </summary>
    [DalTypeId(Name = "eSM DataServer")]
    public class CVKaryoDataAccessESM : ICVKaryoDataAccess
    {
        private string _baseUrl;
        private string _token;

        public CVKaryoDataAccessESM()
        {
        }

        public CVKaryoDataAccessESM(string baseUrl, string token)
        {
            _baseUrl = baseUrl;
            _token = token;
        }

        public bool Initialize(DataSource dataSource)
        {
            _baseUrl = dataSource.ConnectionParameter[0].Value;
            _token = dataSource.SessionObject as string;
            return true;
        }


        public bool CanAnalyseSlide(Slide slide)
        {
            var eSmCytoAppsKaryo = GetProxy();

            return eSmCytoAppsKaryo.CanAnalyse(slide.CaseId, slide.Id);
        }

        public IEnumerable<KaryoCell> GetCells(Slide slide)
        {
            var eSmCytoAppsKaryo = GetProxy();

            return eSmCytoAppsKaryo.GetCellList(slide.CaseId, slide.Id);
        }

        public byte[] GetKaryotypeImageData(KaryoCell cell)
        {
            var eSmCytoAppsKaryo = GetProxy();

            return eSmCytoAppsKaryo.GetKaryotypeImageData(cell.Slide.CaseId, cell.Slide.Id, cell.Id);
        }

        public byte[] GetMetaphaseImageData(KaryoCell cell)
        {
            var eSmCytoAppsKaryo = GetProxy();

            return eSmCytoAppsKaryo.GetMetaphaseImageData(cell.Slide.CaseId, cell.Slide.Id, cell.Id);
        }

        public byte[] GetProbeImageData(KaryoCell cell)
        {
            var eSmCytoAppsKaryo = GetProxy();

            return eSmCytoAppsKaryo.GetProbeImageData(cell.Slide.CaseId, cell.Slide.Id, cell.Id);
        }

        public void SaveKaryotypeImageData(KaryoCell cell, string imageId, byte[] data)
        {
            var eSmCytoAppsKaryo = GetProxy();

            eSmCytoAppsKaryo.PutKaryotypeImageData(cell.Slide.CaseId, cell.Slide.Id, cell.Id, imageId, data);
        }

        private eSmCytoAppsKaryoProxy GetProxy()
        {
            var dsRouter = new DataServerProxyRouterHttp();
            dsRouter.URIHost = _baseUrl;
            dsRouter.URIPort = 80;
            dsRouter.URIPath = "dataserver";

            return new eSmCytoAppsKaryoProxy(dsRouter) { Token = _token };
        }

        public static string GetToken(string baseUrl)
        {
            var dsRouter = new DataServerProxyRouterHttp();
            dsRouter.URIHost = baseUrl;
            dsRouter.URIPort = 80;
            dsRouter.URIPath = "dataserver";

            var coreProxy = new CoreProxy(dsRouter);
            coreProxy.Logon("administrator", "scanscope");

            return coreProxy.Token;
        }
    }
}
