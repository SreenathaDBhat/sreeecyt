﻿using Core.DataServer.SoapClient;
using CVKaryoDAL.Models;
using CytoApps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CytoApps.DAL.eSM
{
    public class eSmCytoAppsKaryoProxy : DataServerProxy
    {
        protected const string APERIO_NAMESPACE = "http://www.aperio.com/webservices/";
        protected const string CytoAppsProxyEndPoint = "Aperio/CytoAppsServices";

        private XNamespace _nsAperio = APERIO_NAMESPACE;

        private DsResponse _lastResponse;

        public eSmCytoAppsKaryoProxy(IDataServerProxyRouter dsProxyRouter) : base(dsProxyRouter)
        {

        }

        public byte[] GetMetaphaseImageData(string caseId, string slideId, string cellId)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", caseId);
            AddMethodParameter("SlideId", slideId);
            AddMethodParameter("CellId", cellId);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsKaryoProxyMethodNames.GetMetaphaseImage);

            int retCode = response.GetResult(eSmCytoAppsKaryoProxyMethodNames.GetMetaphaseImage).Code;

            byte[] imageDataRet = null;
            if (retCode == 0)
            {
                var imageDataBase64 = response.GetElement("ImageData");
                if (imageDataBase64 != null)
                {
                    imageDataRet = Convert.FromBase64String(imageDataBase64.Value);
                }
            }

            _lastResponse = response;
            return imageDataRet;
        }

        public byte[] GetProbeImageData(string caseId, string slideId, string cellId)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", caseId);
            AddMethodParameter("SlideId", slideId);
            AddMethodParameter("CellId", cellId);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsKaryoProxyMethodNames.GetProbeImage);

            int retCode = response.GetResult(eSmCytoAppsKaryoProxyMethodNames.GetProbeImage).Code;

            byte[] imageDataRet = null;
            if (retCode == 0)
            {
                var imageDataBase64 = response.GetElement("ImageData");
                if (imageDataBase64 != null)
                {
                    imageDataRet = Convert.FromBase64String(imageDataBase64.Value);
                }
            }

            _lastResponse = response;
            return imageDataRet;
        }


        public byte[] GetKaryotypeImageData(string caseId, string slideId, string cellId)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", caseId);
            AddMethodParameter("SlideId", slideId);
            AddMethodParameter("CellId", cellId);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsKaryoProxyMethodNames.GetKaryotypeImage);

            int retCode = response.GetResult(eSmCytoAppsKaryoProxyMethodNames.GetKaryotypeImage).Code;

            byte[] imageDataRet = null;
            if (retCode == 0)
            {
                var imageDataBase64 = response.GetElement("ImageData");
                if (imageDataBase64 != null)
                {
                    imageDataRet = Convert.FromBase64String(imageDataBase64.Value);
                }
            }

            _lastResponse = response;
            return imageDataRet;
        }

        public bool PutKaryotypeImageData(string caseId, string slideId, string cellId, string imageId, byte[] imageData)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", caseId);
            AddMethodParameter("SlideId", slideId);
            AddMethodParameter("CellId", cellId);
            AddMethodParameter("ImageId", imageId);
            AddMethodParameter("ImageData", Convert.ToBase64String(imageData));

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsKaryoProxyMethodNames.PutKaryotypeImage);

            int retCode = response.GetResult(eSmCytoAppsKaryoProxyMethodNames.PutKaryotypeImage).Code;

            bool ret = false;
            if (retCode == 0)
            {
                var success = response.GetElement("Success");
                if (success != null)
                {
                    ret = (success.Value.Equals("true", StringComparison.InvariantCultureIgnoreCase));
                }
            }

            _lastResponse = response;
            return ret;
        }

        public bool CanAnalyse(string caseId, string slideId)
        {
            ClearMethodParameters();
            AddMethodParameter("CaseId", caseId);
            AddMethodParameter("SlideId", slideId);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsKaryoProxyMethodNames.CanAnalyse);

            int retCode = response.GetResult(eSmCytoAppsKaryoProxyMethodNames.CanAnalyse).Code;

            bool ret = false;
            if (retCode == 0)
            {
                var success = response.GetElement("Success");
                if (success != null)
                {
                    ret = (success.Value.Equals("true", StringComparison.InvariantCultureIgnoreCase));
                }
            }

            _lastResponse = response;
            return ret;
        }

        public IEnumerable<KaryoCell> GetCellList(string caseId, string slideId)
        {
            ClearMethodParameters();
            ClearMethodParameters();
            AddMethodParameter("CaseId", caseId);
            AddMethodParameter("SlideId", slideId);

            var response = DoCall(CytoAppsProxyEndPoint, eSmCytoAppsKaryoProxyMethodNames.GetCellList);

            int retCode = response.GetResult(eSmCytoAppsKaryoProxyMethodNames.GetCellList).Code;

            List<KaryoCell> cellListRet = null;
            if (retCode == 0)
            {
                var cellList = response.GetElement("CellList");
                if (cellList != null)
                {
                    // need the aperio namespace
                    cellListRet = (from c in cellList.Descendants(_nsAperio + "Cell")
                                   select new KaryoCell()
                                   {
                                       Id = c.Descendants(_nsAperio + "Id").FirstOrDefault().Value,
                                       Slide = new Slide(caseId, "", slideId), 
                                       Name = c.Descendants(_nsAperio+"Name").FirstOrDefault().Value,
                                       HasKaryotype = bool.Parse(c.Descendants(_nsAperio + "HasKaryotype").FirstOrDefault().Value),
                                       HasMetaphase = bool.Parse(c.Descendants(_nsAperio + "HasMetaphase").FirstOrDefault().Value),
                                       HasProbe = bool.Parse(c.Descendants(_nsAperio + "HasProbe").FirstOrDefault().Value),
                                       HasRaw = bool.Parse(c.Descendants(_nsAperio + "HasRaw").FirstOrDefault().Value)
                                   }).ToList();
                }
            }

            _lastResponse = response;
            return cellListRet;
        }

        /// <summary>
        /// for debug really
        /// </summary>
        public string LastResponseString
        {
            get
            {
                return _lastResponse != null ? _lastResponse.Response : "No last response";
            }
        }
    }
}
