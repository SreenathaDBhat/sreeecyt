﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CytoApps.DAL.eSM
{
    public static class eSmCytoAppsKaryoProxyMethodNames
    {
        public static string CanAnalyse = "KaryoCanAnalyse";
        public static string GetCellList = "KaryoGetCellList";
        public static string GetMetaphaseImage = "KaryoGetMetaphaseImage";
        public static string GetKaryotypeImage = "KaryoKaryotypeImage";
        public static string GetProbeImage = "KaryoGetProbeImage";
        public static string PutKaryotypeImage = "KaryoPutKaryotypeImage";
        
    }
}
