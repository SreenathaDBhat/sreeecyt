﻿using KaryoViewModule.ViewModels;
using Prism.Events;

namespace KaryoViewModule.Events
{
    public class OpenKaryoSlideEvent : PubSubEvent<object> { }

    public class CloseKaryoSlideEvent : PubSubEvent<object> { }

    public class KaryotypeEvent : PubSubEvent<SlideViewModel> { }

}
