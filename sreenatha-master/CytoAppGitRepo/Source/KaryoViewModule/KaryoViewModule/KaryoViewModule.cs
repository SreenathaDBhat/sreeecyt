﻿using KaryoViewModule.Presenters;
using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;
using System.ComponentModel.Composition;

namespace KaryoViewModule
{
    [ModuleExport(typeof(KaryoViewModule))]
    public class KaryoViewModule : IModule
    {
#pragma warning disable 0649, 0169
        [Import]
        private IRegionManager _regionManager;

        [Import]
        private KaryoViewPresenter _karyoPresenter;

        [Import]
        private KaryoSlidePresenter _slidePresenter;
#pragma warning restore 0649, 0169

        public void Initialize()
        {
            
        }
    }
}
