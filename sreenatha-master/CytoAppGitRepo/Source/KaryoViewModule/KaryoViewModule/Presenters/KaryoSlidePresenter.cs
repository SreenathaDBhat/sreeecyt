﻿using CytoApps.Infrastructure.UI;
using KaryoViewModule.Events;
using KaryoViewModule.Models;
using KaryoViewModule.Services;
using KaryoViewModule.ViewModels;
using KaryoViewModule.Views;
using Prism.Events;
using Prism.Regions;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System;

/// <summary>
/// This presenter is responsible for navigating and display Cell images from a CytoVision Karyotype Cell
/// This project is prototype for demonstrations purposes
/// </summary>
namespace KaryoViewModule.Presenters
{
    [Export]
    class KaryoSlidePresenter
    {
        private IEventAggregator _eventAggregator;
        private IDataService _dataService;
        private IRegionManager _regionManager;
        private KaryoSlideView _karyoSlideView;

        [ImportingConstructor]
        public KaryoSlidePresenter(IEventAggregator eventAggregator, IRegionManager regionManager, IDataService dataService)
        {
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _dataService = dataService;

            SubscribeEvents();
        }

        private void SubscribeEvents()
        {
            _eventAggregator.GetEvent<OpenKaryoSlideEvent>().Subscribe(OnOpenSlide);
            _eventAggregator.GetEvent<CloseKaryoSlideEvent>().Subscribe(CloseSlide);
            _eventAggregator.GetEvent<KaryotypeEvent>().Subscribe(OnKaryotype);
        }

        private void OnKaryotype(SlideViewModel slideVM)
        {
            string imagefile = @"C:\karyo.png";
            if (slideVM.SelectedCell != null && File.Exists(imagefile))
            {
                var imageData = File.ReadAllBytes(imagefile);
                using (var ms = new MemoryStream(imageData))
                    slideVM.SelectedCell.SideImage = StandardImageDataRenderer.BitmapImageFromStream(ms);

                _dataService.CVKaryoDAL.SaveKaryotypeImageData(slideVM.SelectedCell.Cell, "image0", imageData);
            }
        }

        private void OnOpenSlide(object slideObj)
        {
            var slideVM = slideObj as SlideViewModel;
            if (null != slideVM)
            {
                // NOTE: the view code behind must have Export attribute 
                if (!RegionManagerExtension.CheckViewExistsInRegion(RegionNames.MainRegion, typeof(KaryoViewWorkspaceView)))
                    _regionManager.RegisterViewWithRegion(CytoApps.Infrastructure.UI.RegionNames.MainRegion, typeof(KaryoViewWorkspaceView));
                else
                    _regionManager.RequestNavigate(CytoApps.Infrastructure.UI.RegionNames.MainRegion, "KaryoViewWorkspaceView.xaml");

                RegionManagerExtension.ClearNavigatedViewsFromRegion(KaryoRegionNames.KaryoSlideViewRegion);
                RegionManagerExtension.ActivateViewInRegion(RegionNames.MainRegion, typeof(KaryoViewWorkspaceView));

                // v basic, using DAL, load the ViewModel for display
                Task.Factory.StartNew(() =>
                {
                    // debug, clear any cached cell images
                    //foreach (var cell in slideVM.CellVMs)
                    //{
                    //    cell.MetaphaseImage = null;
                    //    cell.KaryotypeImage = null;
                    //}

                    Parallel.ForEach(slideVM.CellVMs, cell =>
                    {
                        if (cell.Cell.HasMetaphase && cell.MainImage == null)
                        {
                            var imageData = _dataService.CVKaryoDAL.GetMetaphaseImageData(cell.Cell);
                            if (imageData != null)
                            {
                                using (var ms = new MemoryStream(imageData))
                                    cell.MainImage = StandardImageDataRenderer.BitmapImageFromStream(ms);
                            }
                        }
                        else if (cell.Cell.HasProbe && cell.MainImage == null)
                        {
                            var imageData = _dataService.CVKaryoDAL.GetProbeImageData(cell.Cell);
                            if (imageData != null)
                            {
                                using (var ms = new MemoryStream(imageData))
                                    cell.MainImage = StandardImageDataRenderer.BitmapImageFromStream(ms);
                            }
                        }

                        if (cell.Cell.HasKaryotype && cell.SideImage == null)
                        {
                            var imageData = _dataService.CVKaryoDAL.GetKaryotypeImageData(cell.Cell);
                            if (imageData != null)
                            {
                                using (var ms = new MemoryStream(imageData))
                                    cell.SideImage = StandardImageDataRenderer.BitmapImageFromStream(ms);
                            }
                        }
                   });
                }, CancellationToken.None, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default);

                // Now we have the ViewModel populates, create appropriate views for display and navigation
                _karyoSlideView = new KaryoSlideView() { DataContext = slideVM };
                _regionManager.RegisterViewWithRegion(KaryoRegionNames.KaryoSlideViewRegion, () => _karyoSlideView);
            }
        }

        private void CloseSlide(object obj)
        {
            RegionManagerExtension.DeactivateViewFromRegion(RegionNames.MainRegion, typeof(KaryoViewWorkspaceView));
            _regionManager.RequestNavigate(RegionNames.MainRegion, "CaseBrowserWorkspaceView");
        }
    }
}
