﻿using CytoApps.Infrastructure.Helpers;
using CytoApps.Infrastructure.UI;
using CytoApps.Infrastructure.UI.Events;
using CytoApps.Models;
using KaryoViewModule.Services;
using KaryoViewModule.ViewModels;
using KaryoViewModule.Views;
using Prism.Events;
using Prism.Regions;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace KaryoViewModule.Presenters
{
    /// <summary>
    /// This preseneter is responsible for generating UI to represent each Karyotype slide in a Case.
    /// The UI shows appropriate Text/Graphics and a means of opening the slide in the Karyotype Analysis plugin
    /// Opening the slide via the injected Case Navigator UI will show the analysis plugin screens
    /// </summary>
    [Export]
    public class KaryoViewPresenter
    {
        private IEventAggregator _eventAggregator;
        private IRegionManager _regionManager;
        private IDataService _dataService;

        [ImportingConstructor]
        public KaryoViewPresenter(IEventAggregator eventAggregator, IRegionManager regionManager, IDataService dataService)
        {
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _dataService = dataService;

            SubscribeEvents();
        }

        private ObservableCollection<KaryoNavigationViewModel> _karyoViewNavigationVMs;
        public ObservableCollection<KaryoNavigationViewModel> KaryoViewNavigationVMs
        {
            get { return _karyoViewNavigationVMs; }
            set { _karyoViewNavigationVMs = value; }
        }

        private void SubscribeEvents()
        {
            _eventAggregator.GetEvent<SelectedCaseSlidesEvent>().Subscribe(OnSlideLoad);
        }

        private void OnSlideLoad(CaseDataProxy plugginInfo)
        {
            try
            {
                if (plugginInfo != null)
                {
                    _dataService.CreateDatasource(plugginInfo.DataSource);

                    _karyoViewNavigationVMs = new ObservableCollection<KaryoNavigationViewModel>();
                    var slide = plugginInfo.Slide;

                    // Can we handle this known slide type
                    if ((slide.KnownSlideTypes & (UInt32)KnownSlides.KaryotypeSlide) != 0)
                    {
                        // Create the Navigation UI
                        var karyoVM = new KaryoNavigationViewModel(_eventAggregator);
                        karyoVM.SlideVM = new SlideViewModel(slide, _eventAggregator);
                        _karyoViewNavigationVMs.Add(karyoVM);

                        // and add it immediately into casebrowser, user can now press Open to view slide
                        PopulateNavigationRegions(_karyoViewNavigationVMs, plugginInfo.PluginRegionName);

                        // Then go off and get some summary information, asynch so as not to block
                        var task = Task.Factory.StartNew(() =>
                        {
                            karyoVM.SlideVM.CellVMs = (from c in _dataService.CVKaryoDAL.GetCells(slide) select new CellViewModel(c)).ToObservableCollection();

                            karyoVM.RepresentativeCell = karyoVM.SlideVM.CellVMs.FirstOrDefault(); // need to pick a good one
                            if (karyoVM.RepresentativeCell != null)
                            {
                                if (karyoVM.RepresentativeCell.Cell.HasProbe)
                                {
                                    var imageData = _dataService.CVKaryoDAL.GetProbeImageData(karyoVM.RepresentativeCell.Cell);
                                    if (imageData != null)
                                    {
                                        using (var ms = new MemoryStream(imageData))
                                        {
                                            karyoVM.RepresentativeCell.MainImage = StandardImageDataRenderer.BitmapImageFromStream(ms);
                                        }
                                    }
                                }
                                else
                                {
                                    if (karyoVM.RepresentativeCell.Cell.HasMetaphase)
                                    {
                                        var imageData = _dataService.CVKaryoDAL.GetMetaphaseImageData(karyoVM.RepresentativeCell.Cell);
                                        if (imageData != null)
                                        {
                                            using (var ms = new MemoryStream(imageData))
                                            {
                                                karyoVM.RepresentativeCell.MainImage = StandardImageDataRenderer.BitmapImageFromStream(ms);
                                            }
                                        }
                                    }

                                    if (karyoVM.RepresentativeCell.Cell.HasKaryotype)
                                    {
                                        var imageData = _dataService.CVKaryoDAL.GetKaryotypeImageData(karyoVM.RepresentativeCell.Cell);
                                        if (imageData != null)
                                        {
                                            using (var ms = new MemoryStream(imageData))
                                            {
                                                karyoVM.RepresentativeCell.SideImage = StandardImageDataRenderer.BitmapImageFromStream(ms);
                                            }
                                        }
                                    }
                                }

                            }
                        });
                    }
                }
            }
            // HACK - temp to handle that I haven't written a remote DAL at the momemt
            // issue is that if this throws an exception the Navi UI is broken for other modules
            // so probably have to handle exceptions in here
            catch (Exception ex)
            {
                LogManager.Debug("Karyo: probably haven't written the DAL yet : " + ex.Message);
                return;
            }
        }

        /// <summary>
        /// Inject Karyo naviagtion view to plugin region of respective slide
        /// </summary>
        /// <param name="karyoVMs">Karyo Navigation view</param>
        /// <param name="pluginRegionName">plugin region name</param>
        private void PopulateNavigationRegions(ObservableCollection<KaryoNavigationViewModel> karyoVMs, string pluginRegionName)
        {
            if (karyoVMs != null && karyoVMs.Count > 0)
            {
                foreach (var navigationVM in karyoVMs)
                {
                    var view = new KaryoNavigationView();
                    _regionManager.AddToRegion(pluginRegionName, view);
                    view.DataContext = navigationVM;
                }
            }
        }
    }

    // Move to renders project
    public class StandardImageDataRenderer
    {
        public static BitmapImage BitmapImageFromStream(Stream stream, int decodeWidth = 0)
        {
            if (stream == null)
                return null;

            BitmapImage img = new BitmapImage();
            img.BeginInit();
            img.CacheOption = BitmapCacheOption.OnLoad;

            if (decodeWidth > 0)
                img.DecodePixelWidth = decodeWidth;

            img.StreamSource = stream;
            img.EndInit();
            img.Freeze();

            stream.Close();
            return img;
        }
    }
}
