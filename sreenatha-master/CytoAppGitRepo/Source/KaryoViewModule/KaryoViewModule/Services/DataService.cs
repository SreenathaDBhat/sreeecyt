﻿using CVKaryoDAL;
using CytoApps.DAL.eSM;
using CytoApps.Infrastructure;
using CytoApps.Infrastructure.CommonEnums;
using CytoApps.Models;
using System;
using System.ComponentModel.Composition;

namespace KaryoViewModule.Services
{
    [Export(typeof(IDataService))]
    public class DataService : IDataService
    {
        private ICVKaryoDataAccess _karyoDAL;

        // all DAL assemblies found that derive from ICVKaryoDataAccess
        private Type[] availKaryoDalTypes = null;

        /// <summary>
        /// Create the appropriate instance from the set of available DAL's that implement ICVKaryoDataAccess that match the DataSource
        /// </summary>
        /// <param name="dataSource"></param>
        /// <returns></returns>
        public void CreateDatasource(DataSource dataSource)
        {
            // import karyo DALS using reflection not MEF (ExportFactory) due to lifetime concerns
            if (availKaryoDalTypes == null)
                availKaryoDalTypes = DalReflectionFinder.GetTypesForInterface<ICVKaryoDataAccess>(@".");

            // Create a new instance every time, thread safety concerns
            _karyoDAL = DalReflectionFinder.GetMatchingInstanceOf<ICVKaryoDataAccess>(availKaryoDalTypes, dataSource);

            // This will pass the connection parameters required (persisted by application per data source pointed at)
            _karyoDAL.Initialize(dataSource);
        }

        public ICVKaryoDataAccess CVKaryoDAL
        {
            get { return _karyoDAL; }
        }
    }
}
