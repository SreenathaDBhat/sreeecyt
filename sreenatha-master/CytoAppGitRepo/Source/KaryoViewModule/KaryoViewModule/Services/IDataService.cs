﻿using CVKaryoDAL;
using CytoApps.Models;

namespace KaryoViewModule.Services
{
    public interface IDataService
    {
        void CreateDatasource(DataSource dataSource);
        ICVKaryoDataAccess CVKaryoDAL { get; }
    }
}
