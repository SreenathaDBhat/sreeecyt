﻿using CVKaryoDAL.Models;
using Prism.Mvvm;
using System.Windows.Media.Imaging;

namespace KaryoViewModule.ViewModels
{
    public class CellViewModel : BindableBase
    {
        #region Properties
        private KaryoCell _cell;
        public CellViewModel(KaryoCell cell)
        {
            _cell = cell;
        }

        // bit hacky, but for now just expose the DAL model
        public KaryoCell Cell
        {
            get { return _cell; }
        }

        private BitmapSource _mainImage;
        public BitmapSource MainImage
        {
            get { return _mainImage; }
            set { _mainImage = value; base.OnPropertyChanged(); }
        }

        private BitmapSource _sideImage;
        public BitmapSource SideImage
        {
            get { return _sideImage; }
            set { _sideImage = value; base.OnPropertyChanged(); }
        }
        #endregion
    }
}
