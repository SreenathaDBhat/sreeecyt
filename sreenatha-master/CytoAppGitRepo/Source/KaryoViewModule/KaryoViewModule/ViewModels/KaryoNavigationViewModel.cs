﻿using CytoApps.Infrastructure.UI.Utilities;
using CytoApps.Models;
using KaryoViewModule.Events;
using Prism.Commands;
using Prism.Events;
using System.Windows.Input;

namespace KaryoViewModule.ViewModels
{
    public class KaryoNavigationViewModel : DesignTimeBindableBase
    {
        #region Private members
        private IEventAggregator _eventAggregator;
        #endregion

        #region Constructor
        public KaryoNavigationViewModel()
        {
            if (base.IsInDesignMode)
                DesignTimeData();
        }
        
        public KaryoNavigationViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }
        #endregion

        #region Properties
        private SlideViewModel _slideViewModel;
        public SlideViewModel SlideVM
        {
            get { return _slideViewModel; }
            set
            {
                _slideViewModel = value;
                OnPropertyChanged();
            }
        }

        private CellViewModel _representativeCell;
        public CellViewModel RepresentativeCell
        {
            get { return _representativeCell; }
            set { _representativeCell = value; OnPropertyChanged(); }
        }

        private DelegateCommand _openSlideCommand;
        public ICommand OpenSlideCommand
        {
            get
            {
                return _openSlideCommand ?? (_openSlideCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<OpenKaryoSlideEvent>().Publish(SlideVM);
                }, () => true));
            }
        }
        #endregion

        #region Methods
        public void DesignTimeData()
        {
            SlideVM = new SlideViewModel(new Slide("case 1", "Slide 1", "Slide1"), _eventAggregator);
        }
        #endregion
    }
}
