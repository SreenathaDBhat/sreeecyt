﻿using CVKaryoDAL.Models;
using CytoApps.Infrastructure.Helpers;
using CytoApps.Infrastructure.UI.Utilities;
using CytoApps.Models;
using KaryoViewModule.Events;
using Prism.Commands;
using Prism.Events;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace KaryoViewModule.ViewModels
{
    public class SlideViewModel : DesignTimeBindableBase
    {
        #region Private members
        public IEventAggregator _eventAggregator;
        #endregion

        #region Constructor
        public SlideViewModel()
        {
            if (base.IsInDesignMode)
                DesignTimeData();
        }

        public SlideViewModel(Slide slide, IEventAggregator eventAggregator)
        {
            _slide = slide;
            _eventAggregator = eventAggregator;
        }
        #endregion

        #region Properties
        private Slide _slide;
        public Slide Slide
        {
            get { return _slide; }
        }

        private ObservableCollection<CellViewModel> _cellsVMs = new ObservableCollection<CellViewModel>();
        public ObservableCollection<CellViewModel>CellVMs
        {
            get { return _cellsVMs; }
            set
            {
                _cellsVMs = value;
                OnPropertyChanged();
                OnPropertyChanged("KaryotypeCount");
                OnPropertyChanged("MetaphaseCount");
                OnPropertyChanged("CellCount");
            }
        }

        #endregion

        #region Commands
        private DelegateCommand _closeSlideCommand;
        public ICommand CloseSlideCommand
        {
            get
            {
                return _closeSlideCommand ?? (_closeSlideCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<CloseKaryoSlideEvent>().Publish(this);
                }, () => true));
            }
        }

        private DelegateCommand _karyotypeCommand;
        public ICommand KaryotypeCommand
        {
            get
            {
                return _karyotypeCommand ?? (_karyotypeCommand = new DelegateCommand(() =>
                {
                    _eventAggregator.GetEvent<KaryotypeEvent>().Publish(this);
                }, () => true));
            }
        }
        #endregion

        #region Methods
        public int CellCount
        {
            get { return _cellsVMs.Count(); }
        }

        public int MetaphaseCount
        {
            get { return _cellsVMs.Where(c=>c.Cell.HasMetaphase).Count(); }
        }

        public int KaryotypeCount
        {
            get { return _cellsVMs.Where(c => c.Cell.HasKaryotype).Count(); }
        }

        private CellViewModel _selectedCell;
        public CellViewModel SelectedCell
        {
            get { return _selectedCell; }
            set { _selectedCell = value;  base.OnPropertyChanged(); }
        }      

        private void DesignTimeData()
        {
            _slide = new Slide("Test Case", "Test Slide", "Slide0");

            var list = new List<CellViewModel>();

            System.Windows.Media.Imaging.BitmapImage metaphase = null;

            var fi = new System.IO.FileInfo(@"C:\metaphase.png");

            if (fi.Exists)
            {
                using (var fs = fi.OpenRead())
                    metaphase = Presenters.StandardImageDataRenderer.BitmapImageFromStream(fs);
            }

            list.Add(new CellViewModel(new KaryoCell { HasMetaphase = true, HasKaryotype = true, Name = "Good Cell 0", Id = "Cell0", Slide = _slide }) { MainImage = metaphase, SideImage = metaphase });
            list.Add(new CellViewModel(new KaryoCell { HasMetaphase = true, HasKaryotype = true, Name = "Good Cell 1", Id = "Cell1", Slide = _slide }) { MainImage = metaphase });
            list.Add(new CellViewModel(new KaryoCell { HasMetaphase = true, HasKaryotype = true, Name = "Good Cell 2", Id = "Cell2", Slide = _slide }) { MainImage = metaphase });
            list.Add(new CellViewModel(new KaryoCell { HasMetaphase = true, HasKaryotype = true, Name = "Good Cell 99", Id = "Cell3", Slide = _slide }) { MainImage = metaphase });

            SelectedCell = list.FirstOrDefault();

            CellVMs = list.ToObservableCollection();

        }
        #endregion
    }
}
