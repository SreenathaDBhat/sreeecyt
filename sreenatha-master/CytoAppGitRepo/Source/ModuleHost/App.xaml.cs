﻿using CytoApps.Infrastructure.Helpers;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;

namespace ModuleHost
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            // REFACTOR - Need to handle the MainWindow close in all analysing modules. Need to give a freindly message to the user for Saving all unsaved work.
            this.ShutdownMode = ShutdownMode.OnMainWindowClose; 
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(ToolBarModule.Properties.Settings.Default.Language);
            //To Set Current Language On Applcation Start Up(Local Date Time)
            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement),
            new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));
#if !DEBUG
            // log the unhandled exception in release mode
            DispatcherUnhandledException += ApplicationDispatcherUnhandledException; // event for most application unhandled exceptions
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException; // Although Application.DispatcherUnhandledException covers most issues in the current AppDomain, in extremely rare circumstances, you may be running a thread on a second AppDomain. This event conveys the other AppDomain unhandled exception
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException; // If you are using Tasks, then you may have "unobserved task exceptions". This event allows you to trap them  
#endif
            new CaseBrowserBootstrapper().Run();
        }
        
        /// <summary>
        /// Handling unhandled exception and logging to logmanager and Displaying proper message
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">Unhandled Exception</param>
        private void ApplicationDispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            LogManager.Fatal(e.Exception);
            e.Handled = false;
        }

        /// <summary>
        /// If you are using Tasks, then you may have "unobserved task exceptions". This event allows you to trap them
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            LogManager.Fatal(e.Exception);
        }

        /// <summary>
        /// // Although Application.DispatcherUnhandledException covers most issues in the current AppDomain, in extremely rare circumstances, you may be running a thread on a second AppDomain. This event conveys the other AppDomain unhandled exception
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            LogManager.Fatal(e.ExceptionObject as Exception);
        }
    }
}
