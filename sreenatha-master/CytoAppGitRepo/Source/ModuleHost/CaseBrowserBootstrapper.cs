﻿using Prism.Mef;
using Prism.Modularity;
using System.ComponentModel.Composition.Hosting;
using System.Windows;
using ToolBarModule;
using System;
using System.Net;

namespace ModuleHost
{
    class CaseBrowserBootstrapper : MefBootstrapper
    {
        /// <summary>
        /// Creates the <see cref="IModuleCatalog"/> used by Prism.
        /// </summary>
        /// <remarks>
        /// The base implementation returns a new ModuleCatalog.
        /// </remarks>
        /// <returns>
        /// A ConfigurationModuleCatalog.
        /// </returns>
        protected override IModuleCatalog CreateModuleCatalog()
        {
            // When using MEF, the existing Prism ModuleCatalog is still the place to configure modules via configuration files.
            return new ConfigurationModuleCatalog();
        }

        protected override void ConfigureAggregateCatalog()
        {
            string modulePath = @".\Modules";
            if (CheckDirectoryExists(modulePath))
            {
                //Create a new Directory catalog and mention the path from where it should load Dll's
                DirectoryCatalog directoryCatalog = new DirectoryCatalog(modulePath);
                //An aggregate catalog that combines multiple catalogs, Add directory catalog to AggregateCatalog
                AggregateCatalog.Catalogs.Add(directoryCatalog);
            }

            //Create assesmbly catalog and mention assembly name add it to AggregateCatalog
            AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(CaseBrowserBootstrapper).Assembly));
            AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(ToolBarModuleModule).Assembly));

            base.ConfigureAggregateCatalog();
        }

        private bool CheckDirectoryExists(string modulePath)
        {
            System.IO.DirectoryInfo dirinfo = new System.IO.DirectoryInfo(modulePath);
            return dirinfo.Exists;
        }

        protected override void ConfigureModuleCatalog()
        {
            base.ConfigureModuleCatalog();
        }

        protected override DependencyObject CreateShell()
        {
            return Container.GetExportedValue<ShellWindow>();
        }

        protected override void InitializeShell()
        {
            // HACK, override limit here about number of default connections otherwise its 2 and v slow
            // Should it be associated with eSM DAL?
            ServicePointManager.DefaultConnectionLimit = 20;

            base.InitializeShell();
            Application.Current.MainWindow = (ShellWindow)Shell;
            Application.Current.MainWindow.Show();
        }
    }
}
