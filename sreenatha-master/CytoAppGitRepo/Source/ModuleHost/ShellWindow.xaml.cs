﻿using System.ComponentModel.Composition;
using System.Windows;

namespace ModuleHost
{
    /// <summary>
    /// Interaction logic for ShellWindow.xaml
    /// </summary>
    [Export]
    public partial class ShellWindow : Window
    {
        public ShellWindow()
        {
            InitializeComponent();
        }
    }
}
