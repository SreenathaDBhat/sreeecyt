﻿using CytoApps.Infrastructure.UI.ApplicationCommands;
using CytoApps.Infrastructure.UI.Controls;
using CytoApps.Infrastructure.UI.Events;
using Prism.Commands;
using Prism.Mvvm;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace ModuleHost
{
    class ShellWindowViewModel : BindableBase
    {
        public ICommand WindowClosingCommand
        {
            get
            {
                return new DelegateCommand<ShutDownEventArgs>(
                    (args) =>
                    {
                        if (HostCommands.ShutdownCommand.CanExecute((ShutDownEventArgs)args))
                        {
                            HostCommands.ShutdownCommand.Execute(args);
                        }
                        if (args.CanClose)
                        {
                            if (args.CancelEventArgs.Cancel)
                            {
                                MessageBoxResult result = CustomPopUp.Show("Do you want to save the changes you made?", "CaseBrowser", MessageBoxButton.YesNo, MessageBoxImage.Question, true);
                                if (result == MessageBoxResult.No)
                                {
                                    args.CancelEventArgs.Cancel = false;
                                }
                            }
                        }
                        else
                        {
                            args.CancelEventArgs.Cancel = true;
                        }
                    });
            }
        }
    }
}
