////////////////////////////////////////////////////////////////////////////////////////
// Autofocus measurements 
// Header file
// K Ratcliff
////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#ifdef CAMMANAGED
#undef AFMEASURE_API
#define AFMEASURE_API
#elif defined(HELPER2_EXPORTS)
#define AFMEASURE_API __declspec(dllexport)
#else
#define AFMEASURE_API __declspec(dllimport)
#endif

////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

// 8 bit images
double AFMEASURE_API AutoFocusMeasure8(BYTE *pImage, int Width, int Height, int Left, int Top, int Right, int Bottom, int Spacing, int Thresh);      
    
BYTE AFMEASURE_API *Separable3by3Median(BYTE *pImage, int Width, int Height);

WORD AFMEASURE_API *Separable3by3Median(WORD *pImage, int Width, int Height);

double AFMEASURE_API AutoFocusMeasureZoned8(BYTE *pImage, int Width, int Height, int Left, int Top, int Right, int Bottom, int Thresh);

// > 8 bit images
double AFMEASURE_API AutoFocusMeasure16(WORD *pImage, int Width, int Height, int Left, int Top, int Right, int Bottom, int Thresh);     
double AFMEASURE_API AutoFocusMeasure16HV(WORD *pImage, int Width, int Height, int Left, int Top, int Right, int Bottom, int Thresh);     


void AFMEASURE_API ZonedAutoFocusMeasure16(WORD *pImage, int Width, int Height, int NZonesX, int NZonesY, int Thresh, double *pAFM);

// Morphological ops on 1D data

double AFMEASURE_API *OneDExtract(double *Data, int Length, int Width, int NoiseWidth);


double AFMEASURE_API AFMomentThreshold(BYTE *Addr, int Width, int Height);


double AFMEASURE_API Vollath_F4(WORD *pImage, int Width, int Height);
