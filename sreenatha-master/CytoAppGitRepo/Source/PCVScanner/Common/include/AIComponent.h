/*
 * AIComponent.h
 * JMB April 2001
 *
 *	01May01	WH:	Removed from AItiff.cpp. Added Image operations list stuff.
 *				including custom tags AI_TAG_IMAGEOPS.
 *	22Oct01	WH:	Added zstack (serialized ai_tiff) block via custom tags AI_TAG_ZSTACK
 *	02Dec01	WH:	Added StageInfo block via AI_TAG_STAGEINFO
 *  16Jan02	JMB	Added MIP maps.
 */


// Tag numbers for AI directories; may be any unique unsigned 16 bit number.
// These values are unrelated to the standard TIFF tag numbers.
#define AI_TAG_COLOUR						1
#define AI_TAG_IMAGEOPS						2
#define AI_TAG_ANNOTATION					3
#define AI_TAG_ZSTACK						4
#define AI_TAG_ZSTACKINFO					5
#define AI_TAG_STAGEINFO					6
#define AI_TAG_MIPMAPS                    100

#define AI_TAG_EXAMPLESTRING			65000
#define AI_TAG_EXAMPLELONG				65001
#define AI_TAG_EXAMPLESTRUCTARRAY		65002



struct ExampleStruct
{
	int i;
};
// IMPORTANT not to delete any items, but to add new ones to end of structures for all these.
// i.e structure mustn't get smaller or have members rearranged for backwards compatibility
struct IOpsHeader
{
	int type;				// which union to use
	int version;			// version
	int size;				// actual size of structure in union;
};

// Part of ImageOp Union
struct ConStretch
{
	unsigned int min;
	unsigned int max;
};

// Part of ImageOp Union
struct AITiffFilter
{
	TCHAR name[32];
	int hwidth;
	int data[255];
};

// Part of ImageOp Union
struct AITiffUnsharpMask
{
	float radius;
	float depth;
	long  reserved; // Paintshop pro has a 'clipping' parameter and photoshop
	                // has a 'threshold' parameter (not sure if they are the
	                // same thing) - this value is reserved for implementation
	                // of a similar feature in the future.
};

// ImageOp structure, type determines which union to use
struct ImageOp
{
	IOpsHeader header;
	union OpsUnion
	{
		ConStretch        stretch;
		AITiffFilter      filter;
		AITiffUnsharpMask unsharp;
	} u;
	ImageOp *next;
};

// Header
struct ZStackInfo
{
	int m_iszstack;		// Is it a zstack tiff
	int m_spacing;		// space between plane (um)
	int m_objsize;		// object of interest size
};

struct StageInfo
{
	int	  frame_no;
	float ideal_x;
	float ideal_y;
	float ideal_z;
	float pixelscale_x;
	float pixelscale_y;
};


class AI_Tiff;


class AITIFF_API AI_TiffComponent : public TiffComponent
{
// This contains Applied Imaging specific image associated data pointed to by
// 'reusable' (non-standard) tag 65441 (0xFFA1) in an standard IFD.
// e.g. Flourochrome name, annotation list etc...
// The AI tag stores a file offset, at which offset is a structure similar to a IFD.
// This class (AI_TiffComponent) contains data derived from this 'AI IFD'.

public: // WH
	ImageOp * m_iops_list;	// Linked list of image ops
	ImageOp	  m_lastOp;		// Op not commited to main list yet, but performed if not opNone

	int m_annotationSize;	// Annotation buffer and size
	void *m_pAnnotation;

	BOOL AddStretchContrast(int min, int max, BOOL bCommit);
	BOOL AddFilter(LPCTSTR name, int width, int *data, BOOL bCommit);
	BOOL AddUnsharpMask(float radius, float depth, BOOL bCommit);

	StageInfo m_stageinfo;

	void ResetLastOp();
	void UndoLastOp();
	void ProcessIntoCache();
	void MergeLastOpToDisplay();
	void MergeLastOpToCache();
	static void DeleteOp(ImageOp * op); // If someone works out why can't delete outside module, can remove these
	ImageOp * CreateOp();
	ImageOp *CopyOpsList();
	void DeleteOpsList();
	int m_version;

	enum ops {opNone, opStretch, opFilter, opUnsharp};
	// Pointer to processed image data, may just point to the raw pData if no processing
	uint8 *m_pCache;
	// Pointer to processed image data, may just point to the raw pData if no processing
	// or pCache if no m_lastOp but processing
	uint8 *m_displayBuf;

private:
	void ProcessOp(ImageOp * op);
	void Stretch(ImageOp * op);
	void Filter(ImageOp * op);
	void UnsharpMask(ImageOp *op);

	void TidyUp();

	BOOL m_bCacheBuf, m_bDisplayBuf;

public: // MIP map stuff
	AI_Tiff *pMIPMaps;
	BOOL CreateMIPMaps();
	int GetAvailableMIPMapLOD(int idealLOD);
	AI_TiffComponent *GetMIPMap(int lod);

public: // ZStack
	AI_Tiff *pZStackTiff;
	ZStackInfo m_zstackinfo; // If a zstack tiff image, some extra info
	int m_zstackSize;
	void *m_pZstack;

public:
	// Visibility flag used by TiffRenderer class
	BOOL visible;

	rgbTriple colour; // RGB colour for greyscale images
/*
	char *exampleString;
	int exampleLong;

	int numExampleStructs;
	ExampleStruct *pExampleStructArray;
*/
	// Pointer for forming linked lists
	AI_TiffComponent *pNext;

	uint8 *GetData();

	int GetMemoryUsage();

	AI_TiffComponent();
	~AI_TiffComponent();
};

