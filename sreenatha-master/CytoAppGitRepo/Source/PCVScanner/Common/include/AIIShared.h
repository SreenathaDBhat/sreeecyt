// AII_Shared.h: interface for the CAII_Shared class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AII_SHARED_H__D6061CD1_12D7_4F19_8F0B_ECE9A02C282D__INCLUDED_)
#define AFX_AII_SHARED_H__D6061CD1_12D7_4F19_8F0B_ECE9A02C282D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef HELPER3_EXPORTS
#define HELPER3_API __declspec(dllexport)
#else
#define HELPER3_API __declspec(dllimport)
#endif


// Cytovision Registry entries
#define SERVER_NAME            _T("~SERVER")

// Common shared dir between Applications
#ifdef CLEVELAND_BUILD
#define ROOT_SHARED					_T("TMA_SHARED")
#else
#define ROOT_SHARED					_T("AII_SHARED")
#endif

// Common shared subdirs between apps
#define SCRIPTS_SHARED				ROOT_SHARED _T("\\SCRIPTS")
#define ASSAYS_SHARED				ROOT_SHARED _T("\\ASSAYS")
#define CLASSIFIERS_SHARED			ROOT_SHARED _T("\\CLASSIFIERS")
#define CLASSLISTS_SHARED			ROOT_SHARED _T("\\CLASSLISTS")
#define FORMATS_SHARED				ROOT_SHARED _T("\\Formats")
#define BLOBPOOL					_T("\\AII_BLOBPOOL")

class HELPER3_API CAII_Shared  
{
public:
	CAII_Shared();
	virtual ~CAII_Shared();

	static CString Root();
	static CString CaseBase();
	static CString ClassifiersPath();
	static CString ClassListsPath();
	static CString ScriptsPath();
	static CString AssaysPath();
	static CString SPOTAssaysPath();
	static CString FormatsPath();

	int InitialiseAssaysPath();
	CString NextAssaysPath();

	static CString BlobPoolPath();
	static CString TempDirPath();
	static CString CaseBasePath;

private:
	CStringArray m_AssayPaths;
	int m_AssayPathsCount;
	int m_CurrentAssayPath;
};

#endif // !defined(AFX_AII_SHARED_H__D6061CD1_12D7_4F19_8F0B_ECE9A02C282D__INCLUDED_)
