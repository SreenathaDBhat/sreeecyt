//
// AIListCtrl.h
// JMB October 2003
//

class CAIListCtrl;

class CAIListCtrlEdit : public CEdit
{
friend class CAIListCtrl;
protected:
	CAIListCtrl *pAIListCtrl;
	int listItemIndex;
	int listSubItemIndex;
	BOOL Create(const RECT &rect, CAIListCtrl *pParent, UINT nID, int listItemIndex, int listSubItemIndex);
	DECLARE_MESSAGE_MAP()
	afx_msg UINT OnGetDlgCode();
	void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	CAIListCtrlEdit(){ pAIListCtrl = NULL; listItemIndex = -1; listSubItemIndex = -1; }
};

class CAIListCtrlCombo : public CComboBox
{
friend class CAIListCtrl;
protected:
	CAIListCtrl *pAIListCtrl;
	int listItemIndex;
	int listSubItemIndex;
	BOOL Create(const RECT &rect, CAIListCtrl *pParent, UINT nID, int listItemIndex, int listSubItemIndex,
	            CStringArray *pStrings, BOOL editable/*=TRUE*/);
	DECLARE_MESSAGE_MAP()
	afx_msg UINT OnGetDlgCode();
	void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnCbnKillFocus();
	CAIListCtrlCombo(){ pAIListCtrl = NULL; listItemIndex = -1; listSubItemIndex = -1; }
};


class CAIListCtrl : public CListCtrl
{
private:
	CAIListCtrlEdit  editCtrl;
	CAIListCtrlCombo comboBox;

public:
	CAIListCtrl();
	virtual ~CAIListCtrl();

	// Overrides
	int InsertItem(const LVITEM* pItem);
	int InsertItem(UINT nMask, int nItem, LPCTSTR lpszItem, UINT nState, UINT nStateMask,
                   int nImage, LPARAM lParam);
	int InsertItem(int nItem, LPCTSTR lpszItem){ return InsertItem(LVIF_TEXT, nItem, lpszItem, 0, 0, 0, 0); }
	int InsertItem(int nItem, LPCTSTR lpszItem, int nImage){ return InsertItem(LVIF_TEXT|LVIF_IMAGE, nItem, lpszItem, 0, 0, nImage, 0); }
	BOOL DeleteItem(int nItem);
	BOOL DeleteAllItems();
	BOOL SetItemData(int nItem, DWORD_PTR dwData);
	DWORD_PTR GetItemData(int nItem);

	// Extensions to CListCtrl
	BOOL SetEditableItem(int nItem, int nSubItem, LPCTSTR lpszText);
	BOOL SetComboItem(int nItem, int nSubItem, LPCTSTR lpszText, CStringArray *pStringArray, BOOL editable = TRUE);

	// Calling these functions directly will enable modification of a subitem
	// even if it has not been set as an editable or combo item.
	BOOL BeginEdit(int nItem, int nSubItem);
	BOOL BeginCombo(int nItem, int nSubItem, CStringArray *pStringArray, BOOL editable = TRUE);


protected:
	DECLARE_MESSAGE_MAP()
	// Message map functions
protected:
	afx_msg void OnDestroy();
	afx_msg BOOL OnClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
};