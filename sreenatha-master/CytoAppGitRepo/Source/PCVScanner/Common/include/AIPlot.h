// AIPlot.h: interface for the CAIPlot class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AIPLOT_H__22CE41E4_E24A_4AB2_A3DE_C50B66140E2F__INCLUDED_)
#define AFX_AIPLOT_H__22CE41E4_E24A_4AB2_A3DE_C50B66140E2F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <comdef.h>					// For _variant_t class definitions
#include "SRGBinMarker.h"
#include "SRGSyncDisplay.h"
#include "AIShape.h"
#include <vector>

using namespace std;


//
//	High level plotting class wrapping Stingray Objective Charts (OC)
//
class CAIPlot  
{
// Constructors/Destructors
public:
	// Default constructor
	CAIPlot( SRGraph * pGraph = NULL);		
	virtual ~CAIPlot();

//Attributes
public:


// Operations
public:
	// Clears the plot data points
	virtual void ClearData( void );
	// Clear the plot background, titles etc
	virtual void ClearLayout( void );
	// Clears everything, all plot data and layout
	virtual void ClearPlot( void );

	// Sets the graph object
	virtual void      SetGraph( SRGraph * pGraph)  { m_pGraph = pGraph; }
	// Returns the graph object
	virtual SRGraph * GetGraph( void )             { return m_pGraph; }

	// Adds one data item to the plot
	virtual void AddData( const _variant_t& vVal, int nGroup = 0, int nIndex = 0);

	// Gets one group's data back from the chart in the form of an array
	virtual void GetGroupData(CArray<int> & groupData, int nGroup = 0);
	// Get the whole plot data in an STL object
	virtual void GetPlotData(vector< vector< vector<double> > > & plotData);

	// Use plot wizard to edit plot layout
	virtual BOOL PlotWizard( void);
	// Load plot layout from file
	virtual BOOL LoadLayout( LPCTSTR szFileName);
	// Save plot layout to file
	virtual BOOL SaveLayout( LPCTSTR szFileName);

	// Set X-axis label text
	virtual void SetTitleLabel( LPCTSTR szTitle);
	// Set X-axis label text
	virtual void SetXAxisLabel( LPCTSTR szX);
	// Set Y-axis label text
	virtual void SetYAxisLabel( LPCTSTR szY);

	// Set top text panel title text
	virtual void SetTopTextPanelTitle( LPCTSTR szTopTitle);
	// Set top text panel title text
	virtual void SetTopTextPanelText( LPCTSTR szTopText);
	// Set bottom text panel text
	virtual void SetBottomTextPanelTitle( LPCTSTR szBottomTitle);
	// Set bottom text panel text
	virtual void SetBottomTextPanelText( LPCTSTR szBottomText);

	// Draw the plot itself
	virtual void DrawPlot( CDC * pDC, CWnd * pCWnd);

	// Save plot image to file
	virtual BOOL SaveToFileAsDib(LPCTSTR lpszFileName, int nWidth, int nHeight);

	// Find a component that lies at a position
	virtual SRGraphComponent * GetComponentAtPoint( UINT type, CPoint& point);

	// Add a shape for drawing - polygon
	virtual CAIShape * AddShape( CDoublePoint * aDPoint, int nCount,
	                             int nBorderWidth = 1, COLORREF BorderColour = 0x00000000,
	                             ShapeType shape = Shape_Polygon);
	// Add a shape for drawing - rectangle or ellipse
	virtual CAIShape * AddShape( double dLeft, double dTop, double dRight, double dBottom, 
	                             int nBorderWidth = 1, COLORREF BorderColour = 0x00000000,
	                             ShapeType shape = Shape_Rectangle);
	// Remove a shape for drawing
	virtual void RemoveShape( CAIShape * pShape);
	// Clear the shape list
	virtual void ClearShapeList( void);

	// Show X grid lines
	virtual void ShowXGrid( BOOL bShow);
	// Show Y grid lines
	virtual void ShowYGrid( BOOL bShow);
	// Set colour of grid lines
	virtual void SetGridColour( COLORREF colour);
	// Set tick mark size
	virtual void SetTickSize( double dSize);

	// Get the number of point shapes
	virtual int GetPointShapeCount( void );
	// Get a point shape name
	virtual LPCTSTR GetPointShapeName( int nIndex);
	// Sets the shape of every point in the group
	virtual void SetGroupShape( int nGroup, LPCTSTR szShapeName);

	// Sets the size of every point in the scatter plot
	virtual void SetGroupSize( int nGroup, double dWidth, double dHeight);
	// Sets the colour of every point in the group
	virtual void SetGroupColour( int nGroup, COLORREF groupColour );
	// Sets the first group to be plotted
	virtual void SetGroupMin( int nGroupMin);
	// Sets the last group to be plotted
	virtual void SetGroupMax( int nGroupMax);
	// Get the number of groups in the graph
	virtual int GetGroupCount( void)	{ return m_pGraph->GetGroupCount(); };

	// Get the maximum value along the x-axis
	virtual double GetXMax( void);
	// Get the minimum value along the x-axis
	virtual double GetXMin( void);

	//
	// Histogram specific operations
	//

	// Adds the value to the extents for the value axis
	virtual void AddToScan( const _variant_t& vVal);
	// Sets the number of bins to use in this histogram
	//virtual void SetBinCount( int nBinCount) { m_nBinCount = nBinCount; }
	// Returns the number of bins used in this histogram
	virtual int  GetBinCount( void )         { return m_nBinCount; }
	// Determine which bin the value belongs in
	virtual int  GetBinIndex( double dVal);
	// Determine which bin the x pixel position corresponds to
	virtual int  GetBinIndexFromPixelPos( int nX );
	// Determine the range of bin nIndex
	virtual BOOL BinRangeFromIndex( int nIndex, double & dMin, double & dMax);
	// Determine the value axis extents and labels, and initialise groups and bins
	virtual void InitGroupBin( int nGroupCount, int nBinCount, bool niceLimits = true);
	// Set default values for Y axis from all histogram data
	virtual void SetDefaultYRange( void );
	// Set a default label for each bin
	virtual void ResetBinLabels( void);
	// Set a label for each bin by scaling original values
	virtual void SetBinLabels( double dRefValue, double dScaledRefValue, double dScaledInterval);
	// Overlay main histo with another histo
	virtual void AddHistoOverlay( int nGroupMin, int nGroupMax);

	// Returns an array of the bin center values
	virtual void GetBinCenters(CArray<double> & binCenters );
	// Set the colour of a range of bins corresponding to a real valued range
	virtual void SetRangeBinColour( int nGroup, double dMin, double dMax, COLORREF colour);
	// Set the colour of a range of bins corresponding to a bin index range
	virtual void SetRangeBinColour( int nGroup, int nMin, int nMax, COLORREF colour);
	// Set the colour of a single bin
	virtual void SetBinColour( int nGroup, int nIndex, COLORREF colour);
	// Add a bin marker component to plot
	virtual void AddBinMarker( int nRefIndex, COLORREF colour, LPCTSTR szName, int nLevel);
	// Remove bin marker component from plot
	virtual void RemoveBinMarker( int nRefIndex);
	// Move a bin range marker
	virtual void SetBinMarkerRange( int nRefIndex, int nStart, int nFinish);
	// Set the bin marker's text 
	virtual void SetBinMarkerText( int nRefIndex, LPCTSTR szText);
	// Set the bin marker's colour
	virtual void SetBinMarkerColour( int nRefIndex, COLORREF colour);


	//
	// Scatter plot specific operations
	//

	// Sets the shape of one scatter plot point
	virtual void SetDataPointShape( int nIndex, LPCTSTR szShapeName);

	// Sets the size of one scatter plot point
	virtual void SetDataPointSize(   int nIndex, double dWidth, double dHeight);

	// Sets the colour of one scatter plot point
	virtual void SetDataPointColour( int nIndex, COLORREF colour);

	// Sets the shape of every point in the scatter plot
	virtual void SetGroupShape( LPCTSTR szShapeName);

	// Sets the size of every point in the scatter plot
	virtual void SetGroupSize( double dWidth, double dHeight);

	// Sets the colour of every point in the group
	virtual void SetGroupColour( COLORREF groupColour );

// Implementation
protected:
	// Objective Chart graph object
	SRGraph *	m_pGraph;

	// Plot title
	CString		m_sTitle;

	// List of shapes to draw over plot
	CList<CAIShape*, CAIShape*> shapeList;

	//
	// Histogram specific members
	//

	// The number of bins in the histogram
	int m_nBinCount;

	// vector of vectors of list for data in the histogram plot
	// The first dimension is the Group
	// The second dimension is the histogram bin
	// The third dimension is the actual data in each bin
	vector< vector< vector<double> > > m_vPlotData;


// Implementation
protected:
	// Create default background component
	virtual void MakeBackground( void);
	// Create default title component
	virtual void MakeTitleLabel( LPCTSTR szTitle);
	// Create default display component - sets the displayed plot axes and plot type
	virtual void MakeDisplay( void);
	// Create default axis components
	virtual void MakeAxisLabel( LPCTSTR szX, LPCTSTR szY);

	// Sets the colour of one data item
	virtual void SetColour( int nGroup, int nIndex, COLORREF colour)
	{
		SRGraphStyle* pStyle = m_pGraph->GetSafeData( nIndex, nGroup)->GetStyle();
		pStyle->SetInteriorStyle(CX_INTERIOR_COLOR);
		pStyle->SetColor( colour );
	};

	// Sets the shape of one data item
	virtual void SetShape( int nGroup, int nIndex, LPCTSTR szShapeName)
	{
		SRGraphStyle* pStyle = m_pGraph->GetSafeData( nIndex, nGroup)->GetStyle();
		pStyle->SetObjectStyle(CX_OBJECT_POINT);
		pStyle->SetObjectName( szShapeName);
	};

	// Sets the size of one data item
	virtual void SetSize( int nGroup, int nIndex, double dWidth, double dHeight)
	{
		SRGraphStyle* pStyle = m_pGraph->GetSafeData( nIndex, nGroup)->GetStyle();
		pStyle->SetMaxObjectWidth(  dWidth);								// value*10 pixels wide
		pStyle->SetMaxObjectHeight( dHeight);								// value*10 pixels high
	};

	// Sets a label's text
	virtual void SetLabelText( int nLabelNumber, LPCTSTR szLabelText);

	// Sets a text panel's title
	virtual void SetTextPanelTitle( int nTextPanelNumber, LPCTSTR szTitleText);
	// Sets a text panel's text
	virtual void SetTextPanelText( int nTextPanelNumber, LPCTSTR szPanelText);

};



//
//	Generic Histogram class
//
class CAIHisto : public CAIPlot
{
// Constructors/Destructors
public:
	CAIHisto( SRGraph * pGraph);
	virtual ~CAIHisto();

//Attributes
public:


// Operations
public:
	// Clears the data and value axis extents ready for a new histogram
	virtual void ClearData( void );
	// Clears everything, all plot data and layout
	virtual void ClearPlot( void );

	// Adds the value to the extents for the value axis
	virtual void AddToScan( const _variant_t& vVal);

	// Get an array of the bin centers
	virtual void GetBinCenters(CArray<double> & binCenters );

	// Set the colour of a single bin
	virtual void SetBinColour( int nGroup, int nIndex, COLORREF colour);

	// Get the maximum value along the x-axis
	virtual double GetXMax( void);
	// Get the minimum value along the x-axis
	virtual double GetXMin( void);

// Implementation
protected:
	// The scale to use in determining the extents of the value axis to be binned. Objective Chart type.
	CScale m_Scale;
	// The count of data items to be scanned
	//int m_nScanCount;

// Implementation
protected:
	// Create default display component - sets the displayed plot axes and plot type
	virtual void MakeDisplay( void);
};



//
//	Real valued histogram class
//
class CAIHistoReal : public CAIHisto
{
// Constructors/Destructors
public:
	CAIHistoReal( SRGraph * pGraph);
	virtual ~CAIHistoReal();

//Attributes
public:


// Operations
public:
	// Determine which bin the value belongs in
	virtual int GetBinIndex( double dVal);
	// Determine which bin the x pixel position corresponds to
	virtual int  GetBinIndexFromPixelPos( int nX );
	// Determine the range of bin nIndex
	virtual BOOL BinRangeFromIndex( int nIndex, double & dMin, double & dMax);

	// Adds one data item to the scan. Data is assigned to the correct bin
	virtual void AddData( const _variant_t& vVal, int nGroup /*= 0*/, int nIndex /*= 0*/);

	// Determine the value axis extents and labels, and initialise groups and bins
	virtual void InitGroupBin( int nGroupCount, int nBinCount, bool niceLimits = true);
	// Set default values for Y axis from all histogram data
	virtual void SetDefaultYRange( void );
	// Set a default label for each bin
	virtual void ResetBinLabels( void);
	// Set a label for each bin by scaling original values
	virtual void SetBinLabels( double dRefValue, double dScaledRefValue, double dScaledInterval);

	// Overlay main histo with another histo
	virtual void AddHistoOverlay( int nGroupMin, int nGroupMax);

	// Get a group's data in the form of an array
	virtual void GetGroupData(CArray<int> & groupData, int nGroup = 0);

	// Get the whole plot's data
	virtual void GetPlotData(vector< vector< vector<double> > > & plotData);

	// Set the colour of a range of bins corresponding to a real valued range
	virtual void SetRangeBinColour( int nGroup, double dMin, double dMax, COLORREF colour);

	// Set the colour of a range of bins corresponding to a bin index range
	virtual void SetRangeBinColour( int nGroup, int nMinBinIndex, int nMaxBinIndex, COLORREF colour);

	// Add a bin marker component to plot
	virtual void AddBinMarker( int nRefIndex, COLORREF colour, LPCTSTR szName, int nLevel);
	// Remove bin marker component from plot
	virtual void RemoveBinMarker( int nRefIndex);
	// Move a marker
	virtual void SetBinMarkerRange( int nRefIndex, int nStart, int nFinish);
	// Set the bin marker's text 
	virtual void SetBinMarkerText( int nRefIndex, LPCTSTR szText);
	// Set the bin marker's colour
	virtual void SetBinMarkerColour( int nRefIndex, COLORREF colour);


// Implementation
protected:
	// Expands range to nice values
	virtual void FindLimits( CScale* pScale);			
};



//
//	Generic Scatter plot class
//
class CAIScatter : public CAIPlot
{
// Constructors/Destructors
public:
	CAIScatter( SRGraph * pGraph);
	virtual ~CAIScatter();

//Attributes
public:


// Operations
public:
	// Adds one data item to the scatter plot
	virtual void AddData( const _variant_t& vVal, int nGroup /*= 0*/, int nIndex /*= 0*/);

	// Draw the plot itself with application defined shapes
	virtual void DrawPlot( CDC * pDC, CWnd * pCWnd);

	// Sets the size of one scatter plot point
	virtual void SetDataPointSize(   int nIndex, double dWidth, double dHeight);

	// Sets the shape of one scatter plot point
	virtual void SetDataPointShape( int nIndex, LPCTSTR szShapeName);

	// Sets the colour of one scatter plot point
	virtual void SetDataPointColour( int nIndex, COLORREF colour);

	// Sets the size of every point in the group
	virtual void SetGroupSize( double dWidth, double dHeight);

	// Sets the shape of every point in the scatter plot
	virtual void SetGroupShape( LPCTSTR szShapeName);

	// Sets the colour of every point in the group
	virtual void SetGroupColour( COLORREF groupColour );

// Implementation
protected:
	// Create default scatter plot display component - sets the displayed plot axes and plot type
	virtual void MakeDisplay( void);

};







#endif // !defined(AFX_AIPLOT_H__22CE41E4_E24A_4AB2_A3DE_C50B66140E2F__INCLUDED_)
