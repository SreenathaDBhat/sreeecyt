//=================================================================================
// Queue.h
//
// Event locking circular queue (FIFO) using memory mapped files, for interprocess
// data sharing.
//
// History:
//
// 11Oct00	WH:	original
//=================================================================================
#ifndef __QUEUE_H__
#define __QUEUE_H__

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the QUEUE_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// QUEUE_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef QUEUE_EXPORTS
#define QUEUE_API __declspec(dllexport)
#else
#define QUEUE_API __declspec(dllimport)
#endif

// This class is exported from the QUEUE.dll
class QUEUE_API CQueue 
{
public:
	CQueue();
	~CQueue();

	// Public interface functions
	int		Initialize(DWORD size = 20480000, LPCTSTR dir = NULL , LPCTSTR queue = NULL);
	int		Add(VOID *data, UINT datasize, UINT id);
	int		Remove(VOID *buffer, UINT *buffersize,  UINT *id);
	int		Peek(UINT *id, UINT *size);
	int		Reset();
	int		GetInfo(DWORD *size, DWORD *sink, DWORD *source);
	int		Lock(BOOL bWait, UINT timeout);
	int		ClearLock();

	// Return enumerations
	enum retvalues	{Ok, Error, Full, NoItems, LockOk, LockTimeout, NoLock, BufferSize}; 

private:
	HANDLE	m_hMapFile;					// Handle for our test memory-mapped region
	HANDLE	m_hFile;					// The file handle
	LPVOID	m_lpMem;					// Memory pointer
	DWORD	m_size;						// File size
	BOOL	m_bConnect;					// Connect to existing Queue
    HANDLE  m_hLockMutex;               // ensure mutual exclusion
	DWORD   m_lockThreadID;				// The ID of the thread that currently owns the lock
	TCHAR	m_QName[MAX_PATH];

	enum definetypes	{MagicID = 0xABCD};

	typedef struct Queue_header {		// Queue header type
		DWORD snk;	// Data Sink offset
		DWORD src;  // Data Source offset
	};									

	typedef struct item_header {		// Item Header type
		UINT magic;
		UINT size;
		UINT id;
	};							

	Queue_header m_hdr;			

private:
	BOOL	IsLockedByThisThread();
	int		ReadHeader(Queue_header *hdr);
	int		WriteHeader(Queue_header *hdr);
	int		ReadData(VOID *data, UINT datasize, BOOL bPeek);
	int		WriteData(VOID *data, UINT datasize);
	BOOL	FreeSpace(UINT size);  

	void DebugDump(LPTSTR fn_name, item_header *item);
	
};

#endif //__QUEUE_H__
