// AIReBar.h: interface for the CAIReBar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AIREBAR_H__4D1F37D8_B19B_42F4_AFAC_CD6A4BB7563B__INCLUDED_)
#define AFX_AIREBAR_H__4D1F37D8_B19B_42F4_AFAC_CD6A4BB7563B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef CAMMANAGED
#include "afxext.h"

#ifdef HELPER3_EXPORTS
#define HELPER3_API __declspec(dllexport)
#else
#define HELPER3_API __declspec(dllimport)
#endif

class HELPER3_API CAIReBar : public CReBar  
{
public:
	CAIReBar();
	virtual ~CAIReBar();

	BOOL AddAIToolBar(CAIToolBar* pToolBar);
};

#endif
#endif // !defined(AFX_AIREBAR_H__4D1F37D8_B19B_42F4_AFAC_CD6A4BB7563B__INCLUDED_)
