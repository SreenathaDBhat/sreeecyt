// AIShape.h: interface for the CAIShape class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AISHAPE_H__F86D000E_4A02_4C74_8561_7BCB682CB4AC__INCLUDED_)
#define AFX_AISHAPE_H__F86D000E_4A02_4C74_8561_7BCB682CB4AC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CAIScale;

class CDoublePoint
{
// Constructors/Destructors
public:
	CDoublePoint( double dXCoord = 0.0, double dYCoord = 0.0);
	virtual ~CDoublePoint();
	
//Attributes
public:
	double dX;
	double dY;

// Operations
public:

// Implementation
protected:

// Implementation
protected:

};


enum ShapeType
{   
	Shape_Rectangle=0,
	Shape_Ellipse,
	Shape_Polygon
};


class CAIShape  
{
// Constructors/Destructors
public:
	// Constructor for polygons
	CAIShape( CDoublePoint * aDPoint, int nCount,
	          int nBorderWidth = 1, COLORREF BorderColour = 0x00000000,
	          ShapeType shape = Shape_Polygon);
	// Constructor for rectangles and ellipses
	CAIShape( double dLeft, double dTop, double dRight, double dBottom, 
	          int nBorderWidth = 1, COLORREF BorderColour = 0x00000000,
	          ShapeType shape = Shape_Rectangle);
	virtual ~CAIShape();

//Attributes
public:


// Operations
public:
	virtual void Draw( CDC * pDC, CAIScale & scaleX, CAIScale & scaleY);

	virtual void GetPoint( int nIndex, CDoublePoint & point)
	{
		if (0 <= nIndex && nIndex < m_PointCount)
			point = m_aDPoint[nIndex];
	};
	virtual void SetPoint( int nIndex, CDoublePoint point)
	{
		if (0 <= nIndex && nIndex < m_PointCount)
			m_aDPoint[nIndex] = point;
	};

	virtual void GetRect( double & dLeft, double & dTop, double & dRight, double & dBottom)
	{
		if (m_Shape == Shape_Rectangle || m_Shape == Shape_Ellipse)
		{
			dLeft   = m_aDPoint[0].dX;		dTop    = m_aDPoint[0].dY;
			dRight  = m_aDPoint[1].dX;		dBottom = m_aDPoint[1].dY;
		}
	};
	virtual void SetRect( double dLeft, double dTop, double dRight, double dBottom)
	{
		if (m_Shape == Shape_Rectangle || m_Shape == Shape_Ellipse)
		{
			m_aDPoint[0].dX = dLeft;		m_aDPoint[0].dY = dTop;
			m_aDPoint[1].dX = dRight;		m_aDPoint[1].dY = dBottom;
		}
	};

	virtual int  GetBorderWidth( void)       { return m_nBorderWidth; };
	virtual void SetBorderWidth( int nWidth) { m_nBorderWidth = nWidth; };

	virtual COLORREF GetBorderColour( void)            { return m_BorderColour; };
	virtual void     SetBorderColour( COLORREF colour) { m_BorderColour = colour; };

// Implementation
protected:
	// Shape to be drawn
	ShapeType		m_Shape;
	// Number of shape vertices
	int				m_PointCount;
	// Set of shape vertices
	CDoublePoint *	m_aDPoint;
	// Width of border to draw round shape
	int				m_nBorderWidth;
	// Border colour
	COLORREF		m_BorderColour;

// Implementation
protected:

};

#endif // !defined(AFX_AISHAPE_H__F86D000E_4A02_4C74_8561_7BCB682CB4AC__INCLUDED_)
