// AIToolBar.h: interface for the CAIToolBar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AITOOLBAR_H__DF3E6B1A_BF30_434F_8BBC_4B716B695EB4__INCLUDED_)
#define AFX_AITOOLBAR_H__DF3E6B1A_BF30_434F_8BBC_4B716B695EB4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef CAMMANAGED

#ifdef HELPER3_EXPORTS
#define HELPER3_API __declspec(dllexport)
#else
#define HELPER3_API __declspec(dllimport)
#endif

#include "afxext.h"

class HELPER3_API CAIToolBar : public CToolBar  
{
public:
	CAIToolBar();
	virtual ~CAIToolBar();

	void Properties(int nButtonCount,
					UINT nColdBitmapID,
					UINT nHotBitmapID,
					UINT nMaskBitmapID,
					int nBitmapWidth,
					int nBitmapHeight,
					int nButtonMinWidth = 60,
					int nButtonMaxWidth = 150);

	void AddButton( int nIndex, UINT nID, UINT nStyle, int iImage, LPCTSTR lpszText);
	
	void SizeButtons( void);

private:
	int m_nBitmapWidth;
	int m_nBitmapHeight;

};

#endif// CAMMANAGED 
#endif// !defined(AFX_AITOOLBAR_H__DF3E6B1A_BF30_434F_8BBC_4B716B695EB4__INCLUDED_)