
/*
 * AITiff.h
 * JMB April 2001
 */



#ifndef AITIFF_H
#define AITIFF_H



// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the AITIFF_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// AITIFF_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef AITIFF_EXPORTS
#define AITIFF_API __declspec(dllexport)
#else
#define AITIFF_API __declspec(dllimport)
#endif
/*
// This class is exported from the AItiff.dll
class AITIFF_API CAItiff {
public:
	CAItiff(void);
	// TODO: add your methods here.
};
extern AITIFF_API int nAItiff;
AITIFF_API int fnAItiff(void);
*/

#include "tiffio.h"


#ifdef __cplusplus // Only include class definitions into C++ files


class AITIFF_API rgbTriple
{
public:
   uint8 r;
   uint8 g;
   uint8 b;
};



class AITIFF_API TiffComponent
{
// This class contains data derived from a TIFF Image File Directory (IFD)

public:

//////////////////////////////////////////////////////////////////////////////
// Variables corresponding to some of the standard TIFF tags
// Note that 'sample' is the term the TIFF spec uses to describe a
// component of a pixel, e.g. red value.
// Note that TIFF supports an image orientation parameter, but the only
// orientation of TIFF images guaranteed to be supported by readers is with
// the origin at top left, so this will be assumed.

	// Image description. Some older TIFF files used this for copyright information
	char *description;

	// Copyright notice
	char *copyright;

	char *make; // Hardware equipment manufacturer, e.g. "Applied Imaging"
	char *model; // Hardware equipment model

	// Software name and version
	char *software;

	// Computer and/or operating system in use at time of creation
	char *hostComputer;

	// Date and time of image creation.
	// Format of this string must be: "YYYY:MM:DD HH:MM:SS"
	char dateTime[20];


	uint16 *pColourmap;


	// Number of components per pixel, e.g. this is usually 3 for RGB
	// This value refers to how data in the file is stored; the image data
	// pointed to by pData may not be in same format: USE 'bpp' WITH pData!
	uint16 samplesPerPixel;

	// According to the TIFF spec, the number of bits is specified separately
	// for each sample, so this field should be an array of size
	// 'samplesPerPixel'. However, libtiff only supports each sample having
	// the same number of bits and only uses a single value for this field.
	// This value refers to how data in the file is stored; the image data
	// pointed to by pData may not be in same format: USE 'bpp' WITH pData!
	uint16 bitsPerSample;

	uint16 *pExtraSamples;

	// Dimensions of image in pixels
	uint32 width, height/*or 'length'*/;

	// Is the image greyscale, RGB, palette, etc.?
	// Values for this are defined in libtiff's tiff.h as PHOTOMETRIC_XXX
	uint16 photometricInterpretation;

	// e.g. RGBRGBRGB or RRRGGGBBB
	uint16 planarConfiguration;

	// Compression type. Values for this are defined in libtiff's tiff.h as COMPRESSION_XXX
	uint16 compression;


//////////////////////////////////////////////////////////////////////////////
// Variables not directly corresponding to TIFF tags

	// Pointer to image data
	uint8 *pData;

	// How many bytes are allocated to the block of memory pointed to by pData.
	uint32 dataBytes;

	// The index of this component in the file it was read from.
	int fileIndex;

	// How many bytes per pixel the image data pointed to by pData is stored in.
	// This may not be the same as (bitsPerSample*samplesPerPixel)/8, which
	// refers to how the data is stored in file.
	//int dataBytesPerPixel;

	// How many bits per pixel the image data pointed to by pData is stored in.
	// This may not be the same as (bitsPerSample * samplesPerPixel), which
	// refers to how the data is stored in file.
	uint16 bpp;

	// Calculate length of row in bytes
	static uint32 WidthBytes(uint32 w, uint32 bitsPerPixel)
	                       { return ((w * bitsPerPixel) + 7) >> 3; }
	uint32 WidthBytes(uint32 w){ return WidthBytes(w, bpp); }
	uint32 WidthBytes(){ return WidthBytes(width); }
	

	// Pointer for forming linked lists
	TiffComponent *pNext;

//////////////////////////////////////////////////////////////////////////////

	BOOL SetDescription(const char *pString);
	//uint32 GuessIdealStripHeight();


	TiffComponent();
	~TiffComponent();
};


#include "AIComponent.h"

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////



class AITIFF_API Tiff
{

protected:

	TIFF *pTIFF; // Pointer to an open file returned by libtiff

	int jpegQuality; // This must be 0->100. Library default is 75.
	int deflateQuality; // This must be 0->9, or -1, which tells the library to use its default value.
	double jpeg2000Rate; // This must be a positive number. A value of 1.0 is lossless.
	                     // A value greater than 1.0 gives identical results to a value of 1.0.

public:

	// Open a TIFF file for getting or adding components.
	// Mode parameter is similar to stdio
	BOOL OpenFile(const char fileName[], const char mode[]);
	BOOL CloseFile();


	// Get number of IFD's in file
	int CountFileComponents();

	// Get image info from component number 'index' (from zero)
	//TiffComponent *GetComponentInfo(int index); // As GetComponent(), but without the data

	// Load data into supplied component
	BOOL ReadComponent(TiffComponent *pComponent, int index, BOOL noData = FALSE);

	// Load and return Component number 'index' (from zero)
	TiffComponent *ReadComponent(int index);

	// Add Component to TIFF
	BOOL WriteComponent(TiffComponent*);

	// Returns a pointer to the last error message generated by
	// libtiff
	//const char *GetLastErrorMessage();


	BOOL SetJPEGQuality(int quality); // 0->100, 75 is default.
	int  GetJPEGQuality(){ return jpegQuality; }
	BOOL SetDEFLATEQuality(int quality); // 0->9, or -1 for default.
	int  GetDEFLATEQuality(){ return deflateQuality; }
	BOOL SetJPEG2000Rate(double rate); // 0.0->1.0
	double GetJPEG2000Rate(){ return jpeg2000Rate; }


	Tiff();
};


//////////////////////////////////////////////////////////////////////////////

/*
class AITIFF_API AITiffScale
{
	// Negative values become fractions, by the formula (1/(2-x)).
	int scale;

public:
	// Note that either the numerator or the denominator is 1.
	int  GetNumerator(){   return((scale > 0) ? scale :      1);       }
	int  GetDenominator(){ return((scale > 0) ?   1   : (2 - scale)); }

	void Double()
	{
		if (scale > 1000) return;
		scale = (scale > 0) ? (scale+scale) : (scale - (scale / 2) + 1);
	}
	void Halve()
	{ 
		if (scale < -10000) return;
		scale = (scale > 0) ? (scale / 2)   : 2 * (scale - 1);
	}

	double GetAsDouble()
	{
		double dScale = 0.0;

		if (scale > 0)
			dScale = (double)scale;
		else
			dScale = 1.0 / (double)(2 - scale);

		return dScale;
	}

	//int GetMIPmapLOD();
	//static int GetMIPmapScaleDenominator(int lod);

	void Set(AITiffScale newScale){ scale = newScale.scale; }
	void Set(AITiffScale *pNewScale){ if (pNewScale) scale = pNewScale->scale; }
	void SetInteger(unsigned short s){ scale = (int)s; }
	void SetFractional(unsigned short denominator){ scale = 2 - (int)denominator; }

	enum RoundingDirection { roundNearest, roundDown, roundUp };
	void SetFromDouble(double s, RoundingDirection round = roundNearest);

	BOOL Equals(AITiffScale s){ return (s.scale == this->scale); }
	BOOL GreaterThan(AITiffScale s){ return (this->scale > s.scale); }
	BOOL LessThan(AITiffScale s){ return (this->scale < s.scale); }


	AITiffScale(){ scale = 1; }
};
*/


class AITIFF_API AI_Tiff : public Tiff
{

protected:

	// List of loaded components
	AI_TiffComponent *pComponents;

	// Last file read by LoadFile()
	char *filename;


	// Subfile data used by LoadSubfile() and LoadComponentData()
	toff_t subfileOffset;
	tsize_t subfileSize;
	// Buffer pointer used by LoadSubfile() - only to be used by subfiles.
	// This pointer may be invalid once the top-level file has been loaded.
	// This pointer always points to the start of the main buffer - never
	// to the start of a sub-file within the buffer.
	// Never free this pointer directly.
	tdata_t pFileBuffer;



	// Use fast colour mixing code in AI_Tiff::Render()?
	BOOL colourMixFast;

	// Render buffer
	uint8 *pBuf;
	
	// Render buffer width in pixels
	uint32 bufWidth;
	// Render buffer width in bytes, including possible padding.
	uint32 bufWidthBytes;
	// Render buffer height
	uint32 bufHeight;
	// Number of bytes allocated to render buffer. May not be the same as
	// width bytes times height.
	uint32 bufBytes;


	BOOL gdiplusStartedExternally;
	ULONG_PTR gdiplusToken;


	// A change has been made to the image that requires it to be rendered
	// before drawing, e.g. component visibility or colour has changed.
	// Unless this flag is set, Draw() will not render an image unless its
	// size has changed.
	BOOL renderRequired;
	// The actual MIP level of detail used by the last render.
	int actualRenderedLOD;


	BOOL drawFlipX;
	BOOL drawFlipY;
	BOOL interpolateScaleUps;


	// Allocate render buffer based on component dimensions.
	BOOL ConfigureRenderBuffer(int idealLOD = 0);
	void FreeRenderBuffer();

	// Private function used by IsComponentDataLoadedForDraw() and EnsureComponentDataLoadedForDraw()
	BOOL ComponentDataLoadedForDraw(BOOL query, double scale);

	//void SetupWriteBuffer();

	BOOL Load(tdata_t buffer, thandle_t fd, const char fileName[],
              toff_t offset, tsize_t size, BOOL noData);
	BOOL Save(tdata_t *ppBuffer, tsize_t *pSize, const char fileName[],
	          uint16 compression);

	AI_TiffComponent *ReadComponent(int index, BOOL noData = FALSE);
	BOOL ReadComponent(AI_TiffComponent *pAIComponent, int index, BOOL noData = FALSE);
	BOOL ReadComponentData(AI_TiffComponent *pComponent);
	int WriteComponent(AI_TiffComponent *pComponent, uint16 compression);

	int AddComponent(AI_TiffComponent *pComponent);

	// Functions used by AI_Tiff class for adding data from this class to a file
	BOOL WriteColour(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset);

	BOOL WriteZStack(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset);
	BOOL WriteZStackInfo(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset);
	BOOL WriteStageInfo(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset);
	BOOL WriteImageOps(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset); // Image ops (WH)
	BOOL WriteAnnotation(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset);
	BOOL WriteMIPMaps(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset, uint16 compression = COMPRESSION_DEFLATE);
	BOOL WriteExampleString(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset);
	BOOL WriteExampleLong(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset);
	BOOL WriteExampleStructArray(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset);

	// Functions used by AI_Tiff class for reading data from a file into this class
	BOOL ReadColour(AI_TiffComponent *pComponent, TIFFDirEntry entry);

	BOOL ReadZStack(AI_TiffComponent *pComponent, TIFFDirEntry entry);
	BOOL ReadZStackInfo(AI_TiffComponent *pComponent, TIFFDirEntry entry);
	BOOL ReadStageInfo(AI_TiffComponent *pComponent, TIFFDirEntry entry);
	BOOL ReadImageOps(AI_TiffComponent *pComponent, TIFFDirEntry entry); // Image ops (WH)
	BOOL ReadAnnotation(AI_TiffComponent *pComponent, TIFFDirEntry entry);
	BOOL ReadMIPMaps(AI_TiffComponent *pComponent, TIFFDirEntry entry, BOOL noData = FALSE);
	BOOL ReadExampleString(AI_TiffComponent *pComponent, TIFFDirEntry entry);
	BOOL ReadExampleLong(AI_TiffComponent *pComponent, TIFFDirEntry entry);
	BOOL ReadExampleStructArray(AI_TiffComponent *pComponent, TIFFDirEntry entry);

public:

	int GetMemoryUsage();

	// Free any loaded data and initialise variables.
	void Reset();

	void CreateMIPMapsForAllComponents();
	BOOL CreateMissingMIPMaps();

/////////////////////////////////////////////////////////////////////////////
// File I/O

	// Load all components from named file
	BOOL LoadFile(const char fileName[], BOOL noData = FALSE);
	BOOL LoadSubfile(const AI_Tiff *pParent, toff_t offset, tsize_t size, BOOL noData);

	// Load the component pixel data (TiffComponent::pData) for the given component,
	// from the file that LoadFile() or LoadSubfile() was last called for.
	BOOL LoadComponentData(AI_TiffComponent *pComponent);

	// Write components to named file
	BOOL SaveFile(const char fileName[], uint16 compression = COMPRESSION_DEFLATE/*COMPRESSION_PACKBITS*/);

	// Load TIFF data from a block of memory
	BOOL LoadFileBuffer(const char *fileBuffer, size_t size);

	// Return a block of memory containing current TIFF data
	char *SaveFileBuffer(size_t *pSize, uint16 compression = COMPRESSION_DEFLATE/*COMPRESSION_PACKBITS*/);

	// Free a file buffer returned by SaveFileBuffer().
	void FreeFileBuffer(char *pBuf);

	// Copy this AI_Tiff into the passed AI_Tiff
	BOOL CopyInto(AI_Tiff *pDestTiff);

	//// Return a partial copy with only some data copied.
	//BOOL PartialCopyInto(AI_Tiff *pDestTiff);

	BOOL LoadNonTIFFile(wchar_t *filename);

/////////////////////////////////////////////////////////////////////////////
// Component access

	// Access description of component 'index'
	BOOL GetComponentDescription(int index, char *pDesc, int maxLen);
	BOOL SetComponentDescription(int index, char *pDesc);

	BOOL GetComponentColour(int index, rgbTriple *pColour);
	BOOL SetComponentColour(int index, rgbTriple colour);

	//BOOL GetComponentImageOpsSize(int index, int *pSize);
	//BOOL GetComponentImageOps(int index, void *pBuf);
	//BOOL SetComponentImageOps(int index, char *pBuf, int size);

	BOOL GetComponentAnnotationSize(int index, int *pSize);
	BOOL GetComponentAnnotation(int index, void *pBuf);
	BOOL SetComponentAnnotation(int index, char *pBuf, int size);

	BOOL GetComponentZStack(int index, AI_Tiff *pTiff, int *spacing, int *objsize);
	BOOL SetComponentZStack(int index, AI_Tiff *pTiff, int spacing, int objsize);

	BOOL GetComponentStageInfo(int index, int *frame_no, float *ix, float *iy, float *iz, float *scalex, float *scaley);
	BOOL SetComponentStageInfo(int index, int frame_no, float ix, float iy, float iz, float scalex, float scaley);

	BOOL GetComponentMIPMaps(int index, TiffComponent **ppMIPMaps);
	BOOL SetComponentMIPMaps(int index, TiffComponent *pMIPMaps);

	int GetComponentIndexByDescription(char *pDescription);

	BOOL SetComponentVisible(int i, BOOL yesNo);
	BOOL IsComponentVisible(int i);

	AI_TiffComponent *GetComponent(int index);
	int GetComponentWidth(int index);
	int GetComponentHeight(int index);
	//int GetComponentBytesPerPixel(int index);
	int GetComponentBPP(int index);
	const uint8 *GetComponentBytes(int index);
	uint8 *GetComponentBytes(AI_TiffComponent *pComponent);
	BOOL GetComponentBytes(int index, uint8 *pData);
	// Returns the number of components in the AITiff image
	int CountComponents();

	// Does what it says.
	BOOL RemoveComponent(int index);

	// Move the components from another AI_Tiff object to this object.
	BOOL Assimilate(AI_Tiff *pOtherTiff);
	
	// Create a TIFF component, optionally initialising
	// with the supplied data, and add to the list of components.
	// Returns index of new component if successful.
	int Add24bppComponent(uint32 width, uint32 height, uint8 *pData = NULL);
	int Add8bppComponent(uint32 width, uint32 height,
	                     uint8 *pData = NULL, BOOL createMIPMaps = FALSE);
	int Add1bppComponent(uint32 width, uint32 height,
	                     uint8 *pData = NULL, BOOL createMIPMaps = FALSE);

	// Old parameter order for above functions, for old code
	// - don't use in new code.
	int Add24bppComponent(uint8 *pData,uint32 width,uint32 height)
	{ return Add24bppComponent(width, height, pData); }
	int Add8bppComponent(uint8 *pData,uint32 width,uint32 height,BOOL createMIPMaps=FALSE)
	{ return Add8bppComponent(width, height, pData, createMIPMaps); }
	int Add1bppComponent(uint8 *pData,uint32 width,uint32 height,BOOL createMIPMaps=FALSE)
	{ return Add1bppComponent(width, height, pData, createMIPMaps); }

/////////////////////////////////////////////////////////////////////////////
// Rendering (implemented in rendertiff.cpp)

	// Round() rounds a floating-point number to an integer by increasing its
	// absolute value by 0.5 then truncating it. Hopefully as an inline
	// function this will be as fast as a macro.
	static inline int Round(double val){ return (int)((val < 0.0) ? (val - 0.5) : (val + 0.5)); }
	static int GetIdealMIPMapLOD(double scale);
	static int GetMIPMapScaleDenominator(int lod);
	int GetAvailableMIPMapLOD(int idealLOD);

	void SetDrawParameters(BOOL flipX = FALSE, BOOL flipY = FALSE){ drawFlipX = flipX; drawFlipY = flipY; }
	void SetInterpolation(BOOL interpolate){ interpolateScaleUps = interpolate; }

	// Blit render buffer into supplied device context.
	void Draw(HDC hDC, int x, int y, double scale, RECT *pClipRect = NULL,
	          double alpha = 1.0, int compressibilityFactor = 0);
	// Do not use DrawStretched() if at all possible (see comments in implementation).
	void DrawStretched(HDC hDC, RECT *pTargetRect, RECT *pClipRect/*=NULL*/);

	// Render components into render buffer. It is now (Jan 2002) generally
	// not neccessary or recommended to call this directly, as Draw() will call
	// it for you as required. Draw() will determine whether a render is
	// required by calling IsRenderRequired(), you normally do not need to call
	// this function directly either.
	void Render(int idealLOD = 0);
	BOOL IsRenderRequired(int idealLOD = 0);

	// Draw() (via Render()) will always ensure that the data it needs is loaded,
	// but using these functions allows drawing to be skipped, or data to be
	// pre-loaded, for images that were loaded with the noData flag set to TRUE.
	BOOL IsComponentDataLoadedForDraw(double scale);
	BOOL EnsureComponentDataLoadedForDraw(double scale);

	// Copy render buffer to user supplied buffer, in RGB format.
	// Buffer must be at least (GetWidth() * GetHeight() * 3)
	// bytes large.
	BOOL Get24bppBuffer(uint8 *pBuffer);
	BOOL SetBuffer(uint8 *pBuffer);	// Copy render buffer to supplied buffer (using memcpy)

	// Return the actual render buffer
	const uint8 *GetRenderBuffer(){ return pBuf; }

	// Use fast colour mixing code in AI_Tiff::Render()?
	void SetColourMixMode(BOOL fast){ colourMixFast = fast; }

	// Get dimensions of the render buffer. These will only have been set if
	// the render buffer has been configured, which normally only happens if
	// the image has been rendered or drawn.
	uint32 GetRenderBufferHeight(){ return bufHeight; }
	uint32 GetRenderBufferWidth(){  return bufWidth; }
	// Render buffer width in bytes, including possible padding.
	uint32 GetRenderBufferWidthBytes(){ return bufWidthBytes; }

	// Get image dimensions
	int GetWidth();
	int GetHeight();

	// Get scaled image dimensions 
	//int GetDisplayWidth(double scale){  return (int)(GetWidth() * scale); }
	//int GetDisplayHeight(double scale){ return (int)(GetHeight() * scale); }


	// The parameter tells the rendering code it doesn't need to start GDI+.
	AI_Tiff(BOOL gdiplusStartedExternally = FALSE);
	~AI_Tiff();
};


extern AITIFF_API CRITICAL_SECTION *AITiffGetCriticalSection();



// C function access (cfunc.cpp)
extern "C" AITIFF_API int aitiff_load(const char* filename, BOOL bOverlays);
extern "C" AITIFF_API BOOL aitiff_size(int handle, int *w, int *h, int *stride);
extern "C" AITIFF_API BOOL aitiff_getbuffer(int handle, BYTE * buffer, int size);
extern "C" AITIFF_API void aitiff_close(int handle);
extern "C" AITIFF_API int save_raw24_to_jp2(const char *path, BYTE * buffer, int width, int height, int stride, float rate);


#else  // __cplusplus

// C function access (cfunc.cpp)
AITIFF_API int aitiff_load(LPCTSTR filename, BOOL bOverlays);
AITIFF_API BOOL aitiff_size(int handle, int *w, int *h, int *stride);
AITIFF_API BOOL aitiff_getbuffer(int handle, BYTE * buffer, int size);
AITIFF_API void aitiff_close(int handle);
AITIFF_API int save_raw24_to_jp2(LPCTSTR path, BYTE * buffer, int width, int height, int stride, float rate);

#endif // __cplusplus


// JPEG2000 tags and enumerated values.
// I don't think it matters if tag values overlap enumerated values
// as they are used in different contexts.
#define COMPRESSION_JPEG2000  65361 // AITIFF JPEG2000 compression
                                    // - we have our own value in case it is
                                    // not compatible with other
                                    // implementations (there is currenly no
                                    // standard, de facto or otherwise). This
                                    // is within the 'reusable' private values
                                    // range of 65000-65535 (read the TIFF spec
                                    // for details).
                                    // If a future version of libtiff defines
                                    // COMPRESSION_JPEG2000 as a different value
                                    // must call ours COMPRESSION_AI-JPEG2000.
#define COMPRESSION_JP2000    34712 // Leadtools JPEG2000 - DO NOT USE THIS when saving files!
                                    // It is here because I used this value in early testing
                                    // and also if we ever came across a Leadtools file
                                    // there's a chance we may be able to decode it.
                                    // The #define is taken from libtiff 3.6.0,
                                    // so if we start using that (currently using 3.5.7),
                                    // can remove it from here.
#define TIFFTAG_JPCRATE  65600 // JPEG2000 compression rate tag
                               // This is a virtual tag, so is not saved to file,
                               // but its value must not conflict with any other
                               // real or virtual tags that are used by the component.
                               // For this reason, virtual tags shold have a value
                               // greater than 0xFFFF (to not conflict with real tags)
                               // and different from other virtual tags defined in tiff.h


#endif // AITIFF_H


