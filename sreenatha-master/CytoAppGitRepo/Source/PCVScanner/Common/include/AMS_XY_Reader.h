/*
 *		A M S X Y R E A D E R C O N T R O L L E R . H
 *			AMS XY Reader specific motor control
 */

#ifndef _AMSXYREADERCONTROL_H
#define __AMSXYREADERCONTROL_H

#include "MotorController.h"
#include "Axes.h"
#include "newmotor.h"		//AMS header

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the IMSMOTORCONTROLLER_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// IMSMOTORCONTROLLER_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef AMSXYREADERCONTROLLER_EXPORTS
#define AMSXYREADERCONTROLLER_API __declspec(dllexport)
#else
#define AMSXYREADERCONTROLLER_API __declspec(dllimport)
#endif


// This class is exported from the IMScontrol.dll
class CAMSXYReaderController : public CMotorController
 {
public:
		CAMSXYReaderController();
		~CAMSXYReaderController();

					//general control functions
		int SetUp(int AxisID, int iPortAddr, AxisData *AxisControl);
		int ShutDown();
		int ResetStage (); 
		int DatumFound();

		int GetPosition (int *pos);
		int SetPosition (int pos);
		int SetPosition (float pos);
		int GetPosition (float *pos);
		int GetAbsPosition (int *pos);
		int GetAbsPosition (float *pos);

private :
		BOOL HomingDone();

		int stat;
		BOOL m_Open;
		BOOL m_bSetup;
		int ThisAxisID;		

		int Open(int portno);
		int Close();


		double MXEncScale;
		double MYEncScale;
		int MHomingX;
		int MHomingY;
		int XStart;
		int YStart;
		char MXEncDir[10];
		char MYEncDir[10];

		HANDLE m_hDev;			//device handle
		BOOL m_bFirstTime;
};

	//These 2 functions ONLY are exported by dll
extern "C" AMSXYREADERCONTROLLER_API CMotorController *CreateAMSXYReaderController(void);
extern "C" AMSXYREADERCONTROLLER_API void  DestroyAMSXYReaderController(CMotorController *device);


#endif