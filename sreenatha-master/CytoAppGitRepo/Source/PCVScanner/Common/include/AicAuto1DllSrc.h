//================================================= AicAuto1DllSrc.h
//=================================================

//=================================================

//#include "comincls.h"

//########################## Prototypes from AicAuto1DllSrc.CPP

long __declspec(dllexport) aaCheckPersistent(char *name);
long __declspec(dllexport) aaCloseServer(void);
long __declspec(dllexport) aaDebugVisibility(long newvisibility);
long __declspec(dllexport) aaDoCommand(long command);

DWORD __declspec(dllexport) aaGetDataPL(void *data, DWORD limit);
DWORD __declspec(dllexport) aaGetDataSize(void);

long __declspec(dllexport) aaGetMacroInfo(char *macroname, char *buffer, long limit);
long __declspec(dllexport) aaGetPersistent(char *type, char *name, void *destaddr);

DWORD __declspec(dllexport) aaGetScriptPL(void *script, DWORD limit);
DWORD __declspec(dllexport) aaGetScriptSize(void);

// 10

long __declspec(dllexport) aaGetScriptS(void);

long __declspec(dllexport) aaReceivePicture();
void __declspec(dllexport) aaSendDataPL(void *data, DWORD datalen);
long __declspec(dllexport) aaSendPicture(long hndl);
void __declspec(dllexport) aaSendScriptPL(void *script, DWORD scrlen);
void __declspec(dllexport) aaSendScriptS(char* script);
long __declspec(dllexport) aaServerState(long StateCommand);
long __declspec(dllexport) aaSetExecComLine(void *command, long commandlen, long timeoutinsec);
void __declspec(dllexport) aaSetFeedback(DWORD winhandle, DWORD msg);
long __declspec(dllexport) aaSetPersistent(char *type, char *name, void *valaddr);

// 20

long __declspec(dllexport) aaStartServer(void);
DWORD __declspec(dllexport) aaStopServer(void);

long __declspec(dllexport) aaPrepareMacroCall(char *macroname);
long __declspec(dllexport) aaTakeNextMacroParam(char *sname, void *paraddr);
long __declspec(dllexport) aaGenerateScriptAndExecute();
long __declspec(dllexport) aaGetParamNumInfo(long parnum, long *dtype, long *ddir);
long __declspec(dllexport) aaGetOutputParam(long parnum, void *paraddr, long FreePrevPict);
long __declspec(dllexport) aaCallMacroAndWait(char *macroname, char *pardescr, void **params);
long __declspec(dllexport) aaGetLastMessage();
long __declspec(dllexport) aaGetParamDescription();

// 30

//=================================================
//================================================= AicAuto1DllSrc.h
