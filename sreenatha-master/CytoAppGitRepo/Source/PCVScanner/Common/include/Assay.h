// Assay.h: interface for the CAssay class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ASSAY_H__7B6136B9_C635_41FD_878F_5DE43AEC002D__INCLUDED_)
#define AFX_ASSAY_H__7B6136B9_C635_41FD_878F_5DE43AEC002D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define ASSAYVERSION_PRE72  108
#define ASSAYVERSION		109


#ifdef ASSAY_EXPORTS
#define ASSAY_API __declspec(dllexport)
#else
#define ASSAY_API __declspec(dllimport)
#endif

class CScanPass;
class CScanPassList;
class CSpectraChromeList;
class CSerializeXML;

#define ASSAY_TYPE_LEN              64

typedef TCHAR AssayTypeStr[ASSAY_TYPE_LEN];

class ASSAY_API CAssay  
{
public:
	CAssay(AssayTypeStr assayType, int dbNodeType);
	CAssay();
	virtual ~CAssay();
	BOOL    SaveToPath(LPCTSTR assays_filename);
	BOOL    LoadFromPath(LPCTSTR assays_filename, BOOL bXMLFormat = TRUE);
	BOOL	XMLSerialize(CSerializeXML &ar);
	CAssay* duplicate();
	

	
    
	BOOL	IsFluorescent(void);			// Determines whether the assay is a fluorescent one

	int GetVersion(void) { return m_version; }
	


	CString                                  m_Name;                        // The assay name
	CScanPassList*                           m_ScanPassList;                // The list of scan passes
	BOOL                                     m_Modified;
	AssayTypeStr                             m_assayType;
	int                                      m_dbNodeType;
	int										 m_version;
};

#endif // !defined(AFX_ASSAY_H__7B6136B9_C635_41FD_878F_5DE43AEC002D__INCLUDED_)
