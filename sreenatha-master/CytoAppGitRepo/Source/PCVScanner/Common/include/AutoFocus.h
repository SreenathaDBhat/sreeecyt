// AutoFocus.h: interface for the CAutoFocus class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AUTOFOCUS_H__096D211A_AE69_4C37_87EE_91495CB4ED23__INCLUDED_)
#define AFX_AUTOFOCUS_H__096D211A_AE69_4C37_87EE_91495CB4ED23__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MicServer.h"
#include "../Grabber2/GrabServer2/_GrabServer2.h"
#include "GrabDevice.h"
#include <math.h>
#include <float.h>

class CFocusPoint;

#ifdef AUTOFOCUS_EXPORTS
#define AUTOFOCUS_API __declspec(dllexport)
#else
#define AUTOFOCUS_API __declspec(dllimport)
#endif

#define BIG_FOC_STEP				70
#define SMALL_FOC_STEP				15
#define LINES						16
#define SLICES						16
#define PERCENT_DROP				50
#define EPSILON						(1e-06)
#define DIRECTION					0
#define MAX_SAMPLES					30

#define MAX_FOCUS_POINTS			16

class CSpectraChrome;

class AUTOFOCUS_API CAutoFocus  
{
public:

    typedef struct
    {
        double Z;                       // Position at which a measurement has been taken
        double F;                       // The actual focus measurement
    } FocusMeasurement;

	BOOL                                    m_KillFocusFlag;
	HWND                                    m_msgWnd;
	double									m_MaxMeasure;		// Holds the maximum focus reading
	int										m_MaxSamples;		// Max samples to take
    
    CAutoFocus(IMicSrvr* pIMicSrvr, IGrabber* pIGrab, BYTE* capBuf);
	CAutoFocus(HWND msgWnd, IMicSrvr* pIMicSrvr, IGrabber* pIGrab, BYTE* capBuf);
	CAutoFocus(HWND msgWnd, HWND hMainFrame, IMicSrvr* pIMicSrvr, IGrabber* pIGrab, BYTE* capBuf);
	virtual ~CAutoFocus();
    
    void    Init(HWND msgWnd, HWND hMainFrame, IMicSrvr* pIMicSrvr, IGrabber* pIGrab, BYTE* capBuf);
	float   GetCalibratedPosition(void);
	float   GetCalibratedPosition(CSpectraChrome *pCurrentSpec);
	float   GetAtCurrentPosition(CSpectraChrome *pCurrentSpec, int type, int start_from, double Z, BOOL* failed);
    float   MoveToCalibratedPosition(void);
    float   MoveToCalibratedPosition(CSpectraChrome *pSpec);

	double	MoveAndMeasure(float slice);
	BOOL	CurveFit(double z0, double z1, double z2, double f0, double f1, double f2, float *zf);

	BOOL	Focus(double slice_delta, int start_slice, float *pZ, BOOL fullRange = FALSE);

    enum {FromCalibratedStartPoint, FromCurrentPoint, DirectionalFromCurrentPoint};
	enum {Fast, Course, Fine};

    void    FocusMessage(BOOL On);
    void    Capture(void);

private:
	IMicSrvr* m_pIMicSrvr;
	IGrabber* m_pIGrab;
	BYTE	*m_pCapBuf;
	CRect	m_roi;
	float	m_OriginalZ;
	BSTR	m_Zaxis;
	int		m_Iterations;
    HWND    m_hMainFrame;
    HWND    m_hLiveWnd;
    CSyncObjectMutex *m_pAutoFocusMutex;                            // For connection to the grabber
    CSyncObjectEvent *m_pAutoFocusEvent;

    void Swap(FocusMeasurement *pF1, FocusMeasurement *pF2);        // Swap two focus measurements
    BOOL InNoise(FocusMeasurement *pF);                             // Are we in noise ?
};


#endif // !defined(AFX_AUTOFOCUS_H__096D211A_AE69_4C37_87EE_91495CB4ED23__INCLUDED_)
