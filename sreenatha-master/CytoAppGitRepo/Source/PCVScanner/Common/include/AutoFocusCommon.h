
#pragma once

#define FOCUS_THRESHOLD_DEFAULT					0.3            // Default threshold for focusing ops

#define CYTOVISION_FOCUSMEAS_THRESHOLD			4
#define ARIOL_FOCUSMEAS_THRESHOLD				4
#define FLUORESCENT_FOCUSMEAS_THRESHOLD			4

    
typedef enum 
{ 
	Coarse = 1, 
	Fine = 2, 
	UltraFine = 4, 
	SuperUltraFine = 8,
	NearlySuperUltraFine = 16,
	Fast = 32
} FocusOpts;

typedef enum 
{
	FromCalibratedStartPoint, 
	FromCurrentPoint
} FocusStartPoint;

typedef enum 
{
	Trend, 
	Envelope
} FocusMethod;					// Method used for focus

typedef enum
{
	SaturatedLowEnd,
	SaturatedHighEnd,
	NotSaturated
} FocusImageInf;

typedef enum
{
	AtBottomOfRange,
	InMiddleOfRange,
	AtTopOfRange
} FocusReturnStatus;

#define MAX_FOCUS_SAMPLES 40
#define FINE_FOCUS_ITERATIONS	11
#define ULTRA_FOCUS_ITERATIONS	7
