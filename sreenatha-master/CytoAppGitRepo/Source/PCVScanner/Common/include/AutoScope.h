/*
 *		 A U T O S C O P E S . H
 *			Definitions specific to automated microscopes 
 *
 *	Mods
 *	AR 24Jun05	Change default XY screw pitch from 1000 to 2000
 *	AR 19Sept03	Added Mic_MultiBay to MicroscopeComponents structure for support of multibay stages
 *	AR 3Sept03	Added support for Proscan controller command set
 */

#ifndef _AUTOSCOPES_H
#define _AUTOSCOPES_H

#include <tchar.h>
#include "commondefs.h"
#include "Axes.h"

#define BAY_PITCH		28000		/*default bay pitch*/

/* Default wheel information */
#define MAX_WHEELS		10	/* Absolute MAX expected */
#define MAX_ELEMENTS	20	/* Absolute MAX (filters or objectives)expected */

#define EXCITATION		0
#define TRANSMISSION	1
#define OBSERVATION		2
#define DICHROIC		3
#define NDFILTERS		4

/* Default jog speed (percentage of maximum axis speed)*/
#define JOGLOSPEED		10
#define JOGHISPEED		90

//////////////////////////////////////////////////////////
// Set Condenser Positions For Readability May Need        
// Translation Into Manu Specific Condenser Positions      
//////////////////////////////////////////////////////////

#define CONDENSER_IN                    0       
#define CONDENSER_OUT                   1
#define CONDENSER_UNDEFINED             2

// Same For Shutter

#define SHUTTER_OPEN                    0
#define SHUTTER_CLOSED                  1
#define SHUTTER_UNDEFINED				2

#define MICROSCOPE_LOCKED		TRUE
#define MICROSCOPE_UNLOCKED		FALSE

// Observation Light Path

#define LPC_UNKNOWN						0
#define LPC_EYEPIECE					1
#define LPC_TV							2
#define LPC_PHOTO						3
#define LPC_EYETV						4
#define LPC_EYEPHOTO					5
#define LPC_TVPHOTO						6
// Constant Defines A Disabled Limit
#define LIMIT_DISABLED                  -1



#define UNLOAD_DIST			11000	// Microns

#define THUMB	0
#define BASE	1
#define CONFIG	2
#define ACTIVE	3


			//Fluorescent (XCITE) Unit Status Table
#define FL_LOCK			0x20
#define FL_LAMPREADY	0x10
#define FL_HOME			0x08
#define FL_SHUTTER		0x04
#define FL_LAMP			0x02
#define FL_ALARM		0x01



// Identification codes for microscope components (or 'axes')
// - also used as message codes from COM Server.
// DO NOT change the existing numerical values of components, otherwise the meaning of
// existing configuration files will be scrambled (new values can be added though)!
// If a new component is added, be sure to add code to read it from the configuration file
// (add code to CMicroscopeControl::ReadParametersFromFile()), though there's no need to
// add new code to write it to the configuration file - this will be handled automatically.
typedef enum
{
	Mic_None = 0,
	// XYZ stage
	Mic_Stage_X  = 1,							//must be first in list
	Mic_Stage_Y,
	Mic_Stage_Z,
	//Mic_Read_X,									//XY Reader
	//Mic_Read_Y,
	Mic_Stage_End,								//must be last in list

	// Filter wheels/blocks
	Mic_FilterBlock_Excitation = 10,		//must be first in list
	Mic_FilterBlock_Transmission,
	Mic_FilterBlock_Emission,
	Mic_FilterBlock_Observation,
	Mic_FilterBlock_Dichroic,
	Mic_FilterBlock_End,						//must be last in list

	// Auxiliary equipment (just a slideloader, but who knows there may be other things in the future ....)
	Mic_Slideloader = 20,					//must be first in list
	Mic_MultiBay,
	Mic_Oiler,                              // Genetix Oiler
	Mic_Auxiliary_End,						//must be last in list

	// Microscope components
	Mic_Objectives = 30,					//must be first in list
	Mic_FL_Shutter,
	Mic_Condenser,
	Mic_Lamp,								//brightfield lamp
	Mic_Field_Diaphragm,
	Mic_Aperture_Diaphragm,
	Mic_Lightpath,							
	Mic_ND_Filters,					
	Mic_FL_Lamp,							//fluorescent lamp
	Mic_BF_Shutter,                         // BrightField shutter
	Mic_Barcode_Reader,                     // Barcode Reader
	Mic_FL_Field_Diaphragm,					// Fluorescent field diaphragm
	Mic_All,								//all microscope axes
	Mic_End									//must be last in list

} MicroscopeComponents;
			
// Controller types, identifying all types of motor controller supported
// DO NOT change the existing numerical values of these types, otherwise the meaning
// of existing configuration files will be scrambled (new values can be added though)!
// Note that some of the types use the same CMotorController derived oject, but they
// are still defined individually so that users are able to pick the exact hardware
// they have at configuration time (plus there may be idiosyncracies between individual
// scopes that use the same hardware).
// NOTE CTRL_SLIDELOADER refers to the SL50,CTRL_GENETIX_FUSION refers to the GLS120
//
typedef enum
{
	CTRL_NONE                   = 0,
	CTRL_IMS                    = 1,
	CTRL_LEICADMRXA             = 2,
	CTRL_ZEISSAXIOPLAN2         = 3,
	CTRL_NIKONECLIPSE1000       = 4,
	CTRL_OLYMPUSAX70            = 5,
	CTRL_SLIDELOADER            = 6,
	CTRL_OLYMPUSBX61            = 7,
	CTRL_MANUAL                 = 8,
	CTRL_PROSCAN                = 9,
	CTRL_CRI                    = 10,
	CTRL_XCITE120PC             = 11,
	CTRL_LEICADMRXA2            = 12,
	CTRL_NIKON90I               = 13,
	CTRL_ZEISSAXIOVERT100M      = 14,
	CTRL_ZEISSAXIOVERT200       = 15,
	CTRL_ZEISSAXIOPLAN2_IMAGING = 16,
	CTRL_ZEISSAXIOSKOP2         = 17,
	CTRL_LEICADMRA              = 18,
	CTRL_LEICADMRA2             = 19,
	CTRL_ZEISSAXIOIMAGER        = 20,
	CTRL_LEICADM4000            = 21,
	CTRL_LEICADM5000            = 22,
	CTRL_LEICADM6000            = 23,
	CTRL_PSEUDO                 = 24,
	CTRL_GENETIXOILER           = 25,
	CTRL_GENETIX_FUSION			= 26,	//XY stage and Slideloader
	CTRL_AMSXYREADER			= 27,
	CTRL_GENETIX_GSL10			= 28,	//XY stage and Slideloader
	CTRL_BARCODE_READER			= 29,	//Barcode Reader
	CTRL_LEICADM6000SI          = 30,
	NCONTROLLERS,	// the number of different types of controller catered for

} ControllerType;

// Motor controller descriptive names, to be aligned with the ControllerType enum above
typedef struct _controllerlabels
{
	ControllerType CtrlType;
	LPTSTR name;

} ControllerLabels;

// This is used as a look-up table, so items can be in any order - however,
// keep the names alphabetical, so they are easier to find when glancing
// through, plus they will be used in this order in the configuration app.
static ControllerLabels ControllerNames[] = {

	CTRL_NONE,                   _T("None"),
	//CTRL_CRI,                    _T("CRI Filter"),
	CTRL_IMS,                    _T("IMS"),
	/*CTRL_LEICADMRXA,             _T("Leica DMRXA"),
	CTRL_LEICADMRA,              _T("Leica DMRA"),
	CTRL_LEICADMRXA2,            _T("Leica DMRXA2"),
	CTRL_LEICADMRA2,             _T("Leica DMRA2"),*/
	CTRL_LEICADM4000,            _T("Leica DM4000"),
	CTRL_LEICADM5000,            _T("Leica DM5000"),
	CTRL_LEICADM6000,            _T("Leica DM6000"),
	CTRL_MANUAL,                 _T("Manual"),
	/*CTRL_NIKONECLIPSE1000,       _T("Nikon Eclipse 1000"),
	CTRL_NIKON90I,               _T("Nikon 90i"),
	CTRL_OLYMPUSAX70,            _T("Olympus AX70"),*/
	CTRL_OLYMPUSBX61,            _T("Olympus BX61"),
	CTRL_PROSCAN,                _T("Proscan"),
	//CTRL_SLIDELOADER,            _T("SL50 Slide Loader"),
	CTRL_XCITE120PC,             _T("X-Cite 120 PC"),
	/*CTRL_ZEISSAXIOPLAN2,         _T("Zeiss Axioplan 2"),
	CTRL_ZEISSAXIOPLAN2_IMAGING, _T("Zeiss Axioplan 2 imaging"),
	CTRL_ZEISSAXIOVERT100M,      _T("Zeiss Axiovert 100M"),
	CTRL_ZEISSAXIOVERT200,       _T("Zeiss Axiovert 200"),
	CTRL_ZEISSAXIOSKOP2,         _T("Zeiss Axioskop 2"),*/
	CTRL_ZEISSAXIOIMAGER,        _T("Zeiss AxioImager"),
	CTRL_GENETIXOILER,           _T("Oil Dispenser"),
	CTRL_GENETIX_FUSION,		 _T("Genetix Stage/SL120"),
	CTRL_AMSXYREADER,			 _T("AMS XY Reader"),			
	CTRL_GENETIX_GSL10,		     _T("Genetix Stage/GSL10"),
	CTRL_BARCODE_READER,           _T("Barcode Reader"),
	CTRL_LEICADM6000SI,            _T("Leica DM6000 serial"),
	CTRL_NONE,                   _T('\0'),
};

			//Default data for IMS and Proscan motor controllers
			//These data define the character of the coordinate axis system for the application
			//AxisID : ID number for the axis
			//ShortName : char name of the axis (e.g. 'X')
			//MicroStepSize : minimum step size of the motor
			//MoveDirection : defines the sense of the axis (for the application) w.r.t the motor's natural sense
			//				if MoveDirection is 1, the sense is unalterd, if -1 the sense is reversed
			//Home Direction : direction the axis should move to its home position
			//HiSpeed : (initial) speed at which the axis is moved to the home position 
			//LoSpeed : (final) speed at which the axis is moved to the home position
			//StepBackSize : distance (in microns) to move the axis when stepping back before moving to the home position
			//StepBackDirection : direction in which to step back before moving to the home position
			//CalibrationAxisAlignment : difference in sense between the motor's coordinate axis and the coordinate axis of calibration slide
			//				if CalibrationAxis is 1, the sens is the same, if -1 the sense is reversed 
typedef struct _imsdata {
						MicroscopeComponents AxisID;
						char ShortName;			
						double MicroStepSize;	
						int ScrewPitch;			
						int MoveDirection;		
						int HomeDirection;		
						int HiSpeed;
						int LoSpeed;
						int StepBackSize;
						int StepBackDirection;
						} IMSData;

static IMSData IMSControllerData[] = {Mic_Stage_X, 'X', 0.3125, 2000, 1, 1, 10000, 200, 1000, -1,
										Mic_Stage_Y,'U',0.3125, 2000, -1, 1, 10000, 200, 1000, -1,
										Mic_Stage_Z, 'Z',0.625, 100, 1, 0, 10000, 2000, 1000, -1,
										Mic_FilterBlock_Excitation, 'Q', 1.0, 1, 1, 1, 1000, 200, 1000, -1,
										Mic_FilterBlock_Transmission, 'W', 1.0, 1, 1, 1, 1000, 200, 1000, -1,
										Mic_None, '0',0.0, 0, 0, 0, 0, 0, 0, 0,
										};

static IMSData ProscanControllerData[] = {Mic_Stage_X, 'X', 0.04, 2000, -1, 1, 10000, 200, 1000, 1,
										Mic_Stage_Y,'Y',0.04, 2000, 1, 1, 10000, 200, 1000, 1,
										Mic_Stage_Z, 'Z',0.04, 100, 1, 0, 10000, 2000, 1000, -1,
										Mic_FilterBlock_Excitation, '1',1.0, 1, 1, 0, 1000, 200, 1000, -1,
										Mic_FilterBlock_Transmission, '2',1.0, 1, 1, 0, 1000, 200, 1000, -1,
										Mic_None, '0',0.0, 0, 0, 0, 0, 0, 0, 0,
										};
static IMSData GenetixControllerData[] = {Mic_Stage_X, 'X', 0.03, 1000, 1, 1, 10000, 200, 1000, 1,
										Mic_Stage_Y,'Y',0.03, 1000, -1, 1, 10000, 200, 1000, 1,
										Mic_None, '0',0.0, 0, 0, 0, 0, 0, 0, 0,
										};
static IMSData AMSXYReaderData[] = {Mic_Stage_X, 'X', 0.0, 0, 1, 1, 0, 0, 0, 0,
										Mic_Stage_Y,'Y',0.0, 0, -1, 1, 0, 0, 0, 0,
										Mic_None, '0',0.0, 0, 0, 0, 0, 0, 0, 0,
										};


			//Default (MDS2) filter wheel positions of BX61
typedef struct _bx61data {
										MicroscopeComponents AxisID;
										int FWpos;
										} BX61Data;

static BX61Data BX61ControllerData[] = {Mic_FilterBlock_Excitation, 2,
																	Mic_FilterBlock_Transmission, 1,
																	Mic_FilterBlock_Observation, 3,
																	};


// Descriptions of microscope axes for use in microscope.mic file and in user interface.
struct AxisDescription
{
	MicroscopeComponents AxisID;
	LPTSTR configName; // This is used as the section header in the microscope.mic file, i.e. is machine readable.
	LPTSTR description; // This is default name displayed in the GUI, if the resource strings haven't been loaded.
	int resourceStringID; // TODO: allows internationalisation
};

static AxisDescription AxisNames[] = {
	// Note that the description both for the X and Y drives is 'XY Stage' because they are linked.
	Mic_Stage_X,                  _T("XDrive"),                  _T("XY Stage"),             0,
	Mic_Stage_Y,                  _T("YDrive"),                  _T("XY Stage"),             0,
	Mic_Stage_Z,                  _T("ZDrive"),                  _T("Z Drive"),              0,
	Mic_FilterBlock_Excitation,   _T("FilterBlockExcitation"),   _T("Excitation Filters"),   0,
	Mic_FilterBlock_Transmission, _T("FilterBlockTransmission"), _T("Transmission Filters"), 0,
	Mic_FilterBlock_Emission,     _T("FilterBlockEmission"),     _T("Emission Filters"),     0,
	Mic_FilterBlock_Observation,  _T("FilterBlockObservation"),  _T("Observation Filters"),  0,
	Mic_FilterBlock_Dichroic,     _T("FilterBlockDichroic"),     _T("Dichroic Filters"),     0,
	Mic_Slideloader,              _T("SlideLoader"),             _T("Slide Loader"),         0,
	Mic_Objectives,               _T("Objectives"),	             _T("Objectives"),           0,
	Mic_FL_Shutter,               _T("FluorescentShutter"),      _T("Fluorescent Shutter"),  0,
	Mic_Condenser,                _T("Condenser"),               _T("Condenser"),            0,
	Mic_Lamp,                     _T("Lamp"),                    _T("Lamp"),                 0,
	Mic_Field_Diaphragm,          _T("FieldDiaphragm"),          _T("Field Diaphragm (Bf)"),      0, //brightfield  
	Mic_Aperture_Diaphragm,       _T("ApertureDiaphragm"),       _T("Aperture Diaphragm"),   0, //brightfield 
	Mic_Lightpath,                _T("LightPath"),               _T("Light Path"),           0,
	Mic_ND_Filters,               _T("NDFilters"),               _T("Condenser Filters"),    0, // Config name is still NDFilters for backwards compatibility
	Mic_FL_Lamp,                  _T("FluorescentLamp"),         _T("Fluorescent Lamp"),     0,
	Mic_BF_Shutter,               _T("BrightfieldShutter"),      _T("Brightfield Shutter"),  0,
	Mic_Oiler,                    _T("Oiler"),                   _T("Oiler"),                0,
	Mic_Barcode_Reader,           _T("BarcodeReader"),           _T("BarcodeReader"),        0,
	Mic_FL_Field_Diaphragm,		  _T("FieldDiaphragm_FL"),		 _T("Fl. Field Diaphragm"),	 0,		//fluorescent
	//Mic_Read_X,					  _T("XRead"),					 _T("XY Reader"),			 0,	
	//Mic_Read_Y,					  _T("YRead"),					 _T("XY Reader"),			 0,		
	Mic_None,                     _T("None"),                    _T("None"),                 0  // Must be last entry
};

// Microscope capabilities
// These codes are used when sending or receiving axis data from the MicServer.
typedef enum
{
	CAP_CLEAR = -1, // Clear the data at the current position
	CAP_DONE = 0, // Have finished sending capabilities for this axis
	CAP_AXISID = 1,
	CAP_CTRLTYPE,
	CAP_COMPORT,
	CAP_NAME,
	CAP_NBAYS,
	CAP_JOGLOSPEED,
	CAP_JOGHISPEED,
	CAP_IMS_SCREWPITCH,
	CAP_IMS_SHORTNAME,
	CAP_IMS_MICROSTEPSIZE,
	CAP_IMS_MOVEDIR,
	CAP_IMS_HOMEDIR,
	CAP_IMS_HISPEED,
	CAP_IMS_LOSPEED,
	CAP_IMS_STEPBACKDIR,
	CAP_IMS_STEPBACKSIZE,
	CAP_ROTOR_NELEMENTS,
	CAP_ROTOR_HOMEOFFSET,
	CAP_ROTOR_STEPSPERREV,
	CAP_ROTOR_ELEMENTNAME,
	CAP_BX61_FWPOS,
	CAP_UPDATED,
	CAP_OPENEDOK,

} CapabilityTypes;

struct MicConfig
{
// Note that in many places in the code, MicConfig structures are initialised
// simply by using memset() to set everything to zero. Therefore the default
// value for all the variables is zero.
	MicroscopeComponents AxisID;  // Was: int AxisID;
	ControllerType CtrlType;      // Was: int CtrlType;
	int ComPort; // -1 for USB
	TCHAR Name[256];
	DriveData drive_data;
	RotorData rotor_data;
	BOOL updated;  // This flag indicates to the MicServer that the data in
	               // this structure is new or has been modified in some way.
	               // It is reset once the updated data has been copied
	               // (into an AxisConfig structure) by the MicServer.
	BOOL openedOK; // This flag is set by the MicServer when a connection has
	               // been sucessfully made to the hardware for this component.

	BOOL activated;        // This used to be read from a COMPONENT structure (see SpriteTypes.h)
	BOOL configured;       // This used to be read from a COMPONENT structure (see SpriteTypes.h)
	MicConfig *pCompanion; // This used to be read from a COMPONENT structure (see SpriteTypes.h)
};

// Microscope options, these are all the axes each type of controller may control
struct Options
{
	ControllerType CtrlType;
	int Axes[30]; 
};

static Options MicroscopeOptions[] = {

	//CTRL_CRI, {Mic_FilterBlock_Transmission,-1},
	CTRL_IMS, {Mic_Stage_X, Mic_Stage_Y, Mic_Stage_Z, Mic_FilterBlock_Excitation,
	           Mic_FilterBlock_Transmission, -1},

	/*CTRL_LEICADMRXA, {Mic_Stage_Z, Mic_FilterBlock_Dichroic, Mic_Objectives, Mic_FL_Shutter,
	                  Mic_Condenser, Mic_Lamp, Mic_Field_Diaphragm, Mic_Aperture_Diaphragm, -1},
	CTRL_LEICADMRA, {Mic_Stage_Z, Mic_FilterBlock_Dichroic, Mic_Objectives, Mic_FL_Shutter,
	                 Mic_Condenser, Mic_Lamp, Mic_Field_Diaphragm, Mic_Aperture_Diaphragm, -1},
	CTRL_LEICADMRXA2, {Mic_Stage_Z, Mic_FilterBlock_Dichroic, Mic_Objectives, Mic_FL_Shutter,
	                   Mic_Condenser, Mic_Lamp, Mic_Field_Diaphragm, Mic_Aperture_Diaphragm, -1},
	CTRL_LEICADMRA2, {Mic_Stage_Z, Mic_FilterBlock_Dichroic, Mic_Objectives, Mic_FL_Shutter,
	                  Mic_Condenser, Mic_Lamp, Mic_Field_Diaphragm, Mic_Aperture_Diaphragm, -1},*/

	CTRL_LEICADM4000, {Mic_FilterBlock_Dichroic, Mic_FL_Shutter, Mic_Condenser, Mic_Lamp,
	                   Mic_Field_Diaphragm, Mic_Aperture_Diaphragm, Mic_BF_Shutter, -1},
	CTRL_LEICADM5000, {Mic_FilterBlock_Dichroic, Mic_FL_Shutter, Mic_Condenser, Mic_Lamp,
	                   Mic_Field_Diaphragm, Mic_Aperture_Diaphragm, Mic_BF_Shutter, -1},
	CTRL_LEICADM6000, {Mic_Stage_Z, Mic_FilterBlock_Dichroic,
	                   Mic_Objectives, Mic_FL_Shutter, Mic_Condenser, Mic_Lamp,
	                   Mic_Field_Diaphragm, Mic_Aperture_Diaphragm, Mic_BF_Shutter,Mic_FL_Field_Diaphragm, -1},
	CTRL_LEICADM6000SI, {Mic_Stage_Z, Mic_FilterBlock_Dichroic,
	                   Mic_Objectives, Mic_FL_Shutter, Mic_Condenser, Mic_Lamp,
	                   Mic_Field_Diaphragm, Mic_Aperture_Diaphragm, Mic_BF_Shutter,Mic_FL_Field_Diaphragm, -1},


	CTRL_MANUAL, {Mic_Objectives,Mic_FilterBlock_Dichroic, -1},

	/*CTRL_NIKONECLIPSE1000, {Mic_Stage_Z, Mic_FilterBlock_Dichroic, Mic_Objectives, Mic_FL_Shutter,
	                        Mic_Condenser, Mic_Lamp, Mic_Field_Diaphragm, Mic_Aperture_Diaphragm, -1},
	CTRL_NIKON90I, {Mic_Stage_Z, Mic_FilterBlock_Dichroic, Mic_Objectives, Mic_FL_Shutter,
	                Mic_Lamp, Mic_Field_Diaphragm, Mic_Aperture_Diaphragm, Mic_ND_Filters, -1},

	CTRL_OLYMPUSAX70, {Mic_Stage_Z, Mic_FilterBlock_Dichroic, Mic_Objectives, Mic_FL_Shutter,
	                   Mic_Condenser, Mic_Lamp, Mic_Field_Diaphragm, Mic_Aperture_Diaphragm, -1},*/
	CTRL_OLYMPUSBX61, {Mic_Stage_Z, Mic_FilterBlock_Excitation, Mic_FilterBlock_Transmission,
	                   Mic_FilterBlock_Observation, Mic_FilterBlock_Dichroic, Mic_Objectives, Mic_FL_Shutter,
	                   Mic_Condenser, Mic_Lamp, Mic_Field_Diaphragm, Mic_Aperture_Diaphragm, Mic_ND_Filters, -1},

	CTRL_PROSCAN, {Mic_Stage_X, Mic_Stage_Y, Mic_Stage_Z, Mic_FilterBlock_Excitation,
	               Mic_FilterBlock_Transmission, -1},
	CTRL_SLIDELOADER, {Mic_Slideloader, -1},
	CTRL_XCITE120PC, {Mic_FL_Lamp,-1},

	CTRL_ZEISSAXIOPLAN2, {Mic_Stage_Z, Mic_FilterBlock_Dichroic, Mic_Objectives, Mic_FL_Shutter,
	                      Mic_Condenser, Mic_Lamp, Mic_Field_Diaphragm, Mic_Aperture_Diaphragm,
	                      Mic_FilterBlock_Transmission, -1},
	//CTRL_ZEISSAXIOVERT100M,	{Mic_Stage_Z, Mic_FilterBlock_Dichroic, Mic_Objectives, Mic_FL_Shutter,
	//                      Mic_Condenser, Mic_Lamp, Mic_Field_Diaphragm, Mic_Aperture_Diaphragm,
	//                      Mic_FilterBlock_Transmission, -1},
	//CTRL_ZEISSAXIOVERT200,	{Mic_Stage_Z, Mic_FilterBlock_Dichroic, Mic_Objectives, Mic_FL_Shutter,
	//                      Mic_Condenser, Mic_Lamp, Mic_Field_Diaphragm, Mic_Aperture_Diaphragm,
	//                      Mic_FilterBlock_Transmission, -1},
	//CTRL_ZEISSAXIOPLAN2_IMAGING,  {Mic_Stage_Z, Mic_FilterBlock_Dichroic, Mic_Objectives, Mic_FL_Shutter,
	//                      Mic_Condenser, Mic_Lamp, Mic_Field_Diaphragm, Mic_Aperture_Diaphragm,
	//                      Mic_FilterBlock_Transmission, Mic_FilterBlock_Excitation, -1},
	//CTRL_ZEISSAXIOSKOP2,  {Mic_Stage_Z, Mic_FilterBlock_Dichroic, Mic_Objectives, Mic_FL_Shutter,
	//                      Mic_Condenser, Mic_Lamp, Mic_Field_Diaphragm, Mic_Aperture_Diaphragm,
	//                      Mic_FilterBlock_Transmission, -1},
	CTRL_ZEISSAXIOIMAGER, {Mic_Stage_Z, Mic_FilterBlock_Dichroic, Mic_Objectives,
	                      Mic_Condenser, Mic_Lamp, Mic_Field_Diaphragm, Mic_Aperture_Diaphragm,
	                      Mic_FilterBlock_Transmission, Mic_ND_Filters,Mic_BF_Shutter, Mic_FL_Shutter, -1},
	CTRL_GENETIXOILER,		{Mic_Oiler,-1},
	CTRL_GENETIX_FUSION,	{Mic_Stage_X, Mic_Stage_Y,Mic_Slideloader,Mic_Oiler,-1},
	CTRL_AMSXYREADER,		{Mic_Stage_X, Mic_Stage_Y,-1},
	CTRL_GENETIX_GSL10,		{Mic_Stage_X, Mic_Stage_Y,Mic_Slideloader,Mic_Oiler,Mic_Barcode_Reader,-1},
	CTRL_NONE, {-1},			//must be last entry
};


typedef struct _errorcodedata {
												int CtrlType;
												int AxisID;
												int ErrorCode;
												} ErrorCodeData;

					//Applied Imaging Calibration Slide
typedef struct _calibration_slide {
												int FeatureID;
												int Offset_x;
												int Offset_y;
												int Offset_z;
												LPTSTR label;
												} CalibrationSlide;

					//Applied Imaging Calibration Slide  -- Feature IDs
typedef enum {
						Point_A = 0,
						Point_B,
						Point_C,
						Point_1,
						Point_2,
						Point_3,
						Point_4,
						Pattern_4,
						Pattern_32,
						Pattern_256,
						CrossHair_1,
						CrossHair_2,
						CrossHair_3,
						ClearSpace,
						Point_Oiler,
						Feature_End,
						} CalibrationFeatureID;

					//Applied Imaging Calibration Slide  -- Position of features relative to Point_A
static CalibrationSlide AII_CalibrationSlide[]  = {
																	Point_A, 0, 0, 0,_T("Point_A"),
																	Point_B, 21300, 8500, 0, _T("Point_B"),
																	Point_C, -1900, 42500, 0, _T("Point_C"),
																	Point_1, 3000, 18500, 0, _T("Point_1"),
																	Point_2, 16500, 18500, 0,_T("Point_2"),
																	Point_3, 16500, 44500, 0,_T("Point_3"),
																	Point_4, 3000, 44500, 0, _T("Point_4"),
																	Pattern_4, 9500, 40000, 0,_T("Pattern_4"),
																	Pattern_32, 9500, 22000, 0,_T("Pattern_32"),
																	Pattern_256, 9500, 31000, 0, _T("Pattern_256"),
																	CrossHair_1, 800, 23240, 0,_T("CrossHair_1"),
																	CrossHair_2, 800, 39250, 0,_T("CrossHair_2"),
																	CrossHair_3, 9800, 7000, 0,_T("CrossHair_3"),
																	ClearSpace,	14000, 10500, 0,_T("ClearSpace"),
																	Point_Oiler, -14000, 10500, 0,_T("Point_Oiler"),
																	-1, 0, 0, 0, NULL	
				};


	// Stage types
typedef enum {
				UnknownSetup = 0,
				FullyManual = 1,
				ManualWithXyReader = 2,
				MotorisedXYManualZ = 3,
				FullyMotorised = 4
				} StageType;


	//Scanning Default Settings
typedef struct _sds_short {
						BOOL Set;
						int data;
						} SDS_short;
typedef struct _sds_long {
						BOOL Set;
						long data;
						} SDS_long;
typedef struct _sds_double {
						BOOL Set;
						double data;
						} SDS_double;

typedef struct _scanning_default_setting {
							char RegistryKey[256];
							SDS_short short_data;
							SDS_long long_data;
							SDS_double double_data;
							} SDS;	

static SDS IMS_SDD[] = {"ScanDelay",{FALSE,0},{TRUE,0},{FALSE,0.0},
						"FocusLongDist",{FALSE,0},{FALSE,0},{TRUE,400.0},
						"",{FALSE,0},{FALSE,0},{FALSE,0.0}
						};

static SDS SL120_SDD[] = {"ScanDelay",{FALSE,0},{TRUE,50},{FALSE,0.0},
							"FocusLongDist",{FALSE,0},{FALSE,0},{TRUE,600.0},
							"",{FALSE,0},{FALSE,0},{FALSE,0.0}
							};

static SDS GSL10_SDD[] = {"ScanDelay",{FALSE,0},{TRUE,50},{FALSE,0.0},
							"FocusLongDist",{FALSE,0},{FALSE,0},{TRUE,600.0},
							"",{FALSE,0},{FALSE,0},{FALSE,0.0}
							};
#endif
