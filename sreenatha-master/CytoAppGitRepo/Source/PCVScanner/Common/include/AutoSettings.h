//=================================================================================
// AutoSettings.h
// Base class for autosettings control
//
// History:
//
// 23Mar00	WH:	original
//=================================================================================
#ifndef __AUTOSETTINGS_H_
#define __AUTOSETTINGS_H_

/* 
 * Incremental change magnitudes. The lower offset values are much better at avoiding obvious oscillations with
 * black clipping. These values are used to try to "pull the image into view" when its clipping, and the 
 * calculations can't be made. 
*/
#define STEPS           30
#define INVALID_OFFSET  -12345
#define INVALID_GAIN    -12345


#define OFFSETINC       3
#define OFFSETDEC       2
#define GAINDEC         3

#define SEARCH_LIMIT	17

/* 
 * Maximum amount by which gain and offset are allowed to change. This is really just a precaution again silly 
 * values. 
*/
#define MAXGAINDELTA    20
#define MAXOFFSETDELTA  5

/* 
 * These scale factors translate required adjustments in image values to numerical adjustment of gain and offset 
 * values. They are modified as we go. Fixed point integer math is used to provide higher resolution in the values.
 * Note that we initially start with over-estimated slopes, so that changes will be underestimated at first 
 * (bit of a misnoma because its really the inverse of the slope). 
*/
#define SLOPESCALE      100

#define MINGAINSLOPE    (6*SLOPESCALE)
#define DEFGAINSLOPE    (8*SLOPESCALE)
#define MAXGAINSLOPE    (10*SLOPESCALE)

#define MINOFFSETSLOPE  (9*SLOPESCALE)
#define DEFOFFSETSLOPE  (12*SLOPESCALE)
#define MAXOFFSETSLOPE  (15*SLOPESCALE)

/* Minimum required contrast for exposure calculation. */
#define AUTONOLIGHT     10

/* In order to be confident that we reach the desired contrast, we need to aim for just above it. This value is 
 * how many grey levels over min_contrast we aim for. This is calculated to ensure that at least some gain increment 
 * will occur if we are just one grey level below the required contrast. Works out to be 5 (around to 2% contrast). 
*/
#define OVERESTIMATE    (MAXGAINSLOPE/(2*SLOPESCALE))

/* 
 * These define the shape of the half-gaussian "goodness" curve for gain. The integration "goodness" is linearly 
 * proportional to the integration value. These parameters can be used to adjust the bias toward gain adjustment 
 * or integration adjustment. Generally speaking, gain will be adjusted if gain is less than HALFGOODGAIN, and will
 * always be adjusted if its less than BESTGAIN. Integration will be tweaked if gain increases beyond a point 
 * whereby its "goodness" drops below the integrations "goodness". Because the gain curve is gaussian and the
 * integration is linear, this will favour integration adjustment if the image is very dim (gain and integration 
 * are high) and favour gain adjustment if the image is bright (gain and integration are low). Clever, huh? 
*/
#define BESTGAIN        9	/* 10 */
#define HALFGOODGAIN    26	/* 40 */
#define GOODESTINTEG    90

/* Percentage of area that must be clipped to force drastic reduction in exposure. */
#define	MAXCLIPAREA		5

/* Previous offset, gain and integration values to remember for oscillation check in displayup.c */
#define	AUTOVALS		8

/* Number of oscillations allowed before we give up */
#define MAXOSCILLATIONS		3

// Digital camera range underestimate (to provide stability)
#define RANGEREDUCE	95	// Percentage underestimate for max_range

/* 
 * Seems like the exposure calculation under-estimates... You would think that contrast
 * is proportional to exposure (directly) but it doesn't seem to be...
 * This botch increases the calculated exposure time by:
 *	33% (4/3) if DENOM = 3,
 *	25%	(5/4) if DENOM = 4,
 *	17%	(7/6) if DENOM = 6,
 *	10%	(11/10) if DENOM = 10,
 *	5%  (21/20) if DENOM = 20,
 *	etc.... 
*/
#define	BOTCH_DENOM		8
#define	BOTCH_NUMER		(BOTCH_DENOM + 1)

class CAutoSettings
{
public:
	CAutoSettings();
	void SetDevice(CGrabDevice *grab, CMeasurements *meas);
	void Reset(){ m_reset = TRUE; m_resetND = TRUE;};
	virtual int DoAdjustment(BOOL Inverted = FALSE);

	BOOL BestContrast() {return m_best_contrast;};

	WORD m_expose_step;

	int m_format;


protected:
	CMeasurements	* m_meas;
	CGrabDevice		* m_grab;
	BOOL m_reset;
	BOOL m_resetND;
	UINT m_bitshift_limit;
	UINT m_exposure_limit;
	BOOL m_inverted;
	WORD m_acceptable_contrast;
	BOOL m_best_contrast;
	WORD m_gain_limit;
	WORD m_offset_limit;
	WORD m_AdjustmentFnCode;

private:
	BOOL m_exposure_optimised;

};

class CSlowSearch_AutoSettings : public CAutoSettings
{
public:
	CSlowSearch_AutoSettings();
	~CSlowSearch_AutoSettings();
	int DoAdjustment(BOOL inverted);
	int DoSlowSearchAdjustment(BOOL Invert);
	int DoWhiteBalance(BOOL Invert);
	int DoNeutralDensityAdjustment(BOOL inverted);
	void GetBestSettings(int Channel, int *lpos, int *upos, int lower, int upper);
	int DoExposureAdjustment();
	int DoGainAndOffsetAdjustment(BOOL Invert);


protected:
	enum {	actionGain, 
			actionOffset, 
			actionExpose, 
			actionFinish, 
			maxOptTries = 2, 
			maxGiveup = 200, 
			actionCourse, 
			actionCourseFinish, 
			actionFine, 
			actionFineFinish, 
			estimate1, 
			estimate2, 
			estimate3};

	enum {
		PassIn,
		PassStart,
		PassCoarseGain,
		PassCoarseOffset,
		PassFineGain,
		PassFineOffset,
		PassCheck,
		PassOut
		};

	WORD m_white_target;
	WORD m_black_target;
	int m_Passes;
	int m_delta;
	int m_diff_white;
	int m_diff_black;
	int m_diff;
	int m_min_diff;
	int m_ErrorCode;
	int m_prev_gain;
	int m_prev_offset;
	int m_min_prev_offset;
	int m_min_prev_gain;

	int offset;
	int gain;
	int prev_offset;
	int prev_gain;
	int best_course_offset ;
	int best_course_gain   ;
	int contrast[STEPS][STEPS];
	int min[STEPS][STEPS];
	int max[STEPS][STEPS];
	int m_Steps;
	UINT prev_exposure;

			//colour cameras
	int m_step[4];
	int c_gain[4];
	int c_offset[4];
	int last_offset[4];
	int last_gain[4];
	int c_contrast[4];
	int c_gain_steps[4];
	int m_inc_gain;
	int m_inc_offset;
	int Ch1_lpos[SEARCH_LIMIT][SEARCH_LIMIT];
	int Ch2_lpos[SEARCH_LIMIT][SEARCH_LIMIT];
	int Ch1_upos[SEARCH_LIMIT][SEARCH_LIMIT];
	int Ch2_upos[SEARCH_LIMIT][SEARCH_LIMIT];
	int Ch_Base;
	int Ch_1;
	int Ch_2;

	UINT	m_action;				// action state, assigned actionGain, actionOffset, actionExpose or actionFinish
};

#endif //__AUTOSETTINGS_H_
