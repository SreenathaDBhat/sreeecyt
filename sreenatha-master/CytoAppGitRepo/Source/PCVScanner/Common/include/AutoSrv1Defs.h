//=========================================== AutoSrv1Defs.h

#define StateCompBase  0x100
typedef enum  {
  Neutral = 0,
  RunUTS = 1,
  StopUTS,				//  2
  PutScriptToUTS,		//  3
  AddScriptToUTS,		//  4
  ReadFileToUTS,		//  5
  PicToInQueue,			//  6
  PicFromOutQueue,		//  7
  ClearInQueue,			//  8
  ClearOutQueue,		//  9
  UTSTabloToScript,		// 10
  SetFeedbackInfo,		// 11
  GetMacroInfo,			// 12
  AppMinimize,			// 13
  AppRestore,			// 14
  PutScriptToCommand,	// 15
  ExecuteCommandLine,	// 16
  AddFileToUTS,			// 17

  ServerCurrentState = StateCompBase,
  ServerLastError,	// last error command
  ServerLastCommand,
  ServerScriptLen,
  ServerDataLen,
  ServerInQueueLen,
  ServerOutQueueLen,
  ServerResult,
  ServerIsStopping,
  // ...
  ServerStateLimit,	// not a command but just the limitation

  TestCopy = 0xffffff
} Commands;

#define ServerStateCompMax   (ServerStateLimit-StateCompBase)
#define ServerStateBase  0 // 0x40000000
typedef enum  {
    ServerIsFree = ServerStateBase,
	ServerIsBusy = (ServerStateBase | 1),
    ServerIsAbsent = (ServerStateBase - 1)
} ServerStates;

//WM_LBUTTONDBLCLK WM_LBUTTONDOWN

//=========================================== AutoSrv1Defs.h
