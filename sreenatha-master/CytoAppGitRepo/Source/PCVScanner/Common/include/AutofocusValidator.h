#pragma once


#ifdef FOCUSAUTO_EXPORTS
#define FOCUSAUTO_API __declspec(dllexport)
#else
#define FOCUSAUTO_API __declspec(dllimport)
#endif

class CScriptProcessor;

class FOCUSAUTO_API AutofocusValidator 
{
public:
	AutofocusValidator(double XScale, double YScale);
	~AutofocusValidator(void);
	BOOL Initialise(LPCTSTR ValidatorScript);
	long Validate(WORD *pImage, long Width, long Height);
	void SetValidationParams(int MinArea, int MaxArea, int NumberObjects);

private:
	CScriptProcessor		*m_pSP;
	
};

