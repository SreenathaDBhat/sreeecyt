#ifndef _AXES_H
#define _AXES_H

#include "Types.h"
/*
 *		AXES.H
 *
 *		Axis definitions for microscope axis control
 *
 *	Mods:
 *	AR 3Sept03	Removed WheelInfo structure. This legacy is no longer required.
 */

#define WAIT		TRUE
#define DONT_WAIT	FALSE

#define MIC_CONTROL_REMOTE		1
#define MIC_CONTROL_LOCAL		0

#define DRY		0
#define OIL		1
#define DRY_STRING	_T("Dry")
#define OIL_STRING	_T("Oil")
#define NO_LENS_STRING	_T("No Lens")

#define SLIDELOADER_REMOVE_FLAG		_T("\\defaults\\Slideloaderflag.txt")

typedef enum {
				NO_WHEELS	= 0,		    // No Filter Control
				EXCITATION_WHEEL	= 1,    // Excitation Filter Wheel   (EPI)
				DICHROIC_WHEEL		= 2,    // Observation Filter Wheel  (OBS)
				TRANSMISSION_WHEEL = 4,     // Transmission Filter Wheel (DIA)
				MAX_FILTERWHEEL
				} FILTER_WHEEL;

typedef enum  {
				SENSE_POS = 0,
				LOAD_POS =1,
				PRE_LOAD_POS = 2,
				TRAY_PITCH = 3,
				TRAY_0_POS = 4
				} SL120_SysPos;


/* Command terminators */
#define SINGLE_TERMINATOR	13
#define LINE_FEED			10
#define SINGLEAXIS ' '


			//Rotor information structure
typedef struct {
					int pos;				//position of element on rotor
					LPTSTR name;			//descriptive name for element
					DPoint stage_offset;	//stage offset of element from base element
					int	 lamp_offset;		//lamp intensity offset of element from base element
					int aperture;			//aperture setting for element (objective lens)
					BOOL  base;				//position of base element
					int lenstype;			//lens type (dry (0)or oil (1))
					double lenspower;		//lens power
					int field;
					} RotorInfo;


			//General data structure for axes
typedef struct drivedata {
						char ShortName;				//single character name for axis (eg IMS controller name)
						int ID;						//axis identification number
						int ScrewPitch;				// screw thread pitch of axis screw
						double MicroStepSize;		//axis step size, to convert 1 step from applicaton to ~ 1 micron
						double Resolution;			//minimum increment of axis
						int MoveDirection;			//defines the positive axis direction
						int HomeDirection;			//defines the position of the axis 'home' position (IMS controller)
						int HiSpeed;				//speed at which axis is moved to its home position initially (IMS controller)
						int LoSpeed;				//speed at which axis is moved to its home position (IMS controller)
						int StepBackSize;			//size of step to take prior to moving to home position
						int StepBackDirection;		//direction of step back (IMS controller)
						double step_back;			//step size to take in homing procedure (IMS controller)
						void *portal;				//gateway to drive hardware
						TCHAR FullName[100];			//descriptve name 
						int FullSpeed;				//Maximum speed setting for the axis
						int jogLoSpeed;				//Low speed for jog movement 
						int jogHiSpeed;				//High speed for jog movement
						int NBays;					//Number of bays (on XY stage)
						int ApplyLampOffset;
						}DriveData;

			//Specific data structure for filter wheels, objective noespiece etc,
			//anything that has a number of elements and rotates
typedef struct rotordata{					
						int HomeOffset;					//offset from Home position to first element
						int NElements;					//number of elements
						int StepsPerRev;				//number of steps per revolution (for IMS filterwheels)
						LPTSTR *Elements;				//array of element names (filters or objective lenses)
						double *LensPower;				//IS THIS STILL USED? array of lens power 
						LPTSTR paramfile;				//IS THIS STILL USED? name of addition parameters file (esp. for IMS controllers)
						int CurrentPos;					//current position
						int Initialised;				//flag for intialisation
						DPoint *Stage_Offsets;			//X,Y,Z offsets from base position
						int *IOffsets;					//array of lamp intensity offsets from base position
						int *ApertureSetting;			//array of aperture settings for each objective lens
						int *CondenserSetting;			//array of condenser settings for each objective lense
						int Base;						//element from which stage offsets are calculated (e.g. counterstain)
						DPoint BasePosition;			//X, Y, Z position of base element
						int BaseLampSetting;			//Lamp intensity for base element
						int ApplyOffset;				//flag indicating whether offsets should be applied or not
						int FWpos;						//filter wheel position (FW1, FW2, FW3) for BX61
						int *LensType;					//array of lens type (dry (0) or oil (1))
						int *Fl_FieldSetting;				//array of fluorescent field diaphragm settings for each objective lens
						int *FieldSetting;				//array of brightfield field diaphragm settings for each objective lens
				}RotorData;

				//Holding data structure 
typedef struct axisdata{
						int Type;
						DriveData *drive_data;
						RotorData *rotor_data;
						}AxisData;

				//Axis configuration data structure, used in a linked list
typedef struct axisconfig {
					void *Controller;			//pointer to the controller (CMotorController) object
					TCHAR Label[256];			//configuration file identification label of the axis
					int AxisID;					//idenification number of the axis
					int CtrlType;				//type of controller which controls the axis
					int ComPort;				//comport through which the motor is accessed
					TCHAR AxisName[100];			//descriptive name of the axis
					AxisData Data;				//general and specific data about the axis
					HINSTANCE hDLL;				//handle to the DLL for the motor controller
					BOOL openedOk;				//flag indicating motor controller object was opened ok
					BOOL updated;				//flag indicating axis configuration is new or modified
					struct axisconfig *next;	//next axis in the list	
					struct axisconfig *prev;	//previous axis in the list
					} AxisConfig;


#ifndef MAXBAYS
#define MAXBAYS 16
#endif
#define IDEAL_POINTS		4		//maximum number of 'ideal' calibration points
#define MAXOBJECTIVES		10		//maximum number of objectives expected on a microscope
#define MAXAXES				256		//maximum number of axes that a microscope may be may be configured with

#ifndef UNDEFINED_Z_POSITION
#define UNDEFINED_Z_POSITION    -99999
#endif
					//codes for information requests from microscope COM server
#define NUMBEROFBAYS		1
#define BAYORIGIN			2
#define BACKLASH			3
#define STEPSIZE			4
#define XYSCALE				5	


					//SlideLoader default offsets values for the check and unload positions
					//relative to the load position (units microns).
					//defined in Slide Loader Mechanical Interface Specification (3059-04)
#define X11 2000
#define Y11 2000
#define X12 2000				//unload slide x offset
#define Y12 2000				//unload slide y offset
#define X13 7500				//check for slide x offset
#define Y13	20000				//check for slide y offset


				//Microscope calibration data structure
typedef struct stagecalibrationdata {
						int CurrentBay;								//number of active bay
						Tripoint CheckPos;							//slide loader 'check for slide' position
						Tripoint LoadPos;							//slide loader 'load slide' position
						Tripoint UnloadPos;							//slide loader 'unload slide' position
						Tripoint StartPos;							//start position for slide loader calibration
						int NBays;									//number of bays on the system
						Tripoint BayOrigin[MAXBAYS];				//stage coordinates of bay origins (C59 point)
						DPoint XYscale[MAXBAYS];					//stage xy scale (unit : pixels / stage step)
						DPoint Backlash;							//system backlash (unit :stage step)
						DPoint Stepsize;							//system step size (uinit: microns / stage step)
						CoordFrame StageCalib[MAXBAYS];				//coordinates of ideal calibration points in stage coordinate frame
						CoordFrame Ideal[MAXBAYS];					//coordinates of the ideal calibration points in ideal coordinate frame
						ImageCalib ImageScale[MAXOBJECTIVES];		//image pixel dimensions in microns for each objective
						LensPower ObjectiveLens[MAXOBJECTIVES];		//power of the lenses on the system
						POINT ImagePixels;							//number of pixels in the image used for calibration
						TCHAR EFOrigin[MAXBAYS][10];				//England finder coordinates of bay origins
						int FOVOverlap;								//Length of field of view movement overlap (microns)
						CoordPoint XYLimits0;						//Position of XY limits in ideal coordinates
						CoordPoint XYLimits1;						//Position of XY limits in ideal coordinates
						CoordPoint PointB;
						CoordPoint PointC;
						CoordPoint OilOrigin;						//Oil dispenser datum point in ideal coordinates
						int ZDatumPt[MAXBAYS];						//Z datum for the bay, measured in the centre of the calibration slide
						CoordPoint ZUpperLimit;						//Position of Z upper limit in ideal coordinates.  This is currently only used by Leica DM6000 otherwise set to 0.0000
						} StageCalibrationData;



#define AXISSINGLE	0		//single axis controlled by a particualr type of controller
#define AXISMULTI	1		//multiple axes controlled by a particular type of controller

//*******************************************
		//Axis Names
//*******************************************
				// IMS controllers are always known by the following names 
#define  X_MOTOR_NAME  'X'		//x-axis
#define  Y_MOTOR_NAME  'U'		//y-axis, U is used in case of error in mistaking name for setting current */ 
#define  Z_MOTOR_NAME  'Z'		//z-axis
#define  W_AXIS  'W'			//transmission filter wheel
#define  Q_AXIS  'Q'			//excitation filter wheel	


#define MAXSPEED 100				
#define AXIS_UPDATEPERIOD	750L		//update axis position periodically 
#define AXIS_STEPPERIOD	   333L		
#define MAX_STATUS_TRIES	400

		//Message codes from COM SERVER  -- see also MicroscopeComponents structure in AutoScope.h
#define CODE_MASK 0x0000ffff;


#define MIC_ERROR			100
#define Z_STEP				101
#define CURRENTBAY			102
#define MIC_CAPABILITIES	103
#define MIC_DATA_SAVED		104
#define STAGE_AT_DATUM		105

#define STAGE_MOVING		110

#define SLIDELOADER_INITIALIZEDTRAYS	150
#define SLIDELOADER_SLIDEPOSITIONS		151
#define SLIDELOADER_TRAYPOSITIONS		152
#define SLIDELOADER_BARCODES			153
#define SLIDELOADER_TRAY_TAMPERED		154
#define SLIDELOADER_TRAY_ADDED			155
#define SLIDELOADER_TRAY_REMOVED		156
#define SLIDELOADER_TRAY_REINSTATED		157
#define SLIDELOADER_TRAY_MODIFIED		158
#define SLIDELOADER_TRAY_SLIDE_BLOCKED	159

#define MIC_CALIBRATION					170

#define LAST_CLIENT						180
#define DISABLE_CLIENT					190
#define ENABLE_CLIENT					191

		//Sub codes for information
#define INITIALISED			0
#define HOMED				1
#define MOVED				2


		//Joystick control 
#define JC_STOP			0
#define JC_LOSPEED		1
#define	JC_HISPEED		2
#define JC_POSITIVE		1
#define JC_NEGATIVE		2
#define JC_HOME			3

#endif