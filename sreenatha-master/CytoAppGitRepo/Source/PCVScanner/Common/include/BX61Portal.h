/*
 *		 B X 6 1 P O R T A L . H
 *			Interface functions between motor controller and Olympus BX61 library 
 */

#ifndef _BX61PORTAL_H
#define _BX61PORTAL_H

#include "afx.h"
#include "MalMicroscope.h"
#include "MalTaskCore.h"
#include "malbx.h"

#include "MicroErrors.h"
#include "AutoScope.h"

// Olympus SDK Defines - These Two Specify Whether The Callback 
// Should Be Used To Determine When An Operation Is Complete
// OR Whether The Calling Function Should Wait For The Operation
// To Complete - See SDK Spec. 
#define     SYNC        0
#define     ASYNC       1

// Conversion factor to/from Olympus MAL Function SDK units
// to our microscope units. AI uses 0.1 micrometer steps,
// the SDK uses pico-metre steps !

#define BX61_Z_CONVERT			100000
#define BX61_XY_CONVERT			100000

// Absolute Min and Max Positions For Stage Z In Micrometers
// Specified In AI Driver Units (0.1um). NOTE These Assume
// Than Min Is At The Bottom

// Min Focus Position 1um
#define BX61_MIN_FOCUS_POS      0

// Max Focus Position 15um
#define BX61_MAX_FOCUS_POS      150000

// Set Status of All Components 
#define BX61_ALL_STATUS		-1

// Axes Status
#define STATIONARY			0
#define MOVING				1


/////////////////////////////////////////////////////////////////////////////
// These enums Defined As None Provided By Olympus SDK
/////////////////////////////////////////////////////////////////////////////

// LEDs On Switch Panel
typedef enum 
{
    OBJECTIVE1_LED   =   8,
    OBJECTIVE2_LED,
    OBJECTIVE3_LED,
    OBJECTIVE4_LED,
    OBJECTIVE5_LED,
    OBJECTIVE6_LED,
    OBJECTIVE7_LED,
    OBJECTIVE8_LED,
    OBJECTIVE9_LED,
    OBJECTIVE10_LED,
    POWERON_LED      = 15
} BX61_LED_NAMES;

// Button Numbers On Microscope
typedef enum
{
    CAMERA_BUTTON       = 1,
    OBJECTIVEUP_BUTTON  = 1,
    OBJECTIVEDN_BUTTON  = 2,
    LAMPUP_BUTTON       = 3,
    LAMPDN_BUTTON       = 4,
} BX61_BUTTON_NAMES;

class CBX61Portal
 {
public:
	 CBX61Portal();
	~CBX61Portal();
				//General
	// Methods
	/////////////////////////////////////////////////////////////////////////////
	// General Microscope
	/////////////////////////////////////////////////////////////////////////////
	// Initialise Function Initialise Any Attributes Associated With
	// A Specific Microscope And Also Common Attributes Such As The
	// Number of Filters, Objective Nosepeices etc.
    int MicInitialise(int CommsInterface);
	void SetUp (int AxisID, void *Data);
	// Shut It Down
	int ShutDown();
	// Checkname or Identify ?
	int Identify(char *Name);

    // Remote/Local control
    int RemoteControl(void);
    int LocalControl (void);

    /////////////////////////////////////////////////////////////////////////////
	// X, Y, Z  Drive Functions
	// Driver units 0.1 micron, Olympus BX61 units 1 pico-meter
	/////////////////////////////////////////////////////////////////////////////
	int StopFocus ();
	int WaitFocus ();
//	int GetFocusStatus (int & Status);		
 //   int SetFocusStatus (int Status);		
	
	// Absolute Move Functions
	int MoveFocusAbs (int FocusPos);				
	int GetFocusAbs (int &FocusPos);
	// Set and Get Max and Min Limits of Travel
	int SetFocusMax(int MaxFocusPos);
	int GetFocusMax(int& MaxFocusPos);
	int SetFocusMin(int MinFocusPos);
	int GetFocusMin(int& MinFocusPos); 
    // Relative Movement
	int MoveFocusRel(int FocusOffset);

	/*int SetFocusRel(int FocusOffset);*/
	int GetFocusRel(int& FocusOffset);

	int SetFocusSpeed (int Speed);
	int SetFocusSpeed (float Speed, int *StepsPerSecond);
	int SetZSpeed(double StartSpeed, double Speed);
	int SetSpeedParameter(MAL_MS_MOTORPARAM Parameter, double Speed);

		//Set Movement status
	 int SetMicStatus (MAL_MS_EVENT event,  int Status);	
	/////////////////////////////////////////////////////////////////////////////
    // Filter Block Commands
	// Set or Get A Filter In The Block
	/////////////////////////////////////////////////////////////////////////////
	void GetExcitationData (void *Data);
	void GetTransmissionData (void *Data);
	void GetObservationData (void *Data);
	void GetDichroicData (void *Data);

	int SetExcitationFilter(int FilterNum, int waitmove);
    int GetExcitationFilter(int *FilterNum);
	int WaitExcitation();


	int SetTransmissionFilter (int FilterNum, int waitmove);
    int GetTransmissionFilter(int *FilterNum);
	int WaitTransmission();

	int SetDichroicFilter (int FilterNum, int waitmove);
    int GetDichroicFilter(int *FilterNum);
	int WaitDichroic();

	int SetObservationFilter (int FilterNum, int waitmove);
    int GetObservationFilter(int *FilterNum);
	int WaitObservation();

	// Shutter Control
	int SetShutter(int ShutterPos, int waitmove);
	int GetShutter(int *ShutterPos);
	
	/////////////////////////////////////////////////////////////////////////////
    // Objective Nosepeice
	/////////////////////////////////////////////////////////////////////////////
	void GetObjectiveData (void *Data);
	int SetObjective(int ObjectivePos, int waitmove);
	int GetObjective(int *ObjectivePos);
	int WaitObjectives();

	/////////////////////////////////////////////////////////////////////////////
	// Aperture Diaphragm 0% = Closed, 100% = Fully Open
	/////////////////////////////////////////////////////////////////////////////
	int SetAperture(int Aperture, int waitmove);
	int GetAperture(int* Aperture);
	int WaitAperture();

	/////////////////////////////////////////////////////////////////////////////
	// Field Diaphragm 0% = Closed, 100% = Fully Open
    // N/A For BX61 Manual Operation
	/////////////////////////////////////////////////////////////////////////////
	//int SetField(int Field);
	//int GetField(int& Field);

	/////////////////////////////////////////////////////////////////////////////
	// Condenser Control
	/////////////////////////////////////////////////////////////////////////////
	int SetCondenser(int CondenserPos, int waitmove);
	int GetCondenser(int *CondenserPos);
    int SetCondenserPosition(int CondenserPos, int waitmove);
    int GetCondenserPosition(int *CondenserPos);
	int WaitCondenser();
	int WaitCondenserPosition();

	/////////////////////////////////////////////////////////////////////////////
    // Lamp Control Driver 0% to 100% Olympus 0, 2.5V to 12.0V
	/////////////////////////////////////////////////////////////////////////////
    int SetLamp(int LampValue, int waitmove);
    int GetLamp(int * LampValue);
	int WaitLamp();

			//Microscope component status flags
	struct  {
				int ZStage;
				int Excitation;
				int Dichroic;
				int Transmission;
				int Observation;
				int Objectives;
				int Shutter;
				int Lamp;
				int CondenserTopLens;
				int CondenserPosition;
				int Aperture;
				}MicStatus;	

    // Attributes Defined in Base Class Apply Here So
    // Do Not Define Again. Vars Applicable To The Olympus BX61
    // Can Be Declared Here Though.
protected:

	// Olympus Specific Information and
	// Communications Methods
	int MaxFilters[MAX_WHEELS];				//Maximum number of filters on wheel
	MAL_IOTARGET  FilterWheelPos[MAX_WHEELS];			//FW socket the wheel is connect to

    int MaxObjectives;		// Maximum number of objective lenses on nosepeices

			   // Limits of movement. Set either by software or read from the microscope on initialisation
    int MaxZDrive;                  // Absolute Maximum  Drive Limit
	int MaxZLimit;					// Software/Microscope Set Max Position Limit 
	int MinZLimit;					// Software/Microscope Set Min  PositionLimit
	int ZOffset;							// For Conversion of Microscope Pos To 0 Based Pos In Microscope Steps
//    int ZStageStatus;			   // Current Status Of The Stage Focus

	LONGLONG ZStartSpeed;		//default start speed of Z axis
	LONGLONG ZSpeed;					//default speed of Z axis motor
	LONGLONG ZAcceleration;			//default acceleration of z axis
	LONGLONG ZWaitTime;			//deafult wait time for z axis motor

private :
		   // Translate Olympus Error Codes Into Driver Codes
		int TranslateError(MALRESULT MicroscopeCode);
			// Axis Conversion Driver To Olympus and Vice Versa
		LONGLONG ZDriverToBX61(int Position);
		int ZBX61ToDriver(LONGLONG Position);
			// Update LEDS
		void UpdateLEDS(void);
			// Pointer To SDK Microscope Object
		void* pBX61;
			// Driver Error Code
		int ErrorCode;
			// Local Storage Of Current Observation Mode
		char ObservationMode[20];

			//Microscope intialised flag
		BOOL Initialised;
			//Set default settings
		void DefaultSettings();

			//List of components available on the microscope
		int Component[20];		

		float ZPosition;	

		BOOL m_bRemote;
};


#endif

 