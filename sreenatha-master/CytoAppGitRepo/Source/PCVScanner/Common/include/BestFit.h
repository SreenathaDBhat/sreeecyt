//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Calc the best fit plane through a series of datapoints
// Header
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef BEST_FIT_H

#define BEST_FIT_H

/*
**
**
** The MIT license:
**
** Permission is hereby granted, free of charge, to any person obtaining a copy
** of this software and associated documentation files (the "Software"), to deal
** in the Software without restriction, including without limitation the rights
** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
** copies of the Software, and to permit persons to whom the Software is furnished
** to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all
** copies or substantial portions of the Software.

** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
** WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
** CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/
#ifdef HELPER2_EXPORTS
#define BESTFIT_API __declspec(dllexport)
#else
#define BESTFIT_API __declspec(dllimport)
#endif

bool BESTFIT_API getBestFitPlane(unsigned int vcount,     // number of input data points
                     const double *points,     // starting address of points array.
                     unsigned int vstride,    // stride between input points.
                     const double *weights,    // *optional point weighting values.
                     unsigned int wstride,    // weight stride for each vertex.
                     double *plane);


#endif
