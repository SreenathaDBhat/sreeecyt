
////////////////////////////////////////////////////////////////////////////////////////
// Histogram measurements for spectrachrome white balancing
// Header file
// K Ratcliff
////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#ifdef CAMMANAGED
#undef AFMEASURE_API
#define AFMEASURE_API
#elif defined(HELPER2_EXPORTS)
#define AFMEASURE_API __declspec(dllexport)
#else
#define AFMEASURE_API __declspec(dllimport)
#endif


void AFMEASURE_API BiModalMeasure8(BYTE *pBuf, int size, WORD *LowPeak, WORD *HiPeak);

void AFMEASURE_API BiModalMeasure16(const WORD *pBuf, int Width, int Height, int BitDepth, WORD *LowPeak, WORD *HiPeak);
