/////////////////////////////////////////////////////////////////////////////////////////
// Generate scripts for Spot Counting from a collection of script functions etc.
// Header File
// Written By Karl Ratcliff 07012002
//
/////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#ifndef __BUILDSCRIPT_H
#define __BUILDSCRIPT_H

#include "stdafx.h"

/////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////

// Defines whether we have a spot counting or find script
// Absence means its a find script
#define SPOTCOUNTSCRIPT					"' Spot"

// Defines a spectrachrome
#define CAP_SPECTRACHROME               "' Cap"
#define ASSAYNAME		                "' Assay"

// Defines the start of the constants section for spot measurement
#define CONST_START_STRING              "' START CONST\r\n"
// Defines the end of the constants section
#define CONST_END_STRING                "' END CONST\r\n"

// Start of the spots classifier
#define SPOTSCLASSIFIER_START_STRING    "' START CLASSIFIER\r\n"
// End of the spots classifier
#define SPOTSCLASSIFIER_END_STRING      "' END CLASSIFIER\r\n"

// Carriage return linefeed pair
#define CRLF                            "\r\n"

// String not found
#define STRING_NOT_FOUND                -1

/////////////////////////////////////////////////////////////////////////////////////////
// Class definition
/////////////////////////////////////////////////////////////////////////////////////////

// Generic class builds a script from functions

class CBuildSpotScript {
public:
    CBuildSpotScript();
    ~CBuildSpotScript();

    // Get the script
    void GetScript(CString& TheScript);

    // Load a script into memory
    BOOL LoadScript(LPCSTR FileName);

    // Save a script back to a file
    BOOL SaveScript(LPCSTR FileName);

    // Extracts the classifier function and the constants section from a script
    BOOL Extract(CString& CapSection, CString& ClassifyFunc, CString& ConstSection);

    // Constructs the script after extraction with new classifier and constant values
    BOOL Construct(CString& CapSection, CString& ClassifyFunc, CString& ConstSection);

private:

public:
    // This is the script that is currently being constructed or de-constructed
    CString         *m_pTheScript;
};



#endif