#ifndef __CFUNCS_H__
#define __CFUNCS_H__

#ifdef CAMMANAGED
#define HELPER3_API
#elif defined(HELPER3_EXPORTS)
#define HELPER3_API __declspec(dllexport)
#else
#define HELPER3_API __declspec(dllimport)
#endif

BOOL HELPER3_API GetOSDisplayString( LPTSTR pszOS);
BOOL HELPER3_API GetWinVersion( LPTSTR pszOS);
BOOL HELPER3_API CreateFullPathDirectory(LPCTSTR dir);
BOOL HELPER3_API DeleteDirectoryTree(LPCTSTR dirpath);
BOOL HELPER3_API SetEveryonePermission(LPTSTR FileName);
BOOL HELPER3_API SetFilePermission(LPTSTR FileName, LPTSTR TrusteeName, DWORD AccessMask, DWORD InheritFlag);
int	 HELPER3_API DoSystemCommand(LPCTSTR cmd, LPCTSTR dir);
BOOL HELPER3_API SaveAsJPeg(LPCTSTR path, BITMAPINFO bminfo, BYTE *bits);
BOOL HELPER3_API UserDirectory(LPCTSTR app, LPCTSTR folder, LPTSTR rbuff, int size);
BOOL HELPER3_API GetExecDir(TCHAR path[], size_t size);
BOOL HELPER3_API GetExecDirA(char path[], size_t size);
#if defined __cplusplus && _AFX
CString HELPER3_API GetExecDir();
#endif
void HELPER3_API CleanFileName(LPTSTR FileName, TCHAR Subst);			// Remove illegal chars from a filename and replace with _ 

#endif //__CFUNCS_H__
