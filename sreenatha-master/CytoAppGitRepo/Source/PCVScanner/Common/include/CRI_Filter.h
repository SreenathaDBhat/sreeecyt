/*
 *		C R I _ F I L T E R . H
 *			CRI Filter specific control
 */

#ifndef _CRICONTROL_H
#define _CRICONTROL_H

#include "MotorController.h"
#include "Axes.h"
#include "MMTimer.h"

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the IMSMOTORCONTROLLER_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// IMSMOTORCONTROLLER_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef CRICONTROLLER_EXPORTS
#define CRICONTROLLER_API __declspec(dllexport)
#else
#define CRICONTROLLER_API __declspec(dllimport)
#endif


// This class is exported from the IMScontrol.dll
class CCRIController : public CMotorController
 {
public:
		CCRIController(void);
		~CCRIController();

					//general control functions
		int SetUp(int AxisID, int iPortAddr, AxisData *AxisControl);
		int ShutDown();
		int ConfigureSerialPort();
		int OpenMotorController(char *port);
		int InitRotor (int PortNo, AxisData *Data);
		int Home();
		int Origin();
		int MoveToElement (int PartNo, int waitmove);
		int GetCurrentElement (int *Position);
		int WheelWait();
		int Busy();
		int Wait();
		int Stop();
		int SendCommand(char *command);
		void CallbackHandler(UINT wTimerID);
		static void CALLBACK TimerCallbackFn(UINT wTimerID, UINT msg,DWORD dwUser, DWORD dw1, DWORD dw2); 

private :
		int stat;
		BOOL m_Open;
		BOOL m_bSetup;
		HANDLE m_hMotorFile;
		UINT m_nDelayTime;
		BOOL m_bBusy;
		CMMTimer m_DelayTimer;
		UINT m_TimerID;

		int Open(int portno);
		int Close();
};

	//These 2 functions ONLY are exported by dll
extern "C" CRICONTROLLER_API CMotorController *CreateCRIController(void);
extern "C" CRICONTROLLER_API void  DestroyCRIController(CMotorController *device);


#endif