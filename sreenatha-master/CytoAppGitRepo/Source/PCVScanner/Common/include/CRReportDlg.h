//{{AFX_INCLUDES()
#include "../src/report/crystalreportviewer4.h"
//}}AFX_INCLUDES
#if !defined(AFX_CRREPORTDLG_H__F60D853F_2574_4F8E_ACB8_85DE1F3D477F__INCLUDED_)
#define AFX_CRREPORTDLG_H__F60D853F_2574_4F8E_ACB8_85DE1F3D477F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CRReportDlg.h : header file
//
#define IDD_CRREPORTDLG                 22000
/////////////////////////////////////////////////////////////////////////////
// CCRReportDlg dialog
class CReportProgress;

class AFX_EXT_CLASS CCRReportDlg : public CDialog
{
// Construction
public:
	CCRReportDlg(long NodeLevel, long NodeID, LPCTSTR imagepath, LPCTSTR reportpath, long imageLevel,
                 CStringArray *pSelectedClassNames = NULL, BOOL bDirectPrint = FALSE, CWnd* pParent = NULL);

	BOOL Create(CWnd *pParentWnd = NULL, CWnd ** pflag = NULL);
	static void Release();

// Dialog Data
	//{{AFX_DATA(CCRReportDlg)
	enum { IDD = IDD_CRREPORTDLG };
	CCrystalReportViewer4	m_viewer;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCRReportDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

public:
	CReportProgress * m_progressdlg;

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCRReportDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDownloadFinishedCrviewer1(long loadingType);
	afx_msg void OnClose();
	afx_msg void OnPrint();
	afx_msg void OnSetup();
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	long nodelevel;
	long nodeid;
	BOOL bResize;
	CString sReportPath;

	int CCRReportDlg::NumberImages(int nodelevel);
	CString GetSubsetImages(int nodelevel, int nImages);
	CString GetClassFilter();
	BOOL m_bDirectPrint;
	BOOL m_bCreated;
	int m_ImageLevel;
	CStringArray *m_ClassFilter;
	CWnd** m_pFlag;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CRREPORTDLG_H__F60D853F_2574_4F8E_ACB8_85DE1F3D477F__INCLUDED_)
