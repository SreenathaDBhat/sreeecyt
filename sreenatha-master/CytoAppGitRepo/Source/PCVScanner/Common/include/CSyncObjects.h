////////////////////////////////////////////////////////////////////////////////
// These are a few wrapper classes to help with thread object
// synchronisation
// Written By Karl Ratcliff
////////////////////////////////////////////////////////////////////////////////

#pragma once

#ifdef CAMMANAGED
#define HELPER3_API
#elif defined(HELPER3_EXPORTS)
#define HELPER3_API __declspec(dllexport)
#else
#define HELPER3_API __declspec(dllimport)
#endif

typedef enum
{
    SyncObjectCreate,
    SyncObjectOpen
} CSyncObjectOpenType;

////////////////////////////////////////////////////////////////////////////////
// Event wrapper
////////////////////////////////////////////////////////////////////////////////

class HELPER3_API CSyncObjectEvent
{
public:
    CSyncObjectEvent(CSyncObjectOpenType Type, LPCTSTR Name = NULL, BOOL bManualReset = FALSE);
    ~CSyncObjectEvent(void);
    BOOL Signalled(DWORD TimeOut = 0);      // Returns TRUE if event is signalled. Does not hand around
	int WaitSignalled (DWORD TimeOut = 0);
	void Signal(BOOL Set);                  // Set or Reset the event
    HANDLE  Handle(void) { return m_hEvent; };

private:
    HANDLE      m_hEvent;
};

////////////////////////////////////////////////////////////////////////////////
// Mutex wrapper
////////////////////////////////////////////////////////////////////////////////

class HELPER3_API CSyncObjectMutex
{
public:
    CSyncObjectMutex(CSyncObjectOpenType Type, LPCTSTR Name = NULL, BOOL bInitialOwner = FALSE);
    ~CSyncObjectMutex();
    BOOL Grab(DWORD TimeOut = 0);     // Get access to the mutex
    void Release(void);               // Release hold on mutex

private:
    HANDLE      m_hMutex;   
};

////////////////////////////////////////////////////////////////////////////////
// Critical Section wrapper
////////////////////////////////////////////////////////////////////////////////

class HELPER3_API CSyncObjectCriticalSection
{
public:
    CSyncObjectCriticalSection();
    ~CSyncObjectCriticalSection();
    void Grab(void);                        // Get access to the critical section
    BOOL TryAndGrab(DWORD TimeOut = 0);     // Try and Get access to the critical section
    void Release(void);                     // Release hold on critical section

private:
    HANDLE  m_hSemaphore;
};

////////////////////////////////////////////////////////////////////////////////
// Semaphores
////////////////////////////////////////////////////////////////////////////////

class HELPER3_API CSyncObjectSemaphore
{
public:
    CSyncObjectSemaphore(long InitialSpinCount, long MaxSpinCount);
    ~CSyncObjectSemaphore();
   
	HANDLE  Handle(void) { return m_hSemaphore; };

	long Release(long ReleaseCount = 1);	   // Release
	BOOL Signalled(DWORD TimeOut = 0);         // Returns TRUE if semaphore is signalled

private:
    HANDLE  m_hSemaphore;
};

////////////////////////////////////////////////////////////////////////////////
// Lock a section
////////////////////////////////////////////////////////////////////////////////

class HELPER3_API CLockObject
{
public:
	CLockObject(CSyncObjectMutex *pM) 
	{ 
		pMut = pM;
	}

	CLockObject(CSyncObjectMutex *pM, DWORD MillisecsTimeOut) 
	{ 
		pMut = pM;
		Lock(MillisecsTimeOut);
	}
	
	void Lock(DWORD MillisecsTimeOut) 
	{ 
		pMut->Grab(MillisecsTimeOut); 
		Grabbed = true;
	}

	void UnLock(void)
	{
		if (Grabbed)
		{
			pMut->Release(); 
			Grabbed = false;
		}
	}

	~CLockObject() 
	{ 
		UnLock();
	}

private:
	CSyncObjectMutex *pMut;
	bool Grabbed;

};