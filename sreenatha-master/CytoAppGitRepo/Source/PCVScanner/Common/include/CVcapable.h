/*
 *	CVcapable.h 
 *
 *	CytoVision capabilities.
 *	These are initialised at system startup and used to discriminate between 
 *	different cytovision types, and available facilities
 *
 */

/*----------------------- global capabilities ---------------------------*/

/*
 *	Capability and Facility defines
 */
#define CAP_KARY			0x0001
#define	CAP_PROBE			0x0002
#define	CAP_FLEXKAR			0x0004  /* Used to be defined as CAP_FINDER ('Fast Finder' - never used and obsoleted by Fluorecent Finder) */
#define	CAP_SERVER			0x0008	/* Used to be CAP_REVIEW */
#define CAP_CGH				0x0010
#define CAP_ZSTACK			0x0020	/* Should this be a FAC_ ? */
#define CAP_FINDER			0x0040	/* Fluorecent Finder - used to be defined as both CAP_FETAL and CAP_FOETAL. */
#define CAP_CNET			0x0080	/* CytoNet */
#define CAP_CYTOTALK		0x0400	// New capability to import PowerGene or SmartCapture files via CytoTalk
#define CAP_SPOTAX			0x0800	/* Spot AX Requires a separate capability to Spot SA */
#define CAP_SPOTCOUNTING	0x0800  // Spot Counting
#define FAC_GEN_MIC			0x1000	/* generic microscope */
#define CAP_MFISH			0x2000
#define CAP_RxFISH			0x4000
#define CAP_CAPTURE			0x8000	/*capture capabilitiy*/
// second 16 bit, bit of a frig
#define CAP_CWS				0x00010000 
#define CAP_TFISH			0x00020000


// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the DONGLE_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// DONGLE_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef DONGLE_EXPORTS
#define DONGLE_API __declspec(dllexport)
#else
#define DONGLE_API __declspec(dllimport)
#endif



// Don't use BOOL for return values, as some projects don't include Windows
// types (well actually only the padmin project in WinCV, as far as I know).
DONGLE_API int capable_capabilities();
DONGLE_API int capable_read(int *base, int *base2, int *key); // Return is boolean
DONGLE_API int capable_write(int base, int base2, int key);  // Return is boolean

DONGLE_API void capable_create_base(int capabilities, int *base, int *base2);
DONGLE_API int capable_create_key(int base, int base2);
DONGLE_API int capable_extract(int base, int base2);
DONGLE_API int capable_verify(int base, int base2, int key);

DONGLE_API char *capable_str(int cap);
DONGLE_API char *facility_str(int cap);

DONGLE_API void SetUNIXCompatibleState(int onoff);
DONGLE_API int  GetUNIXCompatibleState(void);

DONGLE_API int capable_remotesession_read(unsigned short *remotesessions);
DONGLE_API int capable_remotesession_write(unsigned short remotesessions);
DONGLE_API int capable_remotesession_create_key(int base);

#ifdef __cplusplus
extern "C" {
#endif

DONGLE_API int c_capabilities(); // so I can P/Invoke it

#ifdef __cplusplus
}
#endif


