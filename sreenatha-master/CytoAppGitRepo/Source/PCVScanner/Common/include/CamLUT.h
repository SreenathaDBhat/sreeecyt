#pragma once

#ifdef CAMMANAGED
	#define CCAMLUT_API
#elif defined(HELPER2_EXPORTS)
	#define CCAMLUT_API __declspec(dllexport)
#else
	#define CCAMLUT_API __declspec(dllimport)
#endif
#include "LUT.h"

class CCAMLUT_API CCamLUT : public CLUT
{
public:
    CCamLUT(int GainRange, int OffsetRange, WORD bitdepth, WORD outputbitdepth);
    CCamLUT(void);
    ~CCamLUT(void);

    CCamLUT *operator*(CLUT *MultLUT);

	// Change the slope depending on the mode
	void	FluorescentMode(BOOL On);

    // Default 1:1 LUT
    void    Default(void);

    // Apply input LUT to an image. Uses a gain and offset to control the slope of the LUT
    // and the offset similar to y = mx + c i.e. output = gain * input + offset
    void    Calc(int gn, int ofs);                                      // Calc the LUT based on gain and offset settings
    // Do the same as above but apply y = Grad * mx + c + Inter
    void    Calc(int gn, int ofs, double Grad, double Inter);
    // Do the same using y = slope * x + inter
    BOOL    DCalc(double slope, double inter);
	// Apply gamma
	void    ApplyGamma(double Gamma);

    // Return the gain and offset given the current slope and intersections etc.
    WORD    GetGain(void);
    WORD    GetOffset(void);
    double  GetMaxGainSlope(void);
    double  GainSteps(void);
    WORD    OutputIntensities(void);

    CLUT    *m_pNativeLUT;          // If this camera has a native resolution other than 8 bits keep an N bit to N bit LUT
	
	double  m_SpecGradient;         
    double  m_SpecIntersection;

private:
    WORD    m_OutputIntensities;    // Output intensity range
    WORD    m_OutputBitDepth;       // Bit depth of the output image
    double  m_GainSteps;            // Number of steps for this grabbers gain and offset
    double  m_OffsetSteps;   
    double  m_MaxGainSlope;         // Max slope for the gain
    BOOL    m_UseGainOffset;
    double  m_Gain;                 // Actual gain and offset
    double  m_Offset;
	double	m_Gamma;				// Any gamma value
};
