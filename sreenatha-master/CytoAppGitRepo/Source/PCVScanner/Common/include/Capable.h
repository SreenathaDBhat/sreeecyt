// Capable.h: interface for the CCapable class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CAPABLE_H__F89B84CE_B70E_4A72_8A02_846298E7B02D__INCLUDED_)
#define AFX_CAPABLE_H__F89B84CE_B70E_4A72_8A02_846298E7B02D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


// Application names
#define HERSIGHT		"Hersight"
#define KISIGHT			"Kisight"
#define AESIGHT			"Aesight"
#define NEUSIGHT		"Neusight"
#define MICROSIGHT		"Microsight"
#define GENSIGHT        "Gensight"
#define PLOIDYSIGHT     "Ploidysight"
#define TMASIGHT        "TMAsight"
#define SPOT			"Spot"
#define REVIEW			"Review Station"
#define SCAN			"Scan Station"
#define HW_REVIEW		"Hardware Review Station"
#define DATAENTRY		"Entry Application"
#define LYMPHSIGHT      "Lymphsight"
#define ANGIOSIGHT      "Angiosight"
#define TISSUEFISH      "TissueFISH"	// Added by: Karun Shimoga, Oct 6, 2003.
#define DUMMY			"Dummy"	

// Definitions of capabilities string (stored in NODE_X.AssayType), work right to left to make strings short
#define ASSAYTYPE_NONE				_T("0")
#define ASSAYTYPE_HERSIGHT			_T("1")
#define ASSAYTYPE_KISIGHT			_T("2")
#define ASSAYTYPE_AESIGHT			_T("4")
#define ASSAYTYPE_NEUSIGHT			_T("8")
#define ASSAYTYPE_MICROSIGHT		_T("01")
#define ASSAYTYPE_SPOT				_T("02")
#define ASSAYTYPE_GENSIGHT          _T("04")
#define ASSAYTYPE_PLOIDYSIGHT       _T("08")
#define ASSAYTYPE_TMASIGHT          _T("001")
#define ASSAYTYPE_LYMPHSIGHT        _T("002")
#define ASSAYTYPE_ANGIOSIGHT        _T("004")
#define ASSAYTYPE_TISSUEFISH        _T("008")	// Added by: Karun Shimoga, Oct 6, 2003.
#define ASSAYTYPE_DUMMY				_T("0016")

// Definitions of bits in dongle, we only define 16 bits but this will be
// increased to 32 bits for scrambling. Once the first 16 bits are used
// (requiring 32-bits of dongle memory after scrambling), the next 16 will
// have to be written to a different memory location. Memory offsets are
// defined following the bit definitions.
#define CAP_HERSIGHT				0x0001
#define CAP_KISIGHT					0x0002
#define CAP_AESIGHT					0x0004
#define CAP_NEUSIGHT				0x0008
#define CAP_MICROSIGHT				0x0010
#define CAP_SPOT					0x0020
#define CAP_REVIEW					0x0040
#define CAP_SCAN					0x0080
#define CAP_GENSIGHT                0x0100
#define CAP_HW_REVIEW               0x0200
#define CAP_DATAENTRY				0x0400
#define CAP_PLOIDYSIGHT				0x0800
#define CAP_TMASIGHT				0x1000
#define CAP_LYMPHSIGHT              0x2000
#define CAP_ANGIOSIGHT              0x4000
#define CAP_TISSUEFISH              0x8000	// Added by: Karun Shimoga, Oct 6, 2003.
#define CAP_DUMMY					0x0001	 // progression

// Dongle offsets.
// A KEY-LOK dongle contains 112 bytes of programmable memory, partitioned
// into 56 16-bit words. USB dongles are shared with CytoVision, and on
// these the first half of the memory (offsets 0 to 27) is reserved for MDS2
// components, the second half (offsets 28 to 55) is reserved for CytoVision.
// If more applications are to share USB dongles in future, the memory space
// should be split into quarters, then eighths etc. The offset definitions
// below are multiples of 32-bits, as every group of 16 capabilities
// requires 32-bit of dongle memory.
#define OFFSET_HERSIGHT				0
#define OFFSET_KISIGHT				0
#define OFFSET_AESIGHT				0
#define OFFSET_NEUSIGHT				0
#define OFFSET_MICROSIGHT			0
#define OFFSET_PLOIDYSIGHT			0
#define OFFSET_TMASIGHT				0
#define OFFSET_SPOT					0
#define OFFSET_REVIEW				0
#define OFFSET_SCAN					0
#define OFFSET_GENSIGHT             0
#define OFFSET_HW_REVIEW			0
#define OFFSET_DATAENTRY			0
#define OFFSET_LYMPHSIGHT           0
#define OFFSET_ANGIOSIGHT           0
#define OFFSET_TISSUEFISH           0	// Added by: Karun Shimoga, Oct 6, 2003.
#define OFFSET_DUMMY				1

#define NODE_APPTYPE_UNUSED_0		0

// Maximum number of offsets used, all 0 for moment so 1
// Note that offsets above 14 (if these offests are multiples of 32 bits)
// are reserved for CytoVision, so MAXNOFFSETS must never be greater than 14.
#define MAXNOFFSETS					2	


//
//class AFX_EXT_CLASS CCapable  
//{
//public:
//	CCapable();
//	virtual ~CCapable();
//	BOOL IsCapableOfHexString(LPCTSTR assaytype);
//	BOOL IsCapableOfName(LPCTSTR name);
//	static BOOL HexToName(LPCTSTR hex, CString &name);
//	static BOOL NameToHex(LPCTSTR name, CString &hex);
//    static BOOL NameToAppType(LPCTSTR name, long *App);
//	BOOL IsWinCVDongle();
//	BOOL IsMDS2Dongle();
//	BOOL IsAIIDongle();
//	BOOL CheckForAnyDongle();
//	BOOL ReadOption(CString &Option, BOOL &on, int index);
//	BOOL WriteOption(BOOL on, int index);
//	int CreateKey(int base, int offset);
//	void GenerateBase(int *cap, int *base, int index);
//	void Clear();
//	CString GetOptionString(int key, int offset);
//
//private:
//	BOOL CheckDongleFor(int capability, int offset);
//	BOOL CheckUSB(int capability, int offset);
//	BOOL CheckWinCVDongleForSpot();
//	int UnscrambleUsbCapability(int base);
//	int ScrambleUsbCapability(int capabilities);
//
//private:
//	int ExtractCapability(int base);
//};

#endif // !defined(AFX_CAPABLE_H__F89B84CE_B70E_4A72_8A02_846298E7B02D__INCLUDED_)
