#if !defined(AFX_CLASSCTRLDLG_H__9B8D4E3A_D9EE_4762_84F7_44C03DFF5313__INCLUDED_)
#define AFX_CLASSCTRLDLG_H__9B8D4E3A_D9EE_4762_84F7_44C03DFF5313__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ClassCtrlDlg.h : header file
//

//#include "resource.h"

#include <classifier.h>

#include <classifierGUI.h>


/////////////////////////////////////////////////////////////////////////////
// ClassControl window

class ClassControl : public CWnd
{

friend class CClassCtrlDlg;

protected:

	CStatic nameCtrl;
	CButton checkBox;
	CButton radioButton;

	//COLORREF colour;

	Class *pClass;

// Construction
public:
	ClassControl();

// Attributes
public:
	COLORREF GetColour();
	int GetCheck(){ return (IsWindow(checkBox.m_hWnd) ? checkBox.GetCheck() : 0); }
	int GetSelect(){ return (IsWindow(radioButton.m_hWnd) ? radioButton.GetCheck() : 0); }


// Operations
public:

	BOOL Create(Class *pClass, const RECT& rect, CWnd* pParentWnd, UINT nID);
	void Enable(BOOL enable = TRUE);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ClassControl)
	protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~ClassControl();

	// Generated message map functions
protected:
	//{{AFX_MSG(ClassControl)
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



/////////////////////////////////////////////////////////////////////////////
// CClassCtrlDlg dialog


typedef void (*ClassCtrlDlgCallbackType)(void*);


class CLASSIFIERGUI_API CClassCtrlDlg : public CDialog
{

protected:

	CHeaderCtrl header;

	ClassControl *pClassCtrls;
	int numClassCtrls;

	Classifier *pClassifier;

	void ScrollClasses(int scrollPos);

	
// Construction
public:
	CClassCtrlDlg(CWnd* pParent = NULL);   // standard constructor
	~CClassCtrlDlg();

	void Initialise(Classifier *pClassifier,
	                ClassCtrlDlgCallbackType ChangeCB, void *pCBparam,
	                ClassCtrlDlgCallbackType ColourChangeCB, void *pColourCBparam,
	                ClassList *pClassList = NULL);

	BOOL Create(CWnd* pParentWnd);

	int GetClassCheck(int index);
	BOOL SetClassCheck(const char name[], BOOL check);
	void CheckAll(BOOL check = TRUE);
	void SetClassChecksToControls();
	int GetClassSelect();
	BOOL SetClassSelect(const char name[], BOOL select);
	void DeselectAll();
	COLORREF GetClassColour(int index);
	//BOOL SetClassColour(const char name[]);
	int GetClassCount(){ return numClassCtrls; }
	const char *GetClassName(int index);
	Class *GetSelectedClass();

	BOOL EnableClassControl(const char name[], BOOL enable = TRUE);


	ClassCtrlDlgCallbackType ChangeCB;
	void *pCBparam;
	ClassCtrlDlgCallbackType ColourChangeCB;
	void *pColourCBparam;

	ClassList *pClassList;



// Dialog Data
	//{{AFX_DATA(CClassCtrlDlg)
//	enum { IDD = IDD_DIALOG_CLASSCTRL };
	CScrollBar	m_scrollClasses;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClassCtrlDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CClassCtrlDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLASSCTRLDLG_H__9B8D4E3A_D9EE_4762_84F7_44C03DFF5313__INCLUDED_)
