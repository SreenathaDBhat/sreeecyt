#pragma once

////////////////////////////////////////////////////////////////////////////////////////////
// Class monitor dialog monitors the registry for values stored there from a script
// These values are output as progress bars with % complete and class names. The dialog will
// re size as new entries are found in the registry. Updates are done on a timer tick.
// Written By Karl Ratcliff Feb2003
////////////////////////////////////////////////////////////////////////////////////////////
//
//

#include "stdafx.h"
#include "procapi.h"

#ifdef CLASSMONITOR_EXPORTS
#define CLASSMONITOR_API __declspec(dllexport)
#else
#define CLASSMONITOR_API __declspec(dllimport)
#endif

// Registry entry for class count information
#define CLASSCOUNT_REGENTRY     "Software\\Applied Imaging\\ClassCounts"
// Control ids for the progress bars
#define PROGRESSBAR_CTRL_ID     2000
// Delimiter between max and actual values
#define DELIMITER               _T(",")

#define IDD_CLASSMONITORDLG             5987

////////////////////////////////////////////////////////////////////////////////////////////
// The progress control object. Shows VarName, a progress bar and a % complete
////////////////////////////////////////////////////////////////////////////////////////////

class CProgressObject
{
public:
    CProgressObject(CWnd *pParent, LPRECT lpR, CString &Name, int nID, CFont *pFont);
    ~CProgressObject();
    void UpdateControlValue(CString &VarValue); // Update control values
    void UpdateControlValue(int VarValue);      
    void SetClassMax(CString &MaxVarValue);     // Set the max for the class
    void SetMax(int MaxVarValue);               // Set the max i.e. total number of objects

    CProgressCtrl       *m_pProgress;           // The progress bar
    CStatic             *m_pLabel;              // A textual label 
    CStatic             *m_pPercent;            // A percentage/numerical field of the count totals
    CStatic             *m_pClassPercent;       // If there is a stop count then show the % completion for the class
    CString             m_VarName;              // Variable name this progress bar represents
    int                 m_MaxValue;             // Max value = total objects found
    int                 m_MaxClassValue;        // Max value obtainable for this class
    int                 m_Value;                // The current value
};

typedef CProgressObject    *ProgObjPtr;
	
#define WM_UPDATEBARS  WM_USER + 100			// Update the progress bars

////////////////////////////////////////////////////////////////////////////////////////////
// CClassMonitorDlg dialog - the dialog itself. 
////////////////////////////////////////////////////////////////////////////////////////////

class CLASSMONITOR_API CClassMonitorDlg : public CDialog
{
	DECLARE_DYNAMIC(CClassMonitorDlg)

public:
	CClassMonitorDlg(CWnd* pParent = NULL);                 // standard constructor
	~CClassMonitorDlg();

// Dialog Data
	enum { IDD = IDD_CLASSMONITORDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);        // DDX/DDV support

public:
    virtual BOOL OnInitDialog();
    void ClearOutRegistry(void);                            // Clear out the registry
    void ClearOutRegistrySubKeys(LPCTSTR KeyName);
    void UpdateTotals(void);
	static UINT UpdateThread(LPVOID pParams);				// Update the dialog
	BOOL							m_ShutDown;				// Shut down the updating thread
	BOOL							m_ThreadEnded;			// Thread has ended
    CFont                           m_Font;                 // Dialog font stuff
	CWinThread						*m_pUpdateThread;		// Handle of thread doing the updates
	void Update(void);
    void ParseLine(CString &FileLine);
    void TidyUp(void);
    
    int                             m_TotalObjects;          // Running total for all objects, all classes
    CString                         m_ProcessName;           // Monitoring class counts for this process
	DWORD							m_PID;
    CList<ProgObjPtr, ProgObjPtr>   m_ProgressList;          // Holds a list of variable names and their progress status
    CProcessMonitor					*m_pCPM;					
	
public:
	DECLARE_MESSAGE_MAP()
	afx_msg LRESULT UpdateBars(WPARAM wParam, LPARAM lPARAM);// Update the progress bars done via WM_UPDATEBARS message sent from the update thread
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};

////////////////////////////////////////////////////////////////////////////////////////////
// Utility functions to create a modeless class monitor dialog and destroy it
////////////////////////////////////////////////////////////////////////////////////////////


CClassMonitorDlg CLASSMONITOR_API *CreateClassMonitorDlg(LPCTSTR ProcessName);
CClassMonitorDlg CLASSMONITOR_API *CreateClassMonitorDlgUsingPID(DWORD PID);
void CLASSMONITOR_API DestroyClassMonitorDlg(CClassMonitorDlg *pDlg);
void CLASSMONITOR_API UpdateClassMonitorDlg(CClassMonitorDlg *pDlg);
