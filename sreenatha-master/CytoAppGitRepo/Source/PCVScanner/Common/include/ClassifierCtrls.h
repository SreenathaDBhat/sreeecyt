#if !defined(AFX_CLASSIFIERLISTCTRL_H__5CA683E9_7F76_473E_9483_EA1CB738A84B__INCLUDED_)
#define AFX_CLASSIFIERLISTCTRL_H__5CA683E9_7F76_473E_9483_EA1CB738A84B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ClassifierCtrls.h : header file
//


#include <classifierGUI.h>



/////////////////////////////////////////////////////////////////////////////
// CClassifierListCtrl window

class CLASSIFIERGUI_API CClassifierListCtrl : public CListCtrl
{
// Construction
public:
	CClassifierListCtrl();

// Attributes
public:

// Operations
public:

	void ShowClassifiers(const char path[], const char description[] = NULL,
	                     BOOL onlyWithTrainingFiles = TRUE);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClassifierListCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CClassifierListCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CClassifierListCtrl)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// CClassifierTreeCtrl window

class CLASSIFIERGUI_API CClassifierTreeCtrl : public CTreeCtrl
{
protected:
	CClassifierListCtrl *pList;
	CString rootPath;
	HTREEITEM hNewDirectoryItem;

	BOOL RecurseDirectories(const char path[], HTREEITEM hParent, const char selectedPath[] = NULL);
	CString GetDirectoryPath(HTREEITEM hItem, const char itemNewText[] = NULL);

// Construction
public:
	CClassifierTreeCtrl();

// Attributes
public:

// Operations
public:

	void AttachClassifierListCtrl(CClassifierListCtrl *pListCtrl){ pList = pListCtrl; }
	void ShowDirectories(const char path[], const char selectedPath[] = NULL);
	void NewDirectory();
	void DeleteDirectory();
	CString GetCurrentPath();
	void Refresh();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClassifierTreeCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CClassifierTreeCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CClassifierTreeCtrl)
	afx_msg void OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBeginlabeledit(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLASSIFIERLISTCTRL_H__5CA683E9_7F76_473E_9483_EA1CB738A84B__INCLUDED_)
