///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CoInitialise/CoUninit class
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#ifdef HELPER2_EXPORTS
#define COINIT_API __declspec(dllexport)
#else
#define COINIT_API __declspec(dllimport)
#endif

class COINIT_API CoInit
{
public:
    CoInit(void) { hr = CoInitialize(NULL); };
    ~CoInit(void) { CoUninitialize(); };
    HRESULT GetResult(void) { return hr; };
private:
    HRESULT hr;
};