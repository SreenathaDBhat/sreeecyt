#include <afxcmn.h>


#define RED       0x0000ff
#define GREEN     0x00ff00
#define BLUE      0xff0000

#define LIGHTRED       0x8888ff
#define LIGHTGREEN     0x88ff88
#define LIGHTBLUE      0xff8888

#define CYAN        0xffff00
#define MAGENTA     0x00ffff
#define YELLOW      0xff00ff


#define BLACK			0x000000
#define	WHITE			0xffffff

#define	DARKGREY		0x444444
#define	LIGHTGREY		0xaaaaaa

/////////////////////////////////////////////////////////////////////////////
// Color slider control
class CColorSlider : public CSliderCtrl
{
public:
	CColorSlider();
	void SetBgColor(COLORREF);

protected:
	COLORREF m_BgColor;
	CBrush m_BgBrush;
	//{{AFX_MSG(CColorControls)
	afx_msg HBRUSH CtlColor (CDC*, UINT);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

