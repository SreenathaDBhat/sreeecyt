#pragma once


// CColourButton

class CColourButton : public CButton
{
	DECLARE_DYNAMIC(CColourButton)

public:
	CColourButton();
	virtual ~CColourButton();

    void SetColour(DWORD NewCol);
    DWORD GetColour(void){ return m_Colour; };

protected:
	DECLARE_MESSAGE_MAP()
public:
    virtual void DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/);
private:
    DWORD   m_Colour;
public:
    virtual BOOL Create(LPCTSTR lpszCaption, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID);
};


