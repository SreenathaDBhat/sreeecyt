#if !defined(AFX_COLOURPICKDLG_H__6534B4B7_656F_494E_9D93_065757EE72A4__INCLUDED_)
#define AFX_COLOURPICKDLG_H__6534B4B7_656F_494E_9D93_065757EE72A4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ColourPickDlg.h : header file
//
#include "../FrameViewer/resource.h"

#include <FrameViewer.h>

#include <MinMaxCtrl.h>
#include <ClassCtrlDlg.h>
#include <classlist.h>
#include <ClassifierCtrls.h>
#include <ScriptProc.h>

#include <afxmt.h>

#ifdef FRAMEVIEWER_EXPORTS
#define FRAMEVIEWER_API __declspec(dllexport)
#else
#define FRAMEVIEWER_API __declspec(dllimport)
#endif




//typedef void (*UpdateCallbackType)(void *pParam);

#define NUM_SLIDERS  5


/////////////////////////////////////////////////////////////////////////////
// CColourPickDlg dialog

class FRAMEVIEWER_API CColourPickDlg : public CDialog
{

protected:

	HCURSOR waitCursor;
	HCURSOR arrowCursor;

	static void ColourClassCtrlCallback(void *pParam);
	static void ShapeClassCtrlCallback(void *pParam);
	static void ThresholdClassCtrlCallback(void *pParam);

	ClassList classList;

	CMinMaxSlider sliders[NUM_SLIDERS];


	FrameGroup *pActiveFrameGroup;

	//CMapStringToString shapeClassDependencies;
	CMapStringToString shapeFeatureLimits;
	CMapStringToString thresholdFeatureLimits;
	CMapStringToString scriptOutputThresholdFeatureLimits;

	// Whether to ignore the first tracking message from slider
	// for improving response
	BOOL ignoreFirstTrackingMessage;

	void MoveWndUp(CWnd *pWnd, int dist);
	void HideShapeAndThresholdClassCtrlDlg();
	void HideThresholdClassCtrlDlg();
	void HideShapeAndColourClassCtrlDlg();
	void HideColourClassCtrlDlg();
	void HideColourClassCtrlDlgAndThresholdCtrlDlg();
	void EndThread();

	void DeleteSelectedAreas();

	//static DWORD WINAPI ScriptThread(LPVOID pParameter);

// Construction
public:
	CColourPickDlg(CWnd* pParent = NULL);   // standard constructor
	~CColourPickDlg();
	BOOL Create(CWnd *pParentWnd = NULL);

	BOOL Setup(const char assayType[], const char assayName[], const char scriptName[],
	           FrameGroupSet *pFrameGroupSet);

	CString trainScriptFileName;
	CString classListFileName;
	CWinThread *pThread;

	FrameGroupSet *pFrameGroupSet;
	CString m_assayTypeString;

	BOOL passDataModified;
	BOOL threadActive;
	BOOL dialogDestroyed;

	// Mode UPDATEDEFAULT updates masks and creates missing masks.
	typedef enum UpdateMasksMode { UPDATESLEEP, UPDATEDEFAULT, UPDATEALLCOLOUR, UPDATEALLSHAPE, UPDATEALLTHRESHOLD, UPDATEALL };
	UpdateMasksMode updateMasks;

	CRITICAL_SECTION  m_pdmCriticalSection;         // Used to lock display update flag for process data modified
	CRITICAL_SECTION  m_umCriticalSection;

	CClassCtrlDlg *pColourClassCtrlDlg;
	CClassCtrlDlg *pShapeClassCtrlDlg;
	CClassCtrlDlg *pThresholdClassCtrlDlg;

	void ReCreateClassCtrlDlgs();
	void ConfigureClassCtrlsForImageType(FrameGroupImageType *pImageType);

	void AddArea(ColourPickRect *pRect);
	void AddNewArea(ColourPickRect *pRect);
	void RefreshAreaList();

	void ResetSelectedTrainingData();

	void SetClassTrainingDataToSliders(TrainingData *pTrainingData, Class *pClass,
                                       const char imageTypeName[]);
	void SetControlsToClassifier();

	Class *GetSelectedClass();

	void AddColourTrainingData(FrameGroup *pFrameGroup, int hue, int saturation, int intensity);

	BOOL GetShapeFeatureLimits(const char featureName[], int *pMin, int *pMax,
	                           BOOL *pShowMin = NULL, BOOL *pShowMax = NULL);
	BOOL GetThresholdFeatureLimits(const char featureName[], int *pMin, int *pMax,
	                           BOOL *pShowMin = NULL, BOOL *pShowMax = NULL);

	FrameGroupImageType *GetSelectedImageType();
	void SetActiveFrameGroup(FrameGroup *pFrameGroup);

	BOOL CalculateClassStats(Class *pClass, FrameGroupSet *pFrameGroupSet);

	TrainingData *CreateTrainingDataFromClass(Class *pClass, Classifier *pClassifier, const char imageTypeName[]);
	TrainingData *CreateTrainingDataFromClassifier(Classifier *pClassifier, const char imageTypeName[]);


// Dialog Data
	//{{AFX_DATA(CColourPickDlg)
	enum { IDD = IDD_DIALOG_COLOURPICK };
	CComboBox	m_comboImageType;
	CButton	m_groupFour;
	CButton	m_groupThree;
	CButton	m_groupOne;
	CButton	m_groupTwo;
	CButton	m_groupZero;
	CButton	m_checkDefineArea;
	CButton	m_groupSelectedClass;
	CButton	m_checkPickColours;
	CListBox	m_listAreas;
	CString	m_assayName;
	CString	m_scriptName;
	CString	m_assayType;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CColourPickDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CColourPickDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonDeletearea();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonReset();
	afx_msg void OnButtonSaveclassifier();
	afx_msg void OnRadioFillmasks();
	afx_msg void OnRadioEmptymasks();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg int OnVKeyToItem(UINT nKey, CListBox* pListBox, UINT nIndex);
	afx_msg void OnButtonLoadclassifier();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnButtonUndo();
	afx_msg void OnButtonStats();
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	virtual void OnCancel();
	afx_msg void OnSelchangeComboImagetype();
	afx_msg void OnButtonManageclassifiers();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


//////////////////////////////////////////////////////////////////////////////


class FRAMEVIEWER_API FrameGroupSet
{
	// A set of related frame groups that are used to train shared classifiers.

friend class FrameGroupSetCache;

protected:

	FrameGroup *pGroupList;

	int nextRectNumber;

	CPtrList colourTrainingHistory;
public:
	void AddFrameGroup(FrameGroup *pNewGroup, CDocument *pDoc, BOOL deleteExistingGroup = TRUE);
	BOOL RemoveFrameGroup(CDocument *pDoc, BOOL deleteIt = TRUE);



	// All of the following three strings must be the same for a FrameGroup's assay,
	// in order for the FrameGroup to belong to this frameGroupSet.
	CString assayType;
	CString assayName;
	CString scriptName;

	//CString objectiveName;

	CColourPickDlg *pDlg;

	// The training data is just the sample points, and so it only used for
	// training the min and max values for classes during interaction.
	// The final class statistics for the classifier are taken from all pixels
	// within the min-max range.
	TrainingData *pColourTrainingData;
	//TrainingData *pPrevColourTrainingData; // For undo

	TrainingData *pShapeTrainingData;
	//TrainingData *pPrevShapeTrainingData; // For undo

	TrainingData *pThresholdTrainingData;

	// Trained by the training data
	Classifier *pColourClassifier;
	Classifier *pShapeClassifier;
	Classifier *pThresholdClassifier;

	BOOL classifiersModified; // Set this back to FALSE after saving classifiers.

	
	FrameGroupImageType *pFrameGroupImageTypes;


	FrameGroupSet *pNext; // For forming a list


	FrameGroup *GetFrameGroup(CDocument *pDoc);
	FrameGroup *GetFrameGroupByIndex(int index);

	BOOL AddRectToGroup(ColourPickRect *pNewRect, CDocument *pDoc);
	BOOL DeleteRect(int number);
	void DeleteAllRects();
	ColourPickRect *GetRect(int number);

	BOOL DeleteRectMasks(const char name[], ColourPickMask::MaskType type);
	void DeleteAllRectMasks();

	void DeleteTrainingData();

	BOOL TrainClassifier(Classifier *pClassifier, TrainingData *pAllTrainingData, TrainingData *pSelectedTrainingData);
	BOOL TrainColourClassifier();
	BOOL TrainShapeClassifier();
	BOOL TrainThresholdClassifier();

	void GetHSIStats(const char className[],
                FeatureStatistics *pHStats, FeatureStatistics *pSStats, FeatureStatistics *pIStats);

	void UpdateViews();

	FrameGroupImageType *GetFrameGroupImageType(const char name[]);
	BOOL FrameGroupUsesClass(FrameGroup *pFrameGroup, const char className[]);

	// Access functions for colour training undo stack.
	BOOL PushColourTrainingHistory(TrainingData *pData);
	TrainingData *PopColourTrainingHistory();
	int CountColourTrainingHistory(){ return colourTrainingHistory.GetCount(); }
	void DeleteColourTrainingHistory();

	void SetAllImageTypeNames(const char name[]);
	void ClearAllImageTypeNames();

	FrameGroupSet();
	~FrameGroupSet();
};


class FRAMEVIEWER_API FrameGroupSetCache
{

protected:

	FrameGroupSet *pList;

	FrameGroupSet *AddFrameGroupSet(const char assayType[], const char assayName[], const char scriptName[]);
	BOOL RemoveFrameGroupSet(FrameGroupSet *pNewSet);

public:

	FrameGroupSet *GetFrameGroupSet(const char assayType[], const char assayName[], const char scriptName[]);
	FrameGroupSet *ChangeFrameGroupSetScriptName(const char assayType[], const char assayName[], const char oldScriptName[], const char newScriptName[]);
	FrameGroupSet *GetFrameGroupSetByIndex(int index);
	FrameGroupSet *GetLowestMagnificationFrameGroupSet(CDocument *pDoc);

	BOOL AddFrameGroup(const char assayType[], const char assayName[], const char scriptName[], CDocument *pDoc,
	                   FrameGroup *pNewGroup, BOOL deleteExistingGroup = TRUE);
	FrameGroup *GetFrameGroup(const char assayType[], const char assayName[], const char scriptName[], CDocument *pDoc);
	BOOL RemoveFrameGroup(const char assayType[], const char assayName[], const char scriptName[],
	                      CDocument *pDoc, BOOL deleteIt = TRUE);
	BOOL DeleteFrameGroups(CDocument *pDoc, FrameGroup *pNotThisOne = NULL);

	Classifier *GetColourClassifier(const char assayType[], const char assayName[], const char scriptName[]);
	Classifier *GetShapeClassifier(const char assayType[], const char assayName[], const char scriptName[]);
	Classifier *GetThresholdClassifier(const char assayType[], const char assayName[], const char scriptName[]);

	FrameGroupSetCache();
	~FrameGroupSetCache();
};


//////////////////////////////////////////////////////////////////////////////








//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_COLOURPICKDLG_H__6534B4B7_656F_494E_9D93_065757EE72A4__INCLUDED_)
