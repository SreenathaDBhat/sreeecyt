/*
 *		C O M M P O R T . H
 *				generic serial communation port interface for the motor controllers
 */


#ifndef _COMMPORT_H
#define _COMMPORT_H

//Standard ASCII Comms Character Codes
#define ACK						6
#define NACK					21
#define CR						13

/* Length of Receive Buffer For Communication */
#define GM_COM_BUF_LEN          128	// This needs to be at least 65 bytes for some Leica responses (for example)
#define GM_TIMEOUT              10000L		// Timeout In Milliseconds
#define DEBUG_TIMEOUT			5000000L

// Buffer For ASCII String Comms
typedef char CommBuffer[GM_COM_BUF_LEN];

#define FLOWCONTROL_DEFAULT		0   // As defined in Device Manager
#define FLOWCONTROL_NONE		1
#define FLOWCONTROL_XONXOFF		2
#define FLOWCONTROL_HARDWARE	3


// Define The Serial Port Object
class CCommPort 
{
public:
	CCommPort();
	~CCommPort();

	// Open a COM Port 
	int OpenSerialPort(int Port, DWORD BaudRate);

	int OpenSerialPort(int Port, DWORD BaudRate, BYTE ByteSize, BYTE Parity, BYTE StopBits, BYTE FlowControl);

	// Close an Open COM Port 
	void CloseSerialPort(void);

	// Return The Handle of the Open Port 
	HANDLE GetSerialPort(void) const { return SerialFileHandle; }

	// Send an ASCII String to a Microscope 
	int SendSerialCommand(const char *buf, char terminator);

	// Get A Response Back From One 
	int GetSerialResponse(char *buf, char Terminator, long TimeOutPeriod);

	int ConfigureSerialPort(DWORD BaudRate, BYTE ByteSize, BYTE Parity, BYTE StopBits, BYTE FlowControl);

	int ResetSerialPort(long timeOutPeriod);

	//return serial file handle
	HANDLE GetPortHandle();
	//set serial file handle to a previously open port
	void ReopenPort(HANDLE ComPort);

private:
	HANDLE SerialFileHandle;
	int port;
	DWORD baudrate;
	BYTE bytesize;
	BYTE parity;
	BYTE stopbits;
	BYTE flowcontrol;

	HANDLE m_hCommsMutex;

};
	

#endif            