////////////////////////////////////////////////////////////////////////////////////
// Common header definitions- try and get rid of the magic number syndrome
// Karl Ratcliff
////////////////////////////////////////////////////////////////////////////////////

#ifndef __ARIOLCOMMON_H
#define __ARIOLCOMMON_H

// Common Macros
#ifndef SAFE_DELETE
#define SAFE_DELETE(x)			if (x) { delete x; x = NULL; }
#endif

#ifdef _DEBUG
#define DEBUGSTRING(s) OutputDebugString(s)
#else
#define DEBUGSTRING(s) OutputDebugString(s)
#endif

#define MIN(x,y) (x < y ? x : y)
#define MAX(x,y) (x > y ? x : y)

/////////////////////////////////////////////////////////////////////////////
// Re-enable this if you wish to output focus and scan positions to file
/////////////////////////////////////////////////////////////////////////////
#define SCANLOGGING

// Common values (defines/constants)

// Invalid ideal Z coordinates
#define INVALID_Z_VALUE				-99999.0
#define PI							3.1415927		// Could use M_PI defined in math.h instead? YES

// Common user defined messages
#define WM_GRABSERVERMSG                    WM_USER + 99        // Messages from the grabserver
#define WPARAM_MINVALUE                     1                   // WPARAM meanings for the above message
#define WPARAM_CONTRAST                     2
#define WPARAM_GAIN                         3                   
#define WPARAM_OFFSET                       4
#define WPARAM_EXPOSURE                     5
#define WPARAM_AUTOSETTINGS                 6                   // Autosettings has started/ended
#define LPARAM_BESTCONTRAST                 1                   // LPARAM meanings for the WPARAM_CONTRAST message
#define LPARAM_AUTOSETTINGSOFF              FALSE               // LPARAM meanings for the WPARAM_AUTOSETTINGS message
#define LPARAM_AUTOSETTINGSON               TRUE

#define	WM_LBUTTONCLICK						WM_USER + 1			// Shutter control has been clicked
#define WM_MICUPDATE						WM_USER + 111		// Update of position or mic message
#define WM_MIC_ERROR						WM_USER + 112
#define	WM_CREATECLASSMON					WM_USER + 243		// Class monitor creation
#define WM_OBJECTIVECHANGE					WM_USER + 248		// Notify which element of objectives currently in use
#define WM_LIVECOLCAPTURE                   WM_USER + 249       // Do a live colour capture with the specified spec list
#define WM_CASEFLAGUPDATE                   WM_USER	+ 893       //Case Flag status chane from right click in navigator



// Named events 

// This is event is set by the grabserver and read by the micserver and indicates that a focus measurement 
// has taken place. It is an autoreset event - see the class grabdevice. NOTE the Local prefix
#define GRABFOCUS_EVENT                     _T("Local\\GRABBERFOCUS_EVENT")  

// This event is set when a new exposure value has been applied
#define GRABEXPOSURE_EVENT                  _T("Local\\GRABBEREXPAPPLIED_EVENT")

// This mutex controls access to the focus value - a named mutex is required as the value may accessed
// asynchronously from different threads
#define GRABFOCUS_MUTEX                     _T("Local\\GRABBERFOCUS_MUTEX")

#define SCANTHREADRUNNINGEVENT              _T("Local\\ScanThreadRunningEvent")


#endif
