// CommsDefs.h
//
// Copyright Invetech Operations Pty Ltd 2000
// 495 Blackburn Rd, Mt Waverley, Victoria, AUSTRALIA
//
// The copyright to the computer program(s) herein
// is the property of Invetech Operations Pty Ltd, Australia.
// The program(s) may be used and/or copied only with
// the written permission of Invetech Operations Pty Ltd
// or in accordance with the terms and conditions
// stipulated in the agreement/contract under which
// the program(s) have been supplied.
//
// constants and definitions
// shared with embedded slide loader code

#ifndef CommsDefs_h_included
#define CommsDefs_h_included


// Command ID's as used in communications between DLL and slide loader.
enum CommmandID
{
    CMDID_ABORT =  1,
    CMDID_BEGINSILENT,                  // unused 28/2/01
    CMDID_CHECKFORSLIDEONSTAGE,
    CMDID_CLEARCURRENTFAULT,
    CMDID_ENDSILENT,                    // unused 28/2/01
    CMDID_FINISHCALIBRATION,            // unused 28/2/01
    CMDID_GETFIRMWAREVERSION,
    CMDID_GETHARDWARE,
    CMDID_GETLASTFAULT,
    CMDID_GETSLIDEPOSITIONS,
    CMDID_GETOCCUPIEDTRAYPOSITIONS,     // unused 28/2/01
    CMDID_GETSLIDEBARCODE,
    CMDID_GETSLIDEBARCODESONTRAY,       // unused 28/2/01
    CMDID_GETSTATUS,
    CMDID_GETTRAYBARCODES,
    CMDID_GETTRAYTAMPERED,
    CMDID_INITIALISE,
    CMDID_ISSLIDEONSTAGE,               // unused
    CMDID_LOADSLIDE,
    CMDID_MOVEROBOTHEADABOVESTAGE,
    CMDID_PUTCALIBRATIONSLIDEONSTAGE,   // unused
    CMDID_REMOVESLIDE,
    CMDID_SCANSLIDEPOSITIONS,
    CMDID_SCANTRAYPOSITIONS,
    CMDID_SELECTTRAY,
    CMDID_SETHARDWARE,
    CMDID_STARTLEAKTEST,                // unused 28/2/01
    CMDID_STARTINSTALLATION,			// unused
    CMDID_SETVACUUMTHRESHOLDS,
    CMDID_SENDBARCODE,
    CMDID_GETBARCODERESPONSE,
	CMDID_CHECKSLIDEPRESENCE,
	CMDID_SCANWHOLETRAY,
	CMDID_MUTE
};


enum ReturnCode           // Return Codes for calls to dll functions
{
    RC_SUCCESS =0,        // The SlideLoader has understood the command, and is ready for action
    RC_INVALID_HANDLE,
    RC_INVALID_STATE,     // The SLideLoader is not in a state where this command may be accepted
    RC_SLIDE_ON_STAGE,    // The slideLoader cannot action this comman becasue there is a slide on the stage
    RC_NO_TRAY,           // The command cannot be actioned because there isn't a tray at the specified location
    RC_NO_SLIDE,          // The command cannot be actioned because there isn't a slide at the specified location
    RC_BAD_SLIDE_OR_TRAY_NUMBER,    // Returned when the DLL gets called with an out of range paramter

    //the comms errors
    RC_SERIAL_UNABLE_TO_OPEN,       // Operating System was not able to open the serial port
    RC_SERIAL_PORT_NOT_OPEN,        // Attempted access to a serial port that hasn't been opened
    RC_SERIAL_PORT_EVENT_FAILED,    // Unable to create the overlapped event
    RC_SERIAL_IO_PENDING,           // unused
    RC_SERIAL_OVERLAPPED_FAILED,    // unused
    RC_SERIAL_READ_TIMEOUT,         // attempted read but it timed out

    RC_THREAD_FAILED,       // serial read thread was not created (probably due to system error)
    RC_SERIAL_FAILED,       // TSerialPort could not be created (probably due to system error)

    RC_INVALID_COMMAND,     // the slideloader does not recognize the command
    RC_CHECKSUM_FAIL,       // checksum failed for serial message
    RC_BUFFER_OVERRUN,      // error occurred while trying to write past the end of a buffer

    RC_VACUUM_FILE_FAIL,    // error processing vacuum pump thresholds file

    RC_ALLOCATION_FAILED,   // allocation of internal resources failed
    RC_DIFFERENT_PORT,      // attempted to open a new port while a different one is already open
    RC_CONVERSION_FAILED,   // unable to convert or read a value
	RC_INCOMPATIBLE_VERSION // version may not work together
};

enum States                 // Slide Loader states - Refer to the ICD for details
{
    ST_UN_INITIALISED = 0,
    ST_IDLE,
    ST_BUSY,
    ST_SILENT,
    ST_FAULT,
    ST_SLIDE_ON_HEAD,
    ST_CALIBRATING,
    ST_DEBUG                // Debug mode - not actually getting status from slide loader
};


enum  MainStates
{
    SM_UNINITIALISED = 0,
    SM_IDLE,
    SM_BUSY,
    SM_SILENT, // Unused
    SM_FAULT,
    SM_INITIALISE,
    SM_LOADSLIDE,
    SM_SCANSLIDES,
    SM_SCANTRAYS,
    SM_UNLOADSLIDE,
    SM_TEST,
    SM_VACUUM,
    SM_LEAKTEST,                // unused
    SM_INSTALLATION,
    SM_MOVEROBOTABOVESTAGE,
    SM_FINISHCALIBRATION,       // unused
    SM_ABORT,
    SM_SLIDEONHEAD,
    SM_CALIBRATING,
    SM_MOVETOCLEAR,
    SM_MOVETO_VERTOPTO
}; 


enum FaultCategory          // Fault Categories - Refer to the ICD for details
{
    FC_NONE = 0,
    FC_OPERATION_INTERRUPTED,
    FC_OPERATION_FAILED,
    FC_FAILURE
};

enum OperationInterruptedFaults              
{
    OI_TRAY_TRANSIENT = 0,              
    OI_TRAY_INSERTED_BADLY
};


enum OperationFailedFaults
{
    OF_NO_BARCODE_SLIDE = 0,
    OF_NO_BARCODE_TRAY,
    OF_SLIDE_MISSING_FROM_POCKET,
    OF_SLIDE_MISSING_FROM_STAGE,
    OF_SLIDE_BAD_TRAY,
    OF_SLIDE_BAD_STAGE,
    OF_SLIDE_DROPPED,
    OF_SLIDE_MOVED,
    OF_SLIDE_STUCK,
    OF_STAGE_NOT_FOUND
};


enum FailureConditionFaults
{
    FF_NONE = 0,
    FF_PROGRAM,
    FF_VACUUM_SYSTEM,
    FF_VACUUM_ON_WHILE_RELEASING,
    FF_CONTINUAL_BAD_SLIDE,
    FF_SLIDE_DROPPED,
    FF_SLIDE_MOVED_ON_HEAD,
    FF_SLIDE_STUCK_ON_HEAD,
    FF_BAD_SLIDE_ON_STAGE,
    FF_STEP_LOSS,
    FF_UNEXPECTED_POSITION,
    FF_HORZ_MOVEMENT_IDLE,
    FF_VERT_MOVEMENT_IDLE,
    FF_STEP_LOSS_APPROACH_STAGE,
    FF_FRONT_COVER_OPEN,
    FF_WATCHDOG_ACTIVATED
};


#endif    //CommsDefs_h_included
