// CommsDefs.h
//
// Copyright Invetech Operations Pty Ltd 2000
// 495 Blackburn Rd, Mt Waverley, Victoria, AUSTRALIA
//
// The copyright to the computer program(s) herein
// is the property of Invetech Operations Pty Ltd, Australia.
// The program(s) may be used and/or copied only with
// the written permission of Invetech Operations Pty Ltd
// or in accordance with the terms and conditions
// stipulated in the agreement/contract under which
// the program(s) have been supplied.
//

#ifndef CommsDefs_h_included
#define CommsDefs_h_included


// Command ID's as used in communications between DLL and slide loader.
enum CommmandID
{
    CMDID_ABORT                     =  1,
    CMDID_BEGINSILENT,
    CMDID_CHECKFORSLIDEONSTAGE,
    CMDID_CHECKSTAGEPOS,
    CMDID_CLEARCURRENTFAULT,
    CMDID_ENDSILENT,
    CMDID_FINISHCALIBRATION,
    CMDID_GETHARDWARE,
    CMDID_GETLASTFAULT,
    CMDID_GETSLIDEPOSITIONS,
    CMDID_GETTRAYPOSITIONS,
    CMDID_GETSLIDEBARCODE,
    CMDID_GETSLIDEBARCODESONTRAY,
    CMDID_GETSTATUS,
    CMDID_GETTRAYBARCODES,
    CMDID_GETTRAYTAMPERED,
    CMDID_INITIALISE,
    CMDID_ISSLIDEONSTAGE,
    CMDID_LOADSLIDE,
    CMDID_MOVEROBOTHEADTOSTAGE,
    CMDID_PUTCALIBRATIONSLIDEONSTAGE,
    CMDID_REMOVESLIDE,
    CMDID_SCANSLIDEPOSITIONS,
    CMDID_SCANTRAYPOSITIONS,
    CMDID_SELECTTRAY,
    CMDID_SETHARDWARE,
    CMDID_STARTLEAKTEST,
};


#endif    //CommsDefs_h_included
