#pragma once

#ifdef HELPER2_EXPORTS
#define HELPER2_API __declspec(dllexport)
#else
#define HELPER2_API __declspec(dllimport)
#endif

class HELPER2_API CComponentBlend
{
public:
    CComponentBlend(void);
public:
    ~CComponentBlend(void);

    typedef enum 
    {
        Range = 0,
        Simple = 1,
        HDR = 2
    } BlendTechnique;

    // Blend multiple images taken with different exposures
    BOOL BlendMultipleExposures(BlendTechnique BT, int numImages, BYTE **ppImages, int exposures[], int width, int height, BYTE *pOutputImage);

protected:
    BOOL RangeBlend(int numImages, BYTE **ppImages, BOOL *pUseImages, int exposures[], int width, int height, BYTE *pOutputImage);
    BOOL SimpleBlend(int numImages, BYTE **ppImages, BOOL *pUseImages, int exposures[], int width, int height, BYTE *pOutputImage);
    BOOL HDRBlend(int numImages, BYTE **ppImages, BOOL *pUseImages, int exposures[], int width, int height, BYTE *pOutputImage);

};
