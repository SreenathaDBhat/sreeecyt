#if !defined(AFX_COMPONENTBOX_H__D013BE17_0186_46DB_B341_201CB634CEF6__INCLUDED_)
#define AFX_COMPONENTBOX_H__D013BE17_0186_46DB_B341_201CB634CEF6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ComponentBox.h : header file
//
#include "DrawWndBase.h"
/////////////////////////////////////////////////////////////////////////////
// CComponentBox window
#define WM_COMPONENTMENU WM_USER + 1		// message send to event handle for context menu

class CCol_Item
{

// Construction
public:
	CCol_Item();

public:	
	int		iIndex;
	int		iColumn;
	int		iType;	    // Text, Icon, FillRect
	int		iAppType;	// Application specied type, valid for col 1 only
	int		iWidth;
	BOOL	bOverlay;
	BOOL	bSelected;
	CRect	R;
	CString sTxt;
	BOOL	bDisplay;
	COLORREF colour;	// holds pseudo colour value !
	BOOL     pseudoMode;
};

class AFX_EXT_CLASS CComponentBox : public CDrawWndBase
{
// Construction
public:
	CComponentBox();

// Attributes
public:

// Operations
public:
	int GetEventIndex(){return m_selindex;};
	int GetEventColumn(){return m_selcolumn;};
	BOOL SetSelIndex(int index);
	int GetSelIndex();
	int GetCustomType(int index);
	BOOL SetColour(int index, COLORREF colour);
	COLORREF GetColour();
	void keypress_Display(int index);
	void keypress_Colour(int index);
	BOOL PseudoColourMode();
	void ChangeColourMode();

	int m_state;

	enum States {displayAll, displayNone, displayAllOverlays, displayNoOverlays};

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CComponentBox)
	//}}AFX_VIRTUAL

// Implementation
public:
	void ResetAll();
	void AddItem (LPCTSTR pTxt, BOOL bDisplay, COLORREF pColour, BOOL bOverlay = FALSE, int AppType = 0);
	void ToggleComponentboxSelectionByType( int AppType);
	void ChangeComponentboxSelection(UINT nChar);

	virtual ~CComponentBox();

	// Generated message map functions
protected:
	//{{AFX_MSG(CComponentBox)
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);

	//}}AFX_MSG
	afx_msg void OnAll();
	afx_msg void OnNone();
	afx_msg void OnAllOverlays();
	afx_msg void OnNoOverlays();
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

private:
	CTypedPtrList <CPtrList, CCol_Item * >m_items;
	int m_index;
	int m_selindex;
	int m_selcolumn;
	int m_yoffset;
	int m_height;
	int m_linesScrolled;
	CCol_Item * m_hoveritem;
	CToolTipCtrl m_tooltip;
	void SetupToolTips();
	void ShowToolTip(LPCTSTR txt, CPoint p);
	void PopupMenu(CPoint point);
	void TrackMouse();
	enum _types {typeTxt=0, typeDisplay, typeColour};
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMPONENTBOX_H__D013BE17_0186_46DB_B341_201CB634CEF6__INCLUDED_)
