
#pragma once


#include <afxtempl.h> // For CList, etc.

#include "ConfigData.h"

#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif


struct MicConfig;


class MICROSCOPEGUI_API ComponentItem
{
friend class ControllerItem; // For access to 'removed' flag.

protected:
	// The 'removed' flag indicates that this component, has been de-selected
	// in the GUI. However it is still retained in memory in case it is
	// reselected, so that the configuration data (particularly for things like
	// objectives) doesn't have to be re-entered. Removed items will not be
	// saved to file though.
	BOOL removed;

	void CopyConfiguration(MicConfig *pDestConfig, MicConfig *pSrcConfig);

public:
	MicConfig configuration;

	void SetConfiguration(MicConfig *pConfiguration);
	void SetCompanionConfiguration(MicConfig *pConfiguration);
	LPCTSTR GetName();

	//ComponentItem(CWnd *pParent, MicConfig *pConfiguration, CFont *pFont, int pos, int id);
//	ComponentItem(ControllerType type){ configuration.AxisID = Mic_None; configuration.CtrlType = CTRL_NONE;
//	                  configuration.ComPort = 0; configuration.Name[0] = '\0'; configuration.pCompanion = NULL; }
	ComponentItem(MicConfig *pConfiguration);

//TODO: destructor should free up memory pointed to by members of MicConfig structure.
};

class ControllerItem
{
friend class ControllerList; // For access to 'removed' flag.

protected:
	CList <ComponentItem*> components;

	// The 'removed' flag indicates that this controller, and all its components,
	// has been de-selected in the GUI. However it is still retained in memory
	// in case it is reselected, so that the components' configuration data
	// (particularly for things like objectives) doesn't have to be re-entered.
	// Removed items will not be saved to file though.
	BOOL removed;
	// The port is set in every component, but need somewhere to set it when
	// the controller contains no components.
	int port; 

public:
	ControllerType type;

	POSITION GetHeadPosition();
	ComponentItem *GetNext(POSITION &pos);
	int GetCount();

	ComponentItem *GetComponentItemByAxis(MicroscopeComponents axis, BOOL includeRemovedItems = FALSE);
	ComponentItem *AddComponent(MicConfig *pConfiguration);
	ComponentItem *AddComponent(MicroscopeComponents axis);
	BOOL RemoveComponent(MicroscopeComponents axis);
	LPCTSTR GetName();
	void SetPort(int port);
	int GetPort();

	ControllerItem(ControllerType type){ this->type = type; removed = FALSE; port = 0; }
};

class ControllerList
{
protected:
	CList <ControllerItem*> controllers;

	void CopyConfiguration(MicConfig *pDestConfig, MicConfig *pSrcConfig);

public:
	BOOL notSaved; // Has something in the list been modified since the last save?

	POSITION GetHeadPosition();
	ControllerItem *GetNext(POSITION &pos);
	int GetCount();

	ControllerItem *GetControllerItemByType(ControllerType type, BOOL includeRemoved = FALSE);
	ControllerItem *AddController(ControllerType type);
	BOOL RemoveController(ControllerType type);
	void AddComponent(MicConfig *pConfiguration);
	int CountComponents();
	ComponentItem *GetComponentItemByAxis(MicroscopeComponents axis);
	int FillConfigurationArray(MicConfig configuration[], int maxCount);

	ControllerList(){ notSaved = FALSE; }


};


// CComponentsPanel dialog

typedef void (*ComponentsPanelControlCB_t)(MicroscopeComponents axis);
typedef void (*ComponentsPanelUpdateCB_t)(MicConfig newConfiguration[], int count);
typedef void (*ComponentsPanelSaveCB_t)();
typedef BOOL (*ComponentsPanelDetectCB_t)(MicConfig *pConfig);

class MICROSCOPEGUI_API CComponentsPanel : public CDialog
{
	DECLARE_DYNAMIC(CComponentsPanel)

public:
	CComponentsPanel(CWnd* pParent = NULL);   // standard constructor
	virtual ~CComponentsPanel();

	BOOL Create(ComponentsPanelControlCB_t ControlFunc = NULL,
	            ComponentsPanelControlCB_t SetupFunc = NULL,
	            ComponentsPanelUpdateCB_t UpdateFunc = NULL,
	            ComponentsPanelSaveCB_t SaveFunc = NULL,
	            ComponentsPanelDetectCB_t DetectFunc = NULL,
	            CWnd *pParent = NULL);

// Dialog Data
	//enum { IDD = IDD_COMPONENTS_PANEL };

	void AddComponents(MicConfig configuration[], int count);
	static int GetWidth();
	void Update();
	int  SaveIfModified();
	void AutoDetect();
	void ShowSetup(MicroscopeComponents axis);

protected:
	ControllerList controllerList;
	CButton *pControlButtons;
	CButton *pSetupButtons;
	CStatic *pTitles;
	//int numItems; // Size of each array of buttons.
	CFont defaultFont;
	CFont boldFont;
	ComponentsPanelControlCB_t ControlCallback;
	ComponentsPanelControlCB_t SetupCallback;
	ComponentsPanelUpdateCB_t  UpdateCallback;
	ComponentsPanelSaveCB_t    SaveCallback;
	ComponentsPanelDetectCB_t  DetectCallback;
	CConfigData configDataDlg;		// Configuration input data dialog

	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);

	BOOL SetupTest(MicroscopeComponents AxisID, int CtrlType);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedConfigure();
	afx_msg void OnBnClickedSave();
};
