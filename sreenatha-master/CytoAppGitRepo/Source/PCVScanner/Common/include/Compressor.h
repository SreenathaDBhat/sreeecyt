// Compressor.h

#pragma once

#include <string>
#include <vector>

class Compressor
{
private:
	bool ReadFile(const char *strName, std::vector<unsigned char>* pvec);

	void WriteFile(const char *strName, std::vector<unsigned char>& vec, int Sz);
	// Compression and Save
	void CompressSave(const char *strName, void *rgbyteRaw, int Width, int Height, int Bits, bool Lossy);

public:

	// Lossless JPEG LS Decompression
	bool DecompressAndLoad(const char *strName, std::vector<unsigned char>& rgbyteOut, int& W, int& H, int& Bits);
	// Lossy JPEG LS Compression
	void LossyCompressAndSave(const char *strName, void *rgbyteRaw, int Width, int Height, int Bits);
	// Lossless JPEG LS Compression
	void CompressAndSave(const char *strName, void *rgbyteRaw, int Width, int Height, int Bits);
	
};

