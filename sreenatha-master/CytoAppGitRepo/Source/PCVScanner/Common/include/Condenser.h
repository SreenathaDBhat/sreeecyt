#if !defined(AFX_CONDENSER_H__A9A5FB9E_E074_4418_829E_34FDE18042DF__INCLUDED_)
#define AFX_CONDENSER_H__A9A5FB9E_E074_4418_829E_34FDE18042DF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Condenser.h : header file
//
#include "MicroscopeDialog.h"

/////////////////////////////////////////////////////////////////////////////
// CCondenser dialog
// CShutter dialog

#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

class MICROSCOPEGUI_API CCondenser : public  MicroscopeDialog
{
// Construction
public:
	CCondenser(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag);
	void DisplayCondenserSetting();

// Dialog Data
	//{{AFX_DATA(CCondenser)
	//enum { IDD = IDD_CONDENSERBASE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCondenser)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCondenser)
	virtual BOOL OnInitDialog();
	afx_msg void OnCondenser();
	afx_msg void OnDestroy();
	//}}AFX_MSG

	afx_msg LRESULT OnMicMessage(WPARAM, LPARAM);


	DECLARE_MESSAGE_MAP()

private :
	void GetCondenserState();
	int m_nCondenserState;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONDENSER_H__A9A5FB9E_E074_4418_829E_34FDE18042DF__INCLUDED_)
