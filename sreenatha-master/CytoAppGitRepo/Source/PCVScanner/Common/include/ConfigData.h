#if !defined(AFX_CONFIGDATA_H__08C6988D_CD80_48FE_AD13_684B03B8DDF9__INCLUDED_)
#define AFX_CONFIGDATA_H__08C6988D_CD80_48FE_AD13_684B03B8DDF9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ConfigData.h : header file
//


#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif



#include "AutoScope.h"

class CIMSData;
class CRotorData;


/////////////////////////////////////////////////////////////////////////////
// CConfigData dialog

class MICROSCOPEGUI_API CConfigData : public CDialog
{
// Construction
public:
	CConfigData(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CConfigData)
	//enum { IDD = IDD_CONFIGDATA };
	CComboBox	m_ControllerList;
	CComboBox	m_ComPortList;
	int		m_AxisID;
	CString	m_Name;
	int		m_NBays;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CConfigData)
	public:
	virtual int DoModal(MicConfig *pConfiguration, BOOL configureAppCompatibilityMode = FALSE);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CConfigData)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnSelchangeConfigdataControllertypelist();
	afx_msg void OnChangeConfigdataNumbays();
	afx_msg void OnConfigdataRemove();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	BOOL configureAppCompatibilityMode; // Compatibility mode for using with old configuration app.
	int m_DefaultController;
	int m_DefaultComPort;
	int m_ComPort;
	ControllerType m_Controller;
	int m_NumberOfFilters;
	short m_IMSName;
	MicConfig *m_pConfiguration;

	void SetIMSDataFrame(int CmdShow);
	void GetIMSData();
	void SetRotorDataFrame(int CmdShow);
	BOOL GetRotorData();
	void RemoveDataDialogs();
	int GetComNumOfPorts(LPTSTR Key);
	BOOL GetComPort(LPTSTR key, int Index, LPTSTR Port);
	int GetComPortFromList();
	void SetComPortInList();
	void SetStageDataFrame(int CmdShow);
	void SetScanningDefaults (SDS *SDS_Data);

	CIMSData *m_IMSData[2];
	CRotorData *m_RotorData;
	int m_RectWidth;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONFIGDATA_H__08C6988D_CD80_48FE_AD13_684B03B8DDF9__INCLUDED_)
