#ifndef _CONVERSION_H_
#define _CONVERSION_H_

#include "matrix.h"
#include "Types.h"


class 
#ifdef FGCONTROL_LEGACYDLL
__declspec(dllexport)
#endif
CConversion {

public :
	CConversion();
	virtual ~CConversion();
	
	void CreateMatrices();
	void DefineAxes (int x_home_dir, int y_home_dir, int x_pos_dir, int y_pos_dir);

	double Rotation_StageToNewSystem(double theta);
	double Rotation_NewSystemToStage(double theta);

	void Ideal_To_Stage (float *x, float *y, float *z);
	void Stage_To_Ideal (float *x, float *y, float *z);
	void Ideal_To_Stage (int *x, int *y, int *z);
	void Stage_To_Ideal (int *x, int *y, int *z);

	void Stage_To_Ideal_Matrix (Matrix *Native, Matrix *Foreign, Matrix *Transformer);
	void Ideal_To_Stage_Matrix (Matrix *Native, Matrix *Foreign, Matrix *Transformer);
	void Stage_To_EF_Matrix (Matrix *Native, Matrix *Foreign, Matrix *Transformer);
	void EF_To_Stage_Matrix (Matrix *Native, Matrix *Foreign, Matrix *Transformer);

	int create_ideal_to_EF();
	int create_stage_to_EF(CoordFrame *StageFrame);
	int create_stage_to_ideal (CoordFrame *StageFrame, CoordFrame *IdealFrame);
	int EnglandFinder_Conversion(CoordFrame *StageFrame, Matrix *FromEF, Matrix *ToEF);
	void transformation_matrix (Matrix *Native, Matrix *Foreign, Matrix *Transformer, double theta);
	void EnglandFinderData(CoordFrame *current);
	int CreateEFString(char string[], char alpha, int num, int seg);
	int ParseEFString(const char string[], char *pAlpha, int *pNum, int *pSeg);
	int convert_coords_to_EF(CoordFrame *current, float x, float y, char *EFCoords);
	int convert_EF_to_coords (CoordFrame *current, int *x, int *y, char *EFCoords);

	int convert_idealcoords_to_EF(float x, float y, char *EFCoords);
	int convert_EF_to_idealcoords (int *x, int *y, char *EFCoords);

	void show_matrix (Matrix *mat);
	void round(double *x, int dp);

private:
	EngFinderData EFdata;
	CMatrix matrix;
	int point1;
	int point2;
	Matrix IdealToStage;
	Matrix StageToIdeal;
	Matrix Stage;
	Matrix Ideal;
	Matrix StageToEF;
	Matrix EFToStage;
	Matrix EF;
	Matrix EFToIdeal;
	Matrix IdealToEF;

	int X_Home_Dir;
	int Y_Home_Dir;
	int X_Pos_Dir;
	int Y_Pos_Dir;
};

#endif