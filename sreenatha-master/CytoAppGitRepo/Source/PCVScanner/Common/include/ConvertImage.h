//=================================================================================
// ConvertImage.h
// Image data conversion class for > 8 bit images
//
// History:
//
// 20Mar00	WH:	original
//=================================================================================
#ifndef __CONVERTIMAGE_H_
#define __CONVERTIMAGE_H_

#include "GrabChannels.h"

class CConvertImage
{
public:
	CConvertImage();
	~CConvertImage();

	enum ConvertType
	{
		Stretch = 0,
		InputLUT
	};

	enum BitShiftType
	{
		NoBitShift = 0,
		BitShift1,
		BitShift2,
		BitShift3,
		BitShift4
	};

	void SetImage(WORD * pImage, WORD bitdepth, WORD bitshift, UINT width, UINT height);
	void GammaCorrection(float v_gamma, BOOL v_inverted);
	void SetInputLUT8(BYTE index, BYTE value);
	BOOL DoConversion(BYTE *pConvert, ConvertType type, BOOL bPad);
	void ColourToMonochrome(BYTE *pMono, BYTE *pColour, WORD bitdepth, WORD c_plane, UINT width, UINT height);

private:
	void MinMax(long *min, long *max);
	WORD *m_pImage;
	UINT m_width;
	UINT m_height;
	WORD m_bitdepth;
	UINT *m_inputLUT;
	UINT m_intensities;
	UINT m_bitshift;
};

#endif //__CONVERTIMAGE_H_