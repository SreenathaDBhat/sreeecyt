#pragma once

#include <string>
#include <vector>
#include <map>
#include <AutoScope.h>
#include <d3dx9math.h>

typedef std::map<int, std::pair<double, double>> CoordinateList;
typedef std::map<int, std::string> EnglandFinderCoordList;

class SourceCoordinateConversionData
{
public:
	double sourceBX;
	double sourceBY;
	double sourceCX;
	double sourceCY;
};


class CoordinateConverter
{
public:
    typedef struct 
    {

	    double sourceBX;
	    double sourceBY;
	    double sourceCX;
	    double sourceCY;

	    double targetBX;
	    double targetBY;
	    double targetCX;
	    double targetCY;
    } ConversionData;

	CoordinateConverter(StageType TypeOfStage, const ConversionData& conversionData);
    ~CoordinateConverter();
    CoordinateList ConvertFromIdeal(const CoordinateList& sourceCoordinates);
    CoordinateList ConvertToIdeal(const CoordinateList& sourceCoordinates);
    void ConvertSingleCoord(bool Forward, double Sx, double Sy, double &Tx, double &Ty);
    void ConvertSingleCoord(bool Forward, double Sx, double Sy);
    double XResult(void) { return m_X; };
    double YResult(void) { return m_Y; };

private:

    void CalculateXYReaderTransform(const ConversionData& conversionData);
    void ConvertXYReaderToIdeal(double Sx, double Sy, double &Tx, double &Ty);
    void ConvertXYReaderFromIdeal(double Sx, double Sy, double &Tx, double &Ty);

	void CreateTransformationMatrix(const ConversionData& conversionData);

    CoordinateList ConvertCoordinates(bool Forward, const CoordinateList& sourceCoordinates);

    std::pair<double, double> ConvertSingleCoord(bool Forward, std::pair<double, double> sourceCoord);

    std::pair<double, double> ConvertSingleCoord(bool Forward, D3DXMATRIX *Transform, D3DXMATRIX *Source, D3DXMATRIX *Dest);

    void TransformationMatrix(D3DXMATRIX *mNative, D3DXMATRIX *mForeign);
    double          m_X, m_Y;
    D3DXMATRIX      m_Transform;
    D3DXMATRIX      m_InverseTransform;
    D3DXMATRIX      m_SourceCoords;         // NOTE Source and Result could be just vectors
    D3DXMATRIX      m_Result;
    D3DXMATRIX      m_Native, m_Foreign;
    StageType       m_StageType;
    double          m_ScaleFYNX, m_ScaleFXNY;
    double          m_OfsFYNX, m_OfsFXNY;
  
};

