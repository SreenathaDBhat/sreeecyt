//-------------------------------------------------------------------------------------
// CpuTopology.h
// 
// CpuToplogy class declaration.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//-------------------------------------------------------------------------------------
#pragma once
#ifndef CPU_TOPOLOGY_H
#define CPU_TOPOLOGY_H

#include <windows.h>

#ifdef CAMMANAGED
#undef HELPER3_API
#define HELPER3_API
#elif defined(HELPER3_EXPORTS)
#define HELPER3_API __declspec(dllexport)
#else
#define HELPER3_API __declspec(dllimport)
#endif

class ICpuTopology;

//---------------------------------------------------------------------------------
// Name: CpuToplogy
// Desc: This class constructs a supported cpu topology implementation object on
//       initialization and forwards calls to it.  This is the Abstraction class
//       in the traditional Bridge Pattern.
//---------------------------------------------------------------------------------
class HELPER3_API CpuTopology
{
public:
                CpuTopology( BOOL bForceCpuid = FALSE );
                ~CpuTopology();

    BOOL        IsDefaultImpl() const;
    DWORD       NumberOfProcessCores() const;
    DWORD       NumberOfSystemCores() const;
    DWORD_PTR   CoreAffinityMask( DWORD coreIdx ) const;

    void        ForceCpuid( BOOL bForce );
private:
    void        Destroy_();

    ICpuTopology* m_pImpl;
};


#endif  // CPU_TOPOLOGY_H
