
/*
 *		D3DControlLOD.h	M.Gregson
 *
 *
 */

#ifndef D3DCONTROLLOD_H
#define D3DCONTROLLOD_H


// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif


/////////////////////////////////////////////////////////////////////////////
// Typedefs of the only exported functions from the real D3DControls DLL

typedef void * (__cdecl * CREATED3DCTRL)(void);
typedef void (__cdecl * DESTROYD3DCTRL)(void *);

/////////////////////////////////////////////////////////////////////////////

DllExport void *CreateD3DControl();	// returns pointer to global D3Dctrl object and increments reference count
DllExport void DestroyD3DControl();	// destroys it when all references removed


/////////////////////////////////////////////////////////////////////////////
// Typedefs of the only exported functions from the real XYStageControl3D DLL

typedef void * (__cdecl * CREATEXYSTAGECONTROL3D)(void);
typedef void (__cdecl * DESTROYXYSTAGECONTROL3D)(void *);

/////////////////////////////////////////////////////////////////////////////

class DllExport XYStageControl3DLOD
{
public:
	XYStageControl3DLOD();
	~XYStageControl3DLOD();
	virtual BOOL InitCtrl(CWnd* pParentWnd, void *d3dctrl, int x, int y, int size);
	virtual BOOL AttachMicServer(void *pIMicSrvr); 
	virtual void MoveCtrl(int x, int y);
	virtual void ShowCtrl(int nCmdShow);

private:
	HINSTANCE hStageControl3Ddll, hD3D9dll;
	XYStageControl3DLOD	*realobject;
	CREATEXYSTAGECONTROL3D NewXYStageControl3D;						// Function pointers
	DESTROYXYSTAGECONTROL3D DeleteXYStageControl3D;
};

/////////////////////////////////////////////////////////////////////////////
// Typedefs of the only exported functions from the real ZDrive3D DLL

typedef void * (__cdecl * CREATEZDRIVE3D)(void);
typedef void (__cdecl * DESTROYZDRIVE3D)(void *);

/////////////////////////////////////////////////////////////////////////////

class DllExport ZDrive3DLOD
{
public:
	ZDrive3DLOD();
	~ZDrive3DLOD();
	virtual BOOL InitCtrl(CWnd* pParentWnd, void *d3dctrl, int x, int y, int size);
	virtual BOOL AttachMicServer(void *pIMicSrvr); 
	virtual void MoveCtrl(int x, int y);
	virtual void ShowCtrl(int nCmdShow);

private:
	HINSTANCE hStageControl3Ddll, hD3D9dll;
	ZDrive3DLOD	*realobject;
	CREATEZDRIVE3D NewZDrive3D;						// Function pointers
	DESTROYZDRIVE3D DeleteZDrive3D;
};

#endif 

