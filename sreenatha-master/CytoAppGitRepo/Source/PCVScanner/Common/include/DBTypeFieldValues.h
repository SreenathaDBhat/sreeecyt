/*****************************************************************************
               Copyright (c) 2001 Applied Imaging International 
 
	Source file:    DBTypeFieldValues.h
 
	Function        Header file for Scan and Review apps


	Modification history:
	Author      Date        Description
	SN          30Jul2001    Initial implementation

 ****************************************************************************/

#ifndef H_DBTYPEFIELDVALUES
#define H_DBTYPEFIELDVALUES

/** MACROS ******************************************************************/

// Each type is represented by 4 bytes. These are split into 2 lots of 2
// bytes so we can use
//
//		2 most  significant bytes for bit flags.
//		2 least significant bytes for values 
//
// The least significant byte values are listed here in the range 0-255
// The most significant byte values (bit flags) are as yet undefined.


//
// NODE_X.AppType -
//
// a number associated with a particular application or maybe assay.
// CURRENT VALUE = 99 or 0 or 1

const long NODE_APPTYPE_UNUSED_0 = 0L;
const long NODE_APPTYPE_UNUSED_1 = 1L;
const long NODE_APPTYPE_MICROMET = 99L;
const long NODE_APPTYPE_SPOTCOUNT = 149L;		// CVscan Spot counting - BCR/ABL etc.
const long NODE_APPTYPE_HERSIGHT = 199L;		// MDS2 Hersight
const long NODE_APPTYPE_KISIGHT = 249L;		// MDS2 Kisight
const long NODE_APPTYPE_AESIGHT = 299L;		// MDS2 Kisight
const long NODE_APPTYPE_GENSIGHT = 349L;
const long NODE_APPTYPE_PLOIDYSIGHT = 399L;
const long NODE_APPTYPE_TMASIGHT = 449L;
const long NODE_APPTYPE_LYMPHSIGHT = 499L;  //Lymphsight
const long NODE_APPTYPE_ANGIOSIGHT = 549L;  //Angiosight
const long NODE_APPTYPE_TISSUEFISH = 599L;  //Tissue FISH	Added by: Karun Shimoga, Oct 6, 2003.

//
// BLOB_X.Type - 
//
// indicate what the blob is or how it should be interpreted eg Image or Plot
// CURRENT VALUE = 1

const long BLOB_TYPE_UNUSED						= 1L;
const long BLOB_TYPE_IMAGE							= 10L;
const long BLOB_TYPE_PLOT								= 30L;
const long BLOB_TYPE_ASSAY							= 50L;
const long BLOB_TYPE_STATS							= 53L;
const long BLOB_TYPE_LINKS							= 54L;
const long BLOB_TYPE_SLIDEVIEW_IMAGE		= 55L;
const long BLOB_TYPE_SLIDEMAP					= 70L;
const long BLOB_TYPE_PLOIDY							= 80L;
const long BLOB_TYPE_FRAME_IMAGE            = 90L;



//
// BLOB_X.BlobType - 
//
// indicate the raw image format eg tiff or jpeg
// CURRENT VALUE = 99

const long BLOB_BLOBTYPE_TIFF   = 10L;
const long BLOB_BLOBTYPE_JPEG   = 20L;
const long BLOB_BLOBTYPE_BMP    = 25L;
const long BLOB_BLOBTYPE_GIF    = 26L;
const long BLOB_BLOBTYPE_PNG    = 27L;
const long BLOB_BLOBTYPE_JP2000 = 30L;
const long BLOB_BLOBTYPE_ASSAY  = 40L;
const long BLOB_BLOBTYPE_ASSAYXML = 41L;
const long BLOB_BLOBTYPE_STATS  = 50L;
const long BLOB_BLOBTYPE_LINKS  = 51L;
const long BLOB_BLOBTYPE_SLIDEMAP = 60L;
const long BLOB_BLOBTYPE_PLOIDY = 70L;
const long BLOB_BLOBTYPE_UNUSED = 99L;

		

//
// MEAS_X.Type - 
//
// indicate if the measurement is a real measurement (eg Area, Cirularity etc) or a fake one
// eg Class, Comment, England Finder coords, Pass number, and re-capture toggle.
// CURRENT VALUE = 1

const long MEAS_TYPE_UNUSED         =  1L;
const long MEAS_TYPE_FEATURE        = 10L;
const long MEAS_TYPE_FORMULA        = 15L;
const long MEAS_TYPE_POSITION       = 20L;
const long MEAS_TYPE_POSN_X         = 21L;
const long MEAS_TYPE_POSN_Y         = 22L;
const long MEAS_TYPE_POSN_Z         = 23L;
const long MEAS_TYPE_SCALE          = 25L;
const long MEAS_TYPE_SCALE_X        = 26L;
const long MEAS_TYPE_SCALE_Y        = 27L;
const long MEAS_TYPE_SIZE           = 29L;
const long MEAS_TYPE_PASSNUMBER     = 30L;
const long MEAS_TYPE_REGIONNUMBER   = 31L;
const long MEAS_TYPE_CLASS          = 40L;
const long MEAS_TYPE_COMMENT        = 41L;
const long MEAS_TYPE_RECAPTURE_FLAG = 42L;
const long MEAS_TYPE_REGION_ID        = 43L;
const long MEAS_TYPE_PARENT_REGION_ID = 44L;
const long MEAS_TYPE_REPORT_FLAG	= 45L;


//
// MEAS_X.DataType - 
//
// indicate raw format including whether the data is in the Text or Number fields
// eg Text or Integer Number or Float Number
// CURRENT VALUE = 1

const long MEAS_DATATYPE_UNUSED            = 1L;
const long MEAS_DATATYPE_TEXT              = 10L;
const long MEAS_DATATYPE_NUMBER_BYTE       = 30L;
const long MEAS_DATATYPE_NUMBER_UBYTE      = 31L;
const long MEAS_DATATYPE_NUMBER_SHORT      = 32L;
const long MEAS_DATATYPE_NUMBER_USHORT     = 33L;
const long MEAS_DATATYPE_NUMBER_LONG       = 34L;
const long MEAS_DATATYPE_NUMBER_ULONG      = 35L;
const long MEAS_DATATYPE_NUMBER_FLOAT      = 50L;
const long MEAS_DATATYPE_NUMBER_DOUBLE     = 55L;
// Dont forget to keep next two contants uptodate.
// They allow quick 'is numeric' checks.
const long MEAS_DATATYPE_NUMBER_BEGIN      = MEAS_DATATYPE_NUMBER_BYTE;		
const long MEAS_DATATYPE_NUMBER_END        = MEAS_DATATYPE_NUMBER_DOUBLE;

//
// MEAS_X.States - 
//
// indicates the different states for recapturing
const long MEAS_STATE_NORECAPTURE           = 1L;
const long MEAS_STATE_AUTO_NEXT_PASS        = 10L;
const long MEAS_STATE_AUTO_SAME_PASS        = 30L;
const long MEAS_STATE_MANUAL_NEXT_PASS      = 32L;
const long MEAS_STATE_MANUAL_SAME_PASS      = 31L;

/** TYPEDEFS, STRUCTS *******************************************************/

/** PUBLIC FUNCTION PROTOTYPES **********************************************/

/** PUBLIC (extern) DECLARATIONS ********************************************/


#endif
/*****************************************************************************
               Copyright (c) 2001 Applied Imaging International 
 ****************************************************************************/








