#if !defined(AFX_DATAINPUT_H__97C4BD77_2C63_47D0_978C_AC1C6E4B4E98__INCLUDED_)
#define AFX_DATAINPUT_H__97C4BD77_2C63_47D0_978C_AC1C6E4B4E98__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DataInput.h : header file
//
class CDataInput;
/////////////////////////////////////////////////////////////////////////////
// CScrollStatic window
class CScrollStatic : public CStatic
{
// Construction
public:
	CScrollStatic();
	CDataInput *m_input;

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScrollStatic)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CScrollStatic();

	// Generated message map functions
protected:
	//{{AFX_MSG(CScrollStatic)
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


#include <afxtempl.h>
#include "DBNavigator.h"
#include "AssayTypeCombo.h"
#include "AssaySelector.h"

class CRow
{
public:	
	CRow(){	m_titlebox = NULL; 
			m_valuebox = NULL; 
            m_assaytypecombo = NULL;
            m_assaynamecombo = NULL;
			m_combobox = NULL; 
			m_listbox = NULL;
			m_datebox = NULL; 
			m_datatype = 0; 
			m_state = UpdateItem;
			m_selected = FALSE;
			m_nlines = 1;
			};

	~CRow(){	if (m_titlebox) delete m_titlebox; 
				if (m_valuebox) delete m_valuebox; 
				if (m_combobox) delete m_combobox;
				if (m_listbox) delete m_listbox;
				if (m_datebox) delete m_datebox;
                if (m_assaytypecombo) delete m_assaytypecombo;
                if (m_assaynamecombo) delete m_assaynamecombo;
    };
				
	CStatic             *m_titlebox;
	CEdit               *m_valuebox;
	CComboBox           *m_combobox;
	CListBox			*m_listbox;
    CAssayTypeCombo     *m_assaytypecombo;
    CAssayNameCombo     *m_assaynamecombo;
	CDateTimeCtrl       *m_datebox;
	CWnd * m_tab, *m_tab2;	// assay has 2 ctrls per row
	CString m_value;
	CString m_title;
	int m_type;
	BOOL m_selected;
	CItemInfo m_node;	// Only valid when editting existing details
	unsigned int m_datatype;
	unsigned char m_nlines;
	int m_state;
	COleDateTime m_date;
	CString m_extratext;
	CString m_assaytype;

	enum _type {text, date, combo, assay, sysdate, slidetype, heading, barcode, slidemap, list, scanmode};
	enum _state {UpdateItem, AddItem, DeleteItem};
};

/////////////////////////////////////////////////////////////////////////////
// CDataInput dialog
#define IDD_DATAINPUT                   15022

class CDBNavigator;
class CItemInfo;

class AFX_EXT_CLASS CDataInput : public CDialog
{
// Construction
public:
	CDataInput(int NodeLevel, LPCTSTR nodename, LPCTSTR dir, LPCTSTR tmplname, CWnd* pParent = NULL, CRect * r = NULL, BOOL bUpdate = FALSE);   // standard constructor
	virtual ~CDataInput();

	CTypedPtrList <CPtrList, CRow * >m_rows;
	CString m_tmplname;
	BOOL m_bCheckBarcode;
	BOOL m_bObeyEdittable;
	CStringArray m_assays;
	CStringArray m_assaytypes;
	CStringArray m_slidetypes;
	BOOL m_bDetailchanged;

	void AddDetailForEdit(CItemInfo &info, LPCTSTR title, int type, int datatype, LPCTSTR value, COleDateTime * date, CString * extratext, CString &AssayType);
	void CtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	void CheckExistsInParent(CDBNavigator * nav, CItemInfo * parent){m_nav = nav; m_parentnode = parent;};

	enum types {Edittable = 0x00010000, Mandatory = 0x00020000};

// Dialog Data
	//{{AFX_DATA(CDataInput)
	enum { IDD = IDD_DATAINPUT };
	CSpinButtonCtrl	m_spin;
	CScrollStatic	m_frame;
	CScrollBar	m_vertscroll;
	CComboBox	m_seltmpl;
	CComboBox	m_choices;
	CComboBox	m_selctrl;
	BOOL	m_mandatory;
	CString	m_titlename;
	CString	m_nodename;
	BOOL	m_edittable;
	CString	m_nodetext;
	int		m_lines;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDataInput)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDataInput)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnCreateTempl();
	afx_msg void OnEditTempl();
	afx_msg void OnSaveTempl();
	afx_msg void OnInsert();
	afx_msg void OnAdd();
	afx_msg void OnRemove();
	afx_msg void OnDelete();
	afx_msg void OnUpdate();
	afx_msg void OnSelchangeCtrltype();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSelchangeSelTempl();
	afx_msg void OnAdvanced();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnDeleteTmpl();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void Refresh();
	CStatic * NewTitleBox(int row, LPCTSTR txt, BOOL bEnable = FALSE);
	CEdit * NewValueBox(int row, LPCTSTR txt, CRow * newrow, BOOL bEnable = TRUE);
	CComboBox *NewComboBox(int row, LPCTSTR txt, CRow * newrow, BOOL bEdit = TRUE);
	CListBox * NewListBox(int row, LPCTSTR txt, CRow * newrow, BOOL bEdit = TRUE);
	CDateTimeCtrl * NewDateBox(int row, CRow * newrow, BOOL bInit = TRUE);
    void NewAssayComboBoxes(int row, LPCTSTR txt, CRow * newrow, CAssayTypeCombo **pAssayTypeCombo, CAssayNameCombo **pAssayName);
	BOOL ReadTemplate(LPCTSTR filepath);
	void DeleteItems();
	void EnableEditCtrls(BOOL yes);
	BOOL SaveToFile(LPCTSTR filename);
	BOOL VerifySaveTempl();
	BOOL VerifyInsert(POSITION atpos);
	CRow * InsertItem(POSITION atpos, LPCTSTR str = NULL);
	void RepositionRows();
	void FillEditField(CRow * item);
	void ClearSelected();
	void ShowAdvanced(BOOL on);
	void ArrangeButtons();
	void CreateBoxesForEdit();
	void ResetEditFields();
	BOOL TypeInList(int Type);
	void ExtractValue(CRow * item, CString &str);
	BOOL HandleTabs();
	void SetItemFocus(CRow * item);
	void ChangeCtrlType();
	void AddReportFormatList(CString csAssayType);
	CRow* AddDetail(CString csAssayType, CString csTitle, CString csValue, int type);
	void LoadReportFormat(CRow* row);
	void FillSlideMapsCombo(CComboBox* combo);

	BOOL m_bAdvanced;
	CString m_dir;
	CRect *m_rect;
	int m_ID;
	BOOL m_editmode;
	CString m_ver;
	BOOL m_modified;
	void OffsetItems();
	CRect m_frect;
	int m_y;
	BOOL m_bUpdate;
	POSITION m_selpos;
	CString m_nodestatic;
	int m_plus;
	int m_NodeLevel;
	BOOL m_move;
	CFont m_font, m_headfont;
	// Check for existance stuff
	CDBNavigator *m_nav;
	CItemInfo * m_parentnode;
	CImageList m_imlist;
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATAINPUT_H__97C4BD77_2C63_47D0_978C_AC1C6E4B4E98__INCLUDED_)
