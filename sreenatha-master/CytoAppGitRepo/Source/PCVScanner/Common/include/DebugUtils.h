/*****************************************************************************
               Copyright (c) 2002 Applied Imaging International 
 
	Source file:    DebugUtils.h
 
	Function        Header file for DebugUtils.cpp

	Package:        Helper library part of MDS2

	Modification history:
	Author      Date        Description
	SN          04Apr02     ReviewUtils.h
							Initial implementation
								- home for Review debug tools
	SN			23Sep03		Moved to Helper to support CFragment class.

 ****************************************************************************/

#pragma once

// Support for ADO database services
//#import "D:\Program Files\Common Files\System\ADO\msado15.dll"
#import "ADO\msado15.dll" no_namespace rename("EOF", "EndOfFile")

/** MACROS ******************************************************************/

/** TYPEDEFS, STRUCTS *******************************************************/

/** PUBLIC FUNCTION PROTOTYPES **********************************************/

void DumpComError(_com_error &ce);
void DumpADOError( _ConnectionPtr pConn);
void DumpElapsedTime( CTime endTime, CTime startTime, LPCTSTR szOut);


/** PUBLIC (extern) DECLARATIONS ********************************************/



/*****************************************************************************
               Copyright (c) 2002 Applied Imaging International 
 ****************************************************************************/
