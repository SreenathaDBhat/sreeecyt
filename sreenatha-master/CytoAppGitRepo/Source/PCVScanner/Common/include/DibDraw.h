//=================================================================================
// DibDraw.h
// Base class for dib drawing
//
// History:
//
// 28Feb00	WH:	original
//=================================================================================
#ifndef __DIBDRAW_H_
#define __DIBDRAW_H_

typedef void (*DDOVERLAYPROC)(void * dc, INT x, INT y, INT width, INT height, RECT *r);

class 
#ifdef FGCONTROL_LEGACYDLL
__declspec(dllexport)
#endif
CDIBDraw
{
public:
	CDIBDraw();
	~CDIBDraw();

	virtual void Draw(HWND hWnd, BYTE *image, RECT* pDst, RECT *pSrc, RECT *pClient, UINT bpp = 8);
	virtual void DrawSubRect(HWND hWnd, BYTE *image, RECT* pDst, RECT *pSrc, RECT *pBmp, UINT bpp = 8);
	virtual void OutputLUTEntry(UINT index, UINT red, UINT green, UINT blue);
	void SetOverlayCallback(DDOVERLAYPROC pfnOverlay) { m_fnOverlay = pfnOverlay;};

protected:
	LOGPALETTE*		m_pPal;
	HPALETTE		m_DisplayPal;
	BOOL			bUpdatePal;

	typedef struct
	{
		BITMAPINFOHEADER bmiHeader;
		short bmiColors[256];		// Palette indices
	} BITMAPINFO256;

	BITMAPINFO256	m_DibInfo;
	DDOVERLAYPROC		m_fnOverlay;

};

#endif