// DisplayAttrib.h: interface for the CDisplayAttrib class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DISPLAYATTRIB_H__B42051EF_B714_4940_BCE5_9A693802B2A8__INCLUDED_)
#define AFX_DISPLAYATTRIB_H__B42051EF_B714_4940_BCE5_9A693802B2A8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


//////////////////////////////////////////////////////////////////////
// CDisplayAttribRep
//
// Representation of set of flags for display state.
//
//////////////////////////////////////////////////////////////////////
class CDisplayAttribRep
{
public:
	// Default constructor
	CDisplayAttribRep();


// Attributes
public:


// Operations
public:


// Implementation
protected:
	// Reference count
	unsigned int nRC;
	// Display flags
	BOOL bSelected;
	BOOL bVisible;
	// Colour
	BOOL bColourSet;
	COLORREF colour;

	friend class CDisplayAttrib;

};


//////////////////////////////////////////////////////////////////////
// CDisplayAttrib
//
// This is a reference counted set of flags for the display state
// of it's owner. The reference counting allows sharing of flags 
// between owners with safe cleanup.
//
//////////////////////////////////////////////////////////////////////
class CDisplayAttrib
{
public:
	// Default constructor
	CDisplayAttrib();
	// Copy contructor
	CDisplayAttrib( const CDisplayAttrib & attrib);
	// Assignment operator
	const CDisplayAttrib & operator=( const CDisplayAttrib & attrib);
	// Destructor
	~CDisplayAttrib();


// Attributes
public:


// Operations
public:
	inline BOOL IsSelected( void )
		{ ASSERT(pRep); return pRep  ?  pRep->bSelected : FALSE; };

	inline void SetSelected( BOOL bSelected = TRUE)
		{ ASSERT(pRep); if (pRep) pRep->bSelected = bSelected; };

	inline BOOL IsVisible( void )
		{ ASSERT(pRep); return pRep  ?  pRep->bVisible  : FALSE; };

	inline void SetVisible( BOOL bVisible = TRUE)
		{ ASSERT(pRep); if (pRep) pRep->bVisible = bVisible; };

	inline BOOL HasColour( void )
		{ ASSERT(pRep); return pRep  ?  pRep->bColourSet : FALSE; };

	inline COLORREF GetColour( void )
		{ ASSERT(pRep); return pRep  ?  pRep->colour : RGB(0,0,0); };

	inline void SetColour( COLORREF colour)
		{ ASSERT(pRep);	if (pRep) { pRep->bColourSet = TRUE; pRep->colour = colour; } };

	inline void UnsetColour( void )
		{ ASSERT(pRep);	if (pRep) { pRep->bColourSet = FALSE; pRep->colour = RGB(0,0,0); } };


// Implementation
protected:
	// Representation of the actual flags
	CDisplayAttribRep * pRep;

protected:
	// Shallow copy of flags
	void Copy( const CDisplayAttrib & attrib);

	// Remove one owner of flags
	void Free( void );

};

#endif // !defined(AFX_DISPLAYATTRIB_H__B42051EF_B714_4940_BCE5_9A693802B2A8__INCLUDED_)
