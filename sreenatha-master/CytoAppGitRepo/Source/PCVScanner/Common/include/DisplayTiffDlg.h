#if !defined(AFX_DISPLAYTIFFDLG_H__9E680545_F229_4020_BBD2_23B1045933E1__INCLUDED_)
#define AFX_DISPLAYTIFFDLG_H__9E680545_F229_4020_BBD2_23B1045933E1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DisplayTiffDlg.h : header file
//
#define IDD_TIFFDLG                     17000
/////////////////////////////////////////////////////////////////////////////
// CDisplayTiffDlg dialog

//class CDrawWnd;
class CComponentBox;
class CSRDrawWnd;
class AI_Tiff;
class CHistogram;

typedef void (*APPLYCB)(int type, void * data);

class _declspec(dllexport) CDisplayTiffDlg : public CDialog
{
// Construction
public:
	CDisplayTiffDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDisplayTiffDlg();

// Dialog Data
	//{{AFX_DATA(CDisplayTiffDlg)
	enum { IDD = IDD_TIFFDLG };
	BOOL	m_bAdvanced;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDisplayTiffDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDisplayTiffDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnCtstretch();
	afx_msg void OnApply();
	afx_msg void OnSharpen();
	afx_msg void OnMeasure();
	afx_msg void OnCancel();
	afx_msg void OnOk();
	afx_msg void OnApplyall();
	afx_msg void OnAdvanced();
	afx_msg void OnTest();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void AttachTiff(AI_Tiff * pTiff);
	void AttachApplyCB(APPLYCB, void * data);
	void ShowTools(BOOL on);
	CBitmap * CreateDisplayBM();
	BOOL CreateBMPFile(LPCTSTR filename, int &w, int &h);
	void ScaleToFit(BOOL yes);

	enum applyTypes { aApply, aApplyAll, aOK };

private:

	CSRDrawWnd *	m_pDrawWnd;			// Tiff drawing window
	CComponentBox*	m_selbox;			// Component selection box
	AI_Tiff*		m_pTiff;			// Attached AII_TIFF class
	void *			m_applyData;
	APPLYCB			m_applyCB;
	BOOL			m_bRulersOn;
	BOOL			m_bFirst;			// Get round a CFrame bug

	void AddIconToButton(UINT ButtonID, UINT iconID);
	static void SelBoxEH(UINT event, long x, long y, DWORD data);
	void HandleSelBoxEvents(UINT event, long x, long y);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DISPLAYTIFFDLG_H__9E680545_F229_4020_BBD2_23B1045933E1__INCLUDED_)
