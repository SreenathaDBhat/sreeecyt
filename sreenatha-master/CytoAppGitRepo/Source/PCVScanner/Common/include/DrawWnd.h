#if !defined(AFX_DRAWWND_H__2398F516_BF9A_4F5B_86AD_DB368E6EAF65__INCLUDED_)
#define AFX_DRAWWND_H__2398F516_BF9A_4F5B_86AD_DB368E6EAF65__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DrawWnd.h : header file
//
#include <afxtempl.h>


// Event handler typedef
typedef void (*EVHANDLER)(UINT event, UINT nFlags, long x, long y, DWORD data);

class CScaledBitmap;

// Item for eventhandler list
class CEH_Item
{
public:	
	EVHANDLER	efunc;
	DWORD		data;
};

/////////////////////////////////////////////////////////////////////////////
// CDrawWnd window


class CScaledBitmap : public CBitmap
{
public:
	CRect  Rect;
	DWORD Rop;
	CScaledBitmap();
	~CScaledBitmap();
};


class CDrawWnd : public CWnd
{
// Construction
public:
	CDrawWnd();

// Attributes
public:
	int m_XScale;
	int m_YScale;

	enum BmpOps {AddBmp, DeleteBmp};
	enum HandlerOps {AddHandler, DeleteHandler};

// Operations
public:
	void AttachEventHandler(EVHANDLER func, DWORD data, WORD action);
	void AttachScaledBitmap(CScaledBitmap* scaledBitmap, WORD action);
	void Clear();
	void DrawTransparentBitmap(HDC hdc, HBITMAP hBitmap, short xStart,
                       short yStart, COLORREF cTransparentColor);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDrawWnd)
	public:
	virtual BOOL Create(LPCTSTR lpszWindowName, RECT r, CWnd* pParentWnd, int XScale, int YScale);
	//}}AFX_VIRTUAL

// Implementation
public:
	void Modified(BOOL bRender = FALSE);
	virtual ~CDrawWnd();
	BOOL OnEraseBkgnd(CDC *pDC);


	// Generated message map functions
protected:
	//{{AFX_MSG(CDrawWnd)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:	// MEMBERS
	CTypedPtrList <CPtrList, CScaledBitmap * > m_ImList;
	CTypedPtrList <CPtrList, CEH_Item * >m_eventhandlers;
	BOOL m_LbuttonDown, m_RbuttonDown;
private: 	// FUNCTIONS

	// Event handling
	void HandleEvents(UINT event, UINT nFlags, long x, long y);
};



/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DRAWWND_H__2398F516_BF9A_4F5B_86AD_DB368E6EAF65__INCLUDED_)
