#if !defined(AFX_DRAWWNDBASE_H__2398F516_BF9A_4F5B_86AD_DB368E6EAF65__INCLUDED_)
#define AFX_DRAWWNDBASE_H__2398F516_BF9A_4F5B_86AD_DB368E6EAF65__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DrawWndBase.h : header file
//
#include <afxtempl.h>


// Event handler typedef
typedef void (*EHANDLER)(UINT event, long x, long y, DWORD data);

// Item for eventhandler list
class CEH_Item
{
public:	
	EHANDLER	efunc;
	DWORD		data;
};
/////////////////////////////////////////////////////////////////////////////
// CDrawWndBase window

class AI_Tiff;

class _declspec(dllexport) CDrawWndBase : public CWnd
{
// Construction
public:
	CDrawWndBase(/*LPCTSTR classname = "CDRAWWND"*/);

// Attributes
public:

	enum HandlerOps {AddHandler, DeleteHandler};

// Operations
public:
	void AttachEventHandler(EHANDLER func, DWORD data, WORD action);
	void AttachTiff(AI_Tiff *pTiff);
	void GetScrollOffset(int *x, int *y, int *xmax, int *ymax);
	void SetScrollOffset(int x, int y);
	void SetLocalCursor(HCURSOR cursor);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDrawWndBase)
	public:
	virtual BOOL Create(LPCTSTR lpszWindowName, RECT r, CWnd* pParentWnd);
	//}}AFX_VIRTUAL

// Implementation
public:
	void Modified(BOOL bRender = FALSE);
	virtual ~CDrawWndBase();
	BOOL m_bDrawTiff;


protected:
	AI_Tiff*		m_pTiff;			// Attached AII_TIFF class
	void HandleEvents(UINT event, long x, long y);

	// Generated message map functions
protected:
	//{{AFX_MSG(CDrawWndBase)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:	// MEMBERS
	CTypedPtrList <CPtrList, CEH_Item * >m_eventhandlers;
	int m_xoffset, m_yoffset;
	HCURSOR		m_hCursor;

private: 	// FUNCTIONS

	// Event handling
	void ConfigureScrollBars();
	void EraseBackground(CDC *pDC, CRect &r);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DRAWWNDBASE_H__2398F516_BF9A_4F5B_86AD_DB368E6EAF65__INCLUDED_)
