#ifndef __CDRIVES_H__
#define __CDRIVES_H__

//==================================================================================================
// Drive / Disk functionality
//==================================================================================================
class CDrives
{
public:
	CDrives();
	void LoadCombo(CComboBox & box);
	BOOL WriteArchiveDiskLabel(LPCTSTR rootDir, LPCTSTR label);
	BOOL ReadArchiveDiskLabel(CString &rootDir, CString &label);
	static LONGLONG ActualRequired(LPCTSTR archdev, LONGLONG required, long numFiles);
	LONGLONG GetMediaSpace(LPCTSTR arcdev);
	static BOOL DeleteDir(LPCTSTR dirpath);
	static DWORD SizeDir(LPCTSTR dirpath, int *pNumFiles);
private:
	CStringArray m_strArray;
};
#endif //__CDRIVES_H__