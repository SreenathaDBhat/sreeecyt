
/*
 * E S L I D E R C T R L . H
 * 
 */

#ifndef	_ESLIDERCTRL_H
#define	_ESLIDERCTRL_H

#include <afxcmn.h>

class ESliderCtrl : public CWnd
{

protected:

	CSliderCtrl m_SliderCtrl;

	CStatic MinText;
	CStatic MaxText;
	CStatic PosText;


public:

	ESliderCtrl();
	BOOL Create(UINT Style, const RECT& rect, CWnd* pParentWnd, UINT nID);
	void SetRange(int Min, int Max, BOOL update = TRUE);
	void GetRange(int *pMin, int *pMax);
	void SetPos(int Pos);
	void GetPos(int *pPos);
	void Update();


	// Message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	DECLARE_MESSAGE_MAP()
};


#endif


