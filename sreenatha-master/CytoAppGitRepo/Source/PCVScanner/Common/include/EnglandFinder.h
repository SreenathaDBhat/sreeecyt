#if !defined(AFX_ENGLANDFINDER_H__DC47A988_6A34_4139_BFC5_F66740F19C56__INCLUDED_)
#define AFX_ENGLANDFINDER_H__DC47A988_6A34_4139_BFC5_F66740F19C56__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EnglandFinder.h : header file
//
#include "MicroscopeDialog.h"

/////////////////////////////////////////////////////////////////////////////
// CEnglandFinder dialog

#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

class MICROSCOPEGUI_API CEnglandFinder : public MicroscopeDialog
{
// Construction
public:
	CEnglandFinder(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag);

// Dialog Data
	//{{AFX_DATA(CEnglandFinder)
	//enum { IDD = IDD_ENGLANDFINDERBASE };
	CString	m_sEFCoordinate;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEnglandFinder)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEnglandFinder)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnKillfocusEfcoordinate();
	afx_msg void OnMovetoef();
	afx_msg void OnStagetoef();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private :
		int m_nX;
		int m_nY;
		int m_nZ;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ENGLANDFINDER_H__DC47A988_6A34_4139_BFC5_F66740F19C56__INCLUDED_)
