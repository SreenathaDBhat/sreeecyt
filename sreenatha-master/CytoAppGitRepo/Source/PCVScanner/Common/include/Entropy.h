#pragma once

#ifdef HELPER2_EXPORTS
#define ENTROPYMEASURE_API __declspec(dllexport)
#else
#define ENTROPYMEASURE_API __declspec(dllimport)
#endif

int ENTROPYMEASURE_API EntropyThreshold8(BYTE *pSrcImage, WORD W, WORD H);

int ENTROPYMEASURE_API EntropyThreshold16(WORD *pSrcImage, WORD Bins, WORD W, WORD H);
