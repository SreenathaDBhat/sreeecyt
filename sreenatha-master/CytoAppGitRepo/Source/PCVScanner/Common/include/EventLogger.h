////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Logs events to the event log - header file
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#pragma once

#ifdef EVENTLOGGER_EXPORTS
#define EVENTLOGGER_API __declspec(dllexport)
#else
#define EVENTLOGGER_API __declspec(dllimport)
#endif

#define EVENTLOGNAME         _T("Applied Imaging")

// System event logging

class EVENTLOGGER_API EventLogger
{
public:
    EventLogger(LPCTSTR Name);
    ~EventLogger();
   
    //BOOL AddEventSource();
    void AddEventSource(LPCTSTR pszSrcName, LPCTSTR pszMsgDLL);     
    void RemoveEventSource(LPCTSTR pszName);
    void Error(LPCTSTR Msg);
    void Warning(LPCTSTR Msg);
    void Info(LPCTSTR Msg);

private:
    HANDLE      m_hEventLog;
    PSID GetUserSid();
    void Event(WORD Type, LPCTSTR szMsg);   

};
