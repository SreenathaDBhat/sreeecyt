#if !defined(AFX_EXPLAINERROR_H__577F22F1_A76A_4593_862F_558D38FCA82D__INCLUDED_)
#define AFX_EXPLAINERROR_H__577F22F1_A76A_4593_862F_558D38FCA82D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExplainError.h : header file
//
#include "MicroscopeDialog.h"


/////////////////////////////////////////////////////////////////////////////
// CExplainError dialog

#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

class MICROSCOPEGUI_API CExplainError : public MicroscopeDialog
{
// Construction
public:
	CExplainError(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag);
	LRESULT HandleMessage (WPARAM MsgCode, LPARAM MsgData);

// Dialog Data
	//{{AFX_DATA(CExplainError)
	//enum { IDD = IDD_EXPLAINERRORBASE };
	int		m_nErrorCode;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExplainError)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CExplainError)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnExplainerror();
	afx_msg void OnKillfocusErrorcode();
	afx_msg void OnGetlasterror();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private :

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXPLAINERROR_H__577F22F1_A76A_4593_862F_558D38FCA82D__INCLUDED_)
