///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// These functions were cut and paste all over WinCV source code !
// 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once


#include <settings.h>

#ifdef __cplusplus
extern "C" {
#endif


LPTSTR convertName(LPCTSTR name);
int hash_id(LPCTSTR name);
TCHAR *BP_strstr(TCHAR *s1, TCHAR *s2);
void rawSaveErr(LPCTSTR  message);
void rawWarning();
void rawLoadErr(LPCTSTR message);
int matchFluoName(LPCTSTR s1, LPCTSTR s2);
void  _copyFC(SpectraChrome *f1, SpectraChrome *f2);
SpectraChrome * mergeToList(SpectraChrome *f, SpectraChrome *entry);
LPTSTR newString(LPCTSTR str);

#ifdef __cplusplus
}
#endif
