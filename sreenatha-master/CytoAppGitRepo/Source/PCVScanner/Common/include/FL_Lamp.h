//#ifndef _FL_LAMP_H
//#define _FL_LAMP_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MicroscopeDialog.h"
#include "afxwin.h"

/////////////////////////////////////////////////////////////////////////////
// CSlideLoader_Load dialog


#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

// CFL_Lamp dialog

class MICROSCOPEGUI_API CFL_Lamp : public MicroscopeDialog
{

public:
	CFL_Lamp(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag);
	virtual ~CFL_Lamp();


// Dialog Data
	//enum {IDD = IDD_FL_LAMPBASE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	void Get_FL_LampExposureTime();
	void GetFL_LampState();
	void DisplaySettings();
	void GetSoftwareVersion();
	LRESULT HandleMessage (WPARAM MsgCode, LPARAM MsgData);

	double m_nExposureTime;
	int m_nFL_LampState;
	int m_nIntensity;
	int m_nLampAge;

	virtual BOOL Create(CWnd* pParentWnd) ;
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
	afx_msg void OnEnKillfocusFlLampExposure();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedFlLampAgeBtn();
	afx_msg void OnCbnCloseupFlLampIntensity();
	CComboBox m_nIntensitySetting;
	afx_msg void OnBnClickedFlLampTimedExp();
	CButton m_nTimedExposure;

private:
	BOOL m_bShutterOpen;
	BOOL m_bLampOn;

public:
	afx_msg void OnBnClickedFlShutterControl();
	afx_msg void OnBnClickedFlLampLampControl();
	CEdit m_SoftwareVersion;
	afx_msg void OnBnClickedFlLampClearAlarm();
	afx_msg LRESULT OnMicMessage(WPARAM, LPARAM);
};

//#endif

