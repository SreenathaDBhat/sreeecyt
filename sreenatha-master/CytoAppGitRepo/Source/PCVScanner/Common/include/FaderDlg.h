#pragma once


// CFaderDlg dialog
class AFX_EXT_CLASS CFaderDlg : public CDialog
{
	DECLARE_DYNAMIC(CFaderDlg)

public:
	CFaderDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFaderDlg();
	BOOL Create(CWnd *pParentWnd = NULL);

	CSliderCtrl	m_fade_sb;
	LinkedInfo *m_pLinkedInfo;
	FrameViewer * m_pFrameViewer;
	CView *m_pView;
	CWnd *m_pParWnd;

// Dialog Data
//	enum { IDD = IDD_DIALOG_FADER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	virtual BOOL OnInitDialog();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnRemoveLink();

	DECLARE_MESSAGE_MAP()
};
