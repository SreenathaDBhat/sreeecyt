#ifndef _FFT_H_
#define _FFT_H_

typedef unsigned char Uchar;

class CFFT {

public :
	CFFT();
	virtual ~CFFT();

	void get_fft_size(int imcols, int imlines, int *xoff, int *yoff, int *newcols, int *newlines);
	Uchar val (Uchar *image, int x, int y, int cols, int lines);
	void fourn(float data[], unsigned long nn[], int ndim, int isign);
	void im_fft(Uchar *image, int imcols, int imlines, float *result, int resx, int resy, int rescols, int reslines);
	void fft_image_reg_core(float *image0_fft, float *image1_fft, int newcols, int newlines, int *xshift, int *yshift, int maxshift);
	void fft_image_reg(Uchar *image0, Uchar *image1, int cols, int lines, int *xshift, int *yshift, int maxshift);
	void fft_quick_image_reg(float *image0_fft, Uchar *image1, int cols, int lines, int *xshift, int *yshift, int maxshift);
	void fft_quick_area_image_reg(float *image0_fft, Uchar *image1, int cols, int lines, int xoff, int yoff, int newcols, int newlines, int *xshift, int *yshift, int maxshift);
	void clearImageEdges(unsigned char *image1, int cols, int lines, int edge);

	void fft_power (Uchar *image, int imcols, int imlines, float *result, int resx, int resy, int rescols, int resline);

private:

};

#endif