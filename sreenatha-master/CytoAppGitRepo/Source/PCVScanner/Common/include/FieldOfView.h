#if !defined(AFX_FIELDOFVIEW_H__4CE43803_4A27_426A_B35E_7CC503913368__INCLUDED_)
#define AFX_FIELDOFVIEW_H__4CE43803_4A27_426A_B35E_7CC503913368__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FieldOfView.h : header file
//
#include "MicroscopeDialog.h"


/////////////////////////////////////////////////////////////////////////////
// CFieldOfView dialog

#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

class MICROSCOPEGUI_API CFieldOfView : public MicroscopeDialog
{
// Construction
public:
	CFieldOfView(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag);
	BOOL IsCalibrated();

// Dialog Data
	//{{AFX_DATA(CFieldOfView)
	//enum { IDD = IDD_FIELDOFVIEWBASE };
	int		m_nOverlap;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFieldOfView)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFieldOfView)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnFovDown();
	afx_msg void OnFovLeft();
	afx_msg void OnFovRight();
	afx_msg void OnFovUp();
	afx_msg void OnKillfocusFovOverlap();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIELDOFVIEW_H__4CE43803_4A27_426A_B35E_7CC503913368__INCLUDED_)
