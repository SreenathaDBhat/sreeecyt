#if !defined(AFX_FILTERDLG_H__C1AC66A5_CE21_4B18_AC14_1B8E7D1DEA45__INCLUDED_)
#define AFX_FILTERDLG_H__C1AC66A5_CE21_4B18_AC14_1B8E7D1DEA45__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FilterDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFilterDlg dialog
#define IDD_FILTERDLG                   17022

typedef struct kstructtag
{
	int data[225];
	int hwidth;
} kstruct;

class AI_Tiff;
class CSRDrawWnd;
// Render callback
typedef void (*F_RENDERCB)(void *data);

class AFX_EXT_CLASS CFilterDlg : public CDialog
{
// Construction
public:
	CFilterDlg(AI_Tiff *tiff, int index, CWnd* pParent = NULL);   // standard constructor
	void AttachRenderCB(F_RENDERCB pRender, void * pObj);

// Dialog Data
	//{{AFX_DATA(CFilterDlg)
	enum { IDD = IDD_FILTERDLG };
	CComboBox	m_filterlst;
	CSliderCtrl m_radiusSlider;
	CSliderCtrl m_depthSlider;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFilterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFilterDlg)
	afx_msg void OnApply();
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeFilterlist();
	afx_msg void OnUndo();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void FillCombo();
	BOOL ReadKernel(LPCTSTR);
	void UpdateBuddy(CSliderCtrl *pSlider);

	kstruct		current_kernel;		// Current filter kernel to use in op
	CSRDrawWnd*	m_pTiffWnd;			// Window that displays the tiff, need to update when filter is applied.
	AI_Tiff*	m_pTiff;			// Tiff object that contains Components we wish to manipulate, displayed in m_pTiffWnd
	int			m_index;			// Index of component in m_pTiff to manipulate
	int			m_undoCount;		// Keep track of the number of filter operations;

	F_RENDERCB m_renderCB;
	void * m_renderObj;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILTERDLG_H__C1AC66A5_CE21_4B18_AC14_1B8E7D1DEA45__INCLUDED_)
