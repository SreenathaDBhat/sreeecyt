#pragma once

#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>





//#ifdef _DEBUG
//#define new new(_NORMAL_BLOCK, THIS_FILE, __LINE__)
//#undef THIS_FILE
//static char THIS_FILE[] = __FILE__;
//#endif

class FindMemoryLeaks
{
	_CrtMemState m_checkpoint;
public:
	FindMemoryLeaks()
	{
		_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
		_CrtMemCheckpoint(&m_checkpoint);
		_CrtMemDumpAllObjectsSince(&m_checkpoint);
	};

	~FindMemoryLeaks()
	{
		_CrtMemState checkpoint;
		_CrtMemCheckpoint(&checkpoint);
		_CrtMemState diff;
		_CrtMemDifference(&diff, &m_checkpoint, &checkpoint);
		_CrtMemDumpStatistics(&diff);
		_CrtMemDumpAllObjectsSince(&m_checkpoint);
	};
};