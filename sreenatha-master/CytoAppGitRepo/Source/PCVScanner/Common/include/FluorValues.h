// FluorValues.h : header file
//

#define COLOUR_BUTTON_XOFFSET 15 // offset from text widget
#define COLOUR_BUTTON_YOFFSET 20 // offset from top of CFluorValues dialog
#define COLOUR_WIDTH 20
#define COLOUR_HEIGHT 20

/////////////////////////////////////////////////////////////////////////////
// CFluorValues dialog

#define WM_DELETE_CLASS	WM_USER + 1
#define WM_NORMAL_CLASS	WM_USER + 2

class CFluorValues : public CDialog
{
// Construction
public:
	CFluorValues(CWnd* pParent = NULL);   // standard constructor
	BOOL IsCharValidforValues(char c);
	BOOL IsCharValidforClasses(char c);
	BOOL Change(char *str, BOOL values);

	BOOL m_modified; // accessed in classifier dialog code so can't be protected
	COLORREF classColour;

// Dialog Data
	//{{AFX_DATA(CFluorValues)
	enum { IDD = IDD_FC_VALUES };
	CString	m_val1;
	CString	m_val2;
	CString	m_val3;
	CString	m_val4;
	CString	m_val5;
	CString	m_classname;
	CString	m_fusion;
	CString m_CountPerClass;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFluorValues)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFluorValues)
	afx_msg void OnChangeVal1();
	afx_msg void OnChangeVal2();
	afx_msg void OnChangeVal3();
	afx_msg void OnChangeVal4();
	afx_msg void OnChangeVal5();
	afx_msg void OnDeleteCheck();
	afx_msg void OnChangeFusionVal();
	afx_msg void OnChangeClassnameVal();
	afx_msg void OnColour();
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnNormal();
	afx_msg void OnEnChangeClasscount();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CWnd* parent;
};
