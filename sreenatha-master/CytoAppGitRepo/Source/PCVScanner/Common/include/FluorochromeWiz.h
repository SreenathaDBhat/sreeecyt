///////////////////////////////////////////////////////////////////////////
// Fluorochrome (Fluorescent spectrachrome wizard) header
// K Ratcliff 16122003
///////////////////////////////////////////////////////////////////////////

#ifndef __FLUOROWIZDLL_H
#define __FLUOROWIZDLL_H

#ifdef FLUOROWIZ_EXPORTS
#define FLUOROWIZDLL_API __declspec(dllexport)
#else
#define FLUOROWIZDLL_API __declspec(dllimport)
#endif

#include "WizardDLL.H"

struct IMicSrvr;
struct IGrabber;

class CSpectraChromeList;
class CAssayList;

class FLUOROWIZDLL_API CFluorochromeWizard
{
public:
	CFluorochromeWizard(IMicSrvr* pMic, IGrabber *pGrab, CSpectraChromeList *pSpectrachromes, CAssayList *pAssays, BYTE *pCapBuf);
	~CFluorochromeWizard();

	void Run(HWND hMainWnd, CDocument *pDoc);	// Run the wizard
	WizSheet				*m_pWiz;			// Equivalent to an MFC property sheet
	CRect					WizRect;
	IMicSrvr*               m_pIMicSrvr;		// Pointer to the grabserver
	IGrabber*               m_pIGrab;			// Pointer to the micserver
	CSpectraChromeList		*m_pSpecs;			// Pointer to the default spectrachrome list
	CAssayList				*m_pAssays;			// Pointer to the assays
	BYTE					*m_pCapBuf;			// Capture Buffer
};


#endif		//	#ifndef __FLUOROWIZDLL_H