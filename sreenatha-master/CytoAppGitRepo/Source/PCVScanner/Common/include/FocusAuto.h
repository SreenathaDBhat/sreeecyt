
#pragma once

#include <Process.h>
#include <iostream>
#include <queue>
#include <deque>

using namespace std;

#include "AutoFocusCommon.h"
#include "HRTimer.h"

#include "..\..\Common\Src\NLog\src\NLogC\NLogC.h"

typedef enum
{
	ResultUnknown,
	CloseToFocus,
	VeryCloseToFocus,
	AwayFromFocus,
	TowardsFocus
} FocusResult;

// Capture/image processing struct
typedef struct
{
	void *ImageAddr;
	double FocusMeasurement;
	double TimeStamp;
} TimedImageCapture;



// Move
typedef struct
{
	float Z;
	double TimeStamp;
} TimedFocusMove;


#define MAX_FOCUS_READINGS			5000

typedef struct tagImageData
{
	long W, H, P, BPP;
	WORD *Addr;
	float ZPos;
} ImageData;

typedef queue<ImageData>  IMAGE_QUEUE;

class AutofocusValidator;

// For step and capture type focusing
class afmeasInfo
{
public:
	afmeasInfo()
	{
		ImageAddr = NULL;
		maxContrast = 0;
		focusMeasurement = 0.0;
		focusMeasurement2 = 0.0;
		Lo = 0;
		Hi = 0;
		NSat = 0;
		ImInfo = NotSaturated;
	}
	
	~afmeasInfo()
	{
		if (ImageAddr)
		{
			delete ImageAddr;
			ImageAddr = NULL;
		}
	}

	float			maxContrast;
	float			posZ;
	double			focusMeasurement;
	double			focusMeasurement2;
	WORD			Lo, Hi;
	int				NSat;
	FocusImageInf	ImInfo;
	WORD			*ImageAddr;
};


typedef deque<afmeasInfo *>  MEASUREMENT_QUEUE;

typedef struct
{
	afmeasInfo measurements[MAX_FOCUS_READINGS];
    int		nSteps;
	float	stepSize;
	int		imageWidth;
	int		imageHeight;
	int		imagesProcessed;
	float	originalZ;
	float	threshold;
    int		MeasThresh;
    int		Spacing;
	double	ObjectiveMag;
	BOOL	FluorMode;
	BOOL	CoverslipDetect;
	int		FocusDirection;
	RECT	roi;
	WORD	BitDepth;
	WORD	Bin;
	AutofocusValidator *pAFValidator;

} FocusInfo;


typedef struct
{
	MEASUREMENT_QUEUE measurements;
    int		nSteps;
	float	stepSize;
	int		imageWidth;
	int		imageHeight;
	
	float	originalZ;
	float	threshold;
    int		MeasThresh;
    int		Spacing;
	double	ObjectiveMag;
	BOOL	FluorMode;
	BOOL	CoverslipDetect;
	int		FocusDirection;
	RECT	roi;
	WORD	BitDepth;
	WORD	Bin;
	AutofocusValidator *pAFValidator;

} StepFocusInfo;

double GetDOF(double Mag);

void GetRegKey(double Objective, FocusOpts Opts, CString &RegKey);

double AFGetFromReg(LPCTSTR Value, double Default);

int AFGetFromReg(LPCTSTR Value, int Default);

#ifdef FOCUSAUTO_EXPORTS
#define FOCUSAUTO_API __declspec(dllexport)
#else
#define FOCUSAUTO_API __declspec(dllimport)
#endif

#include "CSyncObjects.h"
#include "MicHelper.h"



FOCUSAUTO_API void BacklashMove(CMicHelper &Mic, float Z, float Backlash);

class FOCUSAUTO_API FocusAuto
{
public:
	

	FocusAuto();
	~FocusAuto();

	float ContinuousFineEnvelopeFocus(double ObjectiveMag, float StartPoint, float Range,  FocusOpts Type, double Threshold, BOOL CoverSlip = FALSE, AutofocusValidator *pAFValidator = NULL);

	BOOL IsValidated(void) { return Validated; }

private:
	void CreateThreadSyncObjects(void);
	void DeleteThreadSyncObjects(void);
	void LaunchContinuousFocusThreadsAndWait(void);

	void FocusDeltaContinuous(double Mag, double &DOF);

	void DumpTheData(double T);
	float FindZ(double T);

	void BackgroundSubtractFocus(void);

	float MoveToFocusCoverslip(void);
	float MoveToFocusNoCoverslip(BOOL& AtEndOfRange);
	void ContinuousFocusMove(void);

	// The threads
	static int ContinuousProcessFrames(LPVOID pParam);
	static int ContinuousCaptureFrames(LPVOID pParam);
	static int ZonedContinuousProcessFrames(LPVOID pParam);

    CRITICAL_SECTION     csMoveCritical;            // Locks scan and capture
    CRITICAL_SECTION     csIPCritical;              // Locks capture and image processing
    CSyncObjectEvent     *pMoveRdyEvent;			// Move thread ready to go
    CSyncObjectEvent     *pGrabRdyEvent;			// Grab thread ready to go
    CSyncObjectEvent     *pStartEvent;              // Synchronise them all
	CSyncObjectSemaphore *pImageDataReady;			//
    CSyncObjectEvent     *pEndOfMoveEvent;          // End of move
    CSyncObjectEvent     *pEndOfCaptureEvent;       // Finished capturing data

	// Pipe for communicating images 
    HANDLE              hWriteImagePipe;
    HANDLE              hReadImagePipe;
	

    // Thread handles and ids
    HANDLE              hCaptureThread;
    HANDLE              hImageProcessingThread;
	DWORD               FocusThreadId;
	DWORD               CaptureThreadId;
	DWORD               ImageProcessingThreadId;

	WORD				imageWidth;
	WORD				imageHeight;

	WORD				*pFocusImage;
	WORD				BitDepth;
	float				InitialFocusPos;
	float				FocusRange;
	float				FocusSpeed;
	float				MaxFocusSpeed;
	double				Magnification;
	double				FrameRate;
	double				FocusThreshold;

	FocusOpts			FocusType;

	TimedFocusMove      ZMoveData[MAX_FOCUS_READINGS];
	int					NumZMoves;
	TimedImageCapture   CaptureData[MAX_FOCUS_READINGS];
	
	int					NumCaptures;
	BOOL				Validated;

	CHRTimer			HRT;

	float			    Backlash;

};