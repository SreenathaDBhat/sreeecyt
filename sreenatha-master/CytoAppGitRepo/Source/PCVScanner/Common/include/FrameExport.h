#pragma once

//#include <gdiplus.h>
//using namespace Gdiplus;

// CFrameExport dialog
class FrameViewer;

class AFX_EXT_CLASS CFrameExport : public CDialog
{
	DECLARE_DYNAMIC(CFrameExport)

public:
	CFrameExport(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFrameExport();
	BOOL GenerateBitmapFromRect(CRect &rect, FrameViewer *pfv, CDC * pDC);
	BOOL SaveAsFormat(LPCTSTR format, LPCTSTR path, ULONG qulaity);
	int GetEncoderClsid(const WCHAR* format, CLSID* pClsid);

// Dialog Data
//	enum { IDD = IDD_DIALOG_EXPORTSECTION };
	CComboBox	m_format_cb;
	CString m_format;
	CString m_filepath;
	CSliderCtrl	m_resolution_sb;
	CSliderCtrl	m_quality_sb;
	int m_qualitypos;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSelchangeFormat();

	DECLARE_MESSAGE_MAP()

	BOOL m_bInitOK;
	ULONG_PTR gdiplusToken;
	BYTE * m_bits;
	BITMAPINFO m_bminfo;
	double m_resolution;
	int m_resolutionpos;
	FrameViewer *m_pFv;
	CDC*				m_pDC;
	CRect				m_rect;

	unsigned long CalculateMaxMem(unsigned long required);
};
