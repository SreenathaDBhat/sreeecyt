/*
 *		G E N E T I X M O T O R C O N T R O L L E R . H
 *			Genetix specific motor control
 */

#ifndef _GENTIXCONTROL_H
#define _GENETIXCONTROL_H

#include "MotorController.h"
#include "Axes.h"
#include "GtxSlideStage.h"

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the IMSMOTORCONTROLLER_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// IMSMOTORCONTROLLER_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef GENETIXMOTORCONTROLLER_EXPORTS
#define GENETIXMOTORCONTROLLER_API __declspec(dllexport)
#else
#define GENETIXMOTORCONTROLLER_API __declspec(dllimport)
#endif


// This class is exported from the GENETIXcontrol.dll
class CGenetixMotorController : public CMotorController
 {
public:
		CGenetixMotorController();
		~CGenetixMotorController();

					//general control functions
		int SetUp(int AxisID, int iPortAddr, AxisData *AxisControl);
		int ShutDown();
		int SetControlState( int state);
		void SetMode(int mode);
		int AutoDetect();
		int Status();
		int Busy();
		int Wait();
		int Stop();

					//stage (x,y,z) control functions
		int MoveXYTo (float xpos, float ypos);
		int MoveTo (int pos);
		int MoveBy (int dist);
		int MoveTo (float pos);
		int MoveBy (float dist);
		int MoveXYBy (float xdist, float ydist);
		int SetPosition (int pos);
		int GetPosition (int *pos);
		int SetPosition (float pos);
		int GetPosition (float *pos);
		int GetAbsPosition (int *pos);
		int GetAbsPosition (float *pos);
		int SetSpeed (int axis1, int speed1, int axis2, int speed2);
		int SetSpeed (float speed, int *StepsPerSecond);
		int SetSpeed (int speed);
		int ResetStage (); 
		int DatumFound();
		int GetMaxPosition (double *pMaxPos);

					//filterwheel control functions
		int InitRotor (int PortNo, AxisData *Data);
		int Home();
		int Origin();
		int MoveToElement (int PartNo, int waitmove);
		int WheelWait();
		int GetCurrentElement (int *Position);

				//Slide loader control functions
		int SL_GetStatus();
		int SL_SelectTray (int iTrayNumber);
		int SL_LoadSlide (int iSlidePosition, BOOL ReadBarcodes);
		int SL_RemoveSlide();
		int SL_NumberOfTrays();
		int SL_NumberOfSlidesPerTray();
		int SL_CurrentTray(int *CurrentTray);
		int SL_GetTrayTampered(int *Trays);
		int SL_MoveRobotHeadAboveStage();
		int SL_GetOccupiedTrayPositions (int *Trays);
		int SL_ScanSlidePositions();
		int SL_GetOccupiedSlidePositions(int *Slides);
		int SL_FinishCalibration();
		int SL_SwapTray(int iTrayPosition, BOOL ReadBarcodes);
		int SL_GetSlideBarcode (int iSlidePositions, char **strBarcode);
		int SL_GetSlideBarcode_From_Index (int iSlideIndex, char **strBarcode);
		int SL_GetSlideBarcodesOnTray (char *astrBarcodes,int max_string_length, int *NBarcodes);
		int SL_ScanTrayPositions(int *Trays);
		int SL_IsTrayLoaded();
		int SL_LoadSlideAndMove (int iSlidePosition, BOOL ReadBarcodes,float xposition, float yposition);
		int SL_SwapTrayAndMove(int iTrayPosition, BOOL ReadBarcodes, float xposition, float yposition);
		int SL_HasDoorBeenOpened (BOOL Reset);
		int	SL_Park();
		int SL_CassetteJog(double Amount);
		int SL_CassetteMove (double Pos);


			// Oil Dispenser
		int BeginOiling();
		int EndOiling();
		int DispenseOil();

		int GetSystemPosition(int SysPos, float *x, float *y);
		int SL_GetSystemInfo(int SysInfo, float *param);
		int SL_IsTrayDetected();
		int SL_IsDoorOpen (BOOL *Open);


private :
		int TranslateErrorCode(int errCode);
		int m_AxisID;				//ID of axis 
		BOOL m_Open;
		int m_ErrorCode;
		char  m_Name;

		int Open(int portno);
		int Close();
		void GetBarcodes();
		void* pPortal;

};

	//These 2 functions ONLY are exported by dll
extern "C" GENETIXMOTORCONTROLLER_API CMotorController *CreateGenetixMotorController(void);
extern "C" GENETIXMOTORCONTROLLER_API void  DestroyGenetixMotorController(CMotorController *device);


#endif