// A folder browser

#pragma once

#ifdef HELPER3_EXPORTS
#define HELPER3_API __declspec(dllexport)
#else
#define HELPER3_API __declspec(dllimport)
#endif

BOOL HELPER3_API GetFolder(CString* strSelectedFolder,
				           LPCTSTR lpszTitle,
				           const HWND hwndOwner, 
				           LPCTSTR strRootFolder, 
				           LPCTSTR strStartFolder);