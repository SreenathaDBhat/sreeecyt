/*
 *		G R A B C A P A B I L I T E S . H
 *
 *		-- define camera capabilites
 */

#ifndef _GRABCAPABILITES
#define _GRABCAPABILITES

typedef enum
{
    BFFocusMode = 0,        // Focussing modes
    FLFocusMode = 1
} GrabberFocusMode;

		//identifiers for adjustment function suitable for use with a given camera
#define	ADJUST_GAIN_AND_OFFSET 1
#define	ADJUST_EXPOSURE 2
#define	ADJUST_EXPOSURE_AND_LUT_STRETCH 3
#define	ADJUST_INCREMENTAL 4
#define	CALIBRATE 5

		//identifiers for autofocus function suitable for use with a given application
#define FOCUS_DEFAULT	1
				

	enum GrabCapabilities		// Capabilities masks
	{
		capROI				= 0x00000001,	// Supports a Region of interest
		capOverlayCB		= 0x00000002,	// Supports an overlay Callback
		capMeasCB			= 0x00000004,	// Supports a measurement Callback
		capGainCtrl			= 0x00000008,	// Supports Gain control, Call MaxGain for limits
		capOffsetCtrl		= 0x00000010,	// Supports Offset control, Call MaxOffset for limits
		capExposeCtrl		= 0x00000020,	// Supports Exposure control, Call MaxExposure for limits
		capBinning			= 0x00000040,	// Supports Binning 1 - 4
		capLastCapture		= 0x00000080,	// Supports use last grabbed image for capture
		capInputLUT			= 0x00000100,	// Supports an applied input lut
		capOutputLUT		= 0x00000200,	// Supports an applied output lut
		capDisplayOffset	= 0x00000400,	// Supports an offset being applied to the displayed image
		capAutoSettings		= 0x00000800,	// Supports auto capture
		capStatusCB			= 0x00001000,	// Supports a exposure status message callback
		capNoAlign			= 0x00002000,	// Supports non DWORD alignment on ROI
		capStretchDisplay	= 0x00004000,	// Supports live image stretch on display
		capHQuality			= 0x00008000,	// A HighQuality mode is available (may be slower)
		capAutoCalibrate	= 0x00010000,	// Supports calibration of grabber settings
		capRGB				= 0x00020000,	// RGB colour camera supported	
        capGainOffsetLUT    = 0x00040000,   // Camera uses the LUT to adjust gain and offsets
		capAutoSetLimits    = 0x00080000,	// Can adjust target end points of the autosetup
		capPartialScan		= 0x00100000,	// Partial scanning
		capFluorescentFocus	= 0x00200000,	// Use binning for focusing - specifically fluorescent work
	};

/*
-- removed temp for Cytovision integration
typedef struct {
		char name[16];
		short maxw, maxh;
		WORD def_gain, def_offset;
		BYTE binx, biny;
		} GrabberInfo;
*/


#endif