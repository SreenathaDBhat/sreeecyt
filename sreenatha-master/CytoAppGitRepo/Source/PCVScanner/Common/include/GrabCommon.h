///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//		GRABBERCOMMON.H
//          Common grabber defs
//		channel definitions
//		image format definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
#pragma once

#define GREEN_CHANNEL	1
#define RED_CHANNEL		2
#define BLUE_CHANNEL	3
#define MONO_CHANNEL	1


#define MONO		1
#define XRGB		2

// Various events - and there are lots of them !
#define GRABEXPOSUREEVENT				_T("GrabExposureEvent")
#define GRABAUTOSETTINGSKILLEVENT		_T("AutoSettingMonitorThreadKill")
#define GRABAUTOSETUPMUTEX				_T("AutoSetupMutex")
#define GRABAUTOSETUPEVENT				_T("AutoSettingEvent")
#define GRABAUTOSETUPFINISHEVENT		_T("AutoSettingEventFinish")
#define GRABSTARTEVENT				_T("GrabStartEvent")
#define GRABCOMPLETEEVENT			_T("GrabCompleteEvent")
#define IMAGEREADYEVENT				_T("ImageReadyEvent")
#define GRABABORTEVENT				_T("GrabAbortEvent")
#define GRABBERTERMINATEEVENT		_T("GrabberTerminateEvent")
#define GRABTIMEOUTEVENT			_T("GrabTimeOutEvent")
#define IMAGEREADYFORCLIENTEVENT	_T("ImageReadyForClientEvent")
#define IMAGEMEMORYMUTEX			_T("ImageMemoryMutex")
#define	LUTMEMORYMUTEX				_T("LUTMemoryMutex")
#define CAPTUREMUTEX				_T("CaptureMutex")
#define CAPTURED					_T("ImageCaptured")
#define FREERUNNINGEVENT			_T("FreeRunningEvent")
#define ABORTCOMPLETEEVENT			_T("AbortCompleteEvent")
#define GRABAPPLYSETTINGSEVENT      _T("GrabApplyEvent")

// Autosettings status

typedef enum
{
	Complete = 0,
	Failed = 2,
	Continue = 1
} AutoSettingsStatus;


// Indication of wether the native exposure range is correct
typedef enum tagExposureState
{
	ExposureTooLow,
	ExposureInRange,
	ExposureTooHigh,
	ExposureOutOfRange,
	ExposureUndefined,
} ExposureState;