//=================================================================================
// GrabDevice.h
// Base class for any frame grabber interface. 
//
// History:
//
// 23Feb00	WH:	original
// 01Mar01	WH: Added HQuality mode (for CoolSnap at the moment)
// 08Apr03	AR: Added support for RGB colour cameras
//=================================================================================
#ifndef __GRABDEVICE_H_
#define __GRABDEVICE_H_

#include <dibdraw.h>

void _GRAB_DEBUG(int Type, char * name, char* message, ...);

// Typedefs of callback functions used
//typedef void (*OVERLAYPROC)(HDC dc, INT x, INT y, INT width, INT height, RECT *r);
typedef void (*MEASUREPROC)(UINT Min, UINT Max, double Focus, UINT *pHist, WORD HistSize, int Format);
typedef void (*AUTOSETPROC)(LPVOID Caller, WORD Gain, WORD Offset, UINT Exposure, BOOL BestContrast);
typedef void (*STATUSPROC)(LPVOID Caller, DWORD flag, VOID *data);
typedef void (*GRABMSGPROC)(UINT Msg, WPARAM w, LPARAM l);         // Post a grabserver message

//=================================================================================
//=================================================================================
#include <stdlib.h>
#include "CSyncObjects.h"
#include "GrabChannels.h"
#include "ConvertImage.h"
#include "CamLUT.h"
#include "Measure.h"

#define DEFAULT_FOCUS_THRESH            40

// When doing a LUT stretch this number determines the measurement points in 
// the original image histogram used to calculate the gradient and slope of the
// resulting LUT.
#define LUT_STRETCH_PC                  0.1
#define DEF_EXP1 2
#define DEF_EXP2 20
#define DEF_EXP3 100
#define DEF_EXP4 500
#define DEF_EXP5 1000
#define DEF_EXP6 2000

// Base class
class CGrabDevice
{

public:

    CGrabDevice();
	virtual ~CGrabDevice();


	// CONTROL
	virtual BOOL Initialize(HWND hWnd, char *cfgDir = NULL);
	virtual BOOL AssignWnd(HWND hWnd) { return FALSE;};
    HWND    GetWnd(void) { return m_hWnd; };
	virtual BOOL ShutDown() {return TRUE;};
	const char *GetName() { return m_name;};

	// DISPLAY
	virtual BOOL Display(BOOL on);
	virtual BOOL Grab(BOOL on);
	virtual BOOL LastCapture(BOOL on);
	virtual BOOL AutoSettings(BOOL on);
	virtual BOOL AutoCalibrate(BOOL on);
	virtual BOOL Capture(BYTE * pBuf = NULL);
    virtual BOOL CaptureRAW(BYTE * pBuf);
	virtual int  ImageReady(BOOL bWait, UINT timeout);
	virtual BOOL UploadImage(BYTE * pBuf, UINT x, UINT y, UINT w, UINT h) { return FALSE;};
	virtual BOOL AbortGrab() { return TRUE;};


	// LUTS
	virtual void InputLUTEntry(UINT index, UINT Grey) {};
	virtual void OutputLUTEntry(UINT index, UINT red, UINT green, UINT blue) {};
	virtual void GammaLUT(float gamma){};
	virtual void Inverted(BOOL on) { m_bInverted = on;};	// Backwards compat. only

	// SETTINGS
	virtual void Gain(WORD gain) {};
	virtual void BitShift(WORD bitshift) {};
	virtual void Offset(WORD offset) {};
	virtual void Exposure(UINT mSecs) {};
	virtual void Binning(WORD binning) {};
	virtual void DisplayOffset(UINT x, UINT y);
	virtual WORD Gain(){return m_gain;};
	virtual WORD BitShift(){return m_bitshift;};
	virtual WORD Offset(){return m_offset;};
	virtual UINT Exposure(){return m_exposure/m_micro_setting;};
	virtual WORD Binning() {return m_binning;};
	virtual POINT& DisplayOffset() {return m_dispOffset;};
	virtual WORD BitDepth() {return m_bitdepth;};
	virtual void StretchDisplay(BOOL on, RECT *pRect);
	virtual RECT &StretchDisplay(){return m_stretchrect;};
	virtual void HighQuality(BOOL on){m_HQuality = on;};
	virtual BOOL HighQuality() {return m_HQuality;};
    virtual void SendSettings(void);

	virtual void MicroExposure (int Channel, UINT microSecs) {};

	// AutoSettings parameters
	virtual void SetAutoLimits(WORD gain, WORD offset, UINT expose, WORD acceptableContrast);
	virtual void GetAutoLimits(WORD * gain, WORD * offset, UINT *expose, WORD *contrast);

	//Limits
	WORD MaxGain(){ return m_MaxGain;};
	WORD MaxOffset(){return m_MaxOffset;};
	UINT MaxExposure(){return m_MaxExposure;};	//units milli seconds
	UINT MaxImageWidth(){return m_MaxWidth;};
	UINT MaxImageHeight(){return m_MaxHeight;};
	UINT MicroMaxExposure(){return m_MaxExposure * m_micro_setting;};			//unit microseconds

	// Camera
	virtual BYTE Camera() {return m_CurrCamera;};
	virtual BOOL Camera(BYTE camera) {return (camera) ? FALSE : TRUE;};

	// Colour Cameras (Channel 0 == red, 1 == green, 2 == blue)
	virtual void Gain(int Channel, WORD gain) {};
	virtual void Offset(int Channel,WORD offset) {};
	virtual void Exposure(int Channel,UINT mSecs) {};
	virtual WORD Gain(int Channel){return m_channel_gain[Channel];};
	virtual WORD Offset(int Channel){return m_channel_offset[Channel];};
	virtual UINT Exposure(int Channel){return m_channel_exposure[Channel]/ m_micro_setting;};
	virtual UINT MicroExposure(int Channel){return m_channel_exposure[Channel];};

	WORD MaxGain (int Channel){return m_MaxGain;};
	WORD MaxOffset (int Channel) {return m_MaxOffset;};
	DWORD MaxExposure (int Channel) {return m_MaxExposure;};


	// THREAD STUFF	(Public as the thread itself needs to see it)
	virtual void DoBackground() {};
	BOOL RunBackground();
	DWORD m_hFnThread;				// Handle to thread, non NULL if running
virtual void StopGrab(){};
	// CALLBACKS
	virtual void SetOverlayCallback(DDOVERLAYPROC pfnOverlay) { m_fnOverlay = pfnOverlay;};
	virtual void SetMeasureCallback(MEASUREPROC pfnMeasure) { m_fnMeasure = pfnMeasure;};
	virtual void SetAutosetCallback(AUTOSETPROC pfnAutoset, LPVOID pCallData) { m_fnAutoset = pfnAutoset; m_fnAutosetData = pCallData;};
	virtual void SetStatusCallback (STATUSPROC  pfnStatus,	LPVOID pCallData) { m_fnStatus = pfnStatus; m_fnStatusData = pCallData;};
    virtual void SetPostMsgCallback(GRABMSGPROC pfnPoster) { m_fnMsgProc = pfnPoster; };
    // ROI
	virtual void DefineROI();
	virtual void SetROI(RECT *r);
	virtual void SetROI(int x, int y, int w, int h);
	RECT &GetROI() { return m_roi;};

    virtual void SetSpectraChromeLUT(double Grad, double Inter);    // Set the LUT for spectrachrome correction
    virtual void GetSpectraChromeLUT(double *Grad, double *Inter);  // Calculate and get the spectrachrome correction
	// LUT stretch automatically determines the gain and offset LUTS based on current image measurements
    virtual BOOL LUTStretch(WORD *pBuf, WORD Lo, WORD Hi);

    // What can we do
	DWORD Capabilities() {return m_capabilities;};

public:
	#include "GrabCapabilities.h"

	int m_Format;				// Image format (Mono or XRGB)
	WORD m_AdjustmentFnCode;	// Adjustment function for auto setting appropriate for camera
    double  m_FocusValue;       // For focussing

private:
	BOOL StartBackground();			// Returns true if m_BgFlags state requires the thread to be running
    void CaptureRAWAndMeasure(WORD *pBuf, int Integ, WORD *pLo, WORD *pHI);

protected:
	WORD m_bgFlags;					// BackGround flag state bits

	enum BgFlagsType				// Background thread state flags
	{
		BG_DISPLAY		= 0x0001,
		BG_GRAB			= 0x0002,
		BG_CAPTURE		= 0x0004,
		BG_LASTCAPTURE	= 0x0008,
		BG_AUTOSETTINGS	= 0x0010,
		BG_AUTOCALIBRATE= 0x0020
	};

	HWND m_hWnd;					// Display window handle
	HANDLE m_hImageReady;			// Image ready event handle

	// Grabbed Image dimensions
	RECT m_roi;						// Current Region of Interest
	UINT m_Width;					// Current grabbed width setting, matches m_roi
	UINT m_Height;					// Current grabbed height setting, matches m_roi
	POINT m_dispOffset;				// Display offset, (default display position is the m_roi)
	RECT m_stretchrect;				// Rectangle to stretch image display into

	// Maximum dimensions
	UINT m_MaxWidth;				// Maximum width of grabbed image (unbinned)
	UINT m_MaxHeight;				// Maximum height of grabbed image (unbinned)

	// Current settings
	WORD m_gain;					// Current gain
	WORD m_offset;					// Current offset
	UINT m_exposure;				// Current exposure time (mSecs)	
	WORD m_binning;					// Current Binning	2, 4, 8 etc (Don't support seperate binning in each plane)
	WORD m_max_binning;				// Maximum binning capability of camera
	WORD m_bitshift;				// Current bitshift	1, 2, 3, 4, etc (Don't support seperate binning in each plane)

	WORD m_micro_setting;			//conversion factor for exposure setting

	WORD m_channel_gain[4];
	WORD m_channel_offset[4];
	UINT m_channel_exposure[4];
	BOOL m_gain_changed;
	BOOL m_offset_changed;
	BOOL m_exposure_changed;

	// Maximum settings
	WORD m_MaxGain;					// Maximum gain	supported
	WORD m_MaxOffset;				// Maximum offset supported
	UINT m_MaxExposure;				// Maximum exposure time (mSecs) supported

	// Auto settings limits
	WORD m_AutoAcceptableContrast;
	WORD m_AutoLimitGain;
	WORD m_AutoLimitOffset;
	UINT m_AutoLimitExposure;

	// Cameras
	BYTE m_MaxCameras;				// Maximum number of cameras selectable
	BYTE m_CurrCamera;				// Current selected camera

	BYTE *m_captureBuf;				// Pointer to buffer for captured images (passed in)

	// Callback Function pointers
	DDOVERLAYPROC m_fnOverlay;      // Overlay callback
	MEASUREPROC m_fnMeasure;		// Measurements callback
	AUTOSETPROC m_fnAutoset;		// Autosetting monitoring callback
	LPVOID		m_fnAutosetData;	// Pointer passed from caller, usually pointer to its self
	STATUSPROC  m_fnStatus;			// Status mointoring callback
	LPVOID		m_fnStatusData;		// Pointer passed from caller, usually pointer to its self
    GRABMSGPROC m_fnMsgProc;        // Post a message to the client with the grabserver locked

	// Capabilities flags
	DWORD m_capabilities;			// Capabilities, masked with the GrabCapabilities masks
	WORD m_bitdepth;				// Bitdepth og grabbed data
	char m_name[56];				// CGrabDevice name

	DWORD m_FrameCount;				// Frame count, reset to 0 by calling Grab(TRUE)
	long m_grabtime;				// For timeouts, set by calling capture)_
	BOOL m_bInverted;				// Indicated if input lut is inverted (Backwards compat with CV only)
	char m_cfgDirectory[_MAX_PATH];	// Directory to look for any config files
	BOOL m_HQuality;				// High Quality mode (use best possible settings)

    CCamLUT     *m_pCamLUT;         // LUT camera conversion stuff

    CMeasurements   *m_pMeasure;    // Measurements

    bool        m_bRawMode;         // Set TRUE to NOT use LUT conversion to 8 bits - in which case the caputre buffer must be big enough to hold the results
    

public:
    virtual void PostGrabberMessage(UINT Msg, WPARAM w, LPARAM l);      // Post a message to the client

    virtual void AutoFocusMode(BOOL On, long Mode);     // Call this to get capture going as fast as possible
    bool m_bAutoFocusMode;                              // Set TRUE when device in autofocus mode
    bool m_bWasAutoFocusMode;                           // Set TRUE when just come out of autofocus mode

    int  m_Thresh;
    CSyncObjectEvent     *m_pAutoFocusEvent;            // This is an autoreset event set whenever a frame has been captured
    CSyncObjectEvent     *m_pExposureEvent;             // This is an autoreset event set whenever a new exposure has been applied
    CSyncObjectMutex     *m_pFocusValMutex;             // Control access to the focus value
    CSyncObjectCriticalSection	*m_pCriticalSection;    // Thread safety semaphore

    WORD        m_LUTHiTarget;      // Hi target for spectrachrome LUT white balancing
    WORD        m_LUTLoTarget;      // Lo target

    virtual int BestIntegration(BOOL Sat, int Exp, WORD max);
    virtual BOOL DoAutoSetup(void);
    virtual void UseLUTToStretch(void);
};

//=================================================================================
//=================================================================================

// Typedefs of the only exported functions from the dll.
// The CGrabDevice must be initialized by calling CreateGrabDevice and destroyed by calling DestroyGrabDevice.
typedef CGrabDevice * (__cdecl * INITGRABBERPROC)(void);
typedef void (__cdecl * DESTROYGRABBERPROC)(CGrabDevice *);

#endif // __GRABDEVICE_H_