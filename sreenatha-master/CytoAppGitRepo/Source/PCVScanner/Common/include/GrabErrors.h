//
//	FileName	: graberrors.h
//	Author		: A Rourke
//	Date		: October 2004
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Header File Containing Error Definitions For The Grab Server //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef graberrors_h
#define graberrors_h

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Error Codes Returned By Frame Grabber and Camera Controller Software //  
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define gbNO_ERRORS					0		//No Errors Have Occurred 
#define gbCOMMS_FAILURE				-1		//Serial Comms Problem
#define gbFGDLL_FAILURE				-2		//Failed to load frame grabber dll
#define gbrINVALID_PARAMETER		-3		//Associated Parameter Is Out Of Range
#define gbrINVALID_COMMAND			-4		//Command Sent Not Understood
#define gbEXEC_ERROR				-5		//Command Couldn't Execute
#define gbINTERFACE_ERROR			-6		//Communications Interface Problem
#define gbIMG_BUFFER				-7		//Problem with image buffer allocation
#define gbNOT_NOTIMPLEMENTED		-8		
#define gbHW_FAILURE				-9		//Failed to configure hardware
#define gbUNKNOWN_CAMERA			-10		//Camera not supported
#define gbGRAB_ERROR				-11		//Frame grabbing error
#define gbCAMDLL_FAILURE			-12		//Failed to load camera dll
#define gbCONFIGFILE_FAILURE		-13		//Failed find config files
#define gbMEMORY_MAP_ERROR			-14		//Failed to allocate memory mapped file
#define gbMEMORY_SHUTDOWN_ERROR		-15		//Failed to destroy memory mapped file
#define gbSHUTDOWN_ERROR			-16		//
#define gbVIDEO_CHANNEL_ERROR		-17		//Failed to select video channel
#define gbVIDEO_STANDARD_ERROR		-18		//
#define gbPIXEL_MODE_ERROR			-19
#define gbGRAB_WINDOW_ERROR			-20
#define gbCALLBACK_ERROR			-21

#define gbUNDEFINED_ERROR			-500    //Unknown Error Code

#endif