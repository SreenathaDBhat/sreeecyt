#ifndef _GRABBER_BOARDS_H
#define _GRABBER_BOARDS_H


typedef enum tagGRABBER_BOARDS
{
	GXCELERACL_LX1	= 0,
	GPC2CL			= 1,
	GX64_Express	= 2,
	GLeica_Firewire = 3,
	GPSEUDO			= 4,
} GRABBER_BOARDS;


static LPTSTR ControllerDLLs[] = 
						{
							_T("X64CLController.dll"),			//GXCELERACL_LX1		
							_T("X64CLController.dll"),			//GPC2CL
							_T("X64CLController.dll"),			//GX64_Express
							_T("LeicaDFCController.dll"),		//GLeica_Firewire
							_T("PseudoCamController.dll"),		//GPSEUDO
						};

//list of supported SupportedGrabbers and associated cameras
//this list should be sub-set of the SupportedGrabbers list
static LPTSTR SupportedGrabbers[]= {
							_T("Xcelera-CL_LX1"),				//GXCELERACL_LX1		
							_T("PC2CL"),						//GPC2CL
							_T("X64-Express"),					//GX64_Express
							_T("Leica_Firewire"),				//GLeica_Firewire
							_T("PseudoDevice"),					//GPSEUDO
							};


static LPTSTR PC2CL[] = {
							_T("JAI_CVM4CL"),
							_T("JAI_CVM2CL"),
							_T("JAI_A10CL"),
						};

static LPTSTR X64_Express[] = 
						{
							_T("JAI_CVM4CL"),
							_T("JAI_CVM2CL"),
							_T("JAI_A10CL"),
						};

static LPTSTR XceleraCL_LX1[] = 
						{
							_T("JAI_CVM4CL"),
							_T("JAI_CVM2CL"),
							_T("JAI_A10CL"),
						};

static LPTSTR Leica_Firewire[] = 
						{
							_T("DFC360_FX_HQ"),
							_T("DFC365_FX_HQ"),
						};

static LPTSTR PseudoDevice[] = {_T("None")};




#endif