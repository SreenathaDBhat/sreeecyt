/*
 *		GrabberControl.h
 *
 */
#ifndef	_GrabberControl_H
#define	_GrabberControl_H

#include "resource.h"
#include "./../GrabServer\GrabServer.h"
#include "CameraCtrl.h"

class CDIBDraw;
class CQueue;

class CGrabberControl
{
public:
    CGrabberControl(CWnd *pWnd); 
    ~CGrabberControl();
	BOOL Initialize();

	void Capture();
	void Live();
	void Stop();
	void Controls();
	void Overlay(int overlay_mask);
	void ClearOverlay(int overlay_mask);
	void Measurements (BOOL on);
	void ImageDimensions (WORD *width, WORD *height);

	BYTE * m_capBuf;
	IGrabber *m_pIGrab;

private:
	CWnd *pViewWnd;

	BOOL m_move;
	POINT m_dispOffsets;
	HCURSOR m_oldcur;
	CDIBDraw *m_dibdraw;
	CCameraCtrl *m_CameraControl;
	CQueue * m_Q;

	BOOL m_bDraw;
	UINT m_DrawWidth;
	UINT m_DrawHeight;

	BOOL m_overlay;
	BOOL m_measurements;
	BOOL m_stretch;
};

#endif