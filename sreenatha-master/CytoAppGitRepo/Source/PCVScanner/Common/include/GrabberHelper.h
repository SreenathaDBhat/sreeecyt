#pragma once

#include "..\Src\Grabber2\GrabServer2\_GrabServer2_i.c"
#include "..\Src\Grabber2\GrabServer2\_GrabServer2.h"
#include "GrabCommon.h"
#include "GrabCapabilities.h"
#include "MemMapFile.h"

class CoInit;
class CSyncObjectEvent;

typedef void (*pfv_bbiiuiuiui)(BYTE, BYTE, int, int, unsigned int, unsigned int, unsigned int);
typedef void (*pfv_ususus)(unsigned short, unsigned short, unsigned short);
typedef void (*pfv_usususa)(unsigned short, unsigned short, unsigned short, AutoSettingsStatus);
typedef void (*pfv_us)(unsigned short);
typedef void (*pfv_d ) (double);
typedef void (*pfv_wwufw) (WORD, WORD,unsigned int,float,WORD);

#ifdef GRABBERHELPER_EXPORTS
#define GRABBERHELPERDLL_API __declspec(dllexport)
#else
#define GRABBERHELPERDLL_API __declspec(dllimport)
#endif

class GRABBERHELPERDLL_API CGrabberHelper
{
public:
    CGrabberHelper();
    CGrabberHelper(IGrabber **pIGrab);
    ~CGrabberHelper();

    IGrabber *m_pIGrabSrvr;
	BOOL m_CoCreate;
	void CopyExistingClient (void *pIGrabber);

    void Init(void);
    BOOL InitCOMServer(IGrabber **pIGrab);
	int InitialiseHardware();
	HRESULT ShutDown();
	HRESULT Release();
	void CoUnitialize();

    BOOL Lock(void);
    BOOL UnLock(void);
	void ForceUnLock(void);
    BOOL AutoSetup(void);
    void CameraSettings(WORD *gain, WORD *offset, UINT *expose);
    WORD BestContrast(void);
  
	HRESULT GetLockState();
	HRESULT SetLockState(BOOL On);
	HRESULT AssignWindow(long wnd);
	HRESULT SetStretch(BOOL On, RECT *wRect);
	HRESULT Grab (BOOL On);
	BOOL	IsGrabbing(void);
	HRESULT Display (BOOL On);
	HRESULT DefineROI (RECT roi);
	HRESULT MaxSize (WORD *Width, WORD *Height);
	HRESULT SetHighPriority (BOOL Priority);
	BOOL ImageReady(BOOL Blocking);
    BOOL WaitExposureComplete(DWORD Millisecs);
	HRESULT GetROI (RECT *roi);
	HRESULT DumpRawImage(int i);
	HRESULT GetExposure(UINT *sexp);
	HRESULT Capture();
	HRESULT BitDepth (WORD *bitdepth);
	HRESULT GetDisplayOffset (POINT *offset);
	HRESULT SetDisplayOffset (POINT offset);
	HRESULT GetBinning (WORD *bin);
	HRESULT SetBinning (WORD bin);
	HRESULT SetPartialScan(BOOL On);
	HRESULT Measurements(BOOL On);
	HRESULT SetOverlayMask(int ov_mask);
	HRESULT ClearOverlayMask (int ov_mask);
	HRESULT SetHQualityMode(BOOL On);
	HRESULT AutoFocusMode(BOOL AFMode, BOOL BFMode);
	HRESULT CopyCapturedImage8();
	HRESULT CopyCapturedImage16();
	HRESULT SetAutoLimits(WORD max_gain, WORD max_offset, UINT max_integ, WORD req_contrast);
	HRESULT AutoSettings (BOOL On, WORD contrast, BOOL UseCSMask);
	HRESULT Capabilities (DWORD *capabilities);
	HRESULT MaxChannelGain (int Channel, WORD *gain);
	HRESULT MaxChannelOffset (int Channel, WORD *offset);
	HRESULT MaxChannelExposure(int Channel, UINT *expose);
	HRESULT GetChannelGain (int Channel, WORD *gain);
	HRESULT GetChannelOffset (int Channel, WORD *offset);
	HRESULT GetChannelExposure(int Channel, UINT *expose);
	HRESULT SetChannelGain (int Channel, WORD gain);
	HRESULT SetChannelOffset (int Channel, WORD offset);
	HRESULT SetChannelExposure(int Channel, UINT expose);
	HRESULT SetDisplayZoom (int zz);
	HRESULT IncrementalAdjust (BOOL IncAdjust);
	HRESULT SetIntegrationLimits (UINT min_expose, UINT expose);
	HRESULT OutputLUTEntry(UINT index, UINT red, UINT green, UINT blue);
	HRESULT LastCapture(BOOL On);
	HRESULT	AbortGrab(BOOL Blocking = FALSE);
	BOOL	AbortComplete();
	BOOL	FreeRunning();
	HRESULT SelectCamera(BYTE cameranum);
	HRESULT GetImageSize (WORD *w, WORD *h);
	void SetProbeAutoCameraSetup(BOOL On);

	// Adjustment of the top and bottom ends of the autosettings range
	HRESULT AutoSetMax(double Max);
	HRESULT AutoSetMin(double Min);
	void SetAutoSetupSuccess(BOOL Ok) { m_bAutoSetupOk = Ok; }
	BOOL AutoSetupSuccess(void) { return m_bAutoSetupOk; }
	// Turn on exposure reading facility
	void ExposureMeter(BOOL On);
	BOOL Displaying();
	void SetFluorescenceMode(BOOL bOn, BOOL bCytovision);
	void MaxBinning (int *max_binning);
    void SpectraChromeLUT(double Gain, double Ofs);
    void GetSpectraChromeLUT(double *Gain, double *Ofs);
    BOOL IsLocked(void);
    double AutoFocusMeasure(void);
	void SendMaskImage(BYTE *Mask, int W, int H);
	void GetMinMaxMeasurements(WORD *Min, WORD *Max);


	//capture process legacy stuff!
	//void GetGrabberInfo (GrabberInfo *Info); -- removed temp for Cytovision integration
	BYTE * GetCaptureBuffer();
	CMemMapFile m_mem;
	BOOL Busy();
	BOOL Wait (int TimeOut);
	BOOL WaitOnAutoSettings(DWORD timeout);
	
	void SetContrastCallback (pfv_bbiiuiuiui func);
	void SetAutoCapCallback (pfv_usususa func);
	void SetFocusCallback (pfv_d func);
	void SetVideoSettingsCallback (pfv_wwufw func);
    double FocusValue(void);

	pfv_bbiiuiuiui contrastCallback;
	pfv_usususa autocapCallback;
	pfv_d focusCallback;
	pfv_wwufw videosettingsCallback;

		//auto setup handler 
	static void	AutoSetupMonitorThread(CGrabberHelper *);
	DWORD AutoSetupMonitorThreadID;
	HANDLE hAutoSetupMonitorThread;
	HANDLE hAutoSetupMonitorThreadKill;
	HANDLE hAutoSetupMutex;

	//contrast setting handler 
	static void	ContrastMonitorThread(void *pObj);
	DWORD ContrastMonitorThreadID;
	HANDLE hContrastMonitorThread;
	HANDLE hContrastMonitorThreadKill;

	//focus setting handler 
	static void	FocusMonitorThread(void *pObj);
	DWORD FocusMonitorThreadID;
	HANDLE hFocusMonitorThread;
	HANDLE hFocusMonitorThreadKill;

	//video setting handler
	static void VideoSettingsMonitorThread (void *pObj);
	DWORD VideoSettingsMonitorThreadID;
	HANDLE hVideoSettingsMonitorThread;
	HANDLE hVideoSettingsMonitorThreadKill;

	void KillMonitorThreads();

	HRESULT CentreImageInDisplay (BOOL centre);
	HRESULT SetGamma (float gamma);
	HRESULT MeasureHotPixels (int HotPixelThreshold, int HotPixelLimit, int *NumHotPixels);
	HRESULT ShowHotPixels (BOOL show, BOOL mask,int *threshold, int *maxallowed, int *found);
	void Smoothing(BOOL bOn);
	BOOL GetSmoothing(void);
	void LineAverage(BOOL bOn);

	void ChangeInterface (void *pIGrab);
	void RestoreInterface ();
	IGrabber *m_pIGrabOriginal;
	DWORD GetPixelMicron(double *x_micron, double *y_micron);

	float GammaValue(void) { return m_Gamma; }

	void SetMaxExposureMode(BOOL bOn);

	void OutputDebug(void);

private:
	BOOL m_Copied;
	HRESULT m_result;
    BOOL m_bLocked;
    CSyncObjectEvent *m_pExposureEvent;
    CoInit  *m_pCoInit;
	float m_Gamma;
	BOOL m_bAutoSetupOk;
};
