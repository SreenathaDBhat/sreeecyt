#if !defined(AFX_GRABBERTYPE_H__40316C99_1173_4D67_A884_723803F0AC02__INCLUDED_)
#define AFX_GRABBERTYPE_H__40316C99_1173_4D67_A884_723803F0AC02__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GrabberType.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGrabberType dialog

class CGrabberType : public CDialog
{
// Construction
public:
	CGrabberType(CWnd* pParent = NULL);   // standard constructor
	static CString &GetRegItem(char *key, char *subkey);
	static void SetRegItem(char *key, char *subkey, const char * dllname);
	void GetRegDllItems();

// Dialog Data
	//{{AFX_DATA(CGrabberType)
	enum { IDD = IDD_GRABBER_TYPE };
	CComboBox	m_GrabberType;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGrabberType)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGrabberType)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeGrabberType();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRABBERTYPE_H__40316C99_1173_4D67_A884_723803F0AC02__INCLUDED_)
