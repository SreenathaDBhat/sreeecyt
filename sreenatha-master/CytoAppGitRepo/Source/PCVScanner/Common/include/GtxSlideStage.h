#pragma once

#include <string>
#include <vector>

#ifdef SLIDESTAGECONTROL_EXPORTS
#define GTXSLIDESTAGE_API __declspec(dllexport)
#else
#define GTXSLIDESTAGE_API __declspec(dllimport)
#endif


/// <summary>
///	Base class to provide callback facility. Derive a class from this type and
/// implement the CommandComplete method, then pass an instance of the derived
/// type to the RegisterCallBack method in GtxSlideStage.
/// </summary>
class GTXSLIDESTAGE_API CommandCompleteNotification
{
public:
	/// <summary>
	/// Method called when a command completes.
	/// </summary>
	virtual void CommandComplete() =0;
};

/// <summary>
/// Class to provide access to the Genetix Slide Loader Stage with Oil Dispener.
/// </summary>
class GTXSLIDESTAGE_API GtxSlideStage
{
public:

	/// <summary>
	/// Status of asynchronous commands.
	/// </summary>
	enum CommandResult
	{
		/// <summary>
		/// No commmand has yet executed. Initial system state.
		/// </summary>
		Idle = 0,

		/// <summary>
		/// A command is currently executing.
		/// <summary>
		Executing = 1,

		/// <summary>
		/// The last command to execute completed successfully.
		/// <summary>
		Success = 2,

		/// <summary>
		/// The last command to execute failed.
		/// <summary>
		Failure = 3,

		/// <summary>
		/// The last command to execute failed because the requisite hardware was not available.
		/// <summary>
		NotConnected = 4,
	};

	/// <summary>
	/// Static class factory method to create instances of the class.
	/// </summary>
	/// <returns>An instance of GtxSlideStage.</returns>
	static GtxSlideStage* CreateInstance();

	/// <summary>
	/// Virtual destructor.
	/// </summary>
	virtual ~GtxSlideStage();

	/// <summary>
	/// Starts up the hardware control libraries. This method blocks whilst the
	/// libraries are initialised, and devices are discovered. No other methods in
	/// this class can be called until this method completes successfully. After
	/// calling this method, call Home to prepare the hardware for use.
	/// </summary>
	/// <remarks>
	/// A call to disconnect must be made subsequent to calls to this methis, irrespective of the result of this method call.
	/// </remarks>
	/// <returns>True if the connection was successful.</returns>
	virtual bool Connect() =0;

	/// <summary>
	/// Disconnects from the hardware. This method should be called prior to destorying
	/// instances of this class, to ensure that the libraries are closed cleanly and all 
	/// open files, such as logs, are closed.
	/// </summary>
	virtual void Disconnect() =0;

	/// <summary>
	/// Homes the hardware in a safe manner, to minimise the likelihood of system damage
	/// irrespective of the initial configuration of the hardware.
	/// </summary>
	/// <remarks>
	/// This command is performed asynchronously. No other operations can be performed
	/// until this command completes.
	/// </remarks>
	/// <returns>True if the command began successfully.</returns>
	virtual bool Home() =0;

	/// <summary>
	/// Moves the oil dispensing nozzle into the dispense position.
	/// </summary>
	/// <remarks>This method blocks until the operation is complete.</remarks>
	/// <returns>True if the command completed successfully.</returns>
	virtual bool BeginOiling() =0;

	/// <summary>
	/// Moves the oil dispensing nozzle clear of the dispense position.
	/// </summary>
	/// <remarks>This method blocks until the operation is complete.</remarks>
	/// <returns>True if the command completed successfully.</returns>
	virtual bool EndOiling() =0;

	/// <summary>
	/// Dispenses a pre-determined volume of oil.
	/// </summary>
	/// <remarks>This method blocks until the operation is complete.</remarks>
	/// <returns>True if the command completed successfully.</returns>
	virtual bool DispenseOil() =0;

	/// <summary>
	/// Determines the level of oil left in the oil reservoir.
	/// </summary>
	/// <remarks>This method blocks until the operation is complete.</remarks>
	/// <returns>A positive value if the oil reservoir level was queried successfully.</returns>
	virtual int CheckOilLevel() =0;

	/// <summary>
	/// Moves the stage X axis to the specified absolute position.
	/// </summary>
	/// <param name="position">The absolute position, in mm, to move the stage to.</param>
	/// <remarks>
	/// This command is performed asynchronously. No other operations can be performed
	/// until this command completes.
	/// </remarks>
	/// <returns>True if the command began successfully.</returns>
	virtual bool MoveStageXAbsolute(float position) =0;

	/// <summary>
	/// Moves the stage Y axis to the specified absolute position.
	/// </summary>
	/// <param name="position">The absolute position, in mm, to move the stage to.</param>
	/// <remarks>
	/// This command is performed asynchronously. No other operations can be performed
	/// until this command completes.
	/// </remarks>
	/// <returns>True if the command began successfully.</returns>
	virtual bool MoveStageYAbsolute(float position) =0;

	/// <summary>
	/// Moves the stage X and Y axis to the specified absolute positions.
	/// </summary>
	/// <param name="xposition">The absolute position, in mm, to move the X axis to.</param>
	/// <param name="yposition">The absolute position, in mm, to move the Y axis to.</param>
	/// <remarks>
	/// This command is performed asynchronously. No other operations can be performed
	/// until this command completes.
	/// </remarks>
	/// <returns>True if the command began successfully.</returns>
	virtual bool MoveStageXYAbsolute(float xposition, float yposition) =0;

	virtual bool GetTrayAndMove(int position, bool readbarcodes, float xposition, float yposition)= 0;
	virtual bool SwapTrayAndMove(int position, bool readbarcodes, float xposition, float yposition)= 0;
	/// <summary>
	/// Determines the current position of the stage X axis.
	/// </summary>
	/// <remarks>This method blocks until the operation is complete.</remarks>
	/// <returns>The absolute position of the X axis.</returns>
	virtual float GetStageXPosition() =0;

	/// <summary>
	/// Determines the current position of the stage Y axis.
	/// </summary>
	/// <remarks>This method blocks until the operation is complete.</remarks>
	/// <returns>The absolute position of the Y axis.</returns>
	virtual float GetStageYPosition() =0;

	/// <summary>
	/// Loads the slide tray from the specified postion onto the stage.
	/// </summary>
	/// <param name="position">The tray position in the cassette to load from. Tray positions are zero-indexed.</param>
	/// <param name="readbarcodes">Whether to read the barcodes on the slides in the tray as the slides are loaded.</param>
	/// <remarks>
	/// This command is performed asynchronously. No other operations can be performed
	/// until this command completes.
	/// </remarks>
	/// <returns>True if the command began successfully.</returns>
	virtual bool GetTray(int position, bool readbarcodes) =0;

	/// <summary>
	/// Returns the tray currently on the stage back into the cassette position it was loaded from.
	/// </summary>
	/// <remarks>
	/// This command is performed asynchronously. No other operations can be performed
	/// until this command completes.
	/// </remarks>
	/// <returns>True if the command began successfully.</returns>
	virtual bool ReturnTray() =0;

	/// <summary>
	/// Swaps the tray currently loaded on the slide stage with the tray at the specified position. The tray currently
	/// loaded on the stage is return to the position in the cassette that it was loaded from. If no tray is currently 
	/// on the stage, the tray at the specified location is loaded without error.
	/// </summary>
	/// <param name="position">The tray position in the cassette to load from. Tray positions are zero-indexed.</param>
	/// <param name="readbarcodes">Whether to read the barcodes on the slides in the tray as the slides are loaded.</param>
	/// <remarks>
	/// This command is performed asynchronously. No other operations can be performed
	/// until this command completes.
	/// </remarks>
	/// <returns>True if the command began successfully.</returns>
	virtual bool SwapTray(int position, bool readbarcodes) =0;

	/// <summary>
	/// Returns the barcodes read (if any) for the currently loaded slide tray.
	/// </summary>
	/// <returns>An array of strings, one for each slide position. A zero length string indicates that barcode was not read.</returns>
	virtual std::vector<std::string>* GetBarcodes() =0;

	/// <summary>
	/// Determines if the last executed command (if any) has completed.
	/// </summary>
	/// <returns>True if the command began successfully.</returns>
	virtual bool IsIdle() =0;

	/// <summary>
	/// Determines the status of the last executed command (if any).
	/// </summary>
	/// <returns>The status of the last executed command.</returns>
	virtual CommandResult GetCommandStatus() =0;

	/// <summary>
	/// This method blocks until the currently executing command has completed, or the specified timeout has
	/// elapsed.
	/// </summary>
	/// <param name="milliseconds">The number of milliseconds to wait for. A value of -1 will cause the 
	/// method to block indefinately.</param>
	/// <returns>True is the command completed in the specified time, or false if the timeout expired before
	/// the command completed.</returns>
	/// <remarks>This method blocks until the operation is complete, or the timeout has elapsed.</remarks>
	virtual bool WaitForCommandComplete(int milliseconds) =0;

	/// <summary>
	/// Registers a callback method that is invoked when an asynchronous command completes.
	/// </summary>
	/// <param name="callback">An instance of a class derived from CommandCompleteNotification.</param>
	virtual void RegisterCallBack(CommandCompleteNotification* callback) =0;

	/// <summary>
	/// Displays the configuration dialog to allow settings to be modified, etc.
	/// </summary>
	/// <param name="parent">The handle of the window to use as the dialog parent.</param>
	/// <returns>True if the dialog was displayed successfully.</returns>
	virtual bool DisplayConfiguration(HWND parent) =0;

	virtual void AttachDevices() = 0;
	virtual bool Park() = 0;
	virtual bool EnableAxisLimits(bool Enable, bool Datuming) = 0;
	virtual bool MoveStageXYJog (int xoffset, int yoffset) = 0;
	virtual bool MoveStageSubXAbsolute (float position)= 0;
	virtual bool MoveCassetteAbsolute (float position) = 0;
	virtual bool MoveCassetteJog (int offset) = 0;
	virtual bool MoveStageSubXJog (int offset)= 0;
	virtual float GetStageSubXPosition () = 0;
	virtual float GetCassettePosition() = 0;
/*
void GtxSlideStageImp::CalibrateCassette()
*/
	virtual std::vector<bool>* ScanCassette()= 0;
/*
virtual string[] ReadBarcode() = 0;
*/
	virtual bool IsDoorOpen (bool *status)= 0;
	virtual bool IsTrayLoaded (bool *status)= 0;

        /// <summary>
        /// Determine whether the door has been opened since the last query. Optionally clears the 
        /// state of the opened flag.
        /// </summary>
        /// <param name="reset">Whether to reset the opened flag back to false, or leave it in its current state.</param>
        /// <returns>True if the door has been opened since the last query.</returns>
	virtual bool HasDoorBeenOpened(bool reset) = 0;

	virtual void  GetStageLoadPosition(float *x, float *y)=0;
	virtual void  GetStagePreLoadPosition(float *x, float *y)=0;
	virtual void GetStageSensePosition(float *x, float *y)=0;
	virtual void GetMotionEnvelope(float *x, float *y, float *w, float *h)=0;

	virtual long GetLastErrorCode()=0;

	virtual float GetCassetteTrayPitch() = 0;
	virtual float GetCassetteTray0Position()=0;

	virtual bool IsTrayDetected(bool *status)= 0;

/*
bool GtxSlideStageImp::IsCondenserRaised (bool *status)

void GtxSlideStageImp::SetStageLoadPosition()
void GtxSlideStageImp::SetStagePreLoadPosition()
void GtxSlideStageImp::SetStageSensePosition()
float GtxSlideStageImp::GetSubXExtendedPosition()
float GtxSlideStageImp::GetSubXRetractedPosition()
float GtxSlideStageImp::GetSubXBarcodePosition()
void GtxSlideStageImp::SetSubXExtendedPosition()
void GtxSlideStageImp::SetSubXRetractedPosition()
void GtxSlideStageImp::SetSubXBarcodePosition()
float GtxSlideStageImp::GetCassetteTray0Position()
void GtxSlideStageImp::SetCassetteTray0Position()
float GtxSlideStageImp::GetCassetteTrayPitch()
*/

};

