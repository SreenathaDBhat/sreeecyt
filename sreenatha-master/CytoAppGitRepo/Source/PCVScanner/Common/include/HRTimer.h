// HRTimer.h : main header file for the PROJECT_NAME application
// A hgih resolution timer - will do microsecond accuracy 
// Written By Karl Ratcliff

#pragma once


#ifdef CAMMANAGED
#define CHRTIMER_API
#elif defined(HELPER2_EXPORTS)
#define CHRTIMER_API __declspec(dllexport)
#else
#define CHRTIMER_API __declspec(dllimport)
#endif

#ifndef CAMMANAGED
class CHRTIMER_API CHRTimer
{
public:
    CHRTimer(void);
    CHRTimer(double Factor);
    ~CHRTimer(void);

    void    Init(void);
    BOOL    Start(void);
    BOOL    ElapsedTime(double *e);
    void    Reset(void);

    double  m_StartTime;        // Time when Start() called
    double  m_ElapsedTime;      // Time since Start() called
    double  m_AverageTime;      // Average time in between N Start() calls

    double  m_HRFrequency;      // High res timer stuff
    double  m_Divisor;

    int     m_Count;

    BOOL    m_bUsePerformanceCount;
};
#endif


