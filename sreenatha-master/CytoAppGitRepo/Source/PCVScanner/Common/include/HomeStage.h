#if !defined(AFX_HOMESTAGE_H__F401EC0D_DC3A_49FC_A43F_0B46E2040455__INCLUDED_)
#define AFX_HOMESTAGE_H__F401EC0D_DC3A_49FC_A43F_0B46E2040455__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HomeStage.h : header file
//
#include "MicroscopeDialog.h"


/////////////////////////////////////////////////////////////////////////////
// CHomeStage dialog


#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif


class MICROSCOPEGUI_API CHomeStage : public MicroscopeDialog
{
// Construction
public:
	CHomeStage(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag);

// Dialog Data
	//{{AFX_DATA(CHomeStage)
	//enum { IDD = IDD_HOMEBASE };
	CButton	m_HomeZ;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHomeStage)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CHomeStage)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnHomestage();
	afx_msg void OnHomez();
	afx_msg void OnDestroy();
	afx_msg LRESULT OnMicMessage(WPARAM, LPARAM);
	afx_msg LRESULT OnMicError (WPARAM, LPARAM);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HOMESTAGE_H__F401EC0D_DC3A_49FC_A43F_0B46E2040455__INCLUDED_)
