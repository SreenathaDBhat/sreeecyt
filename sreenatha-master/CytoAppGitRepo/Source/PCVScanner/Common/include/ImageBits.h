
// ImageBits.h
//
// Functions for converting the bit depth of images, etc.

#pragma once


#ifdef HELPER2_EXPORTS
#define IMAGEBITS_API __declspec(dllexport)
#else
#define IMAGEBITS_API __declspec(dllimport)
#endif


class IMAGEBITS_API ImageBits
{
public:
	static int GetBytesPerPixel(int bitsPerPixel){ return ((bitsPerPixel + 7) / 8); } // Assumes a whole number of bytes for each pixel.
	static int GetImageSizeInBytes(int w, int h, int bpp){ return GetBytesPerPixel(bpp) * w * h; }

	// The following functions taking a pointer to WORD for the source image, require the source or output bits per pixel
	// value to be no greater than 16. Each pixel in the source image must be contained in a two byte word, regardless of bit depth.
	// Each pixel in the output image must be contained in a whole number of bytes byte word, regardless of bit depth.
	// If outputBytesPerPixel is greater than 8, pOut should be cast to (WORD*) after calling the function.
	// Note that scan lines are assumed NOT to be padded.
	static bool ShiftToNBPP(const WORD *pSrc, int width, int height, int srcBitsPerPixel, BYTE *pOut, int outputBitsPerPixel);
	static bool ScaleToNBPP(const WORD *pSrc, int width, int height, int srcBitsPerPixel, BYTE *pOut, int outputBitsPerPixel);
	static bool GetHistogramStats(const WORD *pImage, int w, int h, int bpp, WORD &min, WORD &max, WORD &mode);
	static bool HistogramStretchToNBPP(const WORD *pSrc, int width, int height, int srcBitsPerPixel, BYTE *pOut, int outputBitsPerPixel);
};
