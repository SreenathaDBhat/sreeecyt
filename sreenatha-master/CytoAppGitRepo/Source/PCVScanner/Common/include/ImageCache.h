
/*
 * ImageCache.h
 *
 * JMB August 2001
 */


#ifndef IMAGECACHE_H
#define IMAGECACHE_H


#include <AITiff.h>

#include <afxtempl.h> // For CMap



#define USE_IMAGECACHE_MAP


class ImageCacheItem
{
friend class ImageCache;
friend class AITiffImageCache;

protected:

	CString name;

	// A cache item that is locked is not removed from the cache under
	// any circumstances. This is useful for images that are being modified.
	// A lock count is used, rather than a simple flag, to help locking by
	// multiple threads (e.g. different threads want to lock an item for
	// overlapping periods of time).
	// Note that locking is purely to prevent items being discarded from the
	// cache - it doesn't give exclusive access to an item.
	int lockCount;

	ImageCacheItem *pNext;

public:

	void Lock(){ lockCount++; }
	void Unlock(){ lockCount--; if (lockCount < 0) lockCount = 0; }
	BOOL IsLocked(){ return (lockCount > 0) ? TRUE : FALSE; }

	ImageCacheItem(){ lockCount = 0; pNext = NULL; }
};

class AITiffImageCacheItem : public ImageCacheItem
{
friend class AITiffImageCache;

protected:

	AI_Tiff *pTiff;

public:

	AI_Tiff *GetTiff(){ return pTiff; }
	void SetTiff(AI_Tiff *tiff) { pTiff = tiff; }

	AITiffImageCacheItem(){ pTiff = NULL; }
	~AITiffImageCacheItem(){ if (pTiff) delete pTiff; }
};


class ImageCache
{

protected:

	ImageCacheItem *pImageList;

	int count;
	int maxCount;

	CString sourceDirectory;

#ifdef USE_IMAGECACHE_MAP
	// This is here as an optimisation.
	CMap<CString, PCSTR, ImageCacheItem*, ImageCacheItem*> map;
#endif

	// The critical section is shared between all threads in a process and must
	// only be initialised once. It is used when accessing or modifying the
	// list to prevent other threads from modifying the list simultaneously
	// e.g. one thread deletes a pointer that another is using, resulting
	// in a crash.
	CRITICAL_SECTION criticalSection;

	ImageCacheItem *RemoveOldestItem();
	BOOL RemoveItem(ImageCacheItem *pItem);
	ImageCacheItem *InsertItem(ImageCacheItem *pNewItem);
	ImageCacheItem *Search(const char name[]);

public:

	virtual void Flush() = 0;

	void Initialise(const char imageDirectory[], int maxNumImages);
	// Note that setting the max count to be smaller than before will
	// not immediately lead to any images already in the cache being removed.
	void SetMaxCount(int size){ maxCount = size; }
	int GetMaxCount(){ return maxCount; }
	int GetCount(){ return count; }
	BOOL Full(){ if (count >= maxCount) return TRUE; else return FALSE; }
	BOOL IsCached(const char name[]){ return (Search(name) != NULL ? TRUE : FALSE); }
	const char *GetImageDirectory(){ return (PCSTR)sourceDirectory; }

	ImageCache();
	~ImageCache();
};

class AITiffImageCache : public ImageCache
{

protected:

	int maxMemoryUsage; // Kilobytes.

	unsigned int tiffDeleteCount;

	void InsertItem(AITiffImageCacheItem *pNewItem);

	AITiffImageCacheItem *Search(const char name[]);
	AITiffImageCacheItem *Search(AI_Tiff *pTiff);

public:

	// If there are multiple threads accessing the cache, you should always lock
	// at item when getting a pointer to it (e.g. use GetItemLock() or GetTiffLock())
	// otherwise the pointer may become invalid while you are using it (because it
	// has been deleted by another thread). Always call Unlock() as soon as you have
	// finished with the pointer though!
	// If locking an item, it is better to get a pointer to the the item rather
	// than to the tiff (e.g. use GetItemLock() rather than GetTiffLock(), then
	// call the item's GetTiff() function to get the tiff), as the
	// corresponding unlock function is faster (it doesn't have to search for
	// the item in the cache). A tiff pointer obtained from a cache item is
	// of course still only valid for as long as the item itself.
	AITiffImageCacheItem *GetItem(const char fileName[], BOOL noData = FALSE, BOOL loadIfMissing = TRUE, BOOL lock = FALSE,
	                              unsigned int *pDeleteCount = NULL, AITiffImageCacheItem **ppOldItem = NULL);
	AITiffImageCacheItem *GetItemIfCached(const char fileName[]){ return GetItem(fileName, TRUE, FALSE); }
	AITiffImageCacheItem *GetItemLock(const char fileName[], BOOL noData = FALSE, BOOL loadIfMissing = TRUE)
	                                             { return GetItem(fileName, noData, loadIfMissing, TRUE); }
	AITiffImageCacheItem *GetItemLockIfCached(const char fileName[]){ return GetItemLock(fileName, TRUE, FALSE); }

	AI_Tiff *GetTiff(const char fileName[], BOOL noData = FALSE, BOOL loadIfMissing = TRUE, BOOL lock = FALSE);
	AI_Tiff *GetTiffIfCached(const char fileName[]){ return GetTiff(fileName, TRUE, FALSE); }
	AI_Tiff *GetTiffLock(const char fileName[], BOOL noData = FALSE, BOOL loadIfMissing = TRUE)
	                                               { return GetTiff(fileName, noData, loadIfMissing, TRUE); }
	AI_Tiff *GetTiffLockIfCached(const char fileName[]){ return GetTiffLock(fileName, TRUE, FALSE); }

	// Unlock functions, in decreasing order of speed.
	void Unlock(AITiffImageCacheItem *pItem){ if (pItem) pItem->Unlock(); }
	BOOL Unlock(AI_Tiff *pTiff);
	BOOL Unlock(const char fileName[]);
	BOOL DeleteTiff(const char fileName[]);
	void Flush();

	// Add tiff directly to cache, optionally locking it.
	void AddTiff(AI_Tiff *pTiff, const char fileName[], BOOL lock = FALSE);

	void SetMaxMemoryUsageKBytes(int kBytes){ maxMemoryUsage = kBytes; }
	int  GetMaxMemoryUsageKBytes(){ return maxMemoryUsage; }
	int  GetMemoryUsageKBytes();
	// Call this if more data has been loaded into a TIFF since it was loaded
	// into the cache. One way this can happen is if AI_Tiff::Draw() is called
	// for an image that was originally loaded by calling GetTiff() with the
	// noData flag set to TRUE.
	void EnsureMaxMemoryUsageNotExceeded();

	void Initialise(const char imageDirectory[], int maxKbytes);

	AITiffImageCache(){ maxMemoryUsage = 1000000; tiffDeleteCount = 0; }
	~AITiffImageCache(){ Flush(); }
};



#endif