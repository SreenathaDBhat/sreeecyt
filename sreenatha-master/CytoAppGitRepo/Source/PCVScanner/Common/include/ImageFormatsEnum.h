/////////////////////////////////////////////////////////////////////////////////////////////////
// Image formats enum
/////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

// Feel free to add more formats as and when desired

typedef enum
{
    BMPFormat,
    GIFFormat,
    JPGFormat,
    TIFFormat,
    PNGFormat,
} ImageFormatType;

////////////////////////////////////////////////////////////////////////////////////////////////
// File extensions
////////////////////////////////////////////////////////////////////////////////////////////////

#define     BMP_EXTENSION           _T("BMP")
#define     JPG_EXTENSION           _T("JPG")
#define     TIF_EXTENSION           _T("TIF")
#define     PNG_EXTENSION           _T("PNG")
#define     GIF_EXTENSION           _T("GIF")

