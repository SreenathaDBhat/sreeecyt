#if !defined(AFX_IMAGEOPSDLG_H__19602832_33DB_4597_9651_F947186FDB9E__INCLUDED_)
#define AFX_IMAGEOPSDLG_H__19602832_33DB_4597_9651_F947186FDB9E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImageOpsDlg.h : header file
//
#define IDD_IMAGEOPS                    17044

class AI_Tiff;
struct ImageOp;
/////////////////////////////////////////////////////////////////////////////
// CImageOpsDlg dialog
// Render callback
typedef void (*I_RENDERCB)(void *data);

class AFX_EXT_CLASS CImageOpsDlg : public CDialog
{
// Construction
public:
	CImageOpsDlg(AI_Tiff *tiff, int index, CWnd* pParent = NULL);   // standard constructor
	void AttachRenderCB(I_RENDERCB pRender, void * pObj);
	virtual ~CImageOpsDlg();

// Dialog Data
	//{{AFX_DATA(CImageOpsDlg)
	enum { IDD = IDD_IMAGEOPS };
	CListBox	m_list;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImageOpsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CImageOpsDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnResetall();
	afx_msg void OnRemove();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


	AI_Tiff*	m_pTiff;			// Tiff object that contains Components we wish to manipulate, displayed in m_pTiffWnd
	int			m_index;			// Index of component in m_pTiff to manipulate

	I_RENDERCB m_renderCB;
	void * m_renderObj;
	ImageOp *m_copy;
	BOOL m_modified;

	void CopyList();
	void DeleteCopyList();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMAGEOPSDLG_H__19602832_33DB_4597_9651_F947186FDB9E__INCLUDED_)
