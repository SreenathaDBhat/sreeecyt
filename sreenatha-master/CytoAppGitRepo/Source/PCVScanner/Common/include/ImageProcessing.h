#ifndef _IMAGEPROCESSING_H_
#define _IMAGEPROCESSING_H_

typedef struct  image{
					struct image *next;
					int	width;
					int height;
					int area;
					int perimeter;
					int	components;
					int max_comp;
					int min_comp;
					int ave_grey;		
					int	x;
					int y;
					int z;	
					double cgx;
					double cgy;
					BYTE *data;
					} Image;

typedef struct {
					UINT x;
					UINT y;
					} Point;

typedef struct seedpoint {
					int x;
					int y;
					} SeedPoint;

typedef	struct seedlist {
					int seedcount;
					int seedslost;
					SeedPoint *seeddata;
					}SeedList;
typedef struct {
					double x;
					double y;
					} dPoint;


class CImageProcessing {

public :
	CImageProcessing();
	virtual ~CImageProcessing();

	void imagebuffer (BYTE *imagebuf, UINT width, UINT height);
	int findCross(double *cx, double *cy, double *m,double *c,BOOL closest_to_centre, int min_arm_length);
	int image_fft(double *xscale, double *yscale);
	int image_scale (double *xscale, double *yscale);
	Image * find_objects (Image *image, int maxmem, int min_area, int max_area);
	int getseed(SeedList *seedlist, int *x, int *y);
	void seed(SeedList*seedlist, int x, int y);
	int seed (Image *Iptr, SeedList *seedlist, int x, int y);

	int BlockTestImage();
	int SineXTestImage();
	int CubicSplineInterpolation(float **Patch, int N, int M, double *xpos, double * ypos);
	int Spline (float *x, float *y, int n, float yp1, float ypn, float *y2);
	int Splint (float *xa, float *ya, float *ya2, int n, float x, float *y);
	int Splint2 (float *x1a, float *x2a, float **ya, float **y2a, int m, int n, float x1, float x2, float *y);
	void round (double *x, int dp);
	int CreateCurve (double *data);


private:

	Image *image;
	Image *image1;
	Image *image2;
	Point *points;
	UINT npoints;
	dPoint *dpoints;

	void addPoint(UINT x, UINT y);
	int linefit(Point *points, UINT n, double *slope, double *incpt);
	int background_threshold(Image *image);
	int test_for_cross (double *cx, double *cy, double *m, double *c, BOOL closest_to_centre, int min_arm_length);
	int crosshairs(Image *image, double *cx, double *cy, int min_arm_length);
	int crosshairs_cog(Image *image, double *cx, double *cy, double *m, double *c, int min_arm_length);
	void addPoint(double x, double y);
	int linefit(dPoint *points, UINT n, double *slope, double *incpt);

	void image_single_threshold_apply(Image *image, int threshold);
	void image_erosion (Image *image, int half_width);
	void image_histo(Image *image, int *histo);
	void image_copy_data(Image *from, Image *to);
	void image_invert_data(Image *from, Image *to);
	Image *image_create(int width, int height);
	Image *image_createblank(int width, int height);
	Image *image_make(int width, int height, unsigned char *data);
	Image *image_dup(Image *image);
	void  image_delete(Image *image);
	void image_clear(Image *image);
	void image_clear_region(Image *image, int x, int y, int xsize, int ysize);
	void image_copy_region(Image *from, Image *to, int fx, int fy, int tx, int ty, int xsize, int ysize);

	int image_write(Image *image, FILE *file);
	Image *image_read(FILE *file, int x, int y);
	void data_dump(char *data, char *filename);
	void  image_dump(Image *image, char *filename);


};

#endif