// ImgCompState.h: interface for the CImgCompStates & CImgCompState
//                 classes.
//
//////////////////////////////////////////////////////////////////////

#pragma once


#include <AiTiff.h>

////////////////////////////////////////////////////////////////////////////
// CImgCompState describes a the state of an image component
// eg whether it's visible etc

class CImgCompState
{
public:
	CImgCompState()
		: bVisible( TRUE),
		nBitsPerPixel( 24)
	{
		colour.r = colour.g = colour.b = 0; 
	}

	CImgCompState( LPCTSTR szName, BOOL bVisible, rgbTriple colour, int nBitsPerPixel)
		: sName( szName),
		bVisible( bVisible),
		colour( colour),
		nBitsPerPixel( nBitsPerPixel)
	{
	}

	COLORREF ColorRef( void )
		{ return RGB( colour.r, colour.g, colour.b); }

	BOOL IsOverlay( void)
		{ return (nBitsPerPixel == 1)  ?  TRUE : FALSE; }

public:
	CString		sName;
	BOOL		bVisible;
	rgbTriple	colour;
	int			nBitsPerPixel;


};


typedef CArray<CImgCompState,CImgCompState&> CImgCompStates;