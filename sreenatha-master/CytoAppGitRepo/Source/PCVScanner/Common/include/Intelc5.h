#ifndef  INTELC5_H
#define INTELC5_H

#ifdef INTELC5_EXPORTS
#define INTELC5_API __declspec(dllexport)
#else
#define INTELC5_API __declspec(dllimport)
#endif


INTELC5_API void _stdcall iC5Convol2 (BYTE *mem,	long xe, long ye,	long pitch,
            long xf, long yf, long xc, long yc, long *coeffs, long rshift);

INTELC5_API void _stdcall iC5ConvolSep2 (BYTE *mem, long xe, long ye,	long pitch,
               long xn, long xc, long *xcoeffs, long xrshift,
               long yn, long yc, long *ycoeffs, long yrshift);

INTELC5_API void _stdcall iC5Filter (BYTE *mem,	long xe, long ye,	long pitch,
				long xf, long yf, long xc, long yc, long oper);

INTELC5_API void _stdcall iC5MaskNei (BYTE *mem, DWORD xe, DWORD ye,
				DWORD pitch, long oper, long rep);

INTELC5_API void _stdcall iC5MemOpMem (BYTE *mem1, BYTE *mem2, long size, WORD oper);

INTELC5_API void _stdcall iC5MaskBitwise (BYTE *mem1,
				DWORD xe, DWORD ye, DWORD pitch1,
            BYTE *mem2, DWORD pitch2,
            short oper);

INTELC5_API void _stdcall iC5MemConstMem (
					BYTE *mem1, BYTE *mem2, long size,
					long cnst, short oper);

INTELC5_API void _stdcall iC5NeiMem (BYTE *mem,	long xe, long ye,	long pitch,
            long oper, long rep);


#endif


#define AIConlyMemOp ((1L<<momMIN)|(1L<<momMAX)|(1L<<momPLUS)\
 |(1L<<momMINUS)|(1L<<momIMINUS)|(1L<<momLOWto0)|(1L<<momHIGHtoFF))

#define IntelLibNeiMemOp ((1L<<mnmMIN)|(1L<<mnmMAX)\
 |(1L<<mnmOPEN8)|(1L<<mnmCLOSE8)|(1L<<mnmBLUR)\
 |(1L<<mnmMINH2)|(1L<<mnmMAXH2)|(1L<<mnmMINV2)|(1L<<mnmMAXV2))

#define IntelLibMaskNeiOp ((1L<<mnmMIN)|(1L<<mnmMAX)\
 |(1L<<mnmOPEN8)|(1L<<mnmCLOSE8))

#define IntelLibAllSides (IPL_SIDE_TOP|IPL_SIDE_BOTTOM|IPL_SIDE_LEFT|IPL_SIDE_RIGHT)