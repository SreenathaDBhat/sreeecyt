#pragma once

#ifdef HELPER2_EXPORTS
#define CSPECLUT_API __declspec(dllexport)
#else
#define CSPECLUT_API __declspec(dllimport)
#endif

class CSPECLUT_API CLUT
{
public:
	CLUT(WORD bitdepth);
	~CLUT(void);

	// Apply input LUT to an image. Uses a gain and offset to control the slope of the LUT
	// and the offset similar to y = mx + c i.e. output = gain * input + offset
	void    Calc(double Gradient, double Offset);                        // Calc the LUT based on gain and offset settings
	void    Linear(void);                                                // Make LUT 1:1

	void    Output(void);                                                // DEBUG purposes only

	void    Apply(BYTE *pDestImage, WORD *pSrcImage, UINT W, UINT H);   // Apply it to an image - 8 bit result
	void    Apply(BYTE *pDestImage, UINT W, UINT H);
	void    Apply(WORD *pDestImage, WORD *pSrcImage, UINT W, UINT H);   // Apply it to an image - 16 bit result
	void    Apply(WORD *pDestImage, UINT W, UINT H);
	void    Apply(BYTE *pDestImage, BYTE *pSrcImage, UINT W, UINT H);   // Apply to 8 bit image yeilding 8 bit result

	void	Invert(BYTE *pDestImage, BYTE *pSrcImage, UINT W, UINT H);   //invert the image 


	double  m_Gradient;
	double  m_Offset;

	WORD    *m_pLUT;                // The LUT
	WORD    m_Intensities;          // The number of intensity values
	WORD    m_BitDepth;             // Bit depth of the input image
	WORD	m_MaxValue;
};
