#if !defined(AFX_LAMP_DIAPHRAGM_H__737B65D9_924F_4D19_A8EE_3A91D54D0824__INCLUDED_)
#define AFX_LAMP_DIAPHRAGM_H__737B65D9_924F_4D19_A8EE_3A91D54D0824__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Lamp_Diaphragm.h : header file
//
#include "MicroscopeDialog.h"


/////////////////////////////////////////////////////////////////////////////
// CLamp_Diaphragm dialog

#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

class MICROSCOPEGUI_API  CLamp_Diaphragm : public MicroscopeDialog
{
// Construction
public:
	CLamp_Diaphragm(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag);

	void DisplaySetting(int Setting);
	LRESULT HandleMessage (WPARAM MsgCode, LPARAM MsgData);


// Dialog Data
	//{{AFX_DATA(CLamp_Diaphragm)
	//enum { IDD = IDD_LAMP_DIAPHRAGM_BASE };
	CStatic	m_DialogIcon;
	CScrollBar	m_Setting;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLamp_Diaphragm)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLamp_Diaphragm)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnDestroy();
	//}}AFX_MSG

	afx_msg BOOL OnToolTipNotify( UINT id, NMHDR * pNMHDR, LRESULT * pResult );
	afx_msg LRESULT OnMicMessage(WPARAM, LPARAM);

	DECLARE_MESSAGE_MAP()

private:
	int m_nSetting;
	int MaxSetting;
	int MinSetting;

	void AdjustSetting(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	void initSetting();
	void SetValue(int pos);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LAMP_DIAPHRAGM_H__737B65D9_924F_4D19_A8EE_3A91D54D0824__INCLUDED_)
