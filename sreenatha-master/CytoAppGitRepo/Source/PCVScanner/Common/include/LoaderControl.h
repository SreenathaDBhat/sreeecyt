#pragma once
//#include "afxcmn.h"
#include "..\src\Loader\resource.h"	//either do this '#include "\AI_CODE_TREE\Common\src\Loader\resource.h"' or the #define below

#include "Slide.h"
#include "MicInterface.h"
#include "afxwin.h"

class CScanDoc;
class CScaledBitmap;
class CLoaderGraphics;
class CSlideList;
class CDBNavigator;
class CAssayList;

#define MAXBAYS	16

typedef void (*SlideSelectedCallBackFunc)(CSlide *pSlide, void *pObj);
typedef void (*ScanCallBackFunc)(CSlide *pSlide, void *pObj);
typedef void (*HouseKeeperCallBackFunc)(void *pObj);

// CLoaderControl dialog

		//either do this or the #include full path above
		//This will screw-up the 'add event handler' wizard.
		//You will have to comment-out this line temporarily while 
		//you add event handlers using the wizard
//#define IDD_LOADER_CONTROL 11000			

#ifdef LOADER_EXPORTS
#define LOADER_API	__declspec (dllexport)
#else
#define LOADER_API	__declspec (dllimport)
#endif

class LOADER_API CLoaderControl : public CDialog
{
	DECLARE_DYNAMIC(CLoaderControl)

public:
	CLoaderControl(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLoaderControl();

	static void m_loader_handler (unsigned int eventID, UINT nFlags, long x, long y, unsigned long data);
	static void m_tray_handler (unsigned int eventID, UINT nFlags, long x, long y, unsigned long data);
	static void m_stage_handler (unsigned int eventID, UINT nFlags, long x, long y, unsigned long data);
	enum ParentApplication {LOADER_SCAN, LOADER_REVIEW};
	void SuspendScanUpdate();

// Dialog Data
	enum { IDD = IDD_LOADER_CONTROL };

	// To make things more readable, equate to the node level in the hierarchy
	enum {Study, Case, Slide, Scan, Cell};
	static void NavCallBack(CItemInfo * item, void * pObj);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL Create(CWnd* pParentWnd = NULL, void *MicSrvr = NULL, void *SlideList = NULL, void *pDefaultAssayList = NULL, int LoaderType = 0, int ParentApp = 0);
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedLoaderLoadSlide();
	afx_msg void OnBnClickedLoaderManualLoadSlide();
	afx_msg void OnBnClickedLoaderReadbarcodes();
	afx_msg void OnBnClickedLoaderRefresh();
	afx_msg void OnBnClickedLoaderScan();
	afx_msg void OnBnClickedLoaderUnloadSlide();
	afx_msg void OnNMClickSlideListCtrl(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedLoaderCheck1();
	afx_msg BOOL OnToolTipNotify( UINT id, NMHDR * pNMHDR, LRESULT * pResult );
	afx_msg LRESULT OnScanMessage(WPARAM w, LPARAM l);
	afx_msg void OnBnClickedLoaderCheck2();
	afx_msg void OnBnClickedLoaderPriority();
	afx_msg void OnBnClickedLoaderRegioncam();
	afx_msg void OnBnClickedScanReport();
	afx_msg LRESULT OnFindMessage(WPARAM w, LPARAM l);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg LRESULT OnUpdateLoader(WPARAM w, LPARAM l);
	afx_msg void OnBnClickedLoaderLampoff();

	void OnRefresh(void);
	void OnClear(void);
	void ResetLoaderTray(int iTrayPosition);
	void UpdateLoader(int iTrayPosition, int iSlidePosition, BOOL bSelect);
	void MoveToBay (int BayNo);
	void StageSlideOffset (CRect *r, int i);
	void TraySlideOffset (CRect *r, int i);
	void TrayOffset (CRect *r, int i);
	void UpdateList (int Tray);
	void ClearList(int Tray);
	int SlideLoader_Monitor(BOOL On);

	BOOL m_bResize;
	BOOL m_initialized;
	CRect stage_rect;
	CRect slide_rect;
	CRect tray_rect;
	CImageList m_imgList;
	int m_slide;
	int m_active_slide;

	int m_tray;
	int m_scanning;
	int	m_ErrorCode;
	CListCtrl m_SlideList_Ctrl;
	CScaledBitmap *m_selectedSlideBM;
	CScaledBitmap *m_SlideBM[MAXBAYS];
	CScaledBitmap *m_selectedTrayBM;
	CScaledBitmap *m_TrayBM[5];
	CWnd *m_pParentWnd;

	void SetnTrays(int nTrays, int Action);
	int SetTray (int Tray, int Action);
	int CheckSlideLock (CSlide *pSlide, CString LockMsg);
	int CheckAll();
	void CheckInDB(CSlide *pSlide);

private:
	CMicInterface m_microscope;
	int m_nSlidePositions;
	int m_nTrays;
	BOOL BarcodeOK (CString Barcode);
	void initializeSlide (CSlide *SlidePtr);
	void AddBitmapToButton (UINT ButtonID, UINT iconID, int Size);
	CLoaderGraphics * m_LoaderGraphic;
	CLoaderGraphics * m_TrayGraphic;
	CLoaderGraphics	* m_StageGraphic;
	void UpdateStage();
	void UpdateTray(int sel_slide, BOOL update_list);
	void SetSingleBarcode (CSlide *pSlide, int iSlide, int iTray, CString barcode);
	void ResetPriority();
	int GetNextPriority();
	void SetPriorityBtns (CSlide *pSlide);
	void SetPriorityText (CSlide *pSlide);
	void ReadBarcodesManually(int nSlidePositions);
	void ListInformation (CSlide *pSlide, POSITION pos,int Tray);
	void ClearListInformation (CSlide *slidePtr, POSITION pos, int Tray);
	void SetRegionCAM (CSlide *pSlide);

	int m_nLoaderType;
	IMicSrvr*	m_pIMicSrvr;
	CSlideList * m_slideList;
	CDBNavigator * m_nav;
	CAssayList *m_defaultAssayList;
	int m_ParentApp;

				//callback functions
	SlideSelectedCallBackFunc m_slide_selected_callback;
	ScanCallBackFunc m_scan_callback;
	HouseKeeperCallBackFunc m_housekeeper_callback;
	void *m_pHouseKeeperObj;
	void *m_pScanObj;
	void *m_pSlideSelectedObj;

	void SlideMessages();
	CString m_LockMsg;
	CString CompMsg;
	CString BigMsg;
	int m_nBig;
	int m_nLocked;
	int m_nCompleted;
	int m_bManualLoad;
	HANDLE hTamperEvent;
	HANDLE hFixedEvent;
	BOOL m_bScanUpdate;

	void SlideListCtrlSelection (); 

public:

	afx_msg void OnNMRclickSlidelistCtrl(NMHDR *pNMHDR, LRESULT *pResult);

	CButton m_barcodes_only;
	CButton m_objective;
	CButton m_regioncam;
	CButton m_lampoff;

	void AttachSlideSelectedCallbackFunc(ScanCallBackFunc func, void *pObj);
	void DetachSlideSelectedCallbackFunc();
	void AttachScanCallbackFunc(ScanCallBackFunc func, void *pObj);
	void DetachScanCallbackFunc();
	void AttachHouseKeeperCallbackFunc(HouseKeeperCallBackFunc func, void *pObj);
	void DetachHouseKeeperCallbackFunc();
};
