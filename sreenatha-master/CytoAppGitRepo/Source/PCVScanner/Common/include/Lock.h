#if !defined(AFX_LOCK_H__B7FB9B17_0CDD_4DD0_B841_64245D3A01C0__INCLUDED_)
#define AFX_LOCK_H__B7FB9B17_0CDD_4DD0_B841_64245D3A01C0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Lock.h : header file
//
#include "MicroscopeDialog.h"

/////////////////////////////////////////////////////////////////////////////
// CLock dialog

#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

class MICROSCOPEGUI_API	 CLock : public MicroscopeDialog
{
// Construction
public:
	CLock(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag);
	void DisplayLockState();
	void MicLock (BOOL LockOn);
	
// Dialog Data
	//{{AFX_DATA(CLock)
	//enum { IDD = IDD_LOCKBASE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLock)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLock)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnLock();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	BOOL m_bLockState;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOCK_H__B7FB9B17_0CDD_4DD0_B841_64245D3A01C0__INCLUDED_)
