#pragma once

#include "MicroscopeDialog.h"


/////////////////////////////////////////////////////////////////////////////
// CManualLoad dialog

#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

class MICROSCOPEGUI_API CManualLoad : public MicroscopeDialog
{

public:
	CManualLoad(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParent, CWnd *pMessageWnd, void *pIMicSrvr, int AxisID, BOOL LockFlag);

// Dialog Data
	//enum { IDD = IDD_MANUALLOAD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL Create(CWnd* pParentWnd);
	afx_msg void OnDestroy();
	afx_msg void OnManualLoad();
	afx_msg void OnManualUnLoad();
};
