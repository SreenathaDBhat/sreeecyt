//=================================================================================
// Measure.h
// Base class Measurements
//
// History:
// 08Apr03	AR: Added support for RGB colour cameras
// 23Feb00	WH:	original
//=================================================================================
#ifndef __MEASURE_H_
#define __MEASURE_H_

#include "GrabChannels.h"

class CMeasurements
{
public:
	CMeasurements(UINT depth = 8);
	~CMeasurements();

	void ROI (UINT x, UINT y, UINT w, UINT h) {m_roi.left=x; m_roi.top=y; m_roi.right=x+w; m_roi.bottom=y+h;};
	void SampleFrequency (UINT x, UINT y) {m_xsample = x; m_ysample = y;};
	double focus_measure (BYTE *ptr, double lfocus, int Spacing, int BytesPerPixel);
	RECT &ROI ()	{return m_roi;};

	// returns
	UINT MinClip()			{return m_minclip;};
	UINT MaxClip()			{return m_maxclip;};
	UINT MinContrast()		{return m_min[1];};
	UINT MaxContrast()		{return m_max[1];}; 
	UINT MinContrast(int Channel)		{return m_min[Channel];};
	UINT MaxContrast(int Channel)		{return m_max[Channel];}; 
	double Focus()			{return m_focus;};
	BYTE BitDepth()			{return m_depth;};
	UINT *Histogram()		{return m_histo;};
	UINT Bins()				{return m_bins;};
	
	//Sets
	BOOL BitDepth(BYTE depth);
	void XSample(UINT xsample)	{ m_xsample = xsample;};
	void YSample(UINT ysample)	{ m_ysample = ysample;};
	void Format (UINT Format);

	// Virtuals
	virtual BOOL DoMeasurements(BYTE *image, UINT w, UINT h);
	void TestNDSlideHistogram (int Channel, UINT *upper, UINT *lower, BOOL Limits);
	void TestNDSlideHistogram (UINT *upper, UINT *lower, BOOL Limits);

private:
	RECT m_roi;
	UINT m_min[4], m_max[4];		// contrast to be set
	DWORD  m_focus;			// focus to be set
	UINT m_minclip, m_maxclip;
	BYTE m_depth;			// bit depth

	UINT *m_histo;			// Histogram array
	UINT m_bins;			// size of array
	UINT m_xsample;			// spacing between horizontal sample lines
	UINT m_ysample;			// spacing between vertical sample lines

	int m_Format;			// is the image monochrome or colour ?
};
#endif