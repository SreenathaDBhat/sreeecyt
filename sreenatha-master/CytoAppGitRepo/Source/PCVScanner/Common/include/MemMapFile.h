//=================================================================================
// MemMapFile.cpp
// Small class to deal with the creation and control of a memory mapped file
//
// History:
//
// 07Mar00	WH:	original
//=================================================================================
#ifndef __MEMMAPFILE_H_
#define __MEMMAPFILE_H_

class CMemMapFile
{
public:
	CMemMapFile();
	~CMemMapFile();
	BYTE *CreatePublicSegment(DWORD key, UINT size);
	BYTE *MapPublicSegment(DWORD key);
	BOOL DestroyPublicSegment(void);

private:
	HANDLE m_hMemPublic;
	BYTE * m_hMem;
	DWORD m_key;
};

#endif