/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Helper class for mic server functionality
// K Ratcliff 24042004
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

struct IMicSrvr;

#ifndef _UNICODE
class CSlide;
#endif

class MicLock;

#ifdef MICHELPER_EXPORTS
#define MICHELPERDLL_API __declspec(dllexport)
#else
#define MICHELPERDLL_API __declspec(dllimport)
#endif

////////////////////////////////////////////////////////////////////////////////////////
// Focus threshold
////////////////////////////////////////////////////////////////////////////////////////
#define FOCUS_THRESHOLD             105.

class MICHELPERDLL_API CMicHelper
{
public:
    CMicHelper(IMicSrvr *pIMicSrvr, BOOL GotLoader = FALSE);
    ~CMicHelper(void);

    typedef enum { CoarseFocus = 1, FineFocus = 2, UltraFineFocus = 4, SuperUltraFineFocus = 8 } FocusOpts;

    typedef enum {LOAD, UNLOAD} LoaderAction ;
    typedef enum {NoError, LoadFailed, UnLoadFailed, SlideOnHead, TraySelectFailed, MoveToLoadPosFailed, MoveToUnLoadPosFailed} LoaderStatus ;




	int			GetZControllerType(void);						// Get the controller type for the microscope - checks the Z
    BOOL        WaitXYZ(void);                                  // Wait for XYZ
    BOOL        WaitAllAxis(void);                              // Wait for ALL axis to finish moves.
    BOOL        MoveIdealPos(float X, float Y, float Z);        // Move to an ideal position
    BOOL        MoveStagePos(float X, float Y, float Z, BOOL Wait = TRUE);        // Move to an stage position
    BOOL        MoveStagePos(int X, int Y, int Z, BOOL Wait = TRUE);              // Move to an stage position
    BOOL        MoveStagePos(double X, double Y, double Z, BOOL Wait = TRUE);     // Move to an stage position
    BOOL        MoveToIdealZ(float Z, BOOL Wait = TRUE);                          // Just move Z
    BOOL        GetIdealPos(float *X, float *Y, float *Z);      // Get an ideal position
    BOOL        GetStagePos(float *X, float *Y, float *Z);      // Get the stage position
    BOOL        GetStagePos(int *X, int *Y, int *Z);            // Get the stage position
	BOOL		GetZ(float *Z);									// Get the z position
    BOOL        MoveToSlide(int SlideNo);                       // Move to slide position where SlideNo >=0 && <= N
    BOOL        MoveBayDatum(int BayNo);                        // Move to a bay datum position
    BOOL        StageUpdatePos(void);                           // Position update
    void        CoverslipDetection(BOOL On);                    // Turn on/off coverslip detection
    BOOL        Focus(FocusOpts FO, double FocusThresh);	// Focus from this point
	BOOL		Focus(int focusType, int focusMethod, int maxIterations, double focusThresh /*= FOCUS_THRESHOLD*/); 	// Focus from this point
    BOOL        FineEnvelopeFocus(double StartFrom, double Distance, double Threshold, BOOL MoveToFocus);
    BOOL        FineEnvelopeFocus(double Distance, double Threshold, BOOL MoveToFocus);      // Fine envelope focusing
    BOOL        FastFineEnvelopeFocus(double Distance, double Threshold, BOOL MoveToFocus);      // Fine envelope focusing
    BOOL        SuperFineEnvelopeFocus(int MaxIterations, BOOL SuperUltra, BOOL InterphaseMode, BOOL MoveToFocus);
	BOOL		Ideal2EF(float x, float y, CString &EF);
	void		FluorFocusMode(BOOL On);						// Fluorescent focusing mode
    BOOL        TrendFocus(double Distance, double FocusThresh, double Divisor);// Trend focusing
    BOOL        FastFocus(double Distance, double FocusThresh);  // Fast focusing
    double      MaxFocusMeas(void);                             // Return the max focus measurement after a focus measurement
    int         GetBayDatumZ(void);                             // Return the bay datum z position
    int         GetBayDatumZ(int Bay);                          // Return the bay datum z position for bay Bay
    int         MaxObjectives(void);                            // Return the max number of objectives
    BOOL        GetObjectiveName(int Pos, CString &Name);       // Get the name associated with an objective
    int         GetObjectivePos(double ObjMag);                 // Return the objective turret position for a given magnification
    BOOL        GetObjectiveOffsets(int ObjPos, double *XOfs, double *YOfs, double *ZOfs); // Get the calibrated objective offsets
    BOOL        SetObjective(int Position, BOOL MoveZ);         // Set the objective
    BOOL        SetObjective(double ObjMag, BOOL MoveZ);        // Set the objective based on its magnification
    BOOL        GetLensPower(int Pos, double *Power);           // Get the lens power
    BOOL        SetLamp(int Val);                               // Set the lamp
    BOOL        BrightField(void);                              // Mic operating in brightfield
    BOOL        Fluorescence(void);                             // Mic operating in fluorescence
    BOOL        GetObjective(int *Position);                    // Get the current nosepeice
    BOOL        GetMagnification(double *Power);                // Get current objective magnification
    BOOL        MoveFilter(int Wheel, int FilterNo, BOOL Wait = TRUE);  // Move a filter
    BOOL        ApplyOffsets(void);                             // Apply offsets
    BOOL        StopApplyingOffsets(void);                      // Don't apply offsets
    BOOL        GetObjectiveZOffset(int Position, double &Z);   // Get the calibrated Z offset for the objective
    BOOL        ApplyLampOffsets(BOOL Apply);                   // Apply lamp offsets
    BOOL        GetImageScales(double *Sx, double *Sy);         // Image scales
	BOOL        GetImageScales(double ObjectiveMag, double *Sx, double *Sy);         // Image scales
	BOOL		IsThisAxisMotorised(int axisind, int *Motorised); //Is axis motorised
    BOOL        IsAxisPresent(int axisind);                     // Is axis configured
    BOOL        GetXYScales(double *Sx, double *Sy);            // Pixels per stage step
	BOOL		GetXYLimits(float *Xmin, float *Ymin, float *Xmax, float *Ymax);	// Limits of stage movement
    BOOL        OpenShutter(void);                              // Open the shutter
    BOOL        CloseShutter(void);                             // Close the shutter
    BOOL        OpenBFShutter(void);                              // Open the Brightfield shutter
    BOOL        CloseBFShutter(void);                             // Close the Brightfield shutter
	BOOL        IsShutterOpen(void);                            // Is the shutter open or closed
	BOOL		Busy(void);										// Is busy	
	BOOL		ZBusy(void);									// Z axis is busy
	BOOL        MoveFilterToName(int Wheel, BSTR FilterName, BOOL Wait /* = TRUE */);
    void        PositionNearestFilter(LPCTSTR Name);        
	BOOL        GetCurrentElementPosition(int axisid, int *curpos);
	double		FocusDelta(double Mag);							// Get the focus delta for the current objective
    BOOL        CondenserOut(void);                             // Flip the condenser out
    BOOL        CondenserIn(void);
	BOOL		SetZSpeed(double StartSpeed, double Speed);		// Set the speed in microns/sec

    BOOL        LockMic(void);              // Lock the sewrver
    void        UnlockMic(void);            // Unlock the sewrver
	BOOL		IsLocked(void);				// Are we locked
	void		ForceUnlock(void);			// Force ANY locks to be released
	void		LogZMoves(BOOL LogIt);		// Log the Z moves

    IMicSrvr    *m_pIMicSrvr;				// Pointer to the grabserver

	void		ShutDown();

	void		EnableZUpperLimit();
	void		DisableZUpperLimit();
	BOOL		OilSlide(double X, double Y, double Z);
	BOOL		IsTrayOnStage(BOOL &OnStage);
	BOOL		HideOilMessages();
	BOOL		ShowOilMessages();
	void		ExplainTheError();


private:
	void		OutputZValue(float Z);

    BSTR        m_XYZAxes;
    BSTR        m_SAxes;

    BOOL        m_bGotSlideLoader;          // Set TRUE if we have a slide loader
    BOOL        m_bCreatedServer;           // This class created an instance to the micserver
    MicLock     *m_pLock;
	LONG		m_LastFocusError;
	BOOL		m_bLogZMoves;
public:
    // Return the last good focus position
    double		LastFocusZ(void);
	// Return the last focus error code
	LONG		LastFocusError(void) { return m_LastFocusError; }

	void SetFluorescenceMode (BOOL bOn);
};
