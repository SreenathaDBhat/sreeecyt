#ifndef _MICINTERFACE_H_
#define _MICINTERFACE_H_

#include <Axes.h>
#include "AutoScope.h"
#include "AutoFocusCommon.h"
#include "..\src\micserver\micserver.h"


typedef int (*pfi)();

struct IMicSrvr;

#define MICMESSAGE	_T("Microscope Message")
#define CALIBMESSAGE	_T("Microscope Calibration Message")
#define CONFIGMESSAGE	_T("Microscope Configuration Message")

class CMicInterface {

public :
	CMicInterface();
    CMicInterface(void *pISrvr);
    CMicInterface(long hWnd,void *pISrvr);

    enum { LOAD = FALSE, UNLOAD = TRUE };

	virtual ~CMicInterface();

		void mic_control_interface(void * pISrvr);
		void InitCOMServer (long hWnd,void *pISrvr);

		int mic_initialize(long hWnd = NULL, int SendErrors = 0, short *pFailedAxis = NULL,
		                   BOOL connect = FALSE, BOOL connectAll = FALSE, char *AppId = NULL);
		int mic_open(short *FailedAxis = NULL, BOOL connectAll = FALSE);
		int mic_start_jog_controller ();
		int mic_update();
		int mic_getconfiguration (MicConfig *Configuration, int nCaps);
		int mic_setconfiguration(MicConfig *Configuration, int nCaps, BOOL connect = FALSE);
		int mic_getcontroller_type (int axisid, int *ctrl_type);
		int mic_emergencystop ();
		int set_mic_lock (BOOL Lock);
		int get_mic_lock(BOOL *Lock);
        BOOL LockMic(void);
        BOOL UnlockMic(void);
        BOOL IsLocked(void);

		int mic_shut_down ();
		int mic_wait(char *Axes);
		int mic_loadslide (char *Axes, int iTrayPosition, int iSlidePosition, int HomeStage, BOOL ReadBarcodes, BOOL ReturnZ);
		int mic_loadslide_n (int Slide_N, int HomeStage, BOOL ReadBarcodes, BOOL ReturnZ, BOOL Blocking);
		int mic_loadslide_next (int Slide_N, int HomeStage, BOOL ReadBarcodes, BOOL ReturnZ, BOOL Blocking);
		int mic_saveconfiguration();
		int mic_savejoysticksettings();
		int mic_setcontrolstate(int state);
		int mic_getcontrolstate(int *state);
		int mic_isaxispresent (int axis);
		int mic_isaxismotorised (int axis);
		int mic_trayonstage(int *OnStage);

		int mic_getlasterror (HWND hWnd);
		void ExplainTheError(void);
		void mic_explainerror(HWND hWnd, int ErrorCode, int AxisID);
		void mic_clearlasterror ();
		int mic_getlasterrorcode();
		int mic_capabilities ();

		int mic_SetLocalRemote(int state);

		void GetAxisLabel (int AxisID, LPTSTR label);
		void GetControllerLabel (int AxisID, LPTSTR label);
		int Identify (int AxisID, LPTSTR Name);
		BOOL DetectControllerAxis(MicConfig *pConfig);

        int UnloadSlide(int TrayNum, int PosOnTray);
        int LoadSlide(int TrayNum, int PosOnTray, BOOL ReturnZ);
        int Loader(BOOL action, int tray, int pos_on_tray, BSTR Saxes);

		int mic_record_loadposition();
		int mic_record_unloadposition();
		int mic_record_checkposition();
		int mic_record_zupperlimit();
		int mic_moveto_loadposition();
		int mic_moveto_unloadposition();
		int mic_moveto_checkposition();
		int mic_moveto_zupperlimit();


		int stage_getfd(HANDLE *MicHandle, HANDLE *StageHandle) ;
		int stage_reopen(HANDLE fd);
//		int stage_select(StageType stage, int axes);
		int stage_close();
		int stage_waitmode(int mode);
		int stage_moveto(int xpos, int ypos, int zpos);
		int stage_moveby(int xdist, int ydist, int zdist);
		int stage_movebyfov (int axis, int direction, int overlap);
		int stage_moveaxisby(int axis, int dist);
		int stage_moveaxisto(int axis, int pos);
		int stage_floatmoveaxisto(int axis, float pos);
		int stage_floatmoveaxisby (int axis, float dist);
		int stage_setposition(int nx, int ny, int nz);
		int stage_getcurrentbay(int *CurrentBay);
		int stage_getposition(int *cx, int *cy, int *cz);
		int stage_getabsposition(int *cx, int *cy, int *cz);
		int stage_floatgetabsposition(float *cx, float *cy, float *cz);
		int stage_floatgetposition(float *cx, float *cy, float *cz);
		int stage_setspeed(int axis1, int axis2, int speed1, int speed2);
		int stage_getdatum(char *Axes);
		int stage_updateposition();
		int stage_busy();
		int stage_stop();
		int stage_open(LPTSTR com);
		pfi stage_setupdatecallback(void (*f)(int, int, int));
		int stage_setZstepsize (float stepsize);
		int stage_getZstepsize (float *stepsize);
		int stage_initialize();
		int stage_park();
        void StageWait(void);

		int SetMaxPosition(int axis, double pos);
		int GetMaxPosition(int axis, double *pPos);

		int stage_setbayorigin(int BayNumber, LPTSTR BayOrigin, int SetDefaultIdeal);
		int stage_getbayorigin(int *BayNumber, LPTSTR BayOrigin);
		int stage_setbayoilorigin();
		int stage_movetofocalpoint();
		int stage_getnumberofbays (int *NBays);
		int stage_movetobaydatum (int bayno, BOOL ReturnZ);
		int stage_gotobayposition (int bayno);
		int stage_movetomanualload();
		int stage_movetomanualunload();
		int stage_setbay_zdatum (int BayNumber);

		int stage_getidealposition (float *x, float *y, float *z);
		int stage_getidealposition (int *x, int *y, int *z);
		int stage_movetoidealposition (float x, float y, float z);
		int stage_movetoidealposition (int x, int y, int z);
		int stage_movetoidealZ(float z);
		int stage_getfocalpoint(float* z);
		int stage_moveaxistoidealposition (int axis, float pos);
		int stage_moveaxistoidealposition (int axis, int pos);
		int stage_moveaxisbyideal (int axis, float dist);
        int MoveStageToIdealXY(float posx, float posy);
        int MoveToIdealZ(float Z);                      
        BOOL GetIdealPos(float *X, float *Y, float *Z);
        BOOL MoveIdealPos(float X, float Y, float Z);               // Move to an ideal position
        BOOL WaitXYZ(void);                                         // Wait for completion of a move in X, Y or Z
		int SetZSpeed(double SpeedMultiplier);

		int stage_relocatetoidealposition (float x, float y, float z);
		int stage_relocatetostageposition (float x, float y, float z);
		int stage_getidealz (float *z);

		int getIdealEnglandFinderPosition (int point, LPTSTR efcoord);
		int getEFOffset (int point, double *x, double *y);
		int setEFOffset (int point);

		int convert_Ideal_to_EF (float x, float y, LPTSTR EFCoords);
		int convert_Stage_to_EF (int x, int y, LPTSTR EFCoords);
		int stage_move_to_EF (LPTSTR EFCoords);

		int getelementname (int axisd, int pos, LPTSTR *name);
		int setelementname (int axisd, int pos, LPTSTR name);
		int getelementposition(int axisid, LPTSTR name, int *pos);
		int getelementposition(int axisid, LPTSTR name, int type,int *pos);

		int set_fovoverlap(int overlap);
		int get_fovoverlap(int *overlap);


		//filter wheel control functions
		int filter_initialize( int wheel);
		int filter_reinitialize(int wheel);
		int filter_home(int wheel);
		int filter_maxfilters (int wheel, int *MaxFilters);
		int filter_movetopos(int wheel, int filter_no, int waitmove);
		int filter_movetoname(int wheel, LPCTSTR filter_name, int waitmove);
		int filter_getfilterinfo(int wheel, RotorInfo *fip);
		int filter_getcurrentfilter (int wheel, int *position);
        void PositionNearestFilter(LPCTSTR FilterName);
        void MoveAllFilters(int Excitation, int Dichroic, int Transmission);

		//objective lens control functions
		int obj_initialize();
		int obj_reinitialize();
		int obj_home();
		int obj_maxobjectives (int *MaxObjectives);
		int obj_movetopos (int objective_no, int waitmove, BOOL MoveZ);
		int obj_movetoname (LPTSTR objective_name, int waitmove);
		int obj_movetonameandtype (LPTSTR objective_name, int objective_type, int waitmove);
		int obj_getobjectiveinfo(RotorInfo *fip);
		int obj_getcurrentobjective(int *position);
		int obj_setlenspower (int objective, double power);
		int obj_getlenspower (int objective, double *power);
		int obj_positionstage(int waitmove);
		int obj_setlenstype(int objective, int type);
		int obj_getlenstype(int objective, int *type);
        BOOL SetObjective(int Position, BOOL MoveZ);          // Set the objective
		BOOL GetObjective(int *Position);                    // Get the current nosepeice
		int HideOilObjectiveMessages();
		int ShowOilObjectiveMessages();

		//Common functions for filterwheels and objective turret
		int initialize (int axisid);
		int getmaxelements (int axisid, int *MaxElements);
		int getcurrentelement (int axisid, int *position);
		int setstageoffset(int axisid, int element_no);
		int setstagezoffset(int axisid, int position);
		int getfocusoffset (int axisid, int element_no, double *offset);
		int getcurrentobjectivefocusoffset(double *offset);
		int getstageoffset (int axisid, int element_no, double *xoffset, double *yoffset, double *zoffset);
		int getidealstageoffset (int axisid, int element_no, double *xoffset, double *yoffset, double *zoffset);
		int setlampoffset (int axisid, int element_no);
		int getlampoffset (int axisid, int element_no, int *offset);
		int setapplyoffset(int axisid, int applyoffset);
		int getapplyoffset (int axisid, int *applyoffset);
		int setbase (int axisid, int partNo);
		int getbase (int axisid, int *partNo);
		int home(int wheel);
		int resetoffsets (int wheel, int type);
		int resetzoffsets (int axisid, int type);
		int getaxisid (LPTSTR axisname, int *axisid);
		int getaxisname (int axisid, LPTSTR axisname);

		int getelementinfo (int axisid, RotorInfo *rip);
		int movetopos (int axisid, int position, int waitmove, BOOL MoveZ);
		int movetoname (int axisid, LPCTSTR name, int waitmove);
		int movetonameandtype (int axisid, LPCTSTR name, int type, int waitmove);


		//Shutter
		int getshutter (int *state);
		int setshutter (int state, int waitmove);
        BOOL OpenShutter(void);
        BOOL CloseShutter(void);

		//Shutter
		int getBFshutter (int *state);
		int setBFshutter (int state, int waitmove);

		//Lamp
		int setlamp (int value, int waitmove);
		int getlamp (int *value);

		//Condenser
		int getcondenser (int *state);
		int setcondenser (int state, int waitmove);
		int getcondensersetting(int AxisID, int position, int *setting);
		int setcondensersetting(int AxisID, int position, int  setting);

		//Aperture
		int setaperture (int value, int waitmove);
		int getaperture (int *value);
		int getaperturesetting(int AxisID, int position, int *setting, BOOL fluorescentcomponent);
		int setaperturesetting(int AxisID, int position, int  setting, BOOL fluorescentcomponent);

		//Field diaphragm (Brightfield)
		int setfield (int value, int waitmove);
		int getfield (int *value);
		int getfieldsetting(int AxisID, int position, int *setting);
		int setfieldsetting(int AxisID, int position, int  setting);

		//Field diaphragm (Fluorescent)
		int setfield_fl (int value, int waitmove);
		int getfield_fl (int *value);

		int slideloader_startup(int tray);
		int slideloader_beginsilent();
		int slideloader_endsilent();
		int slideloader_checkforslideonstage(BOOL *bSlidePresent);
		int slideloader_clearcurrentfault();
		int slideloader_getlastfault();
		int slideloader_getoccupiedslidepositions();
		int slideloader_getoccupiedtraypositions();
		int slideloader_getslidebarcode(int iSlidePosition, LPTSTR strBarcode);
		int slideloader_getslidebarcode_from_index(int iSlideIndex, LPTSTR strBarcode);
		int slideloader_getslidebarcodesontray ();
		int slideloader_gettraybarcode (LPTSTR strBarcode);
		int slideloader_loadslide (int iSlidePosition);
		int slideloader_removeslide();
		int slideloader_scanslidepositions();
		int slideloader_scantraypositions();
		int slideloader_selecttray(int iTrayNumber);

		int slideloader_occupiedslideposition (int SlidePosition, int *SlidePresent);
		int slideloader_occupiedtrayposition (int TrayPosition, int *TrayPresent);
		int slideloader_slidebarcodeontray (int iSlidePosition, LPTSTR strBarcode);
		int slideloader_initializetrays (int ReadTrayBarcode, BOOL CheckSlidePresence);
		int slideloader_gettrayinfo(int iTrayPosition, LPTSTR strTrayBarcode, int *TrayPresent, int *NSlidePositions);
		int slideloader_getslideinfo (int iTrayPosition, int iSlidePosition, LPTSTR strSlideBarcode, int *SlidePresent, int *SlideChanged);

		int slideloader_moverobotheadabovestage();
		int slideloader_finishcalibration();
		int slideloader_startcalibration(char *Axes);
		int slideloader_recordstartposition();
		int slideloader_recordloadposition();
		int slideloader_recordcheckposition();
		int slideloader_recordunloadposition();
		int slideloader_getstartposition (int *x, int *y, int *z);
		int slideloader_getloadposition (int *x, int *y, int *z);
		int slideloader_getunloadposition (int *x, int *y, int *z);
		int slideloader_getcheckposition (int *x, int *y, int *z);
		int slideloader_getzupperlimit (int *z);
		int slideloader_remove(BOOL remove);

		int slideloader_gettraytampered();

		int slideloader_checkslidepresence(BOOL On);
		int slideloader_scanwholetray();
		int slideloader_mute (BOOL On);
		int slideloader_monitor (BOOL On);
		int	slideloader_park();
		int slideloader_cassettejog(double Value);
		int slideloader_isdooropen (BOOL *Open);

		int GetTraysAndSlides (int *nTrays, int *nSlidesPerTray);
		int GetLoadedTray (int *LoadedTray);
		int PutLastTrayInLoop (int TrayNum);
		int GetLastTrayInLoop (int *TrayNum);


		void mic_free_configuration (MicConfig *Configuration, int nCaps);

		int movetocalibrationfeature (int FeatureID);
		int recordcalibrationfeature (int FeatureID);

				//joystick control
		int Jog (int AxisID, int Direction, int Speed);
		int SetJogSpeed (int AxisID, int Speed, int jogSpeed);
		int GetJogSpeed (int AxisID, int Speed, int *jogSpeed);

		int FastFocus (float Z_start, float Z_End, float speed_modifier, int Wait);
		int focus(double calibrated_z, FocusOpts type, FocusStartPoint StartFrom, double Z, int maxiterations);
		int focus(double calibrated_z, FocusOpts type, FocusStartPoint StartFrom, double Z, int maxiterations, double Thresh);
		int TrendFocus(double calibrated_z, FocusOpts Method, FocusStartPoint StartFrom, double Z,int maxiterations, double Thresh);

        void GetFocusMeas(double *F);

		int FindXYLimits ();
		int GetXYLimits (float *x0, float *y0,float *x1, float *y1);
		int FindCross (float *x, float *y, float *z);
		int AlignCross (int Start);
		int SetDefaultIdealCoordinates();

				//flourescent lamp
		int fl_lamp_getstatus (int *status);
		int fl_lamp_switchlamp (BOOL Setting, int waitmove);
		int fl_lamp_setlamp (int Setting, int waitmove);
		int fl_lamp_getlamp (int *Seting);
		int fl_lamp_setshutter (BOOL Setting, int waitmove);
		int fl_lamp_setexposure (double Exposure);
		int fl_lamp_getexposure (double *Exposure);
		int fl_lamp_runtimed(int waitmove);
		int fl_lamp_clearalarm ();
		int fl_lamp_getlampage (int *Hours);
		int fl_lamp_ready (int *ready);
		int fl_lamp_lampon (int *on);
	

		// genetix oiler
		int oiler_begin_oiling(BOOL Zero_Z);
		int oiler_dispense_oil();
		int oiler_end_oiling();
		int oiler_oilslide(float x, float y);

			//Calibration data
		int GetXYStepSize (double *xstepsize, double *ystepsize);
		int GetXYScale(double *xscale, double *yscale);
		int GetImageScale (double *xscale,double *yscale); 
		int GetImagePixels (int *x, int *y); 
		int GetImageScaleFromObjective (int obj, double *xscale, double *yscale); 
		int GetXYBacklash (double *xbacklash,double *ybacklash);
		int GetIdealCalibrationPoint (double *x,double *y, int point); 
		int GetIdealPoint (double *x,double *y, int point);

		int floatconvert_to_ideal(float *x, float *y, float *z);
		int floatconvert_to_stage(float *x, float *y, float *z);


		// Utility functions defined in MicroscopeData.cpp
		static void MicConfigCopy(MicConfig *pDestConfig, MicConfig *pSrcConfig);
		static void MicConfigFree(MicConfig *pConfig);
		static LPCTSTR GetControllerName(ControllerType type);
		static LPCTSTR GetAxisName(MicroscopeComponents axis);

		int ResetAllWarningFlags();

		int SetUpdatePositions(int Enable);
		int SetPriority(int Priority);
		int GetMicroscopeName (LPTSTR name, unsigned int nlength);

        IMicSrvr *GetServer(void) { return m_pIMicSrvr; };
		IMicSrvr *m_pIMicSrvr;	

		int GetSystemSlideCapacity();

		// Coordinate Conversion Access
		int GetCoordConversionBCPoints(float& bX, float& bY, float& cX, float& cY);
		
        StageType GetStageType(void);
		BOOL GetSystemHomed();

		BOOL OptionalMagChangerAt1x();
		int EnableZUpperLimit();
		int DisableZUpperLimit();
		int KillMicserverClient(char *appString);
		int DisableMicserverClient(char *appString);
		int EnableMicserverClient(char *appString);
		int DumpMicProcessInfo();

		int SetFluorescenceMode (BOOL bOn);

		int BackupObjectiveOffsets();
		int RestoreObjectiveOffsets();

	private:
		void OutputError(LPCTSTR Func);
		bool DoWeHaveASlideLoader();
		void AllocSysAxes(void);            // Allocate sys strings for frequently used axes
        void FreeSysAxes(void);             // Free em up again


		int ErrorCode;						//error code associated with operation of microscope
		int Response;						//response from COM server
        BOOL m_bInternalServer;
        BSTR        m_XYZAxes;
        BSTR        m_SAxes;
};

#endif
