
#ifndef TRACK_LOCK_J
#define TRACK_LOCK_J

#include "..\..\Common\Src\NLog\src\NLogC\NLogC.h"
#include "..\..\Common\Src\NLog\src\NLogC\NLogger.h"

#define TRACK_MICLOCKING
#ifdef TRACK_MICLOCKING
#define TRACK_LOCK(f, l)				NLog_Trace(_T("LOCK"), _T("------SET LOCK %s %d"), f, l);
#define TRACK_UNLOCK(f, l)				NLog_Trace(_T("LOCK"), _T("----CLEAR LOCK %s %d"), f, l);
#define TRACK_LOCKALRDY(d, f, l)		NLog_Trace(_T("LOCK"), _T("-ALRDY LOCK %d %s %d"), d, f, l);

#define TRACK_LOCKERR(e)				NLog_Error(_T("LOCK"), _T("-Lock ERROR %d"), e);
#else
#define TRACK_LOCK(f, l)				
#define TRACK_UNLOCK(f, l)				
#define TRACK_LOCKALRDY(d, f, l)		
				
#define TRACK_LOCKERR(e)				
#endif

#endif