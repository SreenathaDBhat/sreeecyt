
#define MICMESSAGE	"Microscope Message"
#define CALIBMESSAGE	"Microscope Calibration Message"
#define CONFIGMESSAGE	"Microscope Configuration Message"

int MicMessage (void * m_pISrvr, HWND hWnd, int ErrorCode, char Axis);
void  GetAxisLabel (int AxisID, char *label);
void  GetControllerLabel (int CtrlType, char *label);