//
// FileName : microerrors.h
// Author :  A Rourke
//	Date	: September 2000
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Header File Containing Error Definitions For The Microscope Library //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef microerrors_h
#define microerrors_h


#define GET_LAST_RECORDED_ERROR -1000
#define SHIFT_PLACES 16

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Error Codes Returned By Microscope and Motor Controller Software //  
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define erNO_ERRORS                             0       // No Errors Have Occurred 
#define erCOMMS_FAILURE                         -1      // Serial Comms Problem
#define erDEVICE_BUSY                           -2      // Still Executing Previous Command
#define erINVALID_PARAMETER                     -3      // Associated Parameter Is Out Of Range
#define erINVALID_COMMAND                       -4      // Command Sent Not Understood By Microscope
#define erEXEC_ERROR                            -5      // Command Couldn't Execute e.g. Motor on Limit
#define erINTERFACE_ERROR                       -6      // Communications Interface Problem
#define erMETHOD_NOT_IMPLEMENTED                -7      // Used To Denote Un-implemented Methods
#define erUNIT_UNCONNECTED                      -8      // Non-existent Function On Microscope
#define erINCORRECT_VERSION				        -9		// Microscope Firmware Version Incorrect
#define erCONFIG_READ							-10		// Error reading the Microscope config file
#define erCONFIG_WRITE							-11		// Error writing the Microscope config file
#define erCOMPORT_ALLOCATION		            -12		// A Com port has been allocated to two different controllers
#define erMOVEMENT_LIMIT					    -13		// Axis at limit of travel
#define erINITIALIZED_OK						-14		// Initialized microscope OK
#define erALREADY_INITIALIZED			        -15		// Microscope already initialized
#define erNOT_INITIALIZED					    -16		// Microscope not initialized
#define erLOCKED_OUT							-17		// Server is busy with another client
#define erSERVER_FAILED						    -18		// Server communication error
#define erDEVICE_SILENT						    -19		// Axis is in silent mode
#define erDEVICE_FAULT							-20		// Axis is in fault mode
#define erDEVICE_IDLE						    -21		// Axis is IDLE
#define erDEVICE_DISCONNECTED			        -22		// Axis has been disconnected
#define erDEVICE_INITIALIZING				    -23		// Axis in process of initialization
#define erDEVICE_DEBUG							-24		// Axis is in debug mode (i.e. slide loader hardware not present)
#define erMOVE_FAILED						    -25		// Movement failed to reach requested destination
#define erINVALID_STATE							-26		// System is not in correct state to execute the command
#define erSLIDE_ON_STAGE						-27		// The slide loader cannot action the command because there is a slide on the stage
#define erINVALID_HANDLE						-28		//
#define erNO_TRAY								-29		// There is no tray at the specified location
#define erNO_SLIDE								-30		// There is no slide at the specified location
#define erTHREAD_FAILED							-31		// Serial thread was not created
#define erCHECKSUM_FAIL							-32		// Check sum failed for serial message
#define erBUFFER_OVERRUN						-33		// Error occured while trying to write past end of buffer
#define erNO_RESPONSE							-34		// No response from slide loader
#define erLOAD_FAILED							-35	    // Failed to load slide on to stage
#define erINCOMPATIBLE_VERSION				    -36		// Firmware version incompatible with dll version
#define erCONVERSION_FAILED						-37		// String returned from the slideloader could not be converted to a number
#define erDIFFERENT_PORT						-38		// An attempte was made to open a new port while the current port is still open
#define erVACUUM_FAILED							-39		// Error occured while trying to read the vacuum pump values
#define erALLOCATION_FAILED						-40		// Allocation of slideloader internal resources failed 
#define erCALIBRATING_SLIDELOADER		        -41		// Slideloader in calibration state
#define erSLIDE_ON_HEAD							-42		// Slide on robot head
#define erUNDEFINED_SLIDELOADER_ERROR	        -50     // Undefined error from the slideloader
#define erUNDEFINED_POSITION					-51		// Position not defined
#define erTRAY_TAMPERED							-52		// The slide loader tray has been removed since last being scanned
#define erSLIDE_BLOCKED							-53		// The slide loader tray position for the slide on the stage has another slide in it
#define erUNLOAD_FAILED                         -54     
#define erTRAYSELECT_FAILED                     -55
#define erDOOR_OPEN								-56		// The slideloader door is open
#define erDOOR_OPENED							-57		// The slideloader door has been opened
#define erTRAY_ON_STAGE							-58		// Tray loaded onto stage
#define erNO_TRAY_ON_STAGE						-59		// No tray loaded onto stage
#define erNO_TRAY_TO_UNLOAD						-60		// No tray on stage to unload
#define erTIMED_OUT								-61		// Motion timed out
#define erOIL_START_FAILED						-62		// Oil dispenser failed to deploy
#define erOIL_DISPENSE_FAILED					-63		// Oil dispenser failed to dispense
#define erOIL_END_FAILED						-64		// Oil dispenser failed to retract
#define erTRAY_BLOCKED							-65		// The tray position in the cassette has another tray in it
#define erHOME_XYREADER							-66		// The XY reader device needs homing
#define erSWAPTRAY_FAILED						-67		// The swap tray function failed to unload or laod a tray

#define erIMAGE_FAILED							-100	// Image processing failure (for calibration)
#define erCALIBRATION_FAILED					-101	// Failure of calibration procedure
#define erGRABBER_FAILED						-102	// Failed to connect to grabber
#define erAUTODETECT_FAILED						-103	// Failed to auto detect the microscope component
#define erMANUAL_CONTROL						-104	// Axis is controlled manually
#define erFOCUS_FAILED							-105	// Focus function failed to find a focal point

#define erLensChangeDryToOil					-106
#define erLensChangeOilToDry					-107
#define erLensChangeDryToOil_Manual				-108
#define erLensChangeOilToDry_Manual				-109

#define erFOCUS_TOOMUCHSATURATION				-110
#define erFOCUS_UNDEREXPOSED					-112

#define erUNDEFINED_ERROR                       -500     // Unknown Error Code From Microscope
#define erGenetixFileCopy						-111     // Copying GSL10/GSL120 config files
#endif