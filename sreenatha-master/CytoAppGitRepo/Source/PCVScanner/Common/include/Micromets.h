// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the MICROMET_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// MICROMET_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef MICROMETS_EXPORTS
#define MICROMETS_API __declspec(dllexport)
#else
#define MICROMETS_API __declspec(dllimport)
#endif

#include "CParameterIO.h"


#define TARGET_BACKGROUND_LIGHT         254
#define TARGET_BACKGROUND_DARK          0

// Holds measurement data
typedef struct {
    POINT   Centre;             // Centre of object
    long    Area;               // Square area
    long    Circularity;        // As A %
    long    CaliperRatio;
    long    CaliperMin;
    long    CaliperMax;
    long    Width, Height;      // Bounding Box Size
    long    CMP;                // Compactness
    long    RMA;                // Remedial Axis
    long    DDR;                // Density Ratio
    long    Dens1;
    long    Dens2;
} BLOB_MEASUREMENT;

// This class is exported from the MicroMet.dll
class MICROMETS_API CMicroMet {
public:
	CMicroMet(void);
	~CMicroMet();

	void Defaults(void);									// Default Values For Finding Parameters
    BOOL Initialise(LPCTSTR InitFileRoot, LPCTSTR ResultsRoot, float XScale, float YScale);// Initialisation Of Parameters, ETC
    int Find(BYTE *RedImage, BYTE *GreenImage, LPCTSTR RedName, LPCTSTR GreenName, long ImageWidth, long ImageHeight, float XOffset, float YOffset);
    void DebugMode(BOOL On) { m_bDebugMode = On; };

private:
	void SaveParameters(ParameterFileType PT);			    // Load Finding Parameters From A File
    float Convert(long P, float S, float O) { return (float) P * S + O; };
	void LoadParameters(void);								// Load Finding Parameters From A File
    void CutImageFrag(long SrcImage, long Xcf, long Ycf, long DestImage, long *Xl, long *Yt);
    long AdjImgBgr(long hImg, long TargetVal);
    void FindAdaptiveMask(long hImg, long hMsk1, long hMsk2, long Incr, long Xcf, long Ycf, long Xl, long Yt);
    void GetExtremes(long *Histo, long *Min, long *Max, long *IdxMin, long *IdxMax);
	void BgrRemoval(long SrcImage, long DestImage, BOOL UseMode);			// Preprocessing Background Removal
    void CreateIndicator(long RedImage, long GreenImage, BOOL Invert);
    int BlobDetect(long SrcImage, long Mask, int Thresh, BOOL Invert);      // Find Spots In An Image
    int BlobFilter(void);                                   // Filter Out Blobs
    BOOL BlobPresent(POINT *lpPOINTS, long NBlobs, long xmin, long ymin, long xmax, long ymax);
    long Circularity(long Mask, double DistMin, double DistMax);
    void CellShapeMeasure(long Hndl, BLOB_MEASUREMENT *pBlob);
    int MeasureFragment(long Xcf, long Ycf, long SrcHandle1, long SrcHandle2, long Handle1, long Handle2, long *Ocx, long *Ocy, POINT *lpPOINTS, long NBlobs);
    int ExtractFeatures(long Handle1, long Handle2, int NumObjects);
    void SaveBlob(BLOB_MEASUREMENT *pBlob);

	char		ResultsRootPath[MAX_PATH];					// Root Directory Path For Finding Operations
    char        InitRootPath[MAX_PATH];                     // Parameters Path

    float       m_fXScale;                                  // Scaling parameters
    float       m_fYScale;
    float       m_fXOffset;                                 // Offsets
    float       m_fYOffset;

    BOOL        m_bDebugMode;                               // If TRUE will flick up loads of messages and pictures

    // Optional Parameters
	BOOL		m_bBackGroundRemoval;						// = TRUE To Perform Background removal on images
	long		m_nMinArea;									// Smallest Acceptable Micromet Size
	long		m_nMaxArea;									// Maximum  Acceptable Micromet Size
    long        m_nBgrZoom;                                 // Background Removal Parameters
    long        m_nBgrIterations;      
    long        m_nBgrBlockModeShift; 
    long        m_nBgrHistogramSmooth;
    long        m_nBgrTargetModeOffs; 
    long        m_nBgrMaxModeShift;
    long        m_nBGR1Min;                                 // Background Values For Region of Interest
    long        m_nBGR1Max;
    long        m_nBGR1Rec;
    long        m_nBGR2Min;
    long        m_nBGR2Max;
    long        m_nBGR2Rec;
    long        m_nBBMin;                                   // Bounding Box Parameters
    long        m_nBBMax;           
    long        m_nROIxMin;                                 // ROI Parameters
    long        m_nROIyMin;
    long        m_nROIxMax;
    long        m_nROIyMax;
    long        m_nThresh1_1;                               // Thresholds
    long        m_nThresh2_1;
    long        m_nThreshRelative;
    long        m_nThreshDark;
    long        m_nThreshMask;
    long        m_nPicFragSize;                             // Picture Fragment Size
    double      m_dblClrSatX1;                              // For Color Saturation Algorithm
    double      m_dblClrSatY1;
    double      m_dblClrSatX2;        
    double      m_dblClrSatY2;
    long        m_nCaliperRatioMin;                         // Min Max Classifier Values
    long        m_nCaliperRatioMax;
    long        m_nCaliperMin;
    long        m_nCaliperMax;
    long        m_nRMAMin;                                  
    long        m_nRMAMax;
    long        m_nCMPMin;
    long        m_nCMPMax;
    long        m_nDDRMin;                                  // Optical density ratio
    long        m_nDDRMax;
    double      m_dblChordMinToRad;                         // For Calculation Purposes
    double      m_dblChordMaxToMin;
    long        m_nHalfArcMin;
    long        m_nHalfArcMax;
    long        m_nCircCurvThresh;     
    long        m_nCircMin;
    long        m_nCircMax;

    // General find statistics
    long        m_nNumberObjectsFound;

    // Image Size
    long        m_nImageXe;
    long        m_nImageYe;

    char        m_redName[MAX_PATH];                     // Parameters Path
    char        m_greenName[MAX_PATH];                     // Parameters Path


    // UTS Handles For Temporary Images Such As Masks
    long        m_hMaskImage;
    long        m_hIndicatorImage;
    long        m_hRedImage;
    long        m_hGreenImage;

    // Handle to file for file I/O results
    HANDLE      m_hBlobFile;
};


