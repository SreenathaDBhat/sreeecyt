#ifndef _MicControl_H
#define _MicControl_H

#include "Axes.h"
#include "MotorController.h"
#include "conversion.h"
#include "AutoScope.h"


#define DEBACKLASH	200		/* Steps to move to remove any backlash */


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Parameter limit for microscope operation 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define PositionalTolerance 35			//tolerance allowed in re-positioning for check, load and unload positions

typedef struct loaded_slide {
					int Loaded;
					int fromPosition;
					int fromTray;
					} LoadedSlide;

//********************************************************
//make these functions/definitions  available to all cytovision elements

							//microscope control routines
int CreateMicControl();
int DestroyMicControl();


//////////////////////////////////////////////////////////////////////////////

class CMicroscopeControl
{
//friend class SpeedThread;

public:
	 CMicroscopeControl();
	~CMicroscopeControl();

		void AllocateRotorSpace (RotorData *Rd, int Size);

						//file reading / writing functions
		int ReadParametersFromFile (LPTSTR filename, AxisConfig **Axes);
		void ReadStageAxes (AxisConfig **Axis, LPTSTR FileName);
		void ReadFilterBlocks (AxisConfig **Axis, LPTSTR FileName);
		void ReadObjectives (AxisConfig **Axis, LPTSTR FileName);
		void ReadBlock (AxisConfig **Axis, LPTSTR label, LPTSTR FileName);
		int ReadDriveConfig (AxisConfig **Axis, LPTSTR label, LPTSTR FileName);
		void ReadCondenserBlock (AxisConfig **Axis, LPTSTR FileName);
		void ReadCalibrationData (LPTSTR FileName);

		int WriteParametersToFile (LPTSTR filename, AxisConfig *Axes);
		void WriteDriveConfig (LPTSTR FileName,  AxisConfig *Axis);
		void WriteStageCalibrationData (LPTSTR FileName);
		void WriteBlock (LPTSTR FileName, AxisConfig *Axis);

		AxisConfig *CreateAxis (AxisConfig *Axes);
		AxisConfig *DeleteAxis (AxisConfig *Axes);
		void DestroyAxes (AxisConfig *Axes);
		void DestroyAxisConfig ();
		AxisConfig *DuplicateAxis (AxisConfig *Axis);
		void FreeAxis (AxisConfig *Axis);
		StageCalibrationData * DuplicateStageInfo ();

		void Capabilities(MicConfig *Configuration, int *nAxes);
		void AddCapabilities(MicConfig Configuration[], int nAxes, BOOL connectNewAxes);
		void GetLabel (int AxisID, LPTSTR label);
		void CreateDriveData(AxisConfig *pAxis);
		void CopyDriveData(DriveData *pThisDrive, DriveData *pConfigDrive);
		void CreateRotorData(AxisConfig *pAxis, int NElements);
		void CopyRotorData(RotorData *pRotor, RotorData *pConfigRotor);
		void UpDateRotorData (AxisConfig *pAxis, AxisConfig *pDupAxis, StageCalibrationData *pDupStageInfo);

		void CreateDummyAxis (int AxisID);
		void CreateTransforms();
		void ModifyTransforms(int AxisID,int partNo);

		void SaveCurrentState (AxisConfig *Axis);
		void RestoreState ();

						//microscope general control functions
		int Initialize(UINT ClientID);
		void InitialiseAxes();
		HINSTANCE LoadDLL(int ctrlType, const char fileName[], char createFnName[], char destroyFnName[]);
		void Destroy();
		int Open(short *FailedAxis, BOOL connectAll = FALSE);
		int SetControlState(int state);
		int GetControlState (int *state);
		int AutoDetect();
		BOOL DetectControllerAxis(MicConfig *pConfig);
		int Busy();
		int ZBusy();
		int Wait(char *Axes);
		int Status();
		int Stop();
		int SaveConfiguration();
		int SaveJoystickSettings();
		int GetLastMicError(ErrorCodeData *ErrorData);
		int ClearLastMicError();
		void SetErrorCondition(int ErrorCode);
		void SetErrorCondition (int AxisID, int CtrlType, int ErrorCode);
		void SetErrorCondition (int AxisID, int ErrorCode);
		void ClearErrorCondition();
		void UpdateClients();
		int RemoveAxis (int AxisID);
		int Identify (int AxisID, char *Name);

						//microscope stage (x,y,z) control functions
		HANDLE GetPortHandle();
		int ReOpen(HANDLE fd);

		int MoveTo (int x, int y, int z);
		int MoveTo (float x, float y, float z);
		int MoveBy (int x, int y, int z);
		int MoveAxisBy (int axis, int dist, int updateclient);
		int MoveAxisTo (int axis, int pos, int updateclient);
		int MoveAxisBy (int axis, float dist, int updateclient);
		int MoveAxisTo (int axis, float pos, int updateclient);
		int MoveXYBy (float xdist, float ydist, int updateclient);
		int MoveXYTo (float xpos, float ypos, int updateclient);
		int MoveByFOV(int axis, int direction, int overlap);
		int SetPosition (int x, int y, int z);
		int SetPosition (int axis, int pos);
		int SetPositionX (int x);
		int SetPositionY (int y);
		int SetPositionZ (int z);

		int GetCurrentBay(int *CurrentBay);
		int GetPosition (int *x, int *y, int *z);
		int GetPosition (float *x, float *y, float *z);
		int GetAbsPosition (int *x, int *y, int *z);
		int GetAbsPosition (float *x, float *y, float *z);
		int SetMaxPosition(int axis, double pos);
		int GetMaxPosition(int axis, double *pPos);

		int SetTolerance(int axis, float tolerance);
		int SetSpeed(int axis, int speed);
		int SetSpeed (int axis1, int speed1, int axis2, int speed2);
		int SetZSpeed(double StartSpeed, double Speed);
		int MoveAxisAtSetSpeed(int axisID, float speed, int *pStepPerSecond);
		//static void SpeedThread ();
		int StageWaitMode(int mode);
		int StageBusy();
		int StagePark();
		int GetDatum (char *Axes);
		int DatumFound(char *Axes);
		int UpdatePosition();
		int StageStatus();
		int StageWait();
		int StageStop();
		void SetStageCallback (void (*f)(int x, int y, int z));
		void SetBayCallback (void (*f)(int bay));
		void SetRotorCallback (void (*f)(int AxisID, int Position, int Action));
		int SetUpdatePositions(int Enable);
		void SetGeneralCallback (void (*f)(int AxisID, int Setting));

						//ideal coordinate functions
		int GetIdealPosition (int *x, int *y, int *z);
		int GetIdealPosition (float *x, float *y, float *z);
		int GetIdealZ (float *z);
		int MoveToIdealPosition (int x, int y, int z);
		int MoveToIdealPosition (float x, float y, float z);
		int MoveToIdealZ (float z);
		int MoveAxisToIdealPosition (int axis, float pos);
		int MoveAxisToIdealPosition (int axis, int pos);
        int MoveStageToIdealXY(float posx, float posy);
		int MoveAxisByIdeal (int axis, float dist);
		int RelocateToIdealPosition (float x, float y, float z);
		int RelocateToStagePosition (float x, float y, float z);
		int ConvertToStage (int *x, int *y, int *z);
		int ConvertToStage (float *x, float *y, float *z);
		int ConvertToIdeal (int *x, int *uy, int *z);
		int ConvertToIdeal (float *x, float *y, float *z);
		int GetIdealEnglandFinder(int point, LPTSTR EFCoord);
		int SetEFOffset (int point);
		int GetEFOffset (int point, double *x, double *y);

						//coordinate conversion functions
		int StageToEF (int x, int y, char *EFCoords);
		int IdealToEF (float x, float y, char *EFCoords);
		int MoveToEF (char *EFCoords);
	
				//calibration feature location
		int MoveToCalibrationFeature( int FeatureID);
		int RecordCalibrationFeature (int FeatureID);
		int ReadCalibrationFeatures();
		int WriteCalibrationFeatures();
						//stage calibration/information functions
		int GetNumberOfBays(int *NBays);
		int GetSystemSlideCapacity(int *NSlides);
		void GetTraysAndSlides (int *nTrays, int *nSLidesPerTray);

		int SetBayDatum (int BayNo, int SetDefaultIdeal);
		int MoveToOilDatum (int BayNo);
		int SetDefaultIdealCoordinates ();
		int SetBayOrigin (int BayNumber, LPTSTR EFOrigin, int SetDefaultIdeal);
		int GetBayOrigin (int *BayNumber, LPTSTR EFOrigin);
		int SetOilOrigin();
		int MoveToFocalPoint ();
		int GetFocalPoint();
        int GetBayFocalPoint(int BayNumber, int *FP);
		int SetFocalPoint (int BayNo);

		int SetXYScale (double xscale, double yscale);
		int GetXYScale (double *xscale, double *yscale);
		int GetXYStepSize (double *xstepsize, double *ystepszie);
		int SetXYBacklash (double xbacklash, double ybacklash);
		int GetXYBacklash (double *xbacklash, double *ybacklash);
		int SetFocusBacklash (double zbacklash);
		int GetFocusBacklash (double *zbacklash);
		int SetIdealCalibrationPoint (double x, double y, double z, int point);
		int GetIdealCalibrationPoint (double *x, double *y, int point);
		int GetIdealPoint (double *x, double *y, int point);
		int SetImageScale (double x, double y, int objective, int image_width, int image_height);
		int CalculateImageScale (int objective, double power);
		int GetImageScale (double *x, double *y);
		int GetImageScale(int objective, double *x, double *y);             // Get the image scale for the specified objective
		int GetImagePixels (int *x, int *y);

		int SetFOVOverlap(int overlap);
		int GetFOVOverlap(int *overlap);

						//stage - slide loader related functions
		int RecordLoadPosition();
		int RecordUnloadPosition();
		int RecordCheckPosition();
		int RecordZUpperLimit();
		int RecordStartPosition();
		int GetStartPosition (int *x, int *y, int *z);
		int GetLoadPosition (int *x, int *y, int *z);
		int GetUnloadPosition (int *x, int *y, int *z);
		int GetCheckPosition (int *x, int *y, int *z);
		int GetZUpperLimit (int *z);
		int MoveToStartPosition();
		int MoveToLoadPosition();
		int MoveToUnloadPosition();
		int MoveToCheckPosition();
		int MoveToZUpperLimit();
		int LoadSlide(char *Axes, int iTrayPosition, int iSlidePosition, BOOL HomeStage, BOOL ReadBarcode, BOOL ReturnZ, BOOL HuntForNextTray);
		int LoadSlide_N(char *Axes, int iSlidePosition, BOOL HomeStage, BOOL ReadBarcode, BOOL ReturnZ, BOOL HuntForNextTray);
		int UnloadSlide();
		int MoveToBay(int iSlidePosition, BOOL ReturnZ /*= TRUE*/);
		int GetBayCoordinates (int nBay, float *x, float *y);
		int GoToBayPosition(int iSlidePosition);
		int MoveToManualLoad();
		int MoveToManualUnLoad();

		int ProceedToLoad(int iSlideIndex);
		int SwapTray (int iTrayPosition, int iSlidePosition, BOOL ReadBarcodes, BOOL ReturnZ);

						//microscope focus (z) control functions
		int FocusPosition (int *z);
		int FocusPosition (float *z);
		int FocusMoveAbs (int position);
		int FocusMoveAbs (float position);
		int FocusWait();
		int FocusHome();
		int FocusStatus();
		int FocusMoveRel (int dz);
		int FocusMoveRel (float dz);
		int FocusStop();

						//microscope filter wheel control functions
		int ReInitializeRotor (int axisid, int *maxelements);
		int InitializeRotor (int axisid, int *maxelements);
		int GetWheelInfo (int axisid, LPTSTR Element, LPTSTR *lpString);
		int GetWheelInfo (int axisid, LPTSTR Element, int *param);


								//get information for filter wheels/blocks and objective nosepiece
		int InRange (AxisConfig *ThisAxis, int partNo);
		int GetName (int axisid, int partNo, LPTSTR *partName);
		int GetFocusOffset (int axisid, int partNo, double *offset);
		int GetCurrentObjectiveFocusOffset(double *offset);
		int GetStageOffset (int axisid, int partNo, double *xoffset, double *yoffset, double *zoffset);
		int GetIdealStageOffset (int axisid, int partNo, double *xoffset, double *yoffset, double *zoffset);
		int GetBase (int axisid, int *partNo);
		int GetAxisID (int *axisid, LPTSTR axis_name);
		int GetAxisName (int axisid, LPTSTR *axis_name);
		int GetApplyOffset (int axisid, int *applyoffset);
		int CalculateStageOffset (int axisid, int partNo, int CurrentPos, double *deltaX, double *deltaY, double *deltaZ);
		int CalculateLampOffset (int axisid, int partNo, int *deltaZ);
		int GetPositionOfElement (int axisid, int *partNo, LPTSTR partName);
		int GetPositionOfElementType(int axisid, int *partNo, LPTSTR partName, int type);
		int GetCurrentElement (int axisid, int *partNo);
		int GetMaxElements (int axisid, int *MaxElements);

		int GetLensPower (int Objective, double *LensPower);
		int SetLensPower (int Objective, double LensPower);
		int GetLensType (int Objective, int *LensType);
		int SetLensType (int Objective, int LensType);

								//set information
		int SetName (int axisid, int partNo, LPTSTR partName);
		int SetBase (int axisid, int filterNo);
		int ResetOffsets (int axisid, int type);
		int ResetZOffsets (int axisid, int type);
		int SetApplyOffset (int axisid, int applyoffset);
		int UpdateBasePosition(int axisid);
		int SetStageOffset (int axisid, int partNo);
		int SetStageZOffset (int axisid, int partNo);
		int BackupObjectiveOffsets();
		int RestoreObjectiveOffsets();
								//movement functions
		int MoveToElement (int axisid, int Position, int waitmove, BOOL Ignore_Control_State, BOOL ForceMove = FALSE, BOOL MoveZ = TRUE, BOOL OilChange = TRUE);
		int MoveToElement (int axisid, LPTSTR partName, int waitmove, BOOL Ignore_Control_State, BOOL ForceMove = FALSE, BOOL MoveZ = TRUE, BOOL OilChange = TRUE);
		int MoveZAway ();
		int PositionStageForElement (int axisid,int waitmove, BOOL Ignore_Control_State);
		int WheelWait (int wheelid);
		int WheelHome  (int wheelid, BOOL Ignore_Control_State);


						//slide loader functions
		int SL_BeginSilent(void);
		int SL_CheckForSlideOnStage(BOOL *bSlidePresent);
		int SL_ClearCurrentFault(void);
		int SL_EndSilent(void);
		int SL_GetLastFault();
		int SL_GetOccupiedSlidePositions(int *Slides);
		int SL_GetOccupiedTrayPositions(int *Trays);
		int SL_GetSlideBarcode(int iSlidePosition, char **strBarcode);
		int SL_GetSlideBarcode_From_Index(int iSlideIndex, char **strBarcode);
		int SL_GetSlideBarcodesOnTray(char *astrBarcodes, int max_string_length,int *NBarcodes);
		int SL_GetTrayBarcode(char **strBarcode);
		int SL_GetSlideLoaderStatus();
		int SL_LoadSlide(int iSlidePosition);
		int SL_RemoveSlide(void);
		int SL_ScanSlidePositions(void);
		int SL_ScanTrayPositions(void);
		int SL_SelectTray(int iTrayNumber);
		int SL_CurrentTray (int *CurrentTray);

		int SL_MoveRobotHeadAboveStage();
		int SL_FinishCalibration();
		int SL_StartCalibration (char *Axes); 
		int SL_GetTrayTampered(int *Trays);
		int SL_CheckSlidePresence (BOOL On);
		int SL_ScanWholeTray ();
		int SL_Mute (BOOL On);
		int SL_GetListOfTraysTampered (int *Trays, int num_trays);
		int SL_PositionValid (int SlidePosition);
		int SL_PositionValid (int SlidePosition, int TrayPosition);
		int SL_TrayOnStage();
		int SL_Park(void);
		int SL_CassetteJog(double Amount);
		int SL_CassetteMove (double Pos);
		int SL_DetectTray(int TrayPosition);
		int SL_MoveCassette(int iTrayNum);
		int SL_BacklashMove(int iTrayNum);		
		int SL_IsDoorOpen (BOOL *Open);

				//shutter
		int SetShutter (int State, int waitmove);
		int GetShutter (int *State);

				//BF shutter
		int SetBFShutter (int State, int waitmove);
		int GetBFShutter (int *State);

				//lamp
		int SetLamp (int Value, int waitmove);
		int GetLamp (int *Value);
		void AdjustLampBy (int Value);
		int SetLampOffset (int axisid, int partNo);
		int GetLampOffset (int axisid, int partNo, int *offset);

				//condenser
		int SetCondenser (int State, int waitmove);
		int GetCondenser (int *State);
		int GetCondenserSetting (int axisid, int partNo, int *Setting);
		int SetCondenserSetting (int axisid, int partNo, int Setting);
		BOOL MoveForCondenser(int *x, int *y, int *z, int waitmove);

				//aperture
		int SetAperture (int Value, int waitmove);
		int GetAperture (int *Value);
		int GetApertureSetting (int axisid, int partNo, int *Setting, BOOL FluorescentComponent);
		int SetApertureSetting (int axisid, int partNo, int Setting, BOOL FluorescentComponent);
		int GetMaxMinValues (int axisid, int *max, int *min);

				//field (Brightfield)
		int SetField (int Value, int waitmove);
		int GetField (int *Value);
		int GetFieldSetting (int axisid, int partNo, int *Setting);
		int SetFieldSetting (int axisid, int partNo, int Setting);


						//field (fluorescent)
		int SetField_Fl (int Value, int waitmove);
		int GetField_Fl (int *Value);

				//joystick control
		int Jog (int AxisID, int Direction, int Speed);
		int SetJogSpeed (int AxisID, int Speed, int jogSpeed);
		int GetJogSpeed (int AxisID, int Speed, int *jogSpeed);


				//stage (x,y,z) 
		int GetPositionX (int *x);
		int GetPositionY (int *y);
		int GetPositionZ (int *z);
		int GetPositionX (float *x);
		int GetPositionY (float *y);
		int GetPositionZ (float *z);

				//fluorescent lamp
		int FL_Lamp_GetStatus(int *Status);
		int FL_Lamp_SwitchLamp (BOOL Setting, int waitmove);
		int FL_Lamp_SetLamp (int Setting, int waitmove);
		int FL_Lamp_GetLamp (int *Setting);
		int FL_Lamp_SetShutter (int Setting, int waitmove);
		int FL_Lamp_GetShutter (int *Setting);
		int FL_Lamp_RunTimed (int waitmove);
		int FL_Lamp_ClearAlarm ();
		int FL_Lamp_SetExposure (double Exposure);
		int FL_Lamp_GetExposure (double *Exposure);
		int FL_Lamp_GetLampAge (int *Hours);
		int FL_Lamp_Ready (int *Ready);
		int FL_Lamp_LampOn (int *On);

		int Oiler_BeginOiling(BOOL Zero_Z);
		int Oiler_DispenseOil();
		int Oiler_EndOiling();
		int Oiler_OilSlide(float x, float y);

		LoadedSlide OnStage;

		tripoint Direction;				//movement direction of axes

		int FindXYLimits();
		int GetXYLimits (float *x0, float *y0, float *x1, float *y1);
		int GetPoints_B_C (float *x0, float *y0, float *x1, float *y1);

		void GetCrossCentre (float *x, float *y, float *z);
		void SaveCrossCentre (float x, float y, float z);

		int De_Backlash (int xdir, int ydir);
		int GetMicroscopeName (LPTSTR *name);
		BOOL IsThisAxisPresent (int AxisID);
		BOOL IsThisAxisMotorised (int AxisID);

		int SetLocalRemote (int Remote);
		int PreScanCheck();
		int PostScanCheck();

		void SaveCurrentBay();
		int MoveToZLoadPosition ();

        // Focus measurements
        void SetFocusMeas(double F) { m_FocusMeas = F; }
        double GetFocusMeas(void) { return m_FocusMeas; }
        void SetFocusZ(float Z) { m_FocusZ = Z; }
        float GetFocusZ(void) { return m_FocusZ; }

		int OnStageGet();
		void OnStageSet (int Loaded, int Slide, int Tray);

		void SetSystemHomed(BOOL homed);
		BOOL GetSystemHomed();

        int GetOptionalMagnification(DOUBLE* magnification);

        int EnableZUpperLimit();
        int DisableZUpperLimit();

		int SetFluorescenceMode (BOOL bOn);

		int PutLastTrayInLoop (int TrayNum);
		int GetLastTrayInLoop (int *TrayNum);
		 
private:
		int LastTrayInLoop;

		double version;
		int nFilterBlocks;			//record of number of filter wheels/blocks on the system
		int SetCurrentController (int axis);
		int GetController (int axis, CMotorController **Controller);
		void SetCurrentAxisConfig (int axis);
		AxisConfig * GetThisAxisConfig (int axis);
		AxisConfig * m_CurrentAxis;
		RotorData * m_BackupObjectiveOffsets;
		AxisConfig *ControlAxes;
		StageCalibrationData StageInfo;
		CMotorController *m_CurrentController;
		BOOL m_bcom_port;
		BOOL Datum_Z;
		BOOL m_bControlState;
		BOOL m_bIsSlideLoader;
        double m_FocusMeas;
        float  m_FocusZ;
		//SpeedThread speedThread;

		void (*StageCallbackFunc)(int x, int y, int z);
		void (*BayCallbackFunc)(int bay);
		void (*RotorCallbackFunc)(int axisid, int position, int action);
		void (*ErrorCallbackFunc)( int ErrorCode);
		void (*GeneralCallbackFunc)(int AxisID, int setting);

		int stage_wait_mode;
		int set_update_positions;
		int m_CurrentObjective;		//current setting for objective lens
		ErrorCodeData LastError;

		float Zs;							//focus position prior to offset adjustmens
		int m_RotorElement;		//rotor position to move to and adjust offsets

					//coordinate conversion
		CConversion CoordinateConversion;

					//Applied Imaging Calibration Slide -- feature location
		tripoint FeatureBase;		//recorded position of point A on AII calibration slide
		tripoint ManualLoad;		//recorded coordinates of stage for manual slide loading
		BOOL ManualLoading;			//flag indicating manual loading is underway

		BOOL SlideLoaderPresent;

		CoordPoint CrossCentre;

		TCHAR microscopeFilename[MAX_PATH];
		TCHAR featurebaseFilename[MAX_PATH];

		BOOL m_bSystemHomed;
		BOOL firstDatumZRequest;
		float preDatumZ;

		int MoveGenetixConfigFiles(int loaderType);
		int MoveGenetixFile(TCHAR *sourceFile, TCHAR *destFile);
		
		BOOL	m_bFluorescentMode;

};

typedef CMicroscopeControl * (__cdecl * INITMICCONTROLPROC)(void);
typedef void (__cdecl * DESTROYMICCONTROLPROC)(CMicroscopeControl *);


#endif
