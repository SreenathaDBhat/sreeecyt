//
// MicroscopeDialog.h
//
#include "MicInterface.h"
#include "AutoScope.h"
#include "MicroErrors.h"


#ifndef	_MICROSCOPEDIALOG_H
#define	_MICROSCOPEDIALOG_H

#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

#define MICROSCOPE_GUI_APP	2

typedef enum {
						XYSTAGECONTROL = 1,
						FIELDOFVIEW,
						ZDRIVE,
						STAGECOORDINATES,
						EXCITATION_FILTERS,
						ROTORCONTROL,
						SLIDELOADER,
						OBJECTIVES,
						SHUTTER,
						CONDENSER,
						LAMP,
						FIELD_DIAPHRAGM,
						APERTURE_DIAPHRAGM,
						LIGHTPATH,
						} MicroscopeGUI;

class MICROSCOPEGUI_API MicroscopeDialog : public CDialog
{
// Construction
public:
	MicroscopeDialog(UINT idd, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(MicroscopeDialog)
	//enum { IDD = 0 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	UINT id;
	int xpos, ypos;		// Position.
			
				//override these functions to pass information about the state of the microscope to GUI
	virtual void SetX(int x) {return;}		//x- coordinate
	virtual void SetY(int y) {return;}		//y-ccordinate
	virtual void SetZ(double z)  {return;}		//z-coordinate
	virtual void SetI (int Intenstity) {return;}		//lamp setting
	virtual void SetElement (int AxisID, int Position) {return;}	//filter /lens selection
	
	virtual LRESULT HandleMessage (WPARAM MsgCode, LPARAM MsgData) { return 0; }
	virtual BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag) {return FALSE;}

	void Size (int width, int height);
	void ShowControl (UINT CtrlID, BOOL show);

	IMicSrvr *m_pIMicSrvr;
	CMicInterface m_microscope;
	short FailedAxis;
	long thisWnd;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(MicroscopeDialog)
	public:
	virtual BOOL Create(UINT IDD, CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(MicroscopeDialog)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
	afx_msg void OnMove(int x, int y);
	//}}AFX_MSG

	afx_msg BOOL OnToolTipNotify( UINT id, NMHDR * pNMHDR, LRESULT * pResult );
	afx_msg LRESULT OnMicMessage(WPARAM, LPARAM);

	DECLARE_MESSAGE_MAP()

	void AddBitmapToButton(UINT ButtonID, UINT iconID, int Size);
 	void SetValueText(UINT CtrlId, LPTSTR text);
	void SetCtrlSensitive(UINT CtrlId, BOOL set);

	CWnd ** flagWnd;	//Pointer to window pointer, use a flag for deletion
	BOOL bCreated;		// Set to true if Created, False if called via DoModal()
	int m_ErrorCode;
	int m_nAxisID;
	CWnd *m_pParent;
	CWnd *m_pMessageWnd;
	BOOL m_bLockFlag;

private:

protected:
	virtual void OnCancel();
	virtual void OnOK();
};


#endif