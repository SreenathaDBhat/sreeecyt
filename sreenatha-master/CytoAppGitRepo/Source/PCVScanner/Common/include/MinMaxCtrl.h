
/*
 * MinMaxCtrl.h
 * JMB March 2002
 */

#include <afxcmn.h>

class CMinMaxSlider : public CWnd
{

protected:

	CSliderCtrl minSlider;
	CSliderCtrl maxSlider;

	CStatic minStatic;
	CStatic maxStatic;


public:

	CMinMaxSlider();
	BOOL Create(const RECT& rect, CWnd* pParentWnd, UINT nID);
	void SetRange(int min, int max, BOOL update = TRUE);
	void SetAppropriateRange(int min, int max, int limitMin, int limitMax, BOOL update = TRUE);
	void ShowSliders(BOOL showMin = TRUE, BOOL showMax = TRUE);
	void EnableSliders(BOOL showMin = TRUE, BOOL showMax = TRUE);
	void GetRange(int *pMin, int *pMax);
	void SetMinMax(int min, int max);
	void GetMinMax(int *pMin, int *pMax);
	void Update();


	// Message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	DECLARE_MESSAGE_MAP()
};




