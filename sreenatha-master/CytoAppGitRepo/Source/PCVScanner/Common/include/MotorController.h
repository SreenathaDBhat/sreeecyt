/*
 *		M O T O R C O N T R O L L E R . H
 *				general motor control 
 */

#ifndef _MOTORCONTROLLER_H
#define _MOTORCONTROLLER_H

#define CONTROL_FAILURE	-1
#define CONTROL_SUCCESS 1

#include <iostream>

#include "MicroErrors.h"
#include "Axes.h"
//#include "CommPort.h"

// Filter Wheels
// NB Enums match Cytovision ones (see Wheel.h)


//////////////////////////////////////////////////////////
// Absolute Maximum Number Of Objectives/Filters        //
// Supported By The Drivers                             //
//////////////////////////////////////////////////////////

//#define mcMAX_ELEMENTS					10		// Max Number of Units (Filters or Objectives)
//#define mcMAX_DESCRIPTION_LENGTH		32		// Max Length Of String Allowed For Unit Description

//////////////////////////////////////////////////////////
// Objective and Filter Information Structures          //
//////////////////////////////////////////////////////////

//typedef char MInfoData[mcMAX_ELEMENTS][mcMAX_DESCRIPTION_LENGTH];


//#define GM_COM_BUF_LEN          64


class CMotorController {
public:
	CMotorController(void);
	~CMotorController();
	void _MOTOR_DEBUG(int Type, char * name, char* message, ...);
	void SetAxisID (int AxisID);

//Overrides
						//General
				//set up the motor control object with the necessary data
		virtual int SetUp (int AxisID, int iPortAddr, AxisData *AxisControl){ return erMETHOD_NOT_IMPLEMENTED; }
				//switch between manual and computer control
		virtual int SetControlState( int state){ return erMETHOD_NOT_IMPLEMENTED; } 
				//set the mode of the motor (eg IMS mutli or single axis mode)
		virtual void SetMode (int mode){ return ; } 
				//check for presence of motor
		virtual int AutoDetect(){ return erMETHOD_NOT_IMPLEMENTED; }
				//determine if motor is moving or not 
		virtual int Busy(){ return erMETHOD_NOT_IMPLEMENTED; } 
				//wait until motor comes to rest
		virtual int Wait(){ return erMETHOD_NOT_IMPLEMENTED; } 
				//stop motor immediately
		virtual int Stop(){ return erMETHOD_NOT_IMPLEMENTED; }
				//determine if motor is moving or not
		virtual int Status(){ return erMETHOD_NOT_IMPLEMENTED; }
				//shutdown the controller, removing all memory allocation, dlls etc 
		virtual int ShutDown() {return erMETHOD_NOT_IMPLEMENTED; }


						//Stage (x,y,z) functions
				//absolute positioning
		virtual int MoveTo (int pos){ return erMETHOD_NOT_IMPLEMENTED; } 
		virtual int MoveTo (float pos){ return erMETHOD_NOT_IMPLEMENTED; } 
		virtual int MoveXYTo (float xpos, float ypos) { return erMETHOD_NOT_IMPLEMENTED; }
		
				//relative positioning
		virtual int MoveBy (int dist){ return erMETHOD_NOT_IMPLEMENTED; } 
		virtual int MoveBy (float dist){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int MoveXYBy (float xdist, float ydist) { return erMETHOD_NOT_IMPLEMENTED; }

				//set position 
		virtual int SetPosition (int pos){ return erMETHOD_NOT_IMPLEMENTED; } 
		virtual int SetPosition (float pos){ return erMETHOD_NOT_IMPLEMENTED; } 

				//get (read) position
		virtual int GetPosition (int *pos){ return erMETHOD_NOT_IMPLEMENTED; } 
		virtual int GetPosition (float *pos){ return erMETHOD_NOT_IMPLEMENTED; } 
		virtual int GetAbsPosition (int *pos){ return erMETHOD_NOT_IMPLEMENTED; } 
		virtual int GetAbsPosition (float *pos){ return erMETHOD_NOT_IMPLEMENTED; } 

				//set speed, axis to move at speed and direction
		virtual int SetSpeed(int speed){ return erMETHOD_NOT_IMPLEMENTED; } // As a percentage
		virtual int SetSpeed(int axis1, int speed1, int axis2, int speed2) {return erMETHOD_NOT_IMPLEMENTED; } // As a percentage
		virtual int SetSpeed(float speed, int *pStepsPerSecond) {return erMETHOD_NOT_IMPLEMENTED;} // Units of mm/sec
		virtual int SetZDriveSpeed(double StartSpeed, double Speed) { return erMETHOD_NOT_IMPLEMENTED; }

				// movement tolerance
		virtual int SetTolerance(double tolerance){ return erMETHOD_NOT_IMPLEMENTED; } // In microns
		virtual int GetTolerance(double *pTolerance){ return erMETHOD_NOT_IMPLEMENTED; }

				// set and get upper limit, in microns
		virtual int SetMaxPosition(double maxPos) { return erMETHOD_NOT_IMPLEMENTED; }
		virtual int GetMaxPosition(double *pMaxPos) { return erMETHOD_NOT_IMPLEMENTED; }

		virtual int SetZUpperLimit(double focusPos) { return erMETHOD_NOT_IMPLEMENTED; }
		
				//homing functions
		virtual int ResetStage(){ return erMETHOD_NOT_IMPLEMENTED; } 
		virtual int GetDatum (){ return erMETHOD_NOT_IMPLEMENTED; } 
		virtual int DatumFound(){ return erMETHOD_NOT_IMPLEMENTED; } 
		virtual int StageLimit (int Speed, int Direction) {return erMETHOD_NOT_IMPLEMENTED; }

			    //drive pulse control
		virtual int SetStepSize(int StepSize) { return erMETHOD_NOT_IMPLEMENTED; }
		virtual int GetStepSize(int& StepSize) { return erMETHOD_NOT_IMPLEMENTED; }
		virtual int Step(int NumSteps) { return erMETHOD_NOT_IMPLEMENTED; }

				//specific focus functions
		virtual int InitialiseFocus() { return erMETHOD_NOT_IMPLEMENTED;}
		virtual int MoveFocusRelLimit (int FocusPos){ return erMETHOD_NOT_IMPLEMENTED;}
		virtual int GetFocusRelLimit (int &FocusPos) {return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SetPositionZ(int z) {return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SetMovementLimits(int OldDatum) {return erMETHOD_NOT_IMPLEMENTED; }
		virtual int MoveFocusUpper(){return erMETHOD_NOT_IMPLEMENTED; }
		virtual int MoveFocusLower(){return erMETHOD_NOT_IMPLEMENTED; }

				//Move Z axis to preset positions for loading/working  -- Not to be confused with SlideLoader functions with similar names
		virtual int MoveToLoadPosition() {return erMETHOD_NOT_IMPLEMENTED; }
		virtual int WaitForLoadPosition() {return erMETHOD_NOT_IMPLEMENTED; }
		virtual int MoveToWorkPosition() {return erMETHOD_NOT_IMPLEMENTED; }
		virtual int WaitForWorkPosition() {return erMETHOD_NOT_IMPLEMENTED; }

						//filter wheel functions
		virtual int InitRotor (int PortNo, AxisData *Data) { return erMETHOD_NOT_IMPLEMENTED; }
		virtual int Home () { return erMETHOD_NOT_IMPLEMENTED; }
		virtual int WheelWait() { return erMETHOD_NOT_IMPLEMENTED; }
		virtual int MoveToElement (int PartNo,  int waitmove) { return erMETHOD_NOT_IMPLEMENTED; }
	//	virtual int MoveToFilter (char *FilterName,  int waitmove) { return erMETHOD_NOT_IMPLEMENTED; }
		virtual int GetCurrentElement (int *Position){ return erMETHOD_NOT_IMPLEMENTED; }

						//slide loader functions
		virtual int SL_BeginSilent(){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_CheckForSlideOnStage(BOOL *bSlidePresent){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_ClearCurrentFault(){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_EndSilent(){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_FinishCalibration() { return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_GetDLLVersion (long *dllversion){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_GetFirmwareVersion (long *firmwareversion){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_GetEventLogFilename (char *filename){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_GetHardwareStatus (){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_GetLastFault() {return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_GetOccupiedSlidePositions (int *Slides) {return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_GetOccupiedTrayPositions (int *Trays) {return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_GetSlideBarcode (int iSlidePositions, char **strBarcode){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_GetSlideBarcode_From_Index (int iSlideIndex, char **strBarcode){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_GetSlideBarcodesOnTray (char *astrBarcodes,int max_string_length, int *NBarcodes){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_GetStatus(){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_GetTrayBarcode (char **strBarcode){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_GetTrayTampered (int *Trays){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_Initialise(){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_LoadSlide (int iSlidePositon, BOOL ReadBarcodes){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_MoveRobotHeadAboveStage(){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_Park() { return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_RemoveSlide(){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_ScanSlidePositions() { return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_ScanTrayPositions(){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_SelectTray (int iTrayNumber){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_CurrentTray (int *iTrayNumber) {return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_SetDebugMode (BOOL bDebugMode){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_Hardware (BOOL bPumpOn, BOOL pValveOn){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_StartLeakTest (int iSlideNumber){ return erMETHOD_NOT_IMPLEMENTED; }
		virtual void SL_TranslateReturnCode (int error_code){return ; }
		virtual int SL_CheckSlidePresence (BOOL On){ return erMETHOD_NOT_IMPLEMENTED;}
		virtual int SL_ScanWholeTray (){return erMETHOD_NOT_IMPLEMENTED;}
		virtual int SL_Mute (BOOL On) {return erMETHOD_NOT_IMPLEMENTED;}
		virtual int SL_GetListOfTraysTampered(int *Trays, int num_trays){return erMETHOD_NOT_IMPLEMENTED;}
		virtual	int SL_NumberOfTrays () {return 0;}
		virtual int SL_NumberOfSlidesPerTray(){ return 0;}
		virtual int SL_SwapTray(int iTrayNumber, BOOL ReadBarcodes) {return erMETHOD_NOT_IMPLEMENTED;}
		virtual int SL_IsTrayLoaded(){return erMETHOD_NOT_IMPLEMENTED;}
		virtual int SL_SwapTrayAndMove(int iTrayNumber, BOOL ReadBarcodes, float xpos, float ypos) {return erMETHOD_NOT_IMPLEMENTED;}
		virtual int SL_LoadSlideAndMove(int iTrayNumber, BOOL ReadBarcodes, float xpos, float ypos) {return erMETHOD_NOT_IMPLEMENTED;}
		virtual int SL_HasDoorBeenOpened (BOOL reset){return erMETHOD_NOT_IMPLEMENTED;}
		virtual int SL_CassetteJog(double Amount) { return erMETHOD_NOT_IMPLEMENTED; }
		virtual int SL_CassetteMove (double Pos) {return erMETHOD_NOT_IMPLEMENTED;}
		virtual int GetSystemPosition (int SysPos, float *x, float *y) {return erMETHOD_NOT_IMPLEMENTED;}
		virtual int SL_GetSystemInfo(int SysInfo, float *param) {return erMETHOD_NOT_IMPLEMENTED;}
		virtual int SL_IsTrayDetected (){return erMETHOD_NOT_IMPLEMENTED;}
		virtual int SL_IsDoorOpen (BOOL *Open) {return erMETHOD_NOT_IMPLEMENTED;}

			//AUTOMATED MICROSCOPES
			    // General Microscope
			//initilise the microscope, generally to open comport and connect
	 virtual int MicInitialise(int CommsInterface) { return erMETHOD_NOT_IMPLEMENTED; } 
			// reset the microscope
	 virtual int Reset() { return erMETHOD_NOT_IMPLEMENTED; }
			//get (read) name or identity of microscope (may be firmware version)
	 virtual int Identify(char *Name) { return erMETHOD_NOT_IMPLEMENTED; }

	/////////////////////////////////////////////////////////////////////////////////
    // Observation Light Path Set and Get Functions
    // See LPC Definitions
 	/////////////////////////////////////////////////////////////////////////////////
	virtual int SetObservationLightPath(int LightPath) { return erMETHOD_NOT_IMPLEMENTED; }
	virtual int GetObservationLightPath(int& LightPath) { return erMETHOD_NOT_IMPLEMENTED; }

	/////////////////////////////////////////////////////////////////////////////////
    // Filter Block Commands
    // Set or Get A Filter In The Block - 1 to MaxFilters
    // NOTE The Second Set Is For Microscopes With more Than One Filter Wheel
    // In Which Case The SetFilter(FilterNum) Command Will Work But With The
    // Currently Selected Filter Wheel. The Wheel Is Selected Through use Of 
    // SetFilter(FilterWheel, FilterNum) In Which Case FilterWheel Becomes
    // The Current FilterWheel Or Use Of SetFilterWheel and GetFilterWheel Functions
 	/////////////////////////////////////////////////////////////////////////////////
  //  virtual int SetFilter(int FilterNum) { return erMETHOD_NOT_IMPLEMENTED; }
   // virtual int SetFilter(FILTER_WHEEL FilterWheel, int FilterNum) { return erMETHOD_NOT_IMPLEMENTED; }
   // virtual int GetFilter(int& FilterNum) { return erMETHOD_NOT_IMPLEMENTED; }
  //  virtual int GetFilter(FILTER_WHEEL& FilterWheel, int &FilterNum) { return erMETHOD_NOT_IMPLEMENTED; }
  //  virtual int SetFilterWheel(FILTER_WHEEL FilterWheel) { return erMETHOD_NOT_IMPLEMENTED; }
  //  virtual int GetFilterWheel(FILTER_WHEEL& FilterWheel) { return erMETHOD_NOT_IMPLEMENTED; }

	/////////////////////////////////////////////////////////////////////////////////
    // Shutter Control 0 = Open, 1 = Closed (see shutter define)
	/////////////////////////////////////////////////////////////////////////////////
    virtual int SetShutter(int ShutterPos, int waitmove) { return erMETHOD_NOT_IMPLEMENTED; }
    virtual int GetShutter(int *ShutterPos) { return erMETHOD_NOT_IMPLEMENTED; }

	//BF shutter
    virtual int SetBFShutter(int ShutterPos, int waitmove) { return erMETHOD_NOT_IMPLEMENTED; }
    virtual int GetBFShutter(int *ShutterPos) { return erMETHOD_NOT_IMPLEMENTED; }

	/////////////////////////////////////////////////////////////////////////////////
    // Aperture Diaphragm (Brightfield) - 0% (Closed) to 100% (Open)
	/////////////////////////////////////////////////////////////////////////////////
    virtual int SetAperture(int Aperture, int waitmove) { return erMETHOD_NOT_IMPLEMENTED; }
    virtual int GetAperture(int * Aperture) { return erMETHOD_NOT_IMPLEMENTED; }

 	/////////////////////////////////////////////////////////////////////////////////
    // Field Diaphragm (Brightfield) -  0% (Closed) to 100% (Open)
	/////////////////////////////////////////////////////////////////////////////////
    virtual int SetField(int Field, int waitmove) { return erMETHOD_NOT_IMPLEMENTED; }
    virtual int GetField(int * Field) { return erMETHOD_NOT_IMPLEMENTED; }

	/////////////////////////////////////////////////////////////////////////////////
    // Field Diaphragm (Fluorescent) -  0% (Closed) to 100% (Open)
	/////////////////////////////////////////////////////////////////////////////////
    virtual int SetField_Fl(int Field, int waitmove) { return erMETHOD_NOT_IMPLEMENTED; }
    virtual int GetField_Fl(int * Field) { return erMETHOD_NOT_IMPLEMENTED; }

	/////////////////////////////////////////////////////////////////////////////////
    // Diaphragm  -  maxmum and mimumum settings (default 100 and 0)
	/////////////////////////////////////////////////////////////////////////////////
    virtual int GetMaxValue(int * Value) { *Value = 100;  return erNO_ERRORS; }
    virtual int GetMinValue(int * Value) { *Value = 0; return erNO_ERRORS; }

	/////////////////////////////////////////////////////////////////////////////////
    // Condenser Lens Control - 0=In, 1=Out, 2=Undefined
 	/////////////////////////////////////////////////////////////////////////////////
    virtual int SetCondenser(int CondenserPos, int waitmove) { return erMETHOD_NOT_IMPLEMENTED; }
    virtual int GetCondenser(int *CondenserPos) { return erMETHOD_NOT_IMPLEMENTED; }
    //virtual int SetCondenserPosition(int CondenserPos) { return erMETHOD_NOT_IMPLEMENTED; }
    //virtual int GetCondenserPosition(int *CondenserPos) { return erMETHOD_NOT_IMPLEMENTED; }
    
	/////////////////////////////////////////////////////////////////////////////////
    // Lamp Control - Units % Min/Max 0% = OFF, 100% = ON
 	/////////////////////////////////////////////////////////////////////////////////
    virtual int SetLamp(int LampValue, int waitmove) { return erMETHOD_NOT_IMPLEMENTED; }
    virtual int GetLamp(int *LampValue) { return erMETHOD_NOT_IMPLEMENTED; }

		//Fluorescent lamp
	virtual int GetUnitStatus (int *Status) {return erMETHOD_NOT_IMPLEMENTED;} 
	virtual int GetLampAge (int *Hours) {return erMETHOD_NOT_IMPLEMENTED;}
	virtual int SetExposure (double Exposure) {return erMETHOD_NOT_IMPLEMENTED;}
	virtual int GetExposure (double *Exposure) {return erMETHOD_NOT_IMPLEMENTED;}
	virtual int SwitchLamp (BOOL Setting, int waitmove) {return erMETHOD_NOT_IMPLEMENTED;}
	virtual int ClearAlarm (){return erMETHOD_NOT_IMPLEMENTED;}
	virtual int RunTimed (int waitmove){return erMETHOD_NOT_IMPLEMENTED;}
	virtual int IsLampReady (int *Ready) {return erMETHOD_NOT_IMPLEMENTED;}
	virtual int IsLampOn (int *On) {return erMETHOD_NOT_IMPLEMENTED;}

	virtual int BeginOiling () {return erMETHOD_NOT_IMPLEMENTED;}
	virtual int DispenseOil () {return erMETHOD_NOT_IMPLEMENTED;}
	virtual int EndOiling () {return erMETHOD_NOT_IMPLEMENTED;}

	virtual int SetLocalRemote (int Remote) {return erMETHOD_NOT_IMPLEMENTED;}
	virtual int PreScanCheck () {return erMETHOD_NOT_IMPLEMENTED;}
	virtual int PostScanCheck () {return erMETHOD_NOT_IMPLEMENTED;}
	virtual int GetOptionalMagnification(double* magnification) {return erMETHOD_NOT_IMPLEMENTED;}



			//Axis Control data
	RotorData *m_RotorData;
	DriveData *m_DriveData;
	int m_MotorStatus;						//Record the status of the motor 
	int	m_CurrentSetting;					//Record the current setting of the motor
	int m_WorkingAxisID;					//Inform Genetix controller which axis to operate

protected:
	//int ThisAxisID;				//Indentification code for the axis controlled by the MotorController object
	int MaxFilters;				//Maximum number of filters on wheel
    int MaxObjectives;		// Maximum number of objective lenses on nosepeices

			   // Limits of movement. Set either by software or read from the microscope on initialisation
    int AxisMaxDrive;                  // Absolute Maximum  Drive Limit
	int AxisMaxLimit;					// Software/Microscope Set Max Position Limit 
	int AxisMinLimit;					// Software/Microscope Set Min  PositionLimit
	int AxisOffset;							// For Conversion of Microscope Pos To 0 Based Pos In Microscope Steps
	int AxisDatum;						// Absolute position of the datum point
		
   // int ErrorCode;								// Error Code of Last Error That Occurred
	char ParameterFileName[255];	// Locally Stores The Parameter File Name

//	CCommPort* pSerial;				 // The Comms Port Object

	char motor_debug[256];
	//FILE *mFile;
};

typedef CMotorController * (__cdecl * INITCONTROLLERPROC)(void);
typedef void (__cdecl * DESTROYCONTROLLERPROC)(CMotorController *);

#endif
