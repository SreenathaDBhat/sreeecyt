/*
 * MultiSelectTreeCtrl.h
 */

#pragma once


class CMultiSelectTreeCtrl : public CTreeCtrl
{
public:
	enum SelectionState { selectionNone, selectionSome, selectionAll };
	void SetIcons(HICON hIconNone, HICON hIconSome, HICON hIconAll);
	SelectionState GetSelectionState(HTREEITEM hItem);
	BOOL SetSelectionState(HTREEITEM hItem, SelectionState state);

protected:
	CImageList imlist;
	void SelectDescendants(HTREEITEM hItem, SelectionState state);
	void SelectAncestors(HTREEITEM hItem, SelectionState state);

	DECLARE_MESSAGE_MAP()
	// Message map functions
	afx_msg BOOL OnClick(NMHDR* pNMHDR, LRESULT* pResult);
};
