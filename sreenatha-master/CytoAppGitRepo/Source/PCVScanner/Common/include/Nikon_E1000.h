// Nikon_E1000.h : main header file for the Nikon_E1000 DLL
//

#pragma once

#include "MotorController.h"
#include "Axes.h"

#include "MicroErrors.h"
#include "AutoScope.h"
#include "CommPort.h"

#ifdef NIKON_E1000_EXPORTS
#define NIKON_E1000MOTORCONTROLLER_API __declspec(dllexport)
#else
#define NIKON_E1000MOTORCONTROLLER_API __declspec(dllimport)
#endif

#define ASCHEX2INT(x, y)  (x>='0' && x<='9') ? y = x-'0' : y = x-'A'+10;

//void CALLBACK Nikon_E1000speedCallback(  HWND hwnd, UINT uMsg, UINT idEvent,  DWORD dwTime); 

class CNIKON_E1000MotorController : public CMotorController
{
public:
	CNIKON_E1000MotorController();
	~CNIKON_E1000MotorController();

	int SetUp (int AxisID, int iPortAddr, AxisData *AxisData);
	int ShutDown();
	int MicInitialise (int PortAddr);
	int ReadInquiry(const char *Question, char *Answer);
	int AutoDetect();
	int GetCurrentElement(int *Position);
	int SendCommand(const char *Command);
	int MoveToElement (int PartNum, int waitmove);
	int GetCommandResponse(char *Response, long TimeOutPeriod);
	int TranslateError(const char *ErrorReply);
	int GetAbsPosition (float *Pos);
	BOOL Busy();
	
	int GetHexParameter(const char *ParamStr, int& Value);
	int GetLamp(int *LampValue);
	int SetLamp(int LampValue, int waitmove);
	int LampValue;
	int GetParameter(const char *ParamStr, int& Value);
	int MoveFocusAbs(float FocusPos);
	int MoveFocusAbs(int FocusPos);
	int GetFocusAbs(int& FocusPos);
	int SetFocusMax(int MaxFocusPos);
	int GetAbsPosition (int *Pos);
	int MoveBy (int Offset);
	int MoveBy (float Offset);
	int MoveFocusRel(float Offset);
	int MoveTo(int Pos);
	int MoveTo(float Pos);
	int SetSpeed(int Speed);
	int SetSpeed(float speed, int *pStepsPerSecond);
	int GetPosition(int *pPos);
	int GetPosition(float *pPos);
	int ResetStage();
	int DatumFound();
	UINT m_SpeedTimer;
	int l_speed;
	//BOOL SpeedVal;
	//void MoveToFocusLimit(int Speed, BOOL val);

    // Limits of Movement Set Either By Software Or Read From Microscope on Initialise()
    int MaxZDrive;                  // Absolute Maximum Z Drive Limit
	int MaxZLimit;					// Software/Microscope Set Max Focus Limit 
	int MinZLimit;					// Software/Microscope Set Min Focus Limit
	int ZOffset;					// For Conversion of Microscope Pos To 0 Based Pos In Microscope Steps
	int ZDatum;						// Absolute position of the datum point
    int ZStageStatus;               // Current Status Of The Stage Z Axis


	//FILTERS
	int GetFilter(int& FilterNum);
	int SetFilter(FILTER_WHEEL FilterWheel, int FilterNum);
	int SetFilter(int FilterNum);

	//Diafragm
	int GetField(int* Field);
	int SetField(int Field, int waitmove);
	int NikonFldValue;

	//SHUTTER
	int SetShutter(int ShutterPos, int waitmove);
	int GetShutter(int* ShutterPos);
	int ShutterValue;

	//void UseDefaults();
		BOOL m_Open;
		CCommPort *pSerial;            // The Comms Port Object

	int Home();

private :
		int m_AxisID;		//ID of axis 
		void *pNIKON_E1000;		//pointer to BX61 portal
		int ErrorCode;
		int ThisAxisID;				//Indentification code for the axis controlled by the MotorController object
		//SerialPort *pSerial;

protected :
	
};

	//These 2 functions ONLY are exported by dll
extern "C" NIKON_E1000MOTORCONTROLLER_API CMotorController *CreateNIKON_E1000MotorController(void);
extern "C" NIKON_E1000MOTORCONTROLLER_API void  DestroyNIKON_E1000MotorController(CMotorController *device);
