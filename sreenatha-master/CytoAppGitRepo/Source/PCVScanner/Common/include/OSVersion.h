#pragma once


#ifdef CAMMANAGED
#undef HELPER3_API
#define HELPER3_API
#elif defined(HELPER3_EXPORTS)
#define HELPER3_API __declspec(dllexport)
#else
#define HELPER3_API __declspec(dllimport)
#endif

BOOL HELPER3_API OSVersionNum(DWORD *Version);