#pragma once

#include "MicroscopeDialog.h"
#include "resource.h"

// COilDispenser dialog

#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

class MICROSCOPEGUI_API COilDispenser : public MicroscopeDialog
{
public:
	COilDispenser(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag);
	virtual ~COilDispenser();

// Dialog Data
	//enum { IDD = IDD_OILERBASE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL Create(CWnd* pParentWnd);
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBeginoiling();
	afx_msg void OnBnClickedDispenseoil();
	afx_msg void OnBnClickedEndoiling();
	afx_msg void OnDestroy();
};
