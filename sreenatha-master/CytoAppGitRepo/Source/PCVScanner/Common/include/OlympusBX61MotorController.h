/*
 *		O L Y M P U S  B X 6 1 M O T O R C O N T R O L L E R . H
 *			BX61 specific motor control
 */

#ifndef _BX61CONTROL_H
#define _BX61CONTROL_H

#include "MotorController.h"
#include "Axes.h"

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the BX61MOTORCONTROLLER_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// IMSMOTORCONTROLLER_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef BX61MOTORCONTROLLER_EXPORTS
#define BX61MOTORCONTROLLER_API __declspec(dllexport)
#else
#define BX61MOTORCONTROLLER_API __declspec(dllimport)
#endif

// This class is exported from the IMScontrol.dll
class CBX61MotorController : public CMotorController
 {
public:
		CBX61MotorController(void);
		~CBX61MotorController();

				//General
		int Open (int PortNo);
		int Close();
		int SetUp (int AxisID, int iPortAddr, AxisData *AxisData);
		int Busy();
		int Wait();
		int Stop();
		int Status();

	// Methods
	/////////////////////////////////////////////////////////////////////////////
	// General Microscope
	/////////////////////////////////////////////////////////////////////////////
	// Autodetected Parameters Found Here
	int AutoDetect();
	// Initialise Function Initialise Any Attributes Associated With
	// A Specific Microscope And Also Common Attributes Such As The
	// Number of Filters, Objective Nosepeices etc.
    int MicInitialise(int CommsInterface);
	// Shut It Down
	int ShutDown();
	// Checkname or Identify ?
	int Identify(char *Name);
    // Remote/Local control
	int SetControlState (int state);
 
    /////////////////////////////////////////////////////////////////////////////
	// X, Y, Z  Drive Functions
	// Driver units 0.1 micron, Olympus BX61 units 1 pico-meter
	/////////////////////////////////////////////////////////////////////////////
	// Absolute Move Functions
	int MoveTo (int Pos);				
	int MoveTo (float Pos);					
	int GetAbsPosition (int *Pos);		
	int GetAbsPosition (float *Pos);
	int GetPosition (int *Pos);
	int GetPosition (float *Pos);
	// Set and Get Max and Min Limits of Travel
	int SetMax (int MaxPos);		//SetFocusMax(int MaxFocusPos);
	int GetMax (int &MaxPos);		//GetFocusMax(int& MaxFocusPos);
	int SetMin (int MinPos);			//SetFocusMin(int MinFocusPos);
	int GetMin (int& MinPos);		//GetFocusMin(int& MinFocusPos); 
    // Relative Movement
	int MoveBy (int Offset);			//MoveFocusRel(int FocusOffset);
	int MoveBy (float Offset);

	/*int SetFocusRel(int FocusOffset);*/
	int GetRel (int &Offset);		//GetFocusRel(int& FocusOffset);
	
	int ResetStage();
	int DatumFound();
	int SetSpeed (int Speed);
	int SetSpeed (float Speed, int *StepsPerSecond);
	int SetZDriveSpeed(double StartSpeed, double Speed);

	/////////////////////////////////////////////////////////////////////////////
    // Filter Block Commands
	// Set or Get A Filter In The Block
	/////////////////////////////////////////////////////////////////////////////
	int InitRotor (int PortNo, AxisData *Data);
	int MoveToElement(int PartNum, int waitmove);
	int WheelWait();
	int Home();
	
    int GetCurrentElement(int *Position);

	// Shutter Control
	int SetShutter(int ShutterPos, int waitmove);
	int GetShutter(int *ShutterPos);
	
	/////////////////////////////////////////////////////////////////////////////
	// Aperture Diaphragm 0% = Closed, 100% = Fully Open
	/////////////////////////////////////////////////////////////////////////////
	int SetAperture(int Aperture, int waitmove);
	int GetAperture(int *Aperture);

	/////////////////////////////////////////////////////////////////////////////
	// Field Diaphragm 0% = Closed, 100% = Fully Open
    // N/A For BX61 Manual Operation
	/////////////////////////////////////////////////////////////////////////////
	//int SetField(int Field);
	//int GetField(int& Field);

	/////////////////////////////////////////////////////////////////////////////
	// Condenser Control
	/////////////////////////////////////////////////////////////////////////////
	int SetCondenser(int CondenserPos, int waitmove);
	int GetCondenser(int *CondenserPos);
    //int SetCondenserPosition(int CondenserPos);
    //int GetCondenserPosition(int *CondenserPos);

	/////////////////////////////////////////////////////////////////////////////
    // Lamp Control Driver 0% to 100% Olympus 0, 2.5V to 12.0V
	/////////////////////////////////////////////////////////////////////////////
    int SetLamp(int LampValue, int waitmove);
    int GetLamp(int * LampValue);

	int SetLocalRemote(int Remote);

#endif

protected:

private :
		int m_AxisID;		//ID of axis 
		void *pBX61;		//pointer to BX61 portal
		int ErrorCode;
		int ThisAxisID;				//Indentification code for the axis controlled by the MotorController object
};

	//These 2 functions ONLY are exported by dll
extern "C" BX61MOTORCONTROLLER_API CMotorController *CreateBX61MotorController(void);
extern "C" BX61MOTORCONTROLLER_API void  DestroyBX61MotorController(CMotorController *device);

