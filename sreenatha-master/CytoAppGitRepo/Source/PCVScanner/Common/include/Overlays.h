//=================================================================================
// Overlays.h
// Some definitions for overlay masks
//
// History:
//
// 29Nov00	AR
//=================================================================================
#ifndef __OVERLAYS_H_
#define __OVERLAYS_H_

#define OVERLAY_CROSS	        0x01
#define OVERLAY_CIRCLE	        0x02
#define OVERLAY_MEASUREMENTS    0x04
#define OVERLAY_CROSS_ALIGNED   0x10
#define OVERLAY_FOCUSSING       0x20
#define OVERLAY_MOVE					0x40


#define OVERLAY_ALL OVERLAY_CROSS | OVERLAY_CIRCLE | OVERLAY_MEASUREMENTS


#endif