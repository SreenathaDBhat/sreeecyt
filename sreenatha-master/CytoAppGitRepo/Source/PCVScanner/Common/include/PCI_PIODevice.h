/* 
 *			P C I _ P I O D E V I C E . H
 *
 *			Author			A Rourke
 *			Date			20th Novemeber 2001
 *			Copyright	Applied Imaging International
 *
 */


#ifndef  _PCI_PIODEVICE_H
#define _PCI_PIODEVICE_H


// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the PCI_PIODEVICE_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// PCI_PIODEVICE_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef PCI_PIODEVICE_EXPORTS 
#define PCI_PIODEVICE_API __declspec(dllexport)
#else
#define PCI_PIODEVICE_API __declspec(dllimport)
#endif

#include "MicroErrors.h"
#include <winioctl.h>
#include <time.h>
#include<stdio.h>
#include<conio.h>
#include <string.h>
#include <bluechip.h>

#define IO_DEVICE_APP	1

BOOL PCI_PIODEVICE_API IODevice_Open (HWND parentWnd );
void PCI_PIODEVICE_API IODevice_Close ();

int IODevice_Initialise();
void IODevice_Uninitialise();
void IOMonitor();

#endif