#if !defined(AFX_PASSSUMMARY_H__15D8E813_441F_424E_AC33_234F88773E55__INCLUDED_)
#define AFX_PASSSUMMARY_H__15D8E813_441F_424E_AC33_234F88773E55__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PassSummary.h : header file
//
#define IDD_PASS_SUMMARY_DIALOG 24008


class CAssay;
class CScanPassList;
class CScanPass;

/////////////////////////////////////////////////////////////////////////////
// PassControl window

class PassControl : public CWnd
{

friend class CPassSummary;

protected:

	CButton CompletedCheckBox;
	CStatic ScriptCtrl;

	//COLORREF colour;

	CScanPass* m_pScanPass;

// Construction
public:
	PassControl();

// Attributes
public:


// Operations
public:

	BOOL Create(CScanPass* pScanPass, const RECT& rect, CWnd* pParentWnd, UINT nID);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PassControl)
	protected:
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~PassControl();

	// Generated message map functions
protected:
	//{{AFX_MSG(PassControl)
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};





/////////////////////////////////////////////////////////////////////////////
// CPassSummary dialog

class CPassSummary : public CDialog
{
// Construction
public:
	CPassSummary(CWnd* pParent = NULL);   // standard constructor
	void Edit(CAssay *assay);
	PassControl *pPassCtrls;
	int numPassCtrls;

// Dialog Data
	//{{AFX_DATA(CPassSummary)
	enum { IDD = IDD_PASS_SUMMARY_DIALOG };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPassSummary)
	public:
	virtual BOOL Create(const RECT& rect, CWnd* pParentWnd);
	//}}AFX_VIRTUAL

// Implementation
protected:
	CWnd*                   m_pParentWnd;
	RECT                    m_rect;

	// Generated message map functions
	//{{AFX_MSG(CPassSummary)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


private:

	CAssay* m_assay;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PASSSUMMARY_H__15D8E813_441F_424E_AC33_234F88773E55__INCLUDED_)
