////////////////////////////////////////////////////////////////////////////////
//
//	PassVisibility.h
//

#pragma once

// Pass name to number mapping and visibility setting
class CPassVisibility {
public:
	CString m_name;	// The name of the pass
	int m_passno;	// The pass number with this name
	bool m_visible;	// If no frames, not visible (includes "Focus only" pass)

	CPassVisibility()
	{
		m_name.Empty();
		m_passno = 0;
		m_visible = true;
	}

	// Equality assignment
	CPassVisibility &operator = (const CPassVisibility &rhs)
	{
		// Check for self assignment
		if (this != &rhs) {
			m_name = rhs.m_name;
			m_passno = rhs.m_passno;
			m_visible = rhs.m_visible;
		}

		// Return ref to current object for a=b=c assignments
		return *this;
	}
};

