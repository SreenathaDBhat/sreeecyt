
#pragma once


#ifdef CAMMANAGED
#define HELPER3_API
#elif defined(HELPER3_EXPORTS)
#define HELPER3_API __declspec(dllexport)
#else
#define HELPER3_API __declspec(dllimport)
#endif

class HELPER3_API CCPUCount
{
public:
	CCPUCount();
	~CCPUCount() {};

	BOOL  Count();
	DWORD PhysicalProcessors(void) { return processorCoreCount; }
	DWORD LogicalProcessors(void)  { return logicalProcessorCount; }

private:
	DWORD numaNodeCount;
    DWORD processorCoreCount;
    DWORD logicalProcessorCount;
    DWORD processorL1CacheCount;
    DWORD processorL2CacheCount;
    DWORD processorL3CacheCount;
    DWORD processorPackageCount;

};
