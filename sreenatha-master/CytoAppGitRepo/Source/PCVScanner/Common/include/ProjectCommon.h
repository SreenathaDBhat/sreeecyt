////////////////////////////////////////////////////////////////////////////////////////////////////////////
// THIS HEADER SHOULD CONTAIN NOTHING BUT DEFS COMMON TO ALL PROJECTS
// either include in the stdafx.h or as the FIRST include in any project source file. 
////////////////////////////////////////////////////////////////////////////////////////////////////////////


#pragma once

#pragma warning(disable:4945)

#ifndef _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_DEPRECATE
#endif

#ifndef WINVER
#define WINVER      0x501
#endif
