/*
 *		P R O S C A N M O T O R C O N T R O L L E R . H
 *			proscan specific motor control
 */

#ifndef _PROSCANCONTROL_H
#define _PROSCANCONTROL_H

#include "MotorController.h"
#include "Axes.h"
#include "imsparams.h"

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the PROSCANMOTORCONTROLLER_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// PROSCANMOTORCONTROLLER_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef PROSCANMOTORCONTROLLER_EXPORTS
#define PROSCANMOTORCONTROLLER_API __declspec(dllexport)
#else
#define PROSCANMOTORCONTROLLER_API __declspec(dllimport)
#endif



class CProscanMotorController : public CMotorController
 {
public:
		CProscanMotorController();
		~CProscanMotorController();

					//general control functions
		int SetUp(int AxisID, int iPortAddr, AxisData *AxisControl);
		int ShutDown();
		void GetControllerValues();
		int SetControlState( int state);
		int Identify (char *Name);
		int AutoDetect();
		int Status();
		int Busy();
		int Wait();
		int Stop();

				//joystick/button control state
		int JogControl (int state, char Axis);
		int ProgramControl (int state, char Axis);

					//stage (x,y,z) control functions
		int MoveTo (int pos);
		int MoveBy (int dist);
		int MoveTo (float pos);
		int MoveBy (float dist);
		int SetPosition (int pos);
		int GetPosition (int *pos);
		int SetPosition (float pos);
		int GetPosition (float *pos);
		int GetAbsPosition (int *pos);
		int GetAbsPosition (float *pos);
		int SetSpeed (int speed);
		int SetSpeed (float speed, int *StepsPerSecond);
		int ResetStage (); 
		int DatumFound();

					//filterwheel control functions
		int InitRotor (int PortNo, AxisData *Data);
		int Home();
		int Origin();
		int MoveToElement (int PartNo, int waitmove);
		int WheelWait();
		int GetCurrentElement (int *Position);

private :
		int stat;
		BOOL m_Open;
		char  m_Name;
		int m_Mode;
		HANDLE	fileHandle;
		HANDLE DatumThread;
		DWORD DatumThreadID;
		double travel;
		double destination;
		int m_SpeedIncrease;
		int m_MaxSpeed;
		IMSParams *motor_parameters;
		BOOL m_bControlState;
		BOOL m_bSetup;
		int ErrorCode;
		int ThisAxisID;
};

	//These 2 functions ONLY are exported by dll
extern "C" PROSCANMOTORCONTROLLER_API CMotorController *CreateProscanMotorController(void);
extern "C" PROSCANMOTORCONTROLLER_API void  DestroyProscanMotorController(CMotorController *device);


#endif