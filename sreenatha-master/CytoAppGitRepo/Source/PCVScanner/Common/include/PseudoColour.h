#pragma once


#ifdef HELPER2_EXPORTS
#define PSEUDOCOLOUR_API __declspec(dllexport)
#else
#define PSEUDOCOLOUR_API __declspec(dllimport)
#endif


void PSEUDOCOLOUR_API PseudoColour(BYTE GreyLevel, BYTE *R, BYTE *G, BYTE *B);

