#ifndef QUANTMEASURE_H
#define QUANTMEASURE_H

#ifdef CAMMANAGED
	#define QMMEASURE_API
#elif defined(HELPER2_EXPORTS)
	#define QMMEASURE_API __declspec(dllexport)
#else
	#define QMMEASURE_API __declspec(dllimport)
#endif

void QMMEASURE_API ImageQuantiles16(const WORD *pBufPtr, int ngreys, double PCBelow, double PCAbove,
									int W, int H, int L, int T, int R, int B,
									WORD *pLo, WORD *pHi, const BYTE * pMaskImage = NULL);

void QMMEASURE_API FastImageQuantiles16(const WORD *pSrcImage, int ngreys, double PCBelow, double PCAbove, int W, int H, WORD *pLo, WORD *pHi);


void QMMEASURE_API ImageQuantiles8(const BYTE *pBufPtr, double PCQLo, double PCQHi,
								   int W, int H, int L, int T, int R, int B,
								   BYTE *pLo, BYTE *pHi);

void QMMEASURE_API ImageMinMax16(const WORD *pBufPtr, int s, int ngreys, WORD *pLo, WORD *pHi);

int QMMEASURE_API MomentThreshold(BYTE *pImage, int W, int H);			// Calc threshold based on moment preservation

int QMMEASURE_API EntropyThreshold(BYTE *pImage, int W, int H);

#endif
