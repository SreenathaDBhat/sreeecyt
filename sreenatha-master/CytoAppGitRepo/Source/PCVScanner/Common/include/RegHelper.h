#if !defined(AFX_REGISTRY_H__9FEB6B24_190F_4257_BCBA_FA6108566040__INCLUDED_)
#define AFX_REGISTRY_H__9FEB6B24_190F_4257_BCBA_FA6108566040__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Registry.h : header file
//
#ifdef CAMMANAGED
#undef HELPER3_API
#define HELPER3_API
#elif defined(HELPER3_EXPORTS)
#define HELPER3_API __declspec(dllexport)
#else
#define HELPER3_API __declspec(dllimport)
#endif

/////////////////////////////////////////////////////////////////////////////
// CRegistry window

class HELPER3_API CRegistry
{
// Construction
public:
	CRegistry();
	static CString GetRegItem(LPCTSTR key, LPCTSTR subkey);
	static CString GetCVRegItem(LPCTSTR key, LPCTSTR subkey);
	static int CRegistry::GetRegItem(LPCTSTR key, LPCTSTR subkey, int default_value);
	static void SetRegItem(LPCTSTR key, LPCTSTR subkey, LPCTSTR value);
	static void SetCVRegItem(LPCTSTR key, LPCTSTR subkey, LPCTSTR value);
	//void GetRegDllItems();
	static BOOL SetPermissions();
// Attributes
public:

// Operations
public:

private:
	static CString GetRegistryItem(LPCTSTR AIkey, LPCTSTR key, LPCTSTR subkey);
	static void SetRegistryItem(LPCTSTR AIkey, LPCTSTR key, LPCTSTR subkey, LPCTSTR value);

// Overrides

// Implementation
public:
	virtual ~CRegistry();

	// Generated message map functions
protected:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGISTRY_H__9FEB6B24_190F_4257_BCBA_FA6108566040__INCLUDED_)
