#pragma once

// Private DLL export scheme (to RegionTools Module)
// This is instead of using AFX_EXT_CLASS - which I don't think
// will work if THIS lib is used w/in another DLL
// Export definition
#ifdef REGIONTOOLS_EXPORTS
#define REGION_API __declspec(dllexport)
#else
#define REGION_API __declspec(dllimport)
#endif

// Default sizes for objective magnifications
// Half widths in ideal units
#define REGION_DEFAULT_40X		250
#define REGION_DEFAULT_20X		600
#define REGION_DEFAULT_10X		1200
#define REGION_DEFAULT_5X		2250
#define REGION_DEFAULT_1_25X	9000


// CRegionDefaults dialog

class REGION_API CRegionDefaults : public CPropertyPage
{
	DECLARE_DYNAMIC(CRegionDefaults)

public:
	CRegionDefaults(CWnd* pParent = NULL);   // standard constructor
	virtual ~CRegionDefaults();

// Dialog Data

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

	BOOL OnInitDialog();
	void OnOK();

public:
	int m_RegionSize;
	int m_RegionSizeSmall;	// region size for small selection
	int m_RegionSizeMedium;	// region size for medium selection
	int m_RegionSizeLarge;	// Region size for large selection
	int m_RegionSizeVLarge;	// Region size for Very Large selection

	afx_msg void OnRegionSize();
	afx_msg void OnUpdateRegion(CCmdUI *pCmdUI);

	int GetRegionSizeFromID(int region_size_id);
	int GetCurrentRegionSize(int *region_size_id);
	void SetRegionSize(int region_size_id);
	int GetMarkerSize();
	COLORREF GetMarkerColour();

	// Size and colour of magic marker pen
	int m_MarkerSize;
	COLORREF m_MarkerColour;

	afx_msg void OnBnClickedRegionsizeDefaultsButton();

private:
	int GetRegistryValueOrDefault(char *name);	// Get value from registry, or return default
	int GetDefaultValue(CString name);			// Get default value given name
	CBrush *m_brush;

public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnStnClickedMarkerColour();
	afx_msg void OnStnClickedMarkerColourStatic();
};
