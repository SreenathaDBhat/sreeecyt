#pragma once
////////////////////////////////////////////////////////////////////////////////
//
//	RegionTools.h -- Header for Region tools support
//
//	Dave Burgess, Applied Imaging, August 2003
//

// Private DLL export scheme (to RegionTools Module)
// This is instead of using AFX_EXT_CLASS - which I don't think
// will work if THIS lib is used w/in another DLL
// Export definition
#ifdef REGIONTOOLS_EXPORTS
#define REGION_API __declspec(dllexport)
#else
#define REGION_API __declspec(dllimport)
#endif


class Region;
class FrameGroup;
class AI_Tiff;
class CScriptDirective;
class CAssay;
class CItemInfo;
class CScanPassList;
class CReviewDoc;
class CRegionList;
class FrameGroup;

// CRegionList CList definition
typedef CList <CRegionList*, CRegionList*> CListOfRegionLists;

// Undo list class
class CUndoList {
public:
	CUndoList(void);
	~CUndoList(void);

	// Test if there is anything to undo
	bool CanUndo();

	// Lists of CRegionLists - one for input, one for output
	CListOfRegionLists m_input_lists;
	CListOfRegionLists m_output_lists;

	CString m_description;	// Description of last action

protected:
	// Position in the list - not currently used
	POSITION m_pos_input;
	POSITION m_pos_output;
};

// Max number of find (process) threads to run at once
#define MAX_THREADS	4

// Tool functional class
class REGION_API CRegionTools : public CCmdTarget
{

public:
	CRegionTools(void);
	~CRegionTools(void);
	void Initialise(double xscale, double yscale, double zoom_scale);

	// Functional tools
	// Support state
	void SetStatus(BOOL status);
	BOOL GetStatus();
	BOOL Busy();
	BOOL AutoProcess();
	void SetDrawMode(int mode);
	int GetDrawMode(UINT flags);
	void InvalidateHighlight();	// Invalidate last highlighted region

	// Data modification
	bool IsModified();		// Test modification status
	void ClearModified();	// Clear the modification status

	// Draw access control
	bool GetDrawAccess(int timeout);
	bool ReleaseDrawAccess();

	// Mouse move and button functions
	BOOL MoveDraw(  CView *view, CPoint point, BOOL hasBlackBackground);
	void CancelDraw(CView *view);
	BOOL LButtonUp(CView *view, CPoint point);
	BOOL LButtonDblClk(CView *view, CPoint point, UINT flags);
	BOOL LButtonDown(CView *view, double zoom_scale,
					 double xscale, double yscale, CPoint point, UINT flags);
	void MouseLeave(CView *view, FrameGroup *pFrameGroup);	// Mouse leaves view
	Region *GetMoveRegion();		// Return region being moved (or NULL if not moving a region)
	Region *GetOrigRegion();		// Return original region (before move)
	void ClearMoveRegion();			// Clear the move regiomn pointers
	void BoxSelectRegions(CView *view, CPoint p);

	// Drawing functions
	void DrawRegions(double scale, CDC *pDC);
	void DrawCircle(CRect rect, double scale, CDC *pDC);			// Draw circle (rect is bounding box)
	void DrawRect(CRect rect, double scale, CDC *pDC);				// Draw rectangle (rect is bounding box)
	void DrawLine(CPoint p1, CPoint p2, double scale, CDC *pDC);	// Draw line from p1 to p2
	void DrawMarker(CPoint p1, CPoint p2, double scale, CDC *pDC, BOOL hasBlackBackground);	// Draw marker line from p1 to p2

	// Image functions
	AI_Tiff *GetRegionImage(Region *r, FrameGroup *pFrameGroup, double resolution = 0.0, Region *r_withmask = NULL, double xoffset = 0.0, double yoffset = 0.0);
	AI_Tiff *GetFrameImage(Region *r, FrameGroup *pFrameGroup, double resolution = 0.0);
	BYTE *GetMaskImage(CDC *pDC, double left, double top, int width, int height, Region *r,
						bool b_use_vertex = false, double zoomscale = 1.0);
	AI_Tiff *GetRegionFrame(Region *r, FrameGroup *pFrameGroup,
							int overlap, double resolution, bool alwaysload);

	CBitmap *GetRegionBitmap(CView *view, Region *r, FrameGroup *pFrameGroup);

	void ConvertMarkerToRegion();
	
	// Convert mask to a region definition (Actually to point list)
	void MaskToRegion(long region_mask, bool largest_region = true);
	
	// Split a region using the internal vertex list
	CRegionList *SplitRegion(CView *view, Region *split_region);

	// Join regions together
	Region *JoinRegions(CRegionList *rlist);

	// Region list undo management
	bool PushRegionList(int passno, CRegionList *il, CRegionList *ol);	// Push input and output region lists
	bool PopRegionList(int passno, CRegionList **il, CRegionList **ol);	// Pop input and output region lists
	bool CanUndo(int passno);											// Test whether operation can be undone
	void InitUndoList();												// Initialise the undo lists
	void DeleteUndoLists();												// Delete the undo lists
	void SaveActionString(int passno);									// Push description of last action
	CString PeekActionString(int passno);								// Return last action (for user prompt)
	CString GetActionString();											// Descriptive string for current action

	// Member variables

	// Vertex list for arbitrary shape & magic marker drawing
	CList<CPoint*, CPoint*> m_VertexList;

private:
	BOOL m_status;					// Active or not
	BOOL m_busy;					// Tools are doing something that mustn't be interrupted
	int m_draw_mode;				// What shape are we drawing
	int m_shift_mode;				// Shape to use if shift key is pressed
	BOOL m_test;					// GP debugging flag
	bool m_moving;					// Region is being moved
	CPoint m_last_point;			// Last position we were at
	BOOL m_drawing;					// Currently drawing a shape
	BOOL m_button_down;				// Mouse button pressed
	bool m_modified;				// Data modification flag

	double m_xscale;
	double m_yscale;

	HANDLE m_draw_mutex;			// Draw access mutex

public:
	double m_zoom_scale;			// Frameview zoom factor
	int m_xoffset;					// Frameview offset
	int m_yoffset;

	// As an alternative to setting up the values above, it may be simpler
	// just to include reference to frameviewer (FrameViewer m_frameviewer;)

	// Region and size admin
	int m_region_size;				// Size to make region
	int m_region_size_id;			// Pre-defined size ID
	int m_marker_size;				// Crayon/Marker pen size
	COLORREF m_marker_colour;		// Crayon/Marker pen colour
	int m_region_type;				// type
	CRegionList *m_region_list;		// Region list for mouse selection
	CItemInfo *m_blob_item_info;	// Blob info for the assay
	bool m_process_started;			// Region processing is active
	int m_passno;					// Scan pass number

private:
	CRect m_region_box;				// bounding box
	int m_border_width;				// Border to add to region for context view
	CRect m_last_region_box;		// When drawing

	// Last highlighted region and its original state
	Region *m_last_highlight_region;
	Region *m_current_highlight_region; // the region currently under the cursor (for double-click launching region property dlg)
	BOOL m_last_highlight_state;
	BOOL m_select_state;			// What to do with the region
	Region *m_move_region;			// The region being moved
	Region *m_orig_region;			// Original region being moved

	////////////////////////////////////////////////////////////////////////////////
	//
	// The Undo list (input and output regions for every pass)
	// Separate lists are kept for each scan pass
	// The undo list consists of two lists-of-regionlists
	// (All rather complicated I know)
	// Hierarchy:
	// CRegionList - The list of regions
	// CUndoList - Two lists of region lists, one for input, one for output
	// Finally a list of CUndoLists, one for each scan pass
	friend class CUndoList;
	CList <CUndoList*, CUndoList*> m_undo_list;

	// Current assay information for this region
	CAssay *m_assay;

	////////////////////////////////////////////////////////////////////////////////
	//
	// Process list event control
	// Functions and event handle must be publicly accessible
public:
	bool CreateProcessEvent(int event_num);
	bool SetProcessEvent();
	bool ClearProcessEvent();
	bool DeleteProcessEvent();
	HANDLE m_process_event;
};
