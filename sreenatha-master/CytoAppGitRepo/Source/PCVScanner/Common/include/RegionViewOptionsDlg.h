#pragma once

// Export definition
#ifdef REGIONTOOLS_EXPORTS
#define REGION_API __declspec(dllexport)
#else
#define REGION_API __declspec(dllimport)
#endif


// CRegionViewOptionsDlg dialog

class REGION_API CRegionViewOptionsDlg : public CPropertyPage
{
	DECLARE_DYNAMIC(CRegionViewOptionsDlg)

public:
	CRegionViewOptionsDlg();
	virtual ~CRegionViewOptionsDlg();

// Dialog Data
	enum { IDD = IDD_REGIONVIEWOPTIONS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()


public:
	BOOL m_show_zoom;
	virtual void OnOK();
	BOOL OnInitDialog();
	BOOL m_show_borders;
	BOOL m_show_rulers;
	BOOL m_show_axes;
	BOOL m_show_EFAxes;
	BOOL m_all_passes_shown;
	BOOL m_show_fragments;
	BOOL m_use_interpolation;
	BOOL m_show_region_numbers;
	BOOL m_show_report_mark;
	COLORREF m_text_colour;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnStnClickedTextColourStatic();

private:
	CBrush *m_brush;

};
	//options_dlg.m_show_zoom = frameViewer.showZoom;
	//options_dlg.m_show_borders = frameViewer.showBorders;
	//options_dlg.m_show_rulers = frameViewer.showRulers;
	//options_dlg.m_show_axes = frameViewer.showAxes;
	//options_dlg.m_show_EFAxes = frameViewer.showEFAxes ;
	//options_dlg.m_all_passes_shown = frameViewer.AreAllPassesShown();
	//options_dlg.m_showFragments = frameViewer.showFragments;
	//options_dlg.m_useInterpolation = frameViewer.useInterpolation;