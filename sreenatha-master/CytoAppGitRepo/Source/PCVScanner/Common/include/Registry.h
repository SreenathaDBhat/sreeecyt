/*
 *	Registry.h		BP	12/13/96
 *
 *	Routines for saving and loading entries from the
 *	registry.
 *
 */


#ifndef	_REGISTRY_H
#define	_REGISTRY_H

#ifdef CAMMANAGED
#undef HELPER3_API
#define HELPER3_API
#elif defined(HELPER3_EXPORTS)
#define HELPER3_API __declspec(dllexport)
#else
#define HELPER3_API __declspec(dllimport)
#endif

#include <windows.h>


#ifdef	__cplusplus
extern "C" {
#endif

LPTSTR  HELPER3_API  CVRegKeyGet();

int HELPER3_API CVRegMachineDelete(LPCTSTR subkey);
int HELPER3_API CVRegUserDelete(LPCTSTR subkey);

int HELPER3_API CVRegMachineSet(LPCTSTR subkey, LPCTSTR label, LPCTSTR value);
int HELPER3_API CVRegMachineGet(LPCTSTR subkey, LPCTSTR label, LPTSTR  value, int maxlen);
int HELPER3_API CVRegUserSet(LPCTSTR subkey, LPCTSTR label, LPCTSTR value);
int HELPER3_API CVRegUserGet(LPCTSTR subkey, LPCTSTR label, LPTSTR  value, int maxlen);

int HELPER3_API CVRegMachineGetBin(LPCTSTR subkey, LPCTSTR label, void *data, int maxlen);
int HELPER3_API CVRegUserGetBin(LPCTSTR subkey, LPCTSTR label, void *data, int maxlen);
int HELPER3_API CVRegMachineSetBin(LPCTSTR subkey, LPCTSTR label, void *data, int len);
int HELPER3_API CVRegUserSetBin(LPCTSTR subkey, LPCTSTR label, void *data, int len);

int HELPER3_API CVRegMachineSetShort(LPCTSTR subkey, LPCTSTR label, short value);
int HELPER3_API CVRegMachineGetShort(LPCTSTR subkey, LPCTSTR label, short *value);
int HELPER3_API CVRegUserSetShort(LPCTSTR subkey, LPCTSTR label, short value);
int HELPER3_API CVRegUserGetShort(LPCTSTR subkey, LPCTSTR label, short *value);

int HELPER3_API CVRegMachineSetLong(LPCTSTR subkey, LPCTSTR label, long value);
int HELPER3_API CVRegMachineGetLong(LPCTSTR subkey, LPCTSTR label, long *value);
int HELPER3_API CVRegUserSetLong(LPCTSTR subkey, LPCTSTR label, long value);
int HELPER3_API CVRegUserGetLong(LPCTSTR subkey, LPCTSTR label, long *value);

int HELPER3_API CVRegMachineSetDouble(LPCTSTR  subkey, LPCTSTR  label, double value);
int HELPER3_API CVRegMachineGetDouble(LPCTSTR  subkey, LPCTSTR label, double *value);
int HELPER3_API CVRegUserSetDouble(LPCTSTR  subkey, LPCTSTR  label, double value);
int HELPER3_API CVRegUserGetDouble(LPCTSTR  subkey, LPCTSTR  label, double *value);

int HELPER3_API CVRegMachineSetPoint(LPCTSTR subkey, LPCTSTR label, POINT *value);
int HELPER3_API CVRegMachineGetPoint(LPCTSTR subkey, LPCTSTR label, POINT *value);
int HELPER3_API CVRegUserSetPoint(LPCTSTR subkey, LPCTSTR label, POINT *value);
int HELPER3_API CVRegUserGetPoint(LPCTSTR subkey, LPCTSTR label, POINT *value);

int HELPER3_API CVRegMachineSetRect(LPCTSTR subkey, LPCTSTR label, RECT *value);
int HELPER3_API CVRegMachineGetRect(LPCTSTR subkey, LPCTSTR label, RECT *value);
int HELPER3_API CVRegUserSetRect(LPCTSTR subkey, LPCTSTR label, RECT *value);
int HELPER3_API CVRegUserGetRect(LPCTSTR subkey, LPCTSTR label, RECT *value);

BOOL HELPER3_API SetRegistryPermissions(HKEY hKey, LPCTSTR subKeyName, LPTSTR trusteeName);
BOOL HELPER3_API CVRegSetPermissions();

BOOL HELPER3_API CVGetCurrentUser(LPTSTR   name, int len);
BOOL HELPER3_API CVGetCVDirectory(LPTSTR   name, int len);
BOOL HELPER3_API CVGetCVUserDirectory(LPTSTR   name, int len);
BOOL HELPER3_API CVGetCVAllUsersDirectory(LPTSTR   name, int len);
BOOL HELPER3_API CVGetCVAllUsersTempDirectory(LPTSTR   name, int len);
BOOL HELPER3_API CVGetScanClassifierDirectory(LPTSTR  Dir);
BOOL HELPER3_API CVGetAriolConnectionStrings(LPCTSTR sServerName, LPTSTR connect, int len);
BOOL HELPER3_API CVGetCVUserLocalDirectory(LPTSTR path);

#ifdef	__cplusplus
}
#endif

#endif
