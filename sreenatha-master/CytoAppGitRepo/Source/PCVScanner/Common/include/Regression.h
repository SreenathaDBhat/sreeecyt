#pragma once


#ifdef CAMMANAGED
	#define REGRESSION_API
#elif defined(HELPER2_EXPORTS)
	#define REGRESSION_API __declspec(dllexport)
#else
	#define REGRESSION_API __declspec(dllimport)
#endif


class REGRESSION_API Regression
{
public:
	Regression(int O);				// Regression object of order O
	~Regression(void);
	void Solve(void);				// Do the algorithm

	double Coefficient(int Exp);	// Get a particular coefficient
	int XYCount(void);
	void XYAdd(double X, double Y);	// Add some data

private:
	void	GaussSolve(int O);
	void	BuildMatrix(int O);
	void	FinaliseMatrix(int O);

	double	m_SumX[50];
	double	m_SumYX[50];

	double	m_M[50][50];
	double	m_C[50];				// Coefficients

	int		m_GlobalO;				// Order of the polynomial
	bool	m_Finished;				// Finished
};
