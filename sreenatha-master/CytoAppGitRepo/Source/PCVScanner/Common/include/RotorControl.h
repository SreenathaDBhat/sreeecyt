#if !defined(AFX_ROTORCONTROL_H__CB45B6AC_5CD1_4DE9_9027_375E1DC6A589__INCLUDED_)
#define AFX_ROTORCONTROL_H__CB45B6AC_5CD1_4DE9_9027_375E1DC6A589__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RotorControl.h : header file
//
#include "MicroscopeDialog.h"


/////////////////////////////////////////////////////////////////////////////
// CRotorControl dialog

#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

class MICROSCOPEGUI_API  CRotorControl : public MicroscopeDialog
{
// Construction
public:
	CRotorControl(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag);

	LRESULT HandleMessage (WPARAM MsgCode, LPARAM MsgData);

// Dialog Data
	//{{AFX_DATA(CRotorControl)
	//enum { IDD = IDD_ROTORBASE };
	CStatic	m_Pic;
	CListCtrl	m_ElementsList;
	BOOL	m_bApplyOffset;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRotorControl)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRotorControl)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnInitrotor();
	afx_msg void OnHomerotor();
	afx_msg void OnClickRotorElements(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRotorApplyoffset();
	afx_msg void OnDestroy();
	//}}AFX_MSG

	afx_msg LRESULT OnMicMessage(WPARAM, LPARAM);

	DECLARE_MESSAGE_MAP()

private:
	int m_nMaxElements;
	int m_nCurrentPosition;
	void GetRotorInfo();
	void SetItem(int item, int col, LPTSTR data);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTORCONTROL_H__CB45B6AC_5CD1_4DE9_9027_375E1DC6A589__INCLUDED_)
