//=================================================================================
// RubberNand.h
// Provide implementation of a Rubber Band rectangle class. Could use CTrackRect
// but don't particulary want to link to MFC.
//
// History:
//
// 29Feb00	WH:	original
//=================================================================================
#ifndef __RUBBERBAND_H_
#define __RUBBERBAND_H_

class CRubberBand
{
public:
	CRubberBand() {};
	~CRubberBand() {};

	RECT &GetROI() { return m_roi;};
	BOOL DoDraw(HWND hWnd);

private:
	RECT m_roi;
	HWND m_hWnd;
	INT m_x, m_y;

	void StartDraw(INT x, INT y);
	void StopDraw();
	void Draw(INT x, INT y);
	void _Rectangle(HDC dc, RECT *r);
};


#endif