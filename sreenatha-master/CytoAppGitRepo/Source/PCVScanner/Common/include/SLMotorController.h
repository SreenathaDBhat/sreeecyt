/*
 *		S L M O T O R C O N T R O L L E R . H
 *			slide loader specific motor control
 */

#ifndef _SLCONTROL_H
#define _SLCONTROL_H

#include "MotorController.h"
#include "Axes.h"
#define _IMPORTING			//for use of the Invetech slide loader dll
#include "slideldr.h"

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SLMOTORCONTROLLER_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SLMOTORCONTROLLER_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef SLMOTORCONTROLLER_EXPORTS
#define SLMOTORCONTROLLER_API __declspec(dllexport)
#else
#define SLMOTORCONTROLLER_API __declspec(dllimport)
#endif


// This class is exported from the SLMotorControl.dll
class   CSLMotorController : public CMotorController
 {
public:
	SLMOTORCONTROLLER_API CSLMotorController(void);

				//general controls
	int SetUp(int AxisID, int iPortAddr, AxisData *AxisData);
	int ShutDown();
	int Busy();
	int AutoDetect();
	int Stop();
	int Wait();

	int SL_BeginSilent(void);
	int SL_CheckForSlideOnStage(BOOL *bSlidePresent);
	int SL_ClearCurrentFault(void);
	int SL_EndSilent(void);
	int SL_FinishCalibration();
	int SL_GetDLLVersion (long *dllversion);
	int SL_GetFirmwareVersion (long *firmwareversion);
	int SL_GetEventLogFilename (char *fielname);
	int SL_GetHardwareStatus();
	int SL_GetLastFault();
	int SL_GetOccupiedSlidePositions(int *Slides);
	int SL_GetOccupiedTrayPositions(int *Trays);
	int SL_GetSlideBarcode(int iSlidePosition, char **strBarcode);
	int SL_GetSlideBarcodesOnTray(char *astrBarcodes, int max_string_length,int *NBarcodes);
	int SL_GetStatus();
	int SL_GetTrayBarcode(char **strBarcode);
	int SL_GetTrayTampered (int *Trays);
	int SL_Initialise();
	int SL_LoadSlide(int iSlidePosition, BOOL ReadBarcodes);
	int SL_MoveRobotHeadAboveStage();
	int SL_RemoveSlide(void);
	int SL_ScanSlidePositions(void);
	int SL_ScanTrayPositions(void);
	int SL_SelectTray(int iTrayNumber);
	int SL_CurrentTray (int *CurrentTray);

	int SL_SetDebugMode (BOOL bDebugMode);
	int SL_SetHardware (BOOL bPumpOn, BOOL pValveOn);
	int SL_StartLeakTest (int iSlideNumber);

	void SL_TranslateReturnCode(int error_code);

	int SL_CheckSlidePresence (BOOL On);
	int SL_ScanWholeTray ();
	int SL_Mute (BOOL On);

	int SL_GetListOfTraysTampered(int *Trays, int num_trays);

	int SL_NumberOfTrays ();
	int SL_NumberOfSlidesPerTray();

private:
	HINSTANCE m_hDLL;
	int SlideLoaderHandle;
	int TrayNumber;

	int ListOfTraysTampered[NUMBER_OF_TRAYS+1];
	int ErrorCode;
	int ThisAxisID;

	int Open(int iPortAddr);
	int Close();
};

	//These 2 functions ONLY are exported by dll
extern "C" SLMOTORCONTROLLER_API CMotorController *CreateSLMotorController(void);
extern "C" SLMOTORCONTROLLER_API void  DestroySLMotorController(CMotorController *device);

#endif
