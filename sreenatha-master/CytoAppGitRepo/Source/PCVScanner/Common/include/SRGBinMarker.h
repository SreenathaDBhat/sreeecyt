// SRGBinMarker.h: header for the SRGBinMarker class.
//
//////////////////////////////////////////////////////////////////////



#pragma once

// Totally arbitrary but does avoid the Stingray ones of 53526-53546
#define IDS_SRG_BINMARKERTYPE	1000


class SRGBinMarker : public SRGTextPanel
{
public:
	// Default constructor
	SRGBinMarker( int nRefIndex = 0, int nLevel = 0);
	// Destructor
	virtual ~SRGBinMarker(void);

	DECLARE_SERIAL(SRGBinMarker);
	
	/* Serializes this object*/
	virtual void Serialize(CArchive &ar);

// Attributes
public:
	int GetRefIndex( void)
		{ return _nRefIndex; }

	void SetBinExtents( int nStartBin, int nFinishBin)
		{ _nStartBin = nStartBin; _nFinishBin = nFinishBin; }
	void GetBinExtents( int& nStartBin, int& nFinishBin)
		{ nStartBin = _nStartBin; nFinishBin = _nFinishBin; }

	static void ComposeName( int nRefIndex, CString& sName);

// Operations
public:
	/* Draws this object*/
	virtual void Draw(CDC *pDC,CWnd *pCWnd);

// Implementation
protected:
	const static int	_nMarkerGap      = 20;
	const static int	_nInterMarkerGap = 14;
	const static int	_nMarkerHeight   =  6;

	// Reference index to identify this component
	int			_nRefIndex;
	// Number of levels below plot to display this marker
	int			_nLevel;
	// Start bin index
	int			_nStartBin;
	// Finish bin index
	int			_nFinishBin;

	void DrawSolidRect( CDC *pDC, CWnd *pCWnd, LPRECT pMarkerRect, COLORREF color);
	void DrawFrameRect( CDC *pDC, CWnd *pCWnd, LPRECT pMarkerRect, COLORREF color);
	void DrawScoop( CDC *pDC, CWnd *pCWnd, LPRECT pMarkerRect, COLORREF color);
};
