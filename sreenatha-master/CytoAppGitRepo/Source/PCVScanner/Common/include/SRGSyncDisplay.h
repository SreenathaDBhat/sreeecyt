// SRGSyncDisplay.h: header for the SRGSyncDisplay class.
//
//////////////////////////////////////////////////////////////////////



#pragma once

// Totally arbitrary but does avoid the Stingray ones of 53526-53546
#define IDS_SRG_SYNCDISPLAYTYPE	1010


class SRGSyncDisplay :	public SRGraphDisplay
{
public:
	// Default constructor
	SRGSyncDisplay(void);
	// Destructor
	virtual ~SRGSyncDisplay(void);

	DECLARE_SERIAL(SRGSyncDisplay);
	
	/* Serializes this object*/
	virtual void Serialize(CArchive &ar);

// Attributes
public:

// Operations
public:

	virtual void SetSync(BOOL v){m_bSync=v;}
	virtual BOOL GetSync(){return m_bSync;}
	virtual void SetSyncScales(BOOL v){m_bSyncScales=v;}
	virtual BOOL GetSyncScales(){return m_bSyncScales;}

	// SRGraphComponent overrides

	/* Draws this object*/
	virtual void Draw(CDC *pDC,CWnd *pCWnd);

	/* Fills a polygon held in an array of points according to a style*/
	virtual void FillPoly(LPPOINT points,int nCount,SRGraphStyle *style,DWORD StyleOverride);


// Implementation
protected:
	BOOL m_bSyncScales;
	BOOL m_bSync;
	ULONG_PTR           m_gdiplusToken;	// For init and shutdown of GDI+

	void FillPolyEx( LPPOINT points, int nCount, SRGraphStyle *style);

};
