#ifndef __SVMCLASSIFIER_H
#define __SVMCLASSIFIER_H

#ifdef SVMCLASSIFIER_EXPORTS
#define SVMCLASSIFIER_API __declspec(dllexport)
#else
#define SVMCLASSIFIER_API __declspec(dllimport)
#endif
#include "stdafx.h"

#include "svm.h"

class SVMCLASSIFIER_API CSVMPredict
{
public:
    CSVMPredict(LPCTSTR ModelFile);
    ~CSVMPredict();

    int Predict(int NumFeatures, double *Vectors);

	BOOL HasModel(void) { return m_bHasModel; }

private:
	int max_nr_attr;


	int	svmloadmodel(LPCTSTR modelfile);
	void svmdestroymodel();
	char *readline(FILE *input);
	void exit_input_error(int line_num);
	void predict(FILE *input, FILE *output);
	int	 predictfromsinglevector(int nfeatures, double *features);

	TCHAR* line;
	int max_line_len;
	struct svm_node *x;
	svm_model* model;
	int predict_probability;


    BOOL        m_bHasModel;
};


class SVMCLASSIFIER_API CSVMTrain
{
public:
    CSVMTrain(void);
    ~CSVMTrain(void);

    void DefaultParams(struct svm_parameter *pParams);
    
    int Train(LPCTSTR model_file_name, LPCTSTR input_file_name);
    int Train(LPCTSTR model_file_name, LPCTSTR input_file_name, struct svm_parameter *pParams);

private:
    struct svm_parameter m_Parameters;
};


#endif