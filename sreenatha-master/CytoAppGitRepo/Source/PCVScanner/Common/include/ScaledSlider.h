#if !defined(AFX_SCALEDSLIDER_H__209E7143_6299_11D6_9CB1_00065B84100C__INCLUDED_)
#define AFX_SCALEDSLIDER_H__209E7143_6299_11D6_9CB1_00065B84100C__INCLUDED_

/////////////////////////////////////////////////////////////////////////////
// A Scaled slider control class
// Written By Karl Ratcliff
/////////////////////////////////////////////////////////////////////////////

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScaledSlider.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CScaledSlider window

class CScaledSlider : public CSliderCtrl
{
// Construction
public:
	CScaledSlider();
	~CScaledSlider();

// Attributes
public:

// Operations
public:
	void SetScaledRange(int MinVal, int Maxval, int NumSteps);	// Set min and max and also step size
	int	 GetScaledPos(void);									// Return the scaled position - not the position of the slider
	void SetScaledPos(int NewValue);							// Sets the new position in scaled units - not slider units
	int CalcValueScaledToPos(int Value);

	int	 m_nMinVal;							// Min val of slider
	int  m_nMaxVal;							// Max val of slider
	int  m_nSteps;						// Step size for slider increments

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScaledSlider)
	//}}AFX_VIRTUAL

// Implementation
public:

	// Generated message map functions
protected:
	//{{AFX_MSG(CScaledSlider)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCALEDSLIDER_H__209E7143_6299_11D6_9CB1_00065B84100C__INCLUDED_)
