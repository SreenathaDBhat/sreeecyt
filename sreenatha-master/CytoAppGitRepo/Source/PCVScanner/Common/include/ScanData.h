
// Q "commands"
enum {
    QSCRIPTPARAM,                           // Parameter for script but not for upload to database as a frame measurement
	QFRAMEMEAS,
	QIM,
	QNEWSLIDE,
	QSLIDEEND,
	QNEWPASS,
	QPASSEND,
	QNEWREGION,
	QREGIONEND,
	QEND,
	QUPDATE,
	QSCANABORTED,
	QSLIDEABORTED,
	QERROR,
	QSTRING};

class AI_Tiff;

class MicPos
{
public:
	int x;
	int y;
	int z;
};

///////////////////////////////////////////////////////////////
// QScriptParam queues a variant on the find thread queue
// that is a value specifically for the script
///////////////////////////////////////////////////////////////

struct QScriptParam
{
    char PersistentName[_MAX_PATH];     // Persistent variable name
    VARIANT Value;                      // The value
};

struct QString
{
    TCHAR Name[512];     //  name
    TCHAR Value[512];                      // The value
};

struct FrameMeas {
	UINT MeasType;
	UINT MeasDataType;
	double dval;
	long   lval;
	char title[_MAX_PATH];
};

struct TiffData
{
public:
	AI_Tiff*  addr;
};

