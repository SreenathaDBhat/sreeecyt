// ScanPass.h: interface for the CScanPass class.
//
// SN 30May02 Set enum values for backwards compatibility
//
//////////////////////////////////////////////////////////////////////
#include "Assay.h"
#include <afxtempl.h>


#if !defined(AFX_SCANPASS_H__61FBA36E_51F0_4466_B345_13C4CC64B1B9__INCLUDED_)
#define AFX_SCANPASS_H__61FBA36E_51F0_4466_B345_13C4CC64B1B9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSpectraChromeList;
class CSpectraChrome;

class CScriptParams;
class CSerializeXML;
class CParamSet;
class CScriptDirective;

#define SCANPASSVERSION				110

#ifdef ASSAY_EXPORTS
#define ASSAY_API __declspec(dllexport)
#else
#define ASSAY_API __declspec(dllimport)
#endif

class ASSAY_API CScanPass  
{
public:
	CScanPass();
	virtual ~CScanPass();

	
	CString									  m_ClassifierName;
	CString*                                  m_Script;                     // The UTS script for this pass

	CScriptParams*                            m_ScriptParams;
	

	
	BOOL									  m_Completed;					// This scan pass has been completed
    
	CSpectraChromeList*                       m_SpectraChromeList;          // Spectra chrome list to use for scanning

	

	BOOL                                      SaveToPath(CString region_filename, BOOL bXMLFormat = FALSE);
	BOOL                                      LoadFromPath(CString region_filename, BOOL bXMLFormat = FALSE);
	CScanPass*                                duplicate();                  // returns an exact copy of this scan pass
	
	void                                      XMLSerialize(CSerializeXML &ar);
    

	BOOL                                      completed();


	void                                      complete(BOOL comp);
	CString                                   ShortScriptName();
	void                                      buildConfigFromScriptPath(void);
    void                                      RemoveScriptParams(void);
	CString									  GetScriptPath();
	CSpectraChrome							  *GetSpectrachrome(int Index);	// Get the spectrachrome at a zero based index in the spec list

    bool                                      IsFluorescent(void);			// True if the scan pass is fluorescent spectrachromes only
	void                                      ReplaceMachineName();

	// BOOL m_completed has been replaced with int m_ScanStatus
	// so status values must be compatible with BOOL
	enum {UnScanned = FALSE, Scanned = TRUE, Scanning} eScanStatus;

private:
    CParamSet                                 *ParseParamLine(CString &line);
};





#endif // !defined(AFX_SCANPASS_H__61FBA36E_51F0_4466_B345_13C4CC64B1B9__INCLUDED_)
