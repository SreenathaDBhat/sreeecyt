// ScanPassList.h: interface for the CScanPassList class.
//
//////////////////////////////////////////////////////////////////////
#include <afxtempl.h>

#if !defined(AFX_SCANPASSLIST_H__A290700D_1FCC_42CF_B046_4B8D8856E8DF__INCLUDED_)
#define AFX_SCANPASSLIST_H__A290700D_1FCC_42CF_B046_4B8D8856E8DF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CScanPass;
class CSpectraChromeList;
class CSerializeXML;

#ifdef ASSAY_EXPORTS
#define ASSAY_API __declspec(dllexport)
#else
#define ASSAY_API __declspec(dllimport)
#endif

class ASSAY_API CScanPassList  
{
public:
	CScanPassList();
	virtual ~CScanPassList();

	int                                m_version;
	CList <CScanPass*, CScanPass*>     m_list;
	POSITION                           m_currentPos;
	CScanPassList*                     duplicate();                  // returns an exact copy of this scan pass
	BOOL                               complete();
	
	int                                GetPassNumber(CScanPass* thisPass);
	CScanPass *						   GetScanPass(int passno);
	BOOL                               RemovePassFromList(CScanPass* thisPass);
	void                               RemoveAll();
	POSITION                           ReplacePass(POSITION pos, CScanPass* new_pass);

	void                               XMLSerialize(CSerializeXML &ar);
};

#endif // !defined(AFX_SCANPASSLIST_H__A290700D_1FCC_42CF_B046_4B8D8856E8DF__INCLUDED_)
