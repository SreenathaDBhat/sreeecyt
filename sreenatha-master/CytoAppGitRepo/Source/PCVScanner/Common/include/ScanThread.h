#if !defined(AFX_SCANTHREAD_H__1F5848ED_424F_4523_8129_A53B161DFE6E__INCLUDED_)
#define AFX_SCANTHREAD_H__1F5848ED_424F_4523_8129_A53B161DFE6E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScanThread.h : header file
//

#include "MicServer.h"
#include "..\Grabber2\GrabServer2\_GrabServer2.h"
#include "Region.h"
#include "dbcore.h"
#include "CommonDefs.H"

#ifdef SCANTHREAD_EXPORTS
#define SCANTHREAD_API __declspec(dllexport)
#else
#define SCANTHREAD_API __declspec(dllimport)
#endif

#define _MAX_FOC_POINTS 100
#define _FOCUS_OUTLIER 500.0

class CQueue;
class CItemInfo;
class CSlide;
class CSlideList;
class CAssay;
class CScanPass;
class AI_Tiff;
class CScaledBitmap;
class CAutoFocus;
class CFocusPoint;
struct FrameMeas;
class CSegMode;



/////////////////////////////////////////////////////////////////////////////
// ScanThread window

class SCANTHREAD_API CScanThread
{
// Construction
public:
	CWinThread*                              m_pScanThread;

	CScanThread();
	virtual ~CScanThread();
	void Start( void *pParam );
	UINT ScanSlides(CSlideList *pSlideList);
	void AllocAxesStrings();
	void FreeAxesStrings();
	BOOL SlideLoader(int action, int tray, int pos, BSTR Saxes);
	static void queueScanAborted(CSlideList* pSlideList);
	static void queueSlideAborted(CSlideList* pSlideList);
	static void queueTiffObject(AI_Tiff* pTiff, CSlideList* pSlideList);
	static void queueFrameMeas(UINT MeasType, UINT MeasDataType, char title[], long lval, double dval, CSlideList* pSlideList);
	static void queueScriptParam(char title[], long lval, CSlideList* pSlideList);
	static void queueScriptParam(char title[], double dval, CSlideList* pSlideList);
	static void queueEnd(CSlideList* pSlideList);
	static void queueNewSlide(POSITION pos, CSlideList* pSlideList);
	static void queueNewPass(POSITION pos, CSlideList* pSlideList);
	static void queueNewRegion(POSITION pos, CSlideList* pSlideList);
	static void queueRegionEnd(POSITION pos, CSlideList* pSlideList);
    static void QueueFrameMeasurements(CFrame *pFrame, double zPos, double xscale, double yscale, int FrameW, int FrameH, int PassNum, int RegionNum, CSlideList* pSlides);

	static void queueSlideEnd(POSITION pos, CSlideList* pSlideList);
	static void queuePassEnd(POSITION pos, CSlideList* pSlideList);

	static void MessageText(CString Msg, CWnd *pWnd, CSlideList* pSlideList);

	BOOL scanSlide(POSITION slide_pos, BOOL gotSlideLoader);
	BOOL scanPass(POSITION slide_pos, POSITION scan_pass_pos);
	BOOL ScanRegion(CSlideList *pSlides, CSlide *pSlide, CScanPass *pPrevPass, CScanPass *pScanPass, Region *region, float ZOffset);			// Scan a single region

	void InitialiseMap(Region *region);
	void GetFocusMap(Region *region);
	int FocusEstimate(Region *region, int x, int y);
	int PointOnPlane(CFocusPoint *P, CFocusPoint *A, CFocusPoint *B, CFocusPoint *C);
	// Get the offsets between two scan passes
    float GetZOffset(CScanPass *pPrevPass, CScanPass *pCurPass);

	BOOL InitialiseGrabber(long Hwnd);
	BOOL InitialiseMotors(long Hwnd);
	int Loader(BOOL action, int tray, int pos_on_tray, BSTR Saxes);
	void ExplainError(int AxisID, int ErrorCode);
	bool StopFindThread();

	int CheckSlideLock (CSlide *pSlide,CString LockMsg);
	POSITION GetNextSlideToScan ();
	int scan_Slideloader_load_slide(CSlide *pSlide);
	int scan_Slideloader_unload_slide(CSlide *pSlide);
	BOOL PeptideFocusMapDone;


	enum {LOAD, UNLOAD};
	enum {NoError, LoadFailed, UnLoadFailed, SlideOnHead, TraySelectFailed, MoveToLoadPosFailed, MoveToUnLoadPosFailed};

private:
	CSlideList*      m_pSlideList;
	CDBCore          m_dbcore;
	BOOL             m_single_slide_scan;

	double           xscale;
	double           yscale; 

	BOOL             new_slide_queued;
	BOOL             slide_loaded;
	int              scan_slide_status;
	BOOL             RegionListFocusMapDone;

	CString          m_slideLoaderLog;
	BOOL             m_slideLoaderProblems;

	IMicSrvr*        m_pIMicSrvr;
	IGrabber*        m_pIGrab;
	BYTE*            m_capBuf;

	BSTR             XYZaxes;
	BSTR             XYZWQaxes;
	BSTR             Xaxes;
	BSTR             Yaxes;
	BSTR             Zaxes;
	BSTR             Saxes;

    CSegMode         *m_pSegment;

#ifdef SCANLOGGING
    // Scan thread logging
    void             LogStr(CString Str);
    void             LogStr(void);

    void             CreateLog(CString Name);       
    void             CloseLog(void);

    CStdioFile       *m_pLogFile;
    CString          m_LogStr;
#endif

};



/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCANTHREAD_H__1F5848ED_424F_4523_8129_A53B161DFE6E__INCLUDED_)
