#if !defined(AFX_SCROLLREGION_H__04F8AC0B_DE74_11D5_A3AE_00C04F05FBD1__INCLUDED_)
#define AFX_SCROLLREGION_H__04F8AC0B_DE74_11D5_A3AE_00C04F05FBD1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScrollRegion.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CScrollRegion dialog

class CScrollRegion : public CDialog
{
// Construction
public:
	CScrollRegion(CWnd* pParent = NULL);   // standard constructor
	BOOL Create(CWnd* pParent);
	void SetPageInterval(int size) { pagesize = size;}; // page down interval on scroll

// Dialog Data
	//{{AFX_DATA(CScrollRegion)
	enum { IDD = IDD_SCROLLREGION };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScrollRegion)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CScrollRegion)
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	int pagesize;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCROLLREGION_H__04F8AC0B_DE74_11D5_A3AE_00C04F05FBD1__INCLUDED_)
