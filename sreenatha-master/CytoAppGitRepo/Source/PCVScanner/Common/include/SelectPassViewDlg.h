////////////////////////////////////////////////////////////////////////////////
//
//	SelectPassViewDlg.h
//
#pragma once
#include "afxwin.h"


// CSelectPassViewDlg dialog

class AFX_EXT_CLASS CSelectPassViewDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectPassViewDlg)

public:
	CSelectPassViewDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectPassViewDlg();

// Dialog Data
	enum { IDD = IDD_SELECT_PASSVIEW_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	POSITION m_pos;

	DECLARE_MESSAGE_MAP()

public:
	// The interface check list
	CCheckListBox m_pass_view_listbox;

	virtual BOOL OnInitDialog();

	// Add pass to internal list for later use in constructing the sselection list
	void AddScanPassName(CString name, int passno, bool visible);

	// Get pass number from name - look up in internal list
	//int GetPassNo(CString name);

	// Given name, set the visibility state in internal list
	int SetPassVisibility(CString name, bool visibility_state);

	// Gather results from check list
	afx_msg void OnBnClickedOk();

	// The list of passes and their visibility
	//CList <ShowPass, ShowPass> m_display_passes;
	CList <CPassVisibility, CPassVisibility> m_scan_pass_list;

	//// these should be obsolete
	//CString GetScanPassName(bool start = true);
	//int GetCount(void);
};
