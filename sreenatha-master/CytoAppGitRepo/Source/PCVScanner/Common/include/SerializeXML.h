// SerializeXML.h: interface for the CSerializeXML class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERIALIZEXML_H__9D849A31_6DC4_11D6_BC05_0000E878C400__INCLUDED_)
#define AFX_SERIALIZEXML_H__9D849A31_6DC4_11D6_BC05_0000E878C400__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <xmlbits.h>

#include <afxdisp.h>
#include <stack>


#define XMLCLASSNODE { CSerializeXML& xmlArchive = static_cast<CSerializeXML&>(ar); \
						xmlArchive.GetNode(GetActualClass()->m_lpszClassName);

#define XMLCLASSNODENAME(className, xar) { CSerializeXML& xmlArchive = static_cast<CSerializeXML&>(xar); \
	xmlArchive.GetNode(className);
#define XMLDATA(attrName) { CSerializeXMLNode* nodePtr = xmlArchive.GetCurrentNode();\
	if (nodePtr != NULL) {nodePtr->DataNode(#attrName, attrName);}}
// Use XMLDATANAMED to serialise a value with a different name from its variable name.
// An example of when this may be useful is if you want to change the name of
// a variable that has previously been serialised with XMLDATA.
#define XMLDATANAMED(attrNameString, attrValue) { CSerializeXMLNode* nodePtr = xmlArchive.GetCurrentNode();\
	if (nodePtr != NULL) {nodePtr->DataNode(attrNameString, attrValue);}}
#define XMLINTDATA(attrName) { CSerializeXMLNode* nodePtr = xmlArchive.GetCurrentNode();\
	if (nodePtr != NULL) {nodePtr->DataNode(#attrName, (int&)(attrName));}}
#define XMLENDNODE { CSerializeXMLNode* nodePtr = xmlArchive.GetCurrentNode();\
	if (nodePtr != NULL) {nodePtr->Close(); }}}
#define XMLSTARTLISTDATA(className) { CSerializeXMLNode* curNodePtr = xmlArchive.GetCurrentNode(); \
	int numberObjects = curNodePtr->GetNoChildren(); curNodePtr->SetFirstChildIndex(); \
	for (int _ix = 0; _ix < numberObjects; _ix++, curNodePtr->GetNextChildIndex()){ \
		CString childNodeName; curNodePtr->GetChildName(_ix, childNodeName); \
		if (childNodeName == className){

#define XMLENDLISTDATA }} curNodePtr->SetFirstChildIndex();}



class CSerializeXMLNode;

class CSerializeXML : public CArchive  
{
	std::stack  <CSerializeXMLNode*> m_nodeList;
	static CFile	m_dummyFile;
	IStream*		m_streamPtr;

public:
	CSerializeXML(const CString& fileName, UINT nMode, 
		        IStream* streamPtr = NULL,
				CDocument* docPtr = NULL);
	virtual ~CSerializeXML();
	void	Close();
	CSerializeXMLNode*	GetNode(LPCTSTR nodeNameStr);	
	void				RemoveNode(CSerializeXMLNode* xmlArchiveNodePtr);
	CSerializeXMLNode*	GetCurrentNode();

// Attributes
	const CString				m_fileName;

	GTXXML::IXMLDOMDocumentPtr	m_xmlDocPtr;

};


class CSerializeXMLNode 
{
	friend class CSerializeXML;
	
	// Private constructor, can only be constructed by CSerializeXML
	CSerializeXMLNode(CSerializeXML* archivePtr, GTXXML::IXMLDOMElementPtr newNodePtr, GTXXML::IXMLDOMNodePtr fatherNodePtr);
protected:
	// Cache of RUNTIME_CLASS created, to speed up creation
	static CMapStringToPtr			m_classMap;
	GTXXML::IXMLDOMElementPtr const	m_nodePtr;
	GTXXML::IXMLDOMNodePtr const		m_fatherNodePtr;
	CSerializeXML* const			m_archivePtr;
	GTXXML::IXMLDOMNodeListPtr		m_childNodeListPtr;
	int								m_childIndex;

public:
	virtual ~CSerializeXMLNode();
	GTXXML::IXMLDOMNodePtr CreateDataNode(LPCTSTR attrName, LPCTSTR attrValue);
	GTXXML::IXMLDOMNodePtr GetDataNode(LPCTSTR nodeName, CString& nodeText);
	int		GetNoChildren();
	void	GetChildName(int childIndex, CString &childName);
	int		GetNextChildIndex() { return m_childIndex++; };
	void	SetFirstChildIndex() { m_childIndex = 0; };
	void	SetChildIndex(int ChildIndex) { m_childIndex = ChildIndex; };
	void	Close();
	void DataNode(LPCTSTR attrName, CTime& attrValue);
	void DataNode(LPCTSTR attrName, CTimeSpan& attrValue);
	void DataNode(LPCTSTR attrName, CString& attrValue);
	void DataNode(LPCTSTR attrName, bool& attrValue);
	void DataNode(LPCTSTR attrName, int& attrValue);
	void DataNode(LPCTSTR attrName, double& attrValue);
	void DataNode(LPCTSTR attrName, LONG& attrValue);
	void DataNode(LPCTSTR attrName, BYTE& attrValue);
	void DataNode(LPCTSTR attrName, UINT& attrValue);
	void DataNode(LPCTSTR attrName, CMapStringToPtr& pointerMap);
	void DataNode(LPCTSTR attrName, CMapStringToString& stringMap);
	void DataNode(LPCTSTR attrName, CStringArray& stringArray);
	void DataNode(LPCTSTR attrName, CByteArray& byteArray);
	void DataNode(LPCTSTR attrName, CDWordArray& wordArray);
	void DataNode(LPCTSTR attrName, COleVariant& variant);

	// Loads into existing objects
	void DataNode(LPCTSTR attrName, CObject& object);

	// Creates new object when loading
	void DataNode(LPCTSTR attrName, CObject*& objectPtr);

	// Creates new object from node name
	static CObject*  CreateObject(const CString& className);
};


#endif // !defined(AFX_SERIALIZEXML_H__9D849A31_6DC4_11D6_BC05_0000E878C400__INCLUDED_)
