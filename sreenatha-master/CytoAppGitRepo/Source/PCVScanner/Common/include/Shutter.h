#if !defined(AFX_SHUTTER_H__2CAD2353_4594_4844_9206_7534CC55015B__INCLUDED_)
#define AFX_SHUTTER_H__2CAD2353_4594_4844_9206_7534CC55015B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Shutter.h : header file
//

#include "MicroscopeDialog.h"


/////////////////////////////////////////////////////////////////////////////
// CShutter dialog


#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

class MICROSCOPEGUI_API CShutter : public MicroscopeDialog
{
// Construction
public:
	CShutter(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag);
	void DisplayShutterSetting();

// Dialog Data
	//{{AFX_DATA(CShutter)
	//enum { IDD = IDD_SHUTTERBASE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShutter)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CShutter)
	afx_msg void OnShutter();
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	
	afx_msg LRESULT OnMicMessage(WPARAM, LPARAM);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private :
	int m_nShutterState;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHUTTER_H__2CAD2353_4594_4844_9206_7534CC55015B__INCLUDED_)
