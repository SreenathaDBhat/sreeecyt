#pragma once

////////////////////////////////////////////////////////////////////////
// ShutterCtrl.cpp : header file
// A shutter control for the wizard
// Written By Karl Ratcliff 21012004
////////////////////////////////////////////////////////////////////////
//

// CShutterCtrl
struct IMicSrvr;
struct IGrabber;

#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUISHUTTER_API __declspec(dllexport)
#else
#define MICROSCOPEGUISHUTTER_API __declspec(dllimport)
#endif

class MICROSCOPEGUISHUTTER_API CShutterCtrl : public CButton
{
	DECLARE_DYNAMIC(CShutterCtrl)

public:
	CShutterCtrl(IMicSrvr *pMic);
	virtual ~CShutterCtrl();
	void		SetButtonState(int NewState);
	void		SetShutterState(int NewState);
	void		Disable(BOOL bDisable);

protected:
	DECLARE_MESSAGE_MAP()

	IMicSrvr	*m_pMicSrvr;
	int 		m_nCurrentPos;
	BOOL		m_bInitialised;
	HICON		m_hClosedIcon;
	HICON		m_hOpenIcon;
	BOOL		m_bDisabled;

	void		DisplayState(void);
	void		AddBitmap(UINT nID);

public:
	afx_msg void OnBnClicked();
	BOOL Create(LPCTSTR lpszCaption, const RECT& rect, CWnd* pParentWnd, UINT nID);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};


