// Slide.h: interface for the CSlide class.
//
//	AR 25 Feb03	Added m_TrayPresent for tray tamper monitoring
//				Added m_Priority, m_Scan and m_DoneScan to CSlide class
//////////////////////////////////////////////////////////////////////

class CAssay;

//#include "../../Common/src/MicServer/MicServer.h"

#if !defined(AFX_SLIDE_H__2B1300A8_3FF2_4CCA_BA80_1676F15649C6__INCLUDED_)
#define AFX_SLIDE_H__2B1300A8_3FF2_4CCA_BA80_1676F15649C6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef SLIDE_EXPORTS
#define SLIDE_API __declspec(dllexport)
#else
#define SLIDE_API __declspec(dllimport)
#endif
#define NORMAL_PRIORITY	1
#define HIGH_PRIORITY	-1
class CMLock;
class CScanPass;

class SLIDE_API CSlide  
{
public:
   
	CString		m_InternalAssayName;
	
	CString		m_FileRoot;
	CAssay*     m_Assay;
	CMLock		*m_applock;				// Application level locking
	
	CString m_SessionName;
	CString	m_RootName;					// root file name, this is passed to the script for any files output by the script
	CString m_AssayName;				//copy of the assay name for this slide
	CString m_FramesFile;


	CSlide();
	virtual ~CSlide();

};

#endif // !defined(AFX_SLIDE_H__2B1300A8_3FF2_4CCA_BA80_1676F15649C6__INCLUDED_)
