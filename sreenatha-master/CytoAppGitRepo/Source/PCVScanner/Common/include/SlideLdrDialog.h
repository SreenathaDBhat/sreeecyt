#if !defined(AFX_SLIDELDRDIALOG_H__6955F9C9_0781_475B_98B9_CFFCD7A4A5BF__INCLUDED_)
#define AFX_SLIDELDRDIALOG_H__6955F9C9_0781_475B_98B9_CFFCD7A4A5BF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SlideLdrDialog.h : header file
//


#define IDD_SLIDELOADERBASE 21500

class CDrawWnd;
class CScaledBitmap;
class CSlideInfo;

typedef void (*EVHANDLER)(UINT event, UINT nFlags, long x, long y, DWORD data);

// Slide load/unload operation callback typedef
typedef BOOL (*MICLOAD)(int nTray, int nPosnInTray, CString & sBarcode, void * pObject);

// Read Barcode operation callback typedef
typedef BOOL (*MICREADBARCODE)( void);

// Slide info supplying callback typedef
typedef BOOL (*MICSLIDEINFO)(int nTray, int nPosnInTray, char * szBarcode, char * szAssayName, BOOL& bSlidePresent);



/////////////////////////////////////////////////////////////////////////////
// CSlideLdrDialog dialog

class _declspec(dllexport) CSlideLdrDialog : public CDialog
{
// Construction
public:
	CSlideLdrDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSlideLdrDialog();	

	CDrawWnd*		m_pLoaderDrawWnd;
	CDrawWnd*		m_pTrayDrawWnd;
	BOOL            m_bResize;

	enum BmpOps {AddBmp, DeleteBmp};
	enum HandlerOps {AddHandler, DeleteHandler};

	void AttachScaledLoaderBitmap(CScaledBitmap *stretchBM, WORD action);
	void AttachScaledTrayBitmap(CScaledBitmap *stretchBM, WORD action);
	void AttachLoaderEventHandler(EVHANDLER func, DWORD data, WORD action);
	void AttachTrayEventHandler(EVHANDLER func, DWORD data, WORD action);
	void InvalidateLoaderRect(RECT *r);
	void InvalidateTrayRect(RECT *r);
	void Clear();

	//--------------------------------- List control and button operations

	int                m_tray;
	int                m_slide;
	
	CScaledBitmap*     m_selectedTrayBM;
	CScaledBitmap*     m_selectedSlideBM;
	CScaledBitmap*     m_TrayBM[6];
	CScaledBitmap*     m_SlideBM[11];
	BOOL               m_initialized;
	CImageList         m_imlist;
	CRect              tray_rect;
	CRect              slide_rect;

	void TrayOffset( CRect *r, int i);
	void SlideOffset(CRect *r, int i);
	void AttachLoadCallback(   MICLOAD func, void * pobj);
	void DetachLoadCallback();
	void AttachUnLoadCallback( MICLOAD func, void * pobj);
	void DetachUnLoadCallback();
	void AttachSlideInfoCallback( MICSLIDEINFO func);
	void DetachSlideInfoCallback();
	void AttachReadBarcodeCallback( MICREADBARCODE func);
	void DetachReadBarcodeCallback();

	void RefreshSlideList( void );						// Update the slide list values and tray bitmap and list control
	void GetSelectedSlideInfo( int& nTray, int& nPositionOnTray, BOOL&bSlidePresent, BOOL& bLoaded,
							    CString& sBarcode, CString& sAssayName);
														// Return information on selected slide from slide list
														//  - public version which uses the protected version.

// Dialog Data
	//{{AFX_DATA(CSlideLdrDialog)
	enum { IDD = IDD_SLIDELOADERBASE };
	CButton	m_use_scan_obj_check;
	CButton	m_unload_button;
	CButton	m_read_barcodes_button;
	CButton	m_load_button;
	CListCtrl	m_slide_list_ctr;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSlideLdrDialog)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:
	
	//--------------------------------- List control and button operations

	CList<CSlideInfo*, CSlideInfo*> m_SlideList;		// Slide list holding barcodes and assay names

	MICLOAD			m_loadfunc;							// Callback (function pointer) for load operation
	void *			m_loadobj;							// Instantiation for load callback
	MICLOAD			m_unloadfunc;						// Callback (function pointer) for unload operation
	void *			m_unloadobj;						// Instantiation for unload callback
	MICSLIDEINFO	m_slideinfofunc;					// Callback (function pointer) for app to supply slide info
	MICREADBARCODE	m_readbarcodefunc;					// Callback (function pointer) for read barcode operation

	void UpdateLoader(int nSelectedTray);						// Update slide loader bitmap
	void UpdateTray(  int nSelectedTray, int nSelectedSlide);	// Update slide tray bitmap
	void UpdateList(  int nSelectedTray, int nSelectedSlide,
		BOOL bInit = FALSE);									// Update slide list control

	static void SlideloaderBM_handler(unsigned int eventID, UINT nFlags, long x, long y, unsigned long data);
	static void SlidetrayBM_handler(unsigned int eventID, UINT nFlags, long x, long y, unsigned long data);

	CSlideInfo * GetLoadedSlideInfo( void );			// Return information on loaded slide from slide list
	CSlideInfo * GetSlideInfo( int nTray, int nSlide );	// Return information on specific slide from slide list
	CSlideInfo * GetSelectedSlideInfo( void );			// Return information on selected slide from slide list

	// Generated message map functions
	//{{AFX_MSG(CSlideLdrDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLoadButton();
	afx_msg void OnUnloadButton();
	afx_msg void OnReadBarcodesButton();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnClickSlideList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangedSlideList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUseScanObj();
	afx_msg void OnOpenScan();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SLIDELDRDIALOG_H__6955F9C9_0781_475B_98B9_CFFCD7A4A5BF__INCLUDED_)
