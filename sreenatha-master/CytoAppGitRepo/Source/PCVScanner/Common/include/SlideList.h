// SlideList.h: interface for the CSlideList class.
//
//////////////////////////////////////////////////////////////////////
#include <afxtempl.h>
#include "SpectraChromeList.h"

class CSlide;

class CQueue;
class CSpectraChromeList;
class CWheelList;
class AI_Tiff;
class CScanPass;

#ifdef SLIDE_EXPORTS
#define SLIDE_API __declspec(dllexport)
#else
#define SLIDE_API __declspec(dllimport)
#endif


#if !defined(AFX_SLIDELIST_H__4A5CBCA2_DF97_4311_847C_31B170C1AF96__INCLUDED_)
#define AFX_SLIDELIST_H__4A5CBCA2_DF97_4311_847C_31B170C1AF96__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



class SLIDE_API CSlideList  
{


public:

	CList<CSlide*, CSlide*>                  m_list;
	POSITION                                 m_pos;
	int                                      m_scanType;
	int                                      m_KillScanFlag;
	int                                      m_KillFindFlag;
	BOOL                                     m_scanRunning;
	BOOL                                     m_findRunning;
	BOOL                                     m_abortCurrentSlide;
    // Set TRUE by the find thread tells the scan thread to skip scanning the current scan pass
    BOOL                                     m_bSkipScanPass;               
    // Set TRUE by the find thread tells the scan thread to skip scanning the current region
    BOOL                                     m_bSkipScanRegion;
    // Set TRUE by the find thread OR reprocessing thread 
    BOOL                                     m_bStopCapture;
	int                                      m_NumCellsToGetBeforeStop;
	int                                      m_FramesInScan;
	int                                      m_ScanCurrentFrame;
	int                                      m_FindCurrentFrame;
	int										 m_FindCurrentRegion;	// Region number counter (for status display)
	CString									 m_VarFile;
	CString									 m_VBQFile;
	CString                                  m_QPath;
	BOOL                                     m_FindThreadProcessing;
	CQueue*                                  m_scanQ;
	CQueue*                                  m_findQ;
	CQueue*									 m_resultsQ;
	CSpectraChromeList*                      m_defaultSpectraChromeList; 
	
	BOOL									 m_ReprocessFlag;
	HANDLE									 m_hFindThread;			// Handle to thread
	CString                                  m_ScanSlidesOfType;
	CString									 m_ProcessName;
	CString									 m_CallingProcess;

	HWND									m_hAutoCaptureWnd;

	static  UINT ScanThread( void *pParam );
	static  UINT FindThread( void *pParam );


	void    WaitFindThreadEnd(void);

	void    OnFind(LPCTSTR QPath, LPCTSTR VarFileName, LPCTSTR QFileName);

	HANDLE	FindThreadHandle(void);

	

	void    ScanEnd(void);

	void    Queues(CQueue* scanQ, CQueue* findQ);

  
	CSlideList();
	virtual ~CSlideList();

	enum {Study, Case, Slide, Scan, Cell};
	enum {STOPSCAN, AUTOSCANALL, AUTOSCANONE, MANUSCANSNAP, MANUSCANAREA};

};

#endif // !defined(AFX_SLIDELIST_H__4A5CBCA2_DF97_4311_847C_31B170C1AF96__INCLUDED_)
