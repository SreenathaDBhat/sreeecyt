//-%%%- SlideLoader.h
//-%%%- 
//-%%%- Copyright Invetech Operations Pty Ltd 2000

#ifndef SlideLoader_h_included
#define SlideLoader_h_included

#include <afxtempl.h>

#undef CLASS_DECLSPEC
#define CLASS_DECLSPEC

#ifdef _EXPORTING
	#undef CLASS_DECLSPEC
	#define CLASS_DECLSPEC	__declspec(dllexport)

	//if _IMPORTING is also defined, there is a conflict so report an error
	#ifdef _IMPORTING
		#pragma message("WARNING: Contradictory #defines: both _EXPORTING and _IMPORTING defined!")
	#endif
#endif

#ifdef _IMPORTING
	#undef CLASS_DECLSPEC
	#define CLASS_DECLSPEC	__declspec(dllimport)
#endif

class CSlideLoaderFault;



//-%%%-======================================================================
//-%%%- Class: CSlideLoader
//-%%%-======================================================================
// The CSlideLoader class encapulates a slide loader device.

// When creating a CSlideLoader instance, it is necessary to 
// pass the address of the device on the Universal Serial Bus 
// to the constructor.
// 
class CLASS_DECLSPEC CSlideLoader
{
public:
	enum Status
	{
		Disconnected,
		Initialising,
		Idle,
		Silent,
		Busy,
		Fault
	};

	CSlideLoader(DWORD dwUSBaddress);

	int Abort(void);
	int BeginSilent(void);
	int CheckForSlideOnStage(void);
	int CheckStagePos(void);
	int ClearCurrentFault(void);
	int EndSilent(void);
	int GetLastFault(const CSlideLoaderFault* pSLF);
	int GetOccupiedSlidePostions(CArray<BOOL, BOOL>& abSlides);
	int GetOccupiedTrayPositions(CArray<BOOL, BOOL>& abTrays);
	int GetSlideBarcode(int iSlidePositions, CString strBarcode);
	int GetSlideBarcodesOnTray(CArray<CString, CString>& astrBarcodes);
	Status GetStatus(void);
	int GetTrayBarcode(CString strBarcode);
	int Initialise(void);
	int IsSlideOnStage(BOOL bSlidePresent);
	int IsStageInCorrectPos(BOOL bStageCorrect);
	int LoadSlide(int iSlidePosition);
	int RemoveSlide(void);
	int ScanSlidePositions(void);
	int ScanTrayPositions(void);
	int SelectTray(int iTrayNumber);
};

#endif
