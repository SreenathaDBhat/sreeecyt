#if !defined(AFX_SLIDELOADERDIALOG_H__CB4884F1_3766_44EF_A748_47CEA80F39D6__INCLUDED_)
#define AFX_SLIDELOADERDIALOG_H__CB4884F1_3766_44EF_A748_47CEA80F39D6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SlideLoaderDialog.h : header file
//

#define IDD_SLIDELOADERBASE 21500

/////////////////////////////////////////////////////////////////////////////
// CSlideLoaderDialog dialog

class CDrawWnd;
class CScaledBitmap;

#ifdef SLIDELOADER_EXPORTS
#define SLIDELOADER_API __declspec(dllexport)
#else
#define SLIDELOADER_API __declspec(dllimport)
#endif

typedef void (*EVHANDLER)(UINT event, UINT nFlags, long x, long y, DWORD data);


class SLIDELOADER_API  CSlideLoaderDialog : public CDialog
{
// Construction
public:
	CSlideLoaderDialog(CWnd* pParent = NULL);   // standard constructor
	CDrawWnd*		m_pLoaderDrawWnd;
	CDrawWnd*		m_pTrayDrawWnd;
	BOOL            m_bResize;

	enum BmpOps {AddBmp, DeleteBmp};
	enum HandlerOps {AddHandler, DeleteHandler};

	void AttachScaledLoaderBitmap(CScaledBitmap *stretchBM, WORD action);
	void AttachScaledTrayBitmap(CScaledBitmap *stretchBM, WORD action);
	void AttachLoaderEventHandler(EVHANDLER func, DWORD data, WORD action);
	void AttachTrayEventHandler(EVHANDLER func, DWORD data, WORD action);
	void InvalidateLoaderRect(RECT *r);
	void InvalidateTrayRect(RECT *r);
	void Clear();
// Dialog Data
	//{{AFX_DATA(CSlideLoaderDialog)
	enum { IDD = IDD_SLIDELOADERBASE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSlideLoaderDialog)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSlideLoaderDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SLIDELOADERDIALOG_H__CB4884F1_3766_44EF_A748_47CEA80F39D6__INCLUDED_)
