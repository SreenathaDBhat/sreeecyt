//-%%%- SlideLoaderFault.h
//-%%%- 
//-%%%- Copyright Invetech Operations Pty Ltd 2000

#ifndef SlideLoaderFault_h_included
#define SlideLoaderFault_h_included


#include <afxtempl.h>

#undef CLASS_DECLSPEC
#define CLASS_DECLSPEC

#ifdef _EXPORTING
	#undef CLASS_DECLSPEC
	#define CLASS_DECLSPEC	__declspec(dllexport)

	//if _IMPORTING is also defined, there is a conflict so report an error
	#ifdef _IMPORTING
		#pragma message("WARNING: Contradictory #defines: both _EXPORTING and _IMPORTING defined!")
	#endif
#endif

#ifdef _IMPORTING
	#undef CLASS_DECLSPEC
	#define CLASS_DECLSPEC	__declspec(dllimport)
#endif





//-%%%-======================================================================
//-%%%- Class: CSlideLoaderFault
//-%%%-======================================================================
// The CSlideLoaderFault class encapulates faults.
// 
class CLASS_DECLSPEC CSlideLoaderFault
{
public:
	enum FaultCategory
	{
		UnexpectedCondition,
		Unrecoverable,
		FatalCondition
	};

	CSlideLoaderFault(void);

	FaultCategory GetFaultCategory(void);
	int GetFaultCode(void);
};

#endif
