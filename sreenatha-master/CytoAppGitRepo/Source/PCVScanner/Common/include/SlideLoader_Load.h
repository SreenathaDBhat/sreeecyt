#if !defined(AFX_SLIDELOADER_LOAD_H__183CCCE3_C0CC_4989_B6AE_EBA6EFD912D8__INCLUDED_)
#define AFX_SLIDELOADER_LOAD_H__183CCCE3_C0CC_4989_B6AE_EBA6EFD912D8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SlideLoader_Load.h : header file
//
#include "MicroscopeDialog.h"

/////////////////////////////////////////////////////////////////////////////
// CSlideLoader_Load dialog

#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

class MICROSCOPEGUI_API  CSlideLoader_Load : public MicroscopeDialog
{
// Construction
public:
	CSlideLoader_Load(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag);
	LRESULT HandleMessage (WPARAM MsgCode, LPARAM MsgData);

// Dialog Data
	//{{AFX_DATA(CSlideLoader_Load)
	//enum { IDD = IDD_SLIDELOADER_LOADBASE };
	CButton	m_CheckSlidePresence;
	int		m_nSlidePosition;
	CString	m_sSlideBarcode;
	int		m_nTrayPosition;
	CString	m_sTrayBarcode;
	BOOL	m_nHomeStage;
	int		m_nSlideIndex;
	CButton m_ReadBarcodesChk;

	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSlideLoader_Load)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSlideLoader_Load)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnLoadslide();
	afx_msg void OnUnloadslide();
	afx_msg void OnKillfocusSlideposition();
	afx_msg void OnReadSlideBarcode();
	afx_msg void OnKillfocusTrayposition();
	afx_msg void OnClearFault();
	afx_msg void OnInitializeTrays();
	afx_msg void OnMute();
	afx_msg void OnMonitor();
	afx_msg void OnDestroy();
	afx_msg void OnLoadSlideN();
	afx_msg void OnKillfocusLoadSlideN();
	//}}AFX_MSG

	afx_msg LRESULT OnMicMessage(WPARAM, LPARAM);

	DECLARE_MESSAGE_MAP()

private:
	int m_nCurrentSlidePosition;
	int m_nCurrentTrayPosition;
	BOOL m_Mute;
	BOOL m_Monitor;
	int m_CtrlType;

	void GetBarcodesOnTray();
public:
	afx_msg void OnBnClickedReadSlidebarcodeId();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SLIDELOADER_LOAD_H__183CCCE3_C0CC_4989_B6AE_EBA6EFD912D8__INCLUDED_)
