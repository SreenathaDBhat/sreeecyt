#if !defined(AFX_SLIDEMAPDIALOG_H__8566198D_391F_459A_A270_1B96B6194729__INCLUDED_)
#define AFX_SLIDEMAPDIALOG_H__8566198D_391F_459A_A270_1B96B6194729__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CSlideMapDialog.h : header file
//
#define IDD_SLIDEMAPBASE 19000

#include "FrameViewer.h"

typedef void (*EVHANDLER)(UINT event, UINT nFlags, long x, long y, DWORD data);


#ifdef SLIDEMAP_EXPORTS
#define SLIDEMAP_API __declspec(dllexport)
#else
#define SLIDEMAP_API __declspec(dllimport)
#endif


/////////////////////////////////////////////////////////////////////////////
// CSlideMapDialog dialog

// Item for eventhandler list
class Event_Item
{
public:	
	EVHANDLER	efunc;
	DWORD		data;
};

class SLIDEMAP_API CSlideMapDialog : public CDialog
{
public:
	CSlideMapDialog(AITiffImageCache *pImageCache, CWnd* pParent = NULL);   // constructor
	virtual ~CSlideMapDialog();	

	void SetObjectivePosition(double x, double y);
	void SetFrameGroup(FrameGroup *pFrameGroup, FrameGroupSet *pFrameGroupSet = NULL,
	                   FrameGroupSetCache *pFrameGroupSetCache = NULL);
	FrameGroup *GetFrameGroup(){ return pFrameGroup; }
	void SetRegionList(CRegionList *pInputRegionList, CRegionList *pOutputRegionList = NULL);
	CRegionList *GetOutputRegionList();
	void SetViewArea(double left, double top, double right, double bottom);
	void SetSlideMap(SlideMap **ppSlideMap){ frameViewer.SetSlideMap(ppSlideMap); }

	void ShowAllPasses(BOOL show);
	BOOL AreAllPassesShown(){ return frameViewer.AreAllPassesShown(); }

	enum HandlerOps {AddHandler, DeleteHandler};
	void AttachEventHandler(EVHANDLER func, DWORD data, WORD action);

	void IdealToWindow(double xI, double yI, int *pxW, int *pyW);
	void WindowToIdeal(int xW, int yW, double *pxI, double *pyI);

	void UpdateFrame(Frame *pFrame);

	Frame *LoadAnUnloadedVisibleFrame();
	BOOL SaveSlideMapImageAsJpeg(LPCTSTR path);

	BOOL showFrames;
	BOOL showViewArea;
	BOOL showRegions;
    BOOL showTopLeft;               // Show the top left of the region being defined
    BOOL showBottomRight;           // Show the bottom right of the region being defined

    VECTOR2D TopLeft;                // Top Left of region being defined - both in ideal coordinates
    VECTOR2D BottomRight;            // Bottom Right of region being defined

	int reducedBandwidthFactor;

    void DefineTopLeft(double X, double Y, BOOL On);
    void DefineBottomRight(double X, double Y, BOOL On);


// Dialog Data
	//{{AFX_DATA(CSlideMapDialog)
	enum { IDD = IDD_SLIDEMAPBASE };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSlideMapDialog)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSlideMapDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point) ;
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg LRESULT OnScanMsg(WPARAM, LPARAM);
	DECLARE_MESSAGE_MAP()


protected:

	FrameViewer frameViewer;

	BOOL prevShowAllPasses;

	AITiffImageCache   *pTiffImageCache;
	FrameGroup         *pBuiltinFrameGroup;
	FrameGroup         *pFrameGroup;
	FrameGroupSet      *pFrameGroupSet;
	FrameGroupSetCache *pFrameGroupSetCache;

	double xStagePosition, yStagePosition;

	// The corners of a rectangle showing the area displayed in a CFrameView window.
	double viewAreaLeft, viewAreaTop, viewAreaRight, viewAreaBottom;

	CTypedPtrList <CPtrList, Event_Item * >m_eventhandlers;

	// Do not make this function public - call Invalidate() instead.
	// Otherwise, in a tabbed dialog, the slidemap is always drawn on top,
	// even if its tab is not active.
	void Update(RECT *pUpdateRect = NULL);

	// Event handling
	void HandleEvents(UINT event, UINT nFlags, long x, long y);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SLIDEMAPDIALOG_H__8566198D_391F_459A_A270_1B96B6194729__INCLUDED_)
