// SlideMapGUI.h


#pragma once

#include "SlideMap.h"
#include "AIListCtrl.h"



#ifdef FRAMEVIEWER_EXPORTS
#define FRAMEVIEWER_API __declspec(dllexport)
#else
#define FRAMEVIEWER_API __declspec(dllimport)
#endif


// CSlideMapLoadDlg dialog

class FRAMEVIEWER_API CSlideMapLoadDlg : public CDialog
{
	DECLARE_DYNAMIC(CSlideMapLoadDlg)

protected:
	CString path;
	CString ext;

public:
	CString selectedFileName;

	CSlideMapLoadDlg(const char pathToFiles[] = NULL, const char fileExtension[] = ".slidemap", CWnd* pParent = NULL);   // standard constructor
	virtual ~CSlideMapLoadDlg();

// Dialog Data

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_list;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonBrowse();
	afx_msg void OnLbnDblclkListSlidemaps();

protected:
	virtual void OnOK();
};


// CSlideMapSaveDlg dialog

class FRAMEVIEWER_API CSlideMapSaveDlg : public CDialog
{
	DECLARE_DYNAMIC(CSlideMapSaveDlg)

protected:
	CString path;
	CString ext;
	BOOL showProprietaryCheck;

public:
	CString selectedFileName;
	BOOL saveProprietaryFields;

	CSlideMapSaveDlg(const char pathToFiles[] = NULL, const char fileExtension[] = ".slidemap",
	                 BOOL askAboutProprietaryFields = FALSE, CWnd* pParent = NULL);   // standard constructor
	virtual ~CSlideMapSaveDlg();

// Dialog Data

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_list;
	CEdit m_edit;
	virtual BOOL OnInitDialog();
	afx_msg void OnLbnSelchangeListSlidemaps();
	afx_msg void OnBnClickedButtonBrowse();

protected:
	virtual void OnOK();
};


// CSlideMapSubmapsDlg dialog

class FRAMEVIEWER_API CSlideMapSubmapsDlg : public CDialog
{
	DECLARE_DYNAMIC(CSlideMapSubmapsDlg)

protected:
	SlideMap *pSlideMap;
	BOOL idealisedMode;

public:
	SlideSubMap *pEditSubMap;

	CSlideMapSubmapsDlg(SlideMap *pSlideMap = NULL, BOOL idealisedMode = FALSE, CWnd* pParent = NULL); // standard constructor
	virtual ~CSlideMapSubmapsDlg();

// Dialog Data

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_subMapList;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonDelete();
	afx_msg void OnBnClickedButtonEdit();
	afx_msg void OnBnClickedButtonNew();
	afx_msg void OnBnClickedButtonReset();

protected:
	virtual void OnOK();
};


// CSlideMapObjectPropertiesDlg dialog

#define SLIDEMAPGUI_ELEMENTDESCRIPTIONS_FILENAME "descriptions.txt"

typedef void (*SlideMapDlgCB_t)(void *);

class FRAMEVIEWER_API CSlideMapObjectPropertiesDlg : public CDialog
{
	DECLARE_DYNAMIC(CSlideMapObjectPropertiesDlg)

protected:
	BOOL idealisedMode;

	enum { noneMode, slidemapMode, submapMode, elementMode } mode;
	SlideMap        *pSlideMap;
	SlideSubMap     *pSubMap;
	SlideMapElement *pElement;
	int xIndex;
	int yIndex;
	BOOL arrayIsLocked;
	BOOL showProprietaryValues;
	CStringArray *pPositioningValues;
	CString elementDescriptionsFileName;

	//SlideMapElement::Shape currentShape;
	//double currentDiameter;
	//double xCurrentExtent;
	//double yCurrentExtent;

	SlideMapDlgCB_t CallbackFunc;
	void *pCallbackParam;

	void SlidemapRefresh();
	void SubmapRefresh();
	void ElementRefresh();
	void Refresh();

	//void ElementUpdateShapeParams();

	BOOL SlidemapApply();
	BOOL SubmapApply();
	BOOL ElementApply();

public:
	CSlideMapObjectPropertiesDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSlideMapObjectPropertiesDlg();

	BOOL Create(CWnd *pParentWnd = NULL, BOOL idealisedMode = FALSE);


	// For editing slidemap properties
	void Update(SlideMap *pSlideMap, BOOL showProprietaryValues = FALSE,
	            SlideMapDlgCB_t CallbackFunc = NULL, void *pCallbackParam = NULL);

	// For editing submap properties
	void Update(SlideSubMap *pSubMap, BOOL showProprietaryValues = FALSE,
	            BOOL arrayIsLocked = FALSE, CStringArray *pPositioningValues = NULL,
				SlideMapDlgCB_t CallbackFunc = NULL, void *pCallbackParam = NULL);

	// For editing slidemap element properties
	void Update(SlideMapElement *pElement, BOOL showProprietaryValues = FALSE,
	            BOOL arrayIsLocked = FALSE, SlideSubMap *pSubMap = NULL,
				int xIndex = -1, int yIndex = -1,
	            const char elementDescriptionsFileName[] = NULL,
				SlideMapDlgCB_t CallbackFunc = NULL, void *pCallbackParam = NULL);

	void Empty(){ m_propList.DeleteAllItems(); }

// Dialog Data

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CAIListCtrl m_propList;
	virtual BOOL OnInitDialog();
	afx_msg void OnLvnItemchangedListProperties(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedApply();

protected:
	virtual void OnOK();
	//virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
};


// CSlideMapChooseOverlayDlg dialog

class FRAMEVIEWER_API CSlideMapChooseOverlayDlg : public CDialog
{
	DECLARE_DYNAMIC(CSlideMapChooseOverlayDlg)

protected:
	CStringArray *pOverlayNames;

public:
	CSlideMapChooseOverlayDlg(CStringArray *pOverlayNames = NULL, CWnd* pParent = NULL);   // standard constructor
	virtual ~CSlideMapChooseOverlayDlg();

	CString overlayName;

// Dialog Data

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_list;
	virtual BOOL OnInitDialog();

protected:
	virtual void OnOK();
};


