// SpectraChrome.h: interface for the CSpectraChrome class.
//
// Sep/22/04	HB	Added "m_ShadeImageMean"
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SPECTRACHROME_H__16E5C59B_8B58_11D4_9C43_00C04F8477D1__INCLUDED_)
#define AFX_SPECTRACHROME_H__16E5C59B_8B58_11D4_9C43_00C04F8477D1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define MAXWHEELS 255
#define CALIBRATION_SLIDE_THICKNESS 850.0
#define ND_FILTER_X 12930.0
#define ND_FILTER_Y 40548.0
#include <afxtempl.h>


class AI_Tiff;
class rgbTriple;
class CSpectraChromeList;
class CCaptureSettings;
class CZstackSettings;
class CFilter;
class CWheelList;
class CSerialPort;
class CSerializeXML;

#ifdef SPECTRACHROME_EXPORTS
#define SPECTRACHROME_API __declspec(dllexport)
#else
#define SPECTRACHROME_API __declspec(dllimport)
#endif

#define SPECTRACHROMEVERSION 10


class SPECTRACHROME_API CZstackSettings  
{
public:
	CZstackSettings();
	virtual ~CZstackSettings();
	void XMLSerialize(CSerializeXML &ar);
	void Serialize(CArchive &ar);
	CZstackSettings* duplicate();

	int     m_on,           // Acquire with Z-stack
            m_num_images,   // Number of image slices to take
            m_focus_dist;   // Focus distance between each
};




class SPECTRACHROME_API CSpectraChrome  
{
public:
	CString                   m_Name;                       // Name of the spectra chrome e.g. DAPI, FITC, Red, Green
    rgbTriple*                m_Colour;                     // colour to use when rendering image data captured with this spectra chrome
	CString                   m_Description;                // Spectra chrome description (e.g. "counterstain" for probes)
	CZstackSettings*          m_ZstackSettings;             // Zstack acquisition params 
	CSpectraChromeList*       m_defaultSpectraChromeList;   // List of default spectra chromes
	BOOL                      m_Fluorescent;
    BOOL                      m_Counterstain;               // Spectrachrome is a counterstain


	CSpectraChrome();
	virtual               ~CSpectraChrome();
	CSpectraChrome        *duplicate();
	void                  XMLSerialize(CSerializeXML &ar);
	

};


#endif // !defined(AFX_SPECTRACHROME_H__16E5C59B_8B58_11D4_9C43_00C04F8477D1__INCLUDED_)
