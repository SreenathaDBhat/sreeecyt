// SpectraChromeList.h: interface for the CSpectraChromeList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SPECTRACHROMELIST_H__7EABE64C_8B5C_11D4_9C43_00C04F8477D1__INCLUDED_)
#define AFX_SPECTRACHROMELIST_H__7EABE64C_8B5C_11D4_9C43_00C04F8477D1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>

class CSpectraChrome;
class CWheelList;
class AI_Tiff;
class CSerializeXML;
struct IMicSrvr;
struct IGrabber;

#ifdef SPECTRACHROME_EXPORTS
#define SPECTRACHROME_API __declspec(dllexport)
#else
#define SPECTRACHROME_API __declspec(dllimport)
#endif

#define VERSION 1
#define MAX_IMS 10

class SPECTRACHROME_API CSpectraChromeList  
{
public:
	CList <CSpectraChrome*, CSpectraChrome*>      m_list;

	POSITION                                      m_currentPos;
	


	CSpectraChromeList();
	virtual               ~CSpectraChromeList();

	void                  Defaults(int action);
	BOOL                  Modified();
	void                  Serialize(CArchive &ar);
	void                  XMLSerialize(CSerializeXML &ar);
	CSpectraChromeList*   duplicate();
	
	
	// Check validity of spectrachrome name (not a duplicate, not blank, alphanumeric characters
	bool CheckSpectraChromeName(CString *sp_name);

	// Add spectrachrome to list (after checking it's validity)
	bool AddSpectraChrome(CSpectraChrome *sp);

	CSpectraChrome*       GetNamed(CString name);
    CSpectraChrome*       FindMatchingCounterstain(CSpectraChrome *pSpec);
	void                  RemoveAll();
	CSpectraChromeList*	  FluorescentDefaultList(void);			    // Build a list of fluorescent spectrachromes only
    CSpectraChromeList*	  FluorescentProbes(void);                  // Build a list of fluorescent signals spectrachromes only

	enum {LoadDefaults, SaveDefaults};
};

#endif // !defined(AFX_SPECTRACHROMELIST_H__7EABE64C_8B5C_11D4_9C43_00C04F8477D1__INCLUDED_)
