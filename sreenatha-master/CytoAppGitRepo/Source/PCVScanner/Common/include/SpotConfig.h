///////////////////////////////////////////////////////////////////////////////////////////////////////
// The only thing you have to call to get a modal spot config dialog
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __SPOTCONFIG_H
#define __SPOTCONFIG_H


#include "Assay.h"
// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif

extern "C" 
{
    int DllExport DoSpotConfig(CAssay** pSelectedAssay, BOOL DoReprocess, BOOL AssInList);
}

void DllExport DoSpotConfigScript(CAssay* pSelectedAssay);


#define MAXCLASSES 20


#endif