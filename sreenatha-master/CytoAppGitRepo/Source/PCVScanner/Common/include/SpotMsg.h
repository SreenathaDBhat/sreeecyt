/////////////////////////////////////////////////////////////
// User defined messages handled by various WinCV
// and other dialogs.
// Written By Karl Ratcliff
/////////////////////////////////////////////////////////////

#ifndef __PSOTMSG_H
#define __PSOTMSG_H

/////////////////////////////////////////////////////////////
// Autocapture messages
/////////////////////////////////////////////////////////////
#define WM_AUTOCAPSPOTCOUNT             WM_USER + 0x100     // Start of a new spot count slide
#define WM_AUTOCAPNEWCELLCALL           WM_USER + 0x101     // Equivalent to a new cell call
#define WM_AUTOCAPLIVECALL              WM_USER + 0x102     // Equivalent to a live call
#define WM_AUTOCAPUPDATECOUNT           WM_USER + 0x103     // Update frame counts on the dialog
#define WM_AUTOCAPSTARTDAPISEG          WM_USER + 0x104     // Start Counterstain segmentation mode
#define WM_AUTOCAPENDDAPISEG            WM_USER + 0x105     // End segmentation mode
#define WM_AUTOCAPENABLETOPLEVEL        WM_USER + 0x106     // Enable top level buttons
#define WM_AUTOCAPCVSCANEND             WM_USER + 0x107     // End CVScanner
#define WM_AUTOCAPENABLEBTN             WM_USER + 0x108     // To enable or disable buttons send this message
#define WM_AUTOCAPMESSAGE               WM_USER + 0x109     // Message to display in status window of dialog
#define WM_AUTOCAPNEXTCOMPONENT         WM_USER + 0x110     // Select next component 
#define WM_AUTOCAPRANKUPDATE            WM_USER + 0x111     // Cluster Rank Update for stage controls
#define WM_AUTOCAPUPDATEPOSITION        WM_USER + 0x112     // Update stage positions
#define WM_AUTOCAPDASHENABLE            WM_USER + 0x113     // Enable/Disable new cell, live, capture buttons on dash
#define WM_AUTOCAPENABLEFOCUSSLIDER     WM_USER + 0x114     // Enable or disbale the focus slider
#define WM_AUTOCAPTUREMOVEZ             WM_USER + 0x116     // Move the stage in Z
#define WM_AUTOCAPCAPTURECALL           WM_USER + 0x117     // Capturecall
#define WM_AUTOCAPFLUOUPDATE            WM_USER + 0x118     // Update fluo camera settings
#define WM_AUTOCAPCOUNTERSTAIN          WM_USER + 0x119     // Set to counterstain in fluor select box
#define WM_AUTOCAPSTOP                  WM_USER + 0x120     // Stop autocapture on reception of this message
#define WM_AUTOCAPCHANGEBAY             WM_USER + 0x121     // Change bays 
#define WM_AUTOCAPSTART                 WM_USER + 0x122     // Start autocapture 
#define WM_AUTOCAPDONE                  WM_USER + 0x123     // Done metaphase autocapture
#define WM_AUTOCAPFRAMECOUNT            WM_USER + 0x124     // Update counts
#define WM_AUTOCAPQUIT                  WM_USER + 0x125     // Quit autocapture
#define WM_AUTOCAPFOCUS                 WM_USER + 0x126     // Autofocussing for metaphase finding
#define WM_AUTOCAPFOCUSDONE             WM_USER + 0x127     // Autofocussing complete for metaphase capture
#define WM_AUTOCAPMOVETOCLUSTER         WM_USER + 0x128     // Autocapture move to current cluster

/////////////////////////////////////////////////////////////
// Focus dialog messages
/////////////////////////////////////////////////////////////
#define WM_PRESCANFOCUSSTART            WM_USER + 0x100     // Start autofocusssing on a bay
#define WM_PRESCANFOCUSDONE             WM_USER + 0x101     // Set focus done on the focussing dialog

/////////////////////////////////////////////////////////////
// FLF Control Panel messages used during the prescanning
// phase
/////////////////////////////////////////////////////////////

typedef struct 
{
    BOOL StartBtn, SkipBtn, PauseBtn, StopAllBtn, HotSwapPauseBtn;
} FLFBUTTONS;

typedef struct
{
    int stagex, stagey, stagez;
} FLFSTAGEPOS;

#define WM_FLFPRESCANSTART              WM_USER + 0x101     // Start a prescan for a bay
#define WM_FLFPRESCANEND                WM_USER + 0x102     // End of all prescanning
#define WM_FLFLAUNCHPRESCANTHREAD       WM_USER + 0x103     // Launch a prescan thread
#define WM_FLFSETBUTTONS                WM_USER + 0x104     // Disable buttons specified by lParam
#define WM_FLFENABLETOPLEVEL            WM_USER + 0x105     // Disable or enable top level toolbar buttons depending on wParam
#define WM_FLFMOVESTAGE                 WM_USER + 0x107     // Move the stage to a point specified by LPARAM (points to FLFSTAGEPOS)
#define WM_FLFRESETCOUNT                WM_USER + 0x108
#define WM_FLFCOUNT			WM_USER + 0x109
#define WM_FLFINITGRIDANDTHUMBS		WM_USER + 0x110
#define WM_FLFCLEARDATA			WM_USER + 0x111
#define WM_FLFADDCELL			WM_USER + 0x112


/////////////////////////////////////////////////////////////
// CV Navigator messages
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// This struct passes data as the LPARAM part of the 
// WM_NAVIGATORNEWSLIDE message. From the spot wizard
/////////////////////////////////////////////////////////////

typedef struct 
{
    int     Application;
    CString Assay;  
    CString SlideName;
} NAVNEWSLIDEPARAM;

#define WM_NAVIGATORNEWSLIDE            WM_USER + 0x100     // Add new slide in the navigator
#define WM_NAVIGATORENABLECLOSE         WM_USER + 0x101     // Enable/Disable close and delete options in nav r click menu

/////////////////////////////////////////////////////////////
// Stuff sent to the scan utility dialog from the spot wizard
/////////////////////////////////////////////////////////////

#define WM_SCANUTILCAMERADLG            WM_USER + 0x100     // Pull up the camera settings dialog
#define WM_SCANUTILSETCURRENTBAY        WM_USER + 0x101     // Set the bay specified by wParam as the current bay

/////////////////////////////////////////////////////////////
// Assay selector grid messages to the scan wizard
/////////////////////////////////////////////////////////////

#define WM_GRIDSELECTIONCHANGED         WM_USER + 0x100     // User clicked a different grid selection

/////////////////////////////////////////////////////////////
// Stuff sent to the spot wizard from worker threads
/////////////////////////////////////////////////////////////

typedef struct 
{
    int left, top, bottom, right;
} WIZARDSCANAREA;

#define WM_WIZARDPRESCANEND             WM_USER + 0x120     // ALL Prescanning has finished
#define WM_WIZARDPRESCANFOCUSSING       WM_USER + 0x121     // Prescan thread is focussing on a slide
#define WM_WIZARDPRESCANSCANNING        WM_USER + 0x122     // Prescan thread is doing a prescan on a slide
#define WM_WIZARDPRESCANSCANEND         WM_USER + 0x123     // Prescanning has finished for one bay
#define WM_WIZARDSCANAREAUPDATE         WM_USER + 0x100     // Sent to the scan area proppage to update an area defined by the scan area setup dlg
#define WM_WIZARDSCANAREADEFINE         WM_USER + 0x101     // Sent to the scan area proppage if a new area defined
#define WM_WIZARDREFRESHSCANAREAS       WM_USER + 0x102     // Sent to the scan area proppage to refresh the scan area list
#define WM_WIZARDSLIDECONTROL           WM_USER + 0x104     // Prescan thread control of the slide display
#define WM_WIZARDENDFOCUSMAPPING        WM_USER + 0x100     // At end of focus map building in capture wizard
#define WM_WIZARDFOCUSSING              WM_USER + 0x101     // Capture wizard is focussing on a slide
#define WM_WIZARDENDFOCUS               WM_USER + 0x102     // Wizard ended focussing on current slide
#define WM_WIZARDCLASSIFIERUPDATE       WM_USER + 0x115     // Send the wizard the selected classifier ID
#define WM_WIZARDCHANGEBAY              WM_USER + 0x116     // Notify wizard of bay change
#define WM_WIZARDSORTUPDATEFEATURE		WM_USER + 0x117     // Send the wizard the sortflag
#define WM_WIZARDSORTUPDATEORDER		WM_USER + 0x118     // Send the wizard the sortflag
#define WM_WIZARDSORTUPDATECAPTURECELLS	WM_USER + 0x119     // Send the wizard the sortflag
#define WM_WIZARDUPDATESCANAREADLG      WM_USER + 0x121     // Sent to the scan area dialog when the wizard has changed bays

/////////////////////////////////////////////////////////////
// Stuff sent from the spot wizard to the scan area dialog
/////////////////////////////////////////////////////////////

#define WM_SCANAREADEFINE               WM_USER + 0x100     // Scan area selection done in wizard so update values in dialog

/////////////////////////////////////////////////////////////
// Message sent when the set offset button hit in the stage
// controls
/////////////////////////////////////////////////////////////

#define WM_WIZARDSETOFS                 WM_USER + 0x106

/////////////////////////////////////////////////////////////
// Messages sent to the stage control locate dlg
/////////////////////////////////////////////////////////////

#define WM_LOCATESTART                  WM_USER + 0x100     // Reset to start of metlist

/////////////////////////////////////////////////////////////
// Messages sent to the classifier selection dlg
/////////////////////////////////////////////////////////////

#define WM_WIZARDSELCLASSIFIER          WM_USER + 0x100     // Select classifier specified by wParam

#endif
