#if !defined(AFX_STAGECOORDINATES_H__0092D114_EE01_4FFB_B27F_FBB9A8D7E826__INCLUDED_)
#define AFX_STAGECOORDINATES_H__0092D114_EE01_4FFB_B27F_FBB9A8D7E826__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StageCoordinates.h : header file
//
#include "MicroscopeDialog.h"

/////////////////////////////////////////////////////////////////////////////
// CStageCoordinates dialog


#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

class MICROSCOPEGUI_API CStageCoordinates : public MicroscopeDialog
{
// Construction
public:
	CStageCoordinates(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag);

	LRESULT HandleMessage (WPARAM MsgCode, LPARAM MsgData);

// Dialog Data
	//{{AFX_DATA(CStageCoordinates)
	//enum { IDD = IDD_STAGECOORDINATESBASE };
	int		m_nX;
	int		m_nY;
	int		m_nZ;
	int		m_nBay;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStageCoordinates)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CStageCoordinates)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnStagecoordMovetobay();
	afx_msg void OnDestroy();
	//}}AFX_MSG

	afx_msg LRESULT OnMicMessage(WPARAM, LPARAM);

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STAGECOORDINATES_H__0092D114_EE01_4FFB_B27F_FBB9A8D7E826__INCLUDED_)
