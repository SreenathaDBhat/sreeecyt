//
// StageSlider.h
//
#include "MicInterface.h"

#ifndef	_StageSlider_H
#define	_StageSlider_H

class CStageSlider : public CWnd
{
// Construction

public:

	CStageSlider(BOOL bHorizontal, int Axis);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStageSlider)
	public:
	virtual BOOL Create(LPCTSTR lpszWindowName, short x, short y, short w, short h, CWnd* pParentWnd);
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CStageSlider();
	void SetEventHandler(void (*func)(UINT event, long x, long y, DWORD data) = NULL, DWORD data = 0);
	void Control (BOOL );
	void SetStepSize (float );
	CMicInterface m_microscope;

private:
	int m_nMax_Speed;
	float  m_fStepSize;
	int buttonwidth;
	int m_nSteps;
	BOOL bMove;
	short m_Stage_Axis;
	int Centre_point;
	BOOL bStage_Move_Interlock;
	int RootSlideRange;
	int SlideRange;
	int ypos;
	int xpos;
	RECT limit;
	void (*externalEH)(UINT event, long x, long y, DWORD data);
	DWORD client_data;
	BOOL bHorz;
	BOOL bSliderEnabled;
	UINT m_ButtonDown;


	// Generated message map functions
protected:
	void CALLBACK stagestep(HWND hWnd, UINT uMsg, UINT idEvent, DWORD dwTime);
	int stagespeed  (int speed);

	//{{AFX_MSG(CStageSlider)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnDestroy();
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


#endif
