

#ifdef FOCUSAUTO_EXPORTS
#define FOCUSAUTO_API __declspec(dllexport)
#else
#define FOCUSAUTO_API __declspec(dllimport)
#endif

#include "FocusAuto.h"

class AutofocusValidator;



class FOCUSAUTO_API CStepFocus
{
public:
	CStepFocus(BOOL UseValidation, AutofocusValidator *pAFValidator = NULL);
	~CStepFocus();

	// Focus over a range
	BOOL FocusRange(CMicHelper *pMic, double FocusThresh, FocusOpts FOpts, float Z, float Range,       BOOL Fluorescent, BOOL Tissue, BOOL CoverslipDetect, int* ErrorCode);
	
	// Focus over a finite number of steps
	BOOL FocusSteps(CMicHelper *pMic, double FocusThresh, FocusOpts FOpts, float Z, int NumberOfSteps, BOOL Fluorescent, BOOL Interphase, BOOL Tissue, BOOL CoverslipDetect, int* ErrorCode);

	BOOL FocusZ(float &FZ) { FZ = m_FocusZ; return m_FocusZ != UNDEFINED_Z_POSITION; }

	BOOL IsValidated(void) { return m_Validated; }

	void DoValidationCheck(BOOL DoIt) { m_UseValidation = DoIt; }

	double Backlash(void) { return m_Backlash; }


	BOOL StepFocus(CMicHelper *pMic, double FocusThresh, FocusOpts FOpts, float Z, int NumberOfSteps, BOOL Fluorescent, BOOL Interphase, BOOL Tissue, BOOL CoverslipDetect, int* ErrorCode);
	void CreateFocusThreadSyncObjects(void);
	void DeleteFocusThreadSyncObjects(void);
	BOOL curveFit(double z0, double z1, double z2, double f0, double f1, double f2, double *zf);

	static int FocusThreadStep(CStepFocus *TCB);
	static int FocusThreadCaptureFrames(CStepFocus *TCB);
	static int FocusThreadProcessFrames(CStepFocus *TCB);

	void LaunchFocusThreadsAndWait(StepFocusInfo *SI);
	void BackgroundSubtractFocus(StepFocusInfo &info);

	void GotoFocus(CMicHelper *pMic, float focus, int focusDirection);

	int      FocFineEnvelopeFocus(double *MaxMeas, float *FocusZ, FocusReturnStatus *FocusStatus);
	int  InterphaseFluorescentHighMagFocus(double *MaxMeas, float *FocusZ, FocusReturnStatus *FocusStatus);
	int  TissueFluorescentHighMagFocus(double *MaxMeas, float *FocusZ, FocusReturnStatus *FocusStatus);
	int FocSuperFineEnvelopeFocus(double *MaxMeas, float *FocusZ, FocusReturnStatus *FocusStatus);

	int EnvelopeFocus(int Type, BOOL IsTissue, float OriginalZ, double *MaxMeas, float *FocusZ, FocusReturnStatus *FocusStatus);
	

	void ValidateFocus(int ImageIdx, FocusReturnStatus *FocusStatus, int *ret);

	void ClearData(void);


	void OutputDebugInfo(int FirstImage, int SecondImage, double MaxFocusMeas, int MaxImage, int LowLimit, int HighLimit);


	StepFocusInfo		 m_info;					// 

    // Scan information
    CRITICAL_SECTION     csMoveCritical;            // Locks scan and capture
    CSyncObjectEvent     *pNewLocationEvent;        // Signals at a new location
    CSyncObjectEvent     *pGrabbedLocationEvent;    // Grabbed the new location
 	CSyncObjectSemaphore *pImageDataReady;			//
    CSyncObjectEvent     *pEndOfMoveEvent;          // End of move

	// Pipe for communicating images 
	IMAGE_QUEUE         ImageQ;
	
    // Thread handles and ids
    HANDLE              hCaptureThread;
    HANDLE              hImageProcessingThread;
	DWORD               CaptureThreadId;
	DWORD               ImageProcessingThreadId;

  

	double				m_Mag;						// Objective mag
	CMicHelper			*m_pMic;					// The mic
	float				m_StepSize;					// Focus step size
	double				m_FocusThreshold;
	double				m_Backlash;
	float				m_FocusZ;
	BOOL				m_Validated;
	BOOL				m_UseValidation;
	AutofocusValidator *m_pAFValidator;
};