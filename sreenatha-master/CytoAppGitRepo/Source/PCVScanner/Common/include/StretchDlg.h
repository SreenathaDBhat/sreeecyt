#if !defined(AFX_STRETCHDLG_H__60C0A272_FF42_419E_B7B8_170937B68964__INCLUDED_)
#define AFX_STRETCHDLG_H__60C0A272_FF42_419E_B7B8_170937B68964__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StretchDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStretchDlg dialog
class CDrawWndBase;
class CSRDrawWnd;
class AI_Tiff;
class CHistogram;

#define IDD_STRETCHDLG                  17020

// Render callback
typedef void (*RENDERCB)(void *data);

class AFX_EXT_CLASS CStretchDlg : public CDialog
{
// Construction
public:
	CStretchDlg(AI_Tiff *tiff, int index, CWnd* pParent = NULL);   // standard constructor
	virtual ~CStretchDlg();
	static void StretchEH(UINT event, long x, long y, DWORD data);
	void AttachRenderCB(RENDERCB pRender, void * pObj);



// Dialog Data
	//{{AFX_DATA(CStretchDlg)
	enum { IDD = IDD_STRETCHDLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStretchDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CStretchDlg)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	RENDERCB m_renderCB;
	void * m_renderObj;
	void HandleStretch(UINT event, long x, long y);
	CHistogram* m_histo;			// Histo drawing class (draws into a CDrawWnd object)
	CDrawWndBase*	m_pHistoWnd;	// Used for histogram display
	AI_Tiff*	m_pTiff;			// Tiff object that contains Componenets we wish to manipulate, displayed in m_pTiffWnd
	int			m_index;			// Index of component in m_pTiff to manipulate
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STRETCHDLG_H__60C0A272_FF42_419E_B7B8_170937B68964__INCLUDED_)
