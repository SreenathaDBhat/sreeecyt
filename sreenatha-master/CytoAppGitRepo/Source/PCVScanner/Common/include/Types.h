#ifndef _TYPES_H_
#define _TYPES_H_

#include <tchar.h>

typedef unsigned char Uchar;

#define MAXPOINTS	20
#define	MAXCHARS	25

typedef struct coordpoint {
						double x;
						double y;
						double z;
						} CoordPoint;

typedef struct coordframe {
						int rows;
						int cols;
						CoordPoint Pt[MAXPOINTS];
						TCHAR EFCoord[MAXPOINTS][MAXCHARS];
						CoordPoint Offset[MAXPOINTS];
						}CoordFrame; 

typedef struct tripoint {
						int x;
						int y;
						int z;
						} Tripoint;

typedef struct dpoint{
						double x;
						double y;
						double z;
						} DPoint;

typedef struct imagecalib{
						double x;
						double y;
						double z;
						double lens_power;
						int lens_num;
						int bcalibrated;
						} ImageCalib;

typedef struct lenspower {
						int lens_num;
						double power;
						int type;
						} LensPower;

typedef struct efdata{
						int Axis,Orientation;
						double scaleX,scaleY,Circle_Radius;		
						} EngFinderData;

typedef struct matrix {
						double **Mat;
						int rows;
						int cols;
						} Matrix;

#endif