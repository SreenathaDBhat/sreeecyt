#pragma once


// CWarnFreeSpace dialog

class _declspec(dllexport) CWarnFreeSpace : public CDialog
{
	DECLARE_DYNAMIC(CWarnFreeSpace)

public:
	CWarnFreeSpace(CWnd* pParent = NULL);   // standard constructor
	virtual ~CWarnFreeSpace();

	BOOL	m_neveragain;
	CString	m_warningtxt;

	BOOL m_suppress;
	long IDD;

// Dialog Data
//	enum { IDD = IDD_WARNFREESPACE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
