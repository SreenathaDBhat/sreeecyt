/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Wide Char and MBCS conversion routines - wrappers for SDK calls - header file
// K Ratcliff 100106
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#ifdef HELPER3_EXPORTS
#define HELPER3_API __declspec(dllexport)
#else
#define HELPER3_API __declspec(dllimport)
#endif

#pragma once

// Convert a wide char string to a MBCS one
int  HELPER3_API ToMBCS(LPSTR MBCSString, LPCWSTR WideString);



