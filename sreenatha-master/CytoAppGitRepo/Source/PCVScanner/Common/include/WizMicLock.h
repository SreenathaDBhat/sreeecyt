/////////////////////////////////////////////////////////////////////////////////////
// Simple class to take some effort out of control over the micserver
// Call Lock to lock it, Inlock to unlcok it
// NB will unlock in the destructor 
/////////////////////////////////////////////////////////////////////////////////////

#pragma once

#ifdef MICHELPER_EXPORTS
#define MICHELPERDLL_API __declspec(dllexport)
#else
#define MICHELPERDLL_API __declspec(dllimport)
#endif

struct IMicSrvr;

class MICHELPERDLL_API MicLock
{
public:
    MicLock(IMicSrvr *pMic);
    ~MicLock(void);

    BOOL LockMic(void);                        // Returns FALSE if cant lock it, TRUE otherwise
    void UnlockMic(void);
    IMicSrvr    *m_pIMicSrvr;				// Pointer to the grabserver

private:
    BOOL        m_bLocked;
};
