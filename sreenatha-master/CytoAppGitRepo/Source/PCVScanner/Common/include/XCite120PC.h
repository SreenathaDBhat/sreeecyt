/*
 *		X C I T E 1 2 0 PCC O N T R O L L E R . H
 *			X-Cite120PC Fluorescent lamp control
 */

#ifndef _XCITE120PC_H
#define _XCITE120PC_H

#include "MotorController.h"
#include "Axes.h"
#include "..\CameraLink\ComPort\ComPort.h"
#include "MMTimer.h"

#ifdef XCITE120PCCONTROLLER_EXPORTS
#define XCITE120PCCONTROLLER_API __declspec(dllexport)
#else
#define XCITE120PCCONTROLLER_API __declspec(dllimport)
#endif


// This class is exported from the IMScontrol.dll
class CXCite120PCController : public CMotorController
 {
public:
		XCITE120PCCONTROLLER_API CXCite120PCController(void);
		~CXCite120PCController(void);

				//General
		int Open (int PortNo);
		int Close();
		int SetUp (int AxisID, int iPortAddr, AxisData *AxisData);
		int Busy();
		int Wait();
		int Stop();
		int Status();

		int ShutDown();
		int Identify(char *Name);
		int SetControlState (int state);

		int ClearAlarm();
		int RunTimed(int waitmove);
		int SwitchLamp (BOOL Setting, int waitmove);
		int GetLamp(int *LampValue);
		int SetLamp(int LampValue, int waitmove);
		int GetLampAge (int *Hours);
		int SetExposure (double Exposure);
		int GetExposure (double *Exposure);
		int SetShutter (int Setting, int waitmove);
		int	GetShutter (int *Setting);
		int IsLampReady (int *Ready);
		int IsLampOn (int *On);

		void CallbackHandler(UINT wTimerID);
		static void CALLBACK TimerCallbackFn(UINT wTimerID, UINT msg,DWORD dwUser, DWORD dw1, DWORD dw2); 
		CMMTimer DelayTimer;
		UINT m_TimerID;
		HANDLE m_hCommsFree;			//event handle


		BOOL m_bBusy;
private:
		int ErrorCode;
		int m_Open;
		CComPort ComPort;

		char Command[COMPORT_BUF_LEN];
		char Reply[COMPORT_BUF_LEN];

		int m_PreWriteDelay;	
		int m_PostWriteDelay;	
		int m_PostReadDelay;

		int DeviceComms();
		int Connect();
		int LockFrontPanel();
		int UnlockFrontPanel();
		int GetSoftwareVersion(char *Name);
		int GetLampHours(int *Hrs);
		int GetUnitStatus(int *Status);
		int ClearAlarm(int *Status);
		int RunATimedExposure();
		int OpenShutter();
		int CloseShutter();
		int GetIntensityLevel(int *I);
		int SetIntensityLevel(int I);
		int GetExposureTime(int *Exp);
		int SetExposureTime(int Exp);
		int TurnLampOn();
		int TurnLampOff();

		double m_ExposureTime;

 };

	//These 2 functions ONLY are exported by dll
extern "C" XCITE120PCCONTROLLER_API CMotorController *CreateXCite120PCController(void);
extern "C" XCITE120PCCONTROLLER_API void  DestroyXCite120PCController(CMotorController *device);


#endif

 