#pragma once

#include <xmllite.h>

class CXMLReader
{
private:
	HRESULT hr;
    CComPtr<IXmlReader> pReader;
	CComPtr<IXmlReaderInput> pReaderInput;


public:
	CXMLReader()
	{
		hr = S_FALSE;
		pReader = NULL;
		pReaderInput = NULL;
	}

	CComPtr<IXmlReader> &Reader()
	{
		return pReader;
	}

	bool CreateReader( CComPtr<IStream> & pStream)
	{
		if (FAILED(hr = CreateXmlReader(__uuidof(IXmlReader), (void**) &pReader, NULL)))
		{
			wprintf(L"Error creating xml reader, error is %08.8lx", hr);
			return false;
		}

		pReader->SetProperty(XmlReaderProperty_DtdProcessing, DtdProcessing_Prohibit);

		if (FAILED(hr = CreateXmlReaderInputWithEncodingCodePage(pStream, NULL, 65001, FALSE, L"c:\temp", &pReaderInput)))
		{
			wprintf(L"Error creating xml reader with encoding code page, error is %08.8lx", hr);
			return false;
		}


		if (FAILED(hr = pReader->SetInput(pReaderInput)))
		{
			wprintf(L"Error setting input for reader, error is %08.8lx", hr);
			return false;
		}

		return true;
	}



};