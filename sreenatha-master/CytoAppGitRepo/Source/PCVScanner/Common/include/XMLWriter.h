#pragma once

#include <xmllite.h>

class CXMLWriter
{
private:
	HRESULT hr;
    CComPtr<IXmlWriter> pWriter;
	CComPtr<IXmlWriterOutput> pWriterOutput;


public:
	CXMLWriter()
	{
		hr = S_FALSE;
		pWriter = NULL;
		pWriterOutput = NULL;
	}

	CComPtr<IXmlWriter> &Writer()
	{
		return pWriter;
	}

	bool CreateWriter( CComPtr<IStream> & pStream)
	{
		if (FAILED(hr = CreateXmlWriter(__uuidof(IXmlWriter),(void**) &pWriter, NULL)))
		{
			wprintf(L"Error creating xml writer, error is %08.8lx", hr);
			return false;
		}

		if (FAILED(hr = CreateXmlWriterOutputWithEncodingCodePage( pStream, NULL, 65001, &pWriterOutput)))
		{
			wprintf(L"Error creating xml reader with encoding code page, error is %08.8lx", hr);
			return false;
		}

		if (FAILED(hr = pWriter->SetOutput(pWriterOutput)))
		{
			wprintf(L"Error setting output for writer, error is %08.8lx", hr);
			return false;
		}

		if (FAILED(hr = pWriter->SetProperty(XmlWriterProperty_Indent, TRUE)))
		{
			wprintf(L"Error setting indent property in writer, error is %08.8lx", hr);
			return false;
		}

		return true;
	}



};