// XPonly.h : main header file for the XPonly DLL
//

#pragma once



#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif


#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols


// CXPonlyApp
// See XPonly.cpp for the implementation of this class
//

class CXPonlyApp : public CWinApp
{
public:
	CXPonlyApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};




class DllExport dacl
{
public:
	dacl();
	~dacl();
public:
	BOOL CreateMyDACL(SECURITY_ATTRIBUTES * pSA);

};
