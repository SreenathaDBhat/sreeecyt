#if !defined(AFX_XYSTAGECONTROL_H__41BBBC73_3CD2_4DE0_9FB0_265C347D86FA__INCLUDED_)
#define AFX_XYSTAGECONTROL_H__41BBBC73_3CD2_4DE0_9FB0_265C347D86FA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// XYStageControl.h : header file
//
#include "MicroscopeDialog.h"


/////////////////////////////////////////////////////////////////////////////
// CXYStageControl dialog

#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

class MICROSCOPEGUI_API CXYStageControl : public MicroscopeDialog
{
// Construction
public:
	CXYStageControl(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag);

// Dialog Data
	//{{AFX_DATA(CXYStageControl)
	//enum { IDD = IDD_STAGEXYBASE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXYStageControl)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CXYStageControl)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()


private:
	CPoint centre;
	CPoint joystick;
	CPoint displacement;
	int DeadBand;

	RECT wrect;
	int radius;
	CPoint m_nSteps;
	BOOL bMove;

	int JoyStickPosition (CPoint point);
	void MoveAxes (CPoint move);
	void SetStepSize (int x, int y);
	void StageSpeed (int *SpeedX, int *SpeedY);
	UINT m_ButtonDown;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XYSTAGECONTROL_H__41BBBC73_3CD2_4DE0_9FB0_265C347D86FA__INCLUDED_)
