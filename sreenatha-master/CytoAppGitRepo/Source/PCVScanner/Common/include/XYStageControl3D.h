

/*
 *		XYStageControl3D.h	M.Gregson
 *
 *
 */

#ifndef XYSTAGECONTROL3D_H
#define XYSTAGECONTROL3D_H

#include "MicInterface.h"
#include "AutoScope.h"
#include "MicroErrors.h"

#include <D3DControlLOD.h>
#include <d3dctrl.h>
#include <JoystickWnd.h>

// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif

class DllExport XYStageControl3D : public XYStageControl3DLOD
{
public:
	JoystickWnd		m_joyWnd;

	XYStageControl3D();
	~XYStageControl3D();
	BOOL InitCtrl(CWnd* pParentWnd, void *d3dctrl, int x, int y, int size);
	BOOL AttachMicServer(CMicInterface *pMic);
	static void JoystickEH(UINT event, long x, long y, DWORD data);
	void MoveCtrl(int x, int y);
	void ShowCtrl(int nCmdShow);

protected:
	CPoint			m_nSteps;					
	int				m_stepTimer;
	CMicInterface	*m_pMic;
	void HandleJoystickEvents(UINT event, long x, long y);

private:
};


//These 2 functions ONLY are exported by the this dll
extern "C" DllExport void *CreateXYStageControl3D(void);
extern "C" DllExport void DestroyXYStageControl3D(void *XYStage);



#endif
