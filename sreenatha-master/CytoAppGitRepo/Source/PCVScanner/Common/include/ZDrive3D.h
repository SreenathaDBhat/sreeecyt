

/*
 *		ZDrive3D.h	M.Gregson
 *
 *
 */

#ifndef ZDRIVE3D_H
#define ZDRIVE3D_H

#include "../MicServer/MicServer.h"
#include "MicInterface.h"
#include "AutoScope.h"
#include "MicroErrors.h"

#include <D3DControlLOD.h>
#include <d3dctrl.h>
#include <JoystickWnd.h>

class CMicInterface;

// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif

class DllExport ZDrive3D : public ZDrive3DLOD
{
public:
	JoystickWnd		m_joyWnd;

	ZDrive3D();
	~ZDrive3D();
	BOOL InitCtrl(CWnd* pParentWnd, void *d3dctrl, int x, int y, int size);
	BOOL AttachMicServer(CMicInterface *pMic); 
	static void JoystickEH(UINT event, long x, long y, DWORD data);
	void MoveCtrl(int x, int y);
	void ShowCtrl(int nCmdShow);

protected:
	CEdit*			Tstepsize;
	int				m_nStepsZ;					
	int				m_stepTimer;
	float			m_stepsize;
	CMicInterface	*m_pMic;

	void HandleJoystickEvents(UINT event, long x, long y);
	void ChangeStepsize(long y);
};

//These 2 functions ONLY are exported by the this dll for load on demand
extern "C" DllExport void *CreateZDrive3D(void);
extern "C" DllExport void DestroyZDrive3D(void *zdrive);

#endif 
