/*
 * barcode_ff.h
 *
 * Defines the external interface to the new fileformats DLL.
 *
 * Key barcode declarations for the fileformats DLL.
 *
 * This header contains the required parts of barcode.h - see barcode.h for comments
 *
 * Copyright Applied Imaging corp. 2000
 * Created: 27April2000
 *
 * 
 *
 * Mods:
 *
 *
 *
 */

//-----------------------------------------------------------------------------------
//
//	BARCODE :
//
#define BARCODEMAGIC				0xbaaac0de	// magic number stored at start of barcode file 
#define BARCODE_FILE_VERSION		2			// file format version number stored in barcode file 

#define MAX_BARCODE_FLUORS			9			// Counterstain + 8 possible fluors (256 colours) 

// old version 1 data structure
#include <barcodever1.h>

// new dynamic version 2 structure
typedef struct barcode_type{
	int nfluors;								// Number of fluorochromes used 
	int *fluorID;								// probe image fluorochrome component IDs 
	Canvas **fluorRawCanvas;					// pointers to individual fluorochrome  raw image canvases 
	Uchar **fluorIm;							// pointers to individual fluorochrome raw images 
	Uchar **fluorIm2;							// background subtracted 
	struct object ***fluorList;					// lists of fluor objects 
	struct object **axisList;					// list of chromosome central axes 
	short *Coraxis;								// axis manual correction flags 
	short *objtype;								// CHROMOSOME, OVERLAP, NUCLEUS etc 
	int nobjs;									// Number of separate objects 
	int *fluorThresh;							// threshold values for each fluor
	int nclass;									// number of descernable classes 
	xy_offset *fluorOffset;						// x and y offset of fluor images 
	int registration_ok;						// true if auto-registration succeeds
	int chrom_halfwidth;						// halfwidth of typical chromosome 
	int rawWidth;								// width of all raw images 
	int rawHeight;								// height of all raw images 
	int *fluorMin;								// min fluor intensity
	int *fluorMax;								// max fluor intenisty
	int *fluorNorm;								// Normalisation factors 
	RGB *fluorColour;							// fluor colour in kary or met 
	short *fluorGamma;							// fluor gamma in kary or met 
	short *fluorDisplay;						// fluor displayed in kary or met 
	short *fluorSelect;							// fluor selected in kary or met 
	LUT_Triple **fluorLut;						// fluor display LUTS 
	int *fluorBackgroundMean;					// Mean of background fluorescence 
	float *fluorGranularity;					// fluorochrome hybridisation granularity 
	float *fluorIntensityVariance;				// coeff of  variation of fluorochrome painting 
	float *fluorDynamic;						// Dynamic of Fluorescence 
	int nchromosomes;							// number of chromosome objects 
	float banding_strength;						// Counterstain banding strength approx to bands per unit length 
	int chrom_length;							// average chomosome length 
	float chrom_size_factor;					// ave chrom length / ave chrom width 

	//	Version 2 includes support for flexible karyotyping
	int MaxObjs;								// Current number of objects allocated 
} BARCODE;
