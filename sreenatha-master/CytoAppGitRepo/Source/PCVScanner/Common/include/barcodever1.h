
// barcodever1.h	M.Gregson	
//					Version 1 barcode structure
//					This have been replaced by dynamic structures in version 2 onwards
//					**** This file should only be included into barcode.h ****

#ifndef BARCODEVER1_H
#define BARCODEVER1_H


typedef struct barcodever1_type{
	int nfluors;								/* Number of fluorochromes used */
	int fluorID[MAX_BARCODE_FLUORS];			/* probe image fluorochrome component IDs */
	Canvas *fluorRawCanvas[MAX_BARCODE_FLUORS];	/* pointers to individual fluorochrome */
												/* raw image canvases */
	Uchar *fluorIm[MAX_BARCODE_FLUORS];			/* pointers to individual */
												/* fluorochrome raw images */
	Uchar *fluorIm2[MAX_BARCODE_FLUORS];		/* background subtracted */
	struct object *fluorList[MAX_BARCODE_FLUORS][DEFAULT_MAXOBJS];
												/* lists of fluor objects */
	struct object *axisList[DEFAULT_MAXOBJS];	/* list of chromosome central axes */
	short Coraxis[DEFAULT_MAXOBJS];				/* axis manual correction flags */
	short objtype[DEFAULT_MAXOBJS];				/* CHROMOSOME, OVERLAP, NUCLEUS etc */
	int nobjs;									/* Number of separate objects */
	int fluorThresh[MAX_BARCODE_FLUORS];		/* threshold values for each fluor*/
	int nclass;									/* number of descernable classes */
	xy_offset fluorOffset[MAX_BARCODE_FLUORS];	/* x and y offset of fluor images */
	int registration_ok;						/* true if auto-registration succeeds */
												/* or auto-registration not requested */
	int chrom_halfwidth;						/* halfwidth of typical chromosome */
	int rawWidth;								/* width of all raw images */
	int rawHeight;								/* height of all raw images */
	int fluorMin[MAX_BARCODE_FLUORS];
	int fluorMax[MAX_BARCODE_FLUORS];
	int fluorNorm[MAX_BARCODE_FLUORS];			/* Normalisation factors */
	RGB fluorColour[MAX_BARCODE_FLUORS];		/* fluor colour in kary or met */
	short fluorGamma[MAX_BARCODE_FLUORS];		/* fluor gamma in kary or met */
	short fluorDisplay[MAX_BARCODE_FLUORS];		/* fluor displayed in kary or met */
	short fluorSelect[MAX_BARCODE_FLUORS];		/* fluor selected in kary or met */
	LUT_Triple fluorLut[MAX_BARCODE_FLUORS][256];/* fluor display LUTS */
	int fluorBackgroundMean[MAX_BARCODE_FLUORS];/* Mean of background fluorescence */
	float fluorGranularity[MAX_BARCODE_FLUORS];	/* fluorochrome hybridisation granularity */
	float fluorIntensityVariance[MAX_BARCODE_FLUORS];	
												/* coeff of  variation of fluorochrome painting */
	float fluorDynamic[MAX_BARCODE_FLUORS];		/* Dynamic of Fluorescence */
	int nchromosomes;							/* number of chromosome objects */
	float banding_strength;						/* Counterstain banding strength */
												/* approx to bands per unit length */
	int chrom_length;							/* average chomosome length */
	float chrom_size_factor;					/* ave chrom length / ave chrom width */
} BARCODEver1;


#endif /* BARCODEVER1_H */

