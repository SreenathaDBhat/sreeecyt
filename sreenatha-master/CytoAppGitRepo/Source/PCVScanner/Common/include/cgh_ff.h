/*
 * cgh_ff.h
 *
 * Defines the external interface to the new fileformats DLL.
 *
 * This header contains the required parts of cgh.h - see cgh.h for comments
 *
 *
 * Copyright Applied Imaging corp. 2000
 * Created: 27April2000
 *
 * 
 *
 * Mods:
 *	MC	26/Jan/01	Changed to latest CGH structure, added CGH_TWOTHRESHOLDS defn
 *	MG	20Oct04	-	Added chnages for HRCGH2 - from cgh.h
 *
 *
 *
 */

#define CGH_CHROMLEN_PROFS	// New chromosome length profiles for .rats and .profs file.		//SN08Aug00
#define CGH_TWOTHRESHOLDS	// Stores hhigh and low thresholds in cgh->fixedThresholds.			//SN09Oct00


/* current version */
#define CGH_VERSION	4				// RH-01

#ifdef CGH_CHROMLEN_PROFS
#define CGH_STATSVERSION	2		//SN08Aug00
#define CGH_PROFSVERSION	2
#endif


#define CGHMAGIC	    0xbabaface	/* magic number stored at start of image?.cgh file */
#define RATMAGIC	    0xabadcafe	/* magic number of ratio canvas file */
#define THRMAGIC	    0xdaffdaff	/* magic number of user/defaults/cghthresh.defs file */
#define CGHSTATSMAGIC   0xadeafdad	/* magic number of cgh.stats files */			// RH-01 moved to here from cghcanvas.cpp
#define CGHPROFSMAGIC   0xffaaaaff	/* magic number of cgh.profs files */			// RH-01 and MG
#define CGHOPT1MAGIC	0xface1dad	/* magic number of StdRefInt.defs files */		// RH-01
#define CGHOPT2MAGIC	0xface2dad	/* magic number of StdRefInt.defs files */		// RH-01


#define CGH_FILE_ACCESS 00664
#define CGH_LOCKFILE	".CGHLOCK"

#define BACKSUB_HALFWIDTH 50		/* halfwidth of background subtraction filter */
#define MAXOFFSET 10				/* maximum image registration offset */
#define MAXCGHCELLS 100				/* maximum number of cells on cgh slide */

#define MAXCGHMEAS 12				/* number of CGH measurements */

#define CGH_WORK	0				/* not processed yet */
#define CGH_PASS	1				/* values stored in cell cgh.status files */
#define CGH_FAIL	-1				/* not suitable for CGH analysis */

/* cgh processing status */
#define CGH_PROC_OK		1			/* cgh processing ok */
#define CGH_PROC_LOAD_FAIL	2		/* cgh processing image loading failed */
#define CGH_PROC_SAVE_FAIL	3		/* cgh processing image saving failed */
#define CGH_PROC_MALLOC_FAIL	4	/* cgh processing malloc failure */
#define CGH_PROC_REGISTER_FAIL	5	/* cgh processing image registration failure */
	
/* default CGH measurement thresholds : 
   modified by MG 19Mar96 to cope with new background subtraction technique */
#define MAXGRANULARITY 0.8f
#define MAXVARIANCE 45.0f
#define MINDYNAMIC 1.8f
#define MINBANDSTRENGTH 8.0f
#define MINSIZEFACTOR 2.5
#define MINCHROMOSOMES 20
#define MINMEDIANINTENSITY 40

#ifdef CGH_CHROMLEN_PROFS
/* New uniform profile length and centromere position for CGH standard references */
#define CGH_UNIFORMPROFSIZE		1000
#define CGH_UNIFORMCENT			 500
#endif

// Slide average flags  used by cgh->showslideprof
#define CGH_SLIDEON		1
#define CGH_CONFON		2
#define CGH_CONF95		4		// From now on only used to read CGH version 1, 2 and 3		// RH-01
#define CGH_ALL_CHROMS	8		// Display all ratio profiles of all cells of the slide		// RH-11

#define CGH_CONF950   950	/* 95.0% */ 
#define CGH_CONF990   990	/* 99.0% */
#define CGH_CONF995   995	/* 99.5% */
#define CGH_CONF999   999	/* 99.9% */
#define CGH_CONF9999 9999	/* 99.99% */	// RH-16

// Dynamic Standard Reference Intervals "CGHSTDREF"
//Max title name length of Dynamic Standard Reference Intervals (cllist.h CLCLASSNAMELENGTH is internal) // RH-01
#define CGH_STDREF_NAMELENGTH 30					// RH-00 Might not be needed at all for the cgh stuff					
// Error codes for barcode classifier functions		// RH-00 Not all are used yet
#define CGH_STDREF_OK						  1	// used
#define CGH_STDREF_ERR					  	  0	// used

#define CGH_STDREF_DOESNOTEXIST				 -1	// used
#define CGH_STDREF_ERROPENFILE				 -2	// used
#define CGH_STDREF_ERRREADFILE				 -3 // used
#define CGH_STDREF_ERREOF					 -4 // used 
#define CGH_STDREF_ERRWRITEFILE				 -5 // used 

#define CGH_STDREF_ERRNOTREFFILE			-10 // used
#define CGH_STDREF_ERRBADCLASSHEADER		-12 // used

#define CGH_STDREF_ERRBADSIZE				-30 // used
#define CGH_STDREF_ERRBADNPT				-31
#define CGH_STDREF_ERRBADBUFF				-32

#define CGH_STDREF_ERRNOTENOUGHTRAINDATA	-40	// used
#define CGH_STDREF_ERRNOTENOUGHCLASSDATA	-43
#define CGH_STDREF_ERRBADCLASSIFIER			-45
#define CGH_STDREF_ERRNOCELLHEADER			-46
#define CGH_STDREF_ERREMPTYCHROMDATA		-47
#define CGH_STDREF_ERRNOTEMPLATE			-48 // used

#define CGH_STDREF_ERRMALLOCFAIL			-50

#ifndef cell_file_name
typedef TCHAR cell_file_name[255];	/* for routines calling get_CGH_status_list */
#endif

typedef unsigned char Uchar;

/* CGH locals */

#pragma pack(2)

typedef struct xy_offset_type{		/* image registration offset structure */
	int x;
	int y;
} xy_offset;


/*
 * CGH ratio graph canvas data property list
 */
typedef struct Cratgraphdata_type{
	SMALL		size;				/* size of this structure in bytes */
	float		minratio;			/* min ratio clip - default = 0.5 */
	float		maxratio;			/* max ratio clip - default = 1.5 */
	int			maximised;			/* flag, 0=default (0.5 to 1.5) display, 1=show (minratio to maxratio) */
	int 		nprofs;				/* number of profiles slidemean based on */
	float		*slidemean;			/* temp attachment for slide mean ratios */
/* Replaced by the below lines													// RH-01
	float		*loconf95;			 * temp attachment for confidence intervals * 
	float		*hiconf95;	
	float		*loconf99;
	float		*hiconf99;
*/
	float		*loConf;			// temp attachment for confidence intervals
	float		*hiConf;	
	float		*loThres;			// temp attachment for threshold intervals
	float		*hiThres;
	int			*unbalanced; 		// temp attachment displaying where confidence intervals
									// do not overlap with threshold intervals.
									// Old saved records are reallocated to include this new 
								    // field (via create_shortcuts in cghfile.c) */

} Cratgraphdata;


typedef struct statDistType {// Basics representing a normal distribution
		int nobs;
		float mean;
		float stdev;
} StatDist;

typedef struct stdRefIntTypeClass {	// Dynamic Standard Reference Intervals for a chromosome class.
		int nprofs;
		int profsize;
		int centpos;
		StatDist *statDist;			// List 0 .. profsize-1 of statDists allocated when needed
} StdRefIntClass;

#ifdef CGH_CHROMLEN_PROFS
typedef enum e_CGHloadratio_type {	// Return for function load_ratio_stats			//SN06Aug00
	LOADRATIO_ERROR, LOADRATIO_OK, LOADRATIO_VER_1_RATS
} CGHLoadRatio;
#endif

// ----------------------------------------------------------------------------------
// Old versions of cgh structure - needed for access old format files
//-----------------------------------------------------------------------------------

typedef struct cghver1_type{		// version 1 cgh structure
#include <cghver1.h>
}CGHver1;

//-----------------------------------------------------------------------------------

typedef struct cghver2_type{		// version 2 cgh structure

	// Version 2 includes new bits and pieces to support
	// the 4th fluorochrome - used as a DAPI classification aid

#include <cghver2.h>
}CGHver2;

//-----------------------------------------------------------------------------------

typedef struct cghver3_type{		// version 3 cgh structure

	// Version 3 introduced dynamic arrays - so the cgh structure no longer 
	// includes the array elements - just a pointer to the list
	// this requires a completely new method of reading and writing the structure
	// It also includes support for flexible karyotyping

#include <cghver3.h>
}CGHver3;

//-----------------------------------------------------------------------------------

typedef struct cgh_type{			// version 4 cgh structure

	// Note the size of this structure is 472 (reported by sizeof(CGH)   // RH-08

	// The version 4 stuff for High Resolution CGH is added at the buttom
	// All declarations above this are identical with version 3.

	Canvas *ratkar_canvas;				// ratio profile main canvas 
	Canvas *ratanno_canvas;				// annotation for above 
	Canvas **classptr;					// list chromosome class canvases 
	int *selected;						// list class selection flags 
	int modified;						// modify flag 
	DDGS *dg;							// display sstructure 
	int showchromprof;					// profile visibility flags 
	int showcellprof;			
	int showslideprof;
	int active;							// canvas active - in main window 
	Cselectdata box;					// box selector for interaction 
	int filegroupid;					// filegroup and image identifiers 
	int imageid;						// used by copy and paste routines 

	int csID;							// probe image fluorochrome component IDs 
	int refID;
	int testID;

	Canvas *csRawCanvas;				// pointers to inividual fluorochrome 
	Canvas *refRawCanvas;				// raw image canvases 
	Canvas *testRawCanvas;

	Uchar *csIm;						// pointers to individual 
	Uchar *refIm;						// fluorochrome raw images 
	Uchar *testIm;

	Uchar *csIm2;						// background subtracted with 
	Uchar *refIm2;						// structure element size 10 

	struct object **csList;				// list of counterstain objects 
	struct object **refList;			// list of reference objects 
	struct object **testList;			// list of test objects 
	struct object **ratioList;			// list of test/ref ratio objects 
	struct object **axisList;			// list of chromosome central axes 
	short *Coraxis;						// list axis manual correction flags 

	short *objtype;						// list CHROMOSOME, OVERLAP, NUCLEUS etc 

	int nobjs;							// Number of separate objects     // Same value as kcont->maxnumber says RH

	int csThresh;						// Automatically calculated 
	int refThresh;						// threshold values for each 
	int testThresh;						// fluorochrome 

	xy_offset refOffset;				// x and y offset of reference image 
										// from counterstain image 
	xy_offset testOffset;				// x and y offset of test image 
										// from counterstain image 
	int registration_ok;				// true if auto-registration succeeds 
										// or auto-registration not requested 

	int chrom_halfwidth;				// halfwidth of typical chromosome 

	int rawWidth;						// width of all raw images 
	int rawHeight;						// height of all raw images 

	int csMin, csMax, csNorm;			// Normalisation factors 
	int refMin, refMax, refNorm;
	int testMin, testMax, testNorm;

	float testGranularity;				// Test and reference fluorochrome 
	float refGranularity;				// hybridisation granularity 

	float testIntensityVariance;		// Test and reference coefficient of 
	float refIntensityVariance;			// variation of fluorochrome painting 

	int csBackgroundMean;				// Mean of background fluorescence 
	int refBackgroundMean;
	int testBackgroundMean;

	float testDynamic;					// Dynamic of Fluorescence 
	float refDynamic;

	int nchromosomes;					// number of chromosome objects 

	float banding_strength;				// Counterstain banding strength 
										// approx to bands per unit length 

	int chrom_length;					// average chomosome length 

	float chrom_size_factor;			// ave chrom length / ave chrom width 

	int ratlow;							// CGH ratio cutoffs 
	int rathigh;


	// Version 2 includes new bits and pieces to support
	// the 4th fluorochrome - used as a DAPI classification aid

	int fluo4flag;						// flag 1=4th fluorochrome used for classification
	int fluo4ID;						// probe ID of 4th fluo
	Canvas *fluo4RawCanvas;				// raw image canvas
	Uchar *fluo4Im;						// raw image
	struct object **fluo4List;			// list of 4th fluorchrome objects
	short *fluo4Class;					// list which classes are marked with 4th fluor
	int fluo4Thresh;					// threshold used for registration
	xy_offset fluo4Offset;				// x and y offset of test image 


	//	Version 3 includes support for flexible karyotyping

	int NumOfClasses;				// Current number of classes allocated
	int MaxObjs;					// Current number of objects allocated 
	void *TemplatePtr;				// Species template pointer

	
	// Version 4 includes High Resolution CGH 

	int RHtest;						// Internal RH test variable for development purposes

	int directLabeling;				// Flag: Direct labeling of flourochromes are used. Default is NOT used.
									// Temporarilly turned un until the GUI button appears in cghfile.c
		
	int cot1Correction;				// Flag: Estimate level of repetitive sequences by
									// measuring fluorochrome centromere intensity
	float refRepeatsPercent;		// Estimated ref. DNA level of repetitive sequences
	float testRepeatsPercent;		// Estimated test DNA level of repetitive sequences

	int useNormProf;				// Flag: Use refNormProf,testNormProf, refNormProfClass 
	int refNormProf, testNormProf;	// Measured level. Normalization can then be based on profile values
	int *refNormProfClass;			// List of individual measured class reference levels. Especially for used for sex chromosomes.
									// Data for a chromosome class is stored in refNormProfClass[chromClass-1] etc.
									// For species with X and Y chromosomes it is essential to know if reference fluorescence
									// for X and Y is lower than the autosomes. 

	int handleMissingValues;		// Flag: Handle missing ratio profile values (display and statistical purpose)

	
	int scaleByMode;				// Use mode instead of median as normalization scale
									// when computing ratios
	int *normalClass;				// List flagging which classes are used for normalization
									// Data for a chromosome class is stored in normalClass[chromClass-1] etc.

	int skipUnreliable;				// Flag: Drop unreliable pixel andunreliable mean profile values
		
	int confLevel;					// Used level of confidence intervals (see CGH_CONF950 etc.)

	int showThresholds;				// Flag: Show ratio thresholds, fixed or not fixed 
	int useStdRefInt;				// Flag: Use Standard Reference Intervals otherwise fixed thresholds are used.

/////////////// Old
//	char stdRefInt[20];				// Filename of the last used standard reference intervals  (20 char's == 5 int's)
//	int autoScaleStdRefInt;			// Flag: Automatic scaling of stdRefInt to slide mean 	
//	float scaleStdRefInt;			// Value of manual or automatic set scaling of stdRefInt				
//	float fixedThresholds;			// Value of fixed ratio threshold level on each side of ratio 1
//	StdRefIntClass *stdRefIntClass;	// List of Dynamic Standard Reference Intervals for all chromosome classes.
////////////// End of old

  	int oldversion4;				// Flag: Is != 0 for old version 4 structures. When such an old set is read then the
   									// succeding 34 bytes of the cgh structure aren't usefull and have to be be cleared. 
 									// THIS ONE SHOULD BY NOW HAVE BEEN SAVED AS 0 FOR ALL CGH FILES,
 									// AND CAN BE REUSED FOR SOMETHING ELSE!!											// RH-08
 
   
   	int autoScaleStdRefInt;			// Flag: Automatic scaling of stdRefInt to slide mean 	
   	float scaleStdRefInt;			// Value of manual or automatic set scaling of stdRefInt				
   	float fixedThresholds;			// Value of fixed ratio threshold level on each side of ratio 1
 									// Replaced by the below independently set lowFixedThreshold and highFixedThreshold	// RH-07
 									// Old cases are updated when read and fixedThresholds is then set to -1.0			// RH-07
 									// THIS USER AND SLIDE OPTION SHOULD BY NOW HAVE BEEN SAVED AS -1 FOR ALL OPTION FILES,
 									// AND CAN BE REUSED FOR SOMETHING ELSE!!											// RH-08
   
	StdRefIntClass *stdRefIntClass;	// List of Dynamic Standard Reference Intervals for all chromosome classes.
	char *stdRefIntName;			// Striped down title name of the selected file
////int keepstdRefInt_free;			// NOT USED ANY MORE and it is only RH who have some files where it is turned on
///  maximized takes its place see below

	
	// 	Store  default_minratio and default_maxratio here to improve consistensy as all the other control variables 
	//	for profile display	settings are stored here.													// RH-03
	int maximised;					// Flag: Show all graphs as maximized.								// RH-04	
 	float default_minratio;			// Default profile graph limits used when graphs not maximised.
	float default_maxratio;			// I.e. ratgraph->data.maximised (often cratdata->maximised) is not set.
	

 // The below 4 lines are replaced by the new lowFixedThreshold and highFixedThreshold									// RH-07
 //	int optionsDiscrepancy;			// Flag: True when this cgh is on display outside main display and the options of the main
 //	                                // display were changed and saved. 
 //	int computationalDiscrepancy;	// Remember if the cells computional options is not applied to all cels
 //	int placeholder4;
 
	// the low and high settings are used in the RH 2.69 version (only used by RH) during year 2000 to 2004 
	float  lowFixedThreshold;		// NOW A PLACEHOLDER (3.6) The  low value of a fixed thresholds range.			// RH-07
 	float highFixedThreshold;		// NOW A PLACEHOLDER (3.6) The high value of a fixed thresholds range.			// RH-07
   
 //	int unitDistanceSampling;		// Use Unit Distance Sampling when sampling fluorochrome or ratio profiles.  // RH-08 was placeholder3
 //	int trimToLongerAxes;			// Generate longer axes during automatic trimming and
 //									// do a small hidden trimming of ratio and fluorochrome profiles afterwards. // RH-09 was placeholder2
 //	int placeholder1;
   
 	short unitDistanceSampling;		// Use Unit Distance Sampling when sampling fluorochrome or ratio profiles.  // RH-08 was  low bits of  placeholder3
 	short trimToLongerAxes;			// Generate longer axes during automatic trimming and
 									// do a small hidden trimming of ratio and fluorochrome profiles afterwards. // RH-09 was high bits of placeholder3
 	short fluoBackgroundMethod;		// Is == 0 for SuperSubtract, and is == 1 for MegaSimpleSubtract			 // RH-12 was  low bits of placeholder2 
 	short placeholder2b;																						 // RH-12 was high bits of placeholder2 
 	int   placeholder1;				// Can be changed to e.g. two shorts
									// Temporary statistics of loaded slides; needed because the program can load and
									// display up to two CGH cells (three images per cell) of different slides.
									// The statistics are updated by load_ratio_stats()	
	int nCghCells;					// Number of cells with CGH ratio profiles.


}CGH;

#pragma pack()