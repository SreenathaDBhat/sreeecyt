
// Version 1 CGH structure				M.Gregson	20Jun96
//
//	Note this file is included in the CGHver1 structure, defined in cgh.h 
//

//----------------------------------------------------------------------------------

	Canvas *ratkar_canvas;						// ratio profile main canvas 
	Canvas *ratanno_canvas;						// annotation for above 
	Canvas *classptr[DEFAULT_NUMOFCLASSES+1];	// chromosome class canvases 
	int selected[DEFAULT_NUMOFCLASSES+1];		// class selection flags 
	int modified;								// modify flag 
	DDGS *dg;									// display sstructure 
	int showchromprof;							// profile visibility flags 
	int showcellprof;			
	int showslideprof;
	int active;									// canvas active - in main window 
	Cselectdata box;							// box selector for interaction 
	int filegroupid;							// filegroup and image identifiers 
	int imageid;								// used by copy and paste routines 

	int csID;									// probe image fluorochrome component IDs 
	int refID;
	int testID;

	Canvas *csRawCanvas;						// pointers to inividual fluorochrome 
	Canvas *refRawCanvas;						// raw image canvases 
	Canvas *testRawCanvas;

	Uchar *csIm;								// pointers to individual 
	Uchar *refIm;								// fluorochrome raw images 
	Uchar *testIm;

	Uchar *csIm2;								// background subtracted with 
	Uchar *refIm2;								// structure element size 10 

	struct object *csList[DEFAULT_MAXOBJS];		// list of counterstain objects 
	struct object *refList[DEFAULT_MAXOBJS];	// list of reference objects 
	struct object *testList[DEFAULT_MAXOBJS];	// list of test objects 
	struct object *ratioList[DEFAULT_MAXOBJS];	// list of test/ref ratio objects 
	struct object *axisList[DEFAULT_MAXOBJS];	// list of chromosome central axes 
	short Coraxis[DEFAULT_MAXOBJS];				// axis manual correction flags 

	short objtype[DEFAULT_MAXOBJS];				// CHROMOSOME, OVERLAP, NUCLEUS etc 

	int nobjs;									// Number of separate objects 

	int csThresh;								// Automatically calculated 
	int refThresh;								// threshold values for each 
	int testThresh;								// fluorochrome 

	xy_offset refOffset;						// x and y offset of reference image 
												// from counterstain image 
	xy_offset testOffset;						// x and y offset of test image 
												// from counterstain image 
	int registration_ok;						// true if auto-registration succeeds 
												// or auto-registration not requested 

	int chrom_halfwidth;						// halfwidth of typical chromosome 

	int rawWidth;								// width of all raw images 
	int rawHeight;								// height of all raw images 

	int csMin, csMax, csNorm;					// Normalisation factors 
	int refMin, refMax, refNorm;
	int testMin, testMax, testNorm;

	float testGranularity;						// Test and reference fluorochrome 
	float refGranularity;						// hybridisation granularity 

	float testIntensityVariance;				// Test and reference coefficient of 
	float refIntensityVariance;					// variation of fluorochrome painting 

	int csBackgroundMean;						// Mean of background fluorescence 
	int refBackgroundMean;
	int testBackgroundMean;

	float testDynamic;							// Dynamic of Fluorescence 
	float refDynamic;

	int nchromosomes;							// number of chromosome objects 

	float banding_strength;						// Counterstain banding strength 
												// approx to bands per unit length 

	int chrom_length;							// average chomosome length 

	float chrom_size_factor;					// ave chrom length / ave chrom width 

	int ratlow;									// CGH ratio cutoffs 
	int rathigh;

//----------------------------------------------------------------------------------
