
// Version 2 CGH structure				M.Gregson
//
//	Note this file is included in the CGHver2 structure, defined in cgh.h 
//

//------------------------------------------------------------------------------------------

#include <cghver1.h>					// start with version 1 code

// Version 2 includes new bits and pieces to support
// the 4th fluorochrome - used as a DAPI classification aid

	int fluo4flag;								// flag 1=4th fluorochrome used for classification
	int fluo4ID;								// probe ID of 4th fluo
	Canvas *fluo4RawCanvas;						// raw image canvas
	Uchar *fluo4Im;								// raw image
	struct object *fluo4List[DEFAULT_MAXOBJS];	// list of 4th fluorchrome objects
	short fluo4Class[DEFAULT_NUMOFCLASSES+1];	// which classes are marked with 4th fluor
	int fluo4Thresh;							// threshold used for registration
	xy_offset fluo4Offset;						// x and y offset of test image 

//------------------------------------------------------------------------------------------