
// Version 3 CGH structure				M.Gregson 
//
// Note this file is included in the CGHver3 structure, defined in cgh.h 
//
// NOTE This structure cannot be made by adding a few new members to the end of the 
// previous structures as it is totally rewrittes because dynamic arrays are used. 
//
//
// Modifications:
//	RH-01   Sep99	The current CGH structure is made to a CGHver3 structure as new
//					structure members are added.
//
//------------------------------------------------------------------------------------------
//	
//
	//version 3 introduced dynamic arrays - so the cgh structure no longer 
	//includes the array elements - just a pointer to the list
	//this requires a completely new method of raeding and writing the structure

	Canvas *ratkar_canvas;				// ratio profile main canvas 
	Canvas *ratanno_canvas;				// annotation for above 
	Canvas **classptr;					// list chromosome class canvases 
	int *selected;						// list class selection flags 
	int modified;						// modify flag 
	DDGS *dg;							// display sstructure 
	int showchromprof;					// profile visibility flags 
	int showcellprof;			
	int showslideprof;
	int active;							// canvas active - in main window 
	Cselectdata box;					// box selector for interaction 
	int filegroupid;					// filegroup and image identifiers 
	int imageid;						// used by copy and paste routines 

	int csID;							// probe image fluorochrome component IDs 
	int refID;
	int testID;

	Canvas *csRawCanvas;				// pointers to inividual fluorochrome 
	Canvas *refRawCanvas;				// raw image canvases 
	Canvas *testRawCanvas;

	Uchar *csIm;						// pointers to individual 
	Uchar *refIm;						// fluorochrome raw images 
	Uchar *testIm;

	Uchar *csIm2;						// background subtracted with 
	Uchar *refIm2;						// structure element size 10 

	struct object **csList;				// list of counterstain objects 
	struct object **refList;			// list of reference objects 
	struct object **testList;			// list of test objects 
	struct object **ratioList;			// list of test/ref ratio objects 
	struct object **axisList;			// list of chromosome central axes 
	short *Coraxis;						// list axis manual correction flags 

	short *objtype;						// list CHROMOSOME, OVERLAP, NUCLEUS etc 

	int nobjs;							// Number of separate objects 

	int csThresh;						// Automatically calculated 
	int refThresh;						// threshold values for each 
	int testThresh;						// fluorochrome 

	xy_offset refOffset;				// x and y offset of reference image 
										// from counterstain image 
	xy_offset testOffset;				// x and y offset of test image 
										// from counterstain image 
	int registration_ok;				// true if auto-registration succeeds 
										// or auto-registration not requested 

	int chrom_halfwidth;				// halfwidth of typical chromosome 

	int rawWidth;						// width of all raw images 
	int rawHeight;						// height of all raw images 

	int csMin, csMax, csNorm;			// Normalisation factors 
	int refMin, refMax, refNorm;
	int testMin, testMax, testNorm;

	float testGranularity;				// Test and reference fluorochrome 
	float refGranularity;				// hybridisation granularity 

	float testIntensityVariance;		// Test and reference coefficient of 
	float refIntensityVariance;			// variation of fluorochrome painting 

	int csBackgroundMean;				// Mean of background fluorescence 
	int refBackgroundMean;
	int testBackgroundMean;

	float testDynamic;					// Dynamic of Fluorescence 
	float refDynamic;

	int nchromosomes;					// number of chromosome objects 

	float banding_strength;				// Counterstain banding strength 
										// approx to bands per unit length 

	int chrom_length;					// average chomosome length 

	float chrom_size_factor;			// ave chrom length / ave chrom width 

	int ratlow;							// CGH ratio cutoffs 
	int rathigh;

	// Version 2 includes new bits and pieces to support
	// the 4th fluorochrome - used as a DAPI classification aid

	int fluo4flag;						// flag 1=4th fluorochrome used for classification
	int fluo4ID;						// probe ID of 4th fluo
	Canvas *fluo4RawCanvas;				// raw image canvas
	Uchar *fluo4Im;						// raw image
	struct object **fluo4List;			// list of 4th fluorchrome objects
	short *fluo4Class;					// list which classes are marked with 4th fluor
	int fluo4Thresh;					// threshold used for registration
	xy_offset fluo4Offset;				// x and y offset of test image 

	//	Version 3 includes support for flexible karyotyping

	int NumOfClasses;				// Current number of classes allocated
	int MaxObjs;					// Current number of objects allocated 
	void *TemplatePtr;				// Species template pointer

//------------------------------------------------------------------------------------------