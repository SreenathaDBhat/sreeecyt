    /*
     * chromanal.h		Jim Piper		February 1984
     *
     * header file for chromosome analysis programs
     *
     *	28 May 2003		MC      Taken 3 'shorts' from EXPAND0 for met anno dx,dy and label
	 *							
	 *	25 Aug 2000		SN		Changed MIN_CELL_COUNT from 3 to 2.
	 *	30 Jun 1999		JMB		Commented out MAXOBJS, MAXCLASS. 
	 *							Nothing should be using these now.
	 *	25 Jun 1999		SN		New unique user classifier ids.
	 *	06 Apr 1999		MG		Started adding #defines for flexible karyotyper
	 *
     *	11 Aug 1997		SN		Added #defines for barcode classifiers,
     *							BARCODE_DEF_CLASS, BARCODE_USER_CLASS,
     *							NOFUSER_CLASS, NOFBARCODE_CLASS. 
     *	4/17/97	BP:		BP		Replace class with chromclass.
     *	11/26/96		BP:		Change cllik 'class' member to 'chromclass to compile
     *							with C++
     *
     * 	17 Feb 1995		MG		Moved classifier identifier defines here from redkar.h
     *
     *  17 Jun 1994     CAJ     added 2 more features to chromplist and increased
     *							MAXDIM to 30.
     *  16 Jun 1994     CAJ     changed normaliser to use floating point math,
     *                          also removed struct classifier.
     *	12 Feb 1993				Expansion slot EXPAND1 now filled by offsetx,offsety
     *							& 4 bytes of EXPAND0 now taken up by fscale
     *	14 Jul 1992		MC		added ; to stop warning messages from compiler
     *							(member assoc of structure cellid)
     *
     *	27 Jan 1991		jimp	Added #define FLEXIBLE_SCREEN 7 to the status
     *							return constants (which are actually used for
     *							variable enhancement set-up).
     *	 7 Jan 1991		CAS		Added lock byte to classcont struct
     *  17 Feb 1989		SEAJ	Define constants for status return into chromlyse.
     *	09 Feb 1987		jimp	Define bits with plist->dispmark for permanent
     *							and temporary highlighting.
     *	05 Feb 1987		jimp	NSTACKPOS to 80, moved here from karysubs[12].c
     *							new comment for plist->dispopts
     *	 5 Jan 1987		CAS/jimp	Maxchild to 30 (was 10) 
     *								classcont->chcl[] size = maxchild (was 20)
     *	11 Nov 1986		jimp	Symbolic defines for object types
     *	28 Oct 1986		CAS		Increment MAXOBJS to 250
     *	17 Oct 1986		CAS		Increment MAXOBJS to 120
     */
    
    #ifndef CHROMANAL_H
    #define CHROMANAL_H
    
    /* classifier identifiers upto WinCV v2
    	- all user classifiers have identifiers in the range
						USER_CLASS         to USER_CLASS+NOFUSER_CLASS-1
    	- all user barcode classifiers have identifiers
						BARCODE_USER_CLASS to BARCODE_USER_CLASS+NOFBARCODE_USER_CLASS-1
    */
	#define NO_CLASS			  0 /* indicates no classifier loaded */
    #define MAN_CLASS			 99	/* manual classification */
    #define SHORT_CLASS			100	/* old classifier file format */
    #define LONG_CLASS			101	/* old classifier file format */
    #define RBAND_CLASS			102	/* old classifier file format */
    #define CGHDAPI_CLASS		103	/* NEW classifier file format */
    #define USER_CLASS			105	/* NEW classifier file format */
    #define BARCODE_DEF_CLASS	500 /* Barcode classifier file format */
    #define BARCODE_USER_CLASS	550 /* Barcode classifier file format */
    
    #define NOFUSER_CLASS			200 /* Number of user classifiers allowed */
    #define NOFBARCODE_USER_CLASS	200 /* Number of barcode classifiers allowed */
	
	/* Unique Classifier identifiers to be used for new user classifiers from Flexible Karyotyper onwards,
	   based on hashing the classifier and species name together.

    	- new user standard classifiers have identifiers in the range
						 UNIQUE_USER_CLASS to   UNIQUE_USER_CLASS+NOFUNIQUE_USER_CLASS-1

    	- new user barcode  classifiers have identifiers in the range
						-UNIQUE_USER_CLASS to -(UNIQUE_USER_CLASS+NOFUNIQUE_USER_CLASS-1)
	*/
    #define UNIQUE_USER_CLASS				 1024	// First standard/barcode user classifier
    #define NOFUNIQUE_USER_CLASS			31741	// Number of standard/barcode user classifiers allowed

	/* Unique identifier for CGH Standard References to be used from High Res CGH release onwards.
	   This id will be present in EVERY default and user CGH Std Ref data and class file.
	*/
	#define UNIQUE_CGHSTDREF				  800
    
    /*
     * Object segmentation types
     */
    #define UNKNOWN		0
    #define CHROMOSOME	1
    #define COMPOSITE	2
    #define OVERLAP		3
    #define PIECE		4
    #define NUCLEUS		5
    #define NOISE		6
    #define BLOBB		7
    
	/*
	 *	New constants for Flexible Karyotyper
	 */
	#define	DEFAULT_NUMOFCLASSES	24
	#define	DEFAULT_MAXOBJS			250
	#define DEFAULT_MAXDIM			30

	#define MAX_NUMOFCLASSES		200
	#define SOFT_MAXOBJS			1000		//Soft limit - user warning threshold
    #define HARD_MAXOBJS            4000        // Absolute hard limit - if we hit this then the user is being a ninny
	#define NUMOFSELFEATURES		10			//Number of selected features computed during classifier generation
	#define NUMOFFEATURES			30			//Number of features measured in this CLASSVERSION, successor to MAXDIM
	#define CLASSVERSION			1			//Classifier/Feature version
	#define MIN_CHROM_COUNT			3			//Min number of chroms in a class for generating classifiers - soft limit
	#define MIN_CELL_COUNT			2			//Min number of cells in a training file before classifier is generated

    /*
     * maxima for classification programs - some of these will disappear
	 * forever when the flexible karyotyper id finished
     */
    #define MAXDIM		30
    //#define MAXCLASS	25
    //#define MAXOBJS		250
    #define MAXCHILD	30
    #define LRATIO		-2.0	/* ln(e**(-2)) */
    #define MAXCHAIN	3
    #define CUTOFF		4
    /*
     * parameters for profile extraction and straightening
     */
    #define MAXHALFWIDTH	64
    #define HALF_SLOPE_NHD	5
    /*
     * parameters for karyogram
     */
    #define NSTACKPOS		80
    /*
     * parameter for chromosome axis construction from skeleton
     */
    #define EXTENDTIP	7
    /*
     * parameters for status return to chromlyse.c
     */
    #define ERROR 0
    #define SEGMENT 1
    #define KARYOTYPE 2
    #define MANKARY 3
    #define COUNT 4
    #define EXIT 5
    #define QUIT 6
    #define FLEXIBLE_SCREEN 7


	// BASE WIDTH AND HEIGHT FOR COHU CAMERAS
	#define COHU_PIXEL_WIDTH   0.122  // 0.086 / 0.7 - for COHU
	#define COHU_PIXEL_HEIGHT  0.118  // 0.083 / 0.7 - for COHU
    #define COHU_IMAGE_WIDTH   768
	#define COHU_IMAGE_HEIGHT  576

    struct chromosome {
    	SMALL type;
    	struct intervaldomain *idom;	/* interval domain */
    	struct valuetable *vdom;	/* value domain */
    	struct chromplist *plist;	/* property domain */
    	struct object *assoc;		/* associated object */

    };
    
    /*
     * chromosome property list.  This includes three types of data :
     *	(1) feature measurements
     *	(2) processing history
     *	(3) correct (training) feature values
     */
    #pragma pack(2)
    struct chromplist {
    	SMALL size;		/* of this structure - ESSENTIAL for I/O */
    	SMALL number;		/* serial number in cell */
    	/*
    	 * undisputed scalar properties
    	 */
    	int area;
    	int mass;
    	int hullarea;		/* convex hull parameters */
    	int hullperim;		/* c.h. perimeter */
    	int rectarea;		/* minimum width rectangle parameters */
    	short rectwidth;
    	short rangle;		/* m.w. rectangle orientation */
    	/*
    	 * algorithm dependent measurements
    	 */
    	short length;
    	short width;
    	short angle;		/* orientation of axis */
    	short nsigchords;	/* significant chords of convex hull */
    	short cx;		/* centromere parameters */
    	short cy;
    	short cangle;
    	short clconf;		/* centromere line confidence */
    	short cindexa;		/* centromeric index (%) - area */
    	short cindexm;		/*      "        "    "  - mass */
    	short cindexl;		/*      "        "    "  - length */
    	short dispor;		/* 1 if to be drawn right way up,
    				  -1 if to be inverted, 0 if unset */
    	short disppos;		/* display position in class in karyotype,
    				   0 if not yet set */
    	short dispmark;		/* See defines below for bit interpretation */
    	short dispopts;		/* now used to indicate vertical offset */
        short nbands;       /* number of extrema in density profile */
        short nbindex;      /* ratio of smaller number of extrema in
                         half density profile to nbands (%) */
//    	short EXPAND0[5];	/* EXPANSION SLOT, previously [9] until fscale added */
//    	short EXPAND0[2];	/* EXPANSION SLOT, previously [5] until dx,dy and label for met anno needed */
		short EXPAND0[1];
		short keep_parent_site;	/* Frame parent site, needed to properly use the offsetx, offsety as the origin for these are based on this */
		short metannodx;    //
		short metannody;    //
		short metannolabel; //
    	float fscale;		/* object current scale - default = 1.0 */
    	SMALL otype;		/* object type :
    					1	chromosome
    					2	composite
    					3	overlap
    					4	piece
    					5	nucleus
    					6	spot noise (small unknown)
    					7	blob noise (large unknown) */
    	SMALL otconf;		/* object type confidence */
    	SMALL pnumber;		/* parent number if derived from composite,
    				   of one parent if joined from pieces */
    	int history[8];		/* processing history :
    					0	how derived from parent
    						1 - manual split of composite
    						2 - manual extract from overlap
    						4 - manually joined
    					1	how disposed of (e.g. split)
    						1 - manual split of composite
    						2 - manual extract from overlap
    						4 - manually joined
    					2	chromosome count of non-chromosome object
    					3   	second parent if a joined object
    					4	1 - object has been straightened or trimmed or reflected
							2 - as above but scale is held in fixedptscale - CV3.6 onwards
    					5	1-  object imported by field fusion
    					6   how derived from parent used by classifier trainer
    						1 - split
    						2 - overlap
    					remainder - to be determined */
    	short dgroup;		/* denver group */
    	short dgconf;
    	short pgroup;		/* paris group/class */
    	short pgconf;
    	short cvdd;		/* coefficient of variation of density profile */
    	short nssd;		/* Granum's NSSD of density profile */
    	short mdra;		/* ratio of mass c.i. to area c.i. */
    	short ddm2;		/* radius of gyration / length  of dd */
    	short wdd[6];		/* weighted density distribution parameters */
    	short mwdd[6];		/* wdd of 2nd moment profile parameters */
    	short gwdd[6];		/* wdd of profile of absolute gradient of dd */
    	
    	short offsetx;		/* previously EXPAND1[2] expansion slot now - */
    	short offsety;		/* object canvas offset in karyotype class canvas */
    	/*
    	 * correct values inserted by cytogeneticist
    	 */
    	int Cangle;		/* correct orientation */
    	int Ccx;		/* correct centromere location */
    	int Ccy;
    	int fixedptscale;	// used to be Ccangle - CV3.6 onwards fixedptscale holds fixed point scale of reflected, trimmed or straightened objects - see history[4]
    	SMALL Cotype;		/* correct object type */
    	SMALL Cdgroup;		/* correct group/class */
    	SMALL Cpgroup;



		////////NEW ONES - AK
		double pixel_X_micronsize;
		double pixel_Y_micronsize;
		double capture_objective;
		float reserve1;
		float reserve2;

    };
    #pragma pack()
    
    /*
     * plist->dispmark bits
     */
    #define PERM_MARK		01
    #define	TEMP_MARK_REQ	02
    #define TEMP_MARK_DONE	04
    
    
    
    /*
     * linear normaliser for feature normalisation
     */
    struct normaliser {
    	double A;	/* multiplier */
    	double B;	/* additive constant */
    };
    
    
    /*
     * structures for classification: first per chromosome, then per class
     */
    struct cllik {
    	int chromclass;		// Used to be 'class'
    	double lval;
    };
    
    struct chromcl {
    	int rawclass;
    	int trueclass;
    	int newclass;
    	int bestclass;
    	double moveval;
    	struct chchain *movechain;
    	struct cllik cllika[CUTOFF];
    };
    
    struct chchain {
    	struct chromcl *chrcl;
    	double accum_cost;
    	int nchildren;
     	int chromclass;		// Used to be 'class'
    	int bestchild;
    	struct chchain *child[MAXCHILD];
    };
    
    struct classcont {
    	unsigned char lock;
    	int n;
    	struct chromcl *chcl[MAXCHILD];
    };
    
    struct cellid {
    	SMALL type;			/* 110 cell identifier */
    	struct domain *idom;		/* = NULL */
    	struct value *vdom;		/* = NULL */
    	struct cellplist *plist;	/* contains slide ID and descriptions*/
    	struct object *assoc;		/* =NULL (; added by MC 14/7/92 ) */
    };
    
    struct cellplist {
    	SMALL size;			/* size of list */
    	char slideid[20]; 		/* slide identifier */
    	char doc[10];			/* date of culture */
    	char sex[2];			/* sex of specimen */
    	char stain[10];			/*  type of stain used */
    	char operatorid[20];		/* m/c operator */
    	char karyotype[40];		/* symbolic karyotype */
    };
    
    
    
    /*
     * object identification for karyogram rearrangement
     */
    struct kident {
    	int no;		/* chromosome number in object table */
    	int kclass;	/* measured from zero to MAXCLASS-1 */
    	int pos;	/* first, second, third, ... of this class */
    	int stackpos;	/* position in general object stack when required */
    };
    
    
    /*
     * class contents for karyogram rearrangement
     */
    struct kclasscont {
    	int n;		/* how many in this class */
    	int maxn;	/* high-water mark of table for this class */
    	int kno[MAXCHILD]; /* table of objects in class, -1 if unused entry */
    	int sp[MAXCHILD]; /* table of bottom line stack occupancy */
    };
    
    #endif /* CHROMANAL_H */
    
    
