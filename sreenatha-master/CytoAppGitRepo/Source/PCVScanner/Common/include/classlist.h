
/*
 * classlist.h
 * JMB July 2001
 */

#include "SerializeXML.h"

#ifndef CLASSLIST_H
#define CLASSLIST_H

// Defines for Class change mode
// i.e. what part(s) of the class have been changed
//      or has a class been added or deleted
#define CLASSCHANGE_NAME	0x0001
#define CLASSCHANGE_COLOR	0x0002
#define CLASSCHANGE_GROUP	0x0004
#define CLASSCHANGE_ADD		0x0008
#define CLASSCHANGE_DELETE	0x0010
#define CLASSCHANGE_PRE_GROUP 0x0020 // before the group value is changed


enum ClassListVersion
{
	CLASSLISTV1 = 0,
	CLASSLISTV1_5
};

#define CLASSLISTVERSION 2.0;

class ClassListItem
{

// ClassList is a friend so that it can access pNext etc.
friend class ClassList;

public:
	// this is used to categorize classlistitems into groups (for plotting)
	enum ClassGroup
	{
		CLASSGROUP_NONE = 0,		// does not belong to any group
		CLASSGROUP_PLOIDY_CONTROL,	// Ploidy assay control group
		CLASSGROUP_PLOIDY_TUMOR		// Ploidy assay tumor group
	};

protected:

	CString name;
	COLORREF colour;
	int count;
	double lbound;	// the lower bound of the ploidy peak
	double ubound;	// the upper bound of the ploidy peak
	int lboundBinIndex; // the lower bound bin index of the ploidy peak
	int uboundBinIndex; // the upper bound bin index of the ploidy peak
	ClassGroup classGroup; // which class group this classlistitem belongs to
	bool classRequired; // whether this class is required by the application/assay

	// State of the check box this item is associated with in a CClassesDlg.
	int checkState;

	// Virtual key-code for hot key associated with this class
	WORD hotKey;
	WORD hotKeyModifiers; // e.g. Ctrl, Alt etc.

	ClassListItem *pNext;

public:

	const char *GetName(){ return (PCSTR)name; }
	void SetName(const char string[]){ if (string) name = string; }

	COLORREF GetColour(){ return colour; }
	void SetColour(COLORREF c){ colour = c; }

	double GetLowerBound(){ return lbound; }
	void SetLowerBound(double input){ lbound = input; }

	double GetUpperBound(){ return ubound; }
	void SetUpperBound(double input){ ubound = input; }

	int GetLowerBoundBinIndex(){ return lboundBinIndex; }
	void SetLowerBoundBinIndex(int input){ lboundBinIndex = input; }

	int GetUpperBoundBinIndex(){ return uboundBinIndex; }
	void SetUpperBoundBinIndex(int input){ uboundBinIndex = input; }

	bool GetClassRequired(){ return classRequired; }
	void SetClassRequired(bool input){ classRequired = input; }

	int GetCount(){ return count; }
	void SetCount(int c){ count = c; }
	void IncrementCount(){ count++; }

	int GetCheckState(){ return checkState; }
	void SetCheckState(int state){ checkState = state; }
	void GetHotKey(WORD *pKey, WORD *pModifiers){ *pKey = hotKey; *pModifiers = hotKeyModifiers; }
	void SetHotKey(WORD key, WORD modifiers){ hotKey = key; hotKeyModifiers = modifiers; }

	ClassGroup GetClassGroup(){ return classGroup; }
	void SetClassGroup(ClassGroup group){ classGroup = group; }

	ClassListItem(){ colour = 0; count = 0; checkState = TRUE; 
					 hotKey = 0; hotKeyModifiers = 0; pNext = NULL; 
					 lbound = 0.0; ubound = 0.0; lboundBinIndex = -1; uboundBinIndex = -1; 
					 classRequired = false;
					 classGroup = CLASSGROUP_NONE;}	

	void XMLSerialize(CSerializeXML & ar);
};


class ClassList
{

protected:

	ClassListItem *pItems;
	CString fileName;

public:

	ClassListItem *AddItem();
	ClassListItem *AddItem(const char name[]);
	ClassListItem *GetItem(const char name[]);
	ClassListItem *GetItem(int index);
	int            GetItemIndex(const char name[]);							//SN05Oct01
	ClassListItem *GetItemFromHotKey(WORD key, WORD modifiers);
	void ZeroItemCounts();
	BOOL RemoveItem(int index);
	int CountItems();
	void Clear();

	void SetFileName(const char name[]){ fileName = name; }
	BOOL Save(bool bXMLFormat = false);
	BOOL Load();

	BOOL XMLSerialize(CSerializeXML & ar);

	ClassList(){ pItems = NULL; fileName = ""; }
	~ClassList();
	BOOL IsFileXMLFormat(LPCTSTR filePath);

};


#endif
