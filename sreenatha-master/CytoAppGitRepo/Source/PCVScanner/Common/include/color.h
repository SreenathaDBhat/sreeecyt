/* $Header: color.h,v 1.2 88/06/30 09:58:56 mikey Exp $ */

/* 
 * color.h - color definitions
 * 
 * Author:	Christopher A. Kent
 * 		Western Research Laboratory
 * 		Digital Equipment Corporation
 * Date:	Sun Dec 13 1987
 * Copyright (c) 1987 Christopher A. Kent
 */

/*
 * $Log:	color.h,v $
 *
 * Mods:
 *
 *	22Jan97	BP:	Reentrancy protection.
 *				Full function prototypes, if applicable.
 *
 * Revision 1.2  88/06/30  09:58:56  mikey
 * Handles CMY also.
 * 
 * Revision 1.1  88/06/30  09:10:53  mikey
 * Initial revision
 * 
 *---------------WinCV---------------------------------------------------------
 * MODS
 * SN 23Sep98 RGB struct needs packing for use within MFISH struct see mfish.h.
 *
 */


#ifndef _COLOR_H
#define _COLOR_H

#pragma pack(2)
typedef	struct _RGB {
	unsigned short r, g, b;
} RGB;
#pragma pack()

typedef	struct _HSV {
	float	h, s, v;	/* [0, 1] */
} HSV;

typedef struct _CMY {
	unsigned short c, m, y;
} CMY;

extern RGB	RGBWhite, RGBBlack;


//#ifndef _NO_PROTO

//#ifdef WIN32
/* defined in ProbeUtils\FluoColour.cpp */
#ifdef __cplusplus
extern "C" {
#endif
RGB WINAPI MixRGB(RGB r);
RGB	WINAPI MixHSV(HSV h);
RGB	WINAPI HSVToRGB(HSV h);
HSV	WINAPI RGBToHSV(RGB r);
float WINAPI RGBDist(RGB r);
RGB	WINAPI PctToRGB(float rr, float gg, float bb);
HSV	WINAPI PctToHSV(float hh, float ss, float vv);
RGB	WINAPICMYToRGB(CMY c);
CMY	WINAPIRGBToCMY(RGB r);

#ifdef __cplusplus
}
#endif
//#endif

//#ifdef i386
/* defined in colourpanel\flcolour.c */
/*RGB	MixRGB(RGB r);
RGB	MixHSV(HSV h);
RGB	HSVToRGB(HSV h);
HSV	RGBToHSV(RGB r);
float RGBDist(RGB r);
RGB	PctToRGB();
HSV	PctToHSV();
RGB	CMYToRGB(CMY c);
CMY	RGBToCMY(RGB r);
#endif

#else
RGB	MixRGB();
RGB	MixHSV();
RGB	HSVToRGB();
HSV	RGBToHSV();
float RGBDist();
RGB	PctToRGB();
HSV	PctToHSV();
RGB	CMYToRGB();
CMY	RGBToCMY();
#endif
*/

#endif
