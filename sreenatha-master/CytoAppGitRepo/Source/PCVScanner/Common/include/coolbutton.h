
/*
 * CoolButton.h
 *
 * JMB July 2001
 */


/////////////////////////////////////////////////////////////////////////////
// CCoolButton
/////////////////////////////////////////////////////////////////////////////

class CCoolButton : public CButton
{

protected:

	BOOL down;
	BOOL up;

	BOOL flatMode;

	COLORREF textColour;

// Construction
public:
	CCoolButton();

// Attributes
public:

	void SetTextColour(COLORREF colour);

// Operations
public:

	BOOL Create(LPCTSTR lpszCaption, const RECT& rect,
	            CWnd* pParentWnd, UINT nID, BOOL flat = FALSE);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoolButton)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCoolButton();

	// Generated message map functions
protected:
	//{{AFX_MSG(CCoolButton)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


