/*
 * flex_ff.h
 *
 * Defines the external interface to the new fileformats DLL.
 * - structures from WinCV header file flexred.h
 *
 * Key flex screen declarations for the fileformats DLL.
 * This header contains the required parts of flexred.h - see flexred.h for comments
 *
 * Copyright Applied Imaging corp. 2000
 * Created: 8May2000
 *
 *
 * Mods:
 *
 */

#define MAXSPLITS	50

#define OLDFLEXFILE 1
#define NEWFLEXFILE 2
#define MAGICNUMBER 0xfeedabed

#define FLEXMAGIC   0xbedafeed	// NEW - for file version checking MC
#define FLEXVERSION 2			// Version 1: for file version checking MC
								// Version 2: flexible karyotyper

typedef struct fxcontrol {
	Canvas **flexoriginal;
	Canvas **flexkeeporiginal;
	Canvas **flexkeepcanvas; 	/* for flex_chromptr[] canvases */
	struct object **flextempobj;
	struct object *axes[MAXSPLITS];
	SMALL *flexselected;

/*
 *----- pointer to flexible screen canvas -------
 */
	Canvas *flex_canvas;

/*
 *----- short cut list to individual canvases -------
 */
	Canvas **flex_chromptr;

/* 
 * Data structure for box selector event handlers 
 */

	Cselectdata	flexbox;

/*
 *------ pointers to associated structures ------
 */
	DDGS	*flexdg;	/* ddgs structure for flexible screen */

/*
 *------ polygon object formed after manual splitting ------
 */

	struct object *polyobj;

/*
 * activity flag used by fxcont_admin
 */

	short 	flexactive;

/*
 *------ number of objects on flex canvas ------
 */
	int numflex;
/*
 *------ shape to be drawn and created as a polygon ------
 */
	int shape;

/*
 *------ shape to be open or closed ------
 */
	int openclosed;

/*
 *------ canvas spacing and alignment on/off flags & type (used in alignment operation) -------------
 */
	int 	align;		/* */
	int 	align_type;	/* */
	int 	space;		/* */
	int 	space_type;	/* */
	int 	space_val;	/* */
	int 	anchor_num;	/* */

/*
 *------ background colour (for brightfield or flour images) -------------
 */
	int 	drawmode;	/* */

/* --------- filegroup and image identifiers for casebase ------------- */
	int filegroupid;
	int imageid;

/* --------- flag indicating if last op was a split (for undo op) ------------- */
	short split;

/* --------- no of split objects per object ------------- */
	int splitnum;

/* --------- total no. of split off objects in last operation (for undo op) ------------- */
	short newobjs;

/* -------- flag indicating if last op was a join (for undo op) ------------- */
	short join;

/* -------- modify flag used by flex screen load & save routines --- */
	short 	modified;

/* -------- chromosome width for overlap axis --- */
	short 	chromwidth;

	// New variable for flexible karyotyper
	int maxObjs;	// Current number of objects allocated 

} FXControl;


/*
 * CANVAS data property list for all flex screen objects 
 */

#pragma pack(2)

typedef struct Cflexdata_type {
	SMALL size;	/* size of this structure in bytes */
	short onum;	/* index to object list */
	double scale;	/* for scaling operations */
	double rads;	/* for rotation */
} Cflexdata;

#pragma pack()


