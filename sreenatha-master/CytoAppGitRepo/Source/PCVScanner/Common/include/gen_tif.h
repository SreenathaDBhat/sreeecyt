//
//	G E N _ T I F  . H -- Tiff file 'high level' read/write for libtiff
//							taken from tiffinfo.c [libtiff 'tools' module]
//					
//  Created by Mike Castling 8Dec00
//
//	Modifications
//
//
//
//

#include "tiffio.h"
//#include "tiffiop.h"
//#include "tif_predict.h"

#ifndef GENTIFF_H
#define GENTIFF_H


#include <stdio.h>

typedef unsigned char Ubyte;

#define EIGHT_K			8192
#define TIFF_STRING_LENGTH 64

// not defined in libtiff !
#define TIFF_DATE_LENGTH	20

/*
 *	Function definitions
 */

#ifdef __cplusplus
extern "C" {
#endif

/****** read_tif.c ******/
// Ubyte *open_and_read_tiff_image(char *filename, int *Width, int *Height, int *bpp);

void TIFFLoadRawData(TIFF *tif, Ubyte *image, int bitrev);
void TIFFLoadData(TIFF *tif, Ubyte *image);
void TIFFLoadContigStripData(TIFF* tif, Ubyte *image);

/****** write_tif.c ******/

void TIFFWriteRawData(TIFF *tif, Ubyte *image);
void TIFFWriteData(TIFF *tif, Ubyte *image);
void TIFFSaveContigStripData(TIFF* tif, Ubyte *image);

/****** compress_rgba.c ******/
// int compress_rgb(char *in_filename, char *out_filename, int required_compression);

void ErrorHandler(const char* module, const char* fmt, va_list ap);
void ErrorHandler2(const char* module, const char* fmt, va_list ap);

#ifdef __cplusplus
}
#endif

#endif

