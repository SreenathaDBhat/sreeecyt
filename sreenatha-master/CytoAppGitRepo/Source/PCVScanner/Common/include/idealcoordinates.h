#if !defined(AFX_IDEALLCOORDINATES_H__23C8B882_A7C9_456A_AE0C_77EC11202A5D__INCLUDED_)
#define AFX_IDEALLCOORDINATES_H__23C8B882_A7C9_456A_AE0C_77EC11202A5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IdeallCoordinates.h : header file
//
#include "MicroscopeDialog.h"


/////////////////////////////////////////////////////////////////////////////
// CIdealCoordinates dialog

#ifdef MICROSCOPEGUI_EXPORTS
#define MICROSCOPEGUI_API __declspec(dllexport)
#else
#define MICROSCOPEGUI_API __declspec(dllimport)
#endif

class MICROSCOPEGUI_API CIdealCoordinates : public MicroscopeDialog
{
// Construction
public:
	CIdealCoordinates(CWnd* pParent = NULL);   // standard constructor
	BOOL CreateMicroscopeDialog (CWnd *pParentWnd, CWnd *pMessageWnd, void * pIMicSrvr, int AxisID, BOOL LockFlag);

// Dialog Data
	//{{AFX_DATA(CIdealCoordinates)
	//enum { IDD = IDD_IDEALCOORDSBASE };
	float	m_fIdealX;
	float	m_fIdealY;
	float	m_fIdealZ;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIdealCoordinates)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CIdealCoordinates)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnMovetoideal();
	afx_msg void OnStagetoideal();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IDEALLCOORDINATES_H__23C8B882_A7C9_456A_AE0C_77EC11202A5D__INCLUDED_)
