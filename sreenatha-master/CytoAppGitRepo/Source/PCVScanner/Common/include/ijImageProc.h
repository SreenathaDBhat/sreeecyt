/*****************************************************************************
               Copyright (c) 2003 Applied Imaging International 
 
	Source file:    ijImageProc.h
 
	Function        Header file for all ImageJ ported image processing functions
					ImageJ v1.31 by Wayne Rasband (wayne@codon.nih.gov)
					ImageJ home page http://rsb.info.nih.gov/ij/

	Modification history:
	Author      Date        Description
	DS          15Dec2003   Initial implementation

 ****************************************************************************/

#pragma once

#include <afxtempl.h>		// CArray etc
#include <vector>

using namespace std;


#ifdef IJ_IMAGEPROC_EXPORTS
#define IJ_IMAGEPROC_API __declspec(dllexport)
#else
#define IJ_IMAGEPROC_API __declspec(dllimport)
#endif

class CUTSMemRect;

class IJ_IMAGEPROC_API IJImageProc
{
// static public image procsssing routines
public:
	IJImageProc(CUTSMemRect *pMR);

	long WatershedByEDM (long hInput, long hOutputEDM, long hOutputUEP, long hOutputWS, char *operation, bool isWhiteBackground);
	long AutoThreshold (long hInput, long & minThreshold, long & maxThreshold);
	void RankFilter (long hInput, long hOutput, double maskradius, char * operation);
	void MixtureModelThreshold (long hInput, long & theThreshold);
	
	
	
// static private helper function and internal variables
private:

	/*********************************************************************
	General purpose functions
	********************************************************************/

	// Given a 8 bit grey scale image, get the histogram for it
	void Get8BPPHistogram(vector<long> & input, vector<long> & histo);


	/*********************************************************************
	Watershed segmentation routines
	********************************************************************/

	/**************************************************************
	The following are codes adapted from ImageJ for Watershed
	segmentation for binary image based on Euclidean Distance Map
	 
	Adapted from ImageJ v1.31 by
	Wayne Rasband (wayne@codon.nih.gov)
	ImageJ home page http://rsb.info.nih.gov/ij/

	**************************************************************/

	/**************************************************************
	Supporting variables
	**************************************************************/
	long maxEDM; // max EDM value
	vector< long > xCoordinate; // Array of x coordinates for pixels with specific EDM value
	vector< long > yCoordinate;	// Array of y coordinates for pixels with specific EDM value
	vector< long > levelStart;	// Array of accumulated number of pixels that have value less than the histogram bin
	vector< long > levelOffset;	// Array of number of pixels that are within a histogram bin
	vector< long > histogram;	// Histogram for the EDM

	long count;	// counter used in ProcessLevel routine
	bool watershed;	// flag indicating if we are do watershed segmentation or only getting EDM/UEP
	bool invertImage; // flag indicating if we need to invert the image. it is set based on the isWhiteBackground parameter

	vector< long > input; // Array of pixels for the input image
	vector< long > edm; // Euclidean Distance Map
	vector< long > edm2; // a copy of Euclidean Distance Map for subsequence operations
	vector< long > uep; // Ultimate Erosion Point
	vector< long > ws; // WaterShed
	long imageWidth; // Image width
	long imageHeight;	// Image height
	long imageSize;	// Image size = ImageWidth * ImageHeight
	long imagemin; // Min value in image
	long imagemax; // Max value in image
	long imagemode; // Mode value in image (the value of the highest bin in the histogram)

	const static long one = 41; // magic number taken from NIH ImageJ
	const static long sqrt2 = 58; // ~ 41 * sqrt(2)
	const static long sqrt5 = 92; // ~ 41 * sqrt(5)

	// Watershed fate table
	static int WSFateTable[];			


	/**************************************************************
	Supporting functions
	**************************************************************/
	void Get1BPPImageData(long hInput, vector< long > & localArray);
	void Get8BPPImageData(long hInput, vector< long > & localArray);
	void ArrayToImage( vector<long>& hArrayIn, long hImageOutput);
	void SetValue(long offset, long rowsize, vector<long> & input);
	void SetEdgeValue(long offset, long rowsize, vector<long> & input, long x, long y, long xmax, long ymax);
	void FinishEDM();
	void MakeEDM();
	long Get(long x, long y, vector<long> & pixels, long width, long height);
	void FilterEDM(vector<long>& input, bool smooth);
	void MakeCoordinateArrays();
	void FindUltimatePoints();
	void PostProcess();
	void ProcessLevel(int level, int pass, vector<long>& input1, vector<long>& input2);
	void DoWatershed();


	/*********************************************************************
	END Watershed segmentation routines
	********************************************************************/


	double FindMedian(vector<double> & values);

	double FindMin(vector<double> & values);

	double FindMax(vector<double> & values);

	double FindMean(vector<double> & values);

	vector <long> CircularMask;

	double GetRankingPixel(int x, int y, vector<double> & pixels, int width, int height);

	void CreateCircularMask(int maskwidth, double maskradius);

	enum RankFilterType
	{
		RANK_MIN = 0,
		RANK_MAX,
		RANK_MEDIAN,
		RANK_MEAN,
	};

	void Rank(vector<double>& input, double radius, RankFilterType rankType);

	CUTSMemRect *pUTSMemRect;

};



