		/*Parameters control structure*/
struct IMSParams {
		char name;				//parameter name (see ims controller manual)
		char *explanation;		//explanation of function (see ims controller manual)
		int  address1;			//address of low order value
		int address2;			//address of high order value
		int change;				//requested to change
		int nvalues;			//some parameters take two input values, others only one
		int data_lo;			//low order value
		int data_hi;			//high order value
		int lower_limit;		//limit for parameter value
		int upper_limit;		//limit for parameter value
		};


				/*ims controller default parameters values and addresses*/
 struct IMSParams parameters[] = {'H',"Resolution mode",1985,0,FALSE,1,NULL,NULL,0,1,
		'D',"Microstep resolution",1987,0,FALSE,1,NULL,NULL,0,8,
		'Y',"Hold/Run current",1996,1997,FALSE,2,NULL,NULL,0,100,
		'V',"Slew velocity",2011,2012,FALSE,1,NULL,NULL,20,20000,
		'B',"Jog speed",1988,1989,FALSE,2,NULL,NULL,0,255,
		'K',"Ramp slope",1990,1991,FALSE,2,NULL,NULL,0,255,
		'I',"Initial velocity",2006,2007,FALSE,1,NULL,NULL,20,20000,
		'e',"Encoder resolution",0,0,FALSE,1,NULL,NULL,0,2000
		};

int NParameters = sizeof (parameters) /sizeof (struct IMSParams);	

