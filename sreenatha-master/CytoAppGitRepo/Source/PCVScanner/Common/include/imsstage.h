#ifndef _IMSSTAGE_H
#define _IMSSTAGE_H
/**
 *	I M S S T A G E . H --
 *
 *	IMS motor controller
 *
 */
#include "Axes.h"
#include "stage.h"

#define MOTOR_TIMEOUT   1  /* seconds */
#define MOTOR_CR     0x0D
#define MOTOR_LF     0x0A
#define COMM_SUCCESS    1
#define COMM_FAILURE   -1
#define COM_BUF_LEN   100
#define MOTOR_DELAY     4 /* centiseconds */

//typedef int (*pfii3)(int,int,int);

// DLL function declarations
#define	DLLEXP	__declspec( dllexport )

#ifdef __cplusplus
extern "C" {
#endif


//****** imsstage.c *****
DLLEXP int ims_stage_reopen(HANDLE fd);
DLLEXP HANDLE ims_stage_getfd(HANDLE *MicHandle, HANDLE *StageHandle);
DLLEXP int ims_stage_open(char *line);
DLLEXP int  ims_stage_close();
DLLEXP int ims_stage_axes(int axes);
DLLEXP int ims_stage_waitmode(int mode);
DLLEXP ims_stage_manualcontrols(int state);
DLLEXP ims_stage_moveto(int xpos, int ypos, int zpos);
DLLEXP int ims_stage_moveby(int xdist, int ydist, int zdist);
DLLEXP int ims_stage_moveaxisby(int axis, int dist);
DLLEXP int ims_stage_moveaxisto(int axis, int pos);
DLLEXP int ims_stage_floatmoveaxisto(int axis, float pos);
DLLEXP int ims_stage_setposition(int nx, int ny, int nz);
DLLEXP int ims_stage_getposition(int *cx, int *cy, int *cz);
DLLEXP int ims_stage_floatgetposition(float *cx, float *cy, float *cz);
DLLEXP int ims_stage_getabsposition(int *cx, int *cy, int *cz);
DLLEXP int ims_stage_floatgetabsposition(float *cx, float *cy, float *cz);
DLLEXP int ims_stage_setspeed(int axis, int speed);
DLLEXP int ims_stage_getdatum();
DLLEXP int ims_stage_updateposition();
DLLEXP int ims_stage_busy();
DLLEXP int ims_stage_stop();
DLLEXP pfi  ims_stage_setupdatecallback(void (*)(int, int, int) );

//***** imsstageHI.c ******
DLLEXP int StagePosition(int *x, int *y);
DLLEXP int StageFloatPosition(float *x, float *y);
DLLEXP int StageWait();
DLLEXP int StageStatus();
DLLEXP int StageHome1(int speed);
DLLEXP int StageHome();
DLLEXP int StageOriginX();
DLLEXP int StageOriginY();
DLLEXP int StageOriginZ();
DLLEXP int StageJog(int xspeed, int yspeed);
DLLEXP int StageJogX(int speed);
DLLEXP int StageJogY(int speed);
DLLEXP int StageJogZ(int speed);
DLLEXP int StageStop();
DLLEXP int StageSoftStop();
DLLEXP int StageMoveRel(int dx, int dy);
DLLEXP int StageMoveRelX(int dx);
DLLEXP int StageMoveRelY(int dy);
DLLEXP int StageMoveAbs(int to_x, int to_y);
DLLEXP int StageMoveAbsX(int to_x);
DLLEXP int StageMoveAbsY(int to_y);
DLLEXP int StageFloatMoveAbsX(float to_x);
DLLEXP int StageFloatMoveAbsY(float to_y);
DLLEXP int FocusPosition(int *z);
DLLEXP int FocusFloatPosition(float *z);
DLLEXP int FocusMoveAbs(int position);
DLLEXP int FocusFloatMoveAbs(float position);
DLLEXP int FocusWait();
DLLEXP int FocusHome();
DLLEXP int FocusStatus();
DLLEXP int FocusJog(int zspeed);
DLLEXP int FocusStop();
DLLEXP int FocusMoveRel(int dz);


//****** imsstageLO.c ******/
DLLEXP int ReOpenMotorController(HANDLE fd);
DLLEXP HANDLE GetMotorFile();
DLLEXP int OpenMotorController(char *port);
DLLEXP void CloseMotorController();
DLLEXP void centidelay(int n);
DLLEXP int FlushMotorBuffer(char *buf);
DLLEXP int ReceiveMotorResponse(char *buf);
DLLEXP int SendMotorCommand(char *command);
DLLEXP int SendMotorTerminator(int mode);
DLLEXP int MotorComNoecho(char *command);
DLLEXP int MotorCommand(char *command, char *response);
DLLEXP int MotorName(char name);


//****** imsstageMID.c *****
DLLEXP int MotorMoveStatus();
DLLEXP int MotorWait();
DLLEXP int MotorMoveRel(double travel);
DLLEXP int MotorMoveAbs(double destination);
DLLEXP int MotorJog(int speed);
DLLEXP int MotorPosition(double *position);
DLLEXP int MotorReadPorts(int *in1, int *in2, int *in3);
DLLEXP int MotorSetOrigin();
DLLEXP int MotorSoftStop();
DLLEXP int MotorHome(int speed, int dir);
DLLEXP int sign(int x);


#ifdef __cplusplus
}
#endif


#endif
