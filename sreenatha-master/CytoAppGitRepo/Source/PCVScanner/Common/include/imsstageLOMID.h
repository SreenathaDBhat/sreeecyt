#ifndef _IMSSTAGELOMID_H
#define _IMSSTAGELOMID_H
/**
 *	I M S S T A G E L O M I D . H --
 *
 *	IMS motor controller
 *
 */
#include "axes.h"

#define MOTOR_TIMEOUT   1  /* seconds */
#define MOTOR_CR     0x0D
#define MOTOR_LF     0x0A
#define COMM_SUCCESS    1
#define COMM_FAILURE   -1
#define COM_BUF_LEN   100
#define MOTOR_DELAY     4 /* centiseconds */

//typedef int (*pfii3)(int,int,int);

// DLL function declarations
#define	DLLEXP	__declspec( dllexport )

#ifdef __cplusplus
extern "C" {
#endif



//****** imsstageLO.c ******/
 int imsReOpenMotorController(HANDLE fd);
HANDLE imsGetMotorFile();
int imsOpenMotorController(char *port);
void imsCloseMotorController();
void imscentidelay(int n);
int imsFlushMotorBuffer(char *buf);
int imsReceiveMotorResponse(char *buf);
int imsSendMotorCommand(char *command);
int imsSendMotorTerminator(int mode);
int imsMotorComNoecho(char *command);
int imsMotorCommand(char *command, char *response);
int imsMotorName(char name);


//****** imsstageMID.c *****
int imsMotorMoveStatus();
int imsMotorHardwareStatus (int query);
int imsMotorWait();
int imsMotorMoveRel(double travel);
int imsMotorMoveAbs(double destination);
int imsMotorJog(int speed);
int imsMotorPosition(double *position);
int imsMotorReadPorts(int *in1, int *in2, int *in3);
int imsMotorSetOrigin();
int imsMotorSoftStop();
int imsMotorHome(int speed, int dir);
int imssign(int x);

#ifdef __cplusplus
}
#endif


#endif
