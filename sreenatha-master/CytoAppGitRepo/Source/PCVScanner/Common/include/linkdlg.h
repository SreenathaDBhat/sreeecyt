#if !defined(AFX_LINKDLG_H__C284DF05_6A3D_40B5_8BE8_24E99641676D__INCLUDED_)
#define AFX_LINKDLG_H__C284DF05_6A3D_40B5_8BE8_24E99641676D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// linkdlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLinkDlg dialog
#define IDD_LINKDLG                     10112

class LinkedSet;

class AFX_EXT_CLASS CLinkDlg : public CDialog
{
// Construction
public:
	CLinkDlg(LinkedSet *set, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLinkDlg)
	enum { IDD = IDD_LINKDLG };
	CListCtrl	m_linklb;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLinkDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLinkDlg)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnClickLinklb(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnOK();
	afx_msg void OnSelall();
	afx_msg void OnSelnone();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


	LinkedSet *m_pLinkedSet;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LINKDLG_H__C284DF05_6A3D_40B5_8BE8_24E99641676D__INCLUDED_)
