/*
 *      M A C H D E F . H  --  Machine dependant type deffinutions
 *
 *
 *  Written: Clive A Stubbings
 *           Image Recognition Systems
 *           720 Birchwood Boulevard
 *           Birchwood Science Park
 *           Warrington
 *           Cheshire
 *           WA3 7PX
 *
 *	Copyright (c) and intellectual property rights Image Recognition Systems
 *	Copyright etc Applied Imaging 1995
 *
 *  Date:    7th October 1986
 *
 *  Modifications
 *
 *	9/19/96		BP:		Added WIN32 (same as i386) and changed COORD to
 *						WZCOORD to avoid clash with Windows.
 *
 *	BP	7/17/95:	GREY is now unsigned char.
 *	 4 Apr 1989		dcb/SCG		Add SUN definitions
 *	 4 Mar 1987		CAS		Added definition of FOLD
 *
 */


#ifndef MACHDEF_H
#define MACHDEF_H


#ifdef OSK
typedef short	GREY;		/* pixel values */
typedef short	WZCOORD;		/* integer coordinate */
typedef short	SMALL;		/* small integer value */

/* #define FOLD 0x80000		/* Processor memory fold */
#endif


#ifdef WIN32
typedef unsigned char	GREY;		/* pixel values */
typedef short	WZCOORD;		/* integer coordinate */
typedef short	SMALL;		/* small integer value */
typedef unsigned char UBYTE;
#endif


#ifdef i386
typedef unsigned char	GREY;		/* pixel values */
typedef short	WZCOORD;		/* integer coordinate */
typedef short	SMALL;		/* small integer value */
typedef unsigned char UBYTE;
#endif


#ifdef SUN
typedef short GREY;		/* pixel values */
typedef short WZCOORD;		/* integer coordinate */
typedef short SMALL;		/* small integer value */
#endif


#ifdef VAX
typedef int GREY;		/* pixel values */
typedef int WZCOORD;		/* integer coordinate */
typedef int SMALL;		/* small integer value */
#endif


#endif

