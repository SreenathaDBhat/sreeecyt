#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <Types.h>

static float at,bt,ct;
#define PYTHAG(a,b) ((at=fabs(a)) > (bt=fabs(b)) ? \
(ct=bt/at,at*sqrt(1.0+ct*ct)) : (bt ? (ct=at/bt,bt*sqrt(1.0+ct*ct)): 0.0))
static float maxarg1,maxarg2;
#define MAT_MAX(a,b) (maxarg1=(a),maxarg2=(b),(maxarg1) > (maxarg2) ?\
	(maxarg1) : (maxarg2))
#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))


class CMatrix {

public :
	CMatrix();
	virtual ~CMatrix();


	int matrix_multiply (Matrix *mat1, Matrix *mat2, Matrix *mat3);
	void identity_matrix (Matrix *mat);
	void arerror(char error_text[]);
	double **admatrix(long nrl, long nrh, long ncl, long nch);
	void free_admatrix(double **m, long nrl, long nrh, long ncl, long nch);

	// Private function prototypes
	void from_upper_triang( double *a, double **b, int n);
	void to_upper_triang( double *a, double **b, int n);
	int dvector( int nl, int nh, double **dvect);		
	void nrerror( char *error_text);					
	void free_dvector( double *v, int nl, int nh);		
	int ludcmp( double ** a, int n, int * indx, double * d);
	void lubksb( double **a, int n, int * indx, double b[]);
	void svbksb(double **u,double w[],double **v,int m,int n,double b[],double x[]);
	void svdcmp( double **a,int m,int n,double w[],double **v);

	int matinv( double **a, double **y, int n);
	double matdet( double **a, int n, int *powten);
	int least_squares(double** A, double* x, double* b, int M, int N);
	double **dmatrix( int nrl, int nrh, int ncl, int nch, int * err);
	void free_dmatrix( double **m, int nrl, int nrh, int ncl, int nch);


private:

};

#endif