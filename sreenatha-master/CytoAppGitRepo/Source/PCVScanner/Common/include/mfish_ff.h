/*
 * mfish_ff.h
 *
 * Defines the external interface to the new fileformats DLL.
 *
 * This header contains the required parts of mfish.h - see mfish.h for comments
 *
 *
 * Copyright Applied Imaging corp. 2000
 * Created: 27April2000
 *
 * 
 *
 * Mods:
 *
 *
 *
 */

//-----------------------------------------------------------------------------------
//
//	MFISH :
//
// The following #defines needed in mfish_support.cpp
#define MFISHMAGIC			0xff0000ff		// magic number stored in mfish files

#define MFISH_VERSION				2		// current mfish structure

#define MAX_MFISH_FLUORS			9		// Counterstain + 8 possible fluors (256 colours) 

#define COMBINATORIAL_MFISH			1		// mfish method, combinatorial ... 
#define RATIO_MFISH					2		// or ration mfish 

#define MFISH_CLASSNAME_LENGTH		10		// max length of className strings 
#define MFISH_FLUORNAME_LENGTH		20		// max length of fluorName strings

#ifdef __cplusplus
extern "C" {
#endif

#include <mfishver1.h>						// OLD version 1 data structures

typedef struct fluomap_type{
	// Next 2 fields are new for flexible karyotyper 
	char *speciesName;						// Name of species
	int NumOfClasses;						// Number of classes in species

	int nfluors;							// Number of fluorochromes used 
	int nclass;								// number of descernable classes (including p & q arms)
	char **fluorName;						// fluorochrome component names 
	char **className;						// class names e.g.   1, 6p, X   
	int *classSig;							// fluo composition signature    
	LUT_Triple *classLut;					// RGB classification colour     
	int method;								// combinatorial or ratio 
	int *fluorType;							// combinatorial or ratio for each fluor 
} Fluomap;

//SN18Sep98 For file reading
#pragma pack(2)
typedef struct mfish_type{


	int nfluors;							// Number of fluorochromes used 

	int *fluorID;							// probe image fluorochrome component IDs 
	
	Canvas **fluorRawCanvas;				// pointers to individual fluorochrome raw image canvases 

	Uchar **fluorIm;						// pointers to individual fluorochrome raw images 

	Uchar **fluorIm2;						// background subtracted 

	struct object ***fluorList;				// lists of fluor objects 

	struct object **pseudoList;				// lists of pseudo coloured objects 

	short *objtype;							// CHROMOSOME, OVERLAP, NUCLEUS etc 

	int nobjs;								// Number of separate objects 

	int *fluorThresh;						// threshold values for each fluor

	int nclass;								// number of descernable classes (including p & q arms)
	char **className;						// class names e.g.   1, 6p, X   
	int *classNum;							// derived from name  1, 6,  23  
	int *classSig;							// fluo composition signature    
	LUT_Triple *classLut;					// RGB classification colour     

	xy_offset *fluorOffset;					// x and y offset of fluor images 

	int registration_ok;					// true if auto-registration succeeds 
											// or auto-registration not requested 

	int chrom_halfwidth;					// halfwidth of typical chromosome 

	int rawWidth;							// width of all raw images 
	int rawHeight;							// height of all raw images 

	int *fluorMin;
	int *fluorMax;
	int *fluorNorm;							// Normalisation factors 

	int *fluorBackgroundMean;
											// Mean of background fluorescence 

	RGB *fluorColour;						// fluor colour in kary or met 
	short *fluorGamma;						// fluor gamma in kary or met 
	short *fluorDisplay;					// fluor displayed in kary or met 
	short *fluorSelect;						// fluor selected in kary or met 
	LUT_Triple **fluorLut;					// fluor display LUTS 

	int method;								// combinatorial or ratio 
	int *fluorType;							// combinatorial or ratio for each fluor 

	//	Version 2 includes support for flexible karyotyping

	int NumOfClasses;						// Current number of classes allocated
	int MaxObjs;							// Current number of objects allocated 

} MFISH;
#pragma pack()


#ifdef __cplusplus
}
#endif

