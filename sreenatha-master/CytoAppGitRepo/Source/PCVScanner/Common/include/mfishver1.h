
// mfishver1.h		M.Gregson	
//					Version 1 mfish and fluompa structures
//					These have been replaced by dynamic structures from version 2 onwards
//					**** This file should only be included into mfish.h ****

#define DEFAULT_MAX_MFISH_CLASS		2*(DEFAULT_NUMOFCLASSES + 1)

typedef struct fluomap1_type{
	int nfluors;							// Number of fluorochromes used 
	int nclass;								// number of descernable classes (including p & q arms)
	//SN18Sep98 Changed from int* to char*
	char fluorName[MAX_MFISH_FLUORS][MFISH_FLUORNAME_LENGTH];	// fluorochrome component names 
	char className[DEFAULT_MAX_MFISH_CLASS][MFISH_CLASSNAME_LENGTH];	// class names e.g.   1, 6p, X   
	int classSig[DEFAULT_MAX_MFISH_CLASS];	// fluo composition signature    
	LUT_Triple classLut[DEFAULT_MAX_MFISH_CLASS];	// RGB classification colour     
	int method;								// combinatorial or ratio 
	int fluorType[MAX_MFISH_FLUORS];		// combinatorial or ratio for each fluor 
} Fluomap1;


//SN18Sep98 For file reading
#pragma pack(2)
typedef struct mfishver1_type{


	int nfluors;							// Number of fluorochromes used 
	int fluorID[MAX_MFISH_FLUORS];			// probe image fluorochrome component IDs 

	Canvas *fluorRawCanvas[MAX_MFISH_FLUORS];// pointers to individual fluorochrome 
											// raw image canvases 

	Uchar *fluorIm[MAX_MFISH_FLUORS];		// pointers to individual 
											// fluorochrome raw images 

	Uchar *fluorIm2[MAX_MFISH_FLUORS];		// background subtracted 

	struct object *fluorList[MAX_MFISH_FLUORS][DEFAULT_MAXOBJS];
											// lists of fluor objects 

	struct object *pseudoList[DEFAULT_MAXOBJS];		// lists of pseudo coloured objects 

	short objtype[DEFAULT_MAXOBJS];			// CHROMOSOME, OVERLAP, NUCLEUS etc 

	int nobjs;								// Number of separate objects 

	int fluorThresh[MAX_MFISH_FLUORS];		// threshold values for each fluor

	int nclass;								// number of descernable classes (including p & q arms)
	TCHAR className[DEFAULT_MAX_MFISH_CLASS][MFISH_CLASSNAME_LENGTH];	// class names e.g.   1, 6p, X   
	int classNum[DEFAULT_MAX_MFISH_CLASS];	// derived from name  1, 6,  23  
	int classSig[DEFAULT_MAX_MFISH_CLASS];	// fluo composition signature    
	LUT_Triple classLut[DEFAULT_MAX_MFISH_CLASS];	// RGB classification colour     

	xy_offset fluorOffset[MAX_MFISH_FLUORS];// x and y offset of fluor images 

	int registration_ok;					// true if auto-registration succeeds 
											// or auto-registration not requested 

	int chrom_halfwidth;					// halfwidth of typical chromosome 

	int rawWidth;							// width of all raw images 
	int rawHeight;							// height of all raw images 

	int fluorMin[MAX_MFISH_FLUORS];
	int fluorMax[MAX_MFISH_FLUORS];
	int fluorNorm[MAX_MFISH_FLUORS];		// Normalisation factors 

	int fluorBackgroundMean[MAX_MFISH_FLUORS];
											// Mean of background fluorescence 

	RGB fluorColour[MAX_MFISH_FLUORS];		// fluor colour in kary or met 
	short fluorGamma[MAX_MFISH_FLUORS];		// fluor gamma in kary or met 
	short fluorDisplay[MAX_MFISH_FLUORS];	// fluor displayed in kary or met 
	short fluorSelect[MAX_MFISH_FLUORS];	// fluor selected in kary or met 
	LUT_Triple fluorLut[MAX_MFISH_FLUORS][256];	// fluor display LUTS 

	int method;								// combinatorial or ratio 
	int fluorType[MAX_MFISH_FLUORS];		// combinatorial or ratio for each fluor 

} MFISHver1;
#pragma pack()
