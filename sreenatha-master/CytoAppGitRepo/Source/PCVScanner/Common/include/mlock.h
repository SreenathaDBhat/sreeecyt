////////////////////////////////////////////////////////////////////////////////
//
//	CMLock class -- intra-application locking using MUTEX
//

#if !defined(AFX_MLOCK_H__E407D3CC_12EB_46B0_8CDA_177A77463183__INCLUDED_)
#define AFX_MLOCK_H__E407D3CC_12EB_46B0_8CDA_177A77463183__INCLUDED_
#include <afxmt.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class _declspec(dllexport) CMLock  
{
public:
	CMLock(int node_level, int db_id);
	CMLock(CMLock *mlock);
	virtual ~CMLock();

	// Application level locking
	bool AppLock(int timeout = 4000);	// Place application level lock on data node
	void AppUnlock();					// Clear the application lock
	bool AppIsLocked();					// Check if lock is present

private:
	// Application level locking
	CSingleLock *m_lockobj;							// Application level locking object (CSingleLock)
	CMutex *m_mutex;								// Application locking MUTEX object
	bool m_sublock;									// True if this CMLock does not own the mutex
	CString GetLockName(int node_level, int db_id);	// Construct the lock mutex name
};

#endif // !defined(AFX_MLOCK_H__E407D3CC_12EB_46B0_8CDA_177A77463183__INCLUDED_)

