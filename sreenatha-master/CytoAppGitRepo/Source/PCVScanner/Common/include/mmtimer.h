// MMTimer.h

#pragma once

#include <mmsystem.h>

class CMMTimer
{

public:
	CMMTimer();
	virtual ~CMMTimer();

	MMRESULT StartSingleShot(LPTIMECALLBACK TimerCallbackFn, DWORD_PTR pUserData, UINT TimerDelay);
	int Stop();

private:
		UINT m_elTime;
		MMRESULT m_MMTimerEventID;
		UINT m_MMTimerTime;
		DWORD m_MMTimerResolution;
};