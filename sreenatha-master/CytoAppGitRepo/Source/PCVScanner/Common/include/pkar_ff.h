/*
 * pkar_ff.h
 *
 * Defines the external interface to the new fileformats DLL.
 *
 *
 * This header contains the required parts of pkar.h - see pkar.h for comments
 *
 * Copyright Applied Imaging corp. 2000
 * Created: 27April2000
 *
 * 
 *
 * Mods:
 *
 *
 *
 */

//-----------------------------------------------------------------------------------
//
//	PKAR:
//
#define PKARMAGIC	0xfee1dead
#define MAX_PKAR_FLUORS 9

#pragma pack(2)

typedef struct pkar_type{

	int nfluors;							/* Number of fluorochromes used */
	int fluorID[MAX_PKAR_FLUORS];			/* probe image fluorochrome component IDs */
	Canvas *fluorRawCanvas[MAX_PKAR_FLUORS];/* pointers to individual fluorochrome raw image canvases */
	Uchar *fluorIm[MAX_PKAR_FLUORS];		/* pointers to individual fluorochrome raw images */
	Uchar *fluorIm2[MAX_PKAR_FLUORS];		/* background subtracted */
	struct object **fluorList[MAX_PKAR_FLUORS]; /* [MAXOBJS];  lists of fluor objects */
	struct object **pseudoList;				/* [MAXOBJS];	 lists of pseudo coloured objects */
	short *objtype;							/* [MAXOBJS];			 CHROMOSOME, OVERLAP, NUCLEUS etc */
	int nobjs;								/* Number of separate objects */
	int fluorThresh[MAX_PKAR_FLUORS];		/* threshold values for each fluor*/
	int nclass;								/* number of descernable classes */
	xy_offset fluorOffset[MAX_PKAR_FLUORS];	/* x and y offset of fluor images */
	int registration_ok;					/* true if auto-registration succeeds or auto-registration not requested */
	int chrom_halfwidth;					/* halfwidth of typical chromosome */
	int rawWidth;							/* width of all raw images */
	int rawHeight;							/* height of all raw images */
	int fluorMin[MAX_PKAR_FLUORS];
	int fluorMax[MAX_PKAR_FLUORS];
	int fluorNorm[MAX_PKAR_FLUORS];			/* Normalisation factors */
	int fluorBackgroundMean[MAX_PKAR_FLUORS]; /* Mean of background fluorescence */
	RGB fluorColour[MAX_PKAR_FLUORS];		/* fluor colour in kary or met */
	short fluorGamma[MAX_PKAR_FLUORS];		/* fluor gamma in kary or met */
	short fluorDisplay[MAX_PKAR_FLUORS];	/* fluor displayed in kary or met */
	short fluorSelect[MAX_PKAR_FLUORS];		/* fluor selected in kary or met */
	LUT_Triple fluorLut[MAX_PKAR_FLUORS][256];	/* fluor display LUTS */
	int fluorType[MAX_PKAR_FLUORS];			/* combinatorial or ratio for each fluor */
	int MaxObjs;							/* Current number of objects allocated */

} PKAR;
#pragma pack()