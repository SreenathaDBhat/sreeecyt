#pragma once

#pragma warning(disable:4945)

#undef  _WIN32_WINNT         // Prevent any 'macro refefinition' warning.
#define _WIN32_WINNT 0x0600  // If this is not defined, lots of '_WIN32_WINNT not defined' warnings will be generated.

#ifndef _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_DEPRECATE
#endif