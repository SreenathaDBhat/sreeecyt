/*
 * probe_ff.h
 *
 * Defines the external interface to the new fileformats DLL.
 *
 *
 * This header contains the required parts of probe.h - see probe.h for comments
 *
 * Copyright Applied Imaging corp. 2000
 * Created: 27April2000
 *
 * 
 *
 * Mods:
 *
 *
 *
 */

#define MAX_COMPONENTS	10	/* Max. no. of probe components allowed */


/*	LUT triples are RGBRGBRGB etc. */
#ifndef LUTTRIPPPPPL
#define LUTTRIPPPPPL
#pragma pack(2)
typedef struct {
	unsigned char r, g, b;
} LUT_Triple;
#pragma pack()
#endif

typedef struct pcontrol {
	int	ncomponents;		/* Number of image components. */
	Canvas	*component;		/* Hook for Canvas separate. */
	Canvas	*composite;		/* All together. */
	Canvas	*annocan;		/* Shortcut ptr to annotation canvas */
	int	componentIds[MAX_COMPONENTS];	/* Fluorochrome id's */
	unsigned char *arthur_negus;	/* 24 bit image buffer */
	SpectraChrome *componentList; 	/* Fluorochrome structures - linked list */
	LUT_Triple **luts;		/* Colourisation LUTS */
	Cselectdata	box;		/* Selection box stuff. */
	int		modified;		/* TRUE if modified since last save. */
	short	drawmode;		/* May not need this. */
	DDGS	*dg;			/* Where we draw to. */
	BOOL active;			/* Whether interact stuff running. */
	int		chromwidth;		/* For manual draw axes split. */
	BOOL	colourised;		/* Obvious. */

/*  Filegroup and image ID's. */
    int groupID;
    int imageID;

/* List of original objects in current image for Restore. */
	int nool;
	struct object **ool;

/* List of canvases before processing for Undo. */
	Canvas *keep;

/* List of original objects corresponding to keep list for Undo. */
	int nkool;
	struct object **kool;

} Pcontrol;
