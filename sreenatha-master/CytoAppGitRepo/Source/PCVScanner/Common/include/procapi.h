/////////////////////////////////////////////////////////////////////////////////
// Code to manage processes 
//
// Written By Karl Ratcliff 16092001
// Header file
// 
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#ifdef HELPER3_EXPORTS
#define HELPER3_API __declspec(dllexport)
#else
#define HELPER3_API __declspec(dllimport)
#endif

//#include <windows.h>
#include <tchar.h>

/////////////////////////////////////////////////////////////////////////////////
// This class is used to evaluate all processes currently executing on the 
// system. 
/////////////////////////////////////////////////////////////////////////////////

class HELPER3_API CProcessMonitor
{
public:
    CProcessMonitor();
    ~CProcessMonitor();

    // Check to see if a process is up and running
    DWORD IsProcessRunning(LPCTSTR ProcessName);

	// Kill one
	DWORD KillProcess(LPCTSTR ProcessName);

	// Count the number of instances of a process
	int CountProcesses(LPCTSTR ProcessName);

    // Launch a new process
    BOOL LaunchProcess(LPCTSTR ProcessNameAndPath, LPTSTR CmdLine, PROCESS_INFORMATION *pi);
	BOOL LaunchProcess(LPCTSTR ProcessNameAndPath, LPTSTR CmdLine, DWORD CreationFlags, PROCESS_INFORMATION *pi);


#ifndef _WIN64
	void DumpProcessInfo(DWORD processID);
	void PrintMemoryInfo(DWORD processID);
	void DumpCurrentProcessInfo(void);

	DWORD CurrentWorkingSet(void);
#endif

private:

	HMODULE	m_hInstLib;			// PROCAPI.DLL
};

