/*
 * redkar_ff.h
 *
 * Defines the external interface to the new fileformats DLL.
 *
 *
 * This header contains the required parts of redkar.h - see redkar.h for comments
 *
 * Copyright Applied Imaging corp. 2000
 * Created: 27April2000
 *
 * 
 *
 * Mods:
 *
 *	11Dec02     MC	Moved Cblobdata propertylist dec. from here - to canvas.h
 *
 *
 *
 */

//
// The following defines were from REDKAR.H (WinCV)
//
// file constants
//
#define METMAGIC	0xfadedace	/* metaphase magic number */
#define KARMAGIC	0xacefaded	/* karyotype magic number */
    
// New met and kar versions for flexible karyotyper   
#define METVERSION	2			/* met file version number */
#define KARVERSION	2			/* kar file version number */

// constants used to define associated structures - CGH, MFISH etc
#define ASSOC_NONE		0
#define ASSOC_CGH		1
#define ASSOC_MFISH		2
#define ASSOC_BARCODE	3
#define ASSOC_PKAR		4

typedef struct count_cross_type {
    	short x;
    	short y;
}crosspt;
  
struct kcontrol {
    	char	FIP;				/* FIP ,TV,  FLUOR digitisation ? */
    	SMALL 	maxnumber;			/* current maximum number of objects */
    	SMALL	measnumber;			/* current object to measure */
    
    /* ------ general purpose display parameters and frames ------ */
    	int 	current_obj;		/* used in mouse interaction */
    	SMALL 	identify_set;		/* Which set of objects to be identified */
    	struct 	seg_interact *SI;	/* segmentation control structure */
    	int		chromwidth;			/* chromosome width for overlap axes */
    /*
     *--------  Classifier   --------
     */
    	SMALL 	classifier;			/* classifier SHORT_CLASS, LONG_CLASS, etc */
    
    /*
     *------ particular lists for karyotyping ------
     */
    	struct chromosome **ool;	/* original objects pointer */
    	struct chromosome **eool;	/* enhanced original objects pointer */
    	struct chromosome **mol;	/* measured/rotated/classified objects pointer */
    	struct chromosome **emol;	/* enhanced classified objects pointer */
    	struct object	  **bndool;	/* original object boundary list */
    	struct object	  **bndmol;	/* measured/rotated object boundary list */
    	SMALL 		*acl;			/* feature measurement activity list */
    	SMALL		*selected;		/* currently selected objects */
    
    	struct chromosome **keepeool;	 /* old eool object pointers for undo */
    	struct object	  **keepbndool;	 /* old bndool boundary pointers for undo */
    	struct chromosome **keepemol;	 /* old emol object pointers for undo */
    	struct object	  **keepbndmol;	 /* old bndmol boundary pointers for undo */
    	struct chromplist **keepsharedplist;/* old sharedplist contents for undo */
    
    	SMALL		  *keepclassmax;/* old class canvas maxchroms for undo */
    /*
     *----- pointers to metaphase and karyogram canvases -------
     */
    	Canvas *met_canvas;
    	Canvas *kar_canvas;
    	Canvas *metanno_canvas;
    	Canvas *karanno_canvas;
    
    /*
     * Short cut lists for quick access to individual class and chromomsome canvases
     */
    	Canvas **classptr;
    	Canvas **kar_chromptr;
    	Canvas **met_chromptr;
    
    /* 
     * Data structures for box selector event handlers 
     */
    	Cselectdata	metbox, karbox;
    
    /*
     *------ pointers to associated structures ------
     */
    	DDGS	*kardg;			/* ddgs structure for karyogram */
    	DDGS	*metdg;			/* ddgs structure for metaphase */
    
    	SMALL 	karactive;		/* activity flags used by kcont_admin */
    	SMALL 	metactive;
    
    /*
     *	filegroup and image identifiers - from casbase - used for copy & paste
     */
    	int filegroupid;		
    	int met_imageid;
    	int kar_imageid;
    
    /*
     *------ canvas display zoom factor -----------------------------
     */
    	int 	zoom_factor;		/* ddgs scale i.e  16 => x2 */
    /* 
     *------ canvas drawmode - COPY for brightfield INVERT for fluorescent -------
     */
    
    	SMALL 	drawmode;
    
    /*
     *------- colourised metaphase flag ------------------------------
    */
    	SMALL 	colourised;
    
    /*
     *-------- modification flag - check if met/kar have been updated ------
     */
    	SMALL	modified;
    
    /*
     *----- timer measurements ---------------------------------------
     */
    	UINT meastime;			/* time out id for MeasCallback - object measurement */
    
    /*
     *----- manual count info ----------------------------------------------
     */
    	int manual_count;		/* object count */
    	crosspt *count_cross;	/* cross position of counted object */
    
    /*
     *----- align on centromere if this flag set ----------------------------
     */
    	SMALL	centalign;		/* align on centromere if set */
    
    
    /*
     *------ CGH / MFISH / BARCODE flag and pointer to structure ------------
     */
    	int 	assoc_type;		/* ASSOC_NONE, ASSOC_CGH, ASSOC_MFISH .. */
    	char	*assoc_ptr;		/* NULL      , *cgh     , *mfish         */
    
	// New fields for flexible karyotyper - METVERSION=2, KARVERSION=2
		int NumOfClasses;		// current number of classes allocated
		int MaxObjs;			// Current number of objects allocated 
		void *TemplatePtr;		// Species template pointer
   };


   //
//	DEAD needed in destroy_zombies()
//
//	per-object activity bits 
//
    #define ACTIVE		1	/* still of interest */
    #define DEAD		2	/* zombie waiting to be destroyed */

//
//	Cclassdata needed in create_shortcuts()
//
//	CANVAS data property list for Cchromclass type
//
    typedef struct Cclassdata_type{
    	SMALL	size;		/* size of this structure in bytes */
    	short	chromclass;		/* chromsome class */
    	short	nchroms;	/* current no of chromosomes in class canvas */
    	short	maxchroms;	/* no of chromosomes allowed in class canvas */
    	char	tag[10];	/* tag field for class */
    } Cclassdata;
    
 
//
//  Cchromdata & Cblobdata are needed in destroy_zombies()
//
//
//	CANVAS data property list for Cchromosome type
//
    typedef struct Cchromdata_type{
    	SMALL	size;		/* size of this structure in bytes */
    	short	chromclass;		/* chromsome class */
    	short	posinclass;	/* position in class canvas */
    	short	onum;		/* index to object list */
    	char	tag[10];	/* tag field for class */
    } Cchromdata;
    
    
// needed for Cannodata in free_annokeep
//
// CANVAS data property list for annotation
//

typedef struct Cannodata_type{
    	SMALL	size;		/* size of this structure in bytes */
    	int	selected;	/* canvas selected or not */
    	int	state;		/* -1=delete if freekeep, 0=OK, 1=delete if undo */
    	double 	scale;		/* current scale factor */
    } Cannodata;