////////////////////////////////////////////////////////////////////////////
//
//	r g b t y p e s  . h 
//
//  ID's and a structure for CytoTalk picture images
//
//  Written: Mike Castling 27Jun2001
//
//  Mods:
//

#ifndef RGBTYPES_H
#define RGBTYPES_H

#define RGB_MET			0
#define RGB_FMET		1
#define RGB_FMETCGH		2
#define RGB_FMETMFISH	3
#define RGB_FMETPROBE	4

#define RGB_KAR			5
#define RGB_FKAR		6
#define RGB_FKARCGH		7
#define RGB_FKARMFISH	8
#define RGB_FKARPROBE	9

#define RGB_FLEX		10
#define RGB_FFLEX		11
#define RGB_FUSE		12
#define RGB_FFUSE		13
#define RGB_PROBE		14
#define RGB_CGH			15

#define NO_HEADER       _T("NO HEADER")
#define RAWIMAGE		1
#define PROCESSEDIMAGE	2

struct rgbdata {
//	SMALL size;		/* of this structure - ESSENTIAL for I/O */
	TCHAR name[64];
	TCHAR casename[20];	
	TCHAR slidename[20];
	TCHAR cellname[20];
	int rgbtype;		
};

#endif