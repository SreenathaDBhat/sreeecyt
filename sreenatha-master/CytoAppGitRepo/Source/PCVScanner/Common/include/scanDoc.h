// scanDoc.h : interface of the CScanDoc class
//
/////////////////////////////////////////////////////////////////////////////


#if !defined(AFX_SCANDOC_H__5E47464A_8A80_11D4_9C43_00C04F8477D1__INCLUDED_)
#define AFX_SCANDOC_H__5E47464A_8A80_11D4_9C43_00C04F8477D1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>

#include "..\Src\MicServer\MicServer.h"
#include "..\src\Grabber2\GrabServer2\_GrabServer2.h"
#include "GrabDevice.h"

#define MAXWHEELS 255
#define MAXFILTERS 255

class CDBNavigator;
class CItemInfo;
class CSpectraChrome;
class CSpectraChromeList;
class CWheelList;
class CSlideList;
class CAssayList;
class CAssay;
class CQueue;
class CFindImageDialog;
class CLiveDialog;
class CLiveColDialog;
class CToolDialog;
class CSlideMapDialog;
class CScaledBitmap;
class CAssayDialog;
class CAssayListDialog;
class CSpectraChromeDlg;
class XYStageControl3DLOD;
class ZDrive3DLOD;
class CSlideMapDialog;
class CJoyDialog;
class CLoaderControl;
class CSlide;
class CFluorochromeWizard;
class CClassMonitorDlg;


class CScanDoc : public CDocument
{
protected: // create from serialization only
	CScanDoc();
	DECLARE_DYNCREATE(CScanDoc)

// Attributes
public:
	CSlideList*							m_slideList;                        // List of slides on loader, or multi-bay stage
	CAssayList*							m_defaultAssayList;                 // List of default assays
	CSpectraChromeList*                 m_defaultSpectraChromeList;         // List of default spectra chromes
	CSpectraChrome*                     m_selectedSpectrachrome;
	CQueue*                             m_scanQ;
	CQueue*                             m_findQ;
	CList<int, int&>*                   m_TrayPosnList;

	CDBNavigator*                       m_nav;
	CSlideMapDialog*                    m_slideMapDialog;
	CAssayDialog*                       m_assayDialog;
	CAssayListDialog*                   m_assayListDialog;
	CSpectraChromeDlg*                  m_spectraChromeDialog;
	CLiveDialog*                        m_liveDialog;
	CLiveColDialog*                     m_liveColDialog;
	CFindImageDialog*                   m_findImageDialog;
	CToolDialog*                        m_toolDialog;
	CLoaderControl*						m_LoaderControl;
	CFluorochromeWizard					*m_pFluorWiz;
	CClassMonitorDlg					*m_pClassMon;

	CJoyDialog*                         m_joyDialog;
	void*                               m_d3dctrl;
	XYStageControl3DLOD*                m_XYCtrl;
	ZDrive3DLOD*                        m_ZCtrl;

	IMicSrvr*                           m_pIMicSrvr;
	IGrabber*                           m_pIGrab;
	BYTE*                               m_capBuf;
	BOOL                                m_gotSlideLoader;
	INITGRABBERPROC                     m_fnInitGrab;
	DESTROYGRABBERPROC                  m_fnDestroyGrab;
	HINSTANCE                           m_hDll;
	CWheelList*  						m_wheelList;

	int                                 m_nX;
	int                                 m_nY;
	int                                 m_nZ;
	int                                 m_nTrays;

	long                                m_HmainFrameWnd;
	long                                m_HliveWnd;

	BOOL                                m_initialized;
	BOOL                                m_live;
	BOOL                                m_statusThread;

	char                                m_Axes[_MAX_PATH];

	CScaledBitmap*                      m_CurPosBmp;

	void MicroscopeHardwareControls(int AxisID);

	// To make things more readable, equate to the node level in the hierarchy
	enum {Study, Case, Slide, Scan, Cell};

// Operations
public:
	static void NavCallBack(CItemInfo * item, void * pObj);
	
			//callback functions
	static void HouseKeeperCallback (void *pObj);
	static void NewScanCallback (CSlide *pSlide, void *pObj);
	static void SlideSelectedCallback (CSlide *pSlide, void *pObj);
	static BOOL IsScanningCallback(void *pObj);

	void getFilterNames(int wheel, int MaxFilters);
	void getObjectiveNames(int wheel, int MaxFilters);
	int filter_maxfilters(int wheel, int *MaxFilters);
	void getOccupiedSlidePositions();
	void getOccupiedTrayPositions();
	void getBarcodesOnTray();
	void matchSpectrachromesToHardware();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScanDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CScanDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CScanDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCANDOC_H__5E47464A_8A80_11D4_9C43_00C04F8477D1__INCLUDED_)
