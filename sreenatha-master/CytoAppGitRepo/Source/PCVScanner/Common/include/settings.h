/*
 *	S E T T I N G S . H -- Callback defines, settings structures etc.
 *
 *  Written: David Burgess, Applied Imaging
 *	New version 7th June 1995
 *  Copyright (c) and intellectual property rights Applied Imaging (1995)
 *
 * Mods:
 *	12Nov01		MC	Added spot counting
 *	11Nov99		KS	min_cont now redundant, replaced with bg_sub
 *	03May99		dcb	Add ROI to capture settings (and roi on flag)
 *	16Jul97		MG:	Added BARCODEMODE
 *	16Oct96		MG:	Replaced one of SpectraChrome reserved shorts
 *					with CGHref flag for fourth fluorochrome work
 *	17May96		MG:	Added MFISHMODE
 *	22Jan97	BP:		Windows version.
 *
 *	3Apr96		MG:	Moved MIN & MAX_IMAGE_INTENSITY constants here from
 *					getmmfocus.c coz thery are needed by capture.c too
 *					- BP had moved them out of c_comms.h to getmmfocus.c ???
 *
 *	12/12/95	BP:		Added DEFMIN_CONTRAST and DEFMIN_CONTCHANGE. These
 *						are parameters for autocapture.
 *	11/30/95	BP:		Max integration now 2500 (50 seconds). This parameter
 *						is now changeable (capconfig), so the old value has
 *						been made the default maximum. New value is absolute
 *						maximum maximum!
 *  16Nov95	Ginestet	Change MAX_INTEGRATION to 254
 *	23Aug95	dcb			Change MIN_INTEGRATION back to 0
 *  07Jul95	Ginestet	Change MIN_INTEGRATION to 1
 *	15/11/94 MG	CGHMODE constant added and FluoroChrome priority field
 *				(unused ) changed to CGHtest field.
 *	11/8/93	dcb	gamma parameter into fluorochrome setup
 *	14/4/93	dcb	New typedef for gamma
 *	31/3/93 dcb	Integrate changes from hwh, typedefs GammaShadow & bright_fluo
 *	1/4/93	dcb	Add slider max/min limits
 *	17/5/93	dcb	Add padding short after RGB
 *	20MAR94	dcb	Alter max/min gain values
 */


#ifndef SETTINGS_H
#define SETTINGS_H


#include <color.h>


/*
 *	Callback defines
 */
#define S_GAIN			1
#define S_OFFSET		2
#define S_INTEGRATION	3
#define S_AVERAGING		4

/* Make these also the index into the SpectraChrome Filter array */
/* So need to be 0 - 3 */
#define S_EXCITATION	0
#define S_EMISSION		1
#define S_DICHROIC		2
#define S_TRANSMISSION	3

#define S_X_OFFSET		8
#define S_Y_OFFSET		9
#define S_CURRENT		10
#define S_Z_OFFSET		11	/*AR 16:10:97 -- for zoffset between fluorochromes*/

#define S_AUTOSETHI		12
#define S_AUTOSETLO		13	
//#define S_AUTOSETPROBE	14

/*
 *	Mode definitions
 */
#define BRIGHT			0	/* Must be 0 */
#define FLUO			1	/* Must be != 0 */
#define PROBE			2
#define CGHMODE			3
#define MFISHMODE		4
#define BARCODEMODE		5
#define PKARMODE		6
#define SPOTCOUNTING	7
#define SCANBRIGHT		8
#define SCANFLUO    	9
#define TISSUE_FISH		10
#define PROBE_AUTO		11

/* constants used by capture.c and getmmfocus.c */
#define MAX_IMAGE_INTENSITY 255
#define MIN_IMAGE_INTENSITY 0


/*
 *	Slider values max/min limits
 */
#define MAX_GAIN		255
#define MAX_OFFSET		255
#define MAX_INTEGRATION	5000
#define DEFMAX_INTEGRATION	5000
#define MAX_AVERAGING	8
#define MIN_GAIN		1
#define MIN_OFFSET		0
#define MIN_INTEGRATION	0
#define MIN_AVERAGING	0

/* Percentage ranges for auto-capture. */
#define	ABSMIN_CONTRAST	30
#define	DEFMIN_CONTRAST	85
#define DEFMIN_CONTCHANGE	3


#define MAX_Z_STACKS	16
#define MAX_TFISH_FILES (MAX_Z_STACKS + 1) * 6

#define MAX_WHEELS 10

/* Ensure correct alignment for auto-initialisation */
#pragma pack(4)
/*
 *	Filter within wheel
 */
typedef struct {
	int position;
	TCHAR *name;
} Filter;

/*
 *	Region of interest
 */
typedef struct {
	int x,	/* Coords of top left corner */
		y,
		w,	/* Width & height */
		h;
} ROI;

/*
 *	Zstack setup parameters
 */
typedef struct zstack_setup {
	short	on:1,			/* Acquire with Z-stack */
			define_roi:1,	/* Region of interest on/off */
			dothreshold:1;	/* Threshold image (or not) */

	short	num_images,		/* Number of image slices to take */
			focus_dist,		/* Focus distance between each */
			pc_below,		/* Percentage of images to take below focus */
			obj_size;		/* Process out objects this size */

	ROI		roi;			/* Region of interest */
} ZstackSetup;
 
typedef enum 
{
	NotProbe = 0,
	Probe = 1
} AutoSettingsImageType;

typedef struct autosettings_params
{
	float					min, max;
	int						Spare;
} AutoSettingsParams;
/*
 *	Capture board settings and lamp
 */
typedef struct {
	short	gain,			/* Board gain, etc */
			offset,
			integration,
			averaging,
			gamma,			/* Input gamma value * 100 */
			lampintensity;	/* For controlled lamp source */
	ROI		roi;			// Capture region
	BOOL	roi_on:1;		// Region of interest is on
	BOOL	pad:7;			// 7 bits padding
	AutoSettingsParams autosetparams;/* Target end points for autocamera setup */
} CaptureSettings;

/* Optical correction data structure */
typedef struct {
	short	xoffset,	/* x chromatic shift (registration offset) */
			yoffset,	/* y chromatic shift (registration offset) */
			zoffset;	/* Focus offset (from reference wavelength image) */
	short	xcenter,	/* Optical centre */
			ycenter;
	short	xstretch,	/* x & y unit magnification change */
			ystretch;
} OptCorrect;

typedef enum {
	StretchOn = 1,
	StretchOff = 0
} ContrastStretchOpts;

/*
 *	SpectraChrome - Details about a particular image spectral component
 */
typedef struct spectra_chrome {
	TCHAR			*name;
	RGB				colour;
	short			gamma;			/* Output gamma value */
	unsigned char	isfluor:1,		/* Fluorescence imaging */
					iscounter:1,	/* Probe counterstain indicator */
					display:1,		/* Component is/to be displayed */
					captured:1, 	/* Component has been captured */
					select:1,		/* Component can be selected */
					CGHtest:1,		/* CGH test image */
					CGHref:1,		/* CGH ref image */
					reserved:1;		/* was 10 before CGHref existed */
	unsigned char	attenuate_cs;
	OptCorrect		correct;		/* Optical correction parameters */
	CaptureSettings capture;		/* Capture board settings + lamp */
	short			max_integ;		/* Max integration. */
	short			max_gain;		/* Max gain. */
	short			bgsub_size;		/* feature size for background subtraction. */
	short			brightening;	/* brightening parameter */
	short			topendthresh;	/* top end threshold */
	short			bottomendthresh;/* bottom end threshold */
	ContrastStretchOpts	contraststretch;/* stretch contrast (really a BOOL) */
	float			sharpen1;		/* place holder values for when sharpening is implemented */
	float			sharpen2;
	short			spare2;
	ZstackSetup		zstack;			/* Zstack acquisition params */
	Filter			filter[MAX_WHEELS];	/* Filter name/pos one per wheel */
	short			id;				/* Temporary id */
	struct			spectra_chrome *next;
	CaptureSettings CameraSettings[99];
	int				FramesCount;

} SpectraChrome;
#pragma pack()

// SpectraChrome *mergeToList(), *readList();

#endif

