// slideldr.h
//
// Copyright Invetech Operations Pty Ltd 2000
// 495 Blackburn Rd, Mt Waverley, Victoria, AUSTRALIA
//
// The copyright to the computer program(s) herein
// is the property of Invetech Operations Pty Ltd, Australia.
// The program(s) may be used and/or copied only with
// the written permission of Invetech Operations Pty Ltd
// or in accordance with the terms and conditions
// stipulated in the agreement/contract under which
// the program(s) have been supplied.

// Contains definitions of all the exported functions and enums
//
// *** USAGE ***
// When using this header file you need to define:
// _IMPORTING when using the DLL in your projects
// _EXPORTING in the project that builds the DLL


#ifndef slideldr_h_included
#define slideldr_h_included

#include "CommsDef.h"

#undef DLL_DECL
#define DLL_DECL

#ifdef _EXPORTING
    #undef DLL_DECL
    #define DLL_DECL    extern "C" __declspec(dllexport)

    //if _IMPORTING is also defined, there is a conflict so report an error
    #ifdef _IMPORTING
        #pragma message("WARNING: Contradictory #defines: both _EXPORTING and _IMPORTING defined!")
    #endif
#endif

#ifdef _IMPORTING
    #undef DLL_DECL
    #define DLL_DECL extern "C" __declspec(dllimport)
#endif

typedef int SlideLoaderHandle;



// Maximum number of characters (alpha-numeric) in a barcode
const int MAX_BARCODE_LENGTH = 15;

// The number of slides in a tray
const int NUMBER_OF_SLIDES_PER_TRAY = 10;

// The number of trays in the instrument
const int NUMBER_OF_TRAYS = 5;

const unsigned long BUILD_VERSION = 100000000;	//used to extract build and version from number
												//eg. 120010612 % BUILD_VERSION = 20010612
												//eg. 120010612 / BUILD_VERSION = 1
// for vacuum thresholds
const int VACUUM_THRESHOLDS = 5;

// length of barcode command string
const int MAX_BARCODE_COMMAND_LENGTH = 30;


typedef char            BarCodeStrType[MAX_BARCODE_LENGTH+1];             // a type to hold 1 barcode
typedef BarCodeStrType  SlideBarCodeArrayType[NUMBER_OF_SLIDES_PER_TRAY]; // holds a string for each slide in the tray
typedef BarCodeStrType  TrayBarCodeArrayType[NUMBER_OF_TRAYS];            // holds a string for each tray
typedef char            LongStrType[255];                                 // general string type - because we don't have a string type
typedef bool            SlidePresentArrayType[NUMBER_OF_SLIDES_PER_TRAY]; // a boolean for each slide in a tray
typedef bool            TrayPresentArrayType[NUMBER_OF_TRAYS];            // a boolean for each tray in an instrument
typedef BYTE            VacuumThresholdsType [VACUUM_THRESHOLDS];         // for reading thresholds from file
typedef char            BarcodeCommandStrType[MAX_BARCODE_COMMAND_LENGTH];

typedef struct
{
    // byte 0 - Digital Input Register 0
    BYTE    verticalHomeTop:1;
    BYTE    verticalHomeBottom:1;
    BYTE    verticalCastle:1;    
    BYTE    spare1:5;
    
    // byte 1 - Digital Input Register 1
    BYTE    horizontalHomeLeft:1; 
    BYTE    horizontalHomeRight:1; 
    BYTE    horizontalWellDetect:1; 
    BYTE    horizontalCastle:1;
	BYTE	spare2:4;

    // byte 2 - Digital Input Register 2
    BYTE    tray5Home:1;
    BYTE    tray4Home:1;
    BYTE    tray3Home:1;
    BYTE    tray2Home:1;
    BYTE    tray1Home:1;
    BYTE    trayPCBConnected:1;
	BYTE	spare3:2;

	//Digital Input Register 3 is the ADC and is not returned

    // byte 3 - Digital Input Register 4
    BYTE    frontCoverOn:1;
    BYTE    stageDetect:1;
    BYTE    slideDetect:1;
    BYTE    slideOnStage:1;
    BYTE    spare4:4;

    // byte 4 - Digital Output Register 0
    BYTE    presence:1;
    BYTE	mute:1;
    BYTE    errorLed:1;
    BYTE    valveOn:1;
    BYTE    powerOn:1;
    BYTE    pumpOn:1;
    BYTE    buzzer:1;
    BYTE    adcCS:1;	//Chip Select for ADC

} HardwareStatusType;



DLL_DECL ReturnCode CreateSlideLoader         ( int iComPortNumber );   // open port
DLL_DECL void       SlideLoaderExit           ( void );

DLL_DECL ReturnCode Abort                     ( SlideLoaderHandle   SlideLoaderHandle );
DLL_DECL ReturnCode CheckForSlideOnStage      ( SlideLoaderHandle   SlideLoaderHandle,
                                                bool&               bSlidePresent
                                              );
DLL_DECL ReturnCode ClearCurrentFault         ( SlideLoaderHandle   SlideLoaderHandle );
DLL_DECL ReturnCode GetBarcodeResponse        ( SlideLoaderHandle      SlideLoaderHandle,
                                                BarcodeCommandStrType& barcodeCommandStr
                                              );
DLL_DECL void       GetDLLVersion             ( long&               dllVersion );
DLL_DECL ReturnCode GetEventLogFilename       ( SlideLoaderHandle   SlideLoaderHandle,
                                                LongStrType&        eventLogFilename
                                              );
DLL_DECL ReturnCode GetFirmwareVersion        ( SlideLoaderHandle   SlideLoaderHandle,
                                                long&               firmwareVersion,
                                                int&                epldVersion
                                              );
DLL_DECL ReturnCode GetHardwareStatus         ( SlideLoaderHandle   SlideLoaderHandle,
                                                HardwareStatusType& hardware, 
                                                int&                pressure,
                                                int&                offsetPressure,
                                                int&                elapsedTime     // seconds
                                              );
DLL_DECL ReturnCode GetLastFault              ( SlideLoaderHandle   SlideLoaderHandle,
                                                FaultCategory&      theFaultCategory,
                                                int&                faultNumber
                                              );
DLL_DECL ReturnCode GetOccupiedSlidePositions ( SlideLoaderHandle   SlideLoaderHandle,
                                                SlidePresentArrayType& slidePresent
                                              );
DLL_DECL ReturnCode GetOccupiedTrayPositions  ( SlideLoaderHandle   SlideLoaderHandle,
                                                TrayPresentArrayType& trayPresent
                                              );
DLL_DECL ReturnCode GetSlideBarcode           ( SlideLoaderHandle   SlideLoaderHandle,
                                                int                 iSlidePosition,
                                                BarCodeStrType&     barCodeString
                                              );
DLL_DECL ReturnCode GetStatus                 ( SlideLoaderHandle   SlideLoaderHandle, 
                                                States&             slideLoaderState
                                              );
DLL_DECL ReturnCode GetTrayBarcode            ( SlideLoaderHandle   SlideLoaderHandle,
                                                BarCodeStrType&     barcodeString
                                              );
DLL_DECL ReturnCode GetTrayTampered           ( SlideLoaderHandle   SlideLoaderHandle,
                                                TrayPresentArrayType& trayTampered
                                              );
DLL_DECL ReturnCode Initialise                ( SlideLoaderHandle   SlideLoaderHandle );
DLL_DECL ReturnCode LoadSlide                 ( SlideLoaderHandle   SlideLoaderHandle,
                                                int                 iSlidePosition
                                              );
DLL_DECL ReturnCode MoveRobotHeadAboveStage   ( SlideLoaderHandle   SlideLoaderHandle );
DLL_DECL ReturnCode RemoveSlide               ( SlideLoaderHandle   SlideLoaderHandle );
DLL_DECL ReturnCode ScanSlidePositions        ( SlideLoaderHandle   SlideLoaderHandle );
DLL_DECL ReturnCode ScanTrayPositions         ( SlideLoaderHandle   SlideLoaderHandle );
DLL_DECL ReturnCode SelectTray                ( SlideLoaderHandle   SlideLoaderHandle,
                                                int                 iTrayNumber
                                              );
DLL_DECL ReturnCode SendBarcodeCommand        ( SlideLoaderHandle      SlideLoaderHandle,
                                                BarcodeCommandStrType& barcodeCommandStr
                                              );
DLL_DECL ReturnCode SetDebugMode              ( bool                debugMode );
DLL_DECL ReturnCode SetHardware               ( SlideLoaderHandle   SlideLoaderHandle, 
                                                bool                pumpOn,
                                                bool                valveOn
                                              );
DLL_DECL ReturnCode SetVacuumThresholds       ( SlideLoaderHandle   SlideLoaderHandle );

DLL_DECL ReturnCode CheckSlidePresence		  ( SlideLoaderHandle	SlideLoaderHandle,
											    bool				presenceOn);

DLL_DECL ReturnCode ScanWholeTray			  ( SlideLoaderHandle	SlideLoaderHandle );

DLL_DECL ReturnCode Mute					  (	SlideLoaderHandle	SlideLoaderHandle,
											    bool				muteOn);

#endif
