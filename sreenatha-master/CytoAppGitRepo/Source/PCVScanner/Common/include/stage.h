/*
 *	S T A G E . H
 *
 *	Clive A Stubbings
 *	Applied Imaging International
 *	16 Sept 1993
 *
 *	Modifications
 *	16Feb99 mg	replaced DMRXA with GEN_MIC
 *	25May95	dcb	Change STAGE_Z definition from 3 to 4 (for use in bit defns)
 *	07Dec94	dcb	Added X, Y, Z separate commands + include guard
 */
#ifndef STAGE_H
#define STAGE_H

/*
 *	Physical translations
 */
#define MICRONSTOSTEPS(a)	(int)(((a) * 2) / 5)
#define STEPSTOMICRONS(a)	(int)(((a) * 5) / 2)

/* Bit-wise definitions */
#define STAGE_X	1
#define STAGE_Y	2
#define STAGE_Z	4

#define ST_DOWN 1
#define ST_OK   2

typedef int (*pfi)();
typedef pfi (*pfpfi)(int (*)());

typedef pfi (*pfpfvi3)(void(*)(int, int,int));
typedef void * (*pfh)();
typedef int (*pfih)(HANDLE);

typedef int (*pfiif)(int, float);
typedef int (*pfii)(int);
typedef int (*pfii2)(int, int);
typedef int (*pfii3)(int,int,int);
typedef int (*pfiip3)(int *, int*, int *);
typedef int (*pfifp3) (float *, float *, float *);
typedef int (*pfipc)(char *);
/*
 *	G E N E R I C   S T A G E   I N T E R F A C E
 */
typedef enum {STG_NONE, STG_IMS, STG_MFC, STG_GEN_MIC } StageType;

/*
 *	stage_command() stage command defines
 */
#define	ST_ABORT	0		/* Close down */
#define	ST_INIT		1

#define	ST_HOME		2
#define	ST_POS		3		/* Retrieve position of stage. */
#define	ST_REL		4		/* Move to REL position */
#define	ST_ABS		5		/* Move to ABS position */
#define	ST_JOG		6		/* Send speed to move at. */
#define	ST_STOP		7
#define	ST_READY	8		/* Check status and return 1 if busy. */

#define ST_FOCPOS	9		/* Retrieve position of focus. */
#define ST_FOCREL	10		/* Move to REL position */
#define ST_FOCJOG	11
#define	ST_FOCSTOP	12
#define	ST_FOCREADY	13
#define	ST_FOCABS	14		/* Move to ABS focus position */

#define ST_JOGX		15		/* X Speed setting */
#define ST_JOGY		16		/* Y Speed setting */
#define ST_JOGZ	ST_FOCJOG	/* Z (Focus) speed setting */

#define ST_RELX		18		/* X relative move */
#define ST_RELY		19		/* Y relative move */

#define ST_ABSX		20		/* X Absolute move */
#define ST_ABSY		21		/* Y Absolute move */

#define ST_ORIX		22		/* X Origin */
#define ST_ORIY		23		/* Y Origin */
#define ST_ORIZ		24		/* Z Origin */

#define ST_SOFTSTOP	25		/* Stage soft stop (incl. Z) */

			//stage control 
#define MAXSPEED 100				
#define STAGE_UPDATEPERIOD	750L		//update stage position periodically 
#define STAGE_STEPPERIOD	   333L		

#endif

