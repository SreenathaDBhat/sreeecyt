/*
 *	S T R U C T I O . H -- Structure file IO header info
 *
 *  Written: David Burgess
 *  Applied Imaging, 1st June 1995
 *
 *  Modifications
 *
 *	05May99	dcb	Add definitions for low level read write functions
 *	03Oct95	dcb	Include guard, better comments, ARGS stuff
 *
 */
#ifndef STRUCTIO_H
#define STRUCTIO_H

#ifndef _ARGS
#	ifdef _NO_PROTO
#		define _ARGS(arglist) ()
#	else
#		define _ARGS(arglist) arglist
#	endif
#endif

#include <tchar.h>

/* Field format definitions */
#define TF_VOID			0
#define TF_SHORT		2
#define TF_INT			4
#define TF_STRING		8
#define TF_FIXEDSTRING	16
#define TF_TCHARSTRING  32
#define TF_FLOAT		64
#define TF_DEBUG        128

/* Data size definitions */
#define SZ_FLOAT	4
#define SZ_SHORT	2
#define SZ_INT		4
#define SZ_VOID		0

/* Universal end of data */
#define _END		999

/*	Error numbers */
#define	E_S_HEADER		1
#define E_S_READFUNC	2
#define E_S_ALLOC		3

typedef int (*pfi)();

#pragma pack(4)
/*
 *	Type/format structure
 *	NOTE: the dp field is a char ** and may need to be
 *	appropriately cast before dereferencing
 *	e.g. *((char *)tf[n].dp)
 */
typedef struct type_format{
	int		type;		/* Defined type */
	int		format;		/* Data format (short, int etc) */
	void	**dp;		/* Data pointer - can be NULL */
	pfi		writefunc;	/* Write function */
	pfi		readfunc;	/* Read function */
} TypeFormat;
#pragma pack()

/*
 *	writefunc, and readfunc are called for writing and reading
 *	and passed two args.
 *	1.	The file pointer
 *	2.	Pointer to the typeformat entry
 *	The functions should return 1 if no error, or -1 on error
 *
 *	Generic read write functions are provided which
 * 	work for shorts, ints, strings, fixedstrings and voids
 *	(a fixedstring is a character array)
 */

#ifdef __cplusplus
extern "C" {
#endif
int WriteData _ARGS((FILE *fp, TypeFormat *tf_p));
int ReadData _ARGS((FILE *fp, TypeFormat *tf_p));

/*	Utility read/write structure functions */
void *ReadStruct _ARGS((FILE *cfp, TCHAR *headstr, TCHAR *verstr, void *data_p,
					   void *local, int size, TypeFormat *tf, int ntypes, int *Version));

int WriteStruct _ARGS((FILE *fp, TCHAR *headstr, TCHAR *verstr, void *data_p,
					  void *local, int size, TypeFormat *tf, int ntypes));

/*
 *	Low level read write functions that may be used by a module
 *	to implement specific read write functionality
 */
int read_short(FILE *fp, unsigned int *val);
int read_int(FILE *fp, unsigned int *val);
int read_strn(FILE *fp, TCHAR *str, int len);
int read_tstrn(FILE *fp, TCHAR *str, int len);
int	read_float(FILE *fp, float *val);
int write_short(FILE *fp, unsigned int val);
int write_int(FILE *fp, unsigned int val);
int write_strn(FILE *fp, TCHAR *str, int len);
int write_tstrn(FILE *fp, TCHAR *str, int len);
int write_float(FILE *fp, float val);
int SkipData(FILE *fp);


#ifdef __cplusplus
}
#endif

#endif

