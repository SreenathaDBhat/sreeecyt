
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SUBPROCESS_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SUBPROCESS_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef SUBPROCESS_EXPORTS
#define SUBPROCESS_API __declspec(dllexport)
#else
#define SUBPROCESS_API __declspec(dllimport)
#endif

// This class is exported from the subprocess.dll
class SUBPROCESS_API CSubprocess {
public:
	CSubprocess(void);
	// TODO: add your methods here.
};

extern SUBPROCESS_API int nSubprocess;

SUBPROCESS_API int fnSubprocess(void);

// func_interface functions
extern "C" {
BOOL SUBPROCESS_API Bug();
DWORD SUBPROCESS_API IsProcessRunning(char *procname);
BOOL SUBPROCESS_API LaunchProcess(char *procname, char *cli);
BOOL SUBPROCESS_API StopProcess(char *event_name);
int SUBPROCESS_API StatusProcess();
BOOL SUBPROCESS_API CommProcess(char *name, char *value);
int SUBPROCESS_API SetVar(int value);
int SUBPROCESS_API GetVar();
BOOL SUBPROCESS_API SetEventName(int event_id, char *name);
long SUBPROCESS_API EventStatus(char *event_name);
BOOL SUBPROCESS_API RenameFiles(char *dir, char *pattern1, char *pattern2);

}
