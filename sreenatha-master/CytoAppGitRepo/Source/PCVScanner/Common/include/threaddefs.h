/////////////////////////////////////////////////////////////////////////////
// Different events and messages used by various find threads/scan threads etc.
// Cut out by Karl Ratcliff
/////////////////////////////////////////////////////////////////////////////

#ifndef __THREADDEFS_H
#define __THREADDEFS_H

// This event signals to the calling process that the cvscanner.exe
// child process has ended o.k.
#define SCAN_ENDEVENT             _T("SCANENDEVENT")

// This event signals that the calling process wishes to end
// the cv scanner process
#define SCAN_REQTOENDEVENT        _T("SCANREQTOENDEVENT")

// Defines an event requesting capture to end - set by the find thread and reset when cvscanner launched
#define SCAN_STOPCAPTUREEVENT	  _T("SCANSTOPCAPTUREEVENT")

// Defines an event requesting capture to end - set by the find thread and reset when cvscanner launched
#define SCAN_INITEVENT				_T("SCANINITEVENT")

// Message with the current total number of cells
#define WM_NUMCELLS_FOUND           WM_USER + 1
// Stop the autoscan function
#define WM_STOPAUTOSCAN             WM_USER + 978

/////////////////////////////////////////////////////////////////////////////
// Persistent Variable Names
/////////////////////////////////////////////////////////////////////////////

// Persistent variables set by the finder thread and got by the script
#define FRAME_X_POS					_T("X")
#define FRAME_Y_POS					_T("Y")
#define FRAME_Z_POS					_T("Z")
#define FRAME_X_SCALE               _T("SCALEX")
#define FRAME_Y_SCALE               _T("SCALEY")
#define FRAME_X_SIZE				_T("SIZEX")
#define FRAME_Y_SIZE				_T("SIZEY")
#define FRAME_REGION_NUMBER         _T("REGIONNUMBER")
#define STAGE_X_POS					_T("STAGEXPOSITION")
#define STAGE_Y_POS					_T("STAGEYPOSITION")
#define STAGE_Z_POS					_T("STAGEZPOSITION")
#define STAGE_X_SCALE               _T("STAGEXSCALE")
#define STAGE_Y_SCALE               _T("STAGEYSCALE")
#define CLASSIFIER_NAME             _T("CLASSIFIERNAME")
#define OBJECT_COUNT                _T("OBJECT_COUNT")
#define PASS_NUMBER					_T("PASS")
#define FRAME_NUMBER				_T("FRAMENUMBER")
#define FRAMES_TOTAL				_T("FRAMESTOTAL")
#define SCRIPT_MODE					_T("SCRIPTMODE")
#define FREE_OBJECTS				_T("FREE_OBJECTS")
#define INPUT_REGION_LIST			_T("INPUTREGIONLIST")
#define OUTPUT_REGION_LIST			_T("OUTPUTREGIONLIST")
#define OBJECTIVE_MAG				_T("OBJECTIVEMAG")
#define CLASSIFIER_PASS				_T("CLASSIFIERPASSED")
#define SPECTRACHROME_NAME          _T("SPECTRACHROMENAME")
#define SPECTRACHROME_NUMBER        _T("NUMSPECTRACHROMES")
#define STOP_CAPTURE                _T("STOPCAPTURE")
#define NEXT_FRAME_X_SCALE          _T("NEXTSCALEX")
#define NEXT_FRAME_Y_SCALE          _T("NEXTSCALEY")
#define SLIDEMAP_FULL_PATH			_T("SLIDEMAPFULLPATH")
#define PAUSE_AFTER_PASS			_T("PAUSEAFTERPASS")
#define STUDY_NAME					_T("STUDYNAME")
#define CASE_NAME					_T("CASENAME")
#define SLIDE_NAME					_T("SLIDENAME")
#define SCAN_NAME					_T("SCANNAME")
#define FRAME_ID					_T("FRAME_ID")
#define SCAN_AREA_NAME				_T("SCANAREA_NAME")
#define FRAME_MASK_PATH				_T("FRAME_MASK_PATH")
#define CELLID_FRAME				_T("CELLID_FRAME")

// Output directory
#define OUTPUT_DIRECTORY			_T("OUTPUTDIR")

//Region Lists
#define INPUT_REGION_LIST			_T("INPUTREGIONLIST")
#define OUTPUT_REGION_LIST			_T("OUTPUTREGIONLIST")
#define REGION_ID   				_T("RID")
#endif
