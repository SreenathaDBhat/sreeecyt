#pragma once


#if WINVER < 0x600
#import <msxml.dll>
#define GTXXML			MSXML
#else
#import <msxml6.dll> 
#define GTXXML			MSXML2
#endif