#ifndef YBLOB_H
#define YBLOB_H

#ifdef UTS_EXPORTS
#define ZBLOB_API __declspec(dllexport)
#else
#define ZBLOB_API __declspec(dllimport)
#endif

/************************** Structure for SPOT *********************/

typedef struct
{
	 long Xmin;   //  min X coordinate
	 long Ymin;   //  min Y coordinate
	 long Xmax;   //  max X coordinate
	 long Ymax;   //  max Y coordinate
	 long Thresh; // threshold for the spot
	 long PixNum; // number of pixels (area)
	 long PixSum; // sum of pixels (intensity)
	 long PixMax; // max pixel value
	 long Xrecon; // X, Y coordinates of the
	 long Yrecon; // max pixel for reconstruction
	 long Tag; 	  // user handled working value
	 long Tag1;   // Flags; a set of bits (for description or classification)
	 long Tag2;   // ThreshNum; # of pixels at the threshold
	 long Tag3;   // NextThr; next threshold value
	 long Tag4;   // NextNum; # of pixels at the next threshold
	 long Tag5;   //
	 long Tag6;   //
} Spot;

#define Flags  Tag1
#define ThreshNum  Tag2
#define NextThr  Tag3
#define NextNum  Tag4

#define SpotBufferSizeDefault  	0x8000
#define WorkBufferSize  		0x8000
#define SpotBegIndex      1 	// offset in SpotBuffer where


class CZMemRect;

class ZBLOB_API CZBlob
{
public:
	CZBlob(CZMemRect *pMem);
	~CZBlob();

	 long  spBlobDebugFlag(long newval);

	 long  spFind1 (BYTE *imagemem, long xe, long ye, long pitch, long inverse, long minarea);

	 long  spFind8(BYTE *imagemem, long xe, long ye, long pitch,	long thresh, long inverse, long minarea);

	 long  spCodeAccumLines (void *pcodes, long ncodes, long xstart, long ystart, long period,
		long hacc, double distmin, double distmax);

	 long  spAccumLines (long mask, long hacc, double distmin, double distmax);

	 long  spAccumLinesSeg (long mask, long hacc, long halfstepmin, long halfstepmax, long curvthresh,
		double distmin, double distmax);

	 long  spArcCenter (long mask, double distmin, long *xc, long *yc, double *radaver,
		double *radmin, double *radmax);

	 long  spSpotsNumber(void);

	 long  spSpotListMask (long hndl8, long hmask);

	 long  spConstrWaterShed (long hndl8, long hndl1, long borders, long thrmin, long areamax, long formfactor,
		long formthresh, long formtype);

	 long  spGetReadable(long spotnum, void *pget);

	 long  spCount8 (BYTE *imagemem, long xe, long ye, long pitch, long begthresh, long minarea, long maxarea);

	 long  spCount81 (BYTE *imagemem, long xe, long ye, long pitch, long begthresh, long minarea, long maxarea);

	 long  spCountFast (long hndl, long resmask, long begthresh, long minarea, long maxarea, long threshstep);

	 long  spFindSpots (long hndl, char *operation, long threshold, long minarea);

	 long  spCountRadius (long hndl, long radius, long thresh, long minarea, long invert, long mask);

	 long  spFindMarkers (long hndl, long threshold, long minarea, long maxarea);

	 long  spCountRange (long hndl, long resmask, long begmask, long minarea, long maxarea,
		double quantbeg, double quantfin, long maxrep, long nopen);

	 long  spSombreros (long hsrc, long hres, long rad, long invert);

	 long  spDeleteOutbreaks (double sigfactor, long *border);

	 long  spDeleteTag(long tagvalue);

	 long  spGetSpot(long spotnum, BYTE *pget);

	 long  spContShapeInfo (void *pcodes, long ncodes, long xstart, long ystart, long *resdata);

	 long  spGray8ToNormArg (long hsrc, long hnorm, long harg, long vicinity, long flag);

	 long  spLongSmoothArc (long masksrc, long maskcont, long maskarc,
		long halfstepmin, long halfstepmax, long curvthresh,
		long *resdata, void *codes, void *curvs, long codeslim);

	 long  spMaskShapesInfo (long hmask, long h32, long minarea);

	 long  spNearSpot(long xc, long yc);

	 void  spMinDistances();

	 void  spSetTag(long spotnum, long tagval);

	 long  spMinsAndMaxs (BYTE *data, long pnpnt,
		long noise, long thresh, long cycle,	
		long *res, long reslen);

	 long  spSortTag(char *operation);

	 long  spSortTagInd(char *operation);

	 long  spSpotListForm (long hmask);

	 long  spWaterShed (long hndl8, long hndl1, long borders,long thrmin, long areamax, long formfactor,
		long formthresh, long formtype);

	 long  spMaskShapesInfoNew (long hmask, long h32, long minarea, long sortByThisCol);

	 long  spRemoveSpots(long hmask, long h32, long numComp, long thCol, long thValue, int fillLevel, long hseed);

private:
	long FreeSmallSpots(long minarea, long lastY);
	void CleanTail(long tail);
	long GetFreeSpot(long minarea, long lastY);
	long CompactSpots(void);
	long Find8plus (BYTE *imagemem, long xe, long ye, long pitch, long thresh, long inverse, long minarea, long deftag);
	long FillTagValues(char *operation);
	long SortChain(char *operation, long *first);
	void spShowSpots(long spotnum, long numof,	long specnum);
	bool IsNew (long num, long bordnum, long maxnum);

	long ClearBuffer();
	Spot *SpotBuffer;
	long SpotBufferSize;
	long SpotsNumber; // length of the list (in Spots)
	long FirstFreeSpot;
	long MaxSpotsNumber;
	long CallSum;
	DWORD MaxTime; // msec

	CZMemRect *pMR;

	BYTE WorkBuffer[WorkBufferSize];
};

#endif






