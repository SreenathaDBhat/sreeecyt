////////////////////////////////////////////////////////////////////////////////////////
//
// An Interface to the Borland C UTS DLLs - zmemrect.dll
//
// Written By Karl Ratcliff 01/03/2001
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#ifndef __VCYMEMRECT_H
#define __VCYMEMRECT_H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

#include "zBmpTiff.h"

#define WrongOperation			(-1)
#define AcceptablePixelSizes	0x01010112	// (1<<pix)& ; 1,4,8,16,24
#define StartNumberOfMemRects 256
#define mrStat1DataLength 16
#define mrStat8DataLength 16

#define mrMaskLinkInfoPortion  7

////////////////////////////////////////////////////////////////////////////////////////
// Defines

// Data lengths for various arrays used by the UTS
#define MRMASKLINKINFOPORTION               7
#define MRSTAT1DATALENGTH                   16
#define MRRECTMASKDATALENGTH                11
#define MRMINMAXSUMINFODATALENGTH           12
#define MRMASKCALIPERDATALENGTH             16
#define MEMRECTLABELSIZE	                16
#define MEMRECTISEXTERN	                    0x7FFFFF33	// located in Extern memory
#define MEMRECTISOVERLAY	                0x7FFFFF34	// lays over another MR

////////////////////////////////////////////////////////////////////////////////////////
// Typedefs

// Original UTS code
// Handle information struct
typedef struct {
   DWORD Pixelsize;             // pixel size in bits; 0 means FREE
   DWORD OriginalPixelsize;		// original pixel size in bits, useful when camera image bitdepth is less than storage bitdepth
								// eg for 10, 12, 14 bit images (OriginalPixelsize) the storage bitdepth is 16 bit (Pixelsize) 
   DWORD Xext;     	            // width in pixels
   DWORD Yext;     	            // height in pixels
   DWORD Pitch;                 // pitch in bytes
   DWORD Memory;                // pointer to image data;
   					            // may be aligned on 8 byte boundary (see BaseMemory);
   					            // for MemRectIsExtern - pointer to Extern memory
   					            // for MemRectIsOverlay - handle of sublaying
   DWORD Memorysize;	        // in bytes;
   					            // values MemRectIsExtern and MemRectIsOverlay
                                // are possible
   DWORD Xoffset;               // horizintal offset for Overlays
   DWORD Yoffset;               // vertical offset for Overlays
   DWORD OwnOverList;           // head of the list of direct Overlays / 0
   DWORD SubOverList;           // next in the list of parent's Overlays / 0
   DWORD Tag;    		        // working var; for free entry
   	  					        // references to next free, or equal to 0
   DWORD BaseMemory;            // pointer from malloc(); 0 for extern/overlay rects
   char Label[MEMRECTLABELSIZE+4];	// working short text
} MemRectangle;

/*
typedef enum {
  SMARTCHOICELIB = 0,
  INTELIPLIB = 1,
  AICIPLIB = 2,
  TEEMMMXLIB = 3
} BackgroundLibraries;
*/


#ifdef UTS_EXPORTS
#define ZMEMRECT_API __declspec(dllexport)
#else
#define ZMEMRECT_API __declspec(dllimport)
#endif

class ZMEMRECT_API CZMemRect 
{
public:
	typedef enum {
		SMARTCHOICELIB = 0,
		INTELIPLIB = 1,
		AICIPLIB = 2,
		TEEMMMXLIB = 3
	} Libraries;

	CZMemRect();
	~CZMemRect();

	void  mrFreeOverlay (long hndl);

	void  mrFree (long hndl);

	void  mrFreeAllHandles(void);

	long FreeJustHandle(long hndl);


	long  mrGetExtendedInfo (long hndl,
		long *Xe, long *Ye, long *Pitch, void *Memory);

	long  mrPermute (long hndl, long grouplen, BYTE *permutation);

	long  mrGetTag(long hndl);

	long  mrGetRectInfo (long hndl, long *Xe, long *Ye);

	long  mrGetRectOriginalBPP (long hndl);

	long  mrCreate(long BitsPerPixel, long Xe, long Ye);

	long  mrCreateAs(long sample);

	long  mrCreateAsWithPix(long samplehndl, long pix);

	long  mrMove(long hndlfrom, long hndlto);

	long  mrCopyAs(long samplehndl);

	long  mrRGBtoGray(long hndl24, long hndl8, long red, long green, long blue);

	long  mrBytesToRGB(long hndl81, long hndl24, char *operation, long red, long green, long blue);

	long  mrConstPixelMask(long constant, char *operation, long hndlR, long hndlM);

	long  mrConstPixelNotMask(long constant, char *operation, long hndlR, long hndlM);

	long  mrMaskDrawCode (long hndl, long *x0, long *y0, void *codes, long count, long color);

	long  mrMoveSubRect (long hndlbig,long xleft, long ytop,long hndlsmall, char *way);

	long  mrPixelwise (long hndlA, char *operation, long hndlR);

	long  mrConstPixelwise (long constant, char *operation, long hndlR);

	long  mrStat8 (long hndl, char *operation,long threshold, double *stat);

	long  mrStat1 (long hndl, long *Data);

	long  mrStat1D (long hndl, double *Data);

	long  mrFindNext (long hndl,long *px, long *py, long pixtofind);

	long  mrOperMask (long hndl, char *operation);

	long  mrPutPixel (long hndl,long x, long y, long newval);

	long  mrReconFastD (long hndlLimit,	long hndlSeed, long xc, long yc, long *sizedata);

	long  mrReconFast (long hndlLimit,long hndlSeed, long xc, long yc);

	long  mrMinMaxSumInfo (long hndl, long *Data);

	long  mrNeiFun (long hndl, char *operation, long rep);

	long  mrReconstruct (long hndlLimit, long hndlSeed);

	long  mrSplit (long hsource, long *complist, long maxcomps);

	long  mrCreateOverlay(long onhndl, long xl, long yt, long xe, long ye);

	long  mrMoveOverlay(long hndl, long newxl, long newyt);

	long  mrResizeOverlay(long hndl, long newxl, long newyt, long newxe, long newye);

	long  mrSplitRange (long srcmask, long minmask, long maxmask, long minarea, long maxarea, long *data);

	long  mrConvert(long hndlfrom, long hndlto, long lparam);

	long  mrUseIntelIPlib(long command);

	long  mrRecOutContD (long hndlLimit,long hndlSeed, long xc, long yc, int nei4, long *sizedata);

	long  mrRecOutCont (long hndlLimit, long hndlSeed, long xc, long yc, int nei4);

	long  mrBorders (long hsource, long hborders, long nei);

	long  mrAlphaBeta(long hndl1, long hndl2, long hndl3, long alpha, long beta, long gamma, long *data);

	long  mrAlphaBetaLim(long hndl1, long hndl2, long hndl3, long alpha, long beta, long gamma,	long minval, long maxval, long *data);

	long  mrWriteNextRect(long hndl, short user, long invrows);

	long  mrWriteToFile(long hndl, char *pathname, long invrows);

	long  mrWriteToFileTIFF(long hndl, char *pathname, long invrows);

	long  mrAppendFile(long hndl, char *pathname, long invrows);

	long  mrDiffer (long hndl1, long hndl2);

	long  mrBorder2Masks (long h1, long h2, long hborders, char *nei);

	long  mrCodeContour (long masksrc, long maskcont, long *resdata, void *codes, long codeslim);

	long  mrGetPixel (long hndl, long x, long y);

	long  mrCodeGetPixels (long hndl, long *x0, long *y0, void *pcodes, long count, void *pixels);

	long  mrCodePutPixels (long hndl, long *x0, long *y0, void *pcodes, long count, void *pixels);

	long  mrColorDistance(long *hndls, long hnum, BYTE *hbase, long hdist);

	long  mrColorSaturImage(long *hndls, long hnum, long hsat);

	long  mrColorSaturTrans(long *hndls, long hnum,	double x1, double y1, double x2, double y2);

	long  mrConvolution1D (long hndl32,long *coeffs, long ncoeffs, long vert, long *pmin, long *pmax);

	long  mrConvolution2D (long hsrc32, long hdst32, long *coeffs, long xcoeffs, long ycoeffs, long *pmin, long *pmax);

	long  mrConvolution2Dpic (long hsrc32, long hdst32,	long hcoeffs, long *pmin, long *pmax);

	long  mrConvolution3by3 (long hndl8, long hndl32, long *coeffs,	long *pmin, long *pmax);

	long  mrCorrelation (long hndl1, long hndl2, long hndl3, long xc1, long yc1, long xc2, long yc2, double angle, double *data);

	long  mrCreateRoundPitch(long BitsPerPixel, long Xe, long Ye, long PitchRound);

	long  mrCrossHistogram (long h1, long h2, long hist, long hmask);

	long  mrZoomMask(long hndlfrom, long hndlto);

	long  mrCrossHistInvMask (long h1, long h2, long resmask, long crosshistmask);

	long  mrZoom(long hndlfrom, long hndlto, char *way);

	long  mrDistToMask (long hndl,	long *px, long *py);

	long  mrEllipse(long hndl, long xc, long yc,double a, double b, double phi,long color);

	void  mrFillMaskTab (long zerone, char *tabdescr, BYTE *tab);

	long  mrSubst (long hndl, void *table);

	long  mrGamma (long hndl, double gamma);

	long  mrGetLabel(long hndl, char *mem);

	long  mrGetOverlayPos (long hndl, long *Xoffs, long *Yoffs);

	long  mrHandleInfo (long hndl, char *mem, long memlim);

	long  mrHistogramLim (long hndl, void *res, long reslim);

	long  mrHistogram (long hndl, void *res);

	DWORD  mrBitmapToMemRect (HBITMAP hbmp, long invrows);

	long  mrHistogramMask (long hpic, long hmask, long *hist, long histlen);

	long  mrHistogramSub (long hndl,long xl, long yt, long xsub, long ysub,long xstep, long ystep,
		void *histmem, long histmax);

	long  mrHistogramXarray (long h32, long *xarray, long *hist, long histlen);

	long  mrHSItoRGB(long H, long S, long I, long R, long G, long B);

	long  mrInverseColumns (long hndl);

	long  mrInverseRows(long hndl);

	long  mrIplConvolSep (long hndl,
		long xn, long xc, long *xcoeffs, long xrshift,
		long yn, long yc, long *ycoeffs, long yrshift);

	long  mrIplConvolution (long hndl,
		long xf, long yf, long xc, long yc,
		long *coeffs, long rshift);

	long  mrIplFilter (long hndl, char *filtname,
		long xf, long yf, long xc, long yc);

	long  mrLabelToHandle(char *labeltext);

	long  mrMaskLine (long hndl,char *option, long color, long nsects, long *data);

	long  mrLineEndsInMask (long mask, long *px1, long *py1, long *px2, long *py2);

	long  mrLinkBorders (long hsource, long hborders, char *nei);

	long  mrListOfActive (long *mem, long limit);

	long  mrMakeExtern (void *rectmem, long OriginalBitsPerPixel, long Xe, long Ye, long pitch);

	long  mrMaskCalipers (long hndl,
		long *calipers, long *imin, long *imax,
		long *xc, long *yc);

	long  mrMaskConvexHull (long hndl, double thickness);

	long  mrMaskDistHist (long hmask,
		long cx, long cy, long *hist, long histlim);

	long  mrMaskLinkInfo (long hmask, void *data, long maxcomp, long minarea);

	void  mrMaskQuantiles (long hndl, long hmask, DWORD *res, long numof);

	long  mrMaskSubrectOnes (long hndl, long xl, long yt, long xe, long ye);

	long  mrMaskTable (long hndl, char *NameOrDescr,
		char *oper, long zeroone, long rep);

	long  mrMaskToNeiContrast (long h1, long h8, char *operation);

	long  mrThreshold (long hndl, long hndlmask, char *operation, DWORD threshold);

	long  mrWaterMask (long hpic8, long hprohibmsk, long hmask);

	long  mrMaskWatershed (long hndl8, long srcmask, long hresmask,	long minarea, long maxarea, long margin, long nopen);

	long  mrMoveExtern(char *extmem, char *direction, long hndl);


	long  mrMulDiv (long hndl, DWORD mul, DWORD div);

	long  mrMulDivPN (long hndl, long mulhndl, DWORD div);

	long  mrNumberOfActive (DWORD *usedmem, DWORD *portions);

	long  mrOvusDistThr(long hndl, long dist, long thr);

	long  mrPixelwiseMask (long hndlA, char *operation, long hndlR, long hndlM);

	long  mrPixelwiseNotMask (long hndlA, char *operation, long hndlR, long hndlM);

	long  mrProjection0 (long hndl, char *operation);

	long  mrProjection1 (long hndl,char *operation, void *res);

	void  mrQuantiles (long hndl, DWORD *res, long numof);

	long  mrRecRecon (long hndlLimit, long hndlSeed);

	long  mrRectMaskData (long hndl8, long hndl1, long *data);

	long  mrRectToBitMap (long hndl, long hwndpar);

	long  mrRedimension (long hndl, long BitsPerPixel, long Xe, long Ye);

	long  mrReplaceOrCopy (long hsrc, long *htarget);

	long  mrRGBtoHSI(long R, long G, long B, long H, long S, long I);
	
	long mrPixelwiseMask (long hndlA, char *operation, long hndlR, long hndlM, long neg);

	long  mrRotation (long hndl1, long hndl2, long hndl3, long xc1, long yc1, long xc2, long yc2, double angle);

	long  mrSetLabel(long hndl, char *label);

	long  mrSetTag(long hndl, long tagvalue);

	long  mrSlide (long hfrom, long htarg,long dx, long dy, long adjust);

	long  mrSmoothByZoom(long hndl,long iter, long zoomval,char *zoommode, char *neioper);

	long  mrSubstSubRect (long hndl, long xleft, long ytop,long xesub, long yesub, void *table);

	long  mrThickProfile (long hndl,void *points, long npoints,double x1, double y1, double x2, double y2, double rectwidth);

	long  mrTransp (long hsrc, long hdest);

	long  mrXYtoNA (long hX32, long hY32,long hN8, long hA8, double mul255, long flag);

	long  mrTwoDLUT (long hndlRow, long hndlCol, long hndlTab);

	long  mrGenTwoDLUT (long hndl);

	long  mrGetMaskPicInfo(long h1, long h8, long h32, long hSp32, long numComps);

	long  mrReadJPEGfile(char *pathname);

	
	long  mrDisplayMask (long windhndl, long xshift, long yshift,long hndl, long color, long mirror);

	long  mrMemRectToScreen (long hndl, long xl, long yt, long invrows);

	long  mrTextLine(long hndl, long x, long y,char *txt, long height, long color);

	DWORD mrScreenToMemRect (long xl, long yt, long wid, long hei, long invrows);

	long  mrWatershedByEDM (long hInput, long hOutputEDM, long hOutputUEP, long hOutputWS, char *operation, bool isWhiteBackground);

	void  mrCutImageFrag(long SrcImage, long CentreX, long CentreY, long DestImage, long *Xleft, long *Ytop);

private:
	MemRectangle *GetEntry(long hndl);
	MemRectangle *ActiveEntry (long hndl);
	void  Free(long hndl);
	void CreatePrivateHeap();
	void *My_malloc (DWORD nBytes);
	void *My_realloc(void *pCurrentMem, DWORD nBytes);
	void My_free(void * pMem);
	void AddMemRectangles(void);
	long GetNewHandle(void);
	long MakeNew(long OriginalBitsPerPixel, long Xe, long Ye, long PitchRound);
	long Bytewise(long hndlA, char *operation, long hndlR);
	long Bitwise(long hndlA, char *operation, long hndlR);
	long NeiFunction(long hndl, long oper, long rep);
	long WriteToFileType(long hndl, char *pathname, ImageFileFormat filetype, long invrows);
	long ConstBytewise(long constant, char *operation, long hndlR);
	long ConstBitwise(long constant, char *operation, long hndlR);
	long ConstPixelMask(long constant, char *operation, long hndlR, long hndlM, short neg);
	long GetBasedInfo(long hndl, long *Xoffs, long *Yoffs, long *Xe, long *Ye, long *Pitch, void *baseMem);
	long PixelwiseMask (long hndlA, char *operation, long hndlR, long hndlM, long neg);

	HANDLE	HeapHandle;

	unsigned long NumberOfMemRects;
	unsigned long FreeMemRect;
	MemRectangle *MemRectAddress;
	bool ThisRectIsBased;
};


#endif                                      //  #ifndef __VCMEMRECT_H
