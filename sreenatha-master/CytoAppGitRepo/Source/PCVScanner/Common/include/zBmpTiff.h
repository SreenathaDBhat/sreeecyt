#ifndef ZBMPTIFF_H
#define ZBMPTIFF_H


/*************************     T A G s     *************************/

#define NewSubFileType   254
#define SUBFILETYPE      255
#define IMAGEWIDTH       256
#define IMAGELENGTH      257
#define BITSPERSAMPLE    258
#define COMPRESSION      259
#define PHOTOMETRICINTERPRETATION 262
#define DocumentName     269
#define IMAGEDESCRIPTION 270
#define STRIPOFFSETS     273
#define SAMPLESPERPIXEL  277
#define RowsPerStrip     278
#define StripByteCounts  279
#define MinSampleValue   280
#define MaxSampleValue   281
#define XResolution      282
#define YResolution      283
#define PlanarConfiguration  284
#define GRAYRESPONSEUNIT 290
#define ResolutionUnit   296
#define Software         305
#define DateTime         306
#define ColorMap         320

/*************************  D E F I N E s  ****************************/

#define INTEL        0x4949     /* II - TIFF, Intel byte order    */
#define MOTOROLA     0x4d4d     /* MM - TIFF, Motorola byte order */
#define TIFFVERSION  42
#define BITMAPmark   0x4d42     /* BM - Windows BMP file format   */

#define MaxHeader      8192


/****************************** BMP-structure *************************/

// Data is read from disk and interpreted by this structure
// therefore it needs to be tightly packed (on byte boundary)
#pragma pack(1)
typedef struct
{
    BITMAPFILEHEADER File;
    BITMAPINFOHEADER Info;
} AI_BITMAPFILEINFO;
#pragma pack()

typedef enum
{
   BADformat  = 0,
   TIFformat  = 1,
   BMPformat  = 2
} ImageFileFormat;


#ifdef BMPTIFF_EXPORTS
#define BMPTIFF_API __declspec(dllexport)
#else
#define BMPTIFF_API __declspec(dllimport)
#endif

BMPTIFF_API long _stdcall FreeTheData (short usernum);

BMPTIFF_API short _stdcall ReadFileHeader (char *pathname);

BMPTIFF_API void _stdcall AddComment (short usernum, char *text, short tlen);

BMPTIFF_API short _stdcall ClearAllTB (void);

BMPTIFF_API short _stdcall CreateTheHeader (ImageFileFormat type,
								   short pix, long xe, long ye,
								   char *lut, short nlut);

BMPTIFF_API short _stdcall GetHeaderInfo (short usernum,
	  short *pix, long *xe, long *ye,
	  char *comment,     // NULL is OK
	  short *commentlen, // before: limit, after: length
	  long *compress,    // NULL is OK
	  short *photomint);  // NULL is OK

BMPTIFF_API short _stdcall GetImageFileInfo (char *path,
				long *pix, long *xe, long *ye);

BMPTIFF_API short _stdcall GetLUTinfo (short usernum,
 	        void **pointer_to, long *formal_number, long *size_in_bytes);
           /* NULL is OK for any pointer */

BMPTIFF_API short _stdcall GiveTIFFtag (short usernum, short tag,
										 short *type, long *numval, void **valpnt);

BMPTIFF_API long _stdcall MakeHeaderFile (short usernum, char *path);

BMPTIFF_API short _stdcall ReadNextRows (short usernum, \
						char *where, long rows, long *placed);

BMPTIFF_API long _stdcall RewriteHeader (short usernum);

BMPTIFF_API long _stdcall SetRowOffset (short usernum, long rownum);

BMPTIFF_API short _stdcall ShowFileHeader (char *path,
								  char *outpar, WORD limit);

BMPTIFF_API short _stdcall TIFFtagValuePnt (short usernum, short tag, void **valpnt);

BMPTIFF_API DWORD _stdcall GetThePixel(void);

BMPTIFF_API DWORD _stdcall GetTheWidth(void);    

BMPTIFF_API DWORD _stdcall GetTheLength(void);   

BMPTIFF_API DWORD _stdcall GetTheFileSize(void); 

BMPTIFF_API DWORD _stdcall GetTheRowBytes(void);

BMPTIFF_API DWORD _stdcall GetTheDataSize(void);

BMPTIFF_API short _stdcall WriteNextRows (short usernum, char *buff, long rows);

BMPTIFF_API ImageFileFormat _stdcall HeaderFormat (short usernum);

#endif 













