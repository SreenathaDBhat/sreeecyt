#ifndef ZFILES_H
#define ZFILES_H

#ifdef ZFILES_EXPORTS
#define ZFILES_API __declspec(dllexport)
#else
#define ZFILES_API __declspec(dllimport)
#endif

ZFILES_API long _stdcall GetFileAccess  (char *pathname, long *filesize);

ZFILES_API long _stdcall IsExisting (char *path);

ZFILES_API long CreateAndWriteFile (char *path, char *buff, long size, WORD append);

ZFILES_API long _stdcall WriteToFile (char *path, char *buff, long size);

ZFILES_API long _stdcall IsDirectory (char *path);

ZFILES_API long _stdcall FreeFile (char *path);

ZFILES_API long _stdcall FreeDirectory (char *path);

ZFILES_API long _stdcall FileNameByNumber (char *path,
                        char *buff, long bufsize, long namenum);

ZFILES_API long _stdcall AppendFile (char *path, char *buff, long size);

ZFILES_API long _stdcall MakeDirectory (char *path);

ZFILES_API long _stdcall MakeFileCopy (char *srcname, char *destname);

ZFILES_API long _stdcall ReadFromFile  (char *path, char *buff,
					long limit, long fileoffset, long *filesize);

ZFILES_API long _stdcall ParseFileName (char *pathname,
            long *drive, long *dir, long *name, long *ext);

ZFILES_API long _stdcall Subdirectories (char *path,
					char *buff, long bufsize,
                    DWORD separator, long *filled);

ZFILES_API long SysTimeToDateAndTime  (SYSTEMTIME *systime, DWORD *adate, DWORD *atime);

ZFILES_API long SysTimeToStrDateTime  (SYSTEMTIME *systime, char *dt);

ZFILES_API long FileTimeToDateAndTime  (FILETIME *ftime, DWORD *adate, DWORD *atime);

ZFILES_API long FileTimeToStrDateTime  (FILETIME *ftime, char *dt);

ZFILES_API long _stdcall ListOfFiles (char *path,
					char *buff, long bufsize,
                    DWORD comsep, long *filled);

#endif