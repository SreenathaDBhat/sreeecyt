#ifndef ZGENERIC_H
#define ZGENERIC_H

#ifdef GENERIC_EXPORTS
#define GENERIC_API __declspec(dllexport)
#else
#define GENERIC_API __declspec(dllimport)
#endif

GENERIC_API void _stdcall GroupPermut (char *src, char *dest,
				DWORD grouplen, BYTE *permutation, DWORD size);

GENERIC_API long _stdcall CodeLine(void *dst, long dstsize, long deltax, long deltay);

GENERIC_API long _stdcall CodeCircle(void *dst, long dstsize, long rad, long radrad);

GENERIC_API long _stdcall CodeContour (void *dst, long dstsize,
                void *pmask, long xe, long ye, long pitch,
                long xstart, long ystart, long command);

GENERIC_API void _stdcall CodeUpdateXY(long *X, long *Y, BYTE code);

GENERIC_API void _stdcall CodesToCoords (void *pcodes, long ncodes,
                long xstart, long ystart,
                void *resx, long pitchx,
                void *resy, long pitchy);

GENERIC_API long _stdcall CodeCurvature (void *pcodes, long ncodes,
                long xstart, long ystart,
                void *curvature, long *res,
                long halfstepmin, long halfstepmax, long thresh);

GENERIC_API long _stdcall CodeEquation2(void *dst, long dstsize,
			double *A, long xstart, long ystart);

GENERIC_API long _stdcall CodeEllipse(void *dst, long dstsize,
                double a, double b, double angle,
                long *xstart, long *ystart);

GENERIC_API long _stdcall CodeSplineSeg(void *dst, long dstsize,
                long *finalx, long *finaly,
                double xa1, double xa2, double xa3,
                double ya1, double ya2, double ya3);

GENERIC_API long _stdcall CodesLength (void *pcodes, long ncodes);

GENERIC_API void _stdcall ReorderBufs (void *mem, DWORD len1, DWORD len2);

GENERIC_API void _stdcall VectorInfo (void *v, long pitchv, long nv, long type,
						  void *pmin, void *pmax, void *psum,
                          long *pimin, long *pimax);


GENERIC_API void _stdcall AddVector (void *x, void *y,
				long pitchx, long pitchy,
                long n, double factor);


GENERIC_API long _stdcall BitsDiffer (
			void *p1, void *p2,
            long xe, long ye,
            long pitch1, long pitch2);

GENERIC_API void _stdcall BitTranspose (
			void *p1, void *p2,
            long x1, long y1,
            long pitch1, long pitch2);

GENERIC_API long _stdcall BMtoClpbrd (long wndHandle, // NULL is Ok
                        long bmpHandle);

GENERIC_API DWORD _stdcall ScreenToBitMap (long xl, long yt, long wid, long hei);

GENERIC_API DWORD _stdcall Convert32to8 (
			void *p32, DWORD pitch32,
            DWORD xe, DWORD ye,
			void *p8, DWORD pitch8,
            long shift);

GENERIC_API void _stdcall Convert8to32 (
			void *p8, DWORD pitch8,
            DWORD xe, DWORD ye,
			void *p32, DWORD pitch32,
            DWORD shift);

GENERIC_API long _stdcall FindSubstr (
					void *ptext,
                    DWORD *offs, DWORD limit,
                    void *substr, DWORD sublen,
                    DWORD usecase);

GENERIC_API long _stdcall FindByte (
					void *ptext,
                    DWORD *offs, DWORD limit,
                    void *plist, DWORD listlen);

GENERIC_API void _stdcall Gamma8Table(BYTE *tab, double Gamma);


GENERIC_API DWORD _stdcall GetLong (char  *pnt,DWORD *offs,DWORD  limit,long  *value);

GENERIC_API DWORD _stdcall GetDouble (
					char   *pnt,
               DWORD  *offs,
               DWORD   limit,
               double *value);

GENERIC_API long _stdcall GetVicinity (void *pmask,
                long xe, long ye, long pitch,
                long Xc, long Yc);

GENERIC_API long _stdcall GlobHndlToClpbrd (
						char *FormatName,
                        DWORD globhndl);

GENERIC_API long _stdcall HistInfo(
				long *hist, long histlen, double *info);

GENERIC_API void _stdcall IndexSort (long *indmem, long numof,
      long (_stdcall *compfunc)(long, long, void *),
      void *extrainfo); // NULL is OK

GENERIC_API long _stdcall CompLongArray(long i1, long i2, void *extrainfo);

GENERIC_API long _stdcall CompDwordArray(long i1, long i2, void *extrainfo);

GENERIC_API long _stdcall CompShortArray(long i1, long i2, void *extrainfo);

GENERIC_API long _stdcall CompUshortArray(long i1, long i2, void *extrainfo);

GENERIC_API long _stdcall CompDoubleArray(long i1, long i2, void *extrainfo);

GENERIC_API long _stdcall CompFloatArray(long i1, long i2, void *extrainfo);

GENERIC_API long _stdcall CompCharArray(long i1, long i2, void *extrainfo);

GENERIC_API long _stdcall CompByteArray(long i1, long i2, void *extrainfo);

GENERIC_API long _stdcall CompCharPntArray(long i1, long i2, void *extrainfo);

GENERIC_API long _stdcall CompBytePntArray(long i1, long i2, void *extrainfo);

GENERIC_API void _stdcall IndexSortElems (void *src, long elemsize, long numof,
      long *dst, long comparetype, long decrease);

GENERIC_API long _stdcall CompWordArray(long i1, long i2, void *extrainfo);

GENERIC_API void _stdcall IndexSortWordList(char *list, long *offslen,
						long *inds, long wrdcnt);

GENERIC_API void _stdcall InversePermut (long *src, long numof, long *dst);

GENERIC_API double _stdcall Linear2by2(double *A, double *B,
                        double *X, double eps);

GENERIC_API long _stdcall LineEndsInRect (
				long xmin, long ymin, long xmax, long ymax,
                long *px1, long *py1, long *px2, long *py2);

GENERIC_API void _stdcall LinRegr (void *x, void *y,
				long pitchx, long pitchy, long n,
                double *m, double *b);

GENERIC_API long _stdcall LookUpTable (char *buffile, char *command);

GENERIC_API long _stdcall MatrInverse (double *Src, double *Inv, long n,
			double eps, double *pdet);

GENERIC_API double _stdcall ScalProd (void *x, void *y,
				long pitchx, long pitchy, long n);

GENERIC_API void _stdcall MatrMult (double *M1, double *M2, double *Mprod,
				long cols1rows2, long nrows1, long ncols2);


GENERIC_API void _stdcall MatrTranspose (double *M, double *T, long cols, long rows);


GENERIC_API double _stdcall Scalp (double *x, double *y, long n);

GENERIC_API void _stdcall MatrMmultTrM (double *M, double *N, long cols, long rows);

GENERIC_API void _stdcall MatrTrMmultM (double *M, double *N, long cols, long rows);

GENERIC_API void _stdcall MovMemory (
            void *from,
            void *to,
            DWORD len);

GENERIC_API long  _stdcall MemFromClpbrd (
						char *FormatName,
                  char *mem,
                  DWORD limit);

GENERIC_API DWORD _stdcall MemToBitMap (long hwndpar, long pix,
                long xe, long ye,
                char *prect);

GENERIC_API long _stdcall MemToClpbrd (
							char *FormatName,
                     char *mem,
                     DWORD memlen);

GENERIC_API long _stdcall MemToHex (void *mem, long len, void *to, long wlen);

GENERIC_API long _stdcall MinsAndMaxs (void *v, long nv, long typev,
						long *presult, long reslen);

GENERIC_API DWORD _stdcall MoveSubRect (
							char *bigrect,
                     DWORD pitch, DWORD numlines,
                     DWORD xl, DWORD yt,
							char *smallrect,
                     DWORD xe, DWORD ye,
                     long direction);

GENERIC_API void _stdcall PermutateInPlace (void *mem, long mempitch,
					long *permut, long numof);


GENERIC_API DWORD _stdcall ToBegWord (
					char *pnt,
               DWORD offs,
               DWORD limit);


GENERIC_API DWORD _stdcall WordLength (
					char *pnt,
               DWORD offs,
               DWORD limit);


GENERIC_API DWORD _stdcall GetTheWord (
					char  *ptext,
               DWORD *offs,
               DWORD  limit);

GENERIC_API DWORD _stdcall ParseWordList(char *list, DWORD listsize,
				 DWORD *offslen, DWORD limit);


GENERIC_API long _stdcall ParseAndSortWlist(char *list, long listsize,
				 long *offslen, long limit);


GENERIC_API void _stdcall PolynomMult (double *R, double *X, double *Y,
                long n, long m);


GENERIC_API void _stdcall PolynomMultLong (long *R, long *X, long *Y,
                long n, long m, long add);


GENERIC_API void _stdcall PolynomPlus (double *R, double *X, double *Y,
                long nx, long ny,
				double Ax, double Ay);

GENERIC_API long _stdcall LinSyst (double *A, double *B, double *X, long n);


GENERIC_API long _stdcall PolyRegr (double *X, double *Y, long n,
					long pitchx, long pitchy,
					long terms, double *coefs);


GENERIC_API void _stdcall ReorderBufs (void *mem, DWORD len1, DWORD len2);


GENERIC_API void _stdcall ReverseRows (
								char *src,
                        char *dest,
								DWORD xe,
                        DWORD ye);



GENERIC_API DWORD _stdcall RGB8toHSI(long rv, long gv, long bv,
							long *Hp, long *Sp, long *Ip);


GENERIC_API DWORD _stdcall HSItoRGB8(long hv, long sv, long iv,
							long *Rp, long *Gp, long *Bp);


GENERIC_API void _stdcall SmoothLongs (
			long *pmem, long lmem, short points);


GENERIC_API void _stdcall SortInPlace (void *parmem, long elsize,
					long numof, long operation);


GENERIC_API DWORD _stdcall Substitute(char *mem, DWORD memsize,
			   			char *where, DWORD limit,
                		char *what,  DWORD whatsize,
                		char *to,    DWORD tosize);


GENERIC_API DWORD _stdcall SubwindToBitMap (long wndHandle,
								long xl, long yt, long wid, long hei);


GENERIC_API DWORD _stdcall WindowToBitMap (long wndHandle);


GENERIC_API void _stdcall TableSubst (char *src, char *dest,
				DWORD size, void *ptable);


GENERIC_API long  _stdcall TextFromClpbrd (
                  char *mem,
                  DWORD limit);


GENERIC_API long _stdcall TextToClpbrd (
                     char *mem,
                     DWORD memlen);


GENERIC_API void _stdcall VectorInfo (void *v, long pitchv, long nv, long type,
						  void *pmin, void *pmax, void *psum,
                          long *pimin, long *pimax);


GENERIC_API DWORD _stdcall WordInText (
							char *text,
                     DWORD *offs, DWORD limit,
                     char *pword, DWORD wordlen);


GENERIC_API DWORD _stdcall WordNotInList(
				void *src, DWORD *srcoffs, DWORD srcsize,
                void *list, DWORD listsize);


GENERIC_API DWORD _stdcall WordToWord(char *src, DWORD srcsize,
			   			char *res, DWORD limit,
                		char *what, DWORD whatsize,
                        char *into, DWORD intosize);

GENERIC_API double _stdcall GetCPUclockSpeed (void);

GENERIC_API long _stdcall GetCPUtype (void);

GENERIC_API double _stdcall GetTimeMsec (void);

GENERIC_API void _stdcall InverseBytes (char *buffer, DWORD buffersize);

GENERIC_API void _stdcall LeftToRight (char *src, char *dest,
				DWORD size, long invbytes);

GENERIC_API long _stdcall FillHexLine (void *src, long srclen, void *buff, BYTE *table);


GENERIC_API long _stdcall HexDump (void *src, long srclen, void *buff,
					DWORD addr, DWORD separator, BYTE *table);


GENERIC_API long _stdcall HexToMem (void *hexs, long hexlen, void *to);

GENERIC_API DWORD _stdcall GetTheWordBack (
			   void  *text,
               DWORD *offset,
               DWORD  limit);


GENERIC_API long _stdcall FindPieceOfWord (
					char *where,
               DWORD *offs,
               DWORD limit,
               char *what,
               DWORD whatlen);


GENERIC_API long _stdcall FindWordFromList(char *text,
                     DWORD *offs, DWORD limit,
                     char *wordlist, DWORD listlen,
                     DWORD *wordlen, DWORD usecase);

GENERIC_API long _stdcall GetDirection(long deltax, long deltay);


GENERIC_API long _stdcall ArcCenter(POINT *points, long npoints,
            double distmin, double *centerx, double *centery);

long _stdcall DisplayMem (
		long windhndl, 		// window where to display
		long xl, long yt,   // left top corner coordinates
		char *prect,		// pointer to rectangular piece of memory
		long bitspix, long width, long height, 	// image description
		long flip);


#endif 