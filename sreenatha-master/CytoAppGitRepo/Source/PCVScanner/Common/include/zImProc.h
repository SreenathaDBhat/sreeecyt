#ifndef ZIMPROC_H
#define ZIMPROC_H

#ifdef IMPROC_EXPORTS
#define IMPROC_API __declspec(dllexport)
#else
#define IMPROC_API __declspec(dllimport)
#endif

IMPROC_API long _stdcall MaskDrawCode (void *pmask,
            long xe, long ye, long pitch,
            long *x0, long *y0,
            void *pcodes, long count,
            long pen);

IMPROC_API DWORD _stdcall PictHist (
			BYTE *Pict, DWORD Xe, DWORD Ye, DWORD Pitch,
            DWORD *hist);


IMPROC_API void _stdcall  Spline3by2 (long count, double *incs, double *m3by2);

IMPROC_API void _stdcall  SplineStart (long count, double *incs,
					double *a1, double *a2, long mode);

IMPROC_API long _stdcall BitRowToRow (char *mem1, DWORD len1,
				char *mem2, DWORD len2);

IMPROC_API void _stdcall BitsToRGB (
			BYTE *mem1, DWORD xe, DWORD ye, DWORD pitch1,
			BYTE *mem24, DWORD pitch24,
			long red, long green, long blue, long add);

IMPROC_API void _stdcall BytesToRGB (
			BYTE *mem8, DWORD xe, DWORD ye, DWORD pitch8,
			BYTE *mem24, DWORD pitch24,
			long red, long green, long blue, long add);


IMPROC_API long _stdcall  GetLineCode (void *pcodes, long bufsize,
                  long linemode, long count, long *pdata,
                  long *pxstart, long *pystart);


IMPROC_API DWORD _stdcall GrayCorrelation (
			void *m1, long xe1, long ye1, long pitch1,
			void *m2, long xe2, long ye2, long pitch2,
			void *m3, long pitch3,
         long xc1, long yc1, long xc2, long yc2,
         double angle, double *data);

				  
IMPROC_API void _stdcall GrayRotation (
			void *m1, long xe1, long ye1, long pitch1,
			void *m2, long xe2, long ye2, long pitch2,
			void *m3, long pitch3,
         long xc1, long yc1, long xc2, long yc2, double angle);


IMPROC_API long _stdcall Mask3bord (
			void *m1, long xe, long ye, long pitch1,
         	void *m2, long pitch2,
         	void *m3, long pitch3);

IMPROC_API long _stdcall MaskBitwise (
			void *mem1,
			DWORD xe, DWORD ye, DWORD pitch1,
            void *mem2, DWORD pitch2,
            short oper);


IMPROC_API long _stdcall  MaskConvex (
				void *mem, DWORD xe, DWORD ye, DWORD pitch,
                long pixperc);


IMPROC_API long _stdcall  MaskCalipers (
				void *mem, DWORD xe, DWORD ye, DWORD pitch,
                long *calipers, long *imin, long *imax,
                long *xc, long *yc);


IMPROC_API long _stdcall MaskDistToPoint (
					BYTE *pmem,
               long xl, long yt,
               long xe, long ye, long pitch,
               long *xp, long *yp);


IMPROC_API DWORD _stdcall MemMaskMem (
				BYTE *mem1, BYTE *mem2, BYTE *mask,
				DWORD xe, DWORD ye,
            	DWORD pitch1, DWORD pitch2, DWORD pitchmask,
            	short oper);


IMPROC_API void _stdcall MaskOperMask (
				BYTE *BitMem1,
                DWORD Xe, DWORD Ye, DWORD Pitch1,
				BYTE *BitMem2, DWORD Pitch2,
                short oper);

IMPROC_API long _stdcall  MaskOper (
				BYTE *Mask, DWORD Xe, DWORD Ye, DWORD Pitch,
               	WORD Oper, long *Data);

IMPROC_API void _stdcall MaskReconMask (
				BYTE *BitMem1,
                DWORD Xe, DWORD Ye, DWORD Pitch1,
				BYTE *BitMem2, DWORD Pitch2);

IMPROC_API void _stdcall MaskFillHoles (
				BYTE *BitMask1,
                DWORD Xe, DWORD Ye, DWORD Pitch1,
				BYTE *BitMask2, DWORD Pitch2);

IMPROC_API long _stdcall  MaskLine (void *mask, long xe, long ye, long pitch,
                  long linemode, long pen, long count, long *data);

IMPROC_API void _stdcall MaskNei (BYTE *mem, DWORD xe, DWORD ye,
				DWORD pitch, long oper, long rep);

IMPROC_API long _stdcall  MaskNeiTable (
				BYTE *mask1, DWORD xe, DWORD ye, DWORD pitch1,
               	BYTE *mask2, DWORD pitch2, BYTE *table);

IMPROC_API long _stdcall  MaskNeiTableR (
				BYTE *mask, DWORD xe, DWORD ye, DWORD pitch,
                BYTE *table);

IMPROC_API long _stdcall MaskOnes (
				BYTE *pmem,
                DWORD xl, DWORD yt,
                DWORD xe, DWORD ye, DWORD pitch);

IMPROC_API long _stdcall  MaskOperStat(
				BYTE *Mask, DWORD Xe, DWORD Ye, DWORD Pitch,
               	double *Data);

IMPROC_API void _stdcall MaskReconFast (
				BYTE *BitMem1,
                DWORD Xe, DWORD Ye, DWORD Pitch1,
				BYTE *BitMem2, DWORD Pitch2,
                DWORD *xmin, DWORD *ymin,
                DWORD *xmax, DWORD *ymax);

IMPROC_API void _stdcall MaskRotation (
			void *m1, long xe1, long ye1, long pitch1,
			void *m2, long xe2, long ye2, long pitch2,
			void *m3, long pitch3,
         long xc1, long yc1, long xc2, long yc2, double angle);

IMPROC_API void _stdcall  MaskToNeiContrast (
				BYTE *mem1, DWORD xe, DWORD ye, DWORD pitch1,
               	BYTE *mem8, DWORD pitch2, long oper);

IMPROC_API DWORD _stdcall MemConstMem (
					BYTE *mem1, BYTE *mem2, long psize,
					WORD cnst, short oper);

IMPROC_API DWORD _stdcall MemMaskConstMem (
			BYTE *mem1, BYTE *mem2, BYTE *mask,
			DWORD xe, DWORD ye,
            DWORD pitch1, DWORD pitch2, DWORD pitchmask,
            WORD cnst, short oper);

IMPROC_API DWORD _stdcall MemMaskConstMem32 (
			void *mem1, void *mem2, void *mask,
			DWORD xe, DWORD ye,
            DWORD pitch1, DWORD pitch2, DWORD pitchmask,
            DWORD cnst, DWORD oper);

IMPROC_API DWORD _stdcall MemMaskMemMem32 (
			void *mem1, void *mem2, void *mem3, void *mask,
			DWORD xe, DWORD ye,
            DWORD pitch1, DWORD pitch2, DWORD pitch3,
            DWORD pitchmask, DWORD oper);

IMPROC_API DWORD _stdcall MemMulDivMem (
					BYTE *mem1, BYTE *mem2, DWORD size,
					WORD mul, WORD div);

IMPROC_API DWORD _stdcall MemMaskData (
				BYTE *mem, DWORD xe, DWORD ye, DWORD mempitch,
				BYTE *mask, DWORD maskpitch,
                long *data);

IMPROC_API void _stdcall MemNeiMem (BYTE *Mem1,
               DWORD Xe, DWORD Ye, DWORD Pitch1,
					BYTE *Mem2, DWORD Pitch2, short oper);

IMPROC_API short _stdcall MemOpMem (
				BYTE *mem1, BYTE *mem2,
				DWORD size, short oper);

IMPROC_API DWORD _stdcall MemThreshMem (
					BYTE *mem1, BYTE *mem2, DWORD size,
					WORD threshold, short oper);

IMPROC_API DWORD _stdcall MemThreshVal (
				BYTE *mem, DWORD size,
				WORD threshold, short oper);

IMPROC_API void _stdcall NeiMem (BYTE *mem, DWORD xe, DWORD ye,
				DWORD pitch, long oper, long rep);

IMPROC_API DWORD _stdcall MemToBitMask (
				BYTE *ByteMem, DWORD Xe, DWORD Ye, DWORD BytePitch,
				BYTE *BitMem, DWORD BitPitch, DWORD thresh, DWORD operation);

IMPROC_API void _stdcall OutContFast ( BYTE *mem,
              DWORD xe, DWORD ye, DWORD pitch,
              DWORD xmin, DWORD ymin,
              DWORD xmax, DWORD ymax, int nei4);

IMPROC_API DWORD _stdcall PictHistXarray (
			DWORD *Pmem, DWORD Xe, DWORD Ye, DWORD Pitch,
            DWORD *Xarray, DWORD *Hist, DWORD XHlen);

IMPROC_API DWORD _stdcall PictThreshStat (
				BYTE *Pict, DWORD Xe, DWORD Ye, DWORD Pitch,
               	DWORD threshold, DWORD invert, double *stat);

IMPROC_API DWORD _stdcall Projection (
				BYTE *rect, BYTE *rowcol,
				DWORD xe, DWORD ye, short rc, short oper);

IMPROC_API DWORD _stdcall Projection32 (
				void *rect, DWORD *rowcol,
				DWORD xe, DWORD ye, DWORD pitch,
                long rc, long oper);

IMPROC_API short _stdcall ReplicateMem (
				BYTE *from, DWORD pix, DWORD xe,
				BYTE *to, DWORD rep);

IMPROC_API void _stdcall RGBtoGray (
			BYTE *mem8, DWORD xe, DWORD ye, DWORD pitch8,
			BYTE *mem24, DWORD pitch24,
			long red, long green, long blue);

IMPROC_API void _stdcall ShiftBitMask (
				BYTE *BitMem1,
                DWORD xee, DWORD yee, DWORD Pitch1,
				BYTE *BitMem2, DWORD Pitch2,
                WORD shift, short lr);

IMPROC_API DWORD _stdcall ZoomRectFast (
			BYTE *pict1, DWORD xe1, DWORD ye1, DWORD pitch1,
			BYTE *pict2, DWORD xe2, DWORD ye2, DWORD pitch2, DWORD portion);

IMPROC_API DWORD _stdcall ZoomRectSmooth (
			BYTE *pict1, DWORD xe1, DWORD ye1, DWORD pitch1,
			BYTE *pict2, DWORD xe2, DWORD ye2, DWORD pitch2);

IMPROC_API DWORD _stdcall MemToBitMask32 (
				void *Mem, DWORD Xe, DWORD Ye, DWORD Pitch,
				void *BitMem, DWORD BitPitch, DWORD threshold, DWORD oper);

IMPROC_API DWORD _stdcall MemToValue (BYTE *mem, DWORD size, short oper);


IMPROC_API void _stdcall  MinMaxDirection (
			BYTE *aa, long pitcha,
			BYTE *bb, long pitchb,
		    long xe, long ye,
		    long oper, long direct);


#endif


/*********************** C O N S T A N T s *************************/

// MemOpMem() operations:
#define momZERO 	   0    // set all bits to zero
#define momAND 		   1    // A & B
#define momMORE   	   2    // A & ~B  (1 if A > B)
#define momCOPY1 	   3    // A  (copy A into B)
#define momLESS 	   4    // ~A & B  (1 if A < B)
#define momCOPY2 	   5    // B  (copy B {or const} into B)
#define momEXOR 	   6    // A ^ B  (mod 2)
#define momOR 		   7    // A | B
#define momNOR 		   8    // ~(A | B)  (not OR)
#define momEQUAL 	   9    // ~(A ^ B)  (1 if A = B)
#define momNOT2 	  10    // copy ~B into B
#define momEQMORE 	  11    // A | ~B  (1 if A >= B)
#define momNOT1 	  12    // copy ~A into B
#define momEQLESS 	  13    // ~A | B  (1 if A <= B)
#define momNAND 	  14    // ~(A & B)  (not AND)
#define momFULL 	  15    // set all bits to 1 (bytes to 255)
#define momMIN 		  16    // A MIN B
#define momMAX 		  17    // A MAX B
#define momPLUS 	  18    // (A + B) mod 256
#define momSPLUS 	  19    // (A + B) min 255 (saturation)
#define momMINUS 	  20    // (A - B) mod 256
#define momIMINUS 	  21    // (B - A) mod 256
#define momSMINUS 	  22    // (A - B) max 0   (saturation)
#define momSIMINUS 	  23    // (B - A) max 0   (saturation)
#define momLOWto0 	  24    // if A >= B then A else 0
#define momHIGHtoFF   25    // if A <= B then A else 255
#define mom0orFF 	  26    // if A < B then 0 else 255
#define momAbsDiff 	  27    // if A < B then (B - A) else (A - B)
#define momAbsDiff8	  28    // Abs(A - B) MIN (256 - Abs(A - B))

// MemToVal() operations
#define mtvMIN 		16     // MIN of byte values
#define mtvMAX  	17     // MAX of byte values
#define mtvSUM 		18     // SUM of byte values
#define mtvONES 	19     // number of ONE bits
#define mtvZEROS 	20     // number of ZERO bits

// MemThreshVal() operations
#define mtvBELOW 	  16    // number of bytes below than threshold
#define mtvGREATER    17    // number of bytes greater than threshold
#define mtvEQUAL      18    // number of bytes equal to threshold
#define mtvSUMBELOW   19    // sum of bytes below than threshold
#define mtvSUMGREATER 20    // sum of bytes greater than threshold
#define mtvSUMGEQ     21    // sum of bytes equal to or greater than threshold

// MemThreshMem() operations
#define mtmBZERO 	16    // below-than-threshold bytes => 0
#define mtmBTHRESH 	17    // below-than-threshold bytes => threshold
#define mtmGFULL 	18    // greater-than-threshold bytes => 255
#define mtmGTHRESH  19    // greater-than-threshold bytes => threshold
#define mtmTHRESH  	20    // below-than-threshold bytes => 0,
						  //  greater-than or equal-to-threshold bytes => 255
						  //  (returns the number of)

// Projection() operations
#define prjMIN 		16    // MIN in row/column
#define prjMAX 		17    // MAX in row/column
#define prjSUM 		18    // SUM of bytes in row/column

// MemToBitMask() operations
#define mtbmGE 		16   // below-than-threshold bytes => 0
						 //  greater-than or equal-to-threshold bytes => 1
#define mtbmBE 		17   // greater-than-threshold bytes => 0 _
						 //  below-than or equal-to-threshold bytes => 1
#define mtbmEQ 		18   // equal-to-threshold bytes => 0, others => 1
#define mtbmNE 		19   // equal-to-threshold bytes => 1, others => 0

// MemMaskMem() and MemMaskConstMem() operation modifiers
#define mmcmNEG    128   // (oper | mmcmNEG) means "use inverse mask"
#define mmcmIGNORE  64   // (oper | mmcmIGNORE) means "ignore mask"

// MemMaskConstMem() operation (see also MemOpMem() operations 0 to 26)
#define mmcmMERGE   47   // if (mask) then B=A  else B=cnst;

// MaskOperMask() operations
#define mnmMIN 		  0  // min of self and 8 bit neighbors;
#define mnmMAX 		  1  // max of self and 8 bit neighbors;
#define mnmFRAME 	  2  // frame (1 on sides, 0 inside);
#define mnmNFRAME 	  3  // not frame (0 on sides, 1 inside);
#define mnmShadowLR1  4  // expand ones left to right
#define mnmShadowRL1  5  // expand ones right to left
#define mnmShadowTB1  6  // expand ones top to bottom
#define mnmShadowBT1  7  // expand ones bottom to top
#define mnmShadowLR0  8  // expand zeros left to right
#define mnmShadowRL0  9  // expand zeros right to left
#define mnmShadowTB0 10  // expand zeros top to bottom
#define mnmShadowBT0 11  // expand zeros bottom to top
#define mnm124 		 12  // averaging with matrix {1,2,1; 2,4,2; 1,2,1}/16
#define mnmMED  	 	13  // median of self and 8 bit neighbors;
#define mnmBLUR 	 	14  // averaging with matrix {1,1,1; 1,1,1; 1,1,1}/9
#define mnmMIN4 		 20  // min of self and 4 bit neighbors;
#define mnmMAX4 		 21  // max of self and 4 bit neighbors;
#define mnmMINH2 		 22  // min of self and 2 bit horizontal neighbors;
#define mnmMAXH2		 23  // max of self and 2 bit horizontal neighbors;
#define mnmMINV2 		 24  // min of self and 2 bit vertical neighbors;
#define mnmMAXV2		 25  // max of self and 2 bit vertical neighbors;
#define mnmOPEN8 		 26  // same as mnmMIN followed by mnmMAX;
#define mnmCLOSE8		 27  // same as mnmMAX followed by mnmMIN;
#define mnmOPEN4 		 28  // same as mnmMIN4 followed by mnmMAX4;
#define mnmCLOSE4		 29  // same as mnmMAX4 followed by mnmMIN4;

// MaskOper() operations
#define moGET      0    // Get pixel in point {Data[0], Data[1]};
						// returns 0 or 1 (-1 if coordinates are wrong)
#define moINVPIX   1    // Inverse pixel in point {Data[0], Data[1]};
						// returns new pixel
#define moSET      2    // Set pixel (Data[2] & 1) into point
						// {Data[0], Data[1]}; returns old pixel
#define moSTAT     3    // Data[0]:=Sum(1), Data[1]:=Sum(X), Data[2]:=Sum(Y),
						// Data[3]:=Sum(X*X), Data[4]:=Sum(Y*Y),
						// Data[5]:=Sum(X*Y),
						// Data[6]:=Min(X), Data[7]:=Max(X)),
						// Data[8]:=Min(Y), Data[9]:=Max(Y)
#define moZERO     4    // fill mask with 0
#define moFULL     5    // fill mask with 1
#define moINV      6    // Inverse mask
#define moFIND     7    // find (Data[2] & 1) beginning from point
						// {Data[0], Data[1]}; _
						// returns: 1 if OK (and set the point
						// to new coordinates), 0 if no.
#define mo16OID    8    // Expand mask to convex 16-oid

		// Value Types:
#define vtBYTE	   1
#define vtSHORT	   2
#define vtWORD 	   3
#define vtLONG	   4
#define vtDWORD	   5
#define vtFLOAT    6
#define vtDOUBLE   7

// MaskLine() operations
#define mlPOLY  0x100 		// polyline
#define mlSPLINE0 0x200		// closed spline
#define mlSPLINE1 0x300		// unclosed spline with free ends
#define mlSPLINE2 0x400		// unclosed spline with straight first interval
#define mlSPLINE3 0x500		// unclosed spline with straight last interval
#define mlCIRCLE  0x800    	// closed circle
#define mlARC     0x900    	// round ark