/*
 * AIComponent.cpp
 *
 *	1May01	WH:	Removed from AItiff.cpp. Added Image operations list stuff.
 * 22May01	WH:	Added m_pAnnotation stuff;
 * 22Oct01	WH:	Added m_pZstack stuff;
 * 28Oct01	WH:	Support 1 bit images
 * 16Jan02	JMB	Added MIP maps.
 * 11Mar02  SN: Initialised m_annotationSize member, to fix review release bug.
 *				Also initialised m_zstackSize and colour just in case.
 * 11Mar02  SN: free'd m_pAnnotation in destructor, to fix review memory leak.
 * 22Jan03	JMB	Added CopyOpsList() and DeleteOpsList().
 * 19Sep03	JMB	Added Create1bppMIPMapMap(), GetBit(), SetBit()
 *				and GetAvailableMIPMapLOD().
 */
#include "AItiff.h"
#include "../libtiff/libtiff/tiffiop.h"
#include "xtiffio.h" // For custom tags support


#include <assert.h>
#include <tchar.h>
//------------------------------------------------------------------------------------------------------------
// AI_Tiffcomponent implementation
//------------------------------------------------------------------------------------------------------------
AI_TiffComponent::AI_TiffComponent()
{ 
	visible = TRUE;
	colour.r = colour.g = colour.b = 255;	// SN11Mar02
/*
	exampleString = NULL; 
	exampleLong = 0; 
	numExampleStructs = 0; pExampleStructArray = NULL;
*/
	pNext = NULL; 

	m_iops_list = NULL;	// Linked list of image ops
	m_lastOp.header.type = opNone;
	m_version = 1;
	m_pAnnotation = NULL;
	m_annotationSize = 0;					// SN11Mar02
	m_zstackSize = 0;						// SN11Mar02
	m_pZstack = NULL;
	m_zstackinfo.m_iszstack = 0;
	memset(&m_stageinfo, 0, sizeof(m_stageinfo));

	// Default is to only have the raw buffer. the cached image and display image all point to the raw pData
	// if there are no Image Operations in the list nor a non commited m_lastOp.

	// If there is an ImageOps list this will cause a m_Cache buffer to be allocated and the image operations
	// will be applied to this. The m_displayBuf will point to this

	// If there is a valid m_lastOps, then another buffer will be allocated. This allows the last operation to 
	// be applied to the cached processed image, without having to apply all the operations over again. (for speed)

	m_pCache = m_displayBuf = NULL;		
	m_bCacheBuf = FALSE; m_bDisplayBuf = FALSE;	// No extra buffers, just pData

	pMIPMaps = NULL;
	pZStackTiff = NULL;
}


AI_TiffComponent::~AI_TiffComponent()
{
	TidyUp();

	free( m_pAnnotation);				//SN11Mar02

	if (pMIPMaps) delete pMIPMaps;
	if (pZStackTiff) delete pZStackTiff;
}


//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
BOOL AI_TiffComponent::AddStretchContrast(int min, int max, BOOL bCommit)
{
	m_lastOp.header.type = opStretch;
	m_lastOp.header.version = m_version;
	m_lastOp.header.size = sizeof(ConStretch);
	m_lastOp.u.stretch.min = min;
	m_lastOp.u.stretch.max = max;

	if (bCommit)
	{
		ImageOp * Op = new ImageOp;
		Op->header.type = opStretch;
		Op->header.version = m_version;
		Op->header.size = sizeof(ConStretch);	// Size of appropriate union member

		Op->u.stretch.min = min;
		Op->u.stretch.max = max;
		Op->next = NULL;

		// Add to list
		if (!m_iops_list)
			m_iops_list = Op;
		else	// goto end of list and add
		{
			ImageOp * newop = m_iops_list;
			while (newop->next)
				newop = newop->next;

			newop->next = Op;
		}
		ResetLastOp();	// de-allocated the scratch display buff and points at the cache, for display
	}

	return TRUE;
}
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
BOOL AI_TiffComponent::AddFilter(LPCTSTR name, int hwidth, int *data, BOOL bCommit)
{
	m_lastOp.header.type = opFilter;
	m_lastOp.header.version = m_version;
	m_lastOp.header.size = sizeof(struct AITiffFilter);
	_tcsncpy(m_lastOp.u.filter.name, name, sizeof(m_lastOp.u.filter.name));
	m_lastOp.u.filter.hwidth = hwidth;
	memcpy(m_lastOp.u.filter.data, data, ((2*hwidth+1) * (2*hwidth+1) *sizeof(int)));

	if (bCommit)
	{
		ImageOp * Op = new ImageOp;
		*Op = m_lastOp;
		Op->next = NULL;	

		// Add to list
		if (!m_iops_list)
			m_iops_list = Op;
		else	// goto end of list and add
		{
			ImageOp * newop = m_iops_list;
			while (newop->next)
				newop = newop->next;

			newop->next = Op;
		}
	}

	return TRUE;
}
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
BOOL AI_TiffComponent::AddUnsharpMask(float radius, float depth, BOOL bCommit)
{
#ifndef NO_WOOLZ
	m_lastOp.header.type = opUnsharp;
	m_lastOp.header.version = m_version;
	m_lastOp.header.size = sizeof(AITiffUnsharpMask); // Size of appropriate union member
	m_lastOp.u.unsharp.radius = radius;
	m_lastOp.u.unsharp.depth = depth;
	m_lastOp.u.unsharp.reserved = 0;

	if (bCommit)
	{
		ImageOp *op = new ImageOp;
		*op = m_lastOp;
		op->next = NULL;

		// Add to list
		if (!m_iops_list)
			m_iops_list = op;
		else	// goto end of list and add
		{
			ImageOp *newop = m_iops_list;
			while (newop->next)
				newop = newop->next;

			newop->next = op;
		}
	}
#endif
	return TRUE;
}
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
void AI_TiffComponent::ProcessIntoCache()
{
	// Can't process image operations if full-size image data is not loaded
	// (perhaps the file was loaded with the noData parameter set to TRUE).
	if (!pData)
		return;

	// Have we any processing to do
	if (bpp == 8 && m_iops_list)
	{
		long size = WidthBytes() * height;
		if (!m_bCacheBuf)
		{
			// Allocate a cache buffer
			m_pCache = new uint8[size];
			assert (m_pCache);

			m_bCacheBuf = TRUE;
		}
		
		// Copy raw data to cache, image operation will then be performed on the cache data
		memcpy(m_pCache, pData, size);	

		// If there is no separate display buffer, make sure we use the cache for display
		if (!m_bDisplayBuf)
			m_displayBuf = m_pCache;

		// Apply operation list on it
		ImageOp * op = m_iops_list;
		while (op)
		{
			ProcessOp(op);
			op = op->next;
		}
	}
	else	// No ops therefore we can just use pdata 
	{
		TidyUp();
		// No extra buffers, just pData
		m_pCache = m_displayBuf = pData;
	}
}
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
void AI_TiffComponent::MergeLastOpToDisplay()
{
	if (!m_bDisplayBuf)
	{
		// allocate a scratch display buffer, so as we don't screw up our cached processed image
		// We use this to apply the current operation on the cached image, before commiting
		long size = height * WidthBytes();
		m_displayBuf = new uint8[size];
		assert (m_displayBuf);
		// Remember once we've finished (reset or commited) this operation, we can delete this and
		// use the cached image for display. Just point m_displayBuf at m_Cache, this is done by ResetLastOp()
		m_bDisplayBuf = TRUE;
	}

	// Also process last operation, not commited to list yet
	if (m_lastOp.header.type != opNone)
		ProcessOp(&m_lastOp);
}
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
void AI_TiffComponent::MergeLastOpToCache()
{
	if (m_lastOp.header.type != opNone)
	{
		long size = height * WidthBytes();
		if (!m_bCacheBuf)
		{
			// Allocate a cache buffer
			m_pCache = new uint8[size];
			assert (m_pCache);

			// Copy raw data to cache, image operation will then be performed on the cache data
			memcpy(m_pCache, pData, size);	

			m_bCacheBuf = TRUE;
		}
		
		// If there is no seperate display buffer, make sure we use the cache for display
		if (!m_bDisplayBuf)
			m_displayBuf = m_pCache;

		// Apply last op
		ProcessOp(&m_lastOp);

		ResetLastOp();	// de-allocated the scratch display buff and points at the cache, for display
	}
}
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
void AI_TiffComponent::ResetLastOp()
{
	// delete the scratch buffer used to show the lastop, not rquired at we can just use the cache buffer
	// that contains the cached image operations applied to the raw pData buffer (speed thing)
	if (m_bDisplayBuf)
	{
		delete m_displayBuf;
		m_bDisplayBuf = FALSE;
	}
	m_displayBuf = m_pCache;
	m_lastOp.header.type = opNone;	
}
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
void AI_TiffComponent::ProcessOp(ImageOp * op)
{
	assert (op);

	// Quick check that all buffer pointers are pointing to correct place
	if (!m_bCacheBuf)
		m_pCache = pData;
	if (!m_bDisplayBuf)
		m_displayBuf = m_pCache;

	// Seriously not allowed
	assert(m_displayBuf != pData);

	switch (op->header.type)
	{
		case opStretch:	Stretch(op);     break;
		case opFilter:	Filter(op);      break;
		case opUnsharp: UnsharpMask(op); break;
		default: break;
	}
}
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
void AI_TiffComponent::Stretch(ImageOp * op)
{
	if (bpp != 8)
		return;

	uint8* in = m_pCache;
	uint8* out = m_displayBuf;

	long size = height * WidthBytes();

	// stretch into scratch buffer
	for (long i = 0; i < size; i++)
	{
		if (*in < op->u.stretch.min)
			*out = 0;
		else if (*in > op->u.stretch.max)
			*out = 255;
		else
		{
			int range = op->u.stretch.max - op->u.stretch.min;
			if (range < 1)	// Just in case min and max are the same, don't want to divide by 0 
				range = 1;
			*out = 255 *(*in - op->u.stretch.min)/range;
		}
		out++;
		in++;
	}
}
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
void AI_TiffComponent::Filter(ImageOp * op)
{
	if (bpp != 8)
		return;

	uint8* in = m_pCache;

	// allocate a scatch buffer for processing
	long size = height * WidthBytes();
	uint8* out = new uint8[size];


	int khwidth = op->u.filter.hwidth;
	int *data = op->u.filter.data;

	// Apply filter to raw or cached image data
	int x,y,i,j;
	int sum,max_sum,min_sum,kdata;

	min_sum = max_sum = 0;
	for (j=-khwidth ; j<=khwidth ; j++) 
	{
		for (i=-khwidth ; i<=khwidth ; i++) 
		{
			kdata = data[(i+khwidth) + ((j+khwidth)*(khwidth+khwidth+1))];
			if (kdata < 0)
				min_sum += kdata;
			else
				max_sum += kdata;
		}
	}

	min_sum *= 255;
	max_sum *= 255;

	if ( (max_sum-min_sum) > 0 ) 
	{
		for (y=khwidth ; y<height-khwidth ; y++) 
		{
			for (x=khwidth ; x<width-khwidth ; x++) 
			{
				sum = 0;
				for (j=-khwidth ; j<=khwidth ; j++) 
				{
					for (i=-khwidth ; i<=khwidth ; i++) 
					{
						sum += (int)in[(x+i) + ((y+j)*width)] * (int)data[(i+khwidth) + ((j+khwidth)*(khwidth+khwidth+1))];
					}
				}
				sum =  (255*(sum-min_sum)) / (max_sum-min_sum) ;
				out[x + (y*width)] = (BYTE)sum;	// Ripped off from CV, so invert it
			}
		}
	}
	// Ok, now copy to display/cache
	memcpy(m_displayBuf, out, size);

	delete[] out;
}
//------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------
#include "woolz.h" // Aargh! Don't want something like Woolz in AITiff really, but need it for UnsharpMask :-(
void AI_TiffComponent::UnsharpMask(ImageOp *op)
{
#ifndef NO_WOOLZ
	if (bpp != 8)
		return;

	uint8* in = m_pCache;

	//uint8* out = fsGaussian(in, width, height, 10);
	uint8* out = fsUnsharpMask(in, width, height, op->u.unsharp.radius, op->u.unsharp.depth);

	// Ok, now copy to display/cache
	memcpy(m_displayBuf, out, width * height);

	fsFreeWoolz(out);
#endif
}
//------------------------------------------------------------------------------------------------------------
//	Remove last op
//------------------------------------------------------------------------------------------------------------
void AI_TiffComponent::UndoLastOp()
{
	// Get last op and remove
	ImageOp * op, *prevop = NULL;
	op = m_iops_list;
	while (op)
	{
		if (!op->next)	// end of list
			break;
		prevop = op;
		op = op->next;
	}

	if (op)
	{
		if (op == m_iops_list)
			m_iops_list = NULL;
		else
			prevop->next = NULL;
		delete op;
	}
}
//------------------------------------------------------------------------------------------------------------
// For some reason (ask jago) can't delete anything new'ed from inside this module in any other module
//------------------------------------------------------------------------------------------------------------
void AI_TiffComponent::DeleteOp(ImageOp *op)
{
	delete op;
}
//------------------------------------------------------------------------------------------------------------
// For some reason (ask jago) can't delete anything new'ed from inside this module in any other module
//------------------------------------------------------------------------------------------------------------
ImageOp * AI_TiffComponent::CreateOp()
{
	return new ImageOp;
}


ImageOp *AI_TiffComponent::CopyOpsList()
{
// Create a copy of the image operations list, making sure the order of elements is preserved.
	ImageOp *pCopyList = NULL;

	if (m_iops_list)
	{
		ImageOp *pPrevCopyOp = NULL;
		ImageOp *pOp = m_iops_list;

		while (pOp)
		{
			ImageOp *pCopyOp = new ImageOp;
			if (pCopyOp)
			{
				*pCopyOp = *pOp;

				if (pPrevCopyOp)
					pPrevCopyOp->next = pCopyOp;
				else
					pCopyList = pCopyOp;

				pPrevCopyOp = pCopyOp;
			}
			else
			{
				// Error, clean up
				while (pCopyList)
				{
					ImageOp *pNextOp = pCopyList->next;
					delete pCopyList;
					pCopyList = pNextOp;
				}
				break;
			}

			pOp = pOp->next;
		}
	}

	return pCopyList;
}


void AI_TiffComponent::DeleteOpsList()
{
	while (m_iops_list)
	{
		ImageOp *pNextOp = m_iops_list->next;
		delete m_iops_list;
		m_iops_list = pNextOp;
	}
}


void AI_TiffComponent::TidyUp()
{
	if (m_bCacheBuf)
		delete[] m_pCache;
	if (m_bDisplayBuf)
		delete[] m_displayBuf;

	m_bCacheBuf = FALSE;
	m_bDisplayBuf = FALSE;

	DeleteOpsList();
}


//----------------------------------------------------------------------------
// MIP map functions (JMB)
//----------------------------------------------------------------------------

static inline int GetBit(uint8 *pRow, int x)
{
	uint8 *pByte = pRow + (x >> 3);
	return ((*pByte >> (7 - (x & 0x7))) & 0x01);
}

static inline void SetBit(uint8 *pRow, int x, int bit)
{
	uint8 *pByte = pRow + (x >> 3);
	if (bit)
		*pByte |= 0x80 >> (x & 0x7);
	else
		*pByte &= ~(0x80 >> (x & 0x7));
}

static void Create1bppMIPMapMap(uint8 *pSrc, int srcWidth, int srcHeight, uint8 *pDst)
{
// The dimensions of the map created by this function are
// (srcWidth/2, srcHeight/2), rounded down (integer arithmetic).
	uint8 *pSrcRow     = pSrc;
	int srcRowBytes    = TiffComponent::WidthBytes(srcWidth, 1);
	uint8 *pNextSrcRow = pSrc + srcRowBytes;
	int twoSrcRowBytes = srcRowBytes << 1;
	uint8 *pDstRow  = pDst;
	int dstRowBytes = TiffComponent::WidthBytes(srcWidth / 2, 1);
	BOOL oddWidth  = srcWidth % 2;
	BOOL oddHeight = srcHeight % 2;

	for (int srcY = 0; srcY < (srcHeight - 1); srcY += 2)
	{
		for (int srcX = 0, dstX = 0; srcX < (srcWidth - 1); srcX += 2, dstX++)
		{
			if (oddHeight && srcY == srcHeight - 3)
			{
				if (oddWidth  && srcX == srcWidth - 3)
				{   // Special case:
					// Source image width and height are an odd number of pixels.
					// The bottom-right 3x3 square of pixels in the source image
					// needs to be averaged to produce the bottom right pixel in
					// the destination image, if all of those pixels are to be
					// accounted for.
					int numBits =   GetBit(pSrcRow, srcX) + GetBit(pSrcRow, srcX + 1) + GetBit(pSrcRow, srcX + 2)
					              + GetBit(pNextSrcRow, srcX) + GetBit(pNextSrcRow, srcX + 1) + GetBit(pNextSrcRow, srcX + 2)
					              + GetBit(pNextSrcRow + srcRowBytes, srcX) + GetBit(pNextSrcRow + srcRowBytes, srcX + 1) + GetBit(pNextSrcRow + srcRowBytes, srcX + 2);

					SetBit(pDstRow, dstX, (numBits > 4));
				}
				else
				{
					// Special case:
					// Source image height is an odd number of pixels and there are
					// 3 rows remaining at the bottom of the source image. Average
					// a 2x3 block of pixels from the source image, so that the
					// last row column of pixels in the source image is not ignored.
					int numBits =   GetBit(pSrcRow, srcX) + GetBit(pSrcRow, srcX + 1)
					              + GetBit(pNextSrcRow, srcX) + GetBit(pNextSrcRow, srcX + 1)
					              + GetBit(pNextSrcRow + srcRowBytes, srcX) + GetBit(pNextSrcRow + srcRowBytes, srcX + 1);

					SetBit(pDstRow, dstX, (numBits > 2));
				}
			}
			else if (oddWidth && srcX == srcWidth - 3)
			{
				// Special case:
				// Source image width is an odd number of pixels and there are
				// 3 pixels remaining at the end of the row. Average a 3x2
				// block of pixels from the source image, so that the last
				// column of pixels in the source image is not ignored.
				int numBits =   GetBit(pSrcRow, srcX) + GetBit(pSrcRow, srcX + 1) + GetBit(pSrcRow, srcX + 2)
				              + GetBit(pNextSrcRow, srcX) + GetBit(pNextSrcRow, srcX + 1) + GetBit(pNextSrcRow, srcX + 2);

				SetBit(pDstRow, dstX, (numBits > 2));
			}
			else
			{
				// Average a 2x2 square of pixels in the source image into
				// a single pixel in the destination image.
				int numBits =   GetBit(pSrcRow, srcX) + GetBit(pSrcRow, srcX + 1)
				              + GetBit(pNextSrcRow, srcX) + GetBit(pNextSrcRow, srcX + 1);

				SetBit(pDstRow, dstX, (numBits > 1));
			}
		}

		pSrcRow += twoSrcRowBytes;
		pNextSrcRow = pSrcRow + srcRowBytes;
		pDstRow += dstRowBytes;
	}
}

static void Create8bppMIPMapMap(uint8 *pSrc, int srcWidth, int srcHeight, uint8 *pDst)
{
// The dimensions of the map created by this function are
// (srcWidth/2, srcHeight/2), rounded down (integer arithmetic).
	uint8 *pSrcRow = pSrc;
	uint8 *pD      = pDst;
	int srcRowBytes    = srcWidth;
	int twoSrcRowBytes = srcRowBytes << 1;
	BOOL oddWidth  = srcWidth % 2;
	BOOL oddHeight = srcHeight % 2;

	for (int srcY = 0; srcY < (srcHeight - 1); srcY += 2)
	{
		uint8 *pS = pSrcRow;

		for (int srcX = 0; srcX < (srcWidth - 1); srcX += 2)
		{
			if (oddHeight && srcY == srcHeight - 3)
			{
				if (oddWidth && srcX == srcWidth - 3)
				{   // Special case:
					// Source image width and height are an odd number of pixels.
					// The bottom-right 3x3 square of pixels in the source image
					// needs to be averaged to produce the bottom right pixel in
					// the destination image, if all of those pixels are to be
					// accounted for.
					*pD = (  *pS                    + *(pS + 1)                  + *(pS + 2)
					       + *(pS + srcRowBytes)    + *(pS + srcRowBytes + 1)    + *(pS + srcRowBytes + 2)
					       + *(pS + twoSrcRowBytes) + *(pS + twoSrcRowBytes + 1) + *(pS + twoSrcRowBytes + 2)) / 9;
				}
				else
				{
					// Special case:
					// Source image height is an odd number of pixels and there are
					// 3 rows remaining at the bottom of the source image. Average
					// a 2x3 block of pixels from the source image, so that the
					// last row column of pixels in the source image is not ignored.
					*pD = (  *pS                    + *(pS + 1)              
					       + *(pS + srcRowBytes)    + *(pS + srcRowBytes + 1)
					       + *(pS + twoSrcRowBytes) + *(pS + twoSrcRowBytes + 1)) / 6;
				}
			}
			else if (oddWidth && srcX == srcWidth - 3)
			{
				// Special case:
				// Source image width is an odd number of pixels and there are
				// 3 pixels remaining at the end of the row. Average a 3x2
				// block of pixels from the source image, so that the last
				// column of pixels in the source image is not ignored.
				*pD = (  *pS                 + *(pS + 1)               + *(pS + 2)
				       + *(pS + srcRowBytes) + *(pS + srcRowBytes + 1) + *(pS + srcRowBytes + 2)) / 6;
			}
			else
			{
				// Average a 2x2 square of pixels in the source image into
				// a single pixel in the destination image.
				*pD = (  *pS                 + *(pS + 1)
				       + *(pS + srcRowBytes) + *(pS + srcRowBytes + 1)) >> 2;
			}

			pS += 2;
			pD++;
		}

		pSrcRow += twoSrcRowBytes;
	}
}


BOOL
AI_TiffComponent::CreateMIPMaps()
{
// Create MIP-map from pData.
	BOOL status = FALSE;

	// Currently only 1bpp and 8bpp supported.
	if (bpp != 1 && bpp != 8)
		return FALSE;

	if (!pData)
		return FALSE;


	if (pMIPMaps)
		delete pMIPMaps;

	pMIPMaps = new AI_Tiff;

	if (pMIPMaps)
	{
		// The first MIP map is created from the full-size component data

		// Calculate MIP-map dimensions. Note that 1 is added to the width and
		// height before doing integer arithmetic on them in case they are an odd
		// number, so that the result will be rounded up
		// Width of MIP-map is 1/2 of image width.
		int mipWidth  = (width  + 1) / 2;
		// Height of MIP-map is 1/2 of image height.
		int mipHeight = (height + 1) / 2;

		// Allocate a temporary buffer that is large enough for this
		// and subsequent (smaller) MIP maps.
		uint8 *pMIPData = (uint8*)malloc(mipHeight * WidthBytes(mipWidth));

		if (pMIPData)
		{
			// Calculate pixel value in MIP map level by averaging four
			// corresponding pixels in next larger level.
			uint8 *pSrc   = pData;
			int srcWidth  = width;
			int	srcHeight = height;

			int dstLOD  = 1;
			uint8 *pDst = pMIPData;


			while (srcWidth > 1 && srcHeight > 1)
			{
				if (bpp == 1)
					Create1bppMIPMapMap(pSrc, srcWidth, srcHeight, pDst);
				else if (bpp == 8)
					Create8bppMIPMapMap(pSrc, srcWidth, srcHeight, pDst);
			
				int dstWidth  = srcWidth / 2;
				int dstHeight = srcHeight / 2;

				int index = -1;
				if (bpp == 1)
					index = pMIPMaps->Add1bppComponent(pDst, dstWidth, dstHeight);
				else if (bpp == 8)
					index = pMIPMaps->Add8bppComponent(pDst, dstWidth, dstHeight);

				if (index < 0) break;

				pSrc      = pDst;
				srcWidth  = dstWidth;
				srcHeight = dstHeight;

				status = TRUE;
			}

			// Release temporary buffer.
			free(pMIPData);
		}
	}

	return status;
}


int
AI_TiffComponent::GetAvailableMIPMapLOD(int idealLOD)
{
// Returns the nearest available MIP map LOD to the ideal one
	int availableLOD = 0; // Default to full-size image

	if (idealLOD > 0 && pMIPMaps)
	{
		for (int i = 0; i < idealLOD; i++)
		{
			if (pMIPMaps->GetComponent(i))
				availableLOD = i + 1;
			else
				break;
		}
	}

	return availableLOD;
}


AI_TiffComponent*
AI_TiffComponent::GetMIPMap(int lod)
{
// Returns the MIP map component with the given LOD, if available.
	AI_TiffComponent *pComponent = NULL;

	if (lod > 0 && pMIPMaps)
		pComponent = pMIPMaps->GetComponent(lod - 1);
	else if (lod == 0)
		pComponent = this;

	return pComponent;
}


//----------------------------------------------------------------------------


int
AI_TiffComponent::GetMemoryUsage()
{
// Returns an approximation of the current memory usage of this
// AI_TiffComponent, in kilobytes.
	// Start off with 1K to cover memory usage other than that used by the
	// image data itself.
	int kilobytes = 1;

	// Image data
	if (pData) kilobytes += (dataBytes >> 10);

	// Cache buffer
	if (m_bCacheBuf && m_pCache != pData)
		kilobytes += (WidthBytes() * height) >> 10;

	// Another WH buffer :-)
	if (m_bDisplayBuf && m_displayBuf != m_pCache && m_displayBuf != pData)
		kilobytes += (WidthBytes() * height) >> 10;

	// Annotation buffer
	if (m_pAnnotation) kilobytes += (m_annotationSize >> 10);

	// Z Stack buffer
	if (m_pZstack) kilobytes += (m_zstackSize >> 10);

	// MIP maps, if any.
	if (pMIPMaps) kilobytes += pMIPMaps->GetMemoryUsage();


	return kilobytes;
}



//----------------------------------------------------------------------------


#if 0

/*
// These function are for packing all the MIP maps into one buffer, in a tiled arrangement.
void
AI_TiffComponent::GetMIPOffsets(int lod, int *pX, int *pY)
{
// LOD: Level Of Detail. Level 0 is the original image, the dimensions of
// the level 1 are half those of the level 0 image, those of level 2 are
// half those of level 1, and so on.
// Don't allow a LOD greater than 30, as 2 to the power of 31 is the largest
// number a signed 32-bit integer (i.e. 'divisor') can hold.
	if (lod < 2 || lod > 30)
		*pX = *pY = 0;
	else if (lod == 2)
	{
		*pX = (mipWidth + 1) >> 1; // Divide by 2
		*pY = 0;
	}
	else
	{
		*pX = (mipWidth + 1) >> 1;

		int divisor = 16
		while (lod-- > 3)
		{
			*pX += (mipWidth + 1) / divisor;

			divisor << 1; // Multiply by 2
		}

		*pY = (mipHeight + 1) >> 1;
	}
}

void
AI_TiffComponent::CreateMIPMapMap(uint8 *pSrc, int srcXOff, int srcYOff, int srcMapWidth, int srcMapHeight,
                                  uint8 *pDst, int dstXOff, int dstYOff, )
{
	uint8 *pSrcRow = pSrc + (srcYOff * srcRowBytes) + srcXOff;

	uint8 *pDstRow = pDst + (dstYOff * dstRowBytes) + dstXOff;

	for (srcY = 0; srcY < srcHeight; srcY += 2)
	{
		for (srcX = 0; srcX < srcWidth; srcX += 2)
		{
			*pDstRow = (  *pSrcRow                 + *(pSrcRow + 1)
			            + *(pSrcRow + srcRowBytes) + *(pSrcRow + srcRowBytes + 1)) >> 2;

			pSrcRow++;
			pDstRow++;
		}

		pSrcRow += srcRowBytes;
		pDstRow += dstRowBytes -;
	}
}

BOOL
AI_TiffComponent::CreateMIPMaps()
{
// Create MIP-map from pData.
	BOOL status = FALSE;

	// Currently only 8bpp supported.
	if (bpp != 8) return FALSE;

	// Calculate MIP-map dimensions. Note that 1 is added to the width and
	// height before doing integer arithmetic on them in case they are an odd
	// number, so that the result will be rounded up
	// Width of MIP-map is 3/4 of image width.
	mipWidth = ((width + 1) * 3) / 4;
	// Height of MIP-map is 1/2 of image height.
	mipHeight = (height + 1) / 2;

	pMIPData = (uint8*)malloc(mipHeight * WidthBytes(mipWidth));

	if (pMIPData)
	{
		// Calculate pixel value in MIP map level by averaging four
		// corresponding pixels in next larger level.
		int srcLOD   = 0; // Level Of Detail: level 0 is the original image.
		uint8 *pSrc  = pData;
		int srcXOff  = 0;
		int srcYOff  = 0;
		int srcWidth = width;
		int srcHight = height;

		int dstLOD  = 1;
		uint8 *pDst = pMIPData;
		int dstXOff = 0;
		int dstYOff = 0;

		while (CreateMIPMapMap(pSrc, srcXOff, srcYOff, srcWidth, srcHeight,
		                       pDst, dstXOff, dstYOff))
		{
			srcLOD    = dstLOD;
			pSrc      = pDst;
			srcXOff   = dstXOff;
			srcYOff   = dstYOff;
			srcWidth  = mipWidth;
			srcHeight = mipHeight;

			dstLOD++;
			GetMIPOffsets(dstLOD, &dstXOff, &dstYOff);
		}

	}

	return status;
}
*/
#endif

//------------------------------------------------------------------------------------------------------------


