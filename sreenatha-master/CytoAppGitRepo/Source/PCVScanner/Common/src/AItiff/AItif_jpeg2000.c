
/*
 * JPEG2000 codec for libtiff, using JasPer.
 * Note this uses the JPEG2000 Code Stream format ('JPC'), not the 'JP2' file format.
 * JMB Nov 2003
 */


#include "../libtiff/libtiff/tiffiop.h"

#define JAS_WIN_MSVC_BUILD // Ensure correct header information is included for a Windows build
#include <jasper\jasper.h>

#include "AITiff.h" // Just for tag #defines


/*
 * State block for each open TIFF file using JPC compression/decompression.
 */
typedef	struct
{
	double rate; // Compression setting
	TIFFVGetMethod	vgetparent; // super-class method for tags
	TIFFVSetMethod	vsetparent; // super-class method for tags
} JPCState;



/*
 * Decode a strip or tile.
 */
static int
JPCDecode(TIFF* tif, tidata_t op, tsize_t occ, tsample_t s)
{
	int status = 0; // Boolean
	static const char module[] = "JPCDecode";
	jas_stream_t *pStream;


	pStream = jas_stream_memopen(0, 0);
	if (pStream)
	{
		jas_image_t *pImage;

		// Copy the raw (encoded) data to the JasPer memory stream
		jas_stream_write(pStream, tif->tif_rawdata, tif->tif_rawcc);


		jas_stream_seek(pStream, 0, SEEK_SET);

		pImage = jpc_decode(pStream, "");
		if (pImage)
		{
			int iCmp;

	// The number of JPC components depends on the data. Each sample in a
	// component is represented by an integer of at least 16 bits. For
	// greyscale there is usually one component, for RGB there are usually 3.
	// The number of components usually correspnds with the TIFF component's
	// 'SamplesPerPixel' value (remember that 'component' has a different
	// meaning in TIFF and JasPer).
			for (iCmp = 0; iCmp < jas_image_numcmpts(pImage); iCmp++)
			{
				int c, index = 0;
				jas_stream_seek(pImage->cmpts_[iCmp]->stream_, 0, SEEK_SET);
				while((c = jas_stream_getc(pImage->cmpts_[iCmp]->stream_)) != EOF)
				{
				// TODO: this will currently only work for 8bpp, 1 component
				// TODO: ensure output buffer is not overrun.
					op[index++] = c;
				}
			}

			jas_image_destroy(pImage);

			status = 1;
		}

		jas_stream_close(pStream);
	}

	return status;
}



/*
 * Encode a chunk of pixels.
 * Remember, this is just a strip or tile, probably not a whole image.
 */
static int
JPCEncode(TIFF* tif, tidata_t bp, tsize_t cc, tsample_t s)
{
	int status = 0; // Boolean
	static const char module[] = "JPCEncode";
	JPCState *pState = (JPCState*)tif->tif_data;
	jas_image_t *pImage;


	// Create a JasPer 'image'
	pImage = jas_image_create0();
	if (pImage)
	{
		uint32 width, height;
		tsize_t bytesperline;
		int iCmp;
		jas_stream_t *pStream;

		// JasPer encourates the use of its jas_matrix_t for intermediate
		// storage of component pixel data. Using this model, for each JPC
		// component, the pixels would be copied to a matrix, then the
		// component would be updated using jas_image_writecmpt(). However,
		// bypassing this intermediate storage and writing directly to the
		// jas_image_t's component should be faster and less complicated,
		// given that the component's internal storage format is similar to
		// that used by libtiff. The only problem is that this format is
		// undocumented and if it changes in future versions of Jasper the
		// following code might stop working (works with JasPer 1.700.5).


		// Note that in TIFF parlence, 'length' is height.
		if (isTiled(tif))
		{
			width = tif->tif_dir.td_tilewidth;
			height = tif->tif_dir.td_tilelength;

			bytesperline = TIFFTileRowSize(tif);
		}
		else
		{
			width = tif->tif_dir.td_imagewidth;
			height = tif->tif_dir.td_imagelength - tif->tif_row;
			if (height > tif->tif_dir.td_rowsperstrip)
				height = tif->tif_dir.td_rowsperstrip;

			bytesperline = TIFFScanlineSize(tif);
		}


	// The number of JPC components depends on the data. Each sample in a
	// component is represented by an integer of at least 16 bits. For
	// greyscale there is usually one component, for RGB there are usually 3.
	// The number of components usually correspnds with the TIFF component's
	// 'SamplesPerPixel' value (remember that 'component' has a different
	// meaning in TIFF and JPEG2000).
	// The JPEG2000 standard document supplied with JasPer implies that,
	// when encoding, if there are at least 3 components and that the first 3
	// are the same size, they will be assumed to be R, G and B components and
	// will have a colour transformation applied to them that is intended to
	// improve the efficiency of subsequent encoding steps. 
		for (iCmp = 0; iCmp < tif->tif_dir.td_samplesperpixel; iCmp++)
		{
			int i;
			jas_image_cmptparm_t cmptparm;
			cmptparm.tlx = 0;
			cmptparm.tly = 0;
			cmptparm.hstep = 1;
			cmptparm.vstep = 1;
			cmptparm.width  = width;
			cmptparm.height = height;
			cmptparm.prec = tif->tif_dir.td_bitspersample;
			cmptparm.sgnd = false;

			jas_image_addcmpt(pImage, iCmp, &cmptparm);


			// Copy TIFF data for this component into component's data stream.
			jas_stream_seek(pImage->cmpts_[iCmp]->stream_, 0, SEEK_SET);
			for (i = 0; i < cc; i++)
			{
			// TODO: this will not work for 1bpp. However I have currently modified AITIFF to stop it using anything other than fax compression for bi-level images.
//JasPer allows a minimum of one byte per pixel, even for bilevel data, whereas libtiff has 8 bi-level values in a byte.
				jas_stream_putc(pImage->cmpts_[iCmp]->stream_, bp[i]);
			}
		}


		// Create a temporary in-memory stream for JasPer to encode data into.
		pStream = jas_stream_memopen(0, 0);
		if (pStream)
		{
			int copied = 0, totalCopied = 0;

			double rate = 0.1;
			double minimumRate = 0.0;
			char optionsString[128];
			optionsString[0] = '\0';

			// Get rate from state block, if available.
			if (pState)
				rate = pState->rate;

			// Ensure rate is valid: must be positive (not sure if zero is ok).
			// A rate of greater than 1 is the same as a rate of 1 - lossless.
			//[Not needed thanks to minimumRate calculation below] if (rate <= 0.0) rate = 0.001; // Arbitary minimum

			// A combination of a small image and a high compresstion rate will
			// cause the compression algorithm to break down, resulting in a
			// featureless grey image ("warning: empty layer generated" is
			// written to stderr by the rateallocate() function in jpc_enc.c).
			// In an attempt to avoid this happening, increase the rate for
			// small images to avoid lossy compression being used if the
			// number of pixels times the rate (proportional to the compressed
			// image size) is less than 512. This adjustment has been arived at
			// through experiment and guesswork so may not always work, and
			// there may be a better way of handling this.
			minimumRate = 512.0 / ((width*height)+1); // The +1 is to avoid divide by zero.
			if (rate < minimumRate)
				rate = minimumRate;
			

			// Compose the options string for the JPC encoder
			// My current understanding of some of the encoding options
			// (I may be wrong!):
			// numrlvls: This is a parameter for the wavelet transform - changing
			//           it from the default produces undesirable results.
			//           Not sure what if any relationship this has with the
			//           'progressive recovery by resolution' feature.
			// nomct: I think this disables the colour transformations that happen
			//        if there are at least 3 components - they are only really
			//        appropriate if the first three components are R, G and B, but
			//        won't do any harm if they are not.
			// lazy: This may offer an increase in coding speed at the expense of
			//       the amount of compression achieved.
			// rate: This is actually the compression factor. A value of 1 or more
			//       gives lossless compression (assuming integer mode is being used).
			//       A fractional rate specifies the relative size after compression.
			// prg: This gives the progression order, i.e. the order in which packets
			//      are sorted.
			//      There are 5 alternatives, the default is lrcp (i.e. the packets
			//      are sorted first by the (quality) layer they belong to) which allows
			//      rate scaling (i.e. packets for the lowest quality layer are read first,
			//      then for the next layer, etc.) We may want to use rlcp (i.e. the packets
			//      are sorted first by the resolution they belong to), which allows
			//      resolution scaling, (i.e. packets for the lowest resolution are read
			//      first, then for the next resolutions (size?)), as an alternative to
			//      MIP maps
			sprintf(optionsString, "rate=%f", rate);

			// Write the JPC data to the in-memory stream.
			jpc_encode(pImage, pStream, optionsString);

			// Finally, the encoded data is copied to the libtiff's 'raw data
			// buffer', from where libtiff will write it to file. If the size
			// of the data is bigger than the size of the raw buffer, then the
			// raw buffer must be flushed every time it fills up.

			assert((tif->tif_rawcp - tif->tif_rawdata) == tif->tif_rawcc);

			do
			{
				int remainingRawBuffer = tif->tif_rawdatasize - tif->tif_rawcc;

				jas_stream_seek(pStream, totalCopied, SEEK_SET);

				// Copy encoded data from the JasPer memory stream to libtiff's raw buffer.
				copied = jas_stream_read(pStream, tif->tif_rawcp, remainingRawBuffer);

				tif->tif_rawcp += copied;
				tif->tif_rawcc += copied;

				if (tif->tif_rawcc >= tif->tif_rawdatasize)
				// Raw data buffer is full - flush it in case it needs to be filled again.
				// Note this will reset tif_rawcp and tif_rawcc.
					TIFFFlushData1(tif);

				totalCopied += copied;
			}
			while (copied > 0);


			jas_stream_close(pStream);

			status = 1;
		}

		jas_image_destroy(pImage);
	}

	return status;
}



/*
 * Strips and tiles - using tiles (or strips - strips are effectively tiles
 * that have the same width as the image) with lossy JPEG2000 produces results
 * in visible edge artifacts and the smaller the tiles, the worse the image
 * quality within them. So tiling is to be avoided if at all possible.
 * Actually, JPEG2000 has provision for tiling within the code stream, but
 * this produces similar effects to tiling at the TIFF level (with one
 * codestream per tile). The main reason for having tiles is so that the codec
 * only has to work on a tile-sized amount of data at a time, so if enough
 * memory is available to process images whole, tiles should be avoided.
 */

static uint32
JPCDefaultStripSize(TIFF* tif, uint32 s)
{
	if ((int32) s < 1)
	{
		// If RowsPerStrip is unspecified, return the recommended number
		// of rows in a strip - all of them (so there is only one strip)!
		s = tif->tif_dir.td_imagelength;
	}
	return s;
}

static void
JPCDefaultTileSize(TIFF* tif, uint32* tw, uint32* th)
{
// If a dimension is not specified, return the recommended dimension for a tile
// - ideally only want one tile so return image dimensions.
	if (*(int32*)tw < 1)
		*tw = tif->tif_dir.td_imagewidth;
	if (*(int32*)th < 1)
		*th = tif->tif_dir.td_imagelength;

	// Round up to a multiple of 16 per the TIFF spec
	if (*tw & 0xf)
		*tw = TIFFroundup(*tw, 16);
	if (*th & 0xf)
		*th = TIFFroundup(*th, 16);
}




static int
JPCVSetField(TIFF* tif, ttag_t tag, va_list ap)
{
	JPCState *pState = (JPCState*)tif->tif_data;

	switch (tag)
	{
		case TIFFTAG_JPCRATE:
			pState->rate = va_arg(ap, double);
			return 1;
		default:
			return (*pState->vsetparent)(tif, tag, ap);
	}
}

static int
JPCVGetField(TIFF* tif, ttag_t tag, va_list ap)
{
	JPCState *pState = (JPCState*)tif->tif_data;

	switch (tag)
	{
		case TIFFTAG_JPCRATE:
			*va_arg(ap, double*) = pState->rate;
			return 1;
		default:
			return (*pState->vgetparent)(tif, tag, ap);
	}
}

static const TIFFFieldInfo jpcFieldInfo[] = {
    { TIFFTAG_JPCRATE, 0, 0, TIFF_ANY, FIELD_PSEUDO, TRUE, FALSE, "" },
};



int
TIFFInitJPEG2000(TIFF *tif, int scheme)
{
	JPCState *pState = NULL;

	assert(scheme == COMPRESSION_JPEG2000 || scheme == COMPRESSION_JP2000);

	jas_init(); // But what if this has been called before??


	/*
	 * Allocate and initialise state block.
	 */
	tif->tif_data = (tidata_t)_TIFFmalloc(sizeof(JPCState));
	if (!tif->tif_data)
		return 0;
	pState = (JPCState*)tif->tif_data;
	pState->rate = 0.1; // Sensible default.


	/*
	 * Merge codec-specific pseudo tags and handlers
	 */
	_TIFFMergeFieldInfo(tif, jpcFieldInfo, sizeof (jpcFieldInfo) / sizeof (jpcFieldInfo[0]));
	pState->vgetparent = tif->tif_vgetfield;
	tif->tif_vgetfield = JPCVGetField;	/* hook for codec tags */
	pState->vsetparent = tif->tif_vsetfield;
	tif->tif_vsetfield = JPCVSetField;	/* hook for codec tags */


	/*
	 * Install codec methods.
	 */
	tif->tif_decoderow = JPCDecode;
	tif->tif_decodestrip = JPCDecode;
	tif->tif_decodetile = JPCDecode;
	tif->tif_encoderow = JPCEncode;
	tif->tif_encodestrip = JPCEncode;
	tif->tif_encodetile = JPCEncode;

	tif->tif_defstripsize = JPCDefaultStripSize;
	tif->tif_deftilesize = JPCDefaultTileSize;

	return 1;
}

