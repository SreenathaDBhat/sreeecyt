/*
 * LibTIFF library Win32-specific Routines adapted for AI_Tiff.
 * This file is based on tif_win32.c contained in the libtiff sources.
 * The original tif_win32.c may still be built into libtif.lib, but functions
 * with the same name will always be linked from this file, as it is part
 * of the DLL, whereas functions from statically-linked libraries are only
 * linked into a DLL or EXE to resolve references than cannot be found in the
 * DLL or EXE themselves. This means that libtif.lib can be left intact for use
 * by other code, without the modifications made here.
 * JMB Aug 2003
 */

#include <windows.h>
#include "../libtiff/libtiff/tiffiop.h"



/*****************************************************************************
 * Private heap functions
 * AR 01Jul2003
 */

#define USE_PRIVATE_HEAP

static HANDLE HeapHandle = NULL;

/*
 *		C R E A T E P R I V A T E H E A P
 *			-- create a new heap for use here
 */
static void CreatePrivateHeap()
{
	DWORD InitialHeapSize;
	DWORD MaximumHeapSize = 0;  //heap may grow, restricted only by available memory
	SYSTEM_INFO SysInfo;
	DWORD nPages;

	GetSystemInfo (&SysInfo);

			//allocate 50 Mbytes
	nPages = (50000000 / SysInfo.dwPageSize) + 0.5;
	InitialHeapSize = nPages * SysInfo.dwPageSize;
	HeapHandle = HeapCreate (HEAP_GENERATE_EXCEPTIONS, InitialHeapSize, MaximumHeapSize); 

}

/*
 *		M Y _ M A L L O C
 *			-- allocate a block o f memory on the heap
 */
static void *My_malloc (DWORD nBytes)
{
	void *pMem = NULL;

	if (nBytes <= 0)
		return NULL;

	if (HeapHandle == NULL)
		CreatePrivateHeap();

	if (HeapHandle)
		pMem = HeapAlloc(HeapHandle, HEAP_GENERATE_EXCEPTIONS | HEAP_ZERO_MEMORY, (SIZE_T)nBytes);	
	else
		pMem = malloc(nBytes);

	return pMem;
}

/*
 *		M Y _ R E A L L O C
 *			-- change the size of a previously allocated block of memory on the heap
 */
static void *My_realloc(void *pCurrentMem, DWORD nBytes)
{
	void *pMem = NULL;

	// Standard realloc just does a malloc if the pointer is NULL,
	// but HeapReAlloc() doesn't do this so must do it explicitly.
	if (!pCurrentMem)
		return My_malloc(nBytes);

	if (nBytes <= 0)
		return NULL;

	if (HeapHandle == NULL)
		CreatePrivateHeap();

	if (HeapHandle)
		pMem = HeapReAlloc(HeapHandle, HEAP_GENERATE_EXCEPTIONS | HEAP_ZERO_MEMORY, pCurrentMem, (SIZE_T)nBytes);	
	else
		pMem = realloc(pCurrentMem, nBytes);

	return pMem;
}

/*
 *		M Y _ F R E E
 *			-- free a previously allocated block of memeory from the heap
 */
static void My_free (void * pMem)
{
	if (pMem == NULL)
		return;

	if (HeapHandle)
		HeapFree (HeapHandle,0,pMem);
	else
		free (pMem);
}


/****************************************************************************/


tdata_t
_TIFFmalloc(tsize_t s)
{
#ifdef USE_PRIVATE_HEAP
	return ((tdata_t)My_malloc(s));
#else
	return ((tdata_t)GlobalAlloc(GMEM_FIXED, s));
#endif
}

void
_TIFFfree(tdata_t p)
{
#ifdef USE_PRIVATE_HEAP
	My_free((void *)p);
#else
	GlobalFree(p);
#endif

	return;
}

tdata_t
_TIFFrealloc(tdata_t p, tsize_t s)
{
#ifdef USE_PRIVATE_HEAP
	return (My_realloc((void *) p, (DWORD)s));
#else
	void *pvTmp;
	tsize_t old = GlobalSize(p);
	if (old >= s)
	{
		if ((pvTmp = GlobalAlloc(GMEM_FIXED, s)) != NULL)
		{
			CopyMemory(pvTmp, p, s);
			GlobalFree(p);
		}
	}
	else
	{
		if ((pvTmp = GlobalAlloc(GMEM_FIXED, s)) != NULL)
		{
			CopyMemory(pvTmp, p, old);
			GlobalFree(p);
		}
	}
	return ((tdata_t)pvTmp);
#endif
}

void
_TIFFmemset(void* p, int v, tsize_t c)
{
	FillMemory(p, c, (BYTE)v);
}

void
_TIFFmemcpy(void* d, const tdata_t s, tsize_t c)
{
	CopyMemory(d, s, c);
}

int
_TIFFmemcmp(const tdata_t p1, const tdata_t p2, tsize_t c)
{
	register const BYTE *pb1 = (const BYTE *) p1;
	register const BYTE *pb2 = (const BYTE *) p2;
	register DWORD dwTmp = c;
	register int iTmp;
	for (iTmp = 0; dwTmp-- && !iTmp; iTmp = (int)*pb1++ - (int)*pb2++)
		;
	return (iTmp);
}


/****************************************************************************/


static tsize_t
_tiffReadProc(thandle_t fd, tdata_t buf, tsize_t size)
{
	DWORD dwSizeRead;
	if (!ReadFile(fd, buf, size, &dwSizeRead, NULL))
		return(0);
	return ((tsize_t) dwSizeRead);
}

static tsize_t
_tiffWriteProc(thandle_t fd, tdata_t buf, tsize_t size)
{
	DWORD dwSizeWritten;
	if (!WriteFile(fd, buf, size, &dwSizeWritten, NULL))
		return(0);
	return ((tsize_t) dwSizeWritten);
}

static toff_t
_tiffSeekProc(thandle_t fd, toff_t off, int whence)
{
	DWORD dwMoveMethod, dwMoveHigh;

        /* we use this as a special code, so avoid accepting it */
        if( off == 0xFFFFFFFF )
            return 0xFFFFFFFF;
        
	switch(whence)
	{
	case SEEK_SET:
		dwMoveMethod = FILE_BEGIN;
		break;
	case SEEK_CUR:
		dwMoveMethod = FILE_CURRENT;
		break;
	case SEEK_END:
		dwMoveMethod = FILE_END;
		break;
	default:
		dwMoveMethod = FILE_BEGIN;
		break;
	}
        dwMoveHigh = 0;
	return ((toff_t)SetFilePointer(fd, (LONG) off, (PLONG)&dwMoveHigh,
                                       dwMoveMethod));
}

static int
_tiffCloseProc(thandle_t fd)
{
	return (CloseHandle(fd) ? 0 : -1);
}

static toff_t
_tiffSizeProc(thandle_t fd)
{
	return ((toff_t)GetFileSize(fd, NULL));
}

#ifdef __BORLANDC__
#pragma argsused
#endif
static int
_tiffDummyMapProc(thandle_t fd, tdata_t* pbase, toff_t* psize)
{
	return (0);
}

/*
 * From "Hermann Josef Hill" <lhill@rhein-zeitung.de>:
 *
 * Windows uses both a handle and a pointer for file mapping,
 * but according to the SDK documentation and Richter's book
 * "Advanced Windows Programming" it is safe to free the handle
 * after obtaining the file mapping pointer
 *
 * This removes a nasty OS dependency and cures a problem
 * with Visual C++ 5.0
 */
static int
_tiffMapProc(thandle_t fd, tdata_t* pbase, toff_t* psize)
{
	toff_t size;
	HANDLE hMapFile;

	if ((size = _tiffSizeProc(fd)) == 0xFFFFFFFF)
		return (0);
	hMapFile = CreateFileMapping(fd, NULL, PAGE_READONLY, 0, size, NULL);
	if (hMapFile == NULL)
		return (0);
	*pbase = MapViewOfFile(hMapFile, FILE_MAP_READ, 0, 0, 0);
	CloseHandle(hMapFile);
	if (*pbase == NULL)
		return (0);
	*psize = size;
	return(1);
}

#ifdef __BORLANDC__
#pragma argsused
#endif
static void
_tiffDummyUnmapProc(thandle_t fd, tdata_t base, toff_t size)
{
}

static void
_tiffUnmapProc(thandle_t fd, tdata_t base, toff_t size)
{
	UnmapViewOfFile(base);
}

/*
 * Open a TIFF file descriptor for read/writing.
 * Note that TIFFFdOpen and TIFFOpen recognise the character 'u' in the mode
 * string, which forces the file to be opened unmapped.
 */
TIFF*
TIFFFdOpen(int ifd, const char* name, const char* mode)
{
	TIFF* tif;
	BOOL fSuppressMap = (mode[1] == 'u' || (mode[1]!=0 && mode[2] == 'u'));

	tif = TIFFClientOpen(name, mode,
		 (thandle_t)ifd,
	    _tiffReadProc, _tiffWriteProc,
	    _tiffSeekProc, _tiffCloseProc, _tiffSizeProc,
		 fSuppressMap ? _tiffDummyMapProc : _tiffMapProc,
		 fSuppressMap ? _tiffDummyUnmapProc : _tiffUnmapProc);
	if (tif)
		tif->tif_fd = ifd;
	return (tif);
}

/*
 * Open a TIFF file for read/writing.
 */
TIFF*
TIFFOpen(const char* name, const char* mode)
{
	static const char module[] = "TIFFOpen";
	thandle_t fd;
	int m;
	DWORD dwMode;

	m = _TIFFgetMode(mode, module);

	switch(m)
	{
	case O_RDONLY:
		dwMode = OPEN_EXISTING;
		break;
	case O_RDWR:
		dwMode = OPEN_ALWAYS;
		break;
	case O_RDWR|O_CREAT:
		dwMode = OPEN_ALWAYS;
		break;
	case O_RDWR|O_TRUNC:
		dwMode = CREATE_ALWAYS;
		break;
	case O_RDWR|O_CREAT|O_TRUNC:
		dwMode = CREATE_ALWAYS;
		break;
	default:
		return ((TIFF*)0);
	}
	fd = (thandle_t)CreateFile(name, (m == O_RDONLY) ? GENERIC_READ :
			(GENERIC_READ | GENERIC_WRITE), FILE_SHARE_READ, NULL, dwMode,
			(m == O_RDONLY) ? FILE_ATTRIBUTE_READONLY : FILE_ATTRIBUTE_NORMAL, NULL);
	if (fd == INVALID_HANDLE_VALUE) {
		TIFFError(module, "%s: Cannot open", name);
		return ((TIFF *)0);
	}
	return (TIFFFdOpen((int)fd, name, mode));
}



/***************************************************************************
 * Structure holding data for custom TIFF routines.
 * JMB Aug 2003
 */

typedef struct 
{
	thandle_t    fd;           // Only used for sub-files
	BOOL         closeFileHandle; // Only for sub-files: should the file handle
	                              // be closed when the sub-file is closed?
	toff_t       baseOffset;   // Only used for sub-files
	size_t       subfileSize;  // Only used for sub-files
//////////////////////////////////////////////////
	tdata_t      *ppBuf;       // Only used for file buffers
	toff_t       offset;       // Current position
	size_t       *pFileSize;   // Size of the file in the buffer
	size_t       bufSize;      // May be bigger than file size to reduce number of memory allocations
} AITIFF_Data;



/***************************************************************************
 * TIFF sub-file handling
 * JMB Aug 2003
 * Note that if a sub-file is within a buffer, just use TIFFOpenBuffer() with
 * a pointer to the start of the sub-file.
 */

static tsize_t
_tiffSubfileReadProc(thandle_t clientData, tdata_t buf, tsize_t size)
{
	tsize_t sizeRead = 0;

	AITIFF_Data *pData = (AITIFF_Data*)clientData;

	if (pData && buf && size > 0)
	{
		sizeRead = _tiffReadProc(pData->fd, buf, size);
	}

	return sizeRead;
}

static tsize_t
_tiffSubfileWriteProc(thandle_t clientData, tdata_t buf, tsize_t size)
{
	return 0;
}

static toff_t
_tiffSubfileSeekProc(thandle_t clientData, toff_t off, int whence)
{
// Note that this moves the parent file's file pointer too, so it is assumed
// that following operations on a subfile, the parent file will always do a
// seek before any subsequent reads or writes. The way libtiff works seems to
// ensure this is so, but if this ever proves to be inadequate, will have to
// keep track of a 'virtual' file pointer for the subfile (similar to file
// buffer code) and only move the real file pointer immediately before a read
// or write, immediately returning it to its previous position afterwards.
	toff_t retVal = INVALID_SET_FILE_POINTER;

	AITIFF_Data *pData = (AITIFF_Data*)clientData;

	/* we use this as a special code, so avoid accepting it */
	if (off == 0xFFFFFFFF)
		return 0xFFFFFFFF;

	if (pData)
	{
		switch(whence)
		{
		case SEEK_CUR:
			retVal = _tiffSeekProc(pData->fd, off, whence);
			break;
		case SEEK_END:
			retVal = _tiffSeekProc(pData->fd, pData->baseOffset + pData->subfileSize + off, SEEK_SET);
			break;
		case SEEK_SET:
		default:
			retVal = _tiffSeekProc(pData->fd, pData->baseOffset + off, whence);
			break;
		}

		// The return value of _tiffSeekProc() is the new offset in the parent file,
		// so must adjust this to be the offset in the sub-file.
		retVal -= pData->baseOffset;
	}

	return retVal;
}

static int
_tiffSubfileCloseProc(thandle_t clientData)
{
	AITIFF_Data *pData = (AITIFF_Data*)clientData;

	if (pData)
	{
		if (pData->closeFileHandle)
			CloseHandle(pData->fd);

		_TIFFfree(pData);
	}

	return 0;
}

static toff_t
_tiffSubfileSizeProc(thandle_t clientData)
{
	toff_t size = INVALID_FILE_SIZE;

	AITIFF_Data *pData = (AITIFF_Data*)clientData;

	if (pData)
		size = pData->subfileSize;

	return size;
}

/*
 * Open an TIFF sub-file for reading
 */
static TIFF*
OpenSubfile(AITIFF_Data *pData, const char *name)
{
// This only allows reading, as a subfile is embedded in another file and so
// while writing could be allowed within the existing bounds of the file, that
// is unlikely to be useful as the subfile size will not be known in advance.
// (To write a subfile, write it to a buffer, then save the buffer to file).
	TIFF *pTif = NULL;

	// Check parameters
	if (   pData && pData->fd != INVALID_HANDLE_VALUE
	    && pData->baseOffset >= 0 && pData->subfileSize > 0)
	{
		char subfileName[MAX_PATH];
		if (name)
		{
			strcpy(subfileName, "Sub-file of ");
			strncat(subfileName, name, MAX_PATH - strlen("Sub-file of "));
		}
		else
			strcpy(subfileName, "Sub-file");


		// Move file pointer to beginning of the subfile, so that the reads
		// done by TIFFClientOpen() will start from here.
		_tiffSubfileSeekProc((thandle_t)pData, 0, SEEK_SET);

		pTif = TIFFClientOpen(subfileName, "r", (thandle_t)pData,
		                      _tiffSubfileReadProc, _tiffSubfileWriteProc,
		                      _tiffSubfileSeekProc, _tiffSubfileCloseProc, _tiffSubfileSizeProc,
		                      _tiffDummyMapProc, _tiffDummyUnmapProc);

		if (pTif)
			pTif->tif_fd = (int)pData->fd;
	}

	return pTif;
}


TIFF*
TIFFFdOpenSubfile(int fd, const char *name, toff_t offset, tsize_t size)
{
	TIFF *pTif = NULL;

	AITIFF_Data *pData = _TIFFmalloc(sizeof(AITIFF_Data));

	if (pData)
	{	
		pData->fd              = (thandle_t)fd;
		// The file handle has been opened externally and belongs to the parent
		// file, so should not be closed when the sub-file is closed.
		pData->closeFileHandle = FALSE;
		pData->baseOffset      = offset;
		pData->subfileSize     = size;

		pTif = OpenSubfile(pData, name);

		if (!pTif)
			_TIFFfree(pData);
	}

	return pTif;
}

TIFF*
TIFFOpenSubfile(const char *name, toff_t offset, tsize_t size)
{
	static const char module[] = "TIFFOpenSubfile";
	TIFF *pTif = NULL;


	// Check parameters
	if (name && offset >= 0 && size > 0)
	{
		AITIFF_Data *pData = _TIFFmalloc(sizeof(AITIFF_Data));

		if (pData)
		{	
			pData->fd = (thandle_t)CreateFile(name, GENERIC_READ, FILE_SHARE_READ, NULL,
			                                  OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

			if (pData->fd != INVALID_HANDLE_VALUE)
			{
				// The file handle has been opened internally to the sub-file
				// routines and so should be closed when the sub-file is closed.
				pData->closeFileHandle = TRUE;
				pData->baseOffset      = offset;
				pData->subfileSize     = size;

				pTif = OpenSubfile(pData, name);

				if (!pTif)
				{
					CloseHandle(pData->fd);
					_TIFFfree(pData);
				}
			}
			else
			{
				TIFFError(module, "%s: Cannot open", name);
				_TIFFfree(pData);
			}
		}
	}

	return pTif;
}


/***************************************************************************
 * In-memory TIFF file handling
 * JMB Aug 2003
 * Note that to open a sub-file within a buffer, just use TIFFOpenBuffer() with
 * a pointer to the start of the sub-file.
 */

static tsize_t
_tiffReadBuffProc(thandle_t clientData, tdata_t buf, tsize_t size)
{
// Copy 'size' bytes from file buffer at current offset, unless that would go
// beyond bounds of buffer, in which case copy as many bytes as possible.
// The current position is incremented by the number of bytes read.
	tsize_t sizeRead = 0;

	AITIFF_Data *pData = (AITIFF_Data*)clientData;

	if (   pData && pData->ppBuf && *(pData->ppBuf)
	    && pData->offset >= 0 && *(pData->pFileSize) > 0
	    && pData->offset < *(pData->pFileSize)
	    && buf && size > 0)
	{
		if ((pData->offset + size) > *(pData->pFileSize))
			sizeRead = *(pData->pFileSize) - pData->offset;
		else
			sizeRead = size;

		_TIFFmemcpy(buf, (BYTE*)*(pData->ppBuf) + pData->offset, sizeRead);

		pData->offset += sizeRead;
	}

	return sizeRead;
}

static tsize_t
_tiffWriteBuffProc(thandle_t clientData, tdata_t buf, tsize_t size)
{
// Copy 'size' bytes into file buffer at current offset, unless that would go
// beyond bounds of buffer, in which case enlarge buffer.
// The current position is incremented by the number of bytes written.
	tsize_t sizeWritten = 0;

	AITIFF_Data *pData = (AITIFF_Data*)clientData;

	if (   pData && pData->ppBuf && pData->offset >= 0
	    && *(pData->pFileSize) >= 0 && pData->bufSize >= *(pData->pFileSize)
	    && buf && size > 0)
	{
		if ((pData->offset + size) > pData->bufSize)
		{
			// Don't allocate anything less than 4K at a time, for efficiency
			if (((pData->offset + size) - pData->bufSize) < 4096)
				pData->bufSize += 4096;
			else
				pData->bufSize = pData->offset + size;

			*(pData->ppBuf) = _TIFFrealloc(*(pData->ppBuf), pData->bufSize);
		}

		if (*(pData->ppBuf))
		{
			sizeWritten = size;

			_TIFFmemcpy((BYTE*)*(pData->ppBuf) + pData->offset, buf, sizeWritten);

			// Increment the current position by the number of bytes written
			pData->offset += sizeWritten;

			// Increase the file size if the write has made it bigger.
			if (pData->offset > *(pData->pFileSize))
				*(pData->pFileSize) = pData->offset;
		}
	}

	return sizeWritten;
}

static toff_t
_tiffSeekBuffProc(thandle_t clientData, toff_t off, int whence)
{
// Note that it is possible to seek to any offset whatever the current size of
// the buffer, or even of there is no buffer currently allocated. Subsequent
// reads will fail if the offset is out of bounds though. Subsequent writes
// will enlarge the buffer if they go beyond its end.
	toff_t retVal = INVALID_SET_FILE_POINTER;

	AITIFF_Data *pData = (AITIFF_Data*)clientData;

	/* we use this as a special code, so avoid accepting it */
	if (off == 0xFFFFFFFF)
		return 0xFFFFFFFF;

	if (pData)
	{
		switch(whence)
		{
		case SEEK_CUR:
			pData->offset = pData->offset + off;
			break;
		case SEEK_END:
			// If this takes us beyond the end of the buffer, the buffer must
			// be enlarged next time there is a write operation (which will
			// begin from the current offset).
			pData->offset = *(pData->pFileSize) + off;
			break;
		case SEEK_SET:
		default:
			pData->offset = off;
			break;
		}

		retVal = pData->offset;
	}

	return retVal;
}

static int
_tiffCloseBuffProc(thandle_t clientData)
{
	AITIFF_Data *pData = (AITIFF_Data*)clientData;

	if (pData)
		_TIFFfree(pData);

	return 0;
}

static toff_t
_tiffSizeBuffProc(thandle_t clientData)
{
	toff_t size = INVALID_FILE_SIZE;

	AITIFF_Data *pData = (AITIFF_Data*)clientData;

	if (pData && pData->ppBuf && *(pData->ppBuf))
		size = *(pData->pFileSize);

	return size;
}

static int
_tiffMapBuffProc(thandle_t clientData, tdata_t* pbase, toff_t* psize)
{
	int retVal = 0;
	AITIFF_Data *pData = (AITIFF_Data*)clientData;

	if (pData && pData->ppBuf && *(pData->ppBuf))
	{
		*pbase = *(pData->ppBuf);
		*psize = *(pData->pFileSize);
		retVal = 1;
	}

	return retVal;
}

static void
_tiffUnmapBuffProc(thandle_t clientData, tdata_t base, toff_t size)
{
	// Nothing to do!
}


// This uses invalid filename characters, so could never be a real filename.
#define BUFFER_FILENAME "<* IN MEMORY! *>" 


/*
 * Open an in-memory TIFF file for reading/writing
 */
TIFF*
TIFFOpenBuffer(tdata_t *ppBuf, tsize_t *pSize, const char mode[])
{
// Not sure if this will currently work for any modes other than "w" or "r".
// If writing and an existing buffer is not being used, ppBuf should point at
// a NULL pointer and pSize should point at a value of zero. If an existing
// buffer is being used, it should have been allocated with _TIFFmalloc().
// Buffers created or modified by this code should be freed with _TIFFfree().
// When writing has finished, pSize will point at the current 'file size' i.e.
// the size of the TIFF file data held in the buffer. The size of the buffer
// itself may be larger but the contents of the rest of the buffer should be
// ignored.
	TIFF *pTif = NULL;
	static const char module[] = "TIFFOpenBuffer";

	// Check parameters
	if (ppBuf && pSize && mode && _TIFFgetMode(mode, module) >= 0)
	{
		// For "r" or "r+" modes, buffer must already exist.
		if (mode[0] != 'r' || (mode[0] == 'r' && *ppBuf && *pSize > 0))
		{
			BOOL fSuppressMap = (mode[1] == 'u' || (mode[1] != 0 && mode[2] == 'u'));

			AITIFF_Data *pData = _TIFFmalloc(sizeof(AITIFF_Data));

			if (pData)
			{	
				pData->ppBuf     = ppBuf;
				pData->pFileSize = pSize;
				pData->bufSize   = *pSize;
				pData->offset    = 0;

				pTif = TIFFClientOpen(BUFFER_FILENAME, mode, (thandle_t)pData,
				                      _tiffReadBuffProc, _tiffWriteBuffProc,
				                      _tiffSeekBuffProc, _tiffCloseBuffProc, _tiffSizeBuffProc,
				                      fSuppressMap ? _tiffDummyMapProc   : _tiffMapBuffProc,
				                      fSuppressMap ? _tiffDummyUnmapProc : _tiffUnmapBuffProc);

				if (pTif)
					pTif->tif_fd = (int)INVALID_HANDLE_VALUE;
				else
					_TIFFfree(pData);
			}
		}
	}

	return pTif;
}



/****************************************************************************/



static void
Win32WarningHandler(const char* module, const char* fmt, va_list ap)
{
#ifndef TIF_PLATFORM_CONSOLE
	LPTSTR szTitle;
	LPTSTR szTmp;
	LPCTSTR szTitleText = "%s Warning";
	LPCTSTR szDefaultModule = "TIFFLIB";
	szTmp = (module == NULL) ? (LPTSTR)szDefaultModule : (LPTSTR)module;
	if ((szTitle = (LPTSTR)LocalAlloc(LMEM_FIXED, (lstrlen(szTmp) +
			lstrlen(szTitleText) + lstrlen(fmt) + 128)*sizeof(TCHAR))) == NULL)
		return;
	wsprintf(szTitle, szTitleText, szTmp);
	szTmp = szTitle + (lstrlen(szTitle)+2)*sizeof(TCHAR);
	wvsprintf(szTmp, fmt, ap);
	MessageBox(GetFocus(), szTmp, szTitle, MB_OK | MB_ICONINFORMATION);
	LocalFree(szTitle);
	return;
#else
	if (module != NULL)
		fprintf(stderr, "%s: ", module);
	fprintf(stderr, "Warning, ");
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, ".\n");
#endif        
}
TIFFErrorHandler _TIFFwarningHandler = Win32WarningHandler;

static void
Win32ErrorHandler(const char* module, const char* fmt, va_list ap)
{
#ifndef TIF_PLATFORM_CONSOLE
	LPTSTR szTitle;
	LPTSTR szTmp;
	LPCTSTR szTitleText = "%s Error";
	LPCTSTR szDefaultModule = "TIFFLIB";
	szTmp = (module == NULL) ? (LPTSTR)szDefaultModule : (LPTSTR)module;
	if ((szTitle = (LPTSTR)LocalAlloc(LMEM_FIXED, (lstrlen(szTmp) +
			lstrlen(szTitleText) + lstrlen(fmt) + 128)*sizeof(TCHAR))) == NULL)
		return;
	wsprintf(szTitle, szTitleText, szTmp);
	szTmp = szTitle + (lstrlen(szTitle)+2)*sizeof(TCHAR);
	wvsprintf(szTmp, fmt, ap);
	MessageBox(GetFocus(), szTmp, szTitle, MB_OK | MB_ICONEXCLAMATION);
	LocalFree(szTitle);
	return;
#else
	if (module != NULL)
		fprintf(stderr, "%s: ", module);
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, ".\n");
#endif        
}
TIFFErrorHandler _TIFFerrorHandler = Win32ErrorHandler;
