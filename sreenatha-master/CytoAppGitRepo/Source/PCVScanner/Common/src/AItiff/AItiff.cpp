
/*
 * AItiff.cpp
 * JMB April 2001
 *
 * 19Sep03	JMB	Added GetIdealMIPMapLOD() and GetAvailableMIPMapLOD().
 * 20Aug03	JMB	Added LoadSubfile() and LoadSubfileComponent(). These are now
 *				used to load individual MIP maps on demand.
 *				Also added private functions Load() and Save().
 * 14Aug03	JMB	Modified LoadFileBuffer(), SaveFileBuffer() to use new direct
 *				buffer reading and writing functions (from AITif_win32.c),
 *				rather than having to create temporary files (relatively slow).
 * 03Feb03	JMB	Added rounding direction option to AITiffScale::SetFromDouble().
 * 22Jan03	JMB	Added PartialCopyInto().
 * 13Nov02	JMB	Added AITiffScale::SetFromDouble().
 * 25Feb02  SN  Fixed memory leak in CopyInto() by freeing buffer.
 * 16Jan02	JMB	Added MIP maps.
 * 02Dec01  WH:	Added StageInfo stuff
 * 27Nov01  KS: Fix size calculation in GetComponentBytes
 * 12Nov01  WH: added CopyInto().
 * 28Oct01	WH:	Support 1 bit images, get rid of dataBytesPerPixel and GetComponentBytesPerPixel() thing
 * 23Oct01	WH: AddComponent needs to configure the display buffer otherwise w and h don't get set.
 * 22Oct01	WH: include custom tags AI_TAG_ZSTACK, for serialized objective view objects via
 *				WriteZStack and ReadZStack.
 * 22May01	WH: include custom tags AI_TAG_ANNOTATION, for serialized objective view objects via
 *				WriteAnnotation and ReadAnnotation.
 *	1May01	WH:	included custom tags AI_TAG_IMAGEOPS, for the image operation list actions via
 *				WriteImageOps and ReadImageOps
 */

#include "AItiff.h"

#include "../libtiff/libtiff/tiffiop.h"

#include "xtiffio.h" // For custom tags support

#include <math.h> // For floor() and ceil()
#include <assert.h>
#include <gdiplus.h>

// JPEG2000 support (from AItif_jpeg2000.c)
extern "C" int TIFFInitJPEG2000(TIFF *tif, int scheme);


//////////////////////////////////////////////////////////////////////////////
// AITiff critical section object. This is a global object, so that it is
// shared between all threads and all AITiff objects in a process.
// It is initialised by the constructor of a single global AITiffInitialiser
// object (which will by its nature be called before any AI_Tiff objects are
// created).
//
// The critical section is used to make using AI_Tiff objects thread-safe
// - without it, multithreaded code was crashing randomly, with pointers
// becoming invalid for no apparent reason. It does this in two ways:
// 1) It prevents more than one thread at a time from accessing the internals
//    of any particular AI_Tiff object. This prevents, for example, one thread
//    modifying an AI_Tiff at the same time as another thread is accessing data
//    in it - pointers being used by the second thread may become invalid as
//    data is moved or deleted by the first thread (e.g. the render buffer is
//    deleted then reallocated when a larger MIP map is loaded).
// 2) It prevents more than one thread at a time from running any code used by
//    AI_Tiff objects (even via different AI_Tiff objects), perhaps third-party
//    library code that is not threadsafe.
// In fact (1) is implied by (2), but I describe (1) separately as my first
// implementation only implemented (1), by having a separate critical section
// object in each AI_Tiff. However this proved to be insufficient as
// multithreaded code still crashed, although I don't know what code caused
// this. So (2) was implemented by making all AI_Tiff objects share the same
// critical section, which seems to work well.
//
// I think only public functions need to be protected by the critical section,
// (because private functions will only be called within a public function's
// critical section). However if the only critical part of one or more public
// functions is within a private function, the critical section could be put
// there.

// The AITiff critical section object.
static CRITICAL_SECTION criticalSection;

// The sole purpose of this class is to initialise the critical section.
class AITiffInitialiser
{
public:
	// Want to use: InitializeCriticalSectionAndSpinCount(&criticalSection, 4000); but compiler says it is an undeclared identifier!
	AITiffInitialiser(){ InitializeCriticalSection(&criticalSection); }
	~AITiffInitialiser(){ DeleteCriticalSection(&criticalSection); }
};

// The one and only initialiser object.
// If you have difficultly placing a breakpoint in the initialiser code that
// is called when this object is created (I did - the debugger hung) you could
// try creating an initialiser object on the heap instead - do it when AITiff's
// DllMain() function is called, with ul_reason_for_call == DLL_PROCESS_ATTACH.
static AITiffInitialiser initialiser;

// Note that it may be useful for some non-AI_Tiff modules to share this
// critical section object in order to prevent deadlocks.
CRITICAL_SECTION *AITiffGetCriticalSection(){ return &criticalSection; }

//////////////////////////////////////////////////////////////////////////////


// Reference to default error handling function,
// used when a different error handler has been temporarily substituted.
static TIFFErrorHandler pDefaultErrorHandler = NULL;

static void ErrorHandler(const char* module, const char* fmt, va_list ap)
{
// If the static part of the error message contains one of the strings
// specified in the lines below, ignore the message by returning.
	if (fmt && strstr(fmt, "Rational with zero denominator") != NULL) return;

// Message has not been ignored: call original error handler
	if (pDefaultErrorHandler && pDefaultErrorHandler != ErrorHandler)
		pDefaultErrorHandler(module, fmt, ap);
}


AI_Tiff::AI_Tiff(BOOL gdiplusStartedExternally/*=FALSE*/)
{
	pComponents  = NULL;
	pBuf         = NULL;
	filename     = NULL;
	gdiplusToken = NULL;
	this->gdiplusStartedExternally = gdiplusStartedExternally;

	Reset();

	// Register AITiff's JPEG2000 codec with libtiff. This only needs to be
	// done once for all instances of AI_Tiff.
	if (!TIFFFindCODEC(COMPRESSION_JPEG2000))
		TIFFRegisterCODEC(COMPRESSION_JPEG2000, "JPEG2000", TIFFInitJPEG2000);

	// Also use this codec for Leadtools JP2000 tag, so can read old test data
	// and possible Leadtools' files.
	if (!TIFFFindCODEC(COMPRESSION_JP2000))
		TIFFRegisterCODEC(COMPRESSION_JP2000, "JP2000", TIFFInitJPEG2000);
}

AI_Tiff::~AI_Tiff()
{
	Reset();
}


void
AI_Tiff::Reset()
{
// Free any loaded data and initialise variables.

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pCurrComponent = pComponents;
	while (pCurrComponent)
	{
		AI_TiffComponent *pNextComponent = pCurrComponent->pNext;

		delete pCurrComponent;

		pCurrComponent = pNextComponent;
	}	
	pComponents = NULL;

	colourMixFast = TRUE;
	interpolateScaleUps = FALSE;

	// Free the display buffer and set its parameters to zero
	FreeRenderBuffer();

	renderRequired = TRUE;

	if (filename) free(filename);
	filename = NULL;

	pFileBuffer = NULL;
	subfileOffset = subfileSize = 0;

	drawFlipX = drawFlipY = FALSE;

	// Set default compression settings for AITiff
	SetJPEGQuality(75);    // Default value set by libtiff, in tif_jpeg.c, is 75.

	// For ZIP compression we will usually want the fastest setting, which is 1.
	// The settings don't make a huge amount of difference to the compression
	// achieved, but they do make a difference to how fast it is (though this
	// may only affect the compression speed, not the decompression speed??)
	// (-1 would make the library to use its default value (6), see zlib.h).
	SetDEFLATEQuality(1);

	// Set a sensible lossy default for JPEG2000
	// (the default for the JasPer library is 1.0, i.e. lossless).
	SetJPEG2000Rate(0.1);

	if (gdiplusToken) // gdiplusToken is always NULL if gdiplusStartedExternally is TRUE.
		Gdiplus::GdiplusShutdown(gdiplusToken);


	LeaveCriticalSection(&criticalSection);
}


int
AI_Tiff::GetMemoryUsage()
{
// Returns an approximation of the current memory usage of this AI_Tiff, in kilobytes.
	// Start off with 1K to cover memory usage other than that used by the render buffer and components.
	int kilobytes = 1;

	EnterCriticalSection(&criticalSection);

	kilobytes += (bufBytes >> 10); // Render (display) buffer size

	// Component data sizes, including any MIP maps.
	AI_TiffComponent *pComponent = pComponents;
	while (pComponent)
	{
		kilobytes += pComponent->GetMemoryUsage();

		pComponent = pComponent->pNext;
	}

	LeaveCriticalSection(&criticalSection);

	return kilobytes;
}



BOOL
AI_Tiff::Load(tdata_t buffer, thandle_t fd, const char fileName[],
              toff_t offset, tsize_t size, BOOL noData)
{
// The parameters 'buffer', 'fd' and 'fileName' are checked one at a time, in
// that order, the first one that is a valid value being used to open the file.
// If 'offset' is greater than zero, the file is a sub-file starting at that
// offset within the file specified by 'buffer', 'fd' or 'fileName'.
// The size of a file buffer or sub-file is given by 'size'. If a sub-file is
// within a buffer, 'size' is the size of the sub-file, not the buffer.
//
// The noData flag means that image data for the components is not loaded.
// This is intended as an optimisation for large images: the image data
// is only loaded if it becomes visible. Also, if the image contains MIP maps,
// only the appropriate map for the current zoom level need be loaded. This
// reduces load times and memory usage. The component data is loaded using
// the AI_Tiff::LoadComponentData() function.
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	// Override error handler to supress errors generated when reading CytoVision images.
	// First check this has not been done already (e.g. ReadComponent may call LoadFile,
	// so there may be a parent call to ReadFile above this one that has done it already),
	// or we will end up with DefaultErrorHandler == ErrorHandler.
	if (pDefaultErrorHandler == NULL)
		pDefaultErrorHandler = TIFFSetErrorHandler(ErrorHandler);


	// Clear out any data that may have been previously loaded	
	Reset();


	// Load file according to type.
	// Use XTIFF routines for custom tags (remember to call XTIFFClose())

	if (buffer)
	{
	// The file is contained in an in-memory buffer
		// Save the buffer pointer for possible later use by LoadSubfile().
		// Note that if this is a sub-file the saved buffer pointer never
		// points to the start of the sub-file but always to the whole buffer.
		pFileBuffer = buffer;

		if (offset > 0)
			// The file is a subfile of the file in the buffer.
			buffer = (tdata_t)((BYTE*)buffer + offset);

		pTIFF = XTIFFOpenBuffer(&buffer, &size, "r");
	}
	else if (offset > 0)
	{
	// The file is a subfile
		if (fd != INVALID_HANDLE_VALUE)
		    pTIFF = XTIFFFdOpenSubfile((int)fd, fileName, offset, size);
		else
		    pTIFF = XTIFFOpenSubfile(fileName, offset, size);
	}
	else
	{
	// Standard file
		if (fd != INVALID_HANDLE_VALUE)
		    pTIFF = XTIFFFdOpen((int)fd, fileName, "r");
		else
		    pTIFF = XTIFFOpen(fileName, "r");
	}


    if (pTIFF)
	{
		pComponents = ReadComponent(0, noData);

		AI_TiffComponent *pCurrComponent = pComponents;

		for (int i=1; pCurrComponent; i++)
		{
			pCurrComponent->pNext = ReadComponent(i, noData);

			pCurrComponent = pCurrComponent->pNext;
		}

		XTIFFClose(pTIFF);


		if (fileName)
		{
			filename = (char*)malloc(strlen(fileName) + 1);
			if (filename) strcpy(filename, fileName);
		}

		if (offset > 0)
		{
			// Save the subfile parameters for later use by
			// LoadSubfile() or LoadComponentData().
			subfileOffset = offset;
			subfileSize   = size;
		}


		status = TRUE;
	}


	if (pDefaultErrorHandler != NULL)
	{
		// Put back default error handler
		TIFFSetErrorHandler(pDefaultErrorHandler);
		pDefaultErrorHandler = NULL; // Indicate that default handler is in place
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}


BOOL
AI_Tiff::LoadFile(const char fileName[], BOOL noData/*=FALSE*/)
{
	return Load(NULL, (thandle_t)INVALID_HANDLE_VALUE, fileName, 0, 0, noData);
}


BOOL
AI_Tiff::LoadSubfile(const AI_Tiff *pParent, toff_t offset, tsize_t size, BOOL noData)
{
	BOOL status = FALSE;

	if (pParent)
	{
		EnterCriticalSection(&criticalSection);

		// Check if this is a sub-file of a sub-file.
		if (pParent->subfileOffset > 0)
		{
			// The subfile cannot go beyond the end of the parent sub-file.
			if ((tsize_t)offset + size > pParent->subfileSize)
				size = pParent->subfileSize - offset;

			// Add the offset to the parent's offset to get the offset from
			// the start of the top-level file.
			offset += pParent->subfileOffset;
		}

		status = Load(pParent->pFileBuffer, (thandle_t)pParent->pTIFF->tif_fd, pParent->pTIFF->tif_name,
			          offset, size, noData);

		LeaveCriticalSection(&criticalSection);	
	}

	return status;
}


BOOL
AI_Tiff::LoadComponentData(AI_TiffComponent *pComponent)
{
// Loads the component data (TiffComponent::pData) for the given component,
// from the file that LoadFile() or LoadSubFile() was last called for. This
// is used when LoadFile() or LoadSubFile() was previously called with
// the noData parameter set to TRUE.
// This function doesn't work for files opened from a buffer - firstly because
// if you have a buffer you have had to load all the data from disk already so
// it seems pointless using the noData parameter, secondly because following
// initial load of the file the buffer is often discarded, so the pFileBuffer
// pointer held in the AI_Tiff will no longer be valid. 
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	if (pComponent && !pComponent->pData && pComponent->fileIndex > -1 && filename)
	{
		// Override error handler to supress errors generated when reading CytoVision images.
		// First check this has not been done already.
		if (pDefaultErrorHandler == NULL)
			pDefaultErrorHandler = TIFFSetErrorHandler(ErrorHandler);


		// Use XTIFF routines for custom tags (remember to call XTIFFClose())
		if (subfileOffset > 0)
			pTIFF = XTIFFOpenSubfile(filename, subfileOffset, subfileSize);
		else
			pTIFF = XTIFFOpen(filename, "r"); 


		if (pTIFF)
		{
			if (ReadComponentData(pComponent)) status = TRUE;

			XTIFFClose(pTIFF);
		}


		if (pDefaultErrorHandler != NULL)
		{
			// Put back default error handler
			TIFFSetErrorHandler(pDefaultErrorHandler);
			pDefaultErrorHandler = NULL; // Indicate that default handler is in place
		}
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}


BOOL
AI_Tiff::LoadFileBuffer(const char *fileBuffer, size_t size)
{
// Load TIFF data from a block of memory
	// Not a real file so no filename.
	// Set the noData parameter to FALSE as it doesn't make much sense not to
	// load when it is loadedinto memory already.
	return (Load((tdata_t*)fileBuffer, (thandle_t)INVALID_HANDLE_VALUE, NULL, 0, size, FALSE));
}

/*
Have commented this out as it (surprisingly) doesn't seem to offer any performance gains.
void
AI_Tiff::SetupWriteBuffer()
{
// Libtiff will call TIFFWriteBufferSetup() automatically if needed,
// when TIFFWriteEncodedStrip() is called (via the BUFFERCHECK() macro).
// However, there are two problems with this:
// 1. The current implementation has a bug for strip sizes larger than the 8KB
//    default, such that the buffer is only made as large as the scanline size,
//    not the strip size.
// 2. Even if the automatic allocation worked correctly for large strip sizes,
//    it would (probably) just base the buffer size on the strip size for the
//    first component that was saved, even though this may not have the largest
//    strip size in a multi-component image with different bit-depths.
// Calling this function before writing any components should allocate the
// buffer to a better size, and prevent it being allocated automatically.
	if (pTIFF && pComponents)
	{
		int bufferSize = 0;
		AI_TiffComponent *pComponent = pComponents;
		while (pComponent)
		{
			int stripSize = pComponent->GuessIdealStripHeight() * pComponent->WidthBytes();
			if (stripSize > bufferSize)
				bufferSize = stripSize;

			pComponent = pComponent->pNext;
		}

		// Add extra 1KB to size, as real strip size may be larger than estimate.
		TIFFWriteBufferSetup(pTIFF, NULL, bufferSize + 1024);
	}
}
*/

BOOL 
AI_Tiff::Save(tdata_t *ppBuffer, tsize_t *pSize, const char fileName[], uint16 compression)
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	// Save file according to type.
	// Use XTIFF routines for custom tags (remember to call XTIFFClose())
	if (ppBuffer)
	{
		pTIFF = XTIFFOpenBuffer(ppBuffer, pSize, "w");
	}
	else
	{
	    pTIFF = XTIFFOpen(fileName, "w");
	}


    if (pTIFF)
	{
		//SetupWriteBuffer();

		AI_TiffComponent *pCurrComponent = pComponents;

		while (pCurrComponent)
		{
			WriteComponent(pCurrComponent, compression);
			pCurrComponent = pCurrComponent->pNext;
		}

		XTIFFClose(pTIFF);

		status = TRUE;
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}


BOOL 
AI_Tiff::SaveFile(const char fileName[], uint16 compression)
{
	return Save(NULL, 0, fileName, compression);
}


char *
AI_Tiff::SaveFileBuffer(size_t *pSize, uint16 compression)
{
// Return a block of memory containing current TIFF data as a file
	tdata_t pBuffer = NULL;
	tsize_t tsize = 0;

	// Not a real file so no filename
	if (Save(&pBuffer, &tsize, NULL, compression))
	{
		*pSize = tsize;
	}

	return (char*)pBuffer;
}

void
AI_Tiff::FreeFileBuffer(char *pBuf)
{
// Free a buffer allocated by AI_Tiff::SaveFileBuffer()
	if (pBuf) _TIFFfree(pBuf);
}


BOOL AI_Tiff::CopyInto(AI_Tiff *pDestTiff)
{
// This function ensures a full copy by writing the AI_Tiff to
// a (in-memory) file then reading the file into the copy tiff.
	BOOL status = FALSE;

	if (pDestTiff)
	{
		size_t size;
		char *pBuf = SaveFileBuffer(&size);
		if (pBuf)
		{
			if (pDestTiff->LoadFileBuffer(pBuf, size))
				status = TRUE;
	
			FreeFileBuffer(pBuf);
		}	
	}

	return status;
}

/*
BOOL AI_Tiff::PartialCopyInto(AI_Tiff *pDestTiff)
{
// This function copies just enough of the TIFF to create something that can be
// displayed, as a function to duplicate all of the AI_Tiff data structures in
// memory would be long, complicated and a pain to maintain - that's why
// AI_Tiff::CopyInto() copies a Tiff by re-using the file saving and loading code.
// However, CopyInto() can be rather slow, so this function is a fast alternative
// suitable only certain limited uses.
// If pDestTiff has no components, partial copies of all components are copied
// into it. It pDestTiff already has components it is assumed that this is the
// original copy of the Tiff and that only the updated image data and ops list
// are to be copied back. If a partial copy of a Tiff is to be copied back,
// nothing about it must have been modified except the values of the pixels or
// the contents of the ops list.
	BOOL status = TRUE;

	if (pDestTiff)
	{
		AI_TiffComponent *pComponent     = this->pComponents;
		AI_TiffComponent *pDestComponent = pDestTiff->pComponents;

		while (pComponent)
		{
			if (pDestComponent)
			{
				// The destination component should be the original copy of the component,
				// so just update contents of image buffer.
				if (pComponent->pData)
				{
					if (pDestComponent->pData && pDestComponent->dataBytes == pComponent->dataBytes)
						memcpy(pDestComponent->pData, pComponent->pData, pComponent->dataBytes);
					else
					{ // Destination component doesn't match original.
						status = FALSE;
						break;
					}
				}

				ImageOp *pCopiedOps = pComponent->CopyOpsList();
				if (pCopiedOps || pComponent->m_iops_list == NULL)
				{
					pDestComponent->DeleteOpsList();
					pDestComponent->m_iops_list = pCopiedOps;
				}
				else
				{ // Failed to copy ops list.
					status = FALSE;
					break;
				}

				// If there are no more components in the destination Tiff,
				// don't allow more to be added if there are more in the source.
				if (pComponent->pNext && !pDestComponent->pNext)
				{
					status = FALSE;
					break;
				}
			}
			else
			{
				pDestComponent = new AI_TiffComponent;

				if (pDestComponent)
				{
					pDestComponent->bpp       = pComponent->bpp;
					pDestComponent->colour    = pComponent->colour;
					pDestComponent->width     = pComponent->width;
					pDestComponent->height    = pComponent->height;
					pDestComponent->dataBytes = pComponent->dataBytes;

					pDestComponent->SetDescription(pComponent->description);

					if (pComponent->pData)
					{
						pDestComponent->pData = (uint8*)_TIFFmalloc(pComponent->dataBytes + 1024); //For luck
						if (pDestComponent->pData)
							memcpy(pDestComponent->pData, pComponent->pData, pComponent->dataBytes);
						else
						{ // Failed to allocate memory
							status = FALSE;
							break;
						}

						pDestComponent->m_displayBuf = pDestComponent->m_pCache = pDestComponent->pData;
					}


					pDestComponent->m_iops_list = pComponent->CopyOpsList();
					if (pDestComponent->m_iops_list == NULL && pComponent->m_iops_list != NULL)
					{ // Failed to copy ops list
						status = FALSE;
						break;
					}


					// Do tail insertion of new component so that components
					// in copy are in same order as in source.
					AI_TiffComponent *pDestPtr = pDestTiff->pComponents;
					if (!pDestPtr)
						pDestTiff->pComponents = pDestComponent;
					else
					{
						while (pDestPtr)
						{
							if (!pDestPtr->pNext)
							{
								// Reached tail element; point at new element.
								pDestPtr->pNext = pDestComponent;
								break;
							}

							pDestPtr = pDestPtr->pNext;
						}
					}
				}
				else
				{ // Failed to create new component
					status = FALSE;
					break;
				}
			}

			// Process updated image operations.
			pDestComponent->ProcessIntoCache();

			pComponent     = pComponent->pNext;
			pDestComponent = pDestComponent->pNext;
		}

		// The Tiff has been modified, so a render is required.
		pDestTiff->renderRequired = TRUE;
	}
	else status = FALSE; // NULL pointer passed to function.

	return status;
}
*/

BOOL
AI_Tiff::WriteComponent(AI_TiffComponent *pComponent, uint16 compression)
{
// Write AI_IFD structure and associated data to end of file.
	toff_t aiDataOffset = TIFFSeekFile(pTIFF, 0, SEEK_END);


	// Create an array of TIFFDirEntry structures which is large enough to
	// contain all the possible tags in the AI_IFD
	TIFFDirEntry *pEntries = new TIFFDirEntry [ 32 ];

	// One at a time, create directory entries for variables which are being
	// used, and write their associated data blocks (if any) out to disk,
	// keeping a record of their offsets in the directory entries.
	// Note that, unlike standard TIFF data, the AI data always uses native
	// byte order, and variable length data (e.g. ASCII) is never stored in the
	// 'offset' member of a directory entry even if its 'count' indicates that
	// it can be.
	toff_t currDataOffset = aiDataOffset;
	uint16 numEntries     = 0;


	if (WriteColour(pComponent, pEntries + numEntries, &currDataOffset))
		numEntries++;

	// Write image operations list to component
	if (WriteImageOps(pComponent, pEntries + numEntries, &currDataOffset))
		numEntries++;

	// Write image operations list to component
	if (WriteAnnotation(pComponent, pEntries + numEntries, &currDataOffset))
		numEntries++;

	if (WriteZStack(pComponent, pEntries + numEntries, &currDataOffset))
		numEntries++;

	if (WriteZStackInfo(pComponent, pEntries + numEntries, &currDataOffset))
		numEntries++;

	if (WriteStageInfo(pComponent, pEntries + numEntries, &currDataOffset))
		numEntries++;

	if (pComponent->pMIPMaps)
		if (WriteMIPMaps(pComponent, pEntries + numEntries, &currDataOffset, compression))
			numEntries++;
/*
// Example code, do not delete!
	if (WriteExampleString(pComponent, pEntries + numEntries, &currDataOffset))
		numEntries++;
	if (WriteExampleLong(pComponent, pEntries + numEntries, &currDataOffset))
		numEntries++;
	if (WriteExampleStructArray(pComponent, pEntries + numEntries, &currDataOffset))
		numEntries++;
*/

	toff_t aiDirOffset = currDataOffset;

	// Now write the AI_IFD:
	// Write number of entries as a 2 byte number
	TIFFWriteFile(pTIFF, &numEntries, sizeof(numEntries));
	// Write the entries:
	for (int i = 0; i < numEntries; i++)
	{
		TIFFWriteFile(pTIFF, pEntries + i, sizeof(TIFFDirEntry));
	}


	delete[] pEntries;


	// Create a tag in the main IFD which points at the AI_IFD
	TIFFSetField(pTIFF, TIFFTAG_APPLIEDIMAGING, aiDirOffset);


	// Set some of the standard values which will be saved by Tiff::WriteComponent()
	
	// Use packbits rather than deflate unless deflate is better and is needed, as
	// packbits is more standard?
	//pComponent->compression = COMPRESSION_PACKBITS;
	//pComponent->compression = COMPRESSION_DEFLATE;

	if (pComponent->width * pComponent->height < 128)
		// If the image is very small, don't use compression, as the overhead
		// (in time and space) is not worth it, paricularly for more complex
		// compression schemes. As the size threshold for this depends on both
		// the compression method and the contents of the image it is difficult
		// to pick a size, but around 100 pixels seems about right for moderately
		// complex 8bpp images using DEFLATE compression, based on some casual
		// experimentation, as above that the uncompressed images get
		// significantly bigger than the compressed ones. However a full analysis
		// would also compare the time taken to read and write the data to file,
		// across a network.
		pComponent->compression = COMPRESSION_NONE;
	else if (pComponent->bitsPerSample == 1)
		// If the component is bi-level, always use Group 4 Fax compression,
		// as this is usually the most appropriate (fastest, best compression,
		// most standard), and some other schemes will only work badly,
		// if at all, with bi-level images.
		pComponent->compression = COMPRESSION_CCITTFAX4;
	else
		pComponent->compression = compression;
		


	// Write image, standard tags and directory.
	return Tiff::WriteComponent(pComponent);
}




AI_TiffComponent*
AI_Tiff::ReadComponent(int index, BOOL noData/*=FALSE*/)
{
	AI_TiffComponent *pAIComponent = new AI_TiffComponent;

	if (pAIComponent)
	{
		if (!ReadComponent(pAIComponent, index, noData))
		{
			delete pAIComponent;
			pAIComponent = NULL;
		}
	}

	return pAIComponent;
}

BOOL
AI_Tiff::ReadComponent(AI_TiffComponent *pAIComponent, int index, BOOL noData/*=FALSE*/)
{
	BOOL status = FALSE;

	if (pAIComponent)
	{
		// Set directory and read standard tags
		if (Tiff::ReadComponent(pAIComponent, index, noData))
		{
			// Read AI tag (if any)
			uint32 aiDataOffset;
			if (TIFFGetField(pTIFF, TIFFTAG_APPLIEDIMAGING, &aiDataOffset))
			{
				// Seek to file offset for AI data
				TIFFSeekFile(pTIFF, aiDataOffset, SEEK_SET);


				// Read number of AI directory entries (a 2 byte number)
				uint16 numEntries = 0;
				TIFFReadFile(pTIFF, &numEntries, sizeof(numEntries));


				toff_t currDirOffset  = aiDataOffset + 2;


				for (int i=0; i < numEntries; i++)
				{
					TIFFDirEntry entry;

					// Seek to next directory entry position
					TIFFSeekFile(pTIFF, currDirOffset, SEEK_SET);
					// Read entry
					TIFFReadFile(pTIFF, &entry, sizeof(entry));

					switch (entry.tdir_tag)
					{
						case AI_TAG_COLOUR:
							ReadColour(pAIComponent, entry);
						break;
/*
// Example code, do not delete!
						case AI_TAG_EXAMPLESTRING:
							ReadExampleString(pAIComponent, entry);
						break;
						case AI_TAG_EXAMPLELONG:
							ReadExampleLong(pAIComponent, entry);
						break;
						case AI_TAG_EXAMPLESTRUCTARRAY:
							ReadExampleStructArray(pAIComponent, entry);
						break;
*/
						case AI_TAG_IMAGEOPS:
							ReadImageOps(pAIComponent, entry);
						break;
						case AI_TAG_ANNOTATION:
							ReadAnnotation(pAIComponent, entry);
						break;
						case AI_TAG_ZSTACK:
							ReadZStack(pAIComponent, entry);
						break;
						case AI_TAG_ZSTACKINFO:
							ReadZStackInfo(pAIComponent, entry);
						break;
						case AI_TAG_STAGEINFO:
							ReadStageInfo(pAIComponent, entry);
						break;

						case AI_TAG_MIPMAPS:
							ReadMIPMaps(pAIComponent, entry, noData);
						break;

						default:
							assert(FALSE); // Should never get here
					}

					currDirOffset += sizeof(entry);
				}
			}

			pAIComponent->ProcessIntoCache();	// Once loaded, perform any operations in the component list

			status = TRUE;
		}
	}

	return status;
}



BOOL
AI_Tiff::ReadComponentData(AI_TiffComponent *pComponent)
{
	BOOL status = FALSE;

	if (pComponent && !pComponent->pData && pComponent->fileIndex > -1)
	{
		if (Tiff::ReadComponent(pComponent, pComponent->fileIndex))
		{
			pComponent->ProcessIntoCache();

			status = TRUE;
		}
	}

	return status;
}



BOOL
AI_Tiff::Assimilate(AI_Tiff *pOtherTiff)
{
// Move the components from another AI_Tiff object to this object
	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pCurrComponent = pComponents;


	if (!pCurrComponent) 
	// No components, just point to the other object's components (if any).
		pComponents = pOtherTiff->pComponents;
	else
	{
		// Get to tail of list
		while (pCurrComponent->pNext)
		{
			pCurrComponent = pCurrComponent->pNext;
		}	

		// Append components from other object (if any) to list.
		pCurrComponent->pNext = pOtherTiff->pComponents;
	}


	// Components no longer belong to other Tiff.
	pOtherTiff->pComponents = NULL;
	

	// Set up display buffer to accomodate the new components.
	// Actually no - Don't configure display buffer yet - it may not be needed!
	//ConfigureDisplayBuffer();

	LeaveCriticalSection(&criticalSection);

	return TRUE;
}



int
AI_Tiff::AddComponent(AI_TiffComponent *pComponent)
{
	int index = -1;

	if (pComponent)
	{
		EnterCriticalSection(&criticalSection);

		if (pComponents)
		{
			int i;		

			AI_TiffComponent *pCurrComponent = pComponents;
			for (i=1; pCurrComponent->pNext; i++)
				pCurrComponent = pCurrComponent->pNext;

			pCurrComponent->pNext = pComponent;
			index = i;
		}
		else
		{
			pComponents = pComponent;
			index = 0;
		}

		pComponent->ProcessIntoCache();

		// Don't configure display buffer yet - it may not be needed!
		//ConfigureDisplayBuffer();

		LeaveCriticalSection(&criticalSection);
	}

	return index;
}


int
AI_Tiff::Add24bppComponent(uint32 width, uint32 height, uint8 *pData/*=NULL*/)
{
// If pData is NULL, the component image data buffer will be allocated
// but not set.
	int index = -1;

	AI_TiffComponent *pComponent = new AI_TiffComponent;
	if (pComponent)
	{
//		pComponent->dataBytesPerPixel = 3;
		pComponent->samplesPerPixel   = 3;
		pComponent->bitsPerSample     = 8;
		pComponent->bpp = pComponent->samplesPerPixel * pComponent->bitsPerSample; // 24
		pComponent->width             = width;
		pComponent->height            = height;
		pComponent->planarConfiguration       = PLANARCONFIG_CONTIG;
		pComponent->photometricInterpretation = PHOTOMETRIC_RGB;

		pComponent->dataBytes = width * height * 3;
		pComponent->pData     = (uint8 *)_TIFFmalloc(pComponent->dataBytes);

		if (pData && pComponent->pData)
			_TIFFmemcpy(pComponent->pData, pData, pComponent->dataBytes);

		if (pComponent->dataBytes == 0 || pComponent->pData)
			index = AddComponent(pComponent);

		if (index < 0)
			delete pComponent;
	}

	return index;
}


int
AI_Tiff::Add8bppComponent(uint32 width, uint32 height,
	                      uint8 *pData/*=NULL*/, BOOL createMIPMaps/*=FALSE*/)
{
// If pData is NULL, the component image data buffer will be allocated
// but not set, and the value of createMIPMaps will be ignored.
	int index = -1;

	AI_TiffComponent *pComponent = new AI_TiffComponent;
	if (pComponent)
	{
//		pComponent->dataBytesPerPixel = 1;
		pComponent->samplesPerPixel   = 1;
		pComponent->bitsPerSample     = 8;
		pComponent->bpp = pComponent->samplesPerPixel * pComponent->bitsPerSample; // 8
		pComponent->width             = width;
		pComponent->height            = height;
		pComponent->planarConfiguration       = PLANARCONFIG_CONTIG;
		pComponent->photometricInterpretation = PHOTOMETRIC_MINISBLACK;

		pComponent->dataBytes = width * height;
		pComponent->pData = (uint8*)_TIFFmalloc(pComponent->dataBytes);

		if (pData && pComponent->pData)
		{
			_TIFFmemcpy(pComponent->pData, pData, pComponent->dataBytes);

			if (createMIPMaps)
				pComponent->CreateMIPMaps();
		}

		if (pComponent->dataBytes == 0 || pComponent->pData)
			index = AddComponent(pComponent);

		if (index < 0)
			delete pComponent;
	}

	return index;
}


int
AI_Tiff::Add1bppComponent(uint32 width, uint32 height,
	                      uint8 *pData/*=NULL*/, BOOL createMIPMaps/*=FALSE*/)
{
// If pData is NULL, the component image data buffer will be allocated
// but not set and the value of createMIPMaps will be ignored.
	int index = -1;

	AI_TiffComponent *pComponent = new AI_TiffComponent;
	if (pComponent)
	{
//		pComponent->dataBytesPerPixel = 0;	// An internal frig really, but dataBytesPerPixel is used everywhere
		pComponent->samplesPerPixel   = 1;
		pComponent->bitsPerSample     = 1;
		pComponent->bpp = pComponent->samplesPerPixel * pComponent->bitsPerSample; // 1
		pComponent->width             = width;
		pComponent->height            = height;
		pComponent->planarConfiguration       = PLANARCONFIG_CONTIG;
		pComponent->photometricInterpretation = PHOTOMETRIC_MINISBLACK;

		// Round up to nearest byte
		pComponent->dataBytes = height * pComponent->WidthBytes();
		pComponent->pData     = (uint8 *)_TIFFmalloc(pComponent->dataBytes);

		if (pData && pComponent->pData)
		{
			_TIFFmemcpy(pComponent->pData, pData, pComponent->dataBytes);

			if (createMIPMaps)
				pComponent->CreateMIPMaps();
		}

		if (pComponent->dataBytes == 0 || pComponent->pData)
			index = AddComponent(pComponent);

		if (index < 0)
			delete pComponent;
	}

	return index;
}



//////////////////////////////////////////////////////////////////////////////


AI_TiffComponent*
AI_Tiff::GetComponent(int i)
{
	AI_TiffComponent *pComponent = NULL;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pCurrComponent = pComponents;
	int index = 0;

	while (pCurrComponent)
	{
		if (index == i)
		{
			pComponent = pCurrComponent;
			break;
		}

		pCurrComponent = pCurrComponent->pNext;
		index++;
	}

	LeaveCriticalSection(&criticalSection);

	return pComponent;
}


BOOL
AI_Tiff::RemoveComponent(int index)
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);

	if (pComponent)
	{
		if (index == 0)
		{
			pComponents = pComponent->pNext;
			status = TRUE;
		}
		else
		{
			AI_TiffComponent *pPrevComponent = GetComponent(index - 1);

			if (pPrevComponent)
			{
				pPrevComponent->pNext = pComponent->pNext;
				status = TRUE;
			}
		}

		if (status)
		{
			delete pComponent;
			//ConfigureDisplayBuffer();
		}
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}


int
AI_Tiff::GetComponentIndexByDescription(char *description)
{
	int index = -1;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pCurrComponent = pComponents;
	int i = 0;

	while (pCurrComponent && description)
	{
		if (   pCurrComponent->description
		    && stricmp(pCurrComponent->description, description) == 0)
		{
			index = i;
			break;
		}

		pCurrComponent = pCurrComponent->pNext;
		i++;
	}

	LeaveCriticalSection(&criticalSection);

	return index;
}



BOOL
AI_Tiff::SetComponentVisible(int i, BOOL yesNo)
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(i);

	if (pComponent)
	{
		// Only set renderRequired flag if the visibility state is changing,
		// as otherwise a render is unneccessary and should be avoided,
		// as it can be relatively slow.
		if (pComponent->visible != yesNo)
			renderRequired = TRUE;

		pComponent->visible = yesNo;

		status = TRUE;
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}

BOOL
AI_Tiff::IsComponentVisible(int i)
{
	BOOL visible = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(i);

	if (pComponent && pComponent->visible)
		visible = TRUE;

	LeaveCriticalSection(&criticalSection);

	return visible;
}


BOOL 
AI_Tiff::GetComponentDescription(int index, char *pDesc, int maxLen)
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);			

	if (pComponent && pDesc && maxLen > 0)
	{
		if (pComponent->description)
			strncpy(pDesc, pComponent->description, maxLen);
		else
			pDesc[0] = '\0';

		status = TRUE;
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}

BOOL
AI_Tiff::SetComponentDescription(int index, char *pDesc)
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);			

	if (pComponent)
		status = pComponent->SetDescription(pDesc);

	LeaveCriticalSection(&criticalSection);

	return status;
}


BOOL
AI_Tiff::GetComponentColour(int index, rgbTriple *pColour)
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);			

	if (pComponent && pColour)
	{
		*pColour = pComponent->colour;
		status = TRUE;
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}

BOOL
AI_Tiff::SetComponentColour(int index, rgbTriple colour)
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);			

	if (pComponent)
	{
		// Only set renderRequired flag if the colour is changing,
		// as otherwise a render is unneccessary and should be avoided,
		// as it can be relatively slow.
		if (   pComponent->colour.r != colour.r
		    || pComponent->colour.g != colour.g
		    || pComponent->colour.b != colour.b)
			renderRequired = TRUE;

		pComponent->colour = colour;

		status = TRUE;
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}


int
AI_Tiff::GetComponentWidth(int index)
{
	int width = -1;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);			

	if (pComponent)
		width = (int)pComponent->width;

	LeaveCriticalSection(&criticalSection);

	return width;
}

int
AI_Tiff::GetComponentHeight(int index)
{
	int height = -1;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);

	if (pComponent)
		height = (int)pComponent->height;

	LeaveCriticalSection(&criticalSection);

	return height;
}


//int
//AI_Tiff::GetComponentBytesPerPixel(int index)
//{
//	int bpp = -1;
//	AI_TiffComponent *pComponent = GetComponent(index);

//	if (pComponent)	bpp = pComponent->dataBytesPerPixel;

//	return bpp;
//}
int
AI_Tiff::GetComponentBPP(int index)
{
	int bpp = -1;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);

	if (pComponent)
		bpp = pComponent->bpp;

	LeaveCriticalSection(&criticalSection);

	return bpp;
}


uint8*
AI_Tiff::GetComponentBytes(AI_TiffComponent *pComponent)
{
	uint8 *pComponentData = NULL;

	EnterCriticalSection(&criticalSection);

	if (pComponent)
	{
		if (pComponent->pData)
			pComponentData = pComponent->pData;
		else
		{
			// Try to load data from file.
			if (LoadComponentData(pComponent) && pComponent->pData)
				pComponentData = pComponent->pData;
		}
	}

	LeaveCriticalSection(&criticalSection);

	return pComponentData;
}

const uint8*
AI_Tiff::GetComponentBytes(int index)
{
	uint8 *pData = NULL;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);

	if (pComponent)
		pData = GetComponentBytes(pComponent);

	LeaveCriticalSection(&criticalSection);

	return pData;
}

BOOL
AI_Tiff::GetComponentBytes(int index, uint8 *pData)
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);

	if (pComponent)
	{
		int size = pComponent->height * pComponent->WidthBytes();//(((pComponent->width  * pComponent->bitsPerSample) + 7) >> 3);
		uint8 *pComponentData = GetComponentBytes(pComponent);
		if (pComponentData)
		{
			memcpy(pData, pComponentData, size);
			status = TRUE;
		}
	}

	LeaveCriticalSection(&criticalSection);

	return status;	
}



//////////////////////////////////////////////////////////////////////////////



BOOL
AI_Tiff::WriteColour(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset)
{
	BOOL status = FALSE;

	if (!pComponent) return status;

	// Pack the RGB values into a LONG
	uint32 colourLong = (pComponent->colour.r | pComponent->colour.g << 8) | pComponent->colour.b << 16;


	// Create directory entry for colour.
	pEntry->tdir_tag    = AI_TAG_COLOUR;
	pEntry->tdir_type   = TIFF_LONG;
	pEntry->tdir_count  = 1; // Count is number of items of 'type'
	pEntry->tdir_offset = colourLong; // 'tdir_offset' is used to store the value itself

	status = TRUE;

	return status;
}

BOOL
AI_Tiff::ReadColour(AI_TiffComponent *pComponent, TIFFDirEntry entry)
{
	if (!pComponent) return FALSE;

	uint32 colourLong  = entry.tdir_offset;

	pComponent->colour.r = (uint8)( colourLong        & 0xFF);
	pComponent->colour.g = (uint8)((colourLong >> 8)  & 0xFF);
	pComponent->colour.b = (uint8)((colourLong >> 16) & 0xFF);

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------------------------
//	Image processing operations block implementation
//-----------------------------------------------------------------------------------------------
BOOL AI_Tiff::WriteImageOps(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset)
{
	BOOL status = FALSE;

	if (!pComponent || !(pComponent->m_iops_list)) return status;

	// Linked list into block of memory
	// First calculate how much memory we need to malloc
	int size = 0;
	ImageOp *next = pComponent->m_iops_list;
	while (next)
	{
		size+= sizeof(IOpsHeader);
		size+= next->header.size;
		next = next->next;
	}

	if (!size)	// Nothing to write, shouldn't happen
		return status;

	BYTE *block = new BYTE[size];
	assert(block);

	// Now write list to block, unions are the size of the largest element so we only need what we need
	next = pComponent->m_iops_list;
	BYTE *p = block;
	while (next)
	{
		int bsize = sizeof(IOpsHeader) + next->header.size;
		memcpy(p, next, bsize);
		p += bsize;
		next = next->next;
	}

	// Create directory entry for raw data 
	pEntry->tdir_tag    = AI_TAG_IMAGEOPS;
	pEntry->tdir_type   = TIFF_UNDEFINED;
	// Count for undefined type is number of bytes
	pEntry->tdir_count  = size;
	pEntry->tdir_offset = *pDataOffset;

	// Write data
	TIFFWriteFile(pTIFF, block, pEntry->tdir_count);
	// Update offset for next data
	*pDataOffset += pEntry->tdir_count;

	status = TRUE;

	delete[] block;	// don't need this anymore

	return status;
}

BOOL AI_Tiff::ReadImageOps(AI_TiffComponent *pComponent, TIFFDirEntry entry)
{
	BOOL status = FALSE;

	if (!pComponent) 
		return status;

	int size = entry.tdir_count;

	if (!size)
		return status;

	BYTE * block = new BYTE[size];
	assert (block);

	// Seek to data position
	TIFFSeekFile(pTIFF, entry.tdir_offset, SEEK_SET);
	// Read data
	if (TIFFReadFile(pTIFF, block, entry.tdir_count) == (tsize_t)entry.tdir_count)
		status = TRUE;

	// Parse block into linked list
	ImageOp * next = pComponent->m_iops_list = NULL;

	BYTE *p = block;
	int OpSize = 0;			// Size of indevidual operation in tiff

	while (size > 0)
	{
		ImageOp *Op = new ImageOp;
		assert (Op);

		// Read header
		memcpy(Op, p, sizeof(IOpsHeader));
		// now the whole lot
		assert (size - OpSize >= 0);

		OpSize = sizeof(IOpsHeader) + Op->header.size;
		memcpy(Op, p, OpSize);

		// Attach to component list
		if (!pComponent->m_iops_list)
		{
			pComponent->m_iops_list = Op;
			next = Op;
			next->next = NULL;
		}
		else
		{
			next->next = Op;
			next = next->next;
			next->next = NULL;
		}
		p+=OpSize;	// Move block on to next
		size-=OpSize;
	}

	// Clean up block							//SN22Sep03
	delete[] block;

	return status;
}
//-----------------------------------------------------------------------------------------------
//	Image processing operations block implementation End
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
//	Annotation block implementation
//-----------------------------------------------------------------------------------------------
BOOL AI_Tiff::WriteAnnotation(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset)
{
	BOOL status = FALSE;

	if (!pComponent || !(pComponent->m_pAnnotation)) return status;

	// Create directory entry for raw data 
	pEntry->tdir_tag    = AI_TAG_ANNOTATION;
	pEntry->tdir_type   = TIFF_UNDEFINED;
	// Count for undefined type is number of bytes
	pEntry->tdir_count  = pComponent->m_annotationSize;
	pEntry->tdir_offset = *pDataOffset;

	// Write data
	TIFFWriteFile(pTIFF, pComponent->m_pAnnotation, pEntry->tdir_count);
	// Update offset for next data
	*pDataOffset += pEntry->tdir_count;

	status = TRUE;

	return status;
}

BOOL AI_Tiff::ReadAnnotation(AI_TiffComponent *pComponent, TIFFDirEntry entry)
{
	BOOL status = FALSE;

	if (!pComponent) return FALSE;

	pComponent->m_annotationSize = entry.tdir_count;
	pComponent->m_pAnnotation = malloc(pComponent->m_annotationSize);

	// Seek to data position
	TIFFSeekFile(pTIFF, entry.tdir_offset, SEEK_SET);
	// Read data
	if (TIFFReadFile(pTIFF, pComponent->m_pAnnotation, entry.tdir_count) == (tsize_t)entry.tdir_count)
		status = TRUE;

	return status;
}

BOOL AI_Tiff::GetComponentAnnotationSize(int index, int *pSize)
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);			

	if (pComponent && pSize)
	{
		*pSize = pComponent->m_annotationSize;
		status = TRUE;
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}

BOOL AI_Tiff::GetComponentAnnotation(int index, void *pBuf)
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);			

	if (pComponent && pComponent->m_pAnnotation)
	{
		memcpy(pBuf, pComponent->m_pAnnotation, pComponent->m_annotationSize);
		status = TRUE;
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}

BOOL AI_Tiff::SetComponentAnnotation(int index, char *pBuf, int size)
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);			

	if (pComponent && pBuf)
	{
		if (pComponent->m_pAnnotation) free(pComponent->m_pAnnotation);
		pComponent->m_pAnnotation = malloc(size);

		if (pComponent->m_pAnnotation)
		{
			pComponent->m_annotationSize = size;
			memcpy(pComponent->m_pAnnotation, pBuf, size);
			status = TRUE;		
		}
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}
//-----------------------------------------------------------------------------------------------
//	Annotation block implementation
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
//	Zstack block implementation
//-----------------------------------------------------------------------------------------------
BOOL AI_Tiff::WriteZStack(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset)
{
	BOOL status = FALSE;

	if (!pComponent || !(pComponent->m_pZstack)) return status;

	// Create directory entry for raw data 
	pEntry->tdir_tag    = AI_TAG_ZSTACK;
	pEntry->tdir_type   = TIFF_UNDEFINED;
	// Count for undefined type is number of bytes
	pEntry->tdir_count  = pComponent->m_zstackSize;
	pEntry->tdir_offset = *pDataOffset;

	// Write data
	TIFFWriteFile(pTIFF, pComponent->m_pZstack, pEntry->tdir_count);
	// Update offset for next data
	*pDataOffset += pEntry->tdir_count;

	status = TRUE;

	return status;
}

BOOL AI_Tiff::ReadZStack(AI_TiffComponent *pComponent, TIFFDirEntry entry)
{
	BOOL status = FALSE;

	if (!pComponent) return FALSE;

	pComponent->m_zstackSize = entry.tdir_count;
	pComponent->m_pZstack = malloc(pComponent->m_zstackSize);

	// Seek to data position
	TIFFSeekFile(pTIFF, entry.tdir_offset, SEEK_SET);
	// Read data
	if (TIFFReadFile(pTIFF, pComponent->m_pZstack, entry.tdir_count) == (tsize_t)entry.tdir_count)
		status = TRUE;

	return status;
}

BOOL AI_Tiff::GetComponentZStack(int index, AI_Tiff * pTif, int *spacing, int *objsize)
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);
	
	//'Can't allocate new AI_TIFF here as can't delete it outside of this class for some reason
	// Whys that jago??

	if (pComponent && pComponent->m_pZstack && pTif)
	{
		if (pTif->LoadFileBuffer((const char*)pComponent->m_pZstack, pComponent->m_zstackSize))
		{
			*spacing = pComponent->m_zstackinfo.m_spacing;
			*objsize = pComponent->m_zstackinfo.m_objsize;
			status = TRUE;
		}
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}

BOOL AI_Tiff::SetComponentZStack(int index, AI_Tiff *pTif, int spacing, int objsize)
{
	unsigned int size;
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);			

	if (pComponent && pTif)
	{
		if (pComponent->m_pZstack) 
			free(pComponent->m_pZstack);
		if ((pComponent->m_pZstack = pTif->SaveFileBuffer(&size)))
		{
			pComponent->m_zstackSize = size;
			pComponent->m_zstackinfo.m_iszstack = 1;
			pComponent->m_zstackinfo.m_objsize = objsize;
			pComponent->m_zstackinfo.m_spacing = spacing;
			status = TRUE;
		}
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}
//-----------------------------------------------------------------------------------------------
//	ZStack block implementation
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
//	ZstackInfo block implementation
//-----------------------------------------------------------------------------------------------
BOOL AI_Tiff::WriteZStackInfo(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset)
{
	BOOL status = FALSE;

	if (!pComponent) return status;

	// Create directory entry for raw data 
	pEntry->tdir_tag    = AI_TAG_ZSTACKINFO;
	pEntry->tdir_type   = TIFF_UNDEFINED;
	// Count for undefined type is number of bytes
	pEntry->tdir_count  = sizeof(ZStackInfo);
	pEntry->tdir_offset = *pDataOffset;

	// Write data
	TIFFWriteFile(pTIFF, &pComponent->m_zstackinfo, pEntry->tdir_count);
	// Update offset for next data
	*pDataOffset += pEntry->tdir_count;

	status = TRUE;

	return status;
}

BOOL AI_Tiff::ReadZStackInfo(AI_TiffComponent *pComponent, TIFFDirEntry entry)
{
	BOOL status = FALSE;

	if (!pComponent) return FALSE;

	// Seek to data position
	TIFFSeekFile(pTIFF, entry.tdir_offset, SEEK_SET);
	// Read data
	if (TIFFReadFile(pTIFF, (void *)&pComponent->m_zstackinfo, entry.tdir_count) == (tsize_t)entry.tdir_count)
		status = TRUE;

	return status;
}
//-----------------------------------------------------------------------------------------------
//	ZStackInfo block implementation
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
//	StageInfo block implementation
//-----------------------------------------------------------------------------------------------
BOOL AI_Tiff::WriteStageInfo(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset)
{
	BOOL status = FALSE;

	if (!pComponent) return status;

	// Create directory entry for raw data 
	pEntry->tdir_tag    = AI_TAG_STAGEINFO;
	pEntry->tdir_type   = TIFF_UNDEFINED;
	// Count for undefined type is number of bytes
	pEntry->tdir_count  = sizeof(StageInfo);
	pEntry->tdir_offset = *pDataOffset;

	// Write data
	TIFFWriteFile(pTIFF, &pComponent->m_stageinfo, pEntry->tdir_count);
	// Update offset for next data
	*pDataOffset += pEntry->tdir_count;

	status = TRUE;

	return status;
}

BOOL AI_Tiff::ReadStageInfo(AI_TiffComponent *pComponent, TIFFDirEntry entry)
{
	BOOL status = FALSE;

	if (!pComponent) return FALSE;

	// Seek to data position
	TIFFSeekFile(pTIFF, entry.tdir_offset, SEEK_SET);
	// Read data
	if (TIFFReadFile(pTIFF, (void *)&pComponent->m_stageinfo, entry.tdir_count) == (tsize_t)entry.tdir_count)
		status = TRUE;

	return status;
}

BOOL AI_Tiff::GetComponentStageInfo(int index, int *frame_no, float *ix, float *iy, float *iz, float *scalex, float *scaley)
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);			

	if (pComponent)
	{
		*frame_no = pComponent->m_stageinfo.frame_no;
		*ix = pComponent->m_stageinfo.ideal_x;
		*iy = pComponent->m_stageinfo.ideal_y;
		*iz = pComponent->m_stageinfo.ideal_z;
		*scalex = pComponent->m_stageinfo.pixelscale_x;
		*scaley = pComponent->m_stageinfo.pixelscale_y;

		status = TRUE;
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}

BOOL AI_Tiff::SetComponentStageInfo(int index, int frame_no, float ix, float iy, float iz, float scalex, float scaley)
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pComponent = GetComponent(index);			

	if (pComponent)
	{
		pComponent->m_stageinfo.frame_no = frame_no;
		pComponent->m_stageinfo.ideal_x = ix;
		pComponent->m_stageinfo.ideal_y = iy;
		pComponent->m_stageinfo.ideal_z = iz;
		pComponent->m_stageinfo.pixelscale_x = scalex;
		pComponent->m_stageinfo.pixelscale_y = scaley;

		status = TRUE;
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}

//-----------------------------------------------------------------------------------------------
// MIP maps
//-----------------------------------------------------------------------------------------------


BOOL AI_Tiff::WriteMIPMaps(AI_TiffComponent *pComponent, TIFFDirEntry *pEntry, toff_t *pDataOffset,
                           uint16 compression/*=COMPRESSION_DEFLATE*/)
{
	BOOL status = FALSE;

	if (!pComponent) return status;


	size_t size = 0;
	char *pMIPtifBuf = NULL;

	if (pComponent->pMIPMaps)
	{
		// Copy compression parameters from parent
		pComponent->pMIPMaps->jpegQuality    = jpegQuality;
		pComponent->pMIPMaps->deflateQuality = deflateQuality;
		pComponent->pMIPMaps->jpeg2000Rate   = jpeg2000Rate; 

		// Serialise AI_Tiff holding MIP maps to a block of memory.
		// Use same compression scheme as parent.
		pMIPtifBuf = pComponent->pMIPMaps->SaveFileBuffer(&size, compression);
	}


	// Create directory entry for raw data 
	pEntry->tdir_tag    = AI_TAG_MIPMAPS;
	pEntry->tdir_type   = TIFF_UNDEFINED;
	// Count for undefined type is number of bytes
	pEntry->tdir_count  = size;
	pEntry->tdir_offset = *pDataOffset;


	// Write the block of memory to the file
	TIFFWriteFile(pTIFF, pMIPtifBuf, size);

	// Free the block of memory
	FreeFileBuffer(pMIPtifBuf);


	// Update offset for next data
	*pDataOffset += pEntry->tdir_count;

	status = TRUE;

	return status;
}

BOOL AI_Tiff::ReadMIPMaps(AI_TiffComponent *pComponent, TIFFDirEntry entry, BOOL noData/*=FALSE*/)
{
	BOOL status = FALSE;

	if (!pComponent) return FALSE;

	if (pComponent->pMIPMaps)
	{
		delete pComponent->pMIPMaps;
		pComponent->pMIPMaps = NULL;
	}

	if (entry.tdir_count > 0)
	{
		pComponent->pMIPMaps = new AI_Tiff;

		if (pComponent->pMIPMaps)
		{
			// Seek to data position
			TIFFSeekFile(pTIFF, entry.tdir_offset, SEEK_SET);


			// If the noData is TRUE, the actual image data for the MIPMaps
			// will not be loaded: it can then be loaded if and when required,
			// using LoadComponentData().
			pComponent->pMIPMaps->LoadSubfile(this, entry.tdir_offset, entry.tdir_count, noData);

/*
			// Read data into temporary buffer.
			char *pMIPtifBuf = (char*)malloc(entry.tdir_count);
			if (pMIPtifBuf)
			{
				if (TIFFReadFile(pTIFF, (void *)pMIPtifBuf, entry.tdir_count) == (tsize_t)entry.tdir_count)
					status = TRUE;

				pComponent->pMIPMaps->LoadFileBuffer(pMIPtifBuf, entry.tdir_count);

////{FILE *pF=fopen("E:\\jpgmip.tif","wb"); if (pF){ fwrite(pMIPtifBuf,entry.tdir_count,1,pF); fclose(pF); }}
				// Release temporary buffer.
				free(pMIPtifBuf);
			}
*/
		}
	}

	return status;
}


/////////////////////////////////////////////////////////////////////////////


int AI_Tiff::GetIdealMIPMapLOD(double scale)
{
	int lod = 0; // Default: full size image.
	// What LOD MIP map is appropriate for the scale?
	// 'Level Of Detail', used with MIP maps: LOD 0 is the full-size image,
	// the LOD 1 image has dimensions half those of the full size, etc.
	// Note that if the scale does not match a MIP map exactly, this function
	// always returns the smaller of the possible LODs (i.e. the MIP map
	// with larger dimensions). This is good as it means that no detail is lost.
	if (scale < 1.0 && scale > 0.0)
	{
		double invScale = 1.0 / scale;
		int invMipScale = 1;

		// Make sure invMipScale doesn't overflow by limiting the maximum value of lod.
		while ((invMipScale <<= 1) <= invScale && lod < 30)
		{
			lod++;
		}
	}

	return lod;
}

int AI_Tiff::GetAvailableMIPMapLOD(int idealLOD)
{
// Find the nearest to ideal LOD available in one compoenent, then check that
// LOD is available for all components. If so, return the commonly available LOD.
// If not, return LOD 0.
	// Default to full-size image if all components do not have same LOD available.
	int availableLOD = 0;

	EnterCriticalSection(&criticalSection);

	if (idealLOD > 0 && pComponents)
	{
		int firstAvailableLOD = pComponents->GetAvailableMIPMapLOD(idealLOD);
		BOOL sameAvailableInAllComponents = TRUE;

		AI_TiffComponent *pCurrComponent = pComponents->pNext;
		while (pCurrComponent)
		{
			if (pCurrComponent->GetAvailableMIPMapLOD(idealLOD) != firstAvailableLOD)
			{
				sameAvailableInAllComponents = FALSE;
				break;
			}

			pCurrComponent = pCurrComponent->pNext;
		}

		if (sameAvailableInAllComponents)
			availableLOD = firstAvailableLOD;
	}

	LeaveCriticalSection(&criticalSection);

	return availableLOD;
}

int AI_Tiff::GetMIPMapScaleDenominator(int lod)
{
	int denom = 1;
	while (lod-- > 0) denom <<= 1;
	return denom;
}


void AI_Tiff::CreateMIPMapsForAllComponents()
{
// This will delete any existing MIP maps then generate MIP maps for
// all components from the full-size image data. Note that this will
// only work if the full size image data has been loaded (it will not
// be if the noData parameter was TRUE when this image was loaded).

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pCurrComponent = pComponents;
	while (pCurrComponent)
	{
		pCurrComponent->CreateMIPMaps();

		pCurrComponent = pCurrComponent->pNext;
	}

	LeaveCriticalSection(&criticalSection);
}

BOOL AI_Tiff::CreateMissingMIPMaps() 
{ 
// This function will create MIP maps for any components that don't have 
// them, from the full-size image data. Note that this will only work 
// if the full size image data has been loaded (it will not be if the 
// noData parameter was TRUE when this image was loaded). 
	BOOL wereMIPmapsCreated = FALSE;

	EnterCriticalSection(&criticalSection);

	AI_TiffComponent *pCurrComponent = pComponents; 
	while (pCurrComponent) 
	{ 
		if (!pCurrComponent->pMIPMaps)
		{
			// make sure we got image data, before attempting to create the mipmaps
			if (!pCurrComponent->pData)
				LoadComponentData(pCurrComponent);
			
			if (pCurrComponent->pData && pCurrComponent->CreateMIPMaps()) 
				wereMIPmapsCreated = TRUE; 
		}

		pCurrComponent = pCurrComponent->pNext; 
	}

	LeaveCriticalSection(&criticalSection);

	return wereMIPmapsCreated; 
} 



/*
void AITiffScale::SetFromDouble(double s, RoundingDirection round*=roundNearest*)
{
	if (s < 1.0)
	{
		// Scales of less than 1 can only be of the form 1/x, where x is an integer.
		// Find the value of x giving the closest match to the floating point value.
		double invS = 1.0 / s;
		double prevX = 1.0;
		for (double x = min(2.0, floor(invS)); x < 10000.0; x+=1.0)
		{
			if (invS < x)
			{
				// The last increment of x made it larger than invS, so now
				// decide if the current or the previous value of x should be
				// used to create the scale. Note that rounding up selects a
				// smaller value of x, for a larger value of 1/x.
				if (   round == roundUp
				    || (round == roundNearest && (invS - prevX) <= (x - invS)))
					x = prevX;

				break;
			}

			prevX = x;
		}

		// Add 0.5 before truncating to integer so result is rounded.
		SetFractional((unsigned short)(x + 0.5));
	}
	else
	{
		if (round == roundDown)
			SetInteger((unsigned short)s);
		else if (round == roundUp)
			SetInteger((unsigned short)ceil(s));
		else // roundNearest
			// Add 0.5 before truncating to integer so result is rounded.
			SetInteger((unsigned short)(s + 0.5));
	}
}
*/

// Returns the number of components in the AITiff image
int AI_Tiff::CountComponents(void)
{
	AI_TiffComponent *c;
	int num_components = 0;

	EnterCriticalSection(&criticalSection);

	for (c = pComponents; c; c = c->pNext)
		num_components++;

	LeaveCriticalSection(&criticalSection);

	return num_components;
}



BOOL AI_Tiff::LoadNonTIFFile(wchar_t *filename)
{
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
    // Init GDI+
    if (filename != NULL && Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL) == Gdiplus::Ok)
    {
        // Load in the bitmap from file
        Gdiplus::Bitmap *pBitmap = Gdiplus::Bitmap::FromFile(filename, TRUE);	
		pBitmap->RotateFlip(Gdiplus::RotateNoneFlipXY);
        // Loaded it o.k.
        if (!pBitmap) return FALSE;

		HBITMAP hbm;
		int ok = pBitmap->GetHBITMAP(NULL, &hbm);

		long cx = pBitmap->GetWidth();
		long cy = pBitmap->GetHeight();
		long bpp = 32;

		long size = cx * cy * 4; 
		BYTE *buf = new BYTE[size];
		long nbytes = GetBitmapBits(hbm, size, buf);

		// Okay now BGR->RGB and change from 32 to 24 bpp
		BYTE *in, *out;
		BYTE tmp;
		in = out = buf;
		for (int i = 0; i < cy; i++) 
		{
			for (int j = 0; j < cx; j++)
			{
				tmp = *in;
				*out = *(in+2);
				*(out+1) = *(in+1);
				*(out+2) = tmp;
				in += bpp / 8; //in += bmp.bmBitsPixel/8;
				out += 3;
			}
		}
		Add24bppComponent(cx, cy, buf);
		delete buf;

		Gdiplus::GdiplusShutdown(gdiplusToken);

		return TRUE;
	}

	return FALSE;
}
