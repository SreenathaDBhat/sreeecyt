
/*
 * tiff.cpp
 * JMB April 2001
 *
 * Mods:
 *
 * 4Oct01	MC Added 'if (remainingrows > 0)' test for WriteEncodedStrip()
 *			   in WriteComponent() Problem was that a 128 x 128 image
 *			   fragment from the spot counting script was getting an
 *			   extra strip added - of 0 length. This caused a WD o/p,
 *			   worse could not subsequently open image in MDS2 Reviev grid view
 *
 */


#include "AItiff.h"

#include <limits.h> // For ULONG_MAX



Tiff::Tiff()
{
	pTIFF = NULL;
	jpegQuality    = 75;  // Default value set by libtiff, in tif_jpeg.c, is 75.
	deflateQuality = -1;  // -1 tells the library to use its default value, see zlib.h.
	jpeg2000Rate   = 1.0; // Default value set by JasPer library is 1.0, i.e. lossless.
}


BOOL
Tiff::OpenFile(const char fileName[], const char mode[])
{
	BOOL status = FALSE;

    pTIFF = TIFFOpen(fileName, mode);

    if (pTIFF) status = TRUE;

	return status;
}


BOOL
Tiff::CloseFile()
{
	TIFFClose(pTIFF);

	return TRUE;
}


int
Tiff::CountFileComponents()
{
	int numComponents = 0;

    if (pTIFF)
	{
		tdir_t i = 0;

		while (TIFFSetDirectory(pTIFF, i++))
		{
			numComponents++;
		}
	}    

	return numComponents;
}



BOOL
Tiff::ReadComponent(TiffComponent *pComponent, int index, BOOL noData/*=FALSE*/)
{
// Load data into supplied component
	BOOL status = FALSE;


    if (pTIFF && pComponent)
	{
		if (TIFFSetDirectory(pTIFF, index)) // Set and read directory
		{
			size_t npixels;

			TIFFGetField(pTIFF, TIFFTAG_IMAGEWIDTH,      &(pComponent->width));
			TIFFGetField(pTIFF, TIFFTAG_IMAGELENGTH,     &(pComponent->height));

			TIFFGetField(pTIFF, TIFFTAG_PHOTOMETRIC,     &(pComponent->photometricInterpretation));
			TIFFGetField(pTIFF, TIFFTAG_PLANARCONFIG,    &(pComponent->planarConfiguration));

			TIFFGetField(pTIFF, TIFFTAG_SAMPLESPERPIXEL, &(pComponent->samplesPerPixel));
			TIFFGetField(pTIFF, TIFFTAG_BITSPERSAMPLE,   &(pComponent->bitsPerSample));


		TIFFGetField(pTIFF, TIFFTAG_COMPRESSION,     &(pComponent->compression));

//short or;TIFFGetField(pTIFF, TIFFTAG_ORIENTATION, &or);


			char *pString;
			// This returns a temporary pointer, valid until the next directory is read.
			if (TIFFGetField(pTIFF, TIFFTAG_IMAGEDESCRIPTION, &pString))
				pComponent->SetDescription(pString);



			// Calculate number of pixels in image
			npixels = pComponent->width * pComponent->height;



			if (   pComponent->bitsPerSample   == 8
			    && pComponent->samplesPerPixel == 1
			    && pComponent->planarConfiguration == PLANARCONFIG_CONTIG
			    && (   pComponent->photometricInterpretation == PHOTOMETRIC_MINISWHITE
			        || pComponent->photometricInterpretation == PHOTOMETRIC_MINISBLACK) )
			{
				// Read 8bpp data into 8bpp buffer
				pComponent->bpp = 8;

				if (noData)
				{
					// Don't read in the actual pixel data itself.
					pComponent->dataBytes = 0;
					status = TRUE;
				}
				else
				{
					tstrip_t numStrips = TIFFNumberOfStrips(pTIFF);
					tsize_t  stripSize = TIFFStripSize(pTIFF);

					pComponent->dataBytes = stripSize * numStrips;
					pComponent->pData     = (uint8*)_TIFFmalloc(pComponent->dataBytes + 1024/*For luck*/);

					if (pComponent->pData != NULL)
					{
						uint8 *pStripData = pComponent->pData;
						tstrip_t strip;
						for (strip = 0; strip < numStrips; strip++)
						{
							TIFFReadEncodedStrip(pTIFF, strip, pStripData, (tsize_t)-1);

							pStripData += stripSize;
						}

						status = TRUE;
					}
					else pComponent->dataBytes = 0;
				}
			}
			else
			if (   pComponent->bitsPerSample   == 1
			    && pComponent->samplesPerPixel == 1
			    && pComponent->planarConfiguration == PLANARCONFIG_CONTIG
			    && (   pComponent->photometricInterpretation == PHOTOMETRIC_MINISWHITE
			        || pComponent->photometricInterpretation == PHOTOMETRIC_MINISBLACK) )
			{
				// Read 1pp data into 1bpp buffer
				pComponent->bpp = 1;

				if (noData)
				{
					// Don't read in the actual pixel data itself.
					pComponent->dataBytes = 0;
					status = TRUE;
				}
				else
				{
					tstrip_t numStrips = TIFFNumberOfStrips(pTIFF);
					tsize_t  stripSize = TIFFStripSize(pTIFF);

					pComponent->dataBytes = stripSize * numStrips;
					pComponent->pData = (uint8*)_TIFFmalloc(pComponent->dataBytes + 1024/*For luck*/);

					if (pComponent->pData != NULL)
					{
						uint8 *pStripData = pComponent->pData;
						tstrip_t strip;
						for (strip = 0; strip < numStrips; strip++)
						{
							TIFFReadEncodedStrip(pTIFF, strip, pStripData, (tsize_t)-1);

							pStripData += stripSize;
						}

						status = TRUE;
					}
					else pComponent->dataBytes = 0;
				}
			}
			else
			{
			// For all other data formats, use the TIFFReadRGBAImage function
			// to handle it. Will get 32bpp data in the buffer in this case (RGBA),
			// however many bpp the data in the file is.
				//pComponent->dataBytesPerPixel = 4;
				pComponent->bpp = 32;

				if (noData)
				{
					// Don't read in the actual pixel data itself.
					pComponent->dataBytes = 0;
					status = TRUE;
				}
				else
				{
					pComponent->dataBytes = npixels * 4;
					pComponent->pData     = (uint8*)_TIFFmalloc(pComponent->dataBytes + 1024/*For luck*/);

					if (pComponent->pData != NULL)
					{
						if (TIFFReadRGBAImage(pTIFF,
						                      pComponent->width, pComponent->height,
					                      (uint32*)pComponent->pData, 0))
						{
							// TIFFReadRGBAImage always returns an image whose origin
							// is in the bottom left corner, so must invert it.
							uint8 *pLineBuf = (uint8*)_TIFFmalloc(pComponent->width * 4 + 64/*For luck*/);

							uint32 lineBytes = pComponent->width * 4;
							uint8 *pTop = pComponent->pData; // Initialise to first scanline
							uint8 *pBot = pComponent->pData + lineBytes * (pComponent->height - 1); // Initialise to last scanline

							while (pBot > pTop)
							{
								_TIFFmemcpy(pLineBuf, pTop,     lineBytes);
								_TIFFmemcpy(pTop,     pBot,     lineBytes);
								_TIFFmemcpy(pBot,     pLineBuf, lineBytes);

								pTop += lineBytes;
								pBot -= lineBytes;
							}


							_TIFFfree(pLineBuf);
		
							status = TRUE;
						}
					}
					else pComponent->dataBytes = 0;
				}
			}

			pComponent->fileIndex = index;
		}
	}    

	return status;
}



TiffComponent*
Tiff::ReadComponent(int index)
{
// Load and return Component number 'index' (from zero)
	TiffComponent *pComponent = new TiffComponent;

	if (pComponent)
	{
		if (!ReadComponent(pComponent, index))
		{
			delete pComponent;
			pComponent = NULL;
		}
	}

	return pComponent;
}


BOOL
Tiff::WriteComponent(TiffComponent *pComponent)
{
	BOOL status = FALSE;


	if (pTIFF && pComponent)
	{
		TIFFSetField(pTIFF, TIFFTAG_IMAGEWIDTH,      pComponent->width);
	    TIFFSetField(pTIFF, TIFFTAG_IMAGELENGTH,     pComponent->height);

		TIFFSetField(pTIFF, TIFFTAG_PHOTOMETRIC,     pComponent->photometricInterpretation);
	    TIFFSetField(pTIFF, TIFFTAG_PLANARCONFIG,    pComponent->planarConfiguration);

		//TIFFSetField(pTIFF, TIFFTAG_SAMPLESPERPIXEL, pComponent->samplesPerPixel);
	    //TIFFSetField(pTIFF, TIFFTAG_BITSPERSAMPLE,   pComponent->bitsPerSample);



		switch (pComponent->bpp)
		{
			case 1:
			{
				// Save as 1bpp, contiguous, black is zero.
				TIFFSetField(pTIFF, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
				TIFFSetField(pTIFF, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
				TIFFSetField(pTIFF, TIFFTAG_SAMPLESPERPIXEL, 1);
				TIFFSetField(pTIFF, TIFFTAG_BITSPERSAMPLE,   1);
			} break;

			case 8:
			{
				// Save as 8bpp, contiguous, black is zero.
				TIFFSetField(pTIFF, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
				TIFFSetField(pTIFF, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
				TIFFSetField(pTIFF, TIFFTAG_SAMPLESPERPIXEL, 1);
				TIFFSetField(pTIFF, TIFFTAG_BITSPERSAMPLE,   8);
			} break;

			case 24:
			{
				// Save as RGB, 3 samples per pixel, 8 bits per sample,
				// contiguous.
				TIFFSetField(pTIFF, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
				TIFFSetField(pTIFF, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
				TIFFSetField(pTIFF, TIFFTAG_SAMPLESPERPIXEL, 3);
				TIFFSetField(pTIFF, TIFFTAG_BITSPERSAMPLE,   8);
			} break;

			case 32:
			default:
				// Save as RGB, 4 samples per pixel, 8 bits per sample,
				// contiguous (assume this is RGBA format data created by
				// TIFFReadRGBAImage().
				TIFFSetField(pTIFF, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
				TIFFSetField(pTIFF, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
				TIFFSetField(pTIFF, TIFFTAG_SAMPLESPERPIXEL, 4);
				TIFFSetField(pTIFF, TIFFTAG_BITSPERSAMPLE,   8);
		}




		// Only allow specified compresion schemes for saving (some of them
		// don't work for saving anyway).
		switch (pComponent->compression)
		{
			case COMPRESSION_NONE:      // dump mode (1)
			case COMPRESSION_CCITTRLE:  // CCITT modified Huffman RLE, 1bpp only (2)
			case COMPRESSION_CCITTFAX3: // CCITT Group 3 fax encoding, 1bpp only (3)
			case COMPRESSION_CCITTFAX4: // CCITT Group 4 fax encoding, 1bpp only (4)
			case COMPRESSION_JPEG:      // JPEG compression, technical note 2 (7)
			case COMPRESSION_PACKBITS:  // Macintosh RLE (32773)
			case COMPRESSION_DEFLATE:   // Deflate compression (ZIP) (32946)
			case COMPRESSION_JPEG2000:
				break; // Leave as is
/*
COMPRESSION_CCITTRLEW	32771	// #1 w/ word alignment
// compression code 32947 is reserved for Oceana Matrix <dev@oceana.com>
COMPRESSION_DCS             32947   // Kodak DCS encoding
COMPRESSION_JBIG		34661	// ISO JBIG
*/

			// Don't use the following schemes, including ones not listed here:
			case COMPRESSION_LZW:       // Lempel-Ziv & Welch (patent issues) (5)
			default: 
				pComponent->compression = COMPRESSION_NONE;
		}

		TIFFSetField(pTIFF, TIFFTAG_COMPRESSION, pComponent->compression);


	    switch (pComponent->compression) 
		{
			case COMPRESSION_JPEG:
				TIFFSetField(pTIFF, TIFFTAG_JPEGQUALITY, jpegQuality);
			break;

			case COMPRESSION_DEFLATE:
				TIFFSetField(pTIFF, TIFFTAG_ZIPQUALITY, deflateQuality);
			break;

			case COMPRESSION_JPEG2000:
				TIFFSetField(pTIFF, TIFFTAG_JPCRATE, jpeg2000Rate);
			break;
		/*
			case COMPRESSION_LZW:
			case COMPRESSION_DEFLATE:
				if (predictor != 0)
					TIFFSetField(tif, TIFFTAG_PREDICTOR, predictor);
		    break;
		*/
	    }


		if (pComponent->description)
			TIFFSetField(pTIFF, TIFFTAG_IMAGEDESCRIPTION, pComponent->description);


// Always write data with an orientation of 1 (origin in top-left).
// This is the only orientation supported by the baseline TIFF spec anyway.
//TIFFSetField(pTIFF, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);


		// Get an appropriate number of rows per strip to use.
		uint32 rowsPerStrip = TIFFDefaultStripSize(pTIFF, 0);
		//// Request an 'ideal' number, but some codecs may return a different number.
		//uint32 rowsPerStrip = TIFFDefaultStripSize(pTIFF, pComponent->GuessIdealStripHeight());

		// Set number of rows per strip
		TIFFSetField(pTIFF, TIFFTAG_ROWSPERSTRIP, rowsPerStrip);

		// Calculate number of bytes in a strip, based on other appropriate fields having been set correctly above.
		tsize_t stripSize = TIFFStripSize(pTIFF);

		// Calculate number of bytes per row
//	WH	tsize_t rowBytes = pComponent->width * pComponent->dataBytesPerPixel;
		tsize_t rowBytes = pComponent->WidthBytes();

		// Number of whole strips in image.
		tstrip_t numWholeStrips = pComponent->height / rowsPerStrip;

		// Write strips
		tdata_t pData = pComponent->pData;
		if (pData)
		{
			for (tstrip_t strip = 0; ; strip++)
			{
				if (numWholeStrips > strip)
				{
					if (TIFFWriteEncodedStrip(pTIFF, strip, pData, stripSize) < 0)
						break;
				}
				else
				{	// Last strip is a partial strip. Number of rows in it is
					// remainder after dividing image height by rowsPerStrip.
					tsize_t remainingRows = pComponent->height % rowsPerStrip;

					if (remainingRows > 0) // MC 4/Oct
						TIFFWriteEncodedStrip(pTIFF, strip, pData,
					                          remainingRows * rowBytes);
				    break;
				}

				pData = (uint8*)pData + stripSize;
			}
		}


		// Write directory to file and setup to create the next directory
		status = TIFFWriteDirectory(pTIFF);
	}

	return status;
}



BOOL Tiff::SetJPEGQuality(int quality)
{
// Setting to be used when writing components with COMPRESSION_JPEG.
	BOOL status = FALSE;

	// 0 is worst quality, 100 is best quality, however 100 is still lossy.
	// The value is not a percentage and is not linear.
	// According to the Independent JPEG Group FAQ (www.ijg.org), the
	// recommended value is 75, as this is the lowest setting that doesn't
	// give defects in a typical image. It is the default value set by libtiff,
	// in tif_jpeg.c. The FAQ also says not to use a value higher than 95, as a
	// much larger file will be produced without a noticable increase in quality.
	if (0 <= quality && quality <= 100)
	{
		jpegQuality = quality;
		status = TRUE;
	}

	return status;
}

BOOL Tiff::SetDEFLATEQuality(int quality)
{
// Setting to be used when writing components with COMPRESSION_DEFLATE.
	BOOL status = FALSE;

	// -1 tells the zlib library to use its default value (currently 6),
	// 1 gives best speed, 9 gives best compression, 0 gives no compression at all.
	if (-1 <= quality && quality <= 9)
	{
		deflateQuality = quality;
		status = TRUE;
	}

	return status;
}

BOOL Tiff::SetJPEG2000Rate(double rate)
{
// Setting to be used when writing components with COMPRESSION_JPEG2000.
	BOOL status = FALSE;

	if (rate >= 0.0)
	{
		jpeg2000Rate = rate;
		status = TRUE;
	}

	return status;
}


//////////////////////////////////////////////////////////////////////////////



TiffComponent::TiffComponent()
{ 
	// Initialise to defaults specified in TIFF spec,
	// where possible and appropriate.
	description  = NULL;
	copyright    = NULL;
	software     = NULL;
	hostComputer = NULL;
	dateTime[0]  = '\0';
	make         = NULL;
	model        = NULL;

	pColourmap = NULL;

	photometricInterpretation = 0; 
	compression               = COMPRESSION_NONE;
	planarConfiguration       = PLANARCONFIG_CONTIG;

	samplesPerPixel = 1;
	bitsPerSample   = 1;
	pExtraSamples   = NULL;

	width = height = 0;

	//dataBytesPerPixel = 0;
	// Initialise to invalid value.
	bpp = 0;;
	pData     = NULL;
	dataBytes = 0;
	fileIndex = -1;
	pNext = NULL; 
}

TiffComponent::~TiffComponent()
{
	if (description) _TIFFfree(description);
	if (pData)       _TIFFfree(pData);
}


BOOL
TiffComponent::SetDescription(const char *pString)
{
	BOOL status = FALSE;

	if (pString)
	{
		if (description) _TIFFfree(description);
		description = (char*)_TIFFmalloc(strlen(pString) + 1);
		strcpy(description, pString);
		status = TRUE;
	}

	return status;
}

/*
Have commented this out as it (surprisingly) doesn't seem to offer any performance gains.
uint32
TiffComponent::GuessIdealStripHeight()
{
// The 'ideal' size is assumed to be large - certainly larger than the 8KB
// default used by libtiff and recommended in the TIFF spec. Larger is assumed
// to be faster, particularly as each strip is usually compressed independently.
// However, with increasing size come diminishing returns, and overheads
// associated with allocating large contiguous blocks of memory. So an upper
// limit is set.
	const int maxStripSize = 1048576; // 1MB
	int idealStripHeight = height; // Default - one strip for entire image

	// Assume that the size of the image when saved is the same as its size in
	// memory. This may not be the case, but it is usually a good estimate.
	// Note this does not use this->bitsPerSample or this->samplesPerPixel, as
	// these are only valid for components that have been read from file.
	int imageDataSize = WidthBytes() * height;

	// If estimated image size is larger than the maximum strip size, try to
	// split the image into equal sized strips that are as big as possible but
	// smaller than the maximum.
	if (imageDataSize > maxStripSize)
		idealStripHeight = (height / ((imageDataSize / maxStripSize) + 1)) + 1;

	return idealStripHeight;
}
*/
