#include "stdafx.h"
#include "aitiff.h"

#define JAS_WIN_MSVC_BUILD
#include <jasper\jasper.h>

int aitiff_load(const char*filename, BOOL bOverlays)
{
	int ret = 0;

	AI_Tiff * tiff = new AI_Tiff();

	if (tiff)
	{
		if (tiff->LoadFile(filename))
		{
			// switch off/on any overlays
			int index = 0;
			while (tiff->GetComponent(index))
			{
				if (tiff->GetComponentBPP(index) == 1)
					tiff->SetComponentVisible(index, bOverlays); 
				index++;
			}
			tiff->Render();
			ret = (int)tiff;
		}
		else
			delete tiff;
	}
	return ret;
}

BOOL aitiff_size(int handle, int *w, int*h, int*stride)
{
	if (handle)
	{
		AI_Tiff *tiff = (AI_Tiff*)handle;

		*w = (long)tiff->GetRenderBufferWidth();
		*h = (long)tiff->GetRenderBufferHeight();
		*stride = (long)tiff->GetRenderBufferWidthBytes();
		return TRUE;
	}
	return FALSE;
}

BOOL aitiff_getbuffer(int handle, BYTE * buffer, int size)
{
	if (handle)
	{
		AI_Tiff *tiff = (AI_Tiff*)handle;

		// Bitmap requires same padding, so use renderbuffer
		BYTE *image = (BYTE*)tiff->GetRenderBuffer();
		memcpy(buffer, image, size);
		return TRUE;
//		return tiff->Get24bppBuffer(buffer);
	}
	return FALSE;
}

void aitiff_close(int handle)
{
	if (handle)
	{
		AI_Tiff *tiff = (AI_Tiff*)handle;
		delete tiff;
	}
}

int save_raw24_to_jp2(const char *path, BYTE * buffer, int width, int height, int stride, float rate)
{
	jas_init();

	int status = 0; // Boolean
	static const char module[] = "JPCEncode";

	// Create a JasPer 'image'
	jas_image_t *pImage = jas_image_create0();
	if (pImage)
	{
		tsize_t bytesperline = stride;
		jas_matrix_t *pMatrix;

		jas_image_setclrspc(pImage, JAS_CLRSPC_SRGB);

		// Allocate a matrix for intermediate storage of component pixel data
		// for this strip or tile.
		pMatrix = jas_matrix_create(height, width);

		if (pMatrix)
		{
			int iCmp;
			jas_stream_t *pStream;

	// The number of JPC components depends on the data. Each sample in a
	// component is represented by an integer of at least 16 bits. For
	// greyscale there is usually one component, for RGB there are usually 3.
	// The number of components usually correspnds with the TIFF component's
	// 'SamplesPerPixel' value (remember that 'component' has a different
	// meaning in TIFF and JPEG2000).
			for (iCmp = 0; iCmp < 3; iCmp++)
			{
				jas_image_cmptparm_t cmptparm;
				cmptparm.tlx = 0;
				cmptparm.tly = 0;
				cmptparm.hstep = 1;
				cmptparm.vstep = 1;
				cmptparm.width  = width;
				cmptparm.height = height;
				cmptparm.prec = 8;
				cmptparm.sgnd = false;

				jas_image_addcmpt(pImage, iCmp, &cmptparm);
				
				// Copy TIFF data for this component into matrix.
				if (cmptparm.prec < 8)
				{
				}
				else if (cmptparm.prec == 8)
				{
					int rowIndex, colIndex;
					BYTE *pRow = buffer;
//					for (rowIndex = 0; rowIndex < height; rowIndex++)
					for (rowIndex = height -1; rowIndex >= 0; rowIndex--)
					{
						for (colIndex = width -1; colIndex >= 0; colIndex--)
						{
							int index = (colIndex * 3) + (2 - iCmp);
							jas_matrix_set(pMatrix, rowIndex, colIndex, pRow[index]);
						}
						pRow += bytesperline; 
					}
				}
				else if (cmptparm.prec <= 16)
				{
				}


				// Copy the matrix data into the component
				jas_image_writecmpt(pImage, iCmp, 0, 0, width, height, pMatrix);
			}


//			pStream = jas_stream_memopen(0, 0);
			pStream = jas_stream_fopen(path, "w+b");

			if (pStream)
			{
				// set plane colour
				jas_image_setcmpttype(pImage, 0,	JAS_IMAGE_CT_COLOR(JAS_CLRSPC_CHANIND_RGB_R));
				jas_image_setcmpttype(pImage, 1,	JAS_IMAGE_CT_COLOR(JAS_CLRSPC_CHANIND_RGB_G));
				jas_image_setcmpttype(pImage, 2,	JAS_IMAGE_CT_COLOR(JAS_CLRSPC_CHANIND_RGB_B));

				char comp[56];
				sprintf(comp, "rate=%f", rate);
//				jpc_encode(pImage, pStream, "rate=0.01");
				jp2_encode(pImage, pStream, comp);
//				int format =  jas_image_strtofmt("jp2");
//				jas_image_encode(pImage, pStream, format, "rate=0.01");

				// The encoded data is finally copied to the libtiff's 'raw data
				// buffer', from where libtiff will write it to file. If the size
				// of the data is bigger than the size of the raw buffer, then the
				// raw buffer must be flushed every time it fills up.
				//assert(tif->tif_rawcc == 0); // Am assuming there is no unflushed data in the buffer.
				//assert(tif->tif_rawcp == tif->tif_rawdata); // Not sure if this is important but would be happier if it is true.

//				assert((tif->tif_rawcp - tif->tif_rawdata) == tif->tif_rawcc);
				jas_stream_close(pStream);

				status = 1;
			}
			jas_matrix_destroy(pMatrix);
		}

		// okay not sure if we're doing this right

		jas_image_destroy(pImage);
	}
	return status;
}

