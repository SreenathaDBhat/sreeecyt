
/*
 * rendertiff.cpp
 * JMB April 2001
 *
 *	19Sep2003	JMB	The possibility that only some components contain MIP maps
 *					is now handled correctly, using AI_Tiff::GetAvailableMIPMapLOD().
 *	11Sep2003	JMB	Modifed relevant functions to use arbitary floating point
 *					scales rather than AITiffScale.
 *	29Aug2003	JMB	Added ComponentDataLoadedForDraw().
 *	22Aug2003	JMB	Added DrawStretched().
 *	20Aug2003	JMB	MIP maps are now individually loaded on demand.
 *	24Jul2003	JMB	For efficiency, GDI+ is now not started and stopped every
 *					time it is used. It is started if used, then shut down in
 *					the destructor. However this still means GDI+ is opened
 *					once for each AI_Tiff object, so the most efficient thing
 *					is to start it once for the whole program, so that AI_Tiff
 *					doesn't need to start it at all - use the parameter to the
 *					constructor to indicate this.
 *	11Jun2003	JMB	Used GDI+ in AI_Tiff::Draw() to do interpolation.
 *	18Jan2002	JMB	Re-wrote AI_Tiff::Draw(). Also, ConfigureDisplayBuffer() and
 *					Render() are now called automatically as required.
 *	01May2001	WH:	Render now uses the AI_TiffComponent m_DisplayBuf to extract image data
 *					during render, as this may point to cached image processed data.
 *	24Apr2001	WH:	Fix offset into src buffer in Draw, also set interpolation mode
 *					for zoom down.
 */


#include "AItiff.h"

#include <math.h>
#include <gdiplus.h>


///#define STRETCH_RENDER // Stretch contrast.


// RANGE forces a to be in the range b..c (inclusive)
#define RANGE(a,b,c) ( ((a) < (b)) ? (b): ( ((a) > (c)) ? (c) : (a) ) )


/*
 * The direction of an RGB colour vector represents a colour, and its magnetude
 * represents brightness.
 *
 * RGB colour vectors and RGB triples:
 * The direction of an RGB colour vector is given by the components of its
 * corresponding RGB triple. However, the magnetude of the vector is not that 
 * of the sum of the squares of the components of the triple. The magnetude
 * represents the brightness of the colour, and must be in the range 0 to 255.
 * The square root of the sum of the squares of the components may give
 * values larger than 255 if more than one component is non-zero.
 *
 * Mixing colour vectors:
 * Multiple colours are mixed by combining their colour vectors. The direction
 * of the combined vector is that of the vector sum of the vectors.
 * Note that more than two vectors cannot be accurately combined by first
 * combining two, then combining the combined vector with the next vector, and
 * so on. This will work as an approximation, but the direction of the final 
 * vector will be slightly incorrect.
 */
static
rgbTriple
AddColourVector(rgbTriple *pRGBsum, rgbTriple *pRGBnew, int count)
{
	rgbTriple vectorSum;//, RGBcombined;

	// Calculate vector sum to get direction of combined colours.
	// Divide colour values by count so that the finished sum, of count vectors
	// for each pixel, has each component fitting in the range 0 to 255.
	vectorSum.r = pRGBsum->r + pRGBnew->r / count;
	vectorSum.g = pRGBsum->g + pRGBnew->g / count;
	vectorSum.b = pRGBsum->b + pRGBnew->b / count;

	return vectorSum;
}


BOOL
AI_Tiff::ConfigureRenderBuffer(int idealLOD/*=0*/)
{
// (Re)Allocate render buffer large enough for all components.
	BOOL status = FALSE;

	uint32 oldBufWidth  = bufWidth;
	uint32 oldBufHeight = bufHeight;

	// MIP maps will be used if available for all components
	int lod = GetAvailableMIPMapLOD(idealLOD);

	AI_TiffComponent *pCurrComponent = pComponents;

	bufWidth = bufHeight = 0;
	while (pCurrComponent)
	{
		AI_TiffComponent *pMIPComponent = pCurrComponent->GetMIPMap(lod);

		if (pMIPComponent)
		{
			// Update maximum image dimensions for screen buffer
			if (pMIPComponent->width  > bufWidth)  bufWidth  = pMIPComponent->width;
			if (pMIPComponent->height > bufHeight) bufHeight = pMIPComponent->height;
		}

		pCurrComponent = pCurrComponent->pNext;
	}


	if (bufWidth != oldBufWidth || bufHeight != oldBufHeight)
	{
		if (bufWidth < 1 || bufHeight < 1)
		{
			FreeRenderBuffer();
			status = TRUE;
		}
		else
		{
			// Allocate a 24bpp buffer. As this will be used to store the data
			// for a DIB, space must be allocated to pad each row up to a
			// LONG (4 byte) boundary.
			// Each pixel is 3 bytes (24 bit), so without padding the
			// number of bytes in a row for bufWidth pixels will be:
			bufWidthBytes = bufWidth * 3;

			// If this number of bytes is not exactly divisible by 4,
			// pad it upto a 4 byte boundary.
			int remainder = bufWidthBytes % 4;
			if (remainder > 0) bufWidthBytes = bufWidthBytes - remainder + 4;

			uint32 requiredBufSize = bufWidthBytes * bufHeight;


			// Only re-allocate the buffer if the existing one isn't large enough.
			if (requiredBufSize > bufBytes)
			{
				if (pBuf) free(pBuf);
				pBuf = (uint8*)calloc(requiredBufSize + 1024/* For luck */, sizeof(uint8));
				bufBytes = requiredBufSize;
			}

			if (pBuf)
				status = TRUE;
			else
				FreeRenderBuffer(); // Set parameters to zero
		}
	}	


	return status;
}


void
AI_Tiff::FreeRenderBuffer()
{
	bufWidth = bufWidthBytes = bufHeight = 0;

	if (pBuf) free(pBuf);

	pBuf = NULL;

	bufBytes = 0;
}


BOOL AI_Tiff::ComponentDataLoadedForDraw(BOOL query, double scale)
{
// If query is TRUE, this returns TRUE if all the pixel data required by Render()
// at this scale has previously been loaded.
// If query is FALSE, this ensures that all the pixel data required by Render()
// at this scale is loaded.
// Note that Render() will always ensure that the data it needs is loaded, but
// this function allows drawing to be skipped, or data to be pre-loaded, for
// images that were loaded with LoadFile()'s noData flag set to TRUE.
// Obviously it is essential that this function uses the same logic as Render()
// for picking MIP maps.
	BOOL status = TRUE;

	EnterCriticalSection(AITiffGetCriticalSection());

	// Find out which MIP map level is appropriate for the current scale
	int idealLOD = GetIdealMIPMapLOD(scale);
	// Use MIP maps if available for all components
	int availableLOD = GetAvailableMIPMapLOD(idealLOD);

	AI_TiffComponent *pCurrComponent = pComponents;

	while (pCurrComponent)
	{
		if (pCurrComponent->visible)
		{
			AI_TiffComponent *pMIPComponent = pCurrComponent->GetMIPMap(availableLOD);

			if (pMIPComponent)
			{
				// Components are initially loaded without any pixel data to reduce
				// time and memory usage. The data is then loaded on demand if and
				// when it is needed.
				// If not in query mode and the component doesn't have any
				// image data loaded, load it now.
				if (!query && !pMIPComponent->pData)
				{
					if (availableLOD != 0)
						pCurrComponent->pMIPMaps->LoadComponentData(pMIPComponent);
					else
						LoadComponentData(pMIPComponent);
				}

				if (!pMIPComponent->pData)
				{
				// If querying: at least one component has not had its data
				// loaded - don't bother checking any more
				// If loading: the load failed - this shouldn't happen so quit now.
					status = FALSE;
					break;
				}
			}
		}

		pCurrComponent = pCurrComponent->pNext;
	}

	LeaveCriticalSection(AITiffGetCriticalSection());

	return status;
}

BOOL AI_Tiff::IsComponentDataLoadedForDraw(double scale)
{
	return ComponentDataLoadedForDraw(TRUE, scale);
}
BOOL AI_Tiff::EnsureComponentDataLoadedForDraw(double scale)
{
	return ComponentDataLoadedForDraw(FALSE, scale);
}

void
AI_Tiff::Render(int idealLOD/*=0*/)
{
// Render components to render buffer

	EnterCriticalSection(AITiffGetCriticalSection());

	// First (re)allocate the render buffer if required. This should do nothing
	// if nothing has changed, but only call it here because it is a waste of
	// memory to allocate it if it is not used.
	ConfigureRenderBuffer(idealLOD); 


	if (pBuf)
	{
	// Create composite image in buffer from components, remembering that the
	// components may have smaller dimensions than the buffer.

		// Use MIP maps if available for all components. The same LOD must be
		// used for all components.
		actualRenderedLOD = GetAvailableMIPMapLOD(idealLOD);

		// Clear the buffer
		memset(pBuf, 0, bufBytes);

#ifdef STRETCH_RENDER
		// Maximum value of a colour component from a tiff component image.
		int maxComponentValue = 0;
		// Maximum value of a colour component from the combined image.
		// Initialise to 1 to avoid divide-by-zero.
		int maxCombinedValue = 1;
#endif
		AI_TiffComponent *pCurrComponent = pComponents;

		// Count visible components.
		int numVisibleComponents = 0;
		while (pCurrComponent)
		{
			if (pCurrComponent->visible)// && pCurrComponent->pData)
				numVisibleComponents++;
			pCurrComponent = pCurrComponent->pNext;
		}

		// Count of components rendered.
		int renderCount = 0;

		pCurrComponent = pComponents;
		while (pCurrComponent)
		{
			// Skip 1bpp components here, these are added last.
			if (pCurrComponent->visible && pCurrComponent->bpp != 1)
			{
				AI_TiffComponent *pMIPComponent = pCurrComponent->GetMIPMap(actualRenderedLOD);

				// If the component doesn't have any image data loaded, load it now.
				// Components are initially loaded without any pixel data to reduce
				// time and memory usage. The data is then loaded on demand if and
				// when it is needed.
				if (pMIPComponent && !pMIPComponent->pData)
				{
					if (actualRenderedLOD != 0)
						pCurrComponent->pMIPMaps->LoadComponentData(pMIPComponent);
					else
						LoadComponentData(pMIPComponent);
				}

				if (pMIPComponent && pMIPComponent->pData)
				{
					uint8 *pBufRow = pBuf;
					const uint8 *pC;
					if (pMIPComponent == pCurrComponent)
					// Always use the m_displayBuf, this may actually point
					// to pData if no operations need to be performed on it.
						pC = pCurrComponent->m_displayBuf;
					else
						pC = pMIPComponent->pData;

					for (uint32 y=0; y < pMIPComponent->height; y++)
					{	
						uint8 *pB = pBufRow;

						for (uint32 x=0; x < pMIPComponent->width; x++)
						{
							rgbTriple bufTriple, dataTriple, combinedTriple;

							// Read current RGB values from buffer.
							// Buf stores RGB data in reverse order.
							bufTriple.b = *pB;
							bufTriple.g = *(pB + 1);
							bufTriple.r = *(pB + 2);

							// Read RGB values from current component
							if (pCurrComponent->bpp == 8)
							{
							// Component has single 8bpp value per pixel. This is taken as the
							// intensity of the component colour for this pixel.
							//	dataTriple.r = dataTriple.g = dataTriple.b = *pC++;
							// Calculate colour values for each pixel value 'x',
							// such that e.g. red value = (x/255) * colour.r
								dataTriple.r = (uint8)((pCurrComponent->colour.r * (uint32)(*pC)) >> 8);
								dataTriple.g = (uint8)((pCurrComponent->colour.g * (uint32)(*pC)) >> 8);
								dataTriple.b = (uint8)((pCurrComponent->colour.b * (uint32)(*pC)) >> 8);
								pC++;
							}
							else
							{
							// Treat as RGBA data returned by TIFFReadRGBAImage
								dataTriple.r = *pC++;
								dataTriple.g = *pC++;
								dataTriple.b = *pC++;
								pC++; // Skip alpha value.
							}

							// Combine RGB data from component with that in buffer
							if (colourMixFast)
							{
								combinedTriple.r = bufTriple.r + dataTriple.r;
								combinedTriple.g = bufTriple.g + dataTriple.g;
								combinedTriple.b = bufTriple.b + dataTriple.b;

								// If a value has wrapped around, max it out.
								if (combinedTriple.r < bufTriple.r) combinedTriple.r = 255;
								if (combinedTriple.g < bufTriple.g) combinedTriple.g = 255;
								if (combinedTriple.b < bufTriple.b) combinedTriple.b = 255;
							}
							else
							{
								// Combine colour values in 3D RGB space rather than
								// by simple addition of components.
								combinedTriple = AddColourVector(&bufTriple, &dataTriple, numVisibleComponents);
#ifdef STRETCH_RENDER
								if (dataTriple.r > maxComponentValue) maxComponentValue = dataTriple.r;
								if (dataTriple.g > maxComponentValue) maxComponentValue = dataTriple.g;
								if (dataTriple.b > maxComponentValue) maxComponentValue = dataTriple.b;
#endif
							}

							// Write combined data back to buffer.
							// Buf stores RGB data in reverse order
							*pB++ = combinedTriple.b;
							*pB++ = combinedTriple.g;
							*pB++ = combinedTriple.r;

#ifdef STRETCH_RENDER
							if (!colourMixFast && renderCount == numVisibleComponents - 1)
							{
								// Adding last component
								if (*(pB - 3) > maxCombinedValue) maxCombinedValue = *(pB - 3);
								if (*(pB - 2) > maxCombinedValue) maxCombinedValue = *(pB - 2);
								if (*(pB - 1) > maxCombinedValue) maxCombinedValue = *(pB - 1);
							}
#endif
						}

						pBufRow += bufWidthBytes;
					}

					renderCount++;
				}
			}

			pCurrComponent = pCurrComponent->pNext;
		}


#ifdef STRETCH_RENDER
		if (!colourMixFast)
		{
			uint8 *pB = pBuf;

			for (uint32 i=0; i < bufWidthBytes * bufHeight; i++)
			{	
				int value = ((int)(*pB) * maxComponentValue) / maxCombinedValue;

if (value > 200)
	i = i;

				*pB = RANGE(value, 0, 255);
				pB++;
			}
		}
#endif

		//
		// Now that 8 and 24-bit components have been rendered, render 1bpp components on top
		//
/*
		// First set all pixels that are under a visible 1bpp image to zero
		pCurrComponent = pComponents;
		while (pCurrComponent)
		{
			if (pCurrComponent->bpp == 1 && pCurrComponent->visible)
			{
				AI_TiffComponent *pMIPComponent = pCurrComponent->GetMIPMap(actualRenderedLOD);

				if (pMIPComponent && !pMIPComponent->pData)
				{
					if (actualRenderedLOD != 0)
						pCurrComponent->pMIPMaps->LoadComponentData(pMIPComponent);
					else
						LoadComponentData(pMIPComponent);
				}

				if (pMIPComponent && pMIPComponent->pData)
				{
					uint8 *pBufRow = pBuf;
					const uint8 *pC;
					if (pMIPComponent == pCurrComponent) pC = pCurrComponent->m_displayBuf;
					else                                 pC = pMIPComponent->pData;

					for (uint32 y=0; y < pMIPComponent->height; y++)
					{	
						uint8 *pB = pBufRow;
						int bitIndex = 0;

						for (uint32 x=0; x < pMIPComponent->width; x++)
						{
							// If the bit for this coordinate is set in the component,
							// erase the corresonding pixel in the RGB image
							if ((*pC << bitIndex++) & 0x80)
							{
								*pB++ = (uint8)0; *pB++ = (uint8)0;	*pB++ = (uint8)0;
							}
							else 
								pB += 3;

							if (bitIndex > 7)
							{
								bitIndex = 0;
								pC++;
							}
						}

						if (bitIndex != 0) pC++;

						pBufRow += bufWidthBytes;
					}
				}
			}

			pCurrComponent = pCurrComponent->pNext;
		}
*/

		// Now draw in the 1bpp images, /* blending their colours where they overlap */
		pCurrComponent = pComponents;
		while (pCurrComponent)
		{
			if (pCurrComponent->bpp == 1 && pCurrComponent->visible)
			{
				AI_TiffComponent *pMIPComponent = pCurrComponent->GetMIPMap(actualRenderedLOD);

				if (pMIPComponent && !pMIPComponent->pData)
				{
					if (actualRenderedLOD != 0)
						pCurrComponent->pMIPMaps->LoadComponentData(pMIPComponent);
					else
						LoadComponentData(pMIPComponent);
				}

				if (pMIPComponent && pMIPComponent->pData)
				{
					uint8 *pBufRow = pBuf;
					const uint8 *pC;
					if (pMIPComponent == pCurrComponent)
					// Always use the m_displayBuf, this may actually point
					// to pData if no operations need to be performed on it.
						pC = pCurrComponent->m_displayBuf;
					else
						pC = pMIPComponent->pData;

					for (uint32 y=0; y < pMIPComponent->height; y++)
					{	
						uint8 *pB = pBufRow;
						int bitIndex = 0;

						for (uint32 x=0; x < pMIPComponent->width; x++)
						{
							// If the bit for this coordinate is set in the component,
							// add the corresponding pixel in the RGB image to the component colour.
							if ((*pC << bitIndex++) & 0x80)
							{
							/*
								rgbTriple combinedTriple;

								// Note buf stores RGB data in reverse order.
								combinedTriple.b = (uint8)pCurrComponent->colour.b + *(pB);
								combinedTriple.g = (uint8)pCurrComponent->colour.g + *(pB + 1);
								combinedTriple.r = (uint8)pCurrComponent->colour.r + *(pB + 2);

								// If a value has wrapped around, max it out.
								if (combinedTriple.b < *(pB))     combinedTriple.b = 255;
								if (combinedTriple.g < *(pB + 1)) combinedTriple.g = 255;
								if (combinedTriple.r < *(pB + 2)) combinedTriple.r = 255;

								// Write combined data back to buffer.
								*pB++ = combinedTriple.b;
								*pB++ = combinedTriple.g;
								*pB++ = combinedTriple.r;
							*/
								// Note buf stores RGB data in reverse order.
								*pB++ = (uint8)pCurrComponent->colour.b;
								*pB++ = (uint8)pCurrComponent->colour.g;
								*pB++ = (uint8)pCurrComponent->colour.r;
							}
							else
								pB += 3;

							if (bitIndex > 7)
							{
								bitIndex = 0;
								pC++;
							}
						}

						if (bitIndex != 0) pC++;

						pBufRow += bufWidthBytes;
					}
				}
			}

			pCurrComponent = pCurrComponent->pNext;
		}


		renderRequired = FALSE;
	}

	LeaveCriticalSection(AITiffGetCriticalSection());
}


void
ConvertBitmapForCompressibility(Gdiplus::Bitmap *pBitmap, int compressibilityFactor,
                                const RECT *pSrcRect, const RECT *pDstRect)
{
// Reduce the amount of information in the given bitmap, so that viewing it
// using remote desktop software over a slow connection takes less time.
// Copy to a small (zoomed down) bitmap, then zoom back up to the original bitmap.
// Unfortunately, if there is more than one byte per pixel and the remote control
// software uses a simple byte-oriented run-length encoding compression scheme,
// this won't help much, as a number of adjacent pixels of the same colour will
// not produce a number of adjacent bytes of the same value (will be an
// alternating pattern instead).
// Other ideas: Superimpose edge detected mask? Smooth to reduce variation?
// Sharpen to enhance important features?
// Reduce colour depth? - Maybe not, as remote desktop can do that for you.
// However could do a could count reduction: e.g. do a colour histogram, pick
// the 'N' most frequent colours then replace all other colours with the nearest
// match from the N.
	if (compressibilityFactor < 2) return;

	// Use the compressibility factor as a pixel size.
	// Due to the possible use of MIP-maps, at scales of less than 1 it is
	// not possible to determine how many bitmap pixels would be mapped to
	// each screen pixel just by using the scale. So use the dimensions of
	// the source (bitmap) and destination (screen) rectangles to determine
	// this instead.
	int displayedPixelWidth = 1;
	if (pSrcRect->right > pSrcRect->left)
		displayedPixelWidth =   (pDstRect->right - pDstRect->left)
	                          / (pSrcRect->right - pSrcRect->left);
	if (displayedPixelWidth < 1) displayedPixelWidth = 1;


	// Return if pixel size is already large enough due to scaling.
	if (displayedPixelWidth >= compressibilityFactor) return;



	int reducedWidth  = (pBitmap->GetWidth()  * displayedPixelWidth) / compressibilityFactor;
	int reducedHeight = (pBitmap->GetHeight() * displayedPixelWidth) / compressibilityFactor;

	Gdiplus::Bitmap *pReducedBitmap = new Gdiplus::Bitmap(reducedWidth, reducedHeight, pBitmap->GetPixelFormat());

	if (pReducedBitmap)
	{
		Gdiplus::Graphics *pGraphics = new Gdiplus::Graphics(pReducedBitmap);

		if (pGraphics)
		{
			pGraphics->SetInterpolationMode(Gdiplus::InterpolationModeBilinear);
			pGraphics->DrawImage(pBitmap, 0, 0, reducedWidth, reducedHeight);

			delete pGraphics;

			pGraphics = new Gdiplus::Graphics(pBitmap);

			if (pGraphics)
			{
				// Do not interploate when scaling back up - the whole point is to produce a blocky image!
				pGraphics->SetInterpolationMode(Gdiplus::InterpolationModeNearestNeighbor);
				// By default, GDI+ puts the centre (rather than the top left corner)
				// of the top-left pixel at (0,0). Change this to get the results you expect!
				pGraphics->SetPixelOffsetMode(Gdiplus::PixelOffsetModeHalf);

				pGraphics->DrawImage(pReducedBitmap, 0, 0, pBitmap->GetWidth(), pBitmap->GetHeight());

				delete pGraphics;
			}
		}

		delete pReducedBitmap;
	}
}


BOOL
AI_Tiff::IsRenderRequired(int idealLOD/*=0*/)
{
// Has the image changed?
	BOOL required = FALSE;

	if (renderRequired)
	{
		// If the 'renderRequired' flag is set, a render is always required.
		required = TRUE;
	}
	else
	{
		// 'Render required' flag is not set, but see if we need to render
		// in order to use a new MIP map level because of a change in scale.
		if (pComponents)
		{
			// Get the LOD that will actually be used.
			int lod = GetAvailableMIPMapLOD(idealLOD);

			// Only render if the LOD that would be used for the current
			// scale is different from that used for the last render.
			if (lod != actualRenderedLOD)
			{
				required = TRUE;
			}
		}
	}

	return required;
}


void
AI_Tiff::Draw(HDC hDC, int x, int y, double scale, RECT *pClipRect/*=NULL*/,
              double alpha/*=1.0*/, int compressibilityFactor/*=0*/)//, DWORD dwRop/*=SRCCOPY*/)
{
// Blit render buffer to the supplied device context.
// (x, y) is the offset of the image origin relative to the device context
// origin.
// clipRect is the region of the device context that needs updating
// (relative to the device context). Do not draw outside of clipRect.
// Note that clipRect is NOT (neccesarily) the target area for
// StretchDIBits(), i.e. the image is not to be fitted to clipRect,
// rather, it is to be clipped by clipRect.
// Note that the origin for the coordinate parameters is at the top left.
	if (   pClipRect
	    && (pClipRect->right <= pClipRect->left || pClipRect->bottom <= pClipRect->top))
		return;


	// Be sure to call LeaveCriticalSection() wherever this function returns!
	EnterCriticalSection(AITiffGetCriticalSection());


	// Find out which MIP map level is appropriate for the current scale.
	int idealLOD = GetIdealMIPMapLOD(scale);


	// Check to see if a new render is required. Don't do this in Render()
	// itself, as calling that function directly should always lead to
	// a render, whether or not it is necessary.
	if (IsRenderRequired(idealLOD))
		Render(idealLOD);


	// Find out which MIP map level was used when rendering.
	int mipScaleDenominator = GetMIPMapScaleDenominator(actualRenderedLOD);

	// The overallScale is the scaling to apply to the image in the render buffer.
	// This will be the same as scale unless a MIP map LOD of greater than zero
	// is being used, in which case overallScale will be greater than scale to
	// compensate for the MIP map being smaller that the full size (LOD zero) image.
	double overallScale = scale * mipScaleDenominator;


	// Calculate the extents of the rendered image on screen, once it has been
	// offset and scaled.
	RECT screenRect;
	// Origin of rendered image is at offsets
	screenRect.left = x;
	screenRect.top  = y;
	// Dimensions of rendered image are dimensions of render buffer multiplied
	// by overall scale.
	screenRect.right  = screenRect.left + Round(bufWidth  * overallScale);
	screenRect.bottom = screenRect.top  + Round(bufHeight * overallScale);

	// Intersect the 'screen' rectangle with the clip rectangle (if any).
	// The result will be the area of the window that will be written to.
	RECT destRect = screenRect;
	if (pClipRect)
	{
		// If destRect is empty, return - nothing to show.
		if (!IntersectRect(&destRect, &screenRect, pClipRect))
		{
			LeaveCriticalSection(AITiffGetCriticalSection());
			return;
		}
	}


	// If overallScale is greater than 1, each source pixel will correspond
	// with a square of (overallScale*overallScale) destination pixels. If the
	// dest rect has been clipped such that the distance from the edge of the
	// screen rect to the the edge of the dest rect is not divisible exactly
	// by the scale, then there should be a partial pixel square displayed at
	// the edge of the dest rect. In order to achieve this with StretchDIBits,
	// the destination rectangle must be enlarged to match the nearest scaled
	// pixel edge.
	if (overallScale > 1.0)
	{
		if (destRect.left > screenRect.left)
			destRect.left   -= Round(fmod((destRect.left - screenRect.left),     overallScale));
		if (destRect.top > screenRect.top)
			destRect.top    -= Round(fmod((destRect.top - screenRect.top),       overallScale));
		if (destRect.right < screenRect.right)
			destRect.right  += Round(fmod((screenRect.right - destRect.right),   overallScale));
		if (destRect.bottom < screenRect.bottom)
			destRect.bottom += Round(fmod((screenRect.bottom - destRect.bottom), overallScale));
	}

	
	// Calculate the area of the rendered image buffer that corresponds with
	// destRect, by transforming destRect to image coordinates.
	RECT srcRect;
	srcRect.left   = (screenRect.left < destRect.left) ?
	                   Round((destRect.left - screenRect.left) / overallScale) : 0;

	srcRect.top    = (screenRect.top < destRect.top) ?
	                   Round((destRect.top  - screenRect.top)  / overallScale) : 0;

	srcRect.right  = bufWidth  - ((screenRect.right > destRect.right) ?
                                  Round((screenRect.right  - destRect.right) / overallScale) : 0); 

	srcRect.bottom = bufHeight - ((screenRect.bottom > destRect.bottom) ?
                                  Round((screenRect.bottom - destRect.bottom) / overallScale) : 0); 


	// Ensure that the srcRect contains at least one pixel, or nothing will be
	// displayed. This adjustment is needed at high zoom levels when only part
	// of a single pixel is visible.
	if (srcRect.left >= srcRect.right)
	{
		if (srcRect.left < bufWidth)
			srcRect.right = srcRect.left + 1;
		else
			srcRect.left = srcRect.right - 1;
	}

	if (srcRect.top >= srcRect.bottom)
	{
		if (srcRect.top < bufHeight)
			srcRect.bottom = srcRect.top + 1;
		else
			srcRect.top = srcRect.bottom - 1;
	}



	BITMAPINFO bitsInfo;

	memset(&bitsInfo, 0, sizeof(bitsInfo));
	bitsInfo.bmiHeader.biPlanes      = 1; // Must be 1
	bitsInfo.bmiHeader.biBitCount    = 24; // Bits per pixel
	bitsInfo.bmiHeader.biCompression = BI_RGB; // Not compressed
	bitsInfo.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
	bitsInfo.bmiHeader.biWidth       = bufWidth;
	bitsInfo.bmiHeader.biHeight      = (LONG)bufHeight;


	// If the image is scaled up, and the interpolateScaleUps flag is set,
	// use GDI+ for drawing so that interpolation is used, eliminating
	// pixelation. Don't use interpolation when scaling down, as it is
	// assumed that MIP-maps are being used, plus GDI+ is slower anyway.
	// MIP maps will need interpolation too, if overallScale is not 1.0,
	// but only do it if the apparent scale is > 1.0, othereise things are
	// slowed down too much.
	// Also use GDI+ for the image processing done when the compressibility
	// factor is greater than 1.
	if (  (scale > 1.0 && interpolateScaleUps)
	    || alpha != 1.0
	    || compressibilityFactor > 1)
	{
		// GDI+ is now going to be used, so start it if it isn't already
		// started. This isn't done in the constructor so that the overhead
		// isn't required unless GDI+ is actually used. However don't want to
		// keep starting and stopping GDI+ every time it is used as that incurs
		// a performance hit, so once it is started here it isn't stopped until
		// the AI_Tiff is destroyed. Better still than starting it here is if
		// GDI+ is just started once for the whole program when the
		// program starts, rather than once for each AI_Tiff object - in this
		// case, setting gdiplusStartedExternally (a parameter to the
		// constructor) lets the AI_Tiff object know it doesn't have to start
		// it itself.
		if (!gdiplusStartedExternally && !gdiplusToken)
		{
			Gdiplus::GdiplusStartupInput gdiplusStartupInput;
			if (   Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL)
			    != Gdiplus::Ok)
				gdiplusToken = NULL;
		}

		if ((gdiplusStartedExternally || gdiplusToken) && pBuf)
		{
			Gdiplus::Bitmap *pBitmap = new Gdiplus::Bitmap(&bitsInfo, pBuf);

			if (pBitmap)
			{
				// As the bitmap was created using a BITMAPINFO structure,
				// it starts off 'bottom up', i.e. flipped in the y-axis, 
				// so take this into account when applying the flip flags.
				if (drawFlipX && drawFlipY)	pBitmap->RotateFlip(Gdiplus::RotateNoneFlipX);
				else
				{
					if      (drawFlipX)	pBitmap->RotateFlip(Gdiplus::RotateNoneFlipXY);
					else if (drawFlipY)	pBitmap->RotateFlip(Gdiplus::RotateNoneFlipNone);
					else                pBitmap->RotateFlip(Gdiplus::RotateNoneFlipY);
				}

				Gdiplus::Graphics graphics(hDC);

				// InterpolationModeNearestNeighbor results in no interpolation being done.
				// InterpolationModeBilinear seems to be the lowest quality
				// (so presumably fastest) mode that actually interpolates.
				if (compressibilityFactor > 1)
				{
					ConvertBitmapForCompressibility(pBitmap, compressibilityFactor,
					                                &srcRect, &destRect);
					// Interpolating will reduce the compressibility, so don't do it.
					graphics.SetInterpolationMode(Gdiplus::InterpolationModeNearestNeighbor);
				}
				else if (scale > 1.0 && interpolateScaleUps)
					graphics.SetInterpolationMode(Gdiplus::InterpolationModeBilinear);
				else
					graphics.SetInterpolationMode(Gdiplus::InterpolationModeNearestNeighbor);


				// By default, GDI+ puts the centre (rather than the top left corner)
				// of the top-left pixel at (0,0). Change this so there is no shifting
				// when switching between using GDI+ and GDI. I don't know if this slows
				// things down.
				graphics.SetPixelOffsetMode(Gdiplus::PixelOffsetModeHalf);

				// Create a Rect object that specifies the destination of the image.
				Gdiplus::Rect destGdiplusRect(destRect.left, destRect.top,
			                                  destRect.right - destRect.left, destRect.bottom - destRect.top);

				if (scale < 1.0)
				{
				// At small scales, need to enlarge dest rect dimensions to be sure there are no gaps between frames.
				// TODO: This may be just patching up a symptom of a more serious miscalculation - check this out.
					destGdiplusRect.Width  +=2;
					destGdiplusRect.Height +=2;
				}



				Gdiplus::ImageAttributes *pImageAttributes = NULL;
				if (alpha != 1.0)
				{
					// Initialize a color matrix for transforming the bitmap colours.
					Gdiplus::ColorMatrix colorMatrix = {1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
				                                    0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
				                                    0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
				                                    0.0f, 0.0f, 0.0f, (float)alpha, 0.0f,
				                                    0.0f, 0.0f, 0.0f, 0.0f, 1.0f};

					// Create an ImageAttributes object and set its color matrix.
					pImageAttributes = new Gdiplus::ImageAttributes;
					if (pImageAttributes)
						pImageAttributes->SetColorMatrix(&colorMatrix, Gdiplus::ColorMatrixFlagsDefault,
						                                               Gdiplus::ColorAdjustTypeBitmap);
				}


				graphics.DrawImage(pBitmap, destGdiplusRect,
			                       srcRect.left, srcRect.top,
				                   srcRect.right - srcRect.left, srcRect.bottom - srcRect.top,
				                   Gdiplus::UnitPixel, pImageAttributes, NULL, NULL);


				if (pImageAttributes)
					delete pImageAttributes;

				delete pBitmap;
			}
		}
	}
	else if (pBuf)
	{
		int srcXoffset = 0;
		int srcWidth   = srcRect.right  - srcRect.left;

		if (drawFlipX)
		{
			// Offset is from right, as image is flipped.
			if (screenRect.right > destRect.right)
			{
				if (screenRect.left < destRect.left)
					srcXoffset = bufWidth - srcRect.left - 1;
				else
					srcXoffset = bufWidth - 1;
			}
			else
				srcXoffset = srcWidth - 1;

			// If signs of src and dest width are different, image will be flipped.
			srcWidth   = -srcWidth; 
		}
		else
			srcXoffset = srcRect.left;


		// StretchDIBits expects the parameter giving the yoffset value into the
		// source bitmap to be from the bottom of the bitmap, not the top,
		// even for 'top-down' bitmaps. Calculate this value:
		int srcYoffset = (screenRect.bottom > destRect.bottom) ?
			                                      bufHeight - srcRect.bottom : 0;

		int srcHeight = srcRect.bottom - srcRect.top;

		if (drawFlipY)
		{
			// Treat bitmap as 'bottom-up'.
			bitsInfo.bmiHeader.biHeight = (LONG)bufHeight;
		}
		else
		{
			// Set the height to a negative value
			// to indicate that the bitmap is 'top-down'.
			bitsInfo.bmiHeader.biHeight = -(LONG)(bufHeight);

			// Not sure why, but at a scale of 1, the Y offset is taken from the
			// top of the bitmap rather than the bottom, if the Xoffset is zero!
			// So make the X offset 1 in this instance, to prevent this.
			if (scale == 1.0)
				srcXoffset = (srcXoffset < 1) ? 1 : srcXoffset;
		}

		SetStretchBltMode(hDC, STRETCH_DELETESCANS);

		if (scale < 1.0)
		// At small scales, need to enlarge dest rect dimensions to be sure there are no gaps between frames.
		// TODO: This may be just patching up a symptom of a more serious miscalculation - check this out.
			StretchDIBits(hDC, destRect.left, destRect.top,
			                   (destRect.right - destRect.left)+2, (destRect.bottom - destRect.top)+2,
			                   srcXoffset, srcYoffset, srcWidth, srcHeight,
			                   pBuf, &bitsInfo, DIB_RGB_COLORS, SRCCOPY);//dwRop);
		else
			StretchDIBits(hDC, destRect.left, destRect.top,
			                   (destRect.right - destRect.left), (destRect.bottom - destRect.top),
			                   srcXoffset, srcYoffset, srcWidth, srcHeight,
			                   pBuf, &bitsInfo, DIB_RGB_COLORS, SRCCOPY);//dwRop);
	}

/*
HGDIOBJ pen = GetStockObject(BLACK_PEN);
SelectObject(hDC, pen);
MoveToEx(hDC, destRect.left, destRect.top, NULL);
LineTo(hDC, destRect.left, destRect.bottom);
LineTo(hDC, destRect.right, destRect.bottom);
LineTo(hDC, destRect.right, destRect.top);
LineTo(hDC, destRect.left, destRect.top);
*/

	LeaveCriticalSection(AITiffGetCriticalSection());
}


void
AI_Tiff::DrawStretched(HDC hDC, RECT *pTargetRect, RECT *pClipRect/*=NULL*/)
{
// Draws the image so that it fits exactly in the target rectangle, stretching
// as neccessary. The current scale is ignored. MIP maps are not used. The X
// and Y flip settings are ignored. This function was created for drawing the
// slide image in the slidemap. For most other purposes you should always
// find a way of using AI_Tiff::Draw() instead, if at all possible.
	if (!pTargetRect)
		return;


	// Intersect the on screen target rectangle with the clip rectangle.
	// The result will be the area of the window that will be written to.
	// If there is no clip rect, the dest rect is just the target rect.
	RECT destRect = *pTargetRect;
	if (pClipRect)
	{
		// If destRect is empty, return from function - nothing to show.
		if (!IntersectRect(&destRect, pTargetRect, pClipRect))
			return;
	}


	EnterCriticalSection(AITiffGetCriticalSection());


	// Only need to render image if it hasn't been done before at
	// a scale of 1 or more.
	if (IsRenderRequired(0))
		Render(0);


	// Calculate the area of the rendered image buffer that corresponds with
	// destRect, by transforming destRect to image coordinates.
	RECT srcRect;
	srcRect.left   = (pTargetRect->left < destRect.left) ?
	                   (((destRect.left - pTargetRect->left) * bufWidth) / (pTargetRect->right - pTargetRect->left)) : 0;

	srcRect.top    = (pTargetRect->top < destRect.top) ?
	                   (((destRect.top  - pTargetRect->top) * bufHeight) / (pTargetRect->bottom - pTargetRect->top)) : 0;

	srcRect.right  = bufWidth - ((pTargetRect->right > destRect.right) ?
                                  (((pTargetRect->right  - destRect.right) * bufWidth) / (pTargetRect->right - pTargetRect->left)) : 0); 

	srcRect.bottom = bufHeight - ((pTargetRect->bottom > destRect.bottom) ?
                                  (((pTargetRect->bottom - destRect.bottom) * bufHeight) / (pTargetRect->bottom - pTargetRect->top)) : 0); 


	if (pBuf)
	{
		BITMAPINFO bitsInfo;
		memset(&bitsInfo, 0, sizeof(bitsInfo));
		bitsInfo.bmiHeader.biPlanes      = 1; // Must be 1
		bitsInfo.bmiHeader.biBitCount    = 24; // Bits per pixel
		bitsInfo.bmiHeader.biCompression = BI_RGB; // Not compressed
		bitsInfo.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
		bitsInfo.bmiHeader.biWidth       = bufWidth;
		bitsInfo.bmiHeader.biHeight      = (LONG)bufHeight;


		int srcXoffset = srcRect.left;
		int srcWidth   = srcRect.right  - srcRect.left;

		// StretchDIBits expects the parameter giving the yoffset value into the
		// source bitmap to be from the bottom of the bitmap, not the top,
		// even for 'top-down' bitmaps. Calculate this value:
		int srcYoffset = (pTargetRect->bottom > destRect.bottom) ?
			                                      bufHeight - srcRect.bottom : 0;
		int srcHeight = srcRect.bottom - srcRect.top;


		SetStretchBltMode(hDC, STRETCH_DELETESCANS);

		StretchDIBits(hDC, destRect.left, destRect.top,
		              (destRect.right - destRect.left), (destRect.bottom - destRect.top),
		              srcXoffset, srcYoffset, srcWidth, srcHeight,
		              pBuf, &bitsInfo, DIB_RGB_COLORS, SRCCOPY);
	}

	LeaveCriticalSection(AITiffGetCriticalSection());
}



BOOL
AI_Tiff::Get24bppBuffer(uint8 *pBuffer)
{
// Copy the contents of the render buffer to the given buffer, which must be
// at least (GetWidth() * GetHeight() * 3) bytes large.
// The render buffer stores pixels in BGR format, with rows padded to 4 byte
// boundaries. Want destination buffer to be in RGB format, no padding (as in
// an RGB TIFF component)
	BOOL status = FALSE;

	EnterCriticalSection(AITiffGetCriticalSection());

	if (pBuf && pBuffer)
	{
		uint8 *pBufRow = pBuf;

		for (uint32 y=0; y < bufHeight; y++)
		{
			uint8 *pB = pBufRow;

			for (uint32 x=0; x < bufWidth; x++)
			{
				*pBuffer++ = *(pB++ + 2);
				*pBuffer++ = *(pB++);
				*pBuffer++ = *(pB++ - 2);
			}
		
			pBufRow += bufWidthBytes;
		}

		status = TRUE;
	}

	LeaveCriticalSection(AITiffGetCriticalSection());

	return status;
}

//////////////////////////////////////////////////////////////////////
//
//	SetBuffer()
//	Copy the contents of the render buffer to the given buffer, which must be
//	at least (bufWidth * bufHeight * 3) bytes large.
//	bufWidth bufHeight - are the dimensions of the render buffer
//	pBuf - is a pointer to the render buffer
//	This is similar to the above function, but doesn't mess with the byte order
//
BOOL
AI_Tiff::SetBuffer(uint8 *pBuffer)
{
	BOOL status = FALSE;

	EnterCriticalSection(AITiffGetCriticalSection());

	if (pBuf && pBuffer)
	{
		memcpy(pBuffer, pBuf, bufWidth * bufHeight * 3);
		status = TRUE;
	}

	LeaveCriticalSection(AITiffGetCriticalSection());

	return status;
}



int
AI_Tiff::GetWidth()
{
// Width of image is largest component width
// (although all components will usually be the same size)
	uint32 width = 0;

	EnterCriticalSection(AITiffGetCriticalSection());

	AI_TiffComponent *pCurrComponent = pComponents;

	while (pCurrComponent)
	{
		if (pCurrComponent->width > width)
			width = pCurrComponent->width;

		pCurrComponent = pCurrComponent->pNext;
	}

	LeaveCriticalSection(AITiffGetCriticalSection());

	return (int)width;
}

int
AI_Tiff::GetHeight()
{
// Height of image is largest component height
// (although all components will usually be the same size)
	uint32 height = 0;

	EnterCriticalSection(AITiffGetCriticalSection());

	AI_TiffComponent *pCurrComponent = pComponents;

	while (pCurrComponent)
	{
		if (pCurrComponent->height > height)
			height = pCurrComponent->height;

		pCurrComponent = pCurrComponent->pNext;
	}

	LeaveCriticalSection(AITiffGetCriticalSection());

	return (int)height;
}





#if 0
/*
void
AI_Tiff::OldDraw(HDC hDC, int x, int y, RECT *pUpdateRect, DWORD dwRop)
{
// Blit display buffer to the supplied device context.
// (x, y) is the offset of the device context origin relative to the image origin,
// so to offset the image relative to the device context, use negative values.
// updateRect is the region of the device context to update (relative to the
// device context). 
// To summarise, (x,y) is in image coords, updateRect is in DC coords.
// Note that the origin for the coordinate parameters is at the top left.
	BITMAPINFO bitsInfo;

	memset(&bitsInfo, 0, sizeof(bitsInfo));
	bitsInfo.bmiHeader.biPlanes      = 1; // Must be 1
	bitsInfo.bmiHeader.biBitCount    = 24; // Bits per pixel
	bitsInfo.bmiHeader.biCompression = BI_RGB; // Not compressed
	bitsInfo.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);

	bitsInfo.bmiHeader.biWidth       = bufWidth;
	// Set negative height to indicate that the data is in 'top down'
	// format (origin in top-left).
	bitsInfo.bmiHeader.biHeight      = -(LONG)(bufHeight);


	int destX, destY, destWidth, destHeight;

	// For AITiffScale, either the numerator or the denominator is 1.
	int scaleNumerator   = scale.GetNumerator();
	int scaleDenominator = scale.GetDenominator();

	if (pUpdateRect)
	{
		destX      = pUpdateRect->left;
		destY      = pUpdateRect->top;
		destWidth  = pUpdateRect->right  - pUpdateRect->left + 1;
		destHeight = pUpdateRect->bottom - pUpdateRect->top + 1;
	}
	else
	{
		destX = destY = 0;

		// For AITiffScale, either the numerator or the denominator is 1.
		if (scaleNumerator != 1)
		{
			destWidth  = (bufWidth  * scaleNumerator) - x;
			destHeight = (bufHeight * scaleNumerator) - y;
		}
		else
		{
			destWidth  = (bufWidth  / scaleDenominator) - x;
			destHeight = (bufHeight / scaleDenominator) - y;
		}
	}


	int srcX, srcY, srcWidth, srcHeight;

	if (pUpdateRect)
	{
		// For AITiffScale, either the numerator or the denominator is 1.
		if (scaleNumerator != 1)
		{
			srcX      = (x + destX) / scaleNumerator;
			srcY      = bufHeight - (y + destY + destHeight) / scaleNumerator;
			srcWidth  = destWidth   / scaleNumerator;
			srcHeight = destHeight  / scaleNumerator;
		}
		else
		{
			srcX      = (x + destX) * scaleDenominator;
			srcY      = bufHeight - (y + destY + destHeight) * scaleDenominator;
			srcWidth  = destWidth   * scaleDenominator;
			srcHeight = destHeight  * scaleDenominator;
		}		
	}
	else
	{
		// For AITiffScale, either the numerator or the denominator is 1.
		if (scaleNumerator != 1)
		{
			srcX      = x / scaleNumerator;
			srcY      = 0;
			srcWidth  = bufWidth  - srcX;
			srcHeight = bufHeight - (y / scaleNumerator);
		}
		else
		{
			srcX      = x * scaleDenominator;
			srcY      = 0;
			srcWidth  = bufWidth  - srcX;
			srcHeight = bufHeight - (y * scaleDenominator);
		}		
	}

	// Not sure why but ?
	if (srcY == 0) srcY = 1;

	SetStretchBltMode(hDC,STRETCH_DELETESCANS);

	StretchDIBits(hDC, destX, destY, destWidth, destHeight,
	                   srcX,  srcY,  srcWidth,  srcHeight,
	                   pBuf, &bitsInfo, DIB_PAL_COLORS, dwRop);
}
*/
#endif





/*
void
AI_Tiff::DrawThumbnails(HDC hDC)
{
// Draw loaded images side-by-side, at reduced scale.

	AI_TiffComponent *pCurrComponent = pComponents;


	int numComponents = 0;

	
	while (pCurrComponent)
	{
		numComponents++;
		pCurrComponent = pCurrComponent->pNext;
	}
	pCurrComponent = pComponents;

	

	int offset = 0;

	while (pCurrComponent)
	{
		if (pCurrComponent && pCurrComponent->pData)
		{
			BITMAPINFO bitsInfo;

			memset(&bitsInfo, 0, sizeof(bitsInfo));
			bitsInfo.bmiHeader.biPlanes      = 1; // Must be 1
			bitsInfo.bmiHeader.biBitCount    = 32; // Bits per pixel
			bitsInfo.bmiHeader.biCompression = BI_RGB; // Not compressed
			bitsInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);

			bitsInfo.bmiHeader.biWidth  = pCurrComponent->width;
			// Set negative height to indicate that the data is in 'top down'
			// format (origin in top-left).
			bitsInfo.bmiHeader.biHeight = -(LONG)(pCurrComponent->height);

			if (pCurrComponent->samplesPerPixel)
			{
				StretchDIBits(hDC, offset, 0, pCurrComponent->width / numComponents, pCurrComponent->height / numComponents,
				                   0, 0, pCurrComponent->width, pCurrComponent->height,
				                   pCurrComponent->pData, &bitsInfo, DIB_PAL_COLORS, SRCCOPY);
			}
			else
			{
				uint32 *rgba = (uint32*)pCurrComponent->pData;
				uint32 x, y;

				for (y=0; y < pCurrComponent->height; y++)
				{
					for (x=0; x < pCurrComponent->width; x++)
					{
						SetPixel(hDC, x, pCurrComponent->height - y - 1, 
						         RGB(TIFFGetR(*rgba), TIFFGetG(*rgba), TIFFGetB(*rgba)));

						rgba++;
					}

//			rgba += pDoc->pImage->width;
				}
			}
		}


		offset += pCurrComponent->width / numComponents;

		pCurrComponent = pCurrComponent->pNext;
	}
}
*/


/*
void
ConvertBitmapForCompressibility(Gdiplus::Bitmap *pBitmap, int compressibilityFactor,
                                const RECT *pSrcRect, const RECT *pDstRect)
{
// This uses a simple edge detection algorithm,
// that does nothing for compressibility (probably makes it worse)!
	if (!pBitmap) return;

	int w = pBitmap->GetWidth();
	int h = pBitmap->GetHeight();
	Gdiplus::Rect rect(0, 0, w, h);

	Gdiplus::Bitmap *pDupBitmap = pBitmap->Clone(rect, pBitmap->GetPixelFormat());

	if (pDupBitmap)
	{
		Gdiplus::BitmapData dupBitmapData;
		if (pDupBitmap->LockBits(&rect, Gdiplus::ImageLockModeRead,
		                         PixelFormat24bppRGB, &dupBitmapData) == Gdiplus::Ok)
		{
			Gdiplus::BitmapData bitmapData;
			if (pBitmap->LockBits(&rect, Gdiplus::ImageLockModeWrite,
			                      PixelFormat24bppRGB, &bitmapData) == Gdiplus::Ok)
			{
				// The stride can be positive or negative to indicate
				// whether the bitmap is top-down or bottom-up.
				INT stride = abs(bitmapData.Stride);
				
				UCHAR *pInRow  = (UCHAR*)dupBitmapData.Scan0 + stride;
				UCHAR *pOutRow = (UCHAR*)bitmapData.Scan0 + stride;

				int byteWidth = w * 3;

				for (int y = 1; y < h - 1; y++)
				{
					UCHAR *pIn  = pInRow + 3;
					UCHAR *pOut = pOutRow + 3;
					for (int x = 3; x < byteWidth - 3; x++)
					{
						int value =     (*pIn) * 8
						              - *(pIn - stride - 3)
						              - *(pIn - stride)
						              - *(pIn - stride + 3)
						              - *(pIn - 1)
						              - *(pIn + 1)
						              - *(pIn + stride - 3)
						              - *(pIn + stride)
						              - *(pIn + stride + 3);

						value = (value < 0) ? -(value) : value;
						value = (value > 255) ? 255 : value;
						//value = (value > 0) ? value : 0;
						*pOut++ = value;

						pIn++;
					}

					pInRow  += stride;
					pOutRow += stride;
				}

				pBitmap->UnlockBits(&bitmapData);
			}

			pDupBitmap->UnlockBits(&dupBitmapData);
		}

		delete pDupBitmap;
	}
}
*/

