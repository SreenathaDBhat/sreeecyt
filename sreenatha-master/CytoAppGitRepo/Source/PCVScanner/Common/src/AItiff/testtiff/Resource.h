//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by testtiff.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_TESTTITYPE                  129
#define IDD_DIALOG_SETTINGS             130
#define ID_FILE_OPEN_BUFFER             131
#define ID_FILE_SAVEAS_BUFFER           132
#define IDC_SLIDER_ZOOM                 1000
#define IDC_CHECK1                      1001
#define IDC_CHECK2                      1002
#define IDC_CHECK3                      1003
#define IDC_CHECK0                      1005
#define IDC_EDIT0                       1006
#define IDC_EDIT1                       1007
#define IDC_EDIT2                       1008
#define IDC_EDIT3                       1009
#define IDC_CHECK_FAST                  1010
#define IDC_BUTTON0                     1011
#define IDC_BUTTON1                     1012
#define IDC_BUTTON2                     1013
#define IDC_BUTTON3                     1014
#define IDC_SAVE0                       1015
#define IDC_SAVE1                       1016
#define IDC_SAVE2                       1017
#define IDC_SAVE3                       1018

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
