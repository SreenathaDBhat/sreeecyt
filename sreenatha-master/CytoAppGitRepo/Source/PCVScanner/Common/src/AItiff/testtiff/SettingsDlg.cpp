// SettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "testtiff.h"
#include "SettingsDlg.h"

#include "testtiffView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSettingsDlg dialog


CSettingsDlg::CSettingsDlg(CTesttiffDoc *pDoc /*= NULL*/, CWnd* pParent /*=NULL*/)
	: CDialog(CSettingsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSettingsDlg)
	//}}AFX_DATA_INIT

	this->pDoc = pDoc;
	pTiff = NULL;
}


void CSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSettingsDlg)
	DDX_Control(pDX, IDC_CHECK_FAST, m_checkFast);
	DDX_Control(pDX, IDC_EDIT3, m_edit3);
	DDX_Control(pDX, IDC_EDIT2, m_edit2);
	DDX_Control(pDX, IDC_EDIT1, m_edit1);
	DDX_Control(pDX, IDC_EDIT0, m_edit0);
	DDX_Control(pDX, IDC_CHECK3, m_check3);
	DDX_Control(pDX, IDC_CHECK2, m_check2);
	DDX_Control(pDX, IDC_CHECK0, m_check0);
	DDX_Control(pDX, IDC_CHECK1, m_check1);
	DDX_Control(pDX, IDC_SLIDER_ZOOM, m_zoomSlider);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSettingsDlg, CDialog)
	//{{AFX_MSG_MAP(CSettingsDlg)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_ZOOM, OnReleasedcaptureSliderZoom)
	ON_BN_CLICKED(IDC_CHECK1, OnCheck1)
	ON_BN_CLICKED(IDC_CHECK0, OnCheck0)
	ON_BN_CLICKED(IDC_CHECK2, OnCheck2)
	ON_BN_CLICKED(IDC_CHECK3, OnCheck3)
	ON_EN_CHANGE(IDC_EDIT0, OnChangeEdit0)
	ON_EN_CHANGE(IDC_EDIT1, OnChangeEdit1)
	ON_EN_CHANGE(IDC_EDIT2, OnChangeEdit2)
	ON_EN_CHANGE(IDC_EDIT3, OnChangeEdit3)
	ON_BN_CLICKED(IDC_CHECK_FAST, OnCheckFast)
	ON_BN_CLICKED(IDC_BUTTON0, OnButton0)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	ON_BN_CLICKED(IDC_SAVE0, OnSave0)
	ON_BN_CLICKED(IDC_SAVE1, OnSave1)
	ON_BN_CLICKED(IDC_SAVE2, OnSave2)
	ON_BN_CLICKED(IDC_SAVE3, OnSave3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSettingsDlg message handlers

void CSettingsDlg::OnReleasedcaptureSliderZoom(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	if (pDoc && pTiff)
	{
		if (m_zoomSlider.GetPos() > 0)
			pTiff->scale.SetInteger(m_zoomSlider.GetPos());
		else
			pTiff->scale.SetFractional(2 - m_zoomSlider.GetPos());

		POSITION pos;
		pos = pDoc->GetFirstViewPosition();
		CTesttiffView *pView = (CTesttiffView*)pDoc->GetNextView(pos);
		pView->Invalidate();
	}
	

	*pResult = 0;
}

BOOL CSettingsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	m_zoomSlider.SetRange(-2, 4, TRUE);
	m_zoomSlider.SetPos(1);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CSettingsDlg::UpdateDisplay()
{
	if (pDoc && pTiff) 
	{
		pTiff->Render();

		POSITION pos;
		pos = pDoc->GetFirstViewPosition();
		CTesttiffView *pView = (CTesttiffView*)pDoc->GetNextView(pos);
		pView->Invalidate();
	}
}


void CSettingsDlg::OnCheck0() 
{
	// TODO: Add your control notification handler code here

	if (pTiff) pTiff->SetComponentVisible(0, m_check0.GetCheck());

	UpdateDisplay();
}

void CSettingsDlg::OnCheck1() 
{
	// TODO: Add your control notification handler code here

	if (pTiff) pTiff->SetComponentVisible(1, m_check1.GetCheck());

	UpdateDisplay();
}

void CSettingsDlg::OnCheck2() 
{
	// TODO: Add your control notification handler code here
	if (pTiff) pTiff->SetComponentVisible(2, m_check2.GetCheck());

	UpdateDisplay();
}

void CSettingsDlg::OnCheck3() 
{
	// TODO: Add your control notification handler code here
	if (pTiff) pTiff->SetComponentVisible(3, m_check3.GetCheck());

	UpdateDisplay();
}


void CSettingsDlg::OnChangeEdit0() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	char text[32];

	m_edit0.GetWindowText(text, sizeof(text));

	if (pTiff) pTiff->SetComponentDescription(0, text);
}

void CSettingsDlg::OnChangeEdit1() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	char text[32];

	m_edit1.GetWindowText(text, sizeof(text));

	if (pTiff) pTiff->SetComponentDescription(1, text);	
}

void CSettingsDlg::OnChangeEdit2() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	char text[32];

	m_edit2.GetWindowText(text, sizeof(text));

	if (pTiff) pTiff->SetComponentDescription(2, text);	
}

void CSettingsDlg::OnChangeEdit3() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	char text[32];

	m_edit3.GetWindowText(text, sizeof(text));

	if (pTiff) pTiff->SetComponentDescription(3, text);	
}



void CSettingsDlg::OnCheckFast() 
{
	// TODO: Add your control notification handler code here

	if (pTiff) pTiff->SetColourMixMode(m_checkFast.GetCheck());

	UpdateDisplay();
}


static BOOL GetColour(rgbTriple *pColour)
{
	CColorDialog dlg;

	if (dlg.DoModal() == IDOK)
	{
		COLORREF colorRef = dlg.GetColor();

		pColour->r = GetRValue(colorRef);
		pColour->g = GetGValue(colorRef);
		pColour->b = GetBValue(colorRef);

		return TRUE;
	}

	return FALSE;
}


void CSettingsDlg::OnButton0() 
{
	// TODO: Add your control notification handler code here

	rgbTriple colour;

	if (pTiff && GetColour(&colour)) pTiff->SetComponentColour(0, colour);

	UpdateDisplay();
}

void CSettingsDlg::OnButton1() 
{
	// TODO: Add your control notification handler code here
	rgbTriple colour;

	if (pTiff && GetColour(&colour)) pTiff->SetComponentColour(1, colour);

	UpdateDisplay();	
}

void CSettingsDlg::OnButton2() 
{
	// TODO: Add your control notification handler code here
	rgbTriple colour;

	if (pTiff && GetColour(&colour)) pTiff->SetComponentColour(2, colour);

	UpdateDisplay();	
}

void CSettingsDlg::OnButton3() 
{
	// TODO: Add your control notification handler code here
	rgbTriple colour;

	if (pTiff && GetColour(&colour)) pTiff->SetComponentColour(3, colour);

	UpdateDisplay();	
}


void CSettingsDlg::OnSave0() 
{
	CFileDialog dlg(FALSE);
	
	if (dlg.DoModal() == IDOK)
	{
		AI_Tiff tiff;
		tiff.Assimilate(pTiff);
		tiff.RemoveComponent(3);
		tiff.RemoveComponent(2);
		tiff.RemoveComponent(1);
		tiff.SaveFile((PCSTR)dlg.GetPathName(), COMPRESSION_PACKBITS);
	}
}

void CSettingsDlg::OnSave1() 
{
	CFileDialog dlg(FALSE);
	
	if (dlg.DoModal() == IDOK)
	{
		AI_Tiff tiff;
		tiff.Assimilate(pTiff);
		tiff.RemoveComponent(3);
		tiff.RemoveComponent(2);
		tiff.RemoveComponent(0);
		tiff.SaveFile((PCSTR)dlg.GetPathName(), COMPRESSION_PACKBITS);
	}
}

void CSettingsDlg::OnSave2() 
{
	CFileDialog dlg(FALSE);
	
	if (dlg.DoModal() == IDOK)
	{
		AI_Tiff tiff;
		tiff.Assimilate(pTiff);
		tiff.RemoveComponent(3);
		tiff.RemoveComponent(0);
		tiff.RemoveComponent(0);
		tiff.SaveFile((PCSTR)dlg.GetPathName(), COMPRESSION_PACKBITS);
	}
}

void CSettingsDlg::OnSave3() 
{
	CFileDialog dlg(FALSE);
	
	if (dlg.DoModal() == IDOK)
	{
		AI_Tiff tiff;
		tiff.Assimilate(pTiff);
		tiff.RemoveComponent(0);
		tiff.RemoveComponent(0);
		tiff.RemoveComponent(0);
		tiff.SaveFile((PCSTR)dlg.GetPathName(), COMPRESSION_PACKBITS);
	}
	
}

