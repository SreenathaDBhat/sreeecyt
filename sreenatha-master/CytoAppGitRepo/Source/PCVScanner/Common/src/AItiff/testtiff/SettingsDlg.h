#if !defined(AFX_SETTINGSDLG_H__326B9885_EBFC_4CC1_A80C_197F7954BAFC__INCLUDED_)
#define AFX_SETTINGSDLG_H__326B9885_EBFC_4CC1_A80C_197F7954BAFC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SettingsDlg.h : header file
//


#include <AiTiff.h>

#include "testtiffDoc.h"

/////////////////////////////////////////////////////////////////////////////
// CSettingsDlg dialog

class CSettingsDlg : public CDialog
{
	CTesttiffDoc *pDoc;

public:
	AI_Tiff *pTiff;

	void UpdateDisplay();


// Construction
public:
	CSettingsDlg(CTesttiffDoc *pDoc = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSettingsDlg)
	enum { IDD = IDD_DIALOG_SETTINGS };
	CButton	m_checkFast;
	CEdit	m_edit3;
	CEdit	m_edit2;
	CEdit	m_edit1;
	CEdit	m_edit0;
	CButton	m_check3;
	CButton	m_check2;
	CButton	m_check0;
	CButton	m_check1;
	CSliderCtrl	m_zoomSlider;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSettingsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSettingsDlg)
	afx_msg void OnReleasedcaptureSliderZoom(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL OnInitDialog();
	afx_msg void OnCheck1();
	afx_msg void OnCheck0();
	afx_msg void OnCheck2();
	afx_msg void OnCheck3();
	afx_msg void OnChangeEdit0();
	afx_msg void OnChangeEdit1();
	afx_msg void OnChangeEdit2();
	afx_msg void OnChangeEdit3();
	afx_msg void OnCheckFast();
	afx_msg void OnButton0();
	afx_msg void OnButton1();
	afx_msg void OnButton2();
	afx_msg void OnButton3();
	afx_msg void OnSave0();
	afx_msg void OnSave1();
	afx_msg void OnSave2();
	afx_msg void OnSave3();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETTINGSDLG_H__326B9885_EBFC_4CC1_A80C_197F7954BAFC__INCLUDED_)
