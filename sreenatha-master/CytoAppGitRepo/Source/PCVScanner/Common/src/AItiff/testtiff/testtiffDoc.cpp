// testtiffDoc.cpp : implementation of the CTesttiffDoc class
//

#include "stdafx.h"
#include "testtiff.h"

#include "testtiffDoc.h"

#include "settingsDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTesttiffDoc

IMPLEMENT_DYNCREATE(CTesttiffDoc, CDocument)

BEGIN_MESSAGE_MAP(CTesttiffDoc, CDocument)
	//{{AFX_MSG_MAP(CTesttiffDoc)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_FILE_SAVEAS_BUFFER, OnFileSaveasBuffer)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTesttiffDoc construction/destruction

CTesttiffDoc::CTesttiffDoc()
{
	// TODO: add one-time construction code here
	pTiff = NULL;
}

CTesttiffDoc::~CTesttiffDoc()
{
	if (pTiff) delete pTiff;
}

BOOL CTesttiffDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	if (pTiff) delete pTiff;
	pTiff = NULL;



	CSettingsDlg *pSettingsDlg = ((CTesttiffApp*)AfxGetApp())->pSettingsDlg;
	if (pSettingsDlg)
	{
		pSettingsDlg->pTiff = NULL;
		pSettingsDlg->m_zoomSlider.SetPos(1);
		pSettingsDlg->m_check0.SetCheck(0);
		pSettingsDlg->m_check1.SetCheck(0);
		pSettingsDlg->m_check2.SetCheck(0);
		pSettingsDlg->m_check3.SetCheck(0);
		pSettingsDlg->m_edit0.SetWindowText("");
		pSettingsDlg->m_edit1.SetWindowText("");
		pSettingsDlg->m_edit2.SetWindowText("");
		pSettingsDlg->m_edit3.SetWindowText("");
		pSettingsDlg->m_checkFast.SetCheck(0);
	}



	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CTesttiffDoc serialization

void CTesttiffDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CTesttiffDoc diagnostics

#ifdef _DEBUG
void CTesttiffDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTesttiffDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTesttiffDoc commands

BOOL CTesttiffDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
//	if (!CDocument::OnOpenDocument(lpszPathName))
//		return FALSE;
	
	// TODO: Add your specialized creation code here

	AI_Tiff *pNewTiff = new AI_Tiff;

	pNewTiff->LoadFile(lpszPathName);

	if (!pTiff)	pTiff = pNewTiff;
	else
	{
		pTiff->Assimilate(pNewTiff);
		delete pNewTiff;
	}

/*
size_t size;
char *pBuf = pTiff->SaveFileBuffer(&size);
pTiff->LoadFileBuffer(pBuf, size);
pTiff->FreeFileBuffer(pBuf);
*/



	pTiff->Render();


	CSettingsDlg *pSettingsDlg = ((CTesttiffApp*)AfxGetApp())->pSettingsDlg;


	pSettingsDlg->pTiff = pTiff;

	// Set zoom slider scale to corrspond with value in AI_Tiff object.
	pSettingsDlg->m_zoomSlider.SetPos(pTiff->scale.GetNumerator());

	// Set check boxes to correspond to component visibilities	
	pSettingsDlg->m_check0.SetCheck(pTiff->IsComponentVisible(0));
	pSettingsDlg->m_check1.SetCheck(pTiff->IsComponentVisible(1));
	pSettingsDlg->m_check2.SetCheck(pTiff->IsComponentVisible(2));
	pSettingsDlg->m_check3.SetCheck(pTiff->IsComponentVisible(3));

	char text[32];
	if (pTiff->GetComponentDescription(0, text, sizeof(text)))
		pSettingsDlg->m_edit0.SetWindowText(text);
	if (pTiff->GetComponentDescription(1, text, sizeof(text)))
		pSettingsDlg->m_edit1.SetWindowText(text);
	if (pTiff->GetComponentDescription(2, text, sizeof(text)))
		pSettingsDlg->m_edit2.SetWindowText(text);
	if (pTiff->GetComponentDescription(3, text, sizeof(text)))
		pSettingsDlg->m_edit3.SetWindowText(text);

	pSettingsDlg->m_checkFast.SetCheck(0);


	return TRUE;
}

BOOL CTesttiffDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	// TODO: Add your specialized code here and/or call the base class

	if (pTiff)
	{
/*
uint8 *pBuffer = (uint8*)malloc(pTiff->GetDisplayWidth() * pTiff->GetDisplayHeight() * 3);
pTiff->DrawTo24bppBuffer(pBuffer);
pTiff->Add24bppComponent(pBuffer, pTiff->GetDisplayWidth(), pTiff->GetDisplayHeight());
free(pBuffer);
*/

		pTiff->SaveFile(lpszPathName, COMPRESSION_PACKBITS);
	}


	
	// Don't call this as it will overwrite what you just wrote!  return CDocument::OnSaveDocument(lpszPathName);


	return TRUE;
}

void CTesttiffDoc::OnFileSave() 
{
	// TODO: Add your command handler code here
	AfxMessageBox("Use Save As...!");
}




void CTesttiffDoc::OnFileSaveasBuffer()
{
	if (pTiff)
	{
		OPENFILENAME ofn;       // common dialog box structure
		char szFile[260];       // buffer for file name


		szFile[0] = '\0';

		// Initialize OPENFILENAME
		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = sizeof(szFile);
		ofn.lpstrFilter = "TIF\0*.TIF\0All\0*.*\0";
		ofn.nFilterIndex = 1;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

		if (GetSaveFileName(&ofn)) 
		{
			size_t size;
			char *pBuf = pTiff->SaveFileBuffer(&size);


			FILE *pFILE = NULL;
			if ((pFILE = fopen(ofn.lpstrFile, "wb")) != NULL)
			{
				fwrite(pBuf, size, 1, pFILE);

				fclose(pFILE);
			}

			pTiff->FreeFileBuffer(pBuf);
		}
	}
}
