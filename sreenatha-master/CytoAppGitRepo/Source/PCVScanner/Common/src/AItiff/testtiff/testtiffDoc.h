// testtiffDoc.h : interface of the CTesttiffDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TESTTIFFDOC_H__B56A76B4_A508_4F31_AD86_B637F4D81F5B__INCLUDED_)
#define AFX_TESTTIFFDOC_H__B56A76B4_A508_4F31_AD86_B637F4D81F5B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "AItiff.h"


class CTesttiffDoc : public CDocument
{
protected: // create from serialization only
	CTesttiffDoc();
	DECLARE_DYNCREATE(CTesttiffDoc)

// Attributes
public:

	AI_Tiff *pTiff;


// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTesttiffDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTesttiffDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CTesttiffDoc)
	afx_msg void OnFileSave();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnFileSaveasBuffer();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTTIFFDOC_H__B56A76B4_A508_4F31_AD86_B637F4D81F5B__INCLUDED_)
