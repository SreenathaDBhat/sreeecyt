// testtiffView.cpp : implementation of the CTesttiffView class
//

#include "stdafx.h"
#include "testtiff.h"

#include "testtiffDoc.h"
#include "testtiffView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTesttiffView

IMPLEMENT_DYNCREATE(CTesttiffView, CView)

BEGIN_MESSAGE_MAP(CTesttiffView, CView)
	//{{AFX_MSG_MAP(CTesttiffView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTesttiffView construction/destruction

CTesttiffView::CTesttiffView()
{
	// TODO: add construction code here

}

CTesttiffView::~CTesttiffView()
{
}

BOOL CTesttiffView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTesttiffView drawing

void CTesttiffView::OnDraw(CDC* pDC)
{
	CTesttiffDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here

RECT rect; rect.top = rect.left = 50; rect.bottom = rect.right = 150;

	if (pDoc->pTiff) pDoc->pTiff->Draw(pDC->m_hDC, 0, 0/*, &rect*/);
}

/////////////////////////////////////////////////////////////////////////////
// CTesttiffView printing

BOOL CTesttiffView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTesttiffView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTesttiffView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTesttiffView diagnostics

#ifdef _DEBUG
void CTesttiffView::AssertValid() const
{
	CView::AssertValid();
}

void CTesttiffView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTesttiffDoc* CTesttiffView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTesttiffDoc)));
	return (CTesttiffDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTesttiffView message handlers
