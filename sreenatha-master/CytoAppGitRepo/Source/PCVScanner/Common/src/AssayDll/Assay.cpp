// Assay.cpp: implementation of the CAssay class.
//
//	SN	16Mar04	Added IsRegionProcessing() to check if regions are
//				still being processed.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AITiff.h"
#include "Assay.h"
#include "ScanPassList.h"
#include "ScanPass.h"
#include "RegHelper.h"		
#include "Capable.h"		
#include "AIIShared.h"
#include "SpectraChrome.h"

#include "SpectraChromeList.h"
#include "SerializeXML.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAssay::CAssay(AssayTypeStr assayType, int dbNodeType)
{
	m_Name=_T("Default");
	m_ScanPassList = new CScanPassList;
	m_Modified = FALSE;
	m_version = ASSAYVERSION;
	ZeroMemory(m_assayType, ASSAY_TYPE_LEN);
	if (assayType)
		strncpy(m_assayType, assayType, sizeof(AssayTypeStr));
	m_dbNodeType = dbNodeType;
   
}

CAssay::CAssay()
{
	m_Name=_T("Default");
	m_ScanPassList = new CScanPassList;
	m_Modified = FALSE;
	m_dbNodeType = 0;
	ZeroMemory(m_assayType, ASSAY_TYPE_LEN);
   	m_version = ASSAYVERSION;

}

CAssay::~CAssay()
{
	if (m_ScanPassList)
		delete m_ScanPassList;

}

BOOL CAssay::LoadFromPath(LPCTSTR assays_filename, BOOL bXMLFormat/*=TRUE*/)
{
	
	CSerializeXML assayar(assays_filename, CArchive::load);
	if (XMLSerialize(assayar))
	{
		

		return TRUE;
	}
	return FALSE;
}

BOOL CAssay::SaveToPath(LPCTSTR assays_filename)
{
	CSerializeXML assayar(assays_filename, CArchive::store);
	if (XMLSerialize(assayar))
	{
		m_Modified = FALSE;     // Reset modified once saved
		return TRUE;
	}
	return FALSE;
}

BOOL CAssay::XMLSerialize(CSerializeXML &ar)
{
	CString AssayType = m_assayType;

	XMLCLASSNODENAME("CAssay", ar);
	XMLDATA(m_version);
	XMLDATA(m_Name);
	XMLDATA(AssayType);
	XMLDATA(m_dbNodeType);
	

	m_ScanPassList->XMLSerialize(ar);

	XMLENDNODE;

	strncpy(m_assayType, (LPCTSTR)AssayType, sizeof(m_assayType)); // Maybe should write an XMLDATA for char * null terminated str?

	// If we are reading and old assay need to add path infront of script
	if (!ar.IsStoring() && m_version < ASSAYVERSION)
	{
		
		m_Modified = TRUE;
	}

	return TRUE;
}

// Returns a pointer to a new assay which is a duplicate of this one.
CAssay *CAssay::duplicate()
{
	CAssay *assayPtr;

	if (!this)
	{
		assayPtr = new CAssay();
		return assayPtr;
	}

	assayPtr = new CAssay();

	if (m_assayType)
		strcpy(assayPtr->m_assayType, m_assayType);
	assayPtr->m_dbNodeType = m_dbNodeType;
	assayPtr->m_Name = m_Name;
	assayPtr->m_version = GetVersion();
	delete assayPtr->m_ScanPassList;
	assayPtr->m_ScanPassList = m_ScanPassList->duplicate();

	return assayPtr;
}






//////////////////////////////////////////////////////////////////////////////
// Determines whether the assay is a fluorescent one. A fluorescent one
// is one that contains at least one fluorescent counterstain and has
// at least one fluorescent probe signal in its spectrachrome list.
// Both counterstain and probe must be in the same scan pass.
//////////////////////////////////////////////////////////////////////////////

BOOL CAssay::IsFluorescent(void)
{
	CSpectraChrome		*pSpec;
	CScanPass			*pSP;
	BOOL				HasCounterstain, HasProbe;
	POSITION			PosS, PosC;

	// Go through all scan passes
	PosS = m_ScanPassList->m_list.GetHeadPosition();
	while (PosS)
	{
		pSP = m_ScanPassList->m_list.GetNext(PosS);

		HasCounterstain = FALSE;
		HasProbe = FALSE;

		// Now parse the list of spectrachromes for this scan pass
		PosC = pSP->m_SpectraChromeList->m_list.GetHeadPosition();
		while (PosC)
		{
			pSpec = pSP->m_SpectraChromeList->m_list.GetNext(PosC);

			// Do we have a fluorescent counterstain
			if (pSpec->m_Counterstain && pSpec->m_Fluorescent)
				HasCounterstain = TRUE;
			// Do we have a fluorescent probe signal
			if (!pSpec->m_Counterstain && pSpec->m_Fluorescent)
				HasProbe = TRUE;

			// Got both in the same scan pass so therefore must be a fluorescent assay
			if (HasCounterstain && HasProbe)
				return TRUE;
		}
	}

	return FALSE;
}






