// ScanPass.cpp: implementation of the CScanPass class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "capable.h"
#include "ScanPass.h"
#include "Aitiff.h"
#include "SpectraChromeList.h"
#include "SpectraChrome.h"
#include "RegHelper.h"

#include "ScriptParams.h"
#include "SerializeXML.h"
#include "ScriptDirective.h"
#include "ScriptProc.h"

/*#include "Capable.h"*/		
#include "AIIShared.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

char ALPHANUM[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CScanPass::CScanPass()
{
	m_Script            = new CString;
	*m_Script           = _T("none");
	
	m_ScriptParams      = new CScriptParams;
	
	m_ClassifierName    = _T("Everything");
	m_SpectraChromeList = new CSpectraChromeList;
}

CScanPass::~CScanPass()
{
	delete m_Script;
	delete m_ScriptParams;
	delete m_SpectraChromeList;
}


BOOL CScanPass::LoadFromPath(CString scanpass_filename, BOOL bXMLFormat /* = FALSE */)
{
	CSerializeXML assayar(scanpass_filename, CArchive::load);
	XMLSerialize(assayar);
	return TRUE;
}



BOOL CScanPass::SaveToPath(CString scanpass_filename, BOOL bXMLFormat /*= FALSE */)
{
	CSerializeXML assayar(scanpass_filename, CArchive::store);
	XMLSerialize(assayar);
	return TRUE;		
}


// Returns a pointer to a new scan pass which is a duplicate of this one.
CScanPass *CScanPass::duplicate()
{
	CScanPass *scanPassPtr = new CScanPass;

	*(scanPassPtr->m_Script)         = *m_Script;

    // Make sure lists allocated with the new CScanPass are deleted before
    // duplicating
    delete scanPassPtr->m_ScriptParams;
    delete scanPassPtr->m_SpectraChromeList;

	scanPassPtr->m_ScriptParams      = m_ScriptParams->duplicate();
	scanPassPtr->m_SpectraChromeList = m_SpectraChromeList->duplicate();
	scanPassPtr->m_ClassifierName = m_ClassifierName;

	return scanPassPtr;
}


// return the script name with no .script
CString CScanPass::ShortScriptName()
{
	int i;
	CString script = *m_Script;
	i = script.Find(".script");
	if (i >= 0)
		script = script.Left(i);
	i = script.ReverseFind('\\');
	if (i != -1)
		script = script.Mid(i + 1, script.GetLength());

	return script;
}

// Return a valid script path
CString CScanPass::GetScriptPath()
{
	CString sub_dir;
	int p1;
	CString script, scriptpath, basepath, scriptfullpath;

	basepath = CAII_Shared::ScriptsPath();

	// Strip extraneous path information if there
	p1 = m_Script->ReverseFind('\\');

	if (p1 > 0) {
		script = m_Script->Mid(p1);
		scriptpath = m_Script->Left(p1);

		// May have got script from another sub-directory other
		// than the assay it pertains to, so here extract the sub-dir
		p1 = scriptpath.ReverseFind('\\');
		if (p1 > 0)
			sub_dir = scriptpath.Mid(p1);
		// For instance where the script is in fact located in the 
		// root of the script directory (i.e. //aii_shared/scripts/),
		// we have to identify those, otherwise the "scripts"
		// will be again appended to the basepath (i.e. //aii_shared/scripts/scripts)
		// So, here we will check if sub_dir is "scripts"
		// This may still not 100% bullet prove though.
		if (sub_dir.CompareNoCase("\\Scripts") == 0)
			sub_dir = "";
	} else {
		// What to do ? we don't know what assay it is
		// so we can't even guess the sub-directory
		// best is to return what we had originally
		scriptfullpath = *(m_Script);
		return scriptfullpath;
	}

	// Now assign correct path information
	scriptfullpath.Format("%s%s%s", basepath, sub_dir, script);

	return scriptfullpath;
}

// Is this scan pass completed
BOOL CScanPass::completed()
{
	
		return m_Completed;
}


void CScanPass::complete(BOOL comp)
{
		m_Completed = comp;
}



/////////////////////////////////////////////////////////////////////////////////////////////////
// Determines if the pass is a fluorescent one - ie all its spectrachromes are fluorescent
// spectrachromes
/////////////////////////////////////////////////////////////////////////////////////////////////

bool CScanPass::IsFluorescent(void)
{
    int  NSpecs, FSpecs = 0;
    POSITION P;
    CSpectraChrome *pSpec;

    NSpecs = m_SpectraChromeList->m_list.GetCount();

    P = m_SpectraChromeList->m_list.GetHeadPosition();
    while (P)
    {
        pSpec = m_SpectraChromeList->m_list.GetNext(P);
        if (pSpec->m_Fluorescent)
            FSpecs++;
    }

    return (NSpecs == FSpecs);
}



    
////////////////////////////////////////////////////////////////////////////
// Remove all config variables that are not defined as assay variables.
// Assay variables are not declared at the top of the script so cannot
// be rebuilt directly from the script
////////////////////////////////////////////////////////////////////////////

void CScanPass::RemoveScriptParams(void)
{
    CParamSet *param_set;
    POSITION pos1, pos2;

   for (pos1 = m_ScriptParams->m_ParamSetList.GetHeadPosition(); (pos2 = pos1) != NULL;)
   {
       param_set = m_ScriptParams->m_ParamSetList.GetNext(pos1);
       if (param_set->m_CtrlType != "Assay")
       {
           param_set = m_ScriptParams->m_ParamSetList.GetAt(pos2);  // Save the old pointer for deletion.
           m_ScriptParams->m_ParamSetList.RemoveAt(pos2);
           delete param_set;                                        // Deletion avoids memory leak.
       }
   }
}

////////////////////////////////////////////////////////////////////////////
// Parse a script 'PARAM line 
////////////////////////////////////////////////////////////////////////////

CParamSet *CScanPass::ParseParamLine(CString &line)
{
    int i, k, l;
    CString params_line, sub_string;
    CParamSet *param_set = NULL;

    params_line = line;

	// Trim leading and trailing whitespace
	params_line.Trim();

	// First non-whitespace character must be a comment character
	if (params_line.GetAt(0) != '\'')
		return NULL;

	// Remove comment character and any following whitespace
	params_line.Delete(0, 1);
	params_line.TrimLeft();

	// If this is a Param line, "Param" should now be at the start of the line string.
    if (params_line.Left(strlen("Param ")).CompareNoCase("Param ") == 0)
    {
		// Remove the Param directive from the line string.
        params_line.Delete(0, strlen("Param"));

        param_set = new CParamSet;

        i = 0;
        // Name - a persistent identifier string
        if ( (k = params_line.FindOneOf (ALPHANUM) ) > -1)
        {
            if ((l = params_line.Find (' ' , k) )> -1)
            {
                sub_string = params_line.Mid(k, l-k);
                params_line.Delete (0,l+1);
                params_line.TrimRight();
            }
            else
                sub_string = params_line;
            param_set->m_PersistentName = sub_string;
        }

        // control type - string within single quote marks
        if ((k = params_line.FindOneOf (ALPHANUM)) > -1)
        {
            if ((l = params_line.Find ("'", k) )> -1)
            {
                sub_string = params_line.Mid(k, l-k);
                params_line.Delete (0,l+1);
                params_line.TrimRight();
            }
            else
                sub_string = params_line;
            param_set->m_CtrlType = sub_string;
        }

        //label - string within single quote marks
        if ((k = params_line.FindOneOf (ALPHANUM)) > -1)
        {
            if ((l = params_line.Find ("'", k) )> -1)
            {
                sub_string = params_line.Mid(k, l-k);
                params_line.Delete (0,l+1);
                params_line.TrimRight();
            }
            else
                sub_string = params_line;
            param_set->m_Label = sub_string;
        }

        //range minimum - 
        if ((k = params_line.FindOneOf (ALPHANUM) )> -1)
        {
            if ((l = params_line.Find (' ', k)) > -1)
            {
                sub_string = params_line.Mid(k, l-k);
                params_line.Delete (0,l+1);
                params_line.TrimRight();
            }
            else
                sub_string = params_line;
            sscanf (sub_string,"%d",&(param_set->m_Min).llVal);
            V_VT(&param_set->m_Min) = VT_I4;
        }


        //range maximum -
        if ((k = params_line.FindOneOf (ALPHANUM)) > -1)
        {
            if ((l = params_line.Find (' ', k)) > -1)
            {
                sub_string = params_line.Mid(k, l-k);
                params_line.Delete (0,l+1);
                params_line.TrimRight();
            }
            else 
                sub_string = params_line;
            sscanf (sub_string,"%d",&(param_set->m_Max).llVal);
            V_VT(&param_set->m_Max) = VT_I4;
        }

        //default value -
        if ((k = params_line.FindOneOf (ALPHANUM)) > -1)
        {
            if ((l = params_line.Find (' ', k)) > -1)
            {
                sub_string = params_line.Mid(k, l-k);
                params_line.Delete (0,l+1);
                params_line.TrimRight();
            }
            else 
                sub_string = params_line;

            sscanf (sub_string,"%d",&(param_set->m_Value).llVal);
            V_VT(&param_set->m_Value) = VT_I4;
        }
    }

    return param_set;
}


void CScanPass::buildConfigFromScriptPath(void)
{
    CString		line, params_line;
    CParamSet	*param_set;
    CString		sub_string;
	CScriptParams *pSP;
	CScriptProcessor ScriptProc;
	POSITION P;

    RemoveScriptParams();

	if (ScriptProc.LoadScript(GetScriptPath(), TRUE, TRUE))
	{
		pSP = ScriptProc.BuildConfigFromScript();

		if (pSP)
		{
			P = pSP->m_ParamSetList.GetHeadPosition();
			while (P)
			{
				param_set = pSP->m_ParamSetList.GetNext(P);
                m_ScriptParams->m_ParamSetList.AddTail(param_set->duplicate());
			}

			delete pSP;
		}
    }
}

void CScanPass::XMLSerialize(CSerializeXML & ar)
{
		int m_version = SCANPASSVERSION;

		CString m_ScriptName = *m_Script;	// Basically bad name for xml
	
		XMLCLASSNODENAME("CScanPass", ar);	
		XMLDATA(m_version);
		XMLDATA(m_ScriptName);
		XMLDATA(m_ClassifierName);

		if (m_ClassifierName == _T(""))
			m_ClassifierName = _T("Everything");

		m_ScriptParams->XMLSerialize(ar);
		m_SpectraChromeList->XMLSerialize(ar);
		

		XMLENDNODE;

	
		*m_Script = m_ScriptName; // naming problem

        
}



//////////////////////////////////////////////////////////////////////////////////////
// Get the spectrachrome at a specific index in the spectrachrome list
// Returns NULL if invalid index or non exists
//////////////////////////////////////////////////////////////////////////////////////

CSpectraChrome *CScanPass::GetSpectrachrome(int Index) // Get the spectrachrome at a zero based index in the spec list
{
	CSpectraChrome *pSpec = NULL;
	POSITION		P;

	P = m_SpectraChromeList->m_list.FindIndex(Index);
	if (P)
		pSpec = m_SpectraChromeList->m_list.GetAt(P);

	return pSpec;
}





void CScanPass::ReplaceMachineName()
{
	CString script;
	
	script = (*m_Script);
	script.MakeUpper();
	int i = script.Find(_T("AII_SHARED"));
	if (i != -1)
		(*m_Script) = (*m_Script).Mid(i, (*m_Script).GetLength());
	*m_Script = CAII_Shared::CaseBase() + _T("\\") + (*m_Script);
}
