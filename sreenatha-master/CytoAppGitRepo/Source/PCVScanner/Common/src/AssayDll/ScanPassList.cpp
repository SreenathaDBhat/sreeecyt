// ScanPassList.cpp: implementation of the CScanPassList class.
//
//////////////////////////////////////////////////////////////////////

#include "AIIShared.h"
#include "Capable.h"		
#include "Assay.h"
#include "stdafx.h"
#include "ScanPassList.h"
#include "SpectrachromeList.h"
#include "ScanPass.h"

#include "SerializeXML.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define VERSION 1

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CScanPassList::CScanPassList()
{
	m_list.RemoveAll();
	m_currentPos = NULL;

}

CScanPassList::~CScanPassList()
{
	RemoveAll();
}

void CScanPassList::RemoveAll()
{
	POSITION pos = m_list.GetHeadPosition();
	CScanPass* sp;
	while (pos)
	{
		sp = m_list.GetNext(pos);
		delete sp;
	}
	m_list.RemoveAll();
}

void CScanPassList::XMLSerialize(CSerializeXML &ar)
{
	m_version = ASSAYVERSION;

	XMLCLASSNODENAME("CScanPassList", ar);
	XMLDATA(m_version);
	if (ar.IsStoring())
	{
		POSITION pos=m_list.GetHeadPosition();
		while (pos != NULL)
			m_list.GetNext(pos)->XMLSerialize(ar);
	}
	else
	{
		RemoveAll();
		XMLSTARTLISTDATA("CScanPass");
		CScanPass *sp = new CScanPass;
		sp->XMLSerialize(ar);
		m_list.AddTail(sp);
		XMLENDLISTDATA;
	}

	XMLENDNODE;
}



// Returns a pointer to a new scan pass which is a duplicate of this one.
CScanPassList *CScanPassList::duplicate()
{
	CScanPassList *scanPassListPtr = new CScanPassList;
	POSITION pos;


	pos=m_list.GetHeadPosition();
	while (pos != NULL)
		scanPassListPtr->m_list.AddTail(m_list.GetNext(pos)->duplicate());

	return scanPassListPtr;

}

// Are all passes in this list complete
BOOL CScanPassList::complete()
{
	POSITION pos;

	pos=m_list.GetHeadPosition();
	while (pos != NULL)
	{
		CScanPass *sp = m_list.GetNext(pos);
		if (!sp->completed())
			return FALSE;
	}

	return TRUE;

}

CScanPass * CScanPassList::GetScanPass(int passno)
{
	POSITION pos;
	int pass;
	CScanPass *sp = NULL;

	// Get scan pass object corresponding to supplied scan pass
	pos = m_list.GetHeadPosition();
	for (pass = 0; (pass <= passno) && pos; pass++)
		sp = m_list.GetNext(pos);

	if (pass <= passno)
		return NULL;	// Could not get the specified pass - return NULL
	else
		return sp;		// Return the found pass
}

int CScanPassList::GetPassNumber(CScanPass* thisPass)
{
	POSITION pos;
	int pass=0;

	pos=m_list.GetHeadPosition();
	while (pos != NULL)
	{
		CScanPass *sp = m_list.GetNext(pos);
		if (sp == thisPass)
			return pass;
		pass++;
	}

	return -1;

}


BOOL CScanPassList::RemovePassFromList(CScanPass* thisPass)
{
	BOOL success=FALSE;
	CScanPass* sp = NULL;
	POSITION pos;

	for(pos = m_list.GetHeadPosition(); pos != NULL; )
	{
		sp = m_list.GetAt(pos);
		if (sp == thisPass)
		{
			m_list.RemoveAt(pos);

			success = TRUE;
			break;
		}
		else
			m_list.GetNext(pos);
	}

	return success;	
}


POSITION CScanPassList::ReplacePass(POSITION pos, CScanPass* new_pass)
{
	POSITION new_pos=NULL;
	if (new_pass && pos)
	{
		new_pos = m_list.InsertAfter(pos, new_pass);
		m_list.RemoveAt(pos);
	}
	return new_pos;
}



