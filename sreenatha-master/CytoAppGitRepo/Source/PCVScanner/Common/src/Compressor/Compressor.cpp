// This is the main DLL file.

#include "stdafx.h"

#include <stdio.h>
#include <string>
#include <vector>

#include "interface.h"

#include "util.h"
#include "defaulttraits.h"
#include "losslesstraits.h"
#include "colortransform.h"
#include "streams.h"
#include "processline.h"


#include "Compressor.h"

bool Compressor::ReadFile(const char *strName, std::vector<BYTE>* pvec)
{

	FILE* pfile = fopen(strName, "rb");
	if( !pfile ) 
	{
		fprintf( stderr, "Could not open %s\n", strName );
		return false;
	}

	fseek(pfile, 0, SEEK_END);	
	int cbyteFile = ftell(pfile);
	pvec->resize(cbyteFile);
	fseek(pfile, 0, SEEK_SET);
	fread(&(*pvec)[0],1, pvec->size(), pfile);
	fclose(pfile);
	return true;
}


void Compressor::WriteFile(const char *strName, std::vector<BYTE>& vec, int Sz)
{
	FILE* pfile = fopen(strName, "w+b");
	if( !pfile ) 
	{
		fprintf( stderr, "Could not open %s\n", strName );
		return;
	}

	fwrite(&vec[0],1, Sz, pfile);

	fclose(pfile);
}


// Compression and save
void Compressor::CompressSave(const char *strName, void *rgbyteRaw, int Width, int Height, int Bits, bool Lossy)
{
	std::vector<BYTE> rgbyteCompressed;
	rgbyteCompressed.resize(Width * Height * Bits / 4);

	long Size = Width * Height * ((Bits + 7) / 8);

	JlsParameters params = JlsParameters();
	params.components = 1;
	params.bitspersample = Bits;
	params.height = Height;
	params.width = Width;

	if (Lossy == true)
		params.allowedlossyerror = 1;
	else
		params.allowedlossyerror = 0;

	size_t cbyteCompressed;

	JLS_ERROR err = JpegLsEncode(&rgbyteCompressed[0], rgbyteCompressed.size(), &cbyteCompressed, rgbyteRaw, Size, &params);
	
	WriteFile(strName, rgbyteCompressed, cbyteCompressed);
}

// Lossless JPEG Decompression
bool Compressor::DecompressAndLoad(const char *strName, std::vector<BYTE>& rgbyteOut, int &W, int &H, int &Bits)
{
	std::vector<BYTE> rgbyteFile;
	if (!ReadFile(strName, &rgbyteFile))
		return false;

	JlsParameters metadata;

	if (JpegLsReadHeader(&rgbyteFile[0], rgbyteFile.size(), &metadata) != OK)	
		return false;

	W = metadata.width;
	H = metadata.height;
	Bits = metadata.bitspersample;

	long Size = W * H * ((Bits + 7) / 8);
	rgbyteOut.resize(Size);

	JLS_ERROR err = JpegLsDecode(&rgbyteOut[0], rgbyteOut.size(), &rgbyteFile[0], rgbyteFile.size());

	return (err == OK);
}

// Lossy JPEG LS Compression
void Compressor::LossyCompressAndSave(const char *strName, void *rgbyteRaw, int Width, int Height, int Bits)
{
	CompressSave(strName, rgbyteRaw, Width, Height, Bits, true);
}
// Lossless JPEG LS Compression
void Compressor::CompressAndSave(const char *strName, void *rgbyteRaw, int Width, int Height, int Bits)
{
	CompressSave(strName, rgbyteRaw, Width, Height, Bits, false);
}



