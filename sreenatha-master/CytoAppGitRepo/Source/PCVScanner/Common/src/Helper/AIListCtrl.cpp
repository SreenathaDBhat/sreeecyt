//
// AIListCtrl.cpp
// JMB October 2003
//
// A list control, intended to be used in 'report view', which allows each
// sub-item to be configured as read-only, editable, or modifyable via a
// combo box. Pretty cool, huh?
//
// This control is intended for creation with the following styles and extended styles:
// LVS_REPORT
// LVS_EX_FULLROWSELECT
//
// ...but WITHOUT the following styles and extended styles:
// LVS_EDITLABELS
//

#include "stdafx.h"

#include "AIListCtrl.h"


// TODO: are these IDs ok?
#define IDC_AILIST_EDIT   103
#define IDC_AILIST_COMBO  104


//////////////////////////////////////////////////////////////////////////////
//CAIListCtrlEdit

BEGIN_MESSAGE_MAP(CAIListCtrlEdit, CEdit)
	ON_WM_GETDLGCODE()
	ON_WM_KEYDOWN()
	ON_WM_KILLFOCUS()
END_MESSAGE_MAP()

BOOL CAIListCtrlEdit::Create(const RECT &rect, CAIListCtrl *pParent, UINT nID,
                             int listItemIndex, int listSubItemIndex)
{
	BOOL status = FALSE;

	this->pAIListCtrl      = pParent;
	this->listItemIndex    = listItemIndex;
	this->listSubItemIndex = listSubItemIndex;

	if (CEdit::Create(WS_CHILD | WS_VISIBLE | WS_BORDER, rect, (CWnd*)pParent, nID))
	{
		// Set same font as list control
		SetFont(pParent->GetFont(), FALSE);

		// Get the text and set it
		CString text = pParent->GetItemText(listItemIndex, listSubItemIndex);

		SetWindowText((PCSTR)text);
		SetReadOnly(FALSE);
		pParent->SetFocus();
		// Must set focus to the edit control as losing focus causes it to
		// be destroyed, but it can't lose focus if it didn't have it in the first place!
		SetFocus(); 
		SetSel(0, -1); // Select the text so that typing will overwite it (like Windows Explorer)
		//SetSel(0, 0);	// Set 1st char position
		//SetLimitText(

		status = TRUE;
	}

	return status;
}

UINT CAIListCtrlEdit::OnGetDlgCode()
{
	// TODO: Add your message handler code here and/or call default

	// Process all keys internally to this control (in particular, the Return and Esc keys).
	return DLGC_WANTALLKEYS;   
	//return CEdit::OnGetDlgCode();
}

void CAIListCtrlEdit::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default

	if (nChar == VK_RETURN) // Accept
	{
		pAIListCtrl->SetFocus();
	}
	else if (nChar == VK_ESCAPE) // Cancel
	{
		DestroyWindow();
	}


	CEdit::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CAIListCtrlEdit::OnKillFocus(CWnd* pNewWnd)
{
// If the edit control loses the focus, copy its contents to the list subitem,
// then destroy it.

	CEdit::OnKillFocus(pNewWnd);

	// TODO: Add your message handler code here

	if (pAIListCtrl && listItemIndex >= 0 && listSubItemIndex >= 0)
	{
		// Copy the text in the edit control to the underlying list sub item
		CString text;
		GetWindowText(text);
		pAIListCtrl->SetItemText(listItemIndex, listSubItemIndex, (PCSTR)text);

		listItemIndex    = -1;
		listSubItemIndex = -1;
	}

	DestroyWindow();
}



//////////////////////////////////////////////////////////////////////////////
//CAIListCtrlCombo

BEGIN_MESSAGE_MAP(CAIListCtrlCombo, CComboBox)
	ON_WM_GETDLGCODE()
	ON_WM_KEYDOWN()
	ON_CONTROL_REFLECT(CBN_KILLFOCUS, OnCbnKillFocus)
END_MESSAGE_MAP()

BOOL CAIListCtrlCombo::Create(const RECT &rect, CAIListCtrl *pParent, UINT nID,
                              int listItemIndex, int listSubItemIndex,
                              CStringArray *pStrings, BOOL editable/*=TRUE*/)
{
	BOOL status = FALSE;

	this->pAIListCtrl      = pParent;
	this->listItemIndex    = listItemIndex;
	this->listSubItemIndex = listSubItemIndex;

	DWORD style = WS_CHILD|WS_VISIBLE|WS_BORDER;
	if (editable)
		style |= CBS_DROPDOWN|CBS_AUTOHSCROLL;
	else
		style |= CBS_DROPDOWNLIST;

	if (CComboBox::Create(style, rect, (CWnd*)pParent, nID))
	{
		// Set same font as parent list control
		SetFont(pParent->GetFont(), FALSE);

		// Add the strings
		if (pStrings)             
		{
			for (int i = 0; i < pStrings->GetCount(); i++)
			{
				AddString(pStrings->GetAt(i));
			}
		}

		// Get the text from the underlying list item and set it in the combo
		CString text = pParent->GetItemText(listItemIndex, listSubItemIndex);

		if (editable)
		{
			SetWindowText((PCSTR)text);
			//SetReadOnly(FALSE);
			SetEditSel(0, -1); // Select the text so that typing will overwite it
			//SetSel(0, 0);	// Set 1st char position
			//LimitText(
		}
		else
		{	// For CBS_DROPDOWNLIST style, text must exist in list.
			SelectString(-1, (PCSTR)text);
		}

		//pParent->SetFocus();
		// Must set focus to the combo box as losing focus causes it to
		// be destroyed, but it can't lose focus if it didn't have it in the first place!
		SetFocus();

		status = TRUE;
	}

	return status;
}

UINT CAIListCtrlCombo::OnGetDlgCode()
{
	// TODO: Add your message handler code here and/or call default

	// Process all keys internally to this control (in particular, the Return and Esc keys).
	return DLGC_WANTALLKEYS;   
	//return CCombobox::OnGetDlgCode();
}

void CAIListCtrlCombo::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default

	if (nChar == VK_RETURN) // Accept
	{
		pAIListCtrl->SetFocus();
	}
	else if (nChar == VK_ESCAPE) // Cancel
	{
		DestroyWindow();
	}

	CComboBox::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CAIListCtrlCombo::OnCbnKillFocus()
{
// If the combo box loses the focus, copy its contents to the list subitem,
// then destroy it. Note that this function is a handler for reflected
// CBN_KILLFOCUS messages, rather than WM_KILLFOCUS. This is because a combo
// box (at least one with the CBS_DROPDOWN style) has some child controls
// within it (an edit control and a listbox). If one of the child controls
// has the focus, then loses it, WM_KILLFOCUS will be sent to the child
// control, but NOT to the CComboBox, however CBN_KILLFOCUS is sent even if
// it is one of the child controls that has lost focus.
	if (pAIListCtrl && listItemIndex >= 0 && listSubItemIndex >= 0)
	{
		// Copy the text in the combo box to the underlying list sub item
		CString text;
		GetWindowText(text);
		pAIListCtrl->SetItemText(listItemIndex, listSubItemIndex, (PCSTR)text);

		listItemIndex    = -1;
		listSubItemIndex = -1;
	}

	DestroyWindow();
}



//////////////////////////////////////////////////////////////////////////////
// AIItemData and AISubItemData are kept out of header file in order to hide
// implementation details of CAIListCtrl.

class AISubItemData
{
public:
	BOOL editable;
	CStringArray comboStrings;
	AISubItemData(){ editable = FALSE; }
};

class AIItemData
{
public:
	DWORD_PTR data;
	CArray<AISubItemData> subItemData;
	AIItemData(){ data = 0; }
};

static AIItemData *GetAIItemData(CAIListCtrl *pAIListCtrl, int nItem)
{
	AIItemData *pAIItemData = NULL;

	if (nItem >= 0 && nItem < pAIListCtrl->GetItemCount())
	{
		pAIItemData = (AIItemData*)((CListCtrl*)pAIListCtrl)->GetItemData(nItem);
	}

	return pAIItemData;
}

static AISubItemData *GetAISubItemData(CAIListCtrl *pAIListCtrl, int nItem, int nSubItem)
{
	AISubItemData *pAISubItemData = NULL;

	AIItemData *pAIItemData = GetAIItemData(pAIListCtrl, nItem);

	if (pAIItemData && nSubItem >= 0)
	{
		if (pAIItemData->subItemData.GetSize() < (nSubItem + 1))
			pAIItemData->subItemData.SetSize(nSubItem + 1);

		pAISubItemData = (pAIItemData->subItemData.GetData()) + nSubItem;
	}

	return pAISubItemData;
}

//////////////////////////////////////////////////////////////////////////////


CAIListCtrl::CAIListCtrl()
{
}

CAIListCtrl::~CAIListCtrl()
{
}


int CAIListCtrl::InsertItem(const LVITEM* pItem)
{
	int retVal = -1;

	int newIndex = CListCtrl::InsertItem(pItem);

	if (newIndex >= 0)
	{
		AIItemData *pItemData = new AIItemData;
		if (pItemData)
		{
			if (CListCtrl::SetItemData(newIndex, (DWORD_PTR)pItemData))
				retVal = newIndex;
		}

		if (retVal != newIndex)
			CListCtrl::DeleteItem(newIndex);
	}

	return retVal;
}

int CAIListCtrl::InsertItem(UINT nMask, int nItem, LPCTSTR lpszItem, UINT nState, UINT nStateMask,
                            int nImage, LPARAM lParam)
{
	int revVal = -1;

	LVITEM item;
	item.mask      = nMask;
	item.iItem     = nItem;
	item.iSubItem  = 0;
	item.pszText   = (LPTSTR)lpszItem;
	item.state     = nState;
	item.stateMask = nStateMask;
	item.iImage    = nImage;
	item.lParam    = lParam;

	return InsertItem(&item);
}


BOOL CAIListCtrl::DeleteItem(int nItem)
{
	AIItemData *pItemData = GetAIItemData(this, nItem);

	if (pItemData)
		delete pItemData;

	return CListCtrl::DeleteItem(nItem);
}

BOOL CAIListCtrl::DeleteAllItems()
{
	for (int i = 0; i < GetItemCount(); i++)
	{
		AIItemData *pItemData = GetAIItemData(this, i);
		if (pItemData)
		{
			if (CListCtrl::SetItemData(i, NULL))
				delete pItemData;
		}
	}

	return CListCtrl::DeleteAllItems();
}


BOOL CAIListCtrl::SetItemData(int nItem, DWORD_PTR dwData)
{
	BOOL status = FALSE;

	AIItemData *pItemData = GetAIItemData(this, nItem);
	if (pItemData)
	{
		pItemData->data = dwData;
		status = TRUE;
	}

	return status;
}

DWORD_PTR CAIListCtrl::GetItemData(int nItem)
{
	DWORD_PTR data = NULL;

	AIItemData *pItemData = GetAIItemData(this, nItem);

	if (pItemData)
		data = pItemData->data;

	return data;
}


BOOL CAIListCtrl::SetEditableItem(int nItem, int nSubItem, LPCTSTR lpszText)
{
	BOOL status = FALSE;

	AISubItemData *pSubItemData = GetAISubItemData(this, nItem, nSubItem);

	if (pSubItemData)
	{
		pSubItemData->editable = TRUE;

		status = SetItemText(nItem, nSubItem, lpszText);
	}

	return status;
}

BOOL CAIListCtrl::SetComboItem(int nItem, int nSubItem, LPCTSTR lpszText,
                               CStringArray *pStringArray, BOOL editable/*=TRUE*/)
{
	BOOL status = FALSE;

	AISubItemData *pSubItemData = GetAISubItemData(this, nItem, nSubItem);

	if (pSubItemData)
	{
		pSubItemData->comboStrings.Copy(*pStringArray);
		pSubItemData->editable = editable;

		status = SetItemText(nItem, nSubItem, lpszText);
	}

	return status;
}


BOOL CAIListCtrl::BeginEdit(int nItem, int nSubItem)
{
	BOOL status = FALSE;

	// End any prevous edit or combo - loss of focus will make control destroy itself
	SetFocus();

	// Note: use LVIR_LABEL rather than LVIR_BOUNDS with GetSubItemRect(),
	// else for subitem zero the width of the rect will correspond with
	// that of the the entire row, not just the subitem.
	CRect rect;

	if (GetSubItemRect(nItem, nSubItem, LVIR_LABEL, rect))
	{
		// Create new edit field for sub-item text
		// Ensure edit control including border is large enough for font
		rect.top    -= 2;
		rect.bottom += 1;

		if (editCtrl.Create(rect, this, IDC_AILIST_EDIT, nItem, nSubItem))
			status = TRUE;
	}

	return status;
}

BOOL CAIListCtrl::BeginCombo(int nItem, int nSubItem, CStringArray *pStringArray, BOOL editable/*=TRUE*/)
{
	BOOL status = FALSE;

	// End any prevous edit or combo - loss of focus will make control destroy itself
	SetFocus();

	// Note: use LVIR_LABEL rather than LVIR_BOUNDS with GetSubItemRect(),
	// else for subitem zero the width of the rect will correspond with
	// that of the the entire row, not just the subitem.
	CRect rect;

	if (GetSubItemRect(nItem, nSubItem, LVIR_LABEL, rect))
	{
		if (pStringArray && pStringArray->GetCount() > 0)
		{
			// Note that the height of the combo box is its 'dropped down' height.
			rect.bottom += 50;
			if (comboBox.Create(rect, this, IDC_AILIST_COMBO, nItem, nSubItem, pStringArray, editable))
				status = TRUE;
		}
	}

	return status;
}


BEGIN_MESSAGE_MAP(CAIListCtrl, CListCtrl)
	ON_WM_DESTROY()
	ON_NOTIFY_REFLECT_EX(NM_CLICK, OnClick)
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CAIListCtrl message handlers

void CAIListCtrl::OnDestroy()
{
// Must clean up any data pointed to by items in the list here, rather than in
// the destructor, as by the time the destructor is called, the list control
// won't exist any more.
	DeleteAllItems();

	CListCtrl::OnDestroy();
}


BOOL CAIListCtrl::OnClick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// My control notification handler code here

	BOOL doNotNotifyParent = FALSE;

	// Ensure selection highlights extend across all sub-items.
	SetExtendedStyle(GetExtendedStyle() | LVS_EX_FULLROWSELECT);


	NMITEMACTIVATE *pClickData = (NMITEMACTIVATE*)pNMHDR;


	LVHITTESTINFO hitTestInfo;

	hitTestInfo.pt = pClickData->ptAction;

	if (SubItemHitTest(&hitTestInfo) >= 0)
	{
		AISubItemData *pSubItemData = GetAISubItemData(this, hitTestInfo.iItem, hitTestInfo.iSubItem);

		if (pSubItemData)
		{
			if (pSubItemData->comboStrings.GetCount() > 0)
			{
				BeginCombo(hitTestInfo.iItem, hitTestInfo.iSubItem, &pSubItemData->comboStrings, pSubItemData->editable);
			}
			else if (pSubItemData->editable)
			{
				BeginEdit(hitTestInfo.iItem, hitTestInfo.iSubItem);
			}
		}
	}
	
	*pResult = 0;

	return doNotNotifyParent;
}


void CAIListCtrl::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// My message handler code here then call default

	// Edit control is not scrollable, so must end it if list is 
	// scrolled, else it will not be drawn in the right place.
	// Setting focus to the list will take the focus from any edit control,
	// which will cause it to destroy itself.
	// Note this will have the same effect on any combo box, which is
	// constistent but not entirely neccessary as those seem to scroll ok.
	SetFocus();
	
	CListCtrl::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CAIListCtrl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// My message handler code here then call default

	// Edit control is not scrollable, so must end it if list is 
	// scrolled, else it will not be drawn in the right place.
	// Setting focus to the list will take the focus from the edit control,
	// which will cause it to destroy itself.
	// Note this will have the same effect on any combo box, which is
	// constistent but not entirely neccessary as those seem to scroll ok.
	SetFocus();
	
	CListCtrl::OnHScroll(nSBCode, nPos, pScrollBar);
}


