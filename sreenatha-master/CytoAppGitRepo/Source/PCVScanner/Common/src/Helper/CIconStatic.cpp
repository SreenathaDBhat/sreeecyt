//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Icon stuff
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CIconStatic.h"
#include "..\..\include\ciconstatic.h"

CIconStatic::CIconStatic(UINT IconId)
{
	Load(IconId);
}

CIconStatic::CIconStatic(void)
{
	m_hIcon = NULL;
}

CIconStatic::~CIconStatic()
{
	if (m_hIcon)
		DestroyIcon((HICON) m_hIcon);
}

BEGIN_MESSAGE_MAP(CIconStatic, CWnd)
    ON_WM_PAINT()
END_MESSAGE_MAP()

void CIconStatic::Draw(int x, int y, CDC *pDC)
{
	pDC->DrawIcon(x, y,(HICON) m_hIcon);
}

void CIconStatic::Render(void)
{
	CDC *pDC = GetDC();
	Draw(0, 0, pDC);
	ReleaseDC(pDC);
}

void CIconStatic::Load(UINT IconId)
{
	if (m_hIcon)
	{
		DestroyIcon((HICON) m_hIcon);
		m_hIcon = NULL;
	}

	m_hIcon = LoadImage(AfxGetInstanceHandle(), 
							MAKEINTRESOURCE(IconId),
							IMAGE_ICON,
							32,
							32, 
							LR_SHARED);

	ASSERT(m_hIcon);

	ICONINFO IIF;

    GetIconInfo((HICON) m_hIcon, &IIF);

	m_Hot.x = IIF.xHotspot;
	m_Hot.y = IIF.yHotspot;

	DeleteObject(IIF.hbmMask);
	DeleteObject(IIF.hbmColor);
}


afx_msg void CIconStatic::OnPaint()
{
	CPaintDC dc(this);

	Draw(0, 0, &dc);
}



