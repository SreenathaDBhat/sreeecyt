#include "stdafx.h"
#include "ColControls.h"

////////////////////////////////////////////////////////////////////////////
//	These define DDGS colours
//	Usually defined in common.h, but this includes
//	wstruct.h & canvas.h which aren't nneded
//

////////////////////////////////////////////////////////////////////////////
// Color Slider Control mesage map and member functions
// User should define a slider as:	CColorSlider slider;
//						instead of:	CSliderCtrl slider;
//
// This can be accomplished by after dialog design by modifying 
// the header file of the dialog classes created by ClassWizard
//
// Only the slider background can be modified with SetBgColor()

//--------------------------------------------------------------------------
// Message map
BEGIN_MESSAGE_MAP (CColorSlider, CSliderCtrl)
	ON_WM_CTLCOLOR_REFLECT ()
	ON_WM_MBUTTONDOWN()
	ON_WM_MBUTTONUP()
END_MESSAGE_MAP ()

//--------------------------------------------------------------------------
// Constructor
CColorSlider::CColorSlider()
{
	m_BgColor = ::GetSysColor(COLOR_3DFACE);
	m_BgBrush.CreateSolidBrush(m_BgColor);
}

//--------------------------------------------------------------------------
// Set the background color e.g. slider.SetBgColor(RGB(255,0,0));
void CColorSlider::SetBgColor(COLORREF color)
{
	m_BgColor = color;
	m_BgBrush.DeleteObject();
	m_BgBrush.CreateSolidBrush(color);
	Invalidate();
}

//--------------------------------------------------------------------------
// Control color message handler
HBRUSH CColorSlider::CtlColor(CDC *pDC, UINT nCtlColor)
{
	pDC->SetBkColor(m_BgColor);
	return (HBRUSH) m_BgBrush;
}

//--------------------------------------------------------------------------

void CColorSlider::OnMButtonDown(UINT nFlags, CPoint point)
{
	UINT pos;
	int min, max;
	UINT w_height, w_width;
	RECT rect;
	int OFFSET=7;

	if ( (GetWindowLong(m_hWnd, GWL_STYLE)) & TBS_VERT ) {
		GetRange(min, max);
		GetWindowRect(&rect);
		if (rect.bottom>rect.top)
			w_height = (rect.bottom-OFFSET)-(rect.top+OFFSET);
		else
			w_height = (rect.top-OFFSET)-(rect.bottom+OFFSET);
		pos = min + (UINT)( ((max-min)*point.y)/w_height );
		GetParent()->SendMessage(WM_VSCROLL, (SB_THUMBTRACK)|(pos<<16), (long)m_hWnd);
		SetPos(pos);
	}
	else {
		GetRange(min, max);
		GetWindowRect(&rect);
		if (rect.left>rect.right)
			w_width = (rect.left-OFFSET)-(rect.right+OFFSET);
		else
			w_width = (rect.right-OFFSET)-(rect.left+OFFSET);
		pos = min + (UINT)( ((max-min)*point.x)/w_width );
		GetParent()->SendMessage(WM_HSCROLL, (SB_THUMBTRACK)|(pos<<16), (long)m_hWnd);
		SetPos(pos);
	}
}

//--------------------------------------------------------------------------

void CColorSlider::OnMButtonUp(UINT nFlags, CPoint point)
{
	NMHDR Nmhdr;

	OnMButtonDown(nFlags, point);

				//send notify message to parent signalling the middle mouse button has been released 
	Nmhdr.hwndFrom = m_hWnd;
	Nmhdr.idFrom = GetDlgCtrlID();
	Nmhdr.code = NM_RELEASEDCAPTURE; 
	GetParent()->SendMessage(WM_NOTIFY,(WPARAM)Nmhdr.idFrom, (long)(&Nmhdr));
}


