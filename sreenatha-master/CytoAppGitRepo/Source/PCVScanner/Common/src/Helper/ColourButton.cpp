// ColourButton.cpp : implementation file
//

#include "stdafx.h"
#include "ColourButton.h"


// CColourButton

IMPLEMENT_DYNAMIC(CColourButton, CButton)
CColourButton::CColourButton()
{
    m_Colour = 0xFFFFFF;
}

CColourButton::~CColourButton()
{
}


BEGIN_MESSAGE_MAP(CColourButton, CButton)
END_MESSAGE_MAP()



// CColourButton message handlers


void CColourButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
    CRect       rc;
    CString     Caption;


    CDC * pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
   
    GetClientRect(&rc);

    ::DrawEdge(lpDrawItemStruct->hDC, &rc, EDGE_RAISED, BF_ADJUST | BF_RECT | BF_SOFT);
    pDC->FillSolidRect(rc, m_Colour);

    GetWindowText(Caption);
    UINT oa = pDC->SetTextAlign(TA_CENTER | TA_BASELINE);
    pDC->SetTextColor(0xFFFFFF - m_Colour);
    pDC->TextOut((rc.left + rc.right)/2, (rc.top + rc.bottom)/2, Caption);
 
}

    
void CColourButton::SetColour(DWORD NewCol)
{
    m_Colour = NewCol;

    CRect       rc;

    GetClientRect(&rc);
    InvalidateRect(&rc, TRUE);
}


BOOL CColourButton::Create(LPCTSTR lpszCaption, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID)
{
    // Just in case its not turned on - turn it on
    dwStyle |= BS_OWNERDRAW;

    return CButton::Create(lpszCaption, dwStyle, rect, pParentWnd, nID);
}
