/*
 *		C O N V E R S I O N .C P P	 
 *
 *		Transformation of stage coordinates from one coordinate system to another.
 *
 *		The transformation matrices are represented in 4 x 4 homogeneous form *
 *				__								__	
 *				|	A		B		C	:	P	|
 *				|	D		E		F	 :  Q   |
 *				|	G		H		I	 :	 R	 |
 *				|	---------------- :
 *				|	L		M		N  :	S  |	
 *				__								__
 *
 *		Where :
 *				the sub-matrix elements A - I are responsible for scaling, mirroring, rotation and shearing of 3-D objects;
 *				the sub-matrix elements L - N are responsible for linear translations;
 *				the sub-matrix elements P- S allow transformations to be combined in multiplication form 
 *				(P, Q and R = 0, while S = 1).
 *  
 *
 *		
 *		
 *		Author	 		:	A Rourke
 *		Date		    :	14 Frebruary 2001
 *		Copyright	:	Applied Imaging
 *
 *	Mods:
 *	27Mar2003	JMB	Added CreateEFString() and ParseEFString().
 */
#include <windows.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <malloc.h>
#include <memory.h>
#include <string.h>

#include "MicroErrors.h"
#include "conversion.h"

#define	X	1
#define Y	2
#define Z	3
#define PI	3.141592654

#define IDEAL_POINTS 4

char *Ualphabet = "ABCDEFGHJKLMNOPQRSTUVWXYZ";  /*England Finder doesn't have an I coordinate*/

			//Measured England Finder positions in Ideal coordinates 
#define F40x	6050.0
#define F40y	36200.0
#define U40x	19700.0
#define U40y	36200.0
#define U13x	19700.0
#define U13y	62400.0
#define F13x	6050.0
#define F13y	62300.0	

#define EF_DIAM		0.620	/*ratio of diameter of circle to length of side of square*/


//Constructor
CConversion::CConversion()
{

			//initialise matrices
	StageToIdeal.Mat = NULL;
	IdealToStage.Mat = NULL;
	Stage.Mat = NULL;
	Ideal.Mat = NULL;
	StageToEF.Mat = NULL;
	EFToStage. Mat = NULL;
	EF.Mat = NULL;
	EFToIdeal.Mat = NULL;
	IdealToEF.Mat = NULL;

	CreateMatrices();

	matrix.identity_matrix (&IdealToStage);
	matrix.identity_matrix (&StageToIdeal);

}


//Destructor
CConversion::~CConversion()
{
	 if (StageToIdeal.Mat != NULL)
		matrix.free_admatrix (StageToIdeal.Mat,1,(long)StageToIdeal.rows,(long)1,(long)StageToIdeal.cols);

	if (IdealToStage.Mat != NULL)
		matrix.free_admatrix (IdealToStage.Mat,1,(long)IdealToStage.rows,(long)1,(long)IdealToStage.cols);

	if (Stage.Mat != NULL)
		matrix.free_admatrix (Stage.Mat,1,(long)Stage.rows,(long)1,(long)Stage.cols);
	
	if (Ideal.Mat != NULL)
		matrix.free_admatrix (Ideal.Mat,1,(long)Ideal.rows,(long)1,(long)Ideal.cols);

	if (StageToEF.Mat != NULL)
		matrix.free_admatrix (StageToEF.Mat,1,(long) StageToEF.rows, (long) 1, (long) StageToEF.cols);

	if (EFToStage.Mat != NULL)
		matrix.free_admatrix (EFToStage.Mat,1, (long) EFToStage.rows, (long) 1, (long) EFToStage.cols);

	if (EF.Mat != NULL)
		matrix.free_admatrix (EF.Mat,1,(long) EF.rows, (long) 1, (long) EF.cols);

	if (EFToIdeal.Mat != NULL)
		matrix.free_admatrix (EFToIdeal.Mat,1, (long) EFToIdeal.rows, (long) 1, (long) EFToIdeal.cols);

	if (IdealToEF.Mat != NULL)
		matrix.free_admatrix (IdealToEF.Mat,1, (long) IdealToEF.rows, (long) 1, (long) IdealToEF.cols);


			//home directions for stage, defines position of stage origin wrt slides
	X_Home_Dir = 0;
	Y_Home_Dir = 0;
	X_Pos_Dir = 1;
	Y_Pos_Dir = 1;
}

/*
 *		IdealToStage (int)
 *			-- convert ideal coordinates to stage coordinates
 */
void CConversion::Ideal_To_Stage (int *x, int *y, int *z)
{

	Ideal.Mat[1][X] = (double) *x;
	Ideal.Mat[1][Y] = (double) *y;
	Ideal.Mat[1][Z] = (double) *z;
	Ideal.Mat[1][4] = 1.0;

	matrix.matrix_multiply (&Ideal,&IdealToStage, &Stage);

	*x = (int) Stage.Mat[1][X];
	*y = (int) Stage.Mat[1][Y];
	*z = (int) Stage.Mat[1][Z];
}

/*
 *		IdealToStage (float)
 *			-- convert ideal coordinates to stage coordinates
 */
void CConversion::Ideal_To_Stage (float *x, float *y, float *z)
{

	Ideal.Mat[1][X] = (double) *x;
	Ideal.Mat[1][Y] = (double) *y;
	Ideal.Mat[1][Z] = (double) *z;
	Ideal.Mat[1][4] = 1.0;

	matrix.matrix_multiply (&Ideal,&IdealToStage, &Stage);

	*x = (float) Stage.Mat[1][X];
	*y = (float) Stage.Mat[1][Y];
	*z = (float) Stage.Mat[1][Z];
}

/*
 *		StageToIdeal (int)
 *			-- convert stage coordinates to ideal coordinates
 */
void CConversion::Stage_To_Ideal (int *x, int *y, int *z)
{

	Stage.Mat[1][X] = (double) *x;
	Stage.Mat[1][Y] = (double) *y;
	Stage.Mat[1][Z] = (double) *z;
	Stage.Mat[1][4] = 1.0;

	matrix.matrix_multiply (&Stage,&StageToIdeal, &Ideal);

	*x = (int) Ideal.Mat[1][X];
	*y = (int) Ideal.Mat[1][Y];
	*z = (int) Ideal.Mat[1][Z];
}

/*
 *		StageToIdeal (float)
 *			-- convert stage coordinates to ideal coordinates
 */
void CConversion::Stage_To_Ideal (float *x, float *y, float *z)
{

	Stage.Mat[1][X] = (double) *x;
	Stage.Mat[1][Y] = (double) *y;
	Stage.Mat[1][Z] = (double) *z;
	Stage.Mat[1][4] = 1.0;

	matrix.matrix_multiply (&Stage,&StageToIdeal, &Ideal);

	*x = (float) Ideal.Mat[1][X];
	*y = (float) Ideal.Mat[1][Y];
	*z = (float) Ideal.Mat[1][Z];
}

void CConversion::CreateMatrices()
{
	int k;

		//reserve space for matrices
				//global (persistant) tranformation matrix
	if (Stage.Mat == NULL) {
		Stage.rows = 1;
		Stage.cols = 4;
		Stage.Mat = matrix.admatrix ((long)1,(long)Stage.rows,(long)1,(long)Stage.cols);
		}

	if (IdealToStage.Mat == NULL) {
		IdealToStage.rows = IdealToStage.cols = 4;
		IdealToStage.Mat = matrix.admatrix ((long)1,(long)IdealToStage.rows,(long)1,(long)IdealToStage.cols);
		}

	if (StageToIdeal.Mat == NULL) {
		StageToIdeal.rows = StageToIdeal.cols = 4;
		StageToIdeal.Mat = matrix.admatrix ((long)1,(long)StageToIdeal.rows,(long)1,(long)StageToIdeal.cols);
		}

	if (Ideal.Mat == NULL) {
		Ideal.rows = 1;
		Ideal.cols = 4;
		Ideal.Mat = matrix.admatrix ((long)1,(long)Ideal.rows,(long)1,(long)Ideal.cols);
		}

	if (EF.Mat == NULL) {
		EF.rows = 1;
		EF.cols = 4;
		EF.Mat = matrix.admatrix ((long)1,(long)EF.rows,(long)1,(long)EF.cols);
		}

			//initialise matrices which will hold coordinates
	for (k = 1; k <= Stage.cols; k++) {
		Stage.Mat[1][k] = 0.0;
		Ideal.Mat[1][k] = 0.0;
		}
	Stage.Mat[1][4] = 1.0;
	Ideal.Mat[1][4] = 1.0;
}

/********************************************************************
/*
 *		C R E A T E _ I D E A L _ T O _ E F
 *			-- use predefined England Finder positions to create
 *			-- the Ideal to EF coordinate transformation	
 */
int CConversion::create_ideal_to_EF ()
{
	int stat;
	CoordFrame IdealCoords;

	IdealCoords.cols = 4;
	IdealCoords.rows = 4;
	IdealCoords.Pt[0].x = F13x;
	IdealCoords.Pt[0].y = F13y;
	IdealCoords.Pt[0].z = 0;
	IdealCoords.Pt[1].x = U13x;
	IdealCoords.Pt[1].y = U13y;
	IdealCoords.Pt[1].z = 0;
	IdealCoords.Pt[2].x = U40x;
	IdealCoords.Pt[2].y = U40y;
	IdealCoords.Pt[2].z = 0;
	IdealCoords.Pt[3].x = F40x;
	IdealCoords.Pt[3].y = F40y;
	IdealCoords.Pt[3].z = 0;

	strcpy (&(IdealCoords.EFCoord[0][0]), "F13");
	strcpy (&(IdealCoords.EFCoord[1][0]), "U13");
	strcpy (&(IdealCoords.EFCoord[2][0]), "U40");
	strcpy (&(IdealCoords.EFCoord[3][0]), "F40");

	IdealCoords.Offset[0].x = 0.0;
	IdealCoords.Offset[0].y = 0.0;
	IdealCoords.Offset[1].x = 0.0;
	IdealCoords.Offset[1].y = 0.0;
	IdealCoords.Offset[2].x = 0.0;
	IdealCoords.Offset[2].y = 0.0;
	IdealCoords.Offset[3].x = 0.0;
	IdealCoords.Offset[3].y = 0.0;

	X_Home_Dir = 0;
	Y_Home_Dir = 0;
	X_Pos_Dir = 1;
	Y_Pos_Dir = -1;
	
	CreateMatrices();

	stat = EnglandFinder_Conversion (&IdealCoords, &EFToIdeal, &IdealToEF);

	return stat;
}


int CConversion::CreateEFString(char string[], char alpha, int num, int seg)
{
	int status = erINVALID_PARAMETER;

	if (string)
	{
		sprintf(string, "%c%d/%d", toupper(alpha), num, seg);
		status = erNO_ERRORS;
	}

	return status;
}

int CConversion::ParseEFString(const char string[], char *pAlpha, int *pNum, int *pSeg)
{
	int status = erINVALID_PARAMETER;

	if (string && pAlpha && pNum && pSeg)
	{
		*pAlpha = toupper(string[0]);				// Alphabetic component

		if (sscanf(string + 1, "%d", pNum) == 1)	// Numerical component
		{
			const char *pSlash = strpbrk(string,"/");
			if (pSlash)
				sscanf(pSlash + 1, "%d", pSeg);		// Segment component
			else
				*pSeg = 0;

			status = erNO_ERRORS;
		}
	}

	return status;
}


/*
 *		C O N V E R T _ E F _ T O _ I D E A L C O O R D S
 *			-- convert from England Finder positions to ideal coordinates
 */
int CConversion::convert_EF_to_idealcoords( int *x , int *y, char *EFCoords)
{
	char EFc, *aptr;
	int EFs, EFn, ialpha;
	double Rx, Ry,EFx, EFy;
	int err;

			//England Finder position
	if ((err = ParseEFString(EFCoords, &EFc, &EFn, &EFs)) != erNO_ERRORS)
		return err;

	if (EFc == 'I' || EFc < 'A' || EFc > 'Z' || EFn < 1 || EFn > 75 || EFs < 0 || EFs > 4) {
		sprintf (EFCoords,"Out of range");
		return erINVALID_PARAMETER;
		}

	aptr = Ualphabet;
	ialpha = 0;
	while (*(aptr+ialpha) != EFc)
		ialpha++;
	ialpha += 1;

	EF.Mat[1][X] = (double) ialpha;
	EF.Mat[1][Y] = (double) EFn;
	EF.Mat[1][3] = 0.0;
	EF.Mat[1][4] = 1.0;

	matrix.matrix_multiply (&EF,&EFToIdeal, &Stage);
	EFx = (int) Stage.Mat[1][X];
	EFy = (int) Stage.Mat[1][Y];

			//add offset to segment, using Ideal coordinate system
	Rx = EF_DIAM / 2.0 * EFdata.scaleX;
	Ry = EF_DIAM / 2.0 * EFdata.scaleY;

	switch (EFs) {
		case 1:	EFx = EFx - Rx;
					EFy = EFy + Ry;
					break;
		case 2:	EFx = EFx - Rx;
					EFy = EFy - Ry;
					break;
		case 3:	EFx = EFx + Rx;
					EFy = EFy + Ry;
					break;
		case 4:  EFx = EFx + Rx;
					EFy = EFy - Ry;
					break;
		}

	*x = (int) EFx;
	*y = (int) EFy;

	return erNO_ERRORS;
}

/*
 *		C O N V E R T _ I D E A L C O O R D S _ T O _ E F
 *			-- convert from ideal coordinates to England Finder positions
 */
int CConversion::convert_idealcoords_to_EF(float x , float y, char *EFCoords)
{
	double Rx, Ry, radius;// xx;

	char EFc;
	int EFs, EFn, EFx;

		//check on input coordinates
	if (x < 0.0 || x > 25000.0) {
		sprintf (EFCoords,"Out of range(X)");
		return erINVALID_PARAMETER;
		}
	if (y < 0.0 || y > 75000.0) {
		sprintf (EFCoords,"Out of range(Y)");
		return erINVALID_PARAMETER;
		}

	Stage.Mat[1][X] = (double) x;
	Stage.Mat[1][Y] = (double) y;
	Stage.Mat[1][3] = 0.0;
	Stage.Mat[1][4] = 1.0;

	if (matrix.matrix_multiply (&Stage,&IdealToEF, &EF) < 0)
		return erINVALID_PARAMETER;

	EFc = *(Ualphabet +  (int) (EF.Mat[1][X] - 0.5));	
	 
	EFn = (int) (EF.Mat[1][Y] + 0.5);

	if (EFc < 'A' || EFc > 'Z' || EFn < 1 || EFn > 75) {
		sprintf (EFCoords,"Out of range");
		return erINVALID_PARAMETER;
		}

			//determine segment, using England Finder coordinate system
	EFs = 0;
	Ry = EF.Mat[1][Y] - (double) EFn;
	EFx = (int)(EF.Mat[1][X] + 0.5);
	Rx = EF.Mat[1][X] - (double) EFx;
	radius = sqrt (Rx * Rx + Ry * Ry);

	if (radius > EF_DIAM / 2) {
		if (Rx <= 0.0 && Ry <= 0.0)
			EFs = 1;
		else if (Rx <= 0.0 && Ry >= 0.0)
			EFs = 2;
		else if (Rx >= 0.0 && Ry <= 0.0)
			EFs = 3;
		else if (Rx >= 0.0 && Ry >= 0.0)
			EFs = 4; 
		}
					//convert coordinates to England Finder String
	return CreateEFString(EFCoords, EFc, EFn, EFs);
}


/******************************************************************

/*
 *	C R E A T E _ S T A G E _ T O _ I D E A L
 *		-- and create the 'ideal' transformation matrices to 
 *		-- convert from the stage coordinate frame to the 'ideal' coordinate frame
 *		-- and vice versa
 */
int CConversion::create_stage_to_ideal(CoordFrame *StageFrame, CoordFrame *IdealFrame)
{
	int k,l;
	Matrix SFrame, IFrame;

				//rows and columns in native anf foreign matrices
	SFrame.rows = StageFrame->rows;
	SFrame.cols = StageFrame->cols;
	IFrame.rows = IdealFrame->rows;
	IFrame.cols = IdealFrame->cols;

	CreateMatrices();

	matrix.identity_matrix (&IdealToStage);
	matrix.identity_matrix (&StageToIdeal);

	if (SFrame.cols != IFrame.rows) {
						//created the identity matrices, so the thing will not bomb if used later
		return erINVALID_PARAMETER;			//error
		} 

				//local (non-persistant) matrices
	SFrame.Mat = matrix.admatrix ((long)1,(long)SFrame.rows,(long)1,(long)SFrame.cols);
	IFrame.Mat = matrix.admatrix ((long)1,(long)IFrame.cols,(long)1,(long)IFrame.cols);

	for (k = 1; k <= SFrame.rows; k++) {
		for (l = 1; l <= SFrame.cols; l++) {
			SFrame.Mat[k][l] = 0.0;
			}
		}
	for (k = 1; k <= IFrame.rows; k++) {
		for (l = 1; l <= IFrame.cols; l++) {
			IFrame.Mat[k][l] = 0.0;
			}
		}
			//copy source data for manipulation
	for (k = 1; k <= SFrame.rows; k++) {
		SFrame.Mat[k][X] = StageFrame->Pt[k-1].x;
		SFrame.Mat[k][Y] = StageFrame->Pt[k-1].y;
		SFrame.Mat[k][Z] = StageFrame->Pt[k-1].z;
		}
			//copy target data for manipulation
	for (k = 1; k <= IFrame.rows; k++) {
		IFrame.Mat[k][X] = IdealFrame->Pt[k-1].x + IdealFrame->Offset[k-1].x;		//true corrected position of calibration point
		IFrame.Mat[k][Y] = IdealFrame->Pt[k-1].y + IdealFrame->Offset[k-1].y;
		IFrame.Mat[k][Z] = IdealFrame->Pt[k-1].z;
		}

			/*Determine transformation matrix*/	
	Stage_To_Ideal_Matrix (&SFrame,&IFrame,&StageToIdeal);//	transformation_matrix (&SFrame,&IFrame,&ToIdeal);

	Ideal_To_Stage_Matrix (&SFrame,&IFrame,&IdealToStage);	//transformation_matrix (&IFrame, &SFrame, &ToStage);

			/*free-up memory*/
	matrix.free_admatrix (SFrame.Mat,(long)1,(long)SFrame.rows,(long)1,(long)SFrame.cols);
	matrix.free_admatrix (IFrame.Mat,(long)1,(long)IFrame.rows,(long)1,(long)IFrame.cols);

	return erNO_ERRORS;
}

/*
 *		R O T A T I O N _ S T A G E T O N E W S Y S T E M
 *			-- determine the angle of rotaion between the stage coordinate system
 *			-- and the new coordinate system (ideal or EF)
 */
double CConversion::Rotation_StageToNewSystem(double theta)
{
	
	if (X_Home_Dir == 0 && Y_Home_Dir == 0) {
		if (X_Pos_Dir == 1 && Y_Pos_Dir == -1) {
			;	
			}
		if (X_Pos_Dir == 1 && Y_Pos_Dir == 1) {
			;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == 1) {
			;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == -1) {
			;	// MDS2 normal configuration
			}

		}

	if (X_Home_Dir == 1 && Y_Home_Dir == 1) {
		if (X_Pos_Dir == 1 && Y_Pos_Dir == -1) {
							//Chromoscan normal configuration
			;
			}
		if (X_Pos_Dir == 1 && Y_Pos_Dir == 1) {
			;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == 1) {
			;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == -1) {
			;
			}
		}

	if (X_Home_Dir == 1 && Y_Home_Dir == 0) {
		if (X_Pos_Dir == 1 && Y_Pos_Dir == -1) {
			;
			}
		if (X_Pos_Dir == 1 && Y_Pos_Dir == 1) {
			theta = -theta;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == 1) {
			theta = -theta;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == -1) {
			;
			}
	}

	if (X_Home_Dir == 0 && Y_Home_Dir == 1) {
		if (X_Pos_Dir == 1 && Y_Pos_Dir == -1) {
			;
			}
		if (X_Pos_Dir == 1 && Y_Pos_Dir == 1) {
			theta = -theta;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == 1) {
			theta = -theta;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == -1) {
			;
			}
	}

	return theta;
}

/*
 *		S T A G E T O I D E A L M A T R I X
 *			-- create the transformation matrix for 
 *			-- stage to ideal coordinate transformations
 */
void CConversion::Stage_To_Ideal_Matrix(Matrix *SFrame, Matrix *IFrame, Matrix *Transformer)
{
	double V1[2], V2[2], XYd, theta, phi,alpha;

			/*Determine angular rotation between the Stage and Ideal coordinates*/
	V1[0] = SFrame->Mat[1][X] - SFrame->Mat[4][X];
	V1[1] = SFrame->Mat[1][Y] - SFrame->Mat[4][Y];
	XYd = sqrt (V1[0] * V1[0] + V1[1] * V1[1]);
	phi = asin (V1[0] / XYd);

	V2[0] = IFrame->Mat[1][X] - IFrame->Mat[4][X];
	V2[1] = IFrame->Mat[1][Y] - IFrame->Mat[4][Y];
	XYd = sqrt (V2[0] * V2[0] + V2[1] * V2[1]);
	alpha = asin (V2[0] / XYd);

	theta = Rotation_StageToNewSystem( phi - alpha);

	transformation_matrix (SFrame,IFrame,Transformer, theta);
}

/*
 *		R O T A T I O N _ N E W S Y S T E M S T A G E T O 
 *			-- determine the angle of rotaion between the new coordinate system (ideal or EF)
 *			--	and the stage coordinate system
 */
double CConversion::Rotation_NewSystemToStage(double theta)
{

	if (X_Home_Dir == 0 && Y_Home_Dir == 0) {
		if (X_Pos_Dir == 1 && Y_Pos_Dir == -1) {
										// MDS 2 normal configuration
			theta = -theta;
			}
		if (X_Pos_Dir == 1 && Y_Pos_Dir == 1) {
			theta = -theta;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == 1) {
			;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == -1) {
			;
			}

		}

	if (X_Home_Dir == 1 && Y_Home_Dir == 1) {
		if (X_Pos_Dir == 1 && Y_Pos_Dir == -1) {
										//Chromoscan normal configuration
			theta = -theta;
			}
		if (X_Pos_Dir == 1 && Y_Pos_Dir == 1) {
			theta = -theta;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == 1) {
			;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == -1) {
			;
			}
		}

	if (X_Home_Dir == 1 && Y_Home_Dir == 0) {
		if (X_Pos_Dir == 1 && Y_Pos_Dir == -1) {
			theta = -theta;
			}
		if (X_Pos_Dir == 1 && Y_Pos_Dir == 1) {
			theta = -theta;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == 1) {
			;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == -1) {
			;
			}
	}

	if (X_Home_Dir == 0 && Y_Home_Dir == 1) {
		if (X_Pos_Dir == 1 && Y_Pos_Dir == -1) {
			theta = -theta;
			}
		if (X_Pos_Dir == 1 && Y_Pos_Dir == 1) {
			theta = -theta;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == 1) {
			;
			}
		if (X_Pos_Dir == -1 && Y_Pos_Dir == -1) {
			;
			}
	}

	return theta;
}

void CConversion::Ideal_To_Stage_Matrix(Matrix *SFrame, Matrix *IFrame, Matrix *Transformer)
{
	double V1[2], V2[2], XYd, theta, phi,alpha;

			/*Determine angular rotation between the Stage and Ideal coordinates*/
	V1[0] = SFrame->Mat[1][X] - SFrame->Mat[4][X];
	V1[1] = SFrame->Mat[1][Y] - SFrame->Mat[4][Y];
	XYd = sqrt (V1[0] * V1[0] + V1[1] * V1[1]);
	phi = asin (V1[0] / XYd);

	V2[0] = IFrame->Mat[1][X] - IFrame->Mat[4][X];
	V2[1] = IFrame->Mat[1][Y] - IFrame->Mat[4][Y];
	XYd = sqrt (V2[0] * V2[0] + V2[1] * V2[1]);
	alpha = asin (V2[0] / XYd);

	theta = Rotation_NewSystemToStage (alpha - phi);

	transformation_matrix (IFrame,SFrame,Transformer, theta);
}

/*
 *		D E F I N E A X E S
 *			-- define the stage coordinate axes
 *			-- X_Home_Dir, Y_Home_Dir define position of origin wrt slides
 *			-- X_Pos_Dir,Y_Pos_Dir define direction of positive axis wrt origin (handedness)
 */
void CConversion::DefineAxes (int x_home_dir, int y_home_dir, int x_pos_dir, int y_pos_dir)
{

	X_Home_Dir = x_home_dir;
	Y_Home_Dir = y_home_dir;
	X_Pos_Dir = x_pos_dir;
	Y_Pos_Dir = y_pos_dir;
}


/*
 *		T R A N S F O R M A T I O N _ M A T R I X
 *			-- create the transformation matrix using
 *			-- homogeneous coordinates, a 4 x 4 matrix.
 *			-- uses four calibration points (ideal coordinates)
 */
void CConversion::transformation_matrix (Matrix *Stage, Matrix *Ideal, Matrix *Transformer, double theta)
{
	int i;
	Matrix Operator;
	double XTranslation,YTranslation,ZTranslation;
	double Xscale, Yscale;
	int XMirror, YMirror, ZMirror;
	double SignStage, SignIdeal;
	double s1, s2, s3,s4;

	Operator.rows = IDEAL_POINTS;
	Operator.cols = IDEAL_POINTS;
	Operator.Mat = matrix.admatrix ((long)1,(long)Operator.rows,(long)1,(long)Operator.cols);

	matrix.identity_matrix (&Operator);		//set the identity matrix
	matrix.identity_matrix (Transformer);	//set the identity matrix

			/*Translate centre of slide to origin*/
					//find the centre of gravity of the four calibration points
	XTranslation = YTranslation = ZTranslation = 0.0;
	for (i = 1; i <= IDEAL_POINTS; i++) {
		XTranslation += Stage->Mat[i][X];
		YTranslation += Stage->Mat[i][Y];
		}
	XTranslation /= IDEAL_POINTS;
	YTranslation /= IDEAL_POINTS;
	ZTranslation = Stage->Mat[1][Z];

			/*Apply X Y Z translation*/
	Transformer->Mat[4][1] = Operator.Mat[4][1] = -XTranslation;
	Transformer->Mat[4][2] = Operator.Mat[4][2] = -YTranslation;
	Transformer->Mat[4][3] = Operator.Mat[4][3] = -ZTranslation;

			//Account for any  angular rotation between the coordinate systems
	matrix.identity_matrix (&Operator);

	Operator.Mat[1][1] = cos (theta);
	Operator.Mat[1][2] = sin (theta);
	Operator.Mat[2][1] = -sin (theta);
	Operator.Mat[2][2] = cos (theta);

	matrix.matrix_multiply (Transformer,&Operator,Transformer);


			/*Scale to 'size' of ideal coordinates and align  coordinate systems*/
	matrix.identity_matrix (&Operator);

			/*Align X - axis*/
	SignStage = (Stage->Mat[1][X] - Stage->Mat[2][X]);
	SignIdeal = (Ideal->Mat[1][X] - Ideal->Mat[2][X]);
	if ((SignIdeal > 0 && SignStage > 0) ||( SignIdeal < 0 && SignStage < 0) )
		XMirror = 1;
	else
		XMirror = -1;


			/*Align Y-axis*/
	SignStage = (Stage->Mat[4][Y] - Stage->Mat[1][Y]);
	SignIdeal = (Ideal->Mat[4][Y] - Ideal->Mat[1][Y]);
	if ((SignIdeal > 0 && SignStage > 0) ||( SignIdeal < 0 && SignStage < 0) )
		YMirror = 1;
	else
		YMirror = -1;

			/*Z-axis values should always be positive*/
	if (Stage->Mat[1][Z] < 0 || Ideal->Mat[1][Z] < 0)
		ZMirror = -1;
	else
		ZMirror = 1;

					/*Determine scale values for X and Y*/
			/*Differences in Z scale (unit stepsize) between systems will have to 
			 *be accounted for by the application, since we do not have a 3-D calibration slide.
             *All that is required is an offset between the recorded Z value and the focus position of an object (cell) 
			 */
	s1 = fabs (Ideal->Mat[1][X] - Ideal->Mat[2][X]) / fabs (Stage->Mat[1][X] - Stage->Mat[2][X]);
	s2 = fabs (Ideal->Mat[1][Y] - Ideal->Mat[4][Y]) / fabs (Stage->Mat[1][Y] - Stage->Mat[4][Y]);
	s3 = fabs (Ideal->Mat[4][X] - Ideal->Mat[3][X]) / fabs (Stage->Mat[4][X] - Stage->Mat[3][X]);
	s4 = fabs (Ideal->Mat[2][Y] - Ideal->Mat[3][Y]) / fabs (Stage->Mat[2][Y] - Stage->Mat[3][Y]);

	Xscale = (s1 + s3) / 2.0;
	Yscale = (s2 + s4) / 2.0;

	Operator.Mat[1][1] = Xscale * XMirror;
	Operator.Mat[2][2] = Yscale * YMirror;
	Operator.Mat[3][3] = ZMirror;

	matrix.matrix_multiply (Transformer,&Operator,Transformer);

			/*Translate to Ideal position*/
	matrix.identity_matrix (&Operator);
					//find the centre of gravity of the four calibration points
	XTranslation = YTranslation = ZTranslation = 0.0;
	for (i = 1; i <= IDEAL_POINTS; i++) {
		XTranslation += Ideal->Mat[i][X];
		YTranslation += Ideal->Mat[i][Y];
		}
	XTranslation /= IDEAL_POINTS;
	YTranslation /= IDEAL_POINTS;
	ZTranslation = Ideal->Mat[1][Z];

	Operator.Mat[4][1] = XTranslation;
	Operator.Mat[4][2] = YTranslation;
	Operator.Mat[4][3] = ZTranslation;

	matrix.matrix_multiply (Transformer,&Operator,Transformer);

	matrix.free_admatrix (Operator.Mat,(long)1,(long)Operator.rows,(long)1,(long)Operator.rows);
}

/*
 *	S H O W _ M A T R I X
 *		-- display contents of the matrix
 *		-- for test purposes
 *
 */
void CConversion::show_matrix (Matrix *mat)
{
#ifdef _DEBUG

	int i,j;
	char string[256];

	for (i = 1; i <= mat->rows; i++) {
		for (j = 1; j <= mat->cols; j++) {
			sprintf (string,"%14.6f  ",mat->Mat[i][j]);
			OutputDebugString (string);	
			}
		OutputDebugString ("\n");
		}
	OutputDebugString ("\n");

#endif

}

/**************************************************************************

/*
 *		E N G L A N D F I N D E R _ C O N V E R S I O N
 *			-- create the transformation matrices to convert from stage/idea; coordinates
 *			-- to England Finder coordinates and vice versa
 */
int CConversion::EnglandFinder_Conversion (CoordFrame *StageFrame, Matrix *FromEF, Matrix *ToEF)
{
	int row,col, x;
	Matrix SFrame, EFFrame;
	char EFc, *aptr;
	int EFn;

				//rows and columns in native anf foreign matrices
	SFrame.rows = StageFrame->rows;
	SFrame.cols = StageFrame->cols;

	EFFrame.rows = StageFrame->rows;
	EFFrame.cols = StageFrame->cols;

			//four calibration points (i.e. MDS2 ideal coordinates)
	point1 = 1;
	point2 = 3;

		//reserve space for matrices
				//global (persistant) tranformation matrix
	FromEF->rows = FromEF->cols = 4;
	FromEF->Mat = matrix.admatrix ((long)1,(long)FromEF->rows,(long)1,(long)FromEF->cols);
	ToEF->rows = ToEF->cols  = 4;
	ToEF->Mat = matrix.admatrix ((long)1,(long)ToEF->rows,(long)1,(long)ToEF->cols);

	if (EF.Mat == NULL) {
		EF.rows = 1;	
		EF.cols = 4;
		EF.Mat = matrix.admatrix ((long) 1, (long) EF.rows, (long) 1, (long) EF.cols);
	}

	for (col = 1; col <= EF.cols; col++) 
		EF.Mat[1][col] = 0.0;

	matrix.identity_matrix (ToEF);
	matrix.identity_matrix (FromEF);

	if (SFrame.cols != EFFrame.rows) {
						//created the identity matrices, so the thing will not bomb if used later
		return erINVALID_PARAMETER;			//error
		} 
				//local (non-persistant) matrices
	SFrame.Mat = matrix.admatrix ((long)1,(long)SFrame.rows,(long)1,(long)SFrame.cols);
	EFFrame.Mat = matrix.admatrix ((long)1,(long)EFFrame.cols,(long)1,(long)EFFrame.cols);

	for (col = 1; col <= SFrame.cols; col++) {
		for (row = 1; row <= SFrame.rows; row++) {
			SFrame.Mat[row][col] = 0.0;
			}
		}
	for (col = 1; col <= EFFrame.cols; col++) {
		for (row = 1; row <= EFFrame.rows; row++) {
			EFFrame.Mat[row][col] = 0.0;
			}
		}
			//copy source data for manipulation
	for (row = 1; row <= SFrame.rows; row++) {
		SFrame.Mat[row][X] = StageFrame->Pt[row-1].x + StageFrame->Offset[row-1].x;
		SFrame.Mat[row][Y] = StageFrame->Pt[row-1].y - StageFrame->Offset[row-1].y;
		SFrame.Mat[row][Z] = StageFrame->Pt[row-1].z;
		}
			//copy target data for manipulation
	for (row = 1; row <= EFFrame.rows; row++) {
		EFc = StageFrame->EFCoord[row-1][0];		//alphabetic component
		if (islower (EFc)) {
			EFc = toupper (EFc);
			StageFrame->EFCoord[row-1][0] = EFc;
			}
		sscanf (&(StageFrame->EFCoord[row-1][1]),"%d",&EFn);		//numerical component
		aptr = Ualphabet;
		x = 0;
		while (*(aptr+x) != EFc)
				x++;
		x += 1; 
		EFFrame.Mat[row][X] = (double) x;		//England Finder calibration point alphabetic component
		EFFrame.Mat[row][Y] = (double) EFn;		//England Finder calibration point numerical component
		}

			/*Create the transformation matrices*/		
	Stage_To_EF_Matrix (&SFrame,&EFFrame,ToEF);

	EF_To_Stage_Matrix (&SFrame,&EFFrame,FromEF); 

			/*get England Finder Data*/
	EnglandFinderData (StageFrame);

			/*free-up memory*/
	matrix.free_admatrix (SFrame.Mat,(long)1,(long)SFrame.rows,(long)1,(long)SFrame.cols);
	matrix.free_admatrix (EFFrame.Mat,(long)1,(long)EFFrame.rows,(long)1,(long)EFFrame.cols);

	return erNO_ERRORS;
}

void CConversion::Stage_To_EF_Matrix(Matrix *SFrame, Matrix *EFrame, Matrix *Transformer)
{
	double V1[2], V2[2], XYd, theta, phi,alpha;

			/*Determine angular rotation between the Stage and Ideal coordinates*/
	V1[0] = SFrame->Mat[1][X] - SFrame->Mat[4][X];
	V1[1] = SFrame->Mat[1][Y] - SFrame->Mat[4][Y];
	XYd = sqrt (V1[0] * V1[0] + V1[1] * V1[1]);
	phi = asin (V1[0] / XYd);

	V2[0] = EFrame->Mat[1][X] - EFrame->Mat[4][X];
	V2[1] = EFrame->Mat[1][Y] - EFrame->Mat[4][Y];
	XYd = sqrt (V2[0] * V2[0] + V2[1] * V2[1]);
	alpha = asin (V2[0] / XYd);
	
//	theta = phi - alpha;
	theta = Rotation_StageToNewSystem (phi - alpha);

	transformation_matrix (SFrame,EFrame,Transformer, theta);
}

void CConversion::EF_To_Stage_Matrix(Matrix *SFrame, Matrix *EFrame, Matrix *Transformer)
{
	double V1[2], V2[2], XYd, theta, phi,alpha;

			/*Determine angular rotation between the Stage and Ideal coordinates*/
	V1[0] = SFrame->Mat[1][X] - SFrame->Mat[4][X];
	V1[1] = SFrame->Mat[1][Y] - SFrame->Mat[4][Y];
	XYd = sqrt (V1[0] * V1[0] + V1[1] * V1[1]);
	phi = asin (V1[0] / XYd);

	V2[0] = EFrame->Mat[1][X] - EFrame->Mat[4][X];
	V2[1] = EFrame->Mat[1][Y] - EFrame->Mat[4][Y];
	XYd = sqrt (V2[0] * V2[0] + V2[1] * V2[1]);
	alpha = asin (V2[0] / XYd);
	
//	theta = alpha - phi;
	theta = Rotation_NewSystemToStage (alpha - phi);

	transformation_matrix (EFrame,SFrame,Transformer, theta);
}


/*
 *		C O N V E R T _ C O O R D S _ T O _ E F
 *			-- convert from stage coordinates to England Finder positions
 */
int CConversion::convert_coords_to_EF(CoordFrame *current, float x , float y, char *EFCoords)
{
	double Rx, Ry, radius, xx;
	char EFc;
	int EFs, EFn, EFx;


	Stage.Mat[1][X] = (double) x;
	Stage.Mat[1][Y] = (double) y;
	Stage.Mat[1][3] = 0.0;
	Stage.Mat[1][4] = 1.0;

	matrix.matrix_multiply (&Stage,&StageToEF, &EF);

	xx = EF.Mat[1][X];
	EFc = *(Ualphabet +  (int) (EF.Mat[1][X] - 0.5));	
	EFn = (int) (EF.Mat[1][Y] + 0.5);

	if (EFc < 'A' || EFc > 'Z' || EFn < 1 || EFn > 75) {
		sprintf (EFCoords,"Out of range");
		return erINVALID_PARAMETER;
		}

			//determine segment
	EFs = 0;
	Ry = EF.Mat[1][Y] - (double) EFn;
	EFx = (int)(EF.Mat[1][X] + 0.5);
	Rx = EF.Mat[1][X] - (double) EFx;
	radius = sqrt (Rx * Rx + Ry * Ry);

	if (radius > EF_DIAM / 2) {
		if (Rx <= 0.0 && Ry <= 0.0)
			EFs = 1;
		else if (Rx <= 0.0 && Ry >= 0.0)
			EFs = 2;
		else if (Rx >= 0.0 && Ry >= 0.0)
			EFs = 4;
		else if (Rx >= 0.0 && Ry <= 0.0)
			EFs = 3; 
		}
	
				//convert coordinates to England Finder String
	return CreateEFString(EFCoords, EFc, EFn, EFs);
}

/*
 *		C O N V E R T _ E F _ T O _ C O O R D S
 *			-- convert from England Finder positions to stage coordinates
 */
int CConversion::convert_EF_to_coords(CoordFrame *current, int *x , int *y, char *EFCoords)
{
	char EFc, *aptr;
	int EFs, EFn, ialpha;
	double Rx, Ry,EFx, EFy;
	int err;

			//England Finder position
	if ((err = ParseEFString(EFCoords, &EFc, &EFn, &EFs)) != erNO_ERRORS)
		return err;

	if (EFc < 'A' || EFc > 'Z' || EFn < 1 || EFn > 75 || EFs < 0 || EFs > 4) {
		sprintf (EFCoords,"Out of range");
		return erINVALID_PARAMETER;
		}

	aptr = Ualphabet;
	ialpha = 0;
	while (*(aptr+ialpha) != EFc)
		ialpha++;
	ialpha += 1;

	EF.Mat[1][X] = (double) ialpha;
	EF.Mat[1][Y] = (double) EFn;
	EF.Mat[1][3] = 0.0;
	EF.Mat[1][4] = 1.0;

	matrix.matrix_multiply (&EF,&EFToStage, &Stage);
	EFx = (int) Stage.Mat[1][X];
	EFy = (int) Stage.Mat[1][Y];

			//add offset to segment

	Rx = EF_DIAM / 2.0 * EFdata.scaleX;
	Ry = EF_DIAM / 2.0 * EFdata.scaleY;

	switch (EFs) {
		case 1:	EFx = EFx + Rx;
					EFy = EFy - Ry;
					break;
		case 2:	EFx = EFx + Rx;
					EFy = EFy + Ry;
					break;
		case 3:	EFx = EFx - Rx;
					EFy = EFy - Ry;
					break;
		case 4:  EFx = EFx - Rx;
					EFy = EFy + Ry;
					break;
		}

	*x = (int) EFx;
	*y = (int) EFy;

	return erNO_ERRORS;
}


/*
 *			E N G L A N D F I N D E R D A T A
 *				-- determine X and Y scale factors and slide orientation
 */
void CConversion::EnglandFinderData(CoordFrame *current)
{
	double deltaX,deltaY;
	int AlphaDist,NumDist;
	char cEF1, cEF2;
	int dEF1, dEF2;
	char *aptr1, *aptr2;

			/*distance between calibration points*/
	//FOUR POINTS IN CALIBRATION!!!!! -- take points on the diagonal
	deltaX = fabs ( (current->Pt[0].x + current->Offset[0].x) - (current->Pt[2].x + current->Offset[0].x) );
	deltaY = fabs ( (current->Pt[0].y + current->Offset[0].y) - (current->Pt[2].y + current->Offset[2].y) );

			/*England Finder*/
	cEF1 = current->EFCoord[0][0];
	cEF2 = current->EFCoord[2][0];
	sscanf (&(current->EFCoord[0][1]),"%d",&dEF1);
	sscanf (&(current->EFCoord[2][1]),"%d",&dEF2);

	aptr1 = Ualphabet;
	while (*aptr1 != cEF1)
			aptr1++;
	aptr2 = Ualphabet;
	while (*aptr2 != cEF2)
			aptr2++;
	AlphaDist = abs (aptr2 - aptr1);

	NumDist = abs (dEF2 - dEF1); 	


				//assumes the distance between the calibration points is larger along the 
				//the Y-axis than along the X-axis
	if (fabs (deltaX) < fabs(deltaY)) {		//x-axis parallel with alpha chars
//		EFdata.Axis = Mic_Stage_X;
		EFdata.scaleX = (double) deltaX / (double) AlphaDist;
		EFdata.scaleY = (double) deltaY / (double) NumDist;
/*
		if ( fabs(current->Pt[0].y) > fabs (current->Pt[2].y))
			EFdata.Orientation = 1;
		else
			EFdata.Orientation = 2;	
*/
		}
	else 	{					//y-axis parallel with alpha chars
//		EFdata.Axis = Mic_Stage_Y;	
		EFdata.scaleX = (double) deltaX / (double) NumDist;
		EFdata.scaleY = (double) deltaY / (double) AlphaDist;
/*
		if ( fabs (current->Pt[0].x) > fabs (current->Pt[2].x) )
			EFdata.Orientation = 3;
		else
			EFdata.Orientation = 4;	
*/
		}

	EFdata.Circle_Radius = fabs (EF_DIAM * EFdata.scaleX / 2.0);	
}

/*
 *		R O U N D
 *			-- round the value to dp decimal places
 */
void  CConversion::round(double *x, int dp)
{
	double div;
	int xx;

	if (dp < 0 && dp > 12)
		return;

	div = 1;
	while (dp-- > 0)
		div *= 10;

	xx = (int) (*x * div + 0.5);
	*x = xx / div;

}
