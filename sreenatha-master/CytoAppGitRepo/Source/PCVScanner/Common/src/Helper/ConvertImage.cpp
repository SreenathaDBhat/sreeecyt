//=================================================================================
// ConvertImage.cpp
// Image data conversion class > 8 bit images
//
// History:
//
// 20Mar00	WH:	original
//=================================================================================
#include "stdafx.h"
#include "ConvertImage.h"
#include <math.h>

//=================================================================================
//=================================================================================
CConvertImage::CConvertImage()
{
	m_inputLUT = NULL;
	m_intensities = 0;
}
//=================================================================================
//=================================================================================
CConvertImage::~CConvertImage()
{
	if (m_inputLUT)
		delete m_inputLUT;
}

//=================================================================================
//=================================================================================
void CConvertImage::SetImage(WORD * pImage, WORD bitdepth, WORD bitshift, UINT width, UINT height)
{
	m_bitdepth = bitdepth;
	m_width = width;
	m_height = height;
	m_pImage = pImage;
	UINT nIntensities = (int)pow(2.0, m_bitdepth);
	
	// Allocate a new input LUT if required
	if ((m_intensities != nIntensities) || (m_bitshift != bitshift))
	{
		m_intensities = nIntensities;
		m_bitshift = bitshift;
		UINT factor = (int)pow(2.0, 8.0+m_bitshift);

		if (m_inputLUT)
			delete m_inputLUT;

		m_inputLUT = new UINT[m_intensities];

		
		// allocate default input LUT
		if (m_inputLUT)
			for (UINT i = 0; i < m_intensities; i++)
			{
				int val = i/(m_intensities/factor);
				if (val > 255)
					val=255;
				m_inputLUT[i] = (UINT)val;
			}
	}
}
//=================================================================================
// MeasureMinMax
//		Scan through the 12 bit image data (in a short word) to find the
// min and max intensities. 
//=================================================================================
void CConvertImage::MinMax(long *min, long *max)
{
	long lmin = 0;                 // initially a zeroed image
	long lmax = 0;

	if( m_pImage) // don't scan a non-allocated array
	{              
		WORD value;                        // 16-bit value at address pImage
		WORD *pImage = m_pImage;

		value = *(pImage++);
		lmax = value;           // fill initial intensities with value
		lmin = value;           //   of the first pixel

		for(UINT index=1; index < m_width * m_height; index++ ) 
		{
			value = *(pImage++);
			if( lmax < value )
				lmax = value;
			else 
				if( lmin > value )
					lmin = value;
		}
		*max = lmax;
		*min = lmin;
	}
}
//=================================================================================
// pvcamConvertImageToDisplay
//
//	Converts the 12 bit image data in pImage into 8 bit data in pDisp.
//	Image dimensions are width and height.
//
//	If stretch is TRUE:  Perform contrast stretch
//	If stretch is FALSE: Select a 8 bit plane from the 12 bits.
//=================================================================================
BOOL CConvertImage::DoConversion(BYTE *pDisp, ConvertType type, BOOL bPad)
{
	long intensity;
	DWORD i, j;
	int k, padby = 0;
	BYTE *lp = pDisp;
	WORD *pImage = m_pImage;

	if (pDisp == NULL) {
		TRACE("CConvertImage::DoConversion() Passed NULL image pointer\r\n");
		return FALSE;
	}

	if (pImage == NULL) {
		TRACE("CConvertImage::DoConversion() NULL image conversion pointer\r\n");
		return FALSE;
	}
	// Scan lines need need to be dword aligned in DIBs
	if (bPad)
		padby = (sizeof(DWORD) - (m_width % sizeof(DWORD))) % sizeof(DWORD);

	if (type == Stretch) 
	{
		long max, min;
		float scale;
		int lorange = 0, hirange = 255;

		// Measure image intensity
		MinMax(&min, &max);

		// If there's no contrast - take a short cut
		if (max == min) 
		{
			if (min == lorange)
				memset(pDisp, lorange, m_width * m_height);	// Set all image to lorange
			else
				memset(pDisp, hirange, m_width * m_height);	// Set all image to hirange

			return TRUE;
		}

		// ... otherwise use image measurements to stretch image display
		scale = (float)(hirange - lorange) / (float)(max - min);

		for (j = 0; j < m_height; j++ ) 
		{ // for every line
			for(i = 0; i < m_width; i++)	
			{ // for length of scan line

				// Stretch / squeeze 12 bit data to 8 bit
				intensity = *(pImage++) - min;
				intensity = (long)((float)intensity * scale);
				intensity = intensity + lorange;

				*(lp++) = (unsigned char)m_inputLUT[intensity * 16];
			}

			// End of scan line, pad to 4 byte alignment for DIB
			for (k = 0; k < padby; k++)
				*(lp++) = 0;
		}
	} 

	if (type == InputLUT)
	{

		// No contrast stretch use LUT to convert data
		for (j = 0; j < m_height; j++ ) 
		{		// for every line
			for(i = 0; i < m_width; i++)	
			{	// for length of scan line

				if (*pImage > m_intensities -1) // Shouldn't happen
					*pImage = m_intensities -1;

				*lp = (unsigned char)m_inputLUT[*pImage];

				pImage++;
				lp++;
			}
			// End of scan line, pad to 4 byte alignment for DIB
			// Pad with last values (else disturbs 8bit max/min calculations)
			for (k = 0; k < padby; k++)
				*(lp++) = (unsigned char)m_inputLUT[*(pImage -1)];
		}
	}

	return TRUE;
}
//=================================================================================
//=================================================================================
void CConvertImage::SetInputLUT8(BYTE index, BYTE value)
{
	// Stretch single 8 bit index into a block of 12 bits and all have the same value
	int lo = index*(m_intensities/256);
	int hi = (index +1)*(m_intensities/256);

	for (int i = lo; i < hi; i++)
		m_inputLUT[i] = value;
}
//=================================================================================
//=================================================================================
void CConvertImage::GammaCorrection(float v_gamma, BOOL v_inverted)
{

	int i;
	BYTE lut[256];
	short lut12[4096];

	float scale = (float)(255.0/pow((double)255.0, (double)v_gamma));
	for (i = 0; i < 256; i++)
		lut[i] = (BYTE)(scale*pow((float)i, v_gamma) + 0.49999);

	scale = (float)(4095.0/pow((double)4095.0, (double)v_gamma));
	for (i = 0; i < 4096; i++)
		lut12[i] = (short)(scale*pow((float)i, v_gamma) + 0.49999);


	/* If brightfield, table is inverted. */
	if (v_inverted == TRUE) 
	{
		for (i = 0; i < 256; i++)
			lut[i] = 255 - lut[i];
		for (i = 0; i < 4096; i++)
			lut12[i] = 4096 - lut12[i];
	}


	for (i = 0; i < 4096; i++) 
	{
		m_inputLUT[i] = lut[i/16];
//		input_lut12[i] = lut12[i];
	}

}
//=================================================================================
//=================================================================================
/*
 *	C O L O U R T O M O N O C H R O M E
 *		-- extract a single colour plane for a monochrome image
 */
void CConvertImage::ColourToMonochrome(BYTE *pMono, BYTE *pColour, WORD bitdepth, WORD c_plane, UINT width, UINT height)
{
	BYTE *pImgMono, *pImgCol;	
	int BytesPerPixel;
	
	if (c_plane < 1)
		c_plane = 1;
	if (c_plane > 3)
		c_plane = 1;

	pImgMono = pMono;
	pImgCol = pColour + c_plane;
	BytesPerPixel = 4; //XRGB

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++){
				*pImgMono = (unsigned char) (*pImgCol & 0xff); //one colour plane
				pImgMono++;
				pImgCol += BytesPerPixel;
				}
	}
}
