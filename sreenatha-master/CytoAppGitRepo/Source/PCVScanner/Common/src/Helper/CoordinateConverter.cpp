///////////////////////////////////////////////////////////////////////////////////////////////////
// Coordinate conversion takes points B and C from calibration slide in ideal and foreign
// coordinate systems and calculates the forward and backward transformations between the two
// Rewritten to use Direct X matrix routines rather than cruddy 1950's Kernighan and Ritchie stuff
///////////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//  There is a small lack of precision due to Direct X use of floats rather than doubles

#include "stdafx.h"
#include "CoordinateConverter.h"
#define _USE_MATH_DEFINES
#include <math.h>


#define _CX _11
#define _CY _12
#define _BX _21
#define _BY _22

CoordinateConverter::CoordinateConverter(StageType TypeOfStage, const ConversionData& conversionData)
{
    D3DXMatrixIdentity(&m_SourceCoords);
    D3DXMatrixIdentity(&m_Result);
    m_StageType = TypeOfStage;

    if (m_StageType != ManualWithXyReader)
	    CreateTransformationMatrix(conversionData);
    else
        CalculateXYReaderTransform(conversionData);
}


CoordinateConverter::~CoordinateConverter()
{
}


///////////////////////////////////////////////////////////////////////////////////////////////////
// Convert from ideal coords to the foreign coord system
///////////////////////////////////////////////////////////////////////////////////////////////////

CoordinateList CoordinateConverter::ConvertFromIdeal(const CoordinateList& sourceCoordinates)
{
	CoordinateList targetCoordinates = ConvertCoordinates(true, sourceCoordinates);
	return targetCoordinates;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// The reverse
///////////////////////////////////////////////////////////////////////////////////////////////////

CoordinateList CoordinateConverter::ConvertToIdeal(const CoordinateList& sourceCoordinates)
{
	CoordinateList targetCoordinates = ConvertCoordinates(false, sourceCoordinates);
	return targetCoordinates;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Convert forward or backward - single point in Sx, Sy to Tx, Ty
///////////////////////////////////////////////////////////////////////////////////////////////////

void CoordinateConverter::ConvertSingleCoord(bool Forward, double Sx, double Sy, double &Tx, double &Ty)
{
    m_SourceCoords._11 = Sx;
    m_SourceCoords._12 = Sy;
    m_SourceCoords._13 = 0.0f;
    m_SourceCoords._14 = 1.0f;

    if (Forward)
        ConvertSingleCoord(Forward, &m_Transform, &m_SourceCoords, &m_Result);
    else
        ConvertSingleCoord(Forward, &m_InverseTransform, &m_SourceCoords, &m_Result);

    Tx = m_Result._11;
    Ty = m_Result._12;
}

// Bodges cause of managed crap
void CoordinateConverter::ConvertSingleCoord(bool Forward, double Sx, double Sy)
{
    m_SourceCoords._11 = Sx;
    m_SourceCoords._12 = Sy;
    m_SourceCoords._13 = 0.0f;
    m_SourceCoords._14 = 1.0f;

    if (Forward)
        ConvertSingleCoord(Forward, &m_Transform, &m_SourceCoords, &m_Result);
    else
        ConvertSingleCoord(Forward, &m_InverseTransform, &m_SourceCoords, &m_Result);

    m_X = m_Result._11;
    m_Y = m_Result._12;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
// Do either a forward or backward conversion Forward = from ideal, backward = to ideal
///////////////////////////////////////////////////////////////////////////////////////////////////

CoordinateList CoordinateConverter::ConvertCoordinates(bool Forward, const CoordinateList& sourceCoordinates)
{
	CoordinateList targetCoordinates;

	for (CoordinateList::const_iterator iter = sourceCoordinates.begin();
		 iter != sourceCoordinates.end();
		 ++iter)
	{
		std::pair<double, double> sourceCoord = iter->second;
		std::pair<double, double> targetCoord = ConvertSingleCoord(Forward, sourceCoord);
		
		targetCoordinates[iter->first] = targetCoord;
	}
	
	return targetCoordinates;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

std::pair<double, double> CoordinateConverter::ConvertSingleCoord(bool Forward, std::pair<double, double> sourceCoord)
{
    m_SourceCoords._11 = sourceCoord.first;
    m_SourceCoords._12 = sourceCoord.second;
    m_SourceCoords._13 = 0.0f;
    m_SourceCoords._14 = 1.0f;

    if (Forward)
        return ConvertSingleCoord(Forward, &m_Transform, &m_SourceCoords, &m_Result);
    else
        return ConvertSingleCoord(Forward, &m_InverseTransform, &m_SourceCoords, &m_Result);
}

///////////////////////////////////////////////////////////////////////////////////////////////////

std::pair<double, double> CoordinateConverter::ConvertSingleCoord(bool Forward, D3DXMATRIX *Transform, D3DXMATRIX *Source, D3DXMATRIX *Dest) 
{
    std::pair<double, double> targetCoordinate;

    if (m_StageType == ManualWithXyReader)
    {
        double tx, ty;

        if (Forward)
            ConvertXYReaderFromIdeal(Source->_11, Source->_12, tx, ty);
        else
            ConvertXYReaderToIdeal(Source->_11, Source->_12, tx, ty);
        targetCoordinate.first = tx;
        targetCoordinate.second = ty;
        Dest->_11 = tx;
        Dest->_12 = ty;
    }
    else
    {
        D3DXMatrixMultiply(Dest, Source, Transform);

        targetCoordinate.first  = Dest->_11;
        targetCoordinate.second = Dest->_12;
    }

    return targetCoordinate;
}



///////////////////////////////////////////////////////////////////////////////////////////////////
// Calc the forward and backwards transformations
///////////////////////////////////////////////////////////////////////////////////////////////////

void CoordinateConverter::CreateTransformationMatrix(const ConversionData& conversionData)
{

    D3DXMatrixIdentity(&m_Native);
    D3DXMatrixIdentity(&m_Foreign);
	
    m_Native._CX = conversionData.sourceCX;
    m_Native._CY = conversionData.sourceCY;
    m_Native._BX = conversionData.sourceBX;
    m_Native._BY = conversionData.sourceBY;

	m_Foreign._CX = conversionData.targetCX;
    m_Foreign._CY = conversionData.targetCY;
    m_Foreign._BX = conversionData.targetBX;
    m_Foreign._BY = conversionData.targetBY;
    
	// Determine transformation matrix	
	TransformationMatrix(&m_Native, &m_Foreign);
}


void CoordinateConverter::TransformationMatrix(D3DXMATRIX *mNative, D3DXMATRIX *mForeign)
{
	int Native_YLong,Foreign_YLong,Native_Rhanded,Foreign_Rhanded;
	double XTranslation,YTranslation;
	double Xscale, Yscale, theta;
    D3DXMATRIX mOperator;
    D3DXMATRIX mTransformer;
    D3DXMATRIX mR1, mR2;
	bool swapped_axes;
	bool different_hands;

    swapped_axes = false;
    different_hands = false;

    D3DXMatrixIdentity(&mOperator);
    D3DXMatrixIdentity(&mTransformer);

    XTranslation = mNative->_CX + (mNative->_BX - mNative->_CX) / 2; 
	YTranslation = mNative->_CY + (mNative->_BY - mNative->_CY) / 2;

    mTransformer._41 = mOperator._41 = -XTranslation;
    mTransformer._42 = mOperator._42 = -YTranslation;

	/*Determine along which axis the longest dimension lies*/
	if (fabs(mNative->_CY - mNative->_BY) > fabs(mNative->_CX - mNative->_BX))
		Native_YLong = TRUE;
	else
		Native_YLong = FALSE;

	if (fabs(mForeign->_CY - mForeign->_BY) > fabs(mForeign->_CX - mForeign->_BX))
		Foreign_YLong = TRUE;
	else
		Foreign_YLong = FALSE;

	// Determine 'handedness' of each microscope
	if (Native_YLong == TRUE) 
	{
		if (mNative->_CX < mNative->_BX)
			Native_Rhanded = TRUE;
		else
			Native_Rhanded = FALSE;
	}
	else 
	{
		if (mNative->_CY > mNative->_BY)
			Native_Rhanded = TRUE;
		else
			Native_Rhanded = FALSE;
	}

	if (Foreign_YLong == TRUE) 
	{
		if (mForeign->_CX < mForeign->_BX)
			Foreign_Rhanded = TRUE;
		else
			Foreign_Rhanded = FALSE;
	}
	else 
	{
		if (mForeign->_CX > mForeign->_BX)
		{
			if (mForeign->_CY > mForeign->_BY)
				Foreign_Rhanded = FALSE;
			else
				Foreign_Rhanded = TRUE;
		}
		else 
		{
			if (mForeign->_CY > mForeign->_BY)
				Foreign_Rhanded = TRUE;
			else
				Foreign_Rhanded = FALSE;
		}	 
	}
	
	// Are the X and Y axes the same on each microscope ?
	if (Native_YLong != Foreign_YLong)
	{ 
		// switch axes
		theta =  -M_PI / 2.;	
		swapped_axes = true;
	}
	else 
	{
		theta = 0.;
		swapped_axes = false;
	}

	//Is the 'handedness' the same on each microscope ?
	if (Native_Rhanded != Foreign_Rhanded) 
	{
		//switch 'handedness'
		theta -= M_PI;		
		different_hands = true;
	}
	else
		different_hands = false;

	int XMirror = 1;
	int YMirror = 1;

	if (!different_hands && swapped_axes)
	{
		XMirror = 1;	//leave x-axis as is	
		YMirror = -1;	//flip the y-axis
	}

    D3DXMatrixRotationZ(&mOperator, theta);
    D3DXMatrixMultiply(&mR1, &mTransformer, &mOperator);
    D3DXMatrixIdentity(&mOperator);

	// scale to 'size' of foreign microscope
	if (swapped_axes) 
	{
		Xscale = fabs (mForeign->_CX - mForeign->_BX) / fabs (mNative->_CY - mNative->_BY);
		Yscale = fabs (mForeign->_CY - mForeign->_BY) / fabs (mNative->_CX - mNative->_BX);
	}
	else 
	{
		Xscale = fabs (mForeign->_CX - mForeign->_BX) / fabs (mNative->_CX - mNative->_BX);
		Yscale = fabs (mForeign->_CY - mForeign->_BY) / fabs (mNative->_CY - mNative->_BY);
	}

    mOperator._11 = Xscale * XMirror;
    mOperator._22 = Yscale * YMirror;

    D3DXMatrixMultiply(&mR2, &mR1, &mOperator);

	// Translate to Foreign position
	XTranslation = mForeign->_CX + (mForeign->_BX - mForeign->_CX) / 2; 
	YTranslation = mForeign->_CY + (mForeign->_BY - mForeign->_CY) / 2;

    D3DXMatrixIdentity(&mOperator);

    mOperator._41 = XTranslation;
    mOperator._42 = YTranslation;

    D3DXMatrixMultiply(&m_Transform, &mR2, &mOperator);
    D3DXMatrixInverse(&m_InverseTransform, NULL, &m_Transform);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Calculate the scales and offsets for an XY reader
// then just use y = mx + c
///////////////////////////////////////////////////////////////////////////////////////////////////

void CoordinateConverter::CalculateXYReaderTransform(const ConversionData& CD)
{
    m_ScaleFXNY = (CD.sourceCY - CD.sourceBY) / (CD.targetCX - CD.targetBX);    // Foreign X native Y
    m_ScaleFYNX = (CD.sourceCX - CD.sourceBX) / (CD.targetCY - CD.targetBY);    // Foreign Y native X

    m_OfsFYNX = CD.sourceBX - m_ScaleFYNX * CD.targetBY;
    m_OfsFXNY = CD.sourceCY - m_ScaleFXNY * CD.targetCX;
}

// Native to Foreign
void CoordinateConverter::ConvertXYReaderFromIdeal(double Nx, double Ny, double &Fx, double &Fy)
{
    Fy = (Nx - m_OfsFYNX) / m_ScaleFYNX;
    Fx = (Ny - m_OfsFXNY) / m_ScaleFXNY;
}

// Foreign to Native
void CoordinateConverter::ConvertXYReaderToIdeal(double Fx, double Fy, double &Nx, double &Ny)
{
    Nx = Fy * m_ScaleFYNX + m_OfsFYNX;
    Ny = Fx * m_ScaleFXNY + m_OfsFXNY;
}
