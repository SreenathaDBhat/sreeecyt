/*****************************************************************************
              Copyright (c) 2002 Applied Imaging International
 
	Source file:    DebugUtils.cpp
 
	Function:       Collection of useful error and debug routines

	Package:        Helper library part of MDS2
 
	Modification history:
	Author      Date        Description
	SN          04Apr02     ReviewUtils.cpp
							Initial implementation
	SN			23Sep03		Moved to Helper to support CFragment class.
 
 ****************************************************************************/

/** STANDARD LIBRARY HEADERS ************************************************/

/** OTHER HEADER FILES ******************************************************/
#include "stdafx.h"		// precompiled header

#include "DebugUtils.h"


/** MACROS ******************************************************************/

/** TYPEDEFS, STRUCTS *******************************************************/

/** PRIVATE FUNCTION PROTOTYPES *********************************************/

/** PUBLIC DECLARATIONS (and externs) ***************************************/

/** PRIVATE DECLARATIONS (statics) ******************************************/

/** DEBUG only DECLARATIONS and FUNCTIONS ***********************************/


/** IMPLEMENTATION **********************************************************/

//
// For COM error handling
//
void DumpComError(_com_error &ce)
{
	CString comStr;
	//
	// Compose COM error information.
	//
	comStr.Format( "\nCom Exception Information\n" \
	               "Source      : %s\n"            \
	               "Description : %s\n"            \
	               "Message     : %s\n"            \
	               "HResult     : 0x%08x\n",
				   (char*) ce.Source(),
				   (char*) ce.Description(),
				   ce.ErrorMessage(),
				   ce.Error() );

	// Dump it everywhere
	fprintf( stderr, comStr);
	fflush( stderr);
	TRACE( comStr);
	AfxMessageBox( comStr);
}

//
// For ADO error handling
//
void DumpADOError( _ConnectionPtr pConn)
{
	if (pConn != NULL)
	{
		CString adoStr, tempStr;
		//
		// Compose ADO error information.
		//
		adoStr = "\nADO Exception Information\n";
 		ErrorPtr err;
		for ( long i=0; i<pConn->Errors->Count; i++ ) 
		{
			err = pConn->Errors->Item[i];
			tempStr.Format( "Source      : %s\n",     (char*) err->Source );		adoStr += tempStr;
			tempStr.Format( "Description : %s\n",	  (char*) err->Description );	adoStr += tempStr;
			tempStr.Format( "SQLState    : %s\n",     (char*) err->SQLState );		adoStr += tempStr;
			tempStr.Format( "Number      : 0x%08x\n", err->Number );				adoStr += tempStr;
			tempStr.Format( "NativeError : %d\n",     err->NativeError);	        adoStr += tempStr;
		}

		// Dump it everywhere
		fprintf( stderr, adoStr);
		fflush( stderr);
		TRACE( adoStr);
		AfxMessageBox( adoStr);
	}
}

void DumpElapsedTime( CTime endTime, CTime startTime, LPCTSTR szOut)
{
	CTimeSpan elapsedTime = endTime - startTime;
	long lTotalSeconds = (long) elapsedTime.GetTotalSeconds();

	if (lTotalSeconds > 0)
	{
		static char szmsg[256] = {0};
		sprintf( szmsg, "%s = %ld.\n", szOut, lTotalSeconds);

		TRACE( szmsg);
		fprintf( stderr,   szmsg);
		fflush( stderr);
	}
}


/*****************************************************************************
              Copyright (c) 2002 Applied Imaging International
 ****************************************************************************/
