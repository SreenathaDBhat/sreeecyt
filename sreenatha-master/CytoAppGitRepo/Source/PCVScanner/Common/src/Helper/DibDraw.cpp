//=================================================================================
// Dib.cpp
// Base class for dib drawing
//
// History:
//
// 28Feb00	WH:	original
// 03May01	dcb	Fix bitmap height calculation in CDIBDraw::Draw
//				Add CDIBDraw::DrawSubRect
//=================================================================================
#include "StdAfx.h"
#include "DibDraw.h"
#include <assert.h>

//=================================================================================
//=================================================================================
CDIBDraw::CDIBDraw()
{
	m_DisplayPal = NULL;	// logical palette
	m_pPal = NULL;			// display palette
	m_fnOverlay = NULL;		// overlay function

	// DIB header
	memset(&m_DibInfo, 0, sizeof(m_DibInfo));
	m_DibInfo.bmiHeader.biPlanes = 1; 
	m_DibInfo.bmiHeader.biBitCount = 8; 
	m_DibInfo.bmiHeader.biCompression = BI_RGB; 
	m_DibInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);

	// Use palette indices which will get converted to RGB if 24 bit
	for (int i = 0; i < 256; i++)
		m_DibInfo.bmiColors[i] = i;

	// Use palette indices which will get converted to RGB if 24 bit
	m_pPal = (LOGPALETTE*) malloc(sizeof(LOGPALETTE) +(256 * sizeof(PALETTEENTRY)));

	m_pPal->palNumEntries=256;	
	m_pPal->palVersion = 0x300;

	for (int i=0; i<256; i++ ) 
	{ 
		m_pPal->palPalEntry[i].peRed  =i;
		m_pPal->palPalEntry[i].peGreen=i;
		m_pPal->palPalEntry[i].peBlue =i;
		m_pPal->palPalEntry[i].peFlags=0;
	}

	m_DisplayPal = CreatePalette(m_pPal);   // this creates a "logical palette"
	bUpdatePal = FALSE;
}
//=================================================================================
//=================================================================================
CDIBDraw::~CDIBDraw()
{
	if (m_DisplayPal)
		DeleteObject(m_DisplayPal);

	if (m_pPal)
		free(m_pPal);
}

//=================================================================================
// Draw dib to window
//	hWnd		- window to draw
//  image		- image data
//  pDst		- Destination RECT on window
//  pSrc		- Data ROI within image
//  pClient		- Size of client area
//=================================================================================
void CDIBDraw::Draw(HWND hWnd, BYTE *image, RECT* pDst, RECT *pSrc, RECT *pClient, UINT bpp)
{
	HDC memdc;
	HBITMAP hbm;
	HGDIOBJ oldobj;
	HDC hDstDC = GetDC(hWnd);

	m_DibInfo.bmiHeader.biWidth = pSrc->right - pSrc->left;
	m_DibInfo.bmiHeader.biHeight = -(int)(pSrc->bottom - pSrc->top);
	m_DibInfo.bmiHeader.biBitCount = bpp; 

	// do we have to update the palette before drawing
	if (bUpdatePal)
	{
		if(m_DisplayPal)
			DeleteObject(m_DisplayPal);

		m_DisplayPal = CreatePalette(m_pPal);   // this creates a "logical palette"
		bUpdatePal = FALSE;
	}

	// Create handle to memory compatible DC
	if ( (memdc = CreateCompatibleDC(hDstDC)) == NULL )
	{
		ReleaseDC(hWnd, hDstDC);
		return;
	}

	SetBkMode(memdc, TRANSPARENT);
	// Create bitmap
	if ( (hbm = CreateCompatibleBitmap(hDstDC, pClient->right - pClient->left, pClient->bottom- pClient->top)) == NULL )
	{
		ReleaseDC(hWnd, hDstDC);
		DeleteDC( memdc);
		TRACE("DibDraw: CreateCompatibleBitmap error\n\r");
		return;
	}	

		// Attach bitmap to memory compatible DC
	oldobj = (HGDIOBJ)SelectObject( memdc, hbm);

	SelectPalette(memdc, m_DisplayPal, FALSE);
	RealizePalette(memdc);


	SetStretchBltMode(memdc,STRETCH_DELETESCANS);


	if (StretchDIBits(memdc, 
						pDst->left, pDst->top, pDst->right - pDst->left, pDst->bottom - pDst->top, 
						pSrc->left, pSrc->top, pSrc->right - pSrc->left, pSrc->bottom - pSrc->top, 
						image, (BITMAPINFO *)&m_DibInfo, DIB_PAL_COLORS, SRCCOPY) == GDI_ERROR)
		{
			TRACE("DibDraw: StretchDIBits error\n\r");
		}

		// Apply custom overlay
	if (m_fnOverlay)
		{
			m_fnOverlay(memdc, pDst->left, pDst->top, pDst->right - pDst->left, pDst->bottom - pDst->top, pClient);
		}

		// Blat everything to the window
	BitBlt(hDstDC, pClient->left, pClient->top, pClient->right - pClient->left, pClient->bottom- pClient->top, memdc, 0, 0, SRCCOPY);

		// Tidy up
	SelectObject(memdc, oldobj);
	DeleteObject(hbm);
	DeleteDC(memdc);
	
	ReleaseDC(hWnd, hDstDC);
}

//=================================================================================
//	DrawSubRect - Like draw except bitmap size is specified
//	This allows for drawing a sub rectangle within an image and does
//	not fix the bitmap width as the source rectangle width
//
//	hWnd		- window to draw
//  image		- image data
//  pDst		- Destination RECT on window
//  pSrc		- Data ROI within image
//  pBmp		- Size of bitmap
//
// Note: Overlay code is not yet tested:
//=================================================================================
void CDIBDraw::DrawSubRect(HWND hWnd, BYTE *image, RECT* pDst, RECT *pSrc, RECT *pBmp, UINT bpp)
{
	HDC memdc;
	HBITMAP hbm;
	HGDIOBJ oldobj;
	HDC hDstDC = GetDC(hWnd);

	m_DibInfo.bmiHeader.biWidth = pBmp->right - pBmp->left;
	m_DibInfo.bmiHeader.biHeight = -(int)(pBmp->bottom - pBmp->top);
	m_DibInfo.bmiHeader.biBitCount = bpp; 

	// do we have to update the palette before drawing
	if (bUpdatePal)
	{
		if(m_DisplayPal)
			DeleteObject(m_DisplayPal);

		m_DisplayPal = CreatePalette(m_pPal);   // this creates a "logical palette"
		bUpdatePal = FALSE;
	}

	if (!m_fnOverlay)	// Just do a stretchDibBlt if no overlay
	{
		SelectPalette(hDstDC, m_DisplayPal, FALSE);
		RealizePalette(hDstDC);

		SetStretchBltMode(hDstDC, STRETCH_DELETESCANS);

		if (StretchDIBits(hDstDC, 
							pDst->left, pDst->top, pDst->right - pDst->left, pDst->bottom - pDst->top, 
							pSrc->left, pSrc->top, pSrc->right - pSrc->left, pSrc->bottom - pSrc->top, 
							image, (BITMAPINFO *)&m_DibInfo, DIB_PAL_COLORS, SRCCOPY) == GDI_ERROR)
		{
			TRACE("CDIBDraw::DrawSubRect: StretchDIBits error\n\r");
		}
	}
	else	// Otherwise do it is a memory device context so it doesn't flash
	{
			// Create handle to memory compatible DC
		if ( (memdc = CreateCompatibleDC(hDstDC)) == NULL )
		{
			ReleaseDC(hWnd, hDstDC);
			return;
		}

		SetBkMode(memdc, TRANSPARENT);
		// Create bitmap
		if ( (hbm = CreateCompatibleBitmap(hDstDC, pBmp->right - pBmp->left, pBmp->bottom- pBmp->top)) == NULL )
		{
			ReleaseDC(hWnd, hDstDC);
			DeleteDC( memdc);
			TRACE("CDIBDraw::DrawSubRect: CreateCompatibleBitmap error\n\r");
			return;
		}

		// Attach bitmap to memory compatible DC
		oldobj = (HGDIOBJ)SelectObject( memdc, hbm);

		SelectPalette(memdc, m_DisplayPal, FALSE);
		RealizePalette(memdc);


		SetStretchBltMode(memdc,STRETCH_DELETESCANS);


		if (StretchDIBits(memdc, 
							pDst->left, pDst->top, pDst->right - pDst->left, pDst->bottom - pDst->top, 
							pSrc->left, pSrc->top, pSrc->right - pSrc->left, pSrc->bottom - pSrc->top, 
							image, (BITMAPINFO *)&m_DibInfo, DIB_PAL_COLORS, SRCCOPY) == GDI_ERROR)
		{
			TRACE("CDIBDraw::DrawSubRect: StretchDIBits error\n\r");
		}

		// Apply custom overlay
		if (m_fnOverlay)
		{
			m_fnOverlay(memdc, pDst->left, pDst->top, pDst->right - pDst->left, pDst->bottom - pDst->top, pBmp);
		}

		// Blat everything to the window
//		BitBlt(hDstDC, pClient->left, pClient->top, pClient->right - pClient->left, pClient->bottom- pClient->top, memdc, 0, 0, SRCCOPY);
		BitBlt(hDstDC, pDst->left, pDst->top, pDst->right - pDst->left, pDst->bottom- pDst->top, memdc, 0, 0, SRCCOPY);

		// Tidy up
		SelectObject(memdc, oldobj);
		DeleteObject(hbm);
		DeleteDC(memdc);
	}

	ReleaseDC(hWnd, hDstDC);
}

//=================================================================================
//=================================================================================
void CDIBDraw::OutputLUTEntry(UINT index, UINT red, UINT green, UINT blue) 
{
	assert(m_pPal);

	m_pPal->palPalEntry[index].peRed  =red; 
	m_pPal->palPalEntry[index].peGreen=green;
	m_pPal->palPalEntry[index].peBlue =blue;

	// Indicate the palette must be recreated before drawing
	bUpdatePal = TRUE;
}

//=================================================================================
//=================================================================================
