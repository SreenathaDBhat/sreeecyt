#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <io.h>
#include "atlconv.h"

   typedef BOOL (WINAPI *P_GDFSE)(LPCTSTR, PULARGE_INTEGER, 
                                  PULARGE_INTEGER, PULARGE_INTEGER);


#include "..\..\Common\Src\NLog\src\NLogC\NLogC.h"

unsigned long DiskFree(LPCTSTR UNCPath)
{
	  BOOL  fResult;




	  unsigned long  FreeSpace = 0;

	  P_GDFSE pGetDiskFreeSpaceEx = NULL;

	  unsigned __int64 i64FreeBytesToCaller,
					   i64TotalBytes,
					   i64FreeBytes;	

	  /*
		 Use GetDiskFreeSpaceEx if available; otherwise, use
		 GetDiskFreeSpace.

		 Note: Since GetDiskFreeSpaceEx is not in Windows 95 Retail, we
		 dynamically link to it and only call it if it is present.  We 
		 don't need to call LoadLibrary on KERNEL32.DLL because it is 
		 already loaded into every Win32 process's address space.
	  */ 
	  pGetDiskFreeSpaceEx = (P_GDFSE)GetProcAddress (
							   GetModuleHandle ("kernel32.dll"),
												"GetDiskFreeSpaceExA");
	  if (pGetDiskFreeSpaceEx)
	  {
		 fResult = pGetDiskFreeSpaceEx (UNCPath,
								 (PULARGE_INTEGER)&i64FreeBytesToCaller,
								 (PULARGE_INTEGER)&i64TotalBytes,
								 (PULARGE_INTEGER)&i64FreeBytes);
		 if (fResult)
		 {
			printf ("\n\nGetDiskFreeSpaceEx reports\n\n");
			printf ("Available space to caller = %I64u MB\n",
					i64FreeBytesToCaller / (1024*1024));
			printf ("Total space               = %I64u MB\n",
					i64TotalBytes / (1024*1024));
			printf ("Free space on drive       = %I64u MB\n",
					i64FreeBytes / (1024*1024));

			FreeSpace = i64FreeBytesToCaller / (1024*1024); 
		 }
	  }
	  
	  if (!fResult)
		 printf ("error: %lu:  could not get free space for \"%s\"\n",
				 GetLastError(), UNCPath);

	  return FreeSpace;
}



BOOL CheckNetworkFileAccess(LPCTSTR NetworkPath)
{
	// Check read/write permissions
    if (_access(NetworkPath, 0x6) == 0)
       return TRUE;

	return FALSE;
}