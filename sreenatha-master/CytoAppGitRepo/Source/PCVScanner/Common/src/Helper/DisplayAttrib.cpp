// DisplayAttrib.cpp: implementation of the CDisplayAttrib class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DisplayAttrib.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDisplayAttribRep::CDisplayAttribRep()
:	nRC(1),
	bSelected(FALSE),
	bVisible(TRUE),
	bColourSet(FALSE),
	colour(RGB(0,0,0))				// default to black
{

}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDisplayAttrib::CDisplayAttrib()
	: pRep( new CDisplayAttribRep )
{
	ASSERT( pRep != NULL );
}

CDisplayAttrib::CDisplayAttrib( const CDisplayAttrib & rhs)
{
	Copy( rhs);
}

const CDisplayAttrib & CDisplayAttrib::operator=( const CDisplayAttrib & rhs)
{
	// Check for self assignment
	if (this != &rhs)
	{
		// Reclaim existing memory
		Free();

		// Allocate mem and copy
		Copy( rhs);
	}

	// Return ref to current object for a=b=c assignments
	return *this;
}

CDisplayAttrib::~CDisplayAttrib()
{
	Free();
}

void CDisplayAttrib::Copy( const CDisplayAttrib & attrib)
{
	ASSERT( attrib.pRep != NULL );
	pRep = attrib.pRep;
	pRep->nRC++;
}

void CDisplayAttrib::Free( void )
{
	ASSERT( pRep != NULL );
	pRep->nRC--;
	if (pRep->nRC == 0)
	{
		delete pRep;
		pRep = NULL;
	}
}