// DrawWnd.cpp : implementation file
//

#include "stdafx.h"
#include "DrawWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDrawWnd

CScaledBitmap::CScaledBitmap()
{
	Rect.left   = 0;
	Rect.top    = 0;
	Rect.right  = 25000;
	Rect.bottom = 75000;
}
CScaledBitmap::~CScaledBitmap()
{
}



CDrawWnd::CDrawWnd()
{
	WNDCLASS c;
	HINSTANCE hinstance = AfxGetInstanceHandle();

	c.style = CS_PARENTDC;
	c.lpfnWndProc = ::DefWindowProc;
	c.cbClsExtra = 0;
	c.cbWndExtra = 0;
	c.hInstance = hinstance;
	c.hIcon = 0;
	c.hCursor = NULL;	// No cursor as we change ours all the time
	c.hbrBackground = (HBRUSH)COLOR_BTNFACE;
	c.lpszMenuName = NULL;
	c.lpszClassName = "DrawWnd";

	RegisterClass(&c);

	// Initialize some things
	m_LbuttonDown = m_RbuttonDown = FALSE;


}

CDrawWnd::~CDrawWnd()
{
	Clear();
}


BEGIN_MESSAGE_MAP(CDrawWnd, CWnd)
	//{{AFX_MSG_MAP(CDrawWnd)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_PAINT()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CDrawWnd message handlers
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
BOOL CDrawWnd::Create(LPCTSTR lpszWindowName, RECT r, CWnd* pParentWnd, int XScale, int YScale)
{
	DWORD style;

	style = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

	m_XScale = XScale;
	m_YScale = YScale;

	return CWnd::CreateEx(0, "DrawWnd",
							lpszWindowName, style, r.left, r.top, r.right - r.left, r.bottom - r.top,
							pParentWnd->m_hWnd, NULL, NULL);
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
int CDrawWnd::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawWnd::OnDestroy() 
{
	// Free any event handlers
	for(POSITION pos = m_eventhandlers.GetHeadPosition(); pos != NULL; )
	{
		CEH_Item * item;
		if((item = m_eventhandlers.GetNext( pos )))
			delete item;
	}
	m_eventhandlers.RemoveAll();

	CWnd::OnDestroy();
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawWnd::OnSize(UINT nType, int cx, int cy) 
{
	// Handle resizing of window for Full screen etc
	CWnd::OnSize(nType, cx, cy);
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawWnd::AttachScaledBitmap(CScaledBitmap *scaledBitmap, WORD action)
{

	POSITION pos = NULL;
	CScaledBitmap* item;

	if (action == CDrawWnd::AddBmp)
		m_ImList.AddTail(scaledBitmap);
	else
	{
		for(pos = m_ImList.GetHeadPosition(); pos != NULL; )
		{
			item = m_ImList.GetAt( pos );
			if (item == scaledBitmap)
			{
				POSITION delpos = pos;
				m_ImList.GetNext( pos );
				m_ImList.RemoveAt( delpos );
				delete (CScaledBitmap*)item;
			}
			else
				m_ImList.GetNext( pos );
		}
	}

}

void CDrawWnd::Clear()
{
	for (POSITION pos = m_ImList.GetHeadPosition(); pos != NULL; )
	{
		CScaledBitmap *scaledBitmap = m_ImList.GetAt( pos );
		delete scaledBitmap;
		m_ImList.GetNext( pos );
	}

	m_ImList.RemoveAll();

}
//--------------------------------------------------------------------------------------------------
//						  Event handler stuff
//--------------------------------------------------------------------------------------------------
void CDrawWnd::AttachEventHandler(EVHANDLER func, DWORD data, WORD action)
{
	// action can be AddHandler, DeleteHandler
	CEH_Item *item;
	POSITION pos = NULL;

	// Quick check that its not already in the queue, don't add more than 1 of the same function
	if (action == CDrawWnd::AddHandler)
	{
		for(pos = m_eventhandlers.GetHeadPosition(); pos != NULL; )
		{
			item = m_eventhandlers.GetAt( pos );
			if (item->efunc == func)
				return;
			m_eventhandlers.GetNext( pos );
		}
	}

	// Handle action
	switch(action)
	{
	case CDrawWnd::AddHandler: // Add handler function at end
		item = new CEH_Item;
		item->efunc = func;
		item->data = data;
		m_eventhandlers.AddTail(item);
		break;
	case CDrawWnd::DeleteHandler: // delete handler function 
		for(pos = m_eventhandlers.GetHeadPosition(); pos != NULL; )
		{
			item = m_eventhandlers.GetAt( pos );
			if (item->efunc == func)
			{
				POSITION delpos = pos;
				m_eventhandlers.GetNext( pos );
				m_eventhandlers.RemoveAt( delpos );
				delete item;
			}
			else
				m_eventhandlers.GetNext( pos );
		}
		break;
	default:
		TRACE("AttachEventHandler: Unknown action!\r\n");
		return;
	}
}
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawWnd::HandleEvents(UINT event, UINT nFlags, long x, long y)
{
	// Propogate event through list of event handlers
	CEH_Item *item;

	x *= m_XScale;
	y *= m_YScale;

	for(POSITION  pos = m_eventhandlers.GetHeadPosition(); pos != NULL; )
	{
		item = m_eventhandlers.GetNext( pos );
		if (item)
			item->efunc(event, nFlags, x, y, item->data);
	}
}
//--------------------------------------------------------------------------------------------------
//						  END - Event handler stuff
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
void CDrawWnd::Modified(BOOL bRender)
{
	// Called to indicate that the tiff state has changed, reconfigure and display
	InvalidateRect(NULL);
}

void CDrawWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	m_LbuttonDown = TRUE;
	HandleEvents(WM_LBUTTONDOWN, nFlags, point.x, point.y);
	
	CWnd::OnLButtonDown(nFlags, point);
}

void CDrawWnd::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	m_LbuttonDown = FALSE;
	HandleEvents(WM_LBUTTONUP, nFlags, point.x, point.y);
	
	CWnd::OnLButtonUp(nFlags, point);
}

void CDrawWnd::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	m_RbuttonDown = TRUE;
	HandleEvents(WM_RBUTTONDOWN, nFlags, point.x, point.y);
	
	CWnd::OnRButtonDown(nFlags, point);
}

void CDrawWnd::OnRButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	m_RbuttonDown = FALSE;
	HandleEvents(WM_RBUTTONUP, nFlags, point.x, point.y);
	
	CWnd::OnRButtonUp(nFlags, point);
}

void CDrawWnd::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	HandleEvents(WM_MOUSEMOVE, nFlags, point.x, point.y);

	CWnd::OnMouseMove(nFlags, point);
}


void CDrawWnd::OnPaint() 
{
	CRect r, cr;
	POSITION pos = NULL;

	// Get the exposed rectangle
	if (GetUpdateRect(&r) == FALSE) // Not actually used at the moment, just update the whole thing.
		return;

	GetClientRect(&cr);

	CDC *pDC = GetDC();
	CDC dcCompatible;
	dcCompatible.CreateCompatibleDC(pDC);

    pDC->SetStretchBltMode(COLORONCOLOR);

	if (r == cr)
		pDC->FillSolidRect(&cr, GetSysColor(COLOR_3DFACE));


	// Draw the image (or part of) at the current offsets
	for (pos = m_ImList.GetHeadPosition(); pos != NULL; )
	{
   		CScaledBitmap *scaledBitmap = m_ImList.GetAt( pos );
   
  		void* pold = dcCompatible.SelectObject(scaledBitmap);
   
   		BITMAP binfo;
  		scaledBitmap->GetObject(sizeof(binfo), &binfo);
 
		int x = (scaledBitmap->Rect.left)/m_XScale;
		int y = (scaledBitmap->Rect.top)/m_YScale;
		int w = (scaledBitmap->Rect.right - scaledBitmap->Rect.left)/m_XScale;
		int h = (scaledBitmap->Rect.bottom - scaledBitmap->Rect.top)/m_YScale;
		int rop = scaledBitmap->Rop;
   		pDC->StretchBlt(x,y,w,h,&dcCompatible,0,0,binfo.bmWidth,binfo.bmHeight,rop);
   
   		dcCompatible.SelectObject(pold);
   		m_ImList.GetNext( pos );
	}

	ReleaseDC(pDC);

	// Send paint event to external event handler incase it does something with it
	HandleEvents(WM_PAINT, 0, 0, 0);
	ValidateRect(NULL);


}

BOOL CDrawWnd::OnEraseBkgnd(CDC *pDC)
{
	return TRUE;
}
