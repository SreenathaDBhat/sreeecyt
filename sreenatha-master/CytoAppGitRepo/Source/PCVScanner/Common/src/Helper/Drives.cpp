#include "stdafx.h"
#include "drives.h"
#include <cfuncs.h> // For DeleteDirectoryTree()

//**************************************************************************************************
// CDrives Object, Small class for general drive/disk related functions
//**************************************************************************************************

//==================================================================================================
// Loads array will all available archive drivesa:\:e:\ g:\ or what ever
//==================================================================================================
CDrives::CDrives()
{
	/*
	 * Add local removable media (always want access to these).
	 */
	char driveStrings[64];
	DWORD length, i = 0;
	length = GetLogicalDriveStrings(sizeof(driveStrings), driveStrings);

	while (i < length)
	{
		if ( GetDriveType(driveStrings + i) == DRIVE_FIXED   
			|| GetDriveType(driveStrings + i) == DRIVE_REMOVABLE
			|| GetDriveType(driveStrings + i) == DRIVE_REMOTE
		    || GetDriveType(driveStrings + i) == DRIVE_CDROM    ) // DRIVE_REMOVABLE doesn't include DRIVE_CDROM
		{
			// Logical drives strings specify the root directory of each drive,
			// e.g. C:\, and this format is required by GetDriveType(), but 
			// only display to the user the drive itself.
			m_strArray.Add(driveStrings + i);
		}
		i += strlen(driveStrings + i) + 1;
	}
}
//==================================================================================================
// Load a passed combo box will available archive drives a:\:e:\ g:\ or what ever
//==================================================================================================
void CDrives::LoadCombo(CComboBox & box)
{
	box.Clear();

	int max = m_strArray.GetSize();
	for (int i = 0;i < max; i++)
	{
		box.AddString(m_strArray.GetAt(i));
	}
}
//==================================================================================================
// Pinched from cytovision, writes label into a LABEL file in root dir. Also labels Volume but this
// not used.
//==================================================================================================
BOOL CDrives::WriteArchiveDiskLabel(LPCTSTR rootDir, LPCTSTR label)
{
/* 
 * E.g. WriteArchiveDiskLabel("E:\", "mydisk"); 
 */
	CString Path;
	FILE *pFILE;
	BOOL rv = FALSE;

	Path = rootDir;	
	Path += "LABEL"; 

	if ((pFILE = _tfopen(Path, _T("wb"))) != NULL)
	{
		fwrite(label, 1, _tcslen(label), pFILE);

		if (!ferror(pFILE) && fclose(pFILE) == 0) 
		{
			rv = TRUE;

			// Also set volume label to avoid confusion. Don't check whether this works though, as the file is the 
			// only official place for the label. 
			SetVolumeLabel(rootDir, label);
		}
	}

	return rv;
}
//==================================================================================================
// Pinched from cytovision, Label is held in the LABEL file, does not read volume label
//==================================================================================================
BOOL CDrives::ReadArchiveDiskLabel(CString &rootDir, CString &label)
{
	TCHAR path[MAX_PATH], tmpLabel[MAX_PATH];
	FILE *pFILE;
	BOOL rv = FALSE;

	_tcsncpy(path, (LPCTSTR)rootDir, 3); /* Make sure rootDir is just that. */
	path[3] = _T('\0'); /* strncpy does not null terminate if count is short. */

	_tcscat(path, _T("LABEL"));

	if ((pFILE = _tfopen(path, _T("rb"))) != NULL)
	{
		_fgetts(tmpLabel, sizeof(tmpLabel), pFILE);
		label = tmpLabel;

		if (!ferror(pFILE) && fclose(pFILE) == 0) 
			rv = TRUE;
	}
	return rv;
}
//==================================================================================================
// Pinched from cytovision, Adjust for cluster size (actual disk sspace that will be used)
//==================================================================================================
LONGLONG CDrives::ActualRequired(LPCTSTR archdev, LONGLONG required, long numFiles)
{
// This function calculates how much space the files in the given directory tree would take up on the 
// current archive device. This is more than just the sum of the file sizes, because on average, each 
// file will only half occupy its last cluster. There is information on cluster sizes on FAT and NTFS 
// in Knowledge Base articles Q67321, Q140365.
	LONGLONG dirsize = 0;

//	DWORD clusterSize = 512; // Default if cannot get required information about archive disk.
	DWORD clusterSize = 4096; // default Cluster size for volume over 1 GB, expecting GetDiskFreeSpace to fail (only handles 2GB normally)

	DWORD sectorsPerCluster, bytesPerSector, freeClusters, totalClusters;

	if (GetDiskFreeSpace(archdev, &sectorsPerCluster, &bytesPerSector,
	                              &freeClusters, &totalClusters))
	{
		clusterSize = sectorsPerCluster * bytesPerSector;
	}

	// It will be assumed that on average, half a cluster is wasted per file. If we wanted to play it 
	// completely safe, a whole cluster could be assumed to be wasted per file, but this would generally 
	// be an overestimate. However, only assuming half a cluster will sometimes be an underestimate.
	// LETS GO FOR 80% CLUSTER TO BE ON SAFE SIDE
	dirsize = required + ((clusterSize * 8)/ 10) * numFiles;

	return dirsize;
}
//==================================================================================================
// Based on cytovision code, calculate free disk space on selected media
//==================================================================================================
LONGLONG CDrives::GetMediaSpace(LPCTSTR arcdev)
{
	// This function returns 0 on failure (or if there is no free space),
	// or else the amount of free space, in bytes.
	LONGLONG freespace = (LONGLONG)0;
	ULARGE_INTEGER bytesAvailable, bytesTotal, bytesFree;

	if (GetDiskFreeSpaceEx(arcdev, &bytesAvailable, &bytesTotal, &bytesFree))
	{
		freespace = bytesAvailable.QuadPart;
	}

	return freespace;
}
//==================================================================================================
// D e l e t e D i r -- Delete directory and all of its contents.
// Returns TRUE if OK, or FALSE on failure.
//==================================================================================================
BOOL CDrives::DeleteDir(LPCTSTR dirpath)
{
	// Use shared function in CFuncs.cpp.
	return DeleteDirectoryTree(dirpath);
}
//==================================================================================================
//
//	S i z e D i r -- Calculate the size of the directory and all of its contents. Also counts the 
//  number of files (NOT including directories) if passed a	pointer to a long (can pass NULL pointer).
//	Note that numFiles must be initialised to zero by the calling function.	This is a pain, if you can 
//	work out how to get this function to initialise itself (without adding another parameter!) then do so.
//	Returns total size, or 0 on error
//==================================================================================================
DWORD CDrives::SizeDir(LPCTSTR dirpath, int *pNumFiles)
{
	TCHAR path[MAX_PATH];
	WIN32_FIND_DATA fd;
	HANDLE hdir;
	DWORD dirsize;

	// Get directory wildcard path
	_stprintf(path, _T("%s\\*"), dirpath);
	if ((hdir = FindFirstFile(path, &fd)) == INVALID_HANDLE_VALUE)
		return 0;

	dirsize = 0;
	while (FindNextFile(hdir, &fd)) 
	{
		if (strcmp(fd.cFileName, _T("..")) == 0)
			continue;

		// Directories: recursively calculate. Regular files, just use size
		if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) 
		{
			// Build full pathname
			_stprintf(path, _T("%s\\%s"), dirpath, fd.cFileName);
			dirsize += SizeDir(path, pNumFiles);
		} 
		else
		{
			dirsize += fd.nFileSizeLow;

			// Increment file count if pointer not NULL.
			if (pNumFiles != NULL ) (*pNumFiles)++; 
		}
	}

	FindClose(hdir);

	return dirsize;	// Directory size
}

