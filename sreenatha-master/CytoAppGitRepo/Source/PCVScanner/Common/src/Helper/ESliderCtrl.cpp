/*
 *		E S L I D E R C T R L
 *			-- an extended slider control containing text boxes for min, max and position text
 *
 */

#include "stdafx.h"
#include "ESliderCtrl.h"

#define BASECTRL_ID 8

#define SLIDER_HEIGHT 20
#define TEXT_WIDTH 36

BEGIN_MESSAGE_MAP (ESliderCtrl, CWnd)
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_WM_HSCROLL()
END_MESSAGE_MAP()	

ESliderCtrl::ESliderCtrl()
{

}

BOOL ESliderCtrl::Create (UINT Style, const RECT &rect, CWnd *pParentWnd, UINT nID)
{

	return CWnd::Create (NULL, NULL, WS_CHILD | WS_VISIBLE, rect, pParentWnd, nID);
}

void ESliderCtrl::SetRange (int Min, int Max, BOOL Update)
{
	CString Text;

	Text.Format ("%d", Min);
	MinText.SetWindowText ((PCSTR) Text);

	Text.Format ("%d", Max);
	MaxText.SetWindowText ((PCSTR) Text);

	m_SliderCtrl.SetRange (Min, Max, Update);
}

void ESliderCtrl::GetRange (int *pMin, int *pMax)
{

	*pMin = m_SliderCtrl.GetRangeMin();
	*pMax = m_SliderCtrl.GetRangeMax();
}

void ESliderCtrl::SetPos (int Pos)
{
	m_SliderCtrl.SetPos (Pos);
	Update();
}

void ESliderCtrl::GetPos (int *pPos)
{

	*pPos = m_SliderCtrl.GetPos();
}

int ESliderCtrl::OnCreate (LPCREATESTRUCT lpCreateStruct)
{
	RECT rect;

	if (CWnd::OnCreate (lpCreateStruct) == -1)
		return -1;

			//Get font from parent dialog
	CFont *pFont = CWnd::FromHandle (lpCreateStruct->hwndParent)->GetFont();
	GetClientRect (&rect);
	rect.left += TEXT_WIDTH;
	rect.right -= TEXT_WIDTH;

	m_SliderCtrl.Create (WS_CHILD | WS_VISIBLE | TBS_HORZ | TBS_BOTTOM | TBS_NOTICKS,
								rect, this, BASECTRL_ID + 1);

	GetClientRect (&rect);
	rect.right = rect.left + TEXT_WIDTH;
	MinText.Create ("",WS_CHILD | WS_VISIBLE | SS_RIGHT, rect, this);
	MinText.SetFont (pFont);

	GetClientRect (&rect);
	rect.left = rect.right - TEXT_WIDTH;
	MaxText.Create ("", WS_CHILD |WS_VISIBLE, rect, this);
	MaxText.SetFont (pFont);
	
	GetClientRect (&rect);
	rect.top = rect.bottom + (rect.top - rect.bottom) / 2;
	rect.left = rect.right - TEXT_WIDTH;
	PosText.Create ("", WS_CHILD | WS_VISIBLE, rect, this);
	PosText.SetFont (pFont);
	return 0;

}

void ESliderCtrl::Update()
{
	CString Text;
	int pos;


	pos = m_SliderCtrl.GetPos();
	Text.Format ("%d", pos);
	PosText.SetWindowText ((PCSTR) Text);
}

void ESliderCtrl::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	m_SliderCtrl.ShowWindow(bShow);
}

void ESliderCtrl::OnHScroll (UINT nSBCode, UINT nPos, CScrollBar *pScrollBar)
{

	Update();

	CWnd *pParent = GetParent();

	if (pParent) 
		pParent->PostMessage (WM_HSCROLL, MAKEWPARAM (nSBCode, nPos), (LPARAM)m_hWnd);

	CWnd ::OnHScroll (nSBCode, nPos, pScrollBar);
}