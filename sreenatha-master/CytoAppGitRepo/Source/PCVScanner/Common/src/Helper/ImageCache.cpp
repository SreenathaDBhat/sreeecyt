
/*
 * ImageCache.cpp
 *
 * JMB August 2001
 *
 * The bottleneck when searching for an item in the cache by name is the string
 * comparisons. Maintaining a CMap of name to cache item means that such
 * searches can be done quicker, as searching a CMap is quicker than searching
 * the linked list. The CMap muct be maintained alongside the linked list,
 * rather than replacing it, as the order of the items in the linked list
 * determines which items are discarded first when the cache becomes full.
 * Use of the CMap is enabled only if USE_IMAGECACHE_MAP is defined
 * in ImageCache.h.
 *
 * An unlocked pointer returned by the cache may have a limited lifetime, as
 * the cache may subsequently delete it to free up space. Locking a cache item
 * (particularly important for multithreaded code) will prevent it being
 * deleted while a pointer to it is being used, however locks on items prevent
 * the cache preforming housekeeping operations properly so should be
 * maintained for as short a time as possible (ideally for no more than a
 * single block of code). Once an item has been unlocked the pointer to it may
 * become invalid, so next time a pointer to it is required it should be
 * requested from the cache again. However, doing this can be slow as it means
 * searching the cache every time. In order to speed up this process it is
 * possible to pass two extra parameters to AITiffImageCache::GetItem(): the
 * pointer last returned for this item and a state value which allows the cache
 * to check whether it has deleted any items since that pointer was returned.
 * If the state value has not changed then the pointer will not have changed
 * either and it can be returned without having to search for it again.
 */


#include "stdafx.h" // Precompiled header crap

#include "ImageCache.h"

#include <assert.h>



//////////////////////////////////////////////////////////////////////////////



ImageCache::ImageCache()
{
	pImageList = NULL;
	count = 0;
	maxCount = 1000000;

	InitializeCriticalSectionAndSpinCount(&criticalSection, 4000);
}

ImageCache::~ImageCache()
{
	DeleteCriticalSection(&criticalSection);
}


void
ImageCache::Initialise(const char imageDirectory[], int maxNumImages)
{
	sourceDirectory = imageDirectory;

	maxCount = maxNumImages;
}


ImageCacheItem*
ImageCache::RemoveOldestItem()
{
// Unlink the unlocked item that has been in the list longest.
	ImageCacheItem *pOldestUnlockedItem = NULL;
	ImageCacheItem *pPrevOldestItem     = NULL;

	EnterCriticalSection(&criticalSection);

	if (pImageList && pImageList->pNext)
	{
		ImageCacheItem *pPrevItem = NULL;
		ImageCacheItem *pItem     = pImageList;

		// Traverse the list towards its tail, keeping a record of
		// the last item that is not locked.
		while (pItem)
		{
			if (!pItem->IsLocked())
			{
				pPrevOldestItem     = pPrevItem;
				pOldestUnlockedItem = pItem;
			}

			pPrevItem = pItem;
			pItem     = pItem->pNext;
		}

		if (pOldestUnlockedItem)
		{
			// Unlink oldest unlocked item from list.
			if (pPrevOldestItem)
				pPrevOldestItem->pNext = pOldestUnlockedItem->pNext;
			else
				pImageList = pOldestUnlockedItem->pNext;

			pOldestUnlockedItem->pNext = NULL;

			assert(count > 0);

			count--;

#ifdef USE_IMAGECACHE_MAP
			// Update the map to match the list
			map.RemoveKey(pOldestUnlockedItem->name);
#endif
		}
	}

	LeaveCriticalSection(&criticalSection);

	// The item is returned to the calling function, so that it can be deleted.
	// Cannot delete it here as it may be from a derived class.
	return pOldestUnlockedItem;
}


BOOL
ImageCache::RemoveItem(ImageCacheItem *pItem)
{
// Unlink the item from the list, but do not delete it
// as it may be from a derived class.
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	ImageCacheItem *pPrevItem = NULL;
	ImageCacheItem *pCurrItem = pImageList;

	while (pCurrItem)
	{
		if (pCurrItem == pItem)
		{
			// Found the item, but can only remove if not locked.
			if (!pCurrItem->IsLocked())
			{
				if (pPrevItem)
					pPrevItem->pNext = pCurrItem->pNext;
				else
					pImageList = pCurrItem->pNext;

				pCurrItem->pNext = NULL;

				assert(count > 0);

				count--;

#ifdef USE_IMAGECACHE_MAP
				// Update the map to match the list
				map.RemoveKey(pCurrItem->name);
#endif

				status = TRUE;
			}

			break;
		}

		pPrevItem = pCurrItem;
		pItem     = pCurrItem->pNext;
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}


ImageCacheItem*
ImageCache::InsertItem(ImageCacheItem *pNewItem)
{
	ImageCacheItem *pOldestItem = NULL;

	if (!pNewItem) return NULL;

	EnterCriticalSection(&criticalSection);

	if (count > maxCount)
	{
		pOldestItem = RemoveOldestItem();
	}

	// Insert new item at the head of the list
	pNewItem->pNext = pImageList;
	pImageList      = pNewItem;

	count++;

#ifdef USE_IMAGECACHE_MAP
	// Update the map to match the list
	map.SetAt(pNewItem->name, pNewItem);
#endif

	LeaveCriticalSection(&criticalSection);

	// If required, the tail item is returned to the calling function
	// (expected to be the corresponding function in the derived class), so
	// that it can be deleted. Cannot delete it here as don't know which 
	// derived class it is from.
	return pOldestItem;
}


ImageCacheItem*
ImageCache::Search(const char name[])
{
	ImageCacheItem *pItem = NULL;
	ImageCacheItem *pPrevItem = NULL;

	EnterCriticalSection(&criticalSection);

#ifdef USE_IMAGECACHE_MAP

	ImageCacheItem *pFoundItem = NULL;
	if (map.Lookup(name, pFoundItem) && pFoundItem)
	{
		// Item was found in map, but still need to search list in order to
		// get pPrevItem. However this is a lot faster than not using the map
		// at all, as pointer comparisons are being used rather than string
		// comparisons. This searching step could be removed altogether though
		// if the list was doubly-linked.
		pItem = pImageList;
		while (pItem)
		{
			if (pItem == pFoundItem) break;

			pPrevItem = pItem;

			pItem = pItem->pNext;
		}
	}

#else

	pItem = pImageList;
	while (pItem)
	{
		if (pItem->name.Compare(name) == 0) break;

		pPrevItem = pItem;

		pItem = pItem->pNext;
	}

#endif

	// Every time an item is searched for, and found, it is moved to the head
	// of the list. In this way, the least frequently requested items end up
	// at the tail of the list and are deleted first when the cache becomes full.
	if (pItem && pPrevItem)
	{
		pPrevItem->pNext = pItem->pNext;

		pItem->pNext = pImageList;

		pImageList = pItem;
	}

	LeaveCriticalSection(&criticalSection);

	return pItem;
}



//////////////////////////////////////////////////////////////////////////



void
AITiffImageCache::Initialise(const char imageDirectory[], int maxKbytes)
{
	Flush();

	maxMemoryUsage = maxKbytes;

	ImageCache::Initialise(imageDirectory, INT_MAX);
}


int
AITiffImageCache::GetMemoryUsageKBytes()
{
	int kilobytes = 0;

	EnterCriticalSection(&criticalSection);

	AITiffImageCacheItem *pItem = (AITiffImageCacheItem*)pImageList;
	while (pItem)
	{
		if (pItem->pTiff) kilobytes += pItem->pTiff->GetMemoryUsage();

		pItem = (AITiffImageCacheItem*)pItem->pNext;
	}

	LeaveCriticalSection(&criticalSection);

	return kilobytes;
}


void
AITiffImageCache::Flush()
{
// Delete all unlocked items from the cache
	EnterCriticalSection(&criticalSection);

	ImageCacheItem *pPrevItem = NULL;
	ImageCacheItem *pItem = pImageList;
	int numLocked = 0;

	while (pItem)
	{
		ImageCacheItem *pNextItem = pItem->pNext;

		if (!pItem->IsLocked())
		{	
			if (pPrevItem)
				pPrevItem->pNext = pNextItem;
			else
				pImageList = pNextItem;

#ifdef USE_IMAGECACHE_MAP
			// Update the map to match the list
			map.RemoveKey(pItem->name);
#endif

			delete (AITiffImageCacheItem*)pItem; // This deletes its tiff too

			tiffDeleteCount++; // A tiff has been deleted
		}
		else
		{
			numLocked++;
			pPrevItem = pItem;
		}

		pItem = pNextItem;
	}

	count = numLocked;

	LeaveCriticalSection(&criticalSection);
}


void
AITiffImageCache::EnsureMaxMemoryUsageNotExceeded()
{
// Remove old items from the cache if it is larger than the maximum size.
// Note that RemoveOldestItem() may return NULL when the memory usage is still
// too high and there are still items in the cache if those items are locked,
// as it does not remove locked items (that's what locking is for!).
	EnterCriticalSection(&criticalSection);

	while (GetMemoryUsageKBytes() > maxMemoryUsage)
	{
		ImageCacheItem *pRemovedItem = RemoveOldestItem();

		if (pRemovedItem)
		{
			delete (AITiffImageCacheItem*)pRemovedItem; // This deletes its tiff too

			tiffDeleteCount++; // A tiff has been deleted
		}
		else
			break;
	}

	LeaveCriticalSection(&criticalSection);
}


void
AITiffImageCache::InsertItem(AITiffImageCacheItem *pNewItem)
{
	EnterCriticalSection(&criticalSection);

	ImageCacheItem *pRemovedItem = ImageCache::InsertItem((ImageCacheItem*)pNewItem);

	if (pRemovedItem)
	{
		delete (AITiffImageCacheItem*)pRemovedItem; // This deletes its tiff too

		tiffDeleteCount++; // A tiff has been deleted
	}

	// Temporarily lock the new item while EnsureMaxMemoryUsageNotExceeded()
	// is called, so that it cannot be deleted, as that would make
	// the pNewItem pointer invalid.
	pNewItem->Lock();
	EnsureMaxMemoryUsageNotExceeded();
	pNewItem->Unlock();

	LeaveCriticalSection(&criticalSection);
}


AITiffImageCacheItem*
AITiffImageCache::Search(const char name[])
{
	return (AITiffImageCacheItem*)ImageCache::Search(name);
}

AITiffImageCacheItem*
AITiffImageCache::Search(AI_Tiff *pTiff)
{
// Returns a pointer to the cache item with the given AI_Tiff pointer.
	ImageCacheItem *pItem = NULL;

	if (pTiff != NULL)
	{
		EnterCriticalSection(&criticalSection);

		pItem = pImageList;

		while (pItem)
		{
			if (((AITiffImageCacheItem*)pItem)->pTiff == pTiff) break;

			pItem = pItem->pNext;
		}

		LeaveCriticalSection(&criticalSection);
	}

	return (AITiffImageCacheItem*)pItem;
}


AITiffImageCacheItem*
AITiffImageCache::GetItem(const char fileName[], BOOL noData/*=FALSE*/, BOOL loadIfMissing/*=TRUE*/, BOOL lock/*=FALSE*/,
                          unsigned int *pDeleteCount/*=NULL*/, AITiffImageCacheItem **ppOldItem/*=NULL*/)
{
// Returns a pointer to the cache item with the given name.
// If loadIfMissing is TRUE, if neccessary an item is created (including
// loading its tiff image) and inserted.
// If loadIfMissing is FALSE, NULL is returned if the item is not already in the cache.
// Note that the noData flag is ignored if loadIfMissing is FALSE.
	AITiffImageCacheItem *pItem = NULL;

	if (fileName)
	{
		EnterCriticalSection(&criticalSection);

		// If no images have been deleted from the cache since we last accessed
		// it, use the pointer from last time, as this is much faster than
		// searching the cache for it. If images have been deleted from the
		// cache, must search for this image again, in case it has been deleted
		// or the pointer has changed.
		// Note that eventually the delete count value will overflow and wrap
		// around back to zero and for this reason do not check if the value
		// has increased, just if it has changed.
		if (ppOldItem && *ppOldItem && pDeleteCount && *pDeleteCount == tiffDeleteCount)
			pItem = *ppOldItem;
		else
			pItem = Search(fileName);

		if (pItem)
		{
			if (lock)
				pItem->Lock();
		}
		else if (loadIfMissing)
		{
			pItem = new AITiffImageCacheItem;

			if (pItem)
			{
				pItem->name = fileName;

				CString filePath = sourceDirectory + "\\" + CString(pItem->name);

				pItem->pTiff = new AI_Tiff;

				if (pItem->pTiff && pItem->pTiff->LoadFile(filePath, noData))
				{
					// Ensure the item is locked before it is inserted.
					if (lock)
						pItem->Lock();

					InsertItem(pItem);
				}
				else
				{
					delete pItem;
					pItem = NULL;
				}
			}
		}

		// Update returned value for delete count last, as if a new item was
		// inserted, the delete count may have changed since the start of this
		// function.
		if (pDeleteCount)
			*pDeleteCount = tiffDeleteCount;

		// Must also modify old item pointer inside this function for thread
		// safety (hence ppOldItem is a pointer to the old pointer) - if the
		// old pointer was passed on the stack by value and this function was
		// called simultaneously by two threads, the copy of the value used by
		// the thread that entered the critical section second could have
		// become invalid but this would not be realised as the delete count
		// would have been updated by the first thread as it is passed by
		// reference (if both threads are referencing the same delete count).
		if (ppOldItem)
			*ppOldItem = pItem;

		LeaveCriticalSection(&criticalSection);
	}

	return pItem;
}


AI_Tiff*
AITiffImageCache::GetTiff(const char fileName[], BOOL noData/*=FALSE*/, BOOL loadIfMissing/*=TRUE*/,
                          BOOL lock/*=FALSE*/)
{
	AI_Tiff *pTiff = NULL;

	EnterCriticalSection(&criticalSection);

	AITiffImageCacheItem *pItem = GetItem(fileName, noData, loadIfMissing);

	if (pItem)
		pTiff = pItem->GetTiff();

	LeaveCriticalSection(&criticalSection);

	return pTiff;
}

BOOL
AITiffImageCache::Unlock(AI_Tiff *pTiff)
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AITiffImageCacheItem *pItem = Search(pTiff);

	if (pItem)
	{
		pItem->Unlock();
		status = TRUE;
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}

BOOL
AITiffImageCache::Unlock(const char fileName[])
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AITiffImageCacheItem *pItem = Search(fileName);

	if (pItem)
	{
		pItem->Unlock();
		status = TRUE;
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}


BOOL
AITiffImageCache::DeleteTiff(const char fileName[])
{
	BOOL status = FALSE;

	EnterCriticalSection(&criticalSection);

	AITiffImageCacheItem *pItem = Search(fileName);
	if (pItem && RemoveItem((ImageCacheItem*)pItem))
	{
		delete pItem; // Assuming this deletes its tiff too.

		tiffDeleteCount++; // A tiff has been deleted

		status = TRUE;
	}

	LeaveCriticalSection(&criticalSection);

	return status;
}


void AITiffImageCache::AddTiff(AI_Tiff *pTiff, const char fileName[], BOOL lock/*=FALSE*/)
{
// Add tiff image directly to the cache. If the image doesn't exist as a file
// the fileName parameter must still be unique within the cache and should not
// correspond with any existing file name or any file name that may be created
// while this cache item exists, otherwise if the cache has filled up and
// discarded this item then you request it by name, it will try to load the
// file with that name, which will not contain the image you added to the
// cache with that name, unless you subseqently wrote it to that file. For
// these reasons, if the image doesn't exist as a file, it is best to use
// something lie a GUID as the filename passed to this function, and to lock
// the image in the cache while it is being used, so it can't be discarded
// (though as always, locks shouldn't be used for long).
	// Create a new cache item to contain the tiff
	AITiffImageCacheItem *pItem = new AITiffImageCacheItem;

	// Fill in members of the item
	pItem->SetTiff(pTiff);
	pItem->name = fileName;

	if (lock)
		// Must ensure item is locked before as it is added,
		// otherwise another thread could remove it as soon as it
		// has been added, but before it has been locked.
		pItem->Lock();

	// Insert the item into the cache
	InsertItem(pItem);

	return;
}
