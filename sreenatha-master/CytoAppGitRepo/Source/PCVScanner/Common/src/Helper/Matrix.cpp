/*
 *		M A T R I X .C P P	 
 *
 *		Some stuff from Numerical Recipes for creating matrix arrays (double), starting with a subscript  of 1.
 *		And some stuff for multiply two matrices.
 *
 *		Author	 		:	A Rourke
 *		Date		    :	14 Frebruary 2001
 *		Copyright	:	Applied Imaging
 *
 *
 */

#include <math.h>
#include <malloc.h>
#include <memory.h>
#include <string.h>
#include <matrix.h>

#define NR_END 1
#define FREE_ARG char*
#define TINY 1.0e-20;


//Constructor
CMatrix::CMatrix()
{

}

//Destructor
CMatrix::~CMatrix()
{

}

/*
 *		A R E R R O R
 *			-- Numerical Recipes standard error handler 
 */
void CMatrix::arerror(char error_text[])
{
	void exit();

//	fprintf(stderr,"Run-time error...\n");
//	fprintf(stderr,"%s\n",error_text);

	/*exit(1);*/
}

/* 
 *	D M A T R I X
 *		-- allocate a double matrix with subscript
 *		-- range m[nrl..nrh][ncl..nch] 
 *
 */
double **CMatrix::admatrix(long nrl, long nrh, long ncl, long nch)
{
	long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
	double **m;

		/* allocate pointers to rows */
	m=(double **) malloc((unsigned int)((nrow+NR_END)*sizeof(double*)));

	if (!m) {
		arerror("allocation failure 1 in matrix()");
		return NULL;
		}

	m += NR_END;
	m -= nrl;

		/* allocate rows and set pointers to them */
	m[nrl]=(double *) malloc((unsigned int)((nrow*ncol+NR_END)*sizeof(double)));

	if (!m[nrl]) {
		arerror("allocation failure 2 in matrix()");
		return (NULL);
		}

	m[nrl] += NR_END;
	m[nrl] -= ncl;

	for(i= nrl+1; i<= nrh; i++)
		m[i]=m[i-1]+ncol;

		/* return pointer to array of pointers to rows */
	return m;
}

/*
 *	F R E E _ D M A T R I X
 *		-- free a matrix allocated by admatrix 
 *
 */
void CMatrix::free_admatrix(double **m, long nrl, long nrh, long ncl, long nch)
{
	free((FREE_ARG) (m[nrl]+ncl-NR_END));
	free((FREE_ARG) (m+nrl-NR_END));
}


/*
 *	M A T R I X _ M U L T I P L Y
 *		-- multiply mat1 x mat2, putting result in mat3
 *		-- mat3 may be the same as mat1 or mat2
 *
 */
int CMatrix::matrix_multiply (Matrix *mat1, Matrix *mat2, Matrix *mat3)
{
	int i,k,j;
	double **xx;

	if (mat1->cols != mat2->rows) {
//		printf ("Incompatible matrix order\n");
		return -1;
		}

	xx = admatrix ((long)1,(long)mat1->rows,(long)1,(long)mat2->cols);	/*create intermediate matrix for results*/

	for (k = 1; k <= mat2->cols; k++) {
		for (i = 1; i <= mat1->rows; i++) {
			xx[i][k] = 0.0;
			for (j = 1; j <= mat1->cols; j++) {
				xx[i][k] += (mat1->Mat[i][j] * mat2->Mat[j][k]);
				}
			}
		}

		/*copy results into mat3 for return to calling routine*/
	for (i = 1; i <= mat1->rows; i++)
		for (j = 1; j <= mat2->cols; j++)
			mat3->Mat[i][j] = xx[i][j];

	free_admatrix (xx,(long)1,(long)mat1->rows,(long)1,(long)mat2->cols);	

	return (0);
}

/*
 *	I D E N T I T Y _ M A T R I X
 *		-- create the identity matrix
 *
 */

void CMatrix::identity_matrix (Matrix *mat)
{
	int i,j;

	for (i = 1; i <= mat->rows; i++) {
		for (j = 1; j <= mat->cols; j++) {
			if (i == j)
				mat->Mat[i][j] = 1.0;
			else
				mat->Mat[i][j] = 0.0;
			}
		}
}


int CMatrix::ludcmp( double ** a, int n, int * indx, double * d)
{
	int i,imax,j,k;
	double big,dum,sum,temp;
	double *vv;


	if(!dvector(1,n,&vv))
		return(0);
	
	*d=1.0;
	for (i=1;i<=n;i++) {
		big=0.0;
		for (j=1;j<=n;j++)
			if ((temp=fabs(a[i][j])) > big) big=temp;
		if (big == 0.0) {
			return(0);
		}
		vv[i]=1.0/big;
	}
	for (j=1;j<=n;j++) {
		for (i=1;i<j;i++) {
			sum=a[i][j];
			for (k=1;k<i;k++) sum -= a[i][k]*a[k][j];
			a[i][j]=sum;
		}

		big=0.0;
		for (i=j;i<=n;i++) {
			sum=a[i][j];
			for (k=1;k<j;k++)
				sum -= a[i][k]*a[k][j];
			a[i][j]=sum;
			if ( (dum=vv[i]*fabs(sum)) >= big) {
				big=dum;
				imax=i;
			}
		}

		if (j != imax) {
			for (k=1;k<=n;k++) {
				dum=a[imax][k];
				a[imax][k]=a[j][k];
				a[j][k]=dum;
			}
			*d = -(*d);
			vv[imax]=vv[j];
		}
		indx[j]=imax;
		if (a[j][j] == 0.0) a[j][j]=TINY;
		if (j != n) {
			dum=1.0/(a[j][j]);
			for (i=j+1;i<=n;i++) a[i][j] *= dum;
		}
	}
	free_dvector(vv,1,n);

	return(1);
}

#undef TINY


void CMatrix::lubksb( double **a, int n, int * indx, double b[])
{
	int i,ii=0,ip,j;
	double sum;

	for (i=1;i<=n;i++) {
		ip=indx[i];
		sum=b[ip];
		b[ip]=b[i];
		if (ii)
			for (j=ii;j<=i-1;j++) sum -= a[i][j]*b[j];
		else if (sum != 0.0) ii=i;
		b[i]=sum;
	}
	for (i=n;i>=1;i--) {
		sum=b[i];
		for (j=i+1;j<=n;j++) sum -= a[i][j]*b[j];
		b[i]=sum/a[i][i];
	}
}


void CMatrix::svbksb(double **u,double w[],double **v,int m,int n,double b[],double x[])
{
	int jj,j,i;
	double s,*tmp,*vector();
	void free_vector();

	if(!dvector(1,n,&tmp))
		return;
	for (j=1;j<=n;j++) {
		s=0.0;
		if (w[j]) {
			for (i=1;i<=m;i++) s += u[i][j]*b[i];
			s /= w[j];
		}
		tmp[j]=s;
	}
	for (j=1;j<=n;j++) {
		s=0.0;
		for (jj=1;jj<=n;jj++) s += v[j][jj]*tmp[jj];
		x[j]=s;
	}
	free_dvector(tmp,1,n);
}



void CMatrix::svdcmp(double **a,int m,int n,double w[],double **v)
{
	int flag,i,its,j,jj,k,l,nm;
	double c,f,h,s,x,y,z;
	double anorm=0.0,g=0.0,scale=0.0;
	double *rv1,*vector();

	if (m < n)
		return;
	if(!dvector(1,n,&rv1))
		return;
	for (i=1;i<=n;i++) {
		l=i+1;
		rv1[i]=scale*g;
		g=s=scale=0.0;
		if (i <= m) {
			for (k=i;k<=m;k++) scale += fabs(a[k][i]);
			if (scale) {
				for (k=i;k<=m;k++) {
					a[k][i] /= scale;
					s += a[k][i]*a[k][i];
				}
				f=a[i][i];
				g = -SIGN(sqrt(s),f);
				h=f*g-s;
				a[i][i]=f-g;
				if (i != n) {
					for (j=l;j<=n;j++) {
						for (s=0.0,k=i;k<=m;k++) s += a[k][i]*a[k][j];
						f=s/h;
						for (k=i;k<=m;k++) a[k][j] += f*a[k][i];
					}
				}
				for (k=i;k<=m;k++) a[k][i] *= scale;
			}
		}
		w[i]=scale*g;
		g=s=scale=0.0;
		if (i <= m && i != n) {
			for (k=l;k<=n;k++) scale += fabs(a[i][k]);
			if (scale) {
				for (k=l;k<=n;k++) {
					a[i][k] /= scale;
					s += a[i][k]*a[i][k];
				}
				f=a[i][l];
				g = -SIGN(sqrt(s),f);
				h=f*g-s;
				a[i][l]=f-g;
				for (k=l;k<=n;k++) rv1[k]=a[i][k]/h;
				if (i != m) {
					for (j=l;j<=m;j++) {
						for (s=0.0,k=l;k<=n;k++) s += a[j][k]*a[i][k];
						for (k=l;k<=n;k++) a[j][k] += s*rv1[k];
					}
				}
				for (k=l;k<=n;k++) a[i][k] *= scale;
			}
		}
		anorm=MAT_MAX(anorm,(fabs(w[i])+fabs(rv1[i])));
	}
	for (i=n;i>=1;i--) {
		if (i < n) {
			if (g) {
				for (j=l;j<=n;j++)
					v[j][i]=(a[i][j]/a[i][l])/g;
				for (j=l;j<=n;j++) {
					for (s=0.0,k=l;k<=n;k++) s += a[i][k]*v[k][j];
					for (k=l;k<=n;k++) v[k][j] += s*v[k][i];
				}
			}
			for (j=l;j<=n;j++) v[i][j]=v[j][i]=0.0;
		}
		v[i][i]=1.0;
		g=rv1[i];
		l=i;
	}
	for (i=n;i>=1;i--) {
		l=i+1;
		g=w[i];
		if (i < n)
			for (j=l;j<=n;j++) a[i][j]=0.0;
		if (g) {
			g=1.0/g;
			if (i != n) {
				for (j=l;j<=n;j++) {
					for (s=0.0,k=l;k<=m;k++) s += a[k][i]*a[k][j];
					f=(s/a[i][i])*g;
					for (k=i;k<=m;k++) a[k][j] += f*a[k][i];
				}
			}
			for (j=i;j<=m;j++) a[j][i] *= g;
		} else {
			for (j=i;j<=m;j++) a[j][i]=0.0;
		}
		++a[i][i];
	}
	for (k=n;k>=1;k--) {
		for (its=1;its<=30;its++) {
			flag=1;
			for (l=k;l>=1;l--) {
				nm=l-1;
				if (fabs(rv1[l])+anorm == anorm) {
					flag=0;
					break;
				}
				if (fabs(w[nm])+anorm == anorm) break;
			}
			if (flag) {
				c=0.0;
				s=1.0;
				for (i=l;i<=k;i++) {
					f=s*rv1[i];
					if (fabs(f)+anorm != anorm) {
						g=w[i];
						h=PYTHAG(f,g);
						w[i]=h;
						h=1.0/h;
						c=g*h;
						s=(-f*h);
						for (j=1;j<=m;j++) {
							y=a[j][nm];
							z=a[j][i];
							a[j][nm]=y*c+z*s;
							a[j][i]=z*c-y*s;
						}
					}
				}
			}
			z=w[k];
			if (l == k) {
				if (z < 0.0) {
					w[k] = -z;
					for (j=1;j<=n;j++) v[j][k]=(-v[j][k]);
				}
				break;
			}
			if (its == 30) return;
			x=w[l];
			nm=k-1;
			y=w[nm];
			g=rv1[nm];
			h=rv1[k];
			f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y);
			g=PYTHAG(f,1.0);
			f=((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x;
			c=s=1.0;
			for (j=l;j<=nm;j++) {
				i=j+1;
				g=rv1[i];
				y=w[i];
				h=s*g;
				g=c*g;
				z=PYTHAG(f,h);
				rv1[j]=z;
				c=f/z;
				s=h/z;
				f=x*c+g*s;
				g=g*c-x*s;
				h=y*s;
				y=y*c;
				for (jj=1;jj<=n;jj++) {
					x=v[jj][j];
					z=v[jj][i];
					v[jj][j]=x*c+z*s;
					v[jj][i]=z*c-x*s;
				}
				z=PYTHAG(f,h);
				w[j]=z;
				if (z) {
					z=1.0/z;
					c=f*z;
					s=h*z;
				}
				f=(c*g)+(s*y);
				x=(c*y)-(s*g);
				for (jj=1;jj<=m;jj++) {
					y=a[jj][j];
					z=a[jj][i];
					a[jj][j]=y*c+z*s;
					a[jj][i]=z*c-y*s;

				}
			}
			rv1[l]=0.0;
			rv1[k]=f;
			w[k]=x;
		}
	}
	free_dvector(rv1,1,n);
}





int CMatrix::matinv( double **a, double **y, int n)
{
	int i, j, indx[1000];
	double d, ycol[1000];

	for (i=1; i<=n; i++) {
		for (j=1; j<=n; j++)
			y[i][j] = 0.0;
		y[i][i] = 1.0;
	}
	if (!ludcmp(a,n,indx,&d))
		return(-1);

	for (j=1; j<=n; j++) {
		for (i=1; i<=n; i++)
			ycol[i] = y[i][j];
		lubksb(a,n,indx,ycol);
		for (i=1; i<=n; i++)
			y[i][j] = ycol[i];
	}
	return(0);
}


double CMatrix::matdet( double **a, int n, int *powten)
{
	int j, indx[1000];
	double d;


	if (!ludcmp(a,n,indx,&d))
		return(-1.0);

	*powten = 0;

	for (j=1; j<=n; j++) {
		d *= a[j][j];

		*powten += 2;
		d /= 100;
	}
	return(d);
}

// A is a M by N matrix (for fitting a plane  z = c1.x + c2.y + c3 to 4 points, this is a 4x3 matrix)
// | x1 y1 1 |
// | x2 y2 1 |
// | x3 y3 1 |
// | x4 y4 1 |

// x is a N vector		(3 vector)
// c1
// c2
// c3
// b is a M vector		(4 vector)
// z1
// z2
// z3
// z4

int CMatrix::least_squares(double** A, double* x, double* b, int M, int N)
{
	int i,j,k,err;
	double** AtransA;
	double*  Atransb;

	AtransA = dmatrix(1,N,1,N,&err);
	if (err)
		return(0);

	if(!dvector(1,N,&Atransb))
		return(0);

	for (i=1; i<=N; i++)
	{
		Atransb[i]=0.0;
		for (j=1; j<=N; j++)
		{
			AtransA[i][j]=0.0;
		}
	}

	for (i=1; i<=N; i++)
	{
		for (j=1; j<=N; j++)
		{
			for (k=1; k<=M; k++)
			{
				AtransA[i][j] = AtransA[i][j] + (A[k][i]*A[k][j]);
			}
		}
	}

	for (i=1; i<=N; i++)
	{
		for (k=1; k<=M; k++)
		{
			Atransb[i] = Atransb[i] + (A[k][i]*b[k]);
		}
	}

	for (i=1; i<=N; i++)
		x[i] = Atransb[i];

	int* indx = new int[N+1];
	double d;
	ludcmp(AtransA, N, indx, &d);
	lubksb(AtransA, N, indx, x);

	free_dvector(Atransb,1,N);
	free_dmatrix(AtransA,1,N,1,N);

	return (1);

}

void CMatrix::nrerror( char *error_text)
{
	//TRACE(error_text);
	//fprintf( stderr, "ERROR: vcvtrain: %s\n", error_text);
	//fflush( stderr);											//SN22Oct99
}



int CMatrix::dvector( int nl, int nh, double **dvect)
{
	double *v;


	//v=(double *)malloc((unsigned) (nh-nl+1)*sizeof(double));
	v=(double *)malloc( (unsigned) (nh+1)*sizeof(double) );
	if (!v){
		nrerror("allocation failure in dvector()");
		return(0);
	}
	//*dvect = v-nl;			//SN12Apr99 This moves pointer to invalid memory!
	*dvect = v;
	return(1);
}


double **CMatrix::dmatrix( int nrl, int nrh, int ncl, int nch, int * err)
{
	int i;
	double **m;

	*err = 0;
	m=(double **) malloc((unsigned) (nrh-nrl+1)*sizeof(double*));
	if (!m){
		nrerror("allocation failure 1 in dmatrix()");
		*err = 1;
	}

	if (!*err) {
		m -= nrl;

		for(i=nrl;i<=nrh;i++) {
			m[i]=(double *) malloc((unsigned) (nch-ncl+1)*sizeof(double));
			if (!m[i]){
				nrerror("allocation failure 2 in dmatrix()");
				*err = 1;
			}
			if (!*err)
				m[i] -= ncl;
		}
	}
	return(m);
}



void CMatrix::free_dvector( double *v, int nl, int nh)
{
	free( v);
}


void CMatrix::free_dmatrix( double **m, int nrl, int nrh, int ncl, int nch)
{
	int i;

	for(i=nrh;i>=nrl;i--) free((char*) (m[i]+ncl));
	free((char*) (m+nrl));
}

