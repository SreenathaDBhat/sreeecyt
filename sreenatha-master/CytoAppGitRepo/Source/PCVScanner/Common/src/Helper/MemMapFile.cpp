//=================================================================================
// MemMapFile.cpp
// Small class to deal with the creation and control of a memory mapped file
//
// History:
//
// 07Mar00	WH:	original
//=================================================================================
#include "StdAfx.h"

#include "MemMapFile.h"

//=================================================================================
//=================================================================================
CMemMapFile::CMemMapFile()
{
	m_hMemPublic = NULL;
	m_hMem = NULL;	
}
//=================================================================================
//=================================================================================
CMemMapFile::~CMemMapFile()
{

}
//=================================================================================
//=================================================================================
BYTE *CMemMapFile::CreatePublicSegment(DWORD key, UINT size)
{
	char map_name[20];

	m_key = key;

	// If already opened, just return memory address
	if (m_hMem != NULL)
		return(m_hMem);

	// Generate mapping object name using key number in hex
	sprintf(map_name, "M%x", key);

	// Use file backed by system paging file
	m_hMemPublic = CreateFileMapping(INVALID_HANDLE_VALUE,
					NULL, //&sec_attr,				// Security attributes
					PAGE_READWRITE,		// Data is read/write
					0,					// File size (High)
					size,				// File size (Low)
					__TEXT(map_name));	// Mapping name used subsequently


	if (m_hMemPublic == NULL)
	{
		return(NULL);
	}

	// OK, now set view into the file
	m_hMem = (BYTE *)MapViewOfFile(m_hMemPublic,
					FILE_MAP_READ | FILE_MAP_WRITE, // Read/Write access
					0, 0,							// No offset
					0);								// Map entire file

	return(m_hMem);
}
//=================================================================================
//=================================================================================
BYTE *CMemMapFile::MapPublicSegment(DWORD key)
{
	char map_name[20];

	// If already opened, just return memory address
	if (m_hMem != NULL)
	{
		if (m_key == key)	// okay same one
			return(m_hMem);
		else
			return NULL;
	}
	m_key = key;

	// Generate mapping object name using key number in hex
	sprintf(map_name, "M%x", m_key);

	// Open existing file mapping
	m_hMemPublic = OpenFileMapping(
					FILE_MAP_READ | FILE_MAP_WRITE,	// Data is read/write
					FALSE,
					__TEXT(map_name));	// Existing name of file mapping

	if (m_hMemPublic == NULL)
		return(NULL);

	// OK, now set view into the file
	m_hMem = (BYTE*)MapViewOfFile(m_hMemPublic,
					FILE_MAP_READ | FILE_MAP_WRITE, // Read/Write access
					0, 0,							// No offset
					0);								// Map entire file

	if (m_hMem == NULL)
		return(NULL);

	return(m_hMem);
}
//=================================================================================
//=================================================================================
BOOL CMemMapFile::DestroyPublicSegment(void)
{
	if (m_hMem == NULL)
		return(TRUE);

	if (UnmapViewOfFile(m_hMem) == FALSE) 
	{
		return(FALSE);
	}

	CloseHandle(m_hMemPublic);
	m_hMem = NULL;

	return(TRUE);
}
//=================================================================================
//=================================================================================