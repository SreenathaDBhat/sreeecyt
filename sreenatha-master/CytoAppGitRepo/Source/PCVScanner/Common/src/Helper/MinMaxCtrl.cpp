
/*
 * MinMaxCtrl.cpp
 * JMB March 2002
 */


#include "stdafx.h"

#include "MinMaxCtrl.h"

 
// According to Microsoft's Technical Note 20 (TN020: ID Naming and Numbering
// Conventions), MFC reserves particular ranges for predefined ID's. To avoid
// conflict with these, control ID's should be in the range 8->0xDFFF.
#define BASECTRL_ID   8



#define SLIDER_HEIGHT  20
#define TEXT_WIDTH     30



CMinMaxSlider::CMinMaxSlider()
{
	

}


BOOL
CMinMaxSlider::Create(const RECT& rect, CWnd* pParentWnd, UINT nID)
{
	return CWnd::Create(NULL, NULL, WS_CHILD | WS_VISIBLE,//| WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
	                    rect, pParentWnd, nID);
}



void CMinMaxSlider::SetRange(int min, int max, BOOL update/*=TRUE*/)
{
	// Don't let max be more than 9999 as numbers bigger than that won't fit in the text control
	//if (max > 9999) max = 9999;

	minSlider.SetRange(min, max, update);
	maxSlider.SetRange(min, max, update);
	Update();
}


void CMinMaxSlider::SetAppropriateRange(int min, int max, int limitMin, int limitMax, BOOL update/*=TRUE*/)
{
// Set range based on the current slider positions plus 50% either side, subject to limit values.
	int temp;
	if (max < min)
	{
		temp = min;
		min  = max;
		max  = temp;
	}

	if (limitMax < limitMin)
	{
		temp     = limitMin;
		limitMin = limitMax;
		limitMax = temp;
	}


	int overhead = abs(max - min) / 2;
	if (overhead < 1) overhead = 1;
	max = max + overhead;
	min = min - overhead;


	if (min < limitMin) min = limitMin;
	if (max > limitMax) max = limitMax;

	if (max <= min) max = min + 1;


	SetRange(min, max, update);
}


void CMinMaxSlider::GetRange(int *pMin, int *pMax)
{
	// Both min and max slider should have same range.
	*pMin = minSlider.GetRangeMin();
	*pMax = maxSlider.GetRangeMax();
}


void CMinMaxSlider::SetMinMax(int min, int max)
{
	minSlider.SetPos(min);
	maxSlider.SetPos(max);
	Update();
}

void CMinMaxSlider::GetMinMax(int *pMin, int *pMax)
{
	*pMin = minSlider.GetPos();
	*pMax = maxSlider.GetPos();
}


void CMinMaxSlider::ShowSliders(BOOL showMin/*=TRUE*/, BOOL showMax/*=TRUE*/)
{
	minSlider.ShowWindow(showMin ? SW_SHOWNA : SW_HIDE);
	minStatic.ShowWindow(showMin ? SW_SHOWNA : SW_HIDE);

	maxSlider.ShowWindow(showMax ? SW_SHOWNA : SW_HIDE);
	maxStatic.ShowWindow(showMax ? SW_SHOWNA : SW_HIDE);

	// For some reason I don't understand, calling ShowWindow(SW_HIDE) for one
	// of the above controls doesn't immediately remove it from the screen, and
	// calling Invalidate() or UpdateWindow() makes no difference, even though
	// manual intervention to make the window redraw (temporarily moving another
	// app on top of it) does work. The only way I have found to remove it from
	// view is to hide then show the whole CMinMaxSlider window, as follows!
	ShowWindow(SW_HIDE);
	ShowWindow(SW_SHOW);
}


void CMinMaxSlider::EnableSliders(BOOL showMin/*=TRUE*/, BOOL showMax/*=TRUE*/)
{
	minSlider.EnableWindow(showMin ? SW_SHOWNA : SW_HIDE);
	minStatic.EnableWindow(showMin ? SW_SHOWNA : SW_HIDE);

	maxSlider.EnableWindow(showMax ? SW_SHOWNA : SW_HIDE);
	maxStatic.EnableWindow(showMax ? SW_SHOWNA : SW_HIDE);

	// For some reason I don't understand, calling ShowWindow(SW_HIDE) for one
	// of the above controls doesn't immediately remove it from the screen, and
	// calling Invalidate() or UpdateWindow() makes no difference, even though
	// manual intervention to make the window redraw (temporarily moving another
	// app on top of it) does work. The only way I have found to remove it from
	// view is to hide then show the whole CMinMaxSlider window, as follows!
	ShowWindow(SW_HIDE);
	ShowWindow(SW_SHOW);
}



BEGIN_MESSAGE_MAP(CMinMaxSlider, CWnd)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_HSCROLL()
END_MESSAGE_MAP()


int
CMinMaxSlider::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;


	// Get font from parent dialog.
	CFont *pFont = CWnd::FromHandle(lpCreateStruct->hwndParent)->GetFont();



	RECT rect;
	GetClientRect(&rect);
	rect.left += TEXT_WIDTH; rect.right -= TEXT_WIDTH;
	/*rect.top  += 10;*/ rect.bottom = rect.top + SLIDER_HEIGHT;

	minSlider.Create(WS_CHILD | WS_VISIBLE | TBS_HORZ | TBS_BOTTOM | TBS_NOTICKS,
	                 rect, this, BASECTRL_ID);


	GetClientRect(&rect);
	rect.right = rect.left + TEXT_WIDTH;
	/*rect.top +=10; rect.bottom -=10;*/ rect.bottom = rect.top + SLIDER_HEIGHT;
	minStatic.Create("", WS_CHILD | WS_VISIBLE, rect, this);
	minStatic.SetFont(pFont); // Use the font that was got from the dialog


	GetClientRect(&rect);
	rect.left += TEXT_WIDTH;   rect.right -= TEXT_WIDTH;
	/*rect.bottom -= 10;*/ rect.top = rect.bottom - SLIDER_HEIGHT; 

	maxSlider.Create(WS_CHILD | WS_VISIBLE | TBS_HORZ | TBS_TOP | TBS_NOTICKS,
	                 rect, this, BASECTRL_ID + 1);


	GetClientRect(&rect);
	rect.left = rect.right - TEXT_WIDTH;
	/*rect.top +=10; rect.bottom -=10;*/ rect.top = rect.bottom - SLIDER_HEIGHT; 
	maxStatic.Create("", WS_CHILD | WS_VISIBLE, rect, this);
	maxStatic.SetFont(pFont); // Use the font that was got from the dialog


	return 0;
}



void CMinMaxSlider::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	// Create brushes and pens
	CBrush blackBrush(RGB(0, 0, 0));
	CBrush blueBrush(RGB(0, 0, 255));
	CPen blackPen;
	blackPen.CreatePen(PS_SOLID, 0, RGB(0, 0, 0));
	CPen bluePen;
	bluePen.CreatePen(PS_SOLID, 0, RGB(0, 0, 255));

	CBrush *pBarBrush, *pBackgroundBrush;
	CPen *pBarPen, *pBackgroundPen;

	// Normally, we have a blue bar on a black background. But if min is
	// greater than max, give the impression of the blue bar having
	// wrapped around, by inverting the colours.
	if (minSlider.GetPos() > maxSlider.GetPos())
	{
		pBarBrush = &blackBrush;
		pBarPen   = &blackPen;
		pBackgroundBrush = &blueBrush;
		pBackgroundPen   = &bluePen;
	}
	else
	{
		pBarBrush = &blueBrush;
		pBarPen   = &bluePen;
		pBackgroundBrush = &blackBrush;
		pBackgroundPen   = &blackPen;
	}


	// Draw the background for the range bar.
	RECT sliderRect, channelRect, thumbRect;
	minSlider.GetWindowRect(&sliderRect);
	ScreenToClient(&sliderRect);
	minSlider.GetChannelRect(&channelRect);
	minSlider.GetThumbRect(&thumbRect);


	RECT rect;

	GetClientRect(&rect);
	rect.top += SLIDER_HEIGHT;
	rect.bottom -= SLIDER_HEIGHT;
	int halfThumb = (thumbRect.right - thumbRect.left) / 2;
	rect.left  = sliderRect.left + channelRect.left  + halfThumb;
	rect.right = sliderRect.left + channelRect.right - halfThumb;

	CBrush *pOldBrush = dc.SelectObject(pBackgroundBrush);
	CPen   *pOldPen   = dc.SelectObject(pBackgroundPen);

	// Draw the background rectangle
	dc.Rectangle(&rect);

	dc.SelectObject(pOldBrush);
	dc.SelectObject(pOldPen);



	// Draw the range bar	
	GetWindowRect(&rect);
	rect.top    += SLIDER_HEIGHT;
	rect.bottom -= SLIDER_HEIGHT;

	// Find the screen position of the min slider.
	minSlider.GetWindowRect(&sliderRect);
	minSlider.GetThumbRect(&thumbRect);

	rect.left = sliderRect.left + (thumbRect.left + thumbRect.right) / 2;

	// Find the screen position of the max slider.
	minSlider.GetWindowRect(&sliderRect);
	maxSlider.GetThumbRect(&thumbRect);

	rect.right = sliderRect.left + (thumbRect.left + thumbRect.right) / 2;


	ScreenToClient(&rect);


	pOldBrush = dc.SelectObject(pBarBrush);
	pOldPen   = dc.SelectObject(pBarPen);

	// Draw the bar rectangle
	dc.Rectangle(&rect);


	// Put back the old objects
	dc.SelectObject(pOldBrush);
	dc.SelectObject(pOldPen);
}


void CMinMaxSlider::Update()
{
	CString text;
	text.Format("%d", minSlider.GetPos());
	minStatic.SetWindowText((PCSTR)text);

	text.Format("%d", maxSlider.GetPos());
	maxStatic.SetWindowText((PCSTR)text);


	// Invalidate the area between the sliders only, so the sliders don't flicker.
	RECT rect;
	GetClientRect(&rect);
	rect.top += SLIDER_HEIGHT; rect.bottom -= SLIDER_HEIGHT;
	InvalidateRect(&rect);
	UpdateWindow();
}


void CMinMaxSlider::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	Update();


	CWnd *pParent = GetParent();

	if (pParent)
		//// Tell the parent something has changed. Because this is a control,
		//// it sends its control ID and window handle.
		//pParent->PostMessage(WM_COMMAND, MAKEWPARAM(GetDlgCtrlID(), 0), (LPARAM)m_hWnd);
		// Pass the scroll message on to the parent.
		pParent->PostMessage(WM_HSCROLL, MAKEWPARAM(nSBCode, nPos), (LPARAM)m_hWnd);

	
	CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
}

