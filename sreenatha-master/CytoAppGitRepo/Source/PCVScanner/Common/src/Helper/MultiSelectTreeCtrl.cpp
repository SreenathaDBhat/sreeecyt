/*
 * MultiSelectTreeCtrl.cpp
 * Re-usable tree control that enables multiple selections
 */


#include "stdafx.h"

#include "MultiSelectTreeCtrl.h"


void CMultiSelectTreeCtrl::SetIcons(HICON hIconNone, HICON hIconSome, HICON hIconAll)
{
// Note that the indices of the icons in the list must match the corresponding
// values from the SelectionState enumeration (hence the VERIFYS).
// Note that VERIFY is used, not ASSERT, as the expressions must be evaluated
// even in Release builds.
	imlist.DeleteImageList();
	imlist.Create(16, 16, TRUE, 3, 3);
	VERIFY(imlist.Add(hIconNone) == selectionNone);
	VERIFY(imlist.Add(hIconSome) == selectionSome);
	VERIFY(imlist.Add(hIconAll)  == selectionAll);

	// Apply image list to ctrl
	SetImageList(&imlist, TVSIL_NORMAL);
}

CMultiSelectTreeCtrl::SelectionState CMultiSelectTreeCtrl::GetSelectionState(HTREEITEM hItem)
{
	SelectionState state = selectionNone;
	int image, unused;
	if (GetItemImage(hItem, image, unused))
		state = (SelectionState)image;
	return state;
}

void CMultiSelectTreeCtrl::SelectDescendants(HTREEITEM hItem, SelectionState state) 
{
	if (ItemHasChildren(hItem))
	{
		HTREEITEM hChild = GetChildItem(hItem);
		while (hChild)
		{
			SetItemImage(hChild, state, state);

			SelectDescendants(hChild, state);

			hChild = GetNextSiblingItem(hChild);
		}
	}
}

void CMultiSelectTreeCtrl::SelectAncestors(HTREEITEM hItem, SelectionState state)
{
	while ((hItem = GetParentItem(hItem)) != NULL)
	{
		SelectionState siblingsState = selectionNone;
		int nCount    = 0;
		int nSelected = 0;

		HTREEITEM hChild = GetChildItem(hItem);
		while (hChild)
		{
			SelectionState childState = GetSelectionState(hChild);
			if (childState != selectionNone)
			{
				siblingsState = selectionSome;
				if (childState == selectionAll)
					nSelected++;
			}

			nCount++;

			hChild = GetNextSiblingItem(hChild);
		}

		if (nSelected == nCount)
			siblingsState = selectionAll;


		SetItemImage(hItem, siblingsState, siblingsState);
	}
}

BOOL CMultiSelectTreeCtrl::SetSelectionState(HTREEITEM hItem, SelectionState state)
{
	BOOL status = FALSE;

	if (SetItemImage(hItem, state, state))
	{
		// Copy new selection state to all children if any
		SelectDescendants(hItem, state);

		// Set state of all ancestors based on state of siblings
		// - if sibling states are not all the same, set ancestors
		// to partial selection state.
		SelectAncestors(hItem, state);

		status = TRUE;
	}

	return status;
}


BEGIN_MESSAGE_MAP(CMultiSelectTreeCtrl, CTreeCtrl)
	ON_NOTIFY_REFLECT_EX(NM_CLICK, OnClick)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CMultiSelectTreeCtrl message handlers

BOOL CMultiSelectTreeCtrl::OnClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	BOOL doNotNotifyParent = FALSE;

	CPoint pt;
	GetCursorPos(&pt);
	ScreenToClient(&pt);

	UINT uFlags;
	HTREEITEM hItem = HitTest(pt, &uFlags);

	// Get selected item
	if (hItem && (TVHT_ONITEM & uFlags))
	{
		// Get existing selection state
		SelectionState state = GetSelectionState(hItem);

		// Invert the selection (partial selection becomes no selection)
		state = (state == selectionNone) ? selectionAll : selectionNone;

		// Set the new state
		SetSelectionState(hItem, state);
	}

	*pResult = 0;

	return doNotNotifyParent;
}
