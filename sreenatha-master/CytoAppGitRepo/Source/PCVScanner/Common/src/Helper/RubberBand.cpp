//=================================================================================
// RubberNand.cpp
// Provide implementation of a Rubber Band rectangle class. Could use CTrackRect
// but don't particulary want to link to MFC. May be a problem with GetMessage()
// and having it in a COM object (Different thread)
//
// History:
//
// 29Feb00	WH:	original
//=================================================================================
#include "StdAfx.h"
#include "RubberBand.h"

//=================================================================================
//=================================================================================
BOOL CRubberBand::DoDraw(HWND hWnd)
{
	BOOL done = FALSE;
	MSG msg;
	BOOL start = FALSE;
	HCURSOR oldcur;

	m_hWnd = hWnd;

	SetCapture(hWnd);
	oldcur = SetCursor(LoadCursor(NULL, IDC_CROSS));
	while (!done &&GetMessage(&msg, hWnd, 0, 0) == TRUE)
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);

		switch(msg.message)
		{
		case WM_LBUTTONDOWN:
			if (!start)
				StartDraw(LOWORD(msg.lParam), HIWORD(msg.lParam));
			start = TRUE;
			break;
		case WM_LBUTTONUP:
			if (start)
			{
				StopDraw();
				done = TRUE;
			}
			break;
		case WM_MOUSEMOVE:
			if (start)
				Draw(LOWORD(msg.lParam), HIWORD(msg.lParam));
			break;
		default: break;
		}
		
	}
	SetCursor(oldcur);
	ReleaseCapture();

	return TRUE;
}
//=================================================================================
//=================================================================================
void CRubberBand::StartDraw(INT x, INT y)
{
	m_x = m_roi.left = m_roi.right = x;
	m_y = m_roi.top = m_roi.bottom = y;
}
//=================================================================================
//=================================================================================
void CRubberBand::StopDraw()
{

}
//=================================================================================
//=================================================================================
void CRubberBand::Draw(INT x, INT y)
{
	HDC dc = GetDC(m_hWnd);

	HPEN pen = CreatePen(PS_DOT, 1, RGB(0, 0, 0));
	HPEN oldpen = (HPEN)SelectObject(dc, pen);

	SetROP2(dc, R2_XORPEN);

	// XOR off
	_Rectangle(dc, &m_roi);

	if (x < m_x)
	{
		m_roi.left = m_x - (m_x - x);
		m_roi.right = m_x;
	}
	else
	{
		m_roi.right = m_x + (x - m_x);
		m_roi.left = m_x;
	}

	if (y < m_y)
	{
		m_roi.top = m_y - (m_y - y);
		m_roi.bottom = m_y;
	}
	else
	{
		m_roi.bottom = m_y + (y - m_y);
		m_roi.top = m_y;
	}

	// Draw new one
	_Rectangle(dc, &m_roi);

	SelectObject(dc, oldpen);
	DeleteObject(pen);

	ReleaseDC(m_hWnd, dc);
}
//=================================================================================
//=================================================================================
void CRubberBand::_Rectangle(HDC dc, RECT *r)
{
	MoveToEx(dc, r->left, r->top, NULL); 
	LineTo(dc, r->left+(r->right - r->left), r->top); 
	LineTo(dc, r->left+(r->right - r->left), r->top+(r->bottom - r->top)); 
	LineTo(dc, r->left, r->top+(r->bottom - r->top)); 
	LineTo(dc, r->left, r->top);	
}
//=================================================================================
//=================================================================================