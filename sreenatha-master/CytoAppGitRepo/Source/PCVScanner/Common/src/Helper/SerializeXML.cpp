// SerializeXML.cpp: implementation of the CSerializeXML class.
//
//////////////////////////////////////////////////////////////////////
//
// The code in here is slow when there are hundreds of XML elements. This
// may be because MSXML requires numbers to be converted into strings and
// strings to be converted into BSTRs, or it may be because CSerialiseXML
// and its associated classes are implemented in an inefficient way.
// Not all MSXML functions require BSTRs though, and I have removed unneccesary
// string conversions from the original implementation where possible.
// If we were to use an alternative XML DOM library (such as libxml,
// http://www.xmlsoft.org), this would avoid the need to use BSTRs altogether.
//

#include "stdafx.h"
#import <msxml6.dll>
#include "SerializeXML.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFile CSerializeXML::m_dummyFile;

CSerializeXML::CSerializeXML(const CString& fileName, UINT nMode, 
                             IStream* streamPtr/*=NULL*/,
                             CDocument* docPtr/*=NULL*/) :
CArchive(&m_dummyFile, nMode, 0, NULL),
m_fileName(fileName),
m_streamPtr(streamPtr)
{
	m_pDocument = docPtr;
	m_bForceFlat = FALSE;

	ASSERT(!fileName.IsEmpty() || streamPtr != NULL);

	// Get reference of XML document	
	try 
	{
		m_xmlDocPtr = GTXXML::IXMLDOMDocumentPtr(__uuidof(GTXXML::DOMDocument60));
	}

	catch (_com_error e)
	{

	}

	if (m_xmlDocPtr == NULL)
	{
		::AfxMessageBox(_T("Can't get reference to XML parser, Ensure that msxml.dll is installed"));
		return;
	}
	
	// Don't want async
	m_xmlDocPtr->put_async(VARIANT_FALSE);
	m_xmlDocPtr->put_preserveWhiteSpace(VARIANT_TRUE);

	if (IsLoading())
	{
		VARIANT_BOOL varResult = VARIANT_FALSE;

		try
		{
			if (m_streamPtr == NULL)
			{
				varResult = m_xmlDocPtr->load(_variant_t(fileName));
			}
			else
			{
				LARGE_INTEGER largeZero;

				largeZero.QuadPart = 0;

				m_streamPtr->Seek(largeZero, 0, NULL);
				varResult = m_xmlDocPtr->load(_variant_t(m_streamPtr));
			}
		}

		catch (_com_error e)
		{
			varResult = VARIANT_FALSE;
		}
	}
}



CSerializeXML::~CSerializeXML()
{
	CSerializeXML::Close();
}

void CSerializeXML::Close()
{
	HRESULT hr;

	try
	{
		if (IsStoring())
		{
			if (m_streamPtr == NULL)
			{
				BSTR fileNameBSTR = m_fileName.AllocSysString();
				hr = m_xmlDocPtr->save(fileNameBSTR);
				::SysFreeString(fileNameBSTR);
			}
			else
			{
				hr = m_xmlDocPtr->save(_variant_t(m_streamPtr));
			}
		}
	}

	catch (_com_error e)
	{
		ASSERT(FALSE);
	}
}

CSerializeXMLNode* CSerializeXML::GetCurrentNode()
{ 
	if (m_nodeList.size() <= 0)
	{
		return NULL;
	}
		
	return m_nodeList.top();
};

void CSerializeXML::RemoveNode(CSerializeXMLNode* xmlArchiveNodePtr)
{
	ASSERT(m_nodeList.size() > 0);
	ASSERT(xmlArchiveNodePtr == m_nodeList.top());

	delete m_nodeList.top();
	m_nodeList.pop();
}

CSerializeXMLNode* CSerializeXML::GetNode(LPCTSTR nodeNameStr)
{
	try
	{
		GTXXML::IXMLDOMNodePtr fatherNodePtr;
		
		if (m_nodeList.size() == 0)
		{
			fatherNodePtr = m_xmlDocPtr;
		}
		else
		{
			fatherNodePtr = m_nodeList.top()->m_nodePtr;
		}

		if (fatherNodePtr == NULL)
		{
			return NULL;
		}

		if (IsStoring())
		{
			// Archive is storing
			CSerializeXMLNode* xmlArchiveNodePtr = new CSerializeXMLNode(this, m_xmlDocPtr->createElement(nodeNameStr), fatherNodePtr);
			
			m_nodeList.push(xmlArchiveNodePtr);

			return xmlArchiveNodePtr;
		}

		// Archive is Loading
		GTXXML::IXMLDOMNodeListPtr	nodeListPtr;
		GTXXML::IXMLDOMNodePtr		nodePtr;

		// If child node list is not empty, we are loading using the tags to
		// create CObject derived objects (CArray<Cobject* CObject*>, use child list
		
		if (m_nodeList.size() > 0)
		{
			CSerializeXMLNode* xmlNodePtr = m_nodeList.top();
			nodeListPtr = xmlNodePtr->m_childNodeListPtr;

			if (nodeListPtr != NULL && nodeListPtr->length > 0)
			{
				int childIndex = xmlNodePtr->m_childIndex;

				if (childIndex < nodeListPtr->length)
				{
					nodeListPtr->get_item(childIndex, &nodePtr);

					CSerializeXMLNode* xmlArchiveNodePtr = new CSerializeXMLNode(this, nodePtr, m_xmlDocPtr);

					m_nodeList.push(xmlArchiveNodePtr);

		
					return xmlArchiveNodePtr;
				}

				ASSERT(FALSE);
			}
		}

		// Get all nodes with this name
		if (GTXXML::IXMLDOMDocumentPtr(fatherNodePtr) != NULL)
		{
			// First level node in document
			ASSERT(strlen(nodeNameStr) > 0);
			nodeListPtr = GTXXML::IXMLDOMDocumentPtr(fatherNodePtr)->getElementsByTagName(nodeNameStr);
		}
		else
		{
			// Get node with desired name
			nodeListPtr = GTXXML::IXMLDOMElementPtr(fatherNodePtr)->getElementsByTagName(nodeNameStr);
		}


		int childIndex = 0;
		if (m_nodeList.size() > 0)
		{
			childIndex = m_nodeList.top()->m_childIndex;
		}

		if (childIndex < nodeListPtr->length)
		{
			nodeListPtr->get_item(childIndex, &nodePtr);
		}

		CSerializeXMLNode* xmlArchiveNodePtr = new CSerializeXMLNode(this, nodePtr, m_xmlDocPtr);

		m_nodeList.push(xmlArchiveNodePtr);

		return xmlArchiveNodePtr;
	}

	catch (...)
	{

	}

	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Class CSerializeXMLNode
//

// Cache of objects created
CMapStringToPtr		CSerializeXMLNode::m_classMap;

CSerializeXMLNode::CSerializeXMLNode(CSerializeXML* archivePtr, GTXXML::IXMLDOMElementPtr newNodePtr, GTXXML::IXMLDOMNodePtr fatherNodePtr) :
m_archivePtr(archivePtr), 
m_nodePtr(newNodePtr),
m_fatherNodePtr(fatherNodePtr)
{
	m_childIndex = 0;
}

CSerializeXMLNode::~CSerializeXMLNode()
{
}

void CSerializeXMLNode::Close()
{
	CSerializeXML* archivePtr = m_archivePtr;

	if (archivePtr->IsStoring())
	{
		m_fatherNodePtr->appendChild(m_nodePtr);
	}

	archivePtr->RemoveNode(this);
}

void CSerializeXMLNode::GetChildName(int childIndex, CString &childName)
{
	ASSERT(m_nodePtr != NULL);

	if (m_nodePtr != NULL)
	{
		ASSERT(m_childNodeListPtr != NULL && childIndex < m_childNodeListPtr->length);

		GTXXML::IXMLDOMNodePtr nodePtr;
		m_childNodeListPtr->get_item(childIndex, &nodePtr);


		childName = (BSTR)nodePtr->nodeName;
	}
	else
		childName.Empty();
}

int CSerializeXMLNode::GetNoChildren()
{
	if (m_nodePtr == NULL)
	{
		return 0;
	}

	if (m_childNodeListPtr == NULL)
	{
		// Get all nodes with this name
		m_childNodeListPtr = m_nodePtr->childNodes;
	}  

	return m_childNodeListPtr->length;
}


GTXXML::IXMLDOMNodePtr CSerializeXMLNode::CreateDataNode(LPCTSTR nodeName, LPCTSTR nodeText)
{
	GTXXML::IXMLDOMNodePtr newNodePtr = m_archivePtr->m_xmlDocPtr->createElement(nodeName);

	newNodePtr->text = nodeText;

	m_nodePtr->appendChild(newNodePtr);

	return newNodePtr;
}


GTXXML::IXMLDOMNodePtr CSerializeXMLNode::GetDataNode(LPCTSTR nodeName, CString& nodeText)
{
	// Archive is Loading
	GTXXML::IXMLDOMNodeListPtr nodeListPtr;
	GTXXML::IXMLDOMNodePtr dataNodePtr;

	// Get all nodes with this name
	CSerializeXMLNode* nodePtr = m_archivePtr->GetCurrentNode();

	if (nodePtr == NULL)
	{
		return NULL;
	}


	nodeListPtr = nodePtr->m_nodePtr->getElementsByTagName(nodeName);

	nodeListPtr->get_item(m_childIndex, &dataNodePtr);

	if (dataNodePtr == NULL)
	{
		return NULL;
	}


	nodeText = (BSTR)dataNodePtr->text;
	

	return dataNodePtr;
}


void CSerializeXMLNode::DataNode(LPCTSTR attrName, CString& attrValue)
{
	if (m_nodePtr == NULL)
	{
		return;
	}

	CString attrText;

	if (m_archivePtr->IsStoring())
	{
		attrText = attrValue;

		// Check if there are trailing spaces
		if (attrText.Right(1) == _T(" "))
		{
#if _MFC_VER >= 0x0700
			attrText.Append(_T("_TRSP#@"));
#else
			attrText += _T("_TRSP#@");
#endif
		}

		CreateDataNode(attrName, attrText);
	}
	else
	{
		if (GetDataNode(attrName, attrText) != NULL)
		{
			// Check if there are trailing spaces
			if (attrText.Right(7) == _T("_TRSP#@"))
			{
				attrText = attrText.Left(attrText.GetLength() - 7);
			} 

			attrValue = attrText;
		}
	}
}

void CSerializeXMLNode::DataNode(LPCTSTR attrName, CTimeSpan& attrValue)
{
	if (m_nodePtr == NULL)
	{
		return;
	}

	if (m_archivePtr->IsStoring())
	{
		CString attrText;
		
		attrText = attrValue.Format(_T("%D %H:%M:%S"));

		CreateDataNode(attrName, attrText);
	}
	else
	{
		CString tmpString;

		if (GetDataNode(attrName, tmpString) == NULL)
		{
			return;
		}

		int d=0,h=-1,mm=-1,s=-1;

		attrValue = CTimeSpan(0);

		int noFields = sscanf(tmpString, _T("%d %d:%d:%d"), &d, &h, &mm, &s);
	
		if (noFields != 4)
		{
			return;
		}

		attrValue = CTimeSpan(d, h, mm, s);
	}
}

void CSerializeXMLNode::DataNode(LPCTSTR attrName, CTime& attrValue)
{
	if (m_nodePtr == NULL)
	{
		return;
	}

	if (m_archivePtr->IsStoring())
	{
		CString attrText;
		
		attrText = attrValue.Format(_T("%d/%m/%Y %H:%M:%S"));

		CreateDataNode(attrName, attrText);
	}
	else
	{
		CString tmpString;

		if (GetDataNode(attrName, tmpString) == NULL)
		{
			return;
		}

		int d=0,m=0,y=0,h=-1,mm=-1,s=-1;

		attrValue = CTime(0);

		int noFields = sscanf(tmpString, _T("%d/%d/%d %d:%d:%d"), &d, &m, &y, &h, &mm, &s);
	
		if (noFields != 6)
		{
			return;
		}

		attrValue = CTime(y, m, d, h, mm, s);
	}
}

void CSerializeXMLNode::DataNode(LPCTSTR attrName, bool& attrValue)
{
	if (m_nodePtr == NULL)
	{
		return;
	}

	if (m_archivePtr->IsStoring())
	{
		CString attrText;
		
		attrText = attrValue ? _T("true") : _T("false");

		CreateDataNode(attrName, attrText);
	}
	else
	{
		CString tmpString;

		if (GetDataNode(attrName, tmpString) == NULL)
		{
			return;
		}

		attrValue = tmpString == _T("true") ? true : false;
	}
}

void CSerializeXMLNode::DataNode(LPCTSTR attrName, double& attrValue)
{
	if (m_nodePtr == NULL)
	{
		return;
	}

	CString attrText;

	if (m_archivePtr->IsStoring())
	{
		attrText.Format(_T("%g"), attrValue);

		CreateDataNode(attrName, attrText);
	}
	else
	{
		if (GetDataNode(attrName, attrText) == NULL)
		{
			return;
		}

		attrValue = atof(attrText);
	}
}

void CSerializeXMLNode::DataNode(LPCTSTR attrName, int& attrValue)
{
	if (m_nodePtr == NULL)
	{
		return;
	}

	CString attrText;

	if (m_archivePtr->IsStoring())
	{
		attrText.Format(_T("%d"), attrValue);

		CreateDataNode(attrName, attrText);
	}
	else
	{
		if (GetDataNode(attrName, attrText) == NULL)
		{
			return;
		}

		attrValue = atoi(attrText);
	}
}

void CSerializeXMLNode::DataNode(LPCTSTR attrName, LONG& attrValue)
{
	int tmpValue = attrValue;
	DataNode(attrName, (int&)tmpValue);
	attrValue = tmpValue;
}

void CSerializeXMLNode::DataNode(LPCTSTR attrName, BYTE& attrValue)
{
	int tmpValue = attrValue;
	DataNode(attrName, (int&)tmpValue);
	attrValue = tmpValue;
}

void CSerializeXMLNode::DataNode(LPCTSTR attrName, UINT& attrValue)
{
	int tmpValue = attrValue;
	DataNode(attrName, (int&)tmpValue);
	attrValue = tmpValue;
}

void CSerializeXMLNode::DataNode(LPCTSTR attrName, COleVariant& variant)
{
	if (m_nodePtr == NULL)
	{
		return;
	}

    CSerializeXMLNode* curNodePtr = m_archivePtr->GetNode(attrName);

	if (m_archivePtr->IsStoring())
	{
			CString str;

			str.Format("%d", (UINT)variant.vt);
			curNodePtr->DataNode(_T("Vt"), str);
			// okay, try this for conversion
			COleVariant tmp = variant;
			tmp.ChangeType(VT_BSTR);
			str = (LPCTSTR)(_bstr_t)tmp.bstrVal;
			curNodePtr->DataNode(_T("VarValue"), str);
	}
	else
	{
		variant.Clear();

		// Have we any children to pick up
		if (curNodePtr->GetNoChildren() > 0)
		{
			int vt;
			CString str;

			DataNode(_T("Vt"), vt);
			DataNode(_T("VarValue"), str);

			// Create variant
			COleVariant tmp((LPCTSTR )str, VT_BSTR);
			tmp.ChangeType((VARTYPE)vt);
			variant = tmp;
		}
	}

	m_childIndex = 0;

	curNodePtr->Close();
}

void CSerializeXMLNode::DataNode(LPCTSTR attrName, CStringArray& stringArray)
{
	if (m_nodePtr == NULL)
	{
		return;
	}

    CSerializeXMLNode* curNodePtr = m_archivePtr->GetNode(attrName);

	if (m_archivePtr->IsStoring())
	{
		for (int arrayIndex = 0; arrayIndex < stringArray.GetSize(); arrayIndex++)
		{
			curNodePtr->DataNode(_T("CString"), stringArray[arrayIndex]);
		}
	}
	else
	{
		// Loading
		stringArray.RemoveAll();

		int numberObjects = curNodePtr->GetNoChildren();

		for (m_childIndex = 0; m_childIndex < numberObjects; m_childIndex++)
		{
			CString attrValue;

			DataNode(_T("CString"), attrValue);

			stringArray.Add(attrValue);
		}
	}

	m_childIndex = 0;
	curNodePtr->Close();
}

void CSerializeXMLNode::DataNode(LPCTSTR attrName, CDWordArray& wordArray)
{
	if (m_nodePtr == NULL)
	{
		return;
	}

    CSerializeXMLNode* curNodePtr = m_archivePtr->GetNode(attrName);

	if (m_archivePtr->IsStoring())
	{
		for (int arrayIndex = 0; arrayIndex < wordArray.GetSize(); arrayIndex++)
		{
			int tempInt = wordArray[arrayIndex];
			curNodePtr->DataNode(_T("int"), tempInt);
		}
	}
	else
	{
		// Loading
		wordArray.RemoveAll();

		int numberObjects = curNodePtr->GetNoChildren();

		for (m_childIndex = 0; m_childIndex < numberObjects; m_childIndex++)
		{
			int attrValue;

			DataNode(_T("int"), attrValue);

			wordArray.Add(attrValue);
		}
	}

	m_childIndex = 0;
	curNodePtr->Close();
}

void CSerializeXMLNode::DataNode(LPCTSTR attrName, CMapStringToPtr& pointerMap)
{
	CString keyString;
	int		tempInt;

	if (m_nodePtr == NULL)
	{
		return;
	}

    CSerializeXMLNode* curNodePtr = m_archivePtr->GetNode(attrName);

	if (m_archivePtr->IsStoring())
	{
		for	(POSITION pos = pointerMap.GetStartPosition(); pos != NULL;)
		{
			pointerMap.GetNextAssoc(pos, keyString, (void*&)tempInt);
			curNodePtr->DataNode(_T("keyString"), keyString);
			curNodePtr->DataNode(_T("int"), tempInt);
		}
	}
	else
	{
		// Loading
		pointerMap.RemoveAll();

		int numberObjects = curNodePtr->GetNoChildren();

		for (int i = 0; i < numberObjects; m_childIndex++, i+=2)
		{
			DataNode(_T("keyString"), keyString);
			
			DataNode(_T("int"), tempInt);

			pointerMap.SetAt(keyString, (void*&) tempInt);
		}
	}
	
	m_childIndex = 0;
	curNodePtr->Close();
}

void CSerializeXMLNode::DataNode(LPCTSTR attrName, CMapStringToString& stringMap)
{
	CString keyString;
	CString	tempString;

	if (m_nodePtr == NULL)
	{
		return;
	}

    CSerializeXMLNode* curNodePtr = m_archivePtr->GetNode(attrName);

	if (m_archivePtr->IsStoring())
	{
		for	(POSITION pos = stringMap.GetStartPosition(); pos != NULL;)
		{
			stringMap.GetNextAssoc(pos, keyString, tempString);
			curNodePtr->DataNode(_T("keyString"), keyString);
			curNodePtr->DataNode(_T("string"), tempString);
		}
	}
	else
	{
		// Loading
		stringMap.RemoveAll();

		int numberObjects = curNodePtr->GetNoChildren();

		for (int i = 0; i < numberObjects; m_childIndex++, i+=2)
		{
			DataNode(_T("keyString"), keyString);
			DataNode(_T("string"), tempString);

			stringMap.SetAt(keyString, tempString);
		}
	}
	
	m_childIndex = 0;
	curNodePtr->Close();
}

void CSerializeXMLNode::DataNode(LPCTSTR attrName, CByteArray& byteArray)
{
	if (m_nodePtr == NULL)
	{
		return;
	}

    CSerializeXMLNode* curNodePtr = m_archivePtr->GetNode(attrName);

	if (m_archivePtr->IsStoring())
	{
		for (int arrayIndex = 0; arrayIndex < byteArray.GetSize(); arrayIndex++)
		{
			int tempInt = byteArray[arrayIndex];
			curNodePtr->DataNode(_T("byte"), tempInt);
		}
	}
	else
	{
		// Loading
		byteArray.RemoveAll();

		int numberObjects = curNodePtr->GetNoChildren();

		for (m_childIndex = 0; m_childIndex < numberObjects; m_childIndex++)
		{
			int attrValue;

			DataNode(_T("byte"), attrValue);

			byteArray.Add(attrValue);
		}
	}

	m_childIndex = 0;
	curNodePtr->Close();
}

// Loads into existing objects
void CSerializeXMLNode::DataNode(LPCTSTR attrName, CObject& object)
{
	m_archivePtr->GetNode(attrName);
	object.Serialize(*m_archivePtr);
	m_archivePtr->GetCurrentNode()->Close();
}

// Creates new object when loading
void CSerializeXMLNode::DataNode(LPCTSTR attrName, CObject*& objectPtr)
{
	if (m_archivePtr->IsStoring() && objectPtr == NULL)
	{
		return;
	}

    CSerializeXMLNode* curNodePtr = m_archivePtr->GetNode(attrName);

	if (curNodePtr == NULL)
	{
		return;
	}

	if (m_archivePtr->IsStoring())
	{
		objectPtr->Serialize(*m_archivePtr);
	}
	else
	{
		objectPtr = NULL;

		do
		{
			// Dummy loop, executes only once
			int numberObjects = curNodePtr->GetNoChildren();

			if (numberObjects == 0)
			{
				break;
			}

			CString childNodeName;
			curNodePtr->GetChildName(0, childNodeName);

			objectPtr = CreateObject(childNodeName);

			if (objectPtr == NULL)
			{
				ASSERT(FALSE);
				break;
			}

			objectPtr->Serialize(*m_archivePtr);

			break;
		}
		while (FALSE); // C4702 OK
	}

	m_childIndex = 0;
	curNodePtr->Close();
}

CObject* CSerializeXMLNode::CreateObject(const CString& className)
{
	CRuntimeClass* pClass = NULL;

	// First see if the CRuntimeClass object is cached
	if (!m_classMap.Lookup(className, (void*&)pClass))
	{
		// Search app specific classes
		AFX_MODULE_STATE* pModuleState = AfxGetModuleState();

		for (pClass = pModuleState->m_classList; pClass != NULL; pClass = pClass->m_pNextClass)
		{
			if (className == pClass->m_lpszClassName)
			{
				m_classMap[className] = (void*&) pClass;
				break;
			}
		}
	}

	if (pClass == NULL)
	{
		TRACE1("You did not declare the class %s DECLARE_XMLSERIAL", (const char *) className); 
		ASSERT(FALSE);
		return NULL;
	}

	return pClass->CreateObject();
}

