
/*
 * classlist.cpp
 * JMB July 2001
 */

#include "stdafx.h"

#include "classlist.h"
#include "cfuncs.h"


// Temporary for Ariol v1.5 beta
const char szCPK[] = _T("Control Peak");
const char szDPK[] = _T("Diploid Peak");
const char szAPK[] = _T("Aneuploid Peak");


void ClassListItem::XMLSerialize(CSerializeXML & ar)
{
	long ClassColor = colour;
	long HotKey = hotKey;
	long HotKeyModifiers = hotKeyModifiers;
	int Group = classGroup;
	XMLCLASSNODENAME("ClassListItem", ar);
	XMLDATA(name);
	XMLDATA(ClassColor);
	XMLDATA(checkState);
	XMLDATA(HotKey);
	XMLDATA(HotKeyModifiers);
	XMLDATA(Group);
	XMLDATA(classRequired);
	XMLENDNODE;

	colour = ClassColor;
	hotKey = (WORD) HotKey;
	hotKeyModifiers = (WORD) HotKeyModifiers;
	classGroup = static_cast<ClassGroup> (Group);



}

ClassList::~ClassList()
{
	Clear();
}


void
ClassList::Clear()
{
	while (pItems)
	{
		ClassListItem *pI = pItems;

		pItems = pItems->pNext;

		delete pI;
	}
}


ClassListItem*
ClassList::AddItem()
{
	ClassListItem *pNewItem = new ClassListItem;	

	if (pNewItem != NULL)
	{
		if (pItems != NULL)
		{
			// Insert new item at end of item list.
			ClassListItem *pI = pItems;

			while (pI->pNext != NULL) pI = pI->pNext;

			pI->pNext = pNewItem;
		}
		else
			pItems = pNewItem;
	}

	return pNewItem;
}


ClassListItem*
ClassList::AddItem(const char name[])
{
	if (GetItem(name))
	{
		// already exist, so quit
		return NULL;
	}

	ClassListItem *pNewItem = AddItem();

	if (pNewItem) pNewItem->SetName(name);

	return pNewItem;
}


ClassListItem*
ClassList::GetItem(const char name[])
{
	ClassListItem *pItem = NULL;

	ClassListItem *pI = pItems;

	while (pI)
	{
		if (pI->name.CompareNoCase(name) == 0)
		{
			pItem = pI;
			break;
		}

		pI = pI->pNext;
	}

	return pItem;
}


ClassListItem*
ClassList::GetItem(int index)
{
	ClassListItem *pItem = NULL;

	ClassListItem *pI = pItems;
	int i = 0;

	while (pI)
	{
		if (i == index)
		{
			pItem = pI;
			break;
		}

		pI = pI->pNext;
		i++;
	}

	return pItem;
}


int
ClassList::GetItemIndex(const char name[])
{
	int nIndex = -1;

	ClassListItem *pI = pItems;
	int i = 0;

	while (pI)
	{
		if (pI->name.CompareNoCase(name) == 0)
		{
			nIndex = i;
			break;
		}

		pI = pI->pNext;
		i++;
	}

	return nIndex;
}


ClassListItem*
ClassList::GetItemFromHotKey(WORD key, WORD modifiers)
{
	ClassListItem *pItem = NULL;

	ClassListItem *pI = pItems;

	while (pI)
	{
		if (pI->hotKey == key && pI->hotKeyModifiers == modifiers)
		{
			pItem = pI;
			break;
		}

		pI = pI->pNext;
	}

	return pItem;
}


void
ClassList::ZeroItemCounts()
{
	ClassListItem *pI = pItems;

	while (pI)
	{
		pI->count = 0;

		pI = pI->pNext;
	}
}


BOOL
ClassList::RemoveItem(int index)
{
	BOOL status = FALSE;

	ClassListItem *pItem     = NULL;
	ClassListItem *pPrevItem = NULL;

	ClassListItem *pI = pItems;
	int i = 0;

	while (pI)
	{
		if (i == index)
		{
			pItem = pI;
			break;
		}

		pPrevItem = pI;
		pI = pI->pNext;
		i++;
	}

	if (pItem)
	{
		if (pPrevItem)
			pPrevItem->pNext = pItem->pNext;
		else
			pItems = pItem->pNext; // No previous - at head

		delete pItem;

		status = TRUE;
	}

	return status;
}


int
ClassList::CountItems()
{
	int numItems = 0;

	ClassListItem *pI = pItems;

	while (pI)
	{
		pI = pI->pNext;
		numItems++;
	}
	
	return numItems;
}



BOOL
ClassList::Save(bool bXMLFormat /* = false */)
{
	BOOL status = FALSE;
	FILE *pFILE;

	// Get file path
	int numChar = fileName.ReverseFind( '\\' );
	CString filePath = fileName.Left( numChar);

	// Create all directories comprising the file path
	if (CreateFullPathDirectory( (LPCTSTR) filePath))
    {
		if (bXMLFormat)
		{
			CSerializeXML classlistAR(fileName, CArchive::store);
			if (XMLSerialize(classlistAR))
				return TRUE;
		}
		else
		{
			
			if ((pFILE = fopen((PCSTR)fileName, "w")) != NULL)
			{
				// Start off with a version
				fprintf( pFILE, "%d\n", (int) CLASSLISTV1_5);

				ClassListItem *pI = pItems;
				while (pI)
				{
					WORD key = 0, modifiers = 0;
					pI->GetHotKey(&key, &modifiers);

					// Tab delimited fields allow spaces in strings
					fprintf(pFILE, "%s\t%d\t%d\t%d\t%d\n",
								pI->GetName(), pI->GetColour(), key, modifiers, pI->GetCheckState() );
					pI = pI->pNext;
				}		

				if (fclose(pFILE) == 0) status = TRUE;
			}
			
		}
    }
	else
	{
		AfxMessageBox("Could not create classlist directory", MB_ICONEXCLAMATION | MB_OK);
	}

	return status;
}


BOOL
ClassList::Load()
{
	BOOL status = FALSE;
	FILE *pFILE;
    char strname[_MAX_PATH];

	BOOL bXMLFormat = IsFileXMLFormat(this->fileName);

    strcpy(strname, (LPCTSTR) fileName);
    if (SetEveryonePermission(strname))
    {
		if (bXMLFormat)
		{
			CSerializeXML classlistAR(fileName, CArchive::load);
			if (XMLSerialize(classlistAR))
				return TRUE;
		}
		else
		{
			if ((pFILE = fopen((PCSTR)fileName, "r")) != NULL)
			{
				Clear();

				// Version number stored by itself on first line.
				// No version stored for       CLASSLISTV1   (Spot V1 & Ariol V1).
				// Started storing version for CLASSLISTV1_5 (Spot V2 & Ariol V1.5 onwards).
				int version = -1;
				char szVersion[256]={0};
				if (fscanf( pFILE, "%s\n", &szVersion) == 1)
				{
					char * endptr = NULL;
					long number = strtol( szVersion, &endptr,  10);
					// Check the conversion stop at the string terminator
					if (endptr && *endptr == 0)
					{
						// Successfully converted the WHOLE first line to a 
						// number so it must be the version line.
						version = number;
					}
					else
					{
						// Failed to convert whole line to number so must 
						// be the first line of the original file version.
						version = CLASSLISTV1;
						// Go back to start of file
						fseek( pFILE, 0L, SEEK_SET);
					}
				}

				if (version == CLASSLISTV1)
				{
					while (!feof(pFILE))
					{
						char name[64];
						name[0] = '\0';
						COLORREF colour = 0;
						// fscanf reads ints with %d
						int key = 0, modifiers = 0;

						// Fields are delimited by tabs, not spaces, so that strings can
						// contain spaces. "%[^\t]" reads a string up to a tab.
						if (   fscanf(pFILE, "%[^\t] %d %d %d\n",
											name, &colour, &key, &modifiers) == 4
							&& strlen(name) > 0)
						{
							ClassListItem *pItem = AddItem(name);

							if (pItem)
							{
								pItem->SetColour(colour);
								pItem->SetHotKey((WORD)key, (WORD)modifiers);
							}
						}
						else
							break;
					}
				}
				else if (version == CLASSLISTV1_5)
				{
					// New format, added version and checkstate
					while (!feof(pFILE))
					{
						char name[64] = {0};
						COLORREF colour = 0;
						// fscanf reads ints with %d
						int key = 0, modifiers = 0, checkState = 1;

						// Fields are delimited by tabs, not spaces, so that strings can
						// contain spaces. "%[^\t]" reads a string up to a tab.
						if (   fscanf(pFILE, "%[^\t] %d %d %d %d\n",
											name, &colour, &key, &modifiers, &checkState) == 5
							&& strlen(name) > 0)
						{
							ClassListItem *pItem = AddItem(name);

							if (pItem)
							{
								pItem->SetColour(colour);
								pItem->SetHotKey((WORD)key, (WORD)modifiers);
								pItem->SetCheckState( checkState);
								// Temporary until decide how to load/save the parent class SN17Nov03
								if (CString( szCPK) == pItem->GetName())
								{
									pItem->classGroup = ClassListItem::CLASSGROUP_PLOIDY_CONTROL;
								}
								else if (CString( szDPK) == pItem->GetName() || CString( szAPK) == pItem->GetName())
								{
									pItem->classGroup = ClassListItem::CLASSGROUP_PLOIDY_TUMOR;
								}
							}
						}
						else
							break;
					}
				}
				else
				{
					AfxMessageBox( "Error reading class list,\n unknown version.", MB_OK | MB_ICONEXCLAMATION);
				}

				if (fclose(pFILE) == 0) status = TRUE;
			}
			else
			{
				AfxMessageBox( "Couldn't open class list file.", MB_OK | MB_ICONEXCLAMATION);
			}
		}
    }

	return status;
}


BOOL ClassList::XMLSerialize(CSerializeXML & ar)
{
	double classListVersion = CLASSLISTVERSION;
		
	XMLCLASSNODENAME("ClassList", ar);
	XMLDATA(classListVersion);
	if (ar.IsStoring())
	{
		ClassListItem *pI = pItems;
		while (pI != NULL)
		{
			pI->XMLSerialize(ar);
			pI = pI->pNext;
		}
	}
	else
	{
		Clear();
		XMLSTARTLISTDATA("ClassListItem");

		ClassListItem *pI = AddItem();
		if (pI)
			pI->XMLSerialize(ar);

		XMLENDLISTDATA;
	}
	XMLENDNODE;
	return TRUE;
}

BOOL ClassList::IsFileXMLFormat(LPCTSTR filePath)
{
	// Quick and dirty method to determine if file is XML
	bool bRet = FALSE;
	CFile file;
	char buff[12];

	if (file.Open(filePath, CFile::modeRead))
	{
		if (file.Read(buff, sizeof(buff) -1) == sizeof(buff) -1)
		{
			buff[sizeof(buff) -1] = '\0';
			if (strcmp("<ClassList>", buff) == 0)
				bRet = TRUE;
		}
		file.Close();
	}
	return bRet;


}
