
/*
 * CoolButton.cpp
 *
 * JMB July 2001
 */


#include "stdafx.h"

#include "CoolButton.h"



/////////////////////////////////////////////////////////////////////////////
//
// CCoolButton
//
/////////////////////////////////////////////////////////////////////////////

CCoolButton::CCoolButton()
{
	down      = FALSE;
	up        = FALSE;
	flatMode  = FALSE;

	textColour = RGB(0,0,0);
}

CCoolButton::~CCoolButton()
{
}


BEGIN_MESSAGE_MAP(CCoolButton, CButton)
	//{{AFX_MSG_MAP(CCoolButton)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoolButton message handlers


BOOL
CCoolButton::Create(LPCTSTR lpszCaption, const RECT& rect,
                          CWnd* pParentWnd, UINT nID, BOOL flat)
{
	flatMode = flat;

	return CButton::Create(lpszCaption,
	                       BS_OWNERDRAW|BS_PUSHBUTTON|WS_CHILD|WS_VISIBLE,
	                       rect, pParentWnd, nID);
}

/*
BOOL
CCoolButton::CreateEx(LPCTSTR lpszCaption, DWORD dwStyle, DWORD dwExStyle,
                            const RECT& rect, CWnd* pParentWnd, UINT nID)
{
// Worked out how to do this by looking at MFC source code for CButton::Create().
	CWnd* pWnd = this;

	return pWnd->CreateEx(dwExStyle, _T("BUTTON"), lpszCaption, dwStyle, rect, pParentWnd, nID);
}
*/

void
CCoolButton::SetTextColour(COLORREF colour)
{
	if (textColour != colour)
	{
		textColour = colour;
		//RedrawWindow(); SetWindowText() doesn't redraw window, so don't do it either.
	}
}


void
CCoolButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Add your code to draw the specified item
	CDC *pDC = CDC::FromHandle(lpDrawItemStruct->hDC);

	if (pDC)
	{
		CRect rect = lpDrawItemStruct->rcItem;

		CString text;
		GetWindowText(text);		

		pDC->SetTextColor(textColour);

		pDC->DrawText(text, &rect, DT_CENTER | DT_SINGLELINE | DT_VCENTER);

		if (flatMode)
		{
			if (down)    pDC->Draw3dRect(&rect, RGB(100,100,100), RGB(250,250,250));
			else if (up) pDC->Draw3dRect(&rect, RGB(250,250,250), RGB(100,100,100));
		}
		else
		{
			if (down) pDC->Draw3dRect(&rect, RGB(100,100,100), RGB(250,250,250));
			else      pDC->Draw3dRect(&rect, RGB(250,250,250), RGB(100,100,100));
		}
	}
}


void
CCoolButton::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	if (flatMode)
	{
		// If this button doesn't already have the mouse capture (it will do
		// if the L mouse button has been pressed while the cursor was over it),
		// then capture the mouse, so we can detect when the mouse is no longer
		// over the button. If we did not have the mouse captured, no messages
		// would be received when the mouse was not over the button.
		CWnd *pWnd = GetCapture();

		if (pWnd != (CWnd*)this) SetCapture();


		// Find out what window the mouse cursor is over.
		CPoint screenPoint = point;
		ClientToScreen(&screenPoint);

		pWnd = WindowFromPoint(screenPoint);

		if (pWnd != (CWnd*)this)
		{
			// Mouse cursor is not over button.
			ReleaseCapture();

			down = FALSE;
			up   = FALSE;

			RedrawWindow();
		}
		else
		{
			// Mouse cursor is over button
			if (nFlags & MK_LBUTTON)
			{
				up = FALSE;
				if (!down)
				{
					down = TRUE;
					RedrawWindow();
				}
			}
			else
			{
				down = FALSE;
				if (!up)
				{
					up = TRUE;
					RedrawWindow();
				}
			}
		}
	}


	CButton::OnMouseMove(nFlags, point);
}


void
CCoolButton::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	if (flatMode)
	{
		// First activate the parent window (usually a dialog), if it isn't already.
		// Without this, in flat mode the parent isn't notified the first time a
		// button is clicked, if the parent was not previously active (though the
		// parent will be activated following the click). I don't know why.
		CWnd *pParent = GetParent();
		if (pParent) pParent->SetActiveWindow();
	}

	down = TRUE;
	up   = FALSE;

	// This will set the mouse capture to this window
	CButton::OnLButtonDown(nFlags, point);
}


void
CCoolButton::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	if (flatMode)
	{
		CPoint screenPoint = point;
		ClientToScreen(&screenPoint);

		CWnd *pWnd = WindowFromPoint(screenPoint);
	
		if (pWnd == (CWnd*)this)
		{
			// Mouse button was released within control
			down = FALSE;
			up   = TRUE;
		}
		else
		{
			down = FALSE;
			up   = FALSE;
		}
	}
	else
		down = FALSE;


	RedrawWindow();


	// This will release the mouse capture.	
	CButton::OnLButtonUp(nFlags, point);

	if (flatMode)
	{
		// Make the control act as if it has also received a mouse move message
		// at this point. If this is not done, it is possible to leave the
		// button in an 'up' state after releasing the mouse button then
		// immediately very quickly moving the cursor away from the button
		// (before it can send a mouse move message).
		OnMouseMove(nFlags, point);
	}
}


void
CCoolButton::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default


	// If a derived control is right-clicked and this brings up a context menu,
	// then subsequent clicks outside of the control will not be detected as
	// there will be no intermediate move messages sent because of the context
	// menu. Capturing the mouse is not a solution because the first subsequent
	// click would then be sent to the the wrong control if the cursor was then
	// over a different control.
	// The solution is to redraw the control after receiving a right-click.
	if (   flatMode
	    && (down || up))
	{
		down = FALSE;
		up   = FALSE;

		RedrawWindow();
	}

	
	CButton::OnRButtonDown(nFlags, point);
}

/*
BOOL
CCoolButton::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class


	// Even though this function (at time of writing) does not respond to any
	// command messages, overrides of this function in derived classes may do.
	// This may lead to the mouse cursor having been moved away from the button.
	// Assuming that the overridden function calls the base class function
	// (this function) once it has completed its processing, we now ensure that
	// the mouse is captured so that CCoolButton::OnMouseMove() will get
	// messages wherever the cursor is and be able change the appearance of the
	// button appropriately depending on its position

//	CWnd *pWnd = GetCapture();

//	if (pWnd != (CWnd*)this) SetCapture();

	
	return CButton::OnCommand(wParam, lParam);
}
*/




