////////////////////////////////////////////////////////////////////////////////
//
//	MLock.cpp - Intra-Application locking
//
//	Dave Burgess, Applied Imaging 3rd February 2004
//
//	This is an application level locking that is used in addition to the database locking
//	Once the application has locked a scan (slide, case, etc)
//	these functions should be used to arbitrate access within the application
//	i.e. between multiple thread accesses
//	It relies on a named MUTEX where the name follows the convention:
//	MU_LOCK_<Node Level>_<DB_ID>
//
//	The mutex should be shared across all sets of data where access arbitartion is required
//	There are two overloaded constructors
//	1) Create mutex from supplied DB node level and ID information
//	2) Assign a given mutex object to a lock object
//	If the second form of the constructor is used, the mutex is not deleted
//	in the destructor. This mode is denoted by the m_sublock parameter
//
#include "stdafx.h"
#include "mlock.h"

// Constructor 1
// Create mutex from supplied DB info
CMLock::CMLock(int node_level, int db_id)
{
	TRACE("(%d) CMLock::CMLock() - Start: node_level %d: db_id %d\r\n",
		AfxGetThread()->m_nThreadID, node_level, db_id);

	// Get lock name
	CString lock_name = GetLockName(node_level, db_id);
	
	// Create application locking MUTEX object
	TRACE2("(%d) CMLock::CMLock() - Creating CMutex object (%s)\r\n",
		AfxGetThread()->m_nThreadID, lock_name);
	m_mutex = new CMutex(FALSE, lock_name);

	// ... attach to lock object
	m_lockobj = new CSingleLock(m_mutex);

	// Flag we own the mutex
	m_sublock = false;
}

// Constructor 2
// Just attach existing mutex from supplied locking object into this locking object
CMLock::CMLock(CMLock *mlock)
{
	// Ensure class members are initialised
	m_lockobj = NULL;
	m_mutex = NULL;

	TRACE2("(%d) CMLock::CMLock() - Start: mlock = 0x%x\r\n", AfxGetThread()->m_nThreadID, mlock);
	if (mlock == NULL) {
		TRACE("CMLock::CMLock() - No lock object supplied\r\n");
		return ;
	}

	TRACE2("(%d) CMLock::CMLock() - mutex = 0x%x\r\n", AfxGetThread()->m_nThreadID, mlock->m_mutex);
	if (mlock->m_mutex == NULL) {
		TRACE("CMLock::CMLock() - No mutex found\r\n");
		return ;
	}

	// ... attach to lock object
	m_lockobj = new CSingleLock(mlock->m_mutex);
	m_mutex = mlock->m_mutex;

	// Flag we don't own the mutex
	m_sublock = true;
}
CMLock::~CMLock()
{
	TRACE("(%d) CMLock::~CMLock() - sublock = %s\r\n",
		AfxGetThread()->m_nThreadID, m_sublock ? "true" : "false");

	// Remove mutex if present
	if (m_lockobj) {
		m_lockobj->Unlock();
		delete m_lockobj;
	}
	if (m_mutex && (m_sublock == false)) {
		m_mutex->Unlock();
		delete m_mutex;
	}
}

// Place an application level lock on the specified data node
bool CMLock::AppLock(int timeout)
{
	TRACE("(%d) CMLock::AppLock() - Start: timeout %d\r\n", AfxGetThread()->m_nThreadID, timeout);

	// Must have mutex and locking object
	if ((m_mutex == NULL) || (m_lockobj == NULL)) {
		TRACE("CMLock::AppLock() - No locking information - failed to lock\r\n");
		return false;
	}

	// Try to lock
	if (m_lockobj->Lock(timeout)) {
		TRACE1("(%d) CMLock::AppLock() - Obtained application level MUTEX lock\r\n", AfxGetThread()->m_nThreadID);
		return true;	// OK
	} else {
		TRACE("(%d) CMLock::AppLock() - Could not get MUTEX lock within timeout\r\n", AfxGetThread()->m_nThreadID);
		ASSERT(0);
		return false;	// No lock
	}

	// Check it really did lock
	if (m_lockobj->IsLocked()) {
		TRACE("CMLock::AppLock() - MUTEX lock is LOCKED\r\n");
		return true;	// OK
	} else {
		TRACE("CMLock::AppLock() - MUTEX lock IS NOT LOCKED\r\n");
		ASSERT(0);
		return false;	// No lock
	}
}

// Check if the specified application lock is present
bool CMLock::AppIsLocked()
{
	TRACE("CMLock::AppIsLocked() - Start\r\n");

	// If no application lock object, then we don't know, assume false
	if ((m_mutex == NULL) || (m_lockobj == NULL))
		return false;

	// ... else check for the lock
	if (m_lockobj->IsLocked())
		return true;	// OK
	else
		return false;	// No lock
}

// Clear the specified application lock
void CMLock::AppUnlock()
{
	TRACE1("(%d) CMLock::AppUnlock() - Start\r\n", AfxGetThread()->m_nThreadID);

	if ((m_mutex == NULL) || (m_lockobj == NULL))
		return;

	// could check if locked - TBD

	m_lockobj->Unlock();
	TRACE1("(%d) CMLock::AppUnlock() - Unlocked MUTEX\r\n", AfxGetThread()->m_nThreadID);
}

// Construct the lock mutex name
CString CMLock::GetLockName(int node_level, int db_id)
{
	CString lock_name;

	lock_name.Format("MU_LOCK_%d_%d", node_level, db_id);
	//TRACE1("CMLock::GetLockName() - Name = %s\r\n", lock_name);
	return lock_name;
}
