// Hough.cpp
// 
// M.Gregson 9/5/07
//
// Hough transform

#undef _DO_NOT_DEFINE_FREE
#include "stdafx.h"
#include <woolz.h>
#include <float.h>
#include <math.h>
#include <canvas.h>

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "vcutsfile.h"
#include "UTS.h"

#include "GIPImage.h"
#include "gip.h"
#include "HoughTrans.h"

//#define DRAW_RESULTS	// define if we want to draw lines in external topcan canvas showing what we have found

#ifdef DRAW_RESULTS
extern Canvas *topcan;
#endif

static const double PI = 3.1415926538;


BYTE *LBP(BYTE *Image, int Width, int Height, float Radius, float NoiseThresh)
{
	GIP	 GIPRoc;
	BYTE *LBPImage = NULL;

	if (GIPRoc.Create())
	{
		void	**G1, **G2;
		
		G1 = (void **)GIPRoc.GIPCreateImage(Width, Height, 0, 8);
		G2 = (void **)GIPRoc.GIPCreateImage(Width, Height, 0, 8);

		GIPRoc.PutImage((GIPImage **)G1, (BYTE *)Image);

		GIPRoc.BeginIP();
		GIPRoc.LBP((GIPImage **)G1, (GIPImage **)G2, Radius, NoiseThresh);
		GIPRoc.EndIP();

		LBPImage = new BYTE[Width * Height];

		GIPRoc.GetImage((GIPImage **)G2, (BYTE *)LBPImage);

		GIPRoc.GIPReleaseImage((GIPImage **)G1);
		GIPRoc.GIPReleaseImage((GIPImage **)G2);	
	}

	return LBPImage;
}

// Forward decl
struct object *createCoverslipObject(int cx, int cy, int radius, int imageHeight, int angle1, int angle2);

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
// Local woolz object operations
//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
//	clear data under idom in an image for a given object with certain value
    
static void clear_values(struct object *obj, FSCHAR *image, int image_width, int image_height, int value)
{
    FSCHAR *imptr, *imend;
    struct iwspace iwsp;
    struct gwspace gwsp;
    register int k;

    if (wzcheckobj(obj)) {
    	fprintf(stderr,"Bad obj sent to clear_values\n");
    	return;
    }

    if (wzemptyidom(obj->idom)) {
    	fprintf(stderr, "Empty idom sent to clear_values\n");
    	return;
    }

    if (obj->vdom == NULL) {
    	fprintf(stderr, "Null vdom sent to clear_values\n");
    	return;
    }

    if (obj->vdom->type != 1) {
    	fprintf(stderr, "vdom type = %d\n",obj->vdom->type);
    	if (obj->vdom->type == 21)
    		vdom21to1(obj);
    	else
    		return;
    }
    	
    imend=image + image_width * (image_height - 1);
    	
    initgreyscan(obj,&iwsp,&gwsp);
    while (nextgreyinterval(&iwsp) == 0) {
  		if ((iwsp.linpos >=0) && (iwsp.linpos < image_height))
		{
			imptr=imend - iwsp.linpos * image_width + iwsp.lftpos;

    		for (k=iwsp.lftpos; k<=iwsp.rgtpos; k++) 
			{
				if ((k>=0) && (k<image_width))
   					*imptr=value;
    			imptr++;
    		}
		}
    }

}
//--------------------------------------------------------------------------------------
//	fill object valuetable with data under idom from a given image
    
static void fill_values(struct object *obj, FSCHAR *image, int image_width, int image_height)
{
    FSCHAR *imptr, *imend;
    struct iwspace iwsp;
    struct gwspace gwsp;
    register GREY *g;
    register int k;
    FSCHAR val;

    if (wzcheckobj(obj)) {
    	fprintf(stderr,"Bad obj sent to fill_values\n");
    	return;
    }

    if (wzemptyidom(obj->idom)) {
    	fprintf(stderr, "Empty idom sent to fill_values\n");
    	return;
    }

    if (obj->vdom == NULL) {
    	fprintf(stderr, "Null vdom sent to fill_values\n");
    	return;
    }

    if (obj->vdom->type != 1) {
    	fprintf(stderr, "vdom type = %d\n",obj->vdom->type);
    	if (obj->vdom->type == 21)
    		vdom21to1(obj);
    	else
    		return;
    }
    	
    imend=image + image_width * (image_height - 1);
    	
    initgreyscan(obj,&iwsp,&gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
	{
    	g = gwsp.grintptr;

    	imptr=imend - iwsp.linpos * image_width + iwsp.lftpos;

    	for (k=iwsp.lftpos; k<=iwsp.rgtpos; k++) 
		{
			if ((iwsp.linpos >=0) && (iwsp.linpos < image_height) && (k>=0) && (k<image_width))
			{
				val =*imptr;
		    	*g = (GREY)val;
			}
    		g++; imptr++;
		}
    }
}
    
//--------------------------------------------------------------------------------------
//	draw object values under idom into an image
    
static void set_values(struct object *obj, FSCHAR *image, int image_width, int image_height)
{
    FSCHAR *imptr, *imend;
    struct iwspace iwsp;
    struct gwspace gwsp;
    register GREY *g;
    register int k;
    FSCHAR val;

    if (wzcheckobj(obj)) {
    	fprintf(stderr,"Bad obj sent to set_values\n");
    	return;
    }

    if (wzemptyidom(obj->idom)) {
    	fprintf(stderr, "Empty idom sent to set_values\n");
    	return;
    }

    if (obj->vdom == NULL) {
    	fprintf(stderr, "Null vdom sent to set_values\n");
    	return;
    }

    if (obj->vdom->type != 1) {
    	fprintf(stderr, "vdom type = %d\n",obj->vdom->type);
    	if (obj->vdom->type == 21)
    		vdom21to1(obj);
    	else
    		return;
    }
    	
    imend=image + image_width * (image_height - 1);
    	
    initgreyscan(obj,&iwsp,&gwsp);
    while (nextgreyinterval(&iwsp) == 0) {
    	g = gwsp.grintptr;
    	imptr=imend - iwsp.linpos * image_width + iwsp.lftpos;

    	for (k=iwsp.lftpos; k<=iwsp.rgtpos; k++) 
		{
			if ((iwsp.linpos >=0) && (iwsp.linpos < image_height) && (k>=0) && (k<image_width))
			{
				val = (FSCHAR)*g;
    			*imptr = val;
			}
    		g++; imptr++;
    	}
	}
}


//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
// Morphological operations
//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
static FSCHAR *fsMaxVertical(FSCHAR *frame, int cols, int lines, int halfwidth)
{
	register FSCHAR *fptr, *oldfptr, *vfptr;
	register int col,line;
	FSCHAR max;
	FSCHAR *vframe;
	int size, fw;
	short rack[256];
	
	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	size=lines*cols;

	/* allocate result frame */
	vframe=(FSCHAR *)Malloc(size);

	if (vframe==NULL) {
		fprintf(stderr,"\nfsmaxvertical: could not allocate new frame\n");
		return(NULL);
	}

	/* perform vertical max on frame and save in vframe */

	for (col=0; col<cols; col++) {	/* for each image column */

		/* clear rack */
		memset(rack,0,sizeof(rack[0])*256);

		max=0;
		oldfptr = fptr = frame+col;
		vfptr = vframe+col;


		/* put first halfwidth+1 points into rack and find max */
		for (line=0; line<halfwidth+1; line++) {
			if (*fptr>max)
				max=*fptr;

			rack[*fptr]++;
	
			fptr+=cols;
		}

		/* deal with pixels within half filter 
		   width of bottom edge of image */

		for (line=0; line<halfwidth; line++) {
			/* store max in vframe */
			*vfptr=max;
			vfptr+=cols;

			/* update rack counts */
			rack[*fptr]++;

			/* check if newval is the max */
			if (*fptr>max)
				max=*fptr;

			fptr+=cols;
		}

		/* deal with pixels in main body of image */

		for (line=halfwidth; line<lines-halfwidth-1; line++) {

			/* store max in vframe */
			*vfptr=max;
			vfptr+=cols;
		
			/* remove oldval from rack, insert newval
		   	   and determine new max value */

			if (*oldfptr!=*fptr) {

				/* update rack counts */
				rack[*fptr]++;
				rack[*oldfptr]--;

				/* check if newval is the max */
				if (*fptr>max)
					max=*fptr;

				/* check if oldval was the max
		   		   and if so find new max */
				while (rack[max] == 0)
					max--;
			}

			fptr+=cols;
			oldfptr+=cols;
		}

		/* deal with pixels within half filter 
		   width of top edge of image */

		for (line=lines-halfwidth-1; line<lines; line++) {
			*vfptr=max;
			vfptr+=cols;
		
			/* update rack counts */
			rack[*oldfptr]--;

			/* check if oldval was the max
		   	   and if so find new max */
			while (rack[max] == 0)
				max--;

			oldfptr+=cols;
		}
	}

	/* vframe now contains vertical max */
	return(vframe);
}
//--------------------------------------------------------------------------------------

static FSCHAR *fsMinVertical(FSCHAR *frame, int cols, int lines, int halfwidth)
{
	register FSCHAR *fptr, *oldfptr, *vfptr;
	register int col,line;
	FSCHAR min;
	FSCHAR *vframe;
	int size, fw;
	short rack[256];
	
	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	size=lines*cols;

	/* allocate result frame */
	vframe=(FSCHAR *)Malloc(size);

	if (vframe==NULL) {
		fprintf(stderr,"\nfsmin: could not allocate new frame\n");
		return(NULL);
	}


	/* perform vertical min on frame and save in vframe */

	for (col=0; col<cols; col++) {	/* for each image column */

		/* clear rack */
		memset(rack,0,sizeof(rack[0])*256);

		min=255;
		oldfptr = fptr = frame+col;
		vfptr = vframe+col;


		/* put first halfwidth+1 points into rack and find min */
		for (line=0; line<halfwidth+1; line++) {
			if (*fptr<min)
				min=*fptr;

			rack[*fptr]++;
	
			fptr+=cols;
		}

		/* deal with pixels within half filter 
		   width of bottom edge of image */

		for (line=0; line<halfwidth; line++) {
			/* store min in vframe */
			*vfptr=min;
			vfptr+=cols;

			/* update rack counts */
			rack[*fptr]++;

			/* check if newval is the min */
			if (*fptr<min)
				min=*fptr;

			fptr+=cols;
		}

		/* deal with pixels in main body of image */

		for (line=halfwidth; line<lines-halfwidth-1; line++) {

			/* store min in vframe */
			*vfptr=min;
			vfptr+=cols;
		
			/* remove oldval from rack, insert newval
		   	   and determine new min value */

			if (*oldfptr!=*fptr) {

				/* update rack counts */
				rack[*fptr]++;
				rack[*oldfptr]--;

				/* check if newval is the min */
				if (*fptr<min)
					min=*fptr;

				/* check if oldval was the min
				   and if so find new min */
				while (rack[min] == 0)
					min++;
			}

			fptr+=cols;
			oldfptr+=cols;
		}

		/* deal with pixels within half filter 
		   width of top edge of image */

		for (line=lines-halfwidth-1; line<lines; line++) {
			*vfptr=min;
			vfptr+=cols;
		
			/* update rack counts */
			rack[*oldfptr]--;

			/* check if oldval was the min
		   	   and if so find new min */
			while (rack[min] == 0)
				min++;

			oldfptr+=cols;
		}
	}


	/* vframe now contains vertical dimensional min */
	return(vframe);
}
//--------------------------------------------------------------------------------------

static FSCHAR *fsOpenVertical(FSCHAR *frame, int cols, int lines, int halfwidth)
{
	int fw;
	FSCHAR *f1=NULL;
	FSCHAR *f2=NULL;

	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	/* Erode image */
	f1=fsMinVertical(frame,cols,lines,halfwidth);
	
	if (f1!=NULL) {

		/* Dilate image */
		f2=fsMaxVertical(f1,cols,lines,halfwidth);
		Free(f1);
	}

	return(f2);
}
//--------------------------------------------------------------------------------------
static FSCHAR *fsCloseVertical(FSCHAR *frame, int cols, int lines, int halfwidth)
{
	int fw;
	FSCHAR *f1=NULL;
	FSCHAR *f2=NULL;

	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	/* Dilate image */
	f1=fsMaxVertical(frame,cols,lines,halfwidth);
	
	if (f1!=NULL) {

		/* Erode image */
		f2=fsMinVertical(f1,cols,lines,halfwidth);
		Free(f1);
	}

	return(f2);
}
//--------------------------------------------------------------------------------------
static FSCHAR *fsExtractVertical(FSCHAR *frame, int cols, int lines, int halfwidth, int noise_halfwidth)
{
	register int size;
	FSCHAR register *imptr, *im2ptr, min/*, max*/;
	FSCHAR *f1=NULL;
	FSCHAR *f2=NULL;
	int fw;

	// 2 * halfwidth must be < cols or lines
	fw = (halfwidth * 2) + 2;
	if ((fw > cols) || (fw > lines))
		return NULL;

	size=lines*cols;

	/* Close image */
	f1=fsCloseVertical(frame,cols,lines,noise_halfwidth);

	if (f1!=NULL) {

		/* Open resultant image */
		f2=fsOpenVertical(f1,cols,lines,halfwidth);

		/* free intermediate image */
		Free(f1);

		if (f2!=NULL) {

			imptr=frame;
			im2ptr=f2;
			do {
				/* find min of original and processed images */
				min=*imptr < *im2ptr ? *imptr : *im2ptr;

				/* subtract min from original image */
				*im2ptr++=*imptr++ - min;

			} while (--size);
		}
	}

	return(f2);
}
//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
// Hough transform operations
//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
// Returns dimensions of hough image corresponding to source image width and hieght
// hough image width is always 3600


int getHoughHeight(int width, int height)
{
	int maxdim = max(height,width);
	return (int)(maxdim*2.0f*sqrt(2.0f));
}

//--------------------------------------------------------------------------------------
//
//	Performs Hough Transform on image and returns 16bit accumulator image
//	Generates a transform image that is 3600 wide but only the data
//	between minAngle and maxAngle is generated - at every 10th of a degree

WORD *HoughTransform(FSCHAR *image, int width, int height, int minAngle, int maxAngle)
{
 	if (!image)
		return NULL;

	minAngle*=10;
	maxAngle*=10;

	int maxdim = max(height,width);
	int houghHeight = (int)(maxdim*2.0f*sqrt(2.0f));
	int centre = (int)(maxdim*sqrt(2.0f));

	// create an image that is twice the largest dimension of the original image multiplied by sin(45)=cos(45)
	// this will be the r dmension and handles worse case range theta = 46 and 225, r=-sqrt(2)*maxD, +sqrt(2)*maxD
	WORD *dim = (WORD *)calloc(3600 * houghHeight, sizeof(WORD)); // NOTE 16bit accumulator

	if (!dim)
		return NULL;

 	// generate LUT
	double theta=minAngle;
	double sintheta[3600];
	double costheta[3600];

	for (int k=minAngle; k<maxAngle; k++)
	{
		costheta[k] = cos(k*PI/1800.0f);
		sintheta[k] = sin(k*PI/1800.0f);
	}

	BYTE *fptr = image;
	// dptr points to destination image data
	WORD *dptr=(WORD *)dim;

	for (int y=0; y<height; y++) 
	{	
		for (int x=0; x<width; x++) 
		{
			// if we have a non zero pixel in the original image update accumulator
			if (*fptr>0)
			{
				for (int k=minAngle; k<maxAngle;k++)
				{
					int r = centre + (int)(double(x) * costheta[k] + double(y) * sintheta[k]);	

					if ((r>=0) && (r<houghHeight))
					{
						WORD *cptr=dptr+k+r*3600;
						if (*cptr<65535)
							*cptr += 1;
					}
				}
			}
			fptr++;
		}
	}

	// return transformed image
	return dim;
}

//--------------------------------------------------------------------------------------
//
//	Performs Hough Transform on image and returns 8bit accumulator image
//	Generates a transform image that is 3600 wide but only the data
//	between minAngle and maxAngle is generated - at every 10th of a degree

FSCHAR *HoughTransform8bit(FSCHAR *image, int width, int height, int minAngle, int maxAngle)
{
 	if (!image)
		return NULL;

	minAngle; //*=10;
	maxAngle; //*=10;

	int maxdim = max(height,width);
	int houghHeight = (int)(maxdim*2.0f*sqrt(2.0f));
	int centre = (int)(maxdim*sqrt(2.0f));

	// create an image that is twice the largest dimension of the original image multiplied by sin(45)=cos(45)
	// this will be the r dmension and handles worse case range theta = 46 and 225, r=-sqrt(2)*maxD, +sqrt(2)*maxD
	FSCHAR *dim = (FSCHAR *)calloc(360 * houghHeight, sizeof(FSCHAR)); // NOTE 8bit accumulator

	if (!dim)
		return NULL;

 	// generate LUT
	double theta=minAngle;
	double sintheta[360]; // 3600
	double costheta[360];

	for (int k=minAngle; k<maxAngle; k++)
	{
		costheta[k] = cos(k*PI/180.0f); // 1800
		sintheta[k] = sin(k*PI/180.0f);
	}

	BYTE *fptr = image;
	// dptr points to destination image data
	BYTE *dptr=(BYTE *)dim;

	for (int y=0; y<height; y++) 
	{	
		for (int x=0; x<width; x++) 
		{
			// if we have a non zero pixel in the original image update accumulator
			if (*fptr>0)
			{
				for (int k=minAngle; k<maxAngle;k++)
				{
					int r = centre + (int)(double(x) * costheta[k] + double(y) * sintheta[k]);	
					if ((r>=0) && (r<houghHeight))
					{
						BYTE *cptr=dptr+k+r*360; //3600 
						if (*cptr<255)
							*cptr = *cptr +1;
					}
				}
			}
			fptr++;
		}
	}

	// return transformed image
	return dim;
}

//--------------------------------------------------------------------------------------
// Detect circular coverlsip in an image
// Clear area outside of coverlsip region
// Return coverslip area as Woolz object
// Return TRUE if it finds coverlsip, FALSE otherwise

BOOL DetectCircularCoverslip(FSCHAR *image, int width, int height, int radius, struct object* &coverslipObj, int &cx, int &cy)
{
	BOOL found = FALSE;
	coverslipObj = NULL;

	if (!image)
		return FALSE;

	// perfrom vertical background subtraction to remove vertical slide edges - which may perturb the Hough algorithm
	FSCHAR *lineim = fsExtractVertical(image, width, height,10,1);

	// Erode image to remove edges
	FSCHAR *edgeim = fsMin(lineim, width, height, 3);

	// Subtract from original to reveal edges
	FSCHAR *eimptr = edgeim;
	FSCHAR *imptr = lineim;
	int size = width*height;
	while (size>0)
	{
		*eimptr = *imptr - *eimptr;
		eimptr++;
		imptr++;
		size--;
	}
	free(lineim);

	// perform background subtraction to find thin lines
	FSCHAR *bgsubim = fsExtract(edgeim, width, height, 5, 1);
	free(edgeim);

	// dilate thin lines to strengthen edges
	FSCHAR *circim = fsMax(bgsubim , width, height, 1);
	free(bgsubim);


	// threshold above noise level of 16
	FSCHAR *cim = circim;
	size = width*height;
	while (size>0)
	{
		if (*cim < 16)
			*cim++ = 0;
		else
			*cim++ = 255;
		size--;
	}

	// remove small objects from image
	int n;
  	struct object *bigobj, *objlist[5000];
	bigobj=(struct object *)fsconstruct(circim,height,width,1,0);

    	for (int i=0;i<5000;i++)
    		objlist[i]=NULL;

    	//minimum area of 10, max no objects is 5000
	label_area(bigobj,&n,objlist,5000,10);									
    	freeobj(bigobj);

	// clear image
	cim = circim;
	size = width*height;
	while (size>0)
	{
		*cim++ = 0;
		size--;
	}

	// redraw large objects
    	for (int i=0;i<n;i++)
	{
    		vdom21to1(objlist[i]);
		clear_values(objlist[i], circim, width, height,255);
    		freeobj(objlist[i]);
	}

	// perform Hough transform on image
	FSCHAR *hufim = CircularHoughTransform(circim, width, height, radius);
	free(circim);

	if (!hufim)
		return FALSE;

	// now search for brightest peak - this will be our circle
	int max = 0;

	for (int y=0; y< height; y++)
	{
		for (int x=0; x< width; x++)
		{
			FSCHAR *imptr= hufim + x + y * width;
			if (*imptr > max)
			{
				max = *imptr;
				cx = x;
				cy = y;
			}
		}
	}

	free(hufim);

	// check centre against threshold
	if (max < 64)
		return FALSE;


	// generate object for coverslip - reduce area by reducing radius
	coverslipObj = createCoverslipObject(cx, cy, radius-15, height, 0, 359);


	// clear image
	imptr=image;
	size = width*height;
	while (size>0)
	{
		*imptr++=0;
		size--;
	}

	// set pixels in image under coverslip to 255 - clear_values() handles pixels outside of image
	clear_values(coverslipObj, image, width, height,255);

	return TRUE;
}


//--------------------------------------------------------------------------------------

FSCHAR *CircularHoughTransform(FSCHAR *image, int width, int height, int radius)
{
 	if (!image)
		return NULL;

	FSCHAR *dim = (FSCHAR *)calloc(width*height, sizeof(FSCHAR)); // NOTE 16bit accumulator

	if (radius < 0)
		radius = - radius;

	// generate LUTs
	double rcos[360];
	double rsin[360];
	double dradius = (double) radius;

	for (int angle=0; angle < 360; angle++)
	{
		double dangle = (double)angle * PI /180.0f;
		rcos[angle] = dradius * cos(dangle);
		rsin[angle] = dradius * sin(dangle);
	}

	BYTE *imptr=NULL;

	for (int y=0;y<height; y++)
	{
		for (int x=0; x<width; x++)
		{
			imptr = image + x + y * width;

			if (*imptr == 0)
				continue;

			// for each point in image look for matching triples of points on a circlular arc
			// if they exist the hough space image accumualtor is incremented
			for (int angle=0; angle < 360; angle++)
			{
				// calculate circle centre for this edge position based on radius
				int xh = x + rcos[angle];
				int yh = y + rsin[angle];

				// check for centre off image
				if (xh<0 || xh>width-1 || yh<0 || yh>height-1)
					continue;

				int acc = 0;

				// now for this centre position
				// look back Xdeg and forward Xdeg  for another point on the circle
				int revangle = angle - 180;
				if (revangle < 0)
					revangle += 360;

				for (int i=-2;i<3;i++)
				{
					if (i==0)
						continue;

					// now check two other points moved on by X degrees
					int angle2=revangle + i*10;
					if (angle2<0) angle2 += 360;
					if (angle2>360) angle2 -=360;

					int xr = xh + rcos[angle2];
					int yr = yh + rsin[angle2];
					if (xr<0 || xr>width-1 || yr<0 || yr>height-1)
						continue;
					imptr = image + xr + yr * width;

					if (*imptr>0)
						acc++;
				}

				// increment the hough image pixel at this centre
				imptr = dim + xh + yh * width;
				if (*imptr < 255-acc)
					*imptr = *imptr + acc;
			} // end angle loop
		} // end x loop
	} // end y loop

	return dim;
}
//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
// Coverslip detection operations
//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
// Create coverlsip object

struct object *createCoverslipObject(int cx, int cy, int radius, int imageHeight, int angle1, int angle2)
{
	if (angle2<angle1)
		angle2+=360;

	int nvtx=angle2-angle1+2;

	// generate LUTs
	double rcos[360];
	double rsin[360];
	int angle,i;

	for (angle=0; angle < 360; angle++)
	{
		double dradius = (double)radius;
		double dangle = (double)angle * PI /180.0f;
		rcos[angle] = dradius * cos(dangle);
		rsin[angle] = dradius * sin(dangle);
	}

	// start by creating polygon represnting arc of coverslip
	struct polygondomain *pdom = makepolydmn(1,NULL,nvtx,nvtx,1);

	for (angle=angle1,i=0;angle<=angle2;angle++,i++)
	{
		int ang = angle>=360?angle-360:angle;
		pdom->vtx[i].vtX = cx + rcos[ang];
		pdom->vtx[i].vtY = imageHeight - cy - rsin[ang];
	}
	pdom->vtx[nvtx-1].vtX = pdom->vtx[0].vtX;
	pdom->vtx[nvtx-1].vtY = pdom->vtx[0].vtY;

	// convert arc poly into a solid blob
	struct object *obj=(struct object *)polytoblob(pdom);
	freepolydmn(pdom); // surplus to requirements, so lose it

	struct valuetable *newvdom =  (struct valuetable *)newvaluetb(obj, 1, 0);
	newvdom->linkcount=1;
	obj->vdom = newvdom;

	// set all values in vdom to 255
	setval(obj,255);

	return obj;
}

//--------------------------------------------------------------------------------------
// Finds semi circular coverslips in slide image - assumes coverslip has a known radius
// returns whether one or two coverslips were found and their centres
// Assumes centres must be at least X pixels aprt in y direction
//

BOOL FindArcs(FSCHAR *image, int width, int height, int cx, int cy, int radius, int minArcAngle, int &angle1, int &angle2)
{
	angle1=angle2=-1;

	if (!image)
		return FALSE;

	// generate LUTs
	double rcos[360][21];
	double rsin[360][21];
	int angle,i;

	for (angle=0; angle < 360; angle++)
	{
		for (i=0;i<21;i++)
		{
			double dradius = (double)(radius+i-10);
			double dangle = (double)angle * PI /180.0f;
			rcos[angle][i] = dradius * cos(dangle);
			rsin[angle][i] = dradius * sin(dangle);
		}
	}

	// search for non-zero pixels on or close to the circumfernece of circle in the binary edge image
	int hist[360];
	int dist[360];
	for (angle=0;angle<360;angle++)
	{
		hist[angle]=0;
		dist[angle]=99;
		for (i=0;i<21;i++)
		{
			int x = cx + rcos[angle][i];
			int y = cy + rsin[angle][i];
			if (x>0 && x<width && y>0 && y<height)
			{
				FSCHAR *imptr = image + x + y * width;

				// choose point closest to actual circumference
				if ((*imptr > 0) && (abs(i-10) < abs(dist[angle])))
				{
					hist[angle] = 1;
					dist[angle]=i-10;	
				}
			}
		}
	}

	// now remove any points where the dist is not close between pairs of points
	for (angle=1;angle<359;angle++)
	{
		if ((hist[angle]==1) && (hist[angle-1]==1) && (hist[angle+1]==1))
		{
			if (abs(dist[angle+1]-dist[angle-1])> 3)
				hist[angle]=0;
		}
	}


	// determine which orientation the coverlsip is in
	int sumleft = 0;
	int sumright = 0;
	for (angle=0;angle<360;angle++)
	{
		if (angle>=90 && angle<=270)
			sumleft+=hist[angle];
		else
			sumright+=hist[angle];
	}


	// starting off imaage find the first long arc of non zero pixels
	if (sumleft < sumright)
	{
		// look for bottom edge
		int start = -1;
		int end = -1;
		for (angle = 180; angle < 360; angle++)
		{
			if (hist[angle]==1)
			{
				if (start<0)
					start = angle;
				end = angle;
			}
			else
			{
				if (end-start > minArcAngle)
					break;
	
				start = -1;
				end = -1;
			}
		}

		if (end-start > minArcAngle)
			angle1 = start;

		// look for top edge
		start = -1;
		end = -1;
		for (angle = 180; angle > 0; angle--)
		{
			if (hist[angle]==1)
			{
				if (start<0)
					start = angle;
				end = angle;
			}
			else
			{
				if (start - end > minArcAngle)
					break;
				start = -1;
				end = -1;
			}
		}

		if (start - end > minArcAngle)
			angle2 = start;

	}
	else
	{
		// look for bottom edge
		int start = -1;
		int end = -1;
		for (angle = 0; angle < 180; angle++)
		{
			if (hist[angle]==1)
			{
				if (start<0)
					start = angle;
				end = angle;
			}
			else
			{
				if (end-start > minArcAngle)
					break;
	
				start = -1;
				end = -1;
			}
		}

		if (end-start > minArcAngle)
			angle1 = start;

		// look for top edge
		start = -1;
		end = -1;
		for (angle = 359; angle > 180; angle--)
		{
			if (hist[angle]==1)
			{
				if (start<0)
					start = angle;
				end = angle;
			}
			else
			{
				if (start - end > minArcAngle)
					break;
				start = -1;
				end = -1;
			}
		}

		if (start - end > minArcAngle)
			angle2 = start;
	}

	if (angle1 > 0 && angle2>0)
	{
#ifdef DRAW_RESULTS
		// draw line between endpoints 
		// construct line
		int xa = cx + rcos[angle1][6];
		int ya = cy + rsin[angle1][6];
		int xb = cx + rcos[angle2][6];
		int yb = cy + rsin[angle2][6];

		// handle ddgs inversion and canvas origin
		ya=height-ya;
		yb=height-yb;
		int y=ya<yb?ya:yb;
		int x=xa<xb?xa:xb;

		struct ivector *ivec=(struct ivector *)malloc(sizeof(struct ivector));
		ivec->k1 = xa; ivec->l1 = ya;
		ivec->k2 = xb; ivec->l2 = yb;
		ivec->type = 30;
		Canvas *linecan = create_object_Canvas(Cboundary, x*8, y*8, (struct object*)ivec, MARKIM, BLUE, COPY);
		paste_Canvas(linecan,topcan);
#endif
		return TRUE;
	}

	return FALSE; 
}
//--------------------------------------------------------------------------------------
// Detect sem-circular coverlsips in an image
// Clear area outside of coverlsip region
// Return coverslip areas as Woolz objects
// Return TRUE if it finds one or two coverlsips, FALSE if none

BOOL DetectCircularCoverslip(FSCHAR *image, int width, int height, int radius, struct object* &coverslipObj1, struct object* &coverslipObj2, int &cx1, int &cx2, int &cy1, int &cy2)
{
	BOOL found1 = FALSE;
	BOOL found2 = FALSE;
	coverslipObj1 = NULL;
	coverslipObj2 = NULL;

	if (!image)
		return FALSE;

	// perfrom vertical background subtraction to remove vertical slide edges - which may perturb the Hough algorithm
	FSCHAR *lineim = fsExtractVertical(image, width, height,10,1);

	// Erode image to remove edges
	FSCHAR *edgeim = fsMin(lineim, width, height, 3);

	// Subtract from original to reveal edges
	FSCHAR *eimptr = edgeim;
	FSCHAR *imptr = lineim;
	int size = width*height;
	while (size>0)
	{
		*eimptr = *imptr - *eimptr;
		eimptr++;
		imptr++;
		size--;
	}
	free(lineim);

	// perform background subtraction to find thin lines
	FSCHAR *bgsubim = fsExtract(edgeim, width, height, 5, 1);
	free(edgeim);

	// threshold above noise level of 16
	FSCHAR *bim = bgsubim;
	size = width*height;
	while (size>0)
	{
		if (*bim < 16)
			*bim++ = 0;
		else
			*bim++ = 255;
		size--;
	}

	// remove small objects from image
	int n;
  	struct object *bigobj, *objlist[5000];
	bigobj=(struct object *)fsconstruct(bgsubim,height,width,1,0);

    for (int i=0;i<5000;i++)
    	objlist[i]=NULL;

    //minimum area of 10, max no objects is 5000
	label_area(bigobj,&n,objlist,5000,10);									
    freeobj(bigobj);

	// clear image
	bim = bgsubim;
	size = width*height;
	while (size>0)
	{
		*bim++ = 0;
		size--;
	}

	// redraw large objects
    for (int i=0;i<n;i++)
	{
    	vdom21to1(objlist[i]);
		clear_values(objlist[i], bgsubim, width, height,255);
    	freeobj(objlist[i]);
	}

	// perform Hough transform on image
	FSCHAR *hufim = CircularHoughTransform(bgsubim, width, height, radius);

	if (!hufim)
		return FALSE;

	// now search for brightest peak - this will be one of our circles
	int max = 0;

	for (int y=0; y< height; y++)
	{
		for (int x=0; x< width; x++)
		{
			FSCHAR *imptr= hufim + x + y * width;
			if (*imptr > max)
			{
				max = *imptr;
				cx1 = x;
				cy1 = y;
			}
		}
	}

	// check first centre against threshold
	if (max > 64)
		found1=TRUE;
	else
	{	
		free(hufim);
		return FALSE;
	}

	// now look for second brightest peak - which must be a reasonable distance from the first
	max = 0;
	for (int y=0; y< height; y++)
	{
		for (int x=0; x< width; x++)
		{
			FSCHAR *imptr= hufim + x + y * width;
			if ((*imptr > max) && (abs(y-cy1) > 100))
			{
				max = *imptr;
				cx2 = x;
				cy2 = y;
			}
		}
	}

	// check second centre against threshold
	if (max > 64)
		found2=TRUE;

	free(hufim);

	int angle1=-1;
	int angle2=-1;
	int minArcAngle = 3;

	// find arc end points in first coverslip 
	if (found1)
		if (FindArcs(bgsubim, width, height, cx1, cy1, radius, minArcAngle, angle1, angle2))
		{
			// generate object for first coverslip - reduce area by reducing radius
			if (angle2<angle1)
				angle2+=360;

			coverslipObj1 = createCoverslipObject(cx1, cy1, radius-15, height, angle1, angle2);

			// fill coverslip vdom with data from original image
//			fill_values(coverslipObj1, image, width, height);

		}
		else 
			found1 = FALSE;

    // find arc end points in second coverslip 
	if (found2)
		if (FindArcs(bgsubim, width, height, cx2, cy2, radius, minArcAngle, angle1, angle2))
		{
			if (angle2<angle1)
				angle2+=360;

			// generate object for second coverslip
			coverslipObj2 = createCoverslipObject(cx2, cy2, radius-15, height, angle1, angle2);

			// fill coverslip vdom with data from original image
//			fill_values(coverslipObj2, image, width, height);
		}
		else
			found2 = FALSE;

	if (found1 || found2)
	{
		// clear image
		imptr=image;
		size = width*height;
		while (size>0)
		{
			*imptr++=0;
			size--;
		}
	}

	// set pixels in image under coverslips to 255
	if (found1)
		clear_values(coverslipObj1, image, width, height,255);
	if (found2)
		clear_values(coverslipObj2, image, width, height,255);

	free(bgsubim);

	if (found1 || found2)
		return TRUE;
	else
		return FALSE;
}

void DumpTheData(LPCTSTR FileName, BYTE *Image, int Width, int Height)
{
			CFile BlbFile;
			CFileException FE;

			if (BlbFile.Open(FileName, CFile::modeCreate | CFile::modeWrite, &FE))
			{
				BlbFile.Write(Image, Width * Height);
				BlbFile.Close();
			}
}				

//--------------------------------------------------------------------------------------
// Finds coverslip in slide image - assumes coverslip is around 400 pixels high and wide
// Returns top and bottom y and left and right x positions of coverslip in inage (slightly inside)
// and clears data outside of coverslip from source image
// CHecks are in place if top, bottom, left or right edges are not visible

BOOL DetectCoverslip(FSCHAR *image, int width, int height, int &bottom, int &top, int &left, int &right)
{
	// initialise coverslip limits to scan area
	bottom = 0;
	top = height-1;
	left = 0;
	right = width-1;

	if (!image)
		return FALSE;

		//DumpTheData(_T("D:\\AII_Shared\\Scripts\\SPOT\\Original.raw"), image, width, height);

	// Erode image to remove edges
	FSCHAR *edgeim = fsMin(image, width, height, 3);

	// Subtract from original to reveal edges
	FSCHAR *eimptr = edgeim;
	FSCHAR *imptr = image;
	int size = width*height;
	while (size>0)
	{
		*eimptr = *imptr - *eimptr;
		eimptr++;
		imptr++;
		size--;
	}

	// perform background subtration to find thin lines
	// MG 160610 changed params to 9,2 for working with LBP
	FSCHAR *bgsubim = fsExtract(edgeim, width, height, 9, 2);
	free(edgeim);


	// MG 160610 perform LBP to identify structures in image - radius=1, noise threshold = 16
	// Use of LBP helps eliminate cells and colonies from the image before extracting lines
	BYTE *LBPim = LBP(bgsubim, width, height, 1, 16);
	free (bgsubim);


	// MG 160610 threshold LBP - ignoring values below 3 and above 8 i.e. ignore certain structures
	size = width*height;
	BYTE *L;

	L = LBPim;
	while (size>0)
	{
		if ((*L < 3) || (*L > 8))
			*L++ = 0;
		else
			*L++ = 255;
		size--;
	}

	//DumpTheData(_T("D:\\AII_Shared\\Scripts\\SPOT\\LBP.raw"), LBPim, width, height);

	// MG 160610 close image to merge thin lines
	FSCHAR *closedim = fsClose(LBPim, width, height, 3);
	delete LBPim;

	//DumpTheData(_T("D:\\AII_Shared\\Scripts\\SPOT\\Closed.raw"), closedim, width, height);

	// perform Hough transform over the range 80 - 100 degrees
	WORD *houghim = HoughTransform(closedim, width, height, 80, 100);


    	// fill image before removing data outside of coverslip
    	imptr = image;
	size = width*height;
	while (size>0)
	{
		*imptr++ = 255;
		size--;
	}

	if (!houghim)
	{
		free(closedim);
		return FALSE;
	}

	//============ HORIZONTAL EDGE DETECTION STARTS HERE =============

	BOOL bFoundBottom = FALSE;
	BOOL bFoundTop = FALSE;
	BOOL bFoundLeft = FALSE;
	BOOL bFoundRight = FALSE;

	int maxdim = max(height,width);
	int houghHeight = (int)(maxdim*2.0f*sqrt(2.0f));
	int centre = (int)(maxdim*sqrt(2.0f));

	// search houghim around 80-100 degrees for brightest peak in 1/10 degree intervals
	// looking for bottom edge - start from middle and work out to avoid picking up label edge
	int max = 0;
	int maxr = 0;
	int maxangle = 800;
	for (int r=centre + height/2; r>0; r--)
	{
		for (int angle = 800; angle < 1000; angle++)
		{
			WORD *phdata = houghim + angle + r*3600;

			if (*phdata > max)
			{
				max = *phdata;
				maxr = r;
				maxangle = angle;
				// breakout if we already meet our criteria for a an edge
				if (max > 400)
					break;
			}
		}
	
		if (max > 400)
			break;
	}

	// only consider horizontal lines which are at least 70% of scan area width 
	// or 70% of average coverslip width (400), whichever is the smaller
	int minwidth;
	if (width < 400)
		minwidth = width * 70/100;
	else
		minwidth = 400 * 70/100;

	/*CString dbg;
	dbg.Format(_T("minwidth %d\r\n"), minwidth);
	OutputDebugString(dbg);
	dbg.Format(_T("MAX %d\r\n"), max);
	OutputDebugString(dbg);*/

	if (max > minwidth)
	{
		// x=0
		bottom = (int)((double)(maxr - centre)/sin(maxangle*PI/1800.0f));

		// x=width
		int bottom2 = (int)(((double)(maxr - centre) - (double)width*cos(maxangle*PI/1800.0f))/sin(maxangle*PI/1800.0f));

#ifdef DRAW_RESULTS
		struct ivector *ivec=(struct ivector *)malloc(sizeof(struct ivector));
		ivec->k1 = 0; ivec->l1 = bottom2;
		ivec->k2 = width; ivec->l2 = bottom;
		ivec->type = 30;
		int y=(height - bottom)*8;
		if (bottom<bottom2)
			y=(height - bottom2)*8;
		Canvas *linecan = create_object_Canvas(Cboundary, 0, y, (struct object*)ivec, MARKIM, RED, COPY);
		paste_Canvas(linecan,topcan);
#endif
		if (height - bottom > bottom) // check we are on the bottom of the slide
		{
			if (bottom < bottom2)	// we are so get the topmost bottom i.e inner edge
				bottom = bottom2;
		}
		else
		{
			if (bottom > bottom2)	// we are not so get the bottommost bottom
				bottom = bottom2;
		}

		bFoundBottom = TRUE;
	}

	// now search for second brightest peak 
	// looking for top edge - start from middle and work out to avoid picking up label edge (rotated slide)
	int max2 = 0;
	int maxr2 = 0;
	maxangle = 800;
	for (int r=centre + height/2; r<houghHeight; r++)
	{
		for (int angle = 800; angle<1000; angle++)
		{
			WORD *phdata = houghim + angle +r*3600;

			if (*phdata > max2)
			{
				max2 = *phdata;
				maxr2 = r;
				maxangle = angle;
				if (max2 > 400)
					break;
			}
		}

		if (max2 > 400)
			break;
	}

	// second peak must be at least 70% of first peak to be considered
	if (max2 > minwidth)
	{
		// x=0
		top = (int)((double)(maxr2 - centre)/sin(maxangle*PI/1800.0f));

		// x=width
		int top2 = (int)(((double)(maxr2 - centre) - (double)width*cos(maxangle*PI/1800.0f))/sin(maxangle*PI/1800.0f));

#ifdef DRAW_RESULTS
		struct ivector *ivec=(struct ivector *)malloc(sizeof(struct ivector));
		ivec->k1 = 0; ivec->l1 = top2;
		ivec->k2 = width; ivec->l2 = top;
		ivec->type = 30;
		int y=(height - top)*8;
		if (top<top2)
			y=(height - top2)*8;
		Canvas *linecan = create_object_Canvas(Cboundary, 0, y, (struct object*)ivec, MARKIM, RED, COPY);
		paste_Canvas(linecan,topcan);
#endif

		// we want the inner edge
		if (top > bottom)
		{
			if (top > top2)
				top = top2;
		}
		else
		{
			if (top < top2)
				top = top2;
		}

		bFoundTop = TRUE;

		// make sure top is higher than bottom
		if (top < bottom)
		{
			int temp = bottom;
			bottom = top;
			top = temp;
		}
	}

	if (bFoundBottom && !bFoundTop)
	{
		// only one edge found check which end of slide
		if (height - bottom < bottom)
		{
			top = bottom;
			bottom = 0;
			bFoundTop = TRUE;
			bFoundBottom = FALSE;
		}
	}

	if (bFoundBottom && bFoundTop)
	{
		// both edges found - check they are at least 400 apart
		if (top - bottom < 300)
		{
			// no they are not - best to just reset 
			bottom = 0;
			top = height-1;
		}
	}

	// final check for out of range values
	if (bottom <0) bottom = 0;
	if (top>height-1) top = height-1;

#ifndef DRAW_RESULTS
	if (bFoundBottom)
	{
		// now move bottom up a tad to miss edge of coverslip
		bottom += 10;
		
		// clear data outside of coverslip in source image
		FSCHAR *pdata = image;

		for (int x=0; x<width*bottom; x++)
			*pdata ++ = 0;
	}

	if (bFoundTop)
	{
		// and move top down a tad
		top -= 10;
		FSCHAR *pdata = image + top * width;

		for (int x=0; x < width * (height - top);x++)
			*pdata ++ = 0;
	}
#endif

	//========= VERTICAL EDGE DETECTION STARTS HERE =============

	// perform Hough transform over the range 170 - 190 degrees
	free(houghim);
	houghim = HoughTransform(closedim, width, height, 170, 190);
	free(closedim);

	// now search for vertical edges around 180 in 1/10 degree intervals
	// look for left edge - start from the midlle of the image and work out
	// so we can pick up first strong coverlsip edge and ignore edge of slide
	int maxv = 0;
	int maxvr = 0;
	maxangle = 1700;

	for (int r=centre-width/2; r<centre; r++)
	{
		for (int angle = 1700; angle<1900; angle++)
		{
			WORD *phdata = houghim + angle + r*3600;

			if (*phdata > maxv) 
			{
				maxv = *phdata;
				maxvr = r;
				maxangle = angle;

				// breakout if we already meet our criteria for a an edge
				if (maxv > 400)
					break;

			}
		}

		if (maxv > 400)
				break;
	}

	//dbg.Format(_T("MAXV %d\r\n"), maxv);
	//OutputDebugString(dbg);

	// only consider vertical lines which are at least 70% of typical coverslip height (400)
	if (maxv > 400 * 70/100)
	{
		// y=0
		left = (int)((double)(maxvr - centre)/cos(maxangle*PI/1800.0f));

		// y=height
		int left2 = (int)(((double)(maxvr - centre) - (double)height*sin(maxangle*PI/1800.0f))/cos(maxangle*PI/1800.0f));

#ifdef DRAW_RESULTS
		struct ivector *ivec=(struct ivector *)malloc(sizeof(struct ivector));
		ivec->k1 = left; ivec->l1 = height-1;
		ivec->k2 = left2; ivec->l2 = 0;
		ivec->type = 30;
		int x = left*8;
		if (left2 < left)
			x = left2*8;
		Canvas *linecan = create_object_Canvas(Cboundary, x, 0, (struct object*)ivec, MARKIM, GREEN, COPY);
		paste_Canvas(linecan,topcan);
#endif
		if (width - left > left) // check we are on the left of the slide
		{
			if (left < left2)	// we are so get the rightmost left i.e inner edge
				left = left2;
		}
		else
		{
			if (left > left2)	// we are not so get the leftmost left
				left = left2;
		}

		bFoundLeft = TRUE;
	}


	int maxv2 = 0;
	int maxvr2 = 0;
	maxangle = 1700;

	// look for right edge - start from the midlle of the image and work out
	// so we can pick up first strong coverlsip edge and ignore edge of slide
	for (int r=centre-width/2; r>0; r--)
	{
		for (int angle = 1700; angle < 1900; angle++)
		{
			WORD *phdata = houghim + angle + r*3600;

			if (*phdata > maxv2) 
			{
				maxv2 = *phdata;
				maxvr2 = r;
				maxangle = angle;

				// breakout if we already meet our criteria for a an edge
				if (maxv2 > 400)
					break;
			}
		}

		if (maxv2 > 400)
			break;
	}

	// second peak must be at least 70% of first peak to be considered
	if (maxv2 > 400 * 70 / 100)
	{
		// y=0
		right = (int)((double)(maxvr2 - centre)/cos(maxangle*PI/1800.0f));

		// y=height
		int right2 = (int)(((double)(maxvr2 - centre) - (double)height*sin(maxangle*PI/1800.0f))/cos(maxangle*PI/1800.0f));

#ifdef DRAW_RESULTS
		struct ivector *ivec=(struct ivector *)malloc(sizeof(struct ivector));
		ivec->k1 = right; ivec->l1 = height-1;
		ivec->k2 = right2; ivec->l2 = 0;
		int x = right*8;
		if (right2<right)
			x = right2*8;
		ivec->type = 30;
		Canvas *linecan = create_object_Canvas(Cboundary, x, 0, (struct object*)ivec, MARKIM, GREEN, COPY);
		paste_Canvas(linecan,topcan);
#endif
		// we want the inner edge
		if (right > left)
		{
			if (right > right2)
				right = right2;

		}
		else
		{
			if (right < right2)
				right = right2;
		}


		bFoundRight = TRUE;

		// make sure right is greater than left
		if (right < left)
		{
			int temp = left;
			left = right;
			right = temp;
		}
	}


	if (bFoundLeft && !bFoundRight)
	{
		// only one edge found check which side of slide
		if (width - left < left)
		{
			right = left;
			left = 0;
			bFoundRight = TRUE;
			bFoundLeft = FALSE;
		}
	}

	if (bFoundLeft && bFoundRight)
	{
		// both edges found - check they are at least 400 apart
		if (right - left < 300)
		{
			// no they are not - best just reset
			left = 0;
			right = width-1;
		}
	}

	// final out of range check
	if (left<0) left = 0;
	if (right>width-1) right = width-1;

#ifndef DRAW_RESULTS
	// clear data outside of coverslip in source image
	FSCHAR *pdata = image;

	if (bFoundLeft)
	{
		// move left in a tad to miss edge of coverslip
		left += 10;
		for (int y=0; y<height; y++)
		{
			pdata=image + y*width;
			for (int x=0; x<left; x++)
				*pdata++ = 0;
		}
	}

	if (bFoundRight)
	{
		//move right in a tad
		right -= 10;

		for (int y=0; y<height; y++)
		{
			pdata=image + y*width + right;
			for (int x=right; x<width;x++)
				*pdata++ = 0;
		}
	}
#endif

	free(houghim);

	return TRUE;
}
