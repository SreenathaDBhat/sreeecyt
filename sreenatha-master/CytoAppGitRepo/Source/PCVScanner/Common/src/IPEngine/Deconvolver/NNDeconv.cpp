
#include <stdafx.h>


#include "nndeconv.h"


#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "GIPImage.h"
#include "gip.h"




#define MAX_Z_STACKS 32

static GIP  *pGIP = NULL;
static void **G1, **G2, **G3, **D2, **D3;

void StartDeconvGIP(int Width, int Height)
{
	if (pGIP)
		delete pGIP;

	pGIP = new GIP();
	if (pGIP->Create())
	{
		G1 = (void **)pGIP->GIPCreateImage(Width, Height, 32, 16);
		G2 = (void **)pGIP->GIPCreateImage(Width, Height, 32, 16);
		G3 = (void **)pGIP->GIPCreateImage(Width, Height, 32, 16);
		D2 = (void **)pGIP->GIPCreateImage(Width, Height, 32, 16);
		D3 = (void **)pGIP->GIPCreateImage(Width, Height, 32, 16);
	}
	else
	{
		SAFE_DELETE(pGIP);
	}
}

void EndDeconvGIP(void)
{
	if (pGIP)
	{
		pGIP->GIPFreeImage((GIPImage **)G1);
		pGIP->GIPFreeImage((GIPImage **)G2);
		pGIP->GIPFreeImage((GIPImage **)G3);
		pGIP->GIPFreeImage((GIPImage **)D2);
		pGIP->GIPFreeImage((GIPImage **)D3);
				
		SAFE_DELETE(pGIP);
	}
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

void ImageMinMax(WORD Image[], int Width, int Height, WORD &Min, WORD &Max)
{
	Min = 65535, Max = 0;

	// Get the range in the opened image
	for (int i = 0; i < Width * Height; i++)
	{
		if (Image[i] < Min) Min = Image[i];
		if (Image[i] > Max) Max = Image[i];
	}
}


void StretchTheImage(WORD Image[], int Width, int Height, int nGreys, WORD Min, WORD Max)
{	
	WORD Range = Max - Min;
	if (Range != 0 && Max > Min)
	{
		WORD n, G;
		
		for (int i = 0; i < Width * Height; i++)
		{
			if (Image[i] <= Min)
				G = 0;
			else
				G = Image[i] - Min;

			n = (G * nGreys / Range);
			if (n < nGreys)
				Image[i] = n; 
			else
				Image[i] = nGreys - 1;
		}
	}		
}

void StretchTheImageAboveMin(WORD Image[], int Width, int Height, int nGreys, WORD Min, WORD Max)
{	
	WORD Range = Max - Min;
	if (Range != 0 && Max > Min)
	{
		WORD n, G;
		
		for (int i = 0; i < Width * Height; i++)
		{
			// Noise elimination
			if (Image[i] <= nGreys / 32 || Image[i] < Min)
				G = 0;
			else
				G = Image[i] - Min;

			n = (G * nGreys / Range);
			if (n < nGreys)
				Image[i] = n; 
			else
				Image[i] = nGreys - 1;
		}
	}		
}
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

void ContrastStretch(WORD *Image, int Width, int Height, int nGreys)
{
	WORD Min, Max;

	ImageMinMax(Image, Width, Height, Min, Max);
	
	StretchTheImage(Image, Width, Height, nGreys, Min, Max);
}

/////////////////////////////////////////////////////////////////////////////
// Square an image
/////////////////////////////////////////////////////////////////////////////

void SquareIt(WORD* CSImage, int Width, int Height, int nGreys)
{
	DWORD G2;
	WORD G;

	for (int i = 0; i < Width * Height; i++)
	{
		G2 = CSImage[i] * CSImage[i];
		G = G2 / nGreys;

		if (G < nGreys)
			CSImage[i] = G;
		else
			CSImage[i] = nGreys - 1;
	}
}

/////////////////////////////////////////////////////////////////////////////
// Invert
/////////////////////////////////////////////////////////////////////////////

void Invert(WORD *pP, WORD *pG, int Width, int Height, WORD nGreys)
{
	for (int j = 0; j < Width * Height; j++)
		pG[j] = nGreys - pP[j];
}

/////////////////////////////////////////////////////////////////////////////
// Multiply two images
/////////////////////////////////////////////////////////////////////////////

void Multiply(WORD *pCS, WORD *pG, int Width, int Height, WORD nGreys)
{
	DWORD G2;
	WORD G;

	for (int j = 0; j < Width * Height; j++)
	{
		G2 = pCS[j] * pG[j];
		G = G2 / nGreys;
		if (G < nGreys)
			pCS[j] = G;
		else
			pCS[j] = nGreys - 1;
	}
}

/////////////////////////////////////////////////////////////////////////////
// Open a probe image and find its min and max
/////////////////////////////////////////////////////////////////////////////

void ProcessProbeImage(WORD* Image, int Width, int Height, int BPP, WORD &Min, WORD &Max)
{	
	WORD	nGreys = 1;

	for (int i = 0; i < BPP; i++)
		nGreys = nGreys * 2;

	Min = 0;
	Max = nGreys - 1;

	if (!pGIP)
	{
		StartDeconvGIP(Width, Height);
	}

	if (pGIP)
	{		
		pGIP->PutImage((GIPImage **)G1, (unsigned short *)Image);
		pGIP->BeginIP();
		pGIP->Open(1, (GIPImage **)G1, (GIPImage **)G2);
		pGIP->EndIP();

		WORD *pIm = new WORD[Width * Height];
		pGIP->GetImage((GIPImage **)G2, (unsigned short *)pIm);					// G4 = opened image

		ImageMinMax(pIm, Width, Height, Min, Max);

		if (Max - Min < nGreys / 4)
			Max = Min + nGreys / 4;
		StretchTheImageAboveMin(pIm, Width, Height, nGreys, Min, Max);		

		delete [] pIm;				// Return the opened contrast strecthed probe image
	}
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

void ProcessCounterstain(WORD* CSImage, WORD *ProbeImages[], int nProbes, int Width, int Height, int BPP)
{
	WORD	nGreys = 1;
	WORD    *IProbes[5] = { NULL, NULL, NULL, NULL, NULL };

	for (int i = 0; i < BPP; i++)
		nGreys = nGreys * 2;

	for (int i = 0; i < nProbes; i++)
	{
		IProbes[i] = new WORD[Width * Height];

		Invert(ProbeImages[i], IProbes[i], Width, Height, nGreys);
	}

	for (int i = 0; i < nProbes; i++)
	{
		Multiply(CSImage, IProbes[i], Width, Height, nGreys);
	}
	
	ContrastStretch(CSImage, Width, Height, nGreys);

	for (int i = 0; i < nProbes; i++)
		delete [] IProbes[i];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Subtract the gaussian blurred neighbours from a stack image
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ProcessStacks(int Width, int Height, WORD *Im1, WORD *Im2, WORD *Im3, WORD *Dest)
{
	pGIP->PutImage((GIPImage **)G1, (unsigned short *)Im1);
	pGIP->PutImage((GIPImage **)G2, (unsigned short *)Im2);
	pGIP->PutImage((GIPImage **)G3, (unsigned short *)Im3);

	pGIP->BeginIP();
	pGIP->GaussianBlur(10, 25.0f, (GIPImage **)G2, (GIPImage **)D2);
	pGIP->GaussianBlur(10, 25.0f, (GIPImage **)G3, (GIPImage **)D3);
	pGIP->MathsTechnique(_T("MathAddAndAverage"), (GIPImage **)D2, (GIPImage **)D3, (GIPImage **)G2);
	pGIP->MathsTechnique(_T("MathSub"), (GIPImage **)G1, (GIPImage **)G2, (GIPImage **)D2);
	pGIP->EndIP();

	pGIP->GetImage((GIPImage **)D2, (unsigned short *)Dest);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Nearest neighbour deconvolution on a stack of images
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void NearestNeighbourDeconv(WORD* Images[], int NumImages, int Width, int Height, int BPP, WORD *Deconvolved[])
{
	WORD    *SrcImages[MAX_Z_STACKS];
	WORD	nGreys = 1;

	if (NumImages > MAX_Z_STACKS)
	{
		ASSERT(0);
		NumImages = MAX_Z_STACKS;
	}

	for (int i = 0; i < BPP; i++)
		nGreys = nGreys * 2;

	for (int i = 0; i < NumImages; i++)
	{
		SrcImages[i] = new WORD[Width * Height];
		memcpy(SrcImages[i], Images[i], Width * Height * sizeof(WORD));
		Deconvolved[i] = new WORD[Width * Height];
	}

	if (!pGIP)
		StartDeconvGIP(Width, Height);

	if (pGIP)
	{
		ProcessStacks(Width, Height, SrcImages[0], SrcImages[1], SrcImages[1], Deconvolved[0]);
		ProcessStacks(Width, Height, SrcImages[NumImages - 1], SrcImages[NumImages - 2], SrcImages[NumImages - 2], Deconvolved[NumImages - 1]);
		for (int i = 1; i < NumImages - 1; i++)
		{
			ProcessStacks(Width, Height, SrcImages[i], SrcImages[i - 1], SrcImages[i + 1], Deconvolved[i]);
		}
	}
	else
		OutputDebugString( _T("DECONVOLUTION NO GPU ACCESS"));

	for (int i = 0; i < NumImages; i++)
		delete [] SrcImages[i];
}

// 16 bit version
WORD *MaxProjZStack16(WORD *ImPtr[], int W, int H, int NumImages)
{
    WORD *DestAddr, *Addr;
    int i, j, N;
    WORD Max, g;
	WORD *ImPtrCopy[MAX_Z_STACKS];

	for (j = 0; j < MAX_Z_STACKS; j++)
		ImPtrCopy[j] = ImPtr[j];

	N = W * H;
	DestAddr = new WORD[N];
    
    Addr = DestAddr;

    for (j = 0; j < N; j++)
    {
        Max = 0;

        for (i = 0; i < NumImages; i++)
        {
            g = *ImPtrCopy[i]++;
            if (g > Max) 
                Max = g;
        }

        *Addr++ = Max;
    }

	return DestAddr;
}