#ifndef _DECONVOLVER_HH
#define _DECONVOLVER_HH

void StartDeconvGIP(int Width, int Height);

void EndDeconvGIP(void);

void ProcessCounterstain(WORD *CSImage, WORD *ProbeImages[], int nProbes, int Width, int Height, int BitsPP);

void StretchTheImage(WORD Image[], int Width, int Height, int nGreys, WORD Min, WORD Max);

void ProcessProbeImage(WORD Image[], int Width, int Height, int BitsPP, WORD &Min, WORD &Max);

void NearestNeighbourDeconv(WORD *Images[], int NumImages, int Width, int Height, int BitsPP, WORD *Deconvolved[]);

void HistogramStretch(WORD *Image, int Width, int Height, WORD nGreys);

void StartDeconvGIP(int Width, int Height);

void EndDeconvGIP(void);

WORD *MaxProjZStack16(WORD *ImPtr[], int W, int H, int NumImages);

#endif

