//--------------------------------------------------------------//
// Blend Effects
//--------------------------------------------------------------//
int		numImages;
float	minHDR;
float4	sampleOffsets[8];
float4	ModalCorr[16];
int		filterSize;
float	filterStepSize;

texture ImaTex0;				// not possible to dynamically declare array of textures;
texture ImaTex1;
texture ImaTex2;
texture ImaTex3;
texture ImaTex4;
texture ImaTex5;
texture ImaTex6;
texture ImaTex7;
texture ImaTex8;
texture ImaTex9;
texture ImaTex10;
texture ImaTex11;
texture ImaTex12;
texture ImaTex13;
texture ImaTex14;
texture ImaTex15;

sampler SrcImage0 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex0);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SrcImage1 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex1);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SrcImage2 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex2);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SrcImage3 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex3);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SrcImage4 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex4);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SrcImage5 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex5);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SrcImage6 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex6);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SrcImage7 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex7);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SrcImage8 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex8);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SrcImage9 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex9);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SrcImage10 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex10);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SrcImage11 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex11);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SrcImage12 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex12);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SrcImage13 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex13);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SrcImage14 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex14);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SrcImage15 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex15);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

struct Blend_VS_INPUT        
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Blend_VS_OUTPUT       
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
};

////////////////////////////////////////////////////
// GetPixel for maximum 4 Textures
////////////////////////////////////////////////////
float4 GetPixel4(float2 pos, int imaIndx)
{
	float4 val;
	if (imaIndx < 2)
	{
		if (imaIndx < 1) 
			val=tex2D(SrcImage0, pos);
		else
			val=tex2D(SrcImage1, pos);
	}
	else
	{
		if (imaIndx < 3) 
			val=tex2D(SrcImage2, pos);
		else
			val=tex2D(SrcImage3, pos);
	}
	val=val-ModalCorr[imaIndx];
	if (val.r < 0.0) val.r=0.0;
	if (val.g < 0.0) val.g=0.0;
	if (val.b < 0.0) val.b=0.0;
	if (val.a < 0.0) val.a=0.0;
	return (val);
}

////////////////////////////////////////////////////
// GetPixel for maximum 8 Textures
////////////////////////////////////////////////////
float4 GetPixel8(float2 pos, int imaIndx)
{
	float4 val;
	if (imaIndx < 4)
	{
		if (imaIndx < 2)
		{
			if (imaIndx < 1)
				val=tex2D(SrcImage0, pos);
			else
				val=tex2D(SrcImage1, pos);
		}
		else
		{
			if (imaIndx < 3) 
				val=tex2D(SrcImage2, pos);
			else
				val=tex2D(SrcImage3, pos);
		}
	}
	else
	{
		if (imaIndx < 6)
		{
			if (imaIndx < 5) 
				val=tex2D(SrcImage4, pos);
			else
				val=tex2D(SrcImage5, pos);
		}
		else
		{
			if (imaIndx < 7)
				val=tex2D(SrcImage6, pos);
			else
				val=tex2D(SrcImage7, pos);
		}
	}
	val=val-ModalCorr[imaIndx];
	if (val.r < 0.0) val.r=0.0;
	if (val.g < 0.0) val.g=0.0;
	if (val.b < 0.0) val.b=0.0;
	if (val.a < 0.0) val.a=0.0;
	return (val);
}

////////////////////////////////////////////////////
// GetPixel for maximum 16 Textures
////////////////////////////////////////////////////
float4 GetPixel16(float2 pos, int imaIndx)
{
	float4 val;
	if (imaIndx < 8)
	{
		if (imaIndx < 4)
		{
			if (imaIndx < 2)
			{
				if (imaIndx < 1)
					val=tex2D(SrcImage0, pos);
				else
					val=tex2D(SrcImage1, pos);
			}
			else
			{
				if (imaIndx < 3) 
					val=tex2D(SrcImage2, pos);
				else
					val=tex2D(SrcImage3, pos);
			}
		}
		else
		{
			if (imaIndx < 6)
			{
				if (imaIndx < 5) 
					val=tex2D(SrcImage4, pos);
				else
					val=tex2D(SrcImage5, pos);
			}
			else
			{
				if (imaIndx < 7)
					val=tex2D(SrcImage6, pos);
				else
					val=tex2D(SrcImage7, pos);
			}
		}
	}
	else
	{
		if (imaIndx < 12)
		{
			if (imaIndx < 10)
			{
				if (imaIndx < 9)
					val=tex2D(SrcImage8, pos);
				else
					val=tex2D(SrcImage9, pos);
			}
			else
			{
				if (imaIndx < 11) 
					val=tex2D(SrcImage10, pos);
				else
					val=tex2D(SrcImage11, pos);
			}
		}
		else
		{
			if (imaIndx < 14)
			{
				if (imaIndx < 13) 
					val=tex2D(SrcImage12, pos);
				else
					val=tex2D(SrcImage13, pos);
			}
			else
			{
				if (imaIndx < 15)
					val=tex2D(SrcImage14, pos);
				else
					val=tex2D(SrcImage15, pos);
			}
		}
	}
	val=val-ModalCorr[imaIndx];
	if (val.r < 0.0) val.r=0.0;
	if (val.g < 0.0) val.g=0.0;
	if (val.b < 0.0) val.b=0.0;
	if (val.a < 0.0) val.a=0.0;
	return (val);
}

Blend_VS_OUTPUT BlendVertexShader( Blend_VS_INPUT Input )
{
    Blend_VS_OUTPUT Output;

    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;    // because textures have inverted coordinates 0,0 is upper left
    Output.Tex0 = Input.Tex;
    return Output;
}

//--------------------------------------------------------------//

struct Blend_PS_INPUT 
{
    float2 Tex0 : TEXCOORD0;
};


float4 SimpleBlend4PixelShader( Blend_PS_INPUT Input ) : COLOR0    
{  
	const float4	weightParameter=127.0;
	double4			weightedSum, totalWeight;
	float4			val, weight;
	weightedSum = 0.;
	totalWeight = 0.;
	for (int i=0; i<numImages; i++)
	{
		val = GetPixel4(Input.Tex0,i);
		weight=val;
		if (weight.r > 0.5) weight.r= (1.-weight.r);
		if (weight.g > 0.5) weight.g= (1.-weight.g);
		if (weight.b > 0.5) weight.b= (1.-weight.b);
		if (weight.a > 0.5) weight.a= (1.-weight.a);
		weight = weight+1.0;
		weightedSum += (val*weight);
		totalWeight +=weight;
	}
    return  (weightedSum/totalWeight);
}

float4 SimpleBlend8PixelShader( Blend_PS_INPUT Input ) : COLOR0    
{  
	float4			val, weightedSum, totalWeight, weight;
	const float4	weightParameter=127.0;
	weightedSum = 0.;
	totalWeight = 0.;
	for (int i=0; i<numImages; i++)
	{
		val = GetPixel8(Input.Tex0,i);
		weight=val;
		if (weight.r > 0.5) weight.r= (1.-weight.r);
		if (weight.g > 0.5) weight.g= (1.-weight.g);
		if (weight.b > 0.5) weight.b= (1.-weight.b);
		if (weight.a > 0.5) weight.a= (1.-weight.a);
		weight = weight+1.0;
		weightedSum += (val*weight);
		totalWeight +=weight;
	}
    return  (weightedSum/totalWeight);
}

float4 SimpleBlend16PixelShader( Blend_PS_INPUT Input ) : COLOR0    
{  
	float4			val, weightedSum, totalWeight, weight;
	const float4	weightParameter=127.0;
	weightedSum = 0.;
	totalWeight = 0.;
	for (int i=0; i<numImages; i++)
	{
		val = GetPixel16(Input.Tex0,i);
		weight=val;
		if (weight.r > 0.5) weight.r= (1.-weight.r);
		if (weight.g > 0.5) weight.g= (1.-weight.g);
		if (weight.b > 0.5) weight.b= (1.-weight.b);
		if (weight.a > 0.5) weight.a= (1.-weight.a);
		weight = weight+1.0;
		weightedSum += (val*weight);
		totalWeight +=weight;
	}
    return  (weightedSum/totalWeight);
}


//////////////////////////////////////////////////////////////////////
// Pass1 HDR Blend Pixel Shader
//////////////////////////////////////////////////////////////////////
// sorts two packed greys in place, putting smallest in a
void sort2(inout float4 pix0, inout float4 pix1)
{
    float4 cmin;
    float4 cmax; 
    
	cmin.r = min(pix0.r, pix1.r);
	cmax.r = max(pix0.r, pix1.r);
	cmin.g = min(pix0.g, pix1.g);
	cmax.g = max(pix0.g, pix1.g);
	cmin.b = min(pix0.b, pix1.b);
	cmax.b = max(pix0.b, pix1.b);
	cmin.a = min(pix0.a, pix1.a);
	cmax.a = max(pix0.a, pix1.a);
					
    pix0 = cmin;
    pix1 = cmax;
}

float4 HDRBlendPixelShader( Blend_PS_INPUT Input ) : COLOR0
{
	double4 deviation;
	float4 c[9], pixVal, medianVal;
	int i;
	float2 texCoords;

    for(i = 0; i < 8; i++)
    {
        texCoords = Input.Tex0 + sampleOffsets[i];    // add sample offsets stored in c10-c17 (inclusive)
        // take sample
        c[i] = tex2D(SrcImage0, texCoords)- minHDR; 
    }
	
	// Centre pixel
	pixVal = tex2D(SrcImage0, Input.Tex0)- minHDR;
	c[8] = pixVal;
	
    // 0-1
    sort2(c[0], c[1]);
    
    // 0-2
    sort2(c[1], c[2]);
    sort2(c[0], c[1]);
    
    // 0-3
    sort2(c[2], c[3]);
    sort2(c[1], c[2]);
    sort2(c[0], c[1]);
    
    // 0-4
    sort2(c[3], c[4]);
    sort2(c[2], c[3]);
    sort2(c[1], c[2]);
    sort2(c[0], c[1]);

    // 0-5    
    sort2(c[4], c[5]);
    sort2(c[3], c[4]);
    sort2(c[2], c[3]);
    sort2(c[1], c[2]);
    sort2(c[0], c[1]);

    // 0-6
    sort2(c[5], c[6]);    
    sort2(c[4], c[5]);
    sort2(c[3], c[4]);
    sort2(c[2], c[3]);
    sort2(c[1], c[2]);
    sort2(c[0], c[1]);

    // 0-7
    sort2(c[6], c[7]);
    sort2(c[5], c[6]);    
    sort2(c[4], c[5]);
    sort2(c[3], c[4]);
    sort2(c[2], c[3]);
    sort2(c[1], c[2]);
    sort2(c[0], c[1]);

    // 0-8
    sort2(c[7], c[8]);    
    sort2(c[6], c[7]);
    sort2(c[5], c[6]);    
    sort2(c[4], c[5]);
    sort2(c[3], c[4]);
    sort2(c[2], c[3]);
    sort2(c[1], c[2]);
    sort2(c[0], c[1]); 
   
	medianVal=c[4];
	deviation=(abs(pixVal-medianVal)*256.+1.0)*2.;
	deviation=log(deviation)*0.69314718;		// correction as GPU calculates 2log
	if (pixVal.r < medianVal.r) deviation.r = -deviation.r;
	if (pixVal.g < medianVal.g) deviation.g = -deviation.g;
	if (pixVal.b < medianVal.b) deviation.b = -deviation.b;
	if (pixVal.a < medianVal.a) deviation.a = -deviation.a;
	pixVal=pixVal + deviation/256.;
    return (pixVal);							// clamping between 0.0. and 1.0 goes automatically
}

//--------------------------------------------------------------//
// RangeBlendPixelShader
//	- first pass generate Range Images
//--------------------------------------------------------------//
float4 P0RangeBlendPixelShader( Blend_PS_INPUT Input ) : COLOR0    
{  
	int		i, j;
	float2	offset;
	float4	val, minVal, maxVal;
	
	minVal=1.0;
	maxVal=0.0;
	i=0;
	for (i=0; i<1681; i++)
	{
		offset.x=(i-filterSize)*filterStepSize;
		offset.y=(i-filterSize)*filterStepSize;
			val=tex2D(SrcImage0, Input.Tex0+offset);
			val=val-ModalCorr[0];
			if (val.r < minVal.r) minVal.r=val.r;
			if (val.g < minVal.g) minVal.g=val.g;
			if (val.b < minVal.b) minVal.b=val.b;
			if (val.a < minVal.a) minVal.a=val.a;
			if (val.r > maxVal.r) maxVal.r=val.r;
			if (val.g > maxVal.g) maxVal.g=val.g;
			if (val.b > maxVal.b) maxVal.b=val.b;
			if (val.a > maxVal.a) maxVal.a=val.a;
			i++;
	}
	if (minVal.r < 0.) minVal.r=0.;
	if (minVal.g < 0.) minVal.g=0.;
	if (minVal.b < 0.) minVal.b=0.;
	if (minVal.a < 0.) minVal.a=0.;
	
	return (maxVal-minVal);
}

//--------------------------------------------------------------//
// RangeBlendPixelShader
//	- second pass generate Range Images
//--------------------------------------------------------------//
float4 RangeBlend4PixelShader( Blend_PS_INPUT Input ) : COLOR0    
{  
	float4	val, range, weightedAverage, rangeSum;

	weightedAverage = 0.;
	rangeSum = 0.;
	for (int i=0; i<numImages; i++)
	{
		val = GetPixel8(Input.Tex0,i);				// correct input pixel for mode difference
		val=val-ModalCorr[i];
		if (val.r < 0.0) val.r=0.0;
		if (val.g < 0.0) val.g=0.0;
		if (val.b < 0.0) val.b=0.0;
		if (val.a < 0.0) val.a=0.0;
		range = GetPixel8(Input.Tex0,i+4);			// get range pixel		
		range *=range;
		rangeSum += range;
		weightedAverage += range*val;
	}
	if ( rangeSum.r > 0) weightedAverage.r /= rangeSum.r;
	if ( rangeSum.g > 0) weightedAverage.g /= rangeSum.g;
	if ( rangeSum.b > 0) weightedAverage.b /= rangeSum.b;
	if ( rangeSum.a > 0) weightedAverage.a /= rangeSum.a;
    return  (weightedAverage);
}

float4 RangeBlend8PixelShader( Blend_PS_INPUT Input ) : COLOR0    
{  
	float4	val, range, weightedAverage, rangeSum;
	
	weightedAverage = 0.;
	rangeSum = 0.;
	for (int i=0; i<numImages; i++)
	{
		val = GetPixel16(Input.Tex0,i);
		val=val-ModalCorr[i];
		range = GetPixel16(Input.Tex0,i+8);
		if (val.r < 0.0) val.r=0.0;
		if (val.g < 0.0) val.g=0.0;
		if (val.b < 0.0) val.b=0.0;
		if (val.a < 0.0) val.a=0.0;
		range *=range;
		rangeSum += range;
		weightedAverage += range*val;
	}
	if ( rangeSum.r > 0) weightedAverage.r /= rangeSum.r;
	if ( rangeSum.g > 0) weightedAverage.g /= rangeSum.g;
	if ( rangeSum.b > 0) weightedAverage.b /= rangeSum.b;
	if ( rangeSum.a > 0) weightedAverage.a /= rangeSum.a;
    return  (weightedAverage);
}


//--------------------------------------------------------------//
//	Techniques
//--------------------------------------------------------------//
technique SimpleBlend4
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 BlendVertexShader();
        PixelShader = compile ps_3_0 SimpleBlend4PixelShader();
    }
}

technique SimpleBlend8
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 BlendVertexShader();
        PixelShader = compile ps_3_0 SimpleBlend8PixelShader();
    }
}

technique SimpleBlend16
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 BlendVertexShader();
        PixelShader = compile ps_3_0 SimpleBlend16PixelShader();
    }
}

technique HDRBlend
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 BlendVertexShader();
        PixelShader = compile ps_3_0 HDRBlendPixelShader();
    }
}

technique RangeBlend4
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 BlendVertexShader();
        PixelShader = compile ps_3_0 RangeBlend4PixelShader();
    }
}

technique RangeBlend8
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 BlendVertexShader();
        PixelShader = compile ps_3_0 RangeBlend8PixelShader();
    }
}
