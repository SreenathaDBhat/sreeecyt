//--------------------------------------------------------------//
// Image Processing Effects File - Karl Ratcliff 010106
//--------------------------------------------------------------//
//    Calling program sets up simple quad based on 4 vertices with 
//    with processed display position -1 to +1
//    and matching texture ranges         0 to  1
//
//    CUSTOMVERTEX vertices[] =
//    {
//        // x      Y     Z    U  V
//        {-1.0f, -1.0f, 0.0f, 0, 1},    //lower left
//        {+1.0f, -1.0f, 0.0f, 1, 1}, //lower right
//        {+1.0f, +1.0f, 0.0f, 1, 0}, //upper right
//        {-1.0f, +1.0f, 0.0f, 0, 0}    //upper left
//    };
//
//    After loading into the vertex buffer the quad is then drawn 
//  using DrawPrimitive (D3DPT_TRIANGLEFAN, 0, 2);
//
//    Because the vertices have effectively been processed
//    there is no need for a vertex shader unless additional
//    texture coordinates are generated, e.g. in morphology 
//    or NxN filters
//
//--------------------------------------------------------------//
// Colour image processing fx
//--------------------------------------------------------------//
//

// Globals
texture SourceImageTexture;      // Base texture to load input image  
                                    // Output image will go to current render target (backbuffer by default)
                                    // Calling routines can feed output image back into SourceImageTexture to perform multipass dilations etc.

sampler SourceColImage = sampler_state        // Sampler for base texture
{
    Texture = (SourceImageTexture);
    ADDRESSU = MIRROR;                    // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;                    //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

//--------------------------------------------------------------------------------------
// Vertex shader input structure
//--------------------------------------------------------------------------------------
struct VS_INPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 TextureUV  : TEXCOORD0;  // vertex texture coords 
};
//--------------------------------------------------------------------------------------
// Vertex shader output structure
//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 TextureUV  : TEXCOORD0;  // vertex texture coords 
};

struct PS_INPUT
{
    float2 TextureUV  : TEXCOORD0;  // vertex texture coords 
};


//--------------------------------------------------------------------------------------
// Pixel shader output structure
//--------------------------------------------------------------------------------------
struct PS_OUTPUT
{
    float4 RGBColor : COLOR0;  // Pixel color    
};

VS_OUTPUT LinearVertexShader( VS_INPUT Input )
{
    VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.TextureUV = Input.TextureUV;

    return Output;
}

PS_OUTPUT RGBHSIPixelShader( PS_INPUT Input ) : COLOR0    
{  
    PS_OUTPUT val;

    val.RGBColor = tex2D(SourceColImage, Input.TextureUV);
    val.RGBColor = 0;
    
    return( val );   
}


//--------------------------------------------------------------//
// Technique Section for RGB-HSI style transformations
//--------------------------------------------------------------//

technique RGBHSI
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 RGBHSIPixelShader();
    }
}

