//--------------------------------------------------------------//
// ColourConvcersion Effects
//--------------------------------------------------------------//
float TextureWidth;
float TextureHeight;

texture SourceImageTexture;        // Base texture to load input image  
                                // Output image will go to current render target (backbuffer by default)
                                // Calling routines can feed output image back into SourceImageTexture to perform multipass dilations etc.

sampler SourceImage = sampler_state    // Sampler for base texture
{
    Texture = (SourceImageTexture);
    ADDRESSU = MIRROR;            // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;            //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

/////////////////////////////////////////////////////////////////////////////////////////////
// Vertex shader structures
/////////////////////////////////////////////////////////////////////////////////////////////

struct CVS_INPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 TextureUV  : TEXCOORD;   // vertex texture coords 
};

struct CVS_OUTPUT
{
    float4 Position      : POSITION;   // vertex position 
    float2 Tex0          : TEXCOORD0;  // vertex texture coords 
};

struct CVS_BAYEROUTPUT
{
    float4 Position      : POSITION;   // vertex position 
    float2 Tex0          : TEXCOORD0;  // vertex texture coords 
    float2 Tex1			 : TEXCOORD1;
    float2 Tex2		     : TEXCOORD2;
    float2 Tex3          : TEXCOORD3;
};

struct CVS_OUTPUTCOMPLEX
{
    float4 Position      : POSITION;   // vertex position 
    float2 Tex0          : TEXCOORD0;  // vertex texture coords 
    float2 Tex1			 : TEXCOORD1;
    float2 Tex2		     : TEXCOORD2;
    float2 Tex3          : TEXCOORD3;
    float2 Tex4          : TEXCOORD4;  
    float2 Tex5			 : TEXCOORD5;
    float2 Tex6		     : TEXCOORD6;
    float2 Tex7          : TEXCOORD7;
};


/////////////////////////////////////////////////////////////////////////////////////////////
// Straight through vertex shader
/////////////////////////////////////////////////////////////////////////////////////////////

CVS_OUTPUT LinearVertexShader( CVS_INPUT Input ) 
{
    CVS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
    Output.Position.y = -Input.Position.y;   
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.TextureUV;

    return Output;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Calculates the following texture neighbourhood coordinates
//  Tex0 Tex1
//  Tex2 Tex3
/////////////////////////////////////////////////////////////////////////////////////////////

CVS_BAYEROUTPUT BayerVertexShaderSimple( CVS_INPUT Input ) 
{
    CVS_BAYEROUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
    Output.Position.y = -Input.Position.y;   
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.TextureUV;						// Pixel of interest
	Output.Tex1.x = Output.Tex0.x + 1 / TextureWidth;	// Right
	Output.Tex1.y = Output.Tex0.y;
	Output.Tex2.x = Output.Tex0.x;						// Down
	Output.Tex2.y = Output.Tex0.y + 1 / TextureHeight;
	Output.Tex3.x = Output.Tex1.x;						// Right-Down
	Output.Tex3.y = Output.Tex2.y;

    return Output;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Calculates the following texture neighbourhood coordinates
//  XXXX Tex1 Tex2
//  Tex3 Tex0 Tex4
//  Tex5 Tex6 Tex7
// NOTE Coordinates XXXX must be calculated in the pixel shader as vertex shader can
// only calc up to 8 texture coordinates
/////////////////////////////////////////////////////////////////////////////////////////////

CVS_OUTPUTCOMPLEX Neighbour3by3VertexShader(CVS_INPUT Input) 
{
    CVS_OUTPUTCOMPLEX Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
    Output.Position.y = -Input.Position.y;   
  
    // Matching texture coords in range 0 to 1
    Output.Tex0   = Input.TextureUV;					// Centre
	Output.Tex1.x = Output.Tex0.x;						// Up
	Output.Tex1.y = Output.Tex0.y - 1 / TextureHeight;
	Output.Tex2.x = Output.Tex0.x + 1 / TextureWidth;   // Up Right
	Output.Tex2.y = Output.Tex1.y;
	Output.Tex3.x = Output.Tex1.x - 1 / TextureWidth;	// Left
	Output.Tex3.y = Output.Tex0.y;
	Output.Tex4.x = Output.Tex2.x;						// Right
	Output.Tex4.y = Output.Tex0.y;
	Output.Tex5.x = Output.Tex3.x;						// Left Down
	Output.Tex5.y = Output.Tex0.y + 1 / TextureHeight;
	Output.Tex6.x = Output.Tex0.x;						// Down
	Output.Tex6.y = Output.Tex5.y;
	Output.Tex7.x = Output.Tex2.x;						// Right-Down
	Output.Tex7.y = Output.Tex5.y;


    return Output;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------------------
// Various assorted shaders RGB/HSV shaders courtesy of ATI
//--------------------------------------------------------------------------------------

float4 RGB2HSI( CVS_OUTPUT Input ) : COLOR0
{  
	float  r, g, b, delta;
	float  colorMax, colorMin;
	float  h=0, s=0, v=0;
	float4 hsv;
	float4 rgb;
		
	rgb = tex2D(SourceImage, Input.Tex0);

	r = rgb.r;
	g = rgb.g;
	b = rgb.b;
	
	colorMax = max(r,g);
	colorMax = max(colorMax,b);

	colorMin = min(r,g);
	colorMin = min(colorMin,b);

	v = colorMax;
	
	if( colorMax != 0)
	{
		s = (colorMax - colorMin ) / colorMax;
	}

	if( s != 0)
	{

		delta = colorMax - colorMin;
		if( r == colorMax )
		{
			h = (g-b)/delta;
		}
		else if( g == colorMax)
		{
			h = 2.0 + (b-r) / delta;
		}
		else // b is max
		{
			h = 4.0 + (r-g)/delta;
		}

		h *= 60;

		if( h < 0)
		{
			h +=360;
		}
	
	}
	
	hsv.r = h / 360.0;    // moving h to be between 0 and 1.
	hsv.g = s;
	hsv.b = v;
	hsv.a = 0;

	return hsv;   
}

float4 HSI2RGB( CVS_OUTPUT Input ) : COLOR0
{  
    float4 color=0, hsv;
    float  f,p,q,t;
    float  h,s,v;
    float  r=0,g=0,b=0;
    float  i;
    
    hsv = tex2D(SourceImage, Input.Tex0);

    // this next step is flawed if the texels are 8 bit!!
    h = hsv.r * 360.0;
    s = hsv.g;
    v = hsv.b;

    if (s == 0)
    {
        color = v;
    }
    else
    {

        if( h > 360.0 )
        {
            h -= 360;
        }
        h /=60;
        i = floor( h );
        f = h-i;
        p = v * (1.0 -s );
        q = v * (1.0 -(s * f ));
        t = v * (1.0 -(s * (1.0 -f )));
        if( i == 0)
        {
            r = v;
            g = t;
            b = p;
        }
        else if( i== 1 )
        {
            r = q; 
            g = v; 
            b = p;
        }
        else if( i== 2 )
        {
            r = p;
            g = v;
            b = t;
        }
        else if( i== 3 )
        {
            r = p;
            g = q;
            b = v;
        }
        else if( i== 4 )
        {
            r = t;
            g = p;
            b = v;
        }
        else if( i== 5 )
        {
            r = v;
            g = p;
            b = q;
        }

        color.r = r;
        color.g = g;
        color.b = b;

    }

    return color;
}







/////////////////////////////////////////////////////////////////////////////////////////////
// Rendering shaders
/////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////
// Straight through grey
/////////////////////////////////////////////////////////////////////////////////////////////

float4 greyshader(CVS_OUTPUT In) : COLOR0
{
    float4 c;
    float4 texSample;
    float2 texCoords;
    float  u, v;
    
	texCoords = In.Tex0;
	texSample = tex2D(SourceImage, texCoords);

	c.rgb = texSample.r;
	if (texSample.r >= 0.95)
	{
		c.r = 1.0;
		c.gb *= 0.75;
	}
	
	if (texSample.r <= 0.05)
	{
		c.b = 1.0;
		c.rg *= 0.75;
	}
		
	c.a = 1.0;
		
    return c;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Bayer renderer - based on the following mosaic
//   G R G R G R...
//   B G B G B G
//   G R G R G R
//   B G B G B G
//   : 
/////////////////////////////////////////////////////////////////////////////////////////////

float4 BayerShaderSimple(CVS_BAYEROUTPUT In) : COLOR0
{
    float4 c;
    float4 texSample[4];
    float  v;
    float  u;
    float  w;
    
    // Calculate alternate rows and columns
    // u and w will either be 0 or 1 of something very close to 
    // NOTE rounding errors will occur
    v = 2.0 / TextureHeight;
	u = fmod(In.Tex0.y, v) * TextureHeight;
	
	v = 2.0 / TextureWidth;
	w = fmod(In.Tex0.x, v) * TextureWidth;
	
	///////////////////////////////////////////////
	// 0 1
	// 2 3
	///////////////////////////////////////////////
	texSample[0] = tex2D(SourceImage, In.Tex0);		// This pixel
	texSample[1] = tex2D(SourceImage, In.Tex1);		// Right
	texSample[2] = tex2D(SourceImage, In.Tex2);		// Down
	texSample[3] = tex2D(SourceImage, In.Tex3);		// Right Down
		
	// NOTE 0.95 because rounding errors may occur
	if (u < 0.95)
	{
		// GRG row
		if (w < 0.95) 
		{
			// G location
			c.r = texSample[1].r;
			c.g = (texSample[0].g + texSample[3].g) / 2;
			c.b = texSample[2].b;
		}
		else
		{
			// R location
			c.r = texSample[0].r;
			c.g = (texSample[1].g + texSample[2].g) / 2;
			c.b = texSample[3].b;
		}
	}
	else
	{
		// BGB row
		if (w < 0.95)
		{
			// B location
			c.r = texSample[3].r;
			c.g = (texSample[1].g + texSample[2].g) / 2;			
			c.b = texSample[0].b;
		}
		else
		{
			// G location
			c.r = texSample[2].r;
			c.g = (texSample[0].g + texSample[3].g) / 2;
			c.b = texSample[1].b;
		}
	}
	
	//c.r *= RColourBias;
	//c.g *= GColourBias;
	//c.b *= BColourBias;
	c.a = 1.0;
	
    
    return c;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// A more accurate shader
/////////////////////////////////////////////////////////////////////////////////////////////

float4 BayerShaderComplex(CVS_OUTPUTCOMPLEX In) : COLOR0
{
    float4 c;
    float4 TS[9];
    float  v;
    float  u;
    float  w;
    float2 texX;
    int    i;
    
    // The upper left coordinate
    texX.x = In.Tex0.x - 1 / TextureWidth;
    texX.y = In.Tex0.y - 1 / TextureHeight;
        
    // Calculate alternate rows and columns
    // u and w will either be 0 or 1 of something very close to 
    // NOTE rounding errors will occur
    v = 2.0 / TextureHeight;
	u = fmod(In.Tex0.y, v) * TextureHeight;
	
	v = 2.0 / TextureWidth;
	w = fmod(In.Tex0.x, v) * TextureWidth;
	
	////////////////////////////////////////////////////////////////////
	// 0  1  2
	// 3  8  4
	// 5  6  7 
	////////////////////////////////////////////////////////////////////
	
	TS[0] = tex2D(SourceImage, texX);			// Upper left
	TS[1] = tex2D(SourceImage, In.Tex1);		// Up
	TS[2] = tex2D(SourceImage, In.Tex2);		// Upper right
	TS[3] = tex2D(SourceImage, In.Tex3);		// Left
	TS[4] = tex2D(SourceImage, In.Tex4);		// Right
	TS[5] = tex2D(SourceImage, In.Tex5);		// Lower left
	TS[6] = tex2D(SourceImage, In.Tex6);		// Down
	TS[7] = tex2D(SourceImage, In.Tex7);		// Lower right
    TS[8] = tex2D(SourceImage, In.Tex0);		// Centre pixel
  		
  	c.rgba = 0;
  	
	// NOTE 0.95 because rounding errors may occur
	if (u < 0.95)
	{
		// GRG row
		if (w < 0.95) 
		{
			// G location, red row
			c.g = (TS[8].g + TS[0].g + TS[2].g + TS[5].g + TS[7].g)/5;
			c.r = (TS[3].r + TS[4].r)/2;
			c.b = (TS[1].b + TS[6].b)/2;
		}
		else
		{
			// R location, red row
			c.g = (TS[1].g + TS[3].g + TS[4].g + TS[6].g)/4;
			c.r = TS[8].r;
			c.b = (TS[0].b + TS[2].b + TS[5].b + TS[7].b)/4;
		}
	}
	else
	{
		// BGB row
		if (w < 0.95)
		{
			// B location, blue row
			c.g = (TS[1].g + TS[3].g + TS[4].g + TS[6].g)/4;
			c.b = TS[8].b;
			c.r = (TS[0].r + TS[2].r + TS[5].r + TS[7].r)/4;
		}
		else
		{
			// G location, blue row
			c.g = (TS[8].g + TS[0].g + TS[2].g + TS[5].g + TS[7].g)/5;
			c.b = (TS[3].r + TS[4].r)/2;
			c.r = (TS[1].b + TS[6].b)/2;
		}
	}
	
	c.a = 1.0;
    
    return c;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// A more accurate shader
/////////////////////////////////////////////////////////////////////////////////////////////

float4 BayerShaderLessComplex(CVS_OUTPUTCOMPLEX In) : COLOR0
{
    float4 c;
    float4 TS[9];
    float  v;
    float  u;
    float  w;
    float2 texX;
    int    i;
    
    // The upper left coordinate
    texX.x = In.Tex0.x - 1 / TextureWidth;
    texX.y = In.Tex0.y - 1 / TextureHeight;
        
    // Calculate alternate rows and columns
    // u and w will either be 0 or 1 of something very close to 
    // NOTE rounding errors will occur
    v = 2.0 / TextureHeight;
	u = fmod(In.Tex0.y, v) * TextureHeight;
	
	v = 2.0 / TextureWidth;
	w = fmod(In.Tex0.x, v) * TextureWidth;
	
	////////////////////////////////////////////////////////////////////
	// 0  1  2
	// 3  8  4
	// 5  6  7 
	////////////////////////////////////////////////////////////////////
	
	TS[0] = tex2D(SourceImage, texX);			// Upper left
	TS[1] = tex2D(SourceImage, In.Tex1);		// Up
	TS[2] = tex2D(SourceImage, In.Tex2);		// Upper right
	TS[3] = tex2D(SourceImage, In.Tex3);		// Left
	TS[4] = tex2D(SourceImage, In.Tex4);		// Right
	TS[5] = tex2D(SourceImage, In.Tex5);		// Lower left
	TS[6] = tex2D(SourceImage, In.Tex6);		// Down
	TS[7] = tex2D(SourceImage, In.Tex7);		// Lower right
    TS[8] = tex2D(SourceImage, In.Tex0);		// Centre pixel
  		
  	c.rgba = 0;
  	
	// NOTE 0.95 because rounding errors may occur
	if (u < 0.95)
	{
		// GRG row
		if (w < 0.95) 
		{
			// G location, red row
			c.g = TS[0].g;
			c.r = (TS[3].r + TS[4].r)/2;
			c.b = (TS[1].b + TS[6].b)/2;
		}
		else
		{
			// R location, red row
			c.g = (TS[1].g + TS[3].g + TS[4].g + TS[6].g)/4;
			c.r = TS[8].r;
			c.b = (TS[0].b + TS[2].b + TS[5].b + TS[7].b)/4;
		}
	}
	else
	{
		// BGB row
		if (w < 0.95)
		{
			// B location, blue row
			c.g = (TS[1].g + TS[3].g + TS[4].g + TS[6].g)/4;
			c.b = TS[8].b;
			c.r = (TS[0].r + TS[2].r + TS[5].r + TS[7].r)/4;
		}
		else
		{
			// G location, blue row
			c.g = TS[0].g;
			c.b = (TS[3].r + TS[4].r)/2;
			c.r = (TS[1].b + TS[6].b)/2;
		}
	}
	
	c.a = 1.0;
    
    return c;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// A more accurate shader
/////////////////////////////////////////////////////////////////////////////////////////////

float4 BayerShaderReallyComplex(CVS_OUTPUTCOMPLEX In) : COLOR0
{
    float4 TS[13], col;
    float  v;
    float  u;
    float  w;
    float2 texX, texA, texB, texC, texD;				// Remaining texture coordinates
    int    i;
    float  a, b, c, d, e, f, g, th2, tw2;
    
    th2 = 2 / TextureHeight;
    tw2 = 2 / TextureWidth;
    
    // The upper left coordinate
    texX.x = In.Tex3.x;
    texX.y = In.Tex1.y;
    texA.x = In.Tex0.x;
    texA.y = In.Tex0.y - th2;
    texC.x = In.Tex0.x;
    texC.y = In.Tex0.y + th2;
    texB.x = In.Tex0.x + tw2;
    texB.y = In.Tex0.y;
    texD.x = In.Tex0.x - tw2;
    texD.y = In.Tex0.y;
     
    // Calculate alternate rows and columns
    // u and w will either be 0 or 1 of something very close to 
    // NOTE rounding errors will occur
    v = 2.0 / TextureHeight;
	u = fmod(In.Tex0.y, v) * TextureHeight;
	
	v = 2.0 / TextureWidth;
	w = fmod(In.Tex0.x, v) * TextureWidth;
	
	/////////////////////////////////////////////////////////////////////////////
	//  Texture coordinates            Image Samples
	//		  A                              9                        bd
	//	   X  1  2                       0   1   2                 a  eg  a
	//	D  3  0  4	B                12  3   8   4  10        cd  ef      ef  cd
	//	   5  6  7                       5   6   7                 a  eg  a
	//		  C                              11                       bd
	/////////////////////////////////////////////////////////////////////////////
	
	TS[0]  = tex2D(SourceImage, texX);			// Upper left
	TS[1]  = tex2D(SourceImage, In.Tex1);		// Up
	TS[2]  = tex2D(SourceImage, In.Tex2);		// Upper right
	TS[3]  = tex2D(SourceImage, In.Tex3);		// Left
	TS[4]  = tex2D(SourceImage, In.Tex4);		// Right
	TS[5]  = tex2D(SourceImage, In.Tex5);		// Lower left
	TS[6]  = tex2D(SourceImage, In.Tex6);		// Down
	TS[7]  = tex2D(SourceImage, In.Tex7);		// Lower right
    TS[8]  = tex2D(SourceImage, In.Tex0);		// Centre pixel
  	TS[9]  = tex2D(SourceImage, texA);
  	TS[10] = tex2D(SourceImage, texB);
  	TS[11] = tex2D(SourceImage, texC);
  	TS[12] = tex2D(SourceImage, texD);
  	
  	a = TS[0].g + TS[2].g + TS[5].g + TS[7].g;
  	b = TS[9].g + TS[11].g;
  	c = TS[10].g + TS[12].g;
  	d = b + c;
  	f = TS[3].g + TS[4].g;
  	g = TS[1].g + TS[6].g;
  	e = f + g;
  	col = 0;
  	
	// NOTE 0.95 because rounding errors may occur
	if (u < 0.95)
	{
		// GRG row
		if (w < 0.95) 
		{
			// G location, red row
			col.g = TS[8].g;
			col.r = 5 * TS[8].r + 4 * f + 0.5 * b - c - a;
			col.b = 5 * TS[8].b + 4 * g + 0.5 * c - b - a;
		}
		else
		{
			// R location, red row
			col.r = TS[8].r;
			col.g = 4 * TS[8].g + 2 * e - d;
			col.b = 6 * TS[8].b + 2 * a - 1.5 * d;
		}
	}
	else
	{
		// BGB row
		if (w < 0.95)
		{
			// B location, blue row
			col.b = TS[8].b;
			col.g = 4 * TS[8].g + 2 * e - d;
			col.r = 6 * TS[8].r + 2 * a - 1.5 * d;
		}
		else
		{
			// G location, blue row
			col.g = TS[8].g;
			col.b = 5 * TS[8].b + 4 * f + 0.5 * b - c - a;
			col.r = 5 * TS[8].r + 4 * g + 0.5 * c - b - a;
		}
	}
	
	col.a = 1.0;
    
    return col;
}

///////////////////////////////////////////////////////////////////
// Techniques
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
// Bayer filter renderer
///////////////////////////////////////////////////////////////////
technique bayersimple
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 BayerVertexShaderSimple();
        PixelShader  = compile ps_3_0 BayerShaderSimple();
    }
}

technique bayercomplex
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 Neighbour3by3VertexShader();
        PixelShader  = compile ps_3_0 BayerShaderComplex();
    }
}

technique bayerlesscomplex
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 Neighbour3by3VertexShader();
        PixelShader  = compile ps_3_0 BayerShaderLessComplex();
    }
}

technique bayerreallycomplex
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 Neighbour3by3VertexShader();
        PixelShader  = compile ps_3_0 BayerShaderReallyComplex();
    }
}



//--------------------------------------------------------------//
// Technique Section for RGB-HSI style transformations
//--------------------------------------------------------------//

technique RGBHSI
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 RGB2HSI();
    }
}

technique HSIRGB
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 HSI2RGB();
    }
}
