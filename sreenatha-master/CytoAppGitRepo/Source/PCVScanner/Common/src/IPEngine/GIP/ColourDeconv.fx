//--------------------------------------------------------------//
// ColourDeconvolution effect file
//--------------------------------------------------------------//
float4		QVector[3];
texture 	SourceImageTexture;     // Base texture to load input image  
                                	// Output image will go to current render target (backbuffer by default)
                                	// Calling routines can feed output image back into SourceImageTexture to perform multipass dilations etc.

sampler SourceImage = sampler_state    // Sampler for base texture
{
    Texture = (SourceImageTexture);
    ADDRESSU = MIRROR;            // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;            //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

//--------------------------------------------------------------------------------------
// Vertex shader input structure
//--------------------------------------------------------------------------------------
struct CVS_INPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 TextureUV  : TEXCOORD;   // vertex texture coords 
};

struct CVS_OUTPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 Tex0       : TEXCOORD0;  // vertex texture coords 
};

CVS_OUTPUT LinearVertexShader( CVS_INPUT Input ) 
{
    CVS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.TextureUV;

    return Output;
}

struct CPS_INPUT 
{
    float2 Tex0 : TEXCOORD0;
};

//--------------------------------------------------------------------------------------
// Pixel Shader ColourDeconv
//--------------------------------------------------------------------------------------

float4 ColourDeconv( CPS_INPUT Input ) : COLOR0
{  
	float4  val, outp;
	val=-log(tex2D(SourceImage, Input.Tex0)+1./255.);
	outp.x=val.x*QVector[0].x+val.y*QVector[0].y+val.z*QVector[0].z;
	outp.y=val.x*QVector[1].x+val.y*QVector[1].y+val.z*QVector[1].z;
	outp.z=val.x*QVector[2].x+val.y*QVector[2].y+val.z*QVector[2].z;
	outp.w=0;
	outp=exp(log(255.)-outp)/255.;
	return outp;   
}

//--------------------------------------------------------------//
// Technique Section 
//--------------------------------------------------------------//

technique ColourDeconv
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 ColourDeconv();
    }
}
