//--------------------------------------------------------------//
// Component Effects File
//--------------------------------------------------------------//
float4 componentmask : register (c10);
float4 mincomponentthreshold : register (c10);
float4 maxcomponentthreshold : register (c11);
float4 alphablendreg[3] : register (c10);

// Globals
texture Src1ImageTexture;        // Base texture to load input image  
texture Src2ImageTexture;        // Base texture to load input image  
texture Src3ImageTexture;
                                // Output image will go to current render target (backbuffer by default)

sampler Src1Image = sampler_state    // Sampler for source 1 image
{
    Texture = (Src1ImageTexture);
    ADDRESSU = MIRROR;
    ADDRESSV = MIRROR;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler Src2Image = sampler_state    // Sampler for source 2 image
{
    Texture = (Src2ImageTexture);
    ADDRESSU = MIRROR;
    ADDRESSV = MIRROR;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler Src3Image = sampler_state    // Sampler for source 3 image
{
    Texture = (Src3ImageTexture);
    ADDRESSU = MIRROR;
    ADDRESSV = MIRROR;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

//--------------------------------------------------------------------------------------
// Vertex shader input structure
//--------------------------------------------------------------------------------------
struct CVS_INPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 TextureUV  : TEXCOORD;   // vertex texture coords 
};

struct CVS_OUTPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 Tex0       : TEXCOORD0;  // vertex texture coords 
};

CVS_OUTPUT LinearVertexShader( CVS_INPUT Input ) 
{
    CVS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.TextureUV;

    return Output;
}

float4 splitcomponentshader( CVS_OUTPUT In) : COLOR0
{
	float4 vs;
	float4 vr = 0.0;
	
	vs = tex2D(Src1Image, In.Tex0);
	
	vr.r = vs.r * componentmask.r;
	vr.g = vs.g * componentmask.g;
	vr.b = vs.b * componentmask.b;
	
	return vr;	
}

float4 joincomponentshader( CVS_OUTPUT In) : COLOR0
{
	float4 vr, vg, vb;
	float4 vc;
	
	vr = tex2D(Src1Image, In.Tex0);
	vg = tex2D(Src2Image, In.Tex0);
	vb = tex2D(Src3Image, In.Tex0);
	
	vc.r = vr.r;
	vc.g = vg.g;
	vc.b = vb.b;
	vc.a = 1.0;
	
	return vc;	
}


float4 componentthreshshader( CVS_OUTPUT In) : COLOR0
{
	float4 v;
	
	v = tex2D(Src1Image, In.Tex0);
	if (v.r < mincomponentthreshold.r) v.r = 0;
	if (v.g < mincomponentthreshold.g) v.g = 0;
	if (v.b < mincomponentthreshold.b) v.b = 0;
	if (v.r > maxcomponentthreshold.r) v.r = 0;
	if (v.g > maxcomponentthreshold.g) v.g = 0;
	if (v.b > maxcomponentthreshold.b) v.b = 0;
	
	return v;		
}

float4 alphablendshader( CVS_OUTPUT In) : COLOR0
{
	float4 v1, v2, v3;
	float4 vc;
	
	v1 = tex2D(Src1Image, In.Tex0);
	v2 = tex2D(Src2Image, In.Tex0);
	v3 = tex2D(Src3Image, In.Tex0);
	
	vc = (alphablendreg[0] * v1 + alphablendreg[1] * v2 + alphablendreg[2] * v3);
	
	return vc;	
}

technique splitcomponents
{
	pass P0
	{
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 splitcomponentshader();
	}
}

technique joincomponents
{
	pass P0
	{
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 joincomponentshader();
	}
}

technique componentthresh
{
	pass P0
	{
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 componentthreshshader();
	}
}

technique alphablend
{
	pass P0
	{
		CULLMODE = NONE;
		ZENABLE = FALSE;
		
		VertexShader = compile vs_2_0 LinearVertexShader();
		PixelShader = compile ps_2_0 alphablendshader();
	}
}
