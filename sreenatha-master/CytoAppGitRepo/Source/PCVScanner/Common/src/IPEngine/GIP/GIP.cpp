

//---------------------------------------------------------------------------------------------
// Graphics image processing
//---------------------------------------------------------------------------------------------
#include "stdafx.h"

#include "GIP.h"
#include "Resource.h"

void OT(LPCTSTR Msg, double T)
{
    CString M;

    M.Format("%s %6.3lf\r\n", Msg, T * 1000.);
    OutputDebugString(M);
}

void D3DEXPLAIN(HRESULT hr)
{
    switch (hr)
    {
    case D3D_OK:
        break;

    case D3DERR_DRIVERINTERNALERROR:
        OutputDebugString("GIP D3D Internal Driver Error\r\n");
        break;

    case D3DERR_DEVICELOST:
        OutputDebugString("GIP D3D DEVICE LOST\r\n");
        break;

    case D3DERR_INVALIDCALL:
        OutputDebugString("GIP D3D INVALID CALL\r\n");
        break;

    default:
        CString Msg;

        Msg.Format("D3D ERROR CODE = %x\r\n", hr);
        OutputDebugString(Msg);
    }
}

//---------------------------------------------------------------------------------------------

// constructor
GIP::GIP()
{
    int i;

    pd3dDevice = NULL;
    pDevice = NULL;
	for (i = 0; i<GIP_SWAPBUFFERS; i++)
		pSwapTexture[i] = NULL;
    for (i = 0; i < GIP_MAXIMAGES; i++)
    {
        m_Images[i].pImage = NULL;
        m_Images[i].Status = Empty;
    }

	m_SwapCounter=0;

    // Effects
	m_pGipEffect = NULL;		// pointer to D3DX effect interface
	m_pMorph = NULL;
	m_pMath = NULL;
	m_pComponent = NULL;
	m_pColourConv = NULL;
	m_pThreshold = NULL;
	m_pConvolution = NULL;
    m_pGauss = NULL;
    m_pKuwahara = NULL;
	m_pMedian = NULL;
	m_pBlend = NULL;
	m_pColourDeconv = NULL;
	m_pFFT = NULL;
	m_pEDM = NULL;
	m_pHough = NULL;

	// Scratch pad images
    m_pScratch1 = NULL;
    m_pScratch2 = NULL;
    m_pScratch3 = NULL;
    m_pScratch4 = NULL;
}

//---------------------------------------------------------------------------------------------

// destructor
GIP::~GIP()
{
	Destroy();
}

//---------------------------------------------------------------------------------------------

void GIP::Destroy()
{   
    int i;

    // Remove scratch images
    SAFE_DELETE(m_pScratch1);
    SAFE_DELETE(m_pScratch2);
    SAFE_DELETE(m_pScratch3);
    SAFE_DELETE(m_pScratch4);

    // Free working textures not already empty
    for (i = 0; i < GIP_MAXIMAGES; i++)
    {
        if (m_Images[i].Status != Empty)
            GIPFreeImage(&m_Images[i].pImage);
    }

	// Clear up our effects
    SAFE_DELETE(m_pConvolution);
    SAFE_DELETE(m_pGauss);
    SAFE_DELETE(m_pMedian);
    SAFE_DELETE(m_pKuwahara);
    SAFE_DELETE(m_pComponent);
    SAFE_DELETE(m_pColourConv);
    SAFE_DELETE(m_pThreshold);
    SAFE_DELETE(m_pMath);
    SAFE_DELETE(m_pMorph);
    SAFE_DELETE(m_pGipEffect);
	SAFE_DELETE(m_pBlend);
	SAFE_DELETE(m_pColourDeconv);
	SAFE_DELETE(m_pFFT);
	SAFE_DELETE(m_pEDM);
	SAFE_DELETE(m_pHough);

	for (i = 0; i < GIP_SWAPBUFFERS; i++)
		if (pSwapTexture[i]) delete pSwapTexture[i];
	SAFE_DELETE(pDevice);
}

//---------------------------------------------------------------------------------------------
//
//	Create GIP device and the effect files
//	maxWidth and maxHeight are used for the SwapTexture size. The Size of all images for this
//  device should be smaller or equal than the maximum image size
//---------------------------------------------------------------------------------------------
BOOL GIP::Create()
{
	// destroy any previous textures, effect etc, etc.
	Destroy();

    pDevice = new GIPDevice();

	// initialise D3D object and device
    if (!pDevice->Init())
		return FALSE;
    
    pd3dDevice = pDevice->GetDevice();

 	// Create the effects files
    SAFE_DELETE(m_pGipEffect);
    m_pGipEffect = new GIPEffect(pDevice);
    if (!m_pGipEffect->CreateFXFromResource(IDR_IMAGEPROCESSINGFX))
        return FALSE;

	SAFE_DELETE(m_pMorph);
	m_pMorph = new GIPMorph(pDevice);

	SAFE_DELETE(m_pComponent);
	m_pComponent = new GIPComponent(pDevice);

	SAFE_DELETE(m_pThreshold);
	m_pThreshold = new GIPThreshold(pDevice);

	SAFE_DELETE(m_pColourConv);
    m_pColourConv = new GIPColourConv(pDevice);

	SAFE_DELETE(m_pConvolution);
    m_pConvolution = new GIPFilter(pDevice);

    SAFE_DELETE(m_pGauss);
    m_pGauss = new GIPGaussian(pDevice);
    
    SAFE_DELETE(m_pMedian);
    m_pMedian = new GIPMedian(pDevice);

    SAFE_DELETE(m_pKuwahara);
    m_pKuwahara = new GIPKuwahara(pDevice);

	SAFE_DELETE(m_pMath);
	m_pMath = new GIPMath(pDevice);

 	SAFE_DELETE(m_pBlend);
	m_pBlend = new GIPBlend(pDevice);

 	SAFE_DELETE(m_pColourDeconv);
	m_pColourDeconv = new GIPColourDeconv(pDevice);

	SAFE_DELETE(m_pFFT);
	m_pFFT = new GIPFFT(pDevice);

	SAFE_DELETE(m_pEDM);
	m_pEDM = new GIPEDM(pDevice);

	SAFE_DELETE(m_pHough);
	m_pHough = new GIPHough(pDevice);

  return TRUE;
}

//---------------------------------------------------------------------------------------------
//	BeginIP
//	must call this before applying any techniques
//---------------------------------------------------------------------------------------------
void GIP::BeginIP()
{
	pd3dDevice->BeginScene();
}

//---------------------------------------------------------------------------------------------
//	ENDIP
//	 must call this after applying all techniques
//---------------------------------------------------------------------------------------------
void GIP::EndIP()
{
    HRESULT hr;

	hr = pd3dDevice->EndScene();
}

//---------------------------------------------------------------------------------------------
// Create a scratch pad image if we don't already have one of the correct dimensions
//---------------------------------------------------------------------------------------------

void GIP::CreateScratch(GIP_IMAGE pScratch, GIP_IMAGE src)
{
    long Width, Height, Overlap, Bpp;
    
    // The scratch image is unavailable so create 
    if (!*pScratch)
    {
        (*src)->GetImageDimensions(&Width, &Height, &Overlap, &Bpp);
		*pScratch = new GIPImage(pd3dDevice);
		if (!(*pScratch)->Create(Width, Height, Overlap, Bpp))
            SAFE_DELETE(*pScratch);
    }
    else
    {
        if (!(*src)->SameDimensions((*pScratch)))
        {
            delete (*pScratch);
            (*src)->GetImageDimensions(&Width, &Height, &Overlap, &Bpp);
		    *pScratch = new GIPImage(pd3dDevice);
		    if (!(*pScratch)->Create(Width, Height, Overlap, Bpp))
                SAFE_DELETE(*pScratch);
        }
    }
}

void GIP::CreateScratch(GIP_IMAGE pScratch, long Width, long Height, long Overlap, long Bpp)
{    
    // The scratch image is unavailable so create 
    if (!*pScratch)
    {
		*pScratch = new GIPImage(pd3dDevice);
		if (!(*pScratch)->Create(Width, Height, Overlap, Bpp))
            SAFE_DELETE(*pScratch);
    }
    else
    {
		long w, h, overl, bits;
		(*pScratch)->GetImageDimensions(&w, &h, &overl, &bits);
//		if ((bits > 16) && (Bpp>16)) bits=Bpp;		// 24 bits 32 bits are treated the same
        if ((w!=Width) || (h!=Height) || (overl!=Overlap) || (bits != Bpp))
        {
            delete (*pScratch);
		    *pScratch = new GIPImage(pd3dDevice);
		    if (!(*pScratch)->Create(Width, Height, Overlap, Bpp))
                SAFE_DELETE(*pScratch);
        }
    }
}
//---------------------------------------------------------------------------------------------
// GIPCreateImage - Create GIP Images Dynamically
//---------------------------------------------------------------------------------------------
GIPImage** GIP::GIPCreateImage(long Width, long Height, long Overlap, long BPP)
{
	GIPImage *src, **Handle = NULL;
    int i;
    long W, H, O, B;
    BOOL Alloc = FALSE;

    for (i = 0; i < GIP_MAXIMAGES; i++)
    {
        // First check any released images, if any have the same dimensions
        // just use that image
        if (m_Images[i].Status == Released && m_Images[i].pImage)
        {
            m_Images[i].pImage->GetImageDimensions(&W, &H, &O, &B);
            if (W == Width && H == Height && O == Overlap && B == BPP)
            {
                Handle = m_Images[i].pImage->GetHandle();
                m_Images[i].Status = InUse;                 // Back in use again
                break;
            }
        }
    }

    // No textures that we can already use so can we create a new one ?
    if (Handle == NULL)
    {
        for (i = 0; i < GIP_MAXIMAGES; i++)
        {
            // Found an empty slot so use it
            if (m_Images[i].Status == Empty || !m_Images[i].pImage)
            {
                Alloc = TRUE;
                break;
            }
        }

        // If this is true we have no empty slots so go through any released
        // images and reallocate with new image dimensions
        if (!Alloc)
        {
            for (i = 0; i < GIP_MAXIMAGES; i++)
            {
                // If this condition is true then the image is free to use
                // but is the wrong dimensions so free it off
                if (m_Images[i].Status == Released)
                {
                    Alloc = TRUE;
                    GIPFreeImage(m_Images[i].pImage->GetHandle());
                    break;
                }
            }
        }

        // Allocate a new texture
        if (Alloc)
        {
	        pd3dDevice = pDevice->GetDevice();
	        src = new GIPImage(pd3dDevice);
	        if (src != NULL)
	        {
		        if (src->Create(Width, Height, Overlap, BPP))
                {
			        Handle = src->GetHandle();
                    m_Images[i].pImage = src;
                    m_Images[i].Status = InUse;
                }
				else
				{
					OutputDebugString(_T("ERROR UNABLE TO CREATE TEXTURE\r\n"));
				}
	        }
        }
    }
 	return Handle;
}

//---------------------------------------------------------------------------------------------
// Release a GIP image for re-use elsewhere
//---------------------------------------------------------------------------------------------

void GIP::GIPReleaseImage(GIP_IMAGE GipSource)
{
	if (GipSource > 0)
    {
        int i;

        for (i = 0; i < GIP_MAXIMAGES; i++)
        {
            if (GipSource == m_Images[i].pImage->GetHandle())
            {
                m_Images[i].Status = Released;
                break;
            }
        }
    }
}
//---------------------------------------------------------------------------------------------
// Release all GIP images. Not this does not free all the textures off. These will be kept 
// and re-used if neccessary
//---------------------------------------------------------------------------------------------

void GIP::GIPReleaseAll(void)
{
    int i;

    for (i = 0; i < GIP_MAXIMAGES; i++)
    {
        if (m_Images[i].Status != Empty)
            m_Images[i].Status = Released;
    }
}

//---------------------------------------------------------------------------------------------
// GIPFreeImage - Destroy GIP Images Dynamically
//---------------------------------------------------------------------------------------------
void GIP::GIPFreeImage(GIP_IMAGE HandleGipImage)
{
	if (HandleGipImage > 0)
    {
        int i;

        for (i = 0; i < GIP_MAXIMAGES; i++)
        {
            if (HandleGipImage == m_Images[i].pImage->GetHandle())
            {
                m_Images[i].Status = Empty;
                break;
            }
        }
		delete (GIPImage *)*HandleGipImage;
    }
}

//---------------------------------------------------------------------------------------------
// GIPGetImageDimensions - Obtain Image Dimensions of GIP Image
//---------------------------------------------------------------------------------------------
void GIP::GIPGetImageDimensions(GIP_IMAGE HandleGipImage, long *Width, long *Height, long *Overlap, long *BPP)
{
	if (HandleGipImage > 0)
		((GIPImage *)(*HandleGipImage))->GetImageDimensions(Width, Height, Overlap, BPP);
}

//---------------------------------------------------------------------------------------------
//	GetImage - Get (4 Bytes Colour)Image from GPU into existing image buffer
//---------------------------------------------------------------------------------------------
BOOL GIP::GetImage(GIP_IMAGE targetHandle, RGBQUAD *pImageData)
{
 	GIPSwapTexture* pSwap;
   if (!targetHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*targetHandle))==NULL) return FALSE;
	return ((GIPImage *)(*targetHandle))->GetImage(pSwap, pImageData);
};

//---------------------------------------------------------------------------------------------
//	GetImage - Get (Colour)Image from GPU into existing image buffer
//---------------------------------------------------------------------------------------------
BOOL GIP::GetImage(GIP_IMAGE targetHandle, RGBTRIPLE *pImageData)
{
	GIPSwapTexture* pSwap;
    if (!targetHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*targetHandle))==NULL) return FALSE;
	return ((GIPImage *)(*targetHandle))->GetImage(pSwap, pImageData);
};

//---------------------------------------------------------------------------------------------
//	GetImage - Get (Grey)Image from GPU into existing image buffer
//---------------------------------------------------------------------------------------------
BOOL GIP::GetImage(GIP_IMAGE targetHandle, BYTE *pImageData)
{
	GIPSwapTexture* pSwap;
    if (!targetHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*targetHandle))==NULL) return FALSE;
	return ((GIPImage *)(*targetHandle))->GetImage(pSwap, pImageData);
};

//---------------------------------------------------------------------------------------------
//	GetImage - Get (Colour)Image from GPU into existing image buffer
//---------------------------------------------------------------------------------------------
BOOL GIP::GetImage(GIP_IMAGE targetHandle, RGB16BTRIPLE *pImageData)
{
	GIPSwapTexture* pSwap;
    if (!targetHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*targetHandle))==NULL) return FALSE;
	return ((GIPImage *)(*targetHandle))->GetImage(pSwap, pImageData);
};

//---------------------------------------------------------------------------------------------
//	GetImage - Get (Grey 16 bit)Image from GPU into existing image buffer
//---------------------------------------------------------------------------------------------
BOOL GIP::GetImage(GIP_IMAGE targetHandle, unsigned short *pImageData)
{
	GIPSwapTexture* pSwap;
    if (!targetHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*targetHandle))==NULL) return FALSE;
	return ((GIPImage *)(*targetHandle))->GetImage(pSwap, pImageData);
};

//---------------------------------------------------------------------------------------------
//	GetImage - Get Floating point Image from GPU (R16G16B16A16 format) into existing image buffer
//---------------------------------------------------------------------------------------------
BOOL GIP::GetImage(GIP_IMAGE targetHandle, float *pImageData)
{
	GIPSwapTexture* pSwap;
    if (!targetHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*targetHandle))==NULL) return FALSE;
	return ((GIPImage *)(*targetHandle))->GetImage(pSwap, pImageData);
};

//---------------------------------------------------------------------------------------------
//	GetImageComponent - Get Image Component from GPU into existing image buffer
//---------------------------------------------------------------------------------------------
BOOL GIP::GetImageComponent(GIP_IMAGE targetHandle, long component, BYTE *pImageData)
{
	GIPSwapTexture* pSwap;
    if (!targetHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*targetHandle))==NULL) return FALSE;
	return ((GIPImage *)(*targetHandle))->GetImageComp(pSwap, component, pImageData);
};

//---------------------------------------------------------------------------------------------
//	GetImage - Get (Colour)Image from GPU into existing image buffer
//---------------------------------------------------------------------------------------------
BOOL GIP::GetImageComponent(GIP_IMAGE targetHandle, long component, unsigned short *pImageData)
{
	GIPSwapTexture* pSwap;
    if (!targetHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*targetHandle))==NULL) return FALSE;
	return ((GIPImage *)(*targetHandle))->GetImageComp(pSwap, component, pImageData);
};

//---------------------------------------------------------------------------------------------
//	PutImage - Put (Colour)Image in GPU
//---------------------------------------------------------------------------------------------
BOOL GIP::PutImage(GIP_IMAGE sourceHandle, RGBTRIPLE *pImageData)
{
	GIPSwapTexture* pSwap;
	if (!sourceHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*sourceHandle))==NULL) return FALSE;
	return ((GIPImage *)(*sourceHandle))->PutImage(pSwap, pImageData);
};

//---------------------------------------------------------------------------------------------
//	PutImage - Put (Colour 16 bit)Image in GPU
//---------------------------------------------------------------------------------------------
BOOL GIP::PutImage(GIP_IMAGE sourceHandle, RGB16BTRIPLE *pImageData)
{
	GIPSwapTexture* pSwap;
	if (!sourceHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*sourceHandle))==NULL) return FALSE;
	return ((GIPImage *)(*sourceHandle))->PutImage(pSwap, pImageData);
};

//--------------------------------------------------------------------------------------------------------------
//	PutImage - Put Part of (Colour)Image with width=w and height=h from startpoint (x0, y0) in existing GIPImage
//--------------------------------------------------------------------------------------------------------------
BOOL GIP::PutImage(GIP_IMAGE sourceHandle, RGBTRIPLE *pImageData, long w, long h, long x0, long y0, long ColourMode)
{
	GIPSwapTexture* pSwap;
	if (!sourceHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*sourceHandle))==NULL) return FALSE;
	return ((GIPImage *)(*sourceHandle))->PutImage(pSwap, pImageData, w, h, x0, y0, ColourMode);
};

//--------------------------------------------------------------------------------------------------------------
//	PutImage - Put Part of (Colour)Image with width=w and height=h from startpoint (x0, y0) in existing GIPImage
//--------------------------------------------------------------------------------------------------------------
BOOL GIP::PutImage(GIP_IMAGE sourceHandle, RGB16BTRIPLE *pImageData, long w, long h, long x0, long y0, long ColourMode)
{
	GIPSwapTexture* pSwap;
	if (!sourceHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*sourceHandle))==NULL) return FALSE;
	return ((GIPImage *)(*sourceHandle))->PutImage(pSwap, pImageData, w, h, x0, y0, ColourMode);
};

//---------------------------------------------------------------------------------------------
//	PutImage - Put (Grey)Image in GPU
//---------------------------------------------------------------------------------------------
BOOL GIP::PutImage(GIP_IMAGE sourceHandle, BYTE *pImageData)
{
	GIPSwapTexture* pSwap;
	if (!sourceHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*sourceHandle))==NULL) return FALSE;
	return ((GIPImage *)(*sourceHandle))->PutImage(pSwap, pImageData);
};

//---------------------------------------------------------------------------------------------
//	PutImage - Put (Grey 16  bit)Image in GPU
//---------------------------------------------------------------------------------------------
BOOL GIP::PutImage(GIP_IMAGE sourceHandle, unsigned short *pImageData)
{
	GIPSwapTexture* pSwap;
	if (!sourceHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*sourceHandle))==NULL) return FALSE;
	return ((GIPImage *)(*sourceHandle))->PutImage(pSwap, pImageData);
};


//------------------------------------------------------------------------------------------------------------
//	PutImage - Put Part of (Grey)Image with width=w and height=h from startpoint (x0, y0) in existing GIPImage
//------------------------------------------------------------------------------------------------------------
BOOL GIP::PutImage(GIP_IMAGE sourceHandle, BYTE *pImageData, long w, long h, long x0, long y0)
{
	GIPSwapTexture* pSwap;
	if (!sourceHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*sourceHandle))==NULL) return FALSE;
	return ((GIPImage *)(*sourceHandle))->PutImage(pSwap, pImageData, w, h, x0, y0);
};

//------------------------------------------------------------------------------------------------------------
//	PutImage - Put Part of (Grey 16 bit)Image with width=w and height=h from startpoint (x0, y0) in existing GIPImage
//------------------------------------------------------------------------------------------------------------
BOOL GIP::PutImage(GIP_IMAGE sourceHandle, unsigned short *pImageData, long w, long h, long x0, long y0)
{
	GIPSwapTexture* pSwap;
	if (!sourceHandle || !pImageData || !pDevice)
		return FALSE;
	// set the swap texture surface according to the GipImage
	if ((pSwap=GetSwapTexture(*sourceHandle))==NULL) return FALSE;
	return ((GIPImage *)(*sourceHandle))->PutImage(pSwap, pImageData, w, h, x0, y0);
};

//---------------------------------------------------------------------------------------------
//	GetSwapTexture -  Get a matching SwapTexture from the pool or create a new one
//---------------------------------------------------------------------------------------------
GIPSwapTexture* GIP::GetSwapTexture(GIPImage *sourceHandle)
{
	GIPSwapTexture* pSwap;
	long			w, h, bpp, overl;
	long			minCount=0x7FFFFFFF;
	int				i, i0, i1;
	BOOL			ok = FALSE;

	m_SwapCounter +=1;
	sourceHandle->GetImageDimensions(&w, &h, &overl, &bpp);

	// check if corresponding swaptexture exist 
	i=0; i0=-1;
	do 
	{
		if (pSwapTexture[i]!=NULL)
		{
			if ((m_SwapWidth[i]==w) &&(m_SwapHeight[i]==h) && (m_SwapBpp[i]==bpp) && (m_SwapOverlap[i]==overl))
			{
				m_SwapLastUsed[i]=m_SwapCounter;
				return (pSwapTexture[i]);
			}
			else if ((i0 < 0) && (m_SwapLastUsed[i] < minCount))
			{
				i1=i;
				minCount=m_SwapLastUsed[i];
			}
		}
		else if (i0< 0) i0=i;			
		i++;
	}while (i<GIP_SWAPBUFFERS);

	if (i0<0) i0=i1;
	// not found yet so create new swaptexture
	if (pSwapTexture[i0] != NULL) delete pSwapTexture[i0];

	pSwap = new GIPSwapTexture(pd3dDevice);
    // Create non-rendertarget surface to transfer data in and out of GPU memory
	switch (bpp)
	{
		case 8:
				ok=pSwap->Create(w, h, overl);
				break;
		case 16:
				ok=pSwap->Create16B(w, h, overl);
				break;
		case 24:
				ok=pSwap->Create(w, h);
				break;
		case 32:
				ok=pSwap->CreateFP(w, h);
				break;
		case 48:
				ok=pSwap->Create16B(w, h);
				break;
		case 128:
				ok=pSwap->CreateFP32(w, h);	
				break;
	}
    if (!ok)
	{
		MessageBox(NULL,"Could not create off-screen texture", "DirectX", MB_ICONERROR);
		if (pSwap !=NULL) delete pSwap;
		return NULL;
	}
	pSwapTexture[i0]=pSwap;
	m_SwapWidth[i0]=w;
	m_SwapHeight[i0]=h;
	m_SwapBpp[i0]=bpp;
	m_SwapOverlap[i0]=overl;
	m_SwapLastUsed[i0]=m_SwapCounter;
	return (pSwap);
}

//---------------------------------------------------------------------------------------------
//	Erosion (in MorphEffect.x)
//---------------------------------------------------------------------------------------------
void GIP::Erode(int nPasses, GIP_IMAGE src, GIP_IMAGE target)
{
	if ((!m_pMorph) || (!src) || (!target) || (nPasses <= 0)) 
        return;

	if (!(*src)->SameDimensions(*target)) 
        return;
	            
    CreateScratch(&m_pScratch2, src);
    CreateScratch(&m_pScratch3, src);

    if (m_pMorph->Initialise((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight))
	{
		// setup display quad
		pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pMorph->Apply("MinAxialCross", "MinDiagonalCross", nPasses, src, target, &m_pScratch2, &m_pScratch3);
	}
}

//---------------------------------------------------------------------------------------------
//	Dilation (in MorphEffect.x)
//---------------------------------------------------------------------------------------------
void GIP::Dilate(int nPasses, GIP_IMAGE src, GIP_IMAGE target)
{
	if ((!m_pMorph) || (!src) || (!target) || (nPasses <= 0)) 
        return;
	
    if (!(*src)->SameDimensions(*target)) 
        return;
	
    CreateScratch(&m_pScratch2, src);
    CreateScratch(&m_pScratch3, src);

    if (m_pMorph->Initialise((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight))
	{
		// setup display quad
		pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pMorph->Apply("MaxAxialCross", "MaxDiagonalCross", nPasses, src, target, &m_pScratch2, &m_pScratch3);
	}
}

//---------------------------------------------------------------------------------------------
//	Dilation (in MorphEffect.x)
//---------------------------------------------------------------------------------------------
void GIP::DilateAboveThresh(int nPasses, GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target, float Thresh)
{
	if ((!m_pMorph) || (!src1) || (!src2) || (!target) || (nPasses <= 0)) 
        return;
	
    if (!(*src1)->SameDimensions(*target)) 
        return;
	
    CreateScratch(&m_pScratch2, src1);
    CreateScratch(&m_pScratch3, src1);

    if (m_pMorph->Initialise((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight))
	{
		// setup display quad
		pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
		m_pMorph->Effect()->Effect()->SetFloat("DilateThresh", Thresh);
		m_pMorph->Apply("MaxThreshAxialCross", "MaxThreshDiagonalCross", nPasses, src1, src2, target, &m_pScratch2, &m_pScratch3);
	}
}
//---------------------------------------------------------------------------------------------
//	Close (in MorphEffect.x)
//---------------------------------------------------------------------------------------------
void GIP::Close(int nPasses, GIP_IMAGE src, GIP_IMAGE target)
{
	if ((!m_pMorph) || (!src) || (!target) || (nPasses <= 0)) 
        return;
	
    if (!(*src)->SameDimensions(*target)) 
        return;

	if (m_pMorph->Initialise((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight))
	{
		// setup display quad
		pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		if (nPasses == 1)
		{
            CreateScratch(&m_pScratch1, src);

            if (m_pScratch1)
			{
				Dilate(nPasses, src, m_pScratch1->GetHandle());		// use first swap buffer as target
				Erode(nPasses, m_pScratch1->GetHandle(), target);	// use first swap buffer as source
			}
		}
		else
		{
			// here the src and target are the same but swap buffesr are used in earlier passes so its OK 
			Dilate(nPasses, src, target);
			Erode(nPasses, target, target);
		}
	}
}

//---------------------------------------------------------------------------------------------
//	Open (in MorphEffect.x)
//---------------------------------------------------------------------------------------------
void GIP::Open(int nPasses, GIP_IMAGE src, GIP_IMAGE target)
{
	if ((!m_pMorph) || (!src) || (!target) || (nPasses <= 0)) return;
	if (!(*src)->SameDimensions(*target)) return;

	if (m_pMorph->Initialise((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight))
	{
		// setup display quad
		pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		if (nPasses == 1)
		{
            CreateScratch(&m_pScratch1, src);

            if (m_pScratch1)
			{
				Erode(nPasses, src, m_pScratch1->GetHandle());		// use first swap buffer as target
				Dilate(nPasses, m_pScratch1->GetHandle(), target);	// use first swap buffer as source
			}
		}
		else
		{
			// here the src and target are the same but swap buffesr are used in earlier passes so its OK 
			Erode(nPasses, src, target);
			Dilate(nPasses, target, target);
		}
	}
}

//---------------------------------------------------------------------------------------------
//	MathsTechnique (in MathEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::MathsTechnique(char *Name, GIP_IMAGE Src1, GIP_IMAGE Src2, GIP_IMAGE Target)
{
	if ((!m_pMath)|| (!Name) || (!Src1)  || (!Src2) || (!Target))
		return;
	if (!(*Src1)->SameDimensions(*Src2, *Target)) return;

	pDevice->CreateQuad((*Src1)->pTexture->m_nTexWidth, (*Src1)->pTexture->m_nTexHeight);
	m_pMath->Initialise();
	m_pMath->Apply(Name, (*Src1)->pTexture, (*Src2)->pTexture, (*Target)->pTexture);
}

//---------------------------------------------------------------------------------------------
//	Subtract (in MathEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::Subtract(GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target)
{
	if ((!m_pMath) || (!src1)  || (!src2) || (!target))
		return;
	if (!(*src1)->SameDimensions(*src2, *target)) return;

	pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
	m_pMath->Initialise();
	m_pMath->Apply("MathSub", (*src1)->pTexture, (*src2)->pTexture, (*target)->pTexture);
}

//---------------------------------------------------------------------------------------------
//	Diff(in MathEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::Diff(GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target)
{
	if ((!m_pMath) || (!src1)  || (!src2) || (!target))
		return;
	if (!(*src1)->SameDimensions(*src2, *target)) return;

	pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
	m_pMath->Initialise();
	m_pMath->Apply("MathDiff", (*src1)->pTexture, (*src2)->pTexture, (*target)->pTexture);
}

//---------------------------------------------------------------------------------------------
//	Add(in MathEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::Add(GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target)
{
	if ((!m_pMath) || (!src1)  || (!src2) || (!target))
		return;
	if (!(*src1)->SameDimensions(*src2, *target)) return;

	pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
	m_pMath->Initialise();
	m_pMath->Apply("MathAdd", (*src1)->pTexture, (*src2)->pTexture, (*target)->pTexture);
}

//---------------------------------------------------------------------------------------------
//	Multiply(in MathEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::Multiply(GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target)
{
	if ((!m_pMath) || (!src1)  || (!src2) || (!target))
		return;
	if (!(*src1)->SameDimensions(*src2, *target)) return;

	pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
	m_pMath->Initialise();
	m_pMath->Apply("MathMul", (*src1)->pTexture, (*src2)->pTexture, (*target)->pTexture);
}

//---------------------------------------------------------------------------------------------
//	Divide(in MathEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::Divide(GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target)
{
	if ((!m_pMath) || (!src1)  || (!src2) || (!target))
		return;
	if (!(*src1)->SameDimensions(*src2, *target)) return;

	pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
	m_pMath->Initialise();
	m_pMath->Apply("MathDiv", (*src1)->pTexture, (*src2)->pTexture, (*target)->pTexture);
}

//---------------------------------------------------------------------------------------------
//	Mod(in MathEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::Mod(GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target)
{
	if ((!m_pMath) || (!src1)  || (!src2) || (!target))
		return;
	if (!(*src1)->SameDimensions(*src2, *target)) return;

	pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
	m_pMath->Initialise();
	m_pMath->Apply("MathMod", (*src1)->pTexture, (*src2)->pTexture, (*target)->pTexture);
}

//---------------------------------------------------------------------------------------------
//	SubtractConst (in MathEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::SubtractConst (GIP_IMAGE src1, float val, GIP_IMAGE target)
{
	if ((!m_pMath) || (!src1) || (!target))
		return;
	if (!(*src1)->SameDimensions(*target)) return;

	pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
	m_pMath->Initialise();
	m_pMath->Apply("MathSubConst", (*src1)->pTexture, val, (*target)->pTexture);
}

//---------------------------------------------------------------------------------------------
//	Add(in MathEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::AddConst (GIP_IMAGE src1, float val, GIP_IMAGE target)
{
	if ((!m_pMath) || (!src1) || (!target))
		return;
	if (!(*src1)->SameDimensions(*target)) return;

	pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
	m_pMath->Initialise();
	m_pMath->Apply("MathAddConst", (*src1)->pTexture, val, (*target)->pTexture);
}

//---------------------------------------------------------------------------------------------
//	Multiply(in MathEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::MultiplyConst (GIP_IMAGE src1, float val, GIP_IMAGE target)
{
	if ((!m_pMath) || (!src1) || (!target))
		return;
	if (!(*src1)->SameDimensions(*target)) return;

	pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
	m_pMath->Initialise();
	m_pMath->Apply("MathMulConst", (*src1)->pTexture, val, (*target)->pTexture);
}

//---------------------------------------------------------------------------------------------
//	Divide(in MathEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::DivideConst (GIP_IMAGE src1, float val, GIP_IMAGE target)
{
	if ((!m_pMath) || (!src1) || (!target))
		return;
	if (!(*src1)->SameDimensions(*target)) return;

	pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
	m_pMath->Initialise();
	m_pMath->Apply("MathDivConst", (*src1)->pTexture, val, (*target)->pTexture);
}

//---------------------------------------------------------------------------------------------
//	Copy(in MathEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::Copy (GIP_IMAGE src1, GIP_IMAGE target)
{
	long	width, height, overl, bpp;
	long	width1, height1, overl1, bpp1;
	if ((!m_pMath) || (!src1) || (!target)) return;
	(*src1)->GetImageDimensions(&width, &height, &overl, &bpp);
	(*target)->GetImageDimensions(&width1, &height1, &overl1, &bpp1);
	if ((width != width1) || (height != height1) || (overl != overl1)) return;
	if ((((bpp==8) || (bpp==16)) && (bpp1 != 8) && (bpp1 != 16)) ||
	   (((bpp==24) || (bpp==48)) && (bpp1 != 24) && (bpp1 != 48))) return;
	pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
	m_pMath->Initialise();
	m_pMath->Apply("MathCopy", (*src1)->pTexture, (*target)->pTexture);
}

//---------------------------------------------------------------------------------------------
//	ComponentThreshold(in ComponentEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::ComponentThreshold(float Rmin, float  Rmax, float  Gmin, float  Gmax, float  Bmin, float  Bmax, 
							 GIP_IMAGE src, GIP_IMAGE target)
{

	if ((!m_pComponent) || (!src) || (!target)) return;
	if (!(*src)->SameDimensions(*target)) return;

	if (m_pComponent->Initialise((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight))
	{
		// setup display quad
		pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pComponent->ApplyThreshold(Rmin, Rmax, Gmin, Gmax, Bmin, Bmax, (*src)->pTexture, (*target)->pTexture);
	}
 }

//---------------------------------------------------------------------------------------------
//	ComponentSplit (in ComponentEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::ComponentSplit(GIP_IMAGE src, GIP_IMAGE target1, GIP_IMAGE target2, GIP_IMAGE target3)
{
	if ((!m_pComponent) || (!src) || (!target1) || (!target2)|| (!target3)) return;
	if (!(*src)->SameDimensions(*target1, *target2, *target3)) return;

	if (m_pComponent->Initialise((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight))
	{
		// setup display quad
		pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pComponent->ApplySplit((*src)->pTexture, (*target1)->pTexture, (*target2)->pTexture, (*target3)->pTexture);
	}
}

//---------------------------------------------------------------------------------------------
//	ComponentJoin (in ComponentEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::ComponentJoin(GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE src3, GIP_IMAGE target)
{
	if ((!m_pComponent) || (!src1) || (!scr2) || (!src3) || (!target)) return;
	if (!(*src1)->SameDimensions(*src2, *src3, *target)) return;

	if (m_pComponent->Initialise((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight))
	{
		// setup display quad
		pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
		m_pComponent->ApplyJoin((*src1)->pTexture, (*src2)->pTexture, (*src3)->pTexture, (*target)->pTexture);
	}
}

//---------------------------------------------------------------------------------------------
//	ComponentBlend (in ComponentEffect.fx)
//---------------------------------------------------------------------------------------------
void GIP::ComponentBlend(GIP_IMAGE src1, float a1, GIP_IMAGE src2, float a2, GIP_IMAGE src3, float a3, GIP_IMAGE target)
{
	if ((!m_pComponent) || (!src1) || (!scr2) || (!src3) || (!target)) return;
	if (!(*src1)->SameDimensions(*src2, *src3, *target)) return;

	if (m_pComponent->Initialise((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight))
	{
		// setup display quad
		pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
		m_pComponent->ApplyBlend((*src1)->pTexture, a1, (*src2)->pTexture, a2, (*src3)->pTexture, a3, (*target)->pTexture);
	}
}

//---------------------------------------------------------------------------------------------
//	RGBHSI (in ColourConv.fx)
//---------------------------------------------------------------------------------------------
void GIP::RGBHSI(GIP_IMAGE src, GIP_IMAGE dest)
{
	if ((!m_pColourConv) || (!src) || (!dest)) return;
	if (!(*src)->SameDimensions(*dest)) return;

	if (m_pColourConv->Initialise())
	{
		// setup display quad
		pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pColourConv->Apply("RGBHSI", (*src)->pTexture, (*dest)->pTexture);
	}
}

//---------------------------------------------------------------------------------------------
//	HSIRGB (in ColourConv.fx)
//---------------------------------------------------------------------------------------------
void GIP::HSIRGB(GIP_IMAGE src, GIP_IMAGE dest)
{
	if ((!m_pColourConv) || (!src) || (!dest)) return;
	if (!(*src)->SameDimensions(*dest)) return;

	if (m_pColourConv->Initialise())
	{
		// setup display quad
		pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pColourConv->Apply("HSIRGB", (*src)->pTexture, (*dest)->pTexture);
	}
}

//---------------------------------------------------------------------------------------------
//	Thresh (in Threshold.fx)
//---------------------------------------------------------------------------------------------

void GIP::Thresh(GIP_IMAGE src,float thresh, GIP_IMAGE target)
{
	if ((!m_pThreshold) || (!src) || (!target)) return;
	if (!(*src)->SameDimensions(*target)) return;

	if (m_pThreshold->Initialise(thresh))
	{
		// setup display quad
		pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pThreshold->Apply((*src)->pTexture, (*target)->pTexture);
	}
}

//---------------------------------------------------------------------------------------------
//	GaussianBlur (in gauss.fx)
//---------------------------------------------------------------------------------------------
void GIP::GaussianBlur(int FilterHW, float Sigma, GIP_IMAGE src, GIP_IMAGE dest)
{
    CreateScratch(&m_pScratch1, src);

    if (m_pScratch1)
	{
		pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pGauss->Initialise(FilterHW, Sigma, (*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
        m_pGauss->Apply((*src)->pTexture, (*dest)->pTexture, m_pScratch1->pTexture);
	}
}


//---------------------------------------------------------------------------------------------
//	UnsharpMask (in gauss.fx)
//---------------------------------------------------------------------------------------------
void GIP::UnsharpMask(int FilterHW, float Sigma, float Amount, GIP_IMAGE src, GIP_IMAGE dest)
{
    CreateScratch(&m_pScratch1, src);
    CreateScratch(&m_pScratch2, src);

    if ((m_pScratch1) && (m_pScratch2))
	{
		pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pGauss->Initialise(FilterHW, Sigma, (*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pGauss->ApplyUnsharpMask((*src)->pTexture, (*dest)->pTexture, m_pScratch1->pTexture, 
								   m_pScratch2->pTexture, Amount); 
	}
}

//---------------------------------------------------------------------------------------------
// Difference of gaussians
//---------------------------------------------------------------------------------------------

void GIP::DOG(int Size1, int Size2, GIP_IMAGE src, GIP_IMAGE dest)
{
    CreateScratch(&m_pScratch1, src);
    CreateScratch(&m_pScratch2, src);
	CreateScratch(&m_pScratch3, src);

    if ((m_pScratch1) && (m_pScratch2))
	{
		pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pGauss->Initialise(Size2, Size2, (*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pGauss->Apply((*src)->pTexture, m_pScratch2->pTexture, m_pScratch1->pTexture);
		m_pGauss->Initialise(Size1, Size1, (*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pGauss->Apply((*src)->pTexture, m_pScratch3->pTexture, m_pScratch1->pTexture);
		Subtract(&m_pScratch2, &m_pScratch3, dest);
	}
}


//---------------------------------------------------------------------------------------------
//	NearestNeigbourDeconv Part (in gauss.fx)
//---------------------------------------------------------------------------------------------
void GIP::NearestNeigbourDeconv(GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE src3, GIP_IMAGE dest, float Amount)
{
	if ((!m_pGauss) || (!src1) || (!src2) || (!src3) || (!dest)) return;
	if (!(*src1)->SameDimensions(*src2, *src3, *dest)) return;
	pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
	m_pGauss->Initialise();
	m_pGauss->ApplyNearestNeighbourDeconv((*src1)->pTexture, (*src2)->pTexture, (*src3)->pTexture, 
									(*dest)->pTexture, Amount); 
}

//---------------------------------------------------------------------------------------------
//	MedianGIP (in median.fx)
//---------------------------------------------------------------------------------------------
void GIP::Median(int nPasses, GIP_IMAGE src, GIP_IMAGE target)
{
	if (!m_pMedian) 
        return;
	
    if ((!src) || (!target) || (nPasses <= 0)) 
        return;
	
    if (!(*src)->SameDimensions(*target)) 
        return;

	if (m_pMedian->Initialise((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight))
	{
        CreateScratch(&m_pScratch1, src);
        CreateScratch(&m_pScratch2, src);

		// setup display quad
		pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pMedian->Apply(nPasses, src, target, &m_pScratch1, &m_pScratch2);
	}
}

//---------------------------------------------------------------------------------------------
//	Kuwahara (in Kuwahara.fx)
//---------------------------------------------------------------------------------------------
void GIP::Kuwahara(int FilterHW, GIP_IMAGE src, GIP_IMAGE dest)
{
    CreateScratch(&m_pScratch1, src);

    if (m_pScratch1)
	{
		pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pKuwahara->Initialise(FilterHW, (*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pKuwahara->Apply((*src)->pTexture, (*dest)->pTexture, m_pScratch1->pTexture);
	}
}

//---------------------------------------------------------------------------------------------
// Standard convolution
//---------------------------------------------------------------------------------------------

void GIP::Convolve(long nPasses, int FilterHW, float *pCoeff, GIP_IMAGE src, GIP_IMAGE dest)
{
	if ((!src) || (FilterHW<1) || (!dest) || (nPasses <= 0)) 
        return;
	
    CreateScratch(&m_pScratch1, src);
    CreateScratch(&m_pScratch2, src);

    pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
    m_pConvolution->Initialise(FilterHW, (*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
    m_pConvolution->FilterCoefficients(pCoeff);
    
    LPCTSTR TechName;
    if (FilterHW == 1)
        TechName = "Convolve3by3";
    else
        TechName = "ConvolveNbyN";

	m_pConvolution->Apply(TechName, nPasses, src, dest, &m_pScratch1, &m_pScratch2);
}

void GIP::ConvolveTechnique(LPCTSTR Name, long nPasses, int FilterHW, float *pCoeff, GIP_IMAGE src, GIP_IMAGE dest)
{
    if ((!src) || (FilterHW<1) || (!dest) || (nPasses <= 0)) 
        return;

    CreateScratch(&m_pScratch1, src);
    CreateScratch(&m_pScratch2, src);

    pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
    m_pConvolution->Initialise(FilterHW, (*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
    m_pConvolution->FilterCoefficients(pCoeff);
    m_pConvolution->Apply(Name, nPasses, src, dest, &m_pScratch1, &m_pScratch2);
}

//---------------------------------------------------------------------------------------------
//	Test - Combination of Kuwahara and Convolution
//---------------------------------------------------------------------------------------------
void GIP::Test(GIP_IMAGE src, GIP_IMAGE dest)
{
	pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
#if 0
    float F[49];
    int i;

    for (i = 0; i < 49; i++)
        F[i] = -1.0f;

    F[24] = 48.0f;
    
    m_pConvolution->Initialise(3, (*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
    m_pConvolution->FilterCoefficients(&F[0]);
	m_pConvolution->Apply(1, (*src)->pTexture, (*dest)->pTexture->Surface());
#else
	long Width, Height, Overlap, Bpp;

	(*src)->GetImageDimensions(&Width, &Height, &Overlap, &Bpp);
	GIPImage *scratch= new GIPImage(pd3dDevice);
	if (scratch->Create(Width, Height, Overlap, Bpp))
	{
		m_pKuwahara->Initialise(2, (*src)->pTexture->m_nImageWidth, (*src)->pTexture->m_nImageHeight);
		m_pKuwahara->Apply((*src)->pTexture, (*dest)->pTexture, scratch->pTexture);
	}
	if (scratch != NULL) delete scratch;
#endif
}

void GIP::ApplyTechnique(char *name, GIP_IMAGE src, GIP_IMAGE target, BOOL SetPixOffsets /*= FALSE*/)
{
	UINT cPasses;
    HRESULT hr;

	if ((!name) || (!m_pGipEffect) || (!src) || (!target))
		return;

	if (!(*src)->SameDimensions(*target)) return;

    ID3DXEffect *pEffect = m_pGipEffect->Effect();

	pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
	pEffect->SetFloat( "TextureWidth", (float)(*src)->pTexture->m_nTexWidth);
	pEffect->SetFloat( "TextureHeight", (float)(*src)->pTexture->m_nTexHeight);

	// set source image 
	hr = pEffect->SetTexture("SourceImageTexture", (*src)->pTexture->Texture());

	// set render target texture
	hr = pd3dDevice->SetRenderTarget( 0, (*target)->pTexture->Surface());

	hr = pEffect->SetTechnique(name);

	// Apply the technique contained in the effect - returns num of passes in cPasses
	hr = pEffect->Begin(&cPasses, 0);

	hr = pEffect->BeginPass(0);	// only one pass  - pass 0

    if (SetPixOffsets)
          hr = m_pGipEffect->SetPixelOffsetConstants((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);

	// The effect interface queues up the changes and performs them 
	// with the CommitChanges call. You do not need to call CommitChanges if 
	// you are not setting any parameters between the BeginPass and EndPass.
	hr = pEffect->CommitChanges();

	// Render the simple quad with the applied technique
	pDevice->DrawQuad();

	// end tecnnique pass
	hr = pEffect->EndPass();

	// end technique
	hr = pEffect->End();
}

//---------------------------------------------------------------------------------------------

void GIP::ApplyColourTechnique(char *name, GIP_IMAGE src, GIP_IMAGE target, BOOL SetPixOffsets /*= FALSE*/)
{
	UINT cPasses;
    HRESULT hr;

	if ((!name) || (!m_pGipEffect) || (!src) || (!target))
		return;

	if (!(*src)->SameDimensions(*target)) return;

    ID3DXEffect *pEffect = m_pColourConv->Effect();

	pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
	pEffect->SetFloat( "TextureWidth", (float)(*src)->pTexture->m_nTexWidth);
	pEffect->SetFloat( "TextureHeight", (float)(*src)->pTexture->m_nTexHeight);

	// set source image 
	hr = pEffect->SetTexture("SourceImageTexture", (*src)->pTexture->Texture());

	// set render target texture
	hr = pd3dDevice->SetRenderTarget( 0, (*target)->pTexture->Surface());

	hr = pEffect->SetTechnique(name);

	// Apply the technique contained in the effect - returns num of passes in cPasses
	hr = pEffect->Begin(&cPasses, 0);

	hr = pEffect->BeginPass(0);	// only one pass  - pass 0

    if (SetPixOffsets)
          hr = m_pGipEffect->SetPixelOffsetConstants((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);

	// The effect interface queues up the changes and performs them 
	// with the CommitChanges call. You do not need to call CommitChanges if 
	// you are not setting any parameters between the BeginPass and EndPass.
	hr = pEffect->CommitChanges();

	// Render the simple quad with the applied technique
	pDevice->DrawQuad();

	// end tecnnique pass
	hr = pEffect->EndPass();

	// end technique
	hr = pEffect->End();
}


void GIP::ApplyColourTechnique(char *name, GIP_IMAGE src, GIP_IMAGE target, float A, float B, float C, float D)
{
	UINT cPasses;
    HRESULT hr;

	if ((!name) || (!m_pGipEffect) || (!src) || (!target))
		return;

	if (!(*src)->SameDimensions(*target)) return;

    ID3DXEffect *pEffect = m_pColourConv->Effect();

	pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
	pEffect->SetFloat( "TextureWidth", (float)(*src)->pTexture->m_nTexWidth);
	pEffect->SetFloat( "TextureHeight", (float)(*src)->pTexture->m_nTexHeight);

	// set source image 
	hr = pEffect->SetTexture("SourceImageTexture", (*src)->pTexture->Texture());

	// set render target texture
	hr = pd3dDevice->SetRenderTarget(0, (*target)->pTexture->Surface());

	hr = pEffect->SetTechnique(name);

	// Apply the technique contained in the effect - returns num of passes in cPasses
	hr = pEffect->Begin(&cPasses, 0);

	hr = pEffect->BeginPass(0);	// only one pass  - pass 0

	hr = m_pGipEffect->SetRegister(10, A, B, C, D);

	// The effect interface queues up the changes and performs them 
	// with the CommitChanges call. You do not need to call CommitChanges if 
	// you are not setting any parameters between the BeginPass and EndPass.
	hr = pEffect->CommitChanges();

	// Render the simple quad with the applied technique
	pDevice->DrawQuad();

	// end tecnnique pass
	hr = pEffect->EndPass();

	// end technique
	hr = pEffect->End();
}

void GIP::ApplyColourTechnique(char *name, GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target, float A, float B, float C, float D)
{
	UINT cPasses;
    HRESULT hr;

	if ((!name) || (!m_pGipEffect) || (!src1) || (!target) || (!src2))
		return;

	if (!(*src1)->SameDimensions(*target)) 
		return;

	if (!(*src2)->SameDimensions(*target)) 
		return;

    ID3DXEffect *pEffect = m_pColourConv->Effect();

	pDevice->CreateQuad((*src1)->pTexture->m_nTexWidth, (*src1)->pTexture->m_nTexHeight);
	pEffect->SetFloat( "TextureWidth", (float)(*src1)->pTexture->m_nTexWidth);
	pEffect->SetFloat( "TextureHeight", (float)(*src1)->pTexture->m_nTexHeight);

	// set source images
	hr = pEffect->SetTexture("Src1ImageTexture", (*src1)->pTexture->Texture());
	hr = pEffect->SetTexture("Src2ImageTexture", (*src2)->pTexture->Texture());

	// set render target texture
	hr = pd3dDevice->SetRenderTarget(0, (*target)->pTexture->Surface());

	hr = pEffect->SetTechnique(name);

	// Apply the technique contained in the effect - returns num of passes in cPasses
	hr = pEffect->Begin(&cPasses, 0);

	hr = pEffect->BeginPass(0);	// only one pass  - pass 0

	hr = m_pGipEffect->SetRegister(10, A, B, C, D);

	// The effect interface queues up the changes and performs them 
	// with the CommitChanges call. You do not need to call CommitChanges if 
	// you are not setting any parameters between the BeginPass and EndPass.
	hr = pEffect->CommitChanges();

	// Render the simple quad with the applied technique
	pDevice->DrawQuad();

	// end tecnnique pass
	hr = pEffect->EndPass();

	// end technique
	hr = pEffect->End();
}

// Perform a multipass technique on an image
void GIP::MultipassTechnique(char *Name, GIP_IMAGE src, GIP_IMAGE target, BOOL SetPixOffsets /*= FALSE*/)
{
	if ((!Name) || (!m_pGipEffect) || (!src) || (!target))
		return;

    ID3DXEffect *pEffect = m_pGipEffect->Effect();

	pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
	pEffect->SetFloat( "TextureWidth", (float)(*src)->pTexture->m_nTexWidth);
	pEffect->SetFloat( "TextureHeight", (float)(*src)->pTexture->m_nTexHeight);

    CreateScratch(&m_pScratch1, src);
	if (m_pScratch1)
	{
		CreateScratch(&m_pScratch2, src);
		if (m_pScratch2)
		{
			m_pGipEffect->MultipassTechnique(Name, (*src)->pTexture, (*target)->pTexture, 
											 m_pScratch1->pTexture, m_pScratch2->pTexture, FALSE, 0, 0, NULL);
		}
	}
}


// Ping - Pong two techniques on alternate passes
void GIP::ColourPong(char *pass1, char *pass2, int nPasses, GIP_IMAGE src, GIP_IMAGE target)
{
	if ((!m_pGipEffect) || (!src) || (!target) || (nPasses <= 0))
		return;
	ID3DXEffect *pEff;

	pEff = m_pGipEffect->Effect();
	pEff->SetFloat( "TextureWidth", (float)(*src)->pTexture->m_nTexWidth);
	pEff->SetFloat( "TextureHeight", (float)(*src)->pTexture->m_nTexHeight);

	// setup display quad
	pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);

    CreateScratch(&m_pScratch1, src);
	if (m_pScratch1)
	{
		CreateScratch(&m_pScratch2, src);
		if (m_pScratch2)
		{
			m_pGipEffect->PingPong(pass1, pass2, nPasses, (*src)->pTexture, (*target)->pTexture, 
                                   m_pScratch1->pTexture, m_pScratch2->pTexture);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
//	Histogram
//	- Histogram entrypoint for HistogramGIP
///////////////////////////////////////////////////////////////////////////////////////////////

void GIP::Histogram(GIP_IMAGE src, long *hist, int ColourMode)
{
	BYTE			*p, *pImageData;
	unsigned short	*s;
	long			w, h, overl, bpp, i;

	if ((!src)) return;
	(*src)->GetImageDimensions(&w, &h, &overl, &bpp);
	if (bpp==8)
	{
        memset(hist, 0, sizeof(long) * 256);
		pImageData = new BYTE[w*h];
		GetImage(src, pImageData);
		p=pImageData;
		for (i=0; i<w*h; i++)
			hist[*p++] +=1;
	}
	else if (bpp==16)
	{
        memset(hist, 0, sizeof(long) * 65536);
		pImageData = new BYTE[w*h*2];
		GetImage(src, (unsigned short *)pImageData);
		s=(unsigned short *)pImageData;
		for (i=0; i<w*h; i++)
			hist[*s++] +=1;
	}
	else if (bpp==24)
	{
        memset(hist, 0, sizeof(long) * 256);
		pImageData = new BYTE[w*h*3L];
		GetImage(src, (RGBTRIPLE *)pImageData);
		p=pImageData;
		for (i=0; i<w*h; i++)
		{
			if(ColourMode& 1) hist[*(p+2)] +=1;
			if(ColourMode& 2) hist[*(p+1)] +=1;
			if(ColourMode& 4) hist[*p] +=1;
			p +=3;
		}
	}
	else if (bpp==48)
	{
        memset(hist, 0, sizeof(long) * 65536);
		pImageData = new BYTE[w*h*6L];
		GetImage(src, (RGB16BTRIPLE *)pImageData);
		s=(unsigned short *)pImageData;
		for (i=0; i<w*h; i++)
		{
			if(ColourMode& 1) hist[*(s+2)] +=1;
			if(ColourMode& 2) hist[*(s+1)] +=1;
			if(ColourMode& 4) hist[*s] +=1;
			s +=3;
		}
	}
	delete [] pImageData;
}


//---------------------------------------------------------------------------------------------
//	NNDeconv
//---------------------------------------------------------------------------------------------


void GIP::NNDeconv(int FilterHW, float Sigma, float Amount, int nrImages, int W, int H, int BPP, BYTE **ppImages, BYTE **ppDestImages)
{
	GIP_IMAGE src0, src1, src2, filsrc0, filsrc1, filsrc2, dest;

	int overlap=FilterHW;
	if ((BPP==24) || (BPP==48)) overlap=0;
	// create GPU images for processing
	src0=GIPCreateImage(W, H, overlap, BPP);
	src1=GIPCreateImage(W, H, overlap, BPP);
	src2=GIPCreateImage(W, H, overlap, BPP);
	if (FilterHW > 0)
	{
		filsrc0=GIPCreateImage(W, H, overlap, BPP);
		filsrc1=GIPCreateImage(W, H, overlap, BPP);
		filsrc2=GIPCreateImage(W, H, overlap, BPP);
	}
	dest=GIPCreateImage(W, H, overlap, BPP);
	int idx=0;
	for (int i=0; i<nrImages; i++)
	{
		if (i==0)
		{
			switch (BPP)
			{
				case 8:
						PutImage(src0, (BYTE *)ppImages[i]);
						break;
				case 16:
						PutImage(src0, (unsigned short *)ppImages[i]);
						break;
				case 24:
						PutImage(src0, (RGBTRIPLE *)ppImages[i]);
						break;
				case 48:
						PutImage(src0, (RGB16BTRIPLE *)ppImages[i]);
						break;
			}
			if (FilterHW>0) GaussianBlur(FilterHW, Sigma, src0, filsrc0);
		}
		if (i<(nrImages-1))
		{
			switch (BPP)
			{
				case 8:
						switch (idx)
						{
							case 0:
									PutImage(src1, (BYTE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src1, filsrc1);
									break;
							case 1:
									PutImage(src2, (BYTE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src2, filsrc2);
									break;
							case 2:
									PutImage(src0, (BYTE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src0, filsrc0);
									break;
						}
						break;
				case 16:
						switch (idx)
						{
							case 0:
									PutImage(src1, (unsigned short *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src1, filsrc1);
									break;
							case 1:
									PutImage(src2, (unsigned short *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src2, filsrc2);
									break;
							case 2:
									PutImage(src0, (unsigned short *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src0, filsrc0);
									break;
						}
						break;
				case 24:
						switch (idx)
						{
							case 0:
									PutImage(src1, (RGBTRIPLE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src1, filsrc1);
									break;
							case 1:
									PutImage(src2, (RGBTRIPLE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src2, filsrc2);
									break;
							case 2:
									PutImage(src0, (RGBTRIPLE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src0, filsrc0);
									break;
						}
						break;
				case 48:
						switch (idx)
						{
							case 0:
									PutImage(src1, (RGB16BTRIPLE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src1, filsrc1);
									break;
							case 1:
									PutImage(src2, (RGB16BTRIPLE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src2, filsrc2);
									break;
							case 2:
									PutImage(src0, (RGB16BTRIPLE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src0, filsrc0);
									break;
						}
						break;
			}
		}
		if (i==0)
		{
			if (FilterHW > 0) NearestNeigbourDeconv(src0, filsrc1, filsrc1, dest, Amount);
				else NearestNeigbourDeconv(src0, src1, src1, dest, Amount);
		}
		else if (i==(nrImages-1))
		{
			switch (idx)
			{
				case 0:
						if (FilterHW > 0) NearestNeigbourDeconv(src0, filsrc2, filsrc2, dest, Amount);
							else NearestNeigbourDeconv(src0, src2, src2, dest, Amount);
						break;
				case 1:
						if (FilterHW > 0) NearestNeigbourDeconv(src1, filsrc0, filsrc0, dest, Amount);
							else NearestNeigbourDeconv(src1, src0, src0, dest, Amount);
						break;
				case 2:
						if (FilterHW > 0) NearestNeigbourDeconv(src2, filsrc1, filsrc1, dest, Amount);
							else NearestNeigbourDeconv(src2, src1, src1, dest, Amount);
						break;
			}
		}
		else
		{
			switch (idx)
			{
				case 0:
						if (FilterHW > 0) NearestNeigbourDeconv(src0, filsrc2, filsrc1, dest, Amount);
							else NearestNeigbourDeconv(src0, src2, src1, dest, Amount);
						break;
				case 1:
						if (FilterHW > 0) NearestNeigbourDeconv(src1, filsrc0, filsrc2, dest, Amount);
							else NearestNeigbourDeconv(src1, src0, src2, dest, Amount);
						break;
				case 2:
						if (FilterHW > 0) NearestNeigbourDeconv(src2, filsrc1, filsrc0, dest, Amount);
							else NearestNeigbourDeconv(src2, src1, src0, dest, Amount);
						break;
			}
		}
		// save image
		switch (BPP)
		{
			case 8:
					GetImage(dest, (BYTE *)ppDestImages[i]);
					break;
			case 16:
					GetImage(dest, (unsigned short *)ppDestImages[i]);
					break;
			case 24:
					GetImage(dest, (RGBTRIPLE *)ppDestImages[i]);
					break;
			case 48:
					GetImage(dest, (RGB16BTRIPLE *)ppDestImages[i]);
					break;
		}
		idx +=1;
		if (idx==3) idx=0;
	}
	GIPReleaseImage(src0);
	GIPReleaseImage(src1);
	GIPReleaseImage(src2);
	if (FilterHW > 0)
	{
		GIPReleaseImage(filsrc0);
		GIPReleaseImage(filsrc1);
		GIPReleaseImage(filsrc2);
	}
	GIPReleaseImage(dest);
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
// Perform Local Binary Pattern texture analysis on grey image, generating only uniform LBP codes
// P=8,R=radius 
// Recommend you call this with radius=4, noiseThresh=16
//////////////////////////////////////////////////////////////////////////////////////////////////////

void GIP::LBP(GIP_IMAGE src, GIP_IMAGE target, float radius, int noiseThresh)
{	
	if (!m_pGipEffect || !src || !target || !pd3dDevice)
		return;

	BYTE lut[256];

	// setup LUT to convert non-uniform LBP code to uniform LBP code (only 9 possibilities)
	lut[0] = 0;
	lut[255] = 8;

	for (int i=1;i<255;i++)
	{
		// get minimum rotated value
		BYTE val = (BYTE)i;
		BYTE min = val;
		for (int j=0;j<8;j++)
		{
			// perform ROR on val
			BYTE top = val & 1;
			val = (val >> 1) | (top << 7);
			if (val<min)
				min = val;
		}
		//compare against allowed versions
		int index;

		switch (min)
		{
		case 0:
			index = 0;
			break;
		case 1:
			index = 1;
			break;
		case 3:
			index = 2;
			break;
		case 7:
			index = 3;
			break;
		case 15:
			index = 4;
			break;
		case 31:
			index = 5;
			break;
		case 63:
			index = 6;
			break;
		case 127:
			index = 7;
			break;
		default:
			index = 9;
		}

		lut[i] = index;
	}

	GIPSwapTexture *pSwapLutTex = new GIPSwapTexture(pd3dDevice);
	GIPTexture *pLutTex = new GIPTexture(pd3dDevice);

    // NOTE We are doing a two pass separable version of a gaussian filter. Therefore we only need to create
    // a single dimensional filter Size by 1 elements. 
	// load lut into display lut texture - ready for pixel shader
	if (!pSwapLutTex->Create(256, 1))        
		return;
	if (!pLutTex->Create(256, 1))
		return;
	pSwapLutTex->Put(lut);

	pSwapLutTex->Transmit(pLutTex);

	// setup display quad
	pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);

	ID3DXEffect *pEffect = m_pGipEffect->Effect();

	HRESULT hr;

	// pass data to effect
	hr = pEffect->SetTexture("LBPLutTexture", pLutTex->Texture());
	hr = pEffect->SetFloat("LBPradius",(float)radius);
	hr = pEffect->SetFloat("LBPnoiseThreshold",(float)noiseThresh);
	hr = pEffect->SetFloat( "TextureWidth", (float)(*src)->pTexture->m_nTexWidth);
	hr = pEffect->SetFloat( "TextureHeight", (float)(*src)->pTexture->m_nTexHeight);

	// set source image for first pass
	pEffect->SetTexture( "SourceImageTexture", (*src)->pTexture->Texture());

	pEffect->SetTechnique("LBP");

	pd3dDevice->SetRenderTarget( 0, (*target)->pTexture->Surface());

	UINT Passes;

	// Apply the technique contained in the effect - returns num of passes in cPasses
	pEffect->Begin(&Passes, 0);

	pEffect->BeginPass(0);	// only one pass in each morph techniqe - pass 0
	pEffect->CommitChanges();
	pDevice->DrawQuad();

	pEffect->EndPass();
	pEffect->End();

	delete pSwapLutTex;
	delete pLutTex;
}

//---------------------------------------------------------------------------------------------


