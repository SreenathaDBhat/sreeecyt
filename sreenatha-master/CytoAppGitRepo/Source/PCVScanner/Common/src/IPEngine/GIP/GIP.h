
#ifndef GIP_H
#define GIP_H

#ifdef DEBUG_PS 
#pragma message("DEBUG PIXEL SHADERS")
#ifndef D3D_DEBUG_INFO
    #define D3D_DEBUG_INFO
#endif
#endif
#ifdef DEBUG_VS
#pragma message("DEBUG VERTEX SHADERS")
#ifndef D3D_DEBUG_INFO
    #define D3D_DEBUG_INFO
#endif
#endif
 
#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>


void D3DEXPLAIN(HRESULT hr);

// number of image buffers
const int GIP_MAXIMAGES = 16;				// max number of images - nImages is number actually allocated
const int GIP_MAXCOLIMAGES = 6;             // max number of colour images
const int GIP_SWAPBUFFERS = 16;				// swap buffers required for morphological ops
const int GIP_USERIMAGES = GIP_MAXIMAGES-2;	// available user images
const int GIP_NULLBUFFER = 0xFFFFFFFF;      // empty image buffer
#define IN_GREY		0
#define IN_COLOUR	1

// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif

#pragma warning(disable: 4251)
#include "gipeffect.h"
#include "gipfilter.h"
#include "GIPGaussian.h"
#include "GIPKuwahara.h"
#include "giptexture.h"
#include "gipdevice.h"
#include "GIPImage.h"
#include "GIPMath.h"
#include "GIPMorph.h"
#include "GIPComponent.h"
#include "GIPColourConv.h"
#include "GIPThreshold.h"
#include "GIPMedian.h"
#include "GIPBlend.h"
#include "GIPColourDeconv.h"
#include "GIPFFT.h"
#include "GIPEDM.h"
#include "GIPHough.h"

typedef GIPImage**		GIP_IMAGE;

void D3DEXPLAIN(HRESULT hr);

class DllExport GIP
{
public:
    typedef enum
    {
        Empty,          // Nothing here
        Released,       // Texture allocated but free for use
        InUse           // Texture in use
    } GIPImageStatus;

    typedef struct
    {
        GIPImage        *pImage;        // Pointer to the image texture
        GIPImageStatus  Status;         // Status of the texture
    } GIPStruct;

	// Construction
	GIP();
	~GIP();

	// Configuration
	BOOL Create(void);

	// Image transfer
	BOOL GetImage(GIP_IMAGE target, BYTE *pImageData);
    BOOL GetImage(GIP_IMAGE target, RGBTRIPLE *pImageData);
	BOOL GetImage(GIP_IMAGE target, unsigned short *pImageData);
    BOOL GetImage(GIP_IMAGE target, RGB16BTRIPLE *pImageData);
    BOOL GetImage(GIP_IMAGE target, RGBQUAD *pImageData);
	BOOL GetImage(GIP_IMAGE targetHandle, float *pImageData);
	BOOL GetImageComponent(GIP_IMAGE target, long component, BYTE *pImageData);
	BOOL GetImageComponent(GIP_IMAGE target, long component, unsigned short *pImageData);
	BOOL PutImage(GIP_IMAGE source, BYTE *pImageData);
	BOOL PutImage(GIP_IMAGE source, RGBTRIPLE *pImageData);
	BOOL PutImage(GIP_IMAGE source, unsigned short *pImageData);
	BOOL PutImage(GIP_IMAGE source, RGB16BTRIPLE *pImageData);
	BOOL PutImage(GIP_IMAGE sourceHandle, BYTE *pImageData, long w, long h, long x0, long y0);
	BOOL PutImage(GIP_IMAGE sourceHandle, RGBTRIPLE *pImageData, long w, long h, long x0, long y0, long ColourMode);
	BOOL PutImage(GIP_IMAGE sourceHandle, unsigned short *pImageData, long w, long h, long x0, long y0);
	BOOL PutImage(GIP_IMAGE sourceHandle, RGB16BTRIPLE *pImageData, long w, long h, long x0, long y0, long ColourMode);
   
	// Start and end of image processing sequence
	void BeginIP();
	void EndIP();

	// GipImages
	GIP_IMAGE GIPCreateImage(long Width, long Height, long Overlap, long BPP);
	void GIPFreeImage(GIP_IMAGE GipSource);
	void GIPReleaseImage(GIP_IMAGE GipSource);
	void GIPGetImageDimensions(GIP_IMAGE GIPSource, long *Width, long *Height, long *Overlap, long *BPP);
	GIPSwapTexture* GIP::GetSwapTexture(GIPImage *sourceHandle);
	void GIPReleaseAll(void);

	// Morphology
	void DilateAboveThresh(int nPasses, GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target, float Thresh);
	void Dilate(int nPasses, GIP_IMAGE src, GIP_IMAGE target);
	void Erode(int nPasses, GIP_IMAGE src, GIP_IMAGE target);
	void Open(int nPasses, GIP_IMAGE src, GIP_IMAGE target);
	void Close(int nPasses, GIP_IMAGE src, GIP_IMAGE target);

	// Math
	void Subtract(GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target);
	void Add(GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target);
	void Multiply(GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target);
	void Divide(GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target);
	void Diff(GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target);
	void Mod(GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target);
	void MathsTechnique(char *Name, GIP_IMAGE Src1, GIP_IMAGE Src2, GIP_IMAGE Target);
	void SubtractConst(GIP_IMAGE src1, float val, GIP_IMAGE target);
	void AddConst(GIP_IMAGE src1, float val, GIP_IMAGE target);
	void MultiplyConst(GIP_IMAGE src1, float val, GIP_IMAGE target);
	void DivideConst(GIP_IMAGE src1, float val, GIP_IMAGE target);
	void Copy(GIP_IMAGE src1, GIP_IMAGE target);
	GIPImage** ImageCrop(GIP_IMAGE src, long x0, long y0, long w0, long h0, long overl);
	GIPImage** ImageResize(GIP_IMAGE src, long w0, long h0, long overl);
	
	// Convolutions
	void Convolve(long nPasses, int FilterHW, float *Coeff, GIP_IMAGE src, GIP_IMAGE dest);
	void ConvolveTechnique(LPCTSTR Name, long nPasses, int FilterHW, float *Coeff, GIP_IMAGE src, GIP_IMAGE dest);

	// Thresholding
	void Thresh(GIP_IMAGE src, float thresh, GIP_IMAGE target);

	// Colour operations
	void RGBHSI(GIP_IMAGE src, GIP_IMAGE dest);
	void HSIRGB(GIP_IMAGE src, GIP_IMAGE dest);
	void ColourToGrey(GIP_IMAGE src, GIP_IMAGE dest);
	void GreyToColour(GIP_IMAGE src, GIP_IMAGE dest);

 	// Colour operations
	void Test(GIP_IMAGE src, GIP_IMAGE dest);

	// Ping - Pong two techniques on alternate passes
	void ColourPong(char *pass1, char *pass2, int nPasses, GIP_IMAGE src, GIP_IMAGE target);

	// ImageProcessing.fx
	void ApplyTechnique(char *name, GIP_IMAGE src, GIP_IMAGE target, BOOL SetPixOffsets = FALSE);

	// Apply a technique to a three component image
	// ColourConv.fx
	void ApplyColourTechnique(char *name, GIP_IMAGE src, GIP_IMAGE target, BOOL SetPixOffsets = FALSE);
	void ApplyColourTechnique(char *name, GIP_IMAGE src, GIP_IMAGE target, float A, float B, float C, float D);
	void ApplyColourTechnique(char *name, GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE target, float A, float B, float C, float D);

	// Apply a multipass technique (e.g. separable filter) to a three component image
	void MultipassTechnique(char *Pass, GIP_IMAGE src, GIP_IMAGE target, BOOL SetPixOffsets = FALSE);
    
	// Split an image and render each component into a different texture
	void ComponentSplit(GIP_IMAGE src, GIP_IMAGE target1, GIP_IMAGE target2, GIP_IMAGE target3);
	// Join several components to form a composite colour image
	void ComponentJoin(GIP_IMAGE src1, GIP_IMAGE src2, GIP_IMAGE src3, GIP_IMAGE target);
	// Blend three into 1 
	void ComponentBlend(GIP_IMAGE src1, float a1, GIP_IMAGE src2, float a2, GIP_IMAGE src3, float a3, GIP_IMAGE target);
	// Colour Thresholder
	void ComponentThreshold(float Rmin, float Rmax, float Gmin, float Gmax, float Bmin, float Bmax, GIP_IMAGE src1, GIP_IMAGE target);
 
	// Gaussian blur
	void GaussianBlur(int FliterHW, float Sigma, GIP_IMAGE src, GIP_IMAGE dest);

	// Difference of gaussians
	void DOG(int Size1, int Size2, GIP_IMAGE src, GIP_IMAGE dest);

	// Unsharp Mask
	void UnsharpMask(int FliterHW, float Sigma, float Amount, GIP_IMAGE src, GIP_IMAGE dest);

	// NearestNeighbourDeconv
	void NNDeconv(int FilterHW, float Sigma, float Amount, int nrImages, int W, int H, int BPP, BYTE **ppImages, BYTE **ppDestImages);
	void NearestNeigbourDeconv(GIP_IMAGE src, GIP_IMAGE src2, GIP_IMAGE src3, GIP_IMAGE dest, float Amount);

	// Kuwahara
	void Kuwahara(int FliterHW, GIP_IMAGE src, GIP_IMAGE dest);
	// Median Filter
	void Median(int nPasses, GIP_IMAGE src, GIP_IMAGE target);

	// Multiple Blend
	int GetMinValue(GIP_IMAGE src);
	
	void CalcModalHistogram(int nrImages, BYTE **pGipImages, int *pImageCorr);
	void SimpleBlend(int nrImages, BYTE **pImages, GIP_IMAGE Dest, BOOL corrModeHist=TRUE);
	void HDRBlend(int nrImages, BYTE **pImages, GIP_IMAGE Dest, BOOL corrModeHist=TRUE);
	void RangeBlend(int nrImages, BYTE **pImages, GIP_IMAGE Dest, BOOL corrModeHis=TRUE);

	BOOL BlendMultipleExposures(int BTechnique, int nrImages, BYTE **ppImages, int width, int height, int bpp, BYTE *pOutputImage);

	// Histogram
	void Histogram(GIP_IMAGE Src, long *pHist, int ColourMode);

	// Colour Deconvolution
	void ColourDeconv(GIP_IMAGE src, double *vc1, double *vc2, double *vc3, GIP_IMAGE dest);

	//FFT and FFT related
	GIPImage** fft2DPowerSpectrum(GIP_IMAGE src);
	GIPImage** fft2D(GIP_IMAGE src, long flag, int bppnew=24);
	GIPImage** fft2DButterWorth(GIP_IMAGE SrcHandle, long filtType, double f1, double f2, double order);
	GIPImage** fft2DGaussian(GIP_IMAGE SrcHandle, long filtType, double f1, double f2);
	void	   FFTImageRegistration(GIP_IMAGE Src1, GIP_IMAGE Src2, long *xshift, long *yshift, float *sn, long MaxShift);
	void	   CoreFFTImageRegistration(GIP_IMAGE FFTSrc1, GIP_IMAGE FFTSrc2, long *xshift, long *yshift, float *sn, long MaxShift);
	void	   FFTImageStitch(BYTE *Addr1, BYTE *Addr2, long Width, long Height, long mode, long MaxShift, long *xshift, 
								long *yshift, float *sn);
	void	   FFTImageStitch(unsigned short *Addr1, unsigned short *Addr2, long Width, long Height, long mode, long MaxShift, 
								long *xshift, long *yshift, float *sn);
	void	   FFTImageStitch(RGBTRIPLE *Addr1, RGBTRIPLE *Addr2, long Width, long Height, long mode, long MaxShift, long *xshift, 
								long *yshift, float *sn);
	void	   FFTImageStitch(RGB16BTRIPLE *Addr1, RGB16BTRIPLE *Addr2, long Width, long Height, long mode, long MaxShift, long *xshift, 
								long *yshift, float *sn);
	void	   FFTImageStitchGIP(GIP_IMAGE src1, GIP_IMAGE src2, long mode, long MaxShift, long *xshift, long *yshift, float *sn);

	// Eucledian Distance Map
	void DistanceMap(GIPImage** src, GIP_IMAGE dest, BOOL EDMOnly, BOOL IsWhiteBackground, float NormFactor=1.0);

	//Canny Edge Detection + Hough Transform
	void CannyEdge(GIP_IMAGE src, GIP_IMAGE dest, double thresholdLow, double thresholdHigh);
	void HoughLines(GIP_IMAGE src, GIP_IMAGE dest, char *angles, long minlength, long connection);

	// Local binary patterns
	void LBP(GIP_IMAGE src, GIP_IMAGE target, float radius, int noiseThresh);

private:
	void CreateScratch(GIP_IMAGE pScratch, GIP_IMAGE src);
	void CreateScratch(GIP_IMAGE pScratch, long Width, long Height, long Overlap, long Bpp);
	void Destroy();

	GIPDevice*				pDevice;						// Our D3D device
	IDirect3DDevice9*		pd3dDevice;
	GIPEffect*              m_pGipEffect;					// D3DX effect interface
	GIPMorph*				m_pMorph;
 	GIPMath*				m_pMath;
	GIPComponent*			m_pComponent;
	GIPColourConv*			m_pColourConv;
	GIPThreshold*			m_pThreshold;
  	GIPFilter*              m_pConvolution;
	GIPGaussian*            m_pGauss;
	GIPKuwahara*            m_pKuwahara;
	GIPMedian*				m_pMedian;
	GIPBlend*				m_pBlend;
	GIPColourDeconv*		m_pColourDeconv;
	GIPFFT*					m_pFFT;
	GIPEDM*					m_pEDM;
	GIPHough*				m_pHough;

	GIPStruct               m_Images[GIP_MAXIMAGES];

	GIPImage				*m_pScratch1;					// Scratch pad images
	GIPImage                *m_pScratch2;
	GIPImage                *m_pScratch3;
	GIPImage                *m_pScratch4;

	GIPSwapTexture*         pSwapTexture[GIP_SWAPBUFFERS];	// system side texture
	int						m_SwapWidth[GIP_SWAPBUFFERS];
	int						m_SwapHeight[GIP_SWAPBUFFERS];
	int						m_SwapBpp[GIP_SWAPBUFFERS];
	int						m_SwapOverlap[GIP_SWAPBUFFERS];
	long					m_SwapLastUsed[GIP_SWAPBUFFERS];
	long					m_SwapCounter;			
};

#include "gipcommon.h"

#pragma warning(default: 4251)

#endif // GIP_H



