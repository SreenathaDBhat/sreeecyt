///////////////////////////////////////////////////////////////////////////////////////////////
// A Mathematical functions for the graphics card
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include "stdafx.h"

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>
#include <d3dx9math.h>


#include "gipcommon.h"
#include "gip.h"
#include "gipeffect.h"



GIPBlend::GIPBlend(GIPDevice *Device)
{
	ASSERT(Device);
    m_pDevice = Device;
    m_pd3dDevice = Device->GetDevice();
    ASSERT(m_pd3dDevice);
    m_pGipEffect = NULL;
}

GIPBlend::~GIPBlend(void)
{
	SAFE_DELETE(m_pGipEffect);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation - not much to do here
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPBlend::Initialise()
{
    if (CreateFX()) return TRUE;
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create the FX
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPBlend::CreateFX(void)
{
	if (m_pGipEffect==NULL)
	{
		m_pGipEffect = new GIPEffect(m_pDevice);
		return m_pGipEffect->CreateFXFromResource(IDR_BLENDFX); 
	}

	return(TRUE);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply it
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPBlend::ApplySimpleBlend(int nrImages, BYTE **pImages, GIP_IMAGE dest, int *pModalCorr)
{
 	D3DXVECTOR4	ModalCorr[16];
	GIPImage	**src;
	UINT		cPasses;
	ID3DXEffect	*pEffect = m_pGipEffect->Effect();
	CString		s;
	int			i;
	long		width, height, overl, bpp;
	double		denom;

	// init the Modal correction variables for the effect file
	(*dest)->GetImageDimensions(&width, &height, &overl, &bpp);
	if (bpp==8) denom=256.;
		else denom=65536.;
	for (i=0; i<nrImages; i++)
		{ModalCorr[i].x=ModalCorr[i].y=ModalCorr[i].z=ModalCorr[i].w=(float)((double)pModalCorr[i]/denom);}
	for (i=0; i < nrImages; i++)
		pEffect->SetVectorArray("ModalCorr", ModalCorr, nrImages);

	// set texture source images
	for (int i=0; i < nrImages; i++)
	{
		s.Format("ImaTex%d",i);
		src=(GIP_IMAGE )pImages[i];
		pEffect->SetTexture(s,(*src)->pTexture->Texture());
	}
	m_pd3dDevice->SetRenderTarget( 0, (*dest)->pTexture->Surface());
	pEffect->SetInt("numImages", nrImages);
	if (nrImages <=4)
		pEffect->SetTechnique("SimpleBlend4");
	else if (nrImages <=8)
		pEffect->SetTechnique("SimpleBlend8");
	else
		pEffect->SetTechnique("SimpleBlend16");
	pEffect->Begin(&cPasses, 0);
	// init the rendertargets to zero in pass 0
	pEffect->BeginPass(0);
	pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	pEffect->EndPass();
	// end technique
	pEffect->End();
    return FALSE;
}


///////////////////////////////////////////////////////////////////////////////////////////////
// Apply it
///////////////////////////////////////////////////////////////////////////////////////////////
int GIP::GetMinValue(GIP_IMAGE src)
{
	unsigned short	*sImageData, *s;
	BYTE			*pImageData, *p;
	long			w, h, overl, bpp;
	int				i, minVal=65536;

	(*src)->GetImageDimensions(&w, &h, &overl, &bpp);

	if (bpp==8)
	{
		pImageData = new BYTE[w*h];
		GetImage(src, pImageData);
		p=pImageData;
		for (i=0; i<w*h; i++)
		{
			if (*p < minVal) minVal=*p;
			p +=1;
		}
		delete [] pImageData;
	}
	else
	{
		sImageData = new unsigned short[w*h];
		GetImage(src, sImageData);
		s=sImageData;
		for (i=0; i<w*h; i++)
		{
			if (*s < minVal) minVal=*s;
			s +=1;
		}
		delete [] sImageData;
	}
	return minVal;
}

BOOL GIPBlend::ApplyHDRBlend(GIP_IMAGE src, GIP_IMAGE dest, int minVal)
{
	UINT		cPasses;
	ID3DXEffect	*pEffect = m_pGipEffect->Effect();
	CString		s;
	long		w, h, overl, bpp;

	(*src)->GetImageDimensions(&w, &h, &overl, &bpp);
	pEffect->SetTexture("ImaTex0",(*src)->pTexture->Texture());
	m_pd3dDevice->SetRenderTarget( 0, (*dest)->pTexture->Surface());
	if (bpp==8)
		pEffect->SetFloat("minHDR", (float)((double)minVal/256.));
	else
		pEffect->SetFloat("minHDR", (float)((double)minVal/65536.));
	m_pGipEffect->SetPixelOffsetConstants((*dest)->m_nImageWidth, (*dest)->m_nImageHeight);
	pEffect->SetTechnique("HDRBlend");
	pEffect->Begin(&cPasses, 0);
	pEffect->BeginPass(0);
	pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	pEffect->EndPass(); 
	pEffect->End();
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply it
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPBlend::ApplyRangeBlend(int nrImages, BYTE **pImages, GIP_IMAGE pRangeImages, GIP_IMAGE dest, int *pModalCorr)
{
 	D3DXVECTOR4	ModalCorr[16];
 	GIPImage	**src;
	UINT		cPasses;
	ID3DXEffect	*pEffect = m_pGipEffect->Effect();
	CString		s;
	int			i, offset;
	long		width, height, overl, bpp;
	double		denom;

	(*dest)->GetImageDimensions(&width, &height, &overl, &bpp);
	if (bpp==8) denom=256.;
		else denom=65536.;
	// init the Modal correction variables for the effect file
	for (i=0; i<nrImages; i++)
		{ModalCorr[i].x=ModalCorr[i].y=ModalCorr[i].z=ModalCorr[i].w=(float)((double)pModalCorr[i]/denom);}
	for (i=0; i < nrImages; i++)
		pEffect->SetVectorArray("ModalCorr", ModalCorr, nrImages);
	// set texture source images
	if (nrImages < 4) offset=4;
	else offset=8;
	for (i=0; i < nrImages; i++)
	{
		s.Format("ImaTex%d",i);
		src=(GIP_IMAGE )pImages[i];
		pEffect->SetTexture(s,(*src)->pTexture->Texture());
		s.Format("ImaTex%d",i+offset);
		pEffect->SetTexture(s,pRangeImages[i]->pTexture->Texture());
	}
	m_pd3dDevice->SetRenderTarget( 0, (*dest)->pTexture->Surface());
	pEffect->SetInt("numImages", nrImages);
	if (nrImages <=4)
		pEffect->SetTechnique("RangeBlend4");
	else
		pEffect->SetTechnique("RangeBlend8");
	pEffect->Begin(&cPasses, 0);
	// init the rendertargets to zero in pass 0
	pEffect->BeginPass(0);
	pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	pEffect->EndPass();
	// end technique
	pEffect->End();
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
//	Calc the differences of Modal values between Images
//	- used for SimpleBlend HDRblend and RangeBlend
///////////////////////////////////////////////////////////////////////////////////////////////
void GIP::CalcModalHistogram(int nrImages, BYTE **pGipImages, int *pImageCorr)
{
	long		*hist, maxH, histSize;
	int			i, j, minModal;
	GIPImage	**src;
	long		width, height, overl, bpp;

	minModal=65536;
	src=(GIP_IMAGE )pGipImages[0];
	(*src)->GetImageDimensions(&width, &height, &overl, &bpp);
	if (bpp==8)
		histSize=256;
	else
		histSize=65536;
	hist=new long[histSize];
	for (i=0; i<nrImages; i++)
	{
		src=(GIP_IMAGE )pGipImages[i];
		Histogram(src, hist, 0);
		maxH = 0;
		for (j = 0; j < histSize; j++)
		{
			if (hist[j] > maxH)
			{
				maxH = hist[j];
				pImageCorr[i] = j;
			}
		}
		if (pImageCorr[i] < minModal) minModal=pImageCorr[i];
	}
	for (i=0; i<nrImages; i++) pImageCorr[i] -=minModal;
	delete [] hist;
}

///////////////////////////////////////////////////////////////////////////////////////////////
//	SimpleBlend
//	- SimpleBlend entrypoint for SimpleBlendGIP
//    array of images + output image should be defined on the GIP 
///////////////////////////////////////////////////////////////////////////////////////////////
void GIP::SimpleBlend(int nrImages, BYTE **pGipImages, GIP_IMAGE pOutput, BOOL corrModeHist)
{
    long		Width, Height, Overlap, Bpp;
	GIPImage	**src0, **src;
	int			i;

	// Nr of max simultaneous input textures limited to 16
	if ((!m_pMath) || (nrImages<=0) || (nrImages >16) || !pGipImages || !pOutput) return;
	src0=(GIP_IMAGE )pGipImages[0];
	if (!(*pOutput)->SameDimensions(*src0)) return;
	(*src0)->GetImageDimensions(&Width, &Height, &Overlap, &Bpp);
	if ((Bpp != 8)&& (Bpp!=16)) return;						// source images should be 8 or 16  bit
	int *pModalCorr= new int[nrImages];
	for (i=1; i<nrImages; i++)								// check if dimensions of all source images are the same
	{
		src=(GIP_IMAGE )pGipImages[i];
		if ((!*src) || (!(*src)->SameDimensions(*src0))) return;
	}
	if (corrModeHist)										// calc differences in modal values based on Histogram
		CalcModalHistogram(nrImages, pGipImages, pModalCorr);
	else
		for (i=0; i<nrImages; i++)pModalCorr[i]=0;
	pDevice->CreateQuad((*pOutput)->pTexture->m_nTexWidth, (*pOutput)->pTexture->m_nTexHeight);
	m_pBlend->Initialise();
	m_pBlend->ApplySimpleBlend(nrImages, pGipImages, pOutput, pModalCorr);
	SAFE_DELETE(pModalCorr);
}


///////////////////////////////////////////////////////////////////////////////////////////////
//	HDRBlend
//	- HDRBlend entrypoint for HDRBlendGIP
//    array of images + output image should be defined on the GIP 
///////////////////////////////////////////////////////////////////////////////////////////////

void GIP::HDRBlend(int nrImages, BYTE **pGipImages, GIP_IMAGE pOutput, BOOL corrModeHist)
{
    long		Width, Height, Overlap, Bpp;
	GIPImage	**src0, **src;
	int			i, minHDR;

	// Nr of max simultaneous input textures limited to 16
	if ((!m_pMath) || (nrImages<=0) || (nrImages >16) || !pGipImages || !pOutput) return;
	src0=(GIP_IMAGE )pGipImages[0];
	if (!(*pOutput)->SameDimensions(*src0)) return;
	(*src0)->GetImageDimensions(&Width, &Height, &Overlap, &Bpp);
	if ((Bpp != 8)&& (Bpp!=16)) return;						// source images should be 8 or 16  bit
	int *pModalCorr= new int[nrImages];
	for (i=1; i<nrImages; i++)								// check if dimensions of all source images are the same
	{
		src=(GIP_IMAGE )pGipImages[i];
		if ((!*src) || (!(*src)->SameDimensions(*src0))) return;
	}
	if (corrModeHist)										// calc differences in modal values based on Histogram
		CalcModalHistogram(nrImages, pGipImages, pModalCorr);
	else
		for (i=0; i<nrImages; i++)pModalCorr[i]=0;
    CreateScratch(&m_pScratch1, src);
	pDevice->CreateQuad((*pOutput)->pTexture->m_nTexWidth, (*pOutput)->pTexture->m_nTexHeight);
	m_pBlend->Initialise();
	m_pBlend->ApplySimpleBlend(nrImages, pGipImages, &m_pScratch1, pModalCorr);
	minHDR=GetMinValue(&m_pScratch1);
	m_pBlend->ApplyHDRBlend(&m_pScratch1, pOutput, minHDR);
	SAFE_DELETE(pModalCorr);
}

///////////////////////////////////////////////////////////////////////////////////////////////
//	RangeBlend
//	- called from BlendMultipleExposuresGIP 
//    array of images + output image should be defined on the GIP
///////////////////////////////////////////////////////////////////////////////////////////////

void GIP::RangeBlend(int nrImages, BYTE **pGipImages, GIP_IMAGE pOutput, BOOL corrModeHist)
{
	GIPImage	**pRangeImages;
    long		Width, Height, Overlap, Bpp;
	GIPImage	**src0, **src;
	int			i, *pModalCorr, MinMaxPasses=25, SmoothPasses=5;
	float		filt[225];
	double		denom;

	// Nr of max simultaneous input textures limited to 16, but we need additional corresponding range images
	if ((!m_pMath) || (nrImages<=0) || (nrImages >8) || !pGipImages || !pOutput) return;
	src0=(GIP_IMAGE )pGipImages[0];
	if (!(*pOutput)->SameDimensions(*src0)) return;
	(*src0)->GetImageDimensions(&Width, &Height, &Overlap, &Bpp);
	if ((Bpp != 8)&& (Bpp!=16)) return;						// source images should be 8 or 16  bit
	if (Bpp==8) denom=256.;
	else
		denom=65536.;
	pModalCorr=new int[nrImages];
	pRangeImages = (GIP_IMAGE )new long[nrImages];
	for (i=0; i<225; i++)									// init low pass filter
		filt[i]=(float)(1./225.);
	for (i=1; i<nrImages; i++)								// check if dimensions of all source images are the same
	{
		src=(GIP_IMAGE )pGipImages[i];
		if ((!*src) || (!(*src)->SameDimensions(*src0))) return;
	}
	if (corrModeHist)										// calc differences in modal values based on Histogram
		CalcModalHistogram(nrImages, pGipImages, pModalCorr);
	else
		for (i=0; i<nrImages; i++)pModalCorr[i]=0;
	CreateScratch(&m_pScratch1,src);
	CreateScratch(&m_pScratch2,src);
	CreateScratch(&m_pScratch3,src);
	for (i=0; i<nrImages; i++)
	{
		if (pModalCorr[i]!= 0)								// correct for modal differences here
		{
			SubtractConst((GIP_IMAGE )pGipImages[i],(float)((double)pModalCorr[i]/denom), &m_pScratch1);
			Copy(&m_pScratch1, (GIP_IMAGE )pGipImages[i]);
			pModalCorr[i]=0;
		}
	}


	for (i=0; i<nrImages; i++)
	{
		Dilate(MinMaxPasses, (GIP_IMAGE )pGipImages[i], &m_pScratch1);
		Erode(MinMaxPasses, (GIP_IMAGE )pGipImages[i], &m_pScratch2);
		Subtract(&m_pScratch1, &m_pScratch2, &m_pScratch3);
		pRangeImages[i] = (GIPImage *)new GIPImage(pd3dDevice);
		pRangeImages[i]->Create(Width, Height, Overlap, Bpp);
		Convolve(SmoothPasses, 7, filt, &m_pScratch3, &pRangeImages[i]);
	}
	pDevice->CreateQuad((*pOutput)->pTexture->m_nTexWidth, (*pOutput)->pTexture->m_nTexHeight);
	m_pBlend->Initialise();
	m_pBlend->ApplyRangeBlend(nrImages, pGipImages, pRangeImages, pOutput, pModalCorr);
	for (i=0; i<nrImages; i++)
		GIPFreeImage(&pRangeImages[i]);
	SAFE_DELETE(pRangeImages);
	SAFE_DELETE(pModalCorr);
}








///////////////////////////////////////////////////////////////////////////////////////////////
// BlendMultipleExposures
// - this function is the GIP equivalent of the same call of the CComponentBelend class
//	 images are created on the GIP and are afterwards destroyed.
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIP::BlendMultipleExposures(int BTechnique, int numImages, BYTE **ppImages, int width, int height, int bpp, BYTE *pOutputImage)
{
	GIPImage		 **pGipOutput;
	long			*Hist, B, histSize;
	int				D, Max, i, j, Temp, numGipImages;
	BYTE			*Addr;
	unsigned short	*AddrS;

	if (numImages < 2 || !ppImages || width < 2 || height < 2 || !pOutputImage)
		return FALSE;

	for (int imageIndex = 0; imageIndex < numImages; imageIndex++)
	{
		if (!ppImages[imageIndex])
			return FALSE;
	}

	int *pModes;
	B = width * height;
	if (bpp==8)
		histSize=256;
	else
		histSize=65536;

	Hist=new long[histSize];

	// Find the modal values for each image
	pModes = new int[numImages];

	for (i = 0; i < numImages; i++)
	{
		memset(Hist, 0, sizeof(long) * histSize);
		if (bpp==8)
		{
			Addr = ppImages[i];
			for (j = 0; j < B; j++) Hist[*Addr++]++;
		}
		else
		{
			AddrS= (unsigned short *)ppImages[i];
			for (j = 0; j < B; j++) Hist[*AddrS++]++;
		}
		Max = 0;
		for (j = 0; j < histSize; j++)
			if (Hist[j] > Max)
			{
				Max = Hist[j];
				pModes[i] = j;
			}
	}
	delete [] Hist;

	// Bubble sort the modes - could be done quicker but there will only be a few to sort
	for (i = 0; i < numImages; i++)
	{
		for (j = 0; j < numImages; j++)
		{
			if (i != j)
			{
				if (pModes[i] < pModes[j])
				{
					Temp = pModes[i];
					pModes[i] = pModes[j];
					pModes[j] = Temp;
					// Also arrange the images so that the image with the lowest mode is first
					Addr = ppImages[i];
					ppImages[i] = ppImages[j];
					ppImages[j] = Addr;
				}
			}
		}
	}

	// Now subtract the difference in modes from each of the images but don't do the first as its the lowest mode
	if (bpp==8)
	{
		for (i = 1; i < numImages; i++)
		{
			Addr = ppImages[i];
			D = pModes[i] - pModes[0];
			for (j = 0; j < B; j++)
			{
				*Addr++ = *Addr > D ? *Addr - D : 0;
			}
		}
	}
	else
	{
		for (i = 1; i < numImages; i++)
		{
			AddrS = (unsigned short *)ppImages[i];
			D = pModes[i] - pModes[0];
			for (j = 0; j < B; j++)
			{
				*AddrS++ = *AddrS > D ? *AddrS - D : 0;
			}
		}
	}
	int imageSizeBytes = width * height * sizeof(BYTE);


	BOOL *pUseImages = NULL;

#if defined(SELECT_EXPOSURE_BLENDING_IMAGES)
	// Choose images to use, based on various criteria.
	// Note that this will introduce a 'patchwork' effect if used on a series
	// of images that are displayed as tiles, if the exposures chosen are
	// different for different tiles.
	const int minImages = 2;
	const int initialGate = 32;

	pUseImages = new BOOL[numImages];
	if (pUseImages)
	{
		for (int i = 0; i < numImages; i++)
			pUseImages[i] = TRUE;

		// Exclude images whose mean value is above a maximum value (to remove
		// very overexposed images from the blend), subject to using a
		// minimum number of images.
		int *pImageMeans = new int[numImages];
		if (pImageMeans)
		{
			// Calulate the mean pixel value for each image.
			for (int imageIndex = 0; imageIndex < numImages; imageIndex++)
			{
				const BYTE *pImage = ppImages[imageIndex];

				double sum = 0.0;

				for (int i = 0; i < imageSizeBytes; i++)
				{
					sum += double(*pImage);

					pImage++;
				}

				int mean = imageSizeBytes > 0 ? int(sum / imageSizeBytes) : 0;

				pImageMeans[imageIndex] = mean;
			}

			// If the gate excludes too many images, keep raising it until
			// enough images are included (or until it is fully raised).
			int gate = initialGate;
			if (numImages < minImages)
			{
				while (gate > 0)
				{
					int count = 0;
					for (int i = 0; i < numImages; i++)
					{
						if (pImageMeans[i] <= (255 - gate))
							count++;
					}

					if (count < minImages)
						gate /= 2;
					else
						break;
				}
			}

			// Don't use images that can't get through the gate.
			int i;
			for (i = 0; i < numImages; i++)
			{
				if (pImageMeans[i] > (255 - gate))
					pUseImages[i] = FALSE;
			}

			delete[] pImageMeans;
		}
	}

#endif

	//	convert the memory images into GipImages when they should be used
	numGipImages=0;
	BYTE** pGipImages=(BYTE**) new long[numImages];

	if (bpp==8)
	{
		for (i = 0; i < numImages; i++)
		{
			if ((pUseImages && pUseImages[i]) || (!pUseImages))
			{
				pGipImages[numGipImages]=(BYTE *)GIPCreateImage(width, height, 40, 8);
				PutImage((GIP_IMAGE )pGipImages[numGipImages], ppImages[i]);
				numGipImages +=1;
			}
		}
		pGipOutput=GIPCreateImage(width, height, 40, 8);
	}
	else
	{
		for (i = 0; i < numImages; i++)
		{
			if ((pUseImages && pUseImages[i]) || (!pUseImages))
			{
				pGipImages[numGipImages]=(BYTE *)GIPCreateImage(width, height, 40, 16);
				PutImage((GIP_IMAGE )pGipImages[numGipImages], (unsigned short *)ppImages[i]);
				numGipImages +=1;
			}
		}
		pGipOutput=GIPCreateImage(width, height, 40, 16);
	}

	switch (BTechnique)
	{
	case Range:
		RangeBlend(numGipImages, pGipImages, pGipOutput, FALSE);
		break;

	case Simple:
		SimpleBlend(numGipImages, pGipImages, pGipOutput, FALSE);
		break;

	case HDR:
		HDRBlend(numGipImages, pGipImages, pGipOutput, FALSE);
		break;
	}

	if (bpp==8)
		GetImage(pGipOutput, pOutputImage);
	else
		GetImage(pGipOutput, (unsigned short *)pOutputImage);

	GIPFreeImage(pGipOutput);
	
	for (i=0; i<numGipImages; i++)
	{
		// destroy the GIPImages
		GIPFreeImage((GIP_IMAGE )pGipImages[i]);
	}
	delete [] pGipImages;
	
	if (pUseImages)
		delete[] pUseImages;

	delete pModes;
	return TRUE;
}


