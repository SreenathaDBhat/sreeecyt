#pragma once
///////////////////////////////////////////////////////////////////////////////////////////////
// Simple Math functions written for the graphics card
// Written By Karl Ratcliff     190805
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gipcommon.h"

// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif

typedef enum BlendTechnique
{
	Range,
	Simple,
	HDR
	
}BlendTechnique;

class DllExport GIPBlend
{
public:
    GIPBlend(GIPDevice *Device);
    ~GIPBlend(void);

    BOOL Initialise();				 // Initialisation 
	BOOL ApplySimpleBlend (int nrImages, BYTE **pImages, GIP_IMAGE dest, int *pModalCorr);
	BOOL ApplyHDRBlend	  (GIP_IMAGE src, GIP_IMAGE dest, int minVal);
	BOOL ApplyRangeBlend  (int nrImages, BYTE **pImages, GIP_IMAGE pRangeImages, GIP_IMAGE dest, int *pModalCorr);

private:
    BOOL CreateFX(void);															// Load and compile the FX file
	IDirect3DDevice9            *m_pd3dDevice;      // Our D3D device
    GIPDevice                   *m_pDevice;         // Our GIP Device
    GIPEffect					*m_pGipEffect;		// D3DX convolution effect interface
};
