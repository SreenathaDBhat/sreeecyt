///////////////////////////////////////////////////////////////////////////////////////////////
// A Mathematical functions for the graphics card
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include "stdafx.h"

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>
#include <d3dx9math.h>


#include "gipcommon.h"
#include "gip.h"
#include "gipeffect.h"
#include "GIPColourConv.h"

GIPColourConv::GIPColourConv(GIPDevice *Device)
{
	ASSERT(Device);
    m_pDevice = Device;
    m_pd3dDevice = Device->GetDevice();
    ASSERT(m_pd3dDevice);
    m_pGipEffect = NULL;

	Initialise();
}

GIPColourConv::~GIPColourConv(void)
{
	SAFE_DELETE(m_pGipEffect);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation - not much to do here
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPColourConv::Initialise()
{

    if (CreateFX()) return TRUE;

    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create the FX
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPColourConv::CreateFX(void)
{
	if (m_pGipEffect==NULL)
	{
		m_pGipEffect = new GIPEffect(m_pDevice);
        return m_pGipEffect->CreateFXFromResource(IDR_COLOURCONVFX); 
	}

    return(TRUE);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply it
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPColourConv::Apply(char *name, GIPTexture *pSource, GIPTexture *pTarget)
{
    UINT cPasses;

    ID3DXEffect *pEffect = m_pGipEffect->Effect();
    
	// set source images 
	pEffect->SetTexture( "SourceImageTexture", pSource->Texture());
	m_pd3dDevice->SetRenderTarget( 0, pTarget->Surface());

	pEffect->SetTechnique(name);

	// Apply the technique contained in the effect - returns num of passes in cPasses
	pEffect->Begin(&cPasses, 0);

	pEffect->BeginPass(0);	// only one pass in each morph techniqe - pass 0

	// The effect interface queues up the changes and performs them 
	// with the CommitChanges call. You do not need to call CommitChanges if 
	// you are not setting any parameters between the BeginPass and EndPass.
	pEffect->CommitChanges();

	// Render the simple quad with the applied technique
	m_pDevice->DrawQuad();

	// end tecnnique pass
	pEffect->EndPass();

	// end technique
	pEffect->End();
    return FALSE;
}
