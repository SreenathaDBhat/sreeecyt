///////////////////////////////////////////////////////////////////////////////////////////////
// A Mathematical functions for the graphics card
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include "stdafx.h"

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>
#include <d3dx9math.h>


#include "gipcommon.h"
#include "gip.h"
#include "gipeffect.h"
#include "GIPColourConv.h"

GIPColourDeconv::GIPColourDeconv(GIPDevice *Device)
{
	ASSERT(Device);
    m_pDevice = Device;
    m_pd3dDevice = Device->GetDevice();
    ASSERT(m_pd3dDevice);
    m_pGipEffect = NULL;
}

GIPColourDeconv::~GIPColourDeconv(void)
{
	SAFE_DELETE(m_pGipEffect);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation - not much to do here
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPColourDeconv::Initialise(double *vc1, double *vc2, double *vc3)
{
    if (CreateFX())
	{	
		double	MODx[3], MODy[3], MODz[3], cosx[3], cosy[3], cosz[3], len[3];
		double	leng, A, V, C;
		int		i;
		MODx[0]=vc1[0]; MODx[1]=vc2[0]; MODx[2]=vc3[0];
		MODy[0]=vc1[1]; MODy[1]=vc2[1]; MODy[2]=vc3[1];
		MODz[0]=vc1[2]; MODz[1]=vc2[2]; MODz[2]=vc3[2];
		for (i = 0; i < 3; i++)
		{
			//normalise vector length
			cosx[i] = cosy[i] = cosz[i] = 0.0;
			len[i] = sqrt(MODx[i] * MODx[i] + MODy[i] * MODy[i] + MODz[i] * MODz[i]);
			if (len[i] != 0.0)
			{
				cosx[i] = MODx[i] / len[i];
				cosy[i] = MODy[i] / len[i];
				cosz[i] = MODz[i] / len[i];
			}
		}

		if ((cosx[1] == 0.0) && (cosy[1] == 0.0)&& (cosz[1] == 0.0)) // 2nd colour unspecified
		{
			cosx[1] = cosz[0];
			cosy[1] = cosx[0];
			cosz[1] = cosy[0];
		}

		if ((cosx[2] == 0.0) && (cosy[2] == 0.0) && (cosz[2] == 0.0)) // 3th colour unspecified
		{
			if ((cosx[0] * cosx[0] + cosx[1] * cosx[1]) > 1) cosx[2] = 0.0;
				else cosx[2] = sqrt(1.0 - (cosx[0] * cosx[0]) - (cosx[1] * cosx[1]));
			if ((cosy[0] * cosy[0] + cosy[1] * cosy[1]) > 1) cosy[2] = 0.0;
				else cosy[2] = sqrt(1.0 - (cosy[0] * cosy[0]) - (cosy[1] * cosy[1]));
	       if ((cosz[0] * cosz[0] + cosz[1] * cosz[1]) > 1) cosz[2] = 0.0;
				else cosz[2] = sqrt(1.0 - (cosz[0] * cosz[0]) - (cosz[1] * cosz[1]));
        }
		leng = sqrt(cosx[2] * cosx[2] + cosy[2] * cosy[2] + cosz[2] * cosz[2]);

		cosx[2] /= leng;
		cosy[2] /= leng;
		cosz[2] /= leng;

		for (i = 0; i < 3; i++)
		{
			if (cosx[i] == 0.0) cosx[i] = 0.001;
			if (cosy[i] == 0.0) cosy[i] = 0.001;
			if (cosz[i] == 0.0) cosz[i] = 0.001;
		}
		
		//matrix inversion
		A = cosy[1] - cosx[1] * cosy[0] / cosx[0];
		V = cosz[1] - cosx[1] * cosz[0] / cosx[0];
		C = cosz[2] - cosy[2] * V/A + cosx[2] * (V/A * cosy[0] / cosx[0] - cosz[0] / cosx[0]);

		m_qVec[0].z = (float)((-cosx[2] / cosx[0] - cosx[2] / A * cosx[1] / cosx[0] * cosy[0] / cosx[0] + cosy[2] / A * cosx[1] / cosx[0]) / C);
		m_qVec[0].y = (float)(-m_qVec[0].z * V / A - cosx[1] / (cosx[0] * A));
		m_qVec[0].x = (float)(1.0 / cosx[0] - m_qVec[0].y * cosy[0] / cosx[0] - m_qVec[0].z * cosz[0] / cosx[0]);
		m_qVec[1].z = (float)((-cosy[2] / A + cosx[2] / A * cosy[0] / cosx[0]) / C);
		m_qVec[1].y = (float)(-m_qVec[1].z * V / A + 1.0 / A);
		m_qVec[1].x = (float)(-m_qVec[1].y * cosy[0] / cosx[0] - m_qVec[1].z * cosz[0] / cosx[0]);
		m_qVec[2].z = (float)(1.0 / C);
		m_qVec[2].y = (float)(-m_qVec[2].z * V / A);
		m_qVec[2].x = (float)(-m_qVec[2].y * cosy[0] / cosx[0] - m_qVec[2].z * cosz[0] / cosx[0]);
		m_qVec[0].w = m_qVec[1].w = m_qVec[2].w = 0.0;
		return TRUE;
	}
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create the FX
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPColourDeconv::CreateFX(void)
{
	if (m_pGipEffect==NULL)
	{
		m_pGipEffect = new GIPEffect(m_pDevice);
		return m_pGipEffect->CreateFXFromResource(IDR_DECONVFX); 
	}

	return(TRUE);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply it
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPColourDeconv::Apply(GIPTexture *pSource, GIPTexture *pTarget)
{
    UINT cPasses;

    ID3DXEffect *pEffect = m_pGipEffect->Effect();
    
	// set source images 
	pEffect->SetTexture( "SourceImageTexture", pSource->Texture());
	m_pd3dDevice->SetRenderTarget( 0, pTarget->Surface());

	pEffect->SetTechnique("ColourDeconv");
	pEffect->SetVectorArray("QVector", m_qVec, 3);

	// Apply the technique contained in the effect - returns num of passes in cPasses
	pEffect->Begin(&cPasses, 0);

	pEffect->BeginPass(0);	// only one pass in each morph techniqe - pass 0

	// The effect interface queues up the changes and performs them 
	// with the CommitChanges call. You do not need to call CommitChanges if 
	// you are not setting any parameters between the BeginPass and EndPass.
	pEffect->CommitChanges();

	// Render the simple quad with the applied technique
	m_pDevice->DrawQuad();

	// end tecnnique pass
	pEffect->EndPass();

	// end technique
	pEffect->End();
    return FALSE;
}

void GIP::ColourDeconv(GIP_IMAGE src, double *vc1, double *vc2, double *vc3,  GIP_IMAGE dest)
{
	if ((!src) || (!dest)) return;
	if (!(*src)->SameDimensions(*dest)) return;
	pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
    m_pColourDeconv->Initialise(vc1, vc2, vc3);
	m_pColourDeconv->Apply((*src)->pTexture, (*dest)->pTexture);
}
