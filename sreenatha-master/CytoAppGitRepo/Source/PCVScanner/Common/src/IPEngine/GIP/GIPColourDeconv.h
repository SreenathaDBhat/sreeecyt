#pragma once
///////////////////////////////////////////////////////////////////////////////////////////////
// Simple Math functions written for the graphics card
// Written By Karl Ratcliff     190805
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gipcommon.h"

// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif


class DllExport GIPColourDeconv
{
public:
    GIPColourDeconv(GIPDevice *Device);
    ~GIPColourDeconv(void);

    BOOL Initialise(double *vc1, double *vc2, double *vc3);		// Initialisation 
    BOOL Apply(GIPTexture *pSource, GIPTexture *pTarget);		// Appy it

private:
    BOOL CreateFX(void);															// Load and compile the FX file
    IDirect3DDevice9          *m_pd3dDevice;      // Our D3D device
    GIPDevice                   *m_pDevice;         // Our GIP Device
    GIPEffect					*m_pGipEffect;		// D3DX convolution effect interface
 	D3DXVECTOR4					m_qVec[3];			// Deconvolution matrix
};
