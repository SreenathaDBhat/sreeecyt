
#pragma once

// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif

#ifndef SAFE_DELETE
    #define SAFE_DELETE(p)       { if(p) { delete (p);     (p)=NULL; } }
#endif    
#ifndef SAFE_DELETE_ARRAY
    #define SAFE_DELETE_ARRAY(p) { if(p) { delete[] (p);   (p)=NULL; } }
#endif    
#ifndef SAFE_RELEASE
    #define SAFE_RELEASE(p)      { if(p) { (p)->Release(); (p)=NULL; } }
#endif

#ifndef CHECK_HR
    #define CHECK_HR(expr) { if (FAILED(expr)) throw "bad HRESULT"; };
#endif

#ifdef DEBUG_PS 
#pragma message("DEBUG PIXEL SHADERS")
#ifndef D3D_DEBUG_INFO
    #define D3D_DEBUG_INFO
#endif
#endif
#ifdef DEBUG_VS
#pragma message("DEBUG VERTEX SHADERS")
#ifndef D3D_DEBUG_INFO
    #define D3D_DEBUG_INFO
#endif
#endif

typedef struct tagRGB16BTRIPLE {
        unsigned short    rgbtBlue;
        unsigned short    rgbtGreen;
        unsigned short    rgbtRed;
} RGB16BTRIPLE;

typedef enum
{
	LowPass,
	HighPass,
	BandPass,
	BandStop
}FilterType;
