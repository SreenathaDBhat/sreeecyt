///////////////////////////////////////////////////////////////////////////////////////////////
// A Mathematical functions for the graphics card
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include "stdafx.h"

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>
#include <d3dx9math.h>


#include "gipcommon.h"
#include "gip.h"
#include "gipeffect.h"
#include "GIPComponent.h"

GIPComponent::GIPComponent(GIPDevice *Device)
{
	ASSERT(Device);
    m_pDevice = Device;
    m_pd3dDevice = Device->GetDevice();
    ASSERT(m_pd3dDevice);
    m_pGipEffect = NULL;
}

GIPComponent::~GIPComponent(void)
{
	SAFE_DELETE(m_pGipEffect);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation - not much to do here
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPComponent::Initialise(int texWidth, int texHeight)
{

    if (CreateFX())
	{
		ID3DXEffect *pEff;

		pEff = m_pGipEffect->Effect();
		pEff->SetFloat( "TextureWidth", (float)texWidth);
		pEff->SetFloat( "TextureHeight", (float)texHeight);						
		return TRUE;
	}
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create the FX
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPComponent::CreateFX(void)
{
	if (m_pGipEffect==NULL)
	{
		m_pGipEffect = new GIPEffect(m_pDevice);
        return m_pGipEffect->CreateFXFromResource(IDR_COMPONENTFX); 
	}

	return(TRUE);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// ApplyThreshold
///////////////////////////////////////////////////////////////////////////////////////////////
void GIPComponent::ApplyThreshold(float Rmin, float Rmax, float Gmin, float Gmax, float Bmin, float Bmax, GIPTexture *src, GIPTexture *target)
{
	UINT cPasses;
    HRESULT hr;
    D3DXVECTOR4 c10[2];

	c10[0].x = Rmin;
    c10[0].y = Gmin;
    c10[0].z = Bmin;
    c10[0].w = 1.0f;
    c10[1].x = Rmax;
    c10[1].y = Gmax;
    c10[1].z = Bmax;
    c10[1].w = 1.0f;

	// set source image 
	ID3DXEffect *pEffect = m_pGipEffect->Effect();

	hr = pEffect->SetTexture("Src1ImageTexture", src->Texture());

	// set render target texture
	hr = m_pd3dDevice->SetRenderTarget( 0, target->Surface());

	hr = pEffect->SetTechnique("componentthresh");

	// Apply the technique contained in the effect - returns num of passes in cPasses
	hr = pEffect->Begin(&cPasses, 0);

	hr = pEffect->BeginPass(0);	// only one pass  - pass 0
    
    // Set the thresholds in registers c10 & c11
    hr = m_pd3dDevice->SetPixelShaderConstantF(10, c10[0], 2);

	// The effect interface queues up the changes and performs them 
	// with the CommitChanges call. You do not need to call CommitChanges if 
	// you are not setting any parameters between the BeginPass and EndPass.
	hr = pEffect->CommitChanges();

	// Render the simple quad with the applied technique
	m_pDevice->DrawQuad();

	// end tecnnique pass
	hr = pEffect->EndPass();

	// end technique
	hr = pEffect->End();
}


///////////////////////////////////////////////////////////////////////////////////////////////
// ApplySplit
///////////////////////////////////////////////////////////////////////////////////////////////
void GIPComponent::ApplySplit(GIPTexture *src, GIPTexture *target1, GIPTexture *target2, GIPTexture *target3)
{
	UINT cPasses;
    HRESULT hr;
    int i;
    LPDIRECT3DSURFACE9 Targets[3];
	D3DXVECTOR4 c10[3];		// Channel masks - NOTE ditch the alpha channel

	c10[0].x = 1.0f;        // Red
    c10[0].y = 0.0f;
    c10[0].z = 0.0f;
    c10[0].w = 0.0f;
    c10[1].x = 0.0f;        // Green
    c10[1].y = 1.0f;
    c10[1].z = 0.0f;
    c10[1].w = 0.0f;
    c10[2].x = 0.0f;        // Blue
    c10[2].y = 0.0f;
    c10[2].z = 1.0f;
    c10[2].w = 0.0f;

 
	// set source image 
	ID3DXEffect *pEffect = m_pGipEffect->Effect();

	hr = pEffect->SetTexture("Src1ImageTexture", src->Texture());

	Targets[0] = target1->Surface();
    Targets[1] = target2->Surface();
    Targets[2] = target3->Surface();

    for (i = 0; i < 3; i++)
    {

	    // set render target texture
	    m_pd3dDevice->SetRenderTarget(0, Targets[i]);

	    pEffect->SetTechnique("splitcomponents");

	    // Apply the technique contained in the effect - returns num of passes in cPasses
	    pEffect->Begin(&cPasses, 0);

        pEffect->BeginPass(0);	// only one pass  - pass 0
        hr = m_pd3dDevice->SetPixelShaderConstantF(10, c10[i], 1);

	    // The effect interface queues up the changes and performs them 
	    // with the CommitChanges call. You do not need to call CommitChanges if 
	    // you are not setting any parameters between the BeginPass and EndPass.
	    pEffect->CommitChanges();

	    // Render the simple quad with the applied technique
	    m_pDevice->DrawQuad();

	    // end tecnnique pass
	    pEffect->EndPass();

	    // end technique
	    pEffect->End();
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
// ApplyJoin
///////////////////////////////////////////////////////////////////////////////////////////////
void GIPComponent::ApplyJoin(GIPTexture *src1, GIPTexture *src2, GIPTexture *src3, GIPTexture *target)
{
	UINT cPasses;
    HRESULT hr;

	ID3DXEffect *pEffect = m_pGipEffect->Effect();

	// set source image 
	hr = pEffect->SetTexture("Src1ImageTexture", src1->Texture());
	hr = pEffect->SetTexture("Src2ImageTexture", src2->Texture());
	hr = pEffect->SetTexture("Src3ImageTexture", src3->Texture());
 
    // set render target texture
    m_pd3dDevice->SetRenderTarget(0, target->Surface());

    pEffect->SetTechnique("joincomponents");

    // Apply the technique contained in the effect - returns num of passes in cPasses
    pEffect->Begin(&cPasses, 0);

    pEffect->BeginPass(0);	// only one pass  - pass 0

    // The effect interface queues up the changes and performs them 
    // with the CommitChanges call. You do not need to call CommitChanges if 
    // you are not setting any parameters between the BeginPass and EndPass.
    pEffect->CommitChanges();

    // Render the simple quad with the applied technique
    m_pDevice->DrawQuad();

    // end tecnnique pass
    pEffect->EndPass();

    // end technique
    pEffect->End();
}


///////////////////////////////////////////////////////////////////////////////////////////////
// ApplyJoin
///////////////////////////////////////////////////////////////////////////////////////////////
void GIPComponent::ApplyBlend(GIPTexture *src1, float a1, GIPTexture *src2, float a2, GIPTexture *src3, float a3, GIPTexture *target)
{
	UINT cPasses;
    HRESULT hr;
    float c10[][4] = { a1, 0.0, 0.0, 1.0,
                       0.0, a2, 0.0, 1.0,
                       0.0, 0.0, a3, 1.0 };

    ID3DXEffect *pEffect = m_pGipEffect->Effect();

	// set source image 
	hr = pEffect->SetTexture("Src1ImageTexture", src1->Texture());
	hr = pEffect->SetTexture("Src2ImageTexture", src2->Texture());
	hr = pEffect->SetTexture("Src3ImageTexture", src3->Texture());
 
    // set render target texture
    m_pd3dDevice->SetRenderTarget(0, target->Surface());

    pEffect->SetTechnique("alphablend");

    // Apply the technique contained in the effect - returns num of passes in cPasses
    pEffect->Begin(&cPasses, 0);

    pEffect->BeginPass(0);	// only one pass  - pass 0

    hr = m_pd3dDevice->SetPixelShaderConstantF(10, c10[0], 3);

    // The effect interface queues up the changes and performs them 
    // with the CommitChanges call. You do not need to call CommitChanges if 
    // you are not setting any parameters between the BeginPass and EndPass.
    pEffect->CommitChanges();

    // Render the simple quad with the applied technique
    m_pDevice->DrawQuad();

    // end tecnnique pass
    pEffect->EndPass();

    // end technique
    pEffect->End();
}

