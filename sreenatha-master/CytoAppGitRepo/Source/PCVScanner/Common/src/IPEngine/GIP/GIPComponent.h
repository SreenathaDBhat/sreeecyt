#pragma once
///////////////////////////////////////////////////////////////////////////////////////////////
// Simple Math functions written for the graphics card
// Written By Karl Ratcliff     190805
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gipcommon.h"

// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif


class DllExport GIPComponent
{
public:
    GIPComponent(GIPDevice *Device);
    ~GIPComponent(void);

    BOOL Initialise(int texWidth, int texHeight);				 // Initialisation 
    void ApplyThreshold(float Rmin, float  Rmax, float  Gmin, float  Gmax, float  Bmin, float  Bmax, 
						GIPTexture *pTexSrc, GIPTexture *pTexTarget);
    void ApplyJoin(GIPTexture *pTexSrc1, GIPTexture *pTexSrc2, GIPTexture *pTexSrc3, GIPTexture *pTexTarget);
    void ApplySplit(GIPTexture *pTexSrc, GIPTexture *pTexTarget1, GIPTexture *pTexTarget2, GIPTexture *pTexTarget3);
    void ApplyBlend(GIPTexture *pTexSrc1, float a1, GIPTexture *pTexSrc2, float a2, GIPTexture *pTexSrc3, 
					float a3, GIPTexture *pTexTarget);

private:
    BOOL CreateFX(void);															// Load and compile the FX file
    IDirect3DDevice9          *m_pd3dDevice;      // Our D3D device
    GIPDevice                   *m_pDevice;         // Our GIP Device
    GIPEffect					*m_pGipEffect;		// D3DX convolution effect interface
};
