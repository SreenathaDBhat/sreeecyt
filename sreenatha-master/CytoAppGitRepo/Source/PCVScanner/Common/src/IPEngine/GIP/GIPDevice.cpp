///////////////////////////////////////////////////////////////////////////////////////////////
// GIP D3D device class - source file extracted from Marks original GIP class
// KR270805 
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "gipcommon.h"
#include ".\gipdevice.h"
#include <windows.h>
#include <tchar.h>
#include <strsafe.h>

typedef void (WINAPI *PGNSI)(LPSYSTEM_INFO);

// Utility function
BOOL GetWinVersion(LPTSTR pszOS)
{
   OSVERSIONINFOEX osvi;
   SYSTEM_INFO si;
   PGNSI pGNSI;
   BOOL bOsVersionInfoEx;
   

	StringCchCopy(pszOS, 256, TEXT("UNSUPPORTED"));

   ZeroMemory(&si, sizeof(SYSTEM_INFO));
   ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));

   osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

   if( !(bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi)) )
      return 1;


   // Call GetNativeSystemInfo if supported or GetSystemInfo otherwise.

   pGNSI = (PGNSI) GetProcAddress(GetModuleHandle(TEXT("kernel32.dll")), "GetNativeSystemInfo");
   if(NULL != pGNSI)
      pGNSI(&si);
   else 
	   GetSystemInfo(&si);

   if ( VER_PLATFORM_WIN32_NT==osvi.dwPlatformId && 
        osvi.dwMajorVersion > 4 )
   {
      // Test for the specific product.

      if ( osvi.dwMajorVersion == 6 )
      {
         if ( osvi.dwMinorVersion == 1 )
         {
            if( osvi.wProductType == VER_NT_WORKSTATION )
				StringCchCopy(pszOS, 256, TEXT("WINDOWS_7"));
            else 
				StringCchCopy(pszOS, 256, TEXT("WINDOWS_SERVER_2008_R2"));
         }
      }

      if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2 )
      {
		 if( osvi.wProductType == VER_NT_WORKSTATION && si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64)
         {
			StringCchCopy(pszOS, 256, TEXT("WINDOWS_XP_x64"));
		 }
      }

      if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1 )
      {
			StringCchCopy(pszOS, 256, TEXT("WINDOWS_XP"));
      }

      return TRUE; 
   }
   else
   {  
      return FALSE;
   }
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GIPDevice 
///////////////////////////////////////////////////////////////////////////////////////////////

GIPDevice::GIPDevice(void)
{
    pD3D = NULL;
    pd3dDevice = NULL;
	pVertexBuffer = NULL;
	pVertexDeclaration = NULL;
	QuadValid=FALSE;
    m_nOldQuadW = -1;
    m_nOldQuadH = -1;
	m_hWnd = NULL;
}

GIPDevice::~GIPDevice(void)
{	
    SAFE_RELEASE(pVertexBuffer);
	SAFE_RELEASE(pVertexDeclaration);
    SAFE_RELEASE(pd3dDevice);
	SAFE_RELEASE(pD3D);
	DestroyWindow(m_hWnd);
}


///////////////////////////////////////////////////////////////////////////////////////////////
// Init Direct3D device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPDevice::Init(void)
{
	OSVERSIONINFO osvi;	
	HRESULT hr;

	ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx(&osvi);
	if (osvi.dwMajorVersion < 5) // if Windows NT or older error
		return FALSE;

	TCHAR ver[256];
	if(!GetWinVersion(ver))
		return FALSE;

	if (strncmp("WINDOWS_7", ver, 9) == 0)
	{


		IDirect3D9Ex*			pD3Dtemp;                                   // D3D interfaces          
		IDirect3DDevice9Ex*		pd3dDevicetemp;

		// create direct3d object
		if(Direct3DCreate9Ex(D3D_SDK_VERSION, &pD3Dtemp) != S_OK)
		{
			MessageBox(NULL,"Could not create D3D9ex object", "DirectX", MB_ICONERROR);
			return FALSE;
		}


		// dummy presentation parameters
		D3DPRESENT_PARAMETERS d3dpp; 
		ZeroMemory( &d3dpp, sizeof(d3dpp) );
		d3dpp.Windowed = TRUE;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
		d3dpp.BackBufferWidth = 10;					// not important - should never be used
		d3dpp.BackBufferHeight = 10;

		// create dummy window
		WNDCLASS c;
		HINSTANCE hinstance = (HINSTANCE)GetModuleHandle(NULL);

		// Register dummy window class
		c.style = CS_PARENTDC;
		c.lpfnWndProc = ::DefWindowProc;
		c.cbClsExtra = 0;
		c.cbWndExtra = 0;
		c.hInstance = hinstance;
		c.hIcon = 0;
		c.hCursor = NULL;	// No cursor as we change ours all the time
		c.hbrBackground = (HBRUSH)COLOR_BTNFACE;
		c.lpszMenuName = NULL;
		c.lpszClassName = "D3DWnd";

		RegisterClass(&c);

		DWORD dwWindowStyle = WS_OVERLAPPED ;

		// Create the render window
		m_hWnd = CreateWindow("D3DWnd", "", dwWindowStyle,
								 0, 0, 10, 10, 0,
								 NULL, hinstance, 0);


		hr = pD3Dtemp->CreateDeviceEx(D3DADAPTER_DEFAULT, 
									  D3DDEVTYPE_HAL, 
									  m_hWnd,
									  D3DCREATE_HARDWARE_VERTEXPROCESSING  | D3DCREATE_MULTITHREADED,
									  &d3dpp, 
									  NULL,
									  &pd3dDevicetemp);

		if (!FAILED(hr))
		{
			pD3D = pD3Dtemp;
			pd3dDevice = pd3dDevicetemp;
		}
		else
			OutputDebugString(_T("BIG ERROR - COULD NOT CREATE D3D DEVICE\r\n"));
	}
	else
	{


		if( NULL == (pD3D = Direct3DCreate9(D3D_SDK_VERSION)))
		{
			MessageBox(NULL,"Could not create D3D object", "DirectX", MB_ICONERROR);
			return FALSE;
		}


		// dummy presentation parameters
		D3DPRESENT_PARAMETERS d3dpp; 
		ZeroMemory( &d3dpp, sizeof(d3dpp) );
		d3dpp.Windowed = TRUE;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
		d3dpp.BackBufferWidth = 10;					// not important - should never be used
		d3dpp.BackBufferHeight = 10;

		// create dummy window
		WNDCLASS c;
		HINSTANCE hinstance = (HINSTANCE)GetModuleHandle(NULL);

		// Register dummy window class
		c.style = CS_PARENTDC;
		c.lpfnWndProc = ::DefWindowProc;
		c.cbClsExtra = 0;
		c.cbWndExtra = 0;
		c.hInstance = hinstance;
		c.hIcon = 0;
		c.hCursor = NULL;	// No cursor as we change ours all the time
		c.hbrBackground = (HBRUSH)COLOR_BTNFACE;
		c.lpszMenuName = NULL;
		c.lpszClassName = "D3DWnd";

		RegisterClass(&c);

		DWORD dwWindowStyle = WS_OVERLAPPED ;

		// Create the render window
		m_hWnd = CreateWindow("D3DWnd", "", dwWindowStyle,
								 0, 0, 10, 10, 0,
								 NULL, hinstance, 0);


		hr = pD3D->CreateDevice(D3DADAPTER_DEFAULT, 
									  D3DDEVTYPE_HAL, 
									  m_hWnd,
									  D3DCREATE_HARDWARE_VERTEXPROCESSING  | D3DCREATE_MULTITHREADED,
									  &d3dpp, 
									  &pd3dDevice);
		if (FAILED(hr))
			OutputDebugString(_T("BIG ERROR - COULD NOT CREATE D3D DEVICE\r\n"));
	}

    // creaet direct3d device
	if (FAILED(hr))
	{
		switch (hr)
		{
		case D3DERR_DEVICELOST:
			OutputDebugString("D3DERR_DEVICELOST\r\n");
			break;

		case D3DERR_INVALIDCALL:
			OutputDebugString("D3DERR_INVALIDCALL\r\n");
			break;

		case D3DERR_NOTAVAILABLE:
			OutputDebugString("D3DERR_NOTAVAILABLE\r\n");
			break;

		case D3DERR_OUTOFVIDEOMEMORY:
			OutputDebugString("D3DERR_OUTOFVIDEOMEMORY\r\n");
			break;

		default:
			OutputDebugString("D3DERR UNKNOWN\r\n");
			break;
		}

		OutputDebugString("Could not create d3d device\r\n");
	   
        return FALSE;
	}

    // Check for pixel shader version 2.0 support.
	D3DCAPS9 d3dcaps9;
    ZeroMemory(&d3dcaps9, sizeof(d3dcaps9));

    if (FAILED(pD3D->GetDeviceCaps(
					D3DADAPTER_DEFAULT,
					D3DDEVTYPE_HAL,
					&d3dcaps9)))
		return FALSE; 

    // Check for pixel shader version 3.0 support.
    if (d3dcaps9.PixelShaderVersion < D3DPS_VERSION(2,0))
	{
		//MessageBox(NULL,"Pixel Shader 3.0 not supported", "DirectX", MB_ICONERROR);
        return FALSE;
	}

	// Set appropiate states
    pd3dDevice->SetRenderState( D3DRS_DITHERENABLE,   FALSE );
    pd3dDevice->SetRenderState( D3DRS_SPECULARENABLE, FALSE );
    pd3dDevice->SetRenderState( D3DRS_ZENABLE, D3DZB_FALSE);
    pd3dDevice->SetDepthStencilSurface( NULL );

	return TRUE;
}


//---------------------------------------------------------------------------------------------
//	Create vertex buffer for simple quad - this will be used to render the image
// 
//	Because the image may be smaller tha its supporting texture (which must be a power of 2)
//	the quad vertex and texture coordinates are determined from image (quadrant) and texture sizes
//
void GIPDevice::CreateQuad(int W, int H)
{
	if (W != m_nOldQuadW || H != m_nOldQuadH || !QuadValid)
    {
        DestroyQuad();

        float x0 = -1.0f;
        float y0 = -1.0f;
        float x1 = x0 + 2.0f;	                        // TextureWidth is value passed down to effect
        float y1 = y0 + 2.0f;

        float texelcentreoffsetW = 0.5f/W;              // Sample centre of texel
        float texelcentreoffsetH = 0.5f/H;

        float u0 = 0.0f + texelcentreoffsetW;
        float v0 = 0.0f + texelcentreoffsetH;
        float u1 = 1.0f + texelcentreoffsetW;			// multiply by 1.0, beware of integer division
        float v1 = 1.0f + texelcentreoffsetH;

        // create square quad 
        GIP_CUSTOMVERTEX vertices[] =	                // in clockwise order for front face
        {
            {x0, y0, 0.0f, u0, v0},	// lower left
            {x0, y1, 0.0f, u0, v1},	// upper left
            {x1, y1, 0.0f, u1, v1}, // upper right
            {x1, y0, 0.0f, u1, v0}	// lower right
        };

        // creat vertex buffer to hold 4 vertices
        pd3dDevice->CreateVertexBuffer (sizeof(vertices), D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, &pVertexBuffer, NULL);

        // fill buffer
        void *pVertices;
        pVertexBuffer->Lock( 0, sizeof(vertices), (void**)&pVertices, 0 );
        memcpy(pVertices, vertices, sizeof(vertices));
        pVertexBuffer->Unlock();

        // create the vertex delcaration
        D3DVERTEXELEMENT9 decl[] =
        {
            {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
            {0, 3*sizeof(float), D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
            D3DDECL_END()
        };

        if (FAILED(pd3dDevice->CreateVertexDeclaration(decl, &pVertexDeclaration)))
        {
            SAFE_RELEASE(pVertexDeclaration);
        }
        else
            QuadValid=TRUE;

        m_nOldQuadW = W;
        m_nOldQuadH = H;
    }
}


//---------------------------------------------------------------------------------------------

void GIPDevice::DrawQuad(void)
{    
    pd3dDevice->SetVertexDeclaration(pVertexDeclaration);
    pd3dDevice->SetStreamSource(0, pVertexBuffer, 0, sizeof(GIP_CUSTOMVERTEX));
    pd3dDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, 0, 2);
}

void GIPDevice::DestroyQuad(void)
{
    m_nOldQuadW = -1;
    m_nOldQuadH = -1;
	QuadValid=FALSE;
	SAFE_RELEASE(pVertexBuffer);
	SAFE_RELEASE(pVertexDeclaration);
}

BOOL GIPDevice::IsQuadValid(void)
{
	return(QuadValid);
}
