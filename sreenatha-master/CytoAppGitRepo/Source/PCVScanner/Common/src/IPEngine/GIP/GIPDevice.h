///////////////////////////////////////////////////////////////////////////////////////////////
// GIP D3D device class - header file
// KR270805
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#pragma once

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

struct GIP_CUSTOMVERTEX
{
    float       x, y, z;
    float       u, v;
};

///////////////////////////////////////////////////////////////////////////////////////////////
// Our direct3d device
///////////////////////////////////////////////////////////////////////////////////////////////

class DllExport GIPDevice
{
public:
    GIPDevice(void);
    ~GIPDevice(void);
    BOOL Init(void);                                                // Initialise Direct3D
    void CreateQuad(int W, int H);                                  // Create a render quad for a given texture size
    void DrawQuad(void);                                            // Render the quad
	void DestroyQuad(void);											// Destroy the Quad
	BOOL IsQuadValid(void);

    IDirect3DDevice9* GetDevice(void) { return pd3dDevice; };		// Return the D3d device
	IDirect3D9* D3D(void) { return pD3D; };

private:
    LPDIRECT3DVERTEXBUFFER9 pVertexBuffer;                          // The rendering quad 
	LPDIRECT3DVERTEXDECLARATION9 pVertexDeclaration;

    IDirect3D9*				pD3D;                                   // D3D interfaces          
	IDirect3DDevice9*		pd3dDevice;
	BOOL					QuadValid;

    int                     m_nOldQuadW;
    int                     m_nOldQuadH;

	HWND					m_hWnd;

};
