///////////////////////////////////////////////////////////////////////////////////////////////
// Eucledian Distance Map implemented on the graphics card
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//
//
#include "stdafx.h"

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gipcommon.h"
#include "gip.h"
#include "gipeffect.h"
#include "giptexture.h"
#include "gipEDM.h"


///////////////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////////////////////

GIPEDM::GIPEDM(GIPDevice *Device)
{
    ASSERT(Device);
    m_pDevice = Device;
    m_pd3dDevice = m_pDevice->GetDevice();
    ASSERT(m_pd3dDevice);
    m_pGipEffect = NULL;
    m_bFXCreated = FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Destructor - free up everything 
///////////////////////////////////////////////////////////////////////////////////////////////

GIPEDM::~GIPEDM(void)
{
	SAFE_DELETE(m_pGipEffect);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation - not much to do here
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPEDM::Initialise()
{
    if (CreateFX()) return TRUE;
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create the FX
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPEDM::CreateFX(void)
{
	if (m_pGipEffect==NULL)
	{
		m_pGipEffect = new GIPEffect(m_pDevice);

		if (!m_pGipEffect->CreateFXFromResource(IDR_EDMFX))
			return FALSE;
		m_bFXCreated = TRUE;
	}
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply Euclean Distance Map using Jump Flooding
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPEDM::ApplyEDM(GIPTexture *pScr1, GIPTexture *pScr2, GIPTexture **pResult)
{
    ID3DXEffect	*pEffect = m_pGipEffect->Effect();
	int			swap, woff, hoff, extra;
	float		offsetX, offsetY;
	UINT		cPasses;

	pEffect->SetFloat("RatioW", (float)pScr1->m_nTexWidth/min(pScr1->m_nTexWidth, pScr1->m_nTexHeight));
	pEffect->SetFloat("RatioH", (float)pScr1->m_nTexHeight/min(pScr1->m_nTexWidth, pScr1->m_nTexHeight));
	pEffect->SetTechnique("EDM");
	swap=0;
	extra=0;
	woff=pScr1->m_nTexWidth;
	hoff=pScr1->m_nTexHeight;
	do
	{
		woff /=2;
		if (woff < 1) woff=1;
		hoff /=2;
		if (hoff <1) hoff=1;
		if ((woff==1) && (hoff==1)) extra +=1;
		offsetX=(float)woff/(float)pScr1->m_nTexWidth;
		offsetY=(float)hoff/(float)pScr1->m_nTexHeight;
		pEffect->SetFloat("OffsetX", offsetX);
		pEffect->SetFloat("OffsetY", offsetY);
		if (swap==0)
		{
			pEffect->SetTexture( "EDMImageTexture", pScr1->Texture());
			m_pd3dDevice->SetRenderTarget( 0, pScr2->Surface());
			*pResult=pScr2;
		}
		else
		{
			pEffect->SetTexture( "EDMImageTexture", pScr2->Texture());
			m_pd3dDevice->SetRenderTarget( 0, pScr1->Surface());
			*pResult=pScr1;
		}

		pEffect->Begin(&cPasses, 0);
		pEffect->BeginPass(0);
		pEffect->CommitChanges();
		m_pDevice->DrawQuad();
		pEffect->EndPass();

		swap = 1 - swap;
	}while (extra<=1);
	pEffect->End();
    return FALSE;
}

BOOL GIPEDM::ApplyPrepare(GIPTexture *pSource, GIPTexture *pTarget, long overlap, long bpp, BOOL IsWhiteBckg)
{
    UINT cPasses;

    ID3DXEffect *pEffect = m_pGipEffect->Effect();
    
	// set source images + params
	pEffect->SetTexture( "SourceImageTexture", pSource->Texture());
	pEffect->SetFloat("CorrTexWidth", (float)overlap/(float)pSource->m_nTexWidth);
	pEffect->SetFloat("CorrTexHeight", (float)overlap/(float)pSource->m_nTexHeight);
	if (bpp==8)
		pEffect->SetFloat("GreyScale",256);
	else
		pEffect->SetFloat("GreyScale",65536);
	pEffect->SetBool("IsWhiteBackground",IsWhiteBckg);
	m_pd3dDevice->SetRenderTarget( 0, pTarget->Surface());

	// Apply the named technique contained in the effect
	pEffect->SetTechnique("PrepareEDM");
	pEffect->Begin(&cPasses, 0);
	pEffect->BeginPass(0);	// only one pass
	pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	pEffect->EndPass();
	pEffect->End();
    return FALSE;
}

BOOL GIPEDM::ApplyFinish(GIPTexture *pSource, GIPTexture *pTarget, BOOL EDMOnly, long bpp, float NormFactor)
{
	float	bppFac;
    UINT	cPasses;

    ID3DXEffect *pEffect = m_pGipEffect->Effect();
    
	// set source images + params
	pEffect->SetTexture( "SourceImageTexture", pSource->Texture());
	pEffect->SetBool("EDMOnly",EDMOnly);
	pEffect->SetFloat("RatioW", (float)pSource->m_nTexWidth/min(pSource->m_nTexWidth, pSource->m_nTexHeight));
	pEffect->SetFloat("RatioH", (float)pSource->m_nTexHeight/min(pSource->m_nTexWidth, pSource->m_nTexHeight));
	if (bpp==8) bppFac=255.;
		else bppFac=65535;
	if (NormFactor<=0.)
		NormFactor=(float)sqrt((float)pSource->m_nTexWidth*(float)pSource->m_nTexWidth+(float)pSource->m_nTexHeight*(float)pSource->m_nTexHeight)/(float)bppFac;

	pEffect->SetFloat("NormFactor", (float)NormFactor);
	m_pd3dDevice->SetRenderTarget( 0, pTarget->Surface());

	// Apply the named technique contained in the effect
	pEffect->SetTechnique("FinishEDM");
	pEffect->Begin(&cPasses, 0);
	pEffect->BeginPass(0);	// only one pass
	pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	pEffect->EndPass();
	pEffect->End();
    return FALSE;
}

//---------------------------------------------------------------------------------------------
//	DistanceMap
//---------------------------------------------------------------------------------------------
void GIP::DistanceMap(GIP_IMAGE Src, GIP_IMAGE Dest, BOOL EDMOnly, BOOL IsWhiteBackground, float NormFactor)
{
	GIPTexture*	pResultTexture;
	long		w, h, overl, bpp;

	if (m_pEDM->Initialise())
	{
		(*Src)->GetImageDimensions(&w, &h, &overl, &bpp);		// get overlap for correction when expanding
		CreateScratch(&m_pScratch1, w, h, 0, 128);
		CreateScratch(&m_pScratch2, w, h, 0, 128);
		// setup display quad
		pDevice->CreateQuad(m_pScratch1->pTexture->m_nTexWidth, m_pScratch1->pTexture->m_nTexHeight);
		m_pEDM->ApplyPrepare((*Src)->pTexture, m_pScratch1->pTexture, overl, bpp, IsWhiteBackground);
		pResultTexture=m_pScratch1->pTexture;
		m_pEDM->ApplyEDM(m_pScratch1->pTexture, m_pScratch2->pTexture, &pResultTexture);
		m_pEDM->ApplyFinish(m_pScratch1->pTexture, (*Dest)->pTexture, EDMOnly, bpp, NormFactor);
	}	
}
