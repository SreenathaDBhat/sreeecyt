#pragma once



#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gipcommon.h"

class GIPDevice;
class GIPEffect;
class GIPTexture;
class GIPSwapTexture;

// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif


class GIPEDM
{
public:
    GIPEDM(GIPDevice *Device);
    ~GIPEDM(void);

    BOOL Create(int HSize, int VSize);              // Create the textures, scramblers and butterfly coefficients
    void Destroy(void);                             // Kill any internal workings
    BOOL Initialise();								// Initialisation 
	BOOL ApplyPrepare(GIPTexture *pSource, GIPTexture *pTarget, long overlap, long bpp, BOOL IsWhiteBckg);
	BOOL ApplyEDM(GIPTexture *pSource, GIPTexture *pScr1, GIPTexture **pTarget);
	BOOL ApplyFinish(GIPTexture *pSource, GIPTexture *pTarget, BOOL EDMOnly, long bpp, float NormFactor=1.);

private:
    BOOL CreateFX(void);                            // Load the code

    // Direct3D internals
    IDirect3DDevice9*	    m_pd3dDevice;
    GIPDevice               *m_pDevice;
    GIPEffect               *m_pGipEffect;			// effect interface
    GIPSwapTexture          *m_pSwapFilter;
    BOOL                    m_bFXCreated;           // Flag indicating success of FX creation

};
