///////////////////////////////////////////////////////////////////////////////////////////////
// GIP Effects class - source file
// KR270805
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//  
//

#include "stdafx.h"

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gip.h"
#include "gipdll.h"
#include "gipeffect.h"
#include "gipdevice.h"
#include "giptexture.h"

///////////////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////////////////////

GIPEffect::GIPEffect(GIPDevice *Device)
{
    ASSERT(Device);
    m_pDevice = Device;
    m_pd3dDevice = Device->GetDevice();
    ASSERT(m_pd3dDevice);
    m_pEffect = NULL;
    m_dwShaderFlags = 0;

	// Define DEBUG_VS and/or DEBUG_PS to debug vertex and/or pixel shaders with the 
    // shader debugger. Debugging vertex shaders requires either REF or software vertex 
    // processing, and debugging pixel shaders requires REF.  The 
    // D3DXSHADER_FORCE_*_SOFTWARE_NOOPT flag improves the debug experience in the 
    // shader debugger.  It enables source level debugging, prevents instruction 
    // reordering, prevents dead code elimination, and forces the compiler to compile 
    // against the next higher available software target, which ensures that the 
    // unoptimized shaders do not exceed the shader model limitations.  Setting these 
    // flags will cause slower rendering since the shaders will be unoptimized and 
    // forced into software.  See the DirectX documentation for more information about 
    // using the shader debugger.

#ifdef DEBUG_VS
    m_dwShaderFlags |= D3DXSHADER_DEBUG | D3DXSHADER_SKIPOPTIMIZATION | D3DXSHADER_FORCE_VS_SOFTWARE_NOOPT;
#endif
#ifdef DEBUG_PS
    m_dwShaderFlags |= D3DXSHADER_DEBUG | D3DXSHADER_SKIPOPTIMIZATION | D3DXSHADER_FORCE_PS_SOFTWARE_NOOPT;
#endif

}

GIPEffect::~GIPEffect(void)
{
    SAFE_RELEASE(m_pEffect);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create an effect from the file specified, returns FALSE on compilation error in the FX file
// or the file has not been found
///////////////////////////////////////////////////////////////////////////////////////////////
    
BOOL GIPEffect::CreateFXFromFile(char *FileName)     // Create an effect by compiling a file
{

	BOOL ret = TRUE;

    // NOTE if debug compilation then note any errors with the FX file we are attempting to load
    LPD3DXBUFFER  pBuff = NULL;

    // Load image processing effects file
	if (D3DXCreateEffectFromFile(m_pd3dDevice, 
                                 FileName, 
                                 NULL, 
                                 NULL, 
                                 m_dwShaderFlags, 
                                 NULL, 
                                 &m_pEffect, 
                                 &pBuff) != D3D_OK)
	{	
        CString Msg;
#ifdef _DEBUG
        if (pBuff)
		{
            Msg.Format("Failed to compile/load %s\r\n%s", FileName, pBuff->GetBufferPointer());
		}
        else
            Msg.Format("Failed to compile/load %s\r\n", FileName);

		MessageBox(NULL, Msg, "DirectX", MB_ICONERROR);
#else
        Msg.Format("Failed to load fx from file %s\r\n", FileName);
        OutputDebugString(Msg);
#endif

		ret = FALSE;
	}

    if (pBuff)
		pBuff->Release();

    return ret;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Create an effect from the resource specified, returns FALSE on compilation error in the FX file
///////////////////////////////////////////////////////////////////////////////////////////////////
    
BOOL GIPEffect::CreateFXFromResource(UINT id)     // Create an effect by compiling a file
{
    // NOTE if debug compilation then note any errors with the FX file we are attempting to load
	BOOL ret = TRUE;

    LPD3DXBUFFER pBuff = NULL;
	// load image processing effects file from resource
	if (D3DXCreateEffectFromResource(	m_pd3dDevice, 
										GetGIPInstanceHandle(), //handle to source module
										MAKEINTRESOURCE(id),
										NULL,
										NULL,
										m_dwShaderFlags,
										NULL,
										&m_pEffect, 
										&pBuff) != D3D_OK)
	{	
        CString Msg;

#ifdef _DEBUG
        Msg.Format("Failed to compile/load imageprocessing.fx\r\n%s", pBuff->GetBufferPointer());
		MessageBox(NULL, Msg, "DirectX", MB_ICONERROR);
#else
        Msg = "Failed to load ImageProcessing.fx from GIP resources\r\n";
        OutputDebugString(Msg);
#endif

		ret = FALSE;
	}

	if(pBuff) 
		pBuff->Release();

    return ret;
}

////////////////////////////////////////////////////////////////////////////////
// Set a single register
////////////////////////////////////////////////////////////////////////////////

HRESULT GIPEffect::SetRegister(int RegNum, float A, float B, float C, float D)
{
    HRESULT hr = S_OK;
    D3DXVECTOR4 constants10To17[8];

	constants10To17[0].x = A;
	constants10To17[0].y = B;
	constants10To17[0].z = C;
	constants10To17[0].w = D;

    ///////////////////////////////
    // Load offsets into cRegNum
    ///////////////////////////////
    hr = m_pd3dDevice->SetPixelShaderConstantF(RegNum, &constants10To17[0].x, 1);
    return hr;
}

////////////////////////////////////////////////////////////////////////////////
// SetPixelOffsetConstants
// Load pixel offset constants. Offset constants are 1/width and 1/height.
// These constants are used for reading the 8 neighboring pixels.
////////////////////////////////////////////////////////////////////////////////
HRESULT GIPEffect::SetPixelOffsetConstants(int W, int H)
{
    HRESULT hr = S_OK;
    float width, height; //of each pixel
    D3DXVECTOR4 constants10To17[8];

    
    if (W == 0 || H == 0)
    {
        return S_FALSE;
    }

    width = 1.f / (float) W;
    height = 1.f /( float) H;
    memset(constants10To17,0,sizeof(constants10To17));

    /////////////
    // TOP ROW //
    /////////////
    constants10To17[0].x = -width;         //c10.x// coord for up left
    constants10To17[0].y = -height;        //c10.y// coord for up left

    constants10To17[1].x = 0.0;            //c11.x// coord for up
    constants10To17[1].y = -height;        //c11.y// coord for up

    constants10To17[2].x = width;          //c12.x// coord for up right 
    constants10To17[2].y = -height;        //c12.y// coord for up right 

    ////////////////
    // CENTER ROW //
    ////////////////
    constants10To17[3].x = -width;         //c13.x// coord for left 
    constants10To17[3].y = 0.0;            //c13.y// coord for left 

    constants10To17[4].x = width;          //c14.x// coord for right 
    constants10To17[4].y = 0.0;            //c14.y// coord for right 


    ////////////////
    // BOTTOM ROW //
    ////////////////
    constants10To17[5].x = -width;         //c15.x// coord for down left
    constants10To17[5].y = height;         //c15.y// coord for down left

    constants10To17[6].x = 0.0;            //c16.y// coord for down 
    constants10To17[6].y = height;         //c16.x// coord for down 

    constants10To17[7].x = width;          //c17.x// coord for down right 
    constants10To17[7].y = height;         //c17.y// coord for down right 


    ///////////////////////////////
    // Load offsets into c10-c17 //
    ///////////////////////////////
    hr = m_pd3dDevice->SetPixelShaderConstantF(10, &constants10To17[0].x, 8);
    return hr;
}

////////////////////////////////////////////////////////////////////////////////
// Ping - Pong two techniques on alternate passes
// NOTE it uses two intermediate swap texture buffers 
////////////////////////////////////////////////////////////////////////////////

void GIPEffect::PingPong(char *pass1, char *pass2, int nPasses, 
                         GIPTexture *pSrc, GIPTexture *pDest, GIPTexture *pSwap1, GIPTexture *pSwap2)
{
	UINT cPasses;
    GIPTexture *pSwap[2];

	if (nPasses <= 0)
		return;

	if (!pass1 || !pass2 || nPasses <= 0)
		return;

	if (!m_pd3dDevice || !m_pEffect || !pSrc || !pDest || !pSwap1 || !pSwap2)
		return;

	// init swap buffer index
	int swap = 0;

    pSwap[0] = pSwap1;
    pSwap[1] = pSwap2;

	// set source image for first pass
    m_pEffect->SetTexture("SourceImageTexture", pSrc->Texture());

	for (int iPass = 0; iPass < nPasses; iPass++)
    {
		// set render target - should be final target buffer on last pass
		if (iPass == nPasses-1)
            m_pd3dDevice->SetRenderTarget(0, pDest->Surface());
		else
            m_pd3dDevice->SetRenderTarget(0, pSwap[swap]->Surface());

		// swap technique to achieve round effect
		if (iPass & 1)
			m_pEffect->SetTechnique(pass1);
		else
			m_pEffect->SetTechnique(pass2);

		// Apply the technique contained in the effect - returns num of passes in cPasses
		m_pEffect->Begin(&cPasses, 0);

		m_pEffect->BeginPass(0);	// only one pass in each morph techniqe - pass 0
        
        SetPixelOffsetConstants(pSrc->m_nTexWidth, pSrc->m_nImageHeight);

		// The effect interface queues up the changes and performs them 
		// with the CommitChanges call. You do not need to call CommitChanges if 
		// you are not setting any parameters between the BeginPass and EndPass.
		m_pEffect->CommitChanges();

		// Render the simple quad with the applied technique
		m_pDevice->DrawQuad();

		if (iPass != nPasses-1)
		{
	        // set source image for first pass
	        m_pEffect->SetTexture( "SourceImageTexture", pSwap[swap]->Texture());
			// and set target to the other buffer
			swap ^= 1;
		}

		// end tecnnique pass
		m_pEffect->EndPass();

		// end technique
		m_pEffect->End();
    }
}

////////////////////////////////////////////////////////////////////////////////
// Perform a multipass technique on an image
// NOTE it uses two intermediate swap texture buffers 
////////////////////////////////////////////////////////////////////////////////

void GIPEffect::MultipassTechnique(char *Name, 
                                   GIPTexture *pSrc, GIPTexture *pDest, 
                                   GIPTexture *pScratch0, GIPTexture *pScratch1, 
                                   BOOL SetConstants, int Register, int NRegisters, D3DXVECTOR4 *pConst)
{
	UINT cPasses, i;
    HRESULT hr;
    GIPTexture *pScratch[2];

	if ((!Name) || (!m_pEffect))
		return;
	
    pScratch[0] = pScratch0;
    pScratch[1] = pScratch1;

	hr = m_pEffect->SetTechnique(Name);

	// Apply the technique contained in the effect - returns num of passes in cPasses
	hr = m_pEffect->Begin(&cPasses, 0);

    for (i = 0; i < cPasses; i++)
    {
        if (i == 0)
        {
	        // set source image 
	        hr = m_pEffect->SetTexture("SourceImageTexture", pSrc->Texture());
	  	    // set render target texture
	        hr = m_pd3dDevice->SetRenderTarget( 0, pScratch[0]->Surface());
        }
        else
        if (i != cPasses - 1)
        {
            if (i & 1)
            {
	            // set source image 
	            hr = m_pEffect->SetTexture("SourceImageTexture", pScratch[0]->Texture());
	  	        // set render target texture
	            hr = m_pd3dDevice->SetRenderTarget( 0, pScratch[1]->Surface());
            }
            else
            {
	            // set source image 
	            hr = m_pEffect->SetTexture("SourceImageTexture", pScratch[1]->Texture());
	  	        // set render target texture
	            hr = m_pd3dDevice->SetRenderTarget( 0, pScratch[0]->Surface());
            }

        }
        else
        {
            if (i & 1)
            {
	            // set source image 
	            hr = m_pEffect->SetTexture("SourceImageTexture", pScratch[0]->Texture());
	  	        // set render target texture
	            hr = m_pd3dDevice->SetRenderTarget( 0, pDest->Surface());
            }
            else
            {
	            // set source image 
	            hr = m_pEffect->SetTexture("SourceImageTexture", pScratch[1]->Texture());
	  	        // set render target texture
	            hr = m_pd3dDevice->SetRenderTarget( 0, pDest->Surface());
            }
        }

        hr = m_pEffect->BeginPass(i);	            // Do pass i

        if (SetConstants && i == 0)
            hr = m_pd3dDevice->SetPixelShaderConstantF(Register, &pConst->x, NRegisters);
            //hr = SetPixelOffsetConstants(pSrc->m_nTexWidth, pSrc->m_nTexHeight);

	    // The effect interface queues up the changes and performs them 
	    // with the CommitChanges call. You do not need to call CommitChanges if 
	    // you are not setting any parameters between the BeginPass and EndPass.
	    hr = m_pEffect->CommitChanges();

	    // Render the simple quad with the applied technique
	    m_pDevice->DrawQuad();

	    // end tecnnique pass
	    hr = m_pEffect->EndPass();
    }

	// end technique
	hr = m_pEffect->End();
}
