#pragma once

///////////////////////////////////////////////////////////////////////////////////////////////
// GIP Effects class - source file
// KR270805
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gipcommon.h"

class GIPDevice;
class GIPTexture;

class GIPEffect
{
public:
    GIPEffect(GIPDevice *Device);
    ~GIPEffect(void);

    BOOL    CreateFXFromFile(char *FileName);       // Create an effect by compiling a file
    BOOL    CreateFXFromResource(UINT id);          // Create an effect by compiling from a resource

    // Perform two alternate shader passes
    void    PingPong(char *pass1, char *pass2, int nPasses, GIPTexture *pSrc, GIPTexture *pDest, GIPTexture *pSwap1, GIPTexture *pSwap2);
    
    // Perform a multipass shader technique
    void    MultipassTechnique(char *Name, 
                                GIPTexture *pSrc, GIPTexture *pDest, 
                                GIPTexture *pSwap1, GIPTexture *pSwap2, 
                                BOOL SetConstants, int Register, int NRegisters, D3DXVECTOR4 *pConst);

    HRESULT SetPixelOffsetConstants(int W, int H);  // Set x,y offsets for the neighbourhood directly around a pixel
	// Set a register on the GPU
	HRESULT SetRegister(int RegNum, float A, float B, float C, float D);


    // Accessors
    void    SetShaderFlags(DWORD Flags) { m_dwShaderFlags = Flags; };   // Set up shader flags - useful for shader debugging
    ID3DXEffect* Effect(void)   { return m_pEffect; };

private:
    ID3DXEffect                 *m_pEffect;		    // D3DX effect interface
    IDirect3DDevice9          *m_pd3dDevice;       // Our D3D device
    GIPDevice                   *m_pDevice;         // Our GIP Device
    DWORD                       m_dwShaderFlags;    // Changing these allows us to debug vertex/pixel shaders

};
