///////////////////////////////////////////////////////////////////////////////////////////////
// FFT and inverse FFT implemented on the graphics card
// Written By Karl Ratcliff     190805
///////////////////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gipcommon.h"
#include "gip.h"
#include "gipeffect.h"
#include "giptexture.h"
#include "gipfft.h"


///////////////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////////////////////

GIPFFT::GIPFFT(GIPDevice *Device)
{
    int i;
    ASSERT(Device);
    m_pDevice = Device;
    m_pd3dDevice = m_pDevice->GetDevice();
    ASSERT(m_pd3dDevice);
	for (i=0; i < (4*MaxPower2); i++)
	{
		m_pButterfly[i] = NULL;
		m_pSwapButterfly[i] = NULL;
	}
	for (i=0; i<2; i++)
	{
		m_pScramble[i] = NULL;
		m_pSwapScramble[i] = NULL;
		m_nFFTSize[i]=0;
		m_nPower2[i]=0;
	}

	m_pFilter = NULL;
	m_pSwapFilter = NULL;
    m_pGipEffect = NULL;
	m_bFXCreated = FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Destructor - free up everything 
///////////////////////////////////////////////////////////////////////////////////////////////

GIPFFT::~GIPFFT(void)
{
	Destroy();
	SAFE_DELETE(m_pGipEffect);
	SAFE_DELETE(m_pFilter);
	SAFE_DELETE(m_pSwapFilter);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Releases any attached surfaces, NOTE does nothing with the effect
///////////////////////////////////////////////////////////////////////////////////////////////

void GIPFFT::Destroy(void)
{
    int i;
	for (i=0; i < (4*MaxPower2); i++)
	{
		SAFE_DELETE(m_pButterfly[i]);
		SAFE_DELETE(m_pSwapButterfly[i]);
	}
	for (i=0; i<2; i++)
	{
		SAFE_DELETE(m_pScramble[i]);
		SAFE_DELETE(m_pSwapScramble[i]);
		m_nFFTSize[i]=0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation - not much to do here
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPFFT::Initialise()
{
    if (CreateFX()) return TRUE;
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create the FX
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPFFT::CreateFX(void)
{
	if (m_pGipEffect==NULL)
	{
		m_pGipEffect = new GIPEffect(m_pDevice);

        if (!m_pGipEffect->CreateFXFromResource(IDR_FFTFX))
			return FALSE;
		m_bFXCreated = TRUE;
	}
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Creates working textures for butterfly calcs and scrambling phases of the FFT
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPFFT::CreateTextures(int HSize, int VSize)
{
    BOOL	Success = FALSE;
    int		i, k, j;

    // Test if we have already allocated working textures of the same dimensions 
    if ((HSize == m_nFFTSize[0]) && (VSize == m_nFFTSize[1]))
        return TRUE;            // Yes exit now
   
    // Destroy any previously allocated surfaces
    Destroy();
	try
    {
        //compute the power 
		m_nFFTSize[0]=HSize;
		m_nFFTSize[1]=VSize;
		for (i=0; i<2; i++)
		{
			m_nPower2[i] = 0;
			while (m_nFFTSize[i] > (1<<m_nPower2[i]))
			{
				m_nPower2[i]++;
			}
			// Horizontal or Verical Scramble Texture + Swap Texture
			m_pScramble[i] = new GIPTexture(m_pd3dDevice);
			if (!m_pScramble[i]->CreateFP(m_nFFTSize[i], 1))throw;
			m_pSwapScramble[i] = new GIPSwapTexture(m_pd3dDevice);
			if (!m_pSwapScramble[i]->CreateFP(m_nFFTSize[i], 1)) throw;
		}
		for (k=0; k < (4*MaxPower2); k += (2*MaxPower2))
		{
			for (i=0; i < 2; i++)
			{
				for (j=0; j < m_nPower2[i]; j++)
				{
					//  Textures for Horizontal and Vertical Butterfly passes for FFT and IFFT
					//	+ corresponding Swap Textures
					m_pButterfly[k+i*MaxPower2+j] = new GIPTexture(m_pd3dDevice);
					if (!m_pButterfly[k+i*MaxPower2+j]->CreateFP(m_nFFTSize[i], 1)) throw;
					m_pSwapButterfly[k+i*MaxPower2+j] = new GIPSwapTexture(m_pd3dDevice);
					if (!m_pSwapButterfly[k+i*MaxPower2+j]->CreateFP(m_nFFTSize[i], 1)) throw;
				}
			}
		}
        Success = TRUE;
    }
    
    catch(char * str)
    {
        CString msg;
        
        msg.Format("ERROR COULD NOT ALLOCATE FFT WORKING TEXTURES %s", str);
        OutputDebugString(msg);
        Destroy();
    }
    return Success;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Scrambler texture
///////////////////////////////////////////////////////////////////////////////////////////////

void GIPFFT::CreateScrambleTexture(void)
{
	D3DXVECTOR4 *fImage;
	D3DXVECTOR4 temp;
    int gg, jj, i;

	for (i=0; i< 2; i++)
	{
		// Calc the Horizontal or Vertical scramble coordinates
		fImage = new D3DXVECTOR4[m_nFFTSize[i]];
		for (gg = 0; gg < m_nFFTSize[i]; gg++)
		{
			fImage[gg].x = (float)(gg + 0.5f)/(float)(m_nFFTSize[i]);
			fImage[gg].y = fImage[gg].x;
			fImage[gg].z = fImage[gg].x;
			fImage[gg].w = fImage[gg].x;
		}
		// Apply Scramble
		jj = 0;
		for (gg = 0; gg < m_nFFTSize[i]; gg++)
		{
			if ( gg > jj)
			{
				temp = fImage[jj];
				fImage[jj] = fImage[gg];
				fImage[gg] = temp;
			}

			int m = (m_nFFTSize[i]) >> 1;
			while ((jj >= m) & (m >= 2))
			{
				jj -= m;
				m = m >>1;
			}
			jj += m;
		}
		// Set the textureH for transmission to the GPU
		m_pSwapScramble[i]->Put(fImage);
		m_pSwapScramble[i]->Transmit(m_pScramble[i]);
		delete [] fImage;
	}
}


////////////////////////////////////////////////////////////////////////////
// Compute the butterfly coefficients
////////////////////////////////////////////////////////////////////////////

void GIPFFT::CreateFourierCoeff(void)                  
{
    D3DXVECTOR4	*fImage;
    float		angle;
    float		wre, wim, wpre, wpim;
    int			jj, ikj, ikg, i;
    int			N, half_N, kk, pow2;
    float		wtemp;

	for (i=0; i<4; i++)
	{
		// calc Bufferfly Textures for resp Horizontal FFT, Vertical FFT, 
		// Horizontal Inverse FFT and Vertical Inverse FFT 
		kk=m_nFFTSize[i&1];
		pow2=m_nPower2[i&1];
		fImage = new D3DXVECTOR4[kk];
		N = 1;
		for (int ii=0; ii < pow2; ii++) // calc. pow2 butterfly passes
		{
			// Zero the texture initially
			memset(fImage, 0, kk  * sizeof(D3DXVECTOR4));

			half_N = N;
			N <<= 1;
			angle = -2.0f * 3.1415927f / (float) N; //*dir
			if (i>=2) angle = -angle;		// inverse Fourier
			wtemp = sinf(0.5f * angle);
			wpre = -2.0f * wtemp *wtemp;
			wpim = sinf(angle);
			wre = 1.0f;
			wim = 0.0f;
			for (int offset=0; offset < half_N; offset++)
			{
				for (int gg = offset; gg < kk; gg += N)
				{
					jj = gg + half_N;
					ikj = jj;
					ikg = gg;

					// Store the coords in r and g
					fImage[ikj].x = -((jj + 0.5f) / (float)kk);
					fImage[ikj].y = (gg + 0.5f) / (float)kk;
					fImage[ikg].x = (jj + 0.5f) / (float)kk;
					fImage[ikg].y = (gg + 0.5f) / (float)kk;

					// Store W in b and a
					fImage[ikj].z = wre;
					fImage[ikj].w = wim;
					fImage[ikg].z = wre;
					fImage[ikg].w = wim;
				}
				wtemp = wre;
				wre = wtemp * wpre - wim * wpim + wre;
				wim = wim * wpre + wtemp * wpim + wim;
			}
			// Set the texture up ready for transmit to GPU
			m_pSwapButterfly[i*MaxPower2+ii]->Put(fImage);
			m_pSwapButterfly[i*MaxPower2+ii]->Transmit(m_pButterfly[i*MaxPower2+ii]);
		}
		delete [] fImage;
	}
}

////////////////////////////////////////////////////////////////////////////
// Create all the parts required to do the FFT
// NOTE if this is successful, then we are all clear to do FFTs
////////////////////////////////////////////////////////////////////////////

BOOL GIPFFT::Create(int HSize, int VSize)
{
    BOOL Success = FALSE;

    if (!m_bFXCreated)
        m_bFXCreated = CreateFX();

    if (m_bFXCreated && CreateTextures(HSize, VSize))
    {
        CreateScrambleTexture();
        CreateFourierCoeff();
        Success = TRUE;
    }

    return Success;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// CreateButterWorthFilter
///////////////////////////////////////////////////////////////////////////////////////////////
BOOL GIPFFT::CreateButterWorthFilter(FilterType passType, double f1, double f2, double order)
{
    D3DXVECTOR4	*fltr;			// filter vector
	long	nRowD2, nColD2;		// half of row/column  
	double	bigger;				// passband fraction fct of bigger axis  
	long	maxRad;             // max. sampling radius  
	double	tempL, tempH;
	long	i;
	BOOL	ok;
	
	// allocate filter vector -- radius from (u,v)=(0,0) to corners of freq plot  
	nRowD2 = m_nFFTSize[0]/ 2;
	nColD2 = m_nFFTSize[1]/ 2;
	bigger = (double) (nRowD2 > nColD2) ? nRowD2 : nColD2;
	maxRad = (long) sqrt ((double) (nRowD2 * nRowD2 + nColD2 * nColD2)) + 1;
	fltr = new D3DXVECTOR4[maxRad];
	
	// determine filter vector coefficients for chosen passband type of filter  
	order *= 2.0;
	ok=TRUE;
	switch (passType) 
	{
		case LowPass:                      // low-pass  
			f1 *= bigger;
			for (i = 0; i < maxRad; i++)
				fltr[i].x = (float)(1.0 / (1.0 + 0.414 * pow (((double) i / f1), order)));
			break;

		case HighPass:                      // high-pass  
			f1 *= bigger;
			for (i = 0; i < maxRad; i++)
				fltr[i].x = (float)(1.0 - 1.0 / (1.0 + 0.414 * pow (((double) i / f1), order)));
			break;

		case BandPass:                      // band-pass  
			f1 *= bigger;
			f2 *= bigger;
			for (i = 0; i < maxRad; i++) 
			{
				tempL = 1.0 / (1.0 + 0.414 * pow (((double) i / f1), order));
				tempH = 1.0 - 1.0 / (1.0 + 0.414 * pow (((double) i / f2), order));
				fltr[i].x = (float)(tempL * tempH);
			}
			break;

		case BandStop:                      // and band-stop  
			f1 *= bigger;
			f2 *= bigger;
			for (i = 0; i < maxRad; i++) 
			{
				tempL = 1.0 / (1.0 + 0.414 * pow (((double) i / f1), order));
				tempH = 1.0 - 1.0 / (1.0 + 0.414 * pow (((double) i / f2), order));
				fltr[i].x = (float)(tempL + tempH);
			}
			break;

		default:
			ok=FALSE;
			break;
	}	
	if (ok)
	{
		SAFE_DELETE(m_pFilter);
		SAFE_DELETE(m_pSwapFilter);
		m_pFilter = new GIPTexture(m_pd3dDevice);
		m_pFilter->CreateFP(maxRad, 1);
		m_pSwapFilter = new GIPSwapTexture(m_pd3dDevice);
		m_pSwapFilter->CreateFP(maxRad, 1);
		for (i = 0; i < maxRad; i++) 
		{
			fltr[i].y=fltr[i].x;
			fltr[i].z=fltr[i].x;
			fltr[i].w=fltr[i].x;
		}
		m_pSwapFilter->Put(fltr);
		m_pSwapFilter->Transmit(m_pFilter);
	}	
	delete fltr;
	return (ok);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// CreateGaussianFilter
///////////////////////////////////////////////////////////////////////////////////////////////
BOOL GIPFFT::CreateGaussianFilter(FilterType passType, double f1, double f2)
{
    D3DXVECTOR4	*fltr;			// filter vector
	long	nRowD2, nColD2, i;	// half of row/column  
	double	bigger;				// passband fraction fct of bigger axis  
	double	stdDevL, stdDevH;	// std. dev.s of low and high pass fltrs 
	long	maxRad;				// max. sampling radius  
	double	tempL, tempH;
	BOOL	ok;
	
	// allocate filter vector -- radius from (u,v)=(0,0) to corners of freq plot  
	nRowD2 = m_nFFTSize[0]/ 2;
	nColD2 = m_nFFTSize[1]/ 2;
	bigger = (double) (nRowD2 > nColD2) ? nRowD2 : nColD2;
	maxRad = (long) sqrt ((double) (nRowD2 * nRowD2 + nColD2 * nColD2)) + 1;
	fltr = new D3DXVECTOR4[maxRad];
	
	// determine filter vector coefficients for chosen type of filter  
	ok=TRUE;
	switch (passType) 
	{
		case LowPass:                      // low-pass  
			stdDevL = f1 / ROOTLOG2 * bigger;
			for (i = 0; i < maxRad; i++)
				fltr[i].x = (float)(exp (-(double) i * (double) i / (2.0 * stdDevL * stdDevL)));
			break;

		case HighPass:                      // high-pass  
			stdDevH = f1 / ROOTLOG2 * bigger;
			for (i = 0; i < maxRad; i++)
				fltr[i].x = (float)(1.0 - exp (-(double) i * (double) i / (2.0 * stdDevH * stdDevH)));
			break;

		case BandPass:                      // band-pass  
			stdDevH = f1 / ROOTLOG2 * bigger;
			stdDevL = f2 / ROOTLOG2 * bigger;
			for (i = 0; i < maxRad; i++) 
			{
				tempL = exp (-(double) i * (double) i / (2.0 * stdDevL * stdDevL));
				tempH = 1.0 - exp (-(double) i * (double) i / (2.0 * stdDevH * stdDevH));
				fltr[i].x = (float)(tempL * tempH);
			}
			break;

		case BandStop:                      // and band-stop  
			stdDevL = f1 / ROOTLOG2 * bigger;
			stdDevH = f2 / ROOTLOG2 * bigger;
			for (i = 0; i < maxRad; i++) 
			{
				tempL = exp (-(double) i * (double) i / (2.0 * stdDevL * stdDevL));
				tempH = 1.0 - exp (-(double) i * (double) i / (2.0 * stdDevH * stdDevH));
				fltr[i].x = (float)(tempL + tempH);
			}
			break;

		default:
			ok = FALSE;
			break;
	}
	if (ok)
	{
		SAFE_DELETE(m_pFilter);
		SAFE_DELETE(m_pSwapFilter);
		m_pFilter = new GIPTexture(m_pd3dDevice);
		m_pFilter->CreateFP(maxRad, 1);
		m_pSwapFilter = new GIPSwapTexture(m_pd3dDevice);
		m_pSwapFilter->CreateFP(maxRad, 1);
		for (i = 0; i < maxRad; i++) 
		{
			fltr[i].y=fltr[i].x;
			fltr[i].z=fltr[i].x;
			fltr[i].w=fltr[i].x;
		}
		m_pSwapFilter->Put(fltr);
		m_pSwapFilter->Transmit(m_pFilter);
	}	
	delete fltr;
	return (ok);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply it
//	flag = <0  fft forward
//		   >=0 fft inverse
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPFFT::ApplyFFT(GIPTexture *pSource, GIPTexture *pTarget, GIPTexture *pScratch, long flag)
{
	int			swap, offset;
	GIPTexture	*pTex0, *pTex1;
	UINT		cPasses;
	ID3DXEffect *pEffect = m_pGipEffect->Effect();

	if (flag >=0)
	{
		offset = 2*MaxPower2;	// offset for butterfly filters inverse fft
		pTex0 = pSource;		// the source and the scratch are the floating point textures
		pTex1 = pScratch;
	}
	else
	{
		offset = 0;
		if ((m_nPower2[0] +	m_nPower2[1]) & 1)	// take care that the last texture is pTarget
		{										// for forward fft. It saves an extra copy
			pTex0 = pScratch;					// the target and the scratch are the floating point textures
			pTex1 = pTarget;
		}
		else
		{
			pTex0 = pTarget;
			pTex1 = pScratch;
		}
	}
	swap = 0;
	pEffect->SetTechnique("fft2D");
	pEffect->SetTexture( "SourceImageTexture", pSource->Texture());
	pEffect->SetTexture( "ScrambleTexture", m_pScramble[0]->Texture());
	m_pd3dDevice->SetRenderTarget( 0, pTex1->Surface());
	pEffect->Begin(&cPasses, 0);
	pEffect->BeginPass(0);			// pass 0 - Horizontal Scramble
	pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	pEffect->EndPass();

	pEffect->BeginPass(1);			// pass 1 - Horizontal Butterfly
	for (int iPass = 0; iPass < m_nPower2[0]; iPass++)
	{
		pEffect->SetTexture( "ButterflyTexture", this->m_pButterfly[iPass+offset]->Texture());
		if (swap==0)
		{
			pEffect->SetTexture( "SourceImageTexture", pTex1->Texture());
			m_pd3dDevice->SetRenderTarget( 0, pTex0->Surface());
		}
		else
		{
			pEffect->SetTexture( "SourceImageTexture", pTex0->Texture());
			m_pd3dDevice->SetRenderTarget( 0, pTex1->Surface());
		}
		swap=1-swap;
		pEffect->CommitChanges();
		m_pDevice->DrawQuad();
	}
	pEffect->EndPass();

	pEffect->BeginPass(2);	// pass 2 - Vertical Scramble
	pEffect->SetTexture( "ScrambleTexture", m_pScramble[1]->Texture());
	if (swap==0)
	{
		pEffect->SetTexture( "SourceImageTexture", pTex1->Texture()); 
		m_pd3dDevice->SetRenderTarget( 0, pTex0->Surface());
	}
	else
	{
		pEffect->SetTexture( "SourceImageTexture", pTex0->Texture());
		m_pd3dDevice->SetRenderTarget( 0, pTex1->Surface());
	}
	swap=1-swap;
	pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	pEffect->EndPass();

	pEffect->BeginPass(3);	// pass 3 Vertical Butterfly
	for (int iPass = 0; iPass < m_nPower2[1]; iPass++)
	{
		pEffect->SetTexture( "ButterflyTexture", this->m_pButterfly[iPass+MaxPower2+offset]->Texture());
		if (swap==0)
		{
			pEffect->SetTexture( "SourceImageTexture", pTex1->Texture());
			m_pd3dDevice->SetRenderTarget( 0, pTex0->Surface());
		}
		else
		{
			pEffect->SetTexture( "SourceImageTexture", pTex0->Texture());
			m_pd3dDevice->SetRenderTarget( 0, pTex1->Surface());
		}
		swap=1-swap;
		pEffect->CommitChanges();
		m_pDevice->DrawQuad();
	}
	pEffect->EndPass();

	if (flag >=0)
	{
		pEffect->BeginPass(4);	// pass 4	Rescale in case of inverse fft
		pEffect->SetFloat("ScaleFactor",(float)1./((float)pSource->m_nTexWidth*(float)pSource->m_nTexHeight));
		if (swap==0)
			pEffect->SetTexture( "SourceImageTexture", pTex1->Texture());
		else
			pEffect->SetTexture( "SourceImageTexture", pTex0->Texture());
		m_pd3dDevice->SetRenderTarget( 0, pTarget->Surface());
		pEffect->CommitChanges();
		m_pDevice->DrawQuad();
		pEffect->EndPass();
	}
	// end technique
	pEffect->End();
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply it
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPFFT::Apply(char *name, GIPTexture *pSource, GIPTexture *pSource2, GIPTexture *pTarget, long overlap)
{
    UINT cPasses;

    ID3DXEffect *pEffect = m_pGipEffect->Effect();
    
	// set source images + params
	pEffect->SetTexture( "SourceImageTexture", pSource->Texture());
	m_pd3dDevice->SetRenderTarget( 0, pTarget->Surface());

	if (name=="GreyToColour")
	{
		pEffect->SetFloat("CorrTexWidth", (float)overlap/(float)pSource->m_nTexWidth);
		pEffect->SetFloat("CorrTexHeight", (float)overlap/(float)pSource->m_nTexHeight);
	}
	else if (name=="FilterFFT")
		pEffect->SetTexture( "FilterTexture", this->m_pFilter->Texture());
	else if (name=="MultiplyFFT")
		pEffect->SetTexture("SourceImage2Texture", pSource2->Texture());
	pEffect->SetTechnique(name);
	// Apply the technique contained in the effect - returns num of passes in cPasses
	pEffect->Begin(&cPasses, 0);
	pEffect->BeginPass(0);	// only one pass
	pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	pEffect->EndPass();
	pEffect->End();
    return FALSE;
}

//---------------------------------------------------------------------------------------------
//	ColourToGrey (in fft.fx)
//---------------------------------------------------------------------------------------------
void GIP::ColourToGrey(GIP_IMAGE src, GIP_IMAGE dest)
{
	long			w, h, bpp, overl;
	if ((!m_pColourConv) || (!src) || (!dest)) return;
	if (!(*src)->SameDimensions(*dest)) return;
	(*src)->GetImageDimensions(&w, &h, &overl, &bpp);
	if ((bpp != 24) && (bpp != 48)) return;
	if (m_pFFT->Initialise())
	{
		// setup display quad
		pDevice->CreateQuad((*src)->pTexture->m_nTexWidth, (*src)->pTexture->m_nTexHeight);
		m_pFFT->Apply("ColourToGrey", (*src)->pTexture, NULL, (*dest)->pTexture, overl);
	}
}

//---------------------------------------------------------------------------------------------
//	GreyToColour (in fft.fx)
//---------------------------------------------------------------------------------------------
void GIP::GreyToColour(GIP_IMAGE src, GIP_IMAGE dest)
{
	long	w0, h0, bpp0, overl0;
	long	w1, h1, bpp1, overl1;
	if ((!m_pColourConv) || (!src) || (!dest)) return;
	(*src)->GetImageDimensions(&w0, &h0, &overl0, &bpp0);
	(*dest)->GetImageDimensions(&w1, &h1, &overl1, &bpp1);
	if ((w0!=w1) || (h0 != h1) || ((bpp0!=8) && (bpp0 !=16)))
	{
		return;
	}
	if (((bpp0==8) && (bpp1 != 24)) || ((bpp0==16) && (bpp1 != 48))) 
	{
		TRACE("GreyToColour 2\r\n");
		return;
	}
	if (m_pFFT->Initialise())
	{
		// setup display quad
		pDevice->CreateQuad((*dest)->pTexture->m_nTexWidth, (*dest)->pTexture->m_nTexHeight);
		m_pFFT->Apply("GreyToColour",(*src)->pTexture, NULL, (*dest)->pTexture, overl0);
	}
}

//---------------------------------------------------------------------------------------------
//	fft2DPowerSpectrum (in fft.fx)
//---------------------------------------------------------------------------------------------
GIPImage** GIP::fft2DPowerSpectrum(GIP_IMAGE src)
{
	GIPImage**	dest=NULL;
	long		w, h, bpp, overl;

	if (m_pFFT->Initialise())
	{
		(*src)->GetImageDimensions(&w, &h, &overl, &bpp);
		dest = GIPCreateImage(w, h, 0, 24);
        if (dest)
        {
		    pDevice->CreateQuad((*dest)->pTexture->m_nTexWidth, (*dest)->pTexture->m_nTexHeight);
			TRACE("Apply Powerspectrum\r\n");
		    m_pFFT->Apply("PowerSpectrum", (*src)->pTexture, NULL, (*dest)->pTexture, 0);
        }
	}
	return (dest);
}

//---------------------------------------------------------------------------------------------
//	fft2D (in fft.fx)
//---------------------------------------------------------------------------------------------
GIPImage** GIP::fft2D(GIP_IMAGE src, long flag, int bppnew)
{
	GIPImage**	dest=NULL;
	GIPImage**	addr;

	long	w, h, bpp, overl, wnew, hnew, x0, y0;

	if (flag < 0)
	{
		// normal fft convert to colour image format and crop the image
		(*src)->GetImageDimensions(&w, &h, &overl, &bpp);
		if (bpp == 8)
		{
			// need to convert to colour format
			CreateScratch(&m_pScratch1, w, h, 0,24);
			GreyToColour(src, &m_pScratch1);
		}
		else if (bpp == 16)
		{
			// need to convert to colour format
			CreateScratch(&m_pScratch1, w, h, 0,48);
			GreyToColour(src, &m_pScratch1);
		}
		else  // (in case of bpp=24 or bpp=48) 
		{
			// need to make sure that the colour image has greyvalues
			CreateScratch(&m_pScratch1, src);
			ColourToGrey(src, &m_pScratch1);
		}
		// crop the image  so that width and height are a power to 2
		wnew=4;
		hnew=4;
		do
        { 
            wnew *=2;
        } while(wnew<=w);
		wnew /=2;
		
        if (wnew > m_pFFT->MaxSize) 
            wnew = m_pFFT->MaxSize;
		
        do
        { 
            hnew *=2;
        } while(hnew<=h);
		
        hnew /=2;
		if (hnew > m_pFFT->MaxSize) 
            hnew = m_pFFT->MaxSize;
		x0=(w-wnew)/2;
		y0=(h-hnew)/2;
		// Crop image when necessary
		if ((w != wnew) || (h != hnew))
			addr = ImageCrop(&m_pScratch1, x0, y0, wnew, hnew, 0);
		else
			addr = &m_pScratch1;

		// create 2 textures for fft ping-pong technique (dest will be the result)
		CreateScratch(&m_pScratch3, wnew, hnew, 0, 128);	//R32G32B32A32 format
		dest = GIPCreateImage(wnew, hnew, 0, 128);

		if (m_pFFT->Initialise())
		{
			pDevice->CreateQuad((*dest)->pTexture->m_nTexWidth, (*dest)->pTexture->m_nTexHeight);
			m_pFFT->Create(wnew, hnew);
			m_pFFT->ApplyFFT((*addr)->pTexture, (*dest)->pTexture, m_pScratch3->pTexture, flag);
		}
		
        if (addr != &m_pScratch1) 
            GIPReleaseImage(addr);

		return (dest);
	}
	else
	{
		// create second texture for inverse fft ping-pong technique
		(*src)->GetImageDimensions(&w, &h, &overl, &bpp);
		CreateScratch(&m_pScratch3, w, h, 0, 128);			//R32G32B32A32 format
		// create destination image
		if ((bppnew==8) || (bppnew==24)) bppnew=24;
			else if ((bppnew==16) || (bppnew==48)) bppnew=48;
		dest = GIPCreateImage(w, h, 0, bppnew);
		
        if (dest && m_pFFT->Initialise())
		{
			pDevice->CreateQuad((*dest)->pTexture->m_nTexWidth, (*dest)->pTexture->m_nTexHeight);
			m_pFFT->Create(w, h);
			m_pFFT->ApplyFFT((*src)->pTexture, (*dest)->pTexture, m_pScratch3->pTexture, flag);
		}
		
        return (dest);
	}
}

//---------------------------------------------------------------------------------------------
//	fft2DButterWorth (in fft.fx)
//---------------------------------------------------------------------------------------------
GIPImage** GIP::fft2DButterWorth(GIP_IMAGE SrcHandle, long filtType, double f1, double f2, double order)
{
	BOOL		ok;
	GIPImage**	dest=NULL;
	GIPImage**	fftImaHandle;
	long		w, h, overl, bpp, bppnew;

	(*SrcHandle)->GetImageDimensions(&w, &h, &overl, &bppnew);
	fftImaHandle=fft2D(SrcHandle, -1);
	(*fftImaHandle)->GetImageDimensions(&w, &h, &overl, &bpp);
	ok=m_pFFT->CreateButterWorthFilter((FilterType) filtType, f1, f2, order);
	if (ok)
	{
		CreateScratch(&m_pScratch4, w, h, 0, bpp);
		pDevice->CreateQuad(m_pScratch4->pTexture->m_nTexWidth, m_pScratch4->pTexture->m_nTexHeight);
		m_pFFT->Apply("FilterFFT", (*fftImaHandle)->pTexture, NULL, m_pScratch4->pTexture, 0);
		dest=fft2D(&m_pScratch4, 1, bppnew);
	}
	GIPReleaseImage(fftImaHandle);
	return (dest);
}

//---------------------------------------------------------------------------------------------
//	fft2DGaussian (in fft.fx)
//---------------------------------------------------------------------------------------------
GIPImage** GIP::fft2DGaussian(GIP_IMAGE SrcHandle, long filtType, double f1, double f2)
{
	BOOL		ok;
	GIPImage**	dest=NULL;
	GIPImage**	fftImaHandle;
	long		w, h, overl, bpp, bppnew;

	(*SrcHandle)->GetImageDimensions(&w, &h, &overl, &bppnew);
	fftImaHandle=fft2D(SrcHandle, -1);	// forward fft
	(*fftImaHandle)->GetImageDimensions(&w, &h, &overl, &bpp);
	TRACE2("fft Gaus bpp source:%d bpp fft:%d\r\n",bppnew, bpp);
	ok=m_pFFT->CreateGaussianFilter((FilterType) filtType, f1, f2);
	if (ok)
	{
		CreateScratch(&m_pScratch4, w, h, 0, bpp);
		pDevice->CreateQuad(m_pScratch4->pTexture->m_nTexWidth, m_pScratch4->pTexture->m_nTexHeight);
		m_pFFT->Apply("FilterFFT", (*fftImaHandle)->pTexture, NULL, m_pScratch4->pTexture, 0);
		dest=fft2D(&m_pScratch4, 1, bppnew);
	}
	GIPReleaseImage(fftImaHandle);
	return (dest);
}

//---------------------------------------------------------------------------------------------
//	FFTImageRegistration
//---------------------------------------------------------------------------------------------
void GIP::FFTImageRegistration(GIP_IMAGE Src1, GIP_IMAGE Src2, long *xshift, long *yshift, float *sn, long MaxShift)
{
	GIPImage	**fft1ImaHandle, **fft2ImaHandle;

	fft1ImaHandle=fft2D(Src1, -1);	// forward fft Src1
	fft2ImaHandle=fft2D(Src2, -1);	// forward fft Src2
	CoreFFTImageRegistration(fft1ImaHandle, fft2ImaHandle, xshift, yshift, sn, MaxShift);
	GIPReleaseImage(fft2ImaHandle);
	GIPReleaseImage(fft1ImaHandle);
}

//---------------------------------------------------------------------------------------------
//	CoreFFTImageRegistration
//---------------------------------------------------------------------------------------------
void GIP::CoreFFTImageRegistration(GIP_IMAGE FFTSrc1, GIP_IMAGE FFTSrc2, long *xshift, long *yshift, float *sn, long MaxShift)
{
	double		maxVal=0.0, averVal=0.0, snVal=0.0;
	float		*pData, *pImageData;
	int			i, j;
	long		w, h, overl, bpp;
	GIPImage**	dest=NULL;

	*xshift=0;
	*yshift=0;
	(*FFTSrc1)->GetImageDimensions(&w, &h, &overl, &bpp);
	CreateScratch(&m_pScratch4, w, h, 0, bpp);
	pDevice->CreateQuad((m_pScratch4)->pTexture->m_nTexWidth, (m_pScratch4)->pTexture->m_nTexHeight);
	m_pFFT->Apply("MultiplyFFT", (*FFTSrc1)->pTexture, (*FFTSrc2)->pTexture, (m_pScratch4)->pTexture, 0);
	dest=fft2D(&m_pScratch4, 1, 128);	// reverse fft render to floating point target R32G32B32A32 format
	(*dest)->GetImageDimensions(&w, &h, &overl, &bpp);
	pImageData=new float[w*h*4];
	GetImage(dest, pImageData);
	pData=pImageData;
	for  (j=0; j< h; j++)
	{
		for (i=0; i< w; i++)
		{
			if (*pData > maxVal)
			{
				maxVal=*pData;
				*xshift=i;
				*yshift=j;
			}
			averVal +=*pData;
			pData +=4;
		}
	}
	averVal =averVal/((double)w*(double)h);
	pData=pImageData;
	for  (j=0; j< h; j++)
	{
		for (i=0; i< w; i++)
		{
			snVal +=((*pData-averVal)*(*pData-averVal));	
			pData +=4;
		}
	}
	snVal =sqrt(snVal/((double)w*(double)h));
	if (snVal > 0.0)
		*sn=(float)(maxVal-averVal)/(float)snVal;
	else 
	{
		*sn=0.0;
		*xshift=0;
		*yshift=0;
	}
	if (*xshift > w/2)
		*xshift -= w;
	if (*yshift > h/2)
		*yshift -=h;
	if ((abs(*xshift) > MaxShift) || (abs(*yshift) > MaxShift))
	{
		// it is better not to shift if your not certain
		*sn=(float)0.0;
		*xshift=0;
		*yshift=0;
	}
	*xshift = -(*xshift);
	*yshift = -(*yshift);
	delete [] pImageData;
	GIPReleaseImage(dest);
}

//---------------------------------------------------------------------------------------------
//	FFTImageStitch
//---------------------------------------------------------------------------------------------
void GIP::FFTImageStitch(BYTE *Addr1, BYTE *Addr2, long w, long h, long mode, long MaxShift, 
						 long *xshift, long *yshift, float *sn)
{
	GIPImage		**fft1ImaHandle, **fft2ImaHandle, **crop1, **crop2;
	long			x1, y1, w1, h1, x2, y2, w2, h2, MaxShiftPow2;

	MaxShiftPow2=(long)pow(2.,((long)(log((double)MaxShift)/log(2.))+1));
	switch(mode)
	{
		case 0:	// source2 is stitched to upper part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=(h-MaxShiftPow2);
				y2=0;
				break;
		case 1:	// source2 is stitched to right part of source1
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=w-MaxShiftPow2;
				x2=0;
				break;
		case 2:	// source2 is stitched to lower part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=0;
				y2=(h-MaxShiftPow2);
				break;
		case 3:	// source2 is stitched to left part of source1
		default:
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=0;
				x2=w-MaxShiftPow2;
				break;
	}
	crop1=GIPCreateImage(w1, h1, 0, 24);
	crop2=GIPCreateImage(w2, h2, 0, 24);
	PutImage(crop1, Addr1, w, h, x1, y1);
	PutImage(crop2, Addr2, w, h, x2, y2);
	CreateScratch(&m_pScratch3, w1, h1, 0, 128);	//R32G32B32A32 format
	fft1ImaHandle=GIPCreateImage(w1, h1, 0, 128);
	fft2ImaHandle=GIPCreateImage(w2, h2, 0, 128);
	if (m_pFFT->Initialise())
	{
		pDevice->CreateQuad((*fft1ImaHandle)->pTexture->m_nTexWidth, (*fft1ImaHandle)->pTexture->m_nTexHeight);
		m_pFFT->Create(w1, h1);
		m_pFFT->ApplyFFT((*crop1)->pTexture, (*fft1ImaHandle)->pTexture, m_pScratch3->pTexture, -1);
		m_pFFT->ApplyFFT((*crop2)->pTexture, (*fft2ImaHandle)->pTexture, m_pScratch3->pTexture, -1);
	}
	CoreFFTImageRegistration(fft1ImaHandle, fft2ImaHandle, xshift, yshift, sn, MaxShift);
	GIPReleaseImage(fft2ImaHandle);
	GIPReleaseImage(fft1ImaHandle);
	GIPReleaseImage(crop2);
	GIPReleaseImage(crop1);
}

//---------------------------------------------------------------------------------------------
//	FFTImageStitch
//---------------------------------------------------------------------------------------------
void GIP::FFTImageStitch(unsigned short *Addr1, unsigned short *Addr2, long w, long h, long mode, 
						 long MaxShift, long *xshift, long *yshift, float *sn)
{
	GIPImage		**fft1ImaHandle, **fft2ImaHandle, **crop1, **crop2;
	long			x1, y1, w1, h1, x2, y2, w2, h2, MaxShiftPow2;

	MaxShiftPow2=(long)pow(2.,((long)(log((double)MaxShift)/log(2.))+1));
	switch(mode)
	{
		case 0:	// source2 is stitched to upper part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=(h-MaxShiftPow2);
				y2=0;
				break;
		case 1:	// source2 is stitched to right part of source1
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=w-MaxShiftPow2;
				x2=0;
				break;
		case 2:	// source2 is stitched to lower part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=0;
				y2=(h-MaxShiftPow2);
				break;
		case 3:	// source2 is stitched to left part of source1
		default:
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=0;
				x2=w-MaxShiftPow2;
				break;
	}
	crop1=GIPCreateImage(w1, h1, 0, 48);
	crop2=GIPCreateImage(w2, h2, 0, 48);
	PutImage(crop1, Addr1, w, h, x1, y1);
	PutImage(crop2, Addr2, w, h, x2, y2);
	CreateScratch(&m_pScratch3, w1, h1, 0, 128);	//R32G32B32A32 format
	fft1ImaHandle=GIPCreateImage(w1, h1, 0, 128);
	fft2ImaHandle=GIPCreateImage(w2, h2, 0, 128);
	if (m_pFFT->Initialise())
	{
		pDevice->CreateQuad((*fft1ImaHandle)->pTexture->m_nTexWidth, (*fft1ImaHandle)->pTexture->m_nTexHeight);
		m_pFFT->Create(w1, h1);
		m_pFFT->ApplyFFT((*crop1)->pTexture, (*fft1ImaHandle)->pTexture, m_pScratch3->pTexture, -1);
		m_pFFT->ApplyFFT((*crop2)->pTexture, (*fft2ImaHandle)->pTexture, m_pScratch3->pTexture, -1);
	}
	CoreFFTImageRegistration(fft1ImaHandle, fft2ImaHandle, xshift, yshift, sn, MaxShift);
	GIPReleaseImage(fft2ImaHandle);
	GIPReleaseImage(fft1ImaHandle);
	GIPReleaseImage(crop2);
	GIPReleaseImage(crop1);
}

//---------------------------------------------------------------------------------------------
//	FFTImageStitch
//---------------------------------------------------------------------------------------------
void GIP::FFTImageStitch(RGBTRIPLE *Addr1, RGBTRIPLE *Addr2, long w, long h, long mode, 
						 long MaxShift, long *xshift, long *yshift, float *sn)
{
	GIPImage		**fft1ImaHandle, **fft2ImaHandle, **crop1, **crop2;
	long			x1, y1, w1, h1, x2, y2, w2, h2, MaxShiftPow2;

	MaxShiftPow2=(long)pow(2.,((long)(log((double)MaxShift)/log(2.))+1));
	switch(mode)
	{
		case 0:	// source2 is stitched to upper part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=(h-MaxShiftPow2);
				y2=0;
				break;
		case 1:	// source2 is stitched to right part of source1
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=w-MaxShiftPow2;
				x2=0;
				break;
		case 2:	// source2 is stitched to lower part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=0;
				y2=(h-MaxShiftPow2);
				break;
		case 3:	// source2 is stitched to left part of source1
		default:
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=0;
				x2=w-MaxShiftPow2;
				break;
	}
	crop1=GIPCreateImage(w1, h1, 0, 48);
	crop2=GIPCreateImage(w2, h2, 0, 48);
	PutImage(crop1, Addr1, w, h, x1, y1, IN_GREY);
	PutImage(crop2, Addr2, w, h, x2, y2, IN_GREY);
	CreateScratch(&m_pScratch3, w1, h1, 0, 128);	//R32G32B32A32 format
	fft1ImaHandle=GIPCreateImage(w1, h1, 0, 128);
	fft2ImaHandle=GIPCreateImage(w2, h2, 0, 128);
	if (m_pFFT->Initialise())
	{
		pDevice->CreateQuad((*fft1ImaHandle)->pTexture->m_nTexWidth, (*fft1ImaHandle)->pTexture->m_nTexHeight); 
		m_pFFT->Create(w1, h1);
		m_pFFT->ApplyFFT((*crop1)->pTexture, (*fft1ImaHandle)->pTexture, m_pScratch3->pTexture, -1);
		m_pFFT->ApplyFFT((*crop2)->pTexture, (*fft2ImaHandle)->pTexture, m_pScratch3->pTexture, -1);
	}
	CoreFFTImageRegistration(fft1ImaHandle, fft2ImaHandle, xshift, yshift, sn, MaxShift);
	GIPReleaseImage(fft2ImaHandle);
	GIPReleaseImage(fft1ImaHandle);
	GIPReleaseImage(crop2);
	GIPReleaseImage(crop1);
}

//---------------------------------------------------------------------------------------------
//	FFTImageStitch
//---------------------------------------------------------------------------------------------
void GIP::FFTImageStitch(RGB16BTRIPLE *Addr1, RGB16BTRIPLE *Addr2, long w, long h, long mode, 
						 long MaxShift, long *xshift, long *yshift, float *sn)
{
	GIPImage		**fft1ImaHandle, **fft2ImaHandle, **crop1, **crop2;
	long			x1, y1, w1, h1, x2, y2, w2, h2, MaxShiftPow2;

	MaxShiftPow2=(long)pow(2.,((long)(log((double)MaxShift)/log(2.))+1));
	switch(mode)
	{
		case 0:	// source2 is stitched to upper part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=(h-MaxShiftPow2);
				y2=0;
				break;
		case 1:	// source2 is stitched to right part of source1
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=w-MaxShiftPow2;
				x2=0;
				break;
		case 2:	// source2 is stitched to lower part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=0;
				y2=(h-MaxShiftPow2);
				break;
		case 3:	// source2 is stitched to left part of source1
		default:
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=0;
				x2=w-MaxShiftPow2;
				break;
	}
	crop1=GIPCreateImage(w1, h1, 0, 24);
	crop2=GIPCreateImage(w2, h2, 0, 24);
	PutImage(crop1, Addr1, w, h, x1, y1, IN_GREY);
	PutImage(crop2, Addr2, w, h, x2, y2, IN_GREY);
	CreateScratch(&m_pScratch3, w1, h1, 0, 128);	//R32G32B32A32 format
	fft1ImaHandle=GIPCreateImage(w1, h1, 0, 128);
	fft2ImaHandle=GIPCreateImage(w2, h2, 0, 128);
	if (m_pFFT->Initialise())
	{
		pDevice->CreateQuad((*fft1ImaHandle)->pTexture->m_nTexWidth, (*fft1ImaHandle)->pTexture->m_nTexHeight); 
		m_pFFT->Create(w1, h1);
		m_pFFT->ApplyFFT((*crop1)->pTexture, (*fft1ImaHandle)->pTexture, m_pScratch3->pTexture, -1);
		m_pFFT->ApplyFFT((*crop2)->pTexture, (*fft2ImaHandle)->pTexture, m_pScratch3->pTexture, -1);
	}
	CoreFFTImageRegistration(fft1ImaHandle, fft2ImaHandle, xshift, yshift, sn, MaxShift);
	GIPReleaseImage(fft2ImaHandle);
	GIPReleaseImage(fft1ImaHandle);
	GIPReleaseImage(crop2);
	GIPReleaseImage(crop1);
}

//---------------------------------------------------------------------------------------------
//	FFTImageStitch
//---------------------------------------------------------------------------------------------
void GIP::FFTImageStitchGIP(GIP_IMAGE src1, GIP_IMAGE src2, long mode, long MaxShift, long *xshift, long *yshift, float *sn)
{
	GIPImage	**fft1ImaHandle, **fft2ImaHandle, **crop1, **crop2;
	long		w, h, overl, bpp, x1, y1, w1, h1, x2, y2, w2, h2, MaxShiftPow2;

	MaxShiftPow2=(long)pow(2.,((long)(log((double)MaxShift)/log(2.))+1));
	(*src1)->GetImageDimensions(&w, &h, &overl, &bpp);
	if (bpp==8)
	{
		// need to convert to colour format
		CreateScratch(&m_pScratch1, w, h, 0,24);
		GreyToColour(src1, &m_pScratch1);
		CreateScratch(&m_pScratch2, w, h, 0,24);
		GreyToColour(src2, &m_pScratch2);
	}
	else if (bpp==16)
	{
		// need to convert to colour format
		CreateScratch(&m_pScratch1, w, h, 0,48);
		GreyToColour(src1, &m_pScratch1);
		CreateScratch(&m_pScratch2, w, h, 0,48);
		GreyToColour(src2, &m_pScratch2);
	}
	else
	{
		// need to make sure that the colour image has greyvalues
		CreateScratch(&m_pScratch1, src1);
		ColourToGrey(src1, &m_pScratch1);
		CreateScratch(&m_pScratch2, src2);
		ColourToGrey(src2, &m_pScratch2);
	}
	switch(mode)
	{
		case 0:	// source2 is stitched to upper part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=(h-MaxShiftPow2);
				y2=0;
				break;
		case 1:	// source2 is stitched to right part of source1
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=w-MaxShiftPow2;
				x2=0;
				break;
		case 2:	// source2 is stitched to lower part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=0;
				y2=(h-MaxShiftPow2);
				break;
		case 3:	// source2 is stitched to left part of source1
		default:
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=0;
				x2=w-MaxShiftPow2;
				break;
	}
	// crop the image  so that width and height are a power to 2
	crop1=ImageCrop(&m_pScratch1, x1, y1, w1, h1, 0);
	crop2=ImageCrop(&m_pScratch2, x2, y2, w2, h2, 0);

	// create 2 textures for fft ping-pong technique (dest will be the result)
	CreateScratch(&m_pScratch3, w1, h1, 0, 128);	//R32G32B32A32 format
	fft1ImaHandle=GIPCreateImage(w1, h1, 0, 128);
	fft2ImaHandle=GIPCreateImage(w2, h2, 0, 128);
	if (m_pFFT->Initialise())
	{
		pDevice->CreateQuad((*fft1ImaHandle)->pTexture->m_nTexWidth, (*fft1ImaHandle)->pTexture->m_nTexHeight);
		m_pFFT->Create(w1, h1);
		m_pFFT->ApplyFFT((*crop1)->pTexture, (*fft1ImaHandle)->pTexture, m_pScratch3->pTexture, -1);
		m_pFFT->ApplyFFT((*crop2)->pTexture, (*fft2ImaHandle)->pTexture, m_pScratch3->pTexture, -1);
	}
	CoreFFTImageRegistration(fft1ImaHandle, fft2ImaHandle, xshift, yshift, sn, MaxShift);
	GIPReleaseImage(fft2ImaHandle);
	GIPReleaseImage(fft1ImaHandle);
	GIPReleaseImage(crop2);
	GIPReleaseImage(crop1);
}
