#pragma once



#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gipcommon.h"

class GIPDevice;
class GIPEffect;
class GIPTexture;
class GIPSwapTexture;

// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif

#define ROOTLOG2						0.832554611    /* for Gaussian calc of std. dev. */
#define log2(a)							(log(a)/ 0.6931471805599)
#define exp2(a)							(pow((double)2, (double)a))


class GIPFFT
{
public:
    GIPFFT(GIPDevice *Device);
    ~GIPFFT(void);

    BOOL Create(int HSize, int VSize);              // Create the textures, scramblers and butterfly coefficients
    void Destroy(void);                             // Kill any internal workings
    BOOL Initialise();								// Initialisation 
	BOOL Apply(char *name, GIPTexture *pSource1, GIPTexture *pSource2, GIPTexture *pTarget, long overlap);		// Standard Apply for colour conv
	BOOL ApplyFFT(GIPTexture *pSource, GIPTexture *pTarget, GIPTexture *pScratch, long flag);
	BOOL CreateButterWorthFilter(FilterType filtType, double f1, double f2, double order);
	BOOL CreateGaussianFilter(FilterType filtType, double f1, double f2);

    static const int        MaxPower2 = 12;         // Do images up to 2^12 = 4096 pixels square
    static const int        MaxSize = 4096;

private:
    BOOL CreateTextures(int HSize, int VSize);      // Create working textures
    BOOL CreateFX(void);                            // Load the code
    void CreateScrambleTexture(void);               // Create scramble texture
    void CreateFourierCoeff(void);                  // Compute the butterfly coefficients

    // Direct3D internals
	IDirect3DDevice9*		m_pd3dDevice;
    GIPDevice               *m_pDevice;
    GIPEffect               *m_pGipEffect;			// effect interface
	// Butterfly			0 -   MaxPower2-1		- FFT passes Butterfly Texture Horizontal
	//			    MaxPower2 - 2*MaxPower2-1		- FFT passes Butterfly Texture Vertical
	//			  2*MaxPower2 - 3*MaxPower2-1		- Inverse FFT passes Butterfly Texture Horizontal
	//			  3*MaxPower2 - 4*MaxPower2-1		- Inverse FFT paases Butterfly Texture Vertical
	GIPTexture              *m_pButterfly[4*MaxPower2];		
    GIPSwapTexture          *m_pSwapButterfly[4*MaxPower2];
	GIPTexture              *m_pScramble[2];		// Scramble Textures Horizontal and Vertical
    GIPSwapTexture          *m_pSwapScramble[2];
 	GIPTexture              *m_pFilter;				// Scramble Textures Horizontal and Vertical
    GIPSwapTexture          *m_pSwapFilter;
	int                     m_nFFTSize[2];          // FFT size Horizontal and Vertical
    int                     m_nPower2[2];			// log2(FFTSize) Horizontal and Vertical

    BOOL                    m_bFXCreated;           // Flag indicating success of FX creation

};
