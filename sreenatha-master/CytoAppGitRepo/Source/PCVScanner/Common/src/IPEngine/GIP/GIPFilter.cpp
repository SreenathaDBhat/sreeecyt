///////////////////////////////////////////////////////////////////////////////////////////////
// Generic N x N convolution written for the graphics card
// Written By Karl Ratcliff     190805
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//   Simple convolutions will be much speedier written as simple techniques. 
//   This works better for larger convolutions as we have to deal with shader limitations.
//

#include "stdafx.h"

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>
#include <d3dx9math.h>

#include "gipcommon.h"
#include "gip.h"
#include "gipeffect.h"
#include "gipfilter.h"

///////////////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////////////////////

GIPFilter::GIPFilter(GIPDevice *Device)
{
    ASSERT(Device);
    m_pDevice = Device;
    m_pd3dDevice = Device->GetDevice();
    ASSERT(m_pd3dDevice);
    m_pGraphFilt = NULL;
    m_pFilter = NULL;
    m_pGipEffect = NULL;
    m_nOldWidth = -1;
    m_nOldHeight = -1;
    m_nOldFilterHW = -1;
}

GIPFilter::~GIPFilter(void)
{
    Destroy();
}


void GIPFilter::Destroy(void)
{
    delete m_pFilter;
    delete m_pGraphFilt;
    SAFE_DELETE(m_pGipEffect);
}


///////////////////////////////////////////////////////////////////////////////////////////////
// Initialise the filter sizes, create the underlying textures
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPFilter::Initialise(int FilterHalfWidth, int ImageWidth, int ImageHeight)
{
    BOOL    Success = TRUE;

    int Size = FilterHalfWidth * 2 + 1;

    m_nFilterHalfWidth = FilterHalfWidth;
    m_nImageWidth = ImageWidth;
    m_nImageHeight = ImageHeight;
	if (m_nFilterHalfWidth > 1)
	{
		try
		{
			if (!m_pFilter || !m_pGraphFilt || (FilterHalfWidth != m_nOldFilterHW))
			{
				SAFE_DELETE(m_pFilter);
				SAFE_DELETE(m_pGraphFilt);
				m_pFilter = new GIPSwapTexture(m_pd3dDevice);
				m_pGraphFilt = new GIPTexture(m_pd3dDevice);

				// Create textures and associated surfaces
				if (!m_pFilter->CreateFP(Size * Size, 1))
					throw;
				if (!m_pGraphFilt->CreateFP(Size * Size, 1))
					throw;

				m_nOldFilterHW = FilterHalfWidth;
			}
			Success = TRUE;
		}

		catch(char * str)
		{
			CString msg;
	        
			msg.Format("ERROR COULD NOT ALLOCATE CONVOLUTION WORKING TEXTURES %s", str);
			OutputDebugString(msg);
			Destroy();
			Success = FALSE;
			m_nFilterHalfWidth = 0;
		}
	}
    // Create the FX
    if (Success)
        Success = CreateFX();

    return Success;


}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create the FX
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPFilter::CreateFX(void)
{
 	if (m_pGipEffect==NULL)
	{
		m_pGipEffect = new GIPEffect(m_pDevice);
        return m_pGipEffect->CreateFXFromResource(IDR_CONVOLVEFX);
	}
	return (TRUE);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Fill in the filter co-efficients for an N by N filter
// NOTE setting will pCoeff to NULL will calculate just the offsets to neighbouring pixels
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPFilter::FilterCoefficients(const float *pCoeff)
{
    int i, m, n;
    BOOL Success;
    int Size = m_nFilterHalfWidth * 2 + 1;
    float x, y;
    // NOTE no need to do all this if only a 3 by 3 filter - if 3 by 3 then
    // load the coefficients into constant registers on the graphics card. 
    if (m_nFilterHalfWidth != 1)
    {
        D3DXVECTOR4 *pFilt, *pF;

        pFilt = new D3DXVECTOR4[Size * Size];
        pF = pFilt;

        for (i = 0; i < Size * Size; i++)
        {
            m = (i % Size) - m_nFilterHalfWidth;
            n = (i / Size) - m_nFilterHalfWidth;

            x = (float)m / (float)m_nImageWidth;            // Sample x, y offset positions in image
            y = (float)n / (float)m_nImageHeight;
            pF->x = x;
            pF->y = y;
            pF->z = 0.0f;
            if (pCoeff)
                pF->w = *pCoeff++;              // Actual coefficient multiply with Kernel size 
            else											// to keep coefficients large enough for large kernels
                pF->w = 0.0f;								// in the effect file this is corrected
            pF++;
            
        }

        // Fill the texture with the coefficients
        Success = m_pFilter->Put(pFilt);
        // Send to the GPU memory
        if (Success)
            Success = m_pFilter->Transmit(m_pGraphFilt);

        delete pFilt;
    }
    else
    {
		// Fill 3x3 filter coefficients
 		for (i=0; i<9; i++)
		{
			m_Filt3by3[i].x = pCoeff[i];
			m_Filt3by3[i].y = pCoeff[i];       
			m_Filt3by3[i].z = pCoeff[i];           
			m_Filt3by3[i].w = pCoeff[i];      
		}
        Success = TRUE;
    }
    
    return Success;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply a generic filter to a GIP supply source and destination texture
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPFilter::Apply(LPCTSTR TechniqueName, int nPasses, GIP_IMAGE src, GIP_IMAGE target, GIPImage	**scratch0, GIP_IMAGE scratch1)
{
	long		Width, Height, Overlap, Bpp;
    HRESULT		hr;
    UINT		cPasses;
    BOOL		ok, Success = FALSE;

	if (!m_pGipEffect) return FALSE;

    ID3DXEffect *pEffect = m_pGipEffect->Effect();

 	(*src)->GetImageDimensions(&Width, &Height, &Overlap, &Bpp);
	
    if (nPasses > 1)
	{
		if (scratch0)
            ok = TRUE;
	}
	else
		ok = TRUE;

	if ((ok) && (nPasses > 2))
	{
        if (scratch1)
            ok = TRUE;
    }
	else
		ok = TRUE;

	try
	{
		if (ok)
		{

			if (!pEffect)	
                throw "No effect";

			// init swap buffer index
			int swap = 0;

			CHECK_HR(hr = pEffect->SetFloat("TextureWidth", (float)m_nImageWidth));
			CHECK_HR(hr = pEffect->SetFloat("TextureHeight", (float)m_nImageHeight));
			// Filter size
			int S = 2 * m_nFilterHalfWidth + 1;
			CHECK_HR(hr = pEffect->SetInt("Size", S));
			// Filter size squared
			CHECK_HR(hr = pEffect->SetInt("SqSize", S * S));
		    
			if (m_nFilterHalfWidth == 1)
			{
				// Load filter coefficients in C10 to C18 constant registers on the graphics card 3 by 3 filters only
				pEffect->SetVectorArray("sampleCoeff", m_Filt3by3, 9);
			}
			else
				CHECK_HR(hr = pEffect->SetTexture("CoefficientsTexture", m_pGraphFilt->Texture()));
	        
			// set source image for first pass
			CHECK_HR(hr = pEffect->SetTexture( "SourceImageTexture", (*src)->pTexture->Texture()));

			for (int iPass = 0; iPass < nPasses; iPass++)
			{
				// set render target - should be final target buffer on last pass
				if (iPass == nPasses-1)
					m_pd3dDevice->SetRenderTarget(0, (*target)->pTexture->Surface());
				else if (swap==0)
					m_pd3dDevice->SetRenderTarget(0, (*scratch0)->pTexture->Surface());
				else
					m_pd3dDevice->SetRenderTarget(0, (*scratch1)->pTexture->Surface());

				CHECK_HR(hr = pEffect->SetTechnique(TechniqueName));
		    
				// Apply the technique contained in the effect - returns num of passes in cPasses
				CHECK_HR(hr = pEffect->Begin(&cPasses, 0));
				CHECK_HR(hr = pEffect->BeginPass(0));	// only one pass  - pass 0
	        	CHECK_HR(hr = pEffect->CommitChanges());

				// Render the simple quad with the applied technique
				m_pDevice->DrawQuad();
				if (iPass != nPasses-1)
				{
					// make the result of this pass the source for the next pass
					if (swap == 0)
						pEffect->SetTexture("SourceImageTexture", (*scratch0)->pTexture->Texture());
					else
						pEffect->SetTexture("SourceImageTexture", (*scratch1)->pTexture->Texture());

					// and set target to the other buffer
					swap ^= 1;
				}
				// end tecnnique pass
				CHECK_HR(hr = pEffect->EndPass());
				// end technique
				CHECK_HR(hr = pEffect->End());
			}			
			Success = TRUE;
		}

	}

	catch(char * str)
	{
		CString msg;
	        
		msg.Format("ERROR COULD NOT APPLY FILTER %s", str);
		OutputDebugString(msg);
		Destroy();
	}

    return Success;
}
