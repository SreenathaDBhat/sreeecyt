///////////////////////////////////////////////////////////////////////////////////////////////
// Generic N x N convolution written for the graphics card - header file
// Written By Karl Ratcliff     240805
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//
//
#pragma once

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>
#include <d3dx9math.h>

#include "gipcommon.h"
#include "GIPImage.h"

class GIPDevice;
class GIPEffect;
class GIPTexture;
class GIPSwapTexture;

class DllExport GIPFilter
{
public:
    GIPFilter(GIPDevice *Device);                           // Nuff said
    ~GIPFilter(void);

    void Destroy(void);                                     // Tidy up

    virtual BOOL Initialise(int FilterHalfWidth, int ImageWidth, int ImageHeight);  // Initialise filter textures
    virtual BOOL CreateFX(void);                            // Create the effect 

    BOOL FilterCoefficients(const float *pCoeff);           // Fill in the coefficients

    // Apply the filter
    virtual BOOL Apply(LPCTSTR TechniqueName, int nPasses, GIPImage** pSource, GIPImage**  pTarget, GIP_IMAGE scratch0, GIP_IMAGE scratch1);


    GIPDevice               *m_pDevice;             // Our GIP device
	IDirect3DDevice9		*m_pd3dDevice;          // Pointer to the D3D device

    GIPEffect               *m_pGipEffect;			// D3DX convolution effect interface

    GIPSwapTexture          *m_pFilter;             // Stores the filter coefficients
    GIPTexture              *m_pGraphFilt;          // Same texture on graphics card

    int                     m_nFilterHalfWidth;     // Size of the filter
    int                     m_nImageWidth;          // Image size H & W
    int                     m_nImageHeight;
    int                     m_nOldWidth;
    int                     m_nOldHeight;
    int                     m_nOldFilterHW;
        
private:
    D3DXVECTOR4             m_Filt3by3[9];			// Each constant register is a 4 x 32 bit float vector

};
