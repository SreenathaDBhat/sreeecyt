 //////////////////////////////////////////////////////////////////////////////////////////////////////////
// GIP Gaussian filter - source file
//
// Karl Ratcliff 190905
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"

#define _USE_MATH_DEFINES 
#include <math.h>

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>
#include <d3dx9math.h>

#include "gipcommon.h"
#include "gip.h"
#include "gipeffect.h"
#include "gipfilter.h"

#include ".\gipgaussian.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
//////////////////////////////////////////////////////////////////////////////////////////////////////////

GIPGaussian::GIPGaussian(GIPDevice *Device):GIPFilter(Device)
{
    m_nOldHW = -1;
    m_OldSigma = -1.0;
    m_pFilt = NULL;
}

GIPGaussian::~GIPGaussian(void)
{
	SAFE_DELETE(m_pGipEffect);
	SAFE_DELETE(m_pFilt);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation - not much to do here
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPGaussian::Initialise()
{

    if (CreateFX()) return TRUE;

    return FALSE;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialise the filter textures, create the effect etc.
//////////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPGaussian::Initialise(int FilterHalfWidth, float Sigma, int ImageWidth, int ImageHeight)
{
    BOOL Success = FALSE;
    BOOL Recalc = FALSE;
    int Size = FilterHalfWidth * 2 + 1;

    m_nFilterHalfWidth = FilterHalfWidth;
    m_nImageWidth = ImageWidth;
    m_nImageHeight = ImageHeight;

    try
    {
        if (!Sigma)
            throw "Naughty sigma is zero !";

        // Have we already got the required textures for the filter ?
        // Or is the filter a different size ?
        if (m_nFilterHalfWidth != m_nOldHW || !m_pFilter || !m_pGraphFilt)
        {
            SAFE_DELETE(m_pGraphFilt);
            SAFE_DELETE(m_pFilter);
            
            // Create our filter 
            m_pFilter = new GIPSwapTexture(m_pd3dDevice);

            // Create textures and associated surfaces
            if (!m_pFilter->CreateFP(Size, 1))
                throw;

            m_pGraphFilt = new GIPTexture(m_pd3dDevice);

            // NOTE We are doing a two pass separable version of a gaussian filter. Therefore we only need to create
            // a single dimensional filter Size by 1 elements. 
            if (!m_pGraphFilt->CreateFP(Size, 1))              
                throw;

            m_nOldHW = m_nFilterHalfWidth;
            
            Recalc = TRUE;
        }

        Success = TRUE;
    }

    // Houston we have a problem 
    catch(char * str)
    {
        CString msg;
        
        msg.Format("ERROR COULD NOT ALLOCATE CONVOLUTION WORKING TEXTURES %s", str);
        OutputDebugString(msg);
        Destroy();

        m_nFilterHalfWidth = 0;
    }

    // Create the FX if we haven't done so already
    if (Success && !m_pGipEffect)
        Success = CreateFX();

    // Whoopee doing well so far
    if (Success)
    {
        // Do we have a new sigma or recalc if using a different size filter
        if (m_OldSigma != Sigma || Recalc)
        {
            m_OldSigma = Sigma;

            // Fill the filter with values
            int             i, j;
            float           ep;
            float           Total;

            Total = 0.0f;

            SAFE_DELETE(m_pFilt);

            m_pFilt = new D3DXVECTOR4[Size];

            // Calculate our filter coefficients - NOTE we are doing a vertical and a horizontal pass over
            // the image so we only need a 1D texture for the coefficients
            for (i = 0; i < Size; i++)
            {
                j = i - FilterHalfWidth;
                m_pFilt[i].x = (float)j / (float)m_nImageWidth;
                m_pFilt[i].y = (float)j / (float)m_nImageHeight;
                m_pFilt[i].z = 0.0f;

                // Gaussian filter
                ep = -(j * j) / (2.0f * Sigma * Sigma);
                // NOTE 2.50662827 = sqrt(2 * pi)
                // NOTE if this was applied in both directions this would be 2.50662827 * Sigma * Sigma
                m_pFilt[i].w = (FLOAT) (exp(ep) / (2.50662827 * Sigma));    
                Total += m_pFilt[i].w;                 // NB we need to normalise the filter coeff
            }

            // Normalise them
            for (i = 0; i < Size; i++)
                m_pFilt[i].w /= Total;                 // Multiply divisor by 2 as must apply in X & Y direction

            Success = m_pFilter->Put(m_pFilt);         // Load the filter with the coefficients
        }

        // Send the filter to the graphics card
        if (Success)
            Success = m_pFilter->Transmit(m_pGraphFilt);
        else
            Success = FALSE;
    }

    return Success;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create the FX
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPGaussian::CreateFX(void)
{
 	if (m_pGipEffect==NULL)
	{
		m_pGipEffect = new GIPEffect(m_pDevice);
        return m_pGipEffect->CreateFXFromResource(IDR_GAUSSFX); 
	}
	return (TRUE);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply the FX - This is a two pass technique where we apply the filter horizontally first
// and then apply it vertically to the results of the first pass
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPGaussian::Apply(GIPTexture *pSource, GIPTexture *pTarget, GIPTexture *pSwap)
{
  	UINT cPasses;
    HRESULT hr;
    
    if (!pSource || !pTarget || !pSwap)
        return FALSE;

    ID3DXEffect *pEffect = m_pGipEffect->Effect();

	hr = pEffect->SetTechnique("gaussian");

	// Apply the technique contained in the effect - returns num of passes in cPasses
	hr = pEffect->Begin(&cPasses, 0);
	// set render target texture
    hr = m_pd3dDevice->SetRenderTarget( 0, pSwap->Surface());
    
  	// set source image 
	hr = pEffect->SetTexture("SourceImageTexture", pSource->Texture());
    // Filter size
    hr = pEffect->SetInt("Size", 2 * m_nFilterHalfWidth + 1);
    // Filter coefficients
    hr = pEffect->SetTexture("CoefficientsTexture", m_pGraphFilt->Texture());
	        
    hr = pEffect->BeginPass(0);	            // Do pass i

	// The effect interface queues up the changes and performs them 
	// with the CommitChanges call. You do not need to call CommitChanges if 
	// you are not setting any parameters between the BeginPass and EndPass.
	hr = pEffect->CommitChanges();

	// Render the simple quad with the applied technique
	m_pDevice->DrawQuad();

	// end tecnnique pass
	hr = pEffect->EndPass();

	// set render target texture
    hr = m_pd3dDevice->SetRenderTarget( 0, pTarget->Surface());
  	// set source image 
	hr = pEffect->SetTexture("SourceImageTexture", pSwap->Texture());

    hr = pEffect->BeginPass(1);	            // Do pass 1

	// The effect interface queues up the changes and performs them 
	// with the CommitChanges call. You do not need to call CommitChanges if 
	// you are not setting any parameters between the BeginPass and EndPass.
	hr = pEffect->CommitChanges();

	// Render the simple quad with the applied technique
	m_pDevice->DrawQuad();

	// end tecnnique pass
	hr = pEffect->EndPass();


	// end technique
	hr = pEffect->End();

    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// ApplyUnsharpMask  
// First perform Gaussian and then calculate the unsharp mask
// unsharpmask(source) = source - amount*(gaussian(source))/(1-amount)
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPGaussian::ApplyUnsharpMask(GIPTexture  *pSource, GIPTexture *pTarget, GIPTexture *pSwap1, 
								   GIPTexture *pSwap2, float Amount)
{
	BOOL ok;
	ok=Apply(pSource, pSwap2, pSwap1);			// result gaussian in pSwap2
	if (ok)
	{
		HRESULT hr;
	 	UINT cPasses;
		ID3DXEffect *pEffect = m_pGipEffect->Effect();

		hr = pEffect->SetTechnique("UnsharpMask");

		// Apply the technique contained in the effect - returns num of passes in cPasses
		hr = pEffect->Begin(&cPasses, 0);
		// set render target texture
		hr = m_pd3dDevice->SetRenderTarget( 0, pTarget->Surface());
    
		// set source image 
		hr = pEffect->SetTexture("SourceImageTexture", pSource->Texture());
 		hr = pEffect->SetTexture("SourceImage2Texture", pSwap2->Texture());
		hr = pEffect->SetFloat("Amount", Amount);
		hr = pEffect->BeginPass(0);	            // Do pass i
		hr = pEffect->CommitChanges();
		m_pDevice->DrawQuad();
		hr = pEffect->EndPass();
		hr = pEffect->End();
	}
    return ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// ApplyNearestNeighbourDeconv  
// First perform Gaussian and then calculate the unsharp mask based on 2 neigbouring images
// nndeconv(sourcei) = sourcei - amount*((gaussian(sourcei-1)+gaussian(sourcei+1))/2
///////////////////////////////////////////////////////////////////////////////////////////////
BOOL GIPGaussian::ApplyNearestNeighbourDeconv(GIPTexture  *pSource, GIPTexture *pSource2, GIPTexture *pSource3, 
											  GIPTexture *pTarget, float Amount)
{
	HRESULT hr;
	UINT cPasses;
	ID3DXEffect *pEffect = m_pGipEffect->Effect();

	hr = pEffect->SetTechnique("NearestNeighbourDeconv");

	// Apply the technique contained in the effect - returns num of passes in cPasses
	hr = pEffect->Begin(&cPasses, 0);
	// set render target texture
	hr = m_pd3dDevice->SetRenderTarget( 0, pTarget->Surface());
    
	// set source image 
	hr = pEffect->SetTexture("SourceImageTexture", pSource->Texture());
 	hr = pEffect->SetTexture("SourceImage2Texture", pSource2->Texture());
 	hr = pEffect->SetTexture("SourceImage3Texture", pSource3->Texture());
	hr = pEffect->SetFloat("Amount", Amount);
	hr = pEffect->BeginPass(0);	            // Do pass i
	hr = pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	hr = pEffect->EndPass();
	hr = pEffect->End();
	return TRUE;
}

