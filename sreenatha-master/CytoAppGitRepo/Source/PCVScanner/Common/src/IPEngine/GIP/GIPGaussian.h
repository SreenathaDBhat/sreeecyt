//////////////////////////////////////////////////////////////////////////////////////////////////////////
// GIP Gaussian filter - header file
//
// Karl Ratcliff 190905
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#pragma once

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gipcommon.h"


class GIPDevice;
class GIPEffect;
class GIPTexture;
class GIPSwapTexture;
class GIPFilter;

class GIPGaussian : GIPFilter
{
public:
    GIPGaussian(GIPDevice *Device);
    ~GIPGaussian(void);

    BOOL Initialise();
    BOOL Initialise(int FilterHalfWidth, float Sigma, int ImageWidth, int ImageHeight);
    BOOL Apply(GIPTexture  *pSource, GIPTexture *pTarget, GIPTexture *pSwap);
    BOOL ApplyUnsharpMask(GIPTexture  *pSource, GIPTexture *pTarget, GIPTexture *pSwap1, 
						  GIPTexture *pSwap2, float Amount);
	BOOL ApplyNearestNeighbourDeconv(GIPTexture  *pSource, GIPTexture *pSource2, GIPTexture *pSource3, 
									 GIPTexture *pTarget, float Amount);

private:
    BOOL CreateFX(void);
    int             m_nOldHW;
    float           m_OldSigma;
    D3DXVECTOR4     *m_pFilt;
};
