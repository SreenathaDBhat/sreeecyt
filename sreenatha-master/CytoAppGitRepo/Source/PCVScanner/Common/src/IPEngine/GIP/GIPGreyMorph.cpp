///////////////////////////////////////////////////////////////////////////////////////
// GIP 8 bit morphology routines - implementation
// Taken from Marks original GIP project
///////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include "gip.h"
#include "gipeffect.h"
#include ".\gipgreymorph.h"

///////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////////////

GIPGreyMorph::GIPGreyMorph(GIPDevice *Device)
{
    ASSERT(Device);

    m_pGipEffect = new GIPEffect(Device);
    m_pDevice = Device;
    m_pd3dDevice = Device->GetDevice();
}

GIPGreyMorph::~GIPGreyMorph(void)
{
    if (m_pGipEffect)
        delete m_pGipEffect;
}

///////////////////////////////////////////////////////////////////////////////////////
// Initialisation
///////////////////////////////////////////////////////////////////////////////////////

BOOL GIPGreyMorph::Initialise(int ImageWidth, int ImageHeight, int MaxHalfWidth)
{
    m_nImageWidth  = ImageWidth;
    m_nImageHeight = ImageHeight;
    
    m_nOverlap = MaxHalfWidth;
	m_nQuadrantWidth = ImageWidth / 2;
	m_nQuadrantHeight = ImageHeight / 2;

    if (!m_pGipEffect->CreateFXFromFile("GreyMorph.fx"))
        return FALSE;


    return TRUE;
}


