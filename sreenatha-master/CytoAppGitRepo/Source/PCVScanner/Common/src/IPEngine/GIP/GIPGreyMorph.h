#pragma once

///////////////////////////////////////////////////////////////////////////////////////
// GIP 8 bit morphology routines
// Taken from Marks original GIP project
///////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

class GIPEffect;
class GIPDevice;


class DllExport GIPGreyMorph
{
public:
    GIPGreyMorph(GIPDevice *Device);
    ~GIPGreyMorph(void);

    BOOL Initialise(int ImageWidth, int ImageHeight, int MaxHalfWidth);

private:
    GIPDevice               *m_pDevice;             // Our GIP device
	IDirect3DDevice9Ex      *m_pd3dDevice;          // Pointer to the D3D device
    GIPEffect               *m_pGipEffect;			// D3DX convolution effect interface
    int                     m_nImageWidth;          // Image size H & W
    int                     m_nImageHeight;
    int                     m_nOverlap;             // Overlap between quads
    int                     m_nQuadrantWidth;       // Quad size
    int                     m_nQuadrantHeight;
};
