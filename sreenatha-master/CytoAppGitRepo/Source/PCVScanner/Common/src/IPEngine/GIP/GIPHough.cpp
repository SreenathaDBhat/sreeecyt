///////////////////////////////////////////////////////////////////////////////////////////////
// Eucledian Distance Map implemented on the graphics card
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//
//
#include "stdafx.h"

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gipcommon.h"
#include "gip.h"
#include "gipeffect.h"
#include "giptexture.h"
#include "gipHough.h"


///////////////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////////////////////

GIPHough::GIPHough(GIPDevice *Device)
{
    ASSERT(Device);
    m_pDevice = Device;
    m_pd3dDevice = m_pDevice->GetDevice();
    ASSERT(m_pd3dDevice);
    m_pGipEffect = NULL;
    m_bFXCreated = FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Destructor - free up everything 
///////////////////////////////////////////////////////////////////////////////////////////////

GIPHough::~GIPHough(void)
{
	SAFE_DELETE(m_pGipEffect);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation - not much to do here
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPHough::Initialise()
{
    if (CreateFX()) return TRUE;
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create the FX
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPHough::CreateFX(void)
{
	if (m_pGipEffect==NULL)
	{
		m_pGipEffect = new GIPEffect(m_pDevice);

        if (!m_pGipEffect->CreateFXFromResource(IDR_HOUGHFX))
			return FALSE;
		m_bFXCreated = TRUE;
	}
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply Euclean Distance Map using Jump Flooding
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPHough::ApplyCannyEdge(GIPTexture *pSource, GIPTexture *pScratch, GIPTexture *pResult, double thresholdLow, double thresholdHigh)
{
    ID3DXEffect	*pEffect = m_pGipEffect->Effect();
	UINT		cPasses;

	pEffect->SetTechnique("CannyEdge");

	// pass 0
	pEffect->SetTexture( "SourceImageTexture", pSource->Texture());
	pEffect->SetFloat("OffsetX", (float)1./(float)pSource->m_nImageWidth);
	pEffect->SetFloat("OffsetY", (float)1./(float)pSource->m_nImageHeight);
	m_pd3dDevice->SetRenderTarget( 0, pScratch->Surface());
	pEffect->Begin(&cPasses, 0);
	pEffect->BeginPass(0);
	pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	pEffect->EndPass();

	// pass 1
	pEffect->SetFloat("CannyThresholdLow", (float)thresholdLow);
	pEffect->SetFloat("CannyThresholdHigh", (float)thresholdHigh);
	pEffect->SetTexture( "SourceImageTexture", pScratch->Texture());
	m_pd3dDevice->SetRenderTarget( 0, pResult->Surface());
	pEffect->BeginPass(1);
	pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	pEffect->EndPass();

	pEffect->End();
    return FALSE;
}

BOOL GIPHough::ApplyHoughLines(GIPTexture *pSource, GIPTexture *pTarget, long minlength, long connection)
{
    UINT cPasses;

    ID3DXEffect *pEffect = m_pGipEffect->Effect();
    
	// set source images + params
	pEffect->SetTexture( "SourceImageTexture", pSource->Texture());
	m_pd3dDevice->SetRenderTarget( 0, pTarget->Surface());

	// Apply the named technique contained in the effect
	pEffect->SetTechnique("HoughLines");
	pEffect->Begin(&cPasses, 0);
	pEffect->BeginPass(0);	// only one pass
	pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	pEffect->EndPass();
	pEffect->End();
    return FALSE;
}

//---------------------------------------------------------------------------------------------
//	CannyEdge
//---------------------------------------------------------------------------------------------
void GIP::CannyEdge(GIP_IMAGE src, GIP_IMAGE dest, double thresholdLow, double thresholdHigh)
{
	long		w, h, overl, bpp, w1, h1, overl1, bpp1;

	(*src)->GetImageDimensions(&w, &h, &overl, &bpp);
	(*dest)->GetImageDimensions(&w1, &h1, &overl1, &bpp1);
	if (bpp==8)
	{
		// need to convert to colour format
		if ((w !=w1) || (h != h1) || (overl != 0) || (overl1 != 0) || (bpp1 != 24)) return;
		GreyToColour(src, dest);
		CreateScratch(&m_pScratch1, w, h, 0, 24);
	}
	else if (bpp==16)
	{
		// need to convert to colour format 48 bit
		if ((w !=w1) || (h != h1) || (overl != 0) || (overl1 != 0) || (bpp1 != 48)) return;
		GreyToColour(src, dest);
		CreateScratch(&m_pScratch1, w, h, 0, 48);
	}
	else
	{
		// need to make sure that the colour image has greyvalues
		if ((w !=w1) || (h != h1) || (overl != 0) || (overl1 != 0) || (bpp1 != 24)) return;
		ColourToGrey(src, dest);
		CreateScratch(&m_pScratch1, w, h, 0, 24);
	}
	if (m_pHough->Initialise())
	{
		// setup display quad
		pDevice->CreateQuad((*dest)->pTexture->m_nTexWidth, (*dest)->pTexture->m_nTexHeight);
		m_pHough->ApplyCannyEdge((*dest)->pTexture, m_pScratch1->pTexture, (*dest)->pTexture, thresholdLow, thresholdHigh);
	}	
}

//---------------------------------------------------------------------------------------------
//	HoughLines
//---------------------------------------------------------------------------------------------
void GIP::HoughLines(GIP_IMAGE src, GIP_IMAGE dest, char *angles, long minlength, long connection)
{
	long		w, h, overl, bpp;

	if (m_pHough->Initialise())
	{
		(*src)->GetImageDimensions(&w, &h, &overl, &bpp);
		CreateScratch(&m_pScratch1, w, h, 0, 128);
	}	
}
