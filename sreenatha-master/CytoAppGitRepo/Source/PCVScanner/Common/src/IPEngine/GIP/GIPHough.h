#pragma once



#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gipcommon.h"

class GIPDevice;
class GIPEffect;
class GIPTexture;
class GIPSwapTexture;

// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif


class GIPHough
{
public:
    GIPHough(GIPDevice *Device);
    ~GIPHough(void);

    BOOL Create(int HSize, int VSize);              // Create the textures, scramblers and butterfly coefficients
    void Destroy(void);                             // Kill any internal workings
    BOOL Initialise();								// Initialisation 
    BOOL ApplyCannyEdge(GIPTexture *pSource, GIPTexture *pScratch, GIPTexture *pResult, double thresholdLow, double thresholdHigh);
    BOOL ApplyHoughLines(GIPTexture *pSource, GIPTexture *pTarget, long minlength, long connection);

private:
    BOOL CreateFX(void);                            // Load the code

    // Direct3D internals
    IDirect3DDevice9*	    m_pd3dDevice;
    GIPDevice               *m_pDevice;
    GIPEffect               *m_pGipEffect;			// effect interface
    GIPSwapTexture          *m_pSwapFilter;
    BOOL                    m_bFXCreated;           // Flag indicating success of FX creation

};
