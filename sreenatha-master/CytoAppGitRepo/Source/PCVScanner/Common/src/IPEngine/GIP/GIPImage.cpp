///////////////////////////////////////////////////////////////////////////////////////////////
// GIP D3D device class - source file extracted from Marks original GIP class
// KR270805 
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include "gipcommon.h"
#include "GIP.h"
#include "GIPImage.h"

GIPImage::GIPImage(IDirect3DDevice9* Device)
{
    ASSERT(Device);

    m_nImageWidth = 0;
    m_nImageHeight = 0;
 	m_nOverlap = 0;
	m_nBpp = 0;
	m_pd3dDevice = Device;
	this->m_Handle=this;
}

GIPImage::~GIPImage(void)
{	
	if (pTexture != NULL) delete pTexture;
}

GIP_IMAGE GIPImage::GetHandle()
{
	return((GIP_IMAGE )&m_Handle);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Init Direct3D device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPImage::Create(long Width, long Height, long Overlap, long Bpp)
{
	this->m_nImageWidth=Width;
	this->m_nImageHeight=Height;
	if (Bpp > 16)
	{
		Overlap=0;
	}
	else
	{
		if (Overlap<0) Overlap=0;
		else if (Overlap > Width/4)Overlap=Width/4;
	}

	this->m_nOverlap=Overlap;
	this->m_nBpp=Bpp;
	pTexture = new GIPTexture(this->m_pd3dDevice);
	if (this->m_nBpp==24)
	{
		if (!pTexture->Create(Width, Height)) 
		{
            delete pTexture;
			pTexture=NULL;
			OutputDebugString(_T("GIPIMAGE ERROR CREATING TEXTURE 24 bit"));
			return FALSE;
		}
	}
	else if (this->m_nBpp==32)
	{
		if (!pTexture->CreateFP(Width, Height)) 
		{
            delete pTexture;
			pTexture=NULL;
			OutputDebugString(_T("GIPIMAGE ERROR CREATING TEXTURE 32 bit"));
			return FALSE;
		}
	}
	else if (this->m_nBpp==48)
	{
		if (!pTexture->Create16B(Width, Height)) 
		{
            delete pTexture;
			pTexture=NULL;
			OutputDebugString(_T("GIPIMAGE ERROR CREATING TEXTURE 48 bit"));
			return FALSE;
		}
	}
	else if (this->m_nBpp==128)
	{
		if (!pTexture->CreateFP32(Width, Height)) 
		{
            delete pTexture;
			pTexture=NULL;
			OutputDebugString(_T("GIPIMAGE ERROR CREATING TEXTURE 128 bit"));
			return FALSE;
		}
	}
	else if (this->m_nBpp==16)
	{
		if (!pTexture->Create16B(Width, Height, Overlap)) 
		{
            delete pTexture;
			pTexture=NULL;
			OutputDebugString(_T("GIPIMAGE ERROR CREATING TEXTURE 16 bit"));
			return FALSE;
		}
	}
	else
	{
		if (Overlap == 0)
		{
			if (!pTexture->Create(Width, Height)) 
			{
				delete pTexture;
				pTexture=NULL;
				OutputDebugString(_T("GIPIMAGE ERROR CREATING TEXTURE 8 bit"));
				return FALSE;
			}
		}
		else
		{
			if (!pTexture->Create(Width, Height, Overlap)) 
			{
				delete pTexture;
				pTexture=NULL;
				OutputDebugString(_T("GIPIMAGE ERROR CREATING TEXTURE 8 bit"));
				return FALSE;
			}
		}
	}
	return TRUE;
}

void GIPImage::GetImageDimensions(long *Width, long *Height, long *Overlap, long *BPP)
{
	*Width=this->m_nImageWidth;
	*Height=this->m_nImageHeight;
	*Overlap=this->m_nOverlap;
	*BPP=this->m_nBpp;
}

///////////////////////////////////////////////////////////////////////////////////
// Copy data from (Grey)Image in GIP to GreyImage in memory
///////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::GetImage(GIPSwapTexture* pSwapTexture, BYTE *pImageData)
{
	BOOL ok;

	ok=pSwapTexture->Receive(pTexture);
	if (ok)
		ok=pSwapTexture->Get(pImageData);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////
// Copy data from (Colour)Image in GIP to ColourImage in memory (3bytes)
///////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::GetImage(GIPSwapTexture* pSwapTexture, RGBTRIPLE *pImageData)
{
	BOOL ok;
	ok=pSwapTexture->Receive(pTexture);
	if (ok)
		ok=pSwapTexture->Get(pImageData);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////
// Copy data from (Grey)Image in GIP to GreyImage in memory
///////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::GetImage(GIPSwapTexture* pSwapTexture, unsigned short *pImageData)
{
	BOOL ok;

	ok=pSwapTexture->Receive(pTexture);
	if (ok)
		ok=pSwapTexture->Get(pImageData);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////
// Copy data from (Colour)Image in GIP to ColourImage in memory (3bytes)
///////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::GetImage(GIPSwapTexture* pSwapTexture, RGB16BTRIPLE *pImageData)
{
	BOOL ok;
	ok=pSwapTexture->Receive(pTexture);
	if (ok)
		ok=pSwapTexture->Get(pImageData);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////
// Copy data from (Colour)Image in GIP to ColourImage in memory (4bytes)
///////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::GetImage(GIPSwapTexture* pSwapTexture, RGBQUAD *pImageData)
{
	BOOL ok;
	ok=pSwapTexture->Receive(pTexture);
	if (ok)
		ok=pSwapTexture->Get(pImageData);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////
// Copy data from Float16Image or Float32Image in GIP to Float Image in memory
///////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::GetImage(GIPSwapTexture* pSwapTexture, float *pImageData)
{
	BOOL ok;

	ok=pSwapTexture->Receive(pTexture);
	if (ok)
	{
		if (this->m_nBpp==128)
			ok=pSwapTexture->GetFP32(pImageData);
		else
			ok=pSwapTexture->Get(pImageData);
	}
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////
// Copy Component data from RGBImage in GIP to GreyImage in memory
///////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::GetImageComp(GIPSwapTexture* pSwapTexture, long component, BYTE *pImageData)
{
	BOOL ok;

	ok=pSwapTexture->Receive(pTexture);
	if (ok)
		ok=pSwapTexture->GetComponent(component, pImageData);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////
// Copy Component data from RGBImage in GIP to GreyImage in memory
///////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::GetImageComp(GIPSwapTexture* pSwapTexture, long component, unsigned short *pImageData)
{
	BOOL ok;

	ok=pSwapTexture->Receive(pTexture);
	if (ok)
		ok=pSwapTexture->GetComponent(component, pImageData);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////
// Put data in GIPImage from (Grey)Image in memory
///////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::PutImage(GIPSwapTexture* pSwapTexture, BYTE *pImageData)
{
	BOOL ok;
	ok=pSwapTexture->Put(pImageData);
	if (ok)
		ok=pSwapTexture->Transmit(pTexture);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////
// Put data in GIPImage from (Colour)Image in memory (3 bytes)
///////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::PutImage(GIPSwapTexture* pSwapTexture, RGBTRIPLE *pImageData)
{
	BOOL ok;
	ok=pSwapTexture->Put(pImageData);
	if (ok)
		ok=pSwapTexture->Transmit(pTexture);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////
// Put data in GIPImage from (Grey)Image in memory
///////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::PutImage(GIPSwapTexture* pSwapTexture, unsigned short *pImageData)
{
	BOOL ok;
	ok=pSwapTexture->Put(pImageData);
	if (ok)
		ok=pSwapTexture->Transmit(pTexture);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////
// Put data in GIPImage from (Colour)Image in memory (3 bytes)
///////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::PutImage(GIPSwapTexture* pSwapTexture, RGB16BTRIPLE *pImageData)
{
	BOOL ok;
	ok=pSwapTexture->Put(pImageData);
	if (ok)
		ok=pSwapTexture->Transmit(pTexture);
	return ok;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	PutImage - Put Part of (Grey)Image with width=w and height=h from startpoint (x0, y0) in existing GIPImage
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::PutImage(GIPSwapTexture* pSwapTexture, BYTE *pImageData, long w, long h, long x0, long y0)
{
	BOOL ok;
	ok=pSwapTexture->Put(pImageData, w, h, x0, y0);
	if (ok)
		ok=pSwapTexture->Transmit(pTexture);
	return ok;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	PutImage - Put Part of (Colour)Image with width=w and height=h from startpoint (x0, y0) in existing GIPImage
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::PutImage(GIPSwapTexture* pSwapTexture, RGBTRIPLE *pImageData, long w, long h, long x0, long y0, int ColourMode)
{
	BOOL ok;
	ok=pSwapTexture->Put(pImageData, w, h, x0, y0, ColourMode);
	if (ok)
		ok=pSwapTexture->Transmit(pTexture);
	return ok;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	PutImage - Put Part of (Grey)Image with width=w and height=h from startpoint (x0, y0) in existing GIPImage
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::PutImage(GIPSwapTexture* pSwapTexture, unsigned short *pImageData, long w, long h, long x0, long y0)
{
	BOOL ok;
	ok=pSwapTexture->Put(pImageData, w, h, x0, y0);
	if (ok)
		ok=pSwapTexture->Transmit(pTexture);
	return ok;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	PutImage - Put Part of (Colour)Image with width=w and height=h from startpoint (x0, y0) in existing GIPImage
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL GIPImage::PutImage(GIPSwapTexture* pSwapTexture, RGB16BTRIPLE *pImageData, long w, long h, long x0, long y0, int ColourMode)
{
	BOOL ok;
	ok=pSwapTexture->Put(pImageData, w, h, x0, y0, ColourMode);
	if (ok)
		ok=pSwapTexture->Transmit(pTexture);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////////
// Check if all images have same Dimensions
BOOL GIPImage::SameDimensions (GIPImage *src1, GIPImage *src2, GIPImage *src3)
{
	if (src1!=NULL)
	{
		if ((this->m_nImageWidth != src1->m_nImageWidth) || (this->m_nImageHeight != src1->m_nImageHeight) ||
			(this->m_nBpp!= src1->m_nBpp) || (this->m_nOverlap != src1->m_nOverlap))
			return FALSE;
	}
	if (src2 !=NULL)
	{
		if ((this->m_nImageWidth != src2->m_nImageWidth) || (this->m_nImageHeight != src2->m_nImageHeight) ||
			(this->m_nBpp!= src2->m_nBpp) || (this->m_nOverlap != src2->m_nOverlap))
			return FALSE;
	}
	if (src3 !=NULL)
	{
		if ((this->m_nImageWidth != src3->m_nImageWidth) || (this->m_nImageHeight != src3->m_nImageHeight) ||
			(this->m_nBpp!= src3->m_nBpp) || (this->m_nOverlap != src3->m_nOverlap))
			return FALSE;
	}
	return TRUE;
}
