#pragma once
///////////////////////////////////////////////////////////////////////////////////////////////
// GIP Image class - header file
//
///////////////////////////////////////////////////////////////////////////////////////////////

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>
#include "gipcommon.h"
#include "giptexture.h"



// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif

///////////////////////////////////////////////////////////////////////////////////////////////
// GIP graphics memory textures
///////////////////////////////////////////////////////////////////////////////////////////////

class DllExport GIPImage
{
public:

    GIPImage(IDirect3DDevice9* Device);
    ~GIPImage();

    virtual BOOL		Create(long Width, long Height, long Overlap, long Bpp);        // Create a GIP Image
	void				GetImageDimensions(long *Width, long *Height, long *Overlap, long *BPP);
	BOOL				GetImage(GIPSwapTexture* pSwapTexture, BYTE *pImageData);
    BOOL				GetImage(GIPSwapTexture* swapTexture, RGBTRIPLE *pImageData);
	BOOL				GetImage(GIPSwapTexture* pSwapTexture, unsigned short *pImageData);
    BOOL				GetImage(GIPSwapTexture* swapTexture, RGB16BTRIPLE *pImageData);
    BOOL				GetImage(GIPSwapTexture* swapTexture, RGBQUAD *pImageData);
    BOOL				GetImage(GIPSwapTexture* swapTexture, float *pImageData);
    BOOL				GetImageComp(GIPSwapTexture* swapTexture, long component, BYTE *pImageData);
    BOOL				GetImageComp(GIPSwapTexture* swapTexture, long component, unsigned short *pImageData);
	BOOL				SameDimensions (GIPImage *src1, GIPImage *src2=NULL, GIPImage *src3=NULL);
	BOOL				PutImage(GIPSwapTexture* swapTexture, BYTE *pImageData);
	BOOL				PutImage(GIPSwapTexture* swapTexture, RGBTRIPLE *pImageData);
	BOOL				PutImage(GIPSwapTexture* swapTexture, unsigned short *pImageData);
	BOOL				PutImage(GIPSwapTexture* swapTexture, RGB16BTRIPLE *pImageData);
	BOOL				PutImage(GIPSwapTexture* pSwapTexture, BYTE *pImageData, long w, long h, long x0, long y0);
	BOOL				PutImage(GIPSwapTexture* pSwapTexture, RGBTRIPLE *pImageData, long w, long h, long x0, long y0, int ColourMode);
	BOOL				PutImage(GIPSwapTexture* pSwapTexture, unsigned short *pImageData, long w, long h, long x0, long y0);
	BOOL				PutImage(GIPSwapTexture* pSwapTexture, RGB16BTRIPLE *pImageData, long w, long h, long x0, long y0, int ColourMode);
	GIPImage** 			GetHandle();
   
	GIPImage*			m_Handle;
    int					m_nImageWidth;                  // Actual image dimensions
    int					m_nImageHeight;
    int					m_nBpp;
    int					m_nOverlap;
    IDirect3DDevice9	*m_pd3dDevice;                  // Pointer to our D3D device   
	GIPTexture			*pTexture;						// buffers for user images
};

typedef GIPImage** GIP_IMAGE;