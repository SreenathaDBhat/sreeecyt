///////////////////////////////////////////////////////////////////////////////////////////////
// A Kuwahara filter written for the graphics card
// Written By Karl Ratcliff     190805
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include "stdafx.h"

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>
#include <d3dx9math.h>


#include "gipcommon.h"
#include "gip.h"
#include "gipeffect.h"
#include "gipfilter.h"

#include ".\gipkuwahara.h"

GIPKuwahara::GIPKuwahara(GIPDevice *Device):GIPFilter(Device)
{
}

GIPKuwahara::~GIPKuwahara(void)
{
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation - not much to do here
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPKuwahara::Initialise(int FilterHalfWidth, int ImageWidth, int ImageHeight)
{
    int Size = FilterHalfWidth * 2 + 1;

    m_nFilterHalfWidth = FilterHalfWidth;
    m_nImageWidth = ImageWidth;
    m_nImageHeight = ImageHeight;

    if (CreateFX())
    {
        ID3DXEffect *pEffect = m_pGipEffect->Effect();
        // Set our variables
        pEffect->SetFloat("TexStep1x", 1.0f / m_nImageWidth);
        pEffect->SetFloat("TexStep1y", 1.0f / m_nImageHeight);
        pEffect->SetFloat("TexStep2x", 2.0f / m_nImageWidth);
        pEffect->SetFloat("TexStep2y", 2.0f / m_nImageHeight);
        pEffect->SetFloat("TexStep5x", 5.0f / m_nImageWidth);
        pEffect->SetFloat("TexStep5y", 5.0f / m_nImageHeight);

        return TRUE;
    }

    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create the FX
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPKuwahara::CreateFX(void)
{
	if (m_pGipEffect==NULL)
	{
		m_pGipEffect = new GIPEffect(m_pDevice);

        return m_pGipEffect->CreateFXFromResource(IDR_KUWAHARAFX); 
	}
	return(TRUE);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply it
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPKuwahara::Apply(GIPTexture *pSource, GIPTexture *pTarget, GIPTexture *pSwapText)   
{
    UINT cPasses;
    HRESULT hr;

    ID3DXEffect *pEffect = m_pGipEffect->Effect();
    
    if (m_nFilterHalfWidth == 1)
        hr = pEffect->SetTechnique("Kuwahara3by3");
    else
        hr = pEffect->SetTechnique("Kuwahara5by5");

	// Apply the technique contained in the effect - returns num of passes in cPasses
	hr = pEffect->Begin(&cPasses, 0);
	// Set render target texture
    hr = m_pd3dDevice->SetRenderTarget(0, pSwapText->Surface());
   
  	// Set source image 
	hr = pEffect->SetTexture("SourceImageTexture", pSource->Texture());

    hr = pEffect->BeginPass(0);	            // Do pass 0

	// The effect interface queues up the changes and performs them 
	// with the CommitChanges call. You do not need to call CommitChanges if 
	// you are not setting any parameters between the BeginPass and EndPass.
	hr = pEffect->CommitChanges();

	// Render the simple quad with the applied technique
	m_pDevice->DrawQuad();

	// end tecnnique pass
	hr = pEffect->EndPass();

	// set render target texture
    hr = m_pd3dDevice->SetRenderTarget(0, pTarget->Surface());
  	// set source image 
	hr = pEffect->SetTexture("SourceImageTexture", pSwapText->Texture());

    hr = pEffect->BeginPass(1);	            // Do pass 1

	// The effect interface queues up the changes and performs them 
	// with the CommitChanges call. You do not need to call CommitChanges if 
	// you are not setting any parameters between the BeginPass and EndPass.
	hr = pEffect->CommitChanges();

	// Render the simple quad with the applied technique
	m_pDevice->DrawQuad();

	// end tecnnique pass
	hr = pEffect->EndPass();

	// end technique
	hr = pEffect->End();

    return FALSE;
}
