#pragma once
///////////////////////////////////////////////////////////////////////////////////////////////
// A Kuwahara filter written for the graphics card
// Written By Karl Ratcliff     190805
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gipcommon.h"
#include "gipfilter.h"

class DllExport GIPKuwahara : public GIPFilter
{
public:
    GIPKuwahara(GIPDevice *Device);
    ~GIPKuwahara(void);

    BOOL Initialise(int FilterHalfWidth, int ImageWidth, int ImageHeight);          // Initialisation 
    BOOL Apply(GIPTexture *pSource, GIPTexture *pTarget, GIPTexture *pSwapText);    // Appy it

private:
    BOOL CreateFX(void);                                                            // Load and compile the FX file
};
