///////////////////////////////////////////////////////////////////////////////////////////////
// A Mathematical functions for the graphics card
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include "stdafx.h"

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>
#include <d3dx9math.h>


#include "gipcommon.h"
#include "gip.h"
#include "gipeffect.h"
#include "GIPMath.h"

GIPMath::GIPMath(GIPDevice *Device)
{
	ASSERT(Device);
    m_pDevice = Device;
    m_pd3dDevice = Device->GetDevice();
    ASSERT(m_pd3dDevice);
    m_pGipEffect = NULL;
}

GIPMath::~GIPMath(void)
{
	SAFE_DELETE(m_pGipEffect);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation - not much to do here
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPMath::Initialise()
{

    if (CreateFX()) return TRUE;

    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create the FX
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPMath::CreateFX(void)
{
	if (m_pGipEffect==NULL)
	{
		m_pGipEffect = new GIPEffect(m_pDevice);

        return m_pGipEffect->CreateFXFromResource(IDR_MATHFX); 
	}
	return(TRUE);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply it
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPMath::Apply(char *name, GIPTexture *pSource1, GIPTexture *pSource2, GIPTexture *pResult)
{
    UINT cPasses;

	HRESULT hr;

    ID3DXEffect *pEffect = m_pGipEffect->Effect();
    
	// set source images 
	pEffect->SetTexture( "Src1ImageTexture", pSource1->Texture());
	pEffect->SetTexture( "Src2ImageTexture", pSource2->Texture());
	m_pd3dDevice->SetRenderTarget( 0, pResult->Surface());

	hr = pEffect->SetTechnique(name);

	// Apply the technique contained in the effect - returns num of passes in cPasses
	hr = pEffect->Begin(&cPasses, 0);

	hr = pEffect->BeginPass(0);	// only one pass in each morph techniqe - pass 0

	// The effect interface queues up the changes and performs them 
	// with the CommitChanges call. You do not need to call CommitChanges if 
	// you are not setting any parameters between the BeginPass and EndPass.
	hr = pEffect->CommitChanges();

	// Render the simple quad with the applied technique
	m_pDevice->DrawQuad();

	// end tecnnique pass
	hr = pEffect->EndPass();

	// end technique
	pEffect->End();
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply it
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPMath::Apply(char *name, GIPTexture *pSource1, float val, GIPTexture *pResult)
{
    UINT cPasses;
    ID3DXEffect *pEffect = m_pGipEffect->Effect();
    
	// set source images 
	pEffect->SetTexture( "Src1ImageTexture", pSource1->Texture());
	m_pd3dDevice->SetRenderTarget( 0, pResult->Surface());

	pEffect->SetTechnique(name);
	pEffect->SetFloat("ConstValue", val);

	pEffect->Begin(&cPasses, 0);

	pEffect->BeginPass(0);
	pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	pEffect->EndPass();
	pEffect->End();
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply it
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPMath::Apply(char *name, GIPTexture *pSource1, GIPTexture *pResult)
{
    UINT cPasses;
    ID3DXEffect *pEffect = m_pGipEffect->Effect();
    
	// set source images 
	pEffect->SetTexture( "Src1ImageTexture", pSource1->Texture());
	m_pd3dDevice->SetRenderTarget( 0, pResult->Surface());

	pEffect->SetTechnique(name);
	pEffect->Begin(&cPasses, 0);
	pEffect->BeginPass(0);
	pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	pEffect->EndPass();
	pEffect->End();
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply it
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPMath::ApplyCrop(GIPTexture *pSource, GIPTexture *pResult, long x0, long y0, long bpp)
{
    UINT cPasses;
    ID3DXEffect *pEffect = m_pGipEffect->Effect();

	// set source images 
	pEffect->SetTexture( "Src1ImageTexture", pSource->Texture());
	pEffect->SetFloat("OffsetX",(float)((double)x0/(double)pSource->m_nTexWidth));
	pEffect->SetFloat("OffsetY",(float)((double)y0/(double)pSource->m_nTexHeight));
	pEffect->SetFloat("ScaleX",(float)((double)pResult->m_nTexWidth/(double)pSource->m_nTexWidth));
	pEffect->SetFloat("ScaleY",(float)((double)pResult->m_nTexHeight/(double)pSource->m_nTexHeight));

	m_pd3dDevice->SetRenderTarget( 0, pResult->Surface());
	if (bpp==24)
		pEffect->SetTechnique("CropImage");
	else
		pEffect->SetTechnique("CropGreyImage");
	pEffect->Begin(&cPasses, 0);
	pEffect->BeginPass(0);
	pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	pEffect->EndPass();
	pEffect->End();
    return FALSE;
}

BOOL GIPMath::ApplyResize(GIPTexture *pSource, GIPTexture *pResult, long bpp)
{
    UINT cPasses;
    ID3DXEffect *pEffect = m_pGipEffect->Effect();
    
	// set source images 
	pEffect->SetTexture( "Src1ImageTexture", pSource->Texture());
	pEffect->SetFloat("OffsetX",0.0);
	pEffect->SetFloat("OffsetY",0.0);
	pEffect->SetFloat("ScaleX",1.0);
	pEffect->SetFloat("ScaleY",1.0);

	m_pd3dDevice->SetRenderTarget( 0, pResult->Surface());
	if (bpp==24)
		pEffect->SetTechnique("CropImage");
	else
		pEffect->SetTechnique("CropGreyImage");
	pEffect->Begin(&cPasses, 0);

	pEffect->BeginPass(0);	// only one pass in each morph techniqe - pass 0
	pEffect->CommitChanges();
	m_pDevice->DrawQuad();
	pEffect->EndPass();
	pEffect->End();
    return FALSE;
}

////////////////////////////////////////////////////////////////////////////
// GIPImageCrop
//	- creates a new GIPImage as a Crop from the original image
//	  used by FFT to create the largest central part of the original
//	  image with a width and height which are a power of two
////////////////////////////////////////////////////////////////////////////
GIPImage** GIP::ImageCrop(GIP_IMAGE src, long x0, long y0, long w0, long h0, long overl)
{
	GIP_IMAGE dest;
	long width, height, bpp, overlap;

	if (!m_pMath) return (NULL);
	(*src)->GetImageDimensions(&width, &height, &overlap, &bpp);
	if ((x0+w0 > width) || (y0+h0 >height)) return (NULL);
	dest=GIPCreateImage(w0, h0, overl, bpp);
	if (m_pMath->Initialise())
	{
		// setup display quad
		pDevice->CreateQuad((*dest)->pTexture->m_nTexWidth, (*dest)->pTexture->m_nTexHeight);
		m_pMath->ApplyCrop((*src)->pTexture, (*dest)->pTexture, x0, y0, bpp);
	}
	return(dest);
}

////////////////////////////////////////////////////////////////////////////
// ImageResize
//	- creates a new GIPImage as a Resize from the original image
////////////////////////////////////////////////////////////////////////////
GIPImage** GIP::ImageResize(GIP_IMAGE src, long w0, long h0, long overl)
{
	GIP_IMAGE dest;
	long width, height, bpp, overlap;

	if (!m_pMath) return (NULL);
	(*src)->GetImageDimensions(&width, &height, &overlap, &bpp);
	dest=GIPCreateImage(w0, h0, overl, bpp);
	if (m_pMath->Initialise())
	{
		// setup display quad
		pDevice->CreateQuad((*dest)->pTexture->m_nTexWidth, (*dest)->pTexture->m_nTexHeight);
		m_pMath->ApplyResize((*src)->pTexture, (*dest)->pTexture, bpp);
	}
	return(dest);
}

