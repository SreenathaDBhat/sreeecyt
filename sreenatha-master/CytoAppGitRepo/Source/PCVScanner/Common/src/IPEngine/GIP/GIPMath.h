#pragma once
///////////////////////////////////////////////////////////////////////////////////////////////
// Simple Math functions written for the graphics card
// Written By Karl Ratcliff     190805
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gipcommon.h"

// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif


class DllExport GIPMath
{
public:
    GIPMath(GIPDevice *Device);
    ~GIPMath(void);

    BOOL Initialise();								// Initialisation 
    BOOL Apply(char *name, GIPTexture *pSource1, GIPTexture *pSource2, GIPTexture *pResult);    // Appy it
    BOOL Apply(char *name, GIPTexture *pSource1, float val, GIPTexture *pResult);    // Appy it
    BOOL Apply(char *name, GIPTexture *pSource1, GIPTexture *pResult);    // Appy it
    BOOL ApplyCrop(GIPTexture *pSource, GIPTexture *pResult, long x0, long y, long bpp);    // Appy it
	BOOL ApplyResize(GIPTexture *pSource, GIPTexture *pResult, long bpp);

private:
    BOOL CreateFX(void);															// Load and compile the FX file
    IDirect3DDevice9          *m_pd3dDevice;      // Our D3D device
    GIPDevice                   *m_pDevice;         // Our GIP Device
    GIPEffect					*m_pGipEffect;		// D3DX convolution effect interface
};
