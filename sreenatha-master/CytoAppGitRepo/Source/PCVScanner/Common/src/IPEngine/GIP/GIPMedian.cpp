///////////////////////////////////////////////////////////////////////////////////////////////
// A Mathematical functions for the graphics card
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include "stdafx.h"

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>
#include <d3dx9math.h>


#include "gipcommon.h"
#include "gip.h"
#include "gipeffect.h"
#include "GIPMedian.h"

GIPMedian::GIPMedian(GIPDevice *Device)
{
	ASSERT(Device);
    m_pDevice = Device;
    m_pd3dDevice = Device->GetDevice();
    ASSERT(m_pd3dDevice);
    m_pGipEffect = NULL;
}

GIPMedian::~GIPMedian(void)
{
	SAFE_DELETE(m_pGipEffect);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation - not much to do here
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPMedian::Initialise(int texWidth, int texHeight)
{
    if (CreateFX())
	{
		ID3DXEffect *pEff;

		pEff = m_pGipEffect->Effect();
		pEff->SetFloat( "TextureWidth", (float)texWidth);
		pEff->SetFloat( "TextureHeight", (float)texHeight);
		return TRUE;
	}
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create the FX
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPMedian::CreateFX(void)
{
	if (m_pGipEffect==NULL)
	{
		m_pGipEffect = new GIPEffect(m_pDevice);

        return m_pGipEffect->CreateFXFromResource(IDR_MEDIANFX); 
	}
	return(TRUE);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply it
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPMedian::Apply(int nPasses, GIP_IMAGE src, GIP_IMAGE target, GIP_IMAGE scratch0, GIP_IMAGE scratch1)
{
	BOOL		ok;
	float		offsetX, offsetY;
	long		Width, Height, Overlap, Bpp;
	UINT		cPasses;

	(*src)->GetImageDimensions(&Width, &Height, &Overlap, &Bpp);
	
    if (nPasses > 1)
	{
		if (scratch0)
            ok = TRUE;
    }
	else
	{
		ok = TRUE;
	}

	if ((ok) && (nPasses > 2))
	{
		if (scratch1)
            ok = TRUE;
	}
	else
	{
		ok = TRUE;
	}

	if (ok)
	{
		// init swap buffer index
		int swap = 0;

		ID3DXEffect *pEffect = m_pGipEffect->Effect();

		// set source image for first pass
		pEffect->SetTexture( "SourceImageTexture", (*src)->pTexture->Texture());
		offsetX=(float)1/(float)(*src)->pTexture->m_nTexWidth;
		offsetY=(float)1/(float)(*src)->pTexture->m_nTexHeight;
		pEffect->SetFloat("OffsetX", offsetX);
		pEffect->SetFloat("OffsetY", offsetY);

		pEffect->SetTechnique( "median" );
		
        for (int iPass = 0; iPass < nPasses; iPass++)
		{
			// set render target - should be final target buffer on last pass
			if (iPass == nPasses - 1)
				m_pd3dDevice->SetRenderTarget( 0, (*target)->pTexture->Surface());
			else if (swap==0)
				m_pd3dDevice->SetRenderTarget( 0, (*scratch0)->pTexture->Surface());
			else
				m_pd3dDevice->SetRenderTarget( 0, (*scratch1)->pTexture->Surface());


			// Apply the technique contained in the effect - returns num of passes in cPasses
			pEffect->Begin(&cPasses, 0);

			pEffect->BeginPass(0);	// only one pass in each morph techniqe - pass 0
			pEffect->CommitChanges();
			m_pDevice->DrawQuad();

			if (iPass != nPasses-1)
			{
				if (swap==0)
					pEffect->SetTexture( "SourceImageTexture", (*scratch0)->pTexture->Texture());
				else
					pEffect->SetTexture( "SourceImageTexture", (*scratch1)->pTexture->Texture());
				swap ^= 1;
			}

			pEffect->EndPass();
		}
			
        pEffect->End();
	}


	return FALSE;
}
