///////////////////////////////////////////////////////////////////////////////////////////////
// A Mathematical functions for the graphics card
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include "stdafx.h"

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>
#include <d3dx9math.h>


#include "gipcommon.h"
#include "gip.h"
#include "gipeffect.h"
#include "GIPMorph.h"

GIPMorph::GIPMorph(GIPDevice *Device)
{
	ASSERT(Device);
    m_pDevice = Device;
    m_pd3dDevice = Device->GetDevice();
    ASSERT(m_pd3dDevice);
    m_pGipEffect = NULL;
}

GIPMorph::~GIPMorph(void)
{
	SAFE_DELETE(m_pGipEffect);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation - not much to do here
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPMorph::Initialise(int texWidth, int texHeight)
{

    if (CreateFX())
	{
		ID3DXEffect *pEff;

		pEff = m_pGipEffect->Effect();
		pEff->SetFloat( "TextureWidth", (float)texWidth);
		pEff->SetFloat( "TextureHeight", (float)texHeight);
		return TRUE;
	}
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create the FX
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPMorph::CreateFX(void)
{
	if (m_pGipEffect == NULL)
	{
		m_pGipEffect = new GIPEffect(m_pDevice);
        return m_pGipEffect->CreateFXFromResource(IDR_MORPHFX); 
	}

	return(TRUE);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Apply it
///////////////////////////////////////////////////////////////////////////////////////////////

void GIPMorph::Apply(char *evenPassName, char *oddPassName, int nPasses, GIP_IMAGE src, GIP_IMAGE target, GIP_IMAGE scratch0, GIP_IMAGE scratch1)
{
	BOOL		ok = TRUE;
	long		Width, Height, Overlap, Bpp;
	UINT		cPasses;

	(*src)->GetImageDimensions(&Width, &Height, &Overlap, &Bpp);
	if (nPasses > 1)
	{
		if (scratch0)
            ok = TRUE;
        else
            ok = FALSE;
	}
    else
		ok = TRUE;

    if ((ok) && (nPasses > 2))
	{
		if (scratch1)
            ok = TRUE;
        else
            ok = FALSE;
	}
	else
		ok = TRUE;

    if (ok)
	{

		// init swap buffer index
		int swap = 0;

		ID3DXEffect *pEffect = m_pGipEffect->Effect();
	
		// set source image for first pass
		pEffect->SetTexture( "SourceImageTexture", (*src)->pTexture->Texture());

		for (int iPass = 0; iPass < nPasses; iPass++)
		{
			// set render target - should be final target buffer on last pass
			if (iPass == nPasses-1)
				m_pd3dDevice->SetRenderTarget( 0, (*target)->pTexture->Surface());
			else if (swap==0)
				m_pd3dDevice->SetRenderTarget( 0, (*scratch0)->pTexture->Surface());
			else
				m_pd3dDevice->SetRenderTarget( 0, (*scratch1)->pTexture->Surface());

			// swap technique to achieve round effect
			if (iPass & 1)
				pEffect->SetTechnique( oddPassName );
			else
				pEffect->SetTechnique( evenPassName );

			// Apply the technique contained in the effect - returns num of passes in cPasses
			pEffect->Begin(&cPasses, 0);

			pEffect->BeginPass(0);	// only one pass in each morph techniqe - pass 0

			// The effect interface queues up the changes and performs them 
			// with the CommitChanges call. You do not need to call CommitChanges if 
			// you are not setting any parameters between the BeginPass and EndPass.
			pEffect->CommitChanges();

			// Render the simple quad with the applied technique
			m_pDevice->DrawQuad();

			if (iPass != nPasses-1)
			{
				// make the result of this pass the source for the next pass
				if (swap==0)
					pEffect->SetTexture( "SourceImageTexture", (*scratch0)->pTexture->Texture());
				else
					pEffect->SetTexture( "SourceImageTexture", (*scratch1)->pTexture->Texture());

				// and set target to the other buffer
				swap ^= 1;
			}

			// end tecnnique pass
			pEffect->EndPass();

			// end technique
			pEffect->End();
		}
	}
	
}


void GIPMorph::Apply(char *evenPassName, char *oddPassName, int nPasses, GIP_IMAGE src, GIP_IMAGE src1, GIP_IMAGE target, GIP_IMAGE scratch0, GIP_IMAGE scratch1)
{
	BOOL		ok = TRUE;
	long		Width, Height, Overlap, Bpp;
	UINT		cPasses;

	(*src)->GetImageDimensions(&Width, &Height, &Overlap, &Bpp);
	if (nPasses > 1)
	{
		if (scratch0)
            ok = TRUE;
        else
            ok = FALSE;
	}
    else
		ok = TRUE;

    if ((ok) && (nPasses > 2))
	{
		if (scratch1)
            ok = TRUE;
        else
            ok = FALSE;
	}
	else
		ok = TRUE;

    if (ok)
	{

		// init swap buffer index
		int swap = 0;

		ID3DXEffect *pEffect = m_pGipEffect->Effect();
	
		// set source image for first pass
		pEffect->SetTexture( "SourceImageTexture", (*src)->pTexture->Texture());
		pEffect->SetTexture( "Image2Texture", (*src1)->pTexture->Texture());

		for (int iPass = 0; iPass < nPasses; iPass++)
		{
			// set render target - should be final target buffer on last pass
			if (iPass == nPasses-1)
				m_pd3dDevice->SetRenderTarget( 0, (*target)->pTexture->Surface());
			else if (swap==0)
				m_pd3dDevice->SetRenderTarget( 0, (*scratch0)->pTexture->Surface());
			else
				m_pd3dDevice->SetRenderTarget( 0, (*scratch1)->pTexture->Surface());

			// swap technique to achieve round effect
			if (iPass & 1)
				pEffect->SetTechnique( oddPassName );
			else
				pEffect->SetTechnique( evenPassName );

			// Apply the technique contained in the effect - returns num of passes in cPasses
			pEffect->Begin(&cPasses, 0);

			pEffect->BeginPass(0);	// only one pass in each morph techniqe - pass 0

			// The effect interface queues up the changes and performs them 
			// with the CommitChanges call. You do not need to call CommitChanges if 
			// you are not setting any parameters between the BeginPass and EndPass.
			pEffect->CommitChanges();

			// Render the simple quad with the applied technique
			m_pDevice->DrawQuad();

			if (iPass != nPasses-1)
			{
				// make the result of this pass the source for the next pass
				if (swap==0)
					pEffect->SetTexture( "SourceImageTexture", (*scratch0)->pTexture->Texture());
				else
					pEffect->SetTexture( "SourceImageTexture", (*scratch1)->pTexture->Texture());

				// and set target to the other buffer
				swap ^= 1;
			}

			// end tecnnique pass
			pEffect->EndPass();

			// end technique
			pEffect->End();
		}
	}
	
}