#pragma once
///////////////////////////////////////////////////////////////////////////////////////////////
// Simple Math functions written for the graphics card
// Written By Karl Ratcliff     190805
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gipcommon.h"
#include "GIPImage.h"

// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif


class DllExport GIPMorph
{
public:
    GIPMorph(GIPDevice *Device);
    ~GIPMorph(void);

	GIPEffect					*Effect(void) { return m_pGipEffect; }

    BOOL Initialise(int texWidth, int TexHeight);				// Initialisation 
    void Apply(char *evenPassName, char *oddPassName, int nPasses, GIP_IMAGE src, GIP_IMAGE target, GIP_IMAGE scratch0, GIP_IMAGE scratch1);
	void Apply(char *evenPassName, char *oddPassName, int nPasses, GIP_IMAGE src, GIP_IMAGE src1, GIP_IMAGE target, GIP_IMAGE scratch0, GIP_IMAGE scratch1);

private:
    BOOL CreateFX(void);															// Load and compile the FX file
    IDirect3DDevice9			*m_pd3dDevice;      // Our D3D device
    GIPDevice                   *m_pDevice;         // Our GIP Device
    GIPEffect					*m_pGipEffect;		// D3DX convolution effect interface
};
