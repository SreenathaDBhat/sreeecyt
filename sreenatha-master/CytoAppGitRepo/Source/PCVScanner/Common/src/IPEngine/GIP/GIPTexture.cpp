///////////////////////////////////////////////////////////////////////////////////////////////
// GIP Texture class - source file
// KR270805
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//      GIPTexture is a texture that sits in graphics memory
//      GIPSwapTexture is a texture that sits in system memory and transfers data
//      to and from a texture in graphics memory.
//

#include "stdafx.h"
#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>

#include "gipcommon.h"
#include "gip.h"
#include "giptexture.h"

#define DEBUG_SIGNATURE			"�����"

GIPTexture::GIPTexture(IDirect3DDevice9* Device)
{

    ASSERT(Device);

    m_nTexWidth = 0;
    m_nTexHeight = 0;
    m_nImageWidth = 0;
    m_nImageHeight = 0;
    m_nPitch = 0;
    m_pSurface = NULL;
    m_pTexture = NULL;
    m_pd3dDevice = Device;
    m_bCompressed = FALSE;
    m_nOverlap = 0;
    m_dwUsage = 0;
    m_Format = D3DFMT_UNKNOWN;

	/*TCHAR dbg[1024];
	_stprintf(dbg, DEBUG_SIGNATURE "   ALLOCATED %p\r\n", this);
	OutputDebugString(dbg);*/
}

GIPTexture::~GIPTexture(void)
{
    SAFE_RELEASE(m_pTexture);
    SAFE_RELEASE(m_pSurface);

	/*TCHAR dbg[1024];
	_stprintf(dbg, DEBUG_SIGNATURE " DEALLOCATED %p\r\n", this);
	OutputDebugString(dbg);*/
}


///////////////////////////////////////////////////////////////////////////////////////////////
// Create a graphics memory texture
///////////////////////////////////////////////////////////////////////////////////////////////

// Each image quadrant is packed into the r,g,b,a components of the texture
// Therefore texture is a quarter of the image size
// NOTE the overlap. We actually process a bit more than 1 quad image
// per texture component as we need to take account boundaries where
// the image has been split 
BOOL GIPTexture::Create(int Width, int Height, int Ovr)
{
	m_bCompressed = TRUE;
    m_nImageWidth = Width;
    m_nImageHeight = Height;
    m_nOverlap = Ovr;

    return CreateTexture(Ovr + Width / 2, 
                        Ovr + Height / 2,
                        D3DUSAGE_RENDERTARGET,
                        D3DFMT_A8R8G8B8,
                        D3DPOOL_DEFAULT);
}

BOOL GIPTexture::Create16B(int Width, int Height, int Ovr)
{
    m_bCompressed = TRUE;
    m_nImageWidth = Width;
    m_nImageHeight = Height;
    m_nOverlap = Ovr;

    return CreateTexture(Ovr + Width / 2, 
                        Ovr + Height / 2,
                        D3DUSAGE_RENDERTARGET,
                        D3DFMT_A16B16G16R16,
                        D3DPOOL_DEFAULT);
}

// Full size colour r,g,b,a texture - no quadrant packing
BOOL GIPTexture::Create(int Width, int Height)
{
	m_nImageWidth = Width;
    m_nImageHeight = Height;

    return CreateTexture(Width, 
                        Height,
                        D3DUSAGE_RENDERTARGET,
                        D3DFMT_A8R8G8B8,
                        D3DPOOL_DEFAULT);
}

// Full size colour r,g,b,a texture - no quadrant packing
BOOL GIPTexture::Create16B(int Width, int Height)
{
    m_nImageWidth = Width;
    m_nImageHeight = Height;

    return CreateTexture(Width, 
                        Height,
                        D3DUSAGE_RENDERTARGET,
                        D3DFMT_A16B16G16R16,
                        D3DPOOL_DEFAULT);
}

// 16 bit size floating point r,g,b,a texture
BOOL GIPTexture::CreateFP(int Width, int Height)
{
    m_nImageWidth = Width;
    m_nImageHeight = Height;

    return CreateTexture(Width, 
                        Height,
                        D3DUSAGE_RENDERTARGET,
                        D3DFMT_A16B16G16R16F,
                        D3DPOOL_DEFAULT);
}

// 32 bit size floating point r,g,b,a texture
BOOL GIPTexture::CreateFP32(int Width, int Height)
{
    m_nImageWidth = Width;
    m_nImageHeight = Height;

    return CreateTexture(Width, 
                        Height,
                        D3DUSAGE_RENDERTARGET,
                        D3DFMT_A32B32G32R32F,
                        D3DPOOL_DEFAULT);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Texture create 
// see IDirect3DDevice9::CreateTexture for info on Usage, Format and Pool
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPTexture::CreateTexture(int Width, int Height, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool)
{
    if (FAILED(m_pd3dDevice->CreateTexture(Width, Height, 1,
                                           Usage,
                                           Format,
                                           Pool, 
									       &m_pTexture, 
									       NULL)))
    {
        OutputDebugString("FAILED TO ALLOCATE TEXTURE\r\n");
	    return FALSE;
    }

    // and get pointers to their surfaces
    if (FAILED(m_pTexture->GetSurfaceLevel(0, &m_pSurface)))
    {
        SAFE_RELEASE(m_pTexture);
        OutputDebugString("GIPTexture GetSurfaceLevel Fail\r\n");
        return FALSE;
    }

    D3DSURFACE_DESC Desc;

    m_pTexture->GetLevelDesc(0,&Desc);
	m_nTexWidth = Desc.Width;
	m_nTexHeight = Desc.Height;
    
    // Keep track of the format
    m_Format = Format;

    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
// GIP Swap Texture Class
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
GIPSwapTexture::GIPSwapTexture(IDirect3DDevice9* Device) : GIPTexture(Device)
{
    memset((void *)&m_LockedRect, 0, sizeof(D3DLOCKED_RECT));
}

GIPSwapTexture::~GIPSwapTexture()
{
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create a system memory swap texture
// See GIPTexture Create for explanation on the different variants
///////////////////////////////////////////////////////////////////////////////////////////////
 
BOOL GIPSwapTexture::Create(int Width, int Height, int Ovr)             
{ 
    BOOL Ok;

	if (Ovr == 0) 
	{
		m_nImageWidth = Width;
		m_nImageHeight = Height;
		Ok = CreateTexture(Width, 
						   Height,
						   D3DUSAGE_DYNAMIC,
						   D3DFMT_A8R8G8B8,
						   D3DPOOL_SYSTEMMEM);
	}
	else
	{
		m_bCompressed = TRUE;
		m_nImageWidth = Width;
		m_nImageHeight = Height;
		m_nOverlap = Ovr;
		Ok = CreateTexture( Ovr + Width / 2, 
							Ovr + Height / 2,
							D3DUSAGE_DYNAMIC,
							D3DFMT_A8R8G8B8,
							D3DPOOL_SYSTEMMEM);
	}


    if (Ok && Lock())
    {
        m_nPitch = m_LockedRect.Pitch;
        UnLock();
    }
    return Ok;
}

BOOL GIPSwapTexture::Create(int Width, int Height)             
{ 
    BOOL Ok;

    m_nImageWidth = Width;
    m_nImageHeight = Height;

    Ok = CreateTexture(Width, 
                        Height,
                        D3DUSAGE_DYNAMIC,
                        D3DFMT_A8R8G8B8,
                        D3DPOOL_SYSTEMMEM);

    if (Ok && Lock())
    {
        m_nPitch = m_LockedRect.Pitch;
        UnLock();
    }
    return Ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create a system memory swap texture (16 Bit)
// See GIPTexture Create for explanation on the different variants
///////////////////////////////////////////////////////////////////////////////////////////////
 
BOOL GIPSwapTexture::Create16B(int Width, int Height, int Ovr)             
{ 
    BOOL Ok;

    m_bCompressed = TRUE;
    m_nImageWidth = Width;
    m_nImageHeight = Height;
    m_nOverlap = Ovr;

    Ok = CreateTexture( Ovr + Width / 2, 
                        Ovr + Height / 2,
                        D3DUSAGE_DYNAMIC,
                        D3DFMT_A16B16G16R16,
                        D3DPOOL_SYSTEMMEM);

    if (Ok && Lock())
    {
        m_nPitch = m_LockedRect.Pitch;
        UnLock();
    }
    return Ok;
}

BOOL GIPSwapTexture::Create16B(int Width, int Height)             
{ 
    BOOL Ok;

    m_nImageWidth = Width;
    m_nImageHeight = Height;

    Ok = CreateTexture(Width, 
                        Height,
                        D3DUSAGE_DYNAMIC,
                        D3DFMT_A16B16G16R16,
                        D3DPOOL_SYSTEMMEM);

    if (Ok && Lock())
    {
        m_nPitch = m_LockedRect.Pitch;
        UnLock();
    }
    return Ok;
}

BOOL GIPSwapTexture::CreateFP(int Width, int Height)             
{ 
    BOOL Ok;

    m_nImageWidth = Width;
    m_nImageHeight = Height;

    Ok = CreateTexture(Width, 
                        Height,
                        D3DUSAGE_DYNAMIC,
                        D3DFMT_A16B16G16R16F,
                        D3DPOOL_SYSTEMMEM);

    if (Ok && Lock())
    {
        m_nPitch = m_LockedRect.Pitch;
        UnLock();
    }
    return Ok;
}

BOOL GIPSwapTexture::CreateFP32(int Width, int Height)             
{ 
    BOOL Ok;

    m_nImageWidth = Width;
    m_nImageHeight = Height;

    Ok = CreateTexture(Width, 
                        Height,
                        D3DUSAGE_DYNAMIC,
                        D3DFMT_A32B32G32R32F,
                        D3DPOOL_SYSTEMMEM);

    if (Ok && Lock())
    {
        m_nPitch = m_LockedRect.Pitch;
        UnLock();
    }
    return Ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Lock the texture surface so we can manipulate it
// Returns a point 
///////////////////////////////////////////////////////////////////////////////////////////////

void *GIPSwapTexture::Lock(void)
{
    HRESULT hr;

	// lock surface while we fiddle with its bits
	hr = m_pSurface->LockRect(&m_LockedRect, NULL, 0);

    if (hr != S_OK)
    {
        D3DEXPLAIN(hr);
        return NULL;
    }

    return m_LockedRect.pBits;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Unlock it
///////////////////////////////////////////////////////////////////////////////////////////////

void GIPSwapTexture::UnLock(void)                                         // Unlock it
{
    m_pSurface->UnlockRect();
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Send this swap surface to a graphics mem surface
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPSwapTexture::Transmit(GIPTexture *pDest)  
{
	// transfer between system memory surface and render target
	if (FAILED(m_pd3dDevice->UpdateSurface(m_pSurface, NULL, pDest->Surface(), NULL)))
		return FALSE;

    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Get a graphics mem surface into this surface
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPSwapTexture::Receive(GIPTexture *pSource)       
{
	// transfer render target into system side surface
	if (FAILED(m_pd3dDevice->GetRenderTargetData(pSource->Surface(), m_pSurface)))
        return FALSE;

    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Stores the actual value in the texture, called from D3DXFillTexture below
// NOTE Somewhat lengthy and convoluted for what it actually does ! 
// If someone finds an alternative
// method for filling floating point textures then please let me know !
///////////////////////////////////////////////////////////////////////////////////////////////

VOID WINAPI FillTexel(D3DXVECTOR4* pOut, const D3DXVECTOR2* pTexCoord, const D3DXVECTOR2* pTexelSize, LPVOID pData)
{
    TexFillData FD = *(TexFillData *) pData;

    // NOTE these texture coordinates range from 0 to 1
    int x = (int)(pTexCoord->x * FD.W);
    int y = (int)(pTexCoord->y * FD.H);

    D3DXVECTOR4 *pV = FD.pV;

    pV += (x + y * FD.H);

   *pOut = D3DXVECTOR4(pV->x, pV->y, pV->z, pV->w);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Fill the texture with data specified as D3DXVECTOR4 pointed to by pData
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPSwapTexture::Put(D3DXVECTOR4 *pData)
{
    HRESULT hr = S_OK;

    m_FillData.pV = pData;
    m_FillData.W = m_nTexWidth;
    m_FillData.H = m_nTexHeight;
    m_FillData.P = m_nPitch;
    // Fill the texture using D3DXFillTexture
    hr = D3DXFillTexture(m_pTexture, (LPD3DXFILL2D)FillTexel, (void *) &m_FillData);

    return (hr == S_OK);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Fill our texture with BYTE data    
///////////////////////////////////////////////////////////////////////////////////////////////
   
BOOL GIPSwapTexture::Put(BYTE *pImageData) 
{
	// transfer image data from texture slot
	BYTE *texdata;

	// Lock surface before reading data - and get surface details
	if (!(texdata = (BYTE *)Lock()))
		return FALSE;

    int dW = m_nImageWidth  - m_nTexWidth;
    int dH = m_nImageHeight - m_nTexHeight;
	
	// ARGB data will be used to hold each quadrant of the gray image
	BYTE *bidata = pImageData;
	BYTE *gidata = pImageData + dW;
	BYTE *ridata = pImageData + dH * m_nImageWidth;
	BYTE *aidata = pImageData + dH * m_nImageWidth + dW;

	// Copy surface into image
	for (int row = 0; row < m_nTexHeight; row++)
	{
        for (int col = 0; col < m_nTexWidth; col++)
		{
			*texdata++ = *bidata++;
			*texdata++ = *gidata++;
			*texdata++ = *ridata++;
			*texdata++ = *aidata++;
		}
        bidata += dW;
        gidata += dW;
        ridata += dW;
        aidata += dW;
	}

	UnLock();

    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Fill our texture with BYTE data    
///////////////////////////////////////////////////////////////////////////////////////////////
   
BOOL GIPSwapTexture::Put(unsigned short *pImageData) 
{
	// transfer image data from texture slot
	unsigned short *texdata;

	// Lock surface before reading data - and get surface details
	if (!(texdata = (unsigned short *)Lock()))
		return FALSE;

    int dW = m_nImageWidth  - m_nTexWidth;
    int dH = m_nImageHeight - m_nTexHeight;
	
	// ARGB data will be used to hold each quadrant of the gray image
	unsigned short *ridata = pImageData;
	unsigned short *gidata = pImageData + dW;
	unsigned short *bidata = pImageData + dH * m_nImageWidth;
	unsigned short *aidata = pImageData + dH * m_nImageWidth + dW;

	// difference between image width and texture width (*4 for ARGB data)
//	m_LockedRect.Pitch=m_nImageWidth*2;

	// Copy surface into image
	for (int row = 0; row < m_nTexHeight; row++)
	{
        for (int col = 0; col < m_nTexWidth; col++)
		{
			*texdata++ = *bidata++;
			*texdata++ = *gidata++;
			*texdata++ = *ridata++;
			*texdata++ = *aidata++;
		}
        bidata += dW;
        gidata += dW;
        ridata += dW;
        aidata += dW;
	}

	UnLock();

    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Fill our texture with tri-component RGB data
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPSwapTexture::Put(RGBTRIPLE *pImageData)  
{
	if (!pImageData)
		return FALSE;

	// transfer image data from texture slot
	BYTE *texdata;
    
	// Lock surface before reading data - and get surface details
	if (!(texdata = (BYTE *)Lock()))
		return FALSE;

    RGBTRIPLE *idata = pImageData;

	// difference between image width and texture width (*4 for ARGB data)
	m_LockedRect.Pitch=m_nImageWidth*4;
	int wdiff = ((m_LockedRect.Pitch / 4) - m_nImageWidth);
	wdiff=0;
	// copy surface into image
	for (int row = 0; row < m_nImageHeight; row++)
	{
        for (int col = 0; col < m_nImageWidth; col++)
		{
            *texdata++ = idata->rgbtBlue;
            *texdata++ = idata->rgbtGreen;
            *texdata++ = idata->rgbtRed;
            *texdata++ = 0x0;
            idata++;
		}
		idata += wdiff;
	}

	UnLock();
        
    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Fill our texture with tri-component RGB data
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPSwapTexture::Put(RGB16BTRIPLE *pImageData)  
{
	if (!pImageData)
		return FALSE;

	// transfer image data from texture slot
	unsigned short *texdata;
    
	// Lock surface before reading data - and get surface details
	if (!(texdata = (unsigned short *)Lock()))
		return FALSE;

    RGB16BTRIPLE *idata = pImageData;

	// difference between image width and texture width (*4 for ARGB data)
	m_LockedRect.Pitch=m_nImageWidth*8;
	int wdiff = ((m_LockedRect.Pitch / 8) - m_nImageWidth);
	wdiff=0;

	// copy surface into image
	for (int row = 0; row < m_nImageHeight; row++)
	{
        for (int col = 0; col < m_nImageWidth; col++)
		{
            *texdata++ = idata->rgbtBlue;
            *texdata++ = idata->rgbtGreen;
            *texdata++ = idata->rgbtRed;
            *texdata++ = 0x0;
            idata++;
		}
		idata += wdiff;
	}

	UnLock();
        
    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Fill (grey or colour) texture with image part of RGB image
///////////////////////////////////////////////////////////////////////////////////////////////
BOOL GIPSwapTexture::Put(RGBTRIPLE *pImageData, long w, long h, long x0, long y0, int ColourMode)
{
    RGBTRIPLE	*idata, *bidata, *gidata, *ridata, *aidata;
	BYTE		*texdata, grey;

	if (!pImageData) return FALSE;
	if (!(texdata = (BYTE *)Lock())) return FALSE;
	if (this->m_nImageWidth != this->m_nTexWidth)
	{
		// put colour data in grey texture
		int dW = m_nImageWidth  - m_nTexWidth;
		int dH = m_nImageHeight - m_nTexHeight;
		
		for (int row = 0; row < m_nTexHeight; row++)
		{
			bidata = pImageData + (y0+row)*w + x0;
			gidata = pImageData + (y0+row)*w + x0 + dW;
			ridata = pImageData + (y0+row + dH)*w + x0;
			aidata = pImageData + (y0+row + dH)*w + x0 + dW;
			for (int col = 0; col < m_nTexWidth; col++)
			{
				grey=(BYTE)(0.59*(double)bidata->rgbtGreen+0.30*(double)bidata->rgbtRed+0.11*(double)bidata->rgbtBlue);
				*texdata++ = grey;
				grey=(BYTE)(0.59*(double)gidata->rgbtGreen+0.30*(double)gidata->rgbtRed+0.11*(double)gidata->rgbtBlue);
				*texdata++ = grey;
				grey=(BYTE)(0.59*(double)ridata->rgbtGreen+0.30*(double)ridata->rgbtRed+0.11*(double)ridata->rgbtBlue);
				*texdata++ = grey;
				grey=(BYTE)(0.59*(double)aidata->rgbtGreen+0.30*(double)aidata->rgbtRed+0.11*(double)aidata->rgbtBlue);
				*texdata++ = grey;
				bidata++;
				gidata++;
				ridata++;
				aidata++;
			}
		}
	}
	else
	{		
		if (ColourMode==IN_GREY)	// Colours are convertedto Greys
		{
			for (int row = 0; row < m_nImageHeight; row++)
			{
				idata = pImageData + (y0+row)*w + x0;
				for (int col = 0; col < m_nImageWidth; col++)
				{
					grey=(BYTE)(0.59*(double)idata->rgbtGreen+0.30*(double)idata->rgbtRed+0.11*(double)idata->rgbtBlue);
					*texdata++ = grey;
					*texdata++ = grey;
					*texdata++ = grey;
					*texdata++ = 0x0;
					idata++;
				}
			}
		}
		else	// Colours remain Colours
		{
			for (int row = 0; row < m_nImageHeight; row++)
			{
				idata = pImageData + (y0+row)*w + x0;
				for (int col = 0; col < m_nImageWidth; col++)
				{
					*texdata++ = idata->rgbtBlue;
					*texdata++ = idata->rgbtGreen;
					*texdata++ = idata->rgbtRed;
					*texdata++ = 0x0;
					idata++;
				}
			}
		}
	}
 	UnLock();
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Fill (grey or colour) texture with image part of Grey image
// 
///////////////////////////////////////////////////////////////////////////////////////////////
BOOL GIPSwapTexture::Put(BYTE *pImageData, long w, long h, long x0, long y0)
{
	BYTE	*texdata, *idata, *bidata, *gidata, *ridata, *aidata;

	if (!pImageData) return FALSE;
	if (!(texdata = (BYTE *)Lock())) return FALSE;
 	if (this->m_nImageWidth != this->m_nTexWidth)
	{
		// put grey data in grey texture
		int dW = m_nImageWidth  - m_nTexWidth;
		int dH = m_nImageHeight - m_nTexHeight;
		
		for (int row = 0; row < m_nTexHeight; row++)
		{
			bidata = pImageData + (y0+row)*w + x0;
			gidata = pImageData + (y0+row)*w + x0 + dW;
			ridata = pImageData + (y0+row + dH)*w + x0;
			aidata = pImageData + (y0+row + dH)*w + x0 + dW;
			for (int col = 0; col < m_nTexWidth; col++)
			{
				*texdata++ = *bidata++;
				*texdata++ = *gidata++;
				*texdata++ = *ridata++;
				*texdata++ = *aidata++;
			}
		}
	}
	else
	{
		// put grey data in colour texture
		for (int row = 0; row < m_nImageHeight; row++)
		{
			idata = pImageData + (y0+row)*w + x0;
			for (int col = 0; col < m_nImageWidth; col++)
			{
				*texdata++ = *idata;
				*texdata++ = *idata;
				*texdata++ = *idata;
				*texdata++ = 0x00;
				idata++;
			}
		}
	}
  	UnLock();
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Fill (grey or colour) texture with image part of RGB image
///////////////////////////////////////////////////////////////////////////////////////////////
BOOL GIPSwapTexture::Put(RGB16BTRIPLE *pImageData, long w, long h, long x0, long y0, int ColourMode)
{
    RGB16BTRIPLE	*idata, *bidata, *gidata, *ridata, *aidata;
	unsigned short	*texdata, grey;

	if (!pImageData) return FALSE;
	if (!(texdata = (unsigned short *)Lock())) return FALSE;
	if (this->m_nImageWidth != this->m_nTexWidth)
	{
		// put colour data in grey texture
		int dW = m_nImageWidth  - m_nTexWidth;
		int dH = m_nImageHeight - m_nTexHeight;
		
		for (int row = 0; row < m_nTexHeight; row++)
		{
			bidata = pImageData + (y0+row)*w + x0;
			gidata = pImageData + (y0+row)*w + x0 + dW;
			ridata = pImageData + (y0+row + dH)*w + x0;
			aidata = pImageData + (y0+row + dH)*w + x0 + dW;
			for (int col = 0; col < m_nTexWidth; col++)
			{
				grey=(unsigned short)(0.59*(double)bidata->rgbtGreen+0.30*(double)bidata->rgbtRed+0.11*(double)bidata->rgbtBlue);
				*texdata++ = grey;
				grey=(unsigned short)(0.59*(double)gidata->rgbtGreen+0.30*(double)gidata->rgbtRed+0.11*(double)gidata->rgbtBlue);
				*texdata++ = grey;
				grey=(unsigned short)(0.59*(double)ridata->rgbtGreen+0.30*(double)ridata->rgbtRed+0.11*(double)ridata->rgbtBlue);
				*texdata++ = grey;
				grey=(unsigned short)(0.59*(double)aidata->rgbtGreen+0.30*(double)aidata->rgbtRed+0.11*(double)aidata->rgbtBlue);
				*texdata++ = grey;
				bidata++;
				gidata++;
				ridata++;
				aidata++;
			}
		}
	}
	else
	{		
		if (ColourMode==IN_GREY)	// Colours are converted to Greys
		{
			for (int row = 0; row < m_nImageHeight; row++)
			{
				idata = pImageData + (y0+row)*w + x0;
				for (int col = 0; col < m_nImageWidth; col++)
				{
					grey=(unsigned short)(0.59*(double)idata->rgbtGreen+0.30*(double)idata->rgbtRed+0.11*(double)idata->rgbtBlue);
					*texdata++ = grey;
					*texdata++ = grey;
					*texdata++ = grey;
					*texdata++ = 0x0;
					idata++;
				}
			}
		}
		else	// Colours remain Colours
		{
			for (int row = 0; row < m_nImageHeight; row++)
			{
				idata = pImageData + (y0+row)*w + x0;
				for (int col = 0; col < m_nImageWidth; col++)
				{
					*texdata++ = idata->rgbtBlue;
					*texdata++ = idata->rgbtGreen;
					*texdata++ = idata->rgbtRed;
					*texdata++ = 0x0;
					idata++;
				}
			}
		}
	}
 	UnLock();
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Fill (grey or colour) texture with image part of Grey image
// 
///////////////////////////////////////////////////////////////////////////////////////////////
BOOL GIPSwapTexture::Put(unsigned short *pImageData, long w, long h, long x0, long y0)
{
	unsigned short	*texdata, *idata, *bidata, *gidata, *ridata, *aidata;

	if (!pImageData) return FALSE;
	if (!(texdata = (unsigned short *)Lock())) return FALSE;
 	if (this->m_nImageWidth != this->m_nTexWidth)
	{
		// put grey data in grey texture
		int dW = m_nImageWidth  - m_nTexWidth;
		int dH = m_nImageHeight - m_nTexHeight;
		
		for (int row = 0; row < m_nTexHeight; row++)
		{
			ridata = pImageData + (y0+row)*w + x0;
			gidata = pImageData + (y0+row)*w + x0 + dW;
			bidata = pImageData + (y0+row + dH)*w + x0;
			aidata = pImageData + (y0+row + dH)*w + x0 + dW;
			for (int col = 0; col < m_nTexWidth; col++)
			{
				*texdata++ = *bidata++;
				*texdata++ = *gidata++;
				*texdata++ = *ridata++;
				*texdata++ = *aidata++;
			}
		}
	}
	else
	{
		// put grey data in colour texture
		for (int row = 0; row < m_nImageHeight; row++)
		{
			idata = pImageData + (y0+row)*w + x0;
			for (int col = 0; col < m_nImageWidth; col++)
			{
				*texdata++ = *idata;
				*texdata++ = *idata;
				*texdata++ = *idata;
				*texdata++ = 0x00;
				idata++;
			}
		}
	}
  	UnLock();
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Get image from GPU into existing image buffer, the image is a single component
// that was previously packed into quadrants of a texture
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPSwapTexture::Get(BYTE *pImageData)
{
	if (!pImageData)
		return FALSE;

	// transfer image data from texture slot
	BYTE *texdata;
    
	// Lock surface before reading data - and get surface details
	if (!(texdata = (BYTE *)Lock()))
		return FALSE;

	// ARGB data is used to hold quadrant of gray image
	BYTE *idata = pImageData;

	// Do we have quad packed data - only works with pixel widths divisible by 2
	if (m_nOverlap != 0)
	{
		// difference between image width and texture width (*4 for ARGB data)
		int wdiff = (m_nTexWidth - m_nImageWidth / 2) * 4;

		// Image pointers	
		int dW = m_nImageWidth / 2;
		int dH = m_nImageHeight / 2;
		
		// ARGB data will be used to hold each quadrant of the gray image
		BYTE *bidata = pImageData;
		BYTE *gidata = pImageData + dW;
		BYTE *ridata = pImageData + dH * m_nImageWidth;
		BYTE *aidata = pImageData + dH * m_nImageWidth + dW;

		// Texture pointers
		BYTE *bdata = texdata;
		BYTE *gdata = texdata + m_nOverlap * 4 + 1;
		BYTE *rdata = texdata + m_nTexWidth * m_nOverlap * 4 + 2;
		BYTE *adata = texdata + (m_nTexWidth * m_nOverlap + m_nOverlap) * 4 + 3;


		for (int row = 0; row < dH; row++)
		{
			for (int col = 0; col < dW; col++)
			{
				*bidata++ = *bdata;
				*gidata++ = *gdata;
				*ridata++ = *rdata;
				*aidata++ = *adata;
				adata += 4;
				rdata += 4;
				gdata += 4;
				bdata += 4;
			}
	        
			bidata += dW;
			gidata += dW;
			ridata += dW;
			aidata += dW;

			adata += wdiff;
			rdata += wdiff;
			gdata += wdiff;
			bdata += wdiff;
		}
	}
	else
	{
		// Texture pointers
		BYTE *bidata = pImageData;
		BYTE *bdata = texdata;
		for (int i = 0; i < m_nImageWidth * m_nImageHeight; i++)
		{
			*bidata++ = *bdata;
			bdata += 4;
		}
	}

	UnLock();

	return TRUE;
};

///////////////////////////////////////////////////////////////////////////////////////////////
// Gets an image out of this texture. The image is a tri-component image.
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPSwapTexture::Get(RGBTRIPLE *pImageData)
{
	// transfer image data from texture slot
	BYTE *texdata;

	// Lock surface before reading data - and get surface details
	if (!(texdata = (BYTE *)Lock()))
		return FALSE;

	RGBTRIPLE *idata = pImageData;

    int Size = m_nImageHeight * m_nImageWidth; 

	// copy surface into image
	for (int i = 0; i < Size; i++)
	{
        idata->rgbtBlue  = *texdata++;
        idata->rgbtGreen = *texdata++;
        idata->rgbtRed   = *texdata++;
        idata++;
		texdata++;                      // Ignore the alpha channel
	}

	UnLock();

	return TRUE;
};

///////////////////////////////////////////////////////////////////////////////////////////////
// Get image from GPU into existing image buffer, the image is a single component (16 bit)
// that was previously packed into quadrants of a texture
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPSwapTexture::Get(unsigned short *pImageData)
{
	if (!pImageData)
		return FALSE;

	// transfer image data from texture slot
	unsigned short *texdata;
    
	// Lock surface before reading data - and get surface details
	if (!(texdata = (unsigned short *)Lock()))
		return FALSE;

	// ARGB data is used to hold quadrant of gray image
	unsigned short *idata = pImageData;

	// difference between image width and texture width (*4 for ARGB data)
	int wdiff = (m_nTexWidth - m_nImageWidth / 2) * 4;

    // Image pointers	
    int dW = m_nImageWidth / 2;
    int dH = m_nImageHeight / 2;
	
	// ARGB data will be used to hold each quadrant of the gray image
	unsigned short *ridata = pImageData;
	unsigned short *gidata = pImageData + dW;
	unsigned short *bidata = pImageData + dH * m_nImageWidth;
	unsigned short *aidata = pImageData + dH * m_nImageWidth + dW;

    // Texture pointers
    unsigned short *rdata = texdata + 2;
	unsigned short *gdata = texdata + m_nOverlap * 4 + 1;
	unsigned short *bdata = texdata + m_nTexWidth * m_nOverlap * 4;
	unsigned short *adata = texdata + (m_nTexWidth * m_nOverlap + m_nOverlap) * 4 + 3;


	for (int row = 0; row < dH; row++)
	{
        for (int col = 0; col < dW; col++)
		{
            *bidata++ = *bdata;
            *gidata++ = *gdata;
            *ridata++ = *rdata;
            *aidata++ = *adata;
			adata += 4;
			rdata += 4;
			gdata += 4;
			bdata += 4;
		}
        
        bidata += dW;
        gidata += dW;
        ridata += dW;
        aidata += dW;

        adata += wdiff;
        rdata += wdiff;
        gdata += wdiff;
        bdata += wdiff;
	}

	UnLock();

	return TRUE;
};

///////////////////////////////////////////////////////////////////////////////////////////////
// Gets an image out of this texture. The image is a tri-component image.
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPSwapTexture::Get(RGB16BTRIPLE *pImageData)
{
	// transfer image data from texture slot
	unsigned short *texdata;

	// Lock surface before reading data - and get surface details
	if (!(texdata = (unsigned short *)Lock()))
		return FALSE;

	RGB16BTRIPLE *idata = pImageData;

    int Size = m_nImageHeight * m_nImageWidth; 

	// copy surface into image
	for (int i = 0; i < Size; i++)
	{
        idata->rgbtBlue  = *texdata++;
        idata->rgbtGreen = *texdata++;
        idata->rgbtRed   = *texdata++;
        idata++;
		texdata++;                      // Ignore the alpha channel
	}

	UnLock();

	return TRUE;
};

//////////////////////////////////////////////////////////////////////////////////////////////
// Gets an image out of this texture. The image is a quad-component image.
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPSwapTexture::Get(RGBQUAD *pImageData)                      // Get image data - quad compoent RGBA variety
{	
	// transfer image data from texture slot
	BYTE *texdata;

	// Lock surface before reading data - and get surface details
	if (!(texdata = (BYTE *)Lock()))
		return FALSE;

	RGBQUAD *idata = pImageData;

    int Size = m_nImageHeight * m_nImageWidth; 

	// copy surface into image
    for (int i = 0; i < Size; i++)
    {
        idata->rgbBlue      = *texdata++;
        idata->rgbGreen     = *texdata++;
        idata->rgbRed       = *texdata++;
        idata->rgbReserved  = *texdata++;            
        idata++;
    }

	UnLock();

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Gets an image out of this texture (format:R16G16B16A16)
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPSwapTexture::Get(float *pImageData)                      // Get image data R16G16B16A16
{	
	// transfer image data from texture slot
	D3DXFLOAT16 *texdata;

	// Lock surface before reading data - and get surface details
	if (!(texdata = (D3DXFLOAT16 *)Lock()))
		return FALSE;

    int Size = m_nImageHeight * m_nImageWidth*4; 

	// copy surface into image
	D3DXFloat16To32Array(pImageData, texdata, Size);
	UnLock();

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Gets an image out of this texture (format:R32G32B32A32)
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPSwapTexture::GetFP32(float *pImageData)                      // Get image data R16G16B16A16
{	
	// transfer image data from texture slot
	float *texdata, *idata;

	// Lock surface before reading data - and get surface details
	if (!(texdata = (float *)Lock()))
		return FALSE;
    int Size = m_nImageHeight * m_nImageWidth*4; 

	idata=pImageData;

	// copy surface into image
	for (int i = 0; i < Size; i++) *idata++      = *texdata++;
	UnLock();

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Gets an image component out of this texture. The image is a 24-bit tri-component image.
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPSwapTexture::GetComponent(long comp, BYTE *pImageData)
{
	// transfer image data from texture slot
	BYTE *texdata;

	// Lock surface before reading data - and get surface details
	if (!(texdata = (BYTE *)Lock()))
		return FALSE;

	BYTE *idata = pImageData;

    int Size = m_nImageHeight * m_nImageWidth; 

	// copy surface into image
	for (int i = 0; i < Size; i++)
	{
        *idata++ = *(texdata+2-comp);		// 0=Red; 1=Green; 2=Blue
		texdata += 4;                      // Ignore the alpha channel
	}

	UnLock();

	return TRUE;
};

///////////////////////////////////////////////////////////////////////////////////////////////
// Gets an image component out of this texture. The image is a 48-bit tri-component image.
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL GIPSwapTexture::GetComponent(long comp, unsigned short *pImageData)
{
	// transfer image data from texture slot
	unsigned short *texdata;

	// Lock surface before reading data - and get surface details
	if (!(texdata = (unsigned short *)Lock()))
		return FALSE;

	unsigned short *idata = pImageData;

    int Size = m_nImageHeight * m_nImageWidth; 

	// copy surface into image
	for (int i = 0; i < Size; i++)
	{
        *idata++ = *(texdata+comp);		// 0=Red; 1=Green; 2=Blue
		texdata += 4;                      // Ignore the alpha channel
	}

	UnLock();

	return TRUE;
};

