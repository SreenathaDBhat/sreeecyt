#pragma once
///////////////////////////////////////////////////////////////////////////////////////////////
// GIP Texture class - header file
//
///////////////////////////////////////////////////////////////////////////////////////////////

#include <D3D9.h>
#include <d3d9types.h>
#include <d3dx9effect.h>
#include "gipcommon.h"


///////////////////////////////////////////////////////////////////////////////////////////////
// Struct used to fill floating point textures with data
///////////////////////////////////////////////////////////////////////////////////////////////

typedef struct
{
    int W, H, P;
    D3DXVECTOR4 *pV;
} TexFillData;

// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif

///////////////////////////////////////////////////////////////////////////////////////////////
// GIP graphics memory textures
///////////////////////////////////////////////////////////////////////////////////////////////

class DllExport GIPTexture
{
public:

    GIPTexture(IDirect3DDevice9* Device);                       // Nuff said
    ~GIPTexture();

    virtual BOOL Create(int Width, int Height, int Ovr);        // Create a graphics memory texture - image quadrants stored in R,G,B and A components
    virtual BOOL Create(int Width, int Height);                 // Create a graphics memory texture - RGB textures
    virtual BOOL Create16B(int Width, int Height, int Ovr);     // Create a graphics memory texture - image quadrants stored in R,G,B and A components (16 bits)
    virtual BOOL Create16B(int Width, int Height);              // Create a graphics memory texture - RGB textures (16 bits)
    virtual BOOL CreateFP(int Width, int Height);               // Floating point version
    virtual BOOL CreateFP32(int Width, int Height);             // Floating point 32 bit version ABRG
    BOOL CreateTexture(int Width, int Height, DWORD Usage, D3DFORMAT Format, D3DPOOL Pool);    // Create the texture

    // Accessors
    LPDIRECT3DSURFACE9 Surface(void) { return m_pSurface; };    // Access the texture
    LPDIRECT3DTEXTURE9 Texture(void) { return m_pTexture; };    // Access the surface
    operator LPDIRECT3DSURFACE9() { return m_pSurface; };       // Ditto
    operator LPDIRECT3DTEXTURE9() { return m_pTexture; };
    
    int                         m_nTexWidth;                    // Texture dimensions
    int                         m_nTexHeight;
    int                         m_nImageWidth;                  // Actual image dimensions
    int                         m_nImageHeight;
    int                         m_nPitch;
    BOOL                        m_bCompressed;
    int                         m_nOverlap;
    D3DFORMAT                   m_Format;                       // Texture format - see D3D documentation
    DWORD                       m_dwUsage;                      // Texture usage 

    LPDIRECT3DSURFACE9          m_pSurface;                     // The texture and its surface
    LPDIRECT3DTEXTURE9          m_pTexture;
    IDirect3DDevice9          *m_pd3dDevice;                  // Pointer to our D3D device   
};

///////////////////////////////////////////////////////////////////////////////////////////////
// GIP swap textures
///////////////////////////////////////////////////////////////////////////////////////////////

class DllExport GIPSwapTexture : public GIPTexture
{
public:

    GIPSwapTexture(IDirect3DDevice9* Device);           // Nuff said
    ~GIPSwapTexture();

    BOOL Create(int Width, int Height, int Ovr);        // Create a system memory swap texture
    BOOL Create(int Width, int Height);                 // Create a system memory swap texture
    BOOL Create16B(int Width, int Height, int Ovr);     // Create a system memory swap texture  (16 bits)
    BOOL Create16B(int Width, int Height);              // Create a graphics memory texture - RGB textures (16 bits)
	BOOL CreateFP(int Width, int Height);               // Create a system memory swap texture floating point
    BOOL CreateFP32(int Width, int Height);             // Create a system memory swap texture floating point

    BOOL Transmit(GIPTexture *pSource);                 // Send this swap surface to a graphics mem surface
    BOOL Receive(GIPTexture *pSource);                  // Get a graphics mem surface into this surface
    BOOL Put(D3DXVECTOR4 *pData);                       // Fill the surface with values specified by pData
    BOOL Put(RGBTRIPLE *pImageData);                    // Fill our texture with tri-component RGB data
    BOOL Put(RGB16BTRIPLE *pImageData);                 // Fill our texture with tri-component RGB (16 bit) data
    BOOL Put(BYTE *pImageData);                         // Fill our texture with BYTE data         
    BOOL Put(unsigned short *pImageData);               // Fill our texture with unsigned short data         
    BOOL Put(RGBTRIPLE *pImageData, long w, long h, long x0, long y0, int ColourMode);		// Fill (grey or colour) texture with image part of RGB image
	BOOL Put(RGB16BTRIPLE *pImageData, long w, long h, long x0, long y0, int ColourMode);	// Fill (grey 16bit or colour 16 bit) texture with image part of RGB image
    BOOL Put(BYTE *pImageData, long w, long h, long x0, long y0);							// Fill (grey or colour) texture with image part of Grey image
    BOOL Put(unsigned short *pImageData, long w, long h, long x0, long y0);					// Fill (grey or colour) texture with image part of Grey image
    BOOL Get(BYTE *pImageData);                         // Get image data - single component quadrant packed
    BOOL Get(unsigned short *pImageData);               // Get image data (16 bit) - single component quadrant packed
    BOOL Get(RGBTRIPLE *pImageData);                    // Get image data - tri-component RGB variety
    BOOL Get(RGB16BTRIPLE *pImageData);                 // Get image data - tri-component RGB (16bit per colour) variety
    BOOL Get(RGBQUAD *pImageData);                      // Get image data - quad compoent RGBA variety
	BOOL Get(float *pImageData);						// Get image data R16G16B16A16 (FLOAT16)
	BOOL GetFP32(float *pImageData);					// Get image data R32G32B32A32 (FLOAT32)
    BOOL GetComponent(long comp, BYTE *pImageData);     // Get image data - single component from 24-bit RGB image
    BOOL GetComponent(long comp,unsigned short *pImageData);     // Get image data  - single component from 48-bit RGB image

    void *Lock(void);                                   // Lock the underlying surface
    void UnLock(void);                                  // Unlock it

private:
    D3DLOCKED_RECT              m_LockedRect;
    TexFillData                 m_FillData;             // Data to fill the texture with
};
