// GIP.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "windows.h"
#include "GIPdll.h"

HINSTANCE GIPhinst = NULL;


HINSTANCE GetGIPInstanceHandle(void)
{
    return GIPhinst;
}

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#ifdef _MANAGED
#pragma managed(push, off)
#endif

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
    if (ul_reason_for_call == DLL_PROCESS_ATTACH)
        GIPhinst = hModule;

    return TRUE;
}

#ifdef _MANAGED
#pragma managed(pop)
#endif
