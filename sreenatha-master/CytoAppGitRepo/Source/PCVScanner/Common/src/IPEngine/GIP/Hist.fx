//--------------------------------------------------------------//
// Image Processing Effects File - Mark Gregson 13Apr05
//--------------------------------------------------------------//
//    Calling program sets up simple quad based on 4 vertices with 
//    with processed display position -1 to +1
//    and matching texture ranges         0 to  1
//
//    CUSTOMVERTEX vertices[] =
//    {
//        // x      Y     Z    U  V
//        {-1.0f, -1.0f, 0.0f, 0, 1},    //lower left
//        {+1.0f, -1.0f, 0.0f, 1, 1}, //lower right
//        {+1.0f, +1.0f, 0.0f, 1, 0}, //upper right
//        {-1.0f, +1.0f, 0.0f, 0, 0}    //upper left
//    };
//
//    After loading into the vertex buffer the quad is then drawn 
//  using DrawPrimitive (D3DPT_TRIANGLEFAN, 0, 2);
//
//    Because the vertices have effectively been processed
//    there is no need for a vertex shader unless additional
//    texture coordinates are generated, e.g. in morphology 
//    or NxN filters
//
//--------------------------------------------------------------//
// Morph Effects File - Mark Gregson 13Apr05
//--------------------------------------------------------------//
//
// Globals
float TextureWidth;                // Width of input texture - power of 2, so may be bigger than image
float TextureHeight;            // Height of input texture - power of 2, so may be bigger than image
texture SourceImageTexture;        // Base texture to load input image  
                                // Output image will go to current render target (backbuffer by default)
                                // Calling routines can feed output image back into SourceImageTexture to perform multipass dilations etc.

sampler SourceImage = sampler_state    // Sampler for base texture
{
    Texture = (SourceImageTexture);
    ADDRESSU = MIRROR;            // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;            //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

//--------------------------------------------------------------//
// 3x3 Axial and Diagonal Cross Min & Max Filters
//--------------------------------------------------------------//

struct Cross_VS_INPUT        // used for both axial and diagonal cross filters
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Cross_VS_OUTPUT        // used for both axial and diagonal cross filters
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
};

Cross_VS_OUTPUT AxialCrossVertexShader( Cross_VS_INPUT Input )
{
    Cross_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;
    Output.Tex1 = Output.Tex0;
    Output.Tex1.x = Output.Tex0.x-1.0f/TextureWidth;   // left
    Output.Tex2 = Output.Tex0;
    Output.Tex2.x = Output.Tex0.x+1.0f/TextureWidth;   // right
    Output.Tex3 = Output.Tex0;
    Output.Tex3.y = Output.Tex0.y-1.0f/TextureHeight;  // up
    Output.Tex4 = Output.Tex0;
    Output.Tex4.y = Output.Tex0.y+1.0f/TextureHeight;  // down

    return Output;
}


Cross_VS_OUTPUT DiagonalCrossVertexShader( Cross_VS_INPUT Input )
{
    Cross_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
 
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;
    Output.Tex1.x = Output.Tex0.x-1.0f/TextureWidth;        // left
    Output.Tex1.y = Output.Tex0.y-1.0f/TextureHeight;        // up
    Output.Tex2.x = Output.Tex0.x+1.0f/TextureWidth;        // right
    Output.Tex2.y = Output.Tex1.y;                        // up
    Output.Tex3.x = Output.Tex1.x;                        // left
    Output.Tex3.y = Output.Tex0.y+1.0f/TextureHeight;        // down
    Output.Tex4.x = Output.Tex2.x;                        // right
    Output.Tex4.y = Output.Tex3.y;                        // down

    return Output;
}


struct Cross_PS_INPUT    // used for both axial and diagonal cross filters
{
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
};


float4 MinCrossPixelShader( Cross_PS_INPUT Input ) : COLOR0    // used for both axial and diagonal cross filters
{  
    float4 result, val;

    result = tex2D(SourceImage, Input.Tex0);

    val = tex2D(SourceImage, Input.Tex1);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex2);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex3);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex4);
    result = min(val,result);

    return( result );   
}

float4 MaxCrossPixelShader( Cross_PS_INPUT Input ) : COLOR0    // used for both axial and diagonal cross filters
{  
   float4 result, val;
   
   result = tex2D(SourceImage, Input.Tex0);

   val = tex2D(SourceImage, Input.Tex1);
   result = max(val,result);
   val = tex2D(SourceImage, Input.Tex2);
   result = max(val,result);
   val = tex2D(SourceImage, Input.Tex3);
   result = max(val,result);
   val = tex2D(SourceImage, Input.Tex4);
   result = max(val,result);

   return( result );
}

//--------------------------------------------------------------//
// 3x3 Square Min & Max Filters
//--------------------------------------------------------------//

struct Square_VS_INPUT 
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Square_VS_OUTPUT 
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
    float2 Tex5 : TEXCOORD5;
    float2 Tex6 : TEXCOORD6;
    float2 Tex7 : TEXCOORD7;
};


Square_VS_OUTPUT SquareVertexShader( Square_VS_INPUT Input )
{
    Square_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left

    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;
    Output.Tex1 = Output.Tex0;
    Output.Tex1.x = Output.Tex0.x-1.0f/TextureWidth;   // left
    Output.Tex2 = Output.Tex0;
    Output.Tex2.x = Output.Tex0.x+1.0f/TextureWidth;   // right
    Output.Tex3 = Output.Tex0;
    Output.Tex3.y = Output.Tex0.y-1.0f/TextureHeight;  // up
    Output.Tex4 = Output.Tex0;
    Output.Tex4.y = Output.Tex0.y+1.0f/TextureHeight;  // down
    Output.Tex5 = Output.Tex1;
    Output.Tex5.y = Output.Tex3.y;                   // upper left
    Output.Tex6 = Output.Tex1;
    Output.Tex6.y = Output.Tex4.y;                   // lower left
    Output.Tex7 = Output.Tex2;
    Output.Tex7.y = Output.Tex3.y;                   // upper right

    return Output;
}

struct Square_PS_INPUT 
{
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
    float2 Tex5 : TEXCOORD5;
    float2 Tex6 : TEXCOORD6;
    float2 Tex7 : TEXCOORD7;
};


float4 MinSquarePixelShader( Square_PS_INPUT Input ) : COLOR0
{  
    float4 result, val;
    float2 Tex8;
    Tex8.x = Input.Tex0.x + 1.0f/TextureWidth;
    Tex8.y = Input.Tex0.y + 1.0f/TextureHeight;

    result = tex2D(SourceImage, Input.Tex0);

    val = tex2D(SourceImage, Input.Tex1);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex2);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex3);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex4);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex5);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex6);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex7);
    result = min(val,result);
    val = tex2D(SourceImage, Tex8);
    result = min(val,result);

    return( result );
}

float4 MaxSquarePixelShader( Square_PS_INPUT Input ) : COLOR0
{  

    float4 result, val;
    float2 Tex8;
    Tex8.x = Input.Tex0.x + 1.0f/TextureWidth;
    Tex8.y = Input.Tex0.y + 1.0f/TextureHeight;

    result = tex2D(SourceImage, Input.Tex0);

    val = tex2D(SourceImage, Input.Tex1);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex2);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex3);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex4);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex5);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex6);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex7);
    result = max(val,result);
    val = tex2D(SourceImage, Tex8);
    result = max(val,result);

    return( result );
}

//--------------------------------------------------------------//
// Technique Section for Morph
//--------------------------------------------------------------//

technique MaxAxialCross
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 AxialCrossVertexShader();
        PixelShader = compile ps_2_0 MaxCrossPixelShader();
    }
}

technique MaxDiagonalCross
{
   pass P0
   {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 DiagonalCrossVertexShader();
        PixelShader = compile ps_2_0 MaxCrossPixelShader();
   }
}

technique MaxSquare
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 SquareVertexShader();
        PixelShader = compile ps_2_0 MaxSquarePixelShader();
    }
}

technique MinAxialCross
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 AxialCrossVertexShader();
        PixelShader = compile ps_2_0 MinCrossPixelShader();
    }
}

technique MinDiagonalCross
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 DiagonalCrossVertexShader();
        PixelShader = compile ps_2_0 MinCrossPixelShader();
    }
}

technique MinSquare
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 SquareVertexShader();
        PixelShader = compile ps_2_0 MinSquarePixelShader();
    }
}


//--------------------------------------------------------------//
// Math Effects
//--------------------------------------------------------------//
//
// Globals
texture Src1ImageTexture;        // Base texture to load input image  
texture Src2ImageTexture;        // Base texture to load input image  
                                // Output image will go to current render target (backbuffer by default)
                                // Calling routines can feed output image back into SourceImageTexture to perform multipass dilations etc.

sampler Src1 = sampler_state    // Sampler for source 1 image
{
    Texture = (Src1ImageTexture);
    ADDRESSU = MIRROR;
    ADDRESSV = MIRROR;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler Src2 = sampler_state    // Sampler for source 1 image
{
    Texture = (Src2ImageTexture);
    ADDRESSU = MIRROR;
    ADDRESSV = MIRROR;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

//--------------------------------------------------------------//
// Mathematical filters Output = Src1 OP Src2    e.g. Output = Src1 - Src2
//--------------------------------------------------------------//

struct Math_VS_INPUT        
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Math_VS_OUTPUT       
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
};

Math_VS_OUTPUT MathVertexShader( Math_VS_INPUT Input )
{
    Math_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     Output.Position.y = -Input.Position.y;    // because textures have inverted coordinates 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;

    return Output;
}

//--------------------------------------------------------------//

struct Math_PS_INPUT 
{
    float2 Tex0 : TEXCOORD0;
};

//--------------------------------------------------------------//

float4 MathSubPixelShader( Math_PS_INPUT Input ) : COLOR0    
{  
    return ( clamp(tex2D(Src1, Input.Tex0) - tex2D(Src2,Input.Tex0), 0.0, 1.0) );
}
//--------------------------------------------------------------//

float4 MathDiffPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
    return ( abs(tex2D(Src1, Input.Tex0) - tex2D(Src2,Input.Tex0)) );
}
//--------------------------------------------------------------//

float4 MathAddPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
    return ( clamp(tex2D(Src1, Input.Tex0) + tex2D(Src2,Input.Tex0), 0.0, 1.0) );
}
//--------------------------------------------------------------//

float4 MathMulPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
    return ( clamp(tex2D(Src1, Input.Tex0) * tex2D(Src2,Input.Tex0), 0.0, 1.0) );
}
//--------------------------------------------------------------//

float4 MathDivPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
    return ( clamp(tex2D(Src1, Input.Tex0) / tex2D(Src2,Input.Tex0), 0.0, 1.0) );
}
//--------------------------------------------------------------//
// Technique Section for Math
//--------------------------------------------------------------//

technique MathSub
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathSubPixelShader();
    }
}

technique MathAdd
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathAddPixelShader();
    }
}
technique MathMul
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathMulPixelShader();
    }
}
technique MathDiv
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathDivPixelShader();
    }
}
technique MathDiff
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathDiffPixelShader();
    }
}


//--------------------------------------------------------------//
// Straight transfer of image - to move render target texture to backbuffer
//--------------------------------------------------------------//

struct Show_VS_INPUT       
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Show_VS_OUTPUT    
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
};

Show_VS_OUTPUT ShowVertexShader( Show_VS_INPUT Input )
{
    Show_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     Output.Position.y = -Input.Position.y;    // because textures have inverted coordinates 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;

    return Output;
}

float4 ShowPixelShader( Math_PS_INPUT Input ) : COLOR0  
{  
    return ( tex2D(SourceImage, Input.Tex0));
}

technique Show
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 ShowVertexShader();
        PixelShader = compile ps_2_0 ShowPixelShader();
    }
}
//--------------------------------------------------------------//
// Thresholding routines
//--------------------------------------------------------------//
struct Thresh_VS_INPUT  
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Thresh_VS_OUTPUT 
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
};

Thresh_VS_OUTPUT ThreshVertexShader( Show_VS_INPUT Input )
{
    Show_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;    // because textures have inverted coordinates 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;

    return Output;
}


float SG;          //Sum of gradients
float SE;          //Sum of gradients x value
float4 mode;        //mode of image
float4 valmax;      //20% of dynamic range + mode
float4 hist0[4];    //histogram
float4 hist1[4];
float4 hist2[4];
float4 hist3[4];
float4 hist4[4];
float4 hist5[4];
float4 hist6[4];
float4 hist7[4];
float4 hist8[4];
float4 hist9[4];
float4 hist10[4];
float4 hist11[4];
float4 hist12[4];
float4 hist13[4];
float4 hist14[4];
float4 hist15[4];

float4 hinc(int col,int index)
{
    int comp=int(col-index*4);
    float4 addit=0;
    if (comp==0) addit.b=1;
    else
        if (comp==1) addit.g=1;
        else
            if (comp==2) addit.r=1;
            else
                if (comp==3) addit.a=1;
                
    return addit;
}

float4 HistPixelShader1( Cross_PS_INPUT Input ) : COLOR0    // used for both axial and diagonal cross filters
{  
    float4 mask, modemask, gradmask, val, cval, grad, fgrad;
    int index,comp;
    
    // get pixel value
    cval = tex2D(SourceImage, Input.Tex0) * 256.0f;
    cval = clamp(cval, 0,256);
    
    if (cval.r < 16)
    {
        index=int(cval.r/4);
        hist0[index]+=hinc(cval.r,index);
    }
    else
    if (cval.r < 32)
    {
        cval.r-=16;
        index=int(cval.r/4);
        hist1[index]=hinc(cval.r,index);
    }
        

}

float4 ThreshPixelShader( Cross_PS_INPUT Input ) : COLOR0    // used for both axial and diagonal cross filters
{  
    float4 mask, modemask, gradmask, val, cval, grad, fgrad;

    // get centre pixel value
    cval = tex2D(SourceImage, Input.Tex0);
/*
    // ignore dark areas inside objects and background (below mode)
    // if ((val < 0) || (val > valmax)) then ignore this pixel
    // must check each rgba component because each contain a grey value
    // and continue with the operation if any pass the test
    // to remove unwanted components later we generate a mask
    modemask = clamp(cval - mode, 1,0);            // if value is < mode the mask will be 0
    modemask = sign(modemask);                    // if value is > 0 the mask will be 1
    mask = clamp(valmax-cval, 1,0);                // if value is > valmax the mask will be 0
    mask = sign(mask);                            // if value is > 0 the mask will be 1
    mask *= modemask;                            // composite mask
    
    // if all mask components are 0 then nothing to do
    if (any(mask) == false)
        return cval;
*/
    val = tex2D(SourceImage, Input.Tex1);        //left
    fgrad = abs(cval - val);
        
    val = tex2D(SourceImage, Input.Tex2);        //right
    grad = abs(cval - val);
    fgrad = max(grad, fgrad); 
    
    val = tex2D(SourceImage, Input.Tex3);        //above
    grad = abs(cval - val);
    fgrad = max(grad, fgrad); 

    val = tex2D(SourceImage, Input.Tex4);        //below
    grad = abs(cval - val);
    fgrad = max(grad, fgrad); 

    // ignore small gradients i.e less than 8
    // by clamping result of subtraction,  8/256 = 0.03125
    gradmask = clamp(fgrad - 0.03125, 0.0, 1.0);
    gradmask = sign(gradmask);
//    mask*=gradmask;
    
    // apply mask to remove unwanted components
//    fgrad *= mask;
//    fgrad *= gradmask;
    
    // sum max gradients
//    SE+=fgrad;
 //   SG+=cval*fgrad;
    
    SE+= fgrad.b+fgrad.r+fgrad.g+fgrad.a;
    SG+= dot(cval,fgrad);
    // leave image unchanged
    return cval;   
}


technique Thresh
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 ThreshVertexShader();
        PixelShader = compile ps_2_0 ThreshPixelShader();
    }
}

