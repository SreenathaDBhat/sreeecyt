//------------------------------------------------------------------------//
// Histogram Effect file
//	-	In the first pass the histogram of a grey (or colour image) is
//		calculated and stored in the global variables hist0[4]-hist15[4]
//	-	In the second pass the Histogram texture (render target)
//		is filled with the histogram data
//
//------------------------------------------------------------------------//
texture ImaTex0;

sampler SrcImage0 = sampler_state		// Sampler for base texture
{
    Texture = (ImaTex0);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};
int	colourMode;

float4 hist0[4];
float4 hist1[4];
float4 hist2[4];
float4 hist3[4];
float4 hist4[4];
float4 hist5[4];
float4 hist6[4];
float4 hist7[4];
float4 hist8[4];
float4 hist9[4];
float4 hist10[4];
float4 hist11[4];
float4 hist12[4];
float4 hist13[4];
float4 hist14[4];
float4 hist15[4];

struct HIST_VS_INPUT        
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct HIST_VS_OUTPUT       
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
};

struct HIST_PS_INPUT 
{
    float2 Tex0 : TEXCOORD0;
};

HIST_VS_OUTPUT HistVertexShader( HIST_VS_INPUT Input )
{
    HIST_VS_OUTPUT Output;

    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;    // because textures have inverted coordinates 0,0 is upper left
    Output.Tex0 = Input.Tex;
    return Output;
}

//--------------------------------------------------------------//



float4 HistIndexIncrease (int col)
{
	float4 addit;
	if (col<2)
	{
		if (col<1) addit.b=1;
		 else addit.g=1;
	}
	else
	{
		if (col<3) addit.r=1;
		 else addit.a=1;
	}
	return (addit);
}

void HistIncrease (int col)
{
	int		index;
	
	if (col <128)
	{
		if (col <64)
		{
			if (col < 32)
			{
				if (col < 16)
				{
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist0[0] += HistIndexIncrease(col-index*4);
							else hist0[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist0[2] += HistIndexIncrease(col-index*4);
							else hist0[3] += HistIndexIncrease(col-index*4);
					}
				}
				else
				{
					col -=16;
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist1[0] += HistIndexIncrease(col-index*4);
							else hist1[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist1[2] += HistIndexIncrease(col-index*4);
							else hist1[3] += HistIndexIncrease(col-index*4);
					}
				}
			}
			else
			{
				if (col < 48)
				{
					col -=32;
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist2[0] += HistIndexIncrease(col-index*4);
							else hist2[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist2[2] += HistIndexIncrease(col-index*4);
							else hist2[3] += HistIndexIncrease(col-index*4);
					}
				}
				else
				{
					col -=48;
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist3[0] += HistIndexIncrease(col-index*4);
							else hist3[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist3[2] += HistIndexIncrease(col-index*4);
							else hist3[3] += HistIndexIncrease(col-index*4);
					}
				}
			}
		}
		else
		{
			if (col < 96)
			{
				if (col < 80)
				{
					col -=64;
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist4[0] += HistIndexIncrease(col-index*4);
							else hist4[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist4[2] += HistIndexIncrease(col-index*4);
							else hist4[3] += HistIndexIncrease(col-index*4);
					}
				}
				else
				{
					col -=80;
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist5[0] += HistIndexIncrease(col-index*4);
							else hist5[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist5[2] += HistIndexIncrease(col-index*4);
							else hist5[3] += HistIndexIncrease(col-index*4);
					}
				}
			}
			else
			{
				if (col < 112)
				{
					col -=96;
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist6[0] += HistIndexIncrease(col-index*4);
							else hist6[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist6[2] += HistIndexIncrease(col-index*4);
							else hist6[3] += HistIndexIncrease(col-index*4);
					}
				}
				else
				{
					col -=112;
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist7[0] += HistIndexIncrease(col-index*4);
							else hist7[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist7[2] += HistIndexIncrease(col-index*4);
							else hist7[3] += HistIndexIncrease(col-index*4);
					}
				}
			}
		}
	}
	else
	{
		if (col <192)
		{
			if (col < 160)
			{
				if (col < 144)
				{
					col -=128;
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist8[0] += HistIndexIncrease(col-index*4);
							else hist8[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist8[2] += HistIndexIncrease(col-index*4);
							else hist8[3] += HistIndexIncrease(col-index*4);
					}
				}
				else
				{
					col -=144;
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist9[0] += HistIndexIncrease(col-index*4);
							else hist9[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist9[2] += HistIndexIncrease(col-index*4);
							else hist9[3] += HistIndexIncrease(col-index*4);
					}
				}
			}
			else
			{
				if (col < 176)
				{
					col -=160;
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist10[0] += HistIndexIncrease(col-index*4);
							else hist10[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist10[2] += HistIndexIncrease(col-index*4);
							else hist10[3] += HistIndexIncrease(col-index*4);
					}
				}
				else
				{
					col -=176;
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist11[0] += HistIndexIncrease(col-index*4);
							else hist11[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist11[2] += HistIndexIncrease(col-index*4);
							else hist11[3] += HistIndexIncrease(col-index*4);
					}
				}
			}
		}
		else
		{
			if (col < 224)
			{
				if (col < 208)
				{
					col -=192;
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist12[0] += HistIndexIncrease(col-index*4);
							else hist12[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist12[2] += HistIndexIncrease(col-index*4);
							else hist12[3] += HistIndexIncrease(col-index*4);
					}
				}
				else
				{
					col -=208;
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist13[0] += HistIndexIncrease(col-index*4);
							else hist13[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist13[2] += HistIndexIncrease(col-index*4);
							else hist13[3] += HistIndexIncrease(col-index*4);
					}
				}
			}
			else
			{
				if (col < 240)
				{
					col -=224;
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist14[0] += HistIndexIncrease(col-index*4);
							else hist14[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist14[2] += HistIndexIncrease(col-index*4);
							else hist14[3] += HistIndexIncrease(col-index*4);
					}
				}
				else
				{
					col -=240;
					index=col/4;
					if (index <2)
					{
						if (index < 1) hist15[0] += HistIndexIncrease(col-index*4);
							else hist15[1] += HistIndexIncrease(col-index*4);
					}
					else
					{
						if (index < 3) hist15[2] += HistIndexIncrease(col-index*4);
							else hist15[3] += HistIndexIncrease(col-index*4);
					}
				}
			}
		}
	}
}

float4 GetHistVal (int col)
{
	float4  val;
	int		index;
	
	if (col <128)
	{
		if (col <64)
		{
			if (col < 32)
			{
				if (col < 16)
				{
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist0[0];
							else val=hist0[1];
					}
					else
					{
						if (index < 3) val=hist0[2];
							else val=hist0[3];
					}
				}
				else
				{
					col -=16;
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist1[0];
							else val=hist1[1];
					}
					else
					{
						if (index < 3) val=hist1[2];
							else val=hist1[3];
					}
				}
			}
			else
			{
				if (col < 48)
				{
					col -=32;
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist2[0];
							else val=hist2[1];
					}
					else
					{
						if (index < 3) val=hist2[2];
							else val=hist2[3];
					}
				}
				else
				{
					col -=48;
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist3[0];
							else val=hist3[1];
					}
					else
					{
						if (index < 3) val=hist3[2];
							else val=hist3[3];
					}
				}
			}
		}
		else
		{
			if (col < 96)
			{
				if (col < 80)
				{
					col -=64;
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist4[0];
							else val=hist4[1];
					}
					else
					{
						if (index < 3) val=hist4[2];
							else val=hist4[3];
					}
				}
				else
				{
					col -=80;
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist5[0];
							else val=hist5[1];
					}
					else
					{
						if (index < 3) val=hist5[2];
							else val=hist5[3];
					}
				}
			}
			else
			{
				if (col < 112)
				{
					col -=96;
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist6[0];
							else val=hist6[1];
					}
					else
					{
						if (index < 3) val=hist6[2];
							else val=hist6[3];
					}
				}
				else
				{
					col -=112;
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist7[0];
							else val=hist7[1];
					}
					else
					{
						if (index < 3) val=hist7[2];
							else val=hist7[3];
					}
				}
			}
		}
	}
	else
	{
		if (col <192)
		{
			if (col < 160)
			{
				if (col < 144)
				{
					col -=128;
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist8[0];
							else val=hist8[1];
					}
					else
					{
						if (index < 3) val=hist8[2];
							else val=hist8[3];
					}
				}
				else
				{
					col -=144;
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist9[0];
							else val=hist9[1];
					}
					else
					{
						if (index < 3) val=hist9[2];
							else val=hist9[3];
					}
				}
			}
			else
			{
				if (col < 176)
				{
					col -=160;
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist10[0];
							else val=hist10[1];
					}
					else
					{
						if (index < 3) val=hist10[2];
							else val=hist10[3];
					}
				}
				else
				{
					col -=176;
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist11[0];
							else val=hist11[1];
					}
					else
					{
						if (index < 3) val=hist11[2];
							else val=hist11[3];
					}
				}
			}
		}
		else
		{
			if (col < 224)
			{
				if (col < 208)
				{
					col -=192;
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist12[0];
							else val=hist12[1];
					}
					else
					{
						if (index < 3) val=hist12[2];
							else val=hist12[3];
					}
				}
				else
				{
					col -=208;
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist13[0];
							else val=hist13[1];
					}
					else
					{
						if (index < 3) val=hist13[2];
							else val=hist13[3];
					}
				}
			}
			else
			{
				if (col < 240)
				{
					col -=224;
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist14[0];
							else val=hist14[1];
					}
					else
					{
						if (index < 3) val=hist14[2];
							else val=hist14[3];
					}
				}
				else
				{
					col -=240;
					index=col/4;
					if (index <2)
					{
						if (index < 1) val=hist15[0];
							else val=hist15[1];
					}
					else
					{
						if (index < 3) val=hist15[2];
							else val=hist15[3];
					}
				}
			}
		}
	}
	return (val);
}

float4 GreyHistPixelShader (HIST_PS_INPUT Input) : COLOR0 
{
	float4 cval, val;
	val=tex2D(SrcImage0, Input.Tex0);
	cval=val*256.;
	cval=clamp(cval, 0, 256);
	HistIncrease(cval.r);
	HistIncrease(cval.g);
	HistIncrease(cval.b);
	HistIncrease(cval.a);
	return (val);
}

float4 HistPixelShader (HIST_PS_INPUT Input) : COLOR0 
{
	float4 cval, val;
	val=tex2D(SrcImage0, Input.Tex0);
	cval=val*256.;
	cval=clamp(cval, 0, 256);
	if (colourMode && 1) HistIncrease(cval.r);
	if (colourMode && 2) HistIncrease(cval.g);
	if (colourMode && 4) HistIncrease(cval.b);
	return (val);
}

float4 ReadHistPixelShader (HIST_PS_INPUT Input) : COLOR0 
{
	float4 cval, val;
	cval=Input.Tex0.x *256.0;
	cval=clamp(cval, 0, 256);
	val=GetHistVal(cval);
	return (val);
}

technique GreyHistogram
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 HistVertexShader();
        PixelShader = compile ps_3_0 GreyHistPixelShader();
    }
    pass P1
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 HistVertexShader();
        PixelShader = compile ps_3_0 ReadHistPixelShader();
    }
}


technique Histogram
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 HistVertexShader();
        PixelShader = compile ps_3_0 HistPixelShader();
    }
    pass P1
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 HistVertexShader();
        PixelShader = compile ps_3_0 ReadHistPixelShader();
    }
}

