
//--------------------------------------------------------------//
// Image Processing Effects File - Mark Gregson 13Apr05
//--------------------------------------------------------------//
//    Calling program sets up simple quad based on 4 vertices with 
//    with processed display position -1 to +1
//    and matching texture ranges         0 to  1
//
//    CUSTOMVERTEX vertices[] =
//    {
//        // x      Y     Z    U  V
//        {-1.0f, -1.0f, 0.0f, 0, 1},    //lower left
//        {+1.0f, -1.0f, 0.0f, 1, 1}, //lower right
//        {+1.0f, +1.0f, 0.0f, 1, 0}, //upper right
//        {-1.0f, +1.0f, 0.0f, 0, 0}    //upper left
//    };
//
//    After loading into the vertex buffer the quad is then drawn 
//  using DrawPrimitive (D3DPT_TRIANGLEFAN, 0, 2);
//
//    Because the vertices have effectively been processed
//    there is no need for a vertex shader unless additional
//    texture coordinates are generated, e.g. in morphology 
//    or NxN filters
//
//--------------------------------------------------------------//
// Morph Effects File - Mark Gregson 13Apr05
//--------------------------------------------------------------//
//
// Globals
float TextureWidth;             // Width of input texture - power of 2, so may be bigger than image
float TextureHeight;            // Height of input texture - power of 2, so may be bigger than image
texture SourceImageTexture;     // Base texture to load input image  
                                // Output image will go to current render target (backbuffer by default)
                                // Calling routines can feed output image back into SourceImageTexture to perform multipass dilations etc.

// Globals
texture Src1ImageTexture;        // Base texture to load input image  
texture Src2ImageTexture;        // Base texture to load input image  
texture Src3ImageTexture;
                                 // Output image will go to current render target (backbuffer by default)

float4 floatval : register (c10);

sampler SourceImage = sampler_state    // Sampler for base texture
{
    Texture = (SourceImageTexture);
    ADDRESSU = MIRROR;            // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;            //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

//--------------------------------------------------------------//
// 3x3 Square Min & Max Filters
//--------------------------------------------------------------//

struct Square_VS_INPUT 
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Square_VS_OUTPUT 
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
    float2 Tex5 : TEXCOORD5;
    float2 Tex6 : TEXCOORD6;
    float2 Tex7 : TEXCOORD7;
};


Square_VS_OUTPUT SquareVertexShader( Square_VS_INPUT Input )
{
    Square_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left

    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;
    Output.Tex1 = Output.Tex0;
    Output.Tex1.x = Output.Tex0.x-1.0f/TextureWidth;   // left
    Output.Tex2 = Output.Tex0;
    Output.Tex2.x = Output.Tex0.x+1.0f/TextureWidth;   // right
    Output.Tex3 = Output.Tex0;
    Output.Tex3.y = Output.Tex0.y-1.0f/TextureHeight;  // up
    Output.Tex4 = Output.Tex0;
    Output.Tex4.y = Output.Tex0.y+1.0f/TextureHeight;  // down
    Output.Tex5 = Output.Tex1;
    Output.Tex5.y = Output.Tex3.y;                   // upper left
    Output.Tex6 = Output.Tex1;
    Output.Tex6.y = Output.Tex4.y;                   // lower left
    Output.Tex7 = Output.Tex2;
    Output.Tex7.y = Output.Tex3.y;                   // upper right

    return Output;
}

struct Square_PS_INPUT 
{
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
    float2 Tex5 : TEXCOORD5;
    float2 Tex6 : TEXCOORD6;
    float2 Tex7 : TEXCOORD7;
};


float4 MinSquarePixelShader( Square_PS_INPUT Input ) : COLOR0
{  
    float4 result, val;
    float2 Tex8;
    Tex8.x = Input.Tex0.x + 1.0f/TextureWidth;
    Tex8.y = Input.Tex0.y + 1.0f/TextureHeight;

    result = tex2D(SourceImage, Input.Tex0);

    val = tex2D(SourceImage, Input.Tex1);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex2);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex3);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex4);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex5);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex6);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex7);
    result = min(val,result);
    val = tex2D(SourceImage, Tex8);
    result = min(val,result);

    return( result );
}

float4 MaxSquarePixelShader( Square_PS_INPUT Input ) : COLOR0
{  

    float4 result, val;
    float2 Tex8;
    Tex8.x = Input.Tex0.x + 1.0f/TextureWidth;
    Tex8.y = Input.Tex0.y + 1.0f/TextureHeight;

    result = tex2D(SourceImage, Input.Tex0);

    val = tex2D(SourceImage, Input.Tex1);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex2);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex3);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex4);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex5);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex6);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex7);
    result = max(val,result);
    val = tex2D(SourceImage, Tex8);
    result = max(val,result);

    return( result );
}

//--------------------------------------------------------------//
// Technique Section for Morph
//--------------------------------------------------------------//

technique MaxSquare
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 SquareVertexShader();
        PixelShader = compile ps_2_0 MaxSquarePixelShader();
    }
}

technique MinSquare
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 SquareVertexShader();
        PixelShader = compile ps_2_0 MinSquarePixelShader();
    }
}

//texture Tex0 < string name = "D:\\Images\\ColourBars.bmp"; >;



//--------------------------------------------------------------------------------------
// Vertex shader input structure
//--------------------------------------------------------------------------------------
struct CVS_INPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 TextureUV  : TEXCOORD;   // vertex texture coords 
};

struct CVS_OUTPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 Tex0          : TEXCOORD0;  // vertex texture coords 
};

CVS_OUTPUT LinearVertexShader( CVS_INPUT Input ) 
{
    CVS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.TextureUV;

    return Output;
}



//--------------------------------------------------------------------------------------
// Vertex shader output structure
//--------------------------------------------------------------------------------------
struct CPS_OUTPUT
{
    float4 Position        : POSITION;   // vertex position 
    float2 Tex0            : TEXCOORD0;  // vertex texture coords 
};


float4 sampleOffsets[8] : register (c10);

sampler Src1Image = sampler_state    // Sampler for source 1 image
{
    Texture = (Src1ImageTexture);
    ADDRESSU = MIRROR;
    ADDRESSV = MIRROR;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler Src2Image = sampler_state    // Sampler for source 2 image
{
    Texture = (Src2ImageTexture);
    ADDRESSU = MIRROR;
    ADDRESSV = MIRROR;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler Src3Image = sampler_state    // Sampler for source 3 image
{
    Texture = (Src3ImageTexture);
    ADDRESSU = MIRROR;
    ADDRESSV = MIRROR;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

///////////////////////////////////////////////////////////////////////////////////
// Calc the mean and variance of a three by three neighbourhood
// and put the mean colour in the rgb component and the variance in the alpha
///////////////////////////////////////////////////////////////////////////////////


float4 VarianceCalc3by3( Square_VS_OUTPUT In ) : COLOR0 
{ 
	int    i; 
	float4 texSamples[9], mean, total = 0; 
	float4 variance; 
    float2 texCoords;
    	
	for (i = 0; i < 8; i++) // compute the mean 
	{ 
        texCoords = In.Tex0 + sampleOffsets[i];    // add sample offsets stored in c10-c17 (inclusive)
		
		texSamples[i] = tex2D(SourceImage, texCoords);
		total += texSamples[i]; 
	} 
	
	texSamples[8] = tex2D(SourceImage, In.Tex0);
	
	mean = total / 9.0; 
	total = 0;			 
	
	// now compute the squared variance 
	for (i = 0; i < 9 ; i++) 
	{ 
		total += (mean - texSamples[i]) * (mean - texSamples[i]); 
	} 
	
//	variance = sqrt(total); 
	variance = total/9;
		
	return variance; 
}

float4 ShiftShader( CPS_OUTPUT In ) : COLOR0
{
    float2 texCoords;
	
    texCoords = In.Tex0 + floatval;    // add sample offsets stored in c10-c17 (inclusive)
    
    return tex2D(SourceImage, texCoords);
}

float4 sobelshader( CPS_OUTPUT In ) : COLOR0
{
    int i;
    float4 c;
    float2 texCoords;
    float4 texSamples[8];
    float4 vertGradient;
    float4 horzGradient;

    for(i =0; i < 8; i++)
    {
        texCoords = In.Tex0 + sampleOffsets[i];    // add sample offsets stored in c10-c17 (inclusive)
        // take sample
        texSamples[i] = tex2D(SourceImage, texCoords); 
        
        //texSamples[i] = dot(texSamples[i], .333333f);
    }
    
    // VERTICAL Gradient
    vertGradient = -(texSamples[0] + texSamples[5] + 2*texSamples[3]);
    vertGradient += (texSamples[2] + texSamples[7] + 2*texSamples[4]);
    
    // Horizontal Gradient
    horzGradient = -(texSamples[0] + texSamples[2] + 2*texSamples[1]);
    horzGradient += (texSamples[5] + texSamples[7] + 2*texSamples[6]);

    // we could approximate by adding the abs value..but we have the horse power
    c = sqrt( horzGradient*horzGradient + vertGradient*vertGradient );
	
    return c;
}

float4 laplaceshader( CPS_OUTPUT In ) : COLOR0
{
    int i;
    float4 c;
    float2 texCoords;
	float4 v;

    c = tex2D(SourceImage, In.Tex0);
    c = 8 * c;

    for(i =0; i < 8; i++)
    {
        texCoords = In.Tex0 + sampleOffsets[i];    // add sample offsets stored in c10-c17 (inclusive)
        // take sample
        v = tex2D(SourceImage, texCoords); 
                
        c -= v;
    }
    
    return c;
}

float4 gaussianblurshader( CPS_OUTPUT In ) : COLOR0
{
    int i =0;
    float4 c = .5;
    float2 texCoords;
    float4 texSamples[8];
    float4 total=0;
	float  multipliers[8]= {1,2,1,
                            2,  2,
                            1,2,1};

    for(i =0; i < 8; i++)
    {
        texCoords = In.Tex0 + sampleOffsets[i];    // add sample offsets stored in c10-c17 (inclusive)
        // take sample, mul by by kernel
        total += tex2D(SourceImage, texCoords) * multipliers[i]; 
    }

	total += 4 * tex2D(SourceImage, In.Tex0);
	c = total/16.0;

    return c;
}

float4 maxshader( CPS_OUTPUT In ) : COLOR0
{
    int i;
    float2 texCoords;
	float4 v = 0., v1;

    for(i =0; i < 8; i++)
    {
        texCoords = In.Tex0 + sampleOffsets[i];    // add sample offsets stored in c10-c17 (inclusive)
        // take sample
        v1 = tex2D(SourceImage, texCoords); 
        
        v = max(v1, v);
	}
    
    return v;
}

float4 meanshader( CPS_OUTPUT In ) : COLOR0
{
    int i;
    float2 texCoords;
	float4 t = 0, v = 0., v1;

    for(i =0; i < 8; i++)
    {
        texCoords = In.Tex0 + sampleOffsets[i];    // add sample offsets stored in c10-c17 (inclusive)
        // take sample
        v = tex2D(SourceImage, texCoords); 
        
        t += v;
	}
    
    t += tex2D(SourceImage, In.Tex0);
    
    v = t / 9;
    
    return v;
}

// Combine all colour components to produce an intensity image
float4 intensityshader( CPS_OUTPUT In ) : COLOR0
{
	float v;
	float4 t;
	
	t = tex2D(SourceImage, In.Tex0);
	v = sqrt(t.r * t.r + t.g * t.g + t.b * t.b);
	t.r = v;
	t.g = v;
	t.b = v;
	t.a = 0;
    
    return v;
}

float4 minshader( CPS_OUTPUT In ) : COLOR0
{
    int i;
    float2 texCoords;
	float4 v = 0., v1;

    for(i =0; i < 8; i++)
    {
        texCoords = In.Tex0 + sampleOffsets[i];    // add sample offsets stored in c10-c17 (inclusive)
        // take sample
        v1 = tex2D(SourceImage, texCoords); 
        
        v = max(v1, v);
	}
    
    return v;
}


float4 diagcrossminshader( CPS_OUTPUT In) : COLOR0
{
	float4 v1;
	float4 v2;
	
	v1 = tex2D(SourceImage, In.Tex0 + sampleOffsets[0]);		// Upper left
	v2 = tex2D(SourceImage, In.Tex0 + sampleOffsets[2]);		// Upper right
	v1 = min(v1, v2);
	v2 = tex2D(SourceImage, In.Tex0 + sampleOffsets[5]);		// Bottom left
	v1 = min(v1, v2);
	v2 = tex2D(SourceImage, In.Tex0 + sampleOffsets[7]);		// Bottom right
	v1 = min(v1, v2);
	v2 = tex2D(SourceImage, In.Tex0);							// Centre
	v1 = min(v1, v2);
	
	return v1;	
}

float4 crossminshader( CPS_OUTPUT In) : COLOR0
{
	float4 v1;
	float4 v2;
	
	v1 = tex2D(SourceImage, In.Tex0 + sampleOffsets[1]);		// Middle Top
	v2 = tex2D(SourceImage, In.Tex0 + sampleOffsets[3]);		// Left
	v1 = min(v1, v2);
	v2 = tex2D(SourceImage, In.Tex0 + sampleOffsets[4]);		// Right
	v1 = min(v1, v2);
	v2 = tex2D(SourceImage, In.Tex0 + sampleOffsets[6]);		// Middle Bottom
	v1 = min(v1, v2);
	v2 = tex2D(SourceImage, In.Tex0);							// Centre
	v1 = min(v1, v2);
	
	return v1;	
}

float4 diagcrossmaxshader( CPS_OUTPUT In) : COLOR0
{
	float4 v1;
	float4 v2;
	
	v1 = tex2D(SourceImage, In.Tex0 + sampleOffsets[0]);		// Upper left
	v2 = tex2D(SourceImage, In.Tex0 + sampleOffsets[2]);		// Upper right
	v1 = max(v1, v2);
	v2 = tex2D(SourceImage, In.Tex0 + sampleOffsets[5]);		// Bottom left
	v1 = max(v1, v2);
	v2 = tex2D(SourceImage, In.Tex0 + sampleOffsets[7]);		// Bottom right
	v1 = max(v1, v2);
	v2 = tex2D(SourceImage, In.Tex0);							// Centre
	v1 = max(v1, v2);
	
	return v1;	
}

float4 crossmaxshader( CPS_OUTPUT In) : COLOR0
{
	float4 v1;
	float4 v2;
	
	v1 = tex2D(SourceImage, In.Tex0 + sampleOffsets[1]);		// Middle Top
	v2 = tex2D(SourceImage, In.Tex0 + sampleOffsets[3]);		// Left
	v1 = max(v1, v2);
	v2 = tex2D(SourceImage, In.Tex0 + sampleOffsets[4]);		// Right
	v1 = max(v1, v2);
	v2 = tex2D(SourceImage, In.Tex0 + sampleOffsets[6]);		// Middle Bottom
	v1 = max(v1, v2);
	v2 = tex2D(SourceImage, In.Tex0);							// Centre
	v1 = max(v1, v2);
	
	return v1;	
}

float4 copyimageshader( CPS_OUTPUT In) : COLOR0
{
	float4 v;
	
	v = tex2D(SourceImage, In.Tex0);
	return v;
}


float4 InvertShader( CPS_OUTPUT In) : COLOR0
{
	float4 v;
	
	v = tex2D(SourceImage, In.Tex0);
	return 1.0 - v;
}

// Subtract the minimum value of two images 
// Src1Image is the original image, Src2Image is the other
float4 subtractminshader( CPS_OUTPUT In) : COLOR0
{
	float4 v1, v2, vmin;
		
	v1 = tex2D(Src1Image, In.Tex0);	
	v2 = tex2D(Src2Image, In.Tex0);
	
	vmin = min(v1, v2);
	
	v1 -= vmin;
	
	return v1;	
}

// Subtract the minimum value of two images 
// Src1Image is the original image, Src2Image is the other
float4 subtractmaxshader( CPS_OUTPUT In) : COLOR0
{
	float4 v1, v2, vmax;
		
	v1 = tex2D(Src1Image, In.Tex0);	
	v2 = tex2D(Src2Image, In.Tex0);
	
	vmax = max(v1, v2);
	
	v1 -= vmax;
	
	return v1;	
}

///////////////////////////////////////////////////////////////////
// Techniques
///////////////////////////////////////////////////////////////////
technique Sobel
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 sobelshader();
    }
}

technique Laplace
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 laplaceshader();
    }
}

technique Gaussianblur
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 gaussianblurshader();
    }
}

technique Maxcolour
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 maxshader();
    }
}

technique Meancolour
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 meanshader();
    }
}

technique Mincolour
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 minshader();
    }
}

technique Diagmincross
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 diagcrossminshader();
    }
}

technique Mincross
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 crossminshader();
    }
}

technique Diagmaxcross
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 diagcrossmaxshader();
    }
}

technique Maxcross
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 crossmaxshader();
    }
}

technique Intensity
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 intensityshader();
    }
}


technique Copyimage
{
	pass P0
	{
		CULLMODE = NONE;
		ZENABLE = FALSE;
		
		VertexShader = compile vs_2_0 LinearVertexShader();
		PixelShader = compile ps_2_0 copyimageshader();
	}
}

technique InvertImage
{
	pass P0
	{
		CULLMODE = NONE;
		ZENABLE = FALSE;
		
		VertexShader = compile vs_2_0 LinearVertexShader();
		PixelShader = compile ps_2_0 InvertShader();
	}
}

technique Subtractmin
{
	pass P0
	{
		CULLMODE = NONE;
		ZENABLE = FALSE;
		
		VertexShader = compile vs_2_0 LinearVertexShader();
		PixelShader = compile ps_2_0 subtractminshader();
	}
}

technique Subtractmax
{
	pass P0
	{
		CULLMODE = NONE;
		ZENABLE = FALSE;
		
		VertexShader = compile vs_2_0 LinearVertexShader();
		PixelShader = compile ps_2_0 subtractmaxshader();
	}
}


technique Variance3By3
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 VarianceCalc3by3();
    }
}

technique ShiftImage
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_2_0 LinearVertexShader();
        PixelShader = compile ps_2_0 ShiftShader();
    }
}

//--------------------------------------------------------------//
// Straight transfer of image - to move render target texture to backbuffer
//--------------------------------------------------------------//

struct Show_VS_INPUT       
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Show_VS_OUTPUT    
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
};

struct Show_PS_INPUT 
{
    float2 Tex0 : TEXCOORD0;
};

Show_VS_OUTPUT ShowVertexShader( Show_VS_INPUT Input )
{
    Show_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     Output.Position.y = -Input.Position.y;    // because textures have inverted coordinates 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;

    return Output;
}

float4 ShowPixelShader( Show_PS_INPUT Input ) : COLOR0  
{  
    return ( tex2D(SourceImage, Input.Tex0));
}

technique Show
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 ShowVertexShader();
        PixelShader = compile ps_2_0 ShowPixelShader();
    }
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// LBP vertex and pixel shaders for texture analysis
// combine with texture LUT to get uniform version (only 9 different output values )
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

float LBPradius;				// global LBP radius - passed in by calling function
								// typically 1.0f, 2.0f, image should be gaussian filtered 
								// for larger radii before calling LBP
								// formally radius should be 1 for LBP8,1, 2 for LBP8,2 etc.
float LBPnoiseThreshold;		// to prevent LBP piccking up noise variation typical values are 16, 24, 32
texture LBPLutTexture;			// 1D 32bit texture containing ARGB output LUTs

sampler1D lbpLut = sampler_state    // Sampler for image LUTs
{
    Texture = (LBPLutTexture);
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
    AddressU = CLAMP;
    AddressV = CLAMP;
};

Square_VS_OUTPUT LBPVertexShader( Square_VS_INPUT Input )
{
    Square_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left

    // Matching texture coords in range 0 to 1
    // 2 3 4
	// 1 0 5
	// 8 7 6
	//
	//	Tex8 must be generated by pixel shader

    Output.Tex0 = Input.Tex;
    Output.Tex1 = Output.Tex0;
    Output.Tex1.x = Output.Tex0.x-LBPradius/TextureWidth;	// left
    Output.Tex5 = Output.Tex0;
    Output.Tex5.x = Output.Tex0.x+LBPradius/TextureWidth;	// right
    Output.Tex3 = Output.Tex0;
    Output.Tex3.y = Output.Tex0.y-LBPradius/TextureHeight;	// up
    Output.Tex7 = Output.Tex0;
    Output.Tex7.y = Output.Tex0.y+LBPradius/TextureHeight;	// down
    Output.Tex2 = Output.Tex1;
    Output.Tex2.y = Output.Tex3.y;						// upper left
    Output.Tex6 = Output.Tex5;
    Output.Tex6.y = Output.Tex7.y;						// lower right
    Output.Tex4 = Output.Tex5;
    Output.Tex4.y = Output.Tex3.y;						// upper right

    return Output;
}

float4 LBPPixelShader( Square_PS_INPUT Input ) : COLOR0
{  
    float4 result, val, centre, temp;
    float2 Tex8;
    
    Tex8.x = Input.Tex0.x - LBPradius/TextureWidth;	// lower left
    Tex8.y = Input.Tex0.y + LBPradius/TextureHeight;

	// get centre pixel value
    centre = tex2D(SourceImage, Input.Tex0) + LBPnoiseThreshold/256.0f;	// want to get 1 if val >= centre + t, 0 otherwise; typically  t = 24

	// each nhood pixel val converted into a bit field val>centre:1?0
	result = 0.0f;
    val = tex2D(SourceImage, Input.Tex1);
    result += step(centre,val);				// step returns 1 if val > centre, otherwise 0
    val = tex2D(SourceImage, Input.Tex2);
    result += 2.0f * step(centre,val);
    val = tex2D(SourceImage, Input.Tex3);
    result += 4.0f * step(centre,val);
    val = tex2D(SourceImage, Input.Tex4);
    result += 8.0f * step(centre,val);
    val = tex2D(SourceImage, Input.Tex5);
    result += 16.0f * step(centre,val);
    val = tex2D(SourceImage, Input.Tex6);
    result += 32.0f * step(centre,val);
    val = tex2D(SourceImage, Input.Tex7);
    result += 64.0f * step(centre,val);
    val = tex2D(SourceImage, Tex8);
    result += 128.0f * step(centre,val);
    
    // convert to 0.0 - 1.0
	val = result / 255.0f;
	
	// use LUT to get uniform version
	// note we are dealing with 4 quads simultaneously with different LUT indices
	// so we have to make 4 seperate LUT samples
	temp = tex1D(lbpLut, (val.r*255+0.5)/256);	// must add half a pixel to get centre of lut texture value)
	result.r = temp.r;
	temp = tex1D(lbpLut, (val.g*255+0.5)/256);	
	result.g = temp.g;
	temp = tex1D(lbpLut, (val.b*255+0.5)/256);	
	result.b = temp.b;
	temp = tex1D(lbpLut, (val.a*255+0.5)/256);	
	result.a = temp.a;
	
	// result will now be in the range 0-8 inclusive
    return( result );
}

//--------------------------------------------------------------//
// Texture Effects
//--------------------------------------------------------------//

technique LBP
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 LBPVertexShader();
        PixelShader = compile ps_2_0 LBPPixelShader();
    }
}


//--------------------------------------------------------------//
