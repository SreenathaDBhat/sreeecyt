//--------------------------------------------------------------//
// Math Effects
//--------------------------------------------------------------//

// Globals
texture Src1ImageTexture;		// Base texture to load input image  
texture Src2ImageTexture;		// Base texture to load input image  

float	ConstValue;
float	OffsetX;
float	OffsetY;
float	ScaleX;
float	ScaleY;

sampler Src1 = sampler_state    // Sampler for source1 image
{
    Texture = (Src1ImageTexture);
    ADDRESSU = MIRROR;
    ADDRESSV = MIRROR;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler Src2 = sampler_state    // Sampler for source2 image
{
    Texture = (Src2ImageTexture);
    ADDRESSU = MIRROR;
    ADDRESSV = MIRROR;
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

//--------------------------------------------------------------//
// Mathematical filters Output = Src1 OP Src2    e.g. Output = Src1 - Src2
//--------------------------------------------------------------//

struct Math_VS_INPUT        
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Math_VS_OUTPUT       
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
};

Math_VS_OUTPUT MathVertexShader( Math_VS_INPUT Input )
{
    Math_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     Output.Position.y = -Input.Position.y;    // because textures have inverted coordinates 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;

    return Output;
}

//--------------------------------------------------------------//

struct Math_PS_INPUT 
{
    float2 Tex0 : TEXCOORD0;
};

//--------------------------------------------------------------//

float4 MathSubPixelShader( Math_PS_INPUT Input ) : COLOR0    
{  
    return ( clamp(tex2D(Src1, Input.Tex0) - tex2D(Src2,Input.Tex0), 0.0, 1.0) );
}
//--------------------------------------------------------------//

float4 MathAddPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
    return ( clamp(tex2D(Src1, Input.Tex0) + tex2D(Src2,Input.Tex0), 0.0, 1.0) );
}

float4 MathAddAndAveragePixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
    return ( clamp(0.5f * (tex2D(Src1, Input.Tex0) + tex2D(Src2,Input.Tex0)), 0.0, 1.0) );
}

//--------------------------------------------------------------//

float4 MathMulPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
    return tex2D(Src1, Input.Tex0) * tex2D(Src2,Input.Tex0);
}
//--------------------------------------------------------------//

float4 MathDivPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
    return ( clamp(tex2D(Src1, Input.Tex0) / tex2D(Src2,Input.Tex0), 0.0, 1.0) );
}
//--------------------------------------------------------------//

float4 MathDiffPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
    return ( abs(tex2D(Src1, Input.Tex0) - tex2D(Src2,Input.Tex0)) );
}
//--------------------------------------------------------------//
float4 MathSubConstPixelShader( Math_PS_INPUT Input ) : COLOR0    
{  
    return ( clamp(tex2D(Src1, Input.Tex0) - ConstValue, 0.0, 1.0) );
}
//--------------------------------------------------------------//

float4 MathAddConstPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
    return ( clamp(tex2D(Src1, Input.Tex0) + ConstValue, 0.0, 1.0) );
}
//--------------------------------------------------------------//

float4 MathMulConstPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
    return ( clamp(tex2D(Src1, Input.Tex0) * ConstValue, 0.0, 1.0) );
}
//--------------------------------------------------------------//

float4 MathDivConstPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
    return ( clamp(tex2D(Src1, Input.Tex0) / ConstValue, 0.0, 1.0) );
}
//--------------------------------------------------------------//

float4 MathModPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
	float4 P1, P2;
	
	P1 = tex2D(Src1, Input.Tex0);
	P2 = tex2D(Src2, Input.Tex0);
    return sqrt(P1 * P1 + P2 * P2);
}

//--------------------------------------------------------------//

float4 MathMaxPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
	float4 P1, P2, R;
	
	P1 = tex2D(Src1, Input.Tex0);
	P2 = tex2D(Src2, Input.Tex0);
    
    if (P1.r > P2.r)
		R.r = P1.r;
	else
		R.r = P2.r;
    if (P1.g > P2.g)
		R.g = P1.g;
	else
		R.g = P2.g;
    if (P1.b > P2.b)
		R.b = P1.b;
	else
		R.b = P2.b;
    if (P1.a > P2.a)
		R.a = P1.a;
	else
		R.a = P2.a;
  
    return R;
}

//--------------------------------------------------------------//

float4 MathMinPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
	float4 P1, P2, R;
	
	P1 = tex2D(Src1, Input.Tex0);
	P2 = tex2D(Src2, Input.Tex0);
    
    if (P1.r < P2.r)
		R.r = P1.r;
	else
		R.r = P2.r;
    if (P1.g < P2.g)
		R.g = P1.g;
	else
		R.g = P2.g;
    if (P1.b < P2.b)
		R.b = P1.b;
	else
		R.b = P2.b;
    if (P1.a < P2.a)
		R.a = P1.a;
	else
		R.a = P2.a;
  
    return R;
}
//--------------------------------------------------------------//

float4 MathCopyPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
    return (tex2D(Src1, Input.Tex0));
}

float4 MathCropPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
	float2 Tex;
	Tex.x=Input.Tex0.x*ScaleX+OffsetX;
	Tex.y=Input.Tex0.y*ScaleY+OffsetY;
	return (tex2D(Src1, Tex));
}

float4 MathCropGreyPixelShader( Math_PS_INPUT Input ) : COLOR0   
{  
	float2 Tex;
	Tex.x=Input.Tex0.x*ScaleX+OffsetX;
	Tex.y=Input.Tex0.y*ScaleY+OffsetY;
	return (tex2D(Src1, Tex));
}

//--------------------------------------------------------------//
// Technique Section for Math
//--------------------------------------------------------------//

technique MathSub
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathSubPixelShader();
    }
}

technique MathAdd
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathAddPixelShader();
    }
}

technique MathAddAndAverage
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathAddAndAveragePixelShader();
    }
}

technique MathMul
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathMulPixelShader();
    }
}
technique MathDiv
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathDivPixelShader();
    }
}
technique MathDiff
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathDiffPixelShader();
    }
}
technique MathMod
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathModPixelShader();
    }
}
technique MathSubConst
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathSubConstPixelShader();
    }
}
technique MathMax
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathMaxPixelShader();
    }
}

technique MathMin
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathMinPixelShader();
    }
}

technique MathAddConst
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathAddConstPixelShader();
    }
}
technique MathMulConst
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathMulConstPixelShader();
    }
}
technique MathDivConst
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathDivConstPixelShader();
    }
}

technique MathCopy
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathCopyPixelShader();
    }
}

technique CropImage
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathCropPixelShader();
    }
}
technique CropGreyImage
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 MathVertexShader();
        PixelShader = compile ps_2_0 MathCropGreyPixelShader();
    }
}
