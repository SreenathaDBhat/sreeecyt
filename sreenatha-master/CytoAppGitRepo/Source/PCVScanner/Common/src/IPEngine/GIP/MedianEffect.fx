//--------------------------------------------------------------//
//	MedianFilter Effect
//--------------------------------------------------------------//

// Globals
float OffsetX;					// offset for neighbours during distance determination
float OffsetY;
texture SourceImageTexture;     // Base texture to load input image  
                                // Output image will go to current render target (backbuffer by default)
                                // Calling routines can feed output image back into SourceImageTexture to perform multipass dilations etc.

sampler SourceImage = sampler_state    // Sampler for base texture
{
    Texture = (SourceImageTexture);
    ADDRESSU = MIRROR;            // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;            //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

struct Square_VS_INPUT 
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Square_VS_OUTPUT 
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
    float2 Tex5 : TEXCOORD5;
    float2 Tex6 : TEXCOORD6;
    float2 Tex7 : TEXCOORD7;
    float2 Tex8 : TEXCOORD8;
};

Square_VS_OUTPUT SquareVertexShader( Square_VS_INPUT Input )
{
    Square_VS_OUTPUT Output;
	
    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;

    // Matching texture coords in range 0 to 1
	Output.Tex0.x = Input.Tex.x - OffsetX;		// Upper left
	Output.Tex0.y = Input.Tex.y - OffsetY;
	Output.Tex1.x = Input.Tex.x;				// Up
	Output.Tex1.y = Output.Tex0.y;
	Output.Tex2.x = Input.Tex.x + OffsetX;		// Upper right
	Output.Tex2.y = Output.Tex0.y;
	Output.Tex3.x = Output.Tex0.x;				// Left
	Output.Tex3.y = Input.Tex.y;
	Output.Tex4   = Input.Tex;					// Centre
	Output.Tex5.x = Output.Tex2.x;				// Right
	Output.Tex5.y = Input.Tex.y;
	Output.Tex6.x = Output.Tex0.x;				// Lower left
	Output.Tex6.y = Input.Tex.y + OffsetY;
	Output.Tex7.x = Input.Tex.x;				// Down
	Output.Tex7.y = Output.Tex6.y;				
	Output.Tex8.x = Output.Tex2.x;				// Down right
	Output.Tex8.y = Output.Tex6.y;
    return Output;
}

// sorts two colors in place, putting smallest in a
void sort2(inout float4 a, inout float4 b)
{
    float4 cmin;
    float4 cmax; 
    
	cmin = min(a, b);
	cmax = max(a, b);
					
    a = cmin;
    b = cmax;
}

float4 medianshader( Square_VS_OUTPUT In) : COLOR0
{
	float4 c[9];
	int i;
	float2 texCoords;

    c[0] = tex2D(SourceImage, In.Tex0); 
    c[1] = tex2D(SourceImage, In.Tex1); 
    c[2] = tex2D(SourceImage, In.Tex2); 
    c[3] = tex2D(SourceImage, In.Tex3); 
    c[4] = tex2D(SourceImage, In.Tex4); 
    c[5] = tex2D(SourceImage, In.Tex5); 
    c[6] = tex2D(SourceImage, In.Tex6); 
    c[7] = tex2D(SourceImage, In.Tex7); 
	c[8] = tex2D(SourceImage, In.Tex8);
	
    // 0-1
    sort2(c[0], c[1]);
    
    // 0-2
    sort2(c[1], c[2]);
    sort2(c[0], c[1]);
    
    // 0-3
    sort2(c[2], c[3]);
    sort2(c[1], c[2]);
    sort2(c[0], c[1]);
    
    // 0-4
    sort2(c[3], c[4]);
    sort2(c[2], c[3]);
    sort2(c[1], c[2]);
    sort2(c[0], c[1]);

    // 0-5    
    sort2(c[4], c[5]);
    sort2(c[3], c[4]);
    sort2(c[2], c[3]);
    sort2(c[1], c[2]);
    sort2(c[0], c[1]);

    // 0-6
    sort2(c[5], c[6]);    
    sort2(c[4], c[5]);
    sort2(c[3], c[4]);
    sort2(c[2], c[3]);
    sort2(c[1], c[2]);
    sort2(c[0], c[1]);

    // 0-7
    sort2(c[6], c[7]);
    sort2(c[5], c[6]);    
    sort2(c[4], c[5]);
    sort2(c[3], c[4]);
    sort2(c[2], c[3]);
    sort2(c[1], c[2]);
    sort2(c[0], c[1]);

    // 0-8
    sort2(c[7], c[8]);    
    sort2(c[6], c[7]);
    sort2(c[5], c[6]);    
    sort2(c[4], c[5]);
    sort2(c[3], c[4]);
    sort2(c[2], c[3]);
    sort2(c[1], c[2]);
    sort2(c[0], c[1]); 
   
    return c[4];	
}

///////////////////////////////////////////////////////////////////
// Techniques
///////////////////////////////////////////////////////////////////

technique median
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 SquareVertexShader();
        PixelShader = compile ps_3_0 medianshader();
    }
}
