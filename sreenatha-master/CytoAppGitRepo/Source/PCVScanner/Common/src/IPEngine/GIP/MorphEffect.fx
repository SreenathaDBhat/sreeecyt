//--------------------------------------------------------------//
//  MorphEffect File
//--------------------------------------------------------------//
//    Calling program sets up simple quad based on 4 vertices with 
//    with processed display position -1 to +1
//    and matching texture ranges         0 to  1
//
//    CUSTOMVERTEX vertices[] =
//    {
//        // x      Y     Z    U  V
//        {-1.0f, -1.0f, 0.0f, 0, 1},    //lower left
//        {+1.0f, -1.0f, 0.0f, 1, 1}, //lower right
//        {+1.0f, +1.0f, 0.0f, 1, 0}, //upper right
//        {-1.0f, +1.0f, 0.0f, 0, 0}    //upper left
//    };
//
//    After loading into the vertex buffer the quad is then drawn 
//	  using DrawPrimitive (D3DPT_TRIANGLEFAN, 0, 2);
//
//    Because the vertices have effectively been processed
//    there is no need for a vertex shader unless additional
//    texture coordinates are generated, e.g. in morphology 
//    or NxN filters
//
float TextureWidth;					// Width of input texture - power of 2, so may be bigger than image
float TextureHeight;				// Height of input texture - power of 2, so may be bigger than image
float DilateThresh;
texture SourceImageTexture;			// Base texture to load input image  
texture Image2Texture;
									// Output image will go to current render target (backbuffer by default)
									// Calling routines can feed output image back into SourceImageTexture to perform multipass dilations etc.
sampler SourceImage = sampler_state // Sampler for base texture
{
    Texture = (SourceImageTexture);
    ADDRESSU = MIRROR;				// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;				//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler Source2Image = sampler_state // Sampler for base texture
{
    Texture = (Image2Texture);
    ADDRESSU = MIRROR;				// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;				//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

struct Cross_VS_INPUT				// used for both axial and diagonal cross filters
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Cross_VS_OUTPUT				// used for both axial and diagonal cross filters
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
};

Cross_VS_OUTPUT AxialCrossVertexShader( Cross_VS_INPUT Input )
{
    Cross_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;							// Centre
    Output.Tex1.y = Output.Tex0.y;
    Output.Tex1.x = Output.Tex0.x-1.0f/TextureWidth;   // left
    Output.Tex2.y = Output.Tex0.y;
    Output.Tex2.x = Output.Tex0.x+1.0f/TextureWidth;   // right
    Output.Tex3.x = Output.Tex0.x;
    Output.Tex3.y = Output.Tex0.y-1.0f/TextureHeight;  // up
    Output.Tex4.x = Output.Tex0.x;
    Output.Tex4.y = Output.Tex0.y+1.0f/TextureHeight;  // down

    return Output;
}


Cross_VS_OUTPUT DiagonalCrossVertexShader( Cross_VS_INPUT Input )
{
    Cross_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
 
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;								// Centre
    Output.Tex1.x = Output.Tex0.x-1.0f/TextureWidth;        
    Output.Tex1.y = Output.Tex0.y-1.0f/TextureHeight;		// upper left
    Output.Tex2.x = Output.Tex0.x+1.0f/TextureWidth;        
    Output.Tex2.y = Output.Tex1.y;							// upper right
    Output.Tex3.x = Output.Tex1.x;							
    Output.Tex3.y = Output.Tex0.y+1.0f/TextureHeight;       // lower left
    Output.Tex4.x = Output.Tex2.x;                        
    Output.Tex4.y = Output.Tex3.y;							// lower right


    return Output;
}


struct Cross_PS_INPUT    // used for both axial and diagonal cross filters
{
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
};


float4 MinCrossPixelShader( Cross_PS_INPUT Input ) : COLOR0    // used for both axial and diagonal cross filters
{  
    float4 result, val;

    result = tex2D(SourceImage, Input.Tex0);

    val = tex2D(SourceImage, Input.Tex1);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex2);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex3);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex4);
    result = min(val,result);

    return( result );   
}

float4 MaxCrossPixelShader( Cross_PS_INPUT Input ) : COLOR0    // used for both axial and diagonal cross filters
{  
   float4 result, val;
   
   result = tex2D(SourceImage, Input.Tex0);

   val = tex2D(SourceImage, Input.Tex1);
   result = max(val,result);
   val = tex2D(SourceImage, Input.Tex2);
   result = max(val,result);
   val = tex2D(SourceImage, Input.Tex3);
   result = max(val,result);
   val = tex2D(SourceImage, Input.Tex4);
   result = max(val,result);

   return( result );
}


float4 DiffBelowThresh(float4 s, float4 val)
{
	float4 result;

	result = 0.0;
	if (val.r > DilateThresh && s.r > 0.0)
		result.r = 1.0;

   if (val.g > DilateThresh && s.g > 0.0)
		result.g = 1.0;

   if (val.b > DilateThresh && s.b > 0.0)
		result.b = 1.0;

   if (val.a > DilateThresh && s.a > 0.0)
		result.a = 1.0;

	return result;
}

float4 DiffBelowThresh1(float4 s, float4 val)
{
	float4 result;

	result = 0.0;
	if (val.r > DilateThresh && s.r > 0.0)
		result.r = val.r;

   if (val.g > DilateThresh && s.g > 0.0)
		result.g = val.g;

   if (val.b > DilateThresh && s.b > 0.0)
		result.b = val.b;

   if (val.a > DilateThresh && s.a > 0.0)
		result.a = val.a;

	return result;
}

float4 MaxCrossThresholdPixelShader( Cross_PS_INPUT Input ) : COLOR0    // used for both axial and diagonal cross filters
{  
   float4 p1, p2, p3, p4, p5, val, r1, r2, r3, r4, s1, s2, s3, s4, s5, result;
   
   s1 = tex2D(SourceImage, Input.Tex0);
   s2 = tex2D(SourceImage, Input.Tex1);
   s3 = tex2D(SourceImage, Input.Tex2);
   s4 = tex2D(SourceImage, Input.Tex3);
   s5 = tex2D(SourceImage, Input.Tex4);

   p1 = tex2D(Source2Image, Input.Tex0);
   p2 = abs(tex2D(Source2Image, Input.Tex1) - p1);
   p3 = abs(tex2D(Source2Image, Input.Tex2) - p1);
   p4 = abs(tex2D(Source2Image, Input.Tex3) - p1);
   p5 = abs(tex2D(Source2Image, Input.Tex4) - p1);

   result = 0.0;

   r1 = DiffBelowThresh(s2, p2);
   r2 = DiffBelowThresh(s3, p3);
   r3 = DiffBelowThresh(s4, p4);
   r4 = DiffBelowThresh(s5, p5);

   result = max(r1, r2);
   result = max(r3, result);
   result = max(r4, result);

   return ( result );
}

//--------------------------------------------------------------//
// Technique Section for Morph
//--------------------------------------------------------------//

technique MaxAxialCross
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 AxialCrossVertexShader();
        PixelShader = compile ps_2_0 MaxCrossPixelShader();
    }
}

technique MaxDiagonalCross
{
   pass P0
   {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 DiagonalCrossVertexShader();
        PixelShader = compile ps_2_0 MaxCrossPixelShader();
   }
}

technique MinAxialCross
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 AxialCrossVertexShader();
        PixelShader = compile ps_2_0 MinCrossPixelShader();
    }
}

technique MinDiagonalCross
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 DiagonalCrossVertexShader();
        PixelShader = compile ps_2_0 MinCrossPixelShader();
    }
}

technique MaxThreshAxialCross
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 AxialCrossVertexShader();
        PixelShader = compile ps_2_0 MaxCrossThresholdPixelShader();
    }
}

technique MaxThreshDiagonalCross
{
   pass P0
   {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 DiagonalCrossVertexShader();
        PixelShader = compile ps_2_0 MaxCrossThresholdPixelShader();
   }
}