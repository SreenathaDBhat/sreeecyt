//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GIP.rc
//
#define IDR_FXFILE                      1005
#define IDR_RT_RCDATA1                  1008

#define IDR_IMAGEPROCESSINGFX           1008
#define IDR_MORPHFX                     1009
#define IDR_BLENDFX                     1010
#define IDR_COLOURCONVFX                1011
#define IDR_DECONVFX                    1012
#define IDR_COMPONENTFX                 1013
#define IDR_EDMFX                       1014
#define IDR_CONVOLVEFX                  1015
#define IDR_FFTFX                       1016
#define IDR_GAUSSFX                     1017
#define IDR_HOUGHFX                     1018
#define IDR_KUWAHARAFX                  1019
#define IDR_MATHFX                      1020
#define IDR_MEDIANFX                    1021
#define IDR_THRESHOLDFX                 1022

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1100
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           1000
#endif
#endif
