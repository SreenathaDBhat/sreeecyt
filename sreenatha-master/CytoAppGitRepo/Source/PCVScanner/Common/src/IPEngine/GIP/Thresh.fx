//--------------------------------------------------------------//
// Thresholding routines
//--------------------------------------------------------------//
struct Thresh_VS_INPUT 
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Thresh_VS_OUTPUT      
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
};

Thresh_VS_OUTPUT ThreshVertexShader( Thresh_VS_INPUT Input )
{
    Thresh_VS_OUTPUT Output;

    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;
    Output.Tex1 = Output.Tex0;
    Output.Tex1.x = Output.Tex0.x-1.0f/TextureWidth;   // left
    Output.Tex2 = Output.Tex0;
    Output.Tex2.x = Output.Tex0.x+1.0f/TextureWidth;   // right
    Output.Tex3 = Output.Tex0;
    Output.Tex3.y = Output.Tex0.y-1.0f/TextureHeight;  // up
    Output.Tex4 = Output.Tex0;
    Output.Tex4.y = Output.Tex0.y+1.0f/TextureHeight;  // down

	// Threshold SE and SG are maintained in 1 pixel texture (can hold 4 floats)
    // Clamp the position to 0,0
    Output.Position = 0;
	
    return Output;
}



struct Thresh_PS_INPUT    // used for both axial and diagonal cross filters
{
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
    float2 ThreshTex : TEXCOORD5;
};

float4 ThreshPixelShader( Thresh_PS_INPUT Input ) : COLOR0    // used for both axial and diagonal cross filters
{  
    float4 mask, modemask, gradmask, val, cval, grad, fgrad, texval;

    // get centre pixel value
    cval = tex2D(SourceImage, Input.Tex0);
/*
    // ignore dark areas inside objects and background (below mode)
    // if ((val < 0) || (val > valmax)) then ignore this pixel
    // must check each rgba component because each contain a grey value
    // and continue with the operation if any pass the test
    // to remove unwanted components later we generate a mask
    modemask = clamp(cval - mode, 1,0);            // if value is < mode the mask will be 0
    modemask = sign(modemask);                    // if value is > 0 the mask will be 1
    mask = clamp(valmax-cval, 1,0);                // if value is > valmax the mask will be 0
    mask = sign(mask);                            // if value is > 0 the mask will be 1
    mask *= modemask;                            // composite mask
    
    // if all mask components are 0 then nothing to do
    if (any(mask) == false)
        return cval;
*/
    val = tex2D(SourceImage, Input.Tex1);        //left
    fgrad = abs(cval - val);
        
    val = tex2D(SourceImage, Input.Tex2);        //right
    grad = abs(cval - val);
    fgrad = max(grad, fgrad); 
    
    val = tex2D(SourceImage, Input.Tex3);        //above
    grad = abs(cval - val);
    fgrad = max(grad, fgrad); 

    val = tex2D(SourceImage, Input.Tex4);        //below
    grad = abs(cval - val);
    fgrad = max(grad, fgrad); 

    // ignore small gradients i.e less than 8
    // by clamping result of subtraction,  8/256 = 0.03125
    gradmask = clamp(fgrad - 0.03125, 0.0, 1.0);
    gradmask = sign(gradmask);
//    mask*=gradmask;
    
    // apply mask to remove unwanted components
//    fgrad *= mask;
//    fgrad *= gradmask;
    
    // sum max gradients
//    SE+=fgrad;
 //   SG+=cval*fgrad;
	texval=0;
    texval.r += fgrad.b+fgrad.r+fgrad.g+fgrad.a;	// SE
    
    val = dot(cval,fgrad);
    texval.g = val.b+val.r+val.g+val.a;				// SG

    // output only update single pixel texval
    return texval;   
}


technique Thresh
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;
        ALPHABLENDENABLE = TRUE;
        SRCBLEND = ONE;
        DESTBLEND = ONE;

        VertexShader = compile vs_2_0 ThreshVertexShader();
        PixelShader = compile ps_2_0 ThreshPixelShader();
    }
}

