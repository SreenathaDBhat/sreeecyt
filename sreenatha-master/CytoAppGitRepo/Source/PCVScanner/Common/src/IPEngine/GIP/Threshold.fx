//--------------------------------------------------------------//
// ColourConvcersion Effects
//--------------------------------------------------------------//

texture SourceImageTexture;        // Base texture to load input image  
                                // Output image will go to current render target (backbuffer by default)
                                // Calling routines can feed output image back into SourceImageTexture to perform multipass dilations etc.

sampler SourceImage = sampler_state    // Sampler for base texture
{
    Texture = (SourceImageTexture);
    ADDRESSU = MIRROR;            // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;            //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};


struct Thresh_VS_INPUT        
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Thresh_VS_OUTPUT       
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
};

Thresh_VS_OUTPUT ThreshVertexShader( Thresh_VS_INPUT Input )
{
    Thresh_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     Output.Position.y = -Input.Position.y;    // because textures have inverted coordinates 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;

    return Output;
}

//--------------------------------------------------------------//

struct Thresh_PS_INPUT 
{
    float2 Tex0 : TEXCOORD0;
};


//--------------------------------------------------------------//
//--------------------------------------------------------------//
float threshold;

float4 ThreshShader( Thresh_PS_INPUT Input ) : COLOR0    
{  
    return  (sign(tex2D(SourceImage, Input.Tex0) - threshold));
}

//--------------------------------------------------------------//
technique Thresh
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 ThreshVertexShader();
        PixelShader = compile ps_2_0 ThreshShader();
    }
}
