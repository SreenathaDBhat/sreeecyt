///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Generic convolution HLSL file
// Written By Karl Ratcliff
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

// Globals
float TextureWidth;                     // Width of input texture 
float TextureHeight;					// Height of input texture 
texture SourceImageTexture;             // Base texture to load input image
float4 sampleCoeff[9];					// Filter coefficients
texture CoefficientsTexture;			// Stores the filter coefficients 
int	Size;							    // The filter dimension
int SqSize;                             // Squared size

//--------------------------------------------------------------------------------------
// Source and coefficients image samplers
//--------------------------------------------------------------------------------------

sampler SourceImage = sampler_state		// Sampler for base texture
{
    Texture = (SourceImageTexture);
    ADDRESSU = MIRROR;            // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;            //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler CoeffImage = sampler_state		// Sampler for base texture
{
    Texture = (CoefficientsTexture);
    ADDRESSU = MIRROR;            // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;            //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

//--------------------------------------------------------------------------------------
// Vertex shader input structure
//--------------------------------------------------------------------------------------
struct CVS_INPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 TextureUV  : TEXCOORD;   // vertex texture coords 
};

struct CVS_OUTPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 Tex0       : TEXCOORD0;  // vertex texture coords 
};

//--------------------------------------------------------------------------------------
// Straight through vertex shader with inversion of y
//--------------------------------------------------------------------------------------

CVS_OUTPUT LinearVertexShader( CVS_INPUT Input ) 
{
    CVS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.TextureUV;

    return Output;
}

//--------------------------------------------------------------------------------------
// Calculate a 3 by 3 neighbourhood
//--------------------------------------------------------------------------------------

struct Square_VS_INPUT 
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Square_VS_OUTPUT 
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
    float2 Tex5 : TEXCOORD5;
    float2 Tex6 : TEXCOORD6;
    float2 Tex7 : TEXCOORD7;
    float2 Tex8 : TEXCOORD8;
};

////////////////////////////////////////////////////////////////////////////////////////////////
// NOTE this will work with ps_2_* profiles. In ps_3_0 this vertex shader
// could be used to compute the final vector as there are 10 texture coordinates
// available (rather than the 8 used by ps_2_*)
////////////////////////////////////////////////////////////////////////////////////////////////

Square_VS_OUTPUT SquareVertexShader( Square_VS_INPUT Input )
{
    Square_VS_OUTPUT Output;
	float x;
	float y;
	
	x = 1 / TextureWidth;
	y = 1 / TextureHeight;
	
    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;		// because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left

    // Matching texture coords in range 0 to 1
	Output.Tex0.x = Input.Tex.x - x;			// Upper left
	Output.Tex0.y = Input.Tex.y - y;
	Output.Tex1.x = Input.Tex.x;				// Up
	Output.Tex1.y = Output.Tex0.y;
	Output.Tex2.x = Input.Tex.x + x;			// Upper right
	Output.Tex2.y = Output.Tex0.y;
	Output.Tex3.x = Output.Tex0.x;				// Left
	Output.Tex3.y = Input.Tex.y;
	Output.Tex4 = Input.Tex;					// Centre
	Output.Tex5.x = Output.Tex2.x;				// Right
	Output.Tex5.y = Input.Tex.y;
	Output.Tex6.x = Output.Tex0.x;				// Lower left
	Output.Tex6.y = Input.Tex.y + y;
	Output.Tex7.x = Input.Tex.x;				// Down
	Output.Tex7.y = Output.Tex6.y;				
    Output.Tex8.x = Output.Tex2.x;
    Output.Tex8.y = Output.Tex6.y;
	
	// NOTE we have reached the max number of texture coords we can output from the vertex shader
	// so calculate the last coordinate in the pixel shader

    return Output;
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Registers C6 through c14 hold the coefficients like this
// 0 1 2
// 3 4 5
// 6 7 8
////////////////////////////////////////////////////////////////////////////////////////////////

float4 ConvolveShader3by3(Square_VS_OUTPUT Input ) : COLOR0
{
    float4 result;
    
    // lower right    
    result  = tex2D(SourceImage, Input.Tex0) * sampleCoeff[0];		// Upper left
    result += tex2D(SourceImage, Input.Tex1) * sampleCoeff[1];		// Up
    result += tex2D(SourceImage, Input.Tex2) * sampleCoeff[2];		// Upper right
    result += tex2D(SourceImage, Input.Tex3) * sampleCoeff[3];		// Left
    result += tex2D(SourceImage, Input.Tex4) * sampleCoeff[4];      // Centre
    result += tex2D(SourceImage, Input.Tex5) * sampleCoeff[5];		// Right
    result += tex2D(SourceImage, Input.Tex6) * sampleCoeff[6];		// Lower left
    result += tex2D(SourceImage, Input.Tex7) * sampleCoeff[7];      // Down
    result += tex2D(SourceImage, Input.Tex8) * sampleCoeff[8];		// Lower right
    return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Given the texture coordinate into the filter coefficients texture, look up the 
// float4 value. The x,y components are sample offsets into the image we are applying the
// filter to and the w component is the filter coefficient itself.
////////////////////////////////////////////////////////////////////////////////////////////////

float4 PixelConvolve(float Filt, float2 Pos)
{
	float4 C;
	float4 S;
	float4 R;
	float2 TexS;
	
	// Look up the pixel offsets (x, y component of texture) and filter coefficients (w component of texture)
	C = tex1D(CoeffImage, Filt);
	
	// Adjust sampler coords 
	TexS.x = C.x + Pos.x;
	TexS.y = C.y + Pos.y;
	
	// Multiply by the w component in the texture i.e. the filter coefficients
	S = tex2D(SourceImage, TexS);
	R = mul(S, C.w);
	
	return R;
}

////////////////////////////////////////////////////////////////////////////////////////////////
// N by N convolution 
////////////////////////////////////////////////////////////////////////////////////////////////

float4 ConvolveShaderNbyN(CVS_OUTPUT Input) : COLOR0
{
	int i;
	float TexC;
	float4 Result = 0.0;
	float Step;
	
	Step = Size * Size;
		
    TexC = 0.5 / Size;							// Centre of the coefficients texel
   			
	for (i = 0; i < SqSize; i++)
	{
		Result += PixelConvolve(TexC, Input.Tex0);
		TexC += 1.0 / Step;						// Next coefficient
	}
	return Result;
}



//--------------------------------------------------------------------------------------
// 3 by 3 neighbourhood convolution
// Much faster than the N by N convolve below due to the user of the vertex
// pipeline to do most of the coordinate calculations and also no second texture lookup
// as the coefficients for the convolution are stored in constant registers
//--------------------------------------------------------------------------------------

technique Convolve3by3
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 SquareVertexShader();
        PixelShader = compile ps_3_0 ConvolveShader3by3();
    }
}

//--------------------------------------------------------------------------------------
// N by N neighbourhood convolution
// NOTE we could probably write a faster N by N by basing it on the 3 by 3
// shader above then just using the techniques of the N by N version below to do
// the outer square calculations.
// Anyway who cares ? It goes like a rocket on GEFORCE 6600GT anyway...
//--------------------------------------------------------------------------------------

technique ConvolveNbyN
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 ConvolveShaderNbyN();
    }
}



