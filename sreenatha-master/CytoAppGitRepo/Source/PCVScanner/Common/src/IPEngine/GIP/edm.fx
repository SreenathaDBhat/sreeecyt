//------------------------------------------------------------------------------
// techniques for Eucledian Distance Map using jump flooding
//------------------------------------------------------------------------------
float MAXFLOAT = 9999999999.00;		    // initial distance = max float
float CorrTexWidth;						// correction factors for overlap when quads are
float CorrTexHeight;					// converted to unpacked colour image

float OffsetX;							// offset for neighbours during distance determination
float OffsetY;
float RatioW;							// Texture Width
float RatioH;							// Texture Height
float NormFactor;						// NormFactor
float GreyScale;						// Factor in case of White background

bool IsWhiteBackground;					// flag for distances within objects or background
bool EDMOnly;							// determines output distances only else
										// voronoi diagrams in red channel 
										
texture SourceImageTexture;				// Base texture input image  

sampler SourceImage = sampler_state 	// Sampler for base texture
{
    Texture = (SourceImageTexture);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

texture EDMImageTexture;				// Base texture input image  

sampler EDMImage = sampler_state 	// Sampler for base texture
{
    Texture = (EDMImageTexture);
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};
//--------------------------------------------------------------------------------------
// Vertex shader input structure
//--------------------------------------------------------------------------------------
struct VS_INPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 TextureUV  : TEXCOORD;   // vertex texture coords 
};

struct VS_OUTPUT
{
   float4 Position   : POSITION;   // vertex position 
   float2 Tex0       : TEXCOORD0;  // vertex texture coords 
};

struct PS_INPUT
{
   float2 Tex0       : TEXCOORD0;  // vertex texture coords 
};

VS_OUTPUT LinearVertexShader( VS_INPUT Input ) 
{
    VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;    
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.TextureUV;

    return Output;
}

struct Square_VS_INPUT 
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Square_VS_OUTPUT 
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
    float2 Tex5 : TEXCOORD5;
    float2 Tex6 : TEXCOORD6;
    float2 Tex7 : TEXCOORD7;
    float2 Tex8 : TEXCOORD8;
};

Square_VS_OUTPUT SquareVertexShader( Square_VS_INPUT Input )
{
    Square_VS_OUTPUT Output;
	
    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;

    // Matching texture coords in range 0 to 1
	Output.Tex0.x = Input.Tex.x - OffsetX;		// Upper left
	Output.Tex0.y = Input.Tex.y - OffsetY;
	Output.Tex1.x = Input.Tex.x;				// Up
	Output.Tex1.y = Output.Tex0.y;
	Output.Tex2.x = Input.Tex.x + OffsetX;		// Upper right
	Output.Tex2.y = Output.Tex0.y;
	Output.Tex3.x = Output.Tex0.x;				// Left
	Output.Tex3.y = Input.Tex.y;
	Output.Tex4   = Input.Tex;					// Centre
	Output.Tex5.x = Output.Tex2.x;				// Right
	Output.Tex5.y = Input.Tex.y;
	Output.Tex6.x = Output.Tex0.x;				// Lower left
	Output.Tex6.y = Input.Tex.y + OffsetY;
	Output.Tex7.x = Input.Tex.x;				// Down
	Output.Tex7.y = Output.Tex6.y;				
	Output.Tex8.x = Output.Tex2.x;				// Down right
	Output.Tex8.y = Output.Tex6.y;
    return Output;
}

//------------------------------------------------------------------------------
//
// Converts greyScale image (packed in greyQuads) to
// ColourImage r becomes greyvalue when 
//
//------------------------------------------------------------------------------

float4 PrepareEDMPass( PS_INPUT Input ) : COLOR0
{  
	float4	color;
	float2	pos;
	if ((Input.Tex0.x<0.5) && (Input.Tex0.y<0.5))
    {
		pos.x=(2-2*CorrTexWidth)*Input.Tex0.x;
		pos.y=(2-2*CorrTexHeight)*Input.Tex0.y;
		color = tex2D(SourceImage, pos);
		color.r=color.b;
	}
    else if ((Input.Tex0.x<0.5) && (Input.Tex0.y>0.5))
    {
		pos.x=(2-2*CorrTexWidth)*Input.Tex0.x;
		pos.y=(2-2*CorrTexHeight)*Input.Tex0.y+2*CorrTexHeight-1.;
		color = tex2D(SourceImage, pos);
		color.r=color.r;
    }
    else if ((Input.Tex0.x>0.5) && (Input.Tex0.y<0.5))
    {
		pos.x=(2-2*CorrTexWidth)*Input.Tex0.x+2*CorrTexWidth-1.;
		pos.y=(2-2*CorrTexHeight)*Input.Tex0.y;
		color = tex2D(SourceImage, pos);
		color.r=color.g;
    }
    else if ((Input.Tex0.x>0.5) && (Input.Tex0.y>0.5))
    {
		pos.x=(2-2*CorrTexWidth)*Input.Tex0.x+2*CorrTexWidth-1.;
		pos.y=(2-2*CorrTexHeight)*Input.Tex0.y+2*CorrTexHeight-1.;
		color = tex2D(SourceImage, pos);
		color.r=color.a;
	}
	if (IsWhiteBackground)
	{
		if (color.r==0)
		{
			color.r=GreyScale;			// invert background (eventual labelled object info is lost)
			color.ba=Input.Tex0;		// reference to coordinate during actual EDM passes
			color.g=0;					// distance is zero
		}
		else
		{
			color.r=0;					// invert background (eventual labelled object info is lost) 
			color.ba=-1;				// no reference defined
			color.g=MAXFLOAT;			// becomes distance during actual EDM passes
		}	
	}
	else
	{
		color.ba=0;
		if (color.r==0)
		{
			color.ba=-1;				// no reference defined
			color.g=MAXFLOAT;			// becomes distance during actual EDM passes
		}
		else
		{
			color.ba=Input.Tex0;		// reference to coordinate during actual EDM passes
			color.g=0;					// distance is zero
		}
	}
    return color;
}

float4 CheckDist(float4 curcol, float4 refcol, float2 curpos)
{
	float  dist;
	float4 newcol;
	newcol=curcol;
	if ((refcol.r != 0) || (refcol.b > 0))
	{
		// point is an object point itself so calc distance
		dist=(refcol.b-curpos.x)*RatioW*(refcol.b-curpos.x)*RatioW;
		dist +=((refcol.a-curpos.y)*RatioH*(refcol.a-curpos.y)*RatioH);
		if (dist < curcol.g)
		{
			newcol.g=dist;			// update the new distance
			newcol.ba=refcol.ba;	// and the reference
		}
	}
	return newcol;
}

//------------------------------------------------------------------------------
//
// EDM Pass
//	- calculates closest distance for neighbours at (offsetx, offsety)
//	  Using a ping-pong technique and dividing offsetx and offsety by 2
//	  the distance map is obtained after max(log2(width), log2(height)) passes 
//	  the coordinates to the reference pixel with closest distance is preserved.
//	  In the Finish pass the Voronoi diagram can als be obtained by copying the
//	  pixel value of the reference pixels.
//
//------------------------------------------------------------------------------

float4 EDMPass( Square_VS_OUTPUT In ) : COLOR0
{
	float4	curcolor;
	curcolor = tex2D(EDMImage, In.Tex4);
	if (curcolor.g != 0)		// if dist > 0 we should determine the minimum dist
	{
		curcolor=CheckDist(curcolor, tex2D(EDMImage, In.Tex0), In.Tex4);
		curcolor=CheckDist(curcolor, tex2D(EDMImage, In.Tex1), In.Tex4);
		curcolor=CheckDist(curcolor, tex2D(EDMImage, In.Tex2), In.Tex4);
		curcolor=CheckDist(curcolor, tex2D(EDMImage, In.Tex3), In.Tex4);
		curcolor=CheckDist(curcolor, tex2D(EDMImage, In.Tex5), In.Tex4);
		curcolor=CheckDist(curcolor, tex2D(EDMImage, In.Tex6), In.Tex4);
		curcolor=CheckDist(curcolor, tex2D(EDMImage, In.Tex7), In.Tex4);
		curcolor=CheckDist(curcolor, tex2D(EDMImage, In.Tex8), In.Tex4);
	}
	return curcolor;
}

//------------------------------------------------------------------------------
//
// Finish EDM Pass
//	- Normalizes distances and copies them to color.rgba. However it is
//	  possible, when color.r originally contained labelled object indices 
//    to obtain the Voronoi diagram by copying the original color 
//	  it by color.g it can be preserved.
//------------------------------------------------------------------------------

float4 FinishEDMPass( PS_INPUT Input ) : COLOR0
{
	float4	color;
	float4	refcolor;
	
	color = tex2D(SourceImage, Input.Tex0);
	color.g=NormFactor*sqrt(color.g)/sqrt(RatioW*RatioW + RatioH*RatioH);
	if (EDMOnly)
	{
		color.a=0;
		color.b=color.g;
		color.r=color.g;
	}	
	else
	{
		if (color.b > 0)
		{
			refcolor=tex2D(SourceImage, color.ba);
			color.r=refcolor.r;
		}
		else color.r=0;
		color.ba=0;
	}	
	return color;
}

/////////////////////////////////////////////////////////////////////////
//
// Techniques
//
/////////////////////////////////////////////////////////////////////////

technique PrepareEDM
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 PrepareEDMPass();
    }
}

technique EDM
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_3_0 SquareVertexShader();
        PixelShader = compile ps_3_0 EDMPass();
    }
}

technique FinishEDM
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 FinishEDMPass();
	}
}
