//------------------------------------------------------------------------------
// FFT shader techniques
// Written By Karl Ratcliff
//------------------------------------------------------------------------------

float CorrTexWidth;
float CorrTexHeight;
float ScaleFactor;

texture SourceImageTexture;				// Base texture to load input image  
										// Output image will go to current render target (backbuffer by default)
										// Calling routines can feed output image back into SourceImageTexture to perform multipass dilations etc.
texture SourceImage2Texture;			// Base texture to load second input image  
texture ScrambleTexture;				// Base texture to load input image  

texture ButterflyTexture;

texture FilterTexture;

sampler1D Scrambler = sampler_state 	// Sampler for base texture
{
    Texture = (ScrambleTexture);
    MIPFILTER = LINEAR;
};

sampler1D Butterfly = sampler_state 	// Sampler for base texture
{
    Texture = (ButterflyTexture);
    MIPFILTER = LINEAR;
};

sampler SourceImage = sampler_state 	// Sampler for base texture
{
    Texture = (SourceImageTexture);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SourceImage2 = sampler_state 	// Sampler for base texture
{
    Texture = (SourceImage2Texture);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler1D Filter = sampler_state 	// Sampler for base texture
{
    Texture = (FilterTexture);
    MIPFILTER = LINEAR;
};

//--------------------------------------------------------------------------------------
// Vertex shader input structure
//--------------------------------------------------------------------------------------
struct VS_INPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 TextureUV  : TEXCOORD;   // vertex texture coords 
};

struct VS_OUTPUT
{
   float4 Position   : POSITION;   // vertex position 
   float2 Tex0       : TEXCOORD0;  // vertex texture coords 
};

struct PS_INPUT
{
   float2 Tex0       : TEXCOORD0;  // vertex texture coords 
};

VS_OUTPUT LinearVertexShader( VS_INPUT Input ) 
{
    VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;    
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.TextureUV;

    return Output;
}


//------------------------------------------------------------------------------
//
// Converts image to greyScale, with Alpha of 0 for FFT
// based on black and white shader by Marwan Ansari
//
//------------------------------------------------------------------------------

float4 ColourToGrey( PS_INPUT Input ) : COLOR0
{  
    float4 c;
    float4 bwConverter = { 0.30f, 0.59f, 0.11f, 0.0f};
    float4 currFrameSample;

    currFrameSample = tex2D( SourceImage, Input.Tex0);
    c = dot( currFrameSample, bwConverter);
	c.a = 0.0;
    return c;
}

//------------------------------------------------------------------------------
//
// Converts greyScale image (packed in greyQuads) to
// ColourImage of course  RGB values are the same
//
//------------------------------------------------------------------------------

float4 GreyToColour( PS_INPUT Input ) : COLOR0
{  
	float4	color;
	float2	pos;
	if ((Input.Tex0.x<0.5) && (Input.Tex0.y<0.5))
    {
		pos.x=(2-2*CorrTexWidth)*Input.Tex0.x;
		pos.y=(2-2*CorrTexHeight)*Input.Tex0.y;
		color = tex2D(SourceImage, pos);
		color.r=color.b;
		color.g=color.r;
		color.b=color.r;
		color.a=0;
	}
    else if ((Input.Tex0.x<0.5) && (Input.Tex0.y>0.5))
    {
		pos.x=(2-2*CorrTexWidth)*Input.Tex0.x;
		pos.y=(2-2*CorrTexHeight)*Input.Tex0.y+2*CorrTexHeight-1.;
		color = tex2D(SourceImage, pos);
		color.r=color.r;
		color.g=color.r;
		color.b=color.r;
		color.a=0;
    }
    else if ((Input.Tex0.x>0.5) && (Input.Tex0.y<0.5))
    {
		pos.x=(2-2*CorrTexWidth)*Input.Tex0.x+2*CorrTexWidth-1.;
		pos.y=(2-2*CorrTexHeight)*Input.Tex0.y;
		color = tex2D(SourceImage, pos);
		color.r=color.g;
		color.g=color.r;
		color.b=color.r;
		color.a=0;
    }
    else if ((Input.Tex0.x>0.5) && (Input.Tex0.y>0.5))
    {
		pos.x=(2-2*CorrTexWidth)*Input.Tex0.x+2*CorrTexWidth-1.;
		pos.y=(2-2*CorrTexHeight)*Input.Tex0.y+2*CorrTexHeight-1.;
		color = tex2D(SourceImage, pos);
		color.r=color.a;
		color.g=color.r;
		color.b=color.r;
		color.a=0;
	}
    return color;
}

//------------------------------------------------------------------------------
//
// Display the PowerSpectrum of the FFT.
//
//------------------------------------------------------------------------------
float4 DisplayPower( PS_INPUT In ) : COLOR
{
    float4 pix;
	float mag;

	pix = tex2D( SourceImage, In.Tex0-0.5);
    mag = pix.x*pix.x + pix.w*pix.w;
 
    mag = .05 * log2(mag+1.0);
    return mag;
}

//------------------------------------------------------------------------------
//
// Rescale Output after inverse FFT
//
//------------------------------------------------------------------------------
float4 RescaleFFT ( PS_INPUT In ) : COLOR
{
    float4 pix;
	float4 col;

    pix = tex2D( SourceImage, In.Tex0);
    col.rgb = pix.x*ScaleFactor;		// only real part
	col.a=0;
    return col;
}

//------------------------------------------------------------------------------
//
// Performs horizontal butterfly pass of FFT.
//
//------------------------------------------------------------------------------

float4 HButterflyPass( PS_INPUT In ) : COLOR
{
    float2 sampleCoord;
    float4 butterflyVal;
    float2 a;
    float2 b;
    float2 w;
    float temp;

    butterflyVal = tex1D( Butterfly, In.Tex0.x);
    w = butterflyVal.ba;

    //sample location A
    sampleCoord.x = butterflyVal.y;
    sampleCoord.y = In.Tex0.y;
    a = tex2D( SourceImage, sampleCoord).ra;

    //sample location B
    sampleCoord.x = abs(butterflyVal.x);
    sampleCoord.y = In.Tex0.y;
    b = tex2D( SourceImage, sampleCoord).ra;

    //multiply w*b (complex numbers) 
    temp = w.x*b.x - w.y*b.y;
    b.y = w.y*b.x + w.x*b.y;
    b.x = temp;

    //perform a + w*b or a - w*b
    a = a + ((butterflyVal.x < 0.0) ? -b : b);

    //make it a 4 component output for good measure
    return a.xxxy;

	//return butterflyVal;
}

//------------------------------------------------------------------------------
//
// Performs horizontal scramble pass of FFT.
//
//------------------------------------------------------------------------------

float4 HScramblePass( PS_INPUT In ) : COLOR
{
    float2 fromPos;

    fromPos = In.Tex0;

    // scramble the x coordinate
    fromPos.x = tex1D(Scrambler, In.Tex0.x) ;

	return tex2D(SourceImage, fromPos);
}

//------------------------------------------------------------------------------
//
// Performs vertical butterfly pass of FFT.
//
//------------------------------------------------------------------------------

float4 VButterflyPass( PS_INPUT In ) : COLOR
{
    float2 sampleCoord;
    float4 butterflyVal;
    float2 a;
    float2 b;
    float2 w;
    float temp;

    butterflyVal = tex1D( Butterfly, In.Tex0.y);
    w = butterflyVal.ba;

    //sample location A
    sampleCoord.y = butterflyVal.y;
    sampleCoord.x = In.Tex0.x;
    a = tex2D( SourceImage, sampleCoord).ra;

    //sample location B
    sampleCoord.y = abs(butterflyVal.x);
    sampleCoord.x = In.Tex0.x;
    b = tex2D(SourceImage, sampleCoord).ra;

    //multiply w*b (complex numbers) 
    temp = w.x*b.x - w.y*b.y;
    b.y = w.y*b.x + w.x*b.y;
    b.x = temp;

    //perform a + w*b or a - w*b
    a = a + ((butterflyVal.x < 0.0) ? -b : b);

    //make it a 4 component output for good measure
    return a.xxxy;
}

//------------------------------------------------------------------------------
//
// Performs vertical scramble pass of FFT.
//
//------------------------------------------------------------------------------

float4 VScramblePass( PS_INPUT In ) : COLOR
{
    float2 fromPos;

    fromPos = In.Tex0;

    // scramble the y coordinate
    fromPos.y = tex1D(Scrambler, In.Tex0.y);

    return tex2D(SourceImage, fromPos);
}

//------------------------------------------------------------------------------
//
// Performs vertical scramble pass of FFT.
//
//------------------------------------------------------------------------------

float4 FilterShader( PS_INPUT In ) : COLOR
{
	float4	filtval;
	float2	pos;
	float	dist;

	if (In.Tex0.x < 0.5) pos.x=2.*In.Tex0.x;
	else  pos.x=(1.-In.Tex0.x)*2.;
	if (In.Tex0.y < 0.5) pos.y=2.*In.Tex0.y;
	else  pos.y=(1.-In.Tex0.y)*2.;
	dist=sqrt(pos.x*pos.x+pos.y*pos.y)/sqrt(2.);
    filtval = clamp(tex1D(Filter, dist), 0.0, 1.0);
    return ( tex2D(SourceImage, In.Tex0)*filtval);
}

//------------------------------------------------------------------------------
//
// Multiply Real part and complex conjugate of 2 FFTs
//
//------------------------------------------------------------------------------

float4 MultiplyShader( PS_INPUT In ) : COLOR
{
    float2 pix1, pix2, outp;

	pix1 = tex2D( SourceImage, In.Tex0).ra;
	pix2 = tex2D( SourceImage2, In.Tex0).ra;
	outp.x=(pix1.x*pix2.x + pix1.y*pix2.y);
	outp.y=(pix2.y*pix1.x - pix1.y*pix2.x);
    return outp.xxxy;
}

/////////////////////////////////////////////////////////////////////////
//
// Techniques
//
/////////////////////////////////////////////////////////////////////////

technique ColourToGrey
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 ColourToGrey();
    }
}

technique GreyToColour
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 GreyToColour();
    }
}

technique fft2D
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 HScramblePass();
    }
    pass P1
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 HButterflyPass();
	}
	pass P2
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 VScramblePass();
    }
    pass P3
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 VButterflyPass();
	}
    pass P4
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 RescaleFFT();
	}
}

technique PowerSpectrum
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 DisplayPower();
	}
}

technique FilterFFT
{
	pass P0
	{
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 FilterShader();
	}
}

technique MultiplyFFT
{
	pass P0
	{
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 MultiplyShader();
	}
}
