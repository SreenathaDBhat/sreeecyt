///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Gaussian Filter HLSL file
// Written By Karl Ratcliff
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

// Globals
float TextureWidth;                     // Width of input texture 
float TextureHeight;					// Height of input texture 
texture SourceImageTexture;             // Base texture to load input image
texture SourceImage2Texture;            // Base texture to load input image
texture SourceImage3Texture;            // Base texture to load input image
texture CoefficientsTexture;			// Stores the filter coefficients 
float	Amount;
int		Size;

//--------------------------------------------------------------------------------------
// Source and coefficients image samplers
//--------------------------------------------------------------------------------------

sampler SourceImage = sampler_state		// Sampler for base texture
{
    Texture = (SourceImageTexture);
    ADDRESSU = MIRROR;            // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;            //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SourceImage2 = sampler_state	// Sampler for base texture
{
    Texture = (SourceImage2Texture);
    ADDRESSU = MIRROR;            // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;            //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler SourceImage3 = sampler_state	// Sampler for base texture
{
    Texture = (SourceImage3Texture);
    ADDRESSU = MIRROR;            // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;            //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

sampler CoeffImage = sampler_state		// Sampler for base texture
{
    Texture = (CoefficientsTexture);
    ADDRESSU = MIRROR;            // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;            //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

//--------------------------------------------------------------------------------------
// Vertex shader input structure
//--------------------------------------------------------------------------------------
struct CVS_INPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 TextureUV  : TEXCOORD;   // vertex texture coords 
};

struct CVS_OUTPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 Tex0       : TEXCOORD0;  // vertex texture coords 
};

//--------------------------------------------------------------------------------------
// Straight through vertex shader with inversion of y
//--------------------------------------------------------------------------------------

CVS_OUTPUT LinearVertexShader( CVS_INPUT Input ) 
{
    CVS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.TextureUV;

    return Output;
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Given the texture coordinate into the filter coefficients texture, look up the 
// float4 value. The x,y components are sample offsets into the image we are applying the
// filter to and the w component is the filter coefficient itself.
////////////////////////////////////////////////////////////////////////////////////////////////

float4 PixelConvolveH(float Filt, float2 Pos)
{
	float4 C;
	float4 S;
	float4 R;
	float2 TexS;
	
	// Look up the pixel offsets (x, y component of texture) and filter coefficients (w component of texture)
	C = tex1D(CoeffImage, Filt);
	
	// Adjust sampler coords 
	TexS.x = C.x + Pos.x;
	TexS.y = Pos.y;
	
	// Multiply by the w component in the texture i.e. the filter coefficients
	S = tex2D(SourceImage, TexS);
	R = mul(S, C.w);
	
	return R;
}
////////////////////////////////////////////////////////////////////////////////////////////////
// Given the texture coordinate into the filter coefficients texture, look up the 
// float4 value. The x,y components are sample offsets into the image we are applying the
// filter to and the w component is the filter coefficient itself.
////////////////////////////////////////////////////////////////////////////////////////////////

float4 PixelConvolveV(float Filt, float2 Pos)
{
	float4 C;
	float4 S;
	float4 R;
	float2 TexS;
	
	// Look up the pixel offsets (x, y component of texture) and filter coefficients (w component of texture)
	C = tex1D(CoeffImage, Filt);
	
	// Adjust sampler coords 
	TexS.x = Pos.x;
	TexS.y = C.y + Pos.y;
	
	// Multiply by the w component in the texture i.e. the filter coefficients
	S = tex2D(SourceImage, TexS);
	R = mul(S, C.w);
	
	return R;
}

////////////////////////////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////////////////////////////

float4 GaussH(CVS_OUTPUT Input) : COLOR0
{
	int i;
	float TexC;
	float4 Result = 0.0;
	float4 R = 0.0;
		
    TexC = 0.5 / Size;							// Centre of the coefficients texel
   			
	for (i = 0; i < Size; i++)
	{
		R = PixelConvolveH(TexC, Input.Tex0);
		Result += R;
		TexC += 1.0 / Size;						// Next coefficient
	}
	
	return Result;
}

////////////////////////////////////////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////////////////////////////////////////

float4 GaussV(CVS_OUTPUT Input) : COLOR0
{
	int i;
	float TexC;
	float4 Result = 0.0;
		
    TexC = 0.5 / Size;							// Centre of the coefficients texel
   			
	for (i = 0; i < Size; i++)
	{
		Result += PixelConvolveV(TexC, Input.Tex0);
		TexC += 1.0 / Size;						// Next coefficient
	}
	
	return Result;
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Umask
////////////////////////////////////////////////////////////////////////////////////////////////

float4 Umask(CVS_OUTPUT Input) : COLOR0    
{ 
	float4 outp;
	outp=(tex2D(SourceImage,Input.Tex0)-Amount*tex2D(SourceImage2,Input.Tex0))/(1.-Amount);
    return outp;
}

float4 NNDeconv( CVS_OUTPUT Input ) : COLOR0    
{ 
	float4 neighbourCol, outp;
	neighbourCol=(tex2D(SourceImage2,Input.Tex0) + tex2D(SourceImage3,Input.Tex0))/2.;
	outp=tex2D(SourceImage, Input.Tex0);
    return ((outp - Amount*neighbourCol));
}


//--------------------------------------------------------------------------------------
// Gaussian blur implemented as a 2 pass separable filter
//--------------------------------------------------------------------------------------

technique gaussian
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 GaussH();
    }
    
    pass P1
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 GaussV();
    }
}

//--------------------------------------------------------------------------------------
// UnsharpMask part 
//--------------------------------------------------------------------------------------
technique UnsharpMask
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 Umask();
    }
}

//--------------------------------------------------------------------------------------
// NearestNeighbour part 
//--------------------------------------------------------------------------------------
technique NearestNeighbourDeconv
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 NNDeconv();
    }
}
