
//--------------------------------------------------------------//
// Image Processing Effects File - Mark Gregson 13Apr05
//--------------------------------------------------------------//
//    Calling program sets up simple quad based on 4 vertices with 
//    with processed display position -1 to +1
//    and matching texture ranges         0 to  1
//
//    CUSTOMVERTEX vertices[] =
//    {
//        // x      Y     Z    U  V
//        {-1.0f, -1.0f, 0.0f, 0, 1},    //lower left
//        {+1.0f, -1.0f, 0.0f, 1, 1}, //lower right
//        {+1.0f, +1.0f, 0.0f, 1, 0}, //upper right
//        {-1.0f, +1.0f, 0.0f, 0, 0}    //upper left
//    };
//
//    After loading into the vertex buffer the quad is then drawn 
//  using DrawPrimitive (D3DPT_TRIANGLEFAN, 0, 2);
//
//    Because the vertices have effectively been processed
//    there is no need for a vertex shader unless additional
//    texture coordinates are generated, e.g. in morphology 
//    or NxN filters
//
//--------------------------------------------------------------//
// Morph Effects File - Mark Gregson 13Apr05
//--------------------------------------------------------------//
//
// Globals
float TextureWidth;                 // Width of input texture 
float TextureHeight;                // Height of input texture 
texture SourceImageTexture;         // Base texture to load input image  
                                    // Output image will go to current render target (backbuffer by default)
                                    // Calling routines can feed output image back into SourceImageTexture to perform multipass dilations etc.
//string XFile<bool SasUiVisible = false;> = "tiger\\tiger.x";   // model

// Output image will go to current render target (backbuffer by default)

sampler SourceImage = sampler_state    // Sampler for base texture
{
    Texture = (SourceImageTexture);
    ADDRESSU = MIRROR;            // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;            //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

//--------------------------------------------------------------//
// 3x3 Axial and Diagonal Cross Min & Max Filters
//--------------------------------------------------------------//

struct Cross_VS_INPUT        // used for both axial and diagonal cross filters
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Cross_VS_OUTPUT        // used for both axial and diagonal cross filters
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
};

Cross_VS_OUTPUT AxialCrossVertexShader( Cross_VS_INPUT Input )
{
    Cross_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;
    Output.Tex1 = Output.Tex0;
    Output.Tex1.x = Output.Tex0.x-1.0f/TextureWidth;   // left
    Output.Tex2 = Output.Tex0;
    Output.Tex2.x = Output.Tex0.x+1.0f/TextureWidth;   // right
    Output.Tex3 = Output.Tex0;
    Output.Tex3.y = Output.Tex0.y-1.0f/TextureHeight;  // up
    Output.Tex4 = Output.Tex0;
    Output.Tex4.y = Output.Tex0.y+1.0f/TextureHeight;  // down

    return Output;
}


Cross_VS_OUTPUT DiagonalCrossVertexShader( Cross_VS_INPUT Input )
{
    Cross_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
 
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;
    Output.Tex1.x = Output.Tex0.x-1.0f/TextureWidth;        // left
    Output.Tex1.y = Output.Tex0.y-1.0f/TextureHeight;        // up
    Output.Tex2.x = Output.Tex0.x+1.0f/TextureWidth;        // right
    Output.Tex2.y = Output.Tex1.y;                        // up
    Output.Tex3.x = Output.Tex1.x;                        // left
    Output.Tex3.y = Output.Tex0.y+1.0f/TextureHeight;        // down
    Output.Tex4.x = Output.Tex2.x;                        // right
    Output.Tex4.y = Output.Tex3.y;                        // down

    return Output;
}


struct Cross_PS_INPUT    // used for both axial and diagonal cross filters
{
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
};


float4 MinCrossPixelShader( Cross_PS_INPUT Input ) : COLOR0    // used for both axial and diagonal cross filters
{  
    float4 result, val;

    result = tex2D(SourceImage, Input.Tex0);

    val = tex2D(SourceImage, Input.Tex1);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex2);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex3);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex4);
    result = min(val,result);

    return( result );   
}

float4 MaxCrossPixelShader( Cross_PS_INPUT Input ) : COLOR0    // used for both axial and diagonal cross filters
{  
   float4 result, val;
   
   result = tex2D(SourceImage, Input.Tex0);

   val = tex2D(SourceImage, Input.Tex1);
   result = max(val,result);
   val = tex2D(SourceImage, Input.Tex2);
   result = max(val,result);
   val = tex2D(SourceImage, Input.Tex3);
   result = max(val,result);
   val = tex2D(SourceImage, Input.Tex4);
   result = max(val,result);

   return( result );
}

//--------------------------------------------------------------//
// 3x3 Square Min & Max Filters
//--------------------------------------------------------------//

struct Square_VS_INPUT 
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Square_VS_OUTPUT 
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
    float2 Tex5 : TEXCOORD5;
    float2 Tex6 : TEXCOORD6;
    float2 Tex7 : TEXCOORD7;
};


Square_VS_OUTPUT SquareVertexShader( Square_VS_INPUT Input )
{
    Square_VS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
     Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left

    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.Tex;
    Output.Tex1 = Output.Tex0;
    Output.Tex1.x = Output.Tex0.x-1.0f/TextureWidth;   // left
    Output.Tex2 = Output.Tex0;
    Output.Tex2.x = Output.Tex0.x+1.0f/TextureWidth;   // right
    Output.Tex3 = Output.Tex0;
    Output.Tex3.y = Output.Tex0.y-1.0f/TextureHeight;  // up
    Output.Tex4 = Output.Tex0;
    Output.Tex4.y = Output.Tex0.y+1.0f/TextureHeight;  // down
    Output.Tex5 = Output.Tex1;
    Output.Tex5.y = Output.Tex3.y;                   // upper left
    Output.Tex6 = Output.Tex1;
    Output.Tex6.y = Output.Tex4.y;                   // lower left
    Output.Tex7 = Output.Tex2;
    Output.Tex7.y = Output.Tex3.y;                   // upper right

    return Output;
}

struct Square_PS_INPUT 
{
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
    float2 Tex5 : TEXCOORD5;
    float2 Tex6 : TEXCOORD6;
    float2 Tex7 : TEXCOORD7;
};


float4 MinSquarePixelShader( Square_PS_INPUT Input ) : COLOR0
{  
    float4 result, val;
    float2 Tex8;
    Tex8.x = Input.Tex0.x + 1.0f/TextureWidth;
    Tex8.y = Input.Tex0.y + 1.0f/TextureHeight;

    result = tex2D(SourceImage, Input.Tex0);

    val = tex2D(SourceImage, Input.Tex1);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex2);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex3);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex4);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex5);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex6);
    result = min(val,result);
    val = tex2D(SourceImage, Input.Tex7);
    result = min(val,result);
    val = tex2D(SourceImage, Tex8);
    result = min(val,result);

    return( result );
}

float4 MaxSquarePixelShader( Square_PS_INPUT Input ) : COLOR0
{  

    float4 result, val;
    float2 Tex8;
    Tex8.x = Input.Tex0.x + 1.0f/TextureWidth;
    Tex8.y = Input.Tex0.y + 1.0f/TextureHeight;

    result = tex2D(SourceImage, Input.Tex0);

    val = tex2D(SourceImage, Input.Tex1);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex2);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex3);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex4);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex5);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex6);
    result = max(val,result);
    val = tex2D(SourceImage, Input.Tex7);
    result = max(val,result);
    val = tex2D(SourceImage, Tex8);
    result = max(val,result);

    return( result );
}

//--------------------------------------------------------------//
// Technique Section for Morph
//--------------------------------------------------------------//

technique MaxAxialCross
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 AxialCrossVertexShader();
        PixelShader = compile ps_2_0 MaxCrossPixelShader();
    }
}

technique MaxDiagonalCross
{
   pass P0
   {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 DiagonalCrossVertexShader();
        PixelShader = compile ps_2_0 MaxCrossPixelShader();
   }
}

technique MaxSquare
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 SquareVertexShader();
        PixelShader = compile ps_2_0 MaxSquarePixelShader();
    }
}

technique MinAxialCross
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 AxialCrossVertexShader();
        PixelShader = compile ps_2_0 MinCrossPixelShader();
    }
}

technique MinDiagonalCross
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 DiagonalCrossVertexShader();
        PixelShader = compile ps_2_0 MinCrossPixelShader();
    }
}

technique MinSquare
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 SquareVertexShader();
        PixelShader = compile ps_2_0 MinSquarePixelShader();
    }
}




