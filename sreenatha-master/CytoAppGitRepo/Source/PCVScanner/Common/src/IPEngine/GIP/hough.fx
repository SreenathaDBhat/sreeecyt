//------------------------------------------------------------------------------
// techniques for Eucledian Distance Map using jump flooding
//------------------------------------------------------------------------------
float  OffsetX;
float  OffsetY;
float  CannyThresholdLow;
float  CannyThresholdHigh;
										
texture SourceImageTexture;				// Base texture input image  

sampler SourceImage = sampler_state 	// Sampler for base texture
{
    Texture = (SourceImageTexture);
    ADDRESSU = MIRROR;					// Mirror is better for morphological ops 
    ADDRESSV = MIRROR;					//- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

//--------------------------------------------------------------------------------------
// Vertex shader input structure
//--------------------------------------------------------------------------------------
struct Square_VS_INPUT 
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Square_VS_OUTPUT 
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
    float2 Tex5 : TEXCOORD5;
    float2 Tex6 : TEXCOORD6;
    float2 Tex7 : TEXCOORD7;
    float2 Tex8 : TEXCOORD8;
};

Square_VS_OUTPUT SquareVertexShader( Square_VS_INPUT Input )
{
    Square_VS_OUTPUT Output;
	
    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;

    // Matching texture coords in range 0 to 1
	Output.Tex0.x = Input.Tex.x - OffsetX;		// Upper left
	Output.Tex0.y = Input.Tex.y - OffsetY;
	Output.Tex1.x = Input.Tex.x;				// Up
	Output.Tex1.y = Output.Tex0.y;
	Output.Tex2.x = Input.Tex.x + OffsetX;		// Upper right
	Output.Tex2.y = Output.Tex0.y;
	Output.Tex3.x = Output.Tex0.x;				// Left
	Output.Tex3.y = Input.Tex.y;
	Output.Tex4   = Input.Tex;					// Centre
	Output.Tex5.x = Output.Tex2.x;				// Right
	Output.Tex5.y = Input.Tex.y;
	Output.Tex6.x = Output.Tex0.x;				// Lower left
	Output.Tex6.y = Input.Tex.y + OffsetY;
	Output.Tex7.x = Input.Tex.x;				// Down
	Output.Tex7.y = Output.Tex6.y;				
	Output.Tex8.x = Output.Tex2.x;				// Down right
	Output.Tex8.y = Output.Tex6.y;	
    return Output;
}

//--------------------------------------------------------------------------------------
// Canny Edge Detection Pass0
//--------------------------------------------------------------------------------------
float4 CannyEdgePass0( Square_VS_OUTPUT In ) : COLOR
{
    float4 result;
    float3 texSamples[4];
    float  Magnitude, Theta;
    float  p=0,q=0;
    float  PI = 3.1415926535897932384626433832795;
    int	   i;
   
    
    texSamples[0].xyz = tex2D( SourceImage, In.Tex1);    
    texSamples[1].xyz = tex2D( SourceImage, In.Tex2);    
    texSamples[2].xyz = tex2D( SourceImage, In.Tex4);    
    texSamples[3].xyz = tex2D( SourceImage, In.Tex5);    

    texSamples[0].xyz = dot(texSamples[0], 0.333333);    
    texSamples[1].xyz = dot(texSamples[1], 0.333333);    
    texSamples[2].xyz = dot(texSamples[2], 0.333333);    
    texSamples[3].xyz = dot(texSamples[3], 0.333333);    

	p= ((texSamples[1].x+texSamples[3].x-texSamples[0].x-texSamples[2].x))/2.;
	q= ((texSamples[2].x+texSamples[3].x-texSamples[0].x-texSamples[1].x))/2.;
    Magnitude = sqrt((p*p) + (q*q));
    result = Magnitude;

    // Now we compute the direction of the line to prep for Nonmaxia supression.
    // Nonmaxia supression - If this texel isnt the Max, make it 0 (hense, supress it)
    Theta = atan2(q,p); // result is -pi to pi

    result.a = (abs(Theta) / PI); // now result is 0 to 1..just so it can be written out.
    return result;    
}

//--------------------------------------------------------------------------------------
// Canny Edge Detection Pass1
//--------------------------------------------------------------------------------------

float4 CannyEdgePass1( Square_VS_OUTPUT In ) : COLOR
{
    int i =0;
    float4 result;
    float  Magnitude, Theta;
    float2 texCoords[4];
    float4 texSamples[3];
    float  PI = 3.1415926535897932384626433832795;
    

    // Tap the current texel and figure out what the direction of the line is.
    texSamples[0] = tex2D( SourceImage, In.Tex4); 
    Magnitude = texSamples[0].r;

    // We sample the two neighbors that lie in the direction of the line and then
    // find out if ___this___ texel has a greater Magnitude.
    Theta = texSamples[0].a;


    // Must unpack theta. Prior pass made Theta range between 0 and 1
    // But we really want it to be either 0,1,2, or 4. See Machine Vision book
    // for more details.
    Theta = (Theta - PI/16) * 4 ; // Now theta is between 0 and 4
    Theta = floor(Theta); // Now theta is an INT.

    if( Theta == 0)
    {
        texSamples[1] = tex2D( SourceImage, In.Tex5);    
        texSamples[2] = tex2D( SourceImage, In.Tex3);    
    }
    else if(Theta == 1)
    {
        texSamples[1] = tex2D( SourceImage, In.Tex2);    
        texSamples[2] = tex2D( SourceImage, In.Tex6);    
    }
    else if(Theta == 2)
    {
        texSamples[1] = tex2D( SourceImage, In.Tex1);    
        texSamples[2] = tex2D( SourceImage, In.Tex7);    
    }
    else //if(Theta == 3)
    {
        texSamples[1] = tex2D( SourceImage, In.Tex0);    
        texSamples[2] = tex2D( SourceImage, In.Tex8);    
    }

    // Now its time for Nonmaxia supression.
    // Nonmaxia supression - If this texel isnt the Max, make it 0 (hense, supress it)
    // This keeps the edges nice and thin.
    result = Magnitude;
    if( Magnitude < texSamples[1].x || Magnitude < texSamples[2].x )
    {
        result =0;
    }
  
    // Threshold the result.
    if ((result.x < CannyThresholdLow) || (result.x > CannyThresholdHigh))
    {
        result =0;
    }   
    else 
    {
        result = 1;
    }

    return result;    
}


/////////////////////////////////////////////////////////////////////////
//
// Techniques
//
/////////////////////////////////////////////////////////////////////////
/*
technique HoughLines
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_3_0 LinearVertexShader();
        PixelShader = compile ps_3_0 HoughLinesPass();
    }
}
*/
technique CannyEdge
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_3_0 SquareVertexShader();
        PixelShader = compile ps_3_0 CannyEdgePass0();
    }
    pass P1
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader =  compile vs_3_0 SquareVertexShader();
        PixelShader = compile ps_3_0 CannyEdgePass1();
    }
}
