///////////////////////////////////////////////////////////////////////////////////////////////////////////
// 3 by 3 and 5 by 5 Kuwahara filter HLSL file
// Written By Karl Ratcliff
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//
//--------------------------------------------------------------------------------------
// Source and coefficients image samplers
//--------------------------------------------------------------------------------------
texture SourceImageTexture;             // Base texture to load input image
// Offsets from the centre pixel 1, 2, 5 pixel steps
float TexStep1x;
float TexStep1y;
float TexStep2x;
float TexStep2y;
float TexStep5x;
float TexStep5y;

sampler SourceImage = sampler_state		// Sampler for base texture
{
    Texture = (SourceImageTexture);
    ADDRESSU = MIRROR;            // Mirror is better for morphological ops 
    ADDRESSV = MIRROR;            //- keeps min and max const near image edges
    MIPFILTER = LINEAR;
    MINFILTER = LINEAR;
};

//--------------------------------------------------------------------------------------
// Vertex shader input structure
//--------------------------------------------------------------------------------------
struct CVS_INPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 TextureUV  : TEXCOORD;   // vertex texture coords 
};

struct CVS_OUTPUT
{
    float4 Position   : POSITION;   // vertex position 
    float2 Tex0       : TEXCOORD0;  // vertex texture coords 
};

//--------------------------------------------------------------------------------------
// Straight through vertex shader with inversion of y
//--------------------------------------------------------------------------------------

CVS_OUTPUT LinearVertexShader( CVS_INPUT Input ) 
{
    CVS_OUTPUT Output;

    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;    // because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left
  
    // Matching texture coords in range 0 to 1
    Output.Tex0 = Input.TextureUV;

    return Output;
}

//--------------------------------------------------------------------------------------
// Calculate a 3 by 3 neighbourhood
//--------------------------------------------------------------------------------------

struct Square_VS_INPUT 
{
    float4 Position: POSITION;
    float2 Tex : TEXCOORD;
};

struct Square_VS_OUTPUT 
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
    float2 Tex4 : TEXCOORD4;
    float2 Tex5 : TEXCOORD5;
    float2 Tex6 : TEXCOORD6;
    float2 Tex7 : TEXCOORD7;
};

struct Cross_VS_OUTPUT 
{
    float4 Position : POSITION;
    float2 Tex0 : TEXCOORD0;
    float2 Tex1 : TEXCOORD1;
    float2 Tex2 : TEXCOORD2;
    float2 Tex3 : TEXCOORD3;
};

////////////////////////////////////////////////////////////////////////////////////////////////
// NOTE this will work with ps_2_* profiles. In ps_3_0 this vertex shader
// could be used to compute the final vector as there are 10 texture coordinates
// available (rather than the 8 used by ps_2_*)
////////////////////////////////////////////////////////////////////////////////////////////////

Square_VS_OUTPUT SquareVertexShader( Square_VS_INPUT Input )
{
    Square_VS_OUTPUT Output;
	
    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;		// because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left

    // Matching texture coords in range 0 to 1
	Output.Tex0.x = Input.Tex.x - TexStep1x;	// Upper left
	Output.Tex0.y = Input.Tex.y - TexStep1y;
	Output.Tex1.x = Input.Tex.x;				// Up
	Output.Tex1.y = Output.Tex0.y;
	Output.Tex2.x = Input.Tex.x + TexStep1x;	// Upper right
	Output.Tex2.y = Output.Tex0.y;
	Output.Tex3.x = Output.Tex0.x;				// Left
	Output.Tex3.y = Input.Tex.y;
	Output.Tex4   = Input.Tex;					// Centre
	Output.Tex5.x = Output.Tex2.x;				// Right
	Output.Tex5.y = Input.Tex.y;
	Output.Tex6.x = Output.Tex0.x;				// Lower left
	Output.Tex6.y = Input.Tex.y + TexStep1y;
	Output.Tex7.x = Input.Tex.x;				// Down
	Output.Tex7.y = Output.Tex6.y;				
	
	// NOTE we have reached the max number of texture coords we can output from the vertex shader
	// so calculate the last coordinate in the pixel shader

    return Output;
}

///////////////////////////////////////////////////////////////////////////////////
//      0 1
//       x
//      2 3 
///////////////////////////////////////////////////////////////////////////////////

Cross_VS_OUTPUT CrossVertexShader3( Square_VS_INPUT Input )
{
    Cross_VS_OUTPUT Output;
	
    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;		// because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left

    // Matching texture coords in range 0 to 1
	Output.Tex0.x = Input.Tex.x - TexStep1x;	// Upper left
	Output.Tex0.y = Input.Tex.y - TexStep1y;
	Output.Tex1.x = Input.Tex.x + TexStep1x;	// Upper right
	Output.Tex1.y = Output.Tex0.y;
	Output.Tex2.x = Output.Tex0.x;			    // Lower left
	Output.Tex2.y = Input.Tex.y + TexStep1y;
	Output.Tex3.x = Output.Tex1.x;				// Lower right
	Output.Tex3.y = Output.Tex2.y;
	
    return Output;
}

///////////////////////////////////////////////////////////////////////////////////
//      0 1
//       x
//      2 3 
///////////////////////////////////////////////////////////////////////////////////

Cross_VS_OUTPUT CrossVertexShader5( Square_VS_INPUT Input )
{
    Cross_VS_OUTPUT Output;
	
    // Clamp the position to the screen -1 to +1
    Output.Position = Input.Position;
    Output.Position.y = -Input.Position.y;		// because we are rendering into a texture, and textures have inverted coordinates - 0,0 is upper left

    // Matching texture coords in range 0 to 1
	Output.Tex0.x = Input.Tex.x - TexStep2x;	// Upper left
	Output.Tex0.y = Input.Tex.y - TexStep2y;
	Output.Tex1.x = Input.Tex.x + TexStep2x;	// Upper right
	Output.Tex1.y = Output.Tex0.y;
	Output.Tex2.x = Output.Tex0.x;			    // Lower left
	Output.Tex2.y = Input.Tex.y + TexStep2y;
	Output.Tex3.x = Output.Tex1.x;				// Lower right
	Output.Tex3.y = Output.Tex2.y;
	
    return Output;
}

///////////////////////////////////////////////////////////////////////////////////
// Calc the mean and variance of a three by three neighbourhood
// and put the mean colour in the rgb component and the variance in the alpha
///////////////////////////////////////////////////////////////////////////////////


float4 MeanVarianceCalc3by3( Square_VS_OUTPUT In ) : COLOR0 
{ 
	int    i; 
	float4 texSamples[9], mean, total = 0, c; 
	float variance; 
	//float2 Tex0, Tex1, Tex2, Tex3, Tex5, Tex6, Tex7, Tex8;
	float2 Tex8;
    
	//Tex0.x = In.Tex0.x - TexStep1x;	    // Upper left
	//Tex0.y = In.Tex0.y - TexStep1y;
	//Tex1.x = In.Tex0.x;				    // Up
	//Tex1.y = Tex0.y;
	//Tex2.x = In.Tex0.x + TexStep1x;	    // Upper right
	//Tex2.y = Tex0.y;
	//Tex3.x = Tex0.x;				    // Left
	//Tex3.y = In.Tex0.y;
	//Tex5.x = Tex2.x;				    // Right
	//Tex5.y = Tex3.y;
	//Tex6.x = Tex0.x;				    // Lower left
	//Tex6.y = Tex0.y + TexStep1y;
	//Tex7.x = Tex1.x;				    // Down
	//Tex7.y = Tex6.y;				
    Tex8.x = In.Tex2.x;                    // Lower right
    Tex8.y = In.Tex6.y;
	
	texSamples[0] = tex2D(SourceImage, In.Tex0);
	texSamples[1] = tex2D(SourceImage, In.Tex1);
	texSamples[2] = tex2D(SourceImage, In.Tex2);
	texSamples[3] = tex2D(SourceImage, In.Tex3);
	texSamples[4] = tex2D(SourceImage, In.Tex4);
	texSamples[5] = tex2D(SourceImage, In.Tex5);
	texSamples[6] = tex2D(SourceImage, In.Tex6);
	texSamples[7] = tex2D(SourceImage, In.Tex7);
	texSamples[8] = tex2D(SourceImage,    Tex8);
	
	for (i = 0; i < 9; i++) // compute the mean 
	{ 
		total += texSamples[i]; 
	} 
	
	mean = total / 9.0; 
	total = 0;			 
	
	// now compute the squared variance 
	for (i = 0; i < 9 ; i++) 
	{ 
		total += (mean - texSamples[i]) * (mean - texSamples[i]); 
	} 
	
	variance = dot(total, 0.1111111); // we dont need the root, but we need to add the r,g,and b 's together. 0.111... = 1/9
	
	c.xyz = mean; 
	c.a = variance; 
	
	return c; 
}

float4 MeanVarianceCalc5by5( Square_VS_OUTPUT In ) : COLOR0 
{ 
	int  i = 0; 
	float4 texSamples[25], mean, total = 0, c; 
	float variance; 
	float2 Tex8, Tex;
	float y2, y4;
    
    // lower right
    Tex8.x = In.Tex2.x;
    Tex8.y = In.Tex6.y;
	
	// Get the inner 9 samples using the vertex shader coordinates
	texSamples[0] = tex2D(SourceImage, In.Tex0);
	texSamples[1] = tex2D(SourceImage, In.Tex1);
	texSamples[2] = tex2D(SourceImage, In.Tex2);
	texSamples[3] = tex2D(SourceImage, In.Tex3);
	texSamples[4] = tex2D(SourceImage, In.Tex4);
	texSamples[5] = tex2D(SourceImage, In.Tex5);
	texSamples[6] = tex2D(SourceImage, In.Tex6);
	texSamples[7] = tex2D(SourceImage, In.Tex7);
	texSamples[8] = tex2D(SourceImage, Tex8);
		
	Tex.y = In.Tex4.y - TexStep2y;             // Upper row
	for (i = 0; i < 5; i++)
	{
	    Tex.x = In.Tex4.x + (i - 2) * TexStep5x; 
	    texSamples[9 + i] = tex2D(SourceImage, Tex);
	}
	Tex.y = In.Tex4.y + TexStep2y;             // Lower row
	for (i = 0; i < 5; i++)
	{
	    Tex.x = In.Tex4.x + (i - 2) * TexStep5x; 
	    texSamples[14 + i] = tex2D(SourceImage, Tex);
	}
	Tex.x = In.Tex4.x - TexStep2x;            // Remaining left hand 3 y's
	y2 = In.Tex4.y - TexStep1y;
	y4 = In.Tex4.y + TexStep1y;
	Tex.y = y2;
	texSamples[19] = tex2D(SourceImage, Tex);
    Tex.y = In.Tex4.y;	
	texSamples[20] = tex2D(SourceImage, Tex);
	Tex.y = y4;
	texSamples[21] = tex2D(SourceImage, Tex);
	Tex.x = In.Tex4.x + TexStep2x;            // Remaining right hand 3 y's
	Tex.y = y2;
	texSamples[22] = tex2D(SourceImage, Tex);
    Tex.y = In.Tex4.y;	
	texSamples[23] = tex2D(SourceImage, Tex);
	Tex.y = y4;
	texSamples[24] = tex2D(SourceImage, Tex);
	
	for (i = 0; i < 25; i++) // compute the mean 
	{ 
		total += texSamples[i]; 
	} 
	
	mean = total / 25.0; 
	total = 0;			 
	
	// now compute the squared variance 
	for (i = 0; i < 25 ; i++) 
	{ 
		total += (mean - texSamples[i]) * (mean - texSamples[i]); 
	} 
	
	variance = dot(total, 0.04); // we dont need the root, but we need to add the r,g,and b 's together. 0.04 = 1/25
	
	c.xyz = mean; 
	c.a = variance; 
	
	return c; 
}

///////////////////////////////////////////////////////////////////////////////////
// Sample the mean and variance image and choose the colour with the lowest
// variance using the image produced by the above shaders 
///////////////////////////////////////////////////////////////////////////////////

float4 VarianceSelection( Cross_VS_OUTPUT In ) : COLOR0
{ 
	float4 s0, s1, s2, s3, l2; 
	float  s0a,s1a,s2a,s3a,la, l2a; 
	float4 lowestVariance = 0.0;
		
	s0 = tex2D(SourceImage, In.Tex0);       // Sample the mean variance pairs… 
	s1 = tex2D(SourceImage, In.Tex1);       // Store the variances separately 
	s2 = tex2D(SourceImage, In.Tex2); 
	s3 = tex2D(SourceImage, In.Tex3); 
	
	s0a = s0.a; 
	s1a = s1.a; 
	s2a = s2.a; 
	s3a = s3.a; 
	
	if (s0a < s1a)                          // Find the lowest variance… 
	{ 
		lowestVariance = s0; 
		la = s0a; 
	} 
	else 
	{ 
		lowestVariance = s1; 
		la = s1a; 
	} 
	
	if (s2a < s3a) 
	{ 
		l2 = s2; 
		l2a = s2a; 
	} 
	else 
	{ 
		l2 = s3; 
		l2a = s3a; 
	} 
	
	if (l2a < la) 
	{
		lowestVariance = l2; 
	} 
	
	return lowestVariance;
}

//--------------------------------------------------------------------------------------
// A 3 by 3 Kuwahara filter
//--------------------------------------------------------------------------------------

technique Kuwahara3by3
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 SquareVertexShader();
        PixelShader = compile ps_2_0 MeanVarianceCalc3by3();
    }
    
    pass P1
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_2_0 CrossVertexShader3();
        PixelShader = compile ps_2_0 VarianceSelection();
    }
}

//--------------------------------------------------------------------------------------
// A 5 by 5 Kuwahara filter
//--------------------------------------------------------------------------------------

technique Kuwahara5by5
{
    pass P0
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 SquareVertexShader();
        PixelShader = compile ps_3_0 MeanVarianceCalc5by5();
    }
    
    pass P1
    {
        CULLMODE = NONE;
        ZENABLE = FALSE;

        VertexShader = compile vs_3_0 CrossVertexShader3();
        PixelShader = compile ps_3_0 VarianceSelection();
    }
}


