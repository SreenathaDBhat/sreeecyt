// GIPTIPDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "GIP.h"
#include "TIP.h"
#include "HRTimer.h"
#include <tchar.h>
#include <conio.h>
#include <ctype.h>


void LoadBMPFile(uchar4 **dst, int *width, int *height, const char *name);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Process using the GIP
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ProcessUsingGIP(RGBTRIPLE *Image, RGBTRIPLE *OutputImage, int Width, int Height)
{
	GIP theGIP;
	GIP_IMAGE	SrcImage;
	GIP_IMAGE	DestImage;
	GIP_IMAGE	TempImage;

	CHRTimer	HRT;
	double		e, t = 0.;

	HRT.Start();
	// Create the GIP class - should only do this once in your app as this is a slow call !
	if (theGIP.Create())
	{
		HRT.ElapsedTime(&e);
	    t += e;
		printf(_T("DIRECTX9 GPU INITIALISATION TIME %6.3lf MILLISECONDS\r\n"), e * 1000.);

		// Create some images
		// NOTE the GIP maintains a cached list of graphics textures for use on the GPU
		// use GIPReleaseImage to free them up for reuse later. This is faster than GIPFreeImage 
		// as a subsequent call to GIPCreateImage will reuse the GPU textures that have already 
		// been allocated by the initial call to GIPCreateImage (Provided the images are the same size of course)
		HRT.Start();
		SrcImage  = theGIP.GIPCreateImage(Width, Height, 0, 24);
		TempImage = theGIP.GIPCreateImage(Width, Height, 0, 24);
		DestImage = theGIP.GIPCreateImage(Width, Height, 0, 24);
				
		HRT.ElapsedTime(&e);
		t += e;
		printf(_T("DIRECTX9 GPU TEXTURE ALLOC TIME %6.3lf MILLISECONDS\r\n"), e * 1000.);


		HRT.Start();
		
		theGIP.PutImage(SrcImage, Image);

		// Before processing on the GPU, you need to call BeginIP()
		theGIP.BeginIP();
		// Do ten dilations of the SrcImage into the DestImage
		theGIP.Dilate(10, SrcImage, TempImage);

		// Now erode
		theGIP.Erode(10, TempImage, DestImage);

		// At the end of processing on the GPU you need to call EndIP()
		theGIP.EndIP();

		theGIP.GetImage(DestImage, OutputImage);
		
		HRT.ElapsedTime(&e);
		t += e;
		printf(_T("1ST TIME DIRECTX9 GPU PROCESSING ROUND TRIP + PROCESSING TIME %6.3lf MILLISECONDS\r\n"), e * 1000.);

		// NOTE the second time through the GIP will not need to compile any shaders/allocate resources etc.
		// and therefore is quite a bit quicker !
		HRT.Start();
		
		theGIP.PutImage(SrcImage, Image);

		// Before processing on the GPU, you need to call BeginIP()
		theGIP.BeginIP();
		// Do ten dilations of the SrcImage into the DestImage
		theGIP.Dilate(10, SrcImage, TempImage);

		// Now erode
		theGIP.Erode(10, TempImage, DestImage);

		// At the end of processing on the GPU you need to call EndIP()
		theGIP.EndIP();

		theGIP.GetImage(DestImage, OutputImage);
		
		HRT.ElapsedTime(&e);
		
		t += e;
		printf(_T("2ND TIME DIRECTX9 GPU PROCESSING ROUND TRIP + PROCESSING TIME %6.3lf MILLISECONDS\r\n"), e * 1000.);
		

		// NOTE the second time through the GIP will not need to compile any shaders/allocate resources etc.
		// and therefore is quite a bit quicker !
		HRT.Start();
		
		theGIP.PutImage(SrcImage, Image);

		// Before processing on the GPU, you need to call BeginIP()
		theGIP.BeginIP();
		// Do ten dilations of the SrcImage into the DestImage
		theGIP.Dilate(10, SrcImage, TempImage);

		// Now erode
		theGIP.Erode(10, TempImage, DestImage);

		// At the end of processing on the GPU you need to call EndIP()
		theGIP.EndIP();

		theGIP.GetImage(DestImage, OutputImage);
		
		HRT.ElapsedTime(&e);
		
		t += e;
		printf(_T("3RD TIME DIRECTX9 GPU PROCESSING ROUND TRIP + PROCESSING TIME %6.3lf MILLISECONDS\r\n"), e * 1000.);
		printf(_T("Total DIRECTX9 Processing Time %6.3lf MILLISECONDS\r\n"), t * 1000.);

		// Note you can explicitly call GIPFreeImage but destruction of the GIP object will also clear up
		// any allocated images
		theGIP.GIPFreeImage(DestImage);

	}
	else
		printf("HELP could not create D3D GIP Device\r\n");

}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Process using the TIP
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ProcessUsingTIP(RGBTRIPLE *Image, RGBTRIPLE *OutputImage, int Width, int Height)
{
	TIP			theTIP;
	TIP_IMAGE	SrcImage;
	TIP_IMAGE	DestImage;
	TIP_IMAGE	TempImage;

	CHRTimer	HRT;

	double		e, t;//go home

	int CUDDevCount;

	t = 0.;

	HRT.Start();
	// Create the GIP class - should only do this once in your app as this is a slow call !
	if (theTIP.Create())
	{
		CUDDevCount = theTIP.GetDeviceCount();

		if (CUDDevCount == 0)
		{
			printf(_T("NO CUDA DEVICES !!!!!!!!!!!!!!!!!!!!!!!!!!!\r\n"));
			return;
		}

		HRT.ElapsedTime(&e);
		t += e;
		printf(_T("CUDA INITIALISATION TIME %6.3lf MILLISECONDS\r\n"), e * 1000.);

		// Create some images
		// NOTE the TIP maintains a cached list of graphics textures for use on the GPU
		// use TIPReleaseImage to free them up for reuse later. This is faster than TIPFreeImage 
		// as a subsequent call to TIPCreateImage will reuse the CUDA textures that have already 
		// been allocated by the initial call to TIPCreateImage (Provided the images are the same size of course)
		HRT.Start();
		SrcImage  = theTIP.TIPCreateImage(0, Width, Height, 0, 24, TRUE);   // CUDA Array
		TempImage = theTIP.TIPCreateImage(0, Width, Height, 0, 24, FALSE);  // Normal 2D Array
		DestImage = theTIP.TIPCreateImage(0, Width, Height, 0, 24, FALSE);  // Normal 2D Array

		HRT.ElapsedTime(&e);
		t += e;

		printf(_T("CUDA ARRAY ALLOC TIME %6.3lf MILLISECONDS\r\n"), e * 1000.);

		HRT.Start();
		
		// SrcImage = Image
		theTIP.PutImage(SrcImage, Image);

		// NOTE no need to BeginIP()/EndIP() like with the GIP
		// Do ten dilations of the SrcImage into the DestImage
		theTIP.Dilate(10, SrcImage, TempImage);

		// Need to put this back in a CUDA array as TempImage is a Normal 2D array
		theTIP.Copy(TempImage, SrcImage);
		// Now do an erode
		theTIP.Erode(10, SrcImage, DestImage);

		theTIP.GetImage(DestImage, OutputImage);
		
		HRT.ElapsedTime(&e);
		t += e;
		
		printf(_T("1ST TIME CUDA GPU PROCESSING ROUND TRIP + PROCESSING TIME %6.3lf MILLISECONDS\r\n"), e * 1000.);
		
		HRT.Start();
		
		theTIP.PutImage(SrcImage, Image);

		// Do ten dilations of the SrcImage into the DestImage
		theTIP.Dilate(10, SrcImage, TempImage);

		// Need to put this back in a CUDA array as TempImage is a Normal 2D array
		theTIP.Copy(TempImage, SrcImage);
		// Now do an erode
		theTIP.Erode(10, SrcImage, DestImage);


		theTIP.GetImage(DestImage, OutputImage);
		
		HRT.ElapsedTime(&e);
		t += e;

		
		printf(_T("2ND TIME CUDA GPU PROCESSING ROUND TRIP + PROCESSING TIME %6.3lf MILLISECONDS\r\n"), e * 1000.);
		printf(_T("Total CUDA Processing Time %6.3lf MILLISECONDS\r\n"), t * 1000.);

		// Note you can explicitly call TIPFreeImage but destruction of the TIP object will also clear up
		// any allocated images
		theTIP.TIPFreeImage(DestImage);

	}
	else
		printf("HELP !!!!! Could not create CUDA Device\r\n");

}







//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Load in some image data
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

RGBTRIPLE *GetTheBMP(int *Width, int *Height)
{
	uchar4 *Image;
	RGBTRIPLE *Image24Bit;

	// NOTE this NVidia routine likes 32 bit images
	LoadBMPFile(&Image, Width, Height, _T("Composite.bmp"));

	Image24Bit = (RGBTRIPLE *) malloc((*Width) * (*Height) * sizeof(RGBTRIPLE));

	// Convert to 24 bit
	for (int i = 0; i < (*Width) * (*Height); i++)
	{
		Image24Bit[i].rgbtRed   = Image[i].x;
		Image24Bit[i].rgbtGreen = Image[i].y;
		Image24Bit[i].rgbtBlue  = Image[i].z;
	}

	free(Image);

	return Image24Bit;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int _tmain(int argc, _TCHAR* argv[])
{

	int Width, Height;
	RGBTRIPLE *Image24Bit;
	RGBTRIPLE *GIPOutputImage;
	RGBTRIPLE *TIPOutputImage;

	// Load the image
	Image24Bit = GetTheBMP(&Width, &Height);

	// Output image data
	GIPOutputImage = (RGBTRIPLE *) malloc(Width * Height * sizeof(RGBTRIPLE));
	TIPOutputImage = (RGBTRIPLE *) malloc(Width * Height * sizeof(RGBTRIPLE));

	printf(_T("---------------------------------------------------------------------------------------------\r\n"));

	// Process with DirectX shaders
	ProcessUsingGIP(Image24Bit, GIPOutputImage, Width, Height);

	printf(_T("---------------------------------------------------------------------------------------------\r\n"));
	
	// Process with CUDA
	ProcessUsingTIP(Image24Bit, TIPOutputImage, Width, Height);

	printf(_T("---------------------------------------------------------------------------------------------\r\n"));

	// Clear up
	free(TIPOutputImage);
	free(GIPOutputImage);
	free(Image24Bit);

	printf("Any key to quit....\r\n");

	_getch();

	return 0;
}

