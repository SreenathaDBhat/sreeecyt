// BatchDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IPEDEBUG.h"
#include "BatchDlg.h"


// CBatchDlg

IMPLEMENT_DYNAMIC(CBatchDlg, CDialog)

CBatchDlg::CBatchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBatchDlg::IDD, pParent)
{

}

CBatchDlg::~CBatchDlg()
{
}

void CBatchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SOURCEIMGFOLDER, m_SourceImgFolderCtrl);
	DDX_Control(pDX, IDC_RESULTSFOLDER, m_ResultsFolderCtrl);
	DDX_Control(pDX, IDC_CLASSIFIER, m_ClassifierCtrl);
}


BEGIN_MESSAGE_MAP(CBatchDlg, CDialog)
END_MESSAGE_MAP()



// CBatchDlg message handlers

BOOL CBatchDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here

	m_SourceImgFolderCtrl.SetWindowTextA( this->m_SourceImgFolder);
	m_ResultsFolderCtrl.SetWindowTextA( this->m_ResultsFolder);
	m_ClassifierCtrl.SetWindowTextA( this->m_Classifier);

	m_ClassifierCtrl.EnableFileBrowseButton( _T("model"), _T("Classifiers (*.model) | *.model||") );

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CBatchDlg::OnOK()
{
	// TODO: Add your specialized code here and/or call the base class

	if (UpdateData(TRUE))
	{
		m_SourceImgFolderCtrl.GetWindowTextA( this->m_SourceImgFolder);
		m_ResultsFolderCtrl.GetWindowTextA( this->m_ResultsFolder);
		m_ClassifierCtrl.GetWindowTextA( this->m_Classifier);
	}

	CDialog::OnOK();
}

