#pragma once

#include "afxeditbrowsectrl.h"

// CBatchDlg

class CBatchDlg : public CDialog
{
	DECLARE_DYNAMIC(CBatchDlg)

public:
	CBatchDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CBatchDlg();

// Dialog Data
	enum { IDD = IDD_BATCHDLG };

	CString	m_SourceImgFolder;
	CString m_ResultsFolder;
	CString m_Classifier;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
	virtual void OnOK();

	CMFCEditBrowseCtrl m_SourceImgFolderCtrl;
	CMFCEditBrowseCtrl m_ResultsFolderCtrl;
	CMFCEditBrowseCtrl m_ClassifierCtrl;
};


