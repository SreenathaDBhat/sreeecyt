

#include "stdafx.h"
#include "IPEDEBUG.h"
#include "strdlg.h"
#include "longdlg.h"
#include "Shlwapi.h"
#include "vcutsfile.h"
#include "hrtimer.h"
#include "vcmemrect.h"
#include "CMeasurement.h"
#include <hrtimer.h>
#include "BatchDlg.h"
#include "MainFrm.h"
#include "IPEDEBUGDoc.h"
#include "IPEDEBUGView.h"
#include "ccrystaleditview.h"
#include "cfindtextdlg.h"
#include "persistentsdlg.h"
#include "cmeasurement.h"
#include "IPEXMLReaderWriter.h"
#include "measurementdlg.h"
#include "ScriptThread.h"
#include "ScriptThreadPool.h"
#include "GetFolder.h"
#include "BatchMode.h"
#include <direct.h>
#include <stdio.h>
#include <stdlib.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




DWORD WINAPI TrawlerThread(LPVOID lpParam) 
{
	CUTSMemRect MR;
	ThreadParams  *pParams = (ThreadParams *)lpParam;

	void *ImageAddr;
	long BPP, P;
	long W;
	long H;
	int  Ha = 0;
	DWORD Proc;
	double X = 0.0, Y = 0.0, Z = 0.0;

	CScriptThreadPool	*pThreadPool = pParams->pThreadPool;

	MR.Initialise();

	CUTSFileIO FIO(&MR);
	CFileFind CFF;

	CString Wild;

	long (CUTSFileIO::*loadImageFn) (LPCTSTR filename, BOOL External);

	Wild.Format(_T("%s\\*.bmp"), pParams->SrcFolderPath);
	BOOL bFound = CFF.FindFile(Wild);
	loadImageFn = &CUTSFileIO::LoadDIB;
	if (!bFound)
	{
		CFF.Close();
		Wild.Format(_T("%s\\*.jls"), pParams->SrcFolderPath);
		bFound = CFF.FindFile(Wild);

		if (((CIPEDEBUGApp*)AfxGetApp())->m_loadAs8bpp)
			loadImageFn = &CUTSFileIO::LoadJPGLSAs8Bit;
		else
			loadImageFn = &CUTSFileIO::LoadJPGLS;
	}

	
	while (bFound)
	{
		bFound = CFF.FindNextFile();
		long UTSHandle = -1;

		if (!CFF.IsDirectory())
		{
			UTSHandle = (FIO.*loadImageFn)(CFF.GetFilePath(), TRUE);
		}

		if (UTSHandle != -1)
		{
			BPP = MR.GetExtendedInfo(UTSHandle, &W, &H, &P, &ImageAddr); 

			if (pThreadPool->WaitForFreeProcessor(INFINITE, Proc) == CScriptThreadPool::ThreadFree)
			{
				pThreadPool->AddImage(Proc, (BYTE *)ImageAddr, (int)BPP, (int)W, (int)H, X, Y, Z);
				X += 1000.0;
				if (X > 25000.0)
				{
					X = 0.0;
					Y += 1000.0;
				}
			}
			else
				break;

			MR.FreeImage(UTSHandle);
		}
	}

	CFF.Close();

	MR.FreeAll();

	ShowDebug(_T("IMAGES ADDED TO POOL\r\n"), pThreadPool->ImagesAdded());
	
	pThreadPool->SignalEnd();

	return 0;
}


static	TCHAR C[MAX_PATH];

static TCHAR *TOSTRING(int v)
{
	_stprintf(C, _T("%d"), v);

	return C;
}


static TCHAR *TOSTRING(double v)
{
	_stprintf(C, _T("%lf"), v);

	return C;
}

void WriteId(IPEXMLReaderWriter *pXML, int NumFrags)
{
	USES_CONVERSION;

	LPWSTR x = A2W(TOSTRING(NumFrags));

	pXML->Writer()->WriteAttributeString(NULL, L"Id", NULL, x);
}

void WriteData(IPEXMLReaderWriter *pXML, LPCTSTR Name, double Data)
{
	USES_CONVERSION;

	LPWSTR N = A2W(Name);
	LPWSTR D = A2W(TOSTRING(Data));
	
	pXML->Writer()->WriteElementString(NULL, N, NULL, D);
}

void WriteData(IPEXMLReaderWriter *pXML, LPCTSTR Name, int InGrid, int InClass)
{
	USES_CONVERSION;

	LPWSTR N = A2W(Name);
	LPWSTR G = A2W(TOSTRING(InGrid));
	LPWSTR C = A2W(TOSTRING(InClass));
	
	pXML->Writer()->WriteStartElement(NULL, L"Measurement", NULL);

		pXML->Writer()->WriteStartElement(NULL, L"Name", NULL);
		pXML->Writer()->WriteString(N);
		pXML->Writer()->WriteEndElement();

		pXML->Writer()->WriteStartElement(NULL, L"InClassifier", NULL);
		pXML->Writer()->WriteString(C);
		pXML->Writer()->WriteEndElement();

		pXML->Writer()->WriteStartElement(NULL, L"InGrid", NULL);
		pXML->Writer()->WriteString(G);
		pXML->Writer()->WriteEndElement();

	pXML->Writer()->WriteEndElement();
}

void ProcessResults(LPCTSTR DestFolderPath, int &NumFrags, CScriptThreadPool *pPool, CUTSMemRect *pMR, CUTSFileIO *pFIO, IPEXMLReaderWriter *pXML)
{
	Measurement *pMeas;
	VBS_IMAGE_REC GetImageInfo;
	TCHAR FP[_MAX_PATH];
	CMeasures Meas;
	BOOL GotData;
	BOOL First = TRUE;
	CScriptThread::Coordinate C;

	do
	{
		GotData = FALSE;
		if (pPool->IsDataAvailable(100))
		{
			pPool->GetData(GetImageInfo, Meas, C);

			if (First)
			{
				First = FALSE;
				pXML->Writer()->WriteStartElement(NULL,  L"Measurements", NULL);

				// Upload measurements
				pMeas = Meas.GetFirst();
				while (pMeas)
				{
					WriteData(pXML, pMeas->m_Name, pMeas->m_Flags.Flags.IncludeInGrid, pMeas->m_Flags.Flags.IncludeInClassifier);
					pMeas = Meas.GetNext();
				}
							
				pXML->Writer()->WriteFullEndElement();
			}

			pXML->Writer()->WriteStartElement(NULL,  L"Cell", NULL);
			WriteId(pXML, NumFrags);

			BOOL Class = FALSE;

			// Upload measurements
			pMeas = Meas.GetFirst();
			while (pMeas)
			{
				WriteData(pXML, pMeas->m_Name, pMeas->GetValue());				
				if (pMeas->m_Name == _T("Class"))
					Class = (pMeas->GetValue() > 0.0);
				pMeas = Meas.GetNext();
			}

			Meas.Kill();
			
			long Handle;//
			Handle = pMR->MakeExtern(GetImageInfo.Address, GetImageInfo.BitsPerPixel, GetImageInfo.Width, GetImageInfo.Height, 0);

			pMR->SetBits((BYTE *)GetImageInfo.Address, Handle);
			if (Class)
				_stprintf(FP, _T("%s\\PFrag%4.4d.bmp"), DestFolderPath, NumFrags++);
			else
				_stprintf(FP, _T("%s\\NFrag%4.4d.bmp"), DestFolderPath, NumFrags++);

			pFIO->SaveDIB(FP, Handle);
			pMR->FreeImage(Handle);
			delete GetImageInfo.Address;				
			
			pXML->Writer()->WriteFullEndElement();
			GotData = TRUE;
		}
	}
	while ((GotData || (!GotData && pPool->ImagesToProcess()) || !pPool->HasEnded()) && pPool->Status() == CScriptThreadPool::ThreadOk);

	if (pPool->Status() == CScriptThreadPool::ThreadError)
		MessageBox(NULL, "ERROR", "ERROR", MB_OK);
}

DWORD WINAPI ResultsThread(LPVOID lpParam) 
{
	USES_CONVERSION;

	ThreadParams  *pParams = (ThreadParams *)lpParam;

	TCHAR ResultsFile[_MAX_PATH];

	CUTSMemRect MR;


	int NumFrags = 0;
	IPEXMLReaderWriter *pXML;
	CMeasures Meas;

	CScriptThreadPool *pThreadPool = pParams->pThreadPool;

	CUTSFileIO FIO(&MR);
	CFileFind CFF;

	_stprintf(ResultsFile, _T("%s\\Results.xml"), pParams->DestFolderPath);
		
	BOOL bFound = CFF.FindFile(ResultsFile);
	if (bFound)
		DeleteFile(ResultsFile);

	pXML = new IPEXMLReaderWriter();

	pXML->CreateWriter(A2W(ResultsFile));
		
	pXML->Writer()->WriteStartDocument(XmlStandalone_Omit);
	pXML->Writer()->WriteStartElement(NULL, L"Results", NULL);

	ProcessResults(pParams->DestFolderPath, NumFrags, pThreadPool, &MR, &FIO, pXML);

	ShowDebug(_T("RESULTS PROCESSED BY POOL"), pThreadPool->GetProcessedCount());
	ShowDebug(_T("RESULTS FRAGMENTS"), NumFrags);

	pXML->Writer()->WriteFullEndElement();
	pXML->Writer()->WriteEndDocument();
	pXML->Writer()->Flush();


	MR.FreeAll();

	delete pXML;

	PostMessage(pParams->m_hParentWnd, WM_BATCHCOMPLETE, 0, 0L);

	return 0;
}