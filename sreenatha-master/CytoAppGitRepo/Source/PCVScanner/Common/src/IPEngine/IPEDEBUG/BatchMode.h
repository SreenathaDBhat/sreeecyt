#pragma once

#define		WM_BATCHCOMPLETE				WM_USER + 1

class CScriptThreadPool;

struct ThreadParams
{
	TCHAR				 SrcFolderPath[MAX_PATH];
	TCHAR				DestFolderPath[MAX_PATH];
	TCHAR				ScriptName[MAX_PATH];
	CScriptThreadPool	*pThreadPool;
	HWND				m_hParentWnd;
};

DWORD WINAPI TrawlerThread(LPVOID lpParam);
DWORD WINAPI ResultsThread(LPVOID lpParam);