// ComponentSelBtn.cpp : implementation file
//

#include "stdafx.h"
#include "IPEDEBUG.h"
#include "ComponentSelBtn.h"


CComponentSelBtn::CComponentSelBtn(CString &Caption, CRect &Rect, CWnd *pParent)
{
	m_pCaption = new CStatic();

	m_pCaption->Create(Caption, WS_CHILD|WS_VISIBLE|SS_CENTER, Rect, pParent);
}

CComponentSelBtn::~CComponentSelBtn()
{
	if (m_pCaption)
		delete m_pCaption;
	if (m_pBtn)
		delete m_pBtn;
}


