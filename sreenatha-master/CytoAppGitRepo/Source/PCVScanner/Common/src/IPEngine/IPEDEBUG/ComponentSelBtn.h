#pragma once


// CComponentSelBtn

class CComponentSelBtn 
{
	DECLARE_DYNAMIC(CComponentSelBtn)

public:
	CComponentSelBtn(CString &Caption, CRect &Rect, CWnd *pParent);
	~CComponentSelBtn();

	CStatic	  *m_pCaption;
	CButton   *m_pBtn;

protected:
	DECLARE_MESSAGE_MAP()
};


