// ComponentSelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IPEDEBUG.h"
#include "ComponentSelDlg.h"
#include ".\componentseldlg.h"


// CComponentSelDlg dialog

IMPLEMENT_DYNAMIC(CComponentSelDlg, CDialog)
CComponentSelDlg::CComponentSelDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CComponentSelDlg::IDD, pParent)
{
}

CComponentSelDlg::~CComponentSelDlg()
{
}

void CComponentSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CComponentSelDlg, CDialog)
    ON_BN_CLICKED(IDC_HIDE_BUTTON, OnBnClickedHideButton)
END_MESSAGE_MAP()


// CComponentSelDlg message handlers

void CComponentSelDlg::OnBnClickedHideButton()
{
    // TODO: Add your control notification handler code here
}
