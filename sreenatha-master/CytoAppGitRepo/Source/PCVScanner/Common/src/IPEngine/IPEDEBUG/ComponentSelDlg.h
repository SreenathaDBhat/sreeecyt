#pragma once


// CComponentSelDlg dialog

class CComponentSelDlg : public CDialog
{
	DECLARE_DYNAMIC(CComponentSelDlg)

public:
	CComponentSelDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CComponentSelDlg();

// Dialog Data
	enum { IDD = IDD_COMPONENT_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnBnClickedHideButton();
};
