// ComponentSelectionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "aitiff.h"
#include "IPEDEBUG.h"
#include "ImageView.h"
#include "CompSelectionCtrl.h"
#include "ComponentSelectionDlg.h"


// CComponentSelectionDlg dialog

IMPLEMENT_DYNAMIC(CComponentSelectionDlg, CDialog)

CComponentSelectionDlg::CComponentSelectionDlg(AI_Tiff *pTiff, CWnd* pParent)
	: CDialog(CComponentSelectionDlg::IDD, pParent)
{
	int nComps = 0, i;
	TiffComponent *pComp;
	CCompSelectionCtrl *pSel;
	rgbTriple RGB;

	m_pNotifyWnd = (CImageView *)pParent;

	m_pTiff = pTiff;

    // Get the number of components
    for (i = 0; i < 255; i++)
    {
        if (pComp = m_pTiff->GetComponent(i))
		{
			pSel = new CCompSelectionCtrl();
			pSel->m_Caption.Format("%s", pComp->description);
			m_pTiff->GetComponentColour(i, &RGB);
			pSel->m_Colour.rgbRed = RGB.r;
			pSel->m_Colour.rgbGreen = RGB.g;
			pSel->m_Colour.rgbBlue = RGB.b;
			m_CtrlList.AddTail(pSel);
            nComps++;
		}
        else 
            break;
    }

	// Uses 8 point MS Sans Serif for text
	LOGFONT logFont;
	memset(&logFont, 0, sizeof(logFont));
	strncpy(logFont.lfFaceName, "MS Sans Serif", LF_FACESIZE);
	logFont.lfHeight         = 8;
	logFont.lfWeight         = FW_MEDIUM;
	logFont.lfCharSet        = ANSI_CHARSET;
	logFont.lfQuality        = PROOF_QUALITY;

	m_Font.CreateFontIndirect(&logFont);
}

CComponentSelectionDlg::~CComponentSelectionDlg()
{
	POSITION P;
	CCompSelectionCtrl *pSel;

	// Kill all the controls in the list
	P = m_CtrlList.GetHeadPosition();
	while (P)
	{
		pSel = m_CtrlList.GetNext(P);
		delete pSel;
	}
}

void CComponentSelectionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CComponentSelectionDlg, CDialog)
	ON_CONTROL_RANGE(BN_CLICKED, BTN_BASE_ID, BTN_BASE_ID + 255, OnBnClick)
END_MESSAGE_MAP()


// CComponentSelectionDlg message handlers

BOOL CComponentSelectionDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	int i = 0, Height = 15;
	TiffComponent *pComp;
	CRect rcWind, rcControl, rcCtrlPos;
	POSITION P;
	CCompSelectionCtrl *pSel;

	GetClientRect(&rcWind);

	rcCtrlPos.left = 0;
	rcCtrlPos.top = 0;
	rcCtrlPos.right = rcWind.right - 20;
	rcCtrlPos.bottom = Height;

	// Kill all the controls in the list
	P = m_CtrlList.GetHeadPosition();
	while (P)
	{
		pComp = m_pTiff->GetComponent(i);
		pSel = m_CtrlList.GetNext(P);

		pSel->m_nID = i + BTN_BASE_ID;

		pSel->Create(pSel->m_Caption, 
			         WS_CHILD|WS_VISIBLE|BS_CHECKBOX|BS_AUTOCHECKBOX,
					 rcCtrlPos,
					 this,
					 pSel->m_nID);

		pSel->SetFont(&m_Font, FALSE);
		rcCtrlPos.top += Height;
		rcCtrlPos.bottom += Height;

		rcWind.bottom += Height;
		pSel->ShowWindow(SW_SHOW);
		CheckDlgButton(pSel->m_nID, BST_CHECKED);
		i++;
	}		
	
	MoveWindow(rcWind, TRUE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CComponentSelectionDlg::OnBnClick(UINT nID)
{
	if (m_pNotifyWnd)
		m_pNotifyWnd->PostMessageA(WM_COMPSEL, nID - BTN_BASE_ID, IsDlgButtonChecked(nID));
	m_pNotifyWnd->OnCompSelection(nID - BTN_BASE_ID, IsDlgButtonChecked(nID));
}
