#pragma once

class AI_Tiff;
class CCompSelectionCtrl;
class CImageView;

typedef CCompSelectionCtrl *CompSelPtr;

#define BTN_BASE_ID					1000

#define WM_COMPSEL					WM_USER + 0x100

// CComponentSelectionDlg dialog

class CComponentSelectionDlg : public CDialog
{
	DECLARE_DYNAMIC(CComponentSelectionDlg)

public:
	CComponentSelectionDlg(AI_Tiff *pTiff, CWnd* pParent);   // standard constructor
	virtual ~CComponentSelectionDlg();
	AI_Tiff	*m_pTiff;

// Dialog Data
	enum { IDD = IDD_COMPONENT_DIALOG };

	CList <CompSelPtr, CompSelPtr>  m_CtrlList;
    CFont                           m_Font;      // Dialog font stuff
	CImageView						*m_pNotifyWnd;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClick(UINT nID);
};

