// IPEDEBUG.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "IPEDEBUG.h"
#include "uts.h"
#include "MainFrm.h"
#include "ChildFrm.h"
#include "IPEDEBUGDoc.h"
#include "IPEDEBUGView.h"
#include "ImageDoc.h"
#include "ImageMultiDoc.h"
#include "ImageView.h"
#include "IPEDEBUGFxDoc.h"
#include "IPEDEBUGFxView.h"
#include ".\ipedebug.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
/////////////////////////////////////////////////////////////////////////////
// CRegistry message handlers
CString &CIPERegistry::GetRegItem(char *key, char *subkey)
{
	static CString curr = "";

	curr = "";

	char dll[255];
	char szKey[255];
	HKEY hKey;
	long dwSize;

    wsprintf( szKey, "Software\\IPEDEBUG\\%s", key);
	if( RegOpenKey( HKEY_LOCAL_MACHINE, szKey, &hKey ) == ERROR_SUCCESS ) 
//    if( RegOpenKeyEx( HKEY_LOCAL_MACHINE, szKey, 0, KEY_WRITE, &hKey) == ERROR_SUCCESS )
	{
		dwSize = sizeof( dll );

		DWORD index = 0;

		if (RegQueryValue(hKey, subkey, dll, &dwSize) == ERROR_SUCCESS)
//		if (RegQueryValueEx(hKey, subkey, NULL, &type, (unsigned char *)dll, &dwSize))
			curr = dll;

		RegCloseKey( hKey );
	}	

	return curr;
}

void CIPERegistry::SetRegItem(char *key, char *subkey, const char * value)
{
	char szKey[255];
	HKEY hKey;
	DWORD dwSize;

    wsprintf( szKey, "Software\\IPEDEBUG\\%s", key);
    if( RegOpenKeyEx( HKEY_LOCAL_MACHINE, szKey, 0, KEY_WRITE, &hKey) != ERROR_SUCCESS )
		if( RegCreateKeyEx( HKEY_LOCAL_MACHINE, szKey, 0, 0, 0, KEY_ALL_ACCESS, NULL, &hKey, &dwSize ) != ERROR_SUCCESS ) 
			return;

	dwSize = strlen( value );

	// RegSetValueEx(hKey, subkey, 0, REG_SZ,	(unsigned char *)value, dwSize);
	RegSetValue(hKey, subkey, REG_SZ, value, dwSize);

	RegCloseKey( hKey );
}

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGApp

BEGIN_MESSAGE_MAP(CIPEDEBUGApp, CWinAppEx)
	//{{AFX_MSG_MAP(CIPEDEBUGApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_COMMAND(ID_FILE_NEW, OnFileNew)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinAppEx::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinAppEx::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinAppEx::OnFilePrintSetup)
    ON_COMMAND(ID_FILE_OPENIMAGE, OnFileOpenimage)
    ON_COMMAND(ID_FILE_OPENSCRIPT, OnFileOpenscript)
    ON_COMMAND(ID_FILE_OPENEFFECT, OnFileOpeneffect)
END_MESSAGE_MAP()

// By inserting this hook it is possible to at least set a breakpoint when the size is X bytes and hence find out where a leak
// reported by the debugger is getting allocated
int LeakAllocHook(int nAllocType, void *pvData, size_t nSize, int nBlockUse, long lRequest, const unsigned char *szFileName, int nLine )
{
	if (nAllocType == _CRT_BLOCK)
		return TRUE;
	
	if (nAllocType == _HOOK_ALLOC)
	{
			int x = 1;
			x++;
	}	
	else
	if (nAllocType == _HOOK_FREE)
	{
		

	}	

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGApp construction

CIPEDEBUGApp::CIPEDEBUGApp()
{


	CIPERegistry IPER;
	m_TempDir = IPER.GetRegItem("Directories", "Temp");
	if (m_TempDir == "")
		m_TempDir = "\\Temp";
	m_ImageDir = IPER.GetRegItem("Directories", "Images");
	if (m_ImageDir == "")
		m_ImageDir = "\\Images";
	m_ScriptDir = IPER.GetRegItem("Directories", "Scripts");
	if (m_ScriptDir == "")
		m_ScriptDir = "\\Scripts";
    m_EffectsDir = IPER.GetRegItem("Directories", "Effects");
	if (m_EffectsDir == "")
		m_EffectsDir = "\\Effects";
    m_loadAs8bpp = IPER.GetRegItem("Options", "8bpp") == "1" ? TRUE : FALSE;

//_CrtSetAllocHook(LeakAllocHook);

}

/////////////////////////////////////////////////////////////////////////////
// The one and only CIPEDEBUGApp object

CIPEDEBUGApp theApp;

// This identifier was generated to be statistically unique for your app.
// You may change it if you prefer to choose a specific identifier.

// {3DF11558-6FC0-4355-B90C-B92E7338EF34}
static const CLSID clsid =
{ 0x3df11558, 0x6fc0, 0x4355, { 0xb9, 0xc, 0xb9, 0x2e, 0x73, 0x38, 0xef, 0x34 } };

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGApp initialization

BOOL CIPEDEBUGApp::InitInstance()
{
	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	CWinAppEx::InitInstance();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.


	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("IPENGINE_DEBUGGER"));

	LoadStdProfileSettings(NUMBER_RECENTLY_USED);  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

    // Scripts
	m_pScriptDocTemplate = new CMultiDocTemplate(
		IDR_IPEDEBTYPE,
		RUNTIME_CLASS(CIPEDEBUGDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CIPEDEBUGView));
	AddDocTemplate(m_pScriptDocTemplate);

    // Images
	m_pImageDocTemplate = new CImageMultiDocTemplate(
		IDR_IPEIMAGETYPE,
		RUNTIME_CLASS(CImageDoc),
		RUNTIME_CLASS(CMDIChildWnd), // custom MDI child frame
		RUNTIME_CLASS(CImageView));
	AddDocTemplate(m_pImageDocTemplate);

    // Effects 
	m_pEffectsDocTemplate = new CMultiDocTemplate(
		IDR_IPEDEBFXTYPE,
		RUNTIME_CLASS(CIPEDEBUGFxDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CIPEDEBUGFxView));
	AddDocTemplate(m_pEffectsDocTemplate);

	// Connect the COleTemplateServer to the document template.
	//  The COleTemplateServer creates new documents on behalf
	//  of requesting OLE containers by using information
	//  specified in the document template.
	m_server.ConnectTemplate(clsid, m_pScriptDocTemplate, FALSE);

	// Register all OLE server factories as running.  This enables the
	//  OLE libraries to create objects from other applications.
	COleTemplateServer::RegisterAll();
		// Note: MDI applications register all server objects without regard
		//  to the /Embedding or /Automation on the command line.

	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	m_pMainWnd = pMainFrame;

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Check to see if launched as OLE server
	if (cmdInfo.m_bRunEmbedded || cmdInfo.m_bRunAutomated)
	{
		// Application was run with /Embedding or /Automation.  Don't show the
		//  main window in this case.
		return TRUE;
	}

	// When a server application is launched stand-alone, it is a good idea
	//  to update the system registry in case it has been damaged.
	m_server.UpdateRegistry(OAT_DISPATCH_OBJECT);
	COleObjectFactory::UpdateRegistryAll();

	// Dispatch commands specified on the command line
    if (cmdInfo.m_nShellCommand != CCommandLineInfo::FileNew)
    {
        if (!ProcessShellCommand(cmdInfo))
		    return FALSE;
    }

	// The main window has been initialized, so show and update it.
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();


	


	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CIPEDEBUGApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGApp message handlers


void CIPEDEBUGApp::OnFileNew() 
{
    CWinAppEx::OnFileNew();	
}

// Hacked MFC code
void CIPEDEBUGApp::AppendFilterSuffix(CString& filter, OPENFILENAME& ofn, CDocTemplate* pTemplate, CString* pstrDefaultExt)
{
	ASSERT_VALID(pTemplate);
	ASSERT_KINDOF(CDocTemplate, pTemplate);

	CString strFilterExt, strFilterName;
	if (pTemplate->GetDocString(strFilterExt, CDocTemplate::filterExt) &&
		!strFilterExt.IsEmpty() &&
		pTemplate->GetDocString(strFilterName, CDocTemplate::filterName) &&
		!strFilterName.IsEmpty())
	{
		if (pstrDefaultExt != NULL)
			pstrDefaultExt->Empty();

		// add to filter
		filter += strFilterName;
		ASSERT(!filter.IsEmpty());  // must have a file type name
		filter += (TCHAR)'\0';  // next string please

		int iStart = 0;
		do
		{
			CString strExtension = strFilterExt.Tokenize( _T( ";" ), iStart );

			if (iStart != -1)
			{
				// a file based document template - add to filter list

				// If you hit the following ASSERT, your document template 
				// string is formatted incorrectly.  The section of your 
				// document template string that specifies the allowable file
				// extensions should be formatted as follows:
				//    .<ext1>;.<ext2>;.<ext3>
				// Extensions may contain wildcards (e.g. '?', '*'), but must
				// begin with a '.' and be separated from one another by a ';'.
				// Example:
				//    .jpg;.jpeg
				ASSERT(strExtension[0] == '.');
				if ((pstrDefaultExt != NULL) && pstrDefaultExt->IsEmpty())
				{
					// set the default extension
					*pstrDefaultExt = strExtension.Mid( 1 );  // skip the '.'
					ofn.lpstrDefExt = const_cast< LPTSTR >((LPCTSTR)(*pstrDefaultExt));
					ofn.nFilterIndex = ofn.nMaxCustFilter + 1;  // 1 based number
				}

				filter += (TCHAR)'*';
				filter += strExtension;
				filter += (TCHAR)';';  // Always append a ';'.  The last ';' will get replaced with a '\0' later.
			}
		} while (iStart != -1);

		filter.SetAt( filter.GetLength()-1, '\0' );;  // Replace the last ';' with a '\0'
		ofn.nMaxCustFilter++;
	}
}
    
BOOL CIPEDEBUGApp::DoPromptFileName(CString& fileName, CString InitDir, UINT nIDSTitle, CString Filter, DWORD lFlags, BOOL bOpenFileDialog, CDocTemplate* pTemplate)
{
	CFileDialog dlgFile(bOpenFileDialog, 
                        NULL, NULL, 
                        OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, NULL, NULL, 0);
	CString title;
	VERIFY(title.LoadString(nIDSTitle));
    
	dlgFile.m_ofn.Flags |= lFlags;
    dlgFile.m_ofn.lpstrInitialDir = InitDir;
    
    CString strFilter;
    CString strDefault;
    if (pTemplate != NULL)
    {
        ASSERT_VALID(pTemplate);
        AppendFilterSuffix(strFilter, dlgFile.m_ofn, pTemplate, &strDefault);
    }
    else
    {
        // do for all doc template
        // this section iterates through all document templates
        POSITION pos = GetFirstDocTemplatePosition();
        BOOL bFirst = TRUE;
        while (pos != NULL)
        {
            CDocTemplate* pTemplate = GetNextDocTemplate(pos);
            CString DocFilter;
        
            pTemplate->GetDocString(DocFilter, CDocTemplate::fileNewName);  
            if (DocFilter == Filter)
            {
                AppendFilterSuffix(strFilter, dlgFile.m_ofn, pTemplate, bFirst ? &strDefault : NULL);
                bFirst = FALSE;
            }
        }
    }

    // append the "*.*" all files filter
    CString allFilter;
    VERIFY(allFilter.LoadString(AFX_IDS_ALLFILTER));
    strFilter += allFilter;
    strFilter += (TCHAR)'\0';   // next string please
	strFilter += _T("*.*");
	strFilter += (TCHAR)'\0';   // last string
	dlgFile.m_ofn.nMaxCustFilter++;

	dlgFile.m_ofn.lpstrFilter = strFilter;
	dlgFile.m_ofn.lpstrTitle = title;
	dlgFile.m_ofn.lpstrFile = fileName.GetBuffer(_MAX_PATH);

	INT_PTR nResult = dlgFile.DoModal();
	fileName.ReleaseBuffer();

	return nResult == IDOK;
}

///////////////////////////////////////////////////////////////////////////////////
// Open an image up
///////////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGApp::OnFileOpenimage()
{
	CIPERegistry IPER;
	CString ImageDir = IPER.GetRegItem("Directories", "Images");
	if (ImageDir == "")
		ImageDir = "\\Images";
	// prompt the user (with all document templates)
	CString newName;
	if (!DoPromptFileName(newName, ImageDir, AFX_IDS_OPENFILE, "Image",
	  OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, TRUE, NULL))
		return; // open cancelled

	CDocument *pDoc = OpenDocumentFile(newName);
}

///////////////////////////////////////////////////////////////////////////////////
// Open a script up
///////////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGApp::OnFileOpenscript()
{
	CIPERegistry IPER;
	CString ScriptDir = IPER.GetRegItem("Directories", "Scripts");
	if (ScriptDir == "")
		ScriptDir = "\\Scripts";
	// prompt the user (with all document templates)
	CString newName;
	if (!DoPromptFileName(newName, ScriptDir, AFX_IDS_OPENFILE, "Script",
	  OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, TRUE, NULL))
		return; // open cancelled

	OpenDocumentFile(newName);
}

///////////////////////////////////////////////////////////////////////////////////
// Open an effect up
///////////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGApp::OnFileOpeneffect()
{
	CIPERegistry IPER;
    CString EffectsDir = IPER.GetRegItem("Directories", "Effects");
	if (EffectsDir == "")
		EffectsDir = "\\Effects";
	// prompt the user (with all document templates)
	CString newName;
	if (!DoPromptFileName(newName, EffectsDir, AFX_IDS_OPENFILE, "Effect",
	  OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, TRUE, NULL))
		return; // open cancelled

	OpenDocumentFile(newName);
}

///////////////////////////////////////////////////////////////////////////////////
// Quitting
///////////////////////////////////////////////////////////////////////////////////

int CIPEDEBUGApp::ExitInstance()
{
	
	CIPERegistry IPER;

	IPER.SetRegItem("Directories", "Temp", m_TempDir);
	IPER.SetRegItem("Directories", "Images", m_ImageDir);
	IPER.SetRegItem("Directories", "Scripts", m_ScriptDir);
	IPER.SetRegItem("Directories", "Effects", m_EffectsDir);
	IPER.SetRegItem("Options", "8bpp", m_loadAs8bpp ? "1" : "0");

    return CWinAppEx::ExitInstance();
}

