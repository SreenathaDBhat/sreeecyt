// IPEDEBUG.h : main header file for the IPEDEBUG application
//

#if !defined(AFX_IPEDEBUG_H__3C0CE096_82C6_4DFA_A263_C2C48324E81D__INCLUDED_)
#define AFX_IPEDEBUG_H__3C0CE096_82C6_4DFA_A263_C2C48324E81D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

// Number of files for the recently used file list
#define     NUMBER_RECENTLY_USED            0

class CIPERegistry
{
// Construction
public:
	CIPERegistry() {};
	~CIPERegistry() {};
	static CString &GetRegItem(char *key, char *subkey);
	static void SetRegItem(char *key, char *subkey, const char *value);
};

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGApp:
// See IPEDEBUG.cpp for the implementation of this class
//

class CIPEDEBUGApp : public CWinAppEx
{
public:
	CIPEDEBUGApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIPEDEBUGApp)
	public:
	virtual BOOL InitInstance();
    virtual int ExitInstance();
	//}}AFX_VIRTUAL

    BOOL DoPromptFileName(CString& fileName, CString InitDir, UINT nIDSTitle, CString Filter, DWORD lFlags, BOOL bOpenFileDialog, CDocTemplate* pTemplate);
    void AppendFilterSuffix(CString& filter, OPENFILENAME& ofn, CDocTemplate* pTemplate, CString* pstrDefaultExt);

    // Set/Get the application temporary direcory
    void GetTempDir(CString &Dir) { Dir = m_TempDir; };
    void SetTempDir(CString Dir) { m_TempDir = Dir; };
    // Set/Get the application image directory
    void GetImageDir(CString &Dir) { Dir = m_ImageDir; };
    void SetImageDir(CString Dir) { m_ImageDir = Dir; };
    // Set/Get the application script directory
    void GetScriptDir(CString &Dir) { Dir = m_ScriptDir; };
    void SetScriptDir(CString Dir) { m_ScriptDir = Dir; };
    // Set/Get the effects script directory
    void GetEffectsDir(CString &Dir) { Dir = m_EffectsDir; };
    void SetEffectsDir(CString Dir) { m_EffectsDir = Dir; };

	BOOL m_loadAs8bpp;


// Implementation
	COleTemplateServer m_server;
		// Server object for document creation
	//{{AFX_MSG(CIPEDEBUGApp)
	afx_msg void OnAppAbout();
	afx_msg void OnFileNew();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
    afx_msg void OnFileOpenimage();
    afx_msg void OnFileOpenscript();
    afx_msg void OnFileOpeneffect();

protected:
    // Directory locations
	CString					m_TempDir;
	CString					m_ImageDir;
	CString					m_ScriptDir;
    CString                 m_EffectsDir;

    // Document templates
    CMultiDocTemplate       *m_pScriptDocTemplate;
    CMultiDocTemplate       *m_pImageDocTemplate;
    CMultiDocTemplate       *m_pEffectsDocTemplate;

};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IPEDEBUG_H__3C0CE096_82C6_4DFA_A263_C2C48324E81D__INCLUDED_)
