// IPEDEBUGDoc.cpp : implementation of the CIPEDEBUGDoc class
//

#include "stdafx.h"
#include <D3D9.h>
#include "IPEDEBUG.h"
#include "imageview.h"
#include "IPEDEBUGDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGDoc

IMPLEMENT_DYNCREATE(CIPEDEBUGDoc, CDocument)

BEGIN_MESSAGE_MAP(CIPEDEBUGDoc, CDocument)
	//{{AFX_MSG_MAP(CIPEDEBUGDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CIPEDEBUGDoc, CDocument)
	//{{AFX_DISPATCH_MAP(CIPEDEBUGDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//      DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IIPEDEBUG to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {6ACB791A-13E3-49E3-A9DA-2401000B688D}
static const IID IID_IIPEDEBUG =
{ 0x6acb791a, 0x13e3, 0x49e3, { 0xa9, 0xda, 0x24, 0x1, 0x0, 0xb, 0x68, 0x8d } };

BEGIN_INTERFACE_MAP(CIPEDEBUGDoc, CDocument)
	INTERFACE_PART(CIPEDEBUGDoc, IID_IIPEDEBUG, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGDoc construction/destruction

#pragma warning(disable:4355)
CIPEDEBUGDoc::CIPEDEBUGDoc() : m_xTextBuffer(this)
{
	// TODO: add one-time construction code here

	//	Initialize LOGFONT structure
	memset(&m_lf, 0, sizeof(m_lf));
	m_lf.lfWeight = FW_NORMAL;
	m_lf.lfCharSet = ANSI_CHARSET;
	m_lf.lfOutPrecision = OUT_DEFAULT_PRECIS;
	m_lf.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	m_lf.lfQuality = DEFAULT_QUALITY;
	m_lf.lfPitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
	m_lf.lfHeight = 15;
	m_lf.lfWidth = 8;
	strcpy(m_lf.lfFaceName, "Courier New");
	EnableAutomation();
	AfxOleLockApp();
}

CIPEDEBUGDoc::~CIPEDEBUGDoc()
{
	AfxOleUnlockApp();
}

BOOL CIPEDEBUGDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
	m_xTextBuffer.InitNew();

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGDoc serialization

void CIPEDEBUGDoc::Serialize(CArchive& ar)
{
	// CEditView contains an edit control which handles all serialization
    //if (ar.IsStoring())
	//	((CRichEditView*)m_viewList.GetHead()).GetRichEditCtrl()->StreamOut();
}

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGDoc diagnostics

#ifdef _DEBUG
void CIPEDEBUGDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CIPEDEBUGDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGDoc commands

BOOL CIPEDEBUGDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	return m_xTextBuffer.LoadFromFile(lpszPathName);
}

BOOL CIPEDEBUGDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	m_xTextBuffer.SaveToFile(lpszPathName);
	return TRUE;	//	Note - we didn't call inherited member!
}

void CIPEDEBUGDoc::DeleteContents() 
{
	CDocument::DeleteContents();

	m_xTextBuffer.FreeAll();
}
