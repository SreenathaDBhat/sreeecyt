// IPEDEBUGDoc.h : interface of the CIPEDEBUGDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_IPEDEBUGDOC_H__185047DD_FCC5_4DC3_84FB_FF2251329E2D__INCLUDED_)
#define AFX_IPEDEBUGDOC_H__185047DD_FCC5_4DC3_84FB_FF2251329E2D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "scriptproc.h"
#include "CCrystalTextBuffer.h"


class CIPEDEBUGDoc : public CDocument
{
protected: // create from serialization only
	CIPEDEBUGDoc();
	DECLARE_DYNCREATE(CIPEDEBUGDoc)

// Attributes
public:

// Operations
public:
	class CSampleTextBuffer : public CCrystalTextBuffer
	{
	private:
		CIPEDEBUGDoc *m_pOwnerDoc;
	public:
		CSampleTextBuffer(CIPEDEBUGDoc *pDoc) { m_pOwnerDoc = pDoc; };
		virtual void SetModified(BOOL bModified = TRUE)
			{ m_pOwnerDoc->SetModifiedFlag(bModified); };
	};

	CSampleTextBuffer m_xTextBuffer;
	LOGFONT m_lf;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIPEDEBUGDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	virtual void DeleteContents();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIPEDEBUGDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif


protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CIPEDEBUGDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CIPEDEBUGDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IPEDEBUGDOC_H__185047DD_FCC5_4DC3_84FB_FF2251329E2D__INCLUDED_)
