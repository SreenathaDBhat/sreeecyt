// IPEDEBUGDoc.cpp : implementation of the CIPEDEBUGFxDoc class
//

#include "stdafx.h"
#include <D3D9.h>
#include "IPEDEBUG.h"
#include "imageview.h"
#include "IPEDEBUGFxDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGFxDoc

IMPLEMENT_DYNCREATE(CIPEDEBUGFxDoc, CDocument)

BEGIN_MESSAGE_MAP(CIPEDEBUGFxDoc, CDocument)
	//{{AFX_MSG_MAP(CIPEDEBUGFxDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CIPEDEBUGFxDoc, CDocument)
	//{{AFX_DISPATCH_MAP(CIPEDEBUGFxDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//      DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IIPEDEBUG to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {6ACB791A-13E3-49E3-A9DA-2401000B688E}
static const IID IID_IIPEDEBUGFX =
{ 0x6acb791a, 0x13e3, 0x49e3, { 0xa9, 0xda, 0x24, 0x1, 0x0, 0xb, 0x68, 0x8e } };

BEGIN_INTERFACE_MAP(CIPEDEBUGFxDoc, CDocument)
	INTERFACE_PART(CIPEDEBUGFxDoc, IID_IIPEDEBUGFX, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGFxDoc construction/destruction

#pragma warning(disable:4355)
CIPEDEBUGFxDoc::CIPEDEBUGFxDoc() : m_xTextBuffer(this)
{
	// TODO: add one-time construction code here

	//	Initialize LOGFONT structure
	memset(&m_lf, 0, sizeof(m_lf));
	m_lf.lfWeight = FW_NORMAL;
	m_lf.lfCharSet = ANSI_CHARSET;
	m_lf.lfOutPrecision = OUT_DEFAULT_PRECIS;
	m_lf.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	m_lf.lfQuality = DEFAULT_QUALITY;
	m_lf.lfPitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
	m_lf.lfHeight = 15;
	m_lf.lfWidth = 8;
	strcpy(m_lf.lfFaceName, "Courier New");
	EnableAutomation();
	AfxOleLockApp();
}

CIPEDEBUGFxDoc::~CIPEDEBUGFxDoc()
{
	AfxOleUnlockApp();
}

BOOL CIPEDEBUGFxDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
	m_xTextBuffer.InitNew();

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGFxDoc serialization

void CIPEDEBUGFxDoc::Serialize(CArchive& ar)
{
	// CEditView contains an edit control which handles all serialization
    //if (ar.IsStoring())
	//	((CRichEditView*)m_viewList.GetHead()).GetRichEditCtrl()->StreamOut();
}

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGFxDoc diagnostics

#ifdef _DEBUG
void CIPEDEBUGFxDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CIPEDEBUGFxDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGFxDoc commands

BOOL CIPEDEBUGFxDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	return m_xTextBuffer.LoadFromFile(lpszPathName);
}

BOOL CIPEDEBUGFxDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	m_xTextBuffer.SaveToFile(lpszPathName);
	return TRUE;	//	Note - we didn't call inherited member!
}

void CIPEDEBUGFxDoc::DeleteContents() 
{
	CDocument::DeleteContents();

	m_xTextBuffer.FreeAll();
}
