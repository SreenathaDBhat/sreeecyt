// IPEDEBUGDoc.h : interface of the CIPEDEBUGFxDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_IPEDEBUGFXDOC_H__185047DD_FCC5_4DC3_84FB_FF2251329EFD__INCLUDED_)
#define AFX_IPEDEBUGFXDOC_H__185047DD_FCC5_4DC3_84FB_FF2251329EFD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "scriptproc.h"
#include "CCrystalTextBuffer.h"


class CIPEDEBUGFxDoc : public CDocument
{
protected: // create from serialization only
	CIPEDEBUGFxDoc();
	DECLARE_DYNCREATE(CIPEDEBUGFxDoc)

// Attributes
public:

// Operations
public:
	class CSampleTextBuffer : public CCrystalTextBuffer
	{
	private:
		CIPEDEBUGFxDoc *m_pOwnerDoc;
	public:
		CSampleTextBuffer(CIPEDEBUGFxDoc *pDoc) { m_pOwnerDoc = pDoc; };
		virtual void SetModified(BOOL bModified = TRUE)
			{ m_pOwnerDoc->SetModifiedFlag(bModified); };
	};

	CSampleTextBuffer m_xTextBuffer;
	LOGFONT m_lf;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIPEDEBUGFxDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	virtual void DeleteContents();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIPEDEBUGFxDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif


protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CIPEDEBUGFxDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CIPEDEBUGFxDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IPEDEBUGDOC_H__185047DD_FCC5_4DC3_84FB_FF2251329E2D__INCLUDED_)
