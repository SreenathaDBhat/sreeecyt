// IPEDEBUGView.cpp : implementation of the CIPEDEBUGFxView class
//
/////////////////////////////////////////////////////////////////////////////
// Implements the script view editing environment
//
// Written By Karl Ratcliff
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include "IPEDEBUG.h"
#include "strdlg.h"
#include "longdlg.h"
#include "MainFrm.h"
#include "IPEDEBUGFxDoc.h"
#include "IPEDEBUGFxView.h"
#include "ccrystaleditview.h"
#include "cfindtextdlg.h"
#include "persistentsdlg.h"
#include "cmeasurement.h"
#include "measurementdlg.h"
#include <direct.h>
#include <stdio.h>
#include <stdlib.h>
#include ".\ipedebugfxview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGFxView

IMPLEMENT_DYNCREATE(CIPEDEBUGFxView, CCrystalEditView)

BEGIN_MESSAGE_MAP(CIPEDEBUGFxView, CCrystalEditView)
	//{{AFX_MSG_MAP(CIPEDEBUGFxView)
	ON_COMMAND(IDS_EDIT_UNDO, OnEditUndo)
	ON_COMMAND(ID_HELP_VBSCRIPTHELP, OnHelpVbscripthelp)
	ON_WM_KEYDOWN()
	ON_UPDATE_COMMAND_UI(ID_SCRIPT_MODE_DEBUG, OnUpdateScriptModeDebug)
	ON_UPDATE_COMMAND_UI(ID_SCRIPT_MODE_INTERACTIVE, OnUpdateScriptModeInteractive)
	ON_UPDATE_COMMAND_UI(ID_SCRIPT_MODE_PLAY, OnUpdateScriptModePlay)
	ON_UPDATE_COMMAND_UI(ID_SCRIPT_RUN, OnUpdateScriptRun)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_SCRIPT_RUN, OnScriptRun)
	ON_COMMAND(ID_SCRIPT_MODE_DEBUG, OnScriptModeDebug)
	ON_COMMAND(ID_SCRIPT_MODE_INTERACTIVE, OnScriptModeInteractive)
	ON_COMMAND(ID_SCRIPT_MODE_PLAY, OnScriptModePlay)
	ON_COMMAND(IDI_SCRIPT_RUN, OnScriptRun)
    ON_COMMAND(IDC_RUNBUTTON, OnScriptRun)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CCrystalEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CCrystalEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CCrystalEditView::OnFilePrintPreview)
    ON_WM_ACTIVATE()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGFxView construction/destruction

CIPEDEBUGFxView::CIPEDEBUGFxView()
{
    m_ExecMode = modeDebug;
}

CIPEDEBUGFxView::~CIPEDEBUGFxView()
{
}

BOOL CIPEDEBUGFxView::PreCreateWindow(CREATESTRUCT& cs)
{
	BOOL bPreCreated = CCrystalEditView::PreCreateWindow(cs);
	cs.style &= ~(ES_AUTOHSCROLL|WS_HSCROLL);	// Enable word-wrapping

	return bPreCreated;
}

CCrystalTextBuffer *CIPEDEBUGFxView::LocateTextBuffer()
{
	return &GetDocument()->m_xTextBuffer;
}
    
/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGFxView printing

BOOL CIPEDEBUGFxView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default CEditView preparation
	return CCrystalEditView::OnPreparePrinting(pInfo);
}

void CIPEDEBUGFxView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// Default CEditView begin printing.
	CCrystalEditView::OnBeginPrinting(pDC, pInfo);
}

void CIPEDEBUGFxView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// Default CEditView end printing
	CCrystalEditView::OnEndPrinting(pDC, pInfo);
}

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGFxView diagnostics

#ifdef _DEBUG
void CIPEDEBUGFxView::AssertValid() const
{
	CCrystalEditView::AssertValid();
}

void CIPEDEBUGFxView::Dump(CDumpContext& dc) const
{
	CCrystalEditView::Dump(dc);
}

CIPEDEBUGFxDoc* CIPEDEBUGFxView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CIPEDEBUGFxDoc)));
	return (CIPEDEBUGFxDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGFxView message handlers

/////////////////////////////////////////////////////////////////////////////
// Initialise the view
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGFxView::OnInitialUpdate() 
{
	CCrystalEditView::OnInitialUpdate();
	SetFont(GetDocument()->m_lf);
}

/////////////////////////////////////////////////////////////////////////////
// Run a script
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGFxView::OnScriptRun() 
{
    CMainFrame *pMainWnd;

    pMainWnd = (CMainFrame *) AfxGetMainWnd();
}

/////////////////////////////////////////////////////////////////////////////
// Set the script mode to DEBUG                          
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGFxView::OnScriptModeDebug() 
{
    m_ExecMode = modeDebug;

	CMenu *pMenu;

	pMenu = AfxGetMainWnd()->GetMenu();
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_DEBUG,		 MF_CHECKED);
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_INTERACTIVE, MF_UNCHECKED);
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_PLAY,		 MF_UNCHECKED);
    //SetFrameText(POS_SCRIPTMODE, "DBG");
}

/////////////////////////////////////////////////////////////////////////////
// Set the script mode to INTERACTIVE
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGFxView::OnScriptModeInteractive() 
{
    m_ExecMode = modeInteractive;
	CMenu *pMenu;

	pMenu = AfxGetMainWnd()->GetMenu();
 	pMenu->CheckMenuItem(ID_SCRIPT_MODE_DEBUG,		 MF_UNCHECKED);
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_INTERACTIVE, MF_CHECKED);
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_PLAY,		 MF_UNCHECKED);
   //SetFrameText(POS_SCRIPTMODE, "INT");
}

/////////////////////////////////////////////////////////////////////////////
// Set the script mode to PLAY
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGFxView::OnScriptModePlay() 
{
    m_ExecMode = modePlay;
	CMenu *pMenu;

	pMenu = AfxGetMainWnd()->GetMenu();
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_DEBUG,		 MF_UNCHECKED);
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_INTERACTIVE, MF_UNCHECKED);
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_PLAY,		 MF_CHECKED);
    //SetFrameText(POS_SCRIPTMODE, "PLY");
}



void CIPEDEBUGFxView::OnEditUndo() 
{
	if (m_pTextBuffer != NULL && m_pTextBuffer->CanUndo())
	{
		CPoint ptCursorPos;
		if (m_pTextBuffer->Undo(ptCursorPos))
		{
			ASSERT_VALIDTEXTPOS(ptCursorPos);
			SetAnchor(ptCursorPos);
			SetSelection(ptCursorPos, ptCursorPos);
			SetCursorPos(ptCursorPos);
			EnsureVisible(ptCursorPos);
		}
	}
}


void CIPEDEBUGFxView::OnHelpVbscripthelp() 
{
	TCHAR szDir[MAX_PATH] = "";
	// Get directory of application
	DWORD dw = GetModuleFileName(AfxGetInstanceHandle(), szDir, MAX_PATH);
	TCHAR* pchEnd = _tcsrchr(szDir, '\\') + 1;
	ASSERT_POINTER(pchEnd, TCHAR);
	*pchEnd = '\0';
	// Append subfolder name
	//_tcscat(szDir, _T("Help"));
	// Launch topic
	HINSTANCE hinst = ShellExecute(NULL, //no parent hwnd
		NULL, // open
		"VBS55.CHM", // topic file or URL
		NULL, // no parameters
		szDir, // folder containing file
		SW_SHOWNORMAL); // yes, show it
}

void CIPEDEBUGFxView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CCrystalEditView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CIPEDEBUGFxView::OnUpdateScriptModeDebug(CCmdUI* pCmdUI) 
{
    //pCmdUI->SetCheck(TRUE);
}

void CIPEDEBUGFxView::OnUpdateScriptModeInteractive(CCmdUI* pCmdUI) 
{
    //pCmdUI->SetCheck(TRUE);
}

void CIPEDEBUGFxView::OnUpdateScriptModePlay(CCmdUI* pCmdUI) 
{
    //pCmdUI->SetCheck(TRUE);
}

void CIPEDEBUGFxView::OnUpdateScriptRun(CCmdUI* pCmdUI) 
{
	//pCmdUI->Enable(FALSE);
}



void CIPEDEBUGFxView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView)
{
    // TODO: Add your specialized code here and/or call the base class
    TRACE0("ACTIVE FX VIEW");
    CCrystalEditView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}
