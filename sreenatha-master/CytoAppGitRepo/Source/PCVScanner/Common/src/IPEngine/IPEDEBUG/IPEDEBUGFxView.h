// IPEDEBUGView.h : interface of the CIPEDEBUGFxView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_IPEDEBUGFXVIEW_H__158BF562_5412_4B35_8BB9_D6C5066E01CE__INCLUDED_)
#define AFX_IPEDEBUGFXVIEW_H__158BF562_5412_4B35_8BB9_D6C5066E01CE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "stdafx.h"
#include "scriptproc.h"
#include "mainfrm.h"
#include "CCrystalEditView.h"
#include "IPEDEBUGFxDoc.h"


class CIPEDEBUGFxView : public CCrystalEditView
{
protected: // create from serialization only
	CIPEDEBUGFxView();
	DECLARE_DYNCREATE(CIPEDEBUGFxView)

protected:
	virtual DWORD ParseLine(DWORD dwCookie, int nLineIndex, TEXTBLOCK *pBuf, int &nActualItems);

// Attributes
public:
    void SetLineCol(CPoint point);
    void SetEditFont();
    //void SetFrameText(StatusBarIndicatorPos nIndex, LPCTSTR Text);
	virtual CCrystalTextBuffer *LocateTextBuffer();
	CIPEDEBUGFxDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIPEDEBUGFxView)
	public:
	//virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL
	afx_msg void OnScriptRun();
	afx_msg void OnScriptModeDebug();
	afx_msg void OnScriptModeInteractive();
	afx_msg void OnScriptModePlay();

// Implementation
public:
	virtual ~CIPEDEBUGFxView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

    // Mode to run the script in - default to debug mode
    ScriptExecMode m_ExecMode;

// Generated message map functions
protected:
	//{{AFX_MSG(CIPEDEBUGFxView)
	afx_msg void OnEditUndo();
	afx_msg void OnHelpVbscripthelp();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnUpdateScriptModeDebug(CCmdUI* pCmdUI);
	afx_msg void OnUpdateScriptModeInteractive(CCmdUI* pCmdUI);
	afx_msg void OnUpdateScriptModePlay(CCmdUI* pCmdUI);
	afx_msg void OnUpdateScriptRun(CCmdUI* pCmdUI);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnOutputqueueGetmeasurement();
protected:
    virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
};

#ifndef _DEBUG  // debug version in IPEDEBUGView.cpp
inline CIPEDEBUGFxDoc* CIPEDEBUGFxView::GetDocument()
   { return (CIPEDEBUGFxDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IPEDEBUGVIEW_H__158BF562_5412_4B35_8BB9_D6C5066E01CD__INCLUDED_)
