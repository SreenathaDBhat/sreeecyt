// IPEDEBUGView.cpp : implementation of the CIPEDEBUGView class
//
/////////////////////////////////////////////////////////////////////////////
// Implements the script view editing environment
//
// Written By Karl Ratcliff
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include "IPEDEBUG.h"
#include "strdlg.h"
#include "longdlg.h"
#include "Shlwapi.h"
#include "vcutsfile.h"
#include "hrtimer.h"
#include "vcmemrect.h"
#include "CMeasurement.h"
#include <hrtimer.h>
#include "BatchDlg.h"
#include "MainFrm.h"
#include "IPEDEBUGDoc.h"
#include "IPEDEBUGView.h"
#include "ccrystaleditview.h"
#include "cfindtextdlg.h"
#include "persistentsdlg.h"
#include "cmeasurement.h"
#include "IPEXMLReaderWriter.h"
#include "measurementdlg.h"
#include "ScriptThread.h"
#include "ScriptThreadPool.h"
#include "GetFolder.h"
#include "BatchMode.h"
#include <direct.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



CScriptThreadPool *pThreadPool;
HANDLE hTrawler;
HANDLE hResults;
CHRTimer GHRT;
CWaitCursor *pWC;

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGView

IMPLEMENT_DYNCREATE(CIPEDEBUGView, CCrystalEditView)

BEGIN_MESSAGE_MAP(CIPEDEBUGView, CCrystalEditView)
	//{{AFX_MSG_MAP(CIPEDEBUGView)
	ON_COMMAND(IDS_EDIT_UNDO, OnEditUndo)
	ON_COMMAND(ID_HELP_VBSCRIPTHELP, OnHelpVbscripthelp)
	ON_WM_KEYDOWN()
	ON_UPDATE_COMMAND_UI(ID_SCRIPT_MODE_DEBUG, OnUpdateScriptModeDebug)
	ON_UPDATE_COMMAND_UI(ID_SCRIPT_MODE_INTERACTIVE, OnUpdateScriptModeInteractive)
	ON_UPDATE_COMMAND_UI(ID_SCRIPT_MODE_PLAY, OnUpdateScriptModePlay)
	ON_UPDATE_COMMAND_UI(ID_SCRIPT_RUN, OnUpdateScriptRun)
	ON_WM_SHOWWINDOW()
	ON_COMMAND(ID_SCRIPT_PERSISTENTS_EDIT, OnScriptPersistentsEdit)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_SCRIPT_RUN, OnScriptRun)
	ON_COMMAND(ID_SCRIPT_INPUTQUEUE_PUTLONG, OnScriptInputqueuePutlong)
	ON_COMMAND(ID_SCRIPT_INPUTQUEUE_PUTSTRING, OnScriptInputqueuePutstring)
	ON_COMMAND(ID_SCRIPT_OUTPUTQUEUE_GETSTRING, OnScriptOutputqueueGetstring)
	ON_COMMAND(ID_SCRIPT_OUTPUTQUEUE_GETLONG, OnScriptOutputqueueGetlong)
	ON_COMMAND(ID_SCRIPT_OUTPUTQUEUE_GETDOUBLE, OnScriptOutputqueueGetdouble)
	ON_COMMAND(ID_SCRIPT_INPUTQUEUE_DOUBLE, OnScriptInputqueueDouble)
	ON_COMMAND(ID_SCRIPT_MODE_DEBUG, OnScriptModeDebug)
	ON_COMMAND(ID_SCRIPT_MODE_INTERACTIVE, OnScriptModeInteractive)
	ON_COMMAND(ID_SCRIPT_MODE_PLAY, OnScriptModePlay)
	ON_COMMAND(ID_SCRIPT_FREEALL, OnScriptFreeall)
	ON_COMMAND(ID_SCRIPT_RESETPERSISTENTS, OnScriptResetpersistents)
	ON_COMMAND(IDI_SCRIPT_RUN, OnScriptRun)
    ON_COMMAND(IDC_RUNBUTTON, OnScriptRun)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CCrystalEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CCrystalEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CCrystalEditView::OnFilePrintPreview)
    ON_COMMAND(ID_OUTPUTQUEUE_GETMEASUREMENT, OnOutputqueueGetmeasurement)
	ON_COMMAND(ID_SCRIPT_BATCHMODE, &CIPEDEBUGView::OnScriptBatchmode)
	ON_MESSAGE(WM_BATCHCOMPLETE, &CIPEDEBUGView::OnBatchModeComplete)
	ON_COMMAND(ID_SCRIPT_PARSE, &CIPEDEBUGView::OnScriptParse)
END_MESSAGE_MAP()

#if 0
BEGIN_INTERFACE_MAP(CIPEDEBUGView, CCrystalEditView)
	INTERFACE_PART(CIPEDEBUGView, IID_IActiveScriptSiteWindow, ActiveScriptSiteWindow)
	INTERFACE_PART(CIPEDEBUGView, IID_IActiveScriptSiteDebug, ActiveScriptSiteDebug)
	INTERFACE_PART(CIPEDEBUGView, IID_IDebugDocumentHost, DebugDocumentHost)	
END_INTERFACE_MAP()
#endif


/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGView construction/destruction

CIPEDEBUGView::CIPEDEBUGView()
{
    m_ExecMode = modeDebug;
	//m_pDebugDocHelper = NULL;
	EnableAutomation();
}

CIPEDEBUGView::~CIPEDEBUGView()
{
#if 0
	// XX ActiveX Scripting Debugging XX
   if (m_pDebugApp)
   {
      m_pDebugApp->Release();
      m_pDebugApp = NULL;
   }
   if (m_pdm)
   {
      m_pdm->Release();
      m_pdm = NULL;
   }
#endif

}

void CIPEDEBUGView::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	OnFinalRelease();
}

BOOL CIPEDEBUGView::PreCreateWindow(CREATESTRUCT& cs)
{
	BOOL bPreCreated = CCrystalEditView::PreCreateWindow(cs);
	cs.style &= ~(ES_AUTOHSCROLL|WS_HSCROLL);	// Enable word-wrapping

	return bPreCreated;
}

CCrystalTextBuffer *CIPEDEBUGView::LocateTextBuffer()
{
	return &GetDocument()->m_xTextBuffer;
}

#if 0
void CIPEDEBUGView::CreatePDM()
{
	// Debugging 
	HRESULT hr = CoCreateInstance(CLSID_ProcessDebugManager, 
		NULL, 
		CLSCTX_INPROC_SERVER | CLSCTX_INPROC_HANDLER | CLSCTX_LOCAL_SERVER,
		IID_IProcessDebugManager,
		(void**)&m_pdm);
	if (!SUCCEEDED(hr))
	{
		AfxMessageBox(_T("Unable to initialise scripting control"));
		TRACE(_T("Unable to initialise scripting control\n"));

	}

	ASSERT(m_pdm);

	//hr = m_pdm->GetDefaultApplication(&m_pDebugApp);
	// Initialise the application
	hr = m_pdm->CreateApplication(&m_pDebugApp);
	if (!SUCCEEDED(hr))
	{
		AfxMessageBox(_T("Unable to initialise scripting control"));
		TRACE(_T("Unable to initialise scripting control\n"));
	}

	USES_CONVERSION;
	ASSERT(m_pDebugApp);
	hr = m_pDebugApp->SetName(A2COLE("IPEDebug"));
	if (!SUCCEEDED(hr))
	{
		AfxMessageBox(_T("Unable to initialise scripting control"));
		TRACE(_T("Unable to initialise scripting control\n"));

	}

	hr = m_pdm->AddApplication(m_pDebugApp, &m_dwAppCookie);
	if (!SUCCEEDED(hr))
	{
		AfxMessageBox(_T("Unable to initialise scripting control"));
		TRACE(_T("Unable to initialise scripting control\n"));

	}

	hr = m_pDebugApp->StartDebugSession();
	if (FAILED(hr))
	{
		AfxMessageBox(_T("Unable to start external script debugger"));
		TRACE(_T("Failed to start debugger session\n"));

	}


	m_pDebugDocHelper = NULL;
}
#endif

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGView printing

BOOL CIPEDEBUGView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default CEditView preparation
	return CCrystalEditView::OnPreparePrinting(pInfo);
}

void CIPEDEBUGView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// Default CEditView begin printing.
	CCrystalEditView::OnBeginPrinting(pDC, pInfo);
}

void CIPEDEBUGView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// Default CEditView end printing
	CCrystalEditView::OnEndPrinting(pDC, pInfo);
}

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGView diagnostics

#ifdef _DEBUG
void CIPEDEBUGView::AssertValid() const
{
	CCrystalEditView::AssertValid();
}

void CIPEDEBUGView::Dump(CDumpContext& dc) const
{
	CCrystalEditView::Dump(dc);
}

CIPEDEBUGDoc* CIPEDEBUGView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CIPEDEBUGDoc)));
	return (CIPEDEBUGDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIPEDEBUGView message handlers

/////////////////////////////////////////////////////////////////////////////
// Initialise the view
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGView::OnInitialUpdate() 
{
	CCrystalEditView::OnInitialUpdate();
	SetFont(GetDocument()->m_lf);

	this->SetViewTabs(FALSE);
	this->ShowCursor();
}

/////////////////////////////////////////////////////////////////////////////
// Parse a script for compilation errors
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGView::OnScriptParse()
{
    CMainFrame *pMainWnd;
    CRect rcPane;
    CScriptProcessor *pSP;
    CIPERegistry IPE;
    CString TempDir;

    TempDir = IPE.GetRegItem("Directories", "Temp");
    if (TempDir == "")
        TempDir == "\\Temp";

    pMainWnd = (CMainFrame *) AfxGetMainWnd();

    // Got a processor so run it
    if (pMainWnd->m_pScriptProcessor)
    {
        pSP = pMainWnd->m_pScriptProcessor;

        // Set the script exec mode
        pSP->SetExecutionMode(m_ExecMode);

        CString pathandfile;
        CWaitCursor Wait;
        CString msg;
        double et;
        CIPEDEBUGDoc *pDoc;
        CString DocPath, DirBit;
        int SlashPos;
        BOOL Result;
        CHRTimer HRT;

		CIPEDEBUGApp *pApp = (CIPEDEBUGApp *)AfxGetApp();
		CString TempDir;
		pApp->GetTempDir(TempDir);
		// Make a working temp dir
		_mkdir(TempDir);

        pSP->ReleaseScriptProcessor();
        pSP->InitialiseScriptProcessor("IPEVARS.DAT", TempDir, "IPEIN", "IPEOUT");
        // Get the document
        pDoc = GetDocument();
        // Get the path where the document resides
        DocPath = pDoc->GetPathName();
        // Strip the filename if there is a path
        SlashPos = DocPath.ReverseFind('\\');
        if (SlashPos != -1)
        {
            DirBit = DocPath.Left(SlashPos);
            // Now make sure operating from this directory in order to get any includes
            _chdir(DirBit);
        }
        else
            DirBit = "";

        pathandfile.Format("%s\\TEMP.TMP", DirBit);
        // Save the script to a temp file
        pDoc->m_xTextBuffer.SaveToFile(pathandfile, CRLF_STYLE_AUTOMATIC, FALSE);
        // Now load it back in 
        pSP->LoadScript(pathandfile, TRUE);
        // Now load the persistent vars
        pSP->LoadPersistents();

        // Time total execution 
        HRT.Start();
        // Run it        
        Result = pSP->ParseScript();
        // Calc time elapsed
        HRT.ElapsedTime(&et);

        // Output the total time script was run
        if (Result)
        {
            msg.Format("Script took %10.6lf millisecs to execute", et * 1000.0);
            ::MessageBox(NULL, msg, "End of script", MB_OK);
        }

        // Commit any modified persistents to disk
        pSP->CommitPersistents();

        // Dispose of the temp file
        CFile TempFile;
        CFileException fe;
        if (TempFile.Open(pathandfile, CFile::modeRead, &fe)) 
        {
            TempFile.Close();
            TempFile.Remove(pathandfile);
        }

		//CreatePDM();
    }
}


/////////////////////////////////////////////////////////////////////////////
// Run a script
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGView::OnScriptRun() 
{
    CMainFrame *pMainWnd;
    CRect rcPane;
    CScriptProcessor *pSP;
    CIPERegistry IPE;
    CString TempDir;
	//HRESULT hr;

	CoInitialize(NULL);

	OleInitClass OC;


    TempDir = IPE.GetRegItem("Directories", "Temp");
    if (TempDir == "")
        TempDir == "\\Temp";

    pMainWnd = (CMainFrame *) AfxGetMainWnd();
	

	//if (m_pDebugDocHelper)
 //   {
 //       m_pDebugDocHelper->Detach();
 //       m_pDebugDocHelper->Release();
 //       m_pDebugDocHelper = NULL;
 //   }

	
    // Got a processor so run it
    if (pMainWnd->m_pScriptProcessor)
    {
        pSP = pMainWnd->m_pScriptProcessor;

        // Set the script exec mode
        pSP->SetExecutionMode(m_ExecMode);

        
        CWaitCursor Wait;
        CString msg;
        double et;
        CIPEDEBUGDoc *pDoc;
        CString DocPath, DirBit;
        int SlashPos;
        BOOL Result;
        CHRTimer HRT;

		CIPEDEBUGApp *pApp = (CIPEDEBUGApp *)AfxGetApp();
		CString TempDir;
		pApp->GetTempDir(TempDir);
		// Make a working temp dir
		_mkdir(TempDir);

        pSP->ReleaseScriptProcessor();
        pSP->InitialiseScriptProcessor("IPEVARS.DAT", TempDir, "IPEIN", "IPEOUT");
        // Get the document
        pDoc = GetDocument();
        // Get the path where the document resides
        DocPath = pDoc->GetPathName();
        // Strip the filename if there is a path
        SlashPos = DocPath.ReverseFind('\\');
        if (SlashPos != -1)
        {
            DirBit = DocPath.Left(SlashPos);
            // Now make sure operating from this directory in order to get any includes
            _chdir(DirBit);
        }
        else
            DirBit = "";

        m_srcPathName.Format("%s\\TEMP.SCRIPT", DirBit);
        // Save the script to a temp file
        pDoc->m_xTextBuffer.SaveToFile(m_srcPathName, CRLF_STYLE_AUTOMATIC, FALSE);
        // Now load it back in 
        pSP->LoadScript(m_srcPathName, TRUE);


        // Now load the persistent vars
        pSP->LoadPersistents();

		//DWORD dwSrcContext;
		//CString Script;
		//pSP->GetScript(Script);

		//

		//
		//hr = m_pdm->CreateDebugDocumentHelper(NULL, &m_pDebugDocHelper);
		//if (hr != S_OK)
		//{
		//	TRACE(_T("Failed to create debug document helper.\n"));
		//}
		//
		//USES_CONVERSION;

		//hr = m_pDebugDocHelper->Init(m_pDebugApp, L"Current Text", L"MFC Scripted Current Text", TEXT_DOC_ATTR_READONLY);
		//if (FAILED(hr))
		//{
		//	TRACE(_T("Failed to create debug document.\n"));
		//	
		//}

		//hr = m_pDebugDocHelper->Attach(NULL);
		//if (FAILED(hr))
		//{
		//	TRACE(_T("Failed to create debug document.\n"));
		//	
		//}

		//hr = m_pDebugDocHelper->SetDocumentAttr(TEXT_DOC_ATTR_READONLY);
		//if (FAILED(hr))
		//{
		//	TRACE(_T("Failed to create debug document.\n"));
		//	
		//}


		//// this step is optional but it deosn't require much effort to perform
	 // hr = m_pDebugDocHelper->SetDebugDocumentHost(&(this->m_xDebugDocumentHost));
	 //  if (FAILED(hr))
	 //  {
  //  		TRACE(_T("Failed to create debug document.\n"));			
	 //  }

        // Time total execution 
        HRT.Start();
        
		//
		//
		//hr = m_pDebugDocHelper->AddDBCSText(Script);
		//if (FAILED(hr)) 
		//	TRACE(_T("FAILED ADDDBCSTEXT\r\n"));

		//hr = m_pDebugDocHelper->DefineScriptBlock(0, Script.GetLength(), pSP->ActiveScp(), TRUE, &dwSrcContext);
		//if (FAILED(hr)) 
		//	TRACE(_T("DEFINE SCRIPT BLOCK\r\n"));
		//

		//Result = pSP->DebugScript(dwSrcContext);

		//hr = m_pDebugDocHelper->BringDocumentToTop();
		//if (FAILED(hr)) 
		//	TRACE(_T("BringDocumentToTop\r\n"));
		// 
		//hr = m_pDebugApp->CauseBreak();
		//pSP->DeferredExecute();

		Result = pSP->ExecuteScript();
        // Calc time elapsed
        HRT.ElapsedTime(&et);

        // Output the total time script was run
        if (Result)
        {
            msg.Format("Script took %10.6lf millisecs to execute", et * 1000.0);
            ::MessageBox(NULL, msg, "End of script", MB_OK);
        }

        // Commit any modified persistents to disk
        pSP->CommitPersistents();

        // Dispose of the temp file
        CFile TempFile;
        CFileException fe;
        if (TempFile.Open(m_srcPathName, CFile::modeRead, &fe)) 
        {
            TempFile.Close();
            TempFile.Remove(m_srcPathName);
        }
    }

	CoUninitialize();
}


/////////////////////////////////////////////////////////////////////////////
// Show an input dialog and put a long on the input Q
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGView::OnScriptInputqueuePutlong() 
{
    LongDlg dlg;
    CMainFrame *pMainWnd;

    pMainWnd = (CMainFrame *) AfxGetMainWnd();
    
    if (dlg.DoModal() == IDOK)
        pMainWnd->m_pScriptProcessor->PutLong(dlg.m_LongValue);
}

/////////////////////////////////////////////////////////////////////////////
// Show an input dialog and put a string on the input Q
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGView::OnScriptInputqueuePutstring() 
{
    CStrDlg	dlg;
    CMainFrame *pMainWnd;

    pMainWnd = (CMainFrame *) AfxGetMainWnd();

    if (dlg.DoModal() == IDOK)
        pMainWnd->m_pScriptProcessor->PutString(dlg.m_StringValue);
}

/////////////////////////////////////////////////////////////////////////////
// Get a string from the script output Q and show it
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGView::OnScriptOutputqueueGetstring() 
{
    CString TheString;

    CMainFrame *pMainWnd;

    pMainWnd = (CMainFrame *) AfxGetMainWnd();
    // Get the value
    if (!pMainWnd->m_pScriptProcessor->GetString(TheString))
        MessageBox("Error getting string from Q", "Help !",  MB_OK | MB_ICONEXCLAMATION);
    else
        MessageBox(TheString, "Get String Returned", MB_OK);
}

/////////////////////////////////////////////////////////////////////////////
// Get a long from the script output Q and show it
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGView::OnScriptOutputqueueGetlong() 
{
    long LongValue;
    CMainFrame *pMainWnd;

    pMainWnd = (CMainFrame *) AfxGetMainWnd();

    if (!pMainWnd->m_pScriptProcessor->GetLong(&LongValue))
        MessageBox("Error getting from Q", "Help !",  MB_OK | MB_ICONEXCLAMATION);
    else
    {
        CString TheString;
        
        TheString.Format("%ld", LongValue);
        MessageBox(TheString, "Get Long Returned", MB_OK);
    }	
}

void CIPEDEBUGView::OnScriptOutputqueueGetdouble() 
{
	// TODO: Add your command handler code here
	
}

void CIPEDEBUGView::OnScriptInputqueueDouble() 
{
	// TODO: Add your command handler code here
	
}

//////////////////////////////////////////////////////////////////
// Get a measurement from the output Q
//////////////////////////////////////////////////////////////////

void CIPEDEBUGView::OnOutputqueueGetmeasurement()
{
    Measurement *pValue;
    CMainFrame *pMainWnd;

    pMainWnd = (CMainFrame *) AfxGetMainWnd();

    if (!pMainWnd->m_pScriptProcessor->GetMeasurement(&pValue))
        MessageBox("Error getting from Q", "Help !",  MB_OK | MB_ICONEXCLAMATION);
    else
    {
        CMeasurementDlg MDlg(pValue);

        MDlg.DoModal();

        delete pValue;      // Clear it up
    }
}


/////////////////////////////////////////////////////////////////////////////
// Set the script mode to DEBUG                          
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGView::OnScriptModeDebug() 
{
    m_ExecMode = modeDebug;

	CMenu *pMenu;

	pMenu = AfxGetMainWnd()->GetMenu();
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_DEBUG,		 MF_CHECKED);
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_INTERACTIVE, MF_UNCHECKED);
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_PLAY,		 MF_UNCHECKED);
    //SetFrameText(POS_SCRIPTMODE, "DBG");
}

/////////////////////////////////////////////////////////////////////////////
// Set the script mode to INTERACTIVE
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGView::OnScriptModeInteractive() 
{
    m_ExecMode = modeInteractive;
	CMenu *pMenu;

	pMenu = AfxGetMainWnd()->GetMenu();
 	pMenu->CheckMenuItem(ID_SCRIPT_MODE_DEBUG,		 MF_UNCHECKED);
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_INTERACTIVE, MF_CHECKED);
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_PLAY,		 MF_UNCHECKED);
   //SetFrameText(POS_SCRIPTMODE, "INT");
}

/////////////////////////////////////////////////////////////////////////////
// Set the script mode to PLAY
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGView::OnScriptModePlay() 
{
    m_ExecMode = modePlay;
	CMenu *pMenu;

	pMenu = AfxGetMainWnd()->GetMenu();
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_DEBUG,		 MF_UNCHECKED);
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_INTERACTIVE, MF_UNCHECKED);
	pMenu->CheckMenuItem(ID_SCRIPT_MODE_PLAY,		 MF_CHECKED);
    //SetFrameText(POS_SCRIPTMODE, "PLY");
}

/////////////////////////////////////////////////////////////////////////////
// Frees all data, open images and resets the queues
/////////////////////////////////////////////////////////////////////////////

void CIPEDEBUGView::OnScriptFreeall() 
{
    CMainFrame *pMainWnd;
	BOOL Ok = TRUE;
	VBS_IMAGE_REC	Image;
	CString	cs;
	long l;
	double d;

    pMainWnd = (CMainFrame *) AfxGetMainWnd();
    if (pMainWnd->m_pScriptProcessor)
	{
		// Keep pulling stuff from Qs until nothing left
		while (Ok) 
		{
			Ok = FALSE;
			if (pMainWnd->m_pScriptProcessor->GetLong(&l))
				Ok = TRUE;
			else
			if (pMainWnd->m_pScriptProcessor->GetDouble(&d))
				Ok = TRUE;
			else
			if (pMainWnd->m_pScriptProcessor->GetImage(&Image))
			{
				Ok = TRUE;
				delete Image.Address;
			}
		}

        pMainWnd->m_pScriptProcessor->Reset();         // Reset the Q's etc.	
	}
}


void CIPEDEBUGView::OnScriptResetpersistents() 
{
    CMainFrame *pMainWnd;

    pMainWnd = (CMainFrame *) AfxGetMainWnd();
    
    // Verify we actually want to do this
    if (MessageBox("All persistent data will be lost.\r\nAre you sure", 
                   "Reset persistents", 
                   MB_ICONQUESTION | MB_YESNO) == IDYES) 
                  pMainWnd-> m_pScriptProcessor->ResetPersistentVariables();
}

void CIPEDEBUGView::OnEditUndo() 
{
	if (m_pTextBuffer != NULL && m_pTextBuffer->CanUndo())
	{
		CPoint ptCursorPos;
		if (m_pTextBuffer->Undo(ptCursorPos))
		{
			ASSERT_VALIDTEXTPOS(ptCursorPos);
			SetAnchor(ptCursorPos);
			SetSelection(ptCursorPos, ptCursorPos);
			SetCursorPos(ptCursorPos);
			EnsureVisible(ptCursorPos);
		}
	}
}

//////////////////////////////////////////////////////////////////
// Pull up the persistent variable editor/viewer dialog
//////////////////////////////////////////////////////////////////

void CIPEDEBUGView::OnScriptPersistentsEdit() 
{
	CPersistentsDlg PDlg;
	
    CMainFrame *pMainWnd;

    pMainWnd = (CMainFrame *) AfxGetMainWnd();
	pMainWnd->m_pScriptProcessor->CommitPersistents();
	PDlg.DoModal();
}

void CIPEDEBUGView::OnHelpVbscripthelp() 
{
	TCHAR szDir[MAX_PATH] = "";
	// Get directory of application
	DWORD dw = GetModuleFileName(AfxGetInstanceHandle(), szDir, MAX_PATH);
	TCHAR* pchEnd = _tcsrchr(szDir, '\\') + 1;
	ASSERT_POINTER(pchEnd, TCHAR);
	*pchEnd = '\0';
	// Help file in exe's parent folder
	_tcscat(szDir, _T(".."));

	// Launch topic
	HINSTANCE hinst = ShellExecute(NULL, //no parent hwnd
		NULL, // open
		"VBSCRIP5.CHM", // topic file or URL
		NULL, // no parameters
		szDir, // folder containing file
		SW_SHOWNORMAL); // yes, show it
}

void CIPEDEBUGView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CCrystalEditView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CIPEDEBUGView::OnUpdateScriptModeDebug(CCmdUI* pCmdUI) 
{
    //pCmdUI->SetCheck(TRUE);
}

void CIPEDEBUGView::OnUpdateScriptModeInteractive(CCmdUI* pCmdUI) 
{
    //pCmdUI->SetCheck(TRUE);
}

void CIPEDEBUGView::OnUpdateScriptModePlay(CCmdUI* pCmdUI) 
{
    //pCmdUI->SetCheck(TRUE);
}

void CIPEDEBUGView::OnUpdateScriptRun(CCmdUI* pCmdUI) 
{
	//pCmdUI->Enable(FALSE);
}


static	ThreadParams TP;

//////////////////////////////////////////////////////////////////
// Multithreaded batch processing
//////////////////////////////////////////////////////////////////

void CIPEDEBUGView::OnScriptBatchmode()
{
	CString pathandfile;
	CWaitCursor Wait;
	CString msg;
	CIPEDEBUGDoc *pDoc;
	CString DocPath, DirBit;
	int SlashPos;

	USES_CONVERSION;

	// Get the document
	pDoc = GetDocument();
	// Get the path where the document resides
	DocPath = pDoc->GetPathName();
	// Strip the filename if there is a path
	SlashPos = DocPath.ReverseFind('\\');
	if (SlashPos != -1)
	{
		DirBit = DocPath.Left(SlashPos);
		// Now make sure operating from this directory in order to get any includes
		_chdir(DirBit);
	}
	else
		DirBit = "";

	pathandfile.Format("%s\\TEMP.TMP", DirBit);
	// Save the script to a temp file
	pDoc->m_xTextBuffer.SaveToFile(pathandfile, CRLF_STYLE_AUTOMATIC, FALSE);

	CIPERegistry IPER;
	CString SrcFolderPath = IPER.GetRegItem("Directories", "SrcFolderPath");
	CString DestFolderPath = IPER.GetRegItem("Directories", "DestFolderPath");
	CString Classifier = IPER.GetRegItem("Directories", "Classifier");

	// Set some defaults if the registry is empty
	if (SrcFolderPath.IsEmpty())	SrcFolderPath = _T("\\ThreadPool");
	if (DestFolderPath.IsEmpty())	DestFolderPath = _T("\\Frags");
	if (Classifier.IsEmpty())		Classifier = _T("\\casebase\\AII_Shared\\Classifiers");

	CBatchDlg batchDlg;
	batchDlg.m_SourceImgFolder	= SrcFolderPath;
	batchDlg.m_ResultsFolder	= DestFolderPath;
	batchDlg.m_Classifier		= Classifier;

	if (batchDlg.DoModal() == IDOK)
	{
		IPER.SetRegItem("Directories", "SrcFolderPath", batchDlg.m_SourceImgFolder);
		IPER.SetRegItem("Directories", "DestFolderPath", batchDlg.m_ResultsFolder);
		IPER.SetRegItem("Directories", "Classifier", batchDlg.m_Classifier);

		_tcscpy(TP.DestFolderPath,	batchDlg.m_ResultsFolder);
		_tcscpy(TP.SrcFolderPath,	batchDlg.m_SourceImgFolder);
		_tcscpy(TP.ScriptName,		pathandfile);
			
		pWC = new CWaitCursor();

		VarMapBank  Persistents;

		// Load the defaults
		Persistents.Load(PERSISTENTVARS_FILENAME);

		//pThreadPool = new CScriptThreadPool(128);
		pThreadPool = new CScriptThreadPool(128);
		pThreadPool->Start(pathandfile, _T("IPEVARS.DAT"), &Persistents, Classifier);

		TP.pThreadPool = pThreadPool;
		TP.m_hParentWnd = this->m_hWnd;

		hTrawler = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) TrawlerThread, &TP, 0, NULL);
		hResults = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) ResultsThread, &TP, 0, NULL);
	}
}


LRESULT CIPEDEBUGView::OnBatchModeComplete(WPARAM W, LPARAM L)
{
	pThreadPool->ShutDown(120000);

	delete pThreadPool;

	delete pWC;

	MessageBox(_T("Batch complete"), _T("Information"), MB_ICONINFORMATION | MB_OK);

	return 0L;
}


#if 0

/////////////////////////////////////////////////////////////////////////////
// IActiveScriptSiteWindow Implementation

STDMETHODIMP_(ULONG) CIPEDEBUGView::XActiveScriptSiteWindow::AddRef()
{
    METHOD_PROLOGUE_EX_(CIPEDEBUGView, ActiveScriptSiteWindow)
    return pThis->ExternalAddRef();
}

STDMETHODIMP_(ULONG) CIPEDEBUGView::XActiveScriptSiteWindow::Release()
{
    METHOD_PROLOGUE_EX_(CIPEDEBUGView, ActiveScriptSiteWindow)
    return pThis->ExternalRelease();
}

STDMETHODIMP CIPEDEBUGView::XActiveScriptSiteWindow::QueryInterface(REFIID iid, LPVOID* ppvObj)
{
    METHOD_PROLOGUE_EX_(CIPEDEBUGView, ActiveScriptSiteWindow)
    return pThis->ExternalQueryInterface(&iid, ppvObj);
}

STDMETHODIMP CIPEDEBUGView::XActiveScriptSiteWindow::EnableModeless(BOOL fEnable)
{
    METHOD_PROLOGUE_EX_(CIPEDEBUGView, ActiveScriptSiteWindow)
	
	CWinApp* pApp = AfxGetApp();
	if (!pApp)
		return E_FAIL;

	pApp->EnableModeless(fEnable);
	return S_OK;
}

STDMETHODIMP CIPEDEBUGView::XActiveScriptSiteWindow::GetWindow(HWND* phWnd)
{
    METHOD_PROLOGUE_EX_(CIPEDEBUGView, ActiveScriptSiteWindow)

	CWnd* pWnd = AfxGetMainWnd();
	if (!(pWnd))
		return E_FAIL;

	*phWnd = pWnd->GetSafeHwnd();
	if (*phWnd)
		return S_OK;
	else
		return E_FAIL;
}

/////////////////////////////////////////////////////////////////////////////
// IActiveScriptSiteDebug Implementation
STDMETHODIMP_(ULONG) CIPEDEBUGView::XActiveScriptSiteDebug::AddRef()
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, ActiveScriptSiteDebug)
   return pThis->ExternalAddRef();
}

STDMETHODIMP_(ULONG) CIPEDEBUGView::XActiveScriptSiteDebug::Release()
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, ActiveScriptSiteDebug)
   return pThis->ExternalRelease();
}

STDMETHODIMP CIPEDEBUGView::XActiveScriptSiteDebug::QueryInterface(REFIID iid, LPVOID* ppvObj)
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, ActiveScriptSiteDebug)
   return pThis->ExternalQueryInterface(&iid, ppvObj);
}

// Used by the language engine to delegate IDebugCodeContext::GetSourceContext. 
STDMETHODIMP CIPEDEBUGView::XActiveScriptSiteDebug::GetDocumentContextFromPosition(
      DWORD dwSourceContext,// As provided to ParseScriptText 
                            // or AddScriptlet 
      ULONG uCharacterOffset,// character offset relative 
                             // to start of script block or scriptlet 
      ULONG uNumChars,// Number of characters in context 
                      // Returns the document context corresponding to this character-position range. 
      IDebugDocumentContext **ppsc)
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, ActiveScriptSiteDebug)

   ULONG ulStartPos = 0;
   HRESULT hr;

   if (pThis->m_pDebugDocHelper)
   {
      hr = pThis->m_pDebugDocHelper->GetScriptBlockInfo(dwSourceContext, NULL, &ulStartPos, NULL);
      hr = pThis->m_pDebugDocHelper->CreateDebugDocumentContext(ulStartPos + uCharacterOffset, uNumChars, ppsc);
   }
   else
   {
      hr = E_NOTIMPL;
   }

	return hr;
}

// Returns the debug application object associated with this script site. Provides 
// a means for a smart host to define what application object each script belongs to. 
// Script engines should attempt to call this method to get their containing application 
// and resort to IProcessDebugManager::GetDefaultApplication if this fails. 
STDMETHODIMP CIPEDEBUGView::XActiveScriptSiteDebug::GetApplication( 
      IDebugApplication **ppda)
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, ActiveScriptSiteDebug)
   if (!ppda)
   {
      return E_INVALIDARG;
   }

   // bugbug - should addref to this ?
   if (pThis->m_pDebugApp)
   {
      ULONG ul = pThis->m_pDebugApp->AddRef();
   }

   *ppda = pThis->m_pDebugApp;

	return S_OK;
}

// Gets the application node under which script documents should be added 
// can return NULL if script documents should be top-level. 
STDMETHODIMP CIPEDEBUGView::XActiveScriptSiteDebug::GetRootApplicationNode(IDebugApplicationNode **ppdanRoot)
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, ActiveScriptSiteDebug)

   if (!ppdanRoot)
   {
      return E_INVALIDARG;
   }

   if (pThis->m_pDebugDocHelper)
   {
      return pThis->m_pDebugDocHelper->GetDebugApplicationNode(ppdanRoot);
   }

   return E_NOTIMPL;
}

// Allows a smart host to control the handling of runtime errors 
STDMETHODIMP CIPEDEBUGView::XActiveScriptSiteDebug::OnScriptErrorDebug( 
      // the runtime error that occurred 
      IActiveScriptErrorDebug *pErrorDebug, 
      // whether to pass the error to the debugger to do JIT debugging 
      BOOL*pfEnterDebugger, 
      // whether to call IActiveScriptSite::OnScriptError() when the user 
      // decides to continue without debugging 
      BOOL *pfCallOnScriptErrorWhenContinuing)
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, ActiveScriptSiteDebug)
   if (pfEnterDebugger)
   {
      *pfEnterDebugger = TRUE;
   }
   if (pfCallOnScriptErrorWhenContinuing)
   {
      *pfCallOnScriptErrorWhenContinuing = TRUE;
   }
   return S_OK;
}




/////////////////////////////////////////////////////////////////////////////
// IDebugDocumentHost Implementation

// Return a particular range of characters in the original host document, 
// added using AddDeferredText. 
// 
// It is acceptable for a host to return E_NOTIMPL for this method, 
// as long as the host doesn't call AddDeferredText. 
// 
// (Note that this is text from the _original_ document. The host 
// does not need to be responsible for keeping track of edits, etc.) 
STDMETHODIMP_(ULONG) CIPEDEBUGView::XDebugDocumentHost::AddRef()
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, DebugDocumentHost)
   return pThis->ExternalAddRef();
}

STDMETHODIMP_(ULONG) CIPEDEBUGView::XDebugDocumentHost::Release()
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, DebugDocumentHost)
   return pThis->ExternalRelease();
}

STDMETHODIMP CIPEDEBUGView::XDebugDocumentHost::QueryInterface(REFIID iid, LPVOID* ppvObj)
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, DebugDocumentHost)
   return pThis->ExternalQueryInterface(&iid, ppvObj);
}

STDMETHODIMP CIPEDEBUGView::XDebugDocumentHost::GetDeferredText( 
      DWORD dwTextStartCookie, 
      // Specifies a character text buffer. NULL means do not return characters. 
      WCHAR *pcharText, 
      // Specifies a character attribute buffer. NULL means do not return attributes. 
      SOURCE_TEXT_ATTR *pstaTextAttr, 
      // Indicates the actual number of characters/attributes returned. Must be set to zero 
      // before the call. 
      ULONG *pcNumChars, 
      // Specifies the number maximum number of character desired. 
      ULONG cMaxChars) 
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, DebugDocumentHost)

   return E_NOTIMPL;
}

// Return the text attributes for an arbitrary block of document text. 
// It is acceptable for hosts to return E_NOTIMPL, in which case the 
// default attributes are used. 
STDMETHODIMP CIPEDEBUGView::XDebugDocumentHost::GetScriptTextAttributes( 
      // The script block text. This string need not be null terminated. 
      LPCOLESTR pstrCode, 
      // The number of characters in the script block text. 
      ULONG uNumCodeChars, 
      // See IActiveScriptParse::ParseScriptText for a description of this argument. 
      LPCOLESTR pstrDelimiter, 
      // See IActiveScriptParse::ParseScriptText for a description of this argument. 
      DWORD dwFlags, 
      // Buffer to contain the returned attributes. 
      SOURCE_TEXT_ATTR *pattr) 
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, DebugDocumentHost)

   return E_NOTIMPL;
}

// Notify the host that a new document context is being created 
// and allow the host to optionally return a controlling unknown 
// for the new context. 
// 
// This allows the host to add new functionality to the helper-provided 
// document contexts. It is acceptable for the host to return E_NOTIMPL 
// or a null outer unknown for this method, in which case the context is 
// used "as is". 
STDMETHODIMP CIPEDEBUGView::XDebugDocumentHost::OnCreateDocumentContext( 
      IUnknown** ppunkOuter) 
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, DebugDocumentHost)

   return E_NOTIMPL;
}

// Return the full path (including file name) to the document's source file. 
//*pfIsOriginalPath is TRUE if the path refers to the original file for the document. 
//*pfIsOriginalPath is FALSE if the path refers to a newly created temporary file 
//Returns E_FAIL if no source file can be created/determined. 
STDMETHODIMP CIPEDEBUGView::XDebugDocumentHost::GetPathName( 
      BSTR *pbstrLongName, 
      BOOL *pfIsOriginalFile)
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, DebugDocumentHost)

   *pbstrLongName = pThis->m_srcPathName.AllocSysString();
   *pfIsOriginalFile = TRUE;

   return S_OK;
}

// Return just the name of the document, with no path information. 
// (Used for "Save As...") 
STDMETHODIMP CIPEDEBUGView::XDebugDocumentHost::GetFileName( 
      BSTR *pbstrShortName) 
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, DebugDocumentHost)

   // bugbug - shorten name !
   *pbstrShortName = pThis->m_srcPathName.AllocSysString();

   return S_OK;
}

// Notify the host that the document's source file has been saved and 
// that its contents should be refreshed. 
STDMETHODIMP CIPEDEBUGView::XDebugDocumentHost::NotifyChanged()
{
   METHOD_PROLOGUE_EX_(CIPEDEBUGView, DebugDocumentHost)

   return S_OK;
}

#endif