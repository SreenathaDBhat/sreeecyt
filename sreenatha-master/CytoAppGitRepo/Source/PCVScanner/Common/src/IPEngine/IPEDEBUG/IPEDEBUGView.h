// IPEDEBUGView.h : interface of the CIPEDEBUGView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_IPEDEBUGVIEW_H__158BF562_5412_4B35_8BB9_D6C5066E01CD__INCLUDED_)
#define AFX_IPEDEBUGVIEW_H__158BF562_5412_4B35_8BB9_D6C5066E01CD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "stdafx.h"
#include "scriptproc.h"
#include "mainfrm.h"
#include "CCrystalEditView.h"
#include "IPEDEBUGDoc.h"

#include <activscp.h>
#include <activdbg.h>

class CIPEDEBUGView : public CCrystalEditView
{
protected: // create from serialization only
	CIPEDEBUGView();
	DECLARE_DYNCREATE(CIPEDEBUGView)

protected:
	virtual DWORD ParseLine(DWORD dwCookie, int nLineIndex, TEXTBLOCK *pBuf, int &nActualItems);

// Attributes
public:
    void SetLineCol(CPoint point);
    void SetEditFont();
    //void SetFrameText(StatusBarIndicatorPos nIndex, LPCTSTR Text);
	virtual CCrystalTextBuffer *LocateTextBuffer();
	CIPEDEBUGDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIPEDEBUGView)
	public:
	//virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL
	afx_msg void OnScriptRun();
	afx_msg void OnScriptInputqueuePutlong();
	afx_msg void OnScriptInputqueuePutstring();
	afx_msg void OnScriptOutputqueueGetstring();
	afx_msg void OnScriptOutputqueueGetlong();
	afx_msg void OnScriptOutputqueueGetdouble();
	afx_msg void OnScriptOutputqueueGetMeasurement();
	afx_msg void OnScriptInputqueueDouble();
	afx_msg void OnScriptModeDebug();
	afx_msg void OnScriptModeInteractive();
	afx_msg void OnScriptModePlay();
	afx_msg void OnScriptFreeall();
	afx_msg void OnScriptResetpersistents();
	afx_msg void OnScriptPersistentsEdit();
	LRESULT OnBatchModeComplete(WPARAM W, LPARAM L);

	afx_msg void OnFinalRelease();

// Implementation
public:
	virtual ~CIPEDEBUGView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//void CreatePDM();

    // Mode to run the script in - default to debug mode
    ScriptExecMode m_ExecMode;
	
#if 0
	IProcessDebugManager*	m_pdm;
	IDebugApplication*		m_pDebugApp;
	IDebugDocumentHelper*	m_pDebugDocHelper;
	DWORD					m_dwAppCookie;
#endif
	CString					m_srcPathName;

// Generated message map functions
protected:
	//{{AFX_MSG(CIPEDEBUGView)
	afx_msg void OnEditUndo();
	afx_msg void OnHelpVbscripthelp();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnUpdateScriptModeDebug(CCmdUI* pCmdUI);
	afx_msg void OnUpdateScriptModeInteractive(CCmdUI* pCmdUI);
	afx_msg void OnUpdateScriptModePlay(CCmdUI* pCmdUI);
	afx_msg void OnUpdateScriptRun(CCmdUI* pCmdUI);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	//DECLARE_INTERFACE_MAP()

public:
    afx_msg void OnOutputqueueGetmeasurement();
	afx_msg void OnScriptBatchmode();
	afx_msg void OnScriptParse();

#if 0
	BEGIN_INTERFACE_PART(ActiveScriptSiteWindow, IActiveScriptSiteWindow)
		STDMETHOD(EnableModeless)(BOOL);
		STDMETHOD(GetWindow)(HWND*);
	END_INTERFACE_PART(ActiveScriptSiteWindow)

	BEGIN_INTERFACE_PART(ActiveScriptSiteDebug, IActiveScriptSiteDebug)
		STDMETHOD(GetDocumentContextFromPosition)(DWORD, ULONG, ULONG, IDebugDocumentContext**);
		STDMETHOD(GetApplication)(IDebugApplication**);
		STDMETHOD(GetRootApplicationNode)(IDebugApplicationNode**);
      STDMETHOD(OnScriptErrorDebug)(IActiveScriptErrorDebug*, BOOL*, BOOL*);
	END_INTERFACE_PART(ActiveScriptSiteDebug)

	
	BEGIN_INTERFACE_PART(DebugDocumentHost, IDebugDocumentHost)
      STDMETHOD(GetDeferredText)(DWORD, WCHAR*, SOURCE_TEXT_ATTR*, ULONG*, ULONG); 
      STDMETHOD(GetScriptTextAttributes)(LPCOLESTR, ULONG, LPCOLESTR, DWORD, SOURCE_TEXT_ATTR*); 
      STDMETHOD(OnCreateDocumentContext)(IUnknown**); 
      STDMETHOD(GetPathName)(BSTR*, BOOL*); 
      STDMETHOD(GetFileName)(BSTR*); 
      STDMETHOD(NotifyChanged)(); 
	END_INTERFACE_PART(DebugDocumentHost)
#endif
	
};

#ifndef _DEBUG  // debug version in IPEDEBUGView.cpp
inline CIPEDEBUGDoc* CIPEDEBUGView::GetDocument()
   { return (CIPEDEBUGDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IPEDEBUGVIEW_H__158BF562_5412_4B35_8BB9_D6C5066E01CD__INCLUDED_)
