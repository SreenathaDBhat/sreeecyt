

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Thu Mar 17 13:09:06 2016
 */
/* Compiler settings for IPEDEBUG.odl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__


#ifndef __IPEDEBUG_h_h__
#define __IPEDEBUG_h_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IIPEDEBUG_FWD_DEFINED__
#define __IIPEDEBUG_FWD_DEFINED__
typedef interface IIPEDEBUG IIPEDEBUG;
#endif 	/* __IIPEDEBUG_FWD_DEFINED__ */


#ifndef __Document_FWD_DEFINED__
#define __Document_FWD_DEFINED__

#ifdef __cplusplus
typedef class Document Document;
#else
typedef struct Document Document;
#endif /* __cplusplus */

#endif 	/* __Document_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __IPEDEBUG_LIBRARY_DEFINED__
#define __IPEDEBUG_LIBRARY_DEFINED__

/* library IPEDEBUG */
/* [version][uuid] */ 


DEFINE_GUID(LIBID_IPEDEBUG,0x9EBA3454,0x8882,0x4CE4,0x8F,0x7D,0x6D,0x19,0xE6,0xDE,0xDB,0x54);

#ifndef __IIPEDEBUG_DISPINTERFACE_DEFINED__
#define __IIPEDEBUG_DISPINTERFACE_DEFINED__

/* dispinterface IIPEDEBUG */
/* [uuid] */ 


DEFINE_GUID(DIID_IIPEDEBUG,0x6ACB791A,0x13E3,0x49E3,0xA9,0xDA,0x24,0x01,0x00,0x0B,0x68,0x8D);

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("6ACB791A-13E3-49E3-A9DA-2401000B688D")
    IIPEDEBUG : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct IIPEDEBUGVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IIPEDEBUG * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IIPEDEBUG * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IIPEDEBUG * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IIPEDEBUG * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IIPEDEBUG * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IIPEDEBUG * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IIPEDEBUG * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IIPEDEBUGVtbl;

    interface IIPEDEBUG
    {
        CONST_VTBL struct IIPEDEBUGVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IIPEDEBUG_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IIPEDEBUG_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IIPEDEBUG_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IIPEDEBUG_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IIPEDEBUG_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IIPEDEBUG_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IIPEDEBUG_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* __IIPEDEBUG_DISPINTERFACE_DEFINED__ */


DEFINE_GUID(CLSID_Document,0x3DF11558,0x6FC0,0x4355,0xB9,0x0C,0xB9,0x2E,0x73,0x38,0xEF,0x34);

#ifdef __cplusplus

class DECLSPEC_UUID("3DF11558-6FC0-4355-B90C-B92E7338EF34")
Document;
#endif
#endif /* __IPEDEBUG_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


