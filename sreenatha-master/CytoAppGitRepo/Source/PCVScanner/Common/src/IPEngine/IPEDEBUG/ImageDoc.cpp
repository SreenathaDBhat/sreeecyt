// ImageDoc.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// MFC code modified for multiple file extensions
//
// Written By Karl Ratcliff 29052001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include "IPEDEBUG.h"
#include "imageview.h"
#include "ImageDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImageDoc

IMPLEMENT_DYNCREATE(CImageDoc, CDocument)

CImageDoc::CImageDoc()
{
}

BOOL CImageDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

CImageDoc::~CImageDoc()
{
}


BEGIN_MESSAGE_MAP(CImageDoc, CDocument)
	//{{AFX_MSG_MAP(CImageDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImageDoc diagnostics

#ifdef _DEBUG
void CImageDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CImageDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CImageDoc serialization

void CImageDoc::Serialize(CArchive& ar)
{
    ((CImageView*)m_viewList.GetHead())->Serialize(ar);
}

/////////////////////////////////////////////////////////////////////////////
// CImageDoc commands

/////////////////////////////////////////////////////////////////////////////
// Override the OnSaveDocument member function - the MFC version 
// opens the file exclusively and locks out the serialization - this
// version releases the file and allows file saving in a normal way
/////////////////////////////////////////////////////////////////////////////

BOOL CImageDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{

	CFileException fe;
	CFile* pFile = NULL;
	pFile = GetFile(lpszPathName, CFile::modeCreate | CFile::modeReadWrite | CFile::shareExclusive, &fe);

	if (pFile == NULL)
	{
		ReportSaveLoadException(lpszPathName, &fe, TRUE, AFX_IDP_INVALID_FILENAME);
		return FALSE;
	}
	
    CArchive saveArchive(pFile, CArchive::store | CArchive::bNoFlushOnDelete);
	
    saveArchive.m_pDocument = this;
	saveArchive.m_bForceFlat = FALSE;
	TRY
	{
		CWaitCursor wait;
		saveArchive.Close();
		ReleaseFile(pFile, FALSE);
		Serialize(saveArchive);     // save me
	}
	CATCH_ALL(e)
	{
		ReleaseFile(pFile, TRUE);

		TRY
		{
			ReportSaveLoadException(lpszPathName, e, TRUE, AFX_IDP_FAILED_TO_SAVE_DOC);
		}
		END_TRY
		e->Delete();
		return FALSE;
	}
	END_CATCH_ALL

	SetModifiedFlag(FALSE);     // back to unmodified

	return TRUE;        // success
}
