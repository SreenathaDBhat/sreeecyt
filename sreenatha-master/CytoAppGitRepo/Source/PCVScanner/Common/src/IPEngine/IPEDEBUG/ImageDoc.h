#if !defined(AFX_IMAGEDOC_H__80E00453_4C6E_456B_912C_63FDB9C64ACE__INCLUDED_)
#define AFX_IMAGEDOC_H__80E00453_4C6E_456B_912C_63FDB9C64ACE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImageDoc.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CImageDoc document

class CImageDoc : public CDocument
{
protected:
	CImageDoc();           // protected constructor used by dynamic creation


	DECLARE_DYNCREATE(CImageDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImageDoc)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CImageDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CImageDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMAGEDOC_H__80E00453_4C6E_456B_912C_63FDB9C64ACE__INCLUDED_)
