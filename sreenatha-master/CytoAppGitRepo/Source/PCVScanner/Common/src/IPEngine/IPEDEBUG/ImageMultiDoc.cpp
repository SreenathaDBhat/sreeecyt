/////////////////////////////////////////////////////////////////////////////
// Overridden MFC functions to cope with more than one file extension for
// image loading, saving etc.
//
// Written By Karl Ratcliff 29052001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include "imagemultidoc.h"

/////////////////////////////////////////////////////////////////////////////
// Constructor

CImageMultiDocTemplate::CImageMultiDocTemplate(UINT nIDResource, CRuntimeClass* pDocClass,
                                               CRuntimeClass* pFrameClass, CRuntimeClass* pViewClass ) :
                                                     CMultiDocTemplate(nIDResource, pDocClass, pFrameClass, pViewClass)
{ 
}

/////////////////////////////////////////////////////////////////////////////
// Cater for multiple file extensions by overriding GetDocString()
/////////////////////////////////////////////////////////////////////////////

BOOL CImageMultiDocTemplate::GetDocString(CString& rString, enum DocStringIndex i) const
{
    CString strTemp,strLeft,strRight,resToken;
    int nFindPos = 0;
    
    AfxExtractSubString(strTemp, m_strDocStrings, (int)i);
    
    if (i == CDocTemplate::filterExt)  
    {
        do
        {
            resToken = strTemp.Tokenize(";", nFindPos);
            if (resToken != "") 
        {
            //string contains two extensions
            strLeft  = strTemp.Left(nFindPos + 1);
            strRight = strTemp.Right(lstrlen((const char*) strTemp) - nFindPos - 1);
            strTemp  = strLeft + strRight;
        }
    }
        while (resToken != "");
    }
    rString = strTemp;
    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// Match the selected doc type against multiple file extensions
/////////////////////////////////////////////////////////////////////////////

CDocTemplate::Confidence CImageMultiDocTemplate::MatchDocType(const char* pszPathName, CDocument*& rpDocMatch)
{
    ASSERT(pszPathName != NULL);
    rpDocMatch = NULL;
    
    // go through all documents
    POSITION pos = GetFirstDocPosition();
    while (pos != NULL)
    {
        CDocument* pDoc = GetNextDoc(pos);
        if (pDoc->GetPathName() == pszPathName) 
        {
            // already open
            rpDocMatch = pDoc;
            return yesAlreadyOpen;
        }
    }  // end while
    
    // see if it matches either suffix
    CString strFilterExt;
    
    if (GetDocString(strFilterExt, CDocTemplate::filterExt) && !strFilterExt.IsEmpty())
    {
        // see if extension matches
        ASSERT(strFilterExt[0] == '.');
        CString resToken;
        int nDot = CString(pszPathName).ReverseFind('.');
        int nFindPos = 0;
        const char* pszDot = nDot < 0 ? NULL : pszPathName + nDot;
        
        do
        {
            resToken = strFilterExt.Tokenize(";", nFindPos);
            if (resToken != "") 
        {
                if (nDot >= 0 && (lstrcmpi(pszPathName+nDot, resToken) == 0))
                return yesAttemptNative; // extension matches
        }
        }
        while (resToken != "");
        
        // Filter is contains a single extension
            if (nDot >= 0 && (lstrcmpi(pszPathName+nDot, strFilterExt)==0))
                return yesAttemptNative;  // extension matches
        }
    return yesAttemptForeign; //unknown document type
}



 
