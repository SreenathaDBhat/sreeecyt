#ifndef __IMAGEMULTIDOC_H
#define __IMAGEMULTIDOC_H

#include "stdafx.h"

class CImageMultiDocTemplate : public CMultiDocTemplate
{ 
public:  
   CImageMultiDocTemplate(UINT nIDResource, CRuntimeClass* pDocClass,
      CRuntimeClass* pFrameClass, CRuntimeClass* pViewClass ); 

   BOOL GetDocString(CString& rString, enum DocStringIndex i) const;
   Confidence MatchDocType(const char* pszPathName, CDocument*& rpDocMatch);  
    
    
};



#endif
