// ImageView.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// The image view class caters for both .bmp and .jpg files
// Displays UTS images in an MDI child window
//
// Written By Karl Ratcliff 19052001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include "vcmemrect.h"
#include "IPEDEBUG.h"
#include "ImageView.h"
#include "PicMan.h"
#include "Scriptproc.h"
#include "PicWindow.h"
#include "vcmemrect.h"
#include "vcblob.h"
#include "vcgeneric.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "woolzif.h"
#include "vcthresh.h"
#include "vcImProc.h"
#include "uts.h"
#include "mainfrm.h"
#include <math.h>
#include <aitiff.h>
#include <D3D9.h>
#include ".\imageview.h"

#ifndef SAFE_DELETE
#define SAFE_DELETE(x)      if (x) delete x; x = NULL;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CImageView

IMPLEMENT_DYNCREATE(CImageView, CScrollView)

CImageView::CImageView()
{
    // Safe defaults
    m_csFileName = "";
    // Zoom level of 1
    m_nZoomMult = 1;
    m_nZoomDiv = 1;
    // By Default strect to fit
    m_bStretchToFit = TRUE;
    // Set mouse mode to nothing
    m_MouseMode = mouseDoingNothing;
    // No height or width
    m_nImageW = m_nImageH = 1;
    // Safe value for image memory
    m_pImageAddr = NULL;
    m_pTiff = NULL;
    // No bits per pixel
    m_nBPP = 0;
    // By default no pseudo colour
    m_bPseudo = FALSE;
    // By default show normal not inverted
    m_bInvert = FALSE;
    // Component selection for AITIFF objects
    m_pComponentSelMenu = NULL;

	m_pUTSMemRect = ((CMainFrame *)AfxGetMainWnd())->m_pUTSMemRect;
}

CImageView::~CImageView()
{
    // Clear up
    if (m_pTiff)
        delete m_pTiff;
    else
    if (m_pImageAddr)           // NOTE don't delete this if using a Tiff this is done by deleting the Tiff
        delete m_pImageAddr;
    if (m_pComponentSelMenu)
    { 
        m_pComponentSelMenu->DestroyMenu();
        delete m_pComponentSelMenu;
    }
}


BEGIN_MESSAGE_MAP(CImageView, CScrollView)
	//{{AFX_MSG_MAP(CImageView)
	//ON_COMMAND(ID_IMAGEFILE_OPEN, OnImagefileOpen)
	ON_COMMAND(ID_STRETCHTOFIT, OnStretchtofit)
	ON_COMMAND(IDM_PROFILE, OnProfile)
	ON_COMMAND(IDM_DISTANCE, OnDistance)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(ID_QUEUE_ADDIMAGETOINPUTQUEUE, OnQueueAddimagetoinputqueue)
    ON_COMMAND(ID_QUEUE_ADDAITIFFTOINPUTQUEUE, OnQueueAddAITIFFimagetoinputqueue)
	ON_COMMAND(ID_MEASURE_HISTOGRAM, OnMeasureHistogram)
	ON_COMMAND(ID_QUEUE_GETIMAGEFROMOUTPUTQUEUE, OnQueueGetimagefromoutputqueue)
	ON_COMMAND(ID_VIEW_ZOOM_11, OnViewZoom11)
	ON_COMMAND(ID_VIEW_ZOOM_21, OnViewZoom21)
	ON_COMMAND(ID_VIEW_ZOOM_41, OnViewZoom41)
	ON_COMMAND(ID_VIEW_ZOOM_12, OnViewZoom12)
	ON_COMMAND(ID_VIEW_ZOOM_81, OnViewZoom81)
	ON_COMMAND(ID_VIEW_ZOOM_14, OnViewZoom14)
	ON_COMMAND(ID_VIEW_ZOOM_18, OnViewZoom18)
	ON_COMMAND(IDS_MENU_PSEUDO, OnMenuPseudo)
	ON_UPDATE_COMMAND_UI(IDS_MENU_PSEUDO, OnUpdateMenuPseudo)
	ON_COMMAND(ID_MENU_INVERT, OnMenuInvert)
	ON_UPDATE_COMMAND_UI(ID_MENU_INVERT, OnUpdateMenuInvert)
	ON_UPDATE_COMMAND_UI(ID_STRETCHTOFIT, OnUpdateStretchtofit)
	ON_WM_PAINT()
    ON_COMMAND_RANGE(ID_POPUP_IMAGE, ID_POPUP_IMAGE_MAX, OnPopMenu)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImageView drawing
/////////////////////////////////////////////////////////////////////////////

void CImageView::OnInitialUpdate()
{
    // Make sure this is set or we get an assertion !
    SetStretchMode();
	CScrollView::OnInitialUpdate();
    Refresh();

	CString title;
	title.Format("%s (%d bpp)", GetDocument()->GetTitle(), this->m_nBPP);
	GetDocument()->SetTitle(title);
}

void CImageView::OnDraw(CDC* pDC)
{
    // Image must be valid
    if (m_pImageAddr || m_pTiff)
    {
        HDC         MemDC = NULL, DrawDC = pDC->GetSafeHdc();
        HBITMAP	    hbmBMP, holdbmp = NULL;		// Handle of D-I BMP to use for blit to control	
        BYTE        *image, *destimage = NULL, *pBits;
        BITMAPINFO  *pBMP = NULL;
        int         i;
        long        nBytes;
        BYTE        *srcptr, *destptr;

        image = (BYTE *) m_pImageAddr;

        try 
        {
            // Create a memory DC for drawing on 
            MemDC = CreateCompatibleDC(DrawDC);
            if (!MemDC) throw;

            // Process images based on their bits per pixel
            if (m_nBPP == SIXTEEN_BITS_PER_PIXEL)
            {	// Need to create an 8bpp copy of the image for display purposes.
                long h16 = m_pUTSMemRect->MakeExtern(m_pImageAddr, m_nBPP, m_nImageW, m_nImageH, 0);
                long h8 = m_pUTSMemRect->CreateAsWithPix(h16, EIGHT_BITS_PER_PIXEL);

                m_pUTSMemRect->Convert16To8(h16, h8);
                
                long wi8, hi8, pitch8;
				void *pAddr8;
                m_pUTSMemRect->GetExtendedInfo(h8, &wi8, &hi8, &pitch8, &pAddr8);
                
                nBytes = pitch8 * hi8;
                destimage = new BYTE[nBytes];
                m_pUTSMemRect->GetBits(destimage, h8);
                image = destimage;

				// Free the UTS handles only.
                m_pUTSMemRect->FreeImage(h16);
                m_pUTSMemRect->FreeImage(h8);

                if (m_bPseudo)
                    pBMP = GetDibInfo()->GetBitmapPseudoInfo(8, m_nImageW, m_nImageH);
                else
                    pBMP = GetDibInfo()->GetBitmapInfo(8, m_nImageW, m_nImageH);
            }
			else
            {
                if (m_nBPP == ONE_BIT_PER_PIXEL)
                {
                    // Allocate some mem
                    destimage = new BYTE[m_nImageW * m_nImageH / 8];
                    // Get the bits out of the UTS image
                    memcpy(destimage, image, m_nImageW * m_nImageH / 8);
                    // Reverse the bit order in each byte of the image
                    m_pUTSMemRect->InverseBytes((char *) destimage, m_nImageW * m_nImageH / 8); 
                    image = destimage;
                }

                if (m_nBPP == EIGHT_BITS_PER_PIXEL && m_bPseudo)
                    pBMP = GetDibInfo()->GetBitmapPseudoInfo(8, m_nImageW, m_nImageH);
                else
                    pBMP = GetDibInfo()->GetBitmapInfo(m_nBPP, m_nImageW, m_nImageH);

                nBytes = ((((m_nImageW * m_nBPP) + 31) & ~31) >> 3) * m_nImageH; // DIB rows are padded to DWORD multiples.
            }

            // Get out if NULL pBMP
            if (!pBMP) throw;

            // Create a bmp for drawing 
            hbmBMP = CreateDIBSection(DrawDC, pBMP, DIB_RGB_COLORS, (LPVOID *)(&pBits), NULL, 0);
            // Clean up
            delete pBMP;

            // Set the bits
            if (m_bInvert)
            {
                srcptr = image;
                destptr = pBits;
                for (i = 0; i < nBytes; i++)
                {
                    *destptr++ = ~(*srcptr++);
                }
            }
            else
                memcpy(pBits, image, nBytes);

            // Select into the DC
            holdbmp = (HBITMAP) SelectObject(MemDC, hbmBMP);

            // Set the blitting mode to wipe out previous data
            SetStretchBltMode(DrawDC, COLORONCOLOR);

            // Stretch blit to the screen        
            if (StretchBlt(DrawDC,
                0, 
                0,
                (m_nImageW * m_nZoomMult) / m_nZoomDiv, 
                (m_nImageH * m_nZoomMult) / m_nZoomDiv,
                MemDC,
                0,
                0, 
                m_nImageW,
                m_nImageH, 
                SRCCOPY) == GDI_ERROR)
                throw;
        }

        catch (...)
        {
            OutputDebugString("GDI UTS DRAW ERROR\r\n");
        }

        // Release any used mem
        if (destimage)
            delete destimage;

        // Free any other resources used
        if (holdbmp)
            DeleteObject(SelectObject(MemDC, holdbmp));
        if (MemDC)
            DeleteDC(MemDC);
    }
}

/////////////////////////////////////////////////////////////////////////////
// CImageView diagnostics
/////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
void CImageView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CImageView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// Non CImageView message handlers
/////////////////////////////////////////////////////////////////////////////


/*
/////////////////////////////////////////////////////////////////////////////
// User elected to load an image from file into the window
/////////////////////////////////////////////////////////////////////////////
void CImageView::GetImageFromFile(void)
{
    long Pitch, ImageHandle;
    void *Addr;
	CUTSFileIO UTSFileIO(m_pUTSMemRect);

    CFileDialog FileSelect(TRUE, "bmp", "*.bmp", OFN_HIDEREADONLY,
		"Bitmap Files (*.bmp) | *.bmp | JPEG Files (*.jpg)| *.jpg| TIF Files (*.tif)| *.tif| JPEGLS Files (*.jls) | *.jls||", NULL);

    if (FileSelect.DoModal() == IDOK)
    {
        m_csFileName = FileSelect.GetPathName();

        // If already n image loaded then free it first
        if (m_pImageAddr)
            delete m_pImageAddr;
           
        // To be safe set this NULL as m_pImageAddr is not changed by delete
        m_pImageAddr = NULL;

        if (m_csFileName.Find(".bmp") != -1)
            ImageHandle = UTSFileIO.LoadDIB(m_csFileName, TRUE);
        else
        if (m_csFileName.Find(".jpg") != -1)
            ImageHandle = UTSFileIO.LoadJPG(m_csFileName, TRUE);
        else
        if (m_csFileName.Find(".tif") != -1)
        {

        }
		else
		if (m_csFileName.Find(".jls") != -1)
		{
			ImageHandle = UTSFileIO.LoadJPGLS(m_csFileName, TRUE);
		}
    
        // Set the window title
        if (UTSVALID_HANDLE(ImageHandle))
        {
            // Draw it on screen
            m_nBPP = m_pUTSMemRect->GetExtendedInfo(ImageHandle, &m_nImageW, &m_nImageH, &Pitch, &Addr);

            // Allocate memory to locally store image and avoid
            // image storage as a UTS image handle. This prevents
            // the script from freeing it !
            m_pImageAddr = new BYTE[Pitch * m_nImageH];
            memcpy(m_pImageAddr, Addr, Pitch * m_nImageH);

            // Display the image
            SetWindowText(m_csFileName);
            //UTSDraw.UTSPicWndDisplay(GetSafeHwnd(), ImageHandle);
            SetStretchMode();

            // Free the UTS handle for use elsewhere
            m_pUTSMemRect->FreeImage(ImageHandle);
        }
    }
}
*/

/////////////////////////////////////////////////////////////////////////////
// Sets whether the image should be scrolled or strecthed to fit the window
/////////////////////////////////////////////////////////////////////////////

void CImageView::SetStretchMode(void) 
{
    CSize sizeTotal;
    
    sizeTotal.cx = (m_nImageW * m_nZoomMult) / m_nZoomDiv;
    sizeTotal.cy = (m_nImageH * m_nZoomMult) / m_nZoomDiv;    
        
    if (!m_bStretchToFit)
        SetScrollSizes(MM_TEXT, sizeTotal);
    else
    {
        GetParentFrame()->RecalcLayout();
        SetScaleToFitSize(sizeTotal);
    }
}

/////////////////////////////////////////////////////////////////////////////
// Convert mouse coordinates into image ones
/////////////////////////////////////////////////////////////////////////////

void CImageView::Convert(POINT p, POINT& cp)
{
    if (m_bStretchToFit)
    {
        long W, H;
        RECT cRect;

        GetClientRect(&cRect);
        W = RECTWIDTH(&cRect);
        H = RECTHEIGHT(&cRect);

        // Convert to image pixel coordinates
        if (W)
            cp.x = (m_nImageW * p.x) / W;
        else
            cp.x = p.x;

        if (H)
            cp.y = (m_nImageH * (H - p.y)) / H;
        else
            cp.y = H - p.y;
    }
    else
    {
        CPoint  offs = GetScrollPosition();
            
        cp.x = ((p.x + offs.x) * m_nZoomDiv) / m_nZoomMult;
        cp.y = m_nImageH - (((p.y + offs.y) * m_nZoomDiv) / m_nZoomMult);
    }
}

/////////////////////////////////////////////////////////////////////////////
// Calculate the distance between two supplied window points in image
// units
/////////////////////////////////////////////////////////////////////////////

int CImageView::Distance(POINT p1, POINT p2)
{
    int dx, dy;
    POINT p1c, p2c;
    
    Convert(p1, p1c);
    Convert(p2, p2c);

    dx = p1c.x - p2c.x;
    dy = p1c.y - p2c.y;

    if (dx != 0 || dy != 0)
        return (int) sqrt((double)(dx * dx + dy * dy));
    else
        return 0;
}

/////////////////////////////////////////////////////////////////////////////
// Start the profiling process
/////////////////////////////////////////////////////////////////////////////

void CImageView::EnableProfiling(void)
{ 
    CMenu   *m_pMenu;
    CWnd *pMainFrame;

    pMainFrame = AfxGetMainWnd();

    m_pMenu = pMainFrame->GetMenu();
    if (m_MouseMode == mouseDoingNothing)
    {
        m_MouseMode = mouseStartProfiling;    // Enable processing of left mouse clicks for profiling
        m_pMenu->EnableMenuItem(IDM_PROFILE, MF_GRAYED);
    }
}

/////////////////////////////////////////////////////////////////////////////
// Start the profiling process
/////////////////////////////////////////////////////////////////////////////

void CImageView::EnableDistanceMeasurement(void)
{ 
    CMenu   *m_pMenu;

    m_pMenu = GetMenu();
    if (m_MouseMode == mouseDoingNothing)
    {
        m_MouseMode = mouseStartMeasure;      // Enable processing of left mouse clicks for profiling
        m_pMenu->EnableMenuItem(IDM_PROFILE, MF_GRAYED);
    }
}

/////////////////////////////////////////////////////////////////////////////
// Start a new line drawing sequence
/////////////////////////////////////////////////////////////////////////////

void CImageView::StartLine(POINT point)
{ 
    m_StartPoint = point;
    m_OldEnd = point;
}

/////////////////////////////////////////////////////////////////////////////
// Erase the previous line drawn
/////////////////////////////////////////////////////////////////////////////

void CImageView::EraseOldLine(COLORREF Colour)
{
    // Erase the old line
    CClientDC dc(this);
    
    // Drawing pen
    CPen CBr(PS_SOLID, 1, Colour);
    
    // Select into DC
    CPen *pOldBr = dc.SelectObject(&CBr);
    
    // Draw in XOR mode
    int PrevROP = dc.SetROP2(R2_XORPEN);
    
    // Erase the old line - don't draw the new one
    dc.MoveTo(m_StartPoint);
    dc.LineTo(m_OldEnd);
    
    // Select old pen back again
    dc.SelectObject(pOldBr);
    // Select the old ROP
    dc.SetROP2(PrevROP);
}

/////////////////////////////////////////////////////////////////////////////
// Erase the old line and draw a new one, remembering the last line pos
/////////////////////////////////////////////////////////////////////////////

void CImageView::EraseOldLineDrawNew(POINT NewPoint, COLORREF Colour)
{
    // Draw a line to whereever the mouse now is 
    // erasing the old one first
    CClientDC dc(this);
    
    // Drawing pen
    CPen CBr(PS_SOLID, 1, Colour);
    
    // Select into DC
    CPen *pOldBr = dc.SelectObject(&CBr);
    
    // Draw in XOR mode
    int PrevROP = dc.SetROP2(R2_XORPEN);
    
    // Erase old line first then the new one 
    dc.MoveTo(m_StartPoint);
    dc.LineTo(m_OldEnd);
    dc.MoveTo(m_StartPoint);
    dc.LineTo(NewPoint); 
    
    // Select old pen back again
    dc.SelectObject(pOldBr);
    // Select the old ROP
    dc.SetROP2(PrevROP);
    
    // Remember the end point
    m_OldEnd = NewPoint;
}

/////////////////////////////////////////////////////////////////////////////
// Redraw a window
/////////////////////////////////////////////////////////////////////////////

void CImageView::Refresh()
{
    RECT rcR;

    GetClientRect(&rcR);
    InvalidateRect(&rcR, TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// CImageView message handlers

/*
/////////////////////////////////////////////////////////////////////////////
// Open an image file
/////////////////////////////////////////////////////////////////////////////
void CImageView::OnImagefileOpen() 
{
    GetImageFromFile();
}
*/

/////////////////////////////////////////////////////////////////////////////
// User wants the image to stretch to fit the window
/////////////////////////////////////////////////////////////////////////////

void CImageView::OnStretchtofit() 
{
    // Toggle display mode
    m_bStretchToFit = !m_bStretchToFit;

    SetStretchMode();
}

/////////////////////////////////////////////////////////////////////////////
// Start a profile.
/////////////////////////////////////////////////////////////////////////////

void CImageView::OnProfile() 
{
    Refresh();
    EnableProfiling();	
}

/////////////////////////////////////////////////////////////////////////////
// Allow the user to measure a distance
/////////////////////////////////////////////////////////////////////////////

void CImageView::OnDistance() 
{
	EnableDistanceMeasurement();
}

/////////////////////////////////////////////////////////////////////////////
// User clicked the left button in an image
/////////////////////////////////////////////////////////////////////////////

void CImageView::OnLButtonDown(UINT nFlags, CPoint point) 
{
    // Show the coords in the window caption
    //CString CoordsPos;
    POINT   ip, sp, tp;

    Convert(point, ip);
    
    switch (m_MouseMode)
    {
    case mouseStartProfiling:
        // Define the start point for the profile
        // Start point defined, so switch to end point definition
        m_MouseMode = mouseProfiling;
        // Record the start point
        StartLine(point);
        
        //CoordsPos.Format("%s (%d, %d)", m_csFileName, ip.x, ip.y);
        //SetWindowText(CoordsPos);
        break;
        
    case mouseProfiling:
        long ImageHandle;
        CMainFrame *pMain;

        pMain = (CMainFrame *) AfxGetMainWnd();

        // Second mouse click in profiling mode
        m_MouseMode = mouseDoingNothing;
        //m_pMenu->EnableMenuItem(IDM_DISTANCE, MF_ENABLED);
        Convert(m_StartPoint, sp);
        if (sp.x > ip.x)
        {
            tp = sp;
            sp = ip;
            ip = tp;
        }
       
		ImageHandle = m_pUTSMemRect->Create(m_nBPP, m_nImageW, m_nImageH);
        m_pUTSMemRect->SetBits((BYTE *) m_pImageAddr, ImageHandle);
       
        pMain->m_pPicMan->AddNew(ImageHandle, "Profile", CPicWindow::wndProfile, sp.x, sp.y, ip.x, ip.y);
        break;
        
    case mouseStartMeasure:
        // Define the start point for the profile
        // Start point defined, so switch to end point definition
        m_MouseMode = mouseMeasuring;
        // Record the start point
        StartLine(point);
        
        //CoordsPos.Format("%s (%d, %d)", *m_pImageName, ip.x, ip.y);
        //SetWindowText(CoordsPos);
        break;
        
    case mouseMeasuring:
        EraseOldLine(MEASUREMENT_COLOUR);
        // Finished measuring, so wipe out the line and stop
        m_MouseMode = mouseDoingNothing;
        //m_pMenu->EnableMenuItem(IDM_PROFILE, MF_ENABLED);
        break;
        
    case mouseDoingNothing:
        break;
    }
    
	CScrollView::OnLButtonDown(nFlags, point);
}

/////////////////////////////////////////////////////////////////////////////
// Mouse moving over the top an image
/////////////////////////////////////////////////////////////////////////////

void CImageView::OnMouseMove(UINT nFlags, CPoint point) 
{
    // Output start and end in title bar
    //CString CoordsPos;
    POINT ip, sp;

    Convert(point, ip);
    
    switch (m_MouseMode)
    {
    case mouseProfiling:
        // Erase old line and draw one to the new point
        EraseOldLineDrawNew(point, PROFILE_COLOUR);
        Convert(m_StartPoint, sp);
        // Output coordinates of the start point and the current
        // position of the mouse
        //CoordsPos.Format("%s (%d, %d) to (%d, %d)", *m_pImageName, sp.x, sp.y, ip.x, ip.y);
        //SetWindowText(CoordsPos);
        break;
        
    case mouseMeasuring:
        // Erase old line and draw one to the new point
        EraseOldLineDrawNew(point, MEASUREMENT_COLOUR);
        
        // Output distance from cursor location to start point in title bar
        //CoordsPos.Format("%s Distance = %d", *m_pImageName, Distance(m_StartPoint, point));
        //SetWindowText(CoordsPos);
        break;
        
    case mouseDoingNothing:
        // Output current position in title bar           
        if (m_bGreyLevels)
            //CoordsPos.Format("%s Pos (%d, %d)", *m_pImageName, ip.x, ip.y);
        //else
        {
           // long Level = m_pUTSMemRectGetPixel(m_hImageHandle, ip.x, ip.y);
            //CoordsPos.Format("%s Pos (%d, %d) = %lX", *m_pImageName, ip.x, ip.y, Level);
        }
        //SetWindowText(CoordsPos);
        break;
    }
    
	
	CScrollView::OnMouseMove(nFlags, point);
}

/////////////////////////////////////////////////////////////////////////////
// User clicked the right mouse button so cancel the current operation
// if there was one
/////////////////////////////////////////////////////////////////////////////

void CImageView::OnRButtonDown(UINT nFlags, CPoint point) 
{
    switch (m_MouseMode)
    {
    case mouseStartProfiling:
    case mouseProfiling:
        // Current mode cancelled
        EraseOldLine(PROFILE_COLOUR);
        // Cancel
        m_MouseMode = mouseDoingNothing;
        //m_pMenu->EnableMenuItem(IDM_DISTANCE, MF_ENABLED);
        break;

    case mouseStartMeasure:
    case mouseMeasuring:
        // Current mode cancelled
        EraseOldLine(MEASUREMENT_COLOUR);
        // Cancel
        m_MouseMode = mouseDoingNothing;
        //m_pMenu->EnableMenuItem(IDM_PROFILE, MF_ENABLED);
        break;
        
    case mouseDoingNothing:
        if (m_pTiff && m_pComponentSelMenu)
            m_pComponentSelMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
        break;
    }
    	
	CScrollView::OnRButtonDown(nFlags, point);
}

/////////////////////////////////////////////////////////////////////////////
// Refresh the window
/////////////////////////////////////////////////////////////////////////////

void CImageView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
    Refresh();
}

/////////////////////////////////////////////////////////////////////////////
// User wants to put this image on the script input queue
/////////////////////////////////////////////////////////////////////////////

void CImageView::OnQueueAddimagetoinputqueue() 
{
    CMainFrame  *pMainWnd = (CMainFrame *) AfxGetMainWnd();

    if 	(pMainWnd->m_pScriptProcessor && m_pImageAddr)
    {
        VBS_IMAGE_REC ImData;

        ImData.Width = m_nImageW;
        ImData.Height = m_nImageH;
        ImData.BitsPerPixel = (BYTE) m_nBPP;
        ImData.Address = m_pImageAddr;
	    
        if (!pMainWnd->m_pScriptProcessor->PutImage(&ImData))
            MessageBox("Error sending image info to Q", "Help !", MB_OK);
    }
}

void CImageView::OnQueueAddAITIFFimagetoinputqueue() 
{
    CMainFrame  *pMainWnd = (CMainFrame *) AfxGetMainWnd();

    if 	(pMainWnd->m_pScriptProcessor && m_pImageAddr)
    {
        VBS_IMAGE_REC ImData;

        ImData.Width = m_nImageW;
        ImData.Height = m_nImageH;
        ImData.BitsPerPixel = (BYTE) m_nBPP;
        ImData.Address = m_pTiff;
	    
        if (!pMainWnd->m_pScriptProcessor->PutImage(&ImData))
            MessageBox("Error sending image info to Q", "Help !", MB_OK);
    }
}

/////////////////////////////////////////////////////////////////////////////
// Add and display a histogram for a single 8 bit component
/////////////////////////////////////////////////////////////////////////////

void CImageView::AddHisto(long hImage, CString title)
{
	// Get the number of bins required for this bit depth of image.
	long x, y, p;
	long bpp = m_pUTSMemRect->GetRectInfo(hImage, &x, &y);
	int numberOfBins = 1 << bpp;

	// Allocate a temporary block of memory for storing the histogram;
	DWORD *pHisto = new DWORD [ numberOfBins ];
    
    if (pHisto)
    {
        // Do The Histogram.
		// Must call CUTSMemRect::HistogramMask() with a fake mask,
		// rather than CUTSMemRect::Histogram(), as the latter only works with 8 bpp images.
		long hMask = m_pUTSMemRect->CreateAsWithPix(hImage, 1);
		m_pUTSMemRect->OperMask(hMask, "Ones"); // Fill.
        m_pUTSMemRect->HistogramMask(hImage, hMask, pHisto, numberOfBins);
		m_pUTSMemRect->FreeImage(hMask);

		// Allocate a memory rectangle large enough to store the histogram.
		long hHisto = m_pUTSMemRect->Create(32, numberOfBins, 1); // 32 bits per bin.
		if (hHisto > 0)
		{
			// Get The Pointer To The Histogram Storage Area
			DWORD *hptr;
			m_pUTSMemRect->GetExtendedInfo(hHisto, &x, &y, &p, &hptr);
        
			// Set The Data In The Memory Rectangle
			for (int i = 0; i < numberOfBins; i++)
				*hptr++ = pHisto[i];

			// Create a new window and stick the image in it
			CMainFrame *pMainWnd = (CMainFrame*)AfxGetMainWnd();
			pMainWnd->m_pPicMan->AddNew(hHisto, GetDocument()->GetTitle() + title, CPicWindow::wndHistogram);
		}

		delete[] pHisto;
    }
}

/////////////////////////////////////////////////////////////////////////////
// Measure and draw a histogram in the window
/////////////////////////////////////////////////////////////////////////////

void CImageView::OnMeasureHistogram() 
{
    long ImageHandle;

    if (m_nBPP == EIGHT_BITS_PER_PIXEL || m_nBPP == SIXTEEN_BITS_PER_PIXEL || m_nBPP == TWENTYFOUR_BITS_PER_PIXEL)
    {
        ImageHandle = m_pUTSMemRect->Create(m_nBPP, m_nImageW, m_nImageH);
        m_pUTSMemRect->SetBits((BYTE *) m_pImageAddr, ImageHandle);
        
        if (m_nBPP == EIGHT_BITS_PER_PIXEL || m_nBPP == SIXTEEN_BITS_PER_PIXEL)
            AddHisto(ImageHandle, " Intensity");
        else if (m_nBPP == TWENTYFOUR_BITS_PER_PIXEL)
        {
            // Separate into three components
            long H1, H2, H3;
            H1 = m_pUTSMemRect->CreateAsWithPix(ImageHandle, EIGHT_BITS_PER_PIXEL);
            H2 = m_pUTSMemRect->CreateAsWithPix(ImageHandle, EIGHT_BITS_PER_PIXEL);
            H3 = m_pUTSMemRect->CreateAsWithPix(ImageHandle, EIGHT_BITS_PER_PIXEL);

            m_pUTSMemRect->RGBToGray(ImageHandle, H1, 255, 0, 0);
            m_pUTSMemRect->RGBToGray(ImageHandle, H2, 0, 255, 0);
            m_pUTSMemRect->RGBToGray(ImageHandle, H3, 0, 0, 255);

            AddHisto(H1, " Red");
            AddHisto(H2, " Green");
            AddHisto(H3, " Blue");

            m_pUTSMemRect->FreeImage(H1);
            m_pUTSMemRect->FreeImage(H2);
            m_pUTSMemRect->FreeImage(H3);
        }

        m_pUTSMemRect->FreeImage(ImageHandle);
    }
}

/////////////////////////////////////////////////////////////////////////////
// Load an AITIFF/TIF
/////////////////////////////////////////////////////////////////////////////

int CImageView::LoadAITiff(CString FileName)
{
    int ImageHandle = UTS_ERROR;

    SAFE_DELETE(m_pTiff);
    m_pTiff = new AI_Tiff;

    m_pTiff->LoadFile(FileName);
    m_pTiff->Render();

    // Get dimensions, depth and ptr of image data
    int w = m_pTiff->GetRenderBufferWidth();
    int h = m_pTiff->GetRenderBufferHeight();
    int bpp = 24;

    if (!(m_pImageAddr = (BYTE *)m_pTiff->GetRenderBuffer()))
    {
        SAFE_DELETE(m_pTiff);
        ImageHandle = UTS_ERROR;
    }
    else
    {
        // Allocate a UTS handle
        ImageHandle = m_pUTSMemRect->Create(bpp, w, h);
    }

    // and splat the image data in (may have to do some thing different if a 1 bit mask image)
    if (UTSVALID_HANDLE(ImageHandle))
    {
        // Okay now create a UTS memrect and fill it in
        DWORD *addr;
        long utsbpp, W, H, P;
        long pixels_size = h * ((((w * bpp) + 31) & ~31) >> 3);
        // Get the address of the allocated UTS memory
        utsbpp = m_pUTSMemRect->GetExtendedInfo(ImageHandle, &W, &H, &P, &addr);
        memcpy(addr, m_pImageAddr, pixels_size);
        m_csFileName = FileName;
    }            

    // Got to here - it didn't happen
    return ImageHandle;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Set component display menu up
/////////////////////////////////////////////////////////////////////////////////////////////

void CImageView::LoadComponentMenu(void)               // Set component display menu up
{
    if (m_pTiff)
    {
        int nComps = m_pTiff->CountComponents();
        int i;
        TCHAR CompName[_MAX_PATH];
        int c = 0;

        if (m_pComponentSelMenu)
            delete m_pComponentSelMenu;
        m_pComponentSelMenu = new CMenu();
                
        if (m_pComponentSelMenu->CreatePopupMenu())
        {
            for (i = 0; i < nComps; i++)
            {
                if (m_pTiff->GetComponentDescription(i, CompName, _MAX_PATH))
                {
                    m_pComponentSelMenu->AppendMenu(MF_STRING | MF_POPUP | MF_BYCOMMAND | MF_CHECKED, ID_POPUP_IMAGE + i, CompName);
                    c++;
                }
            }

            // No point in having a pop menu 
            if (c < 2)
            {
                m_pComponentSelMenu->DestroyMenu();
                SAFE_DELETE(m_pComponentSelMenu); 
            }
        }
    }
}

/////////////////////////////////////////////////////////////////////////////
// Load and save image data
/////////////////////////////////////////////////////////////////////////////

void CImageView::Serialize(CArchive& ar) 
{
	CUTSFileIO UTSFileIO(m_pUTSMemRect);

	if (ar.IsStoring())
	{
        BOOL result = FALSE;
        CString message;
        long ImageHandle;

        // Create a UTS Image and set its contents
        ImageHandle = m_pUTSMemRect->Create(m_nBPP, m_nImageW, m_nImageH);
        m_pUTSMemRect->SetBits((BYTE *) m_pImageAddr, ImageHandle);

        // Use the UTS routines to store the image
        if (ar.m_strFileName.Find(".bmp") != -1)
            result = (UTSFileIO.SaveDIB(ar.m_strFileName, ImageHandle) > 0);
        else
        if (ar.m_strFileName.Find(".jpg") != -1)
            result = UTSFileIO.SaveJPG(ar.m_strFileName, ImageHandle, 100);
		else
		if (ar.m_strFileName.Find(".jls") != -1 || ar.m_strFileName.Find(".stk") != -1)
			result = UTSFileIO.SaveJPGLS(ar.m_strFileName, ImageHandle);


        // Saved o.k. or not ?
        if (!result || !UTSVALID_HANDLE(ImageHandle))
        {
            message.Format("Failed to save %s to file", ar.m_strFileName);
            MessageBox(message, "Error", MB_OK | MB_ICONEXCLAMATION);
        }

        // Clear up
        m_pUTSMemRect->FreeImage(ImageHandle);
	}
	else
	{
        void *Addr;
        long Pitch, ImageHandle = -1;

        // Delete old image if loading a new one
        if (m_pImageAddr)
            delete m_pImageAddr;

        // Safety
        m_pImageAddr = NULL;

        if (ar.m_strFileName.Find(".bmp") != -1 || ar.m_strFileName.Find(".BMP") != -1)
            ImageHandle = UTSFileIO.LoadDIB(ar.m_strFileName, TRUE);
        else
        if (ar.m_strFileName.Find(".jpg") != -1 || ar.m_strFileName.Find(".JPG") != -1)
            ImageHandle = UTSFileIO.LoadJPG(ar.m_strFileName, TRUE);
        else
        if (ar.m_strFileName.Find(".tif") != -1 || ar.m_strFileName.Find(".TIF") != -1)
            ImageHandle = LoadAITiff(ar.m_strFileName);
		else
        if (   ar.m_strFileName.Right(4).CompareNoCase(".jls") == 0
		    || ar.m_strFileName.Right(4).CompareNoCase(".stk") == 0)
		{
			if (((CIPEDEBUGApp*)AfxGetApp())->m_loadAs8bpp)
				ImageHandle = UTSFileIO.LoadJPGLSAs8Bit(ar.m_strFileName, TRUE);
			else
				ImageHandle = UTSFileIO.LoadJPGLS(ar.m_strFileName, TRUE);
		}

        // Got a valid image ?
        if (UTSVALID_HANDLE(ImageHandle))
        {
            // Get the image data and store separately to the UTS
            m_nBPP = m_pUTSMemRect->GetExtendedInfo(ImageHandle, &m_nImageW, &m_nImageH, &Pitch, &Addr);
            // Alloc some memory
            m_pImageAddr = new BYTE[m_nImageH * Pitch];
		    //memcpy(m_pImageAddr, Addr, m_nImageH * Pitch);
            m_pUTSMemRect->GetBits((BYTE *) m_pImageAddr, ImageHandle);
            // Free the UTS handle
            m_pUTSMemRect->FreeImage(ImageHandle);
            // Is this an AI TIFF
            if (m_pTiff)
                LoadComponentMenu();
			SetStretchMode();
			Refresh();
        }
        else
        {
            m_nImageW = 0;
            m_nImageH = 0;
			MessageBox(ar.m_strFileName, "Failed to load", MB_ICONERROR | MB_OK);
        }
	}
}

BOOL CImageView::PreCreateWindow(CREATESTRUCT& cs) 
{
	return CScrollView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// Get an image from the script output queue and draw in this window
/////////////////////////////////////////////////////////////////////////////

void CImageView::OnQueueGetimagefromoutputqueue() 
{
    VBS_IMAGE_REC   ImageInfo;
    BOOL            Cont = FALSE;
    CMainFrame      *pMainWnd = (CMainFrame *) AfxGetMainWnd();

    if (!pMainWnd->m_pScriptProcessor->GetImage(&ImageInfo))
        MessageBox("Error getting image from script output Q", "Help !",  MB_OK | MB_ICONEXCLAMATION);
    else
    {
        // Only do if the user wants to
        if (m_pImageAddr > 0)
        {
            if (MessageBox("Warning this will overwirte the image\r\nin this window, continue", "Warning", MB_YESNO | MB_ICONQUESTION) == IDYES)
                Cont = TRUE;
        }
        else
            Cont = TRUE;

        // Image window was free or the user happy to overwrite old image
        if (Cont)
        {
            // Delete the old image
            if (m_pImageAddr)
                delete m_pImageAddr;
            
            // Set window parameters
            m_nImageW = ImageInfo.Width;
            m_nImageH = ImageInfo.Height;
            m_nBPP = ImageInfo.BitsPerPixel;
            // NOTE no need to alloc some memory for our new
            // image as this will have been done by the GetImage() function
            m_pImageAddr = ImageInfo.Address;
            // Make sure this is called
            SetStretchMode();
            // Refresh the window
            Refresh();
        }
        
        // Free image memory allocated by PutImage on the VB script host side
        delete ImageInfo.Address;
    }	
}



/////////////////////////////////////////////////////////////////////////////
// Sets image zoom levels
/////////////////////////////////////////////////////////////////////////////

void  CImageView::SetZoomLevel(int Mult, int Div)        // Set image display zoom level
{
    m_bStretchToFit = FALSE;
	m_nZoomMult = Mult;
	m_nZoomDiv  = Div;
    SetStretchMode();
    Refresh();
}

void CImageView::OnViewZoom11() 
{
    SetZoomLevel(1, 1);
}

void CImageView::OnViewZoom21() 
{
    SetZoomLevel(2, 1);
}

void CImageView::OnViewZoom41() 
{
    SetZoomLevel(4, 1);
}

void CImageView::OnViewZoom12() 
{
    SetZoomLevel(1, 2);	
}

void CImageView::OnViewZoom81() 
{
	SetZoomLevel(8, 1);	
}

void CImageView::OnViewZoom14() 
{
	SetZoomLevel(1, 4);
}

void CImageView::OnViewZoom18() 
{
	SetZoomLevel(1, 8);	
}

void CImageView::OnMenuPseudo() 
{
    if (m_nBPP == EIGHT_BITS_PER_PIXEL || m_nBPP == SIXTEEN_BITS_PER_PIXEL)
        m_bPseudo = !m_bPseudo;
}

void CImageView::OnUpdateMenuPseudo(CCmdUI* pCmdUI) 
{
    pCmdUI->SetCheck(m_bPseudo);
    Refresh();
}

void CImageView::OnMenuInvert() 
{
	m_bInvert = !m_bInvert;
}

void CImageView::OnUpdateMenuInvert(CCmdUI* pCmdUI) 
{
    pCmdUI->SetCheck(m_bInvert);
    Refresh();
}

void CImageView::OnUpdateStretchtofit(CCmdUI* pCmdUI) 
{
    pCmdUI->SetCheck(m_bStretchToFit);	
}

/////////////////////////////////////////////////////////////////////////////////////////
// Component selection
/////////////////////////////////////////////////////////////////////////////////////////

void CImageView::OnPopMenu(UINT id)
{
    int nComp = id - ID_POPUP_IMAGE;

    if (m_pTiff && m_pComponentSelMenu)
    {
        if (m_pTiff->IsComponentVisible(nComp))
        {
            m_pTiff->SetComponentVisible(nComp, FALSE);
            m_pComponentSelMenu->CheckMenuItem(id, MF_UNCHECKED);
        }
        else
        {
            m_pTiff->SetComponentVisible(nComp, TRUE);
            m_pComponentSelMenu->CheckMenuItem(id, MF_CHECKED);
        }
                    
        m_pTiff->Render();
        m_pImageAddr = (BYTE *)m_pTiff->GetRenderBuffer();
        Refresh();
    }
}


void CImageView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView)
{
    CScrollView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}
