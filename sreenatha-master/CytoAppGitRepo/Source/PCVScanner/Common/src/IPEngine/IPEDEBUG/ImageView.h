#if !defined(AFX_IMAGEVIEW_H__190F4374_63BD_40C7_A276_3BC410FED0DF__INCLUDED_)
#define AFX_IMAGEVIEW_H__190F4374_63BD_40C7_A276_3BC410FED0DF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImageView.h : header file
//
// Profiling line colour
#define PROFILE_COLOUR                      RGB(0xFF, 0x00, 0xFF)

// Measurement line colour
#define MEASUREMENT_COLOUR                  RGB(0x80, 0x00, 0xFF)

// This is the valid range of command ids
#define ID_POPUP_IMAGE                      0x9000
#define ID_POPUP_IMAGE_MAX                  0x90FF

class AI_Tiff;
class CUTSMemRect;

/////////////////////////////////////////////////////////////////////////////
// CImageView view

class CImageView : public CScrollView
{
public:
    // Current operation mode of the mouse within the window
    typedef enum {
        mouseDoingNothing,              // Doing sod all
            mouseStartProfiling,            // Started defining a profile line
            mouseProfiling,                 // Presently defining a profile line
            mouseStartMeasure,              // Base point to measure a distance
            mouseMeasuring                  // Measuring distance from the base point
    } InteractiveMode;
    
protected:
	CImageView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CImageView)

// Attributes
public:
    CString  m_csFileName;
    long     m_nImageW, m_nImageH;    // Image size in pixels
    long     m_nZoomMult, m_nZoomDiv; // Zooming Factors
    BOOL     m_bStretchToFit;         // Stretch to fit or scroll
    void     *m_pImageAddr;           // Pointer to image bits as stored in memory
    BOOL     m_bPseudo;               // 8 bit pseudo colour 
    BOOL     m_bInvert;               // Show inverted
    AI_Tiff  *m_pTiff;                // Image is a TIFF object
	CUTSMemRect *m_pUTSMemRect;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImageView)
	public:
	virtual void Serialize(CArchive& ar);
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CImageView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
    //void    GetImageFromFile(void);                 // Load an image from file
    void    SetStretchMode(void);                   // Set the window display mode
    void    EnableProfiling(void);                  // Start defining a profile
    void    EnableDistanceMeasurement(void);        // Start measuring the distance between two points
    void    Convert(POINT p, POINT& cp);            // Converts window mouse coords to image pixels
    int     Distance(POINT p1, POINT p2);           // Distance in image coords between two window points
    void    StartLine(POINT point);                 // Start line drawing on an image
    void    EraseOldLine(COLORREF Colour);          // Erase a previously drawn line
    void    EraseOldLineDrawNew(POINT NewPoint, COLORREF Colour); // Same as above, but draw a new one
    void    Refresh();
    void    SetZoomLevel(int Mult, int Div);        // Set image display zoom level
    int     LoadAITiff(CString FileName);           // Load in an AITIFF
    void    LoadComponentMenu(void);                // Set component display menu up
    void    AddHisto(long hImage, CString title); // Add a histogram

    // Member variables
    POINT   m_StartPoint, m_EndPoint;               // Start and end points for the profile
    POINT   m_OldEnd;                               // Old end of profile (for draw and erase)
    BOOL    m_bGreyLevels;                          // = TRUE if caption should display grey level values
    int     m_nBPP;                                 // Image resolution
    InteractiveMode m_MouseMode;
    CMenu   *m_pComponentSelMenu;

	// Generated message map functions
	//{{AFX_MSG(CImageView)
	//afx_msg void OnImagefileOpen();
	afx_msg void OnStretchtofit();
	afx_msg void OnProfile();
	afx_msg void OnDistance();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnQueueAddimagetoinputqueue();
    afx_msg void OnQueueAddAITIFFimagetoinputqueue();
	afx_msg void OnMeasureHistogram();
	afx_msg void OnQueueGetimagefromoutputqueue();
	afx_msg void OnViewZoom11();
	afx_msg void OnViewZoom21();
	afx_msg void OnViewZoom41();
	afx_msg void OnViewZoom12();
	afx_msg void OnViewZoom81();
	afx_msg void OnViewZoom14();
	afx_msg void OnViewZoom18();
	afx_msg void OnMenuPseudo();
	afx_msg void OnUpdateMenuPseudo(CCmdUI* pCmdUI);
	afx_msg void OnMenuInvert();
	afx_msg void OnUpdateMenuInvert(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStretchtofit(CCmdUI* pCmdUI);
    afx_msg void OnPopMenu(UINT id);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
protected:
    virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMAGEVIEW_H__190F4374_63BD_40C7_A276_3BC410FED0DF__INCLUDED_)
