// InteractiveView.cpp : implementation file
//

#include "stdafx.h"
#include "IPEDEBUG.h"
#include "InteractiveView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInteractiveView

IMPLEMENT_DYNCREATE(CInteractiveView, CView)

CInteractiveView::CInteractiveView()
{
}

CInteractiveView::~CInteractiveView()
{
}


BEGIN_MESSAGE_MAP(CInteractiveView, CView)
	//{{AFX_MSG_MAP(CInteractiveView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInteractiveView drawing

void CInteractiveView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CInteractiveView diagnostics

#ifdef _DEBUG
void CInteractiveView::AssertValid() const
{
	CView::AssertValid();
}

void CInteractiveView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CInteractiveView message handlers
