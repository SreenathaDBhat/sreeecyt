// LongDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LongDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// LongDlg dialog


LongDlg::LongDlg(CWnd* pParent /*=NULL*/)
	: CDialog(LongDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(LongDlg)
	m_LongValue = 0;
	//}}AFX_DATA_INIT
}


void LongDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(LongDlg)
	DDX_Text(pDX, IDC_LONGEDIT, m_LongValue);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(LongDlg, CDialog)
	//{{AFX_MSG_MAP(LongDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// LongDlg message handlers
