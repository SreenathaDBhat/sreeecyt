#if !defined(AFX_LONGDLG_H__56410310_EFDC_41DF_B0F5_ABC5592AD7DE__INCLUDED_)
#define AFX_LONGDLG_H__56410310_EFDC_41DF_B0F5_ABC5592AD7DE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LongDlg.h : header file
//
#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// LongDlg dialog

class LongDlg : public CDialog
{
// Construction
public:
	LongDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(LongDlg)
	enum { IDD = IDD_LONGDLG };
	long	m_LongValue;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(LongDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(LongDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LONGDLG_H__56410310_EFDC_41DF_B0F5_ABC5592AD7DE__INCLUDED_)
