// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "vcmemrect.h"
#include "ImageView.h"
#include "imageDoc.h"
#include "IPEDEBUG.h"
#include "OptionsDlg.h"
#include "MainFrm.h"
#include "PicMan.h"
#include "scriptproc.h"
#include <direct.h>
#include "assert.h"
#include ".\mainfrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static UINT BASED_CODE styles[] =
{
	// same order as in the bitmap 'styles.bmp'
	ID_SEPARATOR,           // for combo box (placeholder)
	ID_SEPARATOR,
	ID_FILE_NEW,
	ID_FILE_OPEN,
	ID_FILE_SAVE,
	ID_EDIT_CUT,
    ID_EDIT_COPY,
    ID_EDIT_PASTE,
    ID_FILE_PRINT,
    ID_SCRIPT_RUN,
    ID_SEPARATOR,
};



/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SIZE()
	ON_COMMAND(ID_FILE_OPTIONS, OnFileOptions)
	//}}AFX_MSG_MAP
    ON_COMMAND(ID_VIEW_CLEAROPENWINDOWS, OnViewClearopenwindows)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,               // status line indicator 
	ID_EDIT_INDICATOR_PROGRESS,
	ID_EDIT_INDICATOR_POSITION,	 
	ID_EDIT_INDICATOR_COL,
	ID_EDIT_INDICATOR_CRLF,
	ID_INDICATOR_OVR,	
	ID_EDIT_INDICATOR_READ,
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
    m_pPicMan = NULL;
    m_pScriptProcessor = NULL;
	m_pUTSMemRect = NULL;
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	EnableDocking(CBRS_ALIGN_ANY);

	if (!CreateToolBar())
		return -1;
	if (!CreateStatusBar())
		return -1;

    // Create the script processor
    m_pScriptProcessor = new CScriptProcessor;

	CIPEDEBUGApp *pApp = (CIPEDEBUGApp *)AfxGetApp();
	CString TempDir;

    pApp->GetTempDir(TempDir);

	// Make a working temp dir if it don't exist
	_mkdir(TempDir);

    // Initialise it
    if (m_pScriptProcessor)
        m_pScriptProcessor->InitialiseScriptProcessor("IPEVARS.DAT", TempDir, "IPEIN", "IPEOUT");
    else
    {
        TRACE0("Failed to initialise the script processor\n");
        return -1;
    }
		
	m_pUTSMemRect = new CUTSMemRect();
	if (!m_pUTSMemRect->Initialise())
	{
		OutputDebugString(_T("IPEDEBUG ZMEMRECT.DLL FAILED TO LOAD\r\n"));
		assert(0);
	}

	// Create a pic manager for managing windows
	m_pPicMan = new CPicManager(m_pUTSMemRect);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

BOOL CMainFrame::CreateToolBar()
{
	const int nDropHeight = 100;

	if (!m_wndToolBar.Create(this, WS_CHILD|WS_VISIBLE|CBRS_TOP|
			CBRS_TOOLTIPS|CBRS_FLYBY, IDW_STYLES) ||
		!m_wndToolBar.LoadBitmap(IDB_MAINFRAME) ||
		!m_wndToolBar.SetButtons(styles, sizeof(styles)/sizeof(UINT)))
	{
		TRACE0("Failed to create stylebar\n");
		return FALSE;       // fail to create
	}

	// Create the combo box
	m_wndToolBar.SetButtonInfo(0, IDC_IMAGECOMBO, TBBS_SEPARATOR, 150);

	// Design guide advises 12 pixel gap between combos and buttons
	m_wndToolBar.SetButtonInfo(1, ID_SEPARATOR, TBBS_SEPARATOR, 12);
	CRect rect;
	m_wndToolBar.GetItemRect(0, &rect);
	rect.top = 3;
	rect.bottom = rect.top + nDropHeight;
	if (!m_wndToolBar.m_comboBox.Create(
			CBS_DROPDOWNLIST|WS_VISIBLE|WS_TABSTOP,
			rect, &m_wndToolBar, IDC_IMAGECOMBO))
	{
		TRACE0("Failed to create combo-box\n");
		return FALSE;
	}

	//  Fill the combo box
	CString szStyle;

	//  Create a font for the combobox
	LOGFONT logFont;
	memset(&logFont, 0, sizeof(logFont));

	if (!::GetSystemMetrics(SM_DBCSENABLED))
	{
		// Since design guide says toolbars are fixed height so is the font.
		logFont.lfHeight = -8;
		logFont.lfWeight = FW_NORMAL;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		lstrcpy(logFont.lfFaceName, _T("MS Sans Serif"));
		if (!m_wndToolBar.m_font.CreateFontIndirect(&logFont))
			TRACE0("Could Not create font for combo\n");
		else
			m_wndToolBar.m_comboBox.SetFont(&m_wndToolBar.m_font);
	}
	else
	{
		m_wndToolBar.m_font.Attach(::GetStockObject(SYSTEM_FONT));
		m_wndToolBar.m_comboBox.SetFont(&m_wndToolBar.m_font);
	}

	return TRUE;
}

BOOL CMainFrame::CreateStatusBar()
{
	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return FALSE;       // fail to create
	}

	UINT nID, nStyle;
	int cxWidth;

	m_wndStatusBar.GetPaneInfo( 0, nID, nStyle, cxWidth);
	m_wndStatusBar.SetPaneInfo( 0, nID, SBPS_STRETCH|SBPS_NORMAL, cxWidth);

	return TRUE;
}

BOOL CMainFrame::DestroyWindow() 
{
    if (m_pScriptProcessor)
    {
        m_pScriptProcessor->ReleaseScriptProcessor();
        delete m_pScriptProcessor;
        m_pScriptProcessor = NULL;
    }

	if (m_pPicMan)
		delete m_pPicMan;

	if (m_pUTSMemRect)
	{
		m_pUTSMemRect->FreeAll();
		delete m_pUTSMemRect;
	}

	return CMDIFrameWnd::DestroyWindow();
}

void CMainFrame::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CMDIFrameWnd::OnShowWindow(bShow, nStatus);
}

void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{
	CMDIFrameWnd::OnSize(nType, cx, cy);
}

void CMainFrame::OnFileOptions() 
{
	COptionsDlg dlg;

	if (dlg.DoModal() == IDOK)
	{
		CIPEDEBUGApp *pApp = (CIPEDEBUGApp *) AfxGetApp();

		pApp->SetTempDir(dlg.m_TempDir);
		pApp->SetScriptDir(dlg.m_ScriptDir);
		pApp->SetEffectsDir(dlg.m_EffectDir);
		pApp->SetImageDir(dlg.m_ImageDir);
		pApp->m_loadAs8bpp = dlg.m_8bpp;
	}
}

void CMainFrame::AddNameToCombo(CString Name, CImageView *pView)          // Add an image to the image combo
{
    int Res;
	
    if (m_wndToolBar.m_comboBox.FindString(0, Name) == CB_ERR)
    {
        Res = m_wndToolBar.m_comboBox.AddString(Name);
        m_wndToolBar.m_comboBox.SetItemDataPtr(Res, pView);
    }
}

BOOL CMainFrame::GetNameFromCombo(CString &Name)
{
    int Idx;    
    Idx = m_wndToolBar.m_comboBox.GetCurSel();
    if (Idx != CB_ERR)
	{
        m_wndToolBar.m_comboBox.GetLBText(Idx, Name);
        return TRUE;
	}
    else
        return FALSE;
}

void CMainFrame::ResetCombo(void)
{
    m_wndToolBar.m_comboBox.ResetContent();
}


void CMainFrame::OnViewClearopenwindows()
{
    m_pPicMan->DeleteAll();
}
