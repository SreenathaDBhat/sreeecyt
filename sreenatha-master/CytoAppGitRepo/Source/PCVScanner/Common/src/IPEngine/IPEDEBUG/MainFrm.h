// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__AA700C82_0EED_4A50_A765_9F03E12C6BA9__INCLUDED_)
#define AFX_MAINFRM_H__AA700C82_0EED_4A50_A765_9F03E12C6BA9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CImageView;
class CUTSMemRect;

typedef enum {
    POS_SEPARATOR = 0,
    POS_CAPS = 1,
    POS_NUM = 2,
    POS_SCRL = 3,
    POS_EDITOR = 4,
    POS_SCRIPTMODE = 5,
    POS_LINECOL = 6,
} StatusBarIndicatorPos;

// Instead of using a class here we could have just put these controls
// directly into CMainFrame.  As it is they are sending messages to the
// main frame just like they were part of it instead of part of a control
// bar.
class CIPEToolBar : public CToolBar
{
public:
	CComboBox   m_comboBox;
	CFont       m_font;
};

class CPicManager;
class CScriptProcessor;

class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// Attributes
public:

// Operations
public:
    CPicManager				*m_pPicMan;         // For histograms, profiles etc.
    CScriptProcessor        *m_pScriptProcessor;
	
	CUTSMemRect				*m_pUTSMemRect;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

    void AddNameToCombo(CString Name, CImageView *pView);   // Add a name to the toolbar combo
    BOOL GetNameFromCombo(CString &Name);  
    void ResetCombo(void);

protected:  // control bar embedded members
    BOOL CreateToolBar();
	BOOL CreateStatusBar();

	CStatusBar  m_wndStatusBar;
	CIPEToolBar m_wndToolBar;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnFileOptions();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnViewClearopenwindows();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__AA700C82_0EED_4A50_A765_9F03E12C6BA9__INCLUDED_)
