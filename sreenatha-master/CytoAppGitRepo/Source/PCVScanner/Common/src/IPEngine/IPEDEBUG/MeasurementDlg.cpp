// MeasurementDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IPEDEBUG.h"
#include "CMeasurement.h"
#include "MeasurementDlg.h"


// CMeasurementDlg dialog

IMPLEMENT_DYNAMIC(CMeasurementDlg, CDialog)
CMeasurementDlg::CMeasurementDlg(Measurement *pM, CWnd* pParent /*=NULL*/)
	: CDialog(CMeasurementDlg::IDD, pParent)
{
    m_pMeas = pM;
}

CMeasurementDlg::~CMeasurementDlg()
{
}

void CMeasurementDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CMeasurementDlg, CDialog)
END_MESSAGE_MAP()


// CMeasurementDlg message handlers

BOOL CMeasurementDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    CString ValStr, TypeStr;
    CWnd *pWnd;

    pWnd = GetDlgItem(IDC_NAMESTATIC);
    pWnd->SetWindowText(m_pMeas->m_Name);

    pWnd = GetDlgItem(IDC_VALUESTATIC);
    switch (m_pMeas->m_Value.vt)
    {
    case VT_I4:
        ValStr.Format("%d", m_pMeas->m_Value.lVal);
        TypeStr = "Long";
        break;
    case VT_R8:
        ValStr.Format("%.8lf", m_pMeas->m_Value.dblVal);
        TypeStr = "Double";
        break;
    case VT_BSTR:
        ValStr = m_pMeas->m_Value.bstrVal;
        TypeStr = "String";
        break;
    default:
        ValStr = "Unknown";
        TypeStr = "Unknown type";
    }
 
    pWnd->SetWindowText(ValStr);
    pWnd = GetDlgItem(IDC_TYPESTATIC);
    pWnd->SetWindowText(TypeStr);

    return TRUE;  // return TRUE unless you set the focus to a control
    // EXCEPTION: OCX Property Pages should return FALSE
}
