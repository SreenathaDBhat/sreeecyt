#pragma once

class Measurement;

// CMeasurementDlg dialog

class CMeasurementDlg : public CDialog
{
	DECLARE_DYNAMIC(CMeasurementDlg)

public:
	CMeasurementDlg(Measurement *pM, CWnd* pParent = NULL);   // standard constructor
	virtual ~CMeasurementDlg();

// Dialog Data
	enum { IDD = IDD_MEASUREMENTDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
    virtual BOOL OnInitDialog();

    Measurement *m_pMeas;
};
