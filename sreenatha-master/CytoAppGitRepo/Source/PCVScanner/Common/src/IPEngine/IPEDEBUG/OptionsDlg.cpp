// OptionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ipedebug.h"
#include "OptionsDlg.h"
#include "mainfrm.h"
#include "xbrowseforfolder.h"
#include ".\optionsdlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COptionsDlg dialog


COptionsDlg::COptionsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COptionsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(COptionsDlg)
	m_ImageDir = _T("");
	m_ScriptDir = _T("");
	m_TempDir = _T("");
    m_EffectDir = _T("");
	m_8bpp = FALSE;
	//}}AFX_DATA_INIT
}


void COptionsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptionsDlg)
	DDX_Text(pDX, IDC_IMAGEDIREDIT, m_ImageDir);
	DDX_Text(pDX, IDC_SCRIPTDIREDIT, m_ScriptDir);
	DDX_Text(pDX, IDC_TEMPDIREDIT, m_TempDir);
	DDX_Text(pDX, IDC_EFFECTDIREDIT, m_EffectDir);
	DDX_Check(pDX, IDC_CHECK8BPP, m_8bpp);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COptionsDlg, CDialog)
	//{{AFX_MSG_MAP(COptionsDlg)
	ON_BN_CLICKED(IDC_TEMPDIRBTN, OnTempdirbtn)
	ON_BN_CLICKED(IDC_IMAGEDIRBTN, OnImagedirbtn)
	ON_BN_CLICKED(IDC_SCRIPTDIRBTN, OnScriptdirbtn)
    ON_BN_CLICKED(IDC_EFFECTDIRBTN, OnEffectdirbtn)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COptionsDlg message handlers

BOOL COptionsDlg::OnInitDialog() 
{
	CIPERegistry IPER;

	CDialog::OnInitDialog();
	
	m_TempDir = IPER.GetRegItem("Directories", "Temp");
	if (m_TempDir == "")
		m_TempDir = "\\Temp";

	m_ImageDir = IPER.GetRegItem("Directories", "Images");
	if (m_ImageDir == "")
		m_ImageDir = "\\Images";

	m_ScriptDir = IPER.GetRegItem("Directories", "Scripts");
	if (m_ScriptDir == "")
		m_ScriptDir = "\\Scripts";

    m_EffectDir = IPER.GetRegItem("Directories", "Effects");
	if (m_EffectDir == "")
		m_EffectDir = "\\Effects";

    m_8bpp = IPER.GetRegItem("Options", "8bpp") == "1" ? TRUE : FALSE;

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void COptionsDlg::OnOK() 
{
	CIPERegistry IPER;

	if (UpdateData(TRUE))
	{
		IPER.SetRegItem("Directories", "Temp", m_TempDir);
		IPER.SetRegItem("Directories", "Images", m_ImageDir);
		IPER.SetRegItem("Directories", "Scripts", m_ScriptDir);
		IPER.SetRegItem("Directories", "Effects", m_EffectDir);
		IPER.SetRegItem("Options", "8bpp", m_8bpp ? "1" : "0");
		CDialog::OnOK();
	}
}

void COptionsDlg::BrowseForFolder(CString &csFolder)
{
    TCHAR   Folder[1024];

    if (XBrowseForFolder(this->GetSafeHwnd(), csFolder, Folder, 1024))
        csFolder.Format("%s", Folder);

    UpdateData(FALSE);
}

void COptionsDlg::OnTempdirbtn() 
{
    BrowseForFolder(m_TempDir);
}

void COptionsDlg::OnImagedirbtn() 
{
    BrowseForFolder(m_ImageDir);	
}

void COptionsDlg::OnScriptdirbtn() 
{
    BrowseForFolder(m_ScriptDir);
}
	
void COptionsDlg::OnEffectdirbtn()
{
    BrowseForFolder(m_EffectDir);
}
