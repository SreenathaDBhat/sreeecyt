#if !defined(AFX_OPTIONSDLG_H__09BE081A_C544_4232_9AA6_67BF3593D4B3__INCLUDED_)
#define AFX_OPTIONSDLG_H__09BE081A_C544_4232_9AA6_67BF3593D4B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OptionsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COptionsDlg dialog

class COptionsDlg : public CDialog
{
// Construction
public:
	COptionsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(COptionsDlg)
	enum { IDD = IDD_OPTIONSDLG };
	CString	m_ImageDir;
	CString	m_ScriptDir;
	CString	m_TempDir;
	CString	m_EffectDir;
	BOOL m_8bpp;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COptionsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COptionsDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnTempdirbtn();
	afx_msg void OnImagedirbtn();
	afx_msg void OnScriptdirbtn();
    afx_msg void OnEffectdirbtn();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

    void BrowseForFolder(CString &Folder);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPTIONSDLG_H__09BE081A_C544_4232_9AA6_67BF3593D4B3__INCLUDED_)
