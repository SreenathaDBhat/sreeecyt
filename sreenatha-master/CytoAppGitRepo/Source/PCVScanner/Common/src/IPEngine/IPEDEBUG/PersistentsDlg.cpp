// PersistentsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ipedebug.h"
#include "MainFrm.h"
#include "PersistentsDlg.h"
#include "ScriptProc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPersistentsDlg dialog


CPersistentsDlg::CPersistentsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPersistentsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPersistentsDlg)
	m_csName = _T("");
	m_csValue = _T("");
	//}}AFX_DATA_INIT
}


void CPersistentsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPersistentsDlg)
	DDX_Control(pDX, IDC_NEWBUTTON, m_NewBtn);
	DDX_Control(pDX, IDC_DELBUTTON, m_DelBtn);
	DDX_Control(pDX, IDC_ADDBUTTON, m_AddBtn);
	DDX_Control(pDX, IDC_VARLIST, m_VarList);
	DDX_Text(pDX, IDC_NAMEEDIT, m_csName);
	DDV_MaxChars(pDX, m_csName, 64);
	DDX_Text(pDX, IDC_VALUEEDIT, m_csValue);
	DDV_MaxChars(pDX, m_csValue, 255);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPersistentsDlg, CDialog)
	//{{AFX_MSG_MAP(CPersistentsDlg)
	ON_LBN_SELCHANGE(IDC_VARLIST, OnSelchangeVarlist)
	ON_EN_KILLFOCUS(IDC_VALUEEDIT, OnKillfocusValueedit)
	ON_BN_CLICKED(IDC_DBLRADIO, OnDblradio)
	ON_BN_CLICKED(IDC_INTRADIO, OnIntradio)
	ON_BN_CLICKED(IDC_DELBUTTON, OnDelbutton)
	ON_BN_CLICKED(IDC_NEWBUTTON, OnNewbutton)
	ON_BN_CLICKED(IDC_ADDBUTTON, OnAddbutton)
	ON_EN_CHANGE(IDC_NAMEEDIT, OnChangeNameedit)
	ON_EN_CHANGE(IDC_VALUEEDIT, OnChangeValueedit)
	ON_BN_CLICKED(IDC_STRINGRADIO, OnStringradio)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPersistentsDlg message handlers

/////////////////////////////////////////////////////////////////////////////
// Fill in the new values for the selected var in the dialog box
/////////////////////////////////////////////////////////////////////////////

void CPersistentsDlg::OnSelchangeVarlist() 
{
	int			i;
	CString		Name, Value;
	BOOL		Found = FALSE;
	BOOL		Ok = FALSE;

	// Get the selected variable name
	for (i = 0; i < m_VarList.GetCount() && !Found; i++)
	{
		if (m_VarList.GetSel(i) > 0)
		{
			m_VarList.GetText(i, m_csName);
			Found = TRUE;
		}
	}

	// Found it so fill in the rest of the dialog
	if (Found)
	{
		// Now get the value
		m_pPersistentVariables->GetValue(m_csName, &m_Var);
		// Show the value
		switch (m_Var.vt)
		{
		case VT_I4:
			CheckRadioButton(IDC_INTRADIO, IDC_STRINGRADIO, IDC_INTRADIO);
			m_csValue.Format("%d", m_Var.lVal);
			Ok = TRUE;
			break;
		case VT_R8:
			CheckRadioButton(IDC_INTRADIO, IDC_STRINGRADIO, IDC_DBLRADIO);
			m_csValue.Format("%lf", m_Var.dblVal);
			Ok = TRUE;
			break;
        case VT_BSTR:
			CheckRadioButton(IDC_INTRADIO, IDC_STRINGRADIO, IDC_STRINGRADIO);
			if (m_Var.bstrVal)
                m_csValue = m_Var.bstrVal;
            else
                m_csValue = "";
			Ok = TRUE;
			break;
		}

		// Enable the delete button if valid var found
		if (Ok)
		{
			m_DelBtn.EnableWindow(TRUE);
			m_AddBtn.EnableWindow(FALSE);
			m_NewBtn.EnableWindow(TRUE);
			m_bNewVar = FALSE;
		}
	}

	UpdateData(FALSE);
}

/////////////////////////////////////////////////////////////////////////////
// Update the dialog controls
/////////////////////////////////////////////////////////////////////////////

void CPersistentsDlg::Update(void)
{
	POSITION    P;
	BOOL		Ok = TRUE;
	CString		Name;
	COleVariant	V;

	// Good so far so now need to go through fill in the list box
	P = m_pPersistentVariables->m_VarBank.GetStartPosition();
	// All members
	while (P && Ok)
	{
		// Get the key and the value associated with the key
		m_pPersistentVariables->m_VarBank.GetNextAssoc(P, Name, V);
		// Add it to the list box
		m_VarList.AddString(Name);
	}
}

/////////////////////////////////////////////////////////////////////////////
// Initialise the dialog, fill in the list box with persistent var data
/////////////////////////////////////////////////////////////////////////////

BOOL CPersistentsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Create the persistents class
	m_pPersistentVariables = new VarMapBank;
    // Load the defaults
    m_pPersistentVariables->Load(PERSISTENTVARS_FILENAME);
	// Update the list box etc
	Update();

	m_bNewVar = FALSE;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/////////////////////////////////////////////////////////////////////////////
// After the dialog is destroyed commit vars back to disk
/////////////////////////////////////////////////////////////////////////////

void CPersistentsDlg::PostNcDestroy() 
{
	if (m_pPersistentVariables)
	{
		if (!m_pPersistentVariables->Save(PERSISTENTVARS_FILENAME))
			MessageBox("Persistents could not be committed to disk", "Error", MB_OK | MB_ICONEXCLAMATION);
        m_pPersistentVariables->ResetBank();
		delete m_pPersistentVariables;
	}

	CDialog::PostNcDestroy();
}

/////////////////////////////////////////////////////////////////////////////
// The value edit box has lost focus so save any changes
/////////////////////////////////////////////////////////////////////////////

void CPersistentsDlg::OnKillfocusValueedit() 
{
	BOOL Ok = FALSE;
	CString Msg;
	CWnd	*pVarValWnd;

	if (!m_bNewVar)
	{
		if (UpdateData(TRUE))
		{
			switch (m_Var.vt)
			{
			case VT_I4:
				if (sscanf(m_csValue, "%ld", &m_Var.lVal) == 1)
					Ok = TRUE;
				break;
			case VT_R8:
				if (sscanf(m_csValue, "%lf", &m_Var.dblVal) == 1)
					Ok = TRUE;
				break;
            case VT_BSTR:
                // Free the old BSTR
                SysFreeString(m_Var.bstrVal);
                // Set the new one
                m_Var.bstrVal = m_csValue.AllocSysString();
                Ok = TRUE;
                break;
			}
		}
		
		if (!Ok)
		{
			// Output an error 
			Msg.Format("Could not set %s = %s", m_csName, m_csValue);
			MessageBox(Msg, "Error", MB_OK | MB_ICONEXCLAMATION);
			
			// Set the focus back on the var value
			pVarValWnd = GetDlgItem(IDC_VALUEEDIT);
			pVarValWnd->SetFocus();
		}
		else
		{
            CMainFrame *pMainWnd = (CMainFrame *) AfxGetMainWnd();

			// Save the value to disk
			m_pPersistentVariables->SetValue(m_csName, m_Var);
			m_pPersistentVariables->Save(PERSISTENTVARS_FILENAME);
			// Reload into the script processor
			pMainWnd->m_pScriptProcessor->LoadPersistents();
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// The radio buttons
/////////////////////////////////////////////////////////////////////////////

void CPersistentsDlg::OnDblradio() 
{
	if (!m_bNewVar)
		CheckRadioButton(IDC_INTRADIO, IDC_STRINGRADIO, IDC_INTRADIO);	
	else
		CheckRadioButton(IDC_INTRADIO, IDC_STRINGRADIO, IDC_DBLRADIO);	
}

void CPersistentsDlg::OnIntradio() 
{
	if (!m_bNewVar)
		CheckRadioButton(IDC_INTRADIO, IDC_STRINGRADIO, IDC_DBLRADIO);
	else
		CheckRadioButton(IDC_INTRADIO, IDC_STRINGRADIO, IDC_INTRADIO);	
}

void CPersistentsDlg::OnStringradio() 
{
	if (!m_bNewVar)
		CheckRadioButton(IDC_INTRADIO, IDC_STRINGRADIO, IDC_STRINGRADIO);
	else
		CheckRadioButton(IDC_INTRADIO, IDC_STRINGRADIO, IDC_INTRADIO);	
	
}

/////////////////////////////////////////////////////////////////////////////
// Delete persistents
/////////////////////////////////////////////////////////////////////////////

void CPersistentsDlg::OnDelbutton() 
{
	int i;
	int NumItems;
    CMainFrame    *pMainWnd = (CMainFrame *) AfxGetMainWnd();

	NumItems = m_VarList.GetCount();
	
	// Delete all elements in the list box
	for (i = 0; i < NumItems; i++)
	{
		m_VarList.DeleteString(0);
	}

	
	m_DelBtn.EnableWindow(FALSE);
	// Reset the value
	pMainWnd->m_pScriptProcessor->CommitPersistents();
	m_pPersistentVariables->m_VarBank.RemoveKey(m_csName);
	pMainWnd->m_pScriptProcessor->DeletePersistent(m_csName);
	m_csName = "";
	m_csValue = "";
	UpdateData(FALSE);
	m_pPersistentVariables->Save(PERSISTENTVARS_FILENAME);
	// Reload in the script processor
	pMainWnd->m_pScriptProcessor->LoadPersistents();
	// Update
	Update();
}

void CPersistentsDlg::OnNewbutton() 
{
	m_csName = "";
	m_csValue = "";
	m_NewBtn.EnableWindow(FALSE);
	UpdateData(FALSE);
	m_bNewVar = TRUE;
}

void CPersistentsDlg::OnAddbutton() 
{
	BOOL Ok = FALSE;
	CString Msg;
	int Btn;
	CWnd *pVarValWnd;
	int i;
	int NumItems;
    CMainFrame *pMainWnd = (CMainFrame *) AfxGetMainWnd();


	pVarValWnd = GetDlgItem(IDC_VALUEEDIT);
	Btn = GetCheckedRadioButton(IDC_INTRADIO, IDC_STRINGRADIO);

	switch (Btn)
	{
	case IDC_INTRADIO:
		m_Var.vt = VT_I4;
		Ok = TRUE;
		break;
	case IDC_DBLRADIO:
		m_Var.vt = VT_R8;
		Ok = TRUE;
		break;
    case IDC_STRINGRADIO:
        m_Var.vt = VT_BSTR;
        Ok = TRUE;
	}
	
	if (UpdateData(TRUE) && Ok)
	{
		Ok = FALSE;
		if (UpdateData(TRUE))
		{
			switch (m_Var.vt)
			{
			case VT_I4:
				if (sscanf(m_csValue, "%ld", &m_Var.lVal) == 1)
					Ok = TRUE;
				break;
			case VT_R8:
				if (sscanf(m_csValue, "%lf", &m_Var.dblVal) == 1)
					Ok = TRUE;
				break;
            case VT_BSTR:
                // Free the old BSTR
                SysFreeString(m_Var.bstrVal);
                // Set the new one
                m_Var.bstrVal = m_csValue.AllocSysString();
                Ok = TRUE;
                break;
			}
		}
		
		// Enable the delete button if valid var found
		if (Ok)
		{
			m_DelBtn.EnableWindow(TRUE);
			m_AddBtn.EnableWindow(FALSE);
			m_NewBtn.EnableWindow(TRUE);
			m_bNewVar = FALSE;
			
			NumItems = m_VarList.GetCount();
			
			// Delete all elements in the list box
			for (i = 0; i < NumItems; i++)
			{
				m_VarList.DeleteString(0);
			}
			// Add the value to the bank and then commit to disk
			m_pPersistentVariables->InitValue(m_csName, m_Var);
			m_pPersistentVariables->Save(PERSISTENTVARS_FILENAME);
			// Reload in the script processor
			pMainWnd->m_pScriptProcessor->LoadPersistents();
			// Update the list box
			Update();
		}
		else
		{
			// Output an error 
			Msg.Format("Could not set %s = %s", m_csName, m_csValue);
			MessageBox(Msg, "Error", MB_OK | MB_ICONEXCLAMATION);
			
			// Set the focus back on the var value
			pVarValWnd->SetFocus();
		}
	}
	else
	{
		// Output an error 
		Msg.Format("Variable %s has no type", m_csName);
		MessageBox(Msg, "Error", MB_OK | MB_ICONEXCLAMATION);
		
		// Set the focus back on the var value
		pVarValWnd->SetFocus();
	}
}

void CPersistentsDlg::OnChangeNameedit() 
{
	m_AddBtn.EnableWindow(TRUE);
}

void CPersistentsDlg::OnChangeValueedit() 
{
	CWnd *pVarValWnd;
	int Btn;
	BOOL Ok = FALSE;
	CString Msg;

	if (m_csName != "")
	{
		pVarValWnd = GetDlgItem(IDC_VALUEEDIT);
		Btn = GetCheckedRadioButton(IDC_INTRADIO, IDC_STRINGRADIO);
		
		switch (Btn)
		{
		case IDC_INTRADIO:
			m_Var.vt = VT_I4;
			Ok = TRUE;
			break;
		case IDC_DBLRADIO:
			m_Var.vt = VT_R8;
			Ok = TRUE;
			break;
        case IDC_STRINGRADIO:
            m_Var.vt = VT_BSTR;
            Ok = TRUE;
            break;
		}
		
		if (UpdateData(TRUE) && Ok)
		{
			Ok = FALSE;
			if (UpdateData(TRUE))
			{
				switch (m_Var.vt)
				{
				case VT_I4:
					if (sscanf(m_csValue, "%ld", &m_Var.lVal) == 1)
						Ok = TRUE;
					break;
				case VT_R8:
					if (sscanf(m_csValue, "%lf", &m_Var.dblVal) == 1)
						Ok = TRUE;
					break;
                case VT_BSTR:
                    // Free the old BSTR
                    SysFreeString(m_Var.bstrVal);
                    // Set the new one
                    m_Var.bstrVal = m_csValue.AllocSysString();
                    Ok = TRUE;
                    break;
                }
            }
        }
	}
}

