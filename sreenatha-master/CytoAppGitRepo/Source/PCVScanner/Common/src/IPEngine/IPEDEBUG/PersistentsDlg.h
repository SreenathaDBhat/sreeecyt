#if !defined(AFX_PERSISTENTSDLG_H__6B55589E_9CB6_4D4B_84EF_93A519039622__INCLUDED_)
#define AFX_PERSISTENTSDLG_H__6B55589E_9CB6_4D4B_84EF_93A519039622__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PersistentsDlg.h : header file
//

#include "scriptpersist.h"

/////////////////////////////////////////////////////////////////////////////
// CPersistentsDlg dialog

class CPersistentsDlg : public CDialog
{
// Construction
public:
	CPersistentsDlg(CWnd* pParent = NULL);   // standard constructor
    // Persistent variable storage
    VarMapBank      *m_pPersistentVariables;


// Dialog Data
	//{{AFX_DATA(CPersistentsDlg)
	enum { IDD = IDD_PERSISTENTS };
	CButton	m_NewBtn;
	CButton	m_DelBtn;
	CButton	m_AddBtn;
	CListBox	m_VarList;
	CString	m_csName;
	CString	m_csValue;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPersistentsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:
	VARIANT				m_Var;
	void				Update(void);

	BOOL				m_bNewVar;

	// Generated message map functions
	//{{AFX_MSG(CPersistentsDlg)
	afx_msg void OnSelchangeVarlist();
	virtual BOOL OnInitDialog();
	afx_msg void OnKillfocusValueedit();
	afx_msg void OnDblradio();
	afx_msg void OnIntradio();
	afx_msg void OnDelbutton();
	afx_msg void OnNewbutton();
	afx_msg void OnAddbutton();
	afx_msg void OnChangeNameedit();
	afx_msg void OnChangeValueedit();
	afx_msg void OnStringradio();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PERSISTENTSDLG_H__6B55589E_9CB6_4D4B_84EF_93A519039622__INCLUDED_)
