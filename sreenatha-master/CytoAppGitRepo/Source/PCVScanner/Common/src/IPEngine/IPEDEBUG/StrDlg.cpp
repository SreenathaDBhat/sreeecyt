// StrDlg.cpp : implementation file
//

#include "stdafx.h"

#include "StrDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStrDlg dialog


CStrDlg::CStrDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CStrDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CStrDlg)
	m_TheString = _T("");
	//}}AFX_DATA_INIT
}


void CStrDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStrDlg)
	DDX_Text(pDX, IDC_STRING, m_TheString);
	DDV_MaxChars(pDX, m_TheString, 255);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CStrDlg, CDialog)
	//{{AFX_MSG_MAP(CStrDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStrDlg message handlers

void CStrDlg::OnOK() 
{
    UpdateData(TRUE);
    m_StringValue = m_TheString;

	CDialog::OnOK();
}
