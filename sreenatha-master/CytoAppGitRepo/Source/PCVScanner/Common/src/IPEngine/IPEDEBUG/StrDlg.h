#if !defined(AFX_STRDLG_H__824A60E9_4A61_432A_A000_BC97B2595FAC__INCLUDED_)
#define AFX_STRDLG_H__824A60E9_4A61_432A_A000_BC97B2595FAC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StrDlg.h : header file
//
#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// CStrDlg dialog

class CStrDlg : public CDialog
{
// Construction
public:
	CStrDlg(CWnd* pParent = NULL);   // standard constructor
    CString m_StringValue;

// Dialog Data
	//{{AFX_DATA(CStrDlg)
	enum { IDD = IDD_STRINGDLG };
	CString	m_TheString;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStrDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CStrDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STRDLG_H__824A60E9_4A61_432A_A000_BC97B2595FAC__INCLUDED_)
