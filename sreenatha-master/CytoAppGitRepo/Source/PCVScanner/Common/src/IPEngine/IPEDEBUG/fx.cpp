//////////////////////////////////////////////////////////////////////////////////////////////
// The syntax highlighting parser for VB script
//
// Written By Karl Ratcliff 20112001
//
//
//////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include "IPEDEBUGFxView.h"
#include "fx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//	C++ keywords (MSVC5.0 + POET5.0)
static LPTSTR s_apszFxKeywordList[] =
{
	_T("option"),
	_T("explicit"),
	_T("true"),
	_T("false"),
	_T("break"),
	_T("end"),
	_T("function"),
	_T("then"),
	_T("exit"),
	_T("true"),
	_T("case"),
	_T("for"),
	_T("sub"),
	_T("const"),
	_T("goto"),
	_T("if"),
	_T("elseif"),
	_T("to"),
	_T("next"),
	_T("do"),
	_T("dot"),
	_T("set"),
	_T("Let"),
	_T("Do"),
	_T("Loop"),
	_T("Until"),
	_T("Select"),
	_T("Case"),
	_T("While"),
	_T("Wend"),
	_T("Const"),
	_T("else"),
	_T("main"),
	_T("byref"),
	_T("byval"),
	_T("createobject"),
	_T("mod"),
	_T("is"),
	_T("eqv"),
	_T("imp"),
	_T("abs"),
	_T("atn"),
	_T("cos"),
	_T("exp"),
	_T("fix"),
	_T("and"),
	_T("or"),
	_T("xor"),
	_T("not"),
	_T("int"),
	_T("Log"),
	_T("randomize"),
	_T("Rnd"),
	_T("Round"),
	_T("Sgn"),
	_T("sin"),
	_T("sqr"),
	_T("tan"),
	_T("CDate"),
	_T("Date"),
	_T("DateAdd"),
	_T("DateDiff"),
	_T("DatePart"),
	_T("DateSerial"),
	_T("DateValue"),
	_T("Day"),
	_T("Hour"),
	_T("IsDate"),
	_T("Minute"),
	_T("Month"),
	_T("MonthName"),
	_T("now"),
	_T("Second"),
	_T("time"),
	_T("timer"),
	_T("timeserial"),
	_T("timevalue"),
	_T("WeekDay"),
	_T("weekday"),
	_T("WeekDayName"),
	_T("year"),
	_T("array"),
	_T("erase"),
	_T("each"),
	_T("isarray"),
	_T("lbound"),
	_T("redim"),
	_T("UBound"),
	_T("FormatCurrency"),
	_T("FormatDateTime"),
	_T("FormatNumber"),
	_T("FormatPercent"),
	_T("Texture"),
	_T("Sampler"),
	_T("Sampler_state"),
	_T("Struct"),
	_T("float2"),
	_T("LenB"),
	_T("LTrim"),
	_T("Mid"),
	_T("MidB"),
	_T("Replace"),
	_T("Right"),
	_T("RTrim"),
	_T("Space"),
	_T("Split"),
	_T("StrComp"),
	_T("String"),
    _T("StrReverse"),
	_T("Trim"),
	_T("UCase"),
	_T("Asc"),
	_T("AscB"),
	_T("AscW"),
	_T("CBool"),
	_T("CByte"),
	_T("CCur"),
	_T("CDbl"),
	_T("Chr"),
	_T("ChrB"),
	_T("ChrW"),
	_T("CInt"),
	_T("CLng"),
	_T("CSng"),
	_T("CStr"),
	_T("Hex"),
	_T("Oct"),
	_T("Eval"),
	_T("float"),
	_T("float4"),
	_T("Register"),
	_T("NONE"),
	_T("Tex2D"),
	_T("Tex1D"),
	_T("mul"),
	_T("technique"),
	_T("compile"),
	_T("vs_3_0"),
	_T("ps_3_0"),
	_T("vs_2_0"),
	_T("ps_2_0"),
	_T("pass"),
	_T("Return"),
	_T("typename"),
	_T("vartype"),
	_T("call"),
	_T("nothing"),
	_T("class"),
	_T("new"),
	NULL
};

BOOL IsFxKeyword(LPCTSTR pszChars, int nLength)
{
	for (int L = 0; s_apszFxKeywordList[L] != NULL; L ++)
	{
		if (_strnicmp(s_apszFxKeywordList[L], pszChars, nLength) == 0
				 && s_apszFxKeywordList[L][nLength] == 0)
			return TRUE;
	}
	return FALSE;
}

static BOOL IsFxNumber(LPCTSTR pszChars, int nLength)
{
	if (nLength > 2 && pszChars[0] == '0' && pszChars[1] == 'x')
	{
		for (int I = 2; I < nLength; I++)
		{
			if (isdigit(pszChars[I]) || (pszChars[I] >= 'A' && pszChars[I] <= 'F') ||
										(pszChars[I] >= 'a' && pszChars[I] <= 'f'))
				continue;
			return FALSE;
		}
		return TRUE;
	}
	if (! isdigit(pszChars[0]))
		return FALSE;
	for (int I = 1; I < nLength; I++)
	{
		if (! isdigit(pszChars[I]) && pszChars[I] != '+' &&
			pszChars[I] != '-' && pszChars[I] != '.' && pszChars[I] != 'e' &&
			pszChars[I] != 'E')
			return FALSE;
	}
	return TRUE;
}

#define DEFINE_BLOCK(pos, colorindex)	\
	ASSERT((pos) >= 0 && (pos) <= nLength);\
	if (pBuf != NULL)\
	{\
		if (nActualItems == 0 || pBuf[nActualItems - 1].m_nCharPos <= (pos)){\
		pBuf[nActualItems].m_nCharPos = (pos);\
		pBuf[nActualItems].m_nColorIndex = (colorindex);\
		nActualItems ++;}\
	}

#define COOKIE_COMMENT			0x0001
#define COOKIE_PREPROCESSOR		0x0002
#define COOKIE_EXT_COMMENT		0x0004
#define COOKIE_STRING			0x0008
#define COOKIE_CHAR				0x0010

DWORD CIPEDEBUGFxView::ParseLine(DWORD dwCookie, int nLineIndex, TEXTBLOCK *pBuf, int &nActualItems)
{
	int I, nLength = GetLineLength(nLineIndex);
	if (nLength <= 0)
		return dwCookie & COOKIE_EXT_COMMENT;

	BOOL bRedefineBlock = TRUE;
	BOOL bDecIndex  = FALSE;
	BOOL bFirstChar = (dwCookie & ~COOKIE_EXT_COMMENT) == 0;
	int nIdentBegin = -1;
	LPTSTR pszChars = GetLineChars(nLineIndex);     
	
	for (I = 0; ; I++)
	{
		if (bRedefineBlock)
		{
			int nPos = I;
			if (bDecIndex)
				nPos--;
			if (dwCookie & (COOKIE_COMMENT | COOKIE_EXT_COMMENT))
			{
				DEFINE_BLOCK(nPos, COLORINDEX_COMMENT);
			}
			else
			if (dwCookie & (COOKIE_CHAR | COOKIE_STRING))
			{
				DEFINE_BLOCK(nPos, COLORINDEX_STRING);
			}
			else
			if (dwCookie & COOKIE_PREPROCESSOR)
			{
				DEFINE_BLOCK(nPos, COLORINDEX_PREPROCESSOR);
			}
			else
			{
				DEFINE_BLOCK(nPos, COLORINDEX_NORMALTEXT);
			}
			bRedefineBlock = FALSE;
			bDecIndex      = FALSE;
		}

		if (I == nLength)
			break;

		if (dwCookie & COOKIE_COMMENT)
		{
			DEFINE_BLOCK(I, COLORINDEX_COMMENT);
			dwCookie |= COOKIE_COMMENT;
			break;
		}

		//	String constant "...."
		if (dwCookie & COOKIE_STRING)
		{
			if (pszChars[I] == '"' && (I == 0 || pszChars[I - 1] != '\\'))
			{
				dwCookie &= ~COOKIE_STRING;
				bRedefineBlock = TRUE;
			}
			continue;
		}


		if (I > 0 && pszChars[I] == '/' && pszChars[I - 1] == '/')
		{
			DEFINE_BLOCK(I - 1, COLORINDEX_COMMENT);
			dwCookie |= COOKIE_COMMENT;
			break;
		}

		//	Preprocessor directive #....
		if (dwCookie & COOKIE_PREPROCESSOR)
		{
			if (I > 0 && pszChars[I] == '*' && pszChars[I - 1] == '/')
			{
				DEFINE_BLOCK(I - 1, COLORINDEX_COMMENT);
				dwCookie |= COOKIE_EXT_COMMENT;
			}
			continue;
		}

		//	Normal text
		if (pszChars[I] == '"')
		{
			DEFINE_BLOCK(I, COLORINDEX_STRING);
			dwCookie |= COOKIE_STRING;
			continue;
		}

		if (I > 0 && pszChars[I] == '*' && pszChars[I - 1] == '/')
		{
			DEFINE_BLOCK(I - 1, COLORINDEX_COMMENT);
			dwCookie |= COOKIE_EXT_COMMENT;
			continue;
		}

		if (pszChars[I] == '\'')
		{
			DEFINE_BLOCK(I, COLORINDEX_PREPROCESSOR);
			dwCookie |= COOKIE_PREPROCESSOR;
			continue;
		}

		if (pBuf == NULL)
			continue;	//	We don't need to extract keywords,
						//	for faster parsing skip the rest of loop

		if (isalnum(pszChars[I]) || pszChars[I] == '_' || pszChars[I] == '.')
		{
			if (nIdentBegin == -1)
				nIdentBegin = I;
		}
		else
		{
			if (nIdentBegin >= 0)
			{
				if (IsFxKeyword(pszChars + nIdentBegin, I - nIdentBegin))
				{
					DEFINE_BLOCK(nIdentBegin, COLORINDEX_KEYWORD);
				}
				else
				if (IsFxNumber(pszChars + nIdentBegin, I - nIdentBegin))
				{
					DEFINE_BLOCK(nIdentBegin, COLORINDEX_NUMBER);
				}
				bRedefineBlock = TRUE;
				bDecIndex = TRUE;
				nIdentBegin = -1;
			}
		}
	}

	if (nIdentBegin >= 0)
	{
		if (IsFxKeyword(pszChars + nIdentBegin, I - nIdentBegin))
		{
			DEFINE_BLOCK(nIdentBegin, COLORINDEX_KEYWORD);
		}
		else
		if (IsFxNumber(pszChars + nIdentBegin, I - nIdentBegin))
		{
			DEFINE_BLOCK(nIdentBegin, COLORINDEX_NUMBER);
		}
	}

	if (pszChars[nLength - 1] != '\\')
		dwCookie &= COOKIE_EXT_COMMENT;
	return dwCookie;
}
