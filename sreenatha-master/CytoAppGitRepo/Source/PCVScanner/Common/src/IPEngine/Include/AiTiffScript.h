#if !defined(AFX_AITIFFSCRIPT_H__47ED03A1_DDD5_4EF2_98B8_054452ED033A__INCLUDED_)
#define AFX_AITIFFSCRIPT_H__47ED03A1_DDD5_4EF2_98B8_054452ED033A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AiTiffScript.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// AiTiffScript command target

class AiTiffScript : public CCmdTarget, public ScriptObject
{
	DECLARE_DYNCREATE(AiTiffScript)

	AiTiffScript();           // protected constructor used by dynamic creation

// Attributes
public:
	virtual ~AiTiffScript();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AiTiffScript)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(AiTiffScript)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(AiTiffScript)
	afx_msg long CreateAITIFF();
	afx_msg BOOL LoadAITIFF(long hnd, LPCTSTR filename);
	afx_msg void FreeAITIFF(long hnd);
	afx_msg void CreateAllMIPMapsAITIFF(long hnd);
	afx_msg BOOL SaveAITIFF(long hnd, LPCTSTR filename);
	afx_msg long Add8bppCmpAITIFF(long hnd, long srcimage);
	afx_msg BOOL AssimilateAITIFF(long newtiff, short oldtiff);
	afx_msg long GetCmpAITIFF(long hnd, long index);
	afx_msg long CountComponentsAITIFF(long hnd);
	afx_msg BOOL GetCmpTextAITIFF(long hnd, long index, VARIANT FAR* text);
	afx_msg BOOL SetCmpTextAITIFF(long hnd, long index, LPCTSTR text);
	afx_msg BOOL GetCmpColourAITIFF(long hnd, long index, VARIANT FAR* R, VARIANT FAR* G, VARIANT FAR* B);
	afx_msg BOOL SetCmpColourAITIFF(long hnd, long index, short R, short G, short B);
	afx_msg BOOL GetCmpVisibilityAITIFF(long hnd, long index);
	afx_msg BOOL SetCmpVisibilityAITIFF(long hnd, long index, BOOL visible);
	afx_msg long GetCmpZStackAITIFF(long hnd, long index, VARIANT FAR* spacing, VARIANT FAR* objsize);
	afx_msg BOOL SetCmpZStackAITIFF(long hnd, long index, long hnd2, long spacing, long objsize);
	afx_msg long GetRenderedAITIFF(long hnd);
	afx_msg long Add1bppCmpAITIFF(long hnd, long srcimage);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()

	void ValidHandle(long hnd);
	BYTE * MakeContigious(BYTE *datain, long w, long h, int bpp, int pitch);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AITIFFSCRIPT_H__47ED03A1_DDD5_4EF2_98B8_054452ED033A__INCLUDED_)
