////////////////////////////////////////////////////////////////////////////////
//
//	Cluster class
//

//#pragma once
#ifndef __CCLUSTER_H
#define __CCLUSTER_H
#include <afxtempl.h>

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

class CClusterList;

class UTS_API CClusterObj
{
public:
	CClusterObj(void);
	~CClusterObj(void);
	void RemoveNeighbours(void);
	void FindNeighbours(CClusterList *list, double thr_dist);
	void AddNeighbour(CClusterObj *obj);
	double NearestNeighbour(CClusterObj *obj);
	double Distance(CClusterObj *obj);

	int m_id;
	double m_x;
	double m_y;
	double m_dist;
	void *m_ext_data;

	CClusterList *m_neighbours;
	//CList <CClusterObj *, CClusterObj *> m_neighbours;
	double m_total_dist;

	// Equality assignment
	CClusterObj &operator = (const CClusterObj &rhs)
	{
		// Check for self assignment
		if (this != &rhs) {
			m_x = rhs.m_x;
			m_y = rhs.m_y;
			m_id = rhs.m_id;
			m_dist = rhs.m_dist;
		}

		// Return ref to current object for a=b=c assignments
		return *this;
	}


};

class UTS_API CClusterList
{

public:
	CClusterList(void);
	~CClusterList(void);

	CList <CClusterObj *, CClusterObj *> m_obj_list;
	int m_next_object_id;

	// Object addition
	CClusterObj *AddObject(double x, double y, void *ext = NULL);	// Create and add object at (x, y)
	bool AddObject(CClusterObj *obj);								// Add specified object

	// Object removal
	void CClusterList::RemoveObjectNeighbours();	// Remove neighbour objects from all objcts in list
	CClusterObj *RemoveObject(int obj_id);			// Remove specified object from list and return it
	void RemoveAllObjects();						// Remove all objects from list
	void RemoveNeighbourObject(int obj_id);			// Remove object with id from neighbour lists

	int MaxNeighbours();							// Find maximum number of neighbours

	CClusterObj *MinTotalDistance(int max_neighbours);

	CClusterObj *GetFirstObject();
	CClusterObj *GetNextObject();
	POSITION m_obj_pos;
};

#endif
