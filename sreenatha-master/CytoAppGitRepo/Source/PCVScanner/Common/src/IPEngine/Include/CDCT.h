#pragma once


#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif
#define CLIP(x) x > 255 ? 255 : x < 0 ? 0 : x
#define CLIPW(x) x > 65535 ? 65535 : x < 0 ? 0 : x

class UTS_API CDCT
{
public:
    CDCT(int N, int W, int H);
    ~CDCT(void);

    void ForwardDCT(double **pDCTBuffer, BYTE *ImgBuffer);
    void ForwardDCT(double **pDCTBuffer, WORD *ImgBuffer);
	void ForwardDCT8(double **pDCTBuffer, BYTE *pImgBuffer);
	void ForwardDCT8(double **pDCTBuffer, WORD *pImgBuffer);
    void DCTToImage(double *pDCTBuffer, BYTE *ImgBuffer);
    void InverseDCT(BYTE *ImgBuffer, double *pDCTBuffer);
    void InverseDCT(WORD *ImgBuffer, double *pDCTBuffer);
	void MultiplyCoeffs(double *pDCTBuffer);
	double AutoFocusMeasure(double *pDCTBuffer, BOOL ShowResults = FALSE);

private:
    void        ComputeDCTBasis(void);

    int         m_Size;         // Size of the DCT
    double      *m_pCosBuf;     // Cosine buffer
    int         m_W;            // Width and height of image
    int         m_H;

};
