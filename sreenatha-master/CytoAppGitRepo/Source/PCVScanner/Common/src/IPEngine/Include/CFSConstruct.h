#pragma once

#include "woolz.h"
#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif



class UTS_API CFSConstruct
{
public:
	CFSConstruct();
	~CFSConstruct();

	struct object *FSConstruct(FSCHAR *fsaddr, WZCOORD fslines, WZCOORD fscols, FSCHAR fsthresh, int fsinvert);

private:
	struct object *ctmakeobj(int lines, int cols, int intcount, int greycount);
	int cthiscancount(FSCHAR *fsaddr, WZCOORD lines, WZCOORD cols, register FSCHAR thresh, int *intcountptr);
	int ctlowscancount(FSCHAR *fsaddr, WZCOORD lines, WZCOORD cols, register FSCHAR thresh, int *intcountptr);
	void cthiscanbuild(struct object *obj, FSCHAR *fsaddr, FSCHAR thresh);
    void ctlowscanbuild(struct object *obj, FSCHAR *fsaddr, FSCHAR thresh, int fsinvert);
	void fsmemerror(int code, char *estr);
	int fscheckerror();

	int FSCAPTURE;
	int memerror;
};