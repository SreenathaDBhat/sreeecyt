#include "woolz.h"
#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif
/*
 * allocate chain list and store information in static structure
 *
 * The size of the chain link is not easily predictable in advance.
 * We therefore permit extension of the chain list by malloc() calls,
 * and require a structure to store a sensible chain list size
 * increment.  Allocated chunks of chain list are themselves chained
 * together (using the first chain location)
 * in order to permit space-freeing; the original base of the chain
 * and most recent chunk base are also stored in this structure.
 */
struct allocthings {
	int allocsize;
	struct link *orig_base;
	struct link *chunk_base;
};


/*
 * The allocation list contains the interval end-points of the intervals of
 * the current and previous lines.  Its length therefore needs to be twice the
 * maximum number of intervals in a line of the input object (see mintcount()).
 */
struct allocbuf {
	struct link *a_link;
	struct interval a_int;
};

/*
 * A chain-link is used to hold either data (line number, interval
 * end points) or a pointer to another chain.  Do not confuse this
 * latter use with the pointer which points to the next link in
 * the same chain.
 */
struct link {
	struct link *l_link;
	union {
		int line;
		struct link *u_link;
		struct interval intv;
	} 
	l_u;
};

class UTS_API CLabel
{
public:
	CLabel();
	~CLabel();

	int LabelArea(struct object *obj, int *mm, struct object * *objlist, int nobj, int ignlns);

private:
	int label(struct object *obj, int *mm, struct object * *objlist, int nobj, int ignlns);
	int mintcount(struct intervaldomain *idom, int *maxinline);
	void merge(struct link * *ak1, struct link * *ak2, struct link *freechain);
	void join(register struct link *list1, register struct link *list2);
	void buckle(struct link *chainbase, register struct link *newlinkbase, register int length);
	struct link *getlink(register struct link *chainbase);
	void freechain(struct link *l);
	void chainfree();
	struct link *chainalloc(int flag, int n);
	struct link *newchain2(struct link *chainbase, int entry1, int entry2);
	struct link *newchain1(struct link *chainbase, int entry);
	int label_maxdim(struct object *obj, int *mm, struct object * *objlist, int nobj, int ignlns);
	struct link *addlink1(register struct link *chainbase, register struct link *chain, int entry);
	struct link *addlink2(register struct link *chainbase, register struct link *chain, int entry1, int entry2);
	void monotonejoin(struct link * *amlist1, struct link *mlist2);

	struct allocthings allocthings;
	SMALL jdqt;

	int LabelTestArea;
	int LabelTestMaxDimension;

};