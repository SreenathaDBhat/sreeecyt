#pragma once

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SCRIPTPROC_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SCRIPTPROC_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef SCRIPTPROC_EXPORTS
#define SCRIPTPROC_API __declspec(dllexport) 
#else
#define SCRIPTPROC_API __declspec(dllimport) 
#endif

#include <afxdisp.h>
#include <afxtempl.h>

typedef struct 
{
	union
	{
		struct tagFlags
		{
			BYTE IncludeInGrid		 : 1;		// Show it in the grid
			BYTE IncludeInClassifier : 1;		// Use it in the classifier
			BYTE IncludeInSort		 : 1;		// Use it for sorting
		} Flags;

		BYTE FlagValue;
	};

} ScriptMeasurementFlags;


class SCRIPTPROC_API Measurement 
{
public:	
		
    Measurement();
    ~Measurement();

    Measurement *Duplicate(void);

    void		Init(CString Name, COleVariant Value, double NormaliseFactor, int IncludeInClassifier, int IncludeInGrid, int IncludeInSort);
    double      GetValue(void);                 // Get the value as a double

    // File I/O
    BOOL Serialize(CArchive &ar);
	BOOL SerializeWithClassUse(CArchive &ar);

    double      Normalise();                    // Return the normalised measurement
    double      m_NormalisationFactor;          // Normalisation factor used for normalising the measurement (default 1.0)
    COleVariant m_Value;                        // Actual value as a variant
    CString     m_Name;                         // Measurement name
	ScriptMeasurementFlags m_Flags;				// Control flags
} ;

////////////////////////////////////////////////////////////////////////////////////////////
// Measurements Q
// New measurements are added to the end of the Q
// Each call to Get removes that measurement from the Q
////////////////////////////////////////////////////////////////////////////////////////////

class SCRIPTPROC_API CMeasures 
{
public:
    CMeasures(void);
    ~CMeasures(void);

    void        Kill(void);                     // Delete the list
    // Duplciation
    CMeasures       *Duplicate(void);

    // Add some measurements
    void            Add(CString Name, COleVariant V, double NormaliseFactor, int IncludeInClassifier, int IncludeInGrid, int IncludeInSort);
    void            Add(CString Name, VARIANT Value, double NormaliseFactor, int IncludeInClassifier, int IncludeInGrid, int IncludeInSort);
    void            Add(CString Name, int Value,     double NormaliseFactor, int IncludeInClassifier, int IncludeInGrid, int IncludeInSort);
    void            Add(CString Name, double Value,  double NormaliseFactor, int IncludeInClassifier, int IncludeInGrid, int IncludeInSort);
	
	// Sum the measurements
	double			Sum();

    Measurement     *FindByName(LPCTSTR Name);      // Search for the measurement by name

    Measurement     *Get();                         // Get the next measurement in the list and remove it too
    Measurement     *GetFirst();                    // Get the first measurement, do not remove it
    Measurement     *GetNext();                     // Get the next measurement, do not remove it
    int             GetClassCount();                // Get the number of measurements used for classification
    int             GetSortCount();                 // Get the number of measurements used for sorting
	int				GetDisplayCount();				// Get the number of measurements used for display
    // File I/O
    BOOL Serialize(CArchive &ar);
	BOOL Serialize(CArchive &ar, int Version);

private:
	int										m_Version;
    CList<Measurement *, Measurement *>     m_Measurements;
    POSITION                                m_CurrentPosition;
};
