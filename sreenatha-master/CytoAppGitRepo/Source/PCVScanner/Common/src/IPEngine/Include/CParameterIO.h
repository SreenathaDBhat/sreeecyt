/////////////////////////////////////////////////////////////////////////////////////
//
// Implements Parameter I/O Functionality - Header File
//
// Written By K Ratcliff 04042001
//
//
/////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#ifndef __CPARAMETERIO_H
#define __CPARAMETERIO_H


/////////////////////////////////////////////////////////////////////////////////////
// Defines

#define NOT_FOUND                  -1

// File extensions
#define USER_PARAMFILE_EXT          _T(".user")
#define DEFAULT_PARAMFILE_EXT       _T(".default")

/////////////////////////////////////////////////////////////////////////////////////
// Enums and typedefs

// Various Return Codes From Parameter File Access Member Functions
typedef enum {
	NO_FILES_FOUND          = 0,        // Neither user or default data file present
	DEFAULTS_FILE_FOUND     = 1,        // Default data file found
	USERPARAMS_FILE_FOUND   = 2,        // User data file found
    BOTH_FILES_FOUND        = 3,        // Both of the above
    NOPARAMETER_FOUND       = 4,        // Parameter non-existent in file
    PARAMETER_FOUND         = 8         // Parameter exists in file
} ParameterFileResult;

// Parameter File Type
typedef enum {
    USER_PARAMETER_FILE,                // Specifies the user data file
    DEFAULT_PARAMETER_FILE,             // Specifies the default data file
    NO_PARAMETER_FILE
} ParameterFileType;

// Types For Parameters
typedef enum {
    INT_PARAMETER,
    LONG_PARAMETER,
    DOUBLE_PARAMETER,
    STRING_PARAMETER
} ParameterType;

/////////////////////////////////////////////////////////////////////////////////////
// Class CParameterIO derived from CFile
//
// Has extra members for reading/writing parameter data
/////////////////////////////////////////////////////////////////////////////////////

class CParameterIO {
public:
	CParameterIO();
	~CParameterIO();

public:
    BOOL FileExists(LPCTSTR FileName);
	ParameterFileResult OpenParameters(LPCTSTR  FileName);	// Open a set of parameters
    BOOL CloseParameters();					                // Close a set of parameters
//    ParameterFileResult GetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, BOOL *Value, BOOL CodedDefault);
    ParameterFileResult GetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, int *Value, int CodedDefault);
    ParameterFileResult GetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, long *Value, long CodedDefault);
    ParameterFileResult GetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, double *Value, double CodedDefault);
    ParameterFileResult GetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, TCHAR *Value, LPCTSTR CodedDefault);
//    ParameterFileResult SetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, BOOL Value);
    ParameterFileResult SetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, int Value);
    ParameterFileResult SetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, long Value);
    ParameterFileResult SetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, double Value);
    ParameterFileResult SetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, LPCTSTR Value);

private:
    // Private member functions
    ParameterFileType SeekParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, int *FilePos);
    ParameterFileResult SetParameterEx(ParameterFileType ParamFile, LPCTSTR ParameterName, ParameterType ParamType, void *Value);
    BOOL GetFileData(ParameterFileType ParamFile);
    BOOL SetFileData(ParameterFileType ParamFile);

    // Data members
	CString     m_UsrParamFileName;		    // Name of user data file
    CString     m_DefParamFileName;         // Name of default data file
    BOOL        m_bDefDataModified;         // = TRUE If new default data to write
    BOOL        m_bUsrDataModified;         // = TRUE If new user data to write
    CString     *m_pUsrDataFile;            // In Memory Storage
    CString     *m_pDefDataFile;        
};


#endif		// #ifndef __CPARAMETERIO_H
