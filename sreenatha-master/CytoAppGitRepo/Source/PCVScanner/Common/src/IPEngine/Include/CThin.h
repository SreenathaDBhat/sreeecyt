#ifndef __CTHIN_H
#define __CTHIN_H

#include "stdafx.h"

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

class UTS_API CThin {
public:
	CThin(CUTSMemRect *pMR);
	~CThin() {};
	long ThinImage(long Handle, long MaxIter);

private:
	long peel0(BYTE *, BYTE *, long, long *, long *, long *);
	long peel(BYTE *, BYTE *, long, long *, long *);
	long ksize(long, long, long);
	long sqron(long, long, long);
	long getring(long, long, long, BYTE *);
	long thinring(BYTE *, long, int *);
	long chkconnect(BYTE *, BYTE *, long);
	long anchor(BYTE *, long);
	long erasesqr(long, long, long, int, long *);

private:
	BYTE OFF, ERASED, ON, PON;  /* values of pixels:
									 * * - ERASED value is for non-anchor
									 * * - ERASED  + 1 is for anchor
* * - ERASED increments +2 each iteration */
	BYTE	 *m_pImage;          /* input/output image */
	long     m_nWidth, m_nHeight, m_nPitch;
	long	 m_nySizeM1;                   /* y length minus 1 */
	long	 **m_pxRun;                    /* no., then x locns of 1/0 runs for each y */

	CUTSMemRect *pUTSMemRect;
};

#endif			// #ifndef __CTHIN_H
