#if !defined(AFX_CFGASSAYVARSDLG_H__06861B49_933C_11D6_9CC8_00065B84100C__INCLUDED_)
#define AFX_CFGASSAYVARSDLG_H__06861B49_933C_11D6_9CC8_00065B84100C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CfgAssayVarsDlg.h : header file
//

class CScriptParams;

#define CFGVARS_CONTROL_BASEID      24291
#define IDDEFAULT					106

#ifdef SCRIPTPROC_EXPORTS
#define SCRIPTPROC_API __declspec(dllexport) 
#else
#define SCRIPTPROC_API __declspec(dllimport) 
#endif

/////////////////////////////////////////////////////////////////////////////
// CCfgAssayVarsDlg dialog

class SCRIPTPROC_API CCfgAssayVarsDlg : public CDialog
{
    typedef CWnd *WndPtr;
// Construction
public:
	CCfgAssayVarsDlg(CScriptParams *CSP);   // standard constructor
    ~CCfgAssayVarsDlg();

    CScriptParams   *m_pCSP;                // Pointer to the script parameter list
    int             m_nWidth;               // Calculated width of the dialog
    int             m_nHeight;              // Calculated dialog height

// Dialog Data
	//{{AFX_DATA(CCfgAssayVarsDlg)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCfgAssayVarsDlg)
	public:
	virtual int DoModal();
	//}}AFX_VIRTUAL

// Implementation
protected:
    int         m_nCtrlItems;   // Number of items in the dialog
    WndPtr      *m_pControls;   // Array of control windows
    WndPtr      *m_pStatics;    // Array for borders around sliders
    CFont       m_Font;         // Font for controls etc
    int         m_nTextHeight;  // For control spacing
    int         m_nTextWidth;

	// Generated message map functions
	//{{AFX_MSG(CCfgAssayVarsDlg)
	virtual BOOL OnInitDialog();
    afx_msg void OnOk();
    afx_msg void OnDefault();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CFGASSAYVARSDLG_H__06861B49_933C_11D6_9CC8_00065B84100C__INCLUDED_)
