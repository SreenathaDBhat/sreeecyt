#if !defined(AFX_CLASSIFIERSCRIPT_H__9EDA76FD_D54A_43A4_A23E_1FA02CBE0329__INCLUDED_)
#define AFX_CLASSIFIERSCRIPT_H__9EDA76FD_D54A_43A4_A23E_1FA02CBE0329__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ClassifierScript.h : header file
//






class CSVMPredict;

/////////////////////////////////////////////////////////////////////////////
// ClassifierScript command target

class ClassifierScript : public CCmdTarget
{
	DECLARE_DYNCREATE(ClassifierScript)

	ClassifierScript();           // protected constructor used by dynamic creation


	
    CSVMPredict            *pSVMClassifier;
    CString                 m_SVMModelName;

	
// Attributes
public:
	virtual ~ClassifierScript();


// Operations
public:
	void	Reset(void);

    // Preloading of a SVM model
    BOOL    LoadSVMModel(LPCTSTR FileName);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ClassifierScript)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(ClassifierScript)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(ClassifierScript)
	
    afx_msg BOOL SVMModel(LPCTSTR FileName);
    afx_msg long SVMClassify(long NumFeatures, VARIANT FAR *Features);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLASSIFIERSCRIPT_H__9EDA76FD_D54A_43A4_A23E_1FA02CBE0329__INCLUDED_)
