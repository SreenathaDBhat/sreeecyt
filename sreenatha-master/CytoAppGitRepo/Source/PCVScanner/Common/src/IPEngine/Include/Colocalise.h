////////////////////////////////////////////////////////////////////////////////////////
//
// Colocalisation Analysis - Header
//
// Written By Karl Ratcliff 130706
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#pragma once

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

class UTS_API CColocalise
{
public:
    CColocalise(CUTSMemRect *pMR);

	~CColocalise(void);

    // Create an image which is basically a product of differences from the mean
    long PDMImage(long Image1, BYTE Mean1, long Image2, BYTE Mean2);

    // Calculate the overlap coefficients k1 and k2
    long OverlapK1K2(long Image1, long Image2, double *k1, double *k2);
    
    // Calculate Mander's overlap coefficients
    long MandersK1K2(long Image1, long Image2, double *k1, double *k2);

    // Pearsons overlap co-efficient
    double Pearsons(long Image1, long Image2, long RegionMask);

private:
	CUTSMemRect *pUTSMemRect;

};
