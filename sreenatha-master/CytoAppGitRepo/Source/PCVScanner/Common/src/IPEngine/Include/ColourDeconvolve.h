#pragma once

#include "uts.h"

typedef struct
{
    double R, G, B;
} ColourVector;


// Colour Deconvolution
long UTS_API ColourDeconvolve(CUTSMemRect &UTSMemRect, long ColourHandle, ColourVector *pCV1, ColourVector *pCV2, ColourVector *pCV3);
