/////////////////////////////////////////////////////////////////////////////
// Common definitions used by the scripting modules
//
// Written By Karl Ratcliff 31072001
//
/////////////////////////////////////////////////////////////////////////////

#ifndef COMMON_SCRIPT_H
#define COMMON_SCRIPT_H

#include "vcMemRect.h"
#include "vcGeneric.h"
#include "vcBlob.h"
#include "vcImproc.h"
#include "vcUTSDraw.h"
#include "vcUTSFile.h"
#include "WoolzIF.h"
#include "ScriptObjects.h"

/////////////////////////////////////////////////////////////////////////////
// Direct out of UTS code.
// Keyword lists used to check parameters passed as strings
/////////////////////////////////////////////////////////////////////////////

static TCHAR *ThresholdOps = 
    _T(">= > < <= ==");

static TCHAR *DirOps = 
    _T("=> <=");

static TCHAR *NeiFunOps =
	_T("AVER124 MEDIAN BLUR ")
   	_T("ERODE8 DILATE8  ERODE4 DILATE4 ")
	_T("ERODEH2 DILATEH2  ERODEV2 DILATEV2 ")
   	_T("OPEN8 CLOSE8 OPEN4 CLOSE4 ");

static TCHAR *NeiFunOpsMix =
   	_T("ERODE84 DILATE84  ERODE48 DILATE48 ")
   	_T("OPEN84 CLOSE84 OPEN48 CLOSE48 ");

static TCHAR *BitwiseOps =
	_T("AND 1&2 ")
   	_T("MORE 1-2 1&~2 ")
   	_T("COPY 1=>2 ")
   	_T("LESS 2-1 ~1&2 ")
   	_T("XOR MOD2 1^2 ")
   	_T("OR 1|2 ")
   	_T("NOR ~(1|2) ")
   	_T("EQUAL ~(1^2) ")
   	_T("MOREEQ 1|~2 ")
   	_T("COPYNOT ~1=>2 ")
   	_T("LESSEQ ~1|2 ")
   	_T("NAND ~(1&2) ");

static TCHAR *BytewiseOps =
	 _T("MIN ")
	 _T("MAX ")
	 _T("PLUS + 1+2 ")
	 _T("+S  Min(1+2,255) ")
	 _T("MINUS  -  1-2 ")
	 _T("-L  2-1 ")
	 _T("-S  Max(1-2,0) ")
	 _T("-LS  -SL  Max(2-1,0) ")
	 _T("LESSTO0  (1>=2)?1:0 ")
	 _T("MORETOFF  (1<=2)?1:FF ")
	 _T("BYTEMASK  (1<2)?0:FF ")
     _T("-A ")
     _T("-A8 ");

static TCHAR *ZoomOps = _T("INT SUB REP ");

static TCHAR *MaskOps = _T("Zeros Ones Inverse 16oid 32oid FillHoles0")
                        _T("FillHoles1 ERODE4 DILATE4 ERODE8 DILATE8")
                        _T("Frame Frame0 OnesRight OnesLeft")
                        _T("OnesDown OnesUp ZerosRight ZerosLeft")
                        _T("ZerosDown ZerosUp");

static TCHAR *OperMaskOps = _T(" 0 1 Inv 16 32 ")
    			            _T(" H0 H1 Fr0 Fr1 ")
                            _T(" 1R 1L 1D 1U  0R 0L 0D 0U ");

static TCHAR *LibraryOps = _T("SMART INTEL AIC TEEM");

#endif
