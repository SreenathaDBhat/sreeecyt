/////////////////////////////////////////////////////////////////////////////
//
// This is the GPU specific script engine keywords class. 
// This is a runtime class
// and the interface to this class gets handed over to the VB
// scripting engine.
//
// Written By Karl Ratcliff 22072005
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#if !defined(AFX_GPUSCRIPT_H__1C1EB773_1B2E_4C40_B61E_13A970B85C0B__INCLUDED_)
#define AFX_GPUSCRIPT_H__1C1EB773_1B2E_4C40_B61E_13A970B85C0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GPUScript.h : header file
//
#include <tchar.h>

class GIP;
class GIPImage;


/////////////////////////////////////////////////////////////////////////////
// TMAScript command target

class GPUIPScript : public CCmdTarget, public ScriptObject
{
	DECLARE_DYNCREATE(GPUIPScript)

	GPUIPScript();           // protected constructor used by dynamic creation
	virtual ~GPUIPScript();

// Attributes
public:
    ScriptExecMode       m_ExecutionMode;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GPUIPScript)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL
    void Reset();

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GPUIPScript)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

    void CheckDimGipImage(GIPImage **gpImage, long Width, long Height, long Bpp);    // Check GipImage dimensions
    void Convolve(long nPasses, LPCTSTR TechniqueName, VARIANT FAR *Coeff, GIPImage **Src, GIPImage **Dest);

	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(GPUIPScript)
    afx_msg BOOL Create(void);
	afx_msg BOOL IsGIPInUse(void);
	afx_msg void DestroyGIP(void);
    afx_msg BOOL PutGIP(long Image, GIPImage **Buffer);
    afx_msg BOOL GetGIP(long Image, GIPImage **Buffer);
    afx_msg BOOL GetComponentGIP(long Image, long component, GIPImage **Buffer);
	afx_msg BOOL PutSubImageGIP(long Image, GIPImage **gpImage, long x0, long y0, long ColourMode);
    afx_msg void OpenGIP(long Passes, GIPImage **SrcBuf, GIPImage **DestBuf);
    afx_msg void CloseGIP(long Passes, GIPImage **SrcBuf, GIPImage **DestBuf);
    afx_msg void DilateGIP(long Passes, GIPImage **SrcBuf, GIPImage **DestBuf);
    afx_msg void ErodeGIP(long Passes, GIPImage **SrcBuf, GIPImage **DestBuf);
    afx_msg void AddGIP(GIPImage **Src1, GIPImage **Src2, GIPImage **DestBuf);
    afx_msg void SubtractGIP(GIPImage **Src1, GIPImage **Src2, GIPImage **DestBuf);
    afx_msg void MultiplyGIP(GIPImage **Src1, GIPImage **Src2, GIPImage **DestBuf);
    afx_msg void DivideGIP(GIPImage **Src1, GIPImage **Src2, GIPImage **DestBuf);
    afx_msg void DiffGIP(GIPImage **Src1, GIPImage **Src2, GIPImage **DestBuf);
    afx_msg void ModGIP(GIPImage **Src1, GIPImage **Src2, GIPImage **DestBuf);
    afx_msg void ThreshGIP(GIPImage **SrcBuf, float Thresh, GIPImage **DestBuf);
    afx_msg void RGBHSIGIP(GIPImage **SrcBuf, GIPImage **DestBuf);
    afx_msg void HSIRGBGIP(GIPImage **SrcBuf, GIPImage **DestBuf);
	afx_msg void GreyQuadsToColourGIP(GIPImage **SrcBuf, GIPImage **DestBuf);
	afx_msg void ColourToGreyGIP(GIPImage **SrcBuf, GIPImage **DestBuf);
    afx_msg void TestGIP(GIPImage **SrcBuf, GIPImage **DestBuf);
    afx_msg void Technique(const TCHAR *Name, GIPImage **SrcBuf, GIPImage **DestBuf, long SetPixConst);
    afx_msg void ColourTechnique(const TCHAR *Name, GIPImage **SrcBuf, GIPImage **DestBuf, long SetPixConst);
    afx_msg void MathsTechnique(const TCHAR *Name, GIPImage **Src1, GIPImage **Src2, GIPImage **DestBuf);
    afx_msg void PongColourTechnique(const TCHAR *Name1, const TCHAR *Name2, long nPasses, GIPImage **SrcBuf, 
										GIPImage **DestBuf);
    afx_msg void MultipassTechnique(const TCHAR *Name, GIPImage **SrcBuf, GIPImage **DestBuf);
    afx_msg void ComponentThreshold(float Rmin, float Rmax, float Gmin, float Gmax, float Bmin, float Bmax, GIPImage **Src,
										GIPImage **Targ1);
    afx_msg void SplitComponents(GIPImage **Src, GIPImage **Targ1, GIPImage **Targ2, GIPImage **Targ3);
    afx_msg void JoinComponents(GIPImage **Src1, GIPImage **Src2, GIPImage **Src3, GIPImage **Target);
    afx_msg void BlendComponents(GIPImage **Src1, float A1, GIPImage **Src2, float A2, GIPImage **Src3, float A4,
										GIPImage **Target);
    afx_msg void GaussianBlur(long FiltHW, float Sigma, GIPImage **Src, GIPImage **Dest);
	afx_msg void DOG(long Size1, long Size2, GIPImage **Src, GIPImage **Dest);
    afx_msg void UnsharpMask(long FiltHW, float Sigma, float Amount, GIPImage **Src, GIPImage **Dest);
 	afx_msg void NearestNeighbDeconv(long FiltHW, float Sigma, float Amount, VARIANT FAR* pImages, VARIANT FAR* pDestImages);
    afx_msg void Kuwahara(long FiltHW, GIPImage **Src, GIPImage **Dest);
    afx_msg void ConvolveGIP(long nPasses, VARIANT FAR *Coeff, GIPImage **Src, GIPImage **Dest);
    afx_msg void ConvolveTechnique(const TCHAR *Name, long nPasses, VARIANT FAR *Coeff, GIPImage **Src, GIPImage **Dest);
    afx_msg void MedianGIP(long Passes, GIPImage **SrcBuf, GIPImage **DestBuf);
    afx_msg long CreateGIPImageAs(long SrcBuf, long Overlap);
    afx_msg long CreateGIPImage(long Width, long Height, long Overlap, long BPP);
    afx_msg void FreeGIPImage(VARIANT FAR* HandleGipImage);
    afx_msg void ReleaseGIPImage(VARIANT FAR* HandleGipImage);
	afx_msg void GetGIPImageDimensions(GIPImage **GIPSource, VARIANT FAR* Width, VARIANT FAR* Height, VARIANT FAR* Overlap,
										VARIANT FAR* BPP);
    afx_msg void BeginGIP(void);
    afx_msg void EndGIP(void);
	afx_msg void SimpleBlend(VARIANT FAR* pImages, GIPImage **Dest);
	afx_msg void HDRBlend(VARIANT FAR* pImages, GIPImage **Dest);
	afx_msg void RangeBlend(VARIANT FAR* pImages, GIPImage **Dest);
	afx_msg void CombineMultiExposures(long Type, VARIANT FAR* Images, long Output);
	afx_msg long Histogram(GIPImage **ImageHandle,  long ColourMode);
    afx_msg void AddConstGIP(GIPImage **Src1, float val, GIPImage **DestBuf);
    afx_msg void SubtractConstGIP(GIPImage **Src1, float val, GIPImage **DestBuf);
    afx_msg void MultiplyConstGIP(GIPImage **Src1, float val, GIPImage **DestBuf);
    afx_msg void DivideConstGIP(GIPImage **Src1, float val, GIPImage **DestBuf);
    afx_msg void CopyGIP(GIPImage **Src1, GIPImage **DestBuf);
	afx_msg void ColourDeconvGIP(GIPImage **Src, VARIANT FAR* pVCV1, VARIANT FAR* pVCV2, VARIANT FAR* pVCV3, GIPImage **Dest);
	afx_msg long CropImageGIP(GIPImage **Src, long x0, long y0, long w0, long h0, long overl);
	afx_msg long ResizeImageGIP(GIPImage **Src, long w0, long h0, long overl);
	afx_msg long FFT2DGIP(GIPImage** Src, long flag, long renderTarget);
	afx_msg long FFT2DPowerSpectrumGIP(GIPImage **Src);
	afx_msg long ButterWorthFFT2DGIP(GIPImage **Src, long filtType, float f1, float f2, float order);
	afx_msg long GaussianFFT2DGIP(GIPImage **Src, long filtType, float f1, float f2);
	afx_msg void FFTImageRegistrationGIP(GIPImage **Src1, GIPImage **Src2, VARIANT FAR*  XShift, VARIANT FAR*  YShift, VARIANT FAR* SN,
									long MaxShift);
	afx_msg void CoreFFTImageRegistrationGIP(GIPImage **FFTSrc1, GIPImage **FFTSrc2, VARIANT FAR*  XShift, VARIANT FAR*  YShift, 
									VARIANT FAR* SN, long MaxShift);
	afx_msg void DistanceMapGIP(GIPImage **src, GIPImage **dest, BOOL EDMOnly, BOOL IsWhiteBackground, float NormFactor);
	afx_msg void CannyEdgeGIP(GIPImage **src, GIPImage **dest, float thresholdLow, float thresholdHigh);
	afx_msg void HoughLinesGIP(GIPImage **src, GIPImage **dest, const TCHAR *angles, long minlength, long connection);
	afx_msg void FFTImageStitch(long Source1, long Source2, long mode, long overlap, VARIANT FAR*  XShift, VARIANT FAR*  YShift, VARIANT FAR* SN);
	afx_msg void FFTImageStitchGIP(GIPImage **src1, GIPImage **src2, long mode, long overlap, VARIANT FAR*  XShift, VARIANT FAR*  YShift,
									VARIANT FAR* SN);
	afx_msg void GIPLBP(GIPImage **src, GIPImage **dest, float radius, long noiseThresh);

	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()

    GIP             *m_pGIP;            // Hardware assisted image processing
    BOOL            m_bGIPCreated;      // Already created 
    BOOL            m_bUseGIP;          // Flags GIP usage
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GPUSCRIPT_H__1C1EB773_1B2E_4C40_B61E_13A970B85C0B__INCLUDED_)
