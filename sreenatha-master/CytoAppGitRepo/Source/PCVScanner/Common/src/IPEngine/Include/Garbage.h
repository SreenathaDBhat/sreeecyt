#pragma once
#include <vector>
#include <list>
#include <iostream>


typedef struct 
{
	void	*address;
	
} MEM_INFO; 
 
typedef std::list<MEM_INFO> AllocList;    

class  Garbage
{
public:

	Garbage::Garbage(void)
	{
	}


	Garbage::~Garbage(void)
	{
		Tidy();
	}

	void Garbage::Tidy(void)
	{
		for each (MEM_INFO MI in m_Alloc)
		{
			delete MI.address;
		}

		m_Alloc.clear();
	}

	void Garbage::Add(void *addr)
	{
		MEM_INFO MI;
	
		MI.address = addr;
		m_Alloc.push_back(MI);
	}

	int Garbage::Count(void)
	{
		return m_Alloc.size();
	}

	AllocList Garbage::List()
	{
		return m_Alloc;
	}

private:
	AllocList m_Alloc;
};

