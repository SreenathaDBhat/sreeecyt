//
// Hough.h
#pragma once

#ifndef FSCHAR
#define FSCHAR BYTE
#endif

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

int UTS_API getHoughHeight(int width, int height);
WORD UTS_API *HoughTransform(FSCHAR *image, int width, int height);
FSCHAR UTS_API *HoughTransform8bit(FSCHAR *image, int width, int height, int minAngle, int maxAngle);
BOOL UTS_API DetectCoverslip(FSCHAR *image, int width, int height, int &bottom, int &top, int &left, int &right);
FSCHAR UTS_API *CircularHoughTransform(FSCHAR *image, int width, int height, int radius);
FSCHAR UTS_API *SemiCircularHoughTransform(FSCHAR *image, int width, int height, int radius);
BOOL UTS_API DetectCircularCoverslip(FSCHAR *image, int width, int height, int radius, struct object* &coverslipObj1, struct object* &coverslipObj2, int &cx1, int &cx2, int &cy1, int &cy2);






