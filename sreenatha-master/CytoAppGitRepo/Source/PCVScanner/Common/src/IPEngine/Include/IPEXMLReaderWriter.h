#pragma once

#include <filestreamer.h>
#include <xmllite.h>
#include <AFXPRIV.H>

typedef CComPtr<IXmlReader> XMLLiteReader;
typedef CComPtr<IXmlWriter> XMLLiteWriter;

class IPEXMLReaderWriter
{
private:
	HRESULT hr;
    XMLLiteReader pReader;
    XMLLiteWriter pWriter;
    CComPtr<IXmlReaderInput> pReaderInput;
	CComPtr<IStream> pOutFileStream;
	CComPtr<IXmlWriterOutput> pWriterOutput;
	CComPtr<IStream> pFileStream;

public:
	IPEXMLReaderWriter()
	{
		pReader = NULL;
		pWriter = NULL;
	}

	BOOL CreateWriter(LPCWSTR OutFile)
	{
		//Open writeable output stream
		if (FAILED(hr = FileStream::OpenFile(OutFile, &pOutFileStream, TRUE)))
		{
			wprintf(L"Error creating file writer, error is %08.8lx", hr);
			return FALSE;
		}

		if (FAILED(hr = CreateXmlWriter(__uuidof(IXmlWriter),(void**) &pWriter, NULL)))
		{
			wprintf(L"Error creating xml writer, error is %08.8lx", hr);
			return FALSE;
		}

		if (FAILED(hr = CreateXmlWriterOutputWithEncodingCodePage(pOutFileStream, NULL, 65001, &pWriterOutput)))
		{
			wprintf(L"Error creating xml reader with encoding code page, error is %08.8lx", hr);
			return FALSE;
		}

		if (FAILED(hr = pWriter->SetOutput(pWriterOutput)))
		{
			wprintf(L"Error setting output for writer, error is %08.8lx", hr);
			return FALSE;
		}

		if (FAILED(hr = pWriter->SetProperty(XmlWriterProperty_Indent, TRUE)))
		{
			wprintf(L"Error setting indent property in writer, error is %08.8lx", hr);
			return FALSE;
		}

		return TRUE;
	}

	BOOL CreateReader(LPCWSTR InFile)
	{
		HRESULT hr;

		//Open read-only input stream
		if (FAILED(hr = FileStream::OpenFile(InFile, &pFileStream, FALSE)))
		{
			wprintf(L"Error creating file reader, error is %08.8lx", hr);
			return FALSE;
		}

		
		if (FAILED(hr = CreateXmlReader(__uuidof(IXmlReader), (void**) &pReader, NULL)))
		{
			wprintf(L"Error creating xml reader, error is %08.8lx", hr);
			return FALSE;
		}

		pReader->SetProperty(XmlReaderProperty_DtdProcessing, DtdProcessing_Prohibit);

		if (FAILED(hr = CreateXmlReaderInputWithEncodingCodePage(pFileStream, NULL, 65001, FALSE, L"c:\temp", &pReaderInput)))
		{
			wprintf(L"Error creating xml reader with encoding code page, error is %08.8lx", hr);
			return FALSE;
		}


		if (FAILED(hr = pReader->SetInput(pReaderInput)))
		{
			wprintf(L"Error setting input for reader, error is %08.8lx", hr);
			return FALSE;
		}

	
		return TRUE;
	}

	BOOL CreateReaderAndWriter(LPCWSTR InFile, LPCWSTR OutFile)
	{
		if (CreateReader(InFile))
			return FALSE;

		if (CreateWriter(OutFile))
			return FALSE;

		return TRUE;
	}

	void DeleteWriter()
	{
		pOutFileStream = NULL;
		pWriter = NULL;
	}

	XMLLiteWriter &Writer()
	{
		return pWriter;
	}

	XMLLiteReader &Reader()
	{
		return pReader;
	}

	// Read until reaching the end-element specified by pName - dont output anything
	BOOL ReadUntilEndElementNoWrite(const WCHAR* pName)
	{
		XmlNodeType nodeType;
		const WCHAR* pQName;
		bool FoundIt = FALSE;
		HRESULT hr;

		//read until there are no more nodes
		while (S_OK == (hr = pReader->Read(&nodeType)) && !FoundIt)
		{
			// WriteNode will move the reader to the next node, so inside the While Read loop, it is
			// essential that we use WriteNodeShallow, which will not move the reader. 
			// If not, a node will be skipped every time you use WriteNode in the while loop.
			switch (nodeType)
			{
			default:
			case XmlNodeType_Element:
			case XmlNodeType_Text:
				
				break;

			case XmlNodeType_EndElement:
				if (FAILED(hr = pReader->GetQualifiedName(&pQName, NULL)))
				{
					wprintf(L"Error reading element name, error is %08.8lx", hr);
					return FALSE;
				}

				if (wcscmp(pQName, pName) == 0)  //if the end element is price, then reset inPrice
					FoundIt = TRUE;
				break;			
			}
		}

		return FoundIt;
	}

	// Read until reaching the end-element specified by pName
	BOOL ReadUntilEndElement(const WCHAR* pName)
	{
		XmlNodeType nodeType;
		const WCHAR* pQName;
		bool FoundIt = FALSE;
		HRESULT hr;

		//read until there are no more nodes
		while (S_OK == (hr = pReader->Read(&nodeType)) && !FoundIt)
		{
			// WriteNode will move the reader to the next node, so inside the While Read loop, it is
			// essential that we use WriteNodeShallow, which will not move the reader. 
			// If not, a node will be skipped every time you use WriteNode in the while loop.
			switch (nodeType)
			{
			default:
			case XmlNodeType_Element:
			case XmlNodeType_Text:
				if (FAILED(hr = pWriter->WriteNodeShallow(pReader, FALSE)))
				{
					wprintf(L"Error writing WriteNodeShallow, error is %08.8lx", hr);
					return FALSE;
				}
				break;

			case XmlNodeType_EndElement:
				if (FAILED(hr = pReader->GetQualifiedName(&pQName, NULL)))
				{
					wprintf(L"Error reading element name, error is %08.8lx", hr);
					return FALSE;
				}

				if (wcscmp(pQName, pName) == 0)  
					FoundIt = TRUE;
				else
				{
					if (FAILED(hr = pWriter->WriteNodeShallow(pReader, FALSE)))
					{
						wprintf(L"Error writing WriteNodeShallow, error is %08.8lx", hr);
						return FALSE;
					}
				}
				break;			
			}
		}

		return FoundIt;
	}

	// Read until reaching the end-element specified by pName
	BOOL ReadUntilElement(const WCHAR* pName)
	{
		XmlNodeType nodeType;
		const WCHAR* pQName;
		HRESULT hr;
		BOOL FoundIt = FALSE;

		while (S_OK == (hr = pReader->Read(&nodeType)) && !FoundIt)
		{
			switch (nodeType)
			{
			default:
			case XmlNodeType_Element:
			case XmlNodeType_Text:
				if (FAILED(hr = pReader->GetQualifiedName(&pQName, NULL)))
				{
					wprintf(L"Error reading element name, error is %08.8lx", hr);
					return FALSE;
				}

				if (wcscmp(pQName, pName) != 0)  //if the end element is price, then reset inPrice
				{
					if (FAILED(hr = pWriter->WriteNodeShallow(pReader, FALSE)))
					{
						wprintf(L"Error writing WriteNodeShallow, error is %08.8lx", hr);
						return FALSE;
					}
				}
				else
					FoundIt = TRUE;
				break;

			case XmlNodeType_EndElement:
				if (FAILED(hr = pWriter->WriteFullEndElement()))
				{
					wprintf(L"Error writing WriteFullEndElement, error is %08.8lx", hr);
					return FALSE;
				}
				break;			
			}
		}

		return FoundIt;
	}

	// Goto an element ElementName with the attribute value = pAttrVal
	BOOL GotoElementWithAttributeValueNoWrite(const WCHAR* pElementName, const WCHAR *pAttrVal)
	{
		XmlNodeType nodeType;
		const WCHAR* pQName;
		BOOL FoundIt = FALSE;
		HRESULT hr;

		//read until there are no more nodes
		while (S_OK == (hr = pReader->Read(&nodeType)))
		{
			// WriteNode will move the reader to the next node, so inside the While Read loop, it is
			// essential that we use WriteNodeShallow, which will not move the reader. 
			// If not, a node will be skipped every time you use WriteNode in the while loop.
			switch (nodeType)
			{
			case XmlNodeType_Element:
				if (!FAILED(hr = pReader->GetQualifiedName(&pQName, NULL)))
				{
					if (wcscmp(pQName, pElementName) == 0)
					{		
						const WCHAR *pAttrName, *pAttrValue;

						pReader->MoveToFirstAttribute();

						do
						{
							pReader->GetQualifiedName(&pAttrName, NULL);
							pReader->GetValue(&pAttrValue, NULL);
							if (wcscmp(pAttrValue, pAttrVal) == 0)
								FoundIt = TRUE;							
							hr = pReader->MoveToNextAttribute();
						}
						while (hr == S_OK);
					}
					
				}
				break;

			default:
			case XmlNodeType_Text:
			case XmlNodeType_EndElement:            
				break;
			}

			if (FoundIt)
				break;

		}

		return FoundIt;
	}

	// Goto an element ElementName
	BOOL GotoElementNoWrite(const WCHAR* pElementName)
	{
		XmlNodeType nodeType;
		const WCHAR* pQName;
		BOOL FoundIt = FALSE;
		HRESULT hr;

		//read until there are no more nodes
		while (S_OK == (hr = pReader->Read(&nodeType)))
		{
			// WriteNode will move the reader to the next node, so inside the While Read loop, it is
			// essential that we use WriteNodeShallow, which will not move the reader. 
			// If not, a node will be skipped every time you use WriteNode in the while loop.
			switch (nodeType)
			{
			case XmlNodeType_Element:
				if (!FAILED(hr = pReader->GetQualifiedName(&pQName, NULL)))
				{
					if (wcscmp(pQName, pElementName) == 0)
						FoundIt = TRUE;
				}
				break;

			default:
			case XmlNodeType_Text:
			case XmlNodeType_EndElement:            
				break;
			}

			if (FoundIt)
				break;

		}

		return FoundIt;
	}

	// Goto an attribute and get its value
	BOOL GotoAttributeNoWrite(const WCHAR* pAttribute)
	{
		const WCHAR *pAttrName;
		BOOL FoundIt = FALSE;

		pReader->MoveToFirstAttribute();

		do
		{
			pReader->GetQualifiedName(&pAttrName, NULL);
			if (wcscmp(pAttribute, pAttrName) == 0)
			{
				FoundIt = TRUE;							
				break;
			}
			hr = pReader->MoveToNextAttribute();
		}
		while (hr == S_OK);

		return FoundIt;
	}

	// Goto an element ElementName with the attribute value = pAttrVal
	BOOL GotoElementWithAttributeValue(const WCHAR* pElementName, const WCHAR *pAttrVal)
	{
		XmlNodeType nodeType;
		const WCHAR* pQName;
		BOOL FoundIt = FALSE;
		HRESULT hr;

		//read until there are no more nodes
		while (S_OK == (hr = pReader->Read(&nodeType)))
		{
			// WriteNode will move the reader to the next node, so inside the While Read loop, it is
			// essential that we use WriteNodeShallow, which will not move the reader. 
			// If not, a node will be skipped every time you use WriteNode in the while loop.
			switch (nodeType)
			{
			case XmlNodeType_Element:
				if (!FAILED(hr = pReader->GetQualifiedName(&pQName, NULL)))
				{
					if (wcscmp(pQName, pElementName) == 0)
					{		
						const WCHAR *pAttrName, *pAttrValue;

						pWriter->WriteStartElement(NULL, pQName, NULL);
						pReader->MoveToFirstAttribute();

						do
						{
							pReader->GetQualifiedName(&pAttrName, NULL);
							pReader->GetValue(&pAttrValue, NULL);
							if (wcscmp(pAttrValue, pAttrVal) == 0)
								FoundIt = TRUE;
							hr = pWriter->WriteAttributeString(NULL, pAttrName, NULL, pAttrValue);
							hr = pReader->MoveToNextAttribute();
						}
						while (hr == S_OK);
					}
					else
					{
						if (FAILED(hr = pWriter->WriteNodeShallow(pReader, FALSE)))
						{
							wprintf(L"Error writing WriteNodeShallow, error is %08.8lx", hr);
							return FALSE;
						}
					}
				}
				else
				{
					if (FAILED(hr = pWriter->WriteNodeShallow(pReader, FALSE)))
					{
						wprintf(L"Error writing WriteNodeShallow, error is %08.8lx", hr);
						return FALSE;
					}
				}
				break;

			default:
			case XmlNodeType_Text:
				if (FAILED(hr = pWriter->WriteNodeShallow(pReader, FALSE)))
				{
					wprintf(L"Error writing WriteNodeShallow, error is %08.8lx", hr);
					return FALSE;
				}
				break;

			case XmlNodeType_EndElement:            
				if (FAILED(hr = pWriter->WriteFullEndElement()))
				{
					wprintf(L"Error writing WriteFullEndElement, error is %08.8lx", hr);
					return FALSE;
				}
				break;
			}

			if (FoundIt)
				break;

		}

		return FoundIt;
	}

	// Read in from one file and output to other file
	BOOL WriteRestOfFile()
	{
		XmlNodeType nodeType;
		HRESULT hr;

		//read until there are no more nodes
		while (S_OK == (hr = pReader->Read(&nodeType)))
		{
			// WriteNode will move the reader to the next node, so inside the While Read loop, it is
			// essential that we use WriteNodeShallow, which will not move the reader. 
			// If not, a node will be skipped every time you use WriteNode in the while loop.
			switch (nodeType)
			{
			case XmlNodeType_Element:
			case XmlNodeType_Text:
			default:
				if (FAILED(hr = pWriter->WriteNodeShallow(pReader, FALSE)))
				{
					wprintf(L"Error writing WriteNodeShallow, error is %08.8lx", hr);
					return FALSE;
				}
				break;


			case XmlNodeType_EndElement:
				if (FAILED(hr = pWriter->WriteFullEndElement()))
				{
					wprintf(L"Error writing WriteFullEndElement, error is %08.8lx", hr);
					return FALSE;
				}
				break;       
			}

		}

		return TRUE;
	}
};