#if !defined(AFX_IMPROCSCRIPT_H__79E7445C_7372_43C2_826E_7EAEF255DB97__INCLUDED_)
#define AFX_IMPROCSCRIPT_H__79E7445C_7372_43C2_826E_7EAEF255DB97__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImprocScript.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// ImprocScript command target

class ImprocScript : public CCmdTarget, public ScriptObject
{
	DECLARE_DYNCREATE(ImprocScript)

	ImprocScript();           // protected constructor used by dynamic creation
    virtual ~ImprocScript();

// Attributes
public:
    ScriptExecMode       m_ExecutionMode;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ImprocScript)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ImprocScript)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(ImprocScript)
    afx_msg long BgrSubtract(long GreyImage, long BgrImage, long Threshold);
    afx_msg long Subtract(long Image1, long Image2);
    afx_msg long LogicalAND(long Image1, long Image2);
	afx_msg long LogicalOR(long Image1, long Image2);
	afx_msg long LogicalXOR(long Image1, long Image2);
    afx_msg long Mean8(long Image1, long MaskImage, VARIANT FAR *A);
    afx_msg long Mode8(long Image1, long MaxLevel);
    afx_msg long Mod8(long Image1, long Image2);
    afx_msg long Divide(long Image1, long Image2, short Scale);
	afx_msg long HuangThreshold(long SrcImage);
	afx_msg long EntropyThreshold(long SrcImage);
	afx_msg long MomentThreshold(long SrcImage);
	afx_msg long DistLineThreshold(long SrcImage);
	afx_msg void GreyThreshold(long SrcImage, short LowThresh, short HighThresh);
	afx_msg long Threshold8(long SrcImage, short LowThresh, short HighThresh);
	afx_msg long Threshold16(long SrcImage, long LowThresh, long HighThresh);
	afx_msg long ThresholdGT8(long SrcImage, short Thresh);
    afx_msg void Quantile8(long Image, double QuantLo, double QuantHi, VARIANT FAR *Lo, VARIANT FAR *Hi);
	afx_msg void Quantile8ROI(long Image, double QuantLo, double QuantHi, long X, long Y, long W, long H, VARIANT FAR *Lo, VARIANT FAR *Hi);
	afx_msg void AutoStretch(long SrcImage);
	afx_msg long EdgeMeasure(long SrcImage, long XSpacing, long YSpacing);
	afx_msg long GetEdge(long SrcImage, long Spacing);
	afx_msg long Thin(long SrcImage, long MaxIter);
	afx_msg void FFTBandPass(long SrcImage, long DestImage, float Freq1, float Freq2);
	afx_msg void ReplaceGreyLevel(long SrcImage, short OldGrey, short NewGrey);
	afx_msg void ImageInvert(long SrcImage);
	afx_msg void FuzzyCmeanSegment(long R, long G, long B, double variance);
	afx_msg void SctSplitSegment(long R, long G, long B, long A_split, long B_split);
	afx_msg void MeanShiftSegment(long R, long G, long B);
	afx_msg void EdgeLink(long BinaryImage, long connection);
	afx_msg void Morph(long op_type, long k_type, long Image, long Se);
	afx_msg void FThin(long BinaryImage);
	afx_msg void EdgeDetection(LPCTSTR methode, long Image, float sigma, long low, long high);
	afx_msg void HoughFilter(long BinaryImage, LPCTSTR degree_string, long minlength, long connection);
	afx_msg void DiscreteCosine(long Image);
	afx_msg long FlatField(long SrcImage, long Spacing);
	afx_msg long ApplyLUT(long SrcImage, long LUTImage);
	afx_msg void DoRegression(VARIANT FAR* X, VARIANT FAR* Y, int O, VARIANT FAR* Coeff);
	afx_msg BOOL ContrastStretch16To8(long ImageIn, long ImageOut);
	afx_msg long ImageSum(long SrcImage);

	//}}AFX_DISPATCH

	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMPROCSCRIPT_H__79E7445C_7372_43C2_826E_7EAEF255DB97__INCLUDED_)
