#ifndef _IMAGE_REGISTER_H
#define _IMAGE_REGISTER_H


class ImageRegister {

public:
	int Register(BYTE *image1, BYTE *image2,
			int width, int height, 
			int search_from, int search_to, int stripsize,
			LPCTSTR orientation);

private:
	int DifferenceInX(BYTE *image1, BYTE *image2,
			int width, int height, 
			int search_from, int search_to, int stripwidth);

	int DifferenceInY(BYTE *image1, BYTE *image2,
			int width, int height, 
			int search_from, int search_to, int stripheight);

	int FindMinOfDiff(float *avdiff, int diffsize);
};



#endif
