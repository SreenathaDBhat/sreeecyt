/////////////////////////////////////////////////////////////////////////////
// Header file
// This OLE automation class is exposed to the script engine interface.
// Implementation of new UTS keywords is done here. This keywords are
// UTS mask operations
//
// Written By Karl Ratcliff 24042001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
// Implementation of new keywords is simply done using the Classwizard (to
// be recommended). This is done by selecting the Automation tab and then
// selecting the class name: MSKscript. Add new 
//

#if !defined(AFX_MSKscript_H__6016770E_44CE_465F_AAA7_AF051A632CED__INCLUDED_)
#define AFX_MSKscript_H__6016770E_44CE_465F_AAA7_AF051A632CED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MSKscript.h : header file
//

//#include "scriptutils.h"

// Defines dimensions of the array passed to the mask data keyword
#define MASKDATALENGTH              25

/////////////////////////////////////////////////////////////////////////////
// MSKscript command target
// This implements our dispatch interface which is then exposed
// to the scripting engine for UTS mask operations
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// MSKscript command target

class MSKscript : public CCmdTarget, public ScriptObject
{
	DECLARE_DYNCREATE(MSKscript)

	MSKscript();           // protected constructor used by dynamic creation

// Attributes
public:
	virtual ~MSKscript();
    void Reset();
    ScriptExecMode  m_ExecutionMode;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(MSKscript)

	virtual void OnFinalRelease();

	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(MSKscript)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(MSKscript)
	afx_msg long DistToMask(long SrcHandle, VARIANT FAR* Xc, VARIANT FAR* Yc);
	afx_msg long RectMaskData(long SrcHandle, long MaskHandle, VARIANT FAR* MaskData);
	afx_msg long ReconFast(long HandleLimit, long HandleSeed, long Xc, long Yc);
	afx_msg long Reconstruct(long HandleLimit, long HandleSeed);
	afx_msg long MaskSubrectOnes(long SrcHandle, long Xleft, long Ytop, long Width, long Height);
	afx_msg long LongSmoothArc(long SrcMask, long ContMask, long ArcMask, long HalfStepMin, long HalfStepMax, long CurvThresh, VARIANT FAR* Results, VARIANT FAR* Codes, VARIANT FAR* Curves);
	afx_msg long MaskCalipers(long Handle, VARIANT FAR* Calipers, VARIANT FAR* Imin, VARIANT FAR* Imax, VARIANT FAR* Xc, VARIANT FAR* Yc);
	afx_msg long LineEndsInMask(long SrcMask, VARIANT FAR* X1, VARIANT FAR* Y1, VARIANT FAR* X2, VARIANT FAR* Y2);
	afx_msg long MaskLinkInfo(long SrcImage, VARIANT FAR*CompData, long MinArea);
	afx_msg void Dilate(long SrcImage, short Neighbourhood, short Iter);
	afx_msg void Erode(long SrcImage, short Neighbourhood, short Iter);
	afx_msg long AccumLinesSeg(long Mask, long Accum, long HalfStepMin, long HalfStepMax, long CurvThresh, double DistMin, double DistMax);
	afx_msg long Borders(long SrcImage, long BordersImage, long Neighbourhood);
	afx_msg void ConvexHull(long SrcImage, double Thickness);
	afx_msg void Open(long SrcImage, short Neighbourhood, short Iter);
	afx_msg void Close(long SrcImage, short Neighbourhood, short Iter);
	afx_msg long MaskShapeInfo(long SrcImage, VARIANT FAR* ShapeInfo, long MinArea);
	afx_msg long NewMaskShapeInfo(long SrcImage, VARIANT FAR* ShapeInfo, long MinArea);
	afx_msg void OperMask(long MaskImage, LPCTSTR Operation);
	afx_msg void MaskSplitRange(long RangeImage, long MinMaskImage, long MaxMaskImage, long MinArea, long MaxArea, VARIANT FAR* Mic, VARIANT FAR* Mac, VARIANT FAR* Moc);
	afx_msg void MaskData(long SrcImage, VARIANT FAR* Data);
	afx_msg void Circle(long Image, long Cx, long Cy, long Radius, long Colour);
	afx_msg void PutPixel(long Image, long X, long Y, long Colour);
	afx_msg void DrawCross(long SrcImage, long Cx, long Cy, long Size, long Colour);
	afx_msg void Rectangle(long SrcImage, long Left, long Top, long Width, long Height, long Colour);
	afx_msg void TextLine(long SrcImage, LPCTSTR Text, long X, long Y, long H, long Colour);
	afx_msg long GetPixel(long image, long X, long Y);
	afx_msg void Line(long SrcImage, long X1, long Y1, long X2, long Y2, long Colour);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MSKscript_H__6016770E_44CE_465F_AAA7_AF051A632CED__INCLUDED_)
