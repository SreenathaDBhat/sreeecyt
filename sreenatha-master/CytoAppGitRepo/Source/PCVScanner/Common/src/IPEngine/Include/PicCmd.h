
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the PICCMD_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// PICCMD_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef PICCMD_EXPORTS
#define PICCMD_API extern "C" __declspec(dllexport) 
#else
#define PICCMD_API extern "C" __declspec(dllimport) 
#endif

// Initialise the UTS DLLs
PICCMD_API BOOL __stdcall picInitialise(void);

// Reset all UTS handles and any associated windows
PICCMD_API void __stdcall picFreeAll(void);

// Create a memory rectanlge
PICCMD_API long __stdcall picCreate(long Pix, long Xe, long Ye);

// Free a memory rectangle
PICCMD_API void __stdcall picFree(long Hndl);

// Set bits in a memory rectangle
PICCMD_API BOOL __stdcall picSetBits(BYTE *lpBits, long Hndl);

// Get bits in a memory rectangle
PICCMD_API BOOL __stdcall picGetBits(BYTE *lpBits, long Hndl);

// Create a picture and an associated window
PICCMD_API BOOL __stdcall picPicture(const char *Name, long Pix, long Xe, long Ye);

// Create a window for a picture
PICCMD_API BOOL __stdcall picPictureWindow(const char *Name, long Hndl);

// Create a rectangle externally
PICCMD_API long __stdcall picMakeExtern(void *lpBits, long Pix, long Xe, long Ye, long Q);

// Get Rectangle Information
PICCMD_API long __stdcall picGetRectInfo(long Hndl, long *imx, long *imy);

// Get extended rectangle information
PICCMD_API long __stdcall picGetExtendedInfo(long Hndl, long *imx, long *imy, long *pitch, void *addr);

// Permute The Byte Order of An Image
PICCMD_API long __stdcall picPermute(long Hndl, long GrpLen, char *Permutation);

// Referesh a currently display image window
PICCMD_API long __stdcall picRefresh(char *name);

// Read an Image File For Display
PICCMD_API long __stdcall picReadBMP(const char *ImageName, char *Filename);

// Histogram A Picture
PICCMD_API long __stdcall picGetHistogramData(long Hndl, void *HistData);

// Histogram An Image, Store The Results In A Mem Rectangle
PICCMD_API long __stdcall picHistogram(long Hndl);

// Create A Histogram Display Window
PICCMD_API BOOL __stdcall picHistogramWindow(const char *Name, long Hndl);

// Threshold An Entire Image
PICCMD_API long __stdcall picThresholdImage(long SrcHndl, long *MaskHndl, char *Operation, DWORD Threshold);

// Create As With Pix
PICCMD_API long __stdcall picCreateAsWithPix(long SrcHndl, long BPP);

// Show a UTS Image In A Predefined Windows Window
PICCMD_API void __stdcall picShowImage(HWND hWnd, long SrcHndl);
