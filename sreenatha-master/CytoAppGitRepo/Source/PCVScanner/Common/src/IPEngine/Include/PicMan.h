// PicDisp.h : header file
//
/////////////////////////////////////////////////////////////////////////////
//
// Manages windows assigned to UTS images. 
// Written for the IPE
//
// Written By Karl Ratcliff 10042001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//
#include <afxtempl.h>
#include "picwindow.h"

#ifndef __PICDISP_H
#define __PICDISP_H

#ifdef PICWINDOW_EXPORTS
#define PICWINDOW_API __declspec(dllexport)
#else
#define PICWINDOW_API __declspec(dllimport)
#endif

/////////////////////////////////////////////////////////////////////////////
// Typedefs etc

class CPicWindow;

/////////////////////////////////////////////////////////////////////////////
// Exported functions
/////////////////////////////////////////////////////////////////////////////

PICWINDOW_API void InitialisePicCmd(void);

/////////////////////////////////////////////////////////////////////////////
// Window management class - exported
// This class implements the window display used for the IPE development
// environment. It creates image or histogram windows, frees them and 
// maintains a list of all windows displayed at any one time.
/////////////////////////////////////////////////////////////////////////////

class PICWINDOW_API CPicManager {

public:
    CPicManager(CUTSMemRect *pMR);
    ~CPicManager();
    BOOL AddNew(long UTSHandle, LPCTSTR Title, CPicWindow::DisplayType DType);         // Create a new window
    BOOL AddNew(long UTSHandle, LPCTSTR Title, CPicWindow::DisplayType DType, long Sx, long Sy, long Ex, long Ey);         // Create a new window
    BOOL Delete(long UTSHandle);                         // Delete an existing window
    void Refresh(long UTSHandle);                        // Refresh a window
    void DeleteAll(void);                                // Delete all currently open windows
    HWND GetHWND(long UTSHandle);                        // Get the HWND for the corresponding UTS image

protected:

private:
    PicWindowPtr Search(long UTSHandle);                 // Search for a particular list entry
    POINT                                m_nScreenPos;   // Screen next top,left corner position of the window
    CList<PicWindowPtr, PicWindowPtr>    m_WindowList;   // List of all windows currently displayed on screen
	CUTSMemRect							 *pUTSMemRect;
};


#endif              // __PICDISP_H