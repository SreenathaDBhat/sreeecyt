// PicWindow.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// 
// Window handling for the display of UTS info in interactive mode
//
// Written by Karl Ratcliff 11042001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#if !defined(AFX_PICWINDOW_H__7559134C_2D16_11D5_9E30_DADA5ABBD751__INCLUDED_)
#define AFX_PICWINDOW_H__7559134C_2D16_11D5_9E30_DADA5ABBD751__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "stdafx.h"
#include "uts.h"

class CPicManager;

/////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////

// Max window title length for an image
#define MAX_UTSNAME_LENGTH                  32

// Default window size on creation
#define DEFAULT_WH                          200

// Profiling line colour
#define PROFILE_COLOUR                      RGB(0xFF, 0x00, 0xFF)

// Measurement line colour
#define MEASUREMENT_COLOUR                  RGB(0x80, 0x00, 0xFF)

// Default Zoom Level 1 to 3
#define DEFAULT_WINDOWZOOM_LEVEL            3
#define DEFAULT_HISTOGRAMZOOM_LEVEL         1

/////////////////////////////////////////////////////////////////////////////
// Typedefs
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CPicWindow window
// Displays an image window with associated UTS image handle.
// Also deals with one dimensional histograms
/////////////////////////////////////////////////////////////////////////////

class CPicWindow : public CWnd
{
    // Construction
public:
    // Current operation mode of the mouse within the window
    typedef enum {
        mouseDoingNothing,              // Doing sod all
            mouseStartProfiling,            // Started defining a profile line
            mouseProfiling,                 // Presently defining a profile line
            mouseStartMeasure,              // Base point to measure a distance
            mouseMeasuring                  // Measuring distance from the base point
    } InteractiveMode;
    
    // Image Dimension Being Referred To
    typedef enum {
        xDimension,
        yDimension
    } Dimension;
    
    // Type of window to display
    typedef enum {
        wndHistogram,
        wndProfile,
        wndImage,
        wndEmpty
    } DisplayType;
    
    CPicWindow();
	virtual ~CPicWindow();
    
    // Attributes
public:
    long    m_hImageHandle;                                 // A UTS Image handle

// Operations
public:
    virtual void AssignImage(long X, long Y, LPCTSTR pName, long ImageHandle, int ZoomLevel, CPicManager *pPM) {};      // Assign a name and UTS image handle to the window
    virtual void Refresh();
    virtual void SetProfilePoints(long sx, long sy, long ex, long ey) {};

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPicWindow)
	public:
	protected:
	//virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

    void    Resize(int Multiplier, int Divisor);    // Resize the display window and image in it

    // Member variables
    CMenu           *m_pMenu;                       // The menu to put in the window                     
    int             m_nMultiplier, m_nDivisor;      // Scale factors for the image display
    CString         *m_pImageName;                  // Stores the name for the window caption
    long            m_nImageW, m_nImageH;           // Actual image dimensions
    InteractiveMode m_MouseMode;                    // Current use of the mouse in interactive mode
    CPicManager     *m_pPicManager;                 // Pointer to the associated CPicManager class
    DisplayType     m_Type;                         // Type of window
	BOOL			m_bInvert;						// = TRUE for inverted image display
	BOOL			m_bPseudo;						// = TRUE for pseudo colour

    // Generated message map functions
	//{{AFX_MSG(CPicWindow)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClose();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

typedef CPicWindow            *PicWindowPtr;

#endif // !defined(AFX_PICWINDOW_H__7559134C_2D16_11D5_9E30_DADA5ABBD751__INCLUDED_)
