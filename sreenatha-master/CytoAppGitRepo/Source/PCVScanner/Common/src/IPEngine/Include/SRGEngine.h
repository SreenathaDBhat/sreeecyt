// SRGEngine.h: interface for the CSRGEngine class.
//
//////////////////////////////////////////////////////////////////////


#pragma once

#include <string.h>
#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <utility>

using namespace std;

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif


class UTS_API CSRGCandidate
{
// Constructors/Destructors
public:
	CSRGCandidate( CPoint & point, int mostSimilarRegionId, double similarityDifference);
	virtual ~CSRGCandidate();

// Attributes
public:
	CPoint m_point;
	int m_nMostSimilarRegionId;
	double m_dSimilarityDifference;

// Operations
public:

	bool operator < (const CSRGCandidate& rhs) const
	{
		if (this->m_point == rhs.m_point)		// <--- Looks dodgy to me
			return false;
		else if (this->m_dSimilarityDifference < rhs.m_dSimilarityDifference)
			return true;
		else if (this->m_dSimilarityDifference > rhs.m_dSimilarityDifference)
			return false;
		else if (this->m_point.x < rhs.m_point.x)
            return true;
		else if (this->m_point.x > rhs.m_point.x)
            return false;
		else if (this->m_point.y < rhs.m_point.y)
            return true;
		else if (this->m_point.y > rhs.m_point.y)
            return false;
		else
		{
            ASSERT(0);// "This condition should never happen."
			return false;
        }
	}

// Implementation
protected:

};


class UTS_API CSRGRegionInfo
{
// Constructors/Destructors
public:
	CSRGRegionInfo( int seedID);
	virtual ~CSRGRegionInfo();

// Attributes
public:

// Operations
public:
	virtual void AddPoint( const int value) = 0;
	virtual double Distance( const double value) = 0;

// Implementation
protected:
	int _seedID;

};


class UTS_API CSRGImage
{
// Constructors/Destructors
public:
	CSRGImage();
	virtual ~CSRGImage();

// Attributes
public:
	long handle;
	long w;
	long h;
	long pitch;
	long bpp;
	void *pImage;

// Operations
public:
};


struct ltstr
{
  bool operator()(const CSRGCandidate* lhs, const CSRGCandidate* rhs) const
  {
		if (lhs->m_dSimilarityDifference < rhs->m_dSimilarityDifference)
			return true;
		else if (lhs->m_dSimilarityDifference > rhs->m_dSimilarityDifference)
			return false;
		else if (lhs->m_point.x < rhs->m_point.x)
            return true;
		else if (lhs->m_point.x > rhs->m_point.x)
            return false;
		else if (lhs->m_point.y < rhs->m_point.y)
            return true;
		else if (lhs->m_point.y > rhs->m_point.y)
            return false;
		else if (lhs->m_point == rhs->m_point)		// <--- Looks dodgy to me
			return false;
		else
		{
            ASSERT(0);// "This condition should never happen."
			return false;
        }

  }
};



typedef CSRGRegionInfo * CREATEREGIONINFO (int) ;

//
// CSRGEngine - Segments images using seeded region growing
//
class UTS_API CSRGEngine
{
// Constructors/Destructors
public:
	CSRGEngine( CUTSMemRect * pMR, CREATEREGIONINFO * pRegionFactory);
	virtual ~CSRGEngine();

// Attributes
public:

// Operations
public:
	void Segment( long source, long seeds, long segmentation);

// Implementation
protected:
	static const int MAX_REGION_NUMBER = 253;
    static const int BACKGROUND_MARK   = 0x00;
	static const int BACKGROUND_REGION = 0x01;
    static const int CANDIDATE_MARK    = 0xff;

	CUTSMemRect *_pUTSMemRect;
	CREATEREGIONINFO * _pRegionInfoFactory;
	set<CSRGCandidate *, ltstr> _ssl;
	vector<CSRGRegionInfo*> _regionInfos;
	int _seedToRegion[MAX_REGION_NUMBER];
	int _regionToSeed[MAX_REGION_NUMBER];

	CSRGImage _source;
	CSRGImage _seeds;
	CSRGImage _regionMarker;

    int _xMin;
	int _xMax;
    int _yMin;
    int _yMax;

	void SetImage( long image) { };
	void SetSeeds( long seeds) { };

	void InitialiseStructures();
	void InitialiseMarkersAndRegionInfo();
	void InitialiseCandidates();

	void CandidatesFromNeighbours( const CPoint & point);
	void AddIfBackground( int x, int y, BYTE * pPixel, vector<CPoint> & backgroundPoints);
	CSRGCandidate * CreateCandidate( CPoint point);
	void CheckNeighbour( int x, int y, vector<int> & neighbours);

	void RestoreOriginalSeedIDs();
	void image_dump(BYTE * pImage, long width, long height, LPCTSTR filename);
};


