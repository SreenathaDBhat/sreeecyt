// SRGRegionInfoScalar.h: interface for the RGRegionInfoScalar class.
//
//////////////////////////////////////////////////////////////////////


#pragma once

#include <string.h>
#include <iostream>
#include <map>
#include <vector>
#include <utility>

using namespace std;

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif



class UTS_API CSRGRegionInfoScalar : CSRGRegionInfo
{
// Constructors/Destructors
private :
	CSRGRegionInfoScalar( int seedID);

public:
	virtual ~CSRGRegionInfoScalar();

// Attributes
public:

// Operations
public:
	static CSRGRegionInfo * Create( int seedID);
	virtual void AddPoint( const int value);
	virtual double Distance( const double value);

// Implementation
protected:
	long _pointCount;
	double _sumIntensity;

	double Mean() const;

};
