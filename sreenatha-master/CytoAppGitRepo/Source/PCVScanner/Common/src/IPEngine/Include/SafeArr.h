/////////////////////////////////////////////////////////////////////////////
// Script utility functions for safe array processing
//
// Written By K Ratcliff 12042001
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#ifndef __SAFEARR_H
#define __SAFEARR_H

/////////////////////////////////////////////////////////////////////////////
// Defines

// Safe array pprocessing should return this if no errors
#define SAFENOERROR                        0

/////////////////////////////////////////////////////////////////////////////
// Macros

#define CHECK_SAFEARR_EXCEPTION(E, ACTION)  if (E != SAFENOERROR)                           \
                                            {                                               \
                                                AfxThrowOleDispatchException(0xFF, E, 0);   \
                                                ACTION;                                     \
                                            }                                               \


/////////////////////////////////////////////////////////////////////////////
// Prototypes

// Set an element in a 1D array
void Set1DArrayIndex(SAFEARRAY *pSafe, long Idx, VARIANT *V);

// Set an element in a 2D array
void Set2DArrayIndex(SAFEARRAY *pSafe, long Idx1, long Idx2, VARIANT *V);

// Check an array dimension too see wether it is large enough
int CheckBounds(SAFEARRAY *pSafe, long Dimension, long Limit);

// Get information about a safe array from its information structure
int GetArrayInfo(VARIANT *ArrayInfo, SAFEARRAY **pSafe, UINT NumDimensions);

// Set longs in a single dimension array
void SetArrayLongData(SAFEARRAY *pSafe, long *Data, int NumElements);

// Set bytes in a single dimension array
void SetArrayByteData(SAFEARRAY *pSafe, BYTE *Data, int NumElements);

// Set doubles in a single dimension array
void SetArrayDoubleData(SAFEARRAY *pSafe, double *Data, int NumElements);

#endif              // #ifdef __SAFEARR_H