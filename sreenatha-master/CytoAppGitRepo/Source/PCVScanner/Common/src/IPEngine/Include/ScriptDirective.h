#pragma once
////////////////////////////////////////////////////////////////////////////////
//
//	ScriptDirective.h
//
//	Extensible system for adding control directives to a script
//	See implementation file for details
//	Dave Burgess, Applied Imaging April 2003
//
#ifdef SCRIPTPROC_EXPORTS
#define SCRIPTPROC_API __declspec(dllexport) 
#else
#define SCRIPTPROC_API __declspec(dllimport) 
#endif

// An individual directive
class CDirective
{
public:
	CDirective(void);
	~CDirective(void);

	CString m_directive;
	CString m_sub_directive;
	CString m_value;
};

// Control directives attached to a script
class SCRIPTPROC_API CScriptDirective
{
public:
	CScriptDirective(bool setdefault = false);
	~CScriptDirective(void);

	// Add Script directive information
	bool AddDirective(CString directive, CString sub_directive);

	// Free all stored directives and their values
	void FreeDirectives(void);

	// Parse file for script directives
	bool ParseScript(CString script_name);

	// Clear all directive values
	void ClearDirectives(void);

	// Search directives looking for match
	// Returns TRUE if match AND value == "1"
	bool MatchDirective(CString directive, CString sub_directive);

	// Search directives, return value of matched directive
	CString GetDirectiveValue(CString directive, CString sub_directive);
	double GetDirectiveDoubleValue(CString directive, CString sub_directive);
	int GetDirectiveIntValue(CString directive, CString sub_directive);

private:
	CDirective *m_directives;	// Pointer to array of directives
	int	m_ndirectives;			// Number of directives in array
	int m_directivespace;		// Actual amount of space allocated
};
