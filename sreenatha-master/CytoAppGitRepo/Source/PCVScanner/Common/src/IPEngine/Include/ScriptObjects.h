/////////////////////////////////////////////////////////////////////////////
//
// Class containing ip objects
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#pragma once

class CIPObjects
{
public:
	CIPObjects(CUTSMemRect *pM, CUTSDraw *pD, CUTSGeneric *pG, CUTSBlob *pB, CWoolzIP *pW)
	{
		m_pUTSMemRect = pM;
		m_pUTSBlob = pB;
		m_pUTSGeneric = pG;
		m_pWoolzLib = pW;
		m_pUTSDraw = pD;
	}

	CUTSMemRect *UTSMemRect() { return m_pUTSMemRect; }
	CUTSGeneric *UTSGeneric() { return m_pUTSGeneric; }
	CUTSBlob	*UTSBlob()    { return m_pUTSBlob; }
	CWoolzIP	*WoolzLib()   { return m_pWoolzLib; }
	CUTSDraw	*UTSDraw()	  { return m_pUTSDraw; }

private:
	// Interface to zmemrect.dll
	CUTSMemRect				*m_pUTSMemRect;
	// Interface to zblob.dll
	CUTSBlob				*m_pUTSBlob;
	// Interface to zgeneric.dll
	CUTSGeneric				*m_pUTSGeneric;
	// Interface to the woolz library
	CWoolzIP				*m_pWoolzLib;
	// Interface to various drawing routines
	CUTSDraw				*m_pUTSDraw;

};


class ScriptObject
{
public:
	ScriptObject()
	{
		m_pIP = NULL;
	}

    void  Initialise(CIPObjects *pPIP)          // Initialisation done before script execution
	{
		m_pIP = pPIP;
	}

	CIPObjects& IP() { ASSERT(m_pIP); return *m_pIP; }

private:
	CIPObjects			 *m_pIP;
};

