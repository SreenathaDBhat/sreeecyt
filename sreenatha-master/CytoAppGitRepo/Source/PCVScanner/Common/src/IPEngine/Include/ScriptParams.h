// ScriptParams.h: interface for the CScriptParams class.
//
//
//////////////////////////////////////////////////////////////////////
#include <afxtempl.h>

#if !defined(_SCRIPT_PARAMS_H)
#define _SCRIPT_PARAMS_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SCRIPTPROC_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SCRIPTPROC_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef SCRIPTPROC_EXPORTS
#define SCRIPTPROC_API __declspec(dllexport) 
#else
#define SCRIPTPROC_API __declspec(dllimport) 
#endif

class CParamSet;
class CSerializeXML;

#define SCRIPTPARAMVERSION				108

class SCRIPTPROC_API CScriptParams
{
public:
	CScriptParams();
	~CScriptParams();

	void			XMLSerialize(CSerializeXML &ar);				// Serialisation
	void			Serialize(CArchive &ar);
	CScriptParams*	duplicate();									// Duplication
    void            duplicate(CScriptParams *pParams);
    CParamSet*		FindParam(CString Name);						// Get param by name
	CParamSet*		FindParam(int Idx);								// Get param by index
	int				GetCount(void);									// Return number of entries
    void            Kill(void);                                     // Delete them all
	CList<CParamSet *, CParamSet*> m_ParamSetList;
};


class SCRIPTPROC_API CParamSet
{
public:
	CParamSet(); 
	CParamSet(CString PersistName, CString Label, CString CtrlType, COleVariant Min, COleVariant Max, COleVariant Val);
	~CParamSet();

	CParamSet*		duplicate();
	CString m_PersistentName;
	CString m_Label;
	CString m_CtrlType;
    COleVariant m_Min;
    COleVariant m_Max;
    COleVariant m_Value;
	COleVariant m_Default;
};



#endif
