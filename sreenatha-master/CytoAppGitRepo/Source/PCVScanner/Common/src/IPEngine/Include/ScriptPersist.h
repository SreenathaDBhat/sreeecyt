/////////////////////////////////////////////////////////////////////////////
// Persistent variables class and file I/O
//
// Written By Karl Ratcliff 22062001
// 
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#ifndef __SCRIPT_PERSIST_H
#define __SCRIPT_PERSIST_H

#include <afxtempl.h>

// Defines the default file to use for persistent variables
#define PERSISTENTVARS_FILENAME             _T("\\SCRIPTVARS.DAT")

#ifdef SCRIPTPROC_EXPORTS
#define SCRIPTPROC_API __declspec(dllexport) 
#else
#define SCRIPTPROC_API __declspec(dllimport) 
#endif

/////////////////////////////////////////////////////////////////////////////
// This is a wrapper for the storage class that stores VarPersist type
// variables as a CMap class. CMap has been used for its hash table
// lookup ability. NOTE entries are stored as variants which is nice
// for VB scripting
/////////////////////////////////////////////////////////////////////////////

class SCRIPTPROC_API VarMapBank
{
public:
    VarMapBank();
    ~VarMapBank();

    void    InitValue(LPCTSTR VarName, VARIANT Value);       // Initialise and add a new value
    void    InitValue(LPCTSTR VarName, double Value);        // double version
    void    InitValue(LPCTSTR VarName, long Value);          // long version
    void    InitValue(LPCTSTR VarName, CString& Value);      // string version
    void    ResetValue(LPCTSTR VarName);                     // Resets a persistent var removing it from the bank
    void    ResetBank(void);                                // Deletes all values stored in the bank
    BOOL    GetValue(LPCTSTR VarName, VARIANT *Value);       // Get a value from the bank
    BOOL    GetValue(LPCTSTR VarName, double *Value);        // double version
    BOOL    GetValue(LPCTSTR VarName, long *Value);          // long version
    BOOL    GetValue(LPCTSTR VarName, CString& Value);       // string version
    void    SetValue(LPCTSTR VarName, VARIANT Value);        // Set a value in the bank
    void    SetValue(LPCTSTR VarName, double Value);         // double version
    void    SetValue(LPCTSTR VarName, long Value);           // long version
    void    SetValue(LPCTSTR VarName, CString& Value);       // string version
    BOOL    Save(LPCTSTR FileNameAndPath);                  // Save the bank to disk
    BOOL    Load(LPCTSTR FileNameAndPath);                  // Load the bank from disk
    BOOL    Serialize(CArchive &ar);                        // Serialisation
	int		GetCount(void);									// Total number of persistents in the bank
	POSITION GetFirst(void);								// Get the first 
	BOOL    GetNext(POSITION &P, CString &Name, VARIANT *Value);		// Get the next

    CMap<CString, LPCTSTR, COleVariant, COleVariant&>     m_VarBank; // Var Storage
};


#endif          // #ifndef __SCRIPT_PERSIST_H


