/////////////////////////////////////////////////////////////////////////////
//
//
// ScriptProc.h : Header file
//
// Script processing class DLL
//
// Written By Karl Ratcliff 12042001
//
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//	19Jun2001	JMB	Added support for classifier script object
//	19Oct2001	WH	Added support for aitiff script object

#ifndef __SCRIPTPROC_H
#define __SCRIPTPROC_H

#pragma message("Including SCRIPT PROC H")

#include <activscp.h>
#include <afxdisp.h>



/////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////

// Maximum size allowed for a script
#define MAX_SCRIPT_SIZE             256 * 1024 * 1024

// Maximum depth for include directives
#define MAX_INCLUDE_DEPTH           8

// Default extension for script files
#define SCRIPT_EXTENSION            "script"

// Maximum name for an identifier in VB script
#define MAX_SCRIPT_ID_LENGTH        255

// Denotes a script file i/o error
#define SCRIPT_FILE_ERROR           -1

// A bit of short hand
#define SCRIPTITEM_AIOBJECT          SCRIPTITEM_ISVISIBLE | SCRIPTITEM_ISSOURCE | SCRIPTITEM_GLOBALMEMBERS

// Uncomment this to provide some memory debugging in the script processing
#ifdef _DEBUG
//#define _SCRIPTPROC_MEM_DEBUG
#endif

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SCRIPTPROC_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SCRIPTPROC_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef SCRIPTPROC_EXPORTS
#define SCRIPTPROC_API __declspec(dllexport) 
#else
#define SCRIPTPROC_API __declspec(dllimport) 
#endif

#define IDC_SCRIPTPROGSCROLL		29675



class Measurement;
class CScriptParams;
class CParamSet;
class CRegionList;
class Region;
class CSyncObjectMutex;
class VBscript;
class UTSscript;
class MSKscript;
class ClassifierScript;
class AiTiffScript;
class TIPScript;
#ifdef ARIOL_BUILD
class TMAScript;
#endif
class SpotsScript;
class RegionListScript;
class WoolzScript;
class ImprocScript;
class GPUIPScript;
class CUTSMemRect;
class CUTSDraw;
class CUTSGeneric;
class CUTSBlob;
class CWoolzIP;
class CIPObjects;

#include "scriptutils.h"
#include "scriptsite.h"


/////////////////////////////////////////////////////////////////////////////
// Class CScriptProcessor
// Initialises and runs the Scripting Engine
/////////////////////////////////////////////////////////////////////////////

class SCRIPTPROC_API CScriptProcessor 
{
   
public:
    CScriptProcessor(DWORD InstanceId = 0);
    ~CScriptProcessor();

    // State of the scripting engine
    typedef enum 
    {
        stateUnInitialised,
        stateInitialised,
        stateScriptReady
    } ScriptProcState;

    // Public member functions
public:
    
    // Initialise anything related to the script processing engine
    BOOL InitialiseScriptProcessor(LPCTSTR PersistentsFileName, LPCTSTR QPath, LPCTSTR Qin, LPCTSTR Qout, BOOL SavePersistents = TRUE);
    
    // Release the script processor    
    void ReleaseScriptProcessor(void);

    // Load a script into memory ready for execution
    BOOL LoadScript(LPCTSTR ScriptName, BOOL Expand, BOOL IgnoreInit = FALSE);
    
    // Parse a script    
	BOOL ParseScript(void);

    // Execute a script    
    BOOL ExecuteScript();
	BOOL DeferredExecute(void);
	BOOL DebugScript(DWORD dwSourceContext);

	// Break script execution
	void Break(unsigned long ThreadID);

    // Set the mode the script should operate in
    void SetExecutionMode(ScriptExecMode Mode);

    // Save a script to an ASCII disk file
    BOOL SaveScript(LPCTSTR FileName, CString& TheScript);

    // Reset the script engine
    void Reset(void);

    // Return the current state of the script processor
    ScriptProcState GetState(void) { return m_ScriptProcessorState; };
    
    // Get the current script
    BOOL GetScript(CString &Script);
    // Set the current script
    BOOL SetScript(CString &Script);

    // Turn an image upside down to match UTS coordinate systems
    void FlipImage(VBS_IMAGE_REC *ImageData);

    // Queue Access
    // Put a long on the script input Q
    BOOL PutLong(long L);

    // Get a long from the script output Q
    BOOL GetLong(long *L);

    // Put a double on the script input Q
    BOOL PutDouble(double D);

    // Get a double from the script output Q
    BOOL GetDouble(double *D);

    // Put an image record on the script input Q
    BOOL PutImage(VBS_IMAGE_REC *ImageData);

    // Get an image record from the script output Q
    BOOL GetImage(VBS_IMAGE_REC *ImageData);

    // Put a string on the script input Q
    BOOL PutString(CString& QString);

    // Get a string from the script output Q
    BOOL GetString(CString& QString);

    // Get a generic measurement from the script output Q
    BOOL GetMeasurement(Measurement **pM);

    // Persistent variable stuff
    void ResetPersistentVariables(void);

    void SetLongPersistent(LPCTSTR VarName, long VarValue);

    BOOL GetLongPersistent(LPCTSTR VarName, long *VarValue);

    void SetDoublePersistent(LPCTSTR VarName, double VarValue);

    BOOL GetDoublePersistent(LPCTSTR VarName, double *VarValue);

    void SetStringPersistent(LPCTSTR VarName, CString& VarValue);

    BOOL GetStringPersistent(LPCTSTR VarName, CString& VarValue);

    // Get a persistent from the current instance of the script processor
    BOOL GetPersistent(LPCTSTR VarName, VARIANT FAR *VarValue);

    // Set a persistent from the host side
    void SetPersistent(LPCTSTR VarName, VARIANT FAR& VarValue);

    void CommitPersistents(void);

	void LoadPersistents(void);

	void DeletePersistent(LPCTSTR VarName);

	// Set the instance id
	void SetInstanceId(DWORD Id);

	// Set the window that will display progress information
	void SetProgressWindow(CWnd *pWnd, LPRECT pRect);	
	// Set the window that will display progress information
	void SetProgressWindow(CWnd *pWnd);	
	// Set the window that will display progress information
	void SetProgressWindowFromHandle(HWND hWnd, LPRECT pRect);
	// Give the scripting a progress bar control to play with
	void SetProgressBar(CProgressCtrl *pProg);
	
    // Preload a SVM model
    void SVMModel(CString ModelName);

    // Refresh the display of an image from a windows WM_PAINT message handler
    void DisplayImage(void);


	// Set a parent window for any GUI creation
	void SetParentWindow(CWnd *pParent);
	
	// Script parameters
	CScriptParams *BuildConfigFromScript(void);

	// Get the memrect object
	CUTSMemRect				*UTSMemRect(void);

	public:
    // Holds access to the input and output queues to and from the script
   int                     m_nMacroLines;

#ifdef _SCRIPTPROC_MEM_DEBUG
	_CrtMemState			m_MemCheck1;
	_CrtMemState			m_MemCheck2;
#endif

	
    // Private member functions
private:
	   VBScriptIO              m_ScriptIO;
  
	// Add named items
	BOOL AddNamedItems(void);

    // Read a script file into memory
    int ReadScriptFile(LPCTSTR FileName, CString *pTheScript, BOOL TopLevel, BOOL Expand, int *MacroLines);

	// Script parameters
	void					RemoveScriptParams(CScriptParams *pParams);
	CParamSet				*ParseParamLine(CString &line);

    // Prevent 2 or more threads simulataneously trying to run an analysis script
    CSyncObjectMutex        *m_pScriptExecMutex;

    BSTR                    m_BSTRScript;
    BOOL                    m_bFirstTime;

    // Instance of the IActiveScriptSite implementation for the script processor.
    ActiveScriptSite        g_iActiveScriptSite;
    IActiveScript           *m_iActiveScript;
    IActiveScriptParse      *m_iActiveScriptParse;
    // Script objects - new objects can be added to perform other tasks
    VBscript                *m_pVBScriptObject;               // AII script object 
    UTSscript               *m_pUTSScriptObject;              // UTS script object
    MSKscript               *m_pMSKScriptObject;              // UTS Mask script object
	ClassifierScript		*m_pClassifierScriptObject;       // Classifier script object
	AiTiffScript			*m_pAiTiffScriptObject;			  // AiTiff script object

    SpotsScript             *m_pSpotsScriptObject;            // Spots script object

	WoolzScript             *m_pWoolzScriptObject;            // Woolz script object
	ImprocScript			*m_pImprocScriptObject;			  // Other image processing 	
    GPUIPScript             *m_pGPUIPScriptObject;            // Graphics card image processing
	TIPScript				*m_pTPUIPScriptObject;			  // CUDA image processing

    // Holds the internal state of the processor
    ScriptProcState         m_ScriptProcessorState;
    // Hold the current execution mode
    ScriptExecMode          m_ExecutionMode;    
    // Holds the script we want to load from file and run
    CString                 *m_pTheScript;

	CIPObjects				*m_pIPObjects;

	// Instance Id
	DWORD					m_Id;

	// Interface to zmemrect.dll
	CUTSMemRect				*m_pUTSMemRect;
	// Interface to zblob.dll
	CUTSBlob				*m_pUTSBlob;
	// Interface to zgeneric.dll
	CUTSGeneric				*m_pUTSGeneric;
	// Interface to the woolz library
	CWoolzIP				*m_pWoolzLib;
	// Interface to various drawing routines
	CUTSDraw				*m_pUTSDraw;
};

#endif                  // #ifndef __SCRIPTPROC_H


