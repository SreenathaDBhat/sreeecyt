/////////////////////////////////////////////////////////////////////////////
//
// Imaging Script Processor
//
// Defines the active script site class for the scripting engine
//
// Written By Karl Ratcliff 22032001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//	19Jun2001	JMB	Added support for classifier script object
//	19Oct2001	WH	Added support for AiTiff script object

#ifndef __SCRIPT_H
#define __SCRIPT_H

// These are the names of the objects given to the ActiveScriptSite
// so that it can do a GetItemInfo for each AddNamedItem()
#define   AI_OBJECT_NAME              L"AppliedImagingObject"
#define   AI_UTS_NAME                 L"UTSAIObject"
#define   AI_UTSMASK_NAME             L"UTSMASKAIobject"
#define   AI_CLASSIFIER_NAME          L"ClassifierAIobject"
#define   AI_AITIFF_NAME		      L"AITiffAIobject"
#define   AI_SPOTS_NAME               L"SpotsAIObject"
#define   AI_WOOLZ_NAME               L"WoolzAIObject"
#define   AI_IMPROC_NAME			  L"ImprocAIObject"
#define   AI_GPUIP_NAME               L"GPUIPObject"
#ifdef _IPECUDA
#define	  AI_TIP_NAME				  L"TPUIPObject"
#endif

/////////////////////////////////////////////////////////////////////////////
// This holds the last error when executing a script
/////////////////////////////////////////////////////////////////////////////

typedef struct 
{
    CString Source;
    CString Description;
    unsigned long LineNumber;
    unsigned short ErrorCode;
} ScriptError;

/////////////////////////////////////////////////////////////////////////////
// The IActiveScriptSite implementation...
/////////////////////////////////////////////////////////////////////////////

class ActiveScriptSite : public IActiveScriptSite
{
private:
	ULONG m_dwRef;						// Reference count
	ScriptError m_LastScriptError;		// Last script error info

public:
    // Pointers to objects that are exposed
    // to the script engine in GetItemInfo().
	IUnknown *m_pUnkVBScriptObject;  
	IUnknown *m_pUnkUTSScriptObject;
	IUnknown *m_pUnkMSKScriptObject;
	IUnknown *m_pUnkClassifierScriptObject;
	IUnknown *m_pUnkAiTiffScriptObject;

	IUnknown *m_pUnkSpotsScriptObject;
	IUnknown *m_pUnkRegionListScriptObject;
	IUnknown *m_pUnkWoolzScriptObject;
	IUnknown *m_pUnkImprocScriptObject;
    IUnknown *m_pUnkGPUIPScriptObject;
#ifdef _IPECUDA
	IUnknown *m_pUnkTIPIPScriptObject;
#endif

	// This is to take into account the macro lines that may also be part
	// of an included script
	int			m_nMacroLines;
	void		SetNumMacroLines(int M) { m_nMacroLines = M; }
	
	BOOL GetLastScriptError(ScriptError *pErrInfo);
	
	// Reset previous errors
	void ResetScriptErrors(void);
	
	// Set TRUE when an error occurs
	BOOL		m_bErrorOccurred;
	
	ActiveScriptSite();
	~ActiveScriptSite();

	// IUnknown methods...
	virtual HRESULT _stdcall QueryInterface(REFIID riid, void **ppvObject) 
	{
		*ppvObject = NULL;
		return E_NOTIMPL;
	}
	// Reference counts
	virtual ULONG _stdcall AddRef(void) 
	{
		return ++m_dwRef;
	}
	virtual ULONG _stdcall Release(void) 
	{
		if(--m_dwRef == 0) return 0;
		return m_dwRef;
	}
	
	// IActiveScriptSite methods...
	virtual HRESULT _stdcall GetLCID(LCID *plcid) 
	{
		return S_OK;
	}
	
	// Gets information about an interface exposed to the script engine
	virtual HRESULT _stdcall GetItemInfo(LPCOLESTR pstrName, DWORD dwReturnMask, IUnknown **ppunkItem, ITypeInfo **ppti) 
	{
		
		// Is it expecting an ITypeInfo?
		if(ppti) 
		{
			// Default to NULL.
			*ppti = NULL;
			
			// Return if asking about ITypeInfo... 
			if (dwReturnMask & SCRIPTINFO_ITYPEINFO)
				return TYPE_E_ELEMENTNOTFOUND;
		}
		
		// Is the engine passing an IUnknown buffer?
		if(ppunkItem) 
		{
			// Default to NULL.
			*ppunkItem = NULL;
			
			// Is Script Engine looking for an IUnknown for our object?
			if (dwReturnMask & SCRIPTINFO_IUNKNOWN) 
			{
				// NOTE for each new class exposed to the scripting engine
				// add its IUnknown interface here.
				
				// Check for our object name...
				if (!_wcsicmp(AI_OBJECT_NAME, pstrName)) 
				{
					// Provide our object.
					*ppunkItem = m_pUnkVBScriptObject;
					// Addref our object...
					m_pUnkVBScriptObject->AddRef();
				}
				else
				if (!_wcsicmp(AI_UTS_NAME, pstrName)) 
				{
					// Provide our object.
					*ppunkItem = m_pUnkUTSScriptObject;
					// Addref our object...
					m_pUnkUTSScriptObject->AddRef();
				}
				else
				if (!_wcsicmp(AI_UTSMASK_NAME, pstrName)) 
				{
					// Provide our object.
					*ppunkItem = m_pUnkMSKScriptObject;
					// Addref our object...
					m_pUnkMSKScriptObject->AddRef();
				}
				else
				if (!_wcsicmp(AI_CLASSIFIER_NAME, pstrName)) 
				{
					// Provide our object.
					*ppunkItem = m_pUnkClassifierScriptObject;
					// Addref our object...
					m_pUnkClassifierScriptObject->AddRef();
				}
				else
				if (!_wcsicmp(AI_AITIFF_NAME, pstrName)) 
				{
					// Provide our object.
					*ppunkItem = m_pUnkAiTiffScriptObject;
					// Addref our object...
					m_pUnkAiTiffScriptObject->AddRef();
				}
				else
				if (!_wcsicmp(AI_SPOTS_NAME, pstrName)) 
				{
					// Provide our object.
					*ppunkItem = m_pUnkSpotsScriptObject;
					// Addref our object...
					m_pUnkSpotsScriptObject->AddRef();
				}
				else
				if (!_wcsicmp(AI_WOOLZ_NAME, pstrName)) 
				{
					// Provide our object.
					*ppunkItem = m_pUnkWoolzScriptObject;
					// Addref our object...
					m_pUnkWoolzScriptObject->AddRef();
				}
				else
				if (!_wcsicmp(AI_IMPROC_NAME, pstrName)) 
				{
					// Provide our object.
					*ppunkItem = m_pUnkImprocScriptObject;
					// Addref our object...
					m_pUnkImprocScriptObject->AddRef();
				}
				else
				if (!_wcsicmp(AI_GPUIP_NAME, pstrName)) 
				{
					// Provide our object.
					*ppunkItem = m_pUnkGPUIPScriptObject;
					// Addref our object...
					m_pUnkGPUIPScriptObject->AddRef();
				}
#ifdef _IPECUDA
                else
				if (!_wcsicmp(AI_TIP_NAME, pstrName)) 
				{
					// Provide our object.
					*ppunkItem = m_pUnkTIPIPScriptObject;
					// Addref our object...
					m_pUnkTIPIPScriptObject->AddRef();
				}
#endif
			}
		}
		
		return S_OK;
   }
   
   virtual HRESULT __stdcall GetDocVersionString(BSTR *pbstrVersion) 
   {
	   return S_OK;
   }
   
   virtual HRESULT __stdcall OnScriptTerminate(const VARIANT *pvarResult, const EXCEPINFO *pexcepInfo) 
   {
	   return S_OK;
   }
   
   virtual HRESULT __stdcall OnStateChange(SCRIPTSTATE ssScriptState) 
   {
	   return S_OK;
   }
   
   virtual HRESULT __stdcall EnableModeless(BOOL fEnable)
   {
	   return S_OK;
   } // end EnableModeless
   
   
   virtual HRESULT __stdcall OnScriptError(IActiveScriptError *pscriptError);
   
   
   virtual HRESULT __stdcall OnEnterScript(void);
   
   virtual HRESULT __stdcall OnLeaveScript(void);
};

/////////////////////////////////////////////////////////////////////////////
// Ole-initialization class. 
// Initializes the COM library on the current apartment etc.
// This is defined as a global object within the script processor
// DLL
/////////////////////////////////////////////////////////////////////////////

class OleInitClass 
{
public:
   OleInitClass() 
   {
      OleInitialize(NULL);
   }
   ~OleInitClass() 
   {
      OleUninitialize();
   }
};



#endif
