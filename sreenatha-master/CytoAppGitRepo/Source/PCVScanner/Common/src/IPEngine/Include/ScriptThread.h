/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Script thread
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#ifdef SCRIPTPROC_EXPORTS
#define SCRIPTPROC_API __declspec(dllexport) 
#else
#define SCRIPTPROC_API __declspec(dllimport) 
#endif

class CScriptProcessor;
class CSyncObjectEvent;
class CSyncObjectSemaphore;
class Measurement;
class VarMapBank;

void SCRIPTPROC_API ShowDebug(LPCTSTR Str, DWORD i);

class SCRIPTPROC_API CScriptThread
{
public:
	typedef struct 
	{
		double X, Y, Z;
	} Coordinate;

	CScriptThread(DWORD InstanceId, CString ScriptName, CString Persistents, LPCRITICAL_SECTION pCS, CSyncObjectSemaphore *pSem, VarMapBank *pPersist, LPCTSTR Classifier);
	CScriptThread(DWORD InstanceId, CString ScriptName, LPCRITICAL_SECTION pCS, CSyncObjectSemaphore *pSem, VarMapBank *pPersist, LPCTSTR Classifier);
	~CScriptThread(void);

	void				RequestToEnd(void);
	BOOL				ThreadBusy(void);
	void				SignalInitialised(void);

	void				AddImage(BYTE *ImageData, int BitsPerPixel, int Width, int Height, double X, double Y, double Z);
	BOOL				GetMeasurement(Measurement **pM);
	BOOL				GetImage(VBS_IMAGE_REC &);
	BOOL				GetCoordinates(double &X, double &Y, double &Z);


	// NOTE if calling this make sure the critical section is locked FIRST !!!!!!
	int					GetProcessedCount(void) { return m_ImagesProcessed; }
	void				IncProcessedCount(void);

	CSyncObjectEvent	*EndEvent(void) { return m_pEndEvent; }
	CSyncObjectSemaphore	*ImageReady(void) { return m_pImageReady; }
	CSyncObjectEvent	*IdleEvent(void) { return m_pIdleEvent; }
	CSyncObjectEvent	*ScriptBusyEvent(void) { return m_pScriptBusyEvent; }

	LPCTSTR				Path(void) { return m_szPath; }
	LPCTSTR				QinPath(void) { return m_Qin; }
	LPCTSTR				QoutPath(void) { return m_Qout; }
	LPCTSTR				ScriptName(void) { return m_ScriptName; }
	LPCTSTR				PersistentsName(void) { return m_PersistentsName; }
	VarMapBank			*GetPersistents(void);
	LPCTSTR				Classifier(void) { return m_ClassifierName; }

	HANDLE				Handle(void) { return m_hThread; }
	HANDLE				Idle(void);
	HANDLE				Error(void);
	HANDLE				Results(void);

	void				ResultsReady(void);

	// Output measurements from the processing
	HANDLE				WriteResultsPipe(void) { return m_hResultsWritePipe; }
	HANDLE				ReadResultsPipe(void)  { return m_hResultsReadPipe; }

	HANDLE				WriteResultsCoordPipe(void) { return m_hResultsCoordWritePipe; }
	HANDLE				ReadResultsCoordPipe(void)  { return m_hResultsCoordReadPipe; }

	// Output measurements from the processing
	HANDLE				WriteFragPipe(void) { return m_hFragWritePipe; }
	HANDLE				ReadFragPipe(void)  { return m_hFragReadPipe; }

	// Input images to the processing
	HANDLE				WriteImagePipe(void) { return m_hImageWritePipe; }
	HANDLE				ReadImagePipe(void)  { return m_hImageReadPipe; }

	// Input coordinates to thge processing
	HANDLE				WriteCoordPipe(void) { return m_hCoordWritePipe; }
	HANDLE				ReadCoordPipe(void)  { return m_hCoordReadPipe; }

	DWORD				Id(void) { return m_InstanceId; }

	void				SignalError(void);
	void				SignalIdle(void);

	BOOL				ThreadTerminated(void) { return m_bEnded; }
	void				SignalTerminated(void) { m_bEnded = TRUE; }

	BOOL				HasInitialised(void) { return m_bInitOK; }

private:
	BOOL				Initialise(DWORD InstanceId, CString ScriptName, CString Persistents, LPCRITICAL_SECTION pCS, CSyncObjectSemaphore *pSem, VarMapBank *pPersist, LPCTSTR Classifier);
	VarMapBank			*m_pPersist;

	HANDLE				m_hThread;
	DWORD				m_InstanceId;
	
	CSyncObjectEvent	*m_pInitEvent;
	CSyncObjectEvent	*m_pEndEvent;
	CSyncObjectSemaphore*m_pImageReady;
	CSyncObjectEvent	*m_pIdleEvent;
	CSyncObjectEvent	*m_pScriptBusyEvent;
	CSyncObjectEvent	*m_pErrorEvent;
	
	LPCRITICAL_SECTION  m_pcsCountLock;

	// Images for processing added to this pipe
	HANDLE				m_hImageWritePipe;
	HANDLE				m_hImageReadPipe;

	// Coordinates for processing
	HANDLE				m_hCoordWritePipe;
	HANDLE				m_hCoordReadPipe;

	// Measurements coming out added to this pipe
	HANDLE				m_hResultsWritePipe;
	HANDLE				m_hResultsReadPipe;

	// Measurements coming out added to this pipe
	HANDLE				m_hResultsCoordWritePipe;
	HANDLE				m_hResultsCoordReadPipe;

	// Fragments coming out added to this pipe
	HANDLE				m_hFragWritePipe;
	HANDLE				m_hFragReadPipe;

	// Results semaphore
	CSyncObjectSemaphore *m_pResultsSemaphore;

	DWORD				m_ThreadId;
	TCHAR				m_szPath[MAX_PATH];
	TCHAR				m_Qin[MAX_PATH];
	TCHAR				m_Qout[MAX_PATH];
	TCHAR				m_ScriptName[MAX_PATH];
	TCHAR				m_PersistentsName[MAX_PATH];
	TCHAR				m_ClassifierName[MAX_PATH];
	int					m_ImagesProcessed;
	BOOL				m_bInitOK;
	BOOL				m_bEnded;
};
