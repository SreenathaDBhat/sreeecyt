#pragma once

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Script thread pool
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "cputopology.h"

#ifdef SCRIPTPROC_EXPORTS
#define SCRIPTPROC_API __declspec(dllexport) 
#else
#define SCRIPTPROC_API __declspec(dllimport) 
#endif

class CScriptThread;
class CpuTopology;
class CSyncObjectEvent;

#define MAX_SCRIPT_THREADS		16
#define INVALID_SCRIPT_THREAD	MAX_SCRIPT_THREADS + 1

class SCRIPTPROC_API CScriptThreadPool
{
public:
	typedef enum tagThreadState
	{
		ThreadFree = 0,
		ThreadTimeout = 1,
		ThreadError = 2,
		ThreadOk = 3
	} ThreadState;

	CScriptThreadPool(DWORD N);
	~CScriptThreadPool(void);

	BOOL Start(CString ScriptName, VarMapBank *p, LPCTSTR Classifier);											// Start them all
	BOOL Start(CString ScriptName, CString PersistentsName, VarMapBank *pPersistents, LPCTSTR Classifier);		// Start them all
	BOOL ShutDown(DWORD Millisecs);				// Shut them all down

	void WaitForEnd(DWORD Millisecs);			
	BOOL HasEnded(void);		
	void SignalEnd(void);

	CScriptProcessor *GetProcessor(DWORD Proc);	// Get the script engine for a thread in the pool

	BOOL FindFreeProcessor(DWORD &FreeProc);	// Find a free thread	
	ThreadState WaitForFreeProcessor(DWORD Millisecs, DWORD &FreeProc);	// Wait for a free processor	
	ThreadState Error(DWORD Millisecs);			// Has an error occurred
	ThreadState Status(void);			// Has an error occurred

	void AddImage(DWORD Proc, BYTE *ImageData, int BitsPerPixel, int Width, int Height, double X = 0.0, double Y = 0.0, double Z = 0.0);	// Add an image for processing
	BOOL GetData(VBS_IMAGE_REC &ImageData, CMeasures &MeasurementData, CScriptThread::Coordinate &Coord);	// Get a frag image and associated measurements
	BOOL DataAvailableFromProcessor(DWORD &Proc);
	BOOL IsDataAvailable(DWORD TimeOut);

	int	 GetProcessedCount(void);				// Get the total images processed so far
	int  ImagesToProcess(void);					// Remaining images to process
	int  ImagesAdded(void);						// Images added

private:

	CScriptThread		*m_ThreadPool[MAX_SCRIPT_THREADS];
	DWORD				m_dwCPUs;
	DWORD				m_NumThreads;
	int					m_ImagesAdded;
	CSyncObjectEvent	*m_pEndEvent;
	BOOL				m_bInitOK;
	CSyncObjectSemaphore *m_pResultsSemaphore;
	CRITICAL_SECTION	 m_csImageCountLock;
};
