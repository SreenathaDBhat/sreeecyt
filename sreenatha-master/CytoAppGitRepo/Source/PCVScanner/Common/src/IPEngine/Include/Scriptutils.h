
/////////////////////////////////////////////////////////////////////////////
// Script utility functions and definitions 
//
//
// Written By K Ratcliff 12042001
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#ifndef __SCRIPTUTILS_H
#define __SCRIPTUTILS_H

class CQueue;
class Measurement;

/////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////

// Names for the input and output queues to the VB script
#define VB_TOSCRIPT_Q               "VBIN"
#define VB_FROMSCRIPT_Q             "VBOUT"

// Max size of the input and output queues (1 MB)
#define VB_Q_SIZE                   1024 * 1024

// Wait timeout for Q locking in milliseconds
#define VB_Q_TIMEOUT                2000

// Maximum string size to send on a Q
#define VB_MAXSTRINGSIZE            255

/////////////////////////////////////////////////////////////////////////////
// Typedefs & enums
/////////////////////////////////////////////////////////////////////////////

// Execution state to work in
// NOTE these have specific values as they are fed back to the
// VB script itself
typedef enum {
    modeDebug = 0,                  // Script running in debug mode
    modeInteractive = 1,            // Script running in interactive mode
    modePlay = 2                    // Script running in play mode
} ScriptExecMode;

// Type of data on a queue

typedef enum {
    VBS_INT     = 1,          // int   - not supported - yet
    VBS_LONG    = 2,          // long
    VBS_UINT    = 3,          // UINT  - not supported - yet
    VBS_ULONG   = 4,          // ULONG - not supported - yet
    VBS_DOUBLE  = 5,          // double
    VBS_IMAGE   = 6,          // image - see next typedef
    VBS_STRING  = 7,          // string
    VBS_AITIFF  = 8,                // AITIFF
} VBS_TYPE;

#define DEFAULT_NUMERIC_ELEMENTS        5

// An object of type image on the queue is made up 
// of several components - mainly the image address
// in memory, the pixel dimensions and bits per pixel
// NB this will only work if both ends of the queue
// are in the same address space as no actual image
// data is transferred via the queue

typedef struct {
    long Width, Height;         // Size of the image
    BYTE BitsPerPixel;          // pixel depth
    void *Address;              // Address of the image memory
} VBS_IMAGE_REC;

typedef struct {
    long Width, Height;         // Size of the image
    BYTE BitsPerPixel;          // pixel depth
    void *Address;              // Address of the image memory
} VBS_AITIFF_REC;

// For strings - pass an address and a string length

typedef struct {
	char *Address;				// Address of the string
	long Length;				// Length in bytes of the string
} VBS_STRING_REC;

// Stores data in a numeric array
typedef struct {
    double *Elements;
} NumericArrayData;

typedef NumericArrayData *NumericArrayPtr;

// These define a list of CArray objects allowing dynamic lists in VB script
typedef CArray<NumericArrayPtr, NumericArrayPtr>    NumericArray;

//typedef NumericArray                *VBArrayPtr;
typedef struct {
    NumericArray        *m_pArray;
    int                 m_nColumns;
} VBArray;

typedef VBArray     *VBArrayPtr;

/////////////////////////////////////////////////////////////////////////////
// Prototypes
/////////////////////////////////////////////////////////////////////////////

BOOL HRVERIFY(HRESULT hr, LPCTSTR msg);

// Returns the variant as a double value
double VariantDbl(VARIANT V);

// Compare two variants 
BOOL VariantLessThan(VARIANT V1, VARIANT V2);

// Compare two variants 
BOOL VariantGreaterThan(VARIANT V1, VARIANT V2);

// Compare two variants 
BOOL VariantEqual(VARIANT V1, VARIANT V2);



/////////////////////////////////////////////////////////////////////////////
// This is a list of CArray objects
/////////////////////////////////////////////////////////////////////////////

class CArrayList {
public:
    CArrayList();
    ~CArrayList();

    // Allocate a new CArray object
    VBArrayPtr NewArray(long MaxSize);
    VBArrayPtr NewArray(long MaxSize, long NumColumns);

    // Delete an array
    void DeleteArray(VBArrayPtr pVBArr);

    // Free elements in an array
    void FreeElements(VBArrayPtr pVBArr);

    // Free all currently allocated CArray objects
    void DisposeAll(void);

    // Check an array is valid
    BOOL CheckValid(VBArrayPtr pVBArr);
    
    // Get the value at an index
    BOOL GetElement(VBArrayPtr pVBArr, int Index1, int Index2, VARIANT *pV);
    // Get the value at an index
    BOOL GetElement(VBArrayPtr pVBArr, int Index1, NumericArrayData **pNA);

    // Sort an array (bubble)
    void Sort(VBArrayPtr p, int Index);

    // Set the value at an index
    BOOL SetElement(VBArrayPtr pVBArr, int Index1, int Index2, VARIANT V);

    // Return the upper bound of an array
    int UBound(VBArrayPtr pVBArr);

private:
    // Maintains a list of CArray objects
    CList<VBArrayPtr, VBArrayPtr> m_ArrayList;
};

/////////////////////////////////////////////////////////////////////////////
// This class is responsible for I/O in the To and From queues
// for both CScriptProcessor class and the VBScript class
/////////////////////////////////////////////////////////////////////////////

class VBScriptIO
{
public:
    VBScriptIO();
    ~VBScriptIO();

    // Public member functions
    // Initialise the Qs
    BOOL InitialiseScriptQs(LPCTSTR Qpath, LPCTSTR FromQName, LPCTSTR ToQName);

    // Reset
    void Reset(void);

    // Send a long to and get a long from the queues
    BOOL SendLongTo(long L);
    BOOL GetLongFrom(long *L);

    // Send a double to and get a double from the queues
    BOOL SendDoubleTo(double D);
    BOOL GetDoubleFrom(double *D);

    // Send and get an image address to and from the queues
    BOOL SendImageTo(VBS_IMAGE_REC *ImageData);
    BOOL GetImageFrom(VBS_IMAGE_REC *ImageData);
    BOOL SendImageTo(VBS_AITIFF_REC *ImageData);

    // Send and get a string
    BOOL SendStringTo(LPCTSTR StringToSend);
    BOOL GetStringFrom(CString& StringToGet);

private:
    // General put on a queue and get from a queue
    BOOL PutOnQueue(CQueue *QToUse, void *Data, UINT Size, VBS_TYPE Type);
    BOOL GetFromQueue(CQueue *QToUse, void *Data, UINT *Size, VBS_TYPE Type);
	// make absolutely sure nothing left on the Q
	void ClearQ(CQueue *Q);

private:
    // Input and output queues
    CQueue      *m_pToQ;
    CQueue      *m_pFromQ;
};

#endif          // #ifndef __SCRIPTUTILS_H
