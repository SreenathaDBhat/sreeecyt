// SeedFinder.h: interface for the CSeedFinder class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include <string.h>
#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <utility>

using namespace std;

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

//
// CSeedFinder - Finds seeds from images representing significant objects.
// This can be used in conjunction with the Seeded Region Growing class, CSRGEngine.
//
class UTS_API CSeedFinder
{
// Constructors/Destructors
public:
	CSeedFinder( CUTSMemRect * pMR, CWoolzIP * pWoolzIP);
	virtual ~CSeedFinder();

// Attributes
public:

// Operations
public:
	long Find( long hSrc, long hObjectMask);
	long FindByLocalmaxima( long hSrc, long hObjectMask);

// Implementation
protected:
	CUTSMemRect *_pUTSMemRect;
	CWoolzIP * _pWoolzIP;

	void FilterMaxima( BYTE * pSrcImage, BYTE * pMaxima, long width, long height, CArray<WoolzPoint> & maxima, BYTE tolerance);
	static int CompareWoolzPoint(const void * pA, const void * pB);
	void RemoveBorderWidthOne( BYTE * pImage, long width, long height);
	void image_dump(BYTE * pImage, long width, long height, char *filename);
};


