/////////////////////////////////////////////////////////////////////////////
//
// This is the Spots specific script engine keywords class. 
// This is a runtime class
// and the interface to this class gets handed over to the VB
// scripting engine.
//
// Written By Karl Ratcliff 22072001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#if !defined(AFX_SpotsScript_H__1C1EB773_1B2E_4C40_B61E_13A970B85C0B__INCLUDED_)
#define AFX_SpotsScript_H__1C1EB773_1B2E_4C40_B61E_13A970B85C0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SpotsScript.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// SpotsScript command target

class SpotsScript : public CCmdTarget, public ScriptObject
{
	DECLARE_DYNCREATE(SpotsScript)

	SpotsScript();           // protected constructor used by dynamic creation
	virtual ~SpotsScript();

// Attributes
public:
    ScriptExecMode       m_ExecutionMode;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SpotsScript)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(SpotsScript)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(SpotsScript)
	afx_msg long FindSpots(long SrcImage, LPCTSTR Oper, long Threshold, long MinArea);
	afx_msg long Spots(long SrcImage, long Threshold, long MinArea, long MaxArea);
	afx_msg long NumberOfSpots();
	afx_msg BOOL GetSpot(long SpotNumber, VARIANT FAR* SpotData);
	afx_msg void SetSpotTag(long SpotNum, long TagValue);
	afx_msg long DeleteTag(long TagValue);
	afx_msg void SpotMask(long SrcImage, long MaskImage);
	afx_msg long QuantileSpots(long SrcImage, long ResultMask, long BegMask, long MinArea, long MaxArea, double QuantileLo, double QuantileHi, long MaxRep, long NOpen);
	afx_msg long WaterShed(long SrcImage, long MaskImage, long Borders, long ThrMin, long AreaMax, long FormFactor, long FormThresh, long FormType);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SpotsScript_H__1C1EB773_1B2E_4C40_B61E_13A970B85C0B__INCLUDED_)
