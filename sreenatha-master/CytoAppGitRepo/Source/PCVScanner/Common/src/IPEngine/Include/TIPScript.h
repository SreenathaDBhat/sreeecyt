/////////////////////////////////////////////////////////////////////////////
//
// This is the TIP specific script engine keywords class. 
// This is a runtime class
// and the interface to this class gets handed over to the VB
// scripting engine.
//
// Written By Hans Vrolijk 12122007
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#if !defined(AFX_TIPSCRIPT_H__1C1EB773_1B2E_4C40_B61E_13A970B85C0B__INCLUDED_)
#define AFX_TIPSCRIPT_H__1C1EB773_1B2E_4C40_B61E_13A970B85C0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TIPScript.h : header file
//
#include <tchar.h>

class TIP;
class TIPImage;

/////////////////////////////////////////////////////////////////////////////
// TIPScript command target

class TIPScript : public CCmdTarget
{
	DECLARE_DYNCREATE(TIPScript)

	TIPScript();           // protected constructor used by dynamic creation
	virtual ~TIPScript();

// Attributes
public:
    ScriptExecMode       m_ExecutionMode;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GPUIPScript)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL
    void Reset();
	BOOL IsTIPInUse(void);

// Implementation
protected:

    void CheckSpecsTipImage(TIPImage **gpImage, long Device, long Width, 
							long Height, long Overlap, long Bpp);		// Check TipImage dimensions

	DECLARE_MESSAGE_MAP()
#ifdef _IPECUDA
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(TIPScript)
    afx_msg BOOL Create(void);
	afx_msg void DestroyTIP(void);
    afx_msg BOOL PutTIP(long Image, TIPImage **Buffer);
    afx_msg BOOL GetTIP(long Image, TIPImage **Buffer);
    afx_msg void OpenTIP(long Passes, TIPImage **SrcBuf, TIPImage **DestBuf);
    afx_msg void CloseTIP(long Passes, TIPImage **SrcBuf, TIPImage **DestBuf);
    afx_msg void DilateTIP(long Passes, TIPImage **SrcBuf, TIPImage **DestBuf);
    afx_msg void ErodeTIP(long Passes, TIPImage **SrcBuf, TIPImage **DestBuf);
    afx_msg void AddTIP(TIPImage **Src1, TIPImage **Src2, TIPImage **DestBuf);
    afx_msg void SubtractTIP(TIPImage **Src1, TIPImage **Src2, TIPImage **DestBuf);
    afx_msg void MultiplyTIP(TIPImage **Src1, TIPImage **Src2, TIPImage **DestBuf);
    afx_msg void DivideTIP(TIPImage **Src1, TIPImage **Src2, TIPImage **DestBuf);
    afx_msg void DiffTIP(TIPImage **Src1, TIPImage **Src2, TIPImage **DestBuf);
    afx_msg void ModTIP(TIPImage **Src1, TIPImage **Src2, TIPImage **DestBuf);
    afx_msg long CreateTIPImageAs(long SrcBuf, long Device, long Overlap, BOOL CudaArrayFlg);
    afx_msg long CreateTIPImage(long Device, long Width, long Height, long Overlap,
					long BPP, BOOL CudaArrayFlg);
    afx_msg void FreeTIPImage(VARIANT FAR* HandleTipImage);
    afx_msg void ReleaseTIPImage(VARIANT FAR* HandleTipImage);
	afx_msg void GetTIPImageSpecs(TIPImage **GIPSource, VARIANT FAR* Device, 
					VARIANT FAR* Width, VARIANT FAR* Height, VARIANT FAR* Overlap,
					VARIANT FAR* BPP, VARIANT FAR* CudaArrayFlg);
	afx_msg long GetDeviceCountTIP(void); 
    afx_msg void AddConstTIP(TIPImage **Src1, float val, TIPImage **DestBuf);
    afx_msg void SubtractConstTIP(TIPImage **Src1, float val, TIPImage **DestBuf);
    afx_msg void MultiplyConstTIP(TIPImage **Src1, float val, TIPImage **DestBuf);
    afx_msg void DivideConstTIP(TIPImage **Src1, float val, TIPImage **DestBuf);
    afx_msg void ThreshTIP(TIPImage **Src1, float thresh, TIPImage **DestBuf);
    afx_msg void CopyTIP(TIPImage **Src1, TIPImage **DestBuf);
    afx_msg void SobelTIP(TIPImage **Src1, TIPImage **DestBuf);
    afx_msg void GaussianTIP(long FiltHW, float Sigma, TIPImage **Src, 
					TIPImage **Dest);
    afx_msg void SplitComponentsTIP(TIPImage **Src, TIPImage **Targ1, TIPImage **Targ2, 
					TIPImage **Targ3);
    afx_msg void JoinComponentsTIP(TIPImage **Src1, TIPImage **Src2, TIPImage **Src3, 
					TIPImage **Target);
    afx_msg void ComponentThresholdTIP(float Rmin, float Rmax, float Gmin, float Gmax, float Bmin, 
					float Bmax, TIPImage **Src, TIPImage **Targ1);
    afx_msg void BlendComponentsTIP(TIPImage **Src1, float A1, TIPImage **Src2, float A2, 
					TIPImage **Src3, float A4, TIPImage **Target);
    afx_msg void ComposedBlendTIP(TIPImage **Src1, float A1, float A2, float A3, TIPImage **Target);
	afx_msg void RGBHSITIP(TIPImage **Src, TIPImage **Target);
    afx_msg void HSIRGBTIP(TIPImage **Src, TIPImage **Target);
	afx_msg void ColourDeconvTIP(TIPImage **Src, VARIANT FAR* pVCV1, VARIANT FAR* pVCV2, 
					VARIANT FAR* pVCV3, TIPImage **Dest);
	afx_msg void MedianTIP(long Passes, TIPImage **Src, TIPImage **Dest);
    afx_msg void LaplaceTIP(TIPImage **Src1, TIPImage **DestBuf);
    afx_msg void MeanTIP(TIPImage **Src1, TIPImage **DestBuf);
	afx_msg long Histogram(TIPImage **ImageHandle,  long ColourMode);
	afx_msg void SimpleBlend(VARIANT FAR* pImages, TIPImage **Dest);
	afx_msg void HDRBlend(VARIANT FAR* pImages, TIPImage **Dest);
	afx_msg void RangeBlend(VARIANT FAR* pImages, TIPImage **Dest);
	afx_msg void CombineMultiExposures(long Type, VARIANT FAR* Images, long Output);
    afx_msg void ConvolveSymTIP(long nPasses, VARIANT FAR *Coeff, TIPImage **Src, TIPImage **Dest);
    afx_msg void ConvolveTIP(long nPasses, VARIANT FAR *CoeffHor, VARIANT FAR *CoeffVert, 
					TIPImage **Src, TIPImage **Dest);
	afx_msg void KuwaharaTIP(long FilterHW, TIPImage **Src, TIPImage **Dest);
	afx_msg void UnpackGreyTIP(TIPImage **Src, TIPImage **Dest);
	afx_msg void PackGreyTIP(TIPImage **Src, TIPImage **Dest);
	afx_msg void ConvertTIP(TIPImage **Src, TIPImage **Dest);
    afx_msg BOOL GetComponentTIP(long Image, long component, TIPImage **Buffer);
	afx_msg BOOL PutSubImageTIP(long Image, TIPImage **gpImage, long x0, long y0, long ColourMode);
	afx_msg long CropImageTIP(TIPImage **Src, long x0, long y0, long w0, long h0);
	afx_msg long ResizeImageTIP(TIPImage **Src, long w0, long h0, long overl);
	afx_msg long FFT2DTIP(TIPImage** Src, long flag, long renderTarget);
	afx_msg long FFT2DPowerSpectrumTIP(TIPImage **Src, long bppOut);
	afx_msg long ButterWorthFFT2DTIP(TIPImage **Src, long filtType, float f1, float f2, float order);
	afx_msg long GaussianFFT2DTIP(TIPImage **Src, long filtType, float f1, float f2);
	afx_msg void FFTImageRegistrationTIP(TIPImage **Src1, TIPImage **Src2, VARIANT FAR*  XShift, 
					VARIANT FAR*  YShift, VARIANT FAR* SN, long MaxShift);
	afx_msg void CoreFFTImageRegistrationTIP(TIPImage **FFTSrc1, TIPImage **FFTSrc2, VARIANT FAR*  XShift, 
					VARIANT FAR*  YShift, VARIANT FAR* SN, long MaxShift);
	afx_msg void FFTImageStitchTIP(long Source1, long Source2, long mode, long overlap, VARIANT FAR*  XShift, 
					VARIANT FAR*  YShift, VARIANT FAR* SN);
	afx_msg void CoreFFTImageStitchTIP(TIPImage **src1, TIPImage **src2, long mode, long overlap, 
					VARIANT FAR*  XShift, VARIANT FAR*  YShift,	VARIANT FAR* SN);
    afx_msg void UnsharpMaskTIP(long FiltHW, float Sigma, float Amount, TIPImage **Src, TIPImage **Dest);
 	afx_msg void NearestNeighbDeconvTIP(long FiltHW, float Sigma, float Amount, VARIANT FAR* pImages, 
					VARIANT FAR* pDestImages);
	afx_msg void DistanceMapTIP(TIPImage **src, TIPImage **dest, BOOL EDMOnly, BOOL IsWhiteBackground, 
					float NormFactor);
	afx_msg void CannyEdgeTIP(TIPImage **src, TIPImage **dest, float thresholdLow, float thresholdHigh);
#endif

	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
#ifdef _IPECUDA
    TIP             *m_pTIP;            // Hardware assisted image processing
#endif
    BOOL			m_bTIPCreated;      // Already created 
	int				m_bTIPDevices;
    BOOL            m_bUseTIP;          // Flags TIP usage
};

#endif
