#pragma once
#include "spotdefs.h"

//#include "spotdata.h"
#include "SlideMap.h"

class SlideMap;
class CQuadrantData;

class CTMAFunc
{
public:
	CTMAFunc(void);
	~CTMAFunc(void);
	long FindTMACrop		(long SrcImage, long* X0, long* Y0, long* X1, long* Y1,
								int nr_filter, float deviationPerc, float subMapDistFact, LPCTSTR xmlFile);
	long CropImage			(long SrcImage, long X0, long Y0, long X1, long Y1, long DestImage); 
	void HoriProfile		(long *pPlotLin, long pltLng, BYTE *srcAddr, 
								long srcW, long srcH, long srcP, long y0, long y1);
	void VertProfile		(long *pPlotLin, long pltLng, BYTE *srcAddr, 
								long srcW, long srcH, long srcP, long x0, long x1);
	void FilterProfile		(long *pPlotLin, long pltLng, int nr_filter = 15);
	double FindBoundaries	(long *pPlotLin, long pltLng, long *c0, long *c1, 
							float deviationPerc = 30.0, float subMapDistFact = 3.0);
	SlideMap				m_coremap;
	CQuadrantData			*m_xmlQuads;
};
