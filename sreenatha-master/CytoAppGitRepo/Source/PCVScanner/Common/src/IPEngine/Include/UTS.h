////////////////////////////////////////////////////////////////////////////////////////
//
// An Interface to the Borland C UTS DLLs 
//
// Written By Karl Ratcliff 01032001
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#ifndef __UTS_H
#define __UTS_H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

#include "vcmemrect.h"
#include "vcutsdraw.h"

////////////////////////////////////////////////////////////////////////////////////////
// Common Defines

// Define this to aid UTS handle debugging
//#define UTS_HANDLE_DEBUG

// Supported image types - so far
#define ONE_BIT_PER_PIXEL               1
#define EIGHT_BITS_PER_PIXEL            8
#define SIXTEEN_BITS_PER_PIXEL			16
#define TWENTYFOUR_BITS_PER_PIXEL       24
#define THIRTYTWO_BITS_PER_PIXEL        32

// Number of bins for a histogram for the given bit depth
#define NBINS_EIGHT						256

// General UTS error code
#define UTS_ERROR                       -1
#define UTS_OK                           0

//#define new DEBUG_NEW
// used to debug keywords in release
//#define KEYWORD_DEBUG


////////////////////////////////////////////////////////////////////////////////////////
// Macros

// Check a UTS handle is valid
#define UTSVALID_HANDLE(H) (H > 0)

// Check parameter against a list of keywords
#define CHECK_PARAMETER(KeywordList, Keyword) (_tcsstr(KeywordList, Keyword) != NULL)

////////////////////////////////////////////////////////////////////////////////////////

UTS_API CDibInfo *GetDibInfo(void);


#endif
