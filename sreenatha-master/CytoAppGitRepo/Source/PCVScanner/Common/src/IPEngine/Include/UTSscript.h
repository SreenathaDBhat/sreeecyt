/////////////////////////////////////////////////////////////////////////////
//
// This is the VB OLE automation server. This is a runtime class
// and the interface to this class gets handed over to the VB
// scripting engine. This class will implement the UTS keywords required
// 
//
// Written By Karl Ratcliff 01032001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//
#if !defined(AFX_UTSSCRIPT_H__143C5216_19D2_48EA_8B2B_95F13D5C3C12__INCLUDED_)
#define AFX_UTSSCRIPT_H__143C5216_19D2_48EA_8B2B_95F13D5C3C12__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UTSscript.h : header file
//

#define MAX_EXTERNAL_IMAGES         3000

class CIPObjects;

/////////////////////////////////////////////////////////////////////////////
// UTSscript command target
// This implements our dispatch interface which is then exposed
// to the scripting engine for UTS commands
/////////////////////////////////////////////////////////////////////////////

class UTSscript : public CCmdTarget, public ScriptObject
{
	DECLARE_DYNCREATE(UTSscript)

	UTSscript();           // protected constructor used by dynamic creation

// Attributes
public:
	virtual ~UTSscript();
    ScriptExecMode       m_ExecutionMode;

    typedef struct 
    {
        long W, H, BPP;
        BYTE *Image;
    } ExternalImageStruct;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UTSscript)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

    // Reset the script queues etc.
    void Reset(void);
// Implementation

private:
    ExternalImageStruct     m_ExternalImages[MAX_EXTERNAL_IMAGES];

protected:
	// Generated message map functions
	//{{AFX_MSG(UTSscript)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
    SAFEARRAY *GetSafeArray(VARIANT FAR *pSafe, int nDims, long *lB, long *uB);

	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(UTSscript)
	afx_msg long MakeHistogram(VARIANT FAR*  HistogramData);
	afx_msg long Histogram(long hImage);
	afx_msg long HistogramMask(long hImage, long hMask);
	afx_msg void GetHistogramData(long Histogram, VARIANT FAR* HistogramData);
	afx_msg void HistogramInfo(long HistogramHandle, VARIANT FAR* Information);
	afx_msg long LoadDIB(LPCTSTR FilePath);
	afx_msg void SaveDIB(long SrcImage, LPCTSTR PathAndName);
	afx_msg void SaveJPG(long UTSHandle, LPCTSTR FilenameAndPath, short Quality);
	afx_msg long LoadJPG(LPCTSTR FileNameAndPath);
	afx_msg void SaveJPGLS(long UTSHandle, LPCTSTR FilenameAndPath);
	afx_msg long LoadJPGLS(LPCTSTR FileNameAndPath);
	afx_msg long LoadRAW(LPCTSTR FileNameAndPath, long Width, long Height, long BPP);
	afx_msg void SaveRAW(long UTSHandle, LPCTSTR FilenameAndPath);
	afx_msg long SaveAs(long SrcImage, LPCTSTR PathAndName);
	afx_msg void Threshold(long SrcImage, long DestImage, LPCTSTR Operator, long ThreshValue);
	afx_msg void ThresholdGTE(long hSrc, long hDstMsk, long threshVal);
	afx_msg BOOL HandleIsValid(long UTSHandle);
	afx_msg void Move(long SrcImage, long DestImage);
	afx_msg long CreateAsUsingBPP(long ImageHandle, long BitsPerPixel);
	afx_msg long Create(long BitsPerPixel, long Width, long Height);
	afx_msg void FreeIm(VARIANT FAR* Handle);
	afx_msg long CreateAs(long SrcImage);	
	afx_msg void CutImageFrag(long SrcImage, long CentreX, long CentreY, long DestImage, VARIANT FAR *Xleft, VARIANT FAR *Ytop);
	afx_msg void ImageToRGB(long SrcImage, long DestImage, LPCTSTR Oper, long R, long G, long B);
	afx_msg void SmoothByZoom(long SrcImage, long Iterations, long ZoomLevel, LPCTSTR Oper1, LPCTSTR Oper2);
	afx_msg void AlphaBeta(long H1, long H2, long H3, long Alpha, long Beta, long Gamma);
	afx_msg long Projection0(long SrcImage, LPCTSTR Oper);
	afx_msg long GetRectInfo(long SrcImage, VARIANT FAR* Width, VARIANT FAR* Height);
	afx_msg long GetRectBPP(long hImage);
	afx_msg long GetRectOriginalBPP(long hImage);
	afx_msg double LogSum(long hScrImage, long hMaskImage);
	afx_msg void SmoothLongs(long Handle, long NPoints, short NSmooth);
	afx_msg long Stat1(long SrcImage, VARIANT FAR* StatData);
	afx_msg long MinMaxSumInfo(long SrcHandle, VARIANT FAR* Info);
	afx_msg long MinsAndMaxs(VARIANT FAR* Data, long PnPnt, long Noise, long Thresh, long Cycle);
	afx_msg void ThickProfile(long SrcHandle, VARIANT FAR* Points, double X1, double Y1, double X2, double Y2, double RectWidth);
	afx_msg void RGBToHSI(long R, long G, long B, long H, long S, long I);
	afx_msg void RGBToColour(long R, long G, long B, long RGBHandle);
	afx_msg void RGBToGray(long RGBImage, long GrayImage, long R, long G, long B);
	afx_msg void Median(long SrcImage);
    afx_msg void PixelWise(long SrcImage, LPCTSTR Operator, long ResultImage);
	afx_msg void ConstPixelWise(long SrcImage, LPCTSTR Operator, long ResultImage);
	afx_msg void PixelWiseMask(long SrcImage, LPCTSTR Operation, long MaskImage, long DestImage);
	afx_msg void CopyUnderMask(long hSrc, long hMsk, long hDst);
	afx_msg void MulDiv(long SrcImage, long Multiplier, long Divisor);
	
	afx_msg void MoveSubRect(long BigImage, LPCTSTR Direction, long SmlImage, long Left, long Top);
	afx_msg void Zoom(long SrcHandle, long DestHandle, LPCTSTR Method);
	afx_msg void ColorSaturTrans(VARIANT FAR* HandlesArray, short NumberOfHandles, double X1, double Y1, double X2, double Y2);
	afx_msg void RangeThreshold(long ImageHandle, long ImageMask, short LowThreshold, short HighThreshold);
	afx_msg BOOL FFTRegisterImages(long hImage1, long hImage2, VARIANT FAR* xshift, VARIANT FAR* yshift, long maxshift);
	afx_msg BOOL FFTPower(long hImage, VARIANT FAR* new_width, VARIANT FAR* new_height);
	afx_msg BOOL FFT(long hImage, VARIANT FAR* new_width, VARIANT FAR* new_height);
	afx_msg long RegisterImages(long hImage1, long hImage2, long minshift, long maxshift, long stripsize, LPCTSTR orientation);
	afx_msg void FlipVertical(long SrcImage);
	afx_msg void FlipHorizontal(long SrcImage);
	afx_msg void ImageConvert(long SrcImage, long DestImage, long LevelParam);
	afx_msg void IPLConvolve(long SrcImage, long Xc, long Yc, VARIANT FAR* Coefficients, short RShift);
	afx_msg void Average(long srcImage, short iter);
	afx_msg void ConstPixelMask(long Constant, LPCTSTR Oper, long HandleR, long HandleM);
	afx_msg void ConstPixelNotMask(long Constant, LPCTSTR Oper, long HandleR, long HandleM);
	afx_msg long ContShapeInfo(VARIANT FAR* PCodes, long NCodes, long Xstart, long Ystart, VARIANT FAR* Results);
	afx_msg void QuantileThreshold(long srcImage, long destImage, LPCTSTR op, long thresholdQuantilePercent);
	afx_msg void ReDimension(long hImage, long bpp, long width, long height);
	afx_msg long GetPixel(long hImage, long x, long y);
	afx_msg void SetPixel(long hImage, long x, long y, long value);
	afx_msg void IPLibrary(LPCTSTR NewLib);
	afx_msg void ColourToRGB(long RGBImage, long R, long G, long B);
	afx_msg void HSIToRGB(long H, long S, long I, long R, long G, long B);
	afx_msg long NumberUsedHandles();
	afx_msg long WatershedByEDM(long hInput, long hOutputEDM, long hOutputUEP, long hOutputWS, LPCTSTR Operator, bool isWhiteBackground);
	afx_msg long AutoThreshold(long hInput, VARIANT FAR *minThreshold, VARIANT FAR *maxThreshold);
	afx_msg void RankFilter(long hInput, long hOutput, double maskRadius, LPCTSTR operation);
	afx_msg void MixtureModelThreshold(long hInput, VARIANT FAR *theThreshold);
	afx_msg long Ellipse(long SrcHandle, long xc, long yc, double a, double b, double phi, long color);
    afx_msg void Convert8To1(long Handle8, long Handle1);
    afx_msg void Convert1To8(long Handle1, long Handle8);
	afx_msg void Convert1To8WithValue(long Handle1, long Handle8, BYTE value);
	afx_msg void Convert16To8(long hSrc, long hDst);
	afx_msg void Convert16To8Scaled(long hSrc, long hDst, long Min, long Max);
	afx_msg void Convert8To16(long hSrc, long hDst);
	afx_msg void HistogramStretch16To8( long hSrc, long hDst);
	afx_msg void ContrastStretch(long hImage);
    afx_msg long PDMImage(long Image1, short Mean1, long Image2, short Mean2);
    afx_msg void OverlapK1K2(long Image1, long Image2, VARIANT FAR *k1, VARIANT FAR *k2);
    afx_msg void MandersK1K2(long Image1, long Image2, VARIANT FAR *k1, VARIANT FAR *k2);
    afx_msg double Pearsons(long Image1, long Image2, long RegionMask);
    afx_msg long ColourDeconv(long SrcImage, VARIANT FAR *v1, VARIANT FAR *v2, VARIANT FAR *v3);
    afx_msg long MaxProjectionImage(VARIANT FAR *ImArr, long NImages);
    afx_msg long ProjectionImage(long IdxImage, VARIANT FAR *ImArr, long NImages);
    afx_msg long MakeExternalImage(long Width, long Height, long BPP);
    afx_msg long ExternalImageToUTS(long Idx);
    afx_msg void UTSToExternalImage(long UTSHandle, long Idx);
    afx_msg long DetectCover(long UTSHandle, VARIANT FAR *ymin, VARIANT FAR *ymax, VARIANT FAR *left, VARIANT FAR *right);
    afx_msg long DetectCircularCover(long UTSHandle, long radius, VARIANT FAR *x1, VARIANT FAR *x2, VARIANT FAR *y1, VARIANT FAR *y2);
	afx_msg double FractalBoxCount(long Image, long Iterations);
	afx_msg void SRGSegment( long image, long seeds, long segmentation);
	afx_msg void Variance( long src, long dst, BYTE radius);
	afx_msg void BlankBorder( long src, int blankWidth);
	afx_msg void TrimSkeletonTips( long src, long dst, int removePixelCount);
	afx_msg void LocalMaxima( long src, long dst, BYTE radius);
	afx_msg long GetSeeds( long src, long objectMask);
	afx_msg long GetSeedsByLocalMaxima( long src, long objectMask);

	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UTSSCRIPT_H__143C5216_19D2_48EA_8B2B_95F13D5C3C12__INCLUDED_)
