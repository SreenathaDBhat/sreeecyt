////////////////////////////////////////////////////////////////////////////////////////
//
// Interface to the WOOLZ Library
//
// Written By Karl Ratcliff 22082001
// 
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//


#ifndef __WOOLZIF_H
#define __WOOLZIF_H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

#include <afxtempl.h>

/////////////////////////////////////////////////////////////////////////////
// Define the maximum number of objects allowed at any one time
/////////////////////////////////////////////////////////////////////////////

#define MAX_WOOLZ_OBJECTS               50000 // Can't make this a lot larger, or many functions' stacks will overflow!
                                              // Note that the default stack size is 1MB.
#define MAX_HOLES                       50
#define MAX_ANGS                        150
#define	ANGTHRES                        -146

////////////////////////////////////////////////////////////////////////////////////////
// Type Definitions
////////////////////////////////////////////////////////////////////////////////////////


typedef struct structMergeObj
{
	struct structMergeObj* Parent;
	struct structMergeObj* N;
	struct structMergeObj* S;
	struct structMergeObj* E;
	struct structMergeObj* W;
	struct structMergeObj* Child1;
	struct structMergeObj* Child2;
	struct structMergeObj* Next;
	BYTE		Level;
	BYTE		type;
	int			col;
	int			line;
	long		val;
}
MERGEOBJ;

// Structure holding a number of measurements

struct WoolzMeasurements
{
	long		EnclosedArea;	    // Object area (filled does not take into account holes)
    long        Area;               // Actual Area 
	long		BBLeft, BBRight, 
				BBTop, BBBottom;	// Bounding box
	double	    Perimeter;			// Perimeter length
	double		Circularity;		// A circularity measurement
	double		AxisRatio;			// An axis ratio measurement.
	double		Compactness;        // A compactness measurement
	double		COGx, COGy;
	double		MaxRadii, MinRadii; // Max and min radii
    double      Orientation;
    long        Width;              // True width and height (min width rectangle)
    long        Height; 
};

// Structure holding a point of interest

struct WoolzPoint
{
	WoolzPoint()
	{
		x = -1;
		y = -1;
		g = 0;
	}

	WoolzPoint( short xCoord, short yCoord, BYTE greyValue)
	{
		x = xCoord;
		y = yCoord;
		g = greyValue;
	}

	short x;
	short y;
	BYTE g;
};

class CUTSMemRect;
class CLabel;
class CFSConstruct;
class CSyncObjectMutex;




////////////////////////////////////////////////////////////////////////////////////////
// Class definitions
////////////////////////////////////////////////////////////////////////////////////////

class UTS_API CWoolzIP 
{
    // Structure linking two objects together - these are just pointers to objects
    typedef struct 
    {
        int     ObjI;           // Pointer to the first object
        int     ObjJ;           // Pointer to the second object 
        BOOL    Measured;
        BOOL    GMeasured;
        BOOL    MaskCreated;
    } MergeObj;


	

public:
	typedef struct 
	{
		int Xmin, Ymin, Xmax, Ymax;
	} Bounds;

    // Return values for measure merged object routine
    enum 
    {
        NotMergedObject = 1,                    // Object not a merged object
        MergedObject = 2,                       // Object is a merged object
        NullObject = 0                          // Object no longer exists
    };


	//////////////////////////////////////////////////////////////////////////
	// Functions in WoolzIF.cpp:
	
    CWoolzIP(CUTSMemRect *pMR);
    ~CWoolzIP();

    BOOL Initialise(void);
    // Kill the list of merged objects
    void KillMergedObjects(void);

    // Woolz 'Frame Store' functions (raster operations).
    int GradThresh2(BYTE *image, int cols, int lines);
    int BackgroundThreshold(BYTE *image, int cols, int lines);
	int BackgroundThreshold16(WORD *image, int cols, int lines);
    BYTE *FSMin(BYTE *image, int cols, int lines, int filtsize);
    BYTE *FSMax(BYTE *image, int cols, int lines, int filtsize);
    BYTE *FSOpen(BYTE *image, int cols, int lines, int filtsize);
    BYTE *FSClose(BYTE *image, int cols, int lines, int filtsize);
    BYTE *FSTopHat(BYTE *image, int cols, int lines, int filtsize);
	BYTE *FSExtract(BYTE *image, int cols, int lines, int filtsize, int noisefiltsize);
	long FSClahe(BYTE *image, int cols, int lines, int Min, int Max, int uiNrX, int uiNrY, int uiNrBins, int fCliplimit);
    BYTE *FSMin84(BYTE *image, int cols, int lines, int filtsize);
    BYTE *FSMax84(BYTE *image, int cols, int lines, int filtsize);
    BYTE *FSOpen84(BYTE *image, int cols, int lines, int filtsize);
    BYTE *FSClose84(BYTE *image, int cols, int lines, int filtsize);
	BYTE *FSExtract84(BYTE *image, int cols, int lines, int filtsize, int noisefiltsize);
	BYTE *FSMedian(BYTE *image, int cols, int lines, int filtsize);
	BYTE *FSGaussian(BYTE *image, int cols, int lines, float radius);
	BYTE *FSUnsharpMask(BYTE *image, int cols, int lines, float radius, float depth);
	void  FSSpotBackgroundSubtract(BYTE *image1, int width, int height, int StructSize);
	BYTE *FSSpotFind(BYTE *image1, int width, int height, int MinArea);
	BYTE *FSSpotExtract(BYTE *image1, int width, int height, int StructSize, int MinArea);

    // Pathvysion routines
    // Spot extraction probe image processing
    BYTE *PathVysion(BYTE *image, int width, int height, int SpotWidth, int ClusterWidth, int MinArea);
    // Cell extraction from tissue
    BYTE *PathOlogical(BYTE *image, int width, int height, int CellWidth);

	// Gradient thresholding routine for woolz objects
	int Objgradthresh(struct object *obj);
	
	// Draw an object in image if its grey levels exceed the threshold
	void ThreshValues(struct object *obj, BYTE *image, int image_width, int image_height, int thresh);

    // Split a mask up
    void WoolzSplitMask(long RangeImage,  long MinMask,   long MaxMask,     
					    long MinArea,     long MaxArea, 
					    long *NumInRange, long *NumInMin, long *NumInMax);
    long WoolzSplitMask(long RangeImage, long MinArea, long MaxArea);

    // Remove objects touching the edge of a mask
    long WoolzRemoveEdgeObjects(long RangeImage, long BorderWidth);

    // Determine the number of components in an image and the largest area of the component
    long WoolzNumberOfComponents(long RangeImage, long *MaxArea);

 	// Convert an image into a suitable format for Woolz and vice versa
	void UnpadImage(long SrcHandle);
	void PadImage(long SrcHandle);
	long MakePaddedImage(long SrcHandle, BYTE *ImageAddr);
	BYTE *CreateWoolzFSFromUTSImage(long srcHandle, long &width, long &height, long &srcBpp);
	long CreateUTSImageFromWoolzFS(BYTE *pFS, long width, long height, long outputBpp);
	void FreeWoolzFS(BYTE *pFS);

	// Split touching objects
	long WoolzDeagglomerate(long SrcHandle, long DstHandle);
	
	// Grow homogenous regions
	long TreeWalk(structMergeObj* seed, structMergeObj* leaf_list[], BOOL init);
	void ColourCost(structMergeObj** leaf_list, int leaf_count, long* val, long* cost);
	void ShapeMoments(structMergeObj** leaf_list, int leaf_count, double ShapeMoments[10]);
		              
	void RangeThreshold(long ImageHandle, long ImageMask, short LowThreshold, short HighThreshold);

	// Texture metaphase finder
	BYTE* TextureMetFind(BYTE *image, int width, int height, int maxFeatureWidth);

    static int curvsmooth2(struct object *histo, int iterations);


	//////////////////////////////////////////////////////////////////////////
	// Functions in WoolzIFobjectlist.cpp:
    
	void FreeObjects(void);    // Clear the current list of objects in memory
    
	void FreePool();					// Free all objects in pool and reset pool    
    
    // Add an object to the internal list.
    BOOL AddObject(BYTE *pImage, int cols, int lines, int x, int y, int threshold);

    // Object shape measurement stuff
    int FindObjects(BYTE *image, int cols, int lines, int Thresh, int MinArea);

    // Object shape measurement stuff with threshold comparison operator
	int FindGreyObjects(BYTE *image, int cols, int lines, LPCTSTR szCompare, int Thresh, int MinArea);

    // Object shape measurement stuff
    int FindObjectsEx(BYTE *image, int cols, int lines, int Thresh, int MinArea, int MaxArea, int Border);

    // Find objects and merge objects within merge object distance of one another
    int FindObjectsAndMerge(BYTE *image, int W, int H, int Thresh, int AbsMin, int MinArea, int MaxArea, int BorderWidth, int MergeDist);
    
	int	GetWoolzObjects(void **array);	// Get objects from found list
	int	SetWoolzObjects(void **array);	// Put objects back to found list    

    // Get data about a specific object
    BOOL GetObjectArea(int ObjectIndex, int *WSD);

    // Get the mass of an object = sum of its grey levels
    BOOL GetObjectMass(int ObjectIndex, int *Mass);

    // Determine the number of convex points on an object
    int NumberVertices(int objectIndex, BOOL troughs, int threshLevel);

    // Find the min width rectangle for an object + its orientation
    BOOL MinWidthRectangle(int objectIndex, float *angle, LPPOINT V1, LPPOINT V2, LPPOINT V3, LPPOINT V4);

	// Find the bounding box for an object
	void BoundingBox(int objectIndex, Bounds &Bounding);

	// Proximity of two bounding boxes
	int BoundingProximity(Bounds &B1, Bounds &B2);

    // Recreate the object in an image
    void DrawObject(int objectIndex, BYTE *image, int image_width, int image_height);

    // Fill the object with a certain value in an image
	void FillObject(int objectIndex, BYTE *image, int image_width, int image_height, int value);
	void FillObjectXY(int ObjectIndex, BYTE *image, int x, int y, int image_width, int image_height, int value);

	// Create convex hull from woolz object and return as UTS bit image
	BOOL WoolzConvexHull(long MaskOut, int objectIndex, unsigned char fill_value = 255);

	// Cluster a set of woolz objcts
	long WoolzClusterObjects(long thresh_cluster_dist);

    // Create a mask from a woolz object
    BOOL CreateMask(int objectIndex, long MaskOut);

    // Create an 8 bit binary mask from a woolz object
    BOOL CreateMaskFast(int objectIndex, long BinIm);

    // Create an 8 bit binary mask from a list of merged objects
    BOOL CreateMaskFromMerged(int objectIndex, long Pitch, long H, BYTE *Image);

    // Same but for all objects 
    BOOL CreateMaskFromAllObjects(long MaskOut);
    
    // Same once again but only for those objects with a matching label
    BOOL CreateMaskFromLabelledObjects(long Label, long MaskOut);
    
    void CreateMaskFromErodedLabelledObjects(long label, long hMask);
    
    void LabelObjectsIntersectingMask(long hMask, long label);

    // Get basic grey level info about a woolz object
	void ObjectGreyData(int ObjectIndex, long *MinGrey, long *MaxGrey, long *MeanGrey);

	// Get position of the point with the largest grey value in the object 
	void MaxGreyPositionInImage(int ObjectIndex, BYTE *pImage, long W, long H, long * pX, long * pY, long * pMaxGrey);

	// Get the local maxima in the object
	void LocalMaximaInImage(int ObjectIndex, BYTE *pImage, long W, long H, CArray<WoolzPoint> * pMaxima);

	// Find the end points of the object 
	void EndPointsInImage( int ObjectIndex, BYTE *pImage, long W, long H, CArray<WoolzPoint> * pEndPoints);

    // Get grey level stats about a woolz object
	void ObjectGreyDataStats(int ObjectIndex, long *Min, long *Max, long *Mean, long *Median, long *Mode, long *ModeCount, long *Variance, long *Skew);

    // Get basic grey level data about a woolz object from ANY image
    void ObjectGreyDataInImage(int objectIndex, BYTE *image, long W, long H, long *MinGrey, long *MaxGrey, long *MeanGrey);

    // Get basic grey level data about a merged woolz object from ANY image
    void MergedObjectGreyDataInImage(int objectIndex, BYTE *image, long W, long H, long *MinGrey, long *MaxGrey, long *MeanGrey);

    // Label an object
    void LabelObject(int objectIndex, long LabelValue);

    // Get the label for an object
    long GetObjectLabel(int objectIndex);

    // Count labelled objects only
    long CountLabelledObjects(void);

	void GetPoint(int ObjectIndex, long *X, long *Y);

	// Take various measurements on an object perimeter
	BOOL MeasureObject(int objectIndex, WoolzMeasurements *WM, int measurementSet = 1);

    // Measure merged objects found with FindObjectsAndMerge
    int MeasureMergedObject(int objectIndex, WoolzMeasurements *WM);

    // Boundary operations - returns an array of boundary points
    int MeasureBoundary(int objectIndex, long **Boundary);

	// Get the connection points for a woolz object
	int GetConnectionPoints(int ObjectIndex, double Dist, long **ConnectionPoints);

	// Get the closest object point to an object
	double GetClosestObjectPoint(int ObjectIndex, long OX, long OY, long *X, long *Y);

	// Get the connection points for a woolz object
	int GetClosestConnectionPoints(double Dist, long **ConnectionPoints);

    // Fractal boundary measurement
    double FractalMeasureBoundary(int ObjectIndex, int NSamples);

    // Get the pointer to a particular object
    void *GetObjectPtr(int objectIndex);

	// Remove an object from the list
	BOOL RemoveObject(int index);

	void Deagglomerate();

	
	void DrawIdom(int objectIndex, BYTE *image, int image_width, int image_height, int value);
	BOOL MaskFill(long R, long G, long B, long Rmask, long Gmask, long Bmask, long Rclass, long Gclass, long Bclass,
	              long RegionMask, long GatedImage, int seed_col, int seed_line, int thresh);
	              
	// Operations on objects
	void ErodeLabelledObjects(long labelNumber);
	//void DilateLabelledObjects(long labelNumber);
	
	int SplitObjects(int maxNeckWidth, /*int minCircumference,*/ int minCurvature, int minArea);
	
	BOOL Get1BitMaskVertices( BYTE * srcMaskImage, int image_width, int image_height, CArray<double> & adVertices);
	BOOL Get8BitMaskVertices( BYTE * srcMaskImage, int image_width, int image_height, CArray<double> & adVertices);

	int GetNumObjects(){ return m_nNumberObjects; }	
	
	//////////////////////////////////////////////////////////////////////////


	
private:
	struct PointInterval
	{
		PointInterval()
		{
			x = 0;
			y = 0;
			interval = 0;
		}

		PointInterval( long xValue, long yValue)
		{
			x = xValue;
			y = yValue;
			interval = 0;
		}

		long x;
		long y;
		long interval;
	};

    // Variables
    // Keep hold of the current number of objects currently found
    int     m_nNumberObjects;   

    // Various utility routines 
    static void clear_values(struct object *obj, BYTE *image, int image_width, int image_height, int value);
    static int number_of_features(struct object *obj, int troughs, int threshLevel = 5);
    static int feature_pos(struct object *obj, BOOL troughs, struct ipoint **pointlist, int **position, int *npoints);
	static void FillObject(object *pObject, BYTE *image, int image_width, int image_height, int value);
    static void WzFillObject(struct object *Obj, BYTE *image, int image_width, int image_height, int value);
	static BOOL WzFillObjectSeed(struct object *Obj, BYTE *image, int cols, int lines, int col, int line, int value);	
    static struct object *closeObj(struct object *obj, int passes);
	static object* ErodeObject(object *pOriginalObject);
    void splitObjects(BYTE *image, int width, int height, int thresh);
	//BOOL FindSplitLine(struct object *pObj, int maxNeckWidth, /*int minCircumference,*/ int minCurvature, struct ivertex *pV1, struct ivertex *pV2);
	//int RecursiveSplitObject(object *pObject, int maxNeckWidth, /*int minCircumference,*/ int minCurvature, object *splitObjectList[]);
    
    static BOOL EnsureType1Object(object *pObject);
    BOOL EnsureType1Object(int objectIndex);
	void Convert1BitImageTo8BitImage( BYTE * srcMaskImage, BYTE * maskImage, int image_width, int image_height);

    struct  object                  **m_pObjectList;            // Current list of woolz objects
    long                            *m_pLabelList;              // Labels associated with each object
    CList<MergeObj *, MergeObj *>   m_MergeObjects;             // Holds a list of connected objects
    BOOL                            *m_pMergeTable;             // Keeps track of which objects are merged and which are not merged
    BOOL                            *m_pMeasuredObject;         // Keeps track of those objects that have been measured
    BOOL                            *m_pMaskObjectCreated;      // Keeps track of those objects that have had a mask created
    BOOL                            *m_pObjectGreys;            // Keeps track of those objects that have had grey level measurement sampling

	// Woolz Object Pool
	CArray <object *, object *> m_object_pool;
	int m_pool_index;	// Maintain last index for faster searching

#if defined(WOOLZ_CLASSES)
	CLabel							*pLabel;					// Woolz stuff
	CFSConstruct					*pFSConstruct;
#endif
	CUTSMemRect						*pUTSMemRect;
	CSyncObjectMutex				*pWoolzMut;
};


#endif                  // #ifndef __WOOLZIF_H
