/////////////////////////////////////////////////////////////////////////////
//
// This is the VB OLE automation server. This is a runtime class
// and the interface to this class gets handed over to the VB
// scripting engine.
//
// Written By Karl Ratcliff 01032001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#if !defined(AFX_VBSCRIPT_H__982A6DA1_1EBC_11D5_83BF_00B0D0633F72__INCLUDED_)
#define AFX_VBSCRIPT_H__982A6DA1_1EBC_11D5_83BF_00B0D0633F72__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// VBscript.h : header file
//

#include "stdafx.h"
#include <afxtempl.h>

#define MAX_UTILITY_ARRAYS			10

class CMeasures;
class CClassMonitorDlg;
class CHRTimer;
class CSyncObjectMutex;

/////////////////////////////////////////////////////////////////////////////
// VBscript command target
// This implements our dispatch interface which is then exposed
// to the scripting engine
/////////////////////////////////////////////////////////////////////////////

class VBscript : public CCmdTarget, public ScriptObject
{
	DECLARE_DYNCREATE(VBscript)
	VBscript();           // protected constructor used by dynamic creation

// Attributes
public:
    ScriptExecMode       m_ExecutionMode;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(VBscript)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL
	virtual ~VBscript();

	void Init(CIPObjects *pPIP);
    
    // Reset the script queues etc.
    void Reset(void);

    // Initialise Qs to and from the script processor from the 
    // script side
    BOOL InitialiseScriptQs(void);
	BOOL InitialiseScriptQs(LPCTSTR QPath, LPCTSTR Qin, LPCTSTR Qout);

    // Reset the default persistent variables
    void ResetPersistentVars(void);

    // Set the window for output image display
    void SetOutputWindow(HWND hWnd);

	// Set the window up to display script progress information in
	void SetProgressWindow(CWnd *pWnd, LPRECT pRect);
	// Set the window up to display script progress information in
	void SetProgressWindow(CWnd *pWnd);
	// Set the window up to display script progress information in
	void SetProgressWindowFromHandle(HWND hWnd, LPRECT pRect);
	// Give the script processor the actual progress bar control
	void SetProgressBarCtrl(CProgressCtrl *pProg);

	// Commit persistent vars to disk
	void CommitPersistents(void);
	
	// Load persistent vars from disk
	void LoadPersistents(LPCTSTR FileName = PERSISTENTVARS_FILENAME);

    // Get an output measurement from the script
    BOOL GetMeasurement(Measurement **pMeasure);

	// Set the instance id
	void SetInstanceId(DWORD Id) { m_Id = Id; }

	// Rectangle for drawing the progress bar in
	RECT			m_rcProgressRect;

    // Persistent variable storage
    VarMapBank      *m_pPersistentVariables;

	VarMapBank		*m_pUtilityArrays[MAX_UTILITY_ARRAYS];

    // Filename for persistent variable storage
    CString         m_PersistentFileName;

    // Measurements list/Q
    CMeasures       *m_pMeasurements;

	// Parent window for any script created GUI to use
	CWnd			*m_pGUIParent;

    // Set this FALSE if you do not wish to commit persistents to disk once the script proc
    // has ended
    BOOL            m_bSavePersistents;

// Implementation
protected:
    // Our input and out queues for sending/receiving data
    VBScriptIO      m_ScriptIO;

    // Pic/window management
    CPicManager     *m_pPicMan;

    // Stores a list of CArrays for use by scripting
    CArrayList      *m_pArrays;

	// For progress display
	CProgressCtrl	*m_pProgressCtrl;

	// Determines wether progress bar deletion allowed
	BOOL			m_bHasProgress;

    // Profiling
    CHRTimer        *m_pHRT;

	// Instance Id
	DWORD			m_Id;

	// Prevent 2 or more threads simulataneously trying to run an analysis script
    CSyncObjectMutex        *m_pLockMutex;


	// Generated message map functions
	//{{AFX_MSG(VBscript)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(VBscript)
	CString m_parameterFileName;
	afx_msg void OnParameterFileNameChanged();
	afx_msg short GetScriptExecMode();
	afx_msg void SetScriptExecMode(short nNewValue);
	afx_msg void FreeAll();
	afx_msg void ShowHistogram(LPCTSTR Name, long Handle);
	afx_msg void ShowImageWindow(LPCTSTR Title, long ImageHandle);
	afx_msg void ShowString(LPCTSTR Message);
	afx_msg void HelpAbout();
	afx_msg BOOL GetLong(VARIANT FAR* L);
	afx_msg BOOL PutLong(long L);
	afx_msg long GetParameter(LPCTSTR ParamName, long Default);
	afx_msg void SetParameter(LPCTSTR ParamName, long Value);
	afx_msg BOOL PutImage(long ImageHandle);
	afx_msg BOOL GetImage(VARIANT FAR* L);
	afx_msg void Break();
	afx_msg BOOL GetDouble(VARIANT FAR* D);
	afx_msg BOOL PutDouble(double D);
    afx_msg void PutMeasurement(LPCTSTR ParamName, double Value, double NF, BOOL IncludeInClassifier, BOOL IncludeInGrid, BOOL IncludeInSort);
	afx_msg short GetExecutionMode();
	afx_msg double GetDoubleParameter(LPCTSTR ParamName, double DefaultValue);
	afx_msg BOOL GetString(VARIANT FAR* QString);
	afx_msg BOOL PutString(LPCTSTR QString);
	afx_msg BOOL RectIntersect(long pt1xl, long pt1yt, long pt1xr, long pt1yb, long pt2xl, long pt2yt, long pt2xr, long pt2yb);
	afx_msg void InitLongPersistent(LPCTSTR VarName, long VarValue);
	afx_msg void InitDoublePersistent(LPCTSTR VarName, double VarValue);
	afx_msg void InitStringPersistent(LPCTSTR VarName, LPCTSTR VarValue);
	afx_msg void SetPersistent(LPCTSTR VarName, VARIANT FAR* VarValue);
	afx_msg void GetPersistent(LPCTSTR VarName, VARIANT FAR* VarValue);
	afx_msg void DeletePersistent(LPCTSTR VarName);
	afx_msg void DebugString(LPCTSTR String);
	afx_msg void CloseImageWindow(long SrcImage);
	afx_msg void GetRGB(long ColourIn, VARIANT FAR* R, VARIANT FAR* G, VARIANT FAR* B);
	afx_msg double Atan2(double Top, double Bottom);
	afx_msg long CreateArray(long InitNumElements);
	afx_msg BOOL SortArray(long ArrayHandle, long ArrayIndex);
	afx_msg BOOL ArraySetAt(long ArrayHandle, long ArrayIndex1, long ArrayIndex2, double ArrayValue);
	afx_msg BOOL ArrayGetAt(long ArrayHandle, long ArrayIndex1, long ArrayIndex2, VARIANT FAR* Value);
	afx_msg long ArrayUBound(long ArrayHandle);
	afx_msg void ArraySort(VARIANT FAR* SafeArr, short Index);
	afx_msg void DeleteArray(long ArrayHandle);
	afx_msg long CreateArray2D(long Dimension1, long Dimension2);
	afx_msg	void SetUtilityArrayValue(long ArrayIdx, LPCTSTR Name, VARIANT FAR* VarValue);
	afx_msg	BOOL GetUtilityArrayValue(long ArrayIdx, LPCTSTR Name, VARIANT FAR* VarValue);
	afx_msg	void GetUtilityArrayValueAt(long ArrayIdx, long Index, VARIANT FAR *VarName, VARIANT FAR* VarValue);
	afx_msg	long GetUtilityArraySize(long ArrayIdx);
	afx_msg void ProgressSetRange(short MinRange, short MaxRange);
	afx_msg void ProgressSetPos(short Position);
	afx_msg void ProgressShowWindow(BOOL bShow);
	afx_msg void ProgressStep(short Increment);
	afx_msg void ProgressSetStep(short StepSize);
	afx_msg void ProgressStepIt();
	afx_msg void ScriptSleep(long MilliSecs);
    afx_msg void SetRegItem(LPCTSTR Key, LPCTSTR SubKey, LPCTSTR Item);
    afx_msg BOOL GetRegItem(LPCTSTR Key, LPCTSTR SubKey, VARIANT FAR *Item);
    afx_msg long GetProcessID(LPCTSTR ProcessName);
    afx_msg long GetCurrentProcessID();
    afx_msg BOOL BlobFileName(VARIANT FAR* BlobFileString);
    afx_msg void GUIDString(VARIANT FAR* BlobFileString);
	// Create the class monitor dialog
	afx_msg BOOL CreateClassMonitor(void);
	afx_msg void DeleteClassMonitor(void);
	
	afx_msg void StartTime();
	afx_msg double GetTime();
	afx_msg long GetInstanceId();
	afx_msg BOOL LockSection(long MilliSecs);
	afx_msg void ReleaseSection();
	afx_msg void FormatNum(LPCTSTR Format, int Number, VARIANT FAR *String);


	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
public:
	long GetAITIFF(void);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VBSCRIPT_H__982A6DA1_1EBC_11D5_83BF_00B0D0633F72__INCLUDED_)
