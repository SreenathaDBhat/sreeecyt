///////////////////////////////////////////////////////////////////////////////////////////////
// Some Morphological image analysis algorithms
// 8 bit images only
//
// Written By HB 11/30/2004
///////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __EDGEDETECTION__H
#define __EDGEDETECTION__H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif


// The EDGEIMAGE data structure
typedef struct UTS_API EdgeImage {
	int nr, nc;				// Rows and columns in the image
	int oi, oj;             // Origin
	BYTE **data;		// Pixel values
} EDGEIMAGE;

// Edge detection Class
class UTS_API CEdgeDetection {
protected:
	EDGEIMAGE *newimage (int nr, int nc);
	void freeimage (EDGEIMAGE *im);
	float **f2d (int nr, int nc);
	void free2d(float **x, int nr);

	float gauss(float x, float sigma);
	float dGauss (float x, float sigma);
	float meanGauss (float x, float sigma);
	float norm (float x, float y);

public:
	CEdgeDetection(void);
	~CEdgeDetection(void);

};

// Marr/Hildreth edge detection
class UTS_API CMarrEdgeDetector: public CEdgeDetection {
private:
	void convolution (EDGEIMAGE *im, float **mask, int nr, int nc, float **res, int NR, int NC);
	float LoG (float x, float sigma);
	int marr (float s, EDGEIMAGE *im);
	void zero_cross (float **lapim, EDGEIMAGE *im);
	float distance (float a, float b, float c, float d);

public:
	CMarrEdgeDetector(void);
	~CMarrEdgeDetector(void);

	int EdgeDetection(BYTE *image, int imwidth, int imheight, float sigma);
};

// Canny edge detection
class UTS_API CCannyEdgeDetector: public CEdgeDetection {
private:
	int WIDTH;

	int range(EDGEIMAGE *im, int i, int j);
	int trace (int i, int j, int low, EDGEIMAGE * im,EDGEIMAGE * mag, EDGEIMAGE * ori);
	void hysteresis (int high, int low, EDGEIMAGE * im, EDGEIMAGE * mag, EDGEIMAGE * oriim);
	int canny (float s, EDGEIMAGE * im, EDGEIMAGE * mag, EDGEIMAGE * ori);
	void seperable_convolution (EDGEIMAGE * im, float *gau, int width, float **smx, float **smy);
	void dxy_seperable_convolution (float** im, int nr, int nc, float *gau, int width, float **sm, int which);
	void nonmax_suppress (float **dx, float **dy, int nr, int nc, EDGEIMAGE * mag, EDGEIMAGE * ori);
	void estimate_thresh (EDGEIMAGE * mag, int *low, int *hi);

public:
	CCannyEdgeDetector(void);
	~CCannyEdgeDetector(void);

	int EdgeDetection(BYTE *image, int imwidth, int imheight, float sigma, int low, int high);
};

// Shen-Casten edge detection
class UTS_API CShenCastenEdgeDetector: public CEdgeDetection {
private:
	double b;
	double low_thresh, high_thresh;
	double ratio;
	int window_size;
	int do_hysteresis;
	float **lap;
	int nr, nc;
	EDGEIMAGE *edges;
	int thinFactor;

	int shen(EDGEIMAGE *im, EDGEIMAGE *res);
	int compute_ISEF(float **x, float **y, int nrows, int ncols);
	void apply_ISEF_vertical(float **x, float **y, float **A, float **B, int nrows, int ncols);
	void apply_ISEF_horizontal(float **x, float **y, float **A, float **B, int nrows, int ncols);
	EDGEIMAGE *compute_bli(float **buff1, float **buff2, int nrows, int ncols);
	void locate_zero_crossings(float **orig, float **smoothed, EDGEIMAGE *bli, int nrows, int ncols);
	void threshold_edges (float **in, EDGEIMAGE *out, int nrows, int ncols);
	int mark_connected (int i, int j, int level);
	int is_candidate_edge (EDGEIMAGE *buff, float **orig, int row, int col);
	float compute_adaptive_gradient (EDGEIMAGE *BLI_buffer, float **orig_buffer, int row, int col);
	void estimate_thresh (double *low, double *hi, int nr, int nc);
	EDGEIMAGE *embed(EDGEIMAGE *im, int width);
	EDGEIMAGE *debed(EDGEIMAGE *im, int width);


public:
	CShenCastenEdgeDetector(void);
	~CShenCastenEdgeDetector(void);

	int EdgeDetection(BYTE *image, int imwidth, int imheight, float sigma, int low, int high, int ws, int tf);
};

#endif
