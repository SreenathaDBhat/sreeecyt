///////////////////////////////////////////////////////////////////////////////////////////////
// Some Spatial Filter algorithms
// 8 bit images only
//
// Written By HB 10/20/2004
///////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __FILTERS_H
#define __FILTERS_H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

class UTS_API CFilters {
private:

public:
	CFilters(void);
	~CFilters(void);

	// Links edge points of a binary image into lines
	int edge_link_filter(BYTE *image, int width, int height, int connection);
	// Performs hough filter for lines
	int hough_filter(BYTE *image, int width, int height, char *degree_string, int threshold, int connection);
};

class UTS_API CHough {
private:
	BYTE *hough_transform(BYTE *image, int width, int height, char *interpret,int num);
	BYTE *hough_inverse(BYTE *image, int rows, int cols, int width, int height, int threshold);
	char *parse_string(char *input);

public:
	CHough(void);
	~CHough(void);

	int hough_filter(BYTE *image, int width, int height, char *degree_string, int threshold, int connection);
};


#endif