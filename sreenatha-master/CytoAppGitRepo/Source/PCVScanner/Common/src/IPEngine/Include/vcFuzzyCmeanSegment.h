///////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 1993 SIUE - by Gregory Hance (Southern Illinois University @ Edwardsville)
//
// Permission to use, copy, modify, and distribute this software and its
// documentation for any purpose and without fee is hereby granted, provided
// that the above copyright notice appear in all copies and that both that
// copyright notice and this permission notice appear in supporting
// documentation.  This software is provided "as is" without express or
// implied warranty.

///////////////////////////////////////////////////////////////////////////////////////////////
// Color Segmentation based on fuzzy C-mean algorithm
// 8 bit images only
// 
// Adapted and Modified By Homayoun Bagherinia 08/04/2004
///////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __FUZZYCMEANSEGMENTAT_H
#define __FUZZYCMEANSEGMENTAT_H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

typedef unsigned char MatrixPixel[3];

class UTS_API CFuzzyCmeanSegment {
private:
	struct  ImageFuzzy {
		int    size;
		int    width;
		int    height;
		int    colors;
		MatrixPixel  **matrix;
	};

	struct  Class {
		double mean[3];
		double std[3];
		int    count;
		double peak[3];
		MatrixPixel  color;
	};

	struct  _Peak {
		double low;
		double peak;
		double up;
	};

	BYTE	*imR, *imG, *imB;
	int		imWidth, imHeight;

	typedef double Histogram[3];
	int   vn[3];		// number of valleys in each space
	int   pn[3];		// number of peaks in each space
	int   max_peak;		// Total number of peaks
	int   not_count;

	typedef struct _Peak Peak[3];
	double peak[64][3];      	// at most 64 peaks in one color dimension
	double valley[64][3];
	Peak   *pk;
	Class  *klass;

	ImageFuzzy in, out;
	Histogram  *his1, *his2;
	int  assign_mean;

	// dynamic memory allocation
	MatrixPixel **create_matrix(int width, int height);
	void free_matrix(MatrixPixel **m);
	int read_ppm(ImageFuzzy *im);
	void write_ppm(ImageFuzzy *im);

	int convolution(Histogram *ser1, double	*ser2, int len1, int len2);
	double *gausian(float tore, float len);
	void count_his(ImageFuzzy *in, Histogram	*his);
	void max_his(Histogram *h, int colors);

	BYTE class_index1(BYTE c, int k);
	int class_index(MatrixPixel p);
	int coarse_seg();
	double member_ship(MatrixPixel p, Class k);
	void fine_seg();
	int find_cluster();

public:
	CFuzzyCmeanSegment();
	~CFuzzyCmeanSegment();
	int fuzzyc_segment(BYTE *R, BYTE *G, BYTE *B, int width, int height, float variance);
};

#endif
