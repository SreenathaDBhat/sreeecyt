////////////////////////////////////////////////////////////////////////////////////////
//
// An Interface to the Borland C UTS DLLs - zgeneric.dll
//
// Written By Karl Ratcliff 01/03/2001
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#ifndef __VCGENERIC_H
#define __VCGENERIC_H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

////////////////////////////////////////////////////////////////////////////////////////
// Defines
// 

// Results from HistInfo are 16 doubles long
#define GHISTINFODATALENGTH                 16

////////////////////////////////////////////////////////////////////////////////////////
// Define Pointers To Functions In DLL
// 
typedef long    (CALLBACK* LPGSMOOTHLONGS)      (long *, long, short);
typedef long    (CALLBACK* LPGHISTINFO)         (long *, long, double *);
typedef DWORD   (CALLBACK* LPRGB8TOHSI)         (long, long, long, long *, long *, long *);

////////////////////////////////////////////////////////////////////////////////////////
// Class Definition For CUTSGeneric

class UTS_API CUTSGeneric {
public:
    CUTSGeneric(); 
    ~CUTSGeneric();
    BOOL Initialise(void);
    void SmoothLongs(long *pMem, long NumElements, short NumPoints);
    long HistInfo(long *HistogramData, long NumElements, double *Info);
    DWORD RGB8toHSI(long R, long G, long B, long *H, long *S, long *I);

private:
    HMODULE             m_hGenericLib;

    LPGSMOOTHLONGS      gSmoothLongs;
    LPGHISTINFO         gHistInfo;
    LPRGB8TOHSI         gRGB8toHSI;
};

#endif
