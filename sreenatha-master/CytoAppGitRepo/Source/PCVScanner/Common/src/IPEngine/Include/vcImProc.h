////////////////////////////////////////////////////////////////////////////////
//
//	vcImProc.h
//
//	Image Processing functions (as opposed to image creation etc functions)
//	Can include all functions from the ImProc UTS DLL, and any other
//	Image Processing functions
//
//	David Burgess, Applied Imaging, October 2001
//
#ifndef __VCIMPROC_H
#define __VCIMPROC_H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

class CUTSMemRect;

class UTS_API CUTSImProc {
    
public:
    CUTSImProc(CUTSMemRect *pMR);
    ~CUTSImProc();

    // Perform a logical AND/OR/XOR on two 8 bit images
    long LogicalAND(long Image1, long Image2);
    long LogicalOR(long Image1, long Image2);
    long LogicalXOR(long Image1, long Image2);

    // Threshold an 8 bit image to produce an 8 bit binary image
    long Threshold8(long Image1, BYTE ThreshLo, BYTE ThreshHi);
	long ThresholdGT8(long Image1, BYTE Thresh);

	// Threshold an N bit image to produce an 8 bit binary image where N <= 16
    long Threshold16(long Image1, WORD ThreshLo, WORD ThreshHi);

    // Find the mean grey level in an image given an 8 bit mask
    long Mean8(long Image1, long MaskImage, long *Area);

    // Find the mode grey level in an image, below a certain MaxLevel
    long Mode8(long SrcImage, BYTE MaxLevel);

    // Threshold to produce a returned mask by comparing image and background approximation
    long BgrSubtract(long GreyImage, long BgrImage, long Threshold);

    // Subtract image1 from image2 returning image3
    long Subtract(long Image1, long Image2);

    // Do the equivalent of |a| = |b| + |c|
    long Mod8(long Image1, long Image2);

    // Produces an image where each pixel is an index to the array of images passed into it
    // the pixel is determined by the max intensity value in the images passed in
    long MaxProjectionImage(long Images[], long NImages);

    // Given an image produced by the above, pick out pixels in the image array indexed by
    // the index image
    long ProjectionImage(long IdxImage, long Images[], long NImages);

    // Divide one image by another and then multiply by a scale
    long Divide(long Image1, long Image2, BYTE Scale);

    // Flat fielding
    long FlatField(long GreyImage, long ShadeImage);

	// Fractal box count on a binrary image
	double FractalBoxCount(long Image, long Iter);

	// Sum the grey levels in an image
	long ImageSum(long Image);

	// Image registration via FFT routines
	int FFTRegisterImages(
						void *image1, void *image2,		// Supplied images
						int width, int height,			// ... and size
						int *xshift, int *yshift,		// Returned registration offsets
						int maxshift);					// Max allowed registartion shift

	// FFT of image Returned image is 32bit of double size - TBD
	int FFT(void *image, int width, int height, int *new_width, int *new_height);

	// FFT Power image. Returned image is 8 bit scaled
	int FFTPower(void *image, int width, int height, int *new_width, int *new_height);

	// Image registration by difference method
	// Only shifts in X or Y are allowed (not both!)
	// With image1 as the reference, in a rectangular grid capture, image2
	// will be N, S, E, or W of image1 as specified by the "orientation' parameter
	// stripsize limits the search area size, for this reason, offsets larger
	// than this cannot be relied on. Smaller stripsizes however will search faster.
	int RegisterImages(
					void *image1, void *image2,		// Supplied images
					int width, int height,			// ... and size
					int minshift, int maxshift,		// Search positions (from, to)
					int stripsize,					// Search area is limited to this size
					LPCTSTR orientation);			// "N", "S", "E", "W" im2 wrt im1

private:
	HMODULE	m_hImProcLib;	// DLL module handle (for zImProc.dll)

	CUTSMemRect	*pUTSMemRect;

};


#endif
