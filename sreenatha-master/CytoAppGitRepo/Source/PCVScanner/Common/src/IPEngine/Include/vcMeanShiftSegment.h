///////////////////////////////////////////////////////////////////////////////////////////////
//Color Image Segmentation
//This is the implementation of the algorithm described in 
//D. Comaniciu, P. Meer, 
//Robust Analysis of Feature Spaces: Color Image Segmentation,
//http://www.caip.rutgers.edu/~meer/RIUL/PAPERS/feature.ps.gz
//appeared in Proceedings of CVPR'97, San Juan, Puerto Rico.
//
///////////////////////////////////////////////////////////////////////////////////////////////
// Color Segmentation based on Mean Shift algorithm
// 8 bit images only
//
///////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __MEANSHIFTSEGMENTAT_H
#define __MEANSHIFTSEGMENTAT_H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

#define Max_rects 50

const int p_max = 3;	// max space dim;  MAX(p_max) == 6 !!

class UTS_API XfRaster {
 public:
    XfRaster() { };
    ~XfRaster() { };

    struct Info {
        int rows;
        int columns;
        int origin_x;
        int origin_y;
    };
};

enum DataType {	// Values of data type:
		eDATA_OCTET = 10,	// unsigned char data
		eDATA_FLOAT			// floating point data
};

typedef struct UTS_API region_str {
      int my_contor;
      int my_class;
      int my_label;
      int *my_region;
      struct region_str *next_region_str;
} REGION;

class UTS_API RasterIpChannels : public XfRaster {
 public:
    RasterIpChannels(
		const XfRaster::Info& info, const int n_channels,
		const DataType, unsigned char* data[], const BOOL
    );
    ~RasterIpChannels();

	inline unsigned char**	chdata() const { return chdata_; }
	inline unsigned char*	chdata(int i) const { return chdata_[i]; }
	inline int		n_channels() const { return n_channels_; }
    inline DataType	dtype() const { return dtype_; }
    inline void	clipf(BOOL cf) { clipf_ = cf; }
    inline BOOL	clipf() const { return clipf_; }

    void raster_info(Info& i);
    unsigned char** raster_float_to_Octet( RasterIpChannels& ras );
	void RANGE( int& a, int b, int c );

    int		rows_;
    int		columns_;
    DataType	dtype_;
    unsigned char**	chdata_;
    int		n_channels_;
    BOOL	clipf_;		// flag that determines wheter to clip or not
						// when converting from eDATA_FLOAT to unsigned char
};


class UTS_API CMeanShiftSegment {
 public:

  CMeanShiftSegment(void);
  ~CMeanShiftSegment(void);

  struct sRectangle {
    int x, y;									// upper left corner
    unsigned int width, height;					// rectangle dimensions
  };
 
  RasterIpChannels*	result_ras_;				// result of the visit to a raster

  BOOL ms_segment( RasterIpChannels*, sRectangle*, int, long);
  RasterIpChannels *read_PPM_file(BYTE *R, BYTE *G, BYTE *B, int width, int height);
  void write_PPM_file(RasterIpChannels* signal, BYTE *R, BYTE *G, BYTE *B);
  int meanshift_segment(BYTE *R, BYTE *G, BYTE *B, int width, int height);

 protected:

  int				_p;							// parameter space dimension
  int				_p_ptr;						// counter for the number of components already
												// stored in _data_all
  int				**_data_all;				// input data
  int				*_data0,*_data1,*_data2;	//to speed up
          
  int				_colms, _rows;				// width, height dimensions of the input data
  int				_ro_col;					// how many points in input data
  int				**_data;					// region of the input data currently segmented
  int				_rcolms, _rrows;			// width, height dimensions of the region
  int				_n_points;					// number of data points (per channel)
  float				_min_per;					// minimum/maximum cluster size
  int				_NJ;						// maximum number of subsamples
  BOOL				auto_segm;
  int				*gen_remain;				//the map of remaining points            
  unsigned char		*gen_class;					//labels
  unsigned char		*taken;
  int				*conn_selected;				//used by connected component
  int				n_remain;					//# of remaining points 
  int				_n_colors;					//# of colors
  int				**_col_all;					//colors in the image (LUV)
  int				*_col0, *_col1, *_col2;     //to speed up
  int				*_col_RGB;					//colors in the image (RGB)
  int				*_col_index;				//color address
  int				*_col_misc;					//misc use in histogram and conversion
  int				*_m_colors;					//how many of each color

  int				*_col_remain;				//the map of remaining colors 
  int				*_m_col_remain;				//table of remaining colors
  int				_n_col_remain;				//# of remaining colors

  void convert_RGB_LUV( RasterIpChannels* , long );
  void cut_rectangle( sRectangle* );
  void init_matr(void);
  float my_sampling( int, float T[Max_rects][p_max]);
  int  subsample( float* );
  void test_neigh(unsigned char* , int *, int*, int);
  void my_actual(unsigned char*);
  void init_neigh(void);
  void conn_comp(unsigned char *, int* , unsigned char* ,float [] [p_max],int,int);
  void find_other_neigh(int, int *, REGION *);
  void new_auto_loop(float *, unsigned char*);
  void nauto_loop(float *, int *, unsigned char *, int *); 
  void optimize(float [][p_max], int);
  void get_codeblock(float [][p_max], int);
  void get_codeblock1(float [][p_max], int);
  void new_codebook(float [][p_max], int);
  void my_clean(float [][p_max], int);
  void my_histogram(RasterIpChannels*, long);
  void eliminate_class(unsigned char *,int *,int *,int, unsigned char *, float [][p_max],REGION *);
  void eliminate_region(int *,int,float [][p_max],REGION *);
  void destroy_region_list(REGION *);
  REGION *create_region_list(int *, int);
  void initializations(RasterIpChannels*, sRectangle [], int *, long , int *);

  void covariance_w(const int N, int M, const int p, int ** data, 
                   int *w, float *T,  float C[p_max][p_max]);
  void mean_s(const int N, const int p, int J[], int **data, float T[]);
  void my_convert(int, float *, int *);
  void reverse_map(unsigned char *, unsigned char *, int *, unsigned char *, float [][p_max]);
  void set_RADIUS(int gen_gen_contor, int final);
  int test_same_cluster(int rect, float [Max_rects][p_max]);
};

#endif