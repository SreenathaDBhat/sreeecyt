///////////////////////////////////////////////////////////////////////////////////////////////
// Some Morphological image analysis algorithms
// 8 bit images only
//
// Written By HB 10/25/2004
///////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __MORPHOLOGY__H
#define __MORPHOLOGY__H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif


// The IMAGE data structure
typedef struct UTS_API MorphImage {
	int nr, nc;				// Rows and columns in the image
	int oi, oj;             // Origin
	BYTE **data;		// Pixel values
} MORPHIMAGE;

class UTS_API CMorphology {
protected:
	MORPHIMAGE *newimage (int nr, int nc);
	void freeimage (MORPHIMAGE *im);

public:
	CMorphology(void);
	~CMorphology(void);
};

class UTS_API CMorph : public CMorphology {
private:
	int range (MORPHIMAGE *im, int i, int j);

	void disk_SE(MORPHIMAGE *stru_elemP, BYTE fgnd);
	void rectangle_SE(MORPHIMAGE *stru_elemP, BYTE fgnd);

	void dil_apply (MORPHIMAGE *im, MORPHIMAGE *p, int ii, int jj, MORPHIMAGE *res);
	void bin_dilate (MORPHIMAGE *im, MORPHIMAGE *p, MORPHIMAGE *res);
	void erode_apply (MORPHIMAGE *im, MORPHIMAGE *p, int ii, int jj, MORPHIMAGE *res);
	void bin_erode (MORPHIMAGE *im, MORPHIMAGE *p, MORPHIMAGE *res);
	int bin_close (MORPHIMAGE *im, MORPHIMAGE *p, MORPHIMAGE *res);
	int bin_open (MORPHIMAGE *im, MORPHIMAGE *p, MORPHIMAGE *res);
	void gray_dilate (MORPHIMAGE *a, MORPHIMAGE *se, MORPHIMAGE *res);
	void gray_erode (MORPHIMAGE *a, MORPHIMAGE *se, MORPHIMAGE *res);
	int gray_close (MORPHIMAGE *im, MORPHIMAGE *p, MORPHIMAGE *res);
	int gray_open (MORPHIMAGE *im, MORPHIMAGE *p, MORPHIMAGE *res);

 public:
	CMorph(void);
	~CMorph(void);

	 int Morph(int op_type, int k_type, BYTE *image, int imwidth, int imheight, 
			  BYTE *se, int sewidth, int seheight);
};

class UTS_API CForceThinning : public CMorphology {
private:
	int t00, t01, t11, t01s;

	int thnz (MORPHIMAGE *im);
	int nays8 (MORPHIMAGE *im, int r, int c);
	int Connectivity (MORPHIMAGE *im, int r, int c);
	void Delete (MORPHIMAGE *im, MORPHIMAGE *tmp);
	void check (int v1, int v2, int v3);
	int edge (MORPHIMAGE *im, int r, int c);
	void stair (MORPHIMAGE *im, MORPHIMAGE *tmp, int dir);
	int Yokoi (MORPHIMAGE *im, int r, int c);
	void pre_smooth (MORPHIMAGE *im);
	void match_du (MORPHIMAGE *im, int r, int c, int k);
	void aae (MORPHIMAGE *image);
	int snays (MORPHIMAGE *im, int r, int c);

public:
	CForceThinning(void);
	~CForceThinning(void);

	 int ForceThin(BYTE *image, int imwidth, int imheight);
};

#endif
