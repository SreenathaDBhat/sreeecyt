///////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 1993 SIUE - by Kun Luo (Southern Illinois University @ Edwardsville)
//
// Permission to use, copy, modify, and distribute this software and its
// documentation for any purpose and without fee is hereby granted, provided
// that the above copyright notice appear in all copies and that both that
// copyright notice and this permission notice appear in supporting
// documentation.  This software is provided "as is" without express or
// implied warranty.

///////////////////////////////////////////////////////////////////////////////////////////////
// Color Segmentation based on SCT/Center Split algorithm
// 8 bit images only
// 
// Adapted and Modified By Homayoun Bagherinia 08/05/2004
///////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __SCTCENTERSPLITSEGMENTAT_H
#define __SCTCENTERSPLITSEGMENTAT_H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

class CUTSMemRect;

class UTS_API CSctCenterSplitSegment {
private:
	void sctMinMax(float **	svecP, unsigned int vdim,float *a_avg,
				   float *b_avg, int A_split, int B_split);
	void colorSplit(float **cvecP, BYTE *splitP_a, BYTE *splitP_b, 
					unsigned int vdim, float *a_avg, float *b_avg);
	int findStats(BYTE **cvecP, BYTE *splitP_a, BYTE *splitP_b,
				  unsigned int vdim, int A_split, int B_split,
                  BYTE **r_avg, BYTE **g_avg, BYTE **b_avg);
	void spher_xform(float **cvecP, long vdim);

	CUTSMemRect		*pUTSMemRect;

public:
	CSctCenterSplitSegment(CUTSMemRect *pMR);
	~CSctCenterSplitSegment(void);
	int sct_split_segment(BYTE *R, BYTE *G, BYTE *B, int width, int height, int A_split, int B_split);
};

#endif