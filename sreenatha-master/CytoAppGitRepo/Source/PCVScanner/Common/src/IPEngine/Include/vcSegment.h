///////////////////////////////////////////////////////////////////////////////////////////////
// Some automated segmentation algorithms
// 8 bit images only
// 
// Written By Homayoun Bagherinia 08/04/2004
///////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __SEGMENTATION_H
#define __SEGMENTATION_H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

// Some Segmentation routines

class UTS_API CSegment {
public:
	CSegment(CUTSMemRect *pMR);
	~CSegment();
	// Segments a RGB color image based on fuzzy C-mean segmentation algorithm
	long FuzzyCmeanSegment(long R, long G, long B, double variance);
	// Segments a RGB color image based on SCT/Center Split segmentation algorithm
	long SctSplitSegment(long R, long G, long B, long A_split, long B_split);
	// Segments a RGB color image based on Mean Shift segmentation algorithm
	long MeanShiftSegment(long R, long G, long B);

private:
	CUTSMemRect *pUTSMemRect;

};

#endif
