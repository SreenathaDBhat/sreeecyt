///////////////////////////////////////////////////////////////////////////////////////////////
// Some Spatial Filter Interface
// 8 bit images only
// 
// Written By HB 10/20/2004
///////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __SPATIALFILTER_H
#define __SPATIALFILTER_H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

class CUTSMemRect;

// Spatial Filter routines

class UTS_API CSpatialFilter 
{
public:
	CSpatialFilter(CUTSMemRect *pMR);
	~CSpatialFilter();

	// Links edge points of a binary image into lines
	long EdgeLink(long BinaryImage, long connection);
	// Performs a morphological operation
	long Morph(int op_type, int k_type, long Image, long Se);
	// Performs Thinning based on Zhang & Suen/Stentiford/Holt combined algorithm
	long FThin(long BinaryImage);
	// Performs Edge Detection based on Marr/Hildreth, Canny, or Shen-Casten algorithm
	long EdgeDetection(char *methode, long Image, float sigma, long low, long high);
	// Performs hough filter for lines
	long HoughFilter(long BinaryImage, char *degree_string, long minlength, long connection);

private:
	CUTSMemRect *pUTSMemRect;
};

#endif