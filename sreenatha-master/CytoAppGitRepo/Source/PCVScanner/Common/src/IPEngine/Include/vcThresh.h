///////////////////////////////////////////////////////////////////////////////////////////////
// Some more rather splendid thresholding algorithms
// To complement those of Woolz, 8 bit images only
// 
// Written By Karl Ratcliff 04102001
///////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __THRESHIMAGE_H
#define __THRESHIMAGE_H

// Some thresholding routines
class UTS_API CThreshImage 
{
public:
	CThreshImage(CUTSMemRect *pMR);
	~CThreshImage();
	int HuangThreshold( long hSrc);				// Calc threshold based on fuzzy set membership
	int EntropyThreshold(long SrcImage);		// Calc threshold based on entropies
	int MomentThreshold(long SrcImage);			// Calc threshold based on moment preservation
	int DistLineThreshold(long SrcImage);		// Calc Threshold beased on Dist to Line from Backgr Peak
	void GreyThreshold(long SrcImage, BYTE Low, BYTE High);

private:
	CUTSMemRect *pUTSMemRect;
};

#endif