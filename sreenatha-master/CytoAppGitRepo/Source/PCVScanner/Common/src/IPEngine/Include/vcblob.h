////////////////////////////////////////////////////////////////////////////////////////
//
// An Interface to the Borland C UTS DLLs - zblob.dll
//
// Written By Karl Ratcliff 01/03/2001
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#ifndef __VCBLOB_H
#define __VCBLOB_H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

////////////////////////////////////////////////////////////////////////////////////////
// Defines
//

// Maximum number of allowed blobs
#define MAX_BLOBS                       1200

// The number of longs in the VCBLOB_SPOT struct below
#define NUMSPOT_LONGS                   17

// Number of results from spLongSmoothArcs
#define SPLONGSMOOTHARCDATALENGTH       32

// Number of results from spContShapeInfo
#define SPCONTSHAPEINFODATALENGTH       10

// Number/array dimension for results returned by the MaskShapeInfo function
#define SPMASKSHAPEINFODATALENGTH       13


////////////////////////////////////////////////////////////////////////////////////////
// Typedefs Used For Parameter Passing To Functions
//
class CZMemRect;
class CZBlob;

typedef struct {
    long xMin;              // Bounding Box Coords
    long yMin;
    long xMax;
    long yMax;
    long Threshold;         // Threshold For Spot
    long Area;              // Area Of Spot
    long Intensity;         // Spot Intensity
    long MaxIntensity;      // Max Pixel Intensity In Spot
    long xRecon;            // X & Y Coordinates Of The Reconstruction ??? (Russian Terminology)
    long yRecon;
    long tag;               // User Values
    long Tag1;
    long Tag2;
    long Tag3;
    long Tag4;
    long Tag5;
    long Tag6;
} VCBLOB_SPOT;


////////////////////////////////////////////////////////////////////////////////////////
// Define Pointers To Functions In DLL
// 
typedef long (CALLBACK* LPSPFINDSPOTS)      (long, char *, long, long);
typedef long (CALLBACK* LPSPGETSPOT)        (long, BYTE *);
typedef long (CALLBACK* LPSPSETTAG)         (long, long);
typedef long (CALLBACK* LPSPDELETETAG)      (long);
typedef long (CALLBACK* LPSPSPOTSNUMBER)    (void);
typedef long (CALLBACK* LPSPACCUMLINESSEG)  (long, long, long, long, long, double, double);
typedef long (CALLBACK* LPSPLONGSMOOTHARC)  (long, long, long, long, long, long, long *, long *, long *, long);
typedef long (CALLBACK* LPSPMINSANDMAXS)    (BYTE *, long, long, long, long, long *, long);
typedef long (CALLBACK* LPSPCONTSHAPEINFO)  (void *, long, long, long, long *);
typedef long (CALLBACK* LPSPWATERSHED)      (long, long, long, long, long, long, long, long);
typedef long (CALLBACK* LPSPFINDMARKERS)    (long, long, long, long);
typedef long (CALLBACK* LPSPSPOTMASK)       (long, long);
typedef long (CALLBACK* LPSPCOUNTRANGE)     (long, long, long, long, long, double, double, long, long);
typedef long (CALLBACK* LPSPMASKSHAPEINFO)  (long, long, long, long);
typedef long (CALLBACK* LPSPARCCENTER)		(long, double, long *, long *, double *, double *, double *);

////////////////////////////////////////////////////////////////////////////////////////
// Class Definition For CUTSBlob

class UTS_API CUTSBlob {
public:
    CUTSBlob(CUTSMemRect *pMR); 
    ~CUTSBlob();

    BOOL Initialise(void);
    long FindSpots(long Handle, char *Operation, long Threshold, long MinArea);
    long GetSpot(long SpotNumber, VCBLOB_SPOT *pSpotData);
    void SetTag(long SpotNumber, long TagValue);
    long DeleteTag(long TagValue);
    long SpotsNumber(void);
    long AccumLinesSeg(long Mask, long hAcc, long HalfStepMin, long HalfStepMax, long CurvThresh, double DistMin, double DistMax);
    long LongSmoothArc(long SrcMask, long ContMask, long ArcMask, long HalfStepMin, long HalfStepMax, long CurvThresh,
                       long *Results, long *Codes, long *Curves, long CodeLim);
    long MinsAndMaxs(BYTE *data, long pnpnt, long noise, long thresh, long cycle, long *res, long reslen); 
    long ContShapeInfo(void *pCodes, long NCodes, long Xstart, long Ystart, long *Results);
    long WaterShed(long SrcImage, long MaskImage,  long Borders,    long thrmin, 
                   long areamax,  long formfactor, long formthresh, long formtype);
    long FindMarkers (long hndl, long threshold, long minarea, long maxarea);
    long SpotMask(long SrcImage, long MaskImage);
    long CountRange(long hndl, long resmask, long begmask, long minarea, long maxarea, double quantbeg, double quantfin, long maxrep, long nopen);
    long MaskShapeInfo(long SrcImage, long Handle32, long MinArea);
	long NewMaskShapeInfo(long MaskIn, long *Data, long minarea, long MaxComps);
	long FindArcCenter(long mask, double distmin, long *xc, long *yc, double *radaver, double *radmin, double *radmax);

private:
	/*
    HMODULE             m_hBlobLib;

    LPSPFINDSPOTS       spFindSpots;
    LPSPGETSPOT         spGetSpot;
    LPSPSETTAG          spSetTag;
    LPSPDELETETAG       spDeleteTag;
    LPSPSPOTSNUMBER     spSpotsNumber;
    LPSPACCUMLINESSEG   spAccumLinesSeg;
    LPSPLONGSMOOTHARC   spLongSmoothArc;
    LPSPMINSANDMAXS     spMinsAndMaxs;
    LPSPCONTSHAPEINFO   spContShapeInfo;
    LPSPWATERSHED       spWaterShed;
    LPSPFINDMARKERS     spFindMarkers;
    LPSPSPOTMASK        spSpotMask;
    LPSPCOUNTRANGE      spCountRange;
    LPSPMASKSHAPEINFO   spMaskShapeInfo;
	LPSPARCCENTER		spArcCenter;
	*/
	CZMemRect			*pZMemRect;
	CZBlob				*pBlob;

};

#endif
