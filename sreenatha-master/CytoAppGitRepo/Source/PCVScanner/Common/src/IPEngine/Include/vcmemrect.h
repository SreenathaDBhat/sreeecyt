////////////////////////////////////////////////////////////////////////////////////////
//
// An Interface to the Borland C UTS DLLs - zmemrect.dll
//
// Written By Karl Ratcliff 01/03/2001
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#ifndef __VCMEMRECT_H
#define __VCMEMRECT_H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

////////////////////////////////////////////////////////////////////////////////////////
// Defines

// Data lengths for various arrays used by the UTS
#define MRMASKLINKINFOPORTION               7
#define MRSTAT1DATALENGTH                   16
#define MRRECTMASKDATALENGTH                11
#define MRMINMAXSUMINFODATALENGTH           12
#define MRMASKCALIPERDATALENGTH             16
#define MEMRECTLABELSIZE	                16
#define MEMRECTISEXTERN	                    0x7FFFFF33	// located in Extern memory
#define MEMRECTISOVERLAY	                0x7FFFFF34	// lays over another MR

////////////////////////////////////////////////////////////////////////////////////////
// Typedefs

// Original UTS code
// Handle information struct
//typedef struct {
//   DWORD Pixelsize;             // pixel size in bits; 0 means FREE
//   DWORD OriginalPixelsize;		// original pixel size in bits, useful when camera image bitdepth is less than storage bitdepth
//								// eg for 10, 12, 14 bit images (OriginalPixelsize) the storage bitdepth is 16 bit (Pixelsize) 
//   DWORD Xext;     	            // width in pixels
//   DWORD Yext;     	            // height in pixels
//   DWORD Pitch;                 // pitch in bytes
//   DWORD Memory;                // pointer to image data;
//   					            // may be aligned on 8 byte boundary (see BaseMemory);
//   					            // for MemRectIsExtern - pointer to Extern memory
//   					            // for MemRectIsOverlay - handle of sublaying
//   DWORD Memorysize;	        // in bytes;
//   					            // values MemRectIsExtern and MemRectIsOverlay
//                                // are possible
//   DWORD Xoffset;               // horizintal offset for Overlays
//   DWORD Yoffset;               // vertical offset for Overlays
//   DWORD OwnOverList;           // head of the list of direct Overlays / 0
//   DWORD SubOverList;           // next in the list of parent's Overlays / 0
//   DWORD Tag;    		        // working var; for free entry
//   	  					        // references to next free, or equal to 0
//   DWORD BaseMemory;            // pointer from malloc(); 0 for extern/overlay rects
//   char Label[MEMRECTLABELSIZE+4];	// working short text
//} MemRectangle;

typedef enum {
  SMARTCHOICELIB = 0,
  INTELIPLIB = 1,
  AICIPLIB = 2,
  TEEMMMXLIB = 3
} BackgroundLibraries;

////////////////////////////////////////////////////////////////////////////////////////
// Define Pointers To Functions In DLL
// 
typedef long (CALLBACK* LPMRUSEINTELIPLIB) (long);
typedef long (CALLBACK* LPMRCREATEFUNC) (long, long, long);
typedef long (CALLBACK* LPMRCREATEAS) (long);
typedef long (CALLBACK* LPMRCREATEASWITHPIX) (long, long);
typedef void (CALLBACK* LPMRFREEFUNC) (long);
typedef void (CALLBACK* LPMRFREEALL) (void);
typedef long (CALLBACK* LPMRMOVE) (long, long);
typedef long (CALLBACK* LPMRMOVEEXTERN) (BYTE *, char *, long);
typedef long (CALLBACK* LPMRMOVESUBRECT) (long, long, long, long, char *);
typedef long (CALLBACK* LPMRGETRECTINFO) (long, long *, long *);
typedef long (CALLBACK* LPMRGETEXTENDEDINFO) (long, long *, long *, long *, void *Addr);
typedef long (CALLBACK* LPMRRECTTOBITMAP) (long, long);
typedef long (CALLBACK* LPMRMAKEEXTERNFUNC) (void*, long, long, long, long);
typedef long (CALLBACK* LPMRPERMUTE) (long, long, char *);
typedef long (CALLBACK* LPMRREADFILE) (char *);
typedef long (CALLBACK* LPMRHISTOGRAM) (long, void *);
typedef long (CALLBACK* LPMRHISTOGRAMMASK) (long, long, void *, long);
typedef long (CALLBACK* LPMRPROJECTION0) (long, char *);
typedef long (CALLBACK* LPMRSMOOTHBYZOOM) (long, long, long, char *, char *);
typedef long (CALLBACK* LPMRTHRESHOLD) (long, long, const char *, DWORD);
typedef long (CALLBACK* LPMRALPHABETA) (long, long, long, long, long, long, long *);
typedef long (CALLBACK* LPMRCOLORSATURTRANS) (long *, long, double, double, double, double);
typedef long (CALLBACK* LPMRCONSTPIXELWISE) (long, char *, long);
typedef long (CALLBACK* LPMRPIXELWISE) (long, char *, long);
typedef long (CALLBACK* LPMRRECONFAST) (long, long, long, long);
typedef long (CALLBACK* LPMRRECONSTRUCT) (long, long);
typedef long (CALLBACK* LPMRDISTTOMASK) (long, long *, long *);
typedef long (CALLBACK* LPMRRECTMASKDATA) (long, long, long *);
typedef long (CALLBACK* LPMRMASKLINKINFO) (long, void *, long, long);
typedef long (CALLBACK* LPMRSTAT1) (long, long *);
typedef long (CALLBACK* LPMRMASKCALIPERS) (long, long *, long *, long *, long *, long *);
typedef long (CALLBACK* LPMRMASKSUBRECTONES) (long, long, long, long, long);
typedef long (CALLBACK* LPMRMINMAXSUMINFO) (long, long *);
typedef long (CALLBACK* LPMRWRITETOFILE) (long, char *, long);
typedef long (CALLBACK* LPMRWRITETOFILETIFF) (long, char *, long);
typedef long (CALLBACK* LPMRCONVERT) (long, long, long);
typedef long (CALLBACK* LPMRBYTESTORGB) (long, long, char *, long, long, long);
typedef long (CALLBACK* LPMROPERMASK) (long, char *);
typedef long (CALLBACK* LPMRLINEENDSINMASK) (long, long *, long *, long *, long *);
typedef long (CALLBACK* LPMRTHICKPROFILE) (long, void *, long, double, double, double, double, double);
typedef long (CALLBACK* LPMRRGBTOHSI) (long, long, long, long, long, long);
typedef long (CALLBACK* LPMRHSITORGB) (long, long, long, long, long, long);
typedef long (CALLBACK* LPMRRGBTOGRAY) (long, long, long, long, long);
typedef long (CALLBACK* LPMRNUMBEROFACTIVE) (DWORD *, DWORD *);
typedef long (CALLBACK* LPMRLISTOFACTIVE) (long *, long);
typedef long (CALLBACK* LPMRNEIFUN) (long, char *, long);
typedef long (CALLBACK* LPMRPIXELWISEMASK) (long, char *, long, long);
typedef long (CALLBACK* LPMRMULDIV) (long, long, long);
typedef long (CALLBACK* LPMRGETPIXEL) (long, long, long);
typedef long (CALLBACK* LPMRBORDERS) (long, long, long);
typedef long (CALLBACK* LPMRLINKBORDERS) (long, long, char *);
typedef long (CALLBACK* LPMRMASKCONVEXHULL) (long, double);
typedef long (CALLBACK* LPMRZOOM) (long, long, char *);
typedef long (CALLBACK* LPMRDISPLAYMASK) (long, long, long, long, long, long);
typedef long (CALLBACK* LPMRMASKSPLITRANGE) (long, long, long, long, long, long *);
typedef long (CALLBACK* LPMRHANDLEINFO) (long, char *, long);
typedef long (CALLBACK* LPMRMASKLINE) (long, char *, long, long, long *);
typedef long (CALLBACK* LPMRPUTPIXEL) (long, long, long, long);
typedef void (CALLBACK* LPMRQUANTILES) (long, DWORD *, long);
typedef long (CALLBACK* LPMRINVERSEROWS) (long);
typedef long (CALLBACK* LPMRINVERSECOLUMNS) (long);
typedef long (CALLBACK* LPMRIPLCONVOLUTION) (long, long, long, long, long, long *, long);
typedef long (CALLBACK* LPMRTEXTLINE) (long, long, long, char *, long, long);
typedef long (CALLBACK* LPMRCONSTPIXELMASK) (long, char *, long, long);
typedef long (CALLBACK* LPMRCONSTPIXELNOTMASK) (long, char *, long, long);
typedef	long (CALLBACK* LPMRFINDNEXT) (long, long *, long *, long);
typedef long (CALLBACK* LPMRRECOUTCONT) (long, long, long, long, int);
typedef long (CALLBACK* LPMRRECONFASTD) (long, long, long, long, long *);
typedef long (CALLBACK* LPMRREDIMENSION) (long, long, long, long);
typedef long (CALLBACK* LPMRELLIPSE) (long, long, long, double, double, double, long);

class CZMemRect;

////////////////////////////////////////////////////////////////////////////////////////
// Class Definition For MemRect
////////////////////////////////////////////////////////////////////////////////////////

class UTS_API CUTSMemRect {
public:
	typedef enum {
		  SMARTCHOICELIB = 0,
		  INTELIPLIB = 1,
		  AICIPLIB = 2,
		  TEEMMMXLIB = 3
	} Libraries;

    CUTSMemRect();
    ~CUTSMemRect();

	CZMemRect *ZMemRect(void) { return m_pMemRect; }

    BOOL Initialise(void);
    BOOL HandleIsValid(long UTSHandle);
    long NumberUsedHandles(void);
    long HandleInfo(long Handle, char *HandleInfoStruct, long StructSize);
	long UseIntelIPlib(long command);
    long Create(long BitsPerPixel, long Xe, long Ye);
    long CreateAs(long Handle);
    long CreateAsWithPix(long Handle, long BitsPerPixel);
	long CopyAs(long Handle);
    void FreeImage(long Hndl);
    void FreeAll(void);
    BOOL SetBits(BYTE *lpBits, long Hndl);
    BOOL GetBits(BYTE *lpBits, long Hndl);
    long GetRectInfo(long Hndl, long *Xe, long *Ye);
	long GetRectOriginalBPP( long Hndl);
    long GetExtendedInfo(long Hndl, long *Xe, long *ye, long *Pitch, void *Addr);
    HBITMAP RectToBitmap(long Hndl, HWND hwnd);
    long MakeExtern(void *lpBits, long Pix, long Xe, long Ye, long Q);
    BOOL Permute(long Hndl, long GrpLen, char *Permutation);
    long Histogram(long Hndl, void *HistoData);
	long HistogramMask(long Hndl, long MaskHndl, void *HistoData, long HistoBins);
    long Move(long HandleFrom, long HandleTo);
    long MoveExtern(BYTE *pBits, char *Direction, long SrcHandle);
    long MoveSubRect(long BigHndl, long XLeft, long YTop, long SmlHndl, char *Direction);
    long Projection0(long Handle, char *Operation);
    long SmoothByZoom(long Handle, long Iterations, long ZoomVal, char *Mode, char *NeiOper);
	long Threshold(long Handle, long MaskHndl, const char *Operation, DWORD Threshold);
	long ThresholdGTE(long hSrc, long hDstMsk, long threshVal);
    long AlphaBeta(long Hndl1, long Hndl2, long Hndl3, long Alpha, long Beta, long Gamma, long *Data);
    long ColorSaturTrans(long *Hndls, long HNum, double X1, double Y1, double X2, double Y2);
    long ConstPixelwise(long HndlA, char *Operation, long HndlR);
    long Pixelwise(long HndlA, char *Operation, long HndlR);
    long ReconFast(long HndlLimit, long HndlSeed, long Xc, long Yc);
    long Reconstruct(long HndlLimit, long HndlSeed);
    long DistToMask(long SrcHndl, long *Px, long *Py);
    long RectMaskData(long SrcHndl8, long MskHndl1, long *Data);
    long MaskLinkInfo(long hMask, void *Data, long MaxComp, long MinArea);
    long Stat1(long hImage, long *Data);
    long MaskCalipers(long Hndl, long *Calipers, long *Imin, long *Imax, long *Xc, long *Yc);
    long MaskSubrectOnes(long Hndl, long Xl, long Yt, long Xe, long Ye);
    long MinMaxSumInfo(long Hndl, long *Results);
    long Convert(long SrcImage, long DestImage, long Param);
    long BytesToRGB(long Src8, long Dest24, char *Operation, long R, long G, long B);
    long OperMask(long SrcImage, char *Operation);
    long LineEndsInMask(long SrcMask, long *X1, long *Y1, long *X2, long *Y2);
    long ThickProfile(long SrcHandle, void *Points, long NPoints, double X1, double Y1, double X2, double Y2, double RectWidth);
    long Profile(long hndl, long *points, long npoints, long x1, long y1, long x2, long y2);
    long RGBToHSI(long R, long G, long B, long H, long S, long I);
    long HSIToRGB(long H, long S, long I, long R, long G, long B);
    long RGBToGray(long RGBImage, long GrayImage, long R, long G, long B);
    long NumberOfActive(DWORD *UsedMem, DWORD *AvailableRectangles);
    long ListOfActive(long *ListOfHandles, long MaxLimit);
    long NeiFun(long SrcImage, char *Oper, long Iterations);
    long PixelWiseMask(long SrcImage, char *Operation, long DestImage, long MaskImage);
	BOOL CopyUnderMask(long hSrc, long hMsk, long hDst);
    long MulDiv(long SrcImage, long Multiplier, long Divisor);
    long GetPixel(long SrcImage, long X, long Y);
    void InverseBytes(char *buffer, DWORD buffersize);
    long Borders(long SrcImage, long BordersImage, long NEI);
    long LinkBorders(long SrcImage, long BordersImages, char *NEI);
    long MaskConvexHull(long SrcImage, double Thickness);
    long Zoom(long SrcImage, long DestImage, char *Method);
    long DisplayMask (long windhndl, long hndl, long color);
    long MaskSplitRange(long RangeImage, long MinImage, long MaxImage, long MinArea, long MaxArea, long *Data);
    long MaskLine(long Mask, char *Option, long Colour, long nSects, long *Data);
    long PutPixel(long Image, long X, long Y, long Colour);
	void Quantiles(long Handle, DWORD *res, long NumOf);
	long InvertRows(long Handle);
	long InvertColumns(long Handle);
	long IplConvolution(long Handle, long xf, long yf, long xc, long yc, long *coeffs, long rshift);
    long TextLine(long hndl, long x, long y, char *txt, long height, long color);
	long ConstPixelMask(long constant, char *operation, long hndlR, long hndlM);
	long ConstPixelNotMask(long constant, char *operation, long hndlR, long hndlM);
	long FindNext(long hndl, long *px, long *py, long ZeroOrOne);
	long RecOutCont(long hndlLimit, long hndlSeed, long xc, long yc, int nei4);
	long ReconFastD(long hndlLimit, long hndlSeed, long xc, long yc, long *sizedata);
	long ReDimension(long hImage, long bpp, long w, long h);
	long Ellipse(long Hndl, long xc, long yc, double a, double b, double phi, long color);
	BOOL Convert8To1(long H8, long H1);
    BOOL Convert1To8(long Hnd1, long Hnd8);
    BOOL Convert1To8WithValue(long Hnd1, long Hnd8, BYTE value);
	BOOL Convert16To8(long hSrc, long hDst);
	BOOL Convert16To8(long hSrc, long hDst, WORD Min, WORD Max);
	BOOL Convert8To16(long hSrc, long hDst);
	BOOL ContrastStretch(long hImage);
	void CutImageFrag(long SrcImage, long CentreX, long CentreY, long DestImage, long *Xleft, long *Ytop);
	BOOL Variance( long hSrc, long hDst, BYTE radius);
	BOOL BlankBorder( long hSrc, int blankWidth);
	BOOL TrimTips( long hSrc, long hDst, int iterations);
	BOOL LocalMaxima( long hSrc, long hDst, BYTE radius);

    // Private member functions
private:
    void TableSubst(char *src, char *dest, DWORD size, void *ptable);
	void TrimTipsKernel( BYTE * pIm1, BYTE * pIm2, long srcWidth, long srcHeight, long srcPitch, long dstPitch);

private:
	CZMemRect			*m_pMemRect;
};

#endif                                      //  #ifndef __VCMEMRECT_H
