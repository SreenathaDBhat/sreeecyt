////////////////////////////////////////////////////////////////////////////////////////
//
// An Interface to some UTS drawing routines
//
// Written By Karl Ratcliff 14052001
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#ifndef __VCUTSDRAW_H
#define __VCUTSDRAW_H

#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

////////////////////////////////////////////////////////////////////////////////////////
// Defines

// Colour of graph lines, axes and text 
#define LINE_COLOUR			    RGB(0xFF, 0x00, 0x00)
#define AXIS_COLOUR             RGB(0x00, 0x00, 0x00)
#define TEXT_COLOUR             RGB(0x00, 0x00, 0x00)

// Size of ticks on axes
#define TICK_MARK_SIZE          5

// These define the % area of the window for use
// e.g. the width of the histogram is defined by:
// Window Width * PLOT_SCALE1 / SCALE_DIVISOR

#define PLOT_SCALE1                 8
#define PLOT_SCALE2                 9
#define SCALE_DIVISOR               10

// Macros
#define RECTWIDTH(lpRect)           ((lpRect)->right  - (lpRect)->left)
#define RECTHEIGHT(lpRect)          ((lpRect)->bottom - (lpRect)->top)

////////////////////////////////////////////////////////////////////////////////////////
// Typedefs

////////////////////////////////////////////////////////////////////////////////////////
// Display related routines
// Single Bit Grey Level Images
typedef struct
{
    BITMAPINFOHEADER bmiHeader;
    RGBQUAD bmiColors[2];			// Palette indices
} BITMAPINFO1BIT;

// 8 Bit Grey Level Images
typedef struct
{
    BITMAPINFOHEADER bmiHeader;
    RGBQUAD bmiColors[256];			// Palette indices
} BITMAPINFO8BIT;

// 8 Bit Grey Level Images
typedef struct
{
    BITMAPINFOHEADER bmiHeader;
    RGBQUAD bmiColors;			    // Palette indices
} BITMAPINFO16BIT, BITMAPINFO24BIT, BITMAPINFO32BIT;

class CUTSMemRect;

class UTS_API CDibInfo
{
public:
	CDibInfo();
	~CDibInfo();

    //BITMAPINFO *GetBitmapInfo(long UTSHandle);
    BITMAPINFO *GetBitmapInfo(long BPP, long Width, long Height);
    BITMAPINFO *GetBitmapPseudoInfo(long BPP, long W, long H);

private:
	BITMAPINFO1BIT      *m_pDibInfo1;
    BITMAPINFO8BIT      *m_pDibInfo8;
	BITMAPINFO16BIT     *m_pDibInfo16;
    BITMAPINFO24BIT     *m_pDibInfo24;
	BITMAPINFO32BIT     *m_pDibInfo32;

    BITMAPINFO8BIT      *m_pDibPseudo;
};




////////////////////////////////////////////////////////////////////////////////////////
// Useful UTS Drawing Routines
////////////////////////////////////////////////////////////////////////////////////////

class UTS_API CUTSDraw {
    
public:
    CUTSDraw(CUTSMemRect *pMR);
    ~CUTSDraw();

   
    void UTSHistogramWndDisplay(HWND hWnd, long Hndl);
    void GetHistogramRect(HWND hWnd, LPRECT pRect);
    void GetHistogramExtremes(long Hndl, long *HistoRangeX, long *HistoRangeY);
    void DrawProfile(HWND hWnd, long UTSHandle, long x1, long y1, long x2, long y2);
    void DrawAxes(HDC TheDC, HPEN AxesPen, int Xorg, int Yorg, int Xrange, int Xwidth, int Yrange, int Yheight, int Xticks, int Yticks); 
    
    
    // Drawing on UTS images
    void ImageTextOut(long Handle, LPCTSTR Text, LPCTSTR FontName, int x, int y, int Height, DWORD Colour);

private:

	CUTSMemRect	*pUTSMemRect;
};


#endif
