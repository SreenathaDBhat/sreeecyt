////////////////////////////////////////////////////////////////////////////////////////
//
// Useful UTS file I/O routines. Additional support for other file
// formats should be implemented in this class.
//
// Written By Karl Ratcliff 14052001
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//


#ifndef __VCUTSFILE_H
#define __VCUTSFILE_H


#ifdef UTS_EXPORTS
#define UTS_API __declspec(dllexport)
#else
#define UTS_API __declspec(dllimport)
#endif

#include <limits.h>

#define DISK_FULL               ULONG_MAX - 1

class CUTSMemRect;
class CUTSDraw;

////////////////////////////////////////////////////////////////////////////////////////
// Class definition
////////////////////////////////////////////////////////////////////////////////////////

class UTS_API CUTSFileIO 
{
public:
    CUTSFileIO(CUTSMemRect *pMR);
    ~CUTSFileIO() {};

    long LoadDIB(LPCTSTR filename, BOOL External);
    DWORD SaveDIB(LPCTSTR filename, long UTSHandle);
    BOOL SaveJPG(LPCTSTR filename, long UTSHandle, int Quality);
    long LoadJPG(LPCTSTR filename, BOOL External);
	long LoadRAW(LPCTSTR filename, long W, long H, long BPP);
	BOOL SaveRAW(LPCTSTR filename, long UTSHandle);
	BOOL SaveAs(LPCTSTR filename, long UTSHandle);
	long LoadJPGLS(LPCTSTR filename, BOOL External);
	long LoadJPGLSAs8Bit(LPCTSTR filename, BOOL External);
	BOOL SaveJPGLS(LPCTSTR filename, long UTSHandle);

private:	
	CUTSMemRect *pUTSMemRect;
};

#endif
