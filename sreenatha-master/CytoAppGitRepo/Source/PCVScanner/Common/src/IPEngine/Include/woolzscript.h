/////////////////////////////////////////////////////////////////////////////
//
// This is the Woolz specific script engine keywords class. 
// This is a runtime class
// and the interface to this class gets handed over to the VB
// scripting engine.
//
// Written By Karl Ratcliff 22082001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#if !defined(AFX_WOOLZSCRIPT_H__1C1EB773_1B2E_4C40_B61E_13A970B85C0B__INCLUDED_)
#define AFX_WOOLZSCRIPT_H__1C1EB773_1B2E_4C40_B61E_13A970B85C0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// WoolzScript.h : header file
//

#include "ScriptObjects.h"

/////////////////////////////////////////////////////////////////////////////
// WoolzScript command target

class WoolzScript : public CCmdTarget, public ScriptObject
{
	DECLARE_DYNCREATE(WoolzScript)

	WoolzScript();           // protected constructor used by dynamic creation
	virtual ~WoolzScript();

// Attributes
public:
    ScriptExecMode       m_ExecutionMode;           // Current script engine execution mode

    void                 Reset(void);               // Clear up after execution

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(WoolzScript)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

private:
	BOOL InternalWoolzMeasureObject(long objectIndex, int measurementSet, BOOL extendedMeasurements, VARIANT FAR *measurements);

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(WoolzScript)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(WoolzScript)
	afx_msg long BackgroundThreshold(long SrcImage);
	afx_msg long FSMin(long ImageHandle, long Element, short FiltSize);
	afx_msg long FSMax(long ImageHandle, long Element, short FiltSize);
	afx_msg long FSOpen(long SrcImage, long Element, short FiltSize);
	afx_msg long FSClose(long SrcImage, long Element, short FiltSize);
	afx_msg long FSTopHat(long SrcImage, short FiltSize);
	afx_msg long FSExtract(long SrcImage, long Element, short FiltSize, short NoiseFiltSize);
    afx_msg long PathVysion(long SrcImage, long SpotWidth, long ClusterWidth, long MinArea);
    afx_msg long PathOlogical(long SrcImage, long CellWidth);
	afx_msg long FSClahe(long SrcImage, int Min, int Max, int uiNrX, int uiNrY, int uiNrBins, int fCliplimit);
	afx_msg long FSMedian(long SrcImage, short FiltSize);
	afx_msg long FSGaussian(long imageHandle, float radius);
	afx_msg long FSUnsharpMask(long imageHandle, float radius, float depth);
	afx_msg long WoolzFindGreyObjects(long SrcImage, LPCTSTR szCompare, long threshold, long MinArea);
	afx_msg long WoolzFindObjects(long SrcImage, long threshold, long MinArea);
	afx_msg long WoolzFindObjectsEx(long SrcImage, short ThesholdLevel, long MinArea, long MaxArea, long BorderWidth, BOOL Flip);
	afx_msg long FindObjectsAndMerge(long SrcImage, short ThesholdLevel, long AbsMin, long MinArea, long MaxArea, long BorderWidth, long Proximity, BOOL Flip);
	afx_msg void WoolzJoinObjectEndPoints(long MaskImage, int maxDistance);
	afx_msg void WoolzJoinSameObjectEndPoints(long MaskImage, int maxDistance);
	afx_msg long WoolzArea(long objectIndex);
	afx_msg long WoolzMass(long objectIndex);
	afx_msg long NumberVertices(long objectIndex, BOOL UseTrough, short ThreshLevel);
	afx_msg void WoolzBoundingBox(long objectIndex, VARIANT FAR *pXMin, VARIANT FAR *pYMin, VARIANT FAR *pXMax, VARIANT FAR *pYMax);
	afx_msg BOOL MinWidthRectangle(long objectIndex, VARIANT FAR* Angle, VARIANT FAR* Width, VARIANT FAR* Height,VARIANT FAR* Vertices);
	afx_msg void WoolzFreeObjects();
	afx_msg void WoolzDrawObject(long SrcHandle, long objectIndex);
	afx_msg void WoolzFillObject(long SrcHandle, long objectIndex, short FillValue);
	afx_msg void WoolzFillObjectXY(long SrcHandle, long objectIndex, int x, int y, short FillValue);
	afx_msg void WoolzCreateMask(long objectIndex, long Mask);
    afx_msg void WoolzCreateMaskFast(long objectIndex, long Mask);
    afx_msg void WoolzCreateMaskFromMerged(long objectIndex, long Mask);
	afx_msg void WoolzGreyData(long objectIndex, VARIANT FAR* MInGrey, VARIANT FAR* MaxGrey, VARIANT FAR* MeanGrey);
	afx_msg void LabelWoolzObject(long objectIndex, long LabelValue);
	afx_msg long GetWoolzObjectLabel(long objectIndex);
	afx_msg long CountLabelledObjects();
	afx_msg long WoolzGetObjects(VARIANT FAR* ObjectArray);
	afx_msg long WoolzGetObjectGreyStats(VARIANT FAR* ObjectArray);
	afx_msg long WoolzGetObjectList(VARIANT FAR* ObjectList);
	afx_msg long WoolzSetObjectList(VARIANT FAR* ObjectList);
	afx_msg long FSSpotExtract(long SrcImage, short StructSize, long MinArea);
	afx_msg long FSSpotFind(long hSrcImage, long minArea);
	afx_msg BOOL WoolzMeasureObject(long objectIndex, VARIANT FAR *measurements);
	afx_msg BOOL WoolzMeasureObject2(long objectIndex, VARIANT FAR *measurements);	
	afx_msg BOOL WoolzMeasureObjectEx(long objectIndex, VARIANT FAR* Measurements);
    afx_msg BOOL WoolzMeasureMergedObject(long objectIndex, VARIANT FAR* Measurements); 
	afx_msg void WoolzSplitMask(long RangeImage, long MinMask, long MaxMask, long MinArea, long MaxArea,
	                            VARIANT FAR* NumInRange, VARIANT FAR* NumInMin, VARIANT FAR* NumInMax);
	afx_msg long WoolzSplitMaskFast(long RangeImage, long MinArea, long MaxArea);
    afx_msg long WoolzRemoveEdgeObjects(long RangeImage, long BorderWidth);
	afx_msg long WoolzNumberOfComponents(long SrcImage, VARIANT FAR* MaxArea);
	afx_msg void WoolzMaskFromAllObjects(long MaskOut);
	afx_msg void WoolzMaskFromLabelledObjects(long Label, long MaskOut);
	afx_msg void WoolzMaskFromErodedLabelledObjects(long label, long hMask);
    afx_msg void WoolzLabelObjectsIntersectingMask(long hMask, long label);	
	afx_msg void ObjectGreysInImage(long objectIndex, long SrcImage, VARIANT FAR* MinGrey, VARIANT FAR* MaxGrey, VARIANT FAR* MeanGrey);
	afx_msg void MergedObjectGreysInImage(long objectIndex, long SrcImage, VARIANT FAR* MinGrey, VARIANT FAR* MaxGrey, VARIANT FAR* MeanGrey);
	afx_msg long GetBoundary(long objectIndex, VARIANT FAR* BoundaryArray);
	afx_msg long GetConnectionPoints(long objectIndex, double Dist, VARIANT FAR* BoundaryArray);
	afx_msg long GetClosestConnectionPoints(double Dist, VARIANT FAR* BoundaryArray) ;
    afx_msg double FractalBoundary(short ObjectIndex, short Spacing);
	afx_msg long TextureMetFind(long SrcHandle, long MaxFeatureWidth);
	afx_msg long WoolzDeagglomerate(long SrcHandle, long DstHandle);
	afx_msg void WoolzDeagglomerateObjects();
	afx_msg long WoolzSplitObjects(long maxNeckWidthSplit, long minCurvatureSplit, long minArea);
	afx_msg long WoolzConvexHull(long mask_image_out, short object_number, short fill_value);
	afx_msg long WoolzClusterObjects(long thresh_cluster_dist);
	afx_msg void WoolzRemoveObject(long index);
	afx_msg long WoolzGetNumberOfObjects();	
	afx_msg void WoolzErodeLabelledObjects(long labelNumber);
	//afx_msg void WoolzDilateLabelledObjects(long labelNumber);	

//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WOOLZSCRIPT_H__1C1EB773_1B2E_4C40_B61E_13A970B85C0B__INCLUDED_)
