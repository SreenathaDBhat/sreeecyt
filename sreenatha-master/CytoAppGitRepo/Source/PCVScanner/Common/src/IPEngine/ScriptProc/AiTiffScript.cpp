// AiTiffScript.cpp : implementation file
//
//	10Oct01	WH	Original

#include "stdafx.h"
#include "resource.h"
#include "CommonScript.h"
#include "AiTiffScript.h"
#include "uts.h"

#ifndef _UNICODE
#include "AITiff.h"		
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AiTiffScript

IMPLEMENT_DYNCREATE(AiTiffScript, CCmdTarget)

AiTiffScript::AiTiffScript()
{
	EnableAutomation();
}

AiTiffScript::~AiTiffScript()
{
}


void AiTiffScript::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}


BEGIN_MESSAGE_MAP(AiTiffScript, CCmdTarget)
	//{{AFX_MSG_MAP(AiTiffScript)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(AiTiffScript, CCmdTarget)
	//{{AFX_DISPATCH_MAP(AiTiffScript)
	DISP_FUNCTION(AiTiffScript, "CreateAITIFF", CreateAITIFF, VT_I4, VTS_NONE)
	DISP_FUNCTION(AiTiffScript, "LoadAITIFF", LoadAITIFF, VT_BOOL, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(AiTiffScript, "FreeAITIFF", FreeAITIFF, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(AiTiffScript, "CreateAllMIPMapsAITIFF", CreateAllMIPMapsAITIFF, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(AiTiffScript, "SaveAITIFF", SaveAITIFF, VT_BOOL, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(AiTiffScript, "Add8bppCmpAITIFF", Add8bppCmpAITIFF, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(AiTiffScript, "AssimilateAITIFF", AssimilateAITIFF, VT_BOOL, VTS_I4 VTS_I2)
	DISP_FUNCTION(AiTiffScript, "GetCmpAITIFF", GetCmpAITIFF, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(AiTiffScript, "CountComponentsAITIFF", CountComponentsAITIFF, VT_I4, VTS_I4)
	DISP_FUNCTION(AiTiffScript, "GetCmpTextAITIFF", GetCmpTextAITIFF, VT_BOOL, VTS_I4 VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(AiTiffScript, "SetCmpTextAITIFF", SetCmpTextAITIFF, VT_BOOL, VTS_I4 VTS_I4 VTS_BSTR)
	DISP_FUNCTION(AiTiffScript, "GetCmpColourAITIFF", GetCmpColourAITIFF, VT_BOOL, VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(AiTiffScript, "SetCmpColourAITIFF", SetCmpColourAITIFF, VT_BOOL, VTS_I4 VTS_I4 VTS_I2 VTS_I2 VTS_I2)
	DISP_FUNCTION(AiTiffScript, "GetCmpVisibilityAITIFF", GetCmpVisibilityAITIFF, VT_BOOL, VTS_I4 VTS_I4)
	DISP_FUNCTION(AiTiffScript, "SetCmpVisibilityAITIFF", SetCmpVisibilityAITIFF, VT_BOOL, VTS_I4 VTS_I4 VTS_BOOL)
	DISP_FUNCTION(AiTiffScript, "GetCmpZStackAITIFF", GetCmpZStackAITIFF, VT_I4, VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(AiTiffScript, "SetCmpZStackAITIFF", SetCmpZStackAITIFF, VT_BOOL, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(AiTiffScript, "GetRenderedAITIFF", GetRenderedAITIFF, VT_I4, VTS_I4)
	DISP_FUNCTION(AiTiffScript, "Add1bppCmpAITIFF", Add1bppCmpAITIFF, VT_I4, VTS_I4 VTS_I4)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IAiTiffScript to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {4B617FA3-6FFE-4FEF-86B4-43B0B37A6924}
static const IID IID_IAiTiffScript =
{ 0x4b617fa3, 0x6ffe, 0x4fef, { 0x86, 0xb4, 0x43, 0xb0, 0xb3, 0x7a, 0x69, 0x24 } };

BEGIN_INTERFACE_MAP(AiTiffScript, CCmdTarget)
	INTERFACE_PART(AiTiffScript, IID_IAiTiffScript, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Class members not exposed to the scripting engine implemented here
/////////////////////////////////////////////////////////////////////////////
void AiTiffScript::ValidHandle(long hnd)
{
#ifndef _UNICODE
	if (!AfxIsValidAddress((const void *)hnd, sizeof(AI_Tiff), TRUE))
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // A handle is invalid
#endif
}

/////////////////////////////////////////////////////////////////////////////
// Handle the pitch of the image to make it contigious
// 
/////////////////////////////////////////////////////////////////////////////
BYTE * AiTiffScript::MakeContigious(BYTE *datain, long w, long h, int bpp, int pitch)
{
#ifndef _UNICODE

	BYTE *bRet = NULL;

	long byteWidth = TiffComponent::WidthBytes(w, bpp);//(((w * bpp) + 7) >> 3); 
	long size = h * byteWidth;
	bRet = new BYTE[size];

	BYTE *in = datain;
	BYTE *out = bRet;
	for (int i = 0; i < h; i++)
	{
		memcpy(out, in, byteWidth);
		out += byteWidth;
		in += pitch;
	}
	
	return bRet;
#else
	return NULL;
#endif
}

/////////////////////////////////////////////////////////////////////////////
// AiTiffScript message handlers

/////////////////////////////////////////////////////////////////////////////
// Creates a new AI_Tiff class on the heap, returns the pointer to this class
// as a long. This long can be used as a handle (derefernced back to a AI_Tiff)
// 
// VB script syntax:
//      CreateAITIFF()
// Parameters:
// Returns:
//      non 0 long value (handle) if successful otherwise 0
/////////////////////////////////////////////////////////////////////////////
long AiTiffScript::CreateAITIFF() 
{
#ifndef _UNICODE

	AI_Tiff * pTiff = new AI_Tiff;
	if (!pTiff)
        AfxThrowOleDispatchException(0xFF, IDS_MEMALLOC, 0);       // Can't allocate new class
	else
		return (long)pTiff;
#endif

	return 0;
}
/////////////////////////////////////////////////////////////////////////////
// Load a Tiff file into an existing aitiff object specified by hnd
// 
// VB script syntax:
//      LoadAITIFF(hnd, filename)
// Parameters:
//		hnd - handle returned by CreateAITIF
//		filename - path to tiff file to load
//
// Returns:
//      TRUE if successful otherwise FALSE
/////////////////////////////////////////////////////////////////////////////
BOOL AiTiffScript::LoadAITIFF(long hnd, LPCTSTR filename) 
{
#ifndef _UNICODE
	ValidHandle(hnd);

	return((AI_Tiff *)hnd)->LoadFile(filename);
#else
	return FALSE;
#endif
}
/////////////////////////////////////////////////////////////////////////////
// Free aitiff object specified by hnd
// 
// VB script syntax:
//      FreeAITIFF(hnd, filename)
// Parameters:
//		hnd - handle returned by CreateAITIF
//
// Returns:
/////////////////////////////////////////////////////////////////////////////
void AiTiffScript::FreeAITIFF(long hnd) 
{

	ValidHandle(hnd);

	AI_Tiff * p = (AI_Tiff*)hnd;
	delete p;
}
/////////////////////////////////////////////////////////////////////////////
// Creates MIP maps for all components in the aitiff object specified by hnd
// 
// VB script syntax:
//      CreateAllMIPMapsAITIFF(hnd)
// Parameters:
//		hnd - handle returned by CreateAITIF
/////////////////////////////////////////////////////////////////////////////
void AiTiffScript::CreateAllMIPMapsAITIFF(long hnd) 
{
	ValidHandle(hnd);

	((AI_Tiff *)hnd)->CreateMIPMapsForAllComponents();
}

/////////////////////////////////////////////////////////////////////////////
// Saves the aitiff object specified by hnd as a file
// 
// VB script syntax:
//      SaveAITIFF(hnd, filename)
// Parameters:
//		hnd - handle returned by CreateAITIF
//		filename - path to create tiff file
//
// Returns:
//      TRUE if successful otherwise FALSE
/////////////////////////////////////////////////////////////////////////////
BOOL AiTiffScript::SaveAITIFF(long hnd, LPCTSTR filename) 
{
#ifndef _UNICODE
	ValidHandle(hnd);

	return ((AI_Tiff *)hnd)->SaveFile(filename);
#else
	return FALSE;
#endif
}
/////////////////////////////////////////////////////////////////////////////
// Add an 8 bpp image component to the tiff, represented by the UTS memrect 
// handle srcimage.
// 
// VB script syntax:
//      Add8bppCmpAITIFF(hnd, srcimage)
// Parameters:
//		hnd - handle returned by CreateAITIF
//		srcimage - handle to a UTS memrect deining the image data to add
//
// Returns:
//      -1 on error other wise the 0 based index of the component
/////////////////////////////////////////////////////////////////////////////
long AiTiffScript::Add8bppCmpAITIFF(long hnd, long srcimage) 
{
#ifndef _UNICODE
	ValidHandle(hnd);

	long bpp, x, y, p;
	long TiffHandle = -1;
	BYTE *addr;
	BYTE *image;

	// Extract info from uts image
	if (srcimage > 0)
	{
        // Get The Bits Per Pixel For The Image
        bpp = IP().UTSMemRect()->GetExtendedInfo(srcimage, &x, &y, &p, &addr);

        // Only support 8 bits per pixel
        if (bpp != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);    // Invalid parameter type
		else
		{
			image = (BYTE *) MakeContigious(addr, x, y, bpp, p);
			if (image) 
			{
				TiffHandle = ((AI_Tiff *)hnd)->Add8bppComponent((unsigned char*)image, x, y);
				delete image;
			}
			else
				AfxThrowOleDispatchException(0xFF, IDS_MEMALLOC, 0);       // Can't allocate new class
		}
	}
	else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

	return TiffHandle;
#else
	return UTS_ERROR;
#endif
}
/////////////////////////////////////////////////////////////////////////////
// Cuts and pastes the components from oldtiff to newtiff
// 
// VB script syntax:
//      AssimilateAITIFF(newtiff, oldtiff)
// Parameters:
//		newtiff - handle returned by CreateAITIF (will end up with oldtiff components)
//		oldtiff - handle returned by CreateAITIF (will end up with no components)
//
// Returns:
//      TRUE if successful otherwise FALSE
/////////////////////////////////////////////////////////////////////////////
BOOL AiTiffScript::AssimilateAITIFF(long newtiff, short oldtiff) 
{
#ifndef _UNICODE
	ValidHandle(newtiff);
	ValidHandle(oldtiff);
	
	return ((AI_Tiff *)newtiff)->Assimilate((AI_Tiff *)oldtiff);
#else
	return FALSE;
#endif
}
/////////////////////////////////////////////////////////////////////////////
// Extracts the image data as an UTS MEMRECT and return the handle to it
// 
// VB script syntax:
//      GetCmpAITIFF(hnd, index)
// Parameters:
//		hnd - handle returned by CreateAITIF
//		index - 0 based component index
//
// Returns:
//      a valid UTS handle if successful otherwise 0
/////////////////////////////////////////////////////////////////////////////
long AiTiffScript::GetCmpAITIFF(long hnd, long index) 
{
#ifndef _UNICODE
	ValidHandle(hnd);

	AI_Tiff * p = (AI_Tiff*)hnd;

	// No component
	if (!p->GetComponent(index))
		return 0;

	// Get dimensions, depth and ptr of image data
	int w = p->GetComponentWidth(index);
	int h = p->GetComponentHeight(index);
	int bpp = p->GetComponentBPP(index);

	BYTE * pImage = NULL;

	if (!(pImage = (BYTE *)p->GetComponentBytes(index)))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);				// Invalid image data

	// Okay now create a UTS memrect and fill it in
	DWORD *addr;
	long utsbpp, W, H, P;
    long pixels_size = h * ((((w * bpp) + 31) & ~31) >> 3);


    // Allocate a UTS handle
    long UTSHandle = IP().UTSMemRect()->Create(bpp, w, h);

	// and splat the image data in (may have to do some thing different if a 1 bit mask image)
    if (UTSVALID_HANDLE(UTSHandle))
    {
	    // Get the address of the allocated UTS memory
		utsbpp = IP().UTSMemRect()->GetExtendedInfo(UTSHandle, &W, &H, &P, &addr);
		memcpy(addr, pImage, pixels_size);
	}

	return UTSHandle;
#else
	return UTS_ERROR;
#endif
}
/////////////////////////////////////////////////////////////////////////////
long AiTiffScript::CountComponentsAITIFF(long hnd)
{
#ifndef _UNICODE
	return ((AI_Tiff *)hnd)->CountComponents();
#else
	return FALSE;
#endif
}
/////////////////////////////////////////////////////////////////////////////
// Extracts the description text from the component
// 
// VB script syntax:
//      GetCmpTextAITIFF(hnd, index, text)
// Parameters:
//		hnd - handle returned by CreateAITIF
//		index - 0 based component index
//		text - string into which to copy the component text
//
// Returns:
//      TRUE if successful otherwise FALSE
/////////////////////////////////////////////////////////////////////////////
BOOL AiTiffScript::GetCmpTextAITIFF(long hnd, long index, VARIANT FAR* text)
{
#ifndef _UNICODE
    // Check we have a BSTR/EMPTY coming in
    if (text->vt != VT_BSTR && text->vt != VT_EMPTY)
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);
    
	ValidHandle(hnd);
	char buf[255];

	if (((AI_Tiff *)hnd)->GetComponentDescription(index, buf, sizeof(buf)))
	{
		CString str = buf;

		// Free up the existing string if there was one
        if (text->vt == VT_BSTR) 
			SysFreeString(text->bstrVal);

		// Change the type to a BSTR
        V_VT(text) = VT_BSTR;
        // Allocate the string and return to VBS
        text->bstrVal = str.AllocSysString();
		return TRUE;
	}
#endif
	return FALSE;
}
/////////////////////////////////////////////////////////////////////////////
// Associate a text description with an existing image component
// 
// VB script syntax:
//      SetCmpTextAITIFF(hnd, srcimage)
// Parameters:
//		hnd - handle returned by CreateAITIF
//		index - 0 based component index with which to associate text
//		text - text string
//
// Returns:
//      TRUE if successful otherwise FALSE
/////////////////////////////////////////////////////////////////////////////
BOOL AiTiffScript::SetCmpTextAITIFF(long hnd, long index, LPCTSTR text) 
{
#ifndef _UNICODE
	ValidHandle(hnd);

	return ((AI_Tiff *)hnd)->SetComponentDescription(index, (char *)text);
#else
	return FALSE;
#endif
}
/////////////////////////////////////////////////////////////////////////////
// Extracts the rgb colour planes from the component
// 
// VB script syntax:
//      GetCmpColourAITIFF(hnd, index, R, G, B)
// Parameters:
//		hnd - handle returned by CreateAITIF
//		index - 0 based component index
//		R	- Red plane (0-255)
//		G	- Green plane (0-255)
//		B	- Blue plane (0-255)
//
// Returns:
//      TRUE if successful otherwise FALSE
/////////////////////////////////////////////////////////////////////////////
BOOL AiTiffScript::GetCmpColourAITIFF(long hnd, long index, VARIANT FAR* R, VARIANT FAR* G, VARIANT FAR* B) 
{
#ifndef _UNICODE
	ValidHandle(hnd);

	rgbTriple rgb;

	if (((AI_Tiff *)hnd)->GetComponentColour(index, &rgb))
	{
		// Set the type for the variant and assign value
		V_VT(R) = VT_I4;
		V_VT(G) = VT_I4;
		V_VT(B) = VT_I4;
		R->lVal = rgb.r;
		G->lVal = rgb.g;
		B->lVal = rgb.b;
		return TRUE;
	}
#endif
	return FALSE;
}
/////////////////////////////////////////////////////////////////////////////
// Associate an colour RGB value with an image component
// 
// VB script syntax:
//      SetCmpColourAITIFF(hnd, srcimage)
// Parameters:
//		hnd - handle returned by CreateAITIF
//		index - 0 based component index with which to associate colour
//		R	- Red plane (0-255)
//		G	- Green plane (0-255)
//		B	- Blue plane (0-255)
//
// Returns:
//      TRUE if successful otherwise FALSE
/////////////////////////////////////////////////////////////////////////////
BOOL AiTiffScript::SetCmpColourAITIFF(long hnd, long index, short R, short G, short B) 
{
#ifndef _UNICODE
	ValidHandle(hnd);

	rgbTriple rgb = {(BYTE)R,(BYTE)G,(BYTE)B};	

	return ((AI_Tiff *)hnd)->SetComponentColour(index, rgb);
#else
	return FALSE;
#endif
}
/////////////////////////////////////////////////////////////////////////////
BOOL AiTiffScript::GetCmpVisibilityAITIFF(long hnd, long index)
{
#ifndef _UNICODE
	return (((AI_Tiff *)hnd)->IsComponentVisible(index));
#else
	return FALSE;
#endif
}
BOOL AiTiffScript::SetCmpVisibilityAITIFF(long hnd, long index, BOOL visible)
{
#ifndef _UNICODE
	return ((AI_Tiff *)hnd)->SetComponentVisible(index, visible);
#else
	return FALSE;
#endif
}
/////////////////////////////////////////////////////////////////////////////
// Extracts any zstack components assocaited with an aitiff componet as a 
// handle to another aitiff object containing the focus planes as components
// 
// VB script syntax:
//      GetCmpZStackAITIFF(hnd, index, spacing, objsize)
// Parameters:
//		hnd - handle returned by CreateAITIF
//		index - 0 based component index
//		spacing - focus distance between planes
//		obsize - min size of objects of interest
//
// Returns:
//      non 0 long value (handle to new tiff) if successful otherwise 0
// Notes:
//		Must call FreeAITIFF on this handle when finished	
/////////////////////////////////////////////////////////////////////////////
long AiTiffScript::GetCmpZStackAITIFF(long hnd, long index, VARIANT FAR* spacing, VARIANT FAR* objsize) 
{
#ifndef _UNICODE

	ValidHandle(hnd);

	AI_Tiff *p = new AI_Tiff;

	if (!p)
        AfxThrowOleDispatchException(0xFF, IDS_MEMALLOC, 0);       // Can't allocate new class

	int sp, ob;
	if (((AI_Tiff *)hnd)->GetComponentZStack(index, p, &sp, &ob))
	{
		V_VT(spacing) = VT_I4;
		V_VT(objsize) = VT_I4;
		spacing->lVal = sp;		
		objsize->lVal = ob;		
		return (long)p;
	}
#endif

	return 0;
}
/////////////////////////////////////////////////////////////////////////////
// Sets an AI_tiff object to be the Zstack (contains focus planes as components)
// for a component in a ai_tiff object
// 
// VB script syntax:
//      SetCmpZStackAITIFF(hnd, index, hnd2, spacing, objsize)
// Parameters:
//		hnd - handle returned by CreateAITIF
//		index - 0 based component index
//		hand2 - handle of AI_Tiff object that contains the focus planes
//		spacing - focus distance between planes
//		obsize - min size of objects of interest
//
// Returns:
//      TRUE if successful otherwise FALSE
/////////////////////////////////////////////////////////////////////////////
BOOL AiTiffScript::SetCmpZStackAITIFF(long hnd, long index, long hnd2, long spacing, long objsize) 
{
#ifndef _UNICODE
	ValidHandle(hnd);
	ValidHandle(hnd2);

	AI_Tiff * cmps = (AI_Tiff *)hnd, *zstack= (AI_Tiff *)hnd2;

	return (cmps->SetComponentZStack(index, zstack, spacing, objsize));
#else
	return FALSE;
#endif
}
/////////////////////////////////////////////////////////////////////////////
// Renders all the visible components into the AI_tiif renderbuff which then
// is converted into a 24 bit UTS memrect which is returned
// 
// VB script syntax:
//      GetRenderedAITIFF(hnd)
// Parameters:
//		hnd - handle returned by CreateAITIF
//
// Returns:
//      a valid UTS handle if successful otherwise 0
/////////////////////////////////////////////////////////////////////////////
long AiTiffScript::GetRenderedAITIFF(long hnd) 
{
#ifndef _UNICODE
	ValidHandle(hnd);

	AI_Tiff * p = (AI_Tiff*)hnd;

	p->Render();

	// Get dimensions, depth and ptr of image data
	int w = p->GetRenderBufferWidth();
	int h = p->GetRenderBufferHeight();
	int bpp = 24;

	BYTE * pImage = NULL;

	if (!(pImage = (BYTE *)p->GetRenderBuffer()))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);				// Invalid image data

	// Okay now create a UTS memrect and fill it in
	DWORD *addr;
	long utsbpp, W, H, P;
    long pixels_size = h * ((((w * bpp) + 31) & ~31) >> 3);


    // Allocate a UTS handle
    long UTSHandle = IP().UTSMemRect()->Create(bpp, w, h);

	// and splat the image data in (may have to do some thing different if a 1 bit mask image)
    if (UTSVALID_HANDLE(UTSHandle))
    {
	    // Get the address of the allocated UTS memory
		utsbpp = IP().UTSMemRect()->GetExtendedInfo(UTSHandle, &W, &H, &P, &addr);
		memcpy(addr, pImage, pixels_size);
	}

	return UTSHandle;
#else
	return UTS_ERROR;
#endif
}
/////////////////////////////////////////////////////////////////////////////
// Add an 1bpp image component to the tiff, represented by the UTS memrect 
// handle srcimage.
// 
// VB script syntax:
//      Add1bppCmpAITIFF(hnd, srcimage)
// Parameters:
//		hnd - handle returned by CreateAITIF
//		srcimage - handle to a UTS memrect defining the image data (must be 1 bit)
//
// Returns:
//      -1 on error other wise the 0 based index of the component
/////////////////////////////////////////////////////////////////////////////
long AiTiffScript::Add1bppCmpAITIFF(long hnd, long srcimage) 
{
#ifndef _UNICODE
	ValidHandle(hnd);

	long bpp, x, y, p;
	DWORD *addr;
	long bRet = -1;

	// Extract info from uts image
	if (srcimage > 0)
	{
        // Get The Bits Per Pixel For The Image
        bpp = IP().UTSMemRect()->GetExtendedInfo(srcimage, &x, &y, &p, &addr);

        // Only support 1 bit per pixel
        if (bpp != ONE_BIT_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);    // Invalid parameter type

		// No padding is expected for aitiff functions
		BYTE *image = MakeContigious((BYTE *)addr, x, y, bpp, p);
		if (image)
		{
			// Also 1 bit UTS images are have byte seq. wrong way round
			IP().UTSMemRect()->InverseBytes((char *) image, y *(((x * bpp) + 7) >> 3));
			bRet = ((AI_Tiff *)hnd)->Add1bppComponent((unsigned char*)image, x, y);
			delete image;
		}
		else
			AfxThrowOleDispatchException(0xFF, IDS_MEMALLOC, 0);       // Can't allocate new class
	}
	else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

	return bRet;
#else
	return UTS_ERROR;
#endif
}
