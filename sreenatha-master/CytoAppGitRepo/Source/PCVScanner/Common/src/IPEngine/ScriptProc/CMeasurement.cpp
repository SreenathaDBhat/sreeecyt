//////////////////////////////////////////////////////////////////////////
// Generic measurement class and measurement list class
// NOTE although this is a list class it is implemented as
// a queue where one end adds to the measurements the other
// gets them. Measurements are deleted when they have been got
//////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "cmeasurement.h"

#define VERSION_NUMBER		0x1

//////////////////////////////////////////////////////////////////////////
// Generic measurement class and measurement list class
//////////////////////////////////////////////////////////////////////////

Measurement::Measurement()
{
	m_Flags.FlagValue = 0;
	m_Flags.Flags.IncludeInClassifier = 1;
	m_Flags.Flags.IncludeInGrid = 1;
	m_Flags.Flags.IncludeInSort = 1;
}

Measurement::~Measurement()
{
}

// Duplicate this measurement
Measurement *Measurement::Duplicate(void)
{
    Measurement *pNew;

    pNew = new Measurement();
	pNew->Init(m_Name, m_Value, m_NormalisationFactor, m_Flags.Flags.IncludeInClassifier, m_Flags.Flags.IncludeInGrid, m_Flags.Flags.IncludeInSort);

    return pNew;
}

void Measurement::Init(CString Name, COleVariant Value, double NormaliseFactor, int IncludeInClassifier, int IncludeInGrid, int IncludeInSort)
{
    m_Value = Value;
    m_Name = Name;
    m_NormalisationFactor = NormaliseFactor;
	m_Flags.Flags.IncludeInClassifier = IncludeInClassifier;
	m_Flags.Flags.IncludeInGrid = IncludeInGrid;
	m_Flags.Flags.IncludeInSort = IncludeInSort;
}

//////////////////////////////////////////////////////////////////////////
// Return the normalised measurement
//////////////////////////////////////////////////////////////////////////

double Measurement::Normalise()                    
{
    double V;

    switch (m_Value.vt)
    {
    case VT_I4:
        V = (double) m_Value.lVal;
        break;
    case VT_R8:
        V = m_Value.dblVal;
        break;
    default:
        V = 0.0;
    }

    return V / m_NormalisationFactor;
}

double Measurement::GetValue(void)
{
    double V;

    switch (m_Value.vt)
    {
    case VT_I4:
        V = (double) m_Value.lVal;
        break;
    case VT_R8:
        V = m_Value.dblVal;
        break;
    default:
        V = 0.0;
    }

    return V;
}

//////////////////////////////////////////////////////////////////////////
// File I/O
//////////////////////////////////////////////////////////////////////////

BOOL Measurement::Serialize(CArchive &ar)
{
    if (ar.IsStoring())
    {
        ar << m_Name;
        ar << m_NormalisationFactor;
        ar << m_Value;
    }
    else
    {
        ar >> m_Name;
        ar >> m_NormalisationFactor;
        ar >> m_Value;
    }

    return TRUE;
}

BOOL Measurement::SerializeWithClassUse(CArchive &ar)
{
    if (ar.IsStoring())
    {
        ar << m_Name;
        ar << m_NormalisationFactor;
        ar << m_Value;
		ar << m_Flags.FlagValue;
    }
    else
    {
        ar >> m_Name;
        ar >> m_NormalisationFactor;
        ar >> m_Value;
		ar >> m_Flags.FlagValue;
    }

    return TRUE;
}

//////////////////////////////////////////////////////////////////////////
// Hold an entire list of measurements
//////////////////////////////////////////////////////////////////////////

CMeasures::CMeasures(void)
{
	m_Version = VERSION_NUMBER;
}

CMeasures::~CMeasures(void)
{
    Kill();
}

void CMeasures::Kill(void)          // Delete the list
{
    // Clear up
    POSITION P;
    Measurement *pM;
    P = m_Measurements.GetHeadPosition();
    while (P)
    {
        pM = m_Measurements.GetNext(P);
        delete pM;
    }

    m_Measurements.RemoveAll();
}


//////////////////////////////////////////////////////////////////////////
// Duplication
//////////////////////////////////////////////////////////////////////////
    
CMeasures *CMeasures::Duplicate(void)
{
    CMeasures *pNewMeas;
    POSITION P;
    Measurement *pM;

    pNewMeas = new CMeasures;
    P = m_Measurements.GetHeadPosition();
    while (P)
    {
        pM = m_Measurements.GetNext(P);
		pNewMeas->Add(pM->m_Name, pM->m_Value, pM->m_NormalisationFactor, pM->m_Flags.Flags.IncludeInClassifier, pM->m_Flags.Flags.IncludeInGrid, pM->m_Flags.Flags.IncludeInSort);
    }
    return pNewMeas;
}

//////////////////////////////////////////////////////////////////////////
// Add a measurement to the list
//////////////////////////////////////////////////////////////////////////
void CMeasures::Add(CString Name, COleVariant V, double NormaliseFactor, int IncludeInClassifier, int IncludeInGrid, int IncludeInSort)
{
    Measurement *pMeasure;

    // Change type if required
    switch (V.vt)
    {
    case VT_I2:
    case VT_INT:
    case VT_BOOL:
    case VT_UI1:
    case VT_UI2:
        V.ChangeType(VT_I4);
        break;

    case VT_R4:
        V.ChangeType(VT_R8);
        break;
    }
        
    pMeasure = new Measurement();
    pMeasure->Init(Name, V, NormaliseFactor, IncludeInClassifier, IncludeInGrid, IncludeInSort);

    m_Measurements.AddTail(pMeasure);
}

void CMeasures::Add(CString Name, VARIANT Value, double NormaliseFactor, int IncludeInClassifier, int IncludeInGrid, int IncludeInSort)
{
    Measurement *pMeasure;
    COleVariant V(Value);

    // Change type if required
    switch (V.vt)
    {
    case VT_I2:
    case VT_INT:
    case VT_BOOL:
    case VT_UI1:
    case VT_UI2:
        V.ChangeType(VT_I4);
        break;

    case VT_R4:
        V.ChangeType(VT_R8);
        break;
    }
        
    pMeasure = new Measurement();
    pMeasure->Init(Name, V, NormaliseFactor, IncludeInClassifier, IncludeInGrid, IncludeInSort);

    m_Measurements.AddTail(pMeasure);
}

//////////////////////////////////////////////////////////////////////////
// Add a measurement to the list
//////////////////////////////////////////////////////////////////////////

void CMeasures::Add(CString Name, int Value, double NormaliseFactor, int IncludeInClassifier, int IncludeInGrid, int IncludeInSort)
{
    Measurement *pMeasure;
    COleVariant V((long) Value);
        
    pMeasure = new Measurement();
    pMeasure->Init(Name, V, NormaliseFactor, IncludeInClassifier, IncludeInGrid, IncludeInSort);

    m_Measurements.AddTail(pMeasure);
}

//////////////////////////////////////////////////////////////////////////
// Add a measurement to the list
//////////////////////////////////////////////////////////////////////////

void CMeasures::Add(CString Name, double Value, double NormaliseFactor, int IncludeInClassifier, int IncludeInGrid, int IncludeInSort)
{
    Measurement *pMeasure;
    COleVariant V(Value);
        
    pMeasure = new Measurement();
    pMeasure->Init(Name, V, NormaliseFactor, IncludeInClassifier, IncludeInGrid, IncludeInSort);

    m_Measurements.AddTail(pMeasure);
}

//////////////////////////////////////////////////////////////////////////
// Search for the measurement by name
//////////////////////////////////////////////////////////////////////////

Measurement *CMeasures::FindByName(LPCTSTR Name)     
{
    POSITION P;
    Measurement *pM = NULL, *pLM;

    P = m_Measurements.GetHeadPosition();
    while (P)
    {
        pLM = m_Measurements.GetNext(P);
        if (pLM->m_Name.CompareNoCase(Name) == 0)
        {
            pM = pLM;
            break;
        }
    }

    return pM;
}

//////////////////////////////////////////////////////////////////////////
// I/O
//////////////////////////////////////////////////////////////////////////

BOOL CMeasures::Serialize(CArchive &ar)
{
    int Count, i;
    POSITION P;
    Measurement *pMeasure;

    if (ar.IsStoring())
    {
        Count = m_Measurements.GetCount();
        ar << Count;

        P = m_Measurements.GetHeadPosition();
        for (i = 0; i < Count && P; i++)
        {
            pMeasure = m_Measurements.GetNext(P);
            pMeasure->Serialize(ar);
        }
    }
    else
    {
        ar >> Count;

        for (i = 0; i < Count; i++)
        {
            pMeasure = new Measurement();
            pMeasure->Serialize(ar);
            m_Measurements.AddTail(pMeasure);
        }
    }

    return TRUE;
}

//////////////////////////////////////////////////////////////////////////
// I/O
//////////////////////////////////////////////////////////////////////////

BOOL CMeasures::Serialize(CArchive &ar, int Version)
{
    int Count, i;
    POSITION P;
    Measurement *pMeasure;

    if (ar.IsStoring())
    {
		m_Version = Version;
		ar << m_Version;
        Count = m_Measurements.GetCount();
        ar << Count;

        P = m_Measurements.GetHeadPosition();
        for (i = 0; i < Count && P; i++)
        {
            pMeasure = m_Measurements.GetNext(P);
            pMeasure->SerializeWithClassUse(ar);
        }
    }
    else
    {
		ar >> m_Version;
        ar >> Count;

        for (i = 0; i < Count; i++)
        {
            pMeasure = new Measurement();
            pMeasure->SerializeWithClassUse(ar);
            m_Measurements.AddTail(pMeasure);
        }
    }

    return TRUE;
}
//////////////////////////////////////////////////////////////////////////
// Get the measurement - NOTE the measurement is then deleted from the
// list
//////////////////////////////////////////////////////////////////////////

Measurement *CMeasures::Get()      
{
    POSITION P = m_Measurements.GetHeadPosition();
    if (P)
    {
        Measurement *pMeas;
        pMeas = m_Measurements.GetAt(P);
        m_Measurements.RemoveAt(P);
        return pMeas;
    }
    else
        return NULL;
}

//////////////////////////////////////////////////////////////////////////
// Get the first measurement - NOTE no deletion
//////////////////////////////////////////////////////////////////////////

Measurement *CMeasures::GetFirst()                    // Get the first measurement, do not remove it
{
    m_CurrentPosition = m_Measurements.GetHeadPosition();
    if (m_CurrentPosition)
        return m_Measurements.GetNext(m_CurrentPosition);
    else
        return NULL;
}

//////////////////////////////////////////////////////////////////////////
// Get the next measurement - NOTE no deletion
//////////////////////////////////////////////////////////////////////////

Measurement *CMeasures::GetNext()                    // Get the next measurement, do not remove it
{
    if (m_CurrentPosition)
        return m_Measurements.GetNext(m_CurrentPosition);
    else
        return NULL;
}

//////////////////////////////////////////////////////////////////////////
// Get the number of measurements for classification
//////////////////////////////////////////////////////////////////////////

int CMeasures::GetClassCount()                     // Get the number of measurements in the list
{
	int C = 0;
    Measurement *pMeas;
    POSITION P = m_Measurements.GetHeadPosition();
    while (P)
    {
		pMeas = m_Measurements.GetNext(P);
		if (pMeas->m_Flags.Flags.IncludeInClassifier && pMeas->m_Name.CompareNoCase(_T("Class")) != 0)
			C++;
	}

	return C;
}

//////////////////////////////////////////////////////////////////////////
// Get the number of measurements for sorting
//////////////////////////////////////////////////////////////////////////

int CMeasures::GetSortCount()                     // Get the number of measurements in the list
{
	int C = 0;
    Measurement *pMeas;
    POSITION P = m_Measurements.GetHeadPosition();
    while (P)
    {
		pMeas = m_Measurements.GetNext(P);
		if (pMeas->m_Flags.Flags.IncludeInSort && pMeas->m_Name.CompareNoCase(_T("Class")) != 0)
			C++;
	}

	return C;
}
//////////////////////////////////////////////////////////////////////////
// Get the number of measurements for display
//////////////////////////////////////////////////////////////////////////

int CMeasures::GetDisplayCount()                     // Get the number of measurements in the list
{
	int C = 0;
    Measurement *pMeas;
    POSITION P = m_Measurements.GetHeadPosition();
    while (P)
    {
		pMeas = m_Measurements.GetNext(P);
		if (pMeas->m_Flags.Flags.IncludeInGrid && pMeas->m_Name.CompareNoCase(_T("Class")) != 0)
			C++;
	}

	return C;
}

//////////////////////////////////////////////////////////////////////////
// Sum the measurements
//////////////////////////////////////////////////////////////////////////

double CMeasures::Sum()  
{
	double Tot = 0.0;
	Measurement *pMeas;

	POSITION P = m_Measurements.GetHeadPosition();
    while (P)
    {
		pMeas = m_Measurements.GetNext(P);
		Tot += pMeas->Normalise();
	}

	return Tot;
}