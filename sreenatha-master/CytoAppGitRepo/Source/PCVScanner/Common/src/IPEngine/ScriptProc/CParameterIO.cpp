/////////////////////////////////////////////////////////////////////////////////////
//
// Implements Parameter I/O Functionality
//
// Written By K Ratcliff 04042001
//
//
/////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include "stdafx.h"
#include "CParameterIO.h"

/////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
//

CParameterIO::CParameterIO()
{
	m_UsrParamFileName = "";		// No file names - yet
    m_DefParamFileName = "";
    m_pUsrDataFile = NULL;          // Data storage for both types of file
    m_pDefDataFile = NULL;
    m_bDefDataModified = FALSE;     // No data mods since opening
    m_bUsrDataModified = FALSE;
}

CParameterIO::~CParameterIO()
{
    // Clean up any memory
    if (m_pUsrDataFile)
        delete m_pUsrDataFile;
    if (m_pDefDataFile)
        delete m_pDefDataFile;
}

/////////////////////////////////////////////////////////////////////////////////////
// FileExists - Checks The File Exists
//	Parameters:
//		FileName		- the name of the file to open. 
//	Returns:
//		TRUE If successful, FALSE otherwise
/////////////////////////////////////////////////////////////////////////////////////

BOOL CParameterIO::FileExists(LPCTSTR FileName)
{
    CFile TheFile;

    if (!TheFile.Open(FileName, CFile::modeRead, NULL))
        return FALSE;
    else
    {
        TheFile.Close();
        return TRUE;
    }
}

/////////////////////////////////////////////////////////////////////////////////////
// OpenParameters - opens the specified parameter file for read/write
//	Parameters:
//		FileName		- the name of the file to open. This is a base name
//						  and SHOULD NOT include the extension.
//	Returns:
//		ParameterFileResult - see header
/////////////////////////////////////////////////////////////////////////////////////

ParameterFileResult CParameterIO::OpenParameters(LPCTSTR FileName)
{
    int Result = NO_FILES_FOUND;
    CFile TheFile;
    BOOL UsrDataExists, DefDataExists;

    // Construct user parameter file name
	m_UsrParamFileName.Format(_T("%s%s"), FileName, USER_PARAMFILE_EXT);
    // Construct default parameter file name
    m_DefParamFileName.Format(_T("%s%s"), FileName, DEFAULT_PARAMFILE_EXT);

    // Check existence of each - user parameter file
    if (!FileExists(m_UsrParamFileName))
        UsrDataExists = FALSE;
    else
    {
        UsrDataExists = TRUE;
        Result |= USERPARAMS_FILE_FOUND;
    }
   
    // Check existence of each - default parameter file
    if (!FileExists(m_DefParamFileName))
        DefDataExists = FALSE;
    else
    {
        DefDataExists = TRUE;
        Result |= DEFAULTS_FILE_FOUND;
    }

    // Defaults file does not exist so create it empty
    if (!DefDataExists)
    {
        CFileException fe;

        TheFile.Open(m_DefParamFileName, CFile::modeCreate, &fe);
        if (fe.m_cause == CFileException::none) 
            TheFile.Close();
        DefDataExists = TRUE;
    }

    // If the default file exists but the usr data one does not
    // then make a copy of the usr data file from the def data file
    if (DefDataExists && !UsrDataExists)
        UsrDataExists = CopyFile(m_DefParamFileName, m_UsrParamFileName, FALSE);

    // Create neccessary objects to hold files
    m_pUsrDataFile = new CString;
    m_pDefDataFile = new CString;
    
    // Retrieve File Data
    GetFileData(DEFAULT_PARAMETER_FILE);
    GetFileData(USER_PARAMETER_FILE);
    
	return (ParameterFileResult) Result;
}

/////////////////////////////////////////////////////////////////////////////////////
// CloseParameters - closes the specified parameter file
//	Parameters:
//		None
//	Returns:
//		TRUE If successful, FALSE otherwise
/////////////////////////////////////////////////////////////////////////////////////

BOOL CParameterIO::CloseParameters()
{
    BOOL RetValU = TRUE, RetValD = TRUE;

    if (m_bUsrDataModified && RetValU)
        RetValU = SetFileData(USER_PARAMETER_FILE);

    if (m_bDefDataModified && RetValD)
        RetValD = SetFileData(DEFAULT_PARAMETER_FILE);

	return (RetValU && RetValD);
}
    
/////////////////////////////////////////////////////////////////////////////////////
// GetParameter - gets a named value from a parameter file
//	Parameters:
//		ParameterName - name of parameter to retrieve
//      Value         - returned value
//      CodedDefault  - a hard coded default if the parameter does not exist
//                      in either the default data file OR the user data file
//	Returns:
//		NOPARAMETER_FOUND or PARAMETER_FOUND - see header
// 
// TODO: Improve this GetParameter() member function
/////////////////////////////////////////////////////////////////////////////////////


// int Version

ParameterFileResult CParameterIO::GetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, int *Value, int CodedDefault)
{
    int ParamPos = NOT_FOUND, Eq;
    CString From, To, Number;
    ParameterFileType FT;
    CString *pDataStr;

    // Try and find in the specified file
    if ((FT = SeekParameter(ParamFile, ParameterName, &ParamPos)) != NO_PARAMETER_FILE)
    {
        // Extract the right part of the file string from ParamPos onwards
        if (FT == USER_PARAMETER_FILE)
            pDataStr = m_pUsrDataFile;
        else
            pDataStr = m_pDefDataFile; 
        // Get the right hand part of the string
        From = pDataStr->Right(pDataStr->GetLength() - ParamPos); 
        // Now get up the end of the line
        To = From.SpanExcluding(_T("\r\n"));        
        // Now get the right part of the string from after the equals sign
        Eq = To.GetLength() - To.Find('=', 0) - 1;
        Number = To.Right(Eq);
        // Extract the value making sure it was extracted o.k.
        if (_stscanf((LPCTSTR) Number, _T("%d"), Value) != 1)
        {
            // No parameter with this name exists so use hard coded default
            *Value = CodedDefault;
            return NOPARAMETER_FOUND;
        }
        // Found parameter O.K.
        return PARAMETER_FOUND;
    }
    else
    {
        // No parameter with this name exists so use hard coded default
        *Value = CodedDefault;
        return NOPARAMETER_FOUND;
    }
}

// long Version

ParameterFileResult CParameterIO::GetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, long *Value, long CodedDefault)
{
    int ParamPos = NOT_FOUND, Eq;
    CString From, To, Number;
    ParameterFileType FT;
    CString *pDataStr;

    // Try and find in the specified file
    if ((FT = SeekParameter(ParamFile, ParameterName, &ParamPos)) != NO_PARAMETER_FILE)
    {
        // Extract the right part of the file string from ParamPos onwards
        if (FT == USER_PARAMETER_FILE)
            pDataStr = m_pUsrDataFile;
        else
            pDataStr = m_pDefDataFile; 
        // Get the right hand part of the string
        From = pDataStr->Right(pDataStr->GetLength() - ParamPos); 
        // Now get up the end of the line
        To = From.SpanExcluding(_T("\r\n"));        
        // Now get the right part of the string from after the equals sign
        Eq = To.GetLength() - To.Find('=', 0) - 1;
        Number = To.Right(Eq);
        // Extract the value making sure it was extracted o.k.
        if (_stscanf((LPCTSTR) Number, _T("%ld"), Value) != 1)
        {
            // No parameter with this name exists so use hard coded default
            *Value = CodedDefault;
            return NOPARAMETER_FOUND;
        }
        // Found parameter O.K.
        return PARAMETER_FOUND;
    }
    else
    {
        // No parameter with this name exists so use hard coded default
        *Value = CodedDefault;
        return NOPARAMETER_FOUND;
    }
}

// string Version

ParameterFileResult CParameterIO::GetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, TCHAR *Value, LPCTSTR CodedDefault)
{
    int ParamPos = NOT_FOUND, Eq;
    CString From, To, Number;
    ParameterFileType FT;
    CString *pDataStr;

    // Try and find in the specified file
    if ((FT = SeekParameter(ParamFile, ParameterName, &ParamPos)) != NO_PARAMETER_FILE)
    {
        // Extract the right part of the file string from ParamPos onwards
        if (FT == USER_PARAMETER_FILE)
            pDataStr = m_pUsrDataFile;
        else
            pDataStr = m_pDefDataFile; 
        // Get the right hand part of the string
        From = pDataStr->Right(pDataStr->GetLength() - ParamPos); 
        // Now get up the end of the line
        To = From.SpanExcluding(_T("\r\n"));        
        // Now get the right part of the string from after the equals sign
        Eq = To.GetLength() - To.Find('=', 0) - 1;
        Number = To.Right(Eq);
        Number.TrimLeft();
        // Extract the value making sure it was extracted o.k.
        if (!_tcscpy(Value, Number))
        {
            // No parameter with this name exists so use hard coded default
            _tcscpy(Value, CodedDefault);
            return NOPARAMETER_FOUND;
        }
        // Found parameter O.K.
        return PARAMETER_FOUND;
    }
    else
    {
        // No parameter with this name exists so use hard coded default
        _tcscpy(Value, CodedDefault);
        return NOPARAMETER_FOUND;
    }
}

// double Version

ParameterFileResult CParameterIO::GetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, double *Value, double CodedDefault)
{
    int ParamPos = NOT_FOUND, Eq;
    CString From, To, Number;
    ParameterFileType FT;
    CString *pDataStr;

    // Try and find in the specified file
    if ((FT = SeekParameter(ParamFile, ParameterName, &ParamPos)) != NO_PARAMETER_FILE)
    {
        // Extract the right part of the file string from ParamPos onwards
        if (FT == USER_PARAMETER_FILE)
            pDataStr = m_pUsrDataFile;
        else
            pDataStr = m_pDefDataFile; 
        // Get the right hand part of the string
        From = pDataStr->Right(pDataStr->GetLength() - ParamPos); 
        // Now get up the end of the line
        To = From.SpanExcluding(_T("\r\n"));
        // Now get the right part of the string from after the equals sign
        Eq = To.GetLength() - To.Find(_T("="), 0) - 1;
        Number = To.Right(Eq);
        // Extract the value making sure it was extracted o.k.
        if (_stscanf(Number, _T("%lf"), Value) != 1)
        {
            // No parameter with this name exists so use hard coded default
            *Value = CodedDefault;
            return NOPARAMETER_FOUND;
        }
        // Found parameter O.K.
        return PARAMETER_FOUND;
    }
    else
    {
        // No parameter with this name exists so use hard coded default
        *Value = CodedDefault;
        return NOPARAMETER_FOUND;
    }
}

/////////////////////////////////////////////////////////////////////////////////////
// SetParameter - sets a named value in a parameter file
//	Parameters:
//		ParameterName - name of parameter 
//      Value         - value to set in file
//
//	Returns:
//		NOPARAMETER_FOUND or PARAMETER_FOUND - see header
/////////////////////////////////////////////////////////////////////////////////////


// int Version

ParameterFileResult CParameterIO::SetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, int Value)
{
    return SetParameterEx(ParamFile, ParameterName, INT_PARAMETER, (void *) &Value);
}

// long Version

ParameterFileResult CParameterIO::SetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, long Value)
{
    return SetParameterEx(ParamFile, ParameterName, LONG_PARAMETER, (void *) &Value);
}

// double Version

ParameterFileResult CParameterIO::SetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, double Value)
{
    return SetParameterEx(ParamFile, ParameterName, DOUBLE_PARAMETER, (void *) &Value);
}

// string Version

ParameterFileResult CParameterIO::SetParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, LPCTSTR Value)
{
    return SetParameterEx(ParamFile, ParameterName, STRING_PARAMETER, (void *) Value);
}

// This does the real work for the above wrappers

ParameterFileResult CParameterIO::SetParameterEx(ParameterFileType ParamFile, LPCTSTR ParameterName, 
                                                 ParameterType ParamType, void *Value)
{
    int ParamPos = NOT_FOUND;
    CString From, To, Number;
    ParameterFileType FT;
    
    // Format the new line to save
    switch (ParamType)
    {
    case INT_PARAMETER:
        Number.Format(_T("%s = %d\r\n"), ParameterName, *(int *) Value);
        break;

    case LONG_PARAMETER:
        Number.Format(_T("%s = %ld\r\n"), ParameterName, *(long *) Value);
        break;

    case DOUBLE_PARAMETER:
        Number.Format(_T("%s = %lf\r\n"), ParameterName, *(double *) Value);
        break;

    case STRING_PARAMETER:
        Number.Format(_T("%s = %s\r\n"), ParameterName, (LPCTSTR) Value);
        break;
    }

    // Try and find in the specified file
    FT = SeekParameter(ParamFile, ParameterName, &ParamPos);
    
    if (ParamFile != DEFAULT_PARAMETER_FILE)
    {
        switch (FT)
        {
        case USER_PARAMETER_FILE:
            // Parameter found in the user parameter file
            m_bUsrDataModified = TRUE;
            // Get the right hand part of the string
            From = m_pUsrDataFile->Right(m_pUsrDataFile->GetLength() - ParamPos); 
            // Now get up the end of the line
            To = From.SpanExcluding(_T("\r\n"));      
            // Replace in the file string
            m_pUsrDataFile->Replace((LPCTSTR) To, Number);
            break;
            
        case NO_PARAMETER_FILE:
            // Parameter Is In Neither So Update Both Files
            // Append the value in both files
            m_bUsrDataModified = TRUE;          // User data
            *m_pUsrDataFile += Number;
            m_bDefDataModified = TRUE;          // Default data
            *m_pDefDataFile += Number;
            break;
            
        case DEFAULT_PARAMETER_FILE:
            // Only exists in default parameter file
            // Append the value to user data only
            m_bUsrDataModified = TRUE;          // User data
            *m_pUsrDataFile += Number;
            break;
        }
    }
    else
    {
        // Explicitly update the default data file only
        if (FT == NO_PARAMETER_FILE)            // Not found in file so append to end
        {
            // Append the value
            m_bDefDataModified = TRUE;          // Default data
            *m_pDefDataFile += Number;
        }
        else
        {
            // Replace existing value in default parameter file
             m_bDefDataModified = TRUE;
            // Get the right hand part of the string
            From = m_pDefDataFile->Right(m_pDefDataFile->GetLength() - ParamPos); 
            // Now get up the end of the line
            To = From.SpanExcluding(_T("\r\n"));      
            // Replace in the file string
            m_pDefDataFile->Replace((LPCTSTR) To, Number);
       }
    }

    
    // No parameter with this name exists so use hard coded default
    return NOPARAMETER_FOUND;
}

/////////////////////////////////////////////////////////////////////////////////////
// SeekParameter - searches a parameter file to get the position for it. If this is
//                 user data then this file is searched first and if this parameter
//                 exists in the file, this value is returned. If the parameter is
//                 not in the file then the defaults file is searched and if it is
//                 present this value is returned. Specifying a value of
//                 DEFAULT_PARAMETER_FILE for ParamFile will result in a search of
//                 the default parameter data file ONLY. 
//	Parameters:
//      ParameterFileType   - either USER_PARAMETER_FILE or DEFAULT_PARAMETER_FILE 
//		ParameterName       - name of parameter to set
//      FilePos             - returned position in the file
//	Returns:
//		TRUE if successful, FALSE otherwise
/////////////////////////////////////////////////////////////////////////////////////

ParameterFileType CParameterIO::SeekParameter(ParameterFileType ParamFile, LPCTSTR ParameterName, int *FilePos)
{
    ParameterFileType FT = NO_PARAMETER_FILE;

    // Default to not found in file
    *FilePos = NOT_FOUND;
    
    // Check the user data file
    if (ParamFile == USER_PARAMETER_FILE)
    {
        // User data is actually available
        if (m_pUsrDataFile)
        {
            *FilePos = m_pUsrDataFile->Find(ParameterName);
            if (*FilePos != NOT_FOUND)
                FT = USER_PARAMETER_FILE;

        }
    }
    
    // If we did not get it or use defaults file 
    // try and retrieve from the default parameters
    if ((FT == NO_PARAMETER_FILE || ParamFile == DEFAULT_PARAMETER_FILE) && m_pDefDataFile)
    {
        *FilePos = m_pDefDataFile->Find(ParameterName);
        if (*FilePos != NOT_FOUND)
            FT = DEFAULT_PARAMETER_FILE;
    }
    
    return FT;
}


/////////////////////////////////////////////////////////////////////////////////////
// GetFileData - gets the data from a file and sticks it in a memory buffer
//	Parameters:
//      ParameterFileType   - either USER_PARAMETER_FILE or DEFAULT_PARAMETER_FILE 
//	Returns:
//		TRUE if successful, FALSE otherwise
//  Notes:
//      It will allocate memory to store the whole file in.
/////////////////////////////////////////////////////////////////////////////////////

BOOL CParameterIO::GetFileData(ParameterFileType ParamFile)
{
    CString         FName;
    BOOL            RetVal = FALSE, FResult;
    CFileStatus     FS;
    CStdioFile      TheParamFile;
    CString         FileData = _T(""), FileLine;

    // Get Access To The Right File
    switch (ParamFile)
    {
    case USER_PARAMETER_FILE:
        FName = m_UsrParamFileName;
        break;

    case DEFAULT_PARAMETER_FILE:
        FName = m_DefParamFileName;
        break;

    default:
        return FALSE;       // Precautionary
    }

    // Now open the file
    if (TheParamFile.Open(FName, CFile::modeRead, NULL))
    {
        // Determine length of the file
        TheParamFile.GetStatus(FS);
        
        // Read the parameter file line by line until EOF
        do
        {
            FResult = TheParamFile.ReadString(FileLine);
            if (FResult)
            {
                // Add the next line of VB script code with a newline and carriage return
                FileData += FileLine;
                FileData += _T("\r\n");
                
            }
        } 
        while (FResult);
        
        // Get Access To The Right File
        switch (ParamFile)
        {
        case USER_PARAMETER_FILE:
            *m_pUsrDataFile = FileData;
            break;
            
        case DEFAULT_PARAMETER_FILE:
            *m_pDefDataFile = FileData;
            break;
            
        default:
            return FALSE;       // Precautionary
        }

        TheParamFile.Close();

        RetVal = TRUE;
    }
        
    // Return the result
    return RetVal;
}

/////////////////////////////////////////////////////////////////////////////////////
// SetFileData - writes the data a file
//	Parameters:
//      ParameterFileType   - either USER_PARAMETER_FILE or DEFAULT_PARAMETER_FILE 
//	Returns:
//		TRUE if successful, FALSE otherwise
//  Notes:
//      
/////////////////////////////////////////////////////////////////////////////////////

BOOL CParameterIO::SetFileData(ParameterFileType ParamFile)
{
    CString FName;
    CString *BuffPtr = NULL;
    BOOL    RetVal = FALSE;
    CFileStatus FS;
    CFile   TheParamFile;
    int     rnrnPos = 0;

    // Get Access To The Right File
    switch (ParamFile)
    {
    case USER_PARAMETER_FILE:
        FName = m_UsrParamFileName;
        BuffPtr = m_pUsrDataFile;
        m_bUsrDataModified = FALSE;
        break;

    case DEFAULT_PARAMETER_FILE:
        FName = m_DefParamFileName;
        BuffPtr = m_pDefDataFile;
        m_bDefDataModified = FALSE;
        break;

    default:
        return FALSE;       // Precautionary
    }

    // Delete any \r\n pairs from the string - prevents the file growing !
    while (BuffPtr->Replace(_T("\r\n\r\n"), _T("\r\n")));

    // Now open the file for writing
    if (TheParamFile.Open(FName, CFile::modeWrite, NULL) && BuffPtr)
    {
        CString Line, ACopy = *BuffPtr;
        int Len;
        TCHAR buf[1024];

        // Commit data to disk
        // For each line write it to file
        do
        {
            Line = ACopy.Left(ACopy.Find(_T("\n")));
            if (Line == _T(""))
                Line = _T("\n");
            _tcscpy(buf, Line); 
            TheParamFile.Write(buf, _tcslen(buf));
            Len = ACopy.GetLength() - Line.GetLength();
            ACopy = ACopy.Right(Len);
        }
        while (Len > 0);
        // Close the file
        TheParamFile.Close();

        return TRUE;
    }
    
    return FALSE;
}
