// CfgAssayVarsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "stdafx.h"
#include "resource.h"
#include "assay.h"
#include "scriptparams.h"
#include "esliderctrl.h"
#include "CfgAssayVarsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define SETRECT(rect, l, t, r, b) \
	rect.left = l; \
	rect.top = t; \
	rect.right = r; \
	rect.bottom = b;

LPWORD lpwAlign ( LPWORD lpIn)
{
    ULONG ul;

    ul = (ULONG) lpIn;
    ul +=3;
    ul >>=2;
    ul <<=2;
    return (LPWORD) ul;
}
/////////////////////////////////////////////////////////////////////////////
// CCfgAssayVarsDlg dialog


CCfgAssayVarsDlg::CCfgAssayVarsDlg(CScriptParams *CSP) : CDialog()
{
	//{{AFX_DATA_INIT(CCfgAssayVarsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// initialize the dialog items
    int i;
    CParamSet *pParam;

    m_pCSP = NULL;
    m_nCtrlItems = 0;
    m_pControls = NULL;
    m_nWidth = 200;
    m_nHeight = 30;

    if (CSP)
    {
        // Store a copy of the list
        m_pCSP = CSP;
        
        // Get the total number of parameters
        m_nCtrlItems = CSP->m_ParamSetList.GetCount();
        
        // Allocate template space for each member of the list
        m_pControls = new WndPtr[m_nCtrlItems];

        // Allocate some static group boxes for sliders
        m_pStatics = new WndPtr[m_nCtrlItems];
        
        // Now create the positions of each control
        POSITION p;
        i = 0;
        p = CSP->m_ParamSetList.GetHeadPosition();
        while (p)
        {
            pParam = CSP->m_ParamSetList.GetNext(p);
            
            if (pParam->m_CtrlType == _T("Slider"))
            {
                m_pControls[i] = new ESliderCtrl;  
                m_nHeight += 35;
                m_pStatics[i] = new CStatic;
            }
            else
            if (pParam->m_CtrlType == _T("Button"))
            {
                m_pControls[i] = new CButton;  
                m_nHeight += 30;
                m_pStatics[i] = NULL;
            }
            else
            {
                // Not a recognised control type so keep it NULL - NOTE not all script params are attached
                // to controls and will have type "Assay"
                m_pControls[i] = NULL;
                m_pStatics[i] = NULL;
            }
            i++;
        }
    }
	// Visual Studio uses 8 point MS Sans Serif by default, for resource based
	// controls, so try to copy this.
	LOGFONT logFont;
	memset(&logFont, 0, sizeof(logFont));
	_tcsncpy(logFont.lfFaceName, _T("MS Sans Serif"), LF_FACESIZE);
	logFont.lfHeight         = 8;
	logFont.lfWeight         = FW_MEDIUM;
	logFont.lfCharSet        = DEFAULT_CHARSET;
	logFont.lfQuality        = PROOF_QUALITY;

	m_Font.CreateFontIndirect(&logFont);
}

CCfgAssayVarsDlg::~CCfgAssayVarsDlg()
{
    int i;

    if (m_pControls)
    {
        for (i = 0; i < m_nCtrlItems; i++)
        {
            if (m_pControls[i])
                delete m_pControls[i];
            if (m_pStatics[i])
                delete m_pStatics[i];
        }
        delete [] m_pStatics;
        delete [] m_pControls;
    }
}


BEGIN_MESSAGE_MAP(CCfgAssayVarsDlg, CDialog)
	//{{AFX_MSG_MAP(CCfgAssayVarsDlg)
	ON_BN_CLICKED(IDOK, OnOk)
	ON_BN_CLICKED(IDCANCEL, OnCancel)
	ON_BN_CLICKED(IDDEFAULT, OnDefault)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCfgAssayVarsDlg message handlers

///////////////////////////////////////////////////////////////////////////
// Run the dialog - this creates a dialog template in memory which uses
// the width and height calculated in the dialog constructor
///////////////////////////////////////////////////////////////////////////

int CCfgAssayVarsDlg::DoModal() 
{
	WCHAR               szFontName[] = L"ARIAL";
	int                 nTotalLength = 0;
    LPDLGTEMPLATE       lpDlgTemp;
    LPDLGITEMTEMPLATE   lpdit;
    LPWORD              lpw;
    LPWSTR              lpwsz;
    int                 nchar;
    int                 DlgHeight;
    int                 DlgWidth;
    int                 Ret = 0;

    if (m_nCtrlItems == 0)
    {
        MessageBox(_T("There are no variables to configure for this application"), _T("Warning"), MB_ICONEXCLAMATION | MB_OK);
        return Ret;
    }

    ASSERT(m_pCSP != NULL);

	TRY     // Catch memory exceptions and don't worry about allocation failures
	{
        // Allocate some memory and build up a simple dialog template
        // consisting of a dialog box with an OK button
		HLOCAL hLocal = LocalAlloc(LHND, 4096);
		if (hLocal == NULL)
			AfxThrowMemoryException();

		BYTE*   pBuffer = (BYTE*)LocalLock(hLocal);
		if (pBuffer == NULL)
		{
			LocalFree(hLocal);
			AfxThrowMemoryException();
		}

		BYTE*   pdest = pBuffer;

        DlgWidth = m_nWidth + 10;
        DlgHeight = m_nHeight + 10;

        // Set up the properties of the dialog box
        lpDlgTemp = (LPDLGTEMPLATE) pBuffer;
        lpDlgTemp->style = WS_POPUP | WS_BORDER | DS_MODALFRAME | WS_CAPTION;
        lpDlgTemp->cdit = 3;        // Controls = OK Button, Default and Cancel Button
        lpDlgTemp->x = 0;
        lpDlgTemp->y = 10 - DlgHeight / 2;
        lpDlgTemp->cx = DlgWidth;
        lpDlgTemp->cy = DlgHeight;
        
        lpw = (LPWORD) (lpDlgTemp + 1);

        *lpw++ = 0;   // no menu
        *lpw++ = 0;   // predefined dialog box class (by default)

        lpwsz = (LPWSTR) lpw;
        nchar = 1 + MultiByteToWideChar(CP_ACP, 0, _T("Assay Configuration"), -1, lpwsz, 50);
        lpw   += nchar;

        // Define the OK button.
        lpw = lpwAlign (lpw);       // align DLGITEMTEMPLATE on DWORD boundary
        lpdit = (LPDLGITEMTEMPLATE) lpw;
        lpdit->cx = 40; 
        lpdit->cy = 12;
        lpdit->x  = 10; 
        lpdit->y  = DlgHeight - lpdit->cy - 10;     // Position button at bottom of dlg
        lpdit->id = IDOK;           // OK button identifier
        lpdit->style = WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON;

        lpw = (LPWORD) (lpdit + 1);
        *lpw++ = 0xFFFF;
        *lpw++ = 0x0080;            // button class

        lpwsz = (LPWSTR) lpw;
        nchar = 1 + MultiByteToWideChar (CP_ACP, 0, "OK", -1, lpwsz, 50);
        lpw += nchar;

		lpw = lpwAlign (lpw);       // align DLGITEMTEMPLATE on DWORD boundary
        lpdit = (LPDLGITEMTEMPLATE) lpw;
        lpdit->cx = 40; 
        lpdit->cy = 12;
        lpdit->x  = 60; 
        lpdit->y  = DlgHeight - lpdit->cy - 10;     // Position button at bottom of dlg
        lpdit->id = IDDEFAULT;           // Default button identifier
        lpdit->style = WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON;

        lpw = (LPWORD) (lpdit + 1);
        *lpw++ = 0xFFFF;
        *lpw++ = 0x0080;            // button class

        lpwsz = (LPWSTR) lpw;
        nchar = 1 + MultiByteToWideChar (CP_ACP, 0, "Default", -1, lpwsz, 50);
        lpw += nchar;

		//lpw = lpwAlign (lpw);       // align creation data on DWORD boundary
        //*lpw++ = 0;                 // no creation data

        // Define the Cancel button.
        lpw = lpwAlign (lpw);       // align DLGITEMTEMPLATE on DWORD boundary
        lpdit = (LPDLGITEMTEMPLATE) lpw;
        lpdit->cx = 40; 
        lpdit->cy = 12;
        lpdit->x  = DlgWidth - 50; 
        lpdit->y  = DlgHeight - lpdit->cy - 10;     // Position button at bottom of dlg
        lpdit->id = IDCANCEL;       // Cancel button identifier
        lpdit->style = WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON;

        lpw = (LPWORD) (lpdit + 1);
        *lpw++ = 0xFFFF;
        *lpw++ = 0x0080;            // button class

        lpwsz = (LPWSTR) lpw;
        nchar = 1 + MultiByteToWideChar (CP_ACP, 0, _T("Cancel"), -1, lpwsz, 50);
        lpw += nchar;
        lpw = lpwAlign (lpw);       // align creation data on DWORD boundary
        *lpw++ = 0;                 // no creation data

		InitModalIndirect((DLGTEMPLATE*)pBuffer);
        Ret = CDialog::DoModal();              // Do the dialog

		LocalUnlock(hLocal);
		LocalFree(hLocal);
	}

	CATCH(CMemoryException, e)
	{
		MessageBox(_T("Memory allocation for dialog template failed.  Demo aborted!"),
			        _T("Allocation Failure"), MB_ICONEXCLAMATION | MB_OK);
	}
	END_CATCH
        
    return Ret;
}

///////////////////////////////////////////////////////////////////////////
// Initialise the dialog - this goes through the param list and creates
// the controls associated with each parameter
///////////////////////////////////////////////////////////////////////////

BOOL CCfgAssayVarsDlg::OnInitDialog() 
{
    int         i;
    POSITION    p;
    CParamSet   *pParam;
    int         Ypos, Xpos;
    CButton     *pCB;
    ESliderCtrl *pSC;
    CStatic     *pSt;
    CRect       rc, rcStat;
    int         W;
    TEXTMETRIC  TM;

	CDialog::OnInitDialog();
	
    Xpos = 20;
    Ypos = 15;

    // Set the dialog font
    SetFont(&m_Font);

    // Get the average text size for spacing controls
    CDC *dc = this->GetDC();

    dc->GetTextMetrics(&TM);

    m_nTextHeight = TM.tmHeight;
    m_nTextWidth = TM.tmMaxCharWidth;
    
    // Now position any defined buttons on the dialog
    p = m_pCSP->m_ParamSetList.GetHeadPosition();
    for (i = 0; i < m_nCtrlItems && p; i++)
    {
        pParam = m_pCSP->m_ParamSetList.GetNext(p);
        if (pParam->m_CtrlType == _T("Button") && m_pControls[i])
        {
            W = 10 + pParam->m_Label.GetLength() * m_nTextWidth;
            if (W > m_nWidth) 
                m_nWidth = W;
            rc.SetRect(Xpos, Ypos, Xpos + W, Ypos + 20);
            pCB = (CButton *) m_pControls[i];
            pCB->Create(pParam->m_Label, WS_CHILD | WS_VISIBLE, rc, this, CFGVARS_CONTROL_BASEID + i);
            pCB->SetButtonStyle(BS_AUTOCHECKBOX, TRUE);
            pCB->SetFont(&m_Font);
            if (pParam->m_Value.lVal)
                pCB->SetCheck(BST_CHECKED);
            else
                pCB->SetCheck(BST_UNCHECKED);
            Ypos += 30;
        }
    }
	
    Ypos += 25;
    Xpos = 5;

    // Now position any defined sliders on the dialog
    p = m_pCSP->m_ParamSetList.GetHeadPosition();
    for (i = 0; i < m_nCtrlItems && p; i++)
    {
        pParam = m_pCSP->m_ParamSetList.GetNext(p);
        if (pParam->m_CtrlType == _T("Slider") && m_pControls[i])
        {
            rc.SetRect(Xpos, Ypos - 25, Xpos + 305, Ypos + 33);
            pSt = (CStatic *) m_pStatics[i];
            pSt->CreateEx(WS_EX_STATICEDGE,
                          _T("STATIC"),
                          pParam->m_Label,
                          WS_CHILD | WS_VISIBLE,
                          rc,
                          this,
                          IDC_STATIC);
            pSt->SetFont(&m_Font);

            rc.SetRect(Xpos + 5, Ypos, Xpos + 290, Ypos + 30);
            pSC = (ESliderCtrl *) m_pControls[i];
            pSC->Create(WS_CHILD | WS_VISIBLE, rc, this, CFGVARS_CONTROL_BASEID + i);
            pSC->SetFont(&m_Font);
            pSC->SetRange(pParam->m_Min.lVal, pParam->m_Max.lVal);
            pSC->SetPos(pParam->m_Value.lVal);
            Ypos += 60;
        }
    }
	return TRUE;  
}

///////////////////////////////////////////////////////////////////////////
// User clicked the OK button so set these parameter values back again
///////////////////////////////////////////////////////////////////////////

void CCfgAssayVarsDlg::OnOk() 
{
    int         i, v;
    POSITION    p;
    CParamSet   *pParam;
    CButton     *pCB;
    ESliderCtrl *pSC;

    // Now position any defined sliders on the dialog
    p = m_pCSP->m_ParamSetList.GetHeadPosition();
    for (i = 0; i < m_nCtrlItems && p; i++)
    {
        pParam = m_pCSP->m_ParamSetList.GetNext(p);
        if (pParam->m_CtrlType == _T("Slider"))
        {
            pSC = (ESliderCtrl *) m_pControls[i];
            pSC->GetPos(&v);
            pParam->m_Value.lVal = (long) v;
        }
        else
        if (pParam->m_CtrlType == _T("Button"))
        {
            pCB = (CButton *) m_pControls[i];
            if (pCB->GetCheck() == BST_CHECKED)
                pParam->m_Value.lVal = 1;
            else
                pParam->m_Value.lVal = 0;
        }
 		pParam->m_Value.vt = VT_I4;
   }

    EndDialog(TRUE);
}

///////////////////////////////////////////////////////////////////////////
// Restore defaults
///////////////////////////////////////////////////////////////////////////

void CCfgAssayVarsDlg:: OnDefault()
{
    int         i;
    POSITION    p;
    CParamSet   *pParam;
    CButton     *pCB;
    ESliderCtrl *pSC;

    // Now position any defined sliders on the dialog
    p = m_pCSP->m_ParamSetList.GetHeadPosition();
    for (i = 0; i < m_nCtrlItems && p; i++)
    {
        pParam = m_pCSP->m_ParamSetList.GetNext(p);
		pParam->m_Value = pParam->m_Default;			// Restore the default
        if (pParam->m_CtrlType == _T("Slider"))
        {
            pSC = (ESliderCtrl *) m_pControls[i];
			pSC->SetPos(pParam->m_Value.lVal);
        }
        else
        if (pParam->m_CtrlType == _T("Button"))
        {
            pCB = (CButton *) m_pControls[i];
			pCB->SetCheck(pParam->m_Value.lVal > 0 ? BST_CHECKED : BST_UNCHECKED);
        }
   }
}
