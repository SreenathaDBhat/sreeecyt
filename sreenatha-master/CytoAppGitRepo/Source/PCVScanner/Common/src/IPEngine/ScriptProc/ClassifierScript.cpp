// ClassifierScript.cpp : implementation file
// JMB Jun 2001
//
// 25Apr2002	JMB	Added GetClassFeatureStats


#include "stdafx.h"
#include "tchar.h"
#include "SVMClassifier.h"
#include "ClassifierScript.h"
#include "SafeArr.h"
#include <comdef.h> // For variant_t


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ClassifierScript

IMPLEMENT_DYNCREATE(ClassifierScript, CCmdTarget)

ClassifierScript::ClassifierScript()
{
	EnableAutomation();

	
    pSVMClassifier = NULL;
    m_SVMModelName = _T("");
}

ClassifierScript::~ClassifierScript()
{
    if (pSVMClassifier)
    {
        delete pSVMClassifier;
        pSVMClassifier = NULL;
    }
}

void ClassifierScript::Reset(void)
{

}

void ClassifierScript::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// delete the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}


BEGIN_MESSAGE_MAP(ClassifierScript, CCmdTarget)
	//{{AFX_MSG_MAP(ClassifierScript)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(ClassifierScript, CCmdTarget)
	//{{AFX_DISPATCH_MAP(ClassifierScript)
	
	DISP_FUNCTION(ClassifierScript, "SVMModel", SVMModel, VT_BOOL, VTS_BSTR)
    DISP_FUNCTION(ClassifierScript, "SVMClassify", SVMClassify, VT_I4, VTS_I4 VTS_PVARIANT)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_CIlassifierScript to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {E98F6FD7-3548-48F1-9DA2-6C0D96BA8138}
static const IID IID_IClassifierScript =
{ 0xe98f6fd7, 0x3548, 0x48f1, { 0x9d, 0xa2, 0x6c, 0xd, 0x96, 0xba, 0x81, 0x38 } };

BEGIN_INTERFACE_MAP(ClassifierScript, CCmdTarget)
	INTERFACE_PART(ClassifierScript, IID_IClassifierScript, Dispatch)
END_INTERFACE_MAP()



/////////////////////////////////////////////////////////////////////////////
// Loads a new SVM Model
// 
// VB script syntax:
//      Result = SVMModel(FileName)
// Parameters:
//      FileName is the name of the model file
// Returns:
//		True if the filename exists
/////////////////////////////////////////////////////////////////////////////

BOOL ClassifierScript::SVMModel(LPCTSTR FileName)
{
    if (pSVMClassifier)
        delete pSVMClassifier;
    m_SVMModelName.Format(_T("%s"), FileName);

    pSVMClassifier = new CSVMPredict(m_SVMModelName);

	return pSVMClassifier->HasModel();
}

BOOL ClassifierScript::LoadSVMModel(LPCTSTR FileName)
{
    return SVMModel(FileName);
}

/////////////////////////////////////////////////////////////////////////////
// Classify a series of feature vectors
// 
// VB script syntax:
//      Result = SVMClassify(Features)
// Parameters:
//      Features is an array of normalised double vectors
// Returns:
//      -1 for unclassified, 1 for classified, 0 for no existing model
/////////////////////////////////////////////////////////////////////////////

long ClassifierScript::SVMClassify(long NumFeatures, VARIANT FAR *Features)
{
    SAFEARRAY   *pSafe;
    long        lB, uB;
    double      *Data;
    int         Result;
    int         i;
    VARIANT     V;                // Temp var for packaging data to put in array
    long        idx[1];

    if (!pSVMClassifier)
        return 0;

	if (!pSVMClassifier->HasModel())
		return 0;

    // Get info about HistogramData
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Features, &pSafe, 1), return 0);
    // Now get the dimensions of the array
    SafeArrayGetLBound(pSafe, 1, &lB);
    SafeArrayGetUBound(pSafe, 1, &uB);

    // Must be >= greater than the number of features to classify
    if ((uB - lB) < NumFeatures)
        AfxThrowOleDispatchException(0xFF, IDS_ARRAYDIMENSIONS, 0);

    Data = new double[NumFeatures];

    // Copy data across
    for (i = 0; i < NumFeatures; i++)
    {
        idx[0] = i;
        SafeArrayGetElement(pSafe, idx, &V);
        Data[i] = V.dblVal;
    }

    // Run the features through the classifier
    Result = pSVMClassifier->Predict(NumFeatures, Data);

    delete [] Data;

    return Result;
}
