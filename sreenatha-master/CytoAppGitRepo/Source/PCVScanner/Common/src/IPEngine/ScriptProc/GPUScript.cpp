/////////////////////////////////////////////////////////////////////////////
// GPUScript.cpp : implementation file
//
// Graphics card hardware accelerated image processing
//
// Written By Karl Ratcliff 22042005
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
// 
//

#include "stdafx.h"
#include "vcmemrect.h"
#include "commonscript.h"
#include "vcblob.h"
#include "vcgeneric.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "woolzif.h"
#include "vcthresh.h"
#include "vcImProc.h"
#include "uts.h"
#include "safearr.h"
#include "scriptutils.h"
#include "resource.h"
#include "GIP.h"
#include "GPUScript.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// TMAScript
/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(GPUIPScript, CCmdTarget)

GPUIPScript::GPUIPScript()
{
	EnableAutomation();
    // By default work in interactive mode
    m_ExecutionMode = modeDebug;

    m_pGIP = new GIP();

    m_bGIPCreated = FALSE;
    m_bUseGIP = TRUE;           // Assume we can use the GIP

}

GPUIPScript::~GPUIPScript()
{
    if (m_pGIP)
        delete m_pGIP;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// NOTE implement class members not exposed to the scripting engine here
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


void GPUIPScript::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}


BEGIN_MESSAGE_MAP(GPUIPScript, CCmdTarget)
	//{{AFX_MSG_MAP(GPUIPScript)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(GPUIPScript, CCmdTarget)
	//{{AFX_DISPATCH_MAP(GPUIPScript)
	DISP_FUNCTION(GPUIPScript, "CreateGIP", Create, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(GPUIPScript, "IsGIPInUse", IsGIPInUse, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(GPUIPScript, "DestroyGIP", DestroyGIP, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(GPUIPScript, "PutGIP", PutGIP, VT_BOOL, VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "GetGIP", GetGIP, VT_BOOL, VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "GetComponentGIP", GetComponentGIP, VT_BOOL, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "PutSubImageGIP", PutSubImageGIP, VT_BOOL, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "BeginGIP", BeginGIP, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(GPUIPScript, "EndGIP", EndGIP, VT_EMPTY, VTS_NONE)
    DISP_FUNCTION(GPUIPScript, "CreateGIPImageAs",CreateGIPImageAs, VT_I4, VTS_I4 VTS_I4)
    DISP_FUNCTION(GPUIPScript, "CreateGIPImage",CreateGIPImage, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "FreeGIPImage",FreeGIPImage, VT_EMPTY, VTS_PVARIANT)
	DISP_FUNCTION(GPUIPScript, "ReleaseGIPImage", ReleaseGIPImage, VT_EMPTY, VTS_PVARIANT)
	DISP_FUNCTION(GPUIPScript, "GetGIPImageDimensions", GetGIPImageDimensions, VT_EMPTY, VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(GPUIPScript, "OpenGIP", OpenGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "CloseGIP", CloseGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "DilateGIP", DilateGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "ErodeGIP", ErodeGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "ThreshGIP", ThreshGIP, VT_EMPTY, VTS_I4 VTS_R4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "RGBHSIGIP", RGBHSIGIP, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "HSIRGBGIP", HSIRGBGIP, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "ColourToGreyGIP", ColourToGreyGIP, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "GreyQuadsToColourGIP", GreyQuadsToColourGIP, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "Technique", Technique, VT_EMPTY, VTS_BSTR VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "ColourTechnique", ColourTechnique, VT_EMPTY, VTS_BSTR VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "MathsTechnique", MathsTechnique, VT_EMPTY, VTS_BSTR VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "AddGIP", AddGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "SubtractGIP", SubtractGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "MultiplyGIP", MultiplyGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "DivideGIP", DivideGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "DiffGIP", DiffGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "ModGIP", ModGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "MultipassTechnique", MultipassTechnique, VT_EMPTY, VTS_BSTR VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "PongColourTechnique", PongColourTechnique, VT_EMPTY, VTS_BSTR VTS_BSTR VTS_I4 VTS_I4 VTS_I4)
    DISP_FUNCTION(GPUIPScript, "ComponentSplit", SplitComponents, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
    DISP_FUNCTION(GPUIPScript, "ComponentJoin", JoinComponents, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
    DISP_FUNCTION(GPUIPScript, "ComponentThreshold", ComponentThreshold, VT_EMPTY, VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_I4)
    DISP_FUNCTION(GPUIPScript, "GaussianBlur", GaussianBlur, VT_EMPTY, VTS_I4 VTS_R4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "DOGGIP", DOG, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
    DISP_FUNCTION(GPUIPScript, "UnsharpMask", UnsharpMask, VT_EMPTY, VTS_I4 VTS_R4 VTS_R4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "NearestNeighbDeconv", NearestNeighbDeconv, VT_EMPTY, VTS_I4 VTS_R4 VTS_R4 VTS_PVARIANT VTS_PVARIANT)
    DISP_FUNCTION(GPUIPScript, "Kuwahara", Kuwahara, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "MedianGIP", MedianGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "TestGIP", TestGIP, VT_EMPTY, VTS_I4 VTS_I4)
    DISP_FUNCTION(GPUIPScript, "ComponentBlend", BlendComponents, VT_EMPTY, VTS_I4 VTS_R4 VTS_I4 VTS_R4 VTS_I4 VTS_R4 VTS_I4)
    DISP_FUNCTION(GPUIPScript, "ConvolveGIP", ConvolveGIP, VT_EMPTY, VTS_I4 VTS_PVARIANT VTS_I4 VTS_I4)
    DISP_FUNCTION(GPUIPScript, "ConvolveTechnique", ConvolveTechnique, VT_EMPTY, VTS_BSTR VTS_I4 VTS_PVARIANT VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "SimpleBlendGIP", SimpleBlend, VT_EMPTY, VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(GPUIPScript, "HDRBlendGIP", HDRBlend, VT_EMPTY, VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(GPUIPScript, "RangeBlendGIP", RangeBlend, VT_EMPTY, VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(GPUIPScript, "CombineMultiExposuresGIP", CombineMultiExposures, VT_EMPTY, VTS_I4 VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(GPUIPScript, "HistogramGIP", Histogram, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "AddConstGIP", AddConstGIP, VT_EMPTY, VTS_I4 VTS_R4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "SubtractConstGIP", SubtractConstGIP, VT_EMPTY, VTS_I4 VTS_R4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "MultiplyConstGIP", MultiplyConstGIP, VT_EMPTY, VTS_I4 VTS_R4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "DivideConstGIP", DivideConstGIP, VT_EMPTY, VTS_I4 VTS_R4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "CopyGIP", CopyGIP, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "ColourDeconvGIP", ColourDeconvGIP, VT_EMPTY, VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(GPUIPScript, "CropImageGIP", CropImageGIP, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "ResizeImageGIP", ResizeImageGIP, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "FFT2DPowerGIP", FFT2DPowerSpectrumGIP, VT_I4, VTS_I4)
	DISP_FUNCTION(GPUIPScript, "FFT2DGIP", FFT2DGIP, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "GaussianFFT2DGIP", GaussianFFT2DGIP, VT_I4, VTS_I4 VTS_I4 VTS_R4 VTS_R4)
	DISP_FUNCTION(GPUIPScript, "ButterWorthFFT2DGIP", ButterWorthFFT2DGIP, VT_I4, VTS_I4 VTS_I4 VTS_R4 VTS_R4 VTS_R4)
	DISP_FUNCTION(GPUIPScript, "FFTImageRegistrationGIP", FFTImageRegistrationGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(GPUIPScript, "CoreFFTImageRegistrationGIP", CoreFFTImageRegistrationGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(GPUIPScript, "DistanceMapGIP", DistanceMapGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_BOOL VTS_BOOL VTS_R4)
	DISP_FUNCTION(GPUIPScript, "CannyEdgeGIP",CannyEdgeGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_R4 VTS_R4)
	DISP_FUNCTION(GPUIPScript, "HoughLinesGIP",HoughLinesGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_BSTR VTS_I4 VTS_I4)
	DISP_FUNCTION(GPUIPScript, "FFTImageStitchGIP",FFTImageStitchGIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(GPUIPScript, "FFTImageStitch",FFTImageStitch, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(GPUIPScript, "LBPGIP", GIPLBP, VT_EMPTY, VTS_I4 VTS_I4 VTS_R4 VTS_I4)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_ITMAScript to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {769FE7B1-DBD6-4643-83F3-7B9026CDC5FC}
static const IID IID_IGPUScript = 
{ 0x769fe7b1, 0xdbd6, 0x4643, { 0x83, 0xf3, 0x7b, 0x90, 0x26, 0xcd, 0xc5, 0xfc } };

BEGIN_INTERFACE_MAP(GPUIPScript, CCmdTarget)
	INTERFACE_PART(GPUIPScript, IID_IGPUScript, Dispatch)
END_INTERFACE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Reset member called from ScriptProc reset method
/////////////////////////////////////////////////////////////////////////////

void GPUIPScript::Reset()
{
    if (m_pGIP)
        m_pGIP->GIPReleaseAll();
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// GPUIPScript message handlers
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// THis function checks for a created GIP and also checks
// width and height match the created GIP
// If Width and or Height are < 0 they are don't cares
/////////////////////////////////////////////////////////////////////////////

void GPUIPScript::CheckDimGipImage(GIPImage **gpSource, long Width, long Height, long Bpp)	// Checks for width and height and GIP creation
{
	long gpImaWidth, gpImaHeight, gpImaOverlp, gpImaBpp;

	if (gpSource<=0)
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Invalid Image
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    if ((Width > 0) && (Width % 4))
        AfxThrowOleDispatchException(0xFF, IDS_IMAGESIZEDIV4, 0);           // Not divisable by 4
	m_pGIP->GIPGetImageDimensions(gpSource, &gpImaWidth, &gpImaHeight, &gpImaOverlp, &gpImaBpp);
   if (((Width > 0)  && (Width != gpImaWidth)) || ((Height > 0) && (Height != gpImaHeight)) || ((Bpp>0) && (Bpp != gpImaBpp)))
        AfxThrowOleDispatchException(0xFF, IDS_GIPSIZEMISMATCH, 0);         // Width and height mismatch
}

/////////////////////////////////////////////////////////////////////////////
// Create the GIP
// Example:
//      If (CreateGIP) Then ....
// Parameters:
//      None 
//      GIP images are dynamically declared and destroyed
// Returns:
//      True if successful, False otherwise
// Notes:
//      An error will occur if image width are not divisible by 4
/////////////////////////////////////////////////////////////////////////////

afx_msg BOOL GPUIPScript::Create(void)
{   

   // Tried initialising GIP before but failed so don't try init again
    //if (!m_bUseGIP)
    //    return FALSE;


    if (!m_bGIPCreated)
    {
		if (!m_pGIP)
			m_pGIP = new GIP();

        m_bGIPCreated = m_pGIP->Create();
        //if (!m_bGIPCreated)
       //     m_bUseGIP = FALSE;      // Avoid creating GIP again
    }
    return m_bGIPCreated;
}

/////////////////////////////////////////////////////////////////////////////
// IsGIPInUse
// - Test If the GIP is created
/////////////////////////////////////////////////////////////////////////////
afx_msg BOOL GPUIPScript::IsGIPInUse(void)
{
	return m_bGIPCreated;
}

/////////////////////////////////////////////////////////////////////////////
// Destroy GIP
// - Test If the GIP is created
/////////////////////////////////////////////////////////////////////////////
afx_msg void GPUIPScript::DestroyGIP(void)
{
	if (m_pGIP) delete m_pGIP;
	m_pGIP = NULL;
	m_bGIPCreated = FALSE;
    m_bUseGIP = TRUE;           // Assume we can use the GIP again
}

/////////////////////////////////////////////////////////////////////////////
// Transfer an image to the GIP
// Example:
//      If (PutGIP(ImageHandle, Buffer)) Then ....
// Parameters:
//      ImageHandle is a valid UTS image handle
//      Buffer is the handle of the buffer to put the image in
// Returns:
//      True if successful, False otherwise
// Notes:
//      An error will occur if images are not divisible by 4
/////////////////////////////////////////////////////////////////////////////

afx_msg BOOL GPUIPScript::PutGIP(long Image, GIPImage **gpImage)
{  
    long Width, Height, Pitch, Bpp;
    void *Addr;

    // Get info about this image
    Bpp=IP().UTSMemRect()->GetExtendedInfo(Image, &Width, &Height, &Pitch, &Addr);

    // Error checks
    CheckDimGipImage(gpImage, Width, Height, Bpp);

    if (Bpp == 8)
		return m_pGIP->PutImage(gpImage, (BYTE *) Addr);
	else if (Bpp == 16)
		return m_pGIP->PutImage(gpImage, (unsigned short *) Addr);
	else if (Bpp == 24)
		return m_pGIP->PutImage(gpImage, (RGBTRIPLE *) Addr);
	else if (Bpp == 48)
		return m_pGIP->PutImage(gpImage, (RGB16BTRIPLE *) Addr);

	else
        AfxThrowOleDispatchException(0xFF, IDS_GIPSIZEMISMATCH, 0);         // Bpp mismatch
	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Transfer a subimage to the GIP
// Parameters:
//      ImageHandle is a valid UTS image handle
//      gpImage is the handle to a GIPImage to put the image in
//		x0, y0 - determine the offset from where the subimage should be copied
//				 the width and height of the gipimage determine the size of the subimage
//		ColourMode - 0 - convert colour to grey
//					 otherwise - no conversion
// Returns:
//      True if successful, False otherwise
// Notes:
//      An error will occur if images are not divisible by 4
/////////////////////////////////////////////////////////////////////////////

afx_msg BOOL GPUIPScript::PutSubImageGIP(long Image, GIPImage **gpImage, long x0, long y0, long ColourMode)
{  
    long Width, Height, Pitch, Bpp;
	long gpWidth, gpHeight, gpOverlp, gpBpp;
    void *Addr;

    // Get info about this image
    Bpp=IP().UTSMemRect()->GetExtendedInfo(Image, &Width, &Height, &Pitch, &Addr);
	if (gpImage <= 0)
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Invalid Image
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    if (Width % 4)
        AfxThrowOleDispatchException(0xFF, IDS_IMAGESIZEDIV4, 0);           // Not divisable by 4
	m_pGIP->GIPGetImageDimensions(gpImage, &gpWidth, &gpHeight, &gpOverlp, &gpBpp);
	if ((gpBpp >24) || (gpWidth+x0 > Width) || (gpHeight+y0 > Height))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Invalid Image
    if (Bpp == 8)
		return m_pGIP->PutImage(gpImage, (BYTE *) Addr, Width, Height, x0, y0);
	else if (Bpp == 16)
		return m_pGIP->PutImage(gpImage, (unsigned short *) Addr, Width, Height, x0, y0);
	else if (Bpp == 24)
		return m_pGIP->PutImage(gpImage, (RGBTRIPLE *) Addr, Width, Height, x0, y0, ColourMode);
	else if (Bpp == 48)
		return m_pGIP->PutImage(gpImage, (RGB16BTRIPLE *) Addr, Width, Height, x0, y0, ColourMode);
	else
        AfxThrowOleDispatchException(0xFF, IDS_GIPSIZEMISMATCH, 0);         // Bpp mismatch
	return FALSE;
}
/////////////////////////////////////////////////////////////////////////////
// Transfer an image from the GIP
// Example:
//      If (GetGIP(ImageHandle, Buffer)) Then ....
// Parameters:
//      ImageHandle is a valid UTS image handle
//      Buffer is the handle of the buffer to put the image in
// Returns:
//      True if successful, False otherwise
// Notes:
//      An error will occur if images are not divisible by 4
//      or no GIP or width and height do no match those for the created 
//      GIP
/////////////////////////////////////////////////////////////////////////////

afx_msg BOOL GPUIPScript::GetGIP(long Image, GIPImage **gpImage)
{  
    long Width, Height, Pitch, Bpp;
    void *Addr;

    // Get info about this image
    Bpp=IP().UTSMemRect()->GetExtendedInfo(Image, &Width, &Height, &Pitch, &Addr);
    // Error checks
 	CheckDimGipImage(gpImage, Width, Height, Bpp);
	if (Bpp == 8)
		return m_pGIP->GetImage(gpImage, (BYTE *) Addr);
	else if (Bpp==16)
		return m_pGIP->GetImage(gpImage, (unsigned short *)Addr);
	else if (Bpp==24)
		return m_pGIP->GetImage(gpImage, (RGBTRIPLE *)Addr);
	else if (Bpp==48)
		return m_pGIP->GetImage(gpImage, (RGB16BTRIPLE *)Addr);
	else
        AfxThrowOleDispatchException(0xFF, IDS_GIPSIZEMISMATCH, 0);         // Bpp mismatch
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// Transfer an image from the GIP
// Example:
//      If (GetGIP(ImageHandle, Buffer)) Then ....
// Parameters:
//      ImageHandle is a valid UTS image handle
//      Buffer is the handle of the buffer to put the image in
// Returns:
//      True if successful, False otherwise
// Notes:
//      An error will occur if images are not divisible by 4
//      or no GIP or width and height do no match those for the created 
//      GIP
/////////////////////////////////////////////////////////////////////////////

afx_msg BOOL GPUIPScript::GetComponentGIP(long Image, long component, GIPImage **gpImage)
{  
    long Width, Height, Pitch, Bpp;
    void *Addr;

    // Get info about this image
    Bpp=IP().UTSMemRect()->GetExtendedInfo(Image, &Width, &Height, &Pitch, &Addr);
    // Error checks
	if (Bpp == 8)
 		CheckDimGipImage(gpImage, Width, Height, 24);
	else if (Bpp == 16)
 		CheckDimGipImage(gpImage, Width, Height, 48);
	if (Bpp == 8)
		return m_pGIP->GetImageComponent(gpImage, component, (BYTE *) Addr);
	else if (Bpp==16)
		return m_pGIP->GetImageComponent(gpImage, component, (unsigned short *)Addr);
	else
        AfxThrowOleDispatchException(0xFF, IDS_GIPSIZEMISMATCH, 0);         // Bpp mismatch
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// Begin GIP Processing
// Example:
//      BeginGIP
// Parameters:
//      None
// Returns:
//      Bugger all
// Notes:
//      An error occurs if no GIP
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::BeginGIP(void)
{  
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error

    m_pGIP->BeginIP();
}

/////////////////////////////////////////////////////////////////////////////
// End GIP Processing
// Example:
//      EndGIP
// Parameters:
//      None
// Returns:
//      Bugger all
// Notes:
//      An error occurs if no GIP
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::EndGIP(void)
{  
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error

    m_pGIP->EndIP();
}

/////////////////////////////////////////////////////////////////////////////
// Create an Image in GPU memory
/////////////////////////////////////////////////////////////////////////////
afx_msg long GPUIPScript::CreateGIPImageAs(long Source, long Overlap)
{
    long Width, Height, Pitch, BPP, Addr;
    long HandleGipImage;

	// Get info about this image
	if (Source !=NULL)
	{
		BPP = IP().UTSMemRect()->GetExtendedInfo(Source, &Width, &Height, &Pitch, &Addr);
		if (!m_bGIPCreated)
			AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
		if (Width % 4)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGESIZEDIV4, 0);           // Not divisable by 4    
		if ((BPP != 8) && (BPP !=24) && (BPP != 16) && (BPP != 48))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Invalid Image (should be 8 bit or 24 bit)
		HandleGipImage=(long)m_pGIP->GIPCreateImage(Width, Height, Overlap, BPP);
        if (!HandleGipImage)
			AfxThrowOleDispatchException(0xFF, IDS_NOGIPIMAGECREATED, 0);            // We have no GIP so kick up an error
		return HandleGipImage;
	}
	return -1L;
}

/////////////////////////////////////////////////////////////////////////////
// Create an Image in GPU memory
/////////////////////////////////////////////////////////////////////////////
afx_msg long GPUIPScript::CreateGIPImage(long Width, long Height, long Overlap, long BPP)
{
    long HandleGipImage;

	if (!m_bGIPCreated)
		AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);				// We have no GIP so kick up an error
	if (Width % 4)
		AfxThrowOleDispatchException(0xFF, IDS_IMAGESIZEDIV4, 0);				// Not divisable by 4    
    
	HandleGipImage=(long)m_pGIP->GIPCreateImage(Width, Height, Overlap, BPP);
    if (!HandleGipImage)
		AfxThrowOleDispatchException(0xFF, IDS_NOGIPIMAGECREATED, 0);           // No GIPImage so kick up an error
	return HandleGipImage;
}

/////////////////////////////////////////////////////////////////////////////
// Release an Image From GPU memory
/////////////////////////////////////////////////////////////////////////////
afx_msg void GPUIPScript::ReleaseGIPImage(VARIANT FAR* HandleGipSource)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    if (HandleGipSource->lVal > 0)
 		m_pGIP->GIPReleaseImage((GIPImage **)HandleGipSource->lVal);
}

/////////////////////////////////////////////////////////////////////////////
// Free an Image From GPU memory
/////////////////////////////////////////////////////////////////////////////
afx_msg void GPUIPScript::FreeGIPImage(VARIANT FAR* HandleGipSource)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    if (HandleGipSource->lVal > 0)
    {
 		m_pGIP->GIPFreeImage((GIPImage **)HandleGipSource->lVal);
		HandleGipSource->lVal = NULL;              // Mark the handle as invalid
    }
}

/////////////////////////////////////////////////////////////////////////////
// Get the dimensions of a GIPImage
/////////////////////////////////////////////////////////////////////////////
afx_msg void GPUIPScript::GetGIPImageDimensions(GIPImage **HandleGipSource, VARIANT FAR*  Width, VARIANT FAR* Height, VARIANT FAR* Overlap,
												VARIANT FAR* BPP)
{
	long	w, h,  overlp, bpp;

	if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error

	if (HandleGipSource)
	{
		m_pGIP->GIPGetImageDimensions(HandleGipSource, &w, &h, &overlp, &bpp);
		V_VT(Width)  = VT_I4;
		V_VT(Height) = VT_I4;
		V_VT(Overlap) = VT_I4;
		V_VT(BPP) = VT_I4;
		Width->lVal  = w;
		Overlap->lVal = overlp;
		Height->lVal = h;
		BPP->lVal= bpp;
	}
}

/////////////////////////////////////////////////////////////////////////////
// Perform an opening
// Example:
//      OpenGIP Passes, SrcBuf, DestBuf
// Parameters:
//      Passes is the number of passes to do opening on
//      SrcBuf is the source handle for the image we wish to open
//      DestBuf is the source handle for the image we wish to open
// Returns:
//      Bugger all
// Notes:
//      An error occurs if no GIP
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::OpenGIP(long Passes, GIPImage **SrcBuf, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    
    m_pGIP->Open((int) Passes, SrcBuf, DestBuf);
}

/////////////////////////////////////////////////////////////////////////////
// Perform an closing
// Example:
//      CloseGIP Passes, SrcBuf, DestBuf
// Parameters:
//      Passes is the number of passes to do opening on
//      SrcBuf is the source handle for the image we wish to open
//      DestBuf is the source handle for the image we wish to open
// Returns:
//      Bugger all
// Notes:
//      An error occurs if no GIP
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::CloseGIP(long Passes, GIPImage **SrcBuf, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error

    m_pGIP->Close((int) Passes, SrcBuf, DestBuf);
}

/////////////////////////////////////////////////////////////////////////////
// Perform a Add, Subtract, Multiply, Divide, Diff or Mod
// between 2 images
// Example:
//      ********GIP Src1, Src2, DestBuf
// Parameters:
//      Src1 is the source handle for the image we wish to open
//      Src2 is the source handle for the image we wish to open
//      DestBuf is the source handle for the image we wish to open 
// Returns:
//      Bugger all
// Notes:
//      An error occurs if no GIP
// Add:         Dest = Src1 + Src2
// Subtract:    Dest = Src1 - Src2
// Multiply:    Dest = Src1 * Src2
// Divide:      Dest = Src1 / Src2
// Diff:        Dest = abs(Src1 - Src2)
// Mod:         Dest = sqrt(Src1 * Src1 + Src2 * Src2)
//
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::AddGIP(GIPImage **Src1, GIPImage **Src2, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error

    m_pGIP->Add(Src1, Src2, DestBuf);
}

afx_msg void GPUIPScript::SubtractGIP(GIPImage **Src1, GIPImage **Src2, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error

    m_pGIP->Subtract(Src1, Src2, DestBuf);
}
afx_msg void GPUIPScript::MultiplyGIP(GIPImage **Src1, GIPImage **Src2, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error

    m_pGIP->Multiply(Src1, Src2, DestBuf);
}

afx_msg void GPUIPScript::DivideGIP(GIPImage **Src1, GIPImage **Src2, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error

    m_pGIP->Divide(Src1, Src2, DestBuf);
}

afx_msg void GPUIPScript::DiffGIP(GIPImage **Src1, GIPImage **Src2, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error

    m_pGIP->Diff(Src1, Src2, DestBuf);
}

afx_msg void GPUIPScript::ModGIP(GIPImage **Src1, GIPImage **Src2, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error

    m_pGIP->Mod(Src1, Src2, DestBuf);
}

/////////////////////////////////////////////////////////////////////////////
// Perform on GPU thresholding 
// Example:
//      ThreshGIP SrcBuf, Threshold, DestBuf
// Parameters:
//      Src1 is the source handle for the image we wish to open
//      Threshold is the threshold to use NOTE >= Threshold
//      DestBuf is the source handle for the image we wish to open
// Returns:
//      Bugger all
// Notes:
//      An error occurs if no GIP
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::ThreshGIP(GIPImage **SrcBuf, float Thresh, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error

    m_pGIP->Thresh(SrcBuf, Thresh, DestBuf);
}

/////////////////////////////////////////////////////////////////////////////
// Perform dilation
// Example:
//      DilateGIP Passes, SrcBuf, DestBuf
// Parameters:
//      Passes is the number of passes to do opening on
//      SrcBuf is the source handle for the image we wish to open
//      DestBuf is the source handle for the image we wish to open
// Returns:
//      Bugger all
// Notes:
//      An error occurs if no GIP
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::DilateGIP(long Passes, GIPImage **SrcBuf, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    
    m_pGIP->Dilate((int) Passes, SrcBuf, DestBuf);
}

/////////////////////////////////////////////////////////////////////////////
// Perform an erosion
// Example:
//      ErodeGIP Passes, SrcBuf, DestBuf
// Parameters:
//      Passes is the number of passes to do opening on
//      SrcBuf is the source handle for the image we wish to open
//      DestBuf is the source handle for the image we wish to open
// Returns:
//      Bugger all
// Notes:
//      An error occurs if no GIP
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::ErodeGIP(long Passes, GIPImage **SrcBuf, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error

    m_pGIP->Erode((int) Passes, SrcBuf, DestBuf);
}

/////////////////////////////////////////////////////////////////////////////
// Convert from RGB to HSI space
// Example:
//      RGBHSIGIP SrcBuf, DestBuf
// Parameters:
//      SrcBuf is the source colour handle 
//      DestBuf is the dest colour handle 
// Returns:
//      Bugger all
// Notes:
//      An error occurs if no GIP
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::RGBHSIGIP(GIPImage **SrcBuf, GIPImage **DestBuf)
{
 	long width, height, overl, BPP;
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
 	if (SrcBuf > NULL)
		m_pGIP->GIPGetImageDimensions(SrcBuf, &width, &height, &overl, &BPP);
	if ((BPP !=24) && (BPP !=48))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 8 bit or 24 bit)
    m_pGIP->RGBHSI(SrcBuf, DestBuf);
}

/////////////////////////////////////////////////////////////////////////////
// Convert from RGB to HSI space
// Example:
//      HSIRGBGIP SrcBuf, DestBuf
// Parameters:
//      SrcBuf is the source colour handle 
//      DestBuf is the dest colour handle 
// Returns:
//      Bugger all
// Notes:
//      An error occurs if no GIP
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::HSIRGBGIP(GIPImage **SrcBuf, GIPImage **DestBuf)
{
	long width, height, overl, BPP;
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
 	if (SrcBuf > NULL)
		m_pGIP->GIPGetImageDimensions(SrcBuf, &width, &height, &overl, &BPP);
	if ((BPP !=24) && (BPP != 48))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 8 bit or 24 bit)
	CheckDimGipImage(DestBuf, width, height, BPP);
    
    m_pGIP->HSIRGB(SrcBuf, DestBuf);
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::GreyQuadsToColourGIP(GIPImage **SrcBuf, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    
    m_pGIP->GreyToColour(SrcBuf, DestBuf);
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::ColourToGreyGIP(GIPImage **SrcBuf, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    
    m_pGIP->ColourToGrey(SrcBuf, DestBuf);
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::TestGIP(GIPImage **SrcBuf, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    m_pGIP->Test(SrcBuf, DestBuf);
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::Technique(const TCHAR *Name, GIPImage **SrcBuf, GIPImage **DestBuf, long SetPixConst)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    m_pGIP->ApplyTechnique((char *)Name, SrcBuf, DestBuf, SetPixConst);
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::ColourTechnique(const TCHAR *Name, GIPImage **SrcBuf, GIPImage **DestBuf, long SetPixConst)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    m_pGIP->ApplyColourTechnique((char *)Name, SrcBuf, DestBuf, SetPixConst);
}

/////////////////////////////////////////////////////////////////////////////
// Apply a maths technique to an image
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::MathsTechnique(const TCHAR *Name, GIPImage **Src1, GIPImage **Src2, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    m_pGIP->MathsTechnique((char *)Name, Src1, Src2, DestBuf);
}

/////////////////////////////////////////////////////////////////////////////
// Ping Pong two techniques for nPasses
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::PongColourTechnique(const TCHAR *Name1, const TCHAR *Name2, long nPasses, GIPImage **SrcBuf,
											  GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    m_pGIP->ColourPong((char *)Name1, (char *)Name2, nPasses, SrcBuf, DestBuf);
}

/////////////////////////////////////////////////////////////////////////////
// Perform a multipass technique
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::MultipassTechnique(const TCHAR *Name, GIPImage **SrcBuf, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    m_pGIP->MultipassTechnique((char *)Name, SrcBuf, DestBuf, TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// Split a composite RGB image into R, G abd B components
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::SplitComponents(GIPImage **Src, GIPImage **Targ1, GIPImage **Targ2, GIPImage **Targ3)
{
	long width, height, overl, BPP;
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
	if (Src > NULL)
		m_pGIP->GIPGetImageDimensions(Src, &width, &height, &overl, &BPP);
	if ((BPP !=24) && (BPP != 48))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 8 bit or 24 bit)
	CheckDimGipImage(Targ1, width, height, BPP);
	CheckDimGipImage(Targ2, width, height, BPP);
	CheckDimGipImage(Targ3, width, height, BPP);
    m_pGIP->ComponentSplit(Src, Targ1, Targ2, Targ3);
}

/////////////////////////////////////////////////////////////////////////////
// Combine R, G and B components into a composite RGB image
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::JoinComponents(GIPImage **Src1, GIPImage **Src2, GIPImage **Src3, GIPImage **Target)
{
	long width, height, overl, BPP;
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
	if (Src1 > NULL)
		m_pGIP->GIPGetImageDimensions(Src1, &width, &height, &overl, &BPP);
	if ((BPP !=24) && (BPP != 48))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 8 bit or 24 bit)
	CheckDimGipImage(Src2, width, height, BPP);
	CheckDimGipImage(Src3, width, height, BPP);
	CheckDimGipImage(Target, width, height, BPP);
    m_pGIP->ComponentJoin(Src1, Src2, Src3, Target);
}

/////////////////////////////////////////////////////////////////////////////
// Threshold a colour image on each individual R, G or B component
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::ComponentThreshold(float Rmin, float Rmax, float Gmin, float Gmax, float Bmin, float Bmax,
											 GIPImage **Src, GIPImage **Targ)
{
	long width, height, overl, BPP;
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
 	if (Src > NULL)
		m_pGIP->GIPGetImageDimensions(Src, &width, &height, &overl, &BPP);
	if ((BPP !=24) && (BPP != 48))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 8 bit or 24 bit)
	CheckDimGipImage(Targ, width, height, BPP);
    m_pGIP->ComponentThreshold(Rmin, Rmax, Gmin, Gmax, Bmin, Bmax, Src, Targ);
}

afx_msg void GPUIPScript::BlendComponents(GIPImage **Src1, float A1, GIPImage **Src2, float A2, GIPImage **Src3, float A3,
										  GIPImage **Target)
{
 	long width, height, overl, BPP;
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
	if (Src1 > NULL)
		m_pGIP->GIPGetImageDimensions(Src1, &width, &height, &overl, &BPP);
	if ((BPP !=24) && (BPP != 48))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 8 bit or 24 bit)
	CheckDimGipImage(Src2, width, height, BPP);
	CheckDimGipImage(Src3, width, height, BPP);
	CheckDimGipImage(Target, width, height, BPP);
    m_pGIP->ComponentBlend(Src1, A1, Src2, A2, Src3, A3, Target);
}

/////////////////////////////////////////////////////////////////////////////
// Gaussian blurring
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::GaussianBlur(long FiltHW, float Sigma, GIPImage **Src, GIPImage **Dest)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    m_pGIP->GaussianBlur(FiltHW, (float)Sigma, Src, Dest);
}

/////////////////////////////////////////////////////////////////////////////
// Difference Of Gaussians
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::DOG(long Size1, long Size2, GIPImage **Src, GIPImage **Dest)
{
	if (!m_bGIPCreated)
		AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
	m_pGIP->DOG(Size1, Size2, Src, Dest);
}

/////////////////////////////////////////////////////////////////////////////
// Unsharp mask
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::UnsharpMask(long FiltHW, float Sigma, float Amount, GIPImage **Src, GIPImage **Dest)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    m_pGIP->UnsharpMask(FiltHW, (float)Sigma, (float)Amount, Src, Dest);
}

/////////////////////////////////////////////////////////////////////////////
// NN Deconvolution
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::NearestNeighbDeconv(long FilterHW, float Sigma, float Amount, VARIANT FAR* Images, VARIANT FAR* DestImages)
{
	BYTE		*Addr;
    int			Ret = IDS_OPOK;
	SAFEARRAY	*pSafe1, *pSafe2;
	VARIANT		V;
    typedef		BYTE *ImageAddr;
    ImageAddr	*ImageBytes, *DestBytes;
	long		i, uB1, lB1, uB2, lB2, Handle;
	long		W, H, BPP, iW, iH, iP, iBPP;


   if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
	// Get and check array is 1D
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Images, &pSafe1, 1), return);
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(DestImages, &pSafe2, 1), return);

    // Get the bounds of the array Coefficients
    SafeArrayGetLBound(pSafe1, 1, &lB1);
    SafeArrayGetUBound(pSafe1, 1, &uB1);
    SafeArrayGetLBound(pSafe2, 1, &lB2);
    SafeArrayGetUBound(pSafe2, 1, &uB2);
	if ((uB1 - lB1 < 3) || (uB2 - lB2 < 3)|| ((uB1 - lB1)!= (uB2 - lB2)))
		Ret = IDS_ARRAYDIMENSIONS;
	else
	{
		ImageBytes=new ImageAddr[uB1-lB1];
		DestBytes=new ImageAddr[uB2-lB2];
		for (i = lB1; i < (uB1 - lB1) && Ret == IDS_OPOK; i++)
		{
			// Get the handle of the input image
			SafeArrayGetElement(pSafe1, &i, &V);

			switch (V.vt)
			{
				case VT_I2:
					Handle = V.iVal;
					break;
				case VT_I4:
					Handle = V.lVal;
					break;
				default:
					Handle = UTS_ERROR;
			}
			if (UTSVALID_HANDLE(Handle))
			{
				iBPP=IP().UTSMemRect()->GetExtendedInfo(Handle, &iW, &iH, &iP, &Addr);
				if (i==lB1)
				{
					W=iW;
					H=iH;
					BPP=iBPP;
					ImageBytes[i-lB1]=Addr;
				}
				else if ((iBPP != BPP) || (iW != W) || (iH != H))
				{
					ImageBytes[i-lB1]=NULL;
					Ret = IDS_OPERATIONFAILED;
				}
				else
					ImageBytes[i-lB1]=Addr;
			}
			else
				Ret = IDS_INVALIDHANDLE;                        // SrcImage handle is invalid
		}
		for (i = lB2; i < (uB2 - lB2) && Ret == IDS_OPOK; i++)
		{
			// Get the handle of the destination images
			SafeArrayGetElement(pSafe2, &i, &V);

			switch (V.vt)
			{
				case VT_I2:
					Handle = V.iVal;
					break;
				case VT_I4:
					Handle = V.lVal;
					break;
				default:
					Handle = UTS_ERROR;
			}
			if (UTSVALID_HANDLE(Handle))
			{
				iBPP=IP().UTSMemRect()->GetExtendedInfo(Handle, &iW, &iH, &iP, &Addr);
				 if ((iBPP != BPP) || (iW != W) || (iH != H))
				{
					DestBytes[i-lB2]=NULL;
					Ret = IDS_OPERATIONFAILED;
				}
				else
					DestBytes[i-lB2]=Addr;
			}
			else
				Ret = IDS_INVALIDHANDLE;                        // SrcImage handle is invalid
		}

		// All o.k. ?
		if (!m_bGIPCreated)
			AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
		if (Ret== IDS_OPOK)
			m_pGIP->NNDeconv(FilterHW, Sigma, Amount, uB1 - lB1, W, H, BPP, ImageBytes, DestBytes);
		delete ImageBytes;
		delete DestBytes;
	}
	if (Ret !=IDS_OPOK)
		AfxThrowOleDispatchException(0xFF, Ret, 0);
}

/////////////////////////////////////////////////////////////////////////////
// Median filter
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::MedianGIP(long Passes, GIPImage **SrcBuf, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    
    m_pGIP->Median((int) Passes, SrcBuf, DestBuf);
}

/////////////////////////////////////////////////////////////////////////////
// Kuwahara filter - FiltHW = 1 or 2
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::Kuwahara(long FiltHW, GIPImage **Src, GIPImage **Dest)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    m_pGIP->Kuwahara(FiltHW, Src, Dest);
}

/////////////////////////////////////////////////////////////////////////////
// N by N convolution
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::ConvolveGIP(long nPasses, VARIANT FAR *Coeff, GIPImage **Src, GIPImage **Dest)
{
    SAFEARRAY   *pSafe;
    long        i, j, idx[2];
    long        lB1, uB1, lB2, uB2, R1, R2;
	long		width, height, overl, BPP;
    float       *pData;
    VARIANT     V;
    int         HW;

    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error

 	if (Src > NULL)
		m_pGIP->GIPGetImageDimensions(Src, &width, &height, &overl, &BPP);
	if ((BPP !=8) && (BPP !=16) && (BPP !=24) && (BPP != 48))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 8 bit or 24 bit)
	CheckDimGipImage(Dest, width, height, BPP);
    // Get and check coefficients are 2D
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Coeff, &pSafe, 2), return);
        
    // Get the bounds of the array Coefficients
    SafeArrayGetLBound(pSafe, 1, &lB1);
    SafeArrayGetUBound(pSafe, 1, &uB1);
    SafeArrayGetLBound(pSafe, 2, &lB2);
    SafeArrayGetUBound(pSafe, 2, &uB2);
	R2 = uB2 - lB2;
	R1 = uB1 - lB1;

    if (R2 != R1)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETERVALUE, 0);

    // Check handle is valid
    // Unpack the coefficients array 
    pData = new float[R1 * R2];
    for (i = lB1; i < uB1; i++)
    {
        for (j = lB2; j < uB2; j++)
        {
            idx[0] = i;
            idx[1] = j;
            SafeArrayGetElement(pSafe, idx, &V);
         
            pData[i * R2 + j] = (float)VariantDbl(V);
        }
    }

    HW = (R1 - 1) / 2;
    // Do the convolution
    m_pGIP->Convolve(nPasses, HW, pData, Src, Dest);

    // Clear up
    delete pData;
}


afx_msg void GPUIPScript::ConvolveTechnique(const TCHAR *Name, long nPasses, VARIANT FAR *Coeff, GIPImage **Src, GIPImage **Dest)
{
    SAFEARRAY   *pSafe;
    long        i, j, idx[2];
    long        lB1, uB1, lB2, uB2, R1, R2;
	long		width, height, overl, BPP;
    float       *pData;
    VARIANT     V;
    int         HW;

	if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
 	if (Src > NULL)
		m_pGIP->GIPGetImageDimensions(Src, &width, &height, &overl, &BPP);
	if ((BPP !=8) && (BPP !=16) && (BPP !=24) && (BPP != 48))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 8 bit or 24 bit)
	CheckDimGipImage(Dest, width, height, BPP);

    // Get and check coefficients are 2D
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Coeff, &pSafe, 2), return);
        
    // Get the bounds of the array Coefficients
    SafeArrayGetLBound(pSafe, 1, &lB1);
    SafeArrayGetUBound(pSafe, 1, &uB1);
    SafeArrayGetLBound(pSafe, 2, &lB2);
    SafeArrayGetUBound(pSafe, 2, &uB2);
	R2 = uB2 - lB2;
	R1 = uB1 - lB1;

    if (R2 != R1)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETERVALUE, 0);

    // Check handle is valid
    // Unpack the coefficients array 
    pData = new float[R1 * R2];
    for (i = lB1; i < uB1; i++)
    {
        for (j = lB2; j < uB2; j++)
        {
            idx[0] = i;
            idx[1] = j;
            SafeArrayGetElement(pSafe, idx, &V);
         
            pData[i * R2 + j] = (float)VariantDbl(V);
        }
    }

    HW = (R1 - 1) / 2;
    // Do the convolution
    m_pGIP->ConvolveTechnique(Name, nPasses, HW, pData, Src, Dest);

    // Clear up
    delete pData;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::SimpleBlend(VARIANT FAR* Images, GIPImage **Dest)
{
    int Ret = IDS_OPOK;
	SAFEARRAY *pSafe;
	VARIANT V;
	long i, uB, lB, Handle;

	if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
  
	// Get and check array is 1D
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Images, &pSafe, 1), return);

    // Get the bounds of the array Coefficients
    SafeArrayGetLBound(pSafe, 1, &lB);
    SafeArrayGetUBound(pSafe, 1, &uB);
    if (uB - lB < 2)
        Ret = IDS_ARRAYDIMENSIONS;
	BYTE** pImages=(BYTE **)new long[uB-lB];
	for (i = lB; i < (uB - lB) && Ret == IDS_OPOK; i++)
	{
        // Get the handle of the input image
        SafeArrayGetElement(pSafe, &i, &V);

        switch (V.vt)
        {
			case VT_I2:
				Handle = V.iVal;
				break;
			case VT_I4:
				Handle = V.lVal;
				break;
			default:
				Handle = UTS_ERROR;
		}
		pImages[i-lB]=(BYTE *)Handle;
	}
	if (Ret== IDS_OPOK)
		m_pGIP->SimpleBlend(uB-lB, pImages, Dest);
	delete pImages;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::HDRBlend(VARIANT FAR* Images, GIPImage **Dest)
{
    int Ret = IDS_OPOK;
	SAFEARRAY *pSafe;
	VARIANT V;
	long i, uB, lB, Handle;

	if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
  
	// Get and check array is 1D
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Images, &pSafe, 1), return);

    // Get the bounds of the array Coefficients
    SafeArrayGetLBound(pSafe, 1, &lB);
    SafeArrayGetUBound(pSafe, 1, &uB);
    if (uB - lB < 2)
        Ret = IDS_ARRAYDIMENSIONS;
	BYTE** pImages=(BYTE **)new long[uB-lB];
	for (i = lB; i < (uB - lB) && Ret == IDS_OPOK; i++)
	{
        // Get the handle of the input image
        SafeArrayGetElement(pSafe, &i, &V);

        switch (V.vt)
        {
			case VT_I2:
				Handle = V.iVal;
				break;
			case VT_I4:
				Handle = V.lVal;
				break;
			default:
				Handle = UTS_ERROR;
		}
		pImages[i-lB]=(BYTE *)Handle;
	}
	if (Ret== IDS_OPOK)
		m_pGIP->HDRBlend(uB-lB, pImages, Dest);
	delete pImages;
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

afx_msg void GPUIPScript::RangeBlend(VARIANT FAR* Images, GIPImage **Dest)
{
    int Ret = IDS_OPOK;
	SAFEARRAY *pSafe;
	VARIANT V;
	long i, uB, lB, Handle;

	if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
  
	// Get and check array is 1D
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Images, &pSafe, 1), return);

    // Get the bounds of the array Coefficients
    SafeArrayGetLBound(pSafe, 1, &lB);
    SafeArrayGetUBound(pSafe, 1, &uB);
    if (uB - lB < 2)
        Ret = IDS_ARRAYDIMENSIONS;
	BYTE** pImages=(BYTE **)new long[uB-lB];
	for (i = lB; i < (uB - lB) && Ret == IDS_OPOK; i++)
	{
        // Get the handle of the input image
        SafeArrayGetElement(pSafe, &i, &V);

        switch (V.vt)
        {
			case VT_I2:
				Handle = V.iVal;
				break;
			case VT_I4:
				Handle = V.lVal;
				break;
			default:
				Handle = UTS_ERROR;
		}
		pImages[i-lB]=(BYTE *)Handle;
	}
	if (Ret== IDS_OPOK)
		m_pGIP->RangeBlend(uB-lB, pImages, Dest);
	delete pImages;
}


afx_msg void GPUIPScript::CombineMultiExposures(long Type, VARIANT FAR* Images, long OutputImage)
{
  	BYTE *DestAddr, *Addr;
    long W, H, P, BPP, iW, iH, iP, iBPP;
    long uB, lB, Handle;
    long  i;
    SAFEARRAY *pSafe;
    typedef BYTE *ImageAddr;
    ImageAddr *ImageBytes;
    VARIANT V;
    int Ret = IDS_OPOK;

     // Get and check array is 1D
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Images, &pSafe, 1), return);

    // Get the bounds of the array Coefficients
    SafeArrayGetLBound(pSafe, 1, &lB);
    SafeArrayGetUBound(pSafe, 1, &uB);

    // Addresses of the input images
    ImageBytes = new ImageAddr[uB - lB];

    // Get the image depth and mask sure images are 8 bits per pixel
	BPP = IP().UTSMemRect()->GetExtendedInfo(OutputImage, &W, &H, &P, &DestAddr);
	if ((BPP != 8) && (BPP != 16)) Ret = IDS_OPERATIONFAILED;
   // Check we have enough images for the blend
    if (uB - lB < 2)
        Ret = IDS_ARRAYDIMENSIONS;

    // Now check our input images and get the addresses of their image data
 	if (Ret == IDS_OPOK)
	{
		for (i = lB; i < (uB - lB) && Ret == IDS_OPOK; i++)
		{
			// Get the handle of the input image
			SafeArrayGetElement(pSafe, &i, &V);

			switch (V.vt)
			{
			case VT_I2:
				Handle = V.iVal;
				break;
			case VT_I4:
				Handle = V.lVal;
				break;
			default:
				Handle = UTS_ERROR;
			}

			// Do we have a valid handle in our array of handles ?
			if (UTSVALID_HANDLE(Handle))
			{
				// Yes it is so check its dimensions compared to our output image
				iBPP = IP().UTSMemRect()->GetExtendedInfo(Handle, &iW, &iH, &iP, &Addr);

				if (iBPP != BPP || iW != W || iH != H)
				{
					ImageBytes[i - lB] = NULL;
					Ret = IDS_OPERATIONFAILED;
				}
				else
					ImageBytes[i - lB] = Addr;
			}
			else
				Ret = IDS_INVALIDHANDLE;                        // SrcImage handle is invalid
		}
	}
    // All o.k. ?
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
	if (Ret == IDS_OPOK)
		m_pGIP->BlendMultipleExposures(Type, uB - lB, (BYTE **)ImageBytes, W, H, BPP, DestAddr);
	delete ImageBytes;

    if (Ret != IDS_OPOK)
        AfxThrowOleDispatchException(0xFF, Ret, 0);         // Flag the error to the script
}

afx_msg long GPUIPScript::Histogram(GIPImage **gpImage,  long ColourMode)
{

    long histHandle = 0, histsize;
	long x, y, width, height, overlap, BPP;
    DWORD *hptr;

    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
	if (gpImage > NULL)
		m_pGIP->GIPGetImageDimensions(gpImage, &width, &height, &overlap, &BPP);
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
	if ((BPP != 8) && (BPP != 16) && (BPP != 24))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Invalid Image
	else
	{
		if (BPP==16) histsize=65536;
			else histsize=256;

		// Allocate a rectangle to store the info in 32 bits, 256 or 65536 values 
		histHandle = IP().UTSMemRect()->Create(32, histsize, 1);
        if (UTSVALID_HANDLE(histHandle))
		{
			IP().UTSMemRect()->GetExtendedInfo(histHandle, &x, &y, &BPP, &hptr);
			// Do the Histogram
			m_pGIP->Histogram(gpImage, (long *)hptr, (int)ColourMode);               
		}
		else
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);   // Mem allocation error
    }
    return histHandle;
}

afx_msg void GPUIPScript::AddConstGIP(GIPImage **Src1, float val, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
    m_pGIP->AddConst(Src1, val, DestBuf);
}

afx_msg void GPUIPScript::SubtractConstGIP(GIPImage **Src1, float val, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
	m_pGIP->SubtractConst(Src1, val, DestBuf);
}

afx_msg void GPUIPScript::MultiplyConstGIP(GIPImage **Src1, float val, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
    m_pGIP->MultiplyConst(Src1, val, DestBuf);
}

afx_msg void GPUIPScript::DivideConstGIP(GIPImage **Src1, float val, GIPImage **DestBuf)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error

   m_pGIP->DivideConst(Src1, val, DestBuf);
}

afx_msg void GPUIPScript::CopyGIP(GIPImage **Src1, GIPImage **DestBuf)
{
	long width, height, overl, bpp;
 	long width1, height1, overl1, bpp1;
   if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);			// We have no GIP so kick up an error
	if ((Src1 > NULL) && (DestBuf > 0))
	{
		m_pGIP->GIPGetImageDimensions(Src1, &width, &height, &overl, &bpp);
		m_pGIP->GIPGetImageDimensions(DestBuf, &width1, &height1, &overl1, &bpp1);
		if ((width != width1) || (height != height1) || (overl != overl1))
			AfxThrowOleDispatchException(0xFF, IDS_GIPSIZEMISMATCH, 0);     // Width and height mismatch
		if ((((bpp==8) || (bpp==16)) && (bpp1 != 8) && (bpp1 != 16)) ||
			(((bpp==24) || (bpp==48)) && (bpp1 != 24) && (bpp1 != 48)))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image
		m_pGIP->Copy(Src1, DestBuf);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

afx_msg long GPUIPScript::CropImageGIP(GIPImage **Src, long x0, long y0, long w0, long h0, long overl)
{
    long HandleGipImage;

	if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
	if (Src !=NULL)
	{
		HandleGipImage=(long)m_pGIP->ImageCrop(Src, x0, y0, w0, h0, overl);
        if (!HandleGipImage)
			AfxThrowOleDispatchException(0xFF, IDS_NOGIPIMAGECREATED, 0);            // We have no GIP so kick up an error
		return HandleGipImage;
	}
	return -1L;
}

afx_msg long GPUIPScript::ResizeImageGIP(GIPImage **Src, long w0, long h0, long overl)
{
    long HandleGipImage;

	if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
	if (Src !=NULL)
	{
		HandleGipImage=(long)m_pGIP->ImageResize(Src, w0, h0, overl);
        if (!HandleGipImage)
			AfxThrowOleDispatchException(0xFF, IDS_NOGIPIMAGECREATED, 0);            // We have no GIP so kick up an error
		return HandleGipImage;
	}
	return -1L;
}

afx_msg void GPUIPScript::ColourDeconvGIP(GIPImage **Src, VARIANT FAR* pVCV1, VARIANT FAR* pVCV2, VARIANT FAR* pVCV3, GIPImage **Dest) 
{
	long		width, height, overl, BPP;
 	double		pCV1[3], pCV2[3], pCV3[3];
	SAFEARRAY   *pSafe;
    long        lB, uB, i;
    VARIANT     Var;
    int			Ret = IDS_OPOK;

    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
 	if (Src > NULL)
		m_pGIP->GIPGetImageDimensions(Src, &width, &height, &overl, &BPP);
	if ((BPP !=24) && (BPP != 48))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 8 bit or 24 bit)
	CheckDimGipImage(Dest, width, height, BPP);
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(pVCV1, &pSafe, 1), return);
    SafeArrayGetLBound(pSafe, 1, &lB);
	SafeArrayGetUBound(pSafe, 1, &uB);
	if (uB - lB != 3) Ret = IDS_ARRAYDIMENSIONS;
	if (Ret==IDS_OPOK)
		for (i=lB; i<uB; i++)
		{
			SafeArrayGetElement(pSafe, &i, &Var);
			pCV1[i-lB] = VariantDbl(Var);
		}
	if (Ret==IDS_OPOK)
	{
		CHECK_SAFEARR_EXCEPTION(GetArrayInfo(pVCV2, &pSafe, 1), return);
		SafeArrayGetLBound(pSafe, 1, &lB);
		SafeArrayGetUBound(pSafe, 1, &uB);
		if (uB - lB != 3) Ret = IDS_ARRAYDIMENSIONS;
		if (Ret==IDS_OPOK)
			for (i=lB; i<uB; i++)
			{
				SafeArrayGetElement(pSafe, &i, &Var);
				pCV2[i-lB] = VariantDbl(Var);
			}
	}
 	if (Ret==IDS_OPOK)
	{
		CHECK_SAFEARR_EXCEPTION(GetArrayInfo(pVCV3, &pSafe, 1), return);
		SafeArrayGetLBound(pSafe, 1, &lB);
		SafeArrayGetUBound(pSafe, 1, &uB);
		if (uB - lB != 3) Ret = IDS_ARRAYDIMENSIONS;
		if (Ret==IDS_OPOK)
			for (i=lB; i<uB; i++)
			{
				SafeArrayGetElement(pSafe, &i, &Var);
				pCV3[i-lB] = VariantDbl(Var);
			}
	}
	if (Ret==IDS_OPOK) m_pGIP->ColourDeconv(Src, pCV1, pCV2, pCV3, Dest);
    if (Ret != IDS_OPOK)
        AfxThrowOleDispatchException(0xFF, Ret, 0);         // Flag the error to the script
}

afx_msg long GPUIPScript::FFT2DPowerSpectrumGIP(GIPImage **Src)
{
    long HandleGipImage=NULL;
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
	CheckDimGipImage(Src, -1, -1, 128);			// soure image should be result of an FFT instruction (BPP==128)
	HandleGipImage=(long)m_pGIP->fft2DPowerSpectrum (Src);
	if (!HandleGipImage)
		AfxThrowOleDispatchException(0xFF, IDS_NOGIPIMAGECREATED, 0);            // We have no GIP so kick up an error
	return HandleGipImage;
}

afx_msg long GPUIPScript::FFT2DGIP( GIPImage **SrcHandle, long flag, long renderTarget)
{
	long HandleGipImage=NULL;
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
	if (flag >= 0)
 		CheckDimGipImage(SrcHandle, -1, -1, 128);						// soure image should be result of an FFT instruction (BPP==128)
	HandleGipImage=(long)m_pGIP->fft2D(SrcHandle, flag, renderTarget);
	if (!HandleGipImage)
		AfxThrowOleDispatchException(0xFF, IDS_NOGIPIMAGECREATED, 0);            // We have no GIP so kick up an error
	return HandleGipImage;
}

afx_msg long GPUIPScript::ButterWorthFFT2DGIP(GIPImage **SrcHandle, long filtType, float f1, float f2, float order)
{
	long HandleGipImage=NULL;
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
	HandleGipImage=(long)m_pGIP->fft2DButterWorth(SrcHandle, filtType, f1, f2, order);
	return HandleGipImage;
}

afx_msg long GPUIPScript::GaussianFFT2DGIP(GIPImage **SrcHandle, long filtType, float f1, float f2)
{
	long HandleGipImage=NULL;
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
	HandleGipImage=(long)m_pGIP->fft2DGaussian(SrcHandle, filtType, f1, f2);
	if (!HandleGipImage)
		AfxThrowOleDispatchException(0xFF, IDS_NOGIPIMAGECREATED, 0);            // No image Created
	return HandleGipImage;
}

afx_msg void GPUIPScript::FFTImageRegistrationGIP(GIPImage **Src1, GIPImage **Src2, VARIANT FAR*  XShift, VARIANT FAR*  YShift, 
												  VARIANT FAR*  sn, long MaxShift)
{
	float	SignalNoise;
	long	xshift, yshift, width, height, overlap, BPP;

    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
	if (Src1 > NULL)
		m_pGIP->GIPGetImageDimensions(Src1, &width, &height, &overlap, &BPP);
	if ((BPP != 8) && (BPP != 16) && (BPP != 24) && (BPP != 48))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Invalid Image
	CheckDimGipImage(Src2, width, height, BPP);
	m_pGIP->FFTImageRegistration(Src1, Src2, &xshift, &yshift, &SignalNoise, MaxShift);
	V_VT(XShift)  = VT_I4;
	V_VT(YShift) = VT_I4;
	XShift->lVal  = xshift;
	YShift->lVal = yshift;
	V_VT(sn) = VT_R4;
	sn->fltVal=SignalNoise;
}

afx_msg void GPUIPScript::CoreFFTImageRegistrationGIP(GIPImage **FFTSrc1, GIPImage **FFTSrc2, VARIANT FAR*  XShift, VARIANT FAR*  YShift, 
													  VARIANT FAR*  sn, long MaxShift)
{
	float	SignalNoise;
	long	xshift, yshift, width, height, overlap, BPP;

    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
	if (FFTSrc1 > NULL)
		m_pGIP->GIPGetImageDimensions(FFTSrc1, &width, &height, &overlap, &BPP);
	if (BPP != 128)
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Invalid Image
	CheckDimGipImage(FFTSrc2, width, height, BPP);
	m_pGIP->CoreFFTImageRegistration(FFTSrc1, FFTSrc2, &xshift, &yshift, &SignalNoise, MaxShift);
	V_VT(XShift)  = VT_I4;
	V_VT(YShift) = VT_I4;
	XShift->lVal  = xshift;
	YShift->lVal = yshift;
	V_VT(sn) = VT_R4;
	sn->fltVal=SignalNoise;
}

afx_msg void GPUIPScript::DistanceMapGIP(GIPImage **src, GIPImage **dest, BOOL EDMOnly, BOOL IsWhiteBackground, float NormFactor)
{
	long width, height, overlap, BPP, width1, height1, overlap1, BPP1;
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
	if ((src > NULL) && (dest >NULL))
	{
		m_pGIP->GIPGetImageDimensions(src, &width, &height, &overlap, &BPP);
		m_pGIP->GIPGetImageDimensions(dest, &width1, &height1, &overlap1, &BPP1);
		if ((BPP != 8) && (BPP != 16))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 8 bit )
		if (((BPP==8)&& (BPP1 != 24)) || ((BPP==16) && (BPP1 != 48)))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image
		if ((width != width1) || (height != height1))
			AfxThrowOleDispatchException(0xFF, IDS_GIPSIZEMISMATCH, 0);     // Width and height mismatch
		m_pGIP->DistanceMap(src, dest, EDMOnly, IsWhiteBackground, NormFactor);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

afx_msg void GPUIPScript::CannyEdgeGIP(GIPImage **src, GIPImage **dest, float thresholdLow, float thresholdHigh)
{
	long width, height, overlap, BPP;
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
	if (src > NULL)
		m_pGIP->GIPGetImageDimensions(src, &width, &height, &overlap, &BPP);
	if ((BPP != 8) && (BPP !=16) && (BPP != 24) && (BPP !=48))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 8 bit )
	m_pGIP->CannyEdge(src, dest, thresholdLow, thresholdHigh);
}

afx_msg void GPUIPScript::HoughLinesGIP(GIPImage **src, GIPImage **dest, const TCHAR *angles, long minlength, long connection)
{
	long width, height, overlap, BPP;
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
	if (src > NULL)
		m_pGIP->GIPGetImageDimensions(src, &width, &height, &overlap, &BPP);
	if (BPP != 24)
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 8 bit )
	CheckDimGipImage(dest, width, height, BPP);							// Result should be 24 bit
	m_pGIP->HoughLines(src, dest, (char *)angles, minlength, connection);
}

//-----------------------------------------------------------------------------------------------
// FFTImageStitch
//	- Source1 and Source2 are valid UTS handles
//	- mode 0 - aline top part src1 with bottom part src2
//		   1 - aline right part src1 with left part src2
//         2 - aline bottom part src1 with top part src2
//		   3 - aline left part src1 with right part src2
//	- overlap specifies maximum possible overlap in the direction specified by the mode
//  - XShift and YShift will contain the shifts necessary to aline Source2 with Source1
//  - sn is the ratio peakvalue at (Xshift, Yshift) and the average value in FFT product
//-----------------------------------------------------------------------------------------------
afx_msg void GPUIPScript::FFTImageStitch(long Source1, long Source2, long mode, long overlap, VARIANT FAR*  XShift, VARIANT FAR*  YShift,
										VARIANT FAR*  sn)
{
	float	SignalNoise;
	long	xshift, yshift, width1, height1, pitch1, BPP1;
	long	width2, height2, pitch2, BPP2;
	void	*SrcAddr1, *SrcAddr2;

	if (!m_bGIPCreated)
		AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
	if (Source1>NULL)
		BPP1 = IP().UTSMemRect()->GetExtendedInfo(Source1, &width1, &height1, &pitch1, &SrcAddr1);
	else BPP1=-1;
	if (width1 % 4)
		AfxThrowOleDispatchException(0xFF, IDS_IMAGESIZEDIV4, 0);           // Not divisable by 4 
	if (Source2>NULL)
		BPP2 = IP().UTSMemRect()->GetExtendedInfo(Source2, &width2, &height2, &pitch2, &SrcAddr2);
	else BPP2=-2;
	if ((BPP1 != BPP2) || (width1!=width2) || (height1 !=height2))
        AfxThrowOleDispatchException(0xFF, IDS_GIPSIZEMISMATCH, 0);         // Width and height mismatch
	if (BPP1==8)
		m_pGIP->FFTImageStitch((BYTE *)SrcAddr1, (BYTE *)SrcAddr2, width1, height1, mode, overlap, &xshift, &yshift, &SignalNoise);
	else if (BPP1==16)
		m_pGIP->FFTImageStitch((unsigned short *)SrcAddr1, (unsigned short *)SrcAddr2, width1, height1, mode, overlap, &xshift, &yshift, &SignalNoise);
	else if (BPP1==24)
		m_pGIP->FFTImageStitch((RGBTRIPLE *)SrcAddr1, (RGBTRIPLE *)SrcAddr2, width1, height1, mode, overlap, &xshift, &yshift, &SignalNoise);
	else if (BPP1==48)
		m_pGIP->FFTImageStitch((RGB16BTRIPLE *)SrcAddr1, (RGB16BTRIPLE *)SrcAddr2, width1, height1, mode, overlap, &xshift, &yshift, &SignalNoise);
	V_VT(XShift)  = VT_I4;
	V_VT(YShift) = VT_I4;
	XShift->lVal  = xshift;
	YShift->lVal = yshift;
	V_VT(sn) = VT_R4;
	sn->fltVal=SignalNoise;
}

//-----------------------------------------------------------------------------------------------
// FFTImageStitchGIP
//	- src1 and src2 are GIPImages
//	- overlap specifies maximum possible overlap in the direction specified by mode
//	- mode 0 - aline top part src1 with bottom part src2
//		   1 - aline right part src1 with left part src2
//         2 - aline bottom part src1 with top part src2
//		   3 - aline left part src1 with right part src2
//	- overlap specifies maximum possible overlap in the direction specified by the mode
//  - XShift and YShift will contain the shifts necessary to aline Source2 with Source1
//  - sn is the ratio peakvalue at (Xshift, Yshift) and the average value in FFT product
//-----------------------------------------------------------------------------------------------
afx_msg void GPUIPScript::FFTImageStitchGIP(GIPImage **src1, GIPImage **src2, long mode, long overlap, VARIANT FAR*  XShift, VARIANT FAR*  YShift,
											 VARIANT FAR*  sn)
{
	float	SignalNoise;
	long	xshift, yshift, width, height, overl, BPP;

    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
	if (src1 > NULL)
		m_pGIP->GIPGetImageDimensions(src1, &width, &height, &overl, &BPP);
	if ((BPP != 8) && (BPP != 16) && (BPP !=24) && (BPP !=48))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 8 bit or 24 bit)
	CheckDimGipImage(src2, width, height, BPP);							// Images should be the same
	m_pGIP->FFTImageStitchGIP(src1, src2, mode, overlap, &xshift, &yshift, &SignalNoise);
	V_VT(XShift)  = VT_I4;
	V_VT(YShift) = VT_I4;
	XShift->lVal  = xshift;
	YShift->lVal = yshift;
	V_VT(sn) = VT_R4;
	sn->fltVal=SignalNoise;
}

//---------------------------------------------------------------------------------------------
// Perform Local Binary Pattern texture analysis on grey image, generating only uniform LBP codes
// P=8,R=radius 
// Recommend you call this with radius=4, noiseThresh=16
//---------------------------------------------------------------------------------------------

afx_msg void GPUIPScript::GIPLBP(GIPImage **src, GIPImage **dest, float radius, long noseThresh)
{
    if (!m_bGIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error

	long width, height, overl, BPP;

	if (src > NULL)
		m_pGIP->GIPGetImageDimensions(src, &width, &height, &overl, &BPP);
	if ((BPP != 8) && (BPP != 16) && (BPP !=24) && (BPP !=48))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 8 bit or 24 bit)
	
	CheckDimGipImage(dest, width, height, BPP);							// Images should be the same

	m_pGIP->LBP(src, dest, radius, noseThresh);
}
