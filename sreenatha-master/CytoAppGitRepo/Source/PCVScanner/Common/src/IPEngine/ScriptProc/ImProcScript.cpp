/////////////////////////////////////////////////////////////////////////////
// ImprocScript.cpp : implementation file
//
// This class is an interface to image processing routines NOT in the
// UTS or WOOLZ libraries at present.
//
// Written By Karl Ratcliff 03102001
//
//////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
// 

#include "stdafx.h"
#include "vcmemrect.h"
#include "commonscript.h"
#include "scriptproc.h"

#include "quantilemeasure.h"
#include "vcblob.h"
#include "vcgeneric.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "safearr.h"
#include "cfft.h"
#include "cdct.h"
#include "woolzif.h"
#include "vcthresh.h"
#include "vcImProc.h"
#include "cthin.h"
#include "uts.h"
#include "vcsegment.h"
#include "vcspatialfilter.h"
#include "ImprocScript.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ImprocScript

IMPLEMENT_DYNCREATE(ImprocScript, CCmdTarget)

ImprocScript::ImprocScript()
{
	EnableAutomation();
    // By default work in interactive mode
    m_ExecutionMode = modeDebug;
}

ImprocScript::~ImprocScript()
{
}

BEGIN_MESSAGE_MAP(ImprocScript, CCmdTarget)
	//{{AFX_MSG_MAP(ImprocScript)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(ImprocScript, CCmdTarget)
	//{{AFX_DISPATCH_MAP(ImprocScript)
	DISP_FUNCTION(ImprocScript, "HuangThreshold", HuangThreshold, VT_I4, VTS_I4)
	DISP_FUNCTION(ImprocScript, "EntropyThreshold", EntropyThreshold, VT_I4, VTS_I4)
	DISP_FUNCTION(ImprocScript, "MomentThreshold", MomentThreshold, VT_I4, VTS_I4)
	DISP_FUNCTION(ImprocScript, "DistLineThreshold", DistLineThreshold, VT_I4, VTS_I4)
	DISP_FUNCTION(ImprocScript, "GreyThreshold", GreyThreshold, VT_EMPTY, VTS_I4 VTS_I2 VTS_I2)
	DISP_FUNCTION(ImprocScript, "Threshold8", Threshold8, VT_I4, VTS_I4 VTS_I2 VTS_I2)
	DISP_FUNCTION(ImprocScript, "Threshold16", Threshold16, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(ImprocScript, "ThresholdGT8", ThresholdGT8, VT_I4, VTS_I4 VTS_I2)
	DISP_FUNCTION(ImprocScript, "Mean8", Mean8, VT_I4, VTS_I4 VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(ImprocScript, "Mode8", Mode8, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(ImprocScript, "Mod8", Mod8, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(ImprocScript, "ImageSum", ImageSum, VT_I4, VTS_I4)
	DISP_FUNCTION(ImprocScript, "Divide8", Divide, VT_I4, VTS_I4 VTS_I4 VTS_I2)
	DISP_FUNCTION(ImprocScript, "AutoStretch", AutoStretch, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(ImprocScript, "EdgeMeasure", EdgeMeasure, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(ImprocScript, "GetEdge", GetEdge, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(ImprocScript, "Quantile8", Quantile8, VT_EMPTY, VTS_I4 VTS_R8 VTS_R8 VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(ImprocScript, "Quantile8ROI", Quantile8ROI, VT_EMPTY, VTS_I4 VTS_R8 VTS_R8 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(ImprocScript, "Thin", Thin, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(ImprocScript, "FFTBandPass", FFTBandPass, VT_EMPTY, VTS_I4 VTS_I4 VTS_R4 VTS_R4)
	DISP_FUNCTION(ImprocScript, "ReplaceGreyLevel", ReplaceGreyLevel, VT_EMPTY, VTS_I4 VTS_I2 VTS_I2)
	DISP_FUNCTION(ImprocScript, "ImageInvert", ImageInvert, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(ImprocScript, "FuzzyCmeanSegment", FuzzyCmeanSegment, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_R8)
	DISP_FUNCTION(ImprocScript, "SctSplitSegment", SctSplitSegment, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(ImprocScript, "MeanShiftSegment", MeanShiftSegment, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(ImprocScript, "EdgeLink", EdgeLink, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(ImprocScript, "Morph", Morph, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(ImprocScript, "FThin", FThin, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(ImprocScript, "EdgeDetection", EdgeDetection, VT_EMPTY, VTS_BSTR VTS_I4 VTS_R4 VTS_I4 VTS_I4)
	DISP_FUNCTION(ImprocScript, "HoughFilter", HoughFilter, VT_EMPTY, VTS_I4 VTS_BSTR VTS_I4 VTS_I4)
	DISP_FUNCTION(ImprocScript, "BgrSubtract", BgrSubtract, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(ImprocScript, "Subtract", Subtract, VT_I4, VTS_I4 VTS_I4)
    DISP_FUNCTION(ImprocScript, "LogicalAND", LogicalAND, VT_I4, VTS_I4 VTS_I4)
    DISP_FUNCTION(ImprocScript, "LogicalOR",  LogicalOR, VT_I4, VTS_I4 VTS_I4)
    DISP_FUNCTION(ImprocScript, "LogicalXOR", LogicalXOR, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(ImprocScript, "DCT", DiscreteCosine, VT_EMPTY, VTS_I4)
    DISP_FUNCTION(ImprocScript, "FlatField", FlatField, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(ImprocScript, "ContrastStretch16To8", ContrastStretch16To8, VT_BOOL, VTS_I4 VTS_I4)

	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_ITMAScript to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {62C41D37-F04F-4484-8517-8A8846B8DA25}
static const IID IID_IImprocScript =
{ 0x62c41d37, 0xf04f, 0x4484, { 0x85, 0x17, 0x8a, 0x88, 0x46, 0xb8, 0xda, 0x25 } };

BEGIN_INTERFACE_MAP(ImprocScript, CCmdTarget)
	INTERFACE_PART(ImprocScript, IID_IImprocScript, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ImprocScript message handlers

void ImprocScript::OnFinalRelease() 
{	
	CCmdTarget::OnFinalRelease();
}

/////////////////////////////////////////////////////////////////////////////
// Perform Huang Thresholding
// Divides the image grey levels into two classes where the division point
// represents the point at which fuzzy set membership is minimised using
// Shannon's entropy function.
// 
// See Huang L.-K. and Wang M.-J.J. (1995) "Image Thresholding by Minimizing  
// the Measures of Fuzziness" Pattern Recognition, 28(1): 41-51
// http://www.ktl.elf.stuba.sk/study/vacso/Zadania-Cvicenia/Cvicenie_3/TimA2/Huang_E016529624.pdf
//
// VB script syntax:
//		Threshold = HuangThreshold(SrcImage)
// Parameters:
//		SrcImage is an 8 image to threshold
// Returns:
//		A threshold value based on minimising fuzzy set membership
/////////////////////////////////////////////////////////////////////////////

long ImprocScript::HuangThreshold(long SrcImage) 
{
	long Thresh = UTS_ERROR;

	if (SrcImage > 0)
	{
		CThreshImage ThreshLib(IP().UTSMemRect());

		Thresh = ThreshLib.HuangThreshold(SrcImage);
		if (Thresh == UTS_ERROR)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation probably not an 8bit image
	}
	else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

	return Thresh;
}


/////////////////////////////////////////////////////////////////////////////
// Perform max Entropy Thresholding
// VB script syntax:
//		Threshold = EntropyThreshold(SrcImage)
// Parameters:
//		SrcImage is an 8 or 16 bpp image to threshold
// Returns:
//		A threshold value based on max entropy calculations
/////////////////////////////////////////////////////////////////////////////

long ImprocScript::EntropyThreshold(long SrcImage) 
{
	long Thresh = UTS_ERROR;
	if (SrcImage > 0)
	{
		CThreshImage ThreshLib(IP().UTSMemRect());

		Thresh = ThreshLib.EntropyThreshold(SrcImage);
		if (Thresh == UTS_ERROR)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
	}
	else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

	return Thresh;
}

/////////////////////////////////////////////////////////////////////////////
// Perform Thresholding by Moment Preservation
// VB script syntax:
//		Threshold = MomentThreshold(SrcImage)
// Parameters:
//		SrcImage is an 8 bit image to threshold
// Returns:
//		A threshold value based on best moment preservation
/////////////////////////////////////////////////////////////////////////////

long ImprocScript::MomentThreshold(long SrcImage) 
{
	long Thresh = UTS_ERROR;
	if (SrcImage > 0)
	{
		CThreshImage ThreshLib(IP().UTSMemRect());

		Thresh = ThreshLib.MomentThreshold(SrcImage);
		if (Thresh == UTS_ERROR)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
	}
	else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

	return Thresh;
}

/////////////////////////////////////////////////////////////////////////////
// Perform Thresholding by Distance Line to Backgr Peak
// VB script syntax:
//		Threshold = DistLineThreshold(SrcImage)
// Parameters:
//		SrcImage is an 8 bit image to threshold
// Returns:
//		A threshold value based on best moment preservation
/////////////////////////////////////////////////////////////////////////////

long ImprocScript::DistLineThreshold(long SrcImage) 
{
	long Thresh = UTS_ERROR;
	if (SrcImage > 0)
	{
		CThreshImage ThreshLib(IP().UTSMemRect());

		Thresh = ThreshLib.DistLineThreshold(SrcImage);
		if (Thresh == UTS_ERROR)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
	}
	else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

	return Thresh;
}

/////////////////////////////////////////////////////////////////////////////
// Grey level threshold an image, preserving greys in the range specified
/////////////////////////////////////////////////////////////////////////////

void ImprocScript::GreyThreshold(long SrcImage, short LowThresh, short HighThresh) 
{
	CThreshImage ThreshLib(IP().UTSMemRect());

	if (SrcImage > 0)
		ThreshLib.GreyThreshold(SrcImage, (BYTE) LowThresh, (BYTE) HighThresh);
	else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
}

/////////////////////////////////////////////////////////////////////////////
//  Threshold an 8 bit image to produce an 8 bit binary image
// VB Script syntax:
//		BinIm = Threshold8(Image, Lo, Hi)
// Parameters:
//		8 BPP Image to threshold
//      Lo & HI are the threshold range
// Returns:
//		An 8 bit binary image where 255 = 1, 0 = 0
/////////////////////////////////////////////////////////////////////////////

long ImprocScript::Threshold8(long SrcImage, short LowThresh, short HighThresh)
{
    long NewHandle = UTS_ERROR;
    CUTSImProc  CIP(IP().UTSMemRect());

    if (SrcImage > 0)
        NewHandle = CIP.Threshold8(SrcImage, (BYTE) LowThresh, (BYTE) HighThresh);
    else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
    return NewHandle;
}

/////////////////////////////////////////////////////////////////////////////
//  Threshold an N bit image to produce an 8 bit binary image
// VB Script syntax:
//		BinIm = Threshold16(Image, Lo, Hi)
// Parameters:
//		9-16 BPP Image to threshold
//      Lo & HI are the threshold range
// Returns:
//		An 8 bit binary image where 255 = 1, 0 = 0
/////////////////////////////////////////////////////////////////////////////

long ImprocScript::Threshold16(long SrcImage, long LowThresh, long HighThresh)
{
    long NewHandle = UTS_ERROR;
    CUTSImProc  CIP(IP().UTSMemRect());

    if (SrcImage > 0)
        NewHandle = CIP.Threshold16(SrcImage, (WORD) LowThresh, (WORD) HighThresh);
    else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
    return NewHandle;
}

/////////////////////////////////////////////////////////////////////////////
//  Threshold an 8 bit image to produce an 8 bit binary image
// VB Script syntax:
//		BinIm = ThresholdGT8(Image, Thresh)
// Parameters:
//		8 BPP Image to threshold
//      Thresh is the threshold
// Returns:
//		An 8 bit binary image where 255 = 1, 0 = 0
/////////////////////////////////////////////////////////////////////////////

long ImprocScript::ThresholdGT8(long SrcImage, short Thresh)
{
    long NewHandle = UTS_ERROR;
    CUTSImProc  CIP(IP().UTSMemRect());

    if (SrcImage > 0)
        NewHandle = CIP.ThresholdGT8(SrcImage, (BYTE) Thresh);
    else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
    return NewHandle;
}

/////////////////////////////////////////////////////////////////////////////
//  Finds the mean 
// VB Script syntax:
//		Mean = Mean8(Image, Mask8, Area)
// Parameters:
//		8 BPP Image to find the mean of
//      Mask8 an 8 bit binary image to act as the mask
//      Area [out] is the number of ones (255) in mask
// Returns:
//		The mean
/////////////////////////////////////////////////////////////////////////////

long ImprocScript::Mean8(long SrcImage, long MaskImage, VARIANT FAR *A)
{
    long Mean = UTS_ERROR, Area;
    CUTSImProc  CIP(IP().UTSMemRect());

    if (SrcImage > 0)
        Mean = CIP.Mean8(SrcImage, MaskImage, &Area);
    else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
    V_VT(A) = VT_I4;
    A->lVal = Area;

    return Mean;
}
/////////////////////////////////////////////////////////////////////////////
//  Finds the modal value below a max level
// VB Script syntax:
//		Mode = Mode8(SrcImage, MaxLevel)
// Parameters:
//		8 BPP Image to find the mean of
//      MaxLevel is the level which the mode cannot exceed
// Returns:
//		The mode
/////////////////////////////////////////////////////////////////////////////

long ImprocScript::Mode8(long SrcImage, long MaxLevel)
{
    long Mode = UTS_ERROR;
    CUTSImProc  CIP(IP().UTSMemRect());

    if (SrcImage > 0)
        Mode = CIP.Mode8(SrcImage, (BYTE) MaxLevel);
    else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

    return Mode;
}

/////////////////////////////////////////////////////////////////////////////
//  Finds the modulus of two images
// VB Script syntax:
//		DestImage = Mod8(Image1, Image2)
// Parameters:
//      Image1, Image2 are the input images
// Returns:
//		A new destination image that is |DestImage| = |Image1| + |Image2|
/////////////////////////////////////////////////////////////////////////////

long ImprocScript::Mod8(long SrcImage1, long SrcImage2)
{
    long Dest = UTS_ERROR;
    CUTSImProc  CIP(IP().UTSMemRect());

    if (UTSVALID_HANDLE(SrcImage1) && UTSVALID_HANDLE(SrcImage2))
        Dest = CIP.Mod8(SrcImage1, SrcImage2);
    else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

    return Dest;
}

/////////////////////////////////////////////////////////////////////////////
//  Finds the total of all greys in the image
// VB Script syntax:
//		Total = ImageSum(Image)
// Parameters:
//      Image is the input image
// Returns:
//		A total of all grey levels
/////////////////////////////////////////////////////////////////////////////

long ImprocScript::ImageSum(long SrcImage)
{
	long Total = UTS_ERROR;
    CUTSImProc  CIP(IP().UTSMemRect());

    if (UTSVALID_HANDLE(SrcImage))
		Total = CIP.ImageSum(SrcImage);
    else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

    return Total;
}


/////////////////////////////////////////////////////////////////////////////
// Measure quantiles in an 8 bit image
// VB Script syntax:
//		Quantile8 Image, Lo, Hi, QLo, QHi
// Parameters:
//		Image is an 8 BPP Image
//      Lo is the lower percentage quantile
//      Hi is the upper percentage quantile
//      QLo is the resulting grey level for the lower percentage q
//      QHi is the resulting grey level for the upper percentage q
// Returns:
//		Nothing
//////////////////////////////////////////////////////////////////////////////
  
void ImprocScript::Quantile8(long Image, double QuantLo, double QuantHi, VARIANT FAR *Lo, VARIANT FAR *Hi)
{
    if (UTSVALID_HANDLE(Image))
    {
	    long BPP, Pitch, Width, Height;
	    void *Addr;

        int QLo, QHi;
        BPP = IP().UTSMemRect()->GetExtendedInfo(Image, &Width, &Height, &Pitch, &Addr);

		if (BPP == EIGHT_BITS_PER_PIXEL)
		{
			BYTE Bo, Bi;

			ImageQuantiles8((const BYTE *)Addr, QuantLo, QuantHi, Width, Height, 0, 0, Width, Height, &Bo, &Bi);
			QLo = Bo;
			QHi = Bi;
		}
		else
		if (BPP == SIXTEEN_BITS_PER_PIXEL)
		{
			WORD Wo, Wi;
			ImageQuantiles16((const WORD *)Addr, 65536, QuantLo, QuantHi, Width, Height, 0, 0, Width, Height, &Wo, &Wi);
			QLo = Wo;
			QHi = Wi;
		}
		else
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);				// A handle is invalid

        V_VT(Lo) = VT_I4;
        Lo->lVal = QLo;
        V_VT(Hi) = VT_I4;
        Hi->lVal = QHi;
    }
    else
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
}

void ImprocScript::Quantile8ROI(long Image, double QuantLo, double QuantHi, long X, long Y, long W, long H, VARIANT FAR *Lo, VARIANT FAR *Hi)
{
    if (UTSVALID_HANDLE(Image))
    {
	    long BPP, Pitch, Width, Height;
	    void *Addr;

        int QLo, QHi;
        BPP = IP().UTSMemRect()->GetExtendedInfo(Image, &Width, &Height, &Pitch, &Addr);

		if (X + W > Width || X < 0)
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);
		if (Y + H > Height || Y < 0)
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);
		

		if (BPP == EIGHT_BITS_PER_PIXEL)
		{
			BYTE Bo, Bi;

			ImageQuantiles8((const BYTE *)Addr, QuantLo, QuantHi, Width, Height, X, Y, W, H, &Bo, &Bi);
			QLo = Bo;
			QHi = Bi;
		}
		else
		if (BPP == SIXTEEN_BITS_PER_PIXEL)
		{
			WORD Wo, Wi;
			ImageQuantiles16((const WORD *)Addr, 65536, QuantLo, QuantHi, Width, Height, X, Y, W, H, &Wo, &Wi);
			QLo = Wo;
			QHi = Wi;
		}
		else
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);				// A handle is invalid

        V_VT(Lo) = VT_I4;
        Lo->lVal = QLo;
        V_VT(Hi) = VT_I4;
        Hi->lVal = QHi;
    }
    else
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
}

/////////////////////////////////////////////////////////////////////////////
// Auto contrast stretch an image
// Subtracts the min grey value, determines the contrast range
// and then multiplies grey levels accordingly
//
// VB Script syntax:
//		AutoStretch Image
// Parameters:
//		8 BPP Image to stretch
// Returns:
//		Nothing
/////////////////////////////////////////////////////////////////////////////

void ImprocScript::AutoStretch(long SrcImage) 
{
	long bpp, x, y;
	double histinfo[GHISTINFODATALENGTH];
	double contrast;

	if (SrcImage > 0)
	{
		        // Get The Bits Per Pixel For The Image
        bpp = IP().UTSMemRect()->GetRectInfo(SrcImage, &x, &y);
        
        // Only support 8 bits per pixel for now
        if (bpp == EIGHT_BITS_PER_PIXEL)
        {
            long histo[NBINS_EIGHT];
            
			// Do The Histogram
			IP().UTSMemRect()->Histogram(SrcImage, histo);
			
			// Get info from the histogram
			IP().UTSGeneric()->HistInfo(histo, NBINS_EIGHT, histinfo);
			
			// Subtract the min
			IP().UTSMemRect()->ConstPixelwise((long) histinfo[1], "-L", SrcImage);
			
			// Now determine the max and min
			contrast = histinfo[2] - histinfo[1];
			
			if (contrast != 0.0)
			{
				if (IP().UTSMemRect()->MulDiv(SrcImage, NBINS_EIGHT, (int) contrast) < 0)
					AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
			}
        }
        else
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);    // Invalid parameter type
	}
	else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
}

/////////////////////////////////////////////////////////////////////////////
// Test only
/////////////////////////////////////////////////////////////////////////////
long ImprocScript::EdgeMeasure(long SrcImage, long SpacingX, long SpacingY) 
{
	long BPP, Pitch, Width, Height;
	void *Addr;
	BYTE *Ptr;
	BYTE gijp1, gij, gijm1;
	int g;
	int i, j;
	long si, sj;


	if (UTSVALID_HANDLE(SrcImage))
	{
		BPP = IP().UTSMemRect()->GetExtendedInfo(SrcImage, &Width, &Height, &Pitch, &Addr);
		if (BPP == EIGHT_BITS_PER_PIXEL)
		{
			sj = 0;
			for (i = SpacingY; i < Height - SpacingY; i += SpacingY)
			{
				si = 0;
				for (j = SpacingX; j < Width - SpacingX; j += SpacingX)
				{
					Ptr = (BYTE *) Addr + j + i * Pitch;
					gijp1 = *(Ptr + SpacingX);
					gij = *Ptr;
					gijm1 = *(Ptr - SpacingX);
					g = gijp1 - 2 * gij + gijm1;
					si += (g * g);
				}
				sj += si;
			}
		}
	}
	
	return sj;
}

/////////////////////////////////////////////////////////////////////////////
// Looks at gradients in an image and tries to determine where the valleys
// are
/////////////////////////////////////////////////////////////////////////////

long ImprocScript::GetEdge(long SrcImage, long Spacing) 
{
	long BPP, Pitch, Width, Height, W, H, P, NewHandle = UTS_ERROR, VDist, MaskHandle = UTS_ERROR;
	void *SrcAddr, *DestAddr;
	BYTE *SrcPtr, *DestPtr;
	BYTE g11, g12, g13, g21, g22, g23, g31, g32, g33;
	int g;
	int i, j;

	if (UTSVALID_HANDLE(SrcImage))
	{
		BPP = IP().UTSMemRect()->GetExtendedInfo(SrcImage, &Width, &Height, &Pitch, &SrcAddr);
		if (BPP == EIGHT_BITS_PER_PIXEL)
		{
			NewHandle = IP().UTSMemRect()->CreateAs(SrcImage);
			BPP = IP().UTSMemRect()->GetExtendedInfo(NewHandle, &W, &H, &P, &DestAddr);
            VDist = Pitch * Spacing;
			for (i = Spacing; i < Height - Spacing; i++)
			{
				for (j = Spacing; j < Width - Spacing; j++)
				{
					SrcPtr = (BYTE *) SrcAddr + j + i * Pitch;
					g11 = *(SrcPtr - Spacing - Pitch);
					g12 = *(SrcPtr - VDist);
					g13 = *(SrcPtr + Spacing - Pitch);
					g21 = *(SrcPtr - Spacing);
					g22 = *SrcPtr;
					g23 = *(SrcPtr + Spacing);
					g31 = *(SrcPtr - Spacing + Pitch);
					g32 = *(SrcPtr + VDist);
					g33 = *(SrcPtr + Spacing + Pitch);
					g = 0;

					if ((g13 > g22) && (g22 < g31)) g = 255;
					if ((g11 > g22) && (g22 < g33)) g = 255;
					if ((g21 > g22) && (g22 < g23)) g = 255;
                    if ((g12 > g22) && (g22 < g32)) g = 255;
					DestPtr = (BYTE *) DestAddr + j + i * Pitch;
					*DestPtr = (BYTE) g;
				}
			}
			MaskHandle = IP().UTSMemRect()->CreateAsWithPix(NewHandle, ONE_BIT_PER_PIXEL);
			IP().UTSMemRect()->Convert(NewHandle, MaskHandle, 1);
			IP().UTSMemRect()->FreeImage(NewHandle);
		}
	}
	
	return MaskHandle;
}

/////////////////////////////////////////////////////////////////////////////
// Thin an image down
// VB syntax:
//		NewImage = Thin(SrcImage, MaxIter)
// Parameters:
//		SrcImage is the image to thin which may be either 1 or 8 bit. Only
//			levels of 255 are considered in an 8 bit thin.
//		MaxIter is the maximum number of thinning iterations to perform.
//		    NOTE that thinning will stop if no more pixels can be removed
//			from the image
// Returns:
//		A one bit mask with the thinned result in it.
/////////////////////////////////////////////////////////////////////////////

long ImprocScript::Thin(long SrcImage, long MaxIter) 
{
	long NewHandle = UTS_ERROR, MaskHandle = UTS_ERROR;
	long BPP, Width, Height, Handle;

	if (UTSVALID_HANDLE(SrcImage))
	{
		BPP = IP().UTSMemRect()->GetRectInfo(SrcImage, &Width, &Height);
		if (BPP == EIGHT_BITS_PER_PIXEL || BPP == ONE_BIT_PER_PIXEL)
		{
			CThin CThinImage(IP().UTSMemRect());

			if (BPP == ONE_BIT_PER_PIXEL)
			{
				// Convert the 1 bit image to an 8 bit one first
				Handle = IP().UTSMemRect()->CreateAsWithPix(SrcImage, EIGHT_BITS_PER_PIXEL);
				IP().UTSMemRect()->Convert(SrcImage, Handle, 255);
			    NewHandle = CThinImage.ThinImage(Handle, MaxIter);
				IP().UTSMemRect()->FreeImage(Handle);
			}
			else 
				NewHandle = CThinImage.ThinImage(SrcImage, MaxIter);

			// Convert to a 1 bit mask
			MaskHandle = IP().UTSMemRect()->CreateAsWithPix(NewHandle, ONE_BIT_PER_PIXEL);
			IP().UTSMemRect()->Convert(NewHandle, MaskHandle, 1);
			IP().UTSMemRect()->FreeImage(NewHandle);
		}
	}
	
	return MaskHandle;
}

/////////////////////////////////////////////////////////////////////////////
// Perform an FFT Gaussian band pass operation on an image
// VB Syntax:
//		FFTBandPass SrcImage, DestImage, F1, F2
// Returns:
//		Nothing
// Parameters:
//		SrcImage is the image to filter (8 bit)
//		DestImage is the filtered result image (8 bit)
//		Freq1 and Freq2 specify the range of frequencies 
//			to filter over 0.0 to 1.0
/////////////////////////////////////////////////////////////////////////////

void ImprocScript::FFTBandPass(long SrcImage, long DestImage, float Freq1, float Freq2) 
{
	CUTSFFT	FFTBP(IP().UTSMemRect());

	FFTBP.GaussFilter(SrcImage, DestImage, CUTSFFT::BandPass, Freq1, Freq2);

}

/////////////////////////////////////////////////////////////////////////////
// Simply performs substitution of one grey level within another in an
// 8 bit grey level image
// VB Syntax:
//      ReplaceGreyLevel SrcImage, OldGrey, NewGrey
// Parameters:
//      SrcImage is a handle to an 8 bit image
//      OldGrey is the grey level to replace
//      NewGrey is the new grey level
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void ImprocScript::ReplaceGreyLevel(long SrcImage, short OldGrey, short NewGrey) 
{
    long BPP, Width, Height, Pitch;
    long i, j;
    BYTE *ptr, *Addr;

    if (UTSVALID_HANDLE(SrcImage))
    {
        BPP = IP().UTSMemRect()->GetExtendedInfo(SrcImage, &Width, &Height, &Pitch, &Addr);
        if (BPP == EIGHT_BITS_PER_PIXEL)
        {
            ptr = Addr;
			for (i = 0; i < Height; i++)
			{
				for (j = 0; j < Width; j++)
				{
                    if (*ptr == OldGrey)
                        *ptr = (BYTE) NewGrey;
                    ptr++;
				}
                ptr += (Pitch - Width);
			}
        }
        else
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
}

/////////////////////////////////////////////////////////////////////////////
// Simply inverts an image
// VB Syntax:
//      ImageInvert SrcImage
// Parameters:
//      SrcImage is a handle to an 8 bit image
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void ImprocScript::ImageInvert(long SrcImage) 
{
	long W, H, P, BPP;
	BYTE *Addr, *ptr;
	long i, j;

	// Get info about the image first
	BPP = IP().UTSMemRect()->GetExtendedInfo(SrcImage, &W, &H, &P, &Addr);
	ptr = Addr;

	switch (BPP)
	{
	case EIGHT_BITS_PER_PIXEL:
		for (i = 0; i < H; i++)
		{
			for (j = 0; j < W; j++)
			{
				*ptr = ~(*ptr);
				ptr++;
			}
			ptr += (P - W);
		}
		break;
	case ONE_BIT_PER_PIXEL:
	case TWENTYFOUR_BITS_PER_PIXEL:
		for (i = 0; i < H; i++)
		{
			for (j = 0; j < P; j++)
			{
				*ptr = ~(*ptr);
				ptr++;
			}
		}
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////
// Segments a RGB color image based on fuzzy C-mean segmentation algorithm
// VB Syntax:
//		FuzzyCmeanSegment RedImage, GreenImage, BlueImage, variance	
// Parameters:
//      RedImage, GreenImage and BlueImage are handles to 8 bit images
//		variance is the value for gaussian kernel variance (0.0 - 20.0)
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void ImprocScript::FuzzyCmeanSegment(long R, long G, long B, double variance)
{
	if ( variance<0.0 ) 
		variance = 0.0;
	if ( variance>20.0 )
		variance = 20.0;
	if ( UTSVALID_HANDLE(R) && UTSVALID_HANDLE(G) && UTSVALID_HANDLE(B) ) {
		CSegment FuzzyCmeanSeg(IP().UTSMemRect());
		if ( FuzzyCmeanSeg.FuzzyCmeanSegment(R, G, B, variance) == UTS_ERROR)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
}

/////////////////////////////////////////////////////////////////////////////
// Segments a RGB color image based on SCT/Center Split segmentation algorithm
// VB Syntax:
//		SctSplitSegment RedImage, GreenImage, BlueImage, A_split, B_split	
// Parameters:
//      RedImage, GreenImage and BlueImage are handles to 8 bit images
//		A_split: number of colors to divide along angle A
//		B_split: number of colors to divide along angle B
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void ImprocScript::SctSplitSegment(long R, long G, long B, long A_split, long B_split)
{
	if ( A_split<1 ) 
		A_split = 1;
	if ( A_split>20 ) 
		A_split = 20;
	if ( B_split<1 ) 
		B_split = 1;
	if ( B_split>20 ) 
		B_split = 20;
	if ( UTSVALID_HANDLE(R) && UTSVALID_HANDLE(G) && UTSVALID_HANDLE(B) ) {
		CSegment SctSplitSeg(IP().UTSMemRect());
		if ( SctSplitSeg.SctSplitSegment(R, G, B, A_split, B_split) == UTS_ERROR)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
}


/////////////////////////////////////////////////////////////////////////////
// Segments a RGB color image based on Mean Shift segmentation algorithm
// VB Syntax:
//		MeanShiftSegment RedImage, GreenImage, BlueImage	
// Parameters:
//      RedImage, GreenImage and BlueImage are handles to 8 bit images
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void ImprocScript::MeanShiftSegment(long R, long G, long B)
{
	if ( UTSVALID_HANDLE(R) && UTSVALID_HANDLE(G) && UTSVALID_HANDLE(B) ) {
		CSegment SctSplitSeg(IP().UTSMemRect());
		if ( SctSplitSeg.MeanShiftSegment(R, G, B) == UTS_ERROR)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
}

/////////////////////////////////////////////////////////////////////////////
// Links edge points of a binary image into lines
//	Parameters:
//      BinaryImage is a handle to a 8 bit image
//		connection is the maximum link distance in pixel (0 - 20)
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void ImprocScript::EdgeLink(long BinaryImage, long connection)
{
	if ( UTSVALID_HANDLE(BinaryImage) ) {
		CSpatialFilter EdgeLinkFilter(IP().UTSMemRect());
		if ( EdgeLinkFilter.EdgeLink(BinaryImage, connection) == UTS_ERROR)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
}

///////////////////////////////////////////////////////////////////////////////////////////////
//  Performs a morphological operation
//	Parameters:
//		op_type: 
//			1: Binary dilation
//			2: Binary erosion
//			3: Binary closing
//			4: Binary opening
//			5: Gray dilation
//			6: Gray erosion
//			7: Gray closing
//			8: Gray opening
//		k_type:
//			0: user defined structuring element
//			1: Flat disk structuring element ("Se" needs to be square)
//			2: Flat rectangular structuring element
//		Image (input and output): a handle to a eight-bit (0-255) or one-bit (0-1) image
//      Se (input): is a handle to a eight-bit (0-255) or one-bit (0-1) structuring element
///////////////////////////////////////////////////////////////////////////////////////////////

void ImprocScript::Morph(long op_type, long k_type, long Image, long Se)
{
	if ( UTSVALID_HANDLE(Image) && UTSVALID_HANDLE(Se) ) {
		CSpatialFilter MorphFilter(IP().UTSMemRect());
		if ( MorphFilter.Morph((int)op_type, (int)k_type, Image, Se) == UTS_ERROR)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
}

/////////////////////////////////////////////////////////////////////////////
// Thin an image down based on Zhang & Suen/Stentiford/Holt combined algorithm
// VB syntax:
//		FThin SrcImage
// Parameters:
//		SrcImage is the image to thin which may be either 1 or 8 bit.
/////////////////////////////////////////////////////////////////////////////

void ImprocScript::FThin(long BinaryImage)
{
	if ( UTSVALID_HANDLE(BinaryImage) ) {
		CSpatialFilter FThinning(IP().UTSMemRect());
		if ( FThinning.FThin(BinaryImage) == UTS_ERROR)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Performs Edge Detection based on Marr/Hildreth, Canny, or Shen-Casten algorithm
//  VB script syntax: 
//				EdgeDetection methode, Image, sigma, low, high	
//	Parameters:
//		methode :	"marr", "canny", "shen"
//      Image:		a handle to a eight-bit (0-255) image
//		sigma:		- standard deviation of gaussian for Marr/Hildreth and Canny algorithm (>0.0) 
//					  2.0 is a typical value.
//					- smoothing factor for Shen-Casten algorithm (0.0 - 1.0). 
//					  0.9 is a typical value.
//		low:		low threshold of the Hysteresis thresholding for Canny and Shen-Casten algorithms. 
//					50 is a typical value. Marr/Hildreth does not use this parameter(low=0).
//		high:		high threshold of the Hysteresis thresholding for Canny and Shen-Casten algorithms. 
//					150 is a typical value. Marr/Hildreth does not use this parameter(high=0).	
///////////////////////////////////////////////////////////////////////////////////////////////

void ImprocScript::EdgeDetection(LPCTSTR methode, long Image, float sigma, long low, long high)
{
	if ( UTSVALID_HANDLE(Image) ) 
	{
		CSpatialFilter ED(IP().UTSMemRect());
		if ( ED.EdgeDetection((char*)methode, Image, sigma, low, high) == UTS_ERROR)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Performs hough filter for lines
//  VB script syntax: 
//		HoughFilter BinaryImage, degree_string, minlength, connection	
//	Parameters:
//      - BinaryImage is a handle to a eight-bit (0-255) or one-bit (0-1) image
//		- degree_string indicates which angles are of interest and should be included 
//		  when hough transform is performed. An example would be like "-18 40-60 90 130-", 
//		  which means keep those lines whose slope are in the range from 0 to 18, 40 to 60, 
//		  90, 130 to 179.
//		- minlength indicates the minimum number of pixels on a none conected line which will 
//		  be kept in the result image.
//		- connection is the maxmum distance to be linked on a line which will be kept after 
//		  minlength thresholding. (0 - 20)
///////////////////////////////////////////////////////////////////////////////////////////////

void ImprocScript::HoughFilter(long BinaryImage, LPCTSTR degree_string, long minlength, long connection)
{
	if ( UTSVALID_HANDLE(BinaryImage) ) 
	{
		CSpatialFilter HF(IP().UTSMemRect());
		if ( HF.HoughFilter(BinaryImage, (char*)degree_string, minlength, connection) == UTS_ERROR)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
}

/////////////////////////////////////////////////////////////////////////////
//
// Parameters:
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

long ImprocScript::BgrSubtract(long GreyImage, long BgrImage, long Threshold)
{
    long NewHandle = UTS_ERROR;
    CUTSImProc CIP(IP().UTSMemRect());

	if (UTSVALID_HANDLE(GreyImage) && UTSVALID_HANDLE(BgrImage)) 
    {
        NewHandle = CIP.BgrSubtract(GreyImage, BgrImage, Threshold);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

    return NewHandle;
}

/////////////////////////////////////////////////////////////////////////////
//  Subtract two 8 bit images quickly subtracts Image1 from Image2
// VB syntax:
//  Im3 = Subtract(Image1, Image2)
// Parameters:
//  Image1 & Image2 are the images
// Returns:
//  A handle to a new image which is the subtraction of image1 from image2
/////////////////////////////////////////////////////////////////////////////

long ImprocScript::Subtract(long Image1, long Image2)
{
    long NewHandle = UTS_ERROR;
    CUTSImProc CIP(IP().UTSMemRect());

	if (UTSVALID_HANDLE(Image1) && UTSVALID_HANDLE(Image2)) 
        NewHandle = CIP.Subtract(Image1, Image2);
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

    return NewHandle;
}

/////////////////////////////////////////////////////////////////////////////
//  AND two 8 bit images quickly 
// VB syntax:
//  Im3 = LogicalAND(Image1, Image2)
// Parameters:
//  Image1 & Image2 are the images
// Returns:
//  A handle to a new image which is the subtraction of image1 from image2
/////////////////////////////////////////////////////////////////////////////

afx_msg long ImprocScript::LogicalAND(long Image1, long Image2)
{
    long NewHandle = UTS_ERROR;
    CUTSImProc CIP(IP().UTSMemRect());

	if (UTSVALID_HANDLE(Image1) && UTSVALID_HANDLE(Image2)) 
        NewHandle = CIP.LogicalAND(Image1, Image2);
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

    return NewHandle;
}

afx_msg long ImprocScript::LogicalOR(long Image1, long Image2)
{
    long NewHandle = UTS_ERROR;
    CUTSImProc CIP(IP().UTSMemRect());

	if (UTSVALID_HANDLE(Image1) && UTSVALID_HANDLE(Image2)) 
        NewHandle = CIP.LogicalOR(Image1, Image2);
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

    return NewHandle;
}

afx_msg long ImprocScript::LogicalXOR(long Image1, long Image2)
{
    long NewHandle = UTS_ERROR;
    CUTSImProc CIP(IP().UTSMemRect());

	if (UTSVALID_HANDLE(Image1) && UTSVALID_HANDLE(Image2)) 
        NewHandle = CIP.LogicalXOR(Image1, Image2);
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

    return NewHandle;
}

/////////////////////////////////////////////////////////////////////////////
// 
/////////////////////////////////////////////////////////////////////////////

afx_msg void ImprocScript::DiscreteCosine(long Image)
{
	long W, H, P;
	void *Addr;

	if (UTSVALID_HANDLE(Image))
	{
		int BPP = IP().UTSMemRect()->GetExtendedInfo(Image, &W, &H, &P, &Addr);

		CDCT dct(8, W, H);

		double *pDCTData;

		if (BPP == EIGHT_BITS_PER_PIXEL)
		{
			dct.ForwardDCT(&pDCTData, (BYTE *)Addr);
			dct.MultiplyCoeffs(pDCTData);
			dct.InverseDCT((BYTE *)Addr, pDCTData);
		}
		else if (BPP == SIXTEEN_BITS_PER_PIXEL)
		{
			dct.ForwardDCT(&pDCTData, (WORD *)Addr);
			dct.MultiplyCoeffs(pDCTData);
			dct.InverseDCT((WORD *)Addr, pDCTData);
		}

		delete pDCTData;
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid
}

/////////////////////////////////////////////////////////////////////////////
// Divide two 8 bit images and multiply the result by a scale 
// VB syntax:
//  Im3 = Divide(Image1, Image2, Scale)
// Parameters:
//  Image1 & Image2 are the images
//  Scale is a BYTE sized value 0 - 255
// Returns:
//  A handle to a new image which is the division of image1 from image2
/////////////////////////////////////////////////////////////////////////////

afx_msg long ImprocScript::Divide(long Image1, long Image2, short Scale)
{
    long NewHandle = UTS_ERROR;
    CUTSImProc CIP(IP().UTSMemRect());

	if (UTSVALID_HANDLE(Image1) && UTSVALID_HANDLE(Image2)) 
        NewHandle = CIP.Divide(Image1, Image2, (BYTE) Scale);
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

    return NewHandle;
}

/////////////////////////////////////////////////////////////////////////////
// Flat fielding or shade correction
// VB Syntax:
//  FlatFieldImage = FlatField(SrcImage, ShadeImage)
// Parameters:
//  SrcImage is the image to flat field
//  ShadeImage is the shade image
// Returns:
//  A handle to the flat fielded image
/////////////////////////////////////////////////////////////////////////////

afx_msg long ImprocScript::FlatField(long SrcImage, long ShadeImage)
{
    long NewHandle = UTS_ERROR;
    CUTSImProc CIP(IP().UTSMemRect());

	if (UTSVALID_HANDLE(SrcImage) && UTSVALID_HANDLE(ShadeImage)) 
        NewHandle = CIP.FlatField(SrcImage, ShadeImage);
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);				// A handle is invalid

    return NewHandle;
}




//------------------------------------------------------------------------------------------------
// ContrastStretch16To8
// - ImageIn handle to 16bit image
//   ImageOut handle to 8 bit image
//------------------------------------------------------------------------------------------------
afx_msg BOOL ImprocScript::ContrastStretch16To8(long ImageIn, long ImageOut)
{  
    long WidthIn, HeightIn, PitchIn, BppIn, size, l;
    long WidthOut, HeightOut, PitchOut, BppOut;
    void *AddrIn, *AddrOut;
	unsigned short *s;
	unsigned char *p;
	int	 minVal, maxVal;
    // Get info about this image
    BppIn=IP().UTSMemRect()->GetExtendedInfo(ImageIn, &WidthIn, &HeightIn, &PitchIn, &AddrIn);
    BppOut=IP().UTSMemRect()->GetExtendedInfo(ImageOut, &WidthOut, &HeightOut, &PitchOut, &AddrOut);

	if ((WidthIn  != WidthOut) || (HeightIn != HeightOut) || (BppIn != 16) || (BppOut != 8))
		AfxThrowOleDispatchException(0xFF, IDS_GIPSIZEMISMATCH, 0);         // Bpp mismatch

	size=WidthIn*HeightIn;
	minVal=99999;
	maxVal=0;
	s=(unsigned short *)AddrIn;
	for (l=0; l< size; l++)
	{
		if (*s > maxVal) maxVal=*s;
		if (*s < minVal) minVal=*s;
		s +=1;
	}
	s=(unsigned short *)AddrIn;
	p=(unsigned char *)AddrOut;
	if (maxVal==minVal)maxVal=minVal+1;
	for (l=0; l < size; l++)
		*p++=(unsigned char)((*s++-minVal)*255/(maxVal-minVal));
	return FALSE;
}

