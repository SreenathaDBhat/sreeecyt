/////////////////////////////////////////////////////////////////////////////
// MSKscript.cpp : implementation file
//
// This OLE automation class is exposed to the script engine interface.
// Implementation of new UTS keywords is done here. This keywords are
// UTS mask operations
//
// Written By Karl Ratcliff 24042001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
// Implementation of new keywords is simply done using the Classwizard (to
// be recommended). This is done by selecting the Automation tab and then
// selecting the class name: MSKscript. Add new 
//

#include "stdafx.h"
#include "resource.h"
#include "scriptutils.h"
#include "vcmemrect.h"
#include "vcblob.h"
#include "vcgeneric.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "woolzif.h"
#include "vcthresh.h"
#include "vcImProc.h"
#include "uts.h"
#include "commonscript.h"
#include "safearr.h"
#include "mskscript.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// MSKscript
/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(MSKscript, CCmdTarget)

MSKscript::MSKscript()
{
	EnableAutomation();
    // By default work in interactive mode
    m_ExecutionMode = modeDebug;
}

MSKscript::~MSKscript()
{
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// NOTE implement class members not exposed to the scripting engine here
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// Reset the queues and anything else that needs it
/////////////////////////////////////////////////////////////////////////////

void MSKscript::Reset(void)
{
}


void MSKscript::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}


BEGIN_MESSAGE_MAP(MSKscript, CCmdTarget)
	//{{AFX_MSG_MAP(MSKscript)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(MSKscript, CCmdTarget)
	//{{AFX_DISPATCH_MAP(MSKscript)
	DISP_FUNCTION(MSKscript, "DistToMask", DistToMask, VT_I4, VTS_I4 VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(MSKscript, "RectMaskData", RectMaskData, VT_I4, VTS_I4 VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(MSKscript, "ReconFast", ReconFast, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(MSKscript, "Reconstruct", Reconstruct, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(MSKscript, "MaskSubrectOnes", MaskSubrectOnes, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(MSKscript, "LongSmoothArc", LongSmoothArc, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(MSKscript, "MaskCalipers", MaskCalipers, VT_I4, VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(MSKscript, "LineEndsInMask", LineEndsInMask, VT_I4, VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(MSKscript, "MaskLinkInfo", MaskLinkInfo, VT_I4, VTS_I4 VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(MSKscript, "Dilate", Dilate, VT_EMPTY, VTS_I4 VTS_I2 VTS_I2)
	DISP_FUNCTION(MSKscript, "Erode", Erode, VT_EMPTY, VTS_I4 VTS_I2 VTS_I2)
	DISP_FUNCTION(MSKscript, "AccumLinesSeg", AccumLinesSeg, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_R8 VTS_R8)
	DISP_FUNCTION(MSKscript, "Borders", Borders, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(MSKscript, "ConvexHull", ConvexHull, VT_EMPTY, VTS_I4 VTS_R8)
	DISP_FUNCTION(MSKscript, "Open", Open, VT_EMPTY, VTS_I4 VTS_I2 VTS_I2)
	DISP_FUNCTION(MSKscript, "Close", Close, VT_EMPTY, VTS_I4 VTS_I2 VTS_I2)
	DISP_FUNCTION(MSKscript, "NewMaskShapeInfo", MaskShapeInfo, VT_I4, VTS_I4 VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(MSKscript, "OperMask", OperMask, VT_EMPTY, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(MSKscript, "MaskSplitRange", MaskSplitRange, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(MSKscript, "MaskData", MaskData, VT_EMPTY, VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(MSKscript, "Circle", Circle, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(MSKscript, "PutPixel", PutPixel, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(MSKscript, "DrawCross", DrawCross, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(MSKscript, "Rectangle", Rectangle, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(MSKscript, "TextLine", TextLine, VT_EMPTY, VTS_I4 VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(MSKscript, "GetPixel", GetPixel, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(MSKscript, "Line", Line, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IMSKscript to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {ED0434A6-CBAF-4D5F-B6F5-7E5BF69801C0}
static const IID IID_IMSKscript =
{ 0xed0434a6, 0xcbaf, 0x4d5f, { 0xb6, 0xf5, 0x7e, 0x5b, 0xf6, 0x98, 0x1, 0xc0 } };

BEGIN_INTERFACE_MAP(MSKscript, CCmdTarget)
	INTERFACE_PART(MSKscript, IID_IMSKscript, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// MSKscript message handlers
////////////////////////////////////////////////////////////////////////////////////////
// VB Syntax:
//      D = DistToMask(SrcHandle, Xc, Yc)
// Parameters:
// Returns:
//	    Square of the distance or -1 if empty;
//      Updates Xc, Yc
//
////////////////////////////////////////////////////////////////////////////////////////

long MSKscript::DistToMask(long SrcHandle, VARIANT FAR* Xc, VARIANT FAR* Yc) 
{
    long D = -1;
    long X = Xc->lVal, Y = Yc->lVal;

    // Got a valid source image
    if (SrcHandle > 0)
    {
        D = IP().UTSMemRect()->DistToMask(SrcHandle, &X, &Y);
        Xc->lVal = X;
        Yc->lVal = Y;
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // SrcImage handle is invalid

	return D;
}

////////////////////////////////////////////////////////////////////////////////////////
// VB Syntax:
//  Area = RectMaskData(SrcHandle, MaskHandle, MaskData)
// Parameters:
//      SrcHandle is an 8 bit image to get info about
//      MaskHandle is a 1 bit image used as the mask
//		MaskData -  pointer to array of not less than 11 longs;
//        		    fills it with:
//        	        data[0] = NUMBER of one bits in mask
//        	        data[1] = SUM of bytes in 'mem' corresponding to one bits in mask
//        	        data[2] = MIN of bytes in 'mem' corresponding to one bits in mask
//        	        data[3] = MAX of bytes in 'mem' corresponding to one bits in mask
//        	        data[4] = X coordinate one of the MIN pixel(s)
//        	        data[5] = Y coordinate one of the MIN pixel(s)
//        	        data[6] = X coordinate one of the MAX pixel(s)
//        	        data[7] = Y coordinate one of the MAX pixel(s)
//        	        data[8] = standard deviation:
//            			      (long)(sqrt(Sum((P-Aver(P))^2)/NumOf)+0.5)
//       	        data[9] = number of pixels with MIN value
//        	        data[10] = number of pixels with MAX value
//
//	Returns:
//		number of 1-bits in mask (same as data[0]),
//        -1 if failure
////////////////////////////////////////////////////////////////////////////////////////

long MSKscript::RectMaskData(long SrcHandle, long MaskHandle, VARIANT FAR* MaskData) 
{
    long A = -1;
    long Data[MRRECTMASKDATALENGTH];

    // Image handles valid ?
    if (SrcHandle > 0 && MaskHandle > 0)
    {
        A = IP().UTSMemRect()->RectMaskData(SrcHandle, MaskHandle, Data);

        // Success so return data to VB 
        if (A >= 0) 
        {
            SAFEARRAY *pSafe;           // Safe array to put data into

            // Get array info
            CHECK_SAFEARR_EXCEPTION(GetArrayInfo(MaskData, &pSafe, 1), return UTS_ERROR);
    
            // Check the dimension is >= MRRECTMASKDATALENGTH
            CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 1, MRRECTMASKDATALENGTH), return UTS_ERROR);
            
            // Copy data across
            SetArrayLongData(pSafe, Data, MRRECTMASKDATALENGTH);
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // SrcImage handle is invalid

    return A;
}

////////////////////////////////////////////////////////////////////////////////////////
// Reconstructs in 'Seed' bit mask within 'Limit'
// begining from point (xc, yc).
// Algorithm of reconstruction is as follows:
//		Do
//			OldSeed := Seed
//			Seed := Dilate(Seed) MIN Limit
//		While OldSeed <> Seed
//
//		Dilate() means "set each pixel to MAX of its neighbors' value"
//		This expansion is limited by Limit.
//
// VB syntax:
//      NumPixels = ReconFast(HandleLimit, HandleSeed, Xc, Yc)
// returns:
// 		Ones in Seed mask if OK, -1 otherwise
////////////////////////////////////////////////////////////////////////////////////////

long MSKscript::ReconFast(long HandleLimit, long HandleSeed, long Xc, long Yc) 
{
	// Check valid handles
    if (HandleLimit > 0 && HandleSeed > 0)
        return IP().UTSMemRect()->ReconFast(HandleLimit, HandleSeed, Xc, Yc);

    AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid

    return UTS_ERROR;
}

////////////////////////////////////////////////////////////////////////////////////////
//		Reconstructs 'Seed' bit mask within 'Limit'.
//		Algorithm of reconstruction is as follows:
//		Do
//			OldSeed := Seed
//			Seed := Dilate(Seed) MIN Limit
//		While OldSeed <> Seed
//
//		Dilate() means "set each pixel to MAX of its neighbors' value"
//		This expansion is limited by Limit.
//
// returns:
// 		if OK: Ones in Seed mask if pix=1; summa of bytes if pix=8;
//        -1 otherwise
////////////////////////////////////////////////////////////////////////////////////////

long MSKscript::Reconstruct(long HandleLimit, long HandleSeed) 
{
	// Check valid handles
    if (HandleLimit > 0 && HandleSeed > 0)
        return IP().UTSMemRect()->Reconstruct(HandleLimit, HandleSeed);

    AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid

    return -1;
}

////////////////////////////////////////////////////////////////////////////////////////
//  MaskSubrectOnes
//  VB syntax:
//      Ones = MaskSubrectOnes(SrcHandle, Xleft, Ytop, Width, Height)
//  Parameters:
//      SrcHandle is the source image 
//      Xleft, Ytop specify the top left hand corner of the rectangle to get data for
//      Width and Height specify the rectangle dimensions to use for the measurements
//  Returns: 
//      Ones if OK, -1 if bad
////////////////////////////////////////////////////////////////////////////////////////

long MSKscript::MaskSubrectOnes(long SrcHandle, long Xleft, long Ytop, long Width, long Height) 
{
	// Check valid handles
    if (SrcHandle > 0)
        return IP().UTSMemRect()->MaskSubrectOnes(SrcHandle, Xleft, Ytop, Width, Height);

    AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid

    return -1;
}

////////////////////////////////////////////////////////////////////////////////////////
// 	Calculates
//
//    For  *codes and/or *curvs  NULL is OK.
//    For  maskarc and/or maskcont  0 is OK.
//    Updates:
//    	resdata[0] = start X coordinate of longest smooth arc;
//    	resdata[1] = start Y coordinate of longest smooth arc;
//    	resdata[2] = number of codes in longest smooth arc;
//    	resdata[3] = maximal curvature;
//      resdata[4] - number of elements with (curvature >= curvthresh);
//    	resdata[5] = number of codes in contour;
//    	resdata[6] = lenght of longest smooth arc;
//    	resdata[7] = lenght of contour;
//    	if (codes != NULL) then it is filled with incremental
//        	codes of the contour, begining from the start point;
// 	Returns:
//   	 (100 * (lenght of longest smooth arc)/(lenght of contour)) if OK,
//     0 otherwise
//
////////////////////////////////////////////////////////////////////////////////////////

long MSKscript::LongSmoothArc(long SrcMask, long ContMask, long ArcMask, long HalfStepMin, long HalfStepMax, long CurvThresh, 
                              VARIANT FAR* Results, VARIANT FAR* Codes, VARIANT FAR* Curves) 
{
    long X = 0;

	// Check valid handles
    if (SrcMask > 0 && ContMask >= 0 && ArcMask >= 0)
    {
        long        Res[SPLONGSMOOTHARCDATALENGTH], NumElements, uB, lB;
        long        *pCv, *pCd;
        SAFEARRAY   *pSafeR, *pSafeCd, *pSafeCv;           // Safe arrays to put data into
        
        // Get array pointers and check they are all 1d arrays
        CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Results, &pSafeR,  1), return 0);
        CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Codes,   &pSafeCd, 1), return 0);
        CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Curves,  &pSafeCv, 1), return 0);

        // Check the bounds of one array
        SafeArrayGetLBound(pSafeCd, 1, &lB);
        SafeArrayGetUBound(pSafeCd, 1, &uB);
        NumElements = uB - lB;
        // Now get the bounds of the second and use the smaller of the two as the maximum 
        // to the UTS call
        SafeArrayGetLBound(pSafeCd, 1, &lB);
        SafeArrayGetUBound(pSafeCd, 1, &uB);
        if ((uB - lB) < NumElements) NumElements = uB - lB;
    
        // Check the dimension is >= SPLONGSMOOTHARCDATALENGTH 
        CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafeR, 1, SPLONGSMOOTHARCDATALENGTH), return 0);

        // Allocate two temporary arrays
        pCv = new long[NumElements];
        pCd = new long[NumElements];

        // Mem Allocated o.k.
        if (pCv && pCd)
        {
            // Do it
            X = IP().UTSBlob()->LongSmoothArc(SrcMask, ContMask, ArcMask, HalfStepMin, HalfStepMax, CurvThresh, Res, pCd, pCv, NumElements);

            // Copy data into arrays
            SetArrayLongData(pSafeCv, pCv, NumElements);
            SetArrayLongData(pSafeCd, pCd, NumElements);
            SetArrayLongData(pSafeR, Res, SPLONGSMOOTHARCDATALENGTH);

            // Dispose of allocated mem
            delete pCv;
            delete pCd;
        }
        else
        {
            // Make sure we free anything that was allocated
            if (pCv) delete pCv;
            if (pCd) delete pCd;
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);
        }

        // Return the variable parameters
        return X;
    }

    AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid

    return 0;
}

////////////////////////////////////////////////////////////////////////////////////////
//
//	Calculates extents in 16 directions, finds extreme ones,
//    calculates 'centroid' coordinates
//       calipers - array[16] to fill with min/max 'caliper' sizes
//        			of mask ONEs in 16 directions:
//                Angle[I] = Pi/2 - I*(Pi/16) rad	= 90 - I * 11.5 degrees
//                	where I = 0...15
//                	All projections are rounded to longs
//        imin, imax - pointers to get directions of min and max calipers
//        xc, yc - pointers to get X,Y coordinates of the centroid
//        		(average of extreme points); NULL is ok
//
//	Returns: number of ONEs in the mask if OK,
//    		-1 otherwise
//
////////////////////////////////////////////////////////////////////////////////////////

long MSKscript::MaskCalipers(long Handle, VARIANT FAR* Calipers, VARIANT FAR* Imin, VARIANT FAR* Imax, VARIANT FAR* Xc, VARIANT FAR* Yc) 
{
	long            Cals[16];
    long            MinI, MaxI, Cx, Cy;
    long            Ret = -1;

    // Check the image handle
    if (Handle > 0)
    {
        // Do the call if successful fill in the array data and return values
        if ((Ret = IP().UTSMemRect()->MaskCalipers(Handle, Cals, &MinI, &MaxI, &Cx, &Cy)) >= 0)
        {
            SAFEARRAY   *pSafe;           // Safe array to put data into
            
            // Get and check array info
            CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Calipers, &pSafe, 1), return -1);
            
            // Check the dimension is >= MRMASKCALIPERDATALENGTH
            CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 1, MRMASKCALIPERDATALENGTH), return -1);
            
            // Copy data across
            SetArrayLongData(pSafe, Cals, MRMASKCALIPERDATALENGTH);

            // Set the other vars
            V_VT(Imin) = VT_I4;
            V_VT(Imax) = VT_I4;
            V_VT(Xc)   = VT_I4;
            V_VT(Yc)   = VT_I4;

            Imin->lVal = MinI;
            Imax->lVal = MaxI;
            Xc->lVal   = Cx;
            Yc->lVal   = Cy;
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid

	return Ret;
}

////////////////////////////////////////////////////////////////////////////////////////
//  Calculates the intersection between a line and a mask, adjusting the line ends
//  to fit inside the mask
//  VB syntax:
//      Result = LineEndsInMask(SrcMask, X1, Y1, X2, Y2)
//  Parameters:
//      SrcMask is the mask image
//      (X1, Y1), (X2, Y2) are the point coordinates for the line
//	Returns:
//      number of pixels in the line and mask crossing;
//      -1 if error
////////////////////////////////////////////////////////////////////////////////////////

long MSKscript::LineEndsInMask(long SrcMask, VARIANT FAR* X1, VARIANT FAR* Y1, VARIANT FAR* X2, VARIANT FAR* Y2) 
{
	long N = 0, ax, ay, bx, by;

    if (SrcMask > 0)
    {
        // Set the values
        ax = (long) X1->dblVal;
        ay = (long) Y1->dblVal;
        bx = (long) X2->dblVal;
        by = (long) Y2->dblVal;

        // Do the UTS call
        N = IP().UTSMemRect()->LineEndsInMask(SrcMask, &ax, &ay, &bx, &by);

        // Return the data
        X1->dblVal = (double) ax;
        Y1->dblVal = (double) ay;
        X2->dblVal = (double) bx;
        Y2->dblVal = (double) by;
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid

	return N;
}

////////////////////////////////////////////////////////////////////////////////////////
//    Fills data with information about linked components in source
//    mask as an array of 7-plets of longs:
//
// VB Syntax:
//      NumberOfComponents = MaskLinkInfo(SrcImage, CompData, MinArea)
// Parameters:
//      SrcImage to get the linked components from
//      CompData is an array large enough to hold the data in 
//               = (MaxComponents, MRMASKLINKINFOPORTION) in size
//      CompData as an array of 7-plets of longs:
//    	        {Ni, Xi, Yi, Xmini, Ymini, Xmaxi, Ymaxi},
//	            where
//        	        Ni - number of points in the component,
//    		        Xi,Yi - coordinates of some point belonging to
//                          the i-th linked component,
//    		        Xmini, Ymini, Xmaxi, Ymaxi - min/max X,Y coordinates
//        			        of the i-th linked component.
//      MinArea is the minimum area to be included in the data
// Returns:
//      Number of components found
// NOTE:
//      The maximum number of components found will be limited to the size
//      of the array CompData passed to it.
////////////////////////////////////////////////////////////////////////////////////////

long MSKscript::MaskLinkInfo(long SrcImage, VARIANT FAR* CompData, long MinArea) 
{
    long NumComps = 0, i, j, MaxComps;
    
    SAFEARRAY *pSafe;           // Safe array to put data into
    long    uB1, lB1;           // Bounds for the array
    VARIANT V;                  // Temp var for packaging data to put in array

    // Get array info
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(CompData, &pSafe, 2), return 0);
    
    // Check the second dimension is >= MRMASKLINKINFOPORTION
    CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 2, MRMASKLINKINFOPORTION), return 0);

    // Check the bounds of the array for both indices
    SafeArrayGetLBound(pSafe, 1, &lB1);
    SafeArrayGetUBound(pSafe, 1, &uB1);

    // Get the maximum number of components from the array bounds
    MaxComps = uB1 - lB1;

    // Got a valid source image
    if (SrcImage > 0)
    {
        // Data passed back as long values
        V_VT(&V) = VT_I4;

        // Allocate some temp storage space
        long *pTempData = new long [MaxComps * MRMASKLINKINFOPORTION];

        // Done ok.
        if (pTempData)
        {
            // Get the mask component data
            NumComps = IP().UTSMemRect()->MaskLinkInfo(SrcImage, pTempData, MaxComps, MinArea);

            // Now copy the number of components across to VB
            for (i = 0; i < MaxComps; i++)
            {
                for (j = 0; j < MRMASKLINKINFOPORTION; j++)
                {
                    // Set the value
                    V.lVal = pTempData[i * MRMASKLINKINFOPORTION + j];
                    Set2DArrayIndex(pSafe, i, j, &V);
                }
            }

            // Dispose of temp storage
            delete [] pTempData;
        }
        else
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);

    // Return number of components actually found
	return NumComps;
}

////////////////////////////////////////////////////////////////////////////////////////
// Image Dilation
// 
// VB Syntax:
//      Dilate SrcImage, NeighbourHood
// Parameters:
//      SrcImage      - A valid UTS handle
//      NeighbourHood - 4, 48, 84 or 8 pixels
//      Iter          - Number of times
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void MSKscript::Dilate(long SrcImage, short Neighbourhood, short Iter) 
{
    //short i;

    if (SrcImage > 0)
    {
        switch (Neighbourhood)
        {
        case 4:
			IP().UTSMemRect()->NeiFun(SrcImage, "DILATE4", Iter);
            break;
        case 8:
            IP().UTSMemRect()->NeiFun(SrcImage, "DILATE8", Iter);
            break;
        case 84:
            IP().UTSMemRect()->NeiFun(SrcImage, "DILATE84", Iter);
            break;
        case 48:
            IP().UTSMemRect()->NeiFun(SrcImage, "DILATE48", Iter);
            break;
        default:
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);
            break;
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

////////////////////////////////////////////////////////////////////////////////////////
// Image Erosion
// 
// VB Syntax:
//      Erode SrcImage, NeighbourHood
// Parameters:
//      SrcImage      - A valid UTS handle
//      NeighbourHood - 4, 8 or 48, 84 neighbourhoods 
//      Iter          - number of times
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void MSKscript::Erode(long SrcImage, short Neighbourhood, short Iter) 
{
    //short i;

    if (SrcImage > 0)
    {
        switch (Neighbourhood)
        {
        case 4:
            IP().UTSMemRect()->NeiFun(SrcImage, "ERODE4", Iter);
            break;
        case 8:
            IP().UTSMemRect()->NeiFun(SrcImage, "ERODE8", Iter);
            break;
        case 84:
            IP().UTSMemRect()->NeiFun(SrcImage, "ERODE84", Iter);
            break;
        case 48:
            IP().UTSMemRect()->NeiFun(SrcImage, "ERODE48", Iter);
            break;
        default:
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);
            break;
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

////////////////////////////////////////////////////////////////////////////////////////
//  	Fills <res> with points of maximal curvature calculated
//  	on the base of incremental code contour, assuming start
//  	point at {xstart,ystart}.
//
//
//	Arguments:
//		pcodes - pointer to the buffer of incremental codes
//        ncodes - the number of code bytes
//        xstart, xstart - coordinates of the start pixel
//		curvature - pointer to the result buffer to fill with
//        		(BYTE)curvmea1 values; NULL is OK
//		res - pointer to the result buffer
//		halfstepmin, halfstepmax - range for half step along the contour
//        thresh - threshold to send into <res>
//
//    Reshuffles <ncodes> bytes in <pcodes> circularly so that
//    	first <Nlongest> bytes correspond to the longest arc where
//        all <curvmea1> values are less than <thresh>.
//	Updates the last portion (nres-th) with:
//        res[0], res[1] - coordinates of the start pixel after reshuffling
//        res[2] - <Nlongest> value
//        res[3] - max value for <curvmea1>
//        res[4] - number of elements <nmeabig> with (curvmea1 >= thresh);
//    Returns if OK:
//	returns:
//   	 number of lines if OK,
//     0 otherwise, -1 lib not loaded
////////////////////////////////////////////////////////////////////////////////////////

long MSKscript::AccumLinesSeg(long Mask, long Accum, long HalfStepMin, long HalfStepMax, long CurvThresh, double DistMin, double DistMax) 
{
	// Check handle OK
    if (Mask > 0 && Accum > 0)
        return IP().UTSBlob()->AccumLinesSeg(Mask, Accum, HalfStepMin, HalfStepMax, CurvThresh, DistMin, DistMax);
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid

	return UTS_ERROR;
}

/////////////////////////////////////////////////////////////////////////////////////////
//    Calculates the mask of borders separating linked components
//    in source mask. This is done by successive image dilations
//    until two neighbouring components touch. The point of contact 
//    is then one point on the border.
//  VB syntax:
//      NumberLinkedComponents = Borders(SrcImage, BordersImage, Neighbourhood)
//  Parameters:
//      SrcImage is the source mask
//      BordersImage is the resulting borders mask
//      Neighbourhood is the neighbourhood 8 or 4 for the dilation
// 	Returns:
//   	 number of linked components / -1
//
/////////////////////////////////////////////////////////////////////////////////////////

long MSKscript::Borders(long SrcImage, long BordersImage, long Neighbourhood) 
{
    long ErrorCode = UTS_ERROR;
    TRACE0("Borders CALL Start\r\n");
	// Check handle OK
    if (SrcImage > 0 && BordersImage > 0)
    {
        switch (Neighbourhood)
        {
        case 48:
            ErrorCode = IP().UTSMemRect()->LinkBorders(SrcImage, BordersImage, "N48");
            break;
        case 84:
            ErrorCode = IP().UTSMemRect()->LinkBorders(SrcImage, BordersImage, "N84");
            break;
        case 8:
            ErrorCode = IP().UTSMemRect()->LinkBorders(SrcImage, BordersImage, "N8");
            break;
        case 4:
            ErrorCode = IP().UTSMemRect()->LinkBorders(SrcImage, BordersImage, "N4");
            break;
        default:
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);
            break;
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid

    TRACE0("Borders CALL End\r\n");
	return ErrorCode;
}

/////////////////////////////////////////////////////////////////////////////////////////
// Do convex hull things
// VB syntax:
//      ConvexHull SrcImage, Thickness
// Parameters:
//      SrcImage is the mask image to do the convex hull operations on
//      Thickness is percentile of pixel to "outdraw"
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////////////////

void MSKscript::ConvexHull(long SrcImage, double Thickness) 
{
    if (SrcImage > 0)
    {
        if (IP().UTSMemRect()->MaskConvexHull(SrcImage, Thickness) < 0)
            AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// Image Opening
// 
// VB Syntax:
//      Open SrcImage, NeighbourHood
// Parameters:
//      SrcImage      - A valid UTS handle
//      NeighbourHood - 8, 48 or 84 neighbourhoods 
//      Iter          - number of times
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void MSKscript::Open(long SrcImage, short Neighbourhood, short Iter) 
{
    if (SrcImage > 0)
    {
        switch (Neighbourhood)
        {
        case 8:
            IP().UTSMemRect()->NeiFun(SrcImage, "OPEN8", Iter);
            break;
        case 84:
            IP().UTSMemRect()->NeiFun(SrcImage, "OPEN84", Iter);
            break;
        case 48:
            IP().UTSMemRect()->NeiFun(SrcImage, "OPEN48", Iter);
            break;
        default:
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);
            break;
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

////////////////////////////////////////////////////////////////////////////////////////
// Image Closing
// 
// VB Syntax:
//      Close SrcImage, NeighbourHood
// Parameters:
//      SrcImage      - A valid UTS handle
//      NeighbourHood - 48 or 84 neighbourhoods 
//      Iter          - number of times
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////


void MSKscript::Close(long SrcImage, short Neighbourhood, short Iter) 
{
    if (SrcImage > 0)
    {
        switch (Neighbourhood)
        {
        case 8:
            IP().UTSMemRect()->NeiFun(SrcImage, "CLOSE8", Iter);
            break;
        case 84:
            IP().UTSMemRect()->NeiFun(SrcImage, "CLOSE84", Iter);
            break;
        case 48:
            IP().UTSMemRect()->NeiFun(SrcImage, "CLOSE48", Iter);
            break;
        default:
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);
            break;
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);

}

////////////////////////////////////////////////////////////////////////////////////////
//
// As MaskShapeInfo, but uses a different core function to compute the values
//
//    Fills first 13(?) pixels in each row of h32 with information
//    about linked components in source mask:
//    	{Ni -  number of points in the component = 0
//         Xi, Yi - coordinates of some point belonging to
//                    the i-th linked component, = 1, 2
//   		 Xmini, Ymini, Xmaxi, Ymaxi - min/max X,Y coordinates = 3, 4, 5, 6
//        			of the i-th linked component.
//         AxRati - main Axis ratio (*1000) = 7
//         Cmpi - measure of compactness (*1000) = 8
//         CalMini, CalMaxi - Caliper Min/Max measures = 9, 10
//         Rmaxi - Max Radius of the 'best fitting' circle = 11
//         Rndi - measure of roundness = 12
//        },
//    maxcomp - max number of components to fit in <data>
//?    The result array is sorted by Ni in decreased order.
//  VB syntax:
//      N = MaskShapeInfo(SrcImage, ShapeData, MinArea)
//  Parameters:
//      SrcImage is a mask/binary image
//      ShapeData is an array of shape data returned by the function. This array
//                must be large enough to hold N by 13 elements, where N is
//                the number of blobs in the image or more.
//      MinArea is the min area of data to return in the array, anything smaller
//              is ignored.
// 	Returns:
//   	 N which is number of linked components bigger than <minarea>, or -1 if an error
////////////////////////////////////////////////////////////////////////////////////////

long MSKscript::MaskShapeInfo(long SrcImage, VARIANT FAR* ShapeInfo, long MinArea) 
{
    long NumComps = 0, i, j, MaxComps;
    
    SAFEARRAY *pSafe;           // Safe array to put data into
    long      uB1, lB1;         // Bounds for the array
    VARIANT   V;                // Temp var for packaging data to put in array
	long	  *pTempData;

    // Get array info
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(ShapeInfo, &pSafe, 2), return UTS_ERROR);
    
    // Check the second dimension is >= SPMASKSHAPEINFODATALENGTH
    CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 2, SPMASKSHAPEINFODATALENGTH), return UTS_ERROR);

    // Check the bounds of the array for the first indice
    SafeArrayGetLBound(pSafe, 1, &lB1);
    SafeArrayGetUBound(pSafe, 1, &uB1);

    // Get the maximum number of components from the array bounds
    MaxComps = uB1 - lB1;

	// Allocate some storage for results to come back in
	pTempData = new long[MaxComps * SPMASKSHAPEINFODATALENGTH];

    // Got a valid source image
    if (SrcImage > 0)
    {
        // Data passed back as long values
        V_VT(&V) = VT_I4;

		TRACE0("MaskShapeInfo CALL Start\r\n");
		
		// Get the mask component data
		NumComps = IP().UTSBlob()->NewMaskShapeInfo(SrcImage, pTempData, MinArea, MaxComps);

		TRACE0("MaskShapeInfo CALL End\r\n");
		
		// Now copy the number of components across to VB
		for (i = 0; i < MaxComps; i++)
		{
			for (j = 0; j < SPMASKSHAPEINFODATALENGTH; j++)
			{
				// Set the value
				V.lVal = *(pTempData + i * SPMASKSHAPEINFODATALENGTH + j);

				Set2DArrayIndex(pSafe, i, j, &V);
			}
		}
    }
    else
	{
		delete pTempData;
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid
	}

	delete pTempData;

    // Return the number of components actually found
    return NumComps;
}


////////////////////////////////////////////////////////////////////////////////////////
// Opermask performs the specified operation on a mask
// VB syntax:
//      OperMask MaskImage, Operation
// Parameters:
//      MaskImage is the image to use
//      Operation as defined below:
// "Zeros"	 fill mask with 0; returns 0
// "Ones"    fill mask with 1; returns 0
// "Inverse" inverse mask; returns 0
// "16oid"	 expand mask to convex 16-oid
//		 returns number of ONEs in the 16-oid
// "32oid"	 expand mask to convex 32-oid
//		 returns number of ONEs in the 32-oid
// "FillHoles0" fills 0-holes in a bit mask;
// 			 a 0-hole is a connected set of ZERO pixels not
//           connected with the boundary.
// "FillHoles1" fills 1-holes in a bit mask;	a 1-hole is a
// 			 connected set of ONE pixels not connected with
//           the boundary.
// "ERODE4", "DILATE4" - erosion/dilation with 4 neighboring;
// "ERODE8", "DILATE8" - erosion/dilation with 8 neighboring,
// "Frame"   frame (1 on sides, 0 inside);
// "Frame0"  not frame (0 on sides, 1 inside);
// "OnesRight"  expand ones left to right
// "OnesLeft"   expand ones right to left
// "OnesDown"   expand ones top to bottom
// "OnesUp"   	 expand ones bottom to top
// "ZerosRight" expand zeros left to right
// "ZerosLeft"  expand zeros right to left
// "ZerosDown"  expand zeros top to bottom
// "ZerosUp"    expand zeros bottom to top
//	char *ops = " 0 1 Inv 16 32 "
//    			" H0 H1 Fr0 Fr1 "
//              " 1R 1L 1D 1U  0R 0L 0D 0U "
//  UTS script: OpPic
//  Returns:
//       Nothing
////////////////////////////////////////////////////////////////////////////////////////

void MSKscript::OperMask(long MaskImage, LPCTSTR Operation) 
{
    if (MaskImage > 0)
    {
        if (CHECK_PARAMETER(MaskOps, Operation))
        {
            if (IP().UTSMemRect()->OperMask(MaskImage, (char *) Operation) < 0)
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);
        }
        else
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

////////////////////////////////////////////////////////////////////////////////////////
// Split a mask into component masks where the masks
// contain mask objects either within the range specified by MinArea and MaxArea
// or those outside of minarea and maxarea
// 
// VB syntax:
//      MaskSplitRange RangeImage, MinMaskImage, MaxMaskImage, MinArea, MaxArea, Mic, Mac, Moc
// Parameters:
//      RangeImage   - this is the source mask and will contain objects within the 
//                     range MinArea to MaxArea on exit.
//      MinMask      - this image contains all objects < MinArea in area
//      MaxMask      - this image contains all objects > MaxArea in area
//      Mic          - number of objects in RangeImage
//      Mac          - number of objects in MinMaskImage
//      Moc          - number of objects in MaxMaskImage
////////////////////////////////////////////////////////////////////////////////////////

void MSKscript::MaskSplitRange(long RangeImage, long MinMaskImage, long MaxMaskImage, long MinArea, long MaxArea, VARIANT FAR* Mic, VARIANT FAR* Mac, VARIANT FAR* Moc) 
{
	if (RangeImage > 0 && MinMaskImage > 0 && MaxMaskImage > 0) 
    {
        long Data[4];

        // Split the mask into two, one containing components inside the area ranges
        // the other containing components outside these ranges
        if (IP().UTSMemRect()->MaskSplitRange(RangeImage, MinMaskImage, MaxMaskImage, MinArea, MaxArea, Data) < 0)
            AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);
        else
        {
            // Return the data back from the call, all as longs
            V_VT(Mic) = VT_I4;
            V_VT(Mac) = VT_I4;
            V_VT(Moc) = VT_I4;
            Mic->lVal = Data[0];    // Number of objects < MinArea
            Mac->lVal = Data[1];    // Number of objects > MinArea and < MaxArea
            Moc->lVal = Data[2];    // Number of objects > MaxArea
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

////////////////////////////////////////////////////////////////////////////////////////
// Get various measurement information from a mask
//
// VB syntax:
//      MaskData SrcImage, Data
// Parameters:
//      SrcImage is the image to get data about
//      Data is an array big enough to hold the data
//   Data[1] = Bits per pixel of the image
//   Data[2] = Image Width
//   Data[3] = Image Height
//   Data[4] = Number Of Ones in image
//   Data[5] = Number Of Zeroes
//   Data[6] = Sum(X)
//   Data[7] = Sum(Y),
//   Data[8] = Sum(X*X), 
//   Data[9] = Sum(Y*Y),
//   Data[10]= Sum(X*Y),
//   Data[11]= Min(X), 
//   Data[12]= Max(X),
//   Data[13]= Min(Y), 
//   Data[14]= Max(Y),
//   Data[15]= 1000 * (shortest main axis / longest main axis)
//   Data[16]= 1000 * (measure of compactness)
//   Data[17]= 1000 * cos(ALPHA)
//   Data[18]= 1000 * sin(ALPHA)
//   Data[19]= 1000 * longest main axis (in pixel/1000)
//   	where ALPHA is an angle between X-axis and longest main axis
//   Data[20]= Shortest caliper length
//   Data[21]= Longest caliper length
//   Data[22]= Direction of shortest caliper
//   Data[23]= Direction of longest caliper
////////////////////////////////////////////////////////////////////////////////////////

void MSKscript::MaskData(long SrcImage, VARIANT FAR* Data) 
{
    long      j;
    SAFEARRAY *pSafe;           // Safe array to put data into
    long      uB1, lB1;         // Bounds for the array
    VARIANT   V;                // Temp var for packaging data to put in array

#if 0
    char *datanames =
    		" Pix Width Height Ones Zeros "
    		" SumX SumY SumXX SumYY SumXY "
            " Xmin Xmax Ymin Ymax AxRat Comp "
            " Cos Sin LongAx "
            " Shortest Longest Sdir Ldir ";

#endif
    // Taken directly out of UTS code
    long pix, calipers[16], UTSData[MASKDATALENGTH], i;

    // Zero the data
    for (i = 0; i < MASKDATALENGTH; i++) UTSData[i] = 0;

    UTSData[1] = pix = IP().UTSMemRect()->GetRectInfo(SrcImage, &UTSData[2], &UTSData[3]);
    if (pix == ONE_BIT_PER_PIXEL)
    {
        UTSData[4] = IP().UTSMemRect()->Stat1(SrcImage, &UTSData[5]);
	    UTSData[5] = UTSData[2] * UTSData[3] - UTSData[4];	// zeros
		if (IP().UTSMemRect()->MaskCalipers(SrcImage, calipers, &UTSData[22], &UTSData[23], NULL, NULL) > 0)
        {
        	UTSData[20] = calipers[UTSData[22]];
        	UTSData[21] = calipers[UTSData[23]];
        }
    }
    else
    {
        if (pix < ONE_BIT_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);
    }

    // Get array info
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Data, &pSafe, 1), return);
    
    // Check the dimension is >= MASKDATALENGTH
    CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 1, MASKDATALENGTH), return);

    // Check the bounds of the array for the first indice
    SafeArrayGetLBound(pSafe, 1, &lB1);
    SafeArrayGetUBound(pSafe, 1, &uB1);

    // Got a valid source image
    if (SrcImage > 0)
    {
        // Data passed back as long values
        V_VT(&V) = VT_I4;        
        
        for (j = 0; j < MASKDATALENGTH; j++)
        {
            // Set the value
            V.lVal = UTSData[j];
            Set1DArrayIndex(pSafe, j, &V);
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// Draw a circle on the image
// VB syntax:
//      Circle Image, Cx, Cy, Radius, Colour
// Parameters:
//      Image is the image to draw on
//      Cx, Cy are the centre of the circle
//      Radius is the radius of the circle
//      Colour is the colour to draw with
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void MSKscript::Circle(long Image, long Cx, long Cy, long Radius, long Colour) 
{
    long Data[3];

    // Got a valid source image
    if (Image > 0)
    {
        Data[0] = Cx;
        Data[1] = Cy;
        Data[2] = Radius;
        IP().UTSMemRect()->MaskLine(Image, "Circle", Colour, 3, Data);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid

}

////////////////////////////////////////////////////////////////////////////////////////
// Putpixel sets a pixel in an image to a specific colour
// 
////////////////////////////////////////////////////////////////////////////////////////

void MSKscript::PutPixel(long Image, long X, long Y, long Colour) 
{
    // Got a valid source image
    if (Image > 0)
        IP().UTSMemRect()->PutPixel(Image, X, Y, Colour);
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// Draws a small cross on the image
////////////////////////////////////////////////////////////////////////////////////////

void MSKscript::DrawCross(long SrcImage, long Cx, long Cy, long Size, long Colour) 
{
	int i;
    // Got a valid source image
    if (SrcImage > 0)
	{
		for (i = Cx - Size / 2; i <= Cx + Size / 2; i++)
			IP().UTSMemRect()->PutPixel(SrcImage, i, Cy, Colour);
		for (i = Cy - Size / 2; i <= Cy + Size / 2; i++)
			IP().UTSMemRect()->PutPixel(SrcImage, Cx, i, Colour);
	}
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid

}

////////////////////////////////////////////////////////////////////////////////////////
// Draws a rectangle on the image
////////////////////////////////////////////////////////////////////////////////////////

void MSKscript::Rectangle(long SrcImage, long Left, long Top, long Right, long Bottom, long Colour) 
{
	int i;
	long Temp;

	if (Right < Left)
	{
		Temp = Right;
		Right = Left;
		Left = Temp;
	}
	if (Top < Bottom)
	{
		Temp = Top;
		Top = Bottom;
	    Bottom = Temp;
	}

    // Got a valid source image
    if (SrcImage > 0)
	{
		for (i = Left; i <= Right; i++)
		{
			IP().UTSMemRect()->PutPixel(SrcImage, i, Top, Colour);
			IP().UTSMemRect()->PutPixel(SrcImage, i, Bottom, Colour);
		}
		for (i = Bottom; i <= Top; i++)
		{
			IP().UTSMemRect()->PutPixel(SrcImage, Left, i, Colour);
			IP().UTSMemRect()->PutPixel(SrcImage, Right, i, Colour);
		}
	}
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid

}

////////////////////////////////////////////////////////////////////////////////////////
// Adds a line of text to an image
// VB Script Syntax:
//      TextLine SrcImage, Text, X, Y, H, Colour
// Parameters:
//      SrcImage is to draw the text on
//      Text is the text to draw
//      X, Y are the position for the text
//      H is the height of the text
//      Colour is the colour of the text
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void MSKscript::TextLine(long SrcImage, LPCTSTR Text, long X, long Y, long H, long Colour) 
{
    // Got a valid source image
    if (SrcImage > 0) 
        IP().UTSDraw()->ImageTextOut(SrcImage, Text, _T("Arial"), X, Y, H, (DWORD) Colour);
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid
}


long MSKscript::GetPixel(long image, long X, long Y) 
{
	long ret = 0;

    // Got a valid source image
    if (image > 0)
        ret = IP().UTSMemRect()->GetPixel(image, X, Y);
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid
	
	return ret;
}

////////////////////////////////////////////////////////////////////////////////////////
// Draw a circle on the image
// VB syntax:
//      Line Image, X1, Y1, X2, Y2, Colour
// Parameters:
//      Image is the image to draw on
//      X1, Y1 to X2, Y2 are the line coords
//      Colour is the colour to draw with
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void MSKscript::Line(long Image, long X1, long Y1, long X2, long Y2, long Colour) 
{
    long Data[4];

    // Got a valid source image
    if (Image > 0)
    {
        Data[0] = X1;
        Data[1] = Y1;
        Data[2] = X2;
        Data[3] = Y2;
        IP().UTSMemRect()->MaskLine(Image, "Line", Colour, 1, Data);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid

}
