//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by VBStrings.rc
//
#define IDS_IMAGEFILENOTFOUND           14102
#define IDS_INVALIDHANDLE               14103
#define IDS_NOPARAMETERFILE             14104
#define IDS_INVALIDOPERATOR             14105
#define IDS_OPERATIONFAILED             14106
#define IDS_BREAKSCRIPT                 14107
#define IDS_INVALIDPARAMETER            14108
#define IDS_ARRAYDIMENSIONS             14109
#define IDS_WRITEFAIL                   14110
#define IDS_IMAGEOPERATIONFAILED        14111
#define IDS_MEMALLOC                    14112
#define IDS_INVALIDPARAMETERVALUE       14113
#define IDS_PERSISTENTVARIABLENOTEXIST  14114
#define IDS_OPOK                        14115
#define IDS_INVALIDIMAGE                14116
#define IDS_IMAGEMEMALLOC               14117
#define IDS_ARRAYNOTEXIST               14118
#define IDS_INVALIDPARAMETERRANGE       14119
#define IDS_NOINPUTREGIONLIST           14120
#define IDS_NOOUTPUTREGIONLIST          14121
#define IDS_NOREGIONLISTS               14122
#define IDS_DISKFULL                    14123
#define IDS_INVALIDPARAMETERTYPE        14124
#define IDS_WOOLZOBJECTLIMIT            14125
#define IDS_FSSPOTEXTRACTOBJECTLIMIT    14125
#define IDS_IMAGESIZEDIV4               14126
#define IDS_GIPSIZEMISMATCH             14127
#define IDS_NOGIPCREATED                14128
#define IDS_NOGIPIMAGECREATED           14129
#define IDS_TIPDEVICEMISMATCH			14130
#define IDS_NOCUDA_ARRAY				14131
#define IDS_CUDA_ARRAY					14132

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        14125
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           125
#endif
#endif
