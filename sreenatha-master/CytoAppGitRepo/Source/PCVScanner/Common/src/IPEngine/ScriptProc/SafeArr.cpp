/////////////////////////////////////////////////////////////////////////////
// Script utility functions for safe array processing
//
// Written By K Ratcliff 12042001
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//
#include "stdafx.h"
#include "resource.h"
#include "safearr.h"

/////////////////////////////////////////////////////////////////////////////
// Helper functions for Safe Array manipulation
// Functions returning a value here, should return either SAFENOERRORS or
// the IDS_ of an exception defined in the VBstrings string table
/////////////////////////////////////////////////////////////////////////////

// Set an element in a 1D array
void Set1DArrayIndex(SAFEARRAY *pSafe, long Idx, VARIANT *V)
{
    long idx;

    idx = Idx;
    SafeArrayPutElement(pSafe, &idx, V);
}

// Set an element in a 2D array
void Set2DArrayIndex(SAFEARRAY *pSafe, long Idx1, long Idx2, VARIANT *V)
{
    long idx[2];

    idx[0] = Idx1;
    idx[1] = Idx2;
    SafeArrayPutElement(pSafe, idx, V);
}

// Check an array dimension too see wether it is large enough
int CheckBounds(SAFEARRAY *pSafe, long Dimension, long Limit)
{
    long lB, uB;

    // Check the bounds of the array 
    SafeArrayGetLBound(pSafe, Dimension, &lB);
    SafeArrayGetUBound(pSafe, Dimension, &uB);
    
    // Check size is large enough
    if ((uB - lB) < Limit)
        return IDS_ARRAYDIMENSIONS;

    return SAFENOERROR;
}

// Get information about a safe array from its information structure
int GetArrayInfo(VARIANT *ArrayInfo, SAFEARRAY **pSafe, UINT NumDimensions)
{
    // Is it an array ?
    if (!(ArrayInfo->vt && VT_ARRAY))
        return IDS_INVALIDPARAMETER;
    
    // Get a pointer to reference the array with
    if (ArrayInfo->vt & VT_BYREF)
        *pSafe = *(ArrayInfo->pparray);
    else
        *pSafe = ArrayInfo->parray;
    
    // Check array dimensions are enough
    if (SafeArrayGetDim(*pSafe) != NumDimensions)
        return IDS_ARRAYDIMENSIONS;

    return SAFENOERROR;
}

// Set an array of longs in a safe array
void SetArrayLongData(SAFEARRAY *pSafe, long *Data, int NumElements)
{
    VARIANT V;
    long i;

    V_VT(&V) = VT_I4;           // long data (int32)

    for (i = 0; i < NumElements; i++)
    {
        V.lVal = *Data++;
        SafeArrayPutElement(pSafe, &i, &V);
    }
}

// Set an array of bytes in a safe array
void SetArrayByteData(SAFEARRAY *pSafe, BYTE *Data, int NumElements)
{
    VARIANT V;
    long i;

    V_VT(&V) = VT_UI1;           // byte data (uint8)

    for (i = 0; i < NumElements; i++)
    {
        V.lVal = *Data++;
        SafeArrayPutElement(pSafe, &i, &V);
    }
}

// Set an array of longs in a safe array
void SetArrayDoubleData(SAFEARRAY *pSafe, double *Data, int NumElements)
{
    VARIANT V;
    long i;

    V_VT(&V) = VT_R8;           // double data

    for (i = 0; i < NumElements; i++)
    {
        V.dblVal = *Data++;
        SafeArrayPutElement(pSafe, &i, &V);
    }
}

