#include "stdafx.h"
#include "scriptdirective.h"

CScriptDirective::CScriptDirective(bool setdefault)
{
	m_directives = NULL;
	m_ndirectives = 0;
	m_directivespace = 0;

	if (setdefault) {
		AddDirective(_T("Image"), _T("Frame"));
		AddDirective(_T("Image"), _T("Region"));
		AddDirective(_T("Image"), _T("AITiff"));
		AddDirective(_T("Image"), _T("Resolution"));
		AddDirective(_T("Image"), _T("Overlap"));
		AddDirective(_T("Image"), _T("Autosplit"));
	}
}

CScriptDirective::~CScriptDirective(void)
{
	// Jan. 6, 2003, DS. Fix memory leak here by freeing all directives
	FreeDirectives();
}

CDirective::CDirective(void)
{
}

CDirective::~CDirective(void)
{
}

// // Script directive information
bool CScriptDirective::AddDirective(CString directive, CString sub_directive)
{
	CDirective *newspace, *oldspace;
	int i;

	// Prevent duplicate additions
	for (i = 0; i < m_ndirectives; i++) {
		if ((m_directives[i].m_directive.CompareNoCase(directive) == 0) &&
			(m_directives[i].m_sub_directive.CompareNoCase(sub_directive) == 0)) {
				// We have a match
				return true;	// Already in the table
		}
	}

	// If no directive space, then add some
	if (m_directivespace == 0) {
		newspace = new CDirective[m_ndirectives + 10];
		oldspace = m_directives;
		for (i = 0; i < m_ndirectives; i++)
			newspace[i] = oldspace[i];

		// Reassign newspace
		delete [] m_directives;
		m_directives = newspace;

		m_directivespace = 10;
	}

	// Add directive
	m_directives[m_ndirectives].m_directive = directive;
	m_directives[m_ndirectives].m_sub_directive = sub_directive;
	m_directives[m_ndirectives].m_value = _T("");
	m_ndirectives++;
	m_directivespace--;

	return false;
}

// Free all stored directives and their values
void CScriptDirective::FreeDirectives(void)
{
	if (m_directives)
		delete [] m_directives;
	m_directives = NULL;
	m_ndirectives = 0;
	m_directivespace = 0;
}

// Parse file for script directives
bool CScriptDirective::ParseScript(CString script_name)
{
	CStdioFile f;
	CString line, DirToken, SubDirToken, ValToken;
	int i, pos;

	// Open file
	if (f.Open(script_name, CFile::modeRead | CFile::shareDenyNone)) {

		// Clear existing directive values
		ClearDirectives();

		// Read a line of file
		while (f.ReadString(line))
		{
			// Trim leading whitespace
			line.TrimLeft();

			// Must be in a comment
			if (line.GetAt(0) == '\'')
			{
				// Remove comment character
				line.Delete(0, 1);

				// Tokenise line
				pos = 0;
				DirToken = line.Tokenize(_T(" \t"), pos);

				if (pos > 0)
					SubDirToken = line.Tokenize(_T(" \t"), pos);

				// Must have first 2 tokens
				if (pos < 0)
					continue;

				// Value toke if any
				ValToken = line.Tokenize(_T(" \t"), pos);

                // Look through directives and try to match up
				for (i = 0; i < m_ndirectives; i++) {
					// Match directive and sub-directive
					if ((DirToken.CompareNoCase(m_directives[i].m_directive) == 0) &&
						(SubDirToken.CompareNoCase(m_directives[i].m_sub_directive) == 0)) {

						// If there's a value set it to that, else set to "1"
						if (pos > 0)
							m_directives[i].m_value = ValToken;
						else
							m_directives[i].m_value = _T("1");

						break;
					}
				}
			}
		}

		// Close file
		f.Close();

	} else {
		CString msg;
		
		// Be silent, but return error state
		//msg.Format("CScriptDirective::ParseScript: Can't open file\r\n%s", script_name);
		//AfxMessageBox(msg);
		return false;
	}

	return true;
}

// Clear all directive values
void CScriptDirective::ClearDirectives(void)
{
	int i;

	for (i = 0; i < m_ndirectives; i++)
		m_directives[i].m_value.Empty();
}

// Match directive and sub-directive
// Return TRUE if value is "1" else FALSE
bool CScriptDirective::MatchDirective(CString directive, CString sub_directive)
{
	CString value = GetDirectiveValue(directive, sub_directive);

	if (value.Compare(_T("1")) == 0)
		return TRUE;
	else
		return FALSE;
}

CString CScriptDirective::GetDirectiveValue(CString directive, CString sub_directive)
{
	for (int i = 0; i < m_ndirectives; i++)
	{
		if ((m_directives[i].m_directive.CompareNoCase(directive) == 0) &&
			(m_directives[i].m_sub_directive.CompareNoCase(sub_directive) == 0)) {
				// We have a match
				return m_directives[i].m_value;
		}
	}

	// Default - should never get here
	return CString();
}

double CScriptDirective::GetDirectiveDoubleValue(CString directive, CString sub_directive)
{
	CString value = GetDirectiveValue(directive, sub_directive);

	return _tstof(value);
}

int CScriptDirective::GetDirectiveIntValue(CString directive, CString sub_directive)
{
	CString value = GetDirectiveValue(directive, sub_directive);

	return _tstoi(value);
}
