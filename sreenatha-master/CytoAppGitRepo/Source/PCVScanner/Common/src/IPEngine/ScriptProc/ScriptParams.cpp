#include "stdafx.h"
#include "ScriptParams.h"
#include "RegHelper.h"
#include "SerializeXML.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CParamSet::CParamSet()
{
}

CParamSet::CParamSet(CString PersistName, CString Label, CString CtrlType, COleVariant Min, COleVariant Max, COleVariant Val)
{
	m_PersistentName = PersistName;
	m_Label = Label;
	m_CtrlType = CtrlType;
	m_Min = Min;
	m_Max = Max;
	m_Value = Val;
	m_Default = Val;
}

CParamSet::~CParamSet()
{
}

CParamSet* CParamSet::duplicate()
{
	CParamSet* ps = new CParamSet();
	ps->m_Label = m_Label;
	ps->m_CtrlType = m_CtrlType;
	ps->m_Max   = m_Max;
	ps->m_Min   = m_Min;
	ps->m_PersistentName = m_PersistentName;
	ps->m_Value = m_Value;
	ps->m_Default = m_Default;
	return ps;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CScriptParams::CScriptParams()
{
}

CScriptParams::~CScriptParams()
{
    Kill();
}

void CScriptParams::Kill(void)                                    // Delete them all
{
    POSITION pos=m_ParamSetList.GetHeadPosition();
    CParamSet* ps;
    while (pos)
    {
        ps = m_ParamSetList.GetNext(pos);
        delete ps;
    }
    m_ParamSetList.RemoveAll();

}

//////////////////////////////////////////////////////////////////////
// Search for a parameter in the list by name, returns NULL if could
// not find it
//////////////////////////////////////////////////////////////////////

CParamSet* CScriptParams::FindParam(CString Name)
{
    CParamSet *pFound = NULL;

	POSITION pos = m_ParamSetList.GetHeadPosition();
    while (pos)
    {
        pFound = m_ParamSetList.GetNext(pos);
		if (pFound->m_PersistentName == Name)
            return pFound;
    }

    return NULL;
}

//////////////////////////////////////////////////////////////////////
// Search for a parameter in the list by index, returns NULL if could
// not find it
//////////////////////////////////////////////////////////////////////

CParamSet* CScriptParams::FindParam(int Idx)
{
    CParamSet *pFound = NULL;

	POSITION pos = m_ParamSetList.FindIndex(Idx);
    if (pos)
        pFound = m_ParamSetList.GetAt(pos);

    return pFound;
}

//////////////////////////////////////////////////////////////////////
// Standard duplicate stuff
//////////////////////////////////////////////////////////////////////

CScriptParams* CScriptParams::duplicate()
{
	CScriptParams* psl = new CScriptParams();

	POSITION pos=m_ParamSetList.GetHeadPosition();
	while (pos != NULL)
	{
		CParamSet* ps = m_ParamSetList.GetNext(pos);
		CParamSet* ps_copy = ps->duplicate();
		psl->m_ParamSetList.AddTail(ps_copy);
	}

	return psl;
}

void CScriptParams::duplicate(CScriptParams *pParams)
{
    m_ParamSetList.RemoveAll();
    int P = pParams->GetCount();

    for (int i = 0; i < P; i++)
    {
        CParamSet *pS = pParams->FindParam(i);
        m_ParamSetList.AddHead(pS->duplicate());
    }
}

//////////////////////////////////////////////////////////////////////
// Return the number of entries
//////////////////////////////////////////////////////////////////////

int	CScriptParams::GetCount(void)									// Return number of entries
{
	return m_ParamSetList.GetCount();
}

//////////////////////////////////////////////////////////////////////
// XML Serialisation
//////////////////////////////////////////////////////////////////////

void CScriptParams::XMLSerialize(CSerializeXML &ar)
{
		POSITION pos;
        COleVariant  VtMin, VtMax, VtValue, VtDef;	// Can't use structure memebrs as name as -> isn't allowed in XML
        //long  Min, Max, Value;					// Can't use structure memebrs as name as -> isn't allowed in XML
		CString CtrlType, Label, Name;

		VtDef.vt = VT_EMPTY;

		XMLCLASSNODENAME(_T("CScriptParams"), ar);	

		// Handle list items
		if (ar.IsStoring())
		{
			pos=m_ParamSetList.GetHeadPosition();
			while (pos != NULL)
			{
				CParamSet * ps = m_ParamSetList.GetNext(pos);
                CtrlType = ps->m_CtrlType;		
                Label = ps->m_Label;
				VtMin = ps->m_Min; 
                VtMax = ps->m_Max; 
                VtValue = ps->m_Value; 
				VtDef = ps->m_Default;
                Name = ps->m_PersistentName;

				XMLCLASSNODENAME(_T("CParamSet"), ar);	
				XMLDATA(Name);
				XMLDATA(CtrlType);
				XMLDATA(Label);
				XMLDATA(VtMin);
				XMLDATA(VtMax);
				XMLDATA(VtValue);
				XMLDATA(VtDef);
				XMLENDNODE;
			}
		}
		else
		{
			XMLSTARTLISTDATA(_T("CParamSet"));					// Iterates class into values
			XMLCLASSNODENAME(_T("CParamSet"), ar);	
			XMLDATA(Name);
			XMLDATA(CtrlType);
			XMLDATA(Label);
			XMLDATA(VtMin);
			XMLDATA(VtMax);
			XMLDATA(VtValue);
			XMLDATA(VtDef);
			CParamSet * ps = new CParamSet;
			ps->m_CtrlType = CtrlType;										// NOTE: can't use -> as a name in XML
			ps->m_PersistentName = Name;
			ps->m_Label = Label;
			ps->m_Min = VtMin;
			ps->m_Max = VtMax;
			ps->m_Value = VtValue;
			if (VtDef.vt != VT_EMPTY)
				ps->m_Default = VtDef;
			else
				ps->m_Default = VtValue;
			m_ParamSetList.AddTail(ps);
			XMLENDNODE;
			XMLENDLISTDATA;
		}
		XMLENDNODE;
}

//////////////////////////////////////////////////////////////////////
// Binary Serialisation
//////////////////////////////////////////////////////////////////////

void CScriptParams::Serialize(CArchive &ar)
{
	int i,count;
	POSITION pos;
	CParamSet* ps;

	if (ar.IsStoring())
	{
		// Storing code here
		ar << SCRIPTPARAMVERSION;
		ar << m_ParamSetList.GetCount();
		pos=m_ParamSetList.GetHeadPosition();
		while (pos != NULL)
		{
			CParamSet * ps = m_ParamSetList.GetNext(pos);
			ar << ps->m_PersistentName;
			ar << ps->m_CtrlType;
			ar << ps->m_Label;
			ar << ps->m_Min;
			ar << ps->m_Max;
			ar << ps->m_Value;
			ar << ps->m_Default;
		}
	}
	else
	{
		int version;
		ar >> version;

		pos=m_ParamSetList.GetHeadPosition();
		while (pos != NULL)
		{
			ps = m_ParamSetList.GetNext(pos);
			delete ps;
		}
		m_ParamSetList.RemoveAll();

		ar >> count;
		for (i=0; i<count; i++)
		{
			CParamSet* ps = new CParamSet;
			ar >> ps->m_PersistentName;
			ar >> ps->m_CtrlType;
			ar >> ps->m_Label;
			ar >> ps->m_Min;
			ar >> ps->m_Max;
			ar >> ps->m_Value;
			if (version == SCRIPTPARAMVERSION)
				ar >> ps->m_Default;
			else
				ps->m_Default = ps->m_Min;
			m_ParamSetList.AddTail(ps);
		}
	}
}
