/////////////////////////////////////////////////////////////////////////////
// Implementation of the persistent variables class
//
// Written By Karl Ratcliff 22062001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include <afxtempl.h>
#include "ScriptPersist.h"
#include "scriptutils.h"
#include "picman.h"
#include "scriptproc.h"

/////////////////////////////////////////////////////////////////////////////
// This is a wrapper for the storage class that stores VarPersist type
// variables as a CMap class. CMap has been used for its hash table
// lookup ability. NOTE entries are stored as variants which is nice
// for VB scripting
/////////////////////////////////////////////////////////////////////////////

// Constructor/destructor

VarMapBank::VarMapBank()
{
}

VarMapBank::~VarMapBank()
{
    POSITION p = m_VarBank.GetStartPosition();
    CString  Name;
    COleVariant  V;

    // Dispose of any system allocated BSTRs 
    while (p)
    {
        m_VarBank.GetNextAssoc(p, Name, V);
        if (V.vt == VT_BSTR)
            SysFreeString(V.bstrVal);
    }
}

/////////////////////////////////////////////////////////////////////////////
// Initialises a value in the bank, this is only done once. Repeated calls 
// to this with the same named value will do absolutely nothing.
// Parameters:
//      VarName is the name of the value to init - NB Case sensitive !
//      Value is the value to get
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void VarMapBank::InitValue(LPCTSTR VarName, VARIANT Value)      // Initialise and add a new value
{
    COleVariant V;

    // Check it exists in the bank
    if (!m_VarBank.Lookup(VarName, V))
    {
        V = Value;
        // Did not find it so set the initial value
        m_VarBank.SetAt(VarName, V);
    }
}

// Double version
void VarMapBank::InitValue(LPCTSTR VarName, double Value)      // Initialise and add a new value
{
    COleVariant V;

    V_VT(&V) = VT_R8;
    V.dblVal = Value;
    InitValue(VarName, V);
}

// Long version
void VarMapBank::InitValue(LPCTSTR VarName, long Value)      // Initialise and add a new value
{
    COleVariant V;

    V_VT(&V) = VT_I4;
    V.lVal = Value;
    InitValue(VarName, V);
}

// String version
void VarMapBank::InitValue(LPCTSTR VarName, CString& Value)   // Initialise and add a new value
{
    COleVariant V;
    
    // Check the string hasn't already been initialised BEFORE 
    // allocating a new SysString
    if (!m_VarBank.Lookup(VarName, V))
    {
            // Not found the value so init a new BSTR for it
        V_VT(&V) = VT_BSTR;
        // Allocate the string and return to VBS
        V.bstrVal = Value.AllocSysString();
        InitValue(VarName, V);
    }
}

/////////////////////////////////////////////////////////////////////////////
// Resets a value in the bank, removing its entry
// Parameters:
//      VarName is the name of the value to reset - NB Case sensitive !
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void  VarMapBank::ResetValue(LPCTSTR VarName)
{
    COleVariant V;
    
    // Check the string hasn't already been initialised BEFORE 
    // allocating a new SysString
    if (m_VarBank.Lookup(VarName, V))
    {
        V.Clear();
        m_VarBank.RemoveKey(VarName);
    }
}

/////////////////////////////////////////////////////////////////////////////
// Resets the entire bank - deleting all variables
/////////////////////////////////////////////////////////////////////////////

void VarMapBank::ResetBank(void)                               // Deletes all values stored in the bank
{
    POSITION    P;
    CString     Name;
    COleVariant V;

    // Good so far so now need to go through and save each VARIANT
    P = m_VarBank.GetStartPosition();
    // All members
    while (P)
    {
        // Get the key and the value associated with the key
        m_VarBank.GetNextAssoc(P, Name, V);
        V.Clear();        
    }

    m_VarBank.RemoveAll();
}

/////////////////////////////////////////////////////////////////////////////
// Total number of persistents in the bank
/////////////////////////////////////////////////////////////////////////////
	
int	VarMapBank::GetCount(void)
{
	return m_VarBank.GetCount();
}

/////////////////////////////////////////////////////////////////////////////
// Iterating
/////////////////////////////////////////////////////////////////////////////

POSITION VarMapBank::GetFirst(void)								// Get the first 
{
	return m_VarBank.GetStartPosition();
}

BOOL VarMapBank::GetNext(POSITION &P, CString &VarName, VARIANT *Value)		// Get the next
{
	COleVariant V;

	m_VarBank.GetNextAssoc(P, VarName, V);

	VariantInit(Value);
    VariantCopyInd(Value, &V);

	return (P != NULL);
}

/////////////////////////////////////////////////////////////////////////////
// Gets a value from the bank
// Parameters:
//		Idx is the 
//      VarName is the name of the value
//      Value is the value to get
// Returns:
//      TRUE if it was a value in the bank
/////////////////////////////////////////////////////////////////////////////

BOOL VarMapBank::GetValue(LPCTSTR VarName, VARIANT *Value)        // Get a value from the bank
{
    COleVariant V;

    BOOL Ok = m_VarBank.Lookup(VarName, V);

    if (Ok)
    {
        VariantInit(Value);
        VariantCopyInd(Value, &V);
    }

    return Ok;
}


// double version
BOOL VarMapBank::GetValue(LPCTSTR VarName, double *Value)         // Get a value from the bank
{
    COleVariant V;

    if (m_VarBank.Lookup(VarName, V))
    {
        *Value = V.dblVal;
        return TRUE;
    }

    return FALSE;
}

// long version
BOOL VarMapBank::GetValue(LPCTSTR VarName, long *Value)         // Get a value from the bank
{
    COleVariant V;

    if (m_VarBank.Lookup(VarName, V))
    {
        *Value = V.lVal;
        return TRUE;
    }

    return FALSE;
}

// string version
BOOL VarMapBank::GetValue(LPCTSTR VarName, CString &Value)         // Get a value from the bank
{
    COleVariant V;

    if (m_VarBank.Lookup(VarName, V))
    {
        Value = V.bstrVal;
        return TRUE;
    }

    return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Parameters:
//      VarName is the name of the value to set - NB Case sensitive !
//      Value is the value to set
// Returns:
//      TRUE if it was a value in the bank
// Modification: Set the value even if it was not in the bank originally - dcb
/////////////////////////////////////////////////////////////////////////////

void VarMapBank::SetValue(LPCTSTR VarName, VARIANT Value)         // Set a value in the bank
{
    COleVariant V, VCpy;
    BOOL        Ok = FALSE;
	HRESULT	vOK;

	// Copy the variant
	// First make sure copy is clear
	vOK = VariantClear(&VCpy);
	if (vOK != S_OK)
		// Error clearing variant - do something - here throw exception
		// using generic IDS_OPERATIONFAILED
		AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);

	// If the original variant was not empty, then copy it
	// Look at returned status
	vOK = VariantCopy(&VCpy, &Value);
	if (vOK != S_OK)
		// Error copying variant, throw different exception from above
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);

	// VB may automatically bind a var to a particular type
	// so rebind to either a long/double - not neccessary for strings
	switch (Value.vt)
	{
	case VT_I2:
		VCpy.lVal = (long) Value.iVal;
		VCpy.vt = VT_I4;
		break;
	case VT_R4:
		VCpy.dblVal = (double) Value.fltVal;
		VCpy.vt = VT_R8;
		break;
	}

    // dcb mod - set at even if not in the bank
    m_VarBank.SetAt(VarName, VCpy);
}

// double version
void VarMapBank::SetValue(LPCTSTR VarName, double Value)         // Set a value in the bank
{
    VARIANT V;

    V_VT(&V) = VT_R8;
    V.dblVal = Value;
    SetValue(VarName, V);
}

// long version
void VarMapBank::SetValue(LPCTSTR VarName, long Value)         // Set a value in the bank
{
    VARIANT V;

    V_VT(&V) = VT_I4;
    V.lVal = Value;
    SetValue(VarName, V);
}

void VarMapBank::SetValue(LPCTSTR VarName, CString& Value)         // Set a value in the bank
{
    VARIANT V;

    V_VT(&V) = VT_BSTR;

    V.bstrVal = Value.AllocSysString();
    
    SetValue(VarName, V);

	// Free the BSTR, now that SetValue(LPCSTR VarName, VARIANT Value) has copied it.
	SysFreeString(V.bstrVal);
}

/////////////////////////////////////////////////////////////////////////////
// Serialisation of the var bank
// Returns:
//      TRUE if succeeded
/////////////////////////////////////////////////////////////////////////////
    
BOOL  VarMapBank::Serialize(CArchive &ar)                       // Serialisation
{
    COleVariant     V;
    CString         Name;
    POSITION        P;
    int             i, Count;

    if (ar.IsStoring())
    {
        ar << m_VarBank.GetCount();
        // Good so far so now need to go through and save each VARIANT
        P = m_VarBank.GetStartPosition();
        // All members
        while (P)
        {
            // Get the key and the value associated with the key
            m_VarBank.GetNextAssoc(P, Name, V);
            ar << Name;
            ar << V;            
        }
    }
    else
    {
        ar >> Count;

        for (i = 0; i < Count; i++)
        {
            ar >> Name;
            ar >> V;
            // Clear the entry in the bank
            ResetValue(Name);
            // Add to the bank
            InitValue(Name, V);
        }
    }

    return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// Save the bank to a disk file
// Parameters:
//      FileNameAndPath is the file name and path to save to
// Returns:
//      TRUE if succeeded
/////////////////////////////////////////////////////////////////////////////

BOOL VarMapBank::Save(LPCTSTR FileNameAndPath)                 // Save the bank to disk
{
    CFile           fBank;
    CFileException  eFile;

    if (FileNameAndPath == _T(""))
        return FALSE;

    // Try and open the file
    if (!fBank.Open(FileNameAndPath, CFile::modeCreate | CFile::modeWrite, &eFile))
    {
#ifdef _DEBUG
		CString text;
		text.Format("Persistents Save : File ""%s"" could not be opened: %d\n", FileNameAndPath, eFile.m_cause);
        afxDump << text;
#endif
        return FALSE;
    }
    
    // Write the number of elements in the bank
 	CArchive PersistentArchive(&fBank, CArchive::store, 4096, NULL);

    Serialize(PersistentArchive);
    PersistentArchive.Close();
	fBank.Close();

    return TRUE;
}

/////////////////////////////////////////////////////////////////////////
// Load the bank from a disk file
// Parameters:
//      FileNameAndPath is the file name and path to save to
// Returns:
//      TRUE if succeeded
/////////////////////////////////////////////////////////////////////////////

BOOL VarMapBank::Load(LPCTSTR FileNameAndPath)                  // Load the bank from disk
{
    CFile           fBank;
    CFileException  eFile;

    if (FileNameAndPath == _T(""))
        return FALSE;

    // Try and open the file
    if (!fBank.Open(FileNameAndPath, CFile::modeRead, &eFile))
    {
#ifdef _DEBUG
		CString text;
		text.Format("Persistents Load : File ""%s"" could not be opened: %d\n", FileNameAndPath, eFile.m_cause);
        afxDump << text;
#endif
        return FALSE;
    }

    // Write the number of elements in the bank
    CArchive PersistentArchive(&fBank, CArchive::load, 4096, NULL);

    Serialize(PersistentArchive);
    PersistentArchive.Close();
	fBank.Close();

    return TRUE;
}
