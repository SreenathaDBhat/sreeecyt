/////////////////////////////////////////////////////////////////////////////
//
//
// ScriptProc.cpp : Defines the main entry routines for the DLL.
//
// Main point of access to the script processing DLL
//
// Written By Karl Ratcliff 12042001
//
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//  ScriptProcessorState is used to keep track of initialisation of the script
//  and processing of it.
//  
//  Before loading and executing a script ScriptProcessorState must be at the
//  INITIALISED state. In order to execute the script ScriptProcessorState
//  must be at the SCRIPT_READY state. To release the script engine
//  ScriptProcessorState must be either INITIALISED or SCRIPT_READY. This
//  is to prevent calls to InitialiseScriptProcessor(), ExecuteScript(),
//  ReleaseScriptProcessor() being done in the wrong order which may or
//  may not result in a nasty crash.
//
//  The script itself is also a global object which is preparsed by the
//  ReadScriptFile() function. ReadScriptFile() is responsible for the
//  pre-processing of macro 'include statements and also for obtaining
//  other relevant processing information such as spectrachrome lists
//  etc. required by the script for processing.
//
//  Initialise(), Execute() and Release() must all be called from the same
//  thread. This is a COM thing.
//
/////////////////////////////////////////////////////////////////////////////
//
//	19Jun2001	JMB	Added support for classifier script object
//	19Oct2001	WH	Added support for aitiff script object
//	15Apr2002	JMB	Added support for relative include paths


#include "stdafx.h"
#include <afxdllx.h>
#include <activscp.h>
#include <afxtempl.h>
#include <direct.h>
#include "assert.h"

#ifdef _SCRIPTPROC_MEM_DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#include "scriptclsid.h"
#include "vcmemrect.h"
#include "cputopology.h"
#include "vcblob.h"
#include "vcgeneric.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "woolzif.h"
#include "vcthresh.h"
#include "vcImProc.h"
#include "commonscript.h"
#include "uts.h"
#include "piccmd.h"
#include "scriptutils.h"
#include "picman.h"
#include "picman.h"
#include "scriptutils.h"
#include "scriptpersist.h"
#include "vbscript.h"
#include "utsscript.h"
#include "mskscript.h"

#include "spotsscript.h"

#include "woolzscript.h"
#include "improcscript.h"
#include "ClassifierScript.h"
#include "AitiffScript.h"
#include "gpuscript.h"
#include "CMeasurement.h"
#include "CSyncObjects.h"
#include "cmeasurement.h"
#include "ScriptParams.h"
#include <ImageBits.h>
#include "scriptproc.h"
#ifdef _IPECUDA
#include "TIPScript.h"
#endif

#define ALPHANUM    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
        
/////////////////////////////////////////////////////////////////////////////
//
// Main entry point for the DLL
//
/////////////////////////////////////////////////////////////////////////////

static AFX_EXTENSION_MODULE ScriptProcDLL = { NULL, NULL };

extern "C" int APIENTRY DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("SCRIPTPROC.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(ScriptProcDLL, hInstance))
			return 0;

		// Insert this DLL into the resource chain
		// NOTE: If this Extension DLL is being implicitly linked to by
		//  an MFC Regular DLL (such as an ActiveX Control)
		//  instead of an MFC application, then you will want to
		//  remove this line from DllMain and put it in a separate
		//  function exported from this Extension DLL.  The Regular DLL
		//  that uses this Extension DLL should then explicitly call that
		//  function to initialize this Extension DLL.  Otherwise,
		//  the CDynLinkLibrary object will not be attached to the
		//  Regular DLL's resource chain, and serious problems will
		//  result.

		new CDynLinkLibrary(ScriptProcDLL);
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("SCRIPTPROC.DLL Terminating!\n");
		// Terminate the library before destructors are called
		AfxTermExtensionModule(ScriptProcDLL);
	}
	return 1;   // ok
}

/////////////////////////////////////////////////////////////////////////////
//
// Constructor and Destructor
//
/////////////////////////////////////////////////////////////////////////////

CScriptProcessor::CScriptProcessor(DWORD InstanceId/*=0*/)
{
    CString MutName;

    // Init to safe values
    m_ScriptProcessorState = stateUnInitialised;
    m_nMacroLines = 1;
   
	m_pVBScriptObject = new VBscript;                
    m_pUTSScriptObject = new UTSscript;              
    m_pMSKScriptObject = new MSKscript;              
	m_pClassifierScriptObject = new ClassifierScript;
	m_pAiTiffScriptObject = new AiTiffScript;
	m_pSpotsScriptObject = new SpotsScript;

    m_pWoolzScriptObject = new WoolzScript;
	m_pImprocScriptObject = new ImprocScript;
    m_pGPUIPScriptObject = new GPUIPScript;


#ifdef _IPECUDA
	m_pTPUIPScriptObject = new TIPScript;
#endif

	m_pUTSMemRect = new CUTSMemRect;

	m_pUTSBlob = new CUTSBlob(m_pUTSMemRect);
	
	m_pUTSGeneric = new CUTSGeneric;
	if (!m_pUTSGeneric->Initialise())
	{
		OutputDebugString(_T("ZGENERIC.DLL MISSING\r\n"));
		assert(0);
	}

	m_pWoolzLib   = new CWoolzIP(m_pUTSMemRect);
	if (!m_pWoolzLib->Initialise())
	{
		OutputDebugString(_T("COULD NOT INITIALISE WOOLZLIB\r\n"));
		assert(0);
	}

	m_pUTSDraw    = new CUTSDraw(m_pUTSMemRect);

	m_pIPObjects  = new CIPObjects(m_pUTSMemRect, m_pUTSDraw, m_pUTSGeneric, m_pUTSBlob, m_pWoolzLib);

	m_pUTSScriptObject->Initialise(m_pIPObjects);
	m_pWoolzScriptObject->Initialise(m_pIPObjects);
	m_pMSKScriptObject->Initialise(m_pIPObjects);
	m_pSpotsScriptObject->Initialise(m_pIPObjects);
	m_pImprocScriptObject->Initialise(m_pIPObjects);
	m_pGPUIPScriptObject->Initialise(m_pIPObjects);
	
	m_pAiTiffScriptObject->Initialise(m_pIPObjects);
	m_pVBScriptObject->Init(m_pIPObjects);

    m_pTheScript = new CString;
	*m_pTheScript = _T("");

    // Construct the mutex name from the process id as it is quite safe
    // to run the multiple instances of the script processor in different
    // processes but not different threads of the same process.
    MutName.Format(_T("%8x_SCRIPTPROC_MUTEX"), GetCurrentProcessId());
    m_pScriptExecMutex = new CSyncObjectMutex(SyncObjectCreate, MutName);
    m_BSTRScript = NULL;
	m_Id = InstanceId;

	m_pVBScriptObject->SetInstanceId(m_Id);
}

CScriptProcessor::~CScriptProcessor()
{
	m_ScriptIO.Reset();

	delete m_pClassifierScriptObject;
    delete m_pVBScriptObject;               // AII script object 
    delete m_pUTSScriptObject;              // UTS script object
    delete m_pMSKScriptObject;              // UTS Mask script object
	delete m_pAiTiffScriptObject;
    delete m_pSpotsScriptObject;
    delete m_pWoolzScriptObject;
	delete m_pImprocScriptObject;
    delete m_pGPUIPScriptObject;

#ifdef _IPECUDA
	delete m_pTPUIPScriptObject;
#endif
	delete m_pTheScript;
    delete m_pScriptExecMutex;

	delete m_pIPObjects;

	delete m_pUTSMemRect;
	delete m_pUTSBlob;
	delete m_pUTSGeneric;
	delete m_pWoolzLib;
	delete m_pUTSDraw;

    SysFreeString(m_BSTRScript);
}

/////////////////////////////////////////////////////////////////////////////
// Initialise anything related to the script processing engine
// Requires the ScriptProcessorState to be UNINITIALISED
// Set SavePersistents = FALSE if you do not wish to store persistent
// settings on disk
// Returns:
//      TRUE if successful
/////////////////////////////////////////////////////////////////////////////

BOOL CScriptProcessor::InitialiseScriptProcessor(LPCTSTR PersistentsFileName, LPCTSTR Qpath, LPCTSTR Qin, LPCTSTR Qout, BOOL SavePersistents/* = TRUE*/)
{
    HRESULT hr;
	CString PersistFileNameAndPath;

    m_BSTRScript = NULL;

    g_iActiveScriptSite.ResetScriptErrors();        // Reset script processing error info

	// Make the temp dir path if non existent
	_tmkdir(Qpath);

    m_bFirstTime = TRUE;


    // Only init the script engine if previously uninitialised
    if (m_ScriptProcessorState == stateUnInitialised)
    {
        // Initialise this sides (the App side) access to the queues
        // NOTE the names are swapped from those implemented in
        // the VB script class so that the members of VBScriptIO
        // Get and Set to the right queues depending on the data flow
        if (!m_ScriptIO.InitialiseScriptQs(Qpath, Qin, Qout))
            return FALSE;

        // Could not initialise Qs on the script side
		// NOTE Qs are reverse for the VB script processor
        if (!m_pVBScriptObject->InitialiseScriptQs(Qpath, Qout, Qin))
			return FALSE;

        // Set the save persistents flag
        m_pVBScriptObject->m_bSavePersistents = SavePersistents;
        // Initialise the COM library on the current apartment
        OleInitialize(NULL);

        // Initialize the IActiveScriptSite implementation with a
        // script object's IUnknown interface...
        // NOTE Add a new IUnknown line here for every new class
        // exposed to the scripting engine
        g_iActiveScriptSite.m_pUnkVBScriptObject		= m_pVBScriptObject->GetInterface(&IID_IUnknown);
        g_iActiveScriptSite.m_pUnkUTSScriptObject		= m_pUTSScriptObject->GetInterface(&IID_IUnknown);
        g_iActiveScriptSite.m_pUnkMSKScriptObject		= m_pMSKScriptObject->GetInterface(&IID_IUnknown);
		g_iActiveScriptSite.m_pUnkClassifierScriptObject = m_pClassifierScriptObject->GetInterface(&IID_IUnknown);
		g_iActiveScriptSite.m_pUnkAiTiffScriptObject	= m_pAiTiffScriptObject->GetInterface(&IID_IUnknown);
        g_iActiveScriptSite.m_pUnkSpotsScriptObject		= m_pSpotsScriptObject->GetInterface(&IID_IUnknown);
        g_iActiveScriptSite.m_pUnkWoolzScriptObject		= m_pWoolzScriptObject->GetInterface(&IID_IUnknown);
		g_iActiveScriptSite.m_pUnkImprocScriptObject	= m_pImprocScriptObject->GetInterface(&IID_IUnknown);
		g_iActiveScriptSite.m_pUnkGPUIPScriptObject		= m_pGPUIPScriptObject->GetInterface(&IID_IUnknown);
#ifdef _IPECUDA
		g_iActiveScriptSite.m_pUnkTIPIPScriptObject = m_pTPUIPScriptObject->GetInterface(&IID_IUnknown);
#endif

        // Start inproc script engine, VBSCRIPT.DLL
        hr = CoCreateInstance(CLSID_VBScript, NULL, CLSCTX_INPROC_SERVER, 
                              IID_IActiveScript, (void **)&m_iActiveScript); 
        if (!HRVERIFY(hr, _T("CoCreateInstance() for CLSID_VBScript")))
            return FALSE;
        
        // Get engine's IActiveScriptParse interface.
        hr = m_iActiveScript->QueryInterface(IID_IActiveScriptParse, (void **)&m_iActiveScriptParse); 
        if (!HRVERIFY(hr, _T("QueryInterface() for IID_IActiveScriptParse")))
            return FALSE;
        
        // Give engine our IActiveScriptSite interface...
        hr = m_iActiveScript->SetScriptSite(&g_iActiveScriptSite); 
        if (!HRVERIFY(hr, _T("IActiveScript::SetScriptSite()")))
            return FALSE;

        // Give the engine a chance to initialize itself...
        hr = m_iActiveScriptParse->InitNew();
        if (!HRVERIFY(hr, _T("IActiveScriptParse::InitNew()")))
            return FALSE;

		// Load the persistents file
		PersistFileNameAndPath.Format(_T("%s\\%s"), Qpath, PersistentsFileName);
		m_pVBScriptObject->LoadPersistents(PersistFileNameAndPath);
    }
    // Go back to the uninitialised state
    m_ScriptProcessorState = stateUnInitialised;
    // Initialise the image processing libraries etc.   	
    m_ScriptProcessorState = stateInitialised;     // Set the state correctly (allows release)            
        
    return (m_ScriptProcessorState == stateInitialised);
}

/////////////////////////////////////////////////////////////////////////////
// Release the script processor
// Free anything created by initialise.
// Requires the ScriptProcessorState to be INITIALISED or SCRIPT_READY
/////////////////////////////////////////////////////////////////////////////

void CScriptProcessor::ReleaseScriptProcessor(void)
{
    // Only release if previously initialised and/or in the script ready state
    if (m_ScriptProcessorState == stateInitialised || m_ScriptProcessorState == stateScriptReady)
    {
        m_pScriptExecMutex->Release();      // Allow other threads access
        // Allow initialisation again
        m_ScriptProcessorState = stateUnInitialised;
        // Release engine...
        m_iActiveScriptParse->Release();
        m_iActiveScript->Release(); 
        // Release COM libraries
        OleUninitialize();

        // Do any other clean up here
    }
}    

/////////////////////////////////////////////////////////////////////////////
// Load a script ready to be run by the script processor
// Requires the ScriptProcessorState to be INITIALISED or SCRIPT_READY
// Parameters:
//      ScriptName      - the name of the script file to run
//      Expand          - = TRUE to load and expand macros into script 
//      IgnoreInit      - = Set TRUE to load a script without 
//							InitialiseScriptProcessor first
// Returns:
//      TRUE if loaded OK, FALSE otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL CScriptProcessor::LoadScript(LPCTSTR ScriptName, BOOL Expand, BOOL IgnoreInit /*=FALSE*/)
{
    // Blank the script in memory
    *m_pTheScript = "";
	// Reset number of macro lines
	m_nMacroLines = 0;

    // Has the script processor been initialised/previouslt ran a different script
    if (m_ScriptProcessorState == stateInitialised || m_ScriptProcessorState == stateScriptReady || IgnoreInit)
    {
        // Read in the script from disk
        if (ReadScriptFile(ScriptName, m_pTheScript, TRUE, Expand, &m_nMacroLines) >= 0)
        {
            // Strictly enforce option explicit so tack it on, if not already there
            if ((m_pTheScript->Find(_T("Explicit")) == -1) && (m_pTheScript->Find(_T("explicit")) == -1))
                *m_pTheScript = _T("Option Explicit\r\n") + (*m_pTheScript);
            if (m_BSTRScript)
                SysFreeString(m_BSTRScript);
            // Parse the code scriptlet...
            m_BSTRScript = m_pTheScript->AllocSysString(); 
            // Script parsed o.k. and ready to run
			if (!IgnoreInit)
				m_ScriptProcessorState = stateScriptReady;
	
            return TRUE;
        }

        // If we got to here the script did not load from file correctly
        // so revert to the initialised state with no script available
        // for execution by the script processing engine.
        if (!IgnoreInit)
			m_ScriptProcessorState = stateInitialised;
    }

    // If we got to here then the script processor had not initialised
    // correctly
    return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// Parses a script
// Requires the ScriptProcessorState to be SCRIPT_READY
// Parameters:
//      Script          - a script filename
// Returns:
//      TRUE if parsed OK, FALSE otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL CScriptProcessor::ParseScript(void)
{
    // Only run the script if the script previously loaded and 
    // preparsed O.K
    if (m_ScriptProcessorState == stateScriptReady/* && m_pScriptExecMutex->Grab(120000)*/)
    {
        // Set the number of extra lines included 
        g_iActiveScriptSite.SetNumMacroLines(m_nMacroLines);

        // Reset previous script errors
        g_iActiveScriptSite.ResetScriptErrors();

        // Go to the initialised state - will wipe out any previous run-time state
        m_iActiveScript->SetScriptState(SCRIPTSTATE_INITIALIZED);

        EXCEPINFO ei;
        m_iActiveScriptParse->ParseScriptText(m_BSTRScript, NULL, NULL, NULL, 0, 0, 0L, NULL, &ei);

        // Allow a different thread to run a script
        //m_pScriptExecMutex->Release();
        // Done OK ?
        return !g_iActiveScriptSite.m_bErrorOccurred;
    }

    // Script not loaded in memory/preparsed o.k.
    return FALSE;               
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
BOOL CScriptProcessor::AddNamedItems(void)
{
	HRESULT hr;

	// Set the number of extra lines included 
	g_iActiveScriptSite.SetNumMacroLines(m_nMacroLines);

	// Reset previous script errors
	g_iActiveScriptSite.ResetScriptErrors();

	// Go to the initialised state - will wipe out any previous run-time state
	m_iActiveScript->SetScriptState(SCRIPTSTATE_INITIALIZED);

	// Add a root-level item to the engine's name space...
	// NOTE For each new class exposed to the scripting engine
	// add it as a named item with a unique name here
	hr = m_iActiveScript->AddNamedItem(AI_OBJECT_NAME,  SCRIPTITEM_AIOBJECT);
	if (!HRVERIFY(hr,  _T("IActiveScript::AddNamedItem()")))
	{
		m_pScriptExecMutex->Release();
		return FALSE;
	}

	hr = m_iActiveScript->AddNamedItem(AI_UTS_NAME,  SCRIPTITEM_AIOBJECT);
	if (!HRVERIFY(hr,  _T("IActiveScript::AddNamedItem()")))
	{
		m_pScriptExecMutex->Release();
		return FALSE;
	}

	hr = m_iActiveScript->AddNamedItem(AI_UTSMASK_NAME,  SCRIPTITEM_AIOBJECT);
	if (!HRVERIFY(hr,  _T("IActiveScript::AddNamedItem()")))
	{
		m_pScriptExecMutex->Release();
		return FALSE;
	}

	hr = m_iActiveScript->AddNamedItem(AI_CLASSIFIER_NAME,  SCRIPTITEM_AIOBJECT);
	if (!HRVERIFY(hr,  _T("IActiveScript::AddNamedItem()")))
	{
		m_pScriptExecMutex->Release();
		return FALSE;
	}

	hr = m_iActiveScript->AddNamedItem(AI_AITIFF_NAME,  SCRIPTITEM_AIOBJECT);
	if (!HRVERIFY(hr,  _T("IActiveScript::AddNamedItem()")))
	{
		m_pScriptExecMutex->Release();
		return FALSE;
	}

	hr = m_iActiveScript->AddNamedItem(AI_SPOTS_NAME,  SCRIPTITEM_AIOBJECT);
	if (!HRVERIFY(hr,  _T("IActiveScript::AddNamedItem()")))
	{
		m_pScriptExecMutex->Release();
		return FALSE;
	}

	hr = m_iActiveScript->AddNamedItem(AI_WOOLZ_NAME,  SCRIPTITEM_AIOBJECT);
	if (!HRVERIFY(hr,  _T("IActiveScript::AddNamedItem()")))
	{
		m_pScriptExecMutex->Release();
		return FALSE;
	}

	hr = m_iActiveScript->AddNamedItem(AI_IMPROC_NAME,  SCRIPTITEM_AIOBJECT);
	if (!HRVERIFY(hr,  _T("IActiveScript::AddNamedItem()")))
	{
		m_pScriptExecMutex->Release();
		return FALSE;
	}

	hr = m_iActiveScript->AddNamedItem(AI_GPUIP_NAME,  SCRIPTITEM_AIOBJECT);
	if (!HRVERIFY(hr,  _T("AI_GPUIP_NAME IActiveScript::AddNamedItem()")))
	{
		m_pScriptExecMutex->Release();
		return FALSE;
	}

#ifdef _IPECUDA
	hr = m_iActiveScript->AddNamedItem(AI_TIP_NAME,  SCRIPTITEM_AIOBJECT);
	if (!HRVERIFY(hr,  _T("AI_TIP_NAME IActiveScript::AddNamedItem()")))
	{
		m_pScriptExecMutex->Release();
		return FALSE;
	}
#endif
	return TRUE;

}

/////////////////////////////////////////////////////////////////////////////
// Run and execute a script
// Requires the ScriptProcessorState to be SCRIPT_READY
// Parameters:
//      None
// Returns:
//      TRUE if executed it OK, FALSE otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL CScriptProcessor::ExecuteScript()
{
    // Only run the script if the script previously loaded and 
    // preparsed O.K
    if (m_ScriptProcessorState == stateScriptReady)
    {
        if (!AddNamedItems())
			return FALSE;

		// Holds any error info
        ScriptError LastError;

        EXCEPINFO ei;
        m_iActiveScriptParse->ParseScriptText(m_BSTRScript, NULL, NULL, NULL, 0, 0, 0L, NULL, &ei);

		return DeferredExecute();
    }

    // Script not loaded in memory/preparsed o.k.
    return FALSE;               
}
	
/////////////////////////////////////////////////////////////////////////////
// Run and execute a script
// Requires the ScriptProcessorState to be SCRIPT_READY
// Parameters:
//     DWORD dwSourceContext	     
// Returns:
//      TRUE if executed it OK, FALSE otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL CScriptProcessor::DebugScript(DWORD dwSourceContext)
{  
    // Only run the script if the script previously loaded and 
    // preparsed O.K
    if (m_ScriptProcessorState == stateScriptReady)
    {
         if (!AddNamedItems())
			 return FALSE;

		// Holds any error info
        ScriptError LastError;

        EXCEPINFO ei;
        m_iActiveScriptParse->ParseScriptText(m_BSTRScript, NULL, NULL, NULL, dwSourceContext, 0, SCRIPTTEXT_ISVISIBLE | SCRIPTTEXT_ISPERSISTENT , NULL, &ei);
    }

    return TRUE;               
}

BOOL CScriptProcessor::DeferredExecute(void)
{
	// Set the engine state. This line actually triggers the execution
	// of the script. The script engine is an inproc engine so this line
	// does not return until script processing has completely finished.
	m_iActiveScript->SetScriptState(SCRIPTSTATE_CONNECTED);

	
	// Done OK ?
	return !g_iActiveScriptSite.m_bErrorOccurred;
}

/////////////////////////////////////////////////////////////////////////////
// Break script execution
/////////////////////////////////////////////////////////////////////////////

void CScriptProcessor::Break(unsigned long ThreadID)
{
	EXCEPINFO ei;
    memset(&ei, 0, sizeof(EXCEPINFO));

    if (m_ScriptProcessorState == stateScriptReady)
	{
		m_iActiveScript->SetScriptState(SCRIPTSTATE_DISCONNECTED);
		m_iActiveScript->InterruptScriptThread(ThreadID, &ei, SCRIPTINTERRUPT_DEBUG);
	}
}

/////////////////////////////////////////////////////////////////////////////
// Set the operating mode for the VB scripting engine
// Parameters:
//      Mode        - the mode to work in - see enum ScriptExecMode
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void CScriptProcessor::SetExecutionMode(ScriptExecMode Mode)
{
    // NOTE for every object exposed to the scripting engine
    // must set its own execution mode property here.
    m_pVBScriptObject->m_ExecutionMode    = Mode;
    m_pUTSScriptObject->m_ExecutionMode   = Mode;
    m_pMSKScriptObject->m_ExecutionMode   = Mode;
    m_pSpotsScriptObject->m_ExecutionMode = Mode;
    m_pWoolzScriptObject->m_ExecutionMode = Mode;
}

/////////////////////////////////////////////////////////////////////////////
// Resets the input and output Q's etc for the scripting engine
// Parameters:
//      None
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void CScriptProcessor::Reset(void)
{
    // NOTE for every object exposed to the scripting engine
    // must call its reset member here.
    m_pVBScriptObject->Reset();       // Reset the VB script side
    m_pUTSScriptObject->Reset();      
    m_pMSKScriptObject->Reset();
#ifdef _IPECUDA
	m_pTPUIPScriptObject->Reset();
#endif
	m_pClassifierScriptObject->Reset();
    m_ScriptIO.Reset();                 // Reset the Qs on this side
    m_pWoolzScriptObject->Reset();
 }


/////////////////////////////////////////////////////////////////////////////
// Write a CString to a script file
// Parameters:
//      FileName    - File name and path for the script
//      TheScript   - CString Buffer to hold the script in
// Returns:
//      TRUE if successful, FALSE otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL CScriptProcessor::SaveScript(LPCTSTR FileName, CString& TheScript)
{
    CFile           TheScriptFile;
    CFileException  FileErr;
    CString         Line;
    CString         ScriptCopy;
    int             Len;
    TCHAR           buf[1024];

    if (TheScriptFile.Open(FileName, CFile::modeWrite | CFile::modeCreate, &FileErr))
    {
        // Take a copy of the original script to dismantle
        ScriptCopy = TheScript;
        
        // For each line write it to file
        do
        {
            Line = ScriptCopy.Left(ScriptCopy.Find(_T("\n")));
            if (Line == _T(""))
                Line = _T("\n");
            _tcscpy(buf, Line); 
            TheScriptFile.Write(buf, _tcslen(buf));
            Len = ScriptCopy.GetLength() - Line.GetLength();
            ScriptCopy = ScriptCopy.Right(Len);
        }
        while (Len > 0);
        TheScriptFile.Close();
        return TRUE;
    }

    // If we get to here something went wrong
    return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Read a script into a CString
// Parameters:
//      FileName    - File name and path for the script
//      TheScript   - CString Buffer to hold the script in
//      TopLevel    - = TRUE when reading a script from outside of this 
//                      function (should always be the case)
//                    = FALSE when called recursively from within 
//                      this function
//      Expand      - = TRUE to load & expand macros into the script
//      MacroLines  - Number of lines included from other files
// Returns:
//      Number of lines read in, -1 otherwise
/////////////////////////////////////////////////////////////////////////////

int CScriptProcessor::ReadScriptFile(LPCTSTR FileName, CString *pTheScript, BOOL TopLevel, BOOL Expand, int *MacroLines)
{
    // Keep hold of the number of recursive levels for include directives
    static int IncludeDepth;
    static CString MacroList;      // Keep a list of previously included macros
    static int LineNumber;
	static CString scriptPath;

    BOOL FResult, Ok = TRUE;

    // Toplevel so re-init the include depth and macro list
    if (TopLevel)
    {
        IncludeDepth = 0;
        MacroList    = FileName;   // Add the top level file to the macrolist
        LineNumber   = 0;
        *MacroLines  = 0;

		// Get the path component of FileName, if any.
		// Find the last path separator in FileName, if any.
		scriptPath = FileName;
		int indexBS = scriptPath.ReverseFind('\\');
		int indexFS = scriptPath.ReverseFind('/');
		if (indexBS >= 0 || indexFS >= 0)
		{
			int indexSlash = (indexBS > indexFS) ? indexBS : indexFS;
			scriptPath = scriptPath.Left(indexSlash + 1);
		}
		else
			scriptPath = ""; // No path
    }

    // Open Up File To Read From
    CStdioFile TheScriptFile;
    CFileException FileErr;
    
    // Try and open the file 
    if (!TheScriptFile.Open(FileName, CFile::modeRead | CFile::shareDenyNone, &FileErr))
        return SCRIPT_FILE_ERROR;

    // Contains the line of VBScript to add
    CString FileLine, Macro, MacroCode;

    // Check the file length for size
    if (TheScriptFile.GetLength() > MAX_SCRIPT_SIZE || TheScriptFile.GetLength() == 0)
    {
        TheScriptFile.Close();
        return SCRIPT_FILE_ERROR;
    }

    // Set the script to blank
    *pTheScript = "";
    
    // Read the script file line by line until EOF
    do
    {
        FResult = TheScriptFile.ReadString(FileLine);
        if (FResult)
        {
            // Add the next line of VB script code with a newline and carriage return
            *pTheScript += FileLine;
            *pTheScript += _T("\r\n");
            LineNumber++;
            // Scan the line looking for the macro include directive
            // Only do if the expand flag is TRUE
            if ((FileLine.Find(_T("'include")) != -1) && Expand)
            {
                // Increment the include depth
                IncludeDepth++;
                // Have we exceeded the max recursion level
                if (IncludeDepth > MAX_INCLUDE_DEPTH)
                    return FALSE;

                // Extract the macro name from the right part of the line
                Macro = FileLine.Right(FileLine.GetLength() - _tcslen((LPCTSTR) FileLine.SpanIncluding(_T("'include"))));
                // Remove any leading spaces and quotes
                Macro.TrimLeft(_T(" \""));
                // Remove any trailing quotes
                Macro.TrimRight(_T("\""));
                // Check to make sure not already included - prevent
                // circular includes...
                if (MacroList.Find(Macro) == -1)
                {
                    // Now read in the macro and bolt it in place before the next
                    // script code line and after the include directive. If not successful 
                    // then flag the error
                    int L = ReadScriptFile(Macro, &MacroCode, FALSE, Expand, MacroLines);

					if (L == -1 && scriptPath.GetLength() > 0)
					{
						// Failed to load macro by treating the supplied path as an absolute
						// path, or a path relative to the current directory.
						// Try appending the file name to the path for the top-level script.
						// This way, include directives can specify include files relative
						// to the directory the script they are being included in is.
						CString fullPath = scriptPath + Macro;
						L = ReadScriptFile(fullPath, &MacroCode, FALSE, Expand, MacroLines);
					}

                    if (L == -1)
                        Ok = FALSE;
                    else
                    {
						int crPos = 0, NLines = 0;

						while (crPos != -1)
						{
							crPos = MacroCode.Find(_T("\r\n"), crPos + 1);
							if (crPos != -1) 
								NLines++;
						}
						L = NLines;
                        *MacroLines += L;
                        LineNumber += L;
                        *pTheScript += MacroCode;     // Insert the macro code
                        // Keep the new macro in list, so cant include again
                        MacroList += Macro;         
                    }
                }
                // Decrement the include depth 
                if (IncludeDepth)
                    IncludeDepth--;
            }

        }
    }
    while (FResult && Ok);
    
    // Close the file
    TheScriptFile.Close();
    
    // Return the result
    if (Ok)
        return LineNumber;
    else
        return SCRIPT_FILE_ERROR;          // Failed
}

/////////////////////////////////////////////////////////////////////////////
// Get the current script
// Parameters:
//  Script - a CString script to run
// Returns:
//  TRUE if OK
/////////////////////////////////////////////////////////////////////////////

BOOL CScriptProcessor::GetScript(CString &Script)
{
    if (m_ScriptProcessorState == stateScriptReady)
    {
        Script = *m_pTheScript;
        return TRUE;
    }

    return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Set the current script
// Parameters:
//  Script - a CString script to run 
// Returns:
//  TRUE if OK
/////////////////////////////////////////////////////////////////////////////

BOOL CScriptProcessor::SetScript(CString &Script)
{
    if (m_ScriptProcessorState == stateInitialised || m_ScriptProcessorState == stateScriptReady)
    {
        *m_pTheScript = Script;
        m_ScriptProcessorState = stateScriptReady;
        return TRUE;
    }

    return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Flip an image vertically
// Parameters:
//      An image record describing the image
/////////////////////////////////////////////////////////////////////////////

void CScriptProcessor::FlipImage(VBS_IMAGE_REC *ImageData)
{
    BYTE *ptr;
    int i, j, lw;
    BYTE *out, *pout;

    lw = ImageData->Width * ImageBits::GetBytesPerPixel(ImageData->BitsPerPixel);
    out = new BYTE[ImageData->Height * lw];

    if (out)
    {
        pout = out;
        ptr = (BYTE *) ImageData->Address;
        ptr += ImageData->Width * (ImageData->Height - 1);

        for (j = ImageData->Height - 1; j >= 0; j--)
        {
            ptr = (BYTE *) ImageData->Address + lw * j;
            for (i = 0; i < lw; i++) *out++ = *ptr++;
        }

        memcpy(ImageData->Address, pout, ImageData->Height * lw);

        delete pout;
    }
}

/////////////////////////////////////////////////////////////////////////////
// Access to the script input and output Qs
// All functions return TRUE if o.k, FALSE otherwise
/////////////////////////////////////////////////////////////////////////////


// Put a long on the script input Q
BOOL CScriptProcessor::PutLong(long L)
{
    if (m_ScriptProcessorState == stateUnInitialised)
        return FALSE;
    else
        return m_ScriptIO.SendLongTo(L);
}

// Get a long from the script output Q
BOOL CScriptProcessor::GetLong(long *L)
{
    if (m_ScriptProcessorState == stateUnInitialised)
        return FALSE;
    else
        return m_ScriptIO.GetLongFrom(L);
}

// Put a double on the script input Q
BOOL CScriptProcessor::PutDouble(double D)
{
    if (m_ScriptProcessorState == stateUnInitialised)
        return FALSE;
    else
        return m_ScriptIO.SendDoubleTo(D);
}

// Get a double from the script output Q
BOOL CScriptProcessor::GetDouble(double *D)
{
    if (m_ScriptProcessorState == stateUnInitialised)
        return FALSE;
    else
        return m_ScriptIO.GetDoubleFrom(D);
}

// Put an image record on the script input Q
BOOL CScriptProcessor::PutImage(VBS_IMAGE_REC *ImageData)
{
    if (m_ScriptProcessorState == stateUnInitialised)
        return FALSE;
    else
        return m_ScriptIO.SendImageTo(ImageData);
}

// Get an image record from the script output Q
BOOL CScriptProcessor::GetImage(VBS_IMAGE_REC *ImageData)
{
    if (m_ScriptProcessorState == stateUnInitialised)
        return FALSE;
    else
        return m_ScriptIO.GetImageFrom(ImageData);
}

// Put a string on the script input Q
BOOL CScriptProcessor::PutString(CString& QString)
{
    if (m_ScriptProcessorState == stateUnInitialised)
        return FALSE;
    else
        return m_ScriptIO.SendStringTo(QString);
}

// Get a string from the script output Q
BOOL CScriptProcessor::GetString(CString& QString)
{
    if (m_ScriptProcessorState == stateUnInitialised)
        return FALSE;
    else
        return m_ScriptIO.GetStringFrom(QString);
}

// Get a generic measurement from the script output Q
BOOL CScriptProcessor::GetMeasurement(Measurement **pM)
{
    if (m_ScriptProcessorState == stateUnInitialised)
        return FALSE;
    else
        return (m_pVBScriptObject->GetMeasurement(pM));
}

/////////////////////////////////////////////////////////////////////////////
// Persistent variables - resets the default set
/////////////////////////////////////////////////////////////////////////////
    
void CScriptProcessor::ResetPersistentVariables(void)
{
    m_pVBScriptObject->ResetPersistentVars();
}

/////////////////////////////////////////////////////////////////////////////
// Get and set persistent longs, strings and doubles
/////////////////////////////////////////////////////////////////////////////

// Get a persistent from the script 
BOOL CScriptProcessor::GetPersistent(LPCTSTR VarName, VARIANT FAR *VarValue) 
{
    VARIANT V;

    BOOL ok = m_pVBScriptObject->m_pPersistentVariables->GetValue(VarName, &V);

    if (ok)
    {
        VariantInit(VarValue);          // Make sure we are initialised to something
        VariantCopy(VarValue, &V);
    }

    return ok;
}

// Get a persistent from the script 
void CScriptProcessor::SetPersistent(LPCTSTR VarName, VARIANT FAR& VarValue) 
{
    m_pVBScriptObject->m_pPersistentVariables->SetValue(VarName, VarValue);
}

// Set and get a long persistent 
void CScriptProcessor::SetLongPersistent(LPCTSTR VarName, long VarValue)
{
    m_pVBScriptObject->m_pPersistentVariables->SetValue(VarName, VarValue);
}

BOOL CScriptProcessor::GetLongPersistent(LPCTSTR VarName, long *VarValue)
{
	BOOL ok = FALSE;
	VARIANT VLong;
    
	if (GetPersistent(VarName, &VLong))
	{
		*VarValue = VLong.lVal;
		ok = TRUE;
	}

    return ok;
}

// Do the same for doubles
void CScriptProcessor::SetDoublePersistent(LPCTSTR VarName, double VarValue)
{
    m_pVBScriptObject->m_pPersistentVariables->SetValue(VarName, VarValue);
}

// Do the same for doubles
BOOL CScriptProcessor::GetDoublePersistent(LPCTSTR VarName, double *VarValue)
{
	BOOL ok = FALSE;
	VARIANT VDouble;

	if (GetPersistent(VarName, &VDouble))
	{
		*VarValue = VDouble.dblVal;
		ok = TRUE;
	}

    return ok;
}

// Do the same for strings
void CScriptProcessor::SetStringPersistent(LPCTSTR VarName, CString& VarValue)
{
    m_pVBScriptObject->m_pPersistentVariables->SetValue(VarName, VarValue);
}

BOOL CScriptProcessor::GetStringPersistent(LPCTSTR VarName, CString& VarValue)
{
	BOOL ok = FALSE;
	VARIANT VStr;

	if (GetPersistent(VarName, &VStr))
	{
		VarValue = VStr.bstrVal;
		ok = TRUE;
	}

	return ok;
}

/////////////////////////////////////////////////////////////////////////////
// Commit Persistent Vars to disk. This is done automatically on destruction
// of the script processor. This should be called if the script processor
// is kept permanently in memory and we are re-running scripts.
/////////////////////////////////////////////////////////////////////////////

void CScriptProcessor::CommitPersistents(void)
{
	m_pVBScriptObject->CommitPersistents();
}

/////////////////////////////////////////////////////////////////////////////
// Reload persistent variables - done on construction of the script processor
// The only time this should be used is if the processor is kept permanently
// open in memory.
/////////////////////////////////////////////////////////////////////////////

void CScriptProcessor::LoadPersistents(void)
{
	m_pVBScriptObject->LoadPersistents();
}

/////////////////////////////////////////////////////////////////////////////
// Deletes a persistent variable.
/////////////////////////////////////////////////////////////////////////////

void CScriptProcessor::DeletePersistent(LPCTSTR VarName)
{
	m_pVBScriptObject->m_pPersistentVariables->ResetValue(VarName);
}

/////////////////////////////////////////////////////////////////////////////
// Set the output window for the display of progress information.
// A value of NULL tells the processor that the handle is no longer used.
/////////////////////////////////////////////////////////////////////////////
    
void CScriptProcessor::SetProgressWindow(CWnd *pWnd, LPRECT pRect)
{
    m_pVBScriptObject->SetProgressWindow(pWnd, pRect);
}
	
void CScriptProcessor::SetProgressWindow(CWnd *pWnd)
{
    m_pVBScriptObject->SetProgressWindow(pWnd);
}

void CScriptProcessor::SetProgressWindowFromHandle(HWND hWnd, LPRECT pRect)
{
	m_pVBScriptObject->SetProgressWindowFromHandle(hWnd, pRect);
}

// Give the scripting a progress bar control to play with
void CScriptProcessor::SetProgressBar(CProgressCtrl *pProg)
{
	if (pProg)
	{
		pProg->SetPos(0);
		m_pVBScriptObject->SetProgressBarCtrl(pProg);
	}
}

/////////////////////////////////////////////////////////////////////////////
// Set a parent window for any GUI creation inside the scripting to use
/////////////////////////////////////////////////////////////////////////////

void CScriptProcessor::SetParentWindow(CWnd *pParent)
{
	m_pVBScriptObject->m_pGUIParent = pParent;
}

/////////////////////////////////////////////////////////////////////////////
// Pre-load an SVM model
/////////////////////////////////////////////////////////////////////////////

void CScriptProcessor::SVMModel(CString ModelName)
{
	m_pScriptExecMutex->Grab(120000);
    m_pClassifierScriptObject->LoadSVMModel(ModelName);
	m_pScriptExecMutex->Release();
}


////////////////////////////////////////////////////////////////////////////
// Remove all config variables that are not defined as assay variables.
// Assay variables are not declared at the top of the script so cannot
// be rebuilt directly from the script
////////////////////////////////////////////////////////////////////////////

void CScriptProcessor::RemoveScriptParams(CScriptParams *pParams)
{
    CParamSet *param_set;
    POSITION pos1, pos2;

   for (pos1 = pParams->m_ParamSetList.GetHeadPosition(); (pos2 = pos1) != NULL;)
   {
       param_set = pParams->m_ParamSetList.GetNext(pos1);
       if (param_set->m_CtrlType != "Assay")
       {
           param_set = pParams->m_ParamSetList.GetAt(pos2);			// Save the old pointer for deletion.
           pParams->m_ParamSetList.RemoveAt(pos2);
           delete param_set;                                        // Deletion avoids memory leak.
       }
   }
}

////////////////////////////////////////////////////////////////////////////
// Parse a script 'PARAM line 
////////////////////////////////////////////////////////////////////////////

CParamSet *CScriptProcessor::ParseParamLine(CString &line)
{
    int i, k, l;
    CString params_line, sub_string;
    CParamSet *param_set = NULL;

    params_line = line;

	// Trim leading and trailing whitespace
	params_line.Trim();

	// First non-whitespace character must be a comment character
	if (params_line.GetAt(0) != '\'')
		return NULL;

	// Remove comment character and any following whitespace
	params_line.Delete(0, 1);
	params_line.TrimLeft();

	// If this is a Param line, "Param" should now be at the start of the line string.
    if (params_line.Left(strlen("Param ")).CompareNoCase("Param ") == 0)
    {
		// Remove the Param directive from the line string.
        params_line.Delete(0, strlen("Param"));

        param_set = new CParamSet;

        i = 0;
        // Name - a persistent identifier string
        if ( (k = params_line.FindOneOf (ALPHANUM) ) > -1)
        {
            if ((l = params_line.Find (' ' , k) )> -1)
            {
                sub_string = params_line.Mid(k, l-k);
                params_line.Delete (0,l+1);
                params_line.TrimRight();
            }
            else
                sub_string = params_line;
            param_set->m_PersistentName = sub_string;
        }

        // control type - string within single quote marks
        if ((k = params_line.FindOneOf (ALPHANUM)) > -1)
        {
            if ((l = params_line.Find ("'", k) )> -1)
            {
                sub_string = params_line.Mid(k, l-k);
                params_line.Delete (0,l+1);
                params_line.TrimRight();
            }
            else
                sub_string = params_line;
            param_set->m_CtrlType = sub_string;
        }

        //label - string within single quote marks
        if ((k = params_line.FindOneOf (ALPHANUM)) > -1)
        {
            if ((l = params_line.Find ("'", k) )> -1)
            {
                sub_string = params_line.Mid(k, l-k);
                params_line.Delete (0,l+1);
                params_line.TrimRight();
            }
            else
                sub_string = params_line;
            param_set->m_Label = sub_string;
        }

        //range minimum - 
        if ((k = params_line.FindOneOf (ALPHANUM) )> -1)
        {
            if ((l = params_line.Find (' ', k)) > -1)
            {
                sub_string = params_line.Mid(k, l-k);
                params_line.Delete (0,l+1);
                params_line.TrimRight();
            }
            else
                sub_string = params_line;
            sscanf (sub_string,"%d",&(param_set->m_Min).llVal);
            V_VT(&param_set->m_Min) = VT_I4;
        }


        //range maximum -
        if ((k = params_line.FindOneOf (ALPHANUM)) > -1)
        {
            if ((l = params_line.Find (' ', k)) > -1)
            {
                sub_string = params_line.Mid(k, l-k);
                params_line.Delete (0,l+1);
                params_line.TrimRight();
            }
            else 
                sub_string = params_line;
            sscanf (sub_string,"%d",&(param_set->m_Max).llVal);
            V_VT(&param_set->m_Max) = VT_I4;
        }

        //default value -
        if ((k = params_line.FindOneOf (ALPHANUM)) > -1)
        {
            if ((l = params_line.Find (' ', k)) > -1)
            {
                sub_string = params_line.Mid(k, l-k);
                params_line.Delete (0,l+1);
                params_line.TrimRight();
            }
            else 
                sub_string = params_line;

            sscanf (sub_string,"%d",&(param_set->m_Value).llVal);
            V_VT(&param_set->m_Value) = VT_I4;
			param_set->m_Default = param_set->m_Value;
        }
    }

    return param_set;
}

////////////////////////////////////////////////////////////////////////////
// Build any script parameters from the script
////////////////////////////////////////////////////////////////////////////

CScriptParams *CScriptProcessor::BuildConfigFromScript(void)
{
    CParamSet		*pPS;
	CScriptParams	*pCSP = NULL;
	CString			ScriptLine, RemainingScript;
	int				NLPos;

	RemainingScript = *m_pTheScript;

	pCSP = new CScriptParams;

	do
	{
		NLPos = RemainingScript.Find(_T("\r\n"));

		if (NLPos >= 0)
		{
			ScriptLine = RemainingScript.Left(NLPos);
			pPS = ParseParamLine(ScriptLine);
			if (pPS)
				pCSP->m_ParamSetList.AddTail(pPS);
			RemainingScript = RemainingScript.Right(RemainingScript.GetLength() - NLPos - 1);
		}
	}
	while (NLPos != -1);

	return pCSP;
}


////////////////////////////////////////////////////////////////////////////
// Get the memrect object
////////////////////////////////////////////////////////////////////////////

CUTSMemRect	*CScriptProcessor::UTSMemRect(void)
{
	return m_pIPObjects->UTSMemRect();
}

