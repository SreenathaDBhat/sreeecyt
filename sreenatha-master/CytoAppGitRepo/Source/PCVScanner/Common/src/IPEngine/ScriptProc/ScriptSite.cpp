/////////////////////////////////////////////////////////////////////////////
// Script engine overridables
//
// Written By K Ratcliff 22032001
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include <activscp.h>

#include "..\..\NLog\src\NLogC\NLogC.h"
#include "scriptsite.h"

// Constructor/Destructor
	ActiveScriptSite::ActiveScriptSite() 
	{
		m_dwRef = 1; 
		m_nMacroLines = 0; 
		m_bErrorOccurred = FALSE;
		NLog_Init(_T("SCRIPT"));
	}
	
	ActiveScriptSite::~ActiveScriptSite() {}
	

/////////////////////////////////////////////////////////////////////////////
// Useful error functions
// Reset the last script error
/////////////////////////////////////////////////////////////////////////////

void ActiveScriptSite::ResetScriptErrors(void)
{
    m_LastScriptError.ErrorCode   = 0;            // Last error code
    m_LastScriptError.LineNumber  = 0;            // Line number where the error occurred
    m_LastScriptError.Source      = "";           // Source that cuased the error
    m_LastScriptError.Description = "";           // Textual description of the error code
	m_bErrorOccurred			  = FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Get the last script error
// Returns TRUE if an error occurred
/////////////////////////////////////////////////////////////////////////////

BOOL ActiveScriptSite::GetLastScriptError(ScriptError *pErrInfo)
{
    pErrInfo->ErrorCode    = m_LastScriptError.ErrorCode;
    pErrInfo->LineNumber   = m_LastScriptError.LineNumber;
    pErrInfo->Source       = m_LastScriptError.Source;
    pErrInfo->Description  = m_LastScriptError.Description;
        
    return (m_LastScriptError.ErrorCode != 0 && !m_bErrorOccurred);
}

/////////////////////////////////////////////////////////////////////////////
// Process Script Errors - This is called whenever an error occurs either
// at the script parsing phase or during run-time of the script
/////////////////////////////////////////////////////////////////////////////


HRESULT __stdcall ActiveScriptSite::OnScriptError(IActiveScriptError *pscriptError) 
{

	EXCEPINFO ei;
	DWORD     dwSrcContext;
	ULONG     ulLine;
	LONG      ichError;
	BSTR      bstrLine = NULL;
	CString   strError;


    // Output a suitable error message and line number should 
    // there be an error.
	pscriptError->GetExceptionInfo(&ei);
	pscriptError->GetSourcePosition(&dwSrcContext, &ulLine, &ichError);
	pscriptError->GetSourceLineText(&bstrLine);
	
    // Keep hold of the last error code 
	m_LastScriptError.Description = (LPCWSTR)ei.bstrDescription;
	m_LastScriptError.Source      = (LPCWSTR)ei.bstrSource;
    m_LastScriptError.ErrorCode   = ei.wCode;
    m_LastScriptError.LineNumber  = ulLine - m_nMacroLines;

	strError.Format(_T("Error:%s\nSrc: %s\nLine:%d\n"), m_LastScriptError.Description, 
                                                        m_LastScriptError.Source, 
                                                        m_LastScriptError.LineNumber);
	AfxMessageBox(strError);

	NLog_Error(_T("SCRIPT"), strError);

	m_bErrorOccurred = TRUE;

    return S_OK;
}

/////////////////////////////////////////////////////////////////////////////
// Pre-processing Before The Script Begins
/////////////////////////////////////////////////////////////////////////////

HRESULT __stdcall ActiveScriptSite::OnEnterScript(void) 
{
	m_bErrorOccurred = FALSE;
    return S_OK;
}

/////////////////////////////////////////////////////////////////////////////
// Post-processing After The Script Has Ended
/////////////////////////////////////////////////////////////////////////////

HRESULT __stdcall ActiveScriptSite::OnLeaveScript(void) 
{
    return S_OK;
}

