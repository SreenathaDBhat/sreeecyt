
/////////////////////////////////////////////////////////////////////////////
// Launch the script processing in an independent thread of execution
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "assert.h"
#include "direct.h"
#include "Shlwapi.h"
#include "ScriptPersist.h"
#include "CMeasurement.h"
#include "CSyncObjects.h"
#include "ScriptProc.h"
#include "ScriptThread.h"

#include "..\..\NLog\src\NLogC\NLogC.h"


#define ACCESS_VIOLATION   { int *pxpx;	pxpx = 0; *pxpx = 0; }

DWORD WINAPI ProcessingThread(LPVOID lpParam);

void SCRIPTPROC_API ShowDebug(LPCTSTR Str, DWORD i)
{
	NLog_Debug(_T("SCRIPT"), _T("%s %d"), Str, i);
}

CScriptThread::CScriptThread(DWORD InstanceId, CString ScriptName, CString PersistsName, LPCRITICAL_SECTION pCS, CSyncObjectSemaphore *pSem, VarMapBank *pPersist, LPCTSTR Classifier)
{
	Initialise(InstanceId, ScriptName, PersistsName, pCS, pSem, pPersist, Classifier);
}


CScriptThread::CScriptThread(DWORD InstanceId, CString ScriptName, LPCRITICAL_SECTION pCS, CSyncObjectSemaphore *pSem, VarMapBank *pPersist, LPCTSTR Classifier)
{
	CString P;

	P.Format(_T("IPE%d"), InstanceId);

	Initialise(InstanceId, ScriptName, P, pCS, pSem, pPersist, Classifier);
}

CScriptThread::~CScriptThread(void)
{
	WaitForSingleObject(m_hThread, 20000);

	CloseHandle(m_hFragReadPipe);
	CloseHandle(m_hFragWritePipe);

	CloseHandle(m_hResultsReadPipe);
	CloseHandle(m_hResultsWritePipe);

	CloseHandle(m_hResultsCoordReadPipe);
	CloseHandle(m_hResultsCoordWritePipe);

	CloseHandle(m_hImageReadPipe);
	CloseHandle(m_hImageWritePipe);

	CloseHandle(m_hCoordReadPipe);
	CloseHandle(m_hCoordWritePipe);

	delete m_pEndEvent;
	delete m_pImageReady;
	delete m_pIdleEvent;
	delete m_pScriptBusyEvent;
	delete m_pErrorEvent;
	delete m_pInitEvent;

	ShowDebug(_T("SCRIPT THREAD DELETED"), m_InstanceId);

}

BOOL CScriptThread::Initialise(DWORD InstanceId, CString ScriptName, CString Persistents, LPCRITICAL_SECTION pCS, CSyncObjectSemaphore *pSem, VarMapBank *pPersist, LPCTSTR Classifier)
{
	NLog_Init(_T("SCRIPT"));

	m_pcsCountLock = pCS;
	m_bInitOK = FALSE;
	m_InstanceId = InstanceId;

	m_ImagesProcessed = 0;

	m_pResultsSemaphore = pSem;

	if(FAILED(SHGetFolderPath(NULL, CSIDL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, m_szPath))) 
	{
		NLog_Error(_T("SCRIPT"), _T("SCRIPT THREADING COULD NOT OPEN TEMP FOLDER %s"), m_szPath);
		return FALSE;
	}

	PathAppend(m_szPath, _T("IPETemp"));

	int R = _tmkdir(m_szPath);
	if (R != 0)
	{
		// Path does not exist - anything else path is o.k.
		if (errno == ENOENT)
		{
			NLog_Error(_T("SCRIPT"), _T("SCRIPT THREADING COULD NOT CREATE TEMP FOLDER %s"), m_szPath);
			return FALSE;
		}
	}

	_stprintf(m_Qin,  _T("IPEIN%d"),  InstanceId);
	_stprintf(m_Qout, _T("IPEOUT%d"), InstanceId);
	_stprintf(m_PersistentsName, _T("%s%d"), Persistents, InstanceId);

	_tcscpy(m_ScriptName, ScriptName);
	_tcscpy(m_ClassifierName, Classifier);

	if (!PathFileExists(ScriptName))
	{
		NLog_Error(_T("SCRIPT"), _T("File does not exist %s"), ScriptName);
		return FALSE;
	}
	
	// Image to process
	m_pImageReady = new CSyncObjectSemaphore(0, INT_MAX);
	// Tell the thread to end
	m_pEndEvent = new CSyncObjectEvent(SyncObjectCreate, NULL, TRUE);				// Manual reset
	// Not processing an image - ready for a new one
	m_pIdleEvent = new CSyncObjectEvent(SyncObjectCreate, NULL, FALSE);				// Autoreset
	// Script is running
	m_pScriptBusyEvent = new CSyncObjectEvent(SyncObjectCreate, NULL, TRUE);		// Manual reset
	// Error occurred
	m_pErrorEvent = new CSyncObjectEvent(SyncObjectCreate, NULL, TRUE);				// Manual reset
	// Thread fully initialised
	m_pInitEvent = new CSyncObjectEvent(SyncObjectCreate, NULL, TRUE);				// Manual reset

	SECURITY_ATTRIBUTES sa;

	// Set this up as the read and write handles are inheritable
    sa.nLength = sizeof(SECURITY_ATTRIBUTES);
    sa.bInheritHandle = TRUE;
    sa.lpSecurityDescriptor = NULL;

	// Create a queue for output measurements 
	CreatePipe(&m_hResultsReadPipe, &m_hResultsWritePipe, &sa, 1024 * 100);

	// Create a queue for output coordinates 
	CreatePipe(&m_hResultsCoordReadPipe, &m_hResultsCoordWritePipe, &sa, 1024 * 100);

	// Create a queue for the output fragments
	CreatePipe(&m_hFragReadPipe, &m_hFragWritePipe, &sa, 1024 * 100);

	// Create a queue for the INPUT images
	CreatePipe(&m_hImageReadPipe, &m_hImageWritePipe, &sa, 1024 * 100);

	// Create a queue for the INPUT coordinates
	CreatePipe(&m_hCoordReadPipe, &m_hCoordWritePipe, &sa, 1024 * 100);

	m_bEnded = FALSE;

	m_pPersist = pPersist;
	m_hThread = CreateThread(NULL, 0, ProcessingThread, this, 0, &m_ThreadId);

	DWORD dwRes = WaitForSingleObject(m_pInitEvent->Handle(), 60000);
	if (dwRes == WAIT_TIMEOUT)
	{
		NLog_Error(_T("SCRIPT"), _T("ERROR COULD NOT INITIALISE"));
		return FALSE;
	}

	NLog_Trace(_T("SCRIPT"), _T("SCRIPT THREAD CREATED %d"), m_InstanceId);
	m_bInitOK = TRUE;
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////

VarMapBank *CScriptThread::GetPersistents(void)
{
	return m_pPersist;
}

/////////////////////////////////////////////////////////////////////////////
// Is the thread busy
/////////////////////////////////////////////////////////////////////////////

BOOL CScriptThread::ThreadBusy(void)
{
	return m_pScriptBusyEvent->Signalled(0);
}

/////////////////////////////////////////////////////////////////////////////
// Add an image for processing
/////////////////////////////////////////////////////////////////////////////

void CScriptThread::AddImage(BYTE *ImageData, int BitsPerPixel, int Width, int Height, double X, double Y, double Z)
{
	VBS_IMAGE_REC PutImageInfo;
	DWORD BytesWritten;
	Coordinate XYZ;

	XYZ.X = X;
	XYZ.Y = Y;
	XYZ.Z = Z;

	WriteFile(m_hCoordWritePipe, &XYZ, sizeof(Coordinate), &BytesWritten, NULL);

	PutImageInfo.Width  = Width;			// put an image of W by H by 256 greys
	PutImageInfo.Height = Height;
	PutImageInfo.BitsPerPixel = BitsPerPixel;
	PutImageInfo.Address = ImageData;		// array of bytes containing image data

	WriteFile(m_hImageWritePipe, &PutImageInfo, sizeof(VBS_IMAGE_REC), &BytesWritten, NULL);
	m_pImageReady->Release();
}

/////////////////////////////////////////////////////////////////////////////
// Get an image back
/////////////////////////////////////////////////////////////////////////////

BOOL CScriptThread::GetImage(VBS_IMAGE_REC &GetImageInfo)
{
	DWORD BytesRead;

	return (ReadFile(m_hImageReadPipe, &GetImageInfo, sizeof(VBS_IMAGE_REC), &BytesRead, NULL) && BytesRead == sizeof(VBS_IMAGE_REC));
}

/////////////////////////////////////////////////////////////////////////////
// Get coordinates back
/////////////////////////////////////////////////////////////////////////////

BOOL CScriptThread::GetCoordinates(double &X, double &Y, double &Z)
{
	DWORD BytesRead;
	Coordinate XYZ;

	BOOL Result = (ReadFile(m_hCoordReadPipe, &XYZ, sizeof(Coordinate), &BytesRead, NULL) && BytesRead == sizeof(Coordinate));

	X = XYZ.X;
	Y = XYZ.Y;
	Z = XYZ.Z;

	return Result;
}

/////////////////////////////////////////////////////////////////////////////
// Increment the processed count
/////////////////////////////////////////////////////////////////////////////

void CScriptThread::IncProcessedCount(void)
{
	EnterCriticalSection(m_pcsCountLock);
	m_ImagesProcessed++;
	LeaveCriticalSection(m_pcsCountLock);
}

/////////////////////////////////////////////////////////////////////////////
// Get the idle event
/////////////////////////////////////////////////////////////////////////////

HANDLE CScriptThread::Idle(void)
{
	return m_pIdleEvent->Handle(); 
}

/////////////////////////////////////////////////////////////////////////////
// Get the error event
/////////////////////////////////////////////////////////////////////////////

HANDLE CScriptThread::Error(void)
{
	return m_pErrorEvent->Handle(); 
}


/////////////////////////////////////////////////////////////////////////////
// An error occurred
/////////////////////////////////////////////////////////////////////////////

void CScriptThread::SignalError(void)
{
	m_pErrorEvent->Signal(TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// End the thread
/////////////////////////////////////////////////////////////////////////////

void CScriptThread::RequestToEnd(void)
{
	m_pEndEvent->Signal(TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// Processing is idle
/////////////////////////////////////////////////////////////////////////////

void CScriptThread::SignalIdle(void)
{
	m_pIdleEvent->Signal(TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// Processing thread initialised
/////////////////////////////////////////////////////////////////////////////

void CScriptThread::SignalInitialised(void)
{
	m_pInitEvent->Signal(TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// Flag Results for processing
/////////////////////////////////////////////////////////////////////////////

void CScriptThread::ResultsReady(void)
{
	m_pResultsSemaphore->Release();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Do the actual processing
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define THREAD_ERROR 2
#define END_EVENT 1
#define IMAGEREADY_SEMAPHORE 0




DWORD WINAPI ProcessingThread(LPVOID lpParam) 
{
	CScriptThread	 *pThread		= (CScriptThread *)lpParam;
	
	CScriptProcessor *pVB;

	ShowDebug(_T("SCRIPT THREAD STARTED "), pThread->Id());

	BOOL Error = FALSE;
	DWORD Result;
	HANDLE ImgReady[3];
	DWORD dwBytesWritten;
	Measurement *pMeas;
	BOOL Quit;
	int NI = 0, NF = 0;
	double X, Y, Z;
	CScriptThread::Coordinate Coord;

	VBS_IMAGE_REC GetImageInfo, PutImageInfo;

	ImgReady[END_EVENT] = pThread->EndEvent()->Handle();
	ImgReady[IMAGEREADY_SEMAPHORE] = pThread->ImageReady()->Handle();
	ImgReady[THREAD_ERROR] = pThread->Error();

	pVB = new CScriptProcessor(pThread->Id());
	pVB->InitialiseScriptProcessor(pThread->PersistentsName(), pThread->Path(), pThread->QinPath(), pThread->QoutPath());
	pVB->LoadScript(pThread->ScriptName(), TRUE);
	
	pVB->ResetPersistentVariables();

	VARIANT V;
	CString Name;

	VarMapBank *pPersist = pThread->GetPersistents();
	POSITION P = pPersist->GetFirst();
	while (P)
	{
		pPersist->GetNext(P, Name, &V);
		pVB->SetPersistent(Name, V);
	}


	if (_tcslen(pThread->Classifier()) != 0)
		pVB->SVMModel(pThread->Classifier());

	pVB->SetExecutionMode(modePlay);
	
	// Initialisation done
	pThread->SignalInitialised();

	// Idle and ready to process
	pThread->SignalIdle();

	try
	{
		do
		{
			// Now check for an image to process OR the end
			Result = WaitForMultipleObjects(2, ImgReady, FALSE, INFINITE);
			Result -= WAIT_OBJECT_0;

			if (Result == IMAGEREADY_SEMAPHORE)
			{
				//'ShowDebug(_T("SCRIPT THREAD  PROCESSING"), pThread->Id());

				// Get the image and associated information and process it
				pThread->GetCoordinates(X, Y, Z);
				pVB->SetDoublePersistent(_T("X"), X);
				pVB->SetDoublePersistent(_T("Y"), Y);
				pVB->SetDoublePersistent(_T("Z"), Z);
				pThread->GetImage(PutImageInfo);
				pVB->PutImage(&PutImageInfo);

				Error = !pVB->ExecuteScript();

				delete PutImageInfo.Address;

				if (Error)
				{
					ShowDebug(_T("SCRIPT THREAD ERROR"), pThread->Id());
					pThread->SignalError();
					break;
				}
				NI++;

				/*if (NI > 50)
				{
					NLog_Error(_T("FLFSCANPC"), _T("OH NO, ERROR PROCESSING FRAMES!"));
					Sleep(5000);
					ACCESS_VIOLATION;
				}*/

				// Add fragments and results to the output Q
				while (pVB->GetImage(&GetImageInfo))
				{
					// Repeat until no more measurements from analysis
					Quit = FALSE;
					do
					{
						if (pVB->GetMeasurement(&pMeas))
						{
							WriteFile(pThread->WriteResultsPipe(), &pMeas, sizeof(Measurement *), &dwBytesWritten, NULL);
							Quit = (pMeas->m_Name == _T("Quit"));
						}
						else
							Quit = TRUE;
					}
					while (!Quit);

					Coord.X = X;
					Coord.Y = Y;
					Coord.Z = Z;
					WriteFile(pThread->WriteResultsCoordPipe(), &Coord, sizeof(CScriptThread::Coordinate), &dwBytesWritten, NULL);
					WriteFile(pThread->WriteFragPipe(), &GetImageInfo, sizeof(VBS_IMAGE_REC), &dwBytesWritten, NULL);

					// Results ready for collecting
					pThread->ResultsReady();

					NF++;
				}
				
				pThread->IncProcessedCount();
				pVB->Reset();
				pThread->SignalIdle();
				
				//ShowDebug(_T("SCRIPT THREAD PROCESSING ENDED"), pThread->Id());
			}
		}
		while (Result != END_EVENT && !Error && Result != THREAD_ERROR);
	
	}

	catch(...) 
    {
        ShowDebug(_T("SCRIPT THREAD EXCEPTION ERROR PROCESSING IMAGE"), pThread->Id());
		pThread->SignalError();
    }

	pVB->ReleaseScriptProcessor();

	delete pVB;
	NLog_Info(_T("SCRIPT"), _T("Script %d processed %d of %d images, created %d fragments"), pThread->Id(), pThread->GetProcessedCount(), NI, NF);

	return 0;
}