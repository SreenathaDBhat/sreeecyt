/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Script thread pool
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ProcessorCount.h"

#include "CMeasurement.h"
#include "CSyncObjects.h"

#include "ScriptPersist.h"
#include "ScriptProc.h"
#include "ScriptThread.h"
#include "ScriptThreadPool.h"

#include "..\..\NLog\src\NLogC\NLogC.h"

CScriptThreadPool::CScriptThreadPool(DWORD N)
{
	CCPUCount CPU;

	m_bInitOK = FALSE;

	for (int i = 0; i < MAX_SCRIPT_THREADS; i++)
		m_ThreadPool[i] = NULL;

	if (CPU.Count())
		m_dwCPUs = CPU.LogicalProcessors();
	else
		m_dwCPUs = 2;

	if (N < m_dwCPUs)
		m_NumThreads = N;
	else
		m_NumThreads = m_dwCPUs;

	m_ImagesAdded = 0;
	InitializeCriticalSection(&m_csImageCountLock);

	m_pEndEvent = new CSyncObjectEvent(SyncObjectCreate, NULL, TRUE);				// Manual reset

	m_pResultsSemaphore = new CSyncObjectSemaphore(0, INT_MAX);

	NLog_Init(_T("SCRIPTPOOL"));
}

CScriptThreadPool::~CScriptThreadPool(void)
{
	for (DWORD i = 0; i < MAX_SCRIPT_THREADS; i++)
	{
		if (m_ThreadPool[i])
			delete m_ThreadPool[i];
	}

	DeleteCriticalSection(&m_csImageCountLock);

	delete m_pEndEvent;
	delete m_pResultsSemaphore;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Start
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
BOOL CScriptThreadPool::Start(CString ScriptName, VarMapBank *pPersistents, LPCTSTR Classifier)				// Start them all
{
	m_bInitOK = FALSE;

	for (DWORD i = 0; i < m_NumThreads; i++)
	{
		m_ThreadPool[i] = new CScriptThread(i, ScriptName, &m_csImageCountLock, m_pResultsSemaphore, pPersistents, Classifier);

		if (!m_ThreadPool[i]->HasInitialised())
		{
			NLog_Error(_T("SCRIPTPOOL"), _T("Exception launching script thread"));
			delete m_ThreadPool[i];
			m_ThreadPool[i] = NULL;
			return FALSE;
		}
	}
	m_bInitOK = TRUE;

	return TRUE;
}

BOOL CScriptThreadPool::Start(CString ScriptName, CString PersistentsName, VarMapBank *pPersistents, LPCTSTR Classifier)				// Start them all
{
	for (DWORD i = 0; i < m_NumThreads; i++)
	{
		
		m_ThreadPool[i] = new CScriptThread(i, ScriptName, PersistentsName, &m_csImageCountLock, m_pResultsSemaphore, pPersistents, Classifier);

		if (!m_ThreadPool[i]->HasInitialised())
		{
			NLog_Error(_T("SCRIPTPOOL"), _T("Exception launching script thread"));
			delete m_ThreadPool[i];
			m_ThreadPool[i] = NULL;
			return FALSE;
		}
	}
	m_bInitOK = TRUE;
	
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Shut it all down again
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CScriptThreadPool::ShutDown(DWORD Millisecs)
{
	HANDLE	handles[MAX_SCRIPT_THREADS];
	
	DWORD c = 0;
	for (DWORD i = 0; i < m_NumThreads; i++)
	{
		if (m_ThreadPool[i] != NULL)
		{
			handles[c++] = m_ThreadPool[i]->Handle();
			m_ThreadPool[i]->RequestToEnd();			// Request to end it
		}
	}

	DWORD dwRes = WAIT_TIMEOUT;

	if (c != 0)
		dwRes = WaitForMultipleObjects(c, handles, TRUE, Millisecs);
	
	for (DWORD i = 0; i < m_NumThreads; i++)
	{
		if (m_ThreadPool[i] != NULL)
			CloseHandle(handles[i]);
	}

	if (dwRes != WAIT_TIMEOUT)
	{
		m_pEndEvent->Signal(TRUE);
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Wait for the end - should only be called from a thread OTHER than the one doing the shutdown
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CScriptThreadPool::WaitForEnd(DWORD Millisecs)
{
	m_pEndEvent->WaitSignalled(Millisecs);
}

// Have we ended
BOOL CScriptThreadPool::HasEnded(void)
{
	return m_pEndEvent->Signalled(0);
}

void CScriptThreadPool::SignalEnd(void)
{
	m_pEndEvent->Signal(TRUE);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Find a free processor
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CScriptThreadPool::FindFreeProcessor(DWORD &FreeProc)
{
	for (DWORD i = 0; i < m_NumThreads; i++)
	{
		if (!m_ThreadPool[i]->ThreadBusy())
		{
			FreeProc = i;
			return TRUE;
		}
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Wait for a free processor
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
CScriptThreadPool::ThreadState CScriptThreadPool::WaitForFreeProcessor(DWORD Millisecs, DWORD &FreeProc)
{
	HANDLE	handles[MAX_SCRIPT_THREADS * 2];
	DWORD i, N = 0;

	for (i = 0; i < m_NumThreads; i++)
		handles[N++] = m_ThreadPool[i]->Idle();
	for (i = 0; i < m_NumThreads; i++)
		handles[N++] = m_ThreadPool[i]->Error();

	DWORD Result = WaitForMultipleObjects(N, handles, FALSE, Millisecs);

	if (Result != WAIT_TIMEOUT)
	{
		FreeProc = Result - WAIT_OBJECT_0;
		if (FreeProc < m_NumThreads)
			return ThreadFree;

		return ThreadError;
	}

	return ThreadTimeout;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Do we have an error ?
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

CScriptThreadPool::ThreadState CScriptThreadPool::Error(DWORD Millisecs)
{
	HANDLE	handles[MAX_SCRIPT_THREADS];
	DWORD i, N = 0;

	for (i = 0; i < m_NumThreads; i++)
		handles[N++] = m_ThreadPool[i]->Error();

	DWORD Result = WaitForMultipleObjects(N, handles, FALSE, Millisecs);

	if (Result != WAIT_TIMEOUT)
	{
		if (Result - WAIT_OBJECT_0 < m_NumThreads)
			return ThreadError;
	}

	return ThreadOk;
}

CScriptThreadPool::ThreadState CScriptThreadPool::Status(void)		// Has an error occurred
{
	return Error(0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Find a free processor and add an image to it
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CScriptThreadPool::AddImage(DWORD Proc, BYTE *ImageData, int BitsPerPixel, int Width, int Height, double X, double Y, double Z)	// Add an image for processing
{
	m_ThreadPool[Proc]->AddImage(ImageData, BitsPerPixel, Width, Height, X, Y, Z);

	EnterCriticalSection(&m_csImageCountLock);
	m_ImagesAdded++;
	LeaveCriticalSection(&m_csImageCountLock);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Get the total number of processed images for all threads in the pool
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

int CScriptThreadPool::GetProcessedCount(void)
{
	int Total = 0;

	EnterCriticalSection(&m_csImageCountLock);
	
	for (DWORD i = 0; i < m_NumThreads; i++)
		Total += m_ThreadPool[i]->GetProcessedCount();

	LeaveCriticalSection(&m_csImageCountLock);

	return Total;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// // Remaining images to process
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

int CScriptThreadPool::ImagesToProcess(void)					
{
	int Total = 0;

	EnterCriticalSection(&m_csImageCountLock);
	
	for (DWORD i = 0; i < m_NumThreads; i++)
		Total += m_ThreadPool[i]->GetProcessedCount();

	Total = m_ImagesAdded - Total;

	LeaveCriticalSection(&m_csImageCountLock);

	return Total;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// // Remaining images to process
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

int CScriptThreadPool::ImagesAdded(void)					
{
	int Total;

	EnterCriticalSection(&m_csImageCountLock);
	Total = m_ImagesAdded;
	LeaveCriticalSection(&m_csImageCountLock);

	return Total;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Get a frag image and associated measurements
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CScriptThreadPool::GetData(VBS_IMAGE_REC &ImageData, CMeasures &MeasurementData, CScriptThread::Coordinate &Coord)
{
	DWORD BytesRead, TotalBytes, BytesThisMessage;
	Measurement *pMeas;
	DWORD Proc;
	CScriptThread::Coordinate C;

	if (DataAvailableFromProcessor(Proc))
	{
		// Get the image
		ReadFile(m_ThreadPool[Proc]->ReadFragPipe(), &ImageData, sizeof(VBS_IMAGE_REC), &BytesRead, NULL);
		// Get the coordinates
		ReadFile(m_ThreadPool[Proc]->ReadResultsCoordPipe(), &C, sizeof(CScriptThread::Coordinate), &BytesRead, NULL);

		Coord.X = C.X;
		Coord.Y = C.Y;
		Coord.Z = C.Z;

		BOOL Quit = FALSE;
		// Get any associated results
		do
		{
			if (PeekNamedPipe(m_ThreadPool[Proc]->ReadResultsPipe(), NULL, 0, NULL, &TotalBytes, &BytesThisMessage) && (TotalBytes > 0))
			{
				ReadFile(m_ThreadPool[Proc]->ReadResultsPipe(), &pMeas, sizeof(pMeas), &BytesRead, NULL);

				if (pMeas->m_Name != _T("Quit"))
					MeasurementData.Add(pMeas->m_Name, pMeas->m_Value, pMeas->m_NormalisationFactor, pMeas->m_Flags.Flags.IncludeInClassifier, pMeas->m_Flags.Flags.IncludeInGrid, pMeas->m_Flags.Flags.IncludeInSort);
				else
					Quit = TRUE;

				delete pMeas;
			}
			else
				Quit = TRUE;
		}
		while (!Quit);

		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Is there data available ? If so which processing thread
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CScriptThreadPool::DataAvailableFromProcessor(DWORD &Proc)
{
	DWORD TotalBytes, BytesThisMessage;
	static DWORD P = 0;

	for (DWORD i = 0; i < m_NumThreads; i++)
	{
		if (PeekNamedPipe(m_ThreadPool[P]->ReadFragPipe(), NULL, 0, NULL, &TotalBytes, &BytesThisMessage))
		{
			if (TotalBytes > 0)
			{
				Proc = P;
				return TRUE;
			}
		}

		P++;
		if (P == m_NumThreads) 
			P = 0;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Is data available - the results semaphore increased by one everytime there is a new fragment
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CScriptThreadPool::IsDataAvailable(DWORD TimeOut)
{
	return m_pResultsSemaphore->Signalled(TimeOut);
}

