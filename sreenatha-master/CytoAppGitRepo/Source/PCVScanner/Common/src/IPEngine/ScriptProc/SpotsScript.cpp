/////////////////////////////////////////////////////////////////////////////
// SpotsScript.cpp : implementation file
//
// This OLE automation class is exposed to the script engine interface.
// Implementation of new spot finding specific keywords is done here. 
//
// Written By Karl Ratcliff 22072001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
// Implementation of new keywords is simply done using the Classwizard (to
// be recommended). This is done by selecting the Automation tab and then
// selecting the class name: SpotsScript. Add new 
//

#include "stdafx.h"
#include "vcmemrect.h"
#include "commonscript.h"
#include "scriptutils.h"
#include "vcblob.h"
#include "vcgeneric.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "woolzif.h"
#include "vcthresh.h"
#include "vcImProc.h"
#include "uts.h"
#include "safearr.h"
#include "SpotsScript.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SpotsScript
/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(SpotsScript, CCmdTarget)

SpotsScript::SpotsScript()
{
	EnableAutomation();
    // By default work in interactive mode
    m_ExecutionMode = modeDebug;
}

SpotsScript::~SpotsScript()
{
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// NOTE implement class members not exposed to the scripting engine here
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


void SpotsScript::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}


BEGIN_MESSAGE_MAP(SpotsScript, CCmdTarget)
	//{{AFX_MSG_MAP(SpotsScript)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(SpotsScript, CCmdTarget)
	//{{AFX_DISPATCH_MAP(SpotsScript)
	DISP_FUNCTION(SpotsScript, "FindSpots", FindSpots, VT_I4, VTS_I4 VTS_BSTR VTS_I4 VTS_I4)
	DISP_FUNCTION(SpotsScript, "Spots", Spots, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(SpotsScript, "NumberOfSpots", NumberOfSpots, VT_I4, VTS_NONE)
	DISP_FUNCTION(SpotsScript, "GetSpot", GetSpot, VT_BOOL, VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(SpotsScript, "SetSpotTag", SetSpotTag, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(SpotsScript, "DeleteTag", DeleteTag, VT_I4, VTS_I4)
	DISP_FUNCTION(SpotsScript, "SpotMask", SpotMask, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(SpotsScript, "QuantileSpots", QuantileSpots, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_I4 VTS_I4)
	DISP_FUNCTION(SpotsScript, "WaterShed", WaterShed, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_ISpotsScript to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {62C41D37-F04F-4484-8517-8A8846B8DA15}
static const IID IID_ISpotsScript =
{ 0x62c41d37, 0xf04f, 0x4484, { 0x85, 0x17, 0x8a, 0x88, 0x46, 0xb8, 0xda, 0x15 } };

BEGIN_INTERFACE_MAP(SpotsScript, CCmdTarget)
	INTERFACE_PART(SpotsScript, IID_ISpotsScript, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// SpotsScript message handlers
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
// Spot Finding
// 
// VB Syntax:
//      NumberSpots = FindSpots(SrcImage, Oper, Threshold, MinArea)
// OR
//      NumberSpots = Spots(SrcImage, Threshold, MinArea, MaxArea)
// Parameters:
//      SrcImage     - A valid UTS handle
//      Oper         - either "<=", "<", ">" or ">="
//      Threshold    - Thresholding Level
//      MinArea      - Minimum Area Of Spot To Find
//      MaxArea      - Maximum Area Of Spot To Find
// Returns:
//      Number of spots if OK, -1 otherwise
////////////////////////////////////////////////////////////////////////////////////////

long SpotsScript::FindSpots(long SrcImage, LPCTSTR Oper, long Threshold, long MinArea) 
{
    if (SrcImage > 0)
    {
        if (CHECK_PARAMETER(ThresholdOps, Oper))
            return IP().UTSBlob()->FindSpots(SrcImage, (char *) Oper, Threshold, MinArea);
        else
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);
    }

    AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
	    
    return UTS_ERROR;
}

long SpotsScript::Spots(long SrcImage, long Threshold, long MinArea, long MaxArea) 
{
	if (SrcImage > 0)
        return IP().UTSBlob()->FindMarkers(SrcImage, Threshold, MinArea, MaxArea);

    AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);               // A handle is invalid

	return UTS_ERROR;
}

////////////////////////////////////////////////////////////////////////////////////////
// Spot finding using quantiles
// VB script syntax:
//      QuantileSpots SrcImage, ResultMask, BegMask, MinArea, MaxArea, QuantileLo, 
//                    QuantileHi, MaxRep, NOpen
// Parameters:
//      SrcImage - is the source image to find spots in
//      ResultMask - is the result of the spot finding
//      BegMask - is the mask used to contain the area to find spots in
//      MinArea, MaxArea - are the area limits for the spot finding
//      QuantileLo, QuantileHi - are the quantile ranges to use for segmentation
//      MaxRep - ???
//      NOpen - number of openings to perform in order to find the spots
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

long SpotsScript::QuantileSpots(long SrcImage, long ResultMask, long BegMask, long MinArea, long MaxArea, double QuantileLo, double QuantileHi, long MaxRep, long NOpen) 
{
    long N = UTS_ERROR;

    // Check for valid handles
    if (UTSVALID_HANDLE(SrcImage) && UTSVALID_HANDLE(BegMask))
    {
        // Check range of parameters
        if (QuantileLo < 0.0 || QuantileHi < 0.0 || QuantileLo > 1.0 || QuantileHi > 1.0)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);
        else
        {
            TRACE0("Quantile Spots CALL Start\r\n");

			// Don't let min and max area be the same
			if (MinArea == MaxArea)
				MaxArea = MinArea + 1;

            // Do the command
            N = IP().UTSBlob()->CountRange(SrcImage, ResultMask, BegMask, MinArea, MaxArea, QuantileLo, QuantileHi, MaxRep, NOpen);
            if (N < 0)
                AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
            TRACE0("Quantile Spots CALL End\r\n");
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);          // A handle is invalid

    return N;
}

////////////////////////////////////////////////////////////////////////////////////////
// NumberOfSpots - returns the number of spots found by FindSpots
// VB syntax:
//      N = NumberOfSpots
// Parameters:
//      None
// Returns:
//      Number of spots found with the FindSpots function
////////////////////////////////////////////////////////////////////////////////////////

long SpotsScript::NumberOfSpots() 
{
	return IP().UTSBlob()->SpotsNumber();
}

////////////////////////////////////////////////////////////////////////////////////////
// GetSpot - get info about a particular spot
// VB syntax:
//      SpotOk = GetSpot(SpotNumber, SpotData)
// Parameters:
//      SpotNumber is the number of the spot we want to get data for (1-MAX_BLOBS)
//      SpotData is an array of 17 longs containing the spot data
// Returns:
//      TRUE if the spot number is valid, FALSE otherwise
////////////////////////////////////////////////////////////////////////////////////////

BOOL SpotsScript::GetSpot(long SpotNumber, VARIANT FAR* SpotData) 
{
    BOOL        Ok = FALSE;
    VCBLOB_SPOT SpotInfo;

    if (SpotNumber >= 0 && SpotNumber < MAX_BLOBS)
    {
        // Get the data about the spot
        if (IP().UTSBlob()->GetSpot(SpotNumber, &SpotInfo) >= 0)
        {
            SAFEARRAY *pSafe;           // Safe array to put data into
            VARIANT V;                  // Temp var for packaging data to put in array

            CHECK_SAFEARR_EXCEPTION(GetArrayInfo(SpotData, &pSafe, 1), return FALSE);
            
            // Check the dimension is >= NUMSPOT_LONGS longs 
            CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 1, NUMSPOT_LONGS), return FALSE);

            // Set the type for the returned values
            V_VT(&V) = VT_I4;

            // Set the spot data
            // NOT elegant
            V.lVal = SpotInfo.xMin;             // Bounding box 
            Set1DArrayIndex(pSafe, 0, &V);
            V.lVal = SpotInfo.yMin;             
            Set1DArrayIndex(pSafe, 1, &V);
            V.lVal = SpotInfo.xMax;             
            Set1DArrayIndex(pSafe, 2, &V);
            V.lVal = SpotInfo.yMax;             
            Set1DArrayIndex(pSafe, 3, &V);
            V.lVal = SpotInfo.Threshold;        
            Set1DArrayIndex(pSafe, 4, &V);
            V.lVal = SpotInfo.Area;             // Area  
            Set1DArrayIndex(pSafe, 5, &V);
            V.lVal = SpotInfo.Intensity;        // Intensity Levels    
            Set1DArrayIndex(pSafe, 6, &V);
            V.lVal = SpotInfo.MaxIntensity;             
            Set1DArrayIndex(pSafe, 7, &V);
            V.lVal = SpotInfo.xRecon;           // Seed For Reconstruction
            Set1DArrayIndex(pSafe, 8, &V);
            V.lVal = SpotInfo.yRecon;             
            Set1DArrayIndex(pSafe, 9, &V);      // Tags
            V.lVal = SpotInfo.tag;             
            Set1DArrayIndex(pSafe, 10, &V);
            V.lVal = SpotInfo.Tag1;             
            Set1DArrayIndex(pSafe, 11, &V);
            V.lVal = SpotInfo.Tag2;             
            Set1DArrayIndex(pSafe, 12, &V);
            V.lVal = SpotInfo.Tag3;             
            Set1DArrayIndex(pSafe, 13, &V);
            V.lVal = SpotInfo.Tag4;             
            Set1DArrayIndex(pSafe, 14, &V);
            V.lVal = SpotInfo.Tag5;             
            Set1DArrayIndex(pSafe, 15, &V);
            V.lVal = SpotInfo.Tag6;             
            Set1DArrayIndex(pSafe, 16, &V);

            // Spot OK
            Ok = TRUE;
        }
    }

	return Ok;
}

////////////////////////////////////////////////////////////////////////////////////////
// SetSpotTag marks a spot for deletion or for keeping
// 
// VB Syntax:
//      SetSpotTag SpotNum, TagValue
// Parameters:
//      SpotNum is the spot to tag
//      TagValue is the value to set
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void SpotsScript::SetSpotTag(long SpotNum, long TagValue) 
{
    if (SpotNum >= 0 && SpotNum < MAX_BLOBS)
        IP().UTSBlob()->SetTag(SpotNum, TagValue);
}

////////////////////////////////////////////////////////////////////////////////////////
// DeleteTag deletes all spots with a specified tag value
// 
// VB Syntax:
//      NumberSpotsLeft = DeleteTag(TagValue)
// Parameters:
//      TagValue is the value of the tag for the spots to be deleted
// Returns:
//      Number of remaining spots
////////////////////////////////////////////////////////////////////////////////////////

long SpotsScript::DeleteTag(long TagValue) 
{
    return IP().UTSBlob()->DeleteTag(TagValue);
}

////////////////////////////////////////////////////////////////////////////////////////
// Reconstructs a mask image from a list of spots found with findspots or findmarkers
//
// VB syntax:
//      SpotMask SrcImage, MaskImage
// Parameters:
//      SrcImage is an 8 bit image containg the original spots
//      MaskImage is a 1 bit mask image to put the results in
// Returns:
//      Number of spots in the mask image
////////////////////////////////////////////////////////////////////////////////////////

void SpotsScript::SpotMask(long SrcImage, long MaskImage) 
{
	if (UTSVALID_HANDLE(SrcImage) && UTSVALID_HANDLE(MaskImage))
        IP().UTSBlob()->SpotMask(SrcImage, MaskImage);
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);               // A handle is invalid
}


//////////////////////////////////////////////////////////////////////////////////////////
// Applies a watershed algortihm in order to expand spots created and currently
// stored in system memory. The algorithm ensures that spots are not expanded into
// one another. 
//
// VB syntax:
//      Watershed Image, Mask, BordersIm, AreaMin, AreaMax, FormFactor, FormThr, FormType 
// Parameters:
//      Image is an 8 bit grey image
//      MaskImage is a mask representing the area of interest
//      Borders is the resulting borders image 
//      AreaMin, AreaMax are the limits on area for each spot
//      FormFactor 0 - 100 is a shape contraint
//////////////////////////////////////////////////////////////////////////////////////////

long SpotsScript::WaterShed(long SrcImage, long MaskImage, long Borders, long AreaMin, long AreaMax, long FormFactor, long FormThresh, long FormType) 
{
	if (UTSVALID_HANDLE(SrcImage) && UTSVALID_HANDLE(MaskImage))
        return IP().UTSBlob()->WaterShed(SrcImage, MaskImage, Borders, AreaMin, AreaMax, FormFactor, FormThresh, FormType);
    
    AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);               // A handle is invalid

    return UTS_ERROR;
}
