/////////////////////////////////////////////////////////////////////////////
// CUDA Script.cpp : implementation file
//
// Graphics card hardware accelerated image processing
//
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
// 
//

#include "stdafx.h"

#ifdef _IPECUDA

#include "vcmemrect.h"
#include "vcblob.h"
#include "vcgeneric.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "woolzif.h"
#include "vcthresh.h"
#include "vcImProc.h"
#include "uts.h"
#include "safearr.h"
#include "scriptutils.h"
#include "resource.h"
#include "TIP.h"
#include "CudaTIP_Kernels.h"
#include "TIPScript.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#undef _IPECUDA

/////////////////////////////////////////////////////////////////////////////
// TIPScript
/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(TIPScript, CCmdTarget)

TIPScript::TIPScript()
{
	EnableAutomation();
    // By default work in interactive mode
    m_ExecutionMode = modeDebug;

    m_pTIP = NULL;

    m_bTIPCreated = FALSE;
    m_bUseTIP = TRUE;           // Assume we can use the GIP

}

TIPScript::~TIPScript()
{
#ifdef _IPECUDA
    if (m_pTIP)
        delete m_pTIP;
#endif
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// NOTE implement class members not exposed to the scripting engine here
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


void TIPScript::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}


BEGIN_MESSAGE_MAP(TIPScript, CCmdTarget)
	//{{AFX_MSG_MAP(GPUIPScript)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(TIPScript, CCmdTarget)
#ifdef _IPECUDA
	//{{AFX_DISPATCH_MAP(GPUIPScript)
	DISP_FUNCTION(TIPScript, "CreateTIP", Create, VT_I4, VTS_NONE)
	DISP_FUNCTION(TIPScript, "IsTIPInUse", IsTIPInUse, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(TIPScript, "DestroyTIP", DestroyTIP, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(TIPScript, "PutTIP", PutTIP, VT_BOOL, VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "GetTIP", GetTIP, VT_BOOL, VTS_I4 VTS_I4)
    DISP_FUNCTION(TIPScript, "CreateTIPImageAs",CreateTIPImageAs, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_BOOL)
    DISP_FUNCTION(TIPScript, "CreateTIPImage",CreateTIPImage, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_BOOL)
	DISP_FUNCTION(TIPScript, "FreeTIPImage",FreeTIPImage, VT_EMPTY, VTS_PVARIANT)
	DISP_FUNCTION(TIPScript, "ReleaseTIPImage", ReleaseTIPImage, VT_EMPTY, VTS_PVARIANT)
	DISP_FUNCTION(TIPScript, "GetTIPImageSpecs", GetTIPImageSpecs, VT_EMPTY, 
					VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(TIPScript, "GetDeviceCountTIP",GetDeviceCountTIP, VT_I4, VTS_NONE)
	DISP_FUNCTION(TIPScript, "OpenTIP", OpenTIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "CloseTIP", CloseTIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "DilateTIP", DilateTIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "ErodeTIP", ErodeTIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "AddTIP", AddTIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "SubtractTIP", SubtractTIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "MultiplyTIP", MultiplyTIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "DivideTIP", DivideTIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "DiffTIP", DiffTIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "ModTIP", ModTIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "AddConstTIP", AddConstTIP, VT_EMPTY, VTS_I4 VTS_R4 VTS_I4)
	DISP_FUNCTION(TIPScript, "SubtractConstTIP", SubtractConstTIP, VT_EMPTY, VTS_I4 VTS_R4 VTS_I4)
	DISP_FUNCTION(TIPScript, "MultiplyConstTIP", MultiplyConstTIP, VT_EMPTY, VTS_I4 VTS_R4 VTS_I4)
	DISP_FUNCTION(TIPScript, "DivideConstTIP", DivideConstTIP, VT_EMPTY, VTS_I4 VTS_R4 VTS_I4)
	DISP_FUNCTION(TIPScript, "ThreshTIP", ThreshTIP, VT_EMPTY, VTS_I4 VTS_R4 VTS_I4)
	DISP_FUNCTION(TIPScript, "CopyTIP", CopyTIP, VT_EMPTY, VTS_I4 VTS_I4)
    DISP_FUNCTION(TIPScript, "SobelTIP", SobelTIP, VT_EMPTY, VTS_I4 VTS_I4)
    DISP_FUNCTION(TIPScript, "GaussianTIP", GaussianTIP, VT_EMPTY, VTS_I4 VTS_R4 VTS_I4 VTS_I4)
    DISP_FUNCTION(TIPScript, "ComponentSplitTIP", SplitComponentsTIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
    DISP_FUNCTION(TIPScript, "ComponentJoinTIP", JoinComponentsTIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
    DISP_FUNCTION(TIPScript, "ComponentThresholdTIP", ComponentThresholdTIP, VT_EMPTY,
					VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_I4)
    DISP_FUNCTION(TIPScript, "ComponentBlendTIP", BlendComponentsTIP, VT_EMPTY,
					VTS_I4 VTS_R4 VTS_I4 VTS_R4 VTS_I4 VTS_R4 VTS_I4)
    DISP_FUNCTION(TIPScript, "ComposedBlendTIP", ComposedBlendTIP, VT_EMPTY, VTS_I4 VTS_R4 VTS_R4 VTS_R4 VTS_I4)
	DISP_FUNCTION(TIPScript, "RGBHSITIP", RGBHSITIP, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "HSIRGBTIP", HSIRGBTIP, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "ColourDeconvTIP", ColourDeconvTIP, 
					VT_EMPTY, VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(TIPScript, "MedianTIP", MedianTIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
    DISP_FUNCTION(TIPScript, "LaplaceTIP", LaplaceTIP, VT_EMPTY, VTS_I4 VTS_I4)
    DISP_FUNCTION(TIPScript, "MeanTIP", MeanTIP, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "SimpleBlendTIP", SimpleBlend, VT_EMPTY, VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(TIPScript, "HDRBlendTIP", HDRBlend, VT_EMPTY, VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(TIPScript, "RangeBlendTIP", RangeBlend, VT_EMPTY, VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(TIPScript, "CombineMultiExposuresTIP", CombineMultiExposures, VT_EMPTY, VTS_I4 VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(TIPScript, "HistogramTIP", Histogram, VT_I4, VTS_I4 VTS_I4)
    DISP_FUNCTION(TIPScript, "ConvolveSymTIP", ConvolveSymTIP, VT_EMPTY, VTS_I4 VTS_PVARIANT VTS_I4 VTS_I4)
    DISP_FUNCTION(TIPScript, "ConvolveTIP", ConvolveTIP, VT_EMPTY, VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "KuwaharaTIP", KuwaharaTIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "ConvertTIP", ConvertTIP, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "UnpackGreyTIP", UnpackGreyTIP, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "PackGreyTIP", PackGreyTIP, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "GetComponentTIP", GetComponentTIP, VT_BOOL, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "PutSubImageTIP", PutSubImageTIP, VT_BOOL, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "CropImageTIP", CropImageTIP, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "ResizeImageTIP", ResizeImageTIP, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "FFT2DPowerTIP", FFT2DPowerSpectrumTIP, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "FFT2DTIP", FFT2DTIP, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "GaussianFFT2DTIP", GaussianFFT2DTIP, VT_I4, VTS_I4 VTS_I4 VTS_R4 VTS_R4)
	DISP_FUNCTION(TIPScript, "ButterWorthFFT2DTIP", ButterWorthFFT2DTIP, VT_I4, VTS_I4 VTS_I4 VTS_R4 VTS_R4 VTS_R4)
	DISP_FUNCTION(TIPScript, "FFTImageRegistrationTIP", FFTImageRegistrationTIP, VT_EMPTY, 
								VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(TIPScript, "CoreFFTImageRegistrationTIP", CoreFFTImageRegistrationTIP, VT_EMPTY, 
								VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(TIPScript, "FFTImageStitchTIP",FFTImageStitchTIP, VT_EMPTY, 
								VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(TIPScript, "CoreFFTImageStitchTIP",CoreFFTImageStitchTIP, VT_EMPTY, 
								VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
    DISP_FUNCTION(TIPScript, "UnsharpMaskTIP", UnsharpMaskTIP, VT_EMPTY, VTS_I4 VTS_R4 VTS_R4 VTS_I4 VTS_I4)
	DISP_FUNCTION(TIPScript, "NearestNeighbDeconvTIP", NearestNeighbDeconvTIP, VT_EMPTY, 
								VTS_I4 VTS_R4 VTS_R4 VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(TIPScript, "DistanceMapTIP", DistanceMapTIP, VT_EMPTY, 
								VTS_I4 VTS_I4 VTS_BOOL VTS_BOOL VTS_R4)
	DISP_FUNCTION(TIPScript, "CannyEdgeTIP",CannyEdgeTIP, VT_EMPTY, VTS_I4 VTS_I4 VTS_R4 VTS_R4)
#endif
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IAiTiffScript to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.


// {B4C52967-1D19-4949-8508-E1964B5D7D2A}
static const IID IID_TIPScript = 
{ 0xb4c52967, 0x1d19, 0x4949, { 0x85, 0x8, 0xe1, 0x96, 0x4b, 0x5d, 0x7d, 0x2a } };

BEGIN_INTERFACE_MAP(TIPScript, CCmdTarget)
	INTERFACE_PART(TIPScript, IID_TIPScript, Dispatch)
END_INTERFACE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Reset member called from ScriptProc reset method
/////////////////////////////////////////////////////////////////////////////

void TIPScript::Reset()
{
#ifdef _IPECUDA
    if (m_pTIP)
        m_pTIP->TIPReleaseAll();
#endif
}

#ifdef _IPECUDA

/////////////////////////////////////////////////////////////////////////////
// Create the TIP
// Example:
//      If (CreateTIP) Then ....
// Parameters:
//      None 
//      TIP images are dynamically declared and destroyed
// Returns:
//      True if successful, False otherwise
// Notes:
//      An error will occur if image width are not divisible by 4
/////////////////////////////////////////////////////////////////////////////

afx_msg int TIPScript::Create(void)
{   

   // Tried initialising GIP before but failed so don't try init again
    if (!m_bUseTIP)
        return 0;


    if (!m_bTIPCreated)
    {
		if (!m_pTIP) m_pTIP = new TIP();
		m_bTIPDevices = m_pTIP->Create();
		if (m_bTIPDevices > 0) m_bTIPCreated=TRUE;
		else m_bTIPCreated = FALSE;
		if (!m_bTIPCreated)
		{
            m_bUseTIP = FALSE;      // Avoid creating GIP again
			if (m_pTIP) delete m_pTIP;
		}
    }
    return m_bTIPDevices;
}

/////////////////////////////////////////////////////////////////////////////
// IsTIPInUse
// - Test If the GIP is created
/////////////////////////////////////////////////////////////////////////////
afx_msg BOOL TIPScript::IsTIPInUse(void)
{
	return m_bTIPCreated;
}

/////////////////////////////////////////////////////////////////////////////
// Destroy TIP
// - Test If the TIP is created
/////////////////////////////////////////////////////////////////////////////
afx_msg void TIPScript::DestroyTIP(void)
{
	if (m_pTIP) delete m_pTIP;
	m_pTIP = NULL;
	m_bTIPCreated = FALSE;
    m_bUseTIP = TRUE;           // Assume we can use the TIP again
}


/////////////////////////////////////////////////////////////////////////////
//	CheckSpecsTipImage
//	This function checks for a created TIP and also checks
//	width, height, overlap, and bpp match the created TIP
//	If Device, Width, Height, Overlap or Bpp are < 0 they are don't cares
/////////////////////////////////////////////////////////////////////////////

void TIPScript::CheckSpecsTipImage(TIPImage **gpSource, long Device, long Width, 
								   long Height, long Overlap, long Bpp)		// Checks for device, width, height, bpp
{
	BOOL gpCudaArrayFlg;
	int gpImaWidth, gpImaHeight, gpDevice, gpImaBpp, gpOverlap;

	if (gpSource<=0)
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Invalid Image
    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no TIP so kick up an error
    if ((Width > 0) && (Width % 4))
        AfxThrowOleDispatchException(0xFF, IDS_IMAGESIZEDIV4, 0);           // Not divisable by 4
	m_pTIP->GetImageSpecs(gpSource, &gpDevice, &gpImaWidth, &gpImaHeight, 
		&gpOverlap, &gpImaBpp, &gpCudaArrayFlg);
/*	TRACE2("CheckSpecsTipImage Source dev:%d w:%d ",gpDevice, gpImaWidth);
	TRACE2("h:%d overl: %d ", gpImaHeight, gpOverlap);
	TRACE2("bpp:%d Cuda:%d\r\n", gpImaBpp, gpCudaArrayFlg);
	TRACE2("Compare  dev:%d w:%d ",Device, Width);
	TRACE2("h:%d overl: %d ", Height, Overlap);
	TRACE1("bpp:%d \r\n", Bpp);*/
	if (((Width >= 0)  && (Width != gpImaWidth)) || 
		((Height >= 0) && (Height != gpImaHeight)) ||
		((Overlap >= 0) && (Overlap != gpOverlap)) ||
		((Bpp >= 0) && (Bpp != gpImaBpp)))
	{
        AfxThrowOleDispatchException(0xFF, IDS_GIPSIZEMISMATCH, 0);         // Width and height mismatch
	}
	if ((Device >= 0) && (Device != gpDevice))
        AfxThrowOleDispatchException(0xFF, IDS_TIPDEVICEMISMATCH, 0);       // mismatch of TIP devices
}

/////////////////////////////////////////////////////////////////////////////
//	CreateTIPImage
//	  if BPP = 8 and overlap != 0 then a quad packed colour image is created
//				 if (overlap < 0) overlap will be zero
/////////////////////////////////////////////////////////////////////////////

long TIPScript::CreateTIPImage(long Device, long Width, long Height, long Overlap, 
							   long BPP, BOOL CudaArrayFlg)
{
    long HandleTipImage;

	if (!m_bTIPCreated)
		AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);			// We have no GIP so kick up an error
	if (Width % 4)
		AfxThrowOleDispatchException(0xFF, IDS_IMAGESIZEDIV4, 0);			// Not divisable by 4    
    
	HandleTipImage=(long)m_pTIP->TIPCreateImage(Device, Width, Height, 
											Overlap, BPP, CudaArrayFlg);
    if (!HandleTipImage)
		AfxThrowOleDispatchException(0xFF, IDS_NOGIPIMAGECREATED, 0);       // No GIPImage so kick up an error
	return HandleTipImage;
}

/////////////////////////////////////////////////////////////////////////////
//	CreateTIPImageAs
//  - create a TIP image as an existing UTS image
//	  if BPP = 8 and overlap != 0 then a quad packed colour image is created
//				 if (overlap < 0) overlap will be zero
/////////////////////////////////////////////////////////////////////////////
long TIPScript::CreateTIPImageAs(long Source, long Device, long Overlap, BOOL CudaArrayFlg)
{

	long Width, Height, Pitch, BPP, Addr;
    long HandleTipImage;

	// Get info about this image
	if (Source !=NULL)
	{
		BPP = UTSMemRect.GetExtendedInfo(Source, &Width, &Height, &Pitch, &Addr);
		if (!m_bTIPCreated)
			AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
		if (Width % 4)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGESIZEDIV4, 0);           // Not divisable by 4    
		if ((BPP != 8) && (BPP !=24) && (BPP != 16))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Invalid Image (should be 8 bit or 24 bit)
		HandleTipImage=(long)m_pTIP->TIPCreateImage(Device, Width, Height, 
													Overlap, BPP, CudaArrayFlg);
        if (!HandleTipImage)
			AfxThrowOleDispatchException(0xFF, IDS_NOGIPIMAGECREATED, 0);            // We have no GIP so kick up an error
		return HandleTipImage;
	}
	return -1L;
}

/////////////////////////////////////////////////////////////////////////////
//	FreeTIPImage
/////////////////////////////////////////////////////////////////////////////
void TIPScript::FreeTIPImage(VARIANT FAR* HandleTipSource)
{
    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    if (HandleTipSource->lVal > 0)
    {
 		m_pTIP->TIPFreeImage((TIPImage **)HandleTipSource->lVal);
		HandleTipSource->lVal = NULL;              // Mark the handle as invalid
    }
}

/////////////////////////////////////////////////////////////////////////////
//	ReleaseTIPImage
/////////////////////////////////////////////////////////////////////////////
void TIPScript::ReleaseTIPImage(VARIANT FAR* HandleTipSource)
{
    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    if (HandleTipSource->lVal > 0)
 		m_pTIP->TIPReleaseImage((TIPImage **)HandleTipSource->lVal);
}

/////////////////////////////////////////////////////////////////////////////
//	GetTIPImageSpecs
/////////////////////////////////////////////////////////////////////////////
void TIPScript::GetTIPImageSpecs(TIPImage **TipSource, VARIANT FAR* Device, VARIANT FAR* Width, 
					  VARIANT FAR* Height, VARIANT FAR* Overlap, VARIANT FAR* BPP,
					  VARIANT FAR* CudaArrayFlg)
{
	BOOL	isCuda;
	int	dev, w, h, overlp, bpp;

	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no TIP so kick up an error

	if (TipSource)
	{
		m_pTIP->GetImageSpecs(TipSource, &dev, &w, &h, &overlp, &bpp, &isCuda);
		V_VT(Device) = VT_I4;
		V_VT(Width)  = VT_I4;
		V_VT(Height) = VT_I4;
		V_VT(Overlap) = VT_I4;
		V_VT(BPP) = VT_I4;
		V_VT(CudaArrayFlg) = VT_BOOL;
		Device->lVal = dev;
		Width->lVal  = w;
		Height->lVal = h;
		Overlap->lVal = overlp;
		BPP->lVal= bpp;
		CudaArrayFlg->bVal=isCuda;
	}
}

/////////////////////////////////////////////////////////////////////////////
//	GetTIPImageSpecs
/////////////////////////////////////////////////////////////////////////////
long TIPScript::GetDeviceCountTIP( void)
{
	long	nrDev;

	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no TIP so kick up an error
	nrDev=m_pTIP->GetDeviceCount();
	return nrDev;
}

/////////////////////////////////////////////////////////////////////////////
// Transfer an image to the TIP
// Example:
//      If (PutTIP(ImageHandle, Buffer)) Then ....
// Parameters:
//      ImageHandle is a valid UTS image handle
//      Buffer is the handle of the buffer to put the image in
// Returns:
//      True if successful, False otherwise
// Notes:
//      An error will occur if images are not divisible by 4
/////////////////////////////////////////////////////////////////////////////

afx_msg BOOL TIPScript::PutTIP(long Image, TIPImage **gpImage)
{  
    long Width, Height, Pitch, Bpp;
    void *Addr;

    // Get info about this image
    Bpp=UTSMemRect.GetExtendedInfo(Image, &Width, &Height, &Pitch, &Addr);

    // Error checks
    CheckSpecsTipImage(gpImage, -1, Width, Height, -1, Bpp);		// ignore device and overlap

    if (Bpp == 8)
		return m_pTIP->PutImage(gpImage, (BYTE *) Addr);
	else if (Bpp == 16)
		return m_pTIP->PutImage(gpImage, (unsigned short *) Addr);
	else if (Bpp == 24)
		return m_pTIP->PutImage(gpImage, (RGBTRIPLE *) Addr);
	else
        AfxThrowOleDispatchException(0xFF, IDS_GIPSIZEMISMATCH, 0);         // Bpp mismatch
	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Transfer an image from the TIP
// Example:
//      If (GetTIP(ImageHandle, TIPImageHandle)) Then ....
// Parameters:
//      ImageHandle is a valid UTS image handle
//      the handle of the TIPImage to put the image in
// Returns:
//      True if successful, False otherwise
// Notes:
//      An error will occur if images are not divisible by 4
//      or no TIP or width and height do no match those for the created 
//      TIPImage
/////////////////////////////////////////////////////////////////////////////

BOOL TIPScript::GetTIP(long Image, TIPImage **gpImage)
{  
    long Width, Height, Pitch, Bpp;
    void *Addr;

    // Get info about this image
    Bpp=UTSMemRect.GetExtendedInfo(Image, &Width, &Height, &Pitch, &Addr);
    // Error checks
 	CheckSpecsTipImage(gpImage, -1, Width, Height, -1, Bpp);		// ignore device and overlap
	if (Bpp == 8)
		return m_pTIP->GetImage(gpImage, (BYTE *) Addr);
	else if (Bpp==16)
		return m_pTIP->GetImage(gpImage, (unsigned short *)Addr);
	else if (Bpp==24)
		return m_pTIP->GetImage(gpImage, (RGBTRIPLE *)Addr);
	else
        AfxThrowOleDispatchException(0xFF, IDS_GIPSIZEMISMATCH, 0);    // Bpp mismatch
	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Perform an opening
// Example:
//      OpenTIP Passes, Src, Dest
// Parameters:
//      Passes is the number of passes to do opening on
//      Src is the source handle for the image we wish to open
//      Dest is the source handle for the image we wish to open
/////////////////////////////////////////////////////////////////////////////

void TIPScript::OpenTIP(long Passes, TIPImage **Src, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
#ifdef MINMAX_SHARED
		if (isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);	// We should not have a cuda array
#else
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
#endif
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
	    m_pTIP->Open((int) Passes, Src, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

/////////////////////////////////////////////////////////////////////////////
// Perform an closing
// Example:
//      CloseTIP Passes, Src, Dest
// Parameters:
//      Passes is the number of passes to do opening on
//      Src is the source handle for the image we wish to open
//      Dest is the source handle for the image we wish to open
/////////////////////////////////////////////////////////////////////////////

void TIPScript::CloseTIP(long Passes, TIPImage **Src, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
#ifdef MINMAX_SHARED
		if (isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);	// We should not have a cuda array
#else
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
#endif
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
	    m_pTIP->Close((int) Passes, Src, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

/////////////////////////////////////////////////////////////////////////////
// Perform dilation
// Example:
//      DilateTIP Passes, Src, Dest
// Parameters:
//      Passes is the number of passes to do opening on
//      SrcBuf is the source handle for the image we wish to open
//      DestBuf is the source handle for the image we wish to open
/////////////////////////////////////////////////////////////////////////////

void TIPScript::DilateTIP(long Passes, TIPImage **Src, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
#ifdef MINMAX_SHARED
		if (isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);	// We should not have a cuda array
#else
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
#endif
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
	    m_pTIP->Dilate((int) Passes, Src, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

/////////////////////////////////////////////////////////////////////////////
// Perform an erosion
// Example:
//      ErodeTIP Passes, Src, Dest
// Parameters:
//      Passes is the number of passes to do opening on
//      Src is the source handle for the image we wish to open
//      Dest is the source handle for the image we wish to open
/////////////////////////////////////////////////////////////////////////////

void TIPScript::ErodeTIP(long Passes, TIPImage **Src, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
#ifdef MINMAX_SHARED
		if (isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);	// We should not have a cuda array
#else
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
#endif
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
	    m_pTIP->Erode((int) Passes, Src, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

/////////////////////////////////////////////////////////////////////////////
// Perform a Add, Subtract, Multiply, Divide, Diff or Mod
// between 2 images
// Example:
//      SubtractTIP Src1, Src2, DestBuf
// Parameters:
//      Src1 is the source handle for the image we wish to open (CudaArray)
//      Src2 is the source handle for the image we wish to open (CudaArray)
//      DestBuf is the source handle for the image we wish to open (No CudaArray)
/////////////////////////////////////////////////////////////////////////////

void TIPScript::AddTIP(TIPImage **Src1, TIPImage **Src2, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src1 > NULL) && (Src2 > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src1, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if ((!isCuda) || !m_pTIP->IsCudaArray(Src2))
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		CheckSpecsTipImage(Src2, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->Add(Src1, Src2, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

void TIPScript::SubtractTIP(TIPImage **Src1, TIPImage **Src2, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src1 > NULL) && (Src2 > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src1, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if ((!isCuda) || !m_pTIP->IsCudaArray(Src2))
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		CheckSpecsTipImage(Src2, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
	    m_pTIP->Subtract(Src1, Src2, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

void TIPScript::MultiplyTIP(TIPImage **Src1, TIPImage **Src2, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src1 > NULL) && (Src2 > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src1, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if ((!isCuda) || !m_pTIP->IsCudaArray(Src2))
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		CheckSpecsTipImage(Src2, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
	    m_pTIP->Multiply(Src1, Src2, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

void TIPScript::DivideTIP(TIPImage **Src1, TIPImage **Src2, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src1 > NULL) && (Src2 > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src1, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if ((!isCuda) || !m_pTIP->IsCudaArray(Src2))
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		CheckSpecsTipImage(Src2, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
	    m_pTIP->Divide(Src1, Src2, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

void TIPScript::DiffTIP(TIPImage **Src1, TIPImage **Src2, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src1 > NULL) && (Src2 > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src1, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if ((!isCuda) || !m_pTIP->IsCudaArray(Src2))
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		CheckSpecsTipImage(Src2, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
	    m_pTIP->Diff(Src1, Src2, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

void TIPScript::ModTIP(TIPImage **Src1, TIPImage **Src2, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src1 > NULL) && (Src2 > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src1, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if ((!isCuda) || !m_pTIP->IsCudaArray(Src2))
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		CheckSpecsTipImage(Src2, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
	    m_pTIP->Mod(Src1, Src2, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

/////////////////////////////////////////////////////////////////////////////
// Perform a Add, Subtract, Multiply, Divide, Diff or Mod
// between an image and a constant
// Example:
//      SubtractTIP Src1, val, DestBuf
// Parameters:
//      Src1 is the source handle for the image we wish to open (CudaArray)
//      val is a floating fraction (between 0.0 and 1.0) as the images are 
//			treated as normalized floats
//      DestBuf is the source handle for the image we wish to open (No CudaArray)
// Notes:
//      An error occurs if no TIP
/////////////////////////////////////////////////////////////////////////////

void TIPScript::AddConstTIP(TIPImage **Src, float val, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
	    m_pTIP->AddConst(Src, val, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

void TIPScript::SubtractConstTIP(TIPImage **Src, float val, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->SubtractConst(Src, val, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

void TIPScript::MultiplyConstTIP(TIPImage **Src, float val, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
	   m_pTIP->MultiplyConst(Src, val, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

void TIPScript::DivideConstTIP(TIPImage **Src, float val, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->DivideConst(Src, val, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

void TIPScript::ThreshTIP(TIPImage **Src, float Thresh, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->Thresh(Src, Thresh, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

///////////////////////////////////////////////////////////////////////////////
//	CopyTIP
//	- copy TIPimage to another TIPImage on same device 
//	  specs should be the same only 
///////////////////////////////////////////////////////////////////////////////
void TIPScript::CopyTIP(TIPImage **Src, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);	// get specs Source
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->Copy(Src, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}


///////////////////////////////////////////////////////////////////////////////
//	SobelTIP
//	- performs Sobel operation
///////////////////////////////////////////////////////////////////////////////
void TIPScript::SobelTIP(TIPImage **Src, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;
	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->Sobel(Src, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

///////////////////////////////////////////////////////////////////////////////
//	GaussianTIP
//	- performs gaussian filter by separable convolution
///////////////////////////////////////////////////////////////////////////////
void TIPScript::GaussianTIP(long FiltHW, float Sigma, TIPImage **Src, 
																TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;
	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We have a cuda array and it should not be so
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->GaussianBlur(FiltHW, Sigma, Src, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

///////////////////////////////////////////////////////////////////////////////
//	SplitComponentsTIP
//	- split RGB components Targets should be grey or colour images
///////////////////////////////////////////////////////////////////////////////
void TIPScript::SplitComponentsTIP(TIPImage **Src, TIPImage **Targ1, TIPImage **Targ2, 
								   TIPImage **Targ3)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;
	int	bpp1, bpp2, bpp3;
	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src > NULL) && (Targ1 > NULL) && (Targ2 > NULL) && (Targ3 > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if ((overlp != 0) || ((bpp !=24) && (bpp!=48)))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 24 or 48 bit)
		bpp1=m_pTIP->GetBpp(Targ1);
		bpp2=m_pTIP->GetBpp(Targ2);
		bpp3=m_pTIP->GetBpp(Targ3);
		if ((bpp1 != bpp2) || (bpp1 != bpp3) || ((bpp == 24) && (bpp1!=8) && (bpp1!=24)) || 
			((bpp == 48) && (bpp1!=8) && (bpp1!=24)))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Targ1) || m_pTIP->IsCudaArray(Targ2) || m_pTIP->IsCudaArray(Targ3))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Targ1, dev, w, h, overlp, -1);				// compare specs (can be cudaArray or not)
		CheckSpecsTipImage(Targ2, dev, w, h, overlp, -1);				// compare specs (can be cudaArray or not)
		CheckSpecsTipImage(Targ3, dev, w, h, overlp, -1);				// compare specs (can be cudaArray or not)
		if (bpp==24)
		m_pTIP->ComponentSplit(Src, Targ1, Targ2, Targ3);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

///////////////////////////////////////////////////////////////////////////////
//	JoinComponentsTIP
//	- join RGB components sources should be grey or colour images
///////////////////////////////////////////////////////////////////////////////
void TIPScript::JoinComponentsTIP(TIPImage **Src1, TIPImage **Src2, TIPImage **Src3, 
								  TIPImage **Target)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;
	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src1 > NULL) && (Src2 > NULL) && (Src3 > NULL) && (Target > NULL))
	{
		m_pTIP->GetImageSpecs(Src1, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (overlp != 0)
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 24 or 48 bit)
		if ((!isCuda) || (!m_pTIP->IsCudaArray(Src2)) || (!m_pTIP->IsCudaArray(Src3)))
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Target))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Src2, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		CheckSpecsTipImage(Src3, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		if ((bpp==8) || (bpp==16))bpp *=3;
		CheckSpecsTipImage(Target, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->ComponentJoin(Src1, Src2, Src3, Target);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

///////////////////////////////////////////////////////////////////////////////
//	ThresholdComponentsTIP
//	- threshold RGB components sources should be colour images
///////////////////////////////////////////////////////////////////////////////
void TIPScript::ComponentThresholdTIP(float Rmin, float Rmax, float Gmin, float Gmax, float Bmin, 
									  float Bmax, TIPImage **Src, TIPImage **Target)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;
	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src > NULL) && (Target > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if ((overlp != 0) || ((bpp!=24) && (bpp!=48)))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 24 or 48 bit)
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Target))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Target, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->ComponentThreshold(Rmin, Rmax, Gmin, Gmax, Bmin, Bmax, Src, Target);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

///////////////////////////////////////////////////////////////////////////////
//	BlendComponentsTIP
//	- Blend RGB components sources should be grey or colour images
///////////////////////////////////////////////////////////////////////////////
void TIPScript::BlendComponentsTIP(TIPImage **Src1, float A1, TIPImage **Src2, float A2, 
								   TIPImage **Src3, float A3, TIPImage **Target)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;
	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src1 > NULL) && (Src2 > NULL) && (Src2 > NULL) && (Target > NULL))
	{
		m_pTIP->GetImageSpecs(Src1, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (overlp != 0)
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 24 or 48 bit)
		if ((!isCuda) || (!m_pTIP->IsCudaArray(Src2)) || (!m_pTIP->IsCudaArray(Src3)))
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Target))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Src2, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		CheckSpecsTipImage(Src3, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		if ((bpp==8) || (bpp==16))bpp *=3;
		CheckSpecsTipImage(Target, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->ComponentBlend(Src1, A1, Src2, A2, Src3, A3, Target);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

///////////////////////////////////////////////////////////////////////////////
//	ComposedBlendTIP
//	- Blend RGB components source is a colour image
///////////////////////////////////////////////////////////////////////////////
void TIPScript::ComposedBlendTIP(TIPImage **Src, float A1, float A2, float A3, 
								 TIPImage **Target)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;
	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src > NULL) && (Target > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if ((overlp != 0) || ((bpp!=24) && (bpp!=48)))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 24 or 48 bit)
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Target))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Target, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->ComposedBlend(Src, A1, A2, A3, Target);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

///////////////////////////////////////////////////////////////////////////////
//	RGBHSITIP
//	- RGB to HSI conversion; sources should colour images
///////////////////////////////////////////////////////////////////////////////
void TIPScript::RGBHSITIP(TIPImage **Src, TIPImage **Target)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;
	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src > NULL) && (Target > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if ((overlp != 0) || ((bpp!=24) && (bpp !=48)))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 24 or 48 bit)
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Target))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Target, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->RGBHSI(Src, Target);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

///////////////////////////////////////////////////////////////////////////////
//	HSIRGBTIP
//	- HSI to RGB conversion; sources should colour images
///////////////////////////////////////////////////////////////////////////////
void TIPScript::HSIRGBTIP(TIPImage **Src, TIPImage **Target)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;
	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src > NULL) && (Target > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if ((overlp != 0) || ((bpp!=24) && (bpp !=48)))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 24 or 48 bit)
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Target))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Target, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->HSIRGB(Src, Target);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

///////////////////////////////////////////////////////////////////////////////
//	ColourDeconvTIP
//	- Blend RGB components source is a colour image
///////////////////////////////////////////////////////////////////////////////
void TIPScript::ColourDeconvTIP(TIPImage **Src, VARIANT FAR* pVCV1, VARIANT FAR* pVCV2, 
								VARIANT FAR* pVCV3, TIPImage **Target) 
{
	BOOL		isCuda;
	int			dev, w, h, overlp, bpp;
 	double		pCV1[3], pCV2[3], pCV3[3];
	SAFEARRAY   *pSafe;
    long        lB, uB, i;
    VARIANT     Var;
    int			Ret = IDS_OPOK;

	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src > NULL) && (Target > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if ((overlp != 0) || ((bpp!=24) && (bpp !=48)))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 24 or 48 bit)
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Target))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Target, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		CHECK_SAFEARR_EXCEPTION(GetArrayInfo(pVCV1, &pSafe, 1), return);
		SafeArrayGetLBound(pSafe, 1, &lB);
		SafeArrayGetUBound(pSafe, 1, &uB);
		if (uB - lB != 3) Ret = IDS_ARRAYDIMENSIONS;
		if (Ret==IDS_OPOK)
		{
			for (i=lB; i<uB; i++)
			{
				SafeArrayGetElement(pSafe, &i, &Var);
				pCV1[i-lB] = VariantDbl(Var);
			}
			CHECK_SAFEARR_EXCEPTION(GetArrayInfo(pVCV2, &pSafe, 1), return);
			SafeArrayGetLBound(pSafe, 1, &lB);
			SafeArrayGetUBound(pSafe, 1, &uB);
			if (uB - lB != 3) Ret = IDS_ARRAYDIMENSIONS;
			if (Ret==IDS_OPOK)
			{
				for (i=lB; i<uB; i++)
				{
					SafeArrayGetElement(pSafe, &i, &Var);
					pCV2[i-lB] = VariantDbl(Var);
				}
				CHECK_SAFEARR_EXCEPTION(GetArrayInfo(pVCV3, &pSafe, 1), return);
				SafeArrayGetLBound(pSafe, 1, &lB);
				SafeArrayGetUBound(pSafe, 1, &uB);
				if (uB - lB != 3) Ret = IDS_ARRAYDIMENSIONS;
				if (Ret==IDS_OPOK)
					for (i=lB; i<uB; i++)
					{
						SafeArrayGetElement(pSafe, &i, &Var);
						pCV3[i-lB] = VariantDbl(Var);
					}
			}
		}
		if (Ret==IDS_OPOK) m_pTIP->ColourDeconv(Src, pCV1, pCV2, pCV3, Target);
		if (Ret != IDS_OPOK)
			AfxThrowOleDispatchException(0xFF, Ret, 0);         // Flag the error to the script
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

/////////////////////////////////////////////////////////////////////////////
// Perform an Median filter
// Example:
//      MedianTIP Passes, Src, Dest
// Parameters:
//      Passes is the number of passes to do opening on
//      Src is the source handle for the image we wish to open
//      Dest is the source handle for the image we wish to open
/////////////////////////////////////////////////////////////////////////////

void TIPScript::MedianTIP(long Passes, TIPImage **Src, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
	    m_pTIP->Median((int) Passes, Src, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

///////////////////////////////////////////////////////////////////////////////
//	LaplaceTIP
//	- performs Laplace operation
///////////////////////////////////////////////////////////////////////////////
void TIPScript::LaplaceTIP(TIPImage **Src, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;
	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->Laplace(Src, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

///////////////////////////////////////////////////////////////////////////////
//	MeanTIP
//	- performs Mean operation
///////////////////////////////////////////////////////////////////////////////
void TIPScript::MeanTIP(TIPImage **Src, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;
	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->Mean(Src, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

///////////////////////////////////////////////////////////////////////////////
//	SimpleBlend
///////////////////////////////////////////////////////////////////////////////
afx_msg void TIPScript::SimpleBlend(VARIANT FAR* Images, TIPImage **Dest)
{
    int Ret = IDS_OPOK;
	SAFEARRAY *pSafe;
	VARIANT V;
	long i, uB, lB, Handle;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);
  
	// Get and check array is 1D
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Images, &pSafe, 1), return);

    // Get the bounds of the array Coefficients
    SafeArrayGetLBound(pSafe, 1, &lB);
    SafeArrayGetUBound(pSafe, 1, &uB);
    if (uB - lB < 2)
        Ret = IDS_ARRAYDIMENSIONS;
	BYTE** pImages=(BYTE **)new long[uB-lB];
	for (i = lB; i < (uB - lB) && Ret == IDS_OPOK; i++)
	{
        // Get the handle of the input image
        SafeArrayGetElement(pSafe, &i, &V);

        switch (V.vt)
        {
			case VT_I2:
				Handle = V.iVal;
				break;
			case VT_I4:
				Handle = V.lVal;
				break;
			default:
				Handle = UTS_ERROR;
		}
		pImages[i-lB]=(BYTE *)Handle;
	}
	if (Ret== IDS_OPOK)
		m_pTIP->SimpleBlend(uB-lB, pImages, Dest);
	delete pImages;
}

///////////////////////////////////////////////////////////////////////////////
//	HDRBlend
///////////////////////////////////////////////////////////////////////////////
afx_msg void TIPScript::HDRBlend(VARIANT FAR* Images, TIPImage **Dest)
{
    int Ret = IDS_OPOK;
	SAFEARRAY *pSafe;
	VARIANT V;
	long i, uB, lB, Handle;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);
  
	// Get and check array is 1D
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Images, &pSafe, 1), return);

    // Get the bounds of the array Coefficients
    SafeArrayGetLBound(pSafe, 1, &lB);
    SafeArrayGetUBound(pSafe, 1, &uB);
    if (uB - lB < 2)
        Ret = IDS_ARRAYDIMENSIONS;
	BYTE** pImages=(BYTE **)new long[uB-lB];
	for (i = lB; i < (uB - lB) && Ret == IDS_OPOK; i++)
	{
        // Get the handle of the input image
        SafeArrayGetElement(pSafe, &i, &V);

        switch (V.vt)
        {
			case VT_I2:
				Handle = V.iVal;
				break;
			case VT_I4:
				Handle = V.lVal;
				break;
			default:
				Handle = UTS_ERROR;
		}
		pImages[i-lB]=(BYTE *)Handle;
	}
	if (Ret== IDS_OPOK)
		m_pTIP->HDRBlend(uB-lB, pImages, Dest);
	delete pImages;
}

///////////////////////////////////////////////////////////////////////////////
//	RangeBlend
///////////////////////////////////////////////////////////////////////////////
afx_msg void TIPScript::RangeBlend(VARIANT FAR* Images, TIPImage **Dest)
{
    int Ret = IDS_OPOK;
	SAFEARRAY *pSafe;
	VARIANT V;
	long i, uB, lB, Handle;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);
  
	// Get and check array is 1D
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Images, &pSafe, 1), return);

    // Get the bounds of the array Coefficients
    SafeArrayGetLBound(pSafe, 1, &lB);
    SafeArrayGetUBound(pSafe, 1, &uB);
    if (uB - lB < 2)
        Ret = IDS_ARRAYDIMENSIONS;
	BYTE** pImages=(BYTE **)new long[uB-lB];
	for (i = lB; i < (uB - lB) && Ret == IDS_OPOK; i++)
	{
        // Get the handle of the input image
        SafeArrayGetElement(pSafe, &i, &V);

        switch (V.vt)
        {
			case VT_I2:
				Handle = V.iVal;
				break;
			case VT_I4:
				Handle = V.lVal;
				break;
			default:
				Handle = UTS_ERROR;
		}
		pImages[i-lB]=(BYTE *)Handle;
	}
	if (Ret== IDS_OPOK)
		m_pTIP->RangeBlend(uB-lB, pImages, Dest);
	delete pImages;
}

///////////////////////////////////////////////////////////////////////////////
//	CombineMultiExposures
///////////////////////////////////////////////////////////////////////////////
afx_msg void TIPScript::CombineMultiExposures(long Type, VARIANT FAR* Images, 
											  long OutputImage)
{
  	BYTE *DestAddr, *Addr;
    long W, H, P, BPP, iW, iH, iP, iBPP;
    long uB, lB, Handle;
    long  i;
    SAFEARRAY *pSafe;
    typedef BYTE *ImageAddr;
    ImageAddr *ImageBytes;
    VARIANT V;
    int Ret = IDS_OPOK;

     // Get and check array is 1D
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Images, &pSafe, 1), return);

    // Get the bounds of the array Coefficients
    SafeArrayGetLBound(pSafe, 1, &lB);
    SafeArrayGetUBound(pSafe, 1, &uB);

    // Addresses of the input images
    ImageBytes = new ImageAddr[uB - lB];

    // Get the image depth and mask sure images are 8 bits per pixel
	BPP = UTSMemRect.GetExtendedInfo(OutputImage, &W, &H, &P, &DestAddr);
	if ((BPP != 8) && (BPP != 16)) Ret = IDS_OPERATIONFAILED;
   // Check we have enough images for the blend
    if (uB - lB < 2)
        Ret = IDS_ARRAYDIMENSIONS;

    // Now check our input images and get the addresses of their image data
 	if (Ret == IDS_OPOK)
	{
		for (i = lB; i < (uB - lB) && Ret == IDS_OPOK; i++)
		{
			// Get the handle of the input image
			SafeArrayGetElement(pSafe, &i, &V);

			switch (V.vt)
			{
			case VT_I2:
				Handle = V.iVal;
				break;
			case VT_I4:
				Handle = V.lVal;
				break;
			default:
				Handle = UTS_ERROR;
			}

			// Do we have a valid handle in our array of handles ?
			if (UTSVALID_HANDLE(Handle))
			{
				// Yes it is so check its dimensions compared to our output image
				iBPP = UTSMemRect.GetExtendedInfo(Handle, &iW, &iH, &iP, &Addr);

				if (iBPP != BPP || iW != W || iH != H)
				{
					ImageBytes[i - lB] = NULL;
					Ret = IDS_OPERATIONFAILED;
				}
				else
					ImageBytes[i - lB] = Addr;
			}
			else
				Ret = IDS_INVALIDHANDLE;                        // SrcImage handle is invalid
		}
	}
    // All o.k. ?
    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);
	if (Ret == IDS_OPOK)
		m_pTIP->BlendMultipleExposures(Type, uB - lB, (BYTE **)ImageBytes, W, H, BPP, DestAddr);
	delete ImageBytes;

    if (Ret != IDS_OPOK)
        AfxThrowOleDispatchException(0xFF, Ret, 0);         // Flag the error to the script
}

///////////////////////////////////////////////////////////////////////////////
//	Histogram
///////////////////////////////////////////////////////////////////////////////
afx_msg long TIPScript::Histogram(TIPImage **Src,  long ColourMode)
{
	long	histHandle = 0, histsize;
	long	x, y, BPP;
	int		dev, width, height, overlp, bpp;
	BOOL	isCuda;
    DWORD	*hptr;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);
	if (Src > NULL)
		m_pTIP->GetImageSpecs(Src, &dev, &width, &height, &overlp, &bpp, &isCuda);
 	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
	if ((bpp==16)|| (bpp==48)) histsize=65536;
		else histsize=256;

	// Allocate a rectangle to store the info in 32 bits, 256 or 65536 values 
	histHandle = UTSMemRect.Create(32, histsize, 1);
	if (UTSVALID_HANDLE(histHandle))
	{
		UTSMemRect.GetExtendedInfo(histHandle, &x, &y, &BPP, &hptr);
		// Do the Histogram
		m_pTIP->Histogram(Src, (long *)hptr, (int)ColourMode);               
	}
	else
		AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);
    return histHandle;
}

///////////////////////////////////////////////////////////////////////////////
//	ConvolveSymTIP
//  - symmetric convolution same coefficients are applied for both
//	  norizontal and vertical pass
///////////////////////////////////////////////////////////////////////////////
void TIPScript::ConvolveSymTIP(long nPasses, VARIANT FAR *Coeff, TIPImage **Src, 
							   TIPImage **Dest)
{
    SAFEARRAY   *pSafe;
    long        i;
    long        lB1, uB1, R1;
	BOOL		isCuda;
	int			dev, w, h, overlp, bpp;
    float       *pData;
    VARIANT     V;
    int         HW;

	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		
		if (isCuda|| m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		if ((bpp !=8) && (bpp !=16) && (bpp !=24) && (bpp != 48))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);	// Invalid Image (should be 8 bit or 24 bit)

		CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Coeff, &pSafe, 1), return);
        
		// Get the bounds of the array Coefficients
		SafeArrayGetLBound(pSafe, 1, &lB1);
		SafeArrayGetUBound(pSafe, 1, &uB1);
		R1 = uB1 - lB1;

		// Check handle is valid
		// Unpack the coefficients array 
		pData = new float[R1];
		for (i = 0; i < R1; i++)
		{
            SafeArrayGetElement(pSafe, &i, &V);
            pData[i] = (float)VariantDbl(V);
        }

		HW = (R1 - 1) / 2;
		m_pTIP->ConvolveSym(nPasses, HW, pData, Src, Dest);

	    delete pData;
	}
}

///////////////////////////////////////////////////////////////////////////////
//	ConvolveTIP
//  - asymmetric convolution coefficient arrays for horizontal and vertical 
//	  pass may differ
///////////////////////////////////////////////////////////////////////////////
void TIPScript::ConvolveTIP(long nPasses, VARIANT FAR *CoeffHor, VARIANT FAR *CoeffVert, 
							TIPImage **Src, TIPImage **Dest)
{
    SAFEARRAY   *pSafe;
    long        i;
    long        lB1, uB1, R1;
	BOOL		isCuda;
	int			dev, w, h, overlp, bpp;
    float       *pDataHor, *pDataVert;
    VARIANT     V;
    int         filtHor, filtVert;

	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		
		if (isCuda|| m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		if ((bpp !=8) && (bpp !=16) && (bpp !=24) && (bpp != 48))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);	// Invalid Image (should be 8 bit or 24 bit)

		CHECK_SAFEARR_EXCEPTION(GetArrayInfo(CoeffHor, &pSafe, 1), return);
		SafeArrayGetLBound(pSafe, 1, &lB1);
		SafeArrayGetUBound(pSafe, 1, &uB1);
		R1 = uB1 - lB1;
		pDataHor = new float[R1];
		for (i = 0; i < R1; i++)
		{
            SafeArrayGetElement(pSafe, &i, &V);
            pDataHor[i] = (float)VariantDbl(V);
        }
		filtHor = (R1 - 1) / 2;

		CHECK_SAFEARR_EXCEPTION(GetArrayInfo(CoeffVert, &pSafe, 1), return);
		SafeArrayGetLBound(pSafe, 1, &lB1);
		SafeArrayGetUBound(pSafe, 1, &uB1);
		R1 = uB1 - lB1;
		pDataVert = new float[R1];
		for (i = 0; i < R1; i++)
		{
            SafeArrayGetElement(pSafe, &i, &V);
            pDataVert[i] = (float)VariantDbl(V);
        }
		filtVert = (R1 - 1) / 2;
		m_pTIP->Convolve(nPasses, filtHor, pDataHor, filtVert, pDataVert, Src, Dest);

	    delete pDataVert;
		delete pDataHor;
	}
}

/////////////////////////////////////////////////////////////////////////////
// KuwaharaTIP Perform an Kuwahara filter
// Example:
//      KuwaharaTIP filterHW, Src, Dest
// Parameters:
//      filterHW is the halfWidth of the filter1=3x3 2=5x5 filter
//      Src is the source handle for the image we wish to open 
//      Dest is the source handle for the image we wish to open
//		All bpp's are supported also overlap != 0
/////////////////////////////////////////////////////////////////////////////

void TIPScript::KuwaharaTIP(long FilterHW, TIPImage **Src, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
	    m_pTIP->Kuwahara((int) FilterHW, Src, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

/////////////////////////////////////////////////////////////////////////////
// UnpackGreyTIP
//	- converts 8 bit (packed) TIPImage into an unpacked 8 or 24 bit TIPImage
//	  or 16 bit (packed) TIPImage intoan unpacked 16 or 48 bit TIPImage
/////////////////////////////////////////////////////////////////////////////

void TIPScript::UnpackGreyTIP(TIPImage **Src, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
 	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		if ((bpp!=8) && (bpp!=16) && (overlp==0))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);	// Invalid Image (should be 8 or 16 bit)
		if ((bpp==8) && (m_pTIP->GetBpp(Dest)!=8) && (m_pTIP->GetBpp(Dest)!=24))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);	// Invalid Image (should be 8 or 16 bit)
		if ((bpp==16) && (m_pTIP->GetBpp(Dest)!=16) && (m_pTIP->GetBpp(Dest)!=48))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);	// Invalid Image (should be 8 or 16 bit)
		CheckSpecsTipImage(Dest, dev, w, h, 0, -1);						// compare specs (can be cudaArray or not)
	  m_pTIP->UnpackGrey(Src, Dest);
	}
}

/////////////////////////////////////////////////////////////////////////////
// PackGreyTIP
//	- converts 8 or 24 bit (unpacked) TIPImage into a packed 8 TIPImage
//	  or a 16 or 48 bit (unpacked) TIPImage into a packed 16 TIPImage
/////////////////////////////////////////////////////////////////////////////

void TIPScript::PackGreyTIP(TIPImage **Src, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;

	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		if ((overlp!=0) || (m_pTIP->GetOverlap(Dest)==0))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);	// Invalid Image (should be 8 or 16 bit)
		if (((bpp==8) || (bpp==24)) && (m_pTIP->GetBpp(Dest)!=8))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);	// Invalid Image (should be 8 or 16 bit)
		if (((bpp==16) || (bpp==48)) && (m_pTIP->GetBpp(Dest)!=16))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);	// Invalid Image (should be 8 or 16 bit)
		CheckSpecsTipImage(Dest, dev, w, h, -1, -1);					// compare specs (can be cudaArray or not)
	  m_pTIP->PackGrey(Src, Dest);
	}
}

/////////////////////////////////////////////////////////////////////////////
// ConvertTIP
//	- converts a TIPImage into a TIPImage with another bpp.
//	  Overlap should be 0; Device, Width and height should be the same.
/////////////////////////////////////////////////////////////////////////////
void TIPScript::ConvertTIP(TIPImage **Src, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;
	int dev1, w1, h1, overlp1, bpp1;

	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
  	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We do not have a cuda array
		if (m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		m_pTIP->GetImageSpecs(Dest, &dev1, &w1, &h1, &overlp1, &bpp1, &isCuda);		// get specs Source
		if ((dev != dev1) || (w != w1) || (h != h1) || (overlp != 0) ||
			(overlp1 != 0))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 24 or 48 bit)
		if ((bpp!=8) && (bpp != 16) && (bpp != 24) && (bpp != 32) && (bpp != 48) &&
			(bpp1!=8) && (bpp1 != 16) && (bpp1 != 24) && (bpp1 != 32) && (bpp1 != 48))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 24 or 48 bit)
		m_pTIP->Convert(Src, Dest);
	}
}   


/////////////////////////////////////////////////////////////////////////////
// Transfer a Component of a Colour image from the TIP
// Parameters:
//      ImageHandle is a valid UTS image handle
// Returns:
//      True if successful, False otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL TIPScript::GetComponentTIP(long Image, long component, TIPImage **Src)
{  
    long Width, Height, Pitch, Bpp;
    void *Addr;

    // Get info about this image
    Bpp=UTSMemRect.GetExtendedInfo(Image, &Width, &Height, &Pitch, &Addr);
    // Error checks
	if (Bpp == 8)
		CheckSpecsTipImage(Src, -1, Width, Height, -1, 24);	
	else if (Bpp == 16)
		CheckSpecsTipImage(Src, -1, Width, Height, -1, 48);		
	if (Bpp == 8)
		return m_pTIP->GetImageComponent(Src, component, (BYTE *) Addr);
	else if (Bpp==16)
		return m_pTIP->GetImageComponent(Src, component, (unsigned short *)Addr);
	else
        AfxThrowOleDispatchException(0xFF, IDS_GIPSIZEMISMATCH, 0);    
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// Transfer a subimage to the TIP
// Parameters:
//      ImageHandle is a valid UTS image handle
//      gpImage is the handle to a TIPImage to put the image in
//		x0, y0 - determine the offset from where the subimage should be copied
//				 the width and height of the gipimage determine the size of the subimage
//		ColourMode - 0 - convert colour to grey
//					 otherwise - no conversion
// Returns:
//      True if successful, False otherwise
// Notes:
//      An error will occur if images are not divisible by 4
/////////////////////////////////////////////////////////////////////////////

BOOL TIPScript::PutSubImageTIP(long Image, TIPImage **gpImage, long x0, long y0, 
							   long ColourMode)
{  
	BOOL isCuda;
    long Width, Height, Pitch, Bpp;
	int dev, w, h, overlp, bpp;
    void *Addr;

    // Get info about this image
    Bpp=UTSMemRect.GetExtendedInfo(Image, &Width, &Height, &Pitch, &Addr);
	if (gpImage <= 0)
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Invalid Image
    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
    if (Width % 4)
        AfxThrowOleDispatchException(0xFF, IDS_IMAGESIZEDIV4, 0);           // Not divisable by 4
	m_pTIP->GetImageSpecs(gpImage, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
	if ((w+x0 > Width) || (h+y0 > Height))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Invalid Image
	if (((Bpp==8) || (Bpp==24)) && (bpp!=8) && (bpp!=24))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Invalid Image
	if (((Bpp==16) || (Bpp==48)) && (bpp!=16) && (bpp!=48))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Invalid Image
    if (Bpp == 8)
		return m_pTIP->PutImage(gpImage, (BYTE *) Addr, Width, Height, x0, y0);
	else if (Bpp == 16)
		return m_pTIP->PutImage(gpImage, (unsigned short *) Addr, Width, Height, x0, y0);
	else if (Bpp == 24)
		return m_pTIP->PutImage(gpImage, (RGBTRIPLE *) Addr, Width, Height, x0, y0, ColourMode);
	else if (Bpp == 48)
		return m_pTIP->PutImage(gpImage, (RGB16BTRIPLE *) Addr, Width, Height, x0, y0, ColourMode);
	else
        AfxThrowOleDispatchException(0xFF, IDS_GIPSIZEMISMATCH, 0);         // Bpp mismatch
	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
//	CropImageTIP
//		- crops an image determined by x0, y0, w0, h0
/////////////////////////////////////////////////////////////////////////////
long TIPScript::CropImageTIP(TIPImage **Src, long x0, long y0, long w0, long h0)
{
 	BOOL isCuda;
	int dev, w, h, overlp, bpp;
	long HandleTipImage;

	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);
	if (Src !=NULL)
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if ((overlp != 0) || !isCuda || (x0+w0 > w) || (y0+h0 > h))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Invalid Image
		HandleTipImage=(long)m_pTIP->ImageCrop(Src, x0, y0, w0, h0);
        if (!HandleTipImage)
			AfxThrowOleDispatchException(0xFF, IDS_NOGIPIMAGECREATED, 0);            // We have no GIP so kick up an error
		return HandleTipImage;
	}
	return -1L;
}

/////////////////////////////////////////////////////////////////////////////
//	CropImageTIP
//		- crops an image to a width w0 and height h0
/////////////////////////////////////////////////////////////////////////////
long TIPScript::ResizeImageTIP(TIPImage **Src, long w0, long h0, long overl)
{
    long HandleTipImage;

	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);  
	if (Src !=NULL)
	{
		HandleTipImage=(long)m_pTIP->ImageResize(Src, w0, h0, overl);
        if (!HandleTipImage)
			AfxThrowOleDispatchException(0xFF, IDS_NOGIPIMAGECREATED, 0); 
		return HandleTipImage;
	}
	return -1L;
}

/////////////////////////////////////////////////////////////////////////////
//	FFT2DPowerSpectrumTIP
//		- power spectrum from FFT Image
/////////////////////////////////////////////////////////////////////////////
afx_msg long TIPScript::FFT2DPowerSpectrumTIP(TIPImage **Src, long bppOut)
{
    long HandleTipImage=NULL;
	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);  
	CheckSpecsTipImage(Src, -1, -1, -1, 0, 64);	
	if ((bppOut!=8) && (bppOut!=16))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);
	HandleTipImage=(long)m_pTIP->fft2DPowerSpectrum (Src, bppOut);
	if (!HandleTipImage)
		AfxThrowOleDispatchException(0xFF, IDS_NOGIPIMAGECREATED, 0); 
	return HandleTipImage;
}

/////////////////////////////////////////////////////////////////////////////
//	FFT2DTIP
//		- power spectrum from FFT Image
/////////////////////////////////////////////////////////////////////////////
afx_msg long TIPScript::FFT2DTIP( TIPImage **Src, long flag, long bppOut)
{
	long HandleTipImage=NULL;
 	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);  
	if (flag >= 0)	// in case of inverse fourier source should be complex image
		CheckSpecsTipImage(Src, -1, -1, -1, 0, 64);	
	HandleTipImage=(long)m_pTIP->fft2D(Src, flag, bppOut);
	if (!HandleTipImage)
		AfxThrowOleDispatchException(0xFF, IDS_NOGIPIMAGECREATED, 0);
	return HandleTipImage;
}

/////////////////////////////////////////////////////////////////////////////
//	ButterWorthFFT2DTIP
//		- Butterworthfilter in FFT domain
/////////////////////////////////////////////////////////////////////////////
afx_msg long TIPScript::ButterWorthFFT2DTIP(TIPImage **SrcHandle, long filtType, 
											float f1, float f2, float order)
{
	long HandleTipImage=NULL;
	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);  
	HandleTipImage=(long)m_pTIP->fft2DButterWorth(SrcHandle, filtType, f1, f2, order);
	return HandleTipImage;
}

/////////////////////////////////////////////////////////////////////////////
//	GaussianFFT2DTIP
//		- Gaussianfilter in FFT domain
/////////////////////////////////////////////////////////////////////////////
afx_msg long TIPScript::GaussianFFT2DTIP(TIPImage **SrcHandle, long filtType, 
										 float f1, float f2)
{
	long HandleTipImage=NULL;
	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);  
	HandleTipImage=(long)m_pTIP->fft2DGaussian(SrcHandle, filtType, f1, f2);
	if (!HandleTipImage)
		AfxThrowOleDispatchException(0xFF, IDS_NOGIPIMAGECREATED, 0); 
	return HandleTipImage;
}

/////////////////////////////////////////////////////////////////////////////
//	FFTImageRegistrationTIP
//		- determine image shift between two TIPImages
/////////////////////////////////////////////////////////////////////////////
afx_msg void TIPScript::FFTImageRegistrationTIP(TIPImage **Src1, TIPImage **Src2, 
							VARIANT FAR*  XShift, VARIANT FAR*  YShift, 
							VARIANT FAR*  sn, long MaxShift)
{
	float	SN;
	int	xshift, yshift, dev, w, h, overl, BPP;
	BOOL	isCuda;

	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);  
	if (Src1 > NULL)
		m_pTIP->GetImageSpecs(Src1, &dev, &w, &h, &overl, &BPP, &isCuda);
	if ((BPP != 8) && (BPP != 16) && (BPP != 24) && (BPP != 48) && (overl != 0))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);
	CheckSpecsTipImage(Src2, dev, w, h, overl, BPP);	
	m_pTIP->FFTImageRegistration(Src1, Src2, &xshift, &yshift, &SN, (int)MaxShift);
	V_VT(XShift)  = VT_I4;
	V_VT(YShift) = VT_I4;
	XShift->lVal  = (long)xshift;
	YShift->lVal = (long)yshift;
	V_VT(sn) = VT_R4;
	sn->fltVal=SN;
}

/////////////////////////////////////////////////////////////////////////////
//	FFTImageRegistrationTIP
//		- determine image shift between two complex FFT TIPImages
/////////////////////////////////////////////////////////////////////////////
afx_msg void TIPScript::CoreFFTImageRegistrationTIP(TIPImage **FFTSrc1, 
							TIPImage **FFTSrc2, VARIANT FAR*  XShift, 
							VARIANT FAR*  YShift, VARIANT FAR*  sn, 
							long MaxShift)
{
	float	SN;
	int		dev, xshift, yshift, w, h, overl, BPP;
	BOOL	isCuda;

	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);  
	if (FFTSrc1 > NULL)
		m_pTIP->GetImageSpecs(FFTSrc1, &dev, &w, &h, &overl, &BPP, &isCuda);
	if (BPP != 64)
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);
	CheckSpecsTipImage(FFTSrc2, dev, w, h, overl, BPP);	
	m_pTIP->CoreFFTImageRegistration(FFTSrc1, FFTSrc2, &xshift, &yshift, &SN, 
		(int)MaxShift);
	V_VT(XShift)  = VT_I4;
	V_VT(YShift) = VT_I4;
	XShift->lVal  = (long)xshift;
	YShift->lVal = (long)yshift;
	V_VT(sn) = VT_R4;
	sn->fltVal=SN;
}

/////////////////////////////////////////////////////////////////////////////
// FFTImageStitchTIP
//	- Source1 and Source2 are valid UTS handles
//	- mode 0 - aline top part src1 with bottom part src2
//		   1 - aline right part src1 with left part src2
//         2 - aline bottom part src1 with top part src2
//		   3 - aline left part src1 with right part src2
//	- overlap specifies maximum possible overlap in the direction specified 
//	  by the mode. 
//	- XShift and YShift will contain the shifts necessary to 
//    align Source2 with Source1
//  - sn is the ratio peakvalue at (Xshift, Yshift) and the average value 
//    in FFT product
/////////////////////////////////////////////////////////////////////////////
afx_msg void TIPScript::FFTImageStitchTIP(long Source1, long Source2, 
							long mode, long overlap, VARIANT FAR*  XShift, 
							VARIANT FAR*  YShift, VARIANT FAR*  sn)
{
	float	SN;
	int		xshft, yshft;
	long	w1, h1, pitch1, BPP1, w2, h2, pitch2, BPP2;
	void	*Addr1, *Addr2;

	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);  
	if (Source1>NULL)
		BPP1 = UTSMemRect.GetExtendedInfo(Source1, &w1, &h1, &pitch1, &Addr1);
	else BPP1=-1;
	if (w1 % 4)
		AfxThrowOleDispatchException(0xFF, IDS_IMAGESIZEDIV4, 0); 
	if (Source2>NULL)
		BPP2 = UTSMemRect.GetExtendedInfo(Source2, &w2, &h2, &pitch2, &Addr2);
	else BPP2=-2;
	if ((BPP1 != BPP2) || (w1!=w2) || (h1 !=h2))
        AfxThrowOleDispatchException(0xFF, IDS_GIPSIZEMISMATCH, 0); 
	if (BPP1==8)
		m_pTIP->FFTImageStitch((BYTE *)Addr1, (BYTE *)Addr2,  w1, h1, 
								mode, overlap, &xshft, &yshft, &SN);
	else if (BPP1==16)
		m_pTIP->FFTImageStitch((unsigned short *)Addr1, (unsigned short *)Addr2, 
								w1, h1, mode, overlap, &xshft, &yshft, &SN);
	else if (BPP1==24)
		m_pTIP->FFTImageStitch((RGBTRIPLE *)Addr1, (RGBTRIPLE *)Addr2, 
								w1, h1, mode, overlap, &xshft, &yshft, &SN);
	else if (BPP1==48)
		m_pTIP->FFTImageStitch((RGB16BTRIPLE *)Addr1, (RGB16BTRIPLE *)Addr2, 
								w1, h1, mode, overlap, &xshft, &yshft, &SN);
	V_VT(XShift)  = VT_I4;
	V_VT(YShift) = VT_I4;
	XShift->lVal  = xshft;
	YShift->lVal = yshft;
	V_VT(sn) = VT_R4;
	sn->fltVal=SN;
}

/////////////////////////////////////////////////////////////////////////////
// CoreFFTImageStitchTIP
//	- Source1 and Source2 are valid TIPImages
//	- mode 0 - aline top part src1 with bottom part src2
//		   1 - aline right part src1 with left part src2
//         2 - aline bottom part src1 with top part src2
//		   3 - aline left part src1 with right part src2
//	- overlap specifies maximum possible overlap in the direction specified 
//	  by the mode. 
//	- XShift and YShift will contain the shifts necessary to 
//    align Source2 with Source1
//  - sn is the ratio peakvalue at (Xshift, Yshift) and the average value 
//    in FFT product
/////////////////////////////////////////////////////////////////////////////
afx_msg void TIPScript::CoreFFTImageStitchTIP(TIPImage **src1, TIPImage **src2, 
							long mode, long overlap, VARIANT FAR*  XShift, 
							VARIANT FAR*  YShift,  VARIANT FAR*  sn)
{
	float	SN;
	int		xshft, yshft;
	int		dev, w, h, overl, BPP;
	BOOL	isCuda;

	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);  
	if (src1 > NULL)
		m_pTIP->GetImageSpecs(src1, &dev, &w, &h, &overl, &BPP, &isCuda);
	if ((BPP != 8) && (BPP != 16) && (BPP !=24) && (BPP !=48) && (overl != 0))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);
	CheckSpecsTipImage(src2, dev, w, h, overl, BPP);	
	m_pTIP->CoreFFTImageStitch(src1, src2, mode, overlap, &xshft, &yshft, &SN);
	V_VT(XShift)  = VT_I4;
	V_VT(YShift) = VT_I4;
	XShift->lVal  = xshft;
	YShift->lVal = yshft;
	V_VT(sn) = VT_R4;
	sn->fltVal = SN;
}

///////////////////////////////////////////////////////////////////////////////
//	UnsharpMask
//	- performs unsharp mask by separable convolution
///////////////////////////////////////////////////////////////////////////////
afx_msg void TIPScript::UnsharpMaskTIP(long FiltHW, float Sigma, float Amount, 
									TIPImage **Src, TIPImage **Dest)
{
	BOOL isCuda;
	int dev, w, h, overlp, bpp;
	if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);		// We have no TIP so kick up an error
	if ((Src > NULL) && (Dest > NULL))
	{
		m_pTIP->GetImageSpecs(Src, &dev, &w, &h, &overlp, &bpp, &isCuda);		// get specs Source
		if (isCuda || m_pTIP->IsCudaArray(Dest))
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		CheckSpecsTipImage(Dest, dev, w, h, overlp, bpp);				// compare specs (can be cudaArray or not)
		m_pTIP->UnsharpMask(FiltHW, Sigma, Amount, Src, Dest);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}


///////////////////////////////////////////////////////////////////////////////
//	NearestNeighbDeconv
//	- performs Nearest Neighbour Deconvolution based on unsharp Mask filter
///////////////////////////////////////////////////////////////////////////////
afx_msg void TIPScript::NearestNeighbDeconvTIP(long FilterHW, float Sigma, 
											   float Amount, 
											   VARIANT FAR* pImages, 
											   VARIANT FAR* pDestImages)
{
	BYTE		*Addr;
    int			Ret = IDS_OPOK;
	SAFEARRAY	*pSafe1, *pSafe2;
	VARIANT		V;
    typedef		BYTE *ImageAddr;
    ImageAddr	*ImageBytes, *DestBytes;
	long		i, uB1, lB1, uB2, lB2, Handle;
	long		W, H, BPP, iW, iH, iP, iBPP;


   if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);            // We have no GIP so kick up an error
	// Get and check array is 1D
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(pImages, &pSafe1, 1), return);
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(pDestImages, &pSafe2, 1), return);

    // Get the bounds of the array Coefficients
    SafeArrayGetLBound(pSafe1, 1, &lB1);
    SafeArrayGetUBound(pSafe1, 1, &uB1);
    SafeArrayGetLBound(pSafe2, 1, &lB2);
    SafeArrayGetUBound(pSafe2, 1, &uB2);
	if ((uB1 - lB1 < 3) || (uB2 - lB2 < 3)|| ((uB1 - lB1)!= (uB2 - lB2)))
		Ret = IDS_ARRAYDIMENSIONS;
	else
	{
		ImageBytes=new ImageAddr[uB1-lB1];
		DestBytes=new ImageAddr[uB2-lB2];
		for (i = lB1; i < (uB1 - lB1) && Ret == IDS_OPOK; i++)
		{
			// Get the handle of the input image
			SafeArrayGetElement(pSafe1, &i, &V);

			switch (V.vt)
			{
				case VT_I2:
					Handle = V.iVal;
					break;
				case VT_I4:
					Handle = V.lVal;
					break;
				default:
					Handle = UTS_ERROR;
			}
			if (UTSVALID_HANDLE(Handle))
			{
				iBPP=UTSMemRect.GetExtendedInfo(Handle, &iW, &iH, &iP, &Addr);
				if (i==lB1)
				{
					W=iW;
					H=iH;
					BPP=iBPP;
					ImageBytes[i-lB1]=Addr;
				}
				else if ((iBPP != BPP) || (iW != W) || (iH != H))
				{
					ImageBytes[i-lB1]=NULL;
					Ret = IDS_OPERATIONFAILED;
				}
				else
					ImageBytes[i-lB1]=Addr;
			}
			else
				Ret = IDS_INVALIDHANDLE;                        // SrcImage handle is invalid
		}
		for (i = lB2; i < (uB2 - lB2) && Ret == IDS_OPOK; i++)
		{
			// Get the handle of the destination images
			SafeArrayGetElement(pSafe2, &i, &V);

			switch (V.vt)
			{
				case VT_I2:
					Handle = V.iVal;
					break;
				case VT_I4:
					Handle = V.lVal;
					break;
				default:
					Handle = UTS_ERROR;
			}
			if (UTSVALID_HANDLE(Handle))
			{
				iBPP=UTSMemRect.GetExtendedInfo(Handle, &iW, &iH, &iP, &Addr);
				 if ((iBPP != BPP) || (iW != W) || (iH != H))
				{
					DestBytes[i-lB2]=NULL;
					Ret = IDS_OPERATIONFAILED;
				}
				else
					DestBytes[i-lB2]=Addr;
			}
			else
				Ret = IDS_INVALIDHANDLE;                        // SrcImage handle is invalid
		}

		// All o.k. ?
		if (Ret== IDS_OPOK)
			m_pTIP->NNDeconv(FilterHW, Sigma, Amount, uB1 - lB1, W, H, BPP, ImageBytes, DestBytes);
		delete ImageBytes;
		delete DestBytes;
	}
	if (Ret !=IDS_OPOK)
		AfxThrowOleDispatchException(0xFF, Ret, 0);
}


///////////////////////////////////////////////////////////////////////////////
//	DistanceMapTIP
//	- calculates distance map
///////////////////////////////////////////////////////////////////////////////
afx_msg void TIPScript::DistanceMapTIP(TIPImage **src, TIPImage **dest, BOOL EDMOnly, 
									   BOOL IsWhiteBackground, float NormFactor)
{
	BOOL isCuda, isCuda1;
	int dev, dev1, width, height, overlap, BPP, width1, height1, overlap1, BPP1;
   if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);        // We have no GIP so kick up an error
	if ((src > NULL) && (dest >NULL))
	{
		m_pTIP->GetImageSpecs(src, &dev, &width, &height, &overlap, &BPP, &isCuda);
		m_pTIP->GetImageSpecs(dest, &dev1, &width1, &height1, &overlap1, &BPP1, &isCuda1);
		if ((BPP != 8) && (BPP != 16))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image (should be 8 bit )
		if (((BPP==8)&& (BPP1 != 24)) || ((BPP==16) && (BPP1 != 48)))
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);		// Invalid Image
		CheckSpecsTipImage(dest, dev, width, height, overlap, -1);			 // compare specs (can be cudaArray or not)
		if (!isCuda)
			AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We have a cuda array and it should not be so
		if (isCuda1)
			AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
		m_pTIP->DistanceMap(src, dest, EDMOnly, IsWhiteBackground, NormFactor);
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

///////////////////////////////////////////////////////////////////////////////
//	CannyEdgeTIP
//	- calculates distance map
///////////////////////////////////////////////////////////////////////////////
afx_msg void TIPScript::CannyEdgeTIP(TIPImage **src, TIPImage **dest, float thresholdLow, 
									 float thresholdHigh)
{
	BOOL isCuda;
	int dev, width, height, overlap, BPP;

    if (!m_bTIPCreated)
        AfxThrowOleDispatchException(0xFF, IDS_NOGIPCREATED, 0);    // We have no TIP so kick up an error
	if (src > NULL)
		m_pTIP->GetImageSpecs(src, &dev, &width, &height, &overlap, &BPP, &isCuda);
	if ((BPP != 8) && (BPP !=16) && (BPP != 24) && (BPP !=48))
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);	// Invalid Image (should be 8 bit )
	CheckSpecsTipImage(dest, dev, width, height, overlap, -1);		// compare specs (can be cudaArray or not)
	if (!isCuda)
		AfxThrowOleDispatchException(0xFF, IDS_NOCUDA_ARRAY, 0);	// We have a cuda array and it should not be so
	if (m_pTIP->IsCudaArray(dest))
		AfxThrowOleDispatchException(0xFF, IDS_CUDA_ARRAY, 0);		// We have a cuda array and it should not be so
	m_pTIP->CannyEdge(src, dest, thresholdLow, thresholdHigh);
}

#endif
#endif