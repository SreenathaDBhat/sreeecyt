#include "stdafx.h"
#include "tmafunc.h"
#include "vcmemrect.h"
#include "vcutsdraw.h"
#include "uts.h"
#include "math.h"
#include "slidemap.h"
#include "spotdata.h"

//#using <mscorlib.dll>

CTMAFunc::CTMAFunc(void)
{
}

CTMAFunc::~CTMAFunc(void)
{
}

long CTMAFunc::CropImage(long SrcImage, long X0, long Y0, long X1, long Y1, long DestImage)\
{
	long	srcW, srcH, srcBPP, srcP;
	long	dstW, dstH, dstBPP, dstP;
	void	*srcAddr, *dstAddr;


	// Create a SpotData structure
	if ((SrcImage > 0) && (DestImage > 0))
	{
		srcBPP = UTSMemRect.GetExtendedInfo(SrcImage, &srcW, &srcH, &srcP, &srcAddr);
		dstBPP = UTSMemRect.GetExtendedInfo(DestImage, &dstW, &dstH, &dstP, &dstAddr);
//		TRACE2("X0:%ld Y0:%ld \r\n", X0, Y0);
//		TRACE2("X1:%ld Y1:%ld \r\n", X1, Y1);
//		TRACE2("scrW:%ld srcH:%ld \r\n", srcW, srcH);
//		TRACE2("dstW:%ld dstH:%ld \r\n", dstW, dstH);
//		TRACE2("srcP:%ld dstP:%ld \r\n", srcP, dstP);
//		TRACE2("srcBPP:%ld dstBPP:%ld \r\n", srcBPP, dstBPP);
		if ((X0 >= 0) && (X0 <srcW) && (X1 > X0) && (X1 <srcW) &&
			(Y0 >= 0) && (Y0 <srcH) && (Y1 > Y0) && (Y1 <srcH) &&
			((X1-X0+1)== dstW) && ((Y1-Y0+1)== dstH) && (srcBPP==dstBPP) &&
			((srcBPP==24) || (srcBPP=8)))
		{
			// Perform Crop
			DWORD	effWidthSrc, effWidthDst;
			BYTE	*pSrc, *pDst;
			long x, y, byt;
			effWidthSrc=(DWORD)srcP;
			effWidthDst=(DWORD)dstP;
			byt=srcBPP/8;
			pSrc=(BYTE *)srcAddr;
			pDst=(BYTE *)dstAddr;
			if (byt==1)
			{
				for (y= Y0; y<=Y1; y++)
				{
					pDst=(BYTE *)dstAddr+(y-Y0)*effWidthDst;
					pSrc=(BYTE *)srcAddr+y*effWidthSrc+X0*byt;
					for (x=X0; x<=X1; x++)
					{
						*pDst++=*pSrc++;
					}
				}
			}
			else
			{
				for (y= Y0; y<=Y1; y++)
				{
					pDst=(BYTE *)dstAddr+(y-Y0)*effWidthDst;
					pSrc=(BYTE *)srcAddr+y*effWidthSrc+X0*byt;
					for (x=X0; x<=X1; x++)
					{
						*pDst++=*pSrc++;
						*pDst++=*pSrc++;
						*pDst++=*pSrc++;
					}
				}
			}
			return (true);
		}
		else return (false);
	}
	else return(false);
}

#define	VAR_THRESHOLD		20.
#define	PROFILE_THRESHOLD	50

long CTMAFunc::FindTMACrop(long SrcImage, long* x0, long* y0, long* x1, long* y1,
						   int nr_filter, float deviationPerc, float subMapDistFact, LPCTSTR xmlFile)
{
	long		srcW, srcH, srcBPP, srcP;
	void		*srcAddr;
	long		*pPlotLin, pltLng;
	long		newx0, newy0, newx1, newy1;
	int			unitRows, unitColumns, expectedWidth, expectedHeight;
	double		meanRow, meanCol, mean;
	Quadrant	*quad;
	POSITION    pi;
	BOOL		xmlOK;

	if (SrcImage > 0)
	{
		srcBPP = UTSMemRect.GetExtendedInfo(SrcImage, &srcW, &srcH, &srcP, &srcAddr);
		xmlOK = m_coremap.Load(xmlFile);
		if (xmlOK)
		{
				m_xmlQuads = new CQuadrantData;
				m_xmlQuads->XMLtoQuadrants(&m_coremap, xmlOK);
				pi = m_xmlQuads->m_Quad.GetHeadPosition();
				unitRows=0;
				unitColumns=0;
				while (pi)
				{
					quad = &m_xmlQuads->m_Quad.GetNext(pi);
					if (quad->maxColumn > unitColumns) unitColumns=quad->maxColumn;
					if (quad->maxRow > unitRows) unitRows=quad->maxRow;
				}
				delete m_xmlQuads;
		}

		if (srcW > srcH)
		{
			pltLng=srcW;
			pPlotLin = new long[pltLng];
			HoriProfile(pPlotLin, pltLng, (BYTE *)srcAddr, srcW, srcH, srcP, 0, srcH);
			FilterProfile(pPlotLin, pltLng, nr_filter);
			meanCol=FindBoundaries(pPlotLin, pltLng, &newx0, &newx1, deviationPerc, subMapDistFact);
			delete pPlotLin;
			if (newx1 > newx0)
			{
				pltLng=srcH;
				pPlotLin = new long[pltLng];
				this->VertProfile(pPlotLin, pltLng, (BYTE *)srcAddr, srcW, srcH, srcP, newx0, newx1);
				this->FilterProfile(pPlotLin, pltLng, nr_filter);
				meanRow=FindBoundaries(pPlotLin, pltLng, &newy0, &newy1, deviationPerc, subMapDistFact);
				delete pPlotLin;
			}
		}
		else
		{
			pltLng=srcH;
			pPlotLin = new long[pltLng];
			this->VertProfile(pPlotLin, pltLng, (BYTE *)srcAddr, srcW, srcH, srcP, 0, srcW);
			FilterProfile(pPlotLin, pltLng, nr_filter);
			meanRow=FindBoundaries(pPlotLin, pltLng, &newy0, &newy1, deviationPerc, subMapDistFact);
			delete pPlotLin;
			if (newy1 > newy0)
			{
				pltLng=srcW;
				pPlotLin = new long[pltLng];
				this->HoriProfile(pPlotLin, pltLng, (BYTE *)srcAddr, srcW, srcH, srcP, newy0, newy1);
				this->FilterProfile(pPlotLin, pltLng, nr_filter);
				meanCol=FindBoundaries(pPlotLin, pltLng, &newx0, &newx1, deviationPerc, subMapDistFact);
				delete pPlotLin;
			}
		}
		if ((newx1 >newx0) && (newy1 > newy0))
		{
			if (!xmlOK)
			{
				// no way to check with expected size
				*x0=0;
				*x1=srcW-1;
				*y0=0;
				*y1=srcH-1;
				return (true);
			}
			else
			{
				if ((meanRow < 0.9*meanCol) || (meanRow >1.1*meanCol))
				{
					mean=max(meanRow, meanCol);
					meanCol=mean;
					meanRow=mean;
				}
				expectedWidth=(int)((double)unitColumns*meanCol);
				expectedHeight=(int)((double)unitRows*meanRow);
				if (expectedWidth > srcW) expectedWidth=srcW;
				if (expectedHeight > srcH) expectedHeight=srcH;
/*
				TRACE2("units Col:%3d meanCol:%8.2f  ",unitColumns,meanCol);
				TRACE2("units Row:%3d  meanRow:%8.2f  ",unitRows,meanRow);
				TRACE2("expected w:%4d  h:%3d  ",expectedWidth, expectedHeight);
				TRACE2("org w:%4ld  h:%4ld\r\n",srcW, srcH);
*/
				if ((newx1-newx0+1) >= (int)(0.8*(double)expectedWidth))
				{
					*x0=newx0;
					*x1=newx1;
				}
				else
				{	
					*x0=0;
					*x1=srcW-1;
				}
				if ((newy1-newy0+1) >=(int)(0.8*(double)expectedHeight))
				{
					*y0=newy0;
					*y1=newy1;
				}
				else
				{
					*y0=0;
					*y1=srcH-1;
				}
			}
//			TRACE2("x0:%4ld x1:%4ld  ",*x0, *x1);
//			TRACE2("y0:%4ld y1:%4ld\r\n",*y0, *y1);
		}
		else
		{
			*x0=0;
			*x1=srcW-1;
			*y0=0;
			*y1=srcH-1;
		}
		return (true);
	}
	else return(false);
}

void CTMAFunc::HoriProfile(long *pPlotLin, long pltLng, BYTE *srcAddr, 
						   long srcW, long srcH, long srcP, long y0, long y1)
{
	long i, j, value, val; 
	BYTE *q, *c;

	c=srcAddr;
	for (i=0; i<pltLng; i++)
	{
		value=0L;
		for (j=y0; j<y1; j++)
		{
			if (srcP <3*srcW)
			{
				q=c+j*srcP+i;							
				val=*q;
			}
			else
			{
				q=c+j*srcP+i*3L;
				val =(*q + *(q+1) + *(q+2))/3;
			}
			if (val < PROFILE_THRESHOLD) val=255;
			value +=val;
		}
		pPlotLin[i]=value;
	}
}

void CTMAFunc::VertProfile(long *pPlotLin, long pltLng, BYTE *srcAddr, 
						   long srcW, long srcH, long srcP, long x0, long x1)
{
	long i, j, value, val; 
	unsigned char *q, *c;

	c=srcAddr;
	for (i=0; i<pltLng; i++)
	{
		value=0L;
		for (j=x0; j<x1; j++)
		{
			if (srcP <3*srcW)
			{
				q=c+i*srcP+j;							
				val=*q;
			}
			else
			{
				q=c+i*srcP+j*3L;
				val =(*q + *(q+1) + *(q+2))/3;
			}
			if (val < PROFILE_THRESHOLD) val=255;
			value +=val;
		}
		pPlotLin[i]=value;
	}
}

void CTMAFunc::FilterProfile(long *pPlotLin, long pltLng, int nr_filter)
{
	long	hlp[7], value, i, j;

	for (j=0; j<nr_filter; j++)
	{
		for (i=0; i<7; i++) hlp[i]=pPlotLin[i];
		for (i=7; i<(pltLng-8); i++)
		{
			value= 8*pPlotLin[i]+7*(pPlotLin[i+1]+hlp[6])+6*(pPlotLin[i+2]+hlp[5])+5*(pPlotLin[i+3]+hlp[4])
					+4*(pPlotLin[i+4]+hlp[3])+3*(pPlotLin[i+5]+hlp[2])+2*(pPlotLin[i+6]+hlp[1])
					+(pPlotLin[i+7]+hlp[0]);
			hlp[0]=hlp[1]; hlp[1]=hlp[2]; hlp[2]=hlp[3]; hlp[3]=hlp[4]; hlp[4]=hlp[5];
			hlp[5]=hlp[6]; hlp[6]=pPlotLin[i];
			pPlotLin[i]=value/64;
		}
	}
}

double CTMAFunc::FindBoundaries(long *pPlotLin, long pltLng, long *c0, long *c1, float deviationPerc, float subMapDistFact)
{
	long			i, j, k, n, down, up;
	long			dif0[1000], dif1[1000];
	long			minV[1000], maxV[1000], prevVal;
	int				minP[1000], maxP[1000];
	double			var[1000];
	long			i0, i1, indx0[25], indx1[25], pos0[25], pos1[25];
	long			meanlow, meanhigh, maxVal;
	bool			found;


	j=1;
	k=0;
	maxVal=0;
	for (i=0; i<pltLng; i++)
		if (pPlotLin[i]> maxVal) maxVal=pPlotLin[i];
	prevVal=0;
	minV[0]=0;
	minP[0]=0;
	down=false;
	up=false;
	// determine Minima and maxima
	for (i=0; i<pltLng; i++)
	{
		if (pPlotLin[i]<prevVal) down=true;
		else if ((pPlotLin[i]>prevVal)&&down)
		{
			minV[j]=prevVal;
			minP[j]=i-1;
			j++;
			down=false;
		}
		if (pPlotLin[i]>prevVal) up=true;
		else if ((pPlotLin[i]<prevVal)&&up)
		{
			maxV[j]=prevVal;
			maxP[k]=i-1;
			k++;
			up=false;
		}
		prevVal=pPlotLin[i];
	}
	if(k<j) 
	{
		maxV[j]=minV[j];
		maxP[j]=minP[j];
	}
	// determine distance between minima + relative variation
	for (i=1; i<j-1; i++)
	{
//		TRACE2("%3d.  diff:%7d  ",i, maxV[i]-minV[i]);
		dif0[i]=abs(minP[i-1]-minP[i]);
		dif1[i]=abs(minP[i+1]-minP[i]);
//		TRACE2("pos:%4d dif-:%4d  ",minP[i],dif0[i]);
		var[i]=(double)abs(dif1[i]-dif0[i])*100./(double)(dif1[i]+dif0[i]);
//		TRACE2("dif+:%4d  var:%5.2f\r\n",dif1[i],var[i]);
	}
	// search largest part with relative low variation
	// to determine meanlow and meanhigh
	down=false;
	k=0;
	indx0[k]=0;
	indx1[k]=0;
	for (i=1; i<j-1; i++)
	{
		if (down)
		{
			if(var[i]>VAR_THRESHOLD)
			{
				down=false;
				i1=i-1;
				if ((i1-i0)>=(indx1[k]-indx0[k])){ indx0[k]=i0; indx1[k]=i1; }
			}
		}
		else if ((var[i]<VAR_THRESHOLD)&& ((maxV[i]-minV[i])>1000)) { down=true; i0=i; }
	}
	// determine mean + sd of distance between minima
	double mean=0.0;
	for (i=indx0[0]; i<=indx1[0]; i++)
	{
		mean +=dif0[i];
		mean +=dif1[i];
	}
	mean /=(double)(2*(indx1[0]-indx0[0]+1));
	meanlow= (long)(mean*(100.-deviationPerc)/100.); 
	meanhigh=(long)(mean*(100.+deviationPerc)/100.);
//	TRACE1("\r\nmean:%6.1f  ",mean); 
//	TRACE2("meanlow:%ld  meanhigh:%ld  ",meanlow, meanhigh);
//	TRACE2("based on i0:%4d  i1:%4d\r\n",indx0[0],indx1[0]);

	// try to find all parts within the meanlow and meanhigh
	// in sequence
	down=false;
	k=0;
	indx0[k]= 0;
	indx1[k]=0;
	for (i=1; i<j-1; i++)
	{
		if (down)
		{
			if ((dif0[i]<meanlow) || (dif0[i]>meanhigh) ||
						(dif1[i]<meanlow) || (dif1[i]>meanhigh))
			{
				down=false;
				indx0[k]= i0; 
				indx1[k]= i-1; 
				if ((indx0[k]!=indx1[k])||(maxV[i0]-minV[i0] > 500))
					k +=1;
				indx0[k]=0;
				indx1[k]=0;
			}
		}
		else if((dif0[i]>=meanlow) && (dif0[i]<=meanhigh) &&
					(dif1[i]>=meanlow) && (dif1[i]<=meanhigh)) 
		{ 
				down=true; 
				i0=i; 
		}
	}
	if (down)
	{
		indx0[k]=i0;
		indx1[k]=i-1;
		k++;
	}
/*
	TRACE("First pass\r\n");
	for (i=0; i<k; i++)
	{
		TRACE2("%d.  i0:%4ld  ",i+1, indx0[i]);
		TRACE2("i1:%4ld  p0:%4d  ",indx1[i],minP[indx0[i]]);
		TRACE1("p1:%4d\r\n",minP[indx1[i]]);
	}
*/
	// try to expand the parts 
	for (n=0; n<k; n++)
	{
		i=indx0[n];
		found=true;
		while ((i>1)&&found)
		{
			found=false;
			if ((((dif0[i-1]>=meanlow) && (dif0[i-1]<=meanhigh)) ||
				((dif1[i-1]>=meanlow) && (dif1[i-1]<=meanhigh)) ||
				((dif0[i-1]>=mean+meanlow) && (dif0[i-1]<=mean+meanhigh)) ||
				((dif1[i-1]>=mean+meanlow) && (dif1[i-1]<=mean+meanhigh))) &&
				(maxV[i]-minV[i] > 500))
			{
				found=true;
				indx0[n]=i-1;
			}
			i--;
		}
		i=indx1[n];
		found=true;
		while ((i<(j-2))&&found)
		{
			found=false;
			if ((((dif0[i+1]>=meanlow) && (dif0[i+1]<=meanhigh)) ||
				((dif1[i+1]>=meanlow) && (dif1[i+1]<=meanhigh)) ||
				((dif0[i+1]>=mean+meanlow) && (dif0[i+1]<=mean+meanhigh)) ||
				((dif1[i+1]>=mean+meanlow) && (dif1[i+1]<=mean+meanhigh))) &&
				(maxV[i]-minV[i] > 500))
			{
				indx1[n]=i+1;
				found=true;
			}
			i++;
		}
	}
/*
	TRACE("Second pass\r\n");
	for (i=0; i<k; i++)
	{
		TRACE2("%d.  i0:%4ld  ",i+1, indx0[i]);
		TRACE2("i1:%4ld  p0:%4d  ",indx1[i],minP[indx0[i]]);
		TRACE1("p1:%4d\r\n",minP[indx1[i]]);
	}
*/
	i=0;
	while(i < k-1)
	{
		if (indx1[i] >indx0[i+1])
		{
			// merge
			indx1[i]=indx1[i+1];
			for (n=i+1; n<k-1; n++)
			{
				indx0[n]=indx0[n+1];
				indx1[n]=indx1[n+1];
			}
			k-=1;
		}
		else i+=1;
	}
//	TRACE("Third pass (merged)\r\n");
	for (i=0; i<k; i++)
	{
//		TRACE2("%d.  i0:%4ld  ",i+1, indx0[i]);
//		TRACE2("i1:%4ld  p0:%4d  ",indx1[i],minP[indx0[i]]);
//		TRACE1("p1:%4d  ",minP[indx1[i]]);
		if (maxP[indx0[i]] > minP[indx0[i]])
		{
			if (indx0[i]==0)
				pos0[i]=0;
			else
				pos0[i]=maxP[indx0[i]-1];
			pos1[i]=maxP[indx1[i]];
		}
		else
		{
			pos0[i]=maxP[indx0[i]];
			if (indx1[j]==j-1)
				pos1[i]=pltLng-1;
			else
				pos1[i]=maxP[indx1[i]+1];
		}
//		TRACE2("mp0:%4d  mp1:%4d\r\n",pos0[i], pos1[i]);
	}
	i=0;
	while(i < k-1)
	{
		if ((pos0[i+1]-pos1[i])< (long)(subMapDistFact * mean))
		{
			// merge
			indx1[i]=indx1[i+1];
			pos1[i]=pos1[i+1];
			for (n=i+1; n<k-1; n++)
			{
				indx0[n]=indx0[n+1];
				indx1[n]=indx1[n+1];
				pos0[n]=pos0[n+1];
				pos1[n]=pos1[n+1];
			}
			k-=1;
		}
		else i+=1;
	}
/*
	TRACE("Fourth pass (merged on Position)\r\n");
	for (i=0; i<k; i++)
	{
		TRACE2("%d.  i0:%4ld  ",i+1, indx0[i]);
		TRACE2("i1:%4ld  p0:%4d  ",indx1[i],pos0[i]);
		TRACE1("p1:%4d\r\n",pos1[i]);
	}
*/
	*c0=0;
	*c1=0;
	for (i=0; i<k; i++)
	{
		if((pos1[i]-pos0[i]) >(*c1-*c0))
		{
			*c0=pos0[i];
			*c1=pos1[i];
		}
	}
//	TRACE2("Coordinates found: %ld - %ld\r\n",*c0, *c1);
	if (mean > 0.)
	{
		*c0 -=(long)(0.5*mean);
		if (*c0 < 0)*c0=0;
		*c1 +=(long)(0.5*mean);
		if (*c1 > (pltLng-1)) *c1=pltLng-1;
	}
//	TRACE2("Coordinates: %ld - %ld\r\n",*c0, *c1);
	return (mean);
}
