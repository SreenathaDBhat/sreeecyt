/////////////////////////////////////////////////////////////////////////////
// UTSscript.cpp : implementation file
//
// This OLE automation class is exposed to the script engine interface.
// Implementation of new UTS keywords is done here. 
//
// Written By Karl Ratcliff 24042001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
// Implementation of new keywords is simply done using the Classwizard (to
// be recommended). This is done by selecting the Automation tab and then
// selecting the class name: UTSscript. Add new 
//
/////////////////////////////////////////////////////////////////////////////
//
//	12Feb2002 JMB	Added UTSscript::Average
//	26Feb2002 JMB	Added UTSscript::QuantileThreshold
//	28Feb2002 JMB	Added UTSscript::HistogramMask and UTSscript::ReDimension
//	01Mar2002 JMB	Added UTSscript::GetPixel and UTSscript::SetPixel
//	27Mar2002 JMB	Fixed a mistake in UTSscript::RangeThreshold: mask is now
//					for threshold value only if thresholds are equal (as in
//					original UTS macro), rather than for all other values.
//  05Sep2003 DS	Add UTSScript::LogSum
//  03Dec2003 DS	Add Watershed transformation based on EDM/Ultimate Erosion Point

#include "stdafx.h"
#include "vcmemrect.h"
#include "commonscript.h"
#include "ComponentBlend.h"
#include "scriptutils.h"
#include "vcblob.h"
#include "vcgeneric.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "vcimproc.h"
#include "Colocalise.h"
#include "woolzif.h"
#include "vcthresh.h"
#include "uts.h"
#include "resource.h"
#include "safearr.h"
#include "ijImageProc.h"
#include "ColourDeconvolve.h"
#include <math.h>
#include "HoughTrans.h"
#include "UTSscript.h"
#include "ImageBits.h"
#include "SRGEngine.h"
#include "SRGRegionInfoScalar.h"
#include "SeedFinder.h"

#include "stdio.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))


/////////////////////////////////////////////////////////////////////////////
// UTSscript
/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(UTSscript, CCmdTarget)

UTSscript::UTSscript()
{
	EnableAutomation();
    // By default work in interactive mode
    m_ExecutionMode = modeDebug;


    for (int i = 0; i < MAX_EXTERNAL_IMAGES; i++)
    {
        m_ExternalImages[i].Image = NULL;
        m_ExternalImages[i].W = 0;
        m_ExternalImages[i].H = 0;
        m_ExternalImages[i].BPP = 0;
    }
}

UTSscript::~UTSscript()
{
    for (int i = 0; i < MAX_EXTERNAL_IMAGES; i++)
    {
        if (m_ExternalImages[i].Image)
            delete m_ExternalImages[i].Image;
        m_ExternalImages[i].W = 0;
        m_ExternalImages[i].H = 0;
        m_ExternalImages[i].BPP = 0;
    }
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// NOTE implement class members not exposed to the scripting engine here
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// Reset the queues and anything else that needs it
/////////////////////////////////////////////////////////////////////////////

void UTSscript::Reset(void)
{
    IP().UTSMemRect()->FreeAll();
}


void UTSscript::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}


BEGIN_MESSAGE_MAP(UTSscript, CCmdTarget)
	//{{AFX_MSG_MAP(UTSscript)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(UTSscript, CCmdTarget)
	//{{AFX_DISPATCH_MAP(UTSscript)
	DISP_FUNCTION(UTSscript, "MakeHistogram", MakeHistogram, VT_I4, VTS_PVARIANT)
	DISP_FUNCTION(UTSscript, "Histogram", Histogram, VT_I4, VTS_I4)
	DISP_FUNCTION(UTSscript, "GetHistogramData", GetHistogramData, VT_EMPTY, VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(UTSscript, "HistogramInfo", HistogramInfo, VT_EMPTY, VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(UTSscript, "LoadDIB", LoadDIB, VT_I4, VTS_BSTR)
	DISP_FUNCTION(UTSscript, "SaveDIB", SaveDIB, VT_EMPTY, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(UTSscript, "SaveJPG", SaveJPG, VT_EMPTY, VTS_I4 VTS_BSTR VTS_I2)
	DISP_FUNCTION(UTSscript, "LoadJPG", LoadJPG, VT_I4, VTS_BSTR)
	DISP_FUNCTION(UTSscript, "SaveJPGLS", SaveJPGLS, VT_EMPTY, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(UTSscript, "LoadJPGLS", LoadJPGLS, VT_I4, VTS_BSTR)
	DISP_FUNCTION(UTSscript, "LoadRAW", LoadRAW, VT_I4, VTS_BSTR VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "SaveRAW", SaveRAW, VT_EMPTY, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(UTSscript, "SaveAs", SaveAs, VT_EMPTY, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(UTSscript, "Threshold", Threshold, VT_EMPTY, VTS_I4 VTS_I4 VTS_BSTR VTS_I4)
	DISP_FUNCTION(UTSscript, "ThresholdGTE", ThresholdGTE, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "HandleIsValid", HandleIsValid, VT_BOOL, VTS_I4)
	DISP_FUNCTION(UTSscript, "Move", Move, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "CreateAsUsingBPP", CreateAsUsingBPP, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "Create", Create, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "Free", FreeIm, VT_EMPTY, VTS_PVARIANT)
	DISP_FUNCTION(UTSscript, "CreateAs", CreateAs, VT_I4, VTS_I4)
	DISP_FUNCTION(UTSscript, "CutImageFrag", CutImageFrag, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(UTSscript, "ImageToRGB", ImageToRGB, VT_EMPTY, VTS_I4 VTS_I4 VTS_BSTR VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "SmoothByZoom", SmoothByZoom, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(UTSscript, "AlphaBeta", AlphaBeta, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "Projection0", Projection0, VT_I4, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(UTSscript, "GetRectInfo", GetRectInfo, VT_I4, VTS_I4 VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(UTSscript, "GetRectBPP", GetRectBPP, VT_I4, VTS_I4)
	DISP_FUNCTION(UTSscript, "GetRectOriginalBPP", GetRectOriginalBPP, VT_I4, VTS_I4)
	DISP_FUNCTION(UTSscript, "LogSum", LogSum, VT_R8, VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "SmoothLongs", SmoothLongs, VT_EMPTY, VTS_I4 VTS_I4 VTS_I2)
	DISP_FUNCTION(UTSscript, "Stat1", Stat1, VT_I4, VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(UTSscript, "MinMaxSumInfo", MinMaxSumInfo, VT_I4, VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(UTSscript, "MinsAndMaxs", MinsAndMaxs, VT_I4, VTS_PVARIANT VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "ThickProfile", ThickProfile, VT_EMPTY, VTS_I4 VTS_PVARIANT VTS_R8 VTS_R8 VTS_R8 VTS_R8 VTS_R8)
	DISP_FUNCTION(UTSscript, "RGBToHSI", RGBToHSI, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "RGBToColour", RGBToColour, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "RGBToGray", RGBToGray, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "Median", Median, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(UTSscript, "PixelWise", PixelWise, VT_EMPTY, VTS_I4 VTS_BSTR VTS_I4)
	DISP_FUNCTION(UTSscript, "ConstPixelWise", ConstPixelWise, VT_EMPTY, VTS_I4 VTS_BSTR VTS_I4)
	DISP_FUNCTION(UTSscript, "PixelWiseMask", PixelWiseMask, VT_EMPTY, VTS_I4 VTS_BSTR VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "CopyUnderMask", CopyUnderMask, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "MulDiv", MulDiv, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	
	DISP_FUNCTION(UTSscript, "MoveSubRect", MoveSubRect, VT_EMPTY, VTS_I4 VTS_BSTR VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "Zoom", Zoom, VT_EMPTY, VTS_I4 VTS_I4 VTS_BSTR)
	DISP_FUNCTION(UTSscript, "ColorSaturTrans", ColorSaturTrans, VT_EMPTY, VTS_PVARIANT VTS_I2 VTS_R8 VTS_R8 VTS_R8 VTS_R8)
	DISP_FUNCTION(UTSscript, "RangeThreshold", RangeThreshold, VT_EMPTY, VTS_I4 VTS_I4 VTS_I2 VTS_I2)
	DISP_FUNCTION(UTSscript, "FFTRegisterImages", FFTRegisterImages, VT_BOOL, VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_I4)
	DISP_FUNCTION(UTSscript, "FFTPower", FFTPower, VT_BOOL, VTS_I4 VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(UTSscript, "FFT", FFT, VT_BOOL, VTS_I4 VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(UTSscript, "RegisterImages", RegisterImages, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_BSTR)
	DISP_FUNCTION(UTSscript, "FlipVertical", FlipVertical, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(UTSscript, "FlipHorizontal", FlipHorizontal, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(UTSscript, "ImageConvert", ImageConvert, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "IPLConvolve", IPLConvolve, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_PVARIANT VTS_I2)
	DISP_FUNCTION(UTSscript, "Average", Average, VT_EMPTY, VTS_I4 VTS_I2)
	DISP_FUNCTION(UTSscript, "ConstPixelMask", ConstPixelMask, VT_EMPTY, VTS_I4 VTS_BSTR VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "ConstPixelNotMask", ConstPixelNotMask, VT_EMPTY, VTS_I4 VTS_BSTR VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "ContShapeInfo", ContShapeInfo, VT_I4, VTS_PVARIANT VTS_I4 VTS_I4 VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(UTSscript, "QuantileThreshold", QuantileThreshold, VT_EMPTY, VTS_I4 VTS_I4 VTS_BSTR VTS_I4)
	DISP_FUNCTION(UTSscript, "HistogramMask", HistogramMask, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "ReDimension", ReDimension, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "GetPixel", GetPixel, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "SetPixel", SetPixel, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "IPLibrary", IPLibrary, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(UTSscript, "ColourToRGB", ColourToRGB, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "HSIToRGB", HSIToRGB, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "NumberUsedHandles", NumberUsedHandles, VT_I4, VTS_NONE)
	DISP_FUNCTION(UTSscript, "WatershedByEDM", WatershedByEDM, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_BSTR VTS_BOOL)
	DISP_FUNCTION(UTSscript, "AutoThreshold", AutoThreshold, VT_I4, VTS_I4 VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(UTSscript, "RankFilter", RankFilter, VT_EMPTY, VTS_I4 VTS_I4 VTS_R8 VTS_BSTR)
	DISP_FUNCTION(UTSscript, "MixtureModelThreshold", MixtureModelThreshold, VT_EMPTY, VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(UTSscript, "Ellipse", Ellipse, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_R8 VTS_R8 VTS_R8 VTS_I4 )
    DISP_FUNCTION(UTSscript, "Convert8To1", Convert8To1, VT_EMPTY, VTS_I4 VTS_I4)
    DISP_FUNCTION(UTSscript, "Convert1To8", Convert1To8, VT_EMPTY, VTS_I4 VTS_I4)
    DISP_FUNCTION(UTSscript, "Convert1To8WithValue", Convert1To8WithValue, VT_EMPTY, VTS_I4 VTS_I4 VTS_UI1)
    DISP_FUNCTION(UTSscript, "Convert16To8", Convert16To8, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "Convert16To8Scaled", Convert16To8Scaled, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
    DISP_FUNCTION(UTSscript, "Convert8To16", Convert8To16, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "HistogramStretch16To8", HistogramStretch16To8, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "ContrastStretch", ContrastStretch, VT_EMPTY, VTS_I4)
    DISP_FUNCTION(UTSscript, "PDMImage", PDMImage, VT_I4, VTS_I4 VTS_I2 VTS_I4 VTS_I2)
    DISP_FUNCTION(UTSscript, "OverlapK1K2", OverlapK1K2, VT_I4, VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT)
    DISP_FUNCTION(UTSscript, "Manders", MandersK1K2, VT_I4, VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(UTSscript, "Pearsons", Pearsons, VT_R8, VTS_I4 VTS_I4 VTS_I4)
    DISP_FUNCTION(UTSscript, "ColourDeconv", ColourDeconv, VT_I4, VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
    DISP_FUNCTION(UTSscript, "MaxProjectionImage", MaxProjectionImage, VT_I4, VTS_PVARIANT VTS_I4)
    DISP_FUNCTION(UTSscript, "ProjectionImage", ProjectionImage, VT_I4, VTS_I4 VTS_PVARIANT VTS_I4)
    DISP_FUNCTION(UTSscript, "MakeExternalImage", MakeExternalImage, VT_I4, VTS_I4 VTS_I4 VTS_I4)
    DISP_FUNCTION(UTSscript, "ExternalImageToUTS", ExternalImageToUTS, VT_I4, VTS_I4)
    DISP_FUNCTION(UTSscript, "UTSToExternalImage", UTSToExternalImage, VT_EMPTY, VTS_I4 VTS_I4)
    DISP_FUNCTION(UTSscript, "DetectCover", DetectCover, VT_BOOL, VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
    DISP_FUNCTION(UTSscript, "DetectCircularCover", DetectCircularCover, VT_BOOL, VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
    DISP_FUNCTION(UTSscript, "FractalBoxCount", FractalBoxCount, VT_R8, VTS_I4 VTS_I4)
    DISP_FUNCTION(UTSscript, "SRGSegment", SRGSegment, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "Variance", Variance, VT_EMPTY, VTS_I4 VTS_I4 VTS_UI1)
	DISP_FUNCTION(UTSscript, "BlankBorder", BlankBorder, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "TrimSkeletonTips", TrimSkeletonTips, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "LocalMaxima", LocalMaxima, VT_EMPTY, VTS_I4 VTS_I4 VTS_UI1)
	DISP_FUNCTION(UTSscript, "GetSeeds", GetSeeds, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(UTSscript, "GetSeedsByLocalMaxima", GetSeedsByLocalMaxima, VT_I4, VTS_I4 VTS_I4)

	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IUTSscript to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {4683F4A5-A3F9-4B2A-90E9-746458DCCD82}
static const IID IID_IUTSscript =
{ 0x4683f4a5, 0xa3f9, 0x4b2a, { 0x90, 0xe9, 0x74, 0x64, 0x58, 0xdc, 0xcd, 0x82 } };

BEGIN_INTERFACE_MAP(UTSscript, CCmdTarget)
	INTERFACE_PART(UTSscript, IID_IUTSscript, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// UTSscript message handlers
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
// Switch image processing libraries in the underlying DLLs
// VB script syntax:
//		IPLibrary(LibraryName)
// Parameters:
//		LibraryName is the name of the lib to use
//		e.g. SMART, INTEL, AIC, TEEM
////////////////////////////////////////////////////////////////////////////////////////


void UTSscript::IPLibrary(LPCTSTR NewLib) 
{
	// Check the parameter is correct
	if (_tcsstr(NewLib, _T("SMART")))
		IP().UTSMemRect()->UseIntelIPlib(CUTSMemRect::SMARTCHOICELIB);
	else
	if (_tcsstr(NewLib, _T("INTEL")))
		IP().UTSMemRect()->UseIntelIPlib(CUTSMemRect::INTELIPLIB);
	else
	if (_tcsstr(NewLib, _T("AIC")))
		IP().UTSMemRect()->UseIntelIPlib(CUTSMemRect::AICIPLIB);
	else
	if (_tcsstr(NewLib, _T("TEEM")))
		IP().UTSMemRect()->UseIntelIPlib(CUTSMemRect::TEEMMMXLIB);
	else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);    // Invalid parameter type
}

/////////////////////////////////////////////////////////////////////////////
// Histograms an image specified by Handle
// 
// VB script syntax:
//      HistogramData = Histogram(ImageHandle)
// Parameters:
//      ImageHandle is the identifier for the image we wish to histogram
// Returns:
//      HistogramData > 0 if successful. HistogramData is the identifier
//      for the histogram.
/////////////////////////////////////////////////////////////////////////////

long UTSscript::Histogram(long hImage) 
{
	long hHisto = 0;

    if (hImage < 1)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
    else
    {
		// Get The Bits Per Pixel For The Image
		long x, y;
		long bpp = IP().UTSMemRect()->GetRectInfo(hImage, &x, &y);

        if (bpp == EIGHT_BITS_PER_PIXEL)
        {
            DWORD histo[256];
            
            // Allocate a rectangle to store the info in
            // 32 bits, 256 values 
            hHisto = IP().UTSMemRect()->Create(32, 256, 1);
            
            // Created ok
            if (UTSVALID_HANDLE(hHisto))
            {
                // Do The Histogram
                IP().UTSMemRect()->Histogram(hImage, histo);
                
                // Get The Pointer To The Histogram Storage Area
				DWORD *hptr;
                IP().UTSMemRect()->GetExtendedInfo(hHisto, &x, &y, &bpp, &hptr);
                
                // Set The Data In The Memory Rectangle
                for (int i = 0; i < 256; i++)
                    *hptr++ = histo[i];
            }
            else
                AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);   // Mem allocation error
        }
        else
		{
			// CUTSMemRect::Histogram() only works with 8 bpp images, so call HistogramMask()
			// with a fake mask, to support other bit depths.
			long hMask = IP().UTSMemRect()->CreateAsWithPix(hImage, 1);
			if (UTSVALID_HANDLE(hMask))
			{
				IP().UTSMemRect()->OperMask(hMask, "Ones"); // Fill.
				hHisto = HistogramMask(hImage, hMask);
				IP().UTSMemRect()->FreeImage(hMask);
			}
            else
                AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);
		}
   }

    return hHisto;
}

/////////////////////////////////////////////////////////////////////////////
// Histograms a masked image
// 
// VB script syntax:
//      HistogramData = HistogramMask(hImage, hMask, numBins)
// Parameters:
//      hImage is the handle for the image we wish to histogram
//		hMask is the handle for the mask image
// Returns:
//      HistogramData > 0 if successful. HistogramData is the handle
//      for the histogram.
/////////////////////////////////////////////////////////////////////////////

long UTSscript::HistogramMask(long hImage, long hMask) 
{
	long hHisto = 0;

	if (hImage < 1)
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
	else
	{
		// Get The Bits Per Pixel For The Image
		long x, y;
		long bpp = IP().UTSMemRect()->GetRectInfo(hImage, &x, &y);

		if (bpp > 0)
		{
			// Allocate a memory rectangle large enough to store a histogram for an image with this bit depth.
			int numberOfBins = 1 << bpp;
			hHisto = IP().UTSMemRect()->Create(32, numberOfBins, 1); // 32 bits per bin.
            
			// Created ok
			if (UTSVALID_HANDLE(hHisto))
			{
				// Allocate a temporary block of memory for storing the histogram;
				DWORD *pHisto = new DWORD [ numberOfBins ];

				if (pHisto)
				{
					// Do The Histogram
					IP().UTSMemRect()->HistogramMask(hImage, hMask, pHisto, numberOfBins);
                
					// Get The Pointer To The Histogram Storage Area
					DWORD *hptr;
					IP().UTSMemRect()->GetExtendedInfo(hHisto, &x, &y, &bpp, &hptr);
                
					// Copy the data to the memory rectangle.
					for (int i = 0; i < numberOfBins; i++)
						*hptr++ = pHisto[i];

					delete[] pHisto;
				}
				else
					AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);
			}
			else
				AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);   // Mem allocation error
		}
        else
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);    // Invalid parameter type
	}

	return hHisto;
}

/////////////////////////////////////////////////////////////////////////////
// Get histogram data into an array
// 
// VB script syntax:
//      GetHistogramData HistogramHandle, HistogramData
// Parameters:
//      HistogramHandle is the handle of the histogram data
//      HistogramData is an array to put the data into
// Returns:
//      Nothing 
/////////////////////////////////////////////////////////////////////////////

void UTSscript::GetHistogramData(long Histogram, VARIANT FAR* HistogramData) 
{
    SAFEARRAY   *pSafe;
    long        X, Y, P, *ptr;

    if (Histogram > 0)
    {
        // Get info about HistogramData
        CHECK_SAFEARR_EXCEPTION(GetArrayInfo(HistogramData, &pSafe, 1), return);

        // Check the bounds of the array 
        // Check its large enough i.e. X elements
        IP().UTSMemRect()->GetExtendedInfo(Histogram, &X, &Y, &P, &ptr);
        CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 1, X), return);

        // Copy data across
        SetArrayLongData(pSafe, ptr, X);        // Data passed back as long values
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

////////////////////////////////////////////////////////////////////////////////////////
// Creates a histogram based on the data passed to the routine
//
// VB syntax:
//      HistogramHandle = MakeHistogram(HistogramData)
// Parameters:
//      HistogramData is a VB script array containing the data
// Returns:
//      A handle to the histogram, -1 if failed
// Notes:
//      Must be integer/long values
////////////////////////////////////////////////////////////////////////////////////////

long UTSscript::MakeHistogram(VARIANT FAR* HistogramData) 
{
    SAFEARRAY   *pSafe;
    long        lB, uB;
    long        UTSHandle = -1;
    long        i;
    long        temp;
    long        *pUTSMem;
    VARIANT     V;

    // Get info about HistogramData and check it is a 1D array
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(HistogramData, &pSafe, 1), return -1);

    // Now Get the dimensions of the array
    SafeArrayGetLBound(pSafe, 1, &lB);
    SafeArrayGetUBound(pSafe, 1, &uB);

    // Must be >= 1 element
    if ((uB - lB) < 1)
    {
        AfxThrowOleDispatchException(0xFF, IDS_ARRAYDIMENSIONS, 0);
        return UTS_ERROR;
    }
    
    // Allocate a UTS handle for it
    UTSHandle = IP().UTSMemRect()->Create(32, (uB - lB), 1);

    // Successfully made a rectangle so fill in the data
    if (UTSHandle > 0)
    {
        // Get the address of the UTS memory
        IP().UTSMemRect()->GetExtendedInfo(UTSHandle, &temp, &temp, &temp, &pUTSMem);
        // Get the data from the safe array and put in the memory rectangle
		            
		i = 0;
		SafeArrayGetElement(pSafe, &i, &V);

		switch (V.vt)
		{
		case VT_I2:
			for (i = 0; i < (uB - lB); i++)
			{
				SafeArrayGetElement(pSafe, &i, &V);
				*pUTSMem++ = (long)V.iVal;
			}
			break;

		case VT_I4:
			for (i = 0; i < (uB - lB); i++)
			{
				SafeArrayGetElement(pSafe, &i, &V);
				*pUTSMem++ = V.lVal;
			}
			break;

		default:
			for (i = 0; i < (uB - lB); i++)
			{
				SafeArrayGetElement(pSafe, &i, &V);
				*pUTSMem++ = (long)VariantDbl(V);
			}
			break;
		}
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_MEMALLOC, 0);       // Failed to allocate memory

    return UTSHandle;
}

////////////////////////////////////////////////////////////////////////////////////////
//  This function calculates some statistics for the histogram
//  given by frequency array.
//
//  VB syntax:
//      HistogramInfo HistogramHandle, Info
//	Arguments:
//		HistogramHandle - Handle to the histogram array
//      info - pointer to double array of output data;
//             must be at least 16 doubles in size
//
//     info[0] = Np  	: number of positive values in the histogram
//     info[1] = Imin: min I where Hist[I] is positive
//     info[2] = Imax: max I where Hist[I] is positive
//     info[3] = Sn  	: Sum of all values Hist(I)
//     info[4] = Av	: Average: (Sum(Hist(I) * I)) / Sn
//     info[5] = D	: Dispersion: (Sum(Hist(I) * (I - Av) ^ 2)) / Sn
//     info[6] = Bord	: Best separating value
//     					(is one of 0.5, 1.5, ... , histlen - 0.5)
//     info[7] = Sl	: Sum Hist(I) for I=0 to Bord- (==Ent(Bord))
//     info[8] = Sr	: Sum Hist(I) for I=Ceil(Bord+) to histlen-1
//     info[9] = Al	: Left Average: (Sum{0 to Bord-}(Hist(I) * I)) / Sl
//     info[10] = Ar	: Right Average: (Sum{Bord+ to histlen-1}(Hist(I) * I)) / Sr
//     info[11] = Dl	: Left Dispersion:
//     					(Sum{0 to Bord-}(Hist(I) * (I - Al)^2) / Sl
//     info[12] = Dr	: Right Dispersion:
//     					(Sum{Bord+ to histlen-1}(Hist(I) * (I - Ar)^2) / Sr
//     info[13] = Cs	: Coefficient of best separation:
//     					(((Sl*Sr) / (Sn*Sn)) * (Ar - Al) ^ 2) / (D + 1/12)
//     info[14] = Mode : Max value in the histogram
//     info[15] = ModePos : Position of mode
//
//    Returns:
//     Nothing
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::HistogramInfo(long HistogramHandle, VARIANT FAR* Information) 
{
    long    X, Y, Pitch;
    void    *Addr;
    double  Results[GHISTINFODATALENGTH];

    if (HandleIsValid(HistogramHandle))
    {
		for (int i = 0; i < GHISTINFODATALENGTH; i++)
			Results[i] = 0;

        // Check the validity of the input array - NOTE this is a histogram
        // so the BPP is 32 = sizeof(long) and X is the dimension were after
        IP().UTSMemRect()->GetExtendedInfo(HistogramHandle, &X, &Y, &Pitch, &Addr);

        IP().UTSGeneric()->HistInfo((long *) Addr, X, Results);

        SAFEARRAY   *pSafe;           // Safe array to put data into
        
        // Get and check array info
        CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Information, &pSafe, 1), return);
        
        // Check the dimension is >= GHISTINFODATALENGTH
        CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 1, GHISTINFODATALENGTH), return);
        
        // Copy data across
        SetArrayDoubleData(pSafe, Results, GHISTINFODATALENGTH);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // A handle is invalid
}

/////////////////////////////////////////////////////////////////////////////
// Load a bitmap from a file
// 
// VB script syntax:
//      Image = LoadDIB(FilePath)
// Parameters:
//      FilePath is the fully qualified file name for the image bmp to load
// Returns:
//      Image handle > 0 if successful
/////////////////////////////////////////////////////////////////////////////

long UTSscript::LoadDIB(LPCTSTR FilePathAndName) 
{
    long H;
	CUTSFileIO FIO(IP().UTSMemRect());

    H = FIO.LoadDIB(FilePathAndName, FALSE);       // Get The Image
    if (H < 1)
        AfxThrowOleDispatchException(0xFF, IDS_IMAGEFILENOTFOUND, 0);       // Could not find file

    return H;
}



////////////////////////////////////////////////////////////////////////////////////////
// Save an image in JPEG format
//
// VB syntax:
//      Result = SaveJPG(SrcImage, "AnImage.JPG", Quality)
// Parameters:
//      FileNameAndPath is the image name and any file path
//      SrcImage is the image to mask out parts of
//      Quality is the compression factor as a % i.e. 0-100
// Returns:
//      True if the file is saved o.k.
// Notes:
//      SrcImage must be 8 or 24 bits per pixel
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::SaveJPG(long UTSHandle, LPCTSTR FilenameAndPath, short Quality) 
{
    long BPP, W, H;
	CUTSFileIO FIO(IP().UTSMemRect());

	if (UTSHandle > 0)
    {
        // Get image info
        BPP = IP().UTSMemRect()->GetRectInfo(UTSHandle, &W, &H);
        // Check the quality is in range
        if ((BPP == ONE_BIT_PER_PIXEL) || (Quality < 0) || (Quality > 100))
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETERVALUE, 0);   // Parameter not correct
        else
        if (!FIO.SaveJPG(FilenameAndPath, UTSHandle, Quality))
            AfxThrowOleDispatchException(0xFF,  IDS_WRITEFAIL, 0);              // Could not save on disk
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);               // A handle is invalid
}


////////////////////////////////////////////////////////////////////////////////////////
// Save the raw bytes of an 8 or 16 bit image
//
// VB Syntax:
//		SaveRaw hImage, "D:\Counterstain.raw"
// Parameters:
//		UTSHandle is the handle of the image to be saved
//      FileNameAndPath is the image file name including the path
// Notes:
//      SrcImage must be 8 or 16 or 32 bits per pixel
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::SaveRAW(long UTSHandle, LPCTSTR FilenameAndPath)
{
    long BPP, W, H;
	CUTSFileIO FIO(IP().UTSMemRect());

	if (UTSHandle > 0)
    {
		CString filename = FilenameAndPath;
		filename = filename.Trim();
		if (filename.IsEmpty())
		{
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETERVALUE, 0);   // Parameter not correct
		}

        BPP = IP().UTSMemRect()->GetRectInfo(UTSHandle, &W, &H);
        if (BPP == EIGHT_BITS_PER_PIXEL || BPP == SIXTEEN_BITS_PER_PIXEL || BPP == THIRTYTWO_BITS_PER_PIXEL)
		{
			if (!FIO.SaveRAW(filename, UTSHandle))
				AfxThrowOleDispatchException(0xFF,  IDS_WRITEFAIL, 0);              // Could not save on disk
		}
		else
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETERVALUE, 0);   // Parameter not correct
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);               // A handle is invalid
}

/////////////////////////////////////////////////////////////////////////////
// Load RAW image data from a file
// 
// VB script syntax:
//      Image = LoadRAW(FilePath, ImageWidth, ImageHeight, BitsPerPixel)
// Parameters:
//      FilePath is the fully qualified file name for the image bmp to load
// Returns:
//      Image handle > 0 if successful
// NOTES:
//		1. Only pure uncompressed data, no image headers.
//		2. BPP must be 8, 16 or 32
/////////////////////////////////////////////////////////////////////////////
long UTSscript::LoadRAW(LPCTSTR FileNameAndPath, long Width, long Height, long BPP)
{
	long H;

	CUTSFileIO FIO(IP().UTSMemRect());

	H = FIO.LoadRAW(FileNameAndPath, Width, Height, BPP);       // Get The Image
	if (H < 1)
		AfxThrowOleDispatchException(0xFF, IDS_IMAGEFILENOTFOUND, 0);       // Could not find file

	return H;
}

/////////////////////////////////////////////////////////////////////////////
// Load  a JPEG image from a file
// 
// VB script syntax:
//      Image = LoadJPG(FilePath)
// Parameters:
//      FilePath is the fully qualified file name for the image bmp to load
// Returns:
//      Image handle > 0 if successful
/////////////////////////////////////////////////////////////////////////////

long UTSscript::LoadJPG(LPCTSTR FileNameAndPath) 
{
    long H;
	CUTSFileIO FIO(IP().UTSMemRect());

    H = FIO.LoadJPG(FileNameAndPath, FALSE);       // Get The Image
    if (H < 1)
        AfxThrowOleDispatchException(0xFF, IDS_IMAGEFILENOTFOUND, 0);       // Could not find file

    return H;
}

/////////////////////////////////////////////////////////////////////////////
// See the comments for LoadJPGLS below
/////////////////////////////////////////////////////////////////////////////

void UTSscript::SaveJPGLS(long UTSHandle, LPCTSTR FilenameAndPath)
{
	CUTSFileIO FIO(IP().UTSMemRect());

	if (UTSVALID_HANDLE(UTSHandle))
		FIO.SaveJPGLS(FilenameAndPath, UTSHandle);       // Get The Image
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // SrcImage handle is invalid
}

/////////////////////////////////////////////////////////////////////////////
// Load  a JPEG LS image from a file
// NOTE this will shift the image up to be a 16 bit image even if it was 
// 10/12/14 bits etc. When you save it again it will automatically be 16 bits
// 
// VB script syntax:
//      Image = LoadJPGLS(FilePath)
// Parameters:
//      FilePath is the fully qualified file name for the image bmp to load
// Returns:
//      Image handle > 0 if successful
/////////////////////////////////////////////////////////////////////////////

long UTSscript::LoadJPGLS(LPCTSTR FileNameAndPath)
{
    long H;
	CUTSFileIO FIO(IP().UTSMemRect());

	H = FIO.LoadJPGLS(FileNameAndPath, FALSE);       // Get The Image
    if (H < 1)
        AfxThrowOleDispatchException(0xFF, IDS_IMAGEFILENOTFOUND, 0);       // Could not find file
    return H;
}

//////////////////////////////////////////////////////////////////////////
// Writes an image (SrcImage) to a path and name (PathAndName) as a DIB
// VB Syntax:
//      SaveDIB SrcImage, PathAndName
// Parameters:
//      SrcImage is a UTS handle
//      PathAndName is the file name with a path
// Returns:
//      Nothing
//////////////////////////////////////////////////////////////////////////

void UTSscript::SaveDIB(long SrcImage, LPCTSTR PathAndName) 
{
	CUTSFileIO  FIO(IP().UTSMemRect());
    DWORD  Result;

    // Got a valid source image
    if (SrcImage > 0)
    {
        Result = FIO.SaveDIB(PathAndName, SrcImage);
        if (Result == ERROR_HANDLE_DISK_FULL)
            AfxThrowOleDispatchException(0xFF, IDS_DISKFULL, 0);       // Couldn't write to disk
        else
        if (Result != ERROR_SUCCESS && Result != ERROR_FILE_EXISTS && Result != ERROR_ALREADY_EXISTS)
            AfxThrowOleDispatchException(0xFF, IDS_WRITEFAIL, 0);

    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // SrcImage handle is invalid
}



//////////////////////////////////////////////////////////////////////////
// Writes an image (SrcImage) to a path and name (PathAndName) as a DIB
// VB Syntax:
//      SaveDIB SrcImage, PathAndName
// Parameters:
//      SrcImage is a UTS handle
//      PathAndName is the file name with a path
// Returns:
//      Nothing
//////////////////////////////////////////////////////////////////////////

long UTSscript::SaveAs(long SrcImage, LPCTSTR PathAndName) 
{
	CUTSFileIO  FIO(IP().UTSMemRect());
    long  Result = UTS_ERROR;

    // Got a valid source image
    if (SrcImage > 0)
    {
        Result = FIO.SaveAs(PathAndName, SrcImage);
        if (Result <= 0)
            AfxThrowOleDispatchException(0xFF, IDS_WRITEFAIL, 0);       // Couldn't write to disk
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // SrcImage handle is invalid

	return Result;
}

//////////////////////////////////////////////////////////////////////////
// Create an image with BitsPerPixel pixel depth and width and height 
// dimensions
// VB Syntax:
//      NewImage = Create(BitsPerPixel, Width, Height)
// Parameters:
//      BitsPerPixel is either 1, 8 or 24
//      Width and Height are the image dimensions
// Returns:
//      A UTS image handle, -1 otherwise
//////////////////////////////////////////////////////////////////////////

long UTSscript::Create(long BitsPerPixel, long Width, long Height) 
{
    long H = UTS_ERROR;

    // Check bit depth is valid
    if (BitsPerPixel != ONE_BIT_PER_PIXEL && 
        BitsPerPixel != EIGHT_BITS_PER_PIXEL && 
        BitsPerPixel != SIXTEEN_BITS_PER_PIXEL && 
        BitsPerPixel != TWENTYFOUR_BITS_PER_PIXEL &&
        BitsPerPixel != THIRTYTWO_BITS_PER_PIXEL)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);        // BPP is invalid
    else
    {
	    H = IP().UTSMemRect()->Create(BitsPerPixel, Width, Height);
        if (H < 1)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);       // Failed to alloc handle 
    }

    return H;
}

//////////////////////////////////////////////////////////////////////////
// Create an image with the same attributes as the SrcImage
// dimensions
// VB Syntax:
//      NewImage = CreateAs(SrcImage)
// Parameters:
//      SrcImage handle
// Returns:
//      A UTS image handle, -1 otherwise
//////////////////////////////////////////////////////////////////////////

long UTSscript::CreateAs(long SrcImage) 
{
    long H = UTS_ERROR;

    // Check source image is valid
    if (SrcImage > 0)
    {
        H = IP().UTSMemRect()->CreateAs(SrcImage);
        if (H < 1)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);   // Failed to alloc handle 
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // SrcImage handle is invalid

	return H;
}

/////////////////////////////////////////////////////////////////////////////
// Creates an image of the same size as ImageHandle but with BitsPerPixel
// depth
//
// VB script syntax:
//      NewImage = CreateAsusingBPP(ImageHandle, BPP)
// Parameters:
//      ImageHandle contains the image dimensions to create new image
//      BPP is the new pixel depth
// Returns:
//      A new image handle
/////////////////////////////////////////////////////////////////////////////

long UTSscript::CreateAsUsingBPP(long ImageHandle, long BitsPerPixel) 
{
	long H = UTS_ERROR;

    // Check for a valid bit depth
    if (BitsPerPixel != ONE_BIT_PER_PIXEL && 
        BitsPerPixel != EIGHT_BITS_PER_PIXEL  && 
		BitsPerPixel != SIXTEEN_BITS_PER_PIXEL &&
        BitsPerPixel != TWENTYFOUR_BITS_PER_PIXEL && 
        BitsPerPixel != THIRTYTWO_BITS_PER_PIXEL)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);    // BPP is invalid
    else
    if (ImageHandle < 1)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid
    else
    {
        H = IP().UTSMemRect()->CreateAsWithPix(ImageHandle, BitsPerPixel);      

        // Couldnt allocate memory for the new image
        if (H < 1)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);
    }

    return H;
}

/////////////////////////////////////////////////////////////////////////////
// Free simply deletes a memory rectangle
//
// VB script syntax
//      Free ImageHandle
// Parameters:
//      ImageHandle is the handle of the image we want to delete
// Returns:
//      Nothing. ImageHandle will be -1 on exit
/////////////////////////////////////////////////////////////////////////////

void UTSscript::FreeIm(VARIANT FAR* Handle) 
{
    // Check to make sure we have a valid handle
    if (Handle->lVal > 0)
    {
        IP().UTSMemRect()->FreeImage(Handle->lVal);  // Free the handle
        Handle->lVal = -1;              // Mark the handle as invalid
    }
}

/////////////////////////////////////////////////////////////////////////////
// Check the validity of a UTS image handle
// 
// VB syntax:
//      if (HandleIsValid(UTSHandle)) then ...
// Parameters:
//      UTSHandle is a potential or is a UTS image handle
// Returns:
//      True if it is a UTS handle, FALSE otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL UTSscript::HandleIsValid(long UTSHandle) 
{
    return IP().UTSMemRect()->HandleIsValid(UTSHandle);
}

/////////////////////////////////////////////////////////////////////////////
// Threshold thresholds an image
//
// VB script syntax
//      Threshold SrcImage, DestImage, Operator, ThresholdLevel
// Parameters:
//      SrcImage is the handle of the image we want to threshold
//      DestImage is the handle holding the result
//      Operator is either ">" ">=", "<=", "<", "="
//      ThresholdLevel is the level for the tresholding process
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void UTSscript::Threshold(long SrcImage, long DestImage, LPCTSTR Operator, long ThreshValue) 
{   
    // Check the thresholding Operator is o.k.
	long ret;
    if (CHECK_PARAMETER(ThresholdOps, Operator))
    {
        if (SrcImage > 0 && DestImage > 0)      // Is the source image handle valid ?
        {
	        // Now Do Threshold On Source Image - Result Into DestImage
		    ret=IP().UTSMemRect()->Threshold(SrcImage, DestImage, (char *) Operator, ThreshValue);
			if (ret<0)
				AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);
        }
        else
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);
}

/////////////////////////////////////////////////////////////////////////////
// ThresholdGTE : Thresholds an image such that pixels with values
//                greater than or equal to the threshold value, are set in the mask.
//                This does the same as Threshold hSrc, hDstMsk, ">=", thresholdVal,
//                but also works for 16bpp images.
//
// VB script syntax
//      ThresholdGTE hSrc, hDstMsk, thresholdVal
// Parameters:
//      hSrc is the handle of the image we want to threshold
//      hDstMsk is the handle holding the resulting mask
//      threshVal is the level for the thresholding process
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////
void UTSscript::ThresholdGTE(long hSrc, long hDstMsk, long threshVal)
{
	if (hSrc > 0 && hDstMsk > 0)
	{
		if (!IP().UTSMemRect()->ThresholdGTE(hSrc, hDstMsk, threshVal))
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);
	}
	else
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

/////////////////////////////////////////////////////////////////////////////
// Threshold an image
//
// VB script syntax
//      QuantileThreshold SrcImage, DestImage, Operator, ThresholdLevel
// Parameters:
//      SrcImage is the handle of the image we want to threshold
//      DestImage is the handle holding the result
//      Op is either ">" ">=", "<=", "<", "="
//      ThresholdLevel is the level for the thresholding process
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void UTSscript::QuantileThreshold(long srcImage, long destImage, LPCTSTR op, long thresholdQuantilePercent) 
{
	long xe, ye;
	if (IP().UTSMemRect()->GetRectInfo(srcImage, &xe, &ye) > 1)
	{
		DWORD thresh = (thresholdQuantilePercent * xe * ye + 50) / 100;

		IP().UTSMemRect()->Quantiles(srcImage, &thresh, 1);

		Threshold(srcImage, destImage, op, thresh);
	}
}

/////////////////////////////////////////////////////////////////////////////
// Move an image from SrcImage to DestImage
//
// VB script syntax:
//      Move SrcImage, DestImage
// Parameters:
//      SrcImage is the source image
//      DestImage is the destination image
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void UTSscript::Move(long SrcImage, long DestImage) 
{
    // Check for valid image handles
    if (SrcImage < 1 || DestImage < 1)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);

    IP().UTSMemRect()->Move(SrcImage, DestImage);
}

////////////////////////////////////////////////////////////////////////////////////////
// Both hndlA, hndlR must be valid rectangles of the same size
// (defined as pitch*yext ); 
// the function operates on 8-bit pixel data i.e. BYTE Wise
//
//			 handlR = hndlA <operation> hndlR
//  the operation is defined by 1st word of the parameter
//  (case insensitive):
//			"And",  "1&2"               A & R
//			"More", "1-2", "1&~2"       A & ~R  (1 if A > R; A without R)
//			"Copy", "1=>2"              A (copy A into R)
//			"Less", "2-1", "~1&2" 	    ~A & R  (1 if A < R; R without A)
//			"Xor",  "Mod2", "1^2"       A ^ R  (mod 2 {exclusive OR})
//			"Or",   "1|2" 	            A | R
//			"Nor",  "~(1|2)" 	        ~(A | R)  (not OR)
//			"Equal", "~(1^2)"           ~(A ^ R)  (1 if A = R)
//			"MoreEq", "1|~2" 	        A | ~R  (1 if A >= R)
//			"CopyNot", "~1=>2" 	        copy ~A into R
//			"LessEq", "~1|2" 	        ~A | R  (1 if A <= R)
//			"Nand", "~(1&2)" 	        ~(A & R)  (not AND)
//	        "MIN" 						A MIN R
//	        "MAX" 						A MAX r
//	        "plus", "+", "1+2" 		    (A + R) mod 256
//	        "+S", "Min(1+2,255)"   	    (A + R) min 255 (saturation)
//	        "Minus", "-", "1-2"    	    (A - R) mod 256
//	        "-L", "2-1" 			   	(R - A) mod 256 (left minus; inverse order)
//	        "-S", "Max(1-2,0)" 	   	    (A - R) max 0   (saturation)
//	        "-LS", "-SL", "Max(2-1,0)"  (R - A) max 0   (left and saturation)
//	        "LessTo0", "(1>=2)?1:0"     if A >= R then A else 0
//	        "MoreToFF", "(1<=2)?1:FF"   if A <= R then A else 255
//	        "ByteMask", "(1<2)?0:FF"    if A < R then 0 else 255
//	        "-A"					    if A < B then (B - A) else (A - B)
//	        "-A8"					    Abs(A - B) MIN (256 - Abs(A - B))
//	        "Abs32" 	                R = Abs(A)
//	        "MulS32" 	                R = A * R (Signed multiplication)
//	        "MulU32" 	                R = A * R (Unsigned multiplication)
//          "Diod32" 	                Keep positives, convert negatives to zero
//
// VB Syntax:
//      PixelWise SrcImage, Operator, ResultImage
// Parameters:
//      SrcImage = handlA above
//      Operator is one of the strings above
//      ResultImage = handlR above
// Returns:
// 		Nothing
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::PixelWise(long SrcImage, LPCTSTR Operator, long ResultImage) 
{
    long R;

    if (SrcImage < 1 || ResultImage < 1)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
    else
    {
        R = IP().UTSMemRect()->Pixelwise(SrcImage, (char *) Operator, ResultImage);
        if (R < 0)
            AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);
    }
}

////////////////////////////////////////////////////////////////////////////////////////
// hndlR must be a valid rectangle ; 
// The function operates on 1- or 8-bit pixel data:
//			 
// handlR = hndlA <operation> hndlR
//  the operation is defined by 1st word of the parameter
//  (case insensitive):
//
//	"And",      "1&2"               A & R
//	"More",     "1-2", "1&~2"       A & ~R  (1 if A > R; A without R)
//	"Copy",     "1=>2"              A (copy A into R)
//	"Less",     "2-1", "~1&2" 	    ~A & R  (1 if A < R; R without A)
//	"Xor",      "Mod2", "1^2"       A ^ R  (mod 2 {exclusive OR})
//	"Or",       "1|2" 	            A | R
//	"Nor",      "~(1|2)" 	        ~(A | R)  (not OR)
//	"Equal",    "~(1^2)"            ~(A ^ R)  (1 if A = R)
//	"MoreEq",   "1|~2" 	            A | ~R  (1 if A >= R)
//	"CopyNot",  "~1=>2" 	        copy ~A into R
//	"LessEq",   "~1|2" 	            ~A | R  (1 if A <= R)
//	"Nand",     "~(1&2)" 	        ~(A & R)  (not AND)
//	"MIN" 						    A MIN R
//	"MAX" 						    A MAX R
//	"plus",     "+", "1+2" 		    (A + R) mod 256
//	"+S",       "Min(1+2,255)"      (A + R) min 255 (saturation)
//	"Minus",    "-", "1-2"    	    (A - R) mod 256
//	"-L",       "2-1" 			    (R - A) mod 256 (left minus; inverse order)
//	"-S",       "Max(1-2,0)" 	    (A - R) max 0   (saturation)
//	"-LS",      "-SL", "Max(2-1,0)" (R - A) max 0   (left and saturation)
//	"LessTo0",  "(1>=2)?1:0"        if A >= R then A else 0
//	"MoreToFF", "(1<=2)?1:FF"       if A <= R then A else 255
//	"ByteMask", "(1<2)?0:FF"        if A < R then 0 else 255
//	"-A"					        if A < B then (B - A) else (A - B)
//	"-A8"					        Abs(A - B) MIN (256 - Abs(A - B))
//
// VB Syntax:
//      ConstPixelWise ByteValue, Operator, ResultImage
// Parameters:
//      ByteValue = handlA above
//      Operator is one of the strings above
//      ResultImage = handlR above
// Returns:
// 		Nothing
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::ConstPixelWise(long ByteValue, LPCTSTR Operator, long ResultImage) 
{
    if (ResultImage < 1)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
    else
    {
        if (IP().UTSMemRect()->ConstPixelwise(ByteValue, (char *) Operator, ResultImage) < 0)
            AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);
    }
}

//////////////////////////////////////////////////////////////////////////
// Cut out an image fragment at X, Y with Size by Size dimensions
// from SrcImage and put in ImageFragment
// VB Syntax:
//      ImageFragment = CutImageFrag(SrcImage, X, Y, Size, Xleft, Ytop)
// Parameters:
//      SrcImage is the image to cut the fragment from
//      X, Y are the centre of the fragment
//      FragSizeW, FragSizeH is the size of the fragment 
//      Xleft, Ytop are the returned top left corner of the fragment
// Returns:
//      Nothing
//////////////////////////////////////////////////////////////////////////

void UTSscript::CutImageFrag(long SrcImage, long CentreX, long CentreY, long DestImage, VARIANT FAR *Xleft, VARIANT FAR *Ytop) 
{
    long Xl, Yt, Xe, Ye, FragSizeW, FragSizeH;

    // Got a valid source image
    if (SrcImage > 0 && DestImage > 0)
    {
        // Get the extents of the image
        IP().UTSMemRect()->GetRectInfo(SrcImage, &Xe, &Ye);
        // Get the extents of the fragment
        IP().UTSMemRect()->GetRectInfo(DestImage, &FragSizeW, &FragSizeH);
        
        // Find XLeft and YTop of fragment and
        // re-adjust extents so that we dont cut over the image edges
        Xl = CentreX - FragSizeW / 2;
        
        if (Xl < 0)
            Xl = 0;
        else
        {
            if (Xl + FragSizeW > Xe)
                Xl = Xe - FragSizeW;
        }    
        Yt = CentreY - FragSizeH / 2;
        
        if (Yt < 0)
            Yt = 0;
        else
        {
            if (Yt + FragSizeH > Ye)
                Yt = Ye - FragSizeH;
        }    
        // Perform the cut
        IP().UTSMemRect()->MoveSubRect(SrcImage, Xl, Yt, DestImage, "=>");
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // SrcImage handle is invalid

    // Return fragment top left corner
    V_VT(Xleft) = VT_I4;
    V_VT(Ytop)  = VT_I4;
    Xleft->lVal = Xl;
    Ytop->lVal  = Yt;
}

//////////////////////////////////////////////////////////////////////////
// Combine image components to create a 24 bit colour composite with
// proportions specified by R, G, B with the results put into DestImage
// VB Syntax:
//      ImageToRGB  SrcImage, DestImage, Oper, R, G, B
// Parameters:
//      SrcImage is an 1/8 bit image to combine into DestImage
//      DestImage is a 24 bit colour image
//      Oper is either "Add" or "Fill"
//      R, G, B proportions of each colour component
// Returns:
//      Nothing
//////////////////////////////////////////////////////////////////////////

void UTSscript::ImageToRGB(long SrcImage, long DestImage, LPCTSTR Oper, long R, long G, long B) 
{
    // Got a valid source image
    if (SrcImage > 0)
    {
        if (IP().UTSMemRect()->BytesToRGB(SrcImage, DestImage, (char *) Oper, R, G, B) < 0)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // SrcImage handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
//  Smooth 8 bit per pixel rectangle, according
//  <zoommode> and <neioper>
//  <zoommode> values (case insensitive) are same
//  as for mrZoom():
// 	"Int" : interpolation
// 	"Sub" : subsampling
//  "Rep" : replication
//  <neioper> values (case insensitive) are same
//  as for mrNeiFun():
//     "AVER124" | "ERODE8" | "DILATE8" | "ERODE4" | "DILATE4"
// e.g.
// 	if (SmoothByZoom (Pict, 5, 16, "Sub", "AVER124") > 0) ...
// returns
//		ye if Ok,
//		-1 otherwise
//
// VB syntax:
//      SmoothByZoom SrcImage, Iterations, ZoomLevel, ZoomMode, NeighbourFunc
//  Parameters:
//      Returns
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::SmoothByZoom(long SrcImage, long Iterations, long ZoomLevel, LPCTSTR ZoomMode, LPCTSTR NeighbourFunc) 
{
    if (Iterations < 1 || ZoomLevel < 1)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);    // Ierations or ZoomLevel invalid
    else
    if (SrcImage > 0)
    {
        // Got a valid source image
        if (IP().UTSMemRect()->SmoothByZoom(SrcImage, Iterations, ZoomLevel, (char *) ZoomMode, (char *) NeighbourFunc) < 1)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // SrcImage handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// This function calcs an image according to the following rules:
//   Hndl3 = Gamma + ((Hndl1 * Alpha) + (Hndl2 * Beta)) / 256
// VB syntax:
//      AlphaBeta H1, H2, H3, Alpha, Beta, Gamma
// Parameters:
//   H1          - Source Image 1
//   H2          - Source Image 2
//   H3          - Destination Image
//   Alpha          - See above formula
//   Beta           - See above formula
//   Gamma          - See above formula
// Returns:
//   Nothing                        
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::AlphaBeta(long H1, long H2, long H3, long Alpha, long Beta, long Gamma) 
{
    if (H1 > 0 && H2 > 0 && H3 > 0)
    {
        if (IP().UTSMemRect()->AlphaBeta(H1, H2, H3, Alpha, Beta, Gamma, NULL) < 0)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);  // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // H1, H2 or H3 handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// Calculates the value defined by *operation
// VB syntax:
//      Result = Projection0
// Parameters:
//      Handle       - the source image
//      Operation    - "MIN", "MAX", "SUM", "ONES", "ZEROES"
// Returns:
//      Number of bytes 
////////////////////////////////////////////////////////////////////////////////////////

long UTSscript::Projection0(long SrcImage, LPCTSTR Oper) 
{
    long Result = 0;

    // Got a valid source image
    if (SrcImage > 0)
    {
        Result = IP().UTSMemRect()->Projection0(SrcImage, (char *) Oper);
        if (Result < 0)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // SrcImage handle is invalid

    return Result;
}

////////////////////////////////////////////////////////////////////////////////////////
// GetRectInfo gets info about an image rectangle
// VB syntax:
//      BitsPerPixel = GetRectInfo(SrcImage, Width, Height)
// Parameters:
//      SrcImage is the image we want the information about
//      Width and Height are returned image dimensions
// Returns:
//      BitsPerPixel of the image
////////////////////////////////////////////////////////////////////////////////////////

long UTSscript::GetRectInfo(long SrcImage, VARIANT FAR* Width, VARIANT FAR* Height) 
{
    long BPP = 0, W, H;

    // Got a valid source image
    if (SrcImage > 0)
    {
        BPP = IP().UTSMemRect()->GetRectInfo(SrcImage, &W, &H);
        V_VT(Width)  = VT_I4;
        V_VT(Height) = VT_I4;
        Width->lVal  = W;
        Height->lVal = H;
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // SrcImage handle is invalid

	return BPP;
}

////////////////////////////////////////////////////////////////////////////////////////
// GetRectBPP gets the bits per pixel for an image rectangle
// VB syntax:
//      bitsPerPixel = GetRectBPP(hImage)
////////////////////////////////////////////////////////////////////////////////////////
long UTSscript::GetRectBPP(long hImage) 
{
    long bpp = 0;
	long w, h;

    if (hImage > 0)
        bpp = IP().UTSMemRect()->GetRectInfo(hImage, &w, &h);
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);

	return bpp;
}

////////////////////////////////////////////////////////////////////////////////////////
// GetRectOriginalBPP returns the original bit depth when the rectangle was created.
//
// GetRectOriginalBPP() and GetRectBPP() are the same unless the original bit depth 
// was 10, 12 or 14 bpp in which case
//		GetRectOriginalBPP() = 10, 12 or 14
//		GetRectBPP()		 = 16			(the underlying bit depth in memory)
// GetRectOriginalBPP() can be used as a hint in scripts to apply initial scalings or
// normalisations after first getting an image when images may come from 10 and 12 bit
// sources.
//
// VB syntax:
//      bitsPerPixel = GetRectOriginalBPP(hImage)
////////////////////////////////////////////////////////////////////////////////////////
long UTSscript::GetRectOriginalBPP(long hImage) 
{
    long bpp = 0;

    if (hImage > 0)
        bpp = IP().UTSMemRect()->GetRectOriginalBPP(hImage);
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);

	return bpp;
}

////////////////////////////////////////////////////////////////////////////////////////
// LogSum gets the Sum of base 10 log of each pixel's value under the mask
// VB syntax:
//      SumOfLog = LogSum(SrcImage, MaskImage)
// Parameters:
//      SrcImage is the image we want calculate the log sum (most likely an intensity image
//      MaskImage is the mask
// Returns:
//      Sum of base 10 log of each pixels' value under the mask
////////////////////////////////////////////////////////////////////////////////////////

double UTSscript::LogSum(long SrcImage, long MaskImage)
{
	double result = 0.0;
	long width = 0;	// image width
	long height = 0;// image height
	long temp = 0;
	IP().UTSMemRect()->GetRectInfo(SrcImage, &width, &height);
	
	for (long i = 0; i < width; i++)
	{
		for (long j = 0; j < height; j++)
		{
			if (IP().UTSMemRect()->GetPixel(MaskImage, i, j) > 0)
			{
				temp = IP().UTSMemRect()->GetPixel(SrcImage, i, j);
				if (temp > 0)
					result += log10((long double)temp);
			}
		}
	}
	return result;
}

////////////////////////////////////////////////////////////////////////////////////////
// SmoothLongs - smooths an array of longs with NPoints over NSmooth points
// VB syntax:
//      SmoothLongs Handle, NPoints, NSmooth
// Parameters:
//      Handle - data to smooth in a memory rectangle
//      NPoints - number of points in rectangle
//      NSmooth - number of points to smooth over
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::SmoothLongs(long Handle, long NPoints, short NSmooth) 
{
    long *Ptr, Temp;

    // Got a valid source image
    if (Handle > 0)
    {
        IP().UTSMemRect()->GetExtendedInfo(Handle, &Temp, &Temp, &Temp, &Ptr);
        IP().UTSGeneric()->SmoothLongs(Ptr, NPoints, NSmooth);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // SrcImage handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// VB syntax:
//      Ones = Stat1(SrcImage, StatData)
//
// Data must be at least 15 longs; use bigger constant
//	 <mrStat1DataLength>  defined in <memrectb.h>
// for 1 bit per pixel memory rectangle calculates:
//	 Data[0]:=Sum(1)
//   Data[1]:=Sum(X)
//   Data[2]:=Sum(Y),
//   Data[3]:=Sum(X*X), 
//   Data[4]:=Sum(Y*Y),
//   Data[5]:=Sum(X*Y),
//   Data[6]:=Min(X), 
//   Data[7]:=Max(X),
//   Data[8]:=Min(Y), 
//   Data[9]:=Max(Y),
//   Data[10]:= 1000 * (shortest main axis / longest main axis)
//   Data[11]:= 1000 * (measure of compactness)
//   Data[12]:= 1000 * cos(ALPHA)
//   Data[13]:= 1000 * sin(ALPHA)
//   Data[14]:= 1000 * longest main axis (in pixel/1000)
//   	where ALPHA is an angle between X-axis and longest main axis
// returns: number of ONEs (same as in Data[0]) if OK,
//   	-1 if bad
////////////////////////////////////////////////////////////////////////////////////////

long UTSscript::Stat1(long SrcImage, VARIANT FAR* StatData) 
{
    long Ones = UTS_ERROR;
    long Data[MRSTAT1DATALENGTH];

    // Image handles valid ?
    if (SrcImage > 0)
    {
        Ones = IP().UTSMemRect()->Stat1(SrcImage, Data);

        // Success so return data to VB 
        if (Ones >= 0) 
        {
            SAFEARRAY *pSafe;           // Safe array to put data into

            // Get array info
            CHECK_SAFEARR_EXCEPTION(GetArrayInfo(StatData, &pSafe, 1), return -1);
    
            // Check the dimension is >= MRSTAT1DATALENGTH
            CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 1, MRSTAT1DATALENGTH), return -1);
            
            // Copy data across
            SetArrayLongData(pSafe, Data, MRSTAT1DATALENGTH);
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // SrcImage handle is invalid

    return Ones;
}

////////////////////////////////////////////////////////////////////////////////////////
//  MinMaxSumInfo
//  VB syntax:
//      PixSize = MinMaxSumInfo(SrcHandle, Data)
//  Parameters:
//      SrcHandle is the source image 
//      For 1/8/32 bit per pixel image fills Data[] as follows:
//      Data[0] = pixel size
//      Data[1] = X extend
//      Data[2] = Y extend
//      Data[3] = Min pixel value
//      Data[4] = Max pixel value
//      Data[5] = Sum of pixel values
//      Data[6] = X coordinate of a (first) pixel with Min value
//      Data[7] = Y coordinate of a (first) pixel with Min value
//      Data[8] = X coordinate of a (first) pixel with Max value
//      Data[9] = Y coordinate of a (first) pixel with Max value
//      Data[10] = number of pixels with Min value
//      Data[11] = number of pixels with Max value
//  Returns: 
//      pix size of OK, -1 if bad
////////////////////////////////////////////////////////////////////////////////////////

long UTSscript::MinMaxSumInfo(long SrcHandle, VARIANT FAR* Info) 
{
    long PixSize = -1;
    long Data[MRMINMAXSUMINFODATALENGTH];

    // Image handles valid ?
    if (SrcHandle > 0)
    {
        PixSize = IP().UTSMemRect()->MinMaxSumInfo(SrcHandle, Data);

        // Success so return data to VB 
        if (PixSize >= 0) 
        {
            SAFEARRAY *pSafe;           // Safe array to put data into

            // Get array info
            CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Info, &pSafe, 1), return UTS_ERROR);
    
            // Check the dimension is >= MRMINMAXSUMINFODATALENGTH
            CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 1, MRMINMAXSUMINFODATALENGTH), return UTS_ERROR);
            
            // Copy data across
            SetArrayLongData(pSafe, Data, MRMINMAXSUMINFODATALENGTH);
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // SrcImage handle is invalid

    return PixSize;
}




////////////////////////////////////////////////////////////////////////////////////////
//	Description:
//    Calculates positions of mins and maxs elements in given vector.
//
//    <v> 	 - pointer to the vector array
// 	  <nv> 	 - number of elements in <v>
//    <type> -
//      <type> & 0xff:
//        0 - char
//        1 - byte
//        2 - short
//        3 - word
//        4 - long
//        5 - dword
//
//        8 - double
//        9 - float
//      <type> & 0x100:
//        0 - isolated ends
//        1 - closed array (first and last elements are neighbors)
//
//    <presult> - pointer to send result
//
//    Updates <presult> with
//        list of minima/maxima indices;
//        if the list begins from a maximum then presult[0]=-1
//        if the list ends with a maximum then presult[last]=-1
//
//	Returns: number of elements in <presult> array
////////////////////////////////////////////////////////////////////////////////////////

long UTSscript::MinsAndMaxs(VARIANT FAR* Data, long PnPnt, long Noise, long Thresh, long Cycle) 
{
    SAFEARRAY *pSafe;           // Safe array to put data into
    long lB, uB;
    BYTE *pData = NULL;
    long i, Result = 0;
    VARIANT V;

    // Get array info
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Data, &pSafe, 1), return 0);
    // Get the bounds of the array 
    SafeArrayGetLBound(pSafe, 1, &lB);
    SafeArrayGetUBound(pSafe, 1, &uB);
    
    if ((uB - lB) > 0)
    {
        // Get the data
        pData = new BYTE[uB - lB];

        // Copy across
        if (pData)
        {
            // Copy the data into our array
            for (i = lB; i < uB; i++)
            {
                SafeArrayGetElement(pSafe, &i, &V);
                pData[i - lB] = V.bVal;
            }
            
            // Do the call
            Result = IP().UTSBlob()->MinsAndMaxs(pData, PnPnt, Noise, Thresh, Cycle, NULL, 0);

            delete pData;

        }
        else
            AfxThrowOleDispatchException(0xFF, IDS_MEMALLOC, 0);       // No memory allocation
    }

	return Result;
}

////////////////////////////////////////////////////////////////////////////////////////
// ThickProfile
//      Fills array <points> with <npoints> of BYTE pixel values e.g does a profile
// VB syntax:
//      ThickProfile SrcHandle, Points, X1, Y1, X2, Y2, Width
// Parameters:
//      SrcHandle is the image to do the profile on
//      Points is a array to store the data in
//      X1, Y1 to X2, Y2 define the line to do the profile along
//      Width specifies the width of the profile line
// Returns:
//      Nothing
// Notes:
//      "npoints" is determined by the dimensions of the array Points
////////////////////////////////////////////////////////////////////////////////////////
    
void UTSscript::ThickProfile(long SrcHandle, VARIANT FAR* Points, double X1, double Y1, double X2, double Y2, double RectWidth) 
{
    if (SrcHandle > 0)
    {
        SAFEARRAY   *pSafe;           // Safe array to put data into
        long        lB, uB, nPoints;
        BYTE        *pData = NULL;

        // Get and check array info
        CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Points, &pSafe, 1), return);
        // Get the bounds of the array 
        SafeArrayGetLBound(pSafe, 1, &lB);
        SafeArrayGetUBound(pSafe, 1, &uB);
        nPoints = uB - lB;
        
        if (nPoints)
        {
            pData = new BYTE[nPoints];

            if (pData)
            {
                // Do the call
                IP().UTSMemRect()->ThickProfile(SrcHandle, pData, nPoints, X1, Y1, X2, Y2, RectWidth);

                // Copy data across to the script
                SetArrayByteData(pSafe, pData, nPoints);

                // Dispose of mem
                delete pData;
            }
            else
                AfxThrowOleDispatchException(0xFF, IDS_MEMALLOC, 0);    // No memory allocation
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // Handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
//    Updates:
//    	resdata[0] = square of the min diameter;
//    	resdata[1] = square of the max diameter;
//    	resdata[2] = X1min
//    	resdata[3] = Y1min
//    	resdata[4] = X2min
//    	resdata[5] = Y2min
//    	resdata[6] = X1max
//    	resdata[7] = Y1max
//    	resdata[8] = X2max
//    	resdata[9] = Y2max
// 	Returns:
//  	 100 * (square of the min diameter)/(square of the max diameter)
////////////////////////////////////////////////////////////////////////////////////////

long UTSscript::ContShapeInfo(VARIANT FAR* PCodes, long NCodes, long Xstart, long Ystart, VARIANT FAR* Results) 
{
    SAFEARRAY   *pSafeR, *pSafeC;
    long        i, Ret = 0;
    VARIANT     V;
    long        Res[SPCONTSHAPEINFODATALENGTH];
    long        lB, uB, nPoints;
    BYTE        *pData;

    // Prevent the UTS crash if NCodes == 0
    if (!NCodes)
        return UTS_ERROR;

    // Get and check arrays are 1D
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(PCodes,  &pSafeC, 1), return 0);
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Results, &pSafeR, 1), return 0);
    // Check the results aray dimension is >= SPCONTSHAPEINFODATALENGTH
    CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafeR, 1, SPCONTSHAPEINFODATALENGTH), return 0);
    
    // Get the bounds of the array PCodes
    SafeArrayGetLBound(pSafeC, 1, &lB);
    SafeArrayGetUBound(pSafeC, 1, &uB);
    nPoints = uB - lB;
    
    if (nPoints)
    {
        pData = new BYTE[nPoints];
        
        if (pData)
        {
            // Load up our pData array for UTS call
            for (i = lB; i < uB; i++)
            {
                SafeArrayGetElement(pSafeC, &i, &V);
                pData[i - lB] = V.bVal;
            }
            
            // Do the call
            Ret = IP().UTSBlob()->ContShapeInfo(pData, NCodes, Xstart, Ystart, Res);

            // Copy the results data back
            SetArrayLongData(pSafeR, Res, SPCONTSHAPEINFODATALENGTH);

            // Clean up
            delete pData;
        }
        else
            AfxThrowOleDispatchException(0xFF, IDS_MEMALLOC, 0);    // No memory allocation
    }
   
    return Ret;
}

////////////////////////////////////////////////////////////////////////////////////////
// Converts colour R, G, B components into H, S and I components
// VB syntax:
//      RGBToHSI R, G, B, H, S, I
// Parameters:
//      R, G and B are handles to the R, G and B components
//      H, S and I are handles to the H, S and I results
// Returns:
//      Nothing
// Notes:
//      All images must be of the same dimensions. R, G, B and H, S, I are all
//      8 bit grey level images
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::RGBToHSI(long R, long G, long B, long H, long S, long I) 
{
    // Check handles are valid
    if (R > 0 && G > 0 && B > 0 && H > 0 && S > 0 && I > 0)
    {
         if (IP().UTSMemRect()->RGBToHSI(R, G, B, H, S, I) < 0)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // A handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// Converts colour R, G, B components into H, S and I components
// VB syntax:
//      HSIToRGB H, S, I, R, G, B
// Parameters:
//      H, S and I are handles to the H, S and I results
//      R, G and B are handles to the R, G and B components
// Returns:
//      Nothing
// Notes:
//      All images must be of the same dimensions. R, G, B and H, S, I are all
//      8 bit grey level images
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::HSIToRGB(long H, long S, long I, long R, long G, long B) 
{
    // Check handles are valid
    if (R > 0 && G > 0 && B > 0 && H > 0 && S > 0 && I > 0)
    {
         if (IP().UTSMemRect()->HSIToRGB(H, S, I, R, G, B) < 0)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // A handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// Converts colour 8 bit R, G, B components into a 24 bit colour image
// VB syntax:
//      RGBToColour R, G, B, RGBImage
// Parameters:
//      R, G and B are handles to the R, G and B components
//      RGBHandle is the resulting 24 bit image
// Returns:
//      Nothing
// Notes:
//      All images must be of the same dimensions. R, G, B and H, S, I are all
//      8 bit grey level images
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::RGBToColour(long R, long G, long B, long RGBHandle) 
{
    long BPPR, BPPG, BPPB, Temp;

    // Check handles are valid
    if (R > 0 && G > 0 && B > 0 && RGBHandle > 0)
    {
        // Get the image depth and mask sure images are 8 bits per pixel
        BPPR = IP().UTSMemRect()->GetRectInfo(R, &Temp, &Temp);
        BPPG = IP().UTSMemRect()->GetRectInfo(G, &Temp, &Temp);
        BPPB = IP().UTSMemRect()->GetRectInfo(B, &Temp, &Temp);

        // Prevent a UTS crash if less images not eight bits per pixel
        if (BPPR == EIGHT_BITS_PER_PIXEL &&
            BPPG == EIGHT_BITS_PER_PIXEL &&
            BPPB == EIGHT_BITS_PER_PIXEL)
        {
            if (IP().UTSMemRect()->BytesToRGB(R, RGBHandle, "Fill", 255, 0,   0) < 0)
                AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
            else
            if (IP().UTSMemRect()->BytesToRGB(G, RGBHandle, "Add",  0,   255, 0) < 0)
                AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
            else
            if (IP().UTSMemRect()->BytesToRGB(B, RGBHandle, "Add",  0,   0,   255) < 0)
                AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
        }
        else
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // A handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// Converts a colour image into a gray one, using R, G, B colour coefficients such that:
// Sum = (R + G + B)
// if (Sum > 0) then
//    PixelValue = (R * Red + G * Green + B * Blue) / Sum
// Else
//    if (Sum == 0)  then PixelValue = min(Red, Green, Blue)
//    if (Sum == -1) then PixelValue = median(Red, Green, Blue)
//    if (Sum == -2) then PixelValue = max(Red, Green, Blue)
//    if (Sum <= -3) then PixelValue = (Red + Green + Blue + 2) /3
// Endif
// VB syntax:
//      RGBToGray RGBImage, GrayImage, R, G, B
// Parameters:
//      RGBImage is the original colour image
//      GrayImage is the resulting gray image
//      R, G, B are the three coefficients
// Returns:
//      Nothing
// Notes:
//      Red, Green and Blue refer to the colour components in the RGBImage
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::RGBToGray(long RGBImage, long GrayImage, long R, long G, long B) 
{
    // Check handles are valid
    if (RGBImage > 0 && GrayImage > 0)
    {
         if (IP().UTSMemRect()->RGBToGray(RGBImage, GrayImage, R, G, B) < 0)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // A handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// Converts a colour image into its R, G, B components:
// Endif
// VB syntax:
//      ColourToRGB RGBImage, R, G, B
// Parameters:
//      RGBImage is the original colour image
//      R, G, B are the three separate component images (must be same dimensions as RGBImage)
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::ColourToRGB(long RGBImage, long R, long G, long B) 
{
    if (RGBImage > 0 && R > 0 && G > 0 && B > 0)
    {
         if (IP().UTSMemRect()->RGBToGray(RGBImage, R, 255, 0, 0) < 0)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
         if (IP().UTSMemRect()->RGBToGray(RGBImage, G, 0, 255, 0) < 0)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
         if (IP().UTSMemRect()->RGBToGray(RGBImage, B, 0, 0, 255) < 0)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // A handle is invalid
}


////////////////////////////////////////////////////////////////////////////////////////
// Median filter an image
//
// VB syntax:
//      Median SrcImage
// Parameters:
//      SrcImage is the handle to the image
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::Median(long SrcImage) 
{
    // Check handles are valid
    if (SrcImage > 0)
    {
        if (IP().UTSMemRect()->NeiFun(SrcImage, "MEDIAN", 1) < 0)
             AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // A handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// Average filter an image with 8-neighbour matrix {1,2,1},{2,4,2},{1,2,1}
//
// VB syntax:
//      Average srcImage iter
// Parameters:
//      srcImage is the handle to the image
//		iter is the number of iterations
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::Average(long srcImage, short iter) 
{
    // Check handles are valid
    if (srcImage > 0)
    {
        if (IP().UTSMemRect()->NeiFun(srcImage, "AVER124", iter) < 0)
             AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // A handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// Mask out parts of an image specified by Mask and Oper
//
// VB syntax:
//      PixelWiseMask SrcImage, Operation, MaskImage, DestImage
// Parameters:
//      SrcImage is the image to mask out parts of
//      Oper is the operation to use 
//          AND, MORE, Copy, LESS, XOR, OR, NOR, EQUAL, MoreEq, CopyNot, LessEq, Nand 
//      MaskImage is the mask image
//      DestImage is the resulting destination image
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::PixelWiseMask(long SrcImage, LPCTSTR Operation, long MaskImage, long DestImage) 
{
	if (SrcImage > 0 && MaskImage > 0 && DestImage > 0)
    {
        if (IP().UTSMemRect()->PixelWiseMask(SrcImage, (char *) Operation, DestImage, MaskImage) < 0)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);               // A handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// CopyUnderMask : Copy pixels from an image that correspond with non-zero pixels in a mask.
//                 This does the same as PixelWiseMask hSrc "Copy" hMask hDst, but
//                 also works for 16bpp images.
// VB syntax:
//      CopyUnderMask hSrc hMask hDst
// Parameters:
//      hSrc is the image to mask out parts of
//      hMsk is the mask image (1bpp)
//      hDst is the resulting destination image
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////
void UTSscript::CopyUnderMask(long hSrc, long hMsk, long hDst)
{
	if (hSrc > 0 && hMsk > 0 && hDst > 0)
    {
        if (!IP().UTSMemRect()->CopyUnderMask(hSrc, hMsk, hDst))
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}


void UTSscript::ConstPixelMask(long Constant, LPCTSTR Oper, long HandleR, long HandleM) 
{
	if (HandleR > 0 && HandleM > 0)
    {
        if (IP().UTSMemRect()->ConstPixelMask(Constant, (char *) Oper, HandleR, HandleM) < 0)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);               // A handle is invalid
}

void UTSscript::ConstPixelNotMask(long Constant, LPCTSTR Oper, long HandleR, long HandleM) 
{
	if (HandleR > 0 && HandleM > 0)
    {
        if (IP().UTSMemRect()->ConstPixelNotMask(Constant, (char *) Oper, HandleR, HandleM) < 0)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);               // A handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// Multiplies/Divides pixel values in an image e.g. 
// Src[X, Y] = Min(Src[X, Y] * Multiplier / Divisor, 0xFF)
//
// VB syntax:
//      MulDiv SrcImage, Multiplier, Divisor
// Parameters:
//      SrcImage is the image to mask out parts of
//      Multiplier is the value to multiply pixel values by
//      Divisor is the value to divide pixel values by
// Returns:
//      Nothing
// Notes:
//      SrcImage must be 8 bits per pixel
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::MulDiv(long SrcImage, long Multiplier, long Divisor) 
{
	if (SrcImage > 0)
    {
        if (IP().UTSMemRect()->MulDiv(SrcImage, Multiplier, Divisor) < 0)
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);       // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);               // A handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// Moves a big rectangle to a small one or vice versa
//
// VB syntax:
//      MoveSubRect BigImage, Direction, SmlImage, Left, Top
// Parameters:
//      BigImage is the big image to move to or from
//      Direction is either "<=" (Sml to Big) or "=>" (Big to Sml)
//      SmlImage is the small image to move
//      Left, Top are the coordinates in the big image
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::MoveSubRect(long BigImage, LPCTSTR Direction, long SmlImage, long Left, long Top) 
{
    // Check for valid handles
    if (UTSVALID_HANDLE(BigImage) && UTSVALID_HANDLE(SmlImage))
    {
        // Check the direction parameter
        if (CHECK_PARAMETER(DirOps, Direction))
        {
            // Do the move
            if (IP().UTSMemRect()->MoveSubRect(BigImage, Left, Top, SmlImage, (char *) Direction) <= 0)
                AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
        }
        else
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);    // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);          // A handle is invalid
}


////////////////////////////////////////////////////////////////////////////////////////
// Zoom zooms SrcImage into DestImage by Method
// VB Syntax:
//   Zoom SrcHandle, DestHandle, Method
// Parameters:
//   SrcHandle is the image to zoom
//   DestHandle is the image to zoom SrcHandle into
//   Method is either:
//      "INT"   for interpolation
//      "SUB"   for subsampling
//      "REP"   for replication
// Returns:
//   Nothing
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::Zoom(long SrcHandle, long DestHandle, LPCTSTR Method) 
{
    // Check for valid handles
    if (UTSVALID_HANDLE(SrcHandle) && UTSVALID_HANDLE(DestHandle))
    {
        // Check the direction parameter
        if (CHECK_PARAMETER(ZoomOps, Method))
        {
            // Do the zoom
            if (IP().UTSMemRect()->Zoom(SrcHandle, DestHandle, (char *) Method) <= 0)
                AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
        }
        else
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);    // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);          // A handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
//  ColourSaturTrans:
// <hndls> contains mrHandles of <hnum> color components.
//          the transformation is pixelwise, pixel values to be changed so
//          that: 1) sum of values is invariant; 2) new saturation value is
//          as close as possible to piece linear function:
// 		    (0,0) - (x1,y1) - (x2,y2) - (1,1)
// VB script syntax:
//  ColourSaturTrans HandlesArray, NumberOfHandles, X1, Y1, X2, Y2
// Parameters:
//      HandlesArray - array of valid image handles
//      NumberOfHandles - number of valid handles in the array
//      X1, Y1, X2, Y2 - see description above
// Returns:
//    	Nothing
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::ColorSaturTrans(VARIANT FAR* HandlesArray, short NumberOfHandles, double X1, double Y1, double X2, double Y2) 
{
    SAFEARRAY   *pSafe;
    long        i;
    long        lB, uB;
    long        *pData;
    VARIANT     V;

    // Get and check arrays are 1D
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(HandlesArray, &pSafe, 1), return);
    
    // Check the results aray dimension is >= SPCONTSHAPEINFODATALENGTH
    CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 1, NumberOfHandles), return);
    
    // Get the bounds of the array PCodes
    SafeArrayGetLBound(pSafe, 1, &lB);
    SafeArrayGetUBound(pSafe, 1, &uB);
    
    pData = new long[NumberOfHandles];
    
    if (pData)
    {
        // Load up our pData array for UTS call
        for (i = lB; i < uB; i++)
        {
            SafeArrayGetElement(pSafe, &i, &V);
            pData[i - lB] = V.lVal;
        }

        // Do the uts call
        if (IP().UTSMemRect()->ColorSaturTrans(pData, NumberOfHandles, X1, Y1, X2, Y2) == UTS_ERROR)
        {
            delete pData;
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
        }

        delete pData;
    }
}

////////////////////////////////////////////////////////////////////////////////////////
// RangeThreshold - thresholds over a range defined by LowThreshold, HighThreshold
//
// VB script syntax:
//      RangeThreshold ImageHandle, ImageMask, LowThreshold, HighThreshold
// Parameters:
//      ImageHandle is an 8 bit image to threshold
//      ImageMask is the returned thresholded binary image 
//      LowThreshold, HighThreshold are the threshold range to use
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::RangeThreshold(long ImageHandle, long ImageMask, short LowThreshold, short HighThreshold) 
{
    long LowMask, Ret = IDS_OPOK, BPP, Temp;
    BOOL RangeOk = TRUE;

    // Check for valid handles
    if (UTSVALID_HANDLE(ImageHandle) && UTSVALID_HANDLE(ImageMask))
    {
        // Get image BPP
        BPP = IP().UTSMemRect()->GetRectInfo(ImageHandle, &Temp, &Temp);
        if (BPP == EIGHT_BITS_PER_PIXEL && ((LowThreshold < 0) || (HighThreshold > 255)))
            RangeOk = FALSE;

        if (RangeOk)
        {
            // Create the mask for the thresholded result
            LowMask = IP().UTSMemRect()->CreateAsWithPix(ImageHandle, ONE_BIT_PER_PIXEL);
            
            // Check mask images are valid
            if (UTSVALID_HANDLE(LowMask))
            {
                if (IP().UTSMemRect()->Threshold(ImageHandle, LowMask, ">=", LowThreshold) < 0)
                    Ret = IDS_IMAGEOPERATIONFAILED;    // Couldn't perform the operation
                else
                    if (IP().UTSMemRect()->Threshold(ImageHandle, ImageMask, "<=", HighThreshold) < 0)
                        Ret = IDS_IMAGEOPERATIONFAILED;    // Couldn't perform the operation
                    else
                    {
						// JMB: Must AND masks if thresholds are equal!
                        if (LowThreshold <= HighThreshold)
                            IP().UTSMemRect()->Pixelwise(LowMask, "And", ImageMask);      // AND the results together
                        else
                            IP().UTSMemRect()->Pixelwise(LowMask, "Or", ImageMask);       // OR the results together
                    }
            }
            else
                Ret = IDS_IMAGEMEMALLOC;       // Mem allocation error
        }
        else
            Ret = IDS_INVALIDPARAMETERVALUE;
        
        // Free any temporary images
        IP().UTSMemRect()->FreeImage(LowMask);
    }
    else
        Ret = IDS_INVALIDHANDLE;           // A handle is invalid

    // Got an exception to throw - an error occurred
    if (Ret != IDS_OPOK)
        AfxThrowOleDispatchException(0xFF, Ret, 0);    // Couldn't perform the operation
}


////////////////////////////////////////////////////////////////////////////////////////
//
//	ImProc Keywords:
//
//	FFTRegisterImages
//	FFT
//	FFTPower
//	RegisterImages
//

////////////////////////////////////////////////////////////////////////////////////////
//
//	Register two images (FFT Method)
//
//	VB syntax:
//		Result = FFTRegisterImages(hImage1, hImage2, xshift, yshift, maxshift)
//
//	Parameters:
//		hImage1, hImage2	- Handles of images to register
//		xshift, yshift		- Returned registration shift
//		maxshift			- Max shift allowed
//
//	Returns:
//		True if iamges register, or False on error
//	Notes:
//		Images must be 8 bits per pixel
////////////////////////////////////////////////////////////////////////////////////////
BOOL UTSscript::FFTRegisterImages(long hImage1, long hImage2,
							   VARIANT FAR *xshift, VARIANT FAR *yshift, long maxshift)
{
	CString Msg;
//	// Debug
//	Msg.Format("%s\r\nContinue ?", "FFTRegisterImages");
//	if (AfxMessageBox(Msg, MB_YESNO | MB_ICONSTOP) == IDNO)
//		AfxThrowOleDispatchException(0xFF, IDS_BREAKSCRIPT, 0);

	// Check image handles
    if (UTSVALID_HANDLE(hImage1) && UTSVALID_HANDLE(hImage2)) {
		long bpp1, width1, height1, pitch1;
		long bpp2, width2, height2, pitch2;
		void *addr1, *addr2;

		// Get image info
        bpp1 = IP().UTSMemRect()->GetExtendedInfo(hImage1, &width1, &height1, &pitch1, &addr1);
        bpp2 = IP().UTSMemRect()->GetExtendedInfo(hImage2, &width2, &height2, &pitch2, &addr2);

		// Check 8 bits
		if ((bpp1 != EIGHT_BITS_PER_PIXEL) || (bpp2 != EIGHT_BITS_PER_PIXEL))
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETERVALUE, 0);   // Parameter not correct
		// Check images sizes are the same
		else if ((width1 != width2) || (height1 != height2))
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETERVALUE, 0);   // Parameter not correct
		else {

			CUTSImProc ip(IP().UTSMemRect());
			int xs, ys;

			ip.FFTRegisterImages(addr1, addr2,
							MIN(width1, width2), MIN(height1, height2),
							&xs, &ys, maxshift);

			// Return new image size
			xshift->lVal = xs;
			yshift->lVal = ys;
		}
	} else {
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);	// A handle is invalid
	}

	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////
//
//	Register two images (Difference method)
//
//	VB syntax:
//		Result = RegisterImages(hImage1, hImage2, minshift, maxshift, stripsize, orientation)
//
//	Parameters:
//		hImage1, hImage2	- Handles of images to register
//		minshift, maxshift	- Min and Max registartion shift expected (speed search)
//		stripsize			- Area to perform difference over (speed search)
//		orientation			- Position of image2 w.r.t. image1
//
//	Returns:
//		Registartion position, or -1 on error
//
//	Notes:
//		Images must be 8 bits per pixel
//
////////////////////////////////////////////////////////////////////////////////////////
long UTSscript::RegisterImages(long hImage1, long hImage2, long minshift, long maxshift, long stripsize, LPCTSTR orientation) 
{
	CString Msg;
	long reg_pos;

	// Default return
	reg_pos = -1;

	// Check image handles
    if (UTSVALID_HANDLE(hImage1) && UTSVALID_HANDLE(hImage2)) 
	{
		long bpp1, width1, height1, pitch1;
		long bpp2, width2, height2, pitch2;
		void *addr1, *addr2;

		// Get image info
        bpp1 = IP().UTSMemRect()->GetExtendedInfo(hImage1, &width1, &height1, &pitch1, &addr1);
        bpp2 = IP().UTSMemRect()->GetExtendedInfo(hImage2, &width2, &height2, &pitch2, &addr2);

		// Check 8 bits
		if ((bpp1 != EIGHT_BITS_PER_PIXEL) || (bpp2 != EIGHT_BITS_PER_PIXEL))
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETERVALUE, 0);   // Parameter not correct
		// Check images sizes are the same
		else if ((width1 != width2) || (height1 != height2))
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETERVALUE, 0);   // Parameter not correct
		else 
		{
			CUTSImProc ip(IP().UTSMemRect());

			reg_pos = ip.RegisterImages(addr1, addr2,
							MIN(width1, width2), MIN(height1, height2),
							minshift, maxshift,
							stripsize,
							orientation);

			// Return registration offset
		}
	} 
	else 
	{
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);	// A handle is invalid
	}

	return reg_pos;
}


BOOL UTSscript::FFTPower(long hImage, VARIANT FAR* new_width, VARIANT FAR* new_height) 
{
	CString Msg;

	// Check image handles
    if (UTSVALID_HANDLE(hImage)) {
		long bpp, width, height, pitch;
		void *addr;

		// Get image info
        bpp = IP().UTSMemRect()->GetExtendedInfo(hImage, &width, &height, &pitch, &addr);

		// Check 8 bits
		if (bpp != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETERVALUE, 0);   // Parameter not correct
		else {
			CUTSImProc ip(IP().UTSMemRect());
			int nw, nh;

			// Get fft power spectrum of image
			ip.FFTPower(addr, width, height, &nw, &nh);

			new_width->lVal = nw;
			new_height->lVal = nh;
		}
	} else {
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);	// A handle is invalid
	}

	return TRUE;
}

BOOL UTSscript::FFT(long hImage, VARIANT FAR* new_width, VARIANT FAR* new_height) 
{
	CString Msg;

//	Msg.Format("%s\r\nContinue ?", "FFT");
//	if (AfxMessageBox(Msg, MB_YESNO | MB_ICONSTOP) == IDNO)
//		AfxThrowOleDispatchException(0xFF, IDS_BREAKSCRIPT, 0);

	// Check image handles
    if (UTSVALID_HANDLE(hImage)) {
		long bpp, width, height, pitch;
		void *addr;

		// Get image info
        bpp = IP().UTSMemRect()->GetExtendedInfo(hImage, &width, &height, &pitch, &addr);

		// Check 8 bits
		if (bpp != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETERVALUE, 0);   // Parameter not correct
		else {
			CUTSImProc ip(IP().UTSMemRect());
			int nw, nh;

			/* Get fft of image */
			ip.FFT(addr, width, height, &nw, &nh);

			new_width->lVal = nw;
			new_height->lVal = nh;
		}
	} else {
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);	// A handle is invalid
	}

	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////
// Flip an image about an axis
// 
// VB syntax:
//		FlipVertical SrcImage   - flips about the central vertical axis
//		FlipHorizontal SrcImage - flips about the central horizontal axis
// Parameters:
//		The image to flip
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::FlipVertical(long SrcImage) 
{
    // Check for valid handles
    if (UTSVALID_HANDLE(SrcImage))
    {
		if (IP().UTSMemRect()->InvertColumns(SrcImage) < 0)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);          // A handle is invalid
}

void UTSscript::FlipHorizontal(long SrcImage) 
{
    // Check for valid handles
    if (UTSVALID_HANDLE(SrcImage))
    {
		if (IP().UTSMemRect()->InvertRows(SrcImage) < 0)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);          // A handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
//  Both hndlfrom, hndlto must be valid and be of the same dimension
// (defined as xext*yext ), but of different pixel sizes;
//  the function converts pixel data with transformation defined by
//  <lparam> value:
//  pixel sizes:
//  	from	to	<lparam> value
//*  	1		1		ignored
//*	1		8       0 => 0, 1 => <lparam> value
//*	1		32		0 => 0, 1 => <lparam> value
//*	8		1		pixels >= <lparam> value go to 1; 0 otherwise
//*	8		8		ignored
//*	8		32		left bit shift for BYTE to DWORD convertion
//*	32		1 		pixels >= <lparam> value go to 1; 0 otherwise
//*	32		8       right bit shift for DWORD to BYTE convertion
//					BUT: if ((lparam == -1) and (max pixel > 255))
//                    		then puts most significant bit of
//                        	the maximal pixel value into 0x80 bit
//        				 if (lparam == -2)
//            				then makes MulDiv to max=255
//*	32		32  	ignored
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::ImageConvert(long SrcImage, long DestImage, long LevelParam) 
{
    // Check for valid handles
    if (UTSVALID_HANDLE(SrcImage) && UTSVALID_HANDLE(DestImage))
    {
		if (IP().UTSMemRect()->Convert(SrcImage, DestImage, LevelParam) == UTS_ERROR)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);    // Couldn't perform the operation
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);          // A handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// Perform a convolution of any size
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::IPLConvolve(long SrcImage, long Xc, long Yc, VARIANT FAR* Coefficients, short RShift) 
{
    SAFEARRAY   *pSafe;
    long        i, j, idx[2];
    long        lB1, uB1, lB2, uB2, R1, R2;
    long        *pData;
    VARIANT     V;

    // Get and check coefficients are 2D
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Coefficients, &pSafe, 2), return);
        
    // Get the bounds of the array Coefficients
    SafeArrayGetLBound(pSafe, 1, &lB1);
    SafeArrayGetUBound(pSafe, 1, &uB1);
    SafeArrayGetLBound(pSafe, 2, &lB2);
    SafeArrayGetUBound(pSafe, 2, &uB2);
	R2 = uB2 - lB2;
	R1 = uB1 - lB1;

	// Check handle is valid
    if (UTSVALID_HANDLE(SrcImage))
    {
		// Unpack the coefficients array 
		pData = new long [R1 * R2];
		for (i = lB1; i < uB1; i++)
		{
			for (j = lB2; j < uB2; j++)
			{
                idx[0] = i;
                idx[1] = j;
                SafeArrayGetElement(pSafe, idx, &V);
				switch (V.vt)
				{
				case VT_I4:
				    pData[i * R2 + j] = V.lVal;
					break;
				case VT_I2:
					pData[i * R2 + j] = V.iVal;
                    break;
                }
			}
		}

		// Do the convolution
		IP().UTSMemRect()->IplConvolution(SrcImage, R1, R2, Xc, Yc, pData, RShift);

		// Clear up
		delete pData;
	}
	else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);          // A handle is invalid
}


////////////////////////////////////////////////////////////////////////////////////////
// Redimension an image
// 
// VB syntax:
//		ReDimension hImage, bpp, width, height
//
// Parameters:
//		hImage	-	the image to redimension
//		bpp		-	the new bits per pixel
//		width	-	the new width
//		height	-	the new height
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::ReDimension(long hImage, long bpp, long width, long height) 
{
	if (hImage < 1)
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
	else if (bpp != ONE_BIT_PER_PIXEL && 
	         bpp != EIGHT_BITS_PER_PIXEL  && 
	         bpp != TWENTYFOUR_BITS_PER_PIXEL &&
	         bpp != THIRTYTWO_BITS_PER_PIXEL)
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);	// BPP is invalid
    else
    {
	    if (IP().UTSMemRect()->ReDimension(hImage, bpp, width, height) < 0)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);
    }
}

////////////////////////////////////////////////////////////////////////////////////////
// Get a pixel value from an image given the X & Y coordinates
// 
// VB syntax:
//		value = GetPixel(hImage, x, y)
// Parameters:
//		hImage	-	the image
//		x, y	-	the co-ordinates
////////////////////////////////////////////////////////////////////////////////////////

long UTSscript::GetPixel(long hImage, long x, long y) 
{
	long value = 0;

    if (hImage < 1)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
    else
    {
		value = IP().UTSMemRect()->GetPixel(hImage, x, y);

		if (value < 0)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);
	}

	return value;
}

////////////////////////////////////////////////////////////////////////////////////////
// Set a pixel value in an image given the X & Y coordinates
// 
// VB syntax:
//		SetPixel hImage, x, y, value
// Parameters:
//		hImage	-	the image
//		x, y	-	the co-ordinates
//		value	-	the new pixel value
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::SetPixel(long hImage, long x, long y, long value) 
{
    if (hImage < 1)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
    else
    {
		if (IP().UTSMemRect()->PutPixel(hImage, x, y, value) < 0)
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEOPERATIONFAILED, 0);
	}
}

////////////////////////////////////////////////////////////////////////////////////////
// NumberUsedHandles
// VB syntax:
//		OpenHandles = NumberOfUsedHandles()
// Parameters:
//      None
// Returns:
//      Number of image handles currently open
////////////////////////////////////////////////////////////////////////////////////////

long UTSscript::NumberUsedHandles() 
{
	return IP().UTSMemRect()->NumberUsedHandles();
}

////////////////////////////////////////////////////////////////////////////////////////
//  Perform Watershed segmentation based on Euclidean Distance Map
//	The whole process goes through:
//	1. Euclidean Distance Map (EDM) generation
//	2. Ultimate Erosion Point (UEP) generation
//	3. Watershed (WS) segmentation 
//	One can choose to proceed to either step 1, 2, or 3 by passing in
//	the "operation" parameters as "EDM", "UEP", or "WS".
//	When choose to process to UEP or WS level, it is OPTIONAL to retrieve
//	the intermediate resulting EDM and UEP by passing in a valid image handle.
//
// VB Syntax:
//      WatershedByEDM hInput, hOutputEDM, hOutputUEP, hOutputWS, operation, isWhiteBackground
//
//	Parameters:
//	 hInput:			Input image (1 bpp binary image) handle
//	 hOutputEDM:		Output EDM image (8 bpp greyscale image) handle (required to be valid for EDM operation, optional for UEP and WS operation)
//	 hOutputUEP:		Output UEP image (8 bpp greyscale image) handle (required to be valid for UEP operation, optional for WS operation)
//	 hOutputWS:			Output WS image (1 bpp binary image) handle (always has white objects on black background)
//	 operation:			Choose among "EDM", "UEP" and "WS"
//	 isWhiteBackground:	Flag indicating if the input image with black objects on white background
// 	Returns:
// 	 -1		Operation failed
//	 1		Operation successful
////////////////////////////////////////////////////////////////////////////////////////

long UTSscript::WatershedByEDM(long hInput, long hOutputEDM, long hOutputUEP, long hOutputWS, LPCTSTR operation, bool isWhiteBackground)
{
	IJImageProc ijImageProc(IP().UTSMemRect());
	return ijImageProc.WatershedByEDM(hInput, hOutputEDM, hOutputUEP, hOutputWS, (char *)operation, isWhiteBackground);
}

////////////////////////////////////////////////////////////////////////////////////////
//  Perform automated thresholding 
//	Will return a lower bound and an upper bound that pixels in between will be of interests
//	Either the lower bound or the upper bound will be the mininum or maximum value in the image
//	While the other bound will be the auto threshold value
//	The process tries to find the single mode peak in the histogram and depending on (the distance
//	between the image min value to image mode value) or (the distance between the image max value
//	to image mode value), it will determine whether to use the image min value as the lower bound
//	or to use the image max value as the upper bound.
//	If the mode peak is closer to the max value, we will use the image min value as the lower bound
//	and use the threshold value as the upper bound
//	If the mode peak is closer to the min vlaue, we will use the image max value as the upper bound
//	and use the threshold value as the lower bound
//
// VB Syntax:
//      AutoThreshold hInput, minThreshold, maxThreshold
//
//	Parameters:
//	 hInput:			Input image (8 bpp greyscale image) handle
//	 minThreshold:		Lower bound of the threshold
//	 maxThreshold:		Upper bound of the threshold
// 	Returns:
//						The threshold value
////////////////////////////////////////////////////////////////////////////////////////
long UTSscript::AutoThreshold(long hInput, VARIANT FAR *minThreshold, VARIANT FAR *maxThreshold)
{
	IJImageProc ijImageProc(IP().UTSMemRect());
	long min = 0;
	long max = 255;
	long threshold = ijImageProc.AutoThreshold(hInput, min, max);
	V_VT(minThreshold) = VT_I4;
    V_VT(maxThreshold) = VT_I4;
    minThreshold->lVal = min;
    maxThreshold->lVal = max;
	return threshold;
}

////////////////////////////////////////////////////////////////////////////////////////
//  Perform rank filtering (min, max, median, mean) on a circular mask with user defined
//	radius value
//
// VB Syntax:
//      RankFilter hInput, hOutput, maskRadius, operation
//
//	Parameters:
//	 hInput:			Input image (8 bpp grey scale image) handle
//	 hOutput:			Output image (8 bpp grey scale image) handle
//	 radius:			The radius of the circular mask (can be 0.5, 1, 1.5, 2, 2.5, etc...)
//	 operation:			Choose among "MIN", "MAX", "MEDIAN", and "MEAN"
// 	Returns:
//	 Nothing
////////////////////////////////////////////////////////////////////////////////////////
void UTSscript::RankFilter(long hInput, long hOutput, double maskRadius, LPCTSTR operation)
{
	IJImageProc ijImageProc(IP().UTSMemRect());
	ijImageProc.RankFilter(hInput, hOutput, maskRadius, (char*)operation);
}

////////////////////////////////////////////////////////////////////////////////////////
//  Perform automated thresholding based on Mixture Modeling
//	Will return a threshold value that the caller can use "Threshold" routine
//
// VB Syntax:
//      AutoThreshold hInput, theThreshold
//
//	Parameters:
//	 hInput:			Input image (8 bpp greyscale image) handle
//	 theThreshold:		The threshold value
// 	Returns:
//	 Nothing
////////////////////////////////////////////////////////////////////////////////////////
void UTSscript::MixtureModelThreshold(long hInput, VARIANT FAR *theThreshold)
{
	IJImageProc ijImageProc(IP().UTSMemRect());
	long max = 255;
	ijImageProc.MixtureModelThreshold(hInput, max);
    V_VT(theThreshold) = VT_I4;
    theThreshold->lVal = max;
}

////////////////////////////////////////////////////////////////////////////////////////

long UTSscript::Ellipse(long SrcHandle, long xc, long yc, double a, double b, double phi, long color) 
{
	int ret = 0;
    // Image handles valid ?
    if (SrcHandle > 0)
    {
        ret = IP().UTSMemRect()->Ellipse(SrcHandle, xc, yc, a, b, phi, color);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);       // SrcImage handle is invalid

    return ret;
}




////////////////////////////////////////////////////////////////////////////////////////
// Convert an 8 bit image into a 1 bit mask FAST ImageConvert is tooo damn slow
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::Convert8To1(long Handle8, long Handle1)
{
    int Ret = IDS_OPOK;

    if (UTSVALID_HANDLE(Handle8) && UTSVALID_HANDLE(Handle1))
	{
        if (!IP().UTSMemRect()->Convert8To1(Handle8, Handle1))
            Ret = IDS_OPERATIONFAILED;
	}
    else
        Ret = IDS_INVALIDHANDLE;                        // SrcImage handle is invalid

    if (Ret != IDS_OPOK)
        AfxThrowOleDispatchException(0xFF, Ret, 0);         // Flag the error to the script
}

////////////////////////////////////////////////////////////////////////////////////////
// Convert a 1 bit mask to an 8 bit image FAST - ImageConvert is tooo damn slow
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::Convert1To8(long Handle1, long Handle8)
{
    int Ret = IDS_OPOK;

    if (UTSVALID_HANDLE(Handle8) && UTSVALID_HANDLE(Handle1))
    {
        if (!IP().UTSMemRect()->Convert1To8(Handle1, Handle8))
            Ret = IDS_OPERATIONFAILED;
	}
    else
        Ret = IDS_INVALIDHANDLE;                        // SrcImage handle is invalid

    if (Ret != IDS_OPOK)
        AfxThrowOleDispatchException(0xFF, Ret, 0);         // Flag the error to the script
}


////////////////////////////////////////////////////////////////////////////////////////
// Convert a 1 bit mask to an 8 bit image FAST - ImageConvert is tooo damn slow
// Specify the grey value representing 1 in the 1 bit image.
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::Convert1To8WithValue(long Handle1, long Handle8, BYTE value)
{
    int Ret = IDS_OPOK;

    if (UTSVALID_HANDLE(Handle8) && UTSVALID_HANDLE(Handle1))
    {
        if (!IP().UTSMemRect()->Convert1To8WithValue(Handle1, Handle8, value))
            Ret = IDS_OPERATIONFAILED;
	}
    else
        Ret = IDS_INVALIDHANDLE;                        // SrcImage handle is invalid

    if (Ret != IDS_OPOK)
        AfxThrowOleDispatchException(0xFF, Ret, 0);         // Flag the error to the script
}


////////////////////////////////////////////////////////////////////////////////////////
// Convert a 16 bpp image to an 8 bpp image - ImageConvert won't do this
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::Convert16To8(long hSrc, long hDst)
{
    int ret = IDS_OPOK;

    if (UTSVALID_HANDLE(hSrc) && UTSVALID_HANDLE(hDst))
	{
        if (!IP().UTSMemRect()->Convert16To8(hSrc, hDst))
            ret = IDS_OPERATIONFAILED;
	}
    else
        ret = IDS_INVALIDHANDLE;

    if (ret != IDS_OPOK)
        AfxThrowOleDispatchException(0xFF, ret, 0); // Flag the error to the script
}

// Do the same thing except scale between Min and Max
void UTSscript::Convert16To8Scaled(long hSrc, long hDst, long Min, long Max)
{
    int ret = IDS_OPOK;

    if (UTSVALID_HANDLE(hSrc) && UTSVALID_HANDLE(hDst))
	{
        if (!IP().UTSMemRect()->Convert16To8(hSrc, hDst, (WORD)Min, (WORD)Max))
            ret = IDS_OPERATIONFAILED;
	}
    else
        ret = IDS_INVALIDHANDLE;

    if (ret != IDS_OPOK)
        AfxThrowOleDispatchException(0xFF, ret, 0); // Flag the error to the script
}

////////////////////////////////////////////////////////////////////////////////////////
// Convert an 8 bpp image to a 16 bpp image - ImageConvert won't do this
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::Convert8To16(long hSrc, long hDst)
{
    int ret = IDS_OPOK;

    if (UTSVALID_HANDLE(hSrc) && UTSVALID_HANDLE(hDst))
	{
        if (!IP().UTSMemRect()->Convert8To16(hSrc, hDst))
            ret = IDS_OPERATIONFAILED;
	}
    else
        ret = IDS_INVALIDHANDLE;

    if (ret != IDS_OPOK)
        AfxThrowOleDispatchException(0xFF, ret, 0); // Flag the error to the script
}

////////////////////////////////////////////////////////////////////////////////////////
// Convert an 16 bpp image to an 8 bpp image using a histogram stretch.
//
// NOTE everything below the modal gray value in the 8 bit image is considered zero in 
// the resulting 8 bit image. This is the way the FindThread converts 16 bit images
// before passing them to scripts running in 8 bit mode.
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::HistogramStretch16To8( long hSrc, long hDst)
{
    int ret = IDS_OPOK;

    if (UTSVALID_HANDLE(hSrc) && UTSVALID_HANDLE(hDst))
	{
		long srcBPP, srcWidth, srcHeight, srcPitch;
		WORD * pSrcImage;
		srcBPP = IP().UTSMemRect()->GetExtendedInfo( hSrc, &srcWidth, &srcHeight, &srcPitch, &pSrcImage);

		long dstBPP, dstWidth, dstHeight, dstPitch;
		BYTE * pDstImage;
		dstBPP = IP().UTSMemRect()->GetExtendedInfo( hDst, &dstWidth, &dstHeight, &dstPitch, &pDstImage);

		if (srcBPP == SIXTEEN_BITS_PER_PIXEL && dstBPP == EIGHT_BITS_PER_PIXEL && srcWidth == dstWidth && srcHeight == dstHeight)
		{
			if (!ImageBits::HistogramStretchToNBPP( pSrcImage, srcWidth, srcHeight, srcBPP, pDstImage, dstBPP))
				ret = IDS_OPERATIONFAILED;
		}
		else
			ret = IDS_INVALIDPARAMETER;
	}
    else
        ret = IDS_INVALIDHANDLE;

    if (ret != IDS_OPOK)
        AfxThrowOleDispatchException(0xFF, ret, 0); // Flag the error to the script
}


////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::ContrastStretch(long hImage)
{
    int ret = IDS_OPOK;

    if (UTSVALID_HANDLE(hImage))
	{
        if (!IP().UTSMemRect()->ContrastStretch(hImage))
            ret = IDS_OPERATIONFAILED;
	}
    else
        ret = IDS_INVALIDHANDLE;

    if (ret != IDS_OPOK)
        AfxThrowOleDispatchException(0xFF, ret, 0); // Flag the error to the script
}


////////////////////////////////////////////////////////////////////////////////////////
// Create an image which is basically a product of differences from the mean
// Inputs: UTS handles Image1, Image2
//         Mean1 is the mean intensity of Image1 
//         Mean2 is the mean intensity of Image2
////////////////////////////////////////////////////////////////////////////////////////

long UTSscript::PDMImage(long Image1, short Mean1, long Image2, short Mean2)
{
    CColocalise CL(IP().UTSMemRect());
    long H;

    H = CL.PDMImage(Image1, (BYTE)Mean1, Image2, (BYTE)Mean2);

    if (H == UTS_ERROR)
        AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);         // Flag the error to the script

    return H;
}

////////////////////////////////////////////////////////////////////////////////////////
// Calculate overlap coefficients k1, k2 from images Image1, Image2
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::OverlapK1K2(long Image1, long Image2, VARIANT FAR *k1, VARIANT FAR *k2)
{
    CColocalise CL(IP().UTSMemRect());
    long H;
    double K1, K2;

    H = CL.OverlapK1K2(Image1, Image2, &K1, &K2);
    if (H == UTS_ERROR)
        AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);         // Flag the error to the script

    V_VT(k1) = VT_R8;
    k1->dblVal = K1;
    V_VT(k2) = VT_R8;
    k2->dblVal = K2;
}

////////////////////////////////////////////////////////////////////////////////////////
// Calculate manders overlap coefficients k1, k2 from images Image1, Image2
////////////////////////////////////////////////////////////////////////////////////////

void UTSscript::MandersK1K2(long Image1, long Image2, VARIANT FAR *k1, VARIANT FAR *k2)
{
    CColocalise CL(IP().UTSMemRect());
    long H;
    double K1, K2;

    H = CL.MandersK1K2(Image1, Image2, &K1, &K2);
    if (H == UTS_ERROR)
        AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);         // Flag the error to the script

    V_VT(k1) = VT_R8;
    k1->dblVal = K1;
    V_VT(k2) = VT_R8;
    k2->dblVal = K2;
}

////////////////////////////////////////////////////////////////////////////////////////
// Calc Pearsons overlap coefficient
// Image1 and Image2 are the intensity images for the signals
// RegionMask is the mask area - which must be 8 bits
// NOTE all pixels outside of the mask area must be zero
////////////////////////////////////////////////////////////////////////////////////////

afx_msg double UTSscript::Pearsons(long Image1, long Image2, long RegionMask)
{
    CColocalise CL(IP().UTSMemRect());
 
    return CL.Pearsons(Image1, Image2, RegionMask);
}


SAFEARRAY *UTSscript::GetSafeArray(VARIANT FAR *pSafe, int nDims, long *lB, long *uB)
{
    SAFEARRAY *pRet;

    // Get and check array is of correct dimensions
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(pSafe, &pRet, nDims), return NULL);

    // Get the bounds of the array Coefficients
    SafeArrayGetLBound(pRet, 1, lB);
    SafeArrayGetUBound(pRet, 1, uB);

    return pRet;
}

////////////////////////////////////////////////////////////////////////////////////////
// Perform colour deconvolution
//  Parameters: 
//      SrcImage is the source image
//      v1, v2, v3 are the 3 colour vectors specified as arrays of 3 doubles
//  Returns:
//      A handle to a new colour image where each component is the deconvolved result
////////////////////////////////////////////////////////////////////////////////////////

afx_msg long UTSscript::ColourDeconv(long SrcImage, VARIANT FAR *v1, VARIANT FAR *v2, VARIANT FAR *v3)
{
    SAFEARRAY *pSafe[3];
    long uB, lB;
    int Ret = IDS_OPOK;
    VARIANT V;
    long NewHandle = UTS_ERROR;
    long BPP, W, H;

    BPP = IP().UTSMemRect()->GetRectInfo(SrcImage, &W, &H);

    if (BPP != TWENTYFOUR_BITS_PER_PIXEL)
        Ret = IDS_INVALIDPARAMETER;
    else
    {
        pSafe[0] = GetSafeArray(v1, 1, &lB, &uB);
        // Check we have enough images for the blend
        if (uB - lB != 3)
            Ret = IDS_ARRAYDIMENSIONS;
        pSafe[1] = GetSafeArray(v2, 1, &lB, &uB);
        if (uB - lB != 3)
            Ret = IDS_ARRAYDIMENSIONS;
        pSafe[2] = GetSafeArray(v3, 1, &lB, &uB);
        if (uB - lB != 3)
            Ret = IDS_ARRAYDIMENSIONS;
    }

    if (UTSVALID_HANDLE(SrcImage) && Ret == IDS_OPOK)
    {
        ColourVector CV1, CV2, CV3;
        double d[3][3];

        for (int i = 0; i < 3 && Ret == IDS_OPOK; i++)
        {
            for (long j = 0; j < 3; j++)
            {
                // Get the handle of the input image
                SafeArrayGetElement(pSafe[i], &j, &V);

                d[i][j] = VariantDbl(V);
            }
        }

        CV1.R = d[0][0];
        CV1.G = d[0][1];
        CV1.B = d[0][2];

        CV2.R = d[1][0];
        CV2.G = d[1][1];
        CV2.B = d[1][2];

        CV3.R = d[2][0];
        CV3.G = d[2][1];
        CV3.B = d[2][2];

        NewHandle = ColourDeconvolve(*IP().UTSMemRect(), SrcImage, &CV1, &CV2, &CV3);
    }
    else
        Ret = IDS_INVALIDHANDLE;                        // SrcImage handle is invalid

    if (Ret != IDS_OPOK)
        AfxThrowOleDispatchException(0xFF, Ret, 0);         // Flag the error to the script

    return NewHandle;
}
    
afx_msg long UTSscript::MaxProjectionImage(VARIANT FAR *ImArr, long NImages)
{
    SAFEARRAY *pSafe;
    long uB, lB;
    long *pHandles;
    long i;
    VARIANT V;
    long H;
    CUTSImProc IP(IP().UTSMemRect());
    int N = 0;

    pSafe = GetSafeArray(ImArr, 1, &lB, &uB);

    pHandles = new long[uB - lB];

    for (i = 0; i < NImages; i++)
    {
        // Get the handle of the input image
        SafeArrayGetElement(pSafe, &i, &V);

        switch (V.vt)
        {
        case VT_I2:
            pHandles[N++] = V.iVal;
            break;
        case VT_I4:
            pHandles[N++] = V.lVal;
            break;
		}
	}

    H = IP.MaxProjectionImage(pHandles, N);
    delete [] pHandles;

    return H;
}


afx_msg long UTSscript::ProjectionImage(long IdxImage, VARIANT FAR *ImArr, long NImages)
{
    SAFEARRAY *pSafe;
    long uB, lB;
    long *pHandles;
    long i;
    VARIANT V;
    long H;
    CUTSImProc IP(IP().UTSMemRect());
    int N = 0;

    pSafe = GetSafeArray(ImArr, 1, &lB, &uB);

    pHandles = new long[uB - lB];

    for (i = 0; i < NImages; i++)
    {
        // Get the handle of the input image
        SafeArrayGetElement(pSafe, &i, &V);

        switch (V.vt)
        {
        case VT_I2:
            pHandles[N++] = V.iVal;
            break;
        case VT_I4:
            pHandles[N++] = V.lVal;
            break;
		}
    }

    H = IP.ProjectionImage(IdxImage, pHandles, N);
    delete [] pHandles;

    return H;
}

    
////////////////////////////////////////////////////////////////////////////////////////
// Make an image external to the UTS with Width, Height and BPP dimensions
// NOTE Calling this more than once will delete any previous image
// NOTE This external image only exists as long as the script processor exists
// Returns UTS_ERROR (-1) if no external image available
////////////////////////////////////////////////////////////////////////////////////////
    
afx_msg long UTSscript::MakeExternalImage(long Width, long Height, long BPP)
{
    int Idx = UTS_ERROR;

    for (int i = 0; i < MAX_EXTERNAL_IMAGES; i++)
        if (m_ExternalImages[i].W == 0 && m_ExternalImages[i].H == 0)
        {
            Idx = i;
            break;
        }

    if (Idx != UTS_ERROR)
    {
        // Delete any previous image
        if (m_ExternalImages[Idx].Image)
            delete m_ExternalImages[Idx].Image;

        m_ExternalImages[Idx].W = Width;
        m_ExternalImages[Idx].H = Height;
        m_ExternalImages[Idx].BPP = BPP;

        long Size = (m_ExternalImages[Idx].W  * m_ExternalImages[Idx].H * m_ExternalImages[Idx].BPP) >> 3;
        m_ExternalImages[Idx].Image = new BYTE[Size];
	    memset(m_ExternalImages[Idx].Image, 0, Size);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);   // Failed to alloc handle 


    return Idx;
}

////////////////////////////////////////////////////////////////////////////////////////
// Copy the contents of the external image into a newly created UTS one
////////////////////////////////////////////////////////////////////////////////////////

afx_msg long UTSscript::ExternalImageToUTS(long Idx)
{
    long UTSHandle = UTS_ERROR;

    if (Idx < 0 || Idx >= MAX_EXTERNAL_IMAGES)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);

    if (!m_ExternalImages[Idx].Image)
        AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);   // Failed to alloc handle 
    else
    {
        UTSHandle = IP().UTSMemRect()->Create(m_ExternalImages[Idx].BPP, m_ExternalImages[Idx].W, m_ExternalImages[Idx].H);

        if (UTSVALID_HANDLE(UTSHandle))
        {
            long X, Y, Pitch, BPP;
            BYTE *UTSAddr;
            BYTE *SrcAddr;
            long NBytesW;

            BPP = IP().UTSMemRect()->GetExtendedInfo(UTSHandle, &X, &Y, &Pitch, &UTSAddr);

            if (m_ExternalImages[Idx].W == X && m_ExternalImages[Idx].H == Y && BPP == m_ExternalImages[Idx].BPP)
            {
                NBytesW = (m_ExternalImages[Idx].W * BPP) >> 3;	            // Number of bytes in a row
                SrcAddr = m_ExternalImages[Idx].Image;	                    // Src address to copy from 

                for (long h = 0; h < Y; h++)
                {
                    memmove(UTSAddr, SrcAddr, NBytesW);
                    UTSAddr += Pitch;
                    SrcAddr += NBytesW;
                }
            }
            else
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);
        }
        else
            AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);   // Failed to alloc handle 
    }

    return UTSHandle;
}

////////////////////////////////////////////////////////////////////////////////////////
// Copy the contents of a UTS image into the external image
////////////////////////////////////////////////////////////////////////////////////////
    
afx_msg void UTSscript::UTSToExternalImage(long UTSHandle, long Idx)
{   
    if (Idx < 0 || Idx >= MAX_EXTERNAL_IMAGES)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);

    if (!m_ExternalImages[Idx].Image)
        AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);   // Failed to alloc handle 

    if (UTSVALID_HANDLE(UTSHandle))
    {
        long X, Y, Pitch, BPP, NBytesW;
        BYTE *UTSAddr, *DestAddr;

        BPP = IP().UTSMemRect()->GetExtendedInfo(UTSHandle, &X, &Y, &Pitch, &UTSAddr);

        if (m_ExternalImages[Idx].W == X && m_ExternalImages[Idx].H == Y && BPP == m_ExternalImages[Idx].BPP)
        {
            NBytesW = (m_ExternalImages[Idx].W * BPP) >> 3;	                // Number of bytes in a row
            DestAddr = m_ExternalImages[Idx].Image;	                        // Dest address to copy to 

            for (long i = 0; i < Y; i++)
            {
                memmove(DestAddr, UTSAddr, NBytesW);
                DestAddr += NBytesW;
                UTSAddr  += Pitch;
            }
        }
        else
            AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
}

#include "woolz.h"

////////////////////////////////////////////////////////////////////////////////////////
// DetectCover UTSHandle, YMin, YMax, Left, Right
// Detects coverslip edges in an image by doing a hough transform
// Parameters:
//  UTSHandle is the image
//  Ymin is the Y pixel position of the first line
//  Ymax is the Y pixel position of the second line
//  Left is the left hand edge
//  Right is the right hand edge
// Returns:
//  TRUE if successful
////////////////////////////////////////////////////////////////////////////////////////

afx_msg long UTSscript::DetectCover(long UTSHandle, VARIANT FAR *ymin, VARIANT FAR *ymax, VARIANT FAR *l, VARIANT FAR *r)
{
    BOOL Result = FALSE;

    if (UTSVALID_HANDLE(UTSHandle))
    {
        IP().WoolzLib()->UnpadImage(UTSHandle);

        long W, H, P;
        void *Addr;

        IP().UTSMemRect()->GetExtendedInfo(UTSHandle, &W, &H, &P, &Addr);

        int Top, Bottom, Left, Right;

        Result = DetectCoverslip((FSCHAR *)Addr, W, H, Bottom, Top, Left, Right);

        V_VT(ymin) = VT_I4;
        ymin->lVal = (long)Bottom;
        V_VT(ymax) = VT_I4;
        ymax->lVal = (long)Top;
		V_VT(l)    = VT_I4;
		l->lVal    = (long)Left;
		V_VT(r)	   = VT_I4;
		r->lVal    = (long)Right;

       IP().WoolzLib()->PadImage(UTSHandle);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

    return Result;
}

afx_msg long UTSscript::DetectCircularCover(long UTSHandle, long radius, VARIANT FAR *ymin, VARIANT FAR *ymax, VARIANT FAR *l, VARIANT FAR *r)
{
    struct object* coverslipObj1=NULL;
    struct object* coverslipObj2=NULL;
    int cx1, cy1, cx2, cy2, left, top, bottom, right;
    BOOL Result = FALSE;

    if (UTSVALID_HANDLE(UTSHandle))
    {
       IP().WoolzLib()->UnpadImage(UTSHandle);

        long W, H, P;
        void *Addr;

        IP().UTSMemRect()->GetExtendedInfo(UTSHandle, &W, &H, &P, &Addr);

        left = 0;
        right = W;
        top = H;
        bottom = 0;
        if (DetectCircularCoverslip((FSCHAR *)Addr, W, H, radius, coverslipObj1, coverslipObj2, cx1, cx2, cy1, cy2))
        {
            if (coverslipObj1 && coverslipObj2)
            {
                if (cx1 < cx2)
                {
                    left = cx1 - radius;
                    right = cx2 + radius;
                }
                else
                {
                    left = cx2 - radius;
                    right = cx1 + radius;
                }

                if (cy1 < cy2)
                {
                    bottom = cy1 - radius;
                    top = cy2 + radius;
                }
                else
                {
                    bottom = cy2 - radius;
                    top = cy1 + radius;
                }
            }

            if (!coverslipObj1 && coverslipObj2)
            {
                left = cx2 - radius;
                right = cx2 + radius;
                bottom = cy2 - radius;
                top = cy2 + radius;
            }

            if (coverslipObj1 && !coverslipObj2)
            {
                left = cx1 - radius;
                right = cx1 + radius;
                bottom = cy1 - radius;
                top = cy1 + radius;
            }

            if (left < 0) left = 0;
            if (right > W) right = W;
            if (bottom < 0) bottom = 0;
            if (top > H) top = H;

            Result = (coverslipObj1 || coverslipObj2);

            freeobj(coverslipObj1);
            freeobj(coverslipObj2);

            V_VT(ymin) = VT_I4;
            ymin->lVal = (long)bottom;
            V_VT(ymax) = VT_I4;
            ymax->lVal = (long)top;
		    V_VT(l)    = VT_I4;
		    l->lVal    = (long)left;
		    V_VT(r)	   = VT_I4;
		    r->lVal    = (long)right;

        }

       IP().WoolzLib()->PadImage(UTSHandle);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

    return Result;
}

////////////////////////////////////////////////////////////////////////////////////////
// Fractal Box Count
//    BoxCountResult = FractalBoxCount(Image, Iterations)
// Parameters:
//	  Image is the image to do the analysis on
//    Iterations is the number of iterations to perform the box count over > 2
// Returns:
//	  The box count result i.e. the least squares regression slope of the result
////////////////////////////////////////////////////////////////////////////////////////

afx_msg double UTSscript::FractalBoxCount(long Image, long Iterations)
{
	double R = 0.;
	CUTSImProc CIP(IP().UTSMemRect());

    if (UTSVALID_HANDLE(Image))
    {
		R = CIP.FractalBoxCount(Image, Iterations);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

	return R;
}

////////////////////////////////////////////////////////////////////////////////////////
// SRGSegment
//    Uses seeded region growing to segment an image. The seeds image supplies the
//	  initial object and background regions, where each region has its own unique
//    grey level.
// Usage:
//    SRGSegment image, seeds, segementation
// Parameters:
//	  image is the grey level image to segment
//    seeds is the grey level image containing "seed" regions
//    segmentation is the grey level image containing the segmented regions,
//			each in a unique grey level corresponding to its parent seed colour.
////////////////////////////////////////////////////////////////////////////////////////
afx_msg void UTSscript::SRGSegment( long image, long seeds, long segmentation)
{
	long result = 0;

    if (UTSVALID_HANDLE(image) && UTSVALID_HANDLE(seeds))
    {
		CSRGEngine srg = CSRGEngine(IP().UTSMemRect(), CSRGRegionInfoScalar::Create);
		srg.Segment( image, seeds, segmentation);
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid
}


////////////////////////////////////////////////////////////////////////////////////////
// Variance
//	  Computes an image of the local grey level variance over a radius.
// Usage:
//	  Variance src dst radius
// Parameters:
//	  src is the source grey level image
//	  dst is the destination grey level image
//	  radius is the radius over which to compute the local variance and
//			must be greater than 0.
////////////////////////////////////////////////////////////////////////////////////////
afx_msg void UTSscript::Variance( long src, long dst, BYTE radius)
{
    if (UTSVALID_HANDLE(src) && UTSVALID_HANDLE(dst))
    {
		if (!IP().UTSMemRect()->Variance( src, dst, radius))
		{
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);
		}
    }
    else
	{
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid
	}
}

////////////////////////////////////////////////////////////////////////////////////////
// BlankBorder
//	  Sets pixels within a border of the image edge to zero.
// Usage:
//	  BlankBorder src, stripWidth
// Parameters:
//	  src is the source 8 bit grey level or binary image
//	  blankWidth is the border width to set to zero.
////////////////////////////////////////////////////////////////////////////////////////
afx_msg void UTSscript::BlankBorder( long src, int blankWidth)
{
    if (UTSVALID_HANDLE(src))
    {
		if (!IP().UTSMemRect()->BlankBorder( src, blankWidth))
		{
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);
		}
    }
    else
	{
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid
	}
}

////////////////////////////////////////////////////////////////////////////////////////
// RemoveSkeletonTips
//	  Removes the tips of branches of a skeleton binary image.
// Usage:
//	  RemoveSkeletonTips src dst removePixelCount
// Parameters:
//	  src is the source 8 bit binary image
//    dst is the destination 8 bit binary image
//	  removePixelCount is the number if pixels to remove from tips
////////////////////////////////////////////////////////////////////////////////////////
afx_msg void UTSscript::TrimSkeletonTips( long src, long dst, int removePixelCount)
{
    if (UTSVALID_HANDLE(src))
    {
		if (!IP().UTSMemRect()->TrimTips( src, dst, removePixelCount))
		{
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);
		}
    }
    else
	{
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid
	}
}


////////////////////////////////////////////////////////////////////////////////////////
// LocalMaxima
//	  Computes a grey image containing the local maxima of the source image.
//	  The grey level corresponds to the number of points in the neighbourhood
//	  which are lower intensity at that point in the src image.
// Usage:
//	  LocalMaxima src dst radius
// Parameters:
//	  src is the source grey level image
//	  dst is the destination grey level image
//	  radius is the radius over which to compute the local maxima and
//			must be greater than 0.
////////////////////////////////////////////////////////////////////////////////////////
afx_msg void UTSscript::LocalMaxima( long src, long dst, BYTE radius)
{
    if (UTSVALID_HANDLE(src) && UTSVALID_HANDLE(dst))
    {
		if (!IP().UTSMemRect()->LocalMaxima( src, dst, radius))
		{
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETER, 0);
		}
    }
    else
	{
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid
	}
}


////////////////////////////////////////////////////////////////////////////////////////
// GetSeeds
//    Creates an 8 bit image with one seed point per object, which is the brightest 
//    point in the object in the source image. The mask defines the objects.
// Usage:
//	  seedImage = GetSeeds( sourceImage, objectMaskImage)
// Parameters:
//	  src is the source 8 bit grey level image
//	  objectMask is the binary image containing the object mask
// Return:
//	  An 8 bit grey level image containing one seed point per object in the mask.
//	  The seed points are indicated by grey level 255.
////////////////////////////////////////////////////////////////////////////////////////
afx_msg long UTSscript::GetSeeds( long src, long objectMask)
{
	long result = 0;

    if (UTSVALID_HANDLE(src) && UTSVALID_HANDLE(objectMask))
    {
		CSeedFinder seedfinder = CSeedFinder( IP().UTSMemRect(), IP().WoolzLib());
		result = seedfinder.Find( src, objectMask);
    }
    else
	{
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid
	}

	return result;
}


////////////////////////////////////////////////////////////////////////////////////////
// GetSeedsByLocalMaxima
//    Creates an 8 bit image with one or more seed points per object. Each point is a
//    local grey level maximum point in the object in the source image. The mask
//    defines the objects.
// Usage:
//	  seedImage = GetSeeds( sourceImage, objectMaskImage)
// Parameters:
//	  src is the source 8 bit grey level image
//	  objectMask is the binary image containing the object mask
// Return:
//	  An 8 bit grey level image containing one or more seed points per object in the mask.
//	  The seed points are indicated by grey level 255.
////////////////////////////////////////////////////////////////////////////////////////
afx_msg long UTSscript::GetSeedsByLocalMaxima( long src, long objectMask)
{
	long result = 0;

    if (UTSVALID_HANDLE(src) && UTSVALID_HANDLE(objectMask))
    {
		CSeedFinder seedfinder = CSeedFinder( IP().UTSMemRect(), IP().WoolzLib());
		result = seedfinder.FindByLocalmaxima( src, objectMask);
    }
    else
	{
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid
	}

	return result;
}
