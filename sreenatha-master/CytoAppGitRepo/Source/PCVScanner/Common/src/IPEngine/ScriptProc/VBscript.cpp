/////////////////////////////////////////////////////////////////////////////
// VBscript.cpp : implementation file
//
// This OLE automation class is exposed to the script engine interface.
// Implementation of new keywords is done here. 
//
// Written By Karl Ratcliff 01042001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
// Implementation of new keywords is simply done using the Classwizard (to
// be recommended). This is done by selecting the Automation tab and then
// selecting the class name: VBscript. Add new 

#include "stdafx.h"

#include "CommonScript.h"
#include "CSyncObjects.h"
#include "scriptproc.h"
#include "scriptutils.h"
#include "piccmd.h"
#include "vcmemrect.h"
#include "vcblob.h"
#include "vcgeneric.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "woolzif.h"
#include "vcthresh.h"
#include "vcImProc.h"
#include "uts.h"
#include "commondefs.h"
#include "cparameterio.h"
#include "picman.h"
#include "scriptpersist.h"
#include "resource.h"
#include "ClassMonitorDlg.h"
#include "VBscript.h"
#include "woolzif.h"
#include "woolz.h"
#include "CMeasurement.h"

#include "aiishared.h"
#include <procapi.h>
#include <math.h>
#include <io.h>
#include <stdlib.h>
#include <hrtimer.h>

#include "..\..\NLog\src\NLogC\NLogC.h"
#include "..\include\vbscript.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// The VBscript Class
/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(VBscript, CCmdTarget)

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
VBscript::VBscript()
{
	EnableAutomation();
    // By default work in interactive mode
    m_ExecutionMode = modeDebug;
    // No parameter file specified
    m_parameterFileName = "";
    // Create our persistent vars
    m_pPersistentVariables = new VarMapBank;
    // Load the defaults
    m_pPersistentVariables->Load(m_PersistentFileName);

	// Utility arrays
	for (int i = 0; i < MAX_UTILITY_ARRAYS; i++)
		m_pUtilityArrays[i] = new VarMapBank;

    // Create the pic manager
    m_pPicMan = NULL;
    // Create a new array list
    m_pArrays = new CArrayList;
	// Progress control display
	m_pProgressCtrl = NULL;
	// Delete the progress bar control
	m_bHasProgress = TRUE;
	// No parent window
	m_pGUIParent = NULL;
    // By default the script processors always commits persistents to disk on exit
    m_bSavePersistents = TRUE;

    // Create a measurements list
    m_pMeasurements = new CMeasures;

    // Profiling
    m_pHRT = NULL;

	m_Id = 0;

	CString MutName;

	MutName.Format(_T("%8x_SCRIPTLOCK_MUTEX"), GetCurrentProcessId());
    m_pLockMutex = new CSyncObjectMutex(SyncObjectCreate, MutName);

	NLog_Init(_T("SCRIPT"));
}

VBscript::~VBscript()
{
    // Dump current persistent variables to disk
    if (m_bSavePersistents)
        m_pPersistentVariables->Save(m_PersistentFileName);

    // Clear up
    if (m_pPersistentVariables)
        delete m_pPersistentVariables;
    if (m_pPicMan)
        delete m_pPicMan;
    if (m_pArrays)
        delete m_pArrays;
	if (m_bHasProgress)
	{
		if (m_pProgressCtrl)
			delete m_pProgressCtrl;
	}

	m_pLockMutex->Release();
	if (m_pLockMutex)
		delete m_pLockMutex;

    if (m_pMeasurements)
        delete m_pMeasurements;

	DeleteClassMonitor();

	for (int i = 0; i < MAX_UTILITY_ARRAYS; i++)
		delete m_pUtilityArrays[i];

    if (m_pHRT)
        delete m_pHRT;

	m_ScriptIO.Reset();
}	


void VBscript::Init(CIPObjects *pPIP)
{
	Initialise(pPIP);
	    // Create the pic manager
    m_pPicMan = new CPicManager(pPIP->UTSMemRect());
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// NOTE implement class members not exposed to the scripting engine here
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// Reset the queues and anything else that needs it
/////////////////////////////////////////////////////////////////////////////

void VBscript::Reset(void)
{
    // Reset the Q's
    m_ScriptIO.Reset();
    // No parameter file specified
    m_parameterFileName = "";
	// Free any image handles
    FreeAll();
}

void VBscript::CommitPersistents(void)
{
    // Dump current persistent variables to disk
    m_pPersistentVariables->Save(m_PersistentFileName);
}

void VBscript::LoadPersistents(LPCTSTR FileName)
{
	m_PersistentFileName = FileName;
    // Load the defaults
    m_pPersistentVariables->Load(m_PersistentFileName);
}

void VBscript::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.
	CCmdTarget::OnFinalRelease();
}

BOOL VBscript::InitialiseScriptQs(void)
{
    // Initialise the I/O queues swapped from the way they are
    // in the ScriptProcessor class. In this way, the Get and 
    // Set member functions will access the correct name queue
    // depending on the direction of the data flow.
    return m_ScriptIO.InitialiseScriptQs(_T(""), VB_FROMSCRIPT_Q, VB_TOSCRIPT_Q);
}

BOOL VBscript::InitialiseScriptQs(LPCTSTR QPath, LPCTSTR Qin, LPCTSTR Qout)
{
    // Initialise the I/O queues swapped from the way they are
    // in the ScriptProcessor class. In this way, the Get and 
    // Set member functions will access the correct name queue
    // depending on the direction of the data flow.
    return m_ScriptIO.InitialiseScriptQs(QPath, Qin, Qout);
}

// Resets ALL the persistent variables currently in use
void VBscript::ResetPersistentVars(void)
{
    m_pPersistentVariables->ResetBank();
}


// Set the output window for image output
void VBscript::SetProgressWindow(CWnd *pWnd, LPRECT pRect)
{
	// Create a progress control to put in the window
	if (pWnd && m_bHasProgress)
	{
		if (m_pProgressCtrl)
		{
			m_pProgressCtrl->ShowWindow(FALSE);
			delete m_pProgressCtrl;
		}
		
		m_pProgressCtrl = new CProgressCtrl;
		// Now give a rectangle to work with
		m_pProgressCtrl->Create(WS_CHILD | WS_VISIBLE | PBS_SMOOTH, 
			*pRect,
			pWnd, IDC_SCRIPTPROGSCROLL);
	}
}

// Set the output window for image output
void VBscript::SetProgressWindow(CWnd *pWnd)
{
	if (pWnd && m_bHasProgress)
	{
		CRect rcWnd;
		
		pWnd->GetWindowRect(&rcWnd);
		
		// Create a progress control to put in the window
		if (m_pProgressCtrl)
		{
			m_pProgressCtrl->ShowWindow(FALSE);
			delete m_pProgressCtrl;
		}
		
		m_pProgressCtrl = new CProgressCtrl;
		
		// Now give a rectangle to work with
		m_pProgressCtrl->Create(WS_CHILD | WS_VISIBLE | PBS_SMOOTH, 
			rcWnd,
			pWnd, 1);
	}
}

// Set the window up to display script progress information in
void VBscript::SetProgressWindowFromHandle(HWND hWnd, LPRECT pRect)
{
	// Create a progress control to put in the window
	if (hWnd && m_bHasProgress)
	{
		if (m_pProgressCtrl)
		{
			m_pProgressCtrl->ShowWindow(FALSE);
			delete m_pProgressCtrl;
		}
		
		m_pProgressCtrl = new CProgressCtrl;
 		// Now give a rectangle to work with
		m_pProgressCtrl->Create(WS_CHILD | WS_VISIBLE | PBS_SMOOTH, 
								*pRect,
								CWnd::FromHandle(hWnd), 
								IDC_SCRIPTPROGSCROLL);
	}
}

void VBscript::SetProgressBarCtrl(CProgressCtrl *pProg)
{
	m_pProgressCtrl = pProg;
	m_bHasProgress = FALSE;	// Control is owned by application not the script processor
}

/////////////////////////////////////////////////////////////////////////////
//
// Class wizard generated from here on in
//
/////////////////////////////////////////////////////////////////////////////


BEGIN_MESSAGE_MAP(VBscript, CCmdTarget)
	//{{AFX_MSG_MAP(VBscript)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(VBscript, CCmdTarget)
	//{{AFX_DISPATCH_MAP(VBscript)
	DISP_PROPERTY_NOTIFY(VBscript, "ParameterFileName", m_parameterFileName, OnParameterFileNameChanged, VT_BSTR)
	DISP_PROPERTY_EX(VBscript, "ScriptExecMode", GetScriptExecMode, SetScriptExecMode, VT_I2)
	DISP_FUNCTION(VBscript, "FreeAll", FreeAll, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(VBscript, "ShowHistogram", ShowHistogram, VT_EMPTY, VTS_BSTR VTS_I4)
	DISP_FUNCTION(VBscript, "ShowImageWindow", ShowImageWindow, VT_EMPTY, VTS_BSTR VTS_I4)
	DISP_FUNCTION(VBscript, "ShowString", ShowString, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(VBscript, "HelpAbout", HelpAbout, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(VBscript, "GetLong", GetLong, VT_BOOL, VTS_PVARIANT)
	DISP_FUNCTION(VBscript, "PutLong", PutLong, VT_BOOL, VTS_I4)
	DISP_FUNCTION(VBscript, "GetParameter", GetParameter, VT_I4, VTS_BSTR VTS_I4)
	DISP_FUNCTION(VBscript, "SetParameter", SetParameter, VT_EMPTY, VTS_BSTR VTS_I4)
	DISP_FUNCTION(VBscript, "PutImage", PutImage, VT_BOOL, VTS_I4)
	DISP_FUNCTION(VBscript, "GetImage", GetImage, VT_BOOL, VTS_PVARIANT)
    DISP_FUNCTION(VBscript, "AddMeasurement", PutMeasurement, VT_EMPTY, VTS_BSTR VTS_R8 VTS_R8 VTS_BOOL VTS_BOOL VTS_BOOL)
	DISP_FUNCTION(VBscript, "Break", Break, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(VBscript, "GetDouble", GetDouble, VT_BOOL, VTS_PVARIANT)
	DISP_FUNCTION(VBscript, "PutDouble", PutDouble, VT_BOOL, VTS_R8)
	DISP_FUNCTION(VBscript, "GetExecutionMode", GetExecutionMode, VT_I2, VTS_NONE)
	DISP_FUNCTION(VBscript, "GetDoubleParameter", GetDoubleParameter, VT_R8, VTS_BSTR VTS_R8)
	DISP_FUNCTION(VBscript, "GetString", GetString, VT_BOOL, VTS_PVARIANT)
	DISP_FUNCTION(VBscript, "PutString", PutString, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(VBscript, "RectIntersect", RectIntersect, VT_BOOL, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(VBscript, "InitLongPersistent", InitLongPersistent, VT_EMPTY, VTS_BSTR VTS_I4)
	DISP_FUNCTION(VBscript, "InitDoublePersistent", InitDoublePersistent, VT_EMPTY, VTS_BSTR VTS_R8)
	DISP_FUNCTION(VBscript, "InitStringPersistent", InitStringPersistent, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(VBscript, "SetPersistent", SetPersistent, VT_EMPTY, VTS_BSTR VTS_PVARIANT)
	DISP_FUNCTION(VBscript, "GetPersistent", GetPersistent, VT_EMPTY, VTS_BSTR VTS_PVARIANT)
	DISP_FUNCTION(VBscript, "DeletePersistent", DeletePersistent, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(VBscript, "DebugString", DebugString, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(VBscript, "CloseImageWindow", CloseImageWindow, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(VBscript, "GetRGB", GetRGB, VT_EMPTY, VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(VBscript, "Atan2", Atan2, VT_R8, VTS_R8 VTS_R8)
	DISP_FUNCTION(VBscript, "CreateArray", CreateArray, VT_I4, VTS_I4)
	DISP_FUNCTION(VBscript, "SortArray", SortArray, VT_BOOL, VTS_I4 VTS_I4)
	DISP_FUNCTION(VBscript, "ArraySetAt", ArraySetAt, VT_BOOL, VTS_I4 VTS_I4 VTS_I4 VTS_R8)
	DISP_FUNCTION(VBscript, "ArrayGetAt", ArrayGetAt, VT_BOOL, VTS_I4 VTS_I4 VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(VBscript, "ArrayUBound", ArrayUBound, VT_I4, VTS_I4)
	DISP_FUNCTION(VBscript, "DeleteArray", DeleteArray, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(VBscript, "CreateArray2D", CreateArray2D, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(VBscript, "ProgressSetRange", ProgressSetRange, VT_EMPTY, VTS_I2 VTS_I2)
	DISP_FUNCTION(VBscript, "ProgressSetPos", ProgressSetPos, VT_EMPTY, VTS_I2)
	DISP_FUNCTION(VBscript, "ProgressShowWindow", ProgressShowWindow, VT_EMPTY, VTS_BOOL)
	DISP_FUNCTION(VBscript, "ProgressStep", ProgressStep, VT_EMPTY, VTS_I2)
	DISP_FUNCTION(VBscript, "ProgressSetStep", ProgressSetStep, VT_EMPTY, VTS_I2)
	DISP_FUNCTION(VBscript, "ProgressStepIt", ProgressStepIt, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(VBscript, "ScriptSleep", ScriptSleep, VT_EMPTY, VTS_I4)
    DISP_FUNCTION(VBscript, "SetRegItem", SetRegItem, VT_EMPTY, VTS_BSTR VTS_BSTR VTS_BSTR)
    DISP_FUNCTION(VBscript, "GetRegItem", GetRegItem, VT_BOOL, VTS_BSTR VTS_BSTR VTS_PVARIANT)
    DISP_FUNCTION(VBscript, "GetProcessID", GetProcessID, VT_I4, VTS_BSTR)
    DISP_FUNCTION(VBscript, "GetCurrentProcessID", GetCurrentProcessID, VT_I4, VTS_NONE)
    DISP_FUNCTION(VBscript, "GetAITIFF", GetAITIFF, VT_I4, VTS_NONE)
	DISP_FUNCTION(VBscript, "BlobFileName", BlobFileName, VT_BOOL, VTS_PVARIANT)
	DISP_FUNCTION(VBscript, "GUIDString", GUIDString, VT_EMPTY, VTS_PVARIANT)
	DISP_FUNCTION(VBscript, "CreateClassMonitor", CreateClassMonitor, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(VBscript, "DeleteClassMonitor", DeleteClassMonitor, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(VBscript, "StartTime", StartTime, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(VBscript, "GetTime", GetTime, VT_R8, VTS_NONE)
	DISP_FUNCTION(VBscript, "GetInstanceId", GetInstanceId, VT_I4, VTS_NONE)
	DISP_FUNCTION(VBscript, "LockSection", LockSection, VT_I4, VTS_I4)
	DISP_FUNCTION(VBscript, "ReleaseSection", ReleaseSection, VT_I4, VTS_NONE)
	DISP_FUNCTION(VBscript, "FormatNum", FormatNum, VT_EMPTY, VTS_BSTR VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(VBscript, "SetUVal", SetUtilityArrayValue, VT_EMPTY, VTS_I4 VTS_BSTR VTS_PVARIANT)
	DISP_FUNCTION(VBscript, "GetUVal", GetUtilityArrayValue, VT_BOOL, VTS_I4 VTS_BSTR VTS_PVARIANT)
	DISP_FUNCTION(VBscript, "GetUValSize", GetUtilityArraySize, VT_I4, VTS_I4)
	DISP_FUNCTION(VBscript, "GetUValAt", GetUtilityArrayValueAt, VT_BOOL, VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT)
//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IVBscript to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {982A6DA0-1EBC-11D5-83BF-00B0D0633F72}
static const IID IID_IVBscript =
{ 0x982a6da0, 0x1ebc, 0x11d5, { 0x83, 0xbf, 0x0, 0xb0, 0xd0, 0x63, 0x3f, 0x72 } };

BEGIN_INTERFACE_MAP(VBscript, CCmdTarget)
	INTERFACE_PART(VBscript, IID_IVBscript, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// VBscript message handlers
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


void VBscript::HelpAbout() 
{
    // Acknowledgement to Microsoft go in here ?
    AfxMessageBox(_T("Applied Imaging Image Script Processor"), 0x10000);
}

/////////////////////////////////////////////////////////////////////////////
// Free deletes all memory rectangles
//
// VB script syntax
//      FreeAll
// Parameters:
//      None
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void VBscript::FreeAll() 
{
    // Result all images with open windows
    m_pPicMan->DeleteAll();
    // Result all UTS image handles
    IP().UTSMemRect()->FreeAll();
}

/////////////////////////////////////////////////////////////////////////////
// Shows an image within a window
// 
// VB script syntax:
//      ShowImageWindow Title, ImageHandle
// Parameters:
//      Title is the title to stick in the window
//      ImageHandle is the handle of the image we want to display
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void VBscript::ShowImageWindow(LPCTSTR Title, long ImageHandle) 
{
    if (m_ExecutionMode == modeDebug)
    {
	    if (ImageHandle > 0)
            m_pPicMan->AddNew(ImageHandle, Title, CPicWindow::wndImage);
        else
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
    }
}

/////////////////////////////////////////////////////////////////////////////
// Closes an image window created by the above
// VB syntax:
//      CloseImageWindow SrcImage
// Parameters:
//      SrcImage is the image to close   
/////////////////////////////////////////////////////////////////////////////

void VBscript::CloseImageWindow(long SrcImage) 
{
    if (m_ExecutionMode == modeDebug)
    {
	    if (SrcImage > 0)
            m_pPicMan->Delete(SrcImage);
        else
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
    }
}


/////////////////////////////////////////////////////////////////////////////
// Shows a histogram created with the above 
// 
// VB script syntax:
//      ShowHistogram HistogramHandle, HistogramTitle
// Parameters:
//      HistogramHandle is the identifier for the image we wish to histogram
//      HistogramTitle is the title we want to put in the histogram window
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void VBscript::ShowHistogram(LPCTSTR Name, long Handle) 
{
    if (m_ExecutionMode == modeDebug)
    {
        if (Handle > 0)
            m_pPicMan->AddNew(Handle, Name, CPicWindow::wndHistogram);
        else
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
    }
}

/////////////////////////////////////////////////////////////////////////////
// Outputs a string in a message box in debug mode else outputs it to a 
// debug string
// VB script syntax:
//      ShowString MessageString
// Parameters:
//      MessageString   - Prompt string
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void VBscript::ShowString(LPCTSTR Message) 
{
    if (m_ExecutionMode == modeDebug)
    {   
        CString Msg;

        Msg.Format(_T("%s\r\nContinue ?"), Message);
        if (AfxMessageBox(Msg, MB_YESNO | MB_ICONSTOP) == IDNO)
                AfxThrowOleDispatchException(0xFF, IDS_BREAKSCRIPT, 0);
    }
    else
    {
        NLog_Trace(_T("SCRIPT"), _T("%d: %s"), GetInstanceId(), Message);
    }
}

/////////////////////////////////////////////////////////////////////////////
// This method is called whenever the parameter filename changes
// VB script syntax:
//      ParameterFileName = "\Images\ParamFile"
//
/////////////////////////////////////////////////////////////////////////////

void VBscript::OnParameterFileNameChanged() 
{
}

/////////////////////////////////////////////////////////////////////////////
// Gets a long from the input Q
//
// VB script syntax:
//      Result = GetLong(L)
// Parameters:
//      L is the long to get
// Returns:
//      True if successful, False otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL VBscript::GetLong(VARIANT FAR* L) 
{
    // Get a long from the input Q to the script
    BOOL Ok = m_ScriptIO.GetLongFrom(&L->lVal);

    // Set the type for the variant
    V_VT(L) = VT_I4;

	return Ok;
}

/////////////////////////////////////////////////////////////////////////////
// Puts a long on the output Q
//
// VB script syntax:
//      Result = PutLong(L)
// Parameters:
//      L is the long to put
// Returns:
//      True if successful, False otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL VBscript::PutLong(long L) 
{
	return m_ScriptIO.SendLongTo(L);
}

/////////////////////////////////////////////////////////////////////////////
// Gets a double from the input Q
//
// VB script syntax:
//      Result = GetDouble(D)
// Parameters:
//      D is the double to get
// Returns:
//      True if successful, False otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL VBscript::GetDouble(VARIANT FAR* D) 
{
    // Get a double from the input Q to the script
    BOOL Ok = m_ScriptIO.GetDoubleFrom(&D->dblVal);

    // Set the type for the variant
    V_VT(D) = VT_R8;

	return Ok;
}

/////////////////////////////////////////////////////////////////////////////
// Puts a double on the output Q
//
// VB script syntax:
//      Result = PutDouble(D)
// Parameters:
//      D is the double to put
// Returns:
//      True if successful, False otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL VBscript::PutDouble(double D) 
{
	return m_ScriptIO.SendDoubleTo(D);
}

/////////////////////////////////////////////////////////////////////////////
// Puts an image record on the output Q
//
// VB script syntax:
//      Result = PutImage(H)
// Parameters:
//      H is the handle of the image to put (a UTS handle)
// Returns:
//      True if successful, False otherwise
// NOTES:
//      It is upto the host application to Free off images placed on the
//      output queue as this will allocate new memory for the image. This
//      is to prevent the script erasing an image before the host application
//      has a chance to get it off the output Q.
/////////////////////////////////////////////////////////////////////////////

BOOL VBscript::PutImage(long ImageHandle) 
{
	long            W, H, BPP, P;
    void            *Addr, *ImageAddr;
    VBS_IMAGE_REC   ImageData;
    BOOL            Ok = FALSE;

    if (ImageHandle < 1)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);
    else
    {
        // Get the image details for UTS
        BPP = IP().UTSMemRect()->GetExtendedInfo(ImageHandle, &W, &H, &P, &Addr);

        // Create some memory for it
        ImageAddr = malloc(H * P);
        // Get the image and put into this new memory
        if (ImageAddr)
        {
            // Copy the image into new memory
            memcpy(ImageAddr, Addr, P * H);
            // Fill in the record
            ImageData.Address = ImageAddr;       // Address 
            ImageData.BitsPerPixel = (BYTE) BPP;
            ImageData.Width  = W;
            ImageData.Height = H;

            // Send the record
	        Ok = m_ScriptIO.SendImageTo(&ImageData);
        }
    }

    return Ok;
}

/////////////////////////////////////////////////////////////////////////////
// Gets an image from the input Q
//
// VB script syntax:
//      Result = GetImage(H)
// Parameters:
//      H is the handle of the image got from the Q (H is a UTS handle)
// Returns:
//      True if successful, False otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL VBscript::GetImage(VARIANT FAR* ImageHandle) 
{
    VBS_IMAGE_REC   ImageData;

    // Get the image rec from the input Q to the script
    BOOL Ok = m_ScriptIO.GetImageFrom(&ImageData);

    // If Ok then try and create a rectangle for it
    if (Ok)
    {
        // Allocate some memory
        ImageHandle->lVal = IP().UTSMemRect()->Create(ImageData.BitsPerPixel, ImageData.Width, ImageData.Height);
        //ImageHandle->lVal = UTSMemRect.MakeExtern((void *)ImageData.Address, 8, ImageData.Width, ImageData.Height, ImageData.Width);
        V_VT(ImageHandle) = VT_I4;

        // If the memory is allocated then copy the image into mem
        if (ImageHandle->lVal > 0)
            IP().UTSMemRect()->SetBits((BYTE *) ImageData.Address, ImageHandle->lVal);
        else
            Ok = FALSE;
    }

	return Ok;
}

/////////////////////////////////////////////////////////////////////////////
// Gets a string from the input Q
//
// VB script syntax:
//      Result = GetString(S)
// Parameters:
//      S is the string to get
// Returns:
//      True if successful, False otherwise
/////////////////////////////////////////////////////////////////////////////


BOOL VBscript::GetString(VARIANT FAR* QString) 
{
    CString String;
    BOOL    Ok = FALSE;
    
    // Check we have a BSTR/EMPTY coming in
    if (QString->vt == VT_BSTR || QString->vt == VT_EMPTY)
    {
        Ok = m_ScriptIO.GetStringFrom(String);
    
        if (Ok) 
        {
            // Free up the existing string if there was one
            if (QString->vt == VT_BSTR) SysFreeString(QString->bstrVal);
            // Change the type to a BSTR
            V_VT(QString) = VT_BSTR;
            // Allocate the string and return to VBS
            QString->bstrVal = String.AllocSysString();
        }
    }

	return Ok;
}

/////////////////////////////////////////////////////////////////////////////
// Puts a string on the output Q and send to the hosting application
//
// VB script syntax:
//      Result = PutString(S)
// Parameters:
//      S is the string to put
// Returns:
//      True if successful, False otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL VBscript::PutString(LPCTSTR QString) 
{
	return m_ScriptIO.SendStringTo(QString);
}

/////////////////////////////////////////////////////////////////////////////
// Puts a generic measurement on the measurements Q and 
// send to the hosting application
//
// VB script syntax:
//      AddMeasurement Name, Value, NF, IncludeInClassifier, IncludeInGrid, IncludeInSort
// Parameters:
//      ParamName is the measurement name
//      Value is the value to add
//      NF is a normalisation factor for the measurement (if applicable)
//		IncludeInClassifier is true if we want to use the measurement in a classifier
//		IncludeInGrid is true if we wish to display the measurement
//		InlcudeInSort is true if the measurement can be used for sorting
// Returns:
//      Nothing
// NOTE:
//      This routine allocates a measurement class for each measurement
//      which is destroyed when the VBScript object is deleted.
/////////////////////////////////////////////////////////////////////////////

void VBscript::PutMeasurement(LPCTSTR ParamName, double Value, double NF, BOOL IncludeInClassifier, BOOL IncludeInGrid, BOOL IncludeInSort)
{    
    m_pMeasurements->Add(ParamName, Value, NF, IncludeInClassifier, IncludeInGrid, IncludeInSort);
}

/////////////////////////////////////////////////////////////////////////////
// Return a measurement called from the hosting side
/////////////////////////////////////////////////////////////////////////////

BOOL VBscript::GetMeasurement(Measurement **pMeasure)
{
    return ((*pMeasure = m_pMeasurements->Get()) != NULL);
}

/////////////////////////////////////////////////////////////////////////////
// Get a parameter from file - if it does not exist in either the
// the .DEF or .USR file, then use the coded default value
//
// VB script syntax:
//      P = GetParameter(ParamFileName, ParamName, Default)
// Parameters:
//      ParamName is the name of the parameter
//      Default is the default value 
// Returns:
//      The parameter value 
// NOTE the parameter filename is set using the ParameterFileName = 
// keyword in VB script
/////////////////////////////////////////////////////////////////////////////

long VBscript::GetParameter(LPCTSTR ParamName, long Default) 
{
	CParameterIO Params;
    long         Value;

    // Check parameter file has been specified
    if (m_parameterFileName == "")
        AfxThrowOleDispatchException(0xFF, IDS_NOPARAMETERFILE, 0);

    // Try and open the parameter file
    if (!Params.OpenParameters(m_parameterFileName))
        return Default;

    // Now try and get the parameter out of it
    Params.GetParameter(USER_PARAMETER_FILE, ParamName, &Value, Default);

    // Close the file
    Params.CloseParameters();

	return Value;
}

// Same as above but for doubles

double VBscript::GetDoubleParameter(LPCTSTR ParamName, double DefaultValue) 
{
	CParameterIO Params;
    double Value = DefaultValue;

    // Check parameter file has been specified
    if (m_parameterFileName == "")
        AfxThrowOleDispatchException(0xFF, IDS_NOPARAMETERFILE, 0);

    // Try and open the parameter file
    if (!Params.OpenParameters(m_parameterFileName))
        return DefaultValue;

    // Now try and get the parameter out of it
    Params.GetParameter(USER_PARAMETER_FILE, ParamName, &Value, DefaultValue);

    // Close the file
    Params.CloseParameters();

	return Value;
}

/////////////////////////////////////////////////////////////////////////////
// Set a parameter in a file - uses the .USR version
//
// VB script syntax:
//      SetParameter(ParamFileName, ParamName, Value)
// Parameters:
//      ParamName is the name of the parameter
//      Value is the new value to set
// Returns:
//      Nothing
// NOTE the parameter filename is set using the ParameterFileName = 
// keyword in VB script
/////////////////////////////////////////////////////////////////////////////

void VBscript::SetParameter(LPCTSTR ParamName, long Value) 
{
	CParameterIO Params;

    // Check parameter file has been specified
    if (m_parameterFileName == "")
        AfxThrowOleDispatchException(0xFF, IDS_NOPARAMETERFILE, 0);

    // If file opened set the value and close again
    if (Params.OpenParameters(m_parameterFileName))
    {
        Params.SetParameter(USER_PARAMETER_FILE, ParamName, Value);
        Params.CloseParameters();
    }
}

/////////////////////////////////////////////////////////////////////////////
// Stop further script execution.
//
// VB script syntax:
//      Break
// Parameters:
//      None
// Returns:
//      Nothing
/////////////////////////////////////////////////////////////////////////////

void VBscript::Break() 
{
    AfxThrowOleDispatchException(0xFF, IDS_BREAKSCRIPT, 0);
}

//////////////////////////////////////////////////////////////////////////////
// Allows the script to determine the mode of operation it is running in
//
// VB script syntax:
//      ExecMode = GetExecutionMode
// Parameters:
//      None
// Returns:
//      The mode the script is running in 0 (Debug), 1 (Interactive), 2 (Play)
//////////////////////////////////////////////////////////////////////////////

short VBscript::GetExecutionMode() 
{
	return (short) m_ExecutionMode;
}

//////////////////////////////////////////////////////////////////////////////
// Determine whether points in a rectangle intersect
//
// VB script syntax:
//      if (RectIntersect(pt1xl.....pt2yb)) then.....
// Parameters:
//      Coordinates for each rectangle of the form 
//      (xleft, ytop) (xright, ybottom)
// Returns:
//      True if they do
//////////////////////////////////////////////////////////////////////////////

BOOL VBscript::RectIntersect(long pt1xl, long pt1yt, long pt1xr, long pt1yb, long pt2xl, long pt2yt, long pt2xr, long pt2yb) 
{
    CRect Rect1(pt1xl, pt1yt, pt1xr, pt1yb);    
    CRect Rect2(pt2xl, pt2yt, pt2xr, pt2yb);    
    CRect RectI;

	return IntersectRect(RectI, (LPRECT) Rect1, (LPRECT) Rect2);
}

//////////////////////////////////////////////////////////////////////////////
// Initialise a long persistent value
//
// VB script syntax:
//      InitLongPersistent VarName, VarValue
// Parameters:
//      VarName is the name of the persistent variable
//      VarValue is its initial value
// Returns:
//      Nothing
// NOTES:
//      Does nothing if the variable already exists
//////////////////////////////////////////////////////////////////////////////

void VBscript::InitLongPersistent(LPCTSTR VarName, long VarValue) 
{
    m_pPersistentVariables->InitValue(VarName, VarValue);
}

//////////////////////////////////////////////////////////////////////////////
// Initialise a double persistent value
//
// VB script syntax:
//      InitDoublePersistent VarName, VarValue
// Parameters:
//      VarName is the name of the persistent variable
//      VarValue is its initial value
// Returns:
//      Nothing
// NOTES:
//      Does nothing if the variable already exists
//////////////////////////////////////////////////////////////////////////////

void VBscript::InitDoublePersistent(LPCTSTR VarName, double VarValue) 
{
    m_pPersistentVariables->InitValue(VarName, VarValue);
}

//////////////////////////////////////////////////////////////////////////////
// Initialise a string persistent value
//
// VB script syntax:
//      InitStringPersistent VarName, VarValue
// Parameters:
//      VarName is the name of the persistent variable
//      VarValue is its initial value
// Returns:
//      Nothing
// NOTES:
//      Does nothing if the variable already exists
//////////////////////////////////////////////////////////////////////////////

void VBscript::InitStringPersistent(LPCTSTR VarName, LPCTSTR VarValue) 
{
    m_pPersistentVariables->InitValue(VarName, CString(VarValue));
}

//////////////////////////////////////////////////////////////////////////////
// Sets a persistent value
//
// VB script syntax:
//      SetPersistent VarName, VarValue
// Parameters:
//      VarName is the name of the persistent variable
//      VarValue is its initial value
// Returns:
//      Nothing
// NOTES:
//      If the variable does not exist then throw an appropriate exception
//////////////////////////////////////////////////////////////////////////////

void VBscript::SetPersistent(LPCTSTR VarName, VARIANT FAR* VarValue) 
{
    m_pPersistentVariables->SetValue(VarName, *VarValue);
}

//////////////////////////////////////////////////////////////////////////////
// Gets a  persistent value
//
// VB script syntax:
//      GetPersistent VarName, VarValue
// Parameters:
//      VarName is the name of the persistent variable
//      VarValue is its initial value
// Returns:
//      Nothing
// NOTES:
//      If the variable does not exist then throw an appropriate exception
//////////////////////////////////////////////////////////////////////////////

void VBscript::GetPersistent(LPCTSTR VarName, VARIANT FAR* VarValue) 
{
    if (!m_pPersistentVariables->GetValue(VarName, VarValue))
	{
		CString dbg;

		dbg.Format(_T("\r\n<SCRIPTPROC> Persistent variable %s does not exist\r\n"), VarName);
		TRACE(dbg);

        AfxThrowOleDispatchException(0xFF, IDS_PERSISTENTVARIABLENOTEXIST, 0);
	}
}

//////////////////////////////////////////////////////////////////////////////
// Deletes a persistent var
// VB script syntax:
//		DeletePersisten VarName
// Parameters:
//		A string value which is the name of the persistent variable
//////////////////////////////////////////////////////////////////////////////

void VBscript::DeletePersistent(LPCTSTR VarName) 
{
	m_pPersistentVariables->ResetValue(VarName);
}

//////////////////////////////////////////////////////////////////////////////
// Utility arrays work like persistents
// except referenced by a handle between 0 and MAX_UTILITY_ARRAYS
//////////////////////////////////////////////////////////////////////////////

void VBscript::SetUtilityArrayValue(long ArrayIdx, LPCTSTR Name, VARIANT FAR* VarValue)
{
	if (ArrayIdx < 0 || ArrayIdx > MAX_UTILITY_ARRAYS)
		AfxThrowOleDispatchException(0xFF, IDS_ARRAYNOTEXIST, 0);
	else
	{
		m_pUtilityArrays[ArrayIdx]->SetValue(Name, *VarValue);
	}
}

BOOL VBscript::GetUtilityArrayValue(long ArrayIdx, LPCTSTR Name, VARIANT FAR* VarValue)
{
	if (ArrayIdx < 0 || ArrayIdx > MAX_UTILITY_ARRAYS)
		AfxThrowOleDispatchException(0xFF, IDS_ARRAYNOTEXIST, 0);

	return m_pUtilityArrays[ArrayIdx]->GetValue(Name, VarValue);
}


long VBscript::GetUtilityArraySize(long ArrayIdx)
{
	if (ArrayIdx < 0 || ArrayIdx > MAX_UTILITY_ARRAYS)
		AfxThrowOleDispatchException(0xFF, IDS_ARRAYNOTEXIST, 0);
	
	return m_pUtilityArrays[ArrayIdx]->GetCount();
}

void VBscript::GetUtilityArrayValueAt(long ArrayIdx, long Index, VARIANT FAR *VarName, VARIANT FAR* VarValue)
{
	if (ArrayIdx < 0 || ArrayIdx > MAX_UTILITY_ARRAYS)
		AfxThrowOleDispatchException(0xFF, IDS_ARRAYNOTEXIST, 0);

	if (Index >= m_pUtilityArrays[ArrayIdx]->GetCount())
		AfxThrowOleDispatchException(0xFF, IDS_ARRAYDIMENSIONS, 0);

	CString Name;


	POSITION P = m_pUtilityArrays[ArrayIdx]->m_VarBank.GetStartPosition();
	int i = 0;
	COleVariant V;

	while (P && i <= Index)
	{
		m_pUtilityArrays[ArrayIdx]->m_VarBank.GetNextAssoc(P, Name, V);
		i++;
	}

	
	VariantCopy(VarValue, &V);

	// Free up the existing string if there was one
	if (VarName->vt == VT_BSTR) 
		SysFreeString(VarName->bstrVal);
	V_VT(VarName) = VT_BSTR;
	// Allocate the string and return to VBS
	VarName->bstrVal = Name.AllocSysString();

}



//////////////////////////////////////////////////////////////////////////////
// Sends debug string to a debug string
//////////////////////////////////////////////////////////////////////////////

void VBscript::DebugString(LPCTSTR String) 
{
	NLog_Debug(_T("SCRIPT"), _T("%d: %s"), GetInstanceId(), String);
}

//////////////////////////////////////////////////////////////////////////////
// These commands make it possible for a script to change its execution 
// mode or detect its execution mode
// NB 0 = debug, 1 = interactive, 2 = play
//////////////////////////////////////////////////////////////////////////////


short VBscript::GetScriptExecMode() 
{
	return (short) m_ExecutionMode;
}

// NB must be 0, 1 or 2 other values ignored
void VBscript::SetScriptExecMode(short nNewValue) 
{
	switch (nNewValue)
	{
	case 0:
		m_ExecutionMode = modeDebug;
		break;
	case 1:
		m_ExecutionMode = modeInteractive;
		break;
	case 2:
		m_ExecutionMode = modePlay;
		break;
	}
}

//////////////////////////////////////////////////////////////////////////////
// Convert a long Colour into its separate R, G, B values
// Done as no current VB script equivalent
//////////////////////////////////////////////////////////////////////////////

void VBscript::GetRGB(long ColourIn, VARIANT FAR* R, VARIANT FAR* G, VARIANT FAR* B) 
{
	BYTE    Rc, Gc, Bc;

    Bc = (BYTE) ColourIn & 0xFF;
    Gc = (BYTE) (ColourIn / 0x100) & 0xFF;
    Rc = (BYTE) (ColourIn / 0x10000) & 0xFF;

    V_VT(R) = V_VT(G) = V_VT(B) = VT_I2;
    R->uiVal = Rc;
    G->uiVal = Gc;
    B->uiVal = Bc;
}

//////////////////////////////////////////////////////////////////////////////
// Atan2 function
//////////////////////////////////////////////////////////////////////////////

double VBscript::Atan2(double Top, double Bottom) 
{
	double A = atan2(Top, Bottom);

    if (A < 0.0)
        A += 2 * 3.141592653589;

    return A;
}

//////////////////////////////////////////////////////////////////////////////
// CreateArray simply creates a new dynamic array of elements 
// VB script syntax:
//      ArrayHandle = CreateArray(InitNumElements)
// Parameters:
//      InitNumElements is the initial size of the array
//////////////////////////////////////////////////////////////////////////////

long VBscript::CreateArray(long InitNumElements) 
{
	VBArrayPtr pVBArr;
    
    pVBArr = m_pArrays->NewArray(InitNumElements);

	return (long) pVBArr;
}

void VBscript::DeleteArray(long ArrayHandle) 
{
    if (m_pArrays->CheckValid((VBArrayPtr) ArrayHandle))
        m_pArrays->DeleteArray((VBArrayPtr) ArrayHandle);
    else
        AfxThrowOleDispatchException(0xff, IDS_ARRAYNOTEXIST, 0);
}

//////////////////////////////////////////////////////////////////////////////
// Bubble Sort an array
// VB script syntax:
//      SortArray ArrayHandle, Index
// Parameters: 
//      A valid array handle to sort elements of
// Returns:
//      True of the array handle is valid
//////////////////////////////////////////////////////////////////////////////

BOOL VBscript::SortArray(long ArrayHandle, long Index) 
{
    if (m_pArrays->CheckValid((VBArrayPtr) ArrayHandle))
    {
        m_pArrays->Sort((VBArrayPtr) ArrayHandle, Index);
	    return TRUE;
    }
    else
        AfxThrowOleDispatchException(0xff, IDS_ARRAYNOTEXIST, 0);

    return FALSE;
}

//////////////////////////////////////////////////////////////////////////////
// Set the value of an array at an index. The array will automatically grow
// if the array index exceeds the current size of the array
// VB script syntax:
//      ArraySetAt ArrayHandle, ArrayIndex, Value
// Parameters:
//      ArrayHandle is the array to set the value in
//      ArrayIndex is the index to set the element for
//      Value si the value to set
// Returns:
//      True if ArrayHandle is valid
//////////////////////////////////////////////////////////////////////////////

BOOL VBscript::ArraySetAt(long ArrayHandle, long ArrayIndex1, long ArrayIndex2, double ArrayValue) 
{
    VARIANT V;

	if (m_pArrays->CheckValid((VBArrayPtr) ArrayHandle))
    {
        V.dblVal = ArrayValue;
        V.vt = VT_R8;
        return m_pArrays->SetElement((VBArrayPtr) ArrayHandle, (int) ArrayIndex1, (int) ArrayIndex2, V);
    }
    else
        AfxThrowOleDispatchException(0xff, IDS_ARRAYNOTEXIST, 0);
    return FALSE;
}

//////////////////////////////////////////////////////////////////////////////
// Get the value of an array at an index. 
// VB script syntax:
//      ArrayGetAt ArrayHandle, ArrayIndex, Value
// Parameters:
//      ArrayHandle is the array to set the value in
//      ArrayIndex is the index to set the element for
//      Value is the value to get
// Returns:
//      True if ArrayHandle is valid or the index is out of bounds
//////////////////////////////////////////////////////////////////////////////

BOOL VBscript::ArrayGetAt(long ArrayHandle, long ArrayIndex1, long ArrayIndex2, VARIANT FAR* Value) 
{
    VARIANT V;

	if (m_pArrays->CheckValid((VBArrayPtr) ArrayHandle))
    {
        // Try to get the value
	    if (m_pArrays->GetElement((VBArrayPtr) ArrayHandle, (int) ArrayIndex1, (int) ArrayIndex2, &V))
        {
            *Value = V;
            // Return TRUE only if a valid set at this location
            if (V.vt != VT_EMPTY)
                return TRUE;
        }
    }
    else
        AfxThrowOleDispatchException(0xff, IDS_ARRAYNOTEXIST, 0);

    return FALSE;
}

//////////////////////////////////////////////////////////////////////////////
// Simply returns the current size of an array or -1 if the array is invalid
//////////////////////////////////////////////////////////////////////////////

long VBscript::ArrayUBound(long ArrayHandle) 
{
	return m_pArrays->UBound((VBArrayPtr) ArrayHandle);
}

//////////////////////////////////////////////////////////////////////////////
// CreateArray simply creates a new dynamic array of elements 
// VB script syntax:
//      ArrayHandle = CreateArray2D(Dimension1, Dimension2)
// Parameters:
//      Dimension1 is the initial size of the array (this can grow)
//      Dimension2 is the size of the second dimension (this can't grow)
//////////////////////////////////////////////////////////////////////////////

long VBscript::CreateArray2D(long Dimension1, long Dimension2) 
{
	VBArrayPtr pVBArr;
    
    pVBArr = m_pArrays->NewArray(Dimension1, Dimension2);

	return (long) pVBArr;
}

//////////////////////////////////////////////////////////////////////////////
// Simply sets the progress bar range for the progress bar displayed
// in somewhere in the main application. NOTE that the application is
// repsonsible for defining where the progress bar sits. If it does not
// call the relevant CScriptProcessor function then this does nothing.
// VB script syntax:
//		ProgressSetRange MinValue, MaxValue
// Parameters:
//		MinValue is the min value for the bar
//		MaxValue is the max value for the bar		
//////////////////////////////////////////////////////////////////////////////

void VBscript::ProgressSetRange(short MinRange, short MaxRange) 
{
	if (m_pProgressCtrl)
	{
		m_pProgressCtrl->SetRange(MinRange, MaxRange);
		m_pProgressCtrl->SetStep(1);					// Default to 1 for stepping
	}
}
//////////////////////////////////////////////////////////////////////////////
// Simply sets the progress bar position for the progress bar displayed
// in somewhere in the main application. NOTE that the application is
// repsonsible for defining where the progress bar sits. If it does not
// call the relevant CScriptProcessor function then this does nothing.
// VB script syntax:
//		ProgressSetPos MinValue, MaxValue
// Parameters:
//		Position is the position value for the bar
//////////////////////////////////////////////////////////////////////////////

void VBscript::ProgressSetPos(short Position) 
{
	if (m_pProgressCtrl)
	{
		m_pProgressCtrl->SetPos(Position);
	}
}
//////////////////////////////////////////////////////////////////////////////
// Simply hides or shows for the progress bar displayed
// in somewhere in the main application. NOTE that the application is
// repsonsible for defining where the progress bar sits. If it does not
// call the relevant CScriptProcessor function then this does nothing.
// VB script syntax:
//		ProgressShowWindow vbFalse or vbTrue
// Parameters:
//		Position is the position value for the bar
//////////////////////////////////////////////////////////////////////////////


void VBscript::ProgressShowWindow(BOOL bShow) 
{
	if (m_pProgressCtrl)
	{
		m_pProgressCtrl->ShowWindow(bShow);
	}
}

//////////////////////////////////////////////////////////////////////////////
// Step the  progress bar a specified increment
//////////////////////////////////////////////////////////////////////////////

void VBscript::ProgressStep(short Increment) 
{
	if (m_pProgressCtrl)
	{
		int Pos;

		Pos = m_pProgressCtrl->GetPos() + Increment;
		m_pProgressCtrl->SetPos(Pos);
	}
}

void VBscript::ProgressSetStep(short StepSize) 
{
	if (m_pProgressCtrl)
	{
		m_pProgressCtrl->SetStep(StepSize);
	}
}

void VBscript::ProgressStepIt() 
{
	if (m_pProgressCtrl)
	{
		m_pProgressCtrl->StepIt();
	}
}

//////////////////////////////////////////////////////////////////////////////
// Put whatever script is running to sleep for a period of milliseconds
//////////////////////////////////////////////////////////////////////////////

void VBscript::ScriptSleep(long MilliSecs) 
{
	Sleep(MilliSecs);
}

//////////////////////////////////////////////////////////////////////////////
// Set a registry entry specified by Key, SubKey and Item. 
//////////////////////////////////////////////////////////////////////////////

void VBscript::SetRegItem(LPCTSTR Key, LPCTSTR SubKey, LPCTSTR Item)
{
	TCHAR szKey[255];
	HKEY hKey;//, hKey_sub;
	DWORD dwSize;

	// Concatenate key and sub-key
	wsprintf(szKey, _T("%s\\%s"), Key, SubKey);

	// Create the key
	if (RegCreateKeyEx(HKEY_CURRENT_USER, szKey, 0, 0, 0, KEY_ALL_ACCESS, NULL, &hKey, &dwSize) == ERROR_SUCCESS) 
	{
		// If that went OK, set the value
	    dwSize = _tcslen(Item);

        RegSetValueEx(
			hKey,			// handle to key
			NULL,			// value name (use default (unnamed) value)
			0,				// reserved - must be zero
			REG_SZ,			// value type - using string
			(BYTE *)Item,	// value data
			dwSize			// size of value data
		);

		// Close the key to release the handle
	    RegCloseKey(hKey);
    }
}

//////////////////////////////////////////////////////////////////////////////
// Get a registry entry specified by Key and SubKey return the value
// in item. Returns FALSE if unsuccessful
//////////////////////////////////////////////////////////////////////////////

BOOL VBscript::GetRegItem(LPCTSTR Key, LPCTSTR SubKey, VARIANT FAR *Item)
{
	CString curr = _T("");
	TCHAR dll[255];
	TCHAR szKey[255];
	HKEY hKey;
	long dwSize;
    BOOL Ok = FALSE;


    wsprintf(szKey, Key);
	if (RegOpenKey(HKEY_CURRENT_USER, szKey, &hKey) == ERROR_SUCCESS) 
	{
		dwSize = sizeof(dll);

		if (RegQueryValue(hKey, SubKey, dll, &dwSize) == ERROR_SUCCESS)
        {
			curr = dll;
            V_VT(Item) = VT_BSTR;
            Item->bstrVal = curr.AllocSysString();
            Ok = TRUE;
        }

		RegCloseKey(hKey);
	}	

    return Ok;
}

//////////////////////////////////////////////////////////////////////////////
// Get the ID of a process. Returns 0 if unsuccessful, ProcID if the 
// process is running
//////////////////////////////////////////////////////////////////////////////

long VBscript::GetProcessID(LPCTSTR ProcessName)
{
    CProcessMonitor CPM;

    return CPM.IsProcessRunning(ProcessName);
}

//////////////////////////////////////////////////////////////////////////////
// Get the ID of the current process
//////////////////////////////////////////////////////////////////////////////

long VBscript::GetCurrentProcessID()
{
    return ::GetCurrentProcessId();
}

/////////////////////////////////////////////////////////////////////////////
//
//	GetAITIFF - Return the 'handle' to a tiff image on the queue
//
//	Similar to GetImage() but returns an AITiff handle
// 
// VB script syntax:
//      tiff = GetAITIFF()
// Parameters:
// Returns:
//      non 0 long value (handle) if successful otherwise 0
/////////////////////////////////////////////////////////////////////////////
long VBscript::GetAITIFF()
{
    VBS_IMAGE_REC   ImageData;

    // Get the image rec from the input Q to the script
    BOOL Ok = m_ScriptIO.GetImageFrom(&ImageData);

	// Test OK status
	if (Ok == FALSE)
		return 0;

	// Extract Tiff from ImageData->Address
	// Note: We don't make a copy of the image data
	// there really should be no need
	return (long)ImageData.Address;
}

//////////////////////////////////////////////////////////////////////////////
// Create a blob file name and complete path containing the AII blob pool
// VB script syntax:
//		Result = BlobFileName(BlobpoolBlobName)
// Parameters:
//		BlobpoolBlobName is the returned COMPLETE path and name for the blob
// Returns:
//      TRUE if successful, FALSE otherwise
//////////////////////////////////////////////////////////////////////////////

BOOL VBscript::BlobFileName(VARIANT FAR* BlobFileString) 
{
	BOOL bRet = FALSE;
	GUID		guid;
	wchar_t		szGUID[39];
    CString     Filepath;

	int attempts = 50;
	while (attempts--)
	{
		if (CoCreateGuid(&guid) != S_OK)
			continue;

		if(::StringFromGUID2 (guid, szGUID, 39) ==0)
			continue;

		CString newname = szGUID;
		newname += _T(".blb");

		// Create a directory name, from the 1st 3 characters of te name {AA
		CString dirname = newname.Left(3) + _T("}");

		// This is used to generate a directory into which to place the image. This
		// is because we don't what all the images in 1 directory. If the GUID is 
		// random enoughh, should get an even spread.

		// Attempt To Create the directory in the blobpool
		CString Dir = CAII_Shared::BlobPoolPath() + _T("\\") + dirname;
		CreateDirectory(Dir, NULL);

		Filepath = CAII_Shared::BlobPoolPath() + _T("\\") + dirname + _T("\\") + newname;

		if (_taccess(Filepath, 00) == -1)
		{
			bRet = TRUE;
			break;
		}
	}

	if (bRet)
    {
        // Got a unique name so return it but first if the var was a string then free it
        if (BlobFileString->vt == VT_BSTR) SysFreeString(BlobFileString->bstrVal);
        // Change the type to a BSTR
        V_VT(BlobFileString) = VT_BSTR;
        // Allocate the string and return to VBS
        BlobFileString->bstrVal = Filepath.AllocSysString();
    }

	return bRet;
}

//////////////////////////////////////////////////////////////////////////////
// Generates a GUID
//////////////////////////////////////////////////////////////////////////////

void VBscript::GUIDString(VARIANT FAR* GUIDString) 
{
	GUID		guid;
	wchar_t		szGUID[39];

	CoCreateGuid(&guid);

	::StringFromGUID2 (guid, szGUID, 39);

	CString newname = szGUID;
	
    // Got a unique name so return it but first if the var was a string then free it
    if (GUIDString->vt == VT_BSTR) SysFreeString(GUIDString->bstrVal);
    // Change the type to a BSTR
    V_VT(GUIDString) = VT_BSTR;
    // Allocate the string and return to VBS
    GUIDString->bstrVal = newname.AllocSysString();
}

//////////////////////////////////////////////////////////////////////////////
//  Create the class monitor dialog
// VB script syntax:
//		Result = CreateClassMonitor
// Parameters:
//		None
// Returns:
//      TRUE if successful, FALSE if already created
//////////////////////////////////////////////////////////////////////////////
BOOL VBscript::CreateClassMonitor(void)
{
	// Create one
	if (m_pGUIParent)
		m_pGUIParent->PostMessage(WM_CREATECLASSMON, 1, 0L);

	return m_pGUIParent ? TRUE : FALSE;
}

//////////////////////////////////////////////////////////////////////////////
//  Delete the class monitor dialog
// VB script syntax:
//		DeleteClassMonitor
// Parameters:
//		None
// Returns:
//      Nothing
//////////////////////////////////////////////////////////////////////////////

void VBscript::DeleteClassMonitor(void)
{
	// Create one
	if (m_pGUIParent)
		m_pGUIParent->PostMessage(WM_CREATECLASSMON, 0, 0L);
}

//////////////////////////////////////////////////////////////////////////////
//  Creates and Starts/Resets a high resolution timer for profiling purposes
// VB script syntax:
//		StartHRTimer
// Parameters:
//		None
// Returns:
//      Nothing
//////////////////////////////////////////////////////////////////////////////


afx_msg void VBscript::StartTime()
{
    if (!m_pHRT)
        m_pHRT = new CHRTimer(1000.);
    m_pHRT->Start();
}

afx_msg double VBscript::GetTime()
{
    if (m_pHRT)
    {
        double e, total;
        CString T;

        m_pHRT->ElapsedTime(&e);
        total = e * m_pHRT->m_Divisor;

        return total;
    }

    return -1.0;    // NO TIMER
}

//////////////////////////////////////////////////////////////////////////////
// Get the Thread Id 
//////////////////////////////////////////////////////////////////////////////

afx_msg long VBscript::GetInstanceId()
{
	return m_Id;
}


afx_msg BOOL VBscript::LockSection(long MilliSecs)
{
	return m_pLockMutex->Grab(MilliSecs);
}

afx_msg void VBscript::ReleaseSection()
{
	m_pLockMutex->Release();
}

//////////////////////////////////////////////////////////////////////////////
// Format a string using a 'C' style string spec
//////////////////////////////////////////////////////////////////////////////

afx_msg void VBscript::FormatNum(LPCTSTR Format, int Number, VARIANT FAR *String)
{
	CString Str;

	Str.Format(Format, Number);
	    // Change the type to a BSTR
    V_VT(String) = VT_BSTR;
    // Allocate the string and return to VBS
    String->bstrVal = Str.AllocSysString();

}
