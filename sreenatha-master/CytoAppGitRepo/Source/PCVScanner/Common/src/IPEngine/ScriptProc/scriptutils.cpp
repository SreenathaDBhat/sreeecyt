/////////////////////////////////////////////////////////////////////////////
// Script utility functions for pre-parsing and script file I/O
//
// Written By K Ratcliff 12042001
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include "AIQueue.h"
#include "CMeasurement.h"
#include <ImageBits.h>
#include "scriptutils.h"

// Output a message box if anything goes wrong with
// initialising and using the VB script engine
BOOL HRVERIFY(HRESULT hr, LPCTSTR msg)
{
   if (FAILED(hr)) 
   {
      CString str;
      str.Format(_T("Error: 0x%08lx (%s)"), hr, msg);
      if (hr == REGDB_E_CLASSNOTREG)
          str += _T("\r\n(Class not registered)");
      else
      if (hr == CLASS_E_NOAGGREGATION)
          str += _T("\r\n(Class no aggregation)");
      else
      if (hr == CO_E_NOTINITIALIZED)
          str += _T("\r\n(The CoInitializeEx function was not called on this thread)");
      else
      if (hr == E_NOINTERFACE)
          str += _T("\r\n(Requested interface not found)");
	  else
	  if (hr == TYPE_E_NAMECONFLICT)
		  str += _T("\r\n(Type name conflict)");
	  else
	  if (hr == TYPE_E_LIBNOTREGISTERED)
		  str += _T("\r\n(Type library not registered)");
	  else
	  if (hr == TYPE_E_ELEMENTNOTFOUND)
		  str += _T("\r\n(Element not found)");

      AfxMessageBox(str, 0x10000);
      return FALSE;
   }
 
   return TRUE;
}
 
/////////////////////////////////////////////////////////////////////////////
// Returns the variant as a double value
/////////////////////////////////////////////////////////////////////////////
double VariantDbl(VARIANT V)
{
    double d = 0.0;

    switch (V.vt)
    {
    case VT_I1:
        VarR8FromI1(V.bVal, &d);
        break;
    case VT_UI1:
        VarR8FromUI1(V.bVal, &d);
        break;
    case VT_I2:
        VarR8FromI2(V.iVal, &d);
        break;
    case VT_UI2:
        VarR8FromUI2(V.uiVal, &d);
        break;
    case VT_I4:
        VarR8FromI4(V.lVal, &d);
        break;
    case VT_UI4:
        VarR8FromUI4(V.uintVal, &d);
        break;
    case VT_R4:
        VarR8FromR4(V.fltVal, &d);
        break;
    case VT_R8:
        d = V.dblVal;
        break;
    }

    return d;
}

/////////////////////////////////////////////////////////////////////////////
// Compare two variants 
/////////////////////////////////////////////////////////////////////////////

BOOL VariantLessThan(VARIANT V1, VARIANT V2)
{
    double d1, d2;

    d1 = VariantDbl(V1);
    d2 = VariantDbl(V2);

    return (d1 < d2);
}

/////////////////////////////////////////////////////////////////////////////
// Compare two variants 
/////////////////////////////////////////////////////////////////////////////

BOOL VariantGreaterThan(VARIANT V1, VARIANT V2)
{
    double d1, d2;

    d1 = VariantDbl(V1);
    d2 = VariantDbl(V2);

    return (d1 > d2);
}

/////////////////////////////////////////////////////////////////////////////
// Compare two variants 
/////////////////////////////////////////////////////////////////////////////

BOOL VariantEqual(VARIANT V1, VARIANT V2)
{
    double d1, d2;

    d1 = VariantDbl(V1);
    d2 = VariantDbl(V2);

    return (d1 == d2);
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Queue Access Functions specific to VBScript and Script Proc classes
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

VBScriptIO::VBScriptIO()
{
    // Initialise to safe values
    m_pToQ      = NULL;
    m_pFromQ    = NULL;
}

VBScriptIO::~VBScriptIO()
{
    // Cleanup
    if (m_pToQ)
	{
        delete m_pToQ;
	}
    if (m_pFromQ)
	{
       delete m_pFromQ;
	}
}

/////////////////////////////////////////////////////////////////////////////
// Initialise both the input and out queues
/////////////////////////////////////////////////////////////////////////////

BOOL VBScriptIO::InitialiseScriptQs(LPCTSTR Qpath, LPCTSTR FromQName, LPCTSTR ToQName)
{
	if (m_pFromQ && m_pToQ)
		return TRUE;

    // Create the queues
    m_pFromQ = new CQueue;
    m_pToQ   = new CQueue;
    
    // Return if either queue does not get created
    if (!m_pFromQ || !m_pToQ)
        return FALSE;
    
    // If we got to here then both queues created so set them up
    if (m_pFromQ->Initialize(VB_Q_SIZE, Qpath, FromQName) != CQueue::Ok)
        return FALSE;
    
    if (m_pToQ->Initialize(VB_Q_SIZE, Qpath, ToQName) != CQueue::Ok)
        return FALSE;
    
    return TRUE;
    
}

/////////////////////////////////////////////////////////////////////////////
// Reset Qs in BOTH directions
/////////////////////////////////////////////////////////////////////////////

void VBScriptIO::Reset(void)
{
	if (m_pToQ)
	{
		// Reset the "to" Q
		if (m_pToQ->Lock(TRUE, VB_Q_TIMEOUT) == CQueue::LockOk)
		{
			m_pToQ->Reset();
			m_pToQ->ClearLock();
		}
	}

	if (m_pFromQ)
	{
		// Reset the "from" Q
		if (m_pFromQ->Lock(TRUE, VB_Q_TIMEOUT) == CQueue::LockOk)
		{
			m_pFromQ->Reset();
			m_pFromQ->ClearLock();
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// Clear Q of any remaining data
/////////////////////////////////////////////////////////////////////////////

void VBScriptIO::ClearQ(CQueue *Q)
{
	BOOL Ok = TRUE;
	VBS_IMAGE_REC	Image;
	CString	cs;
	long l;
	double d;

	// Keep pulling stuff from Qs until nothing left
	while (Ok) 
	{
		Ok = FALSE;
		if (GetLongFrom(&l))
			Ok = TRUE;
		if (GetDoubleFrom(&d))
			Ok = TRUE;
		if (GetImageFrom(&Image))
		{
			Ok = TRUE;
			delete Image.Address;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// Send a long to a queue
/////////////////////////////////////////////////////////////////////////////

BOOL VBScriptIO::SendLongTo(long L)
{
    return PutOnQueue(m_pToQ, &L, sizeof(long), VBS_LONG);
}

/////////////////////////////////////////////////////////////////////////////
// Send a double to a queue
/////////////////////////////////////////////////////////////////////////////

BOOL VBScriptIO::SendDoubleTo(double D)
{
    return PutOnQueue(m_pToQ, &D, sizeof(double), VBS_DOUBLE);
}

/////////////////////////////////////////////////////////////////////////////
// Get a long from a queue
/////////////////////////////////////////////////////////////////////////////

BOOL VBScriptIO::GetLongFrom(long *L)
{
    UINT Size;

	Size = sizeof(long);
    if (GetFromQueue(m_pFromQ, L, &Size, VBS_LONG))
        if (Size == sizeof(long))
        return TRUE;

    return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Get a double from a queue
/////////////////////////////////////////////////////////////////////////////

BOOL VBScriptIO::GetDoubleFrom(double *D)
{
    UINT Size;

	Size = sizeof(double);
    if (GetFromQueue(m_pFromQ, D, &Size, VBS_DOUBLE))
        if (Size == sizeof(double))
        return TRUE;

    return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Send an image record to a queue
/////////////////////////////////////////////////////////////////////////////

BOOL VBScriptIO::SendImageTo(VBS_IMAGE_REC *ImageData)
{
	return PutOnQueue(m_pToQ, ImageData, sizeof(VBS_IMAGE_REC), VBS_IMAGE);
}

BOOL VBScriptIO::SendImageTo(VBS_AITIFF_REC *ImageData)
{
    return PutOnQueue(m_pToQ, ImageData, sizeof(VBS_IMAGE_REC), VBS_AITIFF);
}

/////////////////////////////////////////////////////////////////////////////
// Get an image record from a queue
/////////////////////////////////////////////////////////////////////////////

BOOL VBScriptIO::GetImageFrom(VBS_IMAGE_REC *ImageData)
{
    UINT Size;

	Size = sizeof(VBS_IMAGE_REC);
    if (GetFromQueue(m_pFromQ, ImageData, &Size, VBS_IMAGE))
	{
        if (Size == sizeof(VBS_IMAGE_REC))
		{
			// Be safe, return FALSE if someone silly enough to stick a NULL address
			// or an image with 0 dimensions
			if (ImageData->Address == NULL || ImageData->Height == 0 || ImageData->Width == 0)
				return FALSE;
			else
				return TRUE;
		}
	}
	
    return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Send a string
/////////////////////////////////////////////////////////////////////////////

BOOL VBScriptIO::SendStringTo(LPCTSTR StringToSend)
{
	CString Msg;
	VBS_STRING_REC VBStr;
	
	// Take a copy of the string to send which will be freed when it is
	// pulled of at the other end
	TCHAR *String;
	
	String = (TCHAR *) malloc((_tcslen(StringToSend) + 1) * sizeof(TCHAR));
	_tcscpy(String, StringToSend);
	
	VBStr.Address = String;
	VBStr.Length  = _tcslen(String);
	
	return PutOnQueue(m_pToQ, &VBStr, sizeof(VBStr), VBS_STRING);
}

/////////////////////////////////////////////////////////////////////////////
// Get a string 
/////////////////////////////////////////////////////////////////////////////

BOOL VBScriptIO::GetStringFrom(CString& StringToGet)
{
    UINT Size;
    VBS_STRING_REC VBStr;
	
	Size = sizeof(VBS_STRING_REC);
    if (GetFromQueue(m_pFromQ, &VBStr, &Size, VBS_STRING))
    {
		if (Size == sizeof(VBStr))
		{			
			// Put in a CString
			StringToGet.Format(_T("%*.*s"), VBStr.Length, VBStr.Length, VBStr.Address);
            // Delete the copied string
			free(VBStr.Address);

			return TRUE;
		}
    }
	
    return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Put a value on a queue
// Parameters: 
//  *QToUse         - queue to put value on
//  *Data           - pointer to the data to put on the queue
//  Size            - size of type
//  Type            - type of data to put 
// Returns:
//  TRUE if successful, FALSE otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL VBScriptIO::PutOnQueue(CQueue *QToUse, void *Data, UINT Size, VBS_TYPE Type)
{
    BOOL Ok = FALSE;

    // First try and lock the queue
    if (QToUse->Lock(TRUE, VB_Q_TIMEOUT) == CQueue::LockOk)
    {
        // Put the data on it
        if (QToUse->Add(Data, Size, Type) == CQueue::Ok)
            Ok = TRUE;

        QToUse->ClearLock();
    }

    return Ok;
}

/////////////////////////////////////////////////////////////////////////////
// Get a value from the a queue. 
// Parameters: 
//  *QToUse         - queue to put value on
//  *Data           - pointer to the data to get from the queue
//  *Size           - size of type
//  *Type           - type of data to get
// Returns:
//  TRUE if successful, FALSE otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL VBScriptIO::GetFromQueue(CQueue *QToUse, void *Data, UINT *Size, VBS_TYPE Type)
{
    BOOL Ok = FALSE;
    VBS_TYPE PeekType;
    UINT PeekSize;

    // First try and lock the queue
    if (QToUse->Lock(TRUE, VB_Q_TIMEOUT) == CQueue::LockOk)
    {
        // Peek the queue to see whats in it
        if ((QToUse->Peek((UINT *) &PeekType, &PeekSize) == CQueue::Ok) && (PeekType == Type))
        {
            // Remove the data from it
            if (QToUse->Remove(Data, Size, (UINT *) &PeekType) == CQueue::Ok)
                Ok = TRUE;			
        }

        QToUse->ClearLock();
    }

    return Ok;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// This is a list of CArray objects
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

CArrayList::CArrayList()
{
}

CArrayList::~CArrayList()
{
    // Dispose of all allocated CArrays
    DisposeAll();
}

/////////////////////////////////////////////////////////////////////////////
// Allocate a new CArray object in the list, 
// returns a pointer to the new array
/////////////////////////////////////////////////////////////////////////////

VBArrayPtr CArrayList::NewArray(long MaxSize)
{
    VBArrayPtr      pVBArr;
    int             i;
    NumericArrayPtr pNA;

    pVBArr = new VBArray;
    pVBArr->m_pArray = new NumericArray;
    pVBArr->m_nColumns = DEFAULT_NUMERIC_ELEMENTS;
    // Set a size of MaxSize elements but grow it by 10% every time a new element 
    // added that exceeds current array bounds
    // the size is exceeded
    pVBArr->m_pArray->SetSize(MaxSize, 10);
    for (i = 0; i < MaxSize; i++)
    {
        pNA = new NumericArrayData;
        pNA->Elements = new double[pVBArr->m_nColumns];
        memset(pNA->Elements, 0, sizeof(double) * pVBArr->m_nColumns);
        pVBArr->m_pArray->SetAt(i, pNA);
    }
    m_ArrayList.AddHead(pVBArr);        

    return pVBArr;
}
/////////////////////////////////////////////////////////////////////////////
// Allocate a new CArray object in the list, 
// returns a pointer to the new array
/////////////////////////////////////////////////////////////////////////////

VBArrayPtr CArrayList::NewArray(long MaxSize, long MaxCols)
{
    VBArrayPtr      pVBArr;
    int             i;
    NumericArrayPtr pNA;

    pVBArr = new VBArray;
    pVBArr->m_pArray = new NumericArray;
    pVBArr->m_nColumns = MaxCols;
   
    // Set a size of MaxSize elements but grow it by 10% every time a new element 
    // added that exceeds current array bounds
    // the size is exceeded
    pVBArr->m_pArray->SetSize(MaxSize, 10);
    for (i = 0; i < MaxSize; i++)
    {
        pNA = new NumericArrayData;
        pNA->Elements = new double[pVBArr->m_nColumns];
        memset(pNA->Elements, 0, sizeof(double) * pVBArr->m_nColumns);
        pVBArr->m_pArray->SetAt(i, pNA);
    }
    m_ArrayList.AddHead(pVBArr);        

    return pVBArr;
}

// Free Rows in an array
void CArrayList::FreeElements(VBArrayPtr pVBArr)
{
    int i;
    NumericArrayData *pNA;

    for (i = 0; i <= pVBArr->m_pArray->GetUpperBound(); i++)
    {   
        if (GetElement(pVBArr, i, &pNA))
        {
            delete [] pNA->Elements;
            delete pNA;
        }
    }
    pVBArr->m_pArray->RemoveAll();
}
    

// Delete an array
void CArrayList::DeleteArray(VBArrayPtr pVBArr)
{
    POSITION P, OldP;
    VBArrayPtr  pVBA;
    P = m_ArrayList.GetHeadPosition();
    while (P)
    {
        OldP = P;
        pVBA = m_ArrayList.GetNext(P);
        if (pVBA == pVBArr)
        {
            FreeElements(pVBA);
            delete pVBArr->m_pArray;
            m_ArrayList.RemoveAt(OldP);
            delete pVBA;
        }
    }
}


/////////////////////////////////////////////////////////////////////////////
// Free all currently allocated CArray objects
/////////////////////////////////////////////////////////////////////////////

void CArrayList::DisposeAll(void)
{
    POSITION P, OldP;
    VBArrayPtr  pVBArr;

    P = m_ArrayList.GetHeadPosition();
    while (P)
    {   
        OldP = P;
        pVBArr = m_ArrayList.GetNext(P);
        FreeElements(pVBArr);
        delete pVBArr->m_pArray;
        m_ArrayList.RemoveAt(OldP);
        delete pVBArr;
    }
}

/////////////////////////////////////////////////////////////////////////////
// Check we are using a valid array objewct pointer in our list
/////////////////////////////////////////////////////////////////////////////

BOOL CArrayList::CheckValid(VBArrayPtr pVBArr)
{
    POSITION P;
    VBArrayPtr  pVBA;
    BOOL Ok = FALSE;

    P = m_ArrayList.GetHeadPosition();
    while (P && !Ok)
    {
        pVBA = m_ArrayList.GetNext(P);
        if (pVBA == pVBArr)
            Ok = TRUE;
    }

    return Ok;
}

// Return the upper bound of an array, -1 for an invalid array
int CArrayList::UBound(VBArrayPtr pVBArr)
{
    if (CheckValid(pVBArr))
    {
        return pVBArr->m_pArray->GetUpperBound();
    }
    
    return -1;
}


/////////////////////////////////////////////////////////////////////////////
// Sort an array 
/////////////////////////////////////////////////////////////////////////////

void CArrayList::Sort(VBArrayPtr pVBArr, int Index)
{
    int i, j;
    NumericArrayPtr pVi, pVj;
    int UB;

    // Sort it only if a valid array
    if (CheckValid(pVBArr) && Index >= 0 && Index < pVBArr->m_nColumns)
    {
        UB = pVBArr->m_pArray->GetUpperBound();
        // Just do a simple bubble sort
        for (i = UB; i >= 0; i--)
        {
            for (j = 0; j <= i; j++)
            {
                pVi = pVBArr->m_pArray->GetAt(i);
                pVj = pVBArr->m_pArray->GetAt(j);
                if (pVi->Elements[Index] > pVj->Elements[Index])
                {
                    pVBArr->m_pArray->SetAt(i, pVj);
                    pVBArr->m_pArray->SetAt(j, pVi);
                }
            }
        }
    }
}

/////////////////////////////////////////////////////////////////////////////
// Access elements in an array
/////////////////////////////////////////////////////////////////////////////

// Get the value at an index
BOOL CArrayList::GetElement(VBArrayPtr pVBArr, int Index1, int Index2, VARIANT *pV)
{
    BOOL Ok = FALSE;

    // Sort it only if a valid array
    if (CheckValid(pVBArr))
    {
        if (Index1 <= pVBArr->m_pArray->GetUpperBound() && Index2 < pVBArr->m_nColumns && Index2 >= 0)
        {
            V_VT(pV) = VT_R8;
            pV->dblVal = pVBArr->m_pArray->GetAt(Index1)->Elements[Index2];
            Ok = TRUE;
        }
    }

    return Ok;
}

// Get the value at an index
BOOL CArrayList::GetElement(VBArrayPtr pVBArr, int Index1, NumericArrayPtr *pNA)
{
    BOOL Ok = FALSE;

    // Sort it only if a valid array
    if (CheckValid(pVBArr))
    {
        if (Index1 <= pVBArr->m_pArray->GetUpperBound())
        {
            *pNA = pVBArr->m_pArray->GetAt(Index1);
            Ok = TRUE;
        }
    }

    return Ok;
}

// Set the value at an index
BOOL CArrayList::SetElement(VBArrayPtr pVBArr, int Index1, int Index2, VARIANT V)
{
    BOOL Ok = FALSE;
    NumericArrayPtr pN;
    //int i, UB;

    // Sort it only if a valid array
    if (CheckValid(pVBArr) && Index2 < pVBArr->m_nColumns && Index2 >= 0)
    {
        // Get the element if it already exists
        if (!GetElement(pVBArr, Index1, &pN))
        {
            // Doesn't already exist so add
            pN = new NumericArrayData;
            pN->Elements = new double[pVBArr->m_nColumns];
            pN->Elements[Index2] = VariantDbl(V);
            memset(pN->Elements, 0, sizeof(double) * pVBArr->m_nColumns);
            pVBArr->m_pArray->SetAtGrow(Index1, pN);
        }
        else
        {
            pN = pVBArr->m_pArray->GetAt(Index1);
            // Set the existing element
            pN->Elements[Index2] = VariantDbl(V);
            pVBArr->m_pArray->SetAt(Index1, pN);
        }
        Ok = TRUE;
    }

    return Ok;
}


