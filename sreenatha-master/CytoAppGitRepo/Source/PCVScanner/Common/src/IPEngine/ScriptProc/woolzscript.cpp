/////////////////////////////////////////////////////////////////////////////
// TMAScript.cpp : implementation file
//
// This OLE automation class is exposed to the script engine interface.
// Implementation of new Woolz specific keywords is done here. 
//
// Written By Karl Ratcliff 22072001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
// Implementation of new keywords is simply done using the Classwizard (to
// be recommended). This is done by selecting the Automation tab and then
// selecting the class name: Woolzscript. Add new 
//
// Functions modified to specify either 84 or 8 neighbourhoods
//
// KEEP IMAGES ON 4 BYTE BOUNDARIES THIS SAVES PAD AND UNPAD OPERATIONS THEREFORE QUICKER

#include "stdafx.h"
#include "vcmemrect.h"
#include "vcblob.h"
#include "vcgeneric.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "woolzif.h"
#include "vcthresh.h"
#include "vcImProc.h"
#include "uts.h"
#include "safearr.h"
#include "scriptutils.h"
#include "woolzscript.h"
#include "woolz.h"
#include "safearr.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WoolzScript
/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(WoolzScript, CCmdTarget)

WoolzScript::WoolzScript()
{
	EnableAutomation();
    // By default work in interactive mode
    m_ExecutionMode = modeDebug;
}

WoolzScript::~WoolzScript()
{
}

    

void WoolzScript::Reset(void)               // Reset
{
    IP().WoolzLib()->FreeObjects();                 // Release any previously created objects
    IP().WoolzLib()->KillMergedObjects();           // Kill any merge object information
	IP().WoolzLib()->FreePool();
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// NOTE implement class members not exposed to the scripting engine here
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


void WoolzScript::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}


BEGIN_MESSAGE_MAP(WoolzScript, CCmdTarget)
	//{{AFX_MSG_MAP(WoolzScript)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(WoolzScript, CCmdTarget)
	//{{AFX_DISPATCH_MAP(WoolzScript)
	DISP_FUNCTION(WoolzScript, "BackgroundThreshold", BackgroundThreshold, VT_I4, VTS_I4)
	DISP_FUNCTION(WoolzScript, "FSMin", FSMin, VT_I4, VTS_I4 VTS_I4 VTS_I2)
	DISP_FUNCTION(WoolzScript, "FSMax", FSMax, VT_I4, VTS_I4 VTS_I4 VTS_I2)
	DISP_FUNCTION(WoolzScript, "FSOpen", FSOpen, VT_I4, VTS_I4 VTS_I4 VTS_I2)
	DISP_FUNCTION(WoolzScript, "FSClose", FSClose, VT_I4, VTS_I4 VTS_I4 VTS_I2)
	DISP_FUNCTION(WoolzScript, "FSTopHat", FSTopHat, VT_I4, VTS_I4 VTS_I2)
	DISP_FUNCTION(WoolzScript, "FSExtract", FSExtract, VT_I4, VTS_I4 VTS_I4 VTS_I2 VTS_I2)
    DISP_FUNCTION(WoolzScript, "PathVysion", PathVysion, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4)
    DISP_FUNCTION(WoolzScript, "PathOlogical", PathOlogical, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "FSClahe", FSClahe, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "FSMedian", FSMedian, VT_I4, VTS_I4 VTS_I2)
	DISP_FUNCTION(WoolzScript, "FSGaussian", FSGaussian, VT_I4, VTS_I4 VTS_R4)
	DISP_FUNCTION(WoolzScript, "FSUnsharpMask", FSUnsharpMask, VT_I4, VTS_I4 VTS_R4 VTS_R4)
	DISP_FUNCTION(WoolzScript, "WoolzFindObjects", WoolzFindObjects, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzFindObjectsEx", WoolzFindObjectsEx, VT_I4, VTS_I4 VTS_I2 VTS_I4 VTS_I4 VTS_I4 VTS_BOOL)
	DISP_FUNCTION(WoolzScript, "WoolzFindObjectsAndMerge", FindObjectsAndMerge, VT_I4, VTS_I4 VTS_I2 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_BOOL)
	DISP_FUNCTION(WoolzScript, "WoolzFindGreyObjects", WoolzFindGreyObjects, VT_I4, VTS_I4 VTS_BSTR VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzJoinObjectEndPoints", WoolzJoinObjectEndPoints, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzJoinSameObjectEndPoints", WoolzJoinSameObjectEndPoints, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzArea", WoolzArea, VT_I4, VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzMass", WoolzMass, VT_I4, VTS_I4)
	DISP_FUNCTION(WoolzScript, "NumberVertices", NumberVertices, VT_I4, VTS_I4 VTS_BOOL VTS_I2)
	DISP_FUNCTION(WoolzScript, "WoolzBoundingBox", WoolzBoundingBox, VT_EMPTY, VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "MinWidthRectangle", MinWidthRectangle, VT_BOOL, VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "WoolzFreeObjects", WoolzFreeObjects, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(WoolzScript, "WoolzDrawObject", WoolzDrawObject, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzFillObject", WoolzFillObject, VT_EMPTY, VTS_I4 VTS_I4 VTS_I2)
	DISP_FUNCTION(WoolzScript, "WoolzFillObjectXY", WoolzFillObjectXY, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I2)
	DISP_FUNCTION(WoolzScript, "WoolzCreateMask", WoolzCreateMask, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzCreateMaskFast", WoolzCreateMaskFast, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzCreateMaskFromMerged", WoolzCreateMaskFromMerged, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzGreyData", WoolzGreyData, VT_EMPTY, VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "LabelWoolzObject", LabelWoolzObject, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "GetWoolzObjectLabel", GetWoolzObjectLabel, VT_I4, VTS_I4)
	DISP_FUNCTION(WoolzScript, "CountLabelledObjects", CountLabelledObjects, VT_I4, VTS_NONE)
	DISP_FUNCTION(WoolzScript, "WoolzGetObjects", WoolzGetObjects, VT_I4, VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "WoolzGetObjectGreyStats", WoolzGetObjectGreyStats, VT_I4, VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "FSSpotExtract", FSSpotExtract, VT_I4, VTS_I4 VTS_I2 VTS_I4)
	DISP_FUNCTION(WoolzScript, "FSSpotFind", FSSpotFind, VT_I4, VTS_I4 VTS_I4)	
	DISP_FUNCTION(WoolzScript, "WoolzMeasureObject", WoolzMeasureObject, VT_BOOL, VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "WoolzMeasureObject2", WoolzMeasureObject2, VT_BOOL, VTS_I4 VTS_PVARIANT)	
	DISP_FUNCTION(WoolzScript, "WoolzMeasureObjectEx", WoolzMeasureObjectEx, VT_BOOL, VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "WoolzMeasureMergedObject", WoolzMeasureMergedObject, VT_BOOL, VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "WoolzSplitMask", WoolzSplitMask, VT_EMPTY, VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "WoolzSplitMaskFast", WoolzSplitMaskFast, VT_I4, VTS_I4 VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzRemoveEdgeObjects", WoolzRemoveEdgeObjects, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzNumberOfComponents", WoolzNumberOfComponents, VT_I4, VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "WoolzMaskFromAllObjects", WoolzMaskFromAllObjects, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzMaskFromLabelledObjects", WoolzMaskFromLabelledObjects, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzMaskFromErodedLabelledObjects", WoolzMaskFromErodedLabelledObjects, VT_EMPTY, VTS_I4 VTS_I4)	
	DISP_FUNCTION(WoolzScript, "WoolzLabelObjectsIntersectingMask", WoolzLabelObjectsIntersectingMask, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "ObjectGreysInImage", ObjectGreysInImage, VT_EMPTY, VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "MergedObjectGreysinImage", MergedObjectGreysInImage, VT_EMPTY, VTS_I4 VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "GetBoundary", GetBoundary, VT_I4, VTS_I4 VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "GetConnectionPoints", GetConnectionPoints, VT_I4, VTS_I4 VTS_R8 VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "GetClosestConnectionPoints", GetClosestConnectionPoints, VT_I4, VTS_R8 VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "TextureMetFind", TextureMetFind, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "FractalBoundary", FractalBoundary, VT_R8, VTS_I2 VTS_I2)
	DISP_FUNCTION(WoolzScript, "WoolzDeagglomerate", WoolzDeagglomerate, VT_I4, VTS_I4 VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzDeagglomerateObjects", WoolzDeagglomerateObjects, VT_I4, VTS_NONE)
	DISP_FUNCTION(WoolzScript, "WoolzSplitObjects", WoolzSplitObjects, VT_I4, VTS_I4 VTS_I4 VTS_I4)	
	DISP_FUNCTION(WoolzScript, "WoolzConvexHull", WoolzConvexHull, VT_I4, VTS_I4 VTS_I2 VTS_I2)
	DISP_FUNCTION(WoolzScript, "WoolzClusterObjects", WoolzClusterObjects, VT_I4, VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzRemoveObject", WoolzRemoveObject, VT_I4, VTS_I4)
	DISP_FUNCTION(WoolzScript, "WoolzGetNumberOfObjects", WoolzGetNumberOfObjects, VT_I4, VTS_NONE)
	DISP_FUNCTION(WoolzScript, "WoolzGetObjectList", WoolzGetObjectList, VT_I4, VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "WoolzSetObjectList", WoolzSetObjectList, VT_I4, VTS_PVARIANT)
	DISP_FUNCTION(WoolzScript, "WoolzErodeLabelledObjects", WoolzErodeLabelledObjects, VT_EMPTY, VTS_I4)
	//DISP_FUNCTION(WoolzScript, "WoolzDilateLabelledObjects", WoolzDilateLabelledObjects, VT_EMPTY, VTS_I4)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IWoolzScript to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {62C41D37-F04F-4484-8517-8A8846B8DA16}
static const IID IID_IWoolzScript =
{ 0x62c41d37, 0xf04f, 0x4484, { 0x85, 0x17, 0x8a, 0x88, 0x46, 0xb8, 0xda, 0x16 } };

BEGIN_INTERFACE_MAP(WoolzScript, CCmdTarget)
	INTERFACE_PART(WoolzScript, IID_IWoolzScript, Dispatch)
END_INTERFACE_MAP()


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// WoolzScript message handlers
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
//
//	BackgroundThresh determines the threshold of an image by
//	first calculating the approximate background level in the image
//	and then analysing the greylevel gradients just above this level
//	i.e. it analyses the contours of the objects.
//  VB syntax:
//      Thresh = BackgroundThreshold(SrcImage)
//  Parameters:
//      SrcImage is the handle of the image to determine the threshold for
//  Returns:
//      >=0 for the background threshold
//
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::BackgroundThreshold(long SrcImage) 
{
    long W, H, BPP, P;
    long Thr = UTS_ERROR;
	void *ImageAddr;

	// Check for a valid image	
    if (SrcImage > 0)
    {
        BPP = IP().UTSMemRect()->GetExtendedInfo(SrcImage, &W, &H, &P, &ImageAddr);
		IP().WoolzLib()->UnpadImage(SrcImage);
        if (BPP != EIGHT_BITS_PER_PIXEL && BPP != SIXTEEN_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
        else
		{
			if (BPP == EIGHT_BITS_PER_PIXEL)
				Thr = IP().WoolzLib()->BackgroundThreshold((unsigned char *)ImageAddr, W, H);
			else
				Thr = IP().WoolzLib()->BackgroundThreshold16((WORD *)ImageAddr, W, H);
		}
		IP().WoolzLib()->PadImage(SrcImage);
        if (Thr == UTS_ERROR)
            AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

	return Thr;
}

////////////////////////////////////////////////////////////////////////////////////////
//
//		F S M I N	( erode grey image )
//
//	Inputs address of frame store, lines, cols , filter half width
//		frame - Pointer to begining of frame store
//		FiltSize - Filter halfwidth i.e. 1 => 3x3, 2 => 5x5 etc
//
//	Returns : a new handle containing result of NxN minimum filter
//		  where N = 2*halfwidth + 1
//
//	Uses local histogram ( held in rack[] ) to perform a very efficient
//	running calculation of the local minimum without sorting values.
////////////////////////////////////////////////////////////////////////////////////////
    
long WoolzScript::FSMin(long ImageHandle, long NeighbourHood, short FiltSize) 
{
    long W, H, BPP, P;
    BYTE *ImageAddr, *NewImageAddr;
    long NewHandle = UTS_ERROR;

	// Check for a valid image	
    if (ImageHandle > 0)
    {
        BPP = IP().UTSMemRect()->GetExtendedInfo(ImageHandle, &W, &H, &P, &ImageAddr);
        if (BPP != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
        else
        {
			IP().WoolzLib()->UnpadImage(ImageHandle);
            switch (NeighbourHood)
			{
			case 8:
				NewImageAddr = IP().WoolzLib()->FSMin(ImageAddr, W, H, FiltSize);
				break;
				
			case 84:
				NewImageAddr = IP().WoolzLib()->FSMin84(ImageAddr, W, H, FiltSize);
				break;
				
			default:
				AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);
				break;
			}
			IP().WoolzLib()->PadImage(ImageHandle);
            NewHandle = IP().WoolzLib()->MakePaddedImage(ImageHandle, NewImageAddr);
            if (NewHandle > 0)
			    delete NewImageAddr;
            else
            {
                if (NewImageAddr)
                    delete NewImageAddr;        
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
            }
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

	return NewHandle;
}

////////////////////////////////////////////////////////////////////////////////////////
//
//		F S M A X	( dilate grey image )
//
//	Inputs address of frame store, lines, cols , filter half width
//			frame - Pointer to begining of frame store
//			cols  - Number of pixels in a line
//			lines - Number of lines in a frame
//			halfwidth - Filter halfwidth i.e. 1 => 3x3, 2 => 5x5 etc
//
//	Outputs : new frame store containing result of NxN maximum filter
//		  where N = 2*halfwidth + 1
//
//	Uses local histogram ( held in rack[] ) to perform a very efficient
//	running calculation of the local maximum without sorting values.
////////////////////////////////////////////////////////////////////////////////////////
 
long WoolzScript::FSMax(long ImageHandle, long NeighbourHood, short FiltSize) 
{
    long W, H, BPP, P;
    BYTE *ImageAddr, *NewImageAddr;
    long NewHandle = UTS_ERROR;

	// Check for a valid image	
    if (ImageHandle > 0)
    {
        BPP = IP().UTSMemRect()->GetExtendedInfo(ImageHandle, &W, &H, &P, &ImageAddr);
        if (BPP != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
        else
        {
			IP().WoolzLib()->UnpadImage(ImageHandle);
            switch (NeighbourHood)
			{
			case 8:
				NewImageAddr = IP().WoolzLib()->FSMax(ImageAddr, W, H, FiltSize);
				break;
				
			case 84:
				NewImageAddr = IP().WoolzLib()->FSMax84(ImageAddr, W, H, FiltSize);
				break;
				
			default:
				AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);
				break;
			}
			IP().WoolzLib()->PadImage(ImageHandle);
            NewHandle = IP().WoolzLib()->MakePaddedImage(ImageHandle, NewImageAddr);
            if (NewHandle > 0)
			    delete NewImageAddr;
            else
            {
                if (NewImageAddr)
                    delete NewImageAddr;
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
            }
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

	return NewHandle;
}

long WoolzScript::FSOpen(long SrcImage, long NeighbourHood, short FiltSize) 
{
    long W, H, BPP, P;
    BYTE *ImageAddr, *NewImageAddr, *destimage = NULL;
    long NewHandle = UTS_ERROR;

	// Check for a valid image	
    if (SrcImage > 0)
    {
        BPP = IP().UTSMemRect()->GetExtendedInfo(SrcImage, &W, &H, &P, &ImageAddr);
        if (BPP != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
        else
        {
			// Remove padding from the src
			IP().WoolzLib()->UnpadImage(SrcImage);
			// Process images based on their bits per pixel             
            switch (NeighbourHood)
			{
			case 8:
				NewImageAddr = IP().WoolzLib()->FSOpen(ImageAddr, W, H, FiltSize);
				break;
				
			case 84:
				NewImageAddr = IP().WoolzLib()->FSOpen84(ImageAddr, W, H, FiltSize);
				break;
				
			default:
				AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);
				break;
			}

			if (!NewImageAddr)
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason

			// Repad the source image
			IP().WoolzLib()->PadImage(SrcImage);

			// Create a new image
            NewHandle = IP().WoolzLib()->MakePaddedImage(SrcImage, NewImageAddr);
            if (NewHandle > 0)
			    delete NewImageAddr;
            else
            {
                if (NewImageAddr)
                    delete NewImageAddr;
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
            }
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

	return NewHandle;
}

long WoolzScript::FSClose(long SrcImage, long NeighbourHood, short FiltSize) 
{
    long W, H, BPP, P;
    BYTE *ImageAddr, *NewImageAddr;
    long NewHandle = UTS_ERROR;

	// Check for a valid image	
    if (SrcImage > 0)
    {
        BPP = IP().UTSMemRect()->GetExtendedInfo(SrcImage, &W, &H, &P, &ImageAddr);
        if (BPP != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
        else
        {
			// Remove padding from the src
			IP().WoolzLib()->UnpadImage(SrcImage);
            switch (NeighbourHood)
			{
			case 8:
				NewImageAddr = IP().WoolzLib()->FSClose(ImageAddr, W, H, FiltSize);
				break;
				
			case 84:
				NewImageAddr = IP().WoolzLib()->FSClose84(ImageAddr, W, H, FiltSize);
				break;
				
			default:
				AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);
				break;
			}

			if (!NewImageAddr)
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason

			// Repad the source image
			IP().WoolzLib()->PadImage(SrcImage);

            NewHandle = IP().WoolzLib()->MakePaddedImage(SrcImage, NewImageAddr);
            if (NewHandle > 0)
			    delete NewImageAddr;
            else
            {
                if (NewImageAddr)
                    delete NewImageAddr;
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
            }
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

	return NewHandle;
}

long WoolzScript::FSTopHat(long SrcImage, short FiltSize) 
{
    long W, H, BPP, P;
    BYTE *ImageAddr, *NewImageAddr;
    long NewHandle = UTS_ERROR;

	// Check for a valid image	
    if (SrcImage > 0)
    {
        BPP = IP().UTSMemRect()->GetExtendedInfo(SrcImage, &W, &H, &P, &ImageAddr);
        if (BPP != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
        else
        {
            IP().WoolzLib()->UnpadImage(SrcImage);
            NewImageAddr = IP().WoolzLib()->FSTopHat(ImageAddr, W, H, FiltSize);
            IP().WoolzLib()->PadImage(SrcImage);
            NewHandle = IP().WoolzLib()->MakePaddedImage(SrcImage, NewImageAddr);
            if (NewHandle > 0)
			    delete NewImageAddr;
            else
            {
                if (NewImageAddr)
                    delete NewImageAddr;
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
            }
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

	return NewHandle;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Spot extraction 
//////////////////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::FSExtract(long srcImage, long neighbourHood, short filtSize, short noiseFiltSize) 
{
    long newHandle = UTS_ERROR;

	// Check for a valid image	
    if (srcImage > 0)
    {
		long width, height, srcBpp;
		BYTE *pFS = IP().WoolzLib()->CreateWoolzFSFromUTSImage(srcImage, width, height, srcBpp);

		if (pFS)
        {
			BYTE *pNewFS = NULL;

            switch (neighbourHood)
			{
			case 8:
				pNewFS = IP().WoolzLib()->FSExtract(pFS, width, height, filtSize, noiseFiltSize);
				break;
				
			case 84:
				pNewFS = IP().WoolzLib()->FSExtract84(pFS, width, height, filtSize, noiseFiltSize);
				break;
				
			default:
				AfxThrowOleDispatchException(0xFF, IDS_INVALIDOPERATOR, 0);
				break;
			}

			if (pNewFS)
			{
				newHandle = IP().WoolzLib()->CreateUTSImageFromWoolzFS(pNewFS, width, height, srcBpp);

				IP().WoolzLib()->FreeWoolzFS(pNewFS);

				if (newHandle <= 0)
					AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
			}

			IP().WoolzLib()->FreeWoolzFS(pFS);
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

	return newHandle;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Pathvysion probe image segmentation routine
// Parameters:
//      SrcImage        - the probe image
//      SpotWidth       - halfwidth of individual spots
//      Clusterwidth    - halfwidth of expected cluster sizes
//      MinArea         - min spot area 
// Returns:
//      An 8 bit binary mask containing the segmented probe signals
//////////////////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::PathVysion(long SrcImage, long SpotWidth, long ClusterWidth, long MinArea) 
{
    long W, H, P;
    BYTE *ImageAddr, *NewImageAddr = NULL, *TempAddr;
    long NewHandle = UTS_ERROR, TempHandle = UTS_ERROR;
	
	// Check for a valid image	
    if (UTSVALID_HANDLE(SrcImage))
    {
		// Get addresses
        IP().UTSMemRect()->GetExtendedInfo(SrcImage, &W, &H, &P, &ImageAddr);
        IP().WoolzLib()->UnpadImage(SrcImage);
		// Call the woolz fsSpotExtract
		NewImageAddr = IP().WoolzLib()->PathVysion(ImageAddr, W, H, SpotWidth, ClusterWidth, MinArea);
		IP().WoolzLib()->PadImage(SrcImage);

		// Now convert this into a UTS one bit mask
		TempHandle = IP().UTSMemRect()->CreateAs(SrcImage);
		if (UTSVALID_HANDLE(TempHandle))
		{
			// Set the bits in our temp image
			IP().UTSMemRect()->GetExtendedInfo(TempHandle, &W, &H, &P, &TempAddr);
			memcpy(TempAddr, NewImageAddr, H * P);
			IP().WoolzLib()->PadImage(TempHandle);

			// Now convert the 8 bit image into a single bit one
			NewHandle = IP().UTSMemRect()->Create(ONE_BIT_PER_PIXEL, W, H);
			if (UTSVALID_HANDLE(NewHandle))
			{
				// Created o.k. so now do the bit conversion
				IP().UTSMemRect()->Convert(TempHandle, NewHandle, 1);
				free(NewImageAddr);

				// Delete the temp image handle
				IP().UTSMemRect()->FreeImage(TempHandle);
			}
			else
			{
				free(NewImageAddr);
				IP().UTSMemRect()->FreeImage(TempHandle);
				AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);   // Mem allocation error
			}
		}
		else
		{
			// Free the Woolz created image
			if (NewImageAddr)
				delete NewImageAddr;
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);   // Mem allocation error
		}
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid
	
	return NewHandle;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Pathvysion tissue image segmentation routine
// Parameters:
//      SrcImage  - the counterstain tissue image
//      CellWidth - halfwidth of cells
// Returns:
//      An 8 bit binary mask containing the segmented image
//////////////////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::PathOlogical(long SrcImage, long CellWidth) 
{
    long W, H, BPP, P;
    BYTE *ImageAddr, *NewImageAddr;
    long NewHandle = UTS_ERROR;

    if (CellWidth < 2) 
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDPARAMETERVALUE, 0);
    else
    if (SrcImage > 0)	        // Check for a valid image	
    {
        BPP = IP().UTSMemRect()->GetExtendedInfo(SrcImage, &W, &H, &P, &ImageAddr);
        if (BPP != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
        else
        {
            IP().WoolzLib()->UnpadImage(SrcImage);
            NewImageAddr = IP().WoolzLib()->PathOlogical(ImageAddr, W, H, CellWidth);
            IP().WoolzLib()->PadImage(SrcImage);
            if (NewImageAddr)
                NewHandle = IP().WoolzLib()->MakePaddedImage(SrcImage, NewImageAddr);
            if (NewHandle > 0)
			    delete NewImageAddr;
            else
            {
                if (NewImageAddr)
                    delete NewImageAddr;
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
            }
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

	return NewHandle;
}

long WoolzScript::FSClahe(long SrcImage, int Min, int Max, int uiNrX, int uiNrY, int uiNrBins, int fCliplimit) 
{
    long W, H, BPP, P, ret;
    BYTE *ImageAddr;
    long BigHndl = UTS_ERROR;

	// Check for a valid image	
    if (SrcImage > 0)
    {
        BPP = IP().UTSMemRect()->GetExtendedInfo(SrcImage, &W, &H, &P, &ImageAddr);
        if (BPP != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
        else
        {
			long Xe = P + uiNrX - (P % uiNrX);
			long Ye = H + uiNrY - (H % uiNrY);
			BigHndl = IP().UTSMemRect()->Create(EIGHT_BITS_PER_PIXEL, Xe, Ye);
			IP().UTSMemRect()->MoveSubRect(BigHndl, 0, 0, SrcImage, "<=");
			IP().UTSMemRect()->GetExtendedInfo(BigHndl,  &W,  &H,  &P,  &ImageAddr);
			BYTE *im=(BYTE *)ImageAddr;
			ret = IP().WoolzLib()->FSClahe(im, P, H, Min, Max, uiNrX, uiNrY, uiNrBins, fCliplimit);
			IP().UTSMemRect()->MoveSubRect(BigHndl, 0, 0, SrcImage, "=>");
			IP().UTSMemRect()->FreeImage(BigHndl);
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

	return ret;
}

long WoolzScript::FSMedian(long ImageHandle, short FiltSize) 
{
    long W, H, BPP, P;
    BYTE *ImageAddr, *NewImageAddr;
    long NewHandle = UTS_ERROR;

	// Check for a valid image	
    if (ImageHandle > 0)
    {
        BPP = IP().UTSMemRect()->GetExtendedInfo(ImageHandle, &W, &H, &P, &ImageAddr);
        if (BPP != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
        else
        {
            IP().WoolzLib()->UnpadImage(ImageHandle);
			NewImageAddr = IP().WoolzLib()->FSMedian(ImageAddr, W, H, FiltSize);
			IP().WoolzLib()->PadImage(ImageHandle);
            NewHandle = IP().WoolzLib()->MakePaddedImage(ImageHandle, NewImageAddr);
            if (NewHandle > 0)
			    delete NewImageAddr;
            else
            {
                if (NewImageAddr)
                    delete NewImageAddr;        
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
            }
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

	return NewHandle;
}


long WoolzScript::FSGaussian(long imageHandle, float radius)
{
    long newHandle = UTS_ERROR;

	// Check for a valid image	
    if (imageHandle > 0)
    {
		long W, H, P;
		BYTE *imageAddr;
        long BPP = IP().UTSMemRect()->GetExtendedInfo(imageHandle, &W, &H, &P, &imageAddr);
        if (BPP != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0); // Image not correct bit depth
        else
        {
            IP().WoolzLib()->UnpadImage(imageHandle);
			BYTE *newImageAddr = IP().WoolzLib()->FSGaussian(imageAddr, W, H, radius);
            newHandle = IP().WoolzLib()->MakePaddedImage(imageHandle, newImageAddr);
            if (newHandle > 0)
			    delete newImageAddr;
            else
            {
                if (newImageAddr)
                    delete newImageAddr;        
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0); // Operation failed for some reason
            }
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0); // A handle is invalid

	return newHandle;
}

long WoolzScript::FSUnsharpMask(long imageHandle, float radius, float depth)
{
    long newHandle = UTS_ERROR;

	// Check for a valid image	
    if (imageHandle > 0)
    {
		long W, H, P;
		BYTE *imageAddr;
        long BPP = IP().UTSMemRect()->GetExtendedInfo(imageHandle, &W, &H, &P, &imageAddr);
        if (BPP != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0); // Image not correct bit depth
        else
        {
            IP().WoolzLib()->UnpadImage(imageHandle);
			BYTE *newImageAddr = IP().WoolzLib()->FSUnsharpMask(imageAddr, W, H, radius, depth);
            newHandle = IP().WoolzLib()->MakePaddedImage(imageHandle, newImageAddr);
            if (newHandle > 0)
			    delete newImageAddr;
            else
            {
                if (newImageAddr)
                    delete newImageAddr;        
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0); // Operation failed for some reason
            }
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0); // A handle is invalid

	return newHandle;
}


////////////////////////////////////////////////////////////////////////////////////////
// A Spot Finder - calls MGs SpotExtract function and then converts the result
// into a UTS one bit mask and returns the handle to this mask
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::FSSpotExtract(long SrcImage, short StructSize, long MinArea) 
{
    long W, H, P;
    BYTE *ImageAddr, *NewImageAddr = NULL, *TempAddr;
    long NewHandle = UTS_ERROR, TempHandle = UTS_ERROR;
	
	// Check for a valid image.
    if (UTSVALID_HANDLE(SrcImage))
    {
		// Get the UTS image address and dimensions.
        IP().UTSMemRect()->GetExtendedInfo(SrcImage, &W, &H, &P, &ImageAddr);

        // Unpad the image data so that it can be used by Woolz.
        IP().WoolzLib()->UnpadImage(SrcImage);

		// Call the Woolz FSSpotExtract function. This returns an unpadded Woolz image.
		NewImageAddr = IP().WoolzLib()->FSSpotExtract(ImageAddr, W, H, StructSize, MinArea);

		// Re-pad the UTS image data so that it is valid for a UTS image.
		IP().WoolzLib()->PadImage(SrcImage);

		// If no NewImageAddr (for example object count exceded MAXOBJ = 100000 for FSSpot extract), throw exception
		if (NewImageAddr == NULL)
		{
			AfxThrowOleDispatchException(0xFF, IDS_FSSPOTEXTRACTOBJECTLIMIT, 0);
			return 0;
		}

		// Create a temporary UTS image to copy the Woolz image into.
		TempHandle = IP().UTSMemRect()->CreateAs(SrcImage);
		if (UTSVALID_HANDLE(TempHandle))
		{
			// Get the UTS image address.
			IP().UTSMemRect()->GetExtendedInfo(TempHandle, &W, &H, &P, &TempAddr);

			// Copy the Woolz image into the UTS image. Note that the Woolz image is not padded,
			// so we only copy W*H bytes (not P*H), or there may be a buffer overrun (crash!).
			memcpy(TempAddr, NewImageAddr, W * H);

			// Pad the image data so that it becomes valid for a UTS image.
			IP().WoolzLib()->PadImage(TempHandle);

			// Now convert the 8 bit image into a single bit mask image.
			NewHandle = IP().UTSMemRect()->Create(ONE_BIT_PER_PIXEL, W, H);
			if (UTSVALID_HANDLE(NewHandle))
			{
				// Created o.k. so now do the bit conversion
				IP().UTSMemRect()->Convert(TempHandle, NewHandle, 1);
				free(NewImageAddr);

				// Delete the temp image handle
				IP().UTSMemRect()->FreeImage(TempHandle);
			}
			else
			{
				free(NewImageAddr);
				IP().UTSMemRect()->FreeImage(TempHandle);
				AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);   // Mem allocation error
			}
		}
		else
		{
			// Free the Woolz created image
			if (NewImageAddr)
				delete NewImageAddr;

			AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);   // Mem allocation error
		}
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid
	
	return NewHandle;
}

long WoolzScript::FSSpotFind(long hSrcImage, long minArea)
{
// This function does the second part of what FSSpotExtract() does - used with
// images that have already had the first part done (by FSSpotBackgroundSubtract()).
    long newHandle = UTS_ERROR;

    if (UTSVALID_HANDLE(hSrcImage))
    {
		// Get the UTS image address and dimensions.
        long w, h, p;
		BYTE *pImage;
        IP().UTSMemRect()->GetExtendedInfo(hSrcImage, &w, &h, &p, &pImage);

        // Unpad the image data so that it can be used by Woolz.
        IP().WoolzLib()->UnpadImage(hSrcImage);

		// Call the Woolz FSSpotFind function. This returns an unpadded Woolz image.
		BYTE *pNewImage = IP().WoolzLib()->FSSpotFind(pImage, w, h, minArea);

		// Re-pad the UTS image data so that it is valid for a UTS image.
		IP().WoolzLib()->PadImage(hSrcImage);
		
		// If no pNewImage (for example object count exceded MAXOBJ = 100000 for FSSpotFind), throw exception.
		if (!pNewImage)
		{
			AfxThrowOleDispatchException(0xFF, IDS_FSSPOTEXTRACTOBJECTLIMIT, 0);
			return 0;
		}

		// Create a temporary UTS image to copy the Woolz image into.
		long tempHandle = IP().UTSMemRect()->CreateAs(hSrcImage);
		if (UTSVALID_HANDLE(tempHandle))
		{
			// Get the UTS image address.
			BYTE *pTempAddr;
			IP().UTSMemRect()->GetExtendedInfo(tempHandle, &w, &h, &p, &pTempAddr);
			
			// Copy the Woolz image into the UTS image. Note that the Woolz image is not padded,
			// so must only copy w*h bytes (not p*h), or there may be a buffer overrun (crash!).
			memcpy(pTempAddr, pNewImage, w * h);

			// Pad the image data so that it becomes valid for a UTS image.
			IP().WoolzLib()->PadImage(tempHandle);

			// Now convert the 8 bit image into a single bit one
			newHandle = IP().UTSMemRect()->Create(ONE_BIT_PER_PIXEL, w, h);
			if (UTSVALID_HANDLE(newHandle))
			{
				// Created o.k. so now do the bit conversion
				IP().UTSMemRect()->Convert(tempHandle, newHandle, 1);
				
				free(pNewImage);
				IP().UTSMemRect()->FreeImage(tempHandle);
			}
			else
			{
				free(pNewImage);
				IP().UTSMemRect()->FreeImage(tempHandle);
				
				AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);   // Mem allocation error
			}
		}
		else
		{
			if (pNewImage)
				delete pNewImage;
				
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);   // Mem allocation error
		}
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid
	

	return newHandle;
}


////////////////////////////////////////////////////////////////////////////////////////
// Woolz object stuff
////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
// Find objects 
// Finds a series of Woolz objects and keeps a list internally. 
// VB Syntax:
//      NumberObjects = WoolzFindObjects(SrcImage, ThresholdLevel, MinArea)
// Parameters:
//      SrcImage is the handle to the image to find the objects in
//      ThresholdLevel is the segmentation level for the objects
//      MinArea is the minimum size of object to look for
// Returns:
//      The number of objects found
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::WoolzFindObjects(long SrcImage, long threshold, long MinArea) 
{
	return WoolzFindGreyObjects( SrcImage, ">=", threshold, MinArea);
}

////////////////////////////////////////////////////////////////////////////////////////
// Find objects 
// Finds a series of Woolz objects and keeps a list internally. The ThresholdLevel 
// comparison can be specified, eg blobs with a solid grey level of 10 can be found
// without thresholding to create a binary image first by using the "=" comparison.
//
// VB Syntax:
//      NumberObjects = WoolzFindGreyObjects(SrcImage, "=", ThresholdLevel, MinArea)
// Parameters:
//      SrcImage is the handle to the image to find the objects in
//		szCompare is the threshold comparison operator ("<", "<=", "=", ">=", ">")
//      ThresholdLevel is the segmentation level for the objects
//      MinArea is the minimum size of object to look for
// Returns:
//      The number of objects found
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::WoolzFindGreyObjects(long SrcImage, LPCTSTR szCompare, long threshold, long MinArea) 
{
    long    W, H, BPP, P;
    BYTE    *ImageAddr;
    int     NumObjects = 0;
    long    TempImage;

	// Check for a valid image	
    if (SrcImage > 0)
    {
        BPP = IP().UTSMemRect()->GetRectInfo(SrcImage, &W, &H);
        if (BPP != SIXTEEN_BITS_PER_PIXEL && BPP != EIGHT_BITS_PER_PIXEL && BPP != ONE_BIT_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
        else
        {
            // Create a temporary version of the image which will be flipped vertically.
            TempImage = IP().UTSMemRect()->CreateAsWithPix(SrcImage, EIGHT_BITS_PER_PIXEL);
            if (TempImage < 1)
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
            else
            {
                // Get the addr of the temp image
                IP().UTSMemRect()->GetExtendedInfo(TempImage, &W, &H, &P, &ImageAddr);

                // Copy the src image into the temp, converting to 8bpp if required, as that's all FindObjects understands.
                if (BPP == SIXTEEN_BITS_PER_PIXEL)
				{
                    IP().UTSMemRect()->Convert16To8(SrcImage, TempImage);
					// Must convert the threshold too!
					int threshold8 = threshold >> 8;
					// If threshold8 is 0, CWoolzIP::FindObjects() won't find anything.
					// For 16 bpp images, if the 16-bit threshold was > 0, this probably wasn't what was intended,
					// so bump it up to 1.
					if (threshold8 <= 0 && threshold > 0)
						threshold8 = 1;
					threshold = threshold8;
				}
                else if (BPP == EIGHT_BITS_PER_PIXEL)
                    IP().UTSMemRect()->Move(SrcImage, TempImage);
                else // 1bpp
                    // Convert from one bit to eight bits where each ON bit is set to 255
                    IP().UTSMemRect()->Convert(SrcImage, TempImage, 255);

				// Flip the temp image.
                IP().UTSMemRect()->InvertRows(TempImage);

				// Unpad the UTS image so it can be used by Woolz.
				IP().WoolzLib()->UnpadImage(TempImage);

                // Look for the woolz objects
                if (BPP == ONE_BIT_PER_PIXEL)
                    NumObjects = IP().WoolzLib()->FindGreyObjects(ImageAddr, W, H, szCompare, 1, MinArea); // Look for anything set to one or above
                else
                    NumObjects = IP().WoolzLib()->FindGreyObjects(ImageAddr, W, H, szCompare, threshold, MinArea);

			    // Free the temporary image
                IP().UTSMemRect()->FreeImage(TempImage);
            }
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

	return NumObjects;
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzJoinObjectEndPoints
//
// For each object join its' end points to nearby end points belonging to OTHER
// objects. It is assumed that WoolzFindObject or WoolzFindObjectEx 
// has been called first.
// VB Syntax:
//      WoolzJoinObjectEndPoints MaskImage, maxDistance
// Parameters:
//      MaskImage is the handle to the 8 bit binary image the woolz objects live in
//		maxDistance is the distance used to decide if end points are near.
// Returns:
//      None
////////////////////////////////////////////////////////////////////////////////////////
void WoolzScript::WoolzJoinObjectEndPoints( long MaskImage, int maxDistance)
{
    long    W, H, BPP, P;
	long	TempImage;
    BYTE    *TempImageAddr;

    if (MaskImage < 1)
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

    BPP = IP().UTSMemRect()->GetRectInfo(MaskImage, &W, &H);
    if (BPP != EIGHT_BITS_PER_PIXEL)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Image not correct bit depth


    // Create a temporary version of the image
    TempImage = IP().UTSMemRect()->CreateAsWithPix( MaskImage, EIGHT_BITS_PER_PIXEL);
    if (TempImage < 1)
        AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason

    IP().UTSMemRect()->GetExtendedInfo(TempImage, &W, &H, &P, &TempImageAddr);
	IP().UTSMemRect()->Move(MaskImage, TempImage);
	// Unpad the UTS image so it can be used by Woolz.
	IP().WoolzLib()->UnpadImage(TempImage);

	// Find all end points
	CArray<WoolzPoint> * allEndPoints = new CArray<WoolzPoint>[IP().WoolzLib()->GetNumObjects()];
	for (int i=0; i<IP().WoolzLib()->GetNumObjects(); i++)
	{
		IP().WoolzLib()->EndPointsInImage( i, TempImageAddr, W, H, &(allEndPoints[i]));
	}

	// Free the temporary image
    IP().UTSMemRect()->FreeImage(TempImage);


	// Join nearby end points
	long lineData[4];
	int distanceSquaredLimit = maxDistance * maxDistance;

	// Instead of checking each object against every other object (n squared operations) like an n x n table
	// check just the upper triangle, see asterisks below. Then within each object check against each point. 
	//
	//	 1 2 3 4
	//	1* * * *
	//	2  * * *
	//	3    * * 
	//	4      *
	// 
	for (int obj=0; obj<IP().WoolzLib()->GetNumObjects(); obj++)
	{
		CArray<WoolzPoint> & endPoints = allEndPoints[obj];

		for (int e=0; e<endPoints.GetCount(); e++)
		{
			WoolzPoint endPoint = endPoints[e];

			int MinD = distanceSquaredLimit;

			for (int otherObj=obj; otherObj<IP().WoolzLib()->GetNumObjects(); otherObj++)
			{
				CArray<WoolzPoint> & otherObjectEndPoints = allEndPoints[otherObj];
				for (int other_e=0; other_e<otherObjectEndPoints.GetCount(); other_e++)
				{
					WoolzPoint otherEndPoint = otherObjectEndPoints[other_e];

					int xDiff = endPoint.x - otherEndPoint.x;
					int yDiff = endPoint.y - otherEndPoint.y;

					int D = xDiff * xDiff + yDiff * yDiff;

					if (xDiff*xDiff + yDiff*yDiff < distanceSquaredLimit && D < MinD && D != 0)
					{
						lineData[0] = endPoint.x;
						lineData[1] = endPoint.y;
						lineData[2] = otherEndPoint.x;
						lineData[3] = otherEndPoint.y;
						MinD = D;

					}
				}
			}
						
			if (MinD != distanceSquaredLimit)
				IP().UTSMemRect()->MaskLine(MaskImage, "Line", 255, 1, lineData);

		}
	}

	delete[] allEndPoints;
}


void WoolzScript::WoolzJoinSameObjectEndPoints(long MaskImage, int maxDistance)
{
    long    W, H, BPP, P;
	long	TempImage;
    BYTE    *TempImageAddr;

    if (MaskImage < 1)
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

    BPP = IP().UTSMemRect()->GetRectInfo(MaskImage, &W, &H);
    if (BPP != EIGHT_BITS_PER_PIXEL)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);			// Image not correct bit depth


    // Create a temporary version of the image
    TempImage = IP().UTSMemRect()->CreateAsWithPix( MaskImage, EIGHT_BITS_PER_PIXEL);
    if (TempImage < 1)
        AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason

    IP().UTSMemRect()->GetExtendedInfo(TempImage, &W, &H, &P, &TempImageAddr);
	IP().UTSMemRect()->Move(MaskImage, TempImage);
	// Unpad the UTS image so it can be used by Woolz.
	IP().WoolzLib()->UnpadImage(TempImage);

	// Find all end points
	CArray<WoolzPoint> * allEndPoints = new CArray<WoolzPoint>[IP().WoolzLib()->GetNumObjects()];
	for (int i=0; i<IP().WoolzLib()->GetNumObjects(); i++)
	{
		IP().WoolzLib()->EndPointsInImage( i, TempImageAddr, W, H, &(allEndPoints[i]));
	}

	// Free the temporary image
    IP().UTSMemRect()->FreeImage(TempImage);


	// Join nearby end points
	long lineData[4];
	int distanceSquaredLimit = maxDistance * maxDistance;

	// Instead of checking each object against every other object (n squared operations) like an n x n table
	// check just the upper triangle, see asterisks below. Then within each object check against each point. 
	//
	//	 1 2 3 4
	//	1* * * *
	//	2  * * *
	//	3    * * 
	//	4      *
	// 
	for (int obj=0; obj<IP().WoolzLib()->GetNumObjects(); obj++)
	{
		CArray<WoolzPoint> & endPoints = allEndPoints[obj];

		for (int e=0; e<endPoints.GetCount(); e++)
		{
			WoolzPoint endPoint = endPoints[e];

			int MinD = distanceSquaredLimit;

			for (int otherObj=obj; otherObj<IP().WoolzLib()->GetNumObjects(); otherObj++)
			{
				if (otherObj == obj)
				{
					CArray<WoolzPoint> & otherObjectEndPoints = allEndPoints[otherObj];
					
					for (int other_e=0; other_e<otherObjectEndPoints.GetCount(); other_e++)
					{
						WoolzPoint otherEndPoint = otherObjectEndPoints[other_e];

						int xDiff = endPoint.x - otherEndPoint.x;
						int yDiff = endPoint.y - otherEndPoint.y;

						int D = xDiff * xDiff + yDiff * yDiff;

						if (xDiff*xDiff + yDiff*yDiff < distanceSquaredLimit && D < MinD && D != 0)
						{
							lineData[0] = endPoint.x;
							lineData[1] = endPoint.y;
							lineData[2] = otherEndPoint.x;
							lineData[3] = otherEndPoint.y;
							MinD = D;

						}
					}
				}
			}
						
			if (MinD != distanceSquaredLimit)
				IP().UTSMemRect()->MaskLine(MaskImage, "Line", 255, 1, lineData);

		}
	}

	delete[] allEndPoints;
}
////////////////////////////////////////////////////////////////////////////////////////
// Find objects 
// Finds a series of Woolz objects and keeps a list internally. 
// VB Syntax:
//      NumberObjects = WoolzFindObjectsEx(SrcImage, ThresholdLevel, MinArea, MaxArea, BorderWidth, Flip)
// Parameters:
//      SrcImage is the handle to the image to find the objects in
//      ThresholdLevel is the segmentation level for the objects
//      MinArea is the minimum size of object to look for
//      MaxArea is the maximum size of object to look for
//      BorderWidth is the distance from object to border
//      Image should be flipped so objects right way up
// Returns:
//      The number of objects found
// NOTE:
//      No flipping can save time. NO 1 BIT IMAGE SUPPORT
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::WoolzFindObjectsEx(long SrcImage, short ThesholdLevel, long MinArea, long MaxArea, long BorderWidth, BOOL Flip)
{
    long    W, H, BPP, P;
    BYTE    *ImageAddr, *SrcAddr;
    int     NumObjects = 0;
    long    TempImage = -1;

	// Check for a valid image	
    if (SrcImage > 0)
    {
        BPP = IP().UTSMemRect()->GetExtendedInfo(SrcImage, &W, &H, &P, &SrcAddr);
        if (BPP != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
        else
        {
            // Create a temporary version of the image which is flipped vertically
            if (Flip)
                TempImage = IP().UTSMemRect()->CreateAsWithPix(SrcImage, EIGHT_BITS_PER_PIXEL);

            if (TempImage < 1 && Flip)
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
            else
            {
                if (Flip)
                {
                    // Get the addr of the temp image
                    IP().UTSMemRect()->GetExtendedInfo(TempImage, &W, &H, &P, &ImageAddr);
                    // Copy the src image into the temp one 
                    IP().UTSMemRect()->Move(SrcImage, TempImage);
                    
                    IP().UTSMemRect()->InvertRows(TempImage);
                }
                else
                { 
                    TempImage = SrcImage;
                    ImageAddr = SrcAddr;
                }
				IP().WoolzLib()->UnpadImage(TempImage);
                // Look for the woolz objects
                NumObjects = IP().WoolzLib()->FindObjectsEx(ImageAddr, W, H, ThesholdLevel, MinArea, MaxArea, BorderWidth);

			    // Free the temporary image
                if (Flip)
                    IP().UTSMemRect()->FreeImage(TempImage);
            }
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

	return NumObjects;
}

////////////////////////////////////////////////////////////////////////////////////////
// Find objects and merges objects within a specific proximity as well
// Finds a series of Woolz objects and keeps a list internally. 
// VB Syntax:
//      NumberObjects = WoolzFindObjectsAndMerge(SrcImage, ThresholdLevel, MinArea, MaxArea, BorderWidth, Flip)
// Parameters:
//      SrcImage is the handle to the image to find the objects in
//      ThresholdLevel is the segmentation level for the objects
//      AbsMin is the smallest possible object to merge
//      MinArea is the minimum size of object to look for after merge
//      MaxArea is the maximum size of object to look for
//      BorderWidth is the distance from object to border
//      Proximity used to merge objects
//      Image should be flipped so objects right way up
//      
// Returns:
//      The number of objects found
// NOTE:
//      No flipping can save time. NO 1 BIT IMAGE SUPPORT. Merging objects is done
//      based on bounding box proximity
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::FindObjectsAndMerge(long SrcImage, short ThesholdLevel, long AbsMin, long MinArea, long MaxArea, long BorderWidth, long Proximity, BOOL Flip)
{
    long    W, H, BPP, P;
    BYTE    *ImageAddr, *SrcAddr;
    int     NumObjects = 0;
    long    TempImage = -1;

	// Check for a valid image	
    if (SrcImage > 0)
    {
        BPP = IP().UTSMemRect()->GetExtendedInfo(SrcImage, &W, &H, &P, &SrcAddr);
        if (BPP != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
        else
        {
            // Create a temporary version of the image which is flipped vertically
            if (Flip)
                TempImage = IP().UTSMemRect()->CreateAsWithPix(SrcImage, EIGHT_BITS_PER_PIXEL);

            if (TempImage < 1 && Flip)
                AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
            else
            {
                if (Flip)
                {
                    // Get the addr of the temp image
                    IP().UTSMemRect()->GetExtendedInfo(TempImage, &W, &H, &P, &ImageAddr);
                    // Copy the src image into the temp one 
                    IP().UTSMemRect()->Move(SrcImage, TempImage);
                    
                    IP().UTSMemRect()->InvertRows(TempImage);
                }
                else
                { 
                    TempImage = SrcImage;
                    ImageAddr = SrcAddr;
                }
				IP().WoolzLib()->UnpadImage(TempImage);
                // Look for the woolz objects
                NumObjects = IP().WoolzLib()->FindObjectsAndMerge(ImageAddr, W, H, ThesholdLevel, AbsMin, MinArea, MaxArea, BorderWidth, Proximity);

				//TCHAR dbg[256];
				//_stprintf(dbg, _T("NUMBER OF OBJECTS %d Thr %d W %d H %d Addr %p\r\n"), NumObjects, ThesholdLevel, W, H, ImageAddr);
				//OutputDebugString(dbg);

			    // Free the temporary image
                if (Flip)
                    IP().UTSMemRect()->FreeImage(TempImage);
            }
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

	return NumObjects;
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzFreeObjects
//      Frees the objects found by the find objects function
// VB Script Syntax:
//      WoolzFreeObjects
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void WoolzScript::WoolzFreeObjects() 
{
    IP().WoolzLib()->FreeObjects();
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzArea
//      Finds the area of a woolz object.
// VB Script syntax:
//      Area = WoolzArea(WoolzObject)
// Parameters:
//      WoolzObject is the index of an object currently in the woolz object list
//      created by a call to WoolzFindObjects
// Returns:
//      If WoolzObject is valid then returns the area of the object, else returns 0
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::WoolzArea(long objectIndex) 
{
	int ObjArea;

    if (IP().WoolzLib()->GetObjectArea(objectIndex, &ObjArea))
        return ObjArea;
    else
	    return 0;
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzMass
//      Finds the sum of grey levels for a woolz object.
// VB Script syntax:
//      Mass = WoolzMass(WoolzObject)
// Parameters:
//      WoolzObject is the index of an object currently in the woolz object list
//      created by a call to WoolzFindObjects
// Returns:
//      If WoolzObject is valid then returns the mass of the object, else returns 0
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::WoolzMass(long objectIndex) 
{
	int ObjMass;

    if (IP().WoolzLib()->GetObjectMass(objectIndex, &ObjMass))
        return ObjMass;
    else
	    return 0;
}

long WoolzScript::NumberVertices(long objectIndex, BOOL UseTroughs, short ThreshLevel) 
{
    return IP().WoolzLib()->NumberVertices(objectIndex, UseTroughs, ThreshLevel);
}

void WoolzScript::WoolzBoundingBox(long objectIndex, VARIANT *pXMin, VARIANT *pYMin, VARIANT *pXMax, VARIANT *pYMax) 
{
	CWoolzIP::Bounds B;

	IP().WoolzLib()->BoundingBox(objectIndex, B);

    V_VT(pXMin) = VT_I4;
	pXMin->lVal = B.Xmin;
    V_VT(pYMin) = VT_I4;
	pYMin->lVal = B.Ymin;

    V_VT(pXMax) = VT_I4;
	pXMax->lVal = B.Xmax;
    V_VT(pYMax) = VT_I4;
	pYMax->lVal = B.Ymax;
}

////////////////////////////////////////////////////////////////////////////////////////
// MinWidthRectangle
//      Find the minimum width rectangle for a woolz object found with WoolzFindObjects
// VB Script Syntax:
//      Ok = MinWidthRectangle(objectIndex, Angle, Vertices)
// Parameters:
//      objectIndex is the index of the object we want the vertices for
//      Angle is the returned orientation for the rectangle
//      Vertices should be an array of 4 by 2 elements, one for each vertex coordinate 
////////////////////////////////////////////////////////////////////////////////////////

BOOL WoolzScript::MinWidthRectangle(long objectIndex, VARIANT FAR* Angle, VARIANT FAR* Width, VARIANT FAR* Height, VARIANT FAR* Vertices) 
{
    BOOL        Ok;
    float       A;
    long        Dx, Dy;
    POINT       V1, V2, V3, V4;
    SAFEARRAY   *pSafe;             // Safe array to put data into
    VARIANT     V;                  // Temp var for packaging data to put in array

    // Get array info
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Vertices, &pSafe, 2), return 0);
    
    // Check the first dimension is = 4, 0th element = X, 1st element = Y
    CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 1, 4), return 0);

    // Check the second dimension is = 2, 0th element = X, 1st element = Y
    CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 2, 2), return 0);

    // Call the lib function
    Ok = IP().WoolzLib()->MinWidthRectangle(objectIndex, &A, &V1, &V2, &V3, &V4);
    if (Ok)
    {
        // Fill in the angle
        V_VT(Angle) = VT_R8;
        Angle->dblVal = (double) A;

        // Calc Height and Width of the rectangle
        V_VT(Width) = VT_R8;
        Dx = V1.x - V2.x;
        Dy = V1.y - V2.y;
        Width->dblVal = sqrt(Dx * Dx + Dy * Dy + 0.1);
        V_VT(Height) = VT_R8;
        Dx = V2.x - V3.x;
        Dy = V2.y - V3.y;
        Height->dblVal = sqrt(Dx * Dx + Dy * Dy + 0.1);

        // Done successfully so copy the vertices into the safe array 
        V_VT(&V) = VT_I4;
        V.lVal = V1.x;
        Set2DArrayIndex(pSafe, 0, 0, &V);       // X
        V.lVal = V1.y;
        Set2DArrayIndex(pSafe, 0, 1, &V);       // Y
        V.lVal = V2.x;
        Set2DArrayIndex(pSafe, 1, 0, &V);       // X
        V.lVal = V2.y;
        Set2DArrayIndex(pSafe, 1, 1, &V);       // Y
        V.lVal = V3.x;
        Set2DArrayIndex(pSafe, 2, 0, &V);       // X
        V.lVal = V3.y;
        Set2DArrayIndex(pSafe, 2, 1, &V);       // Y
        V.lVal = V4.x;
        Set2DArrayIndex(pSafe, 3, 0, &V);       // X
        V.lVal = V4.y;
        Set2DArrayIndex(pSafe, 3, 1, &V);       // Y
    }

	return Ok;
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzGetObjects
//      Retrieves all objects found using WoolzFindObjects, returning the area
//      orientation and min width rectangle for each object
// VB Script Syntax:
//      NumObjects = WoolzGetObjects(ObjectData)
// Parameters:
//      ObjectData is a 2 d array where the elements of the array are set as follows:
//          Element 0 = Area of the object
//          Element 1 = Width (TRUE bounding box)
//          Element 2 = Height (TRUE bounding box)
//          Element 3 = XMin of the bounding box
//          Element 4 = YMin of the bounding box
//          Element 5 = XMax of the bounding box
//          Element 6 = Ymax of the bounding box
//          Element 7 = Min grey level in object
//          Element 8 = Max grey level in object
//          Element 9 = Mean grey level in object
//          Element 10 = An X Pos on the object
//          Element 11 = An Y Pos on the object
// Returns:
//      Number of objects 
// Notes:
//      The number of objects returned will be limited to the dimensions of the 
//      array passed as parameter ObjectData.
//      Objects must have previously found using WoolzFindObjects
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::WoolzGetObjects(VARIANT FAR* ObjectArray)
{
    BOOL        Ok;
    float       A;
    int         W, H;
    long        Dx, Dy;
    POINT       V1, V2, V3, V4;
    SAFEARRAY   *pSafe;                 // Safe array to put data into
    VARIANT     V;                      // Temp var for packaging data to put in array
    long        uB, lB;
    short       TotalObjects, i;
    int			XMin, XMax, YMin, YMax;
	long		MinG, MaxG, MeanG, X, Y;
    int         ObjArea;
	CWoolzIP::Bounds B;

    // Get array info
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(ObjectArray, &pSafe, 2), return 0);
    
    // Check the bounds of the array for the first index
    SafeArrayGetLBound(pSafe, 1, &lB);
    SafeArrayGetUBound(pSafe, 1, &uB);

    // Check the second dimension is = 12
    CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 2, 12), return 0);

    // Now use which ever the maximum number of objects is
    if (uB - lB > IP().WoolzLib()->GetNumObjects())
        TotalObjects = IP().WoolzLib()->GetNumObjects();
    else
        TotalObjects = (short) (uB - lB);

    // Now fill in the array with the measurements
    for (i = 0; i < TotalObjects; i++)
    {
        XMin = INT_MAX;
        YMin = INT_MAX;
        XMax = INT_MIN;
        YMax = INT_MIN;

        IP().WoolzLib()->GetObjectArea(i, &ObjArea);
        IP().WoolzLib()->ObjectGreyData(i, &MinG, &MaxG, &MeanG);

        // Call the lib function
        Ok = IP().WoolzLib()->MinWidthRectangle(i, &A, &V1, &V2, &V3, &V4);
        if (Ok)
        {
            // Calc Height and Width of the rectangle
            Dx = V1.x - V2.x;
            Dy = V1.y - V2.y;
            W = (int) sqrt(Dx * Dx + Dy * Dy + 0.1);
            Dx = V2.x - V3.x;
            Dy = V2.y - V3.y;
            H = (int) sqrt(Dx * Dx + Dy * Dy + 0.1);

            // Calc bounding box
			IP().WoolzLib()->BoundingBox(i, B);

            // Set the array up
            V_VT(&V) = VT_I4;
            V.lVal = ObjArea;
            Set2DArrayIndex(pSafe, i, 0, &V);       // Area

            // Width and Height NB Different to the bounding box
            // width and height - this is the TRUE width and height
            V.lVal = W;
            Set2DArrayIndex(pSafe, i, 1, &V);       
            V.lVal = H;
            Set2DArrayIndex(pSafe, i, 2, &V);       // 
			V.lVal = B.Xmin;
            Set2DArrayIndex(pSafe, i, 3, &V);       // Bounding Box
            V.lVal = B.Ymin;
            Set2DArrayIndex(pSafe, i, 4, &V);       // 
            V.lVal = B.Xmax;
            Set2DArrayIndex(pSafe, i, 5, &V);       // 
            V.lVal = B.Ymax;
            Set2DArrayIndex(pSafe, i, 6, &V);       // 
            V.lVal = MinG;
            Set2DArrayIndex(pSafe, i, 7, &V);       // Min Grey
            V.lVal = MaxG;
            Set2DArrayIndex(pSafe, i, 8, &V);       // Max Grey
            V.lVal = MeanG;
            Set2DArrayIndex(pSafe, i, 9, &V);       // Mean Grey
			IP().WoolzLib()->GetPoint(i, &X, &Y);			// Get a point on the object
			V.lVal = X;
            Set2DArrayIndex(pSafe, i,10, &V);       // An X Pos on the object
			V.lVal = Y;
            Set2DArrayIndex(pSafe, i,11, &V);       // An Y Pos on the object
       }
    }

	return TotalObjects;
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzGetObjectGreyStats
//      Retrieves all objects found using WoolzFindObjects, returning the grey stats.
//      
// VB Script Syntax:
//      NumObjects = WoolzGetObjectGreyStats(ObjectData)
// Parameters:
//      ObjectData is a 2 d array where the elements of the array are set as follows:
//          Element 0 = Mean
//          Element 1 = Median
//          Element 2 = Mode
//			Element 3 = ModeCount
//          Element 4 = Variance
//			Element 5 = Skewness
// Returns:
//      Number of objects 
// Notes:
//      The number of objects returned will be limited to the dimensions of the 
//      array passed as parameter ObjectData.
//      Objects must have previously found using WoolzFindObjects
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::WoolzGetObjectGreyStats(VARIANT FAR* ObjectArray)
{
    int TotalObjects = 0;
    SAFEARRAY   *pSafe;                 // Safe array to put data into
    long        uB, lB;

    // Get array info
    CHECK_SAFEARR_EXCEPTION(GetArrayInfo(ObjectArray, &pSafe, 2), return 0);
    
    // Check the bounds of the array for the first index
    SafeArrayGetLBound(pSafe, 1, &lB);
    SafeArrayGetUBound(pSafe, 1, &uB);

    // Check the second dimension is = 6
    CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 2, 6), return 0);

    // Now use which ever the maximum number of objects is
    if (uB - lB > IP().WoolzLib()->GetNumObjects())
        TotalObjects = IP().WoolzLib()->GetNumObjects();
    else
        TotalObjects = (short) (uB - lB);

    // Now fill in the array with the measurements
    for (int i = 0; i < TotalObjects; i++)
    {
		long Min, Max, Mean, Median, Mode, ModeCount, Variance, Skew;
        IP().WoolzLib()->ObjectGreyDataStats(i, &Min, &Max, &Mean, &Median, &Mode, &ModeCount, &Variance, &Skew);

        // Set the array up
		VARIANT V;                      // Temp var for packaging data to put in array
        V_VT(&V) = VT_I4;
        V.lVal = Mean;
        Set2DArrayIndex(pSafe, i, 0, &V);
        V.lVal = Median;
        Set2DArrayIndex(pSafe, i, 1, &V);
        V.lVal = Mode;
        Set2DArrayIndex(pSafe, i, 2, &V);
        V.lVal = ModeCount;
        Set2DArrayIndex(pSafe, i, 3, &V);
        V.lVal = Variance;
        Set2DArrayIndex(pSafe, i, 4, &V);
        V.lVal = Skew;
        Set2DArrayIndex(pSafe, i, 5, &V);
	}

	return TotalObjects;
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzDrawObject
//      Draws an object found by WoolzFindObjects in an image
// VB Script Syntax:
//      WoolzDrawObject ImageHandle, objectIndex
// Parameters:
//      ImageHandle is the handle of the UTS image
//      objectIndex is the index of the object to draw
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void WoolzScript::WoolzDrawObject(long SrcHandle, long objectIndex) 
{
    long    W, H, BPP, P;
    BYTE    *ImageAddr;
    
    // Check for a valid image	
    if (SrcHandle > 0)
    {
        // Get the addr of the image
        BPP = IP().UTSMemRect()->GetExtendedInfo(SrcHandle, &W, &H, &P, &ImageAddr);
        if (BPP != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
        else
        {
            // Look for the woolz objects
            IP().WoolzLib()->DrawObject(objectIndex, ImageAddr, P, H);
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzFillObject
//      Draws an object found by WoolzFindObjects in an image with the specified grey level
// VB Script Syntax:
//      WoolzFillObject ImageHandle, objectIndex, GreyLevel
// Parameters:
//      ImageHandle is the handle of the UTS image
//      objectIndex is the index of the object to draw
//      GreyLevel is the grey level to use
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void WoolzScript::WoolzFillObject(long SrcHandle, long objectIndex, short FillValue) 
{
    long    W, H, BPP, P;
    BYTE    *ImageAddr;
    
    // Check for a valid image	
    if (SrcHandle > 0)
    {
        // Get the addr of the image
        BPP = IP().UTSMemRect()->GetExtendedInfo(SrcHandle, &W, &H, &P, &ImageAddr);
        if (BPP != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
        else
        {
            // Look for the woolz objects
            IP().WoolzLib()->FillObject(objectIndex, ImageAddr, P, H, FillValue);
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid
}

void WoolzScript::WoolzFillObjectXY(long SrcHandle, long objectIndex, int x, int y, short FillValue) 
{
    long    W, H, BPP, P;
    BYTE    *ImageAddr;
    
    // Check for a valid image	
    if (SrcHandle > 0)
    {
        // Get the addr of the image
        BPP = IP().UTSMemRect()->GetExtendedInfo(SrcHandle, &W, &H, &P, &ImageAddr);
        if (BPP != EIGHT_BITS_PER_PIXEL)
            AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
        else
        {
            // Look for the woolz objects
            IP().WoolzLib()->FillObjectXY(objectIndex, ImageAddr, x, y, P, H, FillValue);
        }
    }
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzCreateMaskFast
//      Create a UTS mask from a woolz object
// VB Script Syntax:
//      WoolzCreateMaskFast objectIndex, Mask
// Parameters:
//      objectIndex the object found using WoolzFindObjects
//      MaskOut is a UTS created 8 BIT image, created before the call to this function
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void WoolzScript::WoolzCreateMaskFast(long objectIndex, long Mask) 
{
    long BPP, W, H;

    BPP = IP().UTSMemRect()->GetRectInfo(Mask, &W, &H);
    if (BPP != EIGHT_BITS_PER_PIXEL)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
    else
    if (!IP().WoolzLib()->CreateMaskFast(objectIndex, Mask))
        AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzCreateMaskFromMerged
//      Create a UTS mask from merged woolz objects
// VB Script Syntax:
//      WoolzCreateMaskFromMerged objectIndex, Mask
// Parameters:
//      objectIndex the object found using WoolzFindObjects
//      MaskOut is a UTS created 8 BIT image, created before the call to this function
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void WoolzScript::WoolzCreateMaskFromMerged(long objectIndex, long Mask) 
{
    long BPP, W, H, Pitch;
    BYTE *Addr;

    BPP = IP().UTSMemRect()->GetExtendedInfo(Mask, &W, &H, &Pitch, &Addr);
    if (BPP != EIGHT_BITS_PER_PIXEL)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
    else
    if (!IP().WoolzLib()->CreateMaskFromMerged(objectIndex, Pitch, H, Addr))
        AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzCreateMask
//      Create a UTS mask from a woolz object
// VB Script Syntax:
//      WoolzCreateMask objectIndex, Mask
// Parameters:
//      objectIndex the object found using WoolzFindObjects
//      MaskOut is a UTS created mask, created before the call to this function
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void WoolzScript::WoolzCreateMask(long objectIndex, long Mask) 
{
    long BPP, W, H;

    BPP = IP().UTSMemRect()->GetRectInfo(Mask, &W, &H);
    if (BPP != ONE_BIT_PER_PIXEL)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
    else
	if (!IP().WoolzLib()->CreateMask(objectIndex, Mask))
        AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzCreateMask
//      Create a UTS mask from all objects found
// VB Script Syntax:
//      WoolzMaskFromAllObjects Mask
// Parameters:
//      MaskOut is a UTS created mask, created before the call to this function
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void WoolzScript::WoolzMaskFromAllObjects(long MaskOut) 
{
    long BPP, W, H;

    BPP = IP().UTSMemRect()->GetRectInfo(MaskOut, &W, &H);
    if (BPP != ONE_BIT_PER_PIXEL)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
    else
	if (!IP().WoolzLib()->CreateMaskFromAllObjects(MaskOut))
        AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzMaskFromLabelledObjects
//      Create a UTS mask from all objects found
// VB Script Syntax:
//      WoolzMaskFromLabelledObjects Label, Mask
// Parameters:
//      Label is the set of objects to use to create the mask
//      MaskOut is a UTS created mask, created before the call to this function
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void WoolzScript::WoolzMaskFromLabelledObjects(long Label, long MaskOut)
{
    long BPP, W, H;

    BPP = IP().UTSMemRect()->GetRectInfo(MaskOut, &W, &H);
    if (BPP != ONE_BIT_PER_PIXEL)
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
    else
	if (!IP().WoolzLib()->CreateMaskFromLabelledObjects(Label, MaskOut))
        AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzMaskFromErodedLabelledObjects
//      Create a UTS mask from eroded versions of all objects with the given label,
//      without modifying any of the objects in the object list.
// VB Script Syntax:
//      WoolzMaskFromErodedLabelledObjects label, hMask
// Parameters:
//      label is the label number indicating which objects to use
//      hMask is a UTS created mask, created before the call to this function,
//      that receives the output data
////////////////////////////////////////////////////////////////////////////////////////
void WoolzScript::WoolzMaskFromErodedLabelledObjects(long label, long hMask)
{
	long w, h;
	long bpp = IP().UTSMemRect()->GetRectInfo(hMask, &w, &h);
	if (bpp != ONE_BIT_PER_PIXEL)
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
	else
		IP().WoolzLib()->CreateMaskFromErodedLabelledObjects(label, hMask);
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzLabelObjectsIntersectingMask
//      Label all objects intersecting the mask
// VB Script Syntax:
//      WoolzLabelObjectsIntersectingMask hMask, label
// Parameters:
//      hMask is a UTS created mask, created before the call to this function
//      label is the label number to assign to the objects that intersect the mask
////////////////////////////////////////////////////////////////////////////////////////
void WoolzScript::WoolzLabelObjectsIntersectingMask(long hMask, long label)
{
	long w, h;
	long bpp = IP().UTSMemRect()->GetRectInfo(hMask, &w, &h);
	if (bpp != ONE_BIT_PER_PIXEL)
		AfxThrowOleDispatchException(0xFF, IDS_INVALIDIMAGE, 0);        // Image not correct bit depth
	else
	{
		long hTemp = IP().UTSMemRect()->CreateAs(hMask);
		if (hTemp < 1)
			AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
		else
		{
			// Copy the src image into the temp one and flip.
			IP().UTSMemRect()->Move(hMask, hTemp);
			IP().UTSMemRect()->InvertRows(hTemp);

			IP().WoolzLib()->LabelObjectsIntersectingMask(hTemp, label);

			// Free the temporary image.
			IP().UTSMemRect()->FreeImage(hTemp);
		}
	}
}


////////////////////////////////////////////////////////////////////////////////////////
// WoolzGreyData
//      Create a UTS mask from a woolz object
// VB Script Syntax:
//      WoolzGreyData objectIndex, MinGrey, MaxGrey, MeanGrey
// Parameters:
//      objectIndex the object found using WoolzFindObjects
//      MinGrey is the min grey level in the object
//      MaxGrey is the max grey level in the object
//      MeanGrey is the mean grey level value
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void WoolzScript::WoolzGreyData(long objectIndex, VARIANT FAR* MinGrey, VARIANT FAR* MaxGrey, VARIANT FAR* MeanGrey) 
{
	long    Min, Max, Mean;

    IP().WoolzLib()->ObjectGreyData(objectIndex, &Min, &Max, &Mean);

    V_VT(MaxGrey) = VT_I4;
    MaxGrey->lVal = Max;
    V_VT(MinGrey) = VT_I4;
    MinGrey->lVal = Min;
    V_VT(MeanGrey) = VT_I4;
    MeanGrey->lVal = Mean;
}

////////////////////////////////////////////////////////////////////////////////////////
// LabelWoozObject
//      Attaches a label or marker to an object in the woolz list
// VB Script Syntax:
//      LabelWoolzObject objectIndex, LabelValue
// Parameters:
//      objectIndex is the object found using WoolzFindObjects
//      LabelValue is the value to be associated with the object
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void WoolzScript::LabelWoolzObject(long objectIndex, long LabelValue) 
{
    IP().WoolzLib()->LabelObject(objectIndex, LabelValue);
}

////////////////////////////////////////////////////////////////////////////////////////
// GetWoolzObjectLabel
//      Attaches a label or marker to an object in the woolz list
// VB Script Syntax:
//      LabelValue = GetWoolzObjectLabel(objectIndex)
// Parameters:
//      objectIndex is the object found using WoolzFindObjects
// Returns:
//      LabelValue is the value to be associated with the object, 0 otherwise
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::GetWoolzObjectLabel(long objectIndex) 
{
	return IP().WoolzLib()->GetObjectLabel(objectIndex);
}

////////////////////////////////////////////////////////////////////////////////////////
// Counts separately labelled objects
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::CountLabelledObjects() 
{
	return IP().WoolzLib()->CountLabelledObjects();
}

////////////////////////////////////////////////////////////////////////////////////////

BOOL WoolzScript::InternalWoolzMeasureObject(long objectIndex, int measurementSet, BOOL extendedMeasurements,
                                             VARIANT FAR *measurements)
{
// The measurementSet indicates which set of alternative formula to use
// for calculating the measurements.
// If extendedMeasurements is FALSE, 15 measurements are returned.
// If extendedMeasurements is TRUE, 20 measurements are returned.
	BOOL status = FALSE;

	// Get array info
	SAFEARRAY *pSafe;
	CHECK_SAFEARR_EXCEPTION(GetArrayInfo(measurements, &pSafe, 1), return FALSE);
	
	if (extendedMeasurements) // Check the dimension is = 20 or more
	{
		CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 1, 20), return FALSE);
	}
	else // Check the dimension is 15 or more.
	{
		CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 1, 15), return FALSE);
	}


	WoolzMeasurements WM;
    if (IP().WoolzLib()->MeasureObject(objectIndex, &WM, measurementSet))
	{
		VARIANT V;
		// Set the type of each element to double even if they aren't
		V_VT(&V) = VT_R8;
		V.dblVal = WM.Area;
		Set1DArrayIndex(pSafe, 0, &V);		// Area
		V.dblVal = WM.COGx;
		Set1DArrayIndex(pSafe, 1, &V);		// COG x
		V.dblVal = WM.COGy;
		Set1DArrayIndex(pSafe, 2, &V);		// COG y
		V.dblVal = WM.BBLeft;
		Set1DArrayIndex(pSafe, 3, &V);		// Left (Bounding Box)
		V.dblVal = WM.BBTop;				
		Set1DArrayIndex(pSafe, 4, &V);		// Top
		V.dblVal = WM.BBRight;
		Set1DArrayIndex(pSafe, 5, &V);		// Right
		V.dblVal = WM.BBBottom;
		Set1DArrayIndex(pSafe, 6, &V);		// Bottom
        // NB 1000.0 multiplier only to keep compatibility with
        // existing UTS stuff - i.e. UTS 1 scripting had no 
        // floating point support !!!!!!!!!!!!!!!!!!!!!!!!!!!
		V.dblVal = WM.AxisRatio * 1000.0;
		Set1DArrayIndex(pSafe, 7, &V);		// Axis Ratio
		V.dblVal = WM.Compactness * 1000.0;				
		Set1DArrayIndex(pSafe, 8, &V);		// Compactness
		V.dblVal = WM.MinRadii * 2.0;				
		Set1DArrayIndex(pSafe, 9, &V);		// CLP Min  
		V.dblVal = WM.MaxRadii * 2.0;				
		Set1DArrayIndex(pSafe, 10, &V);		// CLP Max  
		V.dblVal = WM.MaxRadii;				
		Set1DArrayIndex(pSafe, 11, &V);		// Max Radius (Compatibility with old UTS)
		V.dblVal = WM.Circularity * 1000.0;				
		Set1DArrayIndex(pSafe, 12, &V);		// Circularity	
		V.dblVal = WM.Perimeter;
		Set1DArrayIndex(pSafe, 13, &V);		// Perimeter length
		V.dblVal = WM.EnclosedArea;
		Set1DArrayIndex(pSafe, 14, &V);		// Area enclosed by object boundary
		
		if (extendedMeasurements)
		{
			V.dblVal = WM.COGx;		
			Set1DArrayIndex(pSafe, 15, &V);		// Seeds for reconstruction
			V.dblVal = WM.COGy;
			Set1DArrayIndex(pSafe, 16, &V);	
			V.dblVal = WM.Width;                // TRUE Width and height of min width rectangle          
			Set1DArrayIndex(pSafe, 17, &V);		
			V.dblVal = WM.Height;
			Set1DArrayIndex(pSafe, 18, &V);	
			V.dblVal = WM.Orientation;
			Set1DArrayIndex(pSafe, 19, &V);     // Angle of rotation			
		}

		status = TRUE;
	}
	else
	{
		// ReSet the type of each element
		VARIANT V;		
		V_VT(&V) = VT_R8;
		V.dblVal = 0;
        for (int i = 0; i < (extendedMeasurements ? 20 : 15); i++)
		    Set1DArrayIndex(pSafe, i, &V);		
	}

	return status;
}


////////////////////////////////////////////////////////////////////////////////////////
// WoolzMeasureObject  
//		Takes shape measurements for an object found using WoolzFindObjects
// VB Script Syntax:
//		WoolzMeasureObject objectIndex, Measurements
// Parameters:
//      objectIndex is the object found using WoolzFindObjects
//		Measurements is an array of 15 doubles storing the shape measures for the object
// Returns:
//		TRUE if object index is a valid object
////////////////////////////////////////////////////////////////////////////////////////

BOOL WoolzScript::WoolzMeasureObject(long objectIndex, VARIANT FAR *measurements) 
{
	return InternalWoolzMeasureObject(objectIndex, 1, FALSE, measurements);
}

BOOL WoolzScript::WoolzMeasureObject2(long objectIndex, VARIANT FAR *measurements) 
{
	return InternalWoolzMeasureObject(objectIndex, 2, FALSE, measurements);
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzMeasureObjectEx
//		Takes shape measurements for an object found using WoolzFindObjects - 
//      extended version of the above
// VB Script Syntax:
//		WoolzMeasureObjectEx objectIndex, Measurements
// Parameters:
//      objectIndex is the object found using WoolzFindObjects
//		Measurements is an array of 20 doubles storing the shape measures for the object
// Returns:
//		TRUE if object index is a valid object
////////////////////////////////////////////////////////////////////////////////////////

BOOL WoolzScript::WoolzMeasureObjectEx(long objectIndex, VARIANT FAR* measurements) 
{
	return InternalWoolzMeasureObject(objectIndex, 1, TRUE, measurements);
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzMeasureMergedObject
//		Takes shape measurements for an object found using WoolzFindObjectsAndMerge - 
//      extended version which merges objects within a certain proximity
// VB Script Syntax:
//		WoolzMeasureMergedObject objectIndex, Measurements
// Parameters:
//      objectIndex is the object found using WoolzFindObjects
//		Measurements is an array of 20 doubles storing the shape measures for the object
// Returns:
//		TRUE if object index is a valid object
////////////////////////////////////////////////////////////////////////////////////////

BOOL WoolzScript::WoolzMeasureMergedObject(long objectIndex, VARIANT FAR* Measurements) 
{
	WoolzMeasurements WM;
    SAFEARRAY   *pSafe;                 // Safe array to put data into
    VARIANT     V;                      // Temp var for packaging data to put in array
    BOOL        Ok = FALSE;
    int         i;
	BOOL		M;

	try
	{
		M = IP().WoolzLib()->MeasureMergedObject(objectIndex, &WM);
	}
	catch (...)
	{
		M = FALSE;
	}

    if (M)
	{		
        CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Measurements, &pSafe, 1), return FALSE);
    
		// Check the dimension is = 20 or more
		CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 1, 20), return FALSE);

		// Set the type of each element to double even if they aren't
		V_VT(&V) = VT_R8;
		V.dblVal = WM.Area;
		Set1DArrayIndex(pSafe, 0, &V);		// Area
		V.dblVal = WM.COGx;
		Set1DArrayIndex(pSafe, 1, &V);		// COG x
		V.dblVal = WM.COGy;
		Set1DArrayIndex(pSafe, 2, &V);		// COG y
		V.dblVal = WM.BBLeft;
		Set1DArrayIndex(pSafe, 3, &V);		// Left (Bounding Box)
		V.dblVal = WM.BBTop;				
		Set1DArrayIndex(pSafe, 4, &V);		// Top
		V.dblVal = WM.BBRight;
		Set1DArrayIndex(pSafe, 5, &V);		// Right
		V.dblVal = WM.BBBottom;
		Set1DArrayIndex(pSafe, 6, &V);		// Bottom
        // NB 1000.0 multiplier only to keep compatibility with
        // existing UTS stuff - i.e. UTS 1 scripting had no 
        // floating point support !!!!!!!!!!!!!!!!!!!!!!!!!!!
		V.dblVal = WM.AxisRatio * 1000.0;
		Set1DArrayIndex(pSafe, 7, &V);		// Axis Ratio
		V.dblVal = WM.Compactness * 1000.0;				
		Set1DArrayIndex(pSafe, 8, &V);		// Compactness
		V.dblVal = WM.MinRadii * 2.0;				
		Set1DArrayIndex(pSafe, 9, &V);		// Min Radius 
		V.dblVal = WM.MaxRadii * 2.0;				
		Set1DArrayIndex(pSafe, 10, &V);		// Max Radius 
		V.dblVal = WM.MaxRadii;				
		Set1DArrayIndex(pSafe, 11, &V);		// Max Radius (Compatibility with old UTS)
		V.dblVal = WM.Circularity * 1000.0;				
		Set1DArrayIndex(pSafe, 12, &V);		// Circularity	
		V.dblVal = WM.Perimeter;
		Set1DArrayIndex(pSafe, 13, &V);		// Perimeter length
		V.dblVal = WM.EnclosedArea;
		Set1DArrayIndex(pSafe, 14, &V);		// Area enclosed by object boundary
        V.dblVal = WM.COGx;
		Set1DArrayIndex(pSafe, 15, &V);		// Seeds for reconstruction
        V.dblVal = WM.COGy;
		Set1DArrayIndex(pSafe, 16, &V);	
        V.dblVal = WM.Width;                // TRUE Width and height of min width rectangle          
		Set1DArrayIndex(pSafe, 17, &V);		
        V.dblVal = WM.Height;
		Set1DArrayIndex(pSafe, 18, &V);	
        V.dblVal = WM.Orientation;
		Set1DArrayIndex(pSafe, 19, &V);     // Angle of rotation	

		Ok = TRUE;
	}
	else
	{
		CHECK_SAFEARR_EXCEPTION(GetArrayInfo(Measurements, &pSafe, 1), return FALSE);
		// ReSet the type of each element
		V_VT(&V) = VT_R8;
		V.dblVal = 0;
        for (i = 0; i < 20; i++)
		    Set1DArrayIndex(pSafe, i, &V);		
	}
	
	return Ok;
}

////////////////////////////////////////////////////////////////////////////////
//
// Note: Dimension the object array first (using the number of objects in the list,
// as returned by WoolzFindObjects()
//
long WoolzScript::WoolzGetObjectList(VARIANT FAR* ObjectList) 
{
	SAFEARRAY *pSafe;	// Safe array to put data into
    VARIANT V;			// Temp var for packaging data to put in array
    BOOL Ok = FALSE;
    int i;
	int nobjs;
	void **obj_array;

	nobjs = IP().WoolzLib()->GetNumObjects();
	obj_array = new void *[nobjs];
	nobjs = IP().WoolzLib()->GetWoolzObjects(obj_array);

	// Get array info
	CHECK_SAFEARR_EXCEPTION(GetArrayInfo(ObjectList, &pSafe, 1), return FALSE);

    // Check the dimension is able to hold found objects list
	CHECK_SAFEARR_EXCEPTION(CheckBounds(pSafe, 1, nobjs), return FALSE);

	// Get the objects
	for (i = 0; i < nobjs; i++)
	{
		// Set the type of each element to long
		V_VT(&V) = VT_I4;
		V.lVal = (long)obj_array[i];
		Set1DArrayIndex(pSafe, i, &V);
	}

	return nobjs;
}

////////////////////////////////////////////////////////////////////////////////
//
long WoolzScript::WoolzSetObjectList(VARIANT FAR* ObjectList) 
{
    SAFEARRAY *pSafe;	// Safe array to put data into
    VARIANT V;			// Temp var for packaging data to put in array
    BOOL Ok = FALSE;
    long i;
    long lB, uB;
	void **op_array = NULL;
	long nobjs = 0;

	if (ObjectList->vt == VT_EMPTY)
		return 0;

	// Get array info
	CHECK_SAFEARR_EXCEPTION(GetArrayInfo(ObjectList, &pSafe, 1), return FALSE);

	// Get bounds of array lwb etc etc
    SafeArrayGetLBound(pSafe, 1, &lB);
    SafeArrayGetUBound(pSafe, 1, &uB);

    if ((uB - lB) > 0)
    {
		// Construct simple object pointer array
		op_array = new void *[uB - lB + 1];

        // Copy data across
        if (op_array)
        {
            // Copy the data into our array
            for (i = lB; i < uB; i++)
            {
                SafeArrayGetElement(pSafe, &i, &V);
                op_array[i - lB] = (void *)V.lVal; //.byref;//lVal;
            }
        }
        else
            AfxThrowOleDispatchException(0xFF, IDS_MEMALLOC, 0);       // No memory allocation

		// Terminate op_array
		op_array[uB] = NULL;

		// Now set the array
		nobjs = IP().WoolzLib()->SetWoolzObjects(op_array);
	}

	return nobjs;
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzSplitMask
//      Splits a mask into three. One mask with objects in the range specified by
//      MinArea and MaxArea, one with objects below min area and one with objects above
//      MaxArea.
// VB Script syntax:
//      WoolzSplitMask RangeImage, MinMask, MaxMask, MinArea, MaxArea, NumInRange, NumInMin, NumInMax
// Parameters:
//      RangeImage   - this is the source image and will contain objects within the 
//                     range MinArea to MaxArea on exit.
//      MinMask      - this image contains all objects < MinArea in area
//      MaxMask      - this image contains all objects > MaxArea in area
//		MinArea		 - min range limit
//		MaxArea		 - max range limit
//      NumInRange   - returned number of objects in RangeImage
//      NumInMin     - returned number of objects in MinMaskImage
//      NumInMax     - returned number of objects in MaxMaskImage
////////////////////////////////////////////////////////////////////////////////////////

void WoolzScript::WoolzSplitMask(long RangeImage, long MinMask, long MaxMask, long MinArea, long MaxArea, VARIANT FAR* NumInMin, VARIANT FAR* NumInRange, VARIANT FAR* NumInMax) 
{
    long NumR, NumMin, NumMax;

    IP().WoolzLib()->WoolzSplitMask(RangeImage, MinMask, MaxMask, MinArea, MaxArea, &NumMin, &NumR, &NumMax);

    V_VT(NumInRange) = VT_I4;
    V_VT(NumInMin) = VT_I4;
    V_VT(NumInMax) = VT_I4;

    NumInRange->lVal = NumR;
    NumInMin->lVal = NumMin;
    NumInMax->lVal = NumMax;
}


////////////////////////////////////////////////////////////////////////////////////////
// WoolzSplitMask
// VB Script syntax:
//      NumInRange = WoolzSplitMaskFast(RangeImage, MinArea, MaxArea)
// Parameters:
//      RangeImage   - this is the source image and will contain objects within the 
//                     range MinArea to MaxArea on exit.
//		MinArea		 - min range limit
//		MaxArea		 - max range limit
// Returns:
//      NumInRange   - returned number of objects in RangeImage
////////////////////////////////////////////////////////////////////////////////////////


long WoolzScript::WoolzSplitMaskFast(long RangeImage, long MinArea, long MaxArea) 
{
    return IP().WoolzLib()->WoolzSplitMask(RangeImage, MinArea, MaxArea);
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzRemoveEdgeObjects
// VB Script syntax:
//      NumInRange = WoolzRemoveEdgeObjects(RangeImage)
// Parameters:
//      RangeImage   - this is the source image and will contain objects not touching
//                     the edges of the image
//      Borderwidth  - Border round the edge of the image which is used for object exclusion
// Returns:
//      NumInRange   - returned number of objects left
////////////////////////////////////////////////////////////////////////////////////////


long WoolzScript::WoolzRemoveEdgeObjects(long RangeImage, long BorderWidth) 
{
    return IP().WoolzLib()->WoolzRemoveEdgeObjects(RangeImage, BorderWidth);
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzNumberOfComponents
//      Just returns the number of objects and the area of the largest object
// VB script syntax:
//      NumberOfComponents = WoolzNumberOfComponents(RangeImage, MaxArea)
// Parameters:
//      RangeImage   - this is the source image and will contain objects within the 
//                     range MinArea to MaxArea on exit.
//		MaxArea		 - returned largest object area
// Returns:
//      Number of objects in the image
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::WoolzNumberOfComponents(long SrcImage, VARIANT FAR* MaxArea) 
{
	long MA, N;

    N = IP().WoolzLib()->WoolzNumberOfComponents(SrcImage, &MA);

    V_VT(MaxArea) = VT_I4;
    MaxArea->lVal = MA;

	return N;
}

////////////////////////////////////////////////////////////////////////////////////////
// ObjectGreysInImage
//      Given a Woolz object, find grey level stats under the object in the 
//      image specified
// VB script syntax:
//      ObjectGreysInImage objectIndex, SrcImage, MinGrey, MaxGrey, MeanGrey
// Parameters:
//      objectIndex is the index of the object found with WoolzFindObjects
//      SrcImage is the image to measure grey level data from
//      MinGrey is the minimum grey level
//      MaxGrey is the maximum grey level
//      MeanGrey is the mean grey level found
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////


void WoolzScript::ObjectGreysInImage(long objectIndex, long SrcImage, VARIANT FAR* MinGrey, VARIANT FAR* MaxGrey, VARIANT FAR* MeanGrey) 
{
    long Min, Max, Mean;
    long W, H, P;
    BYTE *Image;

    if (IP().UTSMemRect()->GetExtendedInfo(SrcImage, &W, &H, &P, &Image))
    {
        IP().WoolzLib()->ObjectGreyDataInImage(objectIndex, Image, P, H, &Min, &Max, &Mean);
        
        V_VT(MinGrey) = VT_I4;
        V_VT(MaxGrey) = VT_I4;
        V_VT(MeanGrey) = VT_I4;
        MinGrey->lVal = Min;
        MaxGrey->lVal = Max;
        MeanGrey->lVal = Mean;
    }
}

////////////////////////////////////////////////////////////////////////////////////////
// Fractal boundary measurement
////////////////////////////////////////////////////////////////////////////////////////

double WoolzScript::FractalBoundary(short ObjectIndex, short Spacing)
{
    return IP().WoolzLib()->FractalMeasureBoundary(ObjectIndex, Spacing);
}


////////////////////////////////////////////////////////////////////////////////////////
// MergedObjectGreysInImage
//      Given a Woolz object, find grey level stats under the object in the 
//      image specified
// VB script syntax:
//      MergedObjectGreysInImage objectIndex, SrcImage, MinGrey, MaxGrey, MeanGrey
// Parameters:
//      objectIndex is the index of the object found with WoolzFindObjects
//      SrcImage is the image to measure grey level data from
//      MinGrey is the minimum grey level
//      MaxGrey is the maximum grey level
//      MeanGrey is the mean grey level found
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////


void WoolzScript::MergedObjectGreysInImage(long objectIndex, long SrcImage, VARIANT FAR* MinGrey, VARIANT FAR* MaxGrey, VARIANT FAR* MeanGrey) 
{
    long Min, Max, Mean;
    long W, H, P;
    BYTE *Image;

    if (IP().UTSMemRect()->GetExtendedInfo(SrcImage, &W, &H, &P, &Image))
    {
        IP().WoolzLib()->MergedObjectGreyDataInImage(objectIndex, Image, P, H, &Min, &Max, &Mean);
        
        V_VT(MinGrey) = VT_I4;
        V_VT(MaxGrey) = VT_I4;
        V_VT(MeanGrey) = VT_I4;
        MinGrey->lVal = Min;
        MaxGrey->lVal = Max;
        MeanGrey->lVal = Mean;
    }
}

////////////////////////////////////////////////////////////////////////////////////////
// Returns a list of boundary points for an object
// VB script syntax:
//      NPoints = GetBoundary(objectIndex, Boundary)
// Parameters:
//      objectIndex is the index of the object to get the boundary for
//      BoundaryArray is an array (which will be redimensioned) of npoints of x,y points
// Returns:
//      Number of points in the boundary
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::GetBoundary(long objectIndex, VARIANT FAR* BoundaryArray) 
{
    long *BPoints = NULL;
    int NPoints, i;
    SAFEARRAY   *pSafe;
    VARIANT     V;
    long        *Bptr;
    SAFEARRAYBOUND  SAB[2];

    // Array points set to longs
    V_VT(&V) = VT_I4;
    
    // Get a pointer to reference the array with
    if (BoundaryArray->vt & VT_BYREF)
        pSafe = *(BoundaryArray->pparray);
    else
        pSafe = BoundaryArray->parray;
    
    // Get the list of boundary points
    NPoints = IP().WoolzLib()->MeasureBoundary(objectIndex, &BPoints);

    if (NPoints)
    {
        // Destroy any previous data
        SafeArrayDestroyData(pSafe);
    
        // Destroy its descriptor
        SafeArrayDestroyDescriptor(pSafe);
    
        // Allocate a new array
        SAB[0].lLbound = 0;
        SAB[0].cElements = NPoints;
        SAB[1].lLbound = 0;
        SAB[1].cElements = 2;
        pSafe = SafeArrayCreate(VT_VARIANT, 2, SAB);
        if (pSafe == NULL)
            AfxThrowOleDispatchException(0xFF, IDS_MEMALLOC, 0);

        Bptr = BPoints;

        // Fill in the boundary point array
        for (i = 0; i < NPoints; i++)
        {
            V.lVal = *Bptr++;
            Set2DArrayIndex(pSafe, i, 0, &V);
            V.lVal = *Bptr++;
            Set2DArrayIndex(pSafe, i, 1, &V);
        }

        // Free up memory
        delete BPoints;
    }

	return (long) NPoints;
}


////////////////////////////////////////////////////////////////////////////////////////
// Returns a list of boundary points for an object
// VB script syntax:
//      NPoints = GetBoundary(objectIndex, Boundary)
// Parameters:
//      objectIndex is the index of the object to get the boundary for
//      BoundaryArray is an array (which will be redimensioned) of npoints of x,y points
// Returns:
//      Number of points in the boundary
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::GetConnectionPoints(long objectIndex, double Dist, VARIANT FAR* BoundaryArray) 
{
    long *CPoints = NULL;
    int NPoints, i;
    SAFEARRAY   *pSafe;
    VARIANT     V;
    long        *Bptr;
    SAFEARRAYBOUND  SAB[2];

    // Array points set to longs
    V_VT(&V) = VT_I4;
    
    // Get a pointer to reference the array with
    if (BoundaryArray->vt & VT_BYREF)
        pSafe = *(BoundaryArray->pparray);
    else
        pSafe = BoundaryArray->parray;
    
    // Get the list of boundary points
	NPoints = IP().WoolzLib()->GetConnectionPoints(objectIndex, Dist, &CPoints);

    if (NPoints)
    {
        // Destroy any previous data
        SafeArrayDestroyData(pSafe);
    
        // Destroy its descriptor
        SafeArrayDestroyDescriptor(pSafe);
    
        // Allocate a new array
        SAB[0].lLbound = 0;
        SAB[0].cElements = NPoints;
        SAB[1].lLbound = 0;
        SAB[1].cElements = 2;
        pSafe = SafeArrayCreate(VT_VARIANT, 2, SAB);
        if (pSafe == NULL)
            AfxThrowOleDispatchException(0xFF, IDS_MEMALLOC, 0);

        Bptr = CPoints;

        // Fill in the boundary point array
        for (i = 0; i < NPoints; i++)
        {
            V.lVal = *Bptr++;
            Set2DArrayIndex(pSafe, i, 0, &V);
            V.lVal = *Bptr++;
            Set2DArrayIndex(pSafe, i, 1, &V);
        }

        // Free up memory
        delete CPoints;
    }

	return (long) NPoints;
}

////////////////////////////////////////////////////////////////////////////////////////
// Returns a list of boundary points for an object
// VB script syntax:
//      NPoints = GetBoundary(objectIndex, Boundary)
// Parameters:
//      objectIndex is the index of the object to get the boundary for
//      BoundaryArray is an array (which will be redimensioned) of npoints of x,y points
// Returns:
//      Number of points in the boundary
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::GetClosestConnectionPoints(double Dist, VARIANT FAR* BoundaryArray) 
{
    long *CPoints = NULL;
    int NPoints, i;
    SAFEARRAY   *pSafe;
    VARIANT     V;
    long        *Bptr;
    SAFEARRAYBOUND  SAB[2];

    // Array points set to longs
    V_VT(&V) = VT_I4;
    
    // Get a pointer to reference the array with
    if (BoundaryArray->vt & VT_BYREF)
        pSafe = *(BoundaryArray->pparray);
    else
        pSafe = BoundaryArray->parray;
    
    // Get the list of boundary points
	NPoints = IP().WoolzLib()->GetClosestConnectionPoints(Dist, &CPoints);

    if (NPoints)
    {
        // Destroy any previous data
        SafeArrayDestroyData(pSafe);
    
        // Destroy its descriptor
        SafeArrayDestroyDescriptor(pSafe);
    
        // Allocate a new array
        SAB[0].lLbound = 0;
        SAB[0].cElements = NPoints;
        SAB[1].lLbound = 0;
        SAB[1].cElements = 4;
        pSafe = SafeArrayCreate(VT_VARIANT, 2, SAB);
        if (pSafe == NULL)
            AfxThrowOleDispatchException(0xFF, IDS_MEMALLOC, 0);

        Bptr = CPoints;

        // Fill in the boundary point array
        for (i = 0; i < NPoints; i++)
        {
            V.lVal = *Bptr++;
            Set2DArrayIndex(pSafe, i, 0, &V);
            V.lVal = *Bptr++;
            Set2DArrayIndex(pSafe, i, 1, &V);
			 V.lVal = *Bptr++;
            Set2DArrayIndex(pSafe, i, 2, &V);
            V.lVal = *Bptr++;
            Set2DArrayIndex(pSafe, i, 3, &V);
        }

        // Free up memory
        delete CPoints;
    }

	return (long) NPoints;
}



////////////////////////////////////////////////////////////////////////////////////////
// Textural metfind 
//					 Clumpy bone	Blood + Wide Spreads
//	maxFeatureWidth		11					20				typical values
////////////////////////////////////////////////////////////////////////////////////////

afx_msg long WoolzScript::TextureMetFind(long SrcHandle, long MaxFeatureWidth)
{
	long W, H, P;
	BYTE *ImageAddr, *NewImageAddr = NULL;
	long NewHandle = UTS_ERROR, TempHandle = UTS_ERROR;

	// Check for a valid image	
	if (UTSVALID_HANDLE(SrcHandle))
	{
		// Get addresses
		IP().UTSMemRect()->GetExtendedInfo(SrcHandle, &W, &H, &P, &ImageAddr);
		IP().WoolzLib()->UnpadImage(SrcHandle);
		// Texture metfind using MGs algorithm from the WWW
		NewImageAddr = IP().WoolzLib()->TextureMetFind(ImageAddr, W, H, (int)MaxFeatureWidth);

		//If no NewImageAddr 
		if (NewImageAddr == NULL)
		{
			AfxThrowOleDispatchException(0xFF, IDS_IMAGEMEMALLOC, 0);
			return 0;
		}

		IP().WoolzLib()->PadImage(SrcHandle);
            
		NewHandle = IP().WoolzLib()->MakePaddedImage(SrcHandle, NewImageAddr);
		if (UTSVALID_HANDLE(NewHandle))
			delete NewImageAddr;
		else
		{
			if (NewImageAddr)
				delete NewImageAddr;        
			AfxThrowOleDispatchException(0xFF, IDS_OPERATIONFAILED, 0);     // Operation failed for some reason
		}
	}

	return NewHandle;
}

////////////////////////////////////////////////////////////////////////////////////////
// TODO: Explain what this does
////////////////////////////////////////////////////////////////////////////////////////

long WoolzScript::WoolzDeagglomerate(long SrcImage, long DstImage) 
{
	BYTE *SrcAddr, *DstAddr;
	long SrcWidth, SrcHeight, SrcPitch, SrcBPP;
	long DstWidth, DstHeight, DstPitch, DstBPP;

    if (UTSVALID_HANDLE(SrcImage))
	{
		if (UTSVALID_HANDLE(DstImage))
		{
			SrcBPP = IP().UTSMemRect()->GetExtendedInfo(SrcImage, &SrcWidth, &SrcHeight, &SrcPitch, &SrcAddr);
			DstBPP = IP().UTSMemRect()->GetExtendedInfo(DstImage, &DstWidth, &DstHeight, &DstPitch, &DstAddr);
			if ((SrcWidth == DstWidth)&&(SrcHeight == DstHeight)&&(SrcPitch == DstPitch)&&(SrcBPP == 1)&&(DstBPP == 1))
			{
				return IP().WoolzLib()->WoolzDeagglomerate(SrcImage, DstImage);
			}
			return 0;
		}
		else
			AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid
	}
    else
        AfxThrowOleDispatchException(0xFF, IDS_INVALIDHANDLE, 0);           // A handle is invalid

	return 0;
}

void WoolzScript::WoolzDeagglomerateObjects() 
{
	IP().WoolzLib()->Deagglomerate();
}


long WoolzScript::WoolzSplitObjects(long maxNeckWidthSplit, long minCurvatureSplit, long minArea)
{
	return IP().WoolzLib()->SplitObjects(maxNeckWidthSplit, minCurvatureSplit, minArea);
}



////////////////////////////////////////////////////////////////////////////////////////
//
//	WoolzConvexHull
//      Draws the convex hull of an object found by WoolzFindObjects in an image as binary object
// VB Script Syntax: (To be compatible with WoolzDrawObject)
//      WoolzConvexHull mask_image_out, object_number, fill_value
// Parameters:
//      mask_image_out is the handle of the UTS outpuit image mask
//      object_number is the index of the object to draw in the internal object list
//		fill_value is the pixel value to fill the binary mask with
// Returns:
//      TRUE if OK, or FALSE on error
////////////////////////////////////////////////////////////////////////////////////////
long WoolzScript::WoolzConvexHull(long mask_image_out, short object_number, short fill_value) 
{
	return IP().WoolzLib()->WoolzConvexHull(mask_image_out, object_number, (unsigned char)fill_value);
}

////////////////////////////////////////////////////////////////////////////////////////
//
// WoolzClusterObjects
//		Clusters the list of woolz objects together to create a (possibly smaller)
//		subset of objects.
// VB Script Syntax:
//		WoolzClusterObjects ThreshClusterDist
// Parameters:
//		ThreshClusterDist is the threshold distance,
//		beyond which objects are considered to be distinct
// Returns:
//		TRUE if OK, or FALSE on error
////////////////////////////////////////////////////////////////////////////////////////
long WoolzScript::WoolzClusterObjects(long thresh_cluster_dist) 
{
	return IP().WoolzLib()->WoolzClusterObjects(thresh_cluster_dist);
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzRemoveObject
//		Removes the object with the given index from the internal list
////////////////////////////////////////////////////////////////////////////////////////
void WoolzScript::WoolzRemoveObject(long index)
{
	IP().WoolzLib()->RemoveObject(index);
}


////////////////////////////////////////////////////////////////////////////////////////
// WoolzGetNumberOfObjects
////////////////////////////////////////////////////////////////////////////////////////
long WoolzScript::WoolzGetNumberOfObjects()
{
	return IP().WoolzLib()->GetNumObjects();
}


////////////////////////////////////////////////////////////////////////////////////////
// WoolzErodeAllObjects
////////////////////////////////////////////////////////////////////////////////////////
void WoolzScript::WoolzErodeLabelledObjects(long labelNumber)
{
	IP().WoolzLib()->ErodeLabelledObjects(labelNumber);
}
/*
////////////////////////////////////////////////////////////////////////////////////////
// WoolzDilateAllObjects
////////////////////////////////////////////////////////////////////////////////////////
void WoolzScript::WoolzDilateLabelledObjects(long labelNumber)
{
	WoolzLib.DilateLabelledObjects(labelNumber);
}
*/


