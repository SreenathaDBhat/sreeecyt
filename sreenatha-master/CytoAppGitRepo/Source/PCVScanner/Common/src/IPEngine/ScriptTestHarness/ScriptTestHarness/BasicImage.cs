using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using AICompressor;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace BasicBmpImaging
{
    class BasicImage
    {
        public enum ImageType
        {
            EmptyImage,
            RawImage,
            BitmapImage,
            JLSImage
        }

        public int Width, Height, BitsPerPixel, RawStride, BytesPerPixel;
        public byte[] Pixels = null;
        public ImageType Type = ImageType.EmptyImage;
        public String ImagePath;
       

        public BasicImage(String File)
        {
            Width = 0;
            Height = 0;
            BitsPerPixel = 0;
            BytesPerPixel = 0;

            ImagePath = File;
            Type = ImageType.EmptyImage;

            if (ImagePath.Contains(".raw"))
            {
                Type = ImageType.RawImage;
            }
            else
            if (ImagePath.Contains(".blb"))
            {
                Type = ImageType.RawImage;
            }
            else
            if (ImagePath.Contains(".stk"))
            {
                Type = ImageType.JLSImage;
            }
            else
            if (ImagePath.Contains(".bmp"))
            {
                Type = ImageType.BitmapImage;
            }
            else
            if (ImagePath.Contains(".jpg"))
            {
                Type = ImageType.BitmapImage;
            }
        }

        /// <summary>
        /// Return a full size bitmap
        /// </summary>
        public BitmapSource Bitmap()
        {
            BitmapSource bmp = null;
            switch (Type)
            {
                case ImageType.BitmapImage:                    
                    bmp = BMPFromFile(ImagePath);
                    break;

                case ImageType.RawImage:
                    bmp = RAWFromFile(ImagePath);
                    break;

                case ImageType.JLSImage:
                    bmp = JPGLSFromFile(ImagePath);
                    break;
            }

            return bmp;
        }

        public byte[] GetPixels()
        {
            BitmapSource BMS = Bitmap();

            Width  = BMS.PixelWidth;
            Height = BMS.PixelHeight;

            BitsPerPixel = BMS.Format.BitsPerPixel;
            BytesPerPixel = BitsPerPixel / 8;

            RawStride = (Width * BMS.Format.BitsPerPixel + 7) / 8;

            Pixels = new byte[RawStride * Height];

            BMS.CopyPixels(Pixels, RawStride, 0);

            return Pixels;
        }

        public String FilePath
        {
            get { return ImagePath; }
        }

        public String FileName
        {
            get { return Path.GetFileName(ImagePath); }
        }

        // Estimate image dimensions from the file size
        public static bool EstimateDimensions(long FileSz, out int W, out int H, out int BPP, out bool x)
        {
            W = 0;
            H = 0;
            BPP = 8;
            x = false;
            bool Result = false;

            switch (FileSz)
            {
                case 3795340:
                    x = true;
                    W = 1592;
                    H = 1192;
                    BPP = 10;
                    Result = true;
                    break;

                case 3795328:       // JAI CVM2 10 bits per pixel                 
                    W = 1592;
                    H = 1192;
                    BPP = 10;
                    Result = true;
                    break;

                case 3840000:       // JAI CVM2 10 bits per pixel                 
                    W = 1600;
                    H = 1200;
                    BPP = 10;
                    Result = true;
                    break;

                case 3840012:       // JAI CVM2 10 bits per pixel                 
                    W = 1600;
                    H = 1200;
                    BPP = 10;
                    Result = true;
                    x = true;
                    break;

                case 1920000:       // JAI CVM2 8 bits per pixel                 
                    W = 1600;
                    H = 1200;
                    BPP = 8;
                    Result = true;
                    break;

                case 1897664:       // JAI CVM2 8 bits per pixel
                    W = 1592;
                    H = 1192;
                    Result = true;
                    break;

                case 2818048:       // JAI CVM4 10 bits per pixel
                    W = 1376;
                    H = 1024;
                    BPP = 10;
                    Result = true;
                    break;

                case 1409024:       // JAI CVM4 8 bits per pixel
                    W = 1376;
                    H = 1024;
                    Result = true;
                    break;
            }

            return Result;
        }

        /// <summary>
        /// Load in from file
        /// </summary>
        public BitmapSource BMPFromFile(String FileNameAndPath)
        {
            Uri ur = new Uri(FileNameAndPath, UriKind.RelativeOrAbsolute);
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.CacheOption = BitmapCacheOption.OnLoad;
            bi.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
            bi.UriSource = ur;
            bi.EndInit();
            return bi;
        }

        /// <summary>
        /// Load in from file
        /// </summary>
        public BitmapSource RAWFromFile(String FileNameAndPath)
        {
            FileInfo fi = new FileInfo(FileNameAndPath);
            long FileSz = fi.Length;
            bool x = false;

            if (EstimateDimensions(FileSz, out Width, out Height, out BitsPerPixel, out x))
            {
                BytesPerPixel = (BitsPerPixel + 7) / 8;
                int Nbytes = BytesPerPixel * Height * Width;

                using (FileStream fs = File.OpenRead(FileNameAndPath))
                {
                    if (x)
                    {
                        byte[] szSize;
                        szSize = new byte[12];
                        fs.Read(szSize, 0, 12);
                    }

                    Pixels = new byte[Nbytes];
                    if (fs.Read(Pixels, 0, Nbytes) == Nbytes)
                    {
                        Type = ImageType.RawImage;
                    }
                    else
                    {
                        Type = ImageType.EmptyImage;
                        Pixels = null;
                    }
                }

                if (Type == ImageType.RawImage)
                {

                    BitmapSource bmps;

                    if (BitsPerPixel > 8)
                    {
                        ushort[] Im;
                        Im = new ushort[Nbytes / 2];
                        ushort G;
                        ushort Mult = (ushort)(256 / Math.Pow(2.0, BitsPerPixel - 8.0));
                        for (int i = 0; i < Nbytes / 2; i++)
                        {
                            G = (ushort)(Pixels[i * 2 + 1] * 256 + Pixels[i * 2]);
                            Im[i] = (ushort)(G * Mult);
                        }

                        bmps = BitmapSource.Create(Width, Height, 96.0, 96.0, System.Windows.Media.PixelFormats.Gray16, null, Im, Width * BytesPerPixel);
                    }
                    else
                        bmps = BitmapSource.Create(Width, Height, 96.0, 96.0, System.Windows.Media.PixelFormats.Gray8, null, Pixels, Width);

                    return bmps;
                }
            }

            return null;
        }

        /// <summary>
        /// Load in from file
        /// </summary>
        public BitmapSource JPGLSFromFile(String FileNameAndPath)
        {
            FileInfo fi = new FileInfo(FileNameAndPath);
            long FileSz = fi.Length;
        

            ManagedCompressor Compressit = new ManagedCompressor();

            ushort[] Im = Compressit.Load(FileNameAndPath, out Width, out Height, out BitsPerPixel);
            BytesPerPixel = (BitsPerPixel + 7) / 8;

            Type = ImageType.JLSImage;

            int S = BitsPerPixel - 8;

            for (int i = 0; i < Width * Height; i++)
            {
                Im[i] = (ushort)(Im[i] << S);
            }


            return BitmapSource.Create(Width, Height, 96.0, 96.0, System.Windows.Media.PixelFormats.Gray16, null, Im, Width * BytesPerPixel);          
        }
    }
}
