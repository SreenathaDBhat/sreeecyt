// Convert Bitmap <--> BitmapImage and other bitmap bits and bobs

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Windows.Interop;
using System.Runtime.InteropServices;

namespace BasicBmpImaging
{
   // Bitmap bits and bobs
    class BitmapStuff
    {
        [DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);

        // Bitmap to BitmapSource
        public static BitmapSource ToSource(Bitmap bitmap)
        {
            IntPtr h = bitmap.GetHbitmap();

            BitmapSource bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(
                        h,
                        IntPtr.Zero,
                        Int32Rect.Empty,
                        BitmapSizeOptions.FromEmptyOptions());
            DeleteObject(h);   // Do this or suffer handles building up

            return bitmapSource;
        }

        // BitmapSource to Bitmap
        public static Bitmap From(BitmapSource source)
        {
            Bitmap Bmp = null;

            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(source));
                enc.Save(outStream);
                Bmp = new System.Drawing.Bitmap(outStream);
            }
            return Bmp;
        }

        // BitmapSource to Bitmap
        public static void SaveSource(BitmapSource source, String FileNameAndPath)
        {
            if (File.Exists(FileNameAndPath))
            {
                File.Delete(FileNameAndPath);
            }

            using (FileStream outStream = File.Create(FileNameAndPath))
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(source));
                enc.Save(outStream);
            }
        }

        // BitmapImage to Bitmap
        public static Bitmap From(BitmapImage source)
        {
            Bitmap Bmp = null;

            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(source));
                enc.Save(outStream);
                Bmp = new System.Drawing.Bitmap(outStream);
            }
            return Bmp;
        }


        public static bool Scales(int W, int H, int Dimension, out double Scale)
        {
            if (W > Dimension || H > Dimension)
            {
                if (W > H)
                    Scale = (double)Dimension / W;
                else
                    Scale = (double)Dimension / H;

                return true;
            }

            Scale = 1.0;

            return false;
        }

        // Scale to a dimension
        public static Bitmap ScaleTo(Bitmap Bitmap, int Dimension, out double Scale)
        {
            
            if (Scales(Bitmap.Width, Bitmap.Height, Dimension, out Scale))
                return Scaled(Bitmap, Scale, Scale);

            return Bitmap;
        }

        // Scale to a dimension
        public static BitmapSource ScaleTo(BitmapSource Bitmap, int Dimension, out double Scale)
        {

            if (Scales(Bitmap.PixelWidth, Bitmap.PixelHeight, Dimension, out Scale))
                return Scaled(Bitmap, Scale, Scale);

            return Bitmap;
        }


        // Scale a bitmap by a specific scale factor 
        public static Bitmap Scaled(Bitmap Bitmap, double ScaleFactorX, double ScaleFactorY)
        {
            // Scale it
            Transform Scale = new ScaleTransform(ScaleFactorX, ScaleFactorY);
            TransformedBitmap TB = new TransformedBitmap();
            TB.BeginInit();
            TB.Source = ToSource(Bitmap);
            TB.Transform = Scale;
            TB.EndInit();

            return From(TB);
        }

        // Scale a bitmap by a specific scale factor 
        public static BitmapSource Scaled(BitmapSource Bitmap, double ScaleFactorX, double ScaleFactorY)
        {
            // Scale it
            Transform Scale = new ScaleTransform(ScaleFactorX, ScaleFactorY);
            TransformedBitmap TB = new TransformedBitmap();
            TB.BeginInit();
            TB.Source = Bitmap;
            TB.Transform = Scale;
            TB.EndInit();

            return TB;
        }

        // To 8 bit grey
        public static Bitmap ToGray8(Bitmap Bitmap)
        {
            FormatConvertedBitmap FCB = new FormatConvertedBitmap();

            // Convert to 8 bit gray level
            FCB.BeginInit();
            FCB.Source = ToSource(Bitmap);
            FCB.DestinationFormat = PixelFormats.Gray8;
            FCB.EndInit();
            return From(FCB);
        }

        // To 8 bit grey
        public static BitmapSource ToGray8(BitmapSource Bitmap)
        {
            FormatConvertedBitmap FCB = new FormatConvertedBitmap();

            // Convert to 8 bit gray level
            FCB.BeginInit();
            FCB.Source = Bitmap;
            FCB.DestinationFormat = PixelFormats.Gray8;
            FCB.EndInit();
            return FCB;
        }

        // Extract from a bitmap
        // NB Only works for 8 bits per pixel at the moment
        public static byte[] ExtractData(BitmapSource BitmapS)
        {
            Bitmap bmp = From(BitmapS);
            System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height);
            System.Drawing.Imaging.BitmapData bmpData =
                bmp.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                bmp.PixelFormat);


            byte[] ImValues = new byte[bmp.Width * bmp.Height];
            IntPtr ptr = bmpData.Scan0;
            int bytes = bmp.Width * bmp.Height;
            System.Runtime.InteropServices.Marshal.Copy(ptr, ImValues, 0, bytes);
            bmp.UnlockBits(bmpData);

            return ImValues;
        }        
    }
}
