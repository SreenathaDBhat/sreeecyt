﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using ManagedScriptProcessing;
using BasicBmpImaging;


//
// Command Line Arguments
//      ScriptTestHarness <ScriptName> <Input Image> <Output Image> <Result Image>
// 
// <Input Image>   is the input image
// <Output Image>  is the script processed result image
// <Result Image>  is the expected result image
//
// The <Output Image> is compared to the expected <Result Image> if there any differences an error is flagged

namespace ScriptTestHarness
{
    class Program
    {
        static Logger logger = LogManager.GetLogger("ScriptTest"); 
 
        static int Main(string[] args)
        {
            int Error = 0;

            logger.Info("Script Test Started");

            if (args.Length == 4)
            {
                ManagedScriptProc Scripter = new ManagedScriptProc();

                string ScriptPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

                if (Scripter.Initialise("TestHarness", ScriptPath, "Qin", "Qout") == false)
                {
                    logger.Error("Error Initialising Script Processing Engine");
                    Error = 1;
                }
                else
                {
                    if (Scripter.LoadScript(args[0]) == false)
                    {
                        logger.Error("Error " + args[0] + " test script not found");
                        Error = 1;
                    }
                    else
                    {
                        logger.Info("TESTING " + args[0]);

                        BasicImage BI = new BasicImage(args[1]);

                        try
                        {
                            byte[] Pixels = BI.GetPixels();

                            // Must flip the image otherwise it comes out the wrong way
                            if (Scripter.FlipPutImage(BI.Width, BI.Height, Pixels))
                            {
                                Scripter.SetExecutionMode(ManagedScriptProc.ScriptMode.Play);

                                if (Scripter.Execute() == false)
                                {
                                    logger.Error("Error executing " + args[0]);
                                    Error = 1;
                                }
                                else
                                {
                                    // Compare the output with the expected result
                                    BasicImage ActualResult = new BasicImage(args[3]);
                                    byte[] ActualResultPixels = ActualResult.GetPixels();

                                    // This image should be output from the script
                                    BasicImage Result = new BasicImage(args[2]);
                                    byte[] ResultPixels = Result.GetPixels();

                                    if (Result.BitsPerPixel != ActualResult.BitsPerPixel ||
                                        Result.Width != ActualResult.Width ||
                                        Result.Height != ActualResult.Height)
                                    {
                                        logger.Error("Error Script " + args[0] + " does not produce the correct result - error in image format " + args[3]);
                                        Error = 1;
                                    }
                                    else
                                    for (int i = 0; i < ActualResult.Height * ActualResult.RawStride; i++)
                                    {
                                        if (ActualResultPixels[i] != ResultPixels[i])
                                        {
                                            logger.Error("Error Script " + args[0] + " does not produce the correct result - error in processed result " + args[3]);
                                            Error = 1;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                logger.Error("Could not add testimage to input queue");
                                Error = 1;
                            }
                        }

                        catch (Exception e)
                        {
                            logger.Error("Error during script execution " + e.Message);
                            Error = 1;
                        }
                    }
                }
            }
            else
            {
                logger.Error("Script Test incorrect number of arguments");
                Error = 1;
            }

            if (Error == 0)
                logger.Info("Script " + args[0] + " ran successfully !");
            else
                logger.Fatal("Script " + args[0] + " ERROR !");

            logger.Info("Script Test Ended");

            return Error;
        }
    }
}
