'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Simple Seeded Region Growing demo
'
' Shiddhartha Nandy
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

' For the Seeded Region Growing
Const MAX_SEGMENTED_OBJECT_COUNT = 250
Const FIRST_SEGMENTED_OBJECT_INDEX = 2
Const DO_SPLIT = True

Const FrameXScale = 0.117
Const FrameYScale = 0.117

Const minCellAreaMicrons = 5
Dim minCellAreaPixels

Dim FrameXDimension, FrameYDimension	

' Indices valid for arrays returned by WoolzGetObjects and WoolzMeasureObjectEx
Const AreaIdx 			= 0					' Object area
Const XMinIdx 			= 3					' Bounding box - same as UTS
Const YMinIdx 			= 4
Const XMaxIdx 			= 5
Const YMaxIdx 			= 6
Const MinGreyidx		= 7
Const MaxGreyIdx		= 8
Const MeanGreyIdx 		= 9

' Indices valid only for array returned by WoolzMeasureObjectEx
Const COGxIdx 			= 1					' Centre of gravity - not the bounding box or seed for reconstruction
Const COGyIdx		 	= 2
Const AxisRatIdx		= 7					' Axis ratio
Const CMPIdx			= 8					' Compactness
Const MinRadiusIdx		= 9					' Min and max radii taken from the COG
Const MaxRadiusIdx 		= 10
Const CircIdx			= 12				' Circularity
Const PerimIdx			= 13				' Perimeter
Const EnclosedAreaIdx	= 14				' Area enclosed by the perimeter
Const WzSeedReconX		= 15
Const WzSeedReconY		= 16
Const WidthIdx			= 17
Const HeightIdx			= 18
Const AngleIdx			= 19


Dim CellClassifier

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Classifier Stuff
' No need to load a classifier this is done by the hosting code
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Class Classifier
	Dim Features(10)
	Dim NumFeatures

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Add a feature
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Sub Add(ByVal Value, ByVal Normaliser)
		Features(NumFeatures) = Value / Normaliser
		NumFeatures = NumFeatures + 1
	End Sub
		
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' Run the classifier
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	Function RunClassifier(ByRef Measures())
		Dim Circularity, Area, AxisRatio
		
 	    Circularity = Round(Measures(CircIdx), 1)		
		Area = Measures(AreaIdx) * FrameXScale * FrameYScale
        AxisRatio = Round(Measures(AxisRatIdx), 1)

		NumFeatures = 0

		Add Circularity, 1000
		Add Area, 1000
        Add AxisRatio, 1000
		
		Features(NumFeatures) = -1.0
					
		RunClassifier = SVMClassify(NumFeatures, Features)

        'DebugString "Cell class = " & RunClassifier & " CIRC = " & Circularity & " AREA = " & Area & " AXIS = " & AxisRatio
		
		' Output Diagnostic data
		'AddMeasurement "Circularity", Circularity, 1000, False, False, False
		'AddMeasurement "Area", Area, 1000, False, False, False	
		'AddMeasurement "Class", RunClassifier, 1000, False, False, False		
		'AddMeasurement "Quit", 1, 1, False, False, False	
	
	End Function
	
End Class


Function GaussBlur(ByVal Image, ByVal Radius, ByVal Sigma, ByVal Overlap)
	Dim SrcIm, DestIm, Image1, UG
	Dim W, H
	Dim T1, T2, T3, T4
	
	GetRectInfo Image, W, H

	Image1 = CreateAs(Image)

	UG = CreateGIP()

	If (UG) Then
		T1 = StartTime
		SrcIm   = CreateGIPImageAs(Image, Overlap)
		DestIm  = CreateGIPImageAs(Image, Overlap)
	
		PutGIP Image, SrcIm
		T2 = GetTime - T1
		T4 = StartTime
		BeginGIP
			GaussianBlur Radius, Sigma, SrcIm, DestIm
		EndGIP
		T4 = GetTime - T4
		T1 = StartTime
		GetGIP Image1, DestIm							' Image 1 is our Gaussian background approximation
		T3 = GetTime - T1
		ReleaseGIPImage SrcIm
		ReleaseGIPImage DestIm
	End If
		
	GaussBlur = Image1
End Function


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Segment DAPI image by applying a Seeded Region Growing technique
' The output sgementImage is an 8 bit grey image of the objects found with
'	Grey level 1 = Background
'	Grey level 2..250 each representing a different object
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function SegmentDAPIbySRG( ByVal srcImage, ByVal segmentImage)

	Dim smoothImage, objMaskImage, foreImage, foreGreyImage
    Dim backImage, seedImage, medianImage, varianceImage
    Dim T1, T2, T3, T4, i
    Dim thresh, NumberOfForegroundSeeds
    
	T1 = StartTime

	'Smooth image
	smoothImage = GaussBlur( srcImage, 12, 6.0, 12)

	'Foreground seed regions
	objMaskImage = CreateAsUsingBPP(srcImage, 1)
	thresh = HuangThreshold(smoothImage)
	Threshold smoothImage, objMaskImage, ">", thresh
	
	foreImage = GetSeedsByLocalMaxima( smoothImage, objMaskImage)	

	' Redraw each foreground seed region using a different grey level, starting at 2
    NumberOfForegroundSeeds = WoolzFindObjects(foreImage, 250, 1)
	If (NumberOfForegroundSeeds > MAX_SEGMENTED_OBJECT_COUNT) Then NumberOfComponents = MAX_SEGMENTED_OBJECT_COUNT

	If (NumberOfForegroundSeeds > 0) Then
		foreGreyImage = CreateAsUsingBPP(srcImage, 8)
		For i = 0 To NumberOfForegroundSeeds - 1
			WoolzFillObject foreGreyImage, i, i+FIRST_SEGMENTED_OBJECT_INDEX
		Next	    
	
		'Background seed regions
		backImage = CreateAsUsingBPP(srcImage, 1)
		Threshold smoothImage, backImage, "<=", 3
	
		'Seed regions image combining foreground and background images
		seedImage = CreateAsUsingBPP(srcImage, 8)	
		Convert1To8WithValue backImage, seedImage, 1
		
		PixelWise foreGreyImage, "Or", seedImage
	
	    T2 = GetTime - T1
	    DebugString "Seed Time in seconds " &T2
	    T1 = StartTime    
	
		varianceImage = CreateAsUsingBPP(srcImage, 8)
		medianImage = CreateAsUsingBPP(srcImage, 8)
		medianImage = FSMedian( srcImage, 1)
	    Variance medianImage, varianceImage, 2		

	    'Variance srcImage, varianceImage, 2
	    
	    T2 = GetTime - T1
	    DebugString "Variance Time in seconds " &T2
	    T1 = StartTime
	    
		SRGSegment varianceImage, seedImage, segmentImage
	
	    T2 = GetTime - T1
	    DebugString "Segment Time in seconds " &T2
	
	    'ShowImageWindow "Fore image", foreGreyImage
	    'ShowImageWindow "Back image", backImage    
	    'ShowImageWindow "Seed image", seedImage  
	    'ShowImageWindow "Variance image", varianceImage
	    'ShowImageWindow "segmentImage", segmentImage
	
	    Free foreGreyImage	' grey
	    Free backImage		' bin
	    Free seedImage		' grey
	    Free medianImage	' grey	    
	    Free varianceImage	' grey
    End If

    Free foreImage		' grey 	
    Free smoothImage	' grey	
    Free objMaskImage	' bin

	SegmentDAPIbySRG = NumberOfForegroundSeeds
End Function


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Segment the DAPI image requires a DAPI image and precreated mask
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Function SegmentDAPI(ByVal ImageIn, ByVal GreyObjectsOut)
    Dim ImageIn8bit
    Dim GreyObjectCount

	' If ImageIn has 16 bpp, contrast stretch it, as this function seems to only work as intended with contrast stretched
	' images and 16 bpp input images are not contrast stretched. If the script is running with 8 bpp images, the images
	' will have been contrast stretched before sending to the script. Note however that the contrast stretch done for 8 bpp
	' images sent to the script stretches from the mode to the maximum, where as the ContrastStretch Script function
	' stretches from the minimum to the maximum. Because this function seems to be very sensitive to the shape of the
	' histogram, this means that there will be small differences in the DAPI mask calculated in 16bpp mode (though nothing
	' like as great a difference as occurs if no contrast stretching is done).
	'
	' Note that when the BackgroundThreshold function is called with a 16 bpp image, it produces, proportionally, a different
	' threshold than for the same image converted to 8 bpp, even if the neither image has been histogram stretched.
	' This means that this function will behave slightly differently for 16 bpp images even if they were histogram stretched
	' in exactly the same way as 8bpp images. For this reason, BackgroundThreshold should be modified so that it works
	' consistently regardless of bit-depth, or a compltely different threshold finding function should be used.
	'
	' Note that at the time of writing, WoolzDrawObject will draw 8 bpp images, even if WoolzFindObjects was given a 16bpp image.
	' So everything in this function after WoolzFindObjects is called works in 8bpp, regardless of the bit depth of the input
	' image.
	'
	If (GetRectBPP(ImageIn) > 8) Then
        ImageIn8bit = CreateAsUsingBPP( ImageIn, 8)
        HistogramStretch16To8 ImageIn, ImageIn8bit
    Else
        ImageIn8bit = ImageIn
	End If

	'SaveTIFF ImageIn8Bit, debugOutputDirectory & "\DAPI" & FrameNumber & ".tif"

	GreyObjectCount = SegmentDAPIbySRG( ImageIn8bit, GreyObjectsOut)

	If (GetRectBPP(ImageIn) > 8) Then
        Free ImageIn8bit
	End If
        
    SegmentDAPI = GreyObjectCount

End Function



Class CounterstainObject
	Dim GreyLevel
	' Dynamic array of object measurements
	Dim Features
End Class


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Filter the counterstain objects using the minimum cell area, the cell classifier and
' removing edge objects.
'
' SegmentedCounterstain - Output each object to this image in a different grey level.
' CounterstainMask      - Input - Binary image indicating objects
' CounterstainObjects   - Output greylevel and measurements for each object
' CSObjectCount         - Input+Output - Object count in SegmentedCounterstain at start and end.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub FilterBinaryCounterstainObjects( ByRef SegmentedCounterstain, ByVal CounterstainMask, ByRef CounterstainObjects(), ByRef CSObjectCount)

	Dim i, N, SVMClass, ObjMeas(20)
	N = 0

	' Clear the SegmentedCounterstain image
	Free SegmentedCounterstain
	SegmentedCounterstain = CreateAsUsingBPP( CounterstainMask, 8)
	
	DebugString "FilterBinaryCounterstainObjects: Initial CSObjectCount: " &CSObjectCount
	
	If (CSObjectCount > 0) Then

		CSObjectCount = WoolzFindObjects(CounterstainMask, 1, minCellAreaPixels)
		DebugString "FilterBinaryCounterstainObjects: Woolz CSObjectCount: " &CSObjectCount
		
		For i = 0 To CSObjectCount - 1

			'If this is an object of interest
			WoolzMeasureObjectEx i, ObjMeas			
			SVMClass = CellClassifier.RunClassifier( ObjMeas)			
			If (SVMClass >= 0) THEN
			
				' If not touching the boundary add to the filtered objects
				If (ObjMeas(XMinIdx) > 5 And ObjMeas(XMaxIdx) < FrameXDimension - 5 And ObjMeas(YMinIdx) > 5 And ObjMeas(YMaxIdx) < FrameYDimension - 5) Then
					CounterstainObjects(N).GreyLevel = N + FIRST_SEGMENTED_OBJECT_INDEX
					CounterstainObjects(N).Features = ObjMeas
					WoolzFillObject SegmentedCounterstain, i, CounterstainObjects(N).GreyLevel
       				N = N + 1    		
				End If
			End If
		Next

	End If

	CSObjectCount = N
	DebugString "FilterBinaryCounterstainObjects: Final CSObjectCount: " &CSObjectCount	
End Sub


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Filter the counterstain objects using the minimum cell area, the cell classifier and
' removing edge objects.
'
' SegmentedCounterstain - Input - each object to this image in a different grey level.
' CounterstainObjects   - Output - greylevel and measurements for each object
' CSObjectCount         - Output - Object count in SegmentedCounterstain at start and end.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub FilterGreyCounterstainObjects( ByRef SegmentedCounterstain, ByRef CounterstainObjects(), ByRef CSObjectCount)

	Dim i, N, SVMClass, ObjMeas(20)
	Dim tmpObjectCount, CSObjMask, GreyValue
	
	N = 0

	DebugString "FilterGreyCounterstainObjects: Initial CSObjectCount: " &CSObjectCount

	If (CSObjectCount > 0) Then
	
		For i = 0 To CSObjectCount - 1

			GreyValue = i+FIRST_SEGMENTED_OBJECT_INDEX
			tmpObjectCount = WoolzFindGreyObjects( SegmentedCounterstain, "=", GreyValue, minCellAreaPixels)			
			If (tmpObjectCount > 0) Then

				'If this is an object of interest
				WoolzMeasureObjectEx 0, ObjMeas				
				SVMClass = CellClassifier.RunClassifier( ObjMeas)				
				If (SVMClass >= 0) Then
				
					' If not touching the boundary add to the filtered objects
					If (ObjMeas(XMinIdx) > 5 And ObjMeas(XMaxIdx) < FrameXDimension - 5 And ObjMeas(YMinIdx) > 5 And ObjMeas(YMaxIdx) < FrameYDimension - 5) Then
						CounterstainObjects(N).GreyLevel = GreyValue
						CounterstainObjects(N).Features = ObjMeas
	       				N = N + 1    		
					End If
				End If
			End If
		Next
		
	End If
	
	CSObjectCount = N
	DebugString "FilterGreyCounterstainObjects: Final CSObjectCount: " &CSObjectCount	
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Main 
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub Main

    ' Handles to put images into
    Dim i, CounterstainImage, SegmentedCounterstain, CounterstainMask
    Dim CSObjectCount
	Dim T1, T2    
    Dim CounterstainObjects
    Redim CounterstainObjects(MAX_SEGMENTED_OBJECT_COUNT)


	Set CellClassifier = New Classifier
	
	For i=0 To MAX_SEGMENTED_OBJECT_COUNT-1
		Set CounterstainObjects(i) = New CounterstainObject
	Next

    ' Get the images out of the input queue
    If (Not GetImage(CounterstainImage)) Then
       ShowString "No source image on Q"
       Break
    End If

	' Area in pixels
	minCellAreaPixels = minCellAreaMicrons / (FrameXScale * FrameYScale)
	
    GetRectInfo CounterstainImage, FrameXDimension, FrameYDimension	

	SegmentedCounterstain = CreateAsUsingBPP(CounterstainImage, 8)
	CSObjectCount = SegmentDAPI(CounterstainImage, SegmentedCounterstain)

	If (DO_SPLIT) Then
T1 = StartTime
		
		FilterGreyCounterstainObjects SegmentedCounterstain, CounterstainObjects, CSObjectCount
		
T2 = GetTime - T1
DebugString "FilterGreyCounterstainObjects in seconds " &T2
		
		ShowImageWindow "SegmentedCounterstain", SegmentedCounterstain
	Else

T1 = StartTime
	    CounterstainMask = CreateAsUsingBPP(SegmentedCounterstain, 1)
		ThresholdGTE SegmentedCounterstain, CounterstainMask, 2
	    FilterBinaryCounterstainObjects SegmentedCounterstain, CounterstainMask, CounterstainObjects, CSObjectCount
T2 = GetTime - T1
DebugString "FilterBinaryCounterstainObjects in seconds " &T2

	    ShowImageWindow "CounterstainMask", CounterstainMask
		ShowImageWindow "SegmentedCounterstain", SegmentedCounterstain    
    End If 

	For i=0 TO CSObjectCount-1
		DebugString "Object " &i
		DebugString "GreyLevel " &CounterstainObjects(i).GreyLevel
	Next

	For i=0 To MAX_SEGMENTED_OBJECT_COUNT-1
		Set CounterstainObjects(i) = Nothing
	Next
    
	Set CellClassifier = Nothing    

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Entry point into the script
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Main


