#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cutil.h>
#include "cudamath.h"
#include "CudaTIP_Kernels.h"


texture<unsigned char, cudaTextureType2D, cudaReadModeNormalizedFloat> texUC4;
texture<unsigned char, cudaTextureType2D, cudaReadModeNormalizedFloat> texUC5;
texture<unsigned char, cudaTextureType2D, cudaReadModeNormalizedFloat> texUC6;
texture<unsigned char, cudaTextureType2D, cudaReadModeNormalizedFloat> texUC7;
texture<unsigned char, cudaTextureType2D, cudaReadModeNormalizedFloat> texUC8;
texture<unsigned char, cudaTextureType2D, cudaReadModeNormalizedFloat> texUC9;
texture<unsigned char, cudaTextureType2D, cudaReadModeNormalizedFloat> texUC10;
texture<unsigned char, cudaTextureType2D, cudaReadModeNormalizedFloat> texUC11;
texture<unsigned char, cudaTextureType2D, cudaReadModeNormalizedFloat> texUC12;
texture<unsigned char, cudaTextureType2D, cudaReadModeNormalizedFloat> texUC13;
texture<unsigned char, cudaTextureType2D, cudaReadModeNormalizedFloat> texUC14;
texture<unsigned char, cudaTextureType2D, cudaReadModeNormalizedFloat> texUC15;
texture<unsigned char, cudaTextureType2D, cudaReadModeNormalizedFloat> texUC16;
texture<unsigned short, cudaTextureType2D, cudaReadModeNormalizedFloat> texUS4;
texture<unsigned short, cudaTextureType2D, cudaReadModeNormalizedFloat> texUS5;
texture<unsigned short, cudaTextureType2D, cudaReadModeNormalizedFloat> texUS6;
texture<unsigned short, cudaTextureType2D, cudaReadModeNormalizedFloat> texUS7;
texture<unsigned short, cudaTextureType2D, cudaReadModeNormalizedFloat> texUS8;
texture<unsigned short, cudaTextureType2D, cudaReadModeNormalizedFloat> texUS9;
texture<unsigned short, cudaTextureType2D, cudaReadModeNormalizedFloat> texUS10;
texture<unsigned short, cudaTextureType2D, cudaReadModeNormalizedFloat> texUS11;
texture<unsigned short, cudaTextureType2D, cudaReadModeNormalizedFloat> texUS12;
texture<unsigned short, cudaTextureType2D, cudaReadModeNormalizedFloat> texUS13;
texture<unsigned short, cudaTextureType2D, cudaReadModeNormalizedFloat> texUS14;
texture<unsigned short, cudaTextureType2D, cudaReadModeNormalizedFloat> texUS15;
texture<unsigned short, cudaTextureType2D, cudaReadModeNormalizedFloat> texUS16;
texture<uchar4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC4;
texture<uchar4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC5;
texture<uchar4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC6;
texture<uchar4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC7;
texture<uchar4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC8;
texture<uchar4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC9;
texture<uchar4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC10;
texture<uchar4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC11;
texture<uchar4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC12;
texture<uchar4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC13;
texture<uchar4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC14;
texture<uchar4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC15;
texture<uchar4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC16;
texture<ushort4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US4;
texture<ushort4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US5;
texture<ushort4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US6;
texture<ushort4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US7;
texture<ushort4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US8;
texture<ushort4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US9;
texture<ushort4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US10;
texture<ushort4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US11;
texture<ushort4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US12;
texture<ushort4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US13;
texture<ushort4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US14;
texture<ushort4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US15;
texture<ushort4, cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US16;

__device__ __constant__ int d_NumImages;
__device__ __constant__ float4 d_NullfVal = {0.,0.,0.,0.};
__device__ __constant__ float4 d_ModalCorr[8];
__device__ __constant__ float4 d_MinHdr;

///////////////////////////////////////////////////////////////////////////////////////
// GetPixelUC
///////////////////////////////////////////////////////////////////////////////////////
__device__ float GetPixelUC(int i, float x, float y)
{
	float val;
	if (i < 8)
	{
		if (i<4)
		{
			if (i < 2)
			{
				if (i < 1) val=tex2D(texUC1, x, y);
				 else val=tex2D(texUC2, x, y);
			}
			else
			{
				if (i < 3) val=tex2D(texUC3, x, y);
				 else val=tex2D(texUC4, x, y);
			}
		}
		else
		{
			if (i < 6)
			{
				if (i < 5) val=tex2D(texUC5, x, y);
				 else val=tex2D(texUC6, x, y);
			}
			else
			{
				if (i < 7) val=tex2D(texUC7, x, y);
				 else val=tex2D(texUC8, x, y);
			}
		}
		val -=d_ModalCorr[i].x;							// only important for images in SimpleBlend
		if (val < 0.0) val=0.0;							// in Rangeblend correction values are zero
	}
	else
	{
		if (i<12)
		{
			if (i < 10)
			{
				if (i < 9) val=tex2D(texUC9, x, y);
				 else val=tex2D(texUC10, x, y);
			}
			else
			{
				if (i < 11) val=tex2D(texUC11, x, y);
				 else val=tex2D(texUC12, x, y);
			}
		}
		else
		{
			if (i < 14)
			{
				if (i < 13) val=tex2D(texUC13, x, y);
				 else val=tex2D(texUC14, x, y);
			}
			else
			{
				if (i < 15) val=tex2D(texUC15, x, y);
				 else val=tex2D(texUC16, x, y);
			}
		}
	}
	return (val);
}

///////////////////////////////////////////////////////////////////////////////////////
// GetPixelUS
///////////////////////////////////////////////////////////////////////////////////////
__device__ float GetPixelUS(int i, float x, float y)
{
	float val;
	if (i<8)
	{
		if (i<4)
		{
			if (i < 2)
			{
				if (i < 1) val=tex2D(texUS1, x, y);
				 else val=tex2D(texUS2, x, y);
			}
			else
			{
				if (i < 3) val=tex2D(texUS3, x, y);
				 else val=tex2D(texUS4, x, y);
			}
		}
		else
		{
			if (i < 6)
			{
				if (i < 5) val=tex2D(texUS5, x, y);
				 else val=tex2D(texUS6, x, y);
			}
			else
			{
				if (i < 7) val=tex2D(texUS7, x, y);
				 else val=tex2D(texUS8, x, y);
			}
		}
	}
	else
	{
		if (i<12)
		{
			if (i < 10)
			{
				if (i < 9) val=tex2D(texUS9, x, y);
				 else val=tex2D(texUS10, x, y);
			}
			else
			{
				if (i < 11) val=tex2D(texUS11, x, y);
				 else val=tex2D(texUS12, x, y);
			}
		}
		else
		{
			if (i < 14)
			{
				if (i < 13) val=tex2D(texUS13, x, y);
				 else val=tex2D(texUS14, x, y);
			}
			else
			{
				if (i < 15) val=tex2D(texUS15, x, y);
				 else val=tex2D(texUS16, x, y);
			}
		}
	}
	val -=d_ModalCorr[i].x;
	if (val < 0.0) val=0.0;
	return (val);
}

///////////////////////////////////////////////////////////////////////////////////////
// GetPixel4UC
///////////////////////////////////////////////////////////////////////////////////////
__device__ float4 GetPixel4UC(int i, float x, float y)
{
	float4 val;
	if (i<8)
	{
		if (i<4)
		{
			if (i < 2)
			{
				if (i < 1) val=tex2D(tex4UC1, x, y);
				 else val=tex2D(tex4UC2, x, y);
			}
			else
			{
				if (i < 3) val=tex2D(tex4UC3, x, y);
				 else val=tex2D(tex4UC4, x, y);
			}
		}
		else
		{
			if (i < 6)
			{
				if (i < 5) val=tex2D(tex4UC5, x, y);
				 else val=tex2D(tex4UC6, x, y);
			}
			else
			{
				if (i < 7) val=tex2D(tex4UC7, x, y);
				 else val=tex2D(tex4UC8, x, y);
			}
		}
	}
	else
	{
		if (i<12)
		{
			if (i < 10)
			{
				if (i < 9) val=tex2D(tex4UC9, x, y);
				 else val=tex2D(tex4UC10, x, y);
			}
			else
			{
				if (i < 11) val=tex2D(tex4UC11, x, y);
				 else val=tex2D(tex4UC12, x, y);
			}
		}
		else
		{
			if (i < 14)
			{
				if (i < 13) val=tex2D(tex4UC13, x, y);
				 else val=tex2D(tex4UC14, x, y);
			}
			else
			{
				if (i < 15) val=tex2D(tex4UC15, x, y);
				 else val=tex2D(tex4UC16, x, y);
			}
		}
	}
	val -=d_ModalCorr[i];
	if (val.x < 0.0) val.x=0.0;
	if (val.y < 0.0) val.y=0.0;
	if (val.z < 0.0) val.z=0.0;
	if (val.w < 0.0) val.w=0.0;
	return (val);
}

///////////////////////////////////////////////////////////////////////////////////////
// GetPixel4US
///////////////////////////////////////////////////////////////////////////////////////
__device__ float4 GetPixel4US(int i, float x, float y)
{
	float4 val;
	if (i<8)
	{
		if (i<4)
		{
			if (i < 2)
			{
				if (i < 1) val=tex2D(tex4US1, x, y);
				 else val=tex2D(tex4US2, x, y);
			}
			else
			{
				if (i < 3) val=tex2D(tex4US3, x, y);
				 else val=tex2D(tex4US4, x, y);
			}
		}
		else
		{
			if (i < 6)
			{
				if (i < 5) val=tex2D(tex4US5, x, y);
				 else val=tex2D(tex4US6, x, y);
			}
			else
			{
				if (i < 7) val=tex2D(tex4US7, x, y);
				 else val=tex2D(tex4US8, x, y);
			}
		}
	}
	else
	{
		if (i<12)
		{
			if (i < 10)
			{
				if (i < 9) val=tex2D(tex4US9, x, y);
				 else val=tex2D(tex4US10, x, y);
			}
			else
			{
				if (i < 11) val=tex2D(tex4US11, x, y);
				 else val=tex2D(tex4US12, x, y);
			}
		}
		else
		{
			if (i < 14)
			{
				if (i < 13) val=tex2D(tex4US13, x, y);
				 else val=tex2D(tex4US14, x, y);
			}
			else
			{
				if (i < 15) val=tex2D(tex4US15, x, y);
				 else val=tex2D(tex4US16, x, y);
			}
		}
	}
	val -=d_ModalCorr[i];
	if (val.x < 0.0) val.x=0.0;
	if (val.y < 0.0) val.y=0.0;
	if (val.z < 0.0) val.z=0.0;
	if (val.w < 0.0) val.w=0.0;
	return (val);
}

///////////////////////////////////////////////////////////////////////////////////////
// SimpleBlend_8bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void SimpleBlend_8bit(unsigned char *pDest, int width, int height)
{ 
	float			weightedSum, totalWeight;
	float			val, weight;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
 		weightedSum = 0.;
		totalWeight = 0.;
		for (int i=0; i<d_NumImages; i++)
		{
			val=GetPixelUC(i, x, y);
			weight=val;
			if (weight > 0.5) weight= (1.- weight);
			weight = weight+1.0;
			weightedSum += (val*weight);
			totalWeight +=weight;
		}
		val=clamp(weightedSum/totalWeight, 0.0, 1.0);
		pDest[y*width+x]=(unsigned char)(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// SimpleBlend_16bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void SimpleBlend_16bit(unsigned short *pDest, int width, int height)
{ 
	float			weightedSum, totalWeight;
	float			val, weight;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		weightedSum = 0.;
		totalWeight = 0.;
		for (int i=0; i<d_NumImages; i++)
		{
			val=GetPixelUS(i, x, y);
			weight=val;
			if (weight > 0.5) weight= (1.- weight);
			weight = weight+1.0;
			weightedSum += (val*weight);
			totalWeight +=weight;
		}
		val=clamp(weightedSum/totalWeight, 0.0, 1.0);
		pDest[y*width+x]=(unsigned short)(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// SimpleBlend_24bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void SimpleBlend_24bit(uchar4 *pDest, int width, int height)
{ 
	float4			weightedSum, totalWeight;
	float4			val, weight;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		weightedSum = d_NullfVal;
		totalWeight = d_NullfVal;
		for (int i=0; i<d_NumImages; i++)
		{
			val=GetPixel4UC(i, x, y);
			weight=val;
			if (weight.x > 0.5) weight.x= (1.-weight.x);
			if (weight.y > 0.5) weight.y= (1.-weight.y);
			if (weight.z > 0.5) weight.z= (1.-weight.z);
			if (weight.w > 0.5) weight.w= (1.-weight.w);
			weight = weight+1.0;
			weightedSum += (val*weight);
			totalWeight +=weight;
		}
		val=clamp(weightedSum/totalWeight, 0.0, 1.0);
		pDest[y*width+x]=_uchar4(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// SimpleBlend_48bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void SimpleBlend_48bit(ushort4 *pDest, int width, int height)
{ 
	float4			weightedSum, totalWeight;
	float4			val, weight;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		weightedSum = d_NullfVal;
		totalWeight = d_NullfVal;
		for (int i=0; i<d_NumImages; i++)
		{
			val=GetPixel4US(i, x, y);
			weight=val;
			if (weight.x > 0.5) weight.x= (1.-weight.x);
			if (weight.y > 0.5) weight.y= (1.-weight.y);
			if (weight.z > 0.5) weight.z= (1.-weight.z);
			if (weight.w > 0.5) weight.w= (1.-weight.w);
			weight = weight+1.0;
			weightedSum += (val*weight);
			totalWeight +=weight;
		}
		val=clamp(weightedSum/totalWeight, 0.0, 1.0);
		pDest[y*width+x]=_ushort4(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaInitModalCorrection
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaInitBlendConstants(int nrImages, int *modalCorr, int bpp)
{
	float denom;
	float4 fModalCorr[16];
	if (bpp==8) denom=256.;
		else denom=65536.;
	for (int i=0; i<nrImages; i++)
		{
			fModalCorr[i].x=(float)modalCorr[i]/denom;
			fModalCorr[i].y=(float)modalCorr[i]/denom;
			fModalCorr[i].z=(float)modalCorr[i]/denom;
			fModalCorr[i].w=(float)modalCorr[i]/denom;
		}

	CUDA_SAFE_CALL(cudaMemcpyToSymbol(d_ModalCorr, fModalCorr, nrImages*sizeof(float4)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_NumImages", &nrImages, sizeof(int)));
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaSimpleBlend
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaSimpleBlend(int nrImages, cudaArray **src, void *dest, int width, 
								int height, int bpp)
{

    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src[0]));
				if (nrImages>= 2) CUDA_SAFE_CALL(cudaBindTextureToArray(texUC2, src[1]));
				if (nrImages>= 3) CUDA_SAFE_CALL(cudaBindTextureToArray(texUC3, src[2]));
				if (nrImages>= 4) CUDA_SAFE_CALL(cudaBindTextureToArray(texUC4, src[3]));
				if (nrImages>= 5) CUDA_SAFE_CALL(cudaBindTextureToArray(texUC5, src[4]));
				if (nrImages>= 6) CUDA_SAFE_CALL(cudaBindTextureToArray(texUC6, src[5]));
				if (nrImages>= 7) CUDA_SAFE_CALL(cudaBindTextureToArray(texUC7, src[6]));
				if (nrImages>= 8) CUDA_SAFE_CALL(cudaBindTextureToArray(texUC8, src[7]));
				SimpleBlend_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, 
					width, height);
				if (nrImages>= 8) CUDA_SAFE_CALL(cudaUnbindTexture(texUC8));
				if (nrImages>= 7) CUDA_SAFE_CALL(cudaUnbindTexture(texUC7));
				if (nrImages>= 6) CUDA_SAFE_CALL(cudaUnbindTexture(texUC6));
				if (nrImages>= 5) CUDA_SAFE_CALL(cudaUnbindTexture(texUC5));
				if (nrImages>= 4) CUDA_SAFE_CALL(cudaUnbindTexture(texUC4));
				if (nrImages>= 3) CUDA_SAFE_CALL(cudaUnbindTexture(texUC3));
				if (nrImages>= 2) CUDA_SAFE_CALL(cudaUnbindTexture(texUC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src[0]));
				if (nrImages>= 2) CUDA_SAFE_CALL(cudaBindTextureToArray(texUS2, src[1]));
				if (nrImages>= 3) CUDA_SAFE_CALL(cudaBindTextureToArray(texUS3, src[2]));
				if (nrImages>= 4) CUDA_SAFE_CALL(cudaBindTextureToArray(texUS4, src[3]));
				if (nrImages>= 5) CUDA_SAFE_CALL(cudaBindTextureToArray(texUS5, src[4]));
				if (nrImages>= 6) CUDA_SAFE_CALL(cudaBindTextureToArray(texUS6, src[5]));
				if (nrImages>= 7) CUDA_SAFE_CALL(cudaBindTextureToArray(texUS7, src[6]));
				if (nrImages>= 8) CUDA_SAFE_CALL(cudaBindTextureToArray(texUS8, src[7]));
				SimpleBlend_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest, 
					width, height);
				if (nrImages>= 8) CUDA_SAFE_CALL(cudaUnbindTexture(texUS8));
				if (nrImages>= 7) CUDA_SAFE_CALL(cudaUnbindTexture(texUS7));
				if (nrImages>= 6) CUDA_SAFE_CALL(cudaUnbindTexture(texUS6));
				if (nrImages>= 5) CUDA_SAFE_CALL(cudaUnbindTexture(texUS5));
				if (nrImages>= 4) CUDA_SAFE_CALL(cudaUnbindTexture(texUS4));
				if (nrImages>= 3) CUDA_SAFE_CALL(cudaUnbindTexture(texUS3));
				if (nrImages>= 2) CUDA_SAFE_CALL(cudaUnbindTexture(texUS2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:	// packed 8 bit images
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src[0]));
				if (nrImages>= 2) CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC2, src[1]));
				if (nrImages>= 3) CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC3, src[2]));
				if (nrImages>= 4) CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC4, src[3]));
				if (nrImages>= 5) CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC5, src[4]));
				if (nrImages>= 6) CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC6, src[5]));
				if (nrImages>= 7) CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC7, src[6]));
				if (nrImages>= 8) CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC8, src[7]));
				SimpleBlend_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, width, height);
				if (nrImages>= 8) CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC8));
				if (nrImages>= 7) CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC7));
				if (nrImages>= 6) CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC6));
				if (nrImages>= 5) CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC5));
				if (nrImages>= 4) CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC4));
				if (nrImages>= 3) CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC3));
				if (nrImages>= 2) CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:	// packed 16 bit images
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src[0]));
				if (nrImages>= 2) CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US2, src[1]));
				if (nrImages>= 3) CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US3, src[2]));
				if (nrImages>= 4) CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US4, src[3]));
				if (nrImages>= 5) CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US5, src[4]));
				if (nrImages>= 6) CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US6, src[5]));
				if (nrImages>= 7) CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US7, src[6]));
				if (nrImages>= 8) CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US8, src[7]));
				SimpleBlend_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, width, height);
				if (nrImages>= 8) CUDA_SAFE_CALL(cudaUnbindTexture(tex4US8));
				if (nrImages>= 7) CUDA_SAFE_CALL(cudaUnbindTexture(tex4US7));
				if (nrImages>= 6) CUDA_SAFE_CALL(cudaUnbindTexture(tex4US6));
				if (nrImages>= 5) CUDA_SAFE_CALL(cudaUnbindTexture(tex4US5));
				if (nrImages>= 4) CUDA_SAFE_CALL(cudaUnbindTexture(tex4US4));
				if (nrImages>= 3) CUDA_SAFE_CALL(cudaUnbindTexture(tex4US3));
				if (nrImages>= 2) CUDA_SAFE_CALL(cudaUnbindTexture(tex4US2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// sorts two pixels (float) in place, putting smallest in a
///////////////////////////////////////////////////////////////////////////////////////
__device__ void SortF(float* a, float* b)
{
    float tmp;
	if (*a > *b)
	{
		tmp = *a;
		*a = *b;
		*b = tmp;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// MedianF
///////////////////////////////////////////////////////////////////////////////////////
__device__ void MedianF(float* pix)
{
	SortF(&pix[0], &pix[1]);
   
	// 0-2
	SortF(&pix[1], &pix[2]);
	SortF(&pix[0], &pix[1]);
    
	// 0-3
	SortF(&pix[2], &pix[3]);
	SortF(&pix[1], &pix[2]);
	SortF(&pix[0], &pix[1]);
    
	// 0-4
	SortF(&pix[3], &pix[4]);
	SortF(&pix[2], &pix[3]);
	SortF(&pix[1], &pix[2]);
	SortF(&pix[0], &pix[1]);

	// 0-5    
	SortF(&pix[4], &pix[5]);
	SortF(&pix[3], &pix[4]);
	SortF(&pix[2], &pix[3]);
	SortF(&pix[1], &pix[2]);
	SortF(&pix[0], &pix[1]);

	// 0-6
	SortF(&pix[5], &pix[6]);    
	SortF(&pix[4], &pix[5]);
	SortF(&pix[3], &pix[4]);
	SortF(&pix[2], &pix[3]);
	SortF(&pix[1], &pix[2]);
	SortF(&pix[0], &pix[1]);

	// 0-7
	SortF(&pix[6], &pix[7]);
	SortF(&pix[5], &pix[6]);    
	SortF(&pix[4], &pix[5]);
	SortF(&pix[3], &pix[4]);
	SortF(&pix[2], &pix[3]);
	SortF(&pix[1], &pix[2]);
	SortF(&pix[0], &pix[1]);

	// 0-8
	SortF(&pix[7], &pix[8]);    
	SortF(&pix[6], &pix[7]);
	SortF(&pix[5], &pix[6]);    
	SortF(&pix[4], &pix[5]);
	SortF(&pix[3], &pix[4]);
	SortF(&pix[2], &pix[3]);
	SortF(&pix[1], &pix[2]);
	SortF(&pix[0], &pix[1]);
}

///////////////////////////////////////////////////////////////////////////////////////
// sorts two pixels (float4) in place, putting smallest in a
///////////////////////////////////////////////////////////////////////////////////////
__device__ void Sort4F(float4* a, float4* b)
{
    float tmp;
	if (a->x > b->x)
	{
		tmp = a->x;
		a->x = b->x;
		b->x = tmp;
	}
	if (a->y > b->y)
	{
		tmp = a->y;
		a->y = b->y;
		b->y = tmp;
	}
	if (a->z > b->z)
	{
		tmp = a->z;
		a->z = b->z;
		b->z = tmp;
	}
	if (a->w > b->w)
	{
		tmp = a->w;
		a->w = b->w;
		b->w = tmp;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// Median4F
///////////////////////////////////////////////////////////////////////////////////////
__device__ void Median4F(float4* pix)
{
	Sort4F(&pix[0], &pix[1]);
   
	// 0-2
	Sort4F(&pix[1], &pix[2]);
	Sort4F(&pix[0], &pix[1]);
    
	// 0-3
	Sort4F(&pix[2], &pix[3]);
	Sort4F(&pix[1], &pix[2]);
	Sort4F(&pix[0], &pix[1]);
    
	// 0-4
	Sort4F(&pix[3], &pix[4]);
	Sort4F(&pix[2], &pix[3]);
	Sort4F(&pix[1], &pix[2]);
	Sort4F(&pix[0], &pix[1]);

	// 0-5    
	Sort4F(&pix[4], &pix[5]);
	Sort4F(&pix[3], &pix[4]);
	Sort4F(&pix[2], &pix[3]);
	Sort4F(&pix[1], &pix[2]);
	Sort4F(&pix[0], &pix[1]);

	// 0-6
	Sort4F(&pix[5], &pix[6]);    
	Sort4F(&pix[4], &pix[5]);
	Sort4F(&pix[3], &pix[4]);
	Sort4F(&pix[2], &pix[3]);
	Sort4F(&pix[1], &pix[2]);
	Sort4F(&pix[0], &pix[1]);

	// 0-7
	Sort4F(&pix[6], &pix[7]);
	Sort4F(&pix[5], &pix[6]);    
	Sort4F(&pix[4], &pix[5]);
	Sort4F(&pix[3], &pix[4]);
	Sort4F(&pix[2], &pix[3]);
	Sort4F(&pix[1], &pix[2]);
	Sort4F(&pix[0], &pix[1]);

	// 0-8
	Sort4F(&pix[7], &pix[8]);    
	Sort4F(&pix[6], &pix[7]);
	Sort4F(&pix[5], &pix[6]);    
	Sort4F(&pix[4], &pix[5]);
	Sort4F(&pix[3], &pix[4]);
	Sort4F(&pix[2], &pix[3]);
	Sort4F(&pix[1], &pix[2]);
	Sort4F(&pix[0], &pix[1]);
}

///////////////////////////////////////////////////////////////////////////////////////
// HDRBlend_8bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void HDRBlend_8bit(unsigned char *pDestOrig, unsigned int Pitch, int width, 
							  int height)
{ 
	float	pix[9], val, deviation;

	unsigned char *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		pix[0] = tex2D( texUC1, (float) i,   (float) blockIdx.x)-d_MinHdr.x;
        pix[1] = tex2D( texUC1, (float) i+0, (float) blockIdx.x-1)-d_MinHdr.x;
        pix[2] = tex2D( texUC1, (float) i-1, (float) blockIdx.x+0)-d_MinHdr.x;
        pix[3] = tex2D( texUC1, (float) i+1, (float) blockIdx.x+0)-d_MinHdr.x;
        pix[4] = tex2D( texUC1, (float) i+0, (float) blockIdx.x+1)-d_MinHdr.x;
        pix[5] = tex2D( texUC1, (float) i-1, (float) blockIdx.x-1)-d_MinHdr.x;
		pix[6] = tex2D( texUC1, (float) i+1, (float) blockIdx.x-1)-d_MinHdr.x;
        pix[7] = tex2D( texUC1, (float) i-1, (float) blockIdx.x+1)-d_MinHdr.x;
        pix[8] = tex2D( texUC1, (float) i+1, (float) blockIdx.x+1)-d_MinHdr.x;
		val=pix[0]-d_MinHdr.x;
		MedianF(pix);	// pix[4] contains median
		deviation=(abs(val-pix[4])*256.+1.0)*2.;
		deviation=log2(deviation)*0.69314718;	// correction as GPU calculates 2log
		if (val < pix[4]) deviation = -deviation;
		val=val + deviation/256.;
		val=clamp(val, 0.0, 1.0);	
		pDest[i]=(unsigned char)(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// HDRBlend_16bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void HDRBlend_16bit(unsigned short *pDestOrig, unsigned int Pitch, int width, 
							   int height)
{ 
	float	pix[9], val, deviation;

	unsigned short *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		pix[0] = tex2D( texUS1, (float) i,   (float) blockIdx.x)-d_MinHdr.x;
        pix[1] = tex2D( texUS1, (float) i+0, (float) blockIdx.x-1)-d_MinHdr.x;
        pix[2] = tex2D( texUS1, (float) i-1, (float) blockIdx.x+0)-d_MinHdr.x;
        pix[3] = tex2D( texUS1, (float) i+1, (float) blockIdx.x+0)-d_MinHdr.x;
        pix[4] = tex2D( texUS1, (float) i+0, (float) blockIdx.x+1)-d_MinHdr.x;
        pix[5] = tex2D( texUS1, (float) i-1, (float) blockIdx.x-1)-d_MinHdr.x;
		pix[6] = tex2D( texUS1, (float) i+1, (float) blockIdx.x-1)-d_MinHdr.x;
        pix[7] = tex2D( texUS1, (float) i-1, (float) blockIdx.x+1)-d_MinHdr.x;
        pix[8] = tex2D( texUS1, (float) i+1, (float) blockIdx.x+1)-d_MinHdr.x;
		val=pix[0]-d_MinHdr.x;
		MedianF(pix);	// pix[4] contains median
		deviation=(abs(val-pix[4])*256.+1.0)*2.;
		deviation=log2(deviation)*0.69314718;	// correction as GPU calculates 2log
		if (val < pix[4]) deviation = -deviation;
		val=val + deviation/256.;
		val=clamp(pix[4], 0.0, 1.0);	
		pDest[i]=(unsigned short)(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// HDRBlend_24bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void HDRBlend_24bit(uchar4 *pDestOrig, unsigned int Pitch, int width, 
							   int height)
{ 
	float4	pix[9], val, deviation;;

	uchar4 *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		pix[0] = tex2D( tex4UC1, (float) i,   (float) blockIdx.x)-d_MinHdr;
        pix[1] = tex2D( tex4UC1, (float) i+0, (float) blockIdx.x-1)-d_MinHdr;
        pix[2] = tex2D( tex4UC1, (float) i-1, (float) blockIdx.x+0)-d_MinHdr;
        pix[3] = tex2D( tex4UC1, (float) i+1, (float) blockIdx.x+0)-d_MinHdr;
        pix[4] = tex2D( tex4UC1, (float) i+0, (float) blockIdx.x+1)-d_MinHdr;
        pix[5] = tex2D( tex4UC1, (float) i-1, (float) blockIdx.x-1)-d_MinHdr;
		pix[6] = tex2D( tex4UC1, (float) i+1, (float) blockIdx.x-1)-d_MinHdr;
        pix[7] = tex2D( tex4UC1, (float) i-1, (float) blockIdx.x+1)-d_MinHdr;
        pix[8] = tex2D( tex4UC1, (float) i+1, (float) blockIdx.x+1)-d_MinHdr;
		val=pix[0]-d_MinHdr;
		Median4F(pix);
		deviation=(abs(val-pix[4])*256.+1.0)*2.;
		deviation.x=log2(deviation.x)*0.69314718;		// correction as GPU calculates 2log
		deviation.y=log2(deviation.y)*0.69314718;
		deviation.z=log2(deviation.z)*0.69314718;
		deviation.w=log2(deviation.w)*0.69314718;
		if (val.x < pix[4].x) deviation.x = -deviation.x;
		if (val.y < pix[4].y) deviation.y = -deviation.y;
		if (val.z < pix[4].z) deviation.z = -deviation.z;
		if (val.w < pix[4].w) deviation.w = -deviation.w;
		val=val + deviation/256.;
		val=clamp(val, 0.0, 1.0);
		pDest[i]=_uchar4(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// HDRBlend_48bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void HDRBlend_48bit(ushort4 *pDestOrig, unsigned int Pitch, int width, 
							   int height)
{ 
	float4	pix[9], val, deviation;

	ushort4 *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		pix[0] = tex2D( tex4US1, (float) i,   (float) blockIdx.x)-d_MinHdr;
        pix[1] = tex2D( tex4US1, (float) i+0, (float) blockIdx.x-1)-d_MinHdr;
        pix[2] = tex2D( tex4US1, (float) i-1, (float) blockIdx.x+0)-d_MinHdr;
        pix[3] = tex2D( tex4US1, (float) i+1, (float) blockIdx.x+0)-d_MinHdr;
        pix[4] = tex2D( tex4US1, (float) i+0, (float) blockIdx.x+1)-d_MinHdr;
        pix[5] = tex2D( tex4US1, (float) i-1, (float) blockIdx.x-1)-d_MinHdr;
		pix[6] = tex2D( tex4US1, (float) i+1, (float) blockIdx.x-1)-d_MinHdr;
        pix[7] = tex2D( tex4US1, (float) i-1, (float) blockIdx.x+1)-d_MinHdr;
        pix[8] = tex2D( tex4US1, (float) i+1, (float) blockIdx.x+1)-d_MinHdr;
		val=pix[0]-d_MinHdr;
		Median4F(pix);
		deviation=(abs(val-pix[4])*256.+1.0)*2.;
		deviation.x=log2(deviation.x)*0.69314718;		// correction as GPU calculates 2log
		deviation.y=log2(deviation.y)*0.69314718;
		deviation.z=log2(deviation.z)*0.69314718;
		deviation.w=log2(deviation.w)*0.69314718;
		if (val.x < pix[4].x) deviation.x = -deviation.x;
		if (val.y < pix[4].y) deviation.y = -deviation.y;
		if (val.z < pix[4].z) deviation.z = -deviation.z;
		if (val.w < pix[4].w) deviation.w = -deviation.w;
		val=val + deviation/256.;
		val=clamp(val, 0.0, 1.0);
		pDest[i]=_ushort4(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaInitHDRBlendConstant
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaInitHDRBlendConstant(float4 fMinHdr)
{
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_MinHDR", &fMinHdr, sizeof(float4)));
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaHDRBlend
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaHDRBlend(cudaArray *src, void *dest, int width, int height, 
							 int bpp)
{

	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src));
				HDRBlend_8bit<<<height, 96>>>((unsigned char *)dest, width,
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src));
				HDRBlend_16bit<<<height, 96>>>((unsigned short *)dest, width,
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:	// packed 8 bit images
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src));
				HDRBlend_24bit<<<height, 32>>>((uchar4 *)dest, width, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:	// packed 16 bit images
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src));
				HDRBlend_48bit<<<height, 32>>>((ushort4 *)dest, width, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// RangeBlend_8bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void RangeBlend_8bit(unsigned char *pDest, int width, int height)
{ 
	float	val, range, weightedAverage, rangeSum;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
 		weightedAverage = 0.;
		rangeSum = 0.;
		for (int i=0; i<d_NumImages; i++)
		{
			val=GetPixelUC(i, x, y);
			range = GetPixelUC(i+8, x, y);
			range *=range;
			rangeSum += range;
			weightedAverage += range*val;
		}
		if ( rangeSum > 0.0) weightedAverage /= rangeSum;
		val=clamp(weightedAverage, 0.0, 1.0);
		pDest[y*width+x]=(unsigned char)(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// RangeBlend_16bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void RangeBlend_16bit(unsigned short *pDest, int width, int height)
{ 
	float	val, range, weightedAverage, rangeSum;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
 		weightedAverage = 0.;
		rangeSum = 0.;
		for (int i=0; i<d_NumImages; i++)
		{
			val=GetPixelUS(i, x, y);
			range = GetPixelUS(i+8, x, y);
			range *=range;
			rangeSum += range;
			weightedAverage += range*val;
		}
		if ( rangeSum > 0.0) weightedAverage /= rangeSum;
		val=clamp(weightedAverage, 0.0, 1.0);
		pDest[y*width+x]=(unsigned short)(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// RangeBlend_24bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void RangeBlend_24bit(uchar4 *pDest, int width, int height)
{ 
	float4	val, range, weightedAverage, rangeSum;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
 		weightedAverage = d_NullfVal;
		rangeSum = d_NullfVal;
		for (int i=0; i<d_NumImages; i++)
		{
			val=GetPixel4UC(i, x, y);
			range = GetPixel4UC(i+8, x, y);
			range =range*range;
			rangeSum += range;
			weightedAverage += range*val;
		}
		if ( rangeSum.x > 0) weightedAverage.x /= rangeSum.x;
		if ( rangeSum.y > 0) weightedAverage.y /= rangeSum.y;
		if ( rangeSum.z > 0) weightedAverage.z /= rangeSum.z;
		if ( rangeSum.w > 0) weightedAverage.w /= rangeSum.w;
		val=clamp(weightedAverage, 0.0, 1.0);
		pDest[y*width+x]=_uchar4(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// RangeBlend_48bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void RangeBlend_48bit(ushort4 *pDest, int width, int height)
{ 
	float4	val, range, weightedAverage, rangeSum;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
 		weightedAverage = d_NullfVal;
		rangeSum = d_NullfVal;
		for (int i=0; i<d_NumImages; i++)
		{
			val=GetPixel4US(i, x, y);
			range = GetPixel4US(i+8, x, y);
			range =range*range;
			rangeSum += range;
			weightedAverage += range*val;
		}
		if ( rangeSum.x > 0) weightedAverage.x /= rangeSum.x;
		if ( rangeSum.y > 0) weightedAverage.y /= rangeSum.y;
		if ( rangeSum.z > 0) weightedAverage.z /= rangeSum.z;
		if ( rangeSum.w > 0) weightedAverage.w /= rangeSum.w;
		val=clamp(weightedAverage, 0.0, 1.0);
		pDest[y*width+x]=_ushort4(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaRangeBlend
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaRangeBlend(int nrImages, cudaArray **src, cudaArray **range,
							   void *dest, int width, int height, int bpp)
{

    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src[0]));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC9, range[0]));
				if (nrImages>= 2)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUC2, src[1]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUC10, range[1]));
				}
				if (nrImages>= 3)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUC3, src[2]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUC11, range[2]));
				}
				if (nrImages>= 4) 
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUC4, src[3]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUC12, range[3]));
				}
				if (nrImages>= 5)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUC5, src[4]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUC13, range[4]));
				}
				if (nrImages>= 6)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUC6, src[5]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUC14, range[5]));
				}
				if (nrImages>= 7) 
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUC7, src[6]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUC15, range[6]));
				}
				if (nrImages>= 8)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUC8, src[7]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUC16, range[7]));
				}
				RangeBlend_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, 
					width, height);
				if (nrImages>= 8)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(texUC8));
					CUDA_SAFE_CALL(cudaUnbindTexture(texUC16));
				}
				if (nrImages>= 7)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(texUC7));
					CUDA_SAFE_CALL(cudaUnbindTexture(texUC15));
				}
				if (nrImages>= 6)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(texUC6));
					CUDA_SAFE_CALL(cudaUnbindTexture(texUC14));
				}
				if (nrImages>= 5)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(texUC5));
					CUDA_SAFE_CALL(cudaUnbindTexture(texUC13));
				}
				if (nrImages>= 4)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(texUC4));
					CUDA_SAFE_CALL(cudaUnbindTexture(texUC12));
				}
				if (nrImages>= 3)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(texUC3));
					CUDA_SAFE_CALL(cudaUnbindTexture(texUC11));
				}
				if (nrImages>= 2)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(texUC2));
					CUDA_SAFE_CALL(cudaUnbindTexture(texUC10));
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC9));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src[0]));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS9, range[0]));
				if (nrImages>= 2)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUS2, src[1]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUS10, range[1]));
				}
				if (nrImages>= 3)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUS3, src[2]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUS11, range[2]));
				}
				if (nrImages>= 4) 
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUS4, src[3]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUS12, range[3]));
				}
				if (nrImages>= 5)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUS5, src[4]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUS13, range[4]));
				}
				if (nrImages>= 6)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUS6, src[5]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUS14, range[5]));
				}
				if (nrImages>= 7) 
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUS7, src[6]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUS15, range[6]));
				}
				if (nrImages>= 8)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUS8, src[7]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(texUS16, range[7]));
				}
				RangeBlend_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest, 
					width, height);
				if (nrImages>= 8)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(texUS8));
					CUDA_SAFE_CALL(cudaUnbindTexture(texUS16));
				}
				if (nrImages>= 7)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(texUS7));
					CUDA_SAFE_CALL(cudaUnbindTexture(texUS15));
				}
				if (nrImages>= 6)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(texUS6));
					CUDA_SAFE_CALL(cudaUnbindTexture(texUS14));
				}
				if (nrImages>= 5)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(texUS5));
					CUDA_SAFE_CALL(cudaUnbindTexture(texUS13));
				}
				if (nrImages>= 4)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(texUS4));
					CUDA_SAFE_CALL(cudaUnbindTexture(texUS12));
				}
				if (nrImages>= 3)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(texUS3));
					CUDA_SAFE_CALL(cudaUnbindTexture(texUS11));
				}
				if (nrImages>= 2)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(texUS2));
					CUDA_SAFE_CALL(cudaUnbindTexture(texUS10));
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS9));
				break;
		case 24:	// packed 8 bit images
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src[0]));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC9, range[0]));
				if (nrImages>= 2)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC2, src[1]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC10, range[1]));
				}
				if (nrImages>= 3)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC3, src[2]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC11, range[2]));
				}
				if (nrImages>= 4) 
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC4, src[3]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC12, range[3]));
				}
				if (nrImages>= 5)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC5, src[4]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC13, range[4]));
				}
				if (nrImages>= 6)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC6, src[5]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC14, range[5]));
				}
				if (nrImages>= 7) 
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC7, src[6]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC15, range[6]));
				}
				if (nrImages>= 8)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC8, src[7]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC16, range[7]));
				}
				RangeBlend_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, width, height);
				if (nrImages>= 8)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC8));
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC16));
				}
				if (nrImages>= 7)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC7));
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC15));
				}
				if (nrImages>= 6)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC6));
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC14));
				}
				if (nrImages>= 5)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC5));
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC13));
				}
				if (nrImages>= 4)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC4));
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC12));
				}
				if (nrImages>= 3)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC3));
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC11));
				}
				if (nrImages>= 2)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC2));
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC10));
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC9));
				break;
		case 48:	// packed 16 bit images
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src[0]));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US9, range[0]));
				if (nrImages>= 2)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US2, src[1]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US10, range[1]));
				}
				if (nrImages>= 3)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US3, src[2]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US11, range[2]));
				}
				if (nrImages>= 4) 
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US4, src[3]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US12, range[3]));
				}
				if (nrImages>= 5)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US5, src[4]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US13, range[4]));
				}
				if (nrImages>= 6)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US6, src[5]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US14, range[5]));
				}
				if (nrImages>= 7) 
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US7, src[6]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US15, range[6]));
				}
				if (nrImages>= 8)
				{
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US8, src[7]));
					CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US16, range[7]));
				}
				RangeBlend_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, width, height);
				if (nrImages>= 8)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4US8));
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4US16));
				}
				if (nrImages>= 7)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4US7));
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4US15));
				}
				if (nrImages>= 6)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4US6));
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4US14));
				}
				if (nrImages>= 5)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4US5));
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4US13));
				}
				if (nrImages>= 4)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4US4));
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4US12));
				}
				if (nrImages>= 3)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4US3));
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4US11));
				}
				if (nrImages>= 2)
				{
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4US2));
					CUDA_SAFE_CALL(cudaUnbindTexture(tex4US10));
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US9));
				break;
	}
}
