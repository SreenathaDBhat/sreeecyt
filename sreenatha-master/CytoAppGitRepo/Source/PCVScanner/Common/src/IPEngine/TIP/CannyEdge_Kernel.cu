
#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cutil.h>
#include "cudamath.h"
#include "CudaTIP_Kernels.h"

__device__ __constant__ float d_LowThreshold;
__device__ __constant__ float d_HighThreshold;


///////////////////////////////////////////////////////////////////////////////////////
// Canny Edge (pass0)
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CE_Pass0Device(float *pTarg0, float *pTarg1, float *pSrc, int width,  
								int height)
{ 
	float pix[4], p, q, Magnitude, Theta;
	float PI=3.1415926536;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	// edge pixels are set to zero in second pass
	if ((y> 0) && (y<height) && (x<(width-1)))
	{
		pix[0]=pSrc[(y-1)*width+x];
		pix[1]=pSrc[(y-1)*width+x+1];
		pix[2]=pSrc[y*width+x];
		pix[3]=pSrc[y*width+x+1];
		p=(pix[1]+pix[3]-pix[0]-pix[2])/2.;
		q=(pix[2]+pix[3]-pix[0]-pix[1])/2.;
		Magnitude=sqrt(p*p+q*q)/sqrt(2.);
		Theta=atan2(p,q);
		pTarg0[y*width+x]=Magnitude;
		pTarg1[y*width+x]=abs(Theta)/PI;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CE_Pass1Device_8bit (pass1)
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CE_Pass1Device_8bit(unsigned char *pTarg, float *pSrc1, float *pSrc2, 
									 int width, int height)
{ 
	float			Magnitude, Theta;
	float			PI=3.1415926536;
	float2			texSamples;
	unsigned char	result;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		if ((y> 0) && (y<(height-1)) && (x> 0) && (x<(width-1)))
		{
			Magnitude=pSrc1[y*width+x];
			Theta=pSrc2[y*width+x];
			Theta=(Theta-PI/16)*4.;
			Theta=floor(Theta);
			if (Theta==0)
			{
				texSamples.x=pSrc1[y*width+x+1];
				texSamples.y=pSrc1[y*width+x-1];
			}
			else if (Theta == 1)
			{
				texSamples.x=pSrc1[(y-1)*width+x+1];
				texSamples.y=pSrc1[(y+1)*width+x-1];
			}
			else if (Theta == 2)
			{
				texSamples.x=pSrc1[(y-1)*width+x];
				texSamples.y=pSrc1[(y+1)*width+x];
			}
			else if (Theta == 3)
			{
				texSamples.x=pSrc1[(y-1)*width+x-1];
				texSamples.y=pSrc1[(y+1)*width+x+1];
			}
			if ((Magnitude < texSamples.x) || (Magnitude < texSamples.y))
				result=0;
			else
			{
				if ((Magnitude < d_LowThreshold) || (Magnitude > d_HighThreshold))
					result=0;
				else
					result=255;
			}
			pTarg[y*width+x]=result;
		}
		else
			pTarg[y*width+x]=0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CE_Pass1Device_16bit (pass1)
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CE_Pass1Device_16bit(unsigned short *pTarg, float *pSrc1, float *pSrc2, 
									 int width, int height)
{ 
	float			Magnitude, Theta;
	float			PI=3.1415926536;
	float2			texSamples;
	unsigned short	result;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		if ((y> 0) && (y<(height-1)) && (x> 0) && (x<(width-1)))
		{
			Magnitude=pSrc1[y*width+x];
			Theta=pSrc2[y*width+x];
			Theta=(Theta-PI/16)*4.;
			Theta=floor(Theta);
			if (Theta==0)
			{
				texSamples.x=pSrc1[y*width+x+1];
				texSamples.y=pSrc1[y*width+x-1];
			}
			else if (Theta == 1)
			{
				texSamples.x=pSrc1[(y-1)*width+x+1];
				texSamples.y=pSrc1[(y+1)*width+x-1];
			}
			else if (Theta == 2)
			{
				texSamples.x=pSrc1[(y-1)*width+x];
				texSamples.y=pSrc1[(y+1)*width+x];
			}
			else if (Theta == 3)
			{
				texSamples.x=pSrc1[(y-1)*width+x-1];
				texSamples.y=pSrc1[(y+1)*width+x+1];
			}
			if ((Magnitude < texSamples.x) || (Magnitude < texSamples.y))
				result=0;
			else
			{
				if ((Magnitude < d_LowThreshold) || (Magnitude > d_HighThreshold))
					result=0;
				else
					result=65535;
			}
			pTarg[y*width+x]=result;
		}
		else
			pTarg[y*width+x]=0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CE_Pass1Device_24bit (pass1)
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CE_Pass1Device_24bit(uchar4 *pTarg, float *pSrc1, float *pSrc2, 
									 int width, int height)
{ 
	float			Magnitude, Theta;
	float			PI=3.1415926536;
	float2			texSamples;
	uchar4			result;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		if ((y> 0) && (y<(height-1)) && (x> 0) && (x<(width-1)))
		{
		if ((y> 0) && (y<(height-1)) && (x> 0) && (x<(width-1)))
		{
			Magnitude=pSrc1[y*width+x];
			Theta=pSrc2[y*width+x];
			Theta=(Theta-PI/16)*4.;
			Theta=floor(Theta);
			if (Theta==0)
			{
				texSamples.x=pSrc1[y*width+x+1];
				texSamples.y=pSrc1[y*width+x-1];
			}
			else if (Theta == 1)
			{
				texSamples.x=pSrc1[(y-1)*width+x+1];
				texSamples.y=pSrc1[(y+1)*width+x-1];
			}
			else if (Theta == 2)
			{
				texSamples.x=pSrc1[(y-1)*width+x];
				texSamples.y=pSrc1[(y+1)*width+x];
			}
			else if (Theta == 3)
			{
				texSamples.x=pSrc1[(y-1)*width+x-1];
				texSamples.y=pSrc1[(y+1)*width+x+1];
			}
			if ((Magnitude < texSamples.x) || (Magnitude < texSamples.y))
				result=blackVal;
			else
			{
				if ((Magnitude < d_LowThreshold) || (Magnitude > d_HighThreshold))
					result=blackVal;
				else
					result=whiteVal;
			}
			pTarg[y*width+x]=result;
		}
		}
		else
			pTarg[y*width+x]=blackVal;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CE_Pass1Device_48bit (pass1)
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CE_Pass1Device_48bit(ushort4 *pTarg, float *pSrc1, float *pSrc2, 
									 int width, int height)
{ 
	float			Magnitude, Theta;
	float			PI=3.1415926536;
	float2			texSamples;
	ushort4			result;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		if ((y> 0) && (y<(height-1)) && (x> 0) && (x<(width-1)))
		{
		if ((y> 0) && (y<(height-1)) && (x> 0) && (x<(width-1)))
		{
			Magnitude=pSrc1[y*width+x];
			Theta=pSrc2[y*width+x];
			Theta=(Theta-PI/16)*4.;
			Theta=floor(Theta);
			if (Theta==0)
			{
				texSamples.x=pSrc1[y*width+x+1];
				texSamples.y=pSrc1[y*width+x-1];
			}
			else if (Theta == 1)
			{
				texSamples.x=pSrc1[(y-1)*width+x+1];
				texSamples.y=pSrc1[(y+1)*width+x-1];
			}
			else if (Theta == 2)
			{
				texSamples.x=pSrc1[(y-1)*width+x];
				texSamples.y=pSrc1[(y+1)*width+x];
			}
			else if (Theta == 3)
			{
				texSamples.x=pSrc1[(y-1)*width+x-1];
				texSamples.y=pSrc1[(y+1)*width+x+1];
			}
			if ((Magnitude < texSamples.x) || (Magnitude < texSamples.y))
				result=blackShortVal;
			else
			{
				if ((Magnitude < d_LowThreshold) || (Magnitude > d_HighThreshold))
					result=blackShortVal;
				else
					result=whiteShortVal;
			}
			pTarg[y*width+x]=result;
		}
		}
		else
			pTarg[y*width+x]=blackShortVal;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaCannyEdge
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaCannyEdge(void *src, void *tmp1, void *tmp2, void *dest, int width, 
							  int height, int bpp, float lowThresh, float highThresh)
{
	dim3 dimBlock(8, 8, 1);
	dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);
	CE_Pass0Device<<< dimGrid, dimBlock>>>((float *)tmp1, (float *)tmp2, (float *)src, 
		width, height);
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_LowThreshold", &lowThresh, sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_HighThreshold", &highThresh, sizeof(float)));

	switch (bpp)
	{
		case 8:
				CE_Pass1Device_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, 
					(float *)tmp1, (float *)tmp2, width, height);
				break;
		case 16:
				CE_Pass1Device_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest, 
					(float *)tmp1, (float *)tmp2, width, height);
				break;
		case 24: 
				CE_Pass1Device_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, (float *)tmp1, 
					(float *)tmp2, width, height);
				break;
		case 48:
				CE_Pass1Device_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, (float *)tmp1, 
					(float *)tmp2, width, height);
				break;
	}
	
}
