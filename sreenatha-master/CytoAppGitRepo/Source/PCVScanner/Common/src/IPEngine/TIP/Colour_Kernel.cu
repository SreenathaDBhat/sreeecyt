
#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cutil.h>
#include "cudamath.h"
#include "CudaTIP_Kernels.h"

__device__ __constant__ float d_ThreshR0;
__device__ __constant__ float d_ThreshR1;
__device__ __constant__ float d_ThreshG0;
__device__ __constant__ float d_ThreshG1;
__device__ __constant__ float d_ThreshB0;
__device__ __constant__ float d_ThreshB1;
__device__ __constant__ float d_BlendR;
__device__ __constant__ float d_BlendG;
__device__ __constant__ float d_BlendB;
__device__ __constant__ float4 d_DeconvVec[3];




///////////////////////////////////////////////////////////////////////////////////////
// Split24Device_8bit
//	- split 24 colour into 3 8 bit greyvalue images
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  Split24Device_8bit(unsigned char *pTarg1, unsigned char *pTarg2,
									unsigned char *pTarg3, int width, int height)
{ 
	float4	col;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		col=tex2D(tex4UC1, x, y);
		pTarg1[y*width+x]=(unsigned char)(col.x*255.);		// red
		pTarg2[y*width+x]=(unsigned char)(col.y*255.);		// green
		pTarg3[y*width+x]=(unsigned char)(col.z*255.);		// blue
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// Split24Device_24bit
//	- split 24 colour into 3 24 bit colour images
//	  R component into first target, G comp into second one, B comp into third one
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  Split24Device_24bit(uchar4 *pTarg1, uchar4 *pTarg2, uchar4 *pTarg3, 
									 int width, int height)
{ 
	float4	col;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		col=tex2D(tex4UC1, x, y);
		pTarg1[y*width+x].x=(unsigned char)(col.x*255.);	// red
		pTarg1[y*width+x].y=0;
		pTarg1[y*width+x].z=0;
		pTarg1[y*width+x].w=0;
		pTarg2[y*width+x].x=0;
		pTarg2[y*width+x].y=(unsigned char)(col.y*255.);	// green
		pTarg2[y*width+x].z=0;
		pTarg2[y*width+x].w=0;
		pTarg3[y*width+x].x=0;
		pTarg3[y*width+x].y=0;
		pTarg3[y*width+x].z=(unsigned char)(col.z*255.);	// blue
		pTarg3[y*width+x].w=0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// Split48Device_16bit
// - split 48 colour into 3 16 bit greyvalue images
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  Split48Device_16bit(unsigned short *pTarg1, unsigned short *pTarg2,
									 unsigned short *pTarg3, int width, int height)
{ 
	float4	col;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		col=tex2D(tex4US1, x, y);
		pTarg1[y*width+x]=(unsigned char)(col.x*65535.);		// red
		pTarg2[y*width+x]=(unsigned char)(col.y*65535.);		// green
		pTarg3[y*width+x]=(unsigned char)(col.z*65535.);		// blue
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// Split48Device_48bit
// - split 48 colour into 3 48 bit colour images
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  Split48Device_48bit(ushort4 *pTarg1, ushort4 *pTarg2,
									 ushort4* pTarg3, int width, int height)
{ 
	float4	col;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		col=tex2D(tex4UC1, x, y);
		pTarg1[y*width+x].x=(unsigned char)(col.x*65535.);	// red
		pTarg1[y*width+x].y=0;
		pTarg1[y*width+x].z=0;
		pTarg1[y*width+x].w=0;
		pTarg2[y*width+x].x=0;
		pTarg2[y*width+x].y=(unsigned char)(col.y*65535.);	// green
		pTarg2[y*width+x].z=0;
		pTarg2[y*width+x].w=0;
		pTarg3[y*width+x].x=0;
		pTarg3[y*width+x].y=0;
		pTarg3[y*width+x].z=(unsigned char)(col.z*65535.);	// blue
		pTarg3[y*width+x].w=0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaComponentSplit
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaComponentSplit(cudaArray *src, void *target1, void *target2, 
								   void *target3,  int width, int height, int bppSrc, 
								   int bppTarg)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bppSrc)
	{
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src));
				if (bppTarg==8)
					Split24Device_8bit<<< dimGrid, dimBlock>>>(
						(unsigned char *)target1, (unsigned char *)target2, 
						(unsigned char *)target3, width, height);
				else
					Split24Device_24bit<<< dimGrid, dimBlock>>>((uchar4 *)target1, 
						(uchar4 *)target2, (uchar4 *)target3, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src));
				if (bppTarg==16)
					Split48Device_16bit<<< dimGrid, dimBlock>>>(
						(unsigned short *)target1, (unsigned short *)target2, 
						(unsigned short *)target3, width, height);
				else
					Split48Device_48bit<<< dimGrid, dimBlock>>>((ushort4 *)target1, 
						(ushort4 *)target2, (ushort4 *)target3, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// Join8Device_24bit
//	- Join 3 8bit greyvalue images into a 24 bit colour image
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  Join8Device_24bit(uchar4 *pTarg, int width, int height)
{
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		pTarg[y*width+x].x=(unsigned char)(tex2D(texUC1, x, y)*255.);
		pTarg[y*width+x].y=(unsigned char)(tex2D(texUC2, x, y)*255.);
		pTarg[y*width+x].z=(unsigned char)(tex2D(texUC3, x, y)*255.);
		pTarg[y*width+x].w=0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// Join16Device_48bit
//	- Join 3 16 bit greyvalue images into a 48 bit colour image
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  Join16Device_48bit(ushort4 *pTarg, int width, int height)
{ 
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		pTarg[y*width+x].x=(unsigned short)(tex2D(texUS1, x, y)*65535.);
		pTarg[y*width+x].y=(unsigned short)(tex2D(texUS2, x, y)*65535.);
		pTarg[y*width+x].z=(unsigned short)(tex2D(texUS3, x, y)*65535.);
		pTarg[y*width+x].w=0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// Join24Device_24bit
//	- Join 3 24 bit colour images into a 24 bit colour image
//	  R component from first source, G comp from second one, B comp from third one
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  Join24Device_24bit(uchar4 *pTarg, int width, int height)
{ 
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		pTarg[y*width+x].x=(unsigned char)(tex2D(tex4UC1, x, y).x*255.);
		pTarg[y*width+x].y=(unsigned char)(tex2D(tex4UC2, x, y).y*255.);
		pTarg[y*width+x].z=(unsigned char)(tex2D(tex4UC3, x, y).z*255.);
		pTarg[y*width+x].w=0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// Join48Device_48bit
//	- Join 3 48 bit colour images into a 48 bit colour image
//	  R component from first source, G comp from second one, B comp from third one
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  Join48Device_48bit(ushort4 *pTarg, int width, int height)
{ 
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		pTarg[y*width+x].x=(unsigned short)(tex2D(tex4US1, x, y).x*65535.);
		pTarg[y*width+x].y=(unsigned short)(tex2D(tex4US2, x, y).y*65535.);
		pTarg[y*width+x].z=(unsigned short)(tex2D(tex4US3, x, y).z*65535.);
		pTarg[y*width+x].w=0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaComponentJoin
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaComponentJoin(cudaArray *src1, cudaArray *src2, cudaArray *src3, 
								  void *target, int width, int height, int bppSrc)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bppSrc)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC2, src2));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC3, src3));
				Join8Device_24bit<<< dimGrid, dimBlock>>>((uchar4 *)target, 
						width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC3));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS2, src2));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS3, src3));
				Join16Device_48bit<<< dimGrid, dimBlock>>>((ushort4 *)target, 
						width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS3));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC2, src2));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC3, src3));
				Join24Device_24bit<<< dimGrid, dimBlock>>>((uchar4 *)target, 
						width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC3));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US2, src2));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US3, src3));
				Join48Device_48bit<<< dimGrid, dimBlock>>>((ushort4 *)target, 
						width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US3));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CompThresh_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CompThreshDevice_24bit(uchar4 *pTarg, int width, int height)
{ 
	float4	col;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;

	if ((y<height) && (x<width))
	{
		col=tex2D(tex4UC1, x, y);
		if (col.x < d_ThreshR0) col.x=0;
		if (col.x > d_ThreshR1) col.x=0;
		if (col.y < d_ThreshG0) col.y=0;
		if (col.y > d_ThreshG1) col.y=0;
		if (col.z < d_ThreshB0) col.z=0;
		if (col.z > d_ThreshB1) col.z=0;
		pTarg[y*width+x]=_uchar4(col*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CompThresh_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CompThreshDevice_48bit(ushort4 *pTarg, int width, int height)
{ 
	float4	col;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;

	if ((y<height) && (x<width))
	{
		col=tex2D(tex4US1, x, y);
		if (col.x < d_ThreshR0) col.x=0;
		if (col.x > d_ThreshR1) col.x=0;
		if (col.y < d_ThreshG0) col.y=0;
		if (col.y > d_ThreshG1) col.y=0;
		if (col.z < d_ThreshB0) col.z=0;
		if (col.z > d_ThreshB1) col.z=0;
		pTarg[y*width+x]=_ushort4(col*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaInitThresholdConst
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaInitThresholdConst(float r0, float r1, float g0, float g1, 
									   float b0, float b1)
{
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_ThreshR0", &r0, sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_ThreshR1", &r1, sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_ThreshG0", &g0, sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_ThreshG1", &g1, sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_ThreshB0", &b0, sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_ThreshB1", &b1, sizeof(float)));
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaComponentThreshold
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaComponentThreshold (cudaArray *src, void *target, int width, 
										int height, int bpp)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bpp)
	{
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src));
				CompThreshDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)target, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src));
				CompThreshDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)target, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CompBlend8Device_24bit
//	- Blend 3 8bit greyvalue images into a 24 bit colour image
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CompBlend8Device_24bit(uchar4 *pTarg, int width, int height)
{ 
	float4 col;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		col.x=tex2D(texUC1, x, y)*d_BlendR;
		col.y=tex2D(texUC2, x, y)*d_BlendG;
		col.z=tex2D(texUC3, x, y)*d_BlendB;
		col.w=0;
		col=clamp(col, 0.0, 1.0);
		pTarg[y*width+x]=_uchar4(col*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CompBlend16Device_48bit
//	- Blend 3 16 bit greyvalue images into a 48 bit colour image
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CompBlend16Device_48bit(ushort4 *pTarg, int width, int height)
{ 
	float4 col;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		col.x=tex2D(texUS1, x, y)*d_BlendR;
		col.y=tex2D(texUS2, x, y)*d_BlendG;
		col.z=tex2D(texUS3, x, y)*d_BlendB;
		col.w=0;
		col=clamp(col, 0.0, 1.0);
		pTarg[y*width+x]=_ushort4(col*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CompBlend24Device_24bit
//	- Blend 3 24 bit colour images into a 24 bit colour image
//	  R component from first source, G comp from second one, B comp from third one
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CompBlend24Device_24bit(uchar4 *pTarg, int width, int height)
{ 
	float4 col, val;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4UC1, x, y);
		col.x=val.x*d_BlendR;
		val=tex2D(tex4UC2, x, y);
		col.y=val.y*d_BlendG;
		val=tex2D(tex4UC3, x, y);
		col.z=val.z*d_BlendB;
		col.w=0;
		col=clamp(col, 0.0, 1.0);
		pTarg[y*width+x]=_uchar4(col*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CompBlend48Device_48bit
//	- Blend 3 48 bit colour images into a 48 bit colour image
//	  R component from first source, G comp from second one, B comp from third one
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CompBlend48Device_48bit(ushort4 *pTarg, int width, int height)
{ 
	float4 col, val;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4US1, x, y);
		col.x=val.x*d_BlendR;
		val=tex2D(tex4US2, x, y);
		col.y=val.y*d_BlendG;
		val=tex2D(tex4US3, x, y);
		col.z=val.z*d_BlendB;
		col.w=0;
		col=clamp(col, 0.0, 1.0);
		pTarg[y*width+x]=_ushort4(col*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaInitBlendConst
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaInitBlendConst(float a1, float a2, float a3)
{
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_BlendR", &a1, sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_BlendG", &a2, sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_BlendB", &a3, sizeof(float)));
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaComponentBlend
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaComponentBlend(cudaArray *src1, cudaArray *src2, cudaArray *src3, 
								   void *target, int width, int height, int bppSrc)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bppSrc)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC2, src2));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC3, src3));
				CompBlend8Device_24bit<<< dimGrid, dimBlock>>>((uchar4 *)target, 
						width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC3));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS2, src2));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS3, src3));
				CompBlend16Device_48bit<<< dimGrid, dimBlock>>>((ushort4 *)target, 
						width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS3));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC2, src2));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC3, src3));
				CompBlend24Device_24bit<<< dimGrid, dimBlock>>>((uchar4 *)target, 
						width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC3));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US2, src2));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US3, src3));
				CompBlend48Device_48bit<<< dimGrid, dimBlock>>>((ushort4 *)target, 
						width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US3));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// ComposedBlendDevice_24bit
//	- Blend components of a 24 bit colour image into a new 24 bit colour image
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  ComposedBlendDevice_24bit(uchar4 *pTarg, int width, int height)
{ 
	float4 col;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		col=tex2D(tex4UC1, x, y);
		col.x *=d_BlendR;
		col.y *=d_BlendG;
		col.z *=d_BlendB;
		col.w=0.;
		col=clamp(col, 0.0, 1.0);
		pTarg[y*width+x]=_uchar4(col*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// ComposedBlendDevice_48bit
//	- Blend components of a 48 bit colour image into a new 48 bit colour image
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  ComposedBlendDevice_48bit(ushort4 *pTarg, int width, int height)
{ 
	float4 col;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		col=tex2D(tex4US1, x, y);
		col.x *=d_BlendR;
		col.y *=d_BlendG;
		col.z *=d_BlendB;
		col.w=0.;
		col=clamp(col, 0.0, 1.0);
		pTarg[y*width+x]=_ushort4(col*65535.);
	}
}
///////////////////////////////////////////////////////////////////////////////////////
// CudaComposedBlend
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaComposedBlend (cudaArray *src, void *target, int width, 
										int height, int bpp)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bpp)
	{
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src));
				ComposedBlendDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)target, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src));
				ComposedBlendDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)target, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// RGBHSIDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  RGBHSIDevice_24bit(uchar4 *pTarg, int width, int height)
{ 
    float  delta, colorMax, colorMin;
    float4 hsv, col;
	unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;

	if ((y<height) && (x<width))
	{
		hsv.x=(float)0.;
		hsv.y=(float)0.;
		hsv.w=(float)0.;
		col=tex2D(tex4UC1, x, y);
		colorMax = max(col.x, col.y);
		colorMax = max(colorMax, col.z);
		colorMin = min(col.x, col.y);
		colorMin = min(colorMin, col.z);
		hsv.z = colorMax;
		if (colorMax != 0.) hsv.y = (colorMax - colorMin ) / colorMax;
		if (hsv.y != 0.)
		{
			delta = colorMax - colorMin;
			if( col.x == colorMax ) hsv.x = (col.y-col.z)/delta;
				else if (col.y == colorMax) hsv.x = 2.0 + (col.z-col.x)/delta;
				else hsv.x = 4.0 + (col.x-col.y)/delta;
			hsv.x *= 1./6.;		//convert to normalized deegrees
			if (hsv.x < 0.) hsv.x +=1.0;
		}
		pTarg[y*width+x]=_uchar4(hsv*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// RGBHSIDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  RGBHSIDevice_48bit(ushort4 *pTarg, int width, int height)
{ 
    float  delta, colorMax, colorMin;
    float4 hsv, col;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		hsv.x=(float)0.;
		hsv.y=(float)0.;
		hsv.w=(float)0.;
		col=tex2D(tex4US1, x, y);
		colorMax = max(col.x, col.y);
		colorMax = max(colorMax, col.z);
		colorMin = min(col.x, col.y);
		colorMin = min(colorMin, col.z);
		hsv.z = colorMax;
		if (colorMax != 0.) hsv.y = (colorMax - colorMin ) / colorMax;
		if (hsv.y != 0.)
		{
			delta = colorMax - colorMin;
			if( col.x == colorMax ) hsv.x = (col.y-col.z)/delta;
				else if (col.y == colorMax) hsv.x = 2.0 + (col.z-col.x)/delta;
				else hsv.x = 4.0 + (col.x-col.y)/delta;
			hsv.x *= 1./6.;		//convert to normalized deegrees
			if (hsv.x < 0.) hsv.x +=1.0;
		}
		pTarg[y*width+x]=_ushort4(hsv*65535.);
	}
}


///////////////////////////////////////////////////////////////////////////////////////
// CudaRGBHSI
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaRGBHSI(cudaArray *src, void *target, int width,  int height, int bpp)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bpp)
	{
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src));
				RGBHSIDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)target, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src));
				RGBHSIDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)target, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// HSIRGBDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  HSIRGBDevice_24bit(uchar4 *pTarg, int width, int height)
{ 
    float4	col, hsv;
    float	f, p, q, t;
    int		i;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		hsv=tex2D(tex4UC1, x, y);
		if (hsv.y==0.0)
		{
			col.x=hsv.z;
			col.y=hsv.z;
			col.z=hsv.z;
		}
		else
		{
			hsv.x = hsv.x * 360.0;
			if( hsv.x == 360.0 ) hsv.x=0;
			hsv.x /=60.;
			i = floor( hsv.x );
			f = hsv.x-(float)i;
			p = hsv.z * (1.0 -hsv.y );
			q = hsv.z * (1.0 -(hsv.y * f ));
			t = hsv.z * (1.0 -(hsv.y * (1.0 -f )));
			if( i == 0) {col.x = hsv.z; col.y = t; col.z = p;}
				else if( i== 1 ) {col.x = q; col.y = hsv.z; col.z = p;}
				else if( i== 2 ) {col.x = p; col.y = hsv.z; col.z = t;}
				else if( i== 3 ) {col.x = p; col.y = q; col.z = hsv.z;}
				else if( i== 4 ) {col.x = t; col.y = p; col.z = hsv.z;}
				else if( i== 5 ) {col.x = hsv.z; col.y = p; col.z = q;}
		}
		col.w=0.;
		pTarg[y*width+x]=_uchar4(col*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// HSIRGBDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  HSIRGBDevice_48bit(ushort4 *pTarg, int width, int height)
{ 
    float4	col, hsv;
    float	f, p, q, t;
    int		i;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height)&& (x<width))
	{
		hsv=tex2D(tex4US1, x, y);
		if (hsv.y==0.0)
		{
			col.x=hsv.z;
			col.y=hsv.z;
			col.z=hsv.z;
		}
		else
		{
			hsv.x = hsv.x * 360.0;
			if( hsv.x == 360.0 ) hsv.x=0;
			hsv.x /=60.;
			i = floor( hsv.x );
			f = hsv.x-(float)i;
			p = hsv.z * (1.0 -hsv.y );
			q = hsv.z * (1.0 -(hsv.y * f ));
			t = hsv.z * (1.0 -(hsv.y * (1.0 -f )));
			if( i == 0) {col.x = hsv.z; col.y = t; col.z = p;}
				else if( i== 1 ) {col.x = q; col.y = hsv.z; col.z = p;}
				else if( i== 2 ) {col.x = p; col.y = hsv.z; col.z = t;}
				else if( i== 3 ) {col.x = p; col.y = q; col.z = hsv.z;}
				else if( i== 4 ) {col.x = t; col.y = p; col.z = hsv.z;}
				else if( i== 5 ) {col.x = hsv.z; col.y = p; col.z = q;}
		}
		col.w=0.;
		pTarg[y*width+x]=_ushort4(col*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaHSIRGB
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaHSIRGB(cudaArray *src, void *target, int width,  int height, int bpp)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bpp)
	{
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src));
				HSIRGBDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)target, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src));
				HSIRGBDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)target, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// ColourDeconvDevice_24bit
//	- Blend components of a 24 bit colour image into a new 24 bit colour image
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  ColourDeconvDevice_24bit(uchar4 *pTarg, int width, int height)
{ 
	float4 col, outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		col=tex2D(tex4UC1, x, y);
		col.x=-logf(col.x+1./255.);
		col.y=-logf(col.y+1./255.);
		col.z=-logf(col.z+1./255.);
		outp.x=col.x*d_DeconvVec[0].x+col.y*d_DeconvVec[0].y+col.z*d_DeconvVec[0].z;
		outp.y=col.x*d_DeconvVec[1].x+col.y*d_DeconvVec[1].y+col.z*d_DeconvVec[1].z;
		outp.z=col.x*d_DeconvVec[2].x+col.y*d_DeconvVec[2].y+col.z*d_DeconvVec[2].z;
		outp.x=expf(logf(255.)-outp.x)/255.;
		outp.y=expf(logf(255.)-outp.y)/255.;
		outp.z=expf(logf(255.)-outp.z)/255.;
		outp.w=0;
		outp=clamp(outp, 0.0, 1.0);
		pTarg[y*width+x]=_uchar4(outp*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// ColourDeconvDevice_48bit
//	- Colour Deconvolution of a 48 bit colour image into a new48 bit colour image
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  ColourDeconvDevice_48bit(ushort4 *pTarg, int width, int height)
{ 
	float4 col, outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		col=tex2D(tex4US1, x, y);
		col.x=-logf(col.x+1./255.);
		col.y=-logf(col.y+1./255.);
		col.z=-logf(col.z+1./255.);
		outp.x=col.x*d_DeconvVec[0].x+col.y*d_DeconvVec[0].y+col.z*d_DeconvVec[0].z;
		outp.y=col.x*d_DeconvVec[1].x+col.y*d_DeconvVec[1].y+col.z*d_DeconvVec[1].z;
		outp.z=col.x*d_DeconvVec[2].x+col.y*d_DeconvVec[2].y+col.z*d_DeconvVec[2].z;
		outp.x=expf(logf(255.)-outp.x)/255.;
		outp.y=expf(logf(255.)-outp.y)/255.;
		outp.z=expf(logf(255.)-outp.z)/255.;
		outp.w=0;
		outp=clamp(outp, 0.0, 1.0);
		pTarg[y*width+x]=_ushort4(outp*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaColourDeconv
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaColourDeconv (cudaArray *src, void *target, int width, 
										int height, int bpp, float4 *qVec)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);
	CUDA_SAFE_CALL(cudaMemcpyToSymbol(d_DeconvVec, qVec, 3*sizeof(float4)));

	switch (bpp)
	{
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src));
				ColourDeconvDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)target, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src));
				ColourDeconvDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)target, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}
