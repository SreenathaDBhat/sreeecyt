
#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cutil.h>
#include "cudamath.h"
#include "CudaTIP_Kernels.h"


texture<float, cudaTextureType2D, cudaReadModeElementType> texF1;

texture<float2, cudaTextureType2D, cudaReadModeElementType> texComplex;

__device__ __constant__ float d_NormFactor;

///////////////////////////////////////////////////////////////////////////////////////
// ConvertDev_8to8bit, ConvertDev_8to16bit, ConvertDev_8to24bit, ConvertDev_8to48bit,
// ConvertDev_8to64bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  ConvertDev_8to8bit(unsigned char *pTarg, int width, int height)
{ 
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texUC1, x, y);
		pTarg[y*width+x]=(unsigned char)(outp*d_NormFactor*255.);
	}
}

__global__ void  ConvertDev_8to16bit(unsigned short *pTarg, int width, int height)
{ 
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texUC1, x, y);
		pTarg[y*width+x]=(unsigned short)(outp*d_NormFactor*65535.);
	}
}

__global__ void  ConvertDev_8to24bit(uchar4 *pTarg, int width, int height)
{ 
	float4 col;
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texUC1, x, y);
		col.x=outp;
		col.y=outp;
		col.z=outp;
		col.w=0;
		pTarg[y*width+x]=_uchar4(col*d_NormFactor*255.);
	}
}

__global__ void  ConvertDev_8to32bit(float *pTarg, int width, int height)
{ 
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texUC1, x, y);
		pTarg[y*width+x]=outp*d_NormFactor;
	}
}

__global__ void  ConvertDev_8to48bit(ushort4 *pTarg, int width, int height)
{ 
	float4 col;
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texUC1, x, y);
		col.x=outp;
		col.y=outp;
		col.z=outp;
		col.w=0;
		pTarg[y*width+x]=_ushort4(col*d_NormFactor*65535.);
	}
}

__global__ void  ConvertDev_8to64bit(float2 *pTarg, int width, int height)
{ 
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texUC1, x, y);
		pTarg[y*width+x].x=outp*d_NormFactor;
		pTarg[y*width+x].y=0;
	}
}


////////////////////////////////////////////////////////////////////////////////////////
// ConvertDev_16to8bit, ConvertDev_16to16bit, ConvertDev_16to24bit,
// ConvertDev_16to48bit, ConvertDev_16to64bit
////////////////////////////////////////////////////////////////////////////////////////
__global__ void  ConvertDev_16to8bit(unsigned char *pTarg, int width, int height)
{ 
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texUS1, x, y);
		pTarg[y*width+x]=(unsigned char)(outp*d_NormFactor*255.);
	}
}

__global__ void  ConvertDev_16to16bit(unsigned short *pTarg, int width, int height)
{ 
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texUS1, x, y);
		pTarg[y*width+x]=(unsigned short)(outp*d_NormFactor*65535.);
	}
}

__global__ void  ConvertDev_16to24bit(uchar4 *pTarg, int width, int height)
{ 
	float4 col;
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texUS1, x, y);
		col.x=outp;
		col.y=outp;
		col.z=outp;
		col.w=0;
		pTarg[y*width+x]=_uchar4(col*d_NormFactor*255.);
	}
}

__global__ void  ConvertDev_16to32bit(float *pTarg, int width, int height)
{ 
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texUS1, x, y);
		pTarg[y*width+x]=d_NormFactor*outp;
	}
}

__global__ void  ConvertDev_16to48bit(ushort4 *pTarg, int width, int height)
{ 
	float4 col;
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texUS1, x, y);
		col.x=outp;
		col.y=outp;
		col.z=outp;
		col.w=0;
		pTarg[y*width+x]=_ushort4(col*d_NormFactor*65535.);
	}
}

__global__ void  ConvertDev_16to64bit(float2 *pTarg, int width, int height)
{ 
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texUS1, x, y);
		pTarg[y*width+x].x=outp*d_NormFactor;
		pTarg[y*width+x].y=0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// ConvertDev_24to8bit, ConvertDev_24to16bit, ConvertDev_24to24bit, 
// ConvertDev_24to48bit, ConvertDev_24to64bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  ConvertDev_24to8bit(unsigned char *pTarg, int width, int height)
{ 
	float4 outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(tex4UC1, x, y);
		pTarg[y*width+x]=(unsigned char)((outp.x*0.3+ outp.y*0.59+outp.z*0.11)*d_NormFactor*255.);
	}
}

__global__ void  ConvertDev_24to16bit(unsigned short *pTarg, int width, int height)
{ 
	float4 outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(tex4UC1, x, y);
		pTarg[y*width+x]=(unsigned short)((outp.x*0.3+ outp.y*0.59+outp.z*0.11)*d_NormFactor*65535.);
	}
}

__global__ void  ConvertDev_24to24bit(uchar4 *pTarg, int width, int height)
{ 
	float4 outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(tex4UC1, x, y);
		pTarg[y*width+x]=_uchar4(outp*d_NormFactor*255.);
	}
}

__global__ void  ConvertDev_24to32bit(float *pTarg, int width, int height)
{ 
	float4 outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(tex4UC1, x, y);
		pTarg[y*width+x]=(outp.x*0.3+ outp.y*0.59+outp.z*0.11)*d_NormFactor;
	}
}

__global__ void  ConvertDev_24to48bit(ushort4 *pTarg, int width, int height)
{ 
	float4 outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(tex4UC1, x, y);
		pTarg[y*width+x]=_ushort4(outp*d_NormFactor*65535.);
	}
}

__global__ void  ConvertDev_24to64bit(float2 *pTarg, int width, int height)
{ 
	float4 outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(tex4UC1, x, y);
		pTarg[y*width+x].x=(outp.x*0.3+ outp.y*0.59+outp.z*0.11)*d_NormFactor;
		pTarg[y*width+x].y=0;
	}
}

////////////////////////////////////////////////////////////////////////////////////////
// ConvertDev_32to8bit, ConvertDev_32to16bit, ConvertDev_32to24bit, 
// ConvertDev_32to32bit, ConvertDev_32to48bit, ConvertDev_32to64bit
////////////////////////////////////////////////////////////////////////////////////////
__global__ void  ConvertDev_32to8bit(unsigned char *pTarg, int width, int height)
{ 
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		

		outp = tex2D( texF1, (float) x, (float) y);
		pTarg[y*width+x]=(unsigned char)(outp*d_NormFactor*255.);
	}
}

__global__ void  ConvertDev_32to16bit(unsigned short *pTarg, int width, int height)
{ 
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texF1, x, y);
		pTarg[y*width+x]=(unsigned short)(outp*d_NormFactor*65535.);
	}
}

__global__ void  ConvertDev_32to24bit(uchar4 *pTarg, int width, int height)
{ 
	float4 col;
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texF1, x, y);
		col.x=outp;
		col.y=outp;
		col.z=outp;
		col.w=0;
		pTarg[y*width+x]=_uchar4(col*d_NormFactor*255.);
	}
}

__global__ void  ConvertDev_32to32bit(float *pTarg, int width, int height)
{ 
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texF1, x, y);
		pTarg[y*width+x]=outp*d_NormFactor;
	}
}

__global__ void  ConvertDev_32to48bit(ushort4 *pTarg, int width, int height)
{ 
	float4 col;
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texF1, x, y);
		col.x=outp;
		col.y=outp;
		col.z=outp;
		col.w=0;
		pTarg[y*width+x]=_ushort4(col*d_NormFactor*65535.);
	}
}

__global__ void  ConvertDev_32to64bit(float2 *pTarg, int width, int height)
{ 
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texF1, x, y);
		pTarg[y*width+x].x=outp*d_NormFactor;
		pTarg[y*width+x].y=0;
	}
}

////////////////////////////////////////////////////////////////////////////////////////
// ConvertDev_48to8bit, ConvertDev_48to16bit, ConvertDev_48to24bit, ConvertDev_48to48bit,
// ConvertDev_48to64bit
////////////////////////////////////////////////////////////////////////////////////////
__global__ void  ConvertDev_48to8bit(unsigned char *pTarg, int width, int height)
{ 
	float4 outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(tex4US1, x, y);
		pTarg[y*width+x]=(unsigned char)((outp.x*0.3+ outp.y*0.59+outp.z*0.11)*d_NormFactor*255.);
	}
}

__global__ void  ConvertDev_48to16bit(unsigned short *pTarg, int width, int height)
{ 
	float4 outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(tex4US1, x, y);
		pTarg[y*width+x]=(unsigned short)((outp.x*0.3+ outp.y*0.59+outp.z*0.11)*d_NormFactor*65535.);
	}
}

__global__ void  ConvertDev_48to24bit(uchar4 *pTarg, int width, int height)
{ 
	float4 outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(tex4US1, x, y);
		pTarg[y*width+x]=_uchar4(outp*d_NormFactor*255.);
	}
}

__global__ void  ConvertDev_48to32bit(float *pTarg, int width, int height)
{ 
	float4 outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(tex4US1, x, y);
		pTarg[y*width+x]=(outp.x*0.3+ outp.y*0.59+outp.z*0.11)*d_NormFactor;
	}
}

__global__ void  ConvertDev_48to48bit(ushort4 *pTarg, int width, int height)
{ 
	float4 outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(tex4US1, x, y);
		pTarg[y*width+x]=_ushort4(outp*d_NormFactor*65535.);
	}
}

__global__ void  ConvertDev_48to64bit(float2 *pTarg, int width, int height)
{ 
	float4 outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(tex4US1, x, y);
		pTarg[y*width+x].x=(outp.x*0.3+ outp.y*0.59+outp.z*0.11)*d_NormFactor;
		pTarg[y*width+x].y=0;
	}
}

////////////////////////////////////////////////////////////////////////////////////////
// ConvertDev_64to8bit, ConvertDev_64to16bit, ConvertDev_64to24bit, 
// ConvertDev_64to32bit, ConvertDev_64to48bit, ConvertDev_64to64bit
////////////////////////////////////////////////////////////////////////////////////////
__global__ void  ConvertDev_64to8bit(unsigned char *pTarg, int width, int height)
{ 
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texComplex, x, y).x;
		pTarg[y*width+x]=(unsigned char)(outp*d_NormFactor*255.);
	}
}

__global__ void  ConvertDev_64to16bit(unsigned short *pTarg, int width, int height)
{ 
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texComplex, x, y).x;
		pTarg[y*width+x]=(unsigned short)(outp*d_NormFactor*65535.);
	}
}

__global__ void  ConvertDev_64to24bit(uchar4 *pTarg, int width, int height)
{ 
	float4 col;
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texComplex, x, y).x;
		col.x=outp;
		col.y=outp;
		col.z=outp;
		col.w=0;
		pTarg[y*width+x]=_uchar4(col*d_NormFactor*255.);
	}
}

__global__ void  ConvertDev_64to32bit(float *pTarg, int width, int height)
{ 
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texComplex, x, y).x;
		pTarg[y*width+x]=outp*d_NormFactor;
	}
}

__global__ void  ConvertDev_64to48bit(ushort4 *pTarg, int width, int height)
{ 
	float4 col;
	float outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texComplex, x, y).x;
		col.x=outp;
		col.y=outp;
		col.z=outp;
		col.w=0;
		pTarg[y*width+x]=_ushort4(col*d_NormFactor*65535.);
	}
}

__global__ void  ConvertDev_64to64bit(float2 *pTarg, int width, int height)
{ 
	float2 outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp=tex2D(texComplex, x, y);
		pTarg[y*width+x]=outp*d_NormFactor;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaConvert
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaConvert(cudaArray *src, void *target, int width, int height, 
								 int bpp, int bppdest, float normFactor)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_NormFactor", &normFactor, sizeof(float)));
	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src));
				switch (bppdest)
				{
					case 8:
							ConvertDev_8to8bit<<< dimGrid, dimBlock>>>((unsigned char *)target, 
								width, height);
							break;
					case 16:
							ConvertDev_8to16bit<<< dimGrid, dimBlock>>>((unsigned short *)target, 
								width, height);
							break;
					case 24:
							ConvertDev_8to24bit<<< dimGrid, dimBlock>>>((uchar4 *)target, 
								width, height);
							break;
					case 32:
							ConvertDev_8to32bit<<< dimGrid, dimBlock>>>((float *)target, 
								width, height);
							break;
					case 48:
							ConvertDev_8to48bit<<< dimGrid, dimBlock>>>((ushort4 *)target, 
								width, height);
							break;
					case 64:
							ConvertDev_8to64bit<<< dimGrid, dimBlock>>>((float2 *)target, 
								width, height);
							break;
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src));
				switch (bppdest)
				{
					case 8:
							ConvertDev_16to8bit<<< dimGrid, dimBlock>>>((unsigned char *)target, 
								width, height);
							break;
					case 16:
							ConvertDev_16to16bit<<< dimGrid, dimBlock>>>((unsigned short *)target, 
								width, height);
							break;
					case 24:
							ConvertDev_16to24bit<<< dimGrid, dimBlock>>>((uchar4 *)target, 
								width, height);
							break;
					case 32:
							ConvertDev_16to32bit<<< dimGrid, dimBlock>>>((float *)target, 
								width, height);
							break;
					case 48:
							ConvertDev_16to48bit<<< dimGrid, dimBlock>>>((ushort4 *)target, 
								width, height);
							break;
					case 64:
							ConvertDev_16to64bit<<< dimGrid, dimBlock>>>((float2 *)target, 
								width, height);
							break;
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src));
				switch (bppdest)
				{
					case 8:
							ConvertDev_24to8bit<<< dimGrid, dimBlock>>>((unsigned char *)target, 
								width, height);
							break;
					case 16:
							ConvertDev_24to16bit<<< dimGrid, dimBlock>>>((unsigned short *)target, 
								width, height);
							break;
					case 24:
							ConvertDev_24to24bit<<< dimGrid, dimBlock>>>((uchar4 *)target, 
								width, height);
							break;
					case 32:
							ConvertDev_24to32bit<<< dimGrid, dimBlock>>>((float *)target, 
								width, height);
							break;
					case 48:
							ConvertDev_24to48bit<<< dimGrid, dimBlock>>>((ushort4 *)target, 
								width, height);
							break;
					case 64:
							ConvertDev_24to64bit<<< dimGrid, dimBlock>>>((float2 *)target, 
								width, height);
							break;
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 32:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texF1, src));
				switch (bppdest)
				{
					case 8:
							ConvertDev_32to8bit<<< dimGrid, dimBlock>>>((unsigned char *)target, 
								width, height);
							break;
					case 16:
							ConvertDev_32to16bit<<< dimGrid, dimBlock>>>((unsigned short *)target, 
								width, height);
							break;
					case 24:
							ConvertDev_32to24bit<<< dimGrid, dimBlock>>>((uchar4 *)target, 
								width, height);
							break;
					case 32:
							ConvertDev_32to32bit<<< dimGrid, dimBlock>>>((float *)target, 
								width, height);
							break;
					case 48:
							ConvertDev_32to48bit<<< dimGrid, dimBlock>>>((ushort4 *)target, 
								width, height);
							break;
					case 64:
							ConvertDev_32to64bit<<< dimGrid, dimBlock>>>((float2 *)target, 
								width, height);
							break;
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(texF1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src));
				switch (bppdest)
				{
					case 8:
							ConvertDev_48to8bit<<< dimGrid, dimBlock>>>((unsigned char *)target, 
								width, height);
							break;
					case 16:
							ConvertDev_48to16bit<<< dimGrid, dimBlock>>>((unsigned short *)target, 
								width, height);
							break;
					case 24:
							ConvertDev_48to24bit<<< dimGrid, dimBlock>>>((uchar4 *)target, 
								width, height);
							break;
					case 32:
							ConvertDev_48to32bit<<< dimGrid, dimBlock>>>((float *)target, 
								width, height);
							break;
					case 48:
							ConvertDev_48to48bit<<< dimGrid, dimBlock>>>((ushort4 *)target, 
								width, height);
							break;
					case 64:
							ConvertDev_48to64bit<<< dimGrid, dimBlock>>>((float2 *)target, 
								width, height);
							break;
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
		case 64:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texComplex, src));
				switch (bppdest)
				{
					case 8:
							ConvertDev_64to8bit<<< dimGrid, dimBlock>>>((unsigned char *)target, 
								width, height);
							break;
					case 16:
							ConvertDev_64to16bit<<< dimGrid, dimBlock>>>((unsigned short *)target, 
								width, height);
							break;
					case 24:
							ConvertDev_64to24bit<<< dimGrid, dimBlock>>>((uchar4 *)target, 
								width, height);
							break;
					case 32:
							ConvertDev_64to32bit<<< dimGrid, dimBlock>>>((float *)target, 
								width, height);
							break;
					case 48:
							ConvertDev_64to48bit<<< dimGrid, dimBlock>>>((ushort4 *)target, 
								width, height);
							break;
					case 64:
							ConvertDev_64to64bit<<< dimGrid, dimBlock>>>((float2 *)target, 
								width, height);
							break;
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(texComplex));
				break;
	}
}
