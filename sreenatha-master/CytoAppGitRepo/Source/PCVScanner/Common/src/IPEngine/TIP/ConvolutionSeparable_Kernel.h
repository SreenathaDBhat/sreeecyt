#define MAX_KERNELSIZE	41
extern "C" void InitGaussianFilter(int FilterHW, float Sigma, int bpp);
extern "C" void CudaConvolution(void *src, void *dest, void *tmpIma, int width, 
								int height, int bpp, int filterHW);

