#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cutil.h>
#include "cudamath.h"
#include "CudaTIP_Kernels.h"

////////////////////////////////////////////////////////////////////////////////
// Kernel configuration
////////////////////////////////////////////////////////////////////////////////

__device__ __constant__ float d_HKernel[MAX_KERNELSIZE];
__device__ __constant__ float d_VKernel[MAX_KERNELSIZE];
__device__ __constant__ int d_HKernelRadius;
__device__ __constant__ int d_VKernelRadius;
__device__ __constant__ int d_HKernelRadiusAligned;
__device__ __constant__ float4 nullVal = {0,0,0,0};
__device__ __constant__ float d_Amount;

// Assuming COLUMN_TILE_W and dataW are multiples
// of maximum coalescable read/write size, all global memory operations 
// are coalesced in convolutionColumnGPU()

////////////////////////////////////////////////////////////////////////////////
// ConvRow_8bit
////////////////////////////////////////////////////////////////////////////////
__global__ void ConvRow_8bit(unsigned char *d_Result, unsigned char *d_Data, 
							 int width, int dataH)
{
    //Data cache
    __shared__ float data[MAX_KERNELSIZE-1 + ROW_TILE_W];

    //Current tile and apron limits, relative to row start
    const int         tileStart = IMUL(blockIdx.x, ROW_TILE_W);
    const int           tileEnd = tileStart + ROW_TILE_W - 1;
    const int        apronStart = tileStart - d_HKernelRadius;
    const int          apronEnd = tileEnd   + d_HKernelRadius;

    //Clamp tile and apron limits by image borders
    const int    tileEndClamped = min(tileEnd, width - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, width - 1);

    //Row start index in d_Data[]
    const int          rowStart = IMUL(blockIdx.y, width);

    //Aligned apron start. Assuming dataW and ROW_TILE_W are multiples 
    //of half-warp size, rowStart + apronStartAligned is also a 
    //multiple of half-warp size, thus having proper alignment 
    //for coalesced d_Data[] read.
    const int apronStartAligned = tileStart - d_HKernelRadiusAligned;

    const int loadPos = apronStartAligned + threadIdx.x;
    //Set the entire data cache contents
    //Load global memory values, if indices are within the image borders,
    //or initialize with zeroes otherwise
    if(loadPos >= apronStart)
	{
        const int smemPos = loadPos - apronStart;
		if ((loadPos >= apronStartClamped) && (loadPos <= apronEndClamped))
			data[smemPos] = d_Data[rowStart + loadPos];
		else if (loadPos < apronStartClamped)
			data[smemPos] = d_Data[rowStart + 2 * apronStartClamped - loadPos];
		else
            data[smemPos] = d_Data[rowStart + 2 * apronEndClamped - loadPos];
    }

    //Ensure the completness of the loading stage
    //because results, emitted by each thread depend on the data,
    //loaded by another threads
    __syncthreads();

    const int writePos = tileStart + threadIdx.x;
    //Assuming dataW and ROW_TILE_W are multiples of half-warp size,
    //rowStart + tileStart is also a multiple of half-warp size,
    //thus having proper alignment for coalesced d_Result[] write.
    if(writePos <= tileEndClamped)
	{
        const int smemPos = writePos - apronStart;
        float sum = 0;
        for(int k = -d_HKernelRadius; k <= d_HKernelRadius; k++)
            sum += data[smemPos + k] * d_HKernel[d_HKernelRadius - k];
        d_Result[rowStart + writePos] = sum;
    }
}


////////////////////////////////////////////////////////////////////////////////
// ConvColumn_8bit
////////////////////////////////////////////////////////////////////////////////
__global__ void ConvColumn_8bit(unsigned char *d_Result, unsigned char *d_Data,
								int width, int dataH, int smemStride, 
								int gmemStride)
{
    //Data cache
    __shared__ float data[COLUMN_TILE_W * (MAX_KERNELSIZE-1+ COLUMN_TILE_H)];

    //Current tile and apron limits, in rows
    const int         tileStart = IMUL(blockIdx.y, COLUMN_TILE_H);
    const int           tileEnd = tileStart + COLUMN_TILE_H - 1;
    const int        apronStart = tileStart - d_VKernelRadius;
    const int          apronEnd = tileEnd   + d_VKernelRadius;

    //Clamp tile and apron limits by image borders
    const int    tileEndClamped = min(tileEnd, dataH - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, dataH - 1);

    //Current column index
    const int       columnStart = IMUL(blockIdx.x, COLUMN_TILE_W) + threadIdx.x;

    //Shared and global memory indices for current column
    int smemPos = IMUL(threadIdx.y, COLUMN_TILE_W) + threadIdx.x;
    int gmemPos = IMUL(apronStart + threadIdx.y, width) + columnStart;
    //Cycle through the entire data cache
    //Load global memory values, if indices are within the image borders,
    //or initialize with zero otherwise
    for(int y = apronStart + threadIdx.y; y <= apronEnd; y += blockDim.y)
	{
		if ((y >= apronStartClamped) && (y <= apronEndClamped))
			data[smemPos] = d_Data[gmemPos];
		else if (y < apronStartClamped)
			data[smemPos] = d_Data[gmemPos + IMUL((apronStartClamped-y),
									gmemStride)];
		else
            data[smemPos] = d_Data[gmemPos + IMUL((apronEndClamped-y),
									gmemStride)];
        smemPos += smemStride;
        gmemPos += gmemStride;
    }

    //Ensure the completness of the loading stage
    //because results, emitted by each thread depend on the data, 
    //loaded by another threads
    __syncthreads();

    //Shared and global memory indices for current column
    smemPos = IMUL(threadIdx.y + d_VKernelRadius, COLUMN_TILE_W) + threadIdx.x;
    gmemPos = IMUL(tileStart + threadIdx.y , width) + columnStart;
    //Cycle through the tile body, clamped by image borders
    //Calculate and output the results
    for(int y = tileStart + threadIdx.y; y <= tileEndClamped; y += blockDim.y)
	{
        float sum = 0;
		for(int k = -d_VKernelRadius; k <= d_VKernelRadius; k++)
            sum += data[smemPos + IMUL(k, COLUMN_TILE_W)] *
				   d_VKernel[d_VKernelRadius - k];
		d_Result[gmemPos] = sum;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
}

////////////////////////////////////////////////////////////////////////////////
// ConvRow_16bit
////////////////////////////////////////////////////////////////////////////////
__global__ void ConvRow_16bit(unsigned short *d_Result, unsigned short *d_Data, 
							  int width, int dataH)
{
     __shared__ float data[MAX_KERNELSIZE-1 + ROW_TILE_W];

    const int         tileStart = IMUL(blockIdx.x, ROW_TILE_W);
    const int           tileEnd = tileStart + ROW_TILE_W - 1;
    const int        apronStart = tileStart - d_HKernelRadius;
    const int          apronEnd = tileEnd   + d_HKernelRadius;
    const int    tileEndClamped = min(tileEnd, width - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, width - 1);
    const int          rowStart = IMUL(blockIdx.y, width);
    const int apronStartAligned = tileStart - d_HKernelRadiusAligned;
    const int			loadPos = apronStartAligned + threadIdx.x;

	if(loadPos >= apronStart)
	{
        const int smemPos = loadPos - apronStart;
		if ((loadPos >= apronStartClamped) && (loadPos <= apronEndClamped))
			data[smemPos] = d_Data[rowStart + loadPos];
		else if (loadPos < apronStartClamped)
			data[smemPos] = d_Data[rowStart + 2 * apronStartClamped - loadPos];
		else
            data[smemPos] = d_Data[rowStart + 2 * apronEndClamped - loadPos];
    }
    __syncthreads();

    const int writePos = tileStart + threadIdx.x;
    if(writePos <= tileEndClamped)
	{
        const int smemPos = writePos - apronStart;
        float sum = 0;
        for(int k = -d_HKernelRadius; k <= d_HKernelRadius; k++)
            sum += data[smemPos + k] * d_HKernel[d_HKernelRadius - k];
        d_Result[rowStart + writePos] = sum;
    }
}


////////////////////////////////////////////////////////////////////////////////
// ConvColumn_16bit
////////////////////////////////////////////////////////////////////////////////
__global__ void ConvColumn_16bit(unsigned short *d_Result, unsigned short *d_Data,
								 int width, int dataH, int smemStride,
								 int gmemStride)
{
    __shared__ float data[COLUMN_TILE_W * (MAX_KERNELSIZE-1+ COLUMN_TILE_H)];

    const int         tileStart = IMUL(blockIdx.y, COLUMN_TILE_H);
    const int           tileEnd = tileStart + COLUMN_TILE_H - 1;
    const int        apronStart = tileStart - d_VKernelRadius;
    const int          apronEnd = tileEnd   + d_VKernelRadius;
    const int    tileEndClamped = min(tileEnd, dataH - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, dataH - 1);
    const int       columnStart = IMUL(blockIdx.x, COLUMN_TILE_W) + threadIdx.x;

    int smemPos = IMUL(threadIdx.y, COLUMN_TILE_W) + threadIdx.x;
    int gmemPos = IMUL(apronStart + threadIdx.y, width) + columnStart;
    for(int y = apronStart + threadIdx.y; y <= apronEnd; y += blockDim.y)
	{
		if ((y >= apronStartClamped) && (y <= apronEndClamped))
			data[smemPos] = d_Data[gmemPos];
		else if (y < apronStartClamped)
			data[smemPos] = d_Data[gmemPos + IMUL((apronStartClamped-y),
									gmemStride)];
		else
            data[smemPos] = d_Data[gmemPos + IMUL((apronEndClamped-y),
									gmemStride)];
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
    __syncthreads();

    smemPos = IMUL(threadIdx.y + d_VKernelRadius, COLUMN_TILE_W) + threadIdx.x;
    gmemPos = IMUL(tileStart + threadIdx.y , width) + columnStart;
    for(int y = tileStart + threadIdx.y; y <= tileEndClamped; y += blockDim.y)
	{
        float sum = 0;
		for(int k = -d_VKernelRadius; k <= d_VKernelRadius; k++)
            sum += data[smemPos + IMUL(k, COLUMN_TILE_W)] *
				   d_VKernel[d_VKernelRadius - k];
		d_Result[gmemPos] = sum;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
}

////////////////////////////////////////////////////////////////////////////////
// ConvRow_24bit
////////////////////////////////////////////////////////////////////////////////
__global__ void ConvRow_24bit(uchar4 *d_Result, uchar4 *d_Data, int width, 
							  int dataH)
{
     __shared__ float4 data[MAX_KERNELSIZE-1 + ROW_TILE_W];

    const int         tileStart = IMUL(blockIdx.x, ROW_TILE_W);
    const int           tileEnd = tileStart + ROW_TILE_W - 1;
    const int        apronStart = tileStart - d_HKernelRadius;
    const int          apronEnd = tileEnd   + d_HKernelRadius;
    const int    tileEndClamped = min(tileEnd, width - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, width - 1);
    const int          rowStart = IMUL(blockIdx.y, width);
    const int apronStartAligned = tileStart - d_HKernelRadiusAligned;
    const int			loadPos = apronStartAligned + threadIdx.x;

	if(loadPos >= apronStart)
	{
        const int smemPos = loadPos - apronStart;
		if ((loadPos >= apronStartClamped) && (loadPos <= apronEndClamped))
			data[smemPos] = _float4(d_Data[rowStart + loadPos]);
		else if (loadPos < apronStartClamped)
			data[smemPos] = _float4(d_Data[rowStart + 2 * apronStartClamped 
									- loadPos]);
		else
            data[smemPos] = _float4(d_Data[rowStart + 2 * apronEndClamped 
									- loadPos]);
    }
    __syncthreads();

    const int writePos = tileStart + threadIdx.x;
    if (writePos <= tileEndClamped)
	{
        const int smemPos = writePos - apronStart;
		float4 sum = {0, 0, 0, 0};
        for(int k = -d_HKernelRadius; k <= d_HKernelRadius; k++)
            sum += data[smemPos + k] * d_HKernel[d_HKernelRadius - k];
        d_Result[rowStart + writePos] = _uchar4(sum);

    }
}


////////////////////////////////////////////////////////////////////////////////
// ConvColumn_24bit
////////////////////////////////////////////////////////////////////////////////
__global__ void ConvColumn_24bit(uchar4 *d_Result, uchar4 *d_Data, int width,
								 int dataH, int smemStride, int gmemStride)
{
    __shared__ float4 data[COLUMN_TILE_W * (MAX_KERNELSIZE-1 + COLUMN_TILE_H4)];

    const int         tileStart = IMUL(blockIdx.y, COLUMN_TILE_H4);
    const int           tileEnd = tileStart + COLUMN_TILE_H4 - 1;
    const int        apronStart = tileStart - d_VKernelRadius;
    const int          apronEnd = tileEnd   + d_VKernelRadius;
    const int    tileEndClamped = min(tileEnd, dataH - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, dataH - 1);
    const int       columnStart = IMUL(blockIdx.x, COLUMN_TILE_W) + threadIdx.x;


    int smemPos = IMUL(threadIdx.y, COLUMN_TILE_W) + threadIdx.x;
    int gmemPos = IMUL(apronStart + threadIdx.y, width) + columnStart;
    for(int y = apronStart + threadIdx.y; y <= apronEnd; y += blockDim.y)
	{
		if ((y >= apronStartClamped) && (y <= apronEndClamped))
			data[smemPos] = _float4(d_Data[gmemPos]);
		else if (y < apronStartClamped)
			data[smemPos] = _float4(d_Data[gmemPos + IMUL((apronStartClamped-y),
									gmemStride)]);
		else
            data[smemPos] = _float4(d_Data[gmemPos + IMUL((apronEndClamped-y),
									gmemStride)]);
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
    __syncthreads();

    smemPos = IMUL(threadIdx.y + d_VKernelRadius, COLUMN_TILE_W) + threadIdx.x;
    gmemPos = IMUL(tileStart + threadIdx.y , width) + columnStart;
    for(int y = tileStart + threadIdx.y; y <= tileEndClamped; y += blockDim.y)
	{
        float4 sum = nullVal;
		for(int k = -d_VKernelRadius; k <= d_VKernelRadius; k++)
            sum += data[smemPos + IMUL(k, COLUMN_TILE_W)] *
				   d_VKernel[d_VKernelRadius - k];
		d_Result[gmemPos] = _uchar4(sum);
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
}


////////////////////////////////////////////////////////////////////////////////
// ConvRow_24bit
////////////////////////////////////////////////////////////////////////////////
__global__ void ConvRow_48bit(ushort4 *d_Result, ushort4 *d_Data, int width, 
							  int dataH)
{
     __shared__ float4 data[MAX_KERNELSIZE-1 + ROW_TILE_W];

    const int         tileStart = IMUL(blockIdx.x, ROW_TILE_W);
    const int           tileEnd = tileStart + ROW_TILE_W - 1;
    const int        apronStart = tileStart - d_HKernelRadius;
    const int          apronEnd = tileEnd   + d_HKernelRadius;
    const int    tileEndClamped = min(tileEnd, width - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, width - 1);
    const int          rowStart = IMUL(blockIdx.y, width);
    const int apronStartAligned = tileStart - d_HKernelRadiusAligned;
    const int			loadPos = apronStartAligned + threadIdx.x;

	if(loadPos >= apronStart)
	{
        const int smemPos = loadPos - apronStart;
		if ((loadPos >= apronStartClamped) && (loadPos <= apronEndClamped))
			data[smemPos] = _float4(d_Data[rowStart + loadPos]);
		else if (loadPos < apronStartClamped)
			data[smemPos] = _float4(d_Data[rowStart + 2 * apronStartClamped 
									- loadPos]);
		else
            data[smemPos] = _float4(d_Data[rowStart + 2 * apronEndClamped 
									- loadPos]);
    }
    __syncthreads();

    const int writePos = tileStart + threadIdx.x;
    if (writePos <= tileEndClamped)
	{
        const int smemPos = writePos - apronStart;
		float4 sum = {0, 0, 0, 0};
        for(int k = -d_HKernelRadius; k <= d_HKernelRadius; k++)
            sum += data[smemPos + k] * d_HKernel[d_HKernelRadius - k];
        d_Result[rowStart + writePos] = _ushort4(sum);

    }
}


////////////////////////////////////////////////////////////////////////////////
// ConvColumn_48bit
////////////////////////////////////////////////////////////////////////////////
__global__ void ConvColumn_48bit(ushort4 *d_Result, ushort4 *d_Data, int width,
								 int dataH, int smemStride, int gmemStride)
{
    __shared__ float4 data[COLUMN_TILE_W * (MAX_KERNELSIZE-1 + COLUMN_TILE_H4)];

    const int         tileStart = IMUL(blockIdx.y, COLUMN_TILE_H4);
    const int           tileEnd = tileStart + COLUMN_TILE_H4 - 1;
    const int        apronStart = tileStart - d_VKernelRadius;
    const int          apronEnd = tileEnd   + d_VKernelRadius;
    const int    tileEndClamped = min(tileEnd, dataH - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, dataH - 1);
    const int       columnStart = IMUL(blockIdx.x, COLUMN_TILE_W) + threadIdx.x;


    int smemPos = IMUL(threadIdx.y, COLUMN_TILE_W) + threadIdx.x;
    int gmemPos = IMUL(apronStart + threadIdx.y, width) + columnStart;
    for(int y = apronStart + threadIdx.y; y <= apronEnd; y += blockDim.y)
	{
		if ((y >= apronStartClamped) && (y <= apronEndClamped))
			data[smemPos] = _float4(d_Data[gmemPos]);
		else if (y < apronStartClamped)
			data[smemPos] = _float4(d_Data[gmemPos + IMUL((apronStartClamped-y),
									gmemStride)]);
		else
            data[smemPos] = _float4(d_Data[gmemPos + IMUL((apronEndClamped-y),
									gmemStride)]);
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
    __syncthreads();

    smemPos = IMUL(threadIdx.y + d_VKernelRadius, COLUMN_TILE_W) + threadIdx.x;
    gmemPos = IMUL(tileStart + threadIdx.y , width) + columnStart;
    for(int y = tileStart + threadIdx.y; y <= tileEndClamped; y += blockDim.y)
	{
        float4 sum = nullVal;
		for(int k = -d_VKernelRadius; k <= d_VKernelRadius; k++)
            sum += data[smemPos + IMUL(k, COLUMN_TILE_W)] *
				   d_VKernel[d_VKernelRadius - k];
		d_Result[gmemPos] = _ushort4(sum);
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// InitGaussianFilter for ConvolutionSeparable Filter
//	- initialisation of Gaus filter in Constant Memory
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaInitGaussianFilter(int FilterHW, float Sigma)
{
	float *filt;
	filt=(float *)malloc((2*FilterHW+1)*sizeof(float));
	float kernelSum=0.;
	for (int i=0; i < (2*FilterHW+1); i++)
	{
		int j=i-FilterHW;
		float ep= -((float)j*(float)j)/((float)2.0*Sigma*Sigma);
        // NOTE 2.50662827 = sqrt(2 * pi)
		filt[i]=(float)(expf(ep)/((float)2.50662827*Sigma));
		kernelSum +=filt[i];
	}
	for (int i=0; i < (2*FilterHW+1); i++) filt[i] /=kernelSum;
	CUDA_SAFE_CALL(cudaMemcpyToSymbol(d_HKernel, filt, (2*FilterHW+1)*sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol(d_VKernel, filt, (2*FilterHW+1)*sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_HKernelRadius", &FilterHW, sizeof(int)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_VKernelRadius", &FilterHW, sizeof(int)));
	int FilterHWAligned=iAlignUp(FilterHW,16);
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_HKernelRadiusAligned", &FilterHWAligned, 
									  sizeof(int)));
	free (filt);
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaConvolution for ConvolutionSeparable Filter
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaConvolution(void *src, void *dest, void *tmpIma, int width, 
								int height, int bpp, int filterHW)
{
	dim3	blockRows, blockColumns, threadRows, threadColumns;
	int		dataW;

	dataW			= iAlignUp(width, 16);
	blockRows.x		= iDivUp(dataW, ROW_TILE_W);
	blockRows.y		= height;
	blockColumns.x	= iDivUp(width, COLUMN_TILE_W); 
	threadRows.x	= iAlignUp(filterHW, 16) + ROW_TILE_W + filterHW;
	threadRows.y	= 1;
	threadColumns.x = COLUMN_TILE_W;
	threadColumns.y = 8;
	switch (bpp)
	{
		case 8:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H);
				ConvRow_8bit<<<blockRows, threadRows>>>((unsigned char *)tmpIma, 
									(unsigned char *)src, width, height);
				ConvColumn_8bit<<<blockColumns, threadColumns>>>((unsigned char *)dest,
									(unsigned char *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
				break;
		case 16:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H);
				ConvRow_16bit<<<blockRows, threadRows>>>((unsigned short *)tmpIma, 
									(unsigned short *)src, width, height);
				ConvColumn_16bit<<<blockColumns, threadColumns>>>((unsigned short *)dest,
									(unsigned short *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
				break;
		case 24:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H4);
				ConvRow_24bit<<<blockRows, threadRows>>>((uchar4 *)tmpIma, 
									(uchar4 *)src, width, height);
				ConvColumn_24bit<<<blockColumns, threadColumns>>>((uchar4 *)dest,
									(uchar4 *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
				break;
		case 48:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H4);
				ConvRow_48bit<<<blockRows, threadRows>>>((ushort4 *)tmpIma, 
									(ushort4 *)src, width, height);
				ConvColumn_48bit<<<blockColumns, threadColumns>>>((ushort4 *)dest,
									(ushort4 *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// InitFilterCoefficients for ConvolutionSeparable Filter
//	- initialisation of Gaus filter in Constant Memory
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaInitFilterCoefficients(int HFilterHW, float* hCoeff, int VFilterHW, 
										   float* vCoeff)
{
	float kernelSum=0.;
	for (int i=0; i < (2*HFilterHW+1); i++) kernelSum +=hCoeff[i];
	for (int i=0; i < (2*HFilterHW+1); i++) hCoeff[i] /=kernelSum;
	kernelSum=0.;
	for (int i=0; i < (2*VFilterHW+1); i++) kernelSum +=vCoeff[i];
	for (int i=0; i < (2*VFilterHW+1); i++) hCoeff[i] /=kernelSum;	
	CUDA_SAFE_CALL(cudaMemcpyToSymbol(d_HKernel, hCoeff, (2*HFilterHW+1)*sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol(d_VKernel, vCoeff, (2*VFilterHW+1)*sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_HKernelRadius", &HFilterHW, sizeof(int)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_VKernelRadius", &VFilterHW, sizeof(int)));
	int FilterHWAligned=iAlignUp(HFilterHW,16);
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_HKernelRadiusAligned", &FilterHWAligned, 
									  sizeof(int)));
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaRepeatedConvolution for ConvolutionSeparable Filter
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaRepeatedConvolve(int nPasses, void *src, void *dest, void *tmpIma,
									 void *tmpIma2, int width, int height, int bpp, 
									 int HfilterHW)
{
	void	*src0, *dest0;
	dim3	blockRows, blockColumns, threadRows, threadColumns;
	int		dataW;

	dataW			= iAlignUp(width, 16);
	blockRows.x		= iDivUp(dataW, ROW_TILE_W);
	blockRows.y		= height;
	blockColumns.x	= iDivUp(width, COLUMN_TILE_W); 
	threadRows.x	= iAlignUp(HfilterHW, 16) + ROW_TILE_W + HfilterHW;
	threadRows.y	= 1;
	threadColumns.x = COLUMN_TILE_W;
	threadColumns.y = 8;
	switch (bpp)
	{
		case 8:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H);
				src0=src;
				for (int i=0; i<nPasses; i++)
				{
					if (i==nPasses-1)dest0=dest;
					else dest0=tmpIma2;
					ConvRow_8bit<<<blockRows, threadRows>>>((unsigned char *)tmpIma, 
									(unsigned char *)src0, width, height);
					ConvColumn_8bit<<<blockColumns, threadColumns>>>((unsigned char *)dest0,
									(unsigned char *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
					src0=tmpIma2;
				}
				break;
		case 16:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H);
				src0=src;
				for (int i=0; i<nPasses; i++)
				{
					if (i==nPasses-1)dest0=dest;
					else dest0=tmpIma2;
					ConvRow_16bit<<<blockRows, threadRows>>>((unsigned short *)tmpIma, 
									(unsigned short *)src0, width, height);
					ConvColumn_16bit<<<blockColumns, threadColumns>>>((unsigned short *)dest0,
									(unsigned short *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
					src0=tmpIma2;
				}
				break;
		case 24:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H4);
				src0=src;
				for (int i=0; i<nPasses; i++)
				{
					if (i==nPasses-1)dest0=dest;
					else dest0=tmpIma2;
					ConvRow_24bit<<<blockRows, threadRows>>>((uchar4 *)tmpIma, 
									(uchar4 *)src0, width, height);
					ConvColumn_24bit<<<blockColumns, threadColumns>>>((uchar4 *)dest0,
									(uchar4 *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
					src0=tmpIma2;
				}
				break;
		case 48:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H4);
				src0=src;
				for (int i=0; i<nPasses; i++)
				{
					if (i==nPasses-1)dest0=dest;
					else dest0=tmpIma2;
					ConvRow_48bit<<<blockRows, threadRows>>>((ushort4 *)tmpIma, 
									(ushort4 *)src0, width, height);
					ConvColumn_48bit<<<blockColumns, threadColumns>>>((ushort4 *)dest0,
									(ushort4 *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
					src0=tmpIma2;
				}
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// UnsharpMaskDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  UnsharpMaskDevice_8bit(unsigned char *dest, unsigned char *src1,
										unsigned char *src2, int width, int height)
{
	float pix;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		pix = ((float)src1[y*width+x]-d_Amount*(float)src2[y*width+x])/(1.-d_Amount);
		dest[y*width+x]=(unsigned char)pix;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// UnsharpMaskDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  UnsharpMaskDevice_16bit(unsigned short *dest, unsigned short *src1,
										 unsigned short *src2, int width, int height)
{
	float pix;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		pix = ((float)src1[y*width+x]-d_Amount*(float)src2[y*width+x])/(1.-d_Amount);
		dest[y*width+x]=(unsigned short)pix;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// UnsharpMaskDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  UnsharpMaskDevice_24bit(uchar4 *dest, uchar4 *src1, uchar4 *src2, 
										 int width, int height)
{
	float4 pix;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		pix = (_float4(src1[y*width+x])-d_Amount*_float4(src2[y*width+x]))/(1.-d_Amount);
		dest[y*width+x]=_uchar4(pix);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// UnsharpMaskDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  UnsharpMaskDevice_48bit(ushort4 *dest, ushort4 *src1, ushort4 *src2, 
										 int width, int height)
{
	float4 pix;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		pix = (_float4(src1[y*width+x])-d_Amount*_float4(src2[y*width+x]))/(1.-d_Amount);
		dest[y*width+x]=_ushort4(pix);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaInitAmount
//	- initialisation of Amount used in Unsharp Mask and Nearest Neighbour deconvolution
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaInitAmount(float amount)
{
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_Amount", &amount,  sizeof(float)));
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaUnsharpMask
//	- performs unsharp mask function after Gaussian Filter
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaUnsharpMask(void *src1, void *src2, void *dest, int width, 
								int height, int bpp)
{
	dim3 dimBlock(8, 8, 1);
	dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);
	switch (bpp)
	{
		case 8:
				UnsharpMaskDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, 
					(unsigned char *)src1, (unsigned char *)src2, width, height);
				break;
		case 16:
				UnsharpMaskDevice_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest, 
					(unsigned short *)src1, (unsigned short *)src2, width, height);
				break;
		case 24:
				UnsharpMaskDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, 
					(uchar4 *)src1, (uchar4 *)src2, width, height);
				break;
		case 48:
				UnsharpMaskDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, 
					(ushort4 *)src1, (ushort4 *)src2, width, height);
				break;
	}
}



///////////////////////////////////////////////////////////////////////////////////////
// NNDeconvDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  NNDeconvDevice_8bit(unsigned char *dest, unsigned char *src1,
									 unsigned char *src2, unsigned char *src3, 
									 int width, int height)
{
	float pixOrg, pixN;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		pixOrg = (float)src1[y*width+x];
		pixN = ((float)src2[y*width+x]+(float)src2[y*width+x])/2.;
		dest[y*width+x]=(unsigned char)(pixOrg-d_Amount*pixN);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// NNDeconvDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  NNDeconvDevice_16bit(unsigned short *dest, unsigned short *src1,
									  unsigned short *src2, unsigned short *src3,
									  int width, int height)
{
	float pixOrg, pixN;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		pixOrg = (float)src1[y*width+x];
		pixN = ((float)src2[y*width+x]+(float)src2[y*width+x])/2.;
		dest[y*width+x]=(unsigned short)(pixOrg-d_Amount*pixN);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// NNDeconvDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  NNDeconvDevice_24bit(uchar4 *dest, uchar4 *src1, uchar4 *src2,
									  uchar4 *src3, int width, int height)
{
	float4 pixOrg, pixN;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		pixOrg = _float4(src1[y*width+x]);
		pixN = (_float4(src2[y*width+x]) + _float4(src2[y*width+x]))/2.;
		dest[y*width+x]=_uchar4((pixOrg-d_Amount*pixN));
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// NNDeconvDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  NNDeconvDevice_48bit(ushort4 *dest, ushort4 *src1, ushort4 *src2, 
									  ushort4 *src3, int width, int height)
{
	float4 pixOrg, pixN;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		pixOrg = _float4(src1[y*width+x]);
		pixN = (_float4(src2[y*width+x]) + _float4(src2[y*width+x]))/2.;
		dest[y*width+x]=_ushort4((pixOrg-d_Amount*pixN));
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaNearestNeigbourDeconv
//	- performs unsharp mask function based on two neighbouring images
//	  used for nearest neighbour deconvolution
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaNearestNeigbourDeconv(void *src1, void *src2, void *src3, 
										  void *dest, int width, int height, int bpp)
{
	dim3 dimBlock(8, 8, 1);
	dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);
	switch (bpp)
	{
		case 8:
				NNDeconvDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, 
					(unsigned char *)src1, (unsigned char *)src2, (unsigned char *)src3, 
					width, height);
				break;
		case 16:
				NNDeconvDevice_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest, 
					(unsigned short *)src1, (unsigned short *)src2, (unsigned short *)src3,
					width, height);
				break;
		case 24:
				NNDeconvDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, 
					(uchar4 *)src1, (uchar4 *)src2, (uchar4 *)src3, width, height);
				break;
		case 48:
				NNDeconvDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, 
					(ushort4 *)src1, (ushort4 *)src2, (ushort4 *)src3, width, height);
				break;
	}
}
