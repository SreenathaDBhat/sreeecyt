///////////////////////////////////////////////////////////////////////////////////////
// CudaTIP_Kernel.h
//	- header file for CUDA kernels
///////////////////////////////////////////////////////////////////////////////////////
#define OFFSET(i)		((char *)NULL + (i))
//#define SOBEL_SHARED	true
//#define MINMAX_SHARED	true
#define SOBEL_RADIUS	1
#define MAX_KERNELSIZE	41
#define MAX_FILTERSIZE	1450
#define ROW_TILE_W		256
#define COLUMN_TILE_W	4
#define COLUMN_TILE_H	96
#define COLUMN_TILE_H4	32

extern "C" void CudaSobel (cudaArray *src, void *dest, int width, int height, int bpp, 
								float fScale);
extern "C" void CudaInitGaussianFilter (int FilterHW, float Sigma);
extern "C" void CudaConvolution (void *src, void *dest, void *tmpIma, int width, 
								int height, int bpp, int filterHW);
extern "C" void CudaInitFilterCoefficients(int HFilterHW, float* hCoeff, int VFilterHW, 
										   float* vCoeff);
extern "C" void CudaRepeatedConvolve(int nPasses, void *src, void *dest, void *tmpIma, 
									  void *tmpIma2, int width, int height, int bpp, 
									  int HfilterHW);

#ifdef MINMAX_SHARED
extern "C" void CudaErode (void *src, void *dest, void *tmpIma, int width, int height, 
								int bpp, int filterHW);
extern "C" void CudaDilate (void *src, void *dest, void *tmpIma, int width, int height, 
								int bpp, int filterHW);
#else
extern "C" void CudaErode (cudaArray *src, void *dest, cudaArray *tmpIma, int width, 
								int height, int bpp, int filterHW);
extern "C" void CudaDilate (cudaArray *src, void *dest, cudaArray *tmpIma, int width, 
								int height, int bpp, int filterHW);
#endif

extern "C" void CudaSubtract(cudaArray *src1, cudaArray *src2, void *dest, int width, 
								int height, int bpp);
extern "C" void CudaDiff(cudaArray *src1, cudaArray *src2, void *dest, int width, 
								int height, int bpp);
extern "C" void CudaAdd(cudaArray *src1, cudaArray *src2, void *dest, int width, 
								int height, int bpp);
extern "C" void CudaMultiply(cudaArray *src1, cudaArray *src2, void *dest, int width, 
								int height, int bpp);
extern "C" void CudaDivide(cudaArray *src1, cudaArray *src2, void *dest, int width, 
						   int height, int bpp);
extern "C" void CudaMod(cudaArray *src1, cudaArray *src2, void *dest, int width, 
						int height, int bpp);
extern "C" void CudaSubtractConst(cudaArray *src,  void *dest, float val, int width, 
								 int height, int bpp);
extern "C" void CudaAddConst(cudaArray *src,  void *dest, float val, int width, 
								int height, int bpp);
extern "C" void CudaMultiplyConst(cudaArray *src,  void *dest, float val, int width,
								 int height, int bpp);
extern "C" void CudaDivideConst(cudaArray *src,  void *dest, float val, int width, 
								 int height, int bpp);
extern "C" void CudaThresh(cudaArray *src,  void *dest, float thresh, int width, 
						   int height, int bpp);
extern "C" void CudaComponentSplit(cudaArray *src, void *dest1, void *dest2, 
								   void *dest3, int width, int height, int bppSrc,
								   int bppDest);
extern "C" void CudaComponentJoin(cudaArray *src1, cudaArray *src2, cudaArray *src3,
								   void *dest, int width, int height, int bppSrc);
extern "C" void CudaInitThresholdConst(float r0, float r1, float g0, float g1, float b0,
									   float b1); 
extern "C" void CudaInitComponentConst(float r0, float r1, float g0, float g1, float b0,
									   float b1); 
extern "C" void CudaComponentThreshold (cudaArray *src, void *dest, int width, 
										int height, int bpp);
extern "C" void CudaInitBlendConst(float a1, float a2, float a3); 
extern "C" void CudaComponentBlend(cudaArray *src1, cudaArray *src2, cudaArray *src3,
								   void *dest, int width, int height, int bppSrc);
extern "C" void CudaComposedBlend (cudaArray *src, void *dest, int width, int height, 
								   int bpp);
extern "C" void CudaRGBHSI (cudaArray *src, void *dest, int width,  int height, 
							int bpp);
extern "C" void CudaHSIRGB (cudaArray *src, void *dest, int width,  int height, 
							int bpp);
extern "C" void CudaColourDeconv (cudaArray *src, void *dest, int width, int height, 
								   int bpp, float4 *qVec);
extern "C" void CudaMedian(cudaArray *src, void *dest, cudaArray *tmpIma, int width, 
						   int height, int bpp, int nPasses);
extern "C" void CudaLaplace(cudaArray *src, void *dest, int width, int height, int bpp);
extern "C" void CudaMean(cudaArray *src, void *dest, int width, int height, int bpp);
extern "C" void CudaInitBlendConstants(int nrImages, int *modalCorr, int bpp);
extern "C" void CudaSimpleBlend(int nrImages, cudaArray **src, void *dest, int width, 
								int height, int bpp);
extern "C" void CudaInitHDRBlendConstant(float4 fMinHdr);
extern "C" void CudaHDRBlend(cudaArray *src, void *dest, int width, int height, 
							 int bpp);
extern "C" void CudaRangeBlend(int nrImages, cudaArray **src, cudaArray **range,
							   void *dest, int width, int height, int bpp);
extern "C" void CudaInitFilterRadius(int filterHW);
extern "C" void CudaKuwaharaPass1(cudaArray *src, void *destMean, void *destVaira,
								  int width, int height, int bpp);
extern "C" void CudaKuwaharaPass2(cudaArray *mean, cudaArray *varia, void *dest, 
								  int width, int height, int bpp);
extern "C" void CudaConvert(cudaArray *src, void *dest, int width, int height, 
								 int bpp, int bppdest, float normFactor);
extern "C" void CudaInitScaleAndOffset(float scaleX, float scaleY, float offsetX,
									   float offsetY);
extern "C" void CudaCropAndResize(cudaArray *src,  void *dest, int width, int height, 
								  int bpp, int bppDest);
extern "C" void CudaInitCorrOffset(float corrTexWidth, float corrTexHeight);
extern "C" void CudaUnpackGrey(cudaArray *src,  void *dest, int width, int height, 
							   int bpp, int bppDest);
extern "C" void CudaPackGrey(cudaArray *src,  void *dest, int width, int height, 
							 int bpp);
extern "C" void CudaPowerSpectrum(cudaArray *src,  void *dest, int width, int height, 
								  int bpp);
extern "C" void CudaFFTFilter(void *src, void *dest, int width, int height, 
							  float *filtr, int maxRad);
extern "C" void CudaFFTMultiply(void *src1, void *src2, void *dest, int width,
								int height);
extern "C" void CudaInitAmount(float amount);
extern "C" void CudaUnsharpMask(void *src1, void *src2, void *dest, int width, 
								int height, int bpp);
extern "C" void CudaNearestNeigbourDeconv(void *src1, void *src2, void *src3, 
										  void *dest, int width, int height, int bpp);
extern "C" void CudaCannyEdge(void *src, void *tmp1, void *tmp2, void *dest, int width, 
							  int height, int bpp, float lowThresh, float highThresh);
extern "C" void CudaPrepareEDM(cudaArray *src, void *dest,int width, int height,
							   int bpp, int invert);
extern "C" void CudaEDMTransform(void *src, void *scratch, void **result, int width, 
								 int height);
extern "C" void CudaFinishEDM(void *src, void *dest,int width, int height,
							  int bpp, int EDMOnly, float normFactor);
