
#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cutil.h>
#include "cudamath.h"
#include "CudaTIP_Kernels.h"

__device__ __constant__ int   d_Invert;
__device__ __constant__ int   d_EDMOnly;
__device__ __constant__ float d_MaxFloat = 9999999999.0;
__device__ __constant__ float d_SquaredRatioW;
__device__ __constant__ float d_SquaredRatioH;
__device__ __constant__ int	  d_OffX;
__device__ __constant__ int	  d_OffY;

__global__ void  PrepareEDMDevice_8bit(float4 *pTarg, int width, int height)
{ 
	float4 outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp.z=tex2D(texUC1, x, y);
		if (d_Invert)
		{
			// only distance map to the background
			if (outp.z==0)
			{
				outp.z=1.;
				outp.x=(float)x/(float)width;
				outp.y=(float)y/(float)height;
				outp.w=0.;
			}
			else
			{
				outp.z=0.;
				outp.x=-1.;
				outp.y=-1.;
				outp.w=d_MaxFloat;
			}
		}
		else
		{
			// distance to labelled objects  (Voronoi diagrams)
			if (outp.z!=0)
			{
				outp.x=(float)x/(float)width;
				outp.y=(float)y/(float)height;
				outp.w=0.;
			}
			else
			{
				outp.x=-1.;
				outp.y=-1.;
				outp.w=d_MaxFloat;
			}
		}
		pTarg[y*width+x]=outp;
	}
}

__global__ void  PrepareEDMDevice_16bit(float4 *pTarg, int width, int height)
{ 
	float4 outp;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		outp.z=tex2D(texUS1, x, y);
		if (d_Invert)
		{
			// only distance map within objects
			if (outp.z==0)
			{
				outp.z=1.;
				outp.x=(float)x/(float)width;
				outp.y=(float)y/(float)height;
				outp.w=0.;
			}
			else
			{
				outp.z=0;
				outp.x=-1.;
				outp.y=-1.;
				outp.w=d_MaxFloat;
			}
		}
		else
		{
			// distance to labelled objects (Voronoi diagrams)
			if (outp.z!=0)
			{
				outp.x=(float)x/(float)width;
				outp.y=(float)y/(float)height;
				outp.w=0.;
			}
			else
			{
				outp.x=-1.;
				outp.y=-1.;
				outp.w=d_MaxFloat;
			}
		}
		pTarg[y*width+x]=outp;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaPrepareEDM
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaPrepareEDM(cudaArray *src, void *target, int width, int height,
							   int bpp, int invert)
{
	dim3 dimBlock(8, 8, 1);
	dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_Invert", &invert, sizeof(int)));
	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src));
				PrepareEDMDevice_8bit<<< dimGrid, dimBlock>>>((float4 *)target, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src));
				PrepareEDMDevice_16bit<<< dimGrid, dimBlock>>>((float4 *)target, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
	}
}

__device__ float4 CheckDist(float4 pix, float4 pixRef, float x, float y)
{
	float	dist;
	float4	newPix;

	newPix=pix;
	if ((pixRef.z != 0) || (pixRef.x >= 0))
	{
		dist=(pixRef.x-x)*(pixRef.x-x)*d_SquaredRatioW +
			 (pixRef.y-y)*(pixRef.y-y)*d_SquaredRatioH; 
		if (dist < pix.w)
		{
			newPix.w=dist;
			newPix.x=pixRef.x;
			newPix.y=pixRef.y;
		}
	}
	return newPix;
}

__global__ void  EDMDevice(float4 *pTarg, float4 *pSrc, int width, int height)
{
	float4	pix, pixRef;
	float	u, v;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x/(float)width;
		v=(float)y/(float)height;
		pix=pSrc[y*width+x];
		if (pix.w != 0)
		{
			if ((y >= d_OffY) && (x >= d_OffX))
			{
				pixRef = pSrc[(y - d_OffY) * width + x - d_OffX];	//x-offX,y-offY
				pix = CheckDist(pix, pixRef, u, v);
			}
			if (y >= d_OffY)
			{
				pixRef = pSrc[(y - d_OffY) * width + x];			//x,	 y-offY
				pix = CheckDist(pix, pixRef, u, v);
			}
			if ((y >= d_OffY) && (x < (width - d_OffX)))
			{
				pixRef = pSrc[(y - d_OffY) * width + x + d_OffX];	//x+offX,y-offY
				pix = CheckDist(pix, pixRef, u, v);
			}
			if (x >= d_OffX)
			{
				pixRef = pSrc[y * width + x - d_OffX];				//x-offX,y
				pix = CheckDist(pix, pixRef, u, v);
			}
			if (x < (width - d_OffX))
			{
				pixRef = pSrc[y * width + x + d_OffX];				//x+offX,y
				pix = CheckDist(pix, pixRef, u, v);
			}
			if ((y < (height-d_OffY)) && (x >= d_OffX))
			{
				pixRef = pSrc[(y + d_OffY) * width + x - d_OffX];	//x-offX,y+offY
				pix = CheckDist(pix, pixRef, u, v);
			}
			if (y < (height-d_OffY))
			{
				pixRef = pSrc[(y + d_OffY) * width + x];			//x,	 y+offY
				pix = CheckDist(pix, pixRef, u, v);
			}
			if ((y < (height-d_OffY)) && (x < (width - d_OffX)))
			{
				pixRef = pSrc[(y + d_OffY) * width + x + d_OffX];	//x+offX,y+offY
				pix = CheckDist(pix, pixRef, u, v);
			}
		}
		pTarg[y*width+x]=pix;
	}

}

///////////////////////////////////////////////////////////////////////////////////////
// CudaEDMTransform
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaEDMTransform(void *scratch1, void *scratch2, void **result, int width, 
								 int height)
{
	float	ratioW, ratioH, squaredRatioW, squaredRatioH;
	int		swap, woff, hoff, extra;

	dim3 dimBlock(16, 16, 1);
	dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);
	ratioW=(float)width/(float)min(width,height);
	ratioH=(float)height/(float)min(width,height);
	squaredRatioW=ratioW*ratioW;
	squaredRatioH=ratioH*ratioH;
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_SquaredRatioW", &squaredRatioW, sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_SquaredRatioH", &squaredRatioH, sizeof(float)));
	swap=0;
	extra=0;
	woff=width;
	hoff=height;
	do
	{
		woff /=2;
		if (woff < 1) woff=1;
		hoff /=2;
		if (hoff < 1) hoff=1;
		if ((woff==1) && (hoff==1)) extra +=1;
		CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_OffX", &woff, sizeof(int)));
		CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_OffY", &hoff, sizeof(int)));
		if (swap==0)
		{
			EDMDevice<<< dimGrid, dimBlock>>>((float4 *)scratch2, (float4 *)scratch1, 
				width, height);
			*result=scratch2;
		}
		else
		{
			EDMDevice<<< dimGrid, dimBlock>>>((float4 *)scratch1, (float4*)scratch2,
				width, height);
			*result=scratch1;
		}
		swap=1-swap;
	}while (extra <= 1);
}

__global__ void  FinishEDMDevice_24bit(uchar4 *pTarg, float4 *pSrc, int width, 
									   int height)
{
	float4	inp, outp;
	float	dist;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		inp=pSrc[y*width+x];
		dist=d_NormFactor*sqrt(inp.w)/sqrt(d_SquaredRatioW+d_SquaredRatioH);
		if (d_EDMOnly)
		{
			outp.x=dist;
			outp.y=dist;
			outp.z=dist;
			outp.w=0;
		}
		else
		{
			if (inp.x > 0)
				outp.x=pSrc[int(inp.y*(float)height)*width+(int)(inp.x*(float)width)].z;
			else
				outp.x=0;
			outp.y=dist;
			outp.z=0;
			outp.w=0;
		}
		outp=clamp(outp, 0.0, 1.0);
		pTarg[y*width+x]=_uchar4(outp*255.);
	}
}

__global__ void  FinishEDMDevice_48bit(ushort4 *pTarg, float4 *pSrc, int width, 
									   int height)
{
	float4	inp, outp;
	float	dist;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		inp=pSrc[y*width+x];
		dist=d_NormFactor*sqrt(inp.w)/sqrt(d_SquaredRatioW+d_SquaredRatioH);
		if (d_EDMOnly)
		{
			outp.x=dist;
			outp.y=dist;
			outp.z=dist;
			outp.w=0;
		}
		else
		{
			if (inp.x > 0)
				outp.x=pSrc[int(inp.y*(float)height)*width+(int)(inp.x*(float)width)].x;
			else
				outp.x=0;
			outp.y=dist;
			outp.z=0;
			outp.w=0;
		}
		outp=clamp(outp, 0.0, 1.0);
		pTarg[y*width+x]=_ushort4(outp*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaFinishEDM
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaFinishEDM(void *src, void *dest, int width, int height,
							  int bpp, int EDMOnly, float normFactor)
{
	float bppFac;

	dim3 dimBlock(8, 8, 1);
	dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_EDMOnly", &EDMOnly, sizeof(int)));
	if (bpp==24)
		bppFac=255.;
	else
		bppFac=65535.;
	if (normFactor <=0.)
		normFactor=(float)sqrt((float)width*(float)width+(float)height*(float)height)/
					bppFac;
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_NormFactor", &normFactor, sizeof(int)));
	switch (bpp)
	{
		case 24:
				FinishEDMDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, 
					(float4 *)src, width, height);
				break;
		case 48:
				FinishEDMDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest,
					(float4 *)src, width, height);
				break;
	}
}

