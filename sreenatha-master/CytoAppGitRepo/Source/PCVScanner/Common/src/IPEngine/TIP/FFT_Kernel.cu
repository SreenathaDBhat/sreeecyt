
#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cutil.h>
#include "cudamath.h"
#include "CudaTIP_Kernels.h"

texture<float,	 cudaTextureType1D, cudaReadModeElementType> texFilt;
texture<float2,  cudaTextureType2D, cudaReadModeElementType> texNComplex;
texture<float2,  cudaTextureType2D, cudaReadModeElementType> texComplex1;
texture<float2,  cudaTextureType2D, cudaReadModeElementType> texComplex2;

__device__ __constant__ int d_MaxRadius;

///////////////////////////////////////////////////////////////////////////////////////
// SpectrumDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  SpectrumDevice_8bit(unsigned char *pTarg, int width,  int height)
{ 
	float2 pix;
	float u, v, mag;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x/(float)width;
		v=(float)y/(float)height;
		pix=tex2D(texNComplex, u-0.5, v-0.5);
		mag = (pix.x*pix.x + pix.y*pix.y);
		mag=0.05*log2(mag+1.0);
		mag=clamp(mag, 0.0, 1.0);
		pTarg[y*width+x]=(unsigned char)(mag*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// SpectrumDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  SpectrumDevice_16bit(unsigned short *pTarg, int width, int height)
{ 
	float2 pix;
	float mag, u, v;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x/(float)width;
		v=(float)y/(float)height;
		pix=tex2D(texNComplex, u-0.5, v-0.5);
		mag = (pix.x*pix.x + pix.y*pix.y);
		mag=0.05*log2(mag+1.0);
		mag=clamp(mag, 0.0, 1.0);
		pTarg[y*width+x]=(unsigned short)(mag*65535.);
	}
}


///////////////////////////////////////////////////////////////////////////////////////
// CudaPowerSpectrum
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaPowerSpectrum(cudaArray *src,  void *dest, int width, int height, 
								  int bpp)
{
	float normFactor;
	dim3 dimBlock(8, 8, 1);
	normFactor=1./(float)width*(float)height;
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_NormFactor", &normFactor, sizeof(float)));

	switch (bpp)
	{
		case 8:
				texNComplex.addressMode[0] = cudaAddressModeWrap;
				texNComplex.addressMode[1] = cudaAddressModeWrap;
				texNComplex.filterMode = cudaFilterModeLinear;
				texNComplex.normalized = true;    // access with normalized texture coordinates
				CUDA_SAFE_CALL(cudaBindTextureToArray(texNComplex, src));
				SpectrumDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, 
						/*(float2 *)src, */width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texNComplex));
				break;
		case 16:
				texNComplex.addressMode[0] = cudaAddressModeWrap;
				texNComplex.addressMode[1] = cudaAddressModeWrap;
				texNComplex.filterMode = cudaFilterModeLinear;
				texNComplex.normalized = true;    // access with normalized texture coordinates
				CUDA_SAFE_CALL(cudaBindTextureToArray(texNComplex, src));
				SpectrumDevice_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest, 
						width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texNComplex));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// FilterDevice_64bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  FilterDevice_64bit(float2 *pTarg, float2 *pOrg, float* d_Filter, 
									int width, int height)
{ 
	float2 pix;
	float u, v;
	int	  dist;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		pix=pOrg[y*width+x];
		u=(float)x/(float)width;
		v=(float)y/(float)height;
		if (u < 0.5) u=2.*u;
		 else  u=(1.-u)*2.;
		if (v < 0.5) v=2.*v;
		 else  v=(1.-v)*2.;
		dist=(int)((float)d_MaxRadius*sqrt(u*u+v*v)/sqrt(2.));
		pTarg[y*width+x]=pix*d_Filter[dist];
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaFFTFilter
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaFFTFilter(void *src, void *dest, int width, int height, 
							  float *filtr, int maxRad)
{
	float *d_Filter;
	dim3 dimBlock(8, 8, 1);
	dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);
   
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_MaxRadius", &maxRad, sizeof(int)));
	CUDA_SAFE_CALL(cudaMalloc((void**) &d_Filter, maxRad*sizeof(float)));
    CUDA_SAFE_CALL(cudaMemcpy(d_Filter, filtr, maxRad*sizeof(float), cudaMemcpyHostToDevice) );
	FilterDevice_64bit<<< dimGrid, dimBlock>>>((float2 *)dest, (float2 *)src, (
		float *)d_Filter, width, height);
    CUDA_SAFE_CALL(cudaFree(d_Filter));
}

///////////////////////////////////////////////////////////////////////////////////////
// FFTMultiplyDevice_64bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  FFTMultiplyDevice_64bit(float2 *pTarg, float2 *pOrg1, float2 *pOrg2, 
										 int width, int height)
{ 
    float2	pix1, pix2, outp;

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		pix1 = pOrg1[y*width+x];
		pix2 = pOrg2[y*width+x];
		outp.x=(pix1.x*pix2.x + pix1.y*pix2.y);
		outp.y=(pix2.y*pix1.x - pix1.y*pix2.x);
		pTarg[y*width+x]=outp;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaFFTMultiply
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaFFTMultiply(void *src1, void *src2, void *dest, int width, 
								int height)
{
	dim3 dimBlock(8, 8, 1);
	dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);
	FFTMultiplyDevice_64bit<<< dimGrid, dimBlock>>>((float2 *)dest, (float2 *)src1,
		(float2 *)src2, width, height);
}
