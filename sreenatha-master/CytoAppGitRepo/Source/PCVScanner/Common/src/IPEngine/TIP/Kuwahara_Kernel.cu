#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cutil.h>
#include "cudamath.h"
#include "CudaTIP_Kernels.h"

texture<float,	cudaTextureType2D, cudaReadModeElementType> texF2;
texture<float4, cudaTextureType2D, cudaReadModeElementType> tex4F1;
texture<float4, cudaTextureType2D, cudaReadModeElementType> tex4F2;


__device__ __constant__ int d_FilterHW;
__device__ __constant__ int d_FilterSize;

///////////////////////////////////////////////////////////////////////////////////////
// KuwaharaPass1_8bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void KuwaharaPass1_8bit(float *pDestMean, float *pDestVaria, 
								   unsigned int Pitch, int width, int height)
{ 
	int		i, k, l, m;
	float	pix[25], total, variance;

	float *pDestM = pDestMean+blockIdx.x*Pitch;
	float *pDestV = pDestVaria+blockIdx.x*Pitch;
    for (i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		m=0;
		total=0.;
		variance=0.;
		for (k=-d_FilterHW; k <= d_FilterHW; k++)
		{
			for (l = -d_FilterHW; l <= d_FilterHW; l++)
			{
				pix[m] = tex2D( texUC1, (float)(i+k), (float)(blockIdx.x + l));
				total +=pix[m];
				m+=1;
			}
		}
		total /=d_FilterSize;
		for (k=0; k<d_FilterSize; k++)
			variance +=(pix[k]-total)*(pix[k]-total);
		pDestM[i]=total;
		pDestV[i]=variance;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// KuwaharaPass1_24bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void KuwaharaPass1_16bit(float *pDestMean, float *pDestVaria, 
								    unsigned int Pitch, int width, int height)
{ 
	int		i, k, l, m;
	float	pix[25], total, variance;

	float *pDestM = pDestMean+blockIdx.x*Pitch;
	float *pDestV = pDestVaria+blockIdx.x*Pitch;
    for (i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		m=0;
		total=0.;
		variance=0.;
		for (k=-d_FilterHW; k <= d_FilterHW; k++)
		{
			for (l = -d_FilterHW; l <= d_FilterHW; l++)
			{
				pix[m] = tex2D( texUS1, (float)(i+k), (float)(blockIdx.x +l));
				total +=pix[m];
				m+=1;
			}
		}
		total /=d_FilterSize;
		for (k=0; k<d_FilterSize; k++)
			variance=(pix[k]-total)*(pix[k]-total);
		pDestM[i]=total;
		pDestV[i]=variance;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// KuwaharaPass1_24bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void KuwaharaPass1_24bit(float4 *pDestMean, float4 *pDestVaria, 
								    unsigned int Pitch, int width, int height)
{ 
	int		i, k, l, m;
	float4	pix[25], total, variance;

	float4 *pDestM = pDestMean+blockIdx.x*Pitch;
	float4 *pDestV = pDestVaria+blockIdx.x*Pitch;
    for (i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		m=0;
		total=d_NullfVal;
		variance=d_NullfVal;
		for (k=-d_FilterHW; k <= d_FilterHW; k++)
		{
			for (l = -d_FilterHW; l <= d_FilterHW; l++)
			{
				pix[m] = tex2D( tex4UC1, (float)(i + k), (float)(blockIdx.x +l));
				total +=pix[m];
				m+=1;
			}
		}
		total /=d_FilterSize;
		for (k=0; k<d_FilterSize; k++)
			variance=(pix[k]-total)*(pix[k]-total);
		pDestM[i]=total;
		pDestV[i]=variance;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// KuwaharaPass1_48bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void KuwaharaPass1_48bit(float4 *pDestMean, float4 *pDestVaria, 
								    unsigned int Pitch, int width, int height)
{ 
	int		i, k, l, m;
	float4	pix[25], total, variance;

	float4 *pDestM = pDestMean+blockIdx.x*Pitch;
	float4 *pDestV = pDestVaria+blockIdx.x*Pitch;
    for (i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		m=0;
		total=d_NullfVal;
		variance=d_NullfVal;
		for (k=-d_FilterHW; k <= d_FilterHW; k++)
		{
			for (l = -d_FilterHW; l <= d_FilterHW; l++)
			{
				pix[m] = tex2D( tex4US1, (float)(i+k), (float)(blockIdx.x +l));
				total +=pix[m];
				m+=1;
			}
		}
		total /=d_FilterSize;
		for (k=0; k<d_FilterSize; k++)
			variance=(pix[k]-total)*(pix[k]-total);
		pDestM[i]=total;
		pDestV[i]=variance;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaInitHDRBlendConstant
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaInitFilterRadius(int filterHW)
{
	int	filterSize;
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_FilterHW", &filterHW, sizeof(int)));
	filterSize=(2*filterHW+1)*(2*filterHW+1);
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_FilterSize", &filterSize, sizeof(int)));
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaKuwaharaPass1
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaKuwaharaPass1(cudaArray *src, void *destMean, void *destVaria,
								  int width, int height, int bpp)
{
	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src));
				KuwaharaPass1_8bit<<<height, 96>>>((float *)destMean, 
					(float *)destVaria, width, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src));
				KuwaharaPass1_16bit<<<height, 96>>>((float *)destMean, 
					(float *)destVaria, width, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:	// packed 8 bit images
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src));
				KuwaharaPass1_24bit<<<height, 32>>>((float4 *)destMean, 
					(float4 *)destVaria, width, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:	// packed 16 bit images
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src));
				KuwaharaPass1_48bit<<<height, 32>>>((float4 *)destMean, 
					(float4 *)destVaria, width, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// KuwaharaPass2_8bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void KuwaharaPass2_8bit(unsigned char *pDestOrig, unsigned int Pitch, 
								   int width, int height)
{ 
	int		i, idx1, idx2;
	float	varia[4], mean1, mean2;

	unsigned char *pDest = pDestOrig+blockIdx.x*Pitch;
    for (i = threadIdx.x; i < width; i += blockDim.x ) 
	{
        varia[0] = tex2D( texF2, (float)(i-d_FilterHW), (float)(blockIdx.x-d_FilterHW));
        varia[1] = tex2D( texF2, (float)(i+d_FilterHW), (float)(blockIdx.x-d_FilterHW));
        varia[2] = tex2D( texF2, (float)(i-d_FilterHW), (float)(blockIdx.x+d_FilterHW));
        varia[3] = tex2D( texF2, (float)(i+d_FilterHW), (float)(blockIdx.x+d_FilterHW));
		if (varia[0] < varia[1])
		{
			idx1=0;
			mean1=tex2D( texF1, (float)(i-d_FilterHW), (float)(blockIdx.x-d_FilterHW));
		}
		else
		{
			idx1=1;
			mean1=tex2D( texF1, (float)(i+d_FilterHW), (float)(blockIdx.x-d_FilterHW));
		}
		if (varia[2] < varia[3])
		{
			idx2=2;
			mean2=tex2D( texF1, (float)(i-d_FilterHW), (float)(blockIdx.x+d_FilterHW));
		}
		else 
		{
			idx2=3;
			mean2=tex2D( texF1, (float)(i+d_FilterHW), (float)(blockIdx.x+d_FilterHW));
		}
		if (varia[idx1] < varia[idx2])
			pDest[i]=(unsigned char)(mean1*255.);
		else
			pDest[i]=(unsigned char)(mean2*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// KuwaharaPass1_24bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void KuwaharaPass2_16bit(unsigned short *pDestOrig, unsigned int Pitch,
									int width, int height)
{ 
	int		i, idx1, idx2;
	float	varia[4], mean1, mean2;

	unsigned short *pDest = pDestOrig+blockIdx.x*Pitch;
    for (i = threadIdx.x; i < width; i += blockDim.x ) 
	{
        varia[0] = tex2D( texF2, (float)(i-d_FilterHW), (float)(blockIdx.x-d_FilterHW));
        varia[1] = tex2D( texF2, (float)(i+d_FilterHW), (float)(blockIdx.x-d_FilterHW));
        varia[2] = tex2D( texF2, (float)(i-d_FilterHW), (float)(blockIdx.x+d_FilterHW));
        varia[3] = tex2D( texF2, (float)(i+d_FilterHW), (float)(blockIdx.x+d_FilterHW));
		if (varia[0] < varia[1])
		{
			idx1=0;
			mean1=tex2D( texF1, (float)(i-d_FilterHW), (float)(blockIdx.x-d_FilterHW));
		}
		else
		{
			idx1=1;
			mean1=tex2D( texF1, (float)(i+d_FilterHW), (float)(blockIdx.x-d_FilterHW));
		}
		if (varia[2] < varia[3])
		{
			idx2=2;
			mean2=tex2D( texF1, (float)(i-d_FilterHW), (float)(blockIdx.x+d_FilterHW));
		}
		else 
		{
			idx2=3;
			mean2=tex2D( texF1, (float)(i+d_FilterHW), (float)(blockIdx.x+d_FilterHW));
		}
		if (varia[idx1] < varia[idx2])
			pDest[i]=(unsigned short)(mean1*65535.);
		else
			pDest[i]=(unsigned short)(mean2*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// KuwaharaPass2_24bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void KuwaharaPass2_24bit(uchar4 *pDestOrig, unsigned int Pitch, int width, 
									int height)
{ 
	int		i;
	float4	mean[4], varia[4], varc1, varc2, val1, val2;

	uchar4 *pDest = pDestOrig+blockIdx.x*Pitch;
    for (i = threadIdx.x; i < width; i += blockDim.x ) 
	{
        varia[0] = tex2D( tex4F2, (float)(i-d_FilterHW), (float)(blockIdx.x-d_FilterHW));
        varia[1] = tex2D( tex4F2, (float)(i+d_FilterHW), (float)(blockIdx.x-d_FilterHW));
        varia[2] = tex2D( tex4F2, (float)(i-d_FilterHW), (float)(blockIdx.x+d_FilterHW));
        varia[3] = tex2D( tex4F2, (float)(i+d_FilterHW), (float)(blockIdx.x+d_FilterHW));
        mean[0] = tex2D( tex4F1, (float)(i-d_FilterHW), (float)(blockIdx.x-d_FilterHW));
        mean[1] = tex2D( tex4F1, (float)(i+d_FilterHW), (float)(blockIdx.x-d_FilterHW));
        mean[2] = tex2D( tex4F1, (float)(i-d_FilterHW), (float)(blockIdx.x+d_FilterHW));
        mean[3] = tex2D( tex4F1, (float)(i+d_FilterHW), (float)(blockIdx.x+d_FilterHW));
		if (varia[0].x < varia[1].x)
		{
			varc1.x=varia[0].x;
			val1.x=mean[0].x;
		}
		else
		{
			varc1.x=varia[1].x;
			val1.x=mean[1].x;
		}
		if (varia[2].x < varia[3].x)
		{
			varc2.x=varia[2].x;
			val2.x=mean[2].x;
		}
		else
		{
			varc2.x=varia[3].x;
			val2.x=mean[3].x;
		}
		if (varc2.x < varc1.x) val1.x=val2.x;
		if (varia[0].y < varia[1].y)
		{
			varc1.y=varia[0].y;
			val1.y=mean[0].y;
		}
		else
		{
			varc1.y=varia[1].y;
			val1.y=mean[1].y;
		}
		if (varia[2].y < varia[3].y)
		{
			varc2.y=varia[2].y;
			val2.y=mean[2].y;
		}
		else
		{
			varc2.y=varia[3].y;
			val2.y=mean[3].y;
		}
		if (varc2.y < varc1.y) val1.y=val2.y;
		if (varia[0].z < varia[1].z)
		{
			varc1.z=varia[0].z;
			val1.z=mean[0].z;
		}
		else
		{
			varc1.z=varia[1].z;
			val1.z=mean[1].z;
		}
		if (varia[2].z < varia[3].z)
		{
			varc2.z=varia[2].z;
			val2.z=mean[2].z;
		}
		else
		{
			varc2.z=varia[3].z;
			val2.z=mean[3].z;
		}
		if (varc2.z < varc1.z) val1.z=val2.z;
		if (varia[0].w < varia[1].w)
		{
			varc1.w=varia[0].w;
			val1.w=mean[0].w;
		}
		else
		{
			varc1.w=varia[1].w;
			val1.w=mean[1].w;
		}
		if (varia[2].w < varia[3].w)
		{
			varc2.w=varia[2].w;
			val2.w=mean[2].w;
		}
		else
		{
			varc2.w=varia[3].w;
			val2.w=mean[3].w;
		}
		if (varc2.w < varc1.w) val1.w=val2.w;
		pDest[i]=_uchar4(val1*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// KuwaharaPass1_48bit
///////////////////////////////////////////////////////////////////////////////////////

__global__ void KuwaharaPass2_48bit(ushort4 *pDestOrig, unsigned int Pitch, int width, 
									int height)
{ 
	int		i;
	float4	mean[4], varia[4], varc1, varc2, val1, val2;

	ushort4 *pDest = pDestOrig+blockIdx.x*Pitch;
    for (i = threadIdx.x; i < width; i += blockDim.x ) 
	{
        varia[0] = tex2D( tex4F2, (float)(i-d_FilterHW), (float)(blockIdx.x-d_FilterHW));
        varia[1] = tex2D( tex4F2, (float)(i+d_FilterHW), (float)(blockIdx.x-d_FilterHW));
        varia[2] = tex2D( tex4F2, (float)(i-d_FilterHW), (float)(blockIdx.x+d_FilterHW));
        varia[3] = tex2D( tex4F2, (float)(i+d_FilterHW), (float)(blockIdx.x+d_FilterHW));
        mean[0] = tex2D( tex4F1, (float)(i-d_FilterHW), (float)(blockIdx.x-d_FilterHW));
        mean[1] = tex2D( tex4F1, (float)(i+d_FilterHW), (float)(blockIdx.x-d_FilterHW));
        mean[2] = tex2D( tex4F1, (float)(i-d_FilterHW), (float)(blockIdx.x+d_FilterHW));
        mean[3] = tex2D( tex4F1, (float)(i+d_FilterHW), (float)(blockIdx.x+d_FilterHW));
		if (varia[0].x < varia[1].x)
		{
			varc1.x=varia[0].x;
			val1.x=mean[0].x;
		}
		else
		{
			varc1.x=varia[1].x;
			val1.x=mean[1].x;
		}
		if (varia[2].x < varia[3].x)
		{
			varc2.x=varia[2].x;
			val2.x=mean[2].x;
		}
		else
		{
			varc2.x=varia[3].x;
			val2.x=mean[3].x;
		}
		if (varc2.x < varc1.x) val1.x=val2.x;
		if (varia[0].y < varia[1].y)
		{
			varc1.y=varia[0].y;
			val1.y=mean[0].y;
		}
		else
		{
			varc1.y=varia[1].y;
			val1.y=mean[1].y;
		}
		if (varia[2].y < varia[3].y)
		{
			varc2.y=varia[2].y;
			val2.y=mean[2].y;
		}
		else
		{
			varc2.y=varia[3].y;
			val2.y=mean[3].y;
		}
		if (varc2.y < varc1.y) val1.y=val2.y;
		if (varia[0].z < varia[1].z)
		{
			varc1.z=varia[0].z;
			val1.z=mean[0].z;
		}
		else
		{
			varc1.z=varia[1].z;
			val1.z=mean[1].z;
		}
		if (varia[2].z < varia[3].z)
		{
			varc2.z=varia[2].z;
			val2.z=mean[2].z;
		}
		else
		{
			varc2.z=varia[3].z;
			val2.z=mean[3].z;
		}
		if (varc2.z < varc1.z) val1.z=val2.z;
		if (varia[0].w < varia[1].w)
		{
			varc1.w=varia[0].w;
			val1.w=mean[0].w;
		}
		else
		{
			varc1.w=varia[1].w;
			val1.w=mean[1].w;
		}
		if (varia[2].w < varia[3].w)
		{
			varc2.w=varia[2].w;
			val2.w=mean[2].w;
		}
		else
		{
			varc2.w=varia[3].w;
			val2.w=mean[3].w;
		}
		if (varc2.w < varc1.w) val1.w=val2.w;
		pDest[i]=_ushort4(val1*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaKuwaharaPass2
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaKuwaharaPass2(cudaArray *mean, cudaArray *varia, void *dest, 
								  int width, int height, int bpp)
{
	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texF1, mean));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texF2, varia));
				KuwaharaPass2_8bit<<<height, 96>>>((unsigned char *)dest, width, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texF2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texF1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texF1, mean));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texF2, varia));
				KuwaharaPass2_16bit<<<height, 96>>>((unsigned short *)dest, width, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texF2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texF1));
				break;
		case 24:	// packed 8 bit grey or 8 bit colour images
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4F1, mean));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4F2, varia));
				KuwaharaPass2_24bit<<<height, 32>>>((uchar4 *)dest, width, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4F2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4F1));
				break;
		case 48:	// packed 16 bit grey or 16 bit colour images
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4F1, mean));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4F2, varia));
				KuwaharaPass2_48bit<<<height, 32>>>((ushort4 *)dest, width, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4F2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4F1));
				break;
	}
}

