
#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cutil.h>
#include "cudamath.h"
#include "CudaTIP_Kernels.h"

///////////////////////////////////////////////////////////////////////////////////////
// LaplceDevice_8bit
//	- Laplace 8 bit filter based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void LaplaceDevice_8bit(unsigned char *pDestOriginal, int width, int height)
{ 
	int	pix;
    unsigned char *pDest = pDestOriginal+blockIdx.x*width;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		pix=8*tex2D(texUC, (float) i, (float) blockIdx.x);
        pix -= tex2D( texUC, (float) i-1, (float) blockIdx.x-1 );
        pix -= tex2D( texUC, (float) i+0, (float) blockIdx.x-1 );
        pix -= tex2D( texUC, (float) i+1, (float) blockIdx.x-1 );
        pix -= tex2D( texUC, (float) i-1, (float) blockIdx.x+0 );
        pix -= tex2D( texUC, (float) i+1, (float) blockIdx.x+0 );
        pix -= tex2D( texUC, (float) i-1, (float) blockIdx.x+1 );
        pix -= tex2D( texUC, (float) i+0, (float) blockIdx.x+1 );
        pix -= tex2D( texUC, (float) i+1, (float) blockIdx.x+1 );
 		pix=clamp(pix, 0, 255);
        pDest[i] = pix;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// LaplaceDevice_16bit
//	- Laplace 16 bit filter based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void LaplaceDevice_16bit(unsigned short *pDestOriginal, int width, 
									int height)
{ 
	int pix;
    unsigned short *pDest = pDestOriginal+blockIdx.x*width;
    for ( int i = threadIdx.x; i < width; i += blockDim.x )
	{
		pix=8*tex2D(texUS, (float) i, (float) blockIdx.x);
        pix -= tex2D( texUS, (float) i-1, (float) blockIdx.x-1 );
        pix -= tex2D( texUS, (float) i+0, (float) blockIdx.x-1 );
        pix -= tex2D( texUS, (float) i+1, (float) blockIdx.x-1 );
        pix -= tex2D( texUS, (float) i-1, (float) blockIdx.x+0 );
        pix -= tex2D( texUS, (float) i+1, (float) blockIdx.x+0 );
        pix -= tex2D( texUS, (float) i-1, (float) blockIdx.x+1 );
        pix -= tex2D( texUS, (float) i+0, (float) blockIdx.x+1 );
        pix -= tex2D( texUS, (float) i+1, (float) blockIdx.x+1 );
 		pix=clamp(pix, 0, 65535);
        pDest[i] = pix;
   }
}

///////////////////////////////////////////////////////////////////////////////////////
// LaplaceDevice_24bit
//	- Laplace 24 bit filter based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void LaplaceDevice_24bit(uchar4 *pDestOriginal, int width, int height)
{ 
	int4 pix;
    uchar4 *pDest = pDestOriginal+blockIdx.x*width;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		pix=8*_int4(tex2D(tex4UC, (float) i, (float) blockIdx.x));
        pix -= _int4(tex2D( tex4UC, (float) i-1, (float) blockIdx.x-1 ));
        pix -= _int4(tex2D( tex4UC, (float) i+0, (float) blockIdx.x-1 ));
        pix -= _int4(tex2D( tex4UC, (float) i+1, (float) blockIdx.x-1 ));
        pix -= _int4(tex2D( tex4UC, (float) i-1, (float) blockIdx.x+0 ));
        pix -= _int4(tex2D( tex4UC, (float) i+1, (float) blockIdx.x+0 ));
        pix -= _int4(tex2D( tex4UC, (float) i-1, (float) blockIdx.x+1 ));
        pix -= _int4(tex2D( tex4UC, (float) i+0, (float) blockIdx.x+1 ));
        pix -= _int4(tex2D( tex4UC, (float) i+1, (float) blockIdx.x+1 ));
		pix=clamp(pix, 0, 255);
        pDest[i] = _uchar4(pix);
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// LaplaceDevice_48bit
//	- Laplace 48 bit filter based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void LaplaceDevice_48bit(ushort4 *pDestOriginal, int width, int height)
{ 
	int4 pix;
    ushort4 *pDest = pDestOriginal+blockIdx.x*width;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		pix=8*_int4(tex2D(tex4US, (float) i, (float) blockIdx.x));
        pix -= _int4(tex2D( tex4US, (float) i-1, (float) blockIdx.x-1 ));
        pix -= _int4(tex2D( tex4US, (float) i+0, (float) blockIdx.x-1 ));
        pix -= _int4(tex2D( tex4US, (float) i+1, (float) blockIdx.x-1 ));
        pix -= _int4(tex2D( tex4US, (float) i-1, (float) blockIdx.x+0 ));
        pix -= _int4(tex2D( tex4US, (float) i+1, (float) blockIdx.x+0 ));
        pix -= _int4(tex2D( tex4US, (float) i-1, (float) blockIdx.x+1 ));
        pix -= _int4(tex2D( tex4US, (float) i+0, (float) blockIdx.x+1 ));
        pix -= _int4(tex2D( tex4US, (float) i+1, (float) blockIdx.x+1 ));
		pix=clamp(pix, 0, 65535);
        pDest[i] = _ushort4(pix);
    }
}


///////////////////////////////////////////////////////////////////////////////////////
// CudaLaplace
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaLaplace(cudaArray *src, void *dest, int width, int height, int bpp)
{

	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC, src));
				LaplaceDevice_8bit<<<height, 384>>>((unsigned char *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS, src));
				LaplaceDevice_16bit<<<height, 192>>>((unsigned short *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC, src));
				LaplaceDevice_24bit<<<height, 192>>>((uchar4 *)dest, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US, src));
				LaplaceDevice_48bit<<<height, 96>>>((ushort4 *)dest, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US));
				break;
	}
}
