
#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cutil.h>
#include "cudamath.h"
#include "CudaTIP_Kernels.h"

texture<unsigned char, cudaTextureType2D, cudaReadModeNormalizedFloat> texUC1;
texture<unsigned char,  cudaTextureType2D, cudaReadModeNormalizedFloat> texUC2;
texture<unsigned char,  cudaTextureType2D, cudaReadModeNormalizedFloat> texUC3;
texture<unsigned short,  cudaTextureType2D, cudaReadModeNormalizedFloat> texUS1;
texture<unsigned short,  cudaTextureType2D, cudaReadModeNormalizedFloat> texUS2;
texture<unsigned short,  cudaTextureType2D, cudaReadModeNormalizedFloat> texUS3;
texture<uchar4,  cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC1;
texture<uchar4,  cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC2;
texture<uchar4,  cudaTextureType2D, cudaReadModeNormalizedFloat> tex4UC3;
texture<ushort4,  cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US1;
texture<ushort4,  cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US2;
texture<ushort4,  cudaTextureType2D, cudaReadModeNormalizedFloat> tex4US3;
texture<unsigned char,  cudaTextureType2D, cudaReadModeNormalizedFloat> texNUC1;
texture<unsigned short,  cudaTextureType2D, cudaReadModeNormalizedFloat> texNUS1;
texture<float,  cudaTextureType2D, cudaReadModeElementType> texNF1;
texture<uchar4,  cudaTextureType2D, cudaReadModeNormalizedFloat> texN4UC1;
texture<ushort4,  cudaTextureType2D, cudaReadModeNormalizedFloat> texN4US1;

__device__ __constant__ float d_ConstVal;
__device__ __constant__ float d_ScaleX;
__device__ __constant__ float d_ScaleY;
__device__ __constant__ float d_OffsetX;
__device__ __constant__ float d_OffsetY;
__device__ __constant__ float d_CorrTexWidth;
__device__ __constant__ float d_CorrTexHeight;

///////////////////////////////////////////////////////////////////////////////////////
// SubtractDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  SubtractDevice_8bit(unsigned char *pDestOriginal, int width, 
									 int height)
{ 
 	float val;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUC1, x, y) - tex2D(texUC2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(unsigned char)(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// SubtractDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  SubtractDevice_16bit(unsigned short *pDestOriginal, int width, 
									  int height)
{ 
	float val;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUS1, x, y) - tex2D(texUS2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(short)(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// SubtractDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  SubtractDevice_24bit(uchar4 *pDestOriginal, int width, int height)
{ 
 	float4 val;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4UC1, x, y) - tex2D(tex4UC2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_uchar4(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// SubtractDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  SubtractDevice_48bit(ushort4 *pDestOriginal, int width, int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4US1, x, y) - tex2D(tex4US2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_ushort4(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaSubtract
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaSubtract(cudaArray *src1, cudaArray *src2, void *dest, int width, 
							 int height, int bpp)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC2, src2));
				SubtractDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS2, src2));
				SubtractDevice_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest,
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC2, src2));
				SubtractDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US2, src2));
				SubtractDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, width,
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// DiffDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  DiffDevice_8bit(unsigned char *pDestOriginal, int width, int height)
{ 
 	float val;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUC1, x, y) - tex2D(texUC2, x, y);
		pDestOriginal[y*width+x]=(unsigned char)(abs(val)*255.);
	}
}


///////////////////////////////////////////////////////////////////////////////////////
// DiffDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  DiffDevice_16bit(unsigned short *pDestOriginal, int width, int height)
{ 
	float val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUS1, x, y) - tex2D(texUS2, x, y);
		pDestOriginal[y*width+x]=(short)(abs(val)*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// DiffDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  DiffDevice_24bit(uchar4 *pDestOriginal, int width, int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4UC1, x, y) - tex2D(tex4UC2, x, y);
		pDestOriginal[y*width+x]=_uchar4(abs(val)*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// DiffDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  DiffDevice_48bit(ushort4 *pDestOriginal, int width, int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4US1, x, y) - tex2D(tex4US2, x, y);
		pDestOriginal[y*width+x]=_ushort4(abs(val)*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaDiff
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaDiff(cudaArray *src1, cudaArray *src2, void *dest, int width, 
						 int height, int bpp)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC2, src2));
				DiffDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS2, src2));
				DiffDevice_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC2, src2));
				DiffDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US2, src2));
				DiffDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// AddDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  AddDevice_8bit(unsigned char *pDestOriginal, int width, int height)
{
	float val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUC1, x, y) + tex2D(texUC2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(unsigned char)(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// AddDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  AddDevice_16bit(unsigned short *pDestOriginal, int width, int height)
{ 
	float val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUS1, x, y) + tex2D(texUS2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(short)(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// AddDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  AddDevice_24bit(uchar4 *pDestOriginal, int width, int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4UC1, x, y) + tex2D(tex4UC2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_uchar4(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// AddDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  AddDevice_48bit(ushort4 *pDestOriginal, int width, int height)
{ 
 	float4 val;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4US1, x, y) + tex2D(tex4US2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_ushort4(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaAdd
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaAdd(cudaArray *src1, cudaArray *src2, void *dest, int width, 
						int height, int bpp)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC2, src2));
				AddDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS2, src2));
				AddDevice_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC2, src2));
				AddDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US2, src2));
				AddDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// MultiplyDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  MultiplyDevice_8bit(unsigned char *pDestOriginal, int width, 
									 int height)
{ 
	float val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUC1, x, y) * tex2D(texUC2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(unsigned char)(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// MultiplyDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  MultiplyDevice_16bit(unsigned short *pDestOriginal, int width, 
									  int height)
{ 
	float val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUS1, x, y) * tex2D(texUS2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(short)(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// MultiplyDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  MultiplyDevice_24bit(uchar4 *pDestOriginal, int width, int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4UC1, x, y) * tex2D(tex4UC2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_uchar4(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// MultiplyDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  MultiplyDevice_48bit(ushort4 *pDestOriginal, int width, int height)
{ 
 	float4 val;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4US1, x, y) * tex2D(tex4US2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_ushort4(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaMultiply
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaMultiply(cudaArray *src1, cudaArray *src2, void *dest, int width, 
							 int height, int bpp)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC2, src2));
				MultiplyDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS2, src2));
				MultiplyDevice_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC2, src2));
				MultiplyDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US2, src2));
				MultiplyDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// DivideDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  DivideDevice_8bit(unsigned char *pDestOriginal, int width, int height)
{ 
	float val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUC1, x, y) / tex2D(texUC2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(unsigned char)(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// DivideDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  DivideDevice_16bit(unsigned short *pDestOriginal, int width, 
									int height)
{ 
	float val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUS1, x, y) / tex2D(texUS2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(short)(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// DivideDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  DivideDevice_24bit(uchar4 *pDestOriginal, int width, int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4UC1, x, y) / tex2D(tex4UC2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_uchar4(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// DivideDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  DivideDevice_48bit(ushort4 *pDestOriginal, int width, int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4US1, x, y) / tex2D(tex4US2, x, y);
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_ushort4(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaDivide
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaDivide(cudaArray *src1, cudaArray *src2, void *dest, int width, 
						   int height, int bpp)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC2, src2));
				DivideDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS2, src2));
				DivideDevice_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC2, src2));
				DivideDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US2, src2));
				DivideDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// ModDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  ModDevice_8bit(unsigned char *pDestOriginal, int width, int height)
{ 
	float val1, val2;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val1=tex2D(texUC1, x, y);
		val2=tex2D(texUC2, x, y);
		val1=sqrt(val1*val1+val2*val2);
		val1=clamp(val1, 0.0, 1.0);
		pDestOriginal[y*width+x]=(unsigned char)(val1*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// ModDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  ModDevice_16bit(unsigned short *pDestOriginal, int width, int height)
{ 
	float val1, val2;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val1=tex2D(texUS1, x, y);
		val2=tex2D(texUS2, x, y);
		val1=sqrt(val1*val1+val2*val2);
		val1=clamp(val1, 0.0, 1.0);
		pDestOriginal[y*width+x]=(short)(val1*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// ModDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  ModDevice_24bit(uchar4 *pDestOriginal, int width, int height)
{ 
	float4 val1, val2;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val1=tex2D(tex4UC1, x, y);
		val2=tex2D(tex4UC2, x, y);
		val1=val1*val1;
		val2=val2*val2;
		val1=sqrt(val1+val2);
		val1=clamp(val1, 0.0, 1.0);
		pDestOriginal[y*width+x]=_uchar4(val1*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// ModDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  ModDevice_48bit(ushort4 *pDestOriginal, int width, int height)
{ 
	float4 val1, val2;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val1=tex2D(tex4US1, x, y);
		val2=tex2D(tex4US2, x, y);
		val1=val1*val1;
		val2=val2*val2;
		val1=sqrt(val1+val2);
		val1=clamp(val1, 0.0, 1.0);
		pDestOriginal[y*width+x]=_ushort4(val1*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaMod
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaMod(cudaArray *src1, cudaArray *src2, void *dest, int width, 
						int height, int bpp)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC2, src2));
				ModDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS2, src2));
				ModDevice_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS2));
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC2, src2));
				ModDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src1));
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US2, src2));
				ModDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US2));
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// SubtractConstDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  SubtractConstDevice_8bit(unsigned char *pDestOriginal, int width, 
										  int height)
{ 
	float val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUC1, x, y) - d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(unsigned char)(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// SubtractConstDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  SubtractConstDevice_16bit(unsigned short *pDestOriginal, int width, 
										   int height)
{ 
	float val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUS1, x, y) - d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(short)(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// SubtractConstDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  SubtractConstDevice_24bit(uchar4 *pDestOriginal, int width, 
										   int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4UC1, x, y) - d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_uchar4(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// SubtractConstDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  SubtractConstDevice_48bit(ushort4 *pDestOriginal, int width, 
										   int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4US1, x, y) - d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_ushort4(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaSubtractConst
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaSubtractConst(cudaArray *src,  void *dest, float val, int width, 
								 int height, int bpp)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_ConstVal", &val, sizeof(float)));
	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src));
				SubtractConstDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src));
				SubtractConstDevice_16bit<<< dimGrid, dimBlock>>>(
					(unsigned short *)dest, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src));
				SubtractConstDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src));
				SubtractConstDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// AddConstDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  AddConstDevice_8bit(unsigned char *pDestOriginal, int width, 
									 int height)
{ 
	float val;
		
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUC1, x, y) + d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(unsigned char)(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// AddConstDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  AddConstDevice_16bit(unsigned short *pDestOriginal, int width, 
									  int height)
{ 
	float val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUS1, x, y) + d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(short)(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// AddConstDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  AddConstDevice_24bit(uchar4 *pDestOriginal, int width, int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4UC1, x, y) + d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_uchar4(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// AddConstDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  AddConstDevice_48bit(ushort4 *pDestOriginal, int width, int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4US1, x, y) + d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_ushort4(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaAddConst
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaAddConst(cudaArray *src,  void *dest, float val, int width, 
							 int height, int bpp)
{
	dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_ConstVal", &val, sizeof(float)));
	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src));
				AddConstDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src));
				AddConstDevice_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src));
				AddConstDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src));
				AddConstDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// MultiplyConstDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  MultiplyConstDevice_8bit(unsigned char *pDestOriginal, int width, 
										  int height)
{ 
	float val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUC1, x, y) * d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(unsigned char)(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// MultiplyConstDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  MultiplyConstDevice_16bit(unsigned short *pDestOriginal, int width, 
										   int height)
{ 
	float val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUS1, x, y) * d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(short)(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// MultiplyConstDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  MultiplyConstDevice_24bit(uchar4 *pDestOriginal, int width, 
										   int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4UC1, x, y) * d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_uchar4(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// MultiplyConstDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  MultiplyConstDevice_48bit(ushort4 *pDestOriginal, int width, 
										   int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4UC1, x, y) * d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_ushort4(val*65535.);
	}
}


///////////////////////////////////////////////////////////////////////////////////////
// CudaMultiplyConst
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaMultiplyConst(cudaArray *src,  void *dest, float val, int width, 
								 int height, int bpp)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_ConstVal", &val, sizeof(float)));
	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src));
				MultiplyConstDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src));
				MultiplyConstDevice_16bit<<< dimGrid, dimBlock>>>(
					(unsigned short *)dest, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src));
				MultiplyConstDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest,
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src));
				MultiplyConstDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest,
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// DivideConstDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  DivideConstDevice_8bit(unsigned char *pDestOriginal, int width, 
										int height)
{ 
	float val;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUC1, x, y) / d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(unsigned char)(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// DivideConstDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  DivideConstDevice_16bit(unsigned short *pDestOriginal, int width, 
										 int height)
{ 
	float val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(texUS1, x, y) / d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=(short)(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// DivideConstDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  DivideConstDevice_24bit(uchar4 *pDestOriginal, int width, int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4UC1, x, y) / d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_uchar4(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// DivideConstDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  DivideConstDevice_48bit(ushort4 *pDestOriginal, int width, int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4US1, x, y) / d_ConstVal;
		val=clamp(val, 0.0, 1.0);
		pDestOriginal[y*width+x]=_ushort4(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaDivideConst
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaDivideConst(cudaArray *src,  void *dest, float val, int width,
								int height, int bpp)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_ConstVal", &val, sizeof(float)));
	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src));
				DivideConstDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src));
				DivideConstDevice_16bit<<< dimGrid, dimBlock>>>(
					(unsigned short *)dest, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src));
				DivideConstDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src));
				DivideConstDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// ThreshDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  ThreshDevice_8bit(unsigned char *pDest, int width, int height)
{ 
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		if (tex2D(texUC1, x, y) < d_ConstVal)
			pDest[y*width+x]=0;
		else
			pDest[y*width+x]=255;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// ThreshDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  ThreshDevice_16bit(unsigned short *pDest, int width, int height)
{ 
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		if (tex2D(texUS1, x, y) < d_ConstVal)
			pDest[y*width+x]=0;
		else
			pDest[y*width+x]=65535;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// ThreshDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  ThreshDevice_24bit(uchar4 *pDest, int width, int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4UC1, x, y) - d_ConstVal;
		if (val.x < 0) val.x=0.; 
			else val.x=1.;
		if (val.y < 0) val.y=0.; 
			else val.y=1.;
		if (val.z < 0) val.z=0.; 
			else val.z=1.;
		if (val.w < 0) val.w=0.; 
			else val.w=1.;
		pDest[y*width+x]=_uchar4(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// ThreshDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  ThreshDevice_48bit(ushort4 *pDest, int width, int height)
{ 
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		val=tex2D(tex4US1, x, y) - d_ConstVal;
		if (val.x < 0) val.x=0.; 
			else val.x=1.;
		if (val.y < 0) val.y=0.; 
			else val.y=1.;
		if (val.z < 0) val.z=0.; 
			else val.z=1.;
		if (val.w < 0) val.w=0.; 
			else val.w=1.;
		pDest[y*width+x]=_ushort4(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaThresh
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaThresh(cudaArray *src,  void *dest, float thresh, int width, 
						   int height, int bpp)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_ConstVal", &thresh, sizeof(float)));
	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC1, src));
				ThreshDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC1));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS1, src));
				ThreshDevice_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest, 
					width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS1));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC1, src));
				ThreshDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC1));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US1, src));
				ThreshDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CropResizeDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CropResizeDevice_8bit(unsigned char *pDest, int width, int height)
{ 
	float val, u, v;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x*d_ScaleX/(float)width + d_OffsetX;
		v=(float)y*d_ScaleY/(float)height + d_OffsetY;
		val=tex2D(texNUC1, u, v);
		pDest[y*width+x]=(unsigned char)(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CropResizeDevice_8to32bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CropResizeDevice_8to32bit(float *pDest, int width, int height)
{ 
	float val, u, v;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x*d_ScaleX/(float)width + d_OffsetX;
		v=(float)y*d_ScaleY/(float)height + d_OffsetY;
		val=tex2D(texNUC1, u, v);
		pDest[y*width+x]=val;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CropResizeDevice_8to64bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CropResizeDevice_8to64bit(float2 *pDest, int width, int height)
{ 
	float val, u, v;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x*d_ScaleX/(float)width + d_OffsetX;
		v=(float)y*d_ScaleY/(float)height + d_OffsetY;
		val=tex2D(texNUC1, u, v);
		pDest[y*width+x].x=val;
		pDest[y*width+x].y=0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CropResizeDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CropResizeDevice_16bit(unsigned short *pDest, int width, int height)
{ 
	float val, u, v;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x*d_ScaleX/(float)width + d_OffsetX;
		v=(float)y*d_ScaleY/(float)height + d_OffsetY;
		val=tex2D(texNUS1, u, v);
		pDest[y*width+x]=(unsigned char)(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CropResizeDevice_16to32bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CropResizeDevice_16to32bit(float *pDest, int width, int height)
{ 
	float val, u, v;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x*d_ScaleX/(float)width + d_OffsetX;
		v=(float)y*d_ScaleY/(float)height + d_OffsetY;
		val=tex2D(texNUS1, u, v);
		pDest[y*width+x]=val;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CropResizeDevice_16to64bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CropResizeDevice_16to64bit(float2 *pDest, int width, int height)
{ 
	float val, u, v;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x*d_ScaleX/(float)width + d_OffsetX;
		v=(float)y*d_ScaleY/(float)height + d_OffsetY;
		val=tex2D(texNUS1, u, v);
		pDest[y*width+x].x=val;
		pDest[y*width+x].y=0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CropResizeDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CropResizeDevice_24bit(uchar4 *pDest, int width, int height)
{ 
	float u, v;
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x*d_ScaleX/(float)width + d_OffsetX;
		v=(float)y*d_ScaleY/(float)height + d_OffsetY;
		val=tex2D(texN4UC1, u, v);
		pDest[y*width+x]=_uchar4(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CropResizeDevice_24to32bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CropResizeDevice_24to32bit(float *pDest, int width, int height)
{ 
	float u, v;
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x*d_ScaleX/(float)width + d_OffsetX;
		v=(float)y*d_ScaleY/(float)height + d_OffsetY;
		val=tex2D(texN4UC1, u, v);
		pDest[y*width+x]=(val.x*0.3+ val.y*0.59+val.z*0.11);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CropResizeDevice_24to64bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CropResizeDevice_24to64bit(float2 *pDest, int width, int height)
{ 
	float u, v;
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x*d_ScaleX/(float)width + d_OffsetX;
		v=(float)y*d_ScaleY/(float)height + d_OffsetY;
		val=tex2D(texN4UC1, u, v);
		pDest[y*width+x].x=(val.x*0.3+ val.y*0.59+val.z*0.11);
		pDest[y*width+x].y=0;
	}
}
///////////////////////////////////////////////////////////////////////////////////////
// CropResizeDevice_32bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CropResizeDevice_32bit(float *pDest, int width, int height)
{ 
	float val, u, v;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x*d_ScaleX/(float)width + d_OffsetX;
		v=(float)y*d_ScaleY/(float)height + d_OffsetY;
		val=tex2D(texNF1, u, v);
		pDest[y*width+x]=val;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CropResizeDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CropResizeDevice_48bit(ushort4 *pDest, int width, int height)
{ 
	float u, v;
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x*d_ScaleX/(float)width + d_OffsetX;
		v=(float)y*d_ScaleY/(float)height + d_OffsetY;
		val=tex2D(texN4US1, u, v);
		pDest[y*width+x]=_ushort4(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CropResizeDevice_48to32bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CropResizeDevice_48to32bit(float *pDest, int width, int height)
{ 
	float u, v;
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x*d_ScaleX/(float)width + d_OffsetX;
		v=(float)y*d_ScaleY/(float)height + d_OffsetY;
		val=tex2D(texN4US1, u, v);
		pDest[y*width+x]=(val.x*0.3+ val.y*0.59+val.z*0.11);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CropResizeDevice_48to64bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  CropResizeDevice_48to64bit(float2 *pDest, int width, int height)
{ 
	float u, v;
	float4 val;
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x*d_ScaleX/(float)width + d_OffsetX;
		v=(float)y*d_ScaleY/(float)height + d_OffsetY;
		val=tex2D(texN4US1, u, v);
		pDest[y*width+x].x=(val.x*0.3+ val.y*0.59+val.z*0.11);
		pDest[y*width+x].y=0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaInitScaleAndOffset
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaInitScaleAndOffset(float scaleX, float scaleY, float offsetX,
									   float offsetY)
{
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_ScaleX", &scaleX, sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_ScaleY", &scaleY, sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_OffsetX", &offsetX, sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_OffsetY", &offsetY, sizeof(float)));
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaCropAndResize
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaCropAndResize(cudaArray *src,  void *dest, int width, int height, 
								  int bpp, int bppDest)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bpp)
	{
		case 8:
				texNUC1.addressMode[0] = cudaAddressModeWrap;
				texNUC1.addressMode[1] = cudaAddressModeWrap;
				texNUC1.filterMode = cudaFilterModeLinear;
				texNUC1.normalized = true;    // access with normalized texture coordinates
				CUDA_SAFE_CALL(cudaBindTextureToArray(texNUC1, src));
				if (bppDest == 8)
					CropResizeDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, 
						width, height);
				else if (bppDest == 32)
					CropResizeDevice_8to32bit<<< dimGrid, dimBlock>>>((float *)dest, width, 
						height);
				else if (bppDest == 64)
					CropResizeDevice_8to64bit<<< dimGrid, dimBlock>>>((float2 *)dest, width, 
						height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texNUC1));
				break;
		case 16:
				texNUS1.addressMode[0] = cudaAddressModeWrap;
				texNUS1.addressMode[1] = cudaAddressModeWrap;
				texNUS1.filterMode = cudaFilterModeLinear;
				texNUS1.normalized = true;    // access with normalized texture coordinates
				CUDA_SAFE_CALL(cudaBindTextureToArray(texNUS1, src));
				if (bppDest == 16)
					CropResizeDevice_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest, 
						width, height);
				else if (bppDest == 32)
					CropResizeDevice_16to32bit<<< dimGrid, dimBlock>>>((float *)dest, width, 
						height);
				else if (bppDest == 64)
					CropResizeDevice_16to64bit<<< dimGrid, dimBlock>>>((float2 *)dest, width, 
						height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texNUS1));
				break;
		case 24:
				texN4UC1.addressMode[0] = cudaAddressModeWrap;
				texN4UC1.addressMode[1] = cudaAddressModeWrap;
				texN4UC1.filterMode = cudaFilterModeLinear;
				texN4UC1.normalized = true;    // access with normalized texture coordinates
				CUDA_SAFE_CALL(cudaBindTextureToArray(texN4UC1, src));
				if (bppDest == 24)
					CropResizeDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, width, 
						height);
				else if (bppDest == 32)
					CropResizeDevice_24to32bit<<< dimGrid, dimBlock>>>((float *)dest, width, 
						height);
				else if (bppDest == 64)
					CropResizeDevice_24to64bit<<< dimGrid, dimBlock>>>((float2 *)dest, width, 
						height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texN4UC1));
				break;
		case 32:
				texNF1.addressMode[0] = cudaAddressModeWrap;
				texNF1.addressMode[1] = cudaAddressModeWrap;
				texNF1.filterMode = cudaFilterModeLinear;
				texNF1.normalized = true;    // access with normalized texture coordinates
				CUDA_SAFE_CALL(cudaBindTextureToArray(texNF1, src));
				CropResizeDevice_32bit<<< dimGrid, dimBlock>>>((float *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texNF1));
				break;
		case 48:
				texN4US1.addressMode[0] = cudaAddressModeWrap;
				texN4US1.addressMode[1] = cudaAddressModeWrap;
				texN4US1.filterMode = cudaFilterModeLinear;
				texN4US1.normalized = true;    // access with normalized texture coordinates
				CUDA_SAFE_CALL(cudaBindTextureToArray(texN4US1, src));
				if (bppDest == 48)
					CropResizeDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, width, 
						height);
				else if (bppDest == 32)
					CropResizeDevice_48to32bit<<< dimGrid, dimBlock>>>((float *)dest, width, 
						height);
				else if (bppDest == 64)
					CropResizeDevice_48to64bit<<< dimGrid, dimBlock>>>((float2 *)dest, width, 
						height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texN4US1));
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// UnpackGreyDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  UnpackGreyDevice_8bit(unsigned char *pDest, int width, int height)
{ 
	float4	color;
	float	u, v, uu, vv, val;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x/(float)width;
		v=(float)y/(float)height;
		if ((u<=0.5) && (v<=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u;
			vv=(2-2*d_CorrTexHeight)*v;
			color=tex2D(texN4UC1, uu, vv);
			val=color.x;
		}
		else if ((u<=0.5) && (v>=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u;
			vv=(2-2*d_CorrTexHeight)*v+2*d_CorrTexHeight-1.;
			color = tex2D(texN4UC1, uu, vv);
			val=color.z;
		}
		else if ((u>=0.5) && (v<=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u+2*d_CorrTexWidth-1.;
			vv=(2-2*d_CorrTexHeight)*v;
			color = tex2D(texN4UC1, uu, vv);
			val=color.y;
		}
		else if ((u>=0.5) && (v>=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u+2*d_CorrTexWidth-1.;
			vv=(2-2*d_CorrTexHeight)*v+2*d_CorrTexHeight-1.;
			color = tex2D(texN4UC1, uu, vv);
			val=color.w;
		}
		pDest[y*width+x]=(unsigned char)(val*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// UnpackGreyDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  UnpackGreyDevice_16bit(unsigned short *pDest, int width, int height)
{ 
	float4	color;
	float	u, v, uu, vv, val;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x/(float)width;
		v=(float)y/(float)height;
		if ((u<=0.5) && (v<=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u;
			vv=(2-2*d_CorrTexHeight)*v;
			color=tex2D(texN4US1, uu, vv);
			val=color.z;
		}
		else if ((u<=0.5) && (v>=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u;
			vv=(2-2*d_CorrTexHeight)*v+2*d_CorrTexHeight-1.;
			color = tex2D(texN4US1, uu, vv);
			val=color.x;
		}
		else if ((u>=0.5) && (v<=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u+2*d_CorrTexWidth-1.;
			vv=(2-2*d_CorrTexHeight)*v;
			color = tex2D(texN4US1, uu, vv);
			val=color.y;
		}
		else if ((u>=0.5) && (v>=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u+2*d_CorrTexWidth-1.;
			vv=(2-2*d_CorrTexHeight)*v+2*d_CorrTexHeight-1.;
			color = tex2D(texN4US1, uu, vv);
			val=color.w;
		}
		pDest[y*width+x]=(unsigned short)(val*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// UnpackGreyDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  UnpackGreyDevice_24bit(uchar4 *pDest, int width, int height)
{ 
	float4	color;
	float	u, v, uu, vv;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x/(float)width;
		v=(float)y/(float)height;
		if ((u<=0.5) && (v<=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u;
			vv=(2-2*d_CorrTexHeight)*v;
			color=tex2D(texN4UC1, uu, vv);
			color.z=color.x;
			color.y=color.x;
			color.w=0;
		}
		else if ((u<=0.5) && (v>=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u;
			vv=(2-2*d_CorrTexHeight)*v+2*d_CorrTexHeight-1.;
			color = tex2D(texN4UC1, uu, vv);
			color.x=color.z;
			color.y=color.z;
			color.w=0;
		}
		else if ((u>=0.5) && (v<=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u+2*d_CorrTexWidth-1.;
			vv=(2-2*d_CorrTexHeight)*v;
			color = tex2D(texN4UC1, uu, vv);
			color.x=color.y;
			color.z=color.y;
			color.w=0;
		}
		else if ((u>=0.5) && (v>=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u+2*d_CorrTexWidth-1.;
			vv=(2-2*d_CorrTexHeight)*v+2*d_CorrTexHeight-1.;
			color = tex2D(texN4UC1, uu, vv);
			color.x=color.w;
			color.y=color.w;
			color.z=color.w;
			color.w=0;
		}
		pDest[y*width+x]=_uchar4(color*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// UnpackGreyDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  UnpackGreyDevice_48bit(ushort4 *pDest, int width, int height)
{ 
	float4	color;
	float	u, v, uu, vv;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=(float)x/(float)width;
		v=(float)y/(float)height;
		if ((u<=0.5) && (v<=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u;
			vv=(2-2*d_CorrTexHeight)*v;
			color=tex2D(texN4UC1, uu, vv);
			color.x=color.z;
			color.y=color.z;
			color.w=0;
		}
		else if ((u<=0.5) && (v>=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u;
			vv=(2-2*d_CorrTexHeight)*v+2*d_CorrTexHeight-1.;
			color = tex2D(texN4UC1, uu, vv);
			color.z=color.x;
			color.y=color.x;
			color.w=0;
		}
		else if ((u>=0.5) && (v<=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u+2*d_CorrTexWidth-1.;
			vv=(2-2*d_CorrTexHeight)*v;
			color = tex2D(texN4UC1, uu, vv);
			color.x=color.y;
			color.z=color.y;
			color.w=0;
		}
		else if ((u>=0.5) && (v>=0.5))
		{
			uu=(2-2*d_CorrTexWidth)*u+2*d_CorrTexWidth-1.;
			vv=(2-2*d_CorrTexHeight)*v+2*d_CorrTexHeight-1.;
			color = tex2D(texN4UC1, uu, vv);
			color.x=color.w;
			color.y=color.w;
			color.z=color.w;
			color.w=0;
		}
		pDest[y*width+x]=_ushort4(color*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaInitCorrOffset
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaInitCorrOffset(float corrTexWidth, float corrTexHeight)
{
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_CorrTexWidth", &corrTexWidth, sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_CorrTexHeight", &corrTexHeight, sizeof(float)));
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaUnpackGrey
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaUnpackGrey(cudaArray *src,  void *dest, int width, int height, 
								  int bpp, int bppDest)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bpp)
	{
		case 8:
				switch (bppDest)
				{
					case 8:
							texN4UC1.addressMode[0] = cudaAddressModeWrap;
							texN4UC1.addressMode[1] = cudaAddressModeWrap;
							texN4UC1.filterMode = cudaFilterModeLinear;
							texN4UC1.normalized = true;    // access with normalized texture coordinates
							CUDA_SAFE_CALL(cudaBindTextureToArray(texN4UC1, src));
							UnpackGreyDevice_8bit<<< dimGrid, dimBlock>>>((unsigned char *)dest, 
								width, height);
							CUDA_SAFE_CALL(cudaUnbindTexture(texN4UC1));
							break;
					case 24:
							texN4UC1.addressMode[0] = cudaAddressModeWrap;
							texN4UC1.addressMode[1] = cudaAddressModeWrap;
							texN4UC1.filterMode = cudaFilterModeLinear;
							texN4UC1.normalized = true;    // access with normalized texture coordinates
							CUDA_SAFE_CALL(cudaBindTextureToArray(texN4UC1, src));
							UnpackGreyDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, width, 
								height);
							CUDA_SAFE_CALL(cudaUnbindTexture(texN4UC1));
							break;
				}
				break;
		case 16:
				switch (bppDest)
				{
					case 16:
							texN4US1.addressMode[0] = cudaAddressModeWrap;
							texN4US1.addressMode[1] = cudaAddressModeWrap;
							texN4US1.filterMode = cudaFilterModeLinear;
							texN4US1.normalized = true;    // access with normalized texture coordinates
							CUDA_SAFE_CALL(cudaBindTextureToArray(texN4US1, src));
							UnpackGreyDevice_16bit<<< dimGrid, dimBlock>>>((unsigned short *)dest, 
								width, height);
							CUDA_SAFE_CALL(cudaUnbindTexture(texN4US1));
							break;
					case 48:
							texN4US1.addressMode[0] = cudaAddressModeWrap;
							texN4US1.addressMode[1] = cudaAddressModeWrap;
							texN4US1.filterMode = cudaFilterModeLinear;
							texN4US1.normalized = true;    // access with normalized texture coordinates
							CUDA_SAFE_CALL(cudaBindTextureToArray(texN4US1, src));
							UnpackGreyDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, width, 
								height);
							CUDA_SAFE_CALL(cudaUnbindTexture(texN4US1));
							break;
				}
	}
}
///////////////////////////////////////////////////////////////////////////////////////
// PackGreyDevice_8bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  PackGreyDevice_8bit(uchar4 *pDest, int width, int height)
{ 
	float4	color;
	float	u, v, uu, vv;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=0.5*(float)x/(float)width;
		v=0.5*(float)y/(float)height;
		uu=u;
		vv=v;
		color.x=tex2D(texNUC1, uu, vv);
		uu=u + 0.5-d_CorrTexWidth;
		vv=v;
		color.y = tex2D(texNUC1, uu, vv);
		uu=u;
		vv=v +0.5-d_CorrTexHeight;
		color.z = tex2D(texNUC1, uu, vv);
		uu=u + 0.5-d_CorrTexWidth;
		vv=v +0.5-d_CorrTexHeight;
		color.w = tex2D(texNUC1, uu, vv);
		pDest[y*width+x]=_uchar4(color*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// PackGreyDevice_16bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  PackGreyDevice_16bit(ushort4 *pDest, int width, int height)
{ 
	float4	color;
	float	u, v, uu, vv;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=0.5*(float)x/(float)width;
		v=0.5*(float)y/(float)height;
		uu=u;
		vv=v;
		color.z=tex2D(texNUS1, uu, vv);
		uu=u + 0.5-d_CorrTexWidth;
		vv=v;
		color.y = tex2D(texNUS1, uu, vv);
		uu=u;
		vv=v +0.5-d_CorrTexHeight;
		color.x = tex2D(texNUS1, uu, vv);
		uu=u + 0.5-d_CorrTexWidth;
		vv=v +0.5-d_CorrTexHeight;
		color.w = tex2D(texNUS1, uu, vv);
		pDest[y*width+x]=_ushort4(color*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// PackGreyDevice_24bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  PackGreyDevice_24bit(uchar4 *pDest, int width, int height)
{ 
	float4	color, val;
	float	u, v, uu, vv;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=0.5*(float)x/(float)width;
		v=0.5*(float)y/(float)height;
		uu=u;
		vv=v;
		val=tex2D(texN4UC1, uu, vv);
		color.x=(val.x*0.3+ val.y*0.59+val.z*0.11);
		uu=u + 0.5-d_CorrTexWidth;
		vv=v;
		val = tex2D(texN4UC1, uu, vv);
		color.y=(val.x*0.3+ val.y*0.59+val.z*0.11);
		uu=u;
		vv=v +0.5-d_CorrTexHeight;
		val = tex2D(texN4UC1, uu, vv);
		color.z=(val.x*0.3+ val.y*0.59+val.z*0.11);
		uu=u + 0.5-d_CorrTexWidth;
		vv=v +0.5-d_CorrTexHeight;
		val = tex2D(texN4UC1, uu, vv);
		color.w=(val.x*0.3+ val.y*0.59+val.z*0.11);
		pDest[y*width+x]=_uchar4(color*255.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// PackGreyDevice_48bit
///////////////////////////////////////////////////////////////////////////////////////
__global__ void  PackGreyDevice_48bit(ushort4 *pDest, int width, int height)
{ 
	float4	val, color;
	float	u, v, uu, vv;
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((y<height) && (x<width))
	{
		u=0.5*(float)x/(float)width;
		v=0.5*(float)y/(float)height;
		uu=u;
		vv=v;
		val=tex2D(texN4US1, uu, vv);
		color.z=(val.x*0.3+ val.y*0.59+val.z*0.11);
		uu=u + 0.5-d_CorrTexWidth;
		vv=v;
		val = tex2D(texN4US1, uu, vv);
		color.y=(val.x*0.3+ val.y*0.59+val.z*0.11);
		uu=u;
		vv=v +0.5-d_CorrTexHeight;
		val = tex2D(texN4US1, uu, vv);
		color.x=(val.x*0.3+ val.y*0.59+val.z*0.11);
		uu=u + 0.5-d_CorrTexWidth;
		vv=v +0.5-d_CorrTexHeight;
		val = tex2D(texN4US1, uu, vv);
		color.w=(val.x*0.3+ val.y*0.59+val.z*0.11);
		pDest[y*width+x]=_ushort4(color*65535.);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaPackGrey
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaPackGrey(cudaArray *src,  void *dest, int width, int height, 
							int bpp)
{
    dim3 dimBlock(8, 8, 1);
    dim3 dimGrid(iDivUp(width, dimBlock.x), iDivUp(height, dimBlock.y), 1);

	switch (bpp)
	{
		case 8:
					texNUC1.addressMode[0] = cudaAddressModeWrap;
					texNUC1.addressMode[1] = cudaAddressModeWrap;
					texNUC1.filterMode = cudaFilterModeLinear;
					texNUC1.normalized = true;    // access with normalized texture coordinates
					CUDA_SAFE_CALL(cudaBindTextureToArray(texNUC1, src));
					PackGreyDevice_8bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, width, 
						height);
					CUDA_SAFE_CALL(cudaUnbindTexture(texNUC1));
					break;
		case 24:
					texN4UC1.addressMode[0] = cudaAddressModeWrap;
					texN4UC1.addressMode[1] = cudaAddressModeWrap;
					texN4UC1.filterMode = cudaFilterModeLinear;
					texN4UC1.normalized = true;    // access with normalized texture coordinates
					CUDA_SAFE_CALL(cudaBindTextureToArray(texN4UC1, src));
					PackGreyDevice_24bit<<< dimGrid, dimBlock>>>((uchar4 *)dest, width, 
						height);
					CUDA_SAFE_CALL(cudaUnbindTexture(texN4UC1));
					break;
		case 16:
					texNUS1.addressMode[0] = cudaAddressModeWrap;
					texNUS1.addressMode[1] = cudaAddressModeWrap;
					texNUS1.filterMode = cudaFilterModeLinear;
					texNUS1.normalized = true;    // access with normalized texture coordinates
					CUDA_SAFE_CALL(cudaBindTextureToArray(texNUS1, src));
					PackGreyDevice_16bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, width, 
						height);
					CUDA_SAFE_CALL(cudaUnbindTexture(texNUS1));
					break;
		case 48:
					texN4US1.addressMode[0] = cudaAddressModeWrap;
					texN4US1.addressMode[1] = cudaAddressModeWrap;
					texN4US1.filterMode = cudaFilterModeLinear;
					texN4US1.normalized = true;    // access with normalized texture coordinates
					CUDA_SAFE_CALL(cudaBindTextureToArray(texN4US1, src));
					PackGreyDevice_48bit<<< dimGrid, dimBlock>>>((ushort4 *)dest, width, 
						height);
					CUDA_SAFE_CALL(cudaUnbindTexture(texN4US1));
					break;
	}
}
