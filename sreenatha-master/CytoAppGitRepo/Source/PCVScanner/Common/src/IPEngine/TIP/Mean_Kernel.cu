
#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cutil.h>
#include "cudamath.h"
#include "CudaTIP_Kernels.h"

///////////////////////////////////////////////////////////////////////////////////////
// MeanDevice_8bit
//	- Mean 8 bit filter based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MeanDevice_8bit(unsigned char *pDestOriginal, int width, int height)
{ 
	int	pix;
    unsigned char *pDest = pDestOriginal+blockIdx.x*width;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		pix = tex2D(texUC, (float) i, (float) blockIdx.x);
        pix += tex2D( texUC, (float) i-1, (float) blockIdx.x-1 );
        pix += tex2D( texUC, (float) i+0, (float) blockIdx.x-1 );
        pix += tex2D( texUC, (float) i+1, (float) blockIdx.x-1 );
        pix += tex2D( texUC, (float) i-1, (float) blockIdx.x+0 );
        pix += tex2D( texUC, (float) i+1, (float) blockIdx.x+0 );
        pix += tex2D( texUC, (float) i-1, (float) blockIdx.x+1 );
        pix += tex2D( texUC, (float) i+0, (float) blockIdx.x+1 );
        pix += tex2D( texUC, (float) i+1, (float) blockIdx.x+1 );
        pDest[i] = pix/9;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// MeanDevice_16bit
//	- Mean 16 bit filter based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MeanDevice_16bit(unsigned short *pDestOriginal, int width, 
									int height)
{ 
	int pix;
    unsigned short *pDest = pDestOriginal+blockIdx.x*width;
    for ( int i = threadIdx.x; i < width; i += blockDim.x )
	{
		pix = tex2D(texUS, (float) i, (float) blockIdx.x);
        pix += tex2D( texUS, (float) i-1, (float) blockIdx.x-1 );
        pix += tex2D( texUS, (float) i+0, (float) blockIdx.x-1 );
        pix += tex2D( texUS, (float) i+1, (float) blockIdx.x-1 );
        pix += tex2D( texUS, (float) i-1, (float) blockIdx.x+0 );
        pix += tex2D( texUS, (float) i+1, (float) blockIdx.x+0 );
        pix += tex2D( texUS, (float) i-1, (float) blockIdx.x+1 );
        pix += tex2D( texUS, (float) i+0, (float) blockIdx.x+1 );
        pix += tex2D( texUS, (float) i+1, (float) blockIdx.x+1 );
        pDest[i] = pix/9;
   }
}

///////////////////////////////////////////////////////////////////////////////////////
// MeanDevice_24bit
//	- Mean 24 bit filter based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MeanDevice_24bit(uchar4 *pDestOriginal, int width, int height)
{ 
	int4 pix;
    uchar4 *pDest = pDestOriginal+blockIdx.x*width;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		pix = _int4(tex2D(tex4UC, (float) i, (float) blockIdx.x));
        pix += _int4(tex2D( tex4UC, (float) i-1, (float) blockIdx.x-1 ));
        pix += _int4(tex2D( tex4UC, (float) i+0, (float) blockIdx.x-1 ));
        pix += _int4(tex2D( tex4UC, (float) i+1, (float) blockIdx.x-1 ));
        pix += _int4(tex2D( tex4UC, (float) i-1, (float) blockIdx.x+0 ));
        pix += _int4(tex2D( tex4UC, (float) i+1, (float) blockIdx.x+0 ));
        pix += _int4(tex2D( tex4UC, (float) i-1, (float) blockIdx.x+1 ));
        pix += _int4(tex2D( tex4UC, (float) i+0, (float) blockIdx.x+1 ));
        pix += _int4(tex2D( tex4UC, (float) i+1, (float) blockIdx.x+1 ));
        pDest[i] = _uchar4(pix/9);
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// MeanDevice_48bit
//	- Mean 48 bit filter based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MeanDevice_48bit(ushort4 *pDestOriginal, int width, int height)
{ 
	int4 pix;
    ushort4 *pDest = pDestOriginal+blockIdx.x*width;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		pix = _int4(tex2D(tex4US, (float) i, (float) blockIdx.x));
        pix += _int4(tex2D( tex4US, (float) i-1, (float) blockIdx.x-1 ));
        pix += _int4(tex2D( tex4US, (float) i+0, (float) blockIdx.x-1 ));
        pix += _int4(tex2D( tex4US, (float) i+1, (float) blockIdx.x-1 ));
        pix += _int4(tex2D( tex4US, (float) i-1, (float) blockIdx.x+0 ));
        pix += _int4(tex2D( tex4US, (float) i+1, (float) blockIdx.x+0 ));
        pix += _int4(tex2D( tex4US, (float) i-1, (float) blockIdx.x+1 ));
        pix += _int4(tex2D( tex4US, (float) i+0, (float) blockIdx.x+1 ));
        pix += _int4(tex2D( tex4US, (float) i+1, (float) blockIdx.x+1 ));
        pDest[i] = _ushort4(pix/9);
    }
}


///////////////////////////////////////////////////////////////////////////////////////
// CudaMean
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaMean(cudaArray *src, void *dest, int width, int height, int bpp)
{

	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC, src));
				MeanDevice_8bit<<<height, 384>>>((unsigned char *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS, src));
				MeanDevice_16bit<<<height, 192>>>((unsigned short *)dest, width, 
					height);
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC, src));
				MeanDevice_24bit<<<height, 192>>>((uchar4 *)dest, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US, src));
				MeanDevice_48bit<<<height, 96>>>((ushort4 *)dest, width, height);
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US));
				break;
	}
}
