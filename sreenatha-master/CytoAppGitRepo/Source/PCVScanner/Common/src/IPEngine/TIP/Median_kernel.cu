#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cutil.h>
#include "cudamath.h"
#include "CudaTIP_Kernels.h"

///////////////////////////////////////////////////////////////////////////////////////
// sorts two pixels (unsigned char) in place, putting smallest in a
///////////////////////////////////////////////////////////////////////////////////////
__device__ void  SortUC(unsigned char* a, unsigned char* b)
{
    unsigned char tmp;
	if (*a > *b)
	{
		tmp = *a;
		*a = *b;
		*b = tmp;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// MedianDevice_8bit
//	- Median operation 8 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void  MedianDevice_8bit(unsigned char *pDestOrig, unsigned int Pitch, 
								int width, int height)
{ 
	unsigned char  pix[9];
    unsigned char *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		pix[0] = tex2D( texUC, (float) i,   (float) blockIdx.x);
        pix[1] = tex2D( texUC, (float) i+0, (float) blockIdx.x-1);
        pix[2] = tex2D( texUC, (float) i-1, (float) blockIdx.x+0);
        pix[3] = tex2D( texUC, (float) i+1, (float) blockIdx.x+0);
        pix[4] = tex2D( texUC, (float) i+0, (float) blockIdx.x+1);
        pix[5] = tex2D( texUC, (float) i-1, (float) blockIdx.x-1);
		pix[6] = tex2D( texUC, (float) i+1, (float) blockIdx.x-1);
        pix[7] = tex2D( texUC, (float) i-1, (float) blockIdx.x+1);
        pix[8] = tex2D( texUC, (float) i+1, (float) blockIdx.x+1);
		SortUC(&pix[0], &pix[1]);
   
		// 0-2
		SortUC(&pix[1], &pix[2]);
		SortUC(&pix[0], &pix[1]);
    
		// 0-3
		SortUC(&pix[2], &pix[3]);
		SortUC(&pix[1], &pix[2]);
		SortUC(&pix[0], &pix[1]);
    
		// 0-4
		SortUC(&pix[3], &pix[4]);
		SortUC(&pix[2], &pix[3]);
		SortUC(&pix[1], &pix[2]);
		SortUC(&pix[0], &pix[1]);

		// 0-5    
		SortUC(&pix[4], &pix[5]);
		SortUC(&pix[3], &pix[4]);
		SortUC(&pix[2], &pix[3]);
		SortUC(&pix[1], &pix[2]);
		SortUC(&pix[0], &pix[1]);

		// 0-6
		SortUC(&pix[5], &pix[6]);    
		SortUC(&pix[4], &pix[5]);
		SortUC(&pix[3], &pix[4]);
		SortUC(&pix[2], &pix[3]);
		SortUC(&pix[1], &pix[2]);
		SortUC(&pix[0], &pix[1]);

		// 0-7
		SortUC(&pix[6], &pix[7]);
		SortUC(&pix[5], &pix[6]);    
		SortUC(&pix[4], &pix[5]);
		SortUC(&pix[3], &pix[4]);
		SortUC(&pix[2], &pix[3]);
		SortUC(&pix[1], &pix[2]);
		SortUC(&pix[0], &pix[1]);

		// 0-8
		SortUC(&pix[7], &pix[8]);    
		SortUC(&pix[6], &pix[7]);
		SortUC(&pix[5], &pix[6]);    
		SortUC(&pix[4], &pix[5]);
		SortUC(&pix[3], &pix[4]);
		SortUC(&pix[2], &pix[3]);
		SortUC(&pix[1], &pix[2]);
		SortUC(&pix[0], &pix[1]);
		pDest[i]=pix[4];
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// sorts two pixels (unsigned short) in place, putting smallest in a
///////////////////////////////////////////////////////////////////////////////////////
__device__ void  SortUS(unsigned short* a, unsigned short* b)
{
    unsigned short tmp;
	if (*a > *b)
	{
		tmp = *a;
		*a = *b;
		*b = tmp;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// MinCDevice_16bit
//	- Min (cross) operation 16 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MedianDevice_16bit(unsigned short *pDestOrig, unsigned int Pitch, 
								   int width, int height)
{ 
 	unsigned short pix[9];
	unsigned short *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x )
	{
		pix[0] = tex2D( texUS, (float) i,   (float) blockIdx.x);
        pix[1] = tex2D( texUS, (float) i+0, (float) blockIdx.x-1);
        pix[2] = tex2D( texUS, (float) i-1, (float) blockIdx.x+0);
        pix[3] = tex2D( texUS, (float) i+1, (float) blockIdx.x+0);
        pix[4] = tex2D( texUS, (float) i+0, (float) blockIdx.x+1);
        pix[5] = tex2D( texUS, (float) i-1, (float) blockIdx.x-1);
		pix[6] = tex2D( texUS, (float) i+1, (float) blockIdx.x-1);
        pix[7] = tex2D( texUS, (float) i-1, (float) blockIdx.x+1);
        pix[8] = tex2D( texUS, (float) i+1, (float) blockIdx.x+1);
		SortUS(&pix[0], &pix[1]);
   
		// 0-2
		SortUS(&pix[1], &pix[2]);
		SortUS(&pix[0], &pix[1]);
    
		// 0-3
		SortUS(&pix[2], &pix[3]);
		SortUS(&pix[1], &pix[2]);
		SortUS(&pix[0], &pix[1]);
    
		// 0-4
		SortUS(&pix[3], &pix[4]);
		SortUS(&pix[2], &pix[3]);
		SortUS(&pix[1], &pix[2]);
		SortUS(&pix[0], &pix[1]);

		// 0-5    
		SortUS(&pix[4], &pix[5]);
		SortUS(&pix[3], &pix[4]);
		SortUS(&pix[2], &pix[3]);
		SortUS(&pix[1], &pix[2]);
		SortUS(&pix[0], &pix[1]);

		// 0-6
		SortUS(&pix[5], &pix[6]);    
		SortUS(&pix[4], &pix[5]);
		SortUS(&pix[3], &pix[4]);
		SortUS(&pix[2], &pix[3]);
		SortUS(&pix[1], &pix[2]);
		SortUS(&pix[0], &pix[1]);

		// 0-7
		SortUS(&pix[6], &pix[7]);
		SortUS(&pix[5], &pix[6]);    
		SortUS(&pix[4], &pix[5]);
		SortUS(&pix[3], &pix[4]);
		SortUS(&pix[2], &pix[3]);
		SortUS(&pix[1], &pix[2]);
		SortUS(&pix[0], &pix[1]);

		// 0-8
		SortUS(&pix[7], &pix[8]);    
		SortUS(&pix[6], &pix[7]);
		SortUS(&pix[5], &pix[6]);    
		SortUS(&pix[4], &pix[5]);
		SortUS(&pix[3], &pix[4]);
		SortUS(&pix[2], &pix[3]);
		SortUS(&pix[1], &pix[2]);
		SortUS(&pix[0], &pix[1]);
		pDest[i]=pix[4];
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// sorts two pixels (unsigned char) in place, putting smallest in a
///////////////////////////////////////////////////////////////////////////////////////
__device__ void  Sort4UC(uchar4* a, uchar4* b)
{
    unsigned char tmp;
	if (a->x > b->x)
	{
		tmp = a->x;
		a->x = b->x;
		b->x = tmp;
	}
	if (a->y > b->y)
	{
		tmp = a->y;
		a->y = b->y;
		b->y = tmp;
	}
	if (a->z > b->z)
	{
		tmp = a->z;
		a->z = b->z;
		b->z = tmp;
	}
	if (a->w > b->w)
	{
		tmp = a->w;
		a->w = b->w;
		b->w = tmp;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// MedianDevice_24bit
//	- Median operation 24 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MedianDevice_24bit(uchar4 *pDestOrig, unsigned int Pitch, 
								  int width, int height)
{ 
	uchar4  pix[9];
    uchar4 *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		pix[0] = tex2D( tex4UC, (float) i,   (float) blockIdx.x);
        pix[1] = tex2D( tex4UC, (float) i+0, (float) blockIdx.x-1);
        pix[2] = tex2D( tex4UC, (float) i-1, (float) blockIdx.x+0);
        pix[3] = tex2D( tex4UC, (float) i+1, (float) blockIdx.x+0);
        pix[4] = tex2D( tex4UC, (float) i+0, (float) blockIdx.x+1);
        pix[5] = tex2D( tex4UC, (float) i-1, (float) blockIdx.x-1);
		pix[6] = tex2D( tex4UC, (float) i+1, (float) blockIdx.x-1);
        pix[7] = tex2D( tex4UC, (float) i-1, (float) blockIdx.x+1);
        pix[8] = tex2D( tex4UC, (float) i+1, (float) blockIdx.x+1);
		Sort4UC(&pix[0], &pix[1]);
   
		// 0-2
		Sort4UC(&pix[1], &pix[2]);
		Sort4UC(&pix[0], &pix[1]);
    
		// 0-3
		Sort4UC(&pix[2], &pix[3]);
		Sort4UC(&pix[1], &pix[2]);
		Sort4UC(&pix[0], &pix[1]);
    
		// 0-4
		Sort4UC(&pix[3], &pix[4]);
		Sort4UC(&pix[2], &pix[3]);
		Sort4UC(&pix[1], &pix[2]);
		Sort4UC(&pix[0], &pix[1]);

		// 0-5    
		Sort4UC(&pix[4], &pix[5]);
		Sort4UC(&pix[3], &pix[4]);
		Sort4UC(&pix[2], &pix[3]);
		Sort4UC(&pix[1], &pix[2]);
		Sort4UC(&pix[0], &pix[1]);

		// 0-6
		Sort4UC(&pix[5], &pix[6]);    
		Sort4UC(&pix[4], &pix[5]);
		Sort4UC(&pix[3], &pix[4]);
		Sort4UC(&pix[2], &pix[3]);
		Sort4UC(&pix[1], &pix[2]);
		Sort4UC(&pix[0], &pix[1]);

		// 0-7
		Sort4UC(&pix[6], &pix[7]);
		Sort4UC(&pix[5], &pix[6]);    
		Sort4UC(&pix[4], &pix[5]);
		Sort4UC(&pix[3], &pix[4]);
		Sort4UC(&pix[2], &pix[3]);
		Sort4UC(&pix[1], &pix[2]);
		Sort4UC(&pix[0], &pix[1]);

		// 0-8
		Sort4UC(&pix[7], &pix[8]);    
		Sort4UC(&pix[6], &pix[7]);
		Sort4UC(&pix[5], &pix[6]);    
		Sort4UC(&pix[4], &pix[5]);
		Sort4UC(&pix[3], &pix[4]);
		Sort4UC(&pix[2], &pix[3]);
		Sort4UC(&pix[1], &pix[2]);
		Sort4UC(&pix[0], &pix[1]);
		pDest[i]=pix[4];
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// sorts two pixels (unsigned char) in place, putting smallest in a
///////////////////////////////////////////////////////////////////////////////////////
__device__ void  Sort4US(ushort4* a, ushort4* b)
{
    unsigned short tmp;
	if (a->x > b->x)
	{
		tmp = a->x;
		a->x = b->x;
		b->x = tmp;
	}
	if (a->y > b->y)
	{
		tmp = a->y;
		a->y = b->y;
		b->y = tmp;
	}
	if (a->z > b->z)
	{
		tmp = a->z;
		a->z = b->z;
		b->z = tmp;
	}
	if (a->w > b->w)
	{
		tmp = a->w;
		a->w = b->w;
		b->w = tmp;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
// MedianDevice_48bit
//	- Median operation 48 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MedianDevice_48bit(ushort4 *pDestOrig, unsigned int Pitch, 
								  int width, int height)
{ 
	ushort4 pix[9];
	ushort4 *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		pix[0] = tex2D( tex4US, (float) i,   (float) blockIdx.x);
        pix[1] = tex2D( tex4US, (float) i+0, (float) blockIdx.x-1);
        pix[2] = tex2D( tex4US, (float) i-1, (float) blockIdx.x+0);
        pix[3] = tex2D( tex4US, (float) i+1, (float) blockIdx.x+0);
        pix[4] = tex2D( tex4US, (float) i+0, (float) blockIdx.x+1);
        pix[5] = tex2D( tex4US, (float) i-1, (float) blockIdx.x-1);
		pix[6] = tex2D( tex4US, (float) i+1, (float) blockIdx.x-1);
        pix[7] = tex2D( tex4US, (float) i-1, (float) blockIdx.x+1);
        pix[8] = tex2D( tex4US, (float) i+1, (float) blockIdx.x+1);
		Sort4US(&pix[0], &pix[1]);
   
		// 0-2
		Sort4US(&pix[1], &pix[2]);
		Sort4US(&pix[0], &pix[1]);
    
		// 0-3
		Sort4US(&pix[2], &pix[3]);
		Sort4US(&pix[1], &pix[2]);
		Sort4US(&pix[0], &pix[1]);
    
		// 0-4
		Sort4US(&pix[3], &pix[4]);
		Sort4US(&pix[2], &pix[3]);
		Sort4US(&pix[1], &pix[2]);
		Sort4US(&pix[0], &pix[1]);

		// 0-5    
		Sort4US(&pix[4], &pix[5]);
		Sort4US(&pix[3], &pix[4]);
		Sort4US(&pix[2], &pix[3]);
		Sort4US(&pix[1], &pix[2]);
		Sort4US(&pix[0], &pix[1]);

		// 0-6
		Sort4US(&pix[5], &pix[6]);    
		Sort4US(&pix[4], &pix[5]);
		Sort4US(&pix[3], &pix[4]);
		Sort4US(&pix[2], &pix[3]);
		Sort4US(&pix[1], &pix[2]);
		Sort4US(&pix[0], &pix[1]);

		// 0-7
		Sort4US(&pix[6], &pix[7]);
		Sort4US(&pix[5], &pix[6]);    
		Sort4US(&pix[4], &pix[5]);
		Sort4US(&pix[3], &pix[4]);
		Sort4US(&pix[2], &pix[3]);
		Sort4US(&pix[1], &pix[2]);
		Sort4US(&pix[0], &pix[1]);

		// 0-8
		Sort4US(&pix[7], &pix[8]);    
		Sort4US(&pix[6], &pix[7]);
		Sort4US(&pix[5], &pix[6]);    
		Sort4US(&pix[4], &pix[5]);
		Sort4US(&pix[3], &pix[4]);
		Sort4US(&pix[2], &pix[3]);
		Sort4US(&pix[1], &pix[2]);
		Sort4US(&pix[0], &pix[1]);
		pDest[i]=pix[4];
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// CudaMedian
//	- Median filter  in device memory
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaMedian(cudaArray *src, void *dest, cudaArray *tmpIma, int width, 
						   int height, int bpp, int nPasses)
{
	int			size;
	cudaArray	*src0;

	if (bpp==24)
		size=width*height*4;
	else if (bpp==48)
		size=width*height*8;
	else
		size=width*height*bpp/8;
	switch (bpp)
	{
		case 8:
				src0=src;
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC, src0));
				for (int i=0; i<nPasses; i++)
				{
					MedianDevice_8bit<<<height, 96>>>((unsigned char *)dest, width, 
													    width, height);
					if (i != (nPasses-1))
					{
						if (i==0)
						{
							CUDA_SAFE_CALL(cudaUnbindTexture(texUC));
							src0=tmpIma;
							CUDA_SAFE_CALL(cudaBindTextureToArray(texUC, src0));
						}
						CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)src0, 0, 0, 
										dest, size, cudaMemcpyDeviceToDevice));
					}
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC));
				break;
		case 16:
				src0=src;
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS, src0));
				for (int i=0; i<nPasses; i++)
				{
					MedianDevice_16bit<<<height, 96>>>((unsigned short *)dest, width, 
														 width, height);
					if (i != (nPasses-1))
					{
						if (i==0)
						{
							CUDA_SAFE_CALL(cudaUnbindTexture(texUS));
							src0=tmpIma;
							CUDA_SAFE_CALL(cudaBindTextureToArray(texUS, src0));
						}
						CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)src0, 0, 0, 
										dest, size, cudaMemcpyDeviceToDevice));
					}
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS));
				break;
		case 24:
				src0=src;
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC, src0));
				for (int i=0; i<nPasses; i++)
				{
					MedianDevice_24bit<<< height, 32>>>((uchar4 *)dest, width, width, 
														  height);
					if (i != (nPasses-1))
					{
						if (i==0)
						{
							CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC));
							src0=tmpIma;
							CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC, src0));
						}
						CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)src0, 0, 0, 
										dest, size, cudaMemcpyDeviceToDevice));
					}
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC));
				break;
		case 48:
				src0=src;
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US, src0));
				for (int i=0; i<nPasses; i++)
				{
					MedianDevice_48bit<<< height, 32>>>((ushort4 *)dest, width, width, 
														  height);
					if (i != (nPasses-1))
					{
						if (i==0)
						{
							CUDA_SAFE_CALL(cudaUnbindTexture(tex4US));
							src0=tmpIma;
							CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US, src0));
						}
						CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)src0, 0, 0, 
										dest, size, cudaMemcpyDeviceToDevice));
					}
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US));
				break;
	}
}
