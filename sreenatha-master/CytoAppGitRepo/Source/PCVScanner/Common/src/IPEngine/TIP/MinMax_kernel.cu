#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cutil.h>
#include "cudamath.h"
#include "CudaTIP_Kernels.h"

__device__ __constant__ uchar4 whiteVal = {0xFF,0xFF,0xFF,0xFF};
__device__ __constant__ uchar4 blackVal = {0,0,0,0};
__device__ __constant__ ushort4 whiteShortVal = {0xFFFF,0xFFFF,0xFFFF,0xFFFF};
__device__ __constant__ ushort4 blackShortVal = {0,0,0,0};


#ifdef MINMAX_SHARED
////////////////////////////////////////////////////////////////////////////////
// Kernel configuration
////////////////////////////////////////////////////////////////////////////////
// Assuming ROW_TILE_W, KERNEL_RADIUS_ALIGNED and dataW 
// are multiples of maximum coalescable read/write size,
// all global memory operations are coalesced in convolutionRowGPU()
// Assuming COLUMN_TILE_W and dataW are multiples
// of maximum coalescable read/write size, all global memory operations 
// are coalesced in convolutionColumnGPU()

////////////////////////////////////////////////////////////////////////////////
// MinRow_8bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MinRow_8bit(unsigned char *d_Result, unsigned char *d_Data, 
							int width, int dataH)
{
    //Data cache
    __shared__ unsigned char data[MAX_KERNELSIZE-1 + ROW_TILE_W];

    //Current tile and apron limits, relative to row start
    const int         tileStart = IMUL(blockIdx.x, ROW_TILE_W);
    const int           tileEnd = tileStart + ROW_TILE_W - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;

    //Clamp tile and apron limits by image borders
    const int    tileEndClamped = min(tileEnd, width - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, width - 1);

    //Row start index in d_Data[]
    const int          rowStart = IMUL(blockIdx.y, width);

    //Aligned apron start. Assuming dataW and ROW_TILE_W are multiples 
    //of half-warp size, rowStart + apronStartAligned is also a 
    //multiple of half-warp size, thus having proper alignment 
    //for coalesced d_Data[] read.
    const int apronStartAligned = tileStart - d_KernelRadiusAligned;

    const int loadPos = apronStartAligned + threadIdx.x;
    //Set the entire data cache contents
    //Load global memory values, if indices are within the image borders,
    //or initialize with zeroes otherwise
    if(loadPos >= apronStart)
	{
        const int smemPos = loadPos - apronStart;
		data[smemPos] = 
			((loadPos >= apronStartClamped) && (loadPos <= apronEndClamped)) ?
            d_Data[rowStart + loadPos] : 0xFF;
    }

    //Ensure the completness of the loading stage
    //because results, emitted by each thread depend on the data,
    //loaded by another threads
    __syncthreads();

    const int writePos = tileStart + threadIdx.x;
    //Assuming dataW and ROW_TILE_W are multiples of half-warp size,
    //rowStart + tileStart is also a multiple of half-warp size,
    //thus having proper alignment for coalesced d_Result[] write.
    if(writePos <= tileEndClamped)
	{
        const int smemPos = writePos - apronStart;
        unsigned char minV = data[smemPos-d_KernelRadius];
        for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
			if (data[smemPos + k] < minV) minV = data[smemPos + k];
        d_Result[rowStart + writePos] = minV;
    }
}


////////////////////////////////////////////////////////////////////////////////
// MinColumn_8bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MinColumn_8bit(unsigned char *d_Result, unsigned char *d_Data,
							   int width, int dataH, int smemStride, 
							   int gmemStride)
{
    //Data cache
    __shared__ unsigned char data[COLUMN_TILE_W * (MAX_KERNELSIZE-1 +
																COLUMN_TILE_H)];

    //Current tile and apron limits, in rows
    const int         tileStart = IMUL(blockIdx.y, COLUMN_TILE_H);
    const int           tileEnd = tileStart + COLUMN_TILE_H - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;

    //Clamp tile and apron limits by image borders
    const int    tileEndClamped = min(tileEnd, dataH - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, dataH - 1);

    //Current column index
    const int       columnStart = IMUL(blockIdx.x, COLUMN_TILE_W) + threadIdx.x;

    //Shared and global memory indices for current column
    int smemPos = IMUL(threadIdx.y, COLUMN_TILE_W) + threadIdx.x;
    int gmemPos = IMUL(apronStart + threadIdx.y, width) + columnStart;
    //Cycle through the entire data cache
    //Load global memory values, if indices are within the image borders,
    //or initialize with zero otherwise
    for(int y = apronStart + threadIdx.y; y <= apronEnd; y += blockDim.y)
	{
        data[smemPos] = 
			((y >= apronStartClamped) && (y <= apronEndClamped)) ? 
			d_Data[gmemPos] : 0xFF;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }

    //Ensure the completness of the loading stage
    //because results, emitted by each thread depend on the data, 
    //loaded by another threads
    __syncthreads();

    //Shared and global memory indices for current column
    smemPos = IMUL(threadIdx.y + d_KernelRadius, COLUMN_TILE_W) + threadIdx.x;
    gmemPos = IMUL(tileStart + threadIdx.y , width) + columnStart;
    //Cycle through the tile body, clamped by image borders
    //Calculate and output the results
    for(int y = tileStart + threadIdx.y; y <= tileEndClamped; y += blockDim.y)
	{
        unsigned char minV = data[smemPos + IMUL(-d_KernelRadius, COLUMN_TILE_W)];
		for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
			if (data[smemPos + IMUL(k, COLUMN_TILE_W)]< minV) 
				minV = data[smemPos + IMUL(k, COLUMN_TILE_W)];
		d_Result[gmemPos] = minV;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
}

////////////////////////////////////////////////////////////////////////////////
// MinRow_16bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MinRow_16bit(unsigned short *d_Result, unsigned short *d_Data, 
							 int width, int dataH)
{
     __shared__ unsigned short data[MAX_KERNELSIZE-1 + ROW_TILE_W];

    const int         tileStart = IMUL(blockIdx.x, ROW_TILE_W);
    const int           tileEnd = tileStart + ROW_TILE_W - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;
    const int    tileEndClamped = min(tileEnd, width - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, width - 1);
    const int          rowStart = IMUL(blockIdx.y, width);
    const int apronStartAligned = tileStart - d_KernelRadiusAligned;
    const int			loadPos = apronStartAligned + threadIdx.x;

	if(loadPos >= apronStart)
	{
        const int smemPos = loadPos - apronStart;
		data[smemPos] = 
			((loadPos >= apronStartClamped) && (loadPos <= apronEndClamped)) ?
            d_Data[rowStart + loadPos] : 0xFFFF;
    }
    __syncthreads();

    const int writePos = tileStart + threadIdx.x;
    if(writePos <= tileEndClamped)
	{
        const int smemPos = writePos - apronStart;
        unsigned short minV = data[smemPos-d_KernelRadius];
        for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
			if (data[smemPos + k] < minV) minV = data[smemPos + k];
        d_Result[rowStart + writePos] = minV;
    }
}


////////////////////////////////////////////////////////////////////////////////
// MinColumn_16bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MinColumn_16bit(unsigned short *d_Result, unsigned short *d_Data,
								int width, int dataH, int smemStride, 
								int gmemStride)
{
    __shared__ unsigned short data[COLUMN_TILE_W * (MAX_KERNELSIZE-1 + 
																COLUMN_TILE_H)];

    const int         tileStart = IMUL(blockIdx.y, COLUMN_TILE_H);
    const int           tileEnd = tileStart + COLUMN_TILE_H - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;
    const int    tileEndClamped = min(tileEnd, dataH - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, dataH - 1);
    const int       columnStart = IMUL(blockIdx.x, COLUMN_TILE_W) + threadIdx.x;

    int smemPos = IMUL(threadIdx.y, COLUMN_TILE_W) + threadIdx.x;
    int gmemPos = IMUL(apronStart + threadIdx.y, width) + columnStart;
    for(int y = apronStart + threadIdx.y; y <= apronEnd; y += blockDim.y)
	{
        data[smemPos] = 
			((y >= apronStartClamped) && (y <= apronEndClamped)) ? 
			d_Data[gmemPos] : 0xFFFF;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
    __syncthreads();

    smemPos = IMUL(threadIdx.y + d_KernelRadius, COLUMN_TILE_W) + threadIdx.x;
    gmemPos = IMUL(tileStart + threadIdx.y , width) + columnStart;
    for(int y = tileStart + threadIdx.y; y <= tileEndClamped; y += blockDim.y)
	{
        unsigned short minV = data[smemPos + IMUL(-d_KernelRadius, COLUMN_TILE_W)];
		for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
			if (data[smemPos + IMUL(k, COLUMN_TILE_W)] < minV) 
				minV = data[smemPos + IMUL(k, COLUMN_TILE_W)];
		d_Result[gmemPos] = minV;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
}

////////////////////////////////////////////////////////////////////////////////
// MinRow_24bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MinRow_24bit(uchar4 *d_Result, uchar4 *d_Data, int width, 
							 int dataH)
{
     __shared__ uchar4 data[MAX_KERNELSIZE-1 + ROW_TILE_W];

    const int         tileStart = IMUL(blockIdx.x, ROW_TILE_W);
    const int           tileEnd = tileStart + ROW_TILE_W - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;
    const int    tileEndClamped = min(tileEnd, width - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, width - 1);
    const int          rowStart = IMUL(blockIdx.y, width);
    const int apronStartAligned = tileStart - d_KernelRadiusAligned;
    const int			loadPos = apronStartAligned + threadIdx.x;
//	float4		nullVal = {0,0,0,0};

	if(loadPos >= apronStart)
	{
        const int smemPos = loadPos - apronStart;
		data[smemPos] = 
			((loadPos >= apronStartClamped) && (loadPos <= apronEndClamped)) ?
				d_Data[rowStart + loadPos] : whiteVal;
    }
    __syncthreads();

    const int writePos = tileStart + threadIdx.x;
    if(writePos <= tileEndClamped)
	{
        const int smemPos = writePos - apronStart;
        uchar4 minV = data[smemPos-d_KernelRadius];
        for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
		{
			if (data[smemPos + k].x < minV.x) minV.x = data[smemPos + k].x;
			if (data[smemPos + k].y < minV.y) minV.y = data[smemPos + k].y;
			if (data[smemPos + k].z < minV.z) minV.z = data[smemPos + k].z;
			if (data[smemPos + k].w < minV.w) minV.w = data[smemPos + k].w;
		}
        d_Result[rowStart + writePos] = minV;
    }
}


////////////////////////////////////////////////////////////////////////////////
// MinColumn_24bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MinColumn_24bit(uchar4 *d_Result, uchar4 *d_Data, int width, 
								int dataH, int smemStride, int gmemStride)
{
    __shared__ uchar4 data[COLUMN_TILE_W * (MAX_KERNELSIZE-1 + COLUMN_TILE_H4)];

    const int         tileStart = IMUL(blockIdx.y, COLUMN_TILE_H4);
    const int           tileEnd = tileStart + COLUMN_TILE_H4 - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;
    const int    tileEndClamped = min(tileEnd, dataH - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, dataH - 1);
    const int       columnStart = IMUL(blockIdx.x, COLUMN_TILE_W) + threadIdx.x;

    int smemPos = IMUL(threadIdx.y, COLUMN_TILE_W) + threadIdx.x;
    int gmemPos = IMUL(apronStart + threadIdx.y, width) + columnStart;
    for(int y = apronStart + threadIdx.y; y <= apronEnd; y += blockDim.y)
	{
        data[smemPos] = 
			((y >= apronStartClamped) && (y <= apronEndClamped)) ? 
				d_Data[gmemPos] : whiteVal;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
    __syncthreads();

    smemPos = IMUL(threadIdx.y + d_KernelRadius, COLUMN_TILE_W) + threadIdx.x;
    gmemPos = IMUL(tileStart + threadIdx.y , width) + columnStart;
    for(int y = tileStart + threadIdx.y; y <= tileEndClamped; y += blockDim.y)
	{
        uchar4 minV = data[smemPos + IMUL(-d_KernelRadius, COLUMN_TILE_W)];
		for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
		{
			if (data[smemPos + IMUL(k, COLUMN_TILE_W)].x < minV.x) 
				minV.x = data[smemPos + IMUL(k, COLUMN_TILE_W)].x;
			if (data[smemPos + IMUL(k, COLUMN_TILE_W)].y < minV.y) 
				minV.y = data[smemPos + IMUL(k, COLUMN_TILE_W)].y;
			if (data[smemPos + IMUL(k, COLUMN_TILE_W)].z < minV.z) 
				minV.z = data[smemPos + IMUL(k, COLUMN_TILE_W)].z;
			if (data[smemPos + IMUL(k, COLUMN_TILE_W)].w < minV.w) 
				minV.w = data[smemPos + IMUL(k, COLUMN_TILE_W)].w;
		}
		d_Result[gmemPos] = minV;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
}

////////////////////////////////////////////////////////////////////////////////
// MinRow_48bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MinRow_48bit(ushort4 *d_Result, ushort4 *d_Data, int width, 
							 int dataH)
{
     __shared__ ushort4 data[MAX_KERNELSIZE-1 + ROW_TILE_W];

    const int         tileStart = IMUL(blockIdx.x, ROW_TILE_W);
    const int           tileEnd = tileStart + ROW_TILE_W - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;
    const int    tileEndClamped = min(tileEnd, width - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, width - 1);
    const int          rowStart = IMUL(blockIdx.y, width);
    const int apronStartAligned = tileStart - d_KernelRadiusAligned;
    const int			loadPos = apronStartAligned + threadIdx.x;
//	float4		nullVal = {0,0,0,0};

	if(loadPos >= apronStart)
	{
        const int smemPos = loadPos - apronStart;
		data[smemPos] = 
			((loadPos >= apronStartClamped) && (loadPos <= apronEndClamped)) ?
				d_Data[rowStart + loadPos] : whiteShortVal;
    }
    __syncthreads();

    const int writePos = tileStart + threadIdx.x;
    if(writePos <= tileEndClamped)
	{
        const int smemPos = writePos - apronStart;
        ushort4 minV = data[smemPos-d_KernelRadius];
        for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
		{
			if (data[smemPos + k].x < minV.x) minV.x = data[smemPos + k].x;
			if (data[smemPos + k].y < minV.y) minV.y = data[smemPos + k].y;
			if (data[smemPos + k].z < minV.z) minV.z = data[smemPos + k].z;
			if (data[smemPos + k].w < minV.w) minV.w = data[smemPos + k].w;
		}
        d_Result[rowStart + writePos] = minV;
    }
}


////////////////////////////////////////////////////////////////////////////////
// MinColumn_48bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MinColumn_48bit(ushort4 *d_Result, ushort4 *d_Data, int width, 
								int dataH, int smemStride, int gmemStride)
{
    __shared__ ushort4 data[COLUMN_TILE_W * (MAX_KERNELSIZE-1 + COLUMN_TILE_H4)];

    const int         tileStart = IMUL(blockIdx.y, COLUMN_TILE_H4);
    const int           tileEnd = tileStart + COLUMN_TILE_H4 - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;
    const int    tileEndClamped = min(tileEnd, dataH - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, dataH - 1);
    const int       columnStart = IMUL(blockIdx.x, COLUMN_TILE_W) + threadIdx.x;

    int smemPos = IMUL(threadIdx.y, COLUMN_TILE_W) + threadIdx.x;
    int gmemPos = IMUL(apronStart + threadIdx.y, width) + columnStart;
    for(int y = apronStart + threadIdx.y; y <= apronEnd; y += blockDim.y)
	{
        data[smemPos] = 
			((y >= apronStartClamped) && (y <= apronEndClamped)) ? 
				d_Data[gmemPos] : whiteShortVal;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
    __syncthreads();

    smemPos = IMUL(threadIdx.y + d_KernelRadius, COLUMN_TILE_W) + threadIdx.x;
    gmemPos = IMUL(tileStart + threadIdx.y , width) + columnStart;
    for(int y = tileStart + threadIdx.y; y <= tileEndClamped; y += blockDim.y)
	{
        ushort4 minV = data[smemPos + IMUL(-d_KernelRadius, COLUMN_TILE_W)];
		for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
		{
			if (data[smemPos + IMUL(k, COLUMN_TILE_W)].x < minV.x) 
				minV.x = data[smemPos + IMUL(k, COLUMN_TILE_W)].x;
			if (data[smemPos + IMUL(k, COLUMN_TILE_W)].y < minV.y) 
				minV.y = data[smemPos + IMUL(k, COLUMN_TILE_W)].y;
			if (data[smemPos + IMUL(k, COLUMN_TILE_W)].z < minV.z) 
				minV.z = data[smemPos + IMUL(k, COLUMN_TILE_W)].z;
			if (data[smemPos + IMUL(k, COLUMN_TILE_W)].w < minV.w) 
				minV.w = data[smemPos + IMUL(k, COLUMN_TILE_W)].w;
		}
		d_Result[gmemPos] = minV;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
}

////////////////////////////////////////////////////////////////////////////////
// MaxRow_8bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MaxRow_8bit(unsigned char *d_Result, unsigned char *d_Data, 
							int width, int dataH)
{
    __shared__ unsigned char data[MAX_KERNELSIZE-1 + ROW_TILE_W];

    const int         tileStart = IMUL(blockIdx.x, ROW_TILE_W);
    const int           tileEnd = tileStart + ROW_TILE_W - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;
    const int    tileEndClamped = min(tileEnd, width - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, width - 1);
    const int          rowStart = IMUL(blockIdx.y, width);
    const int apronStartAligned = tileStart - d_KernelRadiusAligned;
    const int			loadPos = apronStartAligned + threadIdx.x;

	if(loadPos >= apronStart)
	{
        const int smemPos = loadPos - apronStart;
		data[smemPos] = 
			((loadPos >= apronStartClamped) && (loadPos <= apronEndClamped)) ?
            d_Data[rowStart + loadPos] : 0;
    }
    __syncthreads();

    const int writePos = tileStart + threadIdx.x;
    if(writePos <= tileEndClamped)
	{
        const int smemPos = writePos - apronStart;
        unsigned char maxV = data[smemPos-d_KernelRadius];
        for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
			if (data[smemPos + k] > maxV) maxV = data[smemPos + k];
        d_Result[rowStart + writePos] = maxV;
    }
}


////////////////////////////////////////////////////////////////////////////////
// MaxColumn_8bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MaxColumn_8bit(unsigned char *d_Result, unsigned char *d_Data,
							   int width, int dataH, int smemStride, 
							   int gmemStride)
{
    __shared__ unsigned char data[COLUMN_TILE_W * (MAX_KERNELSIZE-1 +
																COLUMN_TILE_H)];

    const int         tileStart = IMUL(blockIdx.y, COLUMN_TILE_H);
    const int           tileEnd = tileStart + COLUMN_TILE_H - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;
    const int    tileEndClamped = min(tileEnd, dataH - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, dataH - 1);
    const int       columnStart = IMUL(blockIdx.x, COLUMN_TILE_W) + threadIdx.x;

	int smemPos = IMUL(threadIdx.y, COLUMN_TILE_W) + threadIdx.x;
    int gmemPos = IMUL(apronStart + threadIdx.y, width) + columnStart;
    for(int y = apronStart + threadIdx.y; y <= apronEnd; y += blockDim.y)
	{
        data[smemPos] = 
			((y >= apronStartClamped) && (y <= apronEndClamped)) ? 
			d_Data[gmemPos] : 0;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
    __syncthreads();

    smemPos = IMUL(threadIdx.y + d_KernelRadius, COLUMN_TILE_W) + threadIdx.x;
    gmemPos = IMUL(tileStart + threadIdx.y , width) + columnStart;
    for(int y = tileStart + threadIdx.y; y <= tileEndClamped; y += blockDim.y)
	{
        unsigned char maxV = data[smemPos + IMUL(-d_KernelRadius, COLUMN_TILE_W)];
		for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
			if (data[smemPos + IMUL(k, COLUMN_TILE_W)] > maxV) 
				maxV = data[smemPos + IMUL(k, COLUMN_TILE_W)];
		d_Result[gmemPos] = maxV;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
}

////////////////////////////////////////////////////////////////////////////////
// MaxRow_16bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MaxRow_16bit(unsigned short *d_Result, unsigned short *d_Data, 
							 int width, int dataH)
{
     __shared__ unsigned short data[MAX_KERNELSIZE-1 + ROW_TILE_W];

    const int         tileStart = IMUL(blockIdx.x, ROW_TILE_W);
    const int           tileEnd = tileStart + ROW_TILE_W - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;
    const int    tileEndClamped = min(tileEnd, width - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, width - 1);
    const int          rowStart = IMUL(blockIdx.y, width);
    const int apronStartAligned = tileStart - d_KernelRadiusAligned;
    const int			loadPos = apronStartAligned + threadIdx.x;

	if(loadPos >= apronStart)
	{
        const int smemPos = loadPos - apronStart;
		data[smemPos] = 
			((loadPos >= apronStartClamped) && (loadPos <= apronEndClamped)) ?
            d_Data[rowStart + loadPos] : 0;
    }
    __syncthreads();

    const int writePos = tileStart + threadIdx.x;
    if(writePos <= tileEndClamped)
	{
        const int smemPos = writePos - apronStart;
        unsigned short maxV = data[smemPos-d_KernelRadius];
        for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
			if (data[smemPos + k] > maxV) maxV = data[smemPos + k];
        d_Result[rowStart + writePos] = maxV;
    }
}


////////////////////////////////////////////////////////////////////////////////
// MaxColumn_16bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MaxColumn_16bit(unsigned short *d_Result, unsigned short *d_Data,
								int width, int dataH, int smemStride, 
								int gmemStride)
{
    __shared__ unsigned short data[COLUMN_TILE_W * (MAX_KERNELSIZE-1 + 
																COLUMN_TILE_H)];

    const int         tileStart = IMUL(blockIdx.y, COLUMN_TILE_H);
    const int           tileEnd = tileStart + COLUMN_TILE_H - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;
    const int    tileEndClamped = min(tileEnd, dataH - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, dataH - 1);
    const int       columnStart = IMUL(blockIdx.x, COLUMN_TILE_W) + threadIdx.x;

    int smemPos = IMUL(threadIdx.y, COLUMN_TILE_W) + threadIdx.x;
    int gmemPos = IMUL(apronStart + threadIdx.y, width) + columnStart;
    for(int y = apronStart + threadIdx.y; y <= apronEnd; y += blockDim.y)
	{
        data[smemPos] = 
			((y >= apronStartClamped) && (y <= apronEndClamped)) ? 
			d_Data[gmemPos] : 0;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
    __syncthreads();

    smemPos = IMUL(threadIdx.y + d_KernelRadius, COLUMN_TILE_W) + threadIdx.x;
    gmemPos = IMUL(tileStart + threadIdx.y , width) + columnStart;
    for(int y = tileStart + threadIdx.y; y <= tileEndClamped; y += blockDim.y)
	{
        unsigned short maxV = data[smemPos + IMUL(-d_KernelRadius, COLUMN_TILE_W)];
		for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
			if (data[smemPos + IMUL(k, COLUMN_TILE_W)] > maxV) 
				maxV = data[smemPos + IMUL(k, COLUMN_TILE_W)];
		d_Result[gmemPos] = maxV;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
}

////////////////////////////////////////////////////////////////////////////////
// MaxRow_24bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MaxRow_24bit(uchar4 *d_Result, uchar4 *d_Data, int width,
							 int dataH)
{
     __shared__ uchar4 data[MAX_KERNELSIZE-1 + ROW_TILE_W];

    const int         tileStart = IMUL(blockIdx.x, ROW_TILE_W);
    const int           tileEnd = tileStart + ROW_TILE_W - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;
    const int    tileEndClamped = min(tileEnd, width - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, width - 1);
    const int          rowStart = IMUL(blockIdx.y, width);
    const int apronStartAligned = tileStart - d_KernelRadiusAligned;
    const int			loadPos = apronStartAligned + threadIdx.x;

	if(loadPos >= apronStart)
	{
        const int smemPos = loadPos - apronStart;
		data[smemPos] = 
			((loadPos >= apronStartClamped) && (loadPos <= apronEndClamped)) ?
			d_Data[rowStart + loadPos] : blackVal;
    }

    const int writePos = tileStart + threadIdx.x;
    if(writePos <= tileEndClamped)
	{
        const int smemPos = writePos - apronStart;
        uchar4 maxV = data[smemPos-d_KernelRadius];
        for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
		{
			if (data[smemPos + k].x > maxV.x) maxV.x = data[smemPos + k].x;
			if (data[smemPos + k].y > maxV.y) maxV.y = data[smemPos + k].y;
			if (data[smemPos + k].z > maxV.z) maxV.z = data[smemPos + k].z;
			if (data[smemPos + k].w > maxV.w) maxV.w = data[smemPos + k].w;
		}
        d_Result[rowStart + writePos] = maxV;
    }
}


////////////////////////////////////////////////////////////////////////////////
// MaxColumn_24bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MaxColumn_24bit(uchar4 *d_Result, uchar4 *d_Data, int width, 
								int dataH, int smemStride, int gmemStride)
{
    __shared__ uchar4 data[COLUMN_TILE_W * (MAX_KERNELSIZE-1 + COLUMN_TILE_H4)];

    const int         tileStart = IMUL(blockIdx.y, COLUMN_TILE_H4);
    const int           tileEnd = tileStart + COLUMN_TILE_H4 - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;
    const int    tileEndClamped = min(tileEnd, dataH - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, dataH - 1);
    const int       columnStart = IMUL(blockIdx.x, COLUMN_TILE_W) + threadIdx.x;

    int smemPos = IMUL(threadIdx.y, COLUMN_TILE_W) + threadIdx.x;
    int gmemPos = IMUL(apronStart + threadIdx.y, width) + columnStart;
    for(int y = apronStart + threadIdx.y; y <= apronEnd; y += blockDim.y)
	{
        data[smemPos] = 
			((y >= apronStartClamped) && (y <= apronEndClamped)) ? 
			d_Data[gmemPos] : blackVal;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
    __syncthreads();

    smemPos = IMUL(threadIdx.y + d_KernelRadius, COLUMN_TILE_W) + threadIdx.x;
    gmemPos = IMUL(tileStart + threadIdx.y , width) + columnStart;
    for(int y = tileStart + threadIdx.y; y <= tileEndClamped; y += blockDim.y)
	{
        uchar4 maxV = data[smemPos + IMUL(-d_KernelRadius, COLUMN_TILE_W)];
		for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
		{
			if (data[smemPos  + IMUL(k, COLUMN_TILE_W)].x > maxV.x) 
				maxV.x = data[smemPos + IMUL(k, COLUMN_TILE_W)].x;
			if (data[smemPos  + IMUL(k, COLUMN_TILE_W)].y > maxV.y) 
				maxV.y = data[smemPos + IMUL(k, COLUMN_TILE_W)].y;
			if (data[smemPos  + IMUL(k, COLUMN_TILE_W)].z > maxV.z) 
				maxV.z = data[smemPos + IMUL(k, COLUMN_TILE_W)].z;
			if (data[smemPos  + IMUL(k, COLUMN_TILE_W)].w > maxV.w) 
				maxV.w = data[smemPos + IMUL(k, COLUMN_TILE_W)].w;
		}
		d_Result[gmemPos] = maxV;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
}

////////////////////////////////////////////////////////////////////////////////
// MaxRow_48bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MaxRow_48bit(ushort4 *d_Result, ushort4 *d_Data, int width,
							 int dataH)
{
     __shared__ ushort4 data[MAX_KERNELSIZE-1 + ROW_TILE_W];

    const int         tileStart = IMUL(blockIdx.x, ROW_TILE_W);
    const int           tileEnd = tileStart + ROW_TILE_W - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;
    const int    tileEndClamped = min(tileEnd, width - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, width - 1);
    const int          rowStart = IMUL(blockIdx.y, width);
    const int apronStartAligned = tileStart - d_KernelRadiusAligned;
    const int			loadPos = apronStartAligned + threadIdx.x;

	if(loadPos >= apronStart)
	{
        const int smemPos = loadPos - apronStart;
		data[smemPos] = 
			((loadPos >= apronStartClamped) && (loadPos <= apronEndClamped)) ?
			d_Data[rowStart + loadPos] : blackShortVal;
    }

    const int writePos = tileStart + threadIdx.x;
    if(writePos <= tileEndClamped)
	{
        const int smemPos = writePos - apronStart;
        ushort4 maxV = data[smemPos-d_KernelRadius];
        for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
		{
			if (data[smemPos + k].x > maxV.x) maxV.x = data[smemPos + k].x;
			if (data[smemPos + k].y > maxV.y) maxV.y = data[smemPos + k].y;
			if (data[smemPos + k].z > maxV.z) maxV.z = data[smemPos + k].z;
			if (data[smemPos + k].w > maxV.w) maxV.w = data[smemPos + k].w;
		}
        d_Result[rowStart + writePos] = maxV;
    }
}


////////////////////////////////////////////////////////////////////////////////
// MaxColumn_48bit
////////////////////////////////////////////////////////////////////////////////
__global__ void MaxColumn_48bit(ushort4 *d_Result, ushort4 *d_Data, int width, 
								int dataH, int smemStride, int gmemStride)
{
    __shared__ ushort4 data[COLUMN_TILE_W * (MAX_KERNELSIZE-1 + COLUMN_TILE_H4)];

    const int         tileStart = IMUL(blockIdx.y, COLUMN_TILE_H4);
    const int           tileEnd = tileStart + COLUMN_TILE_H4 - 1;
    const int        apronStart = tileStart - d_KernelRadius;
    const int          apronEnd = tileEnd   + d_KernelRadius;
    const int    tileEndClamped = min(tileEnd, dataH - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, dataH - 1);
    const int       columnStart = IMUL(blockIdx.x, COLUMN_TILE_W) + threadIdx.x;

    int smemPos = IMUL(threadIdx.y, COLUMN_TILE_W) + threadIdx.x;
    int gmemPos = IMUL(apronStart + threadIdx.y, width) + columnStart;
    for(int y = apronStart + threadIdx.y; y <= apronEnd; y += blockDim.y)
	{
        data[smemPos] = 
			((y >= apronStartClamped) && (y <= apronEndClamped)) ? 
			d_Data[gmemPos] : blackShortVal;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
    __syncthreads();

    smemPos = IMUL(threadIdx.y + d_KernelRadius, COLUMN_TILE_W) + threadIdx.x;
    gmemPos = IMUL(tileStart + threadIdx.y , width) + columnStart;
    for(int y = tileStart + threadIdx.y; y <= tileEndClamped; y += blockDim.y)
	{
        ushort4 maxV = data[smemPos + IMUL(-d_KernelRadius, COLUMN_TILE_W)];
		for(int k = -d_KernelRadius+1; k <= d_KernelRadius; k++)
		{
			if (data[smemPos  + IMUL(k, COLUMN_TILE_W)].x > maxV.x) 
				maxV.x = data[smemPos + IMUL(k, COLUMN_TILE_W)].x;
			if (data[smemPos  + IMUL(k, COLUMN_TILE_W)].y > maxV.y) 
				maxV.y = data[smemPos + IMUL(k, COLUMN_TILE_W)].y;
			if (data[smemPos  + IMUL(k, COLUMN_TILE_W)].z > maxV.z) 
				maxV.z = data[smemPos + IMUL(k, COLUMN_TILE_W)].z;
			if (data[smemPos  + IMUL(k, COLUMN_TILE_W)].w > maxV.w) 
				maxV.w = data[smemPos + IMUL(k, COLUMN_TILE_W)].w;
		}
		d_Result[gmemPos] = maxV;
        smemPos += smemStride;
        gmemPos += gmemStride;
    }
}
#else

///////////////////////////////////////////////////////////////////////////////////////
// MinCDevice_8bit
//	- Min (cross) operation 8 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void  MinCDevice_8bit(unsigned char *pDestOrig, unsigned int Pitch, 
								int width, int height)
{ 
	unsigned char minV, pix;
    unsigned char *pDest = 
      (unsigned char *) (((char *) pDestOrig)+blockIdx.x*Pitch);
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		minV=tex2D(texUC, (float)i, (float)blockIdx.x);
        pix = tex2D( texUC, (float) i+0, (float) blockIdx.x-1 );
 		if (pix < minV) minV=pix;
        pix = tex2D( texUC, (float) i-1, (float) blockIdx.x+0 );
		if (pix < minV) minV=pix;
        pix = tex2D( texUC, (float) i+1, (float) blockIdx.x+0 );
		if (pix < minV) minV=pix;
        pix = tex2D( texUC, (float) i+0, (float) blockIdx.x+1 );
		if (pix < minV) minV=pix;
		pDest[i]=minV;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// MinDDevice_8bit
//	- Min (diagonal) operation 8 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void  MinDDevice_8bit(unsigned char *pDestOrig, unsigned int Pitch, 
								int width, int height)
{ 
	unsigned char minV, pix;
    unsigned char *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		minV=tex2D(texUC, (float)i, (float)blockIdx.x);
        pix = tex2D( texUC, (float) i-1, (float) blockIdx.x-1 );
		if (pix < minV) minV=pix;
		pix = tex2D( texUC, (float) i+1, (float) blockIdx.x-1 );
		if (pix < minV) minV=pix;
        pix = tex2D( texUC, (float) i-1, (float) blockIdx.x+1 );
		if (pix < minV) minV=pix;
        pix = tex2D( texUC, (float) i+1, (float) blockIdx.x+1 );
		if (pix < minV) minV=pix;
		pDest[i]=minV;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// MinCDevice_16bit
//	- Min (cross) operation 16 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MinCDevice_16bit(unsigned short *pDestOrig, unsigned int Pitch, 
								   int width, int height)
{ 
 	unsigned short minV, pix;
	unsigned short *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x )
	{
		minV=tex2D(texUS, (float)i, (float)blockIdx.x);
        pix = tex2D( texUS, (float) i+0, (float) blockIdx.x-1 );
 		if (pix < minV) minV=pix;
        pix = tex2D( texUS, (float) i-1, (float) blockIdx.x+0 );
		if (pix < minV) minV=pix;
        pix = tex2D( texUS, (float) i+1, (float) blockIdx.x+0 );
		if (pix < minV) minV=pix;
        pix = tex2D( texUS, (float) i+0, (float) blockIdx.x+1 );
		if (pix < minV) minV=pix;
		pDest[i]=minV;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// MinDDevice_16bit
//	- Min (diagonal) operation 16 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MinDDevice_16bit(unsigned short *pDestOrig, unsigned int Pitch, 
								   int width, int height)
{ 
 	unsigned short minV, pix;
	unsigned short *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x )
	{
		minV=tex2D(texUS, (float)i, (float)blockIdx.x);
        pix = tex2D( texUS, (float) i-1, (float) blockIdx.x-1 );
		if (pix < minV) minV=pix;
		pix = tex2D( texUS, (float) i+1, (float) blockIdx.x-1 );
		if (pix < minV) minV=pix;
        pix = tex2D( texUS, (float) i-1, (float) blockIdx.x+1 );
		if (pix < minV) minV=pix;
        pix = tex2D( texUS, (float) i+1, (float) blockIdx.x+1 );
		if (pix < minV) minV=pix;
		pDest[i]=minV;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// MinCDevice_24bit
//	- Min (cross) operation 24 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MinCDevice_24bit(uchar4 *pDestOrig, unsigned int Pitch, 
								  int width, int height)
{ 
	uchar4 pix, minV;
	uchar4 *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		minV=tex2D(tex4UC, (float)i, (float)blockIdx.x);
        pix = tex2D(tex4UC, (float) i+0, (float) blockIdx.x-1 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
        pix = tex2D(tex4UC, (float) i-1, (float) blockIdx.x+0 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
        pix = tex2D(tex4UC, (float) i+1, (float) blockIdx.x+0 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
        pix = tex2D(tex4UC, (float) i+0, (float) blockIdx.x+1 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
		pDest[i]=minV;
    }
}


///////////////////////////////////////////////////////////////////////////////////////
// MinDDevice_24bit
//	- Min (diagonal) operation 24 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MinDDevice_24bit(uchar4 *pDestOrig, unsigned int Pitch, 
								  int width, int height)
{ 
	uchar4 pix, minV;
	uchar4 *pDest = pDestOrig+blockIdx.x*Pitch;

    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		minV=tex2D(tex4UC, (float)i, (float)blockIdx.x);
        pix = tex2D(tex4UC, (float) i-1, (float) blockIdx.x-1 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
		pix = tex2D(tex4UC, (float) i+1, (float) blockIdx.x-1 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
        pix = tex2D(tex4UC, (float) i-1, (float) blockIdx.x+1 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
        pix = tex2D(tex4UC, (float) i+1, (float) blockIdx.x+1 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
		pDest[i]=minV;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// MinCDevice_48bit
//	- Min (cross) operation 48 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MinCDevice_48bit(ushort4 *pDestOrig, unsigned int Pitch, 
								  int width, int height)
{ 
	ushort4 pix, minV;
	ushort4 *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		minV=tex2D(tex4US, (float)i, (float)blockIdx.x);
        pix = tex2D(tex4US, (float) i+0, (float) blockIdx.x-1 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
        pix = tex2D(tex4US, (float) i-1, (float) blockIdx.x+0 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
        pix = tex2D(tex4US, (float) i+1, (float) blockIdx.x+0 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
        pix = tex2D(tex4US, (float) i+0, (float) blockIdx.x+1 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
		pDest[i]=minV;
    }
}


///////////////////////////////////////////////////////////////////////////////////////
// MinDDevice_48bit
//	- Min (diagonal) operation 48 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MinDDevice_48bit(ushort4 *pDestOrig, unsigned int Pitch, 
								  int width, int height)
{ 
	ushort4 pix, minV;
	ushort4 *pDest = pDestOrig+blockIdx.x*Pitch;

    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		minV=tex2D(tex4US, (float)i, (float)blockIdx.x);
        pix = tex2D(tex4US, (float) i-1, (float) blockIdx.x-1 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
		pix = tex2D(tex4US, (float) i+1, (float) blockIdx.x-1 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
        pix = tex2D(tex4US, (float) i-1, (float) blockIdx.x+1 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
        pix = tex2D(tex4US, (float) i+1, (float) blockIdx.x+1 );
		if (pix.x < minV.x) minV.x=pix.x;
		if (pix.y < minV.y) minV.y=pix.y;
		if (pix.z < minV.z) minV.z=pix.z;
		if (pix.w < minV.w) minV.w=pix.w;
		pDest[i]=minV;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// MaxCDevice_8bit
//	- Max (cross) operation 8 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void  MaxCDevice_8bit(unsigned char *pDestOrig, unsigned int Pitch, 
								int width, int height)
{ 
	unsigned char maxV, pix;
    unsigned char *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		maxV=tex2D(texUC, (float)i, (float)blockIdx.x);
        pix = tex2D( texUC, (float) i+0, (float) blockIdx.x-1 );
 		if (pix > maxV) maxV=pix;
        pix = tex2D( texUC, (float) i-1, (float) blockIdx.x+0 );
		if (pix > maxV) maxV=pix;
        pix = tex2D( texUC, (float) i+1, (float) blockIdx.x+0 );
		if (pix > maxV) maxV=pix;
        pix = tex2D( texUC, (float) i+0, (float) blockIdx.x+1 );
		if (pix > maxV) maxV=pix;
		pDest[i]=maxV;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// MaxDDevice_8bit
//	- Max (diagonal) operation 8 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void  MaxDDevice_8bit(unsigned char *pDestOrig, unsigned int Pitch, 
								int width, int height)
{ 
	unsigned char maxV, pix;
    unsigned char *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		maxV=tex2D(texUC, (float)i, (float)blockIdx.x);
        pix = tex2D( texUC, (float) i-1, (float) blockIdx.x-1 );
		if (pix > maxV) maxV=pix;
		pix = tex2D( texUC, (float) i+1, (float) blockIdx.x-1 );
		if (pix > maxV) maxV=pix;
        pix = tex2D( texUC, (float) i-1, (float) blockIdx.x+1 );
		if (pix > maxV) maxV=pix;
        pix = tex2D( texUC, (float) i+1, (float) blockIdx.x+1 );
		if (pix > maxV) maxV=pix;
		pDest[i]=maxV;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// MaxCDevice_16bit
//	- Max (cross) operation 16 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MaxCDevice_16bit(unsigned short *pDestOrig, unsigned int Pitch, 
								   int width, int height)
{ 
 	unsigned short maxV, pix;
	unsigned short *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x )
	{
		maxV=tex2D(texUS, (float)i, (float)blockIdx.x);
        pix = tex2D( texUS, (float) i+0, (float) blockIdx.x-1 );
 		if (pix > maxV) maxV=pix;
        pix = tex2D( texUS, (float) i-1, (float) blockIdx.x+0 );
		if (pix > maxV) maxV=pix;
        pix = tex2D( texUS, (float) i+1, (float) blockIdx.x+0 );
		if (pix > maxV) maxV=pix;
        pix = tex2D( texUS, (float) i+0, (float) blockIdx.x+1 );
		if (pix > maxV) maxV=pix;
		pDest[i]=maxV;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// MaxDDevice_16bit
//	- Max (diagonal) operation 16 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MaxDDevice_16bit(unsigned short *pDestOrig, unsigned int Pitch, 
								   int width, int height)
{ 
 	unsigned short maxV, pix;
	unsigned short *pDest = pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x )
	{
		maxV=tex2D(texUS, (float)i, (float)blockIdx.x);
        pix = tex2D( texUS, (float) i-1, (float) blockIdx.x-1 );
		if (pix > maxV) maxV=pix;
		pix = tex2D( texUS, (float) i+1, (float) blockIdx.x-1 );
		if (pix > maxV) maxV=pix;
        pix = tex2D( texUS, (float) i-1, (float) blockIdx.x+1 );
		if (pix > maxV) maxV=pix;
        pix = tex2D( texUS, (float) i+1, (float) blockIdx.x+1 );
		if (pix > maxV) maxV=pix;
		pDest[i]=maxV;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// MaxCDevice_24bit
//	- Max (cross) operation 24 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MaxCDevice_24bit(uchar4 *pDestOrig, unsigned int Pitch, 
								  int width, int height)
{ 
  	uchar4 maxV, pix;
	uchar4 *pDest =  pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		maxV=tex2D(tex4UC, (float)i, (float)blockIdx.x);
        pix = tex2D(tex4UC, (float) i+0, (float) blockIdx.x-1 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
        pix = tex2D(tex4UC, (float) i-1, (float) blockIdx.x+0 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
        pix = tex2D(tex4UC, (float) i+1, (float) blockIdx.x+0 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
        pix = tex2D(tex4UC, (float) i+0, (float) blockIdx.x+1 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
		pDest[i]=maxV;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// MaxDDevice_24bit
//	- Max (diagonal) operation 24 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MaxDDevice_24bit(uchar4 *pDestOrig, unsigned int Pitch, 
								  int width, int height)
{ 
  	uchar4 maxV, pix;
	uchar4 *pDest =  pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		maxV=tex2D(tex4UC, (float)i, (float)blockIdx.x);
        pix = tex2D(tex4UC, (float) i-1, (float) blockIdx.x-1 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
		pix = tex2D(tex4UC, (float) i+1, (float) blockIdx.x-1 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
        pix = tex2D(tex4UC, (float) i-1, (float) blockIdx.x+1 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
        pix = tex2D(tex4UC, (float) i+1, (float) blockIdx.x+1 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
		pDest[i]=maxV;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// MaxCDevice_48bit
//	- Max (cross) operation 48 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MaxCDevice_48bit(ushort4 *pDestOrig, unsigned int Pitch, 
								  int width, int height)
{ 
  	ushort4 maxV, pix;
	ushort4 *pDest =  pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		maxV=tex2D(tex4US, (float)i, (float)blockIdx.x);
        pix = tex2D(tex4US, (float) i+0, (float) blockIdx.x-1 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
        pix = tex2D(tex4US, (float) i-1, (float) blockIdx.x+0 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
        pix = tex2D(tex4US, (float) i+1, (float) blockIdx.x+0 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
        pix = tex2D(tex4US, (float) i+0, (float) blockIdx.x+1 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
		pDest[i]=maxV;
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// MaxDDevice_48bit
//	- Max (diagonal) operation 24 bit based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void MaxDDevice_48bit(ushort4 *pDestOrig, unsigned int Pitch, 
								  int width, int height)
{ 
  	ushort4 maxV, pix;
	ushort4 *pDest =  pDestOrig+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
		maxV=tex2D(tex4US, (float)i, (float)blockIdx.x);
        pix = tex2D(tex4US, (float) i-1, (float) blockIdx.x-1 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
		pix = tex2D(tex4US, (float) i+1, (float) blockIdx.x-1 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
        pix = tex2D(tex4US, (float) i-1, (float) blockIdx.x+1 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
        pix = tex2D(tex4US, (float) i+1, (float) blockIdx.x+1 );
		if (pix.x > maxV.x) maxV.x=pix.x;
		if (pix.y > maxV.y) maxV.y=pix.y;
		if (pix.z > maxV.z) maxV.z=pix.z;
		if (pix.w > maxV.w) maxV.w=pix.w;
		pDest[i]=maxV;
    }
}
#endif
/*
///////////////////////////////////////////////////////////////////////////////////////
// CudaErode
//	- Cuda Min filter (pingpong small squared kernel in shared memory) 
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaErode(void *src, void *dest, void *tmpIma, int width, 
								int height, int bpp, int nPasses)
{
	void	*src0;
	dim3	blockRows, blockColumns, threadRows, threadColumns;
	int		i, dataW, FilterHW=1;

	dataW			= iAlignUp(width, 16);
	blockRows.x		= iDivUp(dataW, ROW_TILE_W);
	blockRows.y		= height;
	blockColumns.x	= iDivUp(width, COLUMN_TILE_W); 
	threadRows.x	= iAlignUp(FilterHW,16) + ROW_TILE_W + FilterHW;
	threadColumns.x = COLUMN_TILE_W;
	threadColumns.y = 8;
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_KernelRadius", &FilterHW, sizeof(int)));
	int FilterHWAligned=iAlignUp(FilterHW,16);
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_KernelRadiusAligned", &FilterHWAligned, 
									  sizeof(int)));
	switch (bpp)
	{
		case 8:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H);
				src0=src;
				for (i=0; i<nPasses; i++)
				{
					MinRow_8bit<<<blockRows, threadRows>>>((unsigned char *)tmpIma, 
										(unsigned char *)src0, width, height);
					MinColumn_8bit<<<blockColumns, threadColumns>>>((unsigned char *)dest,
										(unsigned char *)tmpIma, width, height, 
										COLUMN_TILE_W * threadColumns.y, 
										width * threadColumns.y);
					src0=dest;
				}
				break;
		case 16:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H);
				src0=src;
				for (i=0; i<nPasses; i++)
				{
					MinRow_16bit<<<blockRows, threadRows>>>((unsigned short *)tmpIma, 
										(unsigned short *)src0, width, height);
					MinColumn_16bit<<<blockColumns, threadColumns>>>((unsigned short *)dest,
										(unsigned short *)tmpIma, width, height, 
										COLUMN_TILE_W * threadColumns.y, 
										width * threadColumns.y);
					src0=dest;
				}
				break;
		case 24:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H4);
				src0=src;
				for (i=0; i<nPasses; i++)
				{
					MinRow_24bit<<<blockRows, threadRows>>>((uchar4 *)tmpIma, 
										(uchar4 *)src0, width, height);
					MinColumn_24bit<<<blockColumns, threadColumns>>>((uchar4 *)dest,
										(uchar4 *)tmpIma, width, height, 
										COLUMN_TILE_W * threadColumns.y, 
										width * threadColumns.y);
					src0=dest;
				}
				break;
		case 48:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H4);
				src0=src;
				for (i=0; i<nPasses; i++)
				{
					MinRow_48bit<<<blockRows, threadRows>>>((ushort4 *)tmpIma, 
										(ushort4 *)src0, width, height);
					MinColumn_48bit<<<blockColumns, threadColumns>>>((ushort4 *)dest,
										(ushort4 *)tmpIma, width, height, 
										COLUMN_TILE_W * threadColumns.y, 
										width * threadColumns.y);
					src0=dest;
				}
				break;
	}
}
*/

#ifdef MINMAX_SHARED
///////////////////////////////////////////////////////////////////////////////////////
// CudaErode
//	- Cuda Min filter (large square kernel in shared memory) 
///////////////////////////////////////////////////////////////////////////////////////
extern "C" void CudaErode(void *src, void *dest, void *tmpIma, int width, 
								int height, int bpp, int filterHW)
{
	dim3	blockRows, blockColumns, threadRows, threadColumns;
	int		dataW;

	dataW			= iAlignUp(width, 16);
	blockRows.x		= iDivUp(dataW, ROW_TILE_W);
	blockRows.y		= height;
	blockColumns.x	= iDivUp(width, COLUMN_TILE_W); 
	threadRows.x	= iAlignUp(filterHW,16) + ROW_TILE_W + filterHW;
	threadColumns.x = COLUMN_TILE_W;
	threadColumns.y = 8;
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_KernelRadius", &filterHW, sizeof(int)));
	int filterHWAligned=iAlignUp(filterHW,16);
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_KernelRadiusAligned", &filterHWAligned, 
									  sizeof(int)));
	switch (bpp)
	{
		case 8:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H);
				MinRow_8bit<<<blockRows, threadRows>>>((unsigned char *)tmpIma, 
									(unsigned char *)src, width, height);
				MinColumn_8bit<<<blockColumns, threadColumns>>>((unsigned char *)dest,
									(unsigned char *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
				break;
		case 16:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H);
				MinRow_16bit<<<blockRows, threadRows>>>((unsigned short *)tmpIma, 
									(unsigned short *)src, width, height);
				MinColumn_16bit<<<blockColumns, threadColumns>>>((unsigned short *)dest,
									(unsigned short *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
				break;
		case 24:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H4);
				MinRow_24bit<<<blockRows, threadRows>>>((uchar4 *)tmpIma, 
									(uchar4 *)src, width, height);
				MinColumn_24bit<<<blockColumns, threadColumns>>>((uchar4 *)dest,
									(uchar4 *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
				break;
		case 48:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H4);
				MinRow_48bit<<<blockRows, threadRows>>>((ushort4 *)tmpIma, 
									(ushort4 *)src, width, height);
				MinColumn_48bit<<<blockColumns, threadColumns>>>((ushort4 *)dest,
									(ushort4 *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
				break;
	}
}
#else

///////////////////////////////////////////////////////////////////////////////////////
// CudaErode
//	- Cuda Min filter (pingpong cross + diagonal in device memory)
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaErode(cudaArray *src, void *dest, cudaArray *tmpIma, int width, 
								int height, int bpp, int filterHW)
{
	int			size;
	cudaArray	*src0;

	if (bpp==24)
		size=width*height*4;
	else if (bpp==48)
		size=width*height*8;
	else
		size=width*height*bpp/8;
	switch (bpp)
	{
		case 8:
				src0=src;
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC, src0));
				for (int i=0; i<filterHW; i++)
				{
					if (i&1)
						MinDDevice_8bit<<<height, 384>>>((unsigned char *)dest, width, 
													    width, height);
					else
						MinCDevice_8bit<<<height, 384>>>((unsigned char *)dest, width, 
													    width, height);
					if (i != (filterHW-1))
					{
						if (i==0)
						{
							CUDA_SAFE_CALL(cudaUnbindTexture(texUC));
							src0=tmpIma;
							CUDA_SAFE_CALL(cudaBindTextureToArray(texUC, src0));
						}
						CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)src0, 0, 0, 
										dest, size, cudaMemcpyDeviceToDevice));
					}
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC));
				break;
		case 16:
				src0=src;
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS, src0));
				for (int i=0; i<filterHW; i++)
				{
					if (i&1)
						MinDDevice_16bit<<<height, 384>>>((unsigned short *)dest, width, 
														 width, height);
					else
						MinCDevice_16bit<<<height, 384>>>((unsigned short *)dest, width, 
														 width, height);
					if (i != (filterHW-1))
					{
						if (i==0)
						{
							CUDA_SAFE_CALL(cudaUnbindTexture(texUS));
							src0=tmpIma;
							CUDA_SAFE_CALL(cudaBindTextureToArray(texUS, src0));
						}
						CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)src0, 0, 0, 
										dest, size, cudaMemcpyDeviceToDevice));
					}
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS));
				break;
		case 24:
				src0=src;
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC, src0));
				for (int i=0; i<filterHW; i++)
				{
					if (i&1)
						MinDDevice_24bit<<< height, 32>>>((uchar4 *)dest, width, width, 
														  height);
					else
						MinCDevice_24bit<<< height, 32>>>((uchar4 *)dest, width, width, 
														  height);
					if (i != (filterHW-1))
					{
						if (i==0)
						{
							CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC));
							src0=tmpIma;
							CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC, src0));
						}
						CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)src0, 0, 0, 
										dest, size, cudaMemcpyDeviceToDevice));
					}
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC));
				break;
		case 48:
				src0=src;
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US, src0));
				for (int i=0; i<filterHW; i++)
				{
					if (i&1)
						MinDDevice_48bit<<< height, 32>>>((ushort4 *)dest, width, width, 
														  height);
					else
						MinCDevice_48bit<<< height, 32>>>((ushort4 *)dest, width, width, 
														  height);
					if (i != (filterHW-1))
					{
						if (i==0)
						{
							CUDA_SAFE_CALL(cudaUnbindTexture(tex4US));
							src0=tmpIma;
							CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US, src0));
						}
						CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)src0, 0, 0, 
										dest, size, cudaMemcpyDeviceToDevice));
					}
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US));
				break;
	}
}
#endif

#ifdef MINMAX_SHARED

///////////////////////////////////////////////////////////////////////////////////////
// CudaDilate
//	- Cuda Max filter (large square kernel in shared memory) 
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaDilate(void *src, void *dest, void *tmpIma, int width, 
								int height, int bpp, int filterHW)
{
	dim3	blockRows, blockColumns, threadRows, threadColumns;
	int		dataW;

	dataW			= iAlignUp(width, 16);
	blockRows.x		= iDivUp(dataW, ROW_TILE_W);
	blockRows.y		= height;
	blockColumns.x	= iDivUp(width, COLUMN_TILE_W); 
	threadRows.x	= iAlignUp(filterHW,16) + ROW_TILE_W + filterHW;
	threadColumns.x = COLUMN_TILE_W;
	threadColumns.y = 8;
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_KernelRadius", &filterHW, sizeof(int)));
	int filterHWAligned=iAlignUp(filterHW,16);
	CUDA_SAFE_CALL(cudaMemcpyToSymbol("d_KernelRadiusAligned", &filterHWAligned, 
									  sizeof(int)));
	switch (bpp)
	{
		case 8:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H);
				MaxRow_8bit<<<blockRows, threadRows>>>((unsigned char *)tmpIma, 
									(unsigned char *)src, width, height);
				MaxColumn_8bit<<<blockColumns, threadColumns>>>((unsigned char *)dest,
									(unsigned char *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
				break;
		case 16:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H);
				MaxRow_16bit<<<blockRows, threadRows>>>((unsigned short *)tmpIma, 
									(unsigned short *)src, width, height);
				MaxColumn_16bit<<<blockColumns, threadColumns>>>((unsigned short *)dest,
									(unsigned short *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
				break;
		case 24:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H4);
				MaxRow_24bit<<<blockRows, threadRows>>>((uchar4 *)tmpIma, 
									(uchar4 *)src, width, height);
				MaxColumn_24bit<<<blockColumns, threadColumns>>>((uchar4 *)dest,
									(uchar4 *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
				break;
		case 48:
				blockColumns.y = iDivUp(height, COLUMN_TILE_H4);
				MaxRow_48bit<<<blockRows, threadRows>>>((ushort4 *)tmpIma, 
									(ushort4 *)src, width, height);
				MaxColumn_48bit<<<blockColumns, threadColumns>>>((ushort4 *)dest,
									(ushort4 *)tmpIma, width, height, 
									COLUMN_TILE_W * threadColumns.y, 
									width * threadColumns.y);
				break;
	}
}

#else

///////////////////////////////////////////////////////////////////////////////////////
// CudaDilate
//	- Cuda Min filter (pingpong "cross" + "diagonal" in device memory)
///////////////////////////////////////////////////////////////////////////////////////

extern "C" void CudaDilate(cudaArray *src, void *dest, cudaArray *tmpIma, int width, 
								int height, int bpp, int filterHW)
{
	int size;
	cudaArray *src0;
	if (bpp==24)
		size=width*height*4;
	else if (bpp==48)
		size=width*height*8;
	else
		size=width*height*bpp/8;
	switch (bpp)
	{
		case 8:
				src0=src;
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC, src0));
				for (int i=0; i<filterHW; i++)
				{
					if (i&1)
						MaxDDevice_8bit<<<height, 384>>>((unsigned char *)dest, width, 
													    width, height);
					else
						MaxCDevice_8bit<<<height, 384>>>((unsigned char *)dest, width, 
													    width, height);
					if (i != (filterHW-1))
					{
						if (i==0)
						{
							CUDA_SAFE_CALL(cudaUnbindTexture(texUC));
							src0=tmpIma;
							CUDA_SAFE_CALL(cudaBindTextureToArray(texUC, src0));
						}
						CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)src0, 0, 0, 
										dest, size, cudaMemcpyDeviceToDevice));
					}
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(texUC));
				break;
		case 16:
				src0=src;
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS, src0));
				for (int i=0; i<filterHW; i++)
				{
					if (i&1)
						MaxDDevice_16bit<<<height, 384>>>((unsigned short *)dest, width, 
														 width, height);
					else
						MaxCDevice_16bit<<<height, 384>>>((unsigned short *)dest, width, 
														 width, height);
					if (i != (filterHW-1))
					{
						if (i==0)
						{
							CUDA_SAFE_CALL(cudaUnbindTexture(texUS));
							src0=tmpIma;
							CUDA_SAFE_CALL(cudaBindTextureToArray(texUS, src0));
						}
						CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)src0, 0, 0, 
										dest, size, cudaMemcpyDeviceToDevice));
					}
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS));
				break;
		case 24:
				src0=src;
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC, src0));
				for (int i=0; i<filterHW; i++)
				{
					if (i&1)
						MaxDDevice_24bit<<<height, 384>>>((uchar4 *)dest, width, width, 
														 height);
					else
						MaxCDevice_24bit<<<height, 384>>>((uchar4 *)dest, width, width, 
														 height);
					if (i != (filterHW-1))
					{
						if (i==0)
						{
							CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC));
							src0=tmpIma;
							CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC, src0));
						}
						CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)src0, 0, 0, 
										dest, size, cudaMemcpyDeviceToDevice));
					}
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC));
				break;
		case 48:
				src0=src;
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US, src0));
				for (int i=0; i<filterHW; i++)
				{
					if (i&1)
						MaxDDevice_48bit<<<height, 32>>>((ushort4 *)dest, width, width, 
														 height);
					else
						MaxCDevice_48bit<<<height, 32>>>((ushort4 *)dest, width, width, 
														 height);
					if (i != (filterHW-1))
					{
						if (i==0)
						{
							CUDA_SAFE_CALL(cudaUnbindTexture(tex4US));
							src0=tmpIma;
							CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US, src0));
						}
						CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)src0, 0, 0, 
										dest, size, cudaMemcpyDeviceToDevice));
					}
				}
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US));
				break;
	}
}
#endif

