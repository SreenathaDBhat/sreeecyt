
#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cutil.h>
#include "cudamath.h"
#include "CudaTIP_Kernels.h"

texture<unsigned char, 2> texUC;
texture<uchar4, 2> tex4UC;
texture<unsigned short, 2> texUS;
texture<ushort4, 2> tex4US;

///////////////////////////////////////////////////////////////////////////////////////
// Sobel_8bit
//	- calculate byte pixel after Sobel filter
///////////////////////////////////////////////////////////////////////////////////////

__device__ unsigned char Sobel_8bit(unsigned char ul, // upper left
									unsigned char um, // upper middle
									unsigned char ur, // upper right
									unsigned char ml, // middle left
									unsigned char mr, // middle right
									unsigned char ll, // lower left
									unsigned char lm, // lower middle
									unsigned char lr, // lower right
									float fScale )
{
    short Horz = ur + 2*mr + lr - ul - 2*ml - ll;
    short Vert = ul + 2*um + ur - ll - 2*lm - lr;
    short Sum = (short) (fScale*(abs(Horz)+abs(Vert)));
    if ( Sum < 0 ) return 0; else if ( Sum > 0xff ) return 0xff;
    return (unsigned char) Sum;
}

///////////////////////////////////////////////////////////////////////////////////////
// Sobel_16bit
//	- calculate short pixel after Sobel filter
///////////////////////////////////////////////////////////////////////////////////////

__device__ unsigned short Sobel_16bit(unsigned short ul, // upper left
									  unsigned short um, // upper middle
									  unsigned short ur, // upper right
									  unsigned short ml, // middle left
									  unsigned short mr, // middle right
									  unsigned short ll, // lower left
									  unsigned short lm, // lower middle
									  unsigned short lr, // lower right
									  float fScale )
{
    float Horz = ur + 2*mr + lr - ul - 2*ml - ll;
    float Vert = ul + 2*um + ur - ll - 2*lm - lr;
    float Sum = (fScale*(abs(Horz)+abs(Vert)));
    if ( Sum < 0 ) return 0; else if ( Sum > 65535 ) return 0xffff;
    return (unsigned short) Sum;
}

///////////////////////////////////////////////////////////////////////////////////////
// Sobel_24bit
//	- calculate colour pixel after Sobel filter
///////////////////////////////////////////////////////////////////////////////////////

__device__ uchar4 Sobel_24bit(short4 ul, // upper left
							  short4 um, // upper middle
							  short4 ur, // upper right
							  short4 ml, // middle left
							  short4 mr, // middle right
							  short4 ll, // lower left
							  short4 lm, // lower middle
							  short4 lr, // lower right
							  float fScale )
{
    short4 Horz = ur + 2*mr + lr - ul - 2*ml - ll;
    short4 Vert = ul + 2*um + ur - ll - 2*lm - lr;
	short4 Sum = fScale*(abs(Horz)+abs(Vert));
	return _uchar4(clamp(Sum, 0, 255));
}

///////////////////////////////////////////////////////////////////////////////////////
// Sobel_48bit
//	- calculate colour pixel after Sobel filter
///////////////////////////////////////////////////////////////////////////////////////

__device__ ushort4 Sobel_48bit(ushort4 ul, // upper left
							  ushort4 um, // upper middle
							  ushort4 ur, // upper right
							  ushort4 ml, // middle left
							  ushort4 mr, // middle right
							  ushort4 ll, // lower left
							  ushort4 lm, // lower middle
							  ushort4 lr, // lower right
							  float fScale )
{
    float4 Horz = _float4(ur + lr) - _float4(ul + ll) + 2*_float4(mr) - 2*_float4(ml);
    float4 Vert = _float4(ul + ur) - _float4(ll + lr) + 2*_float4(um) - 2*_float4(lm);
	float4 Sum = fScale*(abs(Horz)+abs(Vert));
	return _ushort4(clamp(Sum,0, 65535.));
}

///////////////////////////////////////////////////////////////////////////////////////
// SobelShared_8bit
//	- Sobel filter based on shared memory (byte pixels)
///////////////////////////////////////////////////////////////////////////////////////

#ifdef SOBEL_SHARED
extern __shared__ unsigned char		SharedBlk_8bit[];
extern __shared__ unsigned short	SharedBlk_16bit[];
extern __shared__ uchar4			SharedBlk_24bit[];
extern __shared__ ushort4			SharedBlk_48bit[];

__global__ void SobelShared_8bit( uchar4 *pSobelOriginal, unsigned short SobelPitch, 
								  short BlockWidth, short SharedPitch,
								  short w, short h, float fScale )
{ 
    short u = 4*blockIdx.x*BlockWidth;
    short v = blockIdx.y*blockDim.y + threadIdx.y;
    short ib;

    int idx = threadIdx.y * SharedPitch;

    for ( ib = threadIdx.x; ib < BlockWidth+2*SOBEL_RADIUS; ib += blockDim.x ) 
	{
        SharedBlk_8bit[idx+4*ib+0] = tex2D( texUC, (float) (u+4*ib-SOBEL_RADIUS+0), 
											(float) (v-SOBEL_RADIUS));
        SharedBlk_8bit[idx+4*ib+1] = tex2D( texUC, (float) (u+4*ib-SOBEL_RADIUS+1), 
											(float) (v-SOBEL_RADIUS));
        SharedBlk_8bit[idx+4*ib+2] = tex2D( texUC, (float) (u+4*ib-SOBEL_RADIUS+2), 
											(float) (v-SOBEL_RADIUS));
        SharedBlk_8bit[idx+4*ib+3] = tex2D( texUC, (float) (u+4*ib-SOBEL_RADIUS+3), 
											(float) (v-SOBEL_RADIUS));
    }
    if ( threadIdx.y < SOBEL_RADIUS*2 ) 
	{
        //
        // copy trailing SOBEL_RADIUS*2 rows of pixels into shared
        //
        idx = (blockDim.y+threadIdx.y) * SharedPitch;
        for ( ib = threadIdx.x; ib < BlockWidth+2*SOBEL_RADIUS; ib += blockDim.x ) 
		{
            SharedBlk_8bit[idx+4*ib+0] = tex2D( texUC,(float) (u+4*ib-SOBEL_RADIUS+0), 
												(float) (v+blockDim.y-SOBEL_RADIUS));
            SharedBlk_8bit[idx+4*ib+1] = tex2D( texUC,(float) (u+4*ib-SOBEL_RADIUS+1), 
												(float) (v+blockDim.y-SOBEL_RADIUS));
            SharedBlk_8bit[idx+4*ib+2] = tex2D( texUC,(float) (u+4*ib-SOBEL_RADIUS+2), 
												(float) (v+blockDim.y-SOBEL_RADIUS));
            SharedBlk_8bit[idx+4*ib+3] = tex2D( texUC,(float) (u+4*ib-SOBEL_RADIUS+3), 
												(float) (v+blockDim.y-SOBEL_RADIUS));
        }
    }

    __syncthreads();

    u >>= 2;    // index as uchar4 from here

    uchar4 *pSobel = (uchar4 *) (((char *)pSobelOriginal)+v*SobelPitch);
    idx = threadIdx.y * SharedPitch;

    for ( ib = threadIdx.x; ib < BlockWidth; ib += blockDim.x ) 
	{
        unsigned char pix00 = SharedBlk_8bit[idx+4*ib+0*SharedPitch+0];
        unsigned char pix01 = SharedBlk_8bit[idx+4*ib+0*SharedPitch+1];
        unsigned char pix02 = SharedBlk_8bit[idx+4*ib+0*SharedPitch+2];
        unsigned char pix10 = SharedBlk_8bit[idx+4*ib+1*SharedPitch+0];
        unsigned char pix11 = SharedBlk_8bit[idx+4*ib+1*SharedPitch+1];
        unsigned char pix12 = SharedBlk_8bit[idx+4*ib+1*SharedPitch+2];
        unsigned char pix20 = SharedBlk_8bit[idx+4*ib+2*SharedPitch+0];
        unsigned char pix21 = SharedBlk_8bit[idx+4*ib+2*SharedPitch+1];
        unsigned char pix22 = SharedBlk_8bit[idx+4*ib+2*SharedPitch+2];

        uchar4 out;

        out.x = Sobel_8bit(pix00, pix01, pix02, 
                           pix10, pix12, pix20, 
						   pix21, pix22, fScale);

        pix00 = SharedBlk_8bit[idx+4*ib+0*SharedPitch+3];
        pix10 = SharedBlk_8bit[idx+4*ib+1*SharedPitch+3];
        pix20 = SharedBlk_8bit[idx+4*ib+2*SharedPitch+3];
        out.y = Sobel_8bit(pix01, pix02, pix00, 
                           pix11, pix10, pix21, 
						   pix22, pix20, fScale);

        pix01 = SharedBlk_8bit[idx+4*ib+0*SharedPitch+4];
        pix11 = SharedBlk_8bit[idx+4*ib+1*SharedPitch+4];
        pix21 = SharedBlk_8bit[idx+4*ib+2*SharedPitch+4];
        out.z = Sobel_8bit(pix02, pix00, pix01, 
                           pix12, pix11, pix22, 
						   pix20, pix21, fScale);

        pix02 = SharedBlk_8bit[idx+4*ib+0*SharedPitch+5];
        pix12 = SharedBlk_8bit[idx+4*ib+1*SharedPitch+5];
        pix22 = SharedBlk_8bit[idx+4*ib+2*SharedPitch+5];
        out.w = Sobel_8bit(pix00, pix01, pix02, 
                           pix10, pix12, pix20, 
						   pix21, pix22, fScale);
        if ( u+ib < w/4 && v < h ) pSobel[u+ib] = out;
    }

    __syncthreads();
}

///////////////////////////////////////////////////////////////////////////////////////
// SobelShared_16bit
//	- Sobel filter based on shared memory (short pixels)
///////////////////////////////////////////////////////////////////////////////////////


__global__ void SobelShared_16bit( ushort4 *pSobelOriginal, unsigned short SobelPitch, 
								   short BlockWidth, short SharedPitch,
								   short w, short h, float fScale )
{ 
    short u = 4*blockIdx.x*BlockWidth;
    short v = blockIdx.y*blockDim.y + threadIdx.y;
    short ib;

    int idx = threadIdx.y * SharedPitch;

    for ( ib = threadIdx.x; ib < BlockWidth+2*SOBEL_RADIUS; ib += blockDim.x ) 
	{
        SharedBlk_16bit[idx+4*ib+0] = tex2D( texUS, (float) (u+4*ib-SOBEL_RADIUS+0), 
											(float) (v-SOBEL_RADIUS));
        SharedBlk_16bit[idx+4*ib+1] = tex2D( texUS, (float) (u+4*ib-SOBEL_RADIUS+1), 
											(float) (v-SOBEL_RADIUS));
        SharedBlk_16bit[idx+4*ib+2] = tex2D( texUS, (float) (u+4*ib-SOBEL_RADIUS+2), 
											(float) (v-SOBEL_RADIUS));
        SharedBlk_16bit[idx+4*ib+3] = tex2D( texUS, (float) (u+4*ib-SOBEL_RADIUS+3), 
											(float) (v-SOBEL_RADIUS));
    }
    if ( threadIdx.y < SOBEL_RADIUS*2 ) 
	{
        //
        // copy trailing SOBEL_RADIUS*2 rows of pixels into shared
        //
        idx = (blockDim.y+threadIdx.y) * SharedPitch;
        for ( ib = threadIdx.x; ib < BlockWidth+2*SOBEL_RADIUS; ib += blockDim.x ) 
		{
            SharedBlk_16bit[idx+4*ib+0] = tex2D( texUS, (float) (u+4*ib-SOBEL_RADIUS+0), 
												(float) (v+blockDim.y-SOBEL_RADIUS));
            SharedBlk_16bit[idx+4*ib+1] = tex2D( texUS, (float) (u+4*ib-SOBEL_RADIUS+1), 
												(float) (v+blockDim.y-SOBEL_RADIUS));
            SharedBlk_16bit[idx+4*ib+2] = tex2D( texUS, (float) (u+4*ib-SOBEL_RADIUS+2), 
												(float) (v+blockDim.y-SOBEL_RADIUS));
            SharedBlk_16bit[idx+4*ib+3] = tex2D( texUS, (float) (u+4*ib-SOBEL_RADIUS+3), 
												(float) (v+blockDim.y-SOBEL_RADIUS));
        }
    }

    __syncthreads();

	u >>= 2;    // index as short4 from here

    ushort4 *pSobel = (ushort4 *) (((unsigned short *) pSobelOriginal)+v*SobelPitch);
    idx = threadIdx.y * SharedPitch;

    for ( ib = threadIdx.x; ib < BlockWidth; ib += blockDim.x ) 
	{
        unsigned short pix00 = SharedBlk_16bit[idx+4*ib+0*SharedPitch+0];
        unsigned short pix01 = SharedBlk_16bit[idx+4*ib+0*SharedPitch+1];
        unsigned short pix02 = SharedBlk_16bit[idx+4*ib+0*SharedPitch+2];
        unsigned short pix10 = SharedBlk_16bit[idx+4*ib+1*SharedPitch+0];
        unsigned short pix11 = SharedBlk_16bit[idx+4*ib+1*SharedPitch+1];
        unsigned short pix12 = SharedBlk_16bit[idx+4*ib+1*SharedPitch+2];
        unsigned short pix20 = SharedBlk_16bit[idx+4*ib+2*SharedPitch+0];
        unsigned short pix21 = SharedBlk_16bit[idx+4*ib+2*SharedPitch+1];
        unsigned short pix22 = SharedBlk_16bit[idx+4*ib+2*SharedPitch+2];

        ushort4 out;

        out.x = Sobel_16bit(pix00, pix01, pix02, 
                            pix10, pix12, pix20, 
							pix21, pix22, fScale);

        pix00 = SharedBlk_16bit[idx+4*ib+0*SharedPitch+3];
        pix10 = SharedBlk_16bit[idx+4*ib+1*SharedPitch+3];
        pix20 = SharedBlk_16bit[idx+4*ib+2*SharedPitch+3];
        out.y = Sobel_16bit(pix01, pix02, pix00, 
                            pix11, pix10, pix21, 
							pix22, pix20, fScale);

        pix01 = SharedBlk_16bit[idx+4*ib+0*SharedPitch+4];
        pix11 = SharedBlk_16bit[idx+4*ib+1*SharedPitch+4];
        pix21 = SharedBlk_16bit[idx+4*ib+2*SharedPitch+4];
        out.z = Sobel_16bit(pix02, pix00, pix01, 
                            pix12, pix11, pix22, 
							pix20, pix21, fScale);

        pix02 = SharedBlk_16bit[idx+4*ib+0*SharedPitch+5];
        pix12 = SharedBlk_16bit[idx+4*ib+1*SharedPitch+5];
        pix22 = SharedBlk_16bit[idx+4*ib+2*SharedPitch+5];
        out.w = Sobel_16bit(pix00, pix01, pix02, 
                            pix10, pix12, pix20, 
							pix21, pix22, fScale);
        if ( u+ib < w/4 && v < h ) pSobel[u+ib] = out;
    }
    __syncthreads();
}

///////////////////////////////////////////////////////////////////////////////////////
// SobelShared_24bit
//	- Sobel filter based on shared memory (short pixels)
///////////////////////////////////////////////////////////////////////////////////////

__global__ void SobelShared_24bit( uchar4 *pSobelOriginal, unsigned short SobelPitch, 
								   short BlockWidth, short SharedPitch,
								   short w, short h, float fScale )
{ 
    short u = 4*blockIdx.x*BlockWidth;
    short v = blockIdx.y*blockDim.y + threadIdx.y;
    short ib;

    int idx = threadIdx.y * SharedPitch;

    for ( ib = threadIdx.x; ib < BlockWidth+2*SOBEL_RADIUS; ib += blockDim.x ) 
	{
        SharedBlk_24bit[idx+4*ib+0] = tex2D( tex4UC, (float) (u+4*ib-SOBEL_RADIUS+0), 
											(float) (v-SOBEL_RADIUS));
        SharedBlk_24bit[idx+4*ib+1] = tex2D( tex4UC, (float) (u+4*ib-SOBEL_RADIUS+1), 
											(float) (v-SOBEL_RADIUS));
        SharedBlk_24bit[idx+4*ib+2] = tex2D( tex4UC, (float) (u+4*ib-SOBEL_RADIUS+2), 
											(float) (v-SOBEL_RADIUS));
        SharedBlk_24bit[idx+4*ib+3] = tex2D( tex4UC, (float) (u+4*ib-SOBEL_RADIUS+3), 
											(float) (v-SOBEL_RADIUS));
    }
    if ( threadIdx.y < SOBEL_RADIUS*2 ) 
	{
        //
        // copy trailing SOBEL_RADIUS*2 rows of pixels into shared
        //
        idx = (blockDim.y+threadIdx.y) * SharedPitch;
        for ( ib = threadIdx.x; ib < BlockWidth+2*SOBEL_RADIUS; ib += blockDim.x ) 
		{
            SharedBlk_24bit[idx+4*ib+0] = tex2D( tex4UC, (float) (u+4*ib-SOBEL_RADIUS+0), 
												(float) (v+blockDim.y-SOBEL_RADIUS));
            SharedBlk_24bit[idx+4*ib+1] = tex2D( tex4UC, (float) (u+4*ib-SOBEL_RADIUS+1), 
												(float) (v+blockDim.y-SOBEL_RADIUS));
            SharedBlk_24bit[idx+4*ib+2] = tex2D( tex4UC, (float) (u+4*ib-SOBEL_RADIUS+2), 
												(float) (v+blockDim.y-SOBEL_RADIUS));
            SharedBlk_24bit[idx+4*ib+3] = tex2D( tex4UC, (float) (u+4*ib-SOBEL_RADIUS+3), 
												(float) (v+blockDim.y-SOBEL_RADIUS));
        }
    }

    __syncthreads();

    uchar4 *pSobel = pSobelOriginal + v*SobelPitch;
    idx = threadIdx.y * SharedPitch;

    for ( ib = threadIdx.x; ib < BlockWidth; ib += blockDim.x ) 
	{

        short4 pix00 = _short4(SharedBlk_24bit[idx+4*ib+0*SharedPitch+0]);
        short4 pix01 = _short4(SharedBlk_24bit[idx+4*ib+0*SharedPitch+1]);
        short4 pix02 = _short4(SharedBlk_24bit[idx+4*ib+0*SharedPitch+2]);
        short4 pix10 = _short4(SharedBlk_24bit[idx+4*ib+1*SharedPitch+0]);
        short4 pix11 = _short4(SharedBlk_24bit[idx+4*ib+1*SharedPitch+1]);
        short4 pix12 = _short4(SharedBlk_24bit[idx+4*ib+1*SharedPitch+2]);
        short4 pix20 = _short4(SharedBlk_24bit[idx+4*ib+2*SharedPitch+0]);
        short4 pix21 = _short4(SharedBlk_24bit[idx+4*ib+2*SharedPitch+1]);
        short4 pix22 = _short4(SharedBlk_24bit[idx+4*ib+2*SharedPitch+2]);

        uchar4 out;

        out = Sobel_24bit(pix00, pix01, pix02, 
                          pix10, pix12, pix20, 
						  pix21, pix22, fScale);
        if ( u+4*ib < w && v < h ) pSobel[u+4*ib] = out;

        pix00 = _short4(SharedBlk_24bit[idx+4*ib+0*SharedPitch+3]);
        pix10 = _short4(SharedBlk_24bit[idx+4*ib+1*SharedPitch+3]);
        pix20 = _short4(SharedBlk_24bit[idx+4*ib+2*SharedPitch+3]);
        out = Sobel_24bit(pix01, pix02, pix00, 
                          pix11, pix10, pix21, 
						  pix22, pix20, fScale);
        if ( u+4*ib+1 < w && v < h ) pSobel[u+4*ib+1] = out;

        pix01 = _short4(SharedBlk_24bit[idx+4*ib+0*SharedPitch+4]);
        pix11 = _short4(SharedBlk_24bit[idx+4*ib+1*SharedPitch+4]);
        pix21 = _short4(SharedBlk_24bit[idx+4*ib+2*SharedPitch+4]);
        out = Sobel_24bit(pix02, pix00, pix01, 
                          pix12, pix11, pix22, 
						  pix20, pix21, fScale);
       if ( u+4*ib+2 < w && v < h ) pSobel[u+4*ib+2] = out;

        pix02 = _short4(SharedBlk_24bit[idx+4*ib+0*SharedPitch+5]);
        pix12 = _short4(SharedBlk_24bit[idx+4*ib+1*SharedPitch+5]);
        pix22 = _short4(SharedBlk_24bit[idx+4*ib+2*SharedPitch+5]);
        out = Sobel_24bit(pix00, pix01, pix02, 
                          pix10, pix12, pix20, 
						  pix21, pix22, fScale);
        if ( u+4*ib+3 < w && v < h ) pSobel[u+4*ib+3] = out;
    }
    __syncthreads();
}

#else

///////////////////////////////////////////////////////////////////////////////////////
// SobelDevice_8bit
//	- Sobel 8 bit filter based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void  SobelDevice_8bit( unsigned char *pSobelOriginal, unsigned int Pitch, 
								   int width, int height, float fScale )
{ 
    unsigned char *pSobel = pSobelOriginal+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
        unsigned char pix00 = tex2D( texUC, (float) i-1, (float) blockIdx.x-1 );
        unsigned char pix01 = tex2D( texUC, (float) i+0, (float) blockIdx.x-1 );
        unsigned char pix02 = tex2D( texUC, (float) i+1, (float) blockIdx.x-1 );
        unsigned char pix10 = tex2D( texUC, (float) i-1, (float) blockIdx.x+0 );
        unsigned char pix12 = tex2D( texUC, (float) i+1, (float) blockIdx.x+0 );
        unsigned char pix20 = tex2D( texUC, (float) i-1, (float) blockIdx.x+1 );
        unsigned char pix21 = tex2D( texUC, (float) i+0, (float) blockIdx.x+1 );
        unsigned char pix22 = tex2D( texUC, (float) i+1, (float) blockIdx.x+1 );
        pSobel[i] = Sobel_8bit(pix00, pix01, pix02, 
							   pix10, pix12, pix20, 
							   pix21, pix22, fScale);
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// SobelDevice_16bit
//	- Sobel 16 bit filter based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void SobelDevice_16bit( unsigned short *pSobelOriginal, unsigned int Pitch, 
								   int width, int height, float fScale )
{ 
    unsigned short *pSobel = pSobelOriginal+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x )
	{
        unsigned short pix00 = tex2D( texUS, (float) i-1, (float) blockIdx.x-1 );
        unsigned short pix01 = tex2D( texUS, (float) i+0, (float) blockIdx.x-1 );
        unsigned short pix02 = tex2D( texUS, (float) i+1, (float) blockIdx.x-1 );
        unsigned short pix10 = tex2D( texUS, (float) i-1, (float) blockIdx.x+0 );
        unsigned short pix12 = tex2D( texUS, (float) i+1, (float) blockIdx.x+0 );
        unsigned short pix20 = tex2D( texUS, (float) i-1, (float) blockIdx.x+1 );
        unsigned short pix21 = tex2D( texUS, (float) i+0, (float) blockIdx.x+1 );
        unsigned short pix22 = tex2D( texUS, (float) i+1, (float) blockIdx.x+1 );
        pSobel[i] = Sobel_16bit(pix00, pix01, pix02, 
								pix10, pix12, pix20, 
								pix21, pix22, fScale );
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// SobelDevice_24bit
//	- Sobel 24 bit filter based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void SobelDevice_24bit(uchar4 *pSobelOriginal, unsigned int Pitch, 
								  int width, int height, float fScale )
{ 
    uchar4 *pSobel = pSobelOriginal+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
        short4 pix00 = _short4(tex2D( tex4UC, (float) i-1, (float) blockIdx.x-1 ));
        short4 pix01 = _short4(tex2D( tex4UC, (float) i+0, (float) blockIdx.x-1 ));
        short4 pix02 = _short4(tex2D( tex4UC, (float) i+1, (float) blockIdx.x-1 ));
        short4 pix10 = _short4(tex2D( tex4UC, (float) i-1, (float) blockIdx.x+0 ));
        short4 pix12 = _short4(tex2D( tex4UC, (float) i+1, (float) blockIdx.x+0 ));
        short4 pix20 = _short4(tex2D( tex4UC, (float) i-1, (float) blockIdx.x+1 ));
        short4 pix21 = _short4(tex2D( tex4UC, (float) i+0, (float) blockIdx.x+1 ));
        short4 pix22 = _short4(tex2D( tex4UC, (float) i+1, (float) blockIdx.x+1 ));
        pSobel[i] = Sobel_24bit(pix00, pix01, pix02, 
								pix10, pix12, pix20, 
								pix21, pix22, fScale );
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// SobelDevice_48bit
//	- Sobel 48 bit filter based on device memory
///////////////////////////////////////////////////////////////////////////////////////

__global__ void SobelDevice_48bit(ushort4 *pSobelOriginal, unsigned int Pitch, 
								  int width, int height, float fScale )
{ 
    ushort4 *pSobel = pSobelOriginal+blockIdx.x*Pitch;
    for ( int i = threadIdx.x; i < width; i += blockDim.x ) 
	{
        ushort4 pix00 = tex2D( tex4US, (float) i-1, (float) blockIdx.x-1 );
        ushort4 pix01 = tex2D( tex4US, (float) i+0, (float) blockIdx.x-1 );
        ushort4 pix02 = tex2D( tex4US, (float) i+1, (float) blockIdx.x-1 );
        ushort4 pix10 = tex2D( tex4US, (float) i-1, (float) blockIdx.x+0 );
        ushort4 pix12 = tex2D( tex4US, (float) i+1, (float) blockIdx.x+0 );
        ushort4 pix20 = tex2D( tex4US, (float) i-1, (float) blockIdx.x+1 );
        ushort4 pix21 = tex2D( tex4US, (float) i+0, (float) blockIdx.x+1 );
        ushort4 pix22 = tex2D( tex4US, (float) i+1, (float) blockIdx.x+1 );
        pSobel[i] = Sobel_48bit(pix00, pix01, pix02, 
								pix10, pix12, pix20, 
								pix21, pix22, fScale );
    }
}

#endif

///////////////////////////////////////////////////////////////////////////////////////
// CudaSobel
//	- entry point for CUDA
///////////////////////////////////////////////////////////////////////////////////////

extern "C"
void CudaSobel(cudaArray *src, void *dest, int width, int height, int bpp, float fScale)
{
#ifdef SOBEL_SHARED
	int		BlockWidth, SharedPitch, sharedMem;
	dim3	threads, blocks;
#endif

	switch (bpp)
	{
		case 8:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUC, src));
			    
			#ifdef SOBEL_SHARED
				BlockWidth = 96; // must be divisible by 16 for coalescing
				threads.x=8;
				threads.y=8;
				blocks = dim3(iDivUp(width,4*BlockWidth),iDivUp(height,threads.y));
				SharedPitch = ~0x3f&(4*(BlockWidth+2*SOBEL_RADIUS)+0x3f);
				sharedMem = SharedPitch*(threads.y+2*SOBEL_RADIUS);

				// for the shared kernel, width must be divisible by 4
				width &= ~3;

				SobelShared_8bit<<<blocks, threads, sharedMem>>>((uchar4 *)dest, width, 
																 BlockWidth, SharedPitch, 
																 width, height, fScale );
			#else
				SobelDevice_8bit<<<height, 384>>>((unsigned char *)dest, width, width, 
												  height, fScale );
			#endif

				CUDA_SAFE_CALL(cudaUnbindTexture(texUC));
				break;
		case 16:
				CUDA_SAFE_CALL(cudaBindTextureToArray(texUS, src));
			    
			#ifdef SOBEL_SHARED
				BlockWidth = 96; // must be divisible by 16 for coalescing
				threads.x=8;
				threads.y=8;
				blocks = dim3(iDivUp(width,4*BlockWidth),iDivUp(height,threads.y));
				SharedPitch = ~0x3f&(4*(BlockWidth+2*SOBEL_RADIUS)+0x3f);
				sharedMem = 2*SharedPitch*(threads.y+2*SOBEL_RADIUS);

				// for the shared kernel, width must be divisible by 4
				width &= ~3;

				SobelShared_16bit<<<blocks, threads, sharedMem>>>((ushort4 *)dest, width, 
																  BlockWidth, SharedPitch, 
																  width, height, fScale);
			#else
				SobelDevice_16bit<<<height, 384>>>((unsigned short *)dest, width, width, 
												   height, fScale);
			#endif
				CUDA_SAFE_CALL(cudaUnbindTexture(texUS));
				break;
		case 24:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4UC, src));
			#ifdef SOBEL_SHARED
				BlockWidth = 96; // must be divisible by 16 for coalescing
				threads.x=16;
				threads.y=6;
				blocks = dim3(iDivUp(width,4*BlockWidth),iDivUp(height,threads.y));
				SharedPitch = ~0x3f&(4*(BlockWidth+2*SOBEL_RADIUS)+0x3f);
				sharedMem = 4*SharedPitch*(threads.y+2*SOBEL_RADIUS);

				// for the shared kernel, width must be divisible by 4
				width &= ~3;

				SobelShared_24bit<<<blocks, threads, sharedMem>>>((uchar4 *)dest, width, 
																  BlockWidth, SharedPitch, 
																  width, height, fScale);
			#else
				SobelDevice_24bit<<<height, 192>>>((uchar4 *)dest, width, width, height, 
												   fScale);
			#endif
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4UC));
				break;
		case 48:
				CUDA_SAFE_CALL(cudaBindTextureToArray(tex4US, src));
			#ifdef SOBEL_SHARED
				BlockWidth = 96; // must be divisible by 16 for coalescing
				threads.x=16;
				threads.y=6;
				blocks = dim3(iDivUp(width,4*BlockWidth),iDivUp(height,threads.y));
				SharedPitch = ~0x3f&(4*(BlockWidth+2*SOBEL_RADIUS)+0x3f);
				sharedMem = 4*SharedPitch*(threads.y+2*SOBEL_RADIUS);

				// for the shared kernel, width must be divisible by 4
				width &= ~3;

				SobelShared_48bit<<<blocks, threads, sharedMem>>>((ushort4 *)dest, width, 
																  BlockWidth, SharedPitch, 
																  width, height, fScale);
			#else
				SobelDevice_48bit<<<height, 192>>>((ushort4 *)dest, width, width, height, 
												   fScale);
			#endif
				CUDA_SAFE_CALL(cudaUnbindTexture(tex4US));
				break;
	}
}
