// header file for Sobel_Kernel.cu
#define OFFSET(i)		((char *)NULL + (i))
#define Radius			1

//#define SOBEL_SHARED	true

extern "C" void CudaSobel(cudaArray *src, void *dest, int width, int height, int bpp, float fScale);
