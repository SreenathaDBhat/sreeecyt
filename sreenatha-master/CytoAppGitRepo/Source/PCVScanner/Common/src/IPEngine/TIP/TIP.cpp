// TIP.cpp
// Tesla Image Processing

#include "stdafx.h"
#include "cutil.h"
#include "cuda_runtime_api.h"
#include "TIPImage.h"
#include "TIPSwapImage.h"
#include "TIP.h"
#include "math.h"

// Header files Cuda Kernels 
#include "CudaTIP_Kernels.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

TIP::TIP()
{
	int	i, j;
	for (i = 0; i< TIP_SWAPBUFFERS; i++)
		pSwapImage[i] = NULL;
	for (j = 0; j < MAX_CUDA_DEVICES; j++)
	{
		for (i = 0; i < TIP_MAXIMAGES; i++)
		{
			m_Images[j][i].pImage = NULL;
			m_Images[j][i].Status = Empty;
		}
	}
	m_SwapCounter=0;
	m_CUDADeviceCount=0;
}

TIP::~TIP()
{
	int	i, j;
	if (m_CUDADeviceCount> 0)
	{
		for (j = 0; j < m_CUDADeviceCount; j++)
		{
			for (i = 0; i < TIP_MAXIMAGES; i++)
			{
				if (m_Images[j][i].Status != Empty)
					TIPFreeImage(&m_Images[j][i].pImage);
			}
		}

		for (i = 0; i < TIP_SWAPBUFFERS; i++)
			if (pSwapImage[i]) delete pSwapImage[i];
	}
}

int TIP::Create()
{
	BOOL	cudaDevPresent;
    int		dev, devcnt;

    CUDA_SAFE_CALL_NO_SYNC(cudaGetDeviceCount(&m_CUDADeviceCount));
	TRACE1("TIP:: Cuda Device Count:%d\r\n",m_CUDADeviceCount);
    if (m_CUDADeviceCount == 0)
	{
        fprintf(stderr, "There is no device.\n");
        exit(EXIT_FAILURE);
		return m_CUDADeviceCount;
    }
	cudaDevPresent=FALSE;
	dev=0;
    for (devcnt = 0; devcnt < m_CUDADeviceCount; ++devcnt) 
	{
        CUDA_SAFE_CALL_NO_SYNC(cudaGetDeviceProperties(&m_CUDADevProp[dev], dev));
		TRACE2("Dev name:%s\r\n totalGlobalMem:%d\r\n",
			m_CUDADevProp[dev].name, m_CUDADevProp[dev].totalGlobalMem);
		TRACE2(" sharedMemPerBlock:%d\r\n regsPerBlock:%d\r\n", 
			m_CUDADevProp[dev].sharedMemPerBlock, m_CUDADevProp[dev].regsPerBlock);
		TRACE2(" warpSize:%d\r\n memPitch:%d\r\n", 
			m_CUDADevProp[dev].warpSize, m_CUDADevProp[dev].memPitch);
 		TRACE2(" maxThreadsPerBlock:%d\r\n maxThreadsDim[0]:%d\r\n", 
			m_CUDADevProp[dev].maxThreadsPerBlock, m_CUDADevProp[dev].maxThreadsDim[0]);
		TRACE2(" maxThreadsDim[1]:%d\r\n maxThreadsDim[2]:%d\r\n", 
			m_CUDADevProp[dev].maxThreadsDim[1], m_CUDADevProp[dev].maxThreadsDim[2]);
		TRACE2(" maxGridSize[0]:%d\r\n maxGridSize[1]:%d\r\n", 
			m_CUDADevProp[dev].maxGridSize[0], m_CUDADevProp[dev].maxGridSize[1]);
		TRACE2(" maxGridSize[2]:%d\r\n totalConstMem:%d\r\n", 
			m_CUDADevProp[dev].maxGridSize[2], m_CUDADevProp[dev].totalConstMem);
		TRACE2(" major:%d\r\n minor:%d\r\n", 
			m_CUDADevProp[dev].major, m_CUDADevProp[dev].minor);
		TRACE2(" clockRate:%d\r\n textureAlignment:%d\r\n", 
			m_CUDADevProp[dev].clockRate, m_CUDADevProp[dev].textureAlignment);
	     if (m_CUDADevProp[dev].major >= 1)
		 {
			 cudaDevPresent=TRUE;
			 dev +=1;
		 }
    } 
	m_CUDADeviceCount=dev;
    if (!cudaDevPresent)
	{
		m_CUDADeviceCount=0;
        fprintf(stderr, "There is no device supporting CUDA.\n");
		return m_CUDADeviceCount;
    }
    else
	{
        CUDA_SAFE_CALL(cudaSetDevice(0));
	}
	return m_CUDADeviceCount;
}

int TIP::GetDeviceCount(void)
{
	return(this->m_CUDADeviceCount);
}

//--------------------------------------------------------------------------------------
// TIPCreateImage - Create TIP Images Dynamically
//--------------------------------------------------------------------------------------
TIPImage** TIP::TIPCreateImage(int Device, int Width, int Height, int Overlap, int BPP, 
							   BOOL IsCudaArray)
{
	TIPImage *src, **Handle = NULL;
    int i, w, h, dev, bpp, overlap;
    BOOL Alloc = FALSE, is_cuda_array;

    for (i = 0; i < TIP_MAXIMAGES; i++)
    {
        // First check any released images for current Device, 
		// if any have the same dimensions then just use that image
        if (m_Images[Device][i].Status == Released)
        {
            m_Images[Device][i].pImage->GetImageInfo(&dev, &w, &h, &overlap, &bpp, 
													 &is_cuda_array);
            if ((w == Width) && (h == Height) && (bpp == BPP) && (overlap == Overlap) &&
				(is_cuda_array==IsCudaArray))
            {
                Handle = m_Images[Device][i].pImage->GetHandle();
                m_Images[Device][i].Status = InUse;                 // Back in use again
                break;
            }
        }
    }
    // No textures that we can already use so can we create a new one ?
    if (Handle == NULL)
    {
        for (i = 0; i < TIP_MAXIMAGES; i++)
        {
            // Found an empty slot so use it
           if (m_Images[Device][i].Status == Empty)
            {
                Alloc = TRUE;
                break;
            }
        }

        // If this is true we have no empty slots so go through any released
        // images and reallocate with new image dimensions
        if (!Alloc)
        {
 	
			for (i = 0; i < TIP_MAXIMAGES; i++)
            {
                // If this condition is true then the image is free to use
                // but is the wrong dimensions so free it off
                if (m_Images[Device][i].Status == Released)
                {
                    Alloc = TRUE;
                    TIPFreeImage(m_Images[Device][i].pImage->GetHandle());
                    break;
                }
            }
        }

        // Allocate a new texture
        if (Alloc)
        {
	        src = new TIPImage(Device);
	        if (src != NULL)
	        {
		        if (src->Create(Width, Height, Overlap, BPP, IsCudaArray))
                {
			        Handle = src->GetHandle();
                    m_Images[Device][i].pImage = src;
                    m_Images[Device][i].Status = InUse;
                }
	        }
        }
    }
 	return Handle;
}

//--------------------------------------------------------------------------------------
// Release a GIP image for re-use elsewhere
//--------------------------------------------------------------------------------------

void TIP::TIPReleaseImage(TIPImage **TipSource)
{
	if (TipSource > NULL)
    {
        int i, device;
		device=(*TipSource)->GetDevice();
        for (i = 0; i < TIP_MAXIMAGES; i++)
        {
            if (TipSource == m_Images[device][i].pImage->GetHandle())
            {
                m_Images[device][i].Status = Released;
                break;
            }
        }
    }
}

//--------------------------------------------------------------------------------------
// Release all TIP images. Not this does not free all the textures off. These will be 
// kept and re-used if neccessary
//--------------------------------------------------------------------------------------

void TIP::TIPReleaseAll(void)
{
    int i, j;
    for (j = 0; j < MAX_CUDA_DEVICES; j++)
	{
		for (i = 0; i < TIP_MAXIMAGES; i++)
		{
			if (m_Images[j][i].Status != Empty)
				m_Images[j][i].Status = Released;
		}
	}
}

//--------------------------------------------------------------------------------------
// GIPFreeImage - Destroy GIP Images Dynamically
//--------------------------------------------------------------------------------------
void TIP::TIPFreeImage(TIPImage **HandleTipImage)
{
	if (HandleTipImage > 0)
    {
        int i, device;
		device=(*HandleTipImage)->GetDevice();
        for (i = 0; i < TIP_MAXIMAGES; i++)
        {
            if (HandleTipImage == m_Images[device][i].pImage->GetHandle())
            {
				m_Images[device][i].Status = Empty;
                break;
            }
        }
		delete (TIPImage *)*HandleTipImage;
    }
}

//--------------------------------------------------------------------------------------
//	GetSwapImage -  Get a matching SwapImage from locked memory
//--------------------------------------------------------------------------------------
TIPSwapImage* TIP::GetSwapImage(TIPImage *sourceHandle)
{
	TIPSwapImage*	pSwap;
	int				w, h, bpp, overlap, dev;
	long			minCount=0x7FFFFFFF;
	int				i, i0, i1;
	BOOL			ok, is_cuda_array;

	m_SwapCounter +=1;
	sourceHandle->GetImageInfo(&dev, &w, &h, &overlap, &bpp, &is_cuda_array);
	if (bpp==24) bpp=32;		// swapImages for Colour and 32 bit floats are the same

	// check if corresponding swaptexture exist 
	i=0; i0=-1;
	do 
	{
		if (pSwapImage[i]!=NULL)
		{
			if ((pSwapImage[i]->GetWidth()==w) && (pSwapImage[i]->GetHeight()==h) && 
				(pSwapImage[i]->GetOverlap()==overlap) && (pSwapImage[i]->GetBpp()==bpp))
			{
				m_SwapLastUsed[i]=m_SwapCounter;
				return (pSwapImage[i]);
			}
			else if ((i0 < 0) && (m_SwapLastUsed[i] < minCount))
			{
				i1=i;
				minCount=m_SwapLastUsed[i];
			}
		}
		else if (i0< 0) i0=i;			
		i++;
	}while (i<TIP_SWAPBUFFERS);

	if (i0<0) i0=i1;
	// not found yet so create new swaptexture
	if (pSwapImage[i0] != NULL) delete pSwapImage[i0];

	pSwap = new TIPSwapImage();
    // Create locked array in host memory
	switch (bpp)
	{
		case 8:
				ok=pSwap->Create(w, h, overlap, 8);
				break;
		case 16:
				ok=pSwap->Create(w, h, overlap, 16);
				break;
		case 24:
		case 32:
				ok=pSwap->Create(w, h, overlap, 32);
				break;
		case 48:
				ok=pSwap->Create(w, h, overlap, 64);
				break;
	}
    if (!ok)
	{
		AfxMessageBox("Could not create a locked array in host memory");
		if (pSwap !=NULL) delete pSwap;
		return NULL;
	}
	pSwapImage[i0]=pSwap;
	m_SwapLastUsed[i0]=m_SwapCounter;
	return (pSwap);
}

//--------------------------------------------------------------------------------------
//	GetImageSpecs
//--------------------------------------------------------------------------------------
void TIP::GetImageSpecs(TIPImage **Source, int *Device, int *Width, int *Height, 
										int *Overlap, int *BPP, BOOL *CudaArrayFlg)
{
	if (Source > 0)
		((TIPImage *)(*Source))->GetImageInfo(Device, Width, Height, Overlap, BPP,
											  CudaArrayFlg);
}

//--------------------------------------------------------------------------------------
//	GetBpp
//--------------------------------------------------------------------------------------
int TIP::GetBpp(TIPImage **Source)
{
	return (((TIPImage *)(*Source))->GetBpp());
}

//--------------------------------------------------------------------------------------
//	GetOverlap
//--------------------------------------------------------------------------------------
int TIP::GetOverlap(TIPImage **Source)
{
	return (((TIPImage *)(*Source))->GetOverlap());
}

//--------------------------------------------------------------------------------------
//	GetImage - Get 8-bit Image from GPU into existing image buffer
//--------------------------------------------------------------------------------------
BOOL TIP::GetImage(TIPImage **targetHandle, BYTE *pImageData)
{
	TIPSwapImage* pSwap;
    if (!targetHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*targetHandle))==NULL) return FALSE;
	return ((TIPImage *)(*targetHandle))->GetImage(pSwap, pImageData);
};

//--------------------------------------------------------------------------------------
//	GetImage - Get 16-bit Image from GPU into existing image buffer
//--------------------------------------------------------------------------------------
BOOL TIP::GetImage(TIPImage **targetHandle, unsigned short *pImageData)
{
	TIPSwapImage* pSwap;
    if (!targetHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*targetHandle))==NULL) return FALSE;
	return ((TIPImage *)(*targetHandle))->GetImage(pSwap, pImageData);
};

//--------------------------------------------------------------------------------------
//	GetImage - Get 3 x 8-bit Colour Image from GPU into existing image buffer
//--------------------------------------------------------------------------------------
BOOL TIP::GetImage(TIPImage **targetHandle, RGBTRIPLE *pImageData)
{
	TIPSwapImage* pSwap;
    if (!targetHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*targetHandle))==NULL) return FALSE;
	return ((TIPImage *)(*targetHandle))->GetImage(pSwap, pImageData);
};

//--------------------------------------------------------------------------------------
//	GetImage - Get 3 x 16-bit Colour Image from GPU into existing image buffer
//--------------------------------------------------------------------------------------
BOOL TIP::GetImage(TIPImage **targetHandle, RGB16BTRIPLE *pImageData)
{
	TIPSwapImage* pSwap;
    if (!targetHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*targetHandle))==NULL) return FALSE;
	return ((TIPImage *)(*targetHandle))->GetImage(pSwap, pImageData);
};

//--------------------------------------------------------------------------------------
//	GetImage - Get float Image from GPU into existing image buffer
//--------------------------------------------------------------------------------------
BOOL TIP::GetImage(TIPImage **targetHandle, float *pImageData)
{
	TIPSwapImage* pSwap;
    if (!targetHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*targetHandle))==NULL) return FALSE;
	return ((TIPImage *)(*targetHandle))->GetImage(pSwap, pImageData);
};

//--------------------------------------------------------------------------------------
//	GetImageComponent - Get Image component from GPU into existing image buffer
//--------------------------------------------------------------------------------------
BOOL TIP::GetImageComponent(TIPImage **targetHandle, int component, BYTE *pImageData)
{
	TIPSwapImage* pSwap;
    if (!targetHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*targetHandle))==NULL) return FALSE;
	return ((TIPImage *)(*targetHandle))->GetImageComp(pSwap, component, pImageData);
}

//--------------------------------------------------------------------------------------
//	GetImageComponent - Get Image component from GPU into existing image buffer
//--------------------------------------------------------------------------------------
BOOL TIP::GetImageComponent(TIPImage **targetHandle, int component, 
						   unsigned short *pImageData)
{
	TIPSwapImage* pSwap;
    if (!targetHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*targetHandle))==NULL) return FALSE;
	return ((TIPImage *)(*targetHandle))->GetImageComp(pSwap, component, pImageData);
}

//--------------------------------------------------------------------------------------
//	PutImage - Put 8-bit Image in GPU
//--------------------------------------------------------------------------------------
BOOL TIP::PutImage(TIPImage **sourceHandle, BYTE *pImageData)
{
	TIPSwapImage* pSwap;
	if (!sourceHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*sourceHandle))==NULL) return FALSE;
	return ((TIPImage *)(*sourceHandle))->PutImage(pSwap, pImageData);
};

//--------------------------------------------------------------------------------------
//	PutImage - Put 16-bit Image in GPU
//--------------------------------------------------------------------------------------
BOOL TIP::PutImage(TIPImage **sourceHandle, unsigned short *pImageData)
{
	TIPSwapImage* pSwap;
	if (!sourceHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*sourceHandle))==NULL) return FALSE;
	return ((TIPImage *)(*sourceHandle))->PutImage(pSwap, pImageData);
};

//--------------------------------------------------------------------------------------
//	PutImage - Put 3 x 8-bit Colour Image in GPU
//--------------------------------------------------------------------------------------

BOOL TIP::PutImage(TIPImage **sourceHandle, RGBTRIPLE *pImageData)
{
	TIPSwapImage* pSwap;
	if (!sourceHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*sourceHandle))==NULL) return FALSE;
	return ((TIPImage *)(*sourceHandle))->PutImage(pSwap, pImageData);
};

//--------------------------------------------------------------------------------------
//	PutImage - Put 3 x 16-bit Colour Image in GPU
//--------------------------------------------------------------------------------------
BOOL TIP::PutImage(TIPImage **sourceHandle, RGB16BTRIPLE *pImageData)
{
	TIPSwapImage* pSwap;
	if (!sourceHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*sourceHandle))==NULL) return FALSE;
	return ((TIPImage *)(*sourceHandle))->PutImage(pSwap, pImageData);
};

//--------------------------------------------------------------------------------------
//	PutImage - Put float Image in GPU
//--------------------------------------------------------------------------------------
BOOL TIP::PutImage(TIPImage **sourceHandle, float *pImageData)
{
	TIPSwapImage* pSwap;
	if (!sourceHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*sourceHandle))==NULL) return FALSE;
	return ((TIPImage *)(*sourceHandle))->PutImage(pSwap, pImageData);
};

//--------------------------------------------------------------------------------------
//	PutImage - Put Part of grey Image with width=w and height=h from startpoint 
//			   (x0, y0) in existing TIPImage
//--------------------------------------------------------------------------------------
BOOL TIP::PutImage(TIPImage **targetHandle, BYTE *pImageData, int w, int h, int x0, 
				   int y0)
{
	TIPSwapImage* pSwap;
	if (!targetHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*targetHandle))==NULL) return FALSE;
	return ((TIPImage *)(*targetHandle))->PutImage(pSwap, pImageData, w, h, x0, y0);
}

//--------------------------------------------------------------------------------------
//	PutImage - Put Part of 16 bit grey Image with width=w and height=h from startpoint 
//			   (x0, y0) in existing TIPImage
//--------------------------------------------------------------------------------------
BOOL TIP::PutImage(TIPImage **targetHandle, unsigned short *pImageData, int w, int h, 
				   int x0, int y0)
{
	TIPSwapImage* pSwap;
	if (!targetHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*targetHandle))==NULL) return FALSE;
	return ((TIPImage *)(*targetHandle))->PutImage(pSwap, pImageData, w, h, x0, y0);
}

//--------------------------------------------------------------------------------------
//	PutImage - Put Part of colour Image with width=w and height=h from startpoint 
//			   (x0, y0) in existing TIPImage
//--------------------------------------------------------------------------------------
BOOL TIP::PutImage(TIPImage **targetHandle, RGBTRIPLE *pImageData, int w, int h, int x0, 
				   int y0, int ColourMode)
{
	TIPSwapImage* pSwap;
	if (!targetHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*targetHandle))==NULL) return FALSE;
	return ((TIPImage *)(*targetHandle))->PutImage(pSwap, pImageData, w, h, x0, y0,
												   ColourMode);
}

//--------------------------------------------------------------------------------------
//	PutImage - Put Part of 16 bit colour Image with width=w and height=h from startpoint 
//			   (x0, y0) in existing TIPImage
//--------------------------------------------------------------------------------------
BOOL TIP::PutImage(TIPImage **targetHandle, RGB16BTRIPLE *pImageData,int w, int h, int x0, 
				   int y0, int ColourMode)
{
	TIPSwapImage* pSwap;
	if (!targetHandle || !pImageData) return FALSE;
	// set the swap texture surface according to the TIPImage
	if ((pSwap=GetSwapImage(*targetHandle))==NULL) return FALSE;
	return ((TIPImage *)(*targetHandle))->PutImage(pSwap, pImageData, w, h, x0, y0,
												   ColourMode);
}

//--------------------------------------------------------------------------------------
//	IsCudaArray - returns CudaArray Status of TIPImage
//--------------------------------------------------------------------------------------
BOOL TIP::IsCudaArray(TIPImage **source)
{
	return((*source)->IsCudaArray());
}

//--------------------------------------------------------------------------------------
//	TIPCopy - copy source to destination image
//--------------------------------------------------------------------------------------
void TIP::Copy(TIPImage **src, TIPImage **target)
{
	if ((!src) || (!target)) return;
	if (!(*src)->SameSpecs(*target)) return;	// device of images should be the same
	if ((*src)->IsCudaArray() && (*target)->IsCudaArray())
	{
		// cudaArray -> cudaArray
		CUDA_SAFE_CALL(cudaMemcpyArrayToArray((cudaArray *)(*target)->GetCudaPtr(), 0,  
											  0,(cudaArray *)(*src)->GetCudaPtr(), 0, 0, 
											  (*src)->GetSize(), 
											  cudaMemcpyDeviceToDevice));
		return;
	}
	else if (!(*src)->IsCudaArray() && (*target)->IsCudaArray())
	{
		// array -> cudaArray
		CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)(*target)->GetCudaPtr(), 0, 0,
										 (*src)->GetCudaPtr(), 
										 (*src)->GetSize(), cudaMemcpyDeviceToDevice));
		return;
	}
	else if ((*src)->IsCudaArray() && !(*target)->IsCudaArray())
	{
		// cudaArray -> array
		CUDA_SAFE_CALL(cudaMemcpyFromArray((*target)->GetCudaPtr(), 
										 (cudaArray *)(*src)->GetCudaPtr(), 0, 0, 
										 (*src)->GetSize(), cudaMemcpyDeviceToDevice));
		return;
	}
	else
	{
		// array -> array
		CUDA_SAFE_CALL(cudaMemcpy((*target)->GetCudaPtr(), (*src)->GetCudaPtr(),
								  (*src)->GetSize(), cudaMemcpyDeviceToDevice));
	}
}

//--------------------------------------------------------------------------------------
//	Sobel (in Sobel_Kernel.cu)
//--------------------------------------------------------------------------------------
void TIP::Sobel(TIPImage **src, TIPImage **target, float fScale)
{
	int	width, height, bpp;

	if ((!src) || (!target) || (fScale <= 0)) return;
	if (!(*src)->SameSpecs(*target)) return;	// device of images should be the same
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return;
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaSobel((cudaArray *)(*src)->GetCudaPtr(),(*target)->GetCudaPtr(), 
				width, height, bpp, fScale);
}

//---------------------------------------------------------------------------------------------
//	Convolve (in ConvolutionSeparable_Kernel.cu))
//---------------------------------------------------------------------------------------------
void TIP::Convolve(long nPasses, int FiltHorzHW, float *CoeffHorz, int FiltVertHW,
									float *CoeffVert, TIPImage **src, TIPImage **target)
{
	TIPImage	**tmpIma, **tmpIma2;
	int			width, height, bpp;

	if ((!src) || (!target)) return;
	if ((FiltHorzHW<= 0) || (FiltHorzHW>MAX_KERNELRADIUS)) return;
	if ((FiltVertHW<= 0) || (FiltVertHW>MAX_KERNELRADIUS)) return;
	if (!(*src)->SameSpecs(*target)) return;	// device of images should be the same
	if ((*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	tmpIma = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	if (nPasses > 1)
		tmpIma2 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	else
		tmpIma2 = tmpIma;
	CudaInitFilterCoefficients(FiltHorzHW, CoeffHorz, FiltVertHW, CoeffVert);
	CudaRepeatedConvolve(nPasses, (*src)->GetCudaPtr(),(*target)->GetCudaPtr(), 
		(*tmpIma)->GetCudaPtr(), (*tmpIma2)->GetCudaPtr(), width, height, bpp, FiltHorzHW);
	TIPReleaseImage(tmpIma);
	if (nPasses > 1)TIPReleaseImage(tmpIma2);
}

//---------------------------------------------------------------------------------------------
//	ConvolveSym (in ConvolutionSeparable_Kernel.cu))
//---------------------------------------------------------------------------------------------
void TIP::ConvolveSym(long nPasses, int FilterHW, float *Coeff, TIPImage **src, 
									TIPImage **target)
{
	TIPImage	**tmpIma, **tmpIma2;
	int			width, height, bpp;

	if ((!src) || (!target)) return;
	if ((FilterHW<= 0) || (FilterHW>MAX_KERNELRADIUS)) return;
	if (!(*src)->SameSpecs(*target)) return;	// device of images should be the same
	if ((*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	tmpIma = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	if (nPasses > 1)
		tmpIma2 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	else
		tmpIma2 = tmpIma;
	CudaInitFilterCoefficients(FilterHW, Coeff, FilterHW, Coeff);
	CudaRepeatedConvolve(nPasses, (*src)->GetCudaPtr(),(*target)->GetCudaPtr(), 
		(*tmpIma)->GetCudaPtr(), (*tmpIma2)->GetCudaPtr(), width, height, bpp, FilterHW);
	TIPReleaseImage(tmpIma);
	if (nPasses > 1)TIPReleaseImage(tmpIma2);
}

//---------------------------------------------------------------------------------------------
//	GaussianBlur (in ConvolutionSeparable_Kernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::GaussianBlur(int FilterHW, float Sigma, TIPImage **src, TIPImage **target)
{
	TIPImage	**tmpIma;
	int			width, height, bpp;

	if ((!src) || (!target)) return;
	if ((FilterHW<= 0) || (FilterHW>MAX_KERNELRADIUS)) return;
	if (!(*src)->SameSpecs(*target)) return;	// device of images should be the same
	if ((*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	tmpIma = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	CudaInitGaussianFilter(FilterHW, Sigma);
	CudaConvolution((*src)->GetCudaPtr(),(*target)->GetCudaPtr(), (*tmpIma)->GetCudaPtr(),
				width, height, bpp, FilterHW);
	TIPReleaseImage(tmpIma);
}

//---------------------------------------------------------------------------------------------
//	UnsharpMask (in ConvolutionSeparable_Kernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::UnsharpMask(int FilterHW, float Sigma, float Amount, TIPImage **src, 
					  TIPImage **target)
{
	TIPImage	**tmpIma1, **tmpIma2;
	int			height, width, bpp;

	if ((!src) || (!target)) return;
	if ((FilterHW<= 0) || (FilterHW>MAX_KERNELRADIUS)) return;
	if ((Amount <= 0) || (Amount>=1.0)) return;
	if (!(*src)->SameSpecs(*target)) return;	// device of images should be the same
	if ((*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	tmpIma1 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	tmpIma2 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	CudaInitGaussianFilter(FilterHW, Sigma);
	CudaConvolution((*src)->GetCudaPtr(),(*tmpIma1)->GetCudaPtr(), (*tmpIma2)->GetCudaPtr(),
				width, height, bpp, FilterHW);
	CudaInitAmount(Amount);
	CudaUnsharpMask((*src)->GetCudaPtr(),(*tmpIma1)->GetCudaPtr(),(*target)->GetCudaPtr(),
				width, height, bpp);
	TIPReleaseImage(tmpIma2);
	TIPReleaseImage(tmpIma1);
}

//---------------------------------------------------------------------------------------------
//	Erode (in MinMax_Kernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::Erode(int nPasses, TIPImage **src, TIPImage **target)
{
	TIPImage	**tmpIma;
	int			width, height, bpp;

	if ((!src) || (!target)) return;
	if ((nPasses<= 0) || (nPasses>MAX_KERNELRADIUS)) return;
	if (!(*src)->SameSpecs(*target)) return;	// device of images should be the same
	(*src)->GetPackedDimensions(&width, &height, &bpp);
#ifdef MINMAX_SHARED
	if ((*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	tmpIma = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	CudaErode((*src)->GetCudaPtr(),(*target)->GetCudaPtr(), (*tmpIma)->GetCudaPtr(),
				width, height, bpp, nPasses);
#else
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	tmpIma = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), TRUE);
	CudaErode((cudaArray *)(*src)->GetCudaPtr(),(*target)->GetCudaPtr(), 
				(cudaArray *)(*tmpIma)->GetCudaPtr(), width, height, bpp, nPasses);
#endif
	TIPReleaseImage(tmpIma);
}

//---------------------------------------------------------------------------------------------
//	Dilate (in MinMax_Kernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::Dilate(int nPasses, TIPImage **src, TIPImage **target)
{
	TIPImage	**tmpIma;
	int			width, height, bpp;

	if ((!src) || (!target)) return;
	if ((nPasses<= 0) || (nPasses>MAX_KERNELRADIUS)) return;
	if (!(*src)->SameSpecs(*target)) return;	// device of images should be the same
	(*src)->GetPackedDimensions(&width, &height, &bpp);
#ifdef MINMAX_SHARED
	if ((*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	tmpIma = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	CudaDilate((*src)->GetCudaPtr(),(*target)->GetCudaPtr(), (*tmpIma)->GetCudaPtr(),
				width, height, bpp, nPasses);
#else
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	tmpIma = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), TRUE);
	CudaDilate((cudaArray *)(*src)->GetCudaPtr(),(*target)->GetCudaPtr(), 
				(cudaArray *)(*tmpIma)->GetCudaPtr(), width, height, bpp, nPasses);
#endif
	TIPReleaseImage(tmpIma);
}

//---------------------------------------------------------------------------------------------
//	Close (in MinMax_Kernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::Close(int nPasses, TIPImage **src, TIPImage **target)
{
	TIPImage	**tmpIma1; 
	int			width, height, bpp;

	if ((!src) || (!target)) return;
	if ((nPasses<= 0) || (nPasses>MAX_KERNELRADIUS)) return;
	if (!(*src)->SameSpecs(*target)) return;	// device of images should be the same
	(*src)->GetPackedDimensions(&width, &height, &bpp);
#ifdef MINMAX_SHARED
	TIPImage	**tmpIma2;
	if ((*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	tmpIma1 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	tmpIma2 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	CudaDilate((*src)->GetCudaPtr(),(*tmpIma1)->GetCudaPtr(), (*tmpIma2)->GetCudaPtr(),
				width, height, bpp, nPasses);
	CudaErode((*tmpIma1)->GetCudaPtr(),(*target)->GetCudaPtr(), (*tmpIma2)->GetCudaPtr(),
				width, height, bpp, nPasses);
#else
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	tmpIma1 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), TRUE);
	CudaDilate((cudaArray *)(*src)->GetCudaPtr(),(*target)->GetCudaPtr(), 
				(cudaArray *)(*tmpIma1)->GetCudaPtr(), width, height, bpp, nPasses);
	CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)(*tmpIma1)->GetCudaPtr(), 0, 0, 
				(*target)->GetCudaPtr(), (*tmpIma1)->GetSize(), cudaMemcpyDeviceToDevice));
	CudaErode((cudaArray *)(*tmpIma1)->GetCudaPtr(),(*target)->GetCudaPtr(), 
				(cudaArray *)(*tmpIma1)->GetCudaPtr(), width, height, bpp, nPasses);
#endif
	TIPReleaseImage(tmpIma1);
}

//---------------------------------------------------------------------------------------------
//	Open (in MinMax_Kernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::Open(int nPasses, TIPImage **src, TIPImage **target)
{
	TIPImage	**tmpIma1;
	int			width, height, bpp;

	if ((!src) || (!target)) return;
	if ((nPasses<= 0) || (nPasses>MAX_KERNELRADIUS)) return;
	if (!(*src)->SameSpecs(*target)) return;	// device of images should be the same
	(*src)->GetPackedDimensions(&width, &height, &bpp);
#ifdef MINMAX_SHARED
	TIPImage	**tmpIma2;
	if ((*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	tmpIma1 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	tmpIma2 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	CudaErode((*src)->GetCudaPtr(),(*tmpIma1)->GetCudaPtr(), (*tmpIma2)->GetCudaPtr(),
				width, height, bpp, nPasses);
	CudaDilate((*tmpIma1)->GetCudaPtr(),(*target)->GetCudaPtr(), (*tmpIma2)->GetCudaPtr(),
				width, height, bpp, nPasses);
	TIPReleaseImage(tmpIma2);
#else
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	tmpIma1 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), TRUE);
	CudaErode((cudaArray *)(*src)->GetCudaPtr(),(*target)->GetCudaPtr(), 
				(cudaArray *)(*tmpIma1)->GetCudaPtr(), width, height, bpp, nPasses);
	CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)(*tmpIma1)->GetCudaPtr(), 0, 0, 
				(*target)->GetCudaPtr(), (*tmpIma1)->GetSize(), cudaMemcpyDeviceToDevice));
	CudaDilate((cudaArray *)(*tmpIma1)->GetCudaPtr(),(*target)->GetCudaPtr(), 
				(cudaArray *)(*tmpIma1)->GetCudaPtr(), width, height, bpp, nPasses);
#endif
	TIPReleaseImage(tmpIma1);
}

//---------------------------------------------------------------------------------------------
//	Subtract (in MathKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::Subtract(TIPImage **src1, TIPImage **src2, TIPImage **target)
{
	int	width, height, bpp;

	if ((!src1)  || (!src2) || (!target)) return;
	if (!(*src1)->SameSpecs(*src2, *target)) return;
	if (!(*src1)->IsCudaArray() || !(*src2)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src1)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src1)->GetDevice()));
	CudaSubtract((cudaArray *)(*src1)->GetCudaPtr(), (cudaArray *)(*src2)->GetCudaPtr(), 
				(*target)->GetCudaPtr(), width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	Diff (in MathKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::Diff(TIPImage **src1, TIPImage **src2, TIPImage **target)
{
	int	width, height, bpp;

	if ((!src1)  || (!src2) || (!target)) return;
	if (!(*src1)->SameSpecs(*src2, *target)) return;
	if (!(*src1)->IsCudaArray() || !(*src2)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src1)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src1)->GetDevice()));
	CudaDiff((cudaArray *)(*src1)->GetCudaPtr(), (cudaArray *)(*src2)->GetCudaPtr(), 
				(*target)->GetCudaPtr(), width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	Add (in MathKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::Add(TIPImage **src1, TIPImage **src2, TIPImage **target)
{
	int	width, height, bpp;

	if ((!src1)  || (!src2) || (!target)) return;
	if (!(*src1)->SameSpecs(*src2, *target)) return;
	if (!(*src1)->IsCudaArray() || !(*src2)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src1)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src1)->GetDevice()));
	CudaAdd((cudaArray *)(*src1)->GetCudaPtr(), (cudaArray *)(*src2)->GetCudaPtr(), 
				(*target)->GetCudaPtr(), width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	Multiply (in MathKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::Multiply(TIPImage **src1, TIPImage **src2, TIPImage **target)
{
	int	width, height, bpp;

	if ((!src1)  || (!src2) || (!target)) return;
	if (!(*src1)->SameSpecs(*src2, *target)) return;
	if (!(*src1)->IsCudaArray() || !(*src2)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src1)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src1)->GetDevice()));
	CudaMultiply((cudaArray *)(*src1)->GetCudaPtr(), (cudaArray *)(*src2)->GetCudaPtr(), 
				(*target)->GetCudaPtr(), width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	Divide (in MathKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::Divide(TIPImage **src1, TIPImage **src2, TIPImage **target)
{
	int	width, height, bpp;

	if ((!src1)  || (!src2) || (!target)) return;
	if (!(*src1)->SameSpecs(*src2, *target)) return;
	if (!(*src1)->IsCudaArray() || !(*src2)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src1)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src1)->GetDevice()));
	CudaDivide((cudaArray *)(*src1)->GetCudaPtr(), (cudaArray *)(*src2)->GetCudaPtr(), 
				(*target)->GetCudaPtr(), width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	Mod (in MathKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::Mod(TIPImage **src1, TIPImage **src2, TIPImage **target)
{
	int	width, height, bpp;

	if ((!src1)  || (!src2) || (!target)) return;
	if (!(*src1)->SameSpecs(*src2, *target)) return;
	if (!(*src1)->IsCudaArray() || !(*src2)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src1)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src1)->GetDevice()));
	CudaMod((cudaArray *)(*src1)->GetCudaPtr(), (cudaArray *)(*src2)->GetCudaPtr(), 
				(*target)->GetCudaPtr(), width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	SubtractConst (in MathKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::SubtractConst (TIPImage **src, float val, TIPImage **target)
{
	int	width, height, bpp;

	if ((!src) || (!target)) return;
	if (!(*src)->SameSpecs(*target)) return;
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaSubtractConst((cudaArray *)(*src)->GetCudaPtr(), (*target)->GetCudaPtr(), val, 
					  width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	AddConst (in MathKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::AddConst (TIPImage **src, float val, TIPImage **target)
{
	int	width, height, bpp;

	if ((!src) || (!target)) return;
	if (!(*src)->SameSpecs(*target)) return;
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaAddConst((cudaArray *)(*src)->GetCudaPtr(), (*target)->GetCudaPtr(), val,
				width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	MultiplyConst (in MathKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::MultiplyConst (TIPImage **src, float val, TIPImage **target)
{
	int	width, height, bpp;

	if ((!src) || (!target)) return;
	if (!(*src)->SameSpecs(*target)) return;
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaMultiplyConst((cudaArray *)(*src)->GetCudaPtr(), (*target)->GetCudaPtr(), val, 
					  width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	DivideConst (in MathKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::DivideConst (TIPImage **src, float val, TIPImage **target)
{
	int	width, height, bpp;

	if ((!src) || (!target)) return;
	if (!(*src)->SameSpecs(*target)) return;
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaDivideConst((cudaArray *)(*src)->GetCudaPtr(), (*target)->GetCudaPtr(), val, 
					width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	Thresh (in MathKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::Thresh (TIPImage **src, float thresh, TIPImage **target)
{
	int	width, height, bpp;

	if ((!src) || (!target)) return;
	if (!(*src)->SameSpecs(*target)) return;
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaThresh((cudaArray *)(*src)->GetCudaPtr(), (*target)->GetCudaPtr(), thresh, 
					width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	ComponentSplit (in ColourKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::ComponentSplit (TIPImage **src, TIPImage **target1, TIPImage **target2, 
						  TIPImage **target3)
{
	int	width, height, bppSrc, wTarget, hTarget, bppTarget;

	if (!(*target1)->SameSpecs(*target2, *target3)) return;
	if (!(*src)->IsCudaArray() || (*target1)->IsCudaArray() || 
		(*target2)->IsCudaArray() || (*target3)->IsCudaArray()) return; 
	(*src)->GetPackedDimensions(&width, &height, &bppSrc);
	(*target1)->GetPackedDimensions(&wTarget, &hTarget, &bppTarget);
	if ((wTarget != width) || (hTarget != height)) return;
	if ((bppSrc != 24) && (bppSrc != 48)) return;
	if (((bppSrc==24) && (bppTarget != 8) && (bppTarget != 24)) ||
		((bppSrc==48) && (bppTarget != 16) && (bppTarget != 48))) return;
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaComponentSplit((cudaArray *)(*src)->GetCudaPtr(),(*target1)->GetCudaPtr(),
		(*target2)->GetCudaPtr(), (*target3)->GetCudaPtr(), width, height, bppSrc, bppTarget);
}

//---------------------------------------------------------------------------------------------
//	ComponentJoin (in ColourKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::ComponentJoin (TIPImage **src1, TIPImage **src2, TIPImage **src3, TIPImage **target)
{
	int	width, height, bppSrc, wTarget, hTarget, bppTarget;

	if (!(*src1)->SameSpecs(*src2, *src3)) return;
	if (!(*src1)->IsCudaArray() || !(*src2)->IsCudaArray() || 
		!(*src3)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src1)->GetPackedDimensions(&width, &height, &bppSrc);
	(*target)->GetPackedDimensions(&wTarget, &hTarget, &bppTarget);
	if ((wTarget != width) || (hTarget != height)) return;
	if ((bppTarget != 24) && (bppTarget != 48)) return;
	if (((bppTarget==24) && (bppSrc != 8) && (bppSrc != 24)) ||
		((bppTarget==48) && (bppSrc != 16) && (bppSrc != 48))) return;
	CUDA_SAFE_CALL(	cudaSetDevice((*src1)->GetDevice()));
	CudaComponentJoin((cudaArray *)(*src1)->GetCudaPtr(),(cudaArray *)(*src2)->GetCudaPtr(),
		(cudaArray *)(*src3)->GetCudaPtr(), (*target)->GetCudaPtr(), width, height, bppSrc);
}

//---------------------------------------------------------------------------------------------
//	ComponentThreshold (in ColourKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::ComponentThreshold (float Rmin, float Rmax, float Gmin, float Gmax, 
									float Bmin, float Bmax, TIPImage **src, TIPImage **target)
{
	int	width, height, bpp;

	if ((!src) || (!target)) return;
	if (!(*src)->SameSpecs(*target)) return;
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	if ((bpp != 24) && (bpp != 48)) return;
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaInitThresholdConst(Rmin, Rmax, Gmin, Gmax, Bmin, Bmax); 
	CudaComponentThreshold ((cudaArray *)(*src)->GetCudaPtr(),(*target)->GetCudaPtr(),
		width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	ComponentBlend (in ColourKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::ComponentBlend (TIPImage **src1, float a1, TIPImage **src2, float a2, TIPImage **src3, 
						  float a3, TIPImage **target)
{
	int	width, height, bppSrc, wTarget, hTarget, bppTarget;

	if (!(*src1)->SameSpecs(*src2, *src3)) return;
	if (!(*src1)->IsCudaArray() || !(*src2)->IsCudaArray() || 
		!(*src3)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src1)->GetPackedDimensions(&width, &height, &bppSrc);
	(*target)->GetPackedDimensions(&wTarget, &hTarget, &bppTarget);
	if ((wTarget != width) || (hTarget != height)) return;
	if ((bppTarget != 24) && (bppTarget != 48)) return;
	if (((bppTarget==24) && (bppSrc != 8) && (bppSrc != 24)) ||
		((bppTarget==48) && (bppSrc != 16) && (bppSrc != 48))) return;
	CUDA_SAFE_CALL(	cudaSetDevice((*src1)->GetDevice()));
	CudaInitBlendConst(a1, a2, a3); 
	CudaComponentBlend((cudaArray *)(*src1)->GetCudaPtr(),(cudaArray *)(*src2)->GetCudaPtr(),
		(cudaArray *)(*src3)->GetCudaPtr(), (*target)->GetCudaPtr(), width, height, bppSrc);
}

//---------------------------------------------------------------------------------------------
//	ComposedBlend (in ColourKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::ComposedBlend(TIPImage **src, float a1, float a2, float a3, TIPImage **target)
{
	int	width, height, bpp;

	if ((!src) || (!target)) return;
	if (!(*src)->SameSpecs(*target)) return;
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	if ((bpp != 24) && (bpp != 48)) return;
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaInitBlendConst(a1, a2, a3); 
	CudaComposedBlend((cudaArray *)(*src)->GetCudaPtr(),(*target)->GetCudaPtr(), width, height, 
		bpp);
}

//---------------------------------------------------------------------------------------------
//	RGBHSI (in ColourKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::RGBHSI (TIPImage **src, TIPImage **target)
{
	int	width, height, bpp;

	if ((!src) || (!target)) return;
	if (!(*src)->SameSpecs(*target)) return;
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	if ((bpp != 24) && (bpp != 48)) return;
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaRGBHSI ((cudaArray *)(*src)->GetCudaPtr(),(*target)->GetCudaPtr(),
		width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	HSIRGB (in ColourKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::HSIRGB( TIPImage **src, TIPImage **target)
{
	int	width, height, bpp;

	if ((!src) || (!target)) return;
	if (!(*src)->SameSpecs(*target)) return;
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	if ((bpp != 24) && (bpp != 48)) return;
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaHSIRGB ((cudaArray *)(*src)->GetCudaPtr(),(*target)->GetCudaPtr(),
		width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	ColourDeconv (in ColourKernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::InitColourDeconvMatrix (double *vc1, double *vc2, double *vc3, float4 *qVec)
{
	double	MODx[3], MODy[3], MODz[3], cosx[3], cosy[3], cosz[3], len[3];
	double	leng, A, V, C;
	int		i;

	MODx[0]=vc1[0]; MODx[1]=vc2[0]; MODx[2]=vc3[0];
	MODy[0]=vc1[1]; MODy[1]=vc2[1]; MODy[2]=vc3[1];
	MODz[0]=vc1[2]; MODz[1]=vc2[2]; MODz[2]=vc3[2];
	for (i = 0; i < 3; i++)
	{
		//normalise vector length
		cosx[i] = cosy[i] = cosz[i] = 0.0;
		len[i] = sqrt(MODx[i] * MODx[i] + MODy[i] * MODy[i] + MODz[i] * MODz[i]);
		if (len[i] != 0.0)
		{
			cosx[i] = MODx[i] / len[i];
			cosy[i] = MODy[i] / len[i];
			cosz[i] = MODz[i] / len[i];
		}
	}

	if ((cosx[1] == 0.0) && (cosy[1] == 0.0)&& (cosz[1] == 0.0)) // 2nd colour unspecified
	{
		cosx[1] = cosz[0];
		cosy[1] = cosx[0];
		cosz[1] = cosy[0];
	}

	if ((cosx[2] == 0.0) && (cosy[2] == 0.0) && (cosz[2] == 0.0)) // 3th colour unspecified
	{
		if ((cosx[0] * cosx[0] + cosx[1] * cosx[1]) > 1) cosx[2] = 0.0;
			else cosx[2] = sqrt(1.0 - (cosx[0] * cosx[0]) - (cosx[1] * cosx[1]));
		if ((cosy[0] * cosy[0] + cosy[1] * cosy[1]) > 1) cosy[2] = 0.0;
			else cosy[2] = sqrt(1.0 - (cosy[0] * cosy[0]) - (cosy[1] * cosy[1]));
       if ((cosz[0] * cosz[0] + cosz[1] * cosz[1]) > 1) cosz[2] = 0.0;
			else cosz[2] = sqrt(1.0 - (cosz[0] * cosz[0]) - (cosz[1] * cosz[1]));
	}
	leng = sqrt(cosx[2] * cosx[2] + cosy[2] * cosy[2] + cosz[2] * cosz[2]);

	cosx[2] /= leng;
	cosy[2] /= leng;
	cosz[2] /= leng;

	for (i = 0; i < 3; i++)
	{
		if (cosx[i] == 0.0) cosx[i] = 0.001;
		if (cosy[i] == 0.0) cosy[i] = 0.001;
		if (cosz[i] == 0.0) cosz[i] = 0.001;
	}
		
	//matrix inversion
	A = cosy[1] - cosx[1] * cosy[0] / cosx[0];
	V = cosz[1] - cosx[1] * cosz[0] / cosx[0];
	C = cosz[2] - cosy[2] * V/A + cosx[2] * (V/A * cosy[0] / cosx[0] - cosz[0] / cosx[0]);

	qVec[0].z = (float)((-cosx[2] / cosx[0] - cosx[2] / A * cosx[1] / cosx[0] * cosy[0] / cosx[0] + 
				cosy[2] / A * cosx[1] / cosx[0]) / C);
	qVec[0].y = (float)(-qVec[0].z * V / A - cosx[1] / (cosx[0] * A));
	qVec[0].x = (float)(1.0 / cosx[0] - qVec[0].y * cosy[0] / cosx[0] - qVec[0].z * cosz[0] / cosx[0]);
	qVec[1].z = (float)((-cosy[2] / A + cosx[2] / A * cosy[0] / cosx[0]) / C);
	qVec[1].y = (float)(-qVec[1].z * V / A + 1.0 / A);
	qVec[1].x = (float)(-qVec[1].y * cosy[0] / cosx[0] - qVec[1].z * cosz[0] / cosx[0]);
	qVec[2].z = (float)(1.0 / C);
	qVec[2].y = (float)(-qVec[2].z * V / A);
	qVec[2].x = (float)(-qVec[2].y * cosy[0] / cosx[0] - qVec[2].z * cosz[0] / cosx[0]);
	qVec[0].w = qVec[1].w = qVec[2].w = (float)0.0;	
}

void TIP::ColourDeconv(TIPImage **src, double *v1, double *v2, double *v3, TIPImage **target)
{
	float4 qVec[3];
	int	width, height, bpp;

	if ((!src) || (!target)) return;
	if (!(*src)->SameSpecs(*target)) return;
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; 
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	if ((bpp != 24) && (bpp != 48)) return;
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	InitColourDeconvMatrix(v1, v2, v3, qVec);
	CudaColourDeconv((cudaArray *)(*src)->GetCudaPtr(),(*target)->GetCudaPtr(), width, height, 
		bpp, qVec);
}

//---------------------------------------------------------------------------------------------
//	Median (in Median_Kernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::Median(int nPasses, TIPImage **src, TIPImage **target)
{
	TIPImage	**tmpIma;
	int			width, height, bpp;

	if ((!src) || (!target)) return;
	if ((nPasses<= 0) || (nPasses>MAX_KERNELRADIUS)) return;
	if (!(*src)->SameSpecs(*target)) return;	// device of images should be the same
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	tmpIma = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), TRUE);
	CudaMedian((cudaArray *)(*src)->GetCudaPtr(),(*target)->GetCudaPtr(), 
				(cudaArray *)(*tmpIma)->GetCudaPtr(), width, height, bpp, nPasses);
	TIPReleaseImage(tmpIma);
}

//---------------------------------------------------------------------------------------------
//	Laplace (in Laplace_Kernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::Laplace(TIPImage **src, TIPImage **target)
{
	int			width, height, bpp;

	if ((!src) || (!target)) return;
	if (!(*src)->SameSpecs(*target)) return;	// device of images should be the same
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaLaplace((cudaArray *)(*src)->GetCudaPtr(),(*target)->GetCudaPtr(), width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	Mean (in Mean_Kernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::Mean(TIPImage **src, TIPImage **target)
{
	int			width, height, bpp;

	if ((!src) || (!target)) return;
	if (!(*src)->SameSpecs(*target)) return;	// device of images should be the same
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaMean((cudaArray *)(*src)->GetCudaPtr(),(*target)->GetCudaPtr(), width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	Kuwahara (in Kuwahara_Kernel.cu)
//---------------------------------------------------------------------------------------------
void TIP::Kuwahara(int filterHW, TIPImage **src, TIPImage **target)
{
	TIPImage	**tmpMean, **tmpMeanC, **tmpVaria, **tmpVariaC;
	int			width, height, bpp;

	if ((!src) || (!target)) return;
	if (!(*src)->SameSpecs(*target)) return;	// device of images should be the same
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	if (!(*src)->IsCudaArray() || (*target)->IsCudaArray()) return; //images - no CudaArrays
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	if (filterHW >= 2) filterHW=2;
		else filterHW=1;
	if ((bpp==8) || (bpp==16))
	{
		tmpMean = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
								(*src)->GetOverlap(), 32, FALSE);
		tmpMeanC = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
								(*src)->GetOverlap(), 32, TRUE);
		tmpVaria = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
								(*src)->GetOverlap(), 32, FALSE);
		tmpVariaC = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
								(*src)->GetOverlap(), 32, TRUE);
	}
	else
	{
		tmpMean = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
								(*src)->GetOverlap(), 128, FALSE);
		tmpMeanC = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
								(*src)->GetOverlap(), 128, TRUE);
		tmpVaria = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
								(*src)->GetOverlap(), 128, FALSE);
		tmpVariaC = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
								(*src)->GetOverlap(), 128, TRUE);
	}
	CudaInitFilterRadius(filterHW);
	CudaKuwaharaPass1((cudaArray *)(*src)->GetCudaPtr(), (*tmpMean)->GetCudaPtr(),
					  (*tmpVaria)->GetCudaPtr(), width, height, bpp);
	Copy(tmpMean, tmpMeanC);
	Copy(tmpVaria, tmpVariaC);
	CudaInitFilterRadius(1);
	CudaKuwaharaPass2((cudaArray *)(*tmpMeanC)->GetCudaPtr(), 
					  (cudaArray *)(*tmpVariaC)->GetCudaPtr(),
					  (*target)->GetCudaPtr(), width, height, bpp);
	TIPReleaseImage(tmpMean);
	TIPReleaseImage(tmpMeanC);
	TIPReleaseImage(tmpVaria);
	TIPReleaseImage(tmpVariaC);
}

//---------------------------------------------------------------------------------------------
//	Convert
//---------------------------------------------------------------------------------------------
void TIP::Convert(TIPImage **src, TIPImage **dest, float normFactor)
{
	BOOL	isCuda, isCuda1;
	int		dev, w, h, bpp, overl;
	int		dev1, w1, h1, bpp1, overl1;

	if ((!src) || (!dest)) return;
	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
	(*dest)->GetImageInfo(&dev1, &w1, &h1, &overl1, &bpp1, &isCuda1);
	if ((dev != dev1) || (w!= w1) || (h != h1) || !isCuda || isCuda1 ||
		(overl != 0) || (overl1 != 0)) return;
	if ((bpp != 8) && (bpp != 16) && (bpp != 24) && (bpp != 32) && (bpp != 48) &&
		(bpp != 64)) return;
	if ((bpp1 != 8) && (bpp1 != 16) && (bpp1 != 24) && (bpp1 != 32) && (bpp1 != 48) &&
		(bpp1 != 64)) return;
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaConvert((cudaArray *)(*src)->GetCudaPtr(), (*dest)->GetCudaPtr(), w, h, bpp, bpp1,
				normFactor);
}

//---------------------------------------------------------------------------------------------
//	UnpackGrey
//---------------------------------------------------------------------------------------------
void TIP::UnpackGrey(TIPImage **src, TIPImage **dest)
{
	BOOL	isCuda, isCuda1;
	int		dev, w, h, bpp, overl, wP, hP, bppP;
	int		dev1, w1, h1, bpp1, overl1;
	float	corrTexWidth, corrTexHeight;

	if ((!src) || (!dest)) return;
	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
	(*dest)->GetImageInfo(&dev1, &w1, &h1, &overl1, &bpp1, &isCuda1);
	if (((bpp != 8) && (bpp != 16)) || (dev != dev1) || (w!= w1) || 
		(h != h1) || !isCuda || isCuda1 || (overl==0) || (overl1 != 0)) return;
	if ((bpp == 8) && (bpp1 != 8) && (bpp1 != 24) && (bpp1 != 32)) return;
	if ((bpp == 16) && (bpp1 != 16) && (bpp1 != 32) && (bpp1 != 48)) return;
	(*src)->GetPackedDimensions(&wP, &hP,  &bppP);
	if (overl < 0) overl=0;
	corrTexWidth = (float)overl/(float)wP;
	corrTexHeight = (float)overl/(float)hP;
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaInitCorrOffset(corrTexWidth, corrTexHeight);
	CudaUnpackGrey((cudaArray *)(*src)->GetCudaPtr(), (*dest)->GetCudaPtr(), w1, h1, bpp, bpp1);
}

//---------------------------------------------------------------------------------------------
//	PackGrey
//---------------------------------------------------------------------------------------------
void TIP::PackGrey(TIPImage **src, TIPImage **dest)
{
	BOOL	isCuda, isCuda1;
	int		dev, w, h, bpp, overl, wP, hP, bppP;
	int		dev1, w1, h1, bpp1, overl1;
	float	corrTexWidth, corrTexHeight;

	if ((!src) || (!dest)) return;
	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
	(*dest)->GetImageInfo(&dev1, &w1, &h1, &overl1, &bpp1, &isCuda1);
	if (((bpp1 != 8) && (bpp1 != 16)) || (dev != dev1) || (w!= w1) || 
		(h != h1) || !isCuda || isCuda1 || (overl!=0) || (overl1 == 0)) return;
	if (((bpp == 8)|| (bpp==24)) && (bpp1 != 8)) return;
	if (((bpp == 16)|| (bpp==48)) && (bpp1 != 16)) return;
	(*dest)->GetPackedDimensions(&wP, &hP,  &bppP);
	if (overl1 < 0) overl1=0;
	corrTexWidth = (float)overl1/(float)wP;
	corrTexHeight = (float)overl1/(float)hP;
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaInitCorrOffset(corrTexWidth, corrTexHeight);
	CudaPackGrey((cudaArray *)(*src)->GetCudaPtr(), (*dest)->GetCudaPtr(), wP, hP, bpp);
}

//---------------------------------------------------------------------------------------------
// TIPImageCrop
//	- creates a new GIPImage as a Crop from the original image
//	  used by FFT to create the largest central part of the original
//	  image with a width and height which are a power of two
//---------------------------------------------------------------------------------------------
TIPImage** TIP::ImageCrop(TIPImage **src, int x0, int y0, int w0, int h0)
{
	TIPImage	**dest;
	BOOL		isCuda;
	int			dev, w, h, wp, hp, bpp, wd, hd, bppd, overl;
	float		scaleX, scaleY, offsetX, offsetY;

	if (!src)  return (NULL);
	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
	if (!isCuda || (x0+w0 > w) || (y0+h0 > h)) return (NULL);
	if (overl != 0) return (NULL);	// packed images not supported
	dest=TIPCreateImage(dev, w0, h0, 0, bpp, FALSE);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	(*src)->GetPackedDimensions(&wp, &hp,  &bpp);
	(*dest)->GetPackedDimensions(&wd, &hd,  &bppd);
	scaleX=(float)wd/(float)wp;
	scaleY=(float)hd/(float)hp;
	offsetX=(float)x0/(float)w;
	offsetY=(float)y0/(float)h;
	CudaInitScaleAndOffset(scaleX, scaleY, offsetX, offsetY);
	CudaCropAndResize((cudaArray *)(*src)->GetCudaPtr(), (*dest)->GetCudaPtr(), wd, hd, 
					  bpp, bppd);
	return(dest);
}

//---------------------------------------------------------------------------------------------
// ImageResize
//	- creates a new GIPImage as a Resize from the original image
//---------------------------------------------------------------------------------------------
TIPImage** TIP::ImageResize(TIPImage **src, int w0, int h0, int overl0)
{
	TIPImage	**dest;
	BOOL		isCuda;
	int			dev, w, h, bpp, wd, hd, bppd, overl;
	float		scaleX, scaleY, offsetX, offsetY;

	if (!src)  return (NULL);
	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
	if (!isCuda) return (NULL);
	if ((overl == 0) && (overl0 != 0)) return (NULL);	// packed images remain  als packed
	if (overl == 0) overl0=0;
	dest=TIPCreateImage(dev, w0, h0, overl0, bpp, FALSE);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	offsetX=0.0;
	offsetY=0.0;
	scaleX= 1.0;
	scaleY= 1.0;
	(*dest)->GetPackedDimensions(&wd, &hd,  &bppd);
	CudaInitScaleAndOffset(scaleX, scaleY, offsetX, offsetY);
	CudaCropAndResize((cudaArray *)(*src)->GetCudaPtr(), (*dest)->GetCudaPtr(), wd, hd, 
					  bpp, bppd);
	return(dest);
}

//---------------------------------------------------------------------------------------------
// TIPImageCropToFloat
//	- creates a new GIPImage as a Crop from the original image
//	  used by FFT to create the largest central part of the original
//	  image with a width and height which are a power of two
//---------------------------------------------------------------------------------------------
TIPImage** TIP::ImageCropToFloat(TIPImage **src, int x0, int y0, int w0, int h0)
{
	TIPImage	**dest;
	BOOL		isCuda;
	int			dev, w, h, wp, hp, bpp, wd, hd, bppd, overl;
	float		scaleX, scaleY, offsetX, offsetY;

	if (!src)  return (NULL);
	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
	if (!isCuda || (x0+w0 > w) || (y0+h0 > h)) return (NULL);
	if (overl != 0) return (NULL);	// packed images not supported
	dest=TIPCreateImage(dev, w0, h0, 0, 32, FALSE);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	(*src)->GetPackedDimensions(&wp, &hp,  &bpp);
	(*dest)->GetPackedDimensions(&wd, &hd,  &bppd);
	scaleX=(float)wd/(float)wp;
	scaleY=(float)hd/(float)hp;
	offsetX=(float)x0/(float)w;
	offsetY=(float)y0/(float)h;
	CudaInitScaleAndOffset(scaleX, scaleY, offsetX, offsetY);
	CudaCropAndResize((cudaArray *)(*src)->GetCudaPtr(), (*dest)->GetCudaPtr(), wd, hd, 
					  bpp, bppd);
	return(dest);
}

//---------------------------------------------------------------------------------------------
// TIPImageCropToFloat
//	- creates a new GIPImage as a Crop from the original image
//	  used by FFT to create the largest central part of the original
//	  image with a width and height which are a power of two
//---------------------------------------------------------------------------------------------
TIPImage** TIP::ImageCropToComplex(TIPImage **src, int x0, int y0, int w0, int h0)
{
	TIPImage	**dest;
	BOOL		isCuda;
	int			dev, w, h, wp, hp, bpp, wd, hd, bppd, overl;
	float		scaleX, scaleY, offsetX, offsetY;

	if (!src)  return (NULL);
	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
	if (!isCuda || (x0+w0 > w) || (y0+h0 > h)) return (NULL);
	if (overl != 0) return (NULL);	// packed images not supported
	dest=TIPCreateImage(dev, w0, h0, 0, 64, FALSE);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	(*src)->GetPackedDimensions(&wp, &hp,  &bpp);
	(*dest)->GetPackedDimensions(&wd, &hd,  &bppd);
	scaleX=(float)wd/(float)wp;
	scaleY=(float)hd/(float)hp;
	offsetX=(float)x0/(float)w;
	offsetY=(float)y0/(float)h;
	CudaInitScaleAndOffset(scaleX, scaleY, offsetX, offsetY);
	CudaCropAndResize((cudaArray *)(*src)->GetCudaPtr(), (*dest)->GetCudaPtr(), wd, hd, 
					  bpp, bppd);
	return(dest);
}

//---------------------------------------------------------------------------------------------
// DistanceMap
//---------------------------------------------------------------------------------------------
void TIP::DistanceMap(TIPImage **src, TIPImage **dest, BOOL EDMOnly,  BOOL IsWhiteBackground, 
					  float NormFactor)
{
	void		*pResult;
	TIPImage	**tmpIma1, **tmpIma2;
	BOOL		isCuda, isCuda1;
	int			dev, w, h, bpp, overl, wP, hP, bppP;
	int			dev1, w1, h1, bpp1, overl1;

	if ((!src) || (!dest)) return;
	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
	(*dest)->GetImageInfo(&dev1, &w1, &h1, &overl1, &bpp1, &isCuda1);
	if (((bpp != 8) && (bpp != 16)) || (dev != dev1) || (w!= w1) || 
		(h != h1) || !isCuda || isCuda1 || (overl!=0) || (overl1 != 0)) return;
	if ((bpp == 8) && (bpp1 != 24)) return;
	if ((bpp == 16) && (bpp1 != 48)) return;
	tmpIma1=TIPCreateImage(dev, w, h, 0, 128, FALSE);
	tmpIma2=TIPCreateImage(dev, w, h, 0, 128, FALSE);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	(*src)->GetPackedDimensions(&wP, &hP,  &bppP);
	CudaPrepareEDM((cudaArray *)(*src)->GetCudaPtr(), (*tmpIma1)->GetCudaPtr(), wP, hP, bppP,
		(int)IsWhiteBackground);
	CudaEDMTransform((*tmpIma1)->GetCudaPtr(), (*tmpIma2)->GetCudaPtr(), &pResult, wP, hP);
	CudaFinishEDM(pResult, (*dest)->GetCudaPtr(), wP, hP, bpp1, (int)EDMOnly,
		NormFactor);
	TIPReleaseImage(tmpIma2);
	TIPReleaseImage(tmpIma1);
}

//---------------------------------------------------------------------------------------------
// CannyEdge
//---------------------------------------------------------------------------------------------
void TIP::CannyEdge(TIPImage **src, TIPImage **target, float thresholdLow, float thresholdHigh)
{
	TIPImage	**tmpIma1, **tmpIma2, **tmpIma3;
	BOOL		isCuda;
	int			dev, w, h, overl, bpp;

	if ((!src) || (!target)) return;
	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
	if (!(*src)->SameSpecs(*target)) return;	// device of images should be the same
	if (!isCuda || (*target)->IsCudaArray() || (overl != 0)) return;
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	tmpIma1=TIPCreateImage(dev, w, h, 0, 32, FALSE);
	tmpIma2=TIPCreateImage(dev, w, h, 0, 32, FALSE);
	tmpIma3=TIPCreateImage(dev, w, h, 0, 32, FALSE);
	Convert(src, tmpIma1);
	CudaCannyEdge((*tmpIma1)->GetCudaPtr(), (*tmpIma2)->GetCudaPtr(), (*tmpIma3)->GetCudaPtr(), 
				  (*target)->GetCudaPtr(), w, h, bpp, thresholdLow, thresholdHigh);
	TIPReleaseImage(tmpIma3);
	TIPReleaseImage(tmpIma2);
	TIPReleaseImage(tmpIma1);
}

//---------------------------------------------------------------------------------------------
// NNDeconv
//---------------------------------------------------------------------------------------------
void TIP::NNDeconv(int FilterHW, float Sigma, float Amount, int nrImages, int W, int H, 
				   int BPP, BYTE **ppImages, BYTE **ppDestImages)
{
	int			Device, overlap, wp, hp, bppP;
	TIPImage	**src0, **src1, **src2, **filsrc0, **filsrc1, **filsrc2, **dest;

	Device=0;
	overlap=0;
	// create GPU images for processing
	src0=TIPCreateImage(Device, W, H, overlap, BPP, false);
	src1=TIPCreateImage(Device, W, H, overlap, BPP, false);
	src2=TIPCreateImage(Device, W, H, overlap, BPP, false);
	if (FilterHW > 0)
	{
		filsrc0=TIPCreateImage(Device, W, H, overlap, BPP, false);
		filsrc1=TIPCreateImage(Device, W, H, overlap, BPP, false);
		filsrc2=TIPCreateImage(Device, W, H, overlap, BPP, false);
	}
	dest=TIPCreateImage(Device, W, H, overlap, BPP, false);
	(*src0)->GetPackedDimensions(&wp, &hp,  &bppP);
	CudaInitAmount(Amount);
	int idx=0;
	for (int i=0; i<nrImages; i++)
	{
		if (i==0)
		{
			switch (BPP)
			{
				case 8:
						PutImage(src0, (BYTE *)ppImages[i]);
						break;
				case 16:
						PutImage(src0, (unsigned short *)ppImages[i]);
						break;
				case 24:
						PutImage(src0, (RGBTRIPLE *)ppImages[i]);
						break;
				case 48:
						PutImage(src0, (RGB16BTRIPLE *)ppImages[i]);
						break;
			}
			if (FilterHW>0) GaussianBlur(FilterHW, Sigma, src0, filsrc0);
		}
		if (i<(nrImages-1))
		{
			switch (BPP)
			{
				case 8:
						switch (idx)
						{
							case 0:
									PutImage(src1, (BYTE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src1, filsrc1);
									break;
							case 1:
									PutImage(src2, (BYTE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src2, filsrc2);
									break;
							case 2:
									PutImage(src0, (BYTE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src0, filsrc0);
									break;
						}
						break;
				case 16:
						switch (idx)
						{
							case 0:
									PutImage(src1, (unsigned short *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src1, filsrc1);
									break;
							case 1:
									PutImage(src2, (unsigned short *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src2, filsrc2);
									break;
							case 2:
									PutImage(src0, (unsigned short *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src0, filsrc0);
									break;
						}
						break;
				case 24:
						switch (idx)
						{
							case 0:
									PutImage(src1, (RGBTRIPLE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src1, filsrc1);
									break;
							case 1:
									PutImage(src2, (RGBTRIPLE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src2, filsrc2);
									break;
							case 2:
									PutImage(src0, (RGBTRIPLE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src0, filsrc0);
									break;
						}
						break;
				case 48:
						switch (idx)
						{
							case 0:
									PutImage(src1, (RGB16BTRIPLE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src1, filsrc1);
									break;
							case 1:
									PutImage(src2, (RGB16BTRIPLE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src2, filsrc2);
									break;
							case 2:
									PutImage(src0, (RGB16BTRIPLE *)ppImages[i+1]);
									if (FilterHW > 0) GaussianBlur(FilterHW, Sigma, src0, filsrc0);
									break;
						}
						break;
			}
		}
		if (i==0)
		{
			if (FilterHW > 0) 
				CudaNearestNeigbourDeconv((*src0)->GetCudaPtr(), (*filsrc1)->GetCudaPtr(), 
										  (*filsrc1)->GetCudaPtr(), (*dest)->GetCudaPtr(),
										  wp, hp, bppP);
			else 
				CudaNearestNeigbourDeconv((*src0)->GetCudaPtr(), (*src1)->GetCudaPtr(), 
										  (*src1)->GetCudaPtr(), (*dest)->GetCudaPtr(),
										  wp, hp, bppP);
		}
		else if (i==(nrImages-1))
		{
			switch (idx)
			{
				case 0:
						if (FilterHW > 0) 
							CudaNearestNeigbourDeconv((*src0)->GetCudaPtr(), (*filsrc2)->GetCudaPtr(), 
													  (*filsrc2)->GetCudaPtr(), (*dest)->GetCudaPtr(),
													  wp, hp, bppP);
						else 
							CudaNearestNeigbourDeconv((*src0)->GetCudaPtr(), (*src2)->GetCudaPtr(),
													  (*src2)->GetCudaPtr(), (*dest)->GetCudaPtr(),
													  wp, hp, bppP);
						break;
				case 1:
						if (FilterHW > 0) 
							CudaNearestNeigbourDeconv((*src1)->GetCudaPtr(), (*filsrc0)->GetCudaPtr(), 
													  (*filsrc0)->GetCudaPtr(), (*dest)->GetCudaPtr(),
													  wp, hp, bppP);
						else 
							CudaNearestNeigbourDeconv((*src1)->GetCudaPtr(), (*src0)->GetCudaPtr(), 
													  (*src0)->GetCudaPtr(), (*dest)->GetCudaPtr(),
													  wp, hp, bppP);
						break;
				case 2:
						if (FilterHW > 0) 
							CudaNearestNeigbourDeconv((*src2)->GetCudaPtr(), (*filsrc1)->GetCudaPtr(), 
													  (*filsrc1)->GetCudaPtr(), (*dest)->GetCudaPtr(),
													  wp, hp, bppP);
						else 
							CudaNearestNeigbourDeconv((*src2)->GetCudaPtr(), (*src1)->GetCudaPtr(), 
													  (*src1)->GetCudaPtr(), (*dest)->GetCudaPtr(),
													  wp, hp, bppP);
						break;
			}
		}
		else
		{
			switch (idx)
			{
				case 0:
						if (FilterHW > 0) 
							CudaNearestNeigbourDeconv((*src0)->GetCudaPtr(), (*filsrc2)->GetCudaPtr(), 
													  (*filsrc1)->GetCudaPtr(), (*dest)->GetCudaPtr(),
													  wp, hp, bppP);
						else 
							CudaNearestNeigbourDeconv((*src0)->GetCudaPtr(), (*src2)->GetCudaPtr(), 
													  (*src1)->GetCudaPtr(), (*dest)->GetCudaPtr(),
													  wp, hp, bppP);
						break;
				case 1:
						if (FilterHW > 0) 
							CudaNearestNeigbourDeconv((*src1)->GetCudaPtr(), (*filsrc0)->GetCudaPtr(), 
													  (*filsrc2)->GetCudaPtr(), (*dest)->GetCudaPtr(),
													  wp, hp, bppP);
						else 
							CudaNearestNeigbourDeconv((*src1)->GetCudaPtr(), (*src0)->GetCudaPtr(), 
													  (*src2)->GetCudaPtr(), (*dest)->GetCudaPtr(),
													  wp, hp, bppP);
						break;
				case 2:
						if (FilterHW > 0) 
							CudaNearestNeigbourDeconv((*src2)->GetCudaPtr(), (*filsrc1)->GetCudaPtr(), 
													  (*filsrc0)->GetCudaPtr(), (*dest)->GetCudaPtr(),
													  wp, hp, bppP);
						else 
							CudaNearestNeigbourDeconv((*src2)->GetCudaPtr(), (*src1)->GetCudaPtr(), 
													  (*src0)->GetCudaPtr(), (*dest)->GetCudaPtr(),
													  wp, hp, bppP);
						break;
			}
		}
		// save image
		switch (BPP)
		{
			case 8:
					GetImage(dest, (BYTE *)ppDestImages[i]);
					break;
			case 16:
					GetImage(dest, (unsigned short *)ppDestImages[i]);
					break;
			case 24:
					GetImage(dest, (RGBTRIPLE *)ppDestImages[i]);
					break;
			case 48:
					GetImage(dest, (RGB16BTRIPLE *)ppDestImages[i]);
					break;
		}
		idx +=1;
		if (idx==3) idx=0;
	}
	TIPReleaseImage(src0);
	TIPReleaseImage(src1);
	TIPReleaseImage(src2);
	if (FilterHW > 0)
	{
		TIPReleaseImage(filsrc0);
		TIPReleaseImage(filsrc1);
		TIPReleaseImage(filsrc2);
	}
	TIPReleaseImage(dest);
}

