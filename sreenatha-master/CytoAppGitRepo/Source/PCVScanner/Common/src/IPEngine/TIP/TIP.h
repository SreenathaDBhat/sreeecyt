// TIP.h : main header file for the TIP DLL
//
#ifndef TIP_H
#define TIP_H


#include "..\GIP\gipcommon.h"

#pragma once
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif

#define	MAX_CUDA_DEVICES	8
#define	TIP_MAXIMAGES		32
#define	TIP_SWAPBUFFERS		8
#define	MAX_KERNELRADIUS	30
#define IN_GREY				0
#define IN_COLOUR			1
#define FFT_MaxSize			2048
#define ROOTLOG2			0.832554611

class TIPImage;
class TIPSwapImage;

#include "cuda_runtime_api.h"


typedef enum
{
    Empty,							// Nothing here
    Released,						// Texture allocated but free for use
    InUse							// Texture in use
} TIPImageStatus;

typedef struct
{
    TIPImage        *pImage;        // Pointer to the image texture
    TIPImageStatus  Status;         // Status of the texture
} TIPStruct;


#ifdef TIP_EXPORTS
#define TIP_API __declspec(dllexport) 
#else
#define TIP_API __declspec(dllimport) 
#endif 

class TIP_API TIP
{
 	public:
		TIP();
		~TIP();
		int				Create(void);
		int				GetDeviceCount(void);
		TIPImage**		TIPCreateImage(int Device, int Width, int Height, int Overlap, int BPP, 
									   BOOL IsCudaArray);
		void			TIPReleaseImage(TIPImage **TipSource);
		void			TIPReleaseAll(void);
		void			TIPFreeImage(TIPImage **HandleTipImage);
		TIPSwapImage*	GetSwapImage(TIPImage *sourceHandle);
		BOOL			IsCudaArray(TIPImage **source);
		int				GetBpp(TIPImage **source);
		int				GetOverlap(TIPImage **source);
		void			GetImageSpecs(TIPImage **GIPSource, int *Device, int *Width, int *Height, 
									  int *Overlap, int *BPP, BOOL *CudaArrayFlg);
		BOOL			GetImage(TIPImage **targetHandle, BYTE *pImageData);
		BOOL			GetImage(TIPImage **targetHandle, unsigned short *pImageData);
		BOOL			GetImage(TIPImage **targetHandle, RGBTRIPLE *pImageData);
		BOOL			GetImage(TIPImage **targetHandle, RGB16BTRIPLE *pImageData);
		BOOL			GetImage(TIPImage **targetHandle, float *pImageData);
		BOOL			GetImageComponent(TIPImage **targetHandle, int component, BYTE *pImageData);
		BOOL			GetImageComponent(TIPImage **targetHandle, int component, 
										  unsigned short *pImageData);
		BOOL			PutImage(TIPImage **targetHandle, BYTE *pImageData);
		BOOL			PutImage(TIPImage **targetHandle, unsigned short *pImageData);
		BOOL			PutImage(TIPImage **targetHandle, RGBTRIPLE *pImageData);
		BOOL			PutImage(TIPImage **targetHandle, RGB16BTRIPLE *pImageData);
		BOOL			PutImage(TIPImage **targetHandle, float *pImageData);
		BOOL			PutImage(TIPImage **targetHandle, BYTE *pImageData, int Width, int Height, 
								 int X0, int Y0);
		BOOL			PutImage(TIPImage **targetHandle, unsigned short *pImageData, int Width, 
								 int Height, int X0, int Y0);
		BOOL			PutImage(TIPImage **targetHandle, RGBTRIPLE *pImageData, int Width, 
								 int Height, int X0, int Y0, int ColourMode);
		BOOL			PutImage(TIPImage **targetHandle, RGB16BTRIPLE *pImageData, int Width, 
								 int Height, int X0, int Y0, int ColourMode);
		void			Copy(TIPImage **src, TIPImage **target);

		// for Sobel_Kernel
		void			Sobel(TIPImage **src, TIPImage **target, float fScale=1.0);

		// for ConvolutionSeparableKernel
		void			GaussianBlur(int FilterHW, float Sigma, TIPImage **src, TIPImage **dest);
		void			Convolve(long nPasses, int FilHorzHW, float *CoeffHorz, int FilVertHW,
									float *CoeffVert, TIPImage **src, TIPImage **dest);
		void			ConvolveSym(long nPasses, int FilterHW, float *Coeff, TIPImage **src, 
									TIPImage **dest);
		void			UnsharpMask(int FliterHW, float Sigma, float Amount, TIPImage **src, 
									TIPImage **dest);
		void			NNDeconv(int FilterHW, float Sigma, float Amount, int nrImages, int W,
								 int H, int BPP, BYTE **ppImages, BYTE **ppDestImages);

		// for MinMax_Kernel
		void			Erode(int nPasses, TIPImage **src, TIPImage **target);
		void			Dilate(int nPasses, TIPImage **src, TIPImage **target);
		void			Close(int nPasses, TIPImage **src, TIPImage **target);
		void			Open(int nPasses, TIPImage **src, TIPImage **target);

		// for Math_Kernel
		void			Subtract(TIPImage **src1, TIPImage **src2, TIPImage **target);
		void			Add(TIPImage **src1, TIPImage **src2, TIPImage **target);
		void			Multiply(TIPImage **src1, TIPImage **src2, TIPImage **target);
		void			Divide(TIPImage **src1, TIPImage **src2, TIPImage **target);
		void			Diff(TIPImage **src1, TIPImage **src2, TIPImage **target);
		void			Mod(TIPImage **src1, TIPImage **src2, TIPImage **target);
		void			SubtractConst(TIPImage **src, float val, TIPImage **target);
		void			AddConst(TIPImage **src, float val, TIPImage **target);
		void			MultiplyConst(TIPImage **src, float val, TIPImage **target);
		void			DivideConst(TIPImage **src, float val, TIPImage **target);
		void			Thresh(TIPImage **src, float thresh, TIPImage **target);
		TIPImage**		ImageCrop(TIPImage **src, int x0, int y0, int w0, int h0);
		TIPImage**		ImageCropToFloat(TIPImage **src, int x0, int y0, int w0, int h0);
		TIPImage**		ImageCropToComplex(TIPImage **src, int x0, int y0, int w0, int h0);
		TIPImage**		ImageResize(TIPImage **src, int w0, int h0, int overl);
		void			UnpackGrey(TIPImage **src, TIPImage **dest);
		void			PackGrey(TIPImage **src, TIPImage **dest);

		// for Colour_Kernel
		void			ComponentSplit(TIPImage **src, TIPImage **target1, TIPImage **target2, 
									TIPImage **target3);
		void			ComponentJoin(TIPImage **src1, TIPImage **src2, TIPImage **src3, 
									TIPImage **target);
		void			ComponentThreshold(float Rmin, float Rmax, float Gmin, float Gmax, 
									float Bmin, float Bmax, TIPImage **src, TIPImage **target);
		void			ComponentBlend(TIPImage **src1, float a1, TIPImage **src2, float a2, 
									TIPImage **src3, float a3, TIPImage **target);
		void			ComposedBlend(TIPImage **src, float a1, float a2, float a3, 
									TIPImage **target);
		void			RGBHSI(TIPImage **src, TIPImage **target);
		void			HSIRGB(TIPImage **src, TIPImage **target);
		void			InitColourDeconvMatrix (double *vc1, double *vc2, double *vc3, float4 *qVec);
		void			ColourDeconv(TIPImage **src, double *vc1, double *vc2, double *vc3, 
									TIPImage **target);

		// for Convert_Kernel
		void			Convert(TIPImage **src, TIPImage **dest, float normFactor=1.0);
		
		// for Median_Kernel
		void			Median(int nPasses, TIPImage **src, TIPImage **target);

		// for Laplace_Kernel
		void			Laplace(TIPImage **src, TIPImage **target);

		// for Mean_Kernel
		void			Mean(TIPImage **src, TIPImage **target);

		// for Blend_Kernel
		int				GetMinValue(TIPImage **src);
		void			CalcModalHistogram(int nrImages, BYTE **pGipImages, int *pImageCorr);
		void			SimpleBlend(int nrImages, BYTE **pImages, TIPImage **Dest, 
							BOOL corrModeHist=TRUE);
		void			HDRBlend(int nrImages, BYTE **pImages, TIPImage **Dest, 
							BOOL corrModeHist=TRUE);
		void			RangeBlend(int nrImages, BYTE **pImages, TIPImage **Dest, 
							BOOL corrModeHis=TRUE);
		BOOL			BlendMultipleExposures( int BTechnique, int nrImages, BYTE **ppImages, 
							int width, int height, int bpp, BYTE *pOutputImage);
		void			Histogram(TIPImage **Src, long *pHist, int ColourMode);

		// for Kuwahara_Kernel
		void			Kuwahara(int filterHW, TIPImage **src, TIPImage **target);

		//FFT and FFT related (FFT_Kernel)
		TIPImage** fft2DPowerSpectrum(TIPImage **src, int bppOut);
		TIPImage** fft2D(TIPImage **src, int flag, int bppOut=64);
		BOOL	   CreateFFTButterWorthFilter(float *filtr, int w, int h, FilterType passType,  
											  double f1, double f2, double order);
		BOOL	   CreateFFTGaussianFilter(float *filtr, int w, int h, FilterType passType, 
										   double f1, double f2);
		TIPImage** fft2DButterWorth(TIPImage **SrcHandle, int filtType, double f1, double f2, 
									double order);
		TIPImage** fft2DGaussian(TIPImage **SrcHandle, int filtType, double f1, double f2);
		void	   FFTImageRegistration(TIPImage **src1, TIPImage **src2, int *xshift, 
										int *yshift, float *sn, int MaxShift);
		void	   CoreFFTImageRegistration(TIPImage **FFTsrc1, TIPImage **FFTsrc2, int *xshift, 
											int *yshift, float *sn, int MaxShift);
		void	   FFTImageStitch(BYTE *Addr1, BYTE *Addr2, int Width, int Height, int mode, 
								  int MaxShift, int *xshift, int *yshift, float *sn);
		void	   FFTImageStitch(unsigned short *Addr1, unsigned short *Addr2, int Width, 
								  int Height, int mode, int MaxShift, int *xshift, 
								  int *yshift, float *sn);
		void	   FFTImageStitch(RGBTRIPLE *Addr1, RGBTRIPLE *Addr2, int Width, int Height, 
								  int mode, int MaxShift, int *xshift, int *yshift, float *sn);
		void	   FFTImageStitch(RGB16BTRIPLE *Addr1, RGB16BTRIPLE *Addr2, int Width, int Height, 
								  int mode, int MaxShift, int *xshift, int *yshift, float *sn);
		void       CoreFFTImageStitch(TIPImage **src1, TIPImage **src2, int mode, int MaxShift, 
									 int *xshift, int *yshift, float *sn);

		// for Distance_Kernel
		void	   DistanceMap(TIPImage **src, TIPImage **dest, BOOL EDMOnly, 
							   BOOL IsWhiteBackground, float NormFactor);

		// for CannyEdge_Kernel
		void	   CannyEdge(TIPImage **src, TIPImage **dest, float thresholdLow, 
							float thresholdHigh);
	private:
		int				m_CUDADeviceCount;
		cudaDeviceProp	m_CUDADevProp[MAX_CUDA_DEVICES];
		TIPStruct       m_Images[MAX_CUDA_DEVICES][TIP_MAXIMAGES];
		TIPSwapImage*	pSwapImage[TIP_SWAPBUFFERS];	// system side texture
		long			m_SwapLastUsed[TIP_SWAPBUFFERS];
		long			m_SwapCounter;			
};


typedef TIPImage**	TIP_IMAGE;

#endif