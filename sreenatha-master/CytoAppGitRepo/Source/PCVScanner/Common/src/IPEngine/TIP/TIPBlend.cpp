#include "stdafx.h"
#include "cutil.h"
#include "TIPImage.h"
#include "TIPSwapImage.h"
#include "TIP.h"
#include "math.h"
#include "CudaTIP_Kernels.h"

typedef enum BlendTechnique
{
	Range,
	Simple,
	HDR
}BlendTechnique;

//---------------------------------------------------------------------------------------------
//	GetMinValue
//---------------------------------------------------------------------------------------------
int	TIP::GetMinValue(TIPImage **src)
{
	unsigned short	*sImageData, *s;
	BYTE			*pImageData, *p;
	int				dev, w, h, overl, bpp;
	BOOL			isCuda;
	int				i, minVal=65536;

	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
	if ((bpp==8) || (bpp==16))
	{
		if (bpp==8)
		{
			pImageData = new BYTE[w*h];
			GetImage(src, pImageData);
			p=pImageData;
			for (i=0; i<w*h; i++)
			{
				if (*p < minVal) minVal=*p;
				p +=1;
			}
			delete [] pImageData;
		}
		else
		{
			sImageData = new unsigned short[w*h];
			GetImage(src, sImageData);
			s=sImageData;
			for (i=0; i<w*h; i++)
			{
				if (*s < minVal) minVal=*s;
				s +=1;
			}
			delete [] sImageData;
		}
		return minVal;
	}
	else
		return -1;
}

//---------------------------------------------------------------------------------------------
//	Histogram
//---------------------------------------------------------------------------------------------
void TIP::Histogram(TIPImage **src, long *hist, int ColourMode)
{
	BYTE			*p, *pImageData;
	unsigned short	*s;
	int				dev, w, h, overl, bpp, i;
	BOOL			isCuda;

	if ((!src)) return;
	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
	if (bpp==8)
	{
        memset(hist, 0, sizeof(long) * 256);
		pImageData = new BYTE[w*h];
		GetImage(src, pImageData);
		p=pImageData;
		for (i=0; i<w*h; i++)
			hist[*p++] +=1;
	}
	else if (bpp==16)
	{
        memset(hist, 0, sizeof(long) * 65536);
		pImageData = new BYTE[w*h*2];
		GetImage(src, (unsigned short *)pImageData);
		s=(unsigned short *)pImageData;
		for (i=0; i<w*h; i++)
			hist[*s++] +=1;
	}
	else if (bpp==24)
	{
        memset(hist, 0, sizeof(long) * 256);
		pImageData = new BYTE[w*h*3L];
		GetImage(src, (RGBTRIPLE *)pImageData);
		p=pImageData;
		for (i=0; i<w*h; i++)
		{
			if(ColourMode& 1) hist[*(p+2)] +=1;
			if(ColourMode& 2) hist[*(p+1)] +=1;
			if(ColourMode& 4) hist[*p] +=1;
			p +=3;
		}
	}
	else if (bpp==48)
	{
        memset(hist, 0, sizeof(long) * 65536);
		pImageData = new BYTE[w*h*6L];
		GetImage(src, (RGB16BTRIPLE *)pImageData);
		s=(unsigned short *)pImageData;
		for (i=0; i<w*h; i++)
		{
			if(ColourMode& 1) hist[*(s+2)] +=1;
			if(ColourMode& 2) hist[*(s+1)] +=1;
			if(ColourMode& 4) hist[*s] +=1;
			s +=3;
		}
	}
	delete [] pImageData;
}

///////////////////////////////////////////////////////////////////////////////////////////////
//	CalcModalHistogram
//	Calc the differences of Modal values between Images for SimpleBlend, HDRblend, RangeBlend
///////////////////////////////////////////////////////////////////////////////////////////////
void TIP::CalcModalHistogram(int nrImages, BYTE **pTipImages, int *pImageCorr)
{
	long		*hist, maxH, histSize;
	int			i, j, minModal;
	TIPImage	**src;
	int			dev, w, h, overl, bpp;
	BOOL		isCuda;

	minModal=65536;
	src=(TIPImage **)pTipImages[0];
	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
	if (bpp==8)
		histSize=256;
	else
		histSize=65536;
	hist=new long[histSize];
	for (i=0; i<nrImages; i++)
	{
		src=(TIPImage **)pTipImages[i];
		Histogram(src, hist, 0);
		maxH = 0;
		for (j = 0; j < histSize; j++)
		{
			if (hist[j] > maxH)
			{
				maxH = hist[j];
				pImageCorr[i] = j;
			}
		}
		if (pImageCorr[i] < minModal) minModal=pImageCorr[i];
	}
	for (i=0; i<nrImages; i++) pImageCorr[i] -=minModal;
	delete [] hist;
}

//---------------------------------------------------------------------------------------------
//	SimpleBlend
//	- SimpleBlend entrypoint for SimpleBlendTIP
//    array of images + output image should be defined on the TIP 
//---------------------------------------------------------------------------------------------
void TIP::SimpleBlend(int nrImages, BYTE **pImages, TIPImage **Dest, BOOL corrModeHist)
{
    int			dev, width, height, overlap, bpp;
	TIPImage	**src0, **src;
	int			i, modalCorr[8];
	cudaArray*	cudaSrc[8];
	BOOL		isCuda;

	// Nr of max simultaneous input textures limited to 16
	if ((nrImages<=0) || (nrImages >8) || !pImages || !Dest) return;
	src0=(TIPImage **)pImages[0];
	if (!(*Dest)->SameSpecs(*src0)) return;
	(*src0)->GetImageInfo(&dev, &width, &height, &overlap, &bpp, &isCuda);
	if ((bpp != 8)&& (bpp!=16)) return;						// source images should be 8 or 16  bit
	cudaSrc[0]=(cudaArray *)(*src0)->GetCudaPtr();
	for (i=1; i<nrImages; i++)								// check if dimensions of all source images are the same
	{
		src=(TIPImage **)pImages[i];
		cudaSrc[i]=(cudaArray *)(*src)->GetCudaPtr();
		if ((!*src) || (!(*src)->SameSpecs(*src0))) return;
	}
	if (corrModeHist)										// calc differences in modal values based on Histogram
		CalcModalHistogram(nrImages, pImages, modalCorr);
	else
		for (i=0; i<nrImages; i++)modalCorr[i]=0;
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaInitBlendConstants(nrImages, modalCorr, bpp);
	(*src)->GetPackedDimensions(&width, &height, &bpp);
	CudaSimpleBlend(nrImages, cudaSrc, (*Dest)->GetCudaPtr(), width, height, bpp);
}

//---------------------------------------------------------------------------------------------
//	HDRBlend
//	- HDRBlend entrypoint for HDRBlendTIP
//    array of images + output image should be defined on the TIP 
//---------------------------------------------------------------------------------------------
void TIP::HDRBlend(int nrImages, BYTE **pImages, TIPImage **Dest, BOOL corrModeHist)
{
    int			dev, width, height, overlap, bpp, bppPacked;
	TIPImage	**src0, **src, **tmpIma1, **tmpIma2;
	int			i, modalCorr[8];
	float		denom;
	float4		fMinHdr;
	cudaArray*	cudaSrc[8];
	BOOL		isCuda;

	// Nr of max simultaneous input textures limited to 16
	if ((nrImages<=0) || (nrImages >8) || !pImages || !Dest) return;
	src0=(TIPImage **)pImages[0];
	if (!(*Dest)->SameSpecs(*src0)) return;
	(*src0)->GetImageInfo(&dev, &width, &height, &overlap, &bpp, &isCuda);
	if ((bpp != 8)&& (bpp!=16)) return;						// source images should be 8 or 16  bit
	cudaSrc[0]=(cudaArray *)(*src0)->GetCudaPtr();
	for (i=1; i<nrImages; i++)								// check if dimensions of all source images are the same
	{
		src=(TIPImage **)pImages[i];
		cudaSrc[i]=(cudaArray *)(*src)->GetCudaPtr();
		if ((!*src) || (!(*src)->SameSpecs(*src0))) return;
	}
	if (corrModeHist)										// calc differences in modal values based on Histogram
		CalcModalHistogram(nrImages, pImages, modalCorr);
	else
		for (i=0; i<nrImages; i++)modalCorr[i]=0;
	tmpIma1 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	tmpIma2 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), TRUE);
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	CudaInitBlendConstants(nrImages, modalCorr, bpp);
	(*src)->GetPackedDimensions(&width, &height, &bppPacked);
	CudaSimpleBlend(nrImages, cudaSrc, (*tmpIma1)->GetCudaPtr(), width, height, bppPacked);
	Copy(tmpIma1, tmpIma2);
	fMinHdr.x=(float)GetMinValue(tmpIma1);
	if (bpp==8) denom=256.;
		else denom=65536.;
	fMinHdr.x /= denom;
	fMinHdr.y=fMinHdr.z=fMinHdr.w=fMinHdr.x;
	CudaInitHDRBlendConstant(fMinHdr);
	CudaHDRBlend((cudaArray *)(*tmpIma2)->GetCudaPtr(), (*Dest)->GetCudaPtr(), width, 
				height, bppPacked);
	TIPReleaseImage(tmpIma2);
	TIPReleaseImage(tmpIma1);
}

//---------------------------------------------------------------------------------------------
//	RangeBlend
//	- RangeBlend entrypoint for RangeBlendTIP
//    array of images + output image should be defined on the TIP
//---------------------------------------------------------------------------------------------
void TIP::RangeBlend(int nrImages, BYTE **pImages, TIPImage **Dest, BOOL corrModeHist)
{
	BYTE**		pRangeImages;
	TIPImage	**tmpIma1, **tmpIma2, **tmpImaC1, **tmpImaC2, **rangeIma;
    int			dev, width, height, overlap, bpp, bppPacked;
	TIPImage	**src0, **src;
	cudaArray*	cudaSrc[8];
	cudaArray*	cudaRange[8];
	int			i, modalCorr[8], MinMaxPasses=25, SmoothPasses=5;
	float		filt[15];
	double		denom;
	BOOL		isCuda;

	// Nr of input textures limited to 8, as we need additional corresponding range images
	if ((nrImages<=0) || (nrImages >8) || !pImages || !Dest) return;
	src0=(TIPImage **)pImages[0];
	if (!(*Dest)->SameSpecs(*src0)) return;
	(*src0)->GetImageInfo(&dev, &width, &height, &overlap, &bpp, &isCuda);
	if ((bpp != 8)&& (bpp!=16)) return;				// source images should be 8 or 16  bit
	if (bpp==8) denom=256.;
	else
		denom=65536.;
	pRangeImages = (BYTE**)new long[nrImages];
	for (i=0; i<15; i++)									// init low pass filter
		filt[i]=(float)(1./15.);
	cudaSrc[0]=(cudaArray *)(*src0)->GetCudaPtr();
	for (i=1; i<nrImages; i++)								// check dimensions
	{
		src=(TIPImage **)pImages[i];
		cudaSrc[i]=(cudaArray *)(*src)->GetCudaPtr();
		if ((!*src) || (!(*src)->SameSpecs(*src0))) return;
	}
	if (corrModeHist)										// calc differences in modal values
		CalcModalHistogram(nrImages, pImages, modalCorr);
	else
		for (i=0; i<nrImages; i++)modalCorr[i]=0;

	tmpIma1 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	tmpIma2 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), FALSE);
	tmpImaC1 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), TRUE);
	tmpImaC2 = TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), TRUE);

	for (i=0; i<nrImages; i++)
	{
		if (modalCorr[i]!= 0)								// correct modal differences
		{
			SubtractConst((TIPImage **)pImages[i],(float)((double)modalCorr[i]/denom), tmpIma1);
			Copy(tmpIma1, (TIPImage **)pImages[i]);		// becomes cuda array also
			modalCorr[i]=0;
		}
	}
	for (i=0; i<nrImages; i++)
	{
		Dilate(MinMaxPasses, (TIPImage **)pImages[i], tmpIma1);
		Erode(MinMaxPasses, (TIPImage **)pImages[i], tmpIma2);
		Copy (tmpIma1, tmpImaC1);
		Copy (tmpIma2, tmpImaC2);
		Subtract(tmpImaC1, tmpImaC2, tmpIma1);
		rangeIma= TIPCreateImage((*src)->GetDevice(), (*src)->GetWidth(), (*src)->GetHeight(), 
							(*src)->GetOverlap(), (*src)->GetBpp(), TRUE);
			cudaRange[i]=(cudaArray *)(*rangeIma)->GetCudaPtr();
		ConvolveSym(SmoothPasses, 7, filt, tmpIma1, tmpIma2);
		Copy (tmpIma2, rangeIma);
		pRangeImages[i]=(BYTE *)rangeIma;
	}
	CudaInitBlendConstants(nrImages, modalCorr, bpp);
	(*src0)->GetPackedDimensions(&width, &height, &bppPacked);
	CudaRangeBlend(nrImages, cudaSrc, cudaRange, (*Dest)->GetCudaPtr(), width, height, bppPacked);

	for (i=0; i<nrImages; i++)
		TIPReleaseImage((TIPImage **)pRangeImages[i]);
	delete pRangeImages;
	TIPReleaseImage(tmpImaC2);
	TIPReleaseImage(tmpImaC1);
	TIPReleaseImage(tmpIma2);
	TIPReleaseImage(tmpIma1);
}

//---------------------------------------------------------------------------------------------
// BlendMultipleExposures
// - this function is the GIP equivalent of the same call of the CComponentBelend class
//	 images are created on the GIP and are afterwards destroyed.
//---------------------------------------------------------------------------------------------

BOOL TIP::BlendMultipleExposures(int BTechnique, int numImages, BYTE **ppImages, int width, 
								 int height, int bpp, BYTE *pOutputImage)
{
	TIPImage		**pTipOutput;
	long			*Hist, B, histSize;
    int				D, Max, i, j, Temp, numTipImages, dev;
    BYTE			*Addr;
	unsigned short	*AddrS;

	if (numImages < 2 || !ppImages || width < 2 || height < 2 || !pOutputImage)
		return FALSE;

	for (int imageIndex = 0; imageIndex < numImages; imageIndex++)
	{
		if (!ppImages[imageIndex])
			return FALSE;
	}

    int *pModes;
    B = width * height;
	if (bpp==8)
		histSize=256;
	else
		histSize=65536;

	Hist=new long[histSize];

    // Find the modal values for each image
    pModes = new int[numImages];

    for (i = 0; i < numImages; i++)
    {
        memset(Hist, 0, sizeof(long) * histSize);
		if (bpp==8)
		{
			Addr = ppImages[i];
			for (j = 0; j < B; j++) Hist[*Addr++]++;
		}
		else
		{
			AddrS= (unsigned short *)ppImages[i];
 			for (j = 0; j < B; j++) Hist[*AddrS++]++;
		}
		Max = 0;
        for (j = 0; j < histSize; j++)
            if (Hist[j] > Max)
            {
                Max = Hist[j];
                pModes[i] = j;
            }
    }
	delete [] Hist;

    // Bubble sort the modes - could be done quicker but there will only be a few to sort
    for (i = 0; i < numImages; i++)
    {
        for (j = 0; j < numImages; j++)
        {
            if (i != j)
            {
                if (pModes[i] < pModes[j])
                {
                    Temp = pModes[i];
                    pModes[i] = pModes[j];
                    pModes[j] = Temp;
                    // Also arrange the images so that the image with the lowest mode is first
                    Addr = ppImages[i];
                    ppImages[i] = ppImages[j];
                    ppImages[j] = Addr;
                }
            }
        }
    }
	
    // Now subtract the difference in modes from each of the images 
	// but don't do the first as its the lowest mode
	if (bpp==8)
	{
		for (i = 1; i < numImages; i++)
		{
			Addr = ppImages[i];
			D = pModes[i] - pModes[0];
			for (j = 0; j < B; j++)
			{
				*Addr++ = *Addr > D ? *Addr - D : 0;
			}
		}
	}
	else
	{
		for (i = 1; i < numImages; i++)
		{
			AddrS = (unsigned short *)ppImages[i];
			D = pModes[i] - pModes[0];
			for (j = 0; j < B; j++)
			{
				*AddrS++ = *AddrS > D ? *AddrS - D : 0;
			}
		}
	}
	int imageSizeBytes = width * height * sizeof(BYTE);


	BOOL *pUseImages = NULL;

//	convert the memory images into TipImages when they should be used
	numTipImages=0;
	dev=0;
	BYTE** pTipImages=(BYTE**) new long[numImages];
	if (bpp==8)
	{
		for (i = 0; i < numImages; i++)
		{
			if ((pUseImages && pUseImages[i]) || (!pUseImages))
			{
				pTipImages[numTipImages]=(BYTE *)TIPCreateImage(dev, width, height, 0, 8, true);
				PutImage((TIPImage **)pTipImages[numTipImages], ppImages[i]);
				numTipImages +=1;
			}
		}
	   pTipOutput=TIPCreateImage(dev, width, height, 0, 8, false);
	}
	else
	{
		for (i = 0; i < numImages; i++)
		{
			if ((pUseImages && pUseImages[i]) || (!pUseImages))
			{
				pTipImages[numTipImages]=(BYTE *)TIPCreateImage(dev, width, height, 0, 16, true);
				PutImage((TIPImage **)pTipImages[numTipImages], (unsigned short *)ppImages[i]);
				numTipImages +=1;
			}
		}
	   pTipOutput=TIPCreateImage(dev, width, height, 0, 16, false);
	}
   switch (BTechnique)
    {
	    case Range:
					RangeBlend(numTipImages, pTipImages, pTipOutput, FALSE);
					break;

		case Simple:
					SimpleBlend(numTipImages, pTipImages, pTipOutput, FALSE);
					break;

		case HDR:
					HDRBlend(numTipImages, pTipImages, pTipOutput, FALSE);
					break;
    }
	if (bpp==8)
		GetImage(pTipOutput, pOutputImage);
	else
		GetImage(pTipOutput, (unsigned short *)pOutputImage);
	TIPReleaseImage(pTipOutput);
	for (i=0; i<numTipImages; i++)
	{
		// destroy the GIPImages
		TIPReleaseImage((TIPImage **)pTipImages[i]);
	}
	delete [] pTipImages;
	if (pUseImages)
		delete[] pUseImages;

    delete pModes;
	return TRUE;
}

