#include "stdafx.h"
#include "cutil.h"
#include "TIPImage.h"
#include "TIP.h"
#include "math.h"
#include "cufft.h"
#include "CudaTIP_Kernels.h"

//---------------------------------------------------------------------------------------------
//	fft2DPowerSpectrum
//---------------------------------------------------------------------------------------------
TIPImage** TIP::fft2DPowerSpectrum(TIPImage **src, int bppOut)
{
	BOOL		isCuda;
	int			dev, w, h, bpp, overl;
	TIPImage	**dest;
	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
	if (bpp != 64) return NULL;
	if ((bppOut != 8) && (bppOut!= 16)) return NULL;
	dest=TIPCreateImage(dev, w, h, 0, bppOut, FALSE);

	if (!isCuda)
	{
		TIPImage **tmpImaC;
		tmpImaC=TIPCreateImage(dev, w, h, 0, 64, TRUE);
		Copy (src, tmpImaC);
		CudaPowerSpectrum((cudaArray *)(*tmpImaC)->GetCudaPtr(),(*dest)->GetCudaPtr(), w, h, 
			bppOut);
		TIPReleaseImage(tmpImaC);
	}
	else
		CudaPowerSpectrum((cudaArray *)(*src)->GetCudaPtr(),(*dest)->GetCudaPtr(), w, h, 
			bppOut);
	return dest;
}

//---------------------------------------------------------------------------------------------
//	fft2D
//---------------------------------------------------------------------------------------------
TIPImage** TIP::fft2D(TIPImage **src, int flag, int bppOut)
{
	cufftHandle	plan;
	TIPImage	**tmpIma, **tmpImaC, **dest=NULL;
	BOOL		isCuda;
	int			dev, w, h, bpp, overl, x0, y0, wnew, hnew;

	// test if source is multiple of 2 (although in principe also 3 and higher as prime number
	// is allowed for the cuda fft library

	if (!src)  return (NULL);
	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
	if (overl != 0) return NULL;

	// test if the image should be cropped
	wnew=4;
	hnew=4;
	do
    { 
		wnew *=2;
    } while(wnew <= w);
	wnew /=2;
    if (wnew > FFT_MaxSize) wnew = FFT_MaxSize;
	do
	{ 
		hnew *=2;
	} while(hnew<=h);
	hnew /=2;
	if (hnew > FFT_MaxSize) hnew = FFT_MaxSize;
	x0=(w-wnew)/2;
	y0=(h-hnew)/2;
	CUDA_SAFE_CALL(	cudaSetDevice((*src)->GetDevice()));
	if (flag < 0)
	{
		// forward fft
		if (!isCuda)
		{
			if ((w!=wnew) || (h!=hnew) || (bpp != 64))
				return NULL;
			else
				tmpIma=src;
		}
		else
		{
			if ((w != wnew) || (h != hnew))
			{
				// crop
				tmpIma=ImageCropToComplex(src, x0, y0, wnew, hnew);
			}
			else
			{
				tmpIma=TIPCreateImage(dev, wnew, hnew, 0, 64, FALSE);
				Convert(src, tmpIma);
			}
		}
		dest=TIPCreateImage(dev, wnew, hnew, 0, 64, FALSE);
		cufftPlan2d(&plan,wnew, hnew, CUFFT_C2C);
		cufftResult_t  res;
		res=cufftExecC2C(plan,(cufftComplex *)(*tmpIma)->GetCudaPtr(),
				(cufftComplex *)(*dest)->GetCudaPtr(),CUFFT_FORWARD);
		cufftDestroy(plan);
		if (tmpIma != src) TIPReleaseImage(tmpIma);
	}
	else
	{
		// reverse fft
		if ((bpp != 64) || (isCuda) || (overl != 0) || (w != wnew) || (h != hnew)) return NULL;
		if ((bppOut != 8) && (bppOut!= 16) && (bppOut != 32)) return NULL;
		cufftPlan2d(&plan,wnew, hnew, CUFFT_C2C);
		dest=TIPCreateImage(dev, wnew, hnew, 0, bppOut, FALSE);
		tmpIma=TIPCreateImage(dev, wnew, hnew, 0, 64, FALSE);
		tmpImaC=TIPCreateImage(dev, wnew, hnew, 0, 64, TRUE);
		cufftResult_t  res;
		res=cufftExecC2C(plan,(cufftComplex *)(*src)->GetCudaPtr(),
				(cufftComplex *)(*tmpIma)->GetCudaPtr(), CUFFT_INVERSE);
		Copy (tmpIma, tmpImaC);
		Convert(tmpImaC, dest, (float)1./(float)(wnew*hnew));
		TIPReleaseImage(tmpImaC);
		TIPReleaseImage(tmpIma);
		cufftDestroy(plan);
	}
	return dest;
}

//---------------------------------------------------------------------------------------------
//	CreateFFTButterWorthFilter
//---------------------------------------------------------------------------------------------
BOOL TIP::CreateFFTButterWorthFilter(float* fltr, int w, int h, FilterType passType, double f1, 
									 double f2, double order)
{
	long	nRowD2, nColD2;		// half of row/column  
	double	bigger;				// passband fraction fct of bigger axis  
	long	maxRad;             // max. sampling radius  
	double	tempL, tempH;
	long	i;
	BOOL	ok;
	
	// allocate filter vector -- radius from (u,v)=(0,0) to corners of freq plot  
	nRowD2 = w / 2;
	nColD2 = h / 2;
	bigger = (double) (nRowD2 > nColD2) ? nRowD2 : nColD2;
	maxRad = (long) sqrt ((double) (nRowD2 * nRowD2 + nColD2 * nColD2)) + 1;
	
	// determine filter vector coefficients for chosen passband type of filter  
	order *= 2.0;
	ok=TRUE;
	switch (passType) 
	{
		case LowPass:                      // low-pass  
			f1 *= bigger;
			for (i = 0; i < maxRad; i++)
				fltr[i] = (float)(1.0 / (1.0 + 0.414 * pow (((double) i / f1), order)));
			break;

		case HighPass:                      // high-pass  
			f1 *= bigger;
			for (i = 0; i < maxRad; i++)
				fltr[i] = (float)(1.0 - 1.0 / (1.0 + 0.414 * pow (((double) i / f1), order)));
			break;

		case BandPass:                      // band-pass  
			f1 *= bigger;
			f2 *= bigger;
			for (i = 0; i < maxRad; i++) 
			{
				tempL = 1.0 / (1.0 + 0.414 * pow (((double) i / f1), order));
				tempH = 1.0 - 1.0 / (1.0 + 0.414 * pow (((double) i / f2), order));
				fltr[i] = (float)(tempL * tempH);
			}
			break;

		case BandStop:                      // and band-stop  
			f1 *= bigger;
			f2 *= bigger;
			for (i = 0; i < maxRad; i++) 
			{
				tempL = 1.0 / (1.0 + 0.414 * pow (((double) i / f1), order));
				tempH = 1.0 - 1.0 / (1.0 + 0.414 * pow (((double) i / f2), order));
				fltr[i] = (float)(tempL + tempH);
			}
			break;

		default:
			ok=FALSE;
			break;
	}	
	return (ok);
}

//---------------------------------------------------------------------------------------------
// CreateGaussianFilter
//---------------------------------------------------------------------------------------------
BOOL TIP::CreateFFTGaussianFilter(float *fltr, int w, int h, FilterType passType, double f1, 
								  double f2)
{
	int		nRowD2, nColD2, i;	// half of row/column  
	double	bigger;				// passband fraction fct of bigger axis  
	double	stdDevL, stdDevH;	// std. dev.s of low and high pass fltrs 
	int		maxRad;				// max. sampling radius  
	double	tempL, tempH;
	BOOL	ok;
	
	// allocate filter vector -- radius from (u,v)=(0,0) to corners of freq plot  
	nRowD2 = w / 2;
	nColD2 = h / 2;
	bigger = (double) (nRowD2 > nColD2) ? nRowD2 : nColD2;
	maxRad = (int) sqrt ((double) (nRowD2 * nRowD2 + nColD2 * nColD2)) + 1;
	
	// determine filter vector coefficients for chosen type of filter  
	ok=TRUE;
	switch (passType) 
	{
		case LowPass:                      // low-pass  
			stdDevL = f1 / ROOTLOG2 * bigger;
			for (i = 0; i < maxRad; i++)
				fltr[i] = (float)(exp (-(double) i * (double) i / (2.0 * stdDevL * stdDevL)));
			break;

		case HighPass:                      // high-pass  
			stdDevH = f1 / ROOTLOG2 * bigger;
			for (i = 0; i < maxRad; i++)
				fltr[i] = (float)(1.0 - exp (-(double) i * (double) i / (2.0 * stdDevH * stdDevH)));
			break;

		case BandPass:                      // band-pass  
			stdDevH = f1 / ROOTLOG2 * bigger;
			stdDevL = f2 / ROOTLOG2 * bigger;
			for (i = 0; i < maxRad; i++) 
			{
				tempL = exp (-(double) i * (double) i / (2.0 * stdDevL * stdDevL));
				tempH = 1.0 - exp (-(double) i * (double) i / (2.0 * stdDevH * stdDevH));
				fltr[i] = (float)(tempL * tempH);
			}
			break;

		case BandStop:                      // and band-stop  
			stdDevL = f1 / ROOTLOG2 * bigger;
			stdDevH = f2 / ROOTLOG2 * bigger;
			for (i = 0; i < maxRad; i++) 
			{
				tempL = exp (-(double) i * (double) i / (2.0 * stdDevL * stdDevL));
				tempH = 1.0 - exp (-(double) i * (double) i / (2.0 * stdDevH * stdDevH));
				fltr[i] = (float)(tempL + tempH);
			}
			break;

		default:
			ok = FALSE;
			break;
	}
	return (ok);
}

//---------------------------------------------------------------------------------------------
//	fft2DButterWorth
//---------------------------------------------------------------------------------------------
TIPImage** TIP::fft2DButterWorth(TIPImage **src, int filtType, double f1, double f2, double order)
{
	TIPImage	**dest=NULL, **fftIma, **fftIma2;
	int			dev, w, h, overl, bpp, bppnew, maxRad;
	BOOL		isCuda, ok;
	float		*filtr;

	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bppnew, &isCuda);
	fftIma=fft2D(src, -1);
	if (fftIma != NULL)
	{
		(*fftIma)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
		maxRad=(int)sqrt((double)(w*w/4)+(double)(h*h)/4) + 1;
		filtr = new float[maxRad];
		ok=CreateFFTButterWorthFilter(filtr, w, h, (FilterType) filtType, f1, f2, order);
		if (ok)
		{
			fftIma2=TIPCreateImage(dev, w, h, 0, 64, FALSE);			
			CudaFFTFilter((cudaArray *)(*fftIma)->GetCudaPtr(), (*fftIma2)->GetCudaPtr(), 
				w, h, filtr, maxRad);
			if ((bppnew==24) || (bppnew==48)) bppnew /=3;
			dest=fft2D(fftIma2, 1, bppnew);
			TIPReleaseImage(fftIma2);
		}
		TIPReleaseImage(fftIma);
		delete filtr;
	}
	return dest;
}

//---------------------------------------------------------------------------------------------
//	fft2DGaussian
//---------------------------------------------------------------------------------------------
TIPImage** TIP::fft2DGaussian(TIPImage **src, int filtType, double f1, double f2)
{
	TIPImage	**dest=NULL, **fftIma, **fftIma2;
	int			dev, w, h, overl, bpp, bppnew, maxRad;
	BOOL		isCuda, ok;
	float		*filtr;

	(*src)->GetImageInfo(&dev, &w, &h, &overl, &bppnew, &isCuda);
	fftIma=fft2D(src, -1, 64);
	if (fftIma != NULL)
	{
		(*fftIma)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
		maxRad=(int)sqrt((double)(w*w/4)+(double)(h*h)/4) + 1;
		filtr = new float[maxRad];
		ok=CreateFFTGaussianFilter(filtr, w, h, (FilterType) filtType, f1, f2);
		if (ok)
		{
			fftIma2=TIPCreateImage(dev, w, h, 0, 64, FALSE);			
			CudaFFTFilter((*fftIma)->GetCudaPtr(), (*fftIma2)->GetCudaPtr(), 
				w, h, filtr, maxRad);
			if ((bppnew==24) || (bppnew==48)) bppnew /=3;
			dest=fft2D(fftIma2, 1, bppnew);
			TIPReleaseImage(fftIma2);
		}
		TIPReleaseImage(fftIma);
		delete filtr;
	}
	return dest;
}

//---------------------------------------------------------------------------------------------
//	FFTImageRegistration
//---------------------------------------------------------------------------------------------
void TIP::FFTImageRegistration(TIPImage **src1, TIPImage **src2, int *xshift, int *yshift, 
							   float *sn, int MaxShift)
{
	TIPImage	**fft1Ima, **fft2Ima;

	fft1Ima=fft2D(src1, -1);	// forward fft Src1
	fft2Ima=fft2D(src2, -1);	// forward fft Src2
	if ((fft1Ima > NULL) && (fft2Ima>NULL))
		CoreFFTImageRegistration(fft1Ima, fft2Ima, xshift, yshift, sn, MaxShift);
	TIPReleaseImage(fft2Ima);
	TIPReleaseImage(fft1Ima);
}

//---------------------------------------------------------------------------------------------
//	CoreFFTImageRegistration
//---------------------------------------------------------------------------------------------
void TIP::CoreFFTImageRegistration(TIPImage **fftSrc1, TIPImage **fftSrc2, int *xshift, 
								   int *yshift, float *sn, int MaxShift)
{
	double		maxVal=0.0, averVal=0.0, snVal=0.0;
	float		*pData, *pImageData;
	int			i, j, dev1, w1, h1, overl1, bpp1, dev2, w2, h2, overl2, bpp2;
	TIPImage	**fftResult, **dest;
	BOOL		isCuda1, isCuda2;

	*xshift=0;
	*yshift=0;
	*sn=0.;
	(*fftSrc1)->GetImageInfo(&dev1, &w1, &h1, &overl1, &bpp1, &isCuda1);
	(*fftSrc2)->GetImageInfo(&dev2, &w2, &h2, &overl2, &bpp2, &isCuda2);
	if ((dev1 != dev2) || (w1 != w2) || (h1!= h2) || (overl1 != 0) || (overl2 != 0) ||
		(bpp1 != 64) || (bpp2 != 64)) return;
	fftResult=TIPCreateImage(dev1, w1, h1, 0, 64, FALSE);
	dest=TIPCreateImage(dev1, w1, h1, 0, 32, FALSE);
	CudaFFTMultiply((*fftSrc1)->GetCudaPtr(), (*fftSrc2)->GetCudaPtr(),
					(*fftResult)->GetCudaPtr(), w1, h1);
	dest=fft2D(fftResult, 1, 32);	// reverse fft render to floating point target
	pImageData=new float[w1*h1];
	GetImage(dest, pImageData);
	pData=pImageData;
	for  (j=0; j< h1; j++)
	{
		for (i=0; i< w1; i++)
		{
			if (*pData > maxVal)
			{
				maxVal=*pData;
				*xshift=i;
				*yshift=j;
			}
			averVal +=*pData;
			pData ++;
		}
	}
	averVal =averVal/((double)w1*(double)h1);
	pData=pImageData;
	for  (j=0; j< h1; j++)
	{
		for (i=0; i< w1; i++)
		{
			snVal +=((*pData-averVal)*(*pData-averVal));	
			pData ++;
		}
	}
	snVal =sqrt(snVal/((double)w1*(double)h1));
	if (snVal > 0.0)
		*sn=(float)(maxVal-averVal)/(float)snVal;
	else 
	{
		*sn=0.0;
		*xshift=0;
		*yshift=0;
	}
	if (*xshift > w1/2)
		*xshift -= w1;
	if (*yshift > h1/2)
		*yshift -=h1;
	if ((abs(*xshift) > MaxShift) || (abs(*yshift) > MaxShift))
	{
		// it is better not to shift if your not certain
		*sn=(float)0.0;
		*xshift=0;
		*yshift=0;
	}
	*xshift = -(*xshift);
	*yshift = -(*yshift);
	delete [] pImageData;
	TIPReleaseImage(dest);
	TIPReleaseImage(fftResult);
}


//---------------------------------------------------------------------------------------------
//	FFTImageStitch
//---------------------------------------------------------------------------------------------
void TIP::FFTImageStitch(BYTE *Addr1, BYTE *Addr2, int w, int h, int mode, int MaxShift, 
						 int *xshift, int *yshift, float *sn)
{
	TIPImage	**fft1Ima, **fft2Ima, **crop1, **crop2;
	int			x1, y1, w1, h1, x2, y2, w2, h2, MaxShiftPow2;

	MaxShiftPow2=(long)pow(2.,((long)(log((double)MaxShift)/log(2.))+1));
	switch(mode)
	{
		case 0:	// source2 is stitched to upper part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=(h-MaxShiftPow2);
				y2=0;
				break;
		case 1:	// source2 is stitched to right part of source1
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=w-MaxShiftPow2;
				x2=0;
				break;
		case 2:	// source2 is stitched to lower part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=0;
				y2=(h-MaxShiftPow2);
				break;
		case 3:	// source2 is stitched to left part of source1
		default:
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=0;
				x2=w-MaxShiftPow2;
				break;
	}

	crop1=TIPCreateImage(0, w1, h1, 0, 8, TRUE);
	crop2=TIPCreateImage(0, w2, h2, 0, 8, TRUE);
	PutImage(crop1, Addr1, w, h, x1, y1);
	PutImage(crop2, Addr2, w, h, x2, y2);
	fft1Ima=fft2D(crop1, -1);
	fft2Ima=fft2D(crop2, -1);
	CoreFFTImageRegistration(fft1Ima, fft2Ima, xshift, yshift, sn, MaxShift);
	TIPReleaseImage(fft2Ima);
	TIPReleaseImage(fft1Ima);
	TIPReleaseImage(crop2);
	TIPReleaseImage(crop1);
}

//---------------------------------------------------------------------------------------------
//	FFTImageStitch
//---------------------------------------------------------------------------------------------
void TIP::FFTImageStitch(unsigned short *Addr1, unsigned short *Addr2, int w, int h, int mode, 
						 int MaxShift, int *xshift, int *yshift, float *sn)
{
	TIPImage	**fft1Ima, **fft2Ima, **crop1, **crop2;
	int			x1, y1, w1, h1, x2, y2, w2, h2, MaxShiftPow2;

	MaxShiftPow2=(long)pow(2.,((long)(log((double)MaxShift)/log(2.))+1));
	switch(mode)
	{
		case 0:	// source2 is stitched to upper part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=(h-MaxShiftPow2);
				y2=0;
				break;
		case 1:	// source2 is stitched to right part of source1
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=w-MaxShiftPow2;
				x2=0;
				break;
		case 2:	// source2 is stitched to lower part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=0;
				y2=(h-MaxShiftPow2);
				break;
		case 3:	// source2 is stitched to left part of source1
		default:
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=0;
				x2=w-MaxShiftPow2;
				break;
	}

	crop1=TIPCreateImage(0, w1, h1, 0, 16, TRUE);
	crop2=TIPCreateImage(0, w2, h2, 0, 16, TRUE);
	PutImage(crop1, Addr1, w, h, x1, y1);
	PutImage(crop2, Addr2, w, h, x2, y2);
	fft1Ima=fft2D(crop1, -1);
	fft2Ima=fft2D(crop2, -1);
	CoreFFTImageRegistration(fft1Ima, fft2Ima, xshift, yshift, sn, MaxShift);
	TIPReleaseImage(fft2Ima);
	TIPReleaseImage(fft1Ima);
	TIPReleaseImage(crop2);
	TIPReleaseImage(crop1);
}

//---------------------------------------------------------------------------------------------
//	FFTImageStitch
//---------------------------------------------------------------------------------------------
void TIP::FFTImageStitch(RGBTRIPLE *Addr1, RGBTRIPLE *Addr2, int w, int h, int mode, 
						 int MaxShift, int *xshift, int *yshift, float *sn)
{
	TIPImage	**fft1Ima, **fft2Ima, **crop1, **crop2;
	int			x1, y1, w1, h1, x2, y2, w2, h2, MaxShiftPow2;

	MaxShiftPow2=(long)pow(2.,((long)(log((double)MaxShift)/log(2.))+1));
	switch(mode)
	{
		case 0:	// source2 is stitched to upper part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=(h-MaxShiftPow2);
				y2=0;
				break;
		case 1:	// source2 is stitched to right part of source1
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=w-MaxShiftPow2;
				x2=0;
				break;
		case 2:	// source2 is stitched to lower part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=0;
				y2=(h-MaxShiftPow2);
				break;
		case 3:	// source2 is stitched to left part of source1
		default:
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=0;
				x2=w-MaxShiftPow2;
				break;
	}

	crop1=TIPCreateImage(0, w1, h1, 0, 24, TRUE);
	crop2=TIPCreateImage(0, w2, h2, 0, 24, TRUE);
	PutImage(crop1, Addr1, w, h, x1, y1, IN_COLOUR);
	PutImage(crop2, Addr2, w, h, x2, y2, IN_COLOUR);
	fft1Ima=fft2D(crop1, -1);
	fft2Ima=fft2D(crop2, -1);
	CoreFFTImageRegistration(fft1Ima, fft2Ima, xshift, yshift, sn, MaxShift);
	TIPReleaseImage(fft2Ima);
	TIPReleaseImage(fft1Ima);
	TIPReleaseImage(crop2);
	TIPReleaseImage(crop1);
}

//---------------------------------------------------------------------------------------------
//	FFTImageStitch
//---------------------------------------------------------------------------------------------
void TIP::FFTImageStitch(RGB16BTRIPLE *Addr1, RGB16BTRIPLE *Addr2, int w, int h, int mode, 
						 int MaxShift, int *xshift, int *yshift, float *sn)
{
	TIPImage	**fft1Ima, **fft2Ima, **crop1, **crop2;
	int			x1, y1, w1, h1, x2, y2, w2, h2, MaxShiftPow2;

	MaxShiftPow2=(long)pow(2.,((long)(log((double)MaxShift)/log(2.))+1));
	switch(mode)
	{
		case 0:	// source2 is stitched to upper part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=(h-MaxShiftPow2);
				y2=0;
				break;
		case 1:	// source2 is stitched to right part of source1
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=w-MaxShiftPow2;
				x2=0;
				break;
		case 2:	// source2 is stitched to lower part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=0;
				y2=(h-MaxShiftPow2);
				break;
		case 3:	// source2 is stitched to left part of source1
		default:
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=0;
				x2=w-MaxShiftPow2;
				break;
	}

	crop1=TIPCreateImage(0, w1, h1, 0, 48, TRUE);
	crop2=TIPCreateImage(0, w2, h2, 0, 48, TRUE);
	PutImage(crop1, Addr1, w, h, x1, y1, IN_COLOUR);
	PutImage(crop2, Addr2, w, h, x2, y2, IN_COLOUR);
	fft1Ima=fft2D(crop1, -1);
	fft2Ima=fft2D(crop2, -1);
	CoreFFTImageRegistration(fft1Ima, fft2Ima, xshift, yshift, sn, MaxShift);
	TIPReleaseImage(fft2Ima);
	TIPReleaseImage(fft1Ima);
	TIPReleaseImage(crop2);
	TIPReleaseImage(crop1);
}

//---------------------------------------------------------------------------------------------
//	CoreFFTImageStitch
//---------------------------------------------------------------------------------------------
void TIP::CoreFFTImageStitch(TIPImage **src1, TIPImage **src2, int mode, int MaxShift, 
							int *xshift, int *yshift, float *sn)
{
	TIPImage	**fft1Ima, **fft2Ima, **crop1, **crop2;
	int			dev, dev2, w, h, overl, overl2, bpp, bpp2, x1, y1, w1, h1, x2, y2, w2, h2;
	int			MaxShiftPow2;
	BOOL		isCuda, isCuda2;

	*xshift=0;
	*yshift=0;
	*sn=-1.0;

	MaxShiftPow2=(long)pow(2.,((long)(log((double)MaxShift)/log(2.))+1));
	(*src1)->GetImageInfo(&dev, &w, &h, &overl, &bpp, &isCuda);
	(*src2)->GetImageInfo(&dev2, &w2, &h2, &overl2, &bpp2, &isCuda2);
	if ((dev!=dev2) || (w != w2) || (h != h2) || (overl != 0) || (overl2 != 0) || 
		(bpp != bpp2) || (!isCuda) || (!isCuda2)) return;

	switch(mode)
	{
		case 0:	// source2 is stitched to upper part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=(h-MaxShiftPow2);
				y2=0;
				break;
		case 1:	// source2 is stitched to right part of source1
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=w-MaxShiftPow2;
				x2=0;
				break;
		case 2:	// source2 is stitched to lower part of source1
				w1=w2=(long)pow(2.,(long)(log((double)w)/log(2.)));
				h1=h2=MaxShiftPow2;
				x1=x2=(w-w1)/2;
				y1=0;
				y2=(h-MaxShiftPow2);
				break;
		case 3:	// source2 is stitched to left part of source1
		default:
				w1=w2=MaxShiftPow2;
				h1=h2=(long)pow(2.,(long)(log((double)h)/log(2.)));
				y1=y2=(h-h1)/2;
				x1=0;
				x2=w-MaxShiftPow2;
				break;
	}
	// crop the image  so that width and height are a power to 2
	crop1=ImageCropToComplex(src1, x1, y1, w1, h1);
	crop2=ImageCropToComplex(src2, x2, y2, w2, h2);
	fft1Ima=fft2D(crop1, -1);
	fft2Ima=fft2D(crop2, -1);
	CoreFFTImageRegistration(fft1Ima, fft2Ima, xshift, yshift, sn, MaxShift);
	TIPReleaseImage(fft2Ima);
	TIPReleaseImage(fft1Ima);
	TIPReleaseImage(crop2);
	TIPReleaseImage(crop1);
}
