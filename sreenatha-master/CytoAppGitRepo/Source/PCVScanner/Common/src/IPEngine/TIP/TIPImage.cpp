///////////////////////////////////////////////////////////////////////////////////////////////
// TIPImage class
///////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//
#include "stdafx.h"
#include "cutil.h"
#include "TIP.h"
#include "TIPImage.h"
#include "TIPSwapImage.h"

TIPImage::TIPImage(int Device)
{
    m_nImageWidth = 0;
    m_nImageHeight = 0;
	m_nBpp = 0;
	m_nOverlap = 0;
	m_nDevice = Device;
	this->m_Handle=this;
}

TIPImage::~TIPImage(void)
{
	if (this->m_nIsCudaArray)
	{
		CUDA_SAFE_CALL(cudaFreeArray((cudaArray *)this->m_pCudaImage));
	}
	else
	{
		CUDA_SAFE_CALL(cudaFree(this->m_pCudaImage));
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetHandle
///////////////////////////////////////////////////////////////////////////////////////////////

TIPImage **TIPImage::GetHandle()
{
	return((TIPImage **)&m_Handle);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetDevice
///////////////////////////////////////////////////////////////////////////////////////////////

int TIPImage::GetDevice()
{
	return (this->m_nDevice);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetWidth
///////////////////////////////////////////////////////////////////////////////////////////////

int TIPImage::GetWidth()
{
	return (this->m_nImageWidth);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetHeight
///////////////////////////////////////////////////////////////////////////////////////////////

int TIPImage::GetHeight()
{
	return (this->m_nImageHeight);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetSize
///////////////////////////////////////////////////////////////////////////////////////////////

int TIPImage::GetSize()
{
	return (this->m_nSize);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetOverlap
///////////////////////////////////////////////////////////////////////////////////////////////

int TIPImage::GetOverlap()
{
	return (this->m_nOverlap);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetBpp
///////////////////////////////////////////////////////////////////////////////////////////////

int TIPImage::GetBpp()
{
	return (this->m_nBpp);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// IsCudaArray
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::IsCudaArray()
{
	return(this->m_nIsCudaArray);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetCudaArrayPtr
///////////////////////////////////////////////////////////////////////////////////////////////

void* TIPImage::GetCudaPtr()
{
	return(this->m_pCudaImage);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::Create(int Width, int Height, int Overlap, int Bpp, BOOL IsCudaArray)
{
	cudaChannelFormatDesc channelDesc;	
	this->m_nImageWidth=Width;
	this->m_nImageHeight=Height;
	this->m_nOverlap=Overlap;
	this->m_nBpp=Bpp;
	if (Bpp==24) Bpp=32;					// Size of Colour images 4 bytes per pixel
	else if (Bpp==48) Bpp=64;
	if ((Overlap != 0) && ((Bpp==8) || (Bpp==16)))
	{
		// Handle packed 8-bit or 16 bit images
		if (Overlap< 0)Overlap=0;
		Width=this->m_nImageWidth/2+Overlap;
		Height=this->m_nImageHeight/2+Overlap;
		Bpp *=4;
	}
	this->m_nSize=Width*Height*Bpp/8;		// Real Size of Packed or Unpacked Image
	this->m_nIsCudaArray=IsCudaArray;

	cudaSetDevice(this->m_nDevice);
	switch (this->m_nBpp)
	{
		case 8:
			if (this->m_nOverlap != 0)
				channelDesc=cudaCreateChannelDesc(8, 8, 8, 8, 
										cudaChannelFormatKind(cudaChannelFormatKindUnsigned));
			else
				channelDesc=cudaCreateChannelDesc(8, 0, 0, 0, 
										cudaChannelFormatKind(cudaChannelFormatKindUnsigned));
			if (this->m_nIsCudaArray)
			{
				CUDA_SAFE_CALL(cudaMallocArray((cudaArray **)&m_pCudaImage, &channelDesc, Width, Height));
			}
			else
			{
				CUDA_SAFE_CALL(cudaMalloc(&m_pCudaImage,this->m_nSize));
			}
			break;
		case 16:
			if (this->m_nOverlap != 0)
				channelDesc=cudaCreateChannelDesc(16, 16, 16, 16, 
										cudaChannelFormatKind(cudaChannelFormatKindUnsigned));
			else
				channelDesc=cudaCreateChannelDesc(16, 0, 0, 0, 
										cudaChannelFormatKind(cudaChannelFormatKindUnsigned));
			if (this->m_nIsCudaArray)
			{
				CUDA_SAFE_CALL(cudaMallocArray((cudaArray **)&m_pCudaImage, &channelDesc, Width, Height));
			}
			else
			{
				CUDA_SAFE_CALL(cudaMalloc(&m_pCudaImage,this->m_nSize));
			}
			break;

		case 24:
			this->m_nOverlap=0;
			channelDesc=cudaCreateChannelDesc(8, 8, 8, 8, 
										cudaChannelFormatKind(cudaChannelFormatKindUnsigned));
			if (this->m_nIsCudaArray)
			{
				CUDA_SAFE_CALL(cudaMallocArray((cudaArray **)&m_pCudaImage, &channelDesc, Width, Height));
			}
			else
			{
				CUDA_SAFE_CALL(cudaMalloc(&m_pCudaImage,this->m_nSize));
			}
			break;

		case 32:	// floating point
			this->m_nOverlap=0;
			channelDesc=cudaCreateChannelDesc(32, 0, 0, 0, 
										cudaChannelFormatKind(cudaChannelFormatKindFloat));
			if (this->m_nIsCudaArray)
			{
				CUDA_SAFE_CALL(cudaMallocArray((cudaArray **)&m_pCudaImage, &channelDesc, Width, Height));
			}
			else
			{
				CUDA_SAFE_CALL(cudaMalloc(&m_pCudaImage,this->m_nSize));
			}
			break;
		case 48:
			this->m_nOverlap=0;
			channelDesc=cudaCreateChannelDesc(16, 16, 16, 16, 
										cudaChannelFormatKind(cudaChannelFormatKindUnsigned));
			if (this->m_nIsCudaArray)
			{
				CUDA_SAFE_CALL(cudaMallocArray((cudaArray **)&m_pCudaImage, &channelDesc, Width, Height));
			}
			else
			{
				CUDA_SAFE_CALL(cudaMalloc(&m_pCudaImage,this->m_nSize));
			}
			break;

		case 64:	// floating point complex
			this->m_nOverlap=0;
			channelDesc=cudaCreateChannelDesc(32, 32, 0, 0, 
										cudaChannelFormatKind(cudaChannelFormatKindFloat));
			if (this->m_nIsCudaArray)
			{
				CUDA_SAFE_CALL(cudaMallocArray((cudaArray **)&m_pCudaImage, &channelDesc, Width, Height));
			}
			else
			{
				CUDA_SAFE_CALL(cudaMalloc(&m_pCudaImage,this->m_nSize));
			}
			break;

		case 128:	// floating point float4 (used for Eucledean Distance Map
			this->m_nOverlap=0;
			channelDesc=cudaCreateChannelDesc(32, 32, 32, 32, 
										cudaChannelFormatKind(cudaChannelFormatKindFloat));
			if (this->m_nIsCudaArray)
			{
				CUDA_SAFE_CALL(cudaMallocArray((cudaArray **)&m_pCudaImage, &channelDesc, Width, Height));
			}
			else
			{
				CUDA_SAFE_CALL(cudaMalloc(&m_pCudaImage,this->m_nSize));
			}
			break;
	}
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetImageInfo
///////////////////////////////////////////////////////////////////////////////////////////////

void TIPImage::GetImageInfo(int *Device, int *Width, int *Height, int *Overlap, int *BPP, 
							BOOL *IsCudaArray)
{
	*Device=this->m_nDevice;
	*Width=this->m_nImageWidth;
	*Height=this->m_nImageHeight;
	*Overlap=this->m_nOverlap;
	*BPP=this->m_nBpp;
	*IsCudaArray=this->m_nIsCudaArray;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetPackedDimensions
///////////////////////////////////////////////////////////////////////////////////////////////

void TIPImage::GetPackedDimensions(int *Width, int *Height, int *BPP)
{
	int overlap;
	*Width=this->m_nImageWidth;
	*Height=this->m_nImageHeight;
	*BPP=this->m_nBpp;
	if (this->m_nOverlap != 0)
	{
		overlap=this->m_nOverlap;
		if (overlap < 0)  overlap=0;
		*Width=this->m_nImageWidth/2 + overlap;
		*Height=this->m_nImageHeight/2 + overlap;
		*BPP=this->m_nBpp*3;
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////
// SameSpecs
//	- Images can be Cuda Arrays or Cuda Matrices
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::SameSpecs (TIPImage *src1, TIPImage *src2, TIPImage *src3)
{
	if (src1!=NULL)
	{
		if ((this->m_nImageWidth != src1->GetWidth()) || (this->m_nImageHeight != src1->GetHeight()) ||
			(this->m_nBpp!= src1->GetBpp()) || (this->m_nOverlap!= src1->GetOverlap()) || 
			(this->m_nDevice != src1->GetDevice()))
			return FALSE;
	}
	if (src2 !=NULL)
	{
		if ((this->m_nImageWidth != src2->GetWidth()) || (this->m_nImageHeight != src2->GetHeight()) ||
			(this->m_nBpp!= src2->GetBpp()) || (this->m_nOverlap!= src2->GetOverlap()) ||
			(this->m_nDevice != src2->GetDevice()))
			return FALSE;
	}
	if (src3 !=NULL)
	{
		if ((this->m_nImageWidth != src3->GetWidth()) || (this->m_nImageHeight != src3->GetHeight()) ||
			(this->m_nBpp!= src3->GetBpp()) || (this->m_nOverlap!= src1->GetOverlap()) || 
			(this->m_nDevice != src3->GetDevice()))
			return FALSE;
	}
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetImage - Get 8-bit imagedata from the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::GetImage(TIPSwapImage *pSwapImage, BYTE *pImageData)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Receive(this->m_pCudaImage, this->m_nIsCudaArray);
	if (ok) ok=pSwapImage->Get(pImageData);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetImage - Get 16-bit imagedata from the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::GetImage(TIPSwapImage *pSwapImage, unsigned short *pImageData)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Receive(this->m_pCudaImage, this->m_nIsCudaArray);
	if (ok) ok=pSwapImage->Get(pImageData);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetImage - Get 3 x 8-bit Colour imagedata from the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::GetImage(TIPSwapImage *pSwapImage, RGBTRIPLE *pImageData)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Receive(this->m_pCudaImage, this->m_nIsCudaArray);
	if (ok) ok=pSwapImage->Get(pImageData);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetImage - Get 3 x 16-bit Colour imagedata from the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::GetImage(TIPSwapImage *pSwapImage, RGB16BTRIPLE *pImageData)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Receive(this->m_pCudaImage, this->m_nIsCudaArray);
	if (ok) ok=pSwapImage->Get(pImageData);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetImage - Get 32-bit float imagedata from the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::GetImage(TIPSwapImage *pSwapImage, float *pImageData)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Receive(this->m_pCudaImage, this->m_nIsCudaArray);
	if (ok) ok=pSwapImage->Get(pImageData);	// Get Floating point or Complex data
	return ok;
}


///////////////////////////////////////////////////////////////////////////////////////////////
// GetImageComp - Get grey image component from a 24 bit colour image on the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::GetImageComp(TIPSwapImage *pSwapImage, long component, BYTE *pImageData)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Receive(this->m_pCudaImage, this->m_nIsCudaArray);
	if (ok) ok=pSwapImage->GetComponent(component, pImageData);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetImageComp - Get grey image component from a 48 bit colour image on the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::GetImageComp(TIPSwapImage *pSwapImage, long component, unsigned short *pImageData)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Receive(this->m_pCudaImage, this->m_nIsCudaArray);
	if (ok) ok=pSwapImage->GetComponent(component, pImageData);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// PutImage - Send 8-bit imagedata to the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::PutImage(TIPSwapImage *pSwapImage, BYTE *pImageData)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Put(pImageData);
	if (ok) ok=pSwapImage->Transmit(this->m_pCudaImage, this->m_nIsCudaArray);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// PutImage - Send 16-bit imagedata to the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::PutImage(TIPSwapImage *pSwapImage, unsigned short *pImageData)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Put(pImageData);
	if (ok) ok=pSwapImage->Transmit(this->m_pCudaImage, this->m_nIsCudaArray);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// PutImage - Send 3x8-bit colour imagedata to the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::PutImage(TIPSwapImage *pSwapImage, RGBTRIPLE *pImageData)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Put(pImageData);
	if (ok) ok=pSwapImage->Transmit(this->m_pCudaImage, this->m_nIsCudaArray);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// PutImage - Send 8-bit float imagedata to the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::PutImage(TIPSwapImage *pSwapImage, RGB16BTRIPLE *pImageData)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Put(pImageData);
	if (ok) ok=pSwapImage->Transmit(this->m_pCudaImage, this->m_nIsCudaArray);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// PutImage - Send 8-bit float imagedata to the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::PutImage(TIPSwapImage *pSwapImage, float *pImageData)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Put(pImageData);
	if (ok) ok=pSwapImage->Transmit(this->m_pCudaImage, this->m_nIsCudaArray);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// PutImage - Send 8-bit imagedata to the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::PutImage(TIPSwapImage *pSwapImage, BYTE *pImageData, int w, int h, int x0, 
						int y0)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Put(pImageData, w, h, x0, y0);
	if (ok) ok=pSwapImage->Transmit(this->m_pCudaImage, this->m_nIsCudaArray);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// PutImage - Send 16-bit imagedata to the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::PutImage(TIPSwapImage *pSwapImage, unsigned short *pImageData, int w, int h, 
						int x0, int y0)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Put(pImageData, w, h, x0, y0);
	if (ok) ok=pSwapImage->Transmit(this->m_pCudaImage, this->m_nIsCudaArray);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// PutImage - Send 3x8-bit colour imagedata to the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::PutImage(TIPSwapImage *pSwapImage, RGBTRIPLE *pImageData, int w, int h, 
						int x0, int y0, int colourMode)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Put(pImageData, w, h, x0, y0, colourMode);
	if (ok) ok=pSwapImage->Transmit(this->m_pCudaImage, this->m_nIsCudaArray);
	return ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// PutImage - Send 8-bit float imagedata to the GPU/Tesla device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPImage::PutImage(TIPSwapImage *pSwapImage, RGB16BTRIPLE *pImageData, int w, int h, 
						int x0, int y0, int colourMode)
{
	BOOL ok;
	cudaSetDevice(this->m_nDevice);
	ok=pSwapImage->Put(pImageData, w, h, x0, y0, colourMode);
	if (ok) ok=pSwapImage->Transmit(this->m_pCudaImage, this->m_nIsCudaArray);
	return ok;
}
