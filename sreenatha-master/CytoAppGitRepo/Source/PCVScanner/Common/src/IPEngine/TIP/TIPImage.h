#pragma once
///////////////////////////////////////////////////////////////////////////////////////////////
// TIP Image class - header file
//
///////////////////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "cuda_runtime_api.h"
#include "TIP.h"

// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif

class TIPSwapImage;

///////////////////////////////////////////////////////////////////////////////////////////////
// TIPImage
///////////////////////////////////////////////////////////////////////////////////////////////

class DllExport TIPImage
{
	public:

		TIPImage(int cudaDevice);
		~TIPImage();
	
		virtual BOOL		Create(int Width, int Height, int Overlap, int Bpp, 
								   BOOL IsCudaArray=FALSE);
		void				GetImageInfo(int *Device, int *Width, int *Height, int *Overlap, 
										 int *BPP, BOOL *IsCudaArray);
		void				GetPackedDimensions(int *Width, int *Height, int *BPP);
		TIPImage **			GetHandle();
		int					GetDevice();
		int					GetWidth();
		int					GetHeight();
		int					GetSize();
		int					GetOverlap();
		int					GetBpp();
		BOOL				IsCudaArray();
		void*				GetCudaPtr();	
		BOOL				SameSpecs (TIPImage *src1, TIPImage *src2=NULL, TIPImage *src3=NULL);
		BOOL				GetImage(TIPSwapImage *pSwapImage, BYTE *pImageData);
		BOOL				GetImage(TIPSwapImage *pSwapImage, unsigned short *pImageData);
		BOOL				GetImage(TIPSwapImage *pSwapImage, RGBTRIPLE *pImageData);
		BOOL				GetImage(TIPSwapImage *pSwapImage, RGB16BTRIPLE *pImageData);
		BOOL				GetImage(TIPSwapImage *pSwapImage, float *pImageData);
		BOOL				GetImageComp(TIPSwapImage *pSwapImage, long component, 
										BYTE *pImageData);
		BOOL				GetImageComp(TIPSwapImage *pSwapImage, long component, 
										unsigned short *pImageData);
		BOOL				PutImage(TIPSwapImage *pSwapImage, BYTE *pImageData);
		BOOL				PutImage(TIPSwapImage *pSwapImage, unsigned short *pImageData);
		BOOL				PutImage(TIPSwapImage *pSwapImage, RGBTRIPLE *pImageData);
		BOOL				PutImage(TIPSwapImage *pSwapImage, RGB16BTRIPLE *pImageData);
		BOOL				PutImage(TIPSwapImage *pSwapImage, float *pImageData);
		BOOL				PutImage(TIPSwapImage *pSwapImage, BYTE *pImageData, int w, 
									int h, int x0, int y0);
		BOOL				PutImage(TIPSwapImage *pSwapImage, unsigned short *pImageData, int w, 
									int h, int x0, int y0);
		BOOL				PutImage(TIPSwapImage *pSwapImage, RGBTRIPLE *pImageData, int w, 
									int h, int x0, int y0, int ColourMode);
		BOOL				PutImage(TIPSwapImage *pSwapImage, RGB16BTRIPLE *pImageData, int w, 
									int h, int x0, int y0, int ColourMode);

	private:
		TIPImage*			m_Handle;
		int					m_nDevice;
		int					m_nImageWidth;                  // Actual image dimensions
		int					m_nImageHeight;
		int					m_nSize;
		int					m_nOverlap;
		int					m_nBpp;
		BOOL				m_nIsCudaArray;
		void*				m_pCudaImage;

};

