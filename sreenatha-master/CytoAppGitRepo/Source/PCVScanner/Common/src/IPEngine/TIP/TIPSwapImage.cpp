///////////////////////////////////////////////////////////////////////////////////////////////
// TIPLSwapImage class - source file
///////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "cutil.h"
#include "cuda_runtime_api.h"
#include "TIP.h"
#include "TIPSwapImage.h"

TIPSwapImage::TIPSwapImage()
{
}

TIPSwapImage::~TIPSwapImage()
{
	CUDA_SAFE_CALL(cudaFreeHost(this->m_pHostImage));
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Create a swapimage (pitch is controlled by the kernels)
///////////////////////////////////////////////////////////////////////////////////////////////
 
BOOL TIPSwapImage::Create(int Width, int Height, int Overlap, int Bpp)             
{ 
	BOOL	Ok=TRUE;
	
	size_t size;

	this->m_nImageWidth=Width;
	this->m_nImageHeight=Height;
	this->m_nOverlap=Overlap;
	this->m_nBpp=Bpp;
	if (Bpp==24) Bpp=32;					// Size of Colour images 4 bytes per pixel
	if ((Overlap != 0) && ((Bpp==8) || (Bpp==16)))
	{
		// Handle packed 8-bit or 16 bit images
		if (Overlap< 0)Overlap=0;
		Width=this->m_nImageWidth/2+Overlap;
		Height=this->m_nImageHeight/2+Overlap;
		Bpp=Bpp*4;
	}
	this->m_nSize=Width*Height*Bpp/8;		// Real Size of Packed or Unpacked Image
	size=Width*Height*Bpp/8;
	CUDA_SAFE_CALL(cudaMallocHost(&m_pHostImage, size));
    return Ok;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetWidth
///////////////////////////////////////////////////////////////////////////////////////////////

int TIPSwapImage::GetWidth()
{
	return (this->m_nImageWidth);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetDevice
///////////////////////////////////////////////////////////////////////////////////////////////

int TIPSwapImage::GetHeight()
{
	return (this->m_nImageHeight);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// GetOverlap
///////////////////////////////////////////////////////////////////////////////////////////////

int TIPSwapImage::GetOverlap()
{
	return (this->m_nOverlap);
}


///////////////////////////////////////////////////////////////////////////////////////////////
// GetBpp
///////////////////////////////////////////////////////////////////////////////////////////////

int TIPSwapImage::GetBpp()
{
	return (this->m_nBpp);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Send this swap image to a GPU/Tesla Device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPSwapImage::Transmit(void *pDest, BOOL IsCudaArray)  
{
	if (IsCudaArray == TRUE)
	{
		CUDA_SAFE_CALL(cudaMemcpyToArray((cudaArray *)pDest, 0, 0, this->m_pHostImage, this->m_nSize, cudaMemcpyHostToDevice));
	}
	else
	{
 		CUDA_SAFE_CALL(cudaMemcpy(pDest, this->m_pHostImage, this->m_nSize, cudaMemcpyHostToDevice));
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// receive this swap image from a GPU/Tesla Device
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPSwapImage::Receive(void *pSource, BOOL IsCudaArray)       
{
	if (IsCudaArray)
	{
		CUDA_SAFE_CALL(cudaMemcpyFromArray (this->m_pHostImage, (cudaArray *)pSource, 0, 0, this->m_nSize, cudaMemcpyDeviceToHost));
	}
	else
	{
 		CUDA_SAFE_CALL(cudaMemcpy(this->m_pHostImage, pSource, this->m_nSize, cudaMemcpyDeviceToHost));
	}

    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Put 8-bit imagedata into swapImage
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPSwapImage::Put(BYTE *pImageData) 
{
	BYTE	*p, *q;
	size_t	l;

	if (!pImageData) return FALSE;
	if (m_nOverlap==0)
	{
		// unpacked 8 bit image
		q=(BYTE *)this->m_pHostImage;
		p=pImageData;
		for (l=0; l<this->m_nSize; l++)*q++=*p++;
	}
	else
	{
		// packed 8-bit image
		q=(BYTE *)this->m_pHostImage;
		int overlap=m_nOverlap;
		if (overlap < 0) overlap=0;
		int	swapWidth =m_nImageWidth/2 + overlap;
		int	swapHeight =m_nImageHeight/2 + overlap;
		int dW = this->m_nImageWidth  - swapWidth;
		int dH = m_nImageHeight - swapHeight;
	
		// ARGB data will be used to hold each quadrant of the gray image
		BYTE *bidata = pImageData;
		BYTE *gidata = pImageData + dW;
		BYTE *ridata = pImageData + dH * m_nImageWidth;
		BYTE *aidata = pImageData + dH * m_nImageWidth + dW;

		// Copy surface into image
		for (int row = 0; row < swapHeight; row++)
		{
			for (int col = 0; col < swapWidth; col++)
			{
				*q++ = *bidata++;
				*q++ = *gidata++;
				*q++ = *ridata++;
				*q++ = *aidata++;
			}
			bidata += dW;
			gidata += dW;
			ridata += dW;
			aidata += dW;
		}
	}
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Put 16-bit imagedata into swapImage
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPSwapImage::Put(unsigned short *pImageData) 
{
	unsigned short	*p, *q;
	size_t			l;

	if (!pImageData) return FALSE;
	if (m_nOverlap==0)
	{
		// unpacked 16 bit image
		q=(unsigned short *)this->m_pHostImage;
		p=pImageData;
		for (l=0; l<this->m_nSize/2; l++)*q++=*p++;
	}
	else
	{
		//packed 16 bit image
		q=(unsigned short *)this->m_pHostImage;
		int overlap=m_nOverlap;
		if (overlap < 0) overlap=0;
		int	swapWidth =m_nImageWidth/2 + overlap;
		int	swapHeight =m_nImageHeight/2 + overlap;
	    int dW = m_nImageWidth  - swapWidth;
		int dH = m_nImageHeight - swapHeight;
	
		// ARGB data will be used to hold each quadrant of the gray image
		unsigned short *ridata = pImageData;
		unsigned short *gidata = pImageData + dW;
		unsigned short *bidata = pImageData + dH * m_nImageWidth;
		unsigned short *aidata = pImageData + dH * m_nImageWidth + dW;

		// Copy surface into image
		for (int row = 0; row < swapHeight; row++)
		{
			for (int col = 0; col < swapWidth; col++)
			{
				*q++ = *bidata++;
				*q++ = *gidata++;
				*q++ = *ridata++;
				*q++ = *aidata++;
			}
			bidata += dW;
			gidata += dW;
			ridata += dW;
			aidata += dW;
		}
	}
    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Put 3x8-bit colour imagedata into swapImage
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPSwapImage::Put(RGBTRIPLE *pImageData)  
{
	BYTE		*q;
    RGBTRIPLE	*p;
	size_t		l;

	if (!pImageData) return FALSE;
	p=pImageData;
	q=(BYTE *)this->m_pHostImage;
	for (l=0; l<this->m_nSize/4; l++)
	{
		*q++=p->rgbtRed;
		*q++=p->rgbtGreen;
		*q++=p->rgbtBlue;
        *q++=0x0;
        p++;
	}
    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Put 3x16-bit colour imagedata into swapImage
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPSwapImage::Put(RGB16BTRIPLE *pImageData)  
{
	unsigned short	*q;
    RGB16BTRIPLE	*p;
	size_t			l;

	if (!pImageData) return FALSE;
	p=pImageData;
	q=(unsigned short *)this->m_pHostImage;
	for (l=0; l<this->m_nSize/4; l++)
	{
		*q++=p->rgbtRed;
		*q++=p->rgbtGreen;
		*q++=p->rgbtBlue;
        *q++=0x0;
        p++;
	}
    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Put 32-bit float imagedata into swapImage
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPSwapImage::Put(float *pImageData)
{
	float *p, *q;
	size_t	l;

	if (!pImageData) return FALSE;
	q=(float *)this->m_pHostImage;
	p=pImageData;
	for (l=0; l<this->m_nSize/4; l++) *q++=*p++;
    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Fill (grey or colour) cudaarray with image part of Grey image
///////////////////////////////////////////////////////////////////////////////////////////////
BOOL TIPSwapImage::Put(BYTE *pImageData, int w, int h, int x0, int y0)
{
	BYTE	*p, *q;
	BYTE	*bidata, *gidata, *ridata, *aidata;
	int		i, j;

	if (!pImageData) return FALSE;
	if (this->m_nBpp==8)
	{
		// put 8 bit subimage data into 8 bit TIPImage
		if (m_nOverlap==0)
		{
			// unpacked 8 bit TIPImage
			q=(BYTE *)this->m_pHostImage;
			for (i = 0; i < this->m_nImageHeight; i++)
			{
				p = pImageData + (y0 + i)*w + x0;
				for (j = 0; j < this->m_nImageWidth; j++)
					*q++=*p++;
			}
		}
		else
		{
			// packed 8-bit TIPImage
			q=(BYTE *)this->m_pHostImage;
			int overlap=m_nOverlap;
			if (overlap < 0) overlap=0;
			int	swapWidth =m_nImageWidth/2 + overlap;
			int	swapHeight =m_nImageHeight/2 + overlap;
			int dW = this->m_nImageWidth  - swapWidth;
			int dH = m_nImageHeight - swapHeight;
		
			// ARGB data will be used to hold each quadrant of the gray image

			// Copy surface into image
			for (i = 0; i < swapHeight; i++)
			{
				bidata = pImageData + (y0 + i) * w + x0;
				gidata = pImageData + (y0 + i) * w + x0 + dW;
				ridata = pImageData + (y0 + i + dH) * w + x0;
				aidata = pImageData + (y0 + i + dH ) * w + x0 + dW;;
				for (j = 0; j < swapWidth; j++)
				{
					*q++ = *bidata++;
					*q++ = *gidata++;
					*q++ = *ridata++;
					*q++ = *aidata++;
				}
			}
		}
	}
	else
	{
		// put 8 bit subimage data into 24 bit TIPImage
		q=(BYTE *)this->m_pHostImage;
		for (i = 0; i < m_nImageHeight; i++)
		{
			p = pImageData + (y0 + i)*w + x0;
			for (int col = 0; col < m_nImageWidth; col++)
			{
				*q++ = *p;
				*q++ = *p;
				*q++ = *p;
				*q++ = 0x00;
				p++;
			}
		}
	}
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Fill (16 bit grey or 48 bit colour) cudaarray with image part of 16 bit Grey image
///////////////////////////////////////////////////////////////////////////////////////////////
BOOL TIPSwapImage::Put(unsigned short *pImageData, int w, int h, int x0, int y0)
{
	unsigned short	*p, *q;
	unsigned short	*bidata, *gidata, *ridata, *aidata;
	int		i, j;

	if (!pImageData) return FALSE;
	if (this->m_nBpp==16)
	{
		// put 16 bit subimage data into 16 bit TIPImage
		if (m_nOverlap==0)
		{
			// unpacked 16 bit TIPImage
			q=(unsigned short *)this->m_pHostImage;
			for (i = 0; i < this->m_nImageHeight; i++)
			{
				p = pImageData + (y0 + i)*w + x0;
				for (j = 0; j < this->m_nImageWidth; j++)
					*q++=*p++;
			}
		}
		else
		{
			// packed 16-bit TIPImage
			q=(unsigned short *)this->m_pHostImage;
			int overlap=m_nOverlap;
			if (overlap < 0) overlap=0;
			int	swapWidth =m_nImageWidth/2 + overlap;
			int	swapHeight =m_nImageHeight/2 + overlap;
			int dW = this->m_nImageWidth  - swapWidth;
			int dH = m_nImageHeight - swapHeight;
		
			// ARGB data will be used to hold each quadrant of the gray image

			// Copy surface into image
			for (i = 0; i < swapHeight; i++)
			{
				bidata = pImageData + (y0 + i) * w + x0;
				gidata = pImageData + (y0 + i) * w + x0 + dW;
				ridata = pImageData + (y0 + i + dH) * w + x0;
				aidata = pImageData + (y0 + i + dH ) * w + x0 + dW;;
				for (j = 0; j < swapWidth; j++)
				{
					*q++ = *bidata++;
					*q++ = *gidata++;
					*q++ = *ridata++;
					*q++ = *aidata++;
				}
			}
		}
	}
	else
	{
		// put 16 bit subimage data into 48 bit TIPImage
		q=(unsigned short *)this->m_pHostImage;
		for (i = 0; i < m_nImageHeight; i++)
		{
			p = pImageData + (y0 + i)*w + x0;
			for (int col = 0; col < m_nImageWidth; col++)
			{
				*q++ = *p;
				*q++ = *p;
				*q++ = *p;
				*q++ = 0x00;
				p++;
			}
		}
	}
	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////////////////////
// Fill grey of colour TIPImage part with part of RGB image
///////////////////////////////////////////////////////////////////////////////////////////////
BOOL  TIPSwapImage::Put(RGBTRIPLE *pImageData, int w, int h, int x0, int y0, int ColourMode)
{
	BYTE		*q, grey;
    RGBTRIPLE	*p, *bidata, *gidata, *ridata, *aidata;;
	int			i, j;

	if (!pImageData) return FALSE;
	if (this->m_nBpp == 8)
	{
		if (this->m_nOverlap==0)
		{
			// put colour image data in 8 bit unpacked TIPImage
			q=(BYTE *)this->m_pHostImage;
			for (i = 0; i < this->m_nImageHeight; i++)
			{
				p = pImageData + (y0 + i)*w + x0;
				for (j = 0; j < this->m_nImageWidth; j++)
				{
					*q++=(BYTE)(0.59*(double)p->rgbtGreen+0.30*(double)p->rgbtRed+0.11*(double)p->rgbtBlue);
					p++;
				}
			}
		}
		else
		{
			// put colour data in packed grey TIPImage
			int overlap=m_nOverlap;
			if (overlap < 0) overlap=0;
			int	swapWidth =m_nImageWidth/2 + overlap;
			int	swapHeight =m_nImageHeight/2 + overlap;
			int dW = this->m_nImageWidth  - swapWidth;
			int dH = m_nImageHeight - swapHeight;

			q=(BYTE *)this->m_pHostImage;
			for (i = 0; i < this->m_nImageHeight; i++)
			{
				bidata = pImageData + (y0+i)*w + x0;
				gidata = pImageData + (y0+i)*w + x0 + dW;
				ridata = pImageData + (y0+i + dH)*w + x0;
				aidata = pImageData + (y0+i + dH)*w + x0 + dW;
				for (j = 0; j < this->m_nImageWidth; j++)
				{
					grey=(BYTE)(0.59*(double)bidata->rgbtGreen+0.30*(double)bidata->rgbtRed+0.11*(double)bidata->rgbtBlue);
					*q++ = grey;
					grey=(BYTE)(0.59*(double)gidata->rgbtGreen+0.30*(double)gidata->rgbtRed+0.11*(double)gidata->rgbtBlue);
					*q++ = grey;
					grey=(BYTE)(0.59*(double)ridata->rgbtGreen+0.30*(double)ridata->rgbtRed+0.11*(double)ridata->rgbtBlue);
					*q++ = grey;
					grey=(BYTE)(0.59*(double)aidata->rgbtGreen+0.30*(double)aidata->rgbtRed+0.11*(double)aidata->rgbtBlue);
					*q++ = grey;
					bidata++;
					gidata++;
					ridata++;
					aidata++;
				}
			}
		}
	}
	else
	{		
		if (ColourMode==IN_GREY)	// Colours are converted to Greys
		{
			q=(BYTE *)this->m_pHostImage;
			for (int i = 0; i < m_nImageHeight; i++)
			{
				p = pImageData + (y0+i)*w + x0;
				for (j = 0; j < m_nImageWidth; j++)
				{
					grey=(BYTE)(0.59*(double)p->rgbtGreen+0.30*(double)p->rgbtRed+0.11*(double)p->rgbtBlue);
					*q++ = grey;
					*q++ = grey;
					*q++ = grey;
					*q++ = 0x0;
					p++;
				}
			}
		}
		else	// Colours remain Colours
		{
			q=(BYTE *)this->m_pHostImage;
			for (i = 0; i < m_nImageHeight; i++)
			{
				p = pImageData + (y0+i)*w + x0;
				for (j = 0; j < m_nImageWidth; j++)
				{
					*q++ = p->rgbtRed;
					*q++ = p->rgbtGreen;
					*q++ = p->rgbtBlue;
					*q++ = 0x0;
					p++;
				}
			}
		}
	}
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Fill grey of colour TIPImage part with part of RGB image
///////////////////////////////////////////////////////////////////////////////////////////////
BOOL  TIPSwapImage::Put(RGB16BTRIPLE *pImageData, int w, int h, int x0, int y0, int ColourMode)
{
	unsigned short	*q, grey;
    RGB16BTRIPLE	*p, *bidata, *gidata, *ridata, *aidata;;
	int				i, j;

	if (!pImageData) return FALSE;
	if (this->m_nBpp == 16)
	{
		if (this->m_nOverlap==0)
		{
			// put colour image data in 8 bit unpacked TIPImage
			q=(unsigned short *)this->m_pHostImage;
			for (i = 0; i < this->m_nImageHeight; i++)
			{
				p = pImageData + y0 + i*w + x0;
				for (j = 0; j < this->m_nImageWidth; j++)
				{
					*q++=(unsigned short)(0.59*(double)p->rgbtGreen+0.30*(double)p->rgbtRed+0.11*(double)p->rgbtBlue);
					p++;
				}
			}
		}
		else
		{
			// put colour data in packed grey TIPImage
			int overlap=m_nOverlap;
			if (overlap < 0) overlap=0;
			int	swapWidth =m_nImageWidth/2 + overlap;
			int	swapHeight =m_nImageHeight/2 + overlap;
			int dW = this->m_nImageWidth  - swapWidth;
			int dH = m_nImageHeight - swapHeight;

			q=(unsigned short *)this->m_pHostImage;
			for (i = 0; i < this->m_nImageHeight; i++)
			{
				bidata = pImageData + (y0+i)*w + x0;
				gidata = pImageData + (y0+i)*w + x0 + dW;
				ridata = pImageData + (y0+i + dH)*w + x0;
				aidata = pImageData + (y0+i + dH)*w + x0 + dW;
				for (j = 0; j < this->m_nImageWidth; j++)
				{
					grey=(BYTE)(0.59*(double)bidata->rgbtGreen+0.30*(double)bidata->rgbtRed+0.11*(double)bidata->rgbtBlue);
					*q++ = grey;
					grey=(BYTE)(0.59*(double)gidata->rgbtGreen+0.30*(double)gidata->rgbtRed+0.11*(double)gidata->rgbtBlue);
					*q++ = grey;
					grey=(BYTE)(0.59*(double)ridata->rgbtGreen+0.30*(double)ridata->rgbtRed+0.11*(double)ridata->rgbtBlue);
					*q++ = grey;
					grey=(BYTE)(0.59*(double)aidata->rgbtGreen+0.30*(double)aidata->rgbtRed+0.11*(double)aidata->rgbtBlue);
					*q++ = grey;
					bidata++;
					gidata++;
					ridata++;
					aidata++;
				}
			}
		}
	}
	else
	{		
		if (ColourMode==IN_GREY)	// Colours are converted to Greys
		{
			q=(unsigned short *)this->m_pHostImage;
			for (int i = 0; i < m_nImageHeight; i++)
			{
				p = pImageData + (y0+i)*w + x0;
				for (j = 0; j < m_nImageWidth; j++)
				{
					grey=(BYTE)(0.59*(double)p->rgbtGreen+0.30*(double)p->rgbtRed+0.11*(double)p->rgbtBlue);
					*q++ = grey;
					*q++ = grey;
					*q++ = grey;
					*q++ = 0x0;
					p++;
				}
			}
		}
		else	// Colours remain Colours
		{
			q=(unsigned short *)this->m_pHostImage;
			for (i = 0; i < m_nImageHeight; i++)
			{
				p = pImageData + (y0+i)*w + x0;
				for (j = 0; j < m_nImageWidth; j++)
				{
					*q++ = p->rgbtRed;
					*q++ = p->rgbtGreen;
					*q++ = p->rgbtBlue;
					*q++ = 0x0;
					p++;
				}
			}
		}
	}
	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////////////////////
// Get 8-bit imagedata from swapImage
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPSwapImage::Get(BYTE *pImageData) 
{
	BYTE	*p, *q;
	size_t	l;

	if (!pImageData) return FALSE;
	if (m_nOverlap==0)
	{
		// unpacked 8 bit image
		q=(BYTE *)this->m_pHostImage;
		p=pImageData;
		for (l=0; l<this->m_nSize; l++)*p++=*q++;
	}
	else
	{
    
		q=(BYTE *)this->m_pHostImage;
		// ARGB data is used to hold quadrant of gray image
		BYTE *idata = pImageData;

		// difference between image width and texture width (*4 for ARGB data)
		int overlap=m_nOverlap;
		if (overlap < 0) overlap=0;
		int wdiff = overlap * 4;
		int	swapWidth =m_nImageWidth/2 + overlap;
		int dW = m_nImageWidth / 2;
		int dH = m_nImageHeight / 2;
	
		// ARGB data will be used to hold each quadrant of the gray image
		BYTE *bidata = pImageData;
		BYTE *gidata = pImageData + dW;
		BYTE *ridata = pImageData + dH * m_nImageWidth;
		BYTE *aidata = pImageData + dH * m_nImageWidth + dW;

		// Texture pointers
		BYTE *bdata = q;
		BYTE *gdata = q + overlap * 4 + 1;
		BYTE *rdata = q + swapWidth * overlap * 4 + 2;
		BYTE *adata = q + (swapWidth * overlap + overlap) * 4 + 3;


		for (int row = 0; row < dH; row++)
		{
			for (int col = 0; col < dW; col++)
			{
				*bidata++ = *bdata;
				*gidata++ = *gdata;
				*ridata++ = *rdata;
				*aidata++ = *adata;
				adata += 4;
				rdata += 4;
				gdata += 4;
				bdata += 4;
			}
            bidata += dW;
			gidata += dW;
			ridata += dW;
			aidata += dW;

			adata += wdiff;
			rdata += wdiff;
			gdata += wdiff;
			bdata += wdiff;
		}
	}
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Get 16-bit imagedata from swapImage
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPSwapImage::Get(unsigned short *pImageData) 
{
	unsigned short	*p, *q;
	size_t			l;

	if (!pImageData) return FALSE;
	if (m_nOverlap==0)
	{
		// unpacked short image
		q=(unsigned short *)this->m_pHostImage;
		p=pImageData;
		for (l=0; l<this->m_nSize/2; l++)*p++=*q++;
	}
	else
	{
		// packed short image
		q=(unsigned short *)this->m_pHostImage;
		// ARGB data is used to hold quadrant of gray image
		unsigned short *idata = pImageData;

		// difference between image width and texture width (*4 for ARGB data)
		int overlap=m_nOverlap;
		if (overlap < 0) overlap=0;
		int wdiff = overlap * 4;
		int	swapWidth =m_nImageWidth/2 + overlap;
		int dW = m_nImageWidth / 2;
		int dH = m_nImageHeight / 2;
	
		// ARGB data will be used to hold each quadrant of the gray image
		unsigned short *ridata = pImageData;
		unsigned short *gidata = pImageData + dW;
		unsigned short *bidata = pImageData + dH * m_nImageWidth;
		unsigned short *aidata = pImageData + dH * m_nImageWidth + dW;

		// Texture pointers
		unsigned short *rdata = q + 2;
		unsigned short *gdata = q + overlap * 4 + 1;
		unsigned short *bdata = q + swapWidth * overlap * 4;
		unsigned short *adata = q + (swapWidth * overlap + overlap) * 4 + 3;


		for (int row = 0; row < dH; row++)
		{
			for (int col = 0; col < dW; col++)
			{
				*bidata++ = *bdata;
				*gidata++ = *gdata;
				*ridata++ = *rdata;
				*aidata++ = *adata;
				adata += 4;
				rdata += 4;
				gdata += 4;
				bdata += 4;
			}       
			bidata += dW;
			gidata += dW;
			ridata += dW;
			aidata += dW;
	        adata += wdiff;
			rdata += wdiff;
			gdata += wdiff;
			bdata += wdiff;
		}
	}
    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Get 3x8-bit colour imagedata from swapImage
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPSwapImage::Get(RGBTRIPLE *pImageData)  
{
	BYTE		*q;
    RGBTRIPLE	*p;
	size_t		l;

	if (!pImageData) return FALSE;
	p=pImageData;
	q=(BYTE *)this->m_pHostImage;
	for (l=0; l<this->m_nSize/4; l++)
	{
		p->rgbtRed=*q++;
		p->rgbtGreen=*q++;
		p->rgbtBlue=*q++;
        q++;
        p++;
	}
    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Get 3x16-bit colour imagedata from swapImage
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPSwapImage::Get(RGB16BTRIPLE *pImageData)  
{
	unsigned short	*q;
    RGB16BTRIPLE	*p;
	size_t			l;

	if (!pImageData) return FALSE;
	p=pImageData;
	q=(unsigned short *)this->m_pHostImage;
	for (l=0; l<this->m_nSize/4; l++)
	{
		p->rgbtRed=*q++;
		p->rgbtGreen=*q++;
		p->rgbtBlue=*q++;
        q++;
        p++;
	}
    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Get 32-bit float imagedata from swapImage
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPSwapImage::Get(float *pImageData)
{
	float *p, *q;
	size_t	l;

	if (!pImageData) return FALSE;
	q=(float *)this->m_pHostImage;
	p=pImageData;
	for (l=0; l<this->m_nSize/4; l++) *p++=*q++;
    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Gets an image component out of this texture. The image is a 24-bit tri-component image.
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPSwapImage::GetComponent(int comp, BYTE *pImageData)
{
	BYTE		*q, *p;
	size_t		l;

	if (!pImageData) return FALSE;
	p=pImageData;
	q=(BYTE *)this->m_pHostImage;
	for (l=0; l<this->m_nSize/4; l++)
	{
		*p++=*(q + comp);
        q+=4;
	}
    return TRUE;
};

///////////////////////////////////////////////////////////////////////////////////////////////
// Gets an image component out of this texture. The image is a 48-bit tri-component image.
///////////////////////////////////////////////////////////////////////////////////////////////

BOOL TIPSwapImage::GetComponent(int comp, unsigned short *pImageData)
{
	unsigned short	*q, *p;
	size_t			l;

	if (!pImageData) return FALSE;
	p=pImageData;
	q=(unsigned short *)this->m_pHostImage;
	for (l=0; l<this->m_nSize/8; l++)
	{
		*p++=*(q+comp);
        q+=4;
	}
	return TRUE;
};

