#pragma once
///////////////////////////////////////////////////////////////////////////////////////////////
// TIPSwapImage class - header file
//
///////////////////////////////////////////////////////////////////////////////////////////////

#include "TIP.h"

// macro for dll export definition - must be used in front of all data, class
// or functions you wish to export - when used on a class all of its public data and member 
// function are exported
#ifndef DllExport
	#define DllExport   __declspec( dllexport ) 
#endif

class DllExport TIPSwapImage
{
	public:

		TIPSwapImage();
		~TIPSwapImage();

		BOOL	Create(int Width, int Height, int Overlap,
					   int Bpp);							// Create a an appropriate SwapImage
		int		GetWidth();
		int		GetHeight();
		int		GetOverlap();
		int		GetBpp();

		BOOL	Transmit(void *pDest, BOOL IsCudaArray);			// Transmit SwapImage to GPU/Tesla Device
		BOOL	Receive(void *pSource, BOOL IsCudaArray);			// Receive SwapImage from GPU/Tesla Device
		BOOL	Put(BYTE *pImageData);								// Fill the SwapImage with unsigned 8 bit data         
		BOOL	Put(unsigned short *pImageData);					// Fill the SwapImage with unsigned 16 bit data          
		BOOL	Put(RGBTRIPLE *pImageData);							// Fill the SwapImage with tri-component RGB data
		BOOL	Put(RGB16BTRIPLE *pImageData);						// Fill the SwapImage with tri-component RGB (16 bit) data
		BOOL	Put(float *pImageData);								// Fill the SwapImage with 32 bit float data
		BOOL	Put(BYTE *pImageData, int w, int h, int x0,			// Fill the SwapImage with unsigned 8 bit subimage
					int y0);
		BOOL	Put(unsigned short *pImageData, int w, int h,		// Fill the SwapImage with unsigned 16 bit subimage 
					int x0, int y0);
		BOOL	Put(RGBTRIPLE *pImageData, int w, int h, int x0, 	// Fill the SwapImage with 24 bit subimage
					int y0, int ColourMode);
		BOOL	Put(RGB16BTRIPLE *pImageData, int w, int h, int x0,	// Fill the SwapImage with 48 bit subimage
					int y0, int ColourMode);
		BOOL	Get(BYTE *pImageData);								// Get unsigned  8 bit image data from the locked image
		BOOL	Get(unsigned short *pImageData);					// Get unsigned  16 bit image data from the locked image
		BOOL	Get(RGBTRIPLE *pImageData);							// Get 3 x 8 bit RGB image data from the locked image
		BOOL	Get(RGB16BTRIPLE *pImageData);						// Get 3 x 16 bit RGB image data from the locked image
		BOOL	Get(float *pImageData);								// Get 32 bit float image data from the locked image
		BOOL	GetComponent(int comp, BYTE *pImageData);			// Get unsigned 8 bit image data from the locked colour image
		BOOL	GetComponent(int comp,unsigned short *pImageData);	// Get unsigned 16 bit image data from the locked colour image

	private:
		int		m_nImageWidth;								// Actual Swap Image Dimensions (pitch is controlled by kernel)
		int		m_nImageHeight;
		int		m_nOverlap;
		int		m_nBpp;
		size_t	m_nSize;
		void*	m_pHostImage;
};
