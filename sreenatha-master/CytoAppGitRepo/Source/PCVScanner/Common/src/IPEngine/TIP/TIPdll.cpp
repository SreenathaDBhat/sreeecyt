// TIPdll.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "TIPdll.h"

HINSTANCE TIPhinst = NULL;


HINSTANCE GetTIPInstanceHandle(void)
{
    return TIPhinst;
}

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
/*
#ifdef _MANAGED
#pragma managed(push, off)
#endif
*/
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
    if (ul_reason_for_call == DLL_PROCESS_ATTACH)
        TIPhinst = hModule;

    return TRUE;
}
/*
#ifdef _MANAGED
#pragma managed(pop)
#endif
*/