#include "StdAfx.h"
#include "ccluster.h"
#include <math.h>
#include "float.h"

CClusterObj::CClusterObj(void)
{
	m_id = 0;
	m_x = 0.0;
	m_y = 0.0;
	m_dist = 0.0;
	m_total_dist = 0.0;
	m_ext_data = NULL;
	m_neighbours = new CClusterList;
}

CClusterObj::~CClusterObj(void)
{
	delete m_neighbours;
}

void CClusterObj::RemoveNeighbours(void)
{
	delete m_neighbours;
	m_neighbours = new CClusterList;
	m_total_dist = 0.0;
}

void CClusterObj::FindNeighbours(CClusterList *list, double thr_dist)
{
	POSITION pos;
	CClusterObj *obj;
	double dist;

	// Clear any existing list
	RemoveNeighbours();

	// For each object in the list, calculate distance
	// to this object, and if less than threshold, add to neighbour list
	pos = list->m_obj_list.GetHeadPosition();
	while (pos)
	{
		obj = list->m_obj_list.GetAt(pos);

		// Distance of cluster object to this object
		dist = Distance(obj);

		// If within range add as a neighbour
		if (dist < thr_dist)
		{
			// Create new object
			CClusterObj *nobj = new CClusterObj();

			// Copy id and position
			*nobj = *obj;

			// Assign distance from list object
			nobj->m_dist = dist;

			// Add as neighbour object
			m_neighbours->AddObject(nobj);

			// Accumulate total distance measures
			m_total_dist += dist;
		}
		list->m_obj_list.GetNext(pos);
	}
}

void CClusterObj::AddNeighbour(CClusterObj *obj)
{
	if (obj)
	{
		obj->m_dist = Distance(obj);
		m_neighbours->AddObject(obj);
	}
}

double CClusterObj::NearestNeighbour(CClusterObj *obj)
{
	// Return distance of nearest neighbour
	double dist, min_dist;
	CClusterObj *nobj;

	dist = Distance(obj);
	min_dist = dist;

	for (nobj = m_neighbours->GetFirstObject(); nobj; nobj = m_neighbours->GetNextObject())
	{
		dist = obj->Distance(nobj);
		if (dist < min_dist)
			min_dist = dist;
	}	
	return dist;
}


// Distance of this object to supplied object
double CClusterObj::Distance(CClusterObj *obj)
{
	double distx, disty, dist;

	distx = m_x - obj->m_x;
	distx = distx * distx;

	disty = m_y - obj->m_y;
	disty = disty * disty;

	dist = sqrt(distx + disty);

	return dist;
}

////////////////////////////////////////////////////////////////////////////////
//
//	CClusterList	-- Cluster List Functions
//
CClusterList::CClusterList(void)
{
	m_obj_list.RemoveAll();
	m_next_object_id = 0;
	m_obj_pos = NULL;
}

CClusterList::~CClusterList(void)
{
	RemoveAllObjects();
}

CClusterObj *CClusterList::AddObject(double x, double y, void *ext)
{
	CClusterObj *obj = new CClusterObj;

	if (obj)
	{
		m_next_object_id++;

		obj->m_x = x;
		obj->m_y = y;
		obj->m_id = m_next_object_id;
		obj->m_ext_data = ext;

		m_obj_list.AddTail(obj);
	}

	return obj;
}

bool CClusterList::AddObject(CClusterObj *obj)
{
	if (obj)
		m_obj_list.AddTail(obj);

	return true;
}

// Remove object with specified id from list
// Returns object or NULL if object not found
CClusterObj *CClusterList::RemoveObject(int obj_id)
{
	POSITION pos;
	CClusterObj *obj, *removed_obj = NULL;

	pos = m_obj_list.GetHeadPosition();
	while (pos)
	{
		obj = m_obj_list.GetAt(pos);
		if (obj->m_id == obj_id)
		{
			m_obj_list.RemoveAt(pos);
			removed_obj = obj;
			break;
		}
		m_obj_list.GetNext(pos);
	}

	return removed_obj;
}

void CClusterList::RemoveAllObjects()
{
	POSITION pos;
	CClusterObj *obj;

	pos = m_obj_list.GetHeadPosition();
	while(pos)
	{
		obj = m_obj_list.GetAt(pos);
		delete obj;
		m_obj_list.GetNext(pos);
	}

	m_obj_list.RemoveAll();
	m_next_object_id = 0;
}

// Remove neighbour objects from all objcts in list
void CClusterList::RemoveObjectNeighbours()
{
	CClusterObj *obj;
	POSITION pos = m_obj_list.GetHeadPosition();
	while (pos)
	{
		obj = m_obj_list.GetAt(pos);
		obj->RemoveNeighbours();
		m_obj_list.GetNext(pos);
	}
}

// Remove object with specified id from neighbour lists
void CClusterList::RemoveNeighbourObject(int obj_id)
{
	POSITION pos, npos, obj_pos;

	pos = m_obj_list.GetHeadPosition();
	while (pos)
	{
		CClusterObj *obj, *nobj;

		obj = m_obj_list.GetAt(pos);

		npos = obj->m_neighbours->m_obj_list.GetHeadPosition();
		while (npos)
		{
			nobj = obj->m_neighbours->m_obj_list.GetAt(npos);
			obj_pos = npos;
			obj->m_neighbours->m_obj_list.GetNext(npos);
			if (nobj->m_id == obj_id)
			{
				delete nobj;
				obj->m_neighbours->m_obj_list.RemoveAt(obj_pos);
			}
		}

		m_obj_list.GetNext(pos);
	}
}

CClusterObj *CClusterList::GetFirstObject()
{
	m_obj_pos = m_obj_list.GetHeadPosition();

	if (m_obj_pos)
		return m_obj_list.GetAt(m_obj_pos);
	else
		return NULL;
}

CClusterObj *CClusterList::GetNextObject()
{
	if (m_obj_pos)
		m_obj_list.GetNext(m_obj_pos);

	if (m_obj_pos)
		return m_obj_list.GetAt(m_obj_pos);
	else
		return NULL;
}

// Find object with maximum number of neighbours in the list and return the number
int CClusterList::MaxNeighbours()
{
	POSITION pos;
	int max_nn = 0;
	POSITION obj_pos = NULL;

	pos = m_obj_list.GetHeadPosition();
	while (pos)
	{
		CClusterObj *obj;
		int nn;

		obj = m_obj_list.GetAt(pos);
		nn = obj->m_neighbours->m_obj_list.GetCount();

		if (nn > max_nn)
		{
			max_nn = nn;
			obj_pos = pos;
		}

		m_obj_list.GetNext(pos);
	}

	return max_nn;
}

CClusterObj *CClusterList::MinTotalDistance(int max_neighbours)
{
	POSITION pos;
	double min_total_dist = DBL_MAX;
	CClusterObj *min_obj = NULL;

	// Must be more than just itself
	if (max_neighbours < 2)
		return NULL;

	pos = m_obj_list.GetHeadPosition();
	while (pos)
	{
		CClusterObj *obj;
		int nn;

		obj = m_obj_list.GetAt(pos);
		nn = obj->m_neighbours->m_obj_list.GetCount();

		// If max count, look for smallest total distance
		if (nn == max_neighbours)
		{
			if (obj->m_total_dist < min_total_dist)
			{
				min_total_dist = obj->m_total_dist;
				min_obj = obj;
			}
		}

		m_obj_list.GetNext(pos);
	}

	// Return the object
	return min_obj;

}