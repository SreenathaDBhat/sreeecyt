/////////////////////////////////////////////////////////////////////////////////////////////////////
// DCT computations - KR040307
/////////////////////////////////////////////////////////////////////////////////////////////////////
//
/// NOTES there are faster algorithms out there !
//
//

#include "stdafx.h"
#include <math.h>
#include <float.h>
#include "cdct.h"

CDCT::CDCT(int N, int W, int H)
{
    m_Size = N;
    m_W = W;
    m_H = H;

    m_pCosBuf = new double[N * N];

    ComputeDCTBasis();
}

CDCT::~CDCT(void)
{
    delete m_pCosBuf;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
// computes the DCT basis functions
/////////////////////////////////////////////////////////////////////////////////////////////////////

void CDCT::ComputeDCTBasis(void)
{
#ifndef M_PI
#define M_PI 3.1415926535897932385
#endif
	//
	// compute the table of basis functions and
	// store the result in the GBuffer cosBuffer
	//
	for (int m = 0; m < m_Size; ++m)
	{
		for (int n = 0; n < m_Size; ++n)
		{
			double result;
			if (n == 0)
				result = sqrt(1.0 / m_Size) * cos(((2 * m + 1) * n * M_PI) / (2.0 * m_Size));
			else
				result = sqrt(2.0 / m_Size) * cos(((2 * m + 1) * n * M_PI) / (2.0 * m_Size));

			m_pCosBuf[m + m_Size * n] = result;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////   
// Computes the forward DCT will allocate a buffer of W by H doubles which should be freed 
// separately
/////////////////////////////////////////////////////////////////////////////////////////////////////   

void CDCT::ForwardDCT(double **pDCTBuffer, BYTE *pImgBuffer)
{
	long i, Sz = (m_W + m_Size) * (m_H + m_Size);
	*pDCTBuffer = new double[Sz];

	for (i = 0; i < Sz; i++) 
		(*pDCTBuffer)[i] = 0.0;

	// intermediate result (an NxN block)
	double *pDctBlock;

	pDctBlock = new double[m_Size * m_Size];

	// for every NxN block of the image
	for (int y = 0; y < m_H - m_Size; y += m_Size)
	{
		for (int x = 0; x < m_W - m_Size; x += m_Size)
		{
			// do the first matrix multiplication
			// ([cosBuffer]' [imgBuffer])
			// and store the results in dctBlock
			for (int col = 0; col < m_Size; ++col)
			{
				for (int row = 0; row < m_Size; ++row)
				{
					double result = 0.0;
					for (int index = 0; index < m_Size; ++index)
					{
						// note the use of symmetric extension
						BYTE pixel = pImgBuffer[x + index + (y + row) * m_W];

						double cosVal_transpose = m_pCosBuf[index + col * m_Size];

						result += cosVal_transpose * pixel;
					}

					pDctBlock[col + row * m_Size] = result;
				}
			}
			// do the second matrix multiplication
			// ([dctBlock] [cosBuffer])
			// and store the results in dctBuffer
			for (int col2 = 0; col2 < m_Size; ++col2)
			{
				for (int row2 = 0; row2 < m_Size; ++row2)
				{
					double result = 0.0;
					for (int index = 0; index < m_Size; ++index)
					{
						double coeff = pDctBlock[col2 + index * m_Size]; 
						double cosVal = m_pCosBuf[index + row2 * m_Size];

						result += coeff * cosVal;
					}

					(*pDCTBuffer)[x + col2 + (y + row2) * m_W] = result;
				}
			}
		}
	}

	delete pDctBlock;
}

void CDCT::ForwardDCT(double **pDCTBuffer, WORD *pImgBuffer)
{
	long i, Sz = (m_W + m_Size) * (m_H + m_Size);
	*pDCTBuffer = new double[Sz];

	for (i = 0; i < Sz; i++) 
		(*pDCTBuffer)[i] = 0.0;

	// intermediate result (an NxN block)
	double *pDctBlock;

	pDctBlock = new double[m_Size * m_Size];

	// for every NxN block of the image
	for (int y = 0; y < m_H - m_Size; y += m_Size)
	{
		for (int x = 0; x < m_W - m_Size; x += m_Size)
		{
			// do the first matrix multiplication
			// ([cosBuffer]' [imgBuffer])
			// and store the results in dctBlock
			for (int col = 0; col < m_Size; ++col)
			{
				for (int row = 0; row < m_Size; ++row)
				{
					double result = 0.0;
					for (int index = 0; index < m_Size; ++index)
					{
						// note the use of symmetric extension
						WORD pixel = pImgBuffer[x + index + (y + row) * m_W];

						double cosVal_transpose = m_pCosBuf[index + col * m_Size];

						result += cosVal_transpose * pixel;
					}

					pDctBlock[col + row * m_Size] = result;
				}
			}
			// do the second matrix multiplication
			// ([dctBlock] [cosBuffer])
			// and store the results in dctBuffer
			for (int col2 = 0; col2 < m_Size; ++col2)
			{
				for (int row2 = 0; row2 < m_Size; ++row2)
				{
					double result = 0.0;
					for (int index = 0; index < m_Size; ++index)
					{
						double coeff = pDctBlock[col2 + index * m_Size]; 
						double cosVal = m_pCosBuf[index + row2 * m_Size];

						result += coeff * cosVal;
					}

					(*pDCTBuffer)[x + col2 + (y + row2) * m_W] = result;
				}
			}
		}
	}

	delete pDctBlock;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////   
// Computes the forward DCT will allocate a buffer of W by H doubles which should be freed 
// separately
/////////////////////////////////////////////////////////////////////////////////////////////////////   

void CDCT::ForwardDCT8(double **pDCTBuffer, BYTE *pImgBuffer)
{
	long i, Sz = (m_W + 8) * (m_H + 8);
	*pDCTBuffer = new double[Sz];

	for (i = 0; i < Sz; i++) 
		(*pDCTBuffer)[i] = 0.0;

	// intermediate result (an NxN block)
	double DctBlock[64], result;
	int c8, xyrw;

	// for every NxN block of the image
	for (int y = 0; y < m_H - 8; y += 8)
	{
		for (int x = 0; x < m_W - 8; x += 8)
		{
			// do the first matrix multiplication
			// ([cosBuffer]' [imgBuffer])
			// and store the results in dctBlock
			for (int col = 0; col < 8; ++col)
			{
				c8 = col * 8;

				xyrw = x + (y + 0) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col] = result;

				xyrw = x + (y + 1) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col + 8] = result;

				xyrw = x + (y + 2) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col + 16] = result;

				xyrw = x + (y + 3) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col + 24] = result;

				xyrw = x + (y + 4) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col + 32] = result;

				xyrw = x + (y + 5) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col + 40] = result;

				xyrw = x + (y + 6) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col + 48] = result;

				xyrw = x + (y + 7) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col + 56] = result;
			}
			// do the second matrix multiplication
			// ([dctBlock] [cosBuffer])
			// and store the results in dctBuffer
			for (int col2 = 0; col2 < 8; ++col2)
			{
					result  = DctBlock[col2 + 0]  * m_pCosBuf[0];
					result += DctBlock[col2 + 8]  * m_pCosBuf[1];
					result += DctBlock[col2 + 16] * m_pCosBuf[2];
					result += DctBlock[col2 + 24] * m_pCosBuf[3];
					result += DctBlock[col2 + 32] * m_pCosBuf[4];
					result += DctBlock[col2 + 40] * m_pCosBuf[5];
					result += DctBlock[col2 + 48] * m_pCosBuf[6];
					result += DctBlock[col2 + 56] * m_pCosBuf[7];

					(*pDCTBuffer)[x + col2 + y * m_W] = result;

					result  = DctBlock[col2 + 0]  * m_pCosBuf[8];
					result += DctBlock[col2 + 8]  * m_pCosBuf[9];
					result += DctBlock[col2 + 16] * m_pCosBuf[10];
					result += DctBlock[col2 + 24] * m_pCosBuf[11];
					result += DctBlock[col2 + 32] * m_pCosBuf[12];
					result += DctBlock[col2 + 40] * m_pCosBuf[13];
					result += DctBlock[col2 + 48] * m_pCosBuf[14];
					result += DctBlock[col2 + 56] * m_pCosBuf[15];

					(*pDCTBuffer)[x + col2 + (y + 1) * m_W] = result;

					result  = DctBlock[col2 + 0]  * m_pCosBuf[16];
					result += DctBlock[col2 + 8]  * m_pCosBuf[17];
					result += DctBlock[col2 + 16] * m_pCosBuf[18];
					result += DctBlock[col2 + 24] * m_pCosBuf[19];
					result += DctBlock[col2 + 32] * m_pCosBuf[20];
					result += DctBlock[col2 + 40] * m_pCosBuf[21];
					result += DctBlock[col2 + 48] * m_pCosBuf[22];
					result += DctBlock[col2 + 56] * m_pCosBuf[23];

					(*pDCTBuffer)[x + col2 + (y + 2) * m_W] = result;

					result  = DctBlock[col2 + 0]  * m_pCosBuf[24];
					result += DctBlock[col2 + 8]  * m_pCosBuf[25];
					result += DctBlock[col2 + 16] * m_pCosBuf[26];
					result += DctBlock[col2 + 24] * m_pCosBuf[27];
					result += DctBlock[col2 + 32] * m_pCosBuf[28];
					result += DctBlock[col2 + 40] * m_pCosBuf[29];
					result += DctBlock[col2 + 48] * m_pCosBuf[30];
					result += DctBlock[col2 + 56] * m_pCosBuf[31];

					(*pDCTBuffer)[x + col2 + (y + 3) * m_W] = result;				

					result  = DctBlock[col2 + 0]  * m_pCosBuf[32];
					result += DctBlock[col2 + 8]  * m_pCosBuf[33];
					result += DctBlock[col2 + 16] * m_pCosBuf[34];
					result += DctBlock[col2 + 24] * m_pCosBuf[35];
					result += DctBlock[col2 + 32] * m_pCosBuf[36];
					result += DctBlock[col2 + 40] * m_pCosBuf[37];
					result += DctBlock[col2 + 48] * m_pCosBuf[38];
					result += DctBlock[col2 + 56] * m_pCosBuf[39];

					(*pDCTBuffer)[x + col2 + (y + 4) * m_W] = result;

					result  = DctBlock[col2 + 0]  * m_pCosBuf[40];
					result += DctBlock[col2 + 8]  * m_pCosBuf[41];
					result += DctBlock[col2 + 16] * m_pCosBuf[42];
					result += DctBlock[col2 + 24] * m_pCosBuf[43];
					result += DctBlock[col2 + 32] * m_pCosBuf[44];
					result += DctBlock[col2 + 40] * m_pCosBuf[45];
					result += DctBlock[col2 + 48] * m_pCosBuf[46];
					result += DctBlock[col2 + 56] * m_pCosBuf[47];

					(*pDCTBuffer)[x + col2 + (y + 5) * m_W] = result;

					result  = DctBlock[col2 + 0]  * m_pCosBuf[48];
					result += DctBlock[col2 + 8]  * m_pCosBuf[49];
					result += DctBlock[col2 + 16] * m_pCosBuf[50];
					result += DctBlock[col2 + 24] * m_pCosBuf[51];
					result += DctBlock[col2 + 32] * m_pCosBuf[52];
					result += DctBlock[col2 + 40] * m_pCosBuf[53];
					result += DctBlock[col2 + 48] * m_pCosBuf[54];
					result += DctBlock[col2 + 56] * m_pCosBuf[55];

					(*pDCTBuffer)[x + col2 + (y + 6) * m_W] = result;

					result  = DctBlock[col2 + 0]  * m_pCosBuf[56];
					result += DctBlock[col2 + 8]  * m_pCosBuf[57];
					result += DctBlock[col2 + 16] * m_pCosBuf[58];
					result += DctBlock[col2 + 24] * m_pCosBuf[59];
					result += DctBlock[col2 + 32] * m_pCosBuf[60];
					result += DctBlock[col2 + 40] * m_pCosBuf[61];
					result += DctBlock[col2 + 48] * m_pCosBuf[62];
					result += DctBlock[col2 + 56] * m_pCosBuf[63];

					(*pDCTBuffer)[x + col2 + (y + 7) * m_W] = result;
			}
		}
	}
}

void CDCT::ForwardDCT8(double **pDCTBuffer, WORD *pImgBuffer)
{
	long i, Sz = (m_W + 8) * (m_H + 8);
	*pDCTBuffer = new double[Sz];

	for (i = 0; i < Sz; i++) 
		(*pDCTBuffer)[i] = 0.0;

	// intermediate result (an NxN block)
	double DctBlock[64], result;
	int c8, xyrw;

	// for every NxN block of the image
	for (int y = 0; y < m_H - 8; y += 8)
	{
		for (int x = 0; x < m_W - 8; x += 8)
		{
			// do the first matrix multiplication
			// ([cosBuffer]' [imgBuffer])
			// and store the results in dctBlock
			for (int col = 0; col < 8; ++col)
			{
				c8 = col * 8;

				xyrw = x + (y + 0) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col] = result;

				xyrw = x + (y + 1) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col + 8] = result;

				xyrw = x + (y + 2) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col + 16] = result;

				xyrw = x + (y + 3) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col + 24] = result;

				xyrw = x + (y + 4) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col + 32] = result;

				xyrw = x + (y + 5) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col + 40] = result;

				xyrw = x + (y + 6) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col + 48] = result;

				xyrw = x + (y + 7) * m_W;
				result  = m_pCosBuf[0 + c8] * pImgBuffer[xyrw + 0];
				result += m_pCosBuf[1 + c8] * pImgBuffer[xyrw + 1];
				result += m_pCosBuf[2 + c8] * pImgBuffer[xyrw + 2];
				result += m_pCosBuf[3 + c8] * pImgBuffer[xyrw + 3];
				result += m_pCosBuf[4 + c8] * pImgBuffer[xyrw + 4];
				result += m_pCosBuf[5 + c8] * pImgBuffer[xyrw + 5];
				result += m_pCosBuf[6 + c8] * pImgBuffer[xyrw + 6];
				result += m_pCosBuf[7 + c8] * pImgBuffer[xyrw + 7];
				DctBlock[col + 56] = result;
			}
			// do the second matrix multiplication
			// ([dctBlock] [cosBuffer])
			// and store the results in dctBuffer
			for (int col2 = 0; col2 < 8; ++col2)
			{
					result  = DctBlock[col2 + 0]  * m_pCosBuf[0];
					result += DctBlock[col2 + 8]  * m_pCosBuf[1];
					result += DctBlock[col2 + 16] * m_pCosBuf[2];
					result += DctBlock[col2 + 24] * m_pCosBuf[3];
					result += DctBlock[col2 + 32] * m_pCosBuf[4];
					result += DctBlock[col2 + 40] * m_pCosBuf[5];
					result += DctBlock[col2 + 48] * m_pCosBuf[6];
					result += DctBlock[col2 + 56] * m_pCosBuf[7];

					(*pDCTBuffer)[x + col2 + y * m_W] = result;

					result  = DctBlock[col2 + 0]  * m_pCosBuf[8];
					result += DctBlock[col2 + 8]  * m_pCosBuf[9];
					result += DctBlock[col2 + 16] * m_pCosBuf[10];
					result += DctBlock[col2 + 24] * m_pCosBuf[11];
					result += DctBlock[col2 + 32] * m_pCosBuf[12];
					result += DctBlock[col2 + 40] * m_pCosBuf[13];
					result += DctBlock[col2 + 48] * m_pCosBuf[14];
					result += DctBlock[col2 + 56] * m_pCosBuf[15];

					(*pDCTBuffer)[x + col2 + (y + 1) * m_W] = result;

					result  = DctBlock[col2 + 0]  * m_pCosBuf[16];
					result += DctBlock[col2 + 8]  * m_pCosBuf[17];
					result += DctBlock[col2 + 16] * m_pCosBuf[18];
					result += DctBlock[col2 + 24] * m_pCosBuf[19];
					result += DctBlock[col2 + 32] * m_pCosBuf[20];
					result += DctBlock[col2 + 40] * m_pCosBuf[21];
					result += DctBlock[col2 + 48] * m_pCosBuf[22];
					result += DctBlock[col2 + 56] * m_pCosBuf[23];

					(*pDCTBuffer)[x + col2 + (y + 2) * m_W] = result;

					result  = DctBlock[col2 + 0]  * m_pCosBuf[24];
					result += DctBlock[col2 + 8]  * m_pCosBuf[25];
					result += DctBlock[col2 + 16] * m_pCosBuf[26];
					result += DctBlock[col2 + 24] * m_pCosBuf[27];
					result += DctBlock[col2 + 32] * m_pCosBuf[28];
					result += DctBlock[col2 + 40] * m_pCosBuf[29];
					result += DctBlock[col2 + 48] * m_pCosBuf[30];
					result += DctBlock[col2 + 56] * m_pCosBuf[31];

					(*pDCTBuffer)[x + col2 + (y + 3) * m_W] = result;				

					result  = DctBlock[col2 + 0]  * m_pCosBuf[32];
					result += DctBlock[col2 + 8]  * m_pCosBuf[33];
					result += DctBlock[col2 + 16] * m_pCosBuf[34];
					result += DctBlock[col2 + 24] * m_pCosBuf[35];
					result += DctBlock[col2 + 32] * m_pCosBuf[36];
					result += DctBlock[col2 + 40] * m_pCosBuf[37];
					result += DctBlock[col2 + 48] * m_pCosBuf[38];
					result += DctBlock[col2 + 56] * m_pCosBuf[39];

					(*pDCTBuffer)[x + col2 + (y + 4) * m_W] = result;

					result  = DctBlock[col2 + 0]  * m_pCosBuf[40];
					result += DctBlock[col2 + 8]  * m_pCosBuf[41];
					result += DctBlock[col2 + 16] * m_pCosBuf[42];
					result += DctBlock[col2 + 24] * m_pCosBuf[43];
					result += DctBlock[col2 + 32] * m_pCosBuf[44];
					result += DctBlock[col2 + 40] * m_pCosBuf[45];
					result += DctBlock[col2 + 48] * m_pCosBuf[46];
					result += DctBlock[col2 + 56] * m_pCosBuf[47];

					(*pDCTBuffer)[x + col2 + (y + 5) * m_W] = result;

					result  = DctBlock[col2 + 0]  * m_pCosBuf[48];
					result += DctBlock[col2 + 8]  * m_pCosBuf[49];
					result += DctBlock[col2 + 16] * m_pCosBuf[50];
					result += DctBlock[col2 + 24] * m_pCosBuf[51];
					result += DctBlock[col2 + 32] * m_pCosBuf[52];
					result += DctBlock[col2 + 40] * m_pCosBuf[53];
					result += DctBlock[col2 + 48] * m_pCosBuf[54];
					result += DctBlock[col2 + 56] * m_pCosBuf[55];

					(*pDCTBuffer)[x + col2 + (y + 6) * m_W] = result;

					result  = DctBlock[col2 + 0]  * m_pCosBuf[56];
					result += DctBlock[col2 + 8]  * m_pCosBuf[57];
					result += DctBlock[col2 + 16] * m_pCosBuf[58];
					result += DctBlock[col2 + 24] * m_pCosBuf[59];
					result += DctBlock[col2 + 32] * m_pCosBuf[60];
					result += DctBlock[col2 + 40] * m_pCosBuf[61];
					result += DctBlock[col2 + 48] * m_pCosBuf[62];
					result += DctBlock[col2 + 56] * m_pCosBuf[63];

					(*pDCTBuffer)[x + col2 + (y + 7) * m_W] = result;
			}
		}
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////////   
// Convert the DCT into an image
/////////////////////////////////////////////////////////////////////////////////////////////////////   

void CDCT::DCTToImage(double *pDCTBuffer, BYTE *pImgBuffer)
{
    double Max, Min;

    Max = DBL_MIN;
    Min = DBL_MAX;

    double *pD = pDCTBuffer;
    BYTE *pIm = pImgBuffer;
    int i;

    for (i = 0; i < m_W * m_H; i++)
    {
        if (*pD < Min)
            Min = *pD;
        if (*pD > Max)
            Max = *pD;
        pD++;
    }

    pD = pDCTBuffer;

    if (Max - Min != 0.)
    {
        for (i = 0; i < m_W * m_H; i++)
        {
            *pIm++ = (BYTE)((((*pD++) - Min) * 255.0) / (Max - Min));
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////   
// computes the inverse DCT
/////////////////////////////////////////////////////////////////////////////////////////////////////   
    
void CDCT::InverseDCT(BYTE *ImgBuffer, double *pDCTBuffer)
{
	// intermediate result (an NxN block)
	double *pDctBlock;

	pDctBlock = new double[m_Size * m_Size];

	// for every NxN block of the coefficients
	//
	for (int y = 0; y < m_H; y += m_Size)
	{
		for (int x = 0; x < m_W; x += m_Size)
		{
			// do the first matrix multiplication
			// ([cosBuffer][dctBuffer])
			// and store the results in dctBlock
			for (int col = 0; col < m_Size; ++col)
			{
				for (int row = 0; row < m_Size; ++row)
				{
					double result = 0.0;
					for (int index = 0; index < m_Size; ++index)
					{
						double coeff = pDCTBuffer[x + index + (y + row) * m_W];
						double cosVal = m_pCosBuf[col + index * m_Size];

						result += cosVal * coeff;
					}
					pDctBlock[col + row * m_Size] = result;
				}
			}
			// do the second matrix multiplication
			// ([dctBlock][cosBuffer]')
			// and store the results in imgBuffer
			for (int col2 = 0; col2 < m_Size; ++col2)
			{
				for (int row2 = 0; row2 < m_Size; ++row2)
				{
					double result = 0.0;
					for (int index = 0; index < m_Size; ++index)
					{
						double coeff = pDctBlock[col2 + index * m_Size];
						double cosVal_transpose = m_pCosBuf[row2 + index * m_Size];

						result += coeff * cosVal_transpose;
					}

					//
					// assign the computed value back to the image
					BYTE new_pixel = CLIP(static_cast<int>(result + 0.5));

					ImgBuffer[x + col2 + (y + row2) * m_W] = new_pixel;
				}
			}
		}
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////////   
// computes the inverse DCT
/////////////////////////////////////////////////////////////////////////////////////////////////////   
    
void CDCT::InverseDCT(WORD *ImgBuffer, double *pDCTBuffer)
{
	// intermediate result (an NxN block)
	double *pDctBlock;

	pDctBlock = new double[m_Size * m_Size];

	// for every NxN block of the coefficients
	//
	for (int y = 0; y < m_H; y += m_Size)
	{
		for (int x = 0; x < m_W; x += m_Size)
		{
			// do the first matrix multiplication
			// ([cosBuffer][dctBuffer])
			// and store the results in dctBlock
			for (int col = 0; col < m_Size; ++col)
			{
				for (int row = 0; row < m_Size; ++row)
				{
					double result = 0.0;
					for (int index = 0; index < m_Size; ++index)
					{
						double coeff = pDCTBuffer[x + index + (y + row) * m_W];
						double cosVal = m_pCosBuf[col + index * m_Size];

						result += cosVal * coeff;
					}
					pDctBlock[col + row * m_Size] = result;
				}
			}
			// do the second matrix multiplication
			// ([dctBlock][cosBuffer]')
			// and store the results in imgBuffer
			for (int col2 = 0; col2 < m_Size; ++col2)
			{
				for (int row2 = 0; row2 < m_Size; ++row2)
				{
					double result = 0.0;
					for (int index = 0; index < m_Size; ++index)
					{
						double coeff = pDctBlock[col2 + index * m_Size];
						double cosVal_transpose = m_pCosBuf[row2 + index * m_Size];

						result += coeff * cosVal_transpose;
					}

					//
					// assign the computed value back to the image
					WORD new_pixel = CLIPW(static_cast<int>(result + 0.5));

					ImgBuffer[x + col2 + (y + row2) * m_W] = new_pixel;
				}
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////   
// Utility function to quantize the DCT coefficients
/////////////////////////////////////////////////////////////////////////////////////////////////////   

void CDCT::MultiplyCoeffs(double *pDCTBuffer)
{
    // Filter out the higher frequencies
	
    double slice_filter_freq_response[][8] = 
        {
        {1.0, 1.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0},
        {1.0, 1.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0},
        {0.5, 0.5, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0},
        {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
        {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
        {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
        {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
        {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}
        };

/*
	 double slice_filter_freq_response[][8] = 
        {
        {0.0, 0.0, 0.5, 1.0, 1.0, 1.0, 1.0, 1.0},
        {0.0, 0.0, 0.5, 1.0, 1.0, 1.0, 1.0, 1.0},
        {0.5, 0.5, 0.5, 1.0, 1.0, 1.0, 1.0, 1.0},
        {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0},
        {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0},
        {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0},
        {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0},
        {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0}
        };
     */
  
   //
   // for every NxN block of the coefficients
   //
   for (int y = 0; y < m_H; y += m_Size)
   {
      for (int x = 0; x < m_W; x += m_Size)
      {
         for (int col = 0; col < m_Size; ++col)
         {
            for (int row = 0; row < m_Size; ++row)
            {
               double coeff = pDCTBuffer[x + col + (y + row) * m_W];
               coeff *= slice_filter_freq_response[col][row];
              
               pDCTBuffer[x + col + (y + row) * m_W] = coeff;
            }
         }
      }
   }
}



