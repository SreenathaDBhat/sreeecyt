////////////////////////////////////////////////////////////////////////////
//
// Performs thinning on image 
//
// Ported from an old library 
// NB This could be optimised quite a bit when someone has the time....
//
// Written By Karl Ratcliff   03112001
//                            
////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "vcmemrect.h"
#include "cthin.h"
#include "uts.h"

#define DFLTMAXK 3              // dflt maximum thinning kernel size 
#define MAXMAXK 21              // max of the maximum thinning kernel size 
#define MAXITER 20              // maximum number of iterations 
#define MIN0RUN 5               // minimum run of zeros to run-length code 

#define IMAGE(x, y)      m_pImage[x + y * m_nPitch]


CThin::CThin(CUTSMemRect *pMR) 
{
	pUTSMemRect = pMR;
}

/////////////////////////////////////////////////////////////////////////////////////
// Thin an image specified by UTSHandle for a number of iterations the maximum of 
// which is specified by MaxIter
/////////////////////////////////////////////////////////////////////////////////////

long CThin::ThinImage(long Handle, long MaxIter)
{
	long x, y,                      // image coordinates 
		i, k;                       // sidelength of current thinning kernel 
	long maxK,                      // max. sidelength of thinning kernel 
		change[MAXMAXK],            // no. erasures for each mask size 
		nIter,                      // no. iterations 
		nChange,                    // no. thinning operations on iteration 
		nONs,                       // total ONs in original image 
		nErased;                    // cumulative no. ERASED in image 
	long BPP;
    BYTE *ring, *side, *original;
	long NewHandle, W, H, P;

	BPP = pUTSMemRect->GetExtendedInfo(Handle, &W, &H, &P, &original);
	NewHandle = pUTSMemRect->Create(BPP, W, H);
	BPP = pUTSMemRect->GetExtendedInfo(NewHandle, &m_nWidth, &m_nHeight, &m_nPitch, &m_pImage);
	memcpy(m_pImage, original, m_nHeight * m_nPitch);

	m_nySizeM1 = m_nHeight - 1;
	maxK = DFLTMAXK;
	
	m_pxRun = (long **) new long[m_nHeight];
	if (m_pxRun == NULL) 
		return UTS_ERROR;

	ring = new BYTE[sizeof(long) * (maxK - 1)];
	if (ring == NULL) 
		return UTS_ERROR;

	side = new BYTE[maxK];
	if (side == NULL) 
		return UTS_ERROR;
	
	// initialize ON/OFF/ERASED values 
	OFF = 0;
	ERASED = 1;
	ON = 255;
	PON = ON - 1;

	// zero image borders 
	for (y = 0; y < m_nHeight; y++)
	{
		IMAGE(0, y) = 0;
		IMAGE(m_nWidth, y) = 0;
	}
	for (x = 0; x < m_nWidth; x++)
	{
		IMAGE(x, 0) = 0;
		m_pImage[x + (m_nHeight - 1) * m_nPitch] = 0;
	}

	for (k = 0; k < MAXMAXK; k++)
		change[k] = 0;
	
	// on first iteration, perform thinning and accumulate information
	// on x-runs 
	nChange = peel0(ring, side, maxK, change, &nONs, &nErased);
	
	for (i = 3; i <= maxK; i++) 
		change[i] = 0;
	
	ERASED += 2;
	
	// iteratively convolve through image until thinned 
	for (nIter = 1, nChange = 1; (nChange > 0) && nIter <= MaxIter; nIter++) 
	{
		nChange = peel(ring, side, maxK, change, &nErased);
		for (i = 3; i <= maxK; i++) 
			change[i] = 0;
		ERASED += 2;
	}
	
	for (y = 1; y < m_nySizeM1; y++)
	{
		for (x = 1; x < m_nWidth - 1; x++)
			IMAGE(x, y) = (IMAGE(x, y) < PON) ? OFF : ON;
	}	  

	delete ring;
	delete side;
	for (i = 1; i < m_nySizeM1; i++) free(m_pxRun[i]);
	delete m_pxRun;
	
	return NewHandle;
}

/////////////////////////////////////////////////////////////////////////////////////
// PEEL0:       function performs first thinning iteration where information
//              on x-runs of ONs is accumulated
/////////////////////////////////////////////////////////////////////////////////////

long CThin::peel0(BYTE *ring, BYTE *side, long maxK, long *change, long *nONs, long *nErased)
{
	long x, y,                      // image coordinates 
		iXRun,                      // index of runs in x 
		xSizeM1,                    // imgSize.x minus 1 
		k;                          // sidelength of current thinning kernel 
	long onRun,                     // flag = 1 for run of 1s; 0 for 0s 
		permOn,                     // permanent ON flag set to 1 if so 
		kM1,                        // k minus 1 
		nChange;                    // no. thinning operations on iteration 
	BYTE B;
	
	*nONs = *nErased = nChange = 0;
	xSizeM1 = m_nWidth - 1;
	for (y = 1; y < m_nySizeM1; y++) 
	{
		m_pxRun[y] = (long *) calloc (m_nWidth + 1, sizeof (long));
		m_pxRun[y][0] = -MIN0RUN;
		for (x = 1, iXRun = 1, onRun = 0; x < xSizeM1; x++) 
		{
			permOn = 0;
			B = IMAGE(x, y);
			if (B < ERASED) 
			{
				if (onRun == 1) 
				{
					onRun = 0;
					m_pxRun[y][iXRun++] = x - 1;
				}
				k = 0;
			}
			else 
			{
				if (onRun == 0) 
				{
					onRun = 1;
					if ((x - m_pxRun[y][iXRun - 1]) < MIN0RUN)
						--iXRun;
					else
						m_pxRun[y][iXRun++] = x;
				}
				(*nONs)++;
				k = ksize (x, y, maxK);
			}
			kM1 = (k > 3) ? k - 1 : 3;
			while (k >= kM1) 
			{
				if (sqron(x, y, k) == 0)
					break;
				if (getring(x, y, k, ring) == 1)
					break;
				if (thinring(ring, k, (int *) &permOn) == 1) 
				{
					if (chkconnect(ring, side, k) == 1) 
					{
						nChange++;
						(change[k])++;
						erasesqr(x, y, k, anchor(ring, k), nErased);
						break;
					}
				}
				--k;
			}
			if (permOn == 1)
				IMAGE(x, y) = PON;
		}

		--iXRun;
		if (iXRun % 2 != 0)
			m_pxRun[y][++iXRun] = x;

		m_pxRun[y][0] = iXRun;
		m_pxRun[y] = (long *) realloc (m_pxRun[y], (sizeof (long)) * (iXRun + 1));
	}

	return nChange;
}

/////////////////////////////////////////////////////////////////////////////////////
//   PEEL:        function performs an iteration of thinning
/////////////////////////////////////////////////////////////////////////////////////

long CThin::peel(BYTE *ring, BYTE *side, long maxK, long *change, long *nErased)
{
	register long x, y,             // image coordinates 
		xEnd, iXRun,                // index of runs in x 
		k;                          // sidelength of current thinning kernel 
	long noOnsInRow,                // flag = 1 if no ONs in y-row (0 if ONs) 
		permOn,                     // permanent ON flag set to 1 if so 
		kM1,                        // k minus 1 
		nChange;                    // no. thinning operations on iteration 
	
	nChange = 0;
	for (y = 1; y < m_nySizeM1; y++) 
	{
		for (iXRun = 1, noOnsInRow = 1; iXRun <= m_pxRun[y][0]; iXRun += 2) 
		{
			xEnd = m_pxRun[y][iXRun + 1];
			for (x = m_pxRun[y][iXRun]; x <= xEnd; x++) 
			{
				permOn = 0;
				if (IMAGE(x, y) < ERASED)
					k = 0;
				else
					k = ksize(x, y, maxK);
				kM1 = (k > 3) ? k - 1 : 3;
				while (k >= kM1) 
				{
					if (sqron(x, y, k) == 0)
						break;
					noOnsInRow = 0;
					if (getring(x, y, k, ring) == 1)
						break;
					if (thinring(ring, k, (int *) &permOn) == 1) 
					{
						if (chkconnect(ring, side, k) == 1)
						{
							nChange++;
							(change[k])++;
							erasesqr(x, y, k, anchor(ring, k), nErased);
							break;
						}
					}
					--k;
				}
				if (permOn == 1)
					IMAGE(x, y) = PON;
			}
		}
		if (noOnsInRow == 1)
			m_pxRun[y][0] = -m_pxRun[y][0];
	}
	return (nChange);
}

/////////////////////////////////////////////////////////////////////////////////////
// KSIZE:       function determines k, where kxk is largest square
//              around (x,y) which contains all ON or ERASED
/////////////////////////////////////////////////////////////////////////////////////

long CThin::ksize(long x, long y, long maxK)
{
	long xMask, yMask,            // x,y mask coordinates 
		xEnd, yEnd,                 // end coord.s of square 
		k;                          // mask size 
	long upHalf, downHalf,        // half of mask below and above center 
		xStart,                     // x- start and end of square 
		yStart;                     // y- start and end of square 
	
	for (k = 4; k <= maxK; k++) 
	{
		if (k % 2 == 1)
			downHalf = upHalf = (k - 3) / 2;
		else 
		{
			upHalf = (k - 2) / 2;
			downHalf = (k - 4) / 2;
		}
		xStart = x - downHalf;
		xEnd = x + upHalf;
		yStart = y - downHalf;
		yEnd = y + upHalf;
		for (yMask = yStart; yMask <= yEnd; yMask++)
			for (xMask = xStart; xMask <= xEnd; xMask++)
				if (IMAGE(xMask, yMask) < ERASED)
					return (k - 1);
	}
	return (maxK);
}

/////////////////////////////////////////////////////////////////////////////////////
// SQRON:       function tests pixels in kxk square are already erased or 
//              for 3x3 if they can never be erased
//                      usage: flag = sqron (x, y, k)
//              flag = 0 if cannot erase any pixels in square
//                   = 1 if at least one pixel is still ON
/////////////////////////////////////////////////////////////////////////////////////

long CThin::sqron(long x, long y, long k)
{
	long xEnd, yEnd,              // upper bounds of center erase area 
		erasedP1;                // ERASED + 1 
	long upHalf, downHalf,        // half of mask below and above center 
		yStart, xStart;          // bounds of center erase area 
	
	// check for 3x3 
	if (k == 3) 
	{
		if (IMAGE(x, y) == ON)
			return 1;
		else
			return 0;
	}
	
	// check center square 
	if (k % 2 == 1)
		downHalf = upHalf = (k - 3) / 2;
	else 
	{
		upHalf = (k - 2) / 2;
		downHalf = (k - 4) / 2;
	}

	xStart = x - downHalf;
	xEnd = x + upHalf;
	yStart = y - downHalf;
	yEnd = y + upHalf;
	erasedP1 = ERASED + 1;
	for (y = yStart; y <= yEnd; y++)
	{
		for (x = xStart; x <= xEnd; x++)
			if (IMAGE(x, y) >= erasedP1)
				return 1;
	}
			
	return 0;
}



/////////////////////////////////////////////////////////////////////////////////////
// GETRING:     function gets m_pRing of pixels on perimeter of k-size square
//                usage: allOnes = getring (x, y, k, m_pRing)
//              If m_pRing includes only 1-value pixels, function returns 1,
//              otherwise 0.
/////////////////////////////////////////////////////////////////////////////////////

long CThin::getring(long x, long y, long k, BYTE *ring)
{
	register int xEnd, yEnd,          // x,y ends of square   
		allOnes,                  // =1 if all ones in m_pRing; 0 otherwise   
		i;
	long upHalf, downHalf,        // half of mask below and above center   
		xStart,                   // x- start and end of square   
		yStart;                   // y- start and end of square   
	
	if (k % 2 == 1)
		downHalf = upHalf = (k - 1) / 2;
	else 
	{
		upHalf = k / 2;
		downHalf = (k - 2) / 2;
	}
	xStart = x - downHalf;
	xEnd = x + upHalf;
	yStart = y - downHalf;
	yEnd = y + upHalf;
	
	allOnes = 1;
	i = 0;
	for (x = xStart, y = yStart; x <= xEnd; x++) 
	{
		if (IMAGE(x, y) < ERASED)
			allOnes = 0;
		ring[i++] = IMAGE(x, y);
	}
	for (y = yStart + 1, x = xEnd; y <= yEnd; y++) 
	{
		if (IMAGE(x, y) < ERASED)
			allOnes = 0;
		ring[i++] = IMAGE(x, y);
	}
	for (x = xEnd - 1, y = yEnd; x >= xStart; --x) 
	{
		if (IMAGE(x, y) < ERASED)
			allOnes = 0;
		ring[i++] = IMAGE(x, y);
	}
	for (y = yEnd - 1, x = xStart; y > yStart; --y) 
	{
		if (IMAGE(x, y) < ERASED)
			allOnes = 0;
		ring[i++] = IMAGE(x, y);
	}
	
	// if this square is already at border, cannot go to larger square 
	if (xStart <= 0 || 
		yStart <= 0 || 
		xEnd >= (m_nWidth - 1) || 
		yEnd >= (m_nySizeM1))
		return 0;
	
	return allOnes;
}

/////////////////////////////////////////////////////////////////////////////////////
// THINRING:    function makes decision to thin or not based on CNUM
//              and FNUM in perimeter m_pRing
//                       usage: flag = thinring (m_pRing, k, &permOn)
//              Flag = 1 if thinning conditions met, 0 otherwise.
/////////////////////////////////////////////////////////////////////////////////////

long CThin::thinring(BYTE *ring, long k, int *permOn)
{
    int nRing,                      // no. pixels in m_pRing   
		cNum,                       // connectivity number   
		n, i;
	long fNum,                      // no. 1s on m_pRing   
		m;
	long nOff,                      // current run of OFF in m_pRing   
		phi0,                       // maximum run of OFF in m_pRing   
		nFirstRun;                  // no  OFF from m_pRing[0]   
	long lower, upper;              // adjacent m_pRing elements for cNum calc   
	
	nRing = 4 * k - 4;
	
	// calculate FNUM 
	for (i = 0, fNum = 0; i < nRing; i++)
		if (ring[i] >= ERASED)
			fNum++;

	// calculate 4-connected run of 0s 
	nOff = (ring[0] < ERASED) ? 1 : 0;
	nFirstRun = (nOff == 1) ? 0 : -1;
	for (i = 1, phi0 = 0; i < nRing; i++) 
	{
		if (ring[i] < ERASED)
			nOff++;
		else 
		{
			if (nOff > 0) 
			{
				phi0 = (nOff > phi0) ? nOff : phi0;
				if (nFirstRun == 0)
					nFirstRun = nOff;
				nOff = 0;
			}
		}
	}

	if (nOff > 0) 
	{
		if (nFirstRun > 0)
			nOff += nFirstRun;
		phi0 = (nOff > phi0) ? nOff : phi0;
	}
	
	// CNUM 
	// CNUM skipping corners 
	for (i = 2, cNum = 0; i < nRing; i++) 
	{
		lower = (long) ring[i - 1];
		if ((i % (k - 1)) == 0)
			i++;                      // skip the corner pixels 
		upper = (long) ring[i];
		if (upper >= ERASED && lower < ERASED)
			cNum++;
	}

	if (ring[1] >= ERASED && ring[nRing - 1] < ERASED)
		cNum++;
	// CNUM at corners 
	for (n = 1; n < 4; n++) 
	{
		m = n * (k - 1);
		if (ring[m] >= ERASED)
			if (ring[m - 1] < ERASED && ring[m + 1] < ERASED)
				cNum++;
	}
	if (ring[0] >= ERASED && ring[1] < ERASED && ring[nRing - 1]< ERASED)
		cNum++;
	
	// to thin or not to thin 
	if (cNum == 1)
	{
		if (phi0 > (k - 2) && fNum > (k - 2))
			return 1;
	}	
	// for 3x3, set flag for perm. ON pixel if connection, end, or cross pt 
	// note that 2nd conditional for cross added to make equivalent
	// to THINWX; this leaves non-8-conn. jct if cross-jct with one branch
	// just one pixel, but it's consistent 
	if (k == 3)
	{
		if (cNum > 1 || fNum <= 1 || (cNum == 0 && fNum == 4))
			*permOn = 1;
	}
	
	return 0;
}



/////////////////////////////////////////////////////////////////////////////////////
// CHKCONNECT:  function checks connectivity to see if current erasures
//              combined with past erasures will destroy connectivity
//                      usage: flag = chkconnect (m_pRing, m_pSide, k)
//              Function returns flag = 0 if connectivity destroyed,
//              flag = 1 if connectivity retained.
// 
/////////////////////////////////////////////////////////////////////////////////////

// if corner or its nbr is ON, then that is value of m_pSide, otherwise OFF 
#define CORNER(ISIDE, ICORNER, ICORNERSIDE, ICORNEROTHER) \
		if (ring[ICORNER] >= PON || ring[ICORNEROTHER] >= ERASED) \
    		 side[ISIDE] = ON; \
		else side[ISIDE] = OFF

// for a NW corner, check if corner is anchor point, and if so set to ON 
#define CORNERNW(ISIDE, ICORNER, ICORNERSIDE, ICORNEROTHER) \
		if (ring[ICORNER] >= ERASED + 1 \
		    || ring[ICORNEROTHER] >= ERASED) \
    		  side[ISIDE] = ON; \
		else side[ISIDE] = OFF

long CThin::chkconnect(BYTE *ring, BYTE *side, long k)
{
	long i, nSide,				    // no. pixels along 8-connected m_pSide  
		nSideM1,                    // nSide minus 1  
		iSide,                      // m_pSide array index  
		nRing;                      // no. pixels in m_pRing  
	long anON,                      // an ON run along m_pSide  
		ONandERASE,                 // run of ON and n ERASEs  
		N1;                         // no. ones in nbrhood  
	
	nRing = 4 * k - 4;
	nSide = k;
	nSideM1 = nSide - 1;
	
	// calculate N1  
	for (i = 0, N1 = 0; i < nRing; i++)
	{
		if (ring[i] >= PON)
			N1++;
	}
		
	if (N1 == 0)
		return (0);
	
	// check connectivity of west m_pSide  
	i = 3 * (k - 1);
	CORNER (0, i, i + 1, i - 1);
	for (i = i + 1, iSide = 1; i < nRing; i++, iSide++)
		side[iSide] = ring[i];
	CORNERNW (nSideM1, 0, nRing - 1, 1);
	
	for (i = 0, anON = 0, ONandERASE = 0; i < nSide; i++) 
	{
		if (side[i] >= PON) 
		{
			if (ONandERASE == 1)
				return (0);
			else
				anON = 1;
		}
		else if ((side[i] == ERASED || side[i] == ERASED + 1) && anON == 1)
			ONandERASE = 1;
		else if (side[i] < ERASED)
			anON = ONandERASE = 0;    // off 
	}
	
	// check connectivity of north m_pSide 
	i = k - 1;
	CORNER (0, i, i - 1, i + 1);
	for (i = i - 1, iSide = 1; i >= 0; --i, iSide++)
		side[iSide] = ring[i];
	CORNERNW (nSideM1, 0, 1, nRing - 1);
	
	for (i = 0, anON = 0, ONandERASE = 0; i < nSide; i++) 
	{
		if (side[i] >= PON) 
		{
			if (ONandERASE == 1)
				return (0);
			else
				anON = 1;
		}
		else if ((side[i] == ERASED || side[i] == ERASED + 1)
			&& anON == 1)
			ONandERASE = 1;
		else if (side[i] < ERASED)
			anON = ONandERASE = 0;    // off 
	}
	
	// check connectivity of east m_pSide 
	i = k - 1;
	CORNER (0, i, i + 1, i - 1);
	for (i = i + 1, iSide = 1; iSide < nSideM1; i++, iSide++)
		side[iSide] = ring[i];
	CORNER (nSideM1, 2 * (k - 1), 2 * (k - 1) - 1, 2 * (k - 1) + 1);
	
	for (i = 0, anON = 0, ONandERASE = 0; i < nSide; i++) 
	{
		if (side[i] >= PON) 
		{
			if (ONandERASE == 1)
				return 0;
			else
				anON = 1;
		}
		else if ((side[i] == ERASED || side[i] == ERASED + 1) && anON == 1)
			ONandERASE = 1;
		else if (side[i] < ERASED)
			anON = ONandERASE = 0;    // off 
	}
	return 1;
}

///////////////////////////////////////////////////////////////////////////
// ANCHOR:      function returns 1 if core is exposed on NW m_pSide
//                    usage: anchor (m_pRing, k)
///////////////////////////////////////////////////////////////////////////

long CThin::anchor(BYTE *ring, long k)
{
	long nRing;          // no. pixels in m_pRing 
	long i;
	
	nRing = 4 * k - 4;
	for (i = 0; i < k; i++)
	{
		if (ring[i] >= ERASED)
			return 0;
	}
	for (i = 3 * (k - 1) + 1; i < nRing; i++)
	{
		if (ring[i] >= ERASED)
			return 0;
	}
	return 1;
}



///////////////////////////////////////////////////////////////////////////
// ERASESQR:    function erases square contained within square perimeter
//                    usage: erasesqr (x, y, k, anchor, &nErased)
//              If the core is an anchor, then the pixels are erased
//              to ERASED + 1, otherwise they are erased to ERASED.
//              For kxk > 3x3 erasure, PON pixels (permanent ON for 3x3)
//              are erased; and if an anchor point (ERASED + 1) can be
//              erased to a non-anchor point by a larger k 
///////////////////////////////////////////////////////////////////////////

long CThin::erasesqr(long x, long y, long k, int anchor, long *nErased)
{
	register long xEnd, yEnd;     // upper bounds of center erase area 
	long upHalf, downHalf,        // half of mask below and above center 
		yStart,                   // bounds of center erase area 
		xStart;
	
	// erase for 3x3 
	if (k == 3) 
	{
		if (IMAGE(x, y) == ON) 
		{
			(*nErased)++;
			IMAGE(x, y) = (anchor == 0) ? ERASED : ERASED + 1;
		}
	}
	// erase for kxk > 3x3 
	else 
	{
		if (k % 2 == 1)
			downHalf = upHalf = (k - 3) / 2;
		else 
		{
			upHalf = (k - 2) / 2;
			downHalf = (k - 4) / 2;
		}
		xStart = x - downHalf;
		xEnd = x + upHalf;
		yStart = y - downHalf;
		yEnd = y + upHalf;
		for (y = yStart; y <= yEnd; y++) 
		{
			for (x = xStart; x <= xEnd; x++) 
			{
				if (IMAGE(x, y) >= ERASED + 1) 
				{
					if (IMAGE(x, y) >= PON)
						(*nErased)++;
					IMAGE(x, y) = (anchor == 0) ? ERASED : ERASED + 1;
				}
			}
		}
	}

	return 0;
}



