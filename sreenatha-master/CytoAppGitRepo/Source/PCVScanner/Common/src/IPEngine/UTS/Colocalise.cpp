////////////////////////////////////////////////////////////////////////////////////////
//
// Colocalisation Analysis
//
// Written By Karl Ratcliff 130706
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include "stdafx.h"
#include "uts.h"
#include "vcMemRect.h"
#include "Math.h"
#include "Colocalise.h"

CColocalise::CColocalise(CUTSMemRect *pMR)
{
	pUTSMemRect = pMR;
}

CColocalise::~CColocalise(void)
{
}

////////////////////////////////////////////////////////////////////////////////////////
// Create an image which is basically a product of differences from the mean
// Inputs: UTS handles Image1, Image2
//         Mean1 is the mean intensity of Image1 
//         Mean2 is the mean intensity of Image2
////////////////////////////////////////////////////////////////////////////////////////
    
#define DUFF_VALUE      -65536

long CColocalise::PDMImage(long Image1, BYTE Mean1, long Image2, BYTE Mean2)
{
    long W1, H1, W2, H2, P, B, NewHandle, BPP1, BPP2;
    int  j, MaxVal, MinVal, D1, D2, R, I;
    BYTE *Im1Ptr, *Im2Ptr, *Im3Ptr;
    int  *Im4Ptr, *Im16;

    BPP1 = pUTSMemRect->GetExtendedInfo(Image1, &W1, &H1, &P, &Im1Ptr);
    BPP2 = pUTSMemRect->GetExtendedInfo(Image2,  &W2, &H2, &P, &Im2Ptr);

    // Check the bit depth
    if (BPP1 != BPP2 || BPP1 != EIGHT_BITS_PER_PIXEL || BPP2 != EIGHT_BITS_PER_PIXEL)
        return UTS_ERROR;

    // Compare image sizes and make sure they are identical
    if (W1 != W2 || H1 != H2)
        return UTS_ERROR;

    // Create an image for the resulting threshold
    NewHandle = pUTSMemRect->CreateAs(Image1);
    if (!UTSVALID_HANDLE(NewHandle))
        return UTS_ERROR;

    pUTSMemRect->GetExtendedInfo(NewHandle, &W1, &H1, &P, &Im3Ptr);

    // Total bytes in image
    B = H1 * P;

    // Calc an int image
    Im16 = new int[B];
    Im4Ptr = Im16;
    MaxVal = INT_MIN;
    MinVal = INT_MAX;

    for (j = 0; j < B; j++)
    {
        D1 = *Im1Ptr++ - Mean1;
        D2 = *Im2Ptr++ - Mean2;
        R = D1 * D2;
        if (D1 >= 0 || D2 >= 0)
        {
            *Im4Ptr++ = R;
            if (R > MaxVal) MaxVal = R;
            if (R < MinVal) MinVal = R;
        }
        else
            *Im4Ptr++ = DUFF_VALUE;
    }

    // Now scale the output
    Im4Ptr = Im16;
    R = MaxVal - MinVal;

    // Our output image scaled
    for (j = 0; j < B; j++)
    {
        I = *Im4Ptr++;

        if (I != DUFF_VALUE)
            *Im3Ptr++ = (255 * (I - MinVal)) / R;
        else
            *Im3Ptr++ = 0;
    }

    delete [] Im16;

    return NewHandle;
}

////////////////////////////////////////////////////////////////////////////////////////
// Calculate the overlap coefficients k1 and k2
////////////////////////////////////////////////////////////////////////////////////////
    
long CColocalise::OverlapK1K2(long Image1, long Image2, double *k1, double *k2)
{
    long W1, H1, W2, H2, P, B, BPP1, BPP2;
    int  j;
    BYTE *Im1Ptr, *Im2Ptr, g1, g2, g1max, g2max;
    float s1, s2, s12;

    BPP1 = pUTSMemRect->GetExtendedInfo(Image1, &W1, &H1, &P, &Im1Ptr);
    BPP2 = pUTSMemRect->GetExtendedInfo(Image2,  &W2, &H2, &P, &Im2Ptr);

    // Check the bit depth
    if (BPP1 != BPP2 || BPP1 != EIGHT_BITS_PER_PIXEL || BPP2 != EIGHT_BITS_PER_PIXEL)
        return UTS_ERROR;

    // Compare image sizes and make sure they are identical
    if (W1 != W2 || H1 != H2)
        return UTS_ERROR;
    // Total bytes in image
    B = H1 * P;

    s1 = s2 = s12 = 0.0f;
    g1max = g2max = 0;

    for (j = 0; j < B; j++)
    {
        g1 = *Im1Ptr++;
        g2 = *Im2Ptr++;
        if (g1 > g1max) g1max = g1;
        if (g2 > g2max) g2max = g2;
        s1 += g1 * g1;
        s2 += g2 * g2;
        s12 += g1 * g2;
    }

    s2 *= g2max;
    s1 *= g1max;

    *k1 = *k2 = 0.;

    if (s1 > 0)
        *k1 = (g2max * s12) / s1;
    if (s2 > 0)
        *k2 = (g1max * s12) / s2;

    return 0;
}


////////////////////////////////////////////////////////////////////////////////////////
// Calculate the Mander's overlap coefficients k1 and k2
////////////////////////////////////////////////////////////////////////////////////////
    
long CColocalise::MandersK1K2(long Image1, long Image2, double *k1, double *k2)
{
    long W1, H1, W2, H2, P, B, BPP1, BPP2;
    int  j;
    BYTE *Im1Ptr, *Im2Ptr, g1, g2, g1max, g2max;
    float s1, s2, s12, s21;

    BPP1 = pUTSMemRect->GetExtendedInfo(Image1, &W1, &H1, &P, &Im1Ptr);
    BPP2 = pUTSMemRect->GetExtendedInfo(Image2,  &W2, &H2, &P, &Im2Ptr);

    // Check the bit depth
    if (BPP1 != BPP2 || BPP1 != EIGHT_BITS_PER_PIXEL || BPP2 != EIGHT_BITS_PER_PIXEL)
        return UTS_ERROR;

    // Compare image sizes and make sure they are identical
    if (W1 != W2 || H1 != H2)
        return UTS_ERROR;
    // Total bytes in image
    B = H1 * P;

    s1 = s2 = s12 = s21 = 0.0f;
    g1max = g2max = 0;

    for (j = 0; j < B; j++)
    {
        g1 = *Im1Ptr++;
        g2 = *Im2Ptr++;
        s1 += g1;
        s2 += g2;
        if (g2)
            s12 += g1;
        if (g1)
            s21 += g2;
    }

    *k1 = *k2 = 0.;

    if (s1 > 0)
        *k1 = s12 / s1;
    if (s2 > 0)
        *k2 = s21 / s2;

    return 0;
}

////////////////////////////////////////////////////////////////////////////////////////
// Pearsons overlap co-efficient
// NOTE RegionArea is the area of the mask region
// NOTE a -ve return means an ERROR
////////////////////////////////////////////////////////////////////////////////////////

double CColocalise::Pearsons(long Image1, long Image2, long RegionMask)
{
    long W1, H1, W2, H2, P, B, BPP1, BPP2, BPPM, WM, HM, RegionArea;
    int  j;
    BYTE Avg1, Avg2, *Im1Ptr, *Im2Ptr, g1, g2, *Im1, *Im2, *MaskPtr, *MPtr;
    float s1, s2, s12, s11, s22;
    short m1, m2;
    double Pearsons;
    
    BPP1 = pUTSMemRect->GetExtendedInfo(Image1, &W1, &H1, &P, &Im1);
    BPP2 = pUTSMemRect->GetExtendedInfo(Image2,  &W2, &H2, &P, &Im2);
    BPPM = pUTSMemRect->GetExtendedInfo(RegionMask,  &WM, &HM, &P, &MPtr);

    // Check the bit depth
    if (BPP1 != BPP2 || BPP1 != BPPM || BPP1 != EIGHT_BITS_PER_PIXEL || BPP2 != EIGHT_BITS_PER_PIXEL || BPPM != EIGHT_BITS_PER_PIXEL)
        return (double)UTS_ERROR;

    // Compare image sizes and make sure they are identical
    if (W1 != W2 || H1 != H2 || W1 != WM || H1 != HM)
        return (double)UTS_ERROR;
    // Total bytes in image
    B = H1 * P;

    s1 = s2 = s12 = s11 = s22 = 0.0f;

    RegionArea = 0;
    MaskPtr = MPtr;
    Im1Ptr = Im1;
    Im2Ptr = Im2;

    for (j = 0; j < B; j++)
    {
        if (*MaskPtr++)
        {
            g1 = *Im1Ptr++;
            g2 = *Im2Ptr++;
            s1 += g1;
            s2 += g2; 
            RegionArea++;
        }
        else
        {
            Im1Ptr++;
            Im2Ptr++;
        }
    }

    Pearsons = 0.;

    if (RegionArea)
    {
        // Average intensities for both channels
        Avg1 = (BYTE)(s1 / RegionArea);
        Avg2 = (BYTE)(s2 / RegionArea);

        Im1Ptr = Im1;
        Im2Ptr = Im2;
        MaskPtr = MPtr;

        for (j = 0; j < B; j++)
        {
            if (*MaskPtr++)
            {
                m1 = *Im1Ptr++ - Avg1;
                m2 = *Im2Ptr++ - Avg2;
                s12 += m1 * m2;
                s11 += m1 * m1;
                s22 += m2 * m2;
            }
            else
            {
                Im1Ptr++;
                Im2Ptr++;
            }
        }

        if (s11 != 0.0f && s22 != 0.0f)
            Pearsons = s12 / sqrt(s11 * s22);
    }

    return Pearsons;
}

