////////////////////////////////////////////////////////////////////////////////
//
//	ImageRegister
//
//	Image registration by difference method
//
//	David Burgess, Applied Imaging, 3rd October 2001
//
#include "stdafx.h"
#include <math.h>

#include "ImageRegister.h"


typedef unsigned char BYTE;
#define MIN(a,b) (a) < (b) ? (a) : (b)

////////////////////////////////////////////////////////////////////////////////
//
//	Orientation is of image2 with reference to image1
//	Returns registration offset where image 2 starts to overlap image1
//
int ImageRegister::Register(BYTE *image1, BYTE *image2,
									int width, int height, 
									int search_from, int search_to, int stripsize,
									LPCTSTR orientation)
{
	int reg;

	// Switch images and call appropriate difference functions
	// depending on orientation
	switch (*orientation) {
	case 'N':
		reg = DifferenceInY(image1, image2, width, height, search_from, search_to, stripsize);
		break;

	case 'S':
		reg = DifferenceInY(image2, image1, width, height, search_from, search_to, stripsize);
		break;

	case 'E':
		reg = DifferenceInX(image1, image2, width, height, search_from, search_to, stripsize);
		break;

	case 'W':
		reg = DifferenceInX(image2, image1, width, height, search_from, search_to, stripsize);
		break;
	}

	return reg;
}


////////////////////////////////////////////////////////////////////////////////
//
//	DifferenceInX - Calculate image offset in X
//	Image2 is to the right of image1
//
//	Returns image position in X where image2 starts to overlap image1
//	or -1 on error
//
#define MAXSHIFT 2048

int ImageRegister::DifferenceInX(BYTE *image1, BYTE *image2,
									int width, int height, 
									int search_from, int search_to, int stripwidth)
{
	BYTE *im1;
	BYTE *im2;
	int i, j, pos;
	long diff;
//	float *avdiff, mindiff;
	float avdiff[MAXSHIFT];
	int striparea;
	int reg_pos;
	int search_range;
	int sw;	// Working stripwidth

	// Check inputs
	if ((image1 == NULL) || (image2 == NULL))
		return -1;

	if ((search_from < 0) || (search_to < 0))
		return -1;

	search_range = search_to - search_from;
	if ((search_range < 0) || (search_range > width))
		return -1;

	// Create space for average difference array
//	avdiff = new float[search_range];

	//	Clear array
	for (i = 0; i < MAXSHIFT; i++)
		avdiff[i] = 0.0;

	for (pos = search_from; pos < search_to; pos++) {

		// Stripwidth cannot be greater than overlap area
		sw = MIN(stripwidth, width - pos);

		diff = 0;

//		i = height;
//		while (i--) {
		for (i = 0; i < height; i++) {

			im1 = image1 + (i * width) + pos;
			im2 = image2 + (i * width);
			j = sw;

			while (j--)
				diff += abs(*im1++ - *im2++);
		}

		// Find average difference
		striparea = height * sw;
		avdiff[pos - search_from] = (float)diff / (float)striparea;
	}

	// Find first minima as registration offset
	reg_pos = FindMinOfDiff(avdiff, search_range);

//	delete avdiff;

	// Add search offset back in
	reg_pos += search_from;

	return reg_pos;
}

////////////////////////////////////////////////////////////////////////////////
//
//	DifferenceInY - Calculate image offset in Y
//	Image2 is to the bottom of image1
//
//	Returns image position in Y where image2 starts to overlap image1
//	or -1 on error
//
int ImageRegister::DifferenceInY(BYTE *image1, BYTE *image2,
									int width, int height, 
									int search_from, int search_to, int stripheight)
{
	BYTE *im1;
	BYTE *im2;
	int i, j, pos;
	long diff;
//	float *avdiff, mindiff;
	float avdiff[MAXSHIFT];	// Average differences
	int striparea;			// Area of strip being compared
	int search_range;		// Range of offsets to search over
	int sh;					// Working stripheight
	int reg_pos;			// Registration position

	// Check inputs
	if ((image1 == NULL) || (image2 == NULL))
		return -1;

	if ((search_from < 0) || (search_to < 0))
		return -1;

	search_range = search_to - search_from;
	if ((search_range < 0) || (search_range > width))
		return -1;

	// Create space for average difference array
//	avdiff = new float[search_to - search_from];

	//	Clear array
	for (i = 0; i < MAXSHIFT; i++)
		avdiff[i] = 0.0;

	for (pos = search_from; pos < search_to; pos++) {

		// Stripheight cannot be greater than overlap area
		sh = MIN(stripheight, height - pos);

		diff = 0;

//		i = stripheight;
//		while (i--) {
		for (i = 0; i < sh; i++) {
			im1 = image1 + (pos * width) + (i * width);
			im2 = image2 + (i * width);
			j = width;

			while (j--)
				diff =  diff + abs(*im1++ - *im2++);
		}

		// Find average difference
		striparea = width * sh;
		avdiff[pos - search_from] = (float)diff / (float)striparea;
//		avdiff[search_range - pos + search_from] = (float)diff / (float)striparea;
	}

	// Search difference array to find minimum
	reg_pos = FindMinOfDiff(avdiff, search_range);

//	delete avdiff;
	reg_pos += search_from;

	return reg_pos;
}


////////////////////////////////////////////////////////////////////////////////
//
//	FindMinOfDiff - Search difference array to find local minimum.
//	New algorithm
//	The algorithm searches for the highest rate of change
//
//	Returns minimum value, or -1 on error
//
#define IGNORE_MARGIN	15	// Ignore if this close to the edge

int ImageRegister::FindMinOfDiff(float *avdiff, int diffsize)			    
{
	float maxchange;		// Running maximum change value
	int i;					// Counter
	float change[2048];		// Change array
	int maxpoint;			// Position of greatest change

	// Single pass smoothing
	for (i = 0; i < (diffsize - 1); i++)
		avdiff[i] = (avdiff[i] + avdiff[i + 1]) / 2;

	// Dual pass smoothing (opposite way)
	for (i = diffsize; i > 0; i--)
		avdiff[i] = (avdiff[i] + avdiff[i - 1]) / 2;

	// Get rate of change
	for (i = 0; i < diffsize; i++)
		change[i] = fabs(avdiff[i + 1] - avdiff[i]);

	// Find maximum value in change array
	// this is the point of greatest change and
	// thus the notchiest point in the difference array
	maxchange = change[0];
	maxpoint = -1;

	for (i = 0; i < (diffsize - IGNORE_MARGIN); i++) {
		// Search for maximum
		if (change[i] >= maxchange) {
			maxchange = change[i];
			maxpoint = i;
		}
	}
	// Select registration position as first max in change array
	return maxpoint;
}

#if 0
Old algorithm

////////////////////////////////////////////////////////////////////////////////
//
//	FindMinOfDiff - Search difference array to find local minimum.
//
//	The algorithm alternately searches for minima and maxima
//	Separation of minima/maxima is governed by the parameter MIN_SEPARATION
//	The first minimum is reported
//
//	Returns minimum value, or -1 on error
//
#define MIN_SEPARATION 3
#define VAL_SEPARATION 2
#define NMAXIMA 20
int ImageRegister::FindMinOfDiff(float *avdiff, int diffsize)			    
{
	int maxima[NMAXIMA];						// Array of maximum points
	int minima[NMAXIMA];						// Array of minimum points
	int nmaxima = 0;					// Number of maxima
	int nminima = 0;					// Number of minima
	int peak_sep_min = MIN_SEPARATION;	// Minimum separation between minima and maxima
	int peak_sep_value = VAL_SEPARATION;// Value separation between minima & maxima
	int separation = 0;					// Seaparation counter
	BOOL searchmax = TRUE;				// Search for maxima (TRUE)
	float maxdiff, mindiff;				// Running mimimum/maximum difference value
	int i;								// Counter

	// Single pass smoothing
	for (i = 0; i < (diffsize - 1); i++)
		avdiff[i] = (avdiff[i] + avdiff[i + 1]) / 2;

	// Dual pass smoothing
	for (i = 0; i < (diffsize - 1); i++)
		avdiff[i] = (avdiff[i] + avdiff[i + 1]) / 2;

	// Set peak_sep_value to 50% of dynamic range
	maxdiff = mindiff = avdiff[0];
	for (i = 1; i < (diffsize - 5); i++) {
		if (avdiff[i] > maxdiff)
			maxdiff = avdiff[i];
		else if (avdiff[i] < mindiff)
			mindiff = avdiff[i];
	}

	peak_sep_value = (maxdiff - mindiff) * 0.5;

	// Find maxima & minima
	maxima[nmaxima] = minima[nminima] = 0;
	maxdiff = mindiff = avdiff[0];

	// Ignore outer edge
	for (i = 1; i < (diffsize - 5); i++) {

		if (searchmax) {
			// Search for maximum
			if (avdiff[i] >= maxdiff) {
				maxdiff = avdiff[i];
				maxima[nmaxima] = i;
				separation = 0;
			} else if (avdiff[i] < (maxdiff - peak_sep_value)) {
				// Coming down
				separation++;
				if (separation > peak_sep_min) {
					// Reset and start looking for minima
					nmaxima++;
					if (nmaxima > NMAXIMA)
						break;
					mindiff = avdiff[i];
					separation = 0;
					searchmax = FALSE;
				}
			}
		} else {
			// Search for minimum
			if (avdiff[i] < mindiff) {
				mindiff = avdiff[i];
				minima[nminima] = i;
				separation = 0;
			} else if (avdiff[i] > (mindiff + peak_sep_value)) {
				// Going up
				separation++;
				if (separation > peak_sep_min) {
					// Reset and start looking for maxima
					nminima++;
					if (nminima > NMAXIMA)
						break;
					maxdiff = avdiff[i];
					separation = 0;
					searchmax = TRUE;
				}
			}
		}
	}

	// Select registration position as first minimum
	return minima[0];
}
#endif
