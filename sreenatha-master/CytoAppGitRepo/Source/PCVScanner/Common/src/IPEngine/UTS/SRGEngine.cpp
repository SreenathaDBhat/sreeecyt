// SRGEngine.cpp: implementation of the CSRGEngine class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "uts.h"
#include "vcMemRect.h"
#include "Math.h"
#include "hrtimer.h"
#include "SRGEngine.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSRGCandidate::CSRGCandidate( CPoint & point, int mostSimilarRegionId, double similarityDifference) : 
	m_point( point),
	m_nMostSimilarRegionId( mostSimilarRegionId),
	m_dSimilarityDifference( similarityDifference)
{
}

CSRGCandidate::~CSRGCandidate()
{
}



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSRGRegionInfo::CSRGRegionInfo( int seedID)
	: _seedID( seedID)
{
}

CSRGRegionInfo::~CSRGRegionInfo()
{
}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSRGImage::CSRGImage() :
	handle(0),
	w(0),
	h(0),
	pitch(0),
	bpp(0),
	pImage(NULL)
{
}

CSRGImage::~CSRGImage()
{
}


//////////////////////////////////////////////////////////////////////
// CSRGEngine - Segments images using seeded region growing
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSRGEngine::CSRGEngine( CUTSMemRect * pMR, CREATEREGIONINFO * pRegionFactory)
	: _pUTSMemRect( pMR),
	  _pRegionInfoFactory(pRegionFactory)
{
	ASSERT( _pUTSMemRect != NULL);
	ASSERT( _pRegionInfoFactory != NULL);
}

CSRGEngine::~CSRGEngine()
{
}


//////////////////////////////////////////////////////////////////////
// Operations
//////////////////////////////////////////////////////////////////////

void CSRGEngine::Segment( long source, long seeds, long segmentation)
{
	CHRTimer HRT;
	double et;
	CString msg;

	_source.handle = source;
	_source.bpp = _pUTSMemRect->GetExtendedInfo( source, &_source.w, &_source.h, &_source.pitch, &_source.pImage);
	_seeds.handle = seeds;
	_seeds.bpp = _pUTSMemRect->GetExtendedInfo( seeds,  &_seeds.w, &_seeds.h, &_seeds.pitch, &_seeds.pImage);
	_regionMarker.handle = segmentation;
	_regionMarker.bpp = _pUTSMemRect->GetExtendedInfo( _regionMarker.handle, &_regionMarker.w, &_regionMarker.h, &_regionMarker.pitch, &_regionMarker.pImage);

	// Check all images are the same size
	ASSERT( _source.bpp == 8);
	ASSERT( _seeds.bpp == 8);
	ASSERT( _regionMarker.bpp == 8);
	ASSERT( _source.w == _seeds.w && _source.h == _seeds.h);
	ASSERT( _seeds.w == _regionMarker.w && _seeds.h == _regionMarker.h );

	InitialiseStructures();

	InitialiseMarkersAndRegionInfo();

HRT.Start();

	InitialiseCandidates();

HRT.ElapsedTime(&et);
msg.Format("Segment() InitialiseCandidates took %10.6lf millisecs to execute.\n", et * 1000.0);
OutputDebugString( msg);
HRT.Start();

	// while the list of candidates is not empty
	int imageSeq = 0;
	int updatedPixelCount = 0;
	while (!_ssl.empty())
	{
		set<CSRGCandidate*,ltstr>::iterator iter = _ssl.begin();
		CSRGCandidate * c = *iter;
		_ssl.erase(iter);

		int offset = c->m_point.x + c->m_point.y * _regionMarker.w;
		BYTE * pMarkerPixel = ((BYTE*)_regionMarker.pImage) + offset;
		// "Expecting point (" + c.point.x + "," + c.point.y + ") to be candidate got " + (int) marker;)
        ASSERT( *pMarkerPixel == CANDIDATE_MARK);

        // Add this point to its most similar region
		*pMarkerPixel = c->m_nMostSimilarRegionId;

        // Update region info to include this point
        _regionInfos[c->m_nMostSimilarRegionId]->AddPoint(((BYTE*)_source.pImage)[offset]);

        //++processedPixelCount;
		updatedPixelCount++;

        CandidatesFromNeighbours(c->m_point);

		delete c;

		// Add animation frame if required
		//if ((updatedPixelCount-1) % 10000 == 0)
		//{
		//	CString filename; filename.Format( "D:\\dump%04d.bmp", imageSeq++);
		//	image_dump( (BYTE*)_regionMarker.pImage, _regionMarker.w, _regionMarker.h, filename);
		//}
	}

	RestoreOriginalSeedIDs();

	msg.Format( "%d pixels updated.\n", updatedPixelCount);
	OutputDebugString( msg);
	
	// Cleanup _ssl, _regionInfos, _source, _seed and all other structures
	_ssl.clear();
	for (int i=0; i<_regionInfos.size(); i++)
	{
		delete _regionInfos[i];
	}
	_regionInfos.clear();

HRT.ElapsedTime(&et);
msg.Format("Segment() Region Growing took %10.6lf millisecs to execute.\n", et * 1000.0);
OutputDebugString( msg);
}

//////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////
void CSRGEngine::InitialiseStructures()
{
    _xMin = 0;
	_xMax = _regionMarker.w;
    _yMin = 0;
    _yMax = _regionMarker.h;

	// Create regions and lookups...
	DWORD histogram[256];
	long pixelCount = _pUTSMemRect->Histogram( _seeds.handle, &histogram);
	ASSERT( pixelCount > 0);

	for (int i=0; i<MAX_REGION_NUMBER; i++)
	{
		_seedToRegion[i] = 0;
		_regionToSeed[i] = 0;
	}

	// Ignore Seed ID (grey level) 0 and Region ID 0 as this is background to grow into.
	// Create dummy regionInfo for seed grey level 0.
	// Seed   value in [1,253]
	// Region value in [1,253]
	_regionInfos.push_back( NULL);
	int regionCount = 0;
	for (int seed=1; seed<256; seed++)
	{
		if (histogram[seed] > 0)
		{
			if (seed > MAX_REGION_NUMBER)
			{
				ASSERT(0); //throw new IllegalArgumentException("Seed ID cannot be larger than " + MAX_REGION_NUMBER + ", got " + seed + ".");
            }

			_regionInfos.push_back( (*_pRegionInfoFactory)( seed) );
			regionCount++;
			_seedToRegion[seed] = regionCount;
			_regionToSeed[regionCount] = seed;
		}
	}
}

void CSRGEngine::InitialiseMarkersAndRegionInfo()
{
	// Just copy seeds image to result image?

	BYTE * pSource = ((BYTE*)_source.pImage);
	BYTE * pSeeds = ((BYTE*)_seeds.pImage);
	BYTE * pRegionMarker = ((BYTE *) _regionMarker.pImage);

	// TODO: replace these two loops with a single one
	// and maintain offset instead of calculating with millions of multiplications
    for (int y = 0; y < _seeds.h; ++y)
	{
        for (int x = 0; x < _seeds.w; ++x)
		{
            int offset = x + y * _seeds.w;

			int regionID = _seedToRegion[ pSeeds[offset]];
            if (regionID > BACKGROUND_MARK)
			{
				// Add seed to regionMarkers
				pRegionMarker[offset] = regionID;

				// Add seed to region info
				_regionInfos[regionID]->AddPoint( pSource[offset]);
            }
        }
    }
}

void CSRGEngine::InitialiseCandidates()
{
	BYTE * pRegionMarker = ((BYTE *) _regionMarker.pImage);
	int ySize = _regionMarker.h;
	int xSize = _regionMarker.w;
	int processedPixelCount = 0;
	BYTE * pPixel = pRegionMarker;
	int regionId;

    for (int y = 0; y < ySize; ++y)
	{
        for (int x = 0; x < xSize; ++x)
		{
            regionId = *pPixel;
            if (regionId != BACKGROUND_MARK && regionId != CANDIDATE_MARK)
			{
				// Initialize SSL - ordered list of bordering at least one of the regions
				CandidatesFromNeighbours( CPoint(x, y));

				++processedPixelCount;
			}

			pPixel++;
        }
    }

	CString msg;
	msg.Format( "%f%% pixels processed in seed (%d/%d).\n", 
		(processedPixelCount * 100.0)/(xSize * ySize), processedPixelCount, xSize * ySize);
	OutputDebugString( msg);
}

void CSRGEngine::CandidatesFromNeighbours( const CPoint & point)
{
    // Get neighbours of this seed that are not assigned to any region yet
    vector<CPoint> backgroundPoints = vector<CPoint>();
	
	BYTE * pPixel = (BYTE *) _regionMarker.pImage;
	pPixel += point.x + point.y * _regionMarker.w;

    // 4-connected
    AddIfBackground(point.x - 1, point.y, pPixel - 1,				backgroundPoints);
    AddIfBackground(point.x + 1, point.y, pPixel + 1,				backgroundPoints);
    AddIfBackground(point.x, point.y - 1, pPixel - _regionMarker.w,	backgroundPoints);
    AddIfBackground(point.x, point.y + 1, pPixel + _regionMarker.w,	backgroundPoints);
    // 8-connected
    AddIfBackground(point.x - 1, point.y - 1, pPixel - 1 - _regionMarker.w,	backgroundPoints);
    AddIfBackground(point.x + 1, point.y - 1, pPixel + 1 - _regionMarker.w,	backgroundPoints);
    AddIfBackground(point.x - 1, point.y + 1, pPixel - 1 + _regionMarker.w,	backgroundPoints);
    AddIfBackground(point.x + 1, point.y + 1, pPixel + 1 + _regionMarker.w,	backgroundPoints);

	//Update SSL
	int count = (int) backgroundPoints.size(); 
	for ( int i=0; i<count; i++)
	{
		_ssl.insert( CreateCandidate( backgroundPoints[i]));
	}
}

void CSRGEngine::AddIfBackground( int x, int y, BYTE * pPixel, vector<CPoint> & backgroundPoints)
{
    if (x < _xMin || x >= _xMax || y < _yMin || y >= _yMax)
	{
        return;
    }

	if (*pPixel == BACKGROUND_MARK)
	{
        // Add to unassigned pixels
		backgroundPoints.push_back( CPoint(x, y));		// TODO pass pPixel pointer into here
    }
}

CSRGCandidate * CSRGEngine::CreateCandidate( CPoint point)
{
    int offset = point.x + point.y * _regionMarker.w;

    // Mark as candidate
    ((BYTE*)_regionMarker.pImage)[offset] = CANDIDATE_MARK;

	// Which are the neighbouring regions?
	vector<int> neighbours;
	// 4-connected
	CheckNeighbour(point.x, point.y - 1, neighbours);
    CheckNeighbour(point.x, point.y + 1, neighbours);
    CheckNeighbour(point.x - 1, point.y, neighbours);
    CheckNeighbour(point.x + 1, point.y, neighbours);
    // 8-connected
    CheckNeighbour(point.x - 1, point.y - 1, neighbours);
    CheckNeighbour(point.x + 1, point.y - 1, neighbours);
    CheckNeighbour(point.x - 1, point.y + 1, neighbours);
    CheckNeighbour(point.x + 1, point.y + 1, neighbours);

    // Compute distance to most similar region
    double minSigma = DBL_MAX;
    int mostSimilarRegionId = -1;
	int regionID = 0;
	double sigma = 0.0;
	int greyLevel = ((BYTE*)_source.pImage)[offset];
	int count = neighbours.size();
	for (int i=0; i<count; i++)
	{
		regionID = neighbours[i];
		sigma = _regionInfos[regionID]->Distance( greyLevel);
        if (sigma < minSigma)
		{
            minSigma = sigma;
            mostSimilarRegionId = regionID;
        }
	}
	ASSERT( mostSimilarRegionId > 0);

	return new CSRGCandidate( point, mostSimilarRegionId, minSigma);
}

void CSRGEngine::CheckNeighbour( int x, int y, vector<int> & neighbours)
{
    if (x < _xMin || x >= _xMax || y < _yMin || y >= _yMax)
	{
        return;
    }

    int regionID = ((BYTE*)_regionMarker.pImage)[x + y * _regionMarker.w];
    if (regionID != BACKGROUND_MARK && regionID != CANDIDATE_MARK)
	{
		// Add to set of neighbouring regions
		neighbours.push_back( regionID);
    }
}

void CSRGEngine::RestoreOriginalSeedIDs()
{
	BYTE * pRegionMarker = ((BYTE *) _regionMarker.pImage);
	int imageSize = _regionMarker.w * _regionMarker.h;

	for (int i=0; i<imageSize; i++)
	{
		pRegionMarker[i] = _regionToSeed[pRegionMarker[i]];
	}
}


/*
 *		I M A G E _ D U M P  
 *			--  write image to given file name
 */
void CSRGEngine::image_dump(BYTE * pImage, long width, long height, LPCTSTR filename)
{
	FILE	*fp;

	if (fp = _tfopen(filename,"wb"))
	{
		BITMAPFILEHEADER hdr;
		BITMAPINFOHEADER bi;
		RGBQUAD colour_table[256];
		for (int m = 0; m < 256; m++)
		{
			colour_table[m].rgbReserved = 0;
			colour_table[m].rgbRed = m;
			colour_table[m].rgbGreen = m;
			colour_table[m].rgbBlue = m;
		}
	
		bi.biBitCount = 8;
		bi.biClrImportant = 0;
		bi.biClrUsed = 256;
		bi.biCompression = BI_RGB;
		bi.biHeight = height;
		bi.biPlanes = 1;
		bi.biSize = sizeof(BITMAPINFOHEADER);
		bi.biSizeImage = 0;
		bi.biWidth = width;
		bi.biXPelsPerMeter = 0;
		bi.biYPelsPerMeter = 0;
		hdr.bfType= ((WORD) ('M' << 8) | 'B');
		hdr.bfSize= sizeof(bi) + sizeof(hdr) + (sizeof(RGBQUAD) * 256);
		hdr.bfReserved1 = 0;
		hdr.bfReserved2 = 0;
		hdr.bfOffBits= (DWORD) (sizeof(hdr) + bi.biSize + (sizeof(RGBQUAD) * 256));
		fwrite( &hdr, sizeof(hdr), 1, fp);
		fwrite( &bi, sizeof(bi), 1, fp);
		fwrite( colour_table, sizeof(RGBQUAD) * 256, 1, fp);
		if ((int) (fwrite( pImage, width, height, fp)) != height)
		{
			OutputDebugString("CSeedFinder::image_dump: bad write\n");
		}
		fclose(fp);
	}

}

