// SRGRegionInfoScalar.cpp: implementation of the CSRGRegionInfoScalar class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "uts.h"
#include "vcMemRect.h"
#include "Math.h"
#include "SRGEngine.h"
#include "SRGRegionInfoScalar.h"



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSRGRegionInfoScalar::CSRGRegionInfoScalar( int seedID)
	: CSRGRegionInfo( seedID),
	  _pointCount(0),
	  _sumIntensity(0.0)
{
}

CSRGRegionInfoScalar::~CSRGRegionInfoScalar()
{
}

//////////////////////////////////////////////////////////////////////
// Operations
//////////////////////////////////////////////////////////////////////

CSRGRegionInfo * CSRGRegionInfoScalar::Create( int seedID)
{
	return new CSRGRegionInfoScalar( seedID);
}

void CSRGRegionInfoScalar::AddPoint( const int value)
{
	_pointCount++;
	_sumIntensity += value;
}

double CSRGRegionInfoScalar::Distance( const double value)
{
	if (_pointCount > 0)
	{
		return abs(value - (_sumIntensity / _pointCount));
	}
	else
	{
		return 0.0;
	}
}

double CSRGRegionInfoScalar::Mean() const
{
	if (_pointCount > 0)
	{
		return _sumIntensity / _pointCount;
	}
	else
	{
		return 0.0;
	}
}