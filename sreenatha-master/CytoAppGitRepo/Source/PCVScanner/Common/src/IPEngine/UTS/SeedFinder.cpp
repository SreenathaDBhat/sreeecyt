// SeedFinder.cpp: implementation of the CSeedFinder class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "uts.h"
#include "vcMemRect.h"
#include "WoolzIF.h"
#include "Math.h"
#include "hrtimer.h"
#include <vector>
#include <queue>
#include "SeedFinder.h"

// Flags for flooding filling local maxima
const int FILLED	= 0x20;
const int PROCESSED	= 0x40;
const int MAXREGION = 0x80;

struct nbhdOffset
{
	int x;
	int y;
};

const int NEIGHBOURHOODCOUNT = 8;
nbhdOffset dirOffsets[NEIGHBOURHOODCOUNT] =
{
	{ 0, -1 },
	{ 1, -1 },
	{ 1,  0 },
	{ 1,  1 },
	{ 0,  1 },
	{-1,  1 },
	{-1,  0 },
	{-1, -1 }
};

static int imageCounter = 0;

//////////////////////////////////////////////////////////////////////
// CSeedFinder - Finds seeds from images representing significant objects.
// This can be used in conjunction with the Seeded Region Growing class, CSRGEngine.
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSeedFinder::CSeedFinder( CUTSMemRect * pMR, CWoolzIP * pWoolzIP)
	: _pUTSMemRect( pMR),
	  _pWoolzIP( pWoolzIP)
{
	ASSERT( _pUTSMemRect != NULL);
	ASSERT( pWoolzIP != NULL);
}

CSeedFinder::~CSeedFinder()
{
}


//////////////////////////////////////////////////////////////////////
// Operations
//////////////////////////////////////////////////////////////////////

//
// Finds seeds using the max grey level in the source image for each object in the mask image.
// The retunred image is an 8 bit grey image.
//
long CSeedFinder::Find( long hSrc, long hObjectMask)
{
	long result = 0;

	// Check source and mask images are ok
	long srcWidth, srcHeight, srcPitch, srcBPP;
	long mskWidth, mskHeight, mskPitch, mskBPP;
	BYTE *pSrcImage, *pMskImage;

	srcBPP = _pUTSMemRect->GetExtendedInfo( hSrc, &srcWidth, &srcHeight, &srcPitch, &pSrcImage);
	mskBPP = _pUTSMemRect->GetExtendedInfo( hObjectMask, &mskWidth, &mskHeight, &mskPitch, &pMskImage);

	if (srcWidth == mskWidth && srcHeight == mskHeight &&
		srcBPP == EIGHT_BITS_PER_PIXEL &&
		mskBPP == ONE_BIT_PER_PIXEL)
	{
        long hTmp = _pUTSMemRect->CreateAsWithPix( hSrc, EIGHT_BITS_PER_PIXEL);
		result = _pUTSMemRect->CreateAs( hSrc);
        if (hTmp > 0 && result > 0)
        {
			// Firstly, find objects in the mask image
			long tmpWidth, tmpHeight, tempPitch;
		    BYTE *pTempImage;
            _pUTSMemRect->GetExtendedInfo( hTmp, &tmpWidth, &tmpHeight, &tempPitch, &pTempImage);
			// Copy mask to temp
			_pUTSMemRect->Convert(hObjectMask, hTmp, 255);
			// Flip the temp image
            _pUTSMemRect->InvertRows( hTmp);
			// Unpad the UTS image so it can be used by Woolz.
			_pWoolzIP->UnpadImage( hTmp);

            // Look for the woolz objects
			int objectCount = _pWoolzIP->FindObjects( pTempImage, tmpWidth, tmpHeight, 1, 1);

			// Free the temporary image
            _pUTSMemRect->FreeImage( hTmp);


			// Secondly, find the position in each object of the brightest point and
			// mark in the seed image
			long seedWidth, seedHeight, seedPitch;
			BYTE * pSeedImage;
			_pUTSMemRect->GetExtendedInfo( result, &seedWidth, &seedHeight, &seedPitch, &pSeedImage);

			for (int i=0; i<objectCount; i++)
			{
				long maxGrey = 0;
				long seedX = -1;
				long seedY = -1;

				_pWoolzIP->MaxGreyPositionInImage( i, pSrcImage, srcWidth, srcHeight, &seedX, &seedY, &maxGrey);

				if (maxGrey > 0)
				{
					pSeedImage[ seedX + seedY * seedWidth] = 255;
				}
			}
		}
	}

	return result;
}


//
// Finds seeds using the local grey level maxima in the source image for each object in the mask image.
// The returned image is an 8 bit grey image.
//
long CSeedFinder::FindByLocalmaxima( long hSrc, long hObjectMask)
{
	long hSeed = 0;

	// Check source and mask images are ok
	long srcWidth, srcHeight, srcPitch, srcBPP;
	long mskWidth, mskHeight, mskPitch, mskBPP;
	BYTE *pSrcImage, *pMskImage;

	srcBPP = _pUTSMemRect->GetExtendedInfo( hSrc, &srcWidth, &srcHeight, &srcPitch, &pSrcImage);
	mskBPP = _pUTSMemRect->GetExtendedInfo( hObjectMask, &mskWidth, &mskHeight, &mskPitch, &pMskImage);

	if (srcWidth == mskWidth && srcHeight == mskHeight &&
		srcBPP == EIGHT_BITS_PER_PIXEL &&
		mskBPP == ONE_BIT_PER_PIXEL)
	{
        long hTmp = _pUTSMemRect->CreateAsWithPix( hSrc, EIGHT_BITS_PER_PIXEL);
		hSeed = _pUTSMemRect->CreateAs( hSrc);
        if (hTmp > 0 && hSeed > 0)
        {
			// Firstly, find objects in the mask image
			long tmpWidth, tmpHeight, tempPitch;
		    BYTE *pTempImage;
            _pUTSMemRect->GetExtendedInfo( hTmp, &tmpWidth, &tmpHeight, &tempPitch, &pTempImage);
			// Copy mask to temp
			_pUTSMemRect->Convert(hObjectMask, hTmp, 255);
			// Flip the temp image
            _pUTSMemRect->InvertRows( hTmp);
			// Unpad the UTS image so it can be used by Woolz.
			_pWoolzIP->UnpadImage( hTmp);

			// Remove a border of 1 pixel around whole image avoiding edge effects in local maxima determination below
			RemoveBorderWidthOne( pTempImage, tmpWidth, tmpHeight);
            // Look for the woolz objects
			int objectCount = _pWoolzIP->FindObjects( pTempImage, tmpWidth, tmpHeight, 1, 1);

			// Free the temporary image
            _pUTSMemRect->FreeImage( hTmp);


			// Secondly, find the position in each object of all the local maxima and
			// mark them in the seed image
			long seedWidth, seedHeight, seedPitch;
			BYTE * pSeedImage;
			_pUTSMemRect->GetExtendedInfo( hSeed, &seedWidth, &seedHeight, &seedPitch, &pSeedImage);

			CArray<WoolzPoint> maxima;
			for (int i=0; i<objectCount; i++)
			{
				// Find and sort maxima in descending order
				_pWoolzIP->LocalMaximaInImage( i, pSrcImage, srcWidth, srcHeight, &maxima);
				qsort( maxima.GetData(), maxima.GetCount(), sizeof(WoolzPoint), CompareWoolzPoint);

				FilterMaxima( pSrcImage, pSeedImage, seedWidth, seedHeight, maxima, 10);

				maxima.RemoveAll();
			}

			// Cleanup the temporary bit flags in the seed image and keep the maximum regions 
			int imageSize = seedWidth * seedHeight;
			for (int j=0; j<imageSize; j++)
			{
				pSeedImage[j] = (pSeedImage[j] & MAXREGION) ? 255 : 0;
			}
		}
	}

	return hSeed;
}

//
// Flood fill each local maximum point to all pixels within a tolerance
// but ignore this region if it touches an existing flood fill region.
//
// pMaxima is the results image but by using bitflags each plane in it becomes a scratch image.
//
void CSeedFinder::FilterMaxima( BYTE * pSrcImage, BYTE * pMaxima, long width, long height, CArray<WoolzPoint> & maxima, BYTE tolerance)
{
	queue<WoolzPoint> fillQueue;
	vector<int> fillPath;
	WoolzPoint currentPoint;

	for (int i=0; i<maxima.GetCount(); i++)
	{
		// Ignore this maximum if it is Processed
		WoolzPoint maxPoint = maxima[i];
		int offset = maxPoint.x + maxPoint.y * width;
		if ((pMaxima[offset] & PROCESSED) == 0)
		{
			// Flood fill around this local maximum

			// Mark as filled
			fillQueue.push( maxPoint);
			pMaxima[offset] |= FILLED;
			fillPath.push_back( offset);

			bool maxPossible = true;
			//while (!fillQueue.empty() && maxPossible)
			while (!fillQueue.empty())
			{
				// Remove point from queue
				currentPoint = fillQueue.front();
				fillQueue.pop();

				// If we are not on the image edge check neighbourhood for points to fill
				if (currentPoint.x != 0 && currentPoint.x != width - 1 &&
					currentPoint.y != 0 && currentPoint.y != height -1 )
				{
					for (int j=0; j<NEIGHBOURHOODCOUNT; j++)
					{
						WoolzPoint nbhdPoint = WoolzPoint( currentPoint.x + dirOffsets[j].x, currentPoint.y + dirOffsets[j].y, 0);
						offset = nbhdPoint.x + nbhdPoint.y * width;
						nbhdPoint.g = pSrcImage[offset];

						// If point is already filled ignore
						if ((pMaxima[offset] & FILLED) == 0)
						{
							// If point is Processed ignore this local maximum
							// If point value > maximum value ignore this local maximum
							// If point value within tolerance of maximum value Add point to fill queue

							if (pMaxima[offset] & PROCESSED)
							{
								maxPossible = false;
								break;
							}
							else if (nbhdPoint.g > maxPoint.g)
							{
								maxPossible = false;
								break;
							}
							else if (nbhdPoint.g >= maxPoint.g - tolerance)
							{
								// Mark as filled	
								fillQueue.push( nbhdPoint);
								pMaxima[offset] |= FILLED;
								fillPath.push_back( offset);
							}
						}
					}
				}
			}

			//if (imageCounter < 10000)
			//{
			//	CString name;
			//	name.Format( "D:\\LocalMaxima%04d.raw", imageCounter);
			//	image_dump( pMaxima, width, height, (LPTSTR) (LPCTSTR) name);
			//	imageCounter++;
			//}

			// If this local maximum is a maximum then mark up the region
			// otherwise mark up as processed so it is ignored by further flooding
			BYTE bitflags = (maxPossible) ? (PROCESSED | MAXREGION) : PROCESSED;
			for (int f=0; f<(int) fillPath.size(); f++)
			{
				pMaxima[fillPath[f]] = bitflags;
			}

			fillPath.clear();
			while (!fillQueue.empty())
			{
				fillQueue.pop();
			}
		} // if this local maximum is not processed

	} // for each maxima 
}


int CSeedFinder::CompareWoolzPoint(const void * pA, const void * pB)
{
	return ((WoolzPoint *) pB)->g - ((WoolzPoint *) pA)->g;
}

void CSeedFinder::RemoveBorderWidthOne( BYTE * pImage, long width, long height)
{
	BYTE * pTopLine = pImage;
	BYTE * pBottomLine = pImage + width * (height - 1);
	for (int j=0; j<width; j++)
	{
		*pTopLine++ = 0;
		*pBottomLine++ = 0;
	}

	BYTE * pLeftColn = pImage;
	BYTE * pRightColn = pImage + width - 1;
	for (int k=0; k<height; k++)
	{
		*pLeftColn = 0;
		*pRightColn = 0;

		pLeftColn += width;
		pRightColn += width;
	}
}

/*
 *		I M A G E _ D U M P  
 *			--  write image to given file name
 */
void CSeedFinder::image_dump(BYTE * pImage, long width, long height, char *filename)
{
	FILE	*fp;

	if (fp = fopen(filename,"wb"))
	{
		if ((int) (fwrite( pImage, width, height, fp)) != height)
		{
			OutputDebugString("CSeedFinder::image_dump: bad write\n");
		}

		fclose(fp);
	}

}

