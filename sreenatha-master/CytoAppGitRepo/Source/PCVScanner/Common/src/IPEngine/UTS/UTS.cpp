// UTS.cpp : Defines the initialization routines for the DLL.
//
///////////////////////////////////////////////////////////////////////////////////////
//
// An Interface to the Borland C UTS DLLs
//
// Written By Karl Ratcliff 01032001
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include "stdafx.h"
#include <afxdllx.h>

#include "vcmemrect.h"
#include "vcblob.h"
#include "vcgeneric.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "woolzif.h"
#include "vcthresh.h"
#include "vcImProc.h"

#include "uts.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


static AFX_EXTENSION_MODULE UTSDLL = { NULL, NULL };

extern "C" int APIENTRY DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UTS.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UTSDLL, hInstance))
			return 0;

		// Insert this DLL into the resource chain
		// NOTE: If this Extension DLL is being implicitly linked to by
		//  an MFC Regular DLL (such as an ActiveX Control)
		//  instead of an MFC application, then you will want to
		//  remove this line from DllMain and put it in a separate
		//  function exported from this Extension DLL.  The Regular DLL
		//  that uses this Extension DLL should then explicitly call that
		//  function to initialize this Extension DLL.  Otherwise,
		//  the CDynLinkLibrary object will not be attached to the
		//  Regular DLL's resource chain, and serious problems will
		//  result.

		new CDynLinkLibrary(UTSDLL);
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("UTS.DLL Terminating!\n");
		// Terminate the library before destructors are called
		AfxTermExtensionModule(UTSDLL);
	}
	return 1;   // ok
}



static CDibInfo m_DibInfo;

CDibInfo *GetDibInfo(void) { return &m_DibInfo; }