////////////////////////////////////////////////////////////////////////////////////////
// Woolz Interface Class 
//
// Written By karl Ratcliff 22082001
//
////////////////////////////////////////////////////////////////////////////////////////
//
// Member functions of class CWoolzIP that use objects in the internal
// list CWoolzIP::m_pObjectList and associated structures have been moved
// to WoolzIFobjectlist.cpp, as WoolzIF.cpp had become too large, plus separating
// the two sorts of function makes the source code more structured.
//
// PLEASE ALL FUNCTIONS THAT USE CWoolzIP::m_pObjectList in WoolzIFobjectlist.cpp!
// ALL OTHER CWoolzIP FUNCTIONS SHOULD BE PUT IN THIS FILE (WoolzIF.cpp).
//
////////////////////////////////////////////////////////////////////////////////////////
// NOTES
//
// MODS:
//	23Jun2002	JMB	Images allocated with 'new', in both versions of
//					CWoolzIP::WoolzSplitMask(), are now initialised with zeros
//					after creation.

#include "stdafx.h"
#include <math.h>

#include "CSyncObjects.h"

#include "WoolzIF.h"
#include "vcmemrect.h"
#include "uts.h"
#include "vcutsfile.h"

#undef _DO_NOT_DEFINE_FREE
#include "woolz.h"

#define GETXY(x, y)		((dimX * (y)) + (x))




////////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
////////////////////////////////////////////////////////////////////////////////////////

CWoolzIP::CWoolzIP(CUTSMemRect *pMR)
{
    int i;

    // Allocate some space for an object list
    m_pObjectList = (object **) new object[MAX_WOOLZ_OBJECTS];

    // Allocate some space for the labels
    m_pLabelList = new long[MAX_WOOLZ_OBJECTS];

    // Go through and make sure all elements are NULL
    for (i = 0; i < MAX_WOOLZ_OBJECTS; i++)
    {
        m_pObjectList[i] = NULL;
        m_pLabelList[i] = 0;
    }

    // No objects currently found
    m_nNumberObjects = 0;

    // No merged obects
    m_pMergeTable = NULL;
    m_pMeasuredObject = NULL;
    m_pMaskObjectCreated = NULL;
    m_pObjectGreys = NULL;

	// Initialise pool maintenance
	m_pool_index = 0;
	m_object_pool.SetSize(0);

	pUTSMemRect = pMR;
    
	CString MutName;
	MutName.Format(_T("%8x_WOOLZ_MUTEX"), GetCurrentProcessId());
	pWoolzMut = new CSyncObjectMutex(SyncObjectCreate, MutName);
}

CWoolzIP::~CWoolzIP()
{
    // Clear up 
    if (m_pObjectList)
    {
        FreeObjects();              // Clear objects in the list
        delete [] m_pObjectList;    // Delete the list itself
    }

    if (m_pLabelList)
        delete [] m_pLabelList;

    KillMergedObjects();            // Tidy up any merged objects

	FreePool();						// Free object pool list

#ifdef WOOLZ_CLASSES
	delete pLabel;
	delete pFSConstruct;
#endif

	delete pWoolzMut;
}

////////////////////////////////////////////////////////////////////////////////////////
// Initialise the woolz interface
// Returns FALSE if not successful
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::Initialise(void)
{
	KillMergedObjects();
    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////
// Initialise the woolz interface
// Returns FALSE if not successful
////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::KillMergedObjects(void)
{
    POSITION P;
    MergeObj *pM;

    P = m_MergeObjects.GetHeadPosition();
    while (P)
    {
        pM = m_MergeObjects.GetAt(P);
        delete pM;
        m_MergeObjects.GetNext(P);
    }

    m_MergeObjects.RemoveAll();

    if (m_pMergeTable)
    {
        delete m_pMergeTable;
        m_pMergeTable = NULL;
    }

    if (m_pMeasuredObject)
    {
        delete m_pMeasuredObject;
        m_pMeasuredObject = NULL;
    }

    if (m_pMaskObjectCreated)
    {
        delete m_pMaskObjectCreated;
        m_pMaskObjectCreated = NULL;
    }

    if (m_pObjectGreys)
    {
        delete m_pObjectGreys;
        m_pObjectGreys = NULL;
    }
}

////////////////////////////////////////////////////////////////////////////////////////
//	
//	automatic threshold determination by evaluation of grey level
//	gradients in the image, returns >=0 if o.k., UTS_ERROR otherwise
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::GradThresh2(BYTE *image, int cols, int lines)
{
    return gradthresh2(image, cols, lines);
}

////////////////////////////////////////////////////////////////////////////////////////
//
//	BackgroundThresh determines the threshold of an image by
//	first calculating the approximate background level in the image
//	and then analysing the greylevel gradients just above this level
//	i.e. it analyses the contours of the objects.
//  Returns >=0 if o.k., UTS_ERROR otherwise
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::BackgroundThreshold(BYTE *image, int cols, int lines)
{
    return BackgroundThresh(image, cols, lines);
}

int CWoolzIP::BackgroundThreshold16(WORD *image, int cols, int lines)
{
    return BackgroundThresh16(image, cols, lines);
}
////////////////////////////////////////////////////////////////////////////////////////
//
//		F S M I N	( erode grey image )
//
//	Inputs address of frame store, lines, cols , filter half width
//		frame - Pointer to begining of frame store
//		cols  - Number of pixels in a line
//		lines - Number of lines in a frame
//		halfwidth - Filter halfwidth i.e. 1 => 3x3, 2 => 5x5 etc
//
//	Outputs : new frame store containing result of NxN minimum filter
//		  where N = 2*halfwidth + 1
//
//	Uses local histogram ( held in rack[] ) to perform a very efficient
//	running calculation of the local minimum without sorting values.
////////////////////////////////////////////////////////////////////////////////////////
    
BYTE *CWoolzIP::FSMin(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsMin(image, cols, lines, filtsize);
}

////////////////////////////////////////////////////////////////////////////////////////
//
//		F S M A X	( dilate grey image )
//
//	Inputs address of frame store, lines, cols , filter half width
//			frame - Pointer to begining of frame store
//			cols  - Number of pixels in a line
//			lines - Number of lines in a frame
//			halfwidth - Filter halfwidth i.e. 1 => 3x3, 2 => 5x5 etc
//
//	Outputs : new frame store containing result of NxN maximum filter
//		  where N = 2*halfwidth + 1
//
//	Uses local histogram ( held in rack[] ) to perform a very efficient
//	running calculation of the local maximum without sorting values.
////////////////////////////////////////////////////////////////////////////////////////
 
BYTE *CWoolzIP::FSMax(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsMax(image, cols, lines, filtsize);
}

BYTE *CWoolzIP::FSOpen(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsOpen(image, cols, lines, filtsize);
}

BYTE *CWoolzIP::FSClose(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsClose(image, cols, lines, filtsize);
}

BYTE *CWoolzIP::FSTopHat(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsTophat(image, cols, lines, filtsize);
}

BYTE *CWoolzIP::FSMedian(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsMedian(image, cols, lines, filtsize);
}

BYTE *CWoolzIP::FSGaussian(BYTE *image, int cols, int lines, float radius)
{
    return (BYTE *) fsGaussian(image, cols, lines, radius);
}

BYTE *CWoolzIP::FSUnsharpMask(BYTE *image, int cols, int lines, float radius, float depth)
{
    return (BYTE *) fsUnsharpMask(image, cols, lines, radius, depth);
}

BYTE *CWoolzIP::FSExtract(BYTE *image, int cols, int lines, int filtsize, int noisefiltsize)
{
    return (BYTE *) fsExtract(image, cols, lines, filtsize, noisefiltsize);
}

long CWoolzIP::FSClahe(BYTE *image, int cols, int lines, int Min, int Max, int uiNrX, int uiNrY, int uiNrBins, int fCliplimit)
{
	return (long)fsClahe(image, cols, lines, Min, Max, uiNrX, uiNrY, uiNrBins, fCliplimit);
}

////////////////////////////////////////////////////////////////////////////////////////
// Variations of the above using alternate 8 and 4 neighbourhoods on different passes
////////////////////////////////////////////////////////////////////////////////////////
    
BYTE *CWoolzIP::FSMin84(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsMin84(image, cols, lines, filtsize);
}
 
BYTE *CWoolzIP::FSMax84(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsMax84(image, cols, lines, filtsize);
}

////////////////////////////////////////////////////////////////////////////////////////
// Opening and Closing ops with an 84 structuring element
////////////////////////////////////////////////////////////////////////////////////////

BYTE *CWoolzIP::FSOpen84(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsOpen84(image, cols, lines, filtsize);
}

BYTE *CWoolzIP::FSClose84(BYTE *image, int cols, int lines, int filtsize)
{
    return (BYTE *) fsClose84(image, cols, lines, filtsize);
}
	
////////////////////////////////////////////////////////////////////////////////////////
// FSExtract with an 84 structuring element
////////////////////////////////////////////////////////////////////////////////////////

BYTE *CWoolzIP::FSExtract84(BYTE *image, int cols, int lines, int filtsize, int noisefiltsize)
{
    return (BYTE *) fsExtract84(image, cols, lines, filtsize, noisefiltsize);
}


////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::FSSpotBackgroundSubtract(BYTE *image1, int width, int height, int StructSize)
{
	FSCHAR *image2, *im1ptr, *im2ptr;
	int i, min, diff, thresh;

	if (!image1)
		return;

	// find background using large structure element
	image2 = FSOpen84(image1, width, height, StructSize);

	im1ptr = image1;
	im2ptr = image2;

	for (i=0; i< width * height; i++)
	{
		min = (*im2ptr < *im1ptr) ? *im2ptr : *im1ptr;
		diff = *im1ptr - min;

		//thresh = *im1ptr >> 2; // threshold is image value/4 i.e. 25%
		thresh = *im2ptr >> 3; // threshold is image value/4 i.e. 12.5%
		if (thresh < 8)
			thresh = 8;

		if (diff > thresh)
			*im2ptr = diff;
		else
			*im2ptr = 0;

		im1ptr++;
		im2ptr++;
	}

	// Store the background subtracted image back in image 1
	memcpy(image1, image2, width * height);
	
	Free(image2);
}

#define MAXOBJ		100000

BYTE *CWoolzIP::FSSpotFind(BYTE *image1, int width, int height, int MinArea)
{
	if (!image1)
		return NULL;

	// search for objects
	struct object *bigobj, *objlist[MAXOBJ];
	int n, i;

	bigobj=(struct object *)fsconstruct(image1, height, width, 1, 0);

	for (i = 0; i < MAXOBJ; i++)
		objlist[i]=NULL;

	int retobjs = label_area(bigobj, &n, objlist, MAXOBJ, MinArea);						//minimum area of 5
	for (i = 0; i < n; i++)
		vdom21to1(objlist[i]);
	
    // If the return value from label_area is zero then there may be zero objects !
    // Therefore check that the number of returned objects is not zero.
    if (retobjs == 0 && n != 0)
        return NULL;

	freeobj(bigobj);


	// Create an image2 with all pizels zeroed.
	FSCHAR *image2 = (FSCHAR *)Calloc(width * height, 1);

	// draw thresholded spots into image2
	FSCHAR *im2ptr = image2;

	for (i = 0; i < n; i++)
	{
		int thresh = Objgradthresh(objlist[i]);

		if (thresh >0)
			ThreshValues(objlist[i], im2ptr, width, height, thresh);
	}

	// dump all objects
	for (i = 0; i < n; i++) 
	{
		freeobj(objlist[i]);
		objlist[i]=NULL;
	}

	return image2;
}

////////////////////////////////////////////////////////////////////////////////////////
// A Spot processing routine by MG, replaces image1 with a background subtracted version
////////////////////////////////////////////////////////////////////////////////////////

BYTE *CWoolzIP::FSSpotExtract(BYTE *image1, int width, int height, int StructSize, int MinArea)
{
	FSSpotBackgroundSubtract(image1, width, height, StructSize);
	
	BYTE *image2 = FSSpotFind(image1, width, height, MinArea);
	
	return image2;
}


////////////////////////////////////////////////////////////////////////////////////////
//--------------------------------------------------------------------------------------
// PathVysion Image processing routines
//--------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////
//	clear data under idom in an image for a given object with certain value
    
void CWoolzIP::clear_values(struct object *obj, FSCHAR *image, int image_width, int image_height, int value)
{
    FSCHAR *imptr, *imend;
    struct iwspace iwsp;
    struct gwspace gwsp;
    register int k;

    if (wzcheckobj(obj)) {
    	fprintf(stderr,"Bad obj sent to clear_values\n");
    	return;
    }

    if (wzemptyidom(obj->idom)) {
    	fprintf(stderr, "Empty idom sent to clear_values\n");
    	return;
    }

    if (obj->vdom == NULL) {
    	fprintf(stderr, "Null vdom sent to clear_values\n");
    	return;
    }

    if (obj->vdom->type != 1) {
    	fprintf(stderr, "vdom type = %d\n",obj->vdom->type);
    	if (obj->vdom->type == 21)
    		vdom21to1(obj);
    	else
    		return;
    }
    	
    imend=image + image_width * (image_height - 1);
    	
    initgreyscan(obj,&iwsp,&gwsp);
    while (nextgreyinterval(&iwsp) == 0) {

    	imptr=imend - iwsp.linpos * image_width + iwsp.lftpos;

    	for (k=iwsp.lftpos; k<=iwsp.rgtpos; k++) {
    		*imptr=value;
    		imptr++;
    	}
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PathVysion routines
// Process amplified probe images
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BYTE *CWoolzIP::PathVysion(BYTE *image, int width, int height, int SpotWidth, int ClusterWidth, int MinArea)
{
	FSCHAR *image1, *image2, *image3, *imptr1, *imptr2, *imptr3;
	int i;

	if ((!image) || (width<3) || (height<3) || (SpotWidth < 2) || (ClusterWidth < 2))
		return NULL;

	imptr1 = image1 = image;

	imptr2 = image2 = fsExtract(image1, width, height,SpotWidth/2,2);

	imptr3 = image3 = fsExtract(image1, width, height,ClusterWidth/2,2);

	for (i=0; i<width * height; i++)
	{
		int val =0;

		if (*imptr2 > 16)
			val=*imptr2;

		if (*imptr3 > 64)
			val=*imptr3;

		*imptr1++ = val;		// Store background subtracted result in image1

		imptr2++;
		imptr3++;
	}
	Free(image3);

	CLockObject Lock(pWoolzMut, 60000);

	// search for objects
	struct object *bigobj, *objlist[MAX_WOOLZ_OBJECTS];
	int n;

	bigobj=(struct object *)fsconstruct(image1, height, width, 1, 0);

	for (i=0;i<MAX_WOOLZ_OBJECTS;i++)
		objlist[i]=NULL;

   	label_area(bigobj,&n,objlist, MAX_WOOLZ_OBJECTS, MinArea);						//minimum spot area
	for (i = 0; i < n; i++)
		vdom21to1(objlist[i]);

	freeobj(bigobj);
	
	// clear the image2 
	imptr2 = image2;
	for (i=0; i<width * height; i++)
		*imptr2++ = 0;

	// draw thresholded spots into image2
	imptr2 = image2;

	for (i = 0; i < n; i++)
	{
		int thresh = Objgradthresh(objlist[i]);

		if (thresh >0)
			ThreshValues(objlist[i], imptr2, width, height, thresh);
	}

	// dump all objects
	for (i = 0; i < n; i++) 
	{
		freeobj(objlist[i]);
		objlist[i]=NULL;
	}

	return image2;
}
//--------------------------------------------------------------------------------------


/****************************************************************** number_of_tips */
/*
 *	Analyse the curvature of an object and count the number of major peaks
 *	This should roughly represent the number of visible chromosome tips.
 *	If troughs=1 it looks for troughs instead, which are potential cut points.
 */

int CWoolzIP::number_of_features(struct object *obj, BOOL troughs, int threshLevel/*=5*/)
{
	int nfeatures = 0;
	struct object *bobj, *uobj;
	struct object *h;
	struct histogramdomain *hdom;
	struct polygondomain *pdom;
	int *pt;
	int i, n, start, end, mid, wrap, feature;
	int threshold;

	if (troughs)
		threshold = threshLevel;   // Trough threshold
	else
		threshold = -threshLevel;  // Peak threshold

	if (obj == NULL)
		return 0;

	if (obj->type != 1)
		return 0;

	bobj = ibound(obj, 1);
	if (bobj == NULL)
		return 0;

	uobj = unitbound(bobj);
	if (uobj == NULL) 
    {
		freeobj(bobj);
		return 0;
	}

	pdom = (struct polygondomain *)uobj->idom;
	h = curvat(uobj);
	if (h == NULL) 
    {
		freeobj(bobj);
		freeobj(uobj);
		return 0;
	}

	curvsmooth2(h, 10);

	hdom = (struct histogramdomain *)h->idom;
	n = hdom->npoints;
	pt = hdom->hv;

	start = 0;
	end = 0;

	wrap = 20;
	if (wrap > n / 2)
		wrap = n / 2;

	/* scan for features - wrap round to catch end feature */
	for (i = 0; i < n + wrap; i++) 
    {
		if (troughs)
			feature = (*pt >= threshold);
		else
			feature = (*pt <= threshold);

		if (feature) 
        {
			if (start > 0)
				end = i;
			else
				start = i;
		}
		else 
        {
			if (end > 0 && start > 1) 
            {
				if (end - start > 5) 
                {
					mid = (end + start)/ 2;

					if (mid > n - 1)
						mid = mid - n;

					nfeatures++;
				}						
			}

			start = 0;
			end = 0;
					
		}
		if (i == n - 1)
			pt = hdom->hv;
		else
			pt++;
	}

	freeobj(h);
	freeobj(uobj);
	freeobj(bobj);

	return nfeatures;
}


/****************************************************************** feature_pos */
/*
 *	Same operation as number_of_features routine above
 *	but returns the positions of the features in a point list.
 */
#define MAXSPLITS 50

int CWoolzIP::feature_pos(struct object *obj, BOOL troughs, struct ipoint **pointlist, int **position, int *npoints)
{
	int nfeatures = 0;
	struct object *bobj, *uobj;
	struct object *h;
	struct histogramdomain *hdom;
	struct polygondomain *pdom;
	int *pt;
	int i, n, start, end, mid, wrap, feature;
	int threshold;


	if (troughs)
		threshold = 5; /* trough threshold */
	else
		threshold = -5; /* peak threshold */

	
	if (obj == NULL)
		return(0);

	if (obj->type != 1)
		return(0);

	/* malloc space for point list
	 - pointers to positions of possible features */
	*pointlist = (struct ipoint *)Malloc(sizeof(struct ipoint)*MAXSPLITS);
	*position = (int *)Malloc(sizeof(int)*MAXSPLITS);

	if (*pointlist == NULL)
		return(0);


	bobj = ibound(obj, 1);
	if (bobj == NULL)
		return 0;


	uobj = unitbound(bobj);
	if (uobj == NULL)
	{
		freeobj(bobj);
		return 0;
	}

	pdom = (struct polygondomain *)uobj->idom;
	h = curvat(uobj);
	if (h == NULL)
	{
		freeobj(bobj);
		freeobj(uobj);
		return 0;
	}

	curvsmooth2(h,10);


	hdom = (struct histogramdomain *)h->idom;
	n = hdom->npoints;
	pt = hdom->hv;

	start = 0;
	end = 0;

	wrap = 20;
	if (wrap > n / 2)
		wrap = n / 2;

	// return number of points
	*npoints = n;

	/* scan for features - wrap round to catch end feature */
	for (i = 0; i < n + wrap; i++)
	{
		if (troughs)
			feature = (*pt >= threshold);
		else
			feature = (*pt <= threshold);

		if (feature)
		{
			if (start > 0)
				end = i;
			else
				start = i;
		}
		else
		{
			if (end > 0 && start > 1)
			{
				if (end - start > 5)
				{
					mid = (end + start) / 2;

					if (mid > n-1)
						mid = mid - n;

					(*position)[nfeatures] = mid;

					(*pointlist)[nfeatures].type = 40;
					(*pointlist)[nfeatures].k = pdom->vtx[mid].vtX;
					(*pointlist)[nfeatures].l = pdom->vtx[mid].vtY;
					(*pointlist)[nfeatures].style = 1;	
							
					nfeatures++;
				}						
			}

			start=0;
			end=0;
		}
		
		if (i==n-1)
			pt=hdom->hv;
		else
			pt++;

		if (nfeatures == MAXSPLITS)
			break;
	}

	/* check if any feature point positions to return */
	if (nfeatures==0)
	{
		Free(*pointlist);
		*pointlist = NULL;

		Free(*position);
		*position = NULL;

		*npoints=NULL;
	}

	freeobj(h);
	freeobj(uobj);
	freeobj(bobj);

	return nfeatures;
}

//--------------------------------------------------------------------------------------
// binary closing on object

struct object *CWoolzIP::closeObj(struct object *obj, int passes)
{
	struct object *dobj, *nobj;
	int i;

	if ((obj == NULL) || (passes < 1))
		return obj;

	dobj=obj;

	for (i=0; i<passes; i++) {
		nobj = dobj;

		/* alternate dilation structuring element */
		if (i & 1)
			dobj = dilation(nobj);
		else
			dobj = dilation4(nobj);

		if (i>0)
			freeobj(nobj);
	}

	for (i=0; i<passes; i++) {
		nobj = dobj;

		/* alternate dilation structuring element */
		if (i & 1)
			dobj = erosion(nobj);
		else
			dobj = erosion4(nobj);

		freeobj(nobj);
	}

	return dobj;
}

//--------------------------------------------------------------------------------------


void CWoolzIP::splitObjects(FSCHAR *image, int width, int height, int thresh)
{
	FSCHAR *imptr;

	// search image for objects
	struct object *bigobj, *objlist[5000];
	int n, i;

	CLockObject Lock(pWoolzMut, 60000);


    bigobj=(struct object *)fsconstruct(image,height,width,thresh,0);

    for (i=0;i<5000;i++)
    	objlist[i]=NULL;

    label_area(bigobj,&n,objlist,5000,100);									//minimum area of 100, max no objects is 5000
    for (i=0;i<n;i++)
    	vdom21to1(objlist[i]);

    freeobj(bigobj);

	// now transfer all objects which are no larger than a certain size and smoothness to
	// the keeplist - and remove them from the binary image 
	struct ipoint *pointlist=NULL;
	int *position=NULL, npoints;
	int dist, distx, disty, x, y, count, sep1, sep2;
	float xinc, yinc, fx, fy;

	for (i=0;i<n;i++)
	{
		// search for troughs in objects and locate their positions on boundary of object
		position=NULL;
		pointlist=NULL;
		npoints=0;

		int features = feature_pos(objlist[i], TRUE, &pointlist, &position, &npoints);

		// try to split complex objects - but only work on ones with a few features
		// - do splitting directly on blobs in image2, we will re find them again later
		if ((features > 1) && (features < 50))
		{
			// find two points which are close enough to each other to split the objects
			for (int feat1=0; feat1<features-1; feat1++)
			{
				for (int feat2=1; feat2<features; feat2++)
				{
					// first determine seperation around boundary of object
					sep1 = position[feat2] - position[feat1];
					sep2 = npoints -  position[feat2] + position[feat1];

					// ignore if points are too close to each other
					if ((sep1 < 30) || (sep2 < 30))
						continue;

					// then check linear distance between points
					distx = pointlist[feat2].k - pointlist[feat1].k;
					disty = pointlist[feat2].l - pointlist[feat1].l;
					dist = distx*distx + disty*disty;

					// split object if dist between points across object is less than 20;
					if (dist > 400)
						continue;

					// draw splitline through blob
					if (abs(distx) > abs(disty))
					{
                        y = pointlist[feat1].l;
						fy = (float)y;

						xinc = (float)distx / (float)abs(distx);
						yinc = (float)disty / (float)abs(distx);
						count=0;
						for (x=pointlist[feat1].k; count<=abs(distx); x += int(xinc))
						{
							imptr = image + x + width*(height - y);

							*imptr = 0;
							if ((x>1) && (x<width-1) && (y>1) && (y<height-1))
							{
								// nice fat line
								imptr--; *imptr=0;
								imptr-=width; *imptr++=0;
								*imptr++=0;
								*imptr=0; imptr+=width; 
								*imptr=0; imptr+=width;
								*imptr--=0;
							}	*imptr--=0;

							fy+= yinc;
							y=(int)fy;
							count++;
						}
					}
					else
					{
                        x = pointlist[feat1].k;
						fx = (float)x;
						
						if (disty == 0)
							yinc = 0;
						else
							yinc = (float)disty / (float)abs(disty);

						xinc = (float)distx / abs(disty);
						count = 0;
						for (y=pointlist[feat1].l;  count<=abs(disty); y += int(yinc))
						{
							imptr = image + x + width*(height - y);

							*imptr = 0;
							if ((x>1) && (x<width-1) && (y>1) && (y<height-1))
							{
								// nice fat line
								imptr--; *imptr=0;
								imptr-=width; *imptr++=0;
								*imptr++=0;
								*imptr=0; imptr+=width; 
								*imptr=0; imptr+=width;
								*imptr--=0;
							}	*imptr--=0;

							fx+= xinc;
							x=(int)fx;
							count++;
						}
					}

				}
			}
		}

		if (pointlist)
		{
			Free(pointlist);
			pointlist=NULL;
		}

		if (position)
		{
			Free(position);
			position=NULL;
		}
	}

	// dump objects
    for (i=0;i<n;i++) {
    	freeobj(objlist[i]);
    	objlist[i]=NULL;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Routine to segment binary mask of cells from Tissue Image
////////////////////////////////////////////////////////////////////////////////////////////////

BYTE *CWoolzIP::PathOlogical(BYTE *image, int width, int height, int CellWidth)
{
	FSCHAR *image1, *image2, *image3, *imptr1, *imptr2, *imptr3;
	int i;

	if ((!image) || (width <3) || (height<3) || (CellWidth < 2) || (CellWidth > width) || (CellWidth > height))
		return NULL;

	imptr1 = image1 = image;

	imptr2 = image2 = fsExtract(image1, width, height, CellWidth * 3, 2);	// ***** subtract BG 

	if (!image2)
		return NULL;

	imptr3 = image3 = fsOpen84(image2, width, height, CellWidth / 2);		// ***** open using cell halfwidth

	for (i=0; i<width * height; i++)										// invert image 3 into image 2
		*imptr2++ = 255 - *imptr3++;

	imptr2 = fsExtract(image2, width, height,2,2);							// subtract BG to find troughs between cells
	Free(image2);
	image2 = imptr2;

	for (i=0; i<width * height; i++)										// thresold with a value of 1 *** image2 is troughs image ***
		*imptr2++ = (*imptr2 > 1) ? 255 : 0;


	int thresh=BackgroundThresh(image3, width, height);						// automatic threshold determination
																
	imptr3 = image3;
	for (i=0; i<width * height; i++)										// *** image3 is thresholded blob image ***
		*imptr3++ = (*imptr3 > thresh) ? 255 : 0;

	// subtract troughs image from blobs image and store in image3
	imptr2 = image2;
	imptr3 = image3;
	for (i=0; i<width * height; i++)
	{
		if (*imptr3 != 0)
			*imptr3 -= *imptr2;

		imptr2++;
		imptr3++;
	}

	// open 5 to break small attachments - result in image2
	Free(image2);
	image2 = fsOpen84(image3, width, height, 5);				// ***** 5

	CLockObject Lock(pWoolzMut, 60000);

	// split objects by looking for pinched-in regions
	splitObjects(image2, width, height, thresh);


	// search for objects again - now they have been split
	struct object *bigobj, *objlist[5000];
	int n;

	bigobj=(struct object *)fsconstruct(image2,height,width,thresh,0);

    for (i=0;i<5000;i++)
    	objlist[i]=NULL;

    label_area(bigobj,&n,objlist,5000,100);									//***** minimum area of 100
    for (i=0;i<n;i++)
		vdom21to1(objlist[i]);

    freeobj(bigobj);

	// clear the final image 
	imptr2 = image2;
	for (i=0; i<width * height; i++)
		*imptr2++ = 0;

	// look for only single cell objects
	for (i=0;i<n;i++)
	{
		// count troughs in objects 
		int features = number_of_features(objlist[i], 1, 5);

		// keep only smooth objects - draw them into final mask image
		if (features < 2)
		{	
			struct object *fillobj = closeObj(objlist[i], 3);

			// requires a value table for clear_values
			fillobj->vdom = newvaluetb(fillobj,1,255);
    		fillobj->vdom->linkcount++;

			clear_values(fillobj, image2, width, height,255);

			freeobj(fillobj);
		}
	}

	// dump all objects
    for (i=0;i<n;i++) {
    	freeobj(objlist[i]);
    	objlist[i]=NULL;
    }


	// dump temporary images
	Free(image3);

	return image2;	// *****  this image contains the cell mask  *****
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Ensure a type 1 object
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::EnsureType1Object(object *pObject)
{
	BOOL status = FALSE;

	if (pObject)
	{
		if (pObject->type == 1)
		{
			if (wzcheckobj(pObject) == 0) 
			{
				if (wzemptyidom(pObject->idom) == 0) 
				{
					if (pObject->vdom != NULL) 
					{
						if (pObject->vdom->type == 21)
							vdom21to1(pObject);						
						
						if (pObject->vdom->type == 1) 
							status = TRUE;									
					}
				}
			}
		}
	}
	
	return status;
}

////////////////////////////////////////////////////////////////////////////////////////
// Smooth a histogram by applying a low pass filter.
// It is assumed to be a curvature histogram, which has last point
// and first point referring to same boundary location,
// so we wrap round at the ends.
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::curvsmooth2(struct object *histo, int iterations)
{
	register int i;
	register int *iv;
	register int k, km2, km1, kp1, kp2, n;
	struct histogramdomain *hdom;

	if (histo == NULL)
		return 0;

	hdom = (struct histogramdomain *) histo->idom;
	//
	// iterate 1-1-1-1-1 smoothing
	//
	for (n=0; n<iterations; n++) 
    {
		iv = hdom->hv;
		km2 = *(iv + hdom->npoints - 3);
		km1 = *(iv + hdom->npoints - 2);
		kp1 = *(iv);
		kp2 = *(iv + 1);
		for (i = 0; i < hdom->npoints - 2; i++) 
        {
			k = *iv;
			*iv = (km2 + km1 + k + *(iv + 1) + *(iv + 2)) / 5;
			iv++;
			km2 = km1;
			km1 = k;
		}
		k = *iv;
		*iv = (km2 + km1 + *iv + *(iv + 1) + kp1) / 5;

		iv++;
		km2 = km1;
		km1 = k;
		*iv = (km2 + km1 + *iv + kp1 + kp2) / 5;
	}

	return 1;
}



////////////////////////////////////////////////////////////////////////////////////////
//	clear data under idom in an image for a given object with certain value 
////////////////////////////////////////////////////////////////////////////////////////
void CWoolzIP::FillObject(object *pObject, FSCHAR *image, int image_width, int image_height, int value)
{
	if (!EnsureType1Object(pObject))
		return;

    struct iwspace iwsp;
    struct gwspace gwsp;    	
    initgreyscan(pObject, &iwsp, &gwsp);
    
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	FSCHAR *imptr = (FSCHAR *) image + iwsp.linpos * image_width + iwsp.lftpos;

    	for (int k = iwsp.lftpos; k <= iwsp.rgtpos; k++) 
        {
    		*imptr = value;
    		imptr++;
    	}
    }
}   

////////////////////////////////////////////////////////////////////////////////////////
//	Fill an object in an image 
////////////////////////////////////////////////////////////////////////////////////////
   
void CWoolzIP::WzFillObject(struct object *Obj, BYTE *image, int image_width, int image_height, int value)
{
    FSCHAR *imptr, *imend;
    struct iwspace iwsp;
    struct gwspace gwsp;
    register int k;
    	
    imend = image + image_width * (image_height - 1);
    	
    initgreyscan(Obj, &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	imptr=imend - iwsp.linpos * image_width + iwsp.lftpos;

    	for (k = iwsp.lftpos; k <= iwsp.rgtpos; k++) 
        {
    		*imptr = value;
    		imptr++;
    	}
    }
}



//////////////////////////////////////////////////////////////////////////////////
// Some spot processing routines - courtesy of MG
//////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::Objgradthresh(struct object *obj)
{
    struct iwspace iwsp;
    struct gwspace gwsp;
    register GREY *g;
    register int k;
    FSCHAR val;

    int SE=0;
    int SG=0;

    initgreyscan(obj, &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
	{
    	g = gwsp.grintptr;

		int preval = -1;
    	
		for (k=iwsp.lftpos; k<=iwsp.rgtpos; k++) 
		{
    		val = (FSCHAR)*g;

			if (preval>=0)
			{
				int grad = abs(val - preval);
				if (grad > 4)
				{
					SG+= grad;
					SE+= val*grad;
				}
			}

			preval=val;

    		g++; 
    	}
    }

    if (SG == 0)
		return 0;
    else
		return SE/(SG+1);
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Draw an object in image if its grey levels exceed the threshold
////////////////////////////////////////////////////////////////////////////////////////////////
    
void CWoolzIP::ThreshValues(struct object *obj, FSCHAR *image, int image_width, int image_height, int thresh)
{
    BYTE *imptr, *imend;
    struct iwspace iwsp;
    struct gwspace gwsp;
    register GREY *g;
    register int k;
    BYTE val;

    if (wzcheckobj(obj)) 
	{
    	fprintf(stderr,"Bad obj sent to set_values\n");
    	return;
    }

    if (wzemptyidom(obj->idom)) 
	{
    	fprintf(stderr, "Empty idom sent to set_values\n");
    	return;
    }

    if (obj->vdom == NULL) 
	{
    	fprintf(stderr, "Null vdom sent to set_values\n");
    	return;
    }

    if (obj->vdom->type != 1) 
	{
    	fprintf(stderr, "vdom type = %d\n",obj->vdom->type);
    	if (obj->vdom->type == 21)
    		vdom21to1(obj);
    	else
    		return;
    }
    	
    imend=image + image_width * (image_height - 1);
    	
    initgreyscan(obj, &iwsp, &gwsp);
    
	while (nextgreyinterval(&iwsp) == 0) 
	{
    	g = gwsp.grintptr;

    	imptr=imend - iwsp.linpos * image_width + iwsp.lftpos;

    	for (k=iwsp.lftpos; k<=iwsp.rgtpos; k++) 
		{
    		val = (BYTE) *g;
			if (val>thresh)
    			*imptr = 255;
			else
				*imptr = 0;

    		g++; imptr++;
    	}
    }
}


////////////////////////////////////////////////////////////////////////////////////////
// WoolzSplitMask
// Parameters:
//      RangeImage   - this is the source image and will contain objects within the 
//                     range MinArea to MaxArea on exit.
//      MinMask      - this image contains all objects < MinArea in area
//      MaxMask      - this image contains all objects > MaxArea in area
//		MinArea		 - min range limit
//		MaxArea		 - max range limit
//      NumInRange   - number of objects in RangeImage
//      NumInMin     - number of objects in MinMaskImage
//      NumInMax     - number of objects in MaxMaskImage
////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::WoolzSplitMask(long RangeImage,  long MinMask, long MaxMask,     
							  long MinArea,     long MaxArea, 
							  long *NumInMin,   long *NumInRange, long *NumInMax)
{
    long Grey8;
    long W, H, P;
    BYTE *Addr;
    int i;
    BYTE *MinIm, *MaxIm, *Range;
    int A;
    long ImMin, ImMax, ImRange;

	CLockObject Lock(pWoolzMut, 60000);

    // Create an 8 bit grey level equivalent of RangeImage
    Grey8 = pUTSMemRect->CreateAsWithPix(RangeImage, EIGHT_BITS_PER_PIXEL);
    // Convert the mask image to an 8 bit one
    pUTSMemRect->Convert(RangeImage, Grey8, 255);
    // Now unpad it for use with Woolz
    UnpadImage(Grey8);
    // Get the address of the image data 
    pUTSMemRect->GetExtendedInfo(Grey8, &W, &H, &P, &Addr);

	// search for objects
	struct object *BigObj, *ObjList[MAX_WOOLZ_OBJECTS];
	int n;

#ifdef WOOLZ_CLASSES
	BigObj =(struct object *)pFSConstruct->FSConstruct(Addr, (short) H, (short) W, 1, 0);

	for (i = 0; i < MAX_WOOLZ_OBJECTS; i++)
		ObjList[i] = NULL;

   	pLabel->LabelArea(BigObj, &n, ObjList, MAX_WOOLZ_OBJECTS, 1);      // Min area of one						
	for (i = 0; i < n; i++)
		vdom21to1(ObjList[i]);
#else
	for (i = 0; i < MAX_WOOLZ_OBJECTS; i++)
		ObjList[i] = NULL;

	BigObj =(struct object *)fsconstruct(Addr, (short) H, (short) W, 1, 0);

   	label_area(BigObj, &n, ObjList, MAX_WOOLZ_OBJECTS, 1);      // Min area of one						
	for (i = 0; i < n; i++)
		vdom21to1(ObjList[i]);

#endif

    // Create three images and draw objects with specific areas into each of these
    MinIm = new BYTE[W * H];
    MaxIm = new BYTE[W * H];
    Range = new BYTE[W * H];

	// Clear images
	memset(MinIm, 0, W * H);
	memset(MaxIm, 0, W * H);
	memset(Range, 0, W * H);

    *NumInRange = 0;
    *NumInMin = 0;
    *NumInMax = 0;

    // Now draw objects in each image depending on its area
    for (i = 0; i < n; i++)
    {
        // Get the objects area
        A = area(ObjList[i]);
        if (A < MinArea)
        {
            (*NumInMin)++;
            WzFillObject(ObjList[i], MinIm, W, H, 255); // Draw object in min area image
        }
        else
        if (A < MaxArea)
        {
            (*NumInRange)++;
            WzFillObject(ObjList[i], Range, W, H, 255); // Draw object in range image
        }
        else
        {
            (*NumInMax)++;
            WzFillObject(ObjList[i], MaxIm, W, H, 255); // Must be in the max image
        }

        // Free the object as we go
        freeobj(ObjList[i]);
    }

    // Now do the inefficient padding and conversion part
    ImMin = MakePaddedImage(Grey8, MinIm);
    ImMax = MakePaddedImage(Grey8, MaxIm);
    ImRange = MakePaddedImage(Grey8, Range);

    // Get rid of unwanted temp images
    delete MinIm;
    delete MaxIm;
    delete Range;

    // Free the grey level image
    pUTSMemRect->FreeImage(Grey8);

    pUTSMemRect->Convert(ImMin, MinMask, 255);
    pUTSMemRect->Convert(ImMax, MaxMask, 255);
    pUTSMemRect->Convert(ImRange, RangeImage, 255);

    // Now tidy up
    pUTSMemRect->FreeImage(ImMin);
    pUTSMemRect->FreeImage(ImMax);
    pUTSMemRect->FreeImage(ImRange);

	freeobj(BigObj);
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzSplitMask
// Description:
//      This variant of WoolzSplitMask does not create images for objects below
//      the min area threshold or above the max area threshold. Thus the function
//      is quicker.
// Parameters:
//      RangeImage   - this is the source image and will contain objects within the 
//                     range MinArea to MaxArea on exit.
//		MinArea		 - min range limit
//		MaxArea		 - max range limit
// Returns:
//      Number of objects in the range specified
////////////////////////////////////////////////////////////////////////////////////////

long CWoolzIP::WoolzSplitMask(long RangeImage, long MinArea, long MaxArea)
{
    long Grey8;
    long W, H, P, SrcBPP;
    BYTE *Addr;
    int i;
    BYTE *Range;
    int A;
    long ImRange;
    long NumInRange;

	SrcBPP = pUTSMemRect->GetRectInfo(RangeImage, &W, &H);

	if (SrcBPP != 8)
	{
		// Create an 8 bit grey level equivalent of RangeImage
		Grey8 = pUTSMemRect->CreateAsWithPix(RangeImage, EIGHT_BITS_PER_PIXEL);
		// Convert the mask image to an 8 bit one
		pUTSMemRect->Convert(RangeImage, Grey8, 255);
	}
	else
		Grey8 = RangeImage;

    // Now unpad it for use with Woolz
    UnpadImage(Grey8);
    // Get the address of the image data 
    pUTSMemRect->GetExtendedInfo(Grey8, &W, &H, &P, &Addr);

	// search for objects
	struct object *BigObj, *ObjList[MAX_WOOLZ_OBJECTS];
	int n;

	CLockObject Lock(pWoolzMut, 60000);


#ifdef WOOLZ_CLASSES
	BigObj =(struct object *)pFSConstruct->FSConstruct(Addr, (short) H, (short) W, 1, 0);

	for (i = 0; i < MAX_WOOLZ_OBJECTS; i++)
		ObjList[i] = NULL;

   	pLabel->LabelArea(BigObj, &n, ObjList, MAX_WOOLZ_OBJECTS, 1);      // Min area of one						
	for (i = 0; i < n; i++)
		vdom21to1(ObjList[i]);
#else

	for (i = 0; i < MAX_WOOLZ_OBJECTS; i++)
		ObjList[i] = NULL;


	BigObj =(struct object *)fsconstruct(Addr, (short) H, (short) W, 1, 0);

   	label_area(BigObj, &n, ObjList, MAX_WOOLZ_OBJECTS, 1);      // Min area of one	

	for (i = 0; i < n; i++)
		vdom21to1(ObjList[i]);
#endif
	
    // Create three images and draw objects with specific areas into each of these
    Range = new BYTE[W * H];
	// Clear image
	memset(Range, 0, W * H);

    NumInRange = 0;

    // Now draw objects in each image depending on its area
    for (i = 0; i < n; i++)
    {
        A = area(ObjList[i]);
        if (A >= MinArea && A <= MaxArea)
        {
            NumInRange++;
            WzFillObject(ObjList[i], Range, W, H, 255); // Draw object in range image
        }

        // Free the object as we go
        freeobj(ObjList[i]);
    }

	Lock.UnLock();

    ImRange = MakePaddedImage(Grey8, Range);

    // Get rid of unwanted temp images
    delete Range;

	if (SrcBPP != 8)
	{
		// Free the grey level image
		pUTSMemRect->FreeImage(Grey8);

		// Convert back to a 1 bit mask
		pUTSMemRect->Convert(ImRange, RangeImage, 255);
	}
	else
		pUTSMemRect->Move(ImRange, RangeImage);

	// Now tidy up
	pUTSMemRect->FreeImage(ImRange);

	freeobj(BigObj);

    // Return the number of objects in range
    return NumInRange;
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzRemoveEdgeObjects
// Description:
//      This routine will remove any objects within the borderwidth of the image
//      edges.
// Parameters:
//      RangeImage   - this is the source image and will contain objects within the 
//                     range MinArea to MaxArea on exit.
//		Borderwidth  - If object lies in border then remove it
// Returns:
//      Number of objects in the range specified
////////////////////////////////////////////////////////////////////////////////////////

long CWoolzIP::WoolzRemoveEdgeObjects(long RangeImage, long BorderWidth)
{
    long Grey8;
    long W, H, P;
    BYTE *Addr;
    int i;
    BYTE *Range;
    long ImRange;
    long ObjectsLeft;
    struct iwspace iwsp;
    struct gwspace gwsp;
    long Xmin, Xmax, Ymin, Ymax;

	CLockObject Lock(pWoolzMut);

    // Create an 8 bit grey level equivalent of RangeImage
    Grey8 = pUTSMemRect->CreateAsWithPix(RangeImage, EIGHT_BITS_PER_PIXEL);
    // Convert the mask image to an 8 bit one
    pUTSMemRect->Convert(RangeImage, Grey8, 255);
    // Now unpad it for use with Woolz
    UnpadImage(Grey8);
    // Get the address of the image data 
    pUTSMemRect->GetExtendedInfo(Grey8, &W, &H, &P, &Addr);

	// search for objects
	struct object *BigObj, *ObjList[MAX_WOOLZ_OBJECTS];
	int n;

#ifdef WOOLZ_CLASSES
	BigObj =(struct object *)pFSConstruct->FSConstruct(Addr, (short) H, (short) W, 1, 0);

	for (i = 0; i < MAX_WOOLZ_OBJECTS; i++)
		ObjList[i] = NULL;

   	pLabel->LabelArea(BigObj, &n, ObjList, MAX_WOOLZ_OBJECTS, 1);      // Min area of one						
	for (i = 0; i < n; i++)
		vdom21to1(ObjList[i]);
#else
	for (i = 0; i < MAX_WOOLZ_OBJECTS; i++)
		ObjList[i] = NULL;


	BigObj =(struct object *)fsconstruct(Addr, (short) H, (short) W, 1, 0);

   	label_area(BigObj, &n, ObjList, MAX_WOOLZ_OBJECTS, 1);      // Min area of one	

	for (i = 0; i < n; i++)
		vdom21to1(ObjList[i]);
#endif
	
    // Create three images and draw objects with specific areas into each of these
    Range = new BYTE[W * H];
	// Clear image
	memset(Range, 0, W * H);

    ObjectsLeft = 0;

    // Now draw objects in each image depending on its area
    for (i = 0; i < n; i++)
    {
   	    Xmin = LONG_MAX;
	    Ymin = LONG_MAX;
	    Xmax = LONG_MIN;
	    Ymax = LONG_MIN;

        // Determine the bounding box for this object
        initgreyscan(ObjList[i], &iwsp, &gwsp);
        while (nextgreyinterval(&iwsp) == 0) 
        {
		    if (iwsp.linpos < Ymin) Ymin = iwsp.linpos;
		    if (iwsp.linpos > Ymax) Ymax = iwsp.linpos;
		    if (iwsp.lftpos < Xmin) Xmin = iwsp.lftpos;
		    if (iwsp.rgtpos > Xmax) Xmax = iwsp.rgtpos;
        }
     
        if ((Ymin > BorderWidth) && (Xmin > BorderWidth) && (Xmax < (W - BorderWidth)) && (Ymax < (H - BorderWidth)))
        {
            ObjectsLeft++;
            WzFillObject(ObjList[i], Range, W, H, 255); // Draw object in range image
        }

        // Free the object as we go
        freeobj(ObjList[i]);
    }

    ImRange = MakePaddedImage(Grey8, Range);

    // Get rid of unwanted temp images
    delete Range;

    // Free the grey level image
    pUTSMemRect->FreeImage(Grey8);

    // Convert back to a 1 bit mask
    pUTSMemRect->Convert(ImRange, RangeImage, 255);

    // Now tidy up
    pUTSMemRect->FreeImage(ImRange);

	freeobj(BigObj);

    // Return the number of objects in range
    return ObjectsLeft;
}

////////////////////////////////////////////////////////////////////////////////////////
// WoolzNumberOfComponents
// Description:
//      Just returns the number of objects and the area of the largest object
// Parameters:
//      RangeImage   - this is the source image and will contain objects within the 
//                     range MinArea to MaxArea on exit.
//		MaxArea		 - largest object area
// Returns:
//      Number of objects in the image
////////////////////////////////////////////////////////////////////////////////////////

long CWoolzIP::WoolzNumberOfComponents(long RangeImage, long *MaxArea)
{
    long Grey8;
    long W, H, P;
    BYTE *Addr;
    int i;
    int A;

	CLockObject Lock(pWoolzMut, 60000);

    // Create an 8 bit grey level equivalent of RangeImage
    Grey8 = pUTSMemRect->CreateAsWithPix(RangeImage, EIGHT_BITS_PER_PIXEL);
    // Convert the mask image to an 8 bit one
    pUTSMemRect->Convert(RangeImage, Grey8, 255);
    // Now unpad it for use with Woolz
    UnpadImage(Grey8);
    // Get the address of the image data 
    pUTSMemRect->GetExtendedInfo(Grey8, &W, &H, &P, &Addr);
	// search for objects
	struct object *BigObj, *ObjList[MAX_WOOLZ_OBJECTS * 2];
	int n;

#ifdef WOOLZ_CLASSES
	BigObj =(struct object *)pFSConstruct->FSConstruct(Addr, (short) H, (short) W, 1, 0);

	for (i = 0; i < MAX_WOOLZ_OBJECTS*2; i++)
		ObjList[i] = NULL;

   	pLabel->LabelArea(BigObj, &n, ObjList, MAX_WOOLZ_OBJECTS*2, 1);      // Min area of one						
	for (i = 0; i < n; i++)
		vdom21to1(ObjList[i]);
#else
	for (i = 0; i < MAX_WOOLZ_OBJECTS*2; i++)
		ObjList[i] = NULL;

	BigObj =(struct object *)fsconstruct(Addr, (short) H, (short) W, 1, 0);

	for (i = 0; i < MAX_WOOLZ_OBJECTS * 2; i++)
		ObjList[i] = NULL;

   	label_area(BigObj, &n, ObjList, MAX_WOOLZ_OBJECTS * 2, 1);      // Min area of one						
	for (i = 0; i < n; i++)
		vdom21to1(ObjList[i]);
#endif
	
    *MaxArea = 0;

    // Now draw objects in each image depending on its area
    for (i = 0; i < n; i++)
    {
        A = area(ObjList[i]);
        if (A > *MaxArea)
            *MaxArea = A;

        // Free the object as we go
        freeobj(ObjList[i]);
    }

    // Free the grey level image
    pUTSMemRect->FreeImage(Grey8);

	freeobj(BigObj);

    // Return the number of objects in range
    return n;
}

////////////////////////////////////////////////////////////////////////////////////////
// Unpad a UTS image for use by Woolz - i.e. remove any alignment
////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::UnpadImage(long SrcHandle)
{
	BYTE *Addr;
	BYTE *SrcAddr, *DestAddr;
	long Width, Height, Pitch, BPP, NBytesW;
	long y;

	BPP = pUTSMemRect->GetExtendedInfo(SrcHandle, &Width, &Height, &Pitch, &Addr);

	// Only unpad the image if it is not aligned i.e. width and pitch differ
	if (Width != Pitch)
	{
		SrcAddr = Addr;					// Src address to copy from
		DestAddr = Addr;				// Dest address to copy to
		NBytesW = (Width * BPP) >> 3;	// Number of bytes in a row
					
		for (y = 0; y < Height; y++)
		{
			memmove(DestAddr, SrcAddr, NBytesW);
			SrcAddr += Pitch;
			DestAddr += NBytesW;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////
// Re-align a UTS image after Woolz is done with it
////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::PadImage(long SrcHandle)
{
	BYTE *Addr;
	BYTE *SrcAddr, *DestAddr;
	long Width, Height, Pitch, BPP, NBytesW;
	long y;

	BPP = pUTSMemRect->GetExtendedInfo(SrcHandle, &Width, &Height, &Pitch, &Addr);

	// Only pad the image if it is not aligned i.e. width and pitch differ
	if (Width != Pitch)
	{
		NBytesW = (Width * BPP) >> 3;	// Number of bytes in a row
		SrcAddr = Addr + (Height - 1) * NBytesW;	// Src address to copy from
		DestAddr = Addr + (Height - 1) * Pitch;	    // Dest address to copy to

		for (y = 0; y < Height; y++)
		{
			memmove(DestAddr, SrcAddr, NBytesW);
			DestAddr -= Pitch;
			SrcAddr -= NBytesW;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////
// Create a padded UTS image from a Woolz created image and an original UTS image
////////////////////////////////////////////////////////////////////////////////////////

long CWoolzIP::MakePaddedImage(long SrcHandle, BYTE *ImageAddr)
{
	BYTE *DAddr;
	BYTE *SrcAddr, *DestAddr;
	long Width, Height, Pitch, BPP, NBytesW, NBytesP;
	long y;
	long DestHandle;

	DestHandle = pUTSMemRect->CreateAs(SrcHandle);
	BPP = pUTSMemRect->GetExtendedInfo(DestHandle, &Width, &Height, &Pitch, &DAddr);

	// Only unpad the image if it is not aligned i.e. width and pitch differ
	if (Width != Pitch)
	{
		NBytesW = (Width * BPP) >> 3;	// Number of bytes in a row
		NBytesP = (Pitch * BPP) >> 3;
		SrcAddr = ImageAddr + (Height - 1) * NBytesW;	// Src address to copy from
		DestAddr = DAddr + (Height - 1) * NBytesP;		// Dest address to copy to
			
		for (y = 1; y < Height; y++)
		{
			memcpy(DestAddr, SrcAddr, NBytesW);
			SrcAddr  -= NBytesW;
			DestAddr -= NBytesP;
		}
	}
	else
	{
		// Straight forward memory copy
		pUTSMemRect->SetBits(ImageAddr, DestHandle);
	}

	return DestHandle;
}

//////////////////////////////////////////////////////////////////////////////

// Copy the data from a UTS image whose bit depth is a multiple of 8
// to a new Woolz 'Frame Store' i.e. an 8bpp unpadded memory buffer.
BYTE *CWoolzIP::CreateWoolzFSFromUTSImage(long hSrc, long &width, long &height, long &srcBpp)
{
	BYTE *pFS = NULL;

    if (hSrc > 0 && pUTSMemRect)
	{
		long pitch = 0;
		BYTE *pSrcAddress = NULL;
		srcBpp = pUTSMemRect->GetExtendedInfo(hSrc, &width, &height, &pitch, &pSrcAddress);
		if (srcBpp > 0 && srcBpp % 8 == 0 && pSrcAddress) // Check bpp is multiple of 8
		{
			pFS = (BYTE*)Malloc(width * height);
			if (pFS)
			{
				BYTE *pSrcLine = pSrcAddress;
				BYTE *pF = pFS;

				int skip = (srcBpp / 8) - 1;

				for (int y = 0; y < height; y++)
				{
					BYTE *pSrc = pSrcLine;
					
					for (int x = 0; x < width; x++)
					{
						pSrc += skip; // Skip less significant bytes.
						*pF++ = *pSrc++; // Copy most significant byte.
					}

					pSrcLine += pitch;
				}
			}
		}
	}

	return pFS;
}

// Copy the data from a Woolz 'Frame Store' to a UTS image with the given bit depth.
long CWoolzIP::CreateUTSImageFromWoolzFS(BYTE *pFS, long width, long height, long outputBpp)
{
	long hUTSImage = 0;

    if (pFS && pUTSMemRect && outputBpp > 0 && outputBpp % 8 == 0) // Check bpp is multiple of 8
	{
		hUTSImage = pUTSMemRect->Create(outputBpp, width, height);
		if (hUTSImage > 0)
		{
			long pitch;
			BYTE *pDstAddress = NULL;
			if (pUTSMemRect->GetExtendedInfo(hUTSImage, &width, &height, &pitch, &pDstAddress) == outputBpp)
			{
				BYTE *pDstLine = pDstAddress;

				int skip = (outputBpp / 8) - 1;

				for (int y = 0; y < height; y++)
				{
					BYTE *pDst = pDstLine;
					
					for (int x = 0; x < width; x++)
					{
						for (int i = 0; i < skip; i++)
							*pDst++ = 0; // Zero less significant bytes.

						*pDst++ = *pFS++; // Copy to most significant byte.
					}

					pDstLine += pitch;
				}
			}
		}
	}

	return hUTSImage;
}

// Free a pointer returned by CreateWoolzFSFromUTSImage().
void CWoolzIP::FreeWoolzFS(BYTE *pFS)
{
	Free(pFS);
}

//////////////////////////////////////////////////////////////////////////////

long CWoolzIP::WoolzDeagglomerate(long RangeImage, long DeagglomImage)
{
    long W, H, P;
    BYTE *AddrIn, *AddrOut;
    int i,n,obj_num;
    long GreyIn, GreyOut;

    // Create an 8 bit grey level equivalent of RangeImage
    GreyIn  = pUTSMemRect->CreateAsWithPix(RangeImage, EIGHT_BITS_PER_PIXEL);
    GreyOut = pUTSMemRect->CreateAsWithPix(DeagglomImage, EIGHT_BITS_PER_PIXEL);

    // Convert the mask image to an 8 bit one
    pUTSMemRect->Convert(RangeImage, GreyIn, 255);
    pUTSMemRect->Convert(DeagglomImage, GreyOut, 255);
    // Now unpad it for use with Woolz
    UnpadImage(GreyIn);
    UnpadImage(GreyOut);
    // Get the address of the image data 
    pUTSMemRect->GetExtendedInfo(GreyIn, &W, &H, &P, &AddrIn);
    pUTSMemRect->GetExtendedInfo(GreyOut, &W, &H, &P, &AddrOut);
	// search for objects
	struct object *BigObj, *ObjList[MAX_WOOLZ_OBJECTS], *TempObjList[MAX_WOOLZ_OBJECTS], *DeagglomObjList[MAX_WOOLZ_OBJECTS];

#ifdef WOOLZ_CLASSES
	BigObj =(struct object *)pFSConstruct->FSConstruct(AddrIn, (short) H, (short) W, 1, 0);

	for (i = 0; i < MAX_WOOLZ_OBJECTS; i++)
	{
		ObjList[i] = NULL;
		DeagglomObjList[i] = NULL;
		TempObjList[i] = NULL;
	}


   	pLabel->LabelArea(BigObj, &n, ObjList, MAX_WOOLZ_OBJECTS * 5, 1);      // Min area of one						
	for (i = 0; i < n; i++)
		vdom21to1(ObjList[i]);
#else
	for (i = 0; i < MAX_WOOLZ_OBJECTS; i++)
	{
		ObjList[i] = NULL;
		DeagglomObjList[i] = NULL;
		TempObjList[i] = NULL;
	}

	CLockObject Lock(pWoolzMut, 60000);

	BigObj =(struct object *)fsconstruct(AddrIn, (short) H, (short) W, 1, 0);

   	label_area(BigObj, &n, ObjList, MAX_WOOLZ_OBJECTS * 5, 1);      // Min area of one		

	Lock.UnLock();

	for (i = 0; i < n; i++)
		vdom21to1(ObjList[i]);
#endif


	obj_num=0;
    for (i = 0; i < n; i++)
	{
       	struct object * obj = deagglomerate(ObjList[i]);
		WzFillObject(obj, AddrOut, W, H, 255); // Draw object in range image
		freeobj(obj);
	}


	PadImage(GreyIn);
	PadImage(GreyOut);

    // Convert the 8 bit image back to a mask
    pUTSMemRect->Convert(GreyOut, DeagglomImage, 1);

    // Tidy up
	freeobj(BigObj);

    // Free the grey level image
    pUTSMemRect->FreeImage(GreyIn);
    pUTSMemRect->FreeImage(GreyOut);

    // Return the number of objects in range
    return 0;
}




////////////////////////////////////////////////////////////////////////////////////////
//	Fill an object in an image if seed is inside object 
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::WzFillObjectSeed(struct object *obj, BYTE *image, int cols, int lines, int col, int line,int value)
{
    FSCHAR *imptr, *imend;
    struct iwspace iwsp;
    register int k;
	BOOL seedinobj=FALSE;

	initrasterscan(obj,&iwsp,0);
	
	while (nextinterval(&iwsp) == 0)
    	if (iwsp.linpos == (lines-line-1))
    		if ((iwsp.lftpos <= col) && (iwsp.rgtpos >= col))
			{
				seedinobj=TRUE;
				break;
			}
    	
    imend = image + cols * (lines - 1);

	if (seedinobj)
	{
		initrasterscan(obj,&iwsp,0);
		while (nextinterval(&iwsp) == 0)
		{
			imptr=imend - iwsp.linpos * cols + iwsp.lftpos;
			for (k = iwsp.lftpos; k <= iwsp.rgtpos; k++) 
			{
				*imptr = value;
				imptr++;
			}
		}
	}

	return seedinobj;
}


void CWoolzIP::RangeThreshold(long ImageHandle, long ImageMask, short LowThreshold, short HighThreshold) 
{
    long LowMask;//, BPP, Temp;
    BOOL RangeOk = TRUE;

	if (LowThreshold < 0)
	{
		pUTSMemRect->Threshold(ImageHandle, ImageMask, "<=", HighThreshold);
	}
	else
	{
		if (HighThreshold > 255)
		{
			pUTSMemRect->Threshold(ImageHandle, ImageMask, ">=", LowThreshold);
		}
		else
		{
            // Create the mask for the thresholded result
            LowMask = pUTSMemRect->CreateAsWithPix(ImageHandle, ONE_BIT_PER_PIXEL);

            pUTSMemRect->Threshold(ImageHandle, LowMask, ">=", LowThreshold);
            pUTSMemRect->Threshold(ImageHandle, ImageMask, "<=", HighThreshold);
			// JMB: Must AND masks if thresholds are equal!
            if (LowThreshold <= HighThreshold)
                pUTSMemRect->Pixelwise(LowMask, "And", ImageMask);      // AND the results together
            else
                pUTSMemRect->Pixelwise(LowMask, "Or", ImageMask);       // OR the results together

			// Free any temporary images
			pUTSMemRect->FreeImage(LowMask);
		}
	}
}



////////////////////////////////////////////////////////////////////////////////////////
// Texture metaphase finder
//					 Clumpy bone	Blood + Wide Spreads
//	maxFeatureWidth		11					20				typical values
////////////////////////////////////////////////////////////////////////////////////////

BYTE* CWoolzIP::TextureMetFind(BYTE *image, int width, int height, int maxFeatureWidth)
{
	if ((image == NULL) || (width < 5) || (height < 5))
		return NULL;

	int minContrast = 20;	// we may make this a parameter in future

	BYTE *textureIm = (BYTE *)calloc(width * height, 1);

	// imptr1 points to source image data
	BYTE *imptr1= image;

	int max1 = 0;
	int max2 = 0;
	int min = 255;
	int level = 0;
	int max1x  =0;
	int max2x = 0;
	int minx = 0;
	
	imptr1= image;

	// first pass -  perform analysis horizontally
	for (int row=0; row<height; row++)
	{
		imptr1= image + row * width;
		max1 = max2 = 0;
		min = 255;
		max1x  =0;
		max2x = 0;
		minx = 0;

		for (int col=0;col<width;col++)
		{
			int val=*imptr1++;

			val = 255 - val;	// Woolz

			if (min==255) // looking for max1
			{
				if (val > max1+8)
				{
					max1 = val;
					max1x = col;
					level = 0;
				}
				else
					if (val < max1-8)
					{
						//max1x += level/2;		// choose mid point of level peak
						max1x = col - 1;
						min = val;
						minx = col;
						level = 0;
					}
					else
						level++;				// travelling across plateau
			}
			else
			{
				if (max2 == 0)	// looking for min
				{
					if (val < min-8)
					{
						min = val;
						minx = col;
						level = 0;
					}
					else
						if (val > min+8)
						{
							minx += level/2;
							max2 = val;
							max2x = col;
							level = 0;
						}
						else
							level++;
				}
				else	// looking for max2
				{
					if (val > max2+8)
					{
						max2 = val;
						max2x = col;
						level =0;
					}
					else
						if (val < max2-8)
						{
							//max2x += level/2;
							max2x = col - 1;
							// calculate roughness value and store in textureIm
							int v1 = max1 - min;
							int h1 = minx - max1x;
							int v2 = max2 - min;
							int h2 = max2x - minx;
							// check roughness has good minContrast - ignore noise
							if ((v1+v2 > 2*minContrast) && (h1+h2 < maxFeatureWidth))
							{
								// check ratio is greater than 0.6
								if ((float)min(v1,v2)/(float)max(v1,v2) > 0.6)
								{
									int ratio = 8*(v1 + v2)/(h1+h2);
									if (ratio>255)
										ratio = 255;
									FSCHAR *textureImptr = minx + row * width + textureIm;
									*textureImptr = ratio;
								}
							}

							// and start again
							max1 = max2;
							max1x = max2x;
							max2 = 0;
							min = 255;
							level = 0;
							max2x = 0;
							minx = 0;
						}
						else
							level++;
				}
			}
		}	// next col
	}	// next row


	// second pass - perform analysis vertically
	imptr1= image;

	for (int col=0;col<width;col++)
	{
		imptr1= image + col;
		max1 = max2 = 0;
		min = 255;
		max1x  =0;
		max2x = 0;
		minx = 0;

		for (int row=0; row<height; row++)
		{
			int val=*imptr1;
			val = 255 - val;	// Woolz

			imptr1+=width;

			if (min==255) // looking for max1
			{
				if (val > max1+8)
				{
					max1 = val;
					max1x = row;
					level = 0;
				}
				else
					if (val < max1-8)
					{
						//max1x += level/2;		// choose mid point of level peak
						max1x = row - 1;
						min = val;
						minx = row;
						level = 0;
					}
					else
						level++;				// travelling across plateau
			}
			else
			{
				if (max2 == 0)	// looking for min
				{
					if (val < min-8)
					{
						min = val;
						minx = row;
						level = 0;
					}
					else
						if (val > min+8)
						{
							minx += level/2;
							max2 = val;
							max2x = row;
							level = 0;
						}
						else
							level++;
				}
				else	// looking for max2
				{
					if (val > max2+8)
					{
						max2 = val;
						max2x = row;
						level =0;
					}
					else
						if (val < max2-8)
						{
							//max2x += level/2;
							max2x = row - 1;

							// calculate roughness value and store in textureIm
							int v1 = max1 - min;
							int h1 = minx - max1x;
							int v2 = max2 - min;
							int h2 = max2x - minx;
							// check rough has good minContrast - ignore noise
							if ((v1+v2 > 2*minContrast) && (h1+h2 < maxFeatureWidth))
							{
								// check ratio is greater than 0.6
								if ((float)min(v1,v2)/(float)max(v1,v2) > 0.6)
								{
									int ratio = 8*(v1 + v2)/(h1+h2);
									if (ratio>255)
										ratio = 255;
									FSCHAR *textureImptr = col + minx * width + textureIm;
									if (*textureImptr < ratio)	// keep horizontal value if greater
										*textureImptr = ratio;
								}
							}

							// and start again
							max1 = max2;
							max1x = max2x;
							max2 = 0;
							min = 255;
							level = 0;
							max2x = 0;
							minx = 0;
						}
						else
							level++;
				}
			}
		}	// next row
	}	// next col

	return textureIm;
}


/////////////////////////////////////////////////////////////////////////////////////////


object* CWoolzIP::ErodeObject(object *pOriginalObject)
{
// Produces a copy of the object that has been eroded.
	object *pErodedObject = NULL;

	if (pOriginalObject)
	{
		pErodedObject = erosion(pOriginalObject);
		if (pErodedObject)
		{
			// After doing the erosion we have to copy the value table from the original object.
			// I worked this out by looking at some CytoVision code.
			pErodedObject->vdom = (struct valuetable *)copyvaluetb(pOriginalObject, pOriginalObject->vdom->type,
				                                                                    pOriginalObject->vdom->bckgrnd);
		}
	}
	
	return pErodedObject;
}



//
//	Get8BitMaskVertices
//
//	srcMaskImage - a 8 bit per pixel image.
//	image_width - image width.
//	image_height - image height.
//	adVertices - array of vertices populated by this function.
//
//	Returns TRUE on success, FALSE on failure
//
BOOL CWoolzIP::Get8BitMaskVertices( BYTE * srcMaskImage, int image_width, int image_height, CArray<double> & adVertices)
{
	BOOL bRet = FALSE;

	// Get an object from the mask
	struct object * BigObj  = (struct object *) fsconstruct( srcMaskImage, (short) image_height, (short) image_width, 128, 0);
	if (BigObj == NULL)
	{
		return FALSE;
	}

	// Break down into individual objects
	for (int k=0; k < MAX_WOOLZ_OBJECTS; k++)
	{
		if (m_pObjectList[k])
		{
			freeobj(m_pObjectList[k]);
			m_pObjectList[k] = NULL;
		}
	}

	int nObjects = 0;
    label_area( BigObj, &nObjects, m_pObjectList, MAX_WOOLZ_OBJECTS, 2);
	freeobj(BigObj);

	if (nObjects == 0)
	{
		return FALSE;
	}

	int A = -1;
	int MaxArea = -1;
	int LargestObjectIndex = 0;
    for (int j = 0; j < nObjects; j++)  
    {
        if (m_pObjectList[j])
		{
            vdom21to1(m_pObjectList[j]);
			A = area(m_pObjectList[j]);
			if (A > MaxArea)
			{
				MaxArea = A;
				LargestObjectIndex = j;
			}
		}
    }

    //get the boundary of the object 
    //for (int i = 0; i < nObjects; i++)	
	int i = LargestObjectIndex;
    {
        // if we only want the largest object ignore all others
		//if (LargestRegionOnly)
		//{
		//	if (i != LargestObject)
		//		continue;
		//}

        if (m_pObjectList[i] && m_pObjectList[i]->type == 1)  
        {
			int nvtx;
			struct ivertex  *ivtx;
            ivtx = bigbound (m_pObjectList[i], 1, &nvtx);		//get the vertex list of the boundary
            
			// Record points where change is more than delta (image coordinates)
			// Minimum distance is based on total mask area, so that for very small
			// objects the min distance is kept small and resolution is maintained.
			double mindist = area(m_pObjectList[i]) / 10000.0;
			double deltax = mindist;
			double deltay = mindist;
			double lastx, lasty;

			lastx = lasty = INT_MAX;
            if (ivtx)   
            {
                //Region *region = new Region;	//create a new region
                
                //if (nvtx > 1000)
                //    nStep = 20;
                //else if (nvtx > 100)
                //    nStep = 10;
                //else 
                //    nStep = 1;
                
				double x, y;
                for (int j = 0; j < nvtx; j++)// += nStep) 
                {
                    
                    x = (double )ivtx[j].vtX; 
                    y = (image_height - (double) ivtx[j].vtY);
                    
					if ((fabs(x - lastx) > deltax) || (fabs(y - lasty) > deltay))
					{
						// Record new last point
						lastx = x;
						lasty = y;

						// Convert from image to ideal coordinates
						//x = x * xscale + xmin;
						//y = y * yscale + ymin;

						// Create a new region vertex
						//region->AddVertex(x, y);
						adVertices.Add( x);
						adVertices.Add( y);
					}
                }

                Free (ivtx);

				bRet = TRUE;
            }
        }
    }
	
    for (i = 0; i < nObjects; i++)
    {
        if (m_pObjectList[i])
		{
            freeobj(m_pObjectList[i]);
			m_pObjectList[i] = NULL;
		}
    }

	return bRet;
}

//
//	Get1BitMaskVertices
//
//	srcMaskImage - a 1 bit per pixel image.
//	image_width - image width.
//	image_height - image height.
//	adVertices - array of vertices populated by this function.
//
//	Returns TRUE on success, FALSE on failure
//
BOOL CWoolzIP::Get1BitMaskVertices( BYTE * srcMaskImage, int image_width, int image_height, CArray<double> & adVertices)
{
	BOOL bRet = FALSE;

	// Convert 1 bit mask image to 8 bit mask image
	int nSizeOfImage = image_width * image_height;
	BYTE * maskImage = new BYTE[nSizeOfImage];

	Convert1BitImageTo8BitImage( srcMaskImage, maskImage, image_width, image_height);

	bRet = Get8BitMaskVertices( maskImage, image_width, image_height, adVertices);

	// Free temporary 8 bit mask image
	delete[] maskImage;

	return bRet;
}


//
//	Converts 1 bit images to 8 bit images.
//	Note maskImage must already exist and have been allocated memory.
//
void CWoolzIP::Convert1BitImageTo8BitImage( BYTE * oneBitImage, BYTE * eightBitImage, int image_width, int image_height)
{
	BYTE * pIn = oneBitImage;
	BYTE * pOutRow = eightBitImage;

    for (int y=0; y < image_height; y++)
    {	
        BYTE * pOut = pOutRow;
        int bitIndex = 0;

        for (int x=0; x < image_width; x++)
        {
			*pOut++ = ((*pIn << bitIndex++) & 0x80)  ?  255 : 0;

            if (bitIndex > 7)
            {
                bitIndex = 0;
                pIn++;
            }
        }

        if (bitIndex != 0) pIn++;

        pOutRow += image_width;
    }

	//FILE * fp = fopen( "D:\\EightBitMask.raw", "wb");
	//if (fp)
	//{
	//	fwrite( eightBitImage, image_width * image_height, 1, fp);
	//	fclose( fp);
	//}
}

