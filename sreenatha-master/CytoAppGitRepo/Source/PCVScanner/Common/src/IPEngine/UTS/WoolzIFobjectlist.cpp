////////////////////////////////////////////////////////////////////////////////////////
// WoolzIFobjectlist.cpp
//
// This file contains member functions of class CWoolzIP that use objects
// in the internal list CWoolzIP::m_pObjectList and associated structures.
//
// All other code related to CWoolzIP should be put in the source file WoolzIF.cpp.
// PLEASE ONLY PUT FUNCTIONS IN THIS FILE THAT USE CWoolzIP::m_pObjectList!!
//
// The code for CWoolzIP was separated into two source files as WoolzIF.cpp had
// become excessively large, plus separating the two sorts of function makes
// the source code more structured.
//
////////////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include <math.h>
#include <float.h>
#include <assert.h>

#include <CSyncObjects.h>
#include "WoolzIF.h"
#include "vcmemrect.h"
#include "vcutsfile.h"
#include "uts.h"
#include "woolz.h"
#include "CCluster.h"
#include <VECTOR2D.H>

#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define MAX(x, y) ((x) > (y) ? (x) : (y))

const double MINVALUE = 100.0 * DBL_MIN;

////////////////////////////////////////////////////////////////////////////////////////
// Woolz object stuff
////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
// Clears the current list of objects in memory
////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::FreeObjects(void)
{
    // Reset the number of objects
    m_nNumberObjects = 0;
    for (int i = 0; i < MAX_WOOLZ_OBJECTS; i++)
    {
        // If previously an object then free it
    	if (m_pObjectList[i])
        {
            freeobj(m_pObjectList[i]);
            m_pObjectList[i] = NULL;
        }
        // Reset the label anyway
        m_pLabelList[i] = 0;
    }
}
////////////////////////////////////////////////////////////////////////////////////////
//
//	FreePool() - Free object pool list
//
void CWoolzIP::FreePool()
{
	for (int i = 0; i <= m_object_pool.GetUpperBound(); i++)
		freeobj(m_object_pool[i]);
	m_object_pool.RemoveAll();
}


////////////////////////////////////////////////////////////////////////////////////////
// AddObject()
// Create a woolz object from the supplied 8-bit image and add it to the internal list.
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::AddObject(BYTE *pImage, int cols, int lines, int x, int y, int threshold)
{
// Create a Woolz object from the given data and add it to the internal list.
// Not certain this is the best way to do it - an alternative way would be
// to call makerect() followed by threshold(), see the Canvas function
// CreateIdeogramWoolzObject() for an example of this.
	BOOL status = FALSE;

	if (m_nNumberObjects < MAX_WOOLZ_OBJECTS) // Check there's room for another object in m_pObjectList.
	{
		struct object *pObj = (struct object*)fsconstruct(pImage, lines, cols, threshold, 0);
		if (pObj)
		{
			vdom21to1(pObj);

			// Move the object to the correct location, by doing what Woolz function moveobject()
			// does, but without creating a new object.
			moveidom(pObj->idom, x, y);
			movevdom(pObj->vdom, x, y);

			m_pObjectList[ m_nNumberObjects++ ] = pObj;
			
			status = TRUE;
		}
	}
	
	return status;
}


////////////////////////////////////////////////////////////////////////////////////////
// Find objects 
// Finds a series of Woolz objects and keeps a list internally. 
// Parameters:
//      *image is a pointer to an 8 bit image
//      cols is the number of columns in the image
//      lines is the number of rows in the image
//      Thresh is threhold for the image
//      MinArea is the minimum size of object to look for
// Returns:
//      The number of objects found
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::FindObjects(BYTE *image, int cols, int lines, int Thresh, int MinArea)
{
	return FindGreyObjects( image, cols, lines, ">=", Thresh, MinArea);
}

////////////////////////////////////////////////////////////////////////////////////////
// Find objects 
// Finds a series of Woolz objects and keeps a list internally using the threshold
// comparison operator.
// Parameters:
//      *image is a pointer to an 8 bit image
//      cols is the number of columns in the image
//      lines is the number of rows in the image
//		szCompare is a threshold comparison operator which can be one of "<", "<=", "=", ">=", ">"
//      Thresh is threhold for the image
//      MinArea is the minimum size of object to look for
// Returns:
//      The number of objects found
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::FindGreyObjects(BYTE *image, int cols, int lines, LPCTSTR szCompare, int Thresh, int MinArea)
{
	CLockObject Lock(pWoolzMut, 60000);

	int operatorIndex = -1;
	for (int i=0; i<WZCMPOPERATORCOUNT; i++)
	{
		if (_tcscmp( szCompare, aszWzCmpOperator[i]) == 0)
		{
			operatorIndex = i;
			break;
		}
	}

	if (operatorIndex != -1)
	{
		m_nNumberObjects = 0;
    
		// search for objects 
		struct object *bigobj;

		// Clear current list of objects
		FreeObjects();

		bigobj=(struct object *) fsconstructex(image, lines, cols, (WzCmpOperator) operatorIndex, Thresh, 0);

		label_area(bigobj, &m_nNumberObjects, m_pObjectList, MAX_WOOLZ_OBJECTS, MinArea);		
    
		freeobj(bigobj);
	}

    // Send back number of objects found
    return m_nNumberObjects;
}

////////////////////////////////////////////////////////////////////////////////////////
// Find objects Extended
// Finds a series of Woolz objects and keeps a list internally. 
// Parameters:
//      *image is a pointer to an 8 bit image
//      cols is the number of columns in the image
//      lines is the number of rows in the image
//      Thresh is threhold for the image
//      MinArea is the minimum size of object to look for
//      MaxArea is the maxumum size of object to look for
//      Border objects touching the border will be ignored
// Returns:
//      The number of objects found
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::FindObjectsEx(BYTE *image, int W, int H, int Thresh, int MinArea, int MaxArea, int BorderWidth)
{
    int     i, j;
    struct  object **TempObjects;
    int     A;
    int     N;
    struct  iwspace iwsp;
    struct  gwspace gwsp;
    long    Xmin, Xmax, Ymin, Ymax;

	CLockObject Lock(pWoolzMut, 60000);

    m_nNumberObjects = 0;
    
    // search for objects 
	struct object *bigobj;
    
    // Clear current list of objects
    FreeObjects();

	bigobj=(struct object *)fsconstruct(image, H, W, Thresh, 0);

    // Find objects >= MinArea
    label_area(bigobj, &N, m_pObjectList, MAX_WOOLZ_OBJECTS, MinArea);	

    // Go through and reject those objects that are too big
    TempObjects = (object **) new object[N];
    for (i = 0; i < N; i++)
    {
    	vdom21to1(m_pObjectList[i]);
        A = area(m_pObjectList[i]);
        // Under the max size limit
        if (A <= MaxArea)
        {
   	        Xmin = LONG_MAX;
	        Ymin = LONG_MAX;
	        Xmax = LONG_MIN;
	        Ymax = LONG_MIN;

            // Determine the bounding box for this object
            initgreyscan(m_pObjectList[i], &iwsp, &gwsp);
            while (nextgreyinterval(&iwsp) == 0) 
            {
		        if (iwsp.linpos < Ymin) Ymin = iwsp.linpos;
		        if (iwsp.linpos > Ymax) Ymax = iwsp.linpos;
		        if (iwsp.lftpos < Xmin) Xmin = iwsp.lftpos;
		        if (iwsp.rgtpos > Xmax) Xmax = iwsp.rgtpos;
            }
            
            // Keep objects not touching the border
            if ((Ymin > BorderWidth) && (Xmin > BorderWidth) && (Xmax < (W - BorderWidth)) && (Ymax < (H - BorderWidth)))
                TempObjects[i] = m_pObjectList[i];
            else
            {
                freeobj(m_pObjectList[i]);
                TempObjects[i] = NULL;
            }
        }
        else
        {
            freeobj(m_pObjectList[i]);
            TempObjects[i] = NULL;
        }
    }

    for (i = 0; i < N; i++)
        m_pObjectList[i] = NULL;

    j = 0;
    // Copy the object pointers back into the object array
    for (i = 0; i < N; i++)
    {
        if (TempObjects[i])
            m_pObjectList[j++] = TempObjects[i];
    }

    delete TempObjects;

    freeobj(bigobj);

    m_nNumberObjects = j;

    // Send back number of objects found
    return m_nNumberObjects;
}

////////////////////////////////////////////////////////////////////////////////////////
// Find objects and merge them
// Finds a series of Woolz objects and keeps a list internally. 
// Parameters:
//      *image is a pointer to an 8 bit image
//      cols is the number of columns in the image
//      lines is the number of rows in the image
//      Thresh is threhold for the image
//      AbsMin is the absolute min size of object to merge
//      MinArea is the minimum size of object to look for (after merge)
//      MaxArea is the maxumum size of object to look for
//      Border objects touching the border will be ignored
//      MD is the merge distance 
// Returns:
//      The number of objects found
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::FindObjectsAndMerge(BYTE *image, int W, int H, int Thresh, int AbsMin, int MinArea, int MaxArea, int BorderWidth, int MD)
{
    int     i, j;
    struct  object **TempObjects;
    int     N;
    struct  iwspace iwsp;
    struct  gwspace gwsp;
    long    *Xmin, *Xmax, *Ymin, *Ymax, *Area;
    BOOL    AlreadyMerged, *Merged;
    MergeObj *pM;

	CLockObject Lock(pWoolzMut, 60000);

    m_nNumberObjects = 0;
    
    // search for objects 
	struct object *bigobj;

	// Clear current list of objects
    FreeObjects();

	bigobj=(struct object *) fsconstruct(image, H, W, Thresh, 0);

    // Find objects >= MinArea
    label_area(bigobj, &N, m_pObjectList, MAX_WOOLZ_OBJECTS, AbsMin);	
    for (i = 0; i < N; i++)
    	vdom21to1(m_pObjectList[i]);

    // Go through and reject those objects that are too big
    TempObjects = (object **) new object[N];

    Xmin = new long[N];
    Xmax = new long[N];
    Ymin = new long[N];
    Ymax = new long[N];
    Area = new long[N];

    for (i = 0; i < N; i++)
    {

        Xmin[i] = LONG_MAX;
        Ymin[i] = LONG_MAX;
        Xmax[i] = LONG_MIN;
        Ymax[i] = LONG_MIN;

        // Get the area
        Area[i] = 0;

        // Determine the bounding box for this object
        initgreyscan(m_pObjectList[i], &iwsp, &gwsp);
        while (nextgreyinterval(&iwsp) == 0) 
        {
            if (iwsp.linpos < Ymin[i]) Ymin[i] = iwsp.linpos;
            if (iwsp.linpos > Ymax[i]) Ymax[i] = iwsp.linpos;
            if (iwsp.lftpos < Xmin[i]) Xmin[i] = iwsp.lftpos;
            if (iwsp.rgtpos > Xmax[i]) Xmax[i] = iwsp.rgtpos;
            Area[i] += iwsp.rgtpos - iwsp.lftpos;                   // Object area
        }

        if (Area[i] > MaxArea)                 // Out of area range stuff
        {
            freeobj(m_pObjectList[i]);
            TempObjects[i] = NULL;
        }
        else
        {
            // Keep objects not touching the border
            if ((Ymin[i] > BorderWidth) && (Xmin[i] > BorderWidth) && (Xmax[i] < (W - BorderWidth)) && (Ymax[i] < (H - BorderWidth)))
                TempObjects[i] = m_pObjectList[i];
            else
            {
                freeobj(m_pObjectList[i]);
                TempObjects[i] = NULL;
            }
        }
    }

    for (i = 0; i < N; i++)
        m_pObjectList[i] = NULL;

    j = 0;
    // Copy the object pointers back into the object array removing NULL entries
    for (i = 0; i < N; i++)
    {
        if (TempObjects[i])
        {
            Xmin[j] = Xmin[i];
            Xmax[j] = Xmax[i];
            Ymin[j] = Ymin[i];
            Ymax[j] = Ymax[i];
            Area[j] = Area[i];
            m_pObjectList[j++] = TempObjects[i];
        }
    }

    N = j;

    // Now produce a list of merge objects
    KillMergedObjects();
    Merged = new BOOL[N * N];
    for (i = 0; i < (N * N); i++) Merged[i] = FALSE;
    m_pMergeTable = new BOOL[N];
    m_pMeasuredObject = new BOOL[N];
    m_pMaskObjectCreated = new BOOL[N];
    m_pObjectGreys = new BOOL[N];

    for (i = 0; i < N; i++)
    {
        m_pMergeTable[i] = FALSE;
        m_pMeasuredObject[i] = FALSE;
        m_pMaskObjectCreated[i] = FALSE;
        m_pObjectGreys[i] = FALSE;
    }

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            // Don't use the same object and don't use objects touching the border
            if (i != j && m_pObjectList[i] && m_pObjectList[j])
            {
                // Have we already merged the objects ?
                AlreadyMerged = Merged[i * N + j] || Merged[j * N + i];
                // No
                if (!AlreadyMerged)
                {
                    // Check the merge conditions - this is based on bounding boxes overlapping or within
                    // Merge dist of one another
                    if (Xmin[i] > Xmin[j] - MD && Xmin[i] < Xmax[j] + MD && Ymin[i] > Ymin[j] - MD && Ymin[i] < Ymax[j] + MD)
                    {
                        Merged[i * N + j] = TRUE;
                        pM = new MergeObj;
                        pM->ObjI = i;
                        pM->ObjJ = j;
                        pM->Measured = FALSE;
                        pM->GMeasured = FALSE;
                        pM->MaskCreated = FALSE;
                        m_MergeObjects.AddHead(pM);
                        m_pMergeTable[i] = TRUE;
                        m_pMergeTable[j] = TRUE;
                    }
                    else
                    if (Xmin[i] > Xmin[j] - MD && Xmin[i] < Xmax[j] + MD && Ymax[i] > Ymin[j] - MD && Ymax[i] < Ymax[j] + MD)
                    {
                        Merged[i * N + j] = TRUE;
                        pM = new MergeObj;
                        pM->ObjI = i;
                        pM->ObjJ = j;
                        pM->Measured = FALSE;
                        pM->GMeasured = FALSE;
                        pM->MaskCreated = FALSE;
                        m_MergeObjects.AddHead(pM);
                        m_pMergeTable[i] = TRUE;
                        m_pMergeTable[j] = TRUE;
                    }
                    else
                    if (Xmax[i] > Xmin[j] - MD && Xmax[i] < Xmax[j] + MD && Ymin[i] > Ymin[j] - MD && Ymin[i] < Ymax[j] + MD)
                    {
                        Merged[i * N + j] = TRUE;
                        pM = new MergeObj;
                        pM->ObjI = i;
                        pM->ObjJ = j;
                        pM->Measured = FALSE;
                        pM->GMeasured = FALSE;
                        pM->MaskCreated = FALSE;
                        m_MergeObjects.AddHead(pM);
                        m_pMergeTable[i] = TRUE;
                        m_pMergeTable[j] = TRUE;
                    }
                    else
                    if (Xmax[i] > Xmin[j] - MD && Xmax[i] < Xmax[j] + MD && Ymax[i] > Ymin[j] - MD && Ymax[i] < Ymax[j] + MD)
                    {
                        Merged[i * N + j] = TRUE;
                        pM = new MergeObj;
                        pM->ObjI = i;
                        pM->ObjJ = j;
                        pM->Measured = FALSE;
                        pM->GMeasured = FALSE;
                        pM->MaskCreated = FALSE;
                        m_MergeObjects.AddHead(pM);
                        m_pMergeTable[i] = TRUE;
                        m_pMergeTable[j] = TRUE;
                    }
                }
            }
        }
    }


    // Delete objects < MinArea that are not part of a merge
    for (i = 0; i < N; i++)
    {
        if (Area[i] < MinArea && !m_pMergeTable[i])
        {
            freeobj(m_pObjectList[i]);
            m_pObjectList[i] = NULL;
        }
    }

    delete TempObjects;
    delete Xmin;
    delete Ymin;
    delete Xmax;
    delete Ymax;
    delete Area;
    delete Merged;

    freeobj(bigobj);

    m_nNumberObjects = N;

    // Send back number of objects found
    return m_nNumberObjects;
}

////////////////////////////////////////////////////////////////////////////////
//
//	Woolz Object List storage
//
//	A pool of woolz objects is maintained separate from the "found" list
//	The woolz object found list can be retained here and later restored
//
//	GetWoolzObjects()
//	Objects are transferred to the pool from the found objects list.
//	An array of objects and the number of objects transferred is returned.
//
//	SetWoolzObjects()
//	Objects specified in the array are transferred from the pool to the found objects list.
//	Only objects referenced by the array are transferred.
//	As objects are transferred, they are removed from the object pool.
//	Objects cannot exist in both the "found" list ad the pool
//
//	Returns the number of objects actually transferred.
//	and a list of the object pointers (used as object ID's) in the supplied array
//
//	Notes: The object pool array, and management variables are held in the CWoolzIP class.
//	The Get() and Set() functions are also declared in this class
//

////////////////////////////////////////////////////////////////////////////////////////
//
//	GetWoolzObjects()
//
int CWoolzIP::GetWoolzObjects(void **array)
{
	int i, pool_index, pool_size;
	WObject *obj;

	// Starting index for next pool entry
	pool_index = m_object_pool.GetUpperBound() + 1;

	for (i = 0; i < m_nNumberObjects; i++)
		TRACE("m_pObjectList[%d] = 0x%x\r\n", i, m_pObjectList[i]);

	// Set new pool size
	pool_size = pool_index + m_nNumberObjects;
	m_object_pool.SetSize(pool_size);

	// Get all objects from m_pObjectList array and copy to pool
	for (i = 0; i < m_nNumberObjects; i++, pool_index++)
	{
		obj = m_pObjectList[i];
		m_pObjectList[i] = NULL;
		m_object_pool[pool_index] = obj;
		array[i] = (void *)obj;		// Return object ID
	}

	for (i = 0; i <= m_object_pool.GetUpperBound(); i++)
		TRACE("m_object_pool[%d] = 0x%x\r\n", i, m_object_pool[i]);

	int transferred_count = m_nNumberObjects;
	m_nNumberObjects = 0;
	return transferred_count;
}

////////////////////////////////////////////////////////////////////////////////////////
//
//	SetWoolzObjects()
//
//	Note:
//	Start search from m_pool_index, which is maintained over subsequent calls to this function
//	This is because the object array is likely to be in the same order as the pool
//	This will provide the fastest search time
//
int CWoolzIP::SetWoolzObjects(void **array)
{
	int i, ii = 0;
	WObject *obj;
	int transferred_count = 0;

	// Clear out found list first
	FreeObjects();

	for (i = 0; i <= m_object_pool.GetUpperBound(); i++)
		TRACE("m_object_pool[%d] = 0x%x\r\n", i, m_object_pool[i]);

	// Move objects specified in array to found list
	int pool_size = m_object_pool.GetUpperBound() + 1;
	TRACE("pool_size = %d\r\n", pool_size);

	while (array[ii] && pool_size)
	{
		obj = m_object_pool[m_pool_index];

		if (obj == array[ii])
		{
			// Transfer and remove from pool
			TRACE("Transfer 0x%x\r\n", obj);
			m_pObjectList[ii++] = obj;
			m_object_pool.RemoveAt(m_pool_index); // NOTE: RemoveAt() shifts remaining items down
			transferred_count++;
			pool_size--;
		}
		else
			// continue to next pool item
			m_pool_index++;

		// Wrap round index at end of array
		if (m_pool_index > m_object_pool.GetUpperBound())
			m_pool_index = 0;
	}

	m_nNumberObjects = transferred_count;

	for (i = 0; i < m_nNumberObjects; i++)
		TRACE("m_pObjectList[%d] = 0x%x\r\n", i, m_pObjectList[i]);

	return transferred_count;
}


BOOL CWoolzIP::EnsureType1Object(int objectIndex)
{
	BOOL status = FALSE;

    // Check to make sure the object index is not silly.
    if (objectIndex >= 0 && objectIndex < m_nNumberObjects && objectIndex < MAX_WOOLZ_OBJECTS)
		status = EnsureType1Object(m_pObjectList[objectIndex]);
    
    return status;
}


////////////////////////////////////////////////////////////////////////////////////////
// Gets the area of a woolz object
// Parameters:
//      ObjectIndex is the index of the object to get data about
//      *WSD is the area for that object
// Returns:
//      TRUE if the object specified by index is in the list, FALSE otherwise
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::GetObjectArea(int ObjectIndex, int *WSD)
{
    BOOL Found = FALSE;

    // Check to make sure the object index is not silly
    if (ObjectIndex >= 0 && ObjectIndex < m_nNumberObjects && ObjectIndex < MAX_WOOLZ_OBJECTS)
    {
        // Now check to make sure the object is in the object list
        if (m_pObjectList[ObjectIndex])
        {
            // Do some object measurements and fill out the data structure
            *WSD = area(m_pObjectList[ObjectIndex]);

            // Return the object found
            Found = TRUE;
        }
    }

    return Found;
}

////////////////////////////////////////////////////////////////////////////////////////
// Gets the mass a woolz object (mass = sum of grey levels in the object)
// Parameters:
//      ObjectIndex is the index of the object to get data about
//      *WSD is the area for that object
// Returns:
//      TRUE if the object specified by index is in the list, FALSE otherwise
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::GetObjectMass(int ObjectIndex, int *Mass)
{
    BOOL Found = FALSE;

    // Check to make sure the object index is not silly
    if (ObjectIndex >= 0 && ObjectIndex < m_nNumberObjects && ObjectIndex < MAX_WOOLZ_OBJECTS)
    {
        // Now check to make sure the object is in the object list
        if (m_pObjectList[ObjectIndex])
        {
            // Do some object measurements and fill out the data structure
            *Mass = mass(m_pObjectList[ObjectIndex]);

            // Return the object found
            Found = TRUE;
        }
    }

    return Found;
}



////////////////////////////////////////////////////////////////////////////////////////
//	Analyse the curvature of an object and count the number of major peaks
//	If troughs=TRUE it looks for troughs instead.
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::NumberVertices(int objectIndex, BOOL troughs, int threshLevel)
{
    // Check to make sure the object index is not silly
    if (objectIndex < 0 || objectIndex >= m_nNumberObjects || objectIndex >= MAX_WOOLZ_OBJECTS)
        return 0;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[objectIndex])
        return 0;

	return number_of_features(m_pObjectList[objectIndex], troughs, threshLevel);
}

////////////////////////////////////////////////////////////////////////////////////////
// MinWidthRectangle
//      Finds the min width rectangle that fits an object
// Finds the rectangle and its orientation
// Returns TRUE if successful
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::MinWidthRectangle(int ObjectIndex, float *angle, LPPOINT V1, LPPOINT V2, LPPOINT V3, LPPOINT V4)
{
    struct object *rect, *cvh;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return FALSE;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return FALSE;

	if (m_pObjectList[ObjectIndex]->type != 1)
		return FALSE;

    // Convex hull for the object
	cvh = (struct object *)convhull(m_pObjectList[ObjectIndex]);

    // Calc the rectangle
	rect = (struct object *)minwrect(cvh);

    // Get the rectangle vertices
    V1->x = (int) ((struct frect *)rect->idom)->frk[0];
    V2->x = (int) ((struct frect *)rect->idom)->frk[1];
    V3->x = (int) ((struct frect *)rect->idom)->frk[2];
    V4->x = (int) ((struct frect *)rect->idom)->frk[3];
    V1->y = (int) ((struct frect *)rect->idom)->frl[0];
    V2->y = (int) ((struct frect *)rect->idom)->frl[1];
    V3->y = (int) ((struct frect *)rect->idom)->frl[2];
    V4->y = (int) ((struct frect *)rect->idom)->frl[3];

    // Calc orientation of the rectangle
	*angle = ((struct frect *)rect->idom)->rangle;

    // Free the temp objects
    freeobj(rect);
    freeobj(cvh);

    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////
// Find the bounding box for the object - not the same as the min width rectangle
////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::BoundingBox(int ObjectIndex, Bounds &Bounding)
{
    struct iwspace iwsp;
    struct gwspace gwsp;

	Bounding.Xmin = INT_MAX;
	Bounding.Ymin = INT_MAX;
	Bounding.Xmax = INT_MIN;
	Bounding.Ymax = INT_MIN;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

	if (!EnsureType1Object(ObjectIndex))
		return;

    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
		if (iwsp.linpos < Bounding.Ymin) Bounding.Ymin = iwsp.linpos;
		if (iwsp.linpos > Bounding.Ymax) Bounding.Ymax = iwsp.linpos;
		if (iwsp.lftpos < Bounding.Xmin) Bounding.Xmin = iwsp.lftpos;
		if (iwsp.rgtpos > Bounding.Xmax) Bounding.Xmax = iwsp.rgtpos;
    }
}

BOOL Inside(int x, int x1, int x2)
{
	return (x >= x1 && x <= x2);
}
////////////////////////////////////////////////////////////////////////////////////////
// Returns the proximity between two bounding boxes
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::BoundingProximity(Bounds &B1, Bounds &B2)
{
	int ProxMin;

	if (Inside(B1.Xmin, B2.Xmin, B2.Xmax) && Inside(B1.Ymin, B2.Ymin, B2.Ymax))
		return 0;

	if (Inside(B1.Xmax, B2.Xmin, B2.Xmax) && Inside(B1.Ymax, B2.Ymin, B2.Ymax))
		return 0;

	if (Inside(B2.Xmin, B1.Xmin, B1.Xmax) && Inside(B2.Ymin, B1.Ymin, B1.Ymax))
		return 0;

	if (Inside(B2.Xmax, B1.Xmin, B1.Xmax) && Inside(B2.Ymax, B1.Ymin, B1.Ymax))
		return 0;

	int Dx1 = abs(B1.Xmax - B2.Xmin);
	int Dy1 = abs(B1.Ymax - B2.Ymin);
	int Dx2 = abs(B2.Xmax - B1.Xmin);
	int Dy2 = abs(B2.Ymax - B1.Ymin);

	ProxMin = min(Dx1, Dx2);
	ProxMin = min(ProxMin, Dy1);
	ProxMin = min(ProxMin, Dy2);

	return ProxMin;
}

////////////////////////////////////////////////////////////////////////////////////////
// draw object values under idom into an image 
////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::DrawObject(int ObjectIndex, FSCHAR *image, int image_width, int image_height)
{
    FSCHAR *imptr;
    struct iwspace iwsp;
    struct gwspace gwsp;
    register GREY *g;
    register int k;
    FSCHAR val;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

	if (!EnsureType1Object(ObjectIndex))
		return;
    	    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	g = gwsp.grintptr;

    	imptr = (FSCHAR *) image + iwsp.linpos * image_width + iwsp.lftpos;

    	for (k=iwsp.lftpos; k<=iwsp.rgtpos; k++) 
        {
    		val = (FSCHAR)*g;
    		*imptr = val;
    		g++; 
            imptr++;
    	}
    }
}

////////////////////////////////////////////////////////////////////////////////////////
//	clear data under idom in an image for a given object with certain value 
////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::FillObjectXY(int ObjectIndex, FSCHAR *image, int x, int y, int image_width, int image_height, int value)
{
    FSCHAR *imptr;
    struct iwspace iwsp;
    struct gwspace gwsp;
    register int k;

     // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

	if (!EnsureType1Object(ObjectIndex))
		return;

    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	imptr = (FSCHAR *) image + (y + (iwsp.linpos - m_pObjectList[ObjectIndex]->idom->line1)) * image_width + (x + (iwsp.lftpos - m_pObjectList[ObjectIndex]->idom->kol1));

    	for (k = iwsp.lftpos; k <= iwsp.rgtpos; k++) 
        {
    		*imptr = value;
    		imptr++;
    	}
    }
}

void CWoolzIP::FillObject(int objectIndex, FSCHAR *image, int image_width, int image_height, int value)
{
	// Check to make sure the object index is not silly
	if (objectIndex < 0 || objectIndex >= m_nNumberObjects || objectIndex >= MAX_WOOLZ_OBJECTS)
		return;

	FillObject(m_pObjectList[objectIndex], image, image_width, image_height, value);
}


////////////////////////////////////////////////////////////////////////////////
//
//	WoolzConvexHull
//	Produce convex hull for supplied woolz object number
//	Returns convex hull as UTS mask image created prior to calling this function
//	Returns status TRUE if successful else FALSE
//
BOOL CWoolzIP::WoolzConvexHull(long MaskOut, int ObjectIndex, unsigned char fill_value)
{
	BOOL status = FALSE;
	struct object *cvh;

	// Check to make sure the object index is not silly
	if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex > MAX_WOOLZ_OBJECTS)
		return FALSE;

	// Now check to make sure the object is in the object list
	if (!m_pObjectList[ObjectIndex])
		return FALSE;

	if (m_pObjectList[ObjectIndex]->type != 1)
		return FALSE;

	// Generate convex hull for the object
	cvh = (struct object *)convhull(m_pObjectList[ObjectIndex]);

	// Conversion to UTS rect style image

	// Get polydomain of convex hull
	struct polygondomain *poly = (struct polygondomain *)cvh->idom;

	// Create solid filled object from polygon
	struct object *polyobj = polytoblob(poly);

	// Check poly object
	if ((polyobj->type != 1) ||
		(wzcheckobj(polyobj)) ||
		(wzemptyidom(polyobj->idom)))
	{
		freeobj(cvh);
		freeobj(polyobj);
		return status;
	}

	// If OK, convert to UTS image
	long GreyIm, bpp, image_width, image_height, image_pitch;
	BYTE *image_addr;

	bpp = pUTSMemRect->GetRectInfo(MaskOut, &image_width, &image_height);

	// Cope with one or 8 bpp images supplied
	if (bpp == ONE_BIT_PER_PIXEL)
		// Create an 8 bit grey level image to draw the object in
		GreyIm = pUTSMemRect->CreateAsWithPix(MaskOut, EIGHT_BITS_PER_PIXEL);
	else
		GreyIm = MaskOut;	// Use input directly

	if (GreyIm)
	{
		pUTSMemRect->GetExtendedInfo(GreyIm, &image_width, &image_height, &image_pitch, &image_addr);
		FSCHAR *imptr;
		struct iwspace iwsp;
		//struct gwspace gwsp;
		register int k;

		initrasterscan(polyobj, &iwsp, 0);
		while (nextinterval(&iwsp) == 0)
		{
			imptr = (FSCHAR *) image_addr + iwsp.linpos * image_width + iwsp.lftpos;
			for (k = iwsp.lftpos; k <= iwsp.rgtpos; k++) 
			{
				*imptr = fill_value;
				imptr++;
			}
		}

		// If one bpp image suplied, convert generated mask
		if (bpp == ONE_BIT_PER_PIXEL)
		{
			pUTSMemRect->Convert8To1(GreyIm, MaskOut);

			// Free off GreyIm
			pUTSMemRect->FreeImage(GreyIm);
		}

		status = TRUE;
	}

	// Free the temp objects
	freeobj(cvh);
	freeobj(polyobj);

	return status;
}



////////////////////////////////////////////////////////////////////////////////
//
//	WoolzClusterObjects
//	Cluster objects in the (internal) woolz object list
//	Objects that are more than thresh_cluster_dist apart are considered to be distinct
//
//	1. Generate list of objects (CClusterList)
//	2. Each object has image object representation attached (m_ext_data pointer)
//	3. Using Divisive clustering technique with a threshold distance identify the cluster seed points
//	4. Use hierarchical clustering to assign objects to clusters (min dist agglomerative clustering).
//	5. Possibly repeat by re-evaluating cluster seed point as CofG of clusters (K-Means clustering)
//
//	Uses CClusterObj and CClusterList classes as defined in CCluster.cpp/.h
//	Returns status TRUE if successful else FALSE
//
long CWoolzIP::WoolzClusterObjects(long thresh_cluster_dist)
{
	long status = 0;

	// Check to make sure the object list is not empty
	if (m_nNumberObjects == 0)
		return status;

	// Need two lists the original object list, which will become the cluster list
	// and a temporary eliminated objects list. -- Here they are:
	CClusterList *cluster_list = new CClusterList;
	CClusterList *eliminated_list = new CClusterList;

	// Now construct the list, based on the list of Woolz objects
	// Note that the lists should start out empty, since they were just constructed
	for (int ii = 0; ii < m_nNumberObjects; ii++)
	{
		// Get object from object list
		WObject *wobj = m_pObjectList[ii];

		// Get it's position (Use COG)
		WoolzMeasurements meas;
		if (MeasureObject(ii, &meas))
		{
			int x = int(meas.COGx);
			int y = int(meas.COGy);

			// Create a cluster object and place in list
			cluster_list->AddObject(x, y, wobj);
		}
	}

	// Having constructed the list, we pare it down until it consists
	// of objects with a minimum separation of thresh_cluster_dist
	int next_object;
	do
	{
		// Recalculate list of neighbours
		POSITION pos_obj = cluster_list->m_obj_list.GetHeadPosition();

		while (pos_obj)
		{
			CClusterObj *obj;

			obj = cluster_list->m_obj_list.GetAt(pos_obj);
			obj->RemoveNeighbours();
			obj->FindNeighbours(cluster_list, thresh_cluster_dist);
			cluster_list->m_obj_list.GetNext(pos_obj);
		}

		// Now find next object to remove
		int max_neighbours;
		CClusterObj *obj;

		// Step 1 -- Find object with most neighbours - note there may be more than one
		max_neighbours = cluster_list->MaxNeighbours();

		// Step 2 -- Scan list again, looking for smallest total distance for maximum number neighbours
		obj = cluster_list->MinTotalDistance(max_neighbours);

		// Record object ID
		if (obj)
			next_object = obj->m_id;
		else
			next_object = 0;

		// Remove specified object ID
		if (next_object > 0)
		{
			CClusterObj *obj;

			// Remove object from cluster list
			obj = cluster_list->RemoveObject(next_object);

			// .. and cluster neighbours
			cluster_list->RemoveNeighbourObject(next_object);

			// Add this obj to eliminated list after first removing the neighbour list
			if (obj)
			{
				obj->RemoveNeighbours();
				eliminated_list->AddObject(obj);
			}
		}
	} while(next_object);

	// Remove neighbours from cluster list
	// as each object will still have itself as a neighbour
	cluster_list->RemoveObjectNeighbours();

	// Create new cluster list with new objects at the top level
	// and the detected seed objects in the neighbour list
	CClusterObj *obj;
	CClusterList *new_list = new CClusterList;
	POSITION pos, obj_pos;
	
	new_list->m_next_object_id = cluster_list->m_next_object_id;
	pos = cluster_list->m_obj_list.GetHeadPosition();
	while (pos)
	{
		CClusterObj *new_obj;

		obj = cluster_list->m_obj_list.GetAt(pos);

		// Keep object position
		obj_pos = pos;


		// Move to next object
		cluster_list->m_obj_list.GetNext(pos);
		
		// Remove cluster seed object from cluster list
		cluster_list->m_obj_list.RemoveAt(obj_pos);

		// Add new object to cluster list
		// Note: This is a place-holder to denote the cluster position
		// hence we just create a new obj with same position as seed
		// but we don't need the ext_data pointer
		new_obj = new_list->AddObject(obj->m_x, obj->m_y);

		// Add cluster seed object as neighbour to new object
		new_obj->AddNeighbour(obj);
	}
	
	// Swap new list for old
	delete cluster_list;
	cluster_list = new_list;

	// Having got a list of object seed points, we now re-cluster the eliminated list to this.
	POSITION cpos, min_obj_pos;
	CClusterObj *cobj, *min_obj, *min_cluster;
	//int n_neighbours;
	int min_neighbours = INT_MAX;
	int min_obj_id = 0;
	double dist, min_dist;

	// For each object in the eliminated list
	pos = eliminated_list->m_obj_list.GetHeadPosition();
	while (pos)
	{
		obj = eliminated_list->m_obj_list.GetAt(pos);

		// Go through cluster list
		min_dist = (double)thresh_cluster_dist;	// Initialise min dist as threshold dist
		min_obj = NULL;
		cpos = cluster_list->m_obj_list.GetHeadPosition();

		while (cpos)
		{
			cobj = cluster_list->m_obj_list.GetAt(cpos);

			// Find distance of nearest cluster seed
			dist = cobj->Distance(obj);

			if (dist < min_dist)
			{
				min_dist = dist;
				min_obj = obj;
				min_obj_pos = pos;
				min_cluster = cobj;
			}
			
			cluster_list->m_obj_list.GetNext(cpos);
		}

		eliminated_list->m_obj_list.GetNext(pos);

		if (min_obj)
		{
			// Add object to cluster with minimum distance
			min_cluster->AddNeighbour(min_obj);

			// Remove object from eliminated list
			eliminated_list->m_obj_list.RemoveAt(min_obj_pos);
		}
	}

	// Now the cluster list contains a list of object clusters
	// We have to combine all the neighbour objects into a single woolz object
	// using "join" (unionn)
	// We accumulate the list of joined objects in join_list which will then replace
	// the original list of woolz objects

	// Declare and find the number of clusters
	int nclusters = cluster_list->m_obj_list.GetCount();

	// Create the joined objects list
	WObject **join_list = new WObject * [nclusters];

	// Declare and initialise an index into this list
	int join_index = 0;

	// For every object cluster
	pos = cluster_list->m_obj_list.GetHeadPosition();
	while (pos)
	{
		//CClusterObj *new_obj;

		obj = cluster_list->m_obj_list.GetAt(pos);

		// Get all neighbour objects and join them up
		// First generate an array of the objects
		int num_nobjs = obj->m_neighbours->m_obj_list.GetCount();
		WObject **wobj_list = new WObject* [num_nobjs];
		//WObject *join;

		CClusterObj *nobj;
		int nn;

		for (nobj = obj->m_neighbours->GetFirstObject(), nn = 0; nobj; nobj = obj->m_neighbours->GetNextObject(), nn++)
		{
			wobj_list[nn] = (WObject *)(nobj->m_ext_data);
		}

		// Perform join operation
		// NOTE: Call with uvt == 1 (union value table), i.e. construct a new grey table, which we want
		join_list[join_index++] = unionn(wobj_list, num_nobjs, 1);

		// Delete the temporary object list
		delete wobj_list;

		// Move to next object
		cluster_list->m_obj_list.GetNext(pos);
	}

	// Clean out the cluster lists
	delete cluster_list;
	delete eliminated_list;

	// Now replace the original list of woolz objects with the new joined list
	
	// Delete the original objects
	for (int ii = 0; ii < m_nNumberObjects; ii++)
	{
		//freeobj(m_pObjectList[ii]);
		m_pObjectList[ii] = NULL;
	}

	// Now reassign the joined objects
	for (int ii = 0; ii < nclusters; ii++)
	{
		m_pObjectList[ii] = join_list[ii];
	}

	// Reassign the new number of objects in the list
	m_nNumberObjects = nclusters;
	
	// Finally, delete the join list and tidy up
	delete join_list;

	return nclusters;
}

////////////////////////////////////////////////////////////////////////////////////////
// CreateMask
//      Create a UTS mask from a woolz object
// Parameters:
//      ObjectIndex the object found using WoolzFindObjects
//      MaskOut is a UTS created mask, created before the call to this function
// Returns:
//      TRUE if successful
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::CreateMask(int ObjectIndex, long MaskOut)
{
    long GreyIm, BPP, W, H, P;
    BYTE *Addr;
    BOOL Ok = FALSE;

    BPP = pUTSMemRect->GetRectInfo(MaskOut, &W, &H);
    if (BPP == ONE_BIT_PER_PIXEL)
    {
        // Create an 8 bit grey level image to draw the obejct in
        GreyIm = pUTSMemRect->CreateAsWithPix(MaskOut, EIGHT_BITS_PER_PIXEL);
        pUTSMemRect->GetExtendedInfo(GreyIm, &W, &H, &P, &Addr);
        if (GreyIm > 0)
        {
            // Colour it in the max intensity
            FillObject(ObjectIndex, Addr, P, H, NBINS_EIGHT - 1);
            // Now convert this to a UTS mask object
            //pUTSMemRect->Convert(GreyIm, MaskOut, 1);
            pUTSMemRect->Convert8To1(GreyIm, MaskOut);
            // Free off GreyIm
            pUTSMemRect->FreeImage(GreyIm);
            Ok = TRUE;
        }
    }
    return Ok;
}

////////////////////////////////////////////////////////////////////////////////////////
// CreateMaskFast
//      Create an 8 bit binary mask from a woolz object
// Parameters:
//      ObjectIndex the object found using WoolzFindObjects
//      MaskOut is a UTS created 8 bit image, created before the call to this function
// Returns:
//      TRUE if successful
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::CreateMaskFast(int ObjectIndex, long BinIm)
{
    long BPP, W, H, P;
    BYTE *Addr;
    BOOL Ok = FALSE;

    // Create an 8 bit grey level image to draw the obejct in
    BPP = pUTSMemRect->GetExtendedInfo(BinIm, &W, &H, &P, &Addr);
    if (UTSVALID_HANDLE(BinIm) && BPP == EIGHT_BITS_PER_PIXEL)
    {
        // Colour it in the max intensity
        FillObject(ObjectIndex, Addr, P, H, NBINS_EIGHT - 1);
        Ok = TRUE;
    }

    return Ok;
}

////////////////////////////////////////////////////////////////////////////////////////
// CreateMaskFromMerged
//      Create an 8 bit binary mask from a merged woolz object
// Parameters:
//      ObjectIndex the object found using WoolzFindObjects
//      Image is a UTS created 8 bit image, created before the call to this function
// Returns:
//      TRUE if successful
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::CreateMaskFromMerged(int ObjectIndex, long Pitch, long H, BYTE *Image)
{
    BOOL Ok = FALSE;
    POSITION P;
    MergeObj *pM;

    // Only fill the object if we have not already done so
    if (!m_pMaskObjectCreated[ObjectIndex])
    {
        P = m_MergeObjects.GetHeadPosition();
        while (P)
        {
            pM = m_MergeObjects.GetNext(P);
            if (!pM->MaskCreated)
            {
                if (pM->ObjI == ObjectIndex)
                {
                    pM->MaskCreated = TRUE;
                    CreateMaskFromMerged(pM->ObjJ, Pitch, H, Image);
                }
                else
                if (pM->ObjJ == ObjectIndex)
                {
                    pM->MaskCreated = TRUE;
                    CreateMaskFromMerged(pM->ObjI, Pitch, H, Image);
                }
            }
        }
        // Colour it in the max intensity
        FillObject(ObjectIndex, Image, Pitch, H, NBINS_EIGHT - 1);
        m_pMaskObjectCreated[ObjectIndex] = TRUE;                   // Don't fill this object again
        Ok = TRUE;
    }

    return Ok;
}

////////////////////////////////////////////////////////////////////////////////////////
// CreateMaskFromAllObjects
//      Create a UTS mask from a woolz object
// Parameters:
//      MaskOut is a UTS created mask, created before the call to this function
// Returns:
//      TRUE if successful
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::CreateMaskFromAllObjects(long MaskOut)
{
    long GreyIm, BPP, W, H, P;
    BYTE *Addr;
    BOOL Ok = FALSE;
    int  i;

    BPP = pUTSMemRect->GetRectInfo(MaskOut, &W, &H);
    if (BPP == ONE_BIT_PER_PIXEL)
    {
        // Create an 8 bit grey level image to draw the obejct in
        GreyIm = pUTSMemRect->CreateAsWithPix(MaskOut, EIGHT_BITS_PER_PIXEL);
        pUTSMemRect->GetExtendedInfo(GreyIm, &W, &H, &P, &Addr);
        if (GreyIm > 0)
        {
            for (i = 0; i < m_nNumberObjects; i++)
            {
                // Colour it in the max intensity
                FillObject(i, Addr, P, H, NBINS_EIGHT - 1);
            }
            // Now convert this to a UTS mask object
            pUTSMemRect->Convert(GreyIm, MaskOut, 1);
            pUTSMemRect->FreeImage(GreyIm);
            Ok = TRUE;
        }
    }
    return Ok;
}

////////////////////////////////////////////////////////////////////////////////////////
// CreateMaskFromAllObjects
//      Create a UTS mask from a woolz object
// Parameters:
//      MaskOut is a UTS created mask, created before the call to this function
// Returns:
//      TRUE if successful
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::CreateMaskFromLabelledObjects(long Label, long MaskOut)
{
    long GreyIm, BPP, W, H, P;
    BYTE *Addr;
    BOOL Ok = FALSE;
    int  i;

    BPP = pUTSMemRect->GetRectInfo(MaskOut, &W, &H);
    if (BPP == ONE_BIT_PER_PIXEL)
    {
        // Create an 8 bit grey level image to draw the obejct in
        GreyIm = pUTSMemRect->CreateAsWithPix(MaskOut, EIGHT_BITS_PER_PIXEL);
        pUTSMemRect->GetExtendedInfo(GreyIm, &W, &H, &P, &Addr);
        if (GreyIm > 0)
        {
            // Cycle through all found objects
            for (i = 0; i < m_nNumberObjects; i++)
            {
                // Only use labelled objects that match this label
                if (m_pLabelList[i] == Label)   // Colour it in the max intensity
                    FillObject(i, Addr, P, H, NBINS_EIGHT - 1);
            }
            // Now convert this to a UTS mask object
            pUTSMemRect->Convert(GreyIm, MaskOut, 1);
            pUTSMemRect->FreeImage(GreyIm);
            Ok = TRUE;
        }
    }
    return Ok;
}

////////////////////////////////////////////////////////////////////////////////////////
// CreateMaskFromErodedLabelledObjects
//      Create a UTS mask from eroded versions of labelled woolz objects,
//      without modifying the original objects
// Parameters:
//      hMask is a UTS created mask, created before the call to this function,
//      that receives the output result
////////////////////////////////////////////////////////////////////////////////////////
void CWoolzIP::CreateMaskFromErodedLabelledObjects(long label, long hMask)
{
	long w, h, p;
	long bpp = pUTSMemRect->GetRectInfo(hMask, &w, &h);
	if (bpp == ONE_BIT_PER_PIXEL)
	{
		// Create an 8 bit grey level image to draw the obejct in
		long hGrey = pUTSMemRect->CreateAsWithPix(hMask, EIGHT_BITS_PER_PIXEL);
		BYTE *pAddr;
		pUTSMemRect->GetExtendedInfo(hGrey, &w, &h, &p, &pAddr);
		if (hGrey > 0)
		{
			// Cycle through all found objects
			for (int i = 0; i < m_nNumberObjects; i++)
			{
				// Only use labelled objects that match the specfied label
				if (m_pLabelList[i] == label && m_pObjectList[i])   // Colour it in the max intensity
				{
					object *pErodedObject = ErodeObject(m_pObjectList[i]);
				
					FillObject(pErodedObject, pAddr, p, h, NBINS_EIGHT - 1);
					
					freeobj(pErodedObject);
				}
			}
			
			// Now convert this to a UTS mask object
			pUTSMemRect->Convert(hGrey, hMask, 1);
			pUTSMemRect->FreeImage(hGrey);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////
// LabelObjectsIntersectingMask
//      Label objects in the internal list that intersect with the given mask
// Parameters:
//      hMask is a UTS created mask, label is a label number
////////////////////////////////////////////////////////////////////////////////////////
void CWoolzIP::LabelObjectsIntersectingMask(long hMask, long label)
{
	// Create an 8 bit grey level equivalent of hMask
	long hGrey = pUTSMemRect->CreateAsWithPix(hMask, EIGHT_BITS_PER_PIXEL);

	// Convert the mask image to an 8 bit one
	pUTSMemRect->Convert(hMask, hGrey, 255);

	// Now unpad it for use with Woolz
	UnpadImage(hGrey);

	// Get the address of the image data 
	long w, h, p;
	BYTE *pAddr;	
	pUTSMemRect->GetExtendedInfo(hGrey, &w, &h, &p, &pAddr);
    
	// Search for objects
	struct object *maskObjList[MAX_WOOLZ_OBJECTS];
	//for (int i = 0; i < MAX_WOOLZ_OBJECTS; i++)
	//	maskObjList[i] = NULL;

	// Create a type 21 grey-level object from the mask.
	struct object *pBigObj = fsconstruct(pAddr, (short)h, (short)w, 1, 0);
	
	// Extract connected objects from large object and convert to type 1.
	int n;
	label_area(pBigObj, &n, maskObjList, MAX_WOOLZ_OBJECTS, 1);      // Min area of one						
	for (int i = 0; i < n; i++)
		vdom21to1(maskObjList[i]);
		
		
	// Now iterate through the internal object list and label objects
	// that intersect with any of the objects derived from the mask.
	for (int j = 0; j < m_nNumberObjects; j++)
	{
		for (int i = 0; i < n; i++)
		{
			struct object *pIntersectionObject = intersect2(maskObjList[i], m_pObjectList[j]);
			if (pIntersectionObject)
			{
				BOOL intersected = FALSE;
				
				if (area(pIntersectionObject) > 0)
				{
					m_pLabelList[j] = label;
					intersected = TRUE;
				}	
			
				freeobj(pIntersectionObject);
				
				if (intersected)
					break; // Don't need to check this object for intersection with any other mask objects. 				
			}
		}
	}


	// Tidy up
	for (int i = 0; i < n; i++)
		freeobj(maskObjList[i]);
		
	freeobj(pBigObj);
	pUTSMemRect->FreeImage(hGrey);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Calculate grey level data about a woolz object
// GreyData returns an array of grey level values
// GreyData[0] = Min Grey Level
// GreyData[1] = Max Grey Level
// GreyData[2] = Mean Grey Level
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
void CWoolzIP::ObjectGreyData(int ObjectIndex, long *MinGrey, long *MaxGrey, long *MeanGrey)
{
    struct      iwspace iwsp;
    struct      gwspace gwsp;
    register    GREY *g;
    register    int k;
    BYTE        val;
    long        n = 0, total = 0;
    BYTE        Max, Min;

    Max = 0;
    Min = (BYTE) NBINS_EIGHT - 1;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

	if (!EnsureType1Object(ObjectIndex))
		return;
    	    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	g = gwsp.grintptr;

    	for (k = iwsp.lftpos; k<=iwsp.rgtpos; k++) 
        {
    		val = (BYTE) *g;
            if (val > Max) Max = val;
            else
            if (val < Min) Min = val;
            total += val;
    		g++; 
            n++;
    	}
    }

    *MaxGrey = Max;
    *MinGrey = Min;
    if (n)
        *MeanGrey = total / n;
    else
        *MeanGrey = 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Get position of the point with the largest grey value in the object 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CWoolzIP::MaxGreyPositionInImage(int ObjectIndex, BYTE *pImage, long W, long H, long * pX, long * pY, long * pMaxGrey)
{
    struct	iwspace iwsp;
    struct	gwspace gwsp;
    int		k;
    BYTE	val;
	BYTE	*imptr;

    BYTE max = 0;
	WZCOORD	x = -1;
	WZCOORD	y = -1;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

	if (!EnsureType1Object(ObjectIndex))
		return;
    	    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	imptr = pImage + iwsp.linpos * W + iwsp.lftpos;

    	for (k = iwsp.lftpos; k<=iwsp.rgtpos; k++) 
        {
    		val = *imptr;
            if (val > max)
			{
				max = val;
				x = k;
				y = iwsp.linpos;
			}

			imptr++; 
    	}
    }

	*pMaxGrey = max;
	*pX = x;
	*pY = y;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Find the local maxima in the image using a 3x3 neighbourhood.
// ASSUMPTION - none of the objects touch the edges of the image.
// If any objects touch the edges of the image there may be an index out of range error or incorrect maxima output
// as the neighbourhood operation doesnt check array indicies (for speed).
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CWoolzIP::LocalMaximaInImage(int ObjectIndex, BYTE *pImage, long W, long H, CArray<WoolzPoint> * pMaxima)
{
    struct	iwspace iwsp;
    struct	gwspace gwsp;
    int		i, k;
    BYTE	val;
	BYTE	*imptr;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

	if (!EnsureType1Object(ObjectIndex))
		return;

	// Prepare neighbourhood offsets
	int offsets[8] = {	-1-W, -W, -W+1,
						-1,         +1,
						-1+W, +W, +W+1 };
	int count;
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	imptr = pImage + iwsp.linpos * W + iwsp.lftpos;

    	for (k = iwsp.lftpos; k<=iwsp.rgtpos; k++) 
        {
    		val = *imptr;

			count=0;
			for (i=0; i<8 && imptr[offsets[i]]<=val; i++)
			{
				count++;
			}

			if (count == 8)
			{
				pMaxima->Add( WoolzPoint( k, iwsp.linpos, val));
			}

			imptr++; 
    	}
    }

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Find the end points of the object in the binary image, defined by looking for points with 2 non zero items in 
// a 3x3 neighbourhood.
// ASSUMPTION - none of the objects touch the edges of the image.
// If any objects touch the edges of the image there may be an index out of range error or incorrect output
// as the neighbourhood operation doesnt check array indicies (for speed).
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CWoolzIP::EndPointsInImage( int ObjectIndex, BYTE *pImage, long W, long H, CArray<WoolzPoint> * pEndPoints)
{
    struct	iwspace iwsp;
    struct	gwspace gwsp;
    int		i, k;
	BYTE	*imptr;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

	if (!EnsureType1Object(ObjectIndex))
		return;

	// Prepare neighbourhood offsets
	int offsets[9] = {	-1-W, -W, -W+1,
						-1,    0,   +1,
						-1+W, +W, +W+1 };
	int count;
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	imptr = pImage + iwsp.linpos * W + iwsp.lftpos;

    	for (k = iwsp.lftpos; k<=iwsp.rgtpos; k++) 
        {
			count=0;
			for (i=0; i<9; i++)
			{
				if (imptr[offsets[i]] > 0)
				{
					count++;
				}
			}

			if (count == 2)
			{
				pEndPoints->Add( WoolzPoint( k, iwsp.linpos, *imptr));
			}

			imptr++; 
    	}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Calculate grey level stats about a woolz object
//
// skewness - see http://en.wikipedia.org/wiki/Skewness.
// Skew computed using Pearson skewness coefficient = (mean - mode) / standard deviation, for performance
// as it avoids computing cubes.
// Skew output value is 100*Pearson skewness coefficient so we can output 2dp.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CWoolzIP::ObjectGreyDataStats(int ObjectIndex, long *Min, long *Max, long *Mean, long *Median, long *Mode, long *ModeCount, long *Variance, long *Skew)
{
	if (!EnsureType1Object(ObjectIndex))
		return;

	int i;
	long histo[NBINS_EIGHT];

	// Reset histogram
	for (i=0; i<NBINS_EIGHT; i++)
	{
		histo[i] = 0;
	}

	// Compute histogram
    struct      iwspace iwsp;
    struct      gwspace gwsp;
    register    GREY *g;
    register    int k;
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	g = gwsp.grintptr;

    	for (k = iwsp.lftpos; k<=iwsp.rgtpos; k++) 
        {
			histo[*g]++;
    		g++; 
    	}
    }

	// Compute intermediate values from histogram
	long nCount = 0;
	long nTotalCount = 0;
	long nSum = 0;
	long nSumSq = 0;		
	//double dSumCube = 0;
	BYTE nMax = 0;
	BYTE nMin = (BYTE) NBINS_EIGHT - 1;
	BYTE nMode = 0;
	long nMaxCount = 0;
	for (i=0; i<NBINS_EIGHT; i++)
	{
		nCount = histo[i];
		if (nCount > 0)
		{
			nTotalCount += nCount;
			nSum += i * nCount;
			nSumSq += i * i * nCount;
			//dSumCube += i * i * i * nCount;

			if (nCount > nMaxCount)
			{
				nMode = i;
				nMaxCount = nCount;
			}
		}
	}

	// Compute stats from intermediates
	if (nTotalCount > 0)
	{
		double dMean = (double) nSum / nTotalCount;

		double A = ((double) nSumSq) / nTotalCount;
		double B = ((double) nSum * nSum) / (nTotalCount * nTotalCount);
		double dVariance = A - B;

		double dSkew = 0.0;
		if (dVariance > MINVALUE)
		{
			dSkew = (dMean - nMode) / sqrt( dVariance);
		}

		*Min = nMin;
		*Max = nMax;
		*Mean = (long) (dMean + 0.5);
		*Median = 0;
		*Mode = nMode;
		*ModeCount = nMaxCount;
		*Variance = (long) (dVariance + 0.5);
		*Skew = (long) ((100.0 * dSkew) + 0.5);
	}
	else
	{
		*Min = nMin;
		*Max = nMax;
		*Mean = 0;
		*Median = 0;
		*Mode = 0;
		*Variance = 0;
		*Skew = 0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Calculate grey level data about a woolz object - for any UTS image
// GreyData returns an array of grey level values
// GreyData[0] = Min Grey Level
// GreyData[1] = Max Grey Level
// GreyData[2] = Mean Grey Level
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
void CWoolzIP::ObjectGreyDataInImage(int ObjectIndex, BYTE *image, long W, long H, long *MinGrey, long *MaxGrey, long *MeanGrey)
{
    struct      iwspace iwsp;
    struct      gwspace gwsp;
    register    int k;
    BYTE        val;
    long        n = 0, total = 0;
    BYTE        Max, Min;
    BYTE        *imptr;

    Max = 0;
    Min = (BYTE) NBINS_EIGHT - 1;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

	if (!EnsureType1Object(ObjectIndex))
		return;
    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	imptr = image + iwsp.linpos * W + iwsp.lftpos;

    	for (k = iwsp.lftpos; k <= iwsp.rgtpos; k++) 
        {
    		val = (BYTE) *imptr++;
    		
            if (val > Max) 
                Max = val;
            else if (val < Min)
                Min = val;
                
            total += val;
            n++;
    	}
    }

    *MaxGrey = Max;
    *MinGrey = Min;
    
    if (n)
        *MeanGrey = total / n;
    else
        *MeanGrey = 0;

/*
	long long sumOfDeviationSquareds = 0;
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	imptr = image + iwsp.linpos * W + iwsp.lftpos;

    	for (k = iwsp.lftpos; k <= iwsp.rgtpos; k++) 
        {
    		val = (BYTE) *imptr++;
    		
			sumOfDeviationSquareds += (val - *MeanGrey) * (val - *MeanGrey);
    	}
    }

    *MeanGrey = long(sumOfDeviationSquareds / n);
*/
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Calculate grey level data about a woolz object - for any UTS image
// GreyData returns an array of grey level values
// GreyData[0] = Min Grey Level
// GreyData[1] = Max Grey Level
// GreyData[2] = Mean Grey Level
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
void CWoolzIP::MergedObjectGreyDataInImage(int ObjectIndex, BYTE *image, long W, long H, long *MinGrey, long *MaxGrey, long *MeanGrey)
{
    struct      iwspace iwsp;
    struct      gwspace gwsp;
    register    int k;
    BYTE        val;
    long        n = 0, total = 0, TotObj = 0;
    BYTE        Max, Min;
    BYTE        *imptr, *imend;
    POSITION    P;
    MergeObj    *pM;
    long        TempMean, TempMax, TempMin;
    long        TotalMean, TotalMax, TotalMin;

    TotalMean = 0;
    TotalMax = 0;
    TotalMin = 0;

    Max = 0;
    Min = (BYTE) NBINS_EIGHT - 1;

    P = m_MergeObjects.GetHeadPosition();
    while (P)
    {
        pM = m_MergeObjects.GetNext(P);
        if (!m_pObjectGreys[ObjectIndex])
        {
            if (pM->ObjI == ObjectIndex && !pM->GMeasured)
            {
                pM->GMeasured = TRUE;
                MergedObjectGreyDataInImage(pM->ObjJ, image, W, H, &TempMin, &TempMax, &TempMean);
                TotalMin += TempMin;
                TotalMax += TempMax;
                TotalMean += TempMean;
                TotObj++;
                m_pObjectGreys[pM->ObjJ] = TRUE;
            }
            else
            if (pM->ObjJ == ObjectIndex && !pM->GMeasured)
            {
                pM->GMeasured = TRUE;
                MergedObjectGreyDataInImage(pM->ObjI, image, W, H, &TempMin, &TempMax, &TempMean);
                TotalMin += TempMin;
                TotalMax += TempMax;
                TotalMean += TempMean;
                TotObj++;
                m_pObjectGreys[pM->ObjI] = TRUE;
            }
        }
    }
    	    	
    imend = image + W * (H - 1);
    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
    while (nextgreyinterval(&iwsp) == 0) 
    {
    	imptr = image + iwsp.linpos * W + iwsp.lftpos;

    	for (k = iwsp.lftpos; k<=iwsp.rgtpos; k++) 
        {
    		val = (BYTE) *imptr++;
            if (val > Max) 
                Max = val;
            else
            if (val < Min) 
                Min = val;
            total += val;
            n++;
    	}
    }

    if (TotObj)
    {
        TotalMin = (Min + TotalMin / TotObj) / 2;
        TotalMax = (Max + TotalMax / TotObj) / 2;
        
        *MaxGrey = TotalMax;
        *MinGrey = TotalMin;
        if (n)
            *MeanGrey = ((total / n) + TotalMean / TotObj) / 2;
        else
            *MeanGrey = TotalMean / TotObj;
    }
    else
    {
        *MaxGrey = Max;
        *MinGrey = Min;
        if (n)
            *MeanGrey = total / n;
        else
            *MeanGrey = 0;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Label an object
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::LabelObject(int ObjectIndex, long LabelValue)
{
    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return;

    m_pLabelList[ObjectIndex] = LabelValue;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Get the label for an object, returns 0 if invalid for some reason
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

long CWoolzIP::GetObjectLabel(int ObjectIndex)
{
    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return 0;

    return m_pLabelList[ObjectIndex];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Count separately labelled objects within the object list
// i.e. if two objects are labelled with "1" and object is labelled with "2" then the 
// total count will be 2 as two obejcts have the same label
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

long CWoolzIP::CountLabelledObjects(void)
{
    int i, j;
    long Total = 0;
    long InList[MAX_WOOLZ_OBJECTS];         // Keep a list of all labels counted so far
    BOOL AlreadyThere;

    // Clear the list of counted labels
    for (i = 0; i < MAX_WOOLZ_OBJECTS; i++) InList[i] = 0;

    // For all objects in the list
    for (i = 0; i < m_nNumberObjects; i++)
    {
        // Is this a valid object
        if (m_pObjectList[i])
        {
            AlreadyThere = FALSE;
            // Check to make sure the label has not already been counted
            for (j = 0; j < m_nNumberObjects; j++)
                if (InList[j] == m_pLabelList[i])
                {
                    AlreadyThere = TRUE;
                    break;
                }

            // Not already in our list so add to the list and inc the total number of objects
            if (!AlreadyThere)
            {
                for (j = 0; j < m_nNumberObjects; j++)
                {
                    // Found a blank place in the list so add it and quit the loop
                    if (InList[j] == 0)
                    {
                        InList[j] = m_pLabelList[i];
                        Total++;                        // Inc the total
                        break;
                    }
                }
            }
        }
    }

    return Total;
}

void CWoolzIP::GetPoint(int ObjectIndex, long *X, long *Y)
{
    struct      iwspace iwsp;
    struct      gwspace gwsp;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

	if (!EnsureType1Object(ObjectIndex))
		return;
    	    	
    initgreyscan(m_pObjectList[ObjectIndex], &iwsp, &gwsp);
	nextgreyinterval(&iwsp);

    *X = iwsp.lftpos; 
	*Y = iwsp.linpos;
}



////////////////////////////////////////////////////////////////////////////////////////
// Do a selection of measurements on an object found using the find objects above
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::MeasureObject(int ObjectIndex, WoolzMeasurements *WM, int measurementSet/*=1*/)
{
	struct ivertex *vtx, *wtx;
	int i;
	long dx, dy;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return FALSE;

	if (!EnsureType1Object(ObjectIndex))
		return FALSE;
         
	// Convert the object to a boundary with wrap to beginning
	int N;
	struct ivertex *ivtx = (ivertex *)bigbound(m_pObjectList[ObjectIndex], 1, &N);
	if (ivtx == NULL)
		return FALSE;

	// Convert to a unit boundary.
	int NVertices;
	struct ivertex *UnitObj = (ivertex *)unitbigbound((ivertex *)ivtx, N, &NVertices);
	if (UnitObj == NULL) 
    {
		Free(ivtx);
		return FALSE;
	}


	if (measurementSet == 2)
	{
		// Smooth the polygon to get a more accurate perimeter measurement
		// and so a more accurate circularity measurement.
		struct polygondomain *pPoly = makepolydmn(1, (WZCOORD*)UnitObj, NVertices, NVertices, 0);
		polysmooth(pPoly, 3);
		
		// Make sure that the last vertex equals the first vertex, or else the polygon will not be closed.
		pPoly->vtx[NVertices - 1] = pPoly->vtx[0];
		
		freepolydmn(pPoly); // Free polygon domain, but not vertex list.
	}


    // Actual area of object
    WM->Area = area(m_pObjectList[ObjectIndex]);

	// check for closed polygon.
	// should really check vertex adjacency,
	// but this would take a long time.	
	vtx = UnitObj + NVertices - 1;
	wtx = UnitObj;
	if (vtx->vtX != wtx->vtX || vtx->vtY != wtx->vtY) 
	{
		TRACE1("OBJECT %ld NOT A CLOSED POLYGON\r\n", ObjectIndex);
		Free(UnitObj);
		Free(ivtx);
		return FALSE;
	}

	if (NVertices < 2)
	{
		Free(UnitObj);
		Free(ivtx);
		return FALSE;
	}

	double Perim = 0.0;
	long Area = 0;
	long Xmin = LONG_MAX;
	long Xmax = LONG_MIN;
	long Ymin = LONG_MAX;
	long Ymax = LONG_MIN;
	long SumCOGx = 0;
	long SumCOGy = 0;
	double MinDist = DBL_MAX;
	double MaxDist = 0.0;

	wtx = UnitObj;
	vtx = wtx + 1;

	for (i = 0; i < NVertices; i++) 
	{
		if (i == NVertices - 1)
			vtx = UnitObj;
		
		// Bounding box
		if (vtx->vtX > Xmax) 
			Xmax = vtx->vtX;
		if (vtx->vtX < Xmin)
			Xmin = vtx->vtX;
		if (vtx->vtY > Ymax)
			Ymax = vtx->vtY;
		if (vtx->vtY < Ymin)
			Ymin = vtx->vtY;

		// Calculate perimeter
		dx = vtx->vtX - wtx->vtX;
		dy = vtx->vtY - wtx->vtY;
        // Lengthy but accurate enough
		//if (dx != 0 || dy != 0)
		//	Perim += sqrt(dx * dx + dy * dy);
        // This saves 100 microsecs / object on a 100 x DAPI cell !
        if (dx != 0)
        {
            if (dy == 0)
                Perim += 1.0;
            else
                Perim += 1.4142;        // sqrt(2)
        }
        else
        {
            if (dy != 0)
                Perim += 1.0;
        }

		// Calculate Area 
		Area += (vtx->vtX + wtx->vtX) * dy;

		// COG
		SumCOGx += vtx->vtX;
		SumCOGy += vtx->vtY;

		vtx++;
		wtx++;
	}
	Area /= 2;

	// Get the approximate centre of gravity
	WM->COGx = SumCOGx / NVertices;
	WM->COGy = SumCOGy / NVertices;
	
	vtx = UnitObj;

	for (i = 0; i < NVertices; i++) 
	{		
		// Calculate max and min dist from COG to perimeter
		dx = vtx->vtX - (long) WM->COGx;
		dy = vtx->vtY - (long) WM->COGy;

        // NOTE dont sqrt it here, this saves precious microsecs
        double Dist = dx * dx + dy * dy;
		if (Dist < MinDist) 
			MinDist = Dist;
		if (Dist > MaxDist) 
			MaxDist = Dist;
	
		vtx++;
	}

	// Min and max radii, axis ratio
	WM->MinRadii = sqrt(MinDist + 1.);
	WM->MaxRadii = sqrt(MaxDist + 1.);

    // Take the max diameter to be twice the distance from
    // the COG to the furthest point on the boundary. This
    // is not really quite right but gives nice results for
    // compactness and circularity measures.
    double D = WM->MaxRadii + WM->MaxRadii;
    
	// Axis ratio
	WM->AxisRatio = WM->MinRadii / WM->MaxRadii;


	if (measurementSet == 2)
	{
 		// The formula for circularity in measurement set 1 (4.A / PI.D.D) is a bit dubious,
		// as it is based on the area and maximum radius of the object,
		// rather than the area and circumference (as is more usual).
 		// Circularity is usually and more usefully defined as the square of the ratio of
 		// the circumference of a circle with the same area to the actual circumference (4.PI.A / C.C]). 
		// Note that the area measurement used must be the 'enclosed area', as that is the area enclosed
		// by the polygonal representation of the object that is used for calculating the circumference.
		// A lower than expected circularity may be obtained for objects that have a jagged edge at a
		// small scale - if this turns out to be a problem, the polygonal representation of objects may
		// need more smoothing before the perimeter is calculated (tell polysmooth() to do more passes).
		WM->Circularity = (4.0 * 3.1416 * Area) / (Perim * Perim);	
	}
	else // Measurement set 1
	{
		// This measure of circularity is somewhat redundant, as it's just
		// the square of the compactness measure calculated in measurement set 1!
		// The formula in measurement set 2 is better.
		// Must leave this here for compatibility with old code though I suppose.		
		WM->Circularity = (4.0 * Area) / (3.142 * D * D);
	}
    
	// Make sure circularity stays in range sometimes a wee bit over 1.00
	if (WM->Circularity > 1.0000) WM->Circularity = 1.0;


	// Area and perimeter
	WM->EnclosedArea = Area;
	WM->Perimeter = Perim;
	
	// Compactness (sqrt(4.A / PI) / D).
	// Note that a more usual formulation for compactness is (C.C / A), but this is inversely proportional
	// to our circularity measurement (for measurement set 2) so would be redundant.
	if (Area > 0 && D > 0)
		WM->Compactness = sqrt(1.273 * Area) / D;			// NB 1.273 = 4/pi
	else
		WM->Compactness = 0.0;
	
	// Bounding box
	WM->BBLeft   = Xmin;
	WM->BBRight  = Xmax;
	WM->BBTop	 = Ymin;
	WM->BBBottom = Ymax;
    
    // Convex hull for the object
	struct object *cvh = (struct object *)convhull(m_pObjectList[ObjectIndex]);

    // Calc the rectangle
	struct object *rect = (struct object *)minwrect(cvh);

    // Get the rectangle vertices
    POINT  V1, V2, V3, V4;    
    V1.x = (int) ((struct frect *)rect->idom)->frk[0];
    V2.x = (int) ((struct frect *)rect->idom)->frk[1];
    V3.x = (int) ((struct frect *)rect->idom)->frk[2];
    V4.x = (int) ((struct frect *)rect->idom)->frk[3];
    V1.y = (int) ((struct frect *)rect->idom)->frl[0];
    V2.y = (int) ((struct frect *)rect->idom)->frl[1];
    V3.y = (int) ((struct frect *)rect->idom)->frl[2];
    V4.y = (int) ((struct frect *)rect->idom)->frl[3];

    // Calc orientation of the rectangle
	WM->Orientation = ((struct frect *)rect->idom)->rangle;

    // Calc Height and Width of the rectangle
    dx = V1.x - V2.x;
    dy = V1.y - V2.y;
    WM->Width = (long) sqrt(dx * dx + dy * dy + 0.1);
    dx = V2.x - V3.x;
    dy = V2.y - V3.y;
    WM->Height = (long) sqrt(dx * dx + dy * dy + 0.1);		// Get array info
 
/*   
struct object *pHullObject = polytoobj((polygondomain*)cvh->idom);
struct object *pHullObject = polytoblob((polygondomain*)cvh->idom);
int hullArea = area(pHullObject);
assert(hullArea + 1 >= WM->Area);
WM->Compactness = min(WM->Area / hullArea, min(WM->Width, WM->Height) / max(WM->Width, WM->Height));
freeobj(pHullObject);
m_pObjectList[ObjectIndex] = pHullObject;
*/    

    // Free the temp objects
    freeobj(rect);
    freeobj(cvh);

	Free(UnitObj);
    Free(ivtx);

	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////
// Do a selection of measurements on an object found using the find objects above
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::MeasureMergedObject(int ObjectIndex, WoolzMeasurements *WM)
{
    int N;
    BOOL MergedObj = FALSE;
    POSITION P;
    WoolzMeasurements WMerged, WTemp;
    MergeObj *pM;

    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS || m_pMeasuredObject[ObjectIndex] || !m_pObjectList[ObjectIndex])
        return NullObject;

    // Is this a merged object ?
    if (m_pMergeTable)
    {
        MergedObj = m_pMergeTable[ObjectIndex];
        // No it isn't so call the standard measurement routine and exit
        if (!MergedObj)
        {
            MeasureObject(ObjectIndex, WM);             // Standard measurements
            m_pMeasuredObject[ObjectIndex] = TRUE;      // Mark as measured
            return NotMergedObject;                     // Object not a merged object
        }
    }

    // Zero our total measurements
    memset(&WMerged, 0, sizeof(WoolzMeasurements));
    WMerged.BBLeft   = LONG_MAX;
    WMerged.BBRight  = LONG_MIN;
    WMerged.BBTop    = LONG_MAX;
    WMerged.BBBottom = LONG_MIN;

    N = 1;
    P = m_MergeObjects.GetHeadPosition();
    while (P)
    {
        pM = m_MergeObjects.GetAt(P);
        // Do we have an object connected to this one ? If so measure it
        if (pM->ObjI == ObjectIndex && !m_pMeasuredObject[pM->ObjJ] && !pM->Measured)
        {
            pM->Measured = TRUE;
            MeasureMergedObject(pM->ObjJ, &WTemp);
            N++;

            WMerged.Area += WTemp.Area;
            WMerged.AxisRatio += WTemp.AxisRatio;
            WMerged.Circularity += WTemp.Circularity;
            WMerged.Compactness += WTemp.Compactness;
            WMerged.COGx += WTemp.COGx;
            WMerged.COGy += WTemp.COGy;
            WMerged.EnclosedArea += WTemp.EnclosedArea;
            WMerged.MaxRadii += WTemp.MaxRadii;
            WMerged.MinRadii += WTemp.MinRadii;
            WMerged.Perimeter += WTemp.Perimeter;
            WMerged.Orientation += WTemp.Orientation;
            WMerged.BBLeft = MIN(WMerged.BBLeft, WTemp.BBLeft);
            WMerged.BBRight = MAX(WMerged.BBRight, WTemp.BBRight);
            WMerged.BBTop = MIN(WMerged.BBTop, WTemp.BBTop);
            WMerged.BBBottom = MAX(WMerged.BBBottom, WTemp.BBBottom);
            WMerged.Width += WTemp.Width;
            WMerged.Height += WTemp.Height;
            m_pMeasuredObject[pM->ObjJ] = TRUE;
        }
        else
        if (pM->ObjJ == ObjectIndex && !m_pMeasuredObject[pM->ObjI] && !pM->Measured)
        {
            pM->Measured = TRUE;
            MeasureMergedObject(pM->ObjI, &WTemp);
            N++;

            WMerged.Area += WTemp.Area;
            WMerged.AxisRatio += WTemp.AxisRatio;
            WMerged.Circularity += WTemp.Circularity;
            WMerged.Compactness += WTemp.Compactness;
            WMerged.COGx += WTemp.COGx;
            WMerged.COGy += WTemp.COGy;
            WMerged.EnclosedArea += WTemp.EnclosedArea;
            WMerged.MaxRadii += WTemp.MaxRadii;
            WMerged.MinRadii += WTemp.MinRadii;
            WMerged.Perimeter += WTemp.Perimeter;
            WMerged.Orientation += WTemp.Orientation;
            WMerged.BBLeft = MIN(WMerged.BBLeft, WTemp.BBLeft);
            WMerged.BBRight = MAX(WMerged.BBRight, WTemp.BBRight);
            WMerged.BBTop = MIN(WMerged.BBTop, WTemp.BBTop);
            WMerged.BBBottom = MAX(WMerged.BBBottom, WTemp.BBBottom);
            WMerged.Width += WTemp.Width;
            WMerged.Height += WTemp.Height;
            m_pMeasuredObject[pM->ObjI] = TRUE;
        }

        m_MergeObjects.GetNext(P);
    }
        
    // Now measure this object
    MeasureObject(ObjectIndex, &WTemp);
    m_pMeasuredObject[ObjectIndex] = TRUE;

    WMerged.Area += WTemp.Area;
    WMerged.AxisRatio += WTemp.AxisRatio;
    WMerged.Circularity += WTemp.Circularity;
    WMerged.Compactness += WTemp.Compactness;
    WMerged.COGx += WTemp.COGx;
    WMerged.COGy += WTemp.COGy;
    WMerged.EnclosedArea += WTemp.EnclosedArea;
    WMerged.MaxRadii += WTemp.MaxRadii;
    WMerged.MinRadii += WTemp.MinRadii;
    WMerged.Perimeter += WTemp.Perimeter;
    WMerged.Orientation += WTemp.Orientation;
    WMerged.BBLeft = MIN(WMerged.BBLeft, WTemp.BBLeft);
    WMerged.BBRight = MAX(WMerged.BBRight, WTemp.BBRight);
    WMerged.BBTop = MIN(WMerged.BBTop, WTemp.BBTop);
    WMerged.BBBottom = MAX(WMerged.BBBottom, WTemp.BBBottom);
	WMerged.Width += WTemp.Width;
	WMerged.Height += WTemp.Height;

    WM->Area = WMerged.Area;
    WM->AxisRatio = WMerged.AxisRatio / N;
    WM->Circularity = WMerged.Circularity / N;
    WM->COGx = WMerged.COGx / N;
    WM->COGy = WMerged.COGy / N;
    WM->Compactness = WMerged.Compactness / N;
    WM->EnclosedArea = WMerged.EnclosedArea;
    WM->MaxRadii = WMerged.MaxRadii;
    WM->MinRadii = WMerged.MinRadii;
    WM->Perimeter = WMerged.Perimeter;
    WM->BBLeft = WMerged.BBLeft;
    WM->BBRight = WMerged.BBRight;
    WM->BBTop = WMerged.BBTop;
    WM->BBBottom = WMerged.BBBottom;
    WM->Orientation = WMerged.Orientation / N;
	WM->Width = WMerged.BBRight - WMerged.BBLeft;
	WM->Height = WMerged.BBBottom - WMerged.BBTop;

	return MergedObject;
}

////////////////////////////////////////////////////////////////////////////////////////
// Measures the boundary of an object
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::MeasureBoundary(int ObjectIndex, long **Boundary)
{
	struct ivertex *UnitObj, *IObj;
	register struct ivertex *vtx, *wtx;
	register int i;
    int N, NVertices;
    long *Bptr;

    // Free any previous array of points
    if ((*Boundary) != NULL)
        delete [] (*Boundary);
    
    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return 0;

	if (!EnsureType1Object(ObjectIndex))
		return 0;
         
	// Convert the object to a unit boundary with wrap to beginning
	IObj = bigbound(m_pObjectList[ObjectIndex], 1, &N);
	if (IObj == NULL)
		return 0;

	// Calc the boundary points
	UnitObj = unitbigbound(IObj, N, &NVertices);
	if (UnitObj == NULL) 
    {
		Free(IObj);
		return 0;
	}

	// check for closed polygon.
	// should really check vertex adjacency,
	// but this would take a long time.	
	vtx = UnitObj;
	wtx = vtx + NVertices - 1;
	if (vtx->vtX != wtx->vtX || vtx->vtY != wtx->vtY) 
	{
		TRACE1("OBJECT %ld NOT A CLOSED POLYGON\r\n", ObjectIndex);
		return 0;
	}

	if (NVertices < 2)
		return 0;

    *Boundary = (long *) new long[NVertices * 2];

    Bptr = (*Boundary);
	wtx = UnitObj;

	for (i = 0; i < NVertices; i++) 
	{
		*Bptr++ = (long) wtx->vtX;       // Fill in the boundary point array     
        *Bptr++ = (long) wtx->vtY;
		wtx++;
	}
	
	// Get the approximate centre of gravity
	Free(UnitObj);
	Free(IObj);

	return i;             // Return the number of points in the boundary
}

////////////////////////////////////////////////////////////////////////////////////////
// Get the connection points for a woolz object i.e. those points within
// a distance 'dist' of the min width rectangle
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::GetConnectionPoints(int ObjectIndex, double dist, long **ConnectionPoints)
{
	struct ivertex *UnitObj, *IObj;
	register struct ivertex *vtx, *wtx;
	register int i;
    int N, NVertices;
    long  *Cptr;


    // Free any previous array of points
    if ((*ConnectionPoints) != NULL)
        delete [] (*ConnectionPoints);
    
    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return 0;

	if (!EnsureType1Object(ObjectIndex))
		return 0;
         
	// Convert the object to a unit boundary with wrap to beginning
	IObj = bigbound(m_pObjectList[ObjectIndex], 1, &N);
	if (IObj == NULL)
		return 0;

	// Calc the boundary points
	UnitObj = unitbigbound(IObj, N, &NVertices);
	if (UnitObj == NULL) 
    {
		Free(IObj);
		return 0;
	}

	// check for closed polygon.
	// should really check vertex adjacency,
	// but this would take a long time.	
	vtx = UnitObj;
	wtx = vtx + NVertices - 1;
	if (vtx->vtX != wtx->vtX || vtx->vtY != wtx->vtY) 
	{
		TRACE1("OBJECT %ld NOT A CLOSED POLYGON\r\n", ObjectIndex);
		return 0;
	}

	if (NVertices < 2)
		return 0;


	float A;
	POINT V1, V2, V3, V4;

	MinWidthRectangle(ObjectIndex, &A, &V1, &V2, &V3, &V4);

	*ConnectionPoints = (long *) new long[NVertices * 2];

	int CC = 0;

	double D2 = dist * dist;
	
	wtx = UnitObj;

	 Cptr = (*ConnectionPoints);

	for (i = 0; i < NVertices; i++) 
	{
		POINT T;

		T.x = (long)wtx->vtX;
		T.y = (long)wtx->vtY;
		wtx++;

		double D1, D2, D3, D4, d;

		D1 = vDistFromPointToLine(&V1, &V2, &T);
		D2 = vDistFromPointToLine(&V2, &V3, &T);
		D3 = vDistFromPointToLine(&V3, &V4, &T);
		D4 = vDistFromPointToLine(&V4, &V1, &T);

		d = min(D1, D2);
		d = min(d,  D3);
		d = min(d,  D4);

		if (d * d < D2)
		{ 
			*Cptr++ = T.x;
			*Cptr++ = T.y;
			CC++;
		}
	}
	
	Free(UnitObj);
	Free(IObj);


	return CC;             // Return the number of points in the boundary
}


////////////////////////////////////////////////////////////////////////////////////////
// Get the closest object point to an object
////////////////////////////////////////////////////////////////////////////////////////
double CWoolzIP::GetClosestObjectPoint(int ObjectIndex, long OX, long OY, long *X, long *Y)
{
	struct ivertex *UnitObj, *IObj;
	register struct ivertex  *wtx;
	register int i;
    int N, NVertices;
   
    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return -1.0;

	if (!EnsureType1Object(ObjectIndex))
		return -1.0;
         
	// Convert the object to a unit boundary with wrap to beginning
	IObj = bigbound(m_pObjectList[ObjectIndex], 1, &N);
	if (IObj == NULL)
		return -1.0;

	// Calc the boundary points
	UnitObj = unitbigbound(IObj, N, &NVertices);
	if (UnitObj == NULL) 
    {
		Free(IObj);
		return -1.0;
	}

	double DMin = DBL_MAX;
	
	wtx = UnitObj;

	for (i = 0; i < NVertices; i++) 
	{

		long Dx = OX - (long)wtx->vtX;
		long Dy = OY - (long)wtx->vtY;

		double D = Dx * Dx + Dy * Dy;

		if (D < DMin)
		{
			DMin = D;
			*X = (long)wtx->vtX;
			*Y = (long)wtx->vtY;
		}

		wtx++;
	}

	Free(UnitObj);
	Free(IObj);

	return sqrt(DMin + 0.000001);
}


////////////////////////////////////////////////////////////////////////////////////////
// Get the connection points for a collection of woolz objects
// NOTE must have found the objects first !
////////////////////////////////////////////////////////////////////////////////////////

int CWoolzIP::GetClosestConnectionPoints(double Dist, long **ConnectionPoints)
{
	struct ivertex *UnitObj, *IObj;
	register struct ivertex *wtx;
	int j, i, k;
    int N, NVertices;
    long  *Cptr;
	int C = 0;
	Bounds *pBounds;

	 // Free any previous array of points
    if ((*ConnectionPoints) != NULL)
        delete [] (*ConnectionPoints);
	
	*ConnectionPoints = (long *) new long[m_nNumberObjects * m_nNumberObjects * 4];

	pBounds = new Bounds[m_nNumberObjects];

	for (i = 0; i < m_nNumberObjects; i++)
	{
		BoundingBox(i, pBounds[i]);
	}
    	
	Cptr = (*ConnectionPoints);

	for (i = 0; i < m_nNumberObjects; i++)
	{
  		if (EnsureType1Object(i))
		{         
			// Convert the object to a unit boundary with wrap to beginning
			IObj = bigbound(m_pObjectList[i], 1, &N);
			if (IObj != NULL)
			{
				// Calc the boundary points
				UnitObj = unitbigbound(IObj, N, &NVertices);
				if (UnitObj != NULL) 
				{
					for (j = i; j < m_nNumberObjects; j++)
					{
						if (j != i && BoundingProximity(pBounds[i], pBounds[j]) < Dist)
						{
							wtx = UnitObj;

							double MinDist = DBL_MAX;
							long AX = -1, AY = -1, BX = -1, BY = -1;

							for (k = 0; k < NVertices; k++) 
							{
								long CX, CY;

								double D = GetClosestObjectPoint(j, wtx->vtX, wtx->vtY, &CX, &CY);

								if (D < Dist && D < MinDist)
								{
									MinDist = D;
									AX = wtx->vtX;
									AY = wtx->vtY;
									BX = CX;
									BY = CY;
								}

								wtx++;
							}

							if (MinDist < Dist)
							{
								*Cptr++ = AX;
								*Cptr++ = AY;
								*Cptr++ = BX;
								*Cptr++ = BY;
								C++;
							}
						}
					}

					Free(UnitObj);
				}

				Free(IObj);
			}
		}
	}

	delete [] pBounds;

	return C;
}


////////////////////////////////////////////////////////////////////////////////////////
// Fractal measure the boundary of an object
// NSamples is the max sample spacing around the perimeter. 1 to NSamples iterations
// will be done
////////////////////////////////////////////////////////////////////////////////////////

double CWoolzIP::FractalMeasureBoundary(int ObjectIndex, int NSamples)
{
	struct ivertex *UnitObj, *IObj;
	register struct ivertex *vtx, *wtx;
	int i, s, dx, dy;
    int N, NVertices;
    double *Y, *X, Result, *XY, *XX, SX, SXX, SY, SXY;

    // Pointless for less than 3 samples
    if (NSamples < 3) 
        return 0.0;

    
    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return 0;

 	if (!EnsureType1Object(ObjectIndex))
		return 0;

         
	// Convert the object to a unit boundary with wrap to beginning
	IObj = bigbound(m_pObjectList[ObjectIndex], 1, &N);
	if (IObj == NULL)
		return 0;

	// Calc the boundary points
	UnitObj = unitbigbound(IObj, N, &NVertices);
	if (UnitObj == NULL) 
    {
		free(IObj);
		return 0;
	}

	// check for closed polygon.
	// should really check vertex adjacency,
	// but this would take a long time.	
	vtx = UnitObj;
	wtx = vtx + NVertices - 1;
	if (vtx->vtX != wtx->vtX || vtx->vtY != wtx->vtY) 
	{
		TRACE1("OBJECT %ld NOT A CLOSED POLYGON\r\n", ObjectIndex);
		return 0.0;
	}

	if (NVertices < 2)
		return 0.0;

    // Arrays to hold the data in
    X = new double[NSamples];
    Y = new double[NSamples];
    XX = new double[NSamples];
    XY = new double[NSamples];

#if 0
    // Square root table - should be faster than calculating
    // a square root everytime unless NSamples is small
    Roots = new double[(NSamples + 1) * (NSamples + 1)];

    for (i = 0; i <= NSamples; i++)
    {
        for (j = 0; j <= NSamples; j++)
            Roots[i * NSamples + j] = sqrt(i * i + j * j + 0.000000001);
    }
#endif

    // Iterate over the boundary
    for (s = 1; s <= NSamples; s++)
    {
	    wtx = UnitObj;
        vtx = UnitObj;
        wtx += s;
        i = NSamples;
        Result = 0.0;
	    while (i < NVertices)
	    {
            dx = abs(wtx->vtX - vtx->vtX);
            dy = abs(wtx->vtY - vtx->vtY);
            Result += sqrt((double)(dx * dx + dy * dy));//Roots[dx * NSamples + dy];
            i += s;
            vtx += s;
            wtx += s;
        }

        X[s - 1] = log10(Result);
        Y[s - 1] = log10((double)s);
        XY[s - 1] = X[s - 1] * Y[s - 1];
        XX[s - 1] = X[s - 1] * X[s - 1];
    }

    // Use linear regression to find the best line fit though the points 
    // calculated above. The slope is then the fractal measure
    SX = 0.0;
    SY = 0.0;
    SXX = 0.0;
    SXY = 0.0;
    for (s = 0; s < NSamples; s++)
    {
        SX += X[s];
        SY += Y[s];
        SXX += XX[s];
        SXY += XY[s];
    }

    Result = fabs(NSamples * SXY - SX * SY) / (NSamples * SXX - SX * SX);

    // Clear up
    delete X;
    delete Y;
    delete XX;
    delete XY;
    //delete Roots;
	
	free(UnitObj);
	free(IObj);

	return Result;             // Return the number of points in the boundary
}

////////////////////////////////////////////////////////////////////////////////////////
// Simply returns the pointer to an object with a specific object index
////////////////////////////////////////////////////////////////////////////////////////

void *CWoolzIP::GetObjectPtr(int ObjectIndex)
{
    // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return NULL;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return FALSE;

	if (m_pObjectList[ObjectIndex]->type != 1)
		return FALSE;

    return m_pObjectList[ObjectIndex];
}

////////////////////////////////////////////////////////////////////////////////////////
// Remove an object from the internal lists
////////////////////////////////////////////////////////////////////////////////////////

BOOL CWoolzIP::RemoveObject(int index)
{
	BOOL status = FALSE;

    // First check to make sure the object index is not silly
    if (index >= 0 && index < m_nNumberObjects)
	{
		if (m_pObjectList[index])
		{
			freeobj(m_pObjectList[index]);
			m_pObjectList[index] = NULL;
		}

		m_nNumberObjects--;

		// Shuffle down objects to close gap where object was removed.
		// Must shuffle down corresponding labels too!
		for (int i = index; i < m_nNumberObjects; i++)
		{
			m_pObjectList[i] = m_pObjectList[i + 1];
			m_pLabelList[i] = m_pLabelList[i + 1];
		}

		// Must clear the last object that was shuffled down, as FreeObjects()
		// frees everything up to MAX_WOOLZ_OBJECTS rather than just m_nNumberObjects.
		m_pObjectList[m_nNumberObjects] = NULL;
		m_pLabelList[m_nNumberObjects] = 0;

		status = TRUE;
	}

	return status;
}



void CWoolzIP::Deagglomerate()
{
// This version of Deagglomerate() deagglomerates the internal object list.
	struct object *objList[MAX_WOOLZ_OBJECTS], *deagglomObjList[MAX_WOOLZ_OBJECTS];
	
	for (int i = 0; i < MAX_WOOLZ_OBJECTS; i++)
	{
		objList[i] = NULL;
		deagglomObjList[i] = NULL;
	}

	int numDeagglomObjects = 0;

    for (int n = 0; n < m_nNumberObjects; n++)
	{
		// Deagglomerate returns a copy of the original object that has cut marks
		// where it finds boundaries between agglomerated objects.
       	struct object *pDeagglomObj = deagglomerate(m_pObjectList[n]);
       	
		// Extract the deagglomerated objects from the composite object.
		int numObjs = 0;
   		label_area(pDeagglomObj, &numObjs, objList, MAX_WOOLZ_OBJECTS, 1);      // Min area of one
   		
		for (int i = 0; i < numObjs; i++)
		{
			vdom21to1(objList[i]);
			
			deagglomObjList[ numDeagglomObjects++ ] = objList[i];
		}
		
		freeobj(pDeagglomObj);
	}

	// Free the original objects
	FreeObjects();
	
	// Copy objects from deagglomObjList to m_pObjectList
	for (int i = 0; i < numDeagglomObjects; i++)
	{
		m_pObjectList[i] = deagglomObjList[i];
	}
	
	m_nNumberObjects = numDeagglomObjects;
}



////////////////////////////////////////////////////////////////////////////////////////
// draw idom into an image using supplied val
////////////////////////////////////////////////////////////////////////////////////////

void CWoolzIP::DrawIdom(int ObjectIndex, FSCHAR *image, int image_width, int image_height, int value)
{
    FSCHAR *imptr;
    struct iwspace iwsp;
    //struct gwspace gwsp;
    register int k;

     // Check to make sure the object index is not silly
    if (ObjectIndex < 0 || ObjectIndex >= m_nNumberObjects || ObjectIndex >= MAX_WOOLZ_OBJECTS)
        return;

    // Now check to make sure the object is in the object list
    if (!m_pObjectList[ObjectIndex])
        return;

	if (m_pObjectList[ObjectIndex]->type != 1)
        return;

    if (wzcheckobj(m_pObjectList[ObjectIndex])) 
    	return;

    if (wzemptyidom(m_pObjectList[ObjectIndex]->idom)) 
    	return;

    	
	initrasterscan(m_pObjectList[ObjectIndex],&iwsp,0);
	while (nextinterval(&iwsp) == 0)
	{
    	imptr = (FSCHAR *) image + iwsp.linpos * image_width + iwsp.lftpos;
		for (k = iwsp.lftpos; k <= iwsp.rgtpos; k++) 
		{
			*imptr = value;
			imptr++;
		}
	}
}



void CWoolzIP::ErodeLabelledObjects(long labelNumber)
{
	// Cycle through all found objects
	for (int i = 0; i < m_nNumberObjects; i++)
	{
		// Only use labelled objects that match this label
		if (m_pLabelList[i] == labelNumber && m_pObjectList[i])
		{
			object *pErodedObject = ErodeObject(m_pObjectList[i]);
			if (pErodedObject)
			{
				freeobj(m_pObjectList[i]);
				
				m_pObjectList[i] = pErodedObject;
			}
		}
	}
}
/*
void CWoolzIP::DilateLabelledObjects(long labelNumber)
{
// TODO: Calling this repeatedly causes crashes - fix it if you know how!

	// Cycle through all found objects
	for (int i = 0; i < m_nNumberObjects; i++)
	{
		// Only use labelled objects that match this label
		if (m_pLabelList[i] == labelNumber && m_pObjectList[i])
		{
			object *pDilatedObject = dilation4(m_pObjectList[i]);
			if (pDilatedObject)
			{
				object *pOldObject = m_pObjectList[i];
				
				pDilatedObject->vdom = (struct valuetable *)copyvaluetb(pOldObject, pOldObject->vdom->type,
				                                                                    pOldObject->vdom->bckgrnd);				
				
				m_pObjectList[i] = pDilatedObject;
				
				//freeobj(pOldObject);
			}		
		}
	}
}
*/



//////////////////////////////////////////////////////////////////////////////


static BOOL FindNearestSplitVertex(struct polygondomain *pdom, int fromIndex, int maxNeckWidth, /*int minCircumference,*/ struct ivertex *pVertex)
{
// Now find the closest point on the polygon (if any) that is not further away
// (linearly) than maxNeckWidth from the vertex specified by fromIndex,
// but is at least the larger of maxNeckWidth and minCircumference away
// along the circumference.

	BOOL foundNearest = FALSE;

	if (pdom && pVertex)
	{
		struct ivertex fromVertex;
		fromVertex.vtX = pdom->vtx[ fromIndex ].vtX;
		fromVertex.vtY = pdom->vtx[ fromIndex ].vtY;
	
		int minSplitDistSquared = INT_MAX;
		// Don't allow maxNeckWidth to be more than sqrt(INT_MAX), otherwise squaring it will overflow!
		maxNeckWidth = min(maxNeckWidth, 46340); 
		int maxNeckWidthSquared = maxNeckWidth * maxNeckWidth;
						
		int n = pdom->nvertices;
						
		for (int i = 0; i < n; i++)
		{
			if (i == fromIndex)
				continue;
									
			int circumDist1 = abs(i - fromIndex);
			int circumDist2 = n - max(i, fromIndex) + min(i, fromIndex); // Dist the other way around (passing through zero).
			/*
			// At least one of the objects produced by splitting must have at least the minimum circumference
			// (the other one may be a small protruberence attached to a convex object, that needs to be trimmed off).
			if (circumDist1 >= minCircumference || circumDist2 >= minCircumference)
			*/
			{
				int distx = fromVertex.vtX - pdom->vtx[ i ].vtX;
				int disty = fromVertex.vtY - pdom->vtx[ i ].vtY;							

				int distSquared = distx*distx + disty*disty;

				// If a candidate split line has already been found, see if the current distance
				// from the deepest concavity is less than the previous shortest distance.
				// Otherwise check that the distance is no more than maxNeckWidth.
				if ((foundNearest && distSquared < minSplitDistSquared) || (!foundNearest && distSquared <= maxNeckWidthSquared))
				{
					// BOTH the objects produced by splitting must have a circumference that is at least
					// twice as long as the neck width (i.e. square of circumference is at least four times
					// square of neck width). This prevents splitting before going far enough around
					// the circumference to enclose something (e.g. the split line should not be a tangent line).
					// It also prevents splitting something fairly circular, whatever its size.
					if (   circumDist1 * circumDist1 >= distSquared * 4
					    && circumDist2 * circumDist2 >= distSquared * 4)
					{
						pVertex->vtX = pdom->vtx[ i ].vtX;
						pVertex->vtY = pdom->vtx[ i ].vtY;

						minSplitDistSquared = distSquared;

						foundNearest = TRUE; // Found nearest candidate so far.
					}
				}
			}
		}
	}
	
	return foundNearest;
}

static BOOL FindSplitLine(struct object *pObj, int maxNeckWidth, /*int minCircumference,*/ int minCurvature, struct ivertex *pV1, struct ivertex *pV2)
{
// Based on CWoolzIP::feature_pos().
	BOOL foundSplit = FALSE;

	if (pObj && pObj->type == 1 && pV1 && pV2)
	{
		// Compute a boundary polygon object.
		struct object *pBobj = ibound(pObj, 1);
		if (pBobj)
		{
			// Convert the boundary object such that polygon vertices lie on adjacent (8-connected) pixels.
			struct object *pUobj = unitbound(pBobj);
			if (pUobj)
			{
				struct polygondomain *pdom = (struct polygondomain *)pUobj->idom;
				/*
				// No point continuing if the circumference isn't greater than the minimum circumference.
				if (pdom && pdom->nvertices > minCircumference)
				*/
				{
					// Compute the curvature of the polygon at each point along its perimeter.
					// This is output as a histogram, though it's not a histogram, it's graph
					// of curvature against distance around the perimeter.
					struct object *h = curvat(pUobj);
					if (h)
					{
						// Smooth the curvature graph. No smoothing means the graph is too noisy
						// and features are not wide enough to detect. Too much smoothing erases
						// brief, abrupt changes in curvature.
						CWoolzIP::curvsmooth2(h, 3);

						struct histogramdomain *hdom = (struct histogramdomain *)h->idom;
					
						int n = hdom->npoints;
						assert(hdom->npoints == pdom->nvertices);
						int *pt = hdom->hv;

						int bestPeak = -1;
						int currentPeak = -1;
						
						int start = -1;
						int end   = -1;

						int wrap = 20;
						if (wrap > n / 2)
							wrap = n / 2;
						

						// Find the largest graph peak that meets the criteria.
						// Scan for features - wrap around to catch end feature.
						for (int i = 0; i < n + wrap; i++)
						{
							if (*pt >= minCurvature)
							{
								// Above minCurvature, so log start point if we were previously not
								// above the threshold and move end point to current position.
								if (start < 0)
									start = i;
					
								end = i;

								// Record the largest value in the current peak.
								if (*pt >= currentPeak)
									currentPeak = *pt;
							}
							else
							{
								// Below minCurvature, so if we were previously above minCurvature we
								// have just completed traversing a peak in the graph (a concavity).
								// If this peak is at least as large as the best peak (note the best peak
								// may not be the largest peak), check to see if it meets the criteria
								// to be the new best peak. If the point at index zero is within the best peak,
								// the best peak will be found again when we wrap-around, so ensure that the start
								// and end points are based on the second time the feature is found, as these will
								// be more accurate, by checking this peak even if currentPeak is the same as bestPeak.
								if (currentPeak >= bestPeak && start >= 0 && end - start > 5)
								{
									int mid = (end + start) / 2;

									// Handle wrap-around.
									if (mid > n - 1)
										mid = mid - n;
										
									// This can only be a candidate position to start a split, if the closest point to it
									// that is at least the larger of maxNeckWidth and minCircumference away along the
									// circumference, is no further away linearly than maxNeckWidth.
									if (FindNearestSplitVertex(pdom, mid, maxNeckWidth, /*minCircumference,*/ pV2))
									{
										pV1->vtX = pdom->vtx[ mid ].vtX;
										pV1->vtY = pdom->vtx[ mid ].vtY;
										
										foundSplit = TRUE; // Found best candidate split so far.
										
										bestPeak = currentPeak; // A different split will only be used if its peak concavity exceeds this.										
									}
								}
							
								currentPeak = -1;

								start = -1;
								end   = -1;
							}

							if (i == n - 1)
								pt = hdom->hv;
							else
								pt++;
						}

						freeobj(h);
					}
				}

				freeobj(pUobj);
			}

			freeobj(pBobj);
		}
	}

	return foundSplit;
}

static void RecursiveSplitObject(object *pObject, int maxNeckWidth, /*int minCircumference,*/ int minCurvature, int minArea,
                                 object *splitObjectList[], int *pNumSplitObjects)
{
// Note that the calling code should not use pObject after calling this function,
// as it will be freed it if it is smaller than minArea.
// If pObject cannot be split but isn't smaller than minArea,
// it will be copied to splitObjectList.
	if (pObject && splitObjectList && pNumSplitObjects)
	{
		// Put array of split objects on the heap, not the stack, as this is a recursive function
		// and the stack might overflow otherwise.
		// There will only normally be two objects in the array, but some convoluted shapes could
		// produce more and polysplit() may return any number of objects.
		object **tempObjList = new object* [MAX_WOOLZ_OBJECTS];
		int numTempObj = 0;		
	
		struct ivertex v1, v2;
		if (FindSplitLine(pObject, maxNeckWidth, /*minCircumference,*/ minCurvature, &v1, &v2))
		{
			// Draw splitline through blob
			// Create a polygon Woolz object from the splitline.
			struct ivertex polyVertices[2];
			polyVertices[0] = v1;
			polyVertices[1] = v2;			
					
			polygondomain *pPolyDomain = makepolydmn(1, (WZCOORD*)polyVertices, 2, 2, 0);
			object *pPolyObject = makemain(10, (struct intervaldomain *)pPolyDomain, NULL, NULL, NULL);
			pPolyDomain->nvertices = 2;

			// Note that if it succeeds, polysplit() frees the original object.
			polysplit(pObject, pPolyObject, tempObjList, &numTempObj);
			
			freeobj(pPolyObject); // This will free the polygondomain too.
		}

		if (numTempObj > 0)
		{
			// TODO: if only one object is returned by polysplit, should maybe make sure
			// it is smaller than the original object, as if the splitline didn't intersect
			// with the object, it won't be. In this case recursion might carry on infinitely
			// (until it crashes).


			// The object has been split into sub-objects, so the original object can now be deleted.
			freeobj(pObject);

		
			// Now recursively try to split the parts that the object has been split into.
			// If splitting the object only resulted in one object, this may be because
			// the object split off was too small to be returned by polysplit(). Note that
			// split objects may be slightly smaller than expected because the splitline
			// itself is removed from split objects.
			for (int i = 0; i < numTempObj; i++)
				RecursiveSplitObject(tempObjList[i], maxNeckWidth, /*minCircumference,*/ minCurvature, minArea,
				                     splitObjectList, pNumSplitObjects);
		}
		else
		{
			// This object can't be split any more (it's probably either too small or too convex),
			// so add it to the final list, unless it's smaller than minArea.
			if (area(pObject) >= minArea)
			{
				if (*pNumSplitObjects < MAX_WOOLZ_OBJECTS) // Check there's room for another object.
					splitObjectList[ (*pNumSplitObjects)++ ] = pObject;
			}
			else
				freeobj(pObject);
		}
		
		delete [] tempObjList;		
	}
}

int CWoolzIP::SplitObjects(int maxNeckWidthSplit, /*int minCircumferenceSplit,*/ int minCurvatureSplit, int minArea)
{
// This works in a similar way to CWoolzIP::splitObjects(FSCHAR *image, ...),
// but splits objects in the internal list.
// Splitting is done recursively, so long as a split line can be found that is not longer
// than maxNeckWidthSplit and whose end points are at least the larger of maxNeckWidthSplit
// and minCircumferenceSplit apart along the circumference.
// Once a object has been split as much as it can be following the above rules, the
// resulting object fragments are only kept if they are at least minArea large. This
// stops the object list filling up with lots of tiny objects resulting from the splitting.
// So, maxNeckWidthSplit, minCircumferenceSplit and minCurvatureSplit are criteria for
// deciding whether an object can be split, whereas minArea is used for deciding whether
// an object can be kept after splitting has finished.
// Note that this means that even objects that are not split will be removed if they are
// smaller than minArea. It also means that there could be zero objects left after splitting.
	int numSplitObjects = 0;
	struct object *splitObjectList[MAX_WOOLZ_OBJECTS];

	if (maxNeckWidthSplit > 0)
	{
		for (int i = 0; i < m_nNumberObjects; i++)
			RecursiveSplitObject(m_pObjectList[i], maxNeckWidthSplit, /*minCircumferenceSplit,*/ minCurvatureSplit, minArea,
			                     splitObjectList, &numSplitObjects);

		// Copy objects from splitObjectList to m_pObjectList.
		for (int i = 0; i < numSplitObjects; i++)
			m_pObjectList[i] = splitObjectList[i];

		// The number of split objects may be less than the original number of objects
		// if some of the original objects were less than minArea. So must ensure that
		// now invalid pointers are set to NULL in m_pObjectList, as CWoolzIP::FreeObjects()
		// attempts to free all MAX_WOOLZ_OBJECTS objects pointed to by m_pObjectList and
		// so will crash if it is called subsequently and encounters a no longer valid pointer.
		for (int i = numSplitObjects; i < m_nNumberObjects; i++)
			m_pObjectList[i] = NULL;

		m_nNumberObjects = numSplitObjects;
	}

	return m_nNumberObjects;
}

//////////////////////////////////////////////////////////////////////////////
