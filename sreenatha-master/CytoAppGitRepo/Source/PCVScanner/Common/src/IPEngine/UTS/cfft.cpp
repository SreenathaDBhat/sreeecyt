////////////////////////////////////////////////////////////////////////////
// 
// FFT Class
//
// Written By Karl Ratcliff 04202001
//
////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include "vcmemrect.h"
#include "cfft.h"
#include "uts.h"
#include <math.h>

CUTSFFT::CUTSFFT(CUTSMemRect *pMR)
{
	m_pReal = NULL;
	m_pImag = NULL;
	pUTSMemRect = pMR;
}

CUTSFFT::~CUTSFFT()
{
}

//////////////////////////////////////////////////////////////////////////////
// fft()
//       Uses time decomposition with input bit reversal. The Cooley/Tookey
//       Fortran scheme for doing recursive odd/even decimation without really
//       doing bit reversal is used. The computation is done in place, so the
//       output replaces the input.  The contents of the arrays are changed
//     from the input data to the FFT coefficients.
//   Parameters:
//       npoints(int) the number of points in the FFT. Must be a power of 2.
//       real, imag(float *) pointers to arrays of floats for input and output.
//       inv(int)       inv = 1 for inverse transform
//                      inv = -1 for forward transform
//   Returns:
//     Nothing
///////////////////////////////////////////////////////////////////////////////
void CUTSFFT::fft1D(int npoints, float *real, float *imag, int inv)
{
	int i, index, swapindex, j, k;
	float tr, ti, angle, wr, wi;
	
	// SWAP THE INPUT ELEMENTS FOR THE DECIMATION IN TIME ALGORITHM. */
	for (index = 1, swapindex = 0; index < npoints; index++) 
	{
		k = npoints;
		do
		{
			k /= 2;
		}
		while ((swapindex + k) >= npoints);
		
		swapindex = (swapindex % k) + k;
		if (swapindex <= index)
			continue;
		
		tr = real[index];
		real[index] = real[swapindex];
		real[swapindex] = tr;
		ti = imag[index];
		imag[index] = imag[swapindex];
		imag[swapindex] = ti;
	}
	
	/*
	* DO THE BUTTERFLY COMPUTATIONS.
	*      stage index:    k = 1, 2, 4, 8 . . .
	*                      For npoints = 8, for example, there will
	*                      be three stages of butterfly computations.
	*      b'fly indices:  i and j will be separated by a distance
	*                      dependent on the current stage.
	*                      k is used as the separation constant.
	*/
	
	for (k = 1; k < npoints; k *= 2)
	{
		for (index = 0; index < k; index++) 
		{
			angle = (float) PI *((float) index * inv) / ((float) k);
			wr = (float) cos(angle);
			wi = (float) sin(angle);
			for (i = index; i < npoints; i += 2 * k) 
			{
				j = i + k;
				tr = (wr * (real[j])) - (wi * (imag[j]));
				ti = (wr * (imag[j])) + (wi * (real[j]));
				real[j] = real[i] - tr;
				imag[j] = imag[i] - ti;
				real[i] += tr;
				imag[i] += ti;
			}
		}
	}
		
	// for inverse transform, scale output by 1/N 
	if (inv == 1)
		for (i = 0; i < npoints; i++) 
		{
			real[i] = real[i] / npoints;
			imag[i] = imag[i] / npoints;
		}
}

///////////////////////////////////////////////////////////////////////////////
// fft2d()
//     fft2d performs 2-dimensional FFT on square image file
//     using Kernel as the kernel and
//     placing the convolved image in ImageOut
//   Parameters:
//     imageReal(float **) pointer to real array
//     imgImag(float **) pointer to imaginary array
//     nRow, nCol(long) number of rows and columns for real and img arrays
//                      must be power of 2
//     flag(short)      flag=-1 forward transform
//                              flag=1 reverse transform
//   Returns:        
//     Nothing
///////////////////////////////////////////////////////////////////////////////

void CUTSFFT::fft2D(float **imgReal, float **imgImag, long nRow, long nCol, short flag)
{
	int x, y;
	float *tempReal, *tempImag;   // temporary complex vectors 
	
	// allocate space for temporary vectors 
	tempReal = new float[nRow];
	tempImag = new float[nRow];
	
	// perform row-wise FFTs 
	for (y = 0; y < nRow; y++) 
		fft1D(nCol, imgReal[y], imgImag[y], flag);
	
	// perform column-wise FFTs 
	for (x = 0; x < nCol; x++) 
	{
		for (y = 0; y < nRow; y++) 
		{
			tempReal[y] = imgReal[y][x];
			tempImag[y] = imgImag[y][x];
		}
		fft1D(nRow, tempReal, tempImag, flag);
		for (y = 0; y < nRow; y++) 
		{
			imgReal[y][x] = tempReal[y];
			imgImag[y][x] = tempImag[y];
		}
	}
	
	delete tempReal;
	delete tempImag;

}

///////////////////////////////////////////////////////////////////////////////
// Allocate an floating point array for images
///////////////////////////////////////////////////////////////////////////////

FLOAT_PTR *CUTSFFT::AllocateFloatArray(long xSize, long ySize)
{
	FLOAT_PTR	*fp = NULL;
	BOOL		Ok = TRUE;
	long		y;

	// allocate memory for a 2D array 
	fp = new FLOAT_PTR[xSize];
	if (fp != NULL)
	{
		for (y = 0; y < xSize; y++) fp[y] = NULL;
		for (y = 0; y < xSize; y++)
		{
			fp[y] = new float[ySize];
			if (fp[y] == NULL)
				Ok = FALSE;
		}

		// Failed so clear up immediately
		if (!Ok)
		{
			for (y = 0; y < xSize; y++) 
				if (fp[y]) delete fp[y];
			delete fp;

			fp = NULL;
		}
	}

	return fp;
}

///////////////////////////////////////////////////////////////////////////////
// Free up an array of floats
///////////////////////////////////////////////////////////////////////////////

void CUTSFFT::DeallocateFloatArray(FLOAT_PTR *fp, long xSize)
{
	long y;
	
	if (fp)
	{
		for (y = 0; y < xSize; y++)
		{
			if (fp[y])
				delete fp[y];
		}
		
		delete fp;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Converts an 8 bit image into real float values and zero imaginary ones.
///////////////////////////////////////////////////////////////////////////////

void CUTSFFT::Image2Real(BYTE *Addr, float **pReal, float **pImag, float *min0, float *max0)
{
	BYTE *Ptr;
	long x, y;

	Ptr = Addr;
	*max0 = 0.0;
	*min0 = 256.0;
	for (y = 0; y < m_nRow2; y++) 
	{
		for (x = 0; x < m_nCol2; x++) 
		{
			pReal[y][x] = (float) *Ptr++;
			pImag[y][x] = (float) 0.0;
			if (pReal[y][x] > *max0)
				*max0 = pReal[y][x];
			if (pReal[y][x] < *min0)
				*min0 = pReal[y][x];
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// Convert an image back into real ones, scales and normalises result
// according to max0
///////////////////////////////////////////////////////////////////////////////

void CUTSFFT::Real2Image(BYTE *Addr, float **pReal, float max0)
{
	double min, max, norm;
	long x, y;
	BYTE *Ptr;

	min = 1000.0;
	max = -1000.0;
	// find post-processing normalization factors for image 
	for (y = 0; y < m_nRow2; y++) 
	{
		for (x = 0; x < m_nCol2; x++) 
		{
			if (pReal[y][x] < min)
				min = pReal[y][x];
			if (pReal[y][x] > max)
				max = pReal[y][x];
		}
	}
	//if (min > 0.0)
	//	min = 0.0;
	if (max != min)
		norm = max0 / (max - min);
	else
		norm = 1.0;
	
	Ptr = Addr;
	for (y = 0; y < m_nRow2; y++)
	{
		for (x = 0; x < m_nCol2; x++)
			*Ptr++ = (BYTE) ((pReal[y][x] - min) * norm + 0.5);
	}
}

///////////////////////////////////////////////////////////////////////////////
// Calculate the power spectrum image
///////////////////////////////////////////////////////////////////////////////

void CUTSFFT::PowerSpectrumImage(BYTE *Addr, float **pReal, float **pImag)
{
	double min, max, norm, P, R, I, temp;
	long x, y;
	BYTE *Ptr;
	FLOAT_PTR *PFImag, *PSImag;
	long nRowD2, nColD2;

	PFImag = AllocateFloatArray(m_nRow2, m_nCol2);
	PSImag = AllocateFloatArray(m_nRow2, m_nCol2);

	min = 1000.0;
	max = -1000.0;
	// find post-processing normalization factors for image 
	for (y = 0; y < m_nRow2; y++) 
	{
		for (x = 0; x < m_nCol2; x++) 
		{
			R = pReal[y][x];
			I = pImag[y][x];
			if (R != 0.0 || I != 0.0)
			{
				P = sqrt(R * R + I * I);
			
				if (P < min)
					min = P;
				if (P > max)
					max = P;

				PFImag[y][x] = (float) P;
			}
			else
				PFImag[y][x] = 0.0;
		}
	}

	if (max != min)
		norm = 255.0 / (max - min);
	else
		norm = 1.0;
	
	// scale, center origin at middle, and write output bytes 
	max = log10(max - min + 1.0);
	nColD2 = m_nCol2 / 2;
	nRowD2 = m_nRow2 / 2;
	for (y = 0; y < m_nRow2; y++)
	{
		for (x = 0; x < m_nCol2; x++) 
		{
			temp = log10 ((double) PFImag[y][x] - min + 1.0);
			PSImag[(y + nRowD2) % m_nRow2][(x + nColD2) % m_nCol2] = (unsigned char) (temp * 255.0 / max);
		}
	}	
	Ptr = Addr;
	for (y = 0; y < m_nRow2; y++)
	{
		for (x = 0; x < m_nCol2; x++)
			*Ptr++ = (BYTE) PSImag[y][x];
	}
	
	DeallocateFloatArray(PSImag, m_nRow2);
	DeallocateFloatArray(PFImag, m_nRow2);
}

void CUTSFFT::ButterWorth(FilterType passType, double f1, double f2, double order)
{
	long x, y, nRowD2, nColD2,    // half of row/column  
		nRowM1, nColM1;           // row/col minus 1  
	double bigger;                // passband fraction fct of bigger axis  
	double *fltr;                 // filter vector 
	long maxRad;                  // max. sampling radius  
	long dist;                    // euclidean distance to point  
	double tempL, tempH;
	long i;
	
	// allocate filter vector -- radius from (u,v)=(0,0) to corners of freq plot  
	nRowD2 = m_nRow / 2;
	nColD2 = m_nCol / 2;
	bigger = (double) (nRowD2 > nColD2) ? nRowD2 : nColD2;
	maxRad = (long) sqrt ((double) (nRowD2 * nRowD2 + nColD2 * nColD2)) + 1;
	fltr = new double[maxRad];
	
	// determine filter vector coefficients for chosen passband type of filter  
	order *= 2.0;
	switch (passType) 
	{
	case LowPass:                      // low-pass  
		f1 *= bigger;
		for (i = 0; i < maxRad; i++) 
			fltr[i] = 1.0 / (1.0 + 0.414 * pow (((double) i / f1), order));
		break;

	case HighPass:                      // high-pass  
		f1 *= bigger;
		for (i = 0; i < maxRad; i++)
			fltr[i] = 1.0 - 1.0 / (1.0 + 0.414 * pow (((double) i / f1), order));
		break;

	case BandPass:                      // band-pass  
		f1 *= bigger;
		f2 *= bigger;
		for (i = 0; i < maxRad; i++) 
		{
			tempL = 1.0 / (1.0 + 0.414 * pow (((double) i / f1), order));
			tempH = 1.0 - 1.0 / (1.0 + 0.414 * pow (((double) i / f2), order));
			fltr[i] = tempL * tempH;
		}
		break;

	case BandStop:                      // and band-stop  
		f1 *= bigger;
		f2 *= bigger;
		for (i = 0; i < maxRad; i++) 
		{
			tempL = 1.0 / (1.0 + 0.414 * pow (((double) i / f1), order));
			tempH = 1.0 - 1.0 / (1.0 + 0.414 * pow (((double) i / f2), order));
			fltr[i] = tempL + tempH;
		}
		break;

	default:
		break;
	}
	
	// perform filtering 
	nRowM1 = m_nRow - 1;
	nColM1 = m_nCol - 1;
	for (y = 0; y < nRowD2; y++) 
	{
		for (x = 0; x < nColD2; x++) 
		{
			dist = (long) (sqrt ((double) (y * y + x * x)) + 0.5);
					
			m_pReal[y][x] *= (float) fltr[dist];
			m_pReal[y][nColM1 - x] *= (float) fltr[dist];
			m_pReal[nRowM1 - y][x] *= (float) fltr[dist];
			m_pReal[nRowM1 - y][nColM1 - x] *= (float) fltr[dist];
			
			m_pImag[y][x] *= (float) fltr[dist];
			m_pImag[y][nColM1 - x] *= (float) fltr[dist];
			m_pImag[nRowM1 - y][x] *= (float) fltr[dist];
			m_pImag[nRowM1 - y][nColM1 - x] *= (float) fltr[dist];
		}
	}

	// Clear up
	delete fltr;
}

/*
 * fltrgaus()
 *   DESCRIPTION:
 *     fltrgaus performs Gaussian filtering in frequency domain
 *     with specified low & high pass bounds
 *   ARGUMENTS:
 *     imgReal(float **) pointer to real image array
 *     imgImag(float **) pointer to imaginary image array
 *     nRow, nCol(long) number of rows and columns for real and img arrays
 *     passType(short)  passType=0 low pass
 *                              passType=1 high pass
 *                              passType=2 band pass
 *                              passType=3 stop pass
 *     f1, f2(double) low, high pass frequency bounds
 *   RETURN VALUE:
 *     none
 */

void CUTSFFT::Gaussian(long nRow, long nCol, FilterType passType, double f1, double f2)
{
	long x, y, nRowD2, nColD2,    // half of row/column  
		nRowM1, nColM1;           // row/col minus 1  
	double bigger;                // passband fraction fct of bigger axis  
	double *fltr;                 // filter vector  
	double stdDevL, stdDevH;      // std. dev.s of low and high pass fltrs 
	long maxRad;                  // max. sampling radius  
	long dist;                    // euclidean distance to point  
	double tempL, tempH;
	long i;
	
	// allocate filter vector -- radius from (u,v)=(0,0) to corners of freq plot  
	nRowD2 = nRow / 2;
	nColD2 = nCol / 2;
	bigger = (double) (nRowD2 > nColD2) ? nRowD2 : nColD2;
	maxRad = (long) sqrt ((double) (nRowD2 * nRowD2 + nColD2 * nColD2)) + 1;
	fltr = new double[maxRad];
	
	// determine filter vector coefficients for chosen type of filter  
	switch (passType) 
	{
	case LowPass:                      // low-pass  
		stdDevL = f1 / ROOTLOG2 * bigger;
		for (i = 0; i < maxRad; i++)
			fltr[i] = exp (-(double) i * (double) i / (2.0 * stdDevL * stdDevL));
		break;

	case HighPass:                      // high-pass  
		stdDevH = f1 / ROOTLOG2 * bigger;
		for (i = 0; i < maxRad; i++)
			fltr[i] = 1.0 -
			exp (-(double) i * (double) i / (2.0 * stdDevH * stdDevH));
		break;

	case BandPass:                      // band-pass  
		stdDevH = f1 / ROOTLOG2 * bigger;
		stdDevL = f2 / ROOTLOG2 * bigger;
		for (i = 0; i < maxRad; i++) 
		{
			tempL = exp (-(double) i * (double) i / (2.0 * stdDevL * stdDevL));
			tempH = 1.0 -
				exp (-(double) i * (double) i / (2.0 * stdDevH * stdDevH));
			fltr[i] = tempL * tempH;
		}
		break;

	case BandStop:                      // and band-stop  
		stdDevL = f1 / ROOTLOG2 * bigger;
		stdDevH = f2 / ROOTLOG2 * bigger;
		for (i = 0; i < maxRad; i++) 
		{
			tempL = exp (-(double) i * (double) i / (2.0 * stdDevL * stdDevL));
			tempH = 1.0 -
				exp (-(double) i * (double) i / (2.0 * stdDevH * stdDevH));
			fltr[i] = tempL + tempH;
		}
		break;

	default:
		break;
	}
	
	// perform filtering 
	nRowM1 = nRow - 1;
	nColM1 = nCol - 1;
	for (y = 0; y < nRowD2; y++) 
	{
		for (x = 0; x < nColD2; x++) 
		{
			dist = (long) (sqrt ((double) (y * y + x * x)) + 0.5);
			
			m_pReal[y][x] *= (float) fltr[dist];
			m_pReal[y][nColM1 - x] *= (float) fltr[dist];
			m_pReal[nRowM1 - y][x] *= (float) fltr[dist];
			m_pReal[nRowM1 - y][nColM1 - x] *= (float) fltr[dist];
			
			m_pImag[y][x] *= (float) fltr[dist];
			m_pImag[y][nColM1 - x] *= (float) fltr[dist];
			m_pImag[nRowM1 - y][x] *= (float) fltr[dist];
			m_pImag[nRowM1 - y][nColM1 - x] *= (float) fltr[dist];
		}
	}

	delete fltr;
}

///////////////////////////////////////////////////////////////////////////////
// Calculate the power spectrum image
///////////////////////////////////////////////////////////////////////////////

BOOL CUTSFFT::PowerSpectrum(long SrcHandle, long DestHandle)
{
	long W, H, P;
	float max0, min0;             // initial max,min values of image 
	double temp;
	BYTE *SrcAddr, *DestAddr;
	
	// Get info about the source and dest images
	pUTSMemRect->GetExtendedInfo(SrcHandle, &m_nRow, &m_nCol, &P, &SrcAddr);
	pUTSMemRect->GetExtendedInfo(DestHandle, &W, &H, &P, &DestAddr);

	// Images not the same size
	if (W != m_nRow || H != m_nCol)
		return FALSE;
	
	// check for image sidelengths powers of 2 
	m_nRow2 = m_nRow;
	temp = log2 ((double) m_nRow);
	if ((temp - (long) temp) != 0.0)
		m_nRow2 = (long) exp2 ((double) ((long) temp));
	m_nCol2 = m_nCol;
	temp = log2 ((double) m_nCol);
	if ((temp - (long) temp) != 0.0)
		m_nCol2 = (long) exp2 ((double) ((long) temp));
		
	m_pReal = AllocateFloatArray(m_nRow2, m_nCol2);
	m_pImag = AllocateFloatArray(m_nRow2, m_nCol2);
	
	// Convert image to its real representation
    Image2Real(SrcAddr, m_pReal, m_pImag, &min0, &max0);
	
	// perform 2-dimensional FFT 
	fft2D(m_pReal, m_pImag, m_nRow2, m_nCol2, -1);
	
	// Convert the real image to a normal one
	PowerSpectrumImage(DestAddr, m_pReal, m_pImag);
	
	DeallocateFloatArray(m_pReal, m_nRow2);
	DeallocateFloatArray(m_pImag, m_nRow2);
	
	return TRUE;
}

BOOL CUTSFFT::GaussFilter(long SrcHandle, long DestHandle, FilterType FiltType, float Freq1, float Freq2)
{
	long W, H, P;
	float max0, min0;             // initial max,min values of image 
	double temp;
	BYTE *SrcAddr, *DestAddr;
	
	// Get info about the source and dest images
	pUTSMemRect->GetExtendedInfo(SrcHandle, &m_nRow, &m_nCol, &P, &SrcAddr);
	pUTSMemRect->GetExtendedInfo(DestHandle, &W, &H, &P, &DestAddr);

	// Images not the same size
	if (W != m_nRow || H != m_nCol)
		return FALSE;
	
	// check for image sidelengths powers of 2 
	m_nRow2 = m_nRow;
	temp = log2 ((double) m_nRow);
	if ((temp - (long) temp) != 0.0)
		m_nRow2 = (long) exp2 ((double) ((long) temp));
	m_nCol2 = m_nCol;
	temp = log2 ((double) m_nCol);
	if ((temp - (long) temp) != 0.0)
		m_nCol2 = (long) exp2 ((double) ((long) temp));
		
	m_pReal = AllocateFloatArray(m_nRow2, m_nCol2);
	m_pImag = AllocateFloatArray(m_nRow2, m_nCol2);
	
	// Convert image to its real representation
    Image2Real(SrcAddr, m_pReal, m_pImag, &min0, &max0);
	
	// perform 2-dimensional FFT 
	fft2D(m_pReal, m_pImag, m_nRow2, m_nCol2, -1);
	
	// perform filtering on frequency-domain image 
    Gaussian(m_nRow2, m_nCol2, FiltType, Freq1, Freq2);
	
	// perform inverse FFT 
	fft2D(m_pReal, m_pImag, m_nRow2, m_nCol2, 1);
	
	// Convert the real image to a normal one
	Real2Image(DestAddr, m_pReal, max0);
	
	DeallocateFloatArray(m_pReal, m_nRow2);
	DeallocateFloatArray(m_pImag, m_nRow2);
	
	return TRUE;
}

BOOL CUTSFFT::ButterworthFilter(long SrcHandle, long DestHandle, FilterType FiltType, float Freq1, float Freq2, double order)
{
	long W, H, P;
	float max0, min0;             // initial max,min values of image  
	double temp;
	BYTE *SrcAddr, *DestAddr;
	
	// Get info about the source and dest images
	pUTSMemRect->GetExtendedInfo(SrcHandle, &m_nRow, &m_nCol, &P, &SrcAddr);
	pUTSMemRect->GetExtendedInfo(DestHandle, &W, &H, &P, &DestAddr);

	// Images not the same size
	if (W != m_nRow || H != m_nCol)
		return FALSE;
	
	// check for image sidelengths powers of 2 
	m_nRow2 = m_nRow;
	temp = log2 ((double) m_nRow);
	if ((temp - (long) temp) != 0.0)
		m_nRow2 = (long) exp2 ((double) ((long) temp));
	m_nCol2 = m_nCol;
	temp = log2 ((double) m_nCol);
	if ((temp - (long) temp) != 0.0)
		m_nCol2 = (long) exp2 ((double) ((long) temp));
		
	m_pReal = AllocateFloatArray(m_nRow2, m_nCol2);
	m_pImag = AllocateFloatArray(m_nRow2, m_nCol2);
	
	// Convert image to its real representation
    Image2Real(SrcAddr, m_pReal, m_pImag, &min0, &max0);
	
	// perform 2-dimensional FFT 
	fft2D(m_pReal, m_pImag, m_nRow2, m_nCol2, -1);
	
	// perform filtering on frequency-domain image 
    Gaussian(m_nRow2, m_nCol2, FiltType, Freq1, Freq2);
	
	// perform inverse FFT 
	fft2D(m_pReal, m_pImag, m_nRow2, m_nCol2, 1);
	
	// Convert the real image to a normal one
	Real2Image(DestAddr, m_pReal, max0);
	
	DeallocateFloatArray(m_pReal, m_nRow2);
	DeallocateFloatArray(m_pImag, m_nRow2);
	
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
// Calculate the power spectrum image
///////////////////////////////////////////////////////////////////////////////

BOOL CUTSFFT::XYShift(long Image1, long Image2, long *xShift, long *yShift)
{
	long W, H, P;
	float max01, max02, min0;             // initial max,min values of image 
	double temp;
	BYTE *Addr1, *Addr2;
	FLOAT_PTR *pReal1, *pImag1, *pReal2, *pImag2;
	
	// Get info about the source and dest images
	pUTSMemRect->GetExtendedInfo(Image1, &m_nRow, &m_nCol, &P, &Addr1);
	pUTSMemRect->GetExtendedInfo(Image2, &W, &H, &P, &Addr2);

	// Images not the same size
	if (W != m_nRow || H != m_nCol)
		return FALSE;
	
	// check for image sidelengths powers of 2 
	m_nRow2 = m_nRow;
	temp = log2 ((double) m_nRow);
	if ((temp - (long) temp) != 0.0)
		m_nRow2 = (long) exp2 ((double) ((long) temp));
	m_nCol2 = m_nCol;
	temp = log2 ((double) m_nCol);
	if ((temp - (long) temp) != 0.0)
		m_nCol2 = (long) exp2 ((double) ((long) temp));
		
	pReal1 = AllocateFloatArray(m_nRow2, m_nCol2);
	pImag1 = AllocateFloatArray(m_nRow2, m_nCol2);
	pReal2 = AllocateFloatArray(m_nRow2, m_nCol2);
	pImag2 = AllocateFloatArray(m_nRow2, m_nCol2);

	// Convert image to its real representation
    Image2Real(Addr1, pReal1, pImag1, &min0, &max01);
	Image2Real(Addr2, pReal2, pImag2, &min0, &max02);

	// perform 2-dimensional FFT 
	fft2D(pReal1, pImag1, m_nRow2, m_nCol2, -1);
	fft2D(pReal2, pImag2, m_nRow2, m_nCol2, -1);
	
	
	
	DeallocateFloatArray(pReal1, m_nRow2);
	DeallocateFloatArray(pImag1, m_nRow2);
	DeallocateFloatArray(pReal2, m_nRow2);
	DeallocateFloatArray(pImag2, m_nRow2);
	
	return TRUE;
}
