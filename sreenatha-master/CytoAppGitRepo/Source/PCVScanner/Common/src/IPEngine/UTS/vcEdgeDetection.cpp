///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
// J. R. Parker
// Laboratory for Computer Vision
// University of Calgary
// Calgary, Alberta, Canada
// Algorithms for image processing and computer vision
//
// 8 bit images only
// Modified and completed By HB 11/30/2004
///////////////////////////////////////////////////////////////////////////////////////////////
#include <math.h>
#include "stdafx.h"
#include "vcedgedetection.h"

#define ORI_SCALE 40.0
#define MAG_SCALE 20.0
#define MAX_MASK_SIZE 20

#define OUTLINE 25

CEdgeDetection::CEdgeDetection()
{

}

CEdgeDetection::~CEdgeDetection()
{

}

// Create a new image
EDGEIMAGE *CEdgeDetection::newimage (int nr, int nc)
{
	int i, j;
	EDGEIMAGE *x=NULL;

	if (nr < 0 || nc < 0)
		return NULL;
	
	if ( !(x = new EDGEIMAGE) )
		return NULL;

	x->nr = nr;       
	x->nc = nc;
	x->oi = x->oj = 0;

	if ( !(x->data = new BYTE*[nr]) ) {
		delete x;
		return NULL;
	}
	
	for (i=0; i<nr; i++) {
		if ( !(x->data[i] = new BYTE[nc]) ) {
			for (int j=0; j<i; j++)
				delete [] x->data[j];
			delete [] x->data;
			delete x;
			return NULL;
		}
	}

    for (i=0; i<x->nr; i++)
		for (j=0; j<x->nc; j++)
			x->data[i][j] = 0;

	return x;
}

// free a image
void CEdgeDetection::freeimage (EDGEIMAGE *z)
{
	int i;
	if (z != 0) {
	   for (i=0; i<z->nr; i++)
	      delete [] z->data[i];
	   delete [] z->data;
	   delete z;
	}
}

float **CEdgeDetection::f2d (int nr, int nc)
{
	float **x;
	int i, j;

	if ( !(x=new float*[nr]) )
		return NULL;

	for (i=0; i<nr; i++)
	{  
	  if ( !(x[i]=new float[nc]) ) {
		for ( j=0; j<i; j++ )
			delete [] x[j];
		delete [] x;
		return NULL;
	  }
	}

	for (i=0; i<nr; i++)
		for (j=0; j<nc; j++)
			x[i][j] = 0;
	return x;
}

void CEdgeDetection::free2d(float **x, int nr)
{
	if ( x ) {
		for (int i=0; i<nr; i++)
			if ( x[i] )
				delete [] x[i];
		delete [] x;
	}
}

// Gaussian
float CEdgeDetection::gauss(float x, float sigma)
{
    float xx;

    if (sigma == 0) return 0.0;
    xx = (float)exp((double) ((-x*x)/(2*sigma*sigma)));
    return xx;
}

float CEdgeDetection::meanGauss (float x, float sigma)
{
	double z;

	z = (gauss(x,sigma)+gauss(x+0.5,sigma)+gauss(x-0.5,sigma))/3.0;
	z = z/(3.1415926*2.0*sigma*sigma);
	return (float)z;
}

// First derivative of Gaussian
float CEdgeDetection::dGauss (float x, float sigma)
{
	return -x/(sigma*sigma) * gauss(x, sigma);
}

float CEdgeDetection::norm (float x, float y)
{
	return (float) sqrt ( (double)(x*x + y*y) );
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Marr/Hildreth edge detection

CMarrEdgeDetector::CMarrEdgeDetector()
{

}

CMarrEdgeDetector::~CMarrEdgeDetector()
{

}

int CMarrEdgeDetector::EdgeDetection(BYTE *image, int imwidth, int imheight, float sigma)
{
	int i,j;
	float s;
	EDGEIMAGE *im1=NULL, *im2=NULL;

	s = sigma;

	if ( (im1=newimage(imheight, imwidth)) == NULL )
		return -1;
	long index;
	for (i=0; i<imheight; i++) {
		for (j=0; j<imwidth; j++) {
			index = i*imwidth+j;
			im1->data[i][j] = image[index];
		}
	}

	if ( (im2=newimage(im1->nr, im1->nc)) == NULL ) {
		freeimage(im1);
		return -1;
	}
	for (i=0; i<im1->nr; i++)
	  for (j=0; j<im1->nc; j++)
	    im2->data[i][j] = im1->data[i][j];

	// Apply the filter
	if ( marr (s-0.8, im1) == -1 ) {
		freeimage(im2);
		freeimage(im1);
		return -1;
	}
	if ( marr (s+0.8, im2) == -1 ) {
		freeimage(im2);
		freeimage(im1);
		return -1;
	}

	for (i=0; i<im1->nr; i++)
	  for (j=0; j<im1->nc; j++)
	    if (im1->data[i][j] > 0 && im2->data[i][j] > 0)
			im1->data[i][j] = 255;
	    else 
			im1->data[i][j] = 0;

	for (i=0; i<imheight; i++) {
		for (j=0; j<imwidth; j++) {
			index = i*imwidth+j;
			image[index] = im1->data[i][j];
		}
	}

	freeimage(im2);
	freeimage(im1);

	return 0;
}

float CMarrEdgeDetector::distance (float a, float b, float c, float d)
{
	return norm ( (a-c), (b-d) );
}

int CMarrEdgeDetector::marr (float s, EDGEIMAGE *im)
{
	int width;
	float **smx;
	int i,j,n;
	float **lgau;

	// Create a Gaussian and a derivative of Gaussian filter mask
	width = (int)(3.35*s + 0.33);
	n = width+width + 1;
	if ( (lgau=f2d(n, n)) == NULL )
		return -1;
	for (i=0; i<n; i++)
	  for (j=0; j<n; j++)
	    lgau[i][j] = LoG (distance ((float)i, (float)j, (float)width, (float)width), s);

	// Convolution of source image with a Gaussian in X and Y directions
	if ( (smx=f2d(im->nr, im->nc)) == NULL ) {
		free2d(lgau, n);
		return -1;
	}

	convolution (im, lgau, n, n, smx, im->nr, im->nc);

	// Locate the zero crossings
	zero_cross (smx, im);

	// Clear the boundary
	for (i=0; i<im->nr; i++)
	{
	  for (j=0; j<=width; j++) 
		im->data[i][j] = 0;
	  for (j=im->nc-width-1; j<im->nc; j++)
		im->data[i][j] = 0;
	}
	for (j=0; j<im->nc; j++)
	{
	  for (i=0; i<= width; i++) 
		im->data[i][j] = 0;
	  for (i=im->nr-width-1; i<im->nr; i++)
		im->data[i][j] = 0;
	}

	free2d(smx, im->nr);
	free2d(lgau, n);

	return 0;
}

float CMarrEdgeDetector::LoG (float x, float sigma)
{
	float x1;

	x1 = gauss (x, sigma);
	return (x*x-2*sigma*sigma)/(sigma*sigma*sigma*sigma) * x1;
}

void CMarrEdgeDetector::convolution (EDGEIMAGE *im, float **mask, int nr, int nc, float **res, int NR, int NC)
{
	int i,j,ii,jj, n, m, k, kk;
	float x;

	k = nr/2; kk = nc/2;
	for (i=0; i<NR; i++)
	  for (j=0; j<NC; j++)
	  {
	    x = 0.0;
	    for (ii=0; ii<nr; ii++)
	    {
	      n = i - k + ii;
	      if (n<0 || n>=NR) continue;
	      for (jj=0; jj<nc; jj++)
	      {
			m = j - kk + jj;
			if (m<0 || m>=NC) continue;
			x += mask[ii][jj] * (float)(im->data[n][m]);
	      }
	    }
	    res[i][j] = x;
	  }
}

void CMarrEdgeDetector::zero_cross (float **lapim, EDGEIMAGE *im)
{
	int i,j;

	for (i=1; i<im->nr-1; i++)
	  for (j=1; j<im->nc-1; j++)
	  {
	    im->data[i][j] = 0;
	    if(lapim[i-1][j]*lapim[i+1][j]<0) {im->data[i][j]=255; continue;}
	    if(lapim[i][j-1]*lapim[i][j+1]<0) {im->data[i][j]=255; continue;}
	    if(lapim[i+1][j-1]*lapim[i-1][j+1]<0) {im->data[i][j]=255; continue;}
	    if(lapim[i-1][j-1]*lapim[i+1][j+1]<0) {im->data[i][j]=255; continue;}
	  }
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Canny edge detection

CCannyEdgeDetector::CCannyEdgeDetector()
{
	WIDTH = 0;
}

CCannyEdgeDetector::~CCannyEdgeDetector()
{

}

int CCannyEdgeDetector::EdgeDetection(BYTE *image, int imwidth, int imheight, float sigma, int low, int high)
{
	int i,j;
	float s=sigma;
	EDGEIMAGE *im, *magim, *oriim;

	if ( (im=newimage(imheight, imwidth)) == NULL )
		return -1;
	long index;
	for (i=0; i<imheight; i++) {
		for (j=0; j<imwidth; j++) {
			index = i*imwidth+j;
			im->data[i][j] = image[index];
		}
	}

	// Create local image space
	if ( (magim=newimage(im->nr, im->nc)) == NULL ) {
		freeimage(im);
		return -1;
	}
	if ( (oriim=newimage(im->nr, im->nc)) == NULL ) {
		freeimage(magim);
		freeimage(im);
		return -1;
	}

	// Apply the filter
	if ( canny (s, im, magim, oriim) == -1 ) {
		freeimage(oriim);
		freeimage(magim);
		freeimage(im);
		return -1;
	}

	// Hysteresis thresholding of edge pixels
	hysteresis (high, low, im, magim, oriim);

	for (i=0; i<WIDTH; i++)
	  for (j=0; j<im->nc; j++)
	    im->data[i][j] = 255;

	for (i=im->nr-1; i>im->nr-1-WIDTH; i--)
	  for (j=0; j<im->nc; j++)
	    im->data[i][j] = 0;

	for (i=0; i<im->nr; i++)
	  for (j=0; j<WIDTH; j++)
	    im->data[i][j] = 0;

	for (i=0; i<im->nr; i++)
	  for (j=im->nc-WIDTH-1; j<im->nc; j++)
	    im->data[i][j] = 0;

	for (i=0; i<imheight; i++) {
		for (j=0; j<imwidth; j++) {
			index = i*imwidth+j;
			image[index] = im->data[i][j];
		}
	}

	freeimage(oriim);
	freeimage(magim);
	freeimage(im);

	return 0;
}


int CCannyEdgeDetector::canny (float s, EDGEIMAGE *im, EDGEIMAGE *mag, EDGEIMAGE *ori)
{
	int width;
	float **smx,**smy;
	float **dx,**dy;
	int i,j,n;
	float gau[MAX_MASK_SIZE], dgau[MAX_MASK_SIZE], z;

	// Create a Gaussian and a derivative of Gaussian filter mask
	for(i=0; i<MAX_MASK_SIZE; i++)
	{
	  gau[i] = meanGauss ((float)i, s);
	  if (gau[i] < 0.005)
	  {
		width = i;
		break;
	  }
	  dgau[i] = dGauss ((float)i, s);
	}

	n = width+width + 1;
	WIDTH = width/2;

	if ( (smx=f2d(im->nr, im->nc)) == NULL )
		return -1;
	if ( (smy=f2d(im->nr, im->nc)) == NULL ) {
		free2d(smx, im->nr);
		return -1;
	}

	// Convolution of source image with a Gaussian in X and Y directions
	seperable_convolution (im, gau, width, smx, smy);

	// Now convolve smoothed data with a derivative
	if ( (dx=f2d(im->nr, im->nc)) == NULL ) {
		free2d(smx, im->nr);
		free2d(smy, im->nr);
		return -1;
	}
	dxy_seperable_convolution (smx, im->nr, im->nc, dgau, width, dx, 1);
	free2d(smx, im->nr);

	if ( (dy=f2d(im->nr, im->nc)) == NULL ) {
		free2d(dx, im->nr);
		free2d(smy, im->nr);
		return -1;
	}
	dxy_seperable_convolution (smy, im->nr, im->nc, dgau, width, dy, 0);
	free2d(smy, im->nr);

	// Create an image of the norm of dx,dy
	for (i=0; i<im->nr; i++)
	  for (j=0; j<im->nc; j++)
	  {
	      z = norm (dx[i][j], dy[i][j]);
	      mag->data[i][j] = (BYTE)(z*MAG_SCALE);
	  }

	// Non-maximum suppression - edge pixels should be a local max
	nonmax_suppress (dx, dy, (int)im->nr, (int)im->nc, mag, ori);

	free2d(dy, im->nr);
	free2d(dx, im->nr);

	return 0;
}

// HYSTERESIS thersholding of edge pixels. Starting at pixels with a
// value greater than the HIGH threshold, trace a connected sequence
// of pixels that have a value greater than the LOW threhsold.
void CCannyEdgeDetector::hysteresis (int high, int low, EDGEIMAGE *im, EDGEIMAGE *mag, EDGEIMAGE *oriim)
{
	int i,j;

	for (i=0; i<im->nr; i++)
	  for (j=0; j<im->nc; j++)
	    im->data[i][j] = 0;

	if (high<low)
	  estimate_thresh (mag, &high, &low);
	
	// For each edge with a magnitude above the high threshold, begin
	// tracing edge pixels that are above the low threshold.
	for (i=0; i<im->nr; i++)
	  for (j=0; j<im->nc; j++)
	    if (mag->data[i][j] >= high)
	      trace (i, j, low, im, mag, oriim);

	// Make the edge black (to be the same as the other methods)
	for (i=0; i<im->nr; i++)
	  for (j=0; j<im->nc; j++)
	    if (im->data[i][j] == 0) 
			im->data[i][j] = 0;
	    else 
			im->data[i][j] = 255;
}

// Check that a pixel index is in range. Return TRUE(1) if so.     */
int CCannyEdgeDetector::range(EDGEIMAGE *im, int i, int j)
{
	if ((i<0) || (i>=im->nr)) return 0;
	if ((j<0) || (j>=im->nc)) return 0;
	return 1;
}

// TRACE - recursively trace edge pixels that have a threshold > the low
// edge threshold, continuing from the pixel at (i,j).
int CCannyEdgeDetector::trace(int i, int j, int low, EDGEIMAGE *im, EDGEIMAGE *mag, EDGEIMAGE *ori)
{
	int n,m;
	char flag = 0;

	if (im->data[i][j] == 0)
	{
	  im->data[i][j] = 255;
	  flag=0;
	  for (n= -1; n<=1; n++)
	  {
	    for(m= -1; m<=1; m++)
	    {
	      if (i==0 && m==0) continue;
	      if (range(mag, i+n, j+m) && mag->data[i+n][j+m] >= low)
			if (trace(i+n, j+m, low, im, mag, ori))
			{
				flag=1;
				break;
			}
	    }
	    if (flag) break;
	  }
	  return 1;
	}
	return 0;
}

void CCannyEdgeDetector::seperable_convolution(EDGEIMAGE *im, float *gau, int width, float **smx, float **smy)
{
	int i,j,k, I1, I2, nr, nc;
	float x, y;

	nr = im->nr;
	nc = im->nc;

	for (i=0; i<nr; i++)
	  for (j=0; j<nc; j++)
	  {
	    x = gau[0] * im->data[i][j]; y = gau[0] * im->data[i][j];
	    for (k=1; k<width; k++)
	    {
	      I1 = (i+k)%nr; I2 = (i-k+nr)%nr;
	      y += gau[k]*im->data[I1][j] + gau[k]*im->data[I2][j];
	      I1 = (j+k)%nc; I2 = (j-k+nc)%nc;
	      x += gau[k]*im->data[i][I1] + gau[k]*im->data[i][I2];
	    }
	    smx[i][j] = x; smy[i][j] = y;
	  }
}

void CCannyEdgeDetector::dxy_seperable_convolution (float** im, int nr, int nc,  float *gau, 
													int width, float **sm, int which)
{
	int i,j,k, I1, I2;
	float x;

	for (i=0; i<nr; i++)
	  for (j=0; j<nc; j++)
	  {
	    x = 0.0;
	    for (k=1; k<width; k++)
	    {
	      if (which == 0)
	      {
			I1 = (i+k)%nr; I2 = (i-k+nr)%nr;
			x += -gau[k]*im[I1][j] + gau[k]*im[I2][j];
	      }
	      else
	      {
			I1 = (j+k)%nc; I2 = (j-k+nc)%nc;
			x += -gau[k]*im[i][I1] + gau[k]*im[i][I2];
	      }
	    }
	    sm[i][j] = x;
	  }
}

void CCannyEdgeDetector::nonmax_suppress (float **dx, float **dy, int nr, int nc, EDGEIMAGE *mag, EDGEIMAGE *ori)
{
	int i,j;
	float xx, yy, g2, g1, g3, g4, g, xc, yc;

	for (i=1; i<mag->nr-1; i++)
	{
	  for (j=1; j<mag->nc-1; j++)
	  {
	    mag->data[i][j] = 0;

		// Treat the x and y derivatives as components of a vector
	    xc = dx[i][j];
	    yc = dy[i][j];
	    if ( fabs(xc)<0.01 && fabs(yc)<0.01 ) 
			continue;

	    g  = norm (xc, yc);

		// Follow the gradient direction, as indicated by the direction of
		// the vector (xc, yc); retain pixels that are a local maximum.
	    if (fabs(yc) > fabs(xc))
	    {
			// The Y component is biggest, so gradient direction is basically UP/DOWN
	      xx = fabs(xc)/fabs(yc);
	      yy = 1.0;

	      g2 = norm (dx[i-1][j], dy[i-1][j]);
	      g4 = norm (dx[i+1][j], dy[i+1][j]);
	      if (xc*yc > 0.0)
	      {
			g3 = norm (dx[i+1][j+1], dy[i+1][j+1]);
			g1 = norm (dx[i-1][j-1], dy[i-1][j-1]);
	      } else
	      {
			g3 = norm (dx[i+1][j-1], dy[i+1][j-1]);
			g1 = norm (dx[i-1][j+1], dy[i-1][j+1]);
	      }
	    } else
	    {
			// The X component is biggest, so gradient direction is basically LEFT/RIGHT
	      xx = fabs(yc)/fabs(xc);
	      yy = 1.0;

	      g2 = norm (dx[i][j+1], dy[i][j+1]);
	      g4 = norm (dx[i][j-1], dy[i][j-1]);
	      if (xc*yc > 0.0)
	      {
			g3 = norm (dx[i-1][j-1], dy[i-1][j-1]);
			g1 = norm (dx[i+1][j+1], dy[i+1][j+1]);
	      }
	      else
	      {
			g1 = norm (dx[i-1][j+1], dy[i-1][j+1]);
			g3 = norm (dx[i+1][j-1], dy[i+1][j-1]);
	      }
	    }

		// Compute the interpolated value of the gradient magnitude
	    if ( (g > (xx*g1 + (yy-xx)*g2)) && (g > (xx*g3 + (yy-xx)*g4)) )
	    {
	      if (g*MAG_SCALE <= 255)
			mag->data[i][j] = (unsigned char)(g*MAG_SCALE);
	      else
			mag->data[i][j] = 255;
	      ori->data[i][j] = (BYTE)(atan2(yc, xc)*ORI_SCALE);
	    } else
	    {
			mag->data[i][j] = 0;
			ori->data[i][j] = 0;
	    }

	  }
	}
}

void CCannyEdgeDetector::estimate_thresh(EDGEIMAGE *mag, int *hi, int *low)
{
	int i,j,k, hist[256], count;

	// Build a histogram of the magnitude image.
	for (k=0; k<256; k++) 
		hist[k] = 0;

	for (i=WIDTH; i<mag->nr-WIDTH; i++)
	  for (j=WIDTH; j<mag->nc-WIDTH; j++)
	    hist[mag->data[i][j]]++;

	// The high threshold should be > 80 or 90% of the pixels 
	j = mag->nr;
	if (j<mag->nc) j = mag->nc;
	j = (int)(0.9*j);
	k = 255;

	count = hist[255];
	while (count < j)
	{
	  k--;
	  if (k<0) break;
	  count += hist[k];
	}
	*hi = k;

	i=0;
	while (hist[i]==0) i++;

	*low = (int)((*hi+i)/2.0);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Shen-Casten edge detection

CShenCastenEdgeDetector::CShenCastenEdgeDetector()
{
	b = 0.9;			// smoothing factor 0 < b < 1
	low_thresh=20;
	high_thresh=22;		// threshold for hysteresis
	ratio = 0.99;
	window_size = 7;
	do_hysteresis = 1;
}

CShenCastenEdgeDetector::~CShenCastenEdgeDetector()
{

}

int CShenCastenEdgeDetector::EdgeDetection(BYTE *image, int imwidth, int imheight, float sigma, int low, int high, int ws, int tf)
{
	int i,j;
	EDGEIMAGE *im, *res;

	b = sigma;
	if (b<0)	b = 0;
	if (b>1.0)	b = 1.0;
	window_size = ws;
	thinFactor = tf;
	low_thresh = low;
	high_thresh = high;

	if ( (im=newimage(imheight, imwidth)) == NULL )
		return -1;
	long index;
	for (i=0; i<imheight; i++) {
		for (j=0; j<imwidth; j++) {
			index = i*imwidth+j;
			im->data[i][j] = image[index];
		}
	}

	if ( (im=embed(im, OUTLINE)) == NULL ) {
		freeimage(im);
		return -1;
	}

	if ( (res=newimage(im->nr, im->nc)) == NULL ) {
		freeimage(im);
		return -1;
	}
	
	if ( shen(im, res) == -1 ) {
		freeimage(res);
		freeimage(im);
		return -1;
	}
	freeimage(im);

	if ( (res=debed(res, OUTLINE)) == NULL ) {
		freeimage(res);
		return -1;
	}

	for (i=0; i<imheight; i++) {
		for (j=0; j<imwidth; j++) {
			index = i*imwidth+j;
			image[index] = ~res->data[i][j]; 
		}
	}

	freeimage(res);

	return 0;
}

int CShenCastenEdgeDetector::shen(EDGEIMAGE *im, EDGEIMAGE *res)
{
	register int i,j;
	float **buffer;	
	float **smoothed_buffer;
	EDGEIMAGE *bli_buffer;
   
	// Convert the input image to floating point
	if ( (buffer=f2d(im->nr, im->nc)) == NULL )
		return -1;
	for (i=0; i<im->nr; i++)
	  for (j=0; j<im->nc; j++)
	    buffer[i][j] = (float)(im->data[i][j]);

	// Smooth input image using recursively implemented ISEF filter
	if ( (smoothed_buffer=f2d(im->nr, im->nc)) == NULL ) {
		free2d(buffer, im->nr);
		return -1;
	}
	if ( compute_ISEF(buffer, smoothed_buffer, im->nr, im->nc) == -1 ) {
		free2d(smoothed_buffer, im->nr);
		free2d(buffer, im->nr);
		return -1;
	}
      
	// Compute bli image band-limited laplacian image from smoothed image
	if ( (bli_buffer=compute_bli(smoothed_buffer, buffer, im->nr, im->nc)) == NULL ) {
		free2d(smoothed_buffer, im->nr);
		free2d(buffer, im->nr);
		return -1;
	}
      
	// Perform edge detection using bli and gradient thresholding
	locate_zero_crossings (buffer, smoothed_buffer, bli_buffer, im->nr, im->nc);
      
	free2d(smoothed_buffer, im->nr);
	freeimage(bli_buffer);
	
	threshold_edges (buffer, res, im->nr, im->nc);
	for (i=0; i<im->nr; i++)
	  for (j=0; j<im->nc; j++)
	    if (res->data[i][j] > 0) 
			res->data[i][j] = 0;
	     else 
			 res->data[i][j] = 255;
	
	free2d(buffer, im->nr);

	return 0;
}

//	Recursive filter realization of the ISEF (Shen and Castan CVIGP March 1992)
int CShenCastenEdgeDetector::compute_ISEF (float **x, float **y, int nrows, int ncols)
{
	float **A, **B;
   
	if ( (A=f2d(nrows, ncols)) == NULL )	// store causal component
		return -1;
	if ( (B=f2d(nrows, ncols)) == NULL ) {	// store anti-causal component
		free2d(A, nrows);
		return -1;
	}
   
	// first apply the filter in the vertical direcion (to the rows)
	apply_ISEF_vertical (x, y, A, B, nrows, ncols);
   
	// now apply the filter in the horizontal direction (to the columns) and
	// apply this filter to the results of the previous one
	apply_ISEF_horizontal (y, y, A, B, nrows, ncols);
   
	// free up the memory
	free2d(B, nrows);
	free2d(A, nrows);

	return 0;
}

void CShenCastenEdgeDetector::apply_ISEF_vertical(float **x, float **y, float **A, float **B, int nrows, int ncols)
{
	register int row, col;
	float b1, b2;
   
	b1 = (1.0 - b)/(1.0 + b);
	b2 = b*b1;
   
	// compute boundary conditions
	for (col=0; col<ncols; col++)
	{
		// boundary exists for 1st and last column
	   A[0][col] = b1 * x[0][col];	
	   B[nrows-1][col] = b2 * x[nrows-1][col];
	}
   
	// compute causal component
	for (row=1; row<nrows; row++)
	  for (col=0; col<ncols; col++)
	    A[row][col] = b1 * x[row][col] + b * A[row-1][col];

	// compute anti-causal component
	for (row=nrows-2; row>=0; row--)
	  for (col=0; col<ncols; col++)
	    B[row][col] = b2 * x[row][col] + b * B[row+1][col];

	// boundary case for computing output of first filter
	for (col=0; col<ncols-1; col++)
	  y[nrows-1][col] = A[nrows-1][col]; 

	// now compute the output of the first filter and store in y
	// this is the sum of the causal and anti-causal components
	for (row=0; row<nrows-2; row++)
	  for (col=0; col<ncols-1; col++)
	    y[row][col] = A[row][col] + B[row+1][col];
}  


void CShenCastenEdgeDetector::apply_ISEF_horizontal (float **x, float **y, float **A, float **B, int nrows, int ncols)
{
	register int row, col;
	float b1, b2;
   
	b1 = (1.0 - b)/(1.0 + b);
	b2 = b*b1;
   
	// compute boundary conditions
	for (row=0; row<nrows; row++)
	{
	   A[row][0] = b1 * x[row][0];
	   B[row][ncols-1] = b2 * x[row][ncols-1];
	}

	// compute causal component
	for (col=1; col<ncols; col++)
	  for (row=0; row<nrows; row++)
	    A[row][col] = b1 * x[row][col] + b * A[row][col-1];

	// compute anti-causal component
	for (col=ncols-2; col>=0; col--)
	  for (row=0; row<nrows;row++)
	    B[row][col] = b2 * x[row][col] + b * B[row][col+1];

	// boundary case for computing output of first filter  
	for (row=0; row<nrows; row++)
	  y[row][ncols-1] = A[row][ncols-1];

	// now compute the output of the second filter and store in y
	// this is the sum of the causal and anti-causal components
	for (row=0; row<nrows; row++)
	  for (col=0; col<ncols-1; col++)
	    y[row][col] = A[row][col] + B[row][col+1];
}  

// compute the band-limited laplacian of the input image
EDGEIMAGE *CShenCastenEdgeDetector::compute_bli (float **buff1, float **buff2, int nrows, int ncols)
{
	register int row, col;
	EDGEIMAGE *bli_buffer;
   
	if ( (bli_buffer=newimage(nrows, ncols)) == NULL )
		return NULL;
	for (row=0; row<nrows; row++)
	  for (col=0; col<ncols; col++)
	    bli_buffer->data[row][col] = 0;
   
	// The bli is computed by taking the difference between the smoothed image
	// and the original image.  In Shen and Castan's paper this is shown to
	// approximate the band-limited laplacian of the image.  The bli is then
	// made by setting all values in the bli to 1 where the laplacian is
	// positive and 0 otherwise.
	for (row=0; row<nrows; row++)
	  for (col=0; col<ncols; col++)
	  {
		if (row<OUTLINE || row >= nrows-OUTLINE || col<OUTLINE || col >= ncols-OUTLINE) 
			continue;
	    bli_buffer->data[row][col] = ((buff1[row][col] - buff2[row][col]) > 0.0);
	  }
	return bli_buffer;
}

void CShenCastenEdgeDetector::locate_zero_crossings (float **orig, float **smoothed, EDGEIMAGE *bli, int nrows, int ncols)
{
	register int row, col;
   
	for (row=0; row<nrows; row++)
	{
	   for (col=0; col<ncols; col++)
	   {
			// ignore pixels around the boundary of the image
			if (row<OUTLINE || row >= nrows-OUTLINE || col<OUTLINE || col >= ncols-OUTLINE)
				orig[row][col] = 0.0;
			// next check if pixel is a zero-crossing of the laplacian
			else if (is_candidate_edge (bli, smoothed, row, col))
			{
			// now do gradient thresholding
				float grad = compute_adaptive_gradient (bli, smoothed, row, col);
				orig[row][col] = grad;
			}
			else  
				orig[row][col] = 0.0;		    
	   }
	}
}

void CShenCastenEdgeDetector::threshold_edges (float **in, EDGEIMAGE *out, int nrows, int ncols)
{
	register int i, j;
   
	lap = in;
	edges = out;
	nr = nrows;
	nc = ncols;
   
	estimate_thresh (&low_thresh, &high_thresh, nr, nc);
	if (!do_hysteresis)
	  low_thresh = high_thresh;

	for (i=0; i<nrows; i++)
	  for (j=0; j<ncols; j++)
	    edges->data[i][j] = 0;
   
	for (i=0; i<nrows; i++)
	  for (j=0; j<ncols; j++)
	  {
		if (i<OUTLINE || i >= nrows-OUTLINE || j<OUTLINE || j >= ncols-OUTLINE) 
			continue;
		// only check a contour if it is above high_thresh
	    if ((lap[i][j]) > high_thresh) 
			// mark all connected points above low thresh
			mark_connected (i,j,0);	
	  }
   
	for (i=0; i<nrows; i++)	// erase all points which were 255
	  for (j=0; j<ncols; j++)
	    if (edges->data[i][j] == 255) 
			edges->data[i][j] = 0;
}

//	return true if it marked something
int CShenCastenEdgeDetector::mark_connected (int i, int j, int level)
{
	 int notChainEnd;
    
   // stop if you go off the edge of the image
	if (i >= nr || i < 0 || j >= nc || j < 0) 
		return 0;
   
   // stop if the point has already been visited
	if (edges->data[i][j] != 0) 
		return 0;	
   
   // stop when you hit an image boundary
	if (lap[i][j] == 0.0) 
		return 0;
   
	if ((lap[i][j]) > low_thresh)
	   edges->data[i][j] = 1;
	else
	   edges->data[i][j] = 255;
   
	notChainEnd =0;
    
	notChainEnd |= mark_connected(i  ,j+1, level+1);
	notChainEnd |= mark_connected(i  ,j-1, level+1);
	notChainEnd |= mark_connected(i+1,j+1, level+1);
	notChainEnd |= mark_connected(i+1,j  , level+1);
	notChainEnd |= mark_connected(i+1,j-1, level+1);
	notChainEnd |= mark_connected(i-1,j-1, level+1);
	notChainEnd |= mark_connected(i-1,j  , level+1);
	notChainEnd |= mark_connected(i-1,j+1, level+1);

	if (notChainEnd && ( level > 0 ) )
	{
		// do some contour thinning
		if ( thinFactor > 0 )
			if ( (level%thinFactor) != 0  )
			{
				// delete this point 
				edges->data[i][j] = 255;
			}
	}
    
	return 1;
}

// finds zero-crossings in laplacian (buff)  orig is the smoothed image
int CShenCastenEdgeDetector::is_candidate_edge (EDGEIMAGE *buff, float **orig, int row, int col)
{
// test for zero-crossings of laplacian then make sure that zero-crossing
// sign correspondence principle is satisfied.  i.e. a positive z-c must
// have a positive 1st derivative where positive z-c means the 2nd deriv
// goes from positive to negative as we pass through the step edge
   
	if (buff->data[row][col] == 1 && buff->data[row+1][col] == 0) // positive z-c
	{ 
	   if (orig[row+1][col] - orig[row-1][col] > 0) 
		   return 1;
	   else 
		   return 0;
	}
	else if (buff->data[row][col] == 1 && buff->data[row][col+1] == 0 ) // positive z-c
	{
	   if (orig[row][col+1] - orig[row][col-1] > 0) 
		   return 1;
	   else 
		   return 0;
	}
	else if ( buff->data[row][col] == 1 && buff->data[row-1][col] == 0) // negative z-c
	{
	   if (orig[row+1][col] - orig[row-1][col] < 0) 
		   return 1;
	   else 
		   return 0;
	}
	else if (buff->data[row][col] == 1 && buff->data[row][col-1] == 0 ) // negative z-c
	{
	   if (orig[row][col+1] - orig[row][col-1] < 0) 
		   return 1;
	   else 
		   return 0;
	}
	else			// not a z-c
	  return 0;
}

float CShenCastenEdgeDetector::compute_adaptive_gradient (EDGEIMAGE *BLI_buffer, float **orig_buffer, int row, int col)
{
	register int i, j;
	float sum_on, sum_off;
	float avg_on, avg_off;
	int num_on, num_off;
   
	sum_on = sum_off = 0.0;
	num_on = num_off = 0;
   
	for (i= (-window_size/2); i<=(window_size/2); i++)
	{
	   for (j=(-window_size/2); j<=(window_size/2); j++)
	   {
	     if (BLI_buffer->data[row+i][col+j])
	     {
	        sum_on += orig_buffer[row+i][col+j];
	        num_on++;
	     }
	     else
	     {
	        sum_off += orig_buffer[row+i][col+j];
	        num_off++;
	     }
	   }
	}
   
	if (sum_off) avg_off = sum_off / num_off;
	else avg_off = 0;
   
	if (sum_on) avg_on = sum_on / num_on;
	else avg_on = 0;
   
	return (avg_off - avg_on);
}

void CShenCastenEdgeDetector::estimate_thresh (double *low, double *hi, int nr, int nc)
{
	float vmax, vmin, scale, x;
	int i,j,k, hist[256], count;

	// Build a histogram of the Laplacian image.
	vmin = vmax = fabs((float)(lap[20][20]));
	for (i=0; i<nr; i++)
	  for (j=0; j<nc; j++)
	  {
            if (i<OUTLINE || i >= nr-OUTLINE || j<OUTLINE || j >= nc-OUTLINE)
				continue;
	    x = lap[i][j];
	    if (vmin > x) vmin = x;
	    if (vmax < x) vmax = x;
	  }
	for (k=0; k<256; k++) 
		hist[k] = 0;

	scale = 256.0/(vmax-vmin + 1);

	for (i=0; i<nr; i++)
	  for (j=0; j<nc; j++)
	  {
        if (i<OUTLINE || i >= nr-OUTLINE || j<OUTLINE || j >= nc-OUTLINE) 
			continue;
	    x = lap[i][j];
	    k = (int)((x - vmin)*scale);
	    hist[k] += 1;
	  }

	// The high threshold should be > 80 or 90% of the pixels
	k = 255;
	j = (int)(ratio*nr*nc);
	count = hist[255];
	while (count < j)
	{
	  k--;
	  if (k<0) break;
	  count += hist[k];
	}
	*hi = (double)k/scale   + vmin ;
	*low = (*hi)/2;
}

EDGEIMAGE *CShenCastenEdgeDetector::embed(EDGEIMAGE *im, int width)
{
	int i,j,I,J;
	EDGEIMAGE *tmpim;

	width += 2;
	if ( (tmpim=newimage(im->nr+width+width, im->nc+width+width)) == NULL )
		return NULL;
	for (i=0; i<tmpim->nr; i++)
	  for (j=0; j<tmpim->nc; j++)
	  {
	    I = (i-width+im->nr)%im->nr;
	    J = (j-width+im->nc)%im->nc;
	    tmpim->data[i][j] = im->data[I][J];
	  }

	freeimage(im);
	
	return tmpim;
}

EDGEIMAGE *CShenCastenEdgeDetector::debed(EDGEIMAGE *im, int width)
{
	int i,j;
	EDGEIMAGE *old;

	width +=2;
	if ( (old=newimage(im->nr-width-width, im->nc-width-width)) == NULL )
		return NULL;
	for (i=0; i<old->nr-1; i++)
	{
	  for (j=1; j<old->nc; j++)
	  {
	    old->data[i][j] = im->data[i+width][j+width];
	    old->data[old->nr-1][j] = 255;
	  }
	  old->data[i][0] = 255;
	}

	freeimage(im);

	return old;
}
