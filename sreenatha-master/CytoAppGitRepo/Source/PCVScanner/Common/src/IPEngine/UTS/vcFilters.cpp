///////////////////////////////////////////////////////////////////////////////////////////////
// Some Spatial Filter algorithms
// 8 bit images only
//
// Written By HB 10/20/2004
///////////////////////////////////////////////////////////////////////////////////////////////
#include <math.h>

#include "stdafx.h"
#include "vcfilters.h"

///////////////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////////////////////
CFilters::CFilters()
{

}

CFilters::~CFilters()
{

}

///////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 1995 SIUE - by Kun Luo. (Southern Illinois University @ Edwardsville)
//
// Permission to use, copy, modify, and distribute this software and its
// documentation for any purpose and without fee is hereby granted, provided
// that the above copyright notice appear in all copies and that both that
// copyright notice and this permission notice appear in supporting
// documentation.  This software is provided "as is" without express or
// implied warranty.
//
// Adapted and Modified By HB 10/20/2004
//
// Links edge points of a binary image into lines
///////////////////////////////////////////////////////////////////////////////////////////////
int CFilters::edge_link_filter(BYTE *image, int width, int height, int connection)
{
    BYTE *image_o;
    int x,y,u_x,u_y,d_x,d_y;
    int y_inc,x_inc,x_abs,y_abs,r_i,r_j,direction;
    float slope;
	long imgdim=width*height;

	if ( (image_o=new BYTE[imgdim]) == NULL )
		return -1;
	for ( long i=0; i<imgdim; i++ )
		image_o[i] = image[i];
 
	for (y=0;y<height;y++) {
	    for (x=0;x<width;x++) {
			if (image_o[y*width+x] == 255) {
				u_x = x - connection;
				u_y = y - connection;
				d_x = x + connection;
				d_y = y + connection;
				u_x =   (u_x >= 0) ? u_x : 0;
				u_y =   (u_y >= 0) ? u_y : 0;
				d_x = (d_x < width) ? d_x : width-1;
				d_y = (d_y < height) ? d_y : height-1;
				for (int j=u_y;j<=d_y;j++) {
					for (int i=u_x;i<=d_x;i++) {
						if(image[j*width+i] == 255 && !(i==x && j==y)) {
							x_inc = x - i;
							y_inc = y - j;
							x_abs = abs(x_inc);
							y_abs = abs(y_inc);
							if (x_abs > y_abs) {
								direction = (x_inc > 0)? 1 : -1;
								slope = (float)y_inc/x_inc;
								for (r_i=i;r_i!=x;r_i+=direction) {
									r_j=(int)(slope*(r_i-i)+j+0.5);
									image_o[r_j*width+r_i] = 255;
								}
							}
							else {
								direction = (y_inc > 0)? 1 : -1;
								slope = (float)x_inc/y_inc;
								for (r_j=j;r_j!=y;r_j+=direction) {
									r_i=(int)(slope*(r_j-j)+i+0.5);
									image_o[r_j*width+r_i] = 255;
								}
							}
						}
					}
				}
			}
		}
	}

	for ( long i=0; i<imgdim; i++ )
		image[i] = image_o[i];

	delete [] image_o;

    return 0;
}


int CFilters::hough_filter(BYTE *image, int width, int height, char *degree_string, int threshold, int connection)
{
	CHough hf;
	if ( hf.hough_filter(image, width, height, degree_string, threshold, connection) == -1 )
		return -1;

	return 0;
}


///////////////////////////////////////////////////////////////////////////////////////////////
CHough::CHough()
{

}

CHough::~CHough()
{

}

// Performs hough transformation for lines beetween 0 and num=180 degree
BYTE *CHough::hough_transform(BYTE *image, int width, int height, char *interpret,int num)
{
    BYTE *outImage, *out1Image;

    double angle;
    int x, y, i, j, arm_length;
	float arm;
    
    arm = (float)sqrt((float)(height*height+width*width) + 0.25f);
    arm_length = (int)arm;

	if ( (outImage=new BYTE[arm_length*180]) == NULL )
		return NULL;
	for ( long k=0; k<arm_length*180; k++ )
		outImage[k] = 0;

	if ( (out1Image=new BYTE[arm_length*180]) == NULL ) {
		delete [] outImage;
		return NULL;
	}
	for ( long k=0; k<arm_length*180; k++ )
		out1Image[k] = 0;

	double pi=3.14159265;
    for (y=0;y<height;y++) {
		for (x=0;x<width;x++){
			if (image[y*width+x] == 255) {
				for(i=0;i<180;i++) {
					if (i<num && interpret[i] == 0)
						continue;
					angle = (double)(i*pi/180.0);
					j = (int)((x*cos(angle) + y*sin(angle) + arm)/2.0 + 0.5);
					if (j>=0 && j < arm_length)
						outImage[j*180+i]++;
					
				}
			}
		}
	}
    	
	for (y=0; y<arm_length; y++)
		for (x=0; x<180; x++)
			if (outImage[y*180+x]/2 > 255)
				out1Image[y*180+x] = 255;
			else
				out1Image[y*180+x] = outImage[y*180+x]/2;

    delete [] outImage;

	/*
	BYTE min=255, max=0;
	for (long k=0; k<arm_length*180; k++) {
		if ( out1Image[k]<min )
			min = out1Image[k];
		if ( out1Image[k]>max )
			max = out1Image[k];
	}
	double diff=max-min;
	for (long k=0; k<arm_length*180; k++)
		out1Image[k] = (BYTE)((double)(out1Image[k]*255)/diff);
	*/

    return out1Image;
}

// Performs inverse hough transformation for lines beetween 0 and 180 degree
BYTE *CHough::hough_inverse(BYTE *image, int rows, int cols, int width, int height, int threshold)
{
    BYTE *outImage;
    double angle, arm, r_cos, r_sin, test_c, test_s;
    int x, y, i, j;

	if ( (outImage=new BYTE[width*height]) == NULL )
		return NULL;
	for ( long k=0; k<width*height; k++ )
		outImage[k] = 0;

	double pi=3.14159265;
   	for (y=0; y<2*rows; y++) {
		for (x=0; x<cols; x++) {
			if (image[(y/2)*cols+x] >= threshold) {
				angle = (double)(x*pi/180.0);
				arm = y - rows ;
				r_cos = cos(angle);
				r_sin = sin(angle);
				test_c = (r_cos > 0)? r_cos : -1*r_cos;
				test_s = (r_sin > 0)? r_sin : -1*r_sin;
				if (test_c < test_s) {
					for (i=0; i<width; i++) {
						j = (int)((arm - i*r_cos)/r_sin + 0.5);
						if (j>=0 && j<height)
							outImage[j*width+i] = 255;
					}
				}
				else {
					for (j=0; j<height; j++) {
						i = (int)((arm - j*r_sin)/r_cos + 0.5);
						if (i>=0 && i<width)
							outImage[j*width+i] = 255;
					}
				}
			}
		}
	}

    return outImage;
}

// Performs hough filtering for lines beetween 0 and 180 degree
int CHough::hough_filter(BYTE *image, int width, int height, char *degree_string, int threshold, int connection)
{
    BYTE *tempImage, *tempImage1, *tempImage2, *houghImage, *hImage;
    char *interpret;
    BYTE *image_i, *image_o, *image_t;
    int x, y, j, arm_length, flag;
    float  r_cos, r_sin, test_c, test_s, angle, arm;
    int u_x, u_y, d_x, d_y, r_i, r_j;
	long imgdim = width*height;

	if ( !(interpret=parse_string(degree_string)) )
		return -1;
 
	if ( (tempImage=new BYTE[imgdim]) == NULL )
		return -1;
	for ( long i=0; i<imgdim; i++ )
		tempImage[i] = image[i];

	if ( !(hImage=hough_transform(image, width, height, interpret, 180)) ) {
		delete [] tempImage;
		delete [] interpret;
		return -1;
	}
	delete [] interpret;

	arm = (float)sqrt((double)height*height+width*width);
    arm_length = (int)(arm + 0.5);
	if ( (houghImage=new BYTE[arm_length*180]) == NULL ) {
		delete [] tempImage;
		delete [] hImage;
		return -1;
	}
	for ( long i=0; i<arm_length*180; i++ )
		houghImage[i] = hImage[i];

	if ( !(tempImage2=hough_inverse(hImage, arm_length, 180, width, height, threshold)) ) {
		delete [] tempImage;
		delete [] hImage;
		delete [] houghImage;
		return -1;
	}
	delete [] hImage;

	if ( (tempImage1=new BYTE[imgdim]) == NULL ) {
		delete [] tempImage;
		delete [] houghImage;
		delete [] tempImage2;
		return -1;
	}

	image_i = tempImage2;
    image_o = tempImage;
	image_t = tempImage1;
    for (y=0; y<height; y++)
		for (x=0; x<width; x++)
			image_t[y*width+x] = image_i[y*width+x] & image_o[y*width+x];

	delete [] tempImage2;

	for ( long i=0; i<imgdim; i++ )
		tempImage[i] = tempImage1[i];

	if ( connection>0 ) { 
		image_i = houghImage;
		image_o = tempImage1;
		image_t = tempImage;
		int high_degree = 180;
		double pi=3.14159265;
		for (y=0; y<height; y++) {
			for (x=0; x<width; x++) {
				if (image_o[y*width+x] == 255) {
					u_x = x - connection;
					u_y = y - connection;
					d_x = x + connection;
					d_y = y + connection;
					u_x =   (u_x >= 0) ? u_x : 0;
					u_y =   (u_y >= 0) ? u_y : 0;
					d_x = (d_x < width) ? d_x : width-1;
					d_y = (d_y < height) ? d_y : height-1;
					for (int i=0; i<high_degree; i++) {
						angle = (float)((double)i*pi/180.);
						j = (int)((x*cos(angle) + y*sin(angle) + arm_length)/2 + 0.5);
						if	(j>=0 && j<arm_length && image_i[j*180+i]>=(BYTE)threshold) {
							r_cos = (float)cos(angle);
							r_sin = (float)sin(angle);
							arm = x*r_cos + y*r_sin;
							test_c = (r_cos > 0)? r_cos : -1*r_cos;
							test_s = (r_sin > 0)? r_sin : -1*r_sin;
							flag = 0;
							if (test_c < test_s) {
								for (r_i=u_x; r_i<= x; r_i++) {
									r_j = (int)((arm - r_i*r_cos)/r_sin + 0.5);
									if (r_j>=u_y && r_j<=d_y ) {
										if (flag == 1) {
											image_t[r_j*width+r_i] = 255;
										}
										else if (image_t[r_j*width+r_i] == 255 || image_o[r_j*width+r_i] == 255) {
											flag = 1;
										}
									}
								}
								flag = 0;
								for (r_i=d_x; r_i>=x; r_i--) {
									r_j = (int)((arm - r_i*r_cos)/r_sin + 0.5);
									if (r_j>=u_y && r_j<=d_y) {
										if (flag == 1) {
											image_t[r_j*width+r_i] = 255;
										}
										else if(image_t[r_j*width+r_i] == 255 || image_o[r_j*width+r_i] == 255) {
											flag = 1;
										}
									}
								}
							}
							else {
								for (r_j=u_y; r_j<= y; r_j++) {
									r_i = (int)((arm - r_j*r_sin)/r_cos + 0.5);
									if (r_i>=u_x && r_i<=d_x ) {
										if (flag == 1) {
											image_t[r_j*width+r_i] = 255;
										}
										else if(image_t[r_j*width+r_i] == 255 || image_o[r_j*width+r_i] == 255) {
											flag = 1;
										}
									}
								}
								flag = 0;
								for	(r_j=d_y;	r_j>=y;	r_j--)	{
									r_i = (int)((arm - r_j*r_sin)/r_cos + 0.5);
									if	(r_i>=u_x && r_i<=d_x )	{
										if	(flag == 1)	{
											image_t[r_j*width+r_i] = 255;
										}
										else if (image_t[r_j*width+r_i] == 255 || image_o[r_j*width+r_i] == 255) {
											flag = 1;
										}
									}
								}
							}
						}
					}

				}
			}
		}
	}

	for ( long i=0; i<imgdim; i++ )
		image[i] = tempImage[i];

    delete [] houghImage;
    delete [] tempImage1;
    delete [] tempImage;

    return 0;
}

// parse_string function transforms a input string into the format needed by hough transform.
char *CHough::parse_string(char *input)
{
    int count,before,decision = 0,lower,upper;
    char *output;
    char *ptr,temp[10],original[180];

    if ( !(output=(char*)calloc(180,sizeof(char))) )
		return NULL;
    memset(original,0,sizeof(original));
    ptr = input;
    while(*ptr ==' ' || *ptr == '\t')ptr++;
    if (*ptr == '\n' || *ptr == NULL) {
		memset(output,1,180*sizeof(char));
		return output;
    }
    while(*ptr != NULL) {
		before = -1;
		if(*ptr == '-') {
			before = decision;
			ptr++;
		}
		while(*ptr ==' ' || *ptr == '\t')ptr++;
		count = 0;
		while(isdigit(*ptr))
			temp[count++] = *ptr++;
		temp[count] = NULL;
  		if(count != 0){
			sscanf(temp,"%d",&decision);
			if(decision <= 179){
				if(before == -1)
					original[decision] = 1;
				else {
					lower = (before < decision)?before:decision;
					upper = (before > decision)?before:decision;
					memset(&original[lower],1,(upper-lower+1)*sizeof(char));
					before = -1;
				}
			}
			else{
				decision = 179;
				original[decision] = 1;
			}
		}
        while(*ptr ==' ' || *ptr == '\t') ptr++;
		if(*ptr == NULL)
			break;
	}
    if (before != -1)
	memset(&original[before],1,(180 - before)*sizeof(char));
    memcpy(output,&original[90],90*sizeof(char));
    memcpy(&output[90],original,90*sizeof(char));
    
	return output;
}
