///////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 1993 SIUE - by Gregory Hance (Southern Illinois University @ Edwardsville)
//
// Permission to use, copy, modify, and distribute this software and its
// documentation for any purpose and without fee is hereby granted, provided
// that the above copyright notice appear in all copies and that both that
// copyright notice and this permission notice appear in supporting
// documentation.  This software is provided "as is" without express or
// implied warranty.

///////////////////////////////////////////////////////////////////////////////////////////////
// Color Segmentation based on fuzzy C-mean algorithm
// 8 bit images only
// 
// Adapted and Modified By Homayoun Bagherinia 08/04/2004
///////////////////////////////////////////////////////////////////////////////////////////////

#include <math.h>

#include "stdafx.h"
#include "vcfuzzycmeansegment.h"

#define MAX(a,b)	(((a)>(b))?(a):(b))
#define ABS(a)		((((int)(a))<0) ? -(a) : (a))

///////////////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////////////////////
CFuzzyCmeanSegment::CFuzzyCmeanSegment()
{
	assign_mean = 1;
	his1 = NULL;
	his2 = NULL;
	pk = NULL;
	klass = NULL;
}

CFuzzyCmeanSegment::~CFuzzyCmeanSegment()
{
	if ( his1 )
		delete [] his1;
	if ( his2 )
		delete [] his2;
	if ( pk )
		delete [] pk;
	if ( klass )
		delete [] klass;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Segment a RGB image based on fuzzy C-mean algorithm
// variance is the gaussian kernel variance (0.0-20.0)
///////////////////////////////////////////////////////////////////////////////////////////////
int  CFuzzyCmeanSegment::fuzzyc_segment(BYTE *R, BYTE *G, BYTE *B, int width, int height, float variance)
{
    //int  	i, j, k;
    double 	*g;
    double 	d[2] = {-1.0, 1.0};
    int  	choice = -1;
	float 	tore = 5.0;
	float  	t_len;	// 49.0
	Histogram  *his;

	imR = R;
	imG = G;
	imB = B;
	imWidth = width;
	imHeight = height;

    if ( (his=new Histogram[257]) == NULL )
		return -1;
	if ( (his1=new Histogram[257]) == NULL) 
	{
		delete [] his;
		return -1;
	}
    if ( (his2=new Histogram[257]) == NULL)
    {
       delete [] his;
       delete [] his1; his1=NULL;
       return -1;
    }
    
    if ( read_ppm(&in) == -1 )
	{
		delete [] his;
		delete [] his1; his1=NULL;
		delete [] his2; his2=NULL;
		return -1;
	}

	if ( variance )
		tore = variance;
	t_len = (float)(10. * tore - 1.);

	count_his(&in, his);
	max_his(his, in.colors);

	g = gausian(tore, t_len);
	if ( g == NULL )
	{
		delete [] his;
		delete [] his1; his1=NULL;
		delete [] his2; his2=NULL;
		free_matrix(in.matrix);
		return -1;
	}
	convolution(his, g, in.colors+1, (int)t_len);
	delete [] g;
	max_his(his, in.colors);

	memcpy(his1, his, 257*sizeof(Histogram));
	convolution(his1, d, in.colors, 2);
	max_his(his1, in.colors-1);
		
	memcpy(his2, his1, 257*sizeof(Histogram));
	convolution(his2, d, in.colors, 2);
	max_his(his2, in.colors-2);

	if ( find_cluster() == -1 )
	{
		delete [] his;
		delete [] his1; his1=NULL;
		delete [] his2; his2=NULL;
		free_matrix(in.matrix);
	    return -1;
	}
    memcpy(&out, &in, sizeof(ImageFuzzy));
	if((out.matrix = create_matrix(in.width, in.height)) == NULL)
	{
		delete [] his;
		delete [] his1; his1=NULL;
		delete [] his2; his2=NULL;
		free_matrix(in.matrix);
	    return -1;
	}
	
	coarse_seg();
	fine_seg();
    write_ppm(&out);

	free_matrix(in.matrix);
	free_matrix(out.matrix);
	delete [] his;
	delete [] his1; his1=NULL;
	delete [] his2; his2=NULL;
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// create a 2D Pixel image matrix
///////////////////////////////////////////////////////////////////////////////////////////////
MatrixPixel **CFuzzyCmeanSegment::create_matrix(int width, int height)
{
   MatrixPixel **m;
   int col;
   
   if ( (m=new MatrixPixel*[height]) == NULL )
      return NULL;

   if ( (m[0]=new MatrixPixel[width*height]) == NULL )
   {
      delete [] m;
      return NULL;
   }

   for (col = 1; col < height; col++)
      m[col] = &m[0][col*width];
   
   return m;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// free the memory of matrix
///////////////////////////////////////////////////////////////////////////////////////////////
void CFuzzyCmeanSegment::free_matrix(MatrixPixel **m)
{
    if (m[0] != NULL)
        delete [] m[0];
    if (m != NULL)
        delete [] m;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// write ImageFuzzy image in RGB image
///////////////////////////////////////////////////////////////////////////////////////////////
void CFuzzyCmeanSegment::write_ppm(ImageFuzzy *im)
{
   	int   	i=0;
   	BYTE 	*rP, *gP, *bP, *pixelP;
   
   	rP = imR;
   	gP = imG;
   	bP = imB;
   
   	pixelP = (BYTE *) im->matrix[0];

    for( ; i < im->size; i++) {
		*rP++ = *pixelP++; 
        *gP++ = *pixelP++; 
        *bP++ = *pixelP++; 
   	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
// write RGB image in ImageFuzzy image
///////////////////////////////////////////////////////////////////////////////////////////////
int CFuzzyCmeanSegment::read_ppm(ImageFuzzy	*im)
{
   	int   	i;
   	BYTE 	*rP, *gP, *bP, *pixelP;

   	im->width  = imWidth;
   	im->height = imHeight;
   	im->colors = 255;
   	im->size = im->width * im->height;
   	im->matrix = create_matrix(im->width, im->height);
	if ( im->matrix == NULL )
		return -1;

   	rP = imR;
   	gP = imG;
   	bP = imB;
   
   	pixelP = (BYTE *) im->matrix[0];
   
   	for(i=0; i < im->size; i++) {
		*pixelP++ = *rP++;
        *pixelP++ = *gP++;
        *pixelP++ = *bP++;
   	}
	
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// compute the gausion distrubution
///////////////////////////////////////////////////////////////////////////////////////////////
double *CFuzzyCmeanSegment::gausian(float tore, float len)
{
	const double SQRT_2PI=2.506628275;
    double *ser, neg_two_tore_sqr, c;
    int i, j;
    
    neg_two_tore_sqr = -2.0 * tore * tore;
    c = 1.0 / SQRT_2PI / tore;
    
    if ( (ser=new double[(unsigned int)len]) == NULL )
		return NULL;
    
    for (i = 0, j = (int)(-len/2.); i < len; i++, j++)
        ser[i] = c * exp(j * j / neg_two_tore_sqr);
    
    return ser;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// convolution of two series
///////////////////////////////////////////////////////////////////////////////////////////////
int CFuzzyCmeanSegment::convolution(Histogram *ser1, double *ser2, int len1, int len2)
{
    double  *sum;
    int  i, j, k;

    if ( (sum=new double[len1+len2-1]) == NULL )
		return -1;

   // init the sum
	for (k = 0; k < 3; k++)
	{ 	    
		for(i = 0; i < len1+len2-1; i++)
			sum[i] = 0.0;
		for(i = 0; i < len1; i++)
			for (j = 0; j < len2; j++)
				sum[i+len2-j-1] += ser2[j] * ser1[i][k];
	    
		j = (int)floor((double)len2/2.);
		for (i = 0; i <len1; i++)
			ser1[i][k] = (double) sum[i+j];
    }		
    delete [] sum;
    
    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// extract the three color component histograms
///////////////////////////////////////////////////////////////////////////////////////////////
void CFuzzyCmeanSegment::count_his(ImageFuzzy *in, Histogram	*his)   
{
    int i, j;

    for (i=0; i<in->colors+1; i++)
       his[i][0] = his[i][1] = his[i][2] = 0;
    
    for(i=0; i<in->height; i++)
	for(j=0; j<in->width; j++)
	{
	    his[(int)in->matrix[i][j][0]][0]+= 1.0;
	    his[(int)in->matrix[i][j][1]][1]+= 1.0;
	    his[(int)in->matrix[i][j][2]][2]+= 1.0;
	}
   
    max_his(his, in->colors);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// finger the max_value of the histogram
///////////////////////////////////////////////////////////////////////////////////////////////
void CFuzzyCmeanSegment::max_his(Histogram *h, int colors)
{
    Histogram t;
    int i;
    
    h[colors+1][0] = 0;
    h[colors+1][1] = 0;
    h[colors+1][2] = 0;

       	/* don't consider background */
    for (i=1; i < colors+1; i++)
    {
        t[0] = ABS(h[i][0]);
        t[1] = ABS(h[i][1]);
        t[2] = ABS(h[i][2]);
        h[colors+1][0] = MAX (t[0],h[colors+1][0]);
        h[colors+1][1] = MAX (t[1],h[colors+1][1]);
        h[colors+1][2] = MAX (t[2],h[colors+1][2]);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
// find the class index in one dimension
// return 255 if it is in safety margin
///////////////////////////////////////////////////////////////////////////////////////////////
BYTE CFuzzyCmeanSegment::class_index1(BYTE c, int k)  
{
    unsigned char  i;
    
    for(i = 0; i < pn[k]; i++) {
        if (c < pk[i][k].low)
	    return 255;
	if (c <= pk[i][k].up)
	    return i;
    }
    return 255;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// return the class index
///////////////////////////////////////////////////////////////////////////////////////////////
int CFuzzyCmeanSegment::class_index(MatrixPixel p)
{
    int i;
    i = (p[0]*pn[1] + p[1]) * pn[2] + p[2];
    return i;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// first stage, coarse segmentation
///////////////////////////////////////////////////////////////////////////////////////////////
int CFuzzyCmeanSegment::coarse_seg()
{
    int i, j, k, n, m;
    //int d[3];
    Class *kl;

	MatrixPixel  colors[27] = {
		{0,0,0}, {250,0,0}, {0,250,0}, {0,0,250}, 
        {250,250,0}, {250,0,250}, {0,250,250},
		{125,0,0},   {0,125,0},   {0,0,125}, 
		{125,125,0}, {125,0,125}, {0,125,125}, 
		{250,125,0}, {250,0,125}, {125,250,0}, 
		{0,250,125}, {125,0,250}, {0,125,250},
		{250,250,125}, {250,125,250}, {125,250,250}, 
		{250,125,125}, {125,250,125}, {125,125,250}, 
		{250,250,250}, {125,125,125}
	};
        
	// allocate & initiate all necessary class structs
    if ( (klass=new Class[pn[0]*pn[1]*pn[2]]) == NULL )
		return -1;

    // initate class to be {0, {0,0,0}, {0,0,0}, {0,0,0}}
    kl = klass;
    for(i = 0; i < pn[0]; i ++)
      for(j = 0; j < pn[1]; j ++) 
        for(k = 0; k < pn[2]; k++) {
	    kl->mean[0] = kl->mean[1] = kl->mean[2] = 0.0;
	    kl->std[0] = kl->std[1] = kl->std[2] = 0.0;
	    kl->count = 0;
	    kl->peak[0] = pk[i][0].peak;
	    kl->peak[1] = pk[j][1].peak;
	    kl->peak[2] = pk[k][2].peak;
	    kl++;
	}
	
    not_count = 0;   // the counter of unclassified pixels
    // first run to count the class.count and class.mean
    for(i = 0; i < out.height; i ++)
      for(j = 0; j < out.width; j ++) {
        for(k = 0; k < 3; k++)
            if ( ((out.matrix[i][j][k] = class_index1(in.matrix[i][j][k], k))>=pn[k])
				&& (out.matrix[i][j][k] != 255) )
			{
				//msg_CVIP(fn, "error return from index1(): too many peaks in \n\n");
				//getchar();
			}
	if( (out.matrix[i][j][0] == 255) || (out.matrix[i][j][1] == 255) ||
	                                    (out.matrix[i][j][2] == 255) ) 
	{
	    out.matrix[i][j][0] = 255;
	    out.matrix[i][j][1] = 255;
	    out.matrix[i][j][2] = 255;
	    not_count ++;
	} else 
	{
	    n = class_index(out.matrix[i][j]);
	    for(k = 0; k < 3; k++)
	        klass[n].mean[k] += (double) in.matrix[i][j][k];
	    klass[n].count ++;
	}
      }
    // eliminate minor classes, an important coefficient !!!!!!!!!
    m = (int) (in.size * 0.005);
    
    max_peak = 0;
    for(i = 0; i < pn[0]*pn[1]*pn[2]; i++) 
	{
        if(klass[i].count <= m) {
			klass[i].count = 0;
		} else {
			max_peak ++;
			// normalize the mean
			for(k = 0; k < 3; k++)
				klass[i].mean[k] /= (double) klass[i].count;
		}
	}
 
    // decided the represent colors
    if ( max_peak > 27 )
		assign_mean = 1;
    
    if (assign_mean == 0)
		n = 0;
    kl = klass;
    // assign the predefined colors
    for(i = 0; i < pn[0]; i ++)
      for(j = 0; j < pn[1]; j ++) 
        for(k = 0; k < pn[2]; k++)
		{
            if ( (kl->count != 0) && (n < max_peak) ) 
			{
				kl->color[0] = colors[n][0];
				kl->color[1] = colors[n][1];
				kl->color[2] = colors[n][2];
				n ++;
			}
			kl++;
		}
    
    // second run to compute std
    for(i = 0; i < out.height; i ++)
      for(j = 0; j < out.width; j ++) 
	  {
        if( (out.matrix[i][j][0] != 255) && (out.matrix[i][j][1] != 255) &&
	                                    (out.matrix[i][j][2] != 255) )
		{
			n = class_index(out.matrix[i][j]);
	        // re-assign the pixels belongs to minor classes        
			if (klass[n].count == 0) 
			{ // invalid class
				out.matrix[i][j][0] = 255;
				out.matrix[i][j][1] = 255;
				out.matrix[i][j][2] = 255;
				not_count ++;
			} 
			else 
			{ // classify all pixels belongs to valid classes
				for(k = 0; k < 3; k++)
				{
					klass[n].std[k] += (in.matrix[i][j][k] - klass[n].mean[k]) 
										* (in.matrix[i][j][k] - klass[n].mean[k]);
                    if(assign_mean == 1)
						out.matrix[i][j][k] = (unsigned char) klass[n].mean[k];
					else 
						out.matrix[i][j][k] = klass[n].color[k];
				}
			}
		}
	  }

    max_peak = 0;
    // normalize the class.std
    for(i = 0; i < pn[0]*pn[1]*pn[2]; i++)
	{
		if(klass[i].count != 0) 
		{
			// definition of std ??????
			for(k = 0; k < 3; k++)
			{
				klass[i].std[k] /= (double) klass[i].count;
				klass[i].std[k] = sqrt(klass[i].std[k]);
			}
			memcpy(&(klass[max_peak++]), &(klass[i]), sizeof(Class));
		}
    }
         
    return 0;
}	   

///////////////////////////////////////////////////////////////////////////////////////////////
// membership : 3D gausian distrubution
///////////////////////////////////////////////////////////////////////////////////////////////
double CFuzzyCmeanSegment::member_ship(MatrixPixel p, Class k)
{
    int i;
    double d[3];
    
    for(i = 0; i < 3; i++)
	{ 
    	d[i] = (p[i] - k.mean[i]) * (p[i] - k.mean[i]);
        d[i] = -0.5 * d[i] / (k.std[i] * k.std[i]);
		d[i] = exp(d[i]) / k.std[i];
    }

    return (d[0]*d[1]*d[2]);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// second stage : fine segmentation
///////////////////////////////////////////////////////////////////////////////////////////////
void CFuzzyCmeanSegment::fine_seg()
{
	int 	i, j, k, n, member;
    double  mp1, mp2;
    int 	nc[64], ncsum=0;
    
    // exclude those class which std = 0.0
    i = 0;      // the last class with non-zero std
    // compack the valid classes
    for(n = 0; n < max_peak; n++)
	{
        nc[n] = 0;
        if((klass[n].std[0] != 0.0) && (klass[n].std[1] != 0.0)
	                            && (klass[n].std[2] != 0.0))
		{
			if (i != n)
				memcpy(&klass[i], &klass[n], sizeof(Class));
			i ++;
		}
    }   
        
	// no valid peak exists
    if ( (max_peak = i) == 0 )
		return;

	// last run to assign the unclassfied pixels
    for(i = 0; i < out.height; i ++)
      for(j = 0; j < out.width; j ++)
	  {
        if( (out.matrix[i][j][0] == 255) || (out.matrix[i][j][1] == 255) ||
	                                    (out.matrix[i][j][2] == 255) ) 
		{
			// find the min membership
			mp1 = member_ship(in.matrix[i][j], klass[0]);
			member = 0;
			for(n = 1; n < max_peak; n++)
			{
				mp2 = member_ship(in.matrix[i][j], klass[n]);
				if ( mp2 > mp1 )
				{
					member = n;
					mp1 = mp2;
				}
            }
			nc[member]++;
			for (k = 0; k < 3; k++) 
			{
				if (assign_mean == 1)
					out.matrix[i][j][k] = (unsigned char) klass[member].mean[k];
				else 
					out.matrix[i][j][k] = klass[member].color[k];
			}
		}
	  }
      
    // all the memory allocated in seg.c should be freed hers
    delete [] pk; pk=NULL;
    delete [] klass; klass=NULL;
    
    for (n = 0; n < max_peak; n++)
		ncsum += nc[n];
}

///////////////////////////////////////////////////////////////////////////////////////////////
// find the valid cluster
///////////////////////////////////////////////////////////////////////////////////////////////
int CFuzzyCmeanSegment::find_cluster()
{
    int i, j, k;
    int v_or_p = 1;			// a flag, if a peak reached clu = 1, else clu = 0
    float  safe = (float)0.1;     // 10% safety margin

    vn[0] = vn[1] = vn[2] = 0;
    pn[0] = pn[1] = pn[2] = 0;
    peak[0][0] = 0.0;
    peak[0][1] = 0.0;
    peak[0][2] = 0.0;
    valley[0][0] = 0.0;
    valley[0][1] = 0.0;
    valley[0][2] = 0.0;
    
    for(k = 0; k < 3; k++)
    {   
        v_or_p = 1;
        for(j = 1; j < in.colors-2; j++)
        {
			if((his1[j][k] == 0.0) || (his1[j-1][k]*his1[j+1][k] < 0.0))
			{
				if((v_or_p == 1) && (his2[j][k] > 0.0))
				{   // it's a valley
					v_or_p = 0;  // turn the flag
					valley[vn[k]][k] = (double) j;
					vn[k]++;
					if (pn[k] == 0)
						pn[k] ++; 
				} 
				else 
				{ 
					if((v_or_p == 0) && (his2[j][k] < 0.0))
					{  // it's a peak
						v_or_p = 1;     // set the flag
						peak[pn[k]][k] = (double) j;
						pn[k]++;
					}
				}                      
			}
		}
		if((v_or_p == 0) || (pn[k] == vn[k])) 
		{
			// the last one is a valley, so assume a peak at 255
			peak[pn[k]][k] = 255.0;
			pn[k]++;
		}

		// to mark the end
		peak[pn[k]][k] = valley[vn[k]][k] = 0.0;
	}

    // try to fill in the struct peak
    max_peak = 0;
    for (k = 0; k < 3; k++)
	{
       max_peak = MAX(max_peak, pn[k]);
       max_peak = MAX(max_peak, vn[k]);
    }
    if ( (pk=new Peak[max_peak]) == NULL )
		return -1;
    for (k = 0; k < 3; k++) 
	{
        for(i = 0; i < pn[k]; i++) 
		{
			if(peak[i][k] == 0.0)
				pk[i][k].low = 0.0;
			else
				pk[i][k].low = safe*peak[i][k] + (1-safe)*valley[i-1][k];
			pk[i][k].peak = peak[i][k];
			if(valley[i][k] == 0.0)
				pk[i][k].up = safe*peak[i][k] + (1-safe)*255.0;
			else
				pk[i][k].up = safe*peak[i][k] + (1-safe)*valley[i][k];
	    
		}
	}

	return 0;
}
