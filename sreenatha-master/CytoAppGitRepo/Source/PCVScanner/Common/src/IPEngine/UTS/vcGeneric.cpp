///////////////////////////////////////////////////////////////////////////////////////
//
// An Interface to the Borland C UTS DLLs
//
// Written By Karl Ratcliff 01/03/2001
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include <windows.h>
#include <tchar.h>
#include "vcgeneric.h"
#include "uts.h"
     

////////////////////////////////////////////////////////////////////////////////////////
// Constructor

CUTSGeneric::CUTSGeneric()
{
    // Try and load the library
    m_hGenericLib       = NULL;

    gSmoothLongs        = NULL;
    gHistInfo           = NULL;
    gRGB8toHSI          = NULL;
}

// Destructor

CUTSGeneric::~CUTSGeneric()
{
    if (m_hGenericLib)
        FreeLibrary(m_hGenericLib);
}

////////////////////////////////////////////////////////////////////////////////////////
// Member Functions
////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
// Initialise

BOOL CUTSGeneric::Initialise(void)
{
    // Try and load the library
    m_hGenericLib = LoadLibrary(_T("zGeneric.dll"));

		
	// If successful get the proc addresses
    if (m_hGenericLib)
    {
        gSmoothLongs        = (LPGSMOOTHLONGS)      GetProcAddress(m_hGenericLib, "SmoothLongs");
        gHistInfo           = (LPGHISTINFO)         GetProcAddress(m_hGenericLib, "HistInfo");
        gRGB8toHSI          = (LPRGB8TOHSI)         GetProcAddress(m_hGenericLib, "RGB8toHSI");

        return TRUE;
    }
    else
        OutputDebugString(_T("zGeneric DLL Missing !\r\n"));


    // Got to here so return FALSE
    return FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////
//	Function:	SmoothLongs
//
//	Description:
//		This function implements a simpliest smoothing algorithm
//		in place for an array of longs
//
//	Statics/Globals:
//		None
//
//	Input Arguments:
//		pMem            - pointer to the array of longs
//		NumElements     - number of elements in the array
//		NumPoints       - number neighboring points to use
//
//	Returns:
//		none
////////////////////////////////////////////////////////////////////////////////////////

void CUTSGeneric::SmoothLongs(long *pMem, long NumElements, short NumPoints)
{
    if (m_hGenericLib && gSmoothLongs)
        (gSmoothLongs)(pMem, NumElements, NumPoints);
}

////////////////////////////////////////////////////////////////////////////////////////
//  This function calculates some statistics for the histogram
//  given by frequency array.
//
//	Arguments:
//		Histogramaddr - address to the histogram array data
//      HistogramLen  - size of the histogram
//      info - pointer to double array of output data;
//             must be at least 16 doubles in size
//
//     info[0] = Np  	: number of positive values in the histogram
//     info[1] = Imin: min I where Hist[I] is positive
//     info[2] = Imax: max I where Hist[I] is positive
//     info[3] = Sn  	: Sum of all values Hist(I)
//     info[4] = Av	: Average: (Sum(Hist(I) * I)) / Sn
//     info[5] = D	: Dispersion: (Sum(Hist(I) * (I - Av) ^ 2)) / Sn
//     info[6] = Bord	: Best separating value
//     					(is one of 0.5, 1.5, ... , histlen - 0.5)
//     info[7] = Sl	: Sum Hist(I) for I=0 to Bord- (==Ent(Bord))
//     info[8] = Sr	: Sum Hist(I) for I=Ceil(Bord+) to histlen-1
//     info[9] = Al	: Left Average: (Sum{0 to Bord-}(Hist(I) * I)) / Sl
//     info[10] = Ar	: Right Average: (Sum{Bord+ to histlen-1}(Hist(I) * I)) / Sr
//     info[11] = Dl	: Left Dispersion:
//     					(Sum{0 to Bord-}(Hist(I) * (I - Al)^2) / Sl
//     info[12] = Dr	: Right Dispersion:
//     					(Sum{Bord+ to histlen-1}(Hist(I) * (I - Ar)^2) / Sr
//     info[13] = Cs	: Coefficient of best separation:
//     					(((Sl*Sr) / (Sn*Sn)) * (Ar - Al) ^ 2) / (D + 1/12)
//     info[14] = Mode : Max value in the histogram
//     info[15] = ModePos : Position of mode
//
//    Returns:
//     Info[0]
////////////////////////////////////////////////////////////////////////////////////////

long CUTSGeneric::HistInfo(long *HistogramData, long NumElements, double *Info)
{
    if (m_hGenericLib && gHistInfo)
        return (gHistInfo) (HistogramData, NumElements, Info);
    else
        return UTS_ERROR;
}

////////////////////////////////////////////////////////////////////////////////////////
/***
    Calculates Hue, Saturation, Intensity from
    Red, Green, Blue color component values.
    Every H, S, U are normalized
    to the range {0-255}.
	H = arctan{ ((G-B)/sqr(3)) / (R-I) } =
		arctan{ ((G-B)*sqr(3)) / (3*R-(R+G+B)) }
    Hue values are about:
    0 - Cyan, 42 = Blue, 85 - Magenta, 127 - Red,
    170 - Yellow, 212 - Green, 255 - Cyan again
	Returns <color pix>
***/
////////////////////////////////////////////////////////////////////////////////////////

DWORD CUTSGeneric::RGB8toHSI(long R, long G, long B, long *H, long *S, long *I)
{
    if (m_hGenericLib && gRGB8toHSI)
        return (gRGB8toHSI) (R, G, B, H, S, I);
    else
        return UTS_ERROR;
}

