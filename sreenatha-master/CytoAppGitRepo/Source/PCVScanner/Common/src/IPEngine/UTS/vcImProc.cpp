////////////////////////////////////////////////////////////////////////////////
//
//	ImProc	- Image Processing functions
//
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "uts.h"


#include "stdafx.h"
#include "uts.h"
#include "float.h"
#include "vcmemrect.h"
#include "vcImProc.h"	// Maybe later included by uts.h

#include "woolzif.h"

#include  <math.h>
#include "FastFourierTransform.h"
#include "ImageRegister.h"

#define MAX(a,b) ( (a) > (b) ? (a) : (b) )
#define MIN(a,b) ( (a) < (b) ? (a) : (b) )

////////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
////////////////////////////////////////////////////////////////////////////////////////

CUTSImProc::CUTSImProc(CUTSMemRect *pMR)
{
	pUTSMemRect = pMR;
}

CUTSImProc::~CUTSImProc()
{
}

////////////////////////////////////////////////////////////////////////////////
//  Subtract Image1 from Image2
////////////////////////////////////////////////////////////////////////////////


long CUTSImProc::Subtract(long Image1, long Image2)
{
    long W1, H1, W2, H2, P, B, NewHandle;
    int  j;
    BYTE *Im1Ptr, *Im2Ptr, *Im3Ptr, g1, g2;

    pUTSMemRect->GetExtendedInfo(Image1, &W1, &H1, &P, &Im1Ptr);
    pUTSMemRect->GetExtendedInfo(Image2,  &W2, &H2, &P, &Im2Ptr);

    // Compare image sizes and make sure they are identical
    if (W1 != W2 || H1 != H2)
        return UTS_ERROR;

    // Create an image for the resulting threshold
    NewHandle = pUTSMemRect->CreateAs(Image1);
    if (!UTSVALID_HANDLE(NewHandle))
        return UTS_ERROR;

    pUTSMemRect->GetExtendedInfo(NewHandle, &W1, &H1, &P, &Im3Ptr);

    // Total bytes in image
    B = H1 * P;

    for (j = 0; j < B; j++)
    {
        g1 = *Im1Ptr++;
        g2 = *Im2Ptr++;
        *Im3Ptr++ = (g1 < g2) ? (g2 - g1) : (g1 - g2);
    }

    return NewHandle;
}

////////////////////////////////////////////////////////////////////////////////
// Logical AND on two 8 bit images
////////////////////////////////////////////////////////////////////////////////

long CUTSImProc::LogicalAND(long Image1, long Image2)
{
    long BPP1, BPP2, W1, H1, W2, H2, P, B, NewHandle;
    int  j;
    BYTE *Im1Ptr, *Im2Ptr, *Im3Ptr;

    BPP1 = pUTSMemRect->GetExtendedInfo(Image1, &W1, &H1, &P, &Im1Ptr);
    BPP2 = pUTSMemRect->GetExtendedInfo(Image2,  &W2, &H2, &P, &Im2Ptr);

    // Compare image sizes and make sure they are identical
    if (W1 != W2 || H1 != H2 || (BPP1 != EIGHT_BITS_PER_PIXEL && BPP1 != SIXTEEN_BITS_PER_PIXEL) || (BPP2 != EIGHT_BITS_PER_PIXEL && BPP2 != SIXTEEN_BITS_PER_PIXEL) || BPP1 != BPP2)
        return UTS_ERROR;

    // Create an image for the resulting threshold
    NewHandle = pUTSMemRect->CreateAs(Image1);
    if (!UTSVALID_HANDLE(NewHandle))
        return UTS_ERROR;

    pUTSMemRect->GetExtendedInfo(NewHandle, &W1, &H1, &P, &Im3Ptr);
		
	B = H1 * P;

    // Total bytes in image
	if (BPP1 == EIGHT_BITS_PER_PIXEL)
	{
		for (j = 0; j < B; j++)
			*Im3Ptr++ = *Im1Ptr++ & *Im2Ptr++;
	}
	else
	{
		WORD *wIm3Ptr, *wIm1Ptr, *wIm2Ptr;

		wIm3Ptr = (WORD *)Im3Ptr;
		wIm2Ptr = (WORD *)Im2Ptr;
		wIm1Ptr = (WORD *)Im1Ptr;

		for (j = 0; j < B; j++)
			*wIm3Ptr++ = *wIm1Ptr++ & *wIm2Ptr++;
	}

    return NewHandle;
}

////////////////////////////////////////////////////////////////////////////////
// Logical OR
////////////////////////////////////////////////////////////////////////////////

long CUTSImProc::LogicalOR(long Image1, long Image2)
{
    long BPP1, BPP2, W1, H1, W2, H2, P, B, NewHandle;
    int  j;
    BYTE *Im1Ptr, *Im2Ptr, *Im3Ptr;

    BPP1 = pUTSMemRect->GetExtendedInfo(Image1, &W1, &H1, &P, &Im1Ptr);
    BPP2 = pUTSMemRect->GetExtendedInfo(Image2,  &W2, &H2, &P, &Im2Ptr);

    // Compare image sizes and make sure they are identical
    if (W1 != W2 || H1 != H2 || BPP1 != BPP2)
        return UTS_ERROR;

	// hmmmm ????
	if (BPP1 != ONE_BIT_PER_PIXEL && BPP1 != EIGHT_BITS_PER_PIXEL && BPP1 != SIXTEEN_BITS_PER_PIXEL && BPP1 != TWENTYFOUR_BITS_PER_PIXEL && BPP1 != THIRTYTWO_BITS_PER_PIXEL)  
		return UTS_ERROR;

	if (BPP1 != ONE_BIT_PER_PIXEL && BPP2 != EIGHT_BITS_PER_PIXEL && BPP2 != SIXTEEN_BITS_PER_PIXEL && BPP2 != TWENTYFOUR_BITS_PER_PIXEL && BPP2 != THIRTYTWO_BITS_PER_PIXEL) 
		return UTS_ERROR;

    // Create an image for the resulting threshold
    NewHandle = pUTSMemRect->CreateAs(Image1);
    if (!UTSVALID_HANDLE(NewHandle))
        return UTS_ERROR;

    pUTSMemRect->GetExtendedInfo(NewHandle, &W1, &H1, &P, &Im3Ptr);
		
	B = H1 * P;

    // Total bytes in image
	for (j = 0; j < B; j++)
		*Im3Ptr++ = *Im1Ptr++ | *Im2Ptr++;

    return NewHandle;
}

////////////////////////////////////////////////////////////////////////////////
// Logical XOR
////////////////////////////////////////////////////////////////////////////////

long CUTSImProc::LogicalXOR(long Image1, long Image2)
{
    long BPP1, BPP2, W1, H1, W2, H2, P, B, NewHandle;
    int  j;
    BYTE *Im1Ptr, *Im2Ptr, *Im3Ptr;

    BPP1 = pUTSMemRect->GetExtendedInfo(Image1, &W1, &H1, &P, &Im1Ptr);
    BPP2 = pUTSMemRect->GetExtendedInfo(Image2,  &W2, &H2, &P, &Im2Ptr);

    // Compare image sizes and make sure they are identical
    if (W1 != W2 || H1 != H2 || BPP1 != BPP2)
        return UTS_ERROR;

	// hmmmm ????
	if (BPP1 != ONE_BIT_PER_PIXEL && BPP1 != EIGHT_BITS_PER_PIXEL && BPP1 != SIXTEEN_BITS_PER_PIXEL && BPP1 != TWENTYFOUR_BITS_PER_PIXEL && BPP1 != THIRTYTWO_BITS_PER_PIXEL)  
		return UTS_ERROR;

	if (BPP1 != ONE_BIT_PER_PIXEL && BPP2 != EIGHT_BITS_PER_PIXEL && BPP2 != SIXTEEN_BITS_PER_PIXEL && BPP2 != TWENTYFOUR_BITS_PER_PIXEL && BPP2 != THIRTYTWO_BITS_PER_PIXEL) 
		return UTS_ERROR;

    // Create an image for the resulting threshold
    NewHandle = pUTSMemRect->CreateAs(Image1);
    if (!UTSVALID_HANDLE(NewHandle))
        return UTS_ERROR;

    pUTSMemRect->GetExtendedInfo(NewHandle, &W1, &H1, &P, &Im3Ptr);
		
	B = H1 * P;

    // Total bytes in image
	for (j = 0; j < B; j++)
		*Im3Ptr++ = *Im1Ptr++ ^ *Im2Ptr++;

    return NewHandle;
}
////////////////////////////////////////////////////////////////////////////////
// Divide images such that:
//   Result = (Image1 / Image2) * Scale
////////////////////////////////////////////////////////////////////////////////

long CUTSImProc::Divide(long Image1, long Image2, BYTE Scale)
{
    long BPP1, BPP2, W1, H1, W2, H2, P, B, NewHandle;
    int  j;
    BYTE *Im1Ptr, *Im2Ptr, *Im3Ptr;

    BPP1 = pUTSMemRect->GetExtendedInfo(Image1, &W1, &H1, &P, &Im1Ptr);
    BPP2 = pUTSMemRect->GetExtendedInfo(Image2,  &W2, &H2, &P, &Im2Ptr);

    // Compare image sizes and make sure they are identical
    if (W1 != W2 || H1 != H2 || BPP1 != EIGHT_BITS_PER_PIXEL || BPP2 != EIGHT_BITS_PER_PIXEL)
        return UTS_ERROR;

    // Create an image for the resulting threshold
    NewHandle = pUTSMemRect->CreateAs(Image1);
    if (!UTSVALID_HANDLE(NewHandle))
        return UTS_ERROR;

    pUTSMemRect->GetExtendedInfo(NewHandle, &W1, &H1, &P, &Im3Ptr);

    // Total bytes in image
    B = H1 * P;

    for (j = 0; j < B; j++)
        *Im3Ptr++ = ((*Im1Ptr++) * Scale / (1 + *Im2Ptr++));

    return NewHandle;
}

////////////////////////////////////////////////////////////////////////////////
// Finds the mean grey level under an 8 bit mask
////////////////////////////////////////////////////////////////////////////////

long CUTSImProc::Mean8(long Image1, long MaskImage, long *Area)
{
    long BPP1, BPP2, W1, H1, W2, H2, P, B;
    int  j;
    BYTE *Im1Ptr, *Im2Ptr, g, gM;
    long Sum = 0, A = 0;

    *Area = 0;

    BPP1 = pUTSMemRect->GetExtendedInfo(Image1, &W1, &H1, &P, &Im1Ptr);
    BPP2 = pUTSMemRect->GetExtendedInfo(MaskImage,  &W2, &H2, &P, &Im2Ptr);

    // Compare image sizes and make sure they are identical
    if (W1 != W2 || H1 != H2 || BPP1 != EIGHT_BITS_PER_PIXEL || BPP2 != EIGHT_BITS_PER_PIXEL)
        return UTS_ERROR;

    // Total bytes in image
    B = H1 * P;

    for (j = 0; j < B; j++)
    {
        g = *Im1Ptr++;
        gM = *Im2Ptr++;
        if (gM == NBINS_EIGHT - 1)
        {
            Sum += g;
            A++;
        }
    }

    *Area = A;
    if (A)
        return (Sum / A);
    else
        return 0;
}

////////////////////////////////////////////////////////////////////////////////
// Find the mode grey level in an image, below a certain MaxLevel
////////////////////////////////////////////////////////////////////////////////

long CUTSImProc::Mode8(long SrcImage, BYTE MaxLevel)
{
    long Hist[256];
    long W1, H1, P, B, Max, g;
    BYTE *Addr;
    int  i;

    pUTSMemRect->GetExtendedInfo(SrcImage, &W1, &H1, &P, &Addr);
    
    for (i = 0; i < 256; i++) Hist[i] = 0;

    B = W1 * H1;
    for (i = 0; i < B; i++)
        Hist[*Addr++]++;
    Max = 0;
    g = 0;
    for (i = 0; i < MaxLevel; i++)
        if (Hist[i] > Max)
        {
            Max = Hist[i];
            g = i;
        }

    return g;
}

////////////////////////////////////////////////////////////////////////////////
// Given an array of image handles go through each one and produce an
// image where the pixel value = the array index to the image with the maximum
// pixel intensity
////////////////////////////////////////////////////////////////////////////////

long CUTSImProc::MaxProjectionImage(long Images[], long NImages)
{
    BYTE *ImArrPtr[255], *DestAddr, *Addr;
    int i, j;
    long W, H, P, B, Handle;
    BYTE Max, Idx, g;

    for (i = 0; i < NImages; i++)
    {
        pUTSMemRect->GetExtendedInfo(Images[i], &W, &H, &P, &ImArrPtr[i]);
    }

    // Total bytes in image
    B = H * P;

    Handle = pUTSMemRect->CreateAs(Images[0]);
    pUTSMemRect->GetExtendedInfo(Handle, &W, &H, &P, &DestAddr);
    
    Addr = DestAddr;

    for (j = 0; j < B; j++)
    {
        Max = 0;
        Idx = 0;
        for (i = 0; i < NImages; i++)
        {
            g = *ImArrPtr[i]++;
            if (g > Max) 
            {
                Max = g;
                Idx = i;
            }
        }

        *Addr++ = Idx;
    }

    return Handle;
}

////////////////////////////////////////////////////////////////////////////////
// Given an array of image handles and an image whose pixel values represent
// indices into the array create a new image with pixel values set from the 
// relevant image in the array
////////////////////////////////////////////////////////////////////////////////

long CUTSImProc::ProjectionImage(long IdxImage, long Images[], long NImages)
{
    BYTE *ImArrPtr[255], *DestAddr, *IdxAddr;
    int i, j;
    long W, H, P, B, Handle;
    BYTE Idx;

    for (i = 0; i < NImages; i++)
    {
        pUTSMemRect->GetExtendedInfo(Images[i], &W, &H, &P, &ImArrPtr[i]);
    }

    // Total bytes in image
    B = H * P;

    // The result image
    Handle = pUTSMemRect->CreateAs(Images[0]);
    pUTSMemRect->GetExtendedInfo(Handle, &W, &H, &P, &DestAddr);
    
    // Get our index image
    pUTSMemRect->GetExtendedInfo(IdxImage, &W, &H, &P, &IdxAddr);

    for (j = 0; j < B; j++)
    {
        Idx = *IdxAddr++;
        for (i = 0; i < NImages; i++)
        {
            if (i == Idx)
                *DestAddr++ = *ImArrPtr[i]++;
            else
                ImArrPtr[i]++;
        }
    }

    return Handle;
}
////////////////////////////////////////////////////////////////////////////////
// Do the equivalent of |a| = |b| + |c|
////////////////////////////////////////////////////////////////////////////////

long CUTSImProc::Mod8(long Image1, long Image2)
{
    long Image3, BPP1, BPP2, W1, H1, W2, H2, P, B;
    int  j;
    BYTE *Im1Ptr, *Im2Ptr, g1, g2;
    BYTE *Im3Ptr;

    BPP1 = pUTSMemRect->GetExtendedInfo(Image1, &W1, &H1, &P, &Im1Ptr);
    BPP2 = pUTSMemRect->GetExtendedInfo(Image2,  &W2, &H2, &P, &Im2Ptr);
    Image3 = pUTSMemRect->CreateAs(Image1);
    pUTSMemRect->GetExtendedInfo(Image3, &W1, &H1, &P, &Im3Ptr);

    // Compare image sizes and make sure they are identical
    if (W1 != W2 || H1 != H2 || BPP1 != EIGHT_BITS_PER_PIXEL || BPP2 != EIGHT_BITS_PER_PIXEL)
        return UTS_ERROR;

    // Total bytes in image
    B = H1 * P;

    for (j = 0; j < B; j++)
    {
        g1 = *Im1Ptr++;
        g2 = *Im2Ptr++;
        *Im3Ptr++ = MAX(g1, g2);
    }

    return Image3;
}

////////////////////////////////////////////////////////////////////////////////
// Threshold an 8 bit grey level image to get an 8 bit binary image
//  NOTE 0 = 0, 255 = 1
// Thresholds between ThreshLo and ThreshHi returns a new handle
// to the thresholded image
////////////////////////////////////////////////////////////////////////////////

long CUTSImProc::Threshold8(long Image1, BYTE ThreshLo, BYTE ThreshHi)
{
    long BPP1, W1, H1, P, B, NewHandle;
    int  j;
    BYTE *Im1Ptr, *Im2Ptr, g;

    BPP1 = pUTSMemRect->GetExtendedInfo(Image1, &W1, &H1, &P, &Im1Ptr);

    // Compare image sizes and make sure they are identical
    if (BPP1 != EIGHT_BITS_PER_PIXEL)
        return UTS_ERROR;

    // Create an image for the resulting threshold
    NewHandle = pUTSMemRect->CreateAs(Image1);
    if (!UTSVALID_HANDLE(NewHandle))
        return UTS_ERROR;

    pUTSMemRect->GetExtendedInfo(NewHandle, &W1, &H1, &P, &Im2Ptr);

    // Total bytes in image
    B = H1 * P;

    for (j = 0; j < B; j++)
    {
        g = *Im1Ptr++;
        *Im2Ptr++ = (g >= ThreshLo && g <= ThreshHi) ? NBINS_EIGHT - 1 : 0;
    }

    return NewHandle;
}

////////////////////////////////////////////////////////////////////////////////
// Threshold an N bit image to produce an 8 bit binary image where N <= 16
////////////////////////////////////////////////////////////////////////////////
    
long CUTSImProc::Threshold16(long Image1, WORD ThreshLo, WORD ThreshHi)
{
	long BPP1, W1, H1, P, B, NewHandle;
    int  j;
	WORD *Im1Ptr;
	WORD g;
    BYTE *Im2Ptr;

    BPP1 = pUTSMemRect->GetExtendedInfo(Image1, &W1, &H1, &P, &Im1Ptr);

    // Compare image sizes and make sure they are identical
    if (BPP1 != SIXTEEN_BITS_PER_PIXEL)
        return UTS_ERROR;

    // Create an image for the resulting threshold
    NewHandle = pUTSMemRect->Create(EIGHT_BITS_PER_PIXEL, W1, H1);
    if (!UTSVALID_HANDLE(NewHandle))
        return UTS_ERROR;

    pUTSMemRect->GetExtendedInfo(NewHandle, &W1, &H1, &P, &Im2Ptr);

    // Total bytes in image
    B = H1 * P;

    for (j = 0; j < B; j++)
    {
        g = *Im1Ptr++;
        *Im2Ptr++ = (g >= ThreshLo && g <= ThreshHi) ? NBINS_EIGHT - 1 : 0;
    }

    return NewHandle;
}

////////////////////////////////////////////////////////////////////////////////
// Threshold an 8 bit grey level image to get an 8 bit binary image
//  NOTE 0 = 0, 255 = 1
// Thresholds greater than or equal Thresh, returns a new handle
// to the thresholded image
////////////////////////////////////////////////////////////////////////////////

long CUTSImProc::ThresholdGT8(long Image1, BYTE Thresh)
{
    long BPP1, W1, H1, P, B, NewHandle;
    int  j;
    BYTE *Im1Ptr, *Im2Ptr;

    BPP1 = pUTSMemRect->GetExtendedInfo(Image1, &W1, &H1, &P, &Im1Ptr);

    // Compare image sizes and make sure they are identical
    if (BPP1 != EIGHT_BITS_PER_PIXEL)
        return UTS_ERROR;

    // Create an image for the resulting threshold
    NewHandle = pUTSMemRect->CreateAs(Image1);
    if (!UTSVALID_HANDLE(NewHandle))
        return UTS_ERROR;

    pUTSMemRect->GetExtendedInfo(NewHandle, &W1, &H1, &P, &Im2Ptr);

    // Total bytes in image
    B = H1 * P;

    for (j = 0; j < B; j++)
        *Im2Ptr++ = (*Im1Ptr++ >= Thresh) ? NBINS_EIGHT - 1 : 0;

    return NewHandle;
}

////////////////////////////////////////////////////////////////////////////////
// Threshold to produce a returned mask by comparing image 
// and background approximation if the difference is > threshold then
// set the pixel to 1 else set it to 0
////////////////////////////////////////////////////////////////////////////////

long CUTSImProc::BgrSubtract(long GreyImage, long BgrImage, long Threshold)
{
    long W1, H1, W2, H2, P, B, NewHandle;
    int  j;
    BYTE *Im1Ptr, *Im2Ptr, *Im3Ptr;
    BYTE gBgr, gIm;
    int Diff, Delta;

    pUTSMemRect->GetExtendedInfo(GreyImage, &W1, &H1, &P, &Im1Ptr);
    pUTSMemRect->GetExtendedInfo(BgrImage,  &W2, &H2, &P, &Im2Ptr);

    // Compare image sizes and make sure they are identical
    if (W1 != W2 || H1 != H2)
        return UTS_ERROR;

    // Create an image for the resulting threshold
    NewHandle = pUTSMemRect->CreateAs(GreyImage);
    if (!UTSVALID_HANDLE(NewHandle))
        return UTS_ERROR;

    pUTSMemRect->GetExtendedInfo(NewHandle, &W1, &H1, &P, &Im3Ptr);

    // Total bytes in image
    B = H1 * P;

    for (j = 0; j < B; j++)
    {
        gBgr = *Im2Ptr++;
        gIm  = *Im1Ptr++;
        Delta = gIm - Threshold;
        Diff = abs(gIm - gBgr);
        if (gBgr > 0 && Diff > Threshold)
            *Im3Ptr++ = 255;
        else
            *Im3Ptr++ = 0;
    }

    return NewHandle;
}

////////////////////////////////////////////////////////////////////////////////
// Perform shade correction by flat fielding
////////////////////////////////////////////////////////////////////////////////

long CUTSImProc::FlatField(long GreyImage, long BackgroundImage)
{
    long W1, H1, W2, H2, P, B, NewHandle;
    int  j;
    BYTE *ImPtr, *BgrPtr, *DestPtr, *Bgr, *Im, *Dest;
	
    pUTSMemRect->GetExtendedInfo(GreyImage, &W1, &H1, &P, &ImPtr);
    pUTSMemRect->GetExtendedInfo(BackgroundImage,&W2, &H2, &P, &BgrPtr);

    // Compare image sizes and make sure they are identical
    if (W1 != W2 || H1 != H2)
        return UTS_ERROR;

    // Create an image for the resulting threshold
    NewHandle = pUTSMemRect->CreateAs(GreyImage);
    if (!UTSVALID_HANDLE(NewHandle))
        return UTS_ERROR;

    pUTSMemRect->GetExtendedInfo(NewHandle, &W1, &H1, &P, &DestPtr);
    // Total bytes in image
    B = H1 * P;
   
	int Tot = 0;
	Im = BgrPtr;
	for (j = 0; j < B; j++)
		Tot += 255 - *Im++;

	BYTE Mean = (BYTE)(Tot / B);

	Bgr = BgrPtr;
	Im = ImPtr;
	Dest = DestPtr;

	//	*Dest++ = 255 - ((255 - *Im++) * Max) / (1 + (255 - *Bgr++));		

	int z;
    for (j = 0; j < B; j++)
	{
		z = 255 - ((255 - *Im++) * Mean) / (1 + (255 - *Bgr++));	
		if (z < 0) z = 0;
		*Dest++ = z;
	}	

    return NewHandle;
}

////////////////////////////////////////////////////////////////////////////////////////
// Fractal Box Count
//    BoxCountResult = FractalBoxCount(Image, Iterations)
// Parameters:
//	  Image is the image to do the analysis on - binary 0 = black, 255 = object
//    Iterations is the number of iterations to perform the box count over 2 - N
// Returns:
//	  The box count result i.e. the least squares regression slope of the result
////////////////////////////////////////////////////////////////////////////////////////


double CUTSImProc::FractalBoxCount(long Image, long Iter)
{
	long W, H, TempIm, A;
	double X, Y;
	double SX, SY, SXY, SXX, R;
	int i;
	double *loga;

	loga = new double[Iter + 2];
	for (i = 2; i <= 2 + Iter; i++)
		loga[i] = log10((double) i);

	SX = 0.;
	SY = 0.;
	SXY = 0.;
	SXX = 0.;

    pUTSMemRect->GetRectInfo(Image, &W, &H);
		
	for (i = 2; i <= 2 + Iter; i++)
	{
		TempIm = pUTSMemRect->Create(8, W / i, H / i);
		pUTSMemRect->Zoom(Image, TempIm, "SUB");
		A = pUTSMemRect->Projection0(TempIm, "ONES");
		pUTSMemRect->FreeImage(TempIm);
		if (A)
			X = log10((double) A);
		else
			X = 0;
		Y = loga[i];		
		SX += X;
		SY += Y;
		SXX += X * X;
		SXY += X * Y;
	}

	delete loga;
	double D = Iter * SXX - SX * SX;

	if (D != 0.)
		R = (Iter * SXY - SX * SY) / D;
	else
		R = -999999.;

	return R;
}


////////////////////////////////////////////////////////////////////////////////////////
// Equivalent of projection0("SUM")
// Just return the sum of greys in the image - dont use too big an image !!!!!
////////////////////////////////////////////////////////////////////////////////////////

long CUTSImProc::ImageSum(long Image)
{
	long W1, H1, P, B;
    int  j;
    BYTE *ImPtr;
	long Tot = 0;

    pUTSMemRect->GetExtendedInfo(Image, &W1, &H1, &P, &ImPtr);

	// Total bytes in image
    B = H1 * P;

	for (j = 0; j < B; j++)
		Tot += *ImPtr++;

	return Tot;
}

////////////////////////////////////////////////////////////////////////////////
//
//	Register Images (Difference method)
//
//	Image registration by difference method
//	Only shifts in X or Y are allowed (not both!)
//	With image1 as the reference, in a rectangular grid capture, image2
//	will be N, S, E, or W of image1 as specified by the "orientation' parameter
//	stripsize limits the search area size, for this reason, offsets larger
//	than this cannot be relied on. Smaller stripsizes however will search faster.
//
//	Returns registration position (in x or y) by function return value
//
int CUTSImProc::RegisterImages(
					void *image1, void *image2,		// Supplied images
					int width, int height,			// ... and size
					int minshift, int maxshift,		// Search positions (from, to)
					int stripsize,					// Search area is limited to this size
					LPCTSTR orientation)
{
	ImageRegister im_reg;
	int reg;

	// Search minshift to maxshift
	reg = im_reg.Register((BYTE *)image1, (BYTE *)image2, width, height,
							minshift, maxshift, stripsize, orientation);

	return reg;
}


////////////////////////////////////////////////////////////////////////////////
//
//	Register Images (FFT method)
//
int CUTSImProc::FFTRegisterImages(void *image1, void *image2,
								int cols, int lines,
								int *xshift, int *yshift,
								int maxshift)
{
	CFFT fft;

	fft.fft_image_reg((BYTE *)image1, (BYTE *)image2, cols, lines, xshift, yshift, maxshift);
	return 0;
}

// FFT
int CUTSImProc::FFT(void *image, int width, int height, int *new_width, int *new_height)
{
	int xoff, yoff, newcols, newlines;
	CFFT fft;
	float *fft_image;

	// Size of returned image
	// to ensure fft image has cols and lines which are powers of 2
	fft.get_fft_size(width, height, &xoff, &yoff, &newcols, &newlines);
	
	/* allocate space for fft */
	if ((fft_image = (float *)calloc( 1 + newlines * newcols * 2, sizeof(float) )) == NULL) {
		fprintf(stderr,"float calloc failed\n");
		return -1;
	}

	fft.im_fft((unsigned char *)image, width, height, fft_image, xoff, yoff, newcols, newlines);

	// Need to do something with FFT image here

	// .... for now, just free it off again !!!
	free(fft_image);

	// Return new image dimensions
	*new_width = newcols;
	*new_height = newlines;

	return 0;
}

// FFTPower (Power spectrum image)
int CUTSImProc::FFTPower(void *image, int width, int height, int *new_width, int *new_height)
{
	int xoff, yoff, newcols, newlines;
	CFFT fft;
	float *fft_image;
	float *f_im;
float max, min;
int size;
unsigned char p, *im;

	// Size of returned image
	// to ensure fft image has cols and lines which are powers of 2
	fft.get_fft_size(width, height, &xoff, &yoff, &newcols, &newlines);

	
	/* allocate space for fft */
	if ((fft_image = (float *)calloc( 1 + newlines * newcols * 2, sizeof(float) )) == NULL) {
		fprintf(stderr,"float calloc failed\n");
		return -1;
	}

	fft.fft_power((unsigned char *)image, width, height, fft_image, xoff, yoff, newcols, newlines);

	// Replace input image with power image
	float sr;

	size = newcols * newlines;
	f_im = (fft_image + 8);
	sr = (float)sqrt(sqrt((double)*f_im));
	min = max = sr;

	while (size--) {
		sr = (float)sqrt(sqrt((double)*f_im));

		if (sr > max)
			max = sr;
		else if (sr < min)
			min = sr;

		*f_im = sr;
		f_im++;
	}

	// Scale to max
	float scale = 255 / (max - min);
scale = scale * 10;

	size = newcols * newlines;
	f_im = fft_image;
	im = (unsigned char *)image;

	int n128, n10, n1;

	n128 = n10 = n1 = 0;

	while (size--) {
		p = (unsigned char)((*f_im * scale) - min);
			if (p > 128)
				n128++;
			else if (p > 10)
				n10++;
			else if (p > 1)
				n1++;

		*im = p;

		f_im++;
		im++;
	}

	// Free off float image	
	free(fft_image);

	// Return new image dimensions
	*new_width = newcols;
	*new_height = newlines;


	return 0;
}


