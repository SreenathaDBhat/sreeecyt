///////////////////////////////////////////////////////////////////////////////////////////////
// 8 bit images only
// Modified and completed By HB 10/25/2004
///////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "vcmorphology.h"


///////////////////////////////////////////////////////////////////////////////////////////////
CMorphology::CMorphology()
{

}

CMorphology::~CMorphology()
{

}

// Create a new image
MORPHIMAGE *CMorphology::newimage (int nr, int nc)
{
	int i, j;
	MORPHIMAGE *x=NULL;

	if (nr < 0 || nc < 0)
		return NULL;
	
	if ( !(x = new MORPHIMAGE) )
		return NULL;

	x->nr = nr;       
	x->nc = nc;
	x->oi = x->oj = 0;

	if ( !(x->data = new BYTE*[nr]) ) {
		delete x;
		return NULL;
	}
	
	for (i=0; i<nr; i++) {
		if ( !(x->data[i] = new BYTE[nc]) ) {
			for (int j=0; j<i; j++)
				delete [] x->data[j];
			delete [] x->data;
			delete x;
			return NULL;
		}
	}

    for (i=0; i<x->nr; i++)
		for (j=0; j<x->nc; j++)
			x->data[i][j] = 0;

	return x;
}

// free a image
void CMorphology::freeimage (MORPHIMAGE *z)
{
	int i;
	if (z != 0) {
	   for (i=0; i<z->nr; i++)
	      delete [] z->data[i];
	   delete [] z->data;
	   delete z;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////
// J. R. Parker
// Laboratory for Computer Vision
// University of Calgary
// Calgary, Alberta, Canada
///////////////////////////////////////////////////////////////////////////////////////////////
CMorph::CMorph()
{

}

CMorph::~CMorph()
{

}

// perform a morphological operation
int CMorph::Morph(int op_type, int k_type, BYTE *image, int imwidth, int imheight, 
					   BYTE *se, int sewidth, int seheight)
{
	if ( k_type==1 && sewidth!=seheight )
		k_type = 2;

	int i, j;
	long index;

	MORPHIMAGE *im=NULL, *out=NULL, *kernel=NULL;
	if ( (im=newimage(imheight, imwidth)) == NULL )
		return -1;
	if ( (out=newimage(imheight, imwidth)) == NULL ) {
		freeimage(im);
		return -1;
	}
	if ( (kernel=newimage(seheight, sewidth)) == NULL ) {
		freeimage(im);
		freeimage(out);
		return -1;
	}
	kernel->oi = seheight/2;
	kernel->oj = sewidth/2;

	for (i=0; i<imheight; i++) {
		for (j=0; j<imwidth; j++) {
			index = i*imwidth+j;
			im->data[i][j] = image[index];
		}
	}

	switch (k_type) {
		case 0:	for (i=0; i<seheight; i++) {
					for (j=0; j<sewidth; j++) {
						index = i*sewidth+j;
						kernel->data[i][j] = se[index];
					}
				}
				break;
		case 1: disk_SE(kernel, 1); break;
		case 2: rectangle_SE(kernel, 1); break;
		default: freeimage(im);
				 freeimage(kernel);
				 freeimage(out);
				 return -1;
	}

	switch ( op_type ) {
		case 1: bin_dilate(im, kernel, out); break;				// binary dilation
		case 2: bin_erode(im, kernel, out); break;				// binary erosion
		case 3: if ( bin_close(im, kernel, out)==-1 ) {			// binary close
					freeimage(im);
					freeimage(kernel);
					freeimage(out);
					return -1;
				}
				break;				
		case 4: if ( bin_open(im, kernel, out)==-1 ) {			// binary open
					freeimage(im);
					freeimage(kernel);
					freeimage(out);
					return -1;
				}
				break;				
		case 5: gray_dilate(im, kernel, out); break;			// gray dilation
		case 6: gray_erode(im, kernel, out); break;				// gray erosion
		case 7: if ( gray_close(im, kernel, out)==-1 ) {		// gray close
					freeimage(im);
					freeimage(kernel);
					freeimage(out);
					return -1;
				}
				break;
		case 8: if ( gray_open(im, kernel, out)==-1 ) {			// gray open
					freeimage(im);
					freeimage(kernel);
					freeimage(out);
					return -1;
				}
				break;
		default : 
			freeimage(im);
			freeimage(kernel);
			freeimage(out);
			return -1;
	}

	for (i=0; i<seheight; i++) {
		for (j=0; j<sewidth; j++) {
			index = i*sewidth+j;
			se[index] = kernel->data[i][j];
		}
	}

	for (i=0; i<imheight; i++) {
		for (j=0; j<imwidth; j++) {
			index = i*imwidth+j;
			image[index] = out->data[i][j];
		}
	}

	freeimage(im);
	freeimage(kernel);
	freeimage(out);

	return 0;
}

// Apply a dilation step on one pixel of IM, reult to RES
void CMorph::dil_apply (MORPHIMAGE *im, MORPHIMAGE *p, int ii, int jj, MORPHIMAGE *res)
{
	int i,j, is,js, ie, je, k;

	// Find start and end pixel in IM
	is = ii - p->oi;        js = jj - p->oj;
	ie = is + p->nr;        je = js + p->nc;

	// Place SE over the image from (is,js) to (ie,je). Set pixels in RES
	// if the corresponding SE pixel is 1; else do nothing.
	for (i=is; i<ie; i++)
	  for (j=js; j<je; j++)
	  {
	    if (range(im,i,j))
	    {
	      k = p->data[i-is][j-js];
	      if (k>=0) res->data[i][j] |= k;
	    }
	  }
}

// BIN_DILATE - Dilate the given image using the given structuring element
void CMorph::bin_dilate (MORPHIMAGE *im, MORPHIMAGE *p, MORPHIMAGE *res)
{
	int i, j, ii, jj, is, js, ie, je;
	//BYTE k;

	// Apply the SE to each black pixel of the input
	for (i=0; i<im->nr; i++) {
		is = i - p->oi;
		ie = is + p->nr;
		for (j=0; j<im->nc; j++) {
			if ( im->data[i][j] == 1 ) {
				//dil_apply (im, p, i, j, res);
				// Find start and end pixel in IM
				js = j - p->oj;
				je = js + p->nc;
				for (ii=is; ii<ie; ii++) {
					if ( ii<0 || ii>=res->nr )	continue;
					for (jj=js; jj<je; jj++) {
						if ( jj<0 || jj>=res->nc )	continue;
						//k = p->data[ii-is][jj-js];
						//if ( k>=0 )
						res->data[ii][jj] |= p->data[ii-is][jj-js];
					}
				}
			}
		}
	}
}

// Apply a erosion step on one pixel of IM, reult to RES
void CMorph::erode_apply (MORPHIMAGE *im, MORPHIMAGE *p, int ii, int jj, MORPHIMAGE *res)
{
	int i,j, is,js, ie, je, k, r;

	// Find start and end pixel in IM
	is = ii - p->oi;        js = jj - p->oj;
	ie = is + p->nr;        je = js + p->nc;

	// Place SE over the image from (is,js) to (ie,je). Set pixels in RES
	// if the corresponding pixels in the image agree.
	r = 1;
	for (i=is; i<ie; i++)
	{
	  for (j=js; j<je; j++)
	  {
	    if (range(im,i,j))
	    {
	      k = p->data[i-is][j-js];
	      if ((k == 1) && (im->data[i][j]==0)) r = 0;
	    } else if (p->data[i-is][j-js] != 0) r = 0;
	  }
	}
	res->data[ii][jj] = r;
}

// Erode the image IM using the structuring element P
void CMorph::bin_erode (MORPHIMAGE *im, MORPHIMAGE *p, MORPHIMAGE *res)
{
	int i, j, ii, jj, is, js, ie, je, k, r;

	// Apply the SE to each black pixel of the input
	for (i=0; i<im->nr; i++)
		for (j=0; j<im->nc; j++) {
			is = i - p->oi;        js = j - p->oj;
			ie = is + p->nr;        je = js + p->nc;
			r = 1;
			for (ii=is; ii<ie; ii++)
				for (jj=js; jj<je; jj++) {
					if ( ii<0 || ii>=im->nr || jj<0 || jj>=im->nc ) {
						if (p->data[ii-is][jj-js] != 0) 
							r = 0;
					}
					else {
						k = p->data[ii-is][jj-js];
						if ((k == 1) && (im->data[ii][jj]==0)) 
							r = 0;
					}
				}
			res->data[i][j] = r;
		}
	    //erode_apply (im, p, i, j, res);
}

// close the image IM using the structuring element P
int CMorph::bin_close (MORPHIMAGE *im, MORPHIMAGE *p, MORPHIMAGE *res)
{
	MORPHIMAGE *tmp=NULL;
	if ( (tmp=newimage (im->nr, im->nc)) == NULL )
		return -1;

	bin_dilate(im, p, tmp);
	bin_erode(tmp, p, res);

	freeimage(tmp);

	return 0;
}

// open the image IM using the structuring element P
int CMorph::bin_open (MORPHIMAGE *im, MORPHIMAGE *p, MORPHIMAGE *res)
{
	MORPHIMAGE *tmp=NULL;
	if ( (tmp=newimage (im->nr, im->nc)) == NULL )
		return -1;

	bin_erode(im, p, tmp);
	bin_dilate(tmp, p, res);

	freeimage(tmp);

	return 0;
}

// Gray level dilation
void CMorph::gray_dilate (MORPHIMAGE *a, MORPHIMAGE *se, MORPHIMAGE *res)
{
	int i,j,ii,jj,k,n;

	for (i=0; i<a->nr; i++)
	  for (j=0; j<a->nc; j++)
	  {
	    k = -10000;
	    for (ii=0; ii<se->nr; ii++)
	      for (jj=0; jj<se->nc; jj++)
	      {
			if ( i+ii-se->oi<0 || i+ii-se->oi>=a->nr )
				continue;
			if ( j+jj-se->oj<0 || j+jj-se->oj>=a->nc )
				continue;
			n = se->data[ii][jj] + a->data[i+(ii-se->oi)][j+(jj-se->oj)];
			if (n > k) k = n;
	      }
		  if (k > 255)
			res->data[i][j] = 255;
		  else if (k<0) 
			res->data[i][j] = 0;
	      else res->data[i][j] = k;
	  }
}

// Gray level erosion
void CMorph::gray_erode (MORPHIMAGE *a, MORPHIMAGE *se, MORPHIMAGE *res)
{
	int i,j,ii,jj,k,n;

	for (i=0; i<a->nr; i++)
	  for (j=0; j<a->nc; j++) {
	    k = 10000;
	    for (ii=0; ii<se->nr; ii++)
	      for (jj=0; jj<se->nc; jj++) {
		    if ( i+ii-se->oi<0 || i+ii-se->oi>=a->nr )
				continue;
			if ( j+jj-se->oj<0 || j+jj-se->oj>=a->nc )
				continue;
			n =  a->data[i+(ii-se->oi)][j+(jj-se->oj)] - se->data[ii][jj];
			if (n < k) 
				k = n;
		  }
	    if (k > 255)
	      res->data[i][j] = k;
	    else if (k<0) res->data[i][j] = 0;
	    else res->data[i][j] = k;
	  }
}

// close the image IM using the structuring element P
int CMorph::gray_close (MORPHIMAGE *im, MORPHIMAGE *p, MORPHIMAGE *res)
{
	MORPHIMAGE *tmp=NULL;
	if ( (tmp=newimage (im->nr, im->nc)) == NULL )
		return -1;

	gray_dilate(im, p, tmp);
	gray_erode(tmp, p, res);

	freeimage(tmp);

	return 0;
}


// open the image IM using the structuring element P
int CMorph::gray_open (MORPHIMAGE *im, MORPHIMAGE *p, MORPHIMAGE *res)
{
	MORPHIMAGE *tmp=NULL;
	if ( (tmp=newimage (im->nr, im->nc)) == NULL )
		return -1;

	gray_erode(im, p, tmp);
	gray_dilate(tmp, p, res);

	freeimage(tmp);

	return 0;
}

// Check that a pixel index is in range. Return TRUE(1) if so.
int CMorph::range (MORPHIMAGE *im, int i, int j)
{
	if ((i<0) || (i>=im->nr)) return 0;
	if ((j<0) || (j>=im->nc)) return 0;
	return 1;
}



// Create a flat disk SE 
void CMorph::disk_SE(MORPHIMAGE *stru_elemP, BYTE fgnd)
{
  	int 	x_ctr, y_ctr;
	int 	i, j;
 	float 	r, d=(float)stru_elemP->nr;
	
  	r = d/(float)2.0;
  	r = r*r;
  	x_ctr = (int)d/2; 
  	y_ctr = (int)d/2;

	for (i=0; i < stru_elemP->nr; i++)
         for (j=0; j < stru_elemP->nc; j++)
			if ((i-y_ctr)*(i-y_ctr)+(j-x_ctr)*(j-x_ctr) <= r)
				stru_elemP->data[i][j] = fgnd;
}

// Create a flat rectangular SE 
void CMorph::rectangle_SE(MORPHIMAGE *stru_elemP, BYTE fgnd)
{
	int i, j;

	for (i=0; i < stru_elemP->nr; i++)
         for (j=0; j < stru_elemP->nc; j++)
            stru_elemP->data[i][j] = fgnd;
}
///////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////
// J. R. Parker
// Laboratory for Computer Vision
// University of Calgary
// Calgary, Alberta, Canada
//
// Zhang & Suen/Stentiford/Holt combined algorithm (ISBN 0-471-14056-2)
// Zhang & Suen's thinnning algorithm
// Stentiford's pre-processing
// Holt's post-processing
///////////////////////////////////////////////////////////////////////////////////////////////
CForceThinning::CForceThinning()
{

}

CForceThinning::~CForceThinning()
{

}

int CForceThinning::ForceThin(BYTE *image, int imwidth, int imheight)
{
	MORPHIMAGE *im;
	int i,j;
	long index;

	// Embed input into a slightly larger image
	if ( (im=newimage(imheight+2, imwidth+2)) == NULL )
		return -1;
	for (i=0; i<imheight; i++) {
		for (j=0; j<imwidth; j++) {
			index = i*imwidth+j;
			if ( !image[index] )
				im->data[i+1][j+1] = 1;
		}
	}
	for (i=0; i<im->nr; i++) {
	  im->data[i][0] = 1;
	  im->data[i][im->nc-1] = 1;
	}
	for (j=0; j<im->nc; j++) {
	  im->data[0][j] = 1;
	  im->data[im->nr-1][j] = 1;
	}

	// Pre_process
	pre_smooth (im);
	aae (im);

	// Thin
	if ( thnz (im) == -1 ) {
		freeimage(im);
		return -1;
	}

	for (i=0; i<imheight; i++)
		for (j=0; j<imwidth; j++) {
			index = i*imwidth+j;
			image[index] = !(im->data[i+1][j+1]);
		}

	freeimage(im);

	return 0;
}

//	Zhang-Suen with Holt's staircase removal
int CForceThinning::thnz (MORPHIMAGE *im)
{
	int i,j,k, again=1;
	MORPHIMAGE *tmp;

	if ( (tmp=newimage (im->nr, im->nc)) == NULL )
		return -1;

	// BLACK = 1, WHITE = 0.
	for (i=0; i<im->nr; i++)
	  for (j=0; j<im->nc; j++)
	  {
	    if (im->data[i][j] > 0) im->data[i][j] = 0;
	      else im->data[i][j] = 1;
	    tmp->data[i][j] = 0;
	  }

	// Mark and delete
	while (again)
	{
	  again = 0;

	// Second sub-iteration
	for (i=1; i<im->nr-1; i++)
	  for (j=1; j<im->nc-1; j++)
	  {
	    if (im->data[i][j] != 1) continue;
	    k = nays8(im, i, j);
	    if ((k >= 2 && k <= 6) && Connectivity(im, i,j)==1)
	    {
	      if (im->data[i][j+1]*im->data[i-1][j]*im->data[i][j-1] == 0 &&
		  im->data[i-1][j]*im->data[i+1][j]*im->data[i][j-1] == 0)
		{
		  tmp->data[i][j] = 1;
		  again = 1;
		}
	    }
	  }

	  Delete (im, tmp);
	  if (again == 0) break;

	// First sub-iteration
	for (i=1; i<im->nr-1; i++)
	  for (j=1; j<im->nc-1; j++)
	  {
	    if (im->data[i][j] != 1) continue;
	    k = nays8(im, i, j);
	    if ((k >= 2 && k <= 6) && Connectivity(im, i,j)==1)
	    {
	      if (im->data[i-1][j]*im->data[i][j+1]*im->data[i+1][j] == 0 &&
		  im->data[i][j+1]*im->data[i+1][j]*im->data[i][j-1] == 0)
		{
		  tmp->data[i][j] = 1;
		  again = 1;
		}
	    }
	  }

	  Delete (im, tmp);
	}

	// Post_process
    stair (im, tmp, 1);
    Delete (im, tmp);
    stair (im, tmp, 3);
    Delete (im, tmp);

	// Restore levels
	for (i=1; i<im->nr-1; i++)
	  for (j=1; j<im->nc-1; j++)
	  if (im->data[i][j] > 0) im->data[i][j] = 0;
	    else im->data[i][j] = 255;

	freeimage (tmp);

	return 0;
}

//	Delete any pixel in IM corresponding to a 1 in TMP
void CForceThinning::Delete (MORPHIMAGE *im, MORPHIMAGE *tmp)
{
	int i,j;

	// Delete pixels that are marked
	for (i=1; i<im->nr-1; i++)
	  for (j=1; j<im->nc-1; j++)
	    if (tmp->data[i][j])
	    {
	        im->data[i][j] = 0;
		tmp->data[i][j] = 0;
	    }
}

//	Number of neighboring 1 pixels
int CForceThinning::nays8 (MORPHIMAGE *im, int r, int c)
{
	int i,j,k=0;

	for (i=r-1; i<=r+1; i++)
	  for (j=c-1; j<=c+1; j++)
	    if (i!=r || c!=j)
	      if (im->data[i][j] >= 1) k++;
	return k;
}

//	Number of neighboring 0 pixels
int CForceThinning::snays (MORPHIMAGE *im, int r, int c)
{
        int i,j,k=0;

	for (i=r-1; i<=r+1; i++)
	  for (j=c-1; j<=c+1; j++)
	    if (i!=r || c!=j)
	      if (im->data[i][j] == 0) k++;
	return k;
}

//	Connectivity by counting black-white transitions on the boundary
int CForceThinning::Connectivity (MORPHIMAGE *im, int r, int c)
{
	int N=0;

	if (im->data[r][c+1]   >= 1 && im->data[r-1][c+1] == 0) N++;
	if (im->data[r-1][c+1] >= 1 && im->data[r-1][c]   == 0) N++;
	if (im->data[r-1][c]   >= 1 && im->data[r-1][c-1] == 0) N++;
	if (im->data[r-1][c-1] >= 1 && im->data[r][c-1]   == 0) N++;
	if (im->data[r][c-1]   >= 1 && im->data[r+1][c-1] == 0) N++;
	if (im->data[r+1][c-1] >= 1 && im->data[r+1][c]   == 0) N++;
	if (im->data[r+1][c]   >= 1 && im->data[r+1][c+1] == 0) N++;
	if (im->data[r+1][c+1] >= 1 && im->data[r][c+1]   == 0) N++;

	return N;
}

//	Stentiford's boundary smoothing method
void CForceThinning::pre_smooth (MORPHIMAGE *im)
{
	int i,j;

	for (i=0; i<im->nr; i++)
	  for (j=0; j<im->nc; j++)
	    if (im->data[i][j] == 0)
		if (snays(im, i, j) <= 2 && Yokoi (im, i, j)<2)
		  im->data[i][j] = 2;

	for (i=0; i<im->nr; i++)
	  for (j=0; j<im->nc; j++)
	    if (im->data[i][j] == 2) im->data[i][j] = 1;
}

//	Stentiford's Acute Angle Emphasis
void CForceThinning::aae (MORPHIMAGE *im)
{
	int i,j, again = 0, k;

	again = 0;
	for (k=5; k>= 1; k-=2)
	{
	  for (i=2; i<im->nr-2; i++)
	    for (j=2; j<im->nc-2; j++)
	      if (im->data[i][j] == 0)
		match_du (im, i, j, k);

	  for (i=2; i<im->nr-2; i++)
	    for (j=2; j<im->nc-2; j++)
	    if (im->data[i][j] == 2)
	    {
		again = 1;
		im->data[i][j] = 1;
	    }

	  if (again == 0) break;
	} 
}

//	Template matches for acute angle emphasis
void CForceThinning::match_du (MORPHIMAGE *im, int r, int c, int k)
{
	// D1
	if (im->data[r-2][c-2] == 0 && im->data[r-2][c-1] == 0 &&
	    im->data[r-2][c]   == 1 && im->data[r-2][c+1] == 0 &&
	    im->data[r-2][c+2] == 0 &&
	    im->data[r-1][c-2] == 0 && im->data[r-1][c-1] == 0 &&
	    im->data[r-1][c]   == 1 && im->data[r-1][c+1] == 0 &&
	    im->data[r-1][c+2] == 0 &&
	    im->data[r][c-2] == 0 && im->data[r][c-1] == 0 &&
	    im->data[r][c]   == 0 && im->data[r][c+1] == 0 &&
	    im->data[r][c+2] == 0 &&
	    im->data[r+1][c-2] == 0 && im->data[r+1][c-1] == 0 &&
	    im->data[r+1][c]   == 0 && im->data[r+1][c+1] == 0 &&
	    im->data[r+1][c+2] == 0 &&
	    im->data[r+2][c-1] == 0 &&
	    im->data[r+2][c]   == 0 && im->data[r+2][c+1] == 0 )
	{
		im->data[r][c] = 2;
		return;
	}

	// D2
	if (k >= 2)
	if (im->data[r-2][c-2] == 0 && im->data[r-2][c-1] == 1 &&
	    im->data[r-2][c]   == 1 && im->data[r-2][c+1] == 0 &&
	    im->data[r-2][c+2] == 0 &&
	    im->data[r-1][c-2] == 0 && im->data[r-1][c-1] == 0 &&
	    im->data[r-1][c]   == 1 && im->data[r-1][c+1] == 0 &&
	    im->data[r-1][c+2] == 0 &&
	    im->data[r][c-2] == 0 && im->data[r][c-1] == 0 &&
	    im->data[r][c]   == 0 && im->data[r][c+1] == 0 &&
	    im->data[r][c+2] == 0 &&
	    im->data[r+1][c-2] == 0 && im->data[r+1][c-1] == 0 &&
	    im->data[r+1][c]   == 0 && im->data[r+1][c+1] == 0 &&
	    im->data[r+1][c+2] == 0 &&
	    im->data[r+2][c-1] == 0 &&
	    im->data[r+2][c]   == 0 && im->data[r+2][c+1] == 0 )
	{
		im->data[r][c] = 2;
		return;
	}

	// D3
	if (k>=3)
	if (im->data[r-2][c-2] == 0 && im->data[r-2][c-1] == 0 &&
	    im->data[r-2][c]   == 1 && im->data[r-2][c+1] == 1 &&
	    im->data[r-2][c+2] == 0 &&
	    im->data[r-1][c-2] == 0 && im->data[r-1][c-1] == 0 &&
	    im->data[r-1][c]   == 1 && im->data[r-1][c+1] == 0 &&
	    im->data[r-1][c+2] == 0 &&
	    im->data[r][c-2] == 0 && im->data[r][c-1] == 0 &&
	    im->data[r][c]   == 0 && im->data[r][c+1] == 0 &&
	    im->data[r][c+2] == 0 &&
	    im->data[r+1][c-2] == 0 && im->data[r+1][c-1] == 0 &&
	    im->data[r+1][c]   == 0 && im->data[r+1][c+1] == 0 &&
	    im->data[r+1][c+2] == 0 &&
	    im->data[r+2][c-1] == 0 &&
	    im->data[r+2][c]   == 0 && im->data[r+2][c+1] == 0 )
	{
		im->data[r][c] = 2;
		return;
	}

	// D4
	if (k>=4)
	if (im->data[r-2][c-2] == 0 && im->data[r-2][c-1] == 1 &&
	    im->data[r-2][c]   == 1 && im->data[r-2][c+1] == 0 &&
	    im->data[r-2][c+2] == 0 &&
	    im->data[r-1][c-2] == 0 && im->data[r-1][c-1] == 1 &&
	    im->data[r-1][c]   == 1 && im->data[r-1][c+1] == 0 &&
	    im->data[r-1][c+2] == 0 &&
	    im->data[r][c-2] == 0 && im->data[r][c-1] == 0 &&
	    im->data[r][c]   == 0 && im->data[r][c+1] == 0 &&
	    im->data[r][c+2] == 0 &&
	    im->data[r+1][c-2] == 0 && im->data[r+1][c-1] == 0 &&
	    im->data[r+1][c]   == 0 && im->data[r+1][c+1] == 0 &&
	    im->data[r+1][c+2] == 0 &&
	    im->data[r+2][c-1] == 0 &&
	    im->data[r+2][c]   == 0 && im->data[r+2][c+1] == 0 )
	{
		im->data[r][c] = 2;
		return;
	}

	// D5
	if (k>=5)
	if (im->data[r-2][c-2] == 0 && im->data[r-2][c-1] == 0 &&
	    im->data[r-2][c]   == 1 && im->data[r-2][c+1] == 1 &&
	    im->data[r-2][c+2] == 0 &&
	    im->data[r-1][c-2] == 0 && im->data[r-1][c-1] == 0 &&
	    im->data[r-1][c]   == 1 && im->data[r-1][c+1] == 1 &&
	    im->data[r-1][c+2] == 0 &&
	    im->data[r][c-2] == 0 && im->data[r][c-1] == 0 &&
	    im->data[r][c]   == 0 && im->data[r][c+1] == 0 &&
	    im->data[r][c+2] == 0 &&
	    im->data[r+1][c-2] == 0 && im->data[r+1][c-1] == 0 &&
	    im->data[r+1][c]   == 0 && im->data[r+1][c+1] == 0 &&
	    im->data[r+1][c+2] == 0 &&
	    im->data[r+2][c-1] == 0 &&
	    im->data[r+2][c]   == 0 && im->data[r+2][c+1] == 0 )
	{
		im->data[r][c] = 2;
		return;
	}

	// U1
	if (im->data[r+2][c-2] == 0 && im->data[r+2][c-1] == 0 &&
	    im->data[r+2][c]   == 1 && im->data[r+2][c+1] == 0 &&
	    im->data[r+2][c+2] == 0 &&
	    im->data[r+1][c-2] == 0 && im->data[r+1][c-1] == 0 &&
	    im->data[r+1][c]   == 1 && im->data[r+1][c+1] == 0 &&
	    im->data[r+1][c+2] == 0 &&
	    im->data[r][c-2] == 0 && im->data[r][c-1] == 0 &&
	    im->data[r][c]   == 0 && im->data[r][c+1] == 0 &&
	    im->data[r][c+2] == 0 &&
	    im->data[r-1][c-2] == 0 && im->data[r-1][c-1] == 0 &&
	    im->data[r-1][c]   == 0 && im->data[r-1][c+1] == 0 &&
	    im->data[r-1][c+2] == 0 &&
	    im->data[r-1][c-1] == 0 &&
	    im->data[r-1][c]   == 0 && im->data[r-1][c+1] == 0 )
	{
		im->data[r][c] = 2;
		return;
	}

	// U2
	if (k>=2)
	if (im->data[r+2][c-2] == 0 && im->data[r+2][c-1] == 1 &&
	    im->data[r+2][c]   == 1 && im->data[r+2][c+1] == 0 &&
	    im->data[r+2][c+2] == 0 &&
	    im->data[r+1][c-2] == 0 && im->data[r+1][c-1] == 0 &&
	    im->data[r+1][c]   == 1 && im->data[r+1][c+1] == 0 &&
	    im->data[r+1][c+2] == 0 &&
	    im->data[r][c-2] == 0 && im->data[r][c-1] == 0 &&
	    im->data[r][c]   == 0 && im->data[r][c+1] == 0 &&
	    im->data[r][c+2] == 0 &&
	    im->data[r-1][c-2] == 0 && im->data[r-1][c-1] == 0 &&
	    im->data[r-1][c]   == 0 && im->data[r-1][c+1] == 0 &&
	    im->data[r-1][c+2] == 0 &&
	    im->data[r-2][c-1] == 0 &&
	    im->data[r-2][c]   == 0 && im->data[r-2][c+1] == 0 )
	{
		im->data[r][c] = 2;
		return;
	}

	// U3
	if (k>=3)
	if (im->data[r+2][c-2] == 0 && im->data[r+2][c-1] == 0 &&
	    im->data[r+2][c]   == 1 && im->data[r+2][c+1] == 1 &&
	    im->data[r+2][c+2] == 0 &&
	    im->data[r+1][c-2] == 0 && im->data[r+1][c-1] == 0 &&
	    im->data[r+1][c]   == 1 && im->data[r+1][c+1] == 0 &&
	    im->data[r+1][c+2] == 0 &&
	    im->data[r][c-2] == 0 && im->data[r][c-1] == 0 &&
	    im->data[r][c]   == 0 && im->data[r][c+1] == 0 &&
	    im->data[r][c+2] == 0 &&
	    im->data[r-1][c-2] == 0 && im->data[r-1][c-1] == 0 &&
	    im->data[r-1][c]   == 0 && im->data[r-1][c+1] == 0 &&
	    im->data[r-1][c+2] == 0 &&
	    im->data[r-2][c-1] == 0 &&
	    im->data[r-2][c]   == 0 && im->data[r-2][c+1] == 0 )
	{
		im->data[r][c] = 2;
		return;
	}

	// U4
	if (k>=4)
	if (im->data[r+2][c-2] == 0 && im->data[r+2][c-1] == 1 &&
	    im->data[r+2][c]   == 1 && im->data[r+2][c+1] == 0 &&
	    im->data[r+2][c+2] == 0 &&
	    im->data[r+1][c-2] == 0 && im->data[r+1][c-1] == 1 &&
	    im->data[r+1][c]   == 1 && im->data[r+1][c+1] == 0 &&
	    im->data[r+1][c+2] == 0 &&
	    im->data[r][c-2] == 0 && im->data[r][c-1] == 0 &&
	    im->data[r][c]   == 0 && im->data[r][c+1] == 0 &&
	    im->data[r][c+2] == 0 &&
	    im->data[r-1][c-2] == 0 && im->data[r-1][c-1] == 0 &&
	    im->data[r-1][c]   == 0 && im->data[r-1][c+1] == 0 &&
	    im->data[r-1][c+2] == 0 &&
	    im->data[r-2][c-1] == 0 &&
	    im->data[r-2][c]   == 0 && im->data[r-2][c+1] == 0 )
	{
		im->data[r][c] = 2;
		return;
	}

	// U5
	if (k>=5)
	if (im->data[r+2][c-2] == 0 && im->data[r+2][c-1] == 0 &&
	    im->data[r+2][c]   == 1 && im->data[r+2][c+1] == 1 &&
	    im->data[r+2][c+2] == 0 &&
	    im->data[r+1][c-2] == 0 && im->data[r+1][c-1] == 0 &&
	    im->data[r+1][c]   == 1 && im->data[r+1][c+1] == 1 &&
	    im->data[r+1][c+2] == 0 &&
	    im->data[r][c-2] == 0 && im->data[r][c-1] == 0 &&
	    im->data[r][c]   == 0 && im->data[r][c+1] == 0 &&
	    im->data[r][c+2] == 0 &&
	    im->data[r-1][c-2] == 0 && im->data[r-1][c-1] == 0 &&
	    im->data[r-1][c]   == 0 && im->data[r-1][c+1] == 0 &&
	    im->data[r-1][c+2] == 0 &&
	    im->data[r-2][c-1] == 0 &&
	    im->data[r-2][c]   == 0 && im->data[r-2][c+1] == 0 )
	{
		im->data[r][c] = 2;
		return;
	}
}

//	Yokoi's connectivity measure
int CForceThinning::Yokoi (MORPHIMAGE *im, int r, int c)
{
	int N[9];
	int i, k, i1, i2;

	N[0] = im->data[r][c]      != 0;
	N[1] = im->data[r][c+1]    != 0;
	N[2] = im->data[r-1][c+1]  != 0;
	N[3] = im->data[r-1][c]    != 0;
	N[4] = im->data[r-1][c-1]  != 0;
	N[5] = im->data[r][c-1]    != 0;
	N[6] = im->data[r+1][c-1]  != 0;
	N[7] = im->data[r+1][c]    != 0;
	N[8] = im->data[r+1][c+1]  != 0;

	k = 0;
	for (i=1; i<=7; i+=2)
	{
	  i1 = i+1; if (i1 > 8) i1 -= 8;
	  i2 = i+2; if (i2 > 8) i2 -= 8;
	  k += (N[i] - N[i]*N[i1]*N[i2]);
	}

	return k;
}

//	Holt's staircase removal stuff
void CForceThinning::check (int v1, int v2, int v3)
{
	if (!v2 && (!v1 || !v3)) t00 = 1;
	if ( v2 && ( v1 ||  v3)) t11 = 1;
	if ( (!v1 && v2) || (!v2 && v3) )
	{
		t01s = t01;
		t01  = 1;
	}
}

int CForceThinning::edge (MORPHIMAGE *im, int r, int c)
{
	if (im->data[r][c] == 0) return 0;
	t00 = t01 = t01s = t11 = 0;

	// CHECK(vNW, vN, vNE)
	check (im->data[r-1][c-1], im->data[r-1][c], im->data[r-1][c+1]);

	// CHECK (vNE, vE, vSE)
	check (im->data[r-1][c+1], im->data[r][c+1], im->data[r+1][c+1]);

	// CHECK (vSE, vS, vSW)
	check (im->data[r+1][c+1], im->data[r+1][c], im->data[r+1][c-1]);

	// CHECK (vSW, vW, vNW)
	check (im->data[r+1][c-1], im->data[r][c-1], im->data[r-1][c-1]);

	return t00 && t11 && !t01s;
}

void CForceThinning::stair (MORPHIMAGE *im, MORPHIMAGE *tmp, int dir)
{
	int i,j;
	int N, S, E, W, NE, NW, SE, SW, C;

	if (dir == 1)
	for (i=1; i<im->nr-1; i++)
	  for (j=1; j<im->nc-1; j++)
	  {
	   NW = im->data[i-1][j-1]; N = im->data[i-1][j]; NE = im->data[i-1][j+1];
	   W = im->data[i][j-1]; C = im->data[i][j]; E = im->data[i][j+1];
	   SW = im->data[i+1][j-1]; S = im->data[i+1][j]; SE = im->data[i+1][j+1];

	   if (dir == 1)
	   {
	     if (C && !(N && 
		      ((E && !NE && !SW && (!W || !S)) || 
		       (W && !NW && !SE && (!E || !S)) )) )
	       tmp->data[i][j] = 0;		/* Survives */
	     else
	       tmp->data[i][j] = 1;
	   } else if (dir == 3)
	   {
	     if (C && !(S && 
		      ((E && !SE && !NW && (!W || !N)) || 
		       (W && !SW && !NE && (!E || !N)) )) )
	       tmp->data[i][j] = 0;		/* Survives */
	     else
	       tmp->data[i][j] = 1;
	   }
	  }
}


///////////////////////////////////////////////////////////////////////////////////////////////
