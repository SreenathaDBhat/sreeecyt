///////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 1993 SIUE - by Kun Luo (Southern Illinois University @ Edwardsville)
//
// Permission to use, copy, modify, and distribute this software and its
// documentation for any purpose and without fee is hereby granted, provided
// that the above copyright notice appear in all copies and that both that
// copyright notice and this permission notice appear in supporting
// documentation.  This software is provided "as is" without express or
// implied warranty.

///////////////////////////////////////////////////////////////////////////////////////////////
// Color Segmentation based on SCT/Center Split algorithm
// 8 bit images only
// 
// Adapted and Modified By Homayoun Bagherinia 08/05/2004
///////////////////////////////////////////////////////////////////////////////////////////////

#include <math.h>

#include "stdafx.h"
#include "vcmemrect.h"
#include "vcsctcentersplitsegment.h"

#define MAX(a,b)	(((a)>(b))?(a):(b))
#define MIN(a,b)	(((a)<(b))?(a):(b))	

///////////////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////////////////////
CSctCenterSplitSegment::CSctCenterSplitSegment(CUTSMemRect *pMR)
{
	pUTSMemRect = pMR;
}

CSctCenterSplitSegment::~CSctCenterSplitSegment(void)
{
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Color Segmentation based on SCT/Center Split algorithm
///////////////////////////////////////////////////////////////////////////////////////////////
int CSctCenterSplitSegment::sct_split_segment(BYTE *R, BYTE *G, BYTE *B, int width, int height, 
											  int A_split, int B_split)
{
   	unsigned int 	rows, cols;
	long			imgdim;
	float			a_avg[2],b_avg[2];
   	register long 	i;
   	float 			**cvecP;
   	BYTE 			**origP;
   	BYTE 			*splitP_a, *splitP_b, **ravg, **gavg, **bavg;

	// simplify some variables
   	rows 	= height;
   	cols 	= width;
   	imgdim = rows*cols;

	if ( (ravg=(BYTE**)malloc(A_split*sizeof(BYTE*))) == NULL )
		return -1;
	if ( (gavg=(BYTE**)malloc(A_split*sizeof(BYTE*))) == NULL )
	{
		free(ravg);
		return -1;
	}
	if ( (bavg=(BYTE**)malloc(A_split*sizeof(BYTE*))) == NULL )
	{
		free(ravg); free(gavg);
		return -1;
	}
	if ( (*ravg=(BYTE*)malloc(B_split*A_split*sizeof(BYTE))) == NULL )
	{
		free(ravg); free(gavg); free(bavg);
		return -1;
	}
	if ( (*gavg = (BYTE*)malloc(B_split*A_split*sizeof(BYTE))) == NULL )
	{
		free(*ravg);
		free(ravg); free(gavg); free(bavg); free(*ravg);
		return -1;
	}
	if ( (*bavg = (BYTE*)malloc(B_split*A_split*sizeof(BYTE))) == NULL )
	{
		free(*ravg); free(*gavg);
		free(ravg); free(gavg); free(bavg); 
		return -1;
	}

	for (i=1;i<A_split;i++)
	{
	    ravg[i] = &ravg[0][B_split*i];
	    gavg[i] = &gavg[0][B_split*i];
	    bavg[i] = &bavg[0][B_split*i];
	}

	if ( (cvecP=(float **) malloc(3*sizeof(float *))) == NULL )
	{
		free(*ravg); free(*gavg); free(*bavg);
		free(ravg); free(gavg); free(bavg); 
		return -1;
	}
	for ( i=0; i<3; i++ ) {
		if ( (cvecP[i]=(float*)malloc(imgdim*sizeof(float))) == NULL )
		{
			for ( int j=0; j<i; j++ )
				free(cvecP[j]);
			free(cvecP);
			free(*ravg); free(*gavg); free(*bavg);
			free(ravg); free(gavg); free(bavg); 
			return -1;
		}
	}
	for ( i=0; i<imgdim; i++ )
	{
		cvecP[0][i] = (float) R[i];
		cvecP[1][i] = (float) G[i]; 
		cvecP[2][i] = (float) B[i];
	}
	spher_xform(cvecP, imgdim);

   if ( (origP=(BYTE **) malloc(sizeof(BYTE *)*3)) == NULL )
   {
		for ( i=0; i<3; i++ )
			free(cvecP[i]);
		free(cvecP);
		free(*ravg); free(*gavg); free(*bavg);
		free(ravg); free(gavg); free(bavg); 
		return -1;
   }

	origP[0] = R;
	origP[1] = G;
	origP[2] = B;
   sctMinMax(cvecP, imgdim, a_avg, b_avg, A_split, B_split);

   if ( (splitP_a=(BYTE *) malloc(imgdim*sizeof(BYTE))) == NULL ) 
   {
		for ( i=0; i<3; i++ )
			free(cvecP[i]);
		free(cvecP);
		free(*ravg); free(*gavg); free(*bavg);
		free(ravg); free(gavg); free(bavg); 
		free(origP);
		return -1;
   }
   if ( (splitP_b=(BYTE *) malloc(imgdim*sizeof(BYTE))) == NULL )
   {
		for ( i=0; i<3; i++ )
			free(cvecP[i]);
		free(cvecP);
		free(*ravg); free(*gavg); free(*bavg);
		free(ravg); free(gavg); free(bavg); 
		free(origP);
		free(splitP_a);
		return -1;
   }
   colorSplit(cvecP, splitP_a, splitP_b, imgdim, a_avg, b_avg);
   	
   findStats(origP, splitP_a, splitP_b, imgdim, A_split, B_split, ravg, gavg, bavg);

   for(i=0; i < imgdim; i++)
		if(splitP_a[i] != 255) {
      		R[i] = ravg[splitP_a[i]][splitP_b[i]];
      		G[i] = gavg[splitP_a[i]][splitP_b[i]];
      		B[i] = bavg[splitP_a[i]][splitP_b[i]];
		}

	for ( i=0; i<3; i++ )
		free(cvecP[i]);
	free(cvecP);
	free(origP);
	free(splitP_a); free(splitP_b);
	free(*ravg); free(*gavg); free(*bavg);
	free(ravg); free(gavg); free(bavg);

	return 0;
} 

///////////////////////////////////////////////////////////////////////////////////////////////
// 
///////////////////////////////////////////////////////////////////////////////////////////////
void CSctCenterSplitSegment::sctMinMax(float **	svecP, unsigned int vdim,float *a_avg,
									   float *b_avg, int A_split, int B_split)
{
	unsigned int 	i;
	float amin=450, amax=0, bmin=450, bmax=0;
	for ( i=0; i < vdim; i++)
   		if (svecP[0][i] != 0 ) 
		{
       		amax = MAX(amax, svecP[1][i]);
      		bmax = MAX(bmax, svecP[2][i]);
      		amin = MIN(amin, svecP[1][i]);
      		bmin = MIN(bmin, svecP[2][i]);
		}
	a_avg[0] = amin ;
	a_avg[1] = (amax-amin)/A_split;
	b_avg[0] = bmin ;
	b_avg[1] = (bmax-bmin)/B_split;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// 
///////////////////////////////////////////////////////////////////////////////////////////////
void CSctCenterSplitSegment::colorSplit(float **cvecP, BYTE *splitP_a, BYTE *splitP_b, 
										unsigned int vdim, float *a_avg, float *b_avg)
{
 	unsigned int 	i=0;
	float temp;

 	for (; i<vdim; i++, splitP_a++,splitP_b++)
     	if (cvecP[0][i] != 0) 
		{
			temp = cvecP[1][i] - a_avg[0];
			temp /= a_avg[1];
			temp -= (float)0.1;
			if(temp<0.0)
			    temp = 0;
			*splitP_a = (BYTE)temp;
			temp = cvecP[2][i] - b_avg[0];
			temp /= b_avg[1];
			temp -= (float)0.1;
			if(temp<0.0)
			    temp = 0;
			*splitP_b = (BYTE)temp; 
     	}
     	else
        	*splitP_a = *splitP_b = 255;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// 
///////////////////////////////////////////////////////////////////////////////////////////////
int CSctCenterSplitSegment::findStats(BYTE **cvecP, BYTE *splitP_a, BYTE *splitP_b,
									   unsigned int vdim, int A_split, int B_split,
                                       BYTE **r_avg, BYTE **g_avg, BYTE **b_avg)
{
 	long 		**rsum, **bsum, **gsum, **count;

	if ( (rsum=(long**)malloc(A_split*sizeof(long*))) == NULL )
		return -1;
	if ( (gsum=(long**)malloc(A_split*sizeof(long*))) == NULL )
	{
		free(rsum);
		return -1;
	}
	if ( (bsum=(long**)malloc(A_split*sizeof(long*))) == NULL )
	{
		free(rsum); free(gsum);
		return -1;
	}
	if ( (count=(long**)malloc(A_split*sizeof(long*))) == NULL )
	{
		free(rsum); free(gsum); free(bsum);
		return -1;
	}
	if ( (*rsum=(long*)calloc(B_split*A_split,sizeof(long))) == NULL )
	{
		free(rsum); free(gsum); free(bsum); free(count);
		return -1;
	}
	if ( (*gsum=(long*)calloc(B_split*A_split,sizeof(long))) == NULL )
	{
		free(rsum); free(gsum); free(bsum); free(count); 
		free(*rsum);
		return -1;
	}
	if ( (*bsum=(long*)calloc(B_split*A_split,sizeof(long))) == NULL )
	{
		free(rsum); free(gsum); free(bsum); free(count); 
		free(*rsum); free(*gsum); 
		return -1;
	}
	if ( (*count=(long*)calloc(B_split*A_split,sizeof(long))) == NULL )
	{
		free(rsum); free(gsum); free(bsum); free(count); 
		free(*rsum); free(*gsum); free(*bsum);
		return -1;
	}

	for(int i=1;i<A_split;i++)
	{
	    rsum[i] = &rsum[0][B_split*i];
	    gsum[i] = &gsum[0][B_split*i];
	    bsum[i] = &bsum[0][B_split*i];
	    count[i] = &count[0][B_split*i];
    }


 	for (unsigned int i=0; i<vdim; i++, splitP_a++,splitP_b++)
		if (*splitP_a != 255) 
		{
        	rsum[*splitP_a][*splitP_b] += cvecP[0][i];
        	gsum[*splitP_a][*splitP_b] += cvecP[1][i];
        	bsum[*splitP_a][*splitP_b] += cvecP[2][i];
        	count[*splitP_a][*splitP_b]++;
 		}

 	for (int j=0; j < A_split; j++) 
	{
	    for(int i=0; i< B_split;i++)
		{
			if(count[j][i] == 0)
				count[j][i] = 1;
    		r_avg[j][i] = (BYTE)(rsum[j][i]/count[j][i]);
    		g_avg[j][i] = (BYTE)(gsum[j][i]/count[j][i]);
    		b_avg[j][i] = (BYTE)(bsum[j][i]/count[j][i]);
	    }
 	}

	free(*rsum); free(*bsum); free(*gsum);
	free(*count);
	free(rsum); free(bsum); free(gsum);
	free(count);

	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Performs RGB to SCT color transformation. The resulting
// transform will also be normalized to [0.0 ... 1.0].
///////////////////////////////////////////////////////////////////////////////////////////////
void CSctCenterSplitSegment::spher_xform(float **cvecP, long vdim) 
{ 
   double arg_acos;
   float r, g, b, *rho, *phi, *theta;
   register long i=0;
   
   rho   = cvecP[0]; r = *rho;
   phi   = cvecP[1]; g = *phi;
   theta = cvecP[2]; b = *theta;

   for(; i < vdim; i++, r=*++rho, g=*++phi, b=*++theta) 
   {

      *rho = (float) sqrt((double)(r*r + g*g + b*b));

      if (*rho == 0.0) *phi = *theta = 0.0;
      else 
	  {
         arg_acos = ((double) b)/(*rho);
         *phi = (arg_acos > 1.0) ? (float) acos(1.0) : (float) acos(arg_acos);

         arg_acos = ((double) r)/ ( (*rho)*sin((double)*phi) );
         *theta = (arg_acos > 1.0) ? (float) acos(1.0) : (float) acos(arg_acos);
      }
   }
}

