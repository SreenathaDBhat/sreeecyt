///////////////////////////////////////////////////////////////////////////////////////////////
// Some automated segmentation algorithms
// 8 bit images only
// 
// Written By Homayoun Bagherinia 08/04/2004
///////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "uts.h"
#include "vcmemrect.h"
#include "vcsegment.h"
#include "vcfuzzycmeansegment.h"
#include "vcsctcentersplitsegment.h"
#include "vcmeanshiftsegment.h"

///////////////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////////////////////

CSegment::CSegment(CUTSMemRect *pMR)
{
	pUTSMemRect = pMR;
}

CSegment::~CSegment(void)
{
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Segments a RGB color image based on fuzzy C-mean segmentation algorithm
//	Parameters:
//      R, G and B are handles to 8 bit images
//		variance is the value for gaussian kernel variance (0.0 - 20.0)
///////////////////////////////////////////////////////////////////////////////////////////////
long CSegment::FuzzyCmeanSegment(long R, long G, long B, double variance)
{
	long	height, width, pitch;
	BYTE	*pR, *pG, *pB;

	// Get RGB image info
	if (pUTSMemRect->GetExtendedInfo(R, &width, &height, &pitch, &pR) != EIGHT_BITS_PER_PIXEL )
		return -1;
	if (pUTSMemRect->GetExtendedInfo(G, &width, &height, &pitch, &pG) != EIGHT_BITS_PER_PIXEL )
		return -1;
	if (pUTSMemRect->GetExtendedInfo(B, &width, &height, &pitch, &pB) != EIGHT_BITS_PER_PIXEL )
		return -1;

	CFuzzyCmeanSegment FuzzyCmeanSeg;
	if ( FuzzyCmeanSeg.fuzzyc_segment(pR, pG, pB, width, height, (float)variance) == -1 )
		return -1;

	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Segments a RGB color image based on SCT/Center Split segmentation algorithm
//	Parameters:
//      R, G and B are handles to 8 bit images
//		A_split: number of colors to divide along angle A
//		B_split: number of colors to divide along angle B
///////////////////////////////////////////////////////////////////////////////////////////////
long CSegment::SctSplitSegment(long R, long G, long B, long A_split, long B_split)
{
	long	height, width, pitch;
	BYTE	*pR, *pG, *pB;

	// Get RGB image info
	if (pUTSMemRect->GetExtendedInfo(R, &width, &height, &pitch, &pR) != EIGHT_BITS_PER_PIXEL )
		return -1;
	if (pUTSMemRect->GetExtendedInfo(G, &width, &height, &pitch, &pG) != EIGHT_BITS_PER_PIXEL )
		return -1;
	if (pUTSMemRect->GetExtendedInfo(B, &width, &height, &pitch, &pB) != EIGHT_BITS_PER_PIXEL )
		return -1;

	CSctCenterSplitSegment SctCenterSplitSeg(pUTSMemRect);
	if (SctCenterSplitSeg.sct_split_segment(pR, pG, pB, width, height, (int)A_split, (int)B_split) == -1 )
		return -1;

	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Segments a RGB color image based on Mean Shift segmentation algorithm
//	Parameters:
//      R, G and B are handles to 8 bit images
///////////////////////////////////////////////////////////////////////////////////////////////
long CSegment::MeanShiftSegment(long R, long G, long B)
{
	long	height, width, pitch;
	BYTE	*pR, *pG, *pB;

	// Get RGB image info
	if (pUTSMemRect->GetExtendedInfo(R, &width, &height, &pitch, &pR) != EIGHT_BITS_PER_PIXEL )
		return -1;
	if (pUTSMemRect->GetExtendedInfo(G, &width, &height, &pitch, &pG) != EIGHT_BITS_PER_PIXEL )
		return -1;
	if (pUTSMemRect->GetExtendedInfo(B, &width, &height, &pitch, &pB) != EIGHT_BITS_PER_PIXEL )
		return -1;

	CMeanShiftSegment MeanShiftSeg;
	if ( MeanShiftSeg.meanshift_segment(pR, pG, pB, width, height) == -1 )
		return -1;

	return 0;
}