///////////////////////////////////////////////////////////////////////////////////////////////
// Spatial Filters Interface
// 8 bit images only
// 
// Written By HB 10/20/2004
///////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "uts.h"
#include "vcmemrect.h"
#include "vcspatialfilter.h"
#include "vcfilters.h"
#include "vcmorphology.h"
#include "vcutsfile.h"
#include "vcedgedetection.h"

///////////////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////////////////////

CSpatialFilter::CSpatialFilter(CUTSMemRect *pMR)
{
	pUTSMemRect = pMR;
}

CSpatialFilter::~CSpatialFilter(void)
{
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Links edge points of a binary image into lines
//	Parameters:
//      BinaryImage is a handle to a eight-bit (0-255) or one-bit (0-1) image
//		connection is the maximum link distance in pixel (0 - 20)
///////////////////////////////////////////////////////////////////////////////////////////////
long CSpatialFilter::EdgeLink(long BinaryImage, long connection)
{
	long	height, width, pitch, BPP;
	long	tmp8bits;
	BYTE	*pb, *pg;

	// Get binary image info
	BPP = pUTSMemRect->GetExtendedInfo(BinaryImage, &width, &height, &pitch, &pb);
	if ( BPP == ONE_BIT_PER_PIXEL ) 
	{
		if ( (tmp8bits=pUTSMemRect->CreateAsWithPix(BinaryImage, 8)) < 1 )
			return -1;
		if ( pUTSMemRect->Convert(BinaryImage, tmp8bits, 255) == UTS_ERROR ) {
			pUTSMemRect->FreeImage(tmp8bits);
			return -1;
		}
		if ( pUTSMemRect->GetExtendedInfo(tmp8bits, &width, &height, &pitch, &pg) != EIGHT_BITS_PER_PIXEL ) {
			pUTSMemRect->FreeImage(tmp8bits);
			return -1;
		}
	}
	else if ( BPP == EIGHT_BITS_PER_PIXEL ) {
		pg = pb;
	}
	else
		return -1;

	CFilters EdgeLinkFilter;
	if ( EdgeLinkFilter.edge_link_filter(pg, width, height, (int)connection) == -1 ) {
		if ( BPP == ONE_BIT_PER_PIXEL ) 
			pUTSMemRect->FreeImage(tmp8bits);
		return -1;
	}

	if ( BPP == ONE_BIT_PER_PIXEL ) {
		if ( pUTSMemRect->Convert(tmp8bits, BinaryImage, 1) == UTS_ERROR ) {
			pUTSMemRect->FreeImage(tmp8bits);
			return -1;
		}
		pUTSMemRect->FreeImage(tmp8bits);
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////
//  Performs a morphological operation
//	Parameters:
//		op_type: 
//			1: Binary dilation
//			2: Binary erosion
//			3: Binary closing
//			4: Binary opening
//			5: Gray dilation
//			6: Gray erosion
//			7: Gray closing
//			8: Gray opening
//		k_type:
//			0: user defined structuring element
//			1: Flat disk structuring element ("Se" needs to be square)
//			2: Flat rectangular structuring element
//		Image (input and output): a handle to a eight-bit (0-255) or one-bit (0-1) image
//      Se (input): is a handle to a eight-bit (0-255) or one-bit (0-1) structuring element
///////////////////////////////////////////////////////////////////////////////////////////////
long CSpatialFilter::Morph(int op_type, int k_type, long Image, long Se)
{
	long	imheight, imwidth, impitch, imBPP;
	long	imtmp8bits;
	long	seheight, sewidth, sepitch, seBPP;
	long	setmp8bits;
	BYTE	*pbim, *pim, *pbse, *pse;

	// Get image info
	imBPP = pUTSMemRect->GetExtendedInfo(Image, &imwidth, &imheight, &impitch, &pbim);
	if ( imBPP == ONE_BIT_PER_PIXEL ) 
	{
		if ( (imtmp8bits=pUTSMemRect->CreateAsWithPix(Image, 8)) < 1 )
			return -1;
		if ( pUTSMemRect->Convert(Image, imtmp8bits, 1) == UTS_ERROR ) {
			pUTSMemRect->FreeImage(imtmp8bits);
			return -1;
		}	

		if ( pUTSMemRect->GetExtendedInfo(imtmp8bits, &imwidth, &imheight, &impitch, &pim) != EIGHT_BITS_PER_PIXEL ) {
			pUTSMemRect->FreeImage(imtmp8bits);
			return -1;
		}
	}
	else if ( imBPP == EIGHT_BITS_PER_PIXEL ) {
		pim = pbim;
	}
	else
		return -1;

	// Get image info
	seBPP = pUTSMemRect->GetExtendedInfo(Se, &sewidth, &seheight, &sepitch, &pbse);
	if ( seBPP == ONE_BIT_PER_PIXEL ) 
	{
		if ( (setmp8bits=pUTSMemRect->CreateAsWithPix(Se, 8)) < 1 ) {
			pUTSMemRect->FreeImage(imtmp8bits);
			return -1;
		}
		if ( pUTSMemRect->Convert(Se, setmp8bits, 1) == UTS_ERROR ) {
			pUTSMemRect->FreeImage(imtmp8bits);
			pUTSMemRect->FreeImage(setmp8bits);
			return -1;
		}

		if ( pUTSMemRect->GetExtendedInfo(setmp8bits, &sewidth, &seheight, &sepitch, &pse) != EIGHT_BITS_PER_PIXEL ) {
			pUTSMemRect->FreeImage(imtmp8bits);
			pUTSMemRect->FreeImage(setmp8bits);
			return -1;
		}
	}
	else if ( seBPP == EIGHT_BITS_PER_PIXEL ) {
		pse = pbse;
	}
	else {
		pUTSMemRect->FreeImage(imtmp8bits);
		return -1;
	}

	CMorph MorphFilter;
	if ( MorphFilter.Morph(op_type, k_type, pim, imwidth, imheight, pse, sewidth, seheight) == -1 ) {
		if ( imBPP == ONE_BIT_PER_PIXEL ) 
			pUTSMemRect->FreeImage(imtmp8bits);
		if ( seBPP == ONE_BIT_PER_PIXEL ) 
			pUTSMemRect->FreeImage(setmp8bits);
		return -1;
	}

	if ( imBPP == ONE_BIT_PER_PIXEL ) {
		if ( pUTSMemRect->Convert(imtmp8bits, Image, 1) == UTS_ERROR ) {
			pUTSMemRect->FreeImage(imtmp8bits);
			if ( seBPP == ONE_BIT_PER_PIXEL ) 
				pUTSMemRect->FreeImage(setmp8bits);
			return -1;
		}
		pUTSMemRect->FreeImage(imtmp8bits);
	}

	if ( seBPP == ONE_BIT_PER_PIXEL ) {
		if ( pUTSMemRect->Convert(setmp8bits, Se, 1) == UTS_ERROR ) {
			pUTSMemRect->FreeImage(setmp8bits);
			return -1;
		}
		pUTSMemRect->FreeImage(setmp8bits);
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Performs Thinning based on Zhang & Suen/Stentiford/Holt combined algorithm
//	Parameters:
//      BinaryImage is a handle to a eight-bit (0-255) or one-bit (0-1) image
///////////////////////////////////////////////////////////////////////////////////////////////
long CSpatialFilter::FThin(long BinaryImage)
{
	long	height, width, pitch, BPP;
	long	tmp8bits;
	BYTE	*pb, *pg;

	// Get binary image info
	BPP = pUTSMemRect->GetExtendedInfo(BinaryImage, &width, &height, &pitch, &pb);
	if ( BPP == ONE_BIT_PER_PIXEL ) 
	{
		if ( (tmp8bits=pUTSMemRect->CreateAsWithPix(BinaryImage, 8)) < 1 )
			return -1;
		if ( pUTSMemRect->Convert(BinaryImage, tmp8bits, 1) == UTS_ERROR ) {
			pUTSMemRect->FreeImage(tmp8bits);
			return -1;
		}
		if ( pUTSMemRect->GetExtendedInfo(tmp8bits, &width, &height, &pitch, &pg) != EIGHT_BITS_PER_PIXEL ) {
			pUTSMemRect->FreeImage(tmp8bits);
			return -1;
		}
	}
	else if ( BPP == EIGHT_BITS_PER_PIXEL ) {
		pg = pb;
	}
	else
		return -1;

	CForceThinning FThinning;
	if ( FThinning.ForceThin(pg, width, height) == -1 ) {
		if ( BPP == ONE_BIT_PER_PIXEL ) 
			pUTSMemRect->FreeImage(tmp8bits);
		return -1;
	}

	if ( BPP == ONE_BIT_PER_PIXEL ) {
		if ( pUTSMemRect->Convert(tmp8bits, BinaryImage, 1) == UTS_ERROR ) {
			pUTSMemRect->FreeImage(tmp8bits);
			return -1;
		}
		pUTSMemRect->FreeImage(tmp8bits);
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Performs Edge Detection based on Marr/Hildreth, Canny, or Shen-Casten algorithm
//	Parameters:
//		methode :	"marr", "canny", "shen"
//      Image:		a handle to a eight-bit (0-255) image
//		sigma:		- standard deviation of gaussian for Marr/Hildreth and Canny algorithm (>0.0) 
//					  2.0 is a typical value.
//					- smoothing factor for Shen-Casten algorithm (0.0 - 1.0). 
//					  0.9 is a typical value.
//		low:		low threshold of the Hysteresis thresholding for Canny and Shen-Casten algorithms. 
//					50 is a typical value. Marr/Hildreth does not use this parameter(low=0).
//		high:		high threshold of the Hysteresis thresholding for Canny and Shen-Casten algorithms. 
//					150 is a typical value. Marr/Hildreth does not use this parameter(high=0).	
///////////////////////////////////////////////////////////////////////////////////////////////
long CSpatialFilter::EdgeDetection(char *methode, long Image, float sigma, long low, long high)
{
	long	height, width, pitch, BPP;
	BYTE	*pb;

	// Get binary image info
	BPP = pUTSMemRect->GetExtendedInfo(Image, &width, &height, &pitch, &pb);
	if ( BPP == ONE_BIT_PER_PIXEL )
		return -1;

	if (strcmp(methode, "marr") == 0) {
		CMarrEdgeDetector Marr;
		if ( Marr.EdgeDetection(pb, width, height, sigma) == -1 )
			return -1;
	}
	else if (strcmp(methode, "canny") == 0) {
		CCannyEdgeDetector Canny;
		if ( Canny.EdgeDetection(pb, width, height, sigma, (int)low, (int)high) == -1 )
			return -1;
	}
	else if (strcmp(methode, "shen") == 0) {
		CShenCastenEdgeDetector Shen;
		if ( Shen.EdgeDetection(pb, width, height, sigma, (int)low, (int)high, 7, 1) == -1 )
			return -1;
	}
	else
		return -1;

	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Performs hough filter for lines
//	Parameters:
//      - BinaryImage is a handle to a eight-bit (0-255) or one-bit (0-1) image
//		- degree_string indicates which angles are of interest and should be included 
//		  when hough transform is performed. An example would be like "-18 40-60 90 130-", 
//		  which means keep those lines whose slope are in the range from 0 to 18, 40 to 60, 
//		  90, 130 to 179.
//		- minlength indicates the minimum number of pixels on a none conected line which will 
//		  be kept in the result image.
//		- connection is the maxmum distance to be linked on a line which will be kept after 
//		  minlength thresholding. (0 - 20)
///////////////////////////////////////////////////////////////////////////////////////////////
long CSpatialFilter::HoughFilter(long BinaryImage, char *degree_string, long minlength, long connection)
{
	long	height, width, pitch, BPP;
	long	tmp8bits;
	BYTE	*pb, *pg;

	// Get binary image info
	BPP = pUTSMemRect->GetExtendedInfo(BinaryImage, &width, &height, &pitch, &pb);
	if ( BPP == ONE_BIT_PER_PIXEL ) 
	{
		if ( (tmp8bits=pUTSMemRect->CreateAsWithPix(BinaryImage, 8)) < 1 )
			return -1;
		if ( pUTSMemRect->Convert(BinaryImage, tmp8bits, 255) == UTS_ERROR ) {
			pUTSMemRect->FreeImage(tmp8bits);
			return -1;
		}
		if ( pUTSMemRect->GetExtendedInfo(tmp8bits, &width, &height, &pitch, &pg) != EIGHT_BITS_PER_PIXEL ) {
			pUTSMemRect->FreeImage(tmp8bits);
			return -1;
		}
	}
	else if ( BPP == EIGHT_BITS_PER_PIXEL ) {
		pg = pb;
	}
	else
		return -1;

	CFilters HF;
	if ( HF.hough_filter(pg, width, height, degree_string, minlength, connection) == -1 ) {
		if ( BPP == ONE_BIT_PER_PIXEL ) 
			pUTSMemRect->FreeImage(tmp8bits);
		return -1;
	}

	if ( BPP == ONE_BIT_PER_PIXEL ) {
		if ( pUTSMemRect->Convert(tmp8bits, BinaryImage, 1) == UTS_ERROR ) {
			pUTSMemRect->FreeImage(tmp8bits);
			return -1;
		}
		pUTSMemRect->FreeImage(tmp8bits);
	}

	return 0;
}