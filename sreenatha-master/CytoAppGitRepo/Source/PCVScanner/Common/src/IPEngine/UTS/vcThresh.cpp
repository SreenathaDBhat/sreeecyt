///////////////////////////////////////////////////////////////////////////////////////////////
// Some more rather splendid thresholding algorithms
// To complement those of Woolz
// 
// Written By Karl Ratcliff 04102001
///////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "uts.h"
#include "vcmemrect.h"
#include "vcthresh.h"
#include <math.h>
#include <float.h>

///////////////////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////////////////////


CThreshImage::CThreshImage(CUTSMemRect *pMR)
{
	pUTSMemRect = pMR;
}

CThreshImage::~CThreshImage()
{
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Divides the image grey levels into two classes where the division point
// represents the point at which fuzzy set membership is minimised using
// Shannon's entropy function.
// 
// See Huang L.-K. and Wang M.-J.J. (1995) "Image Thresholding by Minimizing  
// the Measures of Fuzziness" Pattern Recognition, 28(1): 41-51
// http://www.ktl.elf.stuba.sk/study/vacso/Zadania-Cvicenia/Cvicenie_3/TimA2/Huang_E016529624.pdf
///////////////////////////////////////////////////////////////////////////////////////////////

int CThreshImage::HuangThreshold( long hSrc)
{
	int threshold = UTS_ERROR;

	// Get image info
	long width, height, pitch;
	void *pAddr;
	long bpp = pUTSMemRect->GetExtendedInfo(hSrc, &width, &height, &pitch, &pAddr);
	ASSERT( bpp == EIGHT_BITS_PER_PIXEL );
	if (bpp == EIGHT_BITS_PER_PIXEL)
	{
		// Get the histogram
		int  nBins = 256;
		int *data = new int[nBins];
		memset(data, 0, nBins * sizeof(int));

		BYTE *pIm = (BYTE*)pAddr;
		for (int y = 0; y < height; y++) 
		{
			for (int x = 0; x < width; x++) 
				data[ *pIm++ ]++;

			pIm += (pitch - width);
		}

		// Compute threshold that minimises the fuzzy set membership measure
		int ih, it;
		int first_bin;
		int last_bin;
		double sum_pix;
		double num_pix;
		double term;
		double ent;  // entropy 
		double min_ent; // min entropy 
		double mu_x;

		/* Determine the first non-zero bin */
		first_bin=0;
		for (ih = 0; ih < nBins; ih++ ) {
			if ( data[ih] != 0 ) {
				first_bin = ih;
				break;
			}
		}

		/* Determine the last non-zero bin */
		last_bin=nBins - 1;
		for (ih = nBins - 1; ih >= first_bin; ih-- ) {
			if ( data[ih] != 0 ) {
				last_bin = ih;
				break;
			}
		}

		// Check the image is not blank (single grey level) 
		// this avoids divide by zero errors with term below
		threshold = first_bin;
		if (first_bin != last_bin)
		{
			term = 1.0 / ( double ) ( last_bin - first_bin );

			double * mu_0 = new double[nBins];
			for ( ih=0; ih<nBins; ih++) mu_0[ih] = 0.0;

			sum_pix = num_pix = 0;
			for ( ih = first_bin; ih < nBins; ih++ ){
				sum_pix += (double)ih * data[ih];
				num_pix += data[ih];
				/* NUM_PIX cannot be zero ! */
				mu_0[ih] = sum_pix / num_pix;
			}

			double * mu_1 = new double[nBins];
			for (ih=0; ih<nBins; ih++) mu_1[ih] = 0.0;

			sum_pix = num_pix = 0;
			for ( ih = last_bin; ih > 0; ih-- ){
				sum_pix += (double)ih * data[ih];
				num_pix += data[ih];
				/* NUM_PIX cannot be zero ! */
				mu_1[ih - 1] = sum_pix / ( double ) num_pix;
			}

			/* Determine the threshold that minimizes the fuzzy entropy */
			threshold = -1;
			min_ent = DBL_MAX; //Double.MAX_VALUE;
			for ( it = 0; it < nBins; it++ ){
				ent = 0.0;
				for ( ih = 0; ih <= it; ih++ ) {
					/* Equation (4) in Ref. 1 */
					mu_x = 1.0 / ( 1.0 + term * abs( ih - mu_0[it] ) );
					if ( !((mu_x  < 1e-06 ) || ( mu_x > 0.999999))) {
						/* Equation (6) & (8) in Ref. 1 */
						ent += data[ih] * ( -mu_x * log ( mu_x ) - ( 1.0 - mu_x ) * log ( 1.0 - mu_x ) );
					}
				}

				for ( ih = it + 1; ih < nBins; ih++ ) {
					/* Equation (4) in Ref. 1 */
					mu_x = 1.0 / ( 1.0 + term * abs ( ih - mu_1[it] ) );
					if ( !((mu_x  < 1e-06 ) || ( mu_x > 0.999999))) {
						/* Equation (6) & (8) in Ref. 1 */
						ent += data[ih] * ( -mu_x * log ( mu_x ) - ( 1.0 - mu_x ) * log ( 1.0 - mu_x ) );
					}
				}
				/* No need to divide by NUM_ROWS * NUM_COLS * LOG(2) ! */
				if ( ent < min_ent ) {
					min_ent = ent;
					threshold = it;
				}
			}

			delete[] mu_0;
			delete[] mu_1;
		}

		delete[] data;
	}

	return threshold;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Divides the image grey levels into two classes where the division point
// represents the point at which max entropy is preserved.
///////////////////////////////////////////////////////////////////////////////////////////////

int CThreshImage::EntropyThreshold(long hSrc)
{
	int thresh = UTS_ERROR;

	// Get image info
	long width, height, pitch;
	void *pAddr;
	long bpp = pUTSMemRect->GetExtendedInfo(hSrc, &width, &height, &pitch, &pAddr);
    if (bpp >= EIGHT_BITS_PER_PIXEL || bpp <= SIXTEEN_BITS_PER_PIXEL)
	{
		int nBins = 1 << bpp;

		int *pHisto = new int[ nBins ];
		if (pHisto)
		{
			memset(pHisto, 0, nBins * sizeof(int));

			// Get the histogram
			if (bpp == EIGHT_BITS_PER_PIXEL)
			{
				BYTE *pIm = (BYTE*)pAddr;
				for (int y = 0; y < height; y++) 
				{
					for (int x = 0; x < width; x++) 
						pHisto[ *pIm++ ]++;

					pIm += (pitch - width);
				}
			}
			else
			{
				BYTE *pIm = (BYTE*)pAddr;
				for (int y = 0; y < height; y++) 
				{
					WORD *pLine = (WORD*)pIm;
					for (int x = 0; x < width; x++) 
						pHisto[ *pLine++ ]++;

					pIm += pitch; // Pitch is always in bytes
				}
			}
		
			double *pProb = new double[ nBins ];
			if (pProb)
			{
				// All error conditions have now been avoided, so set threshold to a
				// non-error default value, in case it is not set in the inner loop below,
				// e.g. if all probabilities are zero (which is not an error).
				thresh = 0;

				// Compute probabilities 
				int numberOfPixels = width * height;
				for (int i = 0; i < nBins; i++)
					pProb[i] = double(pHisto[i]) / double(numberOfPixels);
		
				// Find threshold 
				double Hn = 0.0;
				for (int i = 0; i < nBins; i++)
				{
					double prob = pProb[i];
					if (prob != 0.0)
						Hn -= prob * log(prob);
				}

				// Find threshold at which max entropy obtained
				double Ps = 0.0;
				double Hs = 0.0;
				double psiMax = 0.0;
				double psi = 0.0;
				for (int i = 1; i < nBins; i++) 
				{
					double prob = pProb[ i - 1 ];
					Ps += prob;
					if (prob > 0.0)
						Hs -= prob * log(prob);
			
					if (Ps > 0.0 && Ps < 1.0)
						psi = log(Ps - Ps * Ps) + Hs / Ps + (Hn - Hs) / (1.0 - Ps);

					if (psi > psiMax) 
					{
						psiMax = psi;
						thresh = i;
					}
				}

				delete pProb;
			}

			delete pHisto;
		}
	}

	return thresh;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Threshold selection based on choosing the threshold in which the result binary
// image will have the same image moments as the original grey level image
///////////////////////////////////////////////////////////////////////////////////////////////

int CThreshImage::MomentThreshold(long SrcImage)			// Calc threshold based on moment preservation
{
	int		i, y, n, x;
	int		Thresh = -1;
	long	BPP, Height, Width, Pitch;
	BYTE	*Addr, *ImPtr;
	DWORD	Hist[NBINS_EIGHT];
	double	prob[NBINS_EIGHT];
	double  m1, m2, m3, cd, c0, c1, z0, z1, p0, p1, pd, pDistr;

	// Get image info
	BPP = pUTSMemRect->GetExtendedInfo(SrcImage, &Width, &Height, &Pitch, &Addr);
	
	// Must be an 8 bpp image
    if (BPP == EIGHT_BITS_PER_PIXEL)
	{
		// compile histogram 
		for (i = 0; i < NBINS_EIGHT; i++)
			Hist[i] = 0;
		
		n = Width * Height;
		ImPtr = Addr;
		for (y = 0; y < Height; y++) 
		{
			for (x = 0; x < Width; x++) 
			{
				Hist[*ImPtr++]++;
			}
			ImPtr += (Pitch - Width);
		}
		// compute probabilities 
		for (i = 0; i < NBINS_EIGHT; i++)
			prob[i] = (double) Hist[i] / (double) n;
		
		// calculate first 3 moments 
		m1 = m2 = m3 = 0.0;
		for (i = 0; i < NBINS_EIGHT; i++) 
		{
			m1 += i * prob[i];
			m2 += i * i * prob[i];
			m3 += i * i * i * prob[i];
		}
		
		cd = m2 - m1 * m1;
		c0 = (-m2 * m2 + m1 * m3) / cd;
		c1 = (-m3 + m2 * m1) / cd;
		z0 = 0.5 * (-c1 - sqrt (c1 * c1 - 4.0 * c0));
		z1 = 0.5 * (-c1 + sqrt (c1 * c1 - 4.0 * c0));
		
		pd = z1 - z0;
		p0 = (z1 - m1) / pd;
		p1 = 1.0 - p0;
		 
		pDistr = 0.0;

		// find threshold 
		for (Thresh = 0; Thresh < NBINS_EIGHT; Thresh++) 
		{
			pDistr += prob[Thresh];
			if (pDistr > p0)
				break;
		}
	}

	return Thresh;
}
///////////////////////////////////////////////////////////////////////////////////////////////
// Threshold selection based on distance to line background peak
///////////////////////////////////////////////////////////////////////////////////////////////

int CThreshImage::DistLineThreshold(long SrcImage)			// Calc threshold based on moment preservation
{
	int		i, y, n, x, PeakPos, DistPos;
	int		Thresh = -1;
	long	BPP, Height, Width, Pitch, DistMax, Dist;
	BYTE	*Addr, *ImPtr;
	DWORD	Hist[NBINS_EIGHT], HistPrev, HistCurrent, PeakVal;

	// Get image info
	BPP = pUTSMemRect->GetExtendedInfo(SrcImage, &Width, &Height, &Pitch, &Addr);
	
	// Must be an 8 bpp image
    if (BPP == EIGHT_BITS_PER_PIXEL)
	{
		// compile histogram 
		for (i = 0; i < NBINS_EIGHT; i++)
			Hist[i] = 0;
		
		n = Width * Height;
		ImPtr = Addr;
		for (y = 0; y < Height; y++) 
		{
			for (x = 0; x < Width; x++) 
			{
				Hist[*ImPtr++]++;
			}
			ImPtr += (Pitch - Width);
		}
		// filter empty classes
		for (i = 1; i < NBINS_EIGHT-1; i++)
			if ((Hist[i]==0) && (Hist[i-1]!=0) && (Hist[i+1]!=0))
				Hist[i]=(Hist[i-1]+Hist[i+1])/2;

		// low pass filter
		HistPrev=Hist[0];
		for (i = 1; i < NBINS_EIGHT-1; i++)
		{
			HistCurrent=Hist[i];
			Hist[i]=(2*HistCurrent+HistPrev+Hist[i+1])/4;
			HistPrev=HistCurrent;
		}

		// Determine background Peak
		PeakVal=0;
		for (i = 0; i < NBINS_EIGHT; i++)
		{
			if (Hist[i] > PeakVal)
			{
				PeakVal=Hist[i];
				PeakPos=i;
			}
		}

		// distance Histogram point to Line through (0,0) and (PeakPos, PeakVal)
		DistMax = 0;
		DistPos = 0;
		for (i = 0; i < PeakPos; i++)
		{
			Dist =  (long)PeakVal*(long)i - (long)PeakPos*(long)Hist[i];
			if (Dist > DistMax)
			{
				DistMax=Dist;
				DistPos= i;
			}
		}
		Thresh=DistPos;
	}
	return Thresh;
}


///////////////////////////////////////////////////////////////////////////////////////////////
// Threshold an 8 bit grey level image preserving grey level info
///////////////////////////////////////////////////////////////////////////////////////////////
	
void CThreshImage::GreyThreshold(long SrcImage, BYTE Low, BYTE High)
{
	long BPP, Width, Height, Pitch;
	BYTE *Addr, G;
	DWORD i, n;

	BPP = pUTSMemRect->GetExtendedInfo(SrcImage, &Width, &Height, &Pitch, &Addr);
	if (BPP == EIGHT_BITS_PER_PIXEL)
	{
		n = Pitch * Height;
		for (i = 0; i < n; i++)
		{
			G = *Addr;
			if (G >= Low && G <= High)
				*Addr++;
			else
				*Addr++ = 0;
		}
	}
}
