///////////////////////////////////////////////////////////////////////////////////////
//
// An Interface to the Borland C UTS DLLs
//
// Written By Karl Ratcliff 01/03/2001
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//

#include "stdafx.h"
#include <stdio.h>
#include "yblob.h"
#include "vcmemrect.h"
#include "ymemrect.h"
#include "vcblob.h"
#include "vcutsfile.h"
#include "uts.h"

////////////////////////////////////////////////////////////////////////////////////////
// Constructor

CUTSBlob::CUTSBlob(CUTSMemRect *pMR)
{
	pZMemRect			= pMR->ZMemRect();
	pBlob				= new CZBlob(pZMemRect);
}

// Destructor

CUTSBlob::~CUTSBlob()
{
	delete pBlob;
}

////////////////////////////////////////////////////////////////////////////////////////
// Member Functions
////////////////////////////////////////////////////////////////////////////////////////

BOOL CUTSBlob::Initialise(void)
{
	return TRUE;

}

////////////////////////////////////////////////////////////////////////////////////////
// Spot Finding
// Parameters:
// Hndl         - A valid UTS handle
// Operation    - either "<=", "<", ">" or ">="
// Threshold    - Thresholding Level
// MinArea      - Minimum Area Of Spot To Find
// Returns:
// Number of spots if OK, UTS_ERROR otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSBlob::FindSpots(long Handle, char *Operation, long Threshold, long MinArea)
{
    return pBlob->spFindSpots(Handle, Operation, Threshold, MinArea);
}

////////////////////////////////////////////////////////////////////////////////////////
// Get Information About A Spot
// Parameters:
//  SpotNumber       - from 1
//  *pSpotData       - Data About A Spot (See VCBLOB_SPOT typedef)
// Returns: 
//  Number of BYTEs actually sent, UTS_ERROR if problem
////////////////////////////////////////////////////////////////////////////////////////

long CUTSBlob::GetSpot(long SpotNumber, VCBLOB_SPOT *pSpotData)
{
    return pBlob->spGetSpot(SpotNumber, (BYTE *) pSpotData);
}

////////////////////////////////////////////////////////////////////////////////////////
// Set The 'Tag' Parameter For A Spot
// Parameters:
//  SpotNumber       - from 1
//  TagValue         - Value To Set In Tag (See VCBLOB_SPOT typedef)
// Returns: 
//  Nothing
////////////////////////////////////////////////////////////////////////////////////////

void CUTSBlob::SetTag(long SpotNumber, long TagValue)
{
	pBlob->spSetTag(SpotNumber, TagValue);
}

////////////////////////////////////////////////////////////////////////////////////////
// Delete Spots With A Matching 'Tag' Parameter
// Parameters:
//  TagValue         - Those Spots With 'Tag' Matching This Value
// Returns: 
//  New number of spots, UTS_ERROR otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSBlob::DeleteTag(long TagValue)
{
	return pBlob->spDeleteTag(TagValue);
}

////////////////////////////////////////////////////////////////////////////////////////
// Number Of Spots Found
// Parameters:
//  None
// Returns: 
//  Number of spots, UTS_ERROR otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSBlob::SpotsNumber(void)
{
    return pBlob->spSpotsNumber();
}

////////////////////////////////////////////////////////////////////////////////////////
// 	Calculates
//
//    hacc - handle of the accumulating picture, 8 or 32 bit per pixel
//	returns:
//   	 number of lines if OK,
//     0 otherwise, UTS_ERROR lib not loaded
////////////////////////////////////////////////////////////////////////////////////////

long CUTSBlob::AccumLinesSeg(long Mask, long hAcc, long HalfStepMin, long HalfStepMax, long CurvThresh, double DistMin, double DistMax)
{
    return pBlob->spAccumLinesSeg(Mask, hAcc, HalfStepMin, HalfStepMax, CurvThresh, DistMin, DistMax);
}

////////////////////////////////////////////////////////////////////////////////////////
// 	Calculates
//
//    For  *codes and/or *curvs  NULL is OK.
//    For  maskarc and/or maskcont  0 is OK.
//    Updates:
//    	resdata[0] = start X coordinate of longest smooth arc;
//    	resdata[1] = start Y coordinate of longest smooth arc;
//    	resdata[2] = number of codes in longest smooth arc;
//    	resdata[3] = maximal curvature;
//        resdata[4] - number of elements with (curvature >= curvthresh);
//    	resdata[5] = number of codes in contour;
//    	resdata[6] = lenght of longest smooth arc;
//    	resdata[7] = lenght of contour;
//    	if (codes != NULL) then it is filled with incremental
//        	codes of the contour, begining from the start point;
// 	Returns:
//   	 (100 * (lenght of longest smooth arc)/(lenght of contour)) if OK,
//     0 otherwise, UTS_ERROR NO LIB LOADED
//
////////////////////////////////////////////////////////////////////////////////////////
   
long CUTSBlob::LongSmoothArc(long SrcMask, long ContMask, long ArcMask, long HalfStepMin, long HalfStepMax, long CurvThresh,
                             long *Results, long *Codes, long *Curves, long CodeLim)
{
    return pBlob->spLongSmoothArc(SrcMask, ContMask, ArcMask, HalfStepMin, HalfStepMax, CurvThresh, Results, Codes, Curves, CodeLim);
}

////////////////////////////////////////////////////////////////////////////////////////
//	Description:
//    Calculates positions of mins and maxs elements in given vector.
//
//    <v> 	 - pointer to the vector array
// 	  <nv> 	 - number of elements in <v>
//    <type> -
//      <type> & 0xff:
//        0 - char
//        1 - byte
//        2 - short
//        3 - word
//        4 - long
//        5 - dword
//
//        8 - double
//        9 - float
//      <type> & 0x100:
//        0 - isolated ends
//        1 - closed array (first and last elements are neighbors)
//
//    <presult> - pointer to send result
//
//    Updates <presult> with
//        list of minima/maxima indices;
//        if the list begins from a maximum then presult[0]=-1
//        if the list ends with a maximum then presult[last]=-1
//
//	Returns: number of elements in <presult> array, 0 otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSBlob::MinsAndMaxs(BYTE *data, long pnpnt, long noise, long thresh, long cycle, long *res, long reslen)
{
    return pBlob->spMinsAndMaxs(data, pnpnt, noise, thresh, cycle, res, reslen);
}

////////////////////////////////////////////////////////////////////////////////////////
//    Updates:
//    	resdata[0] = square of the min diameter;
//    	resdata[1] = square of the max diameter;
//    	resdata[2] = X1min
//    	resdata[3] = Y1min
//    	resdata[4] = X2min
//    	resdata[5] = Y2min
//    	resdata[6] = X1max
//    	resdata[7] = Y1max
//    	resdata[8] = X2max
//    	resdata[9] = Y2max
// 	Returns:
//  	 100 * (square of the min diameter)/(square of the max diameter), 0 otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSBlob::ContShapeInfo(void *pCodes, long NCodes, long Xstart, long Ystart, long *Results)
{
    return pBlob->spContShapeInfo(pCodes, NCodes, Xstart, Ystart, Results);
}

////////////////////////////////////////////////////////////////////////////////////////
// Watershed algorithm
//  	Starts from the Spot list after spFindMarkers()
//     and from 'hndl1' mask of markers (source and result).
//	'borders' - calculated mask (subset) of watershed borders;
//   Any of 'hndl1' and 'borders' (or both) may be set to 0;
//   in this case the mask of markers is to be calculated with
//   spSpotListMask() function.
//   Form Factor plays if and only if the threshold is less
//   than 'formthresh'
//   Updates the Spot structure; sets
//      Tag1:
//         0 - limited by surrounding
//      	1 - limited by threshold
//      	2 - limited by area
//      	3 - limited by 'formfactor'
//      Tag2 - measure of 'formfactor';
// 	returns:
//   	 number of spots if OK,
//      -1 otherwise
//
//mrRectMaskData:
//        	data[0] = NUMBER of one bits in mask
//        	data[1] = SUM of bytes in 'mem' corresponding to one bits in mask
//        	data[2] = MIN of bytes in 'mem' corresponding to one bits in mask
//        	data[3] = MAX of bytes in 'mem' corresponding to one bits in mask
//        	data[4] = X coordinate one of the MIN pixel(s)
//        	data[5] = Y coordinate one of the MIN pixel(s)
//        	data[6] = X coordinate one of the MAX pixel(s)
//        	data[7] = Y coordinate one of the MAX pixel(s)
//        	data[8] = standard deviation:
//            			(long)(sqrt(Sum((P-Aver(P))^2)/NumOf)+0.5)
//        	data[9] = number of pixels with MIN value
//        	data[10] = number of pixels with MAX value
//
////////////////////////////////////////////////////////////////////////////////////////

long CUTSBlob::WaterShed(long SrcImage, long MaskImage,  long Borders,    long thrmin, 
                         long areamax,  long formfactor, long formthresh, long formtype)
{
	return pBlob->spWaterShed(SrcImage, MaskImage,  Borders,     thrmin, 
                              areamax,  formfactor, formthresh,  formtype);
}

////////////////////////////////////////////////////////////////////////////////////////
//
// 	Calculates Spot list using recursive algorithm
// 	returns:
//   	 number of spots if OK,
//     -1 otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSBlob::FindMarkers (long hndl, long threshold, long minarea, long maxarea)
{
    return pBlob->spFindMarkers(hndl, threshold, minarea, maxarea);
}

////////////////////////////////////////////////////////////////////////////////////////
// Reconstructs a mask image from a list of spots found with findspots or findmarkers
// Parameters:
//      SrcImage is an 8 bit image containg the original spots
//      MaskImage is a 1 bit mask image to put the results in
// Returns:
//      Number of spots in the mask image
////////////////////////////////////////////////////////////////////////////////////////

long CUTSBlob::SpotMask(long SrcImage, long MaskImage)
{
	return pBlob->spSpotListMask(SrcImage, MaskImage);
}

////////////////////////////////////////////////////////////////////////////////////////
// 	Calculates number of spots using subset of quantiles.
//    If <resmask> is given (i.e. != 0):
//    	Updates <resmask>.
//    If <begmask> is given (i.e. != 0):
//    	Searches spots only inside <begmask> ones;
//
// 	Returns:
//   	 number of spots if OK,
//      -1 otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSBlob::CountRange(long hndl, long resmask, long begmask, long minarea, long maxarea, double quantbeg, double quantfin, long maxrep, long nopen)
{
	return pBlob->spCountRange(hndl, resmask, begmask, minarea, maxarea, quantbeg, quantfin, maxrep, nopen);
}

	
long CUTSBlob::FindArcCenter(long mask, double distmin, long *xc, long *yc, double *radaver, double *radmin, double *radmax)
{
	return pBlob->spArcCenter(mask, distmin, xc, yc, radaver, radmin, radmax);
}


////////////////////////////////////////////////////////////////////////////////////////
//    Fills first 13(?) pixels in each row of h32 with information
//    about linked components in source mask:
//    	{Ni -  number of points in the component
//         Xi, Yi - coordinates of some point belonging to
//                  the i-th linked component,
//   	   Xmini, Ymini, 
//         Xmaxi, Ymaxi - min/max X,Y coordinates
//        			of the i-th linked component.
//         AxRati - main Axis ratio (*1000)
//         Cmpi -	measure of compactness (*1000)
//         CalMini, CalMaxi - Caliper Min/Max measures
//         Rmaxi -	Max Radius of the 'best fitting' circle
//         Rndi -	measure of roundness
//        },
//    maxcomp - max number of components to fit in <data>
//?    The result array is sorted by Ni in decreased order.
// 	Returns:
//   	 number of linked components bigger than <minarea>
//             (may be more than <maxcomp>),     or -1
//
////////////////////////////////////////////////////////////////////////////////////////

long CUTSBlob::MaskShapeInfo(long SrcImage, long Handle32, long MinArea)
{
	return pBlob->spMaskShapesInfo(SrcImage, Handle32, MinArea);
}

////////////////////////////////////////////////////////////////////////////////////////
// Different version adapted and ported from original UTS code
// Takes an array of MaxComps by 13 longs in and comes out with the same measurements.
// Gets around the bugs with the above in Release mode.
////////////////////////////////////////////////////////////////////////////////////////

long CUTSBlob::NewMaskShapeInfo(long MaskIn, long *Data, long minarea, long MaxComps)
{
	long lastx, lasty;
	long FedX, FedY;
	long LimitMask;
    long ones, xc, yc, 
         xc1, yc1, imin, imax,
         compnum = UTS_ERROR, j = 0;
	long *stat1data;
    long *calipers;
	long hseed1;
    double distmin = 5.0, radaver, radmin, radmax;


	stat1data = new long[MRSTAT1DATALENGTH];
	calipers  = new long[MRMASKCALIPERDATALENGTH];

    lastx = 0;
	lasty = 0;

    LimitMask = pZMemRect->mrCreateAs(MaskIn);
	pZMemRect->mrMove(MaskIn, LimitMask);
	FedX = pZMemRect->mrCreateAs(LimitMask);
	FedY = pZMemRect->mrCreateAs(LimitMask);
	hseed1 = pZMemRect->mrCreateAs(LimitMask);
	
	compnum = 0;
	if (LimitMask > 0)
	{
		// fill array of coordinates of components:
		while ((pZMemRect->mrFindNext(LimitMask, &lastx, &lasty, 1) > 0) && (compnum < MaxComps))
		{
			ones = pZMemRect->mrReconFastD(LimitMask, FedX, lastx, lasty, NULL);

			if (ones >= minarea)
			{
				*Data++ = ones;				// Element 0 = Area
				
				pZMemRect->mrStat1(FedX, stat1data);
				xc = stat1data[1] / ones;
				yc = stat1data[2] / ones;
				if (pZMemRect->mrGetPixel(FedX, xc, yc) == 0) 
					pZMemRect->mrDistToMask(FedX, &xc, &yc);
				*Data++ = xc;				// Element 1 = X,Y point on object
				*Data++ = yc;				// Element 2 
				*Data++ = stat1data[6];		// Element 3 = XMin
				*Data++ = stat1data[8];		// Element 4 = YMin
				*Data++ = stat1data[7];		// Element 5 = XMax
				*Data++ = stat1data[9];		// Element 6 = YMax
				*Data++ = stat1data[10];	// Element 7 = Axis Ratio
				*Data++ = stat1data[11];	// Element 8 = Compactness

				pZMemRect->mrMaskCalipers(FedX, calipers, &imin, &imax, &xc1, &yc1);

				*Data++ = calipers[imin];	// Element 9 = Caliper Min
				*Data++ = calipers[imax];	// Element 10= Caliper Max
		
				pZMemRect->mrMove(FedX, hseed1);
				pZMemRect->mrMove(FedX, FedY);
				pZMemRect->mrOperMask(FedY, "ERODE4");
				pZMemRect->mrPixelwise(FedY, "Less", hseed1);
				FindArcCenter(hseed1, distmin, &xc, &yc, &radaver, &radmin, &radmax);

				*Data++ = (long)(radmax + 0.5);// Element 11 = Best fitting circle
				if (radmax != 0.0)
					*Data++ = (long)(1000 * radmin / radmax);// Element 12 = Ratio of radii
				else
					*Data++ = 0;

				compnum++;
			}
			pZMemRect->mrPixelwise(FedX, "Less", LimitMask);
		}
	}

	if (calipers)
		delete calipers;
	if (stat1data)
		delete stat1data;

	if (hseed1 > 0)
		pZMemRect->mrFree(hseed1);
    if (FedX > 0) 
		pZMemRect->mrFree(FedX);
    if (FedY > 0) 
		pZMemRect->mrFree(FedY);
    if (LimitMask > 0) 
		pZMemRect->mrFree(LimitMask);

	return compnum;
}
