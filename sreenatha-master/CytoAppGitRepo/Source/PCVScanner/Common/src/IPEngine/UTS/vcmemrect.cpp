////////////////////////////////////////////////////////////////////////////////////////
//
// An Interface to the Borland C UTS DLLs
//
// Written By Karl Ratcliff 01/03/2001
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
// 28Feb2002 JMB Added CUTSMemRect::HistogramMask and CUTSMemRect::ReDimension
// 27May2002 JMB Modified CUTSMemRect::InvertColumns to handle 1bpp images properly.
// 12Jun2002 JMB Fixed CUTSMemRect::Move, which was not working properly for 1bpp images.

#include "stdafx.h"

#include "vcgeneric.h"
#include "ymemrect.h"
#include "vcmemrect.h"
#include "uts.h"
#include <math.h>

//  Inversed Bytes Table: bytes <k> and InversedBytes[k]
//  	have an inverse order of bits inside the byte

static BYTE InversedBytes[256]=
{
  0x00,0x80,0x40,0xC0, 0x20,0xA0,0x60,0xE0, 0x10,0x90,0x50,0xD0, 0x30,0xB0,0x70,0xF0,
  0x08,0x88,0x48,0xC8, 0x28,0xA8,0x68,0xE8, 0x18,0x98,0x58,0xD8, 0x38,0xB8,0x78,0xF8,
  0x04,0x84,0x44,0xC4, 0x24,0xA4,0x64,0xE4, 0x14,0x94,0x54,0xD4, 0x34,0xB4,0x74,0xF4,
  0x0C,0x8C,0x4C,0xCC, 0x2C,0xAC,0x6C,0xEC, 0x1C,0x9C,0x5C,0xDC, 0x3C,0xBC,0x7C,0xFC,
  0x02,0x82,0x42,0xC2, 0x22,0xA2,0x62,0xE2, 0x12,0x92,0x52,0xD2, 0x32,0xB2,0x72,0xF2,
  0x0A,0x8A,0x4A,0xCA, 0x2A,0xAA,0x6A,0xEA, 0x1A,0x9A,0x5A,0xDA, 0x3A,0xBA,0x7A,0xFA,
  0x06,0x86,0x46,0xC6, 0x26,0xA6,0x66,0xE6, 0x16,0x96,0x56,0xD6, 0x36,0xB6,0x76,0xF6,
  0x0E,0x8E,0x4E,0xCE, 0x2E,0xAE,0x6E,0xEE, 0x1E,0x9E,0x5E,0xDE, 0x3E,0xBE,0x7E,0xFE,

  0x01,0x81,0x41,0xC1, 0x21,0xA1,0x61,0xE1, 0x11,0x91,0x51,0xD1, 0x31,0xB1,0x71,0xF1,
  0x09,0x89,0x49,0xC9, 0x29,0xA9,0x69,0xE9, 0x19,0x99,0x59,0xD9, 0x39,0xB9,0x79,0xF9,
  0x05,0x85,0x45,0xC5, 0x25,0xA5,0x65,0xE5, 0x15,0x95,0x55,0xD5, 0x35,0xB5,0x75,0xF5,
  0x0D,0x8D,0x4D,0xCD, 0x2D,0xAD,0x6D,0xED, 0x1D,0x9D,0x5D,0xDD, 0x3D,0xBD,0x7D,0xFD,
  0x03,0x83,0x43,0xC3, 0x23,0xA3,0x63,0xE3, 0x13,0x93,0x53,0xD3, 0x33,0xB3,0x73,0xF3,
  0x0B,0x8B,0x4B,0xCB, 0x2B,0xAB,0x6B,0xEB, 0x1B,0x9B,0x5B,0xDB, 0x3B,0xBB,0x7B,0xFB,
  0x07,0x87,0x47,0xC7, 0x27,0xA7,0x67,0xE7, 0x17,0x97,0x57,0xD7, 0x37,0xB7,0x77,0xF7,
  0x0F,0x8F,0x4F,0xCF, 0x2F,0xAF,0x6F,0xEF, 0x1F,0x9F,0x5F,0xDF, 0x3F,0xBF,0x7F,0xFF
};



////////////////////////////////////////////////////////////////////////////////////////
// Constructor

CUTSMemRect::CUTSMemRect()
{    
	m_pMemRect = new CZMemRect();
}

// Destructor

CUTSMemRect::~CUTSMemRect()
{
    delete m_pMemRect;
}

////////////////////////////////////////////////////////////////////////////////////////
// Member Functions
////////////////////////////////////////////////////////////////////////////////////////

BOOL CUTSMemRect::Initialise(void)
{
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// Switch between image processing libraries
//.	  SmartChoiceLib=0,
//	  Intel_IP_Lib=1,
//	  AIC_ImProc_Lib=2,
//	  TEEM_MMX_Lib=3
/////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::UseIntelIPlib(long command)
{
    if (m_pMemRect)
        return m_pMemRect->mrUseIntelIPlib(command);
    else
        return UTS_ERROR;
}

/////////////////////////////////////////////////////////////////////////////
// Check the validity of a UTS image handle
//
// Parameters:
//      UTSHandle is a potential or is a UTS image handle
// Returns:
//      True if it is a UTS handle, FALSE otherwise
/////////////////////////////////////////////////////////////////////////////

BOOL CUTSMemRect::HandleIsValid(long UTSHandle)
{
    DWORD UsedMem, AvailableHandles;
    long  UsedHandles, *pListOfActive;
    int   i;
    BOOL  Ok = FALSE;

    // Check to make sure handle in range
    if (UTSHandle < 1)
        return FALSE;

    // Get the number of handles used so far
    UsedHandles = NumberOfActive(&UsedMem, &AvailableHandles);

    // Now get a list of active memory rectangles
    pListOfActive = new long[UsedHandles];

    if (pListOfActive)
    {
        // Get a list of currently used handles
        ListOfActive(pListOfActive, UsedHandles);

        // Now check the list to see if UTS handle is one of them
        for (i = 0; i < UsedHandles; i++)
            if (pListOfActive[i] == UTSHandle)
            {
                Ok = TRUE;
                break;
            }

        // Tidy
        delete pListOfActive;
    }

	return Ok;
}

////////////////////////////////////////////////////////////////////////////////////////
// Returns the number of handles currently used
// Parameters:
//  None
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::NumberUsedHandles(void)
{
    DWORD UsedMem, AvailableHandles;
    long  UsedHandles, *pListOfActive;

    // Get the number of handles used so far
    UsedHandles = NumberOfActive(&UsedMem, &AvailableHandles);

    // Now get a list of active memory rectangles
    pListOfActive = new long[UsedHandles];

    if (pListOfActive)
    {
        // Get a list of currently used handles
        ListOfActive(pListOfActive, UsedHandles);

        // Tidy
        delete pListOfActive;
    }

	return UsedHandles;
}

////////////////////////////////////////////////////////////////////////////////////////
// Get information about a memory rectangle
// Parameters:
//  Handle              - handle of the image to get
//  HandleInfoStruct    - ASCII text to put information in
//  StructSize          - size of the text
////////////////////////////////////////////////////////////////////////////////////////
 
long CUTSMemRect::HandleInfo(long Handle, char *HandleInfoStruct, long StructSize)
{
	return m_pMemRect->mrHandleInfo(Handle, HandleInfoStruct, StructSize);
}

////////////////////////////////////////////////////////////////////////////////////////
// Create A Memory Rectangle
// Parameters:
//  BitsPerPixel    - Pixel Depth
//  Xe and Ye       - Size of the image
// Returns:
//  A handle if ok zero otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::Create(long BitsPerPixel, long Xe, long Ye)
{
    long h;

    h = m_pMemRect->mrCreate(BitsPerPixel, Xe, Ye);
#ifdef UTS_HANDLE_DEBUG
	CString dbg;
	dbg.Format("Create   HANDLE %d\r\n", h);
	TRACE(dbg);
#endif
    if (h > 0)
		return h;
        
	return 0;
}

////////////////////////////////////////////////////////////////////////////////////////
// Create A Memory Rectangle With The Same Attributes As Handle
// Parameters:
//  Handle - A Valid UTS Rectangle Handle
// Returns:
//  A handle if ok zero otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::CreateAs(long Handle)
{
    long h;

    h = m_pMemRect->mrCreateAs(Handle);
	
#ifdef UTS_HANDLE_DEBUG
	CString dbg;
	dbg.Format("%d CreateAs HANDLE %d\r\n", Handle, h);
	TRACE(dbg);
#endif

    if (h > 0) 
		return h;
        
	return 0;
}
    
////////////////////////////////////////////////////////////////////////////////////////
// Create A Memory Rectangle With The Same Size But Different Bits Per Pixel
// Parameters:
//  Handle       - A Valid UTS Rectangle Handle 
//  BitsPerPixel - Pixel Depth 
// Returns:
//  A handle if ok zero otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::CreateAsWithPix(long Handle, long BitsPerPixel)
{
    long h;

    h = m_pMemRect->mrCreateAsWithPix(Handle, BitsPerPixel);

#ifdef UTS_HANDLE_DEBUG
	CString dbg;
	dbg.Format("CreateBPP HANDLE %d\r\n", h);
	TRACE(dbg);
#endif
    if (h > 0) 
		return h;
        
	return 0;
}

////////////////////////////////////////////////////////////////////////////////////////
// Create a new memory rectangle and copy the contents of the old one
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::CopyAs(long Handle)
{
	long NewHandle;

	NewHandle = CreateAs(Handle);
	if (NewHandle > 0) 
	{
#ifdef UTS_HANDLE_DEBUG
		CString dbg;
		dbg.Format("CopyAs  HANDLE %d\r\n", NewHandle);
		TRACE(dbg);
#endif
		if (Move(Handle, NewHandle) >=0) 
			return NewHandle;
	}

	return UTS_ERROR;
}

////////////////////////////////////////////////////////////////////////////////////////
// Free A Memory Rectangle
// Parameters:
// Hndl - an image handle of the mem rect to remove from memory
// Returns:
// Nothing
////////////////////////////////////////////////////////////////////////////////////////

void CUTSMemRect::FreeImage(long Hndl)
{
    // NOTE This is to get round a bug with the UTS whereby the 
    // image can be freed even if it is declared as external. This
    // should not happen.
    char msg[256];
    
    HandleInfo(Hndl, msg, 256);
    
#ifdef UTS_HANDLE_DEBUG
	CString dbg;
	dbg.Format("Free    HANDLE %d\r\n", Hndl);
	TRACE(dbg);
#endif

    // Check if memory is external to UTS allocated stuff
    // if it is then leave well alone, this will at least
    // prevent any application from popping
    if (!strstr(msg, "extern"))
    {
        //TRACE0(" NOT EXTERN");
        
        m_pMemRect->mrFree(Hndl);
    }
    //TRACE0("\r\n");
}

////////////////////////////////////////////////////////////////////////////////////////
// Free All Memory Rectangles
// Parameters:
// None
// Returns:
// Nothing
////////////////////////////////////////////////////////////////////////////////////////

void CUTSMemRect::FreeAll(void)
{		
#ifdef UTS_HANDLE_DEBUG
	TRACE("FREEALL\r\n");
#endif

	m_pMemRect->mrFreeAllHandles();
}

////////////////////////////////////////////////////////////////////////////////////////
// Set The Bits In A Memory Rectangle
// Parameters:
// lpBits - BYTE pointer to image data
// Hndl   - memory rectangle to set bits in
// Returns:
// TRUE if OK, FALSE if unsuccessful
////////////////////////////////////////////////////////////////////////////////////////

BOOL CUTSMemRect::SetBits(BYTE *lpBits, long Hndl)
{
    long r = m_pMemRect->mrMoveExtern((char *)lpBits, "=>", Hndl);

    return r > 0 ? TRUE : FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////
// Get The Bits In A Memory Rectangle
// Parameters:
//  lpBits - BYTE pointer to image data
//  Hndl   - memory rectangle to get bits from
// Returns:
//  TRUE if OK, FALSE if unsuccessful
////////////////////////////////////////////////////////////////////////////////////////

BOOL CUTSMemRect::GetBits(BYTE *lpBits, long Hndl)
{
    long r = m_pMemRect->mrMoveExtern((char *)lpBits, "<=", Hndl);

    return r > 0 ? TRUE : FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////
// Get The Extents Of A Memory Rectangle
// Parameters:
//  Hndl             - memory rectangle to get bits from
//  Xe, Ye           - image size
// Returns:
//  Bits per pixel for the image, zero otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::GetRectInfo(long Hndl, long *Xe, long *Ye)
{
	return m_pMemRect->mrGetRectInfo(Hndl, Xe, Ye);
}

////////////////////////////////////////////////////////////////////////////////////////
// Get The Original Bits Per Pixel Of A Memory Rectangle
//
// GetRectOriginalBPP() and GetRectInfo() return the same unless the original bit depth 
// was 10, 12 or 14 bpp in which case
//		GetRectOriginalBPP() = 10, 12 or 14
//		GetRectInfo()		 = 16			(the underlying bit depth in memory)
// GetRectOriginalBPP() can be used as a hint to apply initial scalings or
// normalisations after first getting an image when images may come from 10 and 12 bit
// sources.
//
// Parameters:
//  Hndl             - memory rectangle to get bits from
// Returns:
//  Original Bits per pixel for the image, zero otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::GetRectOriginalBPP( long Hndl)
{
	return m_pMemRect->mrGetRectOriginalBPP( Hndl);
}

////////////////////////////////////////////////////////////////////////////////////////
// Get The Extended Info About A Memory Rectangle
// Parameters:
//  Hndl             - memory rectangle to get bits from
//  Xe, Ye           - image size
//  Pitch            - actual length of an image row
//  Addr             - address where image data is stored
// Returns:
//  Bits per pixel for the image
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::GetExtendedInfo(long Hndl, long *Xe, long *Ye, long *Pitch, void *Addr)
{
	return m_pMemRect->mrGetExtendedInfo(Hndl, Xe, Ye, Pitch, Addr);;
}

////////////////////////////////////////////////////////////////////////////////////////
// Convert a memory rectangle to bitmap format
// Parameters:
//  Hndl   - memory rectangle to get bits from
//  hwnd   - window with DC to draw bitmap on
// Returns:
//  A windows handle to a bitmap, NULL otherwise
////////////////////////////////////////////////////////////////////////////////////////

HBITMAP CUTSMemRect::RectToBitmap(long Hndl, HWND hwnd)
{
	return (HBITMAP) m_pMemRect->mrRectToBitMap(Hndl, (long) hwnd);
}

////////////////////////////////////////////////////////////////////////////////////////
// Create An Memory Rectangle Externally
// Parameters:
//  lpBits           - Memory To Put In Image
//  Pixel            - 1, 8, 16, 24
//  Xe and Ye        - Size of the image
//  Q                - Pitch (Width of image in bytes)
// Returns:
//  A handle if ok, UTS_ERROR otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::MakeExtern(void *lpBits, long Pix, long Xe, long Ye, long Q)
{
    return m_pMemRect->mrMakeExtern(lpBits, Pix, Xe, Ye, Q);
}

////////////////////////////////////////////////////////////////////////////////////////
// Permute The Bytes In An Image
// Parameters:
//  Hndl             - UTS image handle
//  GrpLen           - Number of bytes in the permutation
//  Permutation      - Order of the permutation e.g. "321" 
// Returns:
//  TRUE if OK
////////////////////////////////////////////////////////////////////////////////////////

BOOL CUTSMemRect::Permute(long Hndl, long GrpLen, char *Permutation)
{
    long r = m_pMemRect->mrPermute(Hndl, GrpLen, (BYTE *)Permutation);

    return r >= 0 ? TRUE : FALSE;
}


////////////////////////////////////////////////////////////////////////////////////////
// Histogram an image 
// Parameters:
//  Hndl         - a UTS image handle
//  HistoData    - a single dimension array to hold histogram data
// Returns:
//  Number of pixels in whole image, UTS_ERROR if no valid UTS handle, 0 if no library
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::Histogram(long Hndl, void *HistoData)
{
	return m_pMemRect->mrHistogram(Hndl, HistoData);
}

////////////////////////////////////////////////////////////////////////////////////////
// Histogram an masked image 
// Parameters:
//  Hndl         - a UTS image handle
//  HistoData    - a single dimension array to hold histogram data
//  HistoBins    - number of bins in histogam (i.e. size of HistoData array)
// Returns:
//  Number of pixels in whole image, UTS_ERROR if no valid UTS handle, 0 if no library
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::HistogramMask(long Hndl, long MaskHndl, void *HistoData, long HistoBins)
{
	return m_pMemRect->mrHistogramMask(Hndl, MaskHndl, (long *)HistoData, HistoBins);
}

////////////////////////////////////////////////////////////////////////////////////////
// Copy the contents of one image into another
// Parameters:
//  HandleFrom   - the source image
//  HandleTo     - the destination image
// Returns:
//  Number of bytes if moved o.k., zero otherwise
// NOTES:
//	Rewritten as mrMove does not work properly in the unerlying UTS DLL
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::Move(long HandleFrom, long HandleTo)
{
	long SrcBPP, SrcW, SrcH, SrcP, DestBPP, DestW, DestH, DestP;
	void *SrcAddr, *DestAddr;
	long NBytes;

	SrcBPP  = GetExtendedInfo(HandleFrom, &SrcW, &SrcH, &SrcP, &SrcAddr);
	DestBPP = GetExtendedInfo(HandleTo, &DestW, &DestH, &DestP, &DestAddr);

	// Check images are same size and bit depth
	if (SrcW == DestW && SrcH == DestH && SrcBPP == DestBPP)
	{
		NBytes = SrcH * SrcP; // Note the 'pitch' (SRCP) is in Bytes.
		memcpy(DestAddr, SrcAddr, NBytes);
		return NBytes;
	}
    
	return 0;
}

////////////////////////////////////////////////////////////////////////////////////////
// Move the contents of memory into or out of a memory rectangle
// Parameters:
//  *pBits       - memory area to set bits in or get bits from
//  *Direction   - either "=>" (to memory rect) or "<=" (from memory rect)
//  SrcHandle    - the handle to move memory to or from
// Returns:
//  Number of bytes if moved o.k., zero otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::MoveExtern(BYTE *pBits, char *Direction, long SrcHandle)
{
	return m_pMemRect->mrMoveExtern((char *)pBits, Direction, SrcHandle);
}

////////////////////////////////////////////////////////////////////////////////////////
// Moves data between given "small" memory rectangle
// and same size subrectangle in "big" one
// the way of moving is defined by 2 bytes of <way> parameter:
// "=>" : from big to small; keep order of rows
// ">R" : from big to small; reverse order of rows
// "<=" : from small to big; keep order of rows
// "<R" : from small to big; reverse order of rows
// Parameters:
//  BigHandle    - the source image
//  SmlHandle    - the destination image
//  Xleft, Ytop  - coords in big rectangle
//  Direction    - see above
// Returns:
//  Number of bytes if moved o.k., zero otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::MoveSubRect(long BigHndl, long XLeft, long YTop, long SmlHndl, char *Direction)
{
	return m_pMemRect->mrMoveSubRect(BigHndl, XLeft, YTop, SmlHndl, Direction);
}

////////////////////////////////////////////////////////////////////////////////////////
// Calculates the value defined by *operation
// Parameters:
//  Handle       - the source image
//  Operation    - "MIN", "MAX", "SUM", "ONES", "ZEROES"
// Returns:
//  Number of bytes, UTS_ERROR otherwise 
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::Projection0(long Handle, char *Operation)
{
	return m_pMemRect->mrProjection0(Handle, Operation);
}

////////////////////////////////////////////////////////////////////////////////////////
// Threshold an image based upon the specified mask 
// Parameters:
//  Handle       - the source image
//  MaskHndl		- the handle of the mask used for mask to threshold source image with
//  Operation    - ">=", ">", "=", "<", "<=", "<>"
//  Threshold	- value to threshold image based on above operation
// Returns:
//  Number of pixels threshold applied to i.e. number of pixels in mask, UTS_ERROR otherwise 
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::Threshold(long Handle, long MaskHndl, const char *Operation, DWORD Threshold)
{
	return m_pMemRect->mrThreshold(Handle, MaskHndl, (char *)Operation, Threshold);
}

///////////////////////////////////////////////////////////////////////////////////////
// ThresholdGTE : Threshold thresholds an image such that pixels with values
//                greater than or equal to the threshold value are set in the mask.
//                This does the same as Threshold hSrc, hDstMsk, ">=", thresholdVal,
//                but also works for 16bpp images.
///////////////////////////////////////////////////////////////////////////////////////
long CUTSMemRect::ThresholdGTE(long hSrc, long hDstMsk, long threshVal)
{
	BOOL status = FALSE;

	long srcWidth, srcHeight, srcPitch; 
	void *pSrcAddress;
	long srcBpp = GetExtendedInfo(hSrc, &srcWidth, &srcHeight, &srcPitch, &pSrcAddress);

	if (srcBpp == SIXTEEN_BITS_PER_PIXEL)
	{
		long dstWidth, dstHeight, dstPitch;
		void *pDstAddress;
		long dstBpp = GetExtendedInfo(hDstMsk, &dstWidth, &dstHeight, &dstPitch, &pDstAddress);
		if (dstBpp == ONE_BIT_PER_PIXEL && dstWidth == srcWidth && dstHeight == srcHeight)
		{
			BYTE *pSrcLine = (BYTE*)pSrcAddress;

			for (int y = 0; y < srcHeight; y++)
			{
				WORD *pSrc = (WORD*)pSrcLine;

				for (int x = 0; x < srcWidth; x++)
				{
					if (*pSrc++ >= threshVal)
						m_pMemRect->mrPutPixel(hDstMsk, x, y, 1);
				}

				pSrcLine += srcPitch; // The pitch is always in bytes.
			}

			status = TRUE;
		}
	}
	else
		status = (m_pMemRect->mrThreshold(hSrc, hDstMsk, ">=", threshVal) >= 0) ? TRUE : FALSE;

	return status;
}

////////////////////////////////////////////////////////////////////////////////////////
//  Smooth 8 bit per pixel rectangle, according
//  <zoommode> and <neioper>
//  <zoommode> values (case insensitive) are same
//  as for mrZoom():
// 	"Int" : interpolation
// 	"Sub" : subsampling
//  "Rep" : replication
//  <neioper> values (case insensitive) are same
//  as for mrNeiFun():
//     "AVER124" | "ERODE8" | "DILATE8" | "ERODE4" | "DILATE4"
// e.g.
// 	if ( SmoothWithZoom (Pict, 5, 16, "Sub", "AVER124") > 0) ...
// returns
//		ye if Ok,
//		UTS_ERROR otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::SmoothByZoom(long Handle, long Iterations, long ZoomVal, char *Mode, char *NeiOper)
{
	return m_pMemRect->mrSmoothByZoom(Handle, Iterations, ZoomVal, Mode, NeiOper);
}

////////////////////////////////////////////////////////////////////////////////////////
// This function calcs an image according to the following rules:
//   Hndl3 = Gamma + ((Hndl1 * Alpha) + (Hndl2 * Beta)) / 256
// Parameters:
//   Hndl1          - Source Image 1
//   Hndl2          - Source Image 2
//   Hndl3          - Destination Image
//   Alpha          - See above formula
//   Beta           - See above formula
//   Gamma          - See above formula
//   *Data          - Resulting extremes in the Destination Image
//                    Data[0] = Minimum Level In Dest
//                    Data[1] = Maximum Level In Dest
//                    Data[2] = Number Of Pixels With A Negative Result
//                    Data[3] = Number Of Pixels With A Result > 255
//                    NB Data Allowed To be NULL
// Returns:
//  0 If O.k., UTS_ERROR Otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::AlphaBeta(long Hndl1, long Hndl2, long Hndl3, long Alpha, long Beta, long Gamma, long *Data)
{
	return m_pMemRect->mrAlphaBeta(Hndl1, Hndl2, Hndl3, Alpha, Beta, Gamma, Data);
}

////////////////////////////////////////////////////////////////////////////////////////
//   
// Parameters:
//  <hndls> contains mrHandles of <hnum> color components.
//          the transformation is pixelwise, pixel values to be changed so
//          that: 1) sum of values is invariant; 2) new saturation value is
//          as close as possible to piece linear function:
// 		    (0,0) - (x1,y1) - (x2,y2) - (1,1)
// Returns:
//          number of changed pixels if OK,
//    		UTS_ERROR otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::ColorSaturTrans(long *Hndls, long HNum, double X1, double Y1, double X2, double Y2)
{
    return m_pMemRect->mrColorSaturTrans(Hndls, HNum, X1, Y1, X2, Y2);
}


////////////////////////////////////////////////////////////////////////////////////////
// hndlR must be a valid rectangle ; 
// The function operates on 1- or 8-bit pixel data:
//			 
// handlR = hndlA <operation> hndlR
//  the operation is defined by 1st word of the parameter
//  (case insensitive):
//
//	"And",      "1&2"               A & R
//	"More",     "1-2", "1&~2"       A & ~R  (1 if A > R; A without R)
//	"Copy",     "1=>2"              A (copy A into R)
//	"Less",     "2-1", "~1&2" 	    ~A & R  (1 if A < R; R without A)
//	"Xor",      "Mod2", "1^2"       A ^ R  (mod 2 {exclusive OR})
//	"Or",       "1|2" 	            A | R
//	"Nor",      "~(1|2)" 	        ~(A | R)  (not OR)
//	"Equal",    "~(1^2)"            ~(A ^ R)  (1 if A = R)
//	"MoreEq",   "1|~2" 	            A | ~R  (1 if A >= R)
//	"CopyNot",  "~1=>2" 	        copy ~A into R
//	"LessEq",   "~1|2" 	            ~A | R  (1 if A <= R)
//	"Nand",     "~(1&2)" 	        ~(A & R)  (not AND)
//	"MIN" 						    A MIN R
//	"MAX" 						    A MAX R
//	"plus",     "+", "1+2" 		    (A + R) mod 256
//	"+S",       "Min(1+2,255)"      (A + R) min 255 (saturation)
//	"Minus",    "-", "1-2"    	    (A - R) mod 256
//	"-L",       "2-1" 			    (R - A) mod 256 (left minus; inverse order)
//	"-S",       "Max(1-2,0)" 	    (A - R) max 0   (saturation)
//	"-LS",      "-SL", "Max(2-1,0)" (R - A) max 0   (left and saturation)
//	"LessTo0",  "(1>=2)?1:0"        if A >= R then A else 0
//	"MoreToFF", "(1<=2)?1:FF"       if A <= R then A else 255
//	"ByteMask", "(1<2)?0:FF"        if A < R then 0 else 255
//	"-A"					        if A < B then (B - A) else (A - B)
//	"-A8"					        Abs(A - B) MIN (256 - Abs(A - B))
//
// returns:
//		number of processed bytes if OK,
//     	-1 otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::ConstPixelwise(long HndlA, char *Operation, long HndlR)
{
	return m_pMemRect->mrConstPixelwise(HndlA, Operation, HndlR);
}

////////////////////////////////////////////////////////////////////////////////////////
// Const Pixel Mask - see UTS Documentation by people in US of A
// Same as above but with a mask
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::ConstPixelMask(long constant, char *operation, long hndlR, long hndlM)
{
	return m_pMemRect->mrConstPixelMask(constant, operation, hndlR, hndlM);
}

////////////////////////////////////////////////////////////////////////////////////////
// Const Pixel Not Mask - see UTS Documentation by people in US of A
// Same as above but with a inverse mask
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::ConstPixelNotMask(long constant, char *operation, long hndlR, long hndlM)
{
	return m_pMemRect->mrConstPixelNotMask(constant, operation, hndlR, hndlM);
}

////////////////////////////////////////////////////////////////////////////////////////
// Both hndlA, hndlR must be valid rectangles of the same size
// (defined as pitch*yext ); 
// the function operates on 8-bit pixel data:
//
//			 handlR = hndlA <operation> hndlR
//  the operation is defined by 1st word of the parameter
//  (case insensitive):
//			"And",  "1&2"               A & R
//			"More", "1-2", "1&~2"       A & ~R  (1 if A > R; A without R)
//			"Copy", "1=>2"              A (copy A into R)
//			"Less", "2-1", "~1&2" 	    ~A & R  (1 if A < R; R without A)
//			"Xor",  "Mod2", "1^2"       A ^ R  (mod 2 {exclusive OR})
//			"Or",   "1|2" 	            A | R
//			"Nor",  "~(1|2)" 	        ~(A | R)  (not OR)
//			"Equal", "~(1^2)"           ~(A ^ R)  (1 if A = R)
//			"MoreEq", "1|~2" 	        A | ~R  (1 if A >= R)
//			"CopyNot", "~1=>2" 	        copy ~A into R
//			"LessEq", "~1|2" 	        ~A | R  (1 if A <= R)
//			"Nand", "~(1&2)" 	        ~(A & R)  (not AND)
//	        "MIN" 						A MIN R
//	        "MAX" 						A MAX r
//	        "plus", "+", "1+2" 		    (A + R) mod 256
//	        "+S", "Min(1+2,255)"   	    (A + R) min 255 (saturation)
//	        "Minus", "-", "1-2"    	    (A - R) mod 256
//	        "-L", "2-1" 			   	(R - A) mod 256 (left minus; inverse order)
//	        "-S", "Max(1-2,0)" 	   	    (A - R) max 0   (saturation)
//	        "-LS", "-SL", "Max(2-1,0)"  (R - A) max 0   (left and saturation)
//	        "LessTo0", "(1>=2)?1:0"     if A >= R then A else 0
//	        "MoreToFF", "(1<=2)?1:FF"   if A <= R then A else 255
//	        "ByteMask", "(1<2)?0:FF"    if A < R then 0 else 255
//	        "-A"					    if A < B then (B - A) else (A - B)
//	        "-A8"					    Abs(A - B) MIN (256 - Abs(A - B))
//	        "Abs32" 	                R = Abs(A)
//	        "MulS32" 	                R = A * R (Signed multiplication)
//	        "MulU32" 	                R = A * R (Unsigned multiplication)
//          "Diod32" 	                Keep positives, convert negatives to zero
//
// Returns:
// 		number of processed bytes if OK, UTS_ERROR otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::Pixelwise(long HndlA, char *Operation, long HndlR)
{
	return m_pMemRect->mrPixelwise(HndlA, Operation, HndlR);
}

////////////////////////////////////////////////////////////////////////////////////////
// Reconstructs in 'Seed' bit mask within 'Limit'
// begining from point (xc, yc).
// Algorithm of reconstruction is as follows:
//		Do
//			OldSeed := Seed
//			Seed := Dilate(Seed) MIN Limit
//		While OldSeed <> Seed
//
//		Dilate() means "set each pixel to MAX of its neighbors' value"
//		This expansion is limited by Limit.
//
// returns:
// 		Ones in Seed mask if OK, -1 otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::ReconFast(long HndlLimit, long HndlSeed, long Xc, long Yc)
{
	return m_pMemRect->mrReconFast(HndlLimit, HndlSeed, Xc, Yc);
}

////////////////////////////////////////////////////////////////////////////////////////
//		Reconstructs 'Seed' bit mask within 'Limit'.
//		Algorithm of reconstruction is as follows:
//		Do
//			OldSeed := Seed
//			Seed := Dilate(Seed) MIN Limit
//		While OldSeed <> Seed
//
//		Dilate() means "set each pixel to MAX of its neighbors' value"
//		This expansion is limited by Limit.
//
// returns:
// 		if OK: Ones in Seed mask if pix=1; summa of bytes if pix=8;
//        -1 otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::Reconstruct(long HndlLimit, long HndlSeed)
{
    return m_pMemRect->mrReconstruct(HndlLimit, HndlSeed);
}

////////////////////////////////////////////////////////////////////////////////////////
//	returns:
//		square of the distance or UTS_ERROR if empty;
//      updates xp, yp
//
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::DistToMask(long SrcHndl, long *Px, long *Py)
{
	return m_pMemRect->mrDistToMask(SrcHndl, Px, Py);
}

////////////////////////////////////////////////////////////////////////////////////////
//
//		data - pointer to array of not less than 11 longs;
//        		fills it with:
//        	data[0] = NUMBER of one bits in mask
//        	data[1] = SUM of bytes in 'mem' corresponding to one bits in mask
//        	data[2] = MIN of bytes in 'mem' corresponding to one bits in mask
//        	data[3] = MAX of bytes in 'mem' corresponding to one bits in mask
//        	data[4] = X coordinate one of the MIN pixel(s)
//        	data[5] = Y coordinate one of the MIN pixel(s)
//        	data[6] = X coordinate one of the MAX pixel(s)
//        	data[7] = Y coordinate one of the MAX pixel(s)
//        	data[8] = standard deviation:
//            			(long)(sqrt(Sum((P-Aver(P))^2)/NumOf)+0.5)
//       	data[9] = number of pixels with MIN value
//        	data[10] = number of pixels with MAX value
//
//	Returns
//		number of 1-bits in mask (same as data[0]),
//        UTS_ERROR if failure
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::RectMaskData(long SrcHndl8, long MskHndl1, long *Data)
{
	return m_pMemRect->mrRectMaskData(SrcHndl8, MskHndl1, Data);
}

////////////////////////////////////////////////////////////////////////////////////////
//    Fills data with information about linked components in source
//    mask as an array of 7-plets of longs:
//    	{Ni, Xi, Yi, Xmini, Ymini, Xmaxi, Ymaxi},
//	    where
//        	Ni - number of points in the component,
//    		Xi,Yi - coordinates of some point belonging to
//                    the i-th linked component,
//    		Xmini, Ymini, Xmaxi, Ymaxi - min/max X,Y coordinates
//        			of the i-th linked component.
//    maxcomp - max number of components to fit in <data>
//    The result array is sorted by Ni in decreased order.
// 	Returns:
//   	 number of linked components bigger than <minarea>
//             (may be more than <maxcomp>),
//     or -1
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::MaskLinkInfo(long hMask, void *Data, long MaxComp, long MinArea)
{
	return m_pMemRect->mrMaskLinkInfo(hMask, Data, MaxComp, MinArea);
}

////////////////////////////////////////////////////////////////////////////////////////
//long mrStat1 (long hndl, long *Data):
// Data must be at least 15 longs; use bigger constant
//	 <mrStat1DataLength>  defined in <memrectb.h>
// for 1 bit per pixel memory rectangle calculates:
//	 Data[0]:=Sum(1)
//   Data[1]:=Sum(X)
//   Data[2]:=Sum(Y),
//   Data[3]:=Sum(X*X), 
//   Data[4]:=Sum(Y*Y),
//   Data[5]:=Sum(X*Y),
//   Data[6]:=Min(X), 
//   Data[7]:=Max(X),
//   Data[8]:=Min(Y), 
//   Data[9]:=Max(Y),
//   Data[10]:= 1000 * (shortest main axis / longest main axis)
//   Data[11]:= 1000 * (measure of compactness)
//   Data[12]:= 1000 * cos(ALPHA)
//   Data[13]:= 1000 * sin(ALPHA)
//   Data[14]:= 1000 * longest main axis (in pixel/1000)
//   	where ALPHA is an angle between X-axis and longest main axis
// returns: number of ONEs (same as in Data[0]) if OK,
//   	-1 if bad
////////////////////////////////////////////////////////////////////////////////////////
 
long CUTSMemRect::Stat1(long hImage, long *Data)
{
	return m_pMemRect->mrStat1(hImage, Data);
}

////////////////////////////////////////////////////////////////////////////////////////
//
//	Calculates extends in 16 directions, finds extreme ones,
//    calculetes 'centroid' coordinates
//       calipers - array[16] to fill with min/max 'caliper' sizes
//        			of mask ONEs in 16 directions:
//                Angle[I] = Pi/2 - I*(Pi/16) rad	= 90 - I * 11.5 degrees
//                	where I = 0...15
//                	All projections are rounded to longs
//        imin, imax - pointers to get directions of min and max calipers
//        xc, yc - pointers to get X,Y coordinates of the centroid
//        		(average of extreme points); NULL is ok
//
//	Returns: number of ONEs in the mask if OK,
//    		-1 otherwise
//
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::MaskCalipers(long Hndl, long *Calipers, long *Imin, long *Imax, long *Xc, long *Yc)
{
	return m_pMemRect->mrMaskCalipers(Hndl, Calipers, Imin, Imax, Xc, Yc);
}

////////////////////////////////////////////////////////////////////////////////////////
// returns: number of 1-bits in given 1 bit per pixel
//            subrectangle if OK,
// note: value 0 for xe,ye means 'whole dimension'
//   	UTS_ERROR if bad
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::MaskSubrectOnes(long Hndl, long Xl, long Yt, long Xe, long Ye)
{
	return m_pMemRect->mrMaskSubrectOnes(Hndl, Xl, Yt, Xe, Ye);
}

////////////////////////////////////////////////////////////////////////////////////////
//  For 1/8/32 bit per pixel image fills Data[] as follows:
//  Data[0] = pixel size
//  Data[1] = X extend
//  Data[2] = Y extend
//  Data[3] = Min pixel value
//  Data[4] = Max pixel value
//  Data[5] = Sum of pixel values
//  Data[6] = X coordinate of a (first) pixel with Min value
//  Data[7] = Y coordinate of a (first) pixel with Min value
//  Data[8] = X coordinate of a (first) pixel with Max value
//  Data[9] = Y coordinate of a (first) pixel with Max value
//  Data[10] = number of pixels with Min value
//  Data[11] = number of pixels with Max value
//  Returns: pix size of OK, UTS_ERROR if bad
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::MinMaxSumInfo(long Hndl, long *Results)
{
	return m_pMemRect->mrMinMaxSumInfo(Hndl, Results);
}




////////////////////////////////////////////////////////////////////////////////////////
// Convert image levels to another use lParam
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::Convert(long SrcImage, long DestImage, long Param)
{
    return m_pMemRect->mrConvert(SrcImage, DestImage, Param);
}

////////////////////////////////////////////////////////////////////////////////////////
// Convert an 8 bit image into RGB 24 bit format
// hndl8 must be 8/1 bits per pixel;
// for hndl24 row size (defined as BitsPerPixel*Xext) must be
//  3/24 times more than for hndl81; number of rows must be the same;
// the function transforms 8/1 bit(s) pixels into 24 according
//   to color coefficients (in range from 0 to 255); saturation at 255.
// For 8 bpp: negative coefficient means 'use inverted pixel value'.
//   operations: "Fill" , "Add"
// R, G, B are the colour components that Src8 contributes to the image
//   Returns: nothing
// returns:
// 		0 if OK,
//     	-1 otherwise
//  
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::BytesToRGB(long Src8, long Dest24, char *Operation, long R, long G, long B)
{
	return m_pMemRect->mrBytesToRGB(Src8, Dest24, Operation, R, G, B);
}

////////////////////////////////////////////////////////////////////////////////////////
//
// "Zeros"	 fill mask with 0; returns 0
// "Ones"    fill mask with 1; returns 0
// "Inverse" inverse mask; returns 0
// "16oid"	 expand mask to convex 16-oid
//		 returns number of ONEs in the 16-oid
// "32oid"	 expand mask to convex 32-oid
//		 returns number of ONEs in the 32-oid
// "FillHoles0" fills 0-holes in a bit mask;
// 			 a 0-hole is a connected set of ZERO pixels not
//           connected with the boundary.
// "FillHoles1" fills 1-holes in a bit mask;	a 1-hole is a
// 			 connected set of ONE pixels not connected with
//           the boundary.
// "ERODE4", "DILATE4" - erosion/dilation with 4 neighboring;
// "ERODE8", "DILATE8" - erosion/dilation with 8 neighboring,
// "Frame"   frame (1 on sides, 0 inside);
// "Frame0"  not frame (0 on sides, 1 inside);
// "OnesRight"  expand ones left to right
// "OnesLeft"   expand ones right to left
// "OnesDown"   expand ones top to bottom
// "OnesUp"   	 expand ones bottom to top
// "ZerosRight" expand zeros left to right
// "ZerosLeft"  expand zeros right to left
// "ZerosDown"  expand zeros top to bottom
// "ZerosUp"    expand zeros bottom to top
//
//  returns  UTS_ERROR if something wrong
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::OperMask(long SrcImage, char *Operation)
{
	return m_pMemRect->mrOperMask(SrcImage, Operation);
}

////////////////////////////////////////////////////////////////////////////////////////
//  Finds the the ends of a line that intersect with a mask, updating those
//  line ends accordingly.
//	returns:
//      number of pixels in the line and mask crossing;
//      UTS_ERROR if error
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::LineEndsInMask(long SrcMask, long *X1, long *Y1, long *X2, long *Y2)
{
    return m_pMemRect->mrLineEndsInMask(SrcMask, X1, Y1, X2, Y2);
}

////////////////////////////////////////////////////////////////////////////////////////
//   fills array <points> with <npoints> BYTE pixel values;
//	returns:
//      (min pixel << 8) | max pixel;
//      UTS_ERROR if error or wrong parameters
////////////////////////////////////////////////////////////////////////////////////////
    
long CUTSMemRect::ThickProfile(long SrcHandle, void *Points, long NPoints, double X1, double Y1, double X2, double Y2, double RectWidth)
{
	return m_pMemRect->mrThickProfile(SrcHandle, Points, NPoints, X1, Y1, X2, Y2, RectWidth);
}

////////////////////////////////////////////////////////////////////////////////////////
//  Does a profile along a line defined by X1, Y1, X2, Y2 filling an array *BYTES
//  with 256 gray level info
//	returns:
//      UTS_ERROR if something wrong
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::Profile(long hndl, long *points, long npoints, long x1, long y1, long x2, long y2)
{
    long  xe, ye, BPP;
    long  dx, dy, ix, iy, i;

    BPP = GetRectInfo(hndl, &xe, &ye);

    dx = x2 - x1;
    dy = y2 - y1;

    switch (BPP)
    {
    case ONE_BIT_PER_PIXEL:
    case EIGHT_BITS_PER_PIXEL:
	case SIXTEEN_BITS_PER_PIXEL:
        for (i = 0; i < npoints; i++)
        {
            ix = x1 + ((i * dx) / npoints);
            iy = y1 + ((i * dy) / npoints);
            points[i] = GetPixel(hndl, ix, iy);
        }
        return 1;

    default:
        break;
    }

    // Unsupported bits per pixel
    return UTS_ERROR;
}

////////////////////////////////////////////////////////////////////////////////////////
// Converts colour R, G, B components into H, S and I components
// Parameters:
//      R, G and B are handles to the R, G and B components
//      H, S and I are handles to the H, S and I results
// Returns:
//      maximal Intensity if OK, -1 otherwise
// Notes:
//      All images must be of the same dimensions. R, G, B and H, S, I are all
//      8 bit grey level images
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::RGBToHSI(long R, long G, long B, long H, long S, long I)
{
/*
    Calculates Hue, Saturation, Intensity from
    Red, Green, Blue color component values.
    Every H, S, U are normalized to the range {0-255}.
	H = arctan{ ((G-B)/sqr(3)) / (R-I) } =
		arctan{ ((G-B)*sqr(3)) / (3*R-(R+G+B)) }
    Hue values are about:
    0 - Cyan, 42 = Blue, 85 - Magenta, 127 - Red,
    170 - Yellow, 212 - Green, 255 - Cyan again
*/
	return m_pMemRect->mrRGBtoHSI(R, G, B, H, S, I);
}

////////////////////////////////////////////////////////////////////////////////////////
// Reverse of the above
// Parameters:
//      R, G and B are handles to the R, G and B components
//      H, S and I are handles to the H, S and I results
// Returns:
//      Returns ((r+g+b)/3) if OK, -1 otherwise
// Notes:
//      All images must be of the same dimensions. R, G, B and H, S, I are all
//      8 bit grey level images
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::HSIToRGB(long H, long S, long I, long R, long G, long B)
{
	return m_pMemRect->mrHSItoRGB(H, S, I, R, G, B);
}

////////////////////////////////////////////////////////////////////////////////////////
// Converts a colour image into a gray one, using R, G, B colour coefficients such that:
// Sum = (R + G + B)
// if (Sum > 0) then
//    PixelValue = (R * Red + G * Green + B * Blue) / Sum
// Else
//    if (Sum == 0)  then PixelValue = min(Red, Green, Blue)
//    if (Sum == -1) then PixelValue = median(Red, Green, Blue)
//    if (Sum == -2) then PixelValue = max(Red, Green, Blue)
//    if (Sum <= -3) then PixelValue = (Red + Green + Blue + 2) /3
// Endif
// Parameters:
//      RGBImage is the original colour image
//      GrayImage is the resulting gray image
//      R, G, B are the three coefficients
// Returns:
//      0 if OK, UTS_ERROR otherwise
// Notes:
//      Red, Green and Blue refer to the colour components in the RGBImage
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::RGBToGray(long RGBImage, long GrayImage, long R, long G, long B)
{
	return m_pMemRect->mrRGBtoGray(RGBImage, GrayImage, R, G, B);
}
    
////////////////////////////////////////////////////////////////////////////////////////
// Number of active gives statitistics about UTS resources available
// Parameters:
//      UsedMem is the total amount of memory used by the rectangles so far
//      AvailableRectangles is the number of available rectangles left
// Returns:
//      Number of used rectangles
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::NumberOfActive(DWORD *UsedMem, DWORD *AvailableRectangles)
{
    return m_pMemRect->mrNumberOfActive(UsedMem, AvailableRectangles);
}
    
////////////////////////////////////////////////////////////////////////////////////////
// Get a list of active memory handles
// Parameters:
//      UsedMem is the total amount of memory used by the rectangles so far
//      AvailableRectangles is the number of available rectangles left
// Returns:
//      Number of used rectangles
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::ListOfActive(long *ListOfHandles, long MaxLimit)
{
    return m_pMemRect->mrListOfActive(ListOfHandles, MaxLimit);
}

////////////////////////////////////////////////////////////////////////////////////////
/***
 executes neighborhood operation defined by
 1st word of the parameter (case insensitive):
   "AVER124" - averaging with matrix {1,2,1}; {2,4,2}; {1,2,1};
						8 neighboring, 8-bit pixels
   "MEDIAN" - middle value of (8-bit) pixel and it's 8 neighbors
   "ERODE4", "DILATE4" - erosion/dilation with 4 neighboring;
   					 1 or 8 bit pixels supported
   "ERODE8", "DILATE8" - erosion/dilation with 8 neighboring,
   					 1 or 8 bit pixels supported
   "ERODEh2", "DILATEh2" - erosion/dilation with 2 horizontal neighboring,
   					 1 or 8 bit pixels supported
   "ERODEv2", "DILATEv2" - erosion/dilation with 2 vertical neighboring,
   					 1 or 8 bit pixels supported
   "ERODE84", "DILATE84" - erosion/dilation with toggling 8/4 neighboring,
   					 1 or 8 bit pixels supported
   "ERODE48", "DILATE48" - erosion/dilation with toggling 4/8 neighboring,
   					 1 or 8 bit pixels supported
   "Open8", "Close8", "Open4", "Close4",
   "Open84", "Close84", "Open48", "Close48" - opening/closing with
         8, 4 or toggling 8/4 or 4/8 neighboring
 operation repeats <rep> times;
 Open - eroding <rep> times followed by dilating <rep> times
 Close - dilating <rep> times followed by eroding <rep> times
 returns 0 if OK, UTS_ERROR otherwise
***/
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::NeiFun(long SrcImage, char *Oper, long Iterations)
{
    return m_pMemRect->mrNeiFun(SrcImage, Oper, Iterations);
}

////////////////////////////////////////////////////////////////////////////////////////
// Mask out parts of an image specified by Mask and Oper
//
// Parameters:
//  SrcImage is the image to mask out parts of
//  Oper is the operation to use 
//    AND, MORE, Copy, LESS, XOR, OR, NOR, EQUAL, MoreEq, CopyNot, LessEq, Nand 
//  DestImage is the resulting destination image
//  MaskImage is the mask image
// Returns:
//  ????
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::PixelWiseMask(long SrcImage, char *Operation, long DestImage, long MaskImage)
{
	return m_pMemRect->mrPixelwiseMask(SrcImage, Operation, DestImage, MaskImage);
}

////////////////////////////////////////////////////////////////////////////////////////
// CopyUnderMask : Copy pixels from an image that correspond with non-zero pixels in a mask.
//                 This does the same as PixelWiseMask hSrc "Copy" hMask hDst, but
//                 also works for 16bpp images.
////////////////////////////////////////////////////////////////////////////////////////
BOOL CUTSMemRect::CopyUnderMask(long hSrc, long hMsk, long hDst)
{
	BOOL status = FALSE;

	long srcWidth, srcHeight, srcPitch; 
	void *pSrcAddress;
	long srcBpp = GetExtendedInfo(hSrc, &srcWidth, &srcHeight, &srcPitch, &pSrcAddress);

	if (srcBpp == SIXTEEN_BITS_PER_PIXEL)
	{
		long dstWidth, dstHeight, dstPitch;
		void *pDstAddress;
		long dstBpp = GetExtendedInfo(hDst, &dstWidth, &dstHeight, &dstPitch, &pDstAddress);
		if (dstBpp == srcBpp && dstWidth == srcWidth && dstHeight == srcHeight)
		{
			BYTE *pSrcLine = (BYTE*)pSrcAddress;
			BYTE *pDstLine = (BYTE*)pDstAddress;

			for (int y = 0; y < srcHeight; y++)
			{
				WORD *pSrc = (WORD*)pSrcLine;
				WORD *pDst = (WORD*)pDstLine;

				for (int x = 0; x < srcWidth; x++)
				{
					if (m_pMemRect->mrGetPixel(hMsk, x, y))
						*pDst = *pSrc;

					pSrc++;
					pDst++;
				}

				// Note that the pitch is always in bytes.
				pSrcLine += srcPitch;
				pDstLine += dstPitch;
			}

			status = TRUE;
		}
	}
	else
		status = (m_pMemRect->mrPixelwiseMask(hSrc, "Copy", hDst, hMsk) >= 0) ? TRUE : FALSE;

	return status;
}


////////////////////////////////////////////////////////////////////////////////////////
// Multiplies/Divides pixel values in an image e.g. 
// Src[X, Y] = Min(Src[X, Y] * Multiplier / Divisor, 0xFF)
//
// VB syntax:
//      MulDiv SrcImage, Multiplier, Divisor
// Parameters:
//      SrcImage is the image to mask out parts of
//      Multiplier is the value to multiply pixel values by
//      Divisor is the value to divide pixel values by
// Returns:
//      UTS_ERROR if error, 0 no overflow, 1 overflow
// Notes:
//      SrcImage must be 8 bits per pixel
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::MulDiv(long SrcImage, long Multiplier, long Divisor) 
{
    return m_pMemRect->mrMulDiv(SrcImage, Multiplier, Divisor);
}

////////////////////////////////////////////////////////////////////////////////////////
// Get a pixel value from an image given the X & Y coordinates
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::GetPixel(long SrcImage, long X, long Y)
{
    return m_pMemRect->mrGetPixel(SrcImage, X, Y);
}

////////////////////////////////////////////////////////////////////////////////////////
// PutPixel
// Draws a pixel on an image
// Parameters:
//      Image is the image to draw on
//      X, Y are the coordinate to do the drawing
//      Colour is the colour to draw with
// Returns:
//      -1 if an error
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::PutPixel(long Image, long X, long Y, long Colour)
{
    return m_pMemRect->mrPutPixel(Image, X, Y, Colour);
}

////////////////////////////////////////////////////////////////////////////////////////
/***
	Function:	InverseBytes

	Description:
		This function inverses the order of bits inside each byte in
		a given buffer using (static char) InversedBytes[256]

	Statics/Globals:
		InversedBytes[]

	Input Arguments:
		buffer 	- pointer to the buffer
		buffersize - size of the buffer in bytes

	Output Arguments:
		None

	Returns: None
    Original UTS code
***/
////////////////////////////////////////////////////////////////////////////////////////

void CUTSMemRect::InverseBytes(char *buffer, DWORD buffersize)
{
	TableSubst(buffer, buffer, buffersize, InversedBytes);
}

/////////////////////////////////////////////////////////////////////////////////////////
// Use a look up table to transform source memory into dest memory
/////////////////////////////////////////////////////////////////////////////////////////

void CUTSMemRect::TableSubst(char *src, char *dest, DWORD size, void *ptable)
{
	DWORD i;
    BYTE *s, *d, *table;

    if (ptable) 
        table = (BYTE *)ptable; 
    else 
        table = InversedBytes;
    s = (BYTE *)src;  
    d = (BYTE *)dest;
    for (i = 0;  i < size;  i++)
        d[i] = table[s[i]];
}

/////////////////////////////////////////////////////////////////////////////////////////
//    Calculates the mask of borders separating linked components
//    in source mask.
//    nei = 8 or 4;
// 	Returns:
//   	 number of linked components / -1
//
/////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::Borders(long SrcImage, long BordersImage, long NEI)
{
    return m_pMemRect->mrBorders(SrcImage, BordersImage, NEI);
}

/////////////////////////////////////////////////////////////////////////////////////////
//    Calculates the mask of borders separating linked components
//    in source mask.
//    nei = N8 N4 N84 N48;
// 	Returns:
//   	 number of linked components / -1
//
/////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::LinkBorders(long SrcImage, long BordersImage, char *NEI)
{
    return m_pMemRect->mrLinkBorders(SrcImage, BordersImage, NEI);
}

/////////////////////////////////////////////////////////////////////////////////////////
//
//	This function expands mask to convex 32-oid with given thickness
//	Returns: number of ONEs in the 32-oid if OK,
//    		-1 otherwise
/////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::MaskConvexHull(long SrcImage, double Thickness)
{
    return m_pMemRect->mrMaskConvexHull(SrcImage, Thickness);
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// Zoom - zooms one rectangle into another
// Parameters:
//      SrcHandle is the source image
//      DestHandle is the destination of the zoom
//      Method is the method used for the zoom
//      i.e. "REP" for replication
//           "SUB" for subsampling
//           "INT" for interpolation
// Returns:
//      UTS_ERROR if there is a problem
/////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::Zoom(long SrcHandle, long DestHandle, char *Method)
{
    return m_pMemRect->mrZoom(SrcHandle, DestHandle, Method);
}

/////////////////////////////////////////////////////////////////////////////////////////
// DisplayMask - displays a mask overlaid on an image
// Parameters:
//      WndHandl is a valid windows handle for display
//      MaskHandle is the image mask to display
//      Colour is the colour to display the mask as ( = COLORREF expressed as a long)
// Returns:
//      UTS_ERROR if there is a problem
// Notes:
//      Does no stretch to fit etc.
/////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::DisplayMask(long WndHandl, long MaskHandle, long Colour)
{
    return m_pMemRect->mrDisplayMask(WndHandl, 0, 0, MaskHandle, Colour, TRUE);
}

////////////////////////////////////////////////////////////////////////////////////////
// Split a mask into component masks where the masks
// contain mask objects either within the range specified by MinArea and MaxArea
// or those outside of minarea and maxarea
// 
// VB syntax:
//      MaskSplitRange(RangeImage, MinMaskImage, MaxMaskImage, MinArea, MaxArea, Data)
// Parameters:
//      RangeImage is the original image
//      MinMaskImage is the result image containing objects within MinArea, MaxArea
//      MaxMaskImage is the result image containing objects outside MinArea, MaxArea
//      MinArea and MaxArea are the range areas
//      Data is an array of at least 4 longs
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::MaskSplitRange(long RangeImage, long MinImage, long MaxImage, long MinArea, long MaxArea, long *Data)
{
	return m_pMemRect->mrSplitRange(RangeImage, MinImage, MaxImage, MinArea, MaxArea, Data);
}

////////////////////////////////////////////////////////////////////////////////////////
/***
		<option> :
		 "Line"			// polyline from {data[0], data[1]}
						// to {data[2], data[3]}, then
						// to {data[4], data[5]}, and so on
         "SplClosed"	// closed spline
         "SplFree"  	// unclosed spline with free ends
         "SplStrF"		// unclosed spline with straight first interval
         "SplStrL"		// unclosed spline with straight last interval

		 "Circle"   	// circle with center in {data[0], data[1]}
                        // and radius= data[2]
		 "Arc"     		// arc with center in {data[0], data[1]}
                        // from {data[2], data[3]} to {data[4], data[5]}
                        // in clockwise direction

      <color> (if 1 bit/pix) - 0, 1 - put 0/1 pixel; >1 - inverse the pixel.
	  <count> - number of sections for polyline/spline

	returns  -1 if something wrong
***/
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::MaskLine(long Mask, char *Option, long Colour, long nSects, long *Data)
{
    return m_pMemRect->mrMaskLine(Mask, Option, Colour, nSects, Data);
}

////////////////////////////////////////////////////////////////////////////////////////
// Returns the quantiles from a histogram of the image specified by Handle
////////////////////////////////////////////////////////////////////////////////////////

void CUTSMemRect::Quantiles(long Handle, DWORD *res, long NumOf)
{
	m_pMemRect->mrQuantiles(Handle, res, NumOf);
}

////////////////////////////////////////////////////////////////////////////////////////
// Flip rows or columns of an image
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::InvertRows(long Handle)
{
	long status = UTS_ERROR;

	long Xe, Ye;
	long bpp = GetRectInfo(Handle, &Xe, &Ye);

	if (bpp == 24)
	{
		long R = Create (8, Xe, Ye);
		long G = Create (8, Xe, Ye);
		long B = Create (8, Xe, Ye);
		RGBToGray(Handle, R, 255, 0, 0);
		RGBToGray(Handle, G, 0, 255, 0);
		RGBToGray(Handle, B, 0, 0, 255);
		InvertRows(R);
		InvertRows(G);
		InvertRows(B);
		BytesToRGB(R, Handle, "Fill", 255, 0, 0);
		BytesToRGB(G, Handle, "Add", 0, 255, 0);
		BytesToRGB(B, Handle, "Add", 0, 0, 255);

		status = UTS_OK;
	}
	else if (bpp > 0)
		status = m_pMemRect->mrInverseRows(Handle);

	return status;
}

long CUTSMemRect::InvertColumns(long Handle)
{
	long status = UTS_ERROR;

	long Xe, Ye;
	long bpp = GetRectInfo(Handle, &Xe, &Ye);

	if (bpp == 24)
	{
		long R = Create (8, Xe, Ye);
		long G = Create (8, Xe, Ye);
		long B = Create (8, Xe, Ye);
		RGBToGray(Handle, R, 255, 0, 0);
		RGBToGray(Handle, G, 0, 255, 0);
		RGBToGray(Handle, B, 0, 0, 255);
		InvertColumns(R);
		InvertColumns(G);
		InvertColumns(B);
		BytesToRGB(R, Handle, "Fill", 255, 0, 0);
		BytesToRGB(G, Handle, "Add", 0, 255, 0);
		BytesToRGB(B, Handle, "Add", 0, 0, 255);

		status = UTS_OK;
	}
	else if (bpp == 1)
	{
		long retVal = m_pMemRect->mrInverseColumns(Handle);

		if (retVal > -1)
		{
			// mrInverseColumns reverses the order of the bytes in each row
			// (ignoring padding bytes), and the order of the bits in each
			// byte (for 1bpp images), but if the last byte is not complete
			// (i.e. the image width is not a multiple of 8), the spare bits
			// at the end of the last byte will become spare bits at the
			// beginning of the first byte, which will have he effect of
			// shifting the whole image to the right. The following corrects this:
			long pitch;
			void *pData;
			GetExtendedInfo(Handle, &Xe, &Ye, &pitch, &pData);

			// Calculate the number of padding bits at the end of each row.
			int bitshift = 8 - (Xe % 8);
			if (bitshift > 0 && bitshift < 8)
			{
				// Crazy UTS stores the bits in each byte the wrong way
				// around, so must reverse the bit order before any sensibe
				// logic can be used to shift the bits.
				InverseBytes((char*)pData, pitch * Ye);

				int rightshift = 8 - bitshift;

				BYTE *pRow = (BYTE*)pData;
				for (int y = 0; y < Ye; y++)
				{
					BYTE *pRowEnd = pRow + pitch;
					for (BYTE *pB = pRow; pB < pRowEnd; pB++)
					{
						*pB = ((*pB) << bitshift) | (*(pB + 1) >> rightshift);
					}

					pRow += pitch;
				}

				// Restore UTS's bit order.
				InverseBytes((char*)pData, pitch * Ye);
			}

			status = retVal;
		}
	}
	else if (bpp > 0)
	{
		status = m_pMemRect->mrInverseColumns(Handle);
	}

	return status;
}

////////////////////////////////////////////////////////////////////////////////////////
// Perform a convolution using a filter specified by *coeffs. Xf and Yf define the
// size of the filter, Xc and Yc define the centre point of the filter, rshift
// is the value to divide the result by
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::IplConvolution(long Handle, long xf, long yf, long xc, long yc, long *coeffs, long rshift)
{
	return m_pMemRect->mrIplConvolution(Handle, xf, yf, xc, yc, coeffs, rshift);
}

////////////////////////////////////////////////////////////////////////////////////////
// Draw a line of text on an image
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::TextLine(long Handle, long x, long y, char *txt, long height, long colour)
{
	return m_pMemRect->mrTextLine(Handle, x, y, txt, height, colour);
}

////////////////////////////////////////////////////////////////////////////////////////
// Presumably finds the next blob in an image
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::FindNext(long hndl, long *px, long *py, long ZeroOrOne)
{
	return m_pMemRect->mrFindNext(hndl, px, py, ZeroOrOne);
}

////////////////////////////////////////////////////////////////////////////////////////
// See Original UTS Documentation for more info
////////////////////////////////////////////////////////////////////////////////////////

long CUTSMemRect::RecOutCont(long HndlLimit, long HndlSeed, long xc, long yc, int nei4)
{
	return m_pMemRect->mrRecOutCont(HndlLimit, HndlSeed, xc, yc, nei4);
}

long CUTSMemRect::ReconFastD(long hndlLimit, long hndlSeed, long xc, long yc, long *sizedata)
{
	return m_pMemRect->mrReconFastD(hndlLimit, hndlSeed, xc, yc, sizedata);
}


long CUTSMemRect::ReDimension(long hImage, long bpp, long w, long h)
{
	return m_pMemRect->mrRedimension(hImage, bpp, w, h);
}


long CUTSMemRect::Ellipse(long Hndl, long xc, long yc, double a, double b, double phi, long color)
{
    return m_pMemRect->mrEllipse(Hndl, xc, yc, a, b, phi, color);
}

////////////////////////////////////////////////////////////////////////////////////////
// Fast 8 to 1 bit conversion
////////////////////////////////////////////////////////////////////////////////////////

BOOL CUTSMemRect::Convert8To1(long Hnd8, long Hnd1)
{
    BYTE *Addr8, *Addr1, *Ptr8, *Ptr1, B;
    long H8, W8, H1, W1, P8, P1, R, BPP8, BPP1;
    int j, i;
    BOOL Ok = FALSE;

    BPP8 = GetExtendedInfo(Hnd8, &W8, &H8, &P8, &Addr8);
    BPP1 = GetExtendedInfo(Hnd1, &W1, &H1, &P1, &Addr1);

    if (BPP8 == EIGHT_BITS_PER_PIXEL && BPP1 == ONE_BIT_PER_PIXEL && W1 == W8 && H1 == H8)
    {
        R = P8 / 8;

        for (j = 0; j < H1; j++)
        {
            Ptr8 = Addr8 + j * P8;
            Ptr1 = Addr1 + j * P1;
            for (i = 0; i < R; i++)
            {
                *Ptr8++ > 0 ?  B  = 0x01 : B = 0x00;
                if (*Ptr8++ > 0)  B |= 0x02;
                if (*Ptr8++ > 0)  B |= 0x04;
                if (*Ptr8++ > 0)  B |= 0x08;
                if (*Ptr8++ > 0)  B |= 0x10;
                if (*Ptr8++ > 0)  B |= 0x20;
                if (*Ptr8++ > 0)  B |= 0x40;
                if (*Ptr8++ > 0)  B |= 0x80;
                *Ptr1++ = B;
            }
        }

        Ok = TRUE;
    }

    return Ok;
}

////////////////////////////////////////////////////////////////////////////////////////
// Fast 1 to 8 bit conversion
////////////////////////////////////////////////////////////////////////////////////////

BOOL CUTSMemRect::Convert1To8(long Hnd1, long Hnd8)
{
	return Convert1To8WithValue( Hnd1, Hnd8, (BYTE) 0xff);
}

BOOL CUTSMemRect::Convert1To8WithValue(long Hnd1, long Hnd8, BYTE value)
{
    BYTE *Addr8, *Addr1, *Ptr8, *Ptr1, B;
    long H8, W8, H1, W1, P8, P1, R, BPP8, BPP1;
    int j, i;

#define BIT_SET(x, y)    (x & y) ? *Ptr8++ = value : *Ptr8++ = 0x00;

    BPP8 = GetExtendedInfo(Hnd8, &W8, &H8, &P8, &Addr8);
    BPP1 = GetExtendedInfo(Hnd1, &W1, &H1, &P1, &Addr1);

    if (BPP8 == EIGHT_BITS_PER_PIXEL && BPP1 == ONE_BIT_PER_PIXEL && W8 == W1 && H8 == H1)
    {
        R = P8 / 8;

        for (j = 0; j < H1; j++)
        {
            Ptr8 = Addr8 + j * P8;
            Ptr1 = Addr1 + j * P1;
            for (i = 0; i < R; i++)
            {
                B = *Ptr1++;
                BIT_SET(B, 0x01)
                BIT_SET(B, 0x02)
                BIT_SET(B, 0x04)
                BIT_SET(B, 0x08)
                BIT_SET(B, 0x10)
                BIT_SET(B, 0x20)
                BIT_SET(B, 0x40)
                BIT_SET(B, 0x80)
            }
        }

        return TRUE;
    }

    return FALSE;
}


BOOL CUTSMemRect::Convert16To8(long hSrc, long hDst)
{
	BOOL status = FALSE;

	long srcWidth, srcHeight, srcPitch; 
	long dstWidth, dstHeight, dstPitch;
	void *pSrcAddress, *pDstAddress;

	long srcBpp = GetExtendedInfo(hSrc, &srcWidth, &srcHeight, &srcPitch, &pSrcAddress);
	long dstBpp = GetExtendedInfo(hDst, &dstWidth, &dstHeight, &dstPitch, &pDstAddress);

	if (   srcBpp == SIXTEEN_BITS_PER_PIXEL && dstBpp == EIGHT_BITS_PER_PIXEL
	    && srcWidth == dstWidth && srcHeight == dstHeight)
	{
		BYTE *pSrcLine = (BYTE*)pSrcAddress;
		BYTE *pDstLine = (BYTE*)pDstAddress;

		for (int y = 0; y < srcHeight; y++)
		{
			BYTE *pSrc = pSrcLine;
			BYTE *pDst = pDstLine;
					
			for (int x = 0; x < srcWidth; x++)
			{
				pSrc++; // Skip least significant byte (assuming little-endian).
				*pDst++ = *pSrc++; // Copy most significant byte.
			}

			pSrcLine += srcPitch;
			pDstLine += dstPitch;
		}

		status = TRUE;
	}

	return status;
}

// Stretch down to 8 bits by scaling down to a range between min and max then 0-255
BOOL CUTSMemRect::Convert16To8(long hSrc, long hDst, WORD Min, WORD Max)
{
	BOOL status = FALSE;

	long srcWidth, srcHeight, srcPitch; 
	long dstWidth, dstHeight, dstPitch;
	void *pSrcAddress, *pDstAddress;

	if (Max - Min <= 0)
		return FALSE;

	float Scale = 255.0f / (Max - Min);

	long srcBpp = GetExtendedInfo(hSrc, &srcWidth, &srcHeight, &srcPitch, &pSrcAddress);
	long dstBpp = GetExtendedInfo(hDst, &dstWidth, &dstHeight, &dstPitch, &pDstAddress);

	if (  srcBpp == SIXTEEN_BITS_PER_PIXEL && dstBpp == EIGHT_BITS_PER_PIXEL
	    && srcWidth == dstWidth && srcHeight == dstHeight)
	{
		WORD *pSrcLine = (WORD*)pSrcAddress;
		BYTE *pDstLine = (BYTE*)pDstAddress;

		for (int y = 0; y < srcHeight; y++)
		{
			WORD *pSrc = pSrcLine;
			BYTE *pDst = pDstLine;
					
			for (int x = 0; x < srcWidth; x++)
			{
				float g = (*pSrc++ - Min) * Scale; // Skip least significant byte (assuming little-endian).
				if (g >= 0.f && g <= 255.0f)
					*pDst++ = (BYTE)g;
				else
				if (g > 255.0f)
					*pDst++ = 255;
				else
					*pDst++ = 0;
			}

			pSrcLine += srcPitch / sizeof(WORD);
			pDstLine += dstPitch;
		}

		status = TRUE;
	}

	return status;
}

BOOL CUTSMemRect::Convert8To16(long hSrc, long hDst)
{
	BOOL status = FALSE;

	long srcWidth, srcHeight, srcPitch; 
	long dstWidth, dstHeight, dstPitch;
	void *pSrcAddress, *pDstAddress;

	long srcBpp = GetExtendedInfo(hSrc, &srcWidth, &srcHeight, &srcPitch, &pSrcAddress);
	long dstBpp = GetExtendedInfo(hDst, &dstWidth, &dstHeight, &dstPitch, &pDstAddress);

	if (   srcBpp == EIGHT_BITS_PER_PIXEL && dstBpp == SIXTEEN_BITS_PER_PIXEL
	    && srcWidth == dstWidth && srcHeight == dstHeight)
	{
		BYTE *pSrcLine = (BYTE*)pSrcAddress;
		BYTE *pDstLine = (BYTE*)pDstAddress;

		for (int y = 0; y < srcHeight; y++)
		{
			BYTE *pSrc = pSrcLine;
			BYTE *pDst = pDstLine;
					
			for (int x = 0; x < srcWidth; x++)
			{
				*pDst++ = 0; // Zero least significant byte (assuming little-endian).
				*pDst++ = *pSrc++; // Copy most significant byte.
			}

			pSrcLine += srcPitch;
			pDstLine += dstPitch;
		}

		status = TRUE;
	}

	return status;
}


BOOL CUTSMemRect::ContrastStretch(long hImage)
{
	BOOL status = FALSE;

	long width, height, pitch; 
	void *pAddress;

	long bpp = GetExtendedInfo(hImage, &width, &height, &pitch, &pAddress);

	if ((bpp == EIGHT_BITS_PER_PIXEL || bpp == SIXTEEN_BITS_PER_PIXEL) && width > 0 && height > 0 && pAddress)
	{
		// Find minimum and maximum pixel values in the image.
		unsigned long min = INT_MAX;
		unsigned long max = 0;

		BYTE *pLine = (BYTE*)pAddress;

		for (int y = 0; y < height; y++)
		{
			if (bpp == EIGHT_BITS_PER_PIXEL)
			{
				BYTE *pPixel = pLine;
				for (int x = 0; x < width; x++, pPixel++)
				{
					if (*pPixel < min) min = *pPixel;
					if (*pPixel > max) max = *pPixel;
				}
			}
			else // SIXTEEN_BITS_PER_PIXEL
			{
				WORD *pPixel = (WORD*)pLine;
				for (int x = 0; x < width; x++, pPixel++)
				{
					if (*pPixel < min) min = *pPixel;
					if (*pPixel > max) max = *pPixel;
				}
			}

			pLine += pitch;
		}

		// If the minimum or maximum possible pixel values are not being used,
		// linearly stretch the image's values to fill the full range.
		unsigned long maxPossiblePixelValue = (1 << bpp) - 1;

		if (min < max && (min > 0 || max < maxPossiblePixelValue))
		{
			unsigned long range = max - min;

			pLine = (BYTE*)pAddress;

			for (int y = 0; y < height; y++)
			{
				if (bpp == EIGHT_BITS_PER_PIXEL)
				{
					BYTE *pPixel = pLine;
					for (int x = 0; x < width; x++)
					{
						if (*pPixel > min)
							*pPixel++ = ((*pPixel - min) * maxPossiblePixelValue) / range;
						else
							*pPixel++ = 0;
					}
				}
				else // SIXTEEN_BITS_PER_PIXEL
				{
					WORD *pPixel = (WORD*)pLine;
					for (int x = 0; x < width; x++)
					{
						if (*pPixel > min)
							*pPixel++ = ((*pPixel - min) * maxPossiblePixelValue) / range;
						else
							*pPixel++ = 0;
					}
				}

				pLine += pitch;
			}
		}

		status = TRUE;
	}

	return status;
}

////////////////////////////////////////////////////////////////////////////////////////
// Removes "iterations" pixels from tips of skeleton.
////////////////////////////////////////////////////////////////////////////////////////
BOOL CUTSMemRect::TrimTips( long hSrc, long hDst, int iterations)
{
	BOOL status = FALSE;

	// Check source and destination are 8 bit
	long srcWidth, srcHeight, srcPitch, srcBPP;
	long dstWidth, dstHeight, dstPitch, dstBPP;
	void *pSrcImage, *pDstImage;

	srcBPP = GetExtendedInfo( hSrc, &srcWidth, &srcHeight, &srcPitch, &pSrcImage);
	dstBPP = GetExtendedInfo( hDst, &dstWidth, &dstHeight, &dstPitch, &pDstImage);

	if (srcWidth == dstWidth && srcHeight == dstHeight &&
		srcBPP == dstBPP &&
		srcBPP == EIGHT_BITS_PER_PIXEL &&
		iterations > 0)
	{
		// Some temporary images
		long hTmpA = NULL;
		long tmpAWidth, tmpAHeight, tmpAPitch;
		long hTmpB = NULL;
		long tmpBWidth, tmpBHeight, tmpBPitch;
		void *pTmpAImage, *pTmpBImage;

		if (iterations > 1)
		{
			hTmpA = CreateAsWithPix( hSrc, EIGHT_BITS_PER_PIXEL);
			GetExtendedInfo( hTmpA, &tmpAWidth, &tmpAHeight, &tmpAPitch, &pTmpAImage);

			hTmpB = CreateAsWithPix( hSrc, EIGHT_BITS_PER_PIXEL);
			GetExtendedInfo( hTmpB, &tmpBWidth, &tmpBHeight, &tmpBPitch, &pTmpBImage);
		}

		void * pIm1;
		void * pIm2;

		for (int i=0; i<iterations; i++)
		{
			// Keep flipping the results between the temp images but remember
			// to put the final result into the destination image
			if (i == 0)
			{
				if (iterations == 1)
				{
					pIm1 = pSrcImage;
					pIm2 = pDstImage;
				}
				else
				{
					pIm1 = pSrcImage;
					pIm2 = pTmpAImage;
				}
			}
			else if (i == iterations-1)
			{
				pIm1 = pIm2;
				pIm2 = pDstImage;
			}
			else
			{
				pIm1 = pIm2;
				pIm2 = (pIm1 == pTmpAImage)  ?  pTmpBImage : pTmpAImage;
			}

			TrimTipsKernel( (BYTE *) pIm1, (BYTE *) pIm2, srcWidth, srcHeight, srcPitch, dstPitch);
		}

		FreeImage( hTmpA);
		FreeImage( hTmpB);

		status = TRUE;
	}

	return status;
}

////////////////////////////////////////////////////////////////////////////////////////
// Kernel for triming tips from skeletons
////////////////////////////////////////////////////////////////////////////////////////
void CUTSMemRect::TrimTipsKernel( BYTE * pIm1, BYTE * pIm2, long srcWidth, long srcHeight, long srcPitch, long dstPitch)
{
	// Prepare neighbourhood offsets
	int offsets[9] = {	-1-srcPitch, -srcPitch, -srcPitch+1,
						-1,               0,             +1,
						-1+srcPitch, +srcPitch, +srcPitch+1 };
	int count;

	// Ignore first and last row
	BYTE * im1Buffer = pIm1 + srcPitch;
	BYTE * im2Buffer = pIm2 + dstPitch;
	BYTE * im1;
	BYTE * im2;

	for (int row = 1; row < srcHeight-1; row++)
	{
		// Ignore first and last column
		im1 = im1Buffer + 1;
		im2 = im2Buffer + 1;

		for (int col = 1; col < srcWidth-1; col++)
		{
			*im2 = *im1;

			if (*im1 > 0)
			{
				// Does the 3 x 3 neighbourhood have 2 points?
				//  => then it's an end point so remove it
				count=0;
				for (int k=0; k<9; k++)
				{
					if (im1[offsets[k]] > 0)
					{
						count++;
					}
				}

				if (count == 2)
				{
					*im2 = 0;
				}
			}

			*im1++;
			*im2++;
		}

		im1Buffer += srcPitch;
		im2Buffer += dstPitch;
	}
}

////////////////////////////////////////////////////////////////////////////////////////
// Blanks pixels within a border of the edge of the image.
////////////////////////////////////////////////////////////////////////////////////////
BOOL CUTSMemRect::BlankBorder( long hSrc, int blankWidth)
{
	BOOL status = FALSE;

	// Check source is 8 bit
	long srcWidth, srcHeight, srcPitch, srcBPP;
	void *pSrcImage;

	srcBPP = GetExtendedInfo( hSrc, &srcWidth, &srcHeight, &srcPitch, &pSrcImage);

	if (srcBPP == EIGHT_BITS_PER_PIXEL &&
		blankWidth > 0 &&
		blankWidth < srcWidth/2 &&
		blankWidth < srcHeight/2)
	{
		BYTE * srcBuffer;
		BYTE * src;

		// Blank top of image
		srcBuffer = (BYTE *) pSrcImage;
		for (int row = 0; row < blankWidth; row++)
		{
			src = srcBuffer;

			for (int col = 0; col < srcWidth; col++)	*src++ = 0;

			srcBuffer += srcPitch;
		}

		// Blank left and right edges of image
		srcBuffer = (BYTE *) pSrcImage + srcPitch * blankWidth;
		for (int row = blankWidth; row < srcHeight - blankWidth - 1; row++)
		{
			src = srcBuffer;

			for (int col = 0; col < blankWidth; col++)						*src++ = 0;
			for (int col = blankWidth; col < srcWidth-blankWidth; col++)	src++;
			for (int col = srcWidth-blankWidth; col < srcWidth; col++)		*src++ = 0;

			srcBuffer += srcPitch;
		}

		// Blank bottom of image
		srcBuffer = (BYTE *) pSrcImage + srcPitch * (srcHeight - blankWidth);
		for (int row = srcHeight - blankWidth; row < srcHeight; row++)
		{
			src = srcBuffer;

			for (int col = 0; col < srcWidth; col++)	*src++ = 0;

			srcBuffer += srcPitch;
		}

		status = TRUE;
	}

	return status;
}

////////////////////////////////////////////////////////////////////////////////////////
// Computes the local grey level variance image.
// This implementation doesnt leave gaps around the edge of an image.
////////////////////////////////////////////////////////////////////////////////////////
BOOL CUTSMemRect::Variance( long hSrc, long hDst, BYTE radius)
{
	BOOL status = FALSE;

	// Check source and destination are 8 bit
	long srcWidth, srcHeight, srcPitch, srcBPP;
	long dstWidth, dstHeight, dstPitch, dstBPP;
	void *pSrcImage, *pDstImage;

	srcBPP = GetExtendedInfo( hSrc, &srcWidth, &srcHeight, &srcPitch, &pSrcImage);
	dstBPP = GetExtendedInfo( hDst, &dstWidth, &dstHeight, &dstPitch, &pDstImage);

	if (srcWidth == dstWidth && srcHeight == dstHeight &&
		srcBPP == dstBPP &&
		srcBPP == EIGHT_BITS_PER_PIXEL &&
		radius > 0)
	{
		int iImageWidth = srcWidth;
		int iImageHeight = srcHeight;
		int iDepthLimit = (int) ( (pow( 2.0, srcBPP) - 1.0) + 0.5);

		BYTE* srcbuffer  = (BYTE*) pSrcImage;
		BYTE* destbuffer = (BYTE*) pDstImage;

		// TODO: 
		// Use cirular neighbourhood for filter to give more faithful, less blocky result

		double sum;
		double sumofsq;
		double mean;
		double variance;
		int filterRowStart;
		int filterRowEnd;
		int filterColStart;
		int filterColEnd;
		int count;

		// for each source pixel
		BYTE* src = srcbuffer;
		BYTE* dest = destbuffer;

		BYTE* nSrcRow;
		BYTE* nSrc;

		for (int row = 0; row < iImageHeight; row++)
		{
			for (int col = 0; col < iImageWidth; col++)
			{
				// for each filter position
				sum = 0.0;
				sumofsq = 0.0;
				filterRowStart = max(0, row - radius);
				filterRowEnd = min(iImageHeight - 1, row + radius);
				filterColStart = max(0, col - radius);
				filterColEnd = min(iImageWidth - 1, col + radius);
				int nRowCount = filterRowEnd - filterRowStart + 1;
				int nColCount = filterColEnd - filterColStart + 1;

				int offsetToNeighbourhoodStart = -((row - filterRowStart) * iImageWidth) - (col - filterColStart);
				nSrcRow = src + offsetToNeighbourhoodStart;

				for (int nRow = 0; nRow < nRowCount; nRow++)
				{
					nSrc = nSrcRow;

					for (int nCol = 0; nCol < nColCount; nCol++)
					{
						sum += *nSrc;
						sumofsq += (*nSrc) * (*nSrc);

						nSrc++;
					}

					nSrcRow  += iImageWidth;
				}

				count = nRowCount * nColCount;
				mean = sum / count;
				variance = (sumofsq / count) - mean * mean;

				*dest = (BYTE) min((int)variance, iDepthLimit);

				src++;
				dest++;
			}
		}

		status = TRUE;
	}

	return status;
}


////////////////////////////////////////////////////////////////////////////////////////
// Computes the local grey level variance image.
// This implementation doesnt leave gaps around the edge of an image.
////////////////////////////////////////////////////////////////////////////////////////
BOOL CUTSMemRect::LocalMaxima( long hSrc, long hDst, BYTE radius)
{
	BOOL status = FALSE;

	// Check source and destination are 8 bit
	long srcWidth, srcHeight, srcPitch, srcBPP;
	long dstWidth, dstHeight, dstPitch, dstBPP;
	void *pSrcImage, *pDstImage;

	srcBPP = GetExtendedInfo( hSrc, &srcWidth, &srcHeight, &srcPitch, &pSrcImage);
	dstBPP = GetExtendedInfo( hDst, &dstWidth, &dstHeight, &dstPitch, &pDstImage);

	if (srcWidth == dstWidth && srcHeight == dstHeight &&
		srcBPP == dstBPP &&
		srcBPP == EIGHT_BITS_PER_PIXEL &&
		radius > 0)
	{
		int iImageWidth = srcWidth;
		int iImageHeight = srcHeight;
		int iDepthLimit = (int) ( (pow( 2.0, srcBPP) - 1.0) + 0.5);

		BYTE* srcbuffer  = (BYTE*) pSrcImage;
		BYTE* destbuffer = (BYTE*) pDstImage;

		// TODO: 
		// Use cirular neighbourhood for filter to give more faithful, less blocky result

		int filterRowStart;
		int filterRowEnd;
		int filterColStart;
		int filterColEnd;
		int nRowCount;
		int nColCount;
		int offsetToNeighbourhoodStart;
		BYTE v;
		int countGreater;


		// for each source pixel
		BYTE* src = srcbuffer;
		BYTE* dest = destbuffer;

		BYTE* nSrcRow;
		BYTE* nSrc;

		for (int row = 0; row < iImageHeight; row++)
		{
			for (int col = 0; col < iImageWidth; col++)
			{
				// for each filter position
				filterRowStart = max(0, row - radius);
				filterRowEnd = min(iImageHeight - 1, row + radius);
				filterColStart = max(0, col - radius);
				filterColEnd = min(iImageWidth - 1, col + radius);
				nRowCount = filterRowEnd - filterRowStart + 1;
				nColCount = filterColEnd - filterColStart + 1;

				offsetToNeighbourhoodStart = -((row - filterRowStart) * iImageWidth) - (col - filterColStart);
				nSrcRow = src + offsetToNeighbourhoodStart;

				v = *src;
				countGreater = 0;

				for (int nRow = 0; nRow < nRowCount; nRow++)
				{
					nSrc = nSrcRow;

					for (int nCol = 0; nCol < nColCount; nCol++)
					{
						if (v >= *nSrc)
						{
							countGreater++;
						}

						nSrc++;
					}

					nSrcRow  += iImageWidth;
				}

				*dest = (BYTE) min(countGreater, iDepthLimit);

				src++;
				dest++;
			}
		}

		status = TRUE;
	}

	return status;
}

void CUTSMemRect::CutImageFrag(long SrcImage, long CentreX, long CentreY, long DestImage, long *Xleft, long *Ytop)
{
	m_pMemRect->mrCutImageFrag(SrcImage, CentreX, CentreY, DestImage, Xleft, Ytop);
}




#if 0
// Performs a contrast stretch on 8-bit RGB components, so that the top 10% of the area under each histogram
// is shifted up to 255.
long CUTSMemRect::ColourContrastStretch(long R, long G, long B, long CSR, long CSS, long CSI)
{
	int		i, y, n, x, j;
	int		Thresh = -1;
	long	BPP, Height, Width, Pitch;
	BYTE	*AddrR, *ImPtrR, *AddrG, *ImPtrG, *AddrB, *ImPtrB;
	BYTE	*AddrH, *ImPtrH, *AddrS, *ImPtrS, *AddrI, *ImPtrI;
	DWORD	HistR[NBINS_EIGHT];
	DWORD	HistG[NBINS_EIGHT];
	DWORD	HistB[NBINS_EIGHT];
	double	prob[NBINS_EIGHT];
	double	Hn, Ps, Hs;
	double	psi, psiMax;

	// Get image info
	BPP = UTSMemRect.GetExtendedInfo(R, &Width, &Height, &Pitch, &AddrR);
	BPP = UTSMemRect.GetExtendedInfo(G, &Width, &Height, &Pitch, &AddrG);
	BPP = UTSMemRect.GetExtendedInfo(B, &Width, &Height, &Pitch, &AddrB);

	// Must be an 8 bpp image
    if (BPP == EIGHT_BITS_PER_PIXEL)
	{
		// Initialise the histograms
		for (i = 0; i < NBINS_EIGHT; i++)
			HistR[i] = HistG[i] = HistB[i] = 0;

		// Generate hitsograms
		n = Width * Height;
		ImPtrR = AddrR; ImPtrG = AddrG; ImPtrB = AddrB;
		for (y = 0; y < Height; y++) 
		{
			for (x = 0; x < Width; x++) 
			{
				HistR[*ImPtrR++]++;
				HistG[*ImPtrG++]++;
				HistB[*ImPtrB++]++;
			}
			ImPtrR += (Pitch - Width);
			ImPtrG += (Pitch - Width);
			ImPtrB += (Pitch - Width);
		}
	}

	// Measure total area under each histogram
	double	TotalAreaR = 0.0;
	double	TotalAreaG = 0.0;
	double	TotalAreaB = 0.0;

	for (i = 0; i < NBINS_EIGHT; i++)
	{
		TotalAreaR +=  (double)HistR[i];
		TotalAreaG +=  (double)HistG[i];
		TotalAreaB +=  (double)HistB[i];
	}


	// Find the level at which 10% of the area of each histogram is
	double AreaR = 0.0;
	double pcTotalAreaR = TotalAreaR/10.0;
	for (i = NBINS_EIGHT-1; i >= 0; i--)
	{
		AreaR +=  (double)HistR[i];
		if (AreaR >= pcTotalAreaR)
			break;
	}
	int redi = i;

	double AreaG = 0.0;
	double pcTotalAreaG = TotalAreaG/10.0;
	for (i = NBINS_EIGHT-1; i >= 0; i--)
	{
		AreaG +=  (double)HistG[i];
		if (AreaG >= pcTotalAreaG)
			break;
	}
	int greeni = i;

	double AreaB = 0.0;
	double pcTotalAreaB = TotalAreaB/10.0;
	for (i = NBINS_EIGHT-1; i >= 0; i--)
	{
		AreaB +=  (double)HistB[i];
		if (AreaB >= pcTotalAreaB)
			break;
	}
	int bluei = i;


	// Stratch each image so that the 10%level calculated above is shifted up to 255
	// Get image info
	BPP = UTSMemRect.GetExtendedInfo(R, &Width, &Height, &Pitch, &AddrR);
	BPP = UTSMemRect.GetExtendedInfo(G, &Width, &Height, &Pitch, &AddrG);
	BPP = UTSMemRect.GetExtendedInfo(B, &Width, &Height, &Pitch, &AddrB);

	BPP = UTSMemRect.GetExtendedInfo(H, &Width, &Height, &Pitch, &AddrH);
	BPP = UTSMemRect.GetExtendedInfo(S, &Width, &Height, &Pitch, &AddrS);
	BPP = UTSMemRect.GetExtendedInfo(I, &Width, &Height, &Pitch, &AddrI);

	// Must be an 8 bpp image
    if (BPP == EIGHT_BITS_PER_PIXEL)
	{

		n = Width * Height;
		ImPtrR = AddrR; ImPtrG = AddrG; ImPtrB = AddrB;
		ImPtrH = AddrH; ImPtrS = AddrS; ImPtrI = AddrI;
		for (y = 0; y < Height; y++) 
		{
			for (x = 0; x < Width; x++) 
			{
				int newr = 255 * (*ImPtrR)/redi;
				int newg = 255 * (*ImPtrG)/greeni;
				int newb = 255 * (*ImPtrB)/bluei;

				if (newr > 255) newr = 255;
				if (newg > 255) newg = 255;
				if (newb > 255) newb = 255;

				*ImPtrH = newr;
				*ImPtrS = newg;
				*ImPtrI = newb;

				ImPtrR++;
				ImPtrG++;
				ImPtrB++;

				ImPtrH++;
				ImPtrS++;
				ImPtrI++;
			}

			ImPtrR += (Pitch - Width);
			ImPtrG += (Pitch - Width);
			ImPtrB += (Pitch - Width);

			ImPtrH += (Pitch - Width);
			ImPtrS += (Pitch - Width);
			ImPtrI += (Pitch - Width);
		}
	}


	return 1;
}
#endif










