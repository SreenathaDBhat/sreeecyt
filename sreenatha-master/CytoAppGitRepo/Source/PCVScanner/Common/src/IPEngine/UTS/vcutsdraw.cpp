////////////////////////////////////////////////////////////////////////////////////////
// Useful routines for drawing UTS images and graphs etc.
// All direct API calls to avoid use of MFC
// 
// Written By Karl Ratcliff 05052001
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//    TODO: Needs a good clean up 
//

#include "stdafx.h"
#include <math.h>
#include "pseudocolour.h"
#include "vcutsdraw.h"
#include "vcmemrect.h"
#include "uts.h"




CDibInfo::CDibInfo()
{
    m_pDibInfo1  = new BITMAPINFO1BIT;
    m_pDibInfo8  = new BITMAPINFO8BIT;
    m_pDibInfo16 = new BITMAPINFO16BIT;
    m_pDibInfo24 = new BITMAPINFO24BIT;
	m_pDibInfo32 = new BITMAPINFO32BIT;

    m_pDibPseudo = new BITMAPINFO8BIT;

    
		int i;

        // DIB header - Single Bit
        memset(m_pDibInfo1, 0, sizeof(BITMAPINFO1BIT));
        m_pDibInfo1->bmiHeader.biPlanes = 1; 
        m_pDibInfo1->bmiHeader.biBitCount = 1; 
        m_pDibInfo1->bmiHeader.biCompression = BI_RGB; 
        m_pDibInfo1->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
        
        m_pDibInfo1->bmiColors[0].rgbRed   = 0;
        m_pDibInfo1->bmiColors[0].rgbGreen = 0;
        m_pDibInfo1->bmiColors[0].rgbBlue  = 0;
        m_pDibInfo1->bmiColors[1].rgbRed   = 0xFF;
        m_pDibInfo1->bmiColors[1].rgbGreen = 0xFF;
        m_pDibInfo1->bmiColors[1].rgbBlue  = 0xFF;
        
        // DIB header - 8 Bits
        memset(m_pDibInfo8, 0, sizeof(BITMAPINFO8BIT));
        m_pDibInfo8->bmiHeader.biPlanes = 1; 
        m_pDibInfo8->bmiHeader.biBitCount = 8; 
        m_pDibInfo8->bmiHeader.biCompression = BI_RGB; 
        m_pDibInfo8->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
        m_pDibInfo8->bmiHeader.biClrUsed = 256;
        
        for (i = 0; i < 256; i++)
        {
            m_pDibInfo8->bmiColors[i].rgbRed   = i;
            m_pDibInfo8->bmiColors[i].rgbGreen = i;
            m_pDibInfo8->bmiColors[i].rgbBlue  = i;
        }
        
		// DIB header - 16 Bits
		// Note that this is 16bpp RGB, i.e. 5 bits per channel
		// - DIBs don't support 16bpp greyscale.
        memset(m_pDibInfo16, 0, sizeof(BITMAPINFO16BIT));
        m_pDibInfo16->bmiHeader.biPlanes = 1; 
        m_pDibInfo16->bmiHeader.biBitCount = 16; 
        m_pDibInfo16->bmiHeader.biCompression = BI_RGB; 
        m_pDibInfo16->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
        
        m_pDibInfo16->bmiColors.rgbRed   = 0;
        m_pDibInfo16->bmiColors.rgbGreen = 0;
        m_pDibInfo16->bmiColors.rgbBlue  = 0;

        // DIB header - 24 Bits
        memset(m_pDibInfo24, 0, sizeof(BITMAPINFO24BIT));
        m_pDibInfo24->bmiHeader.biPlanes = 1; 
        m_pDibInfo24->bmiHeader.biBitCount = 24; 
        m_pDibInfo24->bmiHeader.biCompression = BI_RGB; 
        m_pDibInfo24->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
        
        m_pDibInfo24->bmiColors.rgbRed   = 0;
        m_pDibInfo24->bmiColors.rgbGreen = 0;
        m_pDibInfo24->bmiColors.rgbBlue  = 0;

         // DIB header - 32 Bits
        memset(m_pDibInfo32, 0, sizeof(BITMAPINFO32BIT));
        m_pDibInfo32->bmiHeader.biPlanes = 1; 
        m_pDibInfo32->bmiHeader.biBitCount = 32; 
        m_pDibInfo32->bmiHeader.biCompression = BI_RGB; 
        m_pDibInfo32->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
        
        m_pDibInfo32->bmiColors.rgbRed   = 0;
        m_pDibInfo32->bmiColors.rgbGreen = 0;
        m_pDibInfo32->bmiColors.rgbBlue  = 0;
               
        // DIB header - 8 Bits pseudocolour
        memset(m_pDibPseudo, 0, sizeof(BITMAPINFO8BIT));
        m_pDibPseudo->bmiHeader.biPlanes = 1; 
        m_pDibPseudo->bmiHeader.biBitCount = 8; 
        m_pDibPseudo->bmiHeader.biCompression = BI_RGB; 
        m_pDibPseudo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
        m_pDibPseudo->bmiHeader.biClrUsed = 256;
	    int j;

        for (j = 0; j < 256; j++)
        {
		    PseudoColour(j, &m_pDibPseudo->bmiColors[j].rgbRed, &m_pDibPseudo->bmiColors[j].rgbGreen, &m_pDibPseudo->bmiColors[j].rgbBlue);
        }
}


CDibInfo::~CDibInfo()
{
	delete m_pDibInfo1;
    delete m_pDibInfo8;
    delete m_pDibInfo24;
	delete m_pDibInfo32;
    delete m_pDibPseudo;
}


////////////////////////////////////////////////////////////////////////////////////////
// Gets the pointer to a newly allocated BITMAPINFO structure
// Parameters:
//      BPP is the BitsPerPixel of the image
//      Width is the width of the image
//      Height is the height of the image
// Returns:
//      The address of the BITMAPINFO* pointer
// Notes:
//      pBMP will be a NULL pointer if we can't return a BITMAPINFO*. The created
//      object must be disposed of after use.
////////////////////////////////////////////////////////////////////////////////////////

BITMAPINFO* CDibInfo::GetBitmapInfo(long BPP, long Width, long Height)
{
    BITMAPINFO  *pBMP = NULL;

    switch (BPP)
    {
    case ONE_BIT_PER_PIXEL:
        pBMP = (BITMAPINFO *) new BITMAPINFO1BIT;
        if (pBMP)
            memcpy(pBMP, m_pDibInfo1, sizeof(BITMAPINFO1BIT));
        break;
        
    case EIGHT_BITS_PER_PIXEL:
        pBMP = (BITMAPINFO *) new BITMAPINFO8BIT;
        if (pBMP) 
            memcpy(pBMP, m_pDibInfo8, sizeof(BITMAPINFO8BIT));
        break;

	case SIXTEEN_BITS_PER_PIXEL:
        pBMP = (BITMAPINFO *) new BITMAPINFO16BIT;
        if (pBMP) 
            memcpy(pBMP, m_pDibInfo16, sizeof(BITMAPINFO16BIT));
        break;
        
    case TWENTYFOUR_BITS_PER_PIXEL:
        pBMP = (BITMAPINFO *) new BITMAPINFO24BIT;
        if (pBMP) 
            memcpy(pBMP, m_pDibInfo24, sizeof(BITMAPINFO24BIT));
        break;

	case THIRTYTWO_BITS_PER_PIXEL:
		pBMP = (BITMAPINFO *) new BITMAPINFO32BIT;
		if (pBMP) 
			memcpy(pBMP, m_pDibInfo32, sizeof(BITMAPINFO32BIT));
		break;
        
    default:
        break;
    }
    
    if (pBMP)
    {
        pBMP->bmiHeader.biWidth  = Width;
        pBMP->bmiHeader.biHeight = Height;
    }
    
    return pBMP;
}
    
////////////////////////////////////////////////////////////////////////////////////////
// Do a similar thing for 8 bit pseudo colour
////////////////////////////////////////////////////////////////////////////////////////

BITMAPINFO *CDibInfo::GetBitmapPseudoInfo(long BPP, long W, long H)
{
    BITMAPINFO *pBMP = NULL;

    if (BPP == EIGHT_BITS_PER_PIXEL)
    {
        pBMP = (BITMAPINFO *) new BITMAPINFO8BIT;
        if (pBMP) 
        {
            memcpy(pBMP, m_pDibPseudo, sizeof(BITMAPINFO8BIT));
            pBMP->bmiHeader.biWidth = W;
            pBMP->bmiHeader.biHeight = H;
        }
    }

    return pBMP;
}

////////////////////////////////////////////////////////////////////////////////////////
// Constructor

CUTSDraw::CUTSDraw(CUTSMemRect *pMR)
{
	pUTSMemRect = pMR;
}

// Destructor

CUTSDraw::~CUTSDraw()
{
}

////////////////////////////////////////////////////////////////////////////////////////
// UTSHistogramWndDisplay
// Description: displays the grabbed image plus anything else on the screen
// Parameters:	hWnd - Window to draw to
//              Hndl - UTS histogram handle to display
////////////////////////////////////////////////////////////////////////////////////////

void CUTSDraw::UTSHistogramWndDisplay(HWND hWnd, long Hndl)
{
    HDC         DrawDC;
    int         DestW, DestH, i;
    DWORD       *histdata = NULL;
    long        Xe, Ye, P, BPP;
    DWORD       Hmin, Hmax;
    HPEN        hpenh, holdpen, hpenw;   
    int         HistoX, HistoY, HistoHeight, HistoRangeY, HistoWidth, H1, H2;
    RECT        DestRect;
    int         OldROP;

    if (UTSVALID_HANDLE(Hndl))
    {
        DrawDC = GetDC(hWnd);
        if (!DrawDC)
        {
            TRACE0("Could not get DC in draw histogram\r\n");
            return;
        }

        GetClientRect(hWnd, &DestRect);
        
        // Get width and height of destination rectangle
        DestW = RECTWIDTH(&DestRect);
        DestH = RECTHEIGHT(&DestRect);

        // Get Histogram Information
        BPP = pUTSMemRect->GetExtendedInfo(Hndl, &Xe, &Ye, &P, &histdata);
        
        if (histdata && Xe)
        {
            // Go Though The Histogram To Find Min/Max Values
            Hmin = (DWORD) 9223372036854775807;
            Hmax = 0;
            
            // Start Search
            for (i = 0; i < Xe; i++)
            {
                if (histdata[i] > Hmax) Hmax = histdata[i];
                if (histdata[i] < Hmin) Hmin = histdata[i];
            }
            
            // Pens for drawing the histogram
            hpenh = CreatePen(PS_SOLID, 1, LINE_COLOUR);
            hpenw = CreatePen(PS_SOLID, 1, AXIS_COLOUR);
            
            // Calcs for position of histogram
            HistoHeight = (DestH * PLOT_SCALE1) / SCALE_DIVISOR;
            HistoY = (DestH * PLOT_SCALE2) /  SCALE_DIVISOR;
            HistoX = DestW /  SCALE_DIVISOR;
            
            // Prevent division by zero
            HistoRangeY = Hmax - Hmin;
            if (HistoRangeY == 0)
                HistoRangeY = 1;
            
            if (DestW > (DestW * PLOT_SCALE1) /  SCALE_DIVISOR)
                HistoWidth = (DestW * PLOT_SCALE1) /  SCALE_DIVISOR;
            else
                HistoWidth = 1;
            
            OldROP = SetROP2(DrawDC, R2_COPYPEN);

            holdpen = (HPEN) SelectObject(DrawDC, hpenh);
            // Draw a histogram of grey levels
            for (i = 1; i < Xe; i++)
            {
                H1 = HistoY - (((histdata[i] - Hmin) * HistoHeight) / HistoRangeY);
                H2 = HistoY - (((histdata[i - 1] - Hmin) * HistoHeight) / HistoRangeY);
                MoveToEx(DrawDC, HistoX + ((i - 1) * HistoWidth) / Xe, H2, NULL);
                LineTo(DrawDC, HistoX + (i * HistoWidth) / Xe, H1);
            }
            
            DrawAxes(DrawDC, hpenw, HistoX, HistoY, Xe, HistoWidth, HistoRangeY, HistoHeight, 4, 4);

            // Reselect Old DC objects
            SetROP2(DrawDC, OldROP);
            SelectObject(DrawDC, holdpen);
            DeleteObject(hpenh);
            DeleteObject(hpenw);
        }
             
        ReleaseDC(hWnd, DrawDC);
    }
}

////////////////////////////////////////////////////////////////////////////////////////
// GetHistogramRect
// Description: calcs the histogram area relative to the client area of a histogram
//              window
// Parameters:	hWnd - Window to with histogram in
//              PRect - rectangle to receive histogram area
////////////////////////////////////////////////////////////////////////////////////////

void CUTSDraw::GetHistogramRect(HWND hWnd, LPRECT pRect)
{
    RECT    cRect;
    long    cW, cH;
    long    HistoW, HistoH;

    GetClientRect(hWnd, &cRect);

    cW = RECTWIDTH(&cRect);
    cH = RECTHEIGHT(&cRect);

    HistoW = (cW * PLOT_SCALE1) / SCALE_DIVISOR;
    HistoH = (cH * PLOT_SCALE1) / SCALE_DIVISOR;
    pRect->left   = (cW - HistoW) / 2;
    pRect->right  = pRect->left + HistoW;
    pRect->top    = (cH - HistoH) / 2;
    pRect->bottom = pRect->top + HistoH;
}

////////////////////////////////////////////////////////////////////////////////////////
// GetHistogramExtremes
// Description: gets the extremes of a histogram stored in a UTS handle
// Parameters:  Hndl - single dimensioned UTS mem rectangle with histogram data
//              *HistoRangeX, *HistoRangeY - X & Y ranges for plotting purposes
////////////////////////////////////////////////////////////////////////////////////////

void CUTSDraw::GetHistogramExtremes(long Hndl, long *HistoRangeX, long *HistoRangeY)
{
    long    Ye, P;
    DWORD   Hmin, Hmax;
    DWORD   *histdata;
    int     i;

    // Get Histogram Information
    pUTSMemRect->GetExtendedInfo(Hndl, HistoRangeX, &Ye, &P, &histdata);
    
    if (histdata && (*HistoRangeX))
    {
        // Go Though The Histogram To Find Min/Max Values
        Hmin = (DWORD) (*HistoRangeX) * Ye + 1;
        Hmax = 0;
        
        // Start Search
        for (i = 0; i < (*HistoRangeX); i++)
        {
            if (histdata[i] > Hmax) Hmax = histdata[i];
            if (histdata[i] < Hmin) Hmin = histdata[i];
        }
        
        // Prevent division by zero
        *HistoRangeY = Hmax - Hmin;
        if (*HistoRangeY == 0)
            *HistoRangeY = 1;
    }
    else
    {
        // Safe values
        *HistoRangeX = 0;
        *HistoRangeY = 0;
    }
}

////////////////////////////////////////////////////////////////////////////////////////
// Draws a profile on an image with the profile taken between (X1, Y1) and (X2, Y2)
// Parameters:
//      hWnd is the handle to draw on
//      UTShandle is the UTS handle of the image
//      x1, y1 and x2, y2 specify points on the image to take the profile from
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void CUTSDraw::DrawProfile(HWND hWnd, long UTSHandle, long x1, long y1, long x2, long y2)
{
    int         DestW, DestH, i, OldROP;
    DWORD       *histdata = NULL;
    long        dx, dy, npoints, BPP, W, H;
    BYTE        max = 255, min = 0;
    long        *points;
    HPEN        hpenh, hpenw, holdpen;  
    HDC         DrawDC;
    RECT        DestRect;
    int         ProfX, ProfY, ProfHeight, ProfRangeY, ProfWidth, H1, H2, X1, X2;

    if (UTSVALID_HANDLE(UTSHandle))
    {
        // Get the total number of points required
        dx = x1 - x2;
        dy = y1 - y2;
        if (dx || dy)
            npoints = (long) sqrt((double)(dx * dx + dy * dy));
        else
            npoints = 0;
        
        // Only do if we got more than zero points
        if (npoints)
        {
		
            // Get some mem to put the points in
            points = new long[npoints];
            
            if (points)
            {
                // Get Profgram Information
                pUTSMemRect->Profile(UTSHandle, points, npoints, x1, y1, x2, y2);
                BPP = pUTSMemRect->GetRectInfo(UTSHandle, &W, &H);
                
                // Get the DC
                DrawDC = GetDC(hWnd);
                // Get the client area
                GetClientRect(hWnd, &DestRect);
                // Get width and height of destination rectangle
                DestW = RECTWIDTH(&DestRect);
                DestH = RECTHEIGHT(&DestRect);

                OldROP = SetROP2(DrawDC, R2_MASKPENNOT);

                // Pens for drawing the Profgram
                hpenh = CreatePen(PS_SOLID, 1, LINE_COLOUR);
                hpenw = CreatePen(PS_SOLID, 1, AXIS_COLOUR);
                
                holdpen = (HPEN) SelectObject(DrawDC, hpenh);
                
                // Calcs for position of Profgram
                ProfHeight = (DestH * PLOT_SCALE1) / SCALE_DIVISOR;
                ProfY = (DestH * PLOT_SCALE2) / SCALE_DIVISOR;
                ProfX = DestW / SCALE_DIVISOR;
                
				switch (BPP)
				{
				case ONE_BIT_PER_PIXEL:
                    ProfRangeY = 1;
					break;

				case EIGHT_BITS_PER_PIXEL:
                    ProfRangeY = 255;
					break;

				case SIXTEEN_BITS_PER_PIXEL:
                    ProfRangeY = 65535;
					break;
				}

                if (DestW > (DestW * PLOT_SCALE1) /  SCALE_DIVISOR)
                    ProfWidth = (DestW * PLOT_SCALE1) /  SCALE_DIVISOR;
                else
                    ProfWidth = 1;
                // Draw a Profgram of grey levels
                for (i = 1; i < npoints; i++)
                {
                    H1 = ProfY - ((points[i - 1] * ProfHeight) / ProfRangeY);
                    H2 = ProfY - ((points[i] * ProfHeight) / ProfRangeY);
                    X1 = ProfX + (((i - 1) * ProfWidth) / npoints);
                    X2 = ProfX + ((i  * ProfWidth) / npoints);
                    MoveToEx(DrawDC, X1, H1, NULL);
                    LineTo(DrawDC, X2, H2);
                }
                
                DrawAxes(DrawDC, hpenw, ProfX, ProfY, npoints, ProfWidth, ProfRangeY, ProfHeight, 4, 4);

                // Reselect Old DC objects
                SetROP2(DrawDC, OldROP);
                DeleteObject(hpenw);
                DeleteObject(hpenh);
                ReleaseDC(hWnd, DrawDC);
                // Dispose of memory
                delete [] points;
            }
        }
    }
}



////////////////////////////////////////////////////////////////////////////////////////
// Draw some axes
// Parameters:
//      TheDC is the DC to draw on
//      AxesPen is the pen to draw with
//      Xorg, Yorg are the window coords for the origin
//      Xrange is the range of X values
//      Xwidth is the length of the X axis
//      Yrange is the range of Y values
//      Yheight is the length of the Y axis
//      Xticks, Yticks are the number of tick marks for each axis
// Returns:
//      Nothing
////////////////////////////////////////////////////////////////////////////////////////

void CUTSDraw::DrawAxes(HDC TheDC, HPEN AxesPen, int Xorg, int Yorg, 
                                                 int Xrange, int Xwidth, 
                                                 int Yrange, int Yheight, 
                                                 int Xticks, int Yticks)
{
    TCHAR        str[255];
    TEXTMETRIC  tm;
    int         i;
    int         H1;

    // Draw The Axes
    SelectObject(TheDC, AxesPen);
    MoveToEx(TheDC, Xorg, Yorg, NULL);
    LineTo(TheDC, Xorg + Xwidth, Yorg);
    MoveToEx(TheDC, Xorg, Yorg, NULL);
    LineTo(TheDC, Xorg, Yorg - Yheight);
    
    SetBkMode(TheDC, TRANSPARENT); 
    SetTextAlign(TheDC, TA_CENTER);
    GetTextMetrics(TheDC, &tm);
    SetTextColor(TheDC, TEXT_COLOUR);
    
    // Do the tick marks
    // X Axis
    for (i = 0; i <= Xticks; i++)
    {
        _stprintf(str, _T("%d"), (Xrange * i) / Xticks);
        TextOut(TheDC, Xorg + (i * Xwidth) / Xticks, Yorg + TICK_MARK_SIZE, str, _tcslen(str));
        
        MoveToEx(TheDC, Xorg + (i * Xwidth) / Xticks, Yorg, NULL);
        LineTo(TheDC, Xorg + (i * Xwidth) / Xticks, Yorg + TICK_MARK_SIZE);
    }
    
    SetTextAlign(TheDC, TA_RIGHT | TA_TOP);
    
    // Y Axis
    for (i = 0; i <= Yticks; i++)
    {
        _stprintf(str, _T("%d"), (Yrange * i) / Yticks);
        H1 = Yorg  - (i * Yheight) / Yticks;
        TextOut(TheDC, Xorg - TICK_MARK_SIZE, H1 - tm.tmHeight / 2, str, _tcslen(str));
        
        MoveToEx(TheDC, Xorg, H1, NULL);
        LineTo(TheDC, Xorg - TICK_MARK_SIZE, H1);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Output text onto an image 
// Replaces the textline function in the UTS 
// Parameters:
//      Handle  - is the UTS image handle
//      Text    - is the text to draw on the image
//      x, y    - are the x, y location of the text
//      Height  - is the text height in 1/10th of a point
//      Colour  - is the colour to use - NOTE if working with one bit images 
//                Colour is always 0xFFFFFF
/////////////////////////////////////////////////////////////////////////////////////////////

void CUTSDraw::ImageTextOut(long Handle, LPCTSTR Text, LPCTSTR FontName, int x, int y, int Height, DWORD Colour)
{
    HDC         hdc, MemDC = NULL;
    HBITMAP	    hbmBMP = NULL;		// Handle of D-I BMP to use for blit to control	
    long        ImageHeight, ImageWidth, BPP, Pitch;
    BYTE        *image = NULL, *destimage = NULL, *pBits, *Addr, *FinalImage = NULL;
    BITMAPINFO  *pBMP = NULL;
    CFont       Fnt;

    // Get Image Info
    BPP = pUTSMemRect->GetExtendedInfo(Handle, &ImageWidth, &ImageHeight, &Pitch, &Addr);
    
	if (BPP != UTS_ERROR)
	{
		try 
		{
            image = Addr;

			// Create a memory DC for the bitmap
			hdc = GetDC(NULL);

			if (hdc != NULL)
			{
				MemDC = CreateCompatibleDC(hdc);
				if (!MemDC) throw;
				
				// Process images based on their bits per pixel
				if (BPP == ONE_BIT_PER_PIXEL)
				{
					// Allocate some mem
					destimage = new BYTE[Pitch * ImageHeight];
					// Get the bits out of the UTS image
					memcpy(destimage, image, Pitch * ImageHeight);
					// Reverse the bit order in each byte of the image
					pUTSMemRect->InverseBytes((char *) destimage, Pitch * ImageHeight); 
					image = destimage;
					// If colour is not black then set it to white for 1 bit images 
					if (Colour)
						Colour = 0xFFFFFF;
				}       
				else
				if (BPP == EIGHT_BITS_PER_PIXEL)
				{
					Colour = RGB(Colour, Colour, Colour);
				}
				
				pBMP = GetDibInfo()->GetBitmapInfo(BPP, ImageWidth, ImageHeight);
				
				// Get out if NULL pBMP
				if (!pBMP) throw;
				
				// Create a bmp for drawing 
				hbmBMP = CreateDIBSection(MemDC, pBMP, DIB_RGB_COLORS, reinterpret_cast<LPVOID*>(&pBits), NULL, 0);
				// Set the BMP bits
				memcpy(pBits, image, Pitch * ImageHeight);
	            
				// Select the BMP into the DC
				SelectObject(MemDC, hbmBMP);

				SetBkMode(MemDC, TRANSPARENT);

				// Create the font
				Fnt.CreatePointFont(Height, FontName, NULL);
				// Draw the text
				if (!SelectObject(MemDC, Fnt.GetSafeHandle()))
					TRACE("COULD NOT SELECT FONT\r\n");
				SetTextColor(MemDC, Colour);
	            
				if (!TextOut(MemDC, x, y, Text, _tcslen(Text)))
				{
					TRACE("ERROR TEXT OUT");
					throw;
				}

				Fnt.DeleteObject();
				// Allocate yet another block of memory
				FinalImage = new BYTE[Pitch * ImageHeight];

				// Get the bits back out the BMP
				GetDIBits(MemDC, hbmBMP, 0, ImageHeight, FinalImage, pBMP, DIB_RGB_COLORS);

				if (BPP == ONE_BIT_PER_PIXEL)
				{
					// Reverse the bit order in each byte of the image
					pUTSMemRect->InverseBytes((char *) FinalImage, Pitch * ImageHeight); 
				}
				// Copy these back into the original UTS image
				memcpy(Addr, FinalImage, Pitch * ImageHeight);

				delete pBMP;

				ReleaseDC(NULL, hdc);
			}
		}
		
		
		catch (...)
		{
			TRACE0("GDI UTS DRAW ERROR\r\n");
		}
		
		// Release any used mem
        if (destimage)
            delete destimage;
        if (FinalImage)
            delete FinalImage;
		
		// Free any other resources used
		if (hbmBMP)
			DeleteObject(SelectObject(MemDC, hbmBMP));
		if (MemDC)
			DeleteDC(MemDC);
	}
}
