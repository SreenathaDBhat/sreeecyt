////////////////////////////////////////////////////////////////////////////////////////
//
// Useful UTS file I/O routines
//
// Written By Karl Ratcliff 14052001
//
////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
// 1. DIB routines done to overcome the problems with the existing UTS BMP routines
// 2. Install the Intel JPEG Library
// 3. link "ijl15.lib"
//

#include "stdafx.h"
#include "ijl.h"
#include "widembcs.h"
#include "vcmemrect.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "uts.h"
#include "math.h"
#include "compressor.h"
#include <ImageBits.h>

#pragma comment(lib, "gdiplus.lib")

#include <gdiplus.h>

using namespace Gdiplus;


CUTSFileIO::CUTSFileIO(CUTSMemRect *pMR)
{
	pUTSMemRect = pMR;
}

////////////////////////////////////////////////////////////////////////////////////////
// Loads a DIB from a file, creating a UTS image to store it in
// Parameters:
//      filename is the name of the file we want to load
//      External declares the memory rectangle to be externally defined
// Returns:
//      a valid UTS handle if successful, UTS_ERROR otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSFileIO::LoadDIB(LPCTSTR filename, BOOL External)
{
    size_t      colour_table_size, pixels_size;
    long        num_colours;
    HANDLE      hFile;
    DWORD       dwRead = 0, dwMoved;   
    BOOL        read_ok;
    BITMAPFILEHEADER bmfh;
    BITMAPINFOHEADER bmih;
    long        UTSHandle = UTS_ERROR, ExternalHandle = UTS_ERROR;
    long        H, W, P, BPP;
    void        *UTSAddr, *ExternAddr;
                
    // open the file for shared reading
    hFile = CreateFile(filename, 
        GENERIC_READ, 
        FILE_SHARE_READ, 
        NULL,
        OPEN_EXISTING, 
        FILE_ATTRIBUTE_NORMAL, 
        NULL);
    
    if (hFile != INVALID_HANDLE_VALUE)
    {
        // read the file header
        read_ok = ReadFile(hFile, &bmfh, sizeof(BITMAPFILEHEADER), &dwRead, NULL);
        
        if (read_ok && (dwRead == sizeof(BITMAPFILEHEADER)))
        {
            // check the format
            if (bmfh.bfType == 0x4D42) // BM
            {
                // read the DIB's header
                read_ok = ReadFile(hFile, &bmih, sizeof(BITMAPINFOHEADER), &dwRead, NULL);
                
                if (read_ok && (dwRead == sizeof(BITMAPINFOHEADER)))
                {
                    // get the number of colors in the color table
                    num_colours = bmih.biClrUsed;

                    if (num_colours == 0 && bmih.biBitCount <= 8)
                        num_colours = (1 << bmih.biBitCount);
                    
                    // compute the size of the color table
                    colour_table_size = sizeof(RGBQUAD) * num_colours;
                    
                    // compute the size of the image
                    if (bmih.biSizeImage != 0)
                        pixels_size = bmih.biSizeImage;
                    else
                        pixels_size = abs(bmih.biHeight) * ((((bmih.biWidth * bmih.biBitCount) + 31) & ~31) >> 3);
                    
                    // Allocate a UTS handle
                    UTSHandle = pUTSMemRect->Create(bmih.biBitCount, bmih.biWidth, bmih.biHeight);

                    if (UTSVALID_HANDLE(UTSHandle))
                    {
                        // Get the address of the allocated UTS memory
                        BPP = pUTSMemRect->GetExtendedInfo(UTSHandle, &W, &H, &P, &UTSAddr);
                        // move to the pixels
                        dwMoved = SetFilePointer(hFile, bmfh.bfOffBits, 0, FILE_BEGIN);
                        if (dwMoved == bmfh.bfOffBits)
                        {
                            // read the pixels
                            read_ok = ReadFile(hFile, UTSAddr, pixels_size, &dwRead, NULL);
                            if (!read_ok || (dwRead != pixels_size))
                            {
                                pUTSMemRect->FreeImage(UTSHandle);
                                UTSHandle = UTS_ERROR;
                            }
                            else
                            {
                                // transform the pixels if 1 Bit/Pixel format
                                if (BPP == ONE_BIT_PER_PIXEL)
                                {
                                    // Invert the bit order in each byte
                                    pUTSMemRect->InverseBytes((char *) UTSAddr, H * P);
                                }

                                // Convert the memory rectangle to an external one if required
                                if (External)
                                {
                                    // Allocate external memory
                                    ExternAddr = malloc(pixels_size);
                                    if (ExternAddr)
                                        ExternalHandle = pUTSMemRect->MakeExtern(ExternAddr, BPP, W, H, 0);
                                    // Move data to external memory and free the original UTS handle
                                    if (ExternalHandle != -1)
                                    {
                                        MoveMemory(ExternAddr, UTSAddr, pixels_size);       // Copy the memory
                                        pUTSMemRect->FreeImage(UTSHandle);                         // Free the original UTS image
                                        UTSHandle = ExternalHandle;                         // Use the external handle instead
                                    }
									else
										UTSHandle = UTS_ERROR;
                                }
                            }
                        }
                        else
                        {
                            pUTSMemRect->FreeImage(UTSHandle);
                            UTSHandle = UTS_ERROR;
                        }
                    }
                    else
                    {
                        pUTSMemRect->FreeImage(UTSHandle);
                        UTSHandle = UTS_ERROR;
                    }
                }
                else
                    UTSHandle = UTS_ERROR;
            }
        }
        // close the file
        CloseHandle(hFile);
    }
    
   return UTSHandle;
}

////////////////////////////////////////////////////////////////////////////////////////
// Saves a UTS image to a DIB file
// Parameters:
//      filename is the name of the file to save
//      UTSHandle is the handle of the image we want to save to that file
// Returns:
//      GetlastError() 
////////////////////////////////////////////////////////////////////////////////////////

DWORD CUTSFileIO::SaveDIB(LPCTSTR filename, long UTSHandle)
{
    long            W, H, P, BPP;
    void            *Addr;
    BOOL            write_ok;
    DWORD           dwWritten = 0;
    DWORD           dwBytesWrote = 0;
    BITMAPINFO      *pbmi;
    BITMAPFILEHEADER bmfh;
    size_t          pixels_size;
    size_t          colour_table_size;
    size_t          num_colours;
    BYTE            *DestAddr = NULL;

    if (!UTSVALID_HANDLE(UTSHandle)) return 0;
    
    BPP = pUTSMemRect->GetExtendedInfo(UTSHandle, &W, &H, &P, &Addr);
	pbmi = GetDibInfo()->GetBitmapInfo(BPP, W, H);

    // Process images based on their bits per pixel
    if (BPP == ONE_BIT_PER_PIXEL)
    {

        // Allocate some mem
        DestAddr = new BYTE[P * H];
        // Get the bits out of the UTS image
        memcpy(DestAddr, Addr, P * H);
        // Reverse the bit order in each byte of the image
        pUTSMemRect->InverseBytes((char *) DestAddr, P * H); 
        Addr = DestAddr;
    }        
            
    // first determine the size (in bytes) of the color table
    num_colours = pbmi->bmiHeader.biClrUsed;

    if (num_colours == 0 && pbmi->bmiHeader.biBitCount < 8)
        num_colours = (1 << pbmi->bmiHeader.biBitCount);

    colour_table_size = sizeof(RGBQUAD) * num_colours;
    
    // next, determine the size (in bytes) of the pixels array
    if (pbmi->bmiHeader.biSizeImage != 0)
        pixels_size = pbmi->bmiHeader.biSizeImage; 
    else
        pixels_size = abs(pbmi->bmiHeader.biHeight) * 
                      ((((pbmi->bmiHeader.biWidth * pbmi->bmiHeader.biBitCount) + 31) & ~31) >> 3);
    
    // initialize the file header
    bmfh.bfType = 0x4D42;   // 'B''M'
    
    // size of the file header + size of the DIB's header + size of the color table + size of the pixels array
    bmfh.bfSize = sizeof(BITMAPFILEHEADER) +
                  sizeof(BITMAPINFOHEADER) +
                  colour_table_size +
                  pixels_size;

    bmfh.bfOffBits = bmfh.bfSize - pixels_size;
    
    // create the file
    HANDLE hFile = CreateFile(filename, 
                        GENERIC_WRITE, 
                        0, 
                        NULL,
                        CREATE_ALWAYS, 
                        FILE_ATTRIBUTE_NORMAL, 
                        NULL);
    
    if (hFile != INVALID_HANDLE_VALUE)
    {
        // write the file header
        write_ok = WriteFile(hFile, &bmfh, sizeof(BITMAPFILEHEADER), &dwWritten, NULL);
        if (write_ok && (dwWritten == sizeof(BITMAPFILEHEADER)))
        {
            // accrue the number of bytes written
            dwBytesWrote += dwWritten;
            
            // write the DIB header
            write_ok = WriteFile(hFile, &pbmi->bmiHeader, sizeof(BITMAPINFOHEADER), &dwWritten, NULL);
            if (write_ok && (dwWritten == sizeof(BITMAPINFOHEADER)))
            {
                // accrue the number of bytes written
                dwBytesWrote += dwWritten;
                
                // write the color table
                if (colour_table_size != 0)
                {
                    WriteFile(hFile, pbmi->bmiColors, colour_table_size, &dwWritten, NULL);
                    // accrue the number of bytes written
                    dwBytesWrote += dwWritten;
                }
                
                // write the pixels
                if (!Addr)
                {
                    // assume a packed DIB if no pointer is supplied
                    Addr = reinterpret_cast<unsigned char*>(const_cast<RGBQUAD*>(pbmi->bmiColors) + num_colours);
                }
                WriteFile(hFile, Addr, pixels_size, &dwWritten, NULL);
                // accrue the number of bytes written
                dwBytesWrote += dwWritten;
            }
        }
        else
            dwBytesWrote = 0;

        CloseHandle(hFile);
    }

    // Free any allocated memory
    if (DestAddr)
        delete DestAddr;

    if (pbmi)
        delete pbmi;

    return GetLastError();
}

////////////////////////////////////////////////////////////////////////////////////////
// Loads a JPEG image from a file, creating a UTS image to store it in
// Parameters:
//      filename is the name of the file we want to load
// Returns:
//      a valid UTS handle if successful, UTS_ERROR otherwise
////////////////////////////////////////////////////////////////////////////////////////

long CUTSFileIO::LoadJPG(LPCTSTR filename, BOOL External)
{
    long UTSHandle = UTS_ERROR, ExternalHandle = UTS_ERROR, BPP = 0;
    long P, W, H;
    void *Addr, *ExternAddr;

    JPEG_CORE_PROPERTIES jcProps;
    
    // Initialize jcProps
    IJLERR result = ijlInit(&jcProps);

    if (result == IJL_OK)
    {
#ifdef _UNICODE
		char MBCSFileName[_MAX_PATH];

		WideCharToMultiByte(CP_ACP, 0, filename, -1, MBCSFileName, sizeof(MBCSFileName), NULL, NULL);
        // Specify input filename
        jcProps.JPGFile = MBCSFileName;
#else
		jcProps.JPGFile = filename;
#endif

        // Read JPEG parameters from the file
        result = ijlRead(&jcProps, IJL_JFILE_READPARAMS);
        if (result != IJL_OK)
            return UTS_ERROR;
        
        // Initialize a corresponding UTS image
        switch (jcProps.JPGChannels)
        {
        case 1: 
            UTSHandle = pUTSMemRect->Create(EIGHT_BITS_PER_PIXEL, jcProps.JPGWidth, jcProps.JPGHeight); 
            jcProps.DIBColor = IJL_G; 
            BPP = EIGHT_BITS_PER_PIXEL;
            break;
        case 3: 
            UTSHandle = pUTSMemRect->Create(TWENTYFOUR_BITS_PER_PIXEL, jcProps.JPGWidth, jcProps.JPGHeight); 
            jcProps.DIBColor = IJL_BGR;
            BPP = TWENTYFOUR_BITS_PER_PIXEL;
            break;
        default: 
            return UTS_ERROR;
        }
        
        if (UTSVALID_HANDLE(UTSHandle))
        { 
            pUTSMemRect->GetExtendedInfo(UTSHandle, &W, &H, &P, &Addr);
            
            // Set up the image info for the JPEG decoder
            jcProps.DIBWidth = W;
            jcProps.DIBHeight = -H;      
            jcProps.DIBChannels = jcProps.JPGChannels;
            
            jcProps.DIBPadBytes = P - (jcProps.DIBChannels * W);
            // Tell IJL where to store the image
            jcProps.DIBBytes = (unsigned char *) Addr;
            
            // Read data from the JPEG image into the UTS image
            result = ijlRead(&jcProps, IJL_JFILE_READWHOLEIMAGE);
            if (result != IJL_OK)
            {
                // Image read not successful
                pUTSMemRect->FreeImage(UTSHandle);
                return UTS_ERROR;
            }
            else
            {
                // Convert the memory rectangle to an external one if required
                if (External)
                {
                    // Allocate external memory
                    ExternAddr = malloc(H * P);
                    if (ExternAddr)
                        ExternalHandle = pUTSMemRect->MakeExtern(ExternAddr, BPP, W, H, 0);
                    // Move data to external memory and free the original UTS handle
                    if (ExternalHandle)
                    {
                        MoveMemory(ExternAddr, Addr, H * P);                // Copy the memory
                        pUTSMemRect->FreeImage(UTSHandle);                         // Free the original UTS image
                        UTSHandle = ExternalHandle;                         // Use the external handle instead
                    }
                }
            }
        }
        // Clean up
        ijlFree(&jcProps);
    }
    
    return UTSHandle;
}

////////////////////////////////////////////////////////////////////////////////////////
// Saves a UTS image to a JPEG file
// Parameters:
//      filename is the name of the file to save
//      UTSHandle is the handle of the image we want to save to that file
//      Quality is the compression level 1-100, 100 is the highest compression level
// Returns:
//      TRUE if o.k.
// Notes:
//      8 bit greyscale and 24 bit colour support only
////////////////////////////////////////////////////////////////////////////////////////

BOOL CUTSFileIO::SaveJPG(LPCTSTR filename, long UTSHandle, int Quality)
{
    long            W, H, BPP, P;
    void            *ImAddr;
    JPEG_CORE_PROPERTIES jcProps;
    
    // Initialize jcProps
    IJLERR result = ijlInit(&jcProps);
    if (result != IJL_OK)
        return FALSE;
    
    // Get the image info
    BPP = pUTSMemRect->GetExtendedInfo(UTSHandle, &W, &H, &P, &ImAddr);
    
    if (BPP == ONE_BIT_PER_PIXEL || !UTSVALID_HANDLE(UTSHandle))
    {
        // clean up
        ijlFree(&jcProps);
        return FALSE;
    }
        
    // Set up the DIB info for the JPEG encoder
    jcProps.DIBWidth = W;
    jcProps.DIBHeight = -H;      
    jcProps.DIBChannels = BPP / 8;
    switch (BPP)
    {
    case EIGHT_BITS_PER_PIXEL: 
        jcProps.DIBColor = IJL_G; 
        jcProps.JPGColor = jcProps.DIBColor;
        jcProps.JPGSubsampling = IJL_NONE;
        break;
    case TWENTYFOUR_BITS_PER_PIXEL: 
        jcProps.DIBColor = IJL_BGR; 
        jcProps.JPGColor = IJL_YCBCR;
        break;
    default:           // clean up
        ijlFree(&jcProps);
        return FALSE;
    }
    
    jcProps.DIBPadBytes = P - (jcProps.DIBChannels * W);
    
    // Assign a pointer to the source pixels
    jcProps.DIBBytes = reinterpret_cast<unsigned char*>(ImAddr);
    
    // Initialize the desired JPEG properties
    jcProps.JPGWidth = W;
    jcProps.JPGHeight = H;   
        
    jcProps.JPGChannels = jcProps.DIBChannels;
    
    // Set the compression quality
    jcProps.jquality = Quality;  // percentage [0, 100];
    
#ifdef _UNICODE
    // Specify the destination filename
	char MBCSFileName[_MAX_PATH];

	WideCharToMultiByte(CP_ACP, 0, filename, -1, MBCSFileName, sizeof(MBCSFileName), NULL, NULL);
    
	jcProps.JPGFile = MBCSFileName;
#else
	jcProps.JPGFile = filename;
#endif

    // Write the entire JPEG image to the file
    result = ijlWrite(&jcProps, IJL_JFILE_WRITEWHOLEIMAGE);
    if (result != IJL_OK)
    {
        // clean up
        ijlFree(&jcProps);
        return FALSE;
    }
    
    // Clean up
    ijlFree(&jcProps);
    
    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////
// Load RAW data, BPP is either 8, 16 or 32
////////////////////////////////////////////////////////////////////////////////////////

long CUTSFileIO::LoadRAW(LPCTSTR filename, long W, long H, long BPP)
{
    HANDLE      hFile;
    DWORD       dwRead;   
    long        UTSHandle = UTS_ERROR;
    void        *ExternAddr;
    DWORD		BytesToRead;

    // open the file for shared reading
    hFile = CreateFile(filename, 
						GENERIC_READ, 
						FILE_SHARE_READ, 
						NULL,
						OPEN_EXISTING, 
						FILE_ATTRIBUTE_NORMAL, 
						NULL);

	BytesToRead = (W * H * BPP / 8);

	if (hFile != INVALID_HANDLE_VALUE)
    {
		ExternAddr = new BYTE[BytesToRead];

        // read the data
		if (ReadFile(hFile, ExternAddr, BytesToRead, &dwRead, NULL))
		{
			if (BytesToRead == dwRead)
				UTSHandle = pUTSMemRect->MakeExtern(ExternAddr, BPP, W, H, 0);
			else
				delete ExternAddr;
		}
		else
			delete ExternAddr;
	}

	return UTSHandle;
}

BOOL CUTSFileIO::SaveRAW( LPCTSTR filename, long UTSHandle)
{
	BOOL ret = FALSE;
	
	long  W, H, BPP, P;
    void  *pImage;
    BPP = pUTSMemRect->GetExtendedInfo(UTSHandle, &W, &H, &P, &pImage);

	FILE	*fp;
	if (fp = _tfopen(filename,"wb"))
	{
		if ((int) (fwrite( pImage, P, H, fp)) != H)
		{
			OutputDebugString("CUTSFileIO::SaveRAW(): bad write\n");
		}
		fclose(fp);

		ret = TRUE;
	}

	return ret;
}

////////////////////////////////////////////////////////////////////////////////////////
// GDIPlust stuff
////////////////////////////////////////////////////////////////////////////////////////

static int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
   UINT  num = 0;          // number of image encoders
   UINT  size = 0;         // size of the image encoder array in bytes

   ImageCodecInfo* pImageCodecInfo = NULL;

   GetImageEncodersSize(&num, &size);
   if(size == 0)
      return -1;  // Failure

   pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
   if(pImageCodecInfo == NULL)
      return -1;  // Failure

   GetImageEncoders(num, size, pImageCodecInfo);

   for(UINT j = 0; j < num; ++j)
   {
      if( wcscmp(pImageCodecInfo[j].MimeType, format) == 0 )
      {
         *pClsid = pImageCodecInfo[j].Clsid;
         free(pImageCodecInfo);
         return j;  // Success
      }    
   }

   free(pImageCodecInfo);

   return -1;  // Failure
}

static BOOL GetGDIplusEncoderForFilename(WCHAR wszFileName[], CLSID *pEncoderID)
{
// GdiplusStartup() must have been called before this function is called.
	BOOL status = FALSE;

	if (wszFileName && pEncoderID)
	{
		WCHAR *pFilenameExtension = wcsrchr(wszFileName, '.');
		if (pFilenameExtension)
		{
			// GetImageEncoders returns an array of ImageCodecInfo objects.
			// Determine the size of that array.
			UINT num, size;
			Gdiplus::GetImageEncodersSize(&num, &size);

			// Allocate a buffer large enough to hold the array
			// returned by GetImageEncoders.
			Gdiplus::ImageCodecInfo *pEncoderInfo = (Gdiplus::ImageCodecInfo*)malloc(size);
			if (pEncoderInfo)
			{
				if (Gdiplus::GetImageEncoders(num, size, pEncoderInfo) == Gdiplus::Ok)
				{
					for (UINT i = 0; i < num; i++)
					{
						if (wcsstr(_wcslwr((wchar_t*)pEncoderInfo[i].FilenameExtension),
						           _wcslwr(pFilenameExtension)) != NULL)
						{
							*pEncoderID = pEncoderInfo[i].Clsid;
							status = TRUE;
							break;
						}
					}
				}

				free(pEncoderInfo);
			}
		}
	}

	return status;
}

////////////////////////////////////////////////////////////////////////////////////////
// Save as any format supported by Gdiplus (e.g. PNG etc).
// Format is based on filename extension.
// Will not work with 16bpp greyscale, as this is not supported by Gdiplus
// (Gdiplus::PixelFormat16bppGrayScale doesn't work!).
////////////////////////////////////////////////////////////////////////////////////////

BOOL CUTSFileIO::SaveAs(LPCTSTR filename, long UTSHandle)
{
	long width, height, pitch;
	void *pAddr;

	long bpp = pUTSMemRect->GetExtendedInfo(UTSHandle, &width, &height, &pitch, &pAddr);

	if (bpp != UTS_ERROR)
	{
		CDibInfo dib;
		BITMAPINFO *pBitmapInfo = dib.GetBitmapInfo(bpp, width, height);

		Gdiplus::GdiplusStartupInput gdiplusStartupInput;
		ULONG_PTR gdiplusToken;

		if (Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL) == Gdiplus::Ok)
		{
			Gdiplus::Bitmap *pBitmap = Gdiplus::Bitmap::FromBITMAPINFO(pBitmapInfo, pAddr);

			if (pBitmap)
			{
				WCHAR wszFileName[MAX_PATH];

			#ifndef _UNICODE
				if (MultiByteToWideChar(CP_ACP, 0, filename, _tcslen(filename)+1,
										wszFileName,
										sizeof(wszFileName)/sizeof(wszFileName[0])) != 0)
				{
			#else
					_tcscpy(wszFileName, filename);
			#endif

					CLSID encoder;
					//Gdiplus::EncoderParameters encoderParams;
					// I am assuming that passing NULL to Gdiplus::Bitmap::Save()
					// for the encoder parameters will cause it to use sensible defaults.
					if (!GetGDIplusEncoderForFilename(wszFileName, &encoder) || 
						pBitmap->Save(wszFileName, &encoder, NULL) != Gdiplus::Ok)
					{
						bpp = 0; // Failure
					}
			#ifndef _UNICODE
				}
			#endif                        
			}

			delete pBitmap;

			Gdiplus::GdiplusShutdown(gdiplusToken);
		}
	}

	return (bpp > 0); // Hopefully 0
}

////////////////////////////////////////////////////////////////////////////////////////
// Load and save JPEG LS images
////////////////////////////////////////////////////////////////////////////////////////

long CUTSFileIO::LoadJPGLS(LPCTSTR filename, BOOL External)
{
	Compressor Comp;
	int Width, Height, BitsPerPix, Bits;
	void *Data;
	long UTSHandle = UTS_ERROR;

	std::vector<unsigned char> RawData;
		
	if (Comp.DecompressAndLoad(filename, RawData, Width, Height, BitsPerPix) == true)
	{
		if (BitsPerPix > 8)
		{
			// Data is likely to be 10 or 12 bpp, so scale it to 16 bpp.
			Bits = 16;

			// First need to convert the data from the Compressor into a DWORD array.
			WORD *D = new WORD[Width * Height];
			
			int j = 0;
			for (int i = 0; i < Width * Height; i++)
			{
				D[i] = RawData[j] + (WORD(RawData[j + 1]) << 8);
				j += 2;
			}

			// Now use the same function that CytoVision's find thread uses, when preparing an image
			// to send to an analysis script that requires 16 bpp images, to scale the bit depth.
			BYTE * B = new BYTE[ ImageBits::GetImageSizeInBytes(Width, Height, Bits) ];

			ImageBits::ScaleToNBPP(D, Width, Height, BitsPerPix, B, Bits);

			delete[] D;

			Data = B;
		}
		else
		{
			Bits = 8;
			Data = new BYTE[Width * Height];
			BYTE *B = (BYTE *)Data;
			for (int i = 0; i < Width * Height; i++)
			{
				*B++ = RawData[i];
			}
		}

		UTSHandle = pUTSMemRect->Create(Bits, Width, Height);

		long W, H, Pitch;
		void *Addr;

		// Allocate a UTS handle
		if (UTSVALID_HANDLE(UTSHandle))
		{ 
			pUTSMemRect->GetExtendedInfo(UTSHandle, &W, &H, &Pitch, &Addr);

			int WW = ((BitsPerPix + 7) / 8);

			// Only pad the image if it is not aligned i.e. width and pitch differ
			if (WW * Width != Pitch)
			{
				if (Bits == 8)
				{
					int NBytesW = Width;	// Number of bytes in a row
					BYTE *SrcAddr = (BYTE *)Data + (Height - 1) * NBytesW;	// Src address to copy from
					BYTE *DestAddr = (BYTE *)Addr + (Height - 1) * Pitch;	    // Dest address to copy to

					for (int y = 0; y < Height; y++)
					{
						memmove(DestAddr, SrcAddr, NBytesW);
						DestAddr -= Pitch;
						SrcAddr -= NBytesW;
					}
				}
				else
				{
					int NBytesW = Width * 2;	// Number of bytes in a row
					BYTE *SrcAddr = (BYTE *)Data + (Height - 1) * NBytesW;	// Src address to copy from
					BYTE *DestAddr = (BYTE *)Addr + (Height - 1) * Pitch;	    // Dest address to copy to

					for (int y = 0; y < Height; y++)
					{
						memmove(DestAddr, SrcAddr, NBytesW);
						DestAddr -= Pitch;
						SrcAddr -= NBytesW;
					}
				}
			}
			else
				memcpy(Addr, Data, Height * Width * WW);

			delete [] Data;

			 // Convert the memory rectangle to an external one if required
			if (External)
			{
				// Allocate external memory
				void *ExternAddr = malloc(H * Pitch);

				long ExternalHandle = pUTSMemRect->MakeExtern(ExternAddr, Bits, W, H, 0);
				// Move data to external memory and free the original UTS handle
				if (ExternalHandle)
				{
					MoveMemory(ExternAddr, Addr, H * Pitch);                // Copy the memory
					pUTSMemRect->FreeImage(UTSHandle);                         // Free the original UTS image
					UTSHandle = ExternalHandle;                         // Use the external handle instead
				}
			}
		}
	}

	return UTSHandle;
}


long CUTSFileIO::LoadJPGLSAs8Bit(LPCTSTR filename, BOOL External)
{
	Compressor Comp;
	int Width, Height, BitsPerPix, Bits;
	void *Data;
	long UTSHandle = UTS_ERROR;

	std::vector<unsigned char> RawData;
		
	if (Comp.DecompressAndLoad(filename, RawData, Width, Height, BitsPerPix) == true)
	{
		if (BitsPerPix > 8)
		{
			// Squash n bits into 8 bits using the histogram.
			Bits = 8;

			// First need to convert the data from the Compressor into a DWORD array.
			WORD *D = new WORD[Width * Height];
			
			int j = 0;
			for (int i = 0; i < Width * Height; i++)
			{
				D[i] = RawData[j] + (WORD(RawData[j + 1]) << 8);
				j += 2;
			}

			// Now use the same function that CytoVision's find thread uses, when preparing an image
			// to send to an analysis script that requires 8 bpp images, to squash the bit depth.
			BYTE * B = new BYTE[ ImageBits::GetImageSizeInBytes(Width, Height, Bits) ];

			ImageBits::HistogramStretchToNBPP(D, Width, Height, BitsPerPix, B, Bits);

			delete[] D;

			Data = B;
		}
		else
		{
			Bits = 8;
			Data = new BYTE[Width * Height];
			BYTE *B = (BYTE *)Data;
			for (int i = 0; i < Width * Height; i++)
			{
				*B++ = RawData[i];
			}
		}

		UTSHandle = pUTSMemRect->Create(Bits, Width, Height);

		long W, H, Pitch;
		void *Addr;

		// Allocate a UTS handle
		if (UTSVALID_HANDLE(UTSHandle))
		{ 
			pUTSMemRect->GetExtendedInfo(UTSHandle, &W, &H, &Pitch, &Addr);

			// Only pad the image if it is not aligned i.e. width and pitch differ
			if (Width != Pitch)
			{
				// Bits == 8 always in this case
				int NBytesW = Width;	// Number of bytes in a row
				BYTE *SrcAddr = (BYTE *)Data + (Height - 1) * NBytesW;	// Src address to copy from
				BYTE *DestAddr = (BYTE *)Addr + (Height - 1) * Pitch;	    // Dest address to copy to

				for (int y = 0; y < Height; y++)
				{
					memmove(DestAddr, SrcAddr, NBytesW);
					DestAddr -= Pitch;
					SrcAddr -= NBytesW;
				}
			}
			else
				memcpy(Addr, Data, Height * Width);

			delete [] Data;

			 // Convert the memory rectangle to an external one if required
			if (External)
			{
				// Allocate external memory
				void *ExternAddr = malloc(H * Pitch);

				long ExternalHandle = pUTSMemRect->MakeExtern(ExternAddr, Bits, W, H, 0);
				// Move data to external memory and free the original UTS handle
				if (ExternalHandle)
				{
					MoveMemory(ExternAddr, Addr, H * Pitch);                // Copy the memory
					pUTSMemRect->FreeImage(UTSHandle);                         // Free the original UTS image
					UTSHandle = ExternalHandle;                         // Use the external handle instead
				}
			}
		}
	}

	return UTSHandle;
}


BOOL CUTSFileIO::SaveJPGLS(LPCTSTR filename, long UTSHandle)
{
	if (UTSVALID_HANDLE(UTSHandle))
	{
		long W, H, BPP, Pitch;
		void *Addr;

		BPP = pUTSMemRect->GetExtendedInfo(UTSHandle, &W, &H, &Pitch, &Addr);

		Compressor CompressIt;
		CompressIt.CompressAndSave(filename, Addr, W, H, BPP); 

		return TRUE;
	}

	return FALSE;
}
