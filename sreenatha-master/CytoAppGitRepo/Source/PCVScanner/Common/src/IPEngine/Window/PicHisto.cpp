// PicWindow.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// Associates a display window with an image in the IPEngine 
// Allows profiling, zooming etc to be carried out interactively
// with the windowed image. Useful for debug and testing purposes.
//
// Written By Karl Ratcliff 10042001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include <math.h>
#include "resource.h"
#include "picman.h"
#include "PicWindow.h"
#include "pichisto.h"
#include "vcmemrect.h"
#include "vcblob.h"
#include "vcgeneric.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "woolzif.h"
#include "vcthresh.h"
#include "vcImProc.h"
#include "uts.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CPicHistogram Constructor/Destructor
/////////////////////////////////////////////////////////////////////////////

CPicHistogram::CPicHistogram(GraphType Type, CUTSMemRect *pMR)
{
    // Safe defaults
    m_nHistoX = 0;
    m_nHistoY = 0;
    m_Type = Type;
	pUTSMemRect = pMR;
	pUTSDraw = new CUTSDraw(pMR);

}

CPicHistogram::~CPicHistogram()
{
	delete pUTSDraw;
}

/////////////////////////////////////////////////////////////////////////////
// Message Maps
/////////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(CPicHistogram, CWnd)
	//{{AFX_MSG_MAP(CPicHistogram)
	ON_COMMAND(IDM_ZOOM1TO1, OnZoom1to1)
	ON_COMMAND(IDM_ZOOM1TO2, OnZoom1to2)
	ON_COMMAND(IDM_ZOOM1TO4, OnZoom1to4)
	ON_COMMAND(IDM_ZOOM2TO1, OnZoom2to1)
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_MOUSEMOVE()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Create a window and assign an image to it
/////////////////////////////////////////////////////////////////////////////

void CPicHistogram::AssignImage(long X, long Y, LPCTSTR pName, long ImageHandle, int ZoomLevel, CPicManager *pPM)
{
    RECT rcW;

    // Keep a local copy of the image handle
    m_hImageHandle = ImageHandle;
    // Keep hold of this windows PicManager
    m_pPicManager = pPM;
    
    // Default size for histograms
    switch (m_Type)
    {
    case graphHistogram:
        m_nImageW = DEFAULT_WH;
        m_nImageH = DEFAULT_WH;
        break;
    case graphProfile:
        m_nImageW = DEFAULT_WH * 2;
        m_nImageH = DEFAULT_WH;
    }

    // Get histogram extremes
    pUTSDraw->GetHistogramExtremes(ImageHandle, &m_nHistoX, &m_nHistoY);
    rcW.top     = Y;
    rcW.left    = X;
    rcW.right   = m_nImageW;
    rcW.bottom  = m_nImageH;
    
    m_pMenu = new CMenu;
    
    // Create the overlapped window
    CreateEx(WS_EX_CONTROLPARENT,
        AfxRegisterWndClass(CS_VREDRAW | CS_HREDRAW, 
        LoadCursor(NULL, IDC_CROSS), 
        (HBRUSH) GetStockObject(WHITE_BRUSH), 0),
        pName, 
        0, 
        rcW, 
        NULL,
        0, 
        NULL);
    
    // Load the menu from the resources
    m_pMenu->LoadMenu(MAKEINTRESOURCE(IDR_HISTMENU));
    
    // Set the window menu up
    SetMenu(m_pMenu);
    // Keep a copy of the window title
    *m_pImageName = pName;
    
    // Resize the image accordingly
    Resize(1, ZoomLevel);
    CWnd::ShowWindow(SW_NORMAL);
}


/////////////////////////////////////////////////////////////////////////////
// Convert window coordinates into histogram measurements
/////////////////////////////////////////////////////////////////////////////

void CPicHistogram::Convert(POINT *p, LPRECT pRect)
{
    long    Xe, Ye, Pi;
    DWORD   *haddr;

    p->x -= pRect->left;
    
    pUTSMemRect->GetExtendedInfo(m_hImageHandle, &Xe, &Ye, &Pi, &haddr);
    // Convert to window pixel size
    p->x = (p->x * m_nHistoX) / (pRect->right - pRect->left);
    
    // Get histogram entry for the converted mouse X (histogram index)
    if (p->x >= 0 && p->x < Xe)
        p->y = *(haddr + p->x);
    else
        p->y = 0;
}

/////////////////////////////////////////////////////////////////////////////
// CPicHistogram message handlers


// Zoom to original size image
void CPicHistogram::OnZoom1to1() 
{
	Resize(1, 1);
}

// Zoom to half size image
void CPicHistogram::OnZoom1to2() 
{
    Resize(1, 2);	
}

// Zoom to quarter size image
void CPicHistogram::OnZoom1to4() 
{
    Resize(1, 3);	
}

// Zoom to double size image
void CPicHistogram::OnZoom2to1() 
{
    Resize(2, 1);	
}
 
void CPicHistogram::SetProfilePoints(long sx, long sy, long ex, long ey)
{
    m_StartX = sx;
    m_StartY = sy;
    m_EndX = ex;
    m_EndY = ey;
}

void CPicHistogram::OnPaint() 
{
    CPaintDC dc(this);      // DC to draw on
    
    switch (m_Type)
    {
    case graphHistogram:
        pUTSDraw->UTSHistogramWndDisplay(m_hWnd, m_hImageHandle);
        break;
    case graphProfile:
        pUTSDraw->DrawProfile(m_hWnd, m_hImageHandle, m_StartX, m_StartY, m_EndX, m_EndY);
        break;
    }
}

BOOL CPicHistogram::PreCreateWindow(CREATESTRUCT& cs) 
{
	// Override any assigned window styles
	cs.style = WS_OVERLAPPEDWINDOW;
    
	return CWnd::PreCreateWindow(cs);
}

void CPicHistogram::OnSize(UINT nType, int cx, int cy) 
{
    CWnd::OnSize(nType, cx, cy);
    
    // Force a repaint
    Refresh();
}

void CPicHistogram::OnMouseMove(UINT nFlags, CPoint point) 
{
    if (m_Type == graphHistogram)
    {
        // Output start and end in title bar
        CString CoordsPos;
        
        RECT HRect;
        
        // Get the area taken up by the histogram
        pUTSDraw->GetHistogramRect(this->m_hWnd, &HRect);
        
        // Output current position in title bar        
        if (PtInRect(&HRect, point))
        {
            Convert(&point, &HRect);
            CoordsPos.Format(_T("H(%d) = %d"), point.x, point.y);
            SetWindowText(CoordsPos);
        }
        else
            SetWindowText(*m_pImageName);
    }

    // Default mouse handler
    CWnd::OnMouseMove(nFlags, point);
}
