// PicWindow.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// 
// Window handling for the display of UTS info in interactive mode
//
// Written by Karl Ratcliff 11042001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#ifndef __PICHISTO_H
#define __PICHISTO_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "stdafx.h"

/////////////////////////////////////////////////////////////////////////////
// Typedefs
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CPicHistogram window
// Displays an image window with associated UTS image handle.
// Also deals with one dimensional histograms
/////////////////////////////////////////////////////////////////////////////

class CPicHistogram : public CPicWindow
{

    // Construction
public:
    typedef enum {
        graphHistogram,
        graphProfile
    } GraphType;
   
    CPicHistogram(GraphType Type, CUTSMemRect *pMR);
	~CPicHistogram();
    void AssignImage(long X, long Y, LPCTSTR pName, long ImageHandle, int ZoomLevel, CPicManager *pPM);      // Assign a name and UTS image handle to the window
    void SetProfilePoints(long sx, long sy, long ex, long ey);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPicHistogram)
	public:
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

protected:

    // Member functions not accessible externally
private:
    void Convert(POINT *p, LPRECT pRect);           // Convert mouse coordinates into histogram ones
    GraphType m_Type;                               // Type of data to display
    // Member variables
    long    m_nHistoX, m_nHistoY;                   // Extremes in a histogram
    long    m_StartX, m_StartY, m_EndX, m_EndY;     // Coordinates for the profile
	CUTSMemRect *pUTSMemRect;
	CUTSDraw    *pUTSDraw;


    // Generated message map functions
protected:
	//{{AFX_MSG(CPicHistogram)
	afx_msg void OnZoom1to1();
	afx_msg void OnZoom1to2();
	afx_msg void OnZoom1to4();
	afx_msg void OnZoom2to1();
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PICWINDOW_H__7559134C_2D16_11D5_9E30_DADA5ABBD751__INCLUDED_)
