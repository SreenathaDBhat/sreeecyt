// PicWindow.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// Associates a display window with an image in the IPEngine 
// Allows profiling, zooming etc to be carried out interactively
// with the windowed image. Useful for debug and testing purposes.
//
// Written By Karl Ratcliff 10042001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include <math.h>
#include "resource.h"
#include "vcmemrect.h"
#include "vcblob.h"
#include "vcgeneric.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "woolzif.h"
#include "vcthresh.h"
#include "vcImProc.h"
#include "uts.h"
#include "picman.h"
#include "PicWindow.h"
#include "picimage.h"

#include "OSVersion.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CString         csClassName;


/////////////////////////////////////////////////////////////////////////////
// CPicImage Constructor/Destructor
/////////////////////////////////////////////////////////////////////////////

CPicImage::CPicImage(CUTSMemRect *pMR)
{
    // Safe defaults
    m_bGreyLevels = FALSE;
    m_nBPP = 0;
	m_nImageW = 0;
	m_nImageH = 0;
	m_pImageAddr = NULL;

    // Profiling OFF
    m_StartPoint.x = -1;
    m_StartPoint.y = -1;
    m_EndPoint.x = -1;
    m_EndPoint.y = -1;

	pUTSMemRect = pMR;
	pUTSDraw = new CUTSDraw(pMR);
}

CPicImage::~CPicImage()
{
	delete pUTSDraw;
}

/////////////////////////////////////////////////////////////////////////////
// Message Maps
/////////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(CPicImage, CWnd)
	//{{AFX_MSG_MAP(CPicImage)
	ON_COMMAND(IDM_ZOOM2TO1, OnZoom2to1)
	ON_COMMAND(IDM_ZOOM1TO1, OnZoom1to1)
	ON_COMMAND(IDM_ZOOM1TO2, OnZoom1to2)
	ON_COMMAND(IDM_ZOOM1TO4, OnZoom1to4)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(IDM_PROFILE, OnProfile)
	ON_COMMAND(IDM_DISTANCE, OnDistance)
	ON_COMMAND(IDR_GREYLEVELS, OnGreylevels)
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_COMMAND(IDM_SAVEBMP, OnSavebmp)
	ON_COMMAND(IDM_SAVEJPG, OnSavejpg)
	ON_COMMAND(ID_INVERTCOLOURS, OnInvertcolours)
	ON_COMMAND(ID_PSEUDOCOLOUR, OnPseudocolour)
	ON_UPDATE_COMMAND_UI(ID_PSEUDOCOLOUR, OnUpdatePseudocolour)
	ON_UPDATE_COMMAND_UI(ID_INVERTCOLOURS, OnUpdateInvertcolours)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Create a window and assign an image to it
/////////////////////////////////////////////////////////////////////////////

void CPicImage::AssignImage(long X, long Y, LPCTSTR pName, long ImageHandle, int ZoomLevel, CPicManager *pPM)
{
	long Pitch;

    // Keep a local copy of the image handle
    m_hImageHandle = ImageHandle;
	// Give it a type
    m_Type = CPicWindow::wndImage;
    // Keep hold of this windows PicManager
    m_pPicManager = pPM;

    // Get the actual image dimensions
    m_nBPP = pUTSMemRect->GetExtendedInfo(m_hImageHandle, &m_nImageW, &m_nImageH, &Pitch, &m_pImageAddr);
    
    RECT rcW;

    // Coordinates of the window
    rcW.top     = Y;
    rcW.left    = X;
    rcW.right   = m_nImageW;
    rcW.bottom  = m_nImageH;

    m_pMenu = new CMenu;
    
    // Create the overlapped window
    CreateEx(WS_EX_CONTROLPARENT,
        AfxRegisterWndClass(CS_VREDRAW | CS_HREDRAW, 
        LoadCursor(NULL, IDC_CROSS), 
        NULL, 0),
        pName, 
        0, 
        rcW, 
        NULL,
        0, 
        NULL);

    // Load the menu from the resources
    m_pMenu->LoadMenu(MAKEINTRESOURCE(IDR_PICMENU));

    // Set the window menu up
    SetMenu(m_pMenu);
    // Keep a copy of the window title
    *m_pImageName = pName;
    
    // Resize the image accordingly
    Resize(1, ZoomLevel);
    ShowWindow(SW_NORMAL);

    // Set scroll bar info
    SCROLLINFO SI;
    
    GetScrollInfo(SB_VERT, &SI);
    SI.cbSize = sizeof(SCROLLINFO);
    
    SI.fMask = SIF_POS | SIF_RANGE;
    SI.nMin  = 0;
    SI.nMax  = m_nImageW;
    SI.nPos  = 0;
    SetScrollInfo(SB_HORZ, &SI, FALSE);
    SI.nMax  = m_nImageH;
    SetScrollInfo(SB_VERT, &SI, FALSE);
    
}

/////////////////////////////////////////////////////////////////////////////
// Convert mouse coordinates into image ones
/////////////////////////////////////////////////////////////////////////////

void CPicImage::Convert(POINT p, POINT& cp)
{
    int WP = 0;
    long W, H;
    RECT cRect;

    GetClientRect(&cRect);
    W = RECTWIDTH(&cRect);
    H = RECTHEIGHT(&cRect);

	/*
    // Convert to image pixel coordinates
    if (W)
        cp.x = (m_nImageW * p.x) / W;
    else
        cp.x = p.x;

    if (H)
        cp.y = (m_nImageH * (H - p.y)) / H;
    else
        cp.y = H - p.y;
		*/

	
    {
		
        CPoint  offs;// = GetScrollPosition();
           offs.x = 0;
		   offs.y = 0;

        cp.x = ((p.x + offs.x) * m_nDivisor) / m_nMultiplier;
        cp.y = m_nImageH - (((p.y + offs.y) * m_nDivisor) / m_nMultiplier);
    }

}

/////////////////////////////////////////////////////////////////////////////
// Calculate the distance between two supplied window points in image
// units
/////////////////////////////////////////////////////////////////////////////

int CPicImage::Distance(POINT p1, POINT p2)
{
    int dx, dy;
    POINT p1c, p2c;
    
    Convert(p1, p1c);
    Convert(p2, p2c);

    dx = p1c.x - p2c.x;
    dy = p1c.y - p2c.y;

    if (dx != 0 || dy != 0)
        return (int) sqrt((double)(dx * dx + dy * dy));
    else
        return 0;
}

/////////////////////////////////////////////////////////////////////////////
// Start the profiling process
/////////////////////////////////////////////////////////////////////////////

void CPicImage::EnableProfiling(void)
{ 
    if (m_MouseMode == mouseDoingNothing)
    {
        m_MouseMode = mouseStartProfiling;    // Enable processing of left mouse clicks for profiling
        m_pMenu->EnableMenuItem(IDM_DISTANCE, MF_GRAYED);
    }
}

/////////////////////////////////////////////////////////////////////////////
// Start the profiling process
/////////////////////////////////////////////////////////////////////////////

void CPicImage::EnableDistanceMeasurement(void)
{ 
    if (m_MouseMode == mouseDoingNothing)
    {
        m_MouseMode = mouseStartMeasure;      // Enable processing of left mouse clicks for profiling
        m_pMenu->EnableMenuItem(IDM_PROFILE, MF_GRAYED);
    }
}

/////////////////////////////////////////////////////////////////////////////
// Start a new line drawing sequence
/////////////////////////////////////////////////////////////////////////////

void CPicImage::StartLine(POINT point)
{ 
    m_StartPoint = point;
    m_OldEnd = point;
}

/////////////////////////////////////////////////////////////////////////////
// Erase the previous line drawn
/////////////////////////////////////////////////////////////////////////////

void CPicImage::EraseOldLine(COLORREF Colour)
{
    // Erase the old line
    CClientDC dc(this);
    
    // Drawing pen
    CPen CBr(PS_SOLID, 1, Colour);
    
    // Select into DC
    CPen *pOldBr = dc.SelectObject(&CBr);
    
    // Draw in XOR mode
    int PrevROP = dc.SetROP2(R2_XORPEN);
    
    // Erase the old line - don't draw the new one
    dc.MoveTo(m_StartPoint);
    dc.LineTo(m_OldEnd);
    
    // Select old pen back again
    dc.SelectObject(pOldBr);
    // Select the old ROP
    dc.SetROP2(PrevROP);
}

/////////////////////////////////////////////////////////////////////////////
// Erase the old line and draw a new one, remembering the last line pos
/////////////////////////////////////////////////////////////////////////////

void CPicImage::EraseOldLineDrawNew(POINT NewPoint, COLORREF Colour)
{
    // Draw a line to whereever the mouse now is 
    // erasing the old one first
    CClientDC dc(this);
    
    // Drawing pen
    CPen CBr(PS_SOLID, 1, Colour);
    
    // Select into DC
    CPen *pOldBr = dc.SelectObject(&CBr);
    
    // Draw in XOR mode
    int PrevROP = dc.SetROP2(R2_XORPEN);
    
    // Erase old line first then the new one 
    dc.MoveTo(m_StartPoint);
    dc.LineTo(m_OldEnd);
    dc.MoveTo(m_StartPoint);
    dc.LineTo(NewPoint); 
    
    // Select old pen back again
    dc.SelectObject(pOldBr);
    // Select the old ROP
    dc.SetROP2(PrevROP);
    
    // Remember the end point
    m_OldEnd = NewPoint;
}


/////////////////////////////////////////////////////////////////////////////
// CPicImage message handlers

 
void CPicImage::OnPaint() 
{
	if (pUTSMemRect->HandleIsValid(m_hImageHandle))
	{
		CPaintDC DrawDC(this);      // DC to draw on
		
		HDC         MemDC = NULL;//, DrawDC = pDC->GetSafeHdc();
		HBITMAP	    hbmBMP, holdbmp = NULL;		// Handle of D-I BMP to use for blit to control	
		BYTE        *image, *destimage = NULL, *pBits;
		BITMAPINFO  *pBMP = NULL;
		int         i;
		long        nBytes;
		BYTE        *srcptr, *destptr;
		
		image = (BYTE *) m_pImageAddr;
		
		try 
		{
			// Create a memory DC for drawing on 
			MemDC = CreateCompatibleDC(DrawDC);
			if (!MemDC) throw;
			
			int B = m_nBPP;
			// Process images based on their bits per pixel
			if (m_nBPP == ONE_BIT_PER_PIXEL)
			{
				// Allocate some mem
				destimage = new BYTE[m_nImageW * m_nImageH / 8];
				// Get the bits out of the UTS image
				memcpy(destimage, image, m_nImageW * m_nImageH / 8);
				// Reverse the bit order in each byte of the image
				pUTSMemRect->InverseBytes((char *) destimage, m_nImageW * m_nImageH / 8); 
				image = destimage;
			}        
			else
			if (m_nBPP == SIXTEEN_BITS_PER_PIXEL)
			{
				B = 8;
				// Need to create an 8bpp copy of the image for display purposes.
                long h16 = pUTSMemRect->MakeExtern(m_pImageAddr, m_nBPP, m_nImageW, m_nImageH, 0);
                long h8 = pUTSMemRect->CreateAsWithPix(h16, EIGHT_BITS_PER_PIXEL);

                pUTSMemRect->Convert16To8(h16, h8);
                
                long wi8, hi8, pitch8;
				void *pAddr8;
                pUTSMemRect->GetExtendedInfo(h8, &wi8, &hi8, &pitch8, &pAddr8);
                
                nBytes = pitch8 * hi8;
                destimage = new BYTE[ nBytes ];
                pUTSMemRect->GetBits(destimage, h8);
                image = destimage;

				// Free the UTS handles only.
                pUTSMemRect->FreeImage(h16);
                pUTSMemRect->FreeImage(h8);

                if (m_bPseudo)
                    pBMP = GetDibInfo()->GetBitmapPseudoInfo(8, m_nImageW, m_nImageH);
                else
                    pBMP = GetDibInfo()->GetBitmapInfo(8, m_nImageW, m_nImageH);
			}

			if ((m_nBPP == EIGHT_BITS_PER_PIXEL || m_nBPP == SIXTEEN_BITS_PER_PIXEL) && m_bPseudo)
				pBMP = GetDibInfo()->GetBitmapPseudoInfo(B, m_nImageW, m_nImageH);
			else
				pBMP = GetDibInfo()->GetBitmapInfo(B, m_nImageW, m_nImageH);
			
			// Get out if NULL pBMP
			if (!pBMP) throw;
			
			// Create a bmp for drawing 
			hbmBMP = CreateDIBSection(DrawDC, pBMP, DIB_RGB_COLORS, (LPVOID *)(&pBits), NULL, 0);
			// Clean up
			delete pBMP;
			
			nBytes = m_nImageW * m_nImageH * B / 8;
			
			// Set the bits
			if (m_bInvert)
			{
				srcptr = image;
				destptr = pBits;
				for (i = 0; i < nBytes; i++)
				{
					*destptr++ = ~(*srcptr++);
				}
			}
			else
				memcpy(pBits, image, nBytes);
			
			// Select into the DC
			holdbmp = (HBITMAP) SelectObject(MemDC, hbmBMP);
			
			// Set the blitting mode to wipe out previous data
			SetStretchBltMode(DrawDC, COLORONCOLOR);
			
			// Stretch blit to the screen        
			if (StretchBlt(DrawDC,
				0, 
				0,
				(m_nImageW * m_nMultiplier) / m_nDivisor, 
				(m_nImageH * m_nMultiplier) / m_nDivisor,
				MemDC,
				0,
				0, 
				m_nImageW,
				m_nImageH, 
				SRCCOPY) == GDI_ERROR)
				throw;
			
		}
		
		
		catch (...)
		{
			TRACE0("GDI UTS DRAW ERROR\r\n");
		}
		
		// Release any used mem
		if (destimage)
			delete destimage;
		
		// Free any other resources used
		if (holdbmp)
			DeleteObject(SelectObject(MemDC, holdbmp));
		if (MemDC)
			DeleteDC(MemDC);
	}
	else
		DestroyWindow();
}


BOOL CPicImage::PreCreateWindow(CREATESTRUCT& cs) 
{
	// Override any assigned window styles
	cs.style = WS_OVERLAPPEDWINDOW;
    
    cs.style |= WS_VSCROLL | WS_HSCROLL;

	return CWnd::PreCreateWindow(cs);
}


void CPicImage::OnLButtonDown(UINT nFlags, CPoint point) 
{
    // Show the coords in the window caption
    CString CoordsPos;
    POINT   ip, sp, tp;

    Convert(point, ip);
    
    switch (m_MouseMode)
    {
    case mouseStartProfiling:
        // Define the start point for the profile
        // Start point defined, so switch to end point definition
        m_MouseMode = mouseProfiling;
        // Record the start point
        StartLine(point);
        
        CoordsPos.Format(_T("%s (%d, %d)"), *m_pImageName, ip.x, ip.y);
        SetWindowText(CoordsPos);
        break;
        
    case mouseProfiling:
        // Second mouse click in profiling mode
        m_MouseMode = mouseDoingNothing;
        m_pMenu->EnableMenuItem(IDM_DISTANCE, MF_ENABLED);
        Convert(m_StartPoint, sp);
        if (sp.x > ip.x)
        {
            tp = sp;
            sp = ip;
            ip = tp;
        }
        // Draw profile
        pUTSDraw->DrawProfile(m_hWnd, m_hImageHandle, sp.x, sp.y, ip.x, ip.y);
        break;
        
    case mouseStartMeasure:
        // Define the start point for the profile
        // Start point defined, so switch to end point definition
        m_MouseMode = mouseMeasuring;
        // Record the start point
        StartLine(point);
        
        CoordsPos.Format(_T("%s (%d, %d)"), *m_pImageName, ip.x, ip.y);
        SetWindowText(CoordsPos);
        break;
        
    case mouseMeasuring:
        EraseOldLine(MEASUREMENT_COLOUR);
        // Finished measuring, so wipe out the line and stop
        m_MouseMode = mouseDoingNothing;
        m_pMenu->EnableMenuItem(IDM_PROFILE, MF_ENABLED);
        break;
        
    case mouseDoingNothing:
        break;
    }
    
    // Default L Click Processing
    CWnd::OnLButtonDown(nFlags, point);
}

void CPicImage::OnMouseMove(UINT nFlags, CPoint point) 
{
    // Output start and end in title bar
    CString CoordsPos;
    POINT ip, sp;

    Convert(point, ip);
    
    switch (m_MouseMode)
    {
    case mouseProfiling:
        // Erase old line and draw one to the new point
        EraseOldLineDrawNew(point, PROFILE_COLOUR);
        Convert(m_StartPoint, sp);
        // Output coordinates of the start point and the current
        // position of the mouse
        CoordsPos.Format(_T("%s (%d, %d) to (%d, %d)"), *m_pImageName, sp.x, sp.y, ip.x, ip.y);
        SetWindowText(CoordsPos);
        break;
        
    case mouseMeasuring:
        // Erase old line and draw one to the new point
        EraseOldLineDrawNew(point, MEASUREMENT_COLOUR);
        
        // Output distance from cursor location to start point in title bar
        CoordsPos.Format(_T("%s Distance = %d"), *m_pImageName, Distance(m_StartPoint, point));
        SetWindowText(CoordsPos);
        break;
        
    case mouseDoingNothing:
        // Output current position in title bar           
        if (!m_bGreyLevels)
            CoordsPos.Format(_T("%s Pos (%d, %d)"), *m_pImageName, ip.x, ip.y);
        else
        {
            long Level = pUTSMemRect->GetPixel(m_hImageHandle, ip.x, ip.y);
            CoordsPos.Format(_T("%s Pos (%d, %d) = %6.6lX"), *m_pImageName, ip.x, ip.y, Level);
        }
        SetWindowText(CoordsPos);
        break;
    }
    
    // Default mouse handler
    CWnd::OnMouseMove(nFlags, point);
}

void CPicImage::OnRButtonDown(UINT nFlags, CPoint point) 
{
    switch (m_MouseMode)
    {
    case mouseStartProfiling:
    case mouseProfiling:
        // Current mode cancelled
        EraseOldLine(PROFILE_COLOUR);
        // Cancel
        m_MouseMode = mouseDoingNothing;
        m_pMenu->EnableMenuItem(IDM_DISTANCE, MF_ENABLED);
        break;

    case mouseStartMeasure:
    case mouseMeasuring:
        // Current mode cancelled
        EraseOldLine(MEASUREMENT_COLOUR);
        // Cancel
        m_MouseMode = mouseDoingNothing;
        m_pMenu->EnableMenuItem(IDM_PROFILE, MF_ENABLED);
        break;
        
    case mouseDoingNothing:
        break;
    }
    
    // Default R Button Handler
    CWnd::OnRButtonDown(nFlags, point);
}

// Start profiling using left mouse clicks
void CPicImage::OnProfile() 
{
    // Repaint to erase old
    Refresh();
    
    // Turn profiling operation on
    EnableProfiling();
}

void CPicImage::OnDistance() 
{
    // Turn on distance measuring mode
    EnableDistanceMeasurement();
}

void CPicImage::OnGreylevels() 
{
    // Toggle the mode
    m_bGreyLevels = !m_bGreyLevels;	

    m_pMenu->CheckMenuItem(IDR_GREYLEVELS, m_bGreyLevels ? MF_CHECKED : MF_UNCHECKED);
}

/////////////////////////////////////////////////////////////////////////////
// Menu options available to the window
/////////////////////////////////////////////////////////////////////////////

// Zoom to original size image
void CPicImage::OnZoom1to1() 
{
	Resize(1, 1);
}

// Zoom to half size image
void CPicImage::OnZoom1to2() 
{
    Resize(1, 2);	
}

// Zoom to quarter size image
void CPicImage::OnZoom1to4() 
{
    Resize(1, 3);	
}

// Zoom to double size image
void CPicImage::OnZoom2to1() 
{
    Resize(2, 1);	
}

void CPicImage::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CWnd::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CPicImage::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
}


void CPicImage::OnSavebmp() 
{
    CFileDialog FileSelect(FALSE, _T("bmp"), _T("*.bmp"), OFN_HIDEREADONLY, _T("Bitmap Files (*.bmp) | *.bmp ||"), NULL);
    CUTSFileIO UTSFileIO(pUTSMemRect);
	CString csFileName;

    if (FileSelect.DoModal() == IDOK)
    {
        csFileName = FileSelect.GetPathName();
   
        if (csFileName.Find(_T(".bmp")) != -1)
            UTSFileIO.SaveDIB(csFileName, m_hImageHandle);
	}
	
}

void CPicImage::OnSavejpg() 
{
    CFileDialog FileSelect(FALSE, _T("jpg"), _T("*.jpg"), OFN_HIDEREADONLY, _T("JPG Files (*.jpg) | *.jpg||"), NULL);
    CUTSFileIO UTSFileIO(pUTSMemRect);
	CString csFileName;

    if (FileSelect.DoModal() == IDOK)
    {
        csFileName = FileSelect.GetPathName();
   
        if (csFileName.Find(_T(".jpg")) != -1)
            UTSFileIO.SaveJPG(csFileName, m_hImageHandle, 100);
	}
}

void CPicImage::OnInvertcolours() 
{
	m_bInvert = !m_bInvert;	
	Refresh();
}

void CPicImage::OnPseudocolour() 
{
	m_bPseudo = !m_bPseudo;	
	Refresh();
}

void CPicImage::OnUpdatePseudocolour(CCmdUI* pCmdUI) 
{
    pCmdUI->SetCheck(m_bPseudo);
}

void CPicImage::OnUpdateInvertcolours(CCmdUI* pCmdUI) 
{
    pCmdUI->SetCheck(m_bInvert);
}
