// PicWindow.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// 
// Window handling for the display of UTS info in interactive mode
//
// Written by Karl Ratcliff 11042001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#ifndef __PICIMAGE_H
#define __PICIMAGE_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "stdafx.h"

class CPicManager;

/////////////////////////////////////////////////////////////////////////////
// Typedefs
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CPicImage window
// Displays an image window with associated UTS image handle.
// Also deals with one dimensional histograms
/////////////////////////////////////////////////////////////////////////////

class CPicImage : public CPicWindow
{

    // Construction
public:
    CPicImage(CUTSMemRect *pMR);
	~CPicImage();
    
    void AssignImage(long X, long Y, LPCTSTR pName, long ImageHandle, int ZoomLevel, CPicManager *pPM);      // Assign a name and UTS image handle to the window

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPicImage)
	public:
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL


protected:

    // Member functions not accessible externally
private:
    void    EnableProfiling(void);                  // Start defining a profile
    void    EnableDistanceMeasurement(void);        // Start measuring the distance between two points
    void    Convert(POINT p, POINT& cp);            // Converts window mouse coords to image pixels
    int     Distance(POINT p1, POINT p2);           // Distance in image coords between two window points
    void    StartLine(POINT point);                 // Start line drawing on an image
    void    EraseOldLine(COLORREF Colour);          // Erase a previously drawn line
    void    EraseOldLineDrawNew(POINT NewPoint, COLORREF Colour); // Same as above, but draw a new one

    // Member variables
    POINT   m_StartPoint, m_EndPoint;               // Start and end points for the profile
    POINT   m_OldEnd;                               // Old end of profile (for draw and erase)
    BOOL    m_bGreyLevels;                          // = TRUE if caption should display grey level values
    int     m_nBPP;                                 // Image resolution
	BYTE	*m_pImageAddr;							// Address of image data
	CUTSMemRect *pUTSMemRect;
	CUTSDraw	*pUTSDraw;

    // Generated message map functions
protected:
	//{{AFX_MSG(CPicImage)
	afx_msg void OnZoom2to1();
	afx_msg void OnZoom1to1();
	afx_msg void OnZoom1to2();
	afx_msg void OnZoom1to4();
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnProfile();
	afx_msg void OnDistance();
	afx_msg void OnGreylevels();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSavebmp();
	afx_msg void OnSavejpg();
	afx_msg void OnInvertcolours();
	afx_msg void OnPseudocolour();
	afx_msg void OnUpdatePseudocolour(CCmdUI* pCmdUI);
	afx_msg void OnUpdateInvertcolours(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PICWINDOW_H__7559134C_2D16_11D5_9E30_DADA5ABBD751__INCLUDED_)
