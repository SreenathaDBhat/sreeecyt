// PicMan.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
//
// Manages windows assigned to UTS images. 
// Written for the IPE
//
// Written By Karl Ratcliff 10042001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

//#include <afxtempl.h>
#include "stdafx.h"
#include "picman.h"
#include "picwindow.h"
#include "pichisto.h"
#include "picimage.h"
#include "vcmemrect.h"
#include "vcblob.h"
#include "vcgeneric.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "woolzif.h"
#include "vcthresh.h"
#include "vcImProc.h"
#include "uts.h"


/////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor

CPicManager::CPicManager(CUTSMemRect *pMR)
{
    m_nScreenPos.x = 0;     // Start at top left corner of the screen
    m_nScreenPos.y = 0;
	pUTSMemRect = pMR;

}

CPicManager::~CPicManager()
{
    DeleteAll();
}

/////////////////////////////////////////////////////////////////////////////
// Add a new display window for an image or a histogram
// Parameters:
//      UTSHandle is the UTS image to display in the window
//      Title is the title to put in the window
//      DType is the type of window e.g. wndImage or wndHistogram
// Returns:
//      True if a new window created
/////////////////////////////////////////////////////////////////////////////
    
BOOL CPicManager::AddNew(long UTSHandle, LPCTSTR Title, CPicWindow::DisplayType DType)         // Create a new window
{
    PicWindowPtr    pCPW;
    BOOL            Ok = FALSE;
    long            W, H;
    long            aW, aH;
    int             Zoom;
    
    // Check the handle is valid
    if (pUTSMemRect->HandleIsValid(UTSHandle))
    {
        // Create the new window
        switch (DType)
        {
        case CPicWindow::wndImage:
            pCPW = new CPicImage(pUTSMemRect);	
            break;
        case CPicWindow::wndHistogram:
            pCPW = new CPicHistogram(CPicHistogram::graphHistogram, pUTSMemRect);
            break;
        default:
            pCPW = NULL;
            break;
        }

        if (pCPW)
        {
            // Create a window and give it a UTS image
            switch (DType)
            {
            case CPicWindow::wndImage:
                 // Get the UTS image width and height
                pUTSMemRect->GetRectInfo(UTSHandle, &W, &H);
                Zoom = DEFAULT_WINDOWZOOM_LEVEL;
                break;
            case CPicWindow::wndProfile:
            case CPicWindow::wndHistogram:
                W = H = DEFAULT_WH;
                Zoom = DEFAULT_HISTOGRAMZOOM_LEVEL;
                break;
            }
            
            // Assign the new window its image/histogram, title etc.
            pCPW->AssignImage(m_nScreenPos.x, m_nScreenPos.y, Title, UTSHandle, Zoom, this);
            // Add it to the list
            m_WindowList.AddHead(pCPW);
            // Now update the new top,left corner for screen display
            aW = W / Zoom;
            m_nScreenPos.x += aW;
            if (m_nScreenPos.x > GetSystemMetrics(SM_CXSCREEN) - aW) 
            {
                aH = H / Zoom;
                m_nScreenPos.x = 0;
                m_nScreenPos.y += aH;
                // Restart at top,left of screen
                if (m_nScreenPos.y > GetSystemMetrics(SM_CYSCREEN) - aH)
                {
                    m_nScreenPos.x = 0;
                    m_nScreenPos.y = 0;
                }
            }
            Ok = TRUE;
        }
    }

    return Ok;
}

/////////////////////////////////////////////////////////////////////////////
// Add a new display window for an image or a histogram
// Parameters:
//      UTSHandle is the UTS image to display in the window
//      Title is the title to put in the window
//      DType is the type of window e.g. wndImage or wndHistogram
// Returns:
//      True if a new window created
/////////////////////////////////////////////////////////////////////////////
    
BOOL CPicManager::AddNew(long UTSHandle, LPCTSTR Title,  CPicWindow::DisplayType DType, long Sx, long Sy, long Ex, long Ey)         // Create a new window
{
    PicWindowPtr    pCPW;
    BOOL            Ok = FALSE;
    long            W, H;
    long            aW, aH;
    int             Zoom;
    
    if (DType != CPicWindow::wndProfile)
        return FALSE;

    // Check the handle is valid
    if (pUTSMemRect->HandleIsValid(UTSHandle))
    {
        // Create the new window
        pCPW = new CPicHistogram(CPicHistogram::graphProfile, pUTSMemRect);
        pCPW->SetProfilePoints(Sx, Sy, Ex, Ey);
        
        if (pCPW)
        {
            // Create a window and give it a UTS image
            W = H = DEFAULT_WH;
            Zoom = DEFAULT_HISTOGRAMZOOM_LEVEL;
            
            // Assign the new window its image/histogram, title etc.
            pCPW->AssignImage(m_nScreenPos.x, m_nScreenPos.y, Title, UTSHandle, Zoom, this);
            // Add it to the list
            m_WindowList.AddHead(pCPW);
            // Now update the new top,left corner for screen display
            aW = W / Zoom;
            m_nScreenPos.x += aW;
            if (m_nScreenPos.x > GetSystemMetrics(SM_CXSCREEN) - aW) 
            {
                m_nScreenPos.x = 0;
                aH = H / Zoom;
                m_nScreenPos.y += aH;
                // Restart at top,left of screen
                if (m_nScreenPos.y > GetSystemMetrics(SM_CYSCREEN) - aH)
                {
                    m_nScreenPos.x = 0;
                    m_nScreenPos.y = 0;
                }
            }
            Ok = TRUE;
        }
    }

    return Ok;
}

BOOL CPicManager::Delete(long UTSHandle)
{
    PicWindowPtr    pCPW;
    BOOL            Ok = FALSE;
    POSITION        P;

    // Search the list
    P = m_WindowList.GetHeadPosition();

    // Sequentially search for the window identified by UTSHandle
    while (P != NULL)
    {
        // Get the element
        pCPW = m_WindowList.GetAt(P);

        // Does the current element match
        if (UTSHandle == pCPW->m_hImageHandle)
        {
            delete pCPW;                // Delete the window
            m_WindowList.RemoveAt(P);   // Delete the list entry
            Ok = TRUE;
            break;                      // Quit the loop
        }
        else
            m_WindowList.GetNext(P);    // Try the next position
    }

    return Ok;
}

void CPicManager::DeleteAll(void)                 // Delete all currently open windows
{
    PicWindowPtr    PPtr;

    // Go through all members of the list and delete the windows
    while (!m_WindowList.IsEmpty())
    {
        // Get the head and remove it
        PPtr = m_WindowList.RemoveHead();
        // Delete the window
        delete PPtr;
    }
}

void CPicManager::Refresh(long UTSHandle)
{
    // Get a pointer to the window
    PicWindowPtr pCPW = Search(UTSHandle);

    // Found it cause a refresh
    if (pCPW)
        pCPW->Refresh();
}


PicWindowPtr CPicManager::Search(long UTSHandle)
{
    PicWindowPtr    PPtr;
    POSITION        P;

    // Search the list
    P = m_WindowList.GetHeadPosition();

    // Sequentially search for the window identified by UTSHandle
    while (P != NULL)
    {
        // Get the element
        PPtr = m_WindowList.GetAt(P);

        // Does the current element match
        if (UTSHandle == PPtr->m_hImageHandle)
            return PPtr;                // Found it return the entry
        else
            m_WindowList.GetNext(P);    // Try the next position
    }

    // If we get to here then not found an associated entry
    return NULL;
}


HWND CPicManager::GetHWND(long UTSHandle)                       // Get the HWND for the corresponding UTS image
{
    PicWindowPtr      PPtr;

    // Search for the window
    PPtr = Search(UTSHandle);

    if (PPtr)
        return PPtr->m_hWnd;
    else
        return NULL;
}