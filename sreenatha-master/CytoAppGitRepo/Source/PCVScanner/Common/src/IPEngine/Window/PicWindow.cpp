// PicWindow.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// Associates a display window with an image in the IPEngine 
// Allows profiling, zooming etc to be carried out interactively
// with the windowed image. Useful for debug and testing purposes.
//
// Written By Karl Ratcliff 10042001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include <math.h>
#include "resource.h"
#include "picman.h"
#include "PicWindow.h"
#include "vcmemrect.h"
#include "vcblob.h"
#include "vcgeneric.h"
#include "vcutsfile.h"
#include "vcutsdraw.h"
#include "woolzif.h"
#include "vcthresh.h"
#include "vcImProc.h"
#include "uts.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern HINSTANCE hAppInst;


/////////////////////////////////////////////////////////////////////////////
// CPicWindow Constructor/Destructor
/////////////////////////////////////////////////////////////////////////////

CPicWindow::CPicWindow()
{
    // Safe defaults
	m_hImageHandle  = 0;
    m_nImageW       = 0;
    m_nImageH       = 0;
    m_Type          = wndEmpty;
    m_pImageName    = new CString;
    *m_pImageName   = "";
    m_pMenu         = NULL;
    m_pPicManager   = NULL;

    // Current image scaling/zoom factor for display
    m_nMultiplier   = 1;
    m_nDivisor      = 1;

    // Current mode of operation = not doing anything with the mouse
    m_MouseMode     = mouseDoingNothing;

	// Non inverted, non pseudo colour image
	m_bPseudo		= FALSE;
	m_bInvert		= FALSE;
}

CPicWindow::~CPicWindow()
{
    if (m_pImageName)
        delete m_pImageName;
    if (m_pMenu)
        delete m_pMenu;
}

/////////////////////////////////////////////////////////////////////////////
// Message Maps
/////////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(CPicWindow, CWnd)
	//{{AFX_MSG_MAP(CPicWindow)
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



/////////////////////////////////////////////////////////////////////////////
// Resize the window
//
/////////////////////////////////////////////////////////////////////////////

void CPicWindow::Resize(int Multiplier, int Divisor)
{
    RECT rcR, rcW;

    // Keep hold of new scale
    m_nMultiplier = Multiplier;
    m_nDivisor    = Divisor;

    // Get the client window size
    GetClientRect(&rcR);
    // Get the window size
    GetWindowRect(&rcW);

    // Subtract client area
    rcW.right  -= rcR.right + rcR.left;
    rcW.bottom -= rcR.bottom + rcR.top;

    // Resize it according to the Mag scale factor
    rcR.bottom = rcR.top  + (m_nImageH * Multiplier) / Divisor;
    rcR.right  = rcR.left + (m_nImageW * Multiplier) / Divisor;

    // Adjust with window size, based on the new client area
    rcW.right  += rcR.right - rcR.left;
    rcW.bottom += rcR.bottom - rcR.top;

    // Resize the window
    MoveWindow(&rcW, TRUE);

    // Repaint
    InvalidateRect(&rcR, TRUE);
}

void CPicWindow::Refresh()
{
    RECT rcR;

    GetClientRect(&rcR);
    InvalidateRect(&rcR, TRUE);
}

void CPicWindow::OnSize(UINT nType, int cx, int cy) 
{
    CWnd::OnSize(nType, cx, cy);
    
    // Force a repaint
    Refresh();
}

void CPicWindow::OnClose() 
{
	CWnd::OnClose();
    // Delete the picture out of the window list
    // of this windows pic manager
    if (m_pPicManager)
        m_pPicManager->Delete(m_hImageHandle);	
}

void CPicWindow::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CWnd::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CPicWindow::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{	
	CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
}


