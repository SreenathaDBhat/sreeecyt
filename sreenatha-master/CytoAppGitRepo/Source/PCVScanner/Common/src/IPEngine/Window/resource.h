//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by piccmd.rc
//
#define IDR_PICMENU                     2000
#define IDR_HISTMENU                    2001
#define IDM_ZOOM1TO1                    32771
#define IDM_ZOOM1TO2                    32772
#define IDM_ZOOM1TO4                    32773
#define ID_ZOOM_ZOOM21                  32774
#define IDM_ZOOM2TO1                    32775
#define ID_DISPLAY_ZOOM                 32776
#define IDM_PROFILE                     32777
#define IDM_DISTANCE                    32778
#define IDR_GREYLEVELS                  32779
#define IDM_SAVEBMP                     32780
#define IDM_SAVEJPG                     32782
#define ID_PSEUDOCOLOUR                 32783
#define ID_INVERTCOLOURS                32784

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        2015
#define _APS_NEXT_COMMAND_VALUE         32785
#define _APS_NEXT_CONTROL_VALUE         2000
#define _APS_NEXT_SYMED_VALUE           2000
#endif
#endif
