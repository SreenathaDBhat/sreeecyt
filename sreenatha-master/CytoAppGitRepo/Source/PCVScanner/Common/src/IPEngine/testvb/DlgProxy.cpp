// DlgProxy.cpp : implementation file
//

#include "stdafx.h"
#include "testvb.h"
#include "DlgProxy.h"
#include "testvbDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestvbDlgAutoProxy

IMPLEMENT_DYNCREATE(CTestvbDlgAutoProxy, CCmdTarget)

CTestvbDlgAutoProxy::CTestvbDlgAutoProxy()
{
	EnableAutomation();
	
	// To keep the application running as long as an automation 
	//	object is active, the constructor calls AfxOleLockApp.
	AfxOleLockApp();

	// Get access to the dialog through the application's
	//  main window pointer.  Set the proxy's internal pointer
	//  to point to the dialog, and set the dialog's back pointer to
	//  this proxy.
	ASSERT (AfxGetApp()->m_pMainWnd != NULL);
	ASSERT_VALID (AfxGetApp()->m_pMainWnd);
	ASSERT_KINDOF(CTestvbDlg, AfxGetApp()->m_pMainWnd);
	m_pDialog = (CTestvbDlg*) AfxGetApp()->m_pMainWnd;
	m_pDialog->m_pAutoProxy = this;
}

CTestvbDlgAutoProxy::~CTestvbDlgAutoProxy()
{
	// To terminate the application when all objects created with
	// 	with automation, the destructor calls AfxOleUnlockApp.
	//  Among other things, this will destroy the main dialog
	if (m_pDialog != NULL)
		m_pDialog->m_pAutoProxy = NULL;
	AfxOleUnlockApp();
}

void CTestvbDlgAutoProxy::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(CTestvbDlgAutoProxy, CCmdTarget)
	//{{AFX_MSG_MAP(CTestvbDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CTestvbDlgAutoProxy, CCmdTarget)
	//{{AFX_DISPATCH_MAP(CTestvbDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_ITestvb to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {E4AC6A57-1ED3-11D6-84DA-00C04F377564}
static const IID IID_ITestvb =
{ 0xe4ac6a57, 0x1ed3, 0x11d6, { 0x84, 0xda, 0x0, 0xc0, 0x4f, 0x37, 0x75, 0x64 } };

BEGIN_INTERFACE_MAP(CTestvbDlgAutoProxy, CCmdTarget)
	INTERFACE_PART(CTestvbDlgAutoProxy, IID_ITestvb, Dispatch)
END_INTERFACE_MAP()

// The IMPLEMENT_OLECREATE2 macro is defined in StdAfx.h of this project
// {E4AC6A55-1ED3-11D6-84DA-00C04F377564}
IMPLEMENT_OLECREATE2(CTestvbDlgAutoProxy, "Testvb.Application", 0xe4ac6a55, 0x1ed3, 0x11d6, 0x84, 0xda, 0x0, 0xc0, 0x4f, 0x37, 0x75, 0x64)

/////////////////////////////////////////////////////////////////////////////
// CTestvbDlgAutoProxy message handlers
