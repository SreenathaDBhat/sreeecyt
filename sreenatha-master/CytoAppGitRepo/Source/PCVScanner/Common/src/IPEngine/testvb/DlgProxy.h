// DlgProxy.h : header file
//

#if !defined(AFX_DLGPROXY_H__E4AC6A5E_1ED3_11D6_84DA_00C04F377564__INCLUDED_)
#define AFX_DLGPROXY_H__E4AC6A5E_1ED3_11D6_84DA_00C04F377564__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CTestvbDlg;

/////////////////////////////////////////////////////////////////////////////
// CTestvbDlgAutoProxy command target

class CTestvbDlgAutoProxy : public CCmdTarget
{
	DECLARE_DYNCREATE(CTestvbDlgAutoProxy)

	CTestvbDlgAutoProxy();           // protected constructor used by dynamic creation

// Attributes
public:
	CTestvbDlg* m_pDialog;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestvbDlgAutoProxy)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CTestvbDlgAutoProxy();

	// Generated message map functions
	//{{AFX_MSG(CTestvbDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	DECLARE_OLECREATE(CTestvbDlgAutoProxy)

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CTestvbDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPROXY_H__E4AC6A5E_1ED3_11D6_84DA_00C04F377564__INCLUDED_)
