// testvb.h : main header file for the TESTVB application
//

#if !defined(AFX_TESTVB_H__E4AC6A5A_1ED3_11D6_84DA_00C04F377564__INCLUDED_)
#define AFX_TESTVB_H__E4AC6A5A_1ED3_11D6_84DA_00C04F377564__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CTestvbApp:
// See testvb.cpp for the implementation of this class
//

class CTestvbApp : public CWinApp
{
public:
	CTestvbApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestvbApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CTestvbApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTVB_H__E4AC6A5A_1ED3_11D6_84DA_00C04F377564__INCLUDED_)
