// testvbDlg.cpp : implementation file
//

#include "stdafx.h"
#include "testvb.h"
#include "testvbDlg.h"
#include "DlgProxy.h"
#include "scriptproc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestvbDlg dialog

IMPLEMENT_DYNAMIC(CTestvbDlg, CDialog);

CTestvbDlg::CTestvbDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestvbDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTestvbDlg)
	m_ScriptName = _T("");
	m_NumberGoes = 0;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pAutoProxy = NULL;
}

CTestvbDlg::~CTestvbDlg()
{
	// If there is an automation proxy for this dialog, set
	//  its back pointer to this dialog to NULL, so it knows
	//  the dialog has been deleted.
	if (m_pAutoProxy != NULL)
		m_pAutoProxy->m_pDialog = NULL;
}

void CTestvbDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTestvbDlg)
	DDX_Control(pDX, IDC_BUTTON2, m_ExecBtn);
	DDX_Control(pDX, IDC_BUTTON1, m_NameBtn);
	DDX_Text(pDX, IDC_SCRIPTNAMEEDIT, m_ScriptName);
	DDX_Text(pDX, IDC_NUM, m_NumberGoes);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CTestvbDlg, CDialog)
	//{{AFX_MSG_MAP(CTestvbDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestvbDlg message handlers

BOOL CTestvbDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
    m_pWT = NULL;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTestvbDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTestvbDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

// Automation servers should not exit when a user closes the UI
//  if a controller still holds on to one of its objects.  These
//  message handlers make sure that if the proxy is still in use,
//  then the UI is hidden but the dialog remains around if it
//  is dismissed.

void CTestvbDlg::OnClose() 
{
	if (CanExit())
    {
		CDialog::OnClose();
    }
}

void CTestvbDlg::OnOK() 
{
	if (CanExit())
    {
		CDialog::OnOK();
    }
}

void CTestvbDlg::OnCancel() 
{
	if (CanExit())
    {
		CDialog::OnCancel();
    }
}

BOOL CTestvbDlg::CanExit()
{
	// If the proxy object is still around, then the automation
	//  controller is still holding on to this application.  Leave
	//  the dialog around, but hide its UI.
	if (m_pAutoProxy != NULL)
	{
		ShowWindow(SW_HIDE);
		return FALSE;
	}

	return TRUE;
}

void CTestvbDlg::OnButton2() 
{
    CWnd *pWnd = GetDlgItem(IDC_SCRIPTNAMEEDIT);
    //UpdateData(TRUE);
    
    pWnd->GetWindowText(TS.m_Name);
    TS.m_nNG = m_NumberGoes;

    m_ExecBtn.EnableWindow(FALSE);

    BeginWaitCursor();
    CWnd::SetTimer(1, 500, 0);
    m_pWT = AfxBeginThread(Thread, (void *) &TS, THREAD_PRIORITY_NORMAL);        
    
}

void CTestvbDlg::OnButton1() 
{
    CFileDialog FileSelect(FALSE, "script", "*.script", OFN_HIDEREADONLY, "Script Files (*.script) | *.script ||", NULL);
    CString csFileName;
    
    if (FileSelect.DoModal() == IDOK)
    {
        m_ScriptName  = FileSelect.GetPathName();
        UpdateData(FALSE);
    }	
}

UINT CTestvbDlg::Thread(LPVOID pParam)
{
    int i;
    ThreadStruct TS;
    CScriptProcessor CSP;
    CString C;

    memcpy(&TS, pParam, sizeof(TS));
    
    CSP.InitialiseScriptProcessor();

    // Set the script exec mode
    CSP.SetExecutionMode(modeDebug);
    // Now load it back in 
    CSP.LoadScript(TS.m_Name, TRUE);
    
    C = "_KARL_";

    // Run it        
    for (i = 0; i < TS.m_nNG; i++)
    {
        CSP.SetStringPersistent("__TEST__", C);
        CSP.ExecuteScript();
    }
        
    CSP.ReleaseScriptProcessor();

    return 0;
}

void CTestvbDlg::OnTimer(UINT nIDEvent) 
{
    DWORD Stat;

    if (m_pWT)
    {
        Stat = WaitForSingleObject(m_pWT->m_hThread, 5L);
        // All done
        if (Stat != WAIT_TIMEOUT)
        {
            EndWaitCursor();
            KillTimer(1);
            //delete m_pWT;
            m_pWT = NULL;
            m_ExecBtn.EnableWindow(TRUE);
        }
    }
	CDialog::OnTimer(nIDEvent);
}
