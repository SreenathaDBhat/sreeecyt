// testvbDlg.h : header file
//

#if !defined(AFX_TESTVBDLG_H__E4AC6A5C_1ED3_11D6_84DA_00C04F377564__INCLUDED_)
#define AFX_TESTVBDLG_H__E4AC6A5C_1ED3_11D6_84DA_00C04F377564__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CTestvbDlgAutoProxy;

typedef struct {
    CString              m_Name;
    int                  m_nNG;
} ThreadStruct;

/////////////////////////////////////////////////////////////////////////////
// CTestvbDlg dialog

class CTestvbDlg : public CDialog
{
	DECLARE_DYNAMIC(CTestvbDlg);
	friend class CTestvbDlgAutoProxy;

// Construction
public:
    CTestvbDlg(CWnd* pParent = NULL);	// standard constructor
    virtual ~CTestvbDlg();
     ThreadStruct TS;
   
    CWinThread  *m_pWT;
    
    static UINT Thread(LPVOID pParam);
    
    
    // Dialog Data
    //{{AFX_DATA(CTestvbDlg)
    enum { IDD = IDD_TESTVB_DIALOG };
    CButton	m_ExecBtn;
    CButton	m_NameBtn;
    CString	m_ScriptName;
    UINT	m_NumberGoes;
    //}}AFX_DATA
    
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CTestvbDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CTestvbDlgAutoProxy* m_pAutoProxy;
	HICON m_hIcon;

	BOOL CanExit();

	// Generated message map functions
	//{{AFX_MSG(CTestvbDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnButton2();
	afx_msg void OnButton1();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTVBDLG_H__E4AC6A5C_1ED3_11D6_84DA_00C04F377564__INCLUDED_)
