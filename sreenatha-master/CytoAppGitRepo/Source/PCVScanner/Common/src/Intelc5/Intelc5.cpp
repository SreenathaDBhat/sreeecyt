// Intelc5.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"

#include <stdlib.h>
#include <stdio.h>

#include "Intelc5.h"
#include "IPL.h"
//#include "IntelIpl.h"
#include "zImProc.h"

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}




//------------------------------------------------ file: IntelIpl.CPP

//---------------------------------------------------



//-------------------------------------------------------------------
//#include "comincls.h"

//#undef  Traded
//#define Traded  Exported
//#include "IntelIpl.h"
//#undef  Traded

//#define Traded  Imported

//#undef  Traded



//-------------------------------------------------------- GetIPLinfo
long GetIPLinfo(IplImage *image,
			long *xe, long *ye, long *pitch, char **mem)
{
	long pix = 0, nch;
   char *pnt;

   nch = image->nChannels;
   if ((nch != 1) && (nch != 3)) goto Fin;
	pix = nch * image->depth;
   *xe = image->width;
   *ye = image->height;
   *pitch = image->widthStep;
   if ((pnt = image->imageData) == NULL)
   	 pnt = image->imageDataOrigin;
   *mem = pnt;
Fin:
   return pix;
}




//----------------------------------------------------------- MakeIPL
// Returns pointer to created IPL image struct
// FOR USE ONLY IN THIS FILE
/****************************
IPLAPI(IplImage*, iplCreateImageHeader,
               (int   nChannels,  int     alphaChannel, int     depth,
                char* colorModel, char*   channelSeq,   int     dataOrder,
                int   origin,     int     align,
                int   width,      int   height, IplROI* roi, IplImage* maskROI,
                void* imageId,    IplTileInfo* tileInfo))
*****************************/


IplImage *MakeIPL(long pix, long xe, long ye, long pitch, void *mem)
{
    char *colorModel, *channelSeq, *imageId;
	 IplImage *image = NULL;
    int nChannels, alphaChannel, depth, dataOrder,
    	  origin, align, height, width, i;

    nChannels = alphaChannel = 0;
    colorModel = "GRAY";  channelSeq = "";
    dataOrder = IPL_DATA_ORDER_PIXEL;
    origin = IPL_ORIGIN_TL;  // IPL_ORIGIN_BL;
    if ((mem) && (pitch & 4)) align = IPL_ALIGN_DWORD;
    else align = IPL_ALIGN_QWORD;
    height = ye;  width = xe;
    imageId = "";
    if (pix == 1)
    {
    	  nChannels = 1;  depth = IPL_DEPTH_1U;
    }
    if (pix == 8)
    {
    	  nChannels = 1;  depth = IPL_DEPTH_8U;
    }
    if (pix == 24)
    {
    	  nChannels = 3;  depth = IPL_DEPTH_8U;
        colorModel = "RGB";  channelSeq = "BGR";
    }
    if (!nChannels) goto Fin;
    image = iplCreateImageHeader (
                nChannels,  alphaChannel, depth,
                colorModel, channelSeq, dataOrder,
                origin, align, width, height,
                NULL /*roi*/, NULL /*maskroi*/, imageId,NULL /*tileInfo*/);

    image->imageData = (char *)mem;  // image->imageDataOrigin = NULL;
//    image->imageDataOrigin = (char *)mem;  // image->imageData = NULL;
    if ((mem) && (pitch)) image->widthStep = pitch;
    for (i = 0;  i < 4;  i++)
    	  image->BorderMode[i] = IPL_BORDER_REPLICATE;

Fin:
    return image;
}


//---------------------------------------------------
//---------------------------------------------------- iC5MaskBitwise
/*************************************************
#define momZERO 	   0    // set all bits to zero
#define momAND 		   1    // A & B
#define momMORE   	   2    // A & ~B  (1 if A > B)
#define momCOPY1 	   3    // A  (copy A into B)
#define momLESS 	   4    // ~A & B  (1 if A < B)
#define momCOPY2 	   5    // B  (copy B {or const} into B)
#define momEXOR 	   6    // A ^ B  (mod 2)
#define momOR 		   7    // A | B
#define momNOR 		   8    // ~(A | B)  (not OR)
#define momEQUAL 	   9    // ~(A ^ B)  (1 if A = B)
#define momNOT2 	  10    // copy ~B into B
#define momEQMORE 	  11    // A | ~B  (1 if A >= B)
#define momNOT1 	  12    // copy ~A into B
#define momEQLESS 	  13    // ~A | B  (1 if A <= B)
#define momNAND 	  14    // ~(A & B)  (not AND)
#define momFULL 	  15    // set all bits to 1 (bytes to 255)
*************************************************/

void _stdcall iC5MaskBitwise (BYTE *mem1,
				DWORD xe, DWORD ye, DWORD pitch1,
            BYTE *mem2, DWORD pitch2,
            short oper)
{
	 IplImage *image1, *image2;

    image1 = MakeIPL(1, xe, ye, pitch1, mem1);
    image2 = MakeIPL(1, xe, ye, pitch2, mem2);

	 switch (oper)
    {
		case momZERO: iplSet (image2, 0); break;
		case momAND: iplAnd (image1, image2, image2); break;
		case momMORE: iplNot (image2, image2);
      	iplAnd (image1, image2, image2); break;
		case momCOPY1: iplCopy (image1, image2); break;
		case momLESS: iplNot (image2, image2);
      	iplOr (image1, image2, image2);
         iplNot (image2, image2); break;
		case momCOPY2: iplCopy (image2, image1); break;
		case momEXOR: iplXor (image1, image2, image2); break;
		case momOR: iplOr (image1, image2, image2); break;
		case momNOR: iplOr (image1, image2, image2);
         iplNot (image2, image2); break;
		case momEQUAL: iplXor (image1, image2, image2);
         iplNot (image2, image2); break;
		case momNOT2: iplNot (image2, image2); break;
		case momEQMORE: iplNot (image2, image2);
      	iplOr (image1, image2, image2); break;
		case momNOT1: iplNot (image1, image2); break;
		case momEQLESS: iplNot (image2, image2);
      	iplAnd (image1, image2, image2);
         iplNot (image2, image2); break;
		case momNAND: iplAnd (image1, image2, image2);
         iplNot (image2, image2); break;
		case momFULL: iplSet (image2, 255); break;

      default: ;
    }

    iplDeallocate (image1, IPL_IMAGE_HEADER);
    iplDeallocate (image2, IPL_IMAGE_HEADER);

}


//-------------------------------------------------------- iC5Convol2
/***
***/
void _stdcall iC5Convol2 (BYTE *mem,
					long xe, long ye,	long pitch,
               long xf, long yf, long xc, long yc,
               long *coeffs, long rshift)
{
	 IplImage *image1;
    IplConvKernel *kernel;
    int *values;
    long i, n;

    n = xf * yf;
    values = (int *)malloc(n * sizeof(int));
    for (i = 0;  i < n;  i++) values[i] = (signed char)coeffs[i];
	  kernel = iplCreateConvKernel(xf, yf, xc, yc, values, rshift);
      image1 = MakeIPL(8, xe, ye, pitch, mem);
//      iplSetBorderMode,(image1, IPL_BORDER_REPLICATE, IntelLibAllSides, 0);

	    iplConvolve2D(image1, image1, &kernel, 1, IPL_SUM);

	   iplDeleteConvKernel(kernel);
     iplDeallocate (image1, IPL_IMAGE_HEADER);
	 free (values);
}


//-------------------------------------------------------- iC5MaskNei
/*************************************************
		oper :
*	mnmMIN   =0   min of self and 8 bit neighbors (erosion);
*	mnmMAX   =1   max of self and 8 bit neighbors (dilation);
			mnmMIN4	 =20  min of the pixel and it's 4 neighbors;
			mnmMAX4	 =21  max of the pixel and it's 4 neighbors;
			mnmMINH2 =22  min of self and 2 bit horizontal neighbors;
			mnmMAXH2 =23  max of self and 2 bit horizontal neighbors;
			mnmMINV2 =24  min of self and 2 bit vertical neighbors;
			mnmMAXV2 =25  max of self and 2 bit vertical neighbors;
*	mnmOPEN8 =26  same as mnmMIN followed by mnmMAX;
*	mnmCLOSE8 =27 same as mnmMAX followed by mnmMIN;
			mnmOPEN4 =28  same as mnmMIN4 followed by mnmMAX4;
			mnmCLOSE4 =29 same as mnmMAX4 followed by mnmMIN4;

*************************************************/

void _stdcall iC5MaskNei (BYTE *mem, DWORD xe, DWORD ye,
				DWORD pitch, long oper, long rep)
{
	 IplImage *image;

	 if ((IntelLibMaskNeiOp >> oper) & 1)
    {
	    image = MakeIPL(1, xe, ye, pitch, mem);
   	 switch (oper)
   	 {
			case mnmMIN: iplErode (image, image, rep); break;
			case mnmMAX: iplDilate (image, image, rep); break;
			case mnmOPEN8: iplOpen (image, image, rep); break;
			case mnmCLOSE8: iplClose (image, image, rep); break;

	      default: ;

	    }
       iplDeallocate (image, IPL_IMAGE_HEADER);
    } else
    {
    	 MaskNei (mem, xe, ye, pitch, oper, rep);
    }
}


//----------------------------------------------------- iC5ConvolSep2
/***
***/
void _stdcall iC5ConvolSep2 (BYTE *mem,
					long xe, long ye,	long pitch,
               long xn, long xc, long *xcoeffs, long xrshift,
               long yn, long yc, long *ycoeffs, long yrshift)
{
	 IplImage *image1;
    IplConvKernel *kernel1, *kernel2;
    int values[256];
    long i;

    for (i = 0;  i < xn;  i++) values[i] = (int)xcoeffs[i];
    for (i = 0;  i < yn;  i++) values[xn + i] = (int)ycoeffs[i];

	 kernel1 = iplCreateConvKernel(xn, 1, xc, 0, values, xrshift);
	 kernel2 = iplCreateConvKernel(1, yn, 0, yc, values + xn, yrshift);

	 image1 = MakeIPL(8, xe, ye, pitch, mem);
//    iplSetBorderMode,(image1, IPL_BORDER_REPLICATE, IntelLibAllSides, 0);

	 iplConvolveSep2D(image1, image1, kernel1,  kernel2);

    iplDeallocate (image1, IPL_IMAGE_HEADER);

  	 iplDeleteConvKernel(kernel1);
	 iplDeleteConvKernel(kernel2);
}



//--------------------------------------------------------- iC5Filter
/***
			mnmMIN   =0
			mnmMAX   =1
			mnmMED  	=13
			mnmBLUR 	=14
***/
void _stdcall iC5Filter (BYTE *mem,
					long xe, long ye,	long pitch,
               long xf, long yf, long xc, long yc,
               long oper)
{
	 IplImage *image1, *image2;

    image1 = MakeIPL(8, xe, ye, pitch, mem);
    image2 = MakeIPL(8, xe, ye, (xe + 7) & 0xfffffff8, NULL);
    iplAllocateImage(image2, 0, 0); // do not initialize
	 iplCopy (image1, image2);

	 switch (oper)
    {
		case mnmMIN:
			iplMinFilter(image2, image1, yf, xf, xc, yc);
         break;
		case mnmMAX:
			iplMaxFilter(image2, image1, xf, yf, xc, yc);
         break;
		case mnmMED:
			iplMedianFilter(image2, image1, xf, yf, xc, yc);
         break;
		case mnmBLUR:
			iplBlur(image2, image1, xf, yf, xc, yc);
         break;

      default: // old variant must never work!
      	;
    }

    iplDeallocate (image2, IPL_IMAGE_ALL);
    iplDeallocate (image1, IPL_IMAGE_HEADER);
}



//--------------------------------------------------------- iC5NeiMem
/*************************************************
			mnmMIN   =0   min of self and 8 byte neighbors (erosion);
			mnmMAX   =1   max of self and 8 byte neighbors (dilation);
			mnm124   =12  averaging with matrix {1,2,1; 2,4,2; 1,2,1}/16
			mnmMED  	=13  median of self and 8 bit neighbors;
			mnmBLUR 	=14  averaging with matrix {1,1,1; 1,1,1; 1,1,1}/9
			mnmMIN4	=20  min of the pixel and it's 4 neighbors;
			mnmMAX4	=21  max of the pixel and it's 4 neighbors;
			mnmMINH2 =22  min of self and 2 bit horizontal neighbors;
			mnmMAXH2 =23  max of self and 2 bit horizontal neighbors;
			mnmMINV2 =24  min of self and 2 bit vertical neighbors;
			mnmMAXV2 =25  max of self and 2 bit vertical neighbors;
			mnmOPEN8 =26  same as mnmMIN followed by mnmMAX;
			mnmCLOSE8 =27 same as mnmMAX followed by mnmMIN;
			mnmOPEN4 =28  same as mnmMIN4 followed by mnmMAX4;
			mnmCLOSE4 =29 same as mnmMAX4 followed by mnmMIN4;
*************************************************/
void _stdcall iC5NeiMem (BYTE *mem,
					long xe, long ye,	long pitch,
               long oper, long rep)
{
	 IplImage *image1, *image2;
    long xf, yf, xc, yc, a121[] = {1, 2, 1};

    if (oper == mnm124)
    {
         while (rep--)
         {
				 iC5ConvolSep2 (mem, xe, ye, pitch,
               				 3, 1, a121, 2,
                        	 3, 1, a121, 2);
         }
         return;
    }
	 if (((IntelLibNeiMemOp >> oper) & 1) == 0)
    {
        NeiMem (mem, xe, ye, pitch, oper, rep);
        return;
    }

    image1 = MakeIPL(8, xe, ye, pitch, mem);
    image2 = MakeIPL(8, xe, ye, (xe + 7) & 0xfffffff8, NULL);
    iplAllocateImage(image2, 0, 0); // do not initialize

    xf = yf = 2 * rep + 1;
    xc = yc = rep;
	 switch (oper)
    {
		case mnmMIN:
			iplCopy (image1, image2);
			iplMinFilter(image2, image1, xf, yf, xc, yc);
         break;
		case mnmMAX:
			iplCopy (image1, image2);
			iplMaxFilter(image2, image1, xf, yf, xc, yc);
         break;
		case mnmOPEN8:
//			iplOpen(image2, image1, rep);
			iplMinFilter(image1, image2, xf, yf, xc, yc);
			iplMaxFilter(image2, image1, xf, yf, xc, yc);
         break;
		case mnmCLOSE8:
//			iplClose(image2, image1, rep);
			iplMaxFilter(image1, image2, xf, yf, xc, yc);
			iplMinFilter(image2, image1, xf, yf, xc, yc);
         break;
		case mnmBLUR:
			iplCopy (image1, image2);
			iplBlur(image2, image1, xf, yf, xc, yc);
         break;
		case mnmMINH2:
			iplCopy (image1, image2);
			iplMinFilter(image2, image1, xf, 1, xc, 0);
         break;
		case mnmMAXH2:
			iplCopy (image1, image2);
			iplMaxFilter(image2, image1, xf, 1, xc, 0);
         break;
		case mnmMINV2:
			iplCopy (image1, image2);
			iplMinFilter(image2, image1, 1, yf, 0, yc);
         break;
		case mnmMAXV2:
			iplCopy (image1, image2);
			iplMaxFilter(image2, image1, 1, yf, 0, yc);
         break;

      default: // old variant must never work!
      	;
    }
    iplDeallocate (image2, IPL_IMAGE_ALL);
    iplDeallocate (image1, IPL_IMAGE_HEADER);

}



//---------------------------------------------------- iC5MemConstMem
/*************************************************
			momZERO 	= 0   set all bits to zero
			momAND 	= 1   A & C
			momMORE 	= 2   A & ~C  (1 if A > C)
			momCOPY1 = 3   A  (copy A into B)
			momLESS 	= 4   ~A & C  (1 if A < C)
			momCOPY2 = 5   C  (copy const into B)
			momEXOR 	= 6   A ^ C  (mod 2 {exclusive OR})
			momOR 	= 7   A | C
			momNOR 	= 8   ~(A | C)  (not OR)
			momEQUAL = 9   ~(A ^ C)  (1 if A = C)
			momNOT2 	=10   copy ~C into B
			momEQMORE=11   A | ~C  (1 if A >= C)
			momNOT1 	=12   copy ~A into B
			momEQLESS=13   ~A | C  (1 if A <= C)
			momNAND 	=14   ~(A & C)  (not AND)
			momFULL 	=15   set all bits to 1 (bytes to 255)

			momMIN 	=16   A MIN C
			momMAX 	=17   A MAX C
			momPLUS 	=18   (A + C) mod 256
			momSPLUS =19   (A + C) min 255 (saturation)
			momMINUS =20   (A - C) mod 256
			momIMINUS=21   (C - A) mod 256 (inverse order)
			momSMINUS=22   (A - C) max 0   (saturation)
			momSIMINUS 	=23   (C - A) max 0   (saturation and inverse order)
			momLOWto0 	=24   if A >= C then A else 0
			momHIGHtoFF =25   if A <= C then A else 255
			mom0orFF 	=26   if A < C then 0 else 255
*************************************************/
void _stdcall iC5MemConstMem (
					BYTE *mem1, BYTE *mem2, long size,
					long cnst, short oper)
{
	 IplImage *image1, *image2;
    long notC;

	 if ((AIConlyMemOp >> oper) & 1)
    {
     	  MemConstMem (mem1, mem2, size, (WORD)cnst, oper);
        return;
    }

    image1 = MakeIPL(8, size, 1, size, mem1);
    if (mem1 != mem2)
    	  image2 = MakeIPL(8, size, 1, size, mem2);
    else
    	  image2 = image1;
    notC = (cnst ^ 0xff) & 0xff;
	 switch (oper)
    {
		case momZERO: iplSet (image2, 0); break;
		case momAND: iplAndS (image1, image2, cnst); break;
		case momMORE: iplAndS (image1, image2, notC); break;
		case momCOPY1: iplCopy (image1, image2); break;
		case momLESS: iplNot (image1, image2);
      	iplAndS (image2, image2, cnst); break;
		case momCOPY2: iplSet (image2, cnst); break;
		case momEXOR: iplXorS (image1, image2, cnst); break;
		case momOR: iplOrS (image1, image2, cnst); break;
		case momNOR: iplOrS (image1, image2, cnst);
         iplNot (image2, image2); break;
		case momEQUAL: iplXorS (image1, image2, cnst);
         iplNot (image2, image2); break;
		case momNOT2: iplSet (image2, notC); break;
		case momEQMORE: iplOrS (image1, image2, notC); break;
		case momNOT1: iplNot (image1, image2); break;
		case momEQLESS: iplNot (image1, image2);
      	iplOrS (image2, image2, cnst); break;
		case momNAND: iplAndS (image1, image2, cnst);
         iplNot (image2, image2); break;
		case momFULL: iplSet (image2, 255); break;
		// momMIN, momMAX, momPLUS: default
		case momSPLUS: iplAddS (image1, image2, cnst); break;
		// momMINUS, momIMINUS: default
		case momSMINUS: iplSubtractS (image1, image2, cnst, false); break;
		case momSIMINUS: iplSubtractS (image1, image2, cnst, true); break;
      // momLOWto0, momHIGHtoFF: default
		case mom0orFF: iplThreshold (image1, image2, cnst); break;

      default: // old variant must never work!
      	MemConstMem (mem1, mem2, size, (WORD)cnst, oper);
    }

    iplDeallocate (image1, IPL_IMAGE_HEADER);
    if (mem1 != mem2)
    	  iplDeallocate (image2, IPL_IMAGE_HEADER);

}



//---------------------------------------------------
//------------------------------------------------------- iC5MemOpMem
/*************************************************
#define momZERO 	   0    // set all bits to zero
#define momAND 		   1    // A & B
#define momMORE   	   2    // A & ~B  (1 if A > B)
#define momCOPY1 	   3    // A  (copy A into B)
#define momLESS 	   4    // ~A & B  (1 if A < B)
#define momCOPY2 	   5    // B  (copy B {or const} into B)
#define momEXOR 	   6    // A ^ B  (mod 2)
#define momOR 		   7    // A | B
#define momNOR 		   8    // ~(A | B)  (not OR)
#define momEQUAL 	   9    // ~(A ^ B)  (1 if A = B)
#define momNOT2 	  10    // copy ~B into B
#define momEQMORE 	  11    // A | ~B  (1 if A >= B)
#define momNOT1 	  12    // copy ~A into B
#define momEQLESS 	  13    // ~A | B  (1 if A <= B)
#define momNAND 	  14    // ~(A & B)  (not AND)
#define momFULL 	  15    // set all bits to 1 (bytes to 255)
#define momMIN 		  16    // A MIN B
#define momMAX 		  17    // A MAX B
#define momPLUS 	  18    // (A + B) mod 256
#define momSPLUS 	  19    // (A + B) min 255 (saturation)
#define momMINUS 	  20    // (A - B) mod 256
#define momIMINUS 	  21    // (B - A) mod 256
#define momSMINUS 	  22    // (A - B) max 0   (saturation)
#define momSIMINUS 	  23    // (B - A) max 0   (saturation)
#define momLOWto0 	  24    // if A >= B then A else 0
#define momHIGHtoFF   25    // if A <= B then A else 255
#define mom0orFF 	  26    // if A < B then 0 else 255
*************************************************/

void _stdcall iC5MemOpMem (BYTE *mem1, BYTE *mem2, long size, WORD oper)
{
	 IplImage *image1, *image2;

	 if ((AIConlyMemOp >> oper) & 1)
    {
     	  MemOpMem (mem1, mem2, size, oper);
        return;
    }

    image1 = MakeIPL(8, size, 1, size, mem1);
    image2 = MakeIPL(8, size, 1, size, mem2);
	 switch (oper)
    {
		case momZERO: iplSet (image2, 0); break;
		case momAND: iplAnd (image1, image2, image2); break;
		case momMORE: iplNot (image2, image2);
      	iplAnd (image1, image2, image2); break;
		case momCOPY1: iplCopy (image1, image2); break;
		case momLESS: iplNot (image2, image2);
      	iplOr (image1, image2, image2);
         iplNot (image2, image2); break;
		case momCOPY2: iplCopy (image2, image1); break;
		case momEXOR: iplXor (image1, image2, image2); break;
		case momOR: iplOr (image1, image2, image2); break;
		case momNOR: iplOr (image1, image2, image2);
         iplNot (image2, image2); break;
		case momEQUAL: iplXor (image1, image2, image2);
         iplNot (image2, image2); break;
		case momNOT2: iplNot (image2, image2); break;
		case momEQMORE: iplNot (image2, image2);
      	iplOr (image1, image2, image2); break;
		case momNOT1: iplNot (image1, image2); break;
		case momEQLESS: iplNot (image2, image2);
      	iplAnd (image1, image2, image2);
         iplNot (image2, image2); break;
		case momNAND: iplAnd (image1, image2, image2);
         iplNot (image2, image2); break;
		case momFULL: iplSet (image2, 255); break;
		// momMIN, momMAX, momPLUS: default
		case momSPLUS: iplAdd (image1, image2, image2); break;
		// momMINUS, momIMINUS: default
		case momSMINUS: iplSubtract (image1, image2, image2); break;
		case momSIMINUS: iplSubtract (image2, image1, image2); break;
      // momLOWto0, momHIGHtoFF: default
		case mom0orFF: iplSubtractS (image2, image2, 1, false);
      	iplSubtract (image1, image2, image2);
         iplThreshold (image2, image2, 1); break;

      default: // old variant must never work!
			MemOpMem (mem1, mem2, size, oper);
    }

    iplDeallocate (image1, IPL_IMAGE_HEADER);
    iplDeallocate (image2, IPL_IMAGE_HEADER);

}












