// This is the main DLL file.
#include "Stdafx.h"
#include "ManagedScriptProc.h"
#include <CMeasurement.h>

namespace ManagedScriptProcessing
{
	
	// Initialise the script processor
	bool ManagedScriptProc::Initialise(String^ PersistentsFileName, String^ QPath, String^ QIn, String^ QOut)
	{
		
		if (scriptProcessor->InitialiseScriptProcessor(CString(PersistentsFileName), CString(QPath), CString(QIn), CString(QOut)))
		{
#ifdef _DEBUG
			scriptProcessor->SetExecutionMode(modeDebug);
#else
			scriptProcessor->SetExecutionMode(modePlay);
#endif
			return true;
		}
		else
			return false;
	}

	// Load a script in
	bool ManagedScriptProc::LoadScript(String^ ScriptName)
	{
		if (scriptProcessor->LoadScript(CString(ScriptName), FALSE))
			return true;
		else
			return false;
	}

	// Release the script processor    
	void ManagedScriptProc::ReleaseScriptProcessor()
	{
		scriptProcessor->ReleaseScriptProcessor();
	}

	// Run the script
	bool ManagedScriptProc::Execute()
	{
		if (scriptProcessor->ExecuteScript() == TRUE)
			return true;
		else
			return false;
	}

	// Set the script mode
	void ManagedScriptProc::SetExecutionMode(ScriptMode Mode)
	{
		scriptProcessor->SetExecutionMode((ScriptExecMode) Mode);
	}

	// Put the image on the script processing input queue - note it only deals with 8 bit data at this time
	bool ManagedScriptProc::PutImage(int W, int H, array<Byte>^ image)
	{
		VBS_IMAGE_REC VBImage;

		VBImage.BitsPerPixel = 8;
		VBImage.Width = W;
		VBImage.Height = H;
		
		pin_ptr<Byte> p = &image[0];
		
		// NOTE we have to take a copy before the above pinned pointer is out of scope
		BYTE *B = new BYTE[W * H];

		memcpy(B, p, W * H);

		VBImage.Address = B;

		// Hence we need the garbage collector
		dustbinman->Add(B);

		if (scriptProcessor->PutImage(&VBImage))
			return true;
		else
			return false;
	}

	// Put the image on the script processing input queue - note it only deals with 8 bit data at this time
	bool ManagedScriptProc::FlipPutImage(int W, int H, array<Byte>^ image)
	{
		VBS_IMAGE_REC VBImage;

		VBImage.BitsPerPixel = 8;
		VBImage.Width = W;
		VBImage.Height = H;
		
		pin_ptr<Byte> p = &image[0];
		
		// NOTE we have to take a copy before the above pinned pointer is out of scope
		BYTE *B = new BYTE[W * H];

		memcpy(B, p, W * H);

		VBImage.Address = B;

		// Hence we need the garbage collector
		dustbinman->Add(B);

		scriptProcessor->FlipImage(&VBImage);

		if (scriptProcessor->PutImage(&VBImage))
			return true;
		else
			return false;
	}
			
	// Reset the script engine
	void ManagedScriptProc::Reset()
	{
		dustbinman->Tidy();
		scriptProcessor->Reset();
	}

	// Set a string persistent
	void ManagedScriptProc::SetStringPersistent(String^ VarName, String^ VarValue)
	{
		scriptProcessor->SetStringPersistent(CString(VarName), CString(VarValue));
	}

	// Set a double persistent
	void ManagedScriptProc::SetDoublePersistent(String^ VarName, double VarValue)
	{
		scriptProcessor->SetDoublePersistent(CString(VarName), VarValue);
	}

	// Set a long persistent
	void ManagedScriptProc::SetLongPersistent(String^ VarName, System::Int32 VarValue)
	{
		scriptProcessor->SetLongPersistent(CString(VarName), VarValue);
	}


	// Get a double persistent
	bool ManagedScriptProc::GetDoublePersistent(String^ VarName, REFOUT double% VarValue)
	{
		double d;
		if (scriptProcessor->GetDoublePersistent(CString(VarName), &d))
		{
			VarValue = d;
			return true;
		}

		VarValue = 0.0;
		return false;
	}

	// Get a long persistent
	bool ManagedScriptProc::GetLongPersistent(String^ VarName, REFOUT long% VarValue)
	{
		long i;
		if (scriptProcessor->GetLongPersistent(CString(VarName), &i))
		{
			VarValue = i;
			return true;
		}

		VarValue = 0;
		return false;
	}


	// Get a measurement
	bool ManagedScriptProc::GetMeasurement(REFOUT String^ VarName, REFOUT double% Data)
	{
		Measurement *pValue;

		if (scriptProcessor->GetMeasurement(&pValue))
		{
			Data = pValue->GetValue();

			return true;
		}

		return false;
	}

	// Reset the persistent variables
	void ManagedScriptProc::ResetPersistentVariables()
	{
		scriptProcessor->ResetPersistentVariables();
	}
}

	