// ManagedScriptProc.h
#pragma once

using namespace System;
using namespace System::ComponentModel;

#define REFOUT [System::Runtime::InteropServices::Out] 

#include <scriptproc.h>
#include <Garbage.h>

namespace ManagedScriptProcessing 
{

	public ref class ManagedScriptProc : public IDisposable
	{
	private:
		bool Disposed;

	public:
		enum class ScriptMode
		{
			Debug = modeDebug,
			Play  = modePlay
		};

		ManagedScriptProc() : scriptProcessor(new CScriptProcessor())
		{
			Disposed = false;
			dustbinman = new Garbage();

		}
			
		// User explicitly disposes of the script engine
		~ManagedScriptProc() 	
		{
			this->!ManagedScriptProc();
		}

		// User does not explicitly dispose of the script engine
		!ManagedScriptProc()
		{
			if (!Disposed)
			{
				scriptProcessor->ReleaseScriptProcessor();
				delete scriptProcessor;
				delete dustbinman;
				Disposed = true;
			}
		}

	

		   // Initialise anything related to the script processing engine
		//BOOL InitialiseScriptProcessor(LPCTSTR PersistentsFileName, LPCTSTR QPath, LPCTSTR Qin, LPCTSTR Qout, BOOL SavePersistents = TRUE);
		bool Initialise(String^ PersistentsFileName, String^ QPath, String^ Qin, String^ QOut);

		// Load a script into memory ready for execution
		//BOOL LoadScript(LPCTSTR ScriptName, BOOL Expand, BOOL IgnoreInit = FALSE);
		bool LoadScript(String^ ScriptName);

		// Execute a script    
		//BOOL ExecuteScript(void);
		bool Execute();

		// Reset the script engine
		void Reset();

		// Set the script mode
		 // Set the mode the script should operate in
		//void SetExecutionMode(ScriptExecMode Mode);
		void SetExecutionMode(ScriptMode Mode);

		// Put an image record on the script input Q
		//BOOL PutImage(VBS_IMAGE_REC *ImageData);
		bool PutImage(int W, int H, array<Byte>^ image);

		// Flip and put the image
		bool FlipPutImage(int W, int H, array<Byte>^ image);

		// Release the script processor    
		void ReleaseScriptProcessor();

		// Persistent variable stuff
		void ResetPersistentVariables();

		//void SetStringPersistent(LPCTSTR VarName, CString& VarValue);
		void SetStringPersistent(String^ VarName, String^ VarValue);

		//void SetDoublePersistent(LPCTSTR VarName, double VarValue);
		 void SetDoublePersistent(String^ VarName, double VarValue);

		//void SetLongPersistent(LPCTSTR VarName, long VarValue);
		void SetLongPersistent(String^ VarName, System::Int32 VarValue);

		//BOOL GetDoublePersistent(LPCTSTR VarName, double *VarValue);
		bool GetDoublePersistent(String^ VarName, REFOUT double% VarValue);

		//BOOL GetLongPersistent(LPCTSTR VarName, long *VarValue);
		bool GetLongPersistent(String^ VarName, REFOUT long% VarValue);

		// Get a measurement
		bool GetMeasurement(REFOUT String^ VarName, REFOUT double% Data);


	private:
		Garbage			 * dustbinman;
		CScriptProcessor * scriptProcessor;
	};
}
