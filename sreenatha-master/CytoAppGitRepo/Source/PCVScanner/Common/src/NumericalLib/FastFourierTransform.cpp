/*
 *	fft.c		K.Shields (original code)
 *			M.Gregson (tweaks and optimisation)
 *
 *	Modifications:
 *
 *	05Oct00 MG	Added maxshift to limit registration search area to +/- maxshift in x and y
 *	16Oct98	MG	Added edge clearing around fft to avoid problems of images with noisy edge data
 *	19Aug98	SN	Malloc is actually malloc for Win32.
 *	19Aug98		---------------WinCV-------------------------
 *	9Feb98	MG	Changed double to float - accuracy not needed
 *			and Mallocs halved.
 */
#include "FastFourierTransform.h"
#include <memory.h>
#include <math.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
//#include <fft.h>


#define Malloc	malloc
#define Free	free

#define SWAP(a,b) tempr=(a); (a)=(b); (b)=tempr

#define EDGE 10	// ignore data aroudn image edge - may be noisy	MG6Oct98


/**********************************************************************************/
//Constructor
CFFT::CFFT()
{
}

//Destructor
CFFT::~CFFT()
{
}


/*
 *	Determine size and offset of fft image inside origianl image
 *	to ensure fft image has cols and lines which are powers of 2
 */

void CFFT::get_fft_size(int imcols, int imlines, int *xoff, int *yoff, int *newcols, int *newlines)
{
	int cols,lines;

	cols=1;
	while (cols <= imcols) cols *= 2;
	cols /= 2;

	lines=1;
	while (lines <= imlines) lines *= 2;
	lines /= 2;

	*xoff=(imcols - cols)/2;
	*yoff=(imlines - lines)/2;
	*newcols = cols;
	*newlines = lines;
}

/**********************************************************************************/

Uchar CFFT::val(Uchar *image, int x, int y, int cols, int lines)
{
	int size,off_set;

	size=lines*cols;
	off_set = x + (y*cols);
	if ((off_set < size)&&(off_set >=0))
		return image[off_set];
	else return 0;
}

/**********************************************************************************


    _data[1]
   |                   row of 2*N2 float numbers
   |     _________________________/\_______________________
   |    /                                                  \
   |   ______________________________________________________
   |  |   :   |   :   |                      |   :   |   :   |
   |__|_  :   |   :   |     row 1            |   :   |   :   |___
      |___:___|___:___|______________________|___:___|___:___|   |
                                                                 |
    _____________________________________________________________|
   |
   |   ______________________________________________________
   |  |   :   |   :   |                      |   :   |   :   |
   |_\|Re :Im |   :   |     row 2            |   :   |   :   |___
     /|___:___|___:___|______________________|___:___|___:___|   |
                                                                 |
    __________________________......_____________________________|
   |
   |   ______________________________________________________
   |  |   :   |   :   |                      |   :   |   :   |
   |_\|   :   |   :   |     row N1/2         |   :   |   :   |___
     /|___:___|___:___|______________________|___:___|___:___|   |
                                                                 |
    _____________________________________________________________|
   |
   |   ______________________________________________________
   |  |   :   |   :   |                      |   :   |   :   |
   |_\|   :   |   :   |     row N1/2 + 1     |   :   |   :   |___
     /|___:___|___:___|______________________|___:___|___:___|   |
                                                                 |
    _____________________________________________________________|
   |
   |   ______________________________________________________
   |  |   :   |   :   |                      |   :   |   :   |
   |_\|   :   |   :   |     row N1/2 + 2     |   :   |   :   |___
     /|___:___|___:___|______________________|___:___|___:___|   |
                                                                 |
    __________________________......_____________________________|
   |
   |   ______________________________________________________
   |  |   :   |   :   |                      |   :   |   :   |
   |_\|   :   |   :   |     row N1           |   :   |   :  -|----------data[2*N1*N2]
     /|___:___|___:___|______________________|___:___|___:___|

***************************************************************************/

/* Replaces 'data' by its ndim-dimensional discrete fourier transform, if 'isign' is input as 1,
** nn[1..ndim] is an integer array containing the lengths of each dimension (number of complex
** values), which MUST all be powers of 2. 'data' is a real array of twice the product of these
** lengths, in which the data are stored as in a multidensional complex array: real and imaginary
** parts of each element are in consecutive locations, and the right most index of the array
** increases most rapidly as one proceeds along 'data'. For a 2 dimensional array, this is
** equivelent to storing the array by rows. If 'isign' is input as -1, 'data' is replaced
** by it inverse transform times the product of the lengths of all dimensions
*/

void CFFT::fourn(float data[], unsigned long nn[], int ndim, int isign)
{
	int idim;
	unsigned long i1, i2, i3, i2rev, i3rev, ip1, ip2, ip3, ifp1, ifp2;
	unsigned long ibit, k1, k2, n, nprev, nrem, ntot;
	float tempi, tempr;
	double theta, wi, wpi, wpr, wr, wtemp;
	int qw=42;

	for (ntot=1,idim=1 ; idim<=ndim ; idim++)
		ntot *= nn[idim];
	nprev=1;
	for (idim=ndim ; idim>=1 ; idim--) {
		n = nn[idim];
		nrem = ntot / (n*nprev);
		ip1 = nprev << 1;
		ip2 = ip1 * n;
		ip3 = ip2 * nrem;
		i2rev = 1;
		for (i2=1 ; i2<=ip2 ; i2 += ip1) {
			if (i2 < i2rev) {
				for (i1=i2 ; i1<=i2+ip1-2 ; i1 += 2) {
					for (i3=i1 ; i3<=ip3 ; i3 += ip2) {
						i3rev = i2rev + i3 -i2;
						SWAP(data[i3], data[i3rev]);
						SWAP(data[i3+1], data[i3rev+1]);
					}
				}
			}
			ibit = ip2>>1;
			while (ibit>=ip1 && i2rev>ibit) {
				i2rev -=ibit;
				ibit >>= 1;
			}
			i2rev += ibit;
		}
		ifp1=ip1;
		while (ifp1 < ip2) {
			ifp2 = ifp1<<1;
			theta = isign*6.28318530717959/(ifp2/ip1);
			wtemp=sin(0.5*theta);
			wpr = -2.0*wtemp*wtemp;
			wpi = sin(theta);
			wr=1.0;
			wi=0.0;
			for (i3=1 ; i3<=ifp1 ; i3 += ip1) {
				for (i1=i3 ; i1<=i3+ip1-2 ; i1 += 2) {
					for (i2=i1 ; i2<=ip3 ; i2 += ifp2) {
						k1=i2;
						k2=k1+ifp1;
						tempr=(float)(wr*data[k2]-(float)wi*data[k2+1]);
						tempi=(float)(wr*data[k2+1]+(float)wi*data[k2]);
						data[k2]=data[k1]-tempr;
						data[k2+1]=data[k1+1]-tempi;
						data[k1] += tempr;
						data[k1+1] += tempi;
					}
				}
				wr=(wtemp=wr)*wpr-wi*wpi+wr;
				wi=wi*wpr+wtemp*wpi+wi;
			}
			ifp1=ifp2;
		}
		nprev *= n;
	}
}


/**********************************************************************************/
/* Work out the 2 dimensional fft of the supplied raw image. The raw
** image is of size imcols X imlines, while the fft is worked out over the
** area rescols X reslines, rescols and reslines should both be nth powers of 2.
** The origin of the the result area in image is resx, resy.
** When finished, result holds consecutive real and imaginary values making up
** the fft over the rescols X reslines area.
*/
void CFFT::im_fft(Uchar *image, int imcols, int imlines, float *result, int resx, int resy, int rescols, int reslines)
{
	int i,j;
	unsigned long nn[3];
	float *rptr;
	Uchar *imptr;

	/* check params are sensible */
	if (!image) {
		fprintf(stderr,"im_fft sent NULL source image\n\7");
		return;
	}

	if (!image) {
		fprintf(stderr,"im_fft sent NULL result image\n\7");
		return;
	}

	i=1;
	while (i < rescols) i *= 2;
	if (i > rescols) {
		fprintf(stderr,"im_fft, rescols is not power of 2 (%d)\n\7",rescols);
		return;
	}

	i=1;
	while (i < reslines) i *= 2;
	if (i > reslines) {
		fprintf(stderr,"im_fft, reslines is not power of 2 (%d)\n\7",reslines);
		return;
	}

	if ((resx < 0) 
	|| (resy < 0)
	|| ((resx+rescols) > imcols)
	|| ((resy+reslines) > imlines)) {
		fprintf(stderr,"im_fft , result not contained by image\n\7");
		return;
	}

	nn[1]=rescols;
	nn[2]=reslines;

	imptr=image + resx + resy * imcols;

	rptr=result+1;

	for (j=0; j<reslines ; j++) {

		for (i=0; i<rescols ; i++) {
			/* REAL PART */
			if ((i<EDGE) || (i>=rescols-EDGE) || (j<EDGE) || (j>=reslines-EDGE))
			{
				// ignore possibly noisy data around the image edges
				*rptr++ = (float)0.0;
				imptr++;
			}
			else
				*rptr++ = (float)*imptr++;

			/* IMAGINARY PART */
			*rptr++ = (float)0.0;
		}
		resy++;
		imptr=image + resx + resy * imcols;
	}

	fourn(result, nn, 2, 1);

}

/**********************************************************************************/
/* given fft of two raw images, works out the required offsets and puts the result
** into xshift, yshift - NOTE : image0_fft is preserved, image1_fft is modified
** maxshift limits the registration search area to +/- maxshift in x and y
*/
void CFFT::fft_image_reg_core(float *image0_fft, float *image1_fft, int newcols, int newlines, int *xshift, int *yshift, int maxshift)
{
	float *fptr, *gptr, imag, real, max;
	int i, j, xmax, ymax;
	unsigned long nn[3];

	/* work out the correlation between image0 and image1 by multiplying
	** the fft of image0 with the complex conjugate of the fft of image1
	*/
	fptr = image1_fft+1;
	gptr = image0_fft+1;
	for (j = 0; j < newlines ; j++) {
		for (i = 0; i < newcols ; i++) {
			real =  (*fptr)*(*gptr) + (*(fptr+1))*(*(gptr+1));
			imag =  (*(fptr+1))*(*gptr) - (*fptr)*(*(gptr+1));

			*fptr++ = real ;
			*fptr++ = imag;

			gptr++;
			gptr++ ;
		}
	}

	nn[1]=newcols;
	nn[2]=newlines;
	fourn(image1_fft, nn, 2, -1);

	max = 0.0;
	fptr=image1_fft+1;
	for (j = 0; j < newlines ; j++) {
		for (i = 0; i < newcols ; i++) {

			if (((j<maxshift) || (newlines-j < maxshift)) &&
				((i<maxshift) || (newcols-i < maxshift)))
				if (*fptr > max) {
					max = *fptr;
					xmax = i;
					ymax = j;
				}

			fptr++;
			fptr++;
		}
	}


	if (xmax > newcols/2)
		xmax -= newcols;
	if (ymax > newlines/2)
		ymax -= newlines;

	/* NOTE: invert offsets - coz this routine actually registers image0 to image1
 	   - coz image0_fft is preserved but image1_fft is not */

	*xshift = -xmax;  
	*yshift = -ymax;
}


/**********************************************************************************/
/* given two raw images, works out the required offsets and puts the result
** into xshift, yshift
*/

void CFFT::fft_image_reg(Uchar *image0, Uchar *image1, int cols, int lines, int *xshift, int *yshift, int maxshift)
{
	float *image0_fft;
	int xoff, yoff, newcols, newlines;

	/* Determine size and offset of fft image inside origianl image
	   to ensure fft image has cols and lines which are powers of 2 */

	get_fft_size(cols, lines, &xoff, &yoff, &newcols, &newlines);

	/* allocate space for fft */
	if ((image0_fft=(float *)calloc( 1 + newlines*newcols*2, sizeof(float) ))==NULL) {
		fprintf(stderr,"float calloc failed\n");
		return;
	}

	/* get fft of image0 */
	im_fft(image0, cols, lines, image0_fft, xoff, yoff, newcols, newlines);

	/* register images */
	fft_quick_image_reg(image0_fft, image1, cols, lines, xshift, yshift, maxshift);

	Free(image0_fft);
}

/**********************************************************************************/
/* Same as above except the fft of the first image is supplied - and not Freed
** This saves time when several images are being registered together
** e.g. when MFISH or CGH fluors are being registered to DAPI counterstain 
*/

void CFFT::fft_quick_image_reg(float *image0_fft, Uchar *image1, int cols, int lines, int *xshift, int *yshift, int maxshift)
{
	int xoff, yoff, newcols, newlines;

	/* Determine size and offset of fft image inside origianl image
	   to ensure fft image has cols and lines which are powers of 2 */

	get_fft_size(cols, lines, &xoff, &yoff, &newcols, &newlines);

	/* register images */
	fft_quick_area_image_reg(	image0_fft, image1, 
					cols, lines,
					xoff, yoff, newcols, newlines, 
					xshift, yshift, maxshift);

}

/**********************************************************************************/
/* Same as above except this time we pass in the area and offset of the
 * the window we would like to register - user must take care
 */

void CFFT::fft_quick_area_image_reg(float *image0_fft, Uchar *image1, int cols, int lines, int xoff, int yoff, int newcols, int newlines, int *xshift, int *yshift, int maxshift)
{
	float *image1_fft;

	/* allocate space for fft */
	if ((image1_fft=(float *)calloc( 1 + newlines*newcols*2, sizeof(float) ))==NULL) {
		fprintf(stderr,"float calloc failed\n");
		return;
	}
	
		
	/* get fft of image1 */
	im_fft(image1, cols, lines, image1_fft, xoff, yoff, newcols, newlines);

	/* register images */
	fft_image_reg_core(	image0_fft,
				image1_fft,
				newcols,
				newlines,
				xshift,
				yshift,
				maxshift);

	Free(image1_fft);
}

/**********************************************************************************/


void CFFT::clearImageEdges(unsigned char *image1, int cols, int lines, int edge)
{
	int line, col;
	Uchar *imptr;

	imptr=image1;

	//clear top
	for (line=0;line<edge;line++)
		for (col=0;col<cols;col++)
			*imptr++=0;

	//celar edges
	for (line=edge;line<lines-edge;line++) {

		//left edge
		imptr=cols*line + image1;
		for (col=0;col<edge;col++)
			*imptr++=0;

		//right edge
		imptr=cols*line + image1 + cols-edge;
		for (col=0;col<edge;col++)
			*imptr++=0;
	}

	//clear bottom
	for (line=lines-edge;line<lines;line++) {
		imptr=cols*line +image1;
		for (col=0;col<cols;col++)
			*imptr++=0;
	}
}

/**********************************************************************************/
void CFFT::fft_power (Uchar *image, int imcols, int imlines, float *result, int resx, int resy, int rescols, int resline)
{
	float *Reptr, *Imptr, *Pwptr;
	int i, j;
	float *FFTresult;

				//get FFT
	FFTresult = (float *) malloc (sizeof (float) * (1 + resline * rescols * 2));	
	im_fft (image, imcols, imlines, FFTresult, resx, resy, rescols, resline);
	
				//get power spectrum,  P(i,j) = Re(i,j) * Re(i,j) + Im(i,j) * Im(i,j)
	Pwptr = result + 1;
	Reptr = FFTresult + 1;
	Imptr = Reptr + 1;
		for (i = 0; i < resline; i++) {
			for (j = 0; j < rescols; j++) {
					*Pwptr = (*Imptr * *Imptr) + (*Reptr * *Reptr);
					Reptr++; Reptr++;
					Imptr++; Imptr++;
					Pwptr++;
					}
			}

	free ((void *) FFTresult);
}