/*
 *		Filename	ImageProcessing.cpp
 *
 *		Image processing functions for microscope calibration
 *
 *		Author			A Rourke
 *		Date		     4 January 2001
 *		Copyright	  Applied Imaging International
 *
 * Mods
 *
 *	AR 22Jan04	Added crosshairs_cog to determine centre of cross hair using part graye scale
 *				image. This should be more reliable and accurate than using the binary image.
 *				test_for_cross() may also be tuned to look for the object closest to the centre of the image,
 *				or the largest object in the image. The largest object option is for the large cross-hair,
 *				as used for objective offset and XY scale measurements. The closest to centre option is for
 *				locating cross-hairs used for ideal coordinate calibration. 
 * AR 5Nov2003	Additions for use with large format camera (2Kx2K pixels)and 1.25x lens
 */


//#include "stdafx.h"
#include <windows.h>
#include  <stdio.h>
#include  <math.h>
#include <stdlib.h>
#include <assert.h>

#include "ImageProcessing.h"
#include "FastFourierTransform.h"
#include "MicroErrors.h"
#include "woolz.h"
#include "CommonDefs.h"

static int	dumpresults = 0;
static int 	frameno = 0;



#define MAXSEGWIDTH	320 		/*to cope with x10,x20 and x40 objectives, lets not forget 60x and 100x either */
#define MINSEGWIDTH	0			/*to cope with x1.25 objective*/
#define BINWIDTH	16
#define EPSILON		(1e-6)
#define	FRAGSIZE	128
#define	MAXSEED		2000000			//increase seed points for large format images
#define	MARKED_VALUE	254
#define	BINARY_VALUE	255
#define	BLANK_VALUE		0
#define	DELETE_VALUE	253

				//spatial calibration with large format images and low power (1.25) lenses
				//may allow more than one calibration pattern to be seen
				//however, the features of interest should always be > 10 pixels.
#define MINWIDTH		10		//minimum acceptable width of feature in pixels 
#define MINHEIGHT		10		//minumum acceptable height of feature in pixels

#define MAXI(n,m)	((n) < (m) ? (m) : (n))


//Constructor
CImageProcessing::CImageProcessing()
{
}

//Destructor
CImageProcessing::~CImageProcessing()
{
}

/*
 *		I M A G E B U F F E R
 *			-- create space to hold an image and its associated data
 */
void CImageProcessing::imagebuffer (BYTE *imagebuf, UINT width, UINT height)
{

	image = image_create(width,height);
	memcpy (image->data, imagebuf, width * height);

//#ifdef _DEBUG
image_dump (image,"C:\\GRAY_IMAGE.raw");	//save a copy of the image
//#endif
}

/*
 *		T E S T _ F O R _ C R O S S 
 *			-- test image for the existence of  a cross-hair
 */
int CImageProcessing::test_for_cross (double *cx, double *cy, double *m, double *c, BOOL closest_to_centre, int min_arm_length)
{
	int status,threshold;
	Image *firstobj, *obj1, *obj2;
	int icx, icy;
	Image *grey_image;

	status = erNO_ERRORS;

			//make a copy of the grey level image
	grey_image = image_dup (image);

				/*threshold image*/
	threshold = background_threshold(image);
	if ( threshold ) {
			/*create binary image*/
		image_single_threshold_apply (image,threshold);

				//find binary objects in the image
		firstobj  = (Image *) find_objects (image, 2 * image->width * image->height,100, (image->width * image->height));
		if (firstobj == NULL)
			return  erIMAGE_FAILED;		//found no objects

		if (closest_to_centre) {
				//find the object closest to the centre
			obj1 = obj2 = firstobj;
			icx = image->width / 2;
			icy = image->height / 2;
			double r2 = (icx * icx + icy * icy);
			while (obj1) {
				int dx = (int) (fabs (obj1->cgx - icx));
				int dy = (int) (fabs (obj1->cgy - icy));
				double r1 = sqrt ((double)(dx * dx + dy * dy + 1));
				if (r1 < r2){
					obj2 = obj1;
					r2 = r1;
					}
				obj1 = obj1->next;
				}
			}
		else {
				//find the largest object, this should be the crosshair
			obj1 = obj2 = firstobj;
			while (obj1) {
				if (obj1->area > obj2->area){
					obj2 = obj1;
					}
				obj1 = obj1->next;
				}
		}

			//let the crosshair be the only thing in the image 
		image_clear (image);
		image_copy_region (obj2, image, 0,0,obj2->x,obj2->y,obj2->width-1,obj2->height-1);
		image->cgx = obj2->cgx;
		image->cgy = obj2->cgy;
	 
//#ifdef _DEBUG
image_dump (image,"C:\\SelectedObject.raw");
//#endif
					//tidy up
		obj1 = firstobj;
		while (obj1) {
				obj2 = obj1;
				obj1 = obj1->next;
				image_delete (obj2);
				}

			//use the image as a mask to get the grey scale cross
		unsigned long size = image->width * image->height;
		BYTE *pgrey = grey_image->data;
		BYTE *pbinary = image->data;
		while (size) {
			if (*pbinary){
				if (*pgrey == 0)
					*pbinary = *pgrey + 1; //in case object is black (0)
				else
					*pbinary = *pgrey;
				}
			pbinary++;
			pgrey++;
			size--;
			}

			/*locate (grey_scale) crosshair in image*/
		status = (int) crosshairs_cog (image,cx,cy,m,c,min_arm_length);
		}
	else {
		status = erIMAGE_FAILED;
		}

	return (status);	
}

/*
 *		B A C K G R O U N D _ T H R E S H O L D
 *			-- determine a background threshold
 *			-- from an image's histogram 
 */
int CImageProcessing::background_threshold(Image *image)
{
	int threshold;
	Image *image2;

				//create a temporary image
	image2 = image_create (image->width, image->height);

				//invert image for woolz functions
	image_invert_data (image, image2);	

				//background subtraction, remove any uneveness in the background
	free ((void *) image->data);
	image->data = fsExtract (image2->data, image2->width, image2->height, 100, 2);

				//inverted background threshold
	threshold = 255 - BackgroundThresh (image->data, image->width, image->height);

				//put image back the way it was
	image_invert_data (image, image);

				//delete temporary image
	image_delete (image2);

	return (threshold);
}


/*
 *		A D D P O I N T
 *			-- add image points for finding arms of a cross-hair
 */
void CImageProcessing::addPoint(UINT x, UINT y)
{
	//assert(npoints < POINTS);
	if (npoints > image->width * image->height)
		return;

	points[npoints].x = x;
	points[npoints].y = y;
	npoints += 1;
}

/*
 *		L I N E F I T
 *			-- fit the points to find the best straight line
 *			-- to identify the arms of a cross-hair
 */
int CImageProcessing::linefit(Point *points, UINT n, double *slope, double *incpt)
{
	double Sxx, Sxy, Sx, Sy;
	double divisor;
	UINT x, y, i;

	assert(n > 1);

	Sxx = Sxy = Sx = Sy = 0;
	for (i = 0; i < n; i += 1) {
		x = points[i].x, y = points[i].y;
		Sxx += x * x;
		Sxy += x * y;
		Sx += (double) x;
		Sy += (double) y;
		}

	divisor = n * Sxx - Sx * Sx;
	if (divisor < EPSILON) {
		*incpt = x;
		return -1;
		}

	*slope = (n * Sxy - Sx * Sy) / divisor;
	*incpt = (Sy * Sxx - Sx * Sxy) / divisor;
	
	return 0;
}

/*
 *	C R O S S H A I R S
 *		-- locate crosshairs in image
 *		-- Accept as part of cross artifacts
 *		-- between MINSEGWIDTH and MAXSEGWIDTH. 
 *		-- Determine the coordinates of the centres 
 *		-- of these artifacts and use to determine the
 *		-- equation of the line, defining the components
 *		-- of the cross.
 */
int CImageProcessing::crosshairs(Image *image, double *cx, double *cy, int min_arm_length)
{
	UINT width = image->width;
	UINT height = image->height;
	UINT runstart, rlength;
	UINT midval, maxval, maxbin, total;
	BOOL foreground;
	UINT x, y, i, np;
	double mh, mv, ch, cv;
	BYTE *iptr;
	BOOL horizontal, vertical;


	UINT *histo;	
	UINT XBINS, YBINS, MAXBINS, MINPOINTS;

	XBINS = (image->width + BINWIDTH -1) / BINWIDTH;
	YBINS = (image->height + BINWIDTH - 1) / BINWIDTH;
	MINPOINTS = min_arm_length; //image->height / 4;
	MAXBINS = MAXI (XBINS,YBINS); 
	histo = (UINT *) malloc (MAXBINS * sizeof (UINT));	

	horizontal = vertical = FALSE;
	if (image == NULL)
		return erIMAGE_FAILED;
	points = (Point *) malloc (image->width * image->height * sizeof (Point));	//create array equal in size to image

	npoints = 0;

			/*Scan image in x direction for possible vertical crosshair segments. */
	for (i = 0; i < XBINS; i += 1)
		histo[i] = 0;

	//maskedge (image,1,0);	/*make sure the periphery is clear*/

	for (y = 0; y < height; y += 1) {
		iptr = image->data + y * width;
		foreground = FALSE;
		for (x = 0; x < width /*- MAXSEGWIDTH */; x += 1) {
			if (*iptr++) {
				if (!foreground) {
					runstart = x;
					foreground = TRUE;
					}
				}
			else
				if (foreground) {
					rlength = x - runstart;
					if (rlength >= MINSEGWIDTH && rlength <= MAXSEGWIDTH) {
						midval = (x + runstart) / 2;
						addPoint(midval, y);
						histo[midval / BINWIDTH] += 1;
						}
					foreground = FALSE;
					}
			}
		}

	maxval = 0;
	for (i = 0; i < XBINS; i += 1)
		if (histo[i] > maxval) {
			maxval = histo[i];
			maxbin = i;
			}

	if (maxval) {
		midval = maxval * (BINWIDTH * (2 * maxbin + 1)) / 2;
		total = maxval;
		if (maxbin > 0) {
			midval += histo[maxbin - 1] * (BINWIDTH * (2 * maxbin - 1)) / 2; 
			total += histo[maxbin - 1];
			}
		if (maxbin < XBINS - 1) {
			midval += histo[maxbin + 1] * (BINWIDTH * (2 * maxbin + 3)) / 2; 
			total += histo[maxbin + 1];
			}
		midval = midval / total;
	
			/* When selecting suitable points, swap the x and y values so that the
			 * least squares fit for the vertical component is done with y as the
		  	 * primary parameter.
			 */

		np = 0;
		for (i = 0; i < npoints; i += 1)
			if (abs(int (points[i].x - midval)) < MAXSEGWIDTH) {
				x = points[i].x, y = points[i].y;
				points[np].x = y, points[np].y = x;
				np += 1;
				}

		if (np >= MINPOINTS) {
			if (linefit(points, np, &mv, &cv) != -1) {
				vertical = TRUE;
				mv = 1 / mv; cv = -cv * mv;
				}
			}
		}

		/* Scan image in y direction for possible horizontal crosshair segments. */
	npoints = 0;
	for (i = 0; i < YBINS; i += 1)
		histo[i] = 0;

	for (x = 0; x < width; x += 1) {
		iptr = image->data + x;
		foreground = FALSE;

		for (y = 0; y < height /*- MAXSEGWIDTH */; y += 1) {
			assert(iptr < image->data + width * height);
			if (*iptr) {
				if (!foreground) {
					runstart = y;
					foreground = TRUE;
					}
				}
			else
				if (foreground) {
					rlength = y - runstart;
					if (rlength >= MINSEGWIDTH && rlength <= MAXSEGWIDTH) {
						midval = (y + runstart) / 2;
						addPoint(x, midval);
						histo[midval / BINWIDTH] += 1;
						}
					else {
						DEBUGSTRING ("HORIZONTAL WRONG LENGTH\n");
						}

					foreground = FALSE;
					}
				iptr += width;
				}
		}

	maxval = 0;
	for (i = 0; i < YBINS; i += 1)
		if (histo[i] > maxval) {
			maxval = histo[i];
			maxbin = i;
			}
	if (maxval) {
		midval = maxval * (BINWIDTH * (2 * maxbin + 1)) / 2;
		total = maxval;
		if (maxbin > 0) {
			midval += histo[maxbin - 1] * (BINWIDTH * (2 * maxbin - 1)) / 2; 
			total += histo[maxbin - 1];
			}
		if (maxbin < XBINS - 1) {
			midval += histo[maxbin + 1] * (BINWIDTH * (2 * maxbin + 3)) / 2; 
			total += histo[maxbin + 1];
			}
		midval = midval / total;

		np = 0;
		for (i = 0; i < npoints; i += 1)
			if (abs(int (points[i].y - midval)) < 16 /*MAXSEGWIDTH*/){
				points[np++] = points[i];
				}

		if (np >= MINPOINTS)
 			if (linefit(points, np, &mh, &ch) != -1)
				horizontal = TRUE;
		}

	free((void *) points);
	free ((void *) histo);

	if (horizontal && vertical) {
		if (fabs(mh - mv) < EPSILON) {
			return erIMAGE_FAILED;
			}

			/* Calculate centrepoint as intersection of two lines. */
		*cx = (cv - ch) / (mh - mv);
		*cy = (mh * cv - mv * ch) / (mh - mv);
		return erNO_ERRORS;
		}

	return erIMAGE_FAILED;
} 

/*
 *	F I N D C R O S S
 *		-- acquire image for testing for presence of crosshairs
 */
int CImageProcessing::findCross(double *cx, double *cy, double *m, double *c,BOOL closest_to_centre,int min_arm_length)
{
	int status;
			/*see if image contains a cross-hair*/
	status = (int) test_for_cross (cx,cy, m,c,closest_to_centre, min_arm_length);

	return (status);
}

/*
 *	I M A G E _ S I N G L E _ T H R E S H O L D _ A P P L Y
 *		-- apply threshold to grey-level
 *		-- image to create a binary image
 *		-- by removing pixels with values
 *		-- which lie below a single threshold
 */
void CImageProcessing::image_single_threshold_apply(Image *image, int threshold)
{
	BYTE *imptr,*imend;

	imptr = image->data;
	imend = imptr + image->width * image->height;

//#ifdef _DEBUG
image_dump (image,"C:\\gray_image1.raw");
//#endif

	while (imptr < imend) {
		if (*imptr > threshold)
			*imptr = 0;	
		else
			*imptr = 255;
		imptr++;
		}

//#ifdef _DEBUG
image_dump (image,"C:\\binary_image.raw");
//#endif

}

/*
 *		I M A G E _ E R O S I ON
 *			-- erode binary image, removing small noise points
 */
void CImageProcessing::image_erosion (Image *image, int half_width)
{
	Image *image2;

//#ifdef _DEBUG
image_dump (image,"C:\\PreEroded.raw");
//#endif

	image2 = image_create (image->width, image->height);
	image_copy_data (image, image2);
	free ((void *) image->data);
//	image->data = fsMin (image2->data, image2->width, image2->height, half_width);
	image->data = fsOpen (image2->data, image->width, image->height, half_width);

//#ifdef _DEBUG
image_dump (image,"C:\\PostEroded.raw");
//#endif

	image_delete (image2);

}

/*
 *		I M A G E _ H I S T O  
 *			-- build the histogram for the image
 */
void CImageProcessing::image_histo(Image *image, int *histo)
{
	int 	size, i;
	unsigned char *iptr;

	for (i = 0; i < 256; i += 1)
		histo[i] = 0;
	size = image->height * image->width;
	iptr = image->data;
	while (size--)
		histo[*iptr++]++;
}

/*
 *		I M A G E _ C O P Y _ D A T A
 *			-- copy the data from one Image into another
 *			-- the data area must be the same size;
 */
void CImageProcessing::image_copy_data(Image *from, Image *to)
{
	int 	size;

	size = to->width * to->height;	
	if ((from->width * from->height) != size)
		return;

	if (to->data)
		memcpy(to->data,from->data,size);

}

/*
 *		I M A G E _ I N V E R T _ D A T A
 *			-- copy the data src to dst and invert 
 *			-- the data area must be the same size;
 */
void CImageProcessing::image_invert_data(Image *from, Image *to)
{
	int 	size;
	unsigned char *psrc, *pdst;

	if (to->data == NULL || from->data == NULL)
		return;

	psrc = from->data;
	pdst = to->data;
	size = to->width * to->height;
	while (size) {
			*pdst = 255 - *psrc;
			pdst++;
			psrc++;
			size--;
			} 	

}

/*
 *	I M A G E _ C O P Y _ R E G I O N  --
 *
 *	Copy data between images. Images do not need to be same
 *	size. If source image is too small to cover whole copy region, destination
 *	image will be filled with '0' within the xsize/ysize copy region.
 *	Note that the rest of the target image will be untouched.
 */
void CImageProcessing::image_copy_region(Image *from, Image *to, int fx, int fy, int tx, int ty, int xsize, int ysize)
{
	unsigned char	*fptr, *tptr;
	int 	xshort = 0, yshort = 0, prex = 0;
	int fred = 0;

	assert(from && to);
	assert(from->data && to->data);

/*
 * if copy to a neg position, shift everyting over a bit
 */
	if (tx < 0) {
		fx -= tx;
		tx = 0;
	}
	if (ty < 0) {
		fy -= ty;
		ty = 0;
	}
/*
 * If copy too big for destination
 */
	if (tx + xsize > to->width) {
		/* printf("txsize\n"); */
		fred = 1;
		xsize = to->width - tx;
	}
	if (ty + ysize > to->height) {
		/* printf("tysize\n"); */
		fred = 1;
		ysize = to->height - ty;
	}

	if (fx + xsize > from->width) {
		xshort = (fx + xsize) - from->width;
		xsize = from->width - fx;
		/* printf("fxsize short %d\n",short); */
	}
	if (fy + ysize > from->height) {
		/* printf("fysize\n"); */
		yshort = (fy + ysize) - from->height;
		ysize = from->height - fy;
	}
/*
	if (fred)
		printf("imagecopy: from %d %d, to %d %d, size %d %d, tsize %d %d\n",
			fx,fy,tx,ty,xsize,ysize,to->width, to->height);
*/
/*
 *	Set up basic copy pointers
 */
	fptr = from->data + fy * from->width + fx;
	tptr = to->data + ty * to->width + tx;
/*
 *	Handle -ve 'from' initial line
 */
	for ( ; (fy < 0) && (ysize-- > 0); fy++) {
		memset(tptr, 0, xsize+xshort);
		tptr += to->width;
		fptr += from->width;
	}
/*
 *	Handle -ve 'from' initial col
 */
	if (fx < 0) {
		printf("-ve from x-pos %d\n",fx);
		prex = -fx;
		if (prex > xsize) {
			prex = xsize;
			xsize = 0;
		} else
			xsize -= prex;
		tptr += prex;
		fptr += prex;
	}
/*
 *	Do the copy
 */
	for ( ; ysize-- > 0; ) {
		if (prex)
			memset(tptr-prex, 0, prex);
		memcpy(tptr, fptr, xsize);
		if (xshort)
			memset(tptr+xsize, 0, xshort);
		fptr += from->width;
		tptr += to->width;
	}
/*
 *	Fill in extra lines at end
 */
	for ( ; yshort-- > 0; ) {
		memset(tptr-prex, 0, prex+xsize+xshort);
		tptr += to->width;
	}
}

/*
 *		I M A G E _ M A K E  
 *			--  given data, put into an image struct.
 */
Image * CImageProcessing::image_make(int width, int height, unsigned char *data)
{
	Image	*newImage;

	assert(width && height);

	newImage = (Image *) calloc(sizeof(Image),1);
	newImage->width = width;
	newImage->height = height;
	newImage->data = data;

	return(newImage);
}

/*
 *		I M A G E _ C R E A T E  
 *			--  make an image of given size
 *			-- the data is not initialised
 */
Image * CImageProcessing::image_create(int width, int height)
{
	Image	*newImage;

	assert(width && height);

	newImage = (Image *) calloc(sizeof(Image),1);
	newImage->width = width;
	newImage->height = height;
	if (width && height) 
		newImage->data = (unsigned char *) malloc(width * height);
	else
		newImage->data = NULL;

	return(newImage);
}

/*
 *		I M A G E _ C R E A T E B L A N K
 *			--  make an image of given size and set it clear
 *			-- the data is initialised to 0
 */
Image * CImageProcessing::image_createblank(int width, int height)
{
	Image	*newImage;

	assert(width && height);

	newImage = (Image *) calloc(sizeof(Image),1);
	newImage->width = width;
	newImage->height = height;
	if (width && height)
		newImage->data = (unsigned char *) calloc(width, height);
	else
		newImage->data = NULL;
	return(newImage);
}

/*
 *		I M A G E _ D U P  
 *			--  make a duplicate of an image
 */
Image * CImageProcessing::image_dup(Image *image)
{
	Image *newImage;
	int 	size;

	if (image == NULL)
		return(NULL);

	newImage = (Image *) malloc(sizeof(Image));
	*newImage = *image;
    
	if (image->data) {
		size = image->width * image->height;
		newImage->data = (unsigned char *) malloc(size);
		memcpy(newImage->data,image->data,size);
	}

	return(newImage);
}

/*
 *		I M A G E _ D E L E T E  
 *			--  free space allocated for image
 */
void CImageProcessing::image_delete(Image *image)
{
	if (image) {
		if (image->data)
			free(image->data);
		free(image);
	}
}

/*
 *	I M A G E _ C L E A R  --  Clear an image
 *
 */
void CImageProcessing::image_clear(Image *image)
{
	assert(image && image->data);

	memset(image->data, 0, image->width * image->height);
}

/*
 *	I M A G E _ C L E A R _ R E G I O N  --
 *
 */
void CImageProcessing::image_clear_region(Image *image, int x, int y, int xsize, int ysize)
{
	unsigned char *iptr;

	assert(image && image->data);

	iptr = image->data + y * image->width + x;
	for ( ; ysize--; y++) {
		memset(iptr, 0, xsize);
		iptr += image->width;
	}
}

/*
 *		I M A G E _ W R I T E 
 *			--  write image data to file.
 *			--	returns -1 if failed, 0 if OK
 */
int CImageProcessing::image_write(Image *image, FILE *file)
{
	if ((int) (fwrite(image->data, image->width, image->height, file)) != image->height) {
		printf("image_write: bad write\n");
		return(-1);
	}
	return(0);
}

/*
 *		D A T A _ D U M P 
 *			--write data to given file
 */
void CImageProcessing::data_dump(char *data, char *filename)
{
	FILE	*fp;

	if (fp = fopen(filename,"a")) {
		fprintf (fp,"%s\n",data);
		fclose (fp);
		}
}

/*
 *		I M A G E _ D U M P  
 *			--  write image to given file name
 */
void CImageProcessing::image_dump(Image *image, char *filename)
{
	FILE	*fp;

	if (fp = fopen(filename,"wb")) {
		image_write(image,fp);
		fclose(fp);
	}

}

/*
 *		I M A G E _ R E A D  
 *			--  read image from given file name
 */
Image * CImageProcessing::image_read(FILE *file, int x, int y)
{
	Image	*image;
	image = image_create(x, y);
	if ((int)(fread(image->data, x, y, file)) != y)
		printf("image_read: bad read\n");
	return(image);
}

/*
 *		I M A G E _ S C A L E 
 *			-- determine size of image pixels using centre of gravity
 *			-- of image components of known physical size
 */
int CImageProcessing::image_scale (double *xscale, double *yscale)
{
	int i,threshold,status,sumx,sumy;
	int width[256], height[256], Mode_Width, Mode_Height;
	int xs, ys, minx, miny;
	double dcgx, dcgy,cgx,cgy;
	double cgx1, cgy1, cgx2, cgy2;
	Image *firstobj, *obj1, *obj2, *objX, *objY;
	int Max_Width, Min_Width, Max_Height, Min_Height;
	int nobjects_x, nobjects_y, rejects, nobjects, nvar;
	int half_width;

	status = erNO_ERRORS;

				//get image threshold
	threshold = background_threshold(image);
	if (threshold  == 0) {
		status = erIMAGE_FAILED;
		return status;
		}
			//create binary image*/
	image_single_threshold_apply (image,threshold);


			//remove noise from the image, accept only objects > 1 pixel and < 1/8 of the image size
			//get image calibration pattern features
	firstobj  = (Image *) find_objects (image, 2 * image->width * image->height,1, (image->width * image->height)/4);
	if (firstobj == NULL)
		return  erIMAGE_FAILED;		//found no objects

			//find the typical height and width of the calibration pattern features
	for (i = 0; i < 256; i++) 
		width[i] = height[i] = 0;
	obj1 = firstobj;
	while (obj1 != NULL) {
		if (obj1->width < 256)
			width[obj1->width]++;
		if (obj1->height < 256)
			height[obj1->height]++;
		obj1 = obj1->next;
		}

	Mode_Width = Mode_Height = 0;
	for (i = MINWIDTH; i < 256; i++) {
		if (width[i] > width[Mode_Width])
			Mode_Width = i;
		if (height[i] > height[Mode_Height])
			Mode_Height = i;
		}

	obj1 = firstobj;
	while (obj1) {
			obj2 = obj1;
			obj1 = obj1->next;
			image_delete (obj2);
			}

	if (Mode_Width == 0 || Mode_Height == 0)
		return  erIMAGE_FAILED;

			//estimate a reasonable filter size
	if (Mode_Width < Mode_Height) 
		half_width = Mode_Width / 3 - 2;
	else
		half_width = Mode_Height / 3 - 2;
	if (half_width < 2)
		half_width = 2;
	if (half_width > 15)
		half_width = 15;
				//remove noise;
	image_erosion (image,half_width);
		

			//get image calibration pattern features from the cleaned image
	firstobj  = (Image *) find_objects (image,2 * image->width * image->height,1, (image->width * image->height)/4);
	if (firstobj == NULL)
		return  erIMAGE_FAILED;		//found no objects

			//find the typical height and width of the calibration pattern features
	for (i = 0; i < 256; i++) 
		width[i] = height[i] = 0;
	obj1 = firstobj;
	while (obj1 != NULL) {
		if (obj1->width < 256)
			width[obj1->width]++;
		if (obj1->height < 256)
			height[obj1->height]++;
		obj1 = obj1->next;
		}

	Mode_Width = Mode_Height = 0;
	for (i = MINWIDTH; i < 256; i++) {
		if (width[i] > width[Mode_Width])
			Mode_Width = i;
		if (height[i] > height[Mode_Height])
			Mode_Height = i;
		}


	obj1 = firstobj;
	dcgx = 0.0;
	dcgy = 0.0;
	sumx = 0;
	sumy = 0;
			
			//remove noisey features and features only partially in the field of view
					//accept only objects (image features) within (max, min)  range
	Max_Width = Mode_Width + (Mode_Width / 10);
	Min_Width = Mode_Width - (Mode_Width / 10);
	Max_Height = Mode_Height + (Mode_Height / 10);
	Min_Height = Mode_Height - (Mode_Width / 10);

			//parameters for testing 'goodness' of image and analysis
	nobjects_x = nobjects_y = 0;
	nobjects = rejects = 0;
	nvar = 0;

	while (obj1 != NULL) {

			if (obj1->width >= Min_Width && obj1->width <= Max_Width &&
				 obj1->height >= Min_Height && obj1->height <= Max_Height) {

				nobjects++;		//number of features used in the analysis

				obj2 = firstobj; 
				minx = image->width;
				miny = image->height;
				cgx = obj1->cgx;
				cgy = obj1->cgy;
				objX = objY = NULL;

							//find the nearest neighbour object and determine
							//the difference in position of their centroids
				while (obj2 != NULL) {

					if (obj1 != obj2) {
							if (obj2->width >= Min_Width && obj2->width <= Max_Width &&
									 obj2->height >= Min_Height && obj2->height <= Max_Height) {

							xs = abs (obj2->x - obj1->x);
							ys = abs (obj2->y - obj1->y);

							if (xs < minx && ys < 10) {
								minx = xs;
								objX = obj2;
								}
							if (ys < miny && xs < 10) {
								miny = ys;
								objY = obj2;
								}
							}
						}
					obj2 = obj2->next;
					}	
												//get average distance between centroids
				if (objX) {
					cgx1 = fabs (objX->cgx - obj1->cgx);
					cgy1 = fabs (objX->cgy - obj1->cgy);
					dcgx = dcgx + sqrt (cgx1 * cgx1 + cgy1 * cgy1);
					sumx++;
					}
				if (objY) {
					cgx2 = fabs (objY->cgx - obj1->cgx);
					cgy2 = fabs (objY->cgy - obj1->cgy);
					dcgy = dcgy + sqrt (cgx2 * cgx2 + cgy2 * cgy2);	
					sumy++;
					}
			}
		else if (obj1->width > 2 * Max_Width || obj1->height > 2 * Max_Height) {
				rejects++;		//very large blobs indicate poor image segmentation
				}

		obj1 = obj1->next;
		}

	if (sumx >= 1 && sumy >= 1) {
					//determine the average distance between centroids	
		*xscale = dcgx / sumx;
		*yscale = dcgy / sumy;

					//how many image calibration features should we be able to see
		nobjects_x = (int) (image->width / *xscale + 0.5);	//max number of features along x
		nobjects_y = (int) (image->height / *yscale + 0.5);		//max number of features along y
		nvar = 2 * nobjects_x + 2 * nobjects_y - 4;		//number of features around the perimeter
		}
	else
		status = erIMAGE_FAILED;

					//tests for a 'good' calibration
	int max_rejects = (nobjects_x * nobjects_y) / 10;
	if (max_rejects < 3)
		max_rejects = 3;
	if (rejects > max_rejects ) 
		status = erIMAGE_FAILED;		//too many large objects
	else if (nobjects < nobjects_x * nobjects_y/2)
		status = erIMAGE_FAILED;		//too few objects in analysis	

			//tidy up
	obj1 = firstobj;
	while (obj1) {
			obj2 = obj1;
			obj1 = obj1->next;
			image_delete (obj2);
			}

 	return (status);	
}


void CImageProcessing::seed(SeedList *seedlist, int x, int y)
{
	SeedPoint *seedptr;

	if (seedlist->seedcount < MAXSEED) {
		seedptr = seedlist->seeddata + seedlist->seedcount;
		seedptr->x = x;
		seedptr->y = y;
		seedlist->seedcount++;
		}
	else
		seedlist->seedslost++;
}

int CImageProcessing::getseed(SeedList *seedlist, int *x, int *y)
{
	SeedPoint *seedptr;

	if (!seedlist->seeddata)
		return(0);
	if (seedlist->seedcount == 0)
		return(0);
		
	seedptr = seedlist->seeddata + --seedlist->seedcount;
	*x = seedptr->x;
	*y = seedptr->y;

	return(1);
}

int CImageProcessing::seed(Image *Iptr, SeedList *seedlist, int x, int y)
{
	SeedPoint *seedptr;
	BYTE pix;

	if  (y < 1 || y >= Iptr->height-1 || x < 1 || x >= Iptr->width-1)
		return (FALSE);

	if ( (pix = *(Iptr->data + y * Iptr->width + x)) == 0)
		return (FALSE);
	if ((pix = *(Iptr->data + (y - 1) * Iptr->width + (x -1)) ) == 0)
		return (FALSE);
	if ((pix =  *(Iptr->data + (y - 1) * Iptr->width + x )) == 0)
		return (FALSE);
	if ( (pix =  *(Iptr->data + (y -1) * Iptr->width + (x + 1)) ) == 0)
		return (FALSE);
	if ( (pix = *(Iptr->data + y * Iptr->width + (x - 1) )) == 0)
		return (FALSE);
	if ((pix = *(Iptr->data + y * Iptr->width + (x + 1))) == 0)
		return (FALSE);
	if ((pix = *(Iptr->data + (y + 1) * Iptr->width + (x - 1)) ) == 0)
		return (FALSE);
	if ((pix = *(Iptr->data + (y + 1) * Iptr->width + x) ) == 0)
		return (FALSE);
	if ((pix = *(Iptr->data + (y + 1) * Iptr->width + (x + 1)) )== 0)
		return (FALSE);

				//the point (x ,y) will only be accepted as a seed point if all its neighbours are non-zero
	
	if (seedlist->seedcount < MAXSEED) {
		seedptr = seedlist->seeddata + seedlist->seedcount;
		seedptr->x = x;
		seedptr->y = y;
		seedlist->seedcount++;
		}
	else
		seedlist->seedslost++;

	return (TRUE);
}

/*
 *		F I N D _ O B J E C T S
 *			-- extract the objects , groups of 8-connected 255-value pixels, from the binary image
 *			-- background == 0
 *			-- also calculates area, length of perimeter, centroid, start point, width and height
 */
Image *CImageProcessing::find_objects (Image *image, int maxmem, int min_area, int max_area)
{
	int 	x, y, xs, ys, sumx, sumy;
	int 	xmin, xmax, ymin, ymax, area, region, memsize;
	unsigned char *iptr, *sptr, *tsptr, *fptr;
	Image	*find, *obj, *lastobj, *firstobj;
	SeedList slist;
	int edge_pt,perimeter;


	assert(image && image->data);

		//set binary image to 0 or 255
	iptr = image->data;
	int size = image->width * image->height;
	while (size) {
			if (*iptr)
				*iptr = 255;
			iptr++;
			size--;
			}

	slist.seeddata = (struct seedpoint *) malloc((MAXSEED+1) * sizeof(struct seedpoint));
	slist.seedcount = slist.seedslost = 0;

	region = 0;
	memsize = 0;
	firstobj = lastobj = NULL;
	find = image_create(image->width, image->height);
	image_clear(find);
	iptr = image->data;
	for (y=0; y<image->height; y++) {
		for (x=0; x<image->width; x++) {
			if (*iptr > DELETE_VALUE) {
				seed(image, &slist, x,y);
				*iptr = DELETE_VALUE;
				xmin = xmax = sumx = x;
				ymin = ymax = sumy = y;
				area = 1;
				perimeter = 0;
			
/*
 *	While we have seed points, scan round (8 connected) for more possible points
 *	Any new ones we remove from the image but remember the max extent.
 */
				while (getseed(&slist, &xs, &ys)) {
					sptr = image->data + ys * image->width + xs;
					fptr = find->data + ys * image->width + xs;
					*fptr = 255;
					edge_pt = FALSE;
/*
 *	Previous line, 3 points
 */
					if (ys > 0) {
						tsptr = sptr - image->width;
						if (xs > 0) {
							if (*(tsptr-1) == BINARY_VALUE) {		/* New point - above, left ? */
								if (seed (image, &slist, xs-1, ys-1) ){ ;//seed(&slist, xs-1, ys-1);							
									area++;
									sumx += (xs -1);
									sumy += (ys -1);
									if (xs-1 < xmin)
										xmin = xs-1;
									if (ys-1 < ymin)
										ymin = ys-1;
									}
								*(tsptr-1) = MARKED_VALUE;
								}
							else if (*(tsptr-1) == BLANK_VALUE) 
								edge_pt = TRUE;
						}
		    
						if (*(tsptr) == BINARY_VALUE) {			/* New point - above ? */
							if (seed (image, &slist, xs, ys-1) ) { ;//seed(&slist, xs-1, ys-1);							
									area++;
									sumx += xs;
									sumy += (ys -1);
									if (ys-1 < ymin)
										ymin = ys-1;
									}
							*(tsptr) = MARKED_VALUE;
							}
		    			else if (*(tsptr) == BLANK_VALUE) 
							edge_pt = TRUE;

						if (xs < (image->width-1)) {
							if (*(tsptr+1) == BINARY_VALUE) {		/* New point - above, right ? */
								if (seed (image, &slist, xs+1, ys-1) ){ ;//seed(&slist, xs-1, ys-1);							
									area++;
									sumx += (xs +1);
									sumy += (ys -1);
									if (xs+1 > xmax)
										xmax = xs+1;
									if (ys-1 < ymin)
										ymin = ys-1;
									}
								*(tsptr+1) = MARKED_VALUE;
								}
						else if (*(tsptr+1) == BLANK_VALUE) 
							edge_pt = TRUE;
						}
					}
/*
 *	current line - 2 points
 */

					if (xs > 0) {
						if (*(sptr-1) == BINARY_VALUE) {		/* New point - left ? */
								if (seed (image, &slist, xs-1, ys) ){ ;//seed(&slist, xs-1, ys-1);							
									area++;
									sumx += (xs -1);
									sumy += ys;	
									if (xs-1 < xmin)
										xmin = xs-1;
									}
							*(sptr-1) = MARKED_VALUE;
							}
						else if (*(sptr-1) == BLANK_VALUE) 
							edge_pt = TRUE;

						}
		    
					if (xs < (image->width-1)) {
						if (*(sptr+1) == BINARY_VALUE) {		/* New point - right ? */
								if (seed (image, &slist, xs+1, ys) ){ ;//seed(&slist, xs-1, ys-1);							
									area++;
									sumx += (xs +1);
									sumy += ys;
									if (xs+1 > xmax)
										xmax = xs+1;
									}	
							*(sptr+1) = MARKED_VALUE;
							if (xs+1 > xmax)
								xmax = xs+1;
							}
						else if (*(sptr+1) == BLANK_VALUE) 
							edge_pt = TRUE;

						}
/*
 *	Next line - 3 points
 */
					if (ys < (image->height-1)) {
						tsptr = sptr + image->width;
						if (xs > 0) {
							if (*(tsptr-1) == BINARY_VALUE) {		/* New point - below, left */
								if (seed (image, &slist, xs-1, ys+1)) { ;//seed(&slist, xs-1, ys-1);							
									area++;
									sumx += (xs -1);
									sumy += (ys + 1);
									if (xs-1 < xmin)
										xmin = xs-1;
									if (ys+1 > ymax)
										ymax = ys+1;
									}
								*(tsptr-1) = MARKED_VALUE;
								}
							else if (*(tsptr-1) == BLANK_VALUE) 
								edge_pt = TRUE;

							}
					    
						if (*(tsptr) == BINARY_VALUE) {		/* New point - below */
								if (seed (image, &slist, xs, ys+1)) { ;//seed(&slist, xs-1, ys-1);							
									area++;
									sumx += xs;
									sumy += (ys + 1);
									if (ys+1 > ymax)
										ymax = ys+1;
									}
							*(tsptr) = MARKED_VALUE;
							}
					 	else if (*(tsptr) == BLANK_VALUE) 
							edge_pt = TRUE;

						if (xs < (image->width-1)) {
							if (*(tsptr+1) == BINARY_VALUE) {		/* New point - below, right */
								if (seed (image, &slist, xs+1, ys+1)) { ;//seed(&slist, xs-1, ys-1);							
									area++;
									sumx += (xs +1);
									sumy += (ys + 1);
									if (xs+1 > xmax)
										xmax = xs+1;
									if (ys+1 > ymax)
										ymax = ys+1;
									}
							*(tsptr+1) = MARKED_VALUE;
								}
							else if (*(tsptr+1) == BLANK_VALUE) 
								edge_pt = TRUE;

							}
						}

				if (edge_pt == TRUE) {
					perimeter++;
					}
				}
/*
 *	Report and do something with result
 */				if (area > min_area && area < max_area) {
					region++;

					obj = image_create(xmax-xmin+1, ymax-ymin+1);
				
					obj->x = xmin; obj->y = ymin;
					obj->width = xmax-xmin+1;
					obj->height = ymax-ymin+1;
					obj->area = obj->max_comp = obj->min_comp = area; 
					obj->components = 1;
					obj->perimeter = perimeter;
					obj->cgx = (double) sumx / (double) area;
					obj->cgy = (double) sumy / (double) area;
					memsize += (xmax-xmin+1) * (ymax-ymin+1);
					if (lastobj)
						lastobj->next = obj;
					else
						firstobj = obj;

					lastobj = obj;
					image_copy_region(find, obj, xmin, ymin, 0, 0, xmax-xmin+1, ymax-ymin+1);
					image_clear_region(find, xmin, ymin, xmax-xmin+1, ymax-ymin+1);
					if (maxmem && (memsize >= maxmem)) {
						printf("image_find: Max memory limit reached (%dk)\n", memsize/1024);
						goto quit;
						}

					}
			
			} /* End new region */
			iptr++;
		}
	}

quit:
	image_delete(find);
	free(slist.seeddata);
	return(firstobj);
}




/*
 *		FFT
 *
 */
int  CImageProcessing::image_fft(double * xscale, double * yscale)
{
	CFFT fft;
	int xoff, yoff, newcols, newlines;
	float *FFTpower, **PwPatch, *Pwptr;

	BlockTestImage();

	fft.get_fft_size (image->width, image->height, &xoff, &yoff, &newcols, &newlines);

xoff = yoff = 0;

	FFTpower = (float *) malloc (sizeof (float) * (1 + newlines * newcols));

	if (FFTpower == NULL)
		return erCALIBRATION_FAILED;

	fft.fft_power (image->data, image->width, image->height, FFTpower,xoff, yoff, newcols, newlines);
	
		//find the peak in the FFT 
	float max=0.0;
	int xmax = 0;
	int ymax = 0;
	int i,j;
	Pwptr = FFTpower + 1; 
	for (j=0; j<newlines ; j++) {
		for (i=0; i<newcols ; i++) {

				if (j > 1 && j < newlines-1 && i > 1 && i < newcols-1) {
					if (*Pwptr > max) {
						 max = *Pwptr; xmax = i ; ymax = j ;
						 }
					}

			Pwptr++ ; 
		}
	}

			//take a n x n patch around the point (xmax,ymax) to find a better
			// approximation to the position of the peak by interpolation
	int n, k,l;
	double x,y, deltax, deltay;
	n = 9;

	PwPatch = (float **) malloc (sizeof (float) * n);

	for (i = 0; i < n; i++) {
		PwPatch[i] = (float *) malloc (sizeof (float) * n);
		}

	Pwptr = FFTpower + 1; // + xmax  + (ymax * newlines);

FILE *fptr = fopen ("FFTdata.txt","w");
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {

					k = ymax -  n / 2 + i;
					if (k < 0)
						k = newlines + k;
					else if (k >= newlines)
						k = k - newlines;
					l = xmax - n / 2 + j;
					if (l < 0)
						l = newcols + l;
					else if (l >= newcols)
						l = l - newcols;

					PwPatch[j][i] = *(Pwptr + k * newcols + l) / max;
fprintf (fptr,"%3.3f ",PwPatch[j][i]);
					}
fprintf (fptr,"\n");
			}
fclose (fptr);

	CubicSplineInterpolation (&PwPatch[0], n, n, &deltax, &deltay);

	x = (double) xmax + deltax;
	y = (double) ymax + deltay;

	if (x > newlines / 2)
		x = (double) newlines - x;
	if (y > newcols / 2)
		y = (double) newcols - y;

	*xscale = fabs ((double) newlines / x);
	*yscale = fabs ((double) newcols / y);

	free ((void *) FFTpower);
	for (i = 0; i < n; i++) {
			free ((void *)PwPatch[i]);
		}
	free ((void *) PwPatch);


	return erNO_ERRORS;
}

/*
 *		C U B I C S P L I N E I N T E R P O L A T I O N
 *				--
 */
int CImageProcessing::CubicSplineInterpolation(float **Patch, int N, int M, double *deltax, double *deltay)
{
	float **ya, **y2a;
	float *xa;
	float bestx, besty, max, yy;
	float yp1, ypn;
	int i, j, NN;

	NN = N + 1;

	ya = (float **) malloc (sizeof (float) * NN + 1);
	y2a = (float **) malloc (sizeof (float) * NN + 1);
	xa = (float *) malloc (sizeof (float) * NN + 1);
	for (j = 0; j < N+2; j++) {
		ya[j] = (float *) malloc (sizeof (float) * NN + 1);
		y2a[j] = (float *) malloc (sizeof (float) * NN + 1);
		}

	for (i = 0; i <= NN; i++) {
		for (j = 0; j <= NN; j++) {
				ya[i][j] = y2a[i][j] = 0.0;
			}
		xa[i] = 0.0;
		}

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
				ya[i+1][j+1] = Patch[i][j];
				}
		xa [i+1] = (float) i  * 100;
		}

	ypn = yp1 = (float) 1.0e30;
	for (i = 1; i <= N; i++) {
		Spline (xa, ya[i], N, yp1, ypn, y2a[i]);
		}

				//find the peak by testing the cubic splines
	int start, finish;

	start = (N / 2 - 1) * 100;
	finish = (N / 2 + 1) * 100;
	max = 0.0;
	for (i = start; i < finish; i++) {
		for (j = start; j < finish; j++) {
			Splint2(xa, xa, ya,y2a,N, N, (float) i , (float) j, &yy);
			if (yy > max) {
				bestx = (float) i;
				besty = (float) j;
				max = yy;
				}
			}
		}

	int startx = (int) (bestx - 10) * 10;
	int finishx = (int) (bestx + 10) * 10;
	int starty = (int) (besty - 10) * 10;
	int finishy = (int) (besty + 10) * 10;
	max = 0.0;
	for (i = startx; i < finishx; i++) {
		for (j = starty; j < finishy; j++) {
			Splint2(xa, xa, ya,y2a,N, N, (float) i / 10 , (float) j / 10, &yy);
			if (yy > max) {
				bestx = (float) i;
				besty = (float) j;
				max = yy;
				}
			}
		}	

*deltax = (bestx - N / 2 * 1000.0) / 100.0;
*deltay = (besty -  N / 2 * 1000.0) / 100.0;

return 0;
}

int CImageProcessing::Spline (float *x, float *y, int n, float yp1, float ypn, float *y2)
{
	int i, k;
	float p, qn, sig, un, *u;

	u = (float  *) malloc (sizeof (float) * (n+1));

	for (i = 0; i <= n; i++) {
		y2[i] = 0.0;
		u[i] = 0.0;
		}

	if (yp1 > 0.99e30)
		y2[1] = u[1] = 0.0;
	else {
		y2[1] = -0.5;
		u[1] = ((float) 3.0 / (x[2] - x[1])) * ( (y[2] - y[1]) / (x[2] - x[1]) - yp1);
		}

	for (i = 2; i <= n - 1; i++) {
		sig = (x[i] - x[i -1]) / (x[i +1] - x[i-1]);
		p = sig * y2[i-1] + (float) 2.0;
		y2[i] = (sig - (float) 1.0) / p;
		u[i] = (y[i+1] - y[i]) / (x[i+1] - x[i]) - (y[i] - y[i-1]) / (x[i] - x[i-1]);
		u[i] = ((float) 6.0 * u[i] / (x[i+1] - x[i-1]) - sig * u[i-1]) / p;
		}

	if (ypn > 0.99e30) {
		qn = un = 0.0;
		}
	else {
		qn = 0.5;
		un = ((float) 3.0 / (x[n] - x[n-1]) ) * (ypn - (y[n] - y[n-1]) / (x[n] - x[n-1]) );
		}

	y2[n] = (un - qn * u[n-1]) / (qn * y2[n-1] + (float) 1.0);

	for (k = n-1; k >= 1; k--) {
		y2[k] = y2[k] * y2[k+1] + u[k];
		}

	free ((void *) u);

	return 0;
}

int CImageProcessing::Splint2 (float *x1a, float *x2a, float **ya, float **y2a, int m, int n, float x1, float x2, float *y)
{

	int j;

	float *ytmp, *yytmp;

	ytmp = (float *) malloc (sizeof (float) * (m+2));
	yytmp = (float *) malloc (sizeof (float) * (m+2));
	for (j = 0; j <= m; j++)		
		ytmp[j] = yytmp[j] = 0.0;

	for (j = 1; j <= m; j++)
		Splint (x2a, ya[j], y2a[j], n, x2, &yytmp[j]);

	Spline (x1a, yytmp, m, (float) 1.0e30, (float) 1.0e30, ytmp);
	Splint (x1a, yytmp, ytmp, m, x1, y);


	free ((void *) ytmp);
	free ((void *) yytmp);

	return 0;

}
int CImageProcessing::Splint (float *xa, float *ya, float *ya2, int n, float x, float *y)
{
	int klo, khi, k;
	float h, b, a;

	klo = 1;
	khi = n;

	while (khi - klo > 1) {

			k = (khi + klo) >> 1;
			if (xa[k] > x)
				 khi = k;
			else
				klo = k;
			}

	h = xa[khi] - xa[klo];
	if (h == 0.0) {
//		DEBUGSTRING ("Bad input to routine splint");
		return -1;
		}

	a = (xa[khi] - x) / h;
	b = (x - xa[klo]) / h;

	*y = a * ya[klo] + b *ya[khi] + (( a * a * a - a) * ya2[klo] + (b * b * b - b) * ya2[khi] ) * (h * h) / (float) 6.0;

	return 0;
}


int  CImageProcessing::BlockTestImage()
{
	int j, i, k,l;
	int bwidth, bheight;
	BYTE*fptr, *ptr, pixel;
	BYTE black, white, firstblock;

	fptr = image->data;
	bwidth = 60;
	bheight = 60;
	black  = 25;
	white = 200;
	pixel = white;
	firstblock = pixel;

	for (i = 0; i  < image->height; i+= bheight) {
	
		for (j = 0; j < image->width; j+= bwidth) {
		
						//draw block
				for (k = i; k < i+bheight; k++) {
					for (l = j; l < j+bwidth; l++) {

							if (l < image->width && k < image->height) {
								ptr = image->data + l + k * image->width;
								*ptr = pixel;
								}

							}
					}

				if (pixel == white)
					pixel = black;
				else
					pixel = white;

			}

		if (firstblock == white)
			pixel = firstblock = black;
		else
			pixel = firstblock = white;

		}

//#ifdef _DEBUG
	image_dump (image, "C:\\test_image.raw");
//#endif

	return 0;

}
int  CImageProcessing::SineXTestImage()
{
	int j, i;
	BYTE*fptr,pixelX, pixelY;
	double period, xx, yy, pi;

	fptr = image->data;
	period = 60.0;
	pi = 3.141592654;
	xx = 2 * pi / period;
	yy = 2 * pi / period;

	for (i = 0; i  < image->height; i++) {
			pixelX = (BYTE) (63 + 63 * sin ((double) i * xx));
		for (j = 0; j < image->width; j++) {
				pixelY = (BYTE) (63 + 63 * sin ((double) j * yy));
			
				*fptr = pixelX + pixelY;
				fptr++;
				}

		}

//#ifdef _DEBUG
	image_dump (image, "C:\\test_image.raw");
//#endif

	return 0;

}
/*
 *		R O U N D
 *			-- round the value to dp decimal places
 */
void  CImageProcessing::round(double *x, int dp)
{
	double div;
	int xx;

	if (dp < 0 && dp > 12)
		return;

	div = 1;
	while (dp-- > 0)
		div *= 10;

	xx = (int) (*x * div + 0.5);
	*x = xx / div;

}

/*
 *		C R E A T E C U R V E 
 */
 int CImageProcessing::CreateCurve(double *data)
{
	int i, j;
	double x,y;

	i = j = 0;

	for (i = 0; i < 100; i++) {
		for (j = 0; j < 100; j++) {
			x = (double) (i - 50);
			y = (double) (j - 50);
			*(data+i + j * 100) = 100 - (x * x + y * y);
			}
		}

	return 0;
}

/*
 *	C R O S S H A I R S _ C O G
 *		-- locate crosshairs in image (part grey scale)
 *		-- identify the arms of the cross
 *		-- and determine the centre of gravity of the arms
 *		-- from these points determine best straight line 
 *		-- to find centre of cross 
 */
int CImageProcessing::crosshairs_cog(Image *image, double *cx, double *cy, double *m, double *c, int min_arm_length)
{
	UINT width = image->width;
	UINT height = image->height;
	UINT runstart, rlength;
	UINT maxval, maxbin, total;
	double midval;
	BOOL foreground;
	UINT x, y, i, np;
	double mh, mv, ch, cv;
	BYTE *iptr;
	BOOL horizontal, vertical;
	int PARAM_MAXSEGWIDTH;
	
	PARAM_MAXSEGWIDTH = image->width * 3 / 4;

	UINT *histo;	
	UINT XBINS, YBINS, MAXBINS, MINPOINTS;

	XBINS = (image->width + BINWIDTH -1) / BINWIDTH;
	YBINS = (image->height + BINWIDTH - 1) / BINWIDTH;
	MINPOINTS = min_arm_length; //image->height / 4;
	MAXBINS = MAXI (XBINS,YBINS); 
	histo = (UINT *) malloc (MAXBINS * sizeof (UINT));	

	horizontal = vertical = FALSE;
	if (image == NULL)
		return erIMAGE_FAILED;
	dpoints = (dPoint *) malloc (image->width * image->height * sizeof (dPoint));	//create array equal in size to image

	npoints = 0;

			/*Scan image in x direction for possible vertical crosshair segments. */
	for (i = 0; i < XBINS; i += 1)
		histo[i] = 0;

	for (y = 0; y < height; y += 1) {
		iptr = image->data + y * width;
		foreground = FALSE;
		for (x = 0; x < width /*- MAXSEGWIDTH */; x += 1) {
			if (*iptr++) {
				if (!foreground) {
					runstart = x;
					foreground = TRUE;
					}
				}
			else
				if (foreground) {
					rlength = x - runstart;
					if (rlength >= MINSEGWIDTH && rlength <= PARAM_MAXSEGWIDTH) {

							//determine COG
						BYTE *pPixel;
						double cog = 0.0;
						double ww = 0.0;
						int xx = x;
						pPixel = image->data + y * width + x;
						while (xx > runstart) {

							cog += (double) (255 - *pPixel) * xx;
							ww += (double) (255 - *pPixel);
							xx--;
							pPixel--;
							}	
						midval = cog / ww;	
					
						addPoint(midval,(double) y);
						histo[(int)midval / BINWIDTH] += 1;
						}
					else {
						DEBUGSTRING ("VERTICAL WRONG LENGTH\n");
						}
					foreground = FALSE;
					}
			}
		}

	maxval = 0;
	for (i = 0; i < XBINS; i += 1)
		if (histo[i] > maxval) {
			maxval = histo[i];
			maxbin = i;
			}

	if (maxval) {
		midval = maxval * (BINWIDTH * (2 * maxbin + 1)) / 2;
		total = maxval;
		if (maxbin > 0) {
			midval += histo[maxbin - 1] * (BINWIDTH * (2 * maxbin - 1)) / 2; 
			total += histo[maxbin - 1];
			}
		if (maxbin < XBINS - 1) {
			midval += histo[maxbin + 1] * (BINWIDTH * (2 * maxbin + 3)) / 2; 
			total += histo[maxbin + 1];
			}
		midval = midval / total;
	
			/* When selecting suitable points, swap the x and y values so that the
			 * least squares fit for the vertical component is done with y as the
		  	 * primary parameter.
			 */

		np = 0;
		for (i = 0; i < npoints; i += 1){
			if (abs(int (dpoints[i].x - midval)) < 16 /*MAXSEGWIDTH*/) {
				double xx, yy;
				xx = dpoints[i].x, yy = dpoints[i].y;
				dpoints[np].x = yy, dpoints[np].y = xx;
				np += 1;
				}
			}

		if (np >=  MINPOINTS) {
			if (linefit(dpoints, np, &mv, &cv) != -1) {
				vertical = TRUE;
				mv = 1 / mv; cv = -cv * mv;
				}
			}
		}

		/* Scan image in y direction for possible horizontal crosshair segments. */
	npoints = 0;
	for (i = 0; i < YBINS; i += 1)
		histo[i] = 0;

	for (x = 0; x < width; x += 1) {
		iptr = image->data + x;
		foreground = FALSE;

		for (y = 0; y < height /*- MAXSEGWIDTH */; y += 1) {
			assert(iptr < image->data + width * height);
			if (*iptr) {
				if (!foreground) {
					runstart = y;
					foreground = TRUE;
					}
				}
			else
				if (foreground) {
					rlength = y - runstart;
					if (rlength >= MINSEGWIDTH && rlength <= PARAM_MAXSEGWIDTH) {
							//determine COG
						BYTE *pPixel;
						double cog = 0.0;
						double ww = 0.0;
						int yy = y;
						pPixel = image->data + yy * width + x;
						while (yy > runstart) {
							cog += (double) (255 - *pPixel) * yy;
							ww += (double) (255 - *pPixel);
							yy--;
							pPixel--;
							}	
						midval = cog / ww;	
											
						addPoint((double) x, midval);
						histo[(int)midval / BINWIDTH] += 1;

						}
					else {
						DEBUGSTRING ("HORIZONTAL WRONG LENGTH\n");
						}

					foreground = FALSE;
					}
				iptr += width;
				}
		}

	maxval = 0;
	for (i = 0; i < YBINS; i += 1)
		if (histo[i] > maxval) {
			maxval = histo[i];
			maxbin = i;
			}
	if (maxval) {
		midval = maxval * (BINWIDTH * (2 * maxbin + 1)) / 2;
		total = maxval;
		if (maxbin > 0) {
			midval += histo[maxbin - 1] * (BINWIDTH * (2 * maxbin - 1)) / 2; 
			total += histo[maxbin - 1];
			}
		if (maxbin < XBINS - 1) {
			midval += histo[maxbin + 1] * (BINWIDTH * (2 * maxbin + 3)) / 2; 
			total += histo[maxbin + 1];
			}
		midval = midval / total;

		np = 0;
		for (i = 0; i < npoints; i += 1)
			if (abs(int (dpoints[i].y - midval)) < 16 /*MAXSEGWIDTH*/){
				dpoints[np++] = dpoints[i];
				}

		if (np >= MINPOINTS)
 			if (linefit(dpoints, np, &mh, &ch) != -1)
				horizontal = TRUE;
		}

	free((void *) dpoints);
	free ((void *) histo);

	if (horizontal && vertical) {
		if (fabs(mh - mv) < EPSILON) {
			return erIMAGE_FAILED;
			}

			/* Calculate centrepoint as intersection of two lines. */
		*cx = (cv - ch) / (mh - mv);
		*cy = (mh * cv - mv * ch) / (mh - mv);

		*m = mh;
		*c = ch;

		return erNO_ERRORS;
		}

	return erIMAGE_FAILED;
} 

/*
 *		 A D D P O I N T
 *			-- add image points for finding arms of a cross-hair
 */
void CImageProcessing::addPoint(double x, double y)
{
	//assert(npoints < POINTS);
	if (npoints > image->width * image->height)
		return;

	dpoints[npoints].x = x;
	dpoints[npoints].y = y;
	npoints += 1;
}

/*
 *		D L I N E F I T
 *			-- fit the points to find the best straight line
 *			-- to identify the arms of a cross-hair
 */
int CImageProcessing::linefit(dPoint *points, UINT n, double *slope, double *incpt)
{
	double Sxx, Sxy, Sx, Sy;
	double divisor;
	double x, y;
	UINT i;

	assert(n > 1);

	Sxx = Sxy = Sx = Sy = 0;
	for (i = 0; i < n; i += 1) {
		x = points[i].x, y = points[i].y;
		Sxx += x * x;
		Sxy += x * y;
		Sx += (double) x;
		Sy += (double) y;
		}

	divisor = n * Sxx - Sx * Sx;
	if (divisor < EPSILON) {
		*incpt = x;
		return -1;
		}

	*slope = (n * Sxy - Sx * Sy) / divisor;
	*incpt = (Sy * Sxx - Sx * Sxy) / divisor;
	
	return 0;
}
