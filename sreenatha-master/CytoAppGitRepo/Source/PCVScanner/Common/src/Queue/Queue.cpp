//=================================================================================
// Queue.cpp
//
// Event locking circular queue (FIFO) using memory mapped files, for interprocess
// data sharing.
//
// History:
//
// 25Jun01  MC: Modified 'mname' arg to CreateMutex() to have a forward slash
//				backward slash ok in W2000, NULL return in WNT. I am linking a VBscript
//				engine to WinCV finder (for Interphase finder spot counting) so need this
// 11Oct00	WH:	original
//=================================================================================
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <assert.h>
#include "AIQueue.h"


BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}

//=================================================================================
// _GRAB_DEBUG
// Useful debug routine
//	Type 0 is ERROR and is always reported
//	Type 1 is debug mode only.
//	Name should be the function name
//	message is printf style
//=================================================================================
static void _GRAB_DEBUG(int Type, TCHAR * name, TCHAR* message, ...)
{
#ifndef _DEBUG
	if (Type == 1)	
		return;
#endif
	va_list fptr;	// VA list pointer
	TCHAR output[512];

	// Apply formatting
	va_start(fptr, message);
	_vstprintf(output, message, fptr);
	va_end(fptr);

	if (Type == 0)	// Error
		OutputDebugString(_T("ERROR:["));
#ifdef _DEBUG
	else			// Infomation
		OutputDebugString(_T("INFO: ["));
#endif
	OutputDebugString(name);
	OutputDebugString(_T("]: "));

	OutputDebugString(output);
}


//=================================================================================
// Constructor
// Set all members to good states
//
//=================================================================================
CQueue::CQueue()
{ 
	m_hMapFile	= NULL;
	m_hFile		= NULL;
	m_lpMem		= NULL;
    m_hLockMutex= NULL;
	m_hdr.snk	= 0;
	m_hdr.src	= 0;
	m_bConnect	= FALSE;
	m_QName[0]	= 0;

	m_lockThreadID = 0; // Am assuming 0 is never a valid thread ID i.e. GetCurrentThreadID() never returns 0.
}

//=================================================================================
// Destructor
// Tidy up	
//=================================================================================
CQueue::~CQueue()
{ 
	// Tidy up
	if (m_lpMem)
	{
		FlushViewOfFile(m_lpMem, 0);
		if (!UnmapViewOfFile(m_lpMem))
			_GRAB_DEBUG(0, _T("~Queue"), _T("closing the memory view [%ld]\n\r"),	GetLastError());
	}

	if (m_hMapFile)
	{
		if(!CloseHandle(m_hMapFile))
			_GRAB_DEBUG(0, _T("~Queue"), _T("closing the mapping object [%ld]\n\r"),	GetLastError());
	}

	if (m_hFile)
	{
		if(!CloseHandle(m_hFile))   // close the file itself
			_GRAB_DEBUG(0, _T("~Queue"), _T("closing the file [%ld]\n\r"), GetLastError());
	}

    if (m_hLockMutex)
        CloseHandle(m_hLockMutex);

	if (m_QName[0])
		DeleteFile(m_QName);

}

//=================================================================================
// Initialize
// Lock Queue, Attach to memory mapped file if it already exists otherwise create/open 
// the file and the memory mapping. Signal that the Queue is open and unlock.
// This function must only be called once for each instance of CQueue.
// This function must be called before any other functions are used.
//
// Returns:
//	Ok on success
//	Error on Error
//=================================================================================
int CQueue::Initialize(DWORD size, LPCTSTR dir, LPCTSTR queue)
{
	TCHAR qname[_MAX_PATH];
	TCHAR ename[_MAX_PATH];
	TCHAR fname[_MAX_PATH];
    TCHAR mname[_MAX_PATH];

	if (queue)
		_stprintf(qname, _T("%s"), queue);
	else
		_tcscpy(qname, _T("_MDS2_Q_"));


	// Create event name, based on queue name
    // Put it in a global name space
//	_stprintf(mname, "Global\\%s_MUTEX", qname); MC 25Jun01 see comments above
	_stprintf(mname, _T("Global/%s_MUTEX"), qname);
	_stprintf(ename, _T("Global\\%s_EVENT"), qname);

	// File name, based on queue name
	if (dir)
		_stprintf(fname, _T("%s\\%s_QFILE"), dir, qname);
	else
		_stprintf(fname, _T("\\%s_QFILE"), qname);

	_stprintf(m_QName, _T("%s"), fname);

    m_hLockMutex = CreateMutex(NULL, FALSE, mname);
	if (!m_hLockMutex)
	{
		_GRAB_DEBUG(0, _T("Queue: Initialize"), _T("m_hLockMutex %s is NULL\n\r"), mname);
		return Error;
	}

	// Check if the file is already mapped
	m_hMapFile = OpenFileMapping(
							FILE_MAP_READ | FILE_MAP_WRITE,	// Data is read/write
							FALSE,
							qname);	// Existing name of file mapping

	if (!m_hMapFile)
	{
		// Create the test file. We open it "Create Always" to overwrite any
		// existing file. We re-create the data below.
		m_hFile = CreateFile(fname, GENERIC_READ | GENERIC_WRITE, 0, NULL, 
				 CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

		if (m_hFile == INVALID_HANDLE_VALUE) 
		{
			_GRAB_DEBUG(0, _T("Queue: Initialize"), _T("m_hFile is NULL\n\r"));


			LPVOID lpMsgBuf;
			FormatMessage( 
				FORMAT_MESSAGE_ALLOCATE_BUFFER | 
				FORMAT_MESSAGE_FROM_SYSTEM | 
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				GetLastError(),
				0, // Default language
				(LPTSTR) &lpMsgBuf,
				0,
				NULL 
			);
			// Process any inserts in lpMsgBuf.
			// ...
			// Display the string.
			MessageBox( NULL, (LPCTSTR)lpMsgBuf, _T("Error"), MB_OK | MB_ICONINFORMATION );
			// Free the buffer.
			LocalFree( lpMsgBuf );


			return Error;
		}

		// Now create a file-mapping object for the file.
		m_hMapFile = CreateFileMapping( m_hFile,				// current file handle
									NULL,					// default security
									PAGE_READWRITE,			// read/write permission
									0,						// size of mapping object, high
									size +sizeof(m_hdr),	// size of mapping object, low
									qname);		// name of mapping object
	}
	else
		m_bConnect = TRUE;	// We didn't create the Queue

	if (m_hMapFile == NULL) 
	{
		_GRAB_DEBUG(0, _T("Queue: Initialize"), _T("hMapFile is NULL: last error: %d\n\r"), GetLastError() );
		return Error;
	}

	// Map the view
	m_lpMem =		MapViewOfFile(	m_hMapFile,				// handle to mapping object
									FILE_MAP_ALL_ACCESS,	// read/write permission 
									0,						// high-order 32 bits of file offset
									0,						// low-order 32 bits of file offset
									0);						// map entire file
	if (m_lpMem == NULL) 
	{
		_GRAB_DEBUG(0, _T("Queue: Initialize"), _T("lpMem is NULL: last error: %d\n\r"), GetLastError());
		return Error;
	}

	if (!m_bConnect)	// We created it, so make sure queue_header is ok
	{
		m_size = size;
		WriteHeader(&m_hdr);
	}
	else				// We're just connecting to pre-created queue, so read header
	{
		// Gotta work out the file size
		WIN32_FIND_DATA FindFileData;
		HANDLE hFind = FindFirstFile(fname, &FindFileData);

		if (hFind != INVALID_HANDLE_VALUE)
		{
			m_size = FindFileData.nFileSizeLow - sizeof(Queue_header);
			FindClose(hFind);
		}
		else
		{
			_GRAB_DEBUG(0, _T("Queue: Initialize"), _T("Can't calc file size %d\n\r"), GetLastError() );
			return Error;
		}
		

		ReadHeader(&m_hdr);
	}


  	return Ok;
}

//=================================================================================

BOOL CQueue::IsLockedByThisThread()
{
	// This assumes that this thread is only using this instance of the CQueue
	// object to access the queue (as it should do).
	return (GetCurrentThreadId() == m_lockThreadID);
}

//=================================================================================
// Add
// Add 'data' block to Queue, if 'datasize' of data plus the size of the block
// header will fit. Must have a lock. 'id' is identifier tagged into block header.
// Updates the sink and source pointers
// 
// returns
//	Ok		- On OK
//	NoLock	- On having no lock
//	Full	- No space for data in Queue.
//=================================================================================
int CQueue::Add(VOID *data, UINT datasize, UINT id)
{
    int ret = Ok;

    // Check we are locked by this thread
    if (!IsLockedByThisThread())
        return NoLock;

	if (FreeSpace(datasize + sizeof(item_header)))
	{
		item_header item;

		item.magic = MagicID;
		item.id = id;
		item.size = datasize;

		WriteData(&item, sizeof(item));
		WriteData(data, datasize);
		
//DebugDump("Add", &item);

        ret = Ok;
	}
	else
	{
		ret = Full;
	}

  	return ret;
}
//=================================================================================
// Remove
// Copys the next item (if there are any) into 'buffer' if its big enough as stated
// in 'required size' Also copies the item id into 'id'. Updates the sink and 
// source pointers.
//
// returns
//	Ok			- On OK
//	NoLock		- On having no lock
//	NoItems		- No Data in queue
//  BufferSize	- Passed buffer too small for item
//  Error		- Data Corrupt
//=================================================================================
int CQueue::Remove(VOID *buffer, UINT *requiredsize, UINT *id)
{
    int ret = Ok;

    // Check we are locked by this thread
    if (!IsLockedByThisThread())
        return NoLock;

	ReadHeader(&m_hdr);

	if (m_hdr.src != m_hdr.snk)	// Got some items
	{
		item_header item;

		// Read item header
		memset(&item, 0, sizeof(item));
		ReadData(&item, sizeof(item), FALSE);

		if (item.magic != MagicID)
			ret = Error;
        else
		if (*requiredsize < item.size)
			ret = BufferSize;
        else
        {
		    *requiredsize = item.size;
		    *id = item.id;

		    // Now read data
		    ReadData(buffer, item.size, FALSE);
        }

//DebugDump("Rem", &item);

	}
	else
		ret = NoItems;

  	return ret;
}
//=================================================================================
// Peek
// Reads the next item header extracting item identifier into 'id' and data size into
// 'size'. Sink and source pointers are untouched
//
// returns
//	Ok			- On OK
//	NoLock		- On having no lock
//	NoItems		- No Data in queue
//  Error		- Data Corrupt
//=================================================================================
int CQueue::Peek(UINT *id, UINT *size)
{
    // Check we are locked by this thread
    if (!IsLockedByThisThread())
        return NoLock;

	ReadHeader(&m_hdr);

	if (m_hdr.src != m_hdr.snk)	// Got some items
	{
		item_header item;

		// Read item header
		memset(&item, 0, sizeof(item));
		ReadData(&item, sizeof(item), TRUE);	// Only peek read

		if (item.magic != MagicID)
			return Error;

		*size = item.size;
		*id = item.id;
	}
	else
		return NoItems;

  	return Ok;
}
//=================================================================================
// Lock
// Tests if another CQueue object has locked the queue for its exclusive use, can
// be blocking or non-blocking as in the following: Uses Events.
//		if bWait is TRUE this call will block
//		if timeout > 0 then block will time-out at timeout mSecs or if signalled
//		if timeout = 0 then will block for ever or until signalled
//		if bWait is FALSE, timeout is ignore and the lock state is just tested.
//
// Returns
//	LockOk		- Able to lock queue for exclusive use.
//  NoLock		- (Non-Blocking), Other CQueue object has a lock
//  LockTimeout	- (Blocking), Timed-out with Nolock while waiting to get a lock
//=================================================================================
int CQueue::Lock(BOOL bWait, UINT timeout)
{
	int ret = NoLock;

	DWORD t_out = 0;
	if (bWait)
	{
		if (timeout == 0)
			t_out = INFINITE;
		else
			t_out = timeout;
	}
	else
		t_out = 0;				// No timeout - Just signal

    // WaitForSingleObject() will wait for the mutex to be freed by
	// the owning thread (if there is one), then take ownership of it.
	DWORD status = WaitForSingleObject(m_hLockMutex, t_out);

	if (status == WAIT_OBJECT_0)
	{
		ret = LockOk;

		// This thread now has ownership of the mutex, which means that it has
		// locked the queue. However, once this function has returned, by
		// checking the mutex it will only be possible to see if the queue is
		// locked, not which thread has locked it. Setting a simple boolean flag
		// in the CQueue object is not a thread-safe solution (multiple threads
		// may use the same CQueue object to access the queue), so the ID of
		// the thread is recorded. Hence, so long as a particular thread always
		// uses the same CQueue object, it will be able to tell if it has locked
		// the queue.
		m_lockThreadID = GetCurrentThreadId();
	}
	else 
    {

        //_GRAB_DEBUG(0, "Lock", "Failed %d\r\n", QID);

		// Note: do NOT clear m_lockThreadID here - that is only set or cleared
		// by threads that successfully get a lock.

        if (bWait && status == WAIT_TIMEOUT)
		    ret = LockTimeout;
	    else
		    ret = NoLock;
    }
        
	return ret;
}
//=================================================================================
// ClearLock
// Unlock queue so other CQueue's can access it.
//
// Returns
//	NoLock	- The CQueue hadn't locked it in the first place.
//  Ok		- Lock Cleared
//=================================================================================
int CQueue::ClearLock()
{
	int status = Error;

    // Allow access by another calling thread
	// If this thread doesn't own the mutex, ReleaseMutex() will fail
	// and the critical section should not be released either.
	if (ReleaseMutex(m_hLockMutex))
	{
		// Indicate that no thread has a lock now - am assuming 0 is never a
		// valid thread ID i.e. GetCurrentThreadID() never returns 0.
		m_lockThreadID = 0;
		status = Ok;
	}
	else
		status = NoLock;


	return status;
}
//=================================================================================
// Reset
// Reset sink and source pointers in queue, effectively losing any data in it.
//
// Returns
//  NoLock	- CQueue has no lock on the queue.
//  Ok		- Ok
//=================================================================================
int CQueue::Reset()
{
    // Check to make sure the queue is locked by this thread
	if (!IsLockedByThisThread())
        return NoLock;

	m_hdr.snk	= 0;
	m_hdr.src	= 0;

	WriteHeader(&m_hdr);

	return Ok;
}
//=================================================================================
// GetInfo.
// Get Diagnostic info on the queue. copies max queue size to'size' and the current
// sink and source pointer to 'sink' and 'source'
//
// returns
//	NoLock	- No lock on queue
//	Ok		- Ok
//=================================================================================
int CQueue::GetInfo(DWORD *size, DWORD *sink, DWORD *source)
{
	if (!IsLockedByThisThread())	// Not locked by this thread
		return NoLock;

	Queue_header hdr;
	ReadHeader(&hdr);

	*size	= m_size;
	*sink	= hdr.snk;
	*source = hdr.src;

	return Ok;
}
//=================================================================================
//									PRIVATE
//=================================================================================
//=================================================================================
// ReadHeader
// Read current sink and source positions from queue
//=================================================================================
int CQueue::ReadHeader(Queue_header *hdr)
{
	memcpy(hdr,(BYTE*)m_lpMem + m_size, sizeof(m_hdr));
	return 0;
}
//=================================================================================
// WriteHeader
// Write current sink and source positions to queue
//=================================================================================
int CQueue::WriteHeader(Queue_header *hdr)
{
	memcpy((BYTE*)m_lpMem + m_size, hdr, sizeof(m_hdr));
	return 0;
}
//=================================================================================
// ReadData
// Read datasize block from queue at the source pointer into 'data'. If bPeek then
// don't update the source pointer otherwise do effectively removing it. Data maybe 
// wrapped such that some of the data exists at end of physical queue and the rest
// at the start. i.e Queue is circular with no defined beginning or end.
//=================================================================================
int CQueue::ReadData(VOID *data, UINT datasize, BOOL bPeek)
{
	BYTE * lp;

	ReadHeader(&m_hdr);

	// Check we have enough data to read
	DWORD used, avail;

	if (m_hdr.src > m_hdr.snk)	// sink pointer has wrapped round
		avail = m_hdr.src - m_hdr.snk;
	else						// sink pointer has NOT wrapped round
		avail = (m_size - m_hdr.snk) + m_hdr.src;

	used = m_size - avail;

	if (datasize > used) {
		_GRAB_DEBUG(0, _T("CQueue::ReadData()"), _T("Requested more data %d than in queue %d\r\n"), datasize, used);
		return -1;
	}

	DWORD src = m_hdr.src;

	if (src + datasize <= m_size)			// Guaranteed contigious
	{
		lp = (BYTE *)m_lpMem + src;
		memcpy(data, lp, datasize);
		src += datasize;
	}
	else										// Have to wrap, i.e. do in 2 chunks
	{
		DWORD firstblock = m_size - src;
		// 1st chunk
		lp = (BYTE *)m_lpMem + src;
		memcpy(data, lp, firstblock);

		// Wrap to start, now 2nd chunk
		src = 0;
		lp = (BYTE *)m_lpMem;
		memcpy((BYTE *)data + firstblock, lp, datasize - firstblock);
		src = datasize - firstblock;
	}

	// Write back header, if we weren't just peeking
	if (!bPeek)
	{
		m_hdr.src = src;
		WriteHeader(&m_hdr);
	}

	return 0;
}
//=================================================================================
// WriteData
// Read 'datasize' block contained in 'data' to the queue starting at sink offset. 
// Data maybe wrapped round to start of queue if there is space.
//=================================================================================
int CQueue::WriteData(VOID *data, UINT datasize)
{
	BYTE * lp;

	ReadHeader(&m_hdr);

	if (m_hdr.src > m_hdr.snk)	// Guaranteed contigious
	{
		lp = (BYTE *)m_lpMem + m_hdr.snk;
		memcpy(lp, data, datasize);
		m_hdr.snk += datasize;
	}
	else						// May have to wrap
	{
		if ((m_size - m_hdr.snk) >= datasize)	// Contigious
		{
			lp = (BYTE *)m_lpMem + m_hdr.snk;
			memcpy(lp, data, datasize);
			m_hdr.snk += datasize;
		}
		else					// Have to wrap, i.e. do in 2 chunks
		{
			DWORD firstblock = m_size - m_hdr.snk;
			// 1st chunk
			lp = (BYTE *)m_lpMem + m_hdr.snk;
			memcpy(lp, data, firstblock);

			// Wrap to start, now 2nd chunk
			m_hdr.snk = 0;
			lp = (BYTE *)m_lpMem;
			memcpy(lp, (BYTE *)data + firstblock, datasize - firstblock);
			m_hdr.snk = datasize - firstblock;
		}
	}

	// Write back header
	WriteHeader(&m_hdr);

	return 0;
}
//=================================================================================
// FreeSpace
// Calculate if there is space enough on the queue for a 'size' sized block of data.
// This includes wrapping around to the start if there is space. i.e circular.
//=================================================================================
BOOL CQueue::FreeSpace(UINT size)
{
	DWORD avail = 0;

	ReadHeader(&m_hdr);

	if (m_hdr.src > m_hdr.snk)	// sink pointer has wrapped round
		avail = m_hdr.src - m_hdr.snk;
	else						// sink pointer has NOT wrapped round
		avail = (m_size - m_hdr.snk) + m_hdr.src;

	return (avail >= size) ? TRUE : FALSE;
}
//=================================================================================
//=================================================================================

void CQueue::DebugDump(LPTSTR fn_name, item_header *item)
{
	TCHAR str[256];

	_stprintf(str, _T("(0x%x) CQueue::%s() - magic 0x%x, size %d, id %d: src=0x%x, sink=0x%x\r\n"),
		this, fn_name, item->magic, item->size, item->id, m_hdr.src, m_hdr.snk);
	OutputDebugString(str);
}
