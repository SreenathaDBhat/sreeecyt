========================================================================
    MICROSOFT FOUNDATION CLASS LIBRARY : SVMClassifier Project Overview
========================================================================

Projects in this solution:

FragFileConvert - converts a script frag.dat file into a format suitable
for training a Support Vector Machine.

SVMClassifier - a DLL with small wrapper classes for training and creating
a SVM model and predicting an outcome based on a set of feature vectors.


