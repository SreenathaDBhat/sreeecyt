#include "stdafx.h"
#include "svm.h"
#include "svmclassifier.h"
#include "svmtraining.h"


CSVMTrain::CSVMTrain(void)
{
    default_parameters(&m_Parameters);
}

CSVMTrain::~CSVMTrain(void)
{
}

void CSVMTrain::DefaultParams(struct svm_parameter *pParams)
{    
    default_parameters(pParams);
}

int CSVMTrain::Train(LPCTSTR model_file_name, LPCTSTR input_file_name)
{
    return svmtrain(model_file_name, input_file_name, &m_Parameters);
}

int CSVMTrain::Train(LPCTSTR model_file_name, LPCTSTR input_file_name, struct svm_parameter *pParams)
{
    return svmtrain(model_file_name, input_file_name, pParams);
}
