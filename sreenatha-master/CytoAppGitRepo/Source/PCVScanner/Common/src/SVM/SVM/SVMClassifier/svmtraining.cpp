#include "stdafx.h"
#include "svm.h"

extern "C" void parse_command_line(int argc, char **argv, char *input_file_name, char *model_file_name);
extern "C" void read_problem(const char *filename);
extern "C" void do_cross_validation();


extern "C" struct svm_parameter param;		// set by parse_command_line
extern "C" struct svm_problem prob;		// set by read_problem
extern "C" struct svm_model *model;
extern "C" struct svm_node *x_space;
extern "C" int cross_validation;
extern "C" int nr_fold;

void default_parameters(struct svm_parameter *pParams)
{
    // default values
	pParams->svm_type = C_SVC;
	pParams->kernel_type = RBF;
	pParams->degree = 3;
	pParams->gamma = 0;	// 1/k
	pParams->coef0 = 0;
	pParams->nu = 0.5;
	pParams->cache_size = 40;
	pParams->C = 1000;
	pParams->eps = 1e-3;
	pParams->p = 0.1;
	pParams->shrinking = 1;
	pParams->probability = 0;
	pParams->nr_weight = 0;
	pParams->weight_label = NULL;
	pParams->weight = NULL;
}


int svmtrain(LPCTSTR model_file_name, LPCTSTR input_file_name, struct svm_parameter *pParams)
{
	const TCHAR *error_msg;
	TCHAR msg[1024];

	read_problem(input_file_name);
    if (pParams->gamma == 0.0)
        pParams->gamma = 4.0;

	error_msg = svm_check_parameter(&prob, pParams);

	if(error_msg)
	{
		_stprintf(msg, _T("SVM Train Error: %s\n"),error_msg);
		OutputDebugString(msg);
        return 0;
	}

	if(cross_validation)
	{
		do_cross_validation();
	}
	else
	{
		model = svm_train(&prob, pParams);
		svm_save_model(model_file_name,model);
		svm_destroy_model(model);
	}
	svm_destroy_param(pParams);
	free(prob.y);
	free(prob.x);
	free(x_space);

	return 1;
}