
#ifndef _SVMTRAINING_H
#define _SVMTRAINING_H

int svmtrain(LPCTSTR model_file_name, LPCTSTR input_file_name, struct svm_parameter *pParams);
void default_parameters(struct svm_parameter *pParams);

#endif
