//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by SlideDll.rc
//
#define IDREFRESH                       3
#define IDLOAD                          4
#define IDUNLOAD                        5
#define IDD_SLIDES_DIALOG               2000
#define IDC_SLIDE_LIST                  2001
#define IDI_SLIDE                       2001
#define IDI_SLIDE_SEL                   2002
#define IDD_SCAN_PROGRESS_DIALOG        2003
#define IDC_TRAY_TAB                    2005
#define IDD_BARCODE_DIALOG              2005
#define IDC_BARCODE_STATIC              2006
#define IDI_LOADED                      2006
#define IDC_QUEUEMETERCTRL1             2007
#define IDC_ASSAYS_BUTTON               2008
#define IDC_BARCODE_EDIT                2009
#define IDC_ASSAY_EDIT                  2010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        2007
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         2011
#define _APS_NEXT_SYMED_VALUE           2000
#endif
#endif
