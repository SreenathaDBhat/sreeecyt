// Slide.cpp: implementation of the CSlide class.
//
//	Kevin Shields 13Dec00
//
//
//
//Mods:
//	AR 15 Oct 03	Added GetAssayName() to retreive the name of the assay associated with the barcode
//	AR 19 Sept 03	Added AssignAssay(),CheckLockable(), CheckCaseSize(), CheckComplete()
//					Functions formerly in scan/MainFrm.cpp
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AiTIFF.h"
#include "Slide.h"
#include "Assay.h"
#include "MicroErrors.h"
#include "aiishared.h"
#include "mlock.h"

//#include "../../Common/src/MicServer/MicServer.h"
//#include "../MicServer/MicServer_i.c"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSlide::CSlide()
{
	m_Assay             = NULL;
	
	m_InternalAssayName = _T("<no assay>");
	m_RootName			= _T("Empty");

	m_AssayName			= _T("<no assay>");
	m_applock           = NULL;		    // Application level locking

}


CSlide::~CSlide()
{
	if (m_Assay)
    {
	    delete m_Assay;
        m_Assay = NULL;
    }

	// If we're done with the slide, then we're done with the lock
	//delete m_applock;
	//m_applock = NULL;
}







