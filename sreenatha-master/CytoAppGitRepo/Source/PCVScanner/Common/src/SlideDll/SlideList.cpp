// SlideList.cpp: implementation of the CSlideList class.
//
//	Kevin Shields 13Dec00
//
//
//
//
//////////////////////////////////////////////////////////////////////
//Mods
//
//	AR	22 Oct 2003 Added IsASlideLoaded() and RemoveSlideLoaded(). Renamed SlideLoaded() to GetSlideLoaded().
//
//
//
#include "stdafx.h"
#include "AITiff.h"
#include "threaddefs.h"
#include "SlideList.h"
#include "Slide.h"
#include "AIQueue.h"

#include "Assay.h"

#include "ScanPass.h"
#include "ScanPassList.h"
#include "SpectraChromeList.h"
#include "RegHelper.h"
//#include "../../Common/src/MicServer/MicServer.h"
//#include "../Grabber2/GrabServer2/_GrabServer2_i.c"
#include "Axes.h"
#include "MicroErrors.h"
#include "mlock.h"
#include "AiiShared.h"



#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


// These sync objects must be created by the application
// BEFORE launching the cvscanner process.
static HANDLE       hRequestToEndEvent = NULL;
static HANDLE       hEndEvent = NULL;

//==================================================================================================
//==================================================================================================



void CSlideList::OnFind(LPCTSTR QPath, LPCTSTR VarFileName, LPCTSTR QFileName) 
{
    SECURITY_ATTRIBUTES  sa;
    SECURITY_DESCRIPTOR  sd;

	// Setup for script engine
	m_VarFile = VarFileName;
	m_VBQFile = QFileName;
	m_QPath = QPath;

    InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION);
    
    sa.nLength = sizeof(sa);
    sa.bInheritHandle = TRUE;       // This must be TRUE so that cvscanner can inherit the handles
    sa.lpSecurityDescriptor = &sd;
    
    // This is event signals a request to end the cvscanner process
    hRequestToEndEvent = CreateEvent(&sa, TRUE, FALSE, SCAN_REQTOENDEVENT);
    
    // This is event will be signalled when cvscanner.exe terminates
    hEndEvent  = CreateEvent(&sa, TRUE, FALSE, SCAN_ENDEVENT);
    
    // Make sure these events are reset
    ResetEvent(hRequestToEndEvent);
    ResetEvent(hEndEvent);
    
    
}


///////////////////////////////////////////////////////////////////////////
// Do a controlled shut down... which will be when the finder
// thread has actually emptied the Q.
///////////////////////////////////////////////////////////////////////////

void CSlideList::ScanEnd(void)
{
    // Set up the handles of the events used by the CVSCANNER process
    SetEvent(hRequestToEndEvent);

    // Tidy up handles
    CloseHandle(hRequestToEndEvent);
    CloseHandle(hEndEvent);
}






//==================================================================================================
//==================================================================================================



//==================================================================================================
//==================================================================================================




//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSlideList::CSlideList()
{
	m_KillFindFlag = 1;
	m_KillScanFlag = 1;
	m_scanRunning = FALSE;
	m_findRunning = FALSE;
    m_bSkipScanPass = FALSE;
    m_bSkipScanRegion = FALSE;
    m_bStopCapture = FALSE;
	m_FindThreadProcessing = FALSE;
	m_FramesInScan = 0;
	m_ScanCurrentFrame = 0;
	m_FindCurrentFrame = 0;
	m_defaultSpectraChromeList = NULL;

	m_scanQ = NULL;
	m_findQ = NULL;
	m_resultsQ = NULL;
	m_ScanSlidesOfType = _T("any");
	m_list.RemoveAll();
	m_ReprocessFlag = FALSE;
	m_hFindThread = NULL;

	m_VarFile = "SCRIPTVARS.DAT";
	m_VBQFile = "VBFIND";
	m_QPath = "";
	m_ProcessName = "";

	m_abortCurrentSlide = FALSE;
}


//==================================================================================================
//==================================================================================================

CSlideList::~CSlideList()
{
	m_KillFindFlag = TRUE;

	if (m_defaultSpectraChromeList)
		delete m_defaultSpectraChromeList;

	

	//CloseAllCases();
	POSITION pos=m_list.GetHeadPosition();
	while (pos != NULL)
		delete m_list.GetNext(pos);
	m_list.RemoveAll();

    

}

//==================================================================================================
//==================================================================================================

void CSlideList::Queues(CQueue* scanQ, CQueue* findQ)
{
	m_scanQ = scanQ;
	m_findQ = findQ;
}




