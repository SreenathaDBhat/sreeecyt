// SpectraChrome.cpp: implementation of the CSpectraChrome class.
//
//	Kevin Shields 13Dec00
//
//
//	26Jun02	SN	Added GetObjectiveName().
//
// Sep/22/04 HB	
//	- Added "m_ShadeImageMean" for color correction
//	- Added "Tweak LUT for R,G,B" for shade image.
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ColControls.h"
#include "Axes.h"
#include "AITIFF.h"
#include "SpectraChrome.h"
#include "ConvertImage.h"
#include "RegHelper.h"
#include "SerialPort.h"
#include "SerializeXML.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define MAX_IMAGE_SIZE 4000000

char redstr[] = "5";
char greenstr[] = "6";
char bluestr[] = "7";



//////////////////////////////////////////////////////////////////////
// CZstackSettings Construction/Destruction
//////////////////////////////////////////////////////////////////////

CZstackSettings::CZstackSettings()
{
	m_on=m_num_images=m_focus_dist=0;
}

CZstackSettings::~CZstackSettings()
{
}

CZstackSettings* CZstackSettings::duplicate()
{
    CZstackSettings *zs;

    zs = new CZstackSettings;

	zs->m_on = m_on;
	zs->m_num_images = m_num_images;
	zs->m_focus_dist = m_focus_dist;

	return zs;
}

void CZstackSettings::XMLSerialize(CSerializeXML &ar)
{
	XMLCLASSNODENAME("CZstackSettings", ar);	
	XMLDATA(m_on);
	XMLDATA(m_num_images);
	XMLDATA(m_focus_dist);
	XMLENDNODE;

	
}




//////////////////////////////////////////////////////////////////////
// CSpectraChrome Construction/Destruction
//////////////////////////////////////////////////////////////////////
CSpectraChrome::CSpectraChrome()
{
	m_Name = _T("");
	m_Colour = new rgbTriple;
	m_Colour->r=m_Colour->r=m_Colour->r=0;
	m_Description = _T("");
	m_Fluorescent = FALSE;
    m_Counterstain = FALSE;

	m_ZstackSettings = new CZstackSettings;

	
}

CSpectraChrome::~CSpectraChrome()
{
	delete m_ZstackSettings;
	delete m_Colour;
}





// return a pointer to a copy of this spectra chrome
CSpectraChrome *CSpectraChrome::duplicate()
{
	CSpectraChrome *newsc;

	if (!this) return NULL;

	newsc = new CSpectraChrome;

	newsc->m_Name = m_Name;
	// KR copy the actual colours not just reassign pointers
    newsc->m_Colour->r = m_Colour->r;
	newsc->m_Colour->g = m_Colour->g;
	newsc->m_Colour->b = m_Colour->b;
    
    // KR Stop some mem leaks remove these which were created on new CSpectrachrome above
    delete newsc->m_ZstackSettings;

	newsc->m_ZstackSettings = m_ZstackSettings->duplicate();
	newsc->m_Description = m_Description;
	newsc->m_Fluorescent = m_Fluorescent;
    newsc->m_Counterstain = m_Counterstain;

	return newsc;

}



void CSpectraChrome::XMLSerialize(CSerializeXML &ar)
{
		// Invalid names for XML -> maybe write a new Macro
		int m_red = m_Colour->r;
		int m_green = m_Colour->g;
		int m_blue = m_Colour->b;

		int m_version = SPECTRACHROMEVERSION;
		XMLCLASSNODENAME("CSpectraChrome", ar);	
		XMLDATA(m_version);
		XMLDATA(m_Name);
		XMLDATA(m_red);
		XMLDATA(m_green);
		XMLDATA(m_blue);
		XMLDATA(m_Description);
		
		XMLDATA(m_Fluorescent);
        XMLDATA(m_Counterstain);

		// Handle classes with own XML Serialize
		m_ZstackSettings->XMLSerialize(ar);

		XMLENDNODE;

		// reassign colours to real members XML serialize can't handle the names with -> (maybe colour should be a class)
		m_Colour->r = m_red;
		m_Colour->g = m_green;
		m_Colour->b = m_blue;
}













