// SpectraChromeList.cpp: implementation of the CSpectraChromeList class.
//
//	Kevin Shields 13Dec00
//
//
//
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "AITIFF.h"
#include "SpectraChrome.h"
#include "SpectraChromeList.h"
#include "ConvertImage.h"		
#include "MemMapFile.h"
#include "Axes.h"
#include "AutoScope.h"
#include "RegHelper.h"		
#include "ColControls.h"
#include "SerializeXML.h"
#include "CFuncs.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CSpectraChromeList::CSpectraChromeList()
{
	m_list.RemoveAll();
}

CSpectraChromeList::~CSpectraChromeList()
{
	RemoveAll();
}

void CSpectraChromeList::Defaults(int action)
{
	// Defaults folder is relative to the location of the executable.
	CString spectrachromes_filename = GetExecDir() + "\\defaults\\spectrachromes.def";
	CFile spectrachromes_f;

	if (action == LoadDefaults)
	{
		CSerializeXML spectrachromes_ar(spectrachromes_filename, CArchive::load);
		XMLSerialize(spectrachromes_ar);
	}
	if (action == SaveDefaults)
	{
		CSerializeXML spectrachromes_ar(spectrachromes_filename, CArchive::store);
		XMLSerialize(spectrachromes_ar);
	}
}



void CSpectraChromeList::XMLSerialize(CSerializeXML &ar)
{
	int m_version = VERSION;

	XMLCLASSNODENAME("CSpectraChromeList", ar);
	XMLDATA(m_version);

	if (ar.IsStoring())
	{
		POSITION pos=m_list.GetHeadPosition();
		while (pos != NULL)
			m_list.GetNext(pos)->XMLSerialize(ar);
	}
	else
	{
			RemoveAll();
			XMLSTARTLISTDATA("CSpectraChrome");
			CSpectraChrome *sp = new CSpectraChrome;
			sp->XMLSerialize(ar);
			
			AddSpectraChrome(sp);
			XMLENDLISTDATA;
	}

	XMLENDNODE;
}









CSpectraChromeList *CSpectraChromeList::duplicate()
{
	POSITION pos;
	CSpectraChromeList *new_list;

	if (!this) return NULL;

	new_list = new CSpectraChromeList;

	pos = m_list.GetHeadPosition();
	new_list->m_list.RemoveAll();
	while (pos!=NULL)
	{
		CSpectraChrome* sc = m_list.GetNext(pos);
		CSpectraChrome* new_sc = sc->duplicate();
		new_list->m_list.AddTail(new_sc);	// Don't expect duplicates or invalid entries
	}
	return new_list;

}

CSpectraChrome* CSpectraChromeList::GetNamed(CString name)
{
	POSITION pos;
	CSpectraChrome *sc=NULL;

	pos=m_list.GetHeadPosition();
	while (pos != NULL)
	{
		sc = m_list.GetNext(pos);
		if (sc->m_Name == name)
			return sc;
	}
	return NULL;

}

void CSpectraChromeList::RemoveAll()
{
	POSITION pos=m_list.GetHeadPosition();
	while (pos != NULL)
	{
		CSpectraChrome* sp = m_list.GetNext(pos);
		delete sp;
	}
	m_list.RemoveAll();
}

//////////////////////////////////////////////////////////////////////
// Build a list of fluorescent default spectrachromes
//////////////////////////////////////////////////////////////////////
	
CSpectraChromeList*	CSpectraChromeList::FluorescentDefaultList(void) // Build a list of fluorescent spectrachromes only
{
	CSpectraChromeList *pFluors = NULL;
	POSITION P;
	CSpectraChrome *pSpec;

	pFluors = new CSpectraChromeList;
		
	// Load in the default spectrachromes
	Defaults(CSpectraChromeList::LoadDefaults);

	// Find all the fluorescent ones
	P = m_list.GetHeadPosition();
	while (P)
	{
		pSpec = m_list.GetNext(P);
		if (pSpec->m_Fluorescent)
		{
			//pFluors->m_list.AddTail(pSpec);
			pFluors->AddSpectraChrome(pSpec);
		}
	}

	return pFluors;
}


//////////////////////////////////////////////////////////////////////
// Build a list of fluorescent probe spectrachromes i.e. no
// counterstains
//////////////////////////////////////////////////////////////////////
	
CSpectraChromeList*	CSpectraChromeList::FluorescentProbes(void) // Build a list of fluorescent spectrachromes only
{
	CSpectraChromeList *pFluors = NULL;
	POSITION P;
	CSpectraChrome *pSpec;

	pFluors = new CSpectraChromeList;
		
	// Load in the default spectrachromes
	Defaults(CSpectraChromeList::LoadDefaults);

	// Find all the fluorescent ones
	P = m_list.GetHeadPosition();
	while (P)
	{
		pSpec = m_list.GetNext(P);
		if (pSpec->m_Fluorescent && !pSpec->m_Counterstain)
		{
			//pFluors->m_list.AddTail(pSpec);
			pFluors->AddSpectraChrome(pSpec);
		}
	}

	return pFluors;
}





////////////////////////////////////////////////////////////////////////////////
//
//	CheckSpectraChromeName() - Check validity of spectrachrome name
//	Not a duplicate, not blank, alphanumeric characters
//
bool CSpectraChromeList::CheckSpectraChromeName(CString *sp_name)
{
	bool valid = true;
	//int i;

	// Check blank
	if (sp_name->IsEmpty())
		return false;

	
	// Now check for duplicates
	POSITION pos;
	CString this_name;

	pos = m_list.GetHeadPosition();
	while (pos) {
		this_name = (m_list.GetNext(pos))->m_Name;
		if (sp_name->CompareNoCase(this_name) == 0) {
			valid = false;
			break;
		}
	}

	return valid;
}

////////////////////////////////////////////////////////////////////////////////
//
//	AddSpectraChrome() - Add spectrachrome to list (after checking it's validity)
//
bool CSpectraChromeList::AddSpectraChrome(CSpectraChrome *sp)
{
	CString sp_name = sp->m_Name;

	//TRACE1("CSpectraChromeList::AddSpectraChrome() - Adding %s\r\n", sp_name);

	// Check name is valid
	if (CheckSpectraChromeName(&sp_name) == false) {
		TRACE1("CSpectraChromeList::AddSpectraChrome() - Invalid or duplicate name %s\r\n", sp_name);
		return false;
	}

	// OK, now add it
	m_list.AddTail(sp);

	return true;
}

