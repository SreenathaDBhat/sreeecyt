//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SpectraChromeDll.rc
//
#define IDS_SPECTRACHROMEWIZARD         1
#define IDDELETE                        3
#define IDC_GAIN_SLIDER                 1006
#define IDC_EXPOSURE_SLIDER             1007
#define IDC_OFFSET_SLIDER               1008
#define IDC_ADD_NEW_BUTTON              1041
#define IDC_WHEEL2_LISTCTR              1044
#define IDC_WHEEL3_LISTCTR              1045
#define IDC_WHEEL4_LISTCTR              1046
#define IDC_RED_SLIDER                  1059
#define IDC_GREEN_SLIDER                1060
#define IDC_BLUE_SLIDER                 1061
#define IDC_FRAME                       1062
#define IDC_PLANES_SLIDER               1063
#define IDC_SPACING_SLIDER              1064
#define IDC_PLANES_STATIC               1065
#define IDC_SPACING_STATIC              1066
#define IDC_SSIZE_SLIDER                1067
#define IDC_SSIZE_STATIC                1068
#define IDC_GAIN_STATIC                 1069
#define IDC_OFFSET_STATIC               1070
#define IDC_EXPOSURE_STATIC             1071
#define IDC_WHEEL1_LISTCTR              1072
#define IDC_SPECTRACHROME_LIST          2001
#define IDC_NAME                        17016
#define IDD_SPEC_DIALOG                 21000
#define IDC_FOC_CHECK                   21000
#define IDI_FILTER_UNSELECTED           21001
#define IDI_FILTER_SELECTED             21002
#define IDC_AUTO_SETTINGS               21002
#define IDD_AUTOSETTINGS_DIALOG         21003
#define IDC_KEEP                        21003
#define IDC_AUTO_SETTINGS2              21003
#define IDC_KEEP_SETTINGS               21003
#define IDD_SPEC_LIST_DIALOG            21004
#define IDC_REVERT                      21004
#define IDD_SPECTRACHROMECALIBRATIONWIZARD_DIALOG 21005
#define IDC_INIT_WH_BUTTON              21005
#define IDD_SPECTRACHROME_PAGE          21006
#define IDC_SAVE_BUTTON                 21006
#define IDD_LAMP_DIALOG                 21007
#define IDC_NAME_EDIT                   21007
#define IDD_INTRO_PAGE                  21008
#define IDD_INIT_HW_PAGE                21009
#define IDC_AUTO                        21009
#define IDD_FINISH_PAGE                 21010
#define IDC_STATIC1                     21010
#define IDB_WATERMARK256                21011
#define IDC_STATIC2                     21011
#define IDB_BANNER256                   21012
#define IDC_STATIC3                     21012
#define IDB_WATERMARK16                 21013
#define IDC_STATIC4                     21013
#define IDB_BANNER16                    21014
#define IDC_TAB                         21014
#define IDC_SLIDER1                     21015
#define IDC_SLIDER2                     21016
#define IDC_SLIDER3                     21017
#define IDC_SLIDER4                     21018
#define IDD_SPECTRACHROME_DIALOG        21019
#define IDC_NAME_STATIC                 21020
#define IDD_SHADE_IMAGE_DIALOG          21020
#define IDD_FOCUS_PAGE                  21021
#define IDD_DO_IT_ALL                   21022
#define IDD_NDFILTER_SWITCHES           21023
#define IDC_GRAB_SHADE_IMAGE_BUTTON     21024
#define IDC_SAVE_SHADE_IMAGE_BUTTON     21025
#define IDB_NDSwitches                  21026
#define IDB_NDSwitches_BX61             21026
#define IDC_PROGRESS                    21027
#define IDD_SPECTRACHROMESELECTION_DIALOG 21027
#define IDC_LAMP_BUTTON                 21028
#define IDSAVE                          21029
#define IDLOAD                          21030
#define IDC_OBJECTIVE_EDIT              21030
#define IDC_AUTO_FOCUS                  21031
#define IDC_DONE                        21034
#define IDC_DO_IT_ALL                   21036
#define IDC_FLUO_CHECK                  21037
#define IDC_COUNTERSTAIN_CHECK          21038
#define IDC_NDSWITCHES                  21039
#define IDC_SWND6                       21040
#define IDC_SWND25                      21041
#define IDC_SWLBD                       21042
#define IDC_SWOP                        21043
#define IDC_INSTRUCTIONS                21044
#define IDC_SWLBD_STATE                 21045
#define IDC_SWOP_STATE                  21046
#define IDC_SWND25_STATE                21047
#define IDC_SWND6_STATE                 21048
#define IDC_INSTRUCTIONS_2              21049
#define IDC_SAVESTATIC                  21050
#define IDC_COLOURBUTTON                21051
#define IDC_ALLSPECRADIO                21052
#define IDC_BRIGHTFIELDRADIO            21053
#define IDC_FLUORESCENTRADIO            21054
#define ID_OK                           21055
#define ID_OK2                          21056
#define ID_CANCEL                       21056

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        21028
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         21056
#define _APS_NEXT_SYMED_VALUE           21000
#endif
#endif
