// zGeneric.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#include "zGeneric.h"

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}

//extern "C" {


#define asciiSPACE	    0x20
#define asciiCR   	    0x0D
#define asciiLF   	    0x0A



//////////////consts

//=====================  DIB/LUT  STATICs

// This is the same structure as BITMAPINFO except it has a 256 byte LUT
// Ignore the differences between BITMAPINFOHEADER and BITMAPV5HEADER
// We won't use BITMAPV5HEADER
typedef struct       /** Device Independent BITMAPINFO **/
{
	BITMAPINFOHEADER diInfo;
	RGBQUAD          diLUT[256];
} DI_BITMAPINFO;



//==========================  Step Codes:
//        5:  1, 0;  F:  1, 1;  A: 0, 1;  E: -1, 1;
//        4: -1, 0;  C: -1,-1;  8: 0,-1;  D:  1,-1;
typedef enum {
  StepCode00=0,
  StepCodeM0=0x04, StepCodeP0=0x05, StepCode0M=0x08, StepCode0P=0x0A,
  StepCodeMM=0x0C, StepCodePM=0x0D, StepCodeMP=0x0E, StepCodePP=0x0F
} StepCodes;

// Generic bitmapinfo header fill function
void FillBMIH(BITMAPINFOHEADER *bmih, DWORD bitspix, DWORD width, DWORD height);

// Following static data describes the "current DI BITMAP".
// The most important part of the data is the LOOK UP Table (LUT),
// which may be set or get in different ways by calling
// the LookUpTable().  Some functions, like DisplayMem() and
// MemToBitMap(), use current LUT to create the BITMAP object.

static DI_BITMAPINFO LastBMPinfo;
#define LastLUT      (char*)&LastBMPinfo.diLUT[0]
#define LDI          LastBMPinfo.diInfo
static long LastLUTform = 0;	// number of entries in the LUT
static long LastLUTbytes = 0;	// LUT size in bytes

static long MemToHex_Capitals = 1;
static BYTE MemToHex_Service = '.';
static double ygSqr3 = sqrt(3.0), ygPi = 4.0 * atan(1.0);

//  Inversed Bytes Table: bytes <k> and InversedBytes[k]
//  	have an inverse order of bits inside the byte

static char InversedBytes[256]=
{
  0x00,0x80,0x40,0xC0, 0x20,0xA0,0x60,0xE0, 0x10,0x90,0x50,0xD0, 0x30,0xB0,0x70,0xF0,
  0x08,0x88,0x48,0xC8, 0x28,0xA8,0x68,0xE8, 0x18,0x98,0x58,0xD8, 0x38,0xB8,0x78,0xF8,
  0x04,0x84,0x44,0xC4, 0x24,0xA4,0x64,0xE4, 0x14,0x94,0x54,0xD4, 0x34,0xB4,0x74,0xF4,
  0x0C,0x8C,0x4C,0xCC, 0x2C,0xAC,0x6C,0xEC, 0x1C,0x9C,0x5C,0xDC, 0x3C,0xBC,0x7C,0xFC,
  0x02,0x82,0x42,0xC2, 0x22,0xA2,0x62,0xE2, 0x12,0x92,0x52,0xD2, 0x32,0xB2,0x72,0xF2,
  0x0A,0x8A,0x4A,0xCA, 0x2A,0xAA,0x6A,0xEA, 0x1A,0x9A,0x5A,0xDA, 0x3A,0xBA,0x7A,0xFA,
  0x06,0x86,0x46,0xC6, 0x26,0xA6,0x66,0xE6, 0x16,0x96,0x56,0xD6, 0x36,0xB6,0x76,0xF6,
  0x0E,0x8E,0x4E,0xCE, 0x2E,0xAE,0x6E,0xEE, 0x1E,0x9E,0x5E,0xDE, 0x3E,0xBE,0x7E,0xFE,

  0x01,0x81,0x41,0xC1, 0x21,0xA1,0x61,0xE1, 0x11,0x91,0x51,0xD1, 0x31,0xB1,0x71,0xF1,
  0x09,0x89,0x49,0xC9, 0x29,0xA9,0x69,0xE9, 0x19,0x99,0x59,0xD9, 0x39,0xB9,0x79,0xF9,
  0x05,0x85,0x45,0xC5, 0x25,0xA5,0x65,0xE5, 0x15,0x95,0x55,0xD5, 0x35,0xB5,0x75,0xF5,
  0x0D,0x8D,0x4D,0xCD, 0x2D,0xAD,0x6D,0xED, 0x1D,0x9D,0x5D,0xDD, 0x3D,0xBD,0x7D,0xFD,
  0x03,0x83,0x43,0xC3, 0x23,0xA3,0x63,0xE3, 0x13,0x93,0x53,0xD3, 0x33,0xB3,0x73,0xF3,
  0x0B,0x8B,0x4B,0xCB, 0x2B,0xAB,0x6B,0xEB, 0x1B,0x9B,0x5B,0xDB, 0x3B,0xBB,0x7B,0xFB,
  0x07,0x87,0x47,0xC7, 0x27,0xA7,0x67,0xE7, 0x17,0x97,0x57,0xD7, 0x37,0xB7,0x77,0xF7,
  0x0F,0x8F,0x4F,0xCF, 0x2F,0xAF,0x6F,0xEF, 0x1F,0x9F,0x5F,0xDF, 0x3F,0xBF,0x7F,0xFF
};

/*=========================  Step Codes  ============================
        5:  1, 0;  F:  1, 1;  A: 0, 1;  E: -1, 1;
        4: -1, 0;  C: -1,-1;  8: 0,-1;  D:  1,-1;
            Table {0-7} codes:
             6 5 4
          ^  7 8 3
          |  0 1 2
          y  x ->           #8 corresponds to {Xc,Yc}

        0x0C, 0x08, 0x0D, 0x05, 0x0F, 0x0A, 0x0E, 0x04;
===================================================================*/
static char DirToStepCodes[9]={
    StepCodeMM, StepCode0M, StepCodePM, StepCodeP0,
    StepCodePP, StepCode0P, StepCodeMP, StepCodeM0, 0};
static char StepCodesToDirection[16]={
   8, 8, 8, 8, 7, 3, 8, 8, 1, 8, 5, 8, 0, 2, 6, 4};
// 0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
static char StepCodesDX[16]={
   0, 0, 0, 0,-1,+1, 0, 0, 0, 0, 0, 0,-1,+1,-1,+1};
// 0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
static char StepCodesDY[16]={
   0, 0, 0, 0, 0, 0, 0, 0,-1, 0,+1, 0,-1,-1,+1,+1};
// 0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  

/////////////end of consts



/******************************************************* AnyToLong */
/***
	Function:	AnyToLong

	Description:
		This function gets a pointer and returns it as DWORD value;
		it is useful in VB to imitate some operations on pointers.

	Statics/Globals:
		None

	Input Arguments:
		pnt - any pointer

	Output Arguments:
		None

	Returns: input pointer as DWORD

***/
DWORD _stdcall AnyToLong(void *pnt)
{
	union {
		void *addr;
		DWORD dwrd;
	} addwrd;

	addwrd.addr = pnt;
	return addwrd.dwrd;
}


/*************************************************** AnyOffsToLong */
/***
	Function:	AnyOffsToLong

	Description:
		This function gets a pointer and an offset in bytes,
		and returns address (pointer+offset) as long value;
		it is useful in VB to imitate some operations on pointers.

	Statics/Globals:
		None

	Input Arguments:
		pnt - any pointer
		offs - offset in bytes

	Output Arguments:
		None

	Returns: (input pointer + offset) as a long

***/
DWORD _stdcall AnyOffsToLong(void *pnt, DWORD offs)
{
	union {
		void *addr;
		DWORD dwrd;
	} addwrd;

	addwrd.addr = ((char *)pnt) + offs;
	return addwrd.dwrd;
}


/******************************************************* AddVector */
/*********
 	Calculates for 2 vectors x, y and a factor:

    	x += ( factor * y);

 	<x>, <y> - pointers to the (pitched) vector arrays
 	<pitchx>, <pitchy> - pithes IN BYTES (!)
 	<n> 	  - number of (double) elements in <x> and <y>
    factor - see above

*********/
void _stdcall AddVector (void *x, void *y,
				long pitchx, long pitchy,
                long n, double factor)
{
    BYTE *bx, *by;

    bx = (BYTE *)x;  by = (BYTE *)y;
   	while (n--)
   	{
        *(double *)bx += ((*(double *)by) * factor);
        bx += pitchx;   by += pitchy;
    }
}



/****************************************************** BitsDiffer */
// compares 2 masks; returns 0 if masks are equal, not zero otherwise
// NB: <xe> in bits
long _stdcall BitsDiffer (
			void *p1, void *p2,
            long xe, long ye,
            long pitch1, long pitch2)
{
    DWORD lmask, dwcount, shft, xcnt, ycnt,
    	 *pl1, *pl2;
    BYTE *mem1, *mem2;

    mem1 = (BYTE *)p1;  mem2 = (BYTE *)p2;
    dwcount = (xe + 31) >> 5;
    shft = (dwcount << 5) - xe;
    lmask = 0xffffffff >> shft;
    for (ycnt = 0;  ycnt < (DWORD)ye;  ycnt++)
    {
        pl1 = (DWORD *)mem1;  pl2 = (DWORD *)mem2;
        for (xcnt = 0;  xcnt < dwcount;  xcnt++)
        {
            if (pl1[xcnt] != pl2[xcnt])
            {
           	    if ((xcnt < dwcount - 1) ||
            	    ((pl1[xcnt] ^ pl2[xcnt]) & lmask))
                		return 1;
            }
        }
        mem1 += pitch1;  mem2 += pitch2;
    }
    return 0;
}


/**************************************************** BitTranspose */
// transposes bit matrix
// NB: <xe> in bits
void _stdcall BitTranspose (
			void *p1, void *p2,
            long x1, long y1,
            long pitch1, long pitch2)
{
    DWORD r1, c1, cc;
    BYTE lor, test, *mem1, *mem2, *row, *col;

    mem1 = (BYTE *)p1;  mem2 = (BYTE *)p2;
    for (r1 = 0;  r1 < (DWORD)x1;  r1++)
    {
	    col = mem1 + (r1 >> 3);
        test = 1 << (r1 & 7);
        row = mem2 + (r1 * pitch2);
        for (c1 = 0;  c1 < (DWORD)y1;  c1++)
        {
            cc = c1 >> 3;  lor = 1 << (c1 & 7);
         	if ((*col) & test) row[cc] |= lor;
            else row[cc] &= (0xff ^ lor);
            col += pitch1;
        }
    }
}



/**************************************************** BMtoClpbrd */
/***
	Function:	BMtoClpbrd

	Description:
		This function copyes the BitMap object to
		the system Clipboard

	Statics/Globals:
		None

	Input Arguments:
		wndHandle - Window Handle needed by OpenClipboard()
				WINDOWS API

		bmpHandle - Handle of the BITMAP object to put on
				the system Clipboard


	Output Arguments:
		None

	Returns:
		1 if OK, 0 otherwise
***/
long _stdcall BMtoClpbrd (long wndHandle, // NULL is Ok
                        long bmpHandle)
{
	HWND hnwnd;
	HBITMAP hnbmp;
	long ret = 0;

	if (wndHandle) hnwnd = (HWND)wndHandle;
   else hnwnd = GetActiveWindow();
	hnbmp = (HBITMAP)bmpHandle;
	if (!OpenClipboard(hnwnd)) goto Fin;
	if (!EmptyClipboard()) goto Fin;
	if (!SetClipboardData(CF_BITMAP, hnbmp)) goto Fin; // CF_DIB ?
	if (!CloseClipboard()) goto Fin;
   ret = 1;
Fin:
        // No matter what, close the clipboard so others can use it - dcb
        CloseClipboard();

   return ret;
}



/************************************************** ScreenToBitMap */
/***
	You can use a bitmap to capture an image, and you can store
	the captured image in memory, display it at a different
	location in your application's window, or display it in
	another window.
	In some cases, you may want your application to capture
	images and store them only temporarily. For example, when you
	scale or "zoom" a picture created in a drawing application,
	the application must temporarily save the normal view of the
	image and display the zoomed view. Later, when the user
	selects the normal view, the application must replace the
	zoomed image with a copy of the normal view that it temporarily
	saved.
	To store an image temporarily, your application must call
	CreateCompatibleDC to create a DC that is compatible with the
	current window DC. After you create a compatible DC, you create
	a bitmap with the appropriate dimensions by calling the
	CreateCompatibleBitmap function and then select it into this
	device context by calling the SelectObject function.
	After the compatible device context is created and the
	appropriate bitmap has been selected into it, you can capture
	the image. The Win32 API provides the BitBlt function to capture
	images. This function performs a bit block transfer � that is,
	it copies data from a source bitmap into a destination bitmap.
	Because it copies data from bitmaps, you'd expect that two
	arguments to this function would be bitmap handles; however,
	this is not the case. Instead, BitBlt receives handles that
	identify two device contexts and copies the bitmap data from
	a bitmap selected into the source DC into a bitmap selected
	into the target DC. In this case, the target DC is the compatible
	DC, so when BitBlt completes the transfer, the image has been
	stored in memory. To redisplay the image, call BitBlt a second
	time, specifying the compatible DC as the source DC and a window
	(or printer) DC as the target DC.
	The following example code, from an application that captures an
	image of the entire desktop, creates a compatible device context
	and a bitmap with the appropriate dimensions, selects the bitmap
	into the compatible DC, and then copies the image using the BitBlt
	function.
***/
DWORD _stdcall ScreenToBitMap (long xl, long yt, long wid, long hei)
{
	HDC hdcScreen, hdcCompatible = 0;
   	HBITMAP hbmp = 0;

	/* Create a normal DC and a memory DC for the entire screen. The
	 * normal DC provides a "snapshot" of the screen contents. The
	 * memory DC keeps a copy of this "snapshot" in the associated
	 * bitmap. */
	hdcScreen = CreateDC("DISPLAY", NULL, NULL, NULL);
	if (!hdcScreen) goto Fin;
	hdcCompatible = CreateCompatibleDC(hdcScreen);
	if (!hdcCompatible) goto Fin;
	/* Create a compatible bitmap for hdcScreen. */
	hbmp = CreateCompatibleBitmap(hdcScreen, wid, hei);
	if (!hbmp) goto Fin;
	/* Select the bitmaps into the compatible DC. */
	if (!SelectObject(hdcCompatible, hbmp)) goto Fin;
	/* Copy color data from the display into a
   bitmap that is selected into a compatible DC. */
   	BitBlt(hdcCompatible, 0, 0, wid, hei,
                 hdcScreen, xl, yt, SRCCOPY);
Fin:
	if (hdcScreen) DeleteDC (hdcScreen);
   	if (hdcCompatible) DeleteDC (hdcCompatible);
   	return (DWORD)hbmp;
}


/************************************************** Convert32to8 */
/***
	Function:	Convert32to8

	Description:

	Arguments:
		p8 - pointer to 8 bit per pixel rectangle memory location
		p32 - pointer to 32 bit per pixel rectangle memory location
		pitch8, pitch32 - address differences between neighboring rows
		xe, ye - common dimensions of both rectangles
        shift - convert DWORDs to BYTEs with given right bit shift
        	if ((shift == -1) and (max pixel > 255))
            	then puts most significant bit of
                the maximal pixel value into 0x80 bit
        	if (shift == -2)
            	then makes MulDiv to max=255
        returns:  pmax

***/
DWORD _stdcall Convert32to8 (
			void *p32, DWORD pitch32,
            DWORD xe, DWORD ye,
			void *p8, DWORD pitch8,
            long shift)
{
	DWORD i, k, div, shf, pmax, *w32;
    BYTE *r8, *wrk;

    // find max (unsigned) pixel value:
	r8 = (BYTE *)p8;    wrk = (BYTE *)p32;    pmax = 0;
	for (i = 0;  i < ye;  i++)
    {
    	w32 = (DWORD *)wrk;
		for (k = 0;  k < xe;  k++)
        	if (pmax < w32[k]) pmax = w32[k];
		wrk += pitch32;
	}
    wrk = (BYTE *)p32;  shf = shift;
    if (shift >= 0) goto ShiftTransform;
    shf = 0;
    if ((div = pmax) <= 0xff) goto ShiftTransform;
    if (shift == -1)
    { // find shift for pmax:
        while (div > 255) { div >>= 1;  shf++; }
    	goto ShiftTransform;
    }
    // here ((shift == -2) && (pmax > 0))
    while (div > 0xffffff) { div >>= 1;  shf++; }
	for (i = 0;  i < ye;  i++)
    {
    	w32 = (DWORD *)wrk;
		for (k = 0;  k < xe;  k++)
        	r8[k] = (BYTE)((w32[k] >> shf) * 255 / div);
		r8 += pitch8;   wrk += pitch32;
	}
    goto Fin;
ShiftTransform:
    wrk = (BYTE *)p32;
	for (i = 0;  i < ye;  i++)
    {
    	w32 = (DWORD *)wrk;
		for (k = 0;  k < xe;  k++)
        	r8[k] = (BYTE)(w32[k] >> shf);
		r8 += pitch8;   wrk += pitch32;
	}
Fin:
    return pmax;
}


/************************************************** Convert8to32 */
/***
	Function:	Convert8to32

	Description:
	Arguments:
		p8 - pointer to 8 bit per pixel rectangle memory location
		p32 - pointer to 32 bit per pixel rectangle memory location
		pitch8, pitch32 - address differences between neighboring rows
		xe, ye - common dimensions of both rectangles
        shift - convert BYTEs to DWORDs with given left bit shift

***/
void _stdcall Convert8to32 (
			void *p8, DWORD pitch8,
            DWORD xe, DWORD ye,
			void *p32, DWORD pitch32,
            DWORD shift)
{
	DWORD i, k, *w32;
    BYTE *r8, *wrk;

	r8 = (BYTE *)p8;  wrk = (BYTE *)p32;
	for (i = 0;  i < ye;  i++)
    {
    	w32 = (DWORD *)wrk;
		for (k = 0;  k < xe;  k++)
        	w32[k] = ((DWORD)r8[k]) << shift;
		r8 += pitch8;   wrk += pitch32;
	}
}



/****************************************************** FindSubstr */
/***
	Function:	FindSubstr

	Description:
		This function searches for given substring
        in a given part of text:
			from  ptext+offs  to  ptext+limit
		then updates the <offs> value
        if (usecase) case sensitive search

	Arguments:
		ptext - pointer to the beginning of a piece of memory
		offs - pointer to (DWORD)variable which value is an offset
			from <ptext> to the beginning point of searching
		limit - byte at address <ptext+limit> is the first
			outside of the searched area (if 0 - strlen(text))
		substr - pointer to the beginning of a byte list
        sublen = number of bytes in the substring (if 0 - strlen(substr))

    Returns:
     if found - offset after the substring, and sets <offs> to
                offset before the substring
     if not found - 0, and sets <offs> to <limit> value

***/
long _stdcall FindSubstr (
					void *ptext,
                    DWORD *offs, DWORD limit,
                    void *substr, DWORD sublen,
                    DWORD usecase)
{
	DWORD boffs, slim;
    BYTE firstl, firsth, curr, *text, *sub;
    char firsts[4] = {0, 0, 0, 0};
    long ret = 0;
    bool equal;

    text = (BYTE *)ptext;  sub = (BYTE *)substr;
    if ((!text) || (!sub)) goto Fin;
   	if (!limit) limit = strlen((const char *) text);
   	if (!sublen) sublen = strlen( (const char *) sub);
	boffs = *offs;  *offs = limit;
    firstl = firsth = *sub;  slim = limit - sublen;
    if (!usecase)
    {
        firsts[0] = firsts[2] = firstl;
        _strlwr(firsts);  
        _strupr(firsts + 2);
        firstl = firsts[0];  
        firsth = firsts[2];
    }
	while (boffs <= slim)
   	{
        curr = text[boffs];
      	if ((curr == firstl) || (curr == firsth))
        {

            if (usecase)
            {
                equal = (memcmp(text + boffs, sub, sublen) == 0);
            } else
            {
                equal = (_memicmp(text + boffs, sub, sublen) == 0);
            }
            if (equal)
      		{
                *offs = boffs;          // set result
       			ret = boffs + sublen;   // and return value
                break;
            }
      	}
        boffs++;
   	}
Fin:
	return ret;
}



/******************************************************** FindByte */
/***
	Function:	FindByte

	Description:
		This function searches for the byte from given
        list of bytes in a given part of text:
			from  begtext+offs  to  begtext+limit
		then updates the <offs> value

	Arguments:
		ptext - pointer to the beginning of a piece of memory
		offs - pointer to (DWORD)variable which value is an offset
			from <begtext> to the beginning point of searching
		limit - byte at address <begtext+limit> is the first
			outside of the searched area (if 0 - strlen(text))
		plist - pointer to the beginning of a byte list
        listlen = number of bytes in the list (if 0 - strlen(plist))

    Returns:
     if found - number of found byte in the list
     			(beginning from 1), and sets <offs> to
                offset of the byte
     if not found - 0, and sets <offs> to <limit> value

***/
long _stdcall FindByte (
					void *ptext,
                    DWORD *offs, DWORD limit,
                    void *plist, DWORD listlen)
{
	DWORD boffs, bcnt;
    BYTE current, *text, *list;
    long ret = 0;

    text = (BYTE *)ptext;  list = (BYTE *)plist;
    if ((!text) || (!list)) goto Fin;
   	if (!limit) limit = strlen((const char*) text);
   	if (!listlen) listlen = strlen((const char*) list);
	boffs = *offs;  *offs = limit;
	while (boffs < limit)
   	{
      	current = text[boffs];     bcnt = 0;
        while (bcnt < listlen)
      	{
            if (list[bcnt++] == current)
            {
       			ret = bcnt;		*offs = boffs;	// assign results
                boffs = limit;  // to fihish extern loop
                break; 	// from internal loop
            }
      	}
        boffs++;
   	}
Fin:
	return ret;
}



/***************************************************** Gamma8Table */
/*****
    Calculates 8 bit/value lookup table with given Gamma:
    for i=0 to 255
    	tab[i] = 255 * (i / 255) ** Gamma = (255**(1-Gamma)) * (i**Gamma);
*****/
void _stdcall Gamma8Table(BYTE *tab, double Gamma)
{
    double factor, igamma;
    long i, lval;

    if (Gamma < 0.0) Gamma = 0.0;
    else if (Gamma > 100.0) Gamma = 100.0;
    tab[0] = 0;  tab[255] = 255;
    factor = exp((1.0 - Gamma) * log((long double)255));	// (255**(1-Gamma))
	for (i = 1;  i < 255;  i++)
    {
        igamma = exp(Gamma * log((double)i)); // i**Gamma
        lval = floor(factor * igamma + 0.5); //rounding
     	tab[i] = (BYTE)lval;
    }
}



/********************************************************* GetLong */
/***
	Function:	GetLong

	Description:
		This function searches for the beginning of the next word,
		i.e. first more-than-space symbol in a given part of text:
			from  begtext+offs  to  begtext+limit
		then tryes to convert next symbols in a long value.
		If OK - updates offset and the value;
		returns the length of the long value presentation

	Statics/Globals:
		None

	Input Arguments:
		begtext - pointer to the beginning of a piece of memory

		offs - pointer to (DWORD) variable which value is an offset
			from <begtext> to the beginning point of searching
			long value presentation

		limit - byte at address <begtext+limit> is the first one
			outside of the searched area

	Output Arguments:
		offs - after return is updated (if OK) to the offset
			of the byte next after the long value presentation

		value - pointer to (long) variable which gets the long value
			(if OK)

	Returns:
		if OK - length of the long value presentation in the text,
		0 otherwise

***/
DWORD _stdcall GetLong (
			   char  *pnt,
               DWORD *offs,
               DWORD  limit,
               long  *value)
{
   	DWORD i, k, m;
   	long v, ret;
   	char *q, buff[32];

   	k = *offs;
   	while ((k < limit) && (pnt[k] <= asciiSPACE))
   	 	k++;
   	if (limit > k + 31) limit = k + 31;
   	m = k;  i = 0;
   	while ((k < limit) && (pnt[k] > asciiSPACE))
	    buff[i++] = pnt[k++];
 	buff[i] = 0;
   	if (buff[0] == '-')	v = strtol (buff, &q, 0);
    else v = strtoul (buff, &q, 0);
   	if ((ret = (long)(q - buff)) > 0)
   	{   // update value and offset:
	  	*value = v;   *offs = m + ret;
   	} else ret = 0;
   	return (DWORD)ret;
}



/******************************************************* GetDouble */
/***
	Function:	GetDouble

	Description:
		This function searches for the beginning of the next word,
		i.e. first more-than-space symbol in a given part of text:
			from  begtext+offs  to  begtext+limit
		then tryes to convert next symbols in a double value.
		If OK - updates offset and the value;
		returns the length of the double value presentation

	Statics/Globals:
		None

	Input Arguments:
		begtext - pointer to the beginning of a piece of memory

		offs - pointer to (DWORD) variable which value is an offset
			from <begtext> to the beginning point of searching
			long value presentation

		limit - byte at address <begtext+limit> is the first one
			outside of the searched area

	Output Arguments:
		offs - after return is updated (if OK) to the offset
			of the byte next after the double value presentation

		value - pointer to (Double) variable which gets the double
			value (if OK)

	Returns:
		if OK - length of the double value presentation in the text,
		0 otherwise

***/
DWORD _stdcall GetDouble (
					char   *pnt,
               DWORD  *offs,
               DWORD   limit,
               double *value)
{
   double v;
	DWORD i, k, m;
	long ret;
	char *q, buff[32];

	k = *offs;
	while ((k < limit) && (pnt[k] <= asciiSPACE))
		 k++;
	if (limit > k + 31) limit = k + 31;
	m = k;  i = 0;
	while ((k < limit) && (pnt[k] > asciiSPACE))
		 buff[i++] = pnt[k++];
	buff[i] = 0;
	v = strtod (buff, &q);
   if ((ret = (long)(q - buff)) > 0)
   {   // update value and offset:
       *value = v;   *offs = m + ret;
   } else ret = 0;
   return (DWORD)ret;
}



/***************************************************** GetVicinity */
/***
	Function:	GetVicinity

	Description:
		This function returns the neighborhood of a given pixel
        as a bit set where number of bit corresponds to:

             6 5 4
          ^  7 8 3
          |  0 1 2
          y  x ->           #8 corresponds to {Xc,Yc}

        5:  1, 0;  F:  1, 1;  A: 0, 1;  E: -1, 1;
        4: -1, 0;  C: -1,-1;  8: 0,-1;  D:  1,-1;
            Table {0-7} => code:
        0x0C, 0x08, 0x0D, 0x05, 0x0F, 0x0A, 0x0E, 0x04;

	Statics/Globals:
		None

	Input Arguments:
		pmask - pointer to the Bit Mask
        Xc, Yc - coordinates of the central pixel

		xe - number of pixels (bits) in each row
		ye - number of rows
		pitch - address difference between neighboring rows (in bytes)
				in the limiting Bit Mask

	Returns:
		bit set (see above) if OK,
        -1 if bad coordinates

***/
long _stdcall GetVicinity (void *pmask,
                long xe, long ye, long pitch,
                long Xc, long Yc)
{
	DWORD p, c, n;
    long offsx, shift, sh08, ret = -1;
    BYTE *w;
    DWORD *prv, *cur, *nxt;

    if ((Xc < 0) || (Xc >= xe) || (Yc < 0) || (Yc >= ye))
        goto Fin;
    p = c = n = sh08 = 0;   shift = Xc & 7;
    if ((offsx = Xc >> 3) > 0) offsx--;  else sh08 = 8;
    w = (BYTE *)pmask;    w += (Yc * pitch + offsx);
    cur = (DWORD *)w;     c = ((*cur) << sh08) >> shift;
    if (Yc)
    {
        prv = (DWORD *)(w - pitch);
        p = ((*prv) << sh08) >> shift;
    }
    if (Yc != ye - 1)
    {
        nxt = (DWORD *)(w + pitch);
        n = ((*nxt) << sh08) >> shift;
    }
    ret = (p >> 7) & 0x07;      // 0,1,2
    ret |= ((c >> 6) & 0x08);   // 3
    ret |= (c & 0x180);         // 7,8
    ret |= ((n >> 5) & 0x10);   // 4
    ret |= ((n >> 3) & 0x20);   // 5
    ret |= ((n >> 1) & 0x40);   // 6
    if (Xc == xe - 1) ret &= 0x1e3;  // 2,3,4 set to 0
Fin:
	return ret;
}



/******************************************** GlobHndlToClpbrd */
/***
	Function:	GlobHndlToClpbrd

	Description:
		This function puts a global Windows buffer given
		by its handle and on the system Clipboard as an
		object of a given clipboard type

	Statics/Globals:
		None

	Input Arguments:
		FormatName - pointer to (0-terminated) Clipboard Format Name

		globhndl - Windows Handle of a Globally Allocated Buffer

	Output Arguments:
		None

	Returns:  1 if OK, 0 otherwise

***/
long _stdcall GlobHndlToClpbrd (
								char *FormatName,
                        DWORD globhndl)
{
	 long ret = 0;
	 HGLOBAL ghndl;
	 UINT hClipFormat;

	 ghndl = (HGLOBAL)globhndl;
	 if (!ghndl) goto Fin;
	 hClipFormat = RegisterClipboardFormat(FormatName);
	 if (OpenClipboard(GetActiveWindow()))
	 {
		  if (EmptyClipboard())
		  {
			 if (SetClipboardData (hClipFormat, ghndl))
			 {
				 if (CloseClipboard())
					 ret = 1;
			 }
		}
	}
Fin:
	return ret;
}



/**************************************************** GroupPermut */
/***
	Function:	GroupPermut

	Description:
		This function copies a given buffer, separated
        on "groups" <grouplen> bytes each (<=256), with
        permutation of bytes inside each group;
        the permutation is given by sequence of <group>
        bytes: dest[i] = src[permutation[i]],
        where 0 <= i < grouplen

	Statics/Globals:

	Input Arguments:
		src	- pointer to the source buffer
		dest - pointer to the target buffer
        grouplen - number of bytes in a group
        permutation - pointer to table to permute
		size - (common) size of both buffers (in bytes)

	Output Arguments:
		None

	Returns: None

***/
void _stdcall GroupPermut (char *src, char *dest,
				DWORD grouplen, BYTE *permutation, DWORD size)
{
	DWORD i;
    BYTE *s, *d, buff[256];

    s = (BYTE *)src;  d = (BYTE *)dest;
	while (size >= grouplen)
	{
        for (i = 0;  i < grouplen; i++)
        	buff[i] = s[permutation[i]];
        for (i = 0;  i < grouplen; i++)
        	d[i] = buff[i];
		s += grouplen;
		d += grouplen;
		size -= grouplen;
	}
}



/******************************************************** HistInfo */
/*****
	This function calculates some statistics for the histogram
	given by frequency array.

	Arguments:
		hist - pointer to the beginning of the histogram array
        		of long values
		histlen - number of cells in the histogram array;
        		cells correspond to [0,1), [1,2),...,
                 [histlen-1,histlen] intervals
        info - pointer to double array of output data;
        		must be at least 16 doubles in size

     info[0] = Np  	: number of positive values in the histogram
     info[1] = Imin: min I where Hist[I] is positive
     info[2] = Imax: max I where Hist[I] is positive
     info[3] = Sn  	: Sum of all values Hist(I)
     info[4] = Av	: Average: (Sum(Hist(I) * I)) / Sn
     info[5] = D	: Dispersion: (Sum(Hist(I) * (I - Av) ^ 2)) / Sn
     info[6] = Bord	: Best separating value
     					(is one of 0.5, 1.5, ... , histlen - 0.5)
     info[7] = Sl	: Sum Hist(I) for I=0 to Bord- (==Ent(Bord))
     info[8] = Sr	: Sum Hist(I) for I=Ceil(Bord+) to histlen-1
     info[9] = Al	: Left Average: (Sum{0 to Bord-}(Hist(I) * I)) / Sl
     info[10] = Ar	: Right Average: (Sum{Bord+ to histlen-1}(Hist(I) * I)) / Sr
     info[11] = Dl	: Left Dispersion:
     					(Sum{0 to Bord-}(Hist(I) * (I - Al)^2) / Sl
     info[12] = Dr	: Right Dispersion:
     					(Sum{Bord+ to histlen-1}(Hist(I) * (I - Ar)^2) / Sr
     info[13] = Cs	: Coefficient of best separation:
     					(((Sl*Sr) / (Sn*Sn)) * (Ar - Al) ^ 2) / (D + 1/12)
     info[14] = Mode : Max value in the histogram
     info[15] = ModePos : Position of mode

    Returns: number of positive values in the histogram
    		(same as info[0])
*****/
long _stdcall HistInfo(
				long *hist, long histlen, double *info)
{
   	long np, sn, i, hi, imin, imax, ibest, inext, prev, mode, modepos;
    double av, d2, d, dsn, si, sl, sr, slw, srw, avl, avr,
    		dav, sli, sli2, sri,
    		avlw, avrw,	dl, cs, sp, norm;

	av = avl = avr = d2 = 0.0;
    np = sn = si = modepos = 0;
    imin = 0;  imax = histlen - 1;  mode = hist[0];
    for (i = 0;  i < histlen;  i++)
    {
     	if ((hi = hist[i]) > 0)
        {
            if (hi > mode) { mode = hi;  modepos = i; }
         	if (np) imax = i;  else imin = imax = i;
        	np++;  sn += hi;
            si += (double)hi * i;
            d2 += (double)hi * i * i;
        }
    }
    info[0] = np;
    info[1] = imin;
    info[2] = imax;
    if (!np) goto Fin;  // no positives
    dsn = sn;   av = (double)si / dsn;
    d = (d2 / dsn) - av * av;
    info[3] = sn;
    info[4] = av;
    info[5] = d;  if (np == 1) goto Fin;  // just 1 positive
    sl = 0;  slw = sli = sli2 = 0.0;   cs = -1.0;
    norm = dsn * dsn * (d + 0.08333333333); // 1/12
    prev = 0;
	inext = imax;
    for (i = imin;  i < imax;  i++)
    {
     	if ((hi = hist[i]) > 0)
        {
            if (prev) { prev = 0;  inext = i;  }
            slw += hi;
            sli += (double)hi * i;
            sli2 += (double)hi * i * i;
            srw = sn - slw;  sri = si - sli;
            avlw = sli / slw;    avrw = sri / srw;   dav = avrw - avlw;
            sp = (slw * srw * dav * dav) / norm;
            if (sp > cs)
            {
             	cs = sp;  ibest = i;  avl = avlw;  avr = avrw;
                sl = slw;  sr = srw;  dl = sli2;   prev = 1;
            }
        }
    }
    info[6] = 0.5 * (ibest + inext);
    info[7] = sl;
    info[8] = sr;
    info[9] = avl;
    info[10] = avr;
    info[11] = (dl / sl) - avl * avl;
    info[12] = ((d2 - dl) / sr) - avr * avr;
    info[13] = cs;
    info[14] = (double)mode;
    info[15] = (double)modepos;
Fin:
   	return np;
}




/************************************************** IndexSortMerge */
/***
	Function:	IndexSortMerge

	Description:

	Statics/Globals:

	Arguments:

	Returns: None

***/
void IndexSortMerge (long *src,
		long numof1, long numof2, long *dst,
      	long ( _stdcall *compfunc)(long, long, void *),
      	void *extrainfo) // NULL is OK
{
	long i1, i2, ii1, ii2, k, comp;

    i1 = k = 0;  i2 = numof1;   numof2 +=  numof1;
    while ((i1 < numof1) && (i2 < numof2))
    {
        ii1 = src[i1];  ii2 = src[i2];
    	comp = compfunc(ii1, ii2, extrainfo);
        if (comp <= 0) { dst[k++] = ii1;  i1++; }
        else { dst[k++] = ii2;  i2++; }
    }
    while (i1 < numof1) dst[k++] = src[i1++];
    while (i2 < numof2) dst[k++] = src[i2++];
}



/******************************************************* IndexSort */
/***
	Function:	IndexSort

	Description:

	Statics/Globals:

	Arguments:

	Returns: None

***/

void _stdcall IndexSort (long *indmem, long numof,
      long (_stdcall *compfunc)(long, long, void *),
      void *extrainfo) // NULL is OK
{
	long i1, i2, n1, n2, d, odd;
    long *from, *to, *wrkmem = NULL;

    if (numof < 1) goto Fin;
    wrkmem = (long *)malloc(numof * sizeof(long));
    if (!wrkmem) goto Fin;
    for (i1 = 0;  i1 < numof;  i1++) indmem[i1] = i1;
    odd = 0;  d = 1;
    while (d < numof)
    {
     	i1 = 0;
        while (i1 < numof)
        {
        	i2 = i1 + d;  n1 = n2 = d;
        	if (i2 > numof) { n1 = numof - i1;  i2 = numof; }
        	if (i2 + n2 > numof) n2 = numof - i2;
	        if (odd) { from = wrkmem + i1;  to = indmem + i1; }
    	    else     { from = indmem + i1;  to = wrkmem + i1; }
			IndexSortMerge (from, n1, n2, to, compfunc, extrainfo);
            i1 = i2 + d;
        }
        odd ^= 1;  d += d;
    }
    if (odd)
    {
    	for (i1 = 0;  i1 < numof;  i1++)
        	indmem[i1] = wrkmem[i1];
    }
Fin:
    if (wrkmem) free(wrkmem);
}



/***********************  Compare Functions  ***********************/
typedef struct
{
	char *addr;
	long pitch;
    long decrease;
	long comptype;
} AddrPitchDecr;

long _stdcall CompLongArray(long i1, long i2, void *extrainfo)
{
	AddrPitchDecr *ap;
    long *a1, *a2;

    ap = (AddrPitchDecr *)extrainfo;
    a1 = (long *)(ap->addr + (i1 * ap->pitch));
    a2 = (long *)(ap->addr + (i2 * ap->pitch));
    if (ap->decrease) return ((*a2) - (*a1));
    else return ((*a1) - (*a2));
}

long _stdcall CompDwordArray(long i1, long i2, void *extrainfo)
{
	AddrPitchDecr *ap;
    DWORD l1, l2, *a1, *a2;

    ap = (AddrPitchDecr *)extrainfo;
    a1 = (DWORD *)(ap->addr + (i1 * ap->pitch));
    a2 = (DWORD *)(ap->addr + (i2 * ap->pitch));
    if (ap->decrease) { l1 = *a2;  l2 = *a1; }
    else { l1 = *a1;  l2 = *a2; }
    if (l1 == l2) return 0;
    if (l1 > l2) return 1;
    return -1;
}

long _stdcall CompShortArray(long i1, long i2, void *extrainfo)
{
	AddrPitchDecr *ap;
   	short *a1, *a2;

   	ap = (AddrPitchDecr *)extrainfo;
   	a1 = (short *)(ap->addr + (i1 * ap->pitch));
   	a2 = (short *)(ap->addr + (i2 * ap->pitch));
    if (ap->decrease) return (long)((*a2) - (*a1));
    else return (long)((*a1) - (*a2));
}

long _stdcall CompUshortArray(long i1, long i2, void *extrainfo)
{
	AddrPitchDecr *ap;
    WORD w1, w2, *a1, *a2;


    ap = (AddrPitchDecr *)extrainfo;
    a1 = (WORD *)(ap->addr + (i1 * ap->pitch));
    a2 = (WORD *)(ap->addr + (i2 * ap->pitch));
    if (ap->decrease) { w1 = *a2;  w2 = *a1; }
    else { w1 = *a1;  w2 = *a2; }
    if (w1 == w2) return 0;
    if (w1 > w2) return 1;
    return -1;
}

long _stdcall CompDoubleArray(long i1, long i2, void *extrainfo)
{
	AddrPitchDecr *ap;
    double diff, *a1, *a2;

    ap = (AddrPitchDecr *)extrainfo;
    a1 = (double *)(ap->addr + (i1 * ap->pitch));
    a2 = (double *)(ap->addr + (i2 * ap->pitch));
    if (ap->decrease) diff = *a2 - *a1;
    else diff = *a1 - *a2;
    if (diff < 0.0) return -1;
    if (diff > 0.0) return 1;
    return 0;
}

long _stdcall CompFloatArray(long i1, long i2, void *extrainfo)
{
	AddrPitchDecr *ap;
    float diff, *a1, *a2;

    ap = (AddrPitchDecr *)extrainfo;
    a1 = (float *)(ap->addr + (i1 * ap->pitch));
    a2 = (float *)(ap->addr + (i2 * ap->pitch));
    if (ap->decrease) diff = *a2 - *a1;
    else diff = *a1 - *a2;
    if (diff < 0.0) return -1;
    if (diff > 0.0) return 1;
    return 0;
}

long _stdcall CompCharArray(long i1, long i2, void *extrainfo)
{
	AddrPitchDecr *ap;
    char *a1, *a2;

    ap = (AddrPitchDecr *)extrainfo;
    a1 = (char *)(ap->addr + (i1 * ap->pitch));
    a2 = (char *)(ap->addr + (i2 * ap->pitch));
    if (ap->decrease) return (long)((*a2) - (*a1));
    else return (long)((*a1) - (*a2));
}

long _stdcall CompByteArray(long i1, long i2, void *extrainfo)
{
	AddrPitchDecr *ap;
    BYTE b1, b2, *a1, *a2;

    ap = (AddrPitchDecr *)extrainfo;
    a1 = (BYTE *)(ap->addr + (i1 * ap->pitch));
    a2 = (BYTE *)(ap->addr + (i2 * ap->pitch));
    if (ap->decrease) { b1 = *a2;  b2 = *a1; }
    else { b1 = *a1;  b2 = *a2; }
    if (b1 == b2) return 0;
    if (b1 > b2) return 1;
    return -1;
}

long _stdcall CompCharPntArray(long i1, long i2, void *extrainfo)
{
	AddrPitchDecr *ap;
    char *p1, *p2, *a1, *a2;
   	long len1, len2, lenmin, ret;

    ap = (AddrPitchDecr *)extrainfo;
    a1 = (char *)(ap->addr + (i1 * ap->pitch));
    a2 = (char *)(ap->addr + (i2 * ap->pitch));
    if (ap->decrease) { p1 = a2;  p2 = a1; }
    else { p1 = a1;  p2 = a2; }
    // for type 10 a string is located inside the element
    if (ap->comptype == 6)
    { // there is a pointer:
    	p1 = *(char **)p1;  p2 = *(char **)p2;
    }
    if ((p1) && (p2))
    {
    	lenmin = len1 = strlen(p1);
	    if ((len2 = strlen(p2)) < len1) lenmin = len2;
        if (lenmin)
        { // compare case insensitive:
	    	if ((ret = _memicmp (p1, p2, lenmin)) == 0)
        		ret = len1 - len2;
        } else
        {
	    	if (!len1)
    	    {
        		if (!len2) ret = 0;  else ret = -1;
	    	} else ret = 1;
        }
    } else
    {
    	if (!p1)
        {
        	if (!p2) ret = 0;  else ret = -1;
    	} else ret = 1;
    }
    return ret;
}

long _stdcall CompBytePntArray(long i1, long i2, void *extrainfo)
{
	AddrPitchDecr *ap;
    BYTE *p1, *p2, *a1, *a2;
   	long len1, len2, lenmin, ret;

    ap = (AddrPitchDecr *)extrainfo;
    a1 = (BYTE *)(ap->addr + (i1 * ap->pitch));
    a2 = (BYTE *)(ap->addr + (i2 * ap->pitch));
    if (ap->decrease) { p1 = a2;  p2 = a1; }
    else { p1 = a1;  p2 = a2; }
    // for type 11 a string is located inside the element
    if (ap->comptype == 7)
    { // there is a pointer:
    	p1 = *(BYTE **)p1;  p2 = *(BYTE **)p2;
    }
    if ((p1) && (p2))
    {
    	lenmin = len1 = strlen((const char*) p1);
	    if ((len2 = strlen((const char*) p2)) < len1) lenmin = len2;
        if (lenmin)
        { // compare as unsighed:
	    	if ((ret = memcmp (p1, p2, lenmin)) == 0)
        		ret = len1 - len2;
        } else
        {
	    	if (!len1)
    	    {
        		if (!len2) ret = 0;  else ret = -1;
	    	} else ret = 1;
        }
    } else
    {
    	if (!p1)
        {
        	if (!p2) ret = 0;  else ret = -1;
    	} else ret = 1;
    }
    return ret;
}


/************************************************** IndexSortElems */
/***
	Description:

	Statics/Globals:

	Arguments:
	comparetypes:
        0 - char
        1 - byte
        2 - short
        3 - word
        4 - long
        5 - dword
        6 - *char (C-string, case insensitive)
        7 - *byte (C-string, case sensitive)
        8 - double
        9 - float
        	string inside the element:
       10 - char C-string, case insensitive
       11 - byte C-string, case sensitive
	decrease: if != 0 then decreasing order
	Returns: None

***/
static long( _stdcall *compfuncs[])(long,long,void*)=
{
     CompCharArray, 	CompByteArray,
     CompShortArray, 	CompUshortArray,
     CompLongArray, 	CompDwordArray,
     CompCharPntArray, 	CompBytePntArray,
     CompDoubleArray,	CompFloatArray,
     CompCharPntArray, 	CompBytePntArray
};

void _stdcall IndexSortElems (void *src, long elemsize, long numof,
      long *dst, long comparetype, long decrease)
{
	AddrPitchDecr ap;

	if ((0xdff >> comparetype) & 1)
   	{
	   	ap.addr = (char *)src;
 		ap.pitch = elemsize;
	   	ap.decrease = decrease;
        ap.comptype = comparetype;
		IndexSort (dst, numof, compfuncs[comparetype], &ap);
   	}
}



/*********************************************** IndexSortWordList */
/*****
  <offslen> is filled with pairs {offset, length} of words
  from <list> (exactly <wrdcnt> pairs);
  the function fills <ind> with numbers 0 - <wrdcnt>-1 so
  that list{offset[i],lenght[i]} -words are in alphabetical
  order (case insensitive).
  Returns the Number Of Words in the text
*****/
typedef struct
{
	char *addrtxt; // text with words
	long *addrol;  // offset-lenght pairs
} ParseSortInfo;

long _stdcall CompWordArray(long i1, long i2, void *extrainfo)
{
	ParseSortInfo *psi;
   	char *w1, *w2;
   	long offs1, offs2, len1, len2, lenmin, ret;

    psi = (ParseSortInfo *)extrainfo;
    offs1 = psi->addrol[i1 * 2];
    lenmin = len1 = psi->addrol[i1 * 2 + 1];
    w1 = psi->addrtxt + offs1;
    offs2 = psi->addrol[i2 * 2];
    if ((len2 = psi->addrol[i2 * 2 + 1]) < len1) lenmin = len2;
    w2 = psi->addrtxt + offs2;
    if ((ret = _memicmp (w1, w2, lenmin)) == 0) ret = len1 - len2;
    return ret;
}

void _stdcall IndexSortWordList(char *list, long *offslen,
						long *inds, long wrdcnt)
{
	ParseSortInfo psi;

   	psi.addrtxt = list;
  	psi.addrol = (long *)offslen;
	IndexSort (inds, wrdcnt, CompWordArray, &psi);
}



/*************************************************** InversePermut */
/***
	Function:	InversePermut

	Description:

	Statics/Globals:

	Arguments:

	Returns: None

***/
void _stdcall InversePermut (long *src, long numof, long *dst)
{
	long i;

	for (i = 0;  i < numof;  i++) dst[src[i]] = i;
}



/****************************************************** Linear2by2 */
/*****
    Finds X from linear system
        {a11 a12}   {x1}   {b1}
        |       | * |  | = |  |
        {a21 a22}   {x2}   {b2}

    det = a11*a22 - a12*a21;
    if (abs(det) >= eps)
        x1 = ( a22*b1 - a12*b2) / det;
        x2 = (-a21*b1 + a11*b2) / det;
    else det = 0.0;
    Returns determinant
*****/
double _stdcall Linear2by2(double *A, double *B,
                        double *X, double eps)
{
    double det;

    det = A[0] * A[3] - A[2] * A[1];
    if (fabs(det) >= eps)
    {
        X[0] = (A[3] * B[0] - A[1] * B[1]) / det;
        X[1] = (A[0] * B[1] - A[2] * B[0]) / det;
    } else det = 0.0;
    return det;
}



/************************************************** LineEndsInRect */
/***
	returns:
    	number of intersections with the rectangle's border (0 to 2)
***/
long _stdcall LineEndsInRect (
				long xmin, long ymin, long xmax, long ymax,
                long *px1, long *py1, long *px2, long *py2)
{
    long x1, y1, x2, y2, dx, dy,
    	 yxmin, yxmax, xymin, xymax,
         flags = 0, cnt = 0, ret = 2;
    double ddx, ddy, dwx, dwy;

    x1 = *px1;  y1 = *py1;   x2 = *px2;  y2 = *py2;
    dx = x2 - x1;  dy = y2 - y1;
    if (dx * dy == 0)
    {
        if ((dy) && (x1 >= xmin) && (x1 <= xmax))
        {
        	y1 = ymin;  y2 = ymax;  goto Ready;  // vertical
        }
    	if ((dx) && (y1 >= ymin) && (y1 <= ymax))
        {
        	x1 = xmin;  x2 = xmax;  goto Ready;  // horizontal
        }
        ret = 0;  goto Fin;	// no intersections or same point
    }
    // here both dx,dy != 0
    ddx = (double)dx;  ddy = (double)dy;
    dwy = (ddy / ddx) * (xmax - x1) + y1;
    yxmax = floor(dwy + 0.5);
    dwy = (ddy / ddx) * (xmin - x1) + y1;
    yxmin = floor(dwy + 0.5);
    dwx = (ddx / ddy) * (ymax - y1) + x1;
    xymax = floor(dwx + 0.5);
    dwx = (ddx / ddy) * (ymin - y1) + x1;
    xymin = floor(dwx + 0.5);
    if ((yxmin >= ymin) && (yxmin <= ymax)) { flags |= 1;  cnt++; }
    if ((yxmax >= ymin) && (yxmax <= ymax)) { flags |= 2;  cnt++; }
    if ((xymin >= xmin) && (xymin <= xmax)) { flags |= 4;  cnt++; }
    if ((xymax >= xmin) && (xymax <= xmax)) { flags |= 8;  cnt++; }
    if (cnt < 2) { ret = 0;  goto Fin; }	// 0, 1, 2, 4, 8

    if ((flags & 0x03) == 0x03)
    {	// 3, 7, b, f : left and right
     	x1 = xmin;  y1 = yxmin;  x2 = xmax;  y2 = yxmax;
        goto Ready;
    }
    if ((flags & 0x0c) == 0x0c)
    {	// c, d, e, (f) : top and bottom
     	x1 = xymin;  y1 = ymin;  x2 = xymax;  y2 = ymax;
        goto Ready;
    }
	// still to do: 5, 6, 9, a
    if (flags == 0x05)
    {	// left and bottom
     	x1 = xmin;  y1 = yxmin;  x2 = xymin;  y2 = ymin;
        goto Check1;
    }
    if (flags == 0x06)
    {	// right and bottom
     	x2 = xmax;  y2 = yxmax;  x1 = xymin;  y1 = ymin;
        goto Check1;
    }
    if (flags == 0x09)
    {	// left and top
     	x1 = xmin;  y1 = yxmin;  x2 = xymax;  y2 = ymax;
        goto Check1;
    }
    if (flags == 0x0a)
    {	// right and top
     	x2 = xmax;  y2 = yxmax;  x1 = xymax;  y1 = ymax;
//        goto Check1;
    }
Check1:
	if ((x1 == x2) && (y1 == y2)) ret = 1;
Ready:
    *px1 = x1;  *py1 = y1;  *px2 = x2;  *py2 = y2;
Fin:
	return ret;
}



/************************************************************ LinRegr */
/*
 	linear regression routine finds the line, Y=m*X+b, that is
 	the best in sense of least square for a given set of (x,y) pairs.

 	<x>, <y> - pointers to the (pitched) arrays of X and Y coordinates
 	<pitchx>, <pitchy> - pithes IN BYTES (!)
 	<n> 	  - number of (double) elements in <x> and <y>
 	<m>, <b> - pointers to calculated <m> and <b> in Y=m*X+b
*/
void _stdcall LinRegr (void *x, void *y,
				long pitchx, long pitchy, long n,
                double *m, double *b)
{
   	double xx, yy, xy, xsum, ysum, xval, yval,
   		num, ymean, xmean;
   	long i;
    BYTE *bx, *by;

    bx = (BYTE *)x;  by = (BYTE *)y;
   	xx = yy = xy = xsum = ysum = 0.0;
   	for (i = 0;  i < n;  i++)
   	{
      	xval = *(double *)bx;  	yval = *(double *)by;
      	xx += xval * xval;   	yy += yval * yval;
      	xy += xval * yval;
      	xsum += xval;        	ysum += yval;
        bx += pitchx;   		by += pitchy;
   	}
   	xmean = xsum / n;       ymean = ysum / n;
   	num = xy - xsum * ymean;
   	m[0] = num / (xx - xsum * xmean);  	// 'm' in Y=m*X+b
   	b[0] = ymean - m[0] * xmean;       	// 'b' in Y=m*X+b
}



/*************************************************** LookUpTable */
/***
	Function:	LookUpTable

	Description:
		This function deals with static LastLUT (LastBMPinfo)
		structure which is an internal buffer for current LUT.
		See "command" parameter description for list of possible actions
		Examples:
			LookUpTable (LUTbuffer, "Set");
			LookUpTable (LUTbuffer, "Get");
			LookUpTable (LUTbuffer, "Info");
			LookUpTable (NULL, "Linear");
			LookUpTable (NULL, "Extreme");
			LookUpTable (NULL, "2-Extreme");
			LookUpTable (NULL, "Two colors");
			LookUpTable (NULL, "Black/White");
			LookUpTable (NULL, "White/Black");
			LookUpTable (NULL, "Applied Imaging");

	Statics/Globals:
		LastBMPinfo

	Input Arguments:
		buffile - pointer to memory buffer;
			its size must be not less 1024 bytes

		command - pointer to ASCII text; its 1-st byte defines
			the action:
			's' or 'S' - set LastLUT to the content of <buffile>.
			'g' or 'G' - copy the content of LastLUT into <buffile>.
			'l' or 'L' - set LastLUT to linear grey scale
			'e' or 'E' - set LastLUT to linear grey scale with
						2 special extreme values: 0 is blue, 255 is red
			'2'        - set LastLUT to linear grey scale with
						4 special extreme values: 0,1 are blue, 254,255 are red
			't' or 'T' - set LastLUT to two-colors combination (4*4 bits)
			'b' or 'B' - set LastLUT to Black(0)/White(1) colors
			'w' or 'W' - set LastLUT to White(0)/Black(1) colors;
			'm' or 'M' - set LastLUT to 2 colors given in LUTbuffer
			'i' or 'I' - copy (DWORD)LastLUT, LastLUTbytes and
         			LastLUTform into (DWORD *)buffile.
			'a' or 'A' - set LastLUT to linear/colors (Applied Imaging LUT)

			Commands 'L', 'E', 'T' ignore the parameter buffile.

	Output Arguments:
		None

	Returns:
		0 if OK, -1 otherwise
***/
long _stdcall LookUpTable (char *buffile, char *command)
{
	WORD action, i, r, g, b;
	long ret = 0;
   	DWORD clr, *lbuff;
   	DWORD c2[2] = {0x000000, 0xFFFFFF};

	action = (BYTE)(*command | 0x20);  /** to lower case **/
	switch (action)
	{
	  case 'g':
		memmove (buffile, LastLUT, (short)LastLUTbytes);
		break;

	  case 'i':
      	lbuff = (DWORD *)buffile;
		lbuff[0] = (DWORD)LastLUT;
      	lbuff[1] = LastLUTbytes;
      	lbuff[2] = LastLUTform;
		break;

	  case 's':
		LastLUTform = 256;   LastLUTbytes = LastLUTform * sizeof(RGBQUAD);
		memmove (LastLUT, buffile, (short)LastLUTbytes);
		break;

	  case 'e':
	  case '2':
	  case 'l':
		LastLUTform = 256;   LastLUTbytes = LastLUTform * sizeof(RGBQUAD);
		for (i = 0;  i < 256;  i++)
		{
			LastBMPinfo.diLUT[i].rgbRed   = i;
			LastBMPinfo.diLUT[i].rgbGreen = i;
			LastBMPinfo.diLUT[i].rgbBlue  = i;
		}
		if ((action == 'e') || (action == '2'))
		{ // set 0 to blue, 255 to red:
			LastBMPinfo.diLUT[0].rgbBlue  = 255;

			LastBMPinfo.diLUT[255].rgbGreen = 0;
			LastBMPinfo.diLUT[255].rgbBlue  = 0;
		}
		if (action == '2')
		{ // set 1 to weak blue, 254 to weak red:
			LastBMPinfo.diLUT[1].rgbRed   = 150;
			LastBMPinfo.diLUT[1].rgbGreen = 150;
			LastBMPinfo.diLUT[1].rgbBlue  = 255;

			LastBMPinfo.diLUT[254].rgbRed   = 255;
			LastBMPinfo.diLUT[254].rgbGreen = 150;
			LastBMPinfo.diLUT[254].rgbBlue  = 150;
		}
		break;
	  case 'a':
		LastLUTform = 256;   LastLUTbytes = LastLUTform * sizeof(RGBQUAD);
		for (i = 0;  i < 240;  i++)
		{
            clr = (i * 255) / 239;
			LastBMPinfo.diLUT[i].rgbRed   = clr;
			LastBMPinfo.diLUT[i].rgbGreen = clr;
			LastBMPinfo.diLUT[i].rgbBlue  = clr;
		}
		for (i = 240;  i < 249;  i++)
		{
            if (i == 248) clr = 255; else clr = (i - 240) * 32;
			LastBMPinfo.diLUT[i].rgbRed   = clr;
			LastBMPinfo.diLUT[i].rgbGreen = clr;
			LastBMPinfo.diLUT[i].rgbBlue  = clr;
		}
        clr = 0x561234;
		for (i = 249;  i < 255;  i++)
		{
			LastBMPinfo.diLUT[i].rgbRed   = (clr & 1) * 255;  clr >>= 1;
			LastBMPinfo.diLUT[i].rgbGreen = (clr & 1) * 255;  clr >>= 1;
			LastBMPinfo.diLUT[i].rgbBlue  = (clr & 1) * 255;  clr >>= 1;
            clr >>= 1;
		}
        LastBMPinfo.diLUT[255].rgbRed   = 0xFF;
        LastBMPinfo.diLUT[255].rgbGreen = 0xE6;
        LastBMPinfo.diLUT[255].rgbBlue  = 0xC8;
		break;
	  case 'w':
      	c2[1] = 0x000000;  c2[0] = 0xFFFFFF;
	  case 'b':
        buffile = NULL;
	  case 'm':
        if (!buffile) buffile = (char *)c2;
		LastLUTform = 2;   LastLUTbytes = LastLUTform * sizeof(RGBQUAD);
		memmove (LastLUT, buffile, (short)LastLUTbytes);
		break;

	  case 't':
		LastLUTform = 256;   LastLUTbytes = LastLUTform * sizeof(RGBQUAD);
		for (i = 0;  i < 256;  i++)
		{
			r = (BYTE)(i & 0xf0);
            b = (BYTE)((i << 4) & 0xf0);
            g = (BYTE)((r + b) / 2);
			LastBMPinfo.diLUT[i].rgbRed   = r;
			LastBMPinfo.diLUT[i].rgbGreen = g;
			LastBMPinfo.diLUT[i].rgbBlue  = b;
		}
		break;

	  default: ret = -1; 	break;
	}
	return ret;
}



/***************************************************** MatrInverse */
/************
 	Inverse a square matrix by Gaussian elimination

 	Matrix Inv[n,n] calculates as the inverse of Src[n,n]
    Matrixes MUST BE different.
 	<pdet> is an address for (result) determinant value (NULL is OK).
    <eps> is minimum allowable value for determinant.
 	Returns rang of matrix A, or -1 if bad.
************/
long _stdcall MatrInverse (double *Src, double *Inv, long n,
			double eps, double *pdet)
{
   	long i, k, l, m, rang;
   	double det, maxval, val,
    		*icol, *col, *w1, *w2, *w3, *w4;

    memset (Inv, 0, n * n * sizeof(double)); 	// Inv = 0
   	for (i = 0;  i < n * n;  i += (n + 1)) Inv[i] = 1.0;	// Diagonalization
    det = 1.0;   rang = n;
   	for (i = 0;  i < n;  i++)
   	{
      	col = Src + (i * n);   icol = Inv + (i * n);
      	// Find a max element in the column:
      	w1 = col + i;   maxval = fabs(*w1);   k = m = i;
      	while (k < n)
      	{
	 		if (fabs(*w1) > fabs(maxval)) { maxval = *w1;  m = k; }
	 		w1 += n;   k++;
      	}
      	if (m > i)
      	{	// exchange rows
	 		m -= i;
            w1 = col;    w2 = w1 + (m * n);
            w3 = icol;   w4 = w3 + (m * n);
	 		for (k = i;  k < n;  k++)
	 		{
	    		val = w1[k];   w1[k] = w2[k];   w2[k] = val;
	    		val = w3[k];   w3[k] = w4[k];   w4[k] = val;
	 		}
      	}
      	det *= maxval;
      	if (fabs(maxval) < eps) { rang--;  continue; }
      	w1 = col;  w2 = icol;
      	for (k = 0;  k < n;  k++)
      	{	// normalization
        	w1[k] /= maxval;   w2[k] /= maxval;
        }
      	for (l = 0;  l < n;  l++)
      	{
	 		if (l == i) continue;
	 		w1 = col;   w2 = Src + (l * n);  val = w2[i];
	 		w3 = icol;  w4 = Inv + (l * n);
	 		for (k = i;  k < n;  k++)
	 		{
	    		w2[k] -= (val * w1[k]);
	    		w4[k] -= (val * w3[k]);
	   		}
      	}
   	}
   	if (pdet) *pdet = det;
    return rang;
}


/******************************************************** ScalProd */
/*********
 	scalar product of 2 vectors

 	<x>, <y> - pointers to the (pitched) vector arrays
 	<pitchx>, <pitchy> - pithes IN BYTES (!)
 	<n> 	  - number of (double) elements in <x> and <y>

 	returns: the scalar product as (double)
*********/
double _stdcall ScalProd (void *x, void *y,
				long pitchx, long pitchy, long n)
{
   	double r = 0.0;
    BYTE *bx, *by;

    bx = (BYTE *)x;  by = (BYTE *)y;
   	while (n--)
   	{
   		r += (*(double *)bx) * (*(double *)by);
        bx += pitchx;   by += pitchy;
    }
    return r;
}


/*********************************************************** MatrMult */
/********
 	Multiplication of 2 matrixes
 	M1[cols1rows2 * nrows1], M2[ncols2 * cols1rows2]
 	Mprod[ncols1 * nrows2] is the result matrix product
    Mprod must be different of M1, M2.
********/
void _stdcall MatrMult (double *M1, double *M2, double *Mprod,
				long cols1rows2, long nrows1, long ncols2)
{
   	long ic, ir, pitchd, pitch1, pitch2;
   	double *row1, *col2, *res;

    pitchd = sizeof(double);
    pitch1 = cols1rows2 * pitchd;
    pitch2 = ncols2 * pitchd;
   	row1 = M1;   res = Mprod;
   	for (ir = 0;  ir < nrows1;  ir++)
   	{
      	col2 = M2;
      	for (ic = 0;  ic < ncols2;  ic++)
      	{
	 		*res++ = ScalProd (row1, col2, pitchd, pitch2, cols1rows2);
	 		col2++;
      	}
      	row1 += pitch1;
   	}
}



/*************************************************** MatrTranspose */
/*
 	Calculates transposed matrix

 	M[cols,rows] - the source matrix
 	T[rows,cols] - the result matrix: T = tr(M)
    Matrixes MUST BE different.
*/
void _stdcall MatrTranspose (double *M, double *T, long cols, long rows)
{
   	long rc, i, k, n;

   	rc = rows * cols;   n = 0;
   	for (i = 0;  i < cols;  i++)
   	{
      	for (k = i;  k < rc;  k += cols)
        	T[n++] = M[k];
   	}
}


/*********************************************************** Scalp */
/********
 	scalar product of 2 continuous vectors

 	<x>, <y> - pointers to (double) arrays
 	<n> 	  - number of (double) elements in <x> and <y>
 	returns: the scalar product as (double)
********/
double _stdcall Scalp (double *x, double *y, long n)
{
   	double r = 0.0;
    long i;

   	for (i = 0;  i < n;  i++) r += x[i] * y[i];
    return r;
}


/**************************************************** MatrMmultTrM */
/*
 	Multiply matrix by itself transposed:

 	M[cols,rows] - the source matrix
 	N[rows,rows] - the result matrix: N = M * tr(M)
*/
void _stdcall MatrMmultTrM (double *M, double *N, long cols, long rows)
{
   	long i1, i2, pitch;
   	double *row1, *row2, val;

   	row1 = M;   pitch = cols * sizeof(double);
   	for (i1 = 0;  i1 < rows;  i1++)
   	{
      	row2 = row1;
      	for (i2 = i1;  i2 < rows;  i2++)
      	{
			val = Scalp (row1, row2, cols);
	 		N[i1 * rows + i2] = N[i1 + rows * i2] = val;
	 		row2 += pitch;
      	}
      	row1 += pitch;
   	}
}



/**************************************************** MatrTrMmultM */
/*
 	Multiply transposed matrix by itself:

 	M[cols,rows] - the source matrix
 	N[cols,cols] - the result matrix: N = tr(M) * M
*/
void _stdcall MatrTrMmultM (double *M, double *N, long cols, long rows)
{
   	long i1, i2, pitch;
   	double *col1, *col2, val;

   	col1 = M;   pitch = cols * sizeof(double);
   	for (i1 = 0;  i1 < cols;  i1++)
   	{
      	col2 = col1;
      	for (i2 = i1;  i2 < cols;  i2++)
      	{
			val = ScalProd (col1, col2, pitch, pitch, rows);
	 		N[i1 * cols + i2] = N[i1 + cols * i2] = val;
	 		col2++;
      	}
      	col1++;
   	}
}



/******************************************************* MovMemory */
/***
	Function:	MovMemory

	Description:
		This function copyes a number of bytes from one location
		to another; the RTL/API functions to move memory are different
		for 16/32 bits, Win32/32s/95, so there are different
		implementations of MovMemory(), but using the function one
		improves portability of VB programs.

	Statics/Globals:
		None

	Input Arguments:
		from - pointer to source memory locations

		to - pointer target memory locations

		bytelen - number of bytes to move (DWORD)

	Output Arguments:
		None

	Returns:
		None

***/
void _stdcall MovMemory (
            void *from,
            void *to,
            DWORD len)
{
#ifdef  PentiumIIwithMMX
    if (CPU_MMX() & 1)
    {	// MMX is allowed and presents
    	tmMoveByteRect (from, to, len, 1, len, len, 0);
        return;
//void _stdcall tmMoveByteRect (void *p1, void *p2,
//long xe, long ye, long pitch1, long pitch2, long neg);
    }
#endif // PentiumIIwithMMX

	memmove (to, from, len);
// void *memmove(void *dest, const void *src, size_t n);
}


/************************************************ MemFromClpbrd */
/***
	Function:	MemFromClpbrd

	Description:
		This function copyes the content of the system Clipboard
		object of a given clipboard type into a memory buffer

	Statics/Globals:
		None

	Input Arguments:
		FormatName - pointer to (0-terminated) Clipboard Format Name

		mem - pointer to memory buffer where to copy the content
			of the Clipboard object of a given clipboard type

		limit - length of the memory buffer (bytes)

	Output Arguments:
		None

	Returns:
		memory size of the Clipboard object if OK, 0 otherwise.
		Note: the size (and return value) may be more than <limit>;
		in this case buffer's content is truncated.

***/
long  _stdcall MemFromClpbrd (
						char *FormatName,
                  char *mem,
                  DWORD limit)
{
	 long ret = 0;
	 HGLOBAL ghndl;
	 char *gaddr;
	 DWORD gsize;
	 UINT hClipFormat;


	 hClipFormat = RegisterClipboardFormat(FormatName);
	 if (OpenClipboard(GetActiveWindow()))
	 {
		 ghndl = GetClipboardData (hClipFormat);
		 if (ghndl)
		 {
			 if (CloseClipboard())
			 {
				 gsize = GlobalSize (ghndl);
				 if (gsize > limit) gsize = limit;
				 gaddr = (char *)GlobalLock(ghndl);
				 MovMemory (gaddr, mem, gsize);
				 GlobalUnlock(ghndl);
				 ret = (long)gsize;
			 }
		 }
	 }
//Fin:
	 return ret;
}


/************************************************************ FillLDI */
/***	not exported
	Function:	FillLDI

	Description:
		This (not exported) function fills LDI (LastBMPinfo.diInfo)
		structure with given and default data

	Statics/Globals:
		LDI (LastBMPinfo.diInfo)

	Input Arguments:
		bitspix - pixel size in bits (usually must be set to 8)

		width - number of pixels in the row in the BITMAP

		height -  number of rows in the BITMAP


	Output Arguments:
		None

	Returns:
		None
***/
void FillLDI (DWORD bitspix, DWORD width, DWORD height)
{
	LDI.biSize = sizeof(BITMAPINFOHEADER);
	LDI.biWidth = width;
	LDI.biHeight = height;
	LDI.biPlanes = 1;
	LDI.biBitCount = (WORD)bitspix;
	LDI.biCompression = BI_RGB;  /** no compression **/
	LDI.biSizeImage = ((long)bitspix * width * height + 7) / 8;
	LDI.biXPelsPerMeter = 0;  /** 0 is valid **/
	LDI.biYPelsPerMeter = 0;
	LDI.biClrUsed = LastLUTform;
	LDI.biClrImportant = LastLUTform;
}



////////////////////////////////////////////////////////////////////////
//
//      Code for this function looks horrible
//       it is to be tidied up later - dcb
//      Use section DAVES_SEP (Separate bitmap create and copy)
//      For some reason, the CreateDIBitmap() function was failing
//      under windows 2000 (and XP?)
//      Separating the call into CreateCompatibleBitmap() and SetDIBits()
//      but the SetDIBits() was also failing. Separating the SetDIBits()
//      further so it set chunks of 100 lines at a time
//      this also failed when copying lines 300 to 400 (in my tests)
//      64k segment boundary ?? whatever..
//      Now each chunk of 100 lines is copied to a separate memory
//      area which is then used in the call to SetDIBits()
//      This seems to work ! - dcb
//
/******************************************************** MemToBitMap */
/***
	Function:	MemToBitMap

	Description:
		This function creates Windows BITMAP object from
		rectangular piece of memory using current LUT

	Statics/Globals:
		LastLUT

	Input Arguments:
		hwndpar - window handle to get the device context

		pix - number of bits per pixel

		xe - number of pixels in each row of the rectangle

		ye - number of rows (scanlines) in the rectangle

		prect - pointer to (rectangular) memory buffer

	Output Arguments:
		None

	Returns:
		handle of the created BitMap if OK,
		0 otherwise.
***/
DWORD _stdcall MemToBitMap (long hwndpar, long pix,
                long xe, long ye,
                char *prect)
{
        HWND hwnd;
        HDC hdc;
        int sdib_status;

        if (hwndpar)
                hwnd = (HWND)hwndpar;
        else
                hwnd = GetActiveWindow();

       	hdc = GetDC (hwnd);

        // Fill out the BITMAPINFO header
// See below ...
//#define VLADS_ORIGINAL
//#define DAVES_NEW
#define DAVES_SEP

#ifdef VLADS_ORIGINAL
	FillLDI((DWORD)pix, (DWORD)xe, (DWORD)ye);
// Old code
	ret = (DWORD)CreateDIBitmap(
				hdc,
				(BITMAPINFOHEADER FAR*)&LDI,
				CBM_INIT,
				prect,
				(BITMAPINFO FAR*)&LDI,
//                                DIB_PAL_COLORS);
				DIB_RGB_COLORS);
#endif

#ifdef DAVES_NEW
// New code. Surely we should be passing BITMAPINFOHEADER as parameter 2
// and BITMAPINFO as parameter 5 - The above code appears to be passing
// the same thing for both parameters, though the explicit cast prevents
// us from being warned of that fact.
// Though it appears that the BITMAPINFO structure contains both the
// BITMAPINFOHEADER and the LUT data so why CreateDIBitmap requires
// both parameters is really a mystery. Maybe I should ask Bill Gates
// Further, the LUT data should be NULL if the pixel depth
// (biBitCount) is 24

// Here we pass the different structures
// and NULL out the pointer if it's 24 bit
BITMAPINFO bmi;

        memset(&bmi, 0, sizeof(bmi)); // This will set bmiColors to NULL !
	FillBMIH(&bmi.bmiHeader, (DWORD)pix, (DWORD)xe, (DWORD)ye);

        // For < 24bpp we supply the old colour LUT
        if (bmi.bmiHeader.biBitCount < 24)
                *(bmi.bmiColors) = LastBMPinfo.diLUT[0];

	ret = (DWORD)CreateDIBitmap(
				hdc,
				(BITMAPINFOHEADER *)&bmi.bmiHeader,
				CBM_INIT,
				(void *)prect,
				(BITMAPINFO *)&bmi,
				DIB_RGB_COLORS);
#endif
#ifdef DAVES_SEP
// New New code.
// Do CreateCompatibleBitmap() then SetDIBits()

// Use vlad's old LDI stuff - yuck !
//        BITMAPINFO bmi;
        HBITMAP hbmp;
        UINT startline = 0;
        void *sdib_bits;

        // Vlads old LDI stuff
        FillLDI((DWORD)pix, (DWORD)xe, (DWORD)ye);

//        // Clear out the BITMAPINFO, this will set bmiColors to NULL
//        memset(&bmi, 0, sizeof(bmi));

//        // ... and fill out the relevant members
//	FillBMIH(&bmi.bmiHeader, (DWORD)pix, (DWORD)xe, (DWORD)ye);

        // For bpp < 24 we use the old colour LUT
        // .. something like this ...
        // .. except BITMAPINFO defines bmiColors to be an
        // array size [1] - use vlad's old LDI stuff
        //if (bmi.bmiHeader.biBitCount < 24)
        //        bmi.bmiColors = &LastBMPinfo.diLUT[0];

        // Create the bitmap
        hbmp = CreateCompatibleBitmap(hdc, xe, ye);

        // Set up the bitmap bits pointer
        sdib_bits = prect;

// If SetDIBits fails, then maybe it's because the bitmap has
// the wrong parameters. Let's see what they are ...
//BITMAP bitmap;
//GetObject(hbmp, sizeof(BITMAP), (LPVOID) &bitmap);
// ... No bitmap is fine !

//        sdib_status = SetDIBits(hdc, hbmp, startline, ye, sdib_bits, &bmi, DIB_RGB_COLORS);
        sdib_status = SetDIBits(hdc, hbmp, startline, ye, sdib_bits,
                (BITMAPINFO *)&LastBMPinfo, DIB_RGB_COLORS);

        // Test for errors
        if (sdib_status == 0) {
                int st, nl;
                int num_lines;
                BYTE *bitptr, *freemem, *copybits;
                int copysize;
                int widthbytes;

                // OK if the above failed, maybe we can set bits in sections
                // make sure the section size in bytes is less than 64K
                // For some reason I think this may be an issue - dcb
                widthbytes = (xe * pix) / 8;
                num_lines = 0x10000 / widthbytes;
                nl = num_lines;

                // Allocate memory such that we can use a section
                // exactly located on a 64k boundary
                freemem = (BYTE *)malloc((nl * widthbytes) + 0x10000);

                // Assign copybits to 64k boundary within the allocated block
                unsigned long lmem;

                lmem = (long)freemem;
                lmem = (lmem & 0xFFFF0000) + 0x10000;
                copybits = (BYTE *)lmem;

                st = startline;
                while (st < ye) {
                        nl = num_lines;
                        // Deal with remainder chunk
                        if (nl > (ye - st))
                                nl = ye - st;

                        copysize = nl * widthbytes;
                        bitptr = (BYTE *)sdib_bits + (st * widthbytes);
                        memcpy(copybits, bitptr, copysize);

//                        sdib_status = SetDIBits(hdc, hbmp, st, nl,
//                                 copybits, &bmi, DIB_RGB_COLORS);
                        sdib_status = SetDIBits(hdc, hbmp, st, nl,
                                 copybits, (BITMAPINFO *)&LastBMPinfo,
                                 DIB_RGB_COLORS);
                        if (sdib_status != nl) {
                                // We got problems !
                                DeleteObject(hbmp);
                                hbmp = 0;
                                break;
                        }
                        st += nl;
                }
                if (freemem)
                        free(freemem);
        }

	ReleaseDC (hwnd, hdc);

        return (unsigned long)hbmp;
#endif

#ifdef VLADS_ORIGINAL
//Fin:
        // Test for errors
        if (ret == 0) {
                err = GetLastError();
        }

	ReleaseDC (hwnd, hdc);

   return ret;
#endif

#ifdef DAVES_NEW
        // Test for errors
        if (ret == 0) {
                err = GetLastError();
        }

	ReleaseDC (hwnd, hdc);
        return ret;
#endif
}



/************************************************** MemToClpbrd */
/***
	Function:	MemToClpbrd

	Description:
		This function copyes given piece of memory into a
		global Windows buffer and puts it on the system Clipboard
		as an object of a given clipboard type

	Statics/Globals:
		None

	Input Arguments:
		FormatName - pointer to (0-terminated) Clipboard Format Name

		mem - pointer to memory buffer which content is to put
			on the Clipboard

		memlen - length of the buffer

	Output Arguments:
		None

	Returns:  1 if OK, 0 otherwise

***/
long _stdcall MemToClpbrd (
							char *FormatName,
                     char *mem,
                     DWORD memlen)
{
	 long ret = 0;
	 HGLOBAL ghndl;
	 char *gaddr;
	 UINT hClipFormat;

	 ghndl = GlobalAlloc(GMEM_MOVEABLE, memlen); // get global buffer handle
	 if (!ghndl) goto Fin;
	 gaddr = (char *)GlobalLock(ghndl);          // get global buffer address
	 if (!gaddr) goto FinFree;
	 MovMemory (mem, gaddr, memlen);             // copy to global buffer
	 GlobalUnlock(ghndl);
			// (re)register Clipboard Format Name:
	 hClipFormat = RegisterClipboardFormat(FormatName);
	 if (OpenClipboard(GetActiveWindow()))
	 {
		  if (EmptyClipboard())
		  {
			 if (SetClipboardData (hClipFormat, ghndl))
			 {
				 if (CloseClipboard())
					 ret = 1;
			 }
		}
	}
FinFree:
	if (!ret) GlobalFree(ghndl);
	// otherwise it became a clipboard object
Fin:
	return ret;
}



/******************************************************** MemToHex */
/***
	Function:	MemToHex

	Description:
		This function fills a memory buffer with hexadecimal
		presentation of given memory field content

	Arguments:
		mem - pointer to source memory locations
	    len - number of source bytes
		to - pointer to target memory locations;
			size of this buffer must be big enough to fit target
			information;
		wlen - length of the portion in the source buffer
            (to put space between output portions);
            0 - no spaces to insert

    Special case:
        if (!mem) then
            (len!=0) means 'set hex letters {A-F}'
            else  {a-f}
            wlen sets symbol to represent bytes<0x20
	Returns:
		Number of bytes written to <to>

***/
long _stdcall MemToHex (void *mem, long len, void *to, long wlen)
{
    BYTE b, *r, *w;
	long i, n;
    char *format;

    i = n = 0;
    if (!mem)
    {
        MemToHex_Capitals = len;
        MemToHex_Service = (BYTE)wlen;
        goto Fin;
    }
    if (MemToHex_Capitals) format = "%.2X";
    else  format = "%.2x";
    r = (BYTE *)mem;  w = (BYTE *)to;
    while (i < len)
    {
        b = r[i++];
        n += sprintf ((char*)(w + n), format, b);
        if (wlen > 0) if (i % wlen == 0) w[n++] = asciiSPACE;
    }
Fin:
	return n;
}



/***************************************************** MinsAndMaxs */
/*********
	Description:
    Calculates positions of mins and maxs elements in given vector.

    <v> 	 - pointer to the vector array
 	<nv> 	 - number of elements in <v>
    <type> -
      <type> & 0xff:
        0 - char
        1 - byte
        2 - short
        3 - word
        4 - long
        5 - dword

        8 - double
        9 - float
      <type> & 0x100:
        0 - isolated ends
        1 - closed array (first and last elements are neighbors)

    <presult> - pointer to send result

    Updates <presult> with
        list of minima/maximuma indices;
        if the list begins from a maximum then presult[0]=-1
        if the list ends with a maximum then presult[last]=-1

	Returns: number of elements in <presult> array


*********/
long _stdcall MinsAndMaxs (void *v, long nv, long typev,
						long *presult, long reslen)
{
    long i, k, n, dir, lastdir, diff, type, cycle,
    	 behav, leftmin = -1, ret = 0;

    char   *cv;
    BYTE   *bv;
    short  *sv;
    WORD   *wv;
    long   *lv;
    DWORD  *dwv;
   	double *dbv;
   	float  *fbv;

    if (nv < 2) goto Fin;
    cv = (char *)v;   	bv = (BYTE *)v;
    sv = (short *)v;  	wv = (WORD *)v;
    lv = (long *)v;   	dwv = (DWORD *)v;
   	dbv = (double *)v;  fbv = (float *)v;
    type = typev & 0xff;  cycle = typev & 0x100;
    // dir: 1 - down, 0 - ?, 2 - up
   	i = 0;  dir = 0;
    if (cycle)
    {
    	i = nv;
        while((i--) && (dir != 1))
        {
		  switch (type)
		  {
	  		case 0:	// char
            	if (cv[i] < cv[0]) dir = 2;
                if (cv[i] > cv[0]) dir = 1;		break;
		  	case 1:	// byte
            	if (bv[i] < bv[0]) dir = 2;
                if (bv[i] > bv[0]) dir = 1;		break;
		  	case 2:	// short
            	if (sv[i] < sv[0]) dir = 2;
                if (sv[i] > sv[0]) dir = 1;		break;
		  	case 3:	// word
            	if (wv[i] < wv[0]) dir = 2;
                if (wv[i] > wv[0]) dir = 1;		break;
		  	case 4:	// long
            	if (lv[i] < lv[0]) dir = 2;
                if (lv[i] > lv[0]) dir = 1;		break;
		  	case 5:	// dword
            	if (dwv[i] < dwv[0]) dir = 2;
                if (dwv[i] > dwv[0]) dir = 1;	break;
		  	case 8:	// double
            	if (dbv[i] < dbv[0]) dir = 2;
            	if (dbv[i] > dbv[0]) dir = 1;	break;
		  	case 9:	// float
            	if (fbv[i] < fbv[0]) dir = 2;
            	if (fbv[i] > fbv[0]) dir = 1;	break;
		  	default:
                return ret;
          }
          if (dir == 2) leftmin = i;
        }
        if (!dir) return ret;
    }
    lastdir = dir; i = 1;   k = n = 0;
    while ((i <= nv) && (n < reslen))
    {
        if (i == nv)
        {

            if (cycle) diff = lastdir;
          	else diff = (3 - dir) % 3;	// 0,1,2 => 0,2,1
            goto LastCheck;
        }
        diff = 0;
		switch (type)
		{
	  		case 0:	// char
            	if (cv[i - 1] < cv[i]) diff = 2;
            	if (cv[i - 1] > cv[i]) diff = 1;	break;
		  	case 1:	// byte
            	if (bv[i - 1] < bv[i]) diff = 2;
            	if (bv[i - 1] > bv[i]) diff = 1;	break;
		  	case 2:	// short
            	if (sv[i - 1] < sv[i]) diff = 2;
            	if (sv[i - 1] > sv[i]) diff = 1;	break;
		  	case 3:	// word
            	if (wv[i - 1] < wv[i]) diff = 2;
            	if (wv[i - 1] > wv[i]) diff = 1;	break;
		  	case 4:	// long
            	if (lv[i - 1] < lv[i]) diff = 2;
            	if (lv[i - 1] > lv[i]) diff = 1;	break;
		  	case 5:	// dword
            	if (dwv[i - 1] < dwv[i]) diff = 2;
            	if (dwv[i - 1] > dwv[i]) diff = 1;	break;
		  	case 8:	// double
            	if (dbv[i - 1] < dbv[i]) diff = 2;
            	if (dbv[i - 1] > dbv[i]) diff = 1;	break;
		  	case 9:	// float
            	if (fbv[i - 1] < fbv[i]) diff = 2;
            	if (fbv[i - 1] > fbv[i]) diff = 1;	break;
		  	default:
                return ret;
        }
LastCheck:
        behav = (dir << 4) | diff;
		switch (behav)
		{
            // (dir==0) is possible only in the very begining
	  		case 0x00:	// ?, ==
            	k += (i & 1);	// every second
				break;
		  	case 0x01:	// ?, down; begins from maximum
                presult[n++] = -1;
                presult[n++] = k;
                dir = 1;   k = i;
				break;
		  	case 0x02:	// ?, up; begins from minimum
                presult[n++] = k;
                dir = 2;   k = i;
				break;
            // =======================
		  	case 0x10:	// down, ==
            	k += (i & 1);	// every second
				break;
		  	case 0x11:	// down, down
				k = i;
				break;
		  	case 0x12:	// down, up; the minimum
                presult[n++] = k;
                dir = 2;   k = i;
                break;
            // =======================
		  	case 0x20:	// up, ==
            	k += (i & 1);	// every second
				break;
		  	case 0x21:	// up, down; the maximum
            	if ((cycle) && (!n)) presult[n++] = leftmin;
                presult[n++] = k;
                dir = 1;   k = i;
				break;
		  	case 0x22:	// up, up
				k = i;
				break;
        }
        i++;
	}
    if ((n > 1) && ((n & 1) == 0))
    {
    	if (cycle) presult[n++] = presult[0];
     	else presult[n++] = -1;
    }
   	ret = n;
Fin:
    return ret;
}




/************************************************** MoveSubRect  */
/***
	Function:	MoveSubRect

	Description:
		This function moves a "rectangle" of bytes between two locations:
		- <xe*ye> bytes continuous memory field ("small picture"), and
		- "subrectangle" <xe*ye> , left top corner located at {xl,yt}
			inside <pitch*numlines> continuous memory field ("big picture"),
		where
				0 <= xl < xl + xe <= pitch
				0 <= yt < yt + ye <= numlines
		Moving options include both directions and direct/inversed
		rows order.

	Statics/Globals:
		None

	Input Arguments:
		bigpict - pointer to "big picture" memory location

		pitch, numlines - "big picture" dimensions

		xl, yt - coordinates of the left top corner of the subrectangle
				in the "big picture"

		smallpict - pointer to "small picture" memory location

		xe, ye - "small picture" dimensions

		direction -
			0 : from "big picture" to "small picture", from top to bottom
			1 : from "big picture" to "small picture", from bottom to top
			2 : from "small picture" to "big picture", from top to bottom
			3 : from "small picture" to "big picture", from bottom to top

	Output Arguments:
		None

	Returns:
		a number of moved lines

***/
DWORD _stdcall MoveSubRect (
							char *bigrect,
                     DWORD pitch, DWORD numlines,
                     DWORD xl, DWORD yt,
							char *smallrect,
                     DWORD xe, DWORD ye,
                     long direction)
{
	DWORD frombottom, fromsmalltobig, xeff, i = 0;

	if (xl >= pitch) goto Fin;
	if (xl + xe > pitch) xeff = pitch - xl;
   else xeff = xe;
	frombottom = direction & 1;
	fromsmalltobig = direction & 2;
	bigrect += (pitch * yt + xl);
	if (frombottom) smallrect += (xe * (ye - 1));

	while ((i < ye) && (yt < numlines))
	{
		 if (fromsmalltobig)
			 memmove (bigrect, smallrect, xeff);
		 else
		 	 memmove (smallrect, bigrect, xeff);

		 bigrect += pitch;
		 if (frombottom)  smallrect -= xe;
       else 				smallrect += xe;
		 i++;   yt++;
	}
Fin:
	return i;
}


/************************************************ PermutateInPlace */
/***
	Function:	PermutateInPlace

	Description:
		for i=0 to numof-1: mem[i] = mem[permut[i]]
      where size(mem[i]) == mempitch
	Statics/Globals:

	Arguments:

	Returns: None

***/
void _stdcall PermutateInPlace (void *mem, long mempitch,
					long *permut, long numof)
{
	long i, len;
    char *from, *to, *wrkmem = NULL;

    if (numof < 1) goto Fin;
    len = numof * mempitch;
    wrkmem = (char *)malloc(len);  if (!wrkmem) goto Fin;
	memmove (wrkmem, mem, len);
    to = (char *)mem;
	for (i = 0;  i < numof;  i++)
    {
        from = wrkmem + mempitch * permut[i];
        memmove (to, from, mempitch);
        to += mempitch;
    }
Fin:
    if (wrkmem) free(wrkmem);
}


/****************************************************** ToBegWord */
/***
	Function:	ToBegWord

	Description:
		This function searches for the beginning of the next word,
		i.e. first more-than-space symbol in a given part of text:
			from  begtext+offs  to  begtext+limit.

	Statics/Globals:
		None

	Input Arguments:
		begtext - pointer to the beginning of a piece of memory

		offs - address <begtext+offs> is the beginning point
			to search first more-than-space symbol

		limit - address <begtext+limit> is the first one outside
			of the searched area

	Output Arguments:
		None

	Returns: offset from <begtext> to the beginning of the next word,
		or  limit  if no.

***/
DWORD _stdcall ToBegWord (
					char *pnt,
               DWORD offs,
               DWORD limit)
{
   if (offs > limit) offs = limit;
   while (offs < limit)
   {
       if (pnt[offs] > asciiSPACE) break;
       offs++;
	}
   return offs;
}


/****************************************************** WordLength */
/***
	Function:	WordLength

	Description:
		This function calculates the length of the word,
		i.e. a number of more-than-space symbols in a given
		part of text:
			from  begtext+offs  to  begtext+limit.

	Statics/Globals:
		None

	Input Arguments:
		begtext - pointer to the beginning of a piece of memory

		offs - address <begtext+offs> is the beginning point
			to search first not-more-than-space symbol

		limit - address <begtext+limit> is the first one
			outside of the searched area

	Output Arguments:
		None

	Returns: length of the word

***/
DWORD _stdcall WordLength (
					char *pnt,
               DWORD offs,
               DWORD limit)
{
   DWORD wl;

   wl = offs;
   while ((wl < limit) && (pnt[wl] > asciiSPACE))
       wl++;
   return (wl - offs);
}


/****************************************************** GetTheWord */
/***
	Function:	GetTheWord

	Description:
		This function combines the functionality of 2 functions -
		ToBegWord() and WordLength() - it searches for the beginning
		of the next word, i.e. first more-than-space symbol
		in a given part of text:
			from  begtext+offs  to  begtext+limit
		then updates the <offs> value and returns the length of
		the word, i.e. a number of more-than-space symbols

	Statics/Globals:
		None

	Input Arguments:
		begtext - pointer to the beginning of a piece of memory

		offs - pointer to (DWORD)variable which value is an offset
			from <begtext> to the beginning point of searching for
			first not-more-than-space symbol

		limit - byte at address <begtext+limit> is the first
			outside of the searched area

	Output Arguments:
		offs - after return is updated to the offset of the beginning
			of the word

	Returns: length of the word

***/
DWORD _stdcall GetTheWord (
					char  *ptext,
               DWORD *offs,
               DWORD  limit)
{
	*offs = ToBegWord (ptext, *offs, limit);
	 return WordLength (ptext, *offs, limit);
}



/******************************************* ParseWordList */
/*****
  Fills <offslen> with pairs {offset, length} of words
  from <list> (not more then <limit> pairs).
  (list==NULL) is OK if limit==0 (to count words in the list).
  Returns the Number Of Words in the text
  (may be more than <limit>.
*****/
DWORD _stdcall ParseWordList(char *list, DWORD listsize,
				 DWORD *offslen, DWORD limit)
{
	DWORD wl, woffs = 0, wcnt = 0;

   if (! listsize) listsize = strlen(list);
	while ((wl = GetTheWord (list, &woffs, listsize)) > 0)
   {
      if (wcnt < limit)
      {
        	offslen[2 * wcnt] = woffs;
        	offslen[2 * wcnt + 1] = wl;
      }
     	woffs += wl;
     	wcnt++;	// current number
   }
	return wcnt;
}



/*********************************************** ParseAndSortWlist */
/*****
  Fills <offslen> with pairs {offset, length} of words
  from <list> (not more then <limit> pairs) in alphabetical
  order, case insensitive.
  Returns the Number Of Words in the text
  (may be more than <limit>; if so - does not sort).
*****/
long _stdcall ParseAndSortWlist(char *list, long listsize,
				 long *offslen, long limit)
{
    long wcnt, *inds;

	wcnt = ParseWordList(list, listsize, (DWORD *)offslen, limit);
    if (wcnt <= 0) goto Fin;
    inds = (long *)malloc((wcnt + 1) * sizeof(long));
    if (!inds) goto Fin;
	IndexSortWordList(list, offslen, inds, wcnt);
    PermutateInPlace (offslen, 2 * sizeof(long), inds, wcnt);
    free(inds);
Fin:
	return wcnt;
}


/***************************************************** PolynomMult */
/*********
 	Calculates

    	R(n + m - 1) = X(n) (*p) Y(m);

 	<R>, <X>, <Y> - pointers to vector arrays
 	<n>, <m> 	  - numbers of (double) elements in <X> and <Y>

    Both X, Y must be different from R.

*********/
void _stdcall PolynomMult (double *R, double *X, double *Y,
                long n, long m)
{
	long i, k, nm;
    double cy, *pr;

    nm = n + m - 1;  pr = R;
   	for (i = 0;  i < nm;  i++) pr[i]  = 0;
   	for (k = 0;  k < m;  k++)
    {
        cy = Y[k];
	   	for (i = 0;  i < n;  i++) pr[i] += (cy * X[i]);
        pr++;
    }
}


/************************************************* PolynomMultLong */
/*********
 	Calculates
      if (add)
    	R(n + m - 1) += X(n) (*p) Y(m);
      else
    	R(n + m - 1)  = X(n) (*p) Y(m);

 	<R>, <X>, <Y> - pointers to vector arrays of longs(!)
 	<n>, <m> 	  - numbers of (long) elements in <X> and <Y>

    Both X, Y must be different from R.

*********/
void _stdcall PolynomMultLong (long *R, long *X, long *Y,
                long n, long m, long add)
{
	long i, k, nm, cy, *pr;

    nm = n + m - 1;  pr = R;
    if (!add)
	{
    	for (i = 0;  i < nm;  i++) pr[i]  = 0;
    }
   	for (k = 0;  k < m;  k++)
    {
        if ((cy = Y[k]) != 0)
		   	for (i = 0;  i < n;  i++)
            	pr[i] += (cy * X[i]);
        pr++;
    }
}


/***************************************************** PolynomPlus */
/*********
 	Calculates

    	R(n max m) = Ax * Xn + Ay * Ym;

 	<R>, <X>, <Y> - pointers to vector arrays
 	<n>, <m> 	  - numbers of (double) elements in <X> and <Y>
    <Ax>, <Ay> 	  - see above

*********/
void _stdcall PolynomPlus (double *R, double *X, double *Y,
                long nx, long ny,
				double Ax, double Ay)
{
	long i;

	if (ny > nx)
    {
	   	for (i = 0;  i < ny;  i++)
        {
   			if (i < nx) R[i] = Ay * Y[i] + Ax * X[i];
        	else 		R[i] = Ay * Y[i];
        }
    } else
    {
   		for (i = 0;  i < nx;  i++)
        {
	   		if (i < ny) R[i] = Ax * X[i] + Ay * Y[i];
        	else 		R[i] = Ax * X[i];
        }
    }
}



/********************************************************* LinSyst */
/*
 	Solving of a linear equations system
    	A * X = B

 	A[n,n] 	- the matrix
 	X[n], B[n] - (double) vector arrays
 	n 		- number of elements in both X and B

 	Returns : rang of A, or -1 if bad.
*/
long _stdcall LinSyst (double *A, double *B, double *X, long n)
{
   	long rang = -1;
   	double *inv, det, eps = 1e-8;

   	inv = (double *)malloc (n * n * sizeof(double));
    if (inv)
    {
		rang = MatrInverse (A, inv, n, eps, &det);
   		if (rang == n) MatrMult (inv, B, X, n, n, 1);
   		free (inv);
	}
   	return rang;
}


/******************************************************* PolyRegr  */
/*
 	PolyReg uses the least-square method to determine the
 	coefficients of a polynomial equation that best fits
 	the X-Y data points

 	<X>, <X> 	- pointers to the (pitched) arrays of X and Y coordinates
 	<pitchx>, <pitchy> - pithes IN BYTES (!)
 	<n> 		- number of elements in both X and Y
 	<terms> 	- number of coefficients (1 more polynomial degree)
 	<coefs> 	- the returned result

 	Returns : rang of normal matrix if OK, or -1 if bad.
*/
long _stdcall PolyRegr (double *X, double *Y, long n,
					long pitchx, long pitchy,
					long terms, double *coefs)
{
   	long size, i, k, ret = -1;
   	BYTE *px, *py;
   	double *mem, *ydbl, *norm, *ynorm, *w1, *w2;


   	size = ((n * terms) + (n) + (terms) + (terms * terms))
    		 * sizeof(double);
   	mem = (double *)malloc (size);   if (!mem) goto Fin;
   	ydbl = mem + (n * terms);
   	ynorm = ydbl + n;
    norm = ynorm + terms;
   	w1 = w2 = mem;   py = (BYTE *)Y;
   	for (k = 0;  k < n;  k++)
    {
        ydbl[k] = (*(double *)py);   py += pitchy;
    	*w2++ = 1.0;
    }
   	for (i = 1;  i < terms;  i++)
   	{
      	px = (BYTE *)X;
      	for (k = 0;  k < n;  k++)
        {
        	*w2++ = (*w1++) * (*(double *)px);
            px += pitchx;
        }
   	}
   	MatrMult (mem, ydbl, ynorm, n, terms, 1);
    MatrMmultTrM (mem, norm, n, terms);
   	ret = LinSyst (norm, ynorm, coefs, terms);
   	free (mem);
Fin:
   	return ret;
}



/***************************************************** ReorderBufs */
/***
	Function:	ReorderBufs

	Description:
		This function reverses the orderof 2 buffers:
            <len1> bytes from <mem>  and
            <len2> bytes from <mem>+<len1>

	Statics/Globals:
		None

	Input Arguments:
		mem - pointer to source memory locations
		len1, len2 - number of bytes in buffers

	Returns:
		None

***/
void _stdcall ReorderBufs (void *mem, DWORD len1, DWORD len2)
{
    BYTE *m, *wrk;

    m = (BYTE *)mem;
    wrk = (BYTE *)malloc(len1);
	memmove (wrk, m, len1);           // save 1st buffer
	memmove (m, m + len1, len2);    // move 2nd to the place
	memmove (m + len2, wrk, len1);    // attach saved
    free(wrk);
}



/**************************************************** ReverseRows */
/***
	Function:	ReverseRows

	Description:
		This function moves given continuous rectangle of bytes
		in memory into a different location with reversing of
		row's order.
		If dest = NULL or dest = src then the function reverses
		the row's order in the place.

	Statics/Globals:
		None

	Input Arguments:
		psrc - pointer to source rectangle in memory

		pdest - pointer to destination rectangle in memory;
			destination may be same as source (in this case NULL
			is OK), or it must not overlap the source;
			the function does not verify overlapping.

		xe -  number of bytes in each row of the rectangle

		ye -  number of rows in the rectangle


	Output Arguments:
		None

	Returns:
		None
***/
void _stdcall ReverseRows (
								char *src,
                        char *dest,
								DWORD xe,
                        DWORD ye)
{
	char *rd, *rs, *s;
	DWORD n;
	BYTE pix;

	rs = src + (ye * xe);
   if (! dest) dest = src;
	if (dest != src)
	{
		rd = dest;
		while (ye--)
		{
			rs -= xe;
			MovMemory (rs, rd, xe);
			rd += xe;
		}
	} else
	{  // in place
		rd = src;  ye /= 2;
		while (ye--)
		{
			rs -= xe;  s = rs;  n = xe;
			while (n--)
			{
				pix = *rd;  *rd++ = *s;  *s++ = pix;
			}
		}
	}
}



/******************************************************* RGB8toHSI */
/***
    Calculates Hue, Saturation, Intensity from
    Red, Green, Blue color component values.
    Every H, S, U are normalized
    to the range {0-255}.
	H = arctan{ ((G-B)/sqr(3)) / (R-I) } =
		arctan{ ((G-B)*sqr(3)) / (3*R-(R+G+B)) }
    Hue values are about:
    0 - Cyan, 42 = Blue, 85 - Magenta, 127 - Red,
    170 - Yellow, 212 - Green, 255 - Cyan again
	Returns <color pix>
***/
DWORD _stdcall RGB8toHSI(long rv, long gv, long bv,
							long *Hp, long *Sp, long *Ip)
{
	long wrk, cc, cc2, cmin, hv, sv, iv, ax, ay;
    double atn;

    cc = rv + gv + bv;  cc2 = cc / 2;
    if (rv < gv) cmin = rv; else cmin = gv;
    if (cmin > bv) cmin = bv;
 	// saturation:
    if ((wrk = (cc - cmin * 3)) < 1) sv = 0;
    else sv = (wrk * 255 + cc2) / cc;
    // intensity:
    iv = (cc + 2) / 3;
    // hue:
    ay = gv - bv;  ax = 3 * rv - cc;
    if (ax | ay)
    {
    	atn = atan2(ygSqr3 * ay, (double)ax) / ygPi; // -1 to +1
        hv = (long)floor((atn + 1.0) * 128.0 + 0.5);  hv &= 0xff;
    } else hv = 0;
    *Hp = hv;  *Sp = sv;  *Ip = iv;
    return (((rv << 8) | gv) << 8) | bv;
}


/******************************************************* HSItoRGB8 */
/***
	Inversion for RGB8toHSI.
    Calculates Red, Green, Blue color component values
    from Hue, Saturation, Intensity (normalized to the range {0-255}).
	H = arctan{ ((G-B)/sqr(3)) / (R-I) } =
		arctan{ ((G-B)*sqr(3)) / (3*R-(R+G+B)) }
    Hue values are about:
    0 - Cyan, 42 = Blue, 85 - Magenta, 127 - Red,
    170 - Yellow, 212 - Green, 255 - Cyan again
    If ANGLE corresponds to Hue, then
    Norm = (2/3)*Saturation / ( Abs(cos(ANGLE)) + sqr(3)*Abs(sin(ANGLE)) );
    Cmax = I + Norm * Abs(cos(ANGLE));
    Cmin = I * (1 - Saturation);
    Cmid = 3*I - Cmin - Cmax;
	Returns <color pix>
***/
DWORD _stdcall HSItoRGB8(long hv, long sv, long iv,
							long *Rp, long *Gp, long *Bp)
{
	long ret, direction = 0, maxmidmin[3],
    	 tofix[6] = {43, 43, 128, 128, 213, 213},
    	 indices[6] = {0x210, 0x120, 0x021, 0x012, 0x102, 0x201};
         			//   bgr,   brg,   rbg,   rgb,   grb,   gbr
    double hvfixed, angle, abscos, abssin, norm, dmax, dmid, dmin, dwrk;

    maxmidmin[0] = maxmidmin[1] = maxmidmin[2] = iv;
    if (sv < 1) goto Fin; // all color components are equal:
    // some components are more equal than other;
    direction = (double)hv / 42.500001;  // 0 to 5
    dmin = 1.0 - sv / 255.0;  // min of r, g, b
    // first calculate for the triangle with height=1
    hvfixed = (double)(hv - tofix[direction]);  // -43 to 43
    angle = hvfixed * ygPi / 128.0; // -pi/3 to pi/3
    abscos = cos(angle);  abssin = fabs(sin(angle));
    norm = (2.0 / 255) * sv / (abscos + ygSqr3 * abssin); // 0 to 2
    dmax = (1.0 + norm * abscos);  // max of r, g, b; 0 to 3
    dmid = 3.0 - dmin - dmax;  // mid of r, g, b
    if ((dwrk = dmax * iv) > 255) iv = 255.0 * iv / dwrk;
    maxmidmin[0] = iv * dmax + 0.5;
    maxmidmin[1] = iv * dmid + 0.5;
    maxmidmin[2] = iv * dmin + 0.5;
Fin:
    ret = (*Rp = maxmidmin[(indices[direction] >> 8) & 0x0f]) << 16;
    ret |= (*Gp = maxmidmin[(indices[direction] >> 4) & 0x0f]) << 8;
    ret |= (*Bp = maxmidmin[(indices[direction] >> 0) & 0x0f]);
    return ret;
}


/***************************************************** SmoothLongs */
/***
	Function:	SmoothLongs

	Description:
		This function implements a simpliest smoothing algorithm
		in place for an array of longs

	Statics/Globals:
		None

	Input Arguments:
		pmem - pointer to the array of longs

		lmem - number of elements in the array

		points - number neighboring points to use

	Returns:
		none
****/
void _stdcall SmoothLongs (
			long *pmem, long lmem, short points)
{
	long cnt, wrk, wrk1;
	long *r, *w;

	if ((points--) < 2) return;
Iter:
	if (!(points--)) return;
	r = w = pmem;	cnt = lmem - 1;
	while (cnt--)
	{
		wrk = *r++;  wrk += *r;
		*w++ = (wrk + 1) / 2;
	}
	if (!(points--)) return;
	r = pmem;	wrk1 = *r++;
	w = r;	cnt = lmem - 1;
	while (cnt--)
	{
		wrk = wrk1;	 wrk1 = *r++;
		*w++ = (wrk + wrk1) / 2;
	}
	goto Iter;
}


/***************************************************** SortInPlace */
/***
	Function:	SortInPlace

	Description:
      elsize: size(mem[i]) == mempitch;
      sorting by element of given type at offset offset
      operation = offset * 256 | decrease | type,
      decrease: 0x80   - in decreasing order
      types:
        0 - char
        1 - byte
        2 - short
        3 - word
        4 - long
        5 - dword
        6 - *char (C-string, case insensitive)
        7 - *byte (C-string, case sensitive)
        8 - double
        9 - float
        	string inside the element:
       10 - char C-string, case insensitive
       11 - byte C-string, case sensitive

	Returns: None

***/
void _stdcall SortInPlace (void *parmem, long elsize,
					long numof, long operation)
{
	long elemoffset, comparetype, decrease, *indmem;
    BYTE *mem;

   	elemoffset = operation >> 8;
   	comparetype = operation & 0x7f;
   	decrease = operation & 0x80;
   	mem = (BYTE *)parmem;
   	if ((numof < 1) || (elemoffset < 0)) return;
   	indmem = (long *)malloc(numof * sizeof(long));
    if (indmem)
    {
		IndexSortElems (mem + elemoffset, elsize, numof,
    	  				 indmem, comparetype, decrease);
	   	PermutateInPlace (mem, elsize, indmem, numof);
   		free(indmem);
    }
}


/************************************************ Substitute */
/*****
  Substitute in 'memsize' bytes begining from 'mem' each
   {what,whatsize} piece into {to,tosize} and send everything
   to 'where'.
  Return the size of the result text if OK,
   or 0 if the limit was riched before memsize.
*****/
DWORD _stdcall Substitute(char *mem, DWORD memsize,
			   			char *where, DWORD limit,
                		char *what,  DWORD whatsize,
                		char *to,    DWORD tosize)
{
   	char c0;
   	DWORD k, ns, nw, ret = 0;

   	if (whatsize <= 0)
   	{ // just copy
      	if (memsize <= limit) ret = memsize;
      	else {  ret = 0;   memsize = limit; }
      	memmove (where, mem, memsize);
      	return ret;
   	}
   	c0 = *what;  ns = nw = 0;
   	while ((ns < memsize) && (nw < limit))
    {
         	if (mem[ns] != c0)
            {
GoAhead:
            	where[nw++] = mem[ns++];
            } else
            {
				if (ns + whatsize > memsize) goto GoAhead;
                if (memcmp (mem + ns, what, whatsize) != 0)
                	goto GoAhead;
                ns += whatsize;  k = 0;
                while ((k < tosize) && (nw < limit))
                	where[nw++] = to[k++];
            }
    }
	if (ns == memsize) ret = nw;
   	return ret;
}


/************************************************* SubwindToBitMap */
/***
typedef struct _RECT {    // rc
    LONG left;
    LONG top;
    LONG right;
    LONG bottom;
} RECT;
***/
DWORD _stdcall SubwindToBitMap (long wndHandle,
								long xl, long yt, long wid, long hei)
{
	HDC hdc, hdcCompatible = 0;
   HBITMAP hbmp = 0;
	HWND hwnd;
	RECT wrect;
   long xw, yw;

   hwnd = (HWND)wndHandle;
	hdc = GetDC (hwnd);
   GetClientRect(hwnd, &wrect);
   xw = wrect.right - wrect.left;
   yw = wrect.bottom - wrect.top;
   if ((xl >= xw) || (yt >= yw)) goto Fin;
   if ((wid == 0) || (xl + wid > xw)) wid = xw - xl;
   if ((hei == 0) || (yt + hei > yw)) hei = yw - yt;

	hdcCompatible = CreateCompatibleDC(hdc);
	if (!hdcCompatible) goto Fin;
/* Create a compatible bitmap for hdc. */
	hbmp = CreateCompatibleBitmap(hdc, wid, hei);
	if (!hbmp) goto Fin;
/* Select the bitmaps into the compatible DC. */
	if (!SelectObject(hdcCompatible, hbmp)) goto Fin;
/* Copy color data from the display into a
   bitmap that is selected into a compatible DC. */
   BitBlt(hdcCompatible, 0, 0, wid, hei,
                 hdc, xl, yt, SRCCOPY);
Fin:
	ReleaseDC (hwnd, hdc);
   if (hdcCompatible) DeleteDC (hdcCompatible);
   return (DWORD)hbmp;
}


/************************************************** WindowToBitMap */
DWORD _stdcall WindowToBitMap (long wndHandle)
{
   return (DWORD) SubwindToBitMap (wndHandle, 0, 0, 0, 0);
}


/****************************************************** TableSubst */
/***
	Function:	TableSubst

	Description:
		This function copies a given buffer with substitution
        of each source byte with a byte from a given table.

	Statics/Globals:

	Input Arguments:
		src	- pointer to the source buffer
		dest - pointer to the target buffer
		size - (common) size of both buffers (in bytes)
      	table - pointer to the byte table (256 bytes)

	Output Arguments:
		None

	Returns: None

***/
void _stdcall TableSubst (char *src, char *dest,
				DWORD size, void *ptable)
{
	DWORD i;
    BYTE *s, *d, *table;

    if (ptable) table = (BYTE *)ptable; 
	else table = (unsigned char*)InversedBytes;
    s = (BYTE *)src;  d = (BYTE *)dest;
    for (i = 0;  i < size;  i++)
        d[i] = table[s[i]];
}


/************************************************ TextFromClpbrd */
/***
	Function:	TextFromClpbrd

	Description:
		This function copyes a text in CF_TEXT format from system
      Clipboard into a memory buffer

	Statics/Globals:
		None

	Input Arguments:
		mem - pointer to memory buffer where to copy the content
			of the Clipboard object of a given clipboard type

		limit - length of the memory buffer (bytes)

	Returns:
		memory size of the Clipboard object if OK, 0 otherwise.
		Note1: the size (and return value) may be more than <limit>;
		in this case buffer's content is truncated.
        Note2: if ((mem==NULL) || (limit==0)) then function doesn't copy
        data but still returns memory size of the Clipboard object

***/
long  _stdcall TextFromClpbrd (
                  char *mem,
                  DWORD limit)
{
	 long ret = 0;
	 HGLOBAL ghndl;
	 char *gaddr;
	 DWORD gsize;

	 if (OpenClipboard(GetActiveWindow()))
	 {
		 ghndl = GetClipboardData (CF_TEXT);
		 if (ghndl)
		 {
			 if (CloseClipboard())
			 {
				 gsize = GlobalSize (ghndl);
                 if ((mem) && (limit))
                 {
    				 if (gsize > limit) gsize = limit;
	    			 gaddr = (char *)GlobalLock(ghndl);
		    		 MovMemory (gaddr, mem, gsize);
			    	 GlobalUnlock(ghndl);
                 }
				 ret = (long)gsize;
			 }
		 }
	 }
//Fin:
	 return ret;
}


/************************************************** TextToClpbrd */
/***
	Function:	TextToClpbrd

	Description:
		This function copyes given text into a	global Windows
      buffer and puts it on the system Clipboard as an object
      of CF_TEXT type;
      adds terminating 0 if no

	Statics/Globals:
		None

	Input Arguments:
		mem - pointer to memory buffer which content is to put
			on the Clipboard
		memlen - length of the buffer

	Output Arguments:
		None

	Returns:  1 if OK, 0 otherwise

***/
long _stdcall TextToClpbrd (
                     char *mem,
                     DWORD memlen)
{
	 long ret = 0;
	 HGLOBAL ghndl;
	 char last, *gaddr;
    DWORD gmemlen;

    gmemlen = memlen;  last = mem[memlen - 1];
    if (!last) gmemlen++;
	 ghndl = GlobalAlloc(GMEM_MOVEABLE, gmemlen); // get global buffer handle
	 if (!ghndl) goto Fin;
	 gaddr = (char *)GlobalLock(ghndl);          // get global buffer address
	 if (!gaddr) goto FinFree;
	 MovMemory (mem, gaddr, memlen);             // copy to global buffer
    if (!last) gaddr[memlen] = 0;					// terminating 0
	 GlobalUnlock(ghndl);
	 if (OpenClipboard(GetActiveWindow()))
	 {
		  if (EmptyClipboard())
		  {
			 if (SetClipboardData (CF_TEXT, ghndl))
			 {
				 if (CloseClipboard())
					 ret = 1;
			 }
		}
	}
FinFree:
	if (!ret) GlobalFree(ghndl);
	// otherwise it became a clipboard object
Fin:
	return ret;
}


/****************************************************** VectorInfo */
/*********
	Description:
    Calculates min, max and sum of the vector elements.

    <v> 	 - pointer to the (pitched) vector array
 	<pitchv> - pitch IN BYTES (!)
 	<nv> 	 - number of elements in <v>
    <type> -
        0 - char
        1 - byte
        2 - short
        3 - word
        4 - long
        5 - dword

        8 - double
        9 - float

    <pmin>, <pmax> and <psum> - pointers to get results;
    	if type==8 : results are (double)
    	if type==9 : results are (float)
        else (long) for types 0,2,4; (DWORD) for 1,3,5
    <pimin>, <pimax> - pointers to get indices of min /max values;

	Returns: None


*********/
void _stdcall VectorInfo (void *v, long pitchv, long nv, long type,
						  void *pmin, void *pmax, void *psum,
                          long *pimin, long *pimax)
{
   	double dmin, dmax, dsum, dv;
   	float fmin, fmax, fsum, fv;
    DWORD dwmin, dwmax, dwsum, dwv;
    long lmin, lmax, lsum, lv, i, imin, imax;
    BYTE *bv;

    bv = (BYTE *)v;  i = imin = imax = 0;
	switch (type)
	{
	  	case 0:	// char
            lv = *(char *)bv;
            lmin = lmax = lv;  lsum = 0;
   			while (i < nv)
   			{
             	lv = *(char *)bv;
                if (lmin > lv) { lmin = lv;  imin = i; }
                if (lmax < lv) { lmax = lv;  imax = i; }
                lsum += lv;
                i++;  bv += pitchv;
            }
            if (pmin) *(long *)pmin = lmin;
            if (pmax) *(long *)pmax = lmax;
            if (psum) *(long *)psum = lsum;
			break;
	  	case 1:	// byte
            dwv = *bv;
            dwmin = dwmax = dwv;  dwsum = 0;
   			while (i < nv)
   			{
             	dwv = *bv;
                if (dwmin > dwv) { dwmin = dwv;  imin = i; }
                if (dwmax < dwv) { dwmax = dwv;  imax = i; }
                if ((dwsum += dwv) < dwv) dwsum = 0xffffffff;
                i++;  bv += pitchv;
            }
            if (pmin) *(DWORD *)pmin = dwmin;
            if (pmax) *(DWORD *)pmax = dwmax;
            if (psum) *(DWORD *)psum = dwsum;
			break;
	  	case 2:	// short
            lv = *(short *)bv;
            lmin = lmax = lv;  lsum = 0;
   			while (i < nv)
   			{
             	lv = *(short *)bv;
                if (lmin > lv) { lmin = lv;  imin = i; }
                if (lmax < lv) { lmax = lv;  imax = i; }
                lsum += lv;
                i++;  bv += pitchv;
            }
            if (pmin) *(long *)pmin = lmin;
            if (pmax) *(long *)pmax = lmax;
            if (psum) *(long *)psum = lsum;
			break;
	  	case 3:	// word
            dwv = *(WORD *)bv;
            dwmin = dwmax = dwv;  dwsum = 0;
   			while (i < nv)
   			{
             	dwv = *(WORD *)bv;
                if (dwmin > dwv) { dwmin = dwv;  imin = i; }
                if (dwmax < dwv) { dwmax = dwv;  imax = i; }
                if ((dwsum += dwv) < dwv) dwsum = 0xffffffff;
                i++;  bv += pitchv;
            }
            if (pmin) *(DWORD *)pmin = dwmin;
            if (pmax) *(DWORD *)pmax = dwmax;
            if (psum) *(DWORD *)psum = dwsum;
			break;
	  	case 4:	// long
            lv = *(long *)bv;
            lmin = lmax = lv;  lsum = 0;
   			while (i < nv)
   			{
             	lv = *(long *)bv;
                if (lmin > lv) { lmin = lv;  imin = i; }
                if (lmax < lv) { lmax = lv;  imax = i; }
                lsum += lv;
                i++;  bv += pitchv;
            }
            if (pmin) *(long *)pmin = lmin;
            if (pmax) *(long *)pmax = lmax;
            if (psum) *(long *)psum = lsum;
			break;
	  	case 5:	// dword
            dwv = *(DWORD *)bv;
            dwmin = dwmax = dwv;  dwsum = 0;
   			while (i < nv)
   			{
             	dwv = *(DWORD *)bv;
                if (dwmin > dwv) { dwmin = dwv;  imin = i; }
                if (dwmax < dwv) { dwmax = dwv;  imax = i; }
                if ((dwsum += dwv) < dwv) dwsum = 0xffffffff;
                i++;  bv += pitchv;
            }
            if (pmin) *(DWORD *)pmin = dwmin;
            if (pmax) *(DWORD *)pmax = dwmax;
            if (psum) *(DWORD *)psum = dwsum;
			break;
	  	case 8:	// double
            dv = *(double *)bv;
            dmin = dmax = dv;  dsum = 0;
   			while (i < nv)
   			{
             	dv = *(double *)bv;
                if (dmin > dv) { dmin = dv;  imin = i; }
                if (dmax < dv) { dmax = dv;  imax = i; }
                dsum += dv;
                i++;  bv += pitchv;
            }
            if (pmin) *(double *)pmin = dmin;
            if (pmax) *(double *)pmax = dmax;
            if (psum) *(double *)psum = dsum;
			break;
	  	case 9:	// float
            fv = *(float *)bv;
            fmin = fmax = fv;  fsum = 0;
   			while (i < nv)
   			{
             	fv = *(float *)bv;
                if (fmin > fv) { fmin = fv;  imin = i; }
                if (fmax < fv) { fmax = fv;  imax = i; }
                fsum += fv;
                i++;  bv += pitchv;
            }
            if (pmin) *(float *)pmin = fmin;
            if (pmax) *(float *)pmax = fmax;
            if (psum) *(float *)psum = fsum;
			break;
	  	default:
        	break;
	}
    if (pimin) *pimin = imin;
    if (pimax) *pimax = imax;
}


/****************************************************** WordInText */
/***
	Function:	WordInText

	Description:
		Assume that the text in range
			from  begtext+offs  to  begtext+limit
		consists from 'words' - sequences of more-than-space
		symbols separated by one or more 'blanks' - sequences
		of less-than-or-equal-to-space symbols.
		This function searches for the first word in the range
		which is equal to a given one; search is not case sensitive.

	Statics/Globals:
		None

	Input Arguments:
		begtext - pointer to the beginning of text

		offs - pointer to variable which value is an offset
			from <begtext> to the beginning point of searching;
            NULL is OK

		limit - byte at address <begtext+limit> is the first
			outside of the searched area
         limit==0 means 'up to terminating zero'

		word - address of the word to search

		wordlen - length of the word;
      	wordlen==0 means 'WordLength(word)'

	Output Arguments:
		offs (if given) - after returning is updated
        	to the offset of the beginning of the word

	Returns: number of the word in the text,
   			0 if no.

***/
DWORD _stdcall WordInText (
							char *text,
                     DWORD *offs, DWORD limit,
                     char *pword, DWORD wordlen)
{
	DWORD woffs, wcnt, wl, ret;

    woffs = wcnt = ret = 0;
	if (offs) woffs = *offs;
   	if (! limit) limit = strlen(text);
   	if (!wordlen) wordlen = WordLength (pword, 0L, limit - woffs);
	while ((wl = GetTheWord (text, &woffs, limit)) > 0)
   	{
      	wcnt++;	// current number
      	if (wl == wordlen)
      	{
      		if (_memicmp (text + woffs, pword, wordlen) == 0)
			{  //  ^ - ignoring case
				if (offs) *offs = woffs;
         		ret = wcnt;   break;
         	}
      	}
      	woffs += wl;
   	}
	return ret;
}


/*************************************************** WordNotInList */
/*****
	Function:	WordNotInList

	Description:
		This function searches for the beginning of the first word
		in a given part of text (from  src+offs  to  src+limit),
        which does not present in the list;

	Input Arguments:
		src - pointer to the beginning of the source text
		offs - pointer to (DWORD)variable which value is an offset
			from <src> to the beginning point of searching for
			first not-more-than-space symbol
        srcsize - number of bytes in <src>
		list - pointer to the beginning of the list
		listsize - number of bytes in <list>

	Returns:
        if found, the function updates the <offs> value and
        	returns the length of the word
        0 otherwise

*****/
DWORD _stdcall WordNotInList(
				void *src, DWORD *srcoffs, DWORD srcsize,
                void *list, DWORD listsize)
{
   	DWORD wl, wnum, wrk, offs;
    BYTE *sp, *lp;

    sp = (BYTE *)src;   lp = (BYTE *)list;
    offs = *srcoffs;
	while ((wl = GetTheWord ((char*)sp, &offs, srcsize)) > 0)
   	{
        wrk = 0;
		wnum = WordInText ((char*)lp, &wrk, listsize, (char*)(sp + offs), wl);
        if (wnum) offs += wl;
        else { *srcoffs = offs;   break; } // not in list
   	}
   	return wl;
}


/********************************************** WordToWord */
/*****
  Substitutes in <srcsize> bytes begining from <src>
  every word from <what> list with corresponding word
  from <into> list, and sends resulting text to <res>.
  Returns the size of the text if OK,
    or 0 if the limit was riched before memsize.
*****/
DWORD _stdcall WordToWord(char *src, DWORD srcsize,
			   			char *res, DWORD limit,
                		char *what, DWORD whatsize,
                        char *into, DWORD intosize)
{
   	DWORD nwinto, nwwhat, wl, wnum, wrk,
    		resoffs = 0, oldoffs = 0, woffs = 0;
    DWORD *pairs = NULL;

    nwwhat = ParseWordList(what, whatsize, NULL, 0);
    if (!nwwhat) goto Fin;
    pairs = (DWORD *)malloc(nwwhat * 2 * sizeof(long));
    if (!pairs) goto Fin;
    nwinto = ParseWordList(into, intosize, pairs, nwwhat);
	while ((wl = GetTheWord (src, &woffs, srcsize)) > 0)
   	{
        while ((oldoffs < woffs) && (resoffs < limit))
        	res[resoffs++] = src[oldoffs++];
		if (resoffs == limit) break;
		wnum = WordInText (what, NULL, whatsize, src + woffs, wl);
        woffs += wl;
        if (wnum)
        {
            if (wnum > nwinto) wnum = nwinto;
        	wrk = pairs[2 * wnum - 2];
        	wl = wrk + pairs[2 * wnum - 1];
	        while ((wrk < wl) && (resoffs < limit))
    	    	res[resoffs++] = into[wrk++];
        } else
        {	// move the word:
	        while ((oldoffs < woffs) && (resoffs < limit))
    	    	res[resoffs++] = src[oldoffs++];
        }
        if (resoffs == limit) break;
        oldoffs = woffs;
   	}
Fin:
	if (pairs) free(pairs);
   	return (long)resoffs;
}


/*********************************************** GetCPUclockSpeed */
/*****
    Returns: CPU Clock Speed in clocs per millisecond (=kHz)
***/
double _stdcall GetCPUclockSpeed (void)
{
	double dret = -1.0;
#ifdef  PentiumIIwithMMX
	if (CPU_MMX() >= 0x50)
    {	// at least Pentium
    	dret = GetClockSpeed ();
    }
#endif // PentiumIIwithMMX
    return dret;
}


/****************************************************** GetCPUtype */
/*****
 returns:
  	0x50 - Pentium without MMX support
  	0x51 - Pentium with MMX support
  	0x61 - Pentium II (always with MMX support)
  	0x71 - Pentium III (always with MMX support)
	0 otherwise
long _stdcall CPU_MMX (void);
*****/
long _stdcall GetCPUtype (void)
{
	long ret = 0;

#ifdef  PentiumIIwithMMX
    ret = CPU_MMX ();
#endif // PentiumIIwithMMX
    return ret;
}


/***************************************************** GetTimeMsec */
/*****
    Returns: time from last computer plug in (milliseconds)
***/
double _stdcall GetTimeMsec (void)
{
#ifdef  PentiumIIwithMMX
	if (CPU_MMX() >= 0x50)
    {	// at least Pentium
    	return tmGetTimeMsec ();
    }
#endif // PentiumIIwithMMX
    return (double)GetTickCount();
}


/**************************************************** InverseBytes */
/***
	Function:	InverseBytes

	Description:
		This function inverses the order of bits inside each byte in
		a given buffer using (static char) InversedBytes[256]

	Statics/Globals:
		InversedBytes[]

	Input Arguments:
		buffer 	- pointer to the buffer
		buffersize - size of the buffer in bytes

	Output Arguments:
		None

	Returns: None

***/
void _stdcall InverseBytes (char *buffer, DWORD buffersize)
{
	TableSubst (buffer, buffer, buffersize, InversedBytes);
}


/**************************************************** LeftToRight */
/***
	Function:	LeftToRight

	Description:
		This function copies bytes from a given buffer
        in reverse order, optionaly with substitution
        of each source byte with a byte from
        InversedBytes table.

	Statics/Globals:
		InversedBytes[]

	Input Arguments:
		src	- pointer to the source buffer
		dest - pointer to the target buffer
		size - (common) size of both buffers (in bytes)
      	table - pointer to the byte table (256 bytes)
        invbytes: if not zero - with inversing of bits
        		inside each byte

	Output Arguments:
		None

	Returns: None

***/
void _stdcall LeftToRight (char *src, char *dest,
				DWORD size, long invbytes)
{
	DWORD i, k;
    BYTE c, cf;

    i = 0;  k = size;
	while (i < (k--))
	{
        c = src[i];  cf = src[k];
        if (invbytes)
        {
         	c = InversedBytes[c];
         	cf = InversedBytes[cf];
        }
        dest[k] = c;  dest[i++] = cf;
	}
}


/************************************************* FillHexLine */
/***
	Function:	FillHexLine

	Description:
		This function fills a 54 bytes memory buffer with
        hexadecimal presentation of given memory field content
        (not more than 16 bytes);
		following is the layout of the line in the presentation:
		<16 bytes in hex> "  " <16 bytes as is OR through lookup table>
           4 * (8 + 1)  +   2    +   16 = 54 bytes

	Statics/Globals:
		None

	Arguments:
		src - pointer to source memory locations

		buff - pointer target memory locations;
			size of this buffer must be at least 54 bytes

		srclen - number of source bytes (<= 16)

        table - NULL or pointer to lookup table of BYTE format

    returned value: 54

***/
long _stdcall FillHexLine (void *src, long srclen, void *buff, BYTE *table)
{
	BYTE *to, *from, symb;
	long i, n, n1, k, ret;

   	to = (BYTE *)buff;   from = (BYTE *)src;
    n = 16;  n1 = 38;
    ret = n1 + n;
 	FillMemory (to, ret, asciiSPACE);
    if (srclen > 0)
    {
	    if (srclen < n) k = srclen; else k = n;
    	i = MemToHex (from, k, to, 4);
        to[i] = asciiSPACE;	// erase terminating 0
	    for (i = 0;  i < k;  i++)
    	{
            symb = from[i];  if (table) symb = table[symb];
        	if (symb < asciiSPACE) symb = MemToHex_Service;
	       	to[n1 + i] = symb;
   		}
    }
	return ret;
}


/************************************************* HexDump */
/***
	Function:	HexDump

	Description:
		This function fills a memory buffer with hexadecimal
		presentation of given memory field content;
		here is the layout of the each line in the presentation:
		[? <address> ": " ] <16 bytes in hex> "  " <16 bytes as is>
         [? 8     +  2 ] +  4 * (8 + 1)   +    2  + 16 = [? 10 ] + 54

	Arguments:
		src - pointer to source memory locations

		srclen - number of source bytes

		buff - pointer to target memory;
			size of this buffer must be big enough to fit target
			information; each (full or not) 16 bytes in src
            produce [? 10 if address] + 54 + length of line separator
            bytes. So, required buffer size is:
              Ent((srclen + 15) / 16) *
              ( (addr!=0xffffffff ? 10 : 0) + 54 + separator's length )

		addr - (optional) address to include in each line at the left
        		(may be not equal to <from>);
        	   	if addr==0xffffffff: skip the field

        separator - nonzero LSB are end-of-line bytes

        table - NULL or pointer to lookup table of BYTE format

	Returns:
		Number of bytes written to <buff>

***/
long _stdcall HexDump (void *src, long srclen, void *buff,
					DWORD addr, DWORD separator, BYTE *table)
{
	BYTE *to, *from;
    DWORD offs, sep;
	long k, portion, n, ret;
    char *format;

    if (MemToHex_Capitals) format = "%.8lX: ";
    else  format = "%.8lx: ";
   	to = (BYTE *)buff;   from = (BYTE *)src;
   	offs = ret = 0;  n = 16;
   	while ((portion = srclen - offs) > 0)
   	{
        if (portion > n) portion = n;
    	if (addr != 0xffffffff)
        {
      		k = sprintf ((char*)to, format, addr + offs);
            ret += k;  to += k;
        }
		k =  FillHexLine (from + offs, portion, to, table);
        ret += k;  to += k;  sep = separator;
        while (sep)
        {
         	*to++ = sep & 0xff;
            sep >>= 8;   ret++;
        }
        offs += portion;
   	}
	return ret;
}


/******************************************************** HexToMem */
/***
	Function:	HexToMem

	Description:
		This function fills a memory buffer with bytes
        given by hexadecimals; ignores whitespaces

	Arguments:
		hexs - pointer to source memory locations
	    len - number of source bytes;
		to - pointer to target memory locations;
			size of this buffer must be big enough to fit
            target information {usualy not less than (len+1)/2 bytes }

	Returns:
		Number of bytes written to <to>;
        if it is less than (len+1)/2 bytes - there was not a
        hexadecimal symbol

***/
long _stdcall HexToMem (void *hexs, long hexlen, void *to)
{
    BYTE b, *r, *w, bA10, ba10;
	long i, k, n;
    DWORD c;

    r = (BYTE *)hexs;  w = (BYTE *)to;
    i = n = k = 0;  c = 0;
    bA10 = 'A' - 10;  ba10 = 'a' - 10;
    while (i < hexlen)
    {
        b = r[i++];
        if ((b >= '0') && (b <= '9')) { b -= '0';  goto Ok; }
        if ((b >= 'A') && (b <= 'F')) { b -= bA10;  goto Ok; }
        if ((b >= 'a') && (b <= 'f')) { b -= ba10;  goto Ok; }
        if (b <= asciiSPACE) continue;
        break;  // neither hex, nor whitespace
Ok:
        c = (c << 4) | b;
        if (k)
        {
            w[n++] = (BYTE)c;  c = 0;
        }
        k ^= 1;
    }
    if (k) w[n++] = (BYTE)(c << 4);
	return n;
}


/************************************************** GetTheWordBack */
/***
	Function:	GetTheWordBack

	Description:
		This function searches for the beginning of the word,
		pointed by <*offset> (inside, just before or after),
		then updates the <offset> value and returns the length of
		the word, i.e. a number of more-than-space symbols;

	Input Arguments:
		text - pointer to the beginning of the text

		offset - pointer to (DWORD)variable which value is an offset
			from <begtext> to the point of searching

		limit - size of the text in bytes

	Output Arguments:
		offset - after return is updated to the offset of the beginning
			of the word

	Returns: length of the word

***/
DWORD _stdcall GetTheWordBack (
			   void  *text,
               DWORD *offset,
               DWORD  limit)
{
    BYTE *pnt = (BYTE *)text;
    DWORD offs = *offset;

   	if (offs >= limit) offs = limit;
    else if (pnt[offs] > asciiSPACE) goto BackOverNotBlanks;
    // back through blanks:
   	while (offs > 0)
    	if (pnt[--offs] > asciiSPACE) break;
BackOverNotBlanks:
   	while (offs > 0)
    	if (pnt[--offs] <= asciiSPACE) { offs++;  break; }
	*offset = offs;
	return WordLength ((char *)text, offs, limit);
}


long _stdcall gEmptyClipboard()
{
        return EmptyClipboard();
}


/****************************************************** FindPieceOfWord */
/***
	Function:	FindPieceOfWord

	Description:
		Assume that the text in range
			from  begtext+offs  to  begtext+limit
		consists from 'words' - sequences of more-than-space
		symbols separated by one or more 'blanks' - sequences
		of less-than-or-equal-to-space symbols.
		This function searches for the first word in the range
		which includes a given 'piece'; search is not case sensitive.

	Statics/Globals:
		None

	Input Arguments:
		begtext - pointer to the beginning of text

		offs - pointer to (WORD)variable which value is an offset
			from <begtext> to the beginning point of searching

		limit - byte at address <begtext+limit> is the first
			outside of the searched area

		piece - address of the 'piece'

		piecelen - length of the 'piece'

	Output Arguments:
		offs - after return is updated to the offset of the beginning
			of the word

	Returns: length of the word

***/
long _stdcall FindPieceOfWord (
					char *where,
               DWORD *offs,
               DWORD limit,
               char *what,
               DWORD whatlen)
{
	long l, ll, k, wlen = 0;
    char c, clow, cupper;

    if ((!what) || (!whatlen) || (!where) || (!limit)) goto Fin;
    k = l = *offs;  ll = limit - whatlen;
    c = clow = cupper = *what;
    if ((c >= 'A') && (c <= 'Z')) clow |= 0x20;
    if ((c >= 'a') && (c <= 'z')) cupper &= 0xdf;
    while (l <= ll)
    {
       c = where[l];
       if ((c == clow) || (c == cupper))
       {
// int memicmp(const void *s1, const void *s2, size_t n);
			if (_memicmp(where + l, what, whatlen) == 0)
           	{
               wlen = whatlen;   break;
           	}
       }
       l++;
    }
    if (wlen)
    {
       while (l > k)
       {
          c = where[l-1];
		  if (c <= asciiSPACE) break;
		  wlen++;  l--;
	   }
	   *offs = l;   l += wlen;
	   while ((DWORD)l < limit)
	   {
			  if (where[l] <= asciiSPACE) break;
			  wlen++;  l++;
	   }
	}
Fin:
	return wlen;
}


/********************************************** FindWordFromList */
/*****
  Scans <text> from <*offs> to limit for a word from <wordlist>.
  If (usecase) - case sensitive.
  If found - returns the number of the word in the word list,
    sets <*offs> (if given) to offset before found word,
    sets <*wordlen> (if given) to the lenght of found word.
  Returns 0 otherwise, sets <*offs> (if given) to offset after last word;.
*****/
long _stdcall FindWordFromList(char *text,
                     DWORD *offs, DWORD limit,
                     char *wordlist, DWORD listlen,
                     DWORD *wordlen, DWORD usecase)
{
   	DWORD wl, wnum, listoffs, woffs;
    bool equal = true;

    if (offs) { woffs = *offs;  *offs = limit; }
    else woffs = 0;
	while ((wl = GetTheWord (text, &woffs, limit)) > 0)
   	{
        listoffs = 0;
		wnum = WordInText (wordlist, &listoffs, listlen, text + woffs, wl);
        if (wnum)
        {
            if (usecase)
            {
                equal = (memcmp(text + woffs,
                         wordlist + listoffs, wl) == 0);
            }
            if (equal)
            {
                if (wordlen) *wordlen = wl;
                if (offs) *offs = woffs;
                break;
            }
        }
        woffs += wl;
   	}
   	return (long)wnum;
}


/**************************************************** GetDirection */
/*****
  Fills <dst> with incremental code bytes for straight line
  from point {0,0} (not including) to {dx,dy}.
  Returns direction as a long value in range from 0 to 8:

             6 5 4
          ^  7 8 3
          |  0 1 2
          y  x ->           #8 corresponds to {Xc,Yc}

*****/
long _stdcall GetDirection(long deltax, long deltay)
{
	long dx, dy, cx, cy;

    if (deltax)
   	{
   	    if (deltax < 0) { cx = StepCodeM0;  dx = -deltax; }
   	    else { cx = StepCodeP0;  dx = deltax; }
   	} else { cx = StepCode00;  dx = 0; }
   	if (deltay)
   	{
   	    if (deltay < 0) { cy = StepCode0M;  dy = -deltay; }
   	    else { cy = StepCode0P;  dy = deltay; }
   	} else { cy = StepCode00;  dy = 0; }
   	if (5 * dy < 2 * dx) cy = StepCode00;
    if (5 * dx < 2 * dy) cx = StepCode00;
    return StepCodesToDirection[cx | cy];
}


/******************************************************* ArcCenter */
/*****
    Calculates the point equally distanced from all given points
    (least square criteria for pairs).
    Returns number of used pairs if OK,
            0 if bad.
*****/
long _stdcall ArcCenter(POINT *points, long npoints,
            double distmin, double *centerx, double *centery)
{
    double dxx, dyy, dxy, drx, dry, x1, y1, x2, y2,
           dx, dy, dmin2, r2, dr2, nd, det,
           abx[8];
    long i1, i2, n = 0;

    if (npoints < 3) goto Fin;
    dxx = dyy = dxy = drx = dry = 0.0;
    dmin2 = distmin * distmin;
    for (i1 = 0;  i1 < npoints - 1;  i1++)
    {
        x1 = points[i1].x;   y1 = points[i1].y;
        r2 = x1 * x1 + y1 * y1;
        for (i2 = i1 + 1;  i2 < npoints;  i2++)
        {
            x2 = points[i2].x;   dx = x2 - x1;
            y2 = points[i2].y;   dy = y2 - y1;
            dr2 = x2 * x2 + y2 * y2 - r2;
            if (dx * dx + dy * dy >= dmin2)
            {
                n++;    dxy += (dx * dy);
                dxx += (dx * dx);  drx += (dx * dr2);
                dyy += (dy * dy);  dry += (dy * dr2);
            }
        }
    }
    if (!n) goto Fin;
    nd = (double)n;
    // matrix a[2,2]:
    abx[0] = dxx / nd;  abx[3] = dyy / nd;
    abx[1] = abx[2] = dxy / nd;
    // vector b[2]:
    abx[4] = drx / (2 * nd);  abx[5] = dry / (2 * nd);
    // center coordinates:
    det = Linear2by2(abx, abx + 4, abx + 6, 1e-8);
    if (!det) { n = 0;  goto Fin; }
    *centerx = abx[6];  *centery = abx[7];
Fin:
    return n;
}


/****************************************************** CodeCircle */
/*****
  Fills <dst> with incremental code bytes for circle of a given
  radius, begining from point {0, rad}, clockwise.
  <radrad> is the radius power 2 (to allow not just integer
  values for radius).
  Returns the number of written bytes.
*****/
//  dx = if (code&4) then (code&1)*2-1 else 0
//  dy = if (code&8) then (code&2)-1 else 0
//  invalid codes: 0, 1, 2, 3, 6, 7, 9, B;
//  valid codes:
//  5:  1, 0;  F:  1, 1;  A: 0, 1;  E: -1, 1;
//  4: -1, 0;  C: -1,-1;  8: 0,-1;  D:  1,-1;

long _stdcall CodeCircle(void *dst, long dstsize, long rad, long radrad)
{
	long x, y, r1, r2, ar1, ar2, n, i, dir, ret;
    BYTE c, c1, c2, c3, *w;

    w = (BYTE *)dst;  n = ret = 0;
    r1 = rad * rad - radrad;
    x = 0;  y = 2 * rad;
// Upper octant:
    c1 = StepCodeP0; // {+1,  0}
    c2 = StepCodePM; // {+1, -1}
    c3 = StepCode0M; // { 0, -1}
    while ((y > x) && (ret < dstsize))
    {
        r1 += (x + 1);      ar1 = abs(r1);
        r2 = r1 - y + 1;    ar2 = abs(r2);
        if (ar1 < ar2) { w[n] = c1; }
        else { w[n] = c2;  r1 = r2;  y -= 2; }
        x += 2;  n++;  ret++;
    }
    if (ret >= dstsize) goto Fin;
// Right octant:
    if (y == x) i = n;  else i = n - 1;
    while ((i--) && (ret < dstsize))
    {
        if ((c = w[i]) == c1) c = c3;
        w[n++] = c;   ret++;
    }
    i = 0;
    while ((i < n) && (ret + 2 < dstsize))
    {
        dir = StepCodesToDirection[w[i]];
		w[i + n] = DirToStepCodes[(dir + 6) & 7];
        w[i + 2 * n] = DirToStepCodes[(dir + 4) & 7];
        w[i + 3 * n] = DirToStepCodes[(dir + 2) & 7];
        i++;   ret += 3;
    }
Fin:
    return ret;
}


/***************************************************** CodeContour */
/*****
  Fills <dst> with incremental code bytes for contour
  from point {xstart,ystart}.
  Returns the number of written bytes.
//  dx = if (code&4) then (code&1)*2-1 else 0
//  dy = if (code&8) then (code&2)-1 else 0
//  invalid codes: 0, 1, 2, 3, 6, 7, 9, B;
//  valid codes:
//  5:  1, 0;  F:  1, 1;  A: 0, 1;  E: -1, 1;
//  4: -1, 0;  C: -1,-1;  8: 0,-1;  D:  1,-1;
		This function returns the
    Intermediate coding of neighborhood for a given pixel
    as a bit set where number of bit corresponds to:

             6 5 4
          ^  7 8 3
          |  0 1 2
          y  x ->           #8 corresponds to {Xc,Yc}

            Table {0-7} => code: see <DirToStepCodes>
    0x0C, 0x08, 0x0D, 0x05, 0x0F, 0x0A, 0x0E, 0x04;

	Statics/Globals:
		DirToStepCodes

	Arguments:
		dst - pointer to target memory buffer
        dstsize - its size in bytes
		pmask - pointer to the Bit Mask
    	xe - number of pixels (bits) in each row
		ye - number of rows
		pitch - address difference between neighboring rows (in bytes)
				in the limiting Bit Mask
        xstart, xstart - coordinates of the start pixel
        command bits:
            0-2 : start direction
            3 :  1 - clockwise, 0 - counter clockwise

	Returns: the number of written bytes if OK,
        -1 if bad coordinates

***/
long _stdcall CodeContour (void *dst, long dstsize,
                void *pmask, long xe, long ye, long pitch,
                long xstart, long ystart, long command)
{
    long direction, step, vicinity, x, y, i, t,
        ret = -1, firstdir = -1;
    BYTE *mask, *w;

    x = xstart;   y = ystart;
    if ((x < 0) || (x >= xe) || (y < 0) || (y >= ye))
        goto Fin;
    mask = (BYTE *)pmask;   w = (BYTE *)dst;
    ret = 0;   direction = command & 7;
    step = DirToStepCodes[direction];
    while ((step) && (ret < dstsize))
    {
        vicinity = GetVicinity (mask, xe, ye, pitch, x, y);
        i = 8;
        if (command & 8)
        {   // clockwise
            direction = (direction + 3) & 7;
            while ((i--) && ((t = ((1 << direction) & vicinity)) == 0))
                direction = (direction + 7) & 7;
        } else
        {   // counter clockwise
            direction = (direction + 5) & 7;
            while ((i--) && ((t = ((1 << direction) & vicinity)) == 0))
                direction = (direction + 1) & 7;
        }
        if (!t) break;
        if ((x == xstart) && (y == ystart))
        {
         	if (direction == firstdir) break;
            if (firstdir < 0) firstdir = direction;
        }
        step = DirToStepCodes[direction];
        w[ret++] = step;
        x += StepCodesDX[step];   y += StepCodesDY[step];
    }
Fin:
	return ret;
}


/**************************************************** CodeUpdateXY */
/*****
  Updates coordinates
*****/
//  dx = if (code&4) then (code&1)*2-1 else 0
//  dy = if (code&8) then (code&2)-1 else 0
//  invalid codes: 0, 1, 2, 3, 6, 7, 9, B;
//  valid codes:
//  5:  1, 0;  F:  1, 1;  A: 0, 1;  E: -1, 1;
//  4: -1, 0;  C: -1,-1;  8: 0,-1;  D:  1,-1;

void _stdcall CodeUpdateXY(long *X, long *Y, BYTE code)
{
    code &= 0x0F;    // just a case
    *X += StepCodesDX[code];
    *Y += StepCodesDY[code];
}


/*************************************************** CodesToCoords */
/*****
  	Fills pitched arrays <resx>, <resy> with (long) coordinates
    calculated on the base of incremental code contour,
    assuming start point at {xstart,ystart}.

	Statics/Globals:
		DirToStepCodes

	Arguments:
		pcodes - pointer to the buffer of incremental codes
        ncodes - the number of code bytes
        xstart, xstart - coordinates of the start pixel
		resx - pointer to the result buffer for X-coordinates
		pitchx - address difference between neighboring X values
        			in BYTES
		resy - pointer to the result buffer for Y-coordinates
		pitchy - address difference between neighboring Y values
        			in BYTES

    Returns: nothing
***/
void _stdcall CodesToCoords (void *pcodes, long ncodes,
                long xstart, long ystart,
                void *resx, long pitchx,
                void *resy, long pitchy)
{
	long x, y, i;
    BYTE *codes, *bx, *by;

    x = xstart;  y = ystart;
    codes = (BYTE *)pcodes;
    bx = (BYTE *)resx;    by = (BYTE *)resy;
    for (i = 0;  i < ncodes;  i++)
    {	// set coordinates
     	*(long *)bx = x;   *(long *)by = y;
        CodeUpdateXY(&x, &y, codes[i]);
        bx += pitchx;  by += pitchy;
    }
}



/*************************************************** CodeCurvature */
/*****
  	Fills <res> with points of maximal curvature calculated
  	on the base of incremental code contour, assuming start
  	point at {xstart,ystart}.

	Statics/Globals:
		DirToStepCodes

	Arguments:
		pcodes - pointer to the buffer of incremental codes
        ncodes - the number of code bytes
        xstart, xstart - coordinates of the start pixel
		curvature - pointer to the result buffer to fill with
        		(BYTE)curvmea1 values; NULL is OK
		res - pointer to the result buffer
		halfstepmin, halfstepmax - range for half step along the contour
        thresh - threshold to send into <res>

    Reshuffles <ncodes> bytes in <pcodes> circularly so that
    	first <Nlongest> bytes correspond to the longest arc where
        all <curvmea1> values are less than <thresh>.
	Updates the last portion (nres-th) with:
        res[0], res[1] - coordinates of the start pixel after reshuffling
        res[2] - <Nlongest> value
        res[3] - max value for <curvmea1>
        res[4] - number of elements <nmeabig> with (curvmea1 >= thresh);
    Returns if OK:
    	max value for <curvmea1>;
    -1 otherwise
***/
long _stdcall CodeCurvature (void *pcodes, long ncodes,
                long xstart, long ystart,
                void *curvature, long *res,
                long halfstepmin, long halfstepmax, long thresh)
{
	long x, y, i, k, hst, lmea1, lmeamax, nmeabig,
    	 im, km, xm, ym, ip, kp, xp, yp, portion, elemsize,
         n0, nlongest, indlongest, ncurrent, indcurrent,
    	 ret = -1, *ps;
    BYTE *bcurv;
    double dx, dy, chord, xc, yc, dxc, dyc, dist, arc,
    	 NN400, norm, mea1, mea2, meamax;

    portion = 5;  elemsize = portion * sizeof(long);
    ps = (long *)malloc(ncodes * elemsize);
    if (!ps) goto Fin;
    if (curvature) bcurv = (BYTE *)curvature;
	CodesToCoords (pcodes, ncodes, xstart, ystart,
                	ps, elemsize, ps + 1, elemsize);

    NN400 = (600.0 + ncodes * ncodes) / 400;
    n0 = nlongest = indlongest = nmeabig = ncurrent = indcurrent = 0;
    for (i = 0;  i < ncodes;  i++)
    {	// set measures
        k = i * portion;
        x = ps[k]; 		y = ps[k + 1];
        meamax = -1;    lmeamax = 0;
    	for (hst = halfstepmin;  hst <= halfstepmax;  hst++)
    	{	// find best halfstep
		    arc = (2 * hst + 1);
			im = (i + ncodes - hst) % ncodes;
            ip = (i + hst) % ncodes;
        	km = im * portion;   kp = ip * portion;
     		xm = ps[km];   	ym = ps[km + 1];
     		xp = ps[kp];   	yp = ps[kp + 1];
        	xc = (xp + xm) / 2; yc = (yp + ym) / 2;
        	dx = xp - xm;   dy = yp - ym;
        	dxc = x - xc;	dyc = y - yc;
        	chord = sqrt(dx * dx + dy * dy) + 1;
        	dist = sqrt(dxc * dxc + dyc * dyc);
            mea1 = (arc - chord) / chord;
            if (mea1 < 0) mea1 = 0;
        	norm = 10000 / (arc * arc + NN400);
        	mea1 *= norm;
        	mea2 = dist * 100 / chord;
            if (meamax < mea1)
            {
            	meamax = mea1;
            	lmea1 = floor(mea1 + 0.5);
        		ps[k + 2] = lmeamax = lmea1;
        		ps[k + 3] = floor(mea2 + 0.5);
        		ps[k + 4] = hst; // best halfstep
            }
        }
        if (curvature) bcurv[i] = lmeamax < 255 ? lmeamax : 255;
        if (lmeamax > ret) ret = lmeamax;
        if (lmeamax >= thresh)
        {
         	nmeabig++;
            indcurrent = i + 1;		ncurrent = 0;
        } else
        {
            ncurrent++;
    		if (indcurrent == 0) n0 = ncurrent;
    		if (nlongest < ncurrent)
            {
             	indlongest = indcurrent;	nlongest = ncurrent;
            }
        }
    }
	if ((ncurrent) && (indcurrent) &&
    	(indcurrent + ncurrent == ncodes) &&
		(n0 + ncurrent > nlongest))
    {   // last + first pieces
        indlongest = indcurrent;	nlongest = n0 + ncurrent;
    }
    res[0] = ps[portion * indlongest];
    res[1] = ps[portion * indlongest + 1];
    res[2] = nlongest;
    res[3] = ret;
    res[4] = nmeabig;
    if (indlongest)
    {	// Reshuffle
		ReorderBufs (pcodes, indlongest, ncodes - indlongest);
    	if (curvature)
        	ReorderBufs (bcurv, indlongest, ncodes - indlongest);
    }
	free (ps);
Fin:
	return ret;
}


/****************************************************** EquOrder2 */
/*****
  Calculates 2-nd order equation
    EquOrder2(X,Y) = a5*X*X + a4*Y*Y + a3*X*Y + a2*X + a1*Y + a0 = 0;
*****/
double EquOrder2(double *A, double x, double y)
{
   return (A[5]*x + A[2])*x + (A[4]*y + A[1])*y + A[3]*x*y + A[0];
}


/************************************************** CodeEquOrder2 */
/*****
  Fills <dst> with incremental code bytes for the curve given by
  equation
  		F(X,Y) = a5*X*X + a4*Y*Y + a3*X*Y + a2*X + a1*Y + a0 = 0;
  begining from point {Xstart, Ystart};
  finish in the same point or by <dstsize> limitation.
  The caller responsibility is to guarantee that the value
  	F(Xstart, Ystart) is small enough.
  Returns the number of written bytes.
*****/
long _stdcall CodeEquation2(void *dst, long dstsize,
			double *A, long xstart, long ystart)
{
    long direction, step, x, y, i, st, prevstep, prevdir,
    	 begdir, dircnt, dirmin, wrk, ret;
    BYTE *w;
    double fmin, fcur, scur, xx, yy;

    x = xstart;   y = ystart;
    w = (BYTE *)dst;
    ret = 0;   begdir = 0;  dircnt = 8;
    prevstep = 0x0c;
    while (ret < dstsize)
    {
        fmin = 1e+16;
        for (i = 0;  i < dircnt;  i++)
        {   //
         	direction = (begdir + i) & 7;
	        st = DirToStepCodes[direction];
            xx = (double)(x + StepCodesDX[st]);
            yy = (double)(y + StepCodesDY[st]);
            scur = EquOrder2(A, xx, yy);
            fcur = fabs(scur);
            if (fmin > fcur)
            {
             	fmin = fcur;
                dirmin = direction;
                step = st;
            }
        }
        if ((step & prevstep) == 0)
        { // +/- pi/2
            step |= prevstep;
            dirmin = StepCodesToDirection[step];
        }
        if (!ret) prevdir = dirmin;
        w[ret++] = step;
        x += StepCodesDX[step];
        y += StepCodesDY[step];
        if ((x == xstart) && (y == ystart)) break;
//        begdir = (dirmin + 6) & 7;  dircnt = 5;
        begdir = (dirmin + 7) & 7;  dircnt = 3;
        wrk = (dirmin - prevdir) & 7;
        if (wrk)
        {
         	if (wrk & 4) begdir = (begdir + 7) & 7;
            dircnt = 4;
        }
        prevstep = step;  prevdir = dirmin;
    }
//Fin:
    return ret;
}


/***************************************************** CodeEllipse */
/*****
  	Fills <dst> with incremental code bytes for ellipse:
            (x/a)^2 + (y/b)^2 = 1
    rotated by a given angle.

	Arguments:
		dst - pointer to target memory buffer
        dstsize - its size in bytes
        a - half of the big axis
        b - half of the small axis
		angle - angle between the big axis and X-axis (in radians)
		xstart, ystart - pointers to get coordinates of the start
                point (one of the big axis ends)
  	Returns the number of written bytes.
*****/
long _stdcall CodeEllipse(void *dst, long dstsize,
                double a, double b, double angle,
                long *xstart, long *ystart)
{
    double c, s, aa, bb, F[6];

    c = cos(angle);     s = sin(angle);
    aa = 1.0 / (a * a); bb =  1.0 / (b * b);
    F[5] = c * c * aa + s * s * bb;
    F[4] = s * s * aa + c * c * bb;
    F[3] = 2.0 * c * s * (aa - bb);
    F[2] = 0.0;
    F[1] = 0.0;
    F[0] = -1.0;
    *xstart = (long)floor(a * c + 0.5);
    *ystart = (long)floor(a * s + 0.5);
	return CodeEquation2(dst, dstsize, F, *xstart, *ystart);
}


/******************************************************** CodeLine */
/*****
  Fills <dst> with incremental code bytes for straight line
  from point {0,0} (not including) to {dx,dy}.
  Returns the number of written bytes.
*****/
//  dx = if (code&4) then (code&1)*2-1 else 0
//  dy = if (code&8) then (code&2)-1 else 0
//  invalid codes: 0, 1, 2, 3, 6, 7, 9, B;
//  valid codes:
//  5:  1, 0;  F:  1, 1;  A: 0, 1;  E: -1, 1;
//  4: -1, 0;  C: -1,-1;  8: 0,-1;  D:  1,-1;

long _stdcall CodeLine(void *dst, long dstsize, long deltax, long deltay)
{
	long dx, dy, dist, i, k, ddist;
    BYTE cx, cxy, *targ;

    if (deltax)
   	{
   	    if (deltax < 0) { cx = StepCodeM0;  dx = -deltax; }
   	    else { cx = StepCodeP0;  dx = deltax; }
   	} else { cx = StepCode00;  dx = 0; }
   	if (deltay)
   	{
   	    if (deltay < 0) { cxy = StepCode0M;  dy = -deltay; }
   	    else { cxy = StepCode0P;  dy = deltay; }
   	} else { cxy = StepCode00;  dy = 0; }
   	cxy ^= cx;
   	if ((i = (dy - dx)) > 0) {	cx ^= cxy;  dx += i;  dy -= i; }
   	targ = (BYTE *)dst;
    dist = 0;  ddist = dx - dy * 2;  k = dx;
    if (k > dstsize) k = dstsize;
   	for (i = 0;  i < k;  i++)
   	{
   	    if ((dist += ddist) > 0)
        {
       	    targ[i] = cx;   dist -= dx;
        } else
        {
       	    targ[i] = cxy;  dist += dx;
        }
   	}
   	return k;
}


/*************************************************** CodeSplineSeg */
/*****
  	Fills <dst> with incremental code bytes for a Spline Segment
  	given by parametric coefficients;
	Arguments:
		dst - pointer to target memory buffer
        dstsize - its size in bytes
        finalx, finaly - pointers to updating final coordinates
                (assume start point at {0,0})
		xa1, xa2, xa3 - coeffs for x;
		ya1, ya2, ya3 - coeffs for y;
  	Updates dx, dy coordinates.
  	Returns the number of written bytes.
*****/
//  dx = if (code&4) then (code&1)*2-1 else 0
//  dy = if (code&8) then (code&2)-1 else 0
//  invalid codes: 0, 1, 2, 3, 6, 7, 9, B;
//  valid codes:
//  5:  1, 0;  F:  1, 1;  A: 0, 1;  E: -1, 1;
//  4: -1, 0;  C: -1,-1;  8: 0,-1;  D:  1,-1;

long _stdcall CodeSplineSeg(void *dst, long dstsize,
                long *finalx, long *finaly,
                double xa1, double xa2, double xa3,
                double ya1, double ya2, double ya3)
{
    double countd, t;
	long count, oldx, oldy, s, i,  ix, iy;
	BYTE c, *targ;

    targ = (BYTE *)dst;
    countd = floor(fabs(xa1) + fabs(xa2) + fabs(xa3) + 2);
    t = floor(fabs(ya1) + fabs(ya2) + fabs(ya3) + 2);
    if (countd < t) countd = t;
    count = (long)countd;
    s = oldx = oldy = 0;
    for (i = 0;  i <= count;  i++)
    {
        t = (double)i / countd;
        ix = (long)floor(((xa3 * t + xa2) * t + xa1) * t + 0.5);
        iy = (long)floor(((ya3 * t + ya2) * t + ya1) * t + 0.5);
TryCode:
        c = 0;
        if (ix < oldx) 		{ oldx--;  c = StepCodeM0; }	// -1,0
        else if (ix > oldx) { oldx++;  c = StepCodeP0; }	// +1,0
        if (iy < oldy) 		{ oldy--;  c |= StepCode0M; }	// ?,-1
        else if (iy > oldy) { oldy++;  c |= StepCode0P; }	// ?,+1
        if (c)
        {
            if (s >= dstsize) break;
        	targ[s++] = c;
            if ((ix != oldx) || (iy != oldy)) goto TryCode;
        }
	}
    *finalx = oldx;  *finaly = oldy;
	return s;
}


/***************************************************** CodesLength */
/*****
  	Calculates length of the curve given by incremental code;

	Statics/Globals:

	Arguments:
		pcodes - pointer to the buffer of incremental codes
        ncodes - the number of code bytes

    Returns: length of the curve
***/
long _stdcall CodesLength (void *pcodes, long ncodes)
{
	long i, len, s1 , s2;
    BYTE b, *codes;

    codes = (BYTE *)pcodes;  s1 = s2 = 0;
    for (i = 0;  i < ncodes;  i++)
    {
        b = codes[i] & 0x0c;
        if (b)
        {
        	if (b == 0x0c) s2++;
            else s1++;
        }
    }
    len = floor(sqrt(2.0) * s2 + s1 + 0.5) ;
    return len;
}


/********************************************************* DisplayMem */
/***
	Function:	DisplayMem

	Description:
		This function displays the rectangular piece of memory treated
		as pixel's map, at given location of a given window, using
		current LUT. If there is no LUT then the "Linear" one is
		to set by default.

	Statics/Globals:
		LDI (LastBMPinfo.diInfo)

	Input Arguments:
		windhndl - Handle of the window where to display the rectangle

		xl - X-coordinate in the window to display left side
			of the rectangle

		yt - Y-coordinate in the window to display top side
			of the rectangle

		prect - pointer to the rectangular piece of memory

		bitspix - rectangle's pixel size (bits)

		width - number of pixels in each row of the rectangle

		height - number of rows (scanlines) in the rectangle

		flip - 0: first scan line in memory goes to the top of
				screen picture, otherwise it goes to the bottom.

	Output Arguments:
		None

	Returns:
		number of displayed scanlines if OK, 0 otherwise
***/
long _stdcall DisplayMem (
		long windhndl, 		// window where to display
		long xl, long yt,   // left top corner coordinates
		char *prect,		// pointer to rectangular piece of memory
		long bitspix, long width, long height, 	// image description
		long flip)			// to overturn the image (1) or not (0)
{
	HWND hwnd;
	HDC hdc;
	long scanlines, wbytes;

	if (! LastLUTform)
		LookUpTable (NULL, "Linear");  // if no LUT - set default
	wbytes = (bitspix * width + 7) >> 3;
	FillLDI (bitspix, width, height);

	hwnd = (HWND)windhndl;
	hdc = GetDC (hwnd);
	if (!flip) ReverseRows (prect, NULL, wbytes, height);
	scanlines = SetDIBitsToDevice (hdc /*HDC*/,
				(int)xl /*XDest*/, (int)yt /*YDest*/,
				(DWORD)width /*cx*/, (DWORD)height /*cy*/,
				0 /*XSrc*/, 0 /*YSrc*/, 0 /*uStartScan*/,
				(UINT)height /*cScanLines*/,
				prect /*lpvBits*/,
				(BITMAPINFO *)&LastBMPinfo /*lpbmi*/,
				DIB_RGB_COLORS /*fuColorUse*/);
	if (!flip) ReverseRows (prect, NULL, wbytes, height);
	ReleaseDC (hwnd, hdc);
	return scanlines;
}


/*end*/
//}
