/*
 *		D D G S I M A G E F O R M A T
 *			-- transform image to screen coordinates and vice versa
 *			-- to handle all camera input and display formats 
 *
 */

#include <ddgs.h>

//Image rectangles (for changes to accommodate all format cameras)
typedef struct image_rect {
			double x1;
			double y1;
			double x2;
			double y2;
			}ImageRect;


void translateimage (double XTranslation, double YTranslation, ImageRect *irect)
{
	irect->x1 += XTranslation;
	irect->y1 += YTranslation;
	irect->x2 += XTranslation;
	irect->y2 += YTranslation;
}

void magnifyimage(double Magnification, ImageRect *irect)
{

	irect->x1 *= Magnification;
	irect->y1 *= Magnification;
	irect->x2 *= Magnification;
	irect->y2 *= Magnification;
}


void viewtodisplay(double *x,double *y)
{
	DDGS *dg;
	ImageRect rect1;
	double xoffset,yoffset;
	double ScaleToFitDisplay;

	dg = getcurdg();
	if (dg == NULL)
		return;

		//are image values set ?
	if (dg->ddgsImageFormat.MaxX <= 0)
		return;

			//display dimensions
	rect1.x1 = dg->ddgsImageFormat.XOffset;
	rect1.y1 = dg->ddgsImageFormat.YOffset; 
	rect1.x2 = rect1.x1 + dg->ddgsImageFormat.Width;
	rect1.y2 = rect1.y1 + dg->ddgsImageFormat.Height;

		//set scale to fit image to display
	ScaleToFitDisplay = (double)dg->maxx / (double)dg->ddgsImageFormat.MaxX;

			//offset of point in the display  
	xoffset = *x / dg->ddgsImageFormat.Width;
	yoffset = *y / dg->ddgsImageFormat.Height;

	rect1.x1 = xoffset * dg->ddgsImageFormat.MaxX;
	rect1.y1 = yoffset * dg->ddgsImageFormat.MaxY;

	*x = rect1.x1;
	*y = rect1.y1;

	*y = (double)dg->ddgsImageFormat.MaxY - *y;
}

/*
 *	T O _ C O H U _ S C A L E 
 *		-- get scale from cohu to current image size
 */
double to_cohu_scale ()
{
	double scale_cohu;
	DDGS *dg;

	dg = getcurdg();

	if (dg == NULL)
		return 1.0;

	if (dg->ddgsImageFormat.MaxX <= 0 || dg->ddgsImageFormat.XCohu == 1)
		return 1.0;		
	
	scale_cohu= (double)dg->ddgsImageFormat.MaxX / 768.0;

	return scale_cohu;
}

/*
 *	I M A G E S C A L E S
 *		-- get scales for image to screen conversion
 */
void imagescales(double *xscale, double *yscale)
{
	DDGS *dg;
	dg = getcurdg();

	*xscale = *yscale = 1.0;

	if (dg == NULL)
		return ;

		//are image values set ?
	if (dg->ddgsImageFormat.MaxX <= 0)
		return;

	*xscale = (double) dg->ddgsImageFormat.Magnification / ((double)(dg->maxx +1) / (double)dg->ddgsImageFormat.MaxX);
	*yscale = (double) dg->ddgsImageFormat.Magnification /((double)(dg->maxy + 1) / (double)dg->ddgsImageFormat.MaxY);

	if (*xscale < 1.0)
		*xscale = 1.0;
	if (*yscale < 1.0)
		*yscale = 1.0;

}

/*
 *	S C R E E N T O I M A G E
 *		-- transform screen coordinates to image coordinates
 */
void screentoimage(double *x, double *y)
{
	DDGS *dg;
	double x1,y1,x2,y2;

	dg = getcurdg();
	if (dg == NULL)
		return;

		//are image values set ?
	if (dg->ddgsImageFormat.MaxX <= 0)
		return;		

	x1 = dg->ddgsImageFormat.XOffset;
	y1 = dg->ddgsImageFormat.YOffset; 

	x2 = (double) *x;
	y2 = (double) *y;

	x2 = x2 - x1;
	y2 = y2 - y1;

	*x = x2;
	*y = y2;

	viewtodisplay(x,y);
}

/*
 *	I M A G E T O S C R E E N
 *		-- transform image coordinates to screen coordinates
 *		-- the input parameter values are true image coordinates
 *		-- the output parameter values are the screen coorindinates
 */
BOOL imagetoscreen(int *x,int *y, int *w, int *h, double *scale, double *scale_cohu, BOOL invert_y)
{
	DDGS *dg;
	double xoffset,yoffset;
	double ww,hh;
	double ScaleToFitDisplay;
	ImageRect ImagePt;
	ImageRect ImageFull;
	ImageRect Display;
	double deltaX,deltaY;

	dg = getcurdg();

	if (dg == NULL)
		return FALSE;

		//are image values set ?
	if (dg->ddgsImageFormat.MaxX <= 0)
	{
		if (invert_y == TRUE) 
			*y = dg->maxy - *y;	//flip-upside for some operations!
		*scale = 1.0;
		return FALSE;
	}
			//offset into image
	ImagePt.x1 = dg->ddgsImageFormat.XMousePt;
	ImagePt.y1 = (double) dg->ddgsImageFormat.MaxY - dg->ddgsImageFormat.YMousePt;
	ImagePt.x2 = 0.0;
	ImagePt.y2 = 0.0;

			//image dimensions
	ImageFull.x1 = 0.0;
	ImageFull.y1 = 0.0;
	ImageFull.x2 = dg->ddgsImageFormat.MaxX;
	ImageFull.y2 = dg->ddgsImageFormat.MaxY;

			//set scale to fit image to display to which dimension fits best
	if (dg->maxx - dg->ddgsImageFormat.MaxX < dg->maxy - dg->ddgsImageFormat.MaxY)
		ScaleToFitDisplay = (((double)dg->maxx +1)/ (double)dg->ddgsImageFormat.MaxX);
	else
		ScaleToFitDisplay = (((double)dg->maxy +1 ) / (double)dg->ddgsImageFormat.MaxY);


//	if (dg->ddgsImageFormat.XCohu == 1)
		*scale_cohu = 1.0;
//	else
//		*scale_cohu = ((double)dg->ddgsImageFormat.MaxX / 768.0) * ( ( (double)dg->maxx / (double)dg->ddgsImageFormat.MaxX) / ScaleToFitDisplay) ;
	//This strange scale calculation (one line above) was to accomodate numbers in extended/short karyotypes


	*scale = ScaleToFitDisplay * dg->ddgsImageFormat.Magnification;

	Display.x1 = (double) *x;
	Display.y1 = (double) dg->ddgsImageFormat.MaxY - (double) *y; // images are upside down!
	Display.x2 = Display.x1 + (double) *w;
	Display.y2 = Display.y1 + (double) *h;

	ww = (Display.x2 - Display.x1) / 2.0;
	hh = (Display.y2 - Display.y1) / 2.0;

			//offset of object image into image screen 
	xoffset = Display.x1 / (double) dg->ddgsImageFormat.MaxX;
	yoffset = Display.y1 / (double) dg->ddgsImageFormat.MaxY;

			//fit image to display screen
	magnifyimage (ScaleToFitDisplay, &Display);
	magnifyimage (ScaleToFitDisplay, &ImagePt);
	magnifyimage (ScaleToFitDisplay, &ImageFull);

			//difference in size between display and image
	deltaX = (double) (dg->maxx +1) - (ImageFull.x2 - ImageFull.x1);
	deltaY = (double) (dg->maxy +1) - (ImageFull.y2 - ImageFull.y1);
	translateimage (deltaX/2.0,deltaY/2.0,&Display);
	translateimage (deltaX/2.0,deltaY/2.0,&ImagePt);
	translateimage (deltaX/2.0,deltaY/2.0,&ImageFull);

			//magnify image
	magnifyimage (dg->ddgsImageFormat.Magnification,&Display);
	magnifyimage (dg->ddgsImageFormat.Magnification,&ImagePt);
	magnifyimage (dg->ddgsImageFormat.Magnification,&ImageFull);

	if (dg->ddgsImageFormat.Magnification > 1) 
	{
		translateimage (-ImagePt.x1,-ImagePt.y1,&Display);
		translateimage (-ImagePt.x1,-ImagePt.y1,&ImageFull);
		ww = (double) dg->maxx / 2.0;
		hh = (double) dg->maxy / 2.0;
		translateimage (ww,hh,&Display);
		translateimage (ww,hh,&ImageFull);
	}


		//display screen coordinates	
	*x = (int) (Display.x1 + 0.5);
	*y = (int) (Display.y1 + 0.5);
	*w = (int) (Display.x2 - Display.x1 + 0.5);
	*h = (int) (Display.y2 - Display.y1 + 0.5);

	if (invert_y == TRUE) 
		*y = dg->maxy - *y;	//flip-upside for some operations!

		//display image dimensions
	dg->ddgsImageFormat.XOffset = ImageFull.x1;
	dg->ddgsImageFormat.YOffset = ImageFull.y1;
	dg->ddgsImageFormat.Width = ImageFull.x2 - ImageFull.x1;
	dg->ddgsImageFormat.Height = ImageFull.y2 - ImageFull.y1;

	return TRUE;

}

/*
 *	I M A G E T O S C R E E N _ P T _ S C A L E 
 *		-- transform image coordinates to screen coordinates
 *		-- the input parameter values are true image coordinates
 *		-- the output parameter values are the screen coorindinates
 */
BOOL imagetoscreen_pt_scale(int *x,int *y, double *scale, double *scale_cohu, BOOL invert_y)
{
	int x1 = 0, y1 = 0;

	return	imagetoscreen (x,y,&x1,&y1,scale,scale_cohu,invert_y);

}


/*
 *	I M A G E T O S C R E E N _ P T 
 *		-- transform image coordinates to screen coordinates
 *		-- the input parameter values are true image coordinates
 *		-- the output parameter values are the screen coorindinates
 */
BOOL imagetoscreen_pt(int *x,int *y, BOOL invert_y)
{
	double ss,sc;

	return imagetoscreen_pt_scale (x,y,&ss,&sc,invert_y);
}

