/*
 *		D D G S O V L Y . C  --  DDGS overlay plane stuff
 *
 *  Written: 	Mark Gregson
 *		Applied Imaging
 *
 *		There is only one real overlay plane, bit 7 of the display.
 *		This plane can be cleared, produce line drawings, boxes,
 *		filled areas and text. The overlay plane can also be
 *		switched on or off and be set to any rgb colour.
 *		Overlay routines work the same as normal image routines
 *		except they only effect plane 7 of the display.
 *		All overlay routines are preceded by the letter 'o'
 *		e.g. oclear(), olineto(), ofill() etc
 *		The normal intens() routine is used to select draw/erase
 *
 *		There are 16 destructive overlays or markers.
 *		These can be set to any colour and produce line drawings etc.
 *		However they work on bit planes 0-6 and therefore destroy
 *		grey image data. They appear different colours to grey image
 *		values because they clear plane 6 ( grey image values set plane 6 ) 
 *		All marker routines are preceded by the letter 'm'
 *		e.g.  mlineto() , mfill(), mbox() etc
 *		The normal intens() routine is used to select draw/erase
 *		- also clear() will erase markers from grey images.
 *
 *  Date:    	5th August 1992
 *
 *  Modifications
 *
 *	09Mar2000	JMB	Converted to .cpp.
 *	07Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *	24Apr97 WH:		Afraid we do so, clipping is back by popular demand
 *	20Mar97	BP:		No clip region.... no need?
 *	11Jun96	WH:		modify loadmgc to handle pseudocolour.
 *	BP	12/10/94:	Various changes to accomodate 24-bit 'overlays'.
 *	4 Mar 1993	MG	modified loadmgc to handle fluorescent (inverted) images
 *     19 Jan 1993	MG	VGA cannot XStoreColors 
 *	2 Sep 1992	MG	added destructive markers
 *	
 */

#include "ddgs.h"
#include "ddgsdefs.h"


/***************** Worker functions ****************/

/*
 * These functions do the business in
 * whatever DC is given them. There are
 * public versions of these functions in
 * this module that work in MARKIM,
 * OVLYIM and GREYIM.
 * All are in ddgsline.c except _text()
 * which is in ddgstext.c
 */
extern void _lineto(HDC dc, int x, int y);
extern void _lineby(HDC dc, int x, int y);
extern void _drawpoly(HDC dc, DDGSPoint *xpt, int npts);
extern void _drawfillpoly(HDC dc, DDGSPoint *xpt, int npts);
extern void _drawcircle(HDC dc, int x, int y, int r);
extern void _box(HDC dc, int x, int y);
extern void _fill(HDC dc, int x, int y, int width, int height);
extern void _text(HDC dc, int x, int y, LPCTSTR s, short scale);


/***************** OVLYIM FUNCTIONS ****************/


/*
 * ddgsoverlay
 *
 * Switch overlay plane ON/OFF
 */
void
ddgsoverlay(int state)
{
	if (curdg == NULL)
		return;

//	fprintf(stderr, "ddgs: overlay switching not yet implemented.\n");
}


/*
 * osetrgb
 *
 * Set colour lookup for overlay plane
 */
void
osetrgb(int r, int g, int b)
{
	if (curdg == NULL)
		return;

	DeleteObject(curdg->ovlypen);
	curdg->ovlycol = RGB(r, g, b);
	curdg->ovlypen = CreatePen(PS_SOLID|PS_ENDCAP_FLAT | PS_JOIN_MITER, curdg->cur_lwidth,
		DOG_OVLYMASK(curdg));
}
 

/*
 * oclear
 *
 * Can't have this, because it can't work
 * on a 24-bit display!
 */
void
oclear()
{
	fprintf(stderr, "ddgs: oclear() obsolete.\n");
}


/*
 * loadodc
 *
 * Get dc, and setup for overlay drawing. Derived
 * from loadogc.
 * The DC must be released by the caller!
 */
static HDC loadodc()
{
	HDC dc;

	if (curdg == NULL)
		return NULL;

	dc = DOG_DC(curdg);
	SetROP2(dc, R2_XORPEN);
	SelectObject(dc, curdg->ovlypen);
	SetTextColor(dc, curdg->ovlycol);
	if (curdg->fillbrush)
		SelectObject(dc, curdg->fillbrush);

	SelectClipRgn(dc, curdg->cliprgn);

	return dc;
}


/*
 * olineto
 */
void
olineto(int x, int y)
{
	HDC dc;

	if (dc = loadodc())
	{
		_lineto(dc, x, y);
		DOG_RDC(curdg);
	}
}


/*
 * odrawpoly
 */
void
odrawpoly(DDGSPoint *xpt, int npts, int fill)
{
	HDC dc;

	if (dc = loadodc())
	{
		if (!fill)
			_drawpoly(dc, xpt, npts);
		else
			_drawfillpoly(dc, xpt, npts);
		DOG_RDC(curdg);
	}
}


/*
 * odrawcircle
 */
void
odrawcircle(int x, int y, int r)
{
	HDC dc;

	if (dc = loadodc())
	{
		_drawcircle(dc, x, y, r);
		DOG_RDC(curdg);
	}
}


/*
 * olineby
 */
void
olineby(int x, int y)
{
	HDC dc;

	if (dc = loadodc())
	{
		_lineby(dc, x, y);
		DOG_RDC(curdg);
	}
}


/*
 * obox
 *
 */
void
obox(int x, int y)
{
	HDC dc;

	if (dc = loadodc())
	{
		_box(dc, x, y);
		DOG_RDC(curdg);
	}
}


/*
 * ofill
 */
void
ofill(int x, int y, int width, int height)
{
	HDC dc;

	if (dc = loadodc())
	{
		_fill(dc, x, y, width, height);
		DOG_RDC(curdg);
	}
}


/*
 * otext
 */
void
otext(int x, int y, LPCTSTR s, short scale)
{
	HDC dc;

	if (dc = loadodc())
	{
		_text(dc, x, y, s, scale);
		DOG_RDC(curdg);
	}
}


/***************** MARKIM FUNCTIONS ****************/


/*
 * mcolour
 *
 * Set the current marker colour.
 */
void
mcolour(int marker)
{
	if (curdg == NULL)
		return;

	if ((marker < 0) || (marker >= MAXMARKER))
		marker = 0;

	curdg->cur_marker = marker;
}


/*
 * msetrgb
 *
 * Set the RGB values for a marker.
 */
void
msetrgb(int marker, int r, int g, int b)
{
//	fprintf(stderr, "ddgs: msetrgb() not yet implemented.\n");
}


/*
 * load_markers
 * ???? They're loaded already!
 */
void
load_markers(DDGS *dg)
{
	fprintf(stderr, "ddgs: load_markers() redundant.\n");
}


/*
 * loadmdc
 *
 * Get dc, and setup for marker drawing. Derived
 * from loadmgc.
 * The DC must be released by the caller!
 */
static HDC loadmdc()
{
	HDC dc;

	if (curdg == NULL)
		return NULL;

	dc = DOG_DC(curdg);
	SetROP2(dc, R2_COPYPEN);
	if (curdg->cur_intensity)
	{
		SelectObject(dc, curdg->markpen[curdg->cur_marker]);
		if (curdg->fillbrush)
			SelectObject(dc, curdg->fillbrush);
		SelectObject(dc, curdg->markpen[curdg->cur_marker]);
		SetTextColor(dc, curdg->markcol[curdg->cur_marker]);
	}
	else
	{
		if (curdg->invert)
		{
			SelectObject(dc, curdg->blackpen);
			SetTextColor(dc, RGB(0, 0, 0));
		}
		else
		{
			SelectObject(dc, curdg->whitepen);
			SetTextColor(dc, RGB(255, 255, 255));
		}
	}

	SelectClipRgn(dc, curdg->cliprgn);

	return dc;
}


/*
 * mlineto
 *
 */
void
mlineto(int x, int y)
{
	HDC dc;

	if (dc = loadmdc())
	{
		_lineto(dc, x, y);
		DOG_RDC(curdg);
	}
}


/*
 * mdrawpoly
 */
void
mdrawpoly(DDGSPoint *xpt, int npts, int fill)
{
	HDC dc;

	if (dc = loadmdc())
	{
		if (!fill)
			_drawpoly(dc, xpt, npts);
		else
			_drawfillpoly(dc, xpt, npts);
		DOG_RDC(curdg);
	}
}


/*
 * mdrawcircle
 */
void
mdrawcircle(int x, int y, int r)
{
	HDC dc;

	if (dc = loadmdc())
	{
		_drawcircle(dc, x, y, r);
		DOG_RDC(curdg);
	}
}


/*
 * mlineby
 */
void
mlineby(int x, int y)
{
	HDC dc;

	if (dc = loadmdc())
	{
		_lineby(dc, x, y);
		DOG_RDC(curdg);
	}
}


/*
 * mbox
 */
void
mbox(int x, int y)
{
	HDC dc;

	if (dc = loadmdc())
	{
		_box(dc, x, y);
		DOG_RDC(curdg);
	}
}


/*
 * mfill
 */
void
mfill(int x, int y, int width, int height)
{
	HDC dc;

	if (dc = loadmdc())
	{
		_fill(dc, x, y, width, height);
		DOG_RDC(curdg);
	}
}


/*
 * mtext
 */
void
mtext(int x, int y, LPCTSTR s, short scale)
{
	HDC dc;

	if (dc = loadmdc())
	{
		_text(dc, x, y, s, scale);
		DOG_RDC(curdg);
	}
}


/***************** GREYIM FUNCTIONS ****************/


/*
 * loadgdc
 *
 * Get dc, and setup for greyscale drawing. Derived
 * from loadodc.
 * The DC must be released by the caller!
 * Note that a custom pen is created here (unlike
 * OVLY and MARK whose pens are pre-created) that
 * matches the cur_colour. This must be deleted at
 * the same time that the DC is released.
 */
static HDC loadgdc(HPEN *pen)
{
	HDC dc;

	if (curdg == NULL)
		return NULL;

	dc = DOG_DC(curdg);
	SetROP2(dc, R2_COPYPEN);
	if (curdg->cur_intensity)
	{
		COLORREF rgb = PALETTERGB(curdg->cur_colour, curdg->cur_colour, curdg->cur_colour);
		*pen = CreatePen(PS_SOLID|PS_ENDCAP_FLAT | PS_JOIN_MITER, curdg->cur_lwidth, rgb);
		SelectObject(dc, *pen);
		SetTextColor(dc, rgb);
	}
	else
	{
		*pen = NULL;
		if (curdg->invert)
		{
			SelectObject(dc, curdg->blackpen);
			SetTextColor(dc, RGB(0, 0, 0));
		}
		else
		{
			SelectObject(dc, curdg->whitepen);
			SetTextColor(dc, RGB(255, 255, 255));
		}
	}

	SelectClipRgn(dc, curdg->cliprgn);

	return dc;
}


/*
 * The following functions should all be gXXXXX
 * (eg gbox(), gdrawpoly() etc) really....
 */


/*
 * lineto
 */
void
lineto(int x, int y)
{
	HDC dc;
	HPEN pen;

	if (dc = loadgdc(&pen))
	{
		_lineto(dc, x, y);
		DOG_RDC(curdg);
		if (pen)
			DeleteObject(pen);
	}
}


/*
 * drawpoly
 */
void
drawpoly(DDGSPoint *xpt, int npts)
{
	HDC dc;
	HPEN pen;

	if (dc = loadgdc(&pen))
	{
		_drawpoly(dc, xpt, npts);
		DOG_RDC(curdg);
		if (pen)
			DeleteObject(pen);
	}
}


/*
 * drawcircle
 */
void
drawcircle(int x, int y, int r)
{
	HDC dc;
	HPEN pen;

	if (dc = loadgdc(&pen))
	{
		_drawcircle(dc, x, y, r);
		DOG_RDC(curdg);
		if (pen)
			DeleteObject(pen);
	}
}


/*
 * lineby
 */
void
lineby(int x, int y)
{
	HDC dc;
	HPEN pen;

	if (dc = loadgdc(&pen))
	{
		_lineby(dc, x, y);
		DOG_RDC(curdg);
		if (pen)
			DeleteObject(pen);
	}
}


/*
 * box
 */
void
box(int x, int y)
{
	HDC dc;
	HPEN pen;

	if (dc = loadgdc(&pen))
	{
		_box(dc, x, y);
		DOG_RDC(curdg);
		if (pen)
			DeleteObject(pen);
	}
}


/*
 * fill
 */
void
fill(int x, int y, int width, int height)
{
	HDC dc;
	HPEN pen;

	if (dc = loadgdc(&pen))
	{
		_fill(dc, x, y, width, height);
		DOG_RDC(curdg);
		if (pen)
			DeleteObject(pen);
	}
}


/*
 * text
 */
void
text(int x, int y, LPCTSTR s, short scale)
{
	HDC dc;
	HPEN pen;

	if (dc = loadgdc(&pen))
	{
		_text(dc, x, y, s, scale);
		DOG_RDC(curdg);
		if (pen)
			DeleteObject(pen);
	}
}
