/*
 * 	ddgspix.c	: Mark Gregson 11/8/92
 *
 *	X-Windows pixel drawing routines
 *
 * Mods:
 *
 *	16May2003	JMB	Renamed new_fastpixobj() to fastpixobj() and
 *					added MakeDisplayMask() so that the new fastpixobj() can
 *					be used without a modified version of the Woolz library.
 *	11May2003	JMB	Added new_fastpixobj(). Modified ddgs_get_draw_mask() to
 *					allow extra memory for scanline padding if required.
 *	09Mar2000	JMB	Converted to .cpp; added casts to return values of
 *					ddgs_get_draw_buffer(), ddgs_get_aux_buffer() and
 *					ddgs_get_draw_mask() because of compiler errors.
 *	07Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *	26Jul99	MG		Add ddgs NULL checking at start of a number of routines 
 *	18Jan99	KS:		Masking in fastpixobj24, just changed
 *					this so that the masking happens in memory - looks nicer on screen!
 *	11Mar98	WH:		Added masking in fastpixobj24. Changed allocation in
 *					ddgs_get_draw_mask().
 *	24Apr97 WH:		Afraid we do so, clipping is back by popular demand
 *	20Mar97	BP:		No clip region.... no need?
 *	18Mar97	BP:		Tidy masked/boolean drawing using DDGSMASKED.
 *					Also implemented MARKIM draw properly using
 *					binfo etc (just like GREYIM).
 *					Also implemented OVLYIM draw properly using
 *					binfo XOR (but this should never be used!)
 *					Mask BITMAPINFO is now a member of DDGS (minfo).
 *	20Jan97	BP:		Added fastpixobj24().
 *	10Jan97	BP:		Also free font resources in free_draw_buffers()
 *	BP	12/24/96:	Support buffered or un-buffered display through
 *					DDGSBUFFERED define.
 *	BP	9/24/96:	Windows version.
 *
 *	BP	6/20/96:	Remove the SHM alloc error message! This will cause
 *					actual problems only with capture - which will generate
 *					its own messages!
 *	WH 10jun96		Remove the RMID stuff (see previous comment) as it seems to
 *					to prevent the first time display of insensitive buttons.
 *	BP	5/3/96:		OK - put RMID stuff in (see previous comment!), with XSync.
 *					Looks like this does the trick...
 *	BP	4/1/96:		Patch in (but leave commented-out) call to stop
 *					shmid hanging around after crash.
 *	BP	8/24/95:	Allow for NULL dog window (to draw in pixmap only).
 *	BP	6/28/95:	Got XShmPutImage() stuff working. This can be
 *					included/excluded at compile time through SHM_ENABLED
 *					Note that a regular buffer will be used if there is
 *					an error getting the SHM buffer (for example, when
 *					running on a remote display). Only "draw_buffer" is
 *					included in the SHM system.
 *	BP	5/25/95:	Added ddgs_free_buffers() to free off buffer
 *					memory. Also added (but commented out) stuff
 *					for XShmPutImage(). Commented out because it
 *					doesn't work properly. Note that the free_buffers
 *					this is REQUIRED for shared memory stuff else
 *					the system runs out of resources.
 *					Also added static grey-2-ddgs LUT's for faster
 *					drawing (no more shift-then-OR on each pixel).
 *	BP	5/11/95:	fastpixobj() is now faster in 24-bit mode.
 *	BP	12/10/94:	Allow for different image depths.
 *	BP	9/20/94:	New drawing buffer allocation scheme to accomodate
 *					8- and 24-bit drawing.
 *	 5 Mar 1993	XSetFunction stuff for inverted drawing
 */
 
#include "ddgs.h"
#include "ddgsdefs.h"

#include <memory.h>
//#include "wstruct.h"
#include <gdiplus.h>
using namespace Gdiplus;

#define GREYIM	0
#define OVLYIM	1
#define MARKIM	2


/*
 * Define minimum buffer sizes so that
 * a big enough buffer to do most things
 * will be allocated right away (first
 * time through). This is an attampt to
 * prevent a lot of realloc calls, which
 * result in serious fragmentation.
 */
#define	MINBUFFERSIZE	1048576
#define	MINMASKSIZE		131072

ULONG_PTR gdiplusToken = NULL; //pointer to recieve GDIplus token



/*
 * BP - Drawing buffers.
 *
 * This section of code is used to manage buffers
 * used in drawing operations for grey/probe
 * objects. Higher level functions should call
 * these functions to obtain a buffer for use
 * with all such drawing operations. The same
 * buffers can be used for probes and chroms to
 * minimise memory usage.
 *
 * Memory is allocated based on the visual mode
 * of the server. If we are running 24-bits,
 * buffers of 4x the required size are allocated.
 *
 * Note that there are actually only 3 buffers
 * used. These are never freed. realloc() is used
 * if an object larger than any other already
 * drawn is requested. This method prevents
 * memory fragmentation, and will be fast (most
 * of the time no memory allocation at all will
 * be necessary).
 * The aux buffer is available for use by cases
 * where a temporary work buffer is required
 * whilst draw_buffer is in use - probably only
 * useful for probe drawing.
 */

static char *draw_buffer = NULL;
static char *draw_mask = NULL;
static char *aux_buffer = NULL;
static int draw_buffer_size = 0;
static int draw_mask_size = 0;
static int aux_buffer_size = 0;


/* Private function. */


/*
 * prepare_buffer() is a convenience function to
 * deal with pointer assignments and any allocation
 * or reallocation that may be necessary.
 * Note that malloc and realloc are used (not Malloc
 * and Realloc) because these buffers will never be
 * freed, and I don't want to screw-up the malcheck
 * stuff!
 */

static char *
prepare_buffer(char *buffer, int *oldsize, int newsize, int reqsize)
{
    char *newbuffer;

	if ((newsize < 1) || (reqsize < 1))
		return(buffer);

	/* If the buffer is already allocated, see if
	 * we need to realloc, or just use it as ie. */
    if (buffer)
    {
		/* If we need more space, reallocate. */
        if (newsize > *oldsize)
        {
            newbuffer = (char *)Realloc(buffer, newsize);
            *oldsize = newsize;
        }
        else
            newbuffer = buffer;
    }
    else
    {
		/* Create a new one. */
        newbuffer = (char *)Malloc(newsize);
        *oldsize = newsize;
    }

	/* If the buffer is OK, clear it. This is
	 * required for probes, but may not be for
	 * other stuff... */
    if (newbuffer)
        memset(newbuffer, 0, reqsize);

    return(newbuffer);
} /* prepare_buffer */




/* Public functions. */


/*
 * Return a pointer to the appropriate buffer.
 * These functions will return NULL if there
 * was a malloc/realloc NULL return.
 * The size parameter should be the number of
 * pixels.
 * The mask size is calculated internally (here)
 * from the width and height given (the mask
 * to be a multiple of 8 pixels wide).
 */

unsigned char *
ddgs_get_draw_buffer(int size)
{
	int bytesize;
	int reqsize = size;

	if (size < MINBUFFERSIZE)
		size = MINBUFFERSIZE;

	bytesize = size;

	draw_buffer = prepare_buffer(draw_buffer, &draw_buffer_size,
			bytesize, reqsize);

	return((unsigned char *)draw_buffer);
} /* ddgs_get_draw_buffer */


unsigned char *
ddgs_get_aux_buffer(int size)
{
	int bytesize;
	int reqsize = size;

	if (size < MINBUFFERSIZE)
		size = MINBUFFERSIZE;

	bytesize = size;

	aux_buffer = prepare_buffer(aux_buffer,
			&aux_buffer_size, bytesize, reqsize);

	return((unsigned char *)aux_buffer);
} /* ddgs_get_aux_buffer */


unsigned char *
ddgs_get_draw_mask(int w, int h)
{
	int bytesize;
	int reqsize;

	// Some masks now use 8bpp, some use 1bpp. Allocate enough memory for 8bpp.
	// Also make sure there is enough memory for scanlines to be padded up to
	// the nearest 4 byte boundary.
	int bitsPerPixel = 8;
	int bufWidthBytes = ((w * bitsPerPixel) + 7) >> 3;
	// If this number of bytes is not exactly divisible by 4,
	// pad it upto a 4 byte boundary.
	bufWidthBytes = (bufWidthBytes + 3) & 0xFFFFFFFC;

	reqsize = bytesize = bufWidthBytes * h;

	if (bytesize < MINMASKSIZE)
		bytesize = MINMASKSIZE;

	draw_mask = prepare_buffer(draw_mask,
			&draw_mask_size, bytesize, reqsize);

	return((unsigned char *)draw_mask);
} /* ddgs_get_draw_mask */


/*
 * Free off all the buffers, and set them to NULL.
 * Note that it will still be OK to continue a
 * program after calling this - new buffers will be
 * allocated when required.
 */

void
ddgs_free_buffers()
{
	if (draw_buffer)
		Free(draw_buffer);
	draw_buffer = NULL;
	draw_buffer_size = 0;

	if (aux_buffer)
		Free(aux_buffer);
	aux_buffer = NULL;
	aux_buffer_size = 0;

	if (draw_mask)
		Free(draw_mask);
	draw_mask = NULL;
	draw_mask_size = 0;

	// Free up any other global resources that
	// ddgs has allocated.
	DeleteObject(ddgs_palette);
	free_fonts();
} /* ddgs_free_buffers */



static 
unsigned char *
MakeDisplayMask(int w, int h, unsigned char *pInMask)
{
// This function is used by fastpixobj().
// It takes a 1bpp mask in Woolz format (e.g. as returned by qbuffer())
// and returns a pointer to the same mask in a format compatible for
// display in Windows.
// Woolz 1bpp mask scanline format:
//  Bytes are ordered from left to right
//  Pixels within each byte are ordered from right to left
// Windows compatible 1bpp mask scanline format:
//  Bytes are ordered from left to right
//  Pixels within each byte are ordered from left to right
//  Scanlines are padded, if neccessary, so that they are multiple of 4 bytes long.
	static unsigned char *pOutMask = NULL;
	static size_t outMaskSize = 0;


	if (!pInMask || w < 1 || h < 1)
		return NULL;


	// Make sure that pOutMask is big enough for this mask.
	int unbufferedWidthBytes = (w + 7) >> 3; // ((w * bitsPerPixel) + 7) >> 3
	// If this number of bytes is not exactly divisible by 4,
	// pad it upto a 4 byte boundary.
	int bufferedWidthBytes = (unbufferedWidthBytes + 3) & 0xFFFFFFFC;

	size_t requiredSize = bufferedWidthBytes * h;

	if (requiredSize > outMaskSize)
		pOutMask = (unsigned char*)realloc(pOutMask, requiredSize);

	if (pOutMask)
	{
		unsigned char *pInByte = pInMask;
		unsigned char *pOutScanline = pOutMask;
		for (int y = 0; y < h; y++)
		{
			unsigned char *pOutByte = pOutScanline;
			for (int x = 0; x < unbufferedWidthBytes; x++)
			{
				switch (*pInByte)
				{
					case 0x00: // 00000000
					case 0x81: // 10000001
					case 0x42: // 01000010
					case 0x24: // 00100100
					case 0x18: // 00011000
					case 0xC3: // 11000011
					case 0x66: // 01100110
					case 0x3C: // 00111100
					case 0xE7: // 11100111
					case 0x7E: // 01111110
					case 0xFF: // 11111111
						// The bits in this byte are symmetrical: just copy it
						*pOutByte = *pInByte;
					break;

					default:
					{
						// The bits are not symmetrical: copy them individually.
						// One faster way to do this might be to use a look-up table.
						*pOutByte = 0;
						if (*pInByte & 0x1) *pOutByte |= 0x80;
						if (*pInByte & 0x2) *pOutByte |= 0x40;
						if (*pInByte & 0x4) *pOutByte |= 0x20;
						if (*pInByte & 0x8) *pOutByte |= 0x10;
						if (*pInByte & 0x10) *pOutByte |= 0x8;
						if (*pInByte & 0x20) *pOutByte |= 0x4;
						if (*pInByte & 0x40) *pOutByte |= 0x2;
						if (*pInByte & 0x80) *pOutByte |= 0x1;
						/*
						// Here's an alternative algorithm that uses bitshifts
						// rather then if's. I assume this would be slower,
						// but maybe not.
						// First copy the first 4 bits into the last 4.
						unsigned char inVal = *pInByte;
						inval >>= 1; *pOutByte |= inval & 0x8;
						inval >>= 2; *pOutByte |= inval & 0x4;
						inval >>= 2; *pOutByte |= inval & 0x2;
						inval >>= 2; *pOutByte |= inval & 0x1;
						// Now copy the last 4 into the first 4
						inVal = *pInByte;
						inval <<= 1; *pOutByte |= inval & 0x10;
						inval <<= 2; *pOutByte |= inval & 0x20;
						inval <<= 2; *pOutByte |= inval & 0x40;
						inval <<= 2; *pOutByte |= inval & 0x80;
						*/
					}
					break;
				}

				pInByte++;
				pOutByte++;
			}

			pOutScanline += bufferedWidthBytes;
		}
	}

	return pOutMask;
}


void
fastpixobj(int vx, int vy, int w, int h,
           unsigned char *vdata, unsigned char *mdata,
           short where)
{
// New version of fastpixobj(). Improvements, compared with old version:
// 1) Masking is now done correctly for GREYIM
// 2) Code is easier to read.
//
// Draws an entire object as single pixmap into masked area of image.
// Note that mdata is only used for GREYIM and is ignored for other modes.
// MARKIM is bi-level already so uses itself as a mask.
// OVLYIM does not use masking.
// The mask data is expected to be in the 1bpp format created by Woolz and
// returned by functions such as qbuffer().

	HDC dc;
	int i;
	RGBQUAD keep_col[256];
	DWORD maskOp, imageOp;

	if (!curdg)
		return;

	curdg->binfo.head.biWidth = w;
	curdg->binfo.head.biHeight = -h;
	curdg->binfo.head.biBitCount = 8;

	curdg->minfo.head.biWidth = w;
	curdg->minfo.head.biHeight = -h;
	curdg->minfo.head.biBitCount = 1;


	dc = DOG_DC(curdg);
	SelectClipRgn(dc, curdg->cliprgn);

	/* Make sure the palette is in place. */
	SelectPalette(dc, ddgs_palette, 0);
	RealizePalette(dc);

	if (where == GREYIM)
	{
		// Operation is different depending on
		// image background:
		//	Black bg - AND with inverted mask then OR with image.
		//	White bg - OR with mask then AND with image
		if (   (curdg->binfo.col[0].rgbRed   == 0)
		    && (curdg->binfo.col[0].rgbGreen == 0)
		    && (curdg->binfo.col[0].rgbBlue  == 0))
		{
			maskOp  = SRCAND;
			imageOp = SRCPAINT;
		}
		else
		{
			curdg->binfo.col[0].rgbRed   = 255;
			curdg->binfo.col[0].rgbGreen = 255;
			curdg->binfo.col[0].rgbBlue  = 255;
			maskOp  = MERGEPAINT;
			imageOp = SRCAND;
		}

	SetStretchBltMode(dc,STRETCH_DELETESCANS);	

		// Convert the Woolz format mask into one Windows can use.
		unsigned char *pDisplayMask = MakeDisplayMask(w, h, mdata);
		if (pDisplayMask)
	    	StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
			              0, 0, w, h, pDisplayMask, (BITMAPINFO *)&curdg->minfo,
			              DIB_RGB_COLORS, maskOp);

		StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
		              0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
		              DIB_RGB_COLORS, imageOp);
	}
	else if (where == MARKIM)
	{
		// Keep a copy of the binfo colour table.
		memcpy(keep_col, curdg->binfo.col, 256*sizeof(RGBQUAD));

		// Reset the binfo colour table so that
		// all entries are the marker color - except zero (background).
		curdg->binfo.col[0].rgbRed   = 0;
		curdg->binfo.col[0].rgbGreen = 0;
		curdg->binfo.col[0].rgbBlue  = 0;
		curdg->binfo.col[1].rgbRed   = GetRValue(curdg->markcol[curdg->cur_marker]);
		curdg->binfo.col[1].rgbGreen = GetGValue(curdg->markcol[curdg->cur_marker]);
		curdg->binfo.col[1].rgbBlue  = GetBValue(curdg->markcol[curdg->cur_marker]);
		for (i = 2; i < 256; i++)
			curdg->binfo.col[i] = curdg->binfo.col[i - 1];

		//	Black bg - AND with inverted mask then OR with image.
		maskOp  = SRCAND;
		imageOp = SRCPAINT;

		// Note that here the image data is used as the mask too, but using
		// the mask colour table!
		curdg->minfo.head.biBitCount = 8; // Using the 8bpp image as the mask.
    	StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
		              0, 0, w, h, vdata, (BITMAPINFO *)&curdg->minfo,
		              DIB_RGB_COLORS, maskOp);

		StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
		              0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
		              DIB_RGB_COLORS, imageOp);

		// Put the mask back to its normal condition
		memcpy(curdg->binfo.col, keep_col, 256*sizeof(RGBQUAD));
	}
	else if (OVLYIM)
	{
		// XOR the image on the display, with zero in the background
		// (to leave the area outside objects unchanged by XOR).
		curdg->binfo.col[0].rgbRed   = 0;
		curdg->binfo.col[0].rgbGreen = 0;
		curdg->binfo.col[0].rgbBlue  = 0;
		imageOp = SRCINVERT;

		StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
		              0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
		              DIB_RGB_COLORS, imageOp);
	}

	DOG_RDC(curdg);
}

/*
 *	D I S P L A Y _ I M A G E
 *		-- draw the image data to the device context
 */
void display_image (HDC hdc,int vx, int vy, int w, int h,
				int dispx,int dispy, int dispw, int disph,
				unsigned char *vdata, unsigned char *mdata,DWORD maskOp, DWORD imageOp, short where)
{
	unsigned char *pDisplayMask = NULL;

	if (!curdg)
		return;

	SetStretchBltMode(hdc,STRETCH_DELETESCANS);	

	switch (where) {
		case GREYIM :
							// Convert the Woolz format mask into one Windows can use.
						pDisplayMask = MakeDisplayMask(w, h, mdata);
						if (pDisplayMask)
	   						StretchDIBits(hdc, dispx, dispy, dispw, disph,
										vx, vy, w, h, pDisplayMask, (BITMAPINFO *)&curdg->minfo,
										DIB_RGB_COLORS, maskOp);

						StretchDIBits(hdc, dispx, dispy, dispw, disph,
									vx, vy, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
									DIB_RGB_COLORS, imageOp);

						break;

		case MARKIM:	StretchDIBits(hdc, dispx, dispy, dispw, disph,
										vx, vy, w, h, vdata, (BITMAPINFO *)&curdg->minfo,
										 DIB_RGB_COLORS, maskOp);
						StretchDIBits(hdc, dispx, dispy, dispw, disph,
										  vx, vy, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
											DIB_RGB_COLORS, imageOp);
						break;

		case OVLYIM:	StretchDIBits(hdc, dispx, dispy, dispw, disph,
										vx, vy, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
										DIB_RGB_COLORS, imageOp);

						break;
		}

}

/*
 *	S T A R T G D I P L U S
 *		-- start the GDIplus session
 */
void StartGDIplus()
{

	if (gdiplusToken != NULL)
		return;

	GdiplusStartupInput gdiplusStartupInput;
	if (GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL)!= Ok)
		gdiplusToken = NULL;

}

/*
 *	S T O P G D I P L U S
 *		-- shutdown the GDIplus session
 */
void StopGDIplus()
{
	if (gdiplusToken == NULL)
		return; 

	GdiplusShutdown(gdiplusToken);
	gdiplusToken = NULL;
}

/*
 *	M A K E G D I M A S K R E G I O N
 *		-- creat a GDIplus mask region from a woolz 1bpp mask.
 *
 *		-- Woolz 1bpp mask scanline format:
 *		-- Bytes are ordered from left to right
 *		-- Pixels within each byte are ordered from right to left
 */
void MakeGDIMaskRegion(int w, int h, unsigned char *pInMask, Gdiplus::Region *region, Gdiplus::Rect *rc)
{
	BOOL inblob = FALSE;

	if (!pInMask || w < 1 || h < 1)
		return ;

	unsigned char *pInByte = pInMask;
	int unbufferedWidthBytes = (w + 7) >> 3;
	unsigned int i;
	unsigned char bit;
	int x,y;

	for (y = 0; y < h; y++)	
	{
		for (x = 0; x < unbufferedWidthBytes; x++)
		{
			bit = 0x01;
			i = 0;
			while (bit) 
			{
				if (!inblob)
				{
					if ((*pInByte & bit) != 0)
					{
						inblob = TRUE;
						rc->X = x * 8 + i;// - 2;
						rc->Y = y -1;
						rc->Height = 3;//3;
					}	
				}
				else 
				{
					if ((*pInByte & bit) == 0) 
					{
						inblob = FALSE;
						rc->Width= (x * 8 + i) - rc->X; // + 2;
						region->Union(*rc);
					}
				}
				bit = bit << 1;
				i++;
			}
			pInByte++;
		}
		if (inblob) 
		{
			inblob = FALSE;
			rc->Width= (x * 8) - rc->X; // + 2;
			region->Union(*rc);
		}
	}
}

/*
 *	M A K E G D I M A S K R E G I O N 8
 *		-- creat a GDIplus mask region from a 8 bpp image mask.
 *
 *		-- Woolz 1bpp mask scanline format:
 *		-- Bytes are ordered from left to right
 *		-- Pixels within each byte are ordered from right to left
 */
void MakeGDIMaskRegion8(int w, int h, unsigned char *pInMask, Gdiplus::Region *region, Gdiplus::Rect *rc)
{
	BOOL inblob = FALSE;

	if (!pInMask || w < 1 || h < 1)
		return ;

	unsigned char *pInByte = pInMask;
	int unbufferedWidthBytes = 4*((w + 3) >> 2); 
	int x,y;

	for (y = 0; y < h; y++)	
	{
		for (x = 0; x < w; x++)
		{
			if (!inblob && *pInByte == 0)
			{
				inblob = TRUE;
				rc->X = x;
				rc->Y = y;
				rc->Height = 1;
			}
			else if (inblob && *pInByte != 0) 
			{
				inblob = FALSE;
				rc->Width= x - rc->X;
				region->Union(*rc);
			}
			pInByte++;
		}
		if (inblob) 
		{
			rc->Width= x - rc->X;
			region->Union(*rc);
		}
		pInByte += unbufferedWidthBytes - w;
		inblob = FALSE;
	}
}

Bitmap* ApplyTransparentMask(unsigned char * data, unsigned char * mask, int w, int h)
{
    Bitmap *bitmap = new Bitmap(w, h, PixelFormat32bppARGB);

    Gdiplus::BitmapData bmData;
	Gdiplus::Rect bmRect = Gdiplus::Rect(0, 0, w, h);
	bitmap->LockBits(&bmRect, Gdiplus::ImageLockModeRead, PixelFormat32bppARGB, &bmData);

	int maskw = (w + 7) >> 3;
	unsigned char* bits = (unsigned char*)bmData.Scan0;

	int pt = 0; int bpp = 4;

	unsigned char *maskPtr = mask;
	for (int y = 0; y < h; ++y)
	{
		int rowindex = 0;

		for (int x = 0; x < w * bpp; x += bpp)
		{
			unsigned char alpha = 0xFF;
			unsigned char pixel = data[pt];
			
			if (mask)
			{
				int bit = rowindex % 8;
				int maskindex = rowindex /8;
				alpha = ((*(maskPtr + maskindex) >> bit) & 0x01)  ? 0xFF : 0x00;
			}

			bits[x] = curdg->binfo.col[pixel].rgbRed;
			bits[x+1] = 	curdg->binfo.col[pixel].rgbGreen;
			bits[x+2] = curdg->binfo.col[pixel].rgbBlue;
			bits[x+3] = alpha;
			pt++;
			rowindex++;
		}
		bits += bmData.Stride;
		if (mask)
			maskPtr += maskw;
	}
	bitmap->UnlockBits(&bmData);
	return bitmap;
}

void DisplayImageTransparencyMask ( HDC hdc, int vx, int vy, int w, int h,	int dispx,int dispy, int dispw, int disph,
													unsigned char *vdata, unsigned char *mdata, DWORD maskop, DWORD imageop,short where)
{
	if (!curdg)
		return;

	if (gdiplusToken == NULL)
		return;

	Bitmap * pGDIBitmap = ApplyTransparentMask(vdata, mdata, w, h);

	if (pGDIBitmap == NULL) 
		return;

	Rect destGdiplusRect(dispx, dispy, dispw,disph);
	Graphics graphics(hdc);
	graphics.SetPixelOffsetMode(PixelOffsetModeHalf);

	switch (where) 
	{
		case GREYIM :
					graphics.SetInterpolationMode(InterpolationModeHighQualityBilinear);
					graphics.DrawImage(pGDIBitmap,destGdiplusRect,0,0,w,h,UnitPixel,NULL);
					break;

			case MARKIM:
					SetStretchBltMode(hdc,STRETCH_DELETESCANS);
					StretchDIBits (hdc, dispx,dispy,dispw,disph, 0,0,w,h, vdata, (BITMAPINFO *) &curdg->minfo, DIB_RGB_COLORS, maskop);
					StretchDIBits (hdc, dispx,dispy,dispw,disph, 0,0,w,h, vdata, (BITMAPINFO *) &curdg->binfo, DIB_RGB_COLORS, imageop);
					break;

			case OVLYIM:	
					graphics.SetInterpolationMode(InterpolationModeDefault );
					graphics.DrawImage(pGDIBitmap,destGdiplusRect,vx,vy,w,h,UnitPixel,NULL);
					break;
		}

	delete pGDIBitmap;
	DOG_RDC(curdg);
}

/*
 *	C O P Y I M A G E D A T A
 *		-- copy image data to a bitmap
 *		-- and optionally offset the object
 *		-- this will prevent edge effects when images (objects)
 *		-- are interpolated using GDIplus routines
 */
HBITMAP copyimagedata (HDC hdc, unsigned char *vdata,int w, int h, int xoff,int yoff)
{
	HDC hdcObject;
	HBITMAP bmObject;
	HGDIOBJ oldObj;
	unsigned char *mtemp;
	int ww,hh;

	if (!curdg)
		return NULL;

		//create a temporary image for background
		//which is slightly larger than the original
	ww = ((w + 1 + xoff + 3) >> 2) << 2;		//word align
	hh = h + 1 + yoff;
	mtemp = (unsigned char *) malloc (ww*hh * sizeof (unsigned char));
	memset (mtemp,0,ww*hh * sizeof (unsigned char));


		//create dc and bitmap
	hdcObject = CreateCompatibleDC (hdc);
	bmObject = CreateCompatibleBitmap (hdc,ww,hh);
	oldObj = SelectObject(hdcObject, bmObject); 

		//draw background to bitmap
	curdg->binfo.head.biWidth = ww;
	curdg->binfo.head.biHeight = -hh;
	StretchDIBits(hdcObject, 0, 0, ww, hh, 0, 0, ww, hh, mtemp, (BITMAPINFO *)&curdg->binfo,DIB_RGB_COLORS, SRCCOPY);

		//draw object to bitmap
	curdg->binfo.head.biWidth = w;
	curdg->binfo.head.biHeight = -h;
	StretchDIBits(hdcObject, xoff, yoff, w, h, 0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,DIB_RGB_COLORS, SRCCOPY);
	
	//tidy up
	free ((void *) mtemp);
	DeleteDC(hdcObject);


	return bmObject;
}

void DisplayMaskImage (HDC hdc, int vx, int vy, int w, int h, int dispx,int dispy, int dispw, int disph, unsigned char *mdata, DWORD maskop)
{
	SetStretchBltMode(hdc,STRETCH_DELETESCANS);
	unsigned char * pDisplayMask = MakeDisplayMask(w, h, mdata);
	if (pDisplayMask)
	{
	   	StretchDIBits(hdc, dispx, dispy, dispw, disph, vx, vy, w, h, pDisplayMask, (BITMAPINFO *)&curdg->minfo,	DIB_RGB_COLORS, maskop);
	}
}
/*
 *	D I S P L A Y _ I M A G E _ G D I P L U S
 * 		-- draw the image data to the device context
 *		-- using GDIplus calls for pixel interpolation
 */
void display_image_GDIplus (HDC hdc, int vx, int vy, int w, int h,
				int dispx,int dispy, int dispw, int disph,
				unsigned char *vdata, unsigned char *mdata, DWORD maskop, DWORD imageop,short where)
{
	HDC hdcObject;
	float scaleX = 1.0;
	float scaleY = 1.0;
	HBITMAP bmObject,bmObj;
	int xoffset,yoffset;
	HGDIOBJ oldObj;
	BITMAP bm;
//	COLORREF bckgrndcol;

	if (!curdg)
		return;

	if (gdiplusToken == NULL)
		return;

		//copy the image data to slightly larger and offset
		//bitmap to avoid edge artefacts with interpolation 
	xoffset = yoffset = 1;
	bmObject = copyimagedata (hdc,vdata,w,h,xoffset,yoffset);	

	Bitmap *pGDIBitmap = new Bitmap (bmObject, ddgs_palette);

	if (pGDIBitmap == NULL) 
		return;

		//create a Rect object that specifies the destination of the image.
	Rect destGdiplusRect(xoffset, yoffset,dispw,disph);

		//create memory dc for the larger bitmap
	hdcObject = CreateCompatibleDC (hdc);
	bmObj = CreateCompatibleBitmap (hdc,dispw,disph);
	GetObject(bmObj, sizeof(BITMAP), (LPSTR)&bm);
	bm.bmWidth = dispw;
	bm.bmHeight = disph;
	oldObj = SelectObject (hdcObject, bmObj);

		//create a GDIplus Graphics object
	Graphics graphics(hdcObject);
/*
	if (mdata) {
			//create a mask region from a 1 bpp mask
		Rect maskRect (0,0,0,0); 
		Region maskRegion(maskRect);
		MakeGDIMaskRegion(w, h, mdata, &maskRegion, &maskRect);

			//translate and scale mask region to fit display
		scaleX = (float) dispw / (float) w;
		scaleY = (float) disph / (float) h;
		Matrix matrix(scaleX,0,0,scaleY, (float)dispx,(float)dispy);  
		maskRegion.Transform(&matrix);

			//allow drawing in mask region only
		graphics.SetClip(&maskRegion);
		graphics.TranslateClip (xoffset,yoffset);
		}
*/

	switch (where) {
		case GREYIM :
					graphics.SetInterpolationMode(InterpolationModeHighQualityBicubic);
							// By default, GDI+ puts the centre (rather than the top left corner)
							// of the top-left pixel at (0,0). Change this so there is no shifting
							// when switching between using GDI+ and GDI. I don't know if this slows
							// things down.
					graphics.SetPixelOffsetMode(PixelOffsetModeHalf);
					graphics.DrawImage(pGDIBitmap,destGdiplusRect,xoffset,yoffset,w,h,UnitPixel,NULL);
	   				DisplayMaskImage(hdc, vx, vy, w, h, dispx,dispy,dispw,disph, mdata, maskop);
					BitBlt (hdc,dispx,dispy,dispw,disph,hdcObject,xoffset,yoffset,imageop);
					break;

			case MARKIM:
/*
using GDIplus, doesn't look great!
				if (vdata) {
						//create a mask region from the image
					Rect maskRect (0,0,0,0); 
					Region maskRegion(maskRect);
					MakeGDIMaskRegion8(w, h, vdata, &maskRegion, &maskRect,(unsigned char)1);

						//translate and scale mask region to fit display
					scaleX = (float) dispw / (float) w;
					scaleY = (float) disph / (float) h;
					Matrix matrix(scaleX,0,0,scaleY, (float)dispx,(float)dispy);  
					maskRegion.Transform(&matrix);

						//allow drawing in mask region only
					graphics.SetClip(&maskRegion);
					}
						//draw the image 
					graphics.SetInterpolationMode(InterpolationModeBilinear);
					graphics.SetPixelOffsetMode(PixelOffsetModeHalf);
					graphics.DrawImage(pGDIBitmap,destGdiplusRect,vx,vy,w,h,UnitPixel,NULL);
*/

					SetStretchBltMode(hdc,STRETCH_DELETESCANS);
					StretchDIBits (hdc, dispx,dispy,dispw,disph, 0,0,w,h, vdata, (BITMAPINFO *) &curdg->minfo, DIB_RGB_COLORS, maskop);
					StretchDIBits (hdc, dispx,dispy,dispw,disph, 0,0,w,h, vdata, (BITMAPINFO *) &curdg->binfo, DIB_RGB_COLORS, imageop);

					break;

			case OVLYIM:	
					graphics.SetInterpolationMode(InterpolationModeDefault );
					graphics.SetPixelOffsetMode(PixelOffsetModeHalf);
					graphics.DrawImage(pGDIBitmap,destGdiplusRect,vx,vy,w,h,UnitPixel,NULL);
					BitBlt (hdc,dispx,dispy,dispw,disph,hdcObject,xoffset,yoffset,SRCAND);
					break;
		}

	DeleteObject (bmObj);
	DeleteObject (bmObject);
	DeleteDC(hdcObject);

	delete pGDIBitmap;

	DOG_RDC(curdg);

}


/*
 *		N F A S T P I X O B J
 *
 */
void nfastpixobj(int vx, int vy, int w, int h,
				int dispx,int dispy, int dispw, int disph,
				unsigned char *vdata, unsigned char *mdata,
				short where)
{

	HDC dc;
	int i;
	RGBQUAD keep_col[256];
	DWORD maskOp, imageOp;

	maskOp = imageOp = 0;

	if (!curdg)
		return;


	curdg->binfo.head.biWidth = w;
	curdg->binfo.head.biHeight = -h;
	curdg->binfo.head.biBitCount = 8;

	curdg->minfo.head.biWidth = w;
	curdg->minfo.head.biHeight = -h;
	curdg->minfo.head.biBitCount = 1;


	dc = DOG_DC(curdg);
	SelectClipRgn(dc, curdg->cliprgn);

	/* Make sure the palette is in place. */
	SelectPalette(dc, ddgs_palette, 0);
	RealizePalette(dc);

	if (where == GREYIM)
	{

		// Operation is different depending on
		// image background:
		//	Black bg - AND with inverted mask then OR with image.
		//	White bg - OR with mask then AND with image
		if (   (curdg->binfo.col[0].rgbRed   == 0)
		    && (curdg->binfo.col[0].rgbGreen == 0)
		    && (curdg->binfo.col[0].rgbBlue  == 0))
		{
			maskOp  = SRCAND;
			imageOp = SRCPAINT;
		}
		else
		{
			curdg->binfo.col[0].rgbRed   = 255;
			curdg->binfo.col[0].rgbGreen = 255;
			curdg->binfo.col[0].rgbBlue  = 255;
			maskOp  = MERGEPAINT;
			imageOp = SRCAND;
		}

		if (curdg->ddgsImageFormat.pixel_interpolation)
//			display_image_GDIplus (dc,vx, vy, w, h,dispx, dispy, dispw, disph,vdata,mdata,maskOp,imageOp,where);
			DisplayImageTransparencyMask(dc,vx, vy, w, h,dispx, dispy, dispw, disph,vdata,mdata,maskOp,imageOp,where);
		else
			display_image (dc,vx, vy, w, h,dispx, dispy, dispw, disph,vdata,mdata,maskOp,imageOp,where);

	}
	else if (where == MARKIM)
	{

		// Keep a copy of the binfo colour table.
		memcpy(keep_col, curdg->binfo.col, 256*sizeof(RGBQUAD));

		// Reset the binfo colour table so that
		// all entries are the marker color - except zero (background).
		curdg->binfo.col[0].rgbRed   = 0;
		curdg->binfo.col[0].rgbGreen = 0;
		curdg->binfo.col[0].rgbBlue  = 0;
		curdg->binfo.col[1].rgbRed   = GetRValue(curdg->markcol[curdg->cur_marker]);
		curdg->binfo.col[1].rgbGreen = GetGValue(curdg->markcol[curdg->cur_marker]);
		curdg->binfo.col[1].rgbBlue  = GetBValue(curdg->markcol[curdg->cur_marker]);
		for (i = 2; i < 256; i++)
			curdg->binfo.col[i] = curdg->binfo.col[i - 1];

		//	Black bg - AND with inverted mask then OR with image.
		maskOp  = SRCAND;
		imageOp = SRCPAINT;

		// Note that here the image data is used as the mask too, but using
		// the mask colour table!
		curdg->minfo.head.biBitCount = 8; // Using the 8bpp image as the mask.

		if (curdg->ddgsImageFormat.pixel_interpolation)
			//display_image_GDIplus (dc,vx, vy, w, h,dispx, dispy, dispw, disph,vdata,mdata,maskOp,imageOp,where);
			DisplayImageTransparencyMask(dc,vx, vy, w, h,dispx, dispy, dispw, disph,vdata,mdata,maskOp,imageOp,where);
		else
			display_image (dc,vx, vy, w, h,dispx, dispy, dispw, disph,vdata,mdata,maskOp,imageOp,where);

		// Put the mask back to its normal condition
		memcpy(curdg->binfo.col, keep_col, 256*sizeof(RGBQUAD));
	}
	else if (OVLYIM)
	{

		// XOR the image on the display, with zero in the background
		// (to leave the area outside objects unchanged by XOR).
		curdg->binfo.col[0].rgbRed   = 0;
		curdg->binfo.col[0].rgbGreen = 0;
		curdg->binfo.col[0].rgbBlue  = 0;
		imageOp = SRCINVERT;

		if (curdg->ddgsImageFormat.pixel_interpolation)
			//display_image_GDIplus (dc,vx, vy, w, h,dispx, dispy, dispw, disph,vdata,mdata,maskOp,imageOp,where);
			DisplayImageTransparencyMask(dc,vx, vy, w, h,dispx, dispy, dispw, disph,vdata,mdata,maskOp,imageOp,where);
		else
			display_image (dc,vx, vy, w, h,dispx, dispy, dispw, disph,vdata,mdata,maskOp,imageOp,where);

	}

	DOG_RDC(curdg);
}

/*
 *	D I S P L A Y _ I M A G E 2 4_G D I P L U S
 *		-- draw the 24-bit image data to the device context
 *		-- using GDIplus calls
 */
void display_image24_GDIplus (HDC hdc, int vx, int vy, int w, int h,
				int dispx,int dispy, int dispw, int disph,
				unsigned char *vdata, unsigned char *mdata,
				short where)
{
	float scaleX = 1.0;
	float scaleY = 1.0;

	if (!curdg)
		return;

	if (gdiplusToken == NULL)
		return;

	//image data is in 32 bits, mask is 8
	curdg->binfo.head.biBitCount = 32;
	Bitmap *pGDIBitmap = new Bitmap((BITMAPINFO *)&curdg->binfo, vdata);	

	if (pGDIBitmap == NULL) 
		return;

	//create a Rect object that specifies the destination of the image.
	Rect destGdiplusRect(dispx,dispy,dispw,disph);

	//create a Graphics object
	Graphics graphics(hdc);

// data is premixed, clipping not necessary but useful techinique
	if (mdata) 
	{
		//create a mask region
		Rect maskRect (0,0,0,0); 
		Region maskRegion(maskRect);
		MakeGDIMaskRegion8(w, h, mdata, &maskRegion, &maskRect);

		//translate and scale mask region to fit display
		scaleX = (float) dispw / (float) w;
		scaleY = (float) disph / (float) h;
		Matrix matrix(scaleX,0,0,scaleY, (float)dispx,(float)dispy);  
		maskRegion.Transform(&matrix);

		//allow drawing in mask region only
		graphics.SetClip(&maskRegion);

		// DEBUG: Fill the region to view mask. 
		//SolidBrush brush(Color(255, 255, 255, 255));
		//graphics.FillRegion(&brush, &maskRegion);
	}

	// display seems better with default drawing mode
//	graphics.SetInterpolationMode(InterpolationModeHighQualityBicubic);
	// By default, GDI+ puts the centre (rather than the top left corner)
	// of the top-left pixel at (0,0). Change this so there is no shifting
	// when switching between using GDI+ and GDI. I don't know if this slows
	// things down.
	graphics.SetPixelOffsetMode(PixelOffsetModeHalf);
	graphics.DrawImage(pGDIBitmap,destGdiplusRect,vx,vy,w,h,UnitPixel,NULL);

	delete pGDIBitmap;

	DOG_RDC(curdg);
}

/*
 *	D I S P L A Y _ I M A G E 2 4
 *		-- draw the 24-bit image data to the device context
 *		
 */
void display_image24 (HDC hdc, int vx, int vy, int w, int h,
				int dispx,int dispy, int dispw, int disph,
				unsigned char *vdata, unsigned char *mdata,
				short where)
{
	HDC dataDC;
	HBITMAP dataBmp;
	HGDIOBJ oldObj;

	if (curdg == NULL)
		return;

		// Create compatible DC and Bmp in memory to hold image while we work on it
	dataBmp = CreateCompatibleBitmap(hdc, dispw, disph);
	dataDC = CreateCompatibleDC(hdc);
	oldObj = SelectObject(dataDC, dataBmp); 

		// BitBlt from screen to memory
	BitBlt(dataDC, 0, 0, dispw, disph, hdc, dispx, dispy, SRCCOPY);

		// Mask data is in 8 bits 
	curdg->binfo.head.biBitCount = 8;

	SetStretchBltMode(dataDC,STRETCH_DELETESCANS);
		// AND image data with mask data for this object
	StretchDIBits(dataDC, 0, 0, dispw, disph,0, 0, w, h,
				 mdata, (BITMAPINFO *)&curdg->binfo,DIB_RGB_COLORS, SRCAND);
			
		// Image data is in 32 bits 
	curdg->binfo.head.biBitCount = 32;
		// OR this with image data for this object
	StretchDIBits(dataDC,0, 0, dispw, disph,0, 0, w, h,
					vdata, (BITMAPINFO *)&curdg->binfo,DIB_RGB_COLORS, SRCPAINT);	// OR

		// BitBlt from memory back to screen
	SetStretchBltMode(hdc,STRETCH_DELETESCANS);
	BitBlt(hdc, dispx, dispy, dispw, disph, dataDC, 0, 0, SRCCOPY);


	oldObj = SelectObject(dataDC, oldObj); 
	DeleteObject(dataBmp); 
	DeleteDC(dataDC); 

	DOG_RDC(curdg);

}

/*
 *	D I S P L A Y _ I M A G E 3 2
 *		-- draw the 32-bit image data to the device context
 *
 */
void display_image32 (HDC hdc,int vx, int vy, int w, int h,
				int dispx,int dispy, int dispw, int disph,
				unsigned char *vdata, unsigned char *mdata,
				short where)
{
	if (curdg == NULL)
		return;

			// Image data is in 32 bits 
	curdg->binfo.head.biBitCount = 32;
	StretchDIBits(hdc, dispx, dispy, dispw, disph,vx, vy, w, h, vdata, (BITMAPINFO *)&curdg->binfo,DIB_RGB_COLORS, SRCPAINT);	// OR

	DOG_RDC(curdg);
}

/*
 *	D I S P L A Y _ I M A G E 2 4_G D I P L U S
 *		-- draw the 32-bit image data to the device context
 *		-- using GDIplus calls
 */
void display_image32_GDIplus (HDC hdc,int vx, int vy, int w, int h,
				int dispx,int dispy, int dispw, int disph,
				unsigned char *vdata, unsigned char *mdata,
				short where)
{
	HDC tmpDC;
	HBITMAP tmpBmp;
	HGDIOBJ tmpObj;

	if (!curdg)
		return;

	if (gdiplusToken == NULL)
		return;

		//image data is in 32 bits
	curdg->binfo.head.biBitCount = 32;

		// Create compatible DC and Bmp in memory 
		//to draw to, then blt the image to the screen

	tmpBmp = CreateCompatibleBitmap(hdc, dispw, disph);
	tmpDC = CreateCompatibleDC(hdc);
	tmpObj = SelectObject(tmpDC, tmpBmp); 


	Bitmap *pGDIBitmap = new Bitmap((BITMAPINFO *)&curdg->binfo, vdata);	
	if (pGDIBitmap == NULL) 
		return;

		//create a Rect object that specifies the destination of the image.
	Rect destGdiplusRect(0,0,dispw,disph);

		//create a Graphics object
	Graphics graphics(tmpDC);

	graphics.SetInterpolationMode(InterpolationModeHighQualityBicubic);
	graphics.SetPixelOffsetMode(PixelOffsetModeHalf);
	graphics.DrawImage(pGDIBitmap,destGdiplusRect,vx,vy,w,h,UnitPixel,NULL);

		//Blt OR the interpolated image with the screen
	BitBlt (hdc,dispx,dispy,dispw,disph,tmpDC,0,0,SRCPAINT); 

	tmpObj = SelectObject(tmpDC, tmpObj); 
	DeleteObject(tmpBmp); 
	DeleteDC(tmpDC); 

	delete pGDIBitmap;

	DOG_RDC(curdg);

}

/*
 * 24-bit version of fastpixobj.
 * Assumes in-coming data is in 24-bit
 * RGB format already. The colourmap in
 * binfo is ignored. There is no "where"
 * parameter, since its meaningless here.
 * There is no buffered version of this (ie
 * DDGSBUFFERED is ignored).
 */
void
nfastpixobj24(int vx, int vy, int w, int h,
				int dispx,int dispy, int dispw, int disph,
				unsigned char *vdata, unsigned char *mdata,
				short where)
{
	HDC dc;

	if (curdg == NULL)
		return;

	dc = DOG_DC(curdg);
	SelectClipRgn(dc, curdg->cliprgn);

	/* Make sure the palette is in place.
	 * This allows some kind of reasonable
	 * display of probes on an 8-bit system. */
	SelectPalette(dc, ddgs_palette, 0);
	RealizePalette(dc);
	curdg->binfo.head.biWidth = w;
	curdg->binfo.head.biHeight = -h;

	/* Draw image with SRCCOPY - unlike other
	 * modes, we know probe data is pre-mixed,
	 * so don't need to worry about masking. */
	//StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
	//	0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
	//	DIB_RGB_COLORS, SRCCOPY);

	/* WH  
	** I'm afraid we do need masking, Can't use colour table for mask
	** as we are using 24 bit colour data so need a image mask (mdata).
	** other stuff hit by this are
	** canvas/24bitcanvas.c/draw_overlap_mask_buffer()
	** ddgs_get_draw_mask()
	*/
	// Mask is 8 bit
	// Operation is different depending on
	// image background:
	//	Black bg - AND with inverted mask then OR with image.
	//	White bg - OR with image.
	if ((curdg->binfo.col[0].rgbRed == 0) &&
		(curdg->binfo.col[0].rgbGreen == 0) &&
		(curdg->binfo.col[0].rgbBlue == 0))
	{
	if (curdg->ddgsImageFormat.pixel_interpolation)
		display_image24_GDIplus(dc,vx,vy,w,h,dispx,dispy,dispw,disph,vdata,mdata,where);
	else
		display_image24(dc,vx,vy,w,h,dispx,dispy,dispw,disph,vdata,mdata,where);

	}
	else
	{
	if (curdg->ddgsImageFormat.pixel_interpolation)
		display_image32_GDIplus(dc,vx,vy,w,h,dispx,dispy,dispw,disph,vdata,mdata,where);
	else
		display_image32(dc,vx,vy,w,h,dispx,dispy,dispw,disph,vdata,mdata,where);
	
	}

	// SN 12Oct98 Release ddgs if neccesary
	DOG_RDC(curdg);	
}


/*
 * 24-bit version of fastpixobj.
 * Assumes in-coming data is in 24-bit
 * RGB format already. The colourmap in
 * binfo is ignored. There is no "where"
 * parameter, since its meaningless here.
 * There is no buffered version of this (ie
 * DDGSBUFFERED is ignored).
 */
void
fastpixobj24(int vx, int vy, int w, int h,
		   unsigned char *vdata, unsigned char *mdata)
{
	HDC dc;
	HDC /*maskDC, */dataDC;
	HBITMAP /*maskBmp, */dataBmp;
	HGDIOBJ oldObj;

	if (curdg == NULL)
		return;

	dc = DOG_DC(curdg);
	SelectClipRgn(dc, curdg->cliprgn);

	/* Make sure the palette is in place.
	 * This allows some kind of reasonable
	 * display of probes on an 8-bit system. */
	SelectPalette(dc, ddgs_palette, 0);
	RealizePalette(dc);

	curdg->binfo.head.biWidth = w;
	curdg->binfo.head.biHeight = -h;

	/* Draw image with SRCCOPY - unlike other
	 * modes, we know probe data is pre-mixed,
	 * so don't need to worry about masking. */
	//StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
	//	0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
	//	DIB_RGB_COLORS, SRCCOPY);

	/* WH  
	** I'm afraid we do need masking, Can't use colour table for mask
	** as we are using 24 bit colour data so need a image mask (mdata).
	** other stuff hit by this are
	** canvas/24bitcanvas.c/draw_overlap_mask_buffer()
	** ddgs_get_draw_mask()
	*/
	// Mask is 8 bit
	// Operation is different depending on
	// image background:
	//	Black bg - AND with inverted mask then OR with image.
	//	White bg - OR with image.
	if ((curdg->binfo.col[0].rgbRed == 0) &&
		(curdg->binfo.col[0].rgbGreen == 0) &&
		(curdg->binfo.col[0].rgbBlue == 0))
	{
		// Create compatible DC and Bmp in memory to hold image while we work on it
		dataBmp = CreateCompatibleBitmap(dc, w, h);
		dataDC = CreateCompatibleDC(dc);
		oldObj = SelectObject(dataDC, dataBmp); 
		// BitBlt from screen to memory
		BitBlt(dataDC, 0, 0, w, h, dc, vx, curdg->maxy - vy, SRCCOPY);

		// Mask data is in 8 bits 
		curdg->binfo.head.biBitCount = 8;
		// AND image data with mask data for this object
		StretchDIBits(dataDC, 0, 0, w, h,
			0, 0, w, h, mdata, (BITMAPINFO *)&curdg->binfo,
			DIB_RGB_COLORS, SRCAND);
			
		// Image data is in 32 bits 
		curdg->binfo.head.biBitCount = 32;
		// OR this with image data for this object
		StretchDIBits(dataDC, 0, 0, w, h,
			0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
			DIB_RGB_COLORS, SRCPAINT);	// OR

		// BitBlt from meory back to screen
		BitBlt(dc, vx, curdg->maxy - vy, w, h, dataDC, 0, 0, SRCCOPY);

		oldObj = SelectObject(dataDC, oldObj); 
		DeleteObject(dataBmp); 
		DeleteDC(dataDC); 
	}
	else
	{
		// Image data is in 32 bits 
		curdg->binfo.head.biBitCount = 32;

		StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
			0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
			DIB_RGB_COLORS, SRCPAINT);	// OR
	//	DOG_RDC(curdg);
	}

	// SN 12Oct98 Release ddgs if neccesary
	DOG_RDC(curdg);	
}


/*
 * pixelmode
 *
 * set the pixel-drawing mode as follows (assuming ~INVERT mode):
 *
 * (1) Value inserted in all planes = greyvalue & curdg->cur_colour
 * (2) Value inserted in selected planes only = greyvalue & curdg->cur_colour
 * (3) Greyvalue & curdg->cur_colour inserted ONLY IF existing
 *			value&~curdg->cur_colour == 0
 */
int
pixelmode(int m)
{
	int	old;

	if (!curdg)
		return 0;

	old = curdg->cur_pixelmode;
	curdg->cur_pixelmode = m;
	return(old);
}






#if 0 
/*
 * Old version of fastpixobj.
 * There is at least one bug in this old version, which is that masking is not
 * done correctly for GREYIM. The code uses the actual image data as the mask,
 * rather than mdata. This works okay for MARKIM, because in that case the
 * image is bi-level, however it doesn't work for GREYIM. This has presumably
 * not caused any noticeable problems in the past because the Canvas display
 * code always erases to the background colour before drawing an image, in
 * which case the masking has no effect. However there is now code that needs
 * to display objects without doing an erase first (to reduce flickering), and
 * so this old version of fastpixobj has been replaced.
 */

#define	DDGSMASKED


/*
 * fastpixobj
 * Draw entire object as single pixmap into image.
 *
 * Works on 8-bit and 24-bit displays. On
 * a 24-bit display, the data (which is assumed
 * to be 8 bits) is converted through the ddgs
 * LUTs into a 24-bit buffer. If the display is
 * 8 bits, the data is converted into range
 * 64-127 to use the ddgs display LUT grey range.
 */
void
fastpixobj(int vx, int vy, int w, int h,
		   unsigned char *vdata, unsigned char *mdata,
		   short where)
{
	HDC dc;
	int i;
	RGBQUAD keep_col[256];

#ifdef	DDGSBUFFERED
	RECT rect;
#endif

	if (curdg == NULL)
		return;

	curdg->binfo.head.biWidth = w;
	curdg->binfo.head.biHeight = -h;
	curdg->binfo.head.biBitCount = 8;

#ifdef DDGSMASKED
	curdg->minfo.head.biWidth = w;
	curdg->minfo.head.biHeight = -h;
	curdg->minfo.head.biBitCount = 8;
#endif

	dc = DOG_DC(curdg);
	SelectClipRgn(dc, curdg->cliprgn);

	/* Make sure the palette is in place. */
	SelectPalette(dc, ddgs_palette, 0);
	RealizePalette(dc);

	switch (where) {
	case GREYIM:

#ifdef	DDGSBUFFERED
		// Draw the object data into the ddgs
		// image buffer (should be done elsewhere).
		rect.left = vx;
		rect.top = curdg->maxy - vy;
		rect.right = rect.left + w - 1;
		rect.bottom = rect.top + h - 1;
		ddgsImageDrawGrey(curdg, vdata, &rect);

		// Copy the section of the ddgs DIB image
		// buffer to the dc. Note the destination
		// y coordinate is upside-down to match
		// the upside-down (top-down) DIB.
		ptr = (BYTE *)GlobalLock(curdg->himage);
		StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
			vx, vy - h + 1, w, h, ptr, (BITMAPINFO *)&curdg->binfo,
			DIB_RGB_COLORS, SRCCOPY);
		GlobalUnlock(curdg->himage);
#else
#ifdef DDGSMASKED
		// Operation is different depending on
		// image background:
		//	Black bg - AND with inverted mask then OR with image.
		//	White bg - OR with mask then AND with image

		if ((curdg->binfo.col[0].rgbRed == 0) &&
			(curdg->binfo.col[0].rgbGreen == 0) &&
			(curdg->binfo.col[0].rgbBlue == 0))
		{
			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->minfo,
				DIB_RGB_COLORS, SRCAND);

			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCPAINT);
		}
		else
		{
			curdg->binfo.col[0].rgbRed = 255;
			curdg->binfo.col[0].rgbGreen = 255;
			curdg->binfo.col[0].rgbBlue = 255;

			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->minfo,
				DIB_RGB_COLORS, MERGEPAINT);

			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCAND);
		}

#else
		// If fluorescent, we OR the image onto
		// the black background. Otherwise, its
		// brightfield, and we AND the image with
		// the white background.
		if ((curdg->binfo.col[0].rgbRed == 0) &&
			(curdg->binfo.col[0].rgbGreen == 0) &&
			(curdg->binfo.col[0].rgbBlue == 0))
			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCPAINT);
		else
			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCAND);
#endif
#endif
		break;

	case OVLYIM:

#ifdef	DDGSBUFFERED
		// Draw the object data into the ddgs
		// image buffer (should be done elsewhere).
		rect.left = vx;
		rect.top = curdg->maxy - vy;
		rect.right = rect.left + w - 1;
		rect.bottom = rect.top + h - 1;
		ddgsImageDrawGrey(curdg, vdata, &rect);

		// Copy the section of the ddgs DIB image
		// buffer to the dc. Note the destination
		// y coordinate is upside-down to match
		// the upside-down (top-down) DIB.
		ptr = (BYTE *)GlobalLock(curdg->himage);
		StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
			vx, vy - h, w, h, ptr, (BITMAPINFO *)&curdg->binfo,
			DIB_RGB_COLORS, SRCINVERT);
		GlobalUnlock(curdg->himage);
#else
		// XOR the image on the display, with zero
		// in the background (to leave the area outside
		// objects unchanged by XOR).
		curdg->binfo.col[0].rgbRed = 0;
		curdg->binfo.col[0].rgbGreen = 0;
		curdg->binfo.col[0].rgbBlue = 0;

		StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
			0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
			DIB_RGB_COLORS, SRCINVERT);
#endif

		break;

	case MARKIM:

		// Keep a copy of the binfo colour table.
		memcpy(keep_col, curdg->binfo.col, 256*sizeof(RGBQUAD));

		// Reset the binfo colour table so that
		// all entries are the marker color -
		// except zero (background).
		curdg->binfo.col[0].rgbRed = 0;
		curdg->binfo.col[0].rgbGreen = 0;
		curdg->binfo.col[0].rgbBlue = 0;
		curdg->binfo.col[1].rgbRed = GetRValue(curdg->markcol[curdg->cur_marker]);
		curdg->binfo.col[1].rgbGreen = GetGValue(curdg->markcol[curdg->cur_marker]);
		curdg->binfo.col[1].rgbBlue = GetBValue(curdg->markcol[curdg->cur_marker]);
		for (i = 2; i < 256; i++)
			curdg->binfo.col[i] = curdg->binfo.col[i - 1];

#ifdef DDGSMASKED
		// Operation is different depending on
		// image background:
		//	Black bg - AND with inverted mask then OR with image.
		//	White bg - OR with mask then AND with image.
		if ((curdg->binfo.col[0].rgbRed == 0) &&
			(curdg->binfo.col[0].rgbGreen == 0) &&
			(curdg->binfo.col[0].rgbBlue == 0))
		{
			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->minfo,
				DIB_RGB_COLORS, SRCAND);

			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCPAINT);
		}
		else
		{
			curdg->binfo.col[0].rgbRed = 255;
			curdg->binfo.col[0].rgbGreen = 255;
			curdg->binfo.col[0].rgbBlue = 255;

			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->minfo,
				DIB_RGB_COLORS, MERGEPAINT);

			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCAND);
		}
#else
		// If fluorescent, we OR the image onto
		// the black background. Otherwise, its
		// brightfield, and we AND the image with
		// the white background.
		if ((curdg->binfo.col[0].rgbRed == 0) &&
			(curdg->binfo.col[0].rgbGreen == 0) &&
			(curdg->binfo.col[0].rgbBlue == 0))
			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCPAINT);
		else
			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCAND);
#endif

		// Put the mask back to its normal condition
		memcpy(curdg->binfo.col, keep_col, 256*sizeof(RGBQUAD));
		break;
	}

	DOG_RDC(curdg);
} /* fastpixobj */

#endif

