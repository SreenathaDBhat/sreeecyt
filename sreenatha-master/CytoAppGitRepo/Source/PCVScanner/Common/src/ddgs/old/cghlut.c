/*
 *	ddgslut.c	: Mark Gregson 11/8/92
 *
 *	X-Windows creation and loading of output lookup tables
 *
 *	Modifications :
 *
 *	27 Mar 1993	MG	llut, nlut, pseudolut now use load_lut 
 *	19 Jan 1993	MG	Stop VGA calling XStoreColors
 */

#include "ddgsdefs.h"


/*
 *	S E T R G B  --  Set up the RGB components of the colour selected in the current
 *			 ddgswindow
 *
 */
#ifdef WIN32
void
setrgb(int col, int r, int g, int b)
#endif
#ifdef i386
void
setrgb(col, r, g, b)
int col;
int r;
int g;
int b;
#endif
{
	int		i, j;
	XColor		lutval;

	if (cmap_allocated) {
		/* set desired colour cell */
		lutval.pixel=64 + col/4;
		lutval.flags= DoRed | DoGreen | DoBlue;
		lutval.red = (short)(r * 256);
		lutval.green = (short)(g * 256);
		lutval.blue = (short)(b * 256);

		/* update ddgs clut values */
		curdg->clut.red_lut[col]=r;
		curdg->clut.green_lut[col]=g;
		curdg->clut.blue_lut[col]=b;

		/* store lutval into colormap */
		XStoreColor( 	curdg->display,
				cmap,
				&lutval );
	
	}
	else fprintf(stderr,"Cannot setrgb - no cmap allocated");
}


/*
 * 	L O A D _ L U T
 *
 * 	Loads the output look up tables with the RGB lut belonging to a particular 
 *      ddgs structure 
 *
 */
#ifdef WIN32
void
load_lut(DDGS *dg)
#endif
#ifdef i386
void
load_lut(dg)
DDGS *dg;
#endif
{
	int		i, j;
	XColor		lutval[64];

	/* check colour map exists */
	if (cmap_allocated) {
		for (i=0; i<64; i++) {

			/* set colour cells to values held in ddgs structure */
			lutval[i].pixel=64 + i;
			lutval[i].flags= DoRed | DoGreen | DoBlue;
			lutval[i].red = (short)(dg->clut.red_lut[i*4] * 256);
			lutval[i].green = (short)(dg->clut.green_lut[i*4] * 256);
			lutval[i].blue = (short)(dg->clut.blue_lut[i*4] * 256);
		}
#ifdef VGA
		/* VGA cannot call XStoreColors */
		for (i=0; i<64; i++) {
			/* store lutval into colormap */
			XStoreColor( 	dg->display,
					cmap,
					&lutval[i] );
		}
#else
		XStoreColors( 	dg->display,
				cmap,
				lutval,
				64 );
#endif

	}
	else fprintf(stderr,"Cannot load_lut - no cmap allocated");

}


/*
 *      A P P L Y _ L U T -- Apply a given LUT to the current ddgs framestore.
 *      Takes account of the invertedness of the framestore.
 *      It is the responsibility of the caller to take account of
 *      any overlay planes, or colours in use.
 */
#ifdef WIN32
void
apply_lut(struct look_up_table *lut)
#endif
#ifdef i386
void
apply_lut(lut)
struct look_up_table *lut;
#endif
{
        register unsigned char *rl, *gl, *bl;
        register int i;

        rl = lut->red_lut;
        gl = lut->green_lut;
        bl = lut->blue_lut;

        if (curdg->invert) {
                for (i=0; i<256; i++) {
			curdg->clut.red_lut[i]=255-(int)*rl++;
 			curdg->clut.green_lut[i]=255-(int)*gl++;
 			curdg->clut.blue_lut[i]=255-(int)*bl++;
		}
	} else {
		curdg->clut=*lut;
        }
	load_lut(curdg);
}


/*
 *	L L U T  --  Create a linear lut (white = 0, black = 255) for the current ddgs window
 * 		     Note: only 64 grey levels displayable on SVGA ( 256 on Primagraphics )
 *
 */
#ifdef WIN32
void
llut()
#endif
#ifdef i386
void
llut()
#endif
{
	register int i,j;

	if (cmap_allocated) {
		for (i=0; i<256; i++) {
			j = 255 - i;
			curdg->clut.red_lut[i]=j;
			curdg->clut.green_lut[i]=j;
			curdg->clut.blue_lut[i]=j;
		}
		load_lut(curdg);
	}
	else fprintf(stderr,"Cannot llut - no cmap allocated");
}


/*
 *
 * N L U T 
 *
 * load normal grey level look up table
 */
#ifdef WIN32
void
nlut()
#endif
#ifdef i386
void
nlut()
#endif
{
	register int i;

	if (cmap_allocated) {
		for (i=0; i<256; i++) {
			curdg->clut.red_lut[i]=i;
			curdg->clut.green_lut[i]=i;
			curdg->clut.blue_lut[i]=i;
		}
		load_lut(curdg);
	}
	else fprintf(stderr,"Cannot nlut - no cmap allocated");
}

#ifdef WIN32
void
set_lut_range(colour_lut *clut, int first, int last, char R, char G, char B)
#endif
#ifdef i386
void
set_lut_range(clut, first, last, R, G, B)
colour_lut *clut;
int first;
int last;
char R;
char G;
char B;
#endif
{
	int i;

	for (i=first; i<last; i++) {
		clut->red_lut[i]=R;
		clut->green_lut[i]=G;
		clut->blue_lut[i]=B;
	}
}

/*
 *
 * P S E U D O L U T
 *
 * load pseudocolour look up table
 */
#ifdef WIN32
void
pseudolut()
#endif
#ifdef i386
void
pseudolut()
#endif
{
	register int i,k;

	if (cmap_allocated) {

		set_lut_range(&curdg->clut, 0,   1,   0,   0,   0  );	/* black backround */
		set_lut_range(&curdg->clut, 1,   6,   255, 0,   0  );
		set_lut_range(&curdg->clut, 6,   50,  192, 0,   0  );
		set_lut_range(&curdg->clut, 50,  83,  128, 0,   0  );
		set_lut_range(&curdg->clut, 83,  96,  128, 0,   128);
		set_lut_range(&curdg->clut, 96,  160, 0,   0,   128);
		set_lut_range(&curdg->clut, 160, 173, 0,   128, 128);
		set_lut_range(&curdg->clut, 173, 205, 0,   128, 0  );
		set_lut_range(&curdg->clut, 205, 250, 0,   192, 0  );
		set_lut_range(&curdg->clut, 250, 255, 0,   255, 0  );

		load_lut(curdg);
	}
	else fprintf(stderr,"Cannot pseudolut - no cmap allocated");
}



/*
 *
 *  P S E U D O D G
 *
 * load pseudocolour look up table into colours 192 to 255
 * and fill overlay plane causes all pixels to appear in pseudocolour
 */
#ifdef WIN32
void
pseudodg(int brightfield)
#endif
#ifdef i386
void
pseudodg(brightfield)
int brightfield;
#endif
{
	register int i,j,k;
	colour_lut clut;
	XColor	lutval[64];


	if (cmap_allocated) {
		ddgsoverlay(OFF);

		set_lut_range(&clut, 0,   1,   0,   0,   0  );	/* black backround */
		set_lut_range(&clut, 1,   6,   255, 0,   0  );
		set_lut_range(&clut, 6,   50,  192, 0,   0  );
		set_lut_range(&clut, 50,  83,  128, 0,   0  );
		set_lut_range(&clut, 83,  96,  128, 0,   128);
		set_lut_range(&clut, 96,  160, 0,   0,   128);
		set_lut_range(&clut, 160, 173, 0,   128, 128);
		set_lut_range(&clut, 173, 205, 0,   128, 0  );
		set_lut_range(&clut, 205, 250, 0,   192, 0  );
		set_lut_range(&clut, 250, 255, 0,   255, 0  );


		for (i=0; i<64; i++) {

			/* set colour cells to values held in ddgs structure */
			lutval[i].pixel=192 + i;
			lutval[i].flags= DoRed | DoGreen | DoBlue;
			lutval[i].red = (short)(clut.red_lut[i*4] * 256);
			lutval[i].green = (short)(clut.green_lut[i*4] * 256);
			lutval[i].blue = (short)(clut.blue_lut[i*4] * 256);
		}
#ifdef VGA
		/* VGA cannot call XStoreColors */
		for (i=0; i<64; i++) {
			/* store lutval into colormap */
			XStoreColor( 	curdg->display,
					cmap,
					&lutval[i] );
		}
#else
		XStoreColors( 	curdg->display,
				cmap,
				lutval,
				64 );
#endif



		curdg->pseudocolour=ON;	/* pseudocolour display on */

		oclear();

	}
	else fprintf(stderr,"Cannot pseudolut - no cmap allocated");
}


/*
 * 	M O N O D G
 *
 *	switch pseudocolour display off
 */
#ifdef WIN32
void
monodg()
#endif
#ifdef i386
void
monodg()
#endif
{
	curdg->pseudocolour=OFF;	/* pseudocolour display off */
	oclear();
	ddgsoverlay(ON);
}




/* 
 * 	S A V E _ C O L O U R S
 *
 *	save current ddgs grey table, marker colours and overlay colour to file
 */
#ifdef WIN32
int
save_colours(FILE *f)
#endif
#ifdef i386
int
save_colours(f)
FILE *f;
#endif
{

	if (fwrite(&curdg->clut,sizeof(colour_lut),1,f) != 1) {
		fprintf(stderr,"save_colours failed writing curdg->clut\n");
		return(-1);
	}

	if (fwrite(curdg->markcol,sizeof(rgbcol),MAXMARKER,f) != MAXMARKER) {
		fprintf(stderr,"save_colours failed writing curdg->markcol[]\n");
		return(-1);
	}
	if (fwrite(&curdg->ovlycol,sizeof(rgbcol),1,f) != 1) {
		fprintf(stderr,"save_colours failed writing curdg->ovlycol\n");
		return(-1);
	}

	return(0);	/* save colours successful */
}




/* 
 * 	L O A D _ C O L O U R S
 *
 *	load grey table, marker colours and overlay colour from file to curdg
 */
#ifdef WIN32
int
load_colours(FILE *f)
#endif
#ifdef i386
int
load_colours(f)
FILE *f;
#endif
{

	if (fread(&curdg->clut,sizeof(colour_lut),1,f) != 1) {
		fprintf(stderr,"load_colours failed reading curdg->clut\n");
		return(-1);
	}

	if (fread(curdg->markcol,sizeof(rgbcol),MAXMARKER,f) != MAXMARKER) {
		fprintf(stderr,"load_colours failed reading curdg->markcol[]\n");
		return(-1);
	}

	if (fread(&curdg->ovlycol,sizeof(rgbcol),1,f) != 1) {
		fprintf(stderr,"load_colours failed reading curdg->ovlycol\n");
		return(-1);
	}

	return(0);	/* save colours successful */
}






