
/*
 *	ddgsmouse.c :	Mark Gregson
 *			18th August 1992
 *	
 *	Routine for drawing shapes ( points, lines, polygons, circles, rectangles )
 *	using the mouse under X Windows. The main routine mousedraw activates
 * 	an event handler mousedrawEH to draw a given shape on a particular drawing 
 *	area widget. After activating the event handler it dispatches events to
 *	it until drawing terminates. All drawing takes place in the overlay
 *	plane and is removed on completion of the drawing. Since the drawing
 * 	is intentionally a short term thing there is no attempt made to
 * 	backup the drawing in the current DDGS structure pixmap. The points drawn
 * 	by mousedraw are maintained in dg->mousepts - these can be examined
 * 	by external routines to generate woolz objects. All drawing is commenced
 * 	by clicking the left button and terminated by clicking the right button.
 *	POLYLINE intermediate points are marked by clicking the left button.
 *	POLYLINE backup occurs if the middle button is pressed.
 *	POINT drawing is started and terminated ( in one go ) by pressing any button.
 * 
 *
 *	Modifications:
 *
 *	14 Sep 1993	MG	Removed BP's duff backup fix - did it properly
 *	 1 Sep 93	BP	Stopped doing XDrawLines() if number of lines is
 *				less than 2 in the (probably vain) hope that this
 *				might prevent backing-up of freehand line going pop.
 *				Now does XDrawLine().
 *	 1 Feb 1993	MG	added POINT drawing to mousedraw
 *	29 Jan 1993	MG	allow user to create single point line or polyline
 *	26 Jan 1993	MG	removed window constraint and changed to 3 buttons
 *	17 Sep 1992	MG	get_mousept added
 *
 */


#include <math.h>
#include "ddgsdefs.h"
#include <X11/cursorfont.h>
#include <X11/Intrinsic.h>

extern XtAppContext app_context;

/* global data  */
int maxmousepts;
int drawing=1;
int pt_count;

static int grabstate=GrabSuccess;

/* crosshair drawing cursor */
Cursor drawcursor;

/********************************************************************************/

void checkpoints(pt1,pt2)	/* swap rectangle points if necessary */
XPoint *pt1,*pt2;
{
	short temp;

	if (pt1->x > pt2->x) {
		temp=pt1->x;
		pt1->x=pt2->x;
		pt2->x=temp;
	}
	if (pt1->y > pt2->y) {
		temp=pt1->y;
		pt1->y=pt2->y;
		pt2->y=temp;
	}
	
}


void drawrect(dg,pta,ptb)	/* XOR draw a rectangle */
DDGS *dg;
XPoint pta,ptb;
{	
	checkpoints(&pta,&ptb);	/* make pta top left, ptb bottom right */

	XDrawRectangle(	dg->display, 
	   		dg->window, 
	   		dg->gc,
	   		pta.x,
	   		pta.y,
	   		(ptb.x-pta.x)+1,
	   		(ptb.y-pta.y)+1 );

}

/********************************************************************************/

void Xdrawcircle(dg,ctr,radius)	/* XOR draw a circle */
DDGS *dg;
XPoint ctr;
int radius;
{	
	XDrawArc(	dg->display, 
	   		dg->window, 
	   		dg->gc,
	   		ctr.x-radius,
	   		ctr.y-radius,
	   		2*radius,
	   		2*radius,
			0,
			360*64 );

/*	Line equivalent: scruffy on small objects - easily optimised / tidied

	XPoint	xp[41];
	int	i;
	double 	ang=9.0*3.141596/180.0;

	for (i=0; i<40; i++) {
		xp[i].y=ctr.y+(short)( (double) radius * sin((double)i*ang) );
		xp[i].x=ctr.x+(short)( (double) radius * cos((double)i*ang) );
	}
	xp[40]=xp[0];

	XDrawLines(	dg->display, 
	   		dg->window, 
	   		dg->gc,
	   		xp,
			41,
			CoordModeOrigin);
*/
}

/********************************************************************************/

void mousedrawEH(w,dg,event)	/* mouse object drawing event handler */

Widget w;
DDGS *dg;
XEvent *event;

{ 
	static int oldradius,firstgo;
	static XPoint oldpt;
	int i,x,y,k,radius;
	unsigned int Button;
	XPoint *xpt;
	int npts,dopts;
	

	if (event->type == ButtonRelease) {

		Button=event->xbutton.button;

		/* if drawing single point terminate drawing on any button press */
		if (dg->shape==POINT)
			Button=Button3;		


		switch (Button) {

		default:
		case Button1: /* left button pressed */

			if (dg->nmousepts==0) {	/* start drawing */
				/* switch overlay plane on if required */
				if (dg->ovlystate==OFF)
					ddgsoverlay(ON);

				/* set up planemask and pixel colour to draw in overlay plane */
				XSetPlaneMask(	dg->display,
						dg->gc,
						128 );

				XSetForeground(	dg->display,
						dg->gc,
						128 );

				if (dg->shape==LINE)
					XSetFunction(dg->display, dg->gc, GXcopy);
				else
					XSetFunction(dg->display, dg->gc, GXxor);
				
			      	x=event->xbutton.x;
			      	y=event->xbutton.y;

				x= (x > dg->maxx ) ? dg->maxx : x;
				x= (x < dg->minx ) ? dg->minx : x;
				y= (y > dg->maxy ) ? dg->maxy : y;
				y= (y < dg->miny ) ? dg->miny : y;

				dg->mousepts[0].x=x;
  	         	 	dg->mousepts[0].y=y;
				pt_count=1;
				dg->nmousepts=1;
				oldpt=dg->mousepts[0];
				oldradius=1;
				firstgo=TRUE;
			} /* start drawing handled */
			else
			{ /* if polyline - left button can be pressed during drawing */
				if ((dg->shape==POLYLINE) && (!firstgo)) {
					/* redraw last line to fill end-points
					   left out by GXxor mode */
					XSetFunction(dg->display, dg->gc, GXcopy);
						
					XDrawLine(dg->display,dg->window,dg->gc,
						  dg->mousepts[pt_count-1].x,
						  dg->mousepts[pt_count-1].y,
						  dg->mousepts[pt_count].x,
						  dg->mousepts[pt_count].y);

					XSetFunction(dg->display, dg->gc, GXxor);

					/* and prepare for next line */
					firstgo=TRUE;
 			 	        pt_count++;
					if (pt_count == maxmousepts ) {
						maxmousepts=maxmousepts+100;
						dg->mousepts=(XPoint *) realloc(dg->mousepts, sizeof(XPoint)*maxmousepts);
						if (dg->mousepts==NULL) {
							fprintf(stderr,"mousedraw allocation failure \n");
							exit(1);
						}
					}
			    	} /* shape = POLYLINE */
			}

			break;	 /* button1 press handled */

		case Button2:
			/* if polyline or line - middle button can be pressed during drawing to back up */
			if ((dg->nmousepts !=0 ) && (pt_count > 1)) {

				if (dg->shape==POLYLINE) {

					XSetFunction(dg->display, dg->gc, GXclear);

					/* erase current line */
					XDrawLine(dg->display,dg->window,dg->gc,
					  	dg->mousepts[pt_count-1].x,
						  dg->mousepts[pt_count-1].y,
						  dg->mousepts[pt_count].x,
						  dg->mousepts[pt_count].y);

					/* erase last line */
					XDrawLine(dg->display,dg->window,dg->gc,
						  dg->mousepts[pt_count-2].x,
						  dg->mousepts[pt_count-2].y,
						  dg->mousepts[pt_count-1].x,
						  dg->mousepts[pt_count-1].y);

					XSetFunction(dg->display, dg->gc, GXxor);

					/* move pointer very slightly to force motion events */	   
					XWarpPointer(dg->display,None,None,1,1,1,1,1,1);
	
					/* and back up - decrement pt_count */
					pt_count--;

					/* and prepare for next line */
					firstgo=TRUE;
	
			    	} /* shape = POLYLINE */

				if (dg->shape==LINE) {
				
					XSetFunction(dg->display, dg->gc, GXclear);

					for (k=0; k<5; k++) {
						if (pt_count == 1 )
							break;

						/* back up - decrement pt_count */
						pt_count--;

						/* erase line */
						XDrawLine(dg->display,dg->window,dg->gc,
					 		dg->mousepts[pt_count-1].x,
					 		dg->mousepts[pt_count-1].y,
					 		dg->mousepts[pt_count].x,
					  		dg->mousepts[pt_count].y);

						XWarpPointer(dg->display,None,dg->window,1,1,1,1,
								dg->mousepts[pt_count-1].x,
					 			dg->mousepts[pt_count-1].y	);
					}
					oldpt=dg->mousepts[pt_count-1];

					XSetFunction(dg->display, dg->gc, GXcopy);

					/* redraw current shape to fill any gaps caused by erase */
					if (pt_count > 1) {
						npts=pt_count;
						xpt=dg->mousepts;
						while (npts>1) {

							/* check number of points is not greater than
						  	 the maximum allowed by the X server */

							if (npts > 15 ) 	/* temp fix should be servermaxpts */
								dopts=15; 	/* again should be servermaxpts */
							else 
								dopts=npts;

							npts=npts-dopts+1;
		
							XDrawLines(dg->display,
								dg->window,
								dg->gc,
								xpt,
								dopts,
								CoordModeOrigin );

							xpt+=(dopts-1);
						}
					} /* end redraw */
				}
			}
		
			break;	/* button2 press handled */


		case Button3:	/* right hand button pressed - terminate drawing */
			if (dg->nmousepts==0) {	/* single point object */
			      	x=event->xbutton.x;
			      	y=event->xbutton.y;

				x= (x > dg->maxx ) ? dg->maxx : x;
				x= (x < dg->minx ) ? dg->minx : x;
				y= (y > dg->maxy ) ? dg->maxy : y;
				y= (y < dg->miny ) ? dg->miny : y;

				dg->mousepts[0].x=x;
  	         	 	dg->mousepts[0].y=y;
				pt_count=1;
				dg->nmousepts=1;
				oldpt=dg->mousepts[0];
				oldradius=1;
				firstgo=TRUE;

				switch (dg->shape) {

				case RECTANGLE:
					dg->mousepts[1].x=x+1;
  	         	 		dg->mousepts[1].y=y+1;
					break;
				case CIRCLE:
					dg->mousepts[1].x=x+1;
  	         	 		dg->mousepts[1].y=y;
					break;

				default:
					break;
				}
			} /* nmousepts=0 */

			/* terminate drawing */	

			switch (dg->shape) 

			{

			case POLYLINE:
				if (!firstgo)
					pt_count++;	/* to grab the last point */
			case LINE :
				
				XSetFunction(dg->display, dg->gc, GXclear);

				/* complete dg structure, and reduce memory allocation
				   to just sufficient to hold the mouse points */
				dg->nmousepts=pt_count;
				dg->mousepts=(XPoint *) realloc(dg->mousepts, sizeof(XPoint)*pt_count);

				/* erase lines - do not use XDrawLines since it
				   may clear different pixels */
				for (i=0; i<dg->nmousepts-1; i++) {
					XDrawLine(	dg->display, 
					   		dg->window, 
					   		dg->gc,
				   			dg->mousepts[i].x,
							dg->mousepts[i].y,
							dg->mousepts[i+1].x,
							dg->mousepts[i+1].y);
				}
				break;

			case RECTANGLE :
				checkpoints(&dg->mousepts[0], &dg->mousepts[1]);
				if (!firstgo)
					drawrect(dg,dg->mousepts[0],dg->mousepts[1]);
				dg->nmousepts=2;
				break;

			case CIRCLE:
				if (!firstgo)
					Xdrawcircle(dg,dg->mousepts[0],oldradius);
				dg->nmousepts=2;
				break;


			default:
			case POINT:
			 	break;
			}


			/* restore gc drawing mode, colour and plane mask*/
			intens(dg->cur_intensity);

			colour(dg->cur_colour);	

			XSetPlaneMask(	dg->display,
					dg->gc,
					PLANEMASK );

			/* remove mousobj event handler */
			XtRemoveEventHandler(dg->displaywidget,PointerMotionMask |
				ButtonReleaseMask,FALSE,mousedrawEH,(XtPointer) dg);

			/* unconstrain pointer unless grabbed prior to mousedraw
			   in which case it is down to the calling routine to
			   release the grab */
			if (grabstate!=AlreadyGrabbed)
				XUngrabPointer(dg->display, CurrentTime);

			/* discard crosshair drawing cursor */
			XFreeCursor( dg->display, drawcursor);

			/* set drawing flag - marks end of drawing */
			drawing=1;
			
			/* reset point count */
			pt_count=0;
 
			break;	/* button3 press handled */

		}/* button switch */
	}
	else /* motion event */
	{
		if (dg->nmousepts>0) {	/* drawing */
		      	x=event->xmotion.x;
		      	y=event->xmotion.y;

			x= (x > dg->maxx ) ? dg->maxx : x;
			x= (x < dg->minx ) ? dg->minx : x;
			y= (y > dg->maxy ) ? dg->maxy : y;
			y= (y < dg->miny ) ? dg->miny : y;

			if (dg->shape==CIRCLE) { /* constrain inside window */
				radius=(int) sqrt( (double) ( (x-dg->mousepts[0].x)*(x-dg->mousepts[0].x) +
						   (y-dg->mousepts[0].y)*(y-dg->mousepts[0].y) ) );

				if (dg->mousepts[0].x+radius > dg->maxx ) {
					x=dg->maxx; radius=x-dg->mousepts[0].x;
				}
				if (dg->mousepts[0].x-radius < dg->minx ) {
					x=dg->minx; radius=dg->mousepts[0].x-x;
				}
				if (dg->mousepts[0].y+radius > dg->maxy ) {
					y=dg->maxy; radius=y-dg->mousepts[0].y;
				}					
				if (dg->mousepts[0].y-radius < dg->miny ) {
					y=dg->miny; radius=dg->mousepts[0].y-y;
				}
			}

			if ( (x!=oldpt.x) | (y!=oldpt.y) ) {
		                dg->mousepts[pt_count].x=x;
  		          	dg->mousepts[pt_count].y=y;

				switch (dg->shape) 

				{
					case LINE:               		
						XDrawLine(dg->display,dg->window,
			   				  dg->gc,oldpt.x,oldpt.y,x,y);
						break;

					case POLYLINE:               		
						XDrawLine(dg->display,dg->window,dg->gc,
							  dg->mousepts[pt_count-1].x,
							  dg->mousepts[pt_count-1].y,
							  x,y);
						if (!firstgo)
							XDrawLine(dg->display,dg->window,dg->gc,
								  dg->mousepts[pt_count-1].x,
								  dg->mousepts[pt_count-1].y,
								  oldpt.x,oldpt.y);
						firstgo=FALSE;
						break;

					case RECTANGLE:
						drawrect(dg,dg->mousepts[0],dg->mousepts[1]);
						if (!firstgo)
							drawrect(dg,dg->mousepts[0],oldpt);
						firstgo=FALSE;
						break;

					case CIRCLE:
						Xdrawcircle(dg,dg->mousepts[0],radius);
						if (!firstgo)
							Xdrawcircle(dg,dg->mousepts[0],oldradius);
						firstgo=FALSE;
						oldradius=radius;
						break;
					default:
						break;
					
				} /* case */

			        oldpt.x=x;
   	 		    	oldpt.y=y;
	
				if (dg->shape==LINE) {
 			 	        pt_count++;
					if (pt_count == maxmousepts ) {
						maxmousepts=maxmousepts+100;
						dg->mousepts=(XPoint *) realloc(dg->mousepts, sizeof(XPoint)*maxmousepts);
						if (dg->mousepts==NULL) {
							fprintf(stderr,"mousedraw allocation failure \n");
							exit(1);
						}
					}
			    	} /* shape = LINE */
			} /* new point */
		} /* drawing */
	} /* motion event handled */

} /* mousedraw */

/********************************************************************************/

#ifdef WIN32
void
mousedraw(int shape, DDGS *dg)
#endif
#ifdef i386
void
mousedraw(shape, dg)
int shape;
DDGS *dg;
#endif
{
	XEvent	event;
	int res;

	/* all mouse ops performed on this dg */
	selectddgs(dg);

	/* set shape to draw, and allocate storage for mousepts */
	dg->shape=shape;
	dg->nmousepts=0;

	maxmousepts=100;

	switch (dg->shape) {
		case LINE:
		case POLYLINE:	
			dg->mousepts=(XPoint *) malloc(sizeof(XPoint)*maxmousepts);
			break;

		case CIRCLE:
		case RECTANGLE: 
			dg->mousepts=(XPoint *) malloc(sizeof(XPoint)*2);
			break;

		default:
		case POINT:
			dg->mousepts=(XPoint *) malloc(sizeof(XPoint)*1);
			break;
	}

	/* activate mouse drawing event handler - put at front of list
	   to make sure it happens before events such as bdrawEH in 
	   manualsplit.c in karylib - not mega important this but makes
	   things look smoother */
	XtInsertEventHandler(dg->displaywidget,PointerMotionMask |
			  ButtonReleaseMask,FALSE,mousedrawEH,(XtPointer) dg, XtListHead );

	/* create crosshair drawing cursor */
	drawcursor=XCreateFontCursor( dg->display, XC_crosshair);

	/* grab pointer so all mouse events reported to drawing area widget only.
	   If grabstate=AlreadyGrabbed then mousedraw will not release the grab */

	grabstate=XGrabPointer(dg->display, dg->window, False,
			PointerMotionMask | ButtonReleaseMask,
			GrabModeAsync, GrabModeAsync, 
			None, drawcursor, CurrentTime);


	drawing=0;	/* global flag - 0 = active */

	do {
		XNextEvent(dg->display,&event);
		XtDispatchEvent(&event);
	} while (drawing == 0);

	/* event loop */
/*	RecorderSubLoop(dg->display,
		XtWidgetToApplicationContext(dg->displaywidget), &drawing);
*/
	XFlush(dg->display);
}


/********************************************************************************/

/*
 *	GET_MOUSEPT
 *
 *	Returns current mouse position relative to ddgs window in ddgs coordinates.
 *	Buttons = 1=left, 2=right, 3=both, 0=neither pressed
 *	If the mouse is outside the ddgs window then get_mousept returns False.
 */
#ifdef WIN32
Bool
get_mousept(int *X, int *Y, short *buttons, DDGS *dg)
#endif
#ifdef i386
Bool
get_mousept(X, Y, buttons, dg)
int *X;
int *Y;
short *buttons;
DDGS *dg;
#endif
{
	Window root,child;
	int rootx,rooty,winx,winy;
	unsigned int keys;
	Bool in_window;

	in_window=XQueryPointer(dg->display,
				dg->window,
				&root,
				&child,
				&rootx,
				&rooty,
				&winx,
				&winy,
				&keys );

	if ((winx<dg->minx) || (winx>dg->maxx) || (winy<dg->miny) || (winy>dg->maxy))
	{
		in_window=False;
		*X=0;
		*Y=0;
	} 
	else 
	{
		*X=winx << dg->xshift;
		*Y=(dg->maxy-winy) << dg->yshift;
	}
	*buttons=0;
	if (keys & Button1Mask) *buttons|=1;
	if (keys & Button2Mask) *buttons|=2;
	return(in_window);

}

/********************************************************************************/

			
