/*
 *      D D G S . H	 --  external ddgs defintions
 *
 *
 *  Written: Graham J Page
 *           Image Recognition Systems
 *           720 Birchwood Boulevard
 *           Birchwood Science Park
 *           Warrington
 *           Cheshire
 *           WA3 7PX
 *
 * Copyright (c) and intellectual property rights Image Recognition Systems
 * Copyright etc Applied Imaging 1995, 1996
 *
 * Date:    10th June 1987
 *
 * Mods:
 *
 *	18Mar97	BP:	Add mask BITMAPINFO to DDGS structure.
 *	17Mar97	BP:	More "normal" BITMAPINFO structure.
 *				Remove UNIX fonts.
 *
 *	9/26/96	BP:		Added DDGSPoint for platform independence (is
 *						used in woolz).
 *	9/19/96	BP:		New Windows version.
 *
 *	BP	7/20/95:	Added ddgspropen_8bit, and some tidying (needs more!)
 *	BP	9/14/94:	Added depth and visual to DDGS structure for 24-bit.
 *
 *	 4 Mar 1993		MG		added invert_clear(), invert_intens()
 *	 1 Feb 1993		MG		added POINT to mousedraw() shapes
 *	18 Dec 1992		MG		added ddgspropen()
 *	16 Dec 1992		MG		added ddgsismapped()
 *	17 Sep 1992		MG		get_mousept added
 *	 3 Sep 1992		MG		text routines added
 *	 2 Sep 1992		MG		destuctive markers
 *	11 Aug 1992		MG		mouse drawing stuff
 *	 5 Aug 1992		MG		X overlay plane stuff added
 *	31 Jul 1992		MG		X-Windows additions to ddgs struct
 *	11 Jun 1992 	MG		New X Windows version
 *	 4 Apr 1990		CAS		Declare ddgsopen + dup
 *	30 Jan 1990		dcb		Added look up table structure
 *	 3 Mar 1989		CAS		Added a name to DDGS structure
 *	 4 Feb 1988		CAS		Added lutcopy field
 *	 2 Sep 1987		CAS		Added devno field
 * 	12 Jun 1987		GJP		Added center of display coordinates
 */


#ifdef WIN32


#ifndef DDGS_H
#define DDGS_H


#include <stdio.h>
#include <windows.h>


#define ON 			1		/* overlay state */
#define OFF 		0
#define MAXMARKER	16		/* number of destructive marker colours */

/* Increased this from 2048 to accomodate
 * extreme case of 768x4(zoom)x4(24-bit). */
#define MAXDATALENGTH	12288
#define PLANEMASK	63		/* masks grey image planes, not overlays */

/* ddgs mousedraw shapes */
#define DDGSPOINT		1
#define DDGSLINE		2
#define DDGSPOLYLINE	3
#define DDGSRECTANGLE	4
#define DDGSCIRCLE		5

/* ddgs fonts 0-3 normal, 4-7 bold */
#define MAXFONT		8

 
/*
 *	Look up table structure
 */
typedef struct {
	WORD version;
	WORD numEntries;
	PALETTEENTRY val[256];
} DDGSLOGPALETTE;


/*
 *	Point structure.
 * Note that this is defined the same as
 * the windows POINT structure so can be
 * cast as such.
 */
typedef struct {
	LONG x;
	LONG y;
} DDGSPoint;


/*
 *	Sensible version of BITMAPINFO
 */
typedef struct {
	BITMAPINFOHEADER head;
	RGBQUAD col[256];	// Defines linear of invert grey mapping.
} DDGSBITMAPINFO;


/*
 * Packed (3-byte) RGB triplet.
 */
typedef struct {
	BYTE b, g, r;
} DDGSRGB;


/*
 *	DDGS structure
 */
typedef struct ddgs_control {
	int pseudocolour;		/* 1=pseudocolour display, 0=monochrome */
	int minx;				/* Physical frame store pixel minx */
	int maxx;				/* Physical frame store pixel maxx */
	int miny;				/* Physical frame store pixel miny */
	int maxy;				/* Physical frame store pixel maxy */

	int xshift;					/* Scaling - always 3! */
	int yshift;

	HWND      	window;			/* Window handle */
	HDC 		dc;				/* Device context id */
	HBITMAP		map;			/* window backup bitmap */
	HRGN		cliprgn;		/* clip rectangle defined by window */
	int 		cur_colour;		/* current colour */
	int			cur_intensity;	/* current intensity */
	int 		cur_pixelmode;	/* current pixel mode */
	int 		cur_x;			/* DDGS current x/y pos */
	int 		cur_y;
	BOOL 		invert;
	BOOL		backup;			/* True => ddgs ops backed up to pixmap */
	COLORREF	ovlycol;		/* overlay plane colour */
	HPEN		ovlypen;
	HPEN		blackpen;
	HPEN		whitepen;
	COLORREF	markcol[MAXMARKER];	/* RGB colours for destructive markers */
	HPEN		markpen[MAXMARKER];
	int			cur_marker;		/* current destructive marker */
	int			cur_lwidth;
	int			shape;			/* mouse drawing shape */
	int			nmousepts;		/* num of mouse pts drawn */
	DDGSPoint	*mousepts;		/* ptr to list of mouse pts */
	DDGSBITMAPINFO binfo;		/* DIB drawing structure */
	DDGSBITMAPINFO minfo;		/* DIB mask structure */
	HFONT		font;			/* Currently selected font. */
	HANDLE		himage;			/* DIB drawing buffer (only used with DDGSBUFFERED) */
	unsigned int  invertBG;
	unsigned int  normalBG;
} DDGS;


/*
 * DDGS Routines
 */

#ifdef __cplusplus
extern "C" {
#endif


/**** ddgs.c ****/

void ddgsInit();
void ddgsSetNormalBGColour(unsigned int col);
void ddgsSetInvertBGColour(unsigned int col);
void ddgsGetNormalBGColour(unsigned int *col);
void ddgsGetInvertBGColour(unsigned int *col);
DDGS *ddgsopen(HWND window, int width, int height);
DDGS *ddgspropen(HDC dc, int width, int height);
BOOL ddgsismapped();
void selectddgs(DDGS *dg);
void ddgsclose(DDGS *dg);
DDGS *ddgsdup(DDGS *dg);
DDGS *getcurdg();
void backup(int status);
void colour(int col);
void clear();
void invert_clear();
void erase(int x, int y, int w, int h);
void invert_erase(int x, int y, int w, int h);
void intens(int val);
void invert_intens();
void restore_intens();
void pseudodg();
void monodg();

// Event callbacks.
void ddgsExposeWindow(DDGS *dg);
void ddgsColormapChange();


/**** ddgsline.c ****/

void lwidth(int w);
void moveto(int x, int y);
void moveby(int x, int y);


/**** ddgspix.c ****/

unsigned char *ddgs_get_aux_buffer(int size);
unsigned char *ddgs_get_draw_buffer(int size);
unsigned char *ddgs_get_draw_mask(int w, int h);
void ddgs_free_buffers();

void window(int xl, int xr, int yb, int yt);
void setrgb(int col, int r, int g, int b);
void fastpixobj(int vx, int vy, int w, int h, unsigned char *vdata, unsigned char *mdata, short where);
void fastpixobj24(int vx, int vy, int w, int h, unsigned char *vdata, unsigned char *mdata);
int  pixelmode(int m);


/**** ddgstext.c ****/

void init_fonts();
void free_fonts();
void setfont(int fontnum);
void textsize(char *s, int *width, int *height);


/**** ddgsovly.c ****/

void ddgsoverlay(int state);
void oclear();
void osetrgb(int r, int g, int b);

void olineto(int x, int y);
void odrawpoly(DDGSPoint *xpt, int npts);
void odrawcircle(int x, int y, int r);
void olineby(int x, int y);
void obox(int x, int y);
void ofill(int x, int y, int width, int height);
void otext(int x, int y, char *s, short scale);

void mcolour(int marker);
void msetrgb(int marker, int r, int g, int b);
void load_markers(DDGS *dg);

void mlineto(int x, int y);
void mdrawpoly(DDGSPoint *xpt, int npts);
void mdrawcircle(int x, int y, int r);
void mlineby(int x, int y);
void mbox(int x, int y);
void mfill(int x, int y, int width, int height);
void mtext(int x, int y, char *s, short scale);

void lineto(int x, int y);
void drawpoly(DDGSPoint *xpt, int npts);
void drawcircle(int x, int y, int r);
void lineby(int x, int y);
void box(int x, int y);
void fill(int x, int y, int width, int height);
void text(int x, int y, char *s, short scale);


/**** ddgsmouse.c ****/

// Callbacks. Return TRUE if dogs is busy (eg
// drawing a shape).
BOOL ddgsMouseMove(DDGS *dg, long x, long y);
BOOL ddgsMouseRelease(DDGS *dg, long x, long y, short button);
BOOL ddgsMouseBusy(DDGS *dg);

// OK, same as UNIX/X version - return when finished.
void mousedraw(int shape, DDGS *dg);


/**** ddgsimage.c ****/

void ddgsImageDepth(DDGS *dg, short depth);
void ddgsImageClear(DDGS *dg, BYTE g);
void ddgsImageErase(DDGS *dg, BYTE g, RECT *rect);
void ddgsImageDrawGrey(DDGS *dg, BYTE *src, RECT *rect);
void ddgsImageDrawColor(DDGS *dg, BYTE *src, RECT *rect, DDGSRGB *lut, BOOL addmode);

// Palette definitions, so applications can use
// the ddgs palette.
extern HPALETTE ddgs_palette;
extern COLORREF ddgs_cornsilk;
extern COLORREF ddgs_darkslategrey;
extern COLORREF ddgs_lightslategrey;
extern COLORREF ddgs_banana;


#ifdef __cplusplus
}
#endif

#endif

#endif

