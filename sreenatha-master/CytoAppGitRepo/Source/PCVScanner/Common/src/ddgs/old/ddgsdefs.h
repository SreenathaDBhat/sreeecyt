
/*
 *	D D G S D E F S. H     X-windows/DDGS internal definitions
 *
 * 			Mark Gregson
 *			Applied Imaging
 *
 *	Date:		3rd August 1992
 *
 *	Modifications:
 *
 *	9/24/96	BP	Tweaked for windows.
 *
 *	9/15/94	BP	Macros added for 24-bit options.
 *	5 Aug 1992	MG	Overlay plane stuff added - ddgsovly
 *
 */


#ifndef	_DDGSDEFS_H
#define	_DDGSDEFS_H


#include <stdio.h>
#include <machdef.h>
#include <ddgs.h>



extern	DDGS *curdg;			/* current ddgs in use */
extern  HPALETTE ddgs_palette;	/* global cmap */


/* internal routines */
void cmapinit();
void ddgsExposeEventHandler(/* Widget w, XtPointer xtdg, XEvent event */);
void ddgsCmapEventHandler(/* Widget w, XtPointer xtdg, XEvent event */);
void ifill(/* int x, int y, int width, int height, int val */);
void drawrect(/* DDGS *dg, XPoint pta, XPoint ptb */);
void Xdrawcircle(/* DDGS *dg, XPoint ctr, int radius */);
void mousedraw(/* Widget w, DDGS *dg, XEvent event */);
void reloadgc();
void loadogc();



/******************* MACROS *****************/

/* Utility to check depth. */
//#define	DOG_DEPTH(dog)		(dog->depth == 8)

/* Pixel size in XImage buffer. */
//#define	DOG_PADDING(dog)	(dog->depth == 8 ? 8 : 32)

/* Plane mask for overlay draw. */
//#define	DOG_OVLYMASK(dog)	(dog->depth == 8 ? 128 : (~*(int *)&dog->ovlycol) & 0x00C0C0C0)

/* Plane mask for grey draw. */
#define	DOG_GREYMASK(dog)	(dog->depth == 8 ? 63 : 0x00FFFFFF)

/* Plane mask for marker draw. */
#define	DOG_MARKMASK(dog)	(dog->depth == 8 ? 127 : 0x00FFFFFF)

/* Colour value for black wash. */
#define	DOG_BLACKVAL(dog)	(dog->depth == 8 ? 127 : 0)

/* Colour value for white wash. */
#define	DOG_WHITEVAL(dog)	(dog->depth == 8 ? 64 : 0x00FFFFFF)


// Windows versions, that don't depend on depth.
#define	DOG_DEPTH(dog)		(0)
#define	DOG_PADDING(dog)	(32)
#define	DOG_OVLYMASK(dog)	((~dog->ovlycol) & 0x00C0C0C0)


// Get the appropriate HDC. Calls GetDC() if the
// dog has a window, or just returns the dc. Note
// dog->dc is always updated so that we can call
// DOG_RDC with one parameter and we can use dog->dc
// without needing a local HDC variable if we want.
#define DOG_DC(dog)			(dog->window ? (dog->dc = GetDC(dog->window)) : dog->dc)
#define DOG_RDC(dog)		if (dog->window) ReleaseDC(dog->window, dog->dc)


/* Background colour for pseudo-color. Can't be
 * half-grey because invert-drawn outlines would
 * be invisible... */
#define	PSEUDOBGRGB		PALETTERGB(0x5F, 0x5F, 0x5F)


#endif
