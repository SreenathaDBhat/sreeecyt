/*
 *	ddgslut.c	: Mark Gregson 11/8/92
 *
 *	X-Windows creation and loading of output lookup tables
 *
 *	Modifications :
 *
 *	BP	5/25/95:	ddgs_display no longer static (for external access).
 *	BP	9/15/94:	Various mods for 24-bit (see notes below).
 *
 *	27 Mar 1993	MG	llut, nlut, pseudolut now use load_lut 
 *	19 Jan 1993	MG	Stop VGA calling XStoreColors
 */

#include "ddgsdefs.h"




extern int loadcmaps;
extern char flip_probe_colours;
extern colour_lut pseudo_LUT;
extern colour_lut grey_LUT;




/* 24-bit handling notes.

Most ops in this module are affected by 24-bit only in that
there is no XSetColors (ie no LUT modification) if the DOG
has a depth other than 8. Most of the functions proceed as
normal so that the LUTs in the DOG structure are updated
(these are used by the drawing functions) but the things
don't get copied to the graphics board.

*/




/*
 * ddgs_marker_value
 *
 * Return the X colour value for one of the marker
 * colours - for use in X window foreground color
 * values etc. This is just a convenience function
 * which accounts for whether the display is 24 or
 * 8-bit.
 * This could be a macro, but the DOG_XXXX macros
 * are really only for use within DOGS. This will
 * be required throughout CytoVision (normally for
 * setting UI component colours to DOGS colours).
 * One proviso is that there must be an active
 * DOG already in existance.
 */

#ifdef WIN32
unsigned
ddgs_marker_value(int colour)
#endif
#ifdef i386
unsigned
ddgs_marker_value(colour)
int colour;
#endif
{
	unsigned val = 0;


/* Return zero if there is no current dog,
 * or if the colour is out of range. */
	if ((!curdg) || (colour < 0) || (colour > 13))
		return(val);

	if (curdg->depth == 8)
		val = 48 + colour;
	else
	{
		if (flip_probe_colours)
			val = curdg->markcol[colour].r |
				(curdg->markcol[colour].g << 8) |
				(curdg->markcol[colour].b << 16);
		else
			val = *(unsigned *)&curdg->markcol[colour];
	}

	return(val);
} /* ddgs_marker_value */




/*
 * ddgs_probe_capable
 *
 * In a similar way to the above, this is a
 * simple convenience function which returns a
 * non-zero value if the current DOG has 24-bit
 * capability.
 * This should be called before attempting any
 * probe displaying, since the draw buffers are
 * allocated depending on the current DOG depth.
 */

#ifdef WIN32
int
ddgs_probe_capable()
#endif
#ifdef i386
int
ddgs_probe_capable()
#endif
{
	return(DOG_DEPTH(curdg) == 0);
} /* ddgs_probe_capable */




/*************************************************** ddgs_register_display */

Display *ddgs_display = NULL;

#ifdef WIN32
void
ddgs_register_display(Display *display)
#endif
#ifdef i386
void
ddgs_register_display(display)
Display *display;
#endif
{
	ddgs_display = display;
} /* ddgs_register_display */




/*************************************************** ddgs_default_depth */

/*
 * Return the default depth of the display.
 */

#ifdef WIN32
int
ddgs_default_depth()
#endif
#ifdef i386
int
ddgs_default_depth()
#endif
{
	if (!ddgs_display)
	{
		fprintf(stderr, "ddgs: display not defined! (colors will be wrong on 24-bit system)\n");
		return(8);
	}

	return(DefaultDepth(ddgs_display, 0));
} /* ddgs_default_depth */




/*************************************************** ddgs_init_tabs */

/*
 * Set up colour to pixel transfer lut
 * for ddgs colour() stuff to work (replaces
 * bits of cmapinit, which is now redundant).
 * Note that this stuff is required whether
 * we are 24-bit or 8-bit - the tables are
 * used for mapping grey levels into 24-bit
 * space.
 *
 * New function by BP - replaces duplicated
 * (and rather dubiously-coded) stuff in both
 * ddgspropen() and ddgs_init_lut().
 */

#ifdef WIN32
void
ddgs_init_tabs()
#endif
#ifdef i386
void
ddgs_init_tabs()
#endif
{
	int i, j;
	static char initialised = 0;


	if (initialised)
		return;

	initialised = 1;

/* Set up linear transfer table. */
	for (i = 0; i < 256; i++)
		colourtab[i] = i >> 2;

/* Set up invert transfer table. */
	for (i = 0; i < 64; i++)
		invtab[i] = i << 2;
	while (i++ < 256)
		invtab[i] = 0;

/* Set up grey-scale RGB lut for display. */
	for (i = 0; i < 256; i++)
	{
		j = 255 - i;
		grey_LUT.red_lut[i] = j;
		grey_LUT.green_lut[i] = j;
		grey_LUT.blue_lut[i] = j;
	}

/* Set up pseudocolour RGB lut for display. */
	for (i = 0; i < 52; i++)
	{
		j = i*255.0/51.0;
		pseudo_LUT.red_lut[i] = 0;
		pseudo_LUT.green_lut[i] = j;
		pseudo_LUT.blue_lut[i] = 255;
	}

	for ( ; i < 103; i++)
	{
		j = (103.0 - i)*255.0/51.0;
		pseudo_LUT.red_lut[i] = 0;
		pseudo_LUT.green_lut[i] = 255;
		pseudo_LUT.blue_lut[i] = j;
	}

	for ( ; i < 154; i++)
	{
		j = (i - 103.0)*255.0/51.0;
		pseudo_LUT.red_lut[i] = j;
		pseudo_LUT.green_lut[i] = 255;
		pseudo_LUT.blue_lut[i] = 0;
	}

	for ( ; i < 205; i++)
	{
		j = (205.0 - i)*255.0/51.0;
		pseudo_LUT.red_lut[i] = 255;
		pseudo_LUT.green_lut[i] = j;
		pseudo_LUT.blue_lut[i] = 0;
	}

	for ( ; i < 256; i++)
	{
		j = (i - 205.0)*255.0/51.0;
		pseudo_LUT.red_lut[i] = 255;
		pseudo_LUT.green_lut[i] = 0;
		pseudo_LUT.blue_lut[i] = j;
	}
} /* ddgs_init_tabs */




/*************************************************** ddgs_init_lut */

/*
 * Initialise the ddgs 8-bit lut with
 * the standard ddgs colours. This is
 * done at startup so long as the display
 * has an 8-bit depth - this is used to
 * ensure that the ddgs colours exist on
 * Univision (all the ddgs's will refer
 * to 24-bit windows now, so the existing
 * routines will do nothing).
 * Note that I also keep in sync with the
 * existing 'cmap' and 'cmap_allocated'
 * variables so we aren't ever going to
 * create colourmaps twice etc. The
 * difference between this function and
 * cmapinit() is that this one doesn't
 * care about curdg - it only looks at
 * the capability of the display system.
 * This is important for Univision, where
 * all the ddgs's will be 24-bit, but the
 * rest of the UI is 8-bit (we still need
 * to initialise the colourmaps for the
 * navigator etc).
 */

#ifdef WIN32
void
ddgs_init_lut(Display *display, Window window)
#endif
#ifdef i386
void
ddgs_init_lut(display, window)
Display *display;
Window window;
#endif
{
	XColor lutval[64];
	unsigned long plane_masks, pixvals[256];
	int i;


	ddgs_init_tabs();

/* Don't do anything if either the display
 * is not PseudoColor, or we have already
 * initialised the colormap. All the following
 * code involves X colormap tweaking, which
 * we can't do if the system is purely 24 bits. */
	if ((DefaultDepth(display, 0) != 8) || cmap_allocated)
	{
/* Need to fool the system into thinking we
 * have allocated the LUT if we are running
 * 24-bit (else all the other calls will do
 * nothing). */
		cmap_allocated = 1;
		return;
	}

	for (i = 0; i < 48; i++)
		lutval[i].pixel = i;

/* Get the existing lut entries so we can
 * copy them into ours. */
	XQueryColors(display, DefaultColormap(display, 0), lutval, 48);

/* Create a brand new colormap. */
	cmap = XCreateColormap(display, window,
				DefaultVisual(display, 0), AllocNone);

/* Allocate all of the colormap for modification. */
	if (XAllocColorCells(display, cmap, FALSE, &plane_masks, 0, pixvals, 256))
	{
		cmap_allocated = 1;

/* Put the colours copied from the
 * old colormap into ours. */
		XStoreColors(display, cmap, lutval, 48);

		loadcmaps = True;
	}
	else
	{
		fprintf(stderr,"Cannot allocate colourmap");
		return;
	}

/* The 8 basic colours. */
	lutval[0].red = 0;    lutval[0].green = 0;    lutval[0].blue = 0;
	lutval[1].red = 0;    lutval[1].green = 0;    lutval[1].blue = 255;
	lutval[2].red = 0;    lutval[2].green = 255;  lutval[2].blue = 0;
	lutval[3].red = 0;    lutval[3].green = 255;  lutval[3].blue = 255;
	lutval[4].red = 255;  lutval[4].green = 0;    lutval[4].blue = 0;
	lutval[5].red = 255;  lutval[5].green = 0;    lutval[5].blue = 255;
	lutval[6].red = 255;  lutval[6].green = 255;  lutval[6].blue = 0;
	lutval[7].red = 255;  lutval[7].green = 255;  lutval[7].blue = 255;

/* Now the fancy extra colours. */
	lutval[8].red = 128;    lutval[8].green = 128;  lutval[8].blue = 128;
	lutval[9].red = 0;      lutval[9].green = 160;  lutval[9].blue = 0;
	lutval[10].red = 208;   lutval[10].green = 32;  lutval[10].blue = 144;
	lutval[11].red = 238;   lutval[11].green = 173; lutval[11].blue = 14;
	lutval[12].red = 255;   lutval[12].green = 255; lutval[12].blue = 255;

/* Prepare to add the ddgs marker colours... */
	for (i = 0; i < 13; i++)
	{
		lutval[i].pixel = 48 + i;
		lutval[i].flags= DoRed | DoGreen | DoBlue;

		lutval[i].red *= 256;
		lutval[i].green *= 256;
		lutval[i].blue *= 256;
	}

	XStoreColors(display, cmap, lutval, 13);

/* Now the grey-scale part. */
	for (i = 0; i < 64; i++)
	{
		lutval[i].pixel = 64 + i;
		lutval[i].flags= DoRed | DoGreen | DoBlue;

		lutval[i].red = lutval[i].green = lutval[i].blue = 256*(255 - 4*i);
	}

	XStoreColors(display, cmap, lutval, 64);

/* Make the new colormap active. */
	XInstallColormap(display, cmap);
} /* ddgs_init_lut */




/*
 *	S E T R G B  --  Set up the RGB components of the colour
 *	selected in the current ddgswindow.
 *
 */

#ifdef WIN32
void
setrgb(int col, int r, int g, int b)
#endif
#ifdef i386
void
setrgb(col, r, g, b)
int col;
int r;
int g;
int b;
#endif
{
	int i, j;
	XColor lutval;


	if (cmap_allocated)
	{
/* BP */
/* Only do this on an 8-bit display. */
		if (curdg->depth == 8)
		{
			/* set desired colour cell */
			lutval.pixel=64 + col/4;
			lutval.flags= DoRed | DoGreen | DoBlue;
			lutval.red = (short)(r * 256);
			lutval.green = (short)(g * 256);
			lutval.blue = (short)(b * 256);

			/* store lutval into colormap */
			XStoreColor(curdg->display, cmap, &lutval);
		}

		/* update ddgs clut values */
		curdg->clut.red_lut[col]=r;
		curdg->clut.green_lut[col]=g;
		curdg->clut.blue_lut[col]=b;
	}
	else
		fprintf(stderr,"Cannot setrgb - no cmap allocated");
}




/*
 * 	L O A D _ L U T
 *
 * 	Loads the output look up tables with the RGB lut
 *	belonging to a particular ddgs structure 
 *
 */

#ifdef WIN32
void
load_lut(DDGS *dg)
#endif
#ifdef i386
void
load_lut(dg)
DDGS *dg;
#endif
{
	int		i, j;
	XColor		lutval[64];


/* BP */
/* Do nothing if 24-bit. */
	if (curdg->depth != 8)
		return;

	/* check colour map exists */
	if (cmap_allocated)
	{
		for (i=0; i<64; i++)
		{
			/* set colour cells to values held in ddgs structure */
			lutval[i].pixel=64 + i;
			lutval[i].flags= DoRed | DoGreen | DoBlue;
			lutval[i].red = (short)(dg->clut.red_lut[i*4] * 256);
			lutval[i].green = (short)(dg->clut.green_lut[i*4] * 256);
			lutval[i].blue = (short)(dg->clut.blue_lut[i*4] * 256);
		}

/* VGA cannot call XStoreColors */
#ifdef VGA
		for (i=0; i<64; i++)
			XStoreColor(dg->display, cmap, &lutval[i]);
#else
		XStoreColors(dg->display, cmap, lutval, 64);
#endif

	}
	else
		fprintf(stderr,"Cannot load_lut - no cmap allocated");
}




/*
 *      A P P L Y _ L U T -- Apply a given LUT to the current ddgs framestore.
 *      Takes account of the invertedness of the framestore.
 *      It is the responsibility of the caller to take account of
 *      any overlay planes, or colours in use.
 */

#ifdef WIN32
void
apply_lut(struct look_up_table *lut)
#endif
#ifdef i386
void
apply_lut(lut)
struct look_up_table *lut;
#endif
{
	register unsigned char *rl, *gl, *bl;
	register int i;


/* BP */
/* Do nothing if 24-bit. */
	if (curdg->depth != 8)
		return;

	rl = lut->red_lut;
	gl = lut->green_lut;
	bl = lut->blue_lut;

	if (curdg->invert)
		for (i=0; i<256; i++)
		{
			curdg->clut.red_lut[i] = 255 - (int)*rl++;
			curdg->clut.green_lut[i] = 255 - (int)*gl++;
			curdg->clut.blue_lut[i] = 255 - (int)*bl++;
		}
	else
		curdg->clut=*lut;

	load_lut(curdg);
}


/*
 *	L L U T  --  Create a linear lut (white = 0, black = 255)
 *	for the current ddgs window
 *  Note: only 64 grey levels displayable on SVGA ( 256 on Primagraphics )
 *
 */

#ifdef WIN32
void
llut()
#endif
#ifdef i386
void
llut()
#endif
{
	register int i,j;


	if (cmap_allocated)
	{
/*
		for (i=0; i<256; i++)
		{
			j = 255 - i;
			curdg->clut.red_lut[i]=j;
			curdg->clut.green_lut[i]=j;
			curdg->clut.blue_lut[i]=j;
		}
*/
		memcpy(&curdg->clut, &grey_LUT, sizeof(colour_lut));

		load_lut(curdg);
	}
	else
		fprintf(stderr,"Cannot llut - no cmap allocated");
}


/*
 *
 * N L U T 
 *
 * load normal grey level look up table
 */

#ifdef WIN32
void
nlut()
#endif
#ifdef i386
void
nlut()
#endif
{
	register int i;


	if (cmap_allocated)
	{
		for (i=0; i<256; i++)
		{
			curdg->clut.red_lut[i]=i;
			curdg->clut.green_lut[i]=i;
			curdg->clut.blue_lut[i]=i;
		}

		load_lut(curdg);
	}
	else
		fprintf(stderr,"Cannot nlut - no cmap allocated");
}




/*
 *
 * P S E U D O L U T
 *
 * load pseudocolour look up table
 */

#ifdef WIN32
void
pseudolut()
#endif
#ifdef i386
void
pseudolut()
#endif
{
	register int i,k;


	if (cmap_allocated)
	{
/*
		for (i=0; i<52; i++) {
			k=i*255.0/51.0;
			curdg->clut.red_lut[i]=0;
			curdg->clut.green_lut[i]=k;
			curdg->clut.blue_lut[i]=255;
		}

		for (i=52; i<103; i++) {
			k=(103.0-i)*255.0/51.0;
			curdg->clut.red_lut[i]=0;
			curdg->clut.green_lut[i]=255;
			curdg->clut.blue_lut[i]=k;
		}

		for (i=103; i<154; i++) {
			k=(i-103.0)*255.0/51.0;
			curdg->clut.red_lut[i]=k;
			curdg->clut.green_lut[i]=255;
			curdg->clut.blue_lut[i]=0;
		}

		for (i=154; i<205; i++) {
			k=(205.0-i)*255.0/51.0;
			curdg->clut.red_lut[i]=255;
			curdg->clut.green_lut[i]=k;
			curdg->clut.blue_lut[i]=0;
		}

		for (i=205; i<256; i++) {
			k=(i-205.0)*255.0/51.0;
			curdg->clut.red_lut[i]=255;
			curdg->clut.green_lut[i]=0;
			curdg->clut.blue_lut[i]=k;
		}
*/
memcpy(&curdg->clut, &pseudo_LUT, sizeof(colour_lut));

		load_lut(curdg);
	}
	else
		fprintf(stderr,"Cannot pseudolut - no cmap allocated");
}




/*
 *
 *  P S E U D O D G
 *
 * load pseudocolour look up table into colours 192 to 255
 * and fill overlay plane causes all pixels to appear in pseudocolour
 */
#ifdef WIN32
void
pseudodg(int brightfield)
#endif
#ifdef i386
void
pseudodg(brightfield)
int brightfield;
#endif
{
	register int i,j,k;
	colour_lut clut;
	XColor	lutval[64];


	if (cmap_allocated)
	{
		ddgsoverlay(OFF);

#if 0
		for (i=0; i<52; i++) {
			k=i*255.0/51.0;
			clut.red_lut[i]=0;
			clut.green_lut[i]=k;
			clut.blue_lut[i]=255;
		}
		/* grey background */
		if (brightfield)
			for (i=0; i<4; i++) {
				clut.red_lut[i]=128;
				clut.green_lut[i]=128;
				clut.blue_lut[i]=128;
			}

		for (i=52; i<103; i++) {
			k=(103.0-i)*255.0/51.0;
			clut.red_lut[i]=0;
			clut.green_lut[i]=255;
			clut.blue_lut[i]=k;
		}
		for (i=103; i<154; i++) {
			k=(i-103.0)*255.0/51.0;
			clut.red_lut[i]=k;
			clut.green_lut[i]=255;
			clut.blue_lut[i]=0;
		}
		for (i=154; i<205; i++) {
			k=(205.0-i)*255.0/51.0;
			clut.red_lut[i]=255;
			clut.green_lut[i]=k;
			clut.blue_lut[i]=0;
		}
		for (i=205; i<256; i++) {
			k=(i-205.0)*255.0/51.0;
			clut.red_lut[i]=255;
			clut.green_lut[i]=0;
			clut.blue_lut[i]=k;
		}
		/* grey background */
		if (!brightfield)
			for (i=252; i<256; i++) {
				clut.red_lut[i]=168;
				clut.green_lut[i]=168;
				clut.blue_lut[i]=168;
			}
#endif

memcpy(&clut, &pseudo_LUT, sizeof(colour_lut));
if (brightfield)
	for (i = 0; i < 4; i++)
	{
		clut.red_lut[i] = 128;
		clut.green_lut[i] = 128;
		clut.blue_lut[i] = 128;
	}
else
	for (i = 252; i < 256; i++)
	{
		clut.red_lut[i] = 168;
		clut.green_lut[i] = 168;
		clut.blue_lut[i] = 168;
	}

/* BP */
/* Do nothing if 24-bit. */
		if (DOG_DEPTH(curdg))
		{
			for (i = 0; i < 64; i++)
			{
			/* set colour cells to values held in ddgs structure */
				lutval[i].pixel=192 + i;
				lutval[i].flags= DoRed | DoGreen | DoBlue;
				lutval[i].red = (short)(clut.red_lut[i*4] * 256);
				lutval[i].green = (short)(clut.green_lut[i*4] * 256);
				lutval[i].blue = (short)(clut.blue_lut[i*4] * 256);
			}

/* VGA cannot call XStoreColors */
#ifdef VGA
			for (i=0; i<64; i++)
				XStoreColor(curdg->display, cmap, &lutval[i]);
#else
			XStoreColors(curdg->display, cmap, lutval, 64);
#endif
		}

		curdg->pseudocolour=ON;	/* pseudocolour display on */

		oclear();
	}
	else
		fprintf(stderr,"Cannot pseudolut - no cmap allocated");
}




/*
 * 	M O N O D G
 *
 *	switch pseudocolour display off
 */

#ifdef WIN32
void
monodg()
#endif
#ifdef i386
void
monodg()
#endif
{
	curdg->pseudocolour=OFF;	/* pseudocolour display off */
	oclear();
	ddgsoverlay(ON);
}




/* 
 * 	S A V E _ C O L O U R S
 *
 *	save current ddgs grey table, marker colours and overlay colour to file
 */

#ifdef WIN32
int
save_colours(FILE *f)
#endif
#ifdef i386
int
save_colours(f)
FILE *f;
#endif
{

	if (fwrite(&curdg->clut,sizeof(colour_lut),1,f) != 1) {
		fprintf(stderr,"save_colours failed writing curdg->clut\n");
		return(-1);
	}

	if (fwrite(curdg->markcol,sizeof(rgbcol),MAXMARKER,f) != MAXMARKER) {
		fprintf(stderr,"save_colours failed writing curdg->markcol[]\n");
		return(-1);
	}
	if (fwrite(&curdg->ovlycol,sizeof(rgbcol),1,f) != 1) {
		fprintf(stderr,"save_colours failed writing curdg->ovlycol\n");
		return(-1);
	}

	return(0);	/* save colours successful */
}




/* 
 * 	L O A D _ C O L O U R S
 *
 *	load grey table, marker colours and overlay colour from file to curdg
 */
#ifdef WIN32
int
load_colours(FILE *f)
#endif
#ifdef i386
int
load_colours(f)
FILE *f;
#endif
{

	if (fread(&curdg->clut,sizeof(colour_lut),1,f) != 1) {
		fprintf(stderr,"load_colours failed reading curdg->clut\n");
		return(-1);
	}

	if (fread(curdg->markcol,sizeof(rgbcol),MAXMARKER,f) != MAXMARKER) {
		fprintf(stderr,"load_colours failed reading curdg->markcol[]\n");
		return(-1);
	}

	if (fread(&curdg->ovlycol,sizeof(rgbcol),1,f) != 1) {
		fprintf(stderr,"load_colours failed reading curdg->ovlycol\n");
		return(-1);
	}

	return(0);	/* save colours successful */
}

