/*
 *	ddgsmouse.c :	Mark Gregson
 *			18th August 1992
 *	
 *	Routine for drawing shapes (points, lines, polygons, circles, rectangles)
 *	using the mouse under X Windows. The main routine mousedraw activates
 * 	an event handler mousedrawEH to draw a given shape on a particulardrawing 
 *	area widget. After activating the event handler it dispatches events to
 *	it until drawing terminates. All drawing takes place in the overlay
 *	plane and is removed on completion of the drawing. Since the drawing
 * 	is intentionally a short term thing there is no attempt made to
 * 	backup the drawing in the current DDGS structure pixmap. The points drawn
 * 	by mousedraw are maintained in dg->mousepts - these can be examined
 * 	by external routines to generate woolz objects. All drawing is commenced
 * 	by clicking the left button and terminated by clicking the right button.
 *	POLYLINE intermediate points are marked by clicking the left button.
 *	POLYLINE backup occurs if the middle button is pressed.
 *	POINT drawing is started and terminated ( in one go ) by pressing any
 *	button.
 * 
 *
 *	Modifications:
 *
 *	23Nov99	dcb	Change mouse drawing cursor to coloured cross
 *	26Jul99	MG	Add ddgs NULL checking at start of a number of routines 
 *	16Mar98	WH:	Select pen into line drawing DC's for colour and use XOR rop
 *	05Dec96	BP:	Windows version.
 *
 *	9/15/94	BP	Several small tweaks (mainly in mousedrawEH) for
 *				24-bit operation. Note the use of macros (DOG_XXXX)
 *				throughout.
 *
 *	14 Sep 1993	MG	Removed BP's duff backup fix - did it properly
 *	 1 Sep 93	BP	Stopped doing XDrawLines() if number of lines is
 *				less than 2 in the (probably vain) hope that this
 *				might prevent backing-up of freehand line going pop.
 *				Now does XDrawLine().
 *	 1 Feb 1993	MG	added POINT drawing to mousedraw
 *	29 Jan 1993	MG	allow user to create single point line or polyline
 *	26 Jan 1993	MG	removed window constraint and changed to 3 buttons
 *	17 Sep 1992	MG	get_mousept added
 *
 */


#include <math.h>
#include "ddgsdefs.h"
#include <\WinCV\src\resource\resource.h>

/*

BP 12/6/96	-	Module re-design.
---------------------------------

I have rewritten this module significantly, although
retaining (hopefully) a faithful rendition of its
functionality.

There is no longer a local event loop and a big ugly
event handler callback that does everything. Calling
mousedraw() now initiates callback processing by the
static event callbacks for mouse clicks and movement.
Normally, these callbacks return FALSE indicating that
they are not doing anything. When drawing a shape,
they return TRUE (indicating "I'm busy") in which case
the application should do nothing else with mouse
events.

I have added local functions _drawline() and _drawpoly()
so as not to clutter the callbacks with Windows-specific
calls.


BP 12/12/96	-	UPDATE!
-----------------------

Because of the amount of reorganisation of code in
the rest of the application I have gone back to the
yukky local event loop method. Works fine, and is a
much less dubious approach under Windows than it was
under X...
My approach would have meant big changes to woolz
(interact.c) aswell as higher level bits and I consider
it not worth the effort.

Fortunately, I have been able to retain all my pretty
callbacks - very little has actually changed (only the
new event loop in mousedraw itself and removal of the
callback from _finish_drawing and mousedraw).

*/


/* Forward declarations for internal
 * invert drawing functions. */
static void _checkpoints(DDGSPoint *pt1, DDGSPoint *pt2);
static void _drawcircle(DDGS *dg, DDGSPoint *ctr, int radius);
static void _drawrect(DDGS *dg, DDGSPoint *pta, DDGSPoint *ptb);
static void _drawline(DDGS *dg, DDGSPoint *pta, DDGSPoint *ptb);
static void _drawpoly(DDGS *dg, DDGSPoint *pts, int npts);


static HCURSOR prev_cursor;


/*
 * Add a new point to the array.
 * There's no equivalent for removing, because
 * this can be done simply by decrementing the
 * count.
 */

static void _extend_list(DDGS *dg)
{
	if (!dg)
		return;

	dg->mousepts = (DDGSPoint *)realloc(dg->mousepts,
		(dg->nmousepts + 2)*sizeof(DDGSPoint));
	dg->nmousepts++;
	dg->mousepts[dg->nmousepts] = dg->mousepts[dg->nmousepts - 1];
}


/*
 * Cleanup by calling the callback (if any),
 * freeing allocated space, and resetting the
 * flags.
 *
 * No - back to old style. Just reset the cursor
 * and the busy flag so the local event loop
 * drops through.
 */

static void _finish_drawing(DDGS *dg)
{
	if (!dg)
		return;

	SetCursor(prev_cursor);
	ReleaseCapture();

	dg->shape = 0;
}


/*
 * Calculate radius of circle that stays
 * within dog.
 */

static int _clipradius(DDGS *dg, DDGSPoint *ctr, DDGSPoint *pt)
{
	int radius;

	if (!dg)
		return 0;

	radius = (int)sqrt((double)(pt->x - ctr->x)*(pt->x - ctr->x) +
		(double)(pt->y - ctr->y)*(pt->y - ctr->y));

	if (ctr->x - radius < dg->minx)
		radius = ctr->x - dg->minx;
	if (ctr->y - radius < dg->miny)
		radius = ctr->y - dg->miny;
	if (dg->maxx - ctr->x < radius)
		radius = dg->maxx - ctr->x;
	if (dg->maxy - ctr->y < radius)
		radius = dg->maxy - ctr->y;

	return radius;
}


/*
 * Make sure a point is within a dog.
 */

static void _cliptodog(DDGS *dg, long *x, long *y)
{
	if (!dg)
		return;

	if (*x < dg->minx)
		*x = dg->minx;
	if (*y < dg->miny)
		*y = dg->miny;
	if (*x > dg->maxx)
		*x = dg->maxx;
	if (*y > dg->maxy)
		*y = dg->maxy;
}


/*
 * ddgsMouseBusy
 *
 * Are we in the middle of drawing stuff?
 */

BOOL ddgsMouseBusy(DDGS *dg)
{
	if (!dg)
		return FALSE;

	if (dg->shape == 0)
		return FALSE;

	return TRUE;
}


/*
 * ddgsMouseMove
 *
 * Callback from mouse motion over a dog.
 */
BOOL ddgsMouseMove(DDGS *dg, long x, long y)
{
	int radius;
	DDGSPoint oldpt;

	if (!dg)
		return FALSE;

	if (dg->shape == 0)
		return FALSE;

	if (dg->nmousepts < 1)
		return TRUE;

	_cliptodog(dg, &x, &y);

	oldpt = dg->mousepts[dg->nmousepts];
	dg->mousepts[dg->nmousepts].x = x;
	dg->mousepts[dg->nmousepts].y = y;

	switch (dg->shape) {
	case DDGSCIRCLE:
		radius = _clipradius(dg, &dg->mousepts[0], &dg->mousepts[1]);
		_drawcircle(dg, &dg->mousepts[0], radius);
		radius = _clipradius(dg, &dg->mousepts[0], &oldpt);
		_drawcircle(dg, &dg->mousepts[0], radius);
		break;

	case DDGSPOLYLINE:
		_drawline(dg, &dg->mousepts[dg->nmousepts - 1], &dg->mousepts[dg->nmousepts]);
		_drawline(dg, &dg->mousepts[dg->nmousepts - 1], &oldpt);
		break;

	case DDGSLINE:
		_drawline(dg, &dg->mousepts[dg->nmousepts - 1], &dg->mousepts[dg->nmousepts]);
		break;

	case DDGSRECTANGLE:
		_drawrect(dg, &dg->mousepts[0], &dg->mousepts[1]);
		_drawrect(dg, &dg->mousepts[0], &oldpt);
		break;
	}

	if (dg->shape == DDGSLINE)
		_extend_list(dg);

	return TRUE;
}


/*
 * ddgsMouseRelease
 *
 * Callback from mouse button click over a dog.
 */
BOOL ddgsMouseRelease(DDGS *dg, long x, long y, short button)
{
	int radius, k;
	POINT pt;
	BOOL finished = FALSE;

	if (!dg)
		return FALSE;

	if (dg->shape == 0)
		return FALSE;

	/* If drawing single point, terminate
	 * drawing on any button press. */
	if (dg->shape == DDGSPOINT)
		button = WM_RBUTTONUP;

	_cliptodog(dg, &x, &y);

	switch (button) {
	case WM_LBUTTONUP:
		/* Must start with a left mouse click. */
		if (dg->nmousepts == 0)
		{
			dg->mousepts[0].x = x;
			dg->mousepts[0].y = y;
			dg->nmousepts = 1;

			dg->mousepts[1].x = x;
			dg->mousepts[1].y = y;
		}
		else
			/* Add points to list when drawing polygon. */
			if (dg->shape == DDGSPOLYLINE)
				_extend_list(dg);
		break;

	case WM_MBUTTONUP:
		/* If polyline or line, middle button can
		 * be pressed during drawing to back up. */
		if (dg->nmousepts < 2)
			break;

		switch (dg->shape) {
		case DDGSPOLYLINE:
			/* Erase current line. */
			_drawline(dg, &dg->mousepts[dg->nmousepts - 1],
				&dg->mousepts[dg->nmousepts]);

			/* Erase last line. */
			_drawline(dg, &dg->mousepts[dg->nmousepts - 2],
				&dg->mousepts[dg->nmousepts - 1]);

			/* Draw new line. */
			// Note the order in which these are draw,
			// bacause windows skips the last pixel in a
			// line. This last one goes from 0 -> -2 rather
			// than -2 -> 0 to allow for a complete closure
			// around the loop of three points (leaving no
			// spurious pixels).
			_drawline(dg, &dg->mousepts[dg->nmousepts],
				&dg->mousepts[dg->nmousepts - 2]);

			/* Replace prev point with current and
			 * make this the end of the list. */
			dg->mousepts[dg->nmousepts - 1] = dg->mousepts[dg->nmousepts];
			dg->nmousepts--;
			break;

		case DDGSLINE:
			for (k = 0; k < 5; k++)
			{
				if (dg->nmousepts < 2)
					break;

				dg->nmousepts--;

				// Erase prev segment.
				_drawline(dg, &dg->mousepts[dg->nmousepts - 1],
					&dg->mousepts[dg->nmousepts]);

				// Move the pointer back to the prev point.
				pt.x = dg->mousepts[dg->nmousepts].x;
				pt.y = dg->mousepts[dg->nmousepts].y;
				ClientToScreen(dg->window, &pt);
				SetCursorPos(pt.x, pt.y);
			}
			break;
		}
		break;

	case WM_RBUTTONUP:
		/* Right hand button pressed - terminate drawing. */
		if (dg->nmousepts == 0)
		{
			dg->mousepts[0].x = x;
			dg->mousepts[0].y = y;

			switch (dg->shape) {
			case DDGSRECTANGLE:
				dg->mousepts[1].x = x + 1;
				dg->mousepts[1].y = y + 1;
				break;

			case DDGSCIRCLE:
				dg->mousepts[1].x = x + 1;
				dg->mousepts[1].y = y;
				break;

			default:
				break;
			}
		}

		/* Terminate drawing and erase lines. */
		switch (dg->shape) {
		case DDGSPOLYLINE:
		case DDGSLINE:
			dg->nmousepts++;
			_drawpoly(dg, dg->mousepts, dg->nmousepts);
			break;

		case DDGSRECTANGLE :
			_checkpoints(&dg->mousepts[0], &dg->mousepts[1]);
			_drawrect(dg, &dg->mousepts[0], &dg->mousepts[1]);
			dg->nmousepts = 2;
			break;

		case DDGSCIRCLE:
			radius = _clipradius(dg, &dg->mousepts[0], &dg->mousepts[1]);
			_drawcircle(dg, &dg->mousepts[0], radius);
			dg->nmousepts = 2;
			break;

		default:
		 	break;
		}

		finished = TRUE;
		break;

	default:
		break;
	}

	if (finished)
	{
		_finish_drawing(dg);
		return FALSE;
	}

	return TRUE;
}


/*
 * Initiate drawing.
 * The actual business is done via the event
 * callbacks above - there is no special
 * event loop.
 *
 * No - back to old style. Yes there is an
 * event loop!
 */

void
mousedraw(int shape, DDGS *dg)
{
	MSG msg;

	if (dg == NULL)
		return;

	if (dg->window == NULL)
		return;

	if ((shape < DDGSPOINT) || (shape > DDGSCIRCLE))
		return;

	// Remember what shape we are drawing, and
	// the address of the function to pass the
	// results to.
	dg->shape = shape;

	// Initially have empty array of points.
	dg->nmousepts = 0;
	if (dg->mousepts)
		free(dg->mousepts);
	dg->mousepts = (DDGSPoint *)calloc(sizeof(DDGSPoint), 2);

	// Must get all mouse events.
	SetCapture(dg->window);
	prev_cursor = SetCursor(LoadCursor(NULL, IDC_CROSS));
//	prev_cursor = SetCursor(LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_CUR_COLCROSS)));

	// Stay here until we finish.
	while (dg->shape && (GetMessage(&msg, dg->window, 0, 0) == TRUE))
	{
		TranslateMessage(&msg);
    	DispatchMessage(&msg);
	}
}


/**************************************************************************/

/*
 * Swap rectangle points if necessary.
 */

static void _checkpoints(DDGSPoint *pt1, DDGSPoint *pt2)
{
	long temp;

	if (pt1->x > pt2->x)
	{
		temp = pt1->x;
		pt1->x = pt2->x;
		pt2->x = temp;
	}

	if (pt1->y > pt2->y)
	{
		temp = pt1->y;
		pt1->y = pt2->y;
		pt2->y = temp;
	}
}

/*
** Set up DC in given DDGS for overlay operations and 
** return the HDC
*/
static HDC _loadoverlayDC(DDGS *dg)
{
	HDC dc;

	if (!dg)
		return 0;

	dc = DOG_DC(dg);
	SetROP2(dc, R2_XORPEN);
	SelectObject(dc, dg->ovlypen);
	SetTextColor(dc, dg->ovlycol);

	SelectClipRgn(dc, dg->cliprgn);

	return dc;
}

/*
 * Invert draw a rectangle.
 */

static void _drawrect(DDGS *dg, DDGSPoint *pta, DDGSPoint *ptb)
{
	HDC dc;
	POINT pt[5];

	if (!dg)
		return;

	pt[0] = *(POINT *)pta;
	pt[1].x = ptb->x;
	pt[1].y = pta->y;
	pt[2] = *(POINT*)ptb;
	pt[3].x = pta->x;
	pt[3].y = ptb->y;
	pt[4] = pt[0];

	//dc = GetDC(dg->window);
	//SetROP2(dc, R2_NOT);
	dc = _loadoverlayDC(dg);
	Polyline(dc, pt, 5);
	ReleaseDC(dg->window, dc);
}


/*
 * Invert draw a circle.
 */

static void _drawcircle(DDGS *dg, DDGSPoint *ctr, int radius)
{	
	HDC dc;

	if (!dg)
		return;

	//dc = GetDC(dg->window);
	//SetROP2(dc, R2_NOT);
	dc = _loadoverlayDC(dg);
	Arc(dc, ctr->x - radius, ctr->y - radius,
		ctr->x + radius, ctr->y + radius,
		ctr->x, ctr->y - radius,
		ctr->x, ctr->y - radius);
	ReleaseDC(dg->window, dc);
}


/*
 * Invert draw a line between 2 points.
 */

static void _drawline(DDGS *dg, DDGSPoint *pta, DDGSPoint *ptb)
{
	HDC dc;
	POINT pt[2];

	if (!dg)
		return;

	pt[0] = *(POINT *)pta;
	pt[1] = *(POINT *)ptb;

	//dc = GetDC(dg->window);
	//SetROP2(dc, R2_NOT);
	dc = _loadoverlayDC(dg);
	Polyline(dc, pt, 2);
	ReleaseDC(dg->window, dc);
}


/*
 * Invert draw a polygon.
 */

static void _drawpoly(DDGS *dg, DDGSPoint *pts, int npts)
{
	HDC dc;

	if (!dg)
		return;

	//dc = GetDC(dg->window);
	//SetROP2(dc, R2_NOT);
	dc = _loadoverlayDC(dg);
	Polyline(dc, (POINT *)pts, npts);
	ReleaseDC(dg->window, dc);
}
