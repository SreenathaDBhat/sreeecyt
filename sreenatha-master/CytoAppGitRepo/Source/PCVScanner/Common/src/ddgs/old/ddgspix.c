/*
 * 	ddgspix.c	: Mark Gregson 11/8/92
 *
 *	X-Windows pixel drawing routines
 *
 * Mods:
 *
 *	26Jul99	MG		Add ddgs NULL checking at start of a number of routines 
 *	18Jan99	KS:		Masking in fastpixobj24, just changed
 *					this so that the masking happens in memory - looks nicer on screen!
 *	11Mar98	WH:		Added masking in fastpixobj24. Changed allocation in
 *					ddgs_get_draw_mask().
 *	24Apr97 WH:		Afraid we do so, clipping is back by popular demand
 *	20Mar97	BP:		No clip region.... no need?
 *	18Mar97	BP:		Tidy masked/boolean drawing using DDGSMASKED.
 *					Also implemented MARKIM draw properly using
 *					binfo etc (just like GREYIM).
 *					Also implemented OVLYIM draw properly using
 *					binfo XOR (but this should never be used!)
 *					Mask BITMAPINFO is now a member of DDGS (minfo).
 *
 *	20Jan97	BP:		Added fastpixobj24().
 *	10Jan97	BP:		Also free font resources in free_draw_buffers()
 *	BP	12/24/96:	Support buffered or un-buffered display through
 *					DDGSBUFFERED define.
 *	BP	9/24/96:	Windows version.
 *
 *	BP	6/20/96:	Remove the SHM alloc error message! This will cause
 *					actual problems only with capture - which will generate
 *					its own messages!
 *	WH 10jun96		Remove the RMID stuff (see previous comment) as it seems to
 *					to prevent the first time display of insensitive buttons.
 *	BP	5/3/96:		OK - put RMID stuff in (see previous comment!), with XSync.
 *					Looks like this does the trick...
 *	BP	4/1/96:		Patch in (but leave commented-out) call to stop
 *					shmid hanging around after crash.
 *	BP	8/24/95:	Allow for NULL dog window (to draw in pixmap only).
 *	BP	6/28/95:	Got XShmPutImage() stuff working. This can be
 *					included/excluded at compile time through SHM_ENABLED
 *					Note that a regular buffer will be used if there is
 *					an error getting the SHM buffer (for example, when
 *					running on a remote display). Only "draw_buffer" is
 *					included in the SHM system.
 *	BP	5/25/95:	Added ddgs_free_buffers() to free off buffer
 *					memory. Also added (but commented out) stuff
 *					for XShmPutImage(). Commented out because it
 *					doesn't work properly. Note that the free_buffers
 *					this is REQUIRED for shared memory stuff else
 *					the system runs out of resources.
 *					Also added static grey-2-ddgs LUT's for faster
 *					drawing (no more shift-then-OR on each pixel).
 *	BP	5/11/95:	fastpixobj() is now faster in 24-bit mode.
 *	BP	12/10/94:	Allow for different image depths.
 *	BP	9/20/94:	New drawing buffer allocation scheme to accomodate
 *					8- and 24-bit drawing.
 *	 5 Mar 1993	XSetFunction stuff for inverted drawing
 */
 
#include <memory.h>
#include "wstruct.h"
#include "ddgsdefs.h"


#define GREYIM	0
#define OVLYIM	1
#define MARKIM	2


/*
 * Define minimum buffer sizes so that
 * a big enough buffer to do most things
 * will be allocated right away (first
 * time through). This is an attampt to
 * prevent a lot of realloc calls, which
 * result in serious fragmentation.
 */
#define	MINBUFFERSIZE	1048576
#define	MINMASKSIZE		131072



/*
 * BP - Drawing buffers.
 *
 * This section of code is used to manage buffers
 * used in drawing operations for grey/probe
 * objects. Higher level functions should call
 * these functions to obtain a buffer for use
 * with all such drawing operations. The same
 * buffers can be used for probes and chroms to
 * minimise memory usage.
 *
 * Memory is allocated based on the visual mode
 * of the server. If we are running 24-bits,
 * buffers of 4x the required size are allocated.
 *
 * Note that there are actually only 3 buffers
 * used. These are never freed. realloc() is used
 * if an object larger than any other already
 * drawn is requested. This method prevents
 * memory fragmentation, and will be fast (most
 * of the time no memory allocation at all will
 * be necessary).
 * The aux buffer is available for use by cases
 * where a temporary work buffer is required
 * whilst draw_buffer is in use - probably only
 * useful for probe drawing.
 */

static char *draw_buffer = NULL;
static char *draw_mask = NULL;
static char *aux_buffer = NULL;
static int draw_buffer_size = 0;
static int draw_mask_size = 0;
static int aux_buffer_size = 0;


/* Private function. */


/*
 * prepare_buffer() is a convenience function to
 * deal with pointer assignments and any allocation
 * or reallocation that may be necessary.
 * Note that malloc and realloc are used (not Malloc
 * and Realloc) because these buffers will never be
 * freed, and I don't want to screw-up the malcheck
 * stuff!
 */

static char *
prepare_buffer(char *buffer, int *oldsize, int newsize, int reqsize)
{
    char *newbuffer;

	if ((newsize < 1) || (reqsize < 1))
		return(buffer);

	/* If the buffer is already allocated, see if
	 * we need to realloc, or just use it as ie. */
    if (buffer)
    {
		/* If we need more space, reallocate. */
        if (newsize > *oldsize)
        {
            newbuffer = (char *)Realloc(buffer, newsize);
            *oldsize = newsize;
        }
        else
            newbuffer = buffer;
    }
    else
    {
		/* Create a new one. */
        newbuffer = (char *)Malloc(newsize);
        *oldsize = newsize;
    }

	/* If the buffer is OK, clear it. This is
	 * required for probes, but may not be for
	 * other stuff... */
    if (newbuffer)
        memset(newbuffer, 0, reqsize);

    return(newbuffer);
} /* prepare_buffer */




/* Public functions. */


/*
 * Return a pointer to the appropriate buffer.
 * These functions will return NULL if there
 * was a malloc/realloc NULL return.
 * The size parameter should be the number of
 * pixels.
 * The mask size is calculated internally (here)
 * from the width and height given (the mask
 * to be a multiple of 8 pixels wide).
 */

unsigned char *
ddgs_get_draw_buffer(int size)
{
	int bytesize;
	int reqsize = size;

	if (size < MINBUFFERSIZE)
		size = MINBUFFERSIZE;

	bytesize = size;

	draw_buffer = prepare_buffer(draw_buffer, &draw_buffer_size,
			bytesize, reqsize);

	return(draw_buffer);
} /* ddgs_get_draw_buffer */


unsigned char *
ddgs_get_aux_buffer(int size)
{
	int bytesize;
	int reqsize = size;

	if (size < MINBUFFERSIZE)
		size = MINBUFFERSIZE;

	bytesize = size;

	aux_buffer = prepare_buffer(aux_buffer,
			&aux_buffer_size, bytesize, reqsize);

	return(aux_buffer);
} /* ddgs_get_aux_buffer */


unsigned char *
ddgs_get_draw_mask(int w, int h)
{
	int bytesize;
	int reqsize;

#ifdef i386
	reqsize = bytesize = h*((w + 7) >> 3);
#endif
#ifdef WIN32
	/* 
	** XLib version allocated bits while Windows version needs
	** bytes as we use a mask image rather than a clip mask.
	*/
	reqsize = bytesize = w * h;
#endif


	if (bytesize < MINMASKSIZE)
		bytesize = MINMASKSIZE;

	draw_mask = prepare_buffer(draw_mask,
			&draw_mask_size, bytesize, reqsize);
	return(draw_mask);
} /* ddgs_get_draw_mask */


/*
 * Free off all the buffers, and set them to NULL.
 * Note that it will still be OK to continue a
 * program after calling this - new buffers will be
 * allocated when required.
 */

void
ddgs_free_buffers()
{
	if (draw_buffer)
		Free(draw_buffer);
	draw_buffer = NULL;
	draw_buffer_size = 0;

	if (aux_buffer)
		Free(aux_buffer);
	aux_buffer = NULL;
	aux_buffer_size = 0;

	if (draw_mask)
		Free(draw_mask);
	draw_mask = NULL;
	draw_mask_size = 0;

	// Free up any other global resources that
	// ddgs has allocated.
	DeleteObject(ddgs_palette);
	free_fonts();
} /* ddgs_free_buffers */






#define	DDGSMASKED


/*
 * fastpixobj
 * Draw entire object as single pixmap into image.
 *
 * Works on 8-bit and 24-bit displays. On
 * a 24-bit display, the data (which is assumed
 * to be 8 bits) is converted through the ddgs
 * LUTs into a 24-bit buffer. If the display is
 * 8 bits, the data is converted into range
 * 64-127 to use the ddgs display LUT grey range.
 */

void
fastpixobj(int vx, int vy, int w, int h,
		   unsigned char *vdata, unsigned char *mdata,
		   short where)
{
	HDC dc;
	int i;
	RGBQUAD keep_col[256];

#ifdef	DDGSBUFFERED
	RECT rect;
#endif

	if (curdg == NULL)
		return;

	curdg->binfo.head.biWidth = w;
	curdg->binfo.head.biHeight = -h;
	curdg->binfo.head.biBitCount = 8;

#ifdef DDGSMASKED
	curdg->minfo.head.biWidth = w;
	curdg->minfo.head.biHeight = -h;
	curdg->minfo.head.biBitCount = 8;
#endif

	dc = DOG_DC(curdg);
	SelectClipRgn(dc, curdg->cliprgn);

	/* Make sure the palette is in place. */
	SelectPalette(dc, ddgs_palette, 0);
	RealizePalette(dc);

	switch (where) {
	case GREYIM:

#ifdef	DDGSBUFFERED
		// Draw the object data into the ddgs
		// image buffer (should be done elsewhere).
		rect.left = vx;
		rect.top = curdg->maxy - vy;
		rect.right = rect.left + w - 1;
		rect.bottom = rect.top + h - 1;
		ddgsImageDrawGrey(curdg, vdata, &rect);

		// Copy the section of the ddgs DIB image
		// buffer to the dc. Note the destination
		// y coordinate is upside-down to match
		// the upside-down (top-down) DIB.
		ptr = (BYTE *)GlobalLock(curdg->himage);
		StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
			vx, vy - h + 1, w, h, ptr, (BITMAPINFO *)&curdg->binfo,
			DIB_RGB_COLORS, SRCCOPY);
		GlobalUnlock(curdg->himage);
#else
#ifdef DDGSMASKED
		// Operation is different depending on
		// image background:
		//	Black bg - AND with inverted mask then OR with image.
		//	White bg - OR with mask then AND with image

		if ((curdg->binfo.col[0].rgbRed == 0) &&
			(curdg->binfo.col[0].rgbGreen == 0) &&
			(curdg->binfo.col[0].rgbBlue == 0))
		{
			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->minfo,
				DIB_RGB_COLORS, SRCAND);

			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCPAINT);

		}
		else
		{

			curdg->binfo.col[0].rgbRed = 255;
			curdg->binfo.col[0].rgbGreen = 255;
			curdg->binfo.col[0].rgbBlue = 255;

			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->minfo,
				DIB_RGB_COLORS, MERGEPAINT);

			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCAND);
		}

#else
		// If fluorescent, we OR the image onto
		// the black background. Otherwise, its
		// brightfield, and we AND the image with
		// the white background.
		if ((curdg->binfo.col[0].rgbRed == 0) &&
			(curdg->binfo.col[0].rgbGreen == 0) &&
			(curdg->binfo.col[0].rgbBlue == 0))
			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCPAINT);
		else
			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCAND);
#endif
#endif
		break;

	case OVLYIM:

#ifdef	DDGSBUFFERED
		// Draw the object data into the ddgs
		// image buffer (should be done elsewhere).
		rect.left = vx;
		rect.top = curdg->maxy - vy;
		rect.right = rect.left + w - 1;
		rect.bottom = rect.top + h - 1;
		ddgsImageDrawGrey(curdg, vdata, &rect);

		// Copy the section of the ddgs DIB image
		// buffer to the dc. Note the destination
		// y coordinate is upside-down to match
		// the upside-down (top-down) DIB.
		ptr = (BYTE *)GlobalLock(curdg->himage);
		StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
			vx, vy - h, w, h, ptr, (BITMAPINFO *)&curdg->binfo,
			DIB_RGB_COLORS, SRCINVERT);
		GlobalUnlock(curdg->himage);
#else
		// XOR the image on the display, with zero
		// in the background (to leave the area outside
		// objects unchanged by XOR).
		curdg->binfo.col[0].rgbRed = 0;
		curdg->binfo.col[0].rgbGreen = 0;
		curdg->binfo.col[0].rgbBlue = 0;

		StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
			0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
			DIB_RGB_COLORS, SRCINVERT);
#endif

		break;

	case MARKIM:

		// Keep a copy of the binfo colour table.
		memcpy(keep_col, curdg->binfo.col, 256*sizeof(RGBQUAD));

		// Reset the binfo colour table so that
		// all entries are the marker color -
		// except zero (background).
		curdg->binfo.col[0].rgbRed = 0;
		curdg->binfo.col[0].rgbGreen = 0;
		curdg->binfo.col[0].rgbBlue = 0;
		curdg->binfo.col[1].rgbRed = GetRValue(curdg->markcol[curdg->cur_marker]);
		curdg->binfo.col[1].rgbGreen = GetGValue(curdg->markcol[curdg->cur_marker]);
		curdg->binfo.col[1].rgbBlue = GetBValue(curdg->markcol[curdg->cur_marker]);
		for (i = 2; i < 256; i++)
			curdg->binfo.col[i] = curdg->binfo.col[i - 1];

#ifdef DDGSMASKED
		// Operation is different depending on
		// image background:
		//	Black bg - AND with inverted mask then OR with image.
		//	White bg - OR with mask then AND with image.
		if ((curdg->binfo.col[0].rgbRed == 0) &&
			(curdg->binfo.col[0].rgbGreen == 0) &&
			(curdg->binfo.col[0].rgbBlue == 0))
		{
			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->minfo,
				DIB_RGB_COLORS, SRCAND);

			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCPAINT);
		}
		else
		{
			curdg->binfo.col[0].rgbRed = 255;
			curdg->binfo.col[0].rgbGreen = 255;
			curdg->binfo.col[0].rgbBlue = 255;

			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->minfo,
				DIB_RGB_COLORS, MERGEPAINT);

			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCAND);
		}
#else
		// If fluorescent, we OR the image onto
		// the black background. Otherwise, its
		// brightfield, and we AND the image with
		// the white background.
		if ((curdg->binfo.col[0].rgbRed == 0) &&
			(curdg->binfo.col[0].rgbGreen == 0) &&
			(curdg->binfo.col[0].rgbBlue == 0))
			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCPAINT);
		else
			StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
				0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
				DIB_RGB_COLORS, SRCAND);
#endif

		// Put the mask back to its normal condition
		memcpy(curdg->binfo.col, keep_col, 256*sizeof(RGBQUAD));
		break;
	}

	DOG_RDC(curdg);
} /* fastpixobj */


/*
 * 24-bit version of fastpixobj.
 * Assumes in-coming data is in 24-bit
 * RGB format already. The colourmap in
 * binfo is ignored. There is no "where"
 * parameter, since its meaningless here.
 * There is no buffered version of this (ie
 * DDGSBUFFERED is ignored).
 */
void
fastpixobj24(int vx, int vy, int w, int h,
		   unsigned char *vdata, unsigned char *mdata)
{
	HDC dc;
	HDC maskDC, dataDC;
	HBITMAP maskBmp, dataBmp;
	HGDIOBJ oldObj;

	if (curdg == NULL)
		return;

	dc = DOG_DC(curdg);
	SelectClipRgn(dc, curdg->cliprgn);

	/* Make sure the palette is in place.
	 * This allows some kind of reasonable
	 * display of probes on an 8-bit system. */
	SelectPalette(dc, ddgs_palette, 0);
	RealizePalette(dc);

	curdg->binfo.head.biWidth = w;
	curdg->binfo.head.biHeight = -h;

	/* Draw image with SRCCOPY - unlike other
	 * modes, we know probe data is pre-mixed,
	 * so don't need to worry about masking. */
	//StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
	//	0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
	//	DIB_RGB_COLORS, SRCCOPY);

	/* WH  
	** I'm afraid we do need masking, Can't use colour table for mask
	** as we are using 24 bit colour data so need a image mask (mdata).
	** other stuff hit by this are
	** canvas/24bitcanvas.c/draw_overlap_mask_buffer()
	** ddgs_get_draw_mask()
	*/
	// Mask is 8 bit
	// Operation is different depending on
	// image background:
	//	Black bg - AND with inverted mask then OR with image.
	//	White bg - OR with image.
	if ((curdg->binfo.col[0].rgbRed == 0) &&
		(curdg->binfo.col[0].rgbGreen == 0) &&
		(curdg->binfo.col[0].rgbBlue == 0))
	{


		// Create compatible DC and Bmp in memory to hold image while we work on it
		dataBmp = CreateCompatibleBitmap(dc, w, h);
		dataDC = CreateCompatibleDC(dc);
		oldObj = SelectObject(dataDC, dataBmp); 
		// BitBlt from screen to memory
		BitBlt(dataDC, 0, 0, w, h, dc, vx, curdg->maxy - vy, SRCCOPY);

		// Mask data is in 1 bits 
		curdg->binfo.head.biBitCount = 8;
		// AND image data with mask data for this object
		StretchDIBits(dataDC, 0, 0, w, h,
			0, 0, w, h, mdata, (BITMAPINFO *)&curdg->binfo,
			DIB_RGB_COLORS, SRCAND);
			
		// Image data is in 32 bits 
		curdg->binfo.head.biBitCount = 32;
		// OR this with image data for this object
		StretchDIBits(dataDC, 0, 0, w, h,
			0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
			DIB_RGB_COLORS, SRCPAINT);	// OR

		// BitBlt from meory back to screen
		BitBlt(dc, vx, curdg->maxy - vy, w, h, dataDC, 0, 0, SRCCOPY);

		oldObj = SelectObject(dataDC, oldObj); 
		DeleteObject(dataBmp); 
		DeleteDC(dataDC); 

	}
	else
	{
		// Image data is in 32 bits 
		curdg->binfo.head.biBitCount = 32;

		StretchDIBits(dc, vx, curdg->maxy - vy, w, h,
			0, 0, w, h, vdata, (BITMAPINFO *)&curdg->binfo,
			DIB_RGB_COLORS, SRCPAINT);	// OR
	//	DOG_RDC(curdg);
	}

	// SN 12Oct98 Release ddgs if neccesary
	DOG_RDC(curdg);	
}


/*
 * pixelmode
 *
 * set the pixel-drawing mode as follows (assuming ~INVERT mode):
 *
 * (1) Value inserted in all planes = greyvalue & curdg->cur_colour
 * (2) Value inserted in selected planes only = greyvalue & curdg->cur_colour
 * (3) Greyvalue & curdg->cur_colour inserted ONLY IF existing
 *			value&~curdg->cur_colour == 0
 */
int
pixelmode(int m)
{
	int	old;

	if (!curdg)
		return 0;

	old = curdg->cur_pixelmode;
	curdg->cur_pixelmode = m;
	return(old);
}

