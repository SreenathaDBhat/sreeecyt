/*
 * 	ddgstext.c	: Mark Gregson 11/8/92
 *
 *	X-Windows text display routines
 *
 * Mods:
 *
 *	26Jul99	MG		Add ddgs NULL checking at start of a number of routines 
 *	14Oct98	SN		Made _makefont nonstatic function.
 *	10Jan97	BP:		Rework for Windows with proportianal fonts etc.
 *					See comments in body. New free_fonts() function.
 *	BP	12/10/94:	Just cleanup - no functional changes.
 * 	 3 Sep 1992	MG 	textsize, loadfont routines added
 *	20 Oct 1993 MG	textsize modified to handle newlines
 */

#include <string.h>
#include "ddgsdefs.h"


#define	DDGSFONTNAME	"Arial"


// This is our current font. There is only one,
// which is re-created every time a text draw
// request comes in with a different font size.
// This font resource is only re-allocated when
// the required size changes.
static HFONT font = NULL;


// Pair of fonts used to calculate text canvas
// sizes - one for bold and one for normal.
// These are based on the largest pointsize (31), and
// other-sized text sizes are calculated by scaling
// the result.
static HFONT font_norm = NULL;
static HFONT font_bold = NULL;


// The current font ID. This is used to detect
// when the font size changes and a new font
// needs to be created.
static short fontid = 2;


// These should be OK - but point sizes don't seem
// to be the same as in X, so I scale them by about 30%...
//static short pointsize[4] = {10, 12, 16, 24};
static short pointsize[4] = {12, 15, 21, 31};


/*
 * Initialise the font system, by loading up
 * all the required fonts.
 * Retained for compatibility only.
 */
void init_fonts()
{
	return;
}


/*
 * Free the globally-allocated font resources.
 */
void free_fonts()
{
	if (font)
	{
		DeleteObject(font);
		font = NULL;
	}

	if (font_norm)
	{
		DeleteObject(font_norm);
		font_norm = NULL;
	}

	if (font_bold)
	{
		DeleteObject(font_bold);
		font_bold = NULL;
	}
}


/*
 * Create a new font resource. Its up to the
 * caller to decide whether we need to create
 * a new one or reuse the current one - this
 * function just does it, and does not have
 * memory (other than the LOGFONT contents).
 *
 *	SN 14Oct98	MAde nonstatic function so
 *				mfish\mflabel.cpp has access to it.
 *
 */
//static HFONT _makefont(short id, short scale)
HFONT _makefont(short id, short scale)
{
	static LOGFONT f;
	static BOOL initialised = FALSE;

	// All fonts are basically the same, so we
	// re-use the same data each time.
	if (!initialised)
	{
		initialised = TRUE;

		f.lfWidth = 0;
		f.lfEscapement = 0;
		f.lfOrientation = 0;
		f.lfItalic = FALSE;
		f.lfUnderline = FALSE;
		f.lfStrikeOut = FALSE;
		f.lfCharSet = ANSI_CHARSET;
		f.lfOutPrecision = OUT_TT_ONLY_PRECIS;
		f.lfClipPrecision = CLIP_DEFAULT_PRECIS;
		f.lfQuality = DRAFT_QUALITY;
		f.lfPitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
		strcpy(f.lfFaceName, DDGSFONTNAME);
	}

	// Size and weight of font is based upon
	// the base pointsize for the selected font
	// and the display scale.
	f.lfHeight = (scale*pointsize[id & 3]) >> 3;
	f.lfWeight = (id & 4) ? FW_BOLD : FW_NORMAL;

	return CreateFontIndirect(&f);
}


/*
 * text
 * print text at current position, using current colour
 * and intensity mode current position is treated as
 * bottom left corner of bounding box for the string
 */
void
_text(HDC dc, int x, int y, char *s, short scale)
{
	HGDIOBJ oldfont;
	static short prev_fontid = -1;
	static short prev_scale = -1;

	if (!curdg)
		return;

	if ((fontid != prev_fontid) ||
		(scale != prev_scale))
	{
		if (font)
			DeleteObject(font);
		font = _makefont(fontid, scale);

		prev_fontid = fontid;
		prev_scale = scale;
	}
	curdg->font = font;

	x = x >> 3;
	y = curdg->maxy - (y >> 3);
	oldfont = SelectObject(dc, font);
	SetTextAlign(dc, TA_BOTTOM + TA_LEFT);
	SetBkMode(dc, TRANSPARENT);

	TextOut(dc, x, y, s, strlen(s));

	SelectObject(dc, oldfont);
}


/* 
 * setfont
 * set font to one of ddgs preloaded fonts
 */
void
setfont(int fontnum)
{
	if ((fontnum < 0) || (fontnum >= MAXFONT))
		fontnum = 0;
	fontid = (short)fontnum;
}


/*
 * oneline_textsize
 * returns width and height of a string WITH NO LINEFEEDS in current font
 * scaled to DDGS coordinates
 */
void
oneline_textsize(char *s, int *width, int *height)
{
	SIZE sz;
	HDC dc;
	HGDIOBJ oldfont;
	HFONT f;
	int len = strlen(s);

	if (curdg == NULL)
	{
		*width = *height = 32;
		return;
	}

	// If we haven't done so already, allocate
	// the two static fonts that we use for
	// this function. These are based on the
	// largest standard pointsaize at a scale
	// of 8.
	if (font_norm == NULL)
		font_norm = _makefont(3, 8);
	if (font_bold == NULL)
		font_bold = _makefont(7, 8);

	if (fontid & 4)
		f = font_bold;
	else
		f = font_norm;

	dc = DOG_DC(curdg);
	oldfont = SelectObject(dc, f);

	if (GetTextExtentPoint32(dc, s, len, &sz))
	{
		// Scale result based on actual current
		// point size selected and that used for
		// the measurement.
		// Add a couply of pixels to the height to
		// account for any rounding errors in font
		// scaling.
		// Similarly, add a 5/8 pixel for each
		// character to allow for rounding in width.
		*width = 8 + 5*len + (8*sz.cx*pointsize[fontid & 3])/pointsize[3];
		*height = 8 + (8*sz.cy*pointsize[fontid & 3])/pointsize[3];
	}
	else
		*width = *height = 32;

	SelectObject(dc, oldfont);
	DOG_RDC(curdg);
}


/*
 *	T E X T S I Z E		
 *				returns width and height of string in current font
 *				scaled to DDGS coordinates
 *				HANDLES LINEFEEDS (MG)
 */

void textsize(char *s, int *width, int *height)
{
	int l,newlines,maxwidth;
	int lwidth, lheight;
	char *c, *d;
	char teststr[]="TestString";


	if ((s==NULL) || (strlen(s)==0))
	{
		*width=32;
		*height=32;
		return;
	}


	c = d = s;
	newlines=1;
	maxwidth=1;
	while (*c != '\0')
	{
		if (*c == '\n')
		{
			*c = '\0';
			newlines++;
			l = strlen(d);

			if (l>0)
			{
				oneline_textsize(d, &lwidth, &lheight); 
				if (lwidth>maxwidth)
					maxwidth=lwidth;
			}

			*c = '\n';
			d = c+1;
		}
		c++;
	}

	if (c!=d)
	{
		l = strlen(d);

		if (l>0)
		{
			oneline_textsize(d, &lwidth, &lheight); 
			if (lwidth>maxwidth)
				maxwidth=lwidth;
		}
	}

	oneline_textsize(teststr, &lwidth, &lheight); 
	*height = lheight * newlines;
	*width = maxwidth;
}

