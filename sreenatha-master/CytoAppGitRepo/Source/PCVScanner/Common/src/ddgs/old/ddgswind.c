/*
 * ddgswind.c 	: Mark Gregson 11/8/92
 *	
 * X-Windows version
 *
 * Provide window clearing and restoring functions.
 * These functions operate on all PLANEMASK planes. 
 * window() sets the clipping area for all drawing
 * routines, however it has no effect on the other
 * routines in this module.
 *
 */


#include "ddgsdefs.h"


/*
 * 	W I N D O W --	Set clipping area for current ddgs window
 *
 */
#ifdef WIN32
void
window(int xl, int xr, int yb, int yt)
#endif
#ifdef i386
void
window(xl, xr, yb, yt)
int xl;
int xr;
int yb;
int yt;
#endif
{
	
	curdg->cliprect.x=xl >> XSHIFT;
	curdg->cliprect.y=curdg->maxy - (yt >> YSHIFT);
	curdg->cliprect.width=((xr-xl)+1) >> XSHIFT;
	curdg->cliprect.height=((yt-yb)+1) >> YSHIFT;
	if (curdg->cliprect.width<1)
		curdg->cliprect.width=1;
	if (curdg->cliprect.height<1)
		curdg->cliprect.height=1;

	XSetClipRectangles(	curdg->display,
				curdg->gc,
				0,
				0,
				&curdg->cliprect,
				1,
				Unsorted );

}



/*
 *	W C S  --  all singing, all dancing window save function 
 *                 called by wclear, wsave & wclearsave
 *
 */
#ifdef WIN32
struct ddgs_save *
wcs(int xsize, int ysize, int mode, int val)
#endif
#ifdef i386
struct ddgs_save *
wcs(xsize, ysize, mode, val)
int xsize;
int ysize;
int mode;
int val;
#endif
{
	struct ddgs_save 	*w;
	register int 		x, devxstart, devxtop, devystart;
	int 			devxsize, devysize, devytop;
	XImage			*imbuf;
	XWindowAttributes	winatt;

	devxsize = (xsize+7)>>XSHIFT;
	devysize = (ysize+7)>>YSHIFT;
	devystart = curdg->cur_y>>YSHIFT;
	devytop = devystart + devysize - 1;
	if (devystart < curdg->miny)
		devystart = curdg->miny;
	if (devytop > curdg->maxy)
		devytop = curdg->maxy;

	devxstart = curdg->cur_x>>XSHIFT;
	devxtop = devxstart + devxsize - 1;
	if (devxstart < curdg->minx)
		devxstart = curdg->minx;
	if (devxtop > curdg->maxx)
		devxtop = curdg->maxx;
	devxsize = devxtop-devxstart+1;
	devysize = devytop-devystart+1;

	if (mode < 2) {
		w =(struct ddgs_save *) XtMalloc(sizeof(struct ddgs_save));

		if (w==NULL) {
			fprintf(stderr,"DDGS could not allocate wcs() ddgs_save structure");
			return(NULL);
		}
		w->xo = devxstart;
		w->yo = devystart;
		w->xs = devxsize;
		w->ys = devysize;

		/* now because ddgs is upside down -- */
		devytop=(curdg->maxy-devystart) - (devysize-1);

		XGetWindowAttributes(curdg->display, curdg->window, &winatt );
	
		imbuf=XCreateImage(	curdg->display,
					winatt.visual,
					winatt.depth,
					ZPixmap,
					0, NULL, devxsize, devysize, 8, 0);

		imbuf->data=(char *)XtMalloc(devxsize*devysize*sizeof(unsigned char));
		w->buf=(unsigned char *)imbuf->data;

		if (imbuf->data==NULL) {
			fprintf(stderr,"DDGS could not allocate wcs image buffer");
		}

		else {

			XGetSubImage(	curdg->display,
					curdg->window,
					devxstart,			
					devytop,
					devxsize,
					devysize,
					pmask,		/* 63 for grey, 128 for overlays */
					ZPixmap,
					imbuf, 0, 0);
		}

		
		/* destroy Ximage structure but not buffer data */
		imbuf->data=NULL;
		XDestroyImage(imbuf);
	}
			
	if (mode > 0) {
		/* fill window with val */
	
		ifill(	devxstart,
			devytop,
			devxsize,
			devysize,
			colourtab[val]);
	}

	if (mode < 2)
		return(w);
}

/*
 *	W S A V E  --  save a window at current position, size (xsize,ysize)
 *
 */
#ifdef WIN32
struct ddgs_save *
wsave(int xsize, int ysize)
#endif
#ifdef i386
struct ddgs_save *
wsave(xsize, ysize)
int xsize;
int ysize;
#endif
{
	return(wcs(xsize,ysize,0,0));
}

/*
 *	W C L E A R S A V E  --	save a window and clear to val at current
 *							position, size (xsize,ysize)
 *
 */
#ifdef WIN32
struct ddgs_save *
wclearsave(int xsize, int ysize, int val)
#endif
#ifdef i386
struct ddgs_save *
wclearsave(xsize, ysize, val)
int xsize;
int ysize;
int val;
#endif
{
	return(wcs(xsize,ysize,1,val));
}

/*
 *	W C L E A R  --	clear a window to val at current position, size (xsize,ysize)
 *
 */
#ifdef WIN32
void
wclear(int xsize, int ysize, int val)
#endif
#ifdef i386
void
wclear(xsize, ysize, val)
int xsize;
int ysize;
int val;
#endif
{
	wcs(xsize,ysize,2,val);
}

/*
 *	W R E S T O R E  --  restore window wb.
 *
 * If mode=0, at saved position, free buffer.
 * If mode=1, at current position, free buffer.
 * If mode=2, at saved position, don't free buffer.
 * If mode=3, at current position, don't free buffer.
 *
 */
#ifdef WIN32
void
wrestore(struct ddgs_save *wb, int mode)
#endif
#ifdef i386
void
wrestore(wb, mode)
struct ddgs_save *wb;
int mode;
#endif
{
	register 		devxstart, devystart, devytop;
	XImage			*imbuf;
	XWindowAttributes	winatt;
	XRectangle		rectangle;

	switch (mode) {
		case 0:
		case 2:
		default:
			devxstart = wb->xo;
			devystart = wb->yo;
			break;
		case 1:
		case 3:
			devxstart = curdg->cur_x>>XSHIFT;
			devystart = curdg->cur_y>>YSHIFT;
			break;
	}

	/* restore window  -- */


	/* set gc drawing mode to copy and gc clip area to full window */
	XSetFunction(curdg->display,curdg->gc,GXcopy);

	rectangle.x=0;
	rectangle.y=0;
	rectangle.width=curdg->lineln;
	rectangle.height=curdg->lineno;
	XSetClipRectangles(curdg->display, curdg->gc, 0, 0, &rectangle, 1, Unsorted);
		
	/* create temp Ximage structure  -  imbuf  */
	XGetWindowAttributes(curdg->display, curdg->window, &winatt );

	imbuf=XCreateImage(	curdg->display,
				winatt.visual,
				winatt.depth,
				ZPixmap,
				0, wb->buf, wb->xs, wb->ys, 8, 0);

	/* copy image buffer to ddgs window */

	devytop=(curdg->maxy-devystart) - (wb->ys-1);

	XPutImage( 	curdg->display, 
			curdg->window, 
			curdg->gc, 
			imbuf, 
			0,
		 	0, 
			devxstart, 
			devytop,
			wb->xs, 
			wb->ys);

	/* destroy Ximage structure, but not data */
	imbuf->data=NULL;
	XDestroyImage(imbuf);								


	/* copy window to backup pixmap */
	if (curdg->backup==TRUE)
		XCopyArea(	curdg->display,
				curdg->window,
				curdg->map,
				curdg->gc,
				devxstart,
				devytop, 
				wb->xs, 
				wb->ys, 
				devxstart,
				devytop);

	/* restore old gc drawing mode, and clip area  */
	intens(curdg->cur_intensity);
	XSetClipRectangles(curdg->display, curdg->gc, 0, 0, &curdg->cliprect, 1, Unsorted);

	/* depending on mode free image data buffer */
	switch(mode) {
		case 0:
		case 2:
			XtFree(wb->buf);
			XtFree(wb);
			break;
		case 1:
		case 3:
		default:
			break;
	}
}

