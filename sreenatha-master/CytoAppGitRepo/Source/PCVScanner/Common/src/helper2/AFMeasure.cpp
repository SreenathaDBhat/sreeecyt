////////////////////////////////////////////////////////////////////////////////////////
// Autofocus measurements - implementation
// K Ratcliff
////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AFMeasure.h"
#include "QuantileMeasure.h"
#include <omp.h>


#include "limits.h"
#include <math.h>
#include "float.h"



// sorts two colors in place, putting smallest in a
_inline void sort2(BYTE *a, BYTE *b)
{
    BYTE cmin;
    BYTE cmax; 
    
	if (*a < *b)
	{
		cmin = *a;
		cmax = *b;
	}
	else
	{
		cmin = *b;
		cmax = *a;
	}
    *a = cmin;
    *b = cmax;
}

BYTE *Separable3by3Median(BYTE *pImage, int Width, int Height)
{
	BYTE *pIm1, *pg1, *pg2, *pg3, g1, g2, g3, *pd, *pIm2;
	int x, y;

	pIm1 = new BYTE[Width * Height];
	memcpy(pIm1, pImage, Width * Height);

	// Horizontal
	for (y = 0; y < Height; y++)
	{
		pg1 = pImage + y * Width;
		pg2 = pg1 + 1;
		pg3 = pg1 + 2;
		pd  = pIm1 + y * Width + 1;
		for (x = 1; x < Width - 1; x++)
		{	
			g1 = *pg1++;
			g2 = *pg2++;
			g3 = *pg3++;
			
			sort2(&g2, &g3);
			sort2(&g1, &g2);
			sort2(&g2, &g3);

			*pd++ = g2;
		}
	}

	pIm2 = new BYTE[Width * Height];
	memcpy(pIm2, pIm1, Width * Height);

	// Vertical
	for (y = 1; y < Height - 1; y++)
	{
		pg1 = pIm1 + (y - 1) * Width;
		pg2 = pg1 + Width;
		pg3 = pg2 + Width;
		pd  = pIm2 + y * Width;
		for (x = 0; x < Width; x++)
		{	
			g1 = *pg1++;
			g2 = *pg2++;
			g3 = *pg3++;
			
			sort2(&g2, &g3);
			sort2(&g1, &g2);
			sort2(&g2, &g3);

			*pd++ = g2;
		}
	}

	delete pIm1;

	return pIm2;
}

// sorts two colors in place, putting smallest in a
_inline void sort2(WORD *a, WORD *b)
{
    WORD cmin;
    WORD cmax; 
    
	cmin = min(*a, *b);
	cmax = max(*a, *b);
					
    *a = cmin;
    *b = cmax;
}

WORD *Separable3by3Median(WORD *pImage, int Width, int Height)
{
	WORD *pIm1, *pIm2;
	
	WORD *pg1, *pg2, *pg3, g1, g2, g3, *pd;
	int x, y;

	pIm1 = new WORD[Width * Height];
	memcpy(pIm1, pImage, Width * Height * sizeof(WORD));
 
	// Horizontal
	for (y = 0; y < Height - 1; y++)
	{
		pg1 = pImage + y * Width;
		pg2 = pg1 + 1;
		pg3 = pg1 + 2;
		pd  = pIm1 + y * Width + 1;
		for (x = 1; x < Width - 1; x++)
		{	
			g1 = *pg1++;
			g2 = *pg2++;
			g3 = *pg3++;

			sort2(&g2, &g3);
			sort2(&g1, &g2);
			sort2(&g2, &g3);

			*pd++ = g2;
		}
	}

	pIm2 = new WORD[Width * Height];
	memcpy(pIm2, pIm1, Width * Height);


	// Horizontal
	for (y = 1; y < Height - 2; y++)
	{
		pg1 = pIm1 + (y - 1) * Width;
		pg2 = pg1 + Width;
		pg3 = pg2 + Width;
		pd  = pIm2 + y * Width;
		for (x = 0; x < Width; x++)
		{	
			g1 = *pg1++;
			g2 = *pg2++;
			g3 = *pg3++;

			sort2(&g2, &g3);
			sort2(&g1, &g2);
			sort2(&g2, &g3);

			*pd++ = g2;
		}
	}
	
	delete pIm1;

	return pIm2;
}

// 8 bit version

double AutoFocusMeasure8(BYTE *pImage, int Width, int Height, int Left, int Top, int Right, int Bottom, int Spacing, int Thresh)
{
    int X, Y, H;
    BYTE *pG1, *pG2, g1, g2;
    int g, gs;
    double sj = 0.;
    double si;

	// Median filter the image
	BYTE *pMed = Separable3by3Median(pImage, Width, Height);

	// Do the measurement
    for (Y = Top + Spacing; (Y < Bottom - Spacing)&(Y < Height); Y++)
    {
        H = Y * Width + Left + 1;
        pG1 = pMed + H;
        pG2 = pMed + H + Spacing;
        si = 0.;
        for (X = Left + Spacing; (X < Right - Spacing)&(X < Width); X++)
        {
            g1 = *pG1++;
            g2 = *pG2++;
            g = g1 - g2;
            gs = g * g;
            if (gs > Thresh)
                si += gs;
        }

        sj += si;
    }

	delete pMed;

    return (sj / ((Right - Left) * (Bottom - Top)));
}

// 8 bit version - median filter outside routine

double AutoFocusMeasureZoned8(BYTE *pImage, int Width, int Height, int Left, int Top, int Right, int Bottom, int Thresh)
{
    int X, Y, H;
    BYTE *pG1, *pG2, g1, g2;
    int g, gs;
    double sj = 0.;
    double si;

	// Do the measurement
    for (Y = Top + 1; (Y < Bottom - 1)&(Y < Height); Y++)
    {
        H = Y * Width + Left + 1;
        pG1 = pImage + H;
        pG2 = pG1 + 1;
        si = 0.;
        for (X = Left + 1; (X < Right - 1)&(X < Width); X++)
        {
            g1 = *pG1++;
            g2 = *pG2++;
            g = g1 - g2;
            gs = g * g;
            if (gs > Thresh)
                si += gs;
        }

        sj += si;
    }

    return (sj / ((Right - Left) * (Bottom - Top)));
}


double AFM16(WORD *pMed, int Width, int Height, int Left, int Top, int Right, int Bottom,  int Thresh)
{
	int X, Y, H;
    WORD *pG1, *pG2, g1, g2;
    int g, gs;
    double sj = 0.;
    double si;

	
	// Do the measurement
    for (Y = Top; (Y < Bottom) &&(Y < Height - 1); Y++)
    {
        H = Y * Width + Left + 1;
        pG1 = pMed + H;
        pG2 = pMed + H + 1;
		
        si = 0.;
        for (X = Left; (X < Right - 1) && (X < Width - 1); X++)
        {
            g1 = *pG1++;
            g2 = *pG2++;
            g = g1 - g2;
            gs = g * g;
            if (gs > Thresh)
                si += gs;
			
        }

        sj += si;
    }

	
    return (sj / ((Right - Left) * (Bottom - Top)));
}

// Same as above but does horizontal and vertical
double AFM16HV(WORD *pMed, int Width, int Height, int Left, int Top, int Right, int Bottom,  int Thresh)
{
	int X, Y, H;
    WORD *pG1, *pG2, g1, g2, *pG3;
   
    int g, gs;
    double sj = 0.;
    double si;

	
	// Do the measurement
    for (Y = Top; (Y < Bottom) &&(Y < Height - 1); Y++)
    {
        H = Y * Width + Left + 1;
        pG1 = pMed + H;
        pG2 = pMed + H + 1;
		pG3 = pG1 + Width;
		
        si = 0.;
        for (X = Left; (X < Right - 1) && (X < Width - 1); X++)
        {
            g1 = *pG1++;
            g2 = *pG2++;
            g = g1 - g2;
            gs = g * g;
            if (gs > Thresh)
                si += gs;
			g2 = *pG3++;
			g = g1 - g2;
			gs = g * g;
			if (gs > Thresh)
				si += gs;
			
        }

        sj += si;
    }

	
    return (sj / ((Right - Left) * (Bottom - Top)));
}
// > 8 bits and <= 16 bit version

double AutoFocusMeasure16(WORD *pImage, int Width, int Height, int Left, int Top, int Right, int Bottom, int Thresh)
{
   
	// Median filter the image
	WORD *pMed = Separable3by3Median((WORD *)pImage, Width, Height);

	// Do the measurement
	double AFM = AFM16((WORD *)pMed, Width, Height, Left, Top, Right, Bottom, Thresh);

	delete pMed;

    return AFM;
}

double AutoFocusMeasure16HV(WORD *pImage, int Width, int Height, int Left, int Top, int Right, int Bottom, int Thresh)
{
   
	// Median filter the image
	WORD *pMed = Separable3by3Median((WORD *)pImage, Width, Height);

	// Do the measurement
	double AFM = AFM16HV((WORD *)pMed, Width, Height, Left, Top, Right, Bottom, Thresh);

	delete pMed;

    return AFM;
}

void ZonedAutoFocusMeasure16(WORD *pImage, int Width, int Height, int NZonesX, int NZonesY, int Thresh, double *pAFM)
{
   
	// Median filter the image
	WORD *pMed = Separable3by3Median(pImage, Width, Height);

	
	for (int i = 0; i < NZonesY; i++)
	{
		int Top = (i * Height) / NZonesY;
		int Bottom = Top + Height / NZonesY; 		
		for (int j = 0; j < NZonesX; j++)
		{
			int Left = (j * Width) / NZonesX;
			int Right = Left + Width / NZonesX;

			pAFM[i * NZonesX + j] = AFM16(pMed, Width, Height, Left, Top, Right, Bottom, Thresh);

		}
	}

	delete pMed;

   
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Morphological operations on 1D data
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Equivalent of FSMin

double *OneDMin(double *Data, int Length, int Width)
{
    double *pResult;
    int i, j;
    double Min;
    
    pResult = new double[Length];

    // Deal with data at ends
    for (i = 0; i < Width; i++)
    {
        Min = DBL_MAX;
        for (j = i; j < i + Width; j++)
        {
            if (Data[j] < Min)
                Min = Data[j];
        }

        pResult[i] = Min;
    }
    j = Length - Width;
    // Deal with data in the middle
    for (i = Width; i < Length - Width; i++)
    {
        Min = DBL_MAX;
        for (j = i - Width; j < i + Width; j++)
        {
            if (Data[j] < Min)
                Min = Data[j];
        }
        pResult[i] = Min;
    }
    // Deal with data at ends
    Min = DBL_MAX;
    for (j = Length - Width; j < Length; j++)
    {
        if (Data[j] < Min)
            Min = Data[j];
    }
    // Deal with data at ends
    for (j = Length - Width; j < Length; j++)
    {
        pResult[j] = Min;
    }

    return pResult;
}

// Equivalent of FSMax

double *OneDMax(double *Data, int Length, int Width)
{
    double *pResult;
    int i, j;
    double Max;
    
    pResult = new double[Length];

    // Deal with data at ends
    for (i = 0; i < Width; i++)
    {
        Max = DBL_MIN;
        for (j = i; j < i + Width; j++)
        {
            if (Data[j] > Max)
                Max = Data[j];
        }

        pResult[i] = Max;
    }

    j = Length - Width;
    // Deal with data in the middle
    for (i = Width; i < Length - Width - 1; i++)
    {
        Max = DBL_MIN;
        for (j = i - Width - 1; j < i + Width; j++)
        {
            if (Data[j] > Max)
                Max = Data[j];
        }
        pResult[i] = Max;
    }
    Max = DBL_MIN;
    for (j = Length - Width - 1; j < Length; j++)
    {
        if (Data[j] > Max)
            Max = Data[j];
    }
    // Deal with data at ends
    for (j = Length - Width - 1; j < Length; j++)
    {
        pResult[j] = Max;
    }

    return pResult;
}

double *OneDClose(double *Data, int Length, int Width)
{
    double *pF1, *pF2;

    pF1 = OneDMax(Data, Length, Width);
    pF2 = OneDMin(pF1, Length, Width);

    delete pF1;

    return pF2;
}

double *OneDOpen(double *Data, int Length, int Width)
{
    double *pF1, *pF2;

    pF1 = OneDMin(Data, Length, Width);
    pF2 = OneDMax(pF1, Length, Width);

    delete pF1;

    return pF2;
}

double *OneDExtract(double *Data, int Length, int Width, int NoiseWidth)
{
    double *pF1, *pF2, *pF3;
    int i;
    double Min;

    pF1 = OneDClose(Data, Length, NoiseWidth);
    pF2 = OneDOpen(pF1, Length, Width);
    delete pF1;

    pF3 = new double[Length];

    memcpy(pF3, Data, sizeof(double) * Length);
    for (i = 0; i < Length; i++)
    {
        Min = (Data[i] < pF2[i]) ? Data[i] : pF2[i];
        pF3[i] -= Min;
    }

    delete pF2;

    return pF3;
}


double AFMomentThreshold(BYTE *Addr, int Width, int Height)
{
    int		i, y, n;
    int		Thresh = -1;
    BYTE	*ImPtr;
    DWORD	Hist[256];
    double	prob[256];
    double  m1, m2, m3, cd, c0, c1, z0, z1, p0, p1, pd, pDistr;

	// Median filter the image
	BYTE *pMed = (BYTE *)Separable3by3Median(Addr, Width, Height);

    // compile histogram 
    for (i = 0; i < 256; i++)
        Hist[i] = 0;

    n = Width * Height;
    ImPtr = Addr;
    for (y = 0; y < n; y++) 
        Hist[*ImPtr++]++;
       
    // compute probabilities 
    for (i = 0; i < 256; i++)
        prob[i] = (double) Hist[i] / (double) n;

    // calculate first 3 moments 
    m1 = m2 = m3 = 0.0;
    for (i = 0; i < 256; i++) 
    {
        m1 += i * prob[i];
        m2 += i * i * prob[i];
        m3 += i * i * i * prob[i];
    }

    cd = m2 - m1 * m1;
    c0 = (-m2 * m2 + m1 * m3) / cd;
    c1 = (-m3 + m2 * m1) / cd;
    z0 = 0.5 * (-c1 - sqrt (c1 * c1 - 4.0 * c0));
    z1 = 0.5 * (-c1 + sqrt (c1 * c1 - 4.0 * c0));

    pd = z1 - z0;
    p0 = (z1 - m1) / pd;
    p1 = 1.0 - p0;

    pDistr = 0.0;

    // find threshold 
    for (Thresh = 0; Thresh < 256; Thresh++) 
    {
        pDistr += prob[Thresh];
        if (pDistr > p0)
            break;
    }

    delete pMed;

    return pDistr;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double Vollath_F4(WORD *pImage, int Width, int Height)
{
	int X, Y, H;
    WORD *pG1, *pG2, g1, g2;
    int g;
    double Sig1 = 0., Sig2 = 0.;
    double si;

	// Median filter the image
	WORD *pMed = Separable3by3Median(pImage, Width, Height);

	// Do the measurement sum(I(x,y) * I(x + 1, y))
    for (Y = 0; Y < Height; Y++)
    {
        H = Y * Width;
        pG1 = pMed + H;
        pG2 = pMed + H + 1;
        si = 0.;
        for (X = 0; X < Width - 2; X++)
        {
            g1 = *pG1++;
            g2 = *pG2++;
            g = g1 * g2;
    		if (g > 2)
                si += g;
        }

        Sig1 += si;
    }

	// Do the measurement sum(I(x,y) * I(x + 2, y))
    for (Y = 0; Y < Height; Y++)
    {
        H = Y * Width;
        pG1 = pMed + H;
        pG2 = pMed + H + 2;
        si = 0.;
        for (X = 0; X < Width - 2; X++)
        {
            g1 = *pG1++;
            g2 = *pG2++;
            g = g1 * g2;
            if (g > 2)
                si += g;

        }

        Sig2 += si;
    }


	delete [] pMed;

	// sum(I(x,y) * I(x + 1, y)) - sum(I(x,y) * I(x + 2, y))
	return (Sig1 - Sig2) / (Width * Height);
	
}