//////////////////////////////////////////////////////////////////////////////////////////
// Looks at a bi-modal type B/W image and determines the peaks at either ends 
// of the histogram
//////////////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "math.h"
#include "BiModalMeasure.h"

// 8 bit version

void BiModalMeasure8(BYTE *pBuf, int size, WORD *LowPeak, WORD *HiPeak)
{
    int histo[256];
    WORD i, Min, Max, Mid;
    BYTE G;
    BYTE *ptr;

    Min = 65535;
    Max = 0;

    for (i=0; i<=255; i++) histo[i]=0;
    ptr = pBuf;
    for (int j = 0; j <= size; j++)
    {
        G = *ptr++;
        histo[G]++;

        // Min and max grey levels
        if (G > Max)
            Max = G;
        else
        if (G < Min)
            Min = G;
    }

    // Work out the middle of the range
    Mid = (Max + Min) / 2;

    for (int j = 0; j < 3; j++)
        for (i = 1; i <= 254; i++) 
            histo[i]=(histo[i-1]+histo[i]+histo[i+1])/3;
    
    int peaki=0, peakv=0;
    for (i = Mid; i <= Max; i++)
    {
        if (histo[i] > peakv)
        {
            peakv=histo[i];
            peaki=i;
        }
    }

    *HiPeak = peaki;

    peaki=0; peakv=0;
    for (i = Min; i <= Mid; i++)
    {
        if (histo[i]>peakv)
        {
            peakv=histo[i];
            peaki=i;
        }
    }

    *LowPeak = peaki;
}

// >8  bit version

void BiModalMeasure16(const WORD *pBuf, int Width, int Height, int BitDepth, WORD *LowPeak, WORD *HiPeak)
{
    unsigned int i, Min, Max, Mid;
    int j,k;
    unsigned int G;
    unsigned int Intensities;
	int size = Width * Height;
	const WORD *pImgBuf;
	int x,y;

    Intensities = (WORD) pow(2.0, BitDepth);
    
    unsigned int *pHist = new unsigned int[Intensities];

    Min = 65535;
    Max = 0;

    memset(pHist, 0, sizeof(unsigned int) * Intensities);

	if (Width >= 1000) {
			//a large FOV will give a multi-modal histogram, due to non-uniform
			//image illumination etc. Thus the highest (white)peak may not be the one
			//we actually want. Concentrating on the central region of the
			//image to should reduce the effects of shading  
		x = Width / 3;
		y = Height / 3;
		}
	else {
		x = y = 0;
		}
{
char string[256];
sprintf (string,"BiModalMeasure16 : Width %d X %d Height %d Y %d\n",Width,x, Height, y);
TRACE (string);
}


	for (j = y; j < Height-y; j++) {
		pImgBuf = pBuf + (j * Width + x);
		for (k = x; k < Width-x; k++) {
			G = *pImgBuf++;
		    pHist[G] = pHist[G] + 1;

		   // Min and max grey levels
		   if (G > Max)
		       Max = G;
		    if (G < Min)
		       Min = G;
			}

		}
	

    // Work out the middle of the range
    Mid = (Max + Min) / 2;
{
char string[256];
sprintf (string,"BiModalMeasure16 : Range %d  %d Mid %d\n",Min, Max, Mid);
TRACE (string);
}


    for (j = 0; j < 3; j++)
        for (i = 1; i <= Intensities - 2; i++) 
            pHist[i]=(pHist[i-1] + pHist[i] + pHist[i+1])/3;
    
    unsigned int peaki=0, peakv=0;
    for (i = Mid; i <= Max; i++)
    {
        if (pHist[i] > peakv)
        {
            peakv = pHist[i];
            peaki = i;
        }
    }

    *HiPeak = peaki;

    peaki = 0; 
    peakv = 0;
    for (i = Min; i <= Mid; i++)
    {
        if (pHist[i]>peakv)
        {
            peakv = pHist[i];
            peaki = i;
        }
    }

    delete [] pHist;

    *LowPeak = peaki;
}
