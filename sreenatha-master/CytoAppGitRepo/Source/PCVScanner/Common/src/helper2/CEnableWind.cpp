//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Disable/Enable all child windows of a window remembering their state so that they can be restored
// at a later time
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//


#include "stdafx.h"
#include "CEnableWnd.h"

CEnableWnd::CEnableWnd(CWnd *pParent)
{ 
    m_pParent = pParent; 
}

CEnableWnd::~CEnableWnd()
{
}

//////////////////////////////////////////////////////////////////////////////////////
// NOTE Must call this with bEnable = FALSE first time through
//////////////////////////////////////////////////////////////////////////////////////

void CEnableWnd::EnableAll(BOOL bEnable)
{
    if (!bEnable) 
        Enable(FALSE); 
    else 
        Restore(); 
}

//////////////////////////////////////////////////////////////////////////////////////
// Enable/Disable ALL child controls on a page
//////////////////////////////////////////////////////////////////////////////////////

void CEnableWnd::Enable(BOOL bEnable)
{
    m_bEnable = bEnable;
    EnumChildWindows(m_pParent->GetSafeHwnd(), EnableChildProc, (LPARAM) this); 
}

//////////////////////////////////////////////////////////////////////////////////////
// Restore the controls back to their previous state - provided enable has
// been called first of course !
//////////////////////////////////////////////////////////////////////////////////////

void CEnableWnd::Restore(void)
{
    BOOL State;
    HWND hwndChild;
    POSITION P = m_WndBank.GetStartPosition();
    while (P)
    {
        m_WndBank.GetNextAssoc(P, hwndChild, State);
        CWnd *pWnd = CWnd::FromHandle(hwndChild);

        if (pWnd)
            pWnd->EnableWindow(State);
    }
}

//////////////////////////////////////////////////////////////////////////////////////
// Enable/Disable child controls on a page
//////////////////////////////////////////////////////////////////////////////////////

BOOL CALLBACK CEnableWnd::EnableChildProc(HWND hwndChild, LPARAM lParam) 
{
    CWnd *pWnd = CWnd::FromHandle(hwndChild);
    CEnableWnd *pThis = (CEnableWnd *) lParam;

    if (pWnd)
    {
        BOOL State;

        if (!pThis->m_WndBank.Lookup(hwndChild, State))
        {
            State = pWnd->IsWindowEnabled();
            pThis->m_WndBank.SetAt(hwndChild, State);
        }

        pWnd->EnableWindow(pThis->m_bEnable);
    }

    return TRUE; 
}






