#include "StdAfx.h"
#include "camlut.h"
#include "CommonDefs.h"
#include <math.h>

///////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////////

CCamLUT::CCamLUT(int GainSteps, int OffsetSteps, WORD bitdepth, WORD outputbitdepth) : CLUT(bitdepth)
{
    // Create our native LUT
    m_pNativeLUT = new CLUT(bitdepth);
		
    // Calc the number of intensity values
    m_Intensities = (int) pow(2.0, (double) bitdepth);
    // Store bit depths
    m_BitDepth = bitdepth;
    m_OutputBitDepth = outputbitdepth;

    // Calc the output intensity range
    m_OutputIntensities = m_Intensities >> (m_BitDepth - m_OutputBitDepth);

	m_UseGainOffset = (m_BitDepth > 8) ? TRUE : FALSE;	 

    // Calc the max gain slope 
    m_MaxGainSlope = 1.0;
    if (m_OutputIntensities != m_Intensities)
	{
		switch (bitdepth)
		{
		case 10:
			m_MaxGainSlope = 0.5 * (m_Intensities / m_OutputIntensities);
			break;

		case 12:
			m_MaxGainSlope = 0.25 * (m_Intensities / m_OutputIntensities);	// Higher bit depth camera try and use more exposure 
			break;
		}
	}

    m_GainSteps =  (double) GainSteps;
    m_OffsetSteps = (double) OffsetSteps;

    // White balancing
    m_SpecGradient = 1.0;
    m_SpecIntersection = 0.0;

    m_Gain = 1.0;
    m_Offset = 0.0;
	m_Gamma = 1.0;

    // Default to 1:1
    Default();
}




// 8 bit version
CCamLUT::CCamLUT(void) : CLUT(8)
{
    // Calc the number of intensity values
    m_Intensities = 256;
    // Store bit depths
    m_BitDepth = 8;
    m_OutputBitDepth = 8;

    m_UseGainOffset = FALSE;

    // Calc the output intensity range
    m_OutputIntensities = 256;

    // White balancing
    m_SpecGradient = 1.0;
    m_SpecIntersection = 0.0;

    // Set these even if not applied
    m_MaxGainSlope = 1.0;
    m_GainSteps = 0.0;
    m_OffsetSteps = 0.0;
	m_Gamma = 1.0;

    // Native LUT = none if 8 bits
    m_pNativeLUT = NULL;
}

CCamLUT::~CCamLUT(void)
{
    SAFE_DELETE(m_pNativeLUT);
}


///////////////////////////////////////////////////////////////////////////////////
// Change the slope depending on the mode
///////////////////////////////////////////////////////////////////////////////////
void CCamLUT::FluorescentMode(BOOL On)
{
	if (m_OutputIntensities != m_Intensities)
	{
		switch (m_BitDepth)
		{
		case 10:
			if (On)
			{
				m_MaxGainSlope = 1.0 * (m_Intensities / m_OutputIntensities);
			}
			else
			{
				m_MaxGainSlope = 0.5 * (m_Intensities / m_OutputIntensities);
			}
			break;

		case 12:
			m_MaxGainSlope = 0.25 * (m_Intensities / m_OutputIntensities);
			break;
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////
// Default to a straight forward N to 8 bit LUT conversion
///////////////////////////////////////////////////////////////////////////////////

void CCamLUT::Default(void)
{
    WORD i;
    WORD BitShift = m_BitDepth - m_OutputBitDepth;

    for (i = 0; i < m_Intensities; i++)
    {
        m_pLUT[i] = i >> BitShift;      // m_BitDepth bit to m_OutputBitDepth LUT
        m_pNativeLUT->m_pLUT[i] = i;    // 1:1 look up
    }
}

///////////////////////////////////////////////////////////////////////////////////
// Multiply this LUT by another LUT specified by MultLUT
// NOTE BE CAREFUL that the multiplying LUT has the same bitdepth
// is the same as the output bit depth for this LUT otherwise this does 
// nothing at all
///////////////////////////////////////////////////////////////////////////////////

CCamLUT *CCamLUT::operator*(CLUT *pMultLUT)
{
    WORD i;
    CCamLUT *pC;
    
    pC = new CCamLUT(255, 255, m_BitDepth, pMultLUT->m_BitDepth);

    if (m_OutputBitDepth == pMultLUT->m_BitDepth)
    {
        // Do the transform
        for (i = 0; i < m_Intensities; i++)
            pC->m_pLUT[i] = pMultLUT->m_pLUT[m_pLUT[i]];
    }

    return pC;
}

///////////////////////////////////////////////////////////////////////////////////
// Return current gain and offsets
///////////////////////////////////////////////////////////////////////////////////

WORD CCamLUT::GetGain(void)
{
    return (WORD) m_Gain;
}

WORD CCamLUT::GetOffset(void)
{
    return (WORD) m_Offset;
}

double CCamLUT::GetMaxGainSlope(void)
{
    return m_MaxGainSlope;
}
    
double CCamLUT::GainSteps(void)
{
    return m_GainSteps;
}
    
WORD CCamLUT::OutputIntensities(void)
{
    return m_OutputIntensities;
}

///////////////////////////////////////////////////////////////////////////////////
// Apply input LUT to an image. Uses a gain and offset to control the slope of the LUT
// and the offset similar to y = mx + c i.e. output = gain * input + offset
///////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////
// Create the LUT from the gain and offset values
// gn = gain, ofs = offset
///////////////////////////////////////////////////////////////////////////////////

void CCamLUT::Calc(int gn, int ofs)
{
    WORD i;   
    double g;
    double BitShift;
    double Gain, Div, Gain28;
    double Offset, Offset28;

	/*TCHAR Dbg[1024];

	_stprintf(Dbg, _T("CAMLUT: MAX GAIN SLOPE %8.3lf\r\n"), m_MaxGainSlope);
	OutputDebugString(Dbg);
*/

    // Amount to shift result to get the output bit depth
    BitShift = (double) (m_BitDepth - m_OutputBitDepth);
    Div = pow(2.0, BitShift);

    m_Gain = gn;                        // Keep the non spec corrected gain
    m_Offset = ofs;

    if (m_UseGainOffset)
    {
        // Calc the gain slope
        Gain = 1.0 + (m_MaxGainSlope * (double) (gn)) / (double) m_GainSteps;  
        Gain28 = Gain / Div;            // Remember we're going down to m_OutputBitDepth bits 
        Gain28 *= m_SpecGradient;
        Gain *= m_SpecGradient;

        // Calc the offset
        double OS2, OF;

        OS2 = m_OffsetSteps / 2.0;
        OF = ofs - OS2;
        Offset = (OF * m_Intensities * 2.0) / m_OffsetSteps;

        //Offset = (double)(m_Intensities * (2.0 * ofs - m_OffsetSteps)) / m_OffsetSteps;
        Offset28 = Offset / Div;        // Remember we're going down to m_OutputBitDepth bits 
        Offset28 += m_SpecIntersection;
        Offset += m_SpecIntersection;
    }
    else
    {
        Gain = m_SpecGradient;
        Offset = m_SpecIntersection;
        Gain28 = Gain;
        Offset28 = Offset;
    }

	double scale = (m_OutputIntensities - 1.0) / pow(m_OutputIntensities - 1.0, m_Gamma);

    for (i = 0; i < m_Intensities; i++)
    {
        // Calc the LUT that converts from our native bit depth down
        // to our output bit depth
        g = Gain28 * i + Offset28;

		if (m_Gamma < 0.99 || m_Gamma > 1.01)
			g = scale * pow((double)g, m_Gamma) + 0.49999;

        // Just in case
        if (g >= m_OutputIntensities) 
            g = m_OutputIntensities - 1;
        else
        if (g < 0)
            g = 0;

        m_pLUT[i] = (WORD) g;   
        //TRACE2("LUT = %d,%d\r\n", i, (WORD)g);

        // Calc  the  native  LUT  as  well - this  one  will  get  applied to the image
        // before  any   measurements   take  place  and  transforms  applying  the same
        // slope and offsets but doing a native bit depth to native bit depth conversion
        if (m_pNativeLUT)
        {
            g = Gain * i + Offset;
            if (g >= m_Intensities)
                g = m_Intensities - 1;
            else
            if (g < 0)
                g = 0;
            m_pNativeLUT->m_pLUT[i] = (WORD) g;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// Calc the actual gain and offset in steps based on a slope in the native bit range 
// offset in native camera resolution range. Then calc the LUT to match
///////////////////////////////////////////////////////////////////////////////////////

BOOL CCamLUT::DCalc(double G, double O)
{
    double BitShift;
    double Div;
    BOOL InRange = TRUE;

	// Amount to shift result to get the output bit depth
	BitShift = (double) (m_BitDepth - m_OutputBitDepth);
	Div = pow(2.0, BitShift);

	// Calc the actual gain on a scale 0 - m_GainSteps 
	m_Gain = (m_GainSteps * (G - 1.0)) / m_MaxGainSlope;

	// Note if G is large i.e. autosetup or whatever calls this did not get
	// a reasonbale range in the image then m_Gain may be more m_GainSteps
	// so limit it
	if (m_Gain > m_GainSteps)
	{
		InRange = FALSE;
		m_Gain = m_GainSteps;
		G = 1 + m_MaxGainSlope;
	}

	if (m_Gain < 1)
	{
		InRange = FALSE;
		m_Gain = 1;
		G = 1 + (m_MaxGainSlope/m_GainSteps);
	}

	// Calc the actual offset
	double O1, O2, O3;
	O3 = G * (double) O;

	O1 = (m_OffsetSteps / 2.0);
	O2 = (m_OffsetSteps * O3) / (m_Intensities * 2.0);
	m_Offset = O1 - O2;
	//TRACE2("LUTGAIN = %6.3lf LUTOFSET = %6.3lf \r\n", m_Gain, m_Offset);

	if (m_Offset < 0.0)
	{
		//fix the value of the offset and re-calculate gain using this offset value
		m_Offset = 1.0;
		if (O < 0.001 && O > -0.001)
			O = 1.0;
		G = (((m_OffsetSteps / 2.0) - m_Offset) * (m_Intensities * 2.0))/ (m_OffsetSteps * O);
		m_Gain = (m_GainSteps * (G - 1.0)) / m_MaxGainSlope;
	}

	// Reset any spectrachrome correction
	Calc((int) m_Gain, (int) m_Offset, 1.0, 0.0);

	return InRange;
}

///////////////////////////////////////////////////////////////////////////////////
// Create the LUT from the gain and offset values + white balancing values
// gn = gain, ofs = offset
// Grad, Inter = slope and intersection  of the white balancing correction
///////////////////////////////////////////////////////////////////////////////////

void CCamLUT::Calc(int gn, int ofs, double Grad, double Inter)
{
    m_SpecGradient = Grad;               // White balancing
    m_SpecIntersection = Inter;
    Calc(gn, ofs);                              // Recalc LUT with new white balance measures
}

	
// Apply gamma
void CCamLUT::ApplyGamma(double Gamma)
{
	m_Gamma = Gamma;

	// Reset any spectrachrome correction
	Calc((int) m_Gain, (int) m_Offset, 1.0, 0.0);
}


