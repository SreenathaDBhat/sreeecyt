/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Blending of multiexposure images
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// NOTES
//
//

#include "stdafx.h"
#include "ComponentBlend.h"
#include <math.h>

//#define SELECT_EXPOSURE_BLENDING_IMAGES
//#define USE_SIMPLE_EXPOSURE_BLENDING
#define USE_RANGE_EXPOSUREBLENDING

CComponentBlend::CComponentBlend(void)
{
}

CComponentBlend::~CComponentBlend(void)
{
}

static int IntegerQSortFunc(const void *pEle1, const void *pEle2)
{
// Sorts in ascending order.
	return (*((int*)pEle1) - *((int*)pEle2));
}


BOOL CComponentBlend::SimpleBlend(int numImages, BYTE **ppImages, BOOL *pUseImages, int exposures[], int width, int height, BYTE *pOutputImage)
{ 
// Simply averaging corresponding values from each exposure image has the
// advantage of being fast and not introducing a 'patchwork' effect when used
// on a series of images used as tiles.
	int imageSizeBytes = width * height * sizeof(BYTE);


	// Just average the input images, until we come up with a better solution that is fast.
	BYTE *pOutputPixel = pOutputImage;
	for (int i = 0; i < imageSizeBytes; i++)
	{
		/* Simple average...
		int sum = 0;
		for (int imageIndex = 0; imageIndex < numImages; imageIndex++)
			sum += *(ppImages[imageIndex] + i);
		*pOutputPixel = sum / numImages;
		*/

		// Weighted average...
		double weightedSum = 0;
		double totalWeight = 0;
		double weightedAverage = 0;
		for (int imageIndex = 0; imageIndex < numImages; imageIndex++)
		{
			// Skip images that have not been flagged to be used.
			if (pUseImages && !pUseImages[imageIndex])
				continue;

			int value = *(ppImages[imageIndex] + i);

			// Weight linearly away from the middle, on the basis that pixels
			// with higher or lower values are increasingly likely to be
			// over- or under- exposed and so should be given less weight.
			// If weight Parameter is 127, this gives a weight of 1 at the edges and 2 in the middle.
			double weightParameter = 127.0;
			double weight = ((value < 128 ? value : (255 - value)) / weightParameter) + 1.0;

			/*
			// Weight with a Guassian function of form f(x) = a * 2 ^ (-((x - b)^2) / c^2)
			// a determines the maximum value of the curve
			// b determines the centre of the curve and should stay fixed at 127.5 for 8bpp.
			// c determines the width of the curve
			// If a is 2 and c is 127.5, this gives a weight of 1 at the edges and 2 in the middle.
			double weight = 2.0 * pow(2.0, -((value - 127.5) * (value - 127.5)) / (127.5*127.5));
			*/

			weightedSum += (value * weight);
			totalWeight += weight;
		}

		if (totalWeight > 0)
			weightedAverage = weightedSum / totalWeight;


		// Add 0.5 before truncating, to get a rounded value.
		*pOutputPixel = int(weightedAverage + 0.5);

		pOutputPixel++;
	}

    return TRUE;
}


BOOL CComponentBlend::RangeBlend(int numImages, BYTE **ppImages, BOOL *pUseImages, int exposures[], int width, int height, BYTE *pOutputImage)
{
    // Calculate the average of pixels from the input images, using the local
    // dynamic ranges as weights, in order to maximise local contrast.
	int imageSizeBytes = width * height * sizeof(BYTE);



    // TODO: this algorithm sufferes from an image border artefact, the same width as the filter size, which needs fixing. It is also slow!
	// Note that width of the filters is (filterSize * 2) + 1
	int rangeFilterSize = 20; 
	int smoothFilterSize = 20;
	int smoothIterations = 1;

	BOOL memoryAllocationFailure = FALSE;

	BYTE **ppRangeImages = (BYTE**)malloc(numImages * sizeof(BYTE*));
	if (ppRangeImages)
	{
		for (int i = 0; i < numImages; i++)
		{
			ppRangeImages[i] = (BYTE*)malloc(imageSizeBytes);
			if (!ppRangeImages[i])
				memoryAllocationFailure = TRUE;
		}
	}

	BYTE *pTempRangeImage = (BYTE*)malloc(imageSizeBytes);

	if (memoryAllocationFailure || !pTempRangeImage)
		return FALSE;


	for (int imageIndex = 0; imageIndex < numImages; imageIndex++)
	{
		int x, y;

		const BYTE *pImage = ppImages[imageIndex];
		BYTE *pRangeImage = ppRangeImages[imageIndex];

		// Find the minimum and maximum values of local pixels in order to determine the local contrast levels.
		// Note that variations to this algorithm could find the variance or the histogram of the local values.
		BYTE *pCurrTempRangePixel = pTempRangeImage;
		for (y = 0; y < height; y++)
		{
			// The filter is clipped to the edges of the image.
			int yFilterBegin = y - rangeFilterSize;
			if (yFilterBegin < 0) yFilterBegin = 0;

			int yFilterBeginOffset = yFilterBegin * width;

			int yFilterEnd = y + rangeFilterSize;
			if (yFilterEnd >= height) yFilterEnd = height - 1;

			for (x = 0; x < width; x++)
			{
				int xFilterBegin = x - rangeFilterSize;
				if (xFilterBegin < 0) xFilterBegin = 0;

				int xFilterEnd = x + rangeFilterSize;
				if (xFilterEnd >= width) xFilterEnd = width - 1;

				int yFilterIncrementOffset = width - (xFilterEnd - xFilterBegin + 1);

				const BYTE *pCurrFilterPixel = pImage + yFilterBeginOffset + xFilterBegin;

				// Initialise the minimum and maximum with the value of the first filter pixel, as when
				// only the first pixel has been checked, that will be both the current minimum and maximum values,
				// but within the loop a value is only allowed to become the current maximum if it was not
				// the current minimum (for reasons of speed optimisation).
				int minimum = int(*pCurrFilterPixel);
				int maximum = minimum;

				for (int yFilter = yFilterBegin; yFilter <= yFilterEnd; yFilter++)
				{
					for (int xFilter = xFilterBegin; xFilter <= xFilterEnd; xFilter++)
					{
						int value = int(*pCurrFilterPixel);

						if (value < minimum)
							minimum = value;
						else if (value > maximum)
							maximum = value;

						pCurrFilterPixel++;
					}

					pCurrFilterPixel += yFilterIncrementOffset;
				}


				int range = 0;
				if (maximum > minimum)
					range = maximum - minimum;

				// Store range in temporary range image
				*pCurrTempRangePixel = (BYTE)range;

				pCurrTempRangePixel++;
			}
		}


		// Smooth temporary histogram range image (to smooth bright to dark transitions in final image)
		// and put results in final range image.
		for (int i = 0; i < smoothIterations; i++)
		{
			BYTE *pCurrRangePixel = pRangeImage;

			for (y = 0; y < height; y++)
			{
				// The filter is clipped to the edges of the image.
				int yFilterBegin = y - smoothFilterSize;
				if (yFilterBegin < 0) yFilterBegin = 0;

				int yFilterBeginOffset = yFilterBegin * width;

				int yFilterEnd = y + smoothFilterSize;
				if (yFilterEnd >= height) yFilterEnd = height - 1;

				for (x = 0; x < width; x++)
				{
					int sum = 0;
					int count = 0;

					int xFilterBegin = x - smoothFilterSize;
					if (xFilterBegin < 0) xFilterBegin = 0;

					int xFilterEnd = x + smoothFilterSize;
					if (xFilterEnd >= width) xFilterEnd = width - 1;

					int yFilterIncrementOffset = width - (xFilterEnd - xFilterBegin + 1);

					const BYTE *pCurrFilterPixel = pTempRangeImage + yFilterBeginOffset + xFilterBegin;

					for (int yFilter = yFilterBegin; yFilter <= yFilterEnd; yFilter++)
					{
						for (int xFilter = xFilterBegin; xFilter <= xFilterEnd; xFilter++)
						{
							int value = int(*pCurrFilterPixel);

							sum += value;

							count++;

							pCurrFilterPixel++;
						}

						pCurrFilterPixel += yFilterIncrementOffset;
					}

					*pCurrRangePixel = (BYTE)(sum / count);

					pCurrRangePixel++;
				}
			}

			if (i < smoothIterations - 1)
				memcpy(pTempRangeImage, pRangeImage, imageSizeBytes);
		}

		if (smoothIterations < 1)
			memcpy(pRangeImage, pTempRangeImage, imageSizeBytes);
	}


	// Build final image from (histogram range) weighted sum of input images
	BYTE *pOutputPixel = pOutputImage;
	for (int i = 0; i < imageSizeBytes; i++)
	{
		int rangeSum = 0;
		int weightedAverage = 0;

		for (int imageIndex = 0; imageIndex < numImages; imageIndex++)
		{
			int range = *(ppRangeImages[imageIndex] + i);
			range *= range; // Using the square of the range as the weight gives better results.
			if (range > 0)
			{
				rangeSum += range;
				weightedAverage += range * (*(ppImages[imageIndex] + i));
			}
		}

		if (rangeSum > 0)
			weightedAverage /= rangeSum;

		*pOutputPixel = weightedAverage;

		pOutputPixel++;
	}

	//This will output a range image: memcpy(pOutputImage, ppRangeImages[1], imageSizeBytes);

	// Free memory allocated by this function.
	for (int i = 0; i < numImages; i++)
		free(ppRangeImages[i]);

	free(ppRangeImages);

	free(pTempRangeImage);

    return TRUE;
}

BOOL CComponentBlend::HDRBlend(int numImages, BYTE **ppImages, BOOL *pUseImages, int exposures[], int width, int height, BYTE *pOutputImage)
{
    // Create a high dynamic range image from the input images, then compress it
    // down to a low dynamic range image whilst preserving the magnitude of small
    // intensity changes relative to the surrounding pixels (such small changes
    // are lost if the images are simply averaged).
	int imageSizeBytes = width * height * sizeof(BYTE);



	// Create HDR image by summing images at different exposures.
	int *pHDRImage = (int*)malloc(width * height * sizeof(int)); // HDR: High Dynamic Range
	if (pHDRImage)
	{
		int imageCount = numImages;
		if (pUseImages)
		{
			for (int imageIndex = 0; imageIndex < numImages; imageIndex++)
			{
				if (!pUseImages[imageIndex])
					imageCount--;
			}
		}

		int x, y;
		int minHDR = INT_MAX;
		int maxHDR = 0;

		int *pHDRPixel = pHDRImage;
		for (int i = 0; i < imageSizeBytes; i++)
		{
			double weightedSum = 0;
			double totalWeight = 0;
			for (int imageIndex = 0; imageIndex < numImages; imageIndex++)
			{
				// Skip images that have not been flagged to be used.
				if (pUseImages && !pUseImages[imageIndex])
					continue;

				int value = *(ppImages[imageIndex] + i);

				// Weight linearly away from the middle, on the basis that pixels
				// with higher or lower values are increasingly likely to be
				// over- or under- exposed and so should be given less weight.
				// If weight Parameter is 127, this gives a weight of 1 at the edges and 2 in the middle.
				double weightParameter = 127.0;
				double weight = ((value < 128 ? value : (255 - value)) / weightParameter) + 1.0;

				/*
				// Weight with a Guassian function of form f(x) = a * 2 ^ (-((x - b)^2) / c^2)
				// a determines the maximum value of the curve
				// b determines the centre of the curve and should stay fixed at 127.5 for 8bpp.
				// c determines the width of the curve
				// If a is 2 and c is 127.5, this gives a weight of 1 at the edges and 2 in the middle.
				double weight = 2.0 * pow(2.0, -((value - 127.5) * (value - 127.5)) / (127.5*127.5));
				*/

				weightedSum += (value * weight);
				totalWeight += weight;
			}


			int valueHDR = 0;
			if (totalWeight > 0) // Add 0.5 before truncating, to get a rounded value.
				valueHDR = int(((weightedSum / totalWeight) * imageCount) + 0.5);


			if (valueHDR < minHDR)
				minHDR = valueHDR;
			if (valueHDR > maxHDR)
				maxHDR = valueHDR;

			
			*pHDRPixel = valueHDR;

			pHDRPixel++;
		}


		// Compress the dynamic range by scaling it to fit the range 0 to 255,
		// but add the log of the deviation of each pixel from the local average,
		// to preserve the magnetude of small local changes in intensity.
		const int filterSize = 1;
		int filterValues[(filterSize*2+1)*(filterSize*2+1)]; // This array needs to be of size (filterSize*2+1)^2

		for (y = 0; y < height; y++)
		{
			for (x = 0; x < width; x++)
			{
				// Find the average of the values within the filter.
				// The median is more accurate than the mean as a basis for
				// measurings the deviations, as it doesn't over-react to
				// abrupt changes in a minority of pixels. Using the mean gives
				// a blurrier result with more artefacts.
				// A smaller filter should be more sensitive to small changes
				// (and faster).
				int filterCount = 0;
				//int sum = 0;

				// Note that the filter is clipped to the edges of the image.
				for (int yOffset = -filterSize; yOffset <= filterSize; yOffset++)
				{
					int yFilter = y + yOffset;
					if (yFilter >= 0 && yFilter < height)
					{
						for (int xOffset = -filterSize; xOffset <= filterSize; xOffset++)
						{
							int xFilter = x + xOffset;
							if (xFilter >= 0 && xFilter < width)
							{
								int value = pHDRImage[xFilter + (yFilter * width)];

								filterValues[filterCount] = value;

								filterCount++;

								//sum += value;
							}
						}
					}
				}

				// Sort in ascending order and find the median.
				if (filterCount > 1)
					qsort(filterValues, filterCount, sizeof(filterValues[0]), IntegerQSortFunc);

				int median = filterValues[filterCount / 2];

				//int mean = sum / filterCount;

				int average = median;


				int index = x + (y*width);

				int valueHDR = pHDRImage[index];


				// Find the log of the deviation from the average, so that
				// small changes are given more weight than large ones.
				// Add one before finding the log, so that it passes through
				// the origin. Multiplying the result by two means that a
				// deviation of 1 or 2 in the HDR image is preserved, then
				// larger deviations are given increasingly small weight.
				// Multiplying by other values scales this effect accordingly.
				double deviation = log(double(abs(valueHDR - average) + 1) * 2.0);

				// Restore the sign of the deviation. 
				if (valueHDR < average)
					deviation = -deviation;


				// The final value is equivalent to the average value of the
				// pixel in all the input images, with the addition of the
				// deviation which restores the magnitude of small changes
				// to something like their size in the HDR image.
				// Using 'average' as the basis value is maybe more correct
				// (and is best used with a larger scale factor for the
				// deviation e.g. 4.0), but seems to give slightly worse results
				// than 'valueHDR' (which is best used with smaller scale
				// factors for the deviation e.g. 2.0, perhaps because part of
				// deviation signal is present in it already).
				// 0.5 is added before truncating, to get a rounded value.
				int compressedValue = int((double(/*average*/valueHDR - minHDR) / imageCount) + deviation + 0.5);


				// After adding the deviation, the value may exceed the allowed
				// range for an 8bpp image, in which case clip it. A more
				// sophisticated (and slower) approach would scale the HDR image
				// so that after the deviations were added the ranges were not
				// exceeded.
				if (compressedValue < 0)
					compressedValue = 0;

				if (compressedValue > 255)
					compressedValue = 255;


				pOutputImage[index] = (BYTE)compressedValue;
			}
		}

		free(pHDRImage);
	}

    return TRUE;
}

BOOL CComponentBlend::BlendMultipleExposures(BlendTechnique BT, int numImages, BYTE **ppImages, int exposures[], int width, int height, BYTE *pOutputImage)
{
// ppImages is an array of pointers to numImages images, each of dimensions (width, height). Images must be 8bpp, unpadded.
    long Hist[256];
    long B;
    int D, Max, i, j, Temp;
    BYTE *Addr;

	if (numImages < 2 || !ppImages || width < 2 || height < 2 || !pOutputImage)
		return FALSE;

	for (int imageIndex = 0; imageIndex < numImages; imageIndex++)
	{
		if (!ppImages[imageIndex])
			return FALSE;
	}

#if 0
    int *pModes;
    B = width * height;

    // Find the modal values for each image
    pModes = new int[numImages];

    for (i = 0; i < numImages; i++)
    {
        memset(Hist, 0, sizeof(long) * 256);

        Addr = ppImages[i];
        for (j = 0; j < B; j++)
            Hist[*Addr++]++;
        Max = 0;
        for (j = 0; j < 256; j++)
            if (Hist[j] > Max)
            {
                Max = Hist[j];
                pModes[i] = j;
            }
    }

    // Bubble sort the modes - could be done quicker but there will only be a few to sort
    for (i = 0; i < numImages; i++)
    {
        for (j = 0; j < numImages; j++)
        {
            if (i != j)
            {
                if (pModes[i] < pModes[j])
                {
                    Temp = pModes[i];
                    pModes[i] = pModes[j];
                    pModes[j] = Temp;
                    // Also arrange the images so that the image with the lowest mode is first
                    Addr = ppImages[i];
                    ppImages[i] = ppImages[j];
                    ppImages[j] = Addr;
                }
            }
        }
    }

    // Now subtract the difference in modes from each of the images but don't do the first as its the lowest mode
    for (i = 1; i < numImages; i++)
    {
        Addr = ppImages[i];
        D = pModes[i] - pModes[0];
        for (j = 0; j < B; j++)
        {
            *Addr++ = *Addr > D ? *Addr - D : 0;
        }
    }
#endif

	int imageSizeBytes = width * height * sizeof(BYTE);


	BOOL *pUseImages = NULL;

// Choose images to use, based on various criteria.
// Note that this will introduce a 'patchwork' effect if used on a series
// of images that are displayed as tiles, if the exposures chosen are
// different for different tiles.
	const int minImages = 2;
	const int initialGate = 32;

	pUseImages = new BOOL[numImages];
	if (pUseImages)
	{
		for (int i = 0; i < numImages; i++)
			pUseImages[i] = TRUE;

		// Exclude images whose mean value is above a maximum value (to remove
		// very overexposed images from the blend), subject to using a
		// minimum number of images.
		int *pImageMeans = new int[numImages];
		if (pImageMeans)
		{
			// Calulate the mean pixel value for each image.
			for (int imageIndex = 0; imageIndex < numImages; imageIndex++)
			{
				const BYTE *pImage = ppImages[imageIndex];

				double sum = 0.0;

				for (int i = 0; i < imageSizeBytes; i++)
				{
					sum += double(*pImage);

					pImage++;
				}

				int mean = imageSizeBytes > 0 ? int(sum / imageSizeBytes) : 0;

				pImageMeans[imageIndex] = mean;
			}

			// If the gate excludes too many images, keep raising it until
			// enough images are included (or until it is fully raised).
			int gate = initialGate;
			if (numImages < minImages)
			{
				while (gate > 0)
				{
					int count = 0;
					for (int i = 0; i < numImages; i++)
					{
						if (pImageMeans[i] <= (255 - gate))
							count++;
					}

					if (count < minImages)
						gate /= 2;
					else
						break;
				}
			}

			// Don't use images that can't get through the gate.
			int i;
			for (i = 0; i < numImages; i++)
			{
				if (pImageMeans[i] > (255 - gate))
					pUseImages[i] = FALSE;
			}

			delete[] pImageMeans;
		}
	}


    switch (BT)
    {
    case Range:
        RangeBlend(numImages, ppImages, pUseImages, exposures, width, height, pOutputImage);
        break;

    case Simple:
        SimpleBlend(numImages, ppImages, pUseImages, exposures, width, height, pOutputImage);
        break;

    case HDR:
        HDRBlend(numImages, ppImages, pUseImages, exposures, width, height, pOutputImage);
        break;
    }

	if (pUseImages)
		delete[] pUseImages;

    //delete pModes;

	return TRUE;
}


