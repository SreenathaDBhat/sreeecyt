#include "stdafx.h"
#include <math.h>
#include "Entropy.h"


int EntropyThreshold8(BYTE *pSrcImage,  WORD W, WORD H)
{
	int		i, y, n, j;
	int		Thresh = -1;
	BYTE	*ImPtr;
	DWORD	Hist[256];
	double	prob[256];
	double	Hn, Ps, Hs;
	double	psi, psiMax;

	// Get the histogram
	for (i = 0; i < 256; i++)
		Hist[i] = 0;

	n = W * H;
	ImPtr = pSrcImage;
	for (y = 0; y < n; y++) 
	{
		Hist[*ImPtr++]++;
	}

	// compute probabilities 
	for (i = 0; i < 256; i++)
		prob[i] = (double) Hist[i] / (double) n;

	// find threshold 
	Hn = 0.0;
	for (i = 0; i < 256; i++)
	{
		if (prob[i] != 0.0)
			Hn -= prob[i] * log (prob[i]);
	}


	// Find threshold at which max entropy obtained
	psiMax = 0.0;
	psi = 0.0;
	for (i = 1; i < 256; i++) 
	{
		for (j = 0, Ps = Hs = 0.0; j < i; j++) 
		{
			Ps += prob[j];
			if (prob[j] > 0.0)
				Hs -= prob[j] * log (prob[j]);
		}

		if (Ps > 0.0 && Ps < 1.0)
			psi = log (Ps - Ps * Ps) + Hs / Ps + (Hn - Hs) / (1.0 - Ps);
		if (psi > psiMax) 
		{
			psiMax = psi;
			Thresh = i;
		}
	}

	return Thresh;
}



int EntropyThreshold16(WORD *pSrcImage, WORD Bins, WORD W, WORD H)
{
	int		i, y, n, j;
	int		Thresh = -1;
	WORD	*ImPtr;
	DWORD	Hist[65535];
	double	prob[65535];
	double	Hn, Ps, Hs;
	double	psi, psiMax;

	// Get the histogram
	memset(Hist, 0, sizeof(Hist));

	n = W * H;
	ImPtr = pSrcImage;
	for (y = 0; y < W * H; y++) 
		Hist[*ImPtr++]++;

	// compute probabilities 
	for (i = 0; i < Bins; i++)
		prob[i] = (double) Hist[i] / (double) n;

	// find threshold 
	Hn = 0.0;
	for (i = 0; i < Bins; i++)
	{
		if (prob[i] != 0.0)
			Hn -= prob[i] * log (prob[i]);
	}

	// Find threshold at which max entropy obtained
	psiMax = 0.0;
	psi = 0.0;
	for (i = 1; i < Bins; i++) 
	{
		for (j = 0, Ps = Hs = 0.0; j < i; j++) 
		{
			Ps += prob[j];
			if (prob[j] > 0.0)
				Hs -= prob[j] * log (prob[j]);
		}

		if (Ps > 0.0 && Ps < 1.0)
			psi = log (Ps - Ps * Ps) + Hs / Ps + (Hn - Hs) / (1.0 - Ps);
		if (psi > psiMax) 
		{
			psiMax = psi;
			Thresh = i;
		}
	}

	return Thresh;
}