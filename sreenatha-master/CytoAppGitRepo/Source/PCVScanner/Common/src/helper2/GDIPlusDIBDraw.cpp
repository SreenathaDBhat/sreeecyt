#include "stdafx.h"
#include <gdiplus.h>

//=================================================================================
// Dib.cpp
// Base class for dib drawing
//
// History:
//
// 28Feb00	WH:	original
// 03May01	dcb	Fix bitmap height calculation in CGDIPlusDIBDraw::Draw
//				Add CGDIPlusDIBDraw::DrawSubRect
// 091103   KR: GDI+ version & turned it into a DLL
//=================================================================================
#include "stdafx.h"

#pragma comment(lib, "gdiplus.lib")

#define ULONG_PTR DWORD 

#include <gdiplus.h>
using namespace Gdiplus;

#include "GDIPlusDibDraw.h"
#include <stdio.h>
#include <assert.h>

//=================================================================================
//=================================================================================
CGDIPlusDIBDraw::CGDIPlusDIBDraw()
{
	m_fnOverlay = NULL;		// overlay function

    int i;

	// DIB header
	memset(&m_DibInfo, 0, sizeof(m_DibInfo));
	m_DibInfo.bmiHeader.biPlanes = 1; 
	m_DibInfo.bmiHeader.biBitCount = 8; 
	m_DibInfo.bmiHeader.biCompression = BI_RGB; 
	m_DibInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);

	// Use palette indices which will get converted to RGB if 24 bit
	for (i = 0; i < 256; i++)
		m_DibInfo.bmiColors[i] = i;

    // Allocate a drawing palette
    pDisplayPal = malloc(sizeof(ColorPalette) + 256 * sizeof(ARGB));
    ((ColorPalette *)pDisplayPal)->Count = 256;
    ((ColorPalette *)pDisplayPal)->Flags = 0;

    for (i = 0; i < 256; i++)
    {
        Color PalEntry(0xFF, i, i, i);
        ((ColorPalette *)pDisplayPal)->Entries[i] = PalEntry.GetValue();
    }
        
    // Low end saturation
    Color PalEntryL(0xFF, 0, 0, 0xFF);
    ((ColorPalette *)pDisplayPal)->Entries[0] = PalEntryL.GetValue();
    // High end saturation
    Color PalEntryH(0xFF, 0xFF, 0, 0);
    ((ColorPalette *)pDisplayPal)->Entries[255] = PalEntryH.GetValue();

    m_bInitOK = FALSE;
}


//=================================================================================
//=================================================================================
CGDIPlusDIBDraw::~CGDIPlusDIBDraw()
{
    if (pDisplayPal)
        delete pDisplayPal;
}

BOOL CGDIPlusDIBDraw::Init(void)
{
    Gdiplus::GdiplusStartupInput gdiplusStartupInput;
    if (Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL) == Gdiplus::Ok)
        m_bInitOK = TRUE;
    else
        m_bInitOK = FALSE;

    return m_bInitOK;
}

void CGDIPlusDIBDraw::ShutDown(void)
{
    // Shut down GDI+
    if (m_bInitOK)
        Gdiplus::GdiplusShutdown(gdiplusToken);
}

//=================================================================================
// Draw dib to window
//	hWnd		- window to draw
//  image		- image data
//  pDst		- Destination RECT on window
//  pSrc		- Data ROI within image
//  pClient		- Size of client area
//=================================================================================
void CGDIPlusDIBDraw::Draw(HWND hWnd, BYTE *image, RECT* pDst, RECT *pSrc, RECT *pClient, UINT bpp)
{
    if (m_bInitOK)
    {
        m_DibInfo.bmiHeader.biWidth = pSrc->right - pSrc->left;
        m_DibInfo.bmiHeader.biHeight = -(int)(pSrc->bottom - pSrc->top);
        m_DibInfo.bmiHeader.biBitCount = bpp; 

        // Create our bitmap object
        Bitmap BMP((BITMAPINFO *)&m_DibInfo, image);

        Rect Dest(pDst->left, pDst->top, pDst->right - pDst->left, pDst->bottom - pDst->top);  
        Rect Src(pSrc->left, pSrc->top, pSrc->right - pSrc->left, pSrc->bottom - pSrc->top);  
        
        // Give our bitmap a palette to work with
        BMP.SetPalette((ColorPalette *)pDisplayPal); 

        HDC GDC = GetDC(hWnd);

        // Create our graphics object from the specified hwnd
        Graphics *pGraphWnd = Graphics::FromHDC(GDC);

#if 0
        if (!m_fnOverlay)
        {
            pGraphWnd->DrawImage(&BMP, 
                                Dest, 
                                pSrc->left, pSrc->top, pSrc->right - pSrc->left, pSrc->bottom - pSrc->top,
                                UnitPixel);

        }
        else
#endif
		// Always double buffer, not just when using overlays
        {
            // Equivalent of a MemDC in the old days
            Bitmap MemBMP(pClient->right - pClient->left, pClient->bottom- pClient->top);

            Graphics GraphMem(&MemBMP);

			// Erase all client background
			SolidBrush bkgrd(Color(255,255,255));
			GraphMem.FillRectangle(&bkgrd, 0, 0, pClient->right - pClient->left, pClient->bottom- pClient->top);

            // Draw the image into our dest bmp scaling to correct size
            GraphMem.DrawImage(&BMP, 
                               Dest, 
                               pSrc->left, pSrc->top, pSrc->right - pSrc->left, pSrc->bottom - pSrc->top,
                               UnitPixel);

            // Apply custom overlay
			if (m_fnOverlay)
				m_fnOverlay((void *)&GraphMem, 
                        pDst->left, 
                        pDst->top, 
                        pDst->right - pDst->left, 
                        pDst->bottom - pDst->top, 
                        pClient);

            // Draw the unscaled result into the window
            pGraphWnd->DrawImage(&MemBMP, 0, 0);
        }

        delete pGraphWnd;

        ReleaseDC(hWnd, GDC);
    }
}

//=================================================================================
//	DrawSubRect - Like draw except bitmap size is specified
//	This allows for drawing a sub rectangle within an image and does
//	not fix the bitmap width as the source rectangle width
//
//	hWnd		- window to draw
//  image		- image data
//  pDst		- Destination RECT on window
//  pSrc		- Data ROI within image
//  pBmp		- Size of bitmap
//
// Note: Overlay code is not yet tested:
//=================================================================================
void CGDIPlusDIBDraw::DrawSubRect(HWND hWnd, BYTE *image, RECT* pDst, RECT *pSrc, RECT *pBmp, UINT bpp)
{
	HDC memdc;
	HBITMAP hbm;
	HGDIOBJ oldobj;
	HDC hDstDC = GetDC(hWnd);

	m_DibInfo.bmiHeader.biWidth = pBmp->right - pBmp->left;
	m_DibInfo.bmiHeader.biHeight = -(int)(pBmp->bottom - pBmp->top);
	m_DibInfo.bmiHeader.biBitCount = bpp; 


	if (!m_fnOverlay)	// Just do a stretchDibBlt if no overlay
	{

		SetStretchBltMode(hDstDC, STRETCH_DELETESCANS);

		if (StretchDIBits(hDstDC, 
							pDst->left, pDst->top, pDst->right - pDst->left, pDst->bottom - pDst->top, 
							pSrc->left, pSrc->top, pSrc->right - pSrc->left, pSrc->bottom - pSrc->top, 
							image, (BITMAPINFO *)&m_DibInfo, DIB_PAL_COLORS, SRCCOPY) == GDI_ERROR)
		{
			TRACE("CGDIPlusDIBDraw::DrawSubRect: StretchDIBits error\n\r");
		}
	}
	else	// Otherwise do it is a memory device context so it doesn't flash
	{
			// Create handle to memory compatible DC
		if ( (memdc = CreateCompatibleDC(hDstDC)) == NULL )
		{
			ReleaseDC(hWnd, hDstDC);
			return;
		}

		SetBkMode(memdc, TRANSPARENT);
		// Create bitmap
		if ( (hbm = CreateCompatibleBitmap(hDstDC, pBmp->right - pBmp->left, pBmp->bottom- pBmp->top)) == NULL )
		{
			ReleaseDC(hWnd, hDstDC);
			DeleteDC( memdc);
			TRACE("CGDIPlusDIBDraw::DrawSubRect: CreateCompatibleBitmap error\n\r");
			return;
		}

		// Attach bitmap to memory compatible DC
		oldobj = (HGDIOBJ)SelectObject( memdc, hbm);



		SetStretchBltMode(memdc,STRETCH_DELETESCANS);


		if (StretchDIBits(memdc, 
							pDst->left, pDst->top, pDst->right - pDst->left, pDst->bottom - pDst->top, 
							pSrc->left, pSrc->top, pSrc->right - pSrc->left, pSrc->bottom - pSrc->top, 
							image, (BITMAPINFO *)&m_DibInfo, DIB_PAL_COLORS, SRCCOPY) == GDI_ERROR)
		{
			TRACE("CGDIPlusDIBDraw::DrawSubRect: StretchDIBits error\n\r");
		}

		// Apply custom overlay
		if (m_fnOverlay)
		{
			m_fnOverlay(memdc, pDst->left, pDst->top, pDst->right - pDst->left, pDst->bottom - pDst->top, pBmp);
		}

		// Blat everything to the window
//		BitBlt(hDstDC, pClient->left, pClient->top, pClient->right - pClient->left, pClient->bottom- pClient->top, memdc, 0, 0, SRCCOPY);
		BitBlt(hDstDC, pDst->left, pDst->top, pDst->right - pDst->left, pDst->bottom- pDst->top, memdc, 0, 0, SRCCOPY);

		// Tidy up
		SelectObject(memdc, oldobj);
		DeleteObject(hbm);
		DeleteDC(memdc);
	}

	ReleaseDC(hWnd, hDstDC);
}

//=================================================================================
//=================================================================================
void CGDIPlusDIBDraw::OutputLUTEntry(UINT index, UINT red, UINT green, UINT blue) 
{
	assert(pDisplayPal);
    Color PalEntryH(0xFF, red, green, blue);

    ((ColorPalette *)pDisplayPal)->Entries[index] = PalEntryH.GetValue();
}

//=================================================================================
//=================================================================================
