////////////////////////////////////////////////////////////////////////////
// HRTimer.cpp : Defines the class behaviors for the application.
// A small high resolution timer - microsec accuracy possible
// Written By Karl Ratcliff
////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <HRTimer.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// BN This works well on Athlon XPs but for some reason doesn't seem to go on P4 systems

#define LI_TO_DOUBLE(l, h)      (double) l + (double) h * (double) 0xFFFFFFFFFF

// Scrap the LI_TO_DOUBLE stuff use a 64 bit int directly - works great on Intel platforms


#ifndef CAMMANAGED

CHRTimer::CHRTimer(void)
{
    LARGE_INTEGER   pf;

    m_bUsePerformanceCount = QueryPerformanceFrequency(&pf);

    if (m_bUsePerformanceCount)
        m_HRFrequency = (double) pf.QuadPart;
    else
        m_HRFrequency = (double) CLOCKS_PER_SEC;

    m_Divisor = 1.0;
}

CHRTimer::CHRTimer(double Factor)
{
    LARGE_INTEGER   pf;

    m_bUsePerformanceCount = QueryPerformanceFrequency(&pf);

    if (m_bUsePerformanceCount)
        m_HRFrequency = (double) pf.QuadPart;
    else
        m_HRFrequency = (double) CLOCKS_PER_SEC;

    m_Divisor = Factor;
}

CHRTimer::~CHRTimer(void)
{
}

////////////////////////////////////////////////////////////////////////////
// Call this member function to start timing. Returns FALSE if it
// cannot use a performance counter
////////////////////////////////////////////////////////////////////////////

BOOL CHRTimer::Start(void)
{
    BOOL Ok;

    if (m_bUsePerformanceCount)
    {
        LARGE_INTEGER   pf;
        Ok = QueryPerformanceCounter(&pf);
        m_StartTime = (double) pf.QuadPart;
    }
    else
    {
        m_StartTime = (double) clock();
        Ok = FALSE;
    }

    return Ok;
}

////////////////////////////////////////////////////////////////////////////
// Call this member function to get the time after Start() has been
// called. Returns FALSE if it did not use a performance counter
////////////////////////////////////////////////////////////////////////////

BOOL CHRTimer::ElapsedTime(double *e)
{
    BOOL Ok;

    if (m_bUsePerformanceCount)
    {
        LARGE_INTEGER   pf;
        Ok = QueryPerformanceCounter(&pf);
        
        m_ElapsedTime = (double) pf.QuadPart;
    }
    else
    {
        m_ElapsedTime = (double)clock();
        Ok = FALSE;
    }

    m_ElapsedTime -= m_StartTime;
    m_ElapsedTime /= m_HRFrequency;
    *e = m_ElapsedTime / m_Divisor;                 // Result in seconds

    return Ok;
}

#endif