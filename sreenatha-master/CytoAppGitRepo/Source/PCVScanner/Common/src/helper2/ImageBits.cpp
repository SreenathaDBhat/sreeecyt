
// ImageBits.cpp
//
// Functions for converting the bit depth of images, etc.
//
// JMB October 2011

#include "stdafx.h"
#include "ImageBits.h"



bool ImageBits::ShiftToNBPP(const WORD *pSrc, int width, int height, int srcBitsPerPixel, BYTE *pOut, int outputBitsPerPixel)
{
// This function assumes that the input image uses two bytes to store each pixel
// (though not necessarily all the bits in those bytes), i.e. 11 to 16 bpp.
// The output image will use however many bytes per pixel are required, subject to a maximum value
// for outputBitsPerPixel of 16.
// It just bitshifts the pixel data to fit the output bit depth, such that the
// most significant bit in the input ends up in the most significant bit in the output.
// If the input image uses N bpp where N is less than 16, it is assumed that
// it is the N least significant bits in each two-byte word that are used.
// Similarly, the least significant 'outputBitsPerPixel' bits are used in the
// output image pixels bytes.
	bool status = false;

	if (pSrc && width > 0 && height > 0 && srcBitsPerPixel > 0 && pOut && outputBitsPerPixel)
	{
		int outputBytesPerPixel = GetBytesPerPixel(outputBitsPerPixel);
		int numberOfPixels = width * height;

		int shift = srcBitsPerPixel - outputBitsPerPixel;

		for (int i = 0; i < numberOfPixels; i++)
		{
			// Shift to the output bit depth (maximum for outputBitsPerPixel is 16, else this may will overflow).
			// Note that this ensures that the most significant bits of the input image end up in the most
			// significant bits of the output image, e.g. if the input has 12bpp and the output has 16bpp, they
			// both use 2 bytes per pixel but in the input image the 4 most significant bits are always zero
			// whereas in the output image the 4 least significant bits will be zero.
			unsigned long r;
			if (shift >= 0)
				r = *pSrc++ >> shift;
			else
				r = ((unsigned long)*pSrc++) << -shift;

			// Copy the used bytes from r to the output, in little-endian order.
			for (int remainingBytes = outputBytesPerPixel; remainingBytes > 0; remainingBytes--)
			{
				*pOut++ = (BYTE)(r & 0xFF);
				r >>= 8;
			}
		}

		status = true;
	}

	return status;
}


bool ImageBits::ScaleToNBPP(const WORD *pSrc, int width, int height, int srcBitsPerPixel, BYTE *pOut, int outputBitsPerPixel)
{
// Note that this function does NOT do a histogram stretch or contrast stretch.
// Instead it scales proportionally, based on the potential range of values at each bit depth.
// This means that the intensities in one image will be comparable with those in another image
// in the same scan (with the same camera settings).
// The same proportion of the output bit depth will be used as is used in the input bit depth.
// This ensures a similar range of intensities in the output image regardless of the original
// bit depth, which would not be the case if the image's pixels were histogram stretched or
// just bit-shifted to the top of the output range.
// However, it will cause gaps to appear in the resulting image's histogram. This could be
// mitigated by spacially interpolating pixel values in the final image.
//
// This function assumes that the input image uses two bytes to store each pixel
// (though not necessarily all the bits in those bytes) and that the
// maximum value of outputBitsPerPixel is 16.
	bool status = false;

	if (pSrc && width > 0 && height > 0 && srcBitsPerPixel > 0 && pOut && outputBitsPerPixel)
	{
		int outputBytesPerPixel = GetBytesPerPixel(outputBitsPerPixel);
		int numberOfPixels = width * height;

		unsigned long maxPossibleInputPixelValue  = (1 << srcBitsPerPixel) - 1;
		unsigned long maxPossibleOutputPixelValue = (1 << outputBitsPerPixel) - 1;

		if (maxPossibleInputPixelValue > 0 && outputBytesPerPixel > 0 && outputBytesPerPixel <= 2)
		{
			for (int i = 0; i < numberOfPixels; i++)
			{
				// Mask with maxPossibleInputPixelValue in case there are any stray bits above
				// the maximum level, that could make the subsequent calculation overflow.
				unsigned long result = (*pSrc++ & maxPossibleInputPixelValue);

				// Scale the input value to match the output bit depth
				// (maximum outputBytesPerPixel must be 2, else result * maxPossibleOutputPixelValue
				// will overflow).
				unsigned long r = (result * maxPossibleOutputPixelValue) / maxPossibleInputPixelValue;

				// Copy the used bytes from r to the output, in little-endian order.
				for (int remainingBytes = outputBytesPerPixel; remainingBytes > 0; remainingBytes--)
				{
					*pOut++ = (BYTE)(r & 0xFF);
					r >>= 8;
				}
			}

			status = true;
		}
	}

	return status;
}


bool ImageBits::GetHistogramStats(const WORD *pImage, int w, int h, int bpp, WORD &min, WORD &max, WORD &mode)
{
	bool status = false;

	if (pImage && w > 0 && h > 0 && bpp > 0)
	{
		int nGreys = 1 << bpp;

		int *pHisto = new int[nGreys];
		if (pHisto)
		{
			memset(pHisto, 0, nGreys * sizeof(int));

			int numberOfPixels = w * h;
			for (int i = 0; i < numberOfPixels; i++)
			{
				WORD G = *pImage++;
				pHisto[G]++;
			}

			BOOL foundMin = FALSE;
			int maxCount = 0;
			min = 0;
			max = 0;
			mode = 0;
			for (int i = 0; i < nGreys; i++)
			{
				if (!foundMin && pHisto[i] > 0)
				{
					min = i;
					foundMin = TRUE;
				}

				if (pHisto[i] > maxCount)
				{
					maxCount = pHisto[i];
					mode = i;
				}

				if (pHisto[i] > 0)
					max = i;
			}

			delete[] pHisto;

			status = true;
		}
	}

	return status;
}

bool ImageBits::HistogramStretchToNBPP(const WORD *pSrc, int width, int height, int srcBitsPerPixel, BYTE *pOut, int outputBitsPerPixel)
{
// This function assumes that the input image uses two bytes to store each pixel
// (though not necessarily all the bits in those bytes) and that the
// maximum value of outputBitsPerPixel is 16.
	bool status = false;

	if (pSrc && width > 0 && height > 0 && srcBitsPerPixel > 0 && pOut && outputBitsPerPixel)
	{
		WORD min, max, mode, lo, hi;

		if (GetHistogramStats(pSrc, width, height, srcBitsPerPixel, min, max, mode))
		{
			// Rather than just squashing everything down into less bits (when reducing the bit depth),
			// instead only everything above the mode is used, as everything below that is assumed to be
			// mostly background noise.
			lo = mode;
			hi = max;
		}
		else
			lo = hi = 0;

		WORD range = hi - lo;
		if (range > 0)
		{
			int outputBytesPerPixel = GetBytesPerPixel(outputBitsPerPixel);
			int numberOfPixels = width * height;
			unsigned long maxPossibleOutputPixelValue = (1 << outputBitsPerPixel) - 1;

			for (int i = 0; i < numberOfPixels; i++)
			{
				int result = *pSrc++ - lo;
				if (result < 0)
					result = 0;
				else if (result > range)
					result = range;

				// Stretch the input range to fill the full output bit depth
				// (maximum outputBytesPerPixel must be 2, else result * maxOutputPixelValue
				// will overflow).
				unsigned long r = ((unsigned long)result * maxPossibleOutputPixelValue) / range;

				// Copy the used bytes from r to the output, in little-endian order.
				for (int remainingBytes = outputBytesPerPixel; remainingBytes > 0; remainingBytes--)
				{
					*pOut++ = (BYTE)(r & 0xFF);
					r >>= 8;
				}
			}

			status = true;
		}
		else 
			status = ShiftToNBPP(pSrc, width, height, srcBitsPerPixel, pOut, outputBitsPerPixel);
	}

	return status;
}


