#include "StdAfx.h"
#include "LUT.h"
#include <math.h>

///////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
///////////////////////////////////////////////////////////////////////////////////

CLUT::CLUT(WORD bitdepth)
{
    // Calc the number of intensity values
    m_Intensities = (int) pow(2.0, (double) bitdepth);
    // Create the LUT
    m_pLUT = new WORD[m_Intensities];
    // Store bit depths
    m_BitDepth = bitdepth;

	m_MaxValue = m_Intensities - 1;

	m_Gradient = 1.0;
	m_Offset = 0;

    // Make it default to linear LUT
    Linear();
}

CLUT::~CLUT(void)
{
    // Clear up
    delete [] m_pLUT;
}

///////////////////////////////////////////////////////////////////////////////////
// Apply input LUT to an image. Uses a gain and offset to control the slope of the LUT
// and the offset similar to y = mx + c i.e. output = gain * input + offset
///////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////
// Create the LUT from the gain and offset values
// gn = gain, ofs = offset
///////////////////////////////////////////////////////////////////////////////////

void CLUT::Calc(double Gradient, double Offset)
{
    int i;
    double v;
	for (i = 0; i < m_Intensities; i++)
	{
		v = Gradient * i + Offset;
		if (v >= m_Intensities) 
			m_pLUT[i]= m_Intensities - 1;
		else
			m_pLUT[i] = (WORD) v;
	}
}

///////////////////////////////////////////////////////////////////////////////////
// Reset LUT to linear
///////////////////////////////////////////////////////////////////////////////////

void CLUT::Linear(void)
{
    WORD i;

    for (i = 0; i < m_Intensities; i++)
        m_pLUT[i] = i;
}


///////////////////////////////////////////////////////////////////////////////////
// Apply the LUT created above to the image
///////////////////////////////////////////////////////////////////////////////////

void CLUT::Apply(BYTE *pDestImage, BYTE *pSrcImage, UINT W, UINT H)
{
    long i, j;

    j = W * H;
    for(i = 0; i < j; i++)	
       *pDestImage++ = (BYTE) m_pLUT[*pSrcImage++];
}

void CLUT::Apply(BYTE *pDestImage, WORD *pSrcImage, UINT W, UINT H)
{
    long i, j;

    j = W * H;
	for(i = 0; i < j; i++)	
	   *pDestImage++ = (BYTE) m_pLUT[*pSrcImage++];
}

void CLUT::Apply(BYTE *pDestImage, UINT W, UINT H)
{
    long i, j;

    j = W * H;
    for(i = 0; i < j; i++)	
	    *pDestImage = (BYTE) m_pLUT[*pDestImage++];
}

///////////////////////////////////////////////////////////////////////////////////
// Apply the LUT created above to the image
///////////////////////////////////////////////////////////////////////////////////

void CLUT::Apply(WORD *pDestImage, WORD *pSrcImage, UINT W, UINT H)
{
    long i, j;

    j = W * H;
    for(i = 0; i < j; i++)	
	    *pDestImage++ = (WORD) m_pLUT[*pSrcImage++];
}

///////////////////////////////////////////////////////////////////////////////////
// Apply the LUT created above to the image
///////////////////////////////////////////////////////////////////////////////////

void CLUT::Apply(WORD *pDestImage, UINT W, UINT H)
{
    long i, j;

    j = W * H;
    for(i = 0; i < j; i++)	
	    *pDestImage = (WORD) m_pLUT[*pDestImage++];
}

void CLUT::Invert(BYTE *pDestImage, BYTE *pSrcImage, UINT W, UINT H)
{
    long i, j;

   j = W * H;
   for(i = 0; i < j; i++)	
	*pDestImage++ = (BYTE) m_MaxValue - *pSrcImage++;

}
void CLUT::Output(void)
{
    int i;
    CString dbg;

    for (i = 0; i < m_Intensities; i++)
    {
        dbg.Format(_T("%p LUT %d = %d\r\n"), m_pLUT, i, m_pLUT[i]);
        TRACE(dbg);
    }
}
