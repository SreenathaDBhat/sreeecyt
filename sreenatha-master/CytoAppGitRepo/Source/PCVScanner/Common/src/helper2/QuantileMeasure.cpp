#include "stdafx.h"
#include "math.h"
#include "QuantileMeasure.h"

// Added prototypes here rather than the "public" header file to avoid name clashes (Histogram is a common term)
// and keep these functions "private" to this module.
void Histogram16( const WORD * pSrcImage, int W, int H, int L, int T, int R, int B, int n, int * pHisto);
void Histogram16Mask( const WORD * pSrcImage, int W, int H, int L, int T, int R, int B, int * pHisto, const BYTE * pMaskImage);


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Histogram the image then work out the positions for upper & lower quantile percentages.
// If a mask image is passed in then the histogram and quantiles are calculated through the mask.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ImageQuantiles16(const WORD *pSrcImage, int ngreys, double PCBelow, double PCAbove,
					  int W, int H, int L, int T, int R, int B, WORD *pLo, WORD *pHi, const BYTE * pMaskImage /* = NULL */)
{
	double npix = 0.0;
    int i;

	// Try creating the histogram from the mask first otherwise histogram through the rectangle
    int * pHisto = new int[ngreys];
    memset(pHisto, 0, sizeof(int) * ngreys);
	if (pMaskImage)
	{
		Histogram16Mask( pSrcImage, W, H, L, T, R, B, pHisto, pMaskImage);
		for (int bin=0; bin<ngreys; bin++)
		{
			npix += pHisto[bin];
		}

	}
	else
	{
		Histogram16( pSrcImage, W, H, L, T, R, B, ngreys, pHisto);
		npix = (double)((R - L) * (B - T));
	}

    *pHi = 0x0000;
    *pLo = 0x0000;

    // Work out the number of pixels constituting PCBelow% of the total
    double pc, totsofar;
    pc = npix * PCBelow / 100.0;

    // Now calc the level at which 5% of the pixels are below
    // and the level at which 95% of the pixels are below
    totsofar = 0.0;
    for (i = 0; i < ngreys; i++)
    {
        totsofar += pHisto[i];
        if (totsofar > pc)
        {
            *pLo = i;
            break;
        }
    }

    // Work out the number of pixels constituting (100 - PCAbove)% of the total
    pc = npix * PCAbove / 100.0;

	totsofar = 0.0;
    for (i = ngreys - 1; i >= 0; i--)
    {
        totsofar += pHisto[i];
        if (totsofar > pc)
        {
            *pHi = i;
            break;
        }
    }

    delete[] pHisto;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Fast version - no ROI
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void FastImageQuantiles16(const WORD *pSrcImage, int ngreys, double PCBelow, double PCAbove, int W, int H, WORD *pLo, WORD *pHi)
{
    int i;

	// Try creating the histogram from the mask first otherwise histogram through the rectangle
    int *pHisto = new int[ngreys];
    WORD *G, GR;
	
	for (i = 0; i < ngreys; i++)
		pHisto[i] = 0;

	G = (WORD *)pSrcImage;
	
	for (i = 0; i < W * H; i++)
	{
		GR = *(G + i);
		// Frig for camera in partial scan mode, some lines are junk
		if (GR < ngreys)
			pHisto[GR]++;
	}

    *pHi = 0x0000;
    *pLo = 0x0000;

    // Work out the number of pixels constituting PCBelow% of the total
    double pc, totsofar;
    pc = W * H * PCBelow / 100.0;

    // Now calc the level at which 5% of the pixels are below
    // and the level at which 95% of the pixels are below
    totsofar = 0.0;
    for (i = 0; i < ngreys; i++)
    {
        totsofar += pHisto[i];
        if (totsofar > pc)
        {
            *pLo = i;
            break;
        }
    }

    // Work out the number of pixels constituting (100 - PCAbove)% of the total
    pc = W * H * PCAbove / 100.0;

	totsofar = 0.0;
    for (i = ngreys - 1; i >= 0; i--)
    {
        totsofar += pHisto[i];
        if (totsofar > pc)
        {
            *pHi = i;
            break;
        }
    }

    delete[] pHisto;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Histogram the 16 bit image through the rectangle given by L,T,R,B
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Histogram16( const WORD * pSrcImage, int W, int H, int L, int T, int R, int B, int n, int * pHisto)
{
    int X, Y;
    WORD G, *pG;

    for (Y = T; (Y < B)&(Y < H); Y++)
    {
        pG = (WORD *)pSrcImage + Y * W + L;

        for (X = L; (X < R)&(X < W); X++)
        {
            G = *pG++;
			if (G < n)
				pHisto[G]++;
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Histogram the 16 bit image through the rectangle given by L,T,R,B and the 16 bit mask image
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Histogram16Mask( const WORD * pSrcImage, int W, int H, int L, int T, int R, int B, int * pHisto, const BYTE * pMaskImage)
{
    int X, Y;
    WORD G, *pG;
	BYTE M, *pM;
	int offset;

    for (Y = T; (Y < B)&(Y < H); Y++)
    {
		offset = Y * W + L;
        pG = (WORD *)pSrcImage + offset;
		pM = (BYTE *)pMaskImage + offset;

        for (X = L; (X < R)&(X < W); X++)
        {
            G = *pG++;
			M = *pM++;
			if (M > 0)
				pHisto[G] ++;
        }
    }
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Histogram the image then work out the positions for upper& lower quantile percentages
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ImageQuantiles8(const BYTE *pBufPtr, double PCBelow, double PCAbove, int W, int H, int L, int T, int R, int B, BYTE *pLo, BYTE *pHi)
{
    double pc, totsofar, npix;
    int Histo[256];
    BYTE G, *pBuf, *pG;
    int i;
    int X, Y;

    pBuf = (BYTE *)pBufPtr;

	npix = (double)((R - L) * (B - T));

    // Fill it full of zeroes
    memset(&Histo[0], 0, sizeof(int) * 256);

    // Do the histogram
    for (Y = T; (Y < B)&(Y < H); Y++)
    {
        pG = pBuf + Y * W + L;

        for (X = L; (X < R)&(X < W); X++)
        {
            G = *pBufPtr++;
            Histo[G]++;
        }
    }

	// For probes ???
	//PCAbove = 0.001;

    // Work out the number of pixels constituting PCBelow% of the total
    pc = npix * PCBelow / 100.0;

    *pHi = 0xFF;
    *pLo = 0x00;

    // Now calc the level at which 5% of the pixels are below
    // and the level at which 95% of the pixels are below
    totsofar = 0.0;
    for (i = 0; i < 256; i++)
    {
        totsofar += Histo[i];
        if (totsofar > pc)
        {
            *pLo = i;
            break;
        }
    }

    // Work out the number of pixels constituting (100 - PCAbove)% of the total
    pc = npix * PCAbove / 100.0;

	totsofar = 0.0;
    for (i = 255; i >= 0; i--)
    {
        totsofar += Histo[i];
        if (totsofar > pc)
        {
            *pHi = i;
            break;
        }
    }
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Histogram the image then work out the positions for upper& lower quantile percentages
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ImageMinMax16(const WORD *pBufPtr, int s, int ngreys, WORD *pLo, WORD *pHi)
{
    int *pHisto;
    WORD G, *pBuf;
    int i;

    pBuf = (WORD *)pBufPtr;

    pHisto = new int[ngreys];

    // Fill it full of zeroes
    memset(pHisto, 0, sizeof(int) * ngreys);

    // Get min and max greys in image and histogram it
    for (i = 0; i < s; i++)
    {
        G = *pBufPtr++;
        pHisto[G]++;
    }
   
    *pHi = 0x0000;
    *pLo = 0x0000;

 
    // Find the bottom end of the histogram
    for (i = 0; i < ngreys; i++)
    {
        if (pHisto[i] != 0)
        {
            *pLo = i;
            break;
        }
    }

    // Find the top end of the histogram
    for (i = ngreys - 1; i >= 0; i--)
    {
        if (pHisto[i] != 0)
        {
            *pHi = i;
            break;
        }
    }

    delete pHisto;
}
///////////////////////////////////////////////////////////////////////////////////////////////
// Divides the image grey levels into two classes where the division point
// represents the point at which max entropy is preserved.
///////////////////////////////////////////////////////////////////////////////////////////////

int EntropyThreshold(BYTE *pImage, int W, int H)
{
	int		i, y, n,  j;
	int		Thresh = -1;
	BYTE	*ImPtr;
	DWORD	Hist[256];
	double	prob[256];
	double	Hn, Ps, Hs;
	double	psi, psiMax;

	// Get the histogram
	for (i = 0; i < 256; i++)
		Hist[i] = 0;

	n = W * H;
	ImPtr = pImage;
	for (y = 0; y < n; y++) 
	{			
		Hist[*ImPtr++]++;			
	}

	// compute probabilities 
	for (i = 0; i < 256; i++)
		prob[i] = (double) Hist[i] / (double) n;

	// find threshold 
	Hn = 0.0;
	for (i = 0; i < 256; i++)
	{
		if (prob[i] != 0.0)
			Hn -= prob[i] * log (prob[i]);
	}


	// Find threshold at which max entropy obtained
	psiMax = 0.0;
	psi = 0.0;
	for (i = 1; i < 256; i++) 
	{
		for (j = 0, Ps = Hs = 0.0; j < i; j++) 
		{
			Ps += prob[j];
			if (prob[j] > 0.0)
				Hs -= prob[j] * log (prob[j]);
		}

		if (Ps > 0.0 && Ps < 1.0)
			psi = log (Ps - Ps * Ps) + Hs / Ps + (Hn - Hs) / (1.0 - Ps);
		if (psi > psiMax) 
		{
			psiMax = psi;
			Thresh = i;
		}
	}

	return Thresh;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Threshold selection based on choosing the threshold in which the result binary
// image will have the same image moments as the original grey level image
///////////////////////////////////////////////////////////////////////////////////////////////

int MomentThreshold(BYTE *pImage, int W, int H)			// Calc threshold based on moment preservation
{
	int		i, y, n;
	int		Thresh = -1;
	BYTE	*ImPtr;
	DWORD	Hist[256];
	double	prob[256];
	double  m1, m2, m3, cd, c0, c1, z0, z1, p0, p1, pd, pDistr;

	// compile histogram 
	for (i = 0; i < 256; i++)
		Hist[i] = 0;

	n = W * H;
	ImPtr = pImage;
	for (y = 0; y < n; y++) 
	{			
		Hist[*ImPtr++]++;
	}

	// compute probabilities 
	for (i = 0; i < 255; i++)
		prob[i] = (double) Hist[i] / (double) n;

	// calculate first 3 moments 
	m1 = m2 = m3 = 0.0;
	for (i = 0; i < 255; i++) 
	{
		m1 += i * prob[i];
		m2 += i * i * prob[i];
		m3 += i * i * i * prob[i];
	}

	cd = m2 - m1 * m1;
	c0 = (-m2 * m2 + m1 * m3) / cd;
	c1 = (-m3 + m2 * m1) / cd;
	z0 = 0.5 * (-c1 - sqrt (c1 * c1 - 4.0 * c0));
	z1 = 0.5 * (-c1 + sqrt (c1 * c1 - 4.0 * c0));

	pd = z1 - z0;
	p0 = (z1 - m1) / pd;
	p1 = 1.0 - p0;

	pDistr = 0.0;

	// find threshold 
	for (Thresh = 0; Thresh < 256; Thresh++) 
	{
		pDistr += prob[Thresh];
		if (pDistr > p0)
			break;
	}

	return Thresh;
}