
//////////////////////////////////////////////////////////////////////////////////////
// Gaussian polynomial regression fit
//////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <math.h>
#include "Regression.h"

Regression::Regression(int O)
{
	m_GlobalO = O;
	m_Finished = false;

	int i;

	for (i = 0; i < 26; i++)
	{
		m_SumX[i] = 0.;
		m_SumX[i + 25] = 0.;
		m_SumYX[i] = 0.;
		m_C[i] = 0.;
	}
}

Regression::~Regression(void)
{
}

void Regression::GaussSolve(int O)
{
	int i, j, k, iMax, O1;
	double T;

	O1 = O + 1;

	// First triangulize the matrix
	for (i = 0; i <= O; i++)
	{
		iMax = i;
		T = fabs(m_M[iMax][i]);

		// Find the line with the largest absvalue in this row
		for (j = i + 1; j <= O; j++)
		{
			if (T < fabs(m_M[j][i]))
			{
				iMax = j;
				T = fabs(m_M[iMax][i]);
			}
		}

		if (i < iMax)
		{
			for (k = i; k <= O1; k++)
			{
				T = m_M[i][k];
				m_M[i][k] = m_M[iMax][k];
				m_M[iMax][k] = T;
			}
		}

		// Scale all following lines to have a leading zero
		for (j = i + 1; j <= O; j++)
		{
			T = m_M[j][i] / m_M[i][i];
			m_M[j][i] = 0.;
			for (k = i + 1; k <= O1; k++)
			{
				m_M[j][k] = m_M[j][k] - m_M[i][k] * T;
			}
		}
	}

	for (j = O; j >= 0; j--)
	{
		T = m_M[j][O1];
		for (k = j + 1; k <= O; k++)
		{
			T = T - m_M[j][k] * m_C[k];
		}

		m_C[j] = T / m_M[j][j];
	}

	m_Finished = true;
}


void Regression::BuildMatrix(int O)
{
	int i, j, O1;

	O1 = O + 1;
	for (i = 0; i <= O; i++)
	{
		for (j = 0; j <= O; j++)
		{
			m_M[i][j] = m_SumX[i + j];
		}
		m_M[i][O1] = m_SumYX[i];
	}
}


void Regression::FinaliseMatrix(int O)
{
	int i, O1;
	O1 = O + 1;
	for (i = 0; i <= O; i++)
	{
		m_M[i][O1] = m_SumYX[i];
	}
}

void Regression::Solve(void)
{
	int O = m_GlobalO;

	if (XYCount() <= O) O = XYCount() - 1;

	if (O >= 0)
	{
		BuildMatrix(O);
		GaussSolve(O);
		/*while (1 < O)
		{
			m_C[0] = 0;
			O--;
			FinaliseMatrix(O);
		}*/
	}
}


double Regression::Coefficient(int Exp)
{
	int Ex, O;
	double C;

	if (!m_Finished) Solve();

	Ex = abs(Exp);
	O = m_GlobalO;
	if (XYCount() <= O) O = XYCount() - 1;
	if (O < Ex)
		C = 0.;
	else
		C = m_C[Ex];

	return C;
}


int Regression::XYCount(void)
{
	return (int)m_SumX[0];
}


void Regression::XYAdd(double X, double Y)
{
	int i, Max2O;
	double TX;

	m_Finished = false;
	Max2O = 2 * m_GlobalO;
	TX = 1.;
	m_SumX[0]++;
	m_SumYX[0] += Y;
	for (i = 1; i <= m_GlobalO; i++)
	{
		TX *= X;
		m_SumX[i] += TX;
		m_SumYX[i] += Y * TX;
	}

	for (i = m_GlobalO + 1; i <= Max2O; i++)
	{
		TX *= X;
		m_SumX[i] += TX;
	}
}

