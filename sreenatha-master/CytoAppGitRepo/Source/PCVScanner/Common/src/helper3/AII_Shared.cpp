// AII_Shared.cpp: implementation of the CAII_Shared class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AIIShared.h"
#include "RegHelper.h"
#include "Registry.h"
#include "CFuncs.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAII_Shared::CAII_Shared()
{
}

CAII_Shared::~CAII_Shared()
{
}

CString CAII_Shared::CaseBase()
{

	// CaseBasePath must be assigned once by the app before any static method calls
	ASSERT(!CaseBasePath.IsEmpty());
	return CaseBasePath;
}


CString CAII_Shared::Root()
{
	return CaseBase() + _T("\\") + ROOT_SHARED;
}

CString CAII_Shared::ClassifiersPath()
{
	return CaseBase() + _T("\\") + CLASSIFIERS_SHARED;
}

CString CAII_Shared::ClassListsPath()
{
	return CaseBase() + _T("\\") + CLASSLISTS_SHARED;
}

CString CAII_Shared::AssaysPath()
{
	return CaseBase() + _T("\\") + ASSAYS_SHARED;
}

CString CAII_Shared::SPOTAssaysPath()
{
	return CaseBase() + _T("\\") + ASSAYS_SHARED + _T("\\Spot");
}

CString CAII_Shared::ScriptsPath()
{
	return CaseBase() + _T("\\") + SCRIPTS_SHARED;
}

CString  CAII_Shared::FormatsPath()
{
	return CaseBase() + _T("\\") + FORMATS_SHARED;
}


int CAII_Shared::InitialiseAssaysPath()
{
	CFileFind assay_finder;

	CString AssaysPath = _T("");
	CString Wild = CaseBase() + _T("\\") + ASSAYS_SHARED + _T("\\*.*");
	BOOL bFound = assay_finder.FindFile(Wild);
	m_AssayPathsCount=0;
	m_CurrentAssayPath=0;

	m_AssayPaths.RemoveAll();
	m_AssayPaths.SetSize(1, 1);

	while (bFound)
	{
		bFound = assay_finder.FindNextFile();
		if (assay_finder.IsDirectory())
		{
			CString FolderName = assay_finder.GetFileName();

			// Ignore names containing a '.' (., .., 2.0 etc)
			if (FolderName.FindOneOf(_T(".")) < 0)
			{
				m_AssayPaths.InsertAt(m_AssayPathsCount++, assay_finder.GetFilePath());
			}
		}

	}

	return m_AssayPathsCount;
}

CString CAII_Shared::NextAssaysPath()
{
	CString assayPath = _T("");

	if (m_CurrentAssayPath < m_AssayPathsCount)
		assayPath=m_AssayPaths.GetAt(m_CurrentAssayPath++);

	return assayPath;
}


CString CAII_Shared::BlobPoolPath()
{
	return CaseBase() + _T("\\") + BLOBPOOL;
}

// Get a temp directory path
CString CAII_Shared::TempDirPath()
{
	return GetExecDir() + _T("\\Temp");
}

// Static CasebasePath, should be set once in the app and can then can be accessed via static methods everywhere else
CString CAII_Shared::CaseBasePath = _T("");
