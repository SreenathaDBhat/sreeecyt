// AIReBar.cpp: implementation of the CAIReBar class.
//
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "afxcmn.h"
#include "AIToolBar.h"
#include "AIReBar.h"
#ifndef CAMMANAGED

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAIReBar::CAIReBar()
	: CReBar()
{

}

CAIReBar::~CAIReBar()
{

}

//////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////

BOOL CAIReBar::AddAIToolBar(CAIToolBar* pToolBar)
{
	BOOL brtn = CReBar::AddBar( pToolBar);

	// set up min/max sizes and ideal sizes for pieces of the rebar
	CRect rectToolBar;
	pToolBar->GetItemRect(0, &rectToolBar);

	REBARBANDINFO rbbi;
	rbbi.cbSize = sizeof(rbbi);
	rbbi.fMask = RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE | RBBIM_STYLE;
	rbbi.cxMinChild = rectToolBar.Width();
	rbbi.cyMinChild = rectToolBar.Height();
	rbbi.cx = rbbi.cxIdeal = rectToolBar.Width() * 9;
	rbbi.fStyle = RBBS_NOGRIPPER;
	GetReBarCtrl().SetBandInfo(0, &rbbi);
	

	return brtn;
}
#endif