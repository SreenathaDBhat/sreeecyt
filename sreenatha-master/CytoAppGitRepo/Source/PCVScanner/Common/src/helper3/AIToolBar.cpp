// AIToolBar.cpp: implementation of the CAIToolBar class.
//
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "AIToolBar.h"
#include "afxcmn.h"

#ifndef CAMMANAGED

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAIToolBar::CAIToolBar()
	: CToolBar()
{

}

CAIToolBar::~CAIToolBar()
{

}

//////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////

void CAIToolBar::Properties(int nButtonCount,
							UINT nColdBitmapID,
							UINT nHotBitmapID,
							UINT nMaskBitmapID,
							int nBitmapWidth,
							int nBitmapHeight,
							int nButtonMinWidth,
							int nButtonMaxWidth)
{
	// nBitmapWidth and nBitmapHeight are the individual BUTTON image width and height.
	// They MUST match the resource bitmap width (=nBitmapWidth x nButtonCount)
	// and height, otherwise these button images will look screwy.
	m_nBitmapWidth  = nBitmapWidth;
	m_nBitmapHeight = nBitmapHeight;

	// Set button min and max width
	GetToolBarCtrl().SetButtonWidth(nButtonMinWidth, nButtonMaxWidth);
	// Enable drop down lists from buttons
	GetToolBarCtrl().SetExtendedStyle(TBSTYLE_EX_DRAWDDARROWS);


	// Load mask bitmap
	CBitmap * pMask   = new CBitmap;
	if (pMask->LoadBitmap( nMaskBitmapID))
	{
		// Create image list of the correct size, bit depth and button count
		CImageList img;
		img.Create(nBitmapWidth, nBitmapHeight, ILC_COLOR32 | ILC_MASK, nButtonCount, nButtonCount);

		// Stick bitmap with mask into image list and imagelist into tool bar
		CBitmap * pBitmap = new CBitmap;
		if (pBitmap->LoadBitmap( nHotBitmapID))
		{
			img.Add( pBitmap, pMask );
			GetToolBarCtrl().SetHotImageList(&img);
		}
		img.Detach();
		delete pBitmap;
		pBitmap = NULL;

		// Create image list of the correct size, bit depth and button count
		img.Create(nBitmapWidth, nBitmapHeight, ILC_COLOR32 | ILC_MASK, nButtonCount, nButtonCount);

		// Stick bitmap with mask into image list and imagelist into tool bar
		pBitmap = new CBitmap;
		if (pBitmap->LoadBitmap( nColdBitmapID))
		{
			img.Add( pBitmap, pMask );
			GetToolBarCtrl().SetImageList(&img);
		}
		img.Detach();
		delete pBitmap;
		pBitmap = NULL;
	}

	delete pMask;
	pMask = NULL;


	// Set number of buttons
	SetButtons(NULL, nButtonCount);

	ModifyStyle(0, TBSTYLE_FLAT | TBSTYLE_TRANSPARENT);
	SetBarStyle(GetBarStyle() |	CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_FIXED);
}


void CAIToolBar::AddButton( int nIndex, UINT nID, UINT nStyle, int iImage, LPCTSTR lpszText)
{
	SetButtonInfo(nIndex, nID, nStyle, iImage);
	SetButtonText(nIndex, lpszText);
}


void CAIToolBar::SizeButtons( void)
{
	//CRect rectToolBar;
	//GetItemRect(0, &rectToolBar);
	//SetSizes(rectToolBar.Size(), CSize(m_nBitmapWidth,m_nBitmapHeight));

	//SetSizes(CSize(m_nBitmapWidth+24, m_nBitmapHeight+24), CSize(m_nBitmapWidth,m_nBitmapHeight));
	DWORD size = GetToolBarCtrl().GetButtonSize();
	SetSizes(CSize( LOWORD(size), HIWORD(size)), CSize(m_nBitmapWidth,m_nBitmapHeight));
}


#endif