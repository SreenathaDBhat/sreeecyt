// CFuncs.cpp
// Miscellaneous utility functions.
//
// Mods:
// 13Mar2003	JMB	Moved DeleteDirectoryTree() here,
//					from WinCV\src\casebase\dirfunc.cpp,
//					where it used to be called DeleteDir().	

#include "stdAfx.h"
#include "cfuncs.h"
#include "aclapi.h"
#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <strsafe.h>

#pragma comment(lib, "User32.lib")
#pragma comment(lib, "gdiplus.lib")

#include <gdiplus.h>
using namespace Gdiplus;

#define COMPANY		"Applied Imaging"
#define PROFILEPATH	"Application Data"


#define BUFSIZE 256

#define PRODUCT_PROFESSIONAL                    0x00000030
#define VER_SUITE_WH_SERVER                    0x00008000


typedef void (WINAPI *PGNSI)(LPSYSTEM_INFO);
typedef BOOL (WINAPI *PGPI)(DWORD, DWORD, DWORD, DWORD, PDWORD);

BOOL GetOSDisplayString( LPTSTR pszOS)
{
   OSVERSIONINFOEX osvi;
   SYSTEM_INFO si;
   PGNSI pGNSI;
   PGPI pGPI;
   BOOL bOsVersionInfoEx;
   DWORD dwType;

   ZeroMemory(&si, sizeof(SYSTEM_INFO));
   ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));

   osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

   if( !(bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi)) )
      return 1;

   // Call GetNativeSystemInfo if supported or GetSystemInfo otherwise.

   pGNSI = (PGNSI) GetProcAddress(
      GetModuleHandle(TEXT("kernel32.dll")), 
      "GetNativeSystemInfo");
   if(NULL != pGNSI)
      pGNSI(&si);
   else GetSystemInfo(&si);

   if ( VER_PLATFORM_WIN32_NT==osvi.dwPlatformId && 
        osvi.dwMajorVersion > 4 )
   {
      StringCchCopy(pszOS, BUFSIZE, TEXT("Microsoft "));

      // Test for the specific product.

      if ( osvi.dwMajorVersion == 6 )
      {
         if( osvi.dwMinorVersion == 0 )
         {
            if( osvi.wProductType == VER_NT_WORKSTATION )
                StringCchCat(pszOS, BUFSIZE, TEXT("Windows Vista "));
            else StringCchCat(pszOS, BUFSIZE, TEXT("Windows Server 2008 " ));
         }

         if ( osvi.dwMinorVersion == 1 )
         {
            if( osvi.wProductType == VER_NT_WORKSTATION )
                StringCchCat(pszOS, BUFSIZE, TEXT("Windows 7 "));
            else StringCchCat(pszOS, BUFSIZE, TEXT("Windows Server 2008 R2 " ));
         }
         
         pGPI = (PGPI) GetProcAddress(
            GetModuleHandle(TEXT("kernel32.dll")), 
            "GetProductInfo");

         pGPI( osvi.dwMajorVersion, osvi.dwMinorVersion, 0, 0, &dwType);

         switch( dwType )
         {
            case PRODUCT_ULTIMATE:
               StringCchCat(pszOS, BUFSIZE, TEXT("Ultimate Edition" ));
               break;
            case PRODUCT_PROFESSIONAL:
               StringCchCat(pszOS, BUFSIZE, TEXT("Professional" ));
               break;
            case PRODUCT_HOME_PREMIUM:
               StringCchCat(pszOS, BUFSIZE, TEXT("Home Premium Edition" ));
               break;
            case PRODUCT_HOME_BASIC:
               StringCchCat(pszOS, BUFSIZE, TEXT("Home Basic Edition" ));
               break;
            case PRODUCT_ENTERPRISE:
               StringCchCat(pszOS, BUFSIZE, TEXT("Enterprise Edition" ));
               break;
            case PRODUCT_BUSINESS:
               StringCchCat(pszOS, BUFSIZE, TEXT("Business Edition" ));
               break;
            case PRODUCT_STARTER:
               StringCchCat(pszOS, BUFSIZE, TEXT("Starter Edition" ));
               break;
            case PRODUCT_CLUSTER_SERVER:
               StringCchCat(pszOS, BUFSIZE, TEXT("Cluster Server Edition" ));
               break;
            case PRODUCT_DATACENTER_SERVER:
               StringCchCat(pszOS, BUFSIZE, TEXT("Datacenter Edition" ));
               break;
            case PRODUCT_DATACENTER_SERVER_CORE:
               StringCchCat(pszOS, BUFSIZE, TEXT("Datacenter Edition (core installation)" ));
               break;
            case PRODUCT_ENTERPRISE_SERVER:
               StringCchCat(pszOS, BUFSIZE, TEXT("Enterprise Edition" ));
               break;
            case PRODUCT_ENTERPRISE_SERVER_CORE:
               StringCchCat(pszOS, BUFSIZE, TEXT("Enterprise Edition (core installation)" ));
               break;
            case PRODUCT_ENTERPRISE_SERVER_IA64:
               StringCchCat(pszOS, BUFSIZE, TEXT("Enterprise Edition for Itanium-based Systems" ));
               break;
            case PRODUCT_SMALLBUSINESS_SERVER:
               StringCchCat(pszOS, BUFSIZE, TEXT("Small Business Server" ));
               break;
            case PRODUCT_SMALLBUSINESS_SERVER_PREMIUM:
               StringCchCat(pszOS, BUFSIZE, TEXT("Small Business Server Premium Edition" ));
               break;
            case PRODUCT_STANDARD_SERVER:
               StringCchCat(pszOS, BUFSIZE, TEXT("Standard Edition" ));
               break;
            case PRODUCT_STANDARD_SERVER_CORE:
               StringCchCat(pszOS, BUFSIZE, TEXT("Standard Edition (core installation)" ));
               break;
            case PRODUCT_WEB_SERVER:
               StringCchCat(pszOS, BUFSIZE, TEXT("Web Server Edition" ));
               break;
         }
      }

      if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2 )
      {
         if( GetSystemMetrics(SM_SERVERR2) )
            StringCchCat(pszOS, BUFSIZE, TEXT( "Windows Server 2003 R2, "));
         else if ( osvi.wSuiteMask & VER_SUITE_STORAGE_SERVER )
            StringCchCat(pszOS, BUFSIZE, TEXT( "Windows Storage Server 2003"));
         else if ( osvi.wSuiteMask & VER_SUITE_WH_SERVER )
            StringCchCat(pszOS, BUFSIZE, TEXT( "Windows Home Server"));
         else if( osvi.wProductType == VER_NT_WORKSTATION &&
                  si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64)
         {
            StringCchCat(pszOS, BUFSIZE, TEXT( "Windows XP Professional x64 Edition"));
         }
         else StringCchCat(pszOS, BUFSIZE, TEXT("Windows Server 2003, "));

         // Test for the server type.
         if ( osvi.wProductType != VER_NT_WORKSTATION )
         {
            if ( si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_IA64 )
            {
                if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
                   StringCchCat(pszOS, BUFSIZE, TEXT( "Datacenter Edition for Itanium-based Systems" ));
                else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
                   StringCchCat(pszOS, BUFSIZE, TEXT( "Enterprise Edition for Itanium-based Systems" ));
            }

            else if ( si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64 )
            {
                if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
                   StringCchCat(pszOS, BUFSIZE, TEXT( "Datacenter x64 Edition" ));
                else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
                   StringCchCat(pszOS, BUFSIZE, TEXT( "Enterprise x64 Edition" ));
                else StringCchCat(pszOS, BUFSIZE, TEXT( "Standard x64 Edition" ));
            }

            else
            {
                if ( osvi.wSuiteMask & VER_SUITE_COMPUTE_SERVER )
                   StringCchCat(pszOS, BUFSIZE, TEXT( "Compute Cluster Edition" ));
                else if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
                   StringCchCat(pszOS, BUFSIZE, TEXT( "Datacenter Edition" ));
                else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
                   StringCchCat(pszOS, BUFSIZE, TEXT( "Enterprise Edition" ));
                else if ( osvi.wSuiteMask & VER_SUITE_BLADE )
                   StringCchCat(pszOS, BUFSIZE, TEXT( "Web Edition" ));
                else StringCchCat(pszOS, BUFSIZE, TEXT( "Standard Edition" ));
            }
         }
      }

      if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1 )
      {
         StringCchCat(pszOS, BUFSIZE, TEXT("Windows XP "));
         if( osvi.wSuiteMask & VER_SUITE_PERSONAL )
            StringCchCat(pszOS, BUFSIZE, TEXT( "Home Edition" ));
         else StringCchCat(pszOS, BUFSIZE, TEXT( "Professional" ));
      }

      if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0 )
      {
         StringCchCat(pszOS, BUFSIZE, TEXT("Windows 2000 "));

         if ( osvi.wProductType == VER_NT_WORKSTATION )
         {
            StringCchCat(pszOS, BUFSIZE, TEXT( "Professional" ));
         }
         else 
         {
            if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
               StringCchCat(pszOS, BUFSIZE, TEXT( "Datacenter Server" ));
            else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
               StringCchCat(pszOS, BUFSIZE, TEXT( "Advanced Server" ));
            else StringCchCat(pszOS, BUFSIZE, TEXT( "Server" ));
         }
      }

       // Include service pack (if any) and build number.

      if( _tcslen(osvi.szCSDVersion) > 0 )
      {
          StringCchCat(pszOS, BUFSIZE, TEXT(" ") );
          StringCchCat(pszOS, BUFSIZE, osvi.szCSDVersion);
      }

      TCHAR buf[80];

      StringCchPrintf( buf, 80, TEXT(" (build %d)"), osvi.dwBuildNumber);
      StringCchCat(pszOS, BUFSIZE, buf);

      if ( osvi.dwMajorVersion >= 6 )
      {
         if ( si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64 )
            StringCchCat(pszOS, BUFSIZE, TEXT( ", 64-bit" ));
         else if (si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_INTEL )
            StringCchCat(pszOS, BUFSIZE, TEXT(", 32-bit"));
      }
      
      return TRUE; 
   }

   else
   {  
      printf( "This sample does not support this version of Windows.\n");
      return FALSE;
   }
}

BOOL GetWinVersion( LPTSTR pszOS)
{
   OSVERSIONINFOEX osvi;
   SYSTEM_INFO si;
   PGNSI pGNSI;
   //PGPI pGPI;
   BOOL bOsVersionInfoEx;
   //DWORD dwType;

	StringCchCopy(pszOS, BUFSIZE, TEXT("UNSUPPORTED"));

   ZeroMemory(&si, sizeof(SYSTEM_INFO));
   ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));

   osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

   if( !(bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi)) )
      return 1;



   // Call GetNativeSystemInfo if supported or GetSystemInfo otherwise.

   pGNSI = (PGNSI) GetProcAddress(
      GetModuleHandle(TEXT("kernel32.dll")), 
      "GetNativeSystemInfo");
   if(NULL != pGNSI)
      pGNSI(&si);
   else GetSystemInfo(&si);

   if ( VER_PLATFORM_WIN32_NT==osvi.dwPlatformId && 
        osvi.dwMajorVersion > 4 )
   {
      // Test for the specific product.

      if ( osvi.dwMajorVersion == 6 )
      {
         if ( osvi.dwMinorVersion == 1 )
         {
            if( osvi.wProductType == VER_NT_WORKSTATION )
				StringCchCopy(pszOS, BUFSIZE, TEXT("WINDOWS_7"));
            else 
				StringCchCopy(pszOS, BUFSIZE, TEXT("WINDOWS_SERVER_2008_R2"));
         }
      }

      if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2 )
      {
		 if( osvi.wProductType == VER_NT_WORKSTATION && si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64)
         {
			StringCchCopy(pszOS, BUFSIZE, TEXT("WINDOWS_XP_x64"));
		 }
      }

      if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1 )
      {
			StringCchCopy(pszOS, BUFSIZE, TEXT("WINDOWS_XP"));
      }

      return TRUE; 
   }
   else
   {  
      return FALSE;
   }
}


/////////////////////////////////////////////////////////////////////////////
//
//	CreateFullPathDirectory
//
//	Create directory and any preceeding path to it
//	Use UNC path name (or <driveletter>:\name)
//	SN	24Feb03	Cribbed from casebase.cpp
//
BOOL CreateFullPathDirectory(LPCTSTR dir)
{
// Doesn't check for errors in creation of directories, just checks 
// for existence of complete directory path after creating it.
// Done this because GetFileAttributes() does not return a sensible value for
// some components of NFS paths (export names) on Interactive UNIX 4.1 systems
// without the system maintenance update. This is in addition to 
// CreateDirectory() not returning a useful failure value if these 
// 'directories' already exist, even with 4.1mu systems.
	LPCTSTR pDir, pSep;
	LPTSTR path = new TCHAR[_tcslen(dir)+1];

	// Create parent directories, ignoring errors. If any of these fail to be
	// created there will be an error creating the full directory itself, so
	// such errors will always be picked up indirectly anyway.
	pDir = dir;
	// Move past initial slash before trying to create directories.			//SN24Feb03
	if (*pDir == '\\')
	{
		pDir++;
	}
	// Move past UNC server and share names before creating directories.	//SN26Feb03
	if (*pDir == '\\')
	{
		pDir++;
		pDir = _tcschr(pDir, '\\');	// Server name
		pDir++;
		pDir = _tcschr(pDir, '\\');	// Share name
		pDir++;
	}
	while ((pSep = _tcschr(pDir, '\\')) != NULL)
	{
		_tcsncpy(path, dir, pSep - dir);
		path[pSep-dir] = '\0';

		CreateDirectory(path, NULL);

		pDir = pSep + 1;
	}

	delete path;
	path = NULL;

	// Create full directory.
	CreateDirectory(dir, NULL);

	// Finally, check for errors by checking if full directory path exists.
	DWORD attribs = GetFileAttributes(dir);

	if (attribs != 0xFFFFFFFF && (attribs & FILE_ATTRIBUTE_DIRECTORY))
		return TRUE;
	else
		return FALSE;
}


//////////////////////////////////////////////////////////////////////
//
//	D e l e t e D i r e c t o r y T r e e
//	Delete a directory, all its subdirectories and files within them.
//  Read-only files will be deleted too.
//	Returns TRUE if OK, or FALSE on failure
//
BOOL DeleteDirectoryTree(LPCTSTR dirpath)
{
	BOOL rval = TRUE;
	TCHAR path[MAX_PATH];
	WIN32_FIND_DATA fd;
	HANDLE hdir;
	DWORD err = ERROR_SUCCESS;

	// I find this function very scary, though it's better than having calls
	// to 'rm -rf' scattered around. So here are some checks to stop nasty
	// things happening if dirpath for some reason is set to something invalid.
	if (!dirpath)            return FALSE; // Check for valid pointer.
	// If dirpath is an empty string, we end up deleting a whole drive "\\*"!
	if (_tcslen(dirpath) < 1) return FALSE;

	// Okay, you may not like this one. This assumes that any directory tree 
	// that we intentionally want to delete will not have a path of less than 
	// four characters. This is an attempt to avoid deleting an entire drive 
	// with a path like C:\, for example. Directories that we really want to 
	// delete will generally be subdirectories of 'cases', 'library', 
	// 'cytonet', archive etc., so as CytoVision stands at the minute such a
	// short path would always be a bug. No I'm not paranoid. Well not much.
	if (_tcslen(dirpath) < 4)
	{
		//char mess[64] = "Delete entire directory tree '";
		//strcat(mess, dirpath);
		//strcat(mess, "'?");
		//if (AfxMessageBox(mess, MB_OKCANCEL) != IDOK) return FALSE;
		return FALSE;
	}


	// Get directory wildcard path
	_tcscpy(path, dirpath);
	_tcscat(path, _T("\\*"));
	if ((hdir = FindFirstFile(path, &fd)) != INVALID_HANDLE_VALUE)
	{
		do
		{
			// Ignore "." and ".." directories, if present. Note these cannot
			// be assumed to exist in all situations, e.g. NTFS root directory,
			// directories in UDF filesystems with some UDF drivers.
			if (   _tcscmp(fd.cFileName, _T(".")) == 0
			    || _tcscmp(fd.cFileName, _T("..")) == 0) continue;

			// Build full pathname
			_tcscpy(path, dirpath);
			_tcscat(path, _T("\\"));
			_tcscat(path, fd.cFileName);

			// Make sure file or directory does not have read-only attribute,
			// or it will not be possible to delete it.
			SetFileAttributes(path, FILE_ATTRIBUTE_NORMAL);

			// Directories, recursively delete
			// Regular files, just delete them
			if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				rval = DeleteDirectoryTree(path);
			else
				rval = DeleteFile(path);

			// Need to propagate exact error code back up - get it now before
			// it is overwritten by any subsequent successful function calls.
			if (!rval)
			{
				err = GetLastError();
				break;
			}
		}
		while (FindNextFile(hdir, &fd));

		FindClose(hdir);
	}
	else
		rval = FALSE;


	// After deleting directory contents, delete the directory
	if (rval)
	{
		// Cannot remove a directory if the current directory for the process
		// is set to it, so first make sure the current directory is something
		// different. Note: don't move the current directory to "..", as ".."
		// does not exist in all situations, as noted above.
		if (rval = SetCurrentDirectory(_T("\\")))
		    rval = RemoveDirectory(dirpath);

		if (!rval)
			err = GetLastError();
	}
	
	// Set error code to the last error
	if (!rval)
		SetLastError(err);

	return rval;
}


BOOL SetEveryonePermission(LPTSTR FileName)
{
	TCHAR wEveryOne[] = _T("Everyone");
	return SetFilePermission(FileName, wEveryOne, GENERIC_ALL, SUB_CONTAINERS_AND_OBJECTS_INHERIT);
}

BOOL SetFilePermission(LPTSTR FileName, LPTSTR TrusteeName, DWORD AccessMask, DWORD InheritFlag)
{
//	DWORD AccessMask = GENERIC_ALL;
//	DWORD InheritFlag = SUB_CONTAINERS_AND_OBJECTS_INHERIT;//NO_INHERITANCE;
	ACCESS_MODE option = SET_ACCESS;
	EXPLICIT_ACCESS explicitaccess;

	PACL ExistingDacl;
	PACL NewAcl = NULL;
	PSECURITY_DESCRIPTOR psd = NULL;

	DWORD dwError;
	BOOL bSuccess = FALSE; // assume failure


   //
    // get current Dacl on specified file
    //

    dwError = GetNamedSecurityInfo(
                        FileName,
                        SE_FILE_OBJECT,
                        DACL_SECURITY_INFORMATION,
                        NULL,
                        NULL,
                        &ExistingDacl,
                        NULL,
                        &psd
                        );

    if(dwError != ERROR_SUCCESS) {
//        DisplayLastError("GetNamedSecurityInfo");
        goto cleanup;
    }

    BuildExplicitAccessWithName(
            &explicitaccess,
            TrusteeName,
            AccessMask,
            option,
            InheritFlag
            );

    //
    // add specified access to the object
    //

    dwError = SetEntriesInAcl(
            1,
            &explicitaccess,
            ExistingDacl,
            &NewAcl
            );

    if(dwError != ERROR_SUCCESS) {
//        DisplayLastError("SetEntriesInAcl");
        goto cleanup;
    }

    //
    // apply new security to file
    //

    dwError = SetNamedSecurityInfo(
                    FileName,
                    SE_FILE_OBJECT, // object type
                    DACL_SECURITY_INFORMATION,
                    NULL,
                    NULL,
                    NewAcl,
                    NULL
                    );

    if(dwError != ERROR_SUCCESS) {
//        DisplayLastError("SetNamedSecurityInfo");
        goto cleanup;
    }

    bSuccess = TRUE; // indicate success

cleanup:

    if( NewAcl != NULL ) AccFree( NewAcl );
    if( psd != NULL) AccFree( psd );


    if(!bSuccess)
        return FALSE;

    return TRUE;

}

int DoSystemCommand(LPCTSTR cmd, LPCTSTR dir)
{
	BOOL cpr;
	STARTUPINFO startup;
	PROCESS_INFORMATION pinfo;
	DWORD exit_code;
	TCHAR str[_MAX_PATH];

	_tcscpy(str, cmd);
	// Create a new process and exec it
	memset(&startup, 0, sizeof(startup));
	startup.cb = sizeof(startup);
	startup.dwFlags = STARTF_FORCEOFFFEEDBACK;	// Don't use feedback cursor

	cpr = CreateProcess(NULL,		// pointer to name of executable module
						str,		// pointer to command line string
						NULL,		// pointer to process security attributes
						NULL,		// pointer to thread security attributes
						FALSE,		// handle inheritance flag
						DETACHED_PROCESS,	// creation flags
						NULL,		// pointer to new environment block
						dir,		// pointer to current directory name
						&startup,	// pointer to STARTUPINFO
						&pinfo);	// pointer to PROCESS_INFORMATION


	if (cpr == FALSE)
		return -1;

	// Wait around for termination status (Timeout after 60 sec)
	if (WaitForSingleObject(pinfo.hProcess, INFINITE) == WAIT_FAILED)
		return -2;

	// Get command exit status
	if (GetExitCodeProcess(pinfo.hProcess, &exit_code) == FALSE)
		return -3;	// Can't get exit code

	return exit_code;
}

static int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
   UINT  num = 0;          // number of image encoders
   UINT  size = 0;         // size of the image encoder array in bytes

   ImageCodecInfo* pImageCodecInfo = NULL;

   GetImageEncodersSize(&num, &size);
   if(size == 0)
      return -1;  // Failure

   pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
   if(pImageCodecInfo == NULL)
      return -1;  // Failure

   GetImageEncoders(num, size, pImageCodecInfo);

   for(UINT j = 0; j < num; ++j)
   {
      if( wcscmp(pImageCodecInfo[j].MimeType, format) == 0 )
      {
         *pClsid = pImageCodecInfo[j].Clsid;
         free(pImageCodecInfo);
         return j;  // Success
      }    
   }

   free(pImageCodecInfo);
   return -1;  // Failure
}

BOOL SaveAsJPeg(LPCTSTR path, BITMAPINFO bminfo, BYTE *bits)
{
    Gdiplus::GdiplusStartupInput gdiplusStartupInput;

    // Initialise GDI+ here and set flag
	ULONG_PTR gdiplusToken;
    if (!Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL) == Gdiplus::Ok)
		return FALSE;	

	// If we've got a bitmap then lets save it to path
	CLSID  clsid;
	// Uncomment and provide a value for quality if want to use something other than default res
//	EncoderParameters encoderParameters;
//	EncoderParameters * pEncode = NULL;

	GetEncoderClsid(L"image/jpeg", &clsid);
//	encoderParameters.Count = 1;
//	encoderParameters.Parameter[0].Guid = EncoderQuality;
//	encoderParameters.Parameter[0].Type = EncoderParameterValueTypeLong;
//	encoderParameters.Parameter[0].NumberOfValues = 1;
//	encoderParameters.Parameter[0].Value = &quality;
//	pEncode = &encoderParameters;

	// OK, generate a bitmap out of the bits
	Bitmap *pBitmap;
	if (bits)
	{
		pBitmap = new Bitmap(&bminfo, (VOID*)bits);
	}
	else
		return false;

	// gotta convert to widechar for this function
	WCHAR wszPath[MAX_PATH];

#ifndef _UNICODE
	MultiByteToWideChar( CP_ACP, 0, path, _tcslen(path)+1, wszPath,  sizeof(wszPath)/sizeof(wszPath[0]) );
#else
	_tcscpy(wszPath, path);
#endif

	//pBitmap->Save(wszPath, &clsid, pEncode);
	pBitmap->Save(wszPath, &clsid);

    Gdiplus::GdiplusShutdown(gdiplusToken);

	return TRUE;
}

BOOL UserDirectory(LPCTSTR app, LPCTSTR folder, LPTSTR rbuff, int size)
{
	// Check the macro dir exits and if not then create it
	ASSERT(app);
	ASSERT(folder);
	BOOL bRet = FALSE;

	TCHAR userprofile[_MAX_PATH];
	ExpandEnvironmentStrings(_T("%USERPROFILE%"), userprofile, sizeof(userprofile));
	TCHAR buff[_MAX_PATH];

	wsprintf(buff, _T("%s\\%s\\%s\\%s\\%s"), userprofile, PROFILEPATH, COMPANY, app, folder);

	if (CreateFullPathDirectory(buff))
	{
		if (_tcslen(buff) < (UINT)size)
		{
			_tcsncpy(rbuff, buff, size);
			bRet = TRUE;
		}
	}
	
	return bRet;
}

//Generic version
BOOL GetExecDir(TCHAR path[], size_t size)
{
// Get the directory the current module (e.g. app or DLL) was loaded from.
	BOOL status = FALSE;

	if (path && size > 0)
	{
		path[0] = '\0';
		if (GetModuleFileName(NULL, path, (DWORD)size) != 0)
		{
			// Remove the filename from the end of the path.
			LPTSTR pPathSep = _tcsrchr(path, '\\');
			if (pPathSep)
				*pPathSep = '\0';

			status = TRUE;
		}
	}

	return status;
}

//ASCII version
BOOL GetExecDirA(char path[], size_t size)
{
// Get the directory the current module (e.g. app or DLL) was loaded from.
	BOOL status = FALSE;

	if (path && size > 0)
	{
		path[0] = '\0';
		if (GetModuleFileNameA(NULL, path, (DWORD) size) != 0)
		{
			// Remove the filename from the end of the path.
			char *pPathSep = strrchr(path, '\\');
			if (pPathSep)
				*pPathSep = '\0';

			status = TRUE;
		}
	}

	return status;
}

CString GetExecDir()
{
	CString dir;

	TCHAR path[_MAX_PATH];
	if (GetExecDir(path, (size_t)_MAX_PATH))
		dir = path;

	return dir;
}

//////////////////////////////////////////////////////////////////////
// Replace illegal char specified by Illegal in str
//////////////////////////////////////////////////////////////////////

void ReplaceIllegal(TCHAR Illegal, LPTSTR str, TCHAR Subst)
{
	TCHAR *S;

	while (S = _tcschr(str, Illegal))
		*S = Subst;
}


// Remove illegal chars from a filename and replace with Subst
void CleanFileName(LPTSTR FileName, TCHAR Subst)			
{
	// Trap any illegal chars in dirname
	ReplaceIllegal(_T('\\'), FileName, Subst);
	ReplaceIllegal(_T('/'),  FileName, Subst);
	ReplaceIllegal(_T('*'),  FileName, Subst);
	ReplaceIllegal(_T('?'),  FileName, Subst);
	ReplaceIllegal(_T(':'),  FileName, Subst);
	ReplaceIllegal(_T('<'),  FileName, Subst);
	ReplaceIllegal(_T('>'),  FileName, Subst);
	ReplaceIllegal(_T('|'),  FileName, Subst);
}

