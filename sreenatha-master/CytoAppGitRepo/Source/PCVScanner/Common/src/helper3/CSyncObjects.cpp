////////////////////////////////////////////////////////////////////////////////
// This wrapper class is used to monitor the status of the autofocus event
// as set by the grabserver and read by any thread requiring it. NOTE it is a
// globally machine wide named event which is autoreset
// Implementation file
// NOTE we could just use the MFC classes - but they don't always give you what 
// you want and more than likely have a lot of overheads
// K Ratcliff 23022004
////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "CSyncObjects.h"

////////////////////////////////////////////////////////////////////////////////
// Events
// Type = Open an existing event or create a new one
// Name = the event name if it uses one (defaults to NULL)
// bManualReset = TRUE if the event must be reset programmatically, defaults
//                to an autoreset event
////////////////////////////////////////////////////////////////////////////////

CSyncObjectEvent::CSyncObjectEvent(CSyncObjectOpenType Type, LPCTSTR Name, BOOL bManualReset)
{
    switch (Type)
    {
    case SyncObjectOpen:
        m_hEvent = OpenEvent(EVENT_ALL_ACCESS, FALSE, Name);
        break;
    case SyncObjectCreate:
        m_hEvent = CreateEvent(NULL, bManualReset, FALSE, Name);
        break;
    default:
        m_hEvent = NULL;
        break;
    }

    ASSERT(m_hEvent);
}

CSyncObjectEvent::~CSyncObjectEvent(void)
{
    CloseHandle(m_hEvent);
}

////////////////////////////////////////////////////////////////////////////////
// Simply test to see it the object is signalled - do not hang around for more
// than TimeOut milliseconds. 
////////////////////////////////////////////////////////////////////////////////

BOOL CSyncObjectEvent::Signalled(DWORD TimeOut)
{
    DWORD dwRes;

    dwRes = WaitForSingleObject(m_hEvent, TimeOut);

    return (dwRes == WAIT_OBJECT_0);
}

////////////////////////////////////////////////////////////////////////////////
// Test to see it the object is signalled waiting TimeOut period 
////////////////////////////////////////////////////////////////////////////////

int CSyncObjectEvent::WaitSignalled(DWORD TimeOut)
{
    DWORD dwRes;

    dwRes = WaitForSingleObject(m_hEvent, TimeOut);

    return dwRes;
}
////////////////////////////////////////////////////////////////////////////////
// Set or Reset the event
////////////////////////////////////////////////////////////////////////////////

void CSyncObjectEvent::Signal(BOOL Set)
{
    if (Set)
        SetEvent(m_hEvent);
    else
        ResetEvent(m_hEvent);
}

////////////////////////////////////////////////////////////////////////////////
// Mutexes
// Type = open or create
// Name = the name of the mutex if it has one, defaults to NULL
// bInitialOwner if this is the initial owner of the mutex, defaults to FALSE
////////////////////////////////////////////////////////////////////////////////

CSyncObjectMutex::CSyncObjectMutex(CSyncObjectOpenType Type, LPCTSTR Name, BOOL bInitialOwner)
{
    switch (Type)
    {
    case SyncObjectOpen:
        m_hMutex = OpenMutex(MUTEX_ALL_ACCESS, FALSE, Name);
        break;
    case SyncObjectCreate:
        m_hMutex = CreateMutex(NULL, bInitialOwner, Name);
        break;
    default:
        m_hMutex = NULL;
        break;
    }

    ASSERT(m_hMutex);
}

CSyncObjectMutex::~CSyncObjectMutex()
{
    CloseHandle(m_hMutex);
}

////////////////////////////////////////////////////////////////////////////////
// Grab
//  Tries to get access to the mutex, if it succeeds it has got the mutex
//  and returns TRUE else returns FALSE
//  TimeOut = timeout in millisecs, see WaitForSingleObject, defaults to 0 
//            don't wait
////////////////////////////////////////////////////////////////////////////////

BOOL CSyncObjectMutex::Grab(DWORD TimeOut)
{
    DWORD dwRes;

    dwRes = WaitForSingleObject(m_hMutex, TimeOut);

    return (dwRes == WAIT_OBJECT_0);
}

////////////////////////////////////////////////////////////////////////////////
// Release
//    Let go of the mutex so someone else can play with it
////////////////////////////////////////////////////////////////////////////////

void CSyncObjectMutex::Release(void)           // Release hold on mutex
{
    ReleaseMutex(m_hMutex);
}

////////////////////////////////////////////////////////////////////////////////
// Critical Section wrapper. Critical sections offer considerably LESS
// overheads than other threading objects.
////////////////////////////////////////////////////////////////////////////////

CSyncObjectCriticalSection::CSyncObjectCriticalSection()
{
    m_hSemaphore = CreateSemaphore(NULL, 1, 1, NULL);
}

CSyncObjectCriticalSection::~CSyncObjectCriticalSection()
{
    Release();

	CloseHandle(m_hSemaphore);
}

////////////////////////////////////////////////////////////////////////////////
// Grab
//  BLOCKS until it gets access to the critical section. NOTE this won't
//  cause a problem if the code is right
////////////////////////////////////////////////////////////////////////////////

void CSyncObjectCriticalSection::Grab(void)             // Get access to the critical section
{
    WaitForSingleObject(m_hSemaphore, INFINITE);
}

////////////////////////////////////////////////////////////////////////////////
// TryAndGrab
//  Tries to get access to the critical section, if it succeeds it has got the mutex
//  and returns TRUE else returns FALSE
//  TimeOut = timeout in millisecs
////////////////////////////////////////////////////////////////////////////////

BOOL CSyncObjectCriticalSection::TryAndGrab(DWORD TimeOut)             // Get access to the critical section
{
    DWORD dwRes;
    dwRes = WaitForSingleObject(m_hSemaphore, TimeOut);

    return (dwRes == WAIT_OBJECT_0);
}

void CSyncObjectCriticalSection::Release(void)           // Release the section
{
    long Prev;

    ReleaseSemaphore(m_hSemaphore, 1, &Prev);
}


////////////////////////////////////////////////////////////////////////////////
// Semaphores
////////////////////////////////////////////////////////////////////////////////

CSyncObjectSemaphore::CSyncObjectSemaphore(long InitialSpinCount, long MaxSpinCount)
{
	SECURITY_ATTRIBUTES sa;

	// Set this up as the read and write handles are inheritable
    sa.nLength = sizeof(SECURITY_ATTRIBUTES);
    sa.bInheritHandle = TRUE;
    sa.lpSecurityDescriptor = NULL;

	m_hSemaphore = CreateSemaphore(&sa, InitialSpinCount, MaxSpinCount, NULL);
}

CSyncObjectSemaphore::~CSyncObjectSemaphore()
{
	CloseHandle(m_hSemaphore);
}

long CSyncObjectSemaphore::Release(long ReleaseCount/*=1*/)           
{
    long Prev;

    ReleaseSemaphore(m_hSemaphore, ReleaseCount, &Prev);

	return Prev;
}

BOOL CSyncObjectSemaphore::Signalled(DWORD TimeOut)             // Get access to the critical section
{
    DWORD dwRes;
    dwRes = WaitForSingleObject(m_hSemaphore, TimeOut);

    return (dwRes == WAIT_OBJECT_0);
}
