/*
 *	Registry.c		BP	12/13/96
 *
 *	Routines for saving and loading entries from the
 *	registry.
 *
 * Mods:
 *
 *	05Jun03     MC  Return from CVRegSetPermissions() if not running on an XP box - a crash occurs in LocalFree() if ran on NT.
 *					Put test inside for future proofing
 *	05Mar2003	JMB	Added CVSetRegistryPermissions()
 *	18Nov1999	JMB	Fixed calls to RegSetValueEx() for string values, so that
 *					the size parameter includes the null terminator. This didn't
 *					seem to be causing any problems, other than zero length
 *					strings not being displayed properly in regedit, but is
 *					specified in the function's documentation.
 *	17Sep97   WH: Changed KEYPATH and added subkey to all functions
 */


#include "stdafx.h"
#include "Registry.h"
#include "cfuncs.h"

#include <stdio.h>
#include <tchar.h>
#include <aclapi.h> // For GetSecurityInfo() etc.
#include "OSVersion.h"
#include <userenv.h>
#include "aiishared.h"
/*
 * Path to entries in the registry.
 * Values will be held in this sub-branch of both
 * HKEY_LOCAL_MACHINE and HKEY_CURRENT_USER,
 * depending on the choice of CVRegMachineXXX()
 * or CVRegUserXXX() functions.
 */
#define COMPANY		_T("Applied Imaging")
#define APP			_T("Cytovision")

// On a 64-bit system, registry values are stored in the 32-bit part of the registry, as most of our software is still 32-bit.
// The 32-bit part of the registry appears to 32-bit processes to be in the same location as it is on a 32-bit system,
// but to 64-bit processes it appears under 'Wow6432Node'.
#define	KEYPATH      _T("Software\\Applied Imaging\\Cytovision") // What the registry location for 32-bit apps looks like on 32 or 64-bit OS.
#define	KEYPATH_x64  _T("Software\\Wow6432Node\\Applied Imaging\\Cytovision") // What the registry location for 32-bit apps looks like for 64-bit apps.

#define PROFILEPATH	_T("Application Data")

#define EVERYONE_GROUPNAME  _T("Everyone")

const CString sADOConnString =_T("Provider=SQLOLEDB.1;"
								 "Integrated Security=SSPI;Persist Security Info=False;"
								 "Initial Catalog=aii_db;Data Source=");
/*

Overview.
---------

There are two sets of functions, relating to global
(machine) settings and user-specific settings (preferences).
It is up to the programmer to decide which set a given
parameter should belong to.
It is also up to the programmer what the registry entries
should be called - and to be explicit enough to avoid
duplicate names!

Functions are in pairs, and are mostly based on core
functions that work with strings. These core functions can
be called directly, or via the convenience functions that
incorporate translations to specific parameter types.
e.g. a window position/size should be stored as a RECT,
using CVRegUserGetRect() et al - these functions actually
store the RECT structure as a string of four numbers.

The Windows API for the registry is very nice and compact,
so this is a relatively small wrapper module, and all the
functions herein are public.

All functions return 0 if successful, and pass values back
through pointer parameters of the appropriate kind.

*/


//**********************************************************
// Primitives.
//**********************************************************

/*
 *	Return the key path name - for cases where we need to
 *	use direct registry functions.
 */
LPTSTR CVRegKeyGet()
{
#if defined(_WIN64)
	return KEYPATH_x64;
#else
	return KEYPATH;
#endif
}

/*
 * Delete a Key and all subkeys from HKEY_LOCAL_MACHINE.
 */

int  CVRegMachineDelete(LPCTSTR  subkey)
{
	// This function will fail if the subkeys specified has subkeys itself
	TCHAR fullkey[512];	// buffer where a subkey is appened to the keypath

#if defined(_WIN64)
	_tcscpy(fullkey, KEYPATH_x64);
#else
	_tcscpy(fullkey, KEYPATH);
#endif

	if (subkey)
		_tcscat(fullkey,subkey);

	if (RegDeleteKey(HKEY_LOCAL_MACHINE, fullkey) == ERROR_SUCCESS)
	{
		return 0;
	}

	return -1;
}


/*
 * Delete a Key and all subkeys from HKEY_CURRENT_USER
 */

int  CVRegUserDelete(LPCTSTR  subkey)
{
	// This function will fail if the subkeys specified has subkeys itself
	TCHAR fullkey[512];	// buffer where a subkey is appened to the keypath

#if defined(_WIN64)
	_tcscpy(fullkey, KEYPATH_x64);
#else
	_tcscpy(fullkey, KEYPATH);
#endif

	if (subkey)
		_tcscat(fullkey,subkey);

	if (RegDeleteKey(HKEY_CURRENT_USER, fullkey) == ERROR_SUCCESS)
	{
		return 0;
	}

	return -1;
}

/*
 * Obtain a string value from HKEY_LOCAL_MACHINE.
 */

int  CVRegMachineGet(LPCTSTR  subkey, LPCTSTR  label, LPTSTR  value, int maxlen)
{
	HKEY key;
	DWORD len, type;
	LONG retval;
	TCHAR fullkey[512];	// buffer where a subkey is appened to the keypath

#if defined(_WIN64)
	_tcscpy(fullkey, KEYPATH_x64);
#else
	_tcscpy(fullkey, KEYPATH);
#endif

	if (subkey)
		_tcscat(fullkey,subkey);

	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, fullkey, 0, KEY_READ, &key) == ERROR_SUCCESS)
	{
		len = maxlen;
		type = REG_SZ;
		retval = RegQueryValueEx(key, label, NULL, &type,
			(unsigned char *)value, &len);

		RegCloseKey(key);

		if (retval == ERROR_SUCCESS)
			return 0;
	}

	return -1;
}


/*
 * Obtain a string value from HKEY_CURRENT_USER.
 */

int  CVRegUserGet(LPCTSTR  subkey, LPCTSTR label, LPTSTR value, int maxlen)
{
	HKEY key;
	DWORD len, type;
	LONG retval;
	TCHAR fullkey[512];	// buffer where a subkey is appened to the keypath

#if defined(_WIN64)
	_tcscpy(fullkey, KEYPATH_x64);
#else
	_tcscpy(fullkey, KEYPATH);
#endif

	if (subkey)
		_tcscat(fullkey,subkey);

	if (RegOpenKeyEx(HKEY_CURRENT_USER, fullkey, 0,	KEY_READ, &key) == ERROR_SUCCESS)
	{
		len = maxlen;
		type = REG_SZ;
		retval = RegQueryValueEx(key, label, NULL, &type, (BYTE *) value, &len);

		RegCloseKey(key);

		if (retval == ERROR_SUCCESS)
			return 0;
	}

	return -1;
}


/*
 * Update/Create a string value in HKEY_LOCAL_MACHINE.
 */

int  CVRegMachineSet(LPCTSTR  subkey, LPCTSTR  label, LPCTSTR  value)
{
	HKEY key;
	LONG retval;
	TCHAR fullkey[512];	// buffer where a subkey is appened to the keypath

#if defined(_WIN64)
	_tcscpy(fullkey, KEYPATH_x64);
#else
	_tcscpy(fullkey, KEYPATH);
#endif

	if (subkey)
		_tcscat(fullkey,subkey);

	if (RegCreateKeyEx(HKEY_LOCAL_MACHINE, fullkey, 0,
		NULL, REG_OPTION_NON_VOLATILE,
		KEY_WRITE, NULL, &key, NULL) == ERROR_SUCCESS)
	{
		retval = RegSetValueEx(key, label, 0, REG_SZ, (BYTE *)value, (DWORD)_tcslen(value)+1);

		RegCloseKey(key);

		if (retval == ERROR_SUCCESS)
			return 0;
	}

	return -1;
}


/*
 * Update/Create a string value in HKEY_CURRENT_USER.
 */

int  CVRegUserSet(LPCTSTR   subkey, LPCTSTR  label, LPCTSTR  value)
{
	HKEY key;
	LONG retval;
	TCHAR fullkey[512];	// buffer where a subkey is appended to the keypath
	DWORD length;

#if defined(_WIN64)
	_tcscpy(fullkey, KEYPATH_x64);
#else
	_tcscpy(fullkey, KEYPATH);
#endif

	if (subkey)
		_tcscat(fullkey,subkey);

	if (RegCreateKeyEx(HKEY_CURRENT_USER, fullkey, 0,
		NULL, REG_OPTION_NON_VOLATILE,
		KEY_WRITE, NULL, &key, NULL) == ERROR_SUCCESS)
	{
		length = (DWORD) _tcslen (value);
		retval = RegSetValueEx(key, label, 0, REG_SZ, (BYTE *)value, length);

		RegCloseKey(key);

		if (retval == ERROR_SUCCESS)
			return 0;
	}

	return -1;
}


/*
 *
 *	Binary registry functions
 *
 */

int  CVRegMachineGetBin(LPCTSTR  subkey, LPCTSTR  label, void *data, int maxlen)
{
	HKEY key;
	DWORD len, type;
	LONG retval;
	TCHAR fullkey[512];	// buffer where a subkey is appended to the keypath

#if defined(_WIN64)
	_tcscpy(fullkey, KEYPATH_x64);
#else
	_tcscpy(fullkey, KEYPATH);
#endif

	if (subkey)
		_tcscat(fullkey,subkey);

	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, fullkey, 0,
		KEY_READ, &key) == ERROR_SUCCESS)
	{
		len = maxlen;
		type = REG_BINARY;
		retval = RegQueryValueEx(key, label, NULL, &type, (BYTE *)data, &len);

		RegCloseKey(key);

		if (retval == ERROR_SUCCESS)
			return 0;
	}

	return -1;
}


/*
 * Obtain a binary value from HKEY_CURRENT_USER.
 */

int  CVRegUserGetBin(LPCTSTR  subkey, LPCTSTR  label, void *data, int maxlen)
{
	HKEY key;
	DWORD len, type;
	LONG retval;
	TCHAR fullkey[512];	// buffer where a subkey is appended to the keypath

#if defined(_WIN64)
	_tcscpy(fullkey, KEYPATH_x64);
#else
	_tcscpy(fullkey, KEYPATH);
#endif

	if (subkey)
		_tcscat(fullkey,subkey);


	if (RegOpenKeyEx(HKEY_CURRENT_USER, fullkey, 0,
		KEY_READ, &key) == ERROR_SUCCESS)
	{
		len = maxlen;
		type = REG_BINARY;
		retval = RegQueryValueEx(key, label, NULL, &type,
			(unsigned char *)data, &len);

		RegCloseKey(key);

		if (retval == ERROR_SUCCESS)
			return 0;
	}

	return -1;
}


/*
 * Update/Create a binary value in HKEY_LOCAL_MACHINE.
 */

int  CVRegMachineSetBin(LPCTSTR  subkey, LPCTSTR  label, void *data, int len)
{
	HKEY key;
	LONG retval;
	TCHAR fullkey[512];	// buffer where a subkey is appened to the keypath

#if defined(_WIN64)
	_tcscpy(fullkey, KEYPATH_x64);
#else
	_tcscpy(fullkey, KEYPATH);
#endif

	if (subkey)
		_tcscat(fullkey,subkey);

	if (RegCreateKeyEx(HKEY_LOCAL_MACHINE, fullkey, 0,
		NULL, REG_OPTION_NON_VOLATILE,
		KEY_WRITE, NULL, &key, NULL) == ERROR_SUCCESS)
	{
		retval = RegSetValueEx(key, label, 0, REG_BINARY,
			(unsigned char *)data, len);

		RegCloseKey(key);

		if (retval == ERROR_SUCCESS)
			return 0;
	}

	return -1;
}


/*
 * Update/Create a binary value in HKEY_CURRENT_USER.
 */

int  CVRegUserSetBin(LPCTSTR  subkey, LPCTSTR  label, void *data, int len)
{
	HKEY key;
	LONG retval;
	TCHAR fullkey[512];	// buffer where a subkey is appended to the keypath

#if defined(_WIN64)
	_tcscpy(fullkey, KEYPATH_x64);
#else
	_tcscpy(fullkey, KEYPATH);
#endif

	if (subkey)
		_tcscat(fullkey,subkey);

	if (RegCreateKeyEx(HKEY_CURRENT_USER, fullkey, 0,
		NULL, REG_OPTION_NON_VOLATILE,
		KEY_WRITE, NULL, &key, NULL) == ERROR_SUCCESS)
	{
		retval = RegSetValueEx(key, label, 0, REG_BINARY,
			(unsigned char *)data, len);

		RegCloseKey(key);

		if (retval == ERROR_SUCCESS)
			return 0;
	}

	return -1;
}


//**********************************************************
// Derived calls.
//**********************************************************


// short

int  CVRegMachineSetShort(LPCTSTR  subkey, LPCTSTR  label, short value)
{
	TCHAR str[40];

	_stprintf(str, _T("%d"), (long)value);

	if (CVRegMachineSet(subkey, label, str))
		return -1;

	return 0;
}

int  CVRegMachineGetShort(LPCTSTR  subkey, LPCTSTR  label, short *value)
{
	TCHAR str[40];
	long temp = 0;

	if (CVRegMachineGet(subkey, label, str, 39))
		return -1;

	_stscanf(str, _T("%d"), &temp);
	*value = (short)temp;

	return 0;
}

int  CVRegUserSetShort(LPCTSTR  subkey, LPCTSTR  label, short value)
{
	TCHAR str[40];

	_stprintf(str, _T("%d"), (long)value);

	if (CVRegUserSet(subkey, label, str))
		return -1;

	return 0;
}

int  CVRegUserGetShort(LPCTSTR  subkey, LPCTSTR  label, short *value)
{
	TCHAR str[40];
	long temp = 0;

	if (CVRegUserGet(subkey, label, str, 39))
		return -1;

	_stscanf(str, _T("%d"), &temp);
	*value = (short)temp;

	return 0;
}


// long

int  CVRegMachineSetLong(LPCTSTR  subkey, LPCTSTR  label, long value)
{
	TCHAR str[40];

	_stprintf(str, _T("%d"), value);

	if (CVRegMachineSet(subkey, label, str))
		return -1;

	return 0;
}

int  CVRegMachineGetLong(LPCTSTR  subkey, LPCTSTR  label, long *value)
{
	TCHAR str[40];

	if (CVRegMachineGet(subkey, label, str, 39))
		return -1;

	*value = 0;
	_stscanf(str, _T("%d"), value);

	return 0;
}

int  CVRegMachineSetDouble(LPCTSTR  subkey, LPCTSTR  label, double value)
{
	TCHAR str[40];

	_stprintf(str, _T("%lf"), value);

	if (CVRegMachineSet(subkey, label, str))
		return -1;

	return 0;
}

int  CVRegMachineGetDouble(LPCTSTR  subkey, LPCTSTR label, double *value)
{
	TCHAR str[40];

	if (CVRegMachineGet(subkey, label, str, 39))
		return -1;

	*value = 0;
	_stscanf(str, _T("%lf"), value);

	return 0;
}

int  CVRegUserSetLong(LPCTSTR  subkey, LPCTSTR  label, long value)
{
	TCHAR str[40];

	_stprintf(str, _T("%d"), value);

	if (CVRegUserSet(subkey, label, str))
		return -1;

	return 0;
}

int  CVRegUserGetLong(LPCTSTR  subkey, LPCTSTR  label, long *value)
{
	TCHAR str[40];

	if (CVRegUserGet(subkey, label, str, 39))
		return -1;

	*value = 0;
	_stscanf(str, _T("%d"), value);

	return 0;
}

int  CVRegUserSetDouble(LPCTSTR  subkey, LPCTSTR  label, double value)
{
	TCHAR str[40];

	_stprintf(str, _T("%lf"), value);

	if (CVRegUserSet(subkey, label, str))
		return -1;

	return 0;
}

int  CVRegUserGetDouble(LPCTSTR  subkey, LPCTSTR label, double *value)
{
	TCHAR str[40];

	if (CVRegUserGet(subkey, label, str, 39))
		return -1;

	*value = 0;
	_stscanf(str, _T("%lf"), value);

	return 0;
}


// POINT

int  CVRegMachineSetPoint(LPCTSTR  subkey, LPCTSTR  label, POINT *value)
{
	TCHAR str[40];

	_stprintf(str, _T("%d %d"), value->x, value->y);

	if (CVRegMachineSet(subkey, label, str))
		return -1;

	return 0;
}

int  CVRegMachineGetPoint(LPCTSTR  subkey, LPCTSTR  label, POINT *value)
{
	TCHAR str[40];

	if (CVRegMachineGet(subkey, label, str, 39))
		return -1;

	value->x = 0;
	value->y = 0;
	_stscanf(str, _T("%d %d"), &value->x, &value->y);

	return 0;
}

int  CVRegUserSetPoint(LPCTSTR  subkey, LPCTSTR  label, POINT *value)
{
	TCHAR str[40];

	_stprintf(str, _T("%d %d"), value->x, value->y);

	if (CVRegUserSet(subkey, label, str))
		return -1;

	return 0;
}

int  CVRegUserGetPoint(LPCTSTR  subkey, LPCTSTR  label, POINT *value)
{
	TCHAR str[40];

	if (CVRegUserGet(subkey, label, str, 39))
		return -1;

	value->x = 0;
	value->y = 0;
	_stscanf(str, _T("%d %d"), &value->x, &value->y);

	return 0;
}


// RECT

int  CVRegMachineSetRect(LPCTSTR  subkey, LPCTSTR  label, RECT *value)
{
	TCHAR str[40];

	_stprintf(str, _T("%d %d %d %d"),
		value->left, value->top, value->right, value->bottom);

	if (CVRegMachineSet(subkey, label, str))
		return -1;

	return 0;
}

int  CVRegMachineGetRect(LPCTSTR  subkey, LPCTSTR  label, RECT *value)
{
	TCHAR str[40];

	if (CVRegMachineGet(subkey, label, str, 39))
		return -1;

	value->top = 0;
	value->left = 0;
	value->right = 10;
	value->bottom = 10;
	_stscanf(str, _T("%d %d %d %d"),
		&value->left, &value->top, &value->right, &value->bottom);

	return 0;
}

int  CVRegUserSetRect(LPCTSTR  subkey, LPCTSTR  label, RECT *value)
{
	TCHAR str[40];

	_stprintf(str, _T("%d %d %d %d"),
		value->left, value->top, value->right, value->bottom);

	if (CVRegUserSet(subkey, label, str))
		return -1;

	return 0;
}

int  CVRegUserGetRect(LPCTSTR  subkey, LPCTSTR  label, RECT *value)
{
	TCHAR str[40];

	if (CVRegUserGet(subkey, label, str, 39))
		return -1;

	value->top = 0;
	value->left = 0;
	value->right = 10;
	value->bottom = 10;
	_stscanf(str, _T("%d %d %d %d"),
		&value->left, &value->top, &value->right, &value->bottom);

	return 0;
}


BOOL
SetRegistryPermissions(HKEY hKey, LPCTSTR subKeyName, LPTSTR trusteeName)
{
// This function grants full control rights to the specified trustee (e.g. user
// or group) for the specified subkey and its children.
// This function will only succeed if the user has appropriate privileges, so
// is intended for calling by an administrative user.
	BOOL status = FALSE;
	HKEY hOpenKey;
	DWORD OSVer;

	// If execute-time OS is pre-Windows 2000 then return,
	// otherwise get crash in LocalFree().
	if (OSVersionNum(&OSVer) && OSVer < 0x500)
		return TRUE;

	if (RegOpenKeyEx(hKey, subKeyName, 0, KEY_ALL_ACCESS, &hOpenKey) == ERROR_SUCCESS)
	{
		ACL *pOldDacl, *pNewDacl;
		PSECURITY_DESCRIPTOR pSecDesc;
		// Get the current security info for the key.
		// Don't use RegGetKeySecurity() unless you like pain ;-)
		if (GetSecurityInfo(hOpenKey, SE_REGISTRY_KEY,
		                    DACL_SECURITY_INFORMATION, NULL, NULL,
		                    &pOldDacl, NULL, &pSecDesc) == ERROR_SUCCESS)
		{
			// Create an EXPLICIT_ACCESS structure with the security info we want
			// to add to the key.
			EXPLICIT_ACCESS explicitAccess;
			BuildExplicitAccessWithName(&explicitAccess, trusteeName,
			                            GENERIC_ALL, GRANT_ACCESS,
			                            SUB_CONTAINERS_AND_OBJECTS_INHERIT);

			// Add the security info in the EXPLICT_ACCESS structure to the DACL
			// obtained from the key.
			if (SetEntriesInAcl(1, &explicitAccess, pOldDacl, &pNewDacl) == ERROR_SUCCESS)
			{
				// Apply the new DACL to the key.
				// For Windows 2000 and later, this will be propogated to any child objects
				// On Windows NT it will just be applied to the specified key, but for our
				// purposes that doesn't matter as on NT Everyone has full access by default.
				if (SetSecurityInfo(hOpenKey, SE_REGISTRY_KEY,
				                    DACL_SECURITY_INFORMATION,
				                    NULL, NULL, pNewDacl, NULL) == ERROR_SUCCESS)
				{
					status = TRUE;
				}

				LocalFree(pNewDacl);
			}

			LocalFree(pSecDesc);
		}

		RegCloseKey(hOpenKey);
	}

	return status;
}


BOOL
CVRegSetPermissions()
{
// In Windows XP, the registry settings under HKEY_LOCAL_MACHINE will not be
// modifyable by non-adminstrative users unless this function is called.
// This function grants the 'Everyone' group full control over the CytoVision
// subtree in HKEY_LOCAL_MACHINE. This will only work if KEYPATH exists in
// HKEY_LOCAL_MACHINE!
// Note that 'Everyone', rather than 'CytoUsers' is used due to the possible
// absence of the 'CytoUsers' group is some situations and its ambiguity in a
// multiple domain situation.
#if defined(_WIN64)
	return SetRegistryPermissions(HKEY_LOCAL_MACHINE, KEYPATH_x64, EVERYONE_GROUPNAME);
#else
	return SetRegistryPermissions(HKEY_LOCAL_MACHINE, KEYPATH, EVERYONE_GROUPNAME);
#endif
}


//**********************************************************
// Environment variable functions
//**********************************************************
BOOL  CVGetCurrentUser(LPTSTR   name, int len)
{
	if (!ExpandEnvironmentStrings(_T("%USERNAME%"), name, len))
		return FALSE;

	return TRUE;
}

BOOL CVGetCVDirectory(LPTSTR name, int len)
{
	// All CV executable code is assumed to be located in the same directory, which could be anywhere.
	return GetExecDir(name, len);
}

BOOL  CVGetCVUserDirectory(LPTSTR   name, int len)
{
	WIN32_FIND_DATA finddata;
	TCHAR userprofile[80];
	HANDLE hDir;

	// User profile path for application data
	if (!ExpandEnvironmentStrings(_T("%USERPROFILE%"), userprofile, sizeof(userprofile)))
		return FALSE;

	// Have we room to cat
	if ((int)(_tcslen(userprofile) + _tcslen(PROFILEPATH) + _tcslen(COMPANY) + _tcslen(APP) + 6 )> len)
		return FALSE;

	// e.g: D\\WINNT\\Profiles\\wh\\Application Data\\Applied Imaging\\Cytovision
	_stprintf(name, _T("%s\\%s\\%s\\%s"), userprofile, PROFILEPATH, COMPANY, APP);

	// Check that e.g. C:\users\wh\application data\applied imaging\cytovision exists
	// If not create one and any required subdirectories
	if ((hDir = FindFirstFile(name, &finddata)) == INVALID_HANDLE_VALUE)
	{
		TCHAR buff[200];

		_stprintf(buff, _T("%s\\%s\\%s"), userprofile, PROFILEPATH, COMPANY);
		// Ensure that parent directory exists - can only create one directory level at a time.
		if(!CreateDirectory(buff, NULL) && GetLastError() != ERROR_ALREADY_EXISTS)
		{
			MessageBox(NULL, _T("Can't create user data directory"), _T(""), MB_OK );
			return FALSE;
		}
		// Now create the cytovision directory.
		_stprintf(buff, _T("%s\\%s\\%s\\%s"), userprofile, PROFILEPATH, COMPANY, APP);
		if(!CreateDirectory(buff, NULL))
		{
			MessageBox(NULL, _T("Can't create user data directory"), _T(""), MB_OK );
			return FALSE;
		}
		// Create Macros subdirectory.
		_stprintf(buff, _T("%s\\%s\\%s\\%s\\Macros"), userprofile, PROFILEPATH, COMPANY, APP);
		if(!CreateDirectory(buff, NULL))
		{
			MessageBox(NULL, _T("Can't create user Macros directory"), _T(""), MB_OK );
			return FALSE;
		}
		// Create Defaults subdirectory.
		_stprintf(buff, _T("%s\\%s\\%s\\%s\\Defaults"), userprofile, PROFILEPATH, COMPANY, APP);
		if(!CreateDirectory(buff, NULL))
		{
			MessageBox(NULL, _T("Can't create user Defaults directory"), _T(""), MB_OK );
			return FALSE;
		}
	}
	else
		FindClose(hDir);

	return TRUE;
}

BOOL  CVGetCVAllUsersDirectory(LPTSTR   name, int len)
{
#ifndef _WIN64
#ifdef CAMMANAGED
	return 0;
#else
	WIN32_FIND_DATA finddata;
	TCHAR userprofile[MAX_PATH];
	HANDLE hDir;
	CString uprofile, profile;
	DWORD ud = MAX_PATH;
	TCHAR buff[200];

	if (!GetAllUsersProfileDirectory(userprofile, &ud))
		return FALSE;

	// Have we room to cat
	if ((int)(_tcslen(PROFILEPATH) + _tcslen(COMPANY) + _tcslen(APP) + 6 )> len)
		return FALSE;

	// e.g: D\\WINNT\\Profiles\\all users\\Application Data\\Applied Imaging\\Cytovision
	_stprintf(name, _T("%s\\%s\\%s\\%s"), userprofile, PROFILEPATH, COMPANY, APP);

	// Check that e.g. C:\users\all users\application data\applied imaging\cytovision exists
	// If not create one and any required subdirectories
	if ((hDir = FindFirstFile(name, &finddata)) == INVALID_HANDLE_VALUE)
	{
		TCHAR buff[200];

		_stprintf(buff, _T("%s\\%s\\%s"), userprofile, PROFILEPATH, COMPANY);
		// Ensure that parent directory exists - can only create one directory level at a time.
		if(!CreateDirectory(buff, NULL) && GetLastError() != ERROR_ALREADY_EXISTS)
		{
			MessageBox(NULL, _T("Can't create all users directory"), _T(""), MB_OK );
			return FALSE;
		}
		// Now create the cytovision directory.
		_stprintf(buff, _T("%s\\%s\\%s\\%s"), userprofile, PROFILEPATH, COMPANY, APP);
		if(!CreateDirectory(buff, NULL))
		{
			MessageBox(NULL, _T("Can't create all users directory"), _T(""), MB_OK );
			return FALSE;
		}
	}
	else
		FindClose(hDir);


	// e.g: D\\WINNT\\Profiles\\all users\\Application Data\\Applied Imaging\\Cytovision\\Defaults
	_stprintf(buff, _T("%s\\%s\\%s\\%s\\Defaults"), userprofile, PROFILEPATH, COMPANY, APP);

	// Check that e.g. C:\users\all users\application data\applied imaging\cytovision exists
	// If not create one and any required subdirectories
	if ((hDir = FindFirstFile(buff, &finddata)) == INVALID_HANDLE_VALUE)
	{
		if(!CreateDirectory(buff, NULL))
		{
			MessageBox(NULL, _T("Can't create all users directory"), _T(""), MB_OK );
			return FALSE;
		}
	}
	else
		FindClose(hDir);

	_stprintf(name, _T("%s\\%s\\%s\\%s"), userprofile, PROFILEPATH, COMPANY, APP);
	CreateDirectory(name, NULL);

	return TRUE;
#endif
#else
	return FALSE;
#endif
}


//BOOL  OldCVGetCVAllUsersDirectory(LPTSTR   name, int len)
//{
//#ifdef CAMMANAGED
//	return 0;
//#else
//	WIN32_FIND_DATA finddata;
//	TCHAR userprofile[MAX_PATH];
//	HANDLE hDir;
//	CString uprofile, profile;
//	DWORD ud = MAX_PATH;
//
//	if (!GetAllUsersProfileDirectory(userprofile, &ud))
//		return FALSE;
//
//	// Have we room to cat
//	if ((int)(_tcslen(PROFILEPATH) + _tcslen(COMPANY) + _tcslen(APP) + 6 )> len)
//		return FALSE;
//
//	// e.g: D\\WINNT\\Profiles\\all users\\Application Data\\Applied Imaging\\Cytovision
//	_stprintf(name, _T("%s\\%s\\%s\\%s"), userprofile, PROFILEPATH, COMPANY, APP);
//
//	// Check that e.g. C:\users\all users\application data\applied imaging\cytovision exists
//	// If not create one and any required subdirectories
//	if ((hDir = FindFirstFile(name, &finddata)) == INVALID_HANDLE_VALUE)
//	{
//		TCHAR buff[200];
//
//		_stprintf(buff, _T("%s\\%s\\%s"), userprofile, PROFILEPATH, COMPANY);
//		// Ensure that parent directory exists - can only create one directory level at a time.
//		if(!CreateDirectory(buff, NULL) && GetLastError() != ERROR_ALREADY_EXISTS)
//		{
//			MessageBox(NULL, _T("Can't create all users directory"), _T(""), MB_OK );
//			return FALSE;
//		}
//		// Now create the cytovision directory.
//		_stprintf(buff, _T("%s\\%s\\%s\\%s"), userprofile, PROFILEPATH, COMPANY, APP);
//		if(!CreateDirectory(buff, NULL))
//		{
//			MessageBox(NULL, _T("Can't create all users directory"), _T(""), MB_OK );
//			return FALSE;
//		}
//		// Create Defaults subdirectory.
//		_stprintf(buff, _T("%s\\%s\\%s\\%s\\Defaults"), userprofile, PROFILEPATH, COMPANY, APP);
//		if(!CreateDirectory(buff, NULL))
//		{
//			MessageBox(NULL, _T("Can't create all users Defaults directory"), _T(""), MB_OK );
//			return FALSE;
//		}
//	}
//	else
//		FindClose(hDir);
//
//	_stprintf(name, _T("%s\\%s\\%s\\%s"), userprofile, PROFILEPATH, COMPANY, APP);
//	CreateDirectory(name, NULL);
//
//	return TRUE;
//#endif
//}



BOOL  CVGetCVAllUsersTempDirectory(LPTSTR   name, int len)
{
#ifdef _WIN64
	return FALSE;
#else
#ifdef CAMMANAGED
	return 0;
#else
	TCHAR userprofile[MAX_PATH];
	DWORD ud = MAX_PATH;

	if (!GetAllUsersProfileDirectory(userprofile, &ud))
		return FALSE;

	// Have we room to cat
	if ((int)(_tcslen(PROFILEPATH) + _tcslen(COMPANY) + _tcslen(APP) + 6 )> len)
		return FALSE;
	
	// e.g: D\\WINNT\\Profiles\\wh\\Application Data\\Applied Imaging\\Cytovision\\Temp
	_stprintf(name, _T("%s\\%s\\%s\\%s\\Temp"), userprofile, PROFILEPATH, COMPANY, APP);
	CreateDirectory(name, NULL);

	return TRUE;
#endif
#endif
}

BOOL  CVGetScanClassifierDirectory(LPTSTR  Dir)
{
	CAII_Shared AII;

    _stprintf(Dir, _T("%s\\Metaphase"), (LPCTSTR)AII.ClassifiersPath());

    return TRUE;
}

BOOL CVGetAriolConnectionStrings(LPCTSTR sServerName, LPTSTR connect, int len )
{
	// As this is spot, look in the CV registry for the instance name, Default will be AICV
	CString instanceName = _T("\\AICV");
	TCHAR iName[MAX_PATH];
	if (CVRegMachineGet(_T("\\Db"), _T("InstanceName"), iName, sizeof(iName)) == 0)
	{
		instanceName = "\\" + CString(iName);
	}

	CString connectString = sADOConnString  + sServerName + instanceName;
	if (CVRegMachineGet(_T("\\Db"), _T("SpotConnectionStringOverride"), iName, sizeof(iName)) == 0)
	{
		connectString = iName;
		connectString.Replace("$SERVERNAME$", sServerName);
	}
	_tcsncpy(connect, (LPCTSTR)connectString, len);
	return TRUE;
}

BOOL CVGetCVUserLocalDirectory(LPTSTR path)
{
	if(SHGetFolderPath(NULL, CSIDL_LOCAL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, path)!= S_OK) 
	{
		OutputDebugString(_T("CreateAppDataLocalFolder: SHGetFolderPath could create path\r\n"));
		return FALSE;
	}
	PathAppend(path, _T("Applied Imaging"));
	CreateDirectory(path, NULL);
	PathAppend(path, _T("CytoVision"));
	if(!CreateDirectory(path, NULL) && GetLastError() != ERROR_ALREADY_EXISTS)
	{
		OutputDebugString(_T("CreateAppDataLocalFolder couldn't create path\r\n"));
		return FALSE;
	}
	return TRUE;
} 


