/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// A folder browser 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GetFolder.h"

static CString strTmpPath;

int CALLBACK BrowseCallbackProc(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
{
    TCHAR szDir[MAX_PATH];
    switch(uMsg)
    {
    case BFFM_INITIALIZED:
        if (lpData)
        {
            strcpy(szDir, strTmpPath.GetBuffer(strTmpPath.GetLength()));
            SendMessage(hwnd,BFFM_SETSELECTION,TRUE,(LPARAM)szDir);
        }
        break;

    case BFFM_SELCHANGED: 
        {
            if (SHGetPathFromIDList((LPITEMIDLIST) lParam ,szDir)){
                SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,(LPARAM)szDir);
            }
            break;
        }
    default:
        break;
    }

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Do the folder browse
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL GetFolder(CString* strSelectedFolder,
			   LPCTSTR lpszTitle,
			   const HWND hwndOwner, 
			   LPCTSTR strRootFolder, 
			   LPCTSTR strStartFolder)
{
	TCHAR pszDisplayName[MAX_PATH];
	LPITEMIDLIST lpID;
	BROWSEINFOA bi;
	
	bi.hwndOwner = hwndOwner;
	if (strRootFolder == NULL)
    {
		bi.pidlRoot = NULL;
	}
    else
    {
	   LPITEMIDLIST  pIdl = NULL;
	   IShellFolder* pDesktopFolder;
	   TCHAR         szPath[MAX_PATH];
	   OLECHAR       olePath[MAX_PATH];
	   ULONG         chEaten;
	   ULONG         dwAttributes;

	   _tcscpy(szPath, (LPCTSTR)strRootFolder);
	   if (SUCCEEDED(SHGetDesktopFolder(&pDesktopFolder)))
	   {
		   MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, szPath, -1, olePath, MAX_PATH);
		   pDesktopFolder->ParseDisplayName(NULL, NULL, olePath, &chEaten, &pIdl, &dwAttributes);
		   pDesktopFolder->Release();
	   }
	   bi.pidlRoot = pIdl;
	}

	bi.pszDisplayName = pszDisplayName;
	bi.lpszTitle = lpszTitle;
	bi.ulFlags = BIF_USENEWUI|BIF_RETURNONLYFSDIRS|BIF_STATUSTEXT;
	bi.lpfn = BrowseCallbackProc;

	if (!strStartFolder)
    {
		bi.lParam = FALSE;
	}
    else
    {
		strTmpPath.Format(_T("%s"), strStartFolder);
		bi.lParam = TRUE;
	}

	// must call this if using BIF_USENEWUI
	::OleInitialize(NULL);

	bi.iImage = NULL;
	lpID = SHBrowseForFolder(&bi);
	
    if (lpID)
    {
		BOOL b = SHGetPathFromIDList(lpID, pszDisplayName);
		if (b)
        {
			strSelectedFolder->Format(_T("%s"),pszDisplayName);
			return TRUE;
		}
	}
    else
    {
		strSelectedFolder->Empty();
	}

	::OleUninitialize();

	return FALSE;
}
