#include "stdafx.h"
#include "OSVersion.h"

BOOL OSVersionNum(DWORD *Version)
{
   OSVERSIONINFOEX osvi;

   // Try calling GetVersionEx using the OSVERSIONINFOEX structure.
   // If that fails, try using the OSVERSIONINFO structure.

   ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
   osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

   if (!(GetVersionEx((OSVERSIONINFO *) &osvi)))
   {
      osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
      if (!GetVersionEx((OSVERSIONINFO *) &osvi)) 
         return FALSE;
   }

   *Version = osvi.dwMajorVersion * 0x100 + osvi.dwMinorVersion;

   return TRUE;
}