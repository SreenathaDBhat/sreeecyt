// Registry.cpp : implementation file
//

#include "stdafx.h"
#include "RegHelper.h"

#include "Registry.h" // For SetRegistryPermissions()

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRegistry


/////////////////////////////////////////////////////////////////////////////
// CRegistry message handlers

// Settings.cpp : implementation file
//

#include "stdafx.h"


/////////////////////////////////////////////////////////////////////////////
// CRegistry dialog


CRegistry::CRegistry()
{
}

CRegistry::~CRegistry()
{
}


/////////////////////////////////////////////////////////////////////////////
// CRegistry message handlers
CString CRegistry::GetRegistryItem(LPCTSTR AIkey, LPCTSTR key, LPCTSTR subkey)
{
	CString curr = _T("");

	HKEY hKey;
	TCHAR szKey[255];

    _stprintf(szKey, _T("%s\\%s"), AIkey, key);
	if( RegOpenKey(HKEY_LOCAL_MACHINE, szKey, &hKey) == ERROR_SUCCESS) 
	{
		TCHAR value[255];
		long dwSize = sizeof(value);

		if (RegQueryValue(hKey, subkey, value, &dwSize) == ERROR_SUCCESS)

		if (dwSize > 0)
			curr = value;

		RegCloseKey(hKey);
	}

	return curr;
}

void CRegistry::SetRegistryItem(LPCTSTR AIkey, LPCTSTR key, LPCTSTR subkey, LPCTSTR value)
{
	TCHAR szKey[255];
	HKEY hKey;
	DWORD dwSize;

	// Concatenate key and sub-key
	if (subkey)
		_stprintf(szKey, _T("%s\\%s\\%s"), AIkey, key, subkey);
	else
		_stprintf(szKey, _T("%s\\%s"),AIkey, key);

	// Create the key
	if (RegCreateKeyEx(HKEY_LOCAL_MACHINE, szKey, 0, 0, 0, KEY_ALL_ACCESS, NULL, &hKey, &dwSize) == ERROR_SUCCESS) {
		// If that went OK, set the value
		dwSize = (DWORD)_tcslen(value) * sizeof (TCHAR);

        RegSetValueEx(
			hKey,			// handle to key
			NULL,			// value name (use default (unnamed) value)
			0,				// reserved - must be zero
			REG_SZ,			// value type - using string
			(BYTE *)value,	// value data
			dwSize			// size of value data
		);

		// Close the key to release the handle
		RegCloseKey(hKey);
	}
}


CString CRegistry::GetRegItem(LPCTSTR key, LPCTSTR subkey)
{
	return GetRegistryItem(_T("Software\\Applied Imaging\\MDS2"), key, subkey);
}

CString CRegistry::GetCVRegItem(LPCTSTR key, LPCTSTR subkey)
{
	return GetRegistryItem(_T("Software\\Applied Imaging\\Cytovision"), key, subkey);
}

////////////////////////////////////////////////////////////////////////////////
//
//	GetRegItem()
//	Get a numeric value from registry or use default if registry key not found
//
int CRegistry::GetRegItem(LPCTSTR key, LPCTSTR subkey, int default_value)
{
	int value;
	CString reg_value;
	reg_value = CRegistry::GetRegItem(key, subkey);

	if (reg_value.IsEmpty() == false)
		value = _ttoi(reg_value);
	else
		value = default_value;

	return value;
}

void CRegistry::SetRegItem(LPCTSTR key, LPCTSTR subkey, LPCTSTR value)
{
	SetRegistryItem(_T("Software\\Applied Imaging\\MDS2"), key, subkey, value);
}

void CRegistry::SetCVRegItem(LPCTSTR key, LPCTSTR subkey, LPCTSTR value)
{
	SetRegistryItem(_T("Software\\Applied Imaging\\Cytovision"), key, subkey, value);
}


BOOL CRegistry::SetPermissions()
{
// Give all users full control over the "Applied Imaging" part of
// the HKEY_LOCAL_MACHINE section of the registry. By default, on Windows XP
// non-administrative users cannot modify this part of the registry.
// Note permissions are granted at the "Applied Imaging" level rather than
// the "MDS2" level as I think we may be creating some entries at that higher level.
	return SetRegistryPermissions(HKEY_LOCAL_MACHINE, _T("Software\\Applied Imaging"), _T("Everyone"));
}
