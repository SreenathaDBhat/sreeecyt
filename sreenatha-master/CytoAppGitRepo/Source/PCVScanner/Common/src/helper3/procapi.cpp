/////////////////////////////////////////////////////////////////////////////////
// Code to detect & manage processes requires PSAPI.DLL on system
//
// Written By Karl Ratcliff 17092001
// 
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#undef WINVER
#define WINVER 0x0501
#undef _WIN32_WINNT
#define _WIN32_WINNT 0x0501

#include "procapi.h"
#include <Psapi.h>

/////////////////////////////////////////////////////////////////////////////////
// Constructor/Destructor
/////////////////////////////////////////////////////////////////////////////////

CProcessMonitor::CProcessMonitor()
{
    // Load library and get the procedures explicitly. We do
    // this so that we don't have to worry about modules using
    // this code failing to load under Windows 95, because
    // it can't resolve references to the PSAPI.DLL.
    m_hInstLib = LoadLibraryA("PSAPI.DLL");
}

CProcessMonitor::~CProcessMonitor()
{
	FreeLibrary(m_hInstLib);
}

/////////////////////////////////////////////////////////////////////////////////
// Determine whether a process is running, specified by ProcessName
// Returns the Process ID if it is, zero otherwise
// Ripped off MSDN code
/////////////////////////////////////////////////////////////////////////////////
DWORD CProcessMonitor::IsProcessRunning(LPCTSTR ProcessName)
{
    LPDWORD        lpdwPIDs;
    DWORD          dwSize, dwSize2, dwIndex;
    HMODULE        hMod;
    HANDLE         hProcess;
    TCHAR          szFileName[MAX_PATH];
    CString        csProcName;
    CString        csProcFoundName;
    DWORD          ProcID = 0;          // Returned process ID

    // PSAPI Function Pointers.
    BOOL    (WINAPI *lpfEnumProcesses)(DWORD *, DWORD cb, DWORD *);
    BOOL    (WINAPI *lpfEnumProcessModules)(HANDLE, HMODULE *, DWORD, LPDWORD);
    DWORD   (WINAPI *lpfGetModuleFileNameEx)(HANDLE, HMODULE, LPTSTR, DWORD);

    if (m_hInstLib == NULL)
        return FALSE;

	csProcName.Format(_T("%s"), ProcessName);
    csProcName.MakeUpper();    
    
    // Get procedure addresses.
    lpfEnumProcesses = (BOOL(WINAPI *)(DWORD *,DWORD,DWORD*)) 
                       GetProcAddress(m_hInstLib, "EnumProcesses" );
    lpfEnumProcessModules = (BOOL(WINAPI *)(HANDLE, HMODULE *, DWORD, LPDWORD)) 
                            GetProcAddress(m_hInstLib, "EnumProcessModules");
    lpfGetModuleFileNameEx = (DWORD (WINAPI *)(HANDLE, HMODULE, LPTSTR, DWORD)) 
                             GetProcAddress(m_hInstLib, "GetModuleFileNameExA");

    if (lpfEnumProcesses == NULL || lpfEnumProcessModules == NULL || lpfGetModuleFileNameEx == NULL)
        return FALSE;
    
    // Call the PSAPI function EnumProcesses to get all of the
    // ProcID's currently in the system.
    // NOTE: In the documentation, the third parameter of
    // EnumProcesses is named cbNeeded, which implies that you
    // can call the function once to find out how much space to
    // allocate for a buffer and again to fill the buffer.
    // This is not the case. The cbNeeded parameter returns
    // the number of PIDs returned, so if your buffer size is
    // zero cbNeeded returns zero.
    // NOTE: The "HeapAlloc" loop here ensures that we
    // actually allocate a buffer large enough for all the
    // PIDs in the system.
    dwSize2 = 1024 * sizeof(DWORD);
    lpdwPIDs = NULL;
    do
    {
        if(lpdwPIDs)
        {
            HeapFree(GetProcessHeap(), 0, lpdwPIDs);
            dwSize2 *= 2;
        }

        lpdwPIDs = (DWORD *) HeapAlloc(GetProcessHeap(), 0, dwSize2);
        if (lpdwPIDs == NULL)
            return FALSE;

        if (!lpfEnumProcesses(lpdwPIDs, dwSize2, &dwSize))
        {
            HeapFree(GetProcessHeap(), 0, lpdwPIDs);
            return FALSE;
        }

    } while (dwSize == dwSize2) ;
    
    // How many ProcID's did we get?
    dwSize /= sizeof(DWORD);
    
    // Loop through each ProcID.
    for(dwIndex = 0 ; dwIndex < dwSize ; dwIndex++)
    {
        szFileName[0] = 0 ;
        // Open the process (if we can... security does not
        // permit every process in the system).
        hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, lpdwPIDs[dwIndex]);
        if (hProcess != NULL)
        {
            // Here we call EnumProcessModules to get only the
            // first module in the process this is important,
            // because this will be the .EXE module for which we
            // will retrieve the full path name in a second.
            if(lpfEnumProcessModules(hProcess, &hMod, sizeof( hMod ), &dwSize2))
            {
                // Get Full pathname:
                if(!lpfGetModuleFileNameEx(hProcess, hMod, szFileName, sizeof(szFileName)))
                {
                    szFileName[0] = 0 ;
                }
            }
            CloseHandle(hProcess);
        }

        csProcFoundName.Format(_T("%s"), szFileName);
        csProcFoundName.MakeUpper();

        if (csProcFoundName.Find(csProcName) != -1)
        {
            ProcID = lpdwPIDs[dwIndex];         // Return the right process ID
            break;
        }

    }
    
    HeapFree(GetProcessHeap(), 0, lpdwPIDs) ;
      
    return ProcID;
}

/////////////////////////////////////////////////////////////////////////////////
// Determine whether a process is running, specified by ProcessName
// Returns the Process ID if it is, zero otherwise
// Ripped off MSDN code
/////////////////////////////////////////////////////////////////////////////////
int CProcessMonitor::CountProcesses(LPCTSTR ProcessName)
{
    LPDWORD        lpdwPIDs;
    DWORD          dwSize, dwSize2, dwIndex;
    HMODULE        hMod;
    HANDLE         hProcess;
    TCHAR          szFileName[MAX_PATH];
    CString        csProcName;
    CString        csProcFoundName;
    DWORD          ProcID = 0;          // Returned process ID
	int			   Instances = 0;

    // PSAPI Function Pointers.
    BOOL    (WINAPI *lpfEnumProcesses)(DWORD *, DWORD cb, DWORD *);
    BOOL    (WINAPI *lpfEnumProcessModules)(HANDLE, HMODULE *, DWORD, LPDWORD);
    DWORD   (WINAPI *lpfGetModuleFileNameEx)(HANDLE, HMODULE, LPTSTR, DWORD);

    if (m_hInstLib == NULL)
        return FALSE;

	csProcName.Format(_T("%s"), ProcessName);
    csProcName.MakeUpper();    
    
    // Get procedure addresses.
    lpfEnumProcesses = (BOOL(WINAPI *)(DWORD *,DWORD,DWORD*)) 
                       GetProcAddress(m_hInstLib, "EnumProcesses" );
    lpfEnumProcessModules = (BOOL(WINAPI *)(HANDLE, HMODULE *, DWORD, LPDWORD)) 
                            GetProcAddress(m_hInstLib, "EnumProcessModules");
    lpfGetModuleFileNameEx = (DWORD (WINAPI *)(HANDLE, HMODULE, LPTSTR, DWORD)) 
                             GetProcAddress(m_hInstLib, "GetModuleFileNameExA");

    if (lpfEnumProcesses == NULL || lpfEnumProcessModules == NULL || lpfGetModuleFileNameEx == NULL)
        return FALSE;
    
    // Call the PSAPI function EnumProcesses to get all of the
    // ProcID's currently in the system.
    // NOTE: In the documentation, the third parameter of
    // EnumProcesses is named cbNeeded, which implies that you
    // can call the function once to find out how much space to
    // allocate for a buffer and again to fill the buffer.
    // This is not the case. The cbNeeded parameter returns
    // the number of PIDs returned, so if your buffer size is
    // zero cbNeeded returns zero.
    // NOTE: The "HeapAlloc" loop here ensures that we
    // actually allocate a buffer large enough for all the
    // PIDs in the system.
    dwSize2 = 1024 * sizeof(DWORD);
    lpdwPIDs = NULL;
    do
    {
        if(lpdwPIDs)
        {
            HeapFree(GetProcessHeap(), 0, lpdwPIDs);
            dwSize2 *= 2;
        }

        lpdwPIDs = (DWORD *) HeapAlloc(GetProcessHeap(), 0, dwSize2);
        if (lpdwPIDs == NULL)
            return FALSE;

        if (!lpfEnumProcesses(lpdwPIDs, dwSize2, &dwSize))
        {
            HeapFree(GetProcessHeap(), 0, lpdwPIDs);
            return FALSE;
        }

    } while (dwSize == dwSize2) ;
    
    // How many ProcID's did we get?
    dwSize /= sizeof(DWORD);
    
    // Loop through each ProcID.
    for(dwIndex = 0 ; dwIndex < dwSize ; dwIndex++)
    {
        szFileName[0] = 0 ;
        // Open the process (if we can... security does not
        // permit every process in the system).
        hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, lpdwPIDs[dwIndex]);
        if (hProcess != NULL)
        {
            // Here we call EnumProcessModules to get only the
            // first module in the process this is important,
            // because this will be the .EXE module for which we
            // will retrieve the full path name in a second.
            if(lpfEnumProcessModules(hProcess, &hMod, sizeof( hMod ), &dwSize2))
            {
                // Get Full pathname:
                if(!lpfGetModuleFileNameEx(hProcess, hMod, szFileName, sizeof(szFileName)))
                {
                    szFileName[0] = 0 ;
                }
            }
            CloseHandle(hProcess);
        }

        csProcFoundName.Format(_T("%s"), szFileName);
        csProcFoundName.MakeUpper();

        if (csProcFoundName.Find(csProcName) != -1)
        {
            Instances++;
        }

    }
    
    HeapFree(GetProcessHeap(), 0, lpdwPIDs) ;
      
    return Instances;
}

/////////////////////////////////////////////////////////////////////////////////
// Determine whether a process is running, specified by ProcessName
// Returns the Process ID if it is, zero otherwise
// Ripped off MSDN code
/////////////////////////////////////////////////////////////////////////////////
    
DWORD CProcessMonitor::KillProcess(LPCTSTR ProcessName)
{
	BOOL           Killed=FALSE;
    LPDWORD        lpdwPIDs;
    DWORD          dwSize, dwSize2, dwIndex;
    HMODULE        hMod;
    HANDLE         hProcess;
    TCHAR           szFileName[MAX_PATH];
    CString        csProcName;
    CString        csProcFoundName;
    DWORD          ProcID = 0;          // Returned process ID


    // PSAPI Function Pointers.
    BOOL    (WINAPI *lpfEnumProcesses)(DWORD *, DWORD cb, DWORD *);
    BOOL    (WINAPI *lpfEnumProcessModules)(HANDLE, HMODULE *, DWORD, LPDWORD);
    DWORD   (WINAPI *lpfGetModuleFileNameEx)(HANDLE, HMODULE, LPTSTR, DWORD);

    if (m_hInstLib == NULL)
        return Killed;

	csProcName.Format(_T("%s"), ProcessName);
    csProcName.MakeUpper();    
    
    // Get procedure addresses.
    lpfEnumProcesses = (BOOL(WINAPI *)(DWORD *,DWORD,DWORD*)) 
                       GetProcAddress(m_hInstLib, "EnumProcesses" );
    lpfEnumProcessModules = (BOOL(WINAPI *)(HANDLE, HMODULE *, DWORD, LPDWORD)) 
                            GetProcAddress(m_hInstLib, "EnumProcessModules");
    lpfGetModuleFileNameEx = (DWORD (WINAPI *)(HANDLE, HMODULE, LPTSTR, DWORD)) 
                             GetProcAddress(m_hInstLib, "GetModuleFileNameExA");

    if (lpfEnumProcesses == NULL || lpfEnumProcessModules == NULL || lpfGetModuleFileNameEx == NULL)
        return Killed;
    
    // Call the PSAPI function EnumProcesses to get all of the
    // ProcID's currently in the system.
    // NOTE: In the documentation, the third parameter of
    // EnumProcesses is named cbNeeded, which implies that you
    // can call the function once to find out how much space to
    // allocate for a buffer and again to fill the buffer.
    // This is not the case. The cbNeeded parameter returns
    // the number of PIDs returned, so if your buffer size is
    // zero cbNeeded returns zero.
    // NOTE: The "HeapAlloc" loop here ensures that we
    // actually allocate a buffer large enough for all the
    // PIDs in the system.
    dwSize2 = 1024 * sizeof(DWORD);
    lpdwPIDs = NULL;
    do
    {
        if(lpdwPIDs)
        {
            HeapFree(GetProcessHeap(), 0, lpdwPIDs);
            dwSize2 *= 2;
        }

        lpdwPIDs = (DWORD *) HeapAlloc(GetProcessHeap(), 0, dwSize2);
        if (lpdwPIDs == NULL)
            return Killed;

        if (!lpfEnumProcesses(lpdwPIDs, dwSize2, &dwSize))
        {
            HeapFree(GetProcessHeap(), 0, lpdwPIDs);
            return Killed;
        }

    } while (dwSize == dwSize2) ;
    
    // How many ProcID's did we get?
    dwSize /= sizeof(DWORD);
    
    // Loop through each ProcID.
    for(dwIndex = 0 ; dwIndex < dwSize ; dwIndex++)
    {
        szFileName[0] = 0 ;
        // Open the process (if we can... security does not
        // permit every process in the system).
        hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, lpdwPIDs[dwIndex]);
        if (hProcess != NULL)
        {
            // Here we call EnumProcessModules to get only the
            // first module in the process this is important,
            // because this will be the .EXE module for which we
            // will retrieve the full path name in a second.
            if(lpfEnumProcessModules(hProcess, &hMod, sizeof( hMod ), &dwSize2))
            {
                // Get Full pathname:
                if(!lpfGetModuleFileNameEx(hProcess, hMod, szFileName, sizeof(szFileName)))
                {
                    szFileName[0] = 0 ;
                }
            }
            CloseHandle(hProcess);
        }

        csProcFoundName.Format(_T("%s"), szFileName);
        csProcFoundName.MakeUpper();

        if (csProcFoundName.Find(csProcName) != -1)
        {
	        hProcess = OpenProcess(SYNCHRONIZE|PROCESS_TERMINATE, FALSE, lpdwPIDs[dwIndex]);
			Killed = TerminateProcess(hProcess, 0);
            break;
        }

    }
    
    HeapFree(GetProcessHeap(), 0, lpdwPIDs) ;
      
    return Killed;
}



BOOL CProcessMonitor::LaunchProcess(LPCTSTR ProcessNameAndPath, LPTSTR CmdLine, PROCESS_INFORMATION *pi)
{
    STARTUPINFO si;
    BOOL ok = FALSE;

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);

    if (CreateProcess(ProcessNameAndPath, 
                      CmdLine,				// Cmd Line
                      NULL,					// Process Attributes
                      NULL,					// Thread Attributes
                      FALSE,				// Inherit Handles
                      0,					// Creation Flags
                      NULL,					// Environment
                      NULL,					// Current Dir
                      &si,					// Startup Information
                      pi))					// Process Information
        ok = TRUE;
  
    return ok;
}

BOOL CProcessMonitor::LaunchProcess(LPCTSTR ProcessNameAndPath, LPTSTR CmdLine, DWORD CreationFlags, PROCESS_INFORMATION *pi)
{
    STARTUPINFO si;
    BOOL ok = FALSE;

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);

    if (CreateProcess(ProcessNameAndPath,	// Application Name
                      CmdLine,				// Cmd Line
                      NULL,					// Process Attributes
                      NULL,					// Thread Attributes
                      FALSE,				// Inherit Handles
                      CreationFlags,		// Creation Flags
                      NULL,					// Environment
                      NULL,					// Current Dir
                      &si,					// Startup Information
                      pi))					// Process Information
        ok = TRUE;
  
    return ok;
}


#ifndef _WIN64
// Dump process information
void CProcessMonitor::DumpProcessInfo(DWORD processID)
{

	if(processID)
	{
		HANDLE hProcess;
		PROCESS_MEMORY_COUNTERS pmc;

		hProcess = OpenProcess(  PROCESS_QUERY_INFORMATION |
										PROCESS_VM_READ,
										FALSE, processID );
		if (NULL != hProcess)
		{
			TCHAR Dbg[512];

			DWORD dwHandleCount=0;

			if( GetProcessHandleCount(hProcess, &dwHandleCount))
			{
				if ( GetProcessMemoryInfo( hProcess, &pmc, sizeof(pmc)) )
				{
					_stprintf(Dbg, _T("PFC: %d, PWSS: %d, WSS: %d, QPPPU: %d, QPPU: %d, QPNPPU: %d, QNPPU: %d, PU: %d, PPU: %d, HANDLES: %d\r\n"), 
						pmc.PageFaultCount, pmc.PeakWorkingSetSize, pmc.WorkingSetSize, pmc.QuotaPeakPagedPoolUsage, 
						pmc.QuotaPagedPoolUsage, pmc.QuotaPeakNonPagedPoolUsage,
						pmc.QuotaNonPagedPoolUsage, pmc.PagefileUsage, pmc.PeakPagefileUsage, dwHandleCount);
					OutputDebugString(Dbg);
				}
			}
			CloseHandle( hProcess );
		}
	}
}



void CProcessMonitor::PrintMemoryInfo( DWORD processID )
{
    HANDLE hProcess;
    PROCESS_MEMORY_COUNTERS pmc;
	TCHAR dbg[1024];

    // Print the process identifier.

    printf( "\nProcess ID: %u\n", processID );

    // Print information about the memory usage of the process.

    hProcess = OpenProcess(  PROCESS_QUERY_INFORMATION |
                                    PROCESS_VM_READ,
                                    FALSE, processID );
    if (NULL == hProcess)
        return;

    if (GetProcessMemoryInfo( hProcess, &pmc, sizeof(pmc)) )
    {
        _stprintf(dbg, "\tPageFaultCount: 0x%08X\n", pmc.PageFaultCount);
		OutputDebugString(dbg);

        _stprintf(dbg,  "\tPeakWorkingSetSize: 0x%08X\n", pmc.PeakWorkingSetSize );
				OutputDebugString(dbg);
        _stprintf(dbg, "\tWorkingSetSize: 0x%08X\n", pmc.WorkingSetSize );
				OutputDebugString(dbg);
        _stprintf(dbg, "\tQuotaPeakPagedPoolUsage: 0x%08X\n", 
                  pmc.QuotaPeakPagedPoolUsage );
				OutputDebugString(dbg);
        _stprintf(dbg, "\tQuotaPagedPoolUsage: 0x%08X\n", 
                  pmc.QuotaPagedPoolUsage );
				OutputDebugString(dbg);
        _stprintf(dbg,  "\tQuotaPeakNonPagedPoolUsage: 0x%08X\n", 
                  pmc.QuotaPeakNonPagedPoolUsage );
				OutputDebugString(dbg);
        _stprintf(dbg, "\tQuotaNonPagedPoolUsage: 0x%08X\n", 
                  pmc.QuotaNonPagedPoolUsage );
				OutputDebugString(dbg);
        _stprintf(dbg,  "\tPagefileUsage: 0x%08X\n", pmc.PagefileUsage ); 
				OutputDebugString(dbg);
        _stprintf(dbg, "\tPeakPagefileUsage: 0x%08X\n",  pmc.PeakPagefileUsage );
				OutputDebugString(dbg);
    }

    CloseHandle( hProcess );
}



void CProcessMonitor::DumpCurrentProcessInfo(void)
{
	DWORD processID = GetCurrentProcessId();

	PrintMemoryInfo(processID);
}


DWORD CProcessMonitor::CurrentWorkingSet(void)
{
    HANDLE hProcess;
    PROCESS_MEMORY_COUNTERS pmc;
	DWORD WS = 0;

    hProcess = OpenProcess(PROCESS_QUERY_INFORMATION |
                                    PROCESS_VM_READ,
                                    FALSE,  GetCurrentProcessId());
    if (NULL != hProcess)
	{
		if (GetProcessMemoryInfo(hProcess, &pmc, sizeof(pmc)))
			WS = pmc.WorkingSetSize;

		CloseHandle( hProcess );
	}

	return WS;
}
#endif