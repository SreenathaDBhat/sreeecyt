#include "StdAfx.h"
#include "mixturemodelthreshold.h"
#include <math.h>	// for "exp" and "pow"
#include <float.h> // for DBL_MAX


CMixtureModelThreshold::~CMixtureModelThreshold(void)
{
	
}

CMixtureModelThreshold::CMixtureModelThreshold(vector<long> histogram)
:CONST_MIN(0), CONST_MAX(255), INDEX_MIN(1), INDEX_MAX(253)
{
	this->histogram = histogram;
	cardinal = histogram.size();
	index = INDEX_MIN - 1;

	// other initialization
	mu1 = mu2 = 0.0;
	sigma2_1 = sigma2_2 = 0.0;
	mult1 = mult2 = 0.0;
	twoVariance1 = twoVariance2 = 0.0;
	max1 = max2 = 0.0;
	cardinal1 = cardinal2 = 0;
	cardinal = 0;
}

bool CMixtureModelThreshold::AddToIndex()
{
	index++;
	if (index > INDEX_MAX)
	{
		return false;
	}
	else
	{
		SetValues();
		return true;
	}
}

double CMixtureModelThreshold::CalculateMax(int index)
{
	double sum = (double)histogram[index];
	double num = 1.0;
	if (index - 1 >= 0)
	{
		sum += (double)histogram[index - 1];
		num++;
	}
	if (index + 1 < 255)
	{
		sum += (double)histogram[index + 1];
		num++;
	}
	return sum/num;
}

void CMixtureModelThreshold::SetValues()
{
	mu1 = mu2 = 0.0;
	sigma2_1 = sigma2_2 = 0.0;
	max1 = max2 = 0.0;
	cardinal1 = cardinal2 = 0;

	for (int i = CONST_MIN; i <= index; i++)
	{
		cardinal1 += histogram[i];
		mu1 += i * histogram[i];
	}

	for (int j = index + 1; j <= CONST_MAX; j++)
	{
		cardinal2 += histogram[j];
		mu2 += j * histogram[j];
	}

	if (cardinal1 == 0)
	{
		mu1 = 0.0;
		sigma2_1 = 0.0;
	}
	else
	{
		mu1 /= (double)cardinal1;
	}

	if (cardinal2 == 0)
	{
		mu2 = 0.0;
		sigma2_2 = 0.0;
	}
	else
	{
		mu2 /= (double)cardinal2;
	}

	if (mu1 != 0.0)
	{
		for (int k = CONST_MIN; k <= index; k++)
		{
			sigma2_1 += (double)histogram[k] * pow((double)k - mu1, 2);
		}

		sigma2_1 /= (double)cardinal1;
		max1 = CalculateMax((int)mu1);
		mult1 = max1;
		twoVariance1 = 2.0 * sigma2_1;
	}

	if(mu2 != 0.0)
    {
        for(int l = index + 1; l <= CONST_MAX; l++)
        {
            sigma2_2 += (double)histogram[l] * pow((double)l - mu2, 2);
        }

        sigma2_2 /= (double)cardinal2;
        max2 = CalculateMax((int)mu2);
        mult2 = max2;
        twoVariance2 = 2.0 * sigma2_2;
    }
}

double CMixtureModelThreshold::Gamma1(int i)
{
	if (sigma2_1 == 0.0)
	{
		return 0.0;
	}
	else
	{
		return mult1 * exp((-1.0) * pow((double)i - mu1, 2) / twoVariance1);
	}
}

double CMixtureModelThreshold::Gamma2(int i)
{
	if (sigma2_2 == 0.0)
	{
		return 0.0;
	}
	else
	{
		return mult2 * exp((-1.0) * pow((double)i - mu2, 2) / twoVariance2);
	}
}

double CMixtureModelThreshold::Gamma(int i)
{
	return Gamma1(i) + Gamma2(i);
}

double CMixtureModelThreshold::DifferenceGamma(int i)
{
	return Gamma1(i) - Gamma2(i);
}

void CMixtureModelThreshold::CalculateThreshold(long& theThreshold)
{
	int threshold = 0; // this is the direct threshold
	double minError = DBL_MAX;
	while(AddToIndex())
	{
		double currentError = CalculateError();
		if (currentError < minError)
		{
			minError = currentError;
			threshold = index;
		}
	}
	index = threshold; 
	SetValues();

	theThreshold = FindThreshold((int)mu1, (int)mu2); // this returns the final threshold based on mixture modeling
}

long CMixtureModelThreshold::FindThreshold(int i, int j)
{
	double minValue = DBL_MAX;
	int threshold = 0;
	for (int k = i; k < j; k++)
	{
		double currentValue = pow(this->DifferenceGamma(k), 2);
		if (minValue > currentValue)
		{
			minValue = currentValue;
			threshold = k;
		}
	}

	return threshold;
}

double CMixtureModelThreshold::CalculateError()
{
	double theError = 0.0;
	for (int i = 0; i <= 255; i++)
	{
		theError = theError + pow(Gamma(i) - histogram[i], 2);
	}

	return theError / 256.0;
}


