// MixtureModelThreshold.h
// Jan. 29, 2004 David Shen, Applied Imaging
// This module implements the Automated Thresholding based on Mixture Modeling

// This algorithm separates the histogram of an image into two classes using 
// a Gaussian model. It then calculates the image threshold as the intersection 
// of these two Gaussians. This thresholding technique has the advantage of 
// finding a threshold that is in certain cases closer to real world data. 
// The Gaussian parameters can also be used to caracterise the two regions obtained.

// Based on Mixture Modeling plug-in moduel for ImageJ by christopher.mei@sophia.inria.fr


#pragma once

#include <vector> // for STL vector
using namespace std;

#ifdef IJ_IMAGEPROC_EXPORTS
#define IJ_IMAGEPROC_API __declspec(dllexport)
#else
#define IJ_IMAGEPROC_API __declspec(dllimport)
#endif


class IJ_IMAGEPROC_API CMixtureModelThreshold
{
public:

	// constructor and destructor
	CMixtureModelThreshold(vector<long> histogram);
	~CMixtureModelThreshold(void);

	// Perform automatic thresholding based on Mixture Modeling
	void CalculateThreshold(long & theThreshold);

private:
	vector<long> histogram;	// input image's histogram
	int index;	
	double mu1;	
    double mu2;
    double sigma2_1; 
    double sigma2_2; 
    double mult1;
    double mult2;
    double twoVariance1;
    double twoVariance2;
    double max1;
    double max2;
    int cardinal1;
    int cardinal2;
    int cardinal;
    const int INDEX_MIN;
    const int INDEX_MAX;
    const int CONST_MIN;
    const int CONST_MAX;

	bool AddToIndex();
	double CalculateMax(int i);
	void SetValues();
	double Gamma1(int i);
	double Gamma2(int i);
	double Gamma(int i);
	double DifferenceGamma(int i);

	double CalculateError();
	long FindThreshold(int i, int j);
	

};
