// ijImageProcessing.cpp
// Implement Watershed segmentation based on Euclidean Distance Map (EDM)
// Implmenet Auto Thresholding


#include "StdAfx.h"
#include "ijImageProc.h"
#include "vcmemrect.h"
#include "uts.h"
#include <math.h>
#include "MixtureModelThreshold.h"

/**************************************************************
 The following are codes ported from ImageJ for Watershed
 segmentation for binary image based on Euclidean Distance Map
 
 Adapted from ImageJ v1.31 by
 Wayne Rasband (wayne@codon.nih.gov)
 ImageJ home page http://rsb.info.nih.gov/ij/

 **************************************************************/

// Watershed fate table
// Taken from ImageJ v1.31 by
// Wayne Rasband (wayne@codon.nih.gov)
// ImageJ home page http://rsb.info.nih.gov/ij/

int IJImageProc::WSFateTable[] = {
            0, 0, 4, 4, 0, 0, 4, 4, 8, 0, 
            12, 12, 8, 0, 12, 12, 0, 0, 0, 0, 
            0, 0, 0, 0, 8, 0, 12, 12, 8, 0, 
            12, 12, 1, 0, 0, 0, 0, 0, 0, 0, 
            8, 0, 15, 15, 8, 0, 15, 15, 1, 0, 
            0, 0, 0, 0, 0, 0, 9, 0, 15, 15, 
            9, 0, 15, 15, 0, 0, 0, 0, 0, 0, 
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
            0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 
            0, 0, 0, 0, 9, 0, 15, 15, 9, 0, 
            15, 15, 1, 0, 0, 0, 0, 0, 0, 0, 
            9, 0, 15, 15, 9, 0, 15, 15, 2, 2, 
            6, 6, 0, 0, 6, 6, 0, 0, 15, 15, 
            0, 0, 15, 15, 0, 0, 0, 0, 0, 0, 
            0, 0, 0, 0, 15, 15, 0, 0, 15, 15, 
            1, 3, 15, 15, 0, 0, 15, 15, 15, 15, 
            15, 15, 15, 15, 15, 15, 3, 3, 15, 15, 
            0, 0, 15, 15, 15, 15, 15, 15, 15, 15, 
            15, 15, 2, 2, 6, 6, 0, 0, 6, 6, 
            0, 0, 15, 15, 0, 0, 15, 15, 0, 0, 
            0, 0, 0, 0, 0, 0, 0, 0, 15, 15, 
            0, 0, 15, 15, 3, 3, 15, 15, 0, 0, 
            15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 
            3, 3, 15, 15, 0, 0, 15, 15, 15, 15, 
            15, 15, 15, 15, 15, 15
        };
		


IJImageProc::IJImageProc(CUTSMemRect *pMR)
{
	pUTSMemRect = pMR;
}

/******************************************
 Get histogram for an array
 *****************************************/
void IJImageProc::Get8BPPHistogram(vector< long > & input, vector< long > & histo)
{
	histo.clear();
	histo.resize(256);
	vector<long>::iterator itor;
	for (itor = input.begin(); itor != input.end(); itor++)
	{
		histo[*itor]++;
	}

	imagemin = 0;
	imagemax = 255;
	imagemode = 0;
	bool foundMin = false;
	bool foundMax = false;
	int temp = 0;
	int maxValue = 0;
	for (int i = 0; i < 256; i++)
	{
		// find the min value in the histogram
		if (!foundMin && histo[i] > 0)
		{
			imagemin = i;
			foundMin = true;
		}
		// find the mode value
		if (histo[i] > maxValue)
		{
			maxValue = histo[i];
			imagemode = i;
		}
	}
	for (int i = 255; i >= 0; i--)
	{
		// find the max value in the histogram
		if (!foundMax && histo[i] > 0)
		{
			imagemax = i;
			break;
		}
	}

}


/******************************************
 Get pixel data into a linear array for a binary image
 *****************************************/
void IJImageProc::Get1BPPImageData(long hInput, vector <long> & localArray)
{
	pUTSMemRect->GetRectInfo(hInput, &imageWidth, &imageHeight);
	imageSize = imageWidth * imageHeight;

	localArray.clear();
	localArray.resize(imageSize);

	for (long y = 0; y < imageHeight; y++)
	{
		for (long x = 0; x < imageWidth; x++)
		{
			long offset = x + y * imageWidth;
			// CUTSMemRect::GetPixel returns the lower left corner pixel as coordinate (0, 0)
			// We need the top left corner pixel as coordinate (0, 0). 
			// The following line will return achieve that.
			long temp = pUTSMemRect->GetPixel(hInput, x, imageHeight - 1 - y); 
			if (invertImage)
			{
				input[offset] = (temp == 0 ? 255 : 0); // invert and pump up to 8 bit
			}
			else
			{
				input[offset] = (temp == 0 ? 0 : 255); // pump up to 8 bit
			}
		}
	}
}

/******************************************
 Get pixel data into a linear array for a 8bpp image
 *****************************************/
void IJImageProc::Get8BPPImageData(long hInput, vector <long> & localArray)
{
	pUTSMemRect->GetRectInfo(hInput, &imageWidth, &imageHeight);
	imageSize = imageWidth * imageHeight;

	localArray.clear();
	localArray.resize(imageSize);

	for (long y = 0; y < imageHeight; y++)
	{
		for (long x = 0; x < imageWidth; x++)
		{
			long offset = x + y * imageWidth;
			// CUTSMemRect::GetPixel returns the lower left corner pixel as coordinate (0, 0)
			// We need the top left corner pixel as coordinate (0, 0). 
			// The following line will return achieve that.
			long temp = pUTSMemRect->GetPixel(hInput, x, imageHeight - 1 - y); 
			localArray[offset] = temp;
		}
	}
}

/******************************************
 Put pixels values in a linear array back
 into an image handle
 *****************************************/
void IJImageProc::ArrayToImage( vector<long> & hArrayIn, long hImageOutput)
{
	for (long y = 0; y < imageHeight; y++)
	{
		for (long x = 0; x < imageWidth; x++)
		{
			long offset = x + y * imageWidth;
			// CUTSMemRect::PutPixel returns the lower left corner pixel as coordinate (0, 0)
			// We need the top left corner pixel as coordinate (0, 0). 
			// The following line will return achieve that.
			pUTSMemRect->PutPixel(hImageOutput, x, imageHeight - 1 - y, hArrayIn[offset]);
		}
	}
}

/******************************************
 Supporting routine to generate Euclidean Distance Map
 *****************************************/
void IJImageProc::SetValue(long offset, long rowsize, vector<long> & input)
{

    long  v;
    long r1  = offset - rowsize - rowsize - 2;
    long r2  = r1 + rowsize;
    long r3  = r2 + rowsize;
    long r4  = r3 + rowsize;
    long r5  = r4 + rowsize;
    long min = 32767;

    v = input[r2 + 2] + one;
    if (v < min)
        min = v;
    v = input[r3 + 1] + one;
    if (v < min)
        min = v;
    v = input[r3 + 3] + one;
    if (v < min)
        min = v;
    v = input[r4 + 2] + one;
    if (v < min)
        min = v;
        
    v = input[r2 + 1] + sqrt2;
    if (v < min)
        min = v;
    v = input[r2 + 3] + sqrt2;
    if (v < min)
        min = v;
    v = input[r4 + 1] + sqrt2;
    if (v < min)
        min = v;
    v = input[r4 + 3] + sqrt2;
    if (v < min)
        min = v;

    v = input[r1 + 1] + sqrt5;
    if (v < min)
        min = v;
    v = input[r1 + 3] + sqrt5;
    if (v < min)
        min = v;
    v = input[r2 + 4] + sqrt5;
    if (v < min)
        min = v;
    v = input[r4 + 4] + sqrt5;
    if (v < min)
        min = v;
    v = input[r5 + 3] + sqrt5;
    if (v < min)
        min = v;
    v = input[r5 + 1] + sqrt5;
    if (v < min)
        min = v;
    v = input[r4] + sqrt5;
    if (v < min)
        min = v;
    v = input[r2] + sqrt5;
    if (v < min)
        min = v;

    input[offset] = min;
}

/******************************************
 Supporting routine to generate Euclidean Distance Map
 *****************************************/
void IJImageProc::SetEdgeValue(long offset, long rowsize, vector<long> & input, long x, long y, long xmax, long ymax)
{

    long  v;
    long r1 = offset - rowsize - rowsize - 2;
    long r2 = r1 + rowsize;
    long r3 = r2 + rowsize;
    long r4 = r3 + rowsize;
    long r5 = r4 + rowsize;
    long min = 32767;
    long offimage = input[r3 + 2];

    if (y<2)
        v = offimage + one;
    else
        v = input[r2 + 2] + one;
    if (v < min)
        min = v;

    if (x<2)
        v = offimage + one;
    else
        v = input[r3 + 1] + one;
    if (v < min)
        min = v;

    if (x>xmax)
        v = offimage + one;
    else
        v = input[r3 + 3] + one;
    if (v < min)
        min = v;

    if (y>ymax)
        v = offimage + one;
    else
        v = input[r4 + 2] + one;
    if (v < min)
        min = v;

    if ((x<2) || (y<2))
        v = offimage + sqrt2;
    else
        v = input[r2 + 1] + sqrt2;
    if (v < min)
        min = v;

    if ((x>xmax) || (y<2))
        v = offimage + sqrt2;
    else
        v = input[r2 + 3] + sqrt2;
    if (v < min)
        min = v;

    if ((x<2) || (y>ymax))
        v = offimage + sqrt2;
    else
        v = input[r4 + 1] + sqrt2;
    if (v < min)
        min = v;

    if ((x>xmax) || (y>ymax))
        v = offimage + sqrt2;
    else
        v = input[r4 + 3] + sqrt2;
    if (v < min)
        min = v;

    if ((x<2) || (y<2))
        v = offimage + sqrt5;
    else
        v = input[r1 + 1] + sqrt5;
    if (v < min)
        min = v;

    if ((x>xmax) || (y<2))
        v = offimage + sqrt5;
    else
        v = input[r1 + 3] + sqrt5;
    if (v < min)
        min = v;

    if ((x>xmax) || (y<2))
        v = offimage + sqrt5;
    else
        v = input[r2 + 4] + sqrt5;
    if (v < min)
        min = v;

    if ((x>xmax) || (y>ymax))
        v = offimage + sqrt5;
    else
        v = input[r4 + 4] + sqrt5;
    if (v < min)
        min = v;

    if ((x>xmax) || (y>ymax))
        v = offimage + sqrt5;
    else
        v = input[r5 + 3] + sqrt5;
    if (v < min)
        min = v;

    if ((x<2) || (y>ymax))
        v = offimage + sqrt5;
    else
        v = input[r5 + 1] + sqrt5;
    if (v < min)
        min = v;

    if ((x<2) || (y>ymax))
        v = offimage + sqrt5;
    else
        v = input[r4] + sqrt5;
    if (v < min)
        min = v;

    if ((x<2) || (y<2))
        v = offimage + sqrt5;
    else
        v = input[r2] + sqrt5;
    if (v < min)
        min = v;

    input[offset] = (short)min;
  
}

/******************************************
 Supporting routine to generate Euclidean Distance Map
 *****************************************/
void IJImageProc::FinishEDM()
{
	long round = one / 2;
	maxEDM = 0;
	for (long y = 0; y < imageHeight; y ++)
	{
		for (long x = 0; x < imageWidth; x++)
		{
			long offset = x + y * imageWidth;
			// Reduce the value for each pixel by "one" which equals 41
			// The counters the action in "MakeEDM" which multiple each item by "one"
			long v = (input[offset] + round) / one; 

			if (v > 255)
			{
				v = 255;
			}
			// Also keep track of the max EDM value
			if (v > maxEDM)
			{
				maxEDM = v;
			}
			edm[offset] = v;
		}
	}

	// copy all items in edm into edm2
	edm2.assign(edm.begin(), edm.end());
}

/******************************************
 Routine to generate Euclidean Distance Map

 Converts a binary image into a greyscale Euclidean Distance Map (EDM).
 Each foreground (black) pixel in the binary image is assigned a value
 equals to its distance from the nearest background (white) pixel. Uses
 the two-pass EDM algorithm from the "Image Processing Handbook" by
 John Russ.
 *****************************************/
void IJImageProc::MakeEDM()
{

	long width = imageWidth;
	long height = imageHeight;
	long rowsize = width;
	long xmax = width - 3;
	long ymax = height - 3;

	// multiple every items in input by "one" which actually equals 41
	vector< long >::iterator itor;
	for (itor = input.begin(); itor != input.end(); itor++)
	{
		// Multiplying each element of the vector "itor" by one = 41
		*itor *= one;
	}
	
	// Two-pass EDM algorithm from the "Image Processing Handbook" by John Russ
	for (long y = 0; y < height; y++)
	{
		for (long x = 0; x < width; x++)
		{
			long offset = x + y * rowsize;
			if (input[offset] <= 0)
			{
				continue;
			}

			if (x < 2 || x > xmax || y < 2 || y > ymax)
			{
				SetEdgeValue(offset, rowsize, input, x, y, xmax, ymax);
			}
			else
			{
				SetValue(offset, rowsize, input);
			}			
		}
	}

	for (long y = height - 1; y >= 0; y--)
	{
		for (long x = width - 1; x >= 0; x--)
		{
			long offset = x + y * rowsize;
			if (input[offset] <= 0)
			{
				continue;
			}
			if (x < 2 || x > xmax || y < 2 || y > ymax)
			{
				SetEdgeValue(offset, rowsize, input, x, y, xmax, ymax);
			}
			else
			{
				SetValue(offset, rowsize, input);
			}
			
		}
	}

	FinishEDM();
}

/******************************************
 Supporting routine for finding Ultimate Eorsion Point (UEP)
 *****************************************/
long IJImageProc::Get(long x, long y, vector<long> & pixels, long width, long height)
{
    if(x <= 0)
	{
		x = 0;
	}
    if(x >= width)
	{
		x = width - 1;
	}
    if(y <= 0)
	{
		y = 0;
	}
    if(y >= height)
	{
		y = height - 1;
	}
    return pixels[x + y * width];

}

/******************************************
 Supporting routine for Watershed processing
 *****************************************/
void IJImageProc::FilterEDM(vector<long>& input, bool smooth)
{
	vector<long> edmTemp(input);

    long xmax = imageWidth - 1;
    long ymax = imageHeight - 1;
	long rowsize = imageWidth;
    for(long y = 0; y < imageHeight; y++)
    {
        for(long x = 0; x < imageWidth; x++)
        {
            long offset = x + y * rowsize;
            long p0 = edmTemp[offset];
            if(p0 <= 1)
                continue;
            long sum = edmTemp[offset] * 2;
            long p1 = x <= 0 || y <= 0 ? Get(x - 1, y - 1, edmTemp, imageWidth, imageHeight) : ((long) (edmTemp[offset - rowsize - 1]));
            long p2 = y <= 0 ? Get(x, y - 1, edmTemp, imageWidth, imageHeight) : ((long) (edmTemp[offset - rowsize]));
            long p3 = x >= xmax || y <= 0 ? Get(x + 1, y - 1, edmTemp, imageWidth, imageHeight) : ((long) (edmTemp[(offset - rowsize) + 1]));
            long p4 = x >= xmax ? Get(x + 1, y, edmTemp, imageWidth, imageHeight) : ((long) (edmTemp[offset + 1]));
            long p5 = x >= xmax || y >= ymax ? Get(x + 1, y + 1, edmTemp, imageWidth, imageHeight) : ((long) (edmTemp[offset + rowsize + 1]));
            long p6 = y >= ymax ? Get(x, y + 1, edmTemp, imageWidth, imageHeight) : ((long) (edmTemp[offset + rowsize]));
            long p7 = x <= 0 || y >= ymax ? Get(x - 1, y + 1, edmTemp, imageWidth, imageHeight) : ((long) (edmTemp[(offset + rowsize) - 1]));
            long p8 = x <= 0 ? Get(x - 1, y, edmTemp, imageWidth, imageHeight) : ((long) (edmTemp[offset - 1]));
            long v = p0 - 1;
            if(smooth)
            {
                input[offset] = ((p0 + p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8) / 9);
                continue;
            }
            if(p2 == v && p4 == v && p6 == v && p8 == v && (p1 == p0 && p3 == v && p5 == v && p7 == v || p3 == p0 && p1 == v && p5 == v && p7 == v || p5 == p0 && p1 == v && p3 == v && p7 == v || p7 == p0 && p1 == v && p3 == v && p5 == v))
                input[offset] = v;
        }
    }
}

/******************************************
 Generate the xy coordinate arrays that allow pixels
 at each level to be accessed directly without searching
 through the entire image. This helps to speed up
 the watershed segmentation routine.
 *****************************************/
void IJImageProc::MakeCoordinateArrays()
{
    long ArraySize = 0;
    for(long i = 0; i < maxEDM - 1; i++)
	{
        ArraySize += histogram[i];
	}

    xCoordinate.clear();
	yCoordinate.clear();
	xCoordinate.resize(ArraySize, 0);
	yCoordinate.resize(ArraySize, 0);

    long offset = 0;
	levelStart.clear();
	levelStart.resize(256, 0);
    for(long i = 0; i < 256; i++)
    {
        levelStart[i] = offset;
        if(i > 0 && i < maxEDM)
            offset += histogram[i];
    }

	levelOffset.clear();
	levelOffset.resize(256, 0);
    for(long y = 0; y < imageHeight; y++)
    {
        for(long x = 0; x < imageWidth; x++)
        {
            long v = edm2[x + y * imageWidth];
            if(v > 0 && v < maxEDM)
            {
                offset = levelStart[v] + levelOffset[v];
				xCoordinate[offset] = x;
				yCoordinate[offset] = y;
				levelOffset[v]++;
            }
        }
    }
}

/***************************************
 Finds peaks in the EDM that contain pixels 
 equals or greater than all of their neighbors.

 Note: The UEP is further modified if we need it
       for final Watershed processing. So the UEP
	   generated just for its purpose and the UEP 
	   generated for Watershed are DIFFERENT!
 ******************************************/
void IJImageProc::FindUltimatePoints()
{
    if(watershed)
    {
        FilterEDM(edm2, true);
        FilterEDM(edm2, false);
    }

	Get8BPPHistogram(edm2, histogram);

	// Make the cooridnate arrays for faster searching
    MakeCoordinateArrays();
	
	vector<long> edmTemp(edm2); // create a temp local copy of edm2

    long xmax = imageWidth - 1;
    long ymax = imageHeight - 1;
	long rowsize = imageWidth;
    for(long level = maxEDM - 1; level >= 1; level--)
    {
        long count;
        do
        {
            count = 0;
            for(long i = 0; i < histogram[level]; i++)
            {
                long CoordOffset = levelStart[level] + i;
                long x = xCoordinate[CoordOffset];
                long y = yCoordinate[CoordOffset];
                long offset = x + y * imageWidth;
                if((edm2[offset]) != 255)
				{
					bool setPixel = false;
					if(x > 0 && y > 0 && (edm2[offset - rowsize - 1]) > level)
						setPixel = true;
					if(y > 0 && (edm2[offset - rowsize]) > level)
						setPixel = true;
					if(x < xmax && y > 0 && (edm2[(offset - rowsize) + 1]) > level)
						setPixel = true;
					if(x < xmax && (edm2[offset + 1]) > level)
						setPixel = true;
					if(x < xmax && y < ymax && (edm2[offset + rowsize + 1]) > level)
						setPixel = true;
					if(y < ymax && (edm2[offset + rowsize]) > level)
						setPixel = true;
					if(x > 0 && y < ymax && (edm2[(offset + rowsize) - 1]) > level)
						setPixel = true;
					if(x > 0 && (edm2[offset - 1]) > level)
						setPixel = true;
					if(setPixel)
					{
						edm2[offset] = 255;
						count++;
					}
				}
            }
        } while(count != 0);
    }

    if(watershed)
    {
        for(long i = 0; i < imageWidth * imageHeight; i++)
		{
            if((edm2[i]) > 0 && (edm2[i]) < 255)
			{
                edmTemp[i] = 255;
			}
		}

		// try to do what I think the ImageJ is trying to do with "ip.insert(ip2, 0, 0)"
		// I think it's trying to Get the modified data in ip2 back into ip
		// and ip2 is local and will be discarded later
		// So we will just swap edm2 and edmTemp
		edm2.swap(edmTemp); // this moves the data in edmTemp into edm2
    } 
	else
    {
        for(long i = 0; i < imageWidth * imageHeight; i++)
		{
            if((edm2[i]) == 255)
			{
                edm2[i] = 0;
			}
		}
    }

	// copy all items of edm2 into uep
	uep.assign(edm2.begin(), edm2.end());
}


/******************************************
 Supporting routine for Watershed processing
 Turn the final result into a binary mask
 *****************************************/
void IJImageProc::PostProcess()
{
	ws.assign(edm2.begin(), edm2.end());
	vector<long>::iterator itor;
	for (itor = ws.begin(); itor != ws.end(); itor++)
	{
		if (*itor < 255)
			*itor = 0;
		if (*itor == 255)
			*itor = 1;
	}
}

/******************************************
 Supporting routine for Watershed processing
 *****************************************/
void IJImageProc::ProcessLevel(int level, int pass, vector<long>& input1, vector<long>& input2)
{
    long rowSize = imageWidth;
    long height = imageHeight;
    long xmax = imageWidth - 1;
    long ymax = imageHeight - 1;

	input2.assign(input1.begin(), input1.end());

    for(long i = 0; i < histogram[level]; i++)
    {
        long coordOffset = levelStart[level] + i;
        long x = xCoordinate[coordOffset];
        long y = yCoordinate[coordOffset];
        long offset = x + y * rowSize;
        if((input2[offset]) != 255)
        {
            long index = 0;
            if(x > 0 && y > 0 && (input2[offset - rowSize - 1]) == 255)
                index ^= 1;
            if(y > 0 && (input2[offset - rowSize]) == 255)
                index ^= 2;
            if(x < xmax && y > 0 && (input2[(offset - rowSize) + 1]) == 255)
                index ^= 4;
            if(x < xmax && (input2[offset + 1]) == 255)
                index ^= 8;
            if(x < xmax && y < ymax && (input2[offset + rowSize + 1]) == 255)
                index ^= 0x10;
            if(y < ymax && (input2[offset + rowSize]) == 255)
                index ^= 0x20;
            if(x > 0 && y < ymax && (input2[(offset + rowSize) - 1]) == 255)
                index ^= 0x40;
            if(x > 0 && (input2[offset - 1]) == 255)
                index ^= 0x80;
            switch(pass)
            {
            default:
                break;

            case 1: // '\001'
                if((WSFateTable[index] & 1) == 1)
                {
                    input1[offset] = 255;
                    count++;
                }
                break;

            case 2: // '\002'
                if((WSFateTable[index] & 2) == 2)
                {
                    input1[offset] = 255;
                    count++;
                }
                break;

            case 3: // '\003'
                if((WSFateTable[index] & 4) == 4)
                {
                    input1[offset] = 255;
                    count++;
                }
                break;

            case 4: // '\004'
                if((WSFateTable[index] & 8) == 8)
                {
                    input1[offset] = 255;
                    count++;
                }
                break;
            }
        }
    }
}

/******************************************
 Do Watershed segmentation and generate a bianry mask
 The generated binary mask always has white objects 
 on blackbackground (i.e. objects are NOT masked)
 *****************************************/
void IJImageProc::DoWatershed()
{
    long level = maxEDM - 1;
	vector<long> edmTemp(edm2);
    do
    {
        if(level < 1)
            break;
        do
        {
            count = 0;
            ProcessLevel(level, 1, edm2, edmTemp);
            ProcessLevel(level, 3, edm2, edmTemp);
            ProcessLevel(level, 2, edm2, edmTemp);
            ProcessLevel(level, 4, edm2, edmTemp);
        } while(count > 0);
        level--;
    } while(true);
    PostProcess();
}

/************************************************** mrWatershedByEDM */
/***
    Perform Watershed segmentation based on Euclidean Distance Map
	The whole process goes through:
	1. Euclidean Distance Map (EDM) generation
	2. Ultimate Erosion Point (UEP) generation
	3. Watershed (WS) segmentation 
	One can choose to proceed to either step 1, 2, or 3 by passing in
	the "operation" parameters as "EDM", "UEP", or "WS".
	When choose to process to UEP or WS level, it is OPTIONAL to retrieve
	the intermediate resulting EDM and UEP by passing in a valid image handle.

	Parameters:
	 hInput:			Input image (1 bpp binary image) handle
	 hOutputEDM:		Output EDM image (8 bpp greyscale image) handle (required to be valid for EDM operation, optional for UEP and WS operation)
	 hOutputUEP:		Output UEP image (8 bpp greyscale image) handle (required to be valid for UEP operation, optional for WS operation)
	 hOutputWS:			Output WS image (1 bpp binary image) handle (always has white objects on black background)
	 operation:			Choose among "EDM", "UEP" and "WS"
	 isWhiteBackground:	Flag indicating if the input image with black objects on white background
 	Returns:
   	 -1		Operation failed
	 1		Operation successful

***/
long IJImageProc::WatershedByEDM (long hInput, long hOutputEDM, long hOutputUEP, long hOutputWS, char *operation, bool isWhiteBackground)
{
	long ret = -1; // return value
	CString oper(operation);

	if (hInput < 1 || (hOutputEDM < 1 && hOutputUEP < 1 && hOutputWS < 1))
		goto Fin;
	else
	{
		long xe, ye, pitch, address, bitPerPixel, xeEDM, yeEDM, xeUEP, yeUEP, xeWS, yeWS;
		bitPerPixel = pUTSMemRect->GetExtendedInfo(hInput, &xe, &ye, &pitch, &address);
		if (bitPerPixel != 1)
			goto Fin;
		else
		{
			// set the inverse flag
			if (!isWhiteBackground)
			{
				invertImage = false;
			}
			else
			{
				invertImage = true;
			}

			if (hOutputEDM >= 1)
			{
				bitPerPixel = pUTSMemRect->GetExtendedInfo(hOutputEDM, &xeEDM, &yeEDM, &pitch, &address);
				if (bitPerPixel != 8 || xe != xeEDM || ye != yeEDM )
				{
					goto Fin;
				}
			}
			if (hOutputUEP >= 1)
			{
				bitPerPixel = pUTSMemRect->GetExtendedInfo(hOutputUEP, &xeUEP, &yeUEP, &pitch, &address);
				if (bitPerPixel != 8 || xe != xeUEP || ye != yeUEP )
				{
					goto Fin;
				}
			}
			if (hOutputWS >= 1)
			{
				bitPerPixel = pUTSMemRect->GetExtendedInfo(hOutputWS, &xeWS, &yeWS, &pitch, &address);
				if (bitPerPixel != 1 || xe != xeWS || ye != yeWS )
				{
					goto Fin;
				}
			}
			
			// Initialize
			Get1BPPImageData(hInput, input);
			edm.clear();
			edm2.clear();
			uep.clear();
			ws.clear();
			edm.resize(imageSize);
			edm2.resize(imageSize);
			uep.resize(imageSize);
			ws.resize(imageSize);

			// Initialize the watershed flag
			watershed = false;

			MakeEDM();
			if (oper.CompareNoCase("EDM") == 0)
			{
				ArrayToImage(edm, hOutputEDM);
			}
			else if (oper.CompareNoCase("UEP") == 0)
			{
				FindUltimatePoints();
				if (hOutputEDM >= 1)
					ArrayToImage(edm, hOutputEDM);
				ArrayToImage(uep, hOutputUEP);
			}
			else if (oper.CompareNoCase("WS") == 0)
			{
				watershed = true;
				FindUltimatePoints();
				DoWatershed();
				if (hOutputEDM >= 1)
					ArrayToImage(edm, hOutputEDM);
				if (hOutputUEP >= 1)
					ArrayToImage(uep, hOutputUEP);
				ArrayToImage(ws, hOutputWS);
			}
			else
			{
				goto Fin;			
			}
		}
	}
	ret = 1;
Fin:
	return ret;
}

/******************************************
 Automatically determine the threshold
 Expect 8bpp greyscale image as input
 It calculates a threshold value inside the histo
 That will be the returned value from this routine
 In addition:
 It tries to determine the modal peak inside the histo
 If the min value of the histo is closer to the modal peak (compare to the distance between modal peak and max value),
 it will use the threshold as the lower bound and max value as the upper bound
 If the max value of the histo is closer to the modal peak (compare to the distance between modal peak and min value),
 it will use the threshold as the upper bound and the min value as the lower bound
 *****************************************/

long IJImageProc::AutoThreshold(long hInput, long & minThreshold, long & maxThreshold)
{
	// Get the input image's pixels
	Get8BPPImageData(hInput, input);
	
	// Get the histogram and image stats
	Get8BPPHistogram(input, histogram);

	long threshold = 0;

	/** Iterative thresholding technique, described originally by Ridler & Calvard in
		"PIcture Thresholding Using an Iterative Selection Method", IEEE transactions
		on Systems, Man and Cybernetics, August, 1978. 
		*/
    int maxValue = 255;
    double result,tempSum1,tempSum2,tempSum3,tempSum4;

    histogram[0] = 0; //set to zero so erased areas aren't included
    histogram[maxValue] = 0;
    int localmin = 0;
    while ((histogram[localmin]==0) && (localmin<maxValue))
        localmin++;
    int localmax = maxValue;
    while ((histogram[localmax]==0) && (localmax>0))
        localmax--;
    if (localmin>=localmax) 
	{
        threshold = 256/2;
    }
	else
	{	
		int movingIndex = localmin;
		int inc = localmax/40;
		if (inc<1) inc = 1;
		do 
		{
			tempSum1=tempSum2=tempSum3=tempSum4=0.0;
			for (int i=localmin; i<=movingIndex; i++) 
			{
				tempSum1 += i*histogram[i];
				tempSum2 += histogram[i];
			}
			for (int i=(movingIndex+1); i<=localmax; i++) 
			{
				tempSum3 += i *histogram[i];
				tempSum4 += histogram[i];
			}

			result = (tempSum1 / tempSum2 / 2.0) + (tempSum3 / tempSum4 / 2.0);
			movingIndex++;

		} 
		while ( ( (movingIndex+1) <= result ) && ( movingIndex <= (localmax - 1)) );

		threshold = (long)(result + 0.5);
	}
	

	if ((imagemax - imagemode) < (imagemode - imagemin))
	{
		minThreshold = imagemin;
		maxThreshold = threshold;
	}
	else
	{
		minThreshold = threshold;
		maxThreshold = imagemax;

	}
	return threshold;
}

/******************************************
 Supporting routine for Rank Filtering
 *****************************************/

// Modified algorithm  according to http://www.geocities.com/zabrodskyvlada/3alg.html
// Contributed by Heinz Klar.
double IJImageProc::FindMedian(vector<double> & values) 
{
	int nValues = values.size();
    int nv1b2 = (nValues-1)/2;
    int i,j;
    int l=0;
    int m=nValues-1;
    double med=values[nv1b2];
    double dum ;

    while (l<m) 
	{
        i=l ;
        j=m ;
        do 
		{
            while (values[i]<med) i++ ;
            while (med<values[j]) j-- ;
            dum=values[j];
            values[j]=values[i];
            values[i]=dum;
            i++ ; j-- ;
        } while ((j>=nv1b2) && (i<=nv1b2)) ;
        if (j<nv1b2) l=i ;
        if (nv1b2<i) m=j ;
        med=values[nv1b2] ;
    }
    return med ;
}

/******************************************
 Supporting routine for Rank Filtering
 *****************************************/
double IJImageProc::FindMin(vector<double> & values) 
{
    double min = values[0];
    for (int i=1; i<values.size(); i++)
		if (values[i]<min)
            min = values[i];
    return min;
}

/******************************************
 Supporting routine for Rank Filtering
 *****************************************/
double IJImageProc::FindMax(vector<double> & values) 
{
    double max = values[0];
    for (int i=1; i<values.size(); i++)
		if (values[i]>max)
            max = values[i];
    return max;
}

/******************************************
 Supporting routine for Rank Filtering
 *****************************************/
double IJImageProc::FindMean(vector<double> & values) 
{
    double sum = values[0];
    for (int i=1; i<values.size(); i++)
		sum += values[i];
    return (double)(sum/values.size());
	
}

/******************************************
 Supporting routine for Rank Filtering
 *****************************************/
double IJImageProc::GetRankingPixel(int x, int y, vector<double> & pixels, int width, int height) 
{
    if (x<=0)
		x = 0;
    if (x>=width) 
		x = width-1;
    if (y<=0) y = 0;
    if (y>=height) 
		y = height-1;
    return pixels[x+y*width];
	
}

/******************************************
 Supporting routine for Rank Filtering
 Creating a circular mask with user defined radius
 Radius = 0.5
  1
 111
  1
 Radius = 1
 111
 111
 111
 Radius = 1.5
   1  
  111
 11111
  111
   1
 Radius = 2
  111
 11111
 11111
 11111
  111
 *****************************************/
void IJImageProc::CreateCircularMask(int maskWidth, double maskRadius) 
{
    CircularMask.clear();

	CircularMask.resize(maskWidth*maskWidth);
    int r = maskWidth/2;
    int r2 = (int) (maskRadius*maskRadius) + 1;
    for (int x=-r; x<=r; x++)
	{
		for (int y=-r; y<=r; y++)
		{
			if ((x*x+y*y)<=r2)
			{
				CircularMask[r+x+(r+y)*maskWidth]=1;
			}
		}
	}
}

/******************************************
 Supporting routine for Rank Filtering
 *****************************************/
void IJImageProc::Rank(vector<double> & input, double maskRadius, RankFilterType rankType) 
{
    int width = imageWidth;
    int height = imageHeight;
    int x1=0, y1=0, x2=width-1, y2=height-1;
    int kw = ((int)(maskRadius+0.5))*2 + 1;
    int kh = kw;

	CreateCircularMask(kw, maskRadius);
    int maskSize = 0;
	int i;
    for (i=0; i<kw*kw; i++)
        if (CircularMask[i]!=0)
            maskSize++;
    vector<double> values;
	values.resize(maskSize);
    int uc = kw/2;
    int vc = kh/2;
    vector <double> pixels2(input);
    double sum;
    int offset, count;
    bool edgePixel;
    int xedge = width-uc;
    int yedge = height-vc;
    for(int y=y1; y<=y2; y++) 
	{
        for(int x=x1; x<=x2; x++) 
		{
            sum = 0.0;
            i = 0;
            count = 0;
            edgePixel = (y<vc || y>=yedge || x<uc || x>=xedge);
            for(int v=-vc; v <= vc; v++) 
			{
                offset = x+(y+v)*width;
                for(int u = -uc; u <= uc; u++) 
				{
                    if (CircularMask[i++]!=0) 
					{
                        if (edgePixel)
						{
                            values[count] = GetRankingPixel(x+u, y+v, pixels2, width, height);
						}
                        else
						{
                            values[count] = pixels2[offset+u];
						}
                        count++;
                    }
                }
            }
			if (rankType == IJImageProc::RANK_MEDIAN)
			{
                input[x+y*width] = FindMedian(values);
			}
			else if (rankType == IJImageProc::RANK_MEAN)
			{
                input[x+y*width] = FindMean(values);
			}
			else if (rankType == IJImageProc::RANK_MIN)
			{
                input[x+y*width] = FindMin(values);
			}
			else  if (rankType == IJImageProc::RANK_MAX)
			{
                input[x+y*width] = FindMax(values);
			}
			

        }
    }
}

/******************************************
 Rank filtering
 Expect an input and a output image with the same size (either 1bpp or 8bpp)
 Caller defined circular mask radius
 Operation can be "MIN", "MAX", "MEDIAN", or "MEAN"
 *****************************************/
void IJImageProc::RankFilter(long hInput, long hOutput, double maskRadius, char* operation)
{
	// Get the image pixels
	Get8BPPImageData(hInput, input);
	vector<double> doubleValues;
	doubleValues.resize(input.size());
	for (long i = 0; i < input.size(); i ++)
	{
		doubleValues[i] = (double)input[i];
	}

	CString oper(operation);
	if (oper.CompareNoCase("MEDIAN") == 0)
	{
		Rank(doubleValues, maskRadius, IJImageProc::RANK_MEDIAN);
	}
	else if (oper.CompareNoCase("MEAN") == 0)
	{
		Rank(doubleValues, maskRadius, IJImageProc::RANK_MEAN);
	}
	else if (oper.CompareNoCase("MIN") == 0)
	{
		Rank(doubleValues, maskRadius, IJImageProc::RANK_MIN);
	}
	else if (oper.CompareNoCase("MAX") == 0)
	{
		Rank(doubleValues, maskRadius, IJImageProc::RANK_MAX);
	}

	for (long i = 0; i < input.size(); i++)
	{
		input[i] = (long)doubleValues[i];
	}

	ArrayToImage(input, hOutput);
}



void IJImageProc::MixtureModelThreshold(long hInput, long & theThreshold)
{

	Get8BPPImageData(hInput, input);

	Get8BPPHistogram(input, histogram);

	CMixtureModelThreshold mmt(histogram);
	
	mmt.CalculateThreshold(theThreshold);
}









 