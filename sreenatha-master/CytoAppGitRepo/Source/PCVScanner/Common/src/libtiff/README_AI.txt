How I added libtiff to the source tree
--------------------------------------
Extracted tiff-v3.5.6-beta.zip into AI_CODE_TREE\Common\src\

Renamed directory tiff-v3.5.6-beta to libtiff

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
NEW METHOD: CREATE FRESH PROJECT

Opened the MDS2 workspace and added a new Win32 Static Library project,
named libtiff, located in AI_CODE_TREE\Common\src\. Pre-compiled header
and MFC support were NOT addded.

Added all .c files from the libtiff\libtiff directory whose object files are
listed in the file libtiff\libtiff\makefile.vc.

In the project settings:
Added TIF_PLATFORM_WINDOWED to the preprocessor definitions for both Debug
and Release builds.
Modified the Code Generation settings for Debug builds to use the
'Debug Multithreaded DLL' version of the runtime library, and for Release
builds to use the 'Multithreaded DLL' version.
Modified the 'Output file name' to be ../../lib/dLibtiff.lib for Debug builds
and ../../lib/libtiff.lib for Release builds.

Added a new Win32 Console Application project named 'tiffvers', and located
it in the libtiff directory (careful - the new project dialog tries to locate
it in a directory named after the new project). Added the file mkversion.c
to the project files. Changed the output and intermediate directories to
'DebugTiffvers' and 'ReleaseTiffvers' respectively, so that intermediate
files are not mixed with those for libtiff (can lead to build errors).
Added the following to the Custom Build Step for Debug build (added same for
Release build, but substituted 'Release' for 'Debug'):

Command Line:

cd DebugTiffvers
if exist tiffvers.h del tiffvers.h
tiffvers.exe -v ..\VERSION tiffvers.h
copy tiffvers.h ..\..\..\include

Outputs (note the path is relative to the project):

..\..\include\tiffvers.h

The custom build step is only performed if the outputs do not exist or are
not up to date - this is important, as if it was performed every time then
all the files that include tiffvers.h would be rebuilt too.
The custom build step is used rather than the post build step as the latter
is only performed if tiffvers.exe is not up to date, rather than if tiffvers.h
is not up to date.


Made libtiff dependent on tiffvers.

NEW METHOD ENDS
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
OLD METHOD: USE SUPPLIED MAKEFILE
Opened "AI_CODE_TREE\Common\src\libtiff\libtiff\makefile.vc" in Visual Studio,
using File | Open Workspace..., which allows you to create a project file for
the library (named it libtiff.dsp).

Edited makefile.vc so that WINMODE = -DTIF_PLATFORM_WINDOWED is defined
(rather than WINMODE = -DTIF_PLATFORM_CONSOLE). Added the lines:
	copy libtiff.lib ..\..\..\lib
	copy tiffvers.h ..\..\..\include
to the commands for the libtiff.lib target, so that the library file is copied
to the common library directory (AI_CODE_TREE\Common\lib), and the tiffvers.h
file (which is automatically generated every time the library is built) is
copied to the common include directory (AI_CODE_TREE\Common\include).

11/06/2002: Added /MD to CFLAGS, so that the Multithreaded DLL version of
the run-time library is used, in common with other libraries used by MDS2.

Edited the project settings in Visual Studio (Project | Settings...),
to set 'Build command line' to "nmake /f makefile.vc"

The project could then be built from visual studio in the normal way.
OLD METHOD ENDS
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Copied tiff.h and tiffio.h to AI_CODE_TREE\Common\include

Note that because the tiffvers.h file is created when the library is built, it
must NOT be checked in, either from the libtiff directory or from the include
directory (where it is copied to).

Note also that the library is built as a static library rather than as a DLL,
because the AItiff library needs access to some of the internals of libtiff,
which aren't exported from the DLL if the supplied .DEF file is used. However,
it would be relatively simple to add to the required functions to the .DEF
file if desired, so that a DLL could be used.


How I added zlib to the source tree
-----------------------------------
Extracted zlib113.zip into AI_CODE_TREE\Common\src\zlib\

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
NEW METHOD: CREATE FRESH PROJECT

Copied the file zlib\nt\zlib.dnt to zlib\zlib.def.

Opened the MDS2 workspace and added a new Win32 Dynamic-Link Library project,
named zlib, located in AI_CODE_TREE\Common\src\. No files were added by the wizard.

Added the .c files adler32, compress, crc32, deflate, gzio, infblock, infcodes,
inffast, inflate, inftrees, infutil, trees, uncompr, zutil, and the zlib.def file
to the project.

Created a new file dZLib.def which is a copy of zlib.def but with dZlib.dll
specified as the LIBRARY name. Added this to the project. Modified the Debug build
settings for zlib.def so that it is excluded from the Debug build. Modified the
Release build settings for dZlib.def so that it is excluded from the Release build.

In the project settings:
Modified the Code Generation settings for Debug builds to use the
'Debug Multithreaded DLL' version of the runtime library, and for Release
builds to use the 'Multithreaded DLL' version.
Modified the 'Output file name' to be ../../bin/dZlib.dll for Debug builds
and ../../bin/zlib.dll for Release builds.
Modified the inport library file name to be ../../lib/dZlib.lib for Debug builds
and ../../lib/zlib.lib for Release builds (in 'Project Options' panel at bottom
of 'link' tab).

NEW METHOD ENDS
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
OLD METHOD: USE SUPPLIED MAKEFILE
Copied the files Makefile.nt and zlib.dnt from the directory
AI_CODE_TREE\Common\src\zlib\nt\ to the directory AI_CODE_TREE\Common\src\zlib\

Opened "AI_CODE_TREE\Common\src\zlib\Makefile.nt" in Visual Studio,
using File | Open Workspace..., which allows you to create a project file for
the library (named it zlib.dsp).

Edited makefile.nt: added the lines:
	copy zlib.dll ..\..\bin
	copy zlib.lib ..\..\lib
	copy zlib.h ..\..\include
	copy zconf.h ..\..\include
to the commands for the zlib.dll target, so that the DLL, import library and
files for zlib are copied to the appropriate shared directories.

Edited the project settings in Visual Studio (Project | Settings...),
to set 'Build command line' to "nmake /f makefile.nt zlib.dll"

The project could then be built from visual studio in the normal way.

The makefile already uses the DLL version of the C / C++ run-time library,
which is consistent with other libraries in MDS2.
OLD METHOD ENDS
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


How I added ZIP ('DEFLATE') compression support to libtiff
----------------------------------------------------------
NEW METHOD:
Added ZIP_SUPPORT to the preprocessor definitions and ../zlib to the
'Addition include directories', for both Debug and Release builds. Made
libtiff dependent on zlib.

OLD METHOD:
Added -DZIP_SUPPORT to CFLAGS and -I../../zlib to INCL (so the zlib
headers could be found) in libtiff's Makefile.vc


How I added jpeg to the source tree
-----------------------------------
Extracted jpegsrc.v6b.tar.gz into AI_CODE_TREE\Common\src\jpeglib\

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
NEW METHOD: CREATE FRESH PROJECT

Opened the MDS2 workspace and added a new Win32 Static Library project,
named jpeglib, located in AI_CODE_TREE\Common\src\. Pre-compiled header
and MFC support were NOT addded.

Added all the .c files whose name begins with a 'j' from the jpeglib directory
to the project, except for jmemansi, jmemdos, jmemmac, jmemname.

In the project settings:
Modified the Code Generation settings for Debug builds to use the
'Debug Multithreaded DLL' version of the runtime library, and for Release
builds to use the 'Multithreaded DLL' version.
Modified the 'Output file name' to be ../../lib/dJpeglib.lib for Debug builds
and ../../lib/jpeglib.lib for Release builds.

NEW METHOD ENDS
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
OLD METHOD: USE SUPPLIED MAKEFILE
Followed the instructions in 'install.doc' under the heading 'Microsoft Windows, Microsoft Developer Studio'.

In Visual Studio, modified the project settings for the library
so that it uses the Multithreaded DLL run-time library,
in common with other libraries used by MDS2.

The self-test failed, with: 'FC: testimg.ppm longer than TESTOUT.PPM', but TESTOUT.PPM was readable, so I ignored this.
OLD METHOD ENDS
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


How I added jpeg compression support to libtiff
-----------------------------------------------
NEW METHOD:
Added JPEG_SUPPORT to the preprocessor definitions and ../jpeglib to the
'Addition include directories', for both Debug and Release builds. Made
libtiff dependent on jpeglib.

OLD METHOD:
Added -DJPEG_SUPPORT to CFLAGS and -I../jpeglib include to INCL (so the jpeg
headers could be found) in libtiff's Makefile.vc



JMB July 2002
