/*
 * add.c	Jim Piper	July 1983
 *
 */

#include "woolz.h"

#include <stdio.h>
//#include <wstruct.h>

/*
 * Add grey values of object, but only if they have identical domain.
 * Resulting object must pre-exist, and also have identical domain.
 * There is no objection to any of the objects being the same, so this
 * routine may be used to add a first object to a second, leaving the
 * result in the second.
 */
#ifdef WIN32
int
add(struct object *o1, struct object *o2, struct object *o3)
#endif
#ifdef i386
int
add(o1, o2, o3)
struct object *o1;
struct object *o2;
struct object *o3;
#endif
{
	struct iwspace i1, i2, i3;
	struct gwspace g1, g2, g3;
	register int i;
	if (o1->idom != o2->idom)
		return(0);
	if (o1->idom != o3->idom)
		return(0);
	initgreyscan(o1,&i1,&g1);
	initgreyscan(o2,&i2,&g2);
	initgreyscan(o3,&i3,&g3);
	while (nextgreyinterval(&i1) == 0) {
		nextgreyinterval(&i2);
		nextgreyinterval(&i3);
		for (i=0; i<i1.colrmn; i++)
			*g3.grintptr++ = *g1.grintptr++
					     + *g2.grintptr++;
	}
	return(1);
}
