/* 
 *		C O N S T R U C T . C   --  Construct woolz object from 2D memory
 *
 *  Written: Clive A Stubbings
 *           Applied Imaging
 *           720 Birchwood Boulevard
 *           Birchwood Science Park
 *           Warrington
 *           Cheshire
 *           WA3 7PX
 *
 *	Copyright (c) and intellectual property rights Applied Imaging (1992)
 *
 *	Based on:	fip/fsconstruct by B. Pinnington
 *
 *  Date:    	27 July 1992
 *
 *	Description:
 *				Uses two passes through the data, one to find
 *				the number of intervals required, the other to fill
 *				them out. The object returned is interval structured
 *				with a completed interval structured grey table.
 *
 *  Modifications
 *
 * 10Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *					Also, made minor mods to stop compiler warnings and removed
 *					standard C parameter lists (were '#ifdef i386' only).
 */

#include <woolz.h>

#include <stdio.h>
#include <malloc.h> // For malloc, calloc, free
//#include <wstruct.h>

#define TRUE 1
#define FALSE 0

static void build_high();
static int /*build_high(),*/count_high(), count_low();
static struct object *cmakeobj();
static int memerrfunc(), checkerror();
extern struct object *makemain();
extern struct intervaldomain *makedomain();

static int memerror = 0;

/*
 *	C O N S T R U C T  --  Construct woolz obj from memory
 *
 *	Uses two passes through the frame store , firstly to find out how
 *	many pixels will be in the object and how many intervals will
 *	contain them, and the second pass will fill out the extents of the
 *	intervals, and fill out the grey values. The object returned contains a
 *	type 21 interval structured grey table. This is filled out at the same
 *	time as the interval records themselves.
 *
 *	Input:	addr			- Pointer to top left of region
 *			xsize, ysize	- the number of lines and columns to scan
 *			pitch			- increment to move down 1 line
 *			thresh			- the threshold value
 *			invert			- indicates whether to invert grey values so
 *								white is zero.
 *
 *	Returns:	Object pointer
 */
struct object *
construct(unsigned char *addr, int xsize, int ysize, int pitch, 
          unsigned char thresh, int invert)
{
	struct object *obj;
	int intcount, greycount;
/*	int		(*old_ex)();*/

/*	old_ex = (int (*)()) Alloc_exit(memerrfunc);*/

/*	fprintf(stderr,"construct: addr 0x%x, size %d %d, pitch %d, thresh %d, inv %d\n",
			addr,xsize, ysize, pitch, thresh, invert); */
	if (invert)
		greycount = count_low(addr,xsize,ysize,pitch,thresh,&intcount);
	else
		greycount = count_high(addr,xsize,ysize,pitch,thresh,&intcount);
/*	fprintf(stderr,"construct: first scan complete greycount %d, intcount %d\n",greycount,intcount); */

	if (greycount > 0) {
		obj = cmakeobj(xsize,ysize,intcount,greycount);
		if (obj != NULL) {
/*			fprintf(stderr,"construct: obj made\n"); */
			if (invert)
				build_high(obj,addr,pitch,0,thresh);
			else
				build_high(obj,addr,pitch,1,thresh);
		}
	} else
		obj = makemain(1,makedomain(1,1,0,1,0),NULL,NULL,NULL);
/*	Alloc_exit(old_ex);*/
	return(obj);
}					

/*
 *	C M A K E O B J  --  Build up object data structure
 *
 *	Obtains space for interval domain and grey table and builds object
 *
 *	Inputs:	lines, cols - number of lines and columns
 *			intcount - number of intervals there will be
 *			greycount - no of pixels
 *
 *	Returns:	Object pointer
 */
static struct object *
cmakeobj(int cols, int lines, int intcount, int greycount)
{
	struct ivtable *ivtable;
	struct intervaldomain *idom;

	ivtable = (struct ivtable *) Calloc( sizeof(struct ivtable) +
									lines * sizeof(struct valintline) +
									intcount * sizeof(struct valueline) , 1);
	if (checkerror())
		return(NULL);
	ivtable->type = 21;
	ivtable->freeptr = (char *) Calloc(greycount, sizeof(GREY) );
	ivtable->linkcount = 0;		/* will be incremented by a makemain */
	if (checkerror()) {
		Free(ivtable);
		return(NULL);
	}
	ivtable->line1 = 0;
	ivtable->lastln = lines - 1;
	ivtable->bckgrnd = 0;
	ivtable->vil = (struct valintline *) (ivtable + 1);
	ivtable->original = NULL;

	idom = makedomain(1, 0, lines-1, 0, cols-1);
	idom->freeptr = (char *) Malloc(intcount * sizeof(struct interval));
	idom->linkcount = 0;			/* will be incremented by a makemain */
	if (checkerror()) {
		Free(idom);
		Free(ivtable->freeptr);
		Free(ivtable);
		return(NULL);
	}

	return(makemain(1, idom, (struct valuetable *)ivtable, NULL, NULL));
}

/*
 *	C O U N T _ H I G H  --  count intervals and above threshold pixels
 *
 *	count number of pixels which are above or equal to the threshold value,
 *	and counting the number of intervals (series of above threshold pixels).
 *	Designed for 8 bit operation.
 *
 */
int
count_high(register unsigned char *addr, int xsize, int ysize, int pitch, 
           register unsigned char thresh, int *intcount)
{
	register int colno, totcount, ininterval;
	int line, lineinc;
//	register unsigned char *chr;
	
	*intcount = 0;
	totcount = 0;
	
/*	fprintf(stderr,"Count high, thresh %d\n",thresh); */
	lineinc = pitch - xsize;
	for (line=0; line<ysize; line++) {
		ininterval = FALSE;		/* initialise to false at line start */
		colno = xsize;
		while (colno) {
			if (*(addr++) >= thresh) {
				totcount++;
				ininterval = TRUE;
			} else {
/*
 * if we were in an interval but current is below thresh, then one more interval
 */
				if (ininterval)
					(*intcount)++;
				ininterval = FALSE;
			}
			colno--;
		}
/* at end of line see if an interval is in progress, if so inc count */
		if (ininterval)
			(*intcount)++;
		addr += lineinc;
	}
	return(totcount);
}

/*
 *	C O U N T _ L O W  --  count intervals and below threshold pixels
 *
 *	as count_high but in this case white is high (255)
 *	and so below threshold values are searched for
 */
int
count_low(register unsigned char *addr, int xsize, int ysize, int pitch, 
          register unsigned char thresh, int *intcount)
{
	register int colno, totcount, ininterval;
	int line, lineinc;
//	register unsigned char *chr;
	
	*intcount = 0;
	totcount = 0;
	
	lineinc = pitch - xsize;
	for (line=0; line<ysize; line++) {
		ininterval = FALSE;		/* initialise to false at line start */
		colno = xsize;
		while (colno) {
			if (*addr++ < thresh) {
				totcount++;
				ininterval = TRUE;
			} else {
/*
 * if we were in an interval but current is below thresh, then one more interval
 */
				if (ininterval)
					(*intcount)++;
				ininterval = FALSE;
			}
			colno--;
		}
/* at end of line see if an interval is in progress, if so inc count */
		if (ininterval)
			(*intcount)++;
		addr += lineinc;
	}
	return(totcount);
}


/*
 *	B U I L D _ H I G H  --  mark interval end points and collect grey values
 *
 *	Description:	performs the second pass of the data, to fill
 *					all the grey values in the grey table, and the
 *					coordinate positions of the interval structures for
 *					both the interval and  values domain.  The values
 *					domain could be more easily but less efficiently
 *					filled out at the end of the scan by makeivtable.
 *
 *	Inputs:
 *		obj		- object pointer. the object contains a suitably sized
 *					interval domain, and value domain, but with none of
 *					the relevant line entries filled out. the interval
 *					domain also contains a valid but unused interval
 *					list, and the value domain contains a suitable
 *					length grey table.
 *		addr	- Pointer to data
 *		pitch	-
 *		thresh	- The threshold value
 *
 *	Exit:			All remaining object fields are completed
 *
 */
static void
build_high(struct object *obj, unsigned char *addr, int pitch, int high, 
           register unsigned char thresh)
{
	struct intervaldomain *idom;
//	struct intervalline *intvline;
	register struct interval *intv;
	struct interval *intvlinestart;
	struct ivtable *sbivtable;		/* grey version of idom */
	struct valintline *sbvil;		/* grey version of intvline */
	struct valueline *sbval;		/* grey version of intv */
	register GREY *g;
	register unsigned char *chr;
	register int colno,lastabove,intvcount;
	int lineno, lastln,lastkl, lineinc;
/* 	int cols; */
	
	/* set up data structures from supplied object . This routine is assumed 
	to be internal to this module, so no explicit object checks are necessary */
	
	idom = obj->idom;
	lastln = idom->lastln;
	lastkl = idom->lastkl;
	intv = (struct interval *) idom->freeptr;
	sbivtable = (struct ivtable *) obj->vdom;
	sbvil = sbivtable->vil;
	sbval = (struct valueline *) (sbvil + lastln - idom->line1 + 1);
	g = (GREY *) sbivtable->freeptr;

/*	fprintf(stderr,"construct: build: last %d line1 %d, pitch %d\n",
			lastln, idom->line1, pitch);
	fprintf(stderr,"construct: build: high %d\n",high); */
	chr = addr + (lastln - idom->line1) * pitch;	/* this is to speed up the scan */
		
/*
 *	We work backwards up the image: line numbers increment up the screen, but
 *	frame memory increments down the screen, so to get to beginning of the next
 *	line we have to do some odd maths...
 */
	lineinc = pitch + (lastkl - idom->kol1 + 1);

	/*	The outer loop below is the line loop, and will result in the 
	intervalline structure being completed and incremented once. The same
	applies to the valintline structure. */
	
	for (lineno = idom->line1; lineno <= lastln; lineno++ ) {
/*		fprintf(stderr,"construct: build: line %d, ptr 0x%x, ints %d, greys %d\n",
				lineno,chr,intv - (struct interval *) idom->freeptr,g-(GREY *) sbivtable->freeptr); */
		sbvil->vtbint = sbval;		/* grey interval line start */
		intvlinestart = intv;		/* record this for makeinterval(line) */
		intvcount = 0;
		lastabove = FALSE;			/* cant be mid interval at line start */
		
		if (high) {
			for (colno = idom->kol1; colno <= lastkl; colno++ ) {
				if (*chr >= thresh) {		/* is current above threshold */
/*
 * if previous was below then mark new interval start, else
 * just add current grey value to grey table
 */
					if (lastabove == FALSE) {
						intv->ileft = colno;
						sbval->vkol1 = colno;
						sbval->values = g;		/* record this at start of each interval */
						lastabove = TRUE;
					}
					*g++ = *chr;
				} else {					/* current below threshold */
					/* if previous was above then that was the end of an interval
					so mark it up, otherwise just ignore the current value */
					if (lastabove) {
						intv->iright = sbval->vlastkl = colno - 1;
						lastabove = FALSE;
						sbval++;		/* incr to next grey interval */
						intv++;			/* incr to next interval +*/
						intvcount++;	/* total for current line */
					}
				}
/* 				*chr = 0x7f; */
				chr++;
			}
		} else {
			for (colno = idom->kol1; colno <= lastkl; colno++ ) {
				if (*chr < thresh) {	/* is current below threshold */
					/* if previous was below then mark new interval start, else
					just add current grey value to grey table */
				
					if (lastabove == FALSE) {
						intv->ileft = colno;
						sbval->vkol1 = colno;
						sbval->values = g;		/* record this at start of each interval */
						lastabove = TRUE;
					}
					*g++ = 255 - *chr;
				} else {					/* current above threshold */
					/* if previous was below then that was the end of an interval
					so mark it up, otherwise just ignore the current value */
				
					if (lastabove) {
						colno--;
						intv->iright = colno ;	/* previous char was the end */
						sbval->vlastkl = colno ;
						colno++;
						lastabove = FALSE;
						sbval++;		/* incr to next grey interval */
						intv++;			/* incr to next interval +*/
						intvcount++;	/* total for current line */
					}
				}
				chr++;
			}
		}
		/* now at the end of line , if an interval is current at this point
		then terminate it, this being the second interval end condition */
			
		if (lastabove == TRUE) {
			colno--;
			intv->iright = colno ;
			sbval->vlastkl = colno ;
			colno++;
			sbval++;
			intv++;
			intvcount++;
		}
		/* can now complete the interval line and valintline structures and
		increment to next line */
		
		sbvil->nintvs = intvcount;
		makeinterval(lineno, idom, intvcount, intvlinestart);
		sbvil++;		/* only need to incr valintline, other is recalculated eacc h line */

		chr -= lineinc;
	}	
}

/*
 *	M E M E R R O R  --  Error routine to give to 'Alloc' to
 *							 trap memory allocation failures
 */
static
memerrfunc(int code, char *estr)
{
	memerror = 1;
}


static int
checkerror()
{
	int err;
	
	err = memerror;
	memerror = 0;	/* reset this ready for next request */
	return(err);
}

