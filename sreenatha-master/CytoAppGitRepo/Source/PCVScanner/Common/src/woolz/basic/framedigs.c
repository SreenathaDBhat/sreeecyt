/* framedigs.c 		7/3/86		Denis Rutovitz
 *
 * subroutines for transforming frame to digs coordinates
 * and inversely, digs to frame coordinates
 *
 * Modifications
 *
 *	9/20/96		BP:		Replace register parameters with register int!!!!
 *
 *	24 Jun 1992	MG		Modified limxdigfrm & limydigfrm to used getcurdg (in DDGS)
 *	16 Aug 1987	CAS		Hacked a temp fix limit checking..
 *	 2 aug	1987	BDP		Changed limit checks for larger virtuoso board
 */
 
#include "woolz.h"

#include <ddgs.h>

#include <stdio.h>
//#include <wstruct.h>
//#include <ddgs.h>


#define BIGNUM 60000


/* calculate frame distance in col-direction */
#ifdef WIN32
int
frkdis(register int x, register struct pframe *f)
#endif
#ifdef i386
int
frkdis(x, f)
register int x;
register struct pframe *f;
#endif
{
	register int xs;
	xs = f->ix*f->scale;
	return ( x*xs );	
}
/* calculate inverse frame distance in line-direction */
#ifdef WIN32
int
frldis(register int y, register struct pframe *f)
#endif
#ifdef i386
int
frldis(y, f)
register int y;
register struct pframe *f;
#endif
{
	register int ys;
	ys = f->iy*f->scale;
	return ( y*ys) ;	
}
/* calculate inverse frame distance in col-direction */
#ifdef WIN32
int
frkvdis(register int x, register struct pframe *f)
#endif
#ifdef i386
int
frkvdis(x, f)
register int x;
register struct pframe *f;
#endif
{
	register int xs;
	xs = f->ix*f->scale;
	if (!xs) xs = 1;
	return ( (int) ( x/xs) );	
}
/* calculate inverse frame distance in line-direction */
#ifdef WIN32
int
frlvdis(register int y, register struct pframe *f)
#endif
#ifdef i386
int
frlvdis(y, f)
register int y;
register struct pframe *f;
#endif
{
	register int ys;
	if (!ys) ys = 1;
	ys = f->iy*f->scale;
	return ( (int) ( y/ys) );	
}
#ifdef WIN32
int
showframe(struct pframe *fr)
#endif
#ifdef i386
int
showframe(fr)
struct pframe *fr;
#endif
{
	printf( "Frame %d \n",fr);
	printf(
"fr->dx %d fr->dy %d fr->ox %d fr->oy %d fr->ix %d fr->iy %d fr->scale %d\n",
fr->dx , fr->dy , fr->ox , fr->oy , fr->ix , fr->iy , fr->scale );
}
/* calculate digs coordinates, given frame and position */
#ifdef WIN32
int
frdigsxpos(register int x, register struct pframe *f)
#endif
#ifdef i386
int
frdigsxpos(x, f)
register int x;
register struct pframe *f;
#endif
{
	register int xs,xo;
	
	xs = f->ix * f->scale;
	xo = f->dx - xs * f->ox;
	return (xo + xs*x);
}
#ifdef WIN32
int
frdigsypos(register int y, register struct pframe *f)
#endif
#ifdef i386
int
frdigsypos(y, f)
register int y;
register struct pframe *f;
#endif
{
	register int ys,yo;
	
	ys = f->iy * f->scale;
	yo = f->dy - ys * f->oy;
	return (yo + ys*y);
}
/* calculate frame coordinates, given digs coordinates */
#ifdef WIN32
short
frinvxdig(register int xv, struct pframe *f)
#endif
#ifdef i386
short
frinvxdig(xv, f)
register int xv;
struct pframe *f;
#endif
{
	register int xs,xo;
	register int x;
	xs = f->ix* f->scale;
	xo = f->dx - xs* f->ox;
	if ( !xs ) {
		if (xv > xo ) x = - BIGNUM;
		else x = BIGNUM;
	} else x = (xv - xo ) / xs ;
	return(x);
}
#ifdef WIN32
short
frinvydig(register int yv, struct pframe *f)
#endif
#ifdef i386
short
frinvydig(yv, f)
register int yv;
struct pframe *f;
#endif
{
	register int ys,yo;
	register int y;
	ys = f->iy* f->scale;
	yo = f->dy - ys* f->oy;
	if ( !ys ) {
		if (yv > yo ) y = - BIGNUM;
		else y = BIGNUM;
	} else y = (yv - yo ) / ys ;
	return(y);
}
/* enforce digs limits on frame coordinates */
#ifdef WIN32
int
limxdigfrm(register int x, register struct pframe *f)
#endif
#ifdef i386
int
limxdigfrm(x, f)
register int x;
register struct pframe *f;
#endif
{
	register int xtemp;
	int WINDMINX, WINDMINY, WINDMAXX, WINDMAXY;
	DDGS *dgb;
	dgb = getcurdg();

	WINDMINX= (dgb->minx<<dgb->xshift);
	WINDMINY= (dgb->miny<<dgb->yshift);
	WINDMAXX= (((dgb->maxx+1)<<dgb->xshift)-1);
	WINDMAXY= (((dgb->maxy+1)<<dgb->yshift)-1);
	xtemp = frdigsxpos(x,f);
	if ( xtemp > WINDMAXX )  x = frinvxdig(WINDMAXX , f);
	else if ( xtemp < WINDMINX )  x = frinvxdig(WINDMINX , f);
	return(x);
}
#ifdef WIN32
int
limydigfrm(register int y, register struct pframe *f)
#endif
#ifdef i386
int
limydigfrm(y, f)
register int y;
register struct pframe *f;
#endif
{
	register int ytemp;
	int WINDMINX, WINDMINY, WINDMAXX, WINDMAXY;
	DDGS *dgb;
	dgb = getcurdg();

	WINDMINX= (dgb->minx<<dgb->xshift);
	WINDMINY= (dgb->miny<<dgb->yshift);
	WINDMAXX= (((dgb->maxx+1)<<dgb->xshift)-1);
	WINDMAXY= (((dgb->maxy+1)<<dgb->yshift)-1);
	ytemp = frdigsypos(y,f);
	if ( ytemp > WINDMAXY )  y = frinvydig(WINDMAXY , f);
	else if ( ytemp < WINDMINY )  y = frinvydig(WINDMINY , f);
	return(y);
}

