
/*
 * grab_buffer.c		MG sometime in 93, I guess... (BP)
 *
 * Management of static buffers for drawing etc.
 *
 * Modifications:
 *
 *	10Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *					Also, made minor mods to stop compiler warnings and removed
 *					standard C parameter lists (were '#ifdef i386' only).
 *	dcb	17Nov99		Redirect grab buffer error messages to stderr
 *	dcb	11Oct99		Fix ungrab_buffer() too !
 *	dcb	08Oct99		grab_buffer() - Fix block_grabbed check and allocation ranges
 *	KS	27Jul99		Force width and height of grab buffers to be bigger (1400x1050) than
 *					the currently biggest captured images.
 *	dcb	25Jun99		Add image width & height parameters to init_grab_buffer
 *	BP	12/10/94:	Added this header.
 *					Use Malloc, not Calloc.
 *
 */

#include <woolz.h>

#include <stdio.h>
#include <malloc.h>
//#include <wstruct.h>

/*
#define DEBUG
*/

unsigned char *GrabBuffer=NULL;
static int maxblocks = 0;
static int gb_width;
static int gb_height;
static int block_grabbed[6];

#define BIGGEST_WIDTH 2024
#define BIGGEST_HEIGHT 2024
	
/* 
	GrabBuffer

	Used by 8 and 24 bit drawing routines, and capture routines as
   	temporary storage area rather than malloc'ing and free'ing 
	memory on the fly.

	GrabBuffer is allocated once at the start of Cytovision.

	Less memory is allocated if probe imaging is disabled.

	Routines

*/

unsigned char *
init_grab_buffer(int nblocks, int width, int height)
{
	int b;

	maxblocks = nblocks;
	gb_width = width;
	gb_height = height;

	if (width  < BIGGEST_WIDTH) width = BIGGEST_WIDTH;
	if (height < BIGGEST_HEIGHT) height = BIGGEST_HEIGHT;

	GrabBuffer = (unsigned char *)Malloc(maxblocks * width * height);

	if (GrabBuffer == NULL) {
		fprintf(stderr, "init_grab_buffer: GrabBuffer could not be allocated\n");
		maxblocks = 0;
	}

	/* mark all GrabBuffer blocks as ungrabbed */
	for (b = 0; b < maxblocks; b++) {
		block_grabbed[b] = 0;
	}

	return(GrabBuffer);
}


/*
 	GRAB_BUFFER

	Grabs nblocks of GrabBuffer starting and start_block
	- each block is gb_width x gb_height bytes.

	If successful returns a pointer to the start block in GrabBuffer
	otherwise NULL.

	These blocks cannot be grabbed again unless they are ungrabbed
	- see ungrab_buffer()

*/

unsigned char *
grab_buffer(int start_block, int nblocks)
{
	int b;
	unsigned char *addr;

	if (GrabBuffer==NULL) {
		fprintf(stderr, "grab buffer: GrabBuffer has not been allocated\n");
		return(NULL);
	}

	if (start_block+nblocks > maxblocks) {
		fprintf(stderr, "grab_buffer : not enough blocks available\n");
		fprintf(stderr, "start_block=%d, nblocks=%d, maxblocks=%d\n",
			start_block,nblocks,maxblocks);
		return(NULL);
	}

	for (b = start_block; b < (start_block + nblocks);b++) {
		if (block_grabbed[b]) {
			fprintf(stderr, "grab_buffer : not enough free blocks\n");
			return(NULL);
		}
	}
	
	for (b = start_block; b < (start_block + nblocks);b++) {
		block_grabbed[b]=1;
	}

#ifdef DEBUG
	fprintf(stderr, "grab_buffer : start_block=%d, nblocks=%d, gb_width = %d, gb_height = %d\n",
		start_block, nblocks, gb_width, gb_height);
#endif

	addr = GrabBuffer + (gb_width * gb_height * start_block);
	return addr;
}


/*
 	UNGRAB_BUFFER

	Releases nblocks of GrabBuffer starting and start_block
	- each block is gb_width x gb_height bytes.

	Always returns NULL.

	These blocks can now be grabbed again.

*/
unsigned char *
ungrab_buffer(int start_block, int nblocks)
{
	int b;

	if (GrabBuffer==NULL) {
		fprintf(stderr, "ungrab buffer: GrabBuffer has not been allocated\n");
	}

	if (start_block+nblocks > maxblocks) {
		fprintf(stderr, "ungrab_buffer : too many blocks specified\n");
		fprintf(stderr, "start_block=%d, nblocks=%d, maxblocks=%d\n");
	}

	for (b = start_block; b < (start_block + nblocks); b++) {
		block_grabbed[b]=0;
	}
#ifdef DEBUG
fprintf(stderr, "ungrab_buffer : start_block=%d, nblocks=%d\n",start_block,nblocks);
#endif
	
	return(NULL);
}
