/*
 *	INVERTOBJVER.C	9/6/93	 Mike Castling
 *
 *	180 degree woolz object inversion (T->B)
 *
 *	Modifications: 
 *
 *	10Mar2000		JMB	Changed headers included, as part of conversion to DLL.
 *						Also, made minor mods to stop compiler warnings and removed
 *						standard C parameter lists (were '#ifdef i386' only).
 *	11/6/93
 *	4/8/93	        MG added Free(co_ords)
 *	6/8/93	        MC set 'plist arg.' of makemain() to NULL
 *
 *	Note :- the idom (type 1) lines are interval structured
 *		the vdom (type 1) lines are are not.
 */

#include <woolz.h>

#include <stdio.h>
#include <time.h>
#include <malloc.h>
//#include <wstruct.h>

/* #define DEBUG
#ifdef  DEBUG
	FILE *dbfile;
	struct tm *gm;
	time_t t;
#endif */

/* INVIDOM :- invert idom co-ords (top->bottom) */
/* this program is in fact based on copydomain() written by bdp */
/* note its possible for nintvs = 0 in middle of a object (joined split objects with gap) */
#ifdef WIN32
struct intervaldomain *
invidom_ver(struct intervaldomain *idom, int *co_ords)
#endif
#ifdef i386
struct intervaldomain *
invidom_ver(idom, co_ords)
struct intervaldomain *idom;
int *co_ords;
#endif
{
struct intervaldomain *jdom, *makedomain();
register struct interval *itvl, *jtvl, *ktvl;
struct intervalline *ivln;
int fin, num = 0;
register /*i,*/l, n = 0;

	/* protection added checks firstly that supplied domain exists, if its
	NULL then NULL is returned. If an empty object is supplied then its still
	copied, but returned before any further operations. */
	if (idom == NULL)
		{
		return(NULL);
		}

	jdom = makedomain(1,idom->line1,idom->lastln,idom->kol1,idom->lastkl);
	if (wzemptyidom(idom) > 0)
		{
		return(jdom);	/* return copy of the empty domain */
		}

/* #ifdef DEBUG
	fprintf(dbfile,"\nINTERVAL DOMAIN : original non inverted idom values \n");
#endif DEBUG */

	ivln = idom->intvlines;
	for (l = idom->line1; l <= idom->lastln; l++) 
		{
		n += ivln->nintvs;

/* #ifdef DEBUG
	fprintf(dbfile,"line %d ivln->nintvs %d ",l,ivln->nintvs);
	itvl = ivln->intvs;
	for (i = 0; i < ivln->nintvs; i++)
		{
		fprintf(dbfile,": l %d r %d ",itvl->ileft,itvl->iright);
		itvl++;
		}
	fprintf(dbfile,"\n");
#endif DEBUG */

		ivln++;
		}
	ivln--; /* essential, otherwise ivln would point past end of idom */

/* #ifdef DEBUG
	fprintf(dbfile,"Total no. of intervals = %d\n\n",n);
	fprintf(dbfile,"inverted idom values (T->B) \n");
#endif DEBUG */

	jtvl = itvl = (struct interval *)Malloc(n * sizeof(struct interval)); /* used to be 2 * n ?? */
	jdom->freeptr = (char *) itvl;
	jdom->linkcount = 0;
	for (l = idom->line1; l <= idom->lastln; l++)
		{
		fin = 0;
		ktvl = ivln->intvs;
		for (n = 0; n < ivln->nintvs; n++)
			{
			jtvl->ileft = ktvl->ileft;
			jtvl->iright = ktvl->iright;

/* #ifdef DEBUG
	fprintf(dbfile,"line %d kl %d kr %d ",l,ktvl->ileft,ktvl->iright);
	fprintf(dbfile,"jl %d jr %d \n",jtvl->ileft,jtvl->iright);
#endif DEBUG */

			jtvl++; /* work towards end of line */
			if (!fin)
				{ /* on each line store 1st idom start co-ord (for vdom invert) */
				co_ords[num] = ktvl->ileft;
				fin = 1;
				}
			ktvl++;
		}
		makeinterval(l,jdom,ivln->nintvs,itvl);
		itvl += ivln->nintvs;
		ivln--;
		num++; /* num MUST be inc on every line (whether nintvs = 0 or not) */
	}
	return(jdom);
}

/* 
 * INVVDOM_VER (invert vdom co-ords (top->bottom)
 */
#ifdef WIN32
struct valuetable *
invvdom_ver(struct object *invobj, struct object *obj, int *co_ords)
#endif
#ifdef i386
struct valuetable *
invvdom_ver(invobj, obj, co_ords)
struct object *invobj;
struct object *obj;
int *co_ords;
#endif
{
struct valuetable *vdom, *wdom, *newvaluetb();
struct valueline **vline, **wline;
GREY *gptr;
int line, i, num, offset, index = 0, val;

	vdom = obj->vdom;

/* #ifdef DEBUG
	fprintf(dbfile,"\nVALUE DOMAIN ");
	fprintf(dbfile,"vdom->line1 %d vdom->lastln %d\n",vdom->line1,vdom->lastln);
	vline = vdom->vtblines;
	fprintf(dbfile,"original non inverted vdom values ");
	for (line = vdom->line1; line <= vdom->lastln; line++)
		{
		fprintf(dbfile,"\nline %d l %d r %d values :",line,(*vline)->vkol1,(*vline)->vlastkl);
		for (i = 0; i <= ((*vline)->vlastkl - (*vline)->vkol1); i++)
			{
			fprintf(dbfile," %d",(*vline)->values[i]);
			}
		vline++;
		}
#endif DEBUG */

	vline = vdom->vtblines;
	wdom = newvaluetb(invobj,1,vdom->bckgrnd); /* wdom will contain inverted values */
	if (wdom == NULL)
		{
		return(NULL);
		}

	/* set vline to point to last line of vdom, with VALID values */
	line = vdom->line1;
	while (line < wdom->lastln)
		{
		vline++;
		line++;
		}

	wline = wdom->vtblines;
	gptr = (GREY *)wdom->freeptr;
	/* place values into wdom */
	for (line = wdom->line1; line <= wdom->lastln; line++)
		{
		num = (*wline)->vlastkl - (*wline)->vkol1 + 1; /* wline ! */
		offset = obj->idom->kol1 + co_ords[index] - (*vline)->vkol1;
		for (i = 0; i < num; i++)
			{
			val = offset + i;
			if (co_ords[index] != -1)
				{ /* = -1 when nintvs = 0 ie gap between objects */
				*gptr++ = (*vline)->values[val];       /* vline ! */
				}
			}
		vline--; wline++; index++;
		}

/* #ifdef DEBUG
	fprintf(dbfile,"\n\nafter wdom contents (inverted vdom values, T->B ");
	fprintf(dbfile,"wdom->line1 %d wdom->lastln %d",wdom->line1,wdom->lastln);
	wline = wdom->vtblines;
	index = 0;
	for (line = wdom->line1; line <= wdom->lastln; line++)
		{
		fprintf(dbfile,"\nline %2d l %d r %d ",line,(*wline)->vkol1,(*wline)->vlastkl);
		fprintf(dbfile,"co_ords[] %d values : ",co_ords[index++]);
		for (i = 0; i <= ((*wline)->vlastkl - (*wline)->vkol1); i++)
			{
			fprintf(dbfile," %d",(*wline)->values[i]);
			}
		wline++;
		}
#endif DEBUG */

	wdom->linkcount = 1; 
	return(wdom);
}

#ifdef WIN32
struct object *
invertobject_ver(struct object *obj)
#endif
#ifdef i386
struct object *
invertobject_ver(obj)
struct object *obj;
#endif
{
struct object *invobj, *makemain();
struct intervaldomain *idom;
//struct valuetable *vdom;
int objheight,i;
int *co_ords;

	if (obj->idom->type != 1 || obj->vdom->type != 1)
		{
		return(NULL);
		}

/* #ifdef DEBUG
	if ((dbfile = fopen("invfile_ver","w")) == NULL)
		{
		fprintf(stderr,"invfile could not be opened \n");
		exit();
		}
	t = time(NULL);
	gm = gmtime(&t);
	fprintf(dbfile,"%s\n",asctime(gm));
	fprintf(dbfile,"invfile_ver : o/p from invertobject_ver() function \n");
	fprintf(dbfile,"obj = %d \n",obj);
	fprintf(dbfile,"obj->idom = %d \n",obj->idom);
	fprintf(dbfile,"obj->idom->type = %d \n",obj->idom->type);
	fprintf(dbfile,"obj->idom->line1 = %d \n",obj->idom->line1);
	fprintf(dbfile,"obj->idom->kol1 = %d \n",obj->idom->kol1);
	fprintf(dbfile,"obj->idom->lastln = %d \n",obj->idom->lastln);
	fprintf(dbfile,"obj->idom->lastkl = %d \n",obj->idom->lastkl);
	fprintf(dbfile,"obj->idom->linkcount = %d \n",obj->idom->linkcount);
	fprintf(dbfile,"obj->idom->freeptr = %d \n",obj->idom->freeptr);

	fprintf(dbfile,"obj->vdom->type = %d \n",obj->vdom->type);
	fprintf(dbfile,"obj->vdom->line1 = %d \n",obj->vdom->line1);
	fprintf(dbfile,"obj->vdom->lastln = %d \n",obj->vdom->lastln);
	fprintf(dbfile,"obj->vdom->linkcount = %d \n",obj->vdom->linkcount);
	fprintf(dbfile,"obj->vdom->freeptr = %d \n",obj->vdom->freeptr);

	fprintf(dbfile,"obj->plist = %d \n",obj->plist);
#endif DEBUG */

	objheight = obj->idom->lastln - obj->idom->line1 + 1;
	co_ords = (int *)Malloc(objheight * sizeof(int));
	for (i = 0; i < objheight; i++)
		{
		co_ords[i] = -1;
		}

	/* invert intervaldomain */
	idom = invidom_ver(obj->idom,co_ords);
	if (idom == NULL)
		{
		return(NULL);
		}

	invobj = makemain(1,idom,NULL,NULL,NULL);
	invobj->vdom = invvdom_ver(invobj,obj,co_ords);
	if (invobj->vdom == NULL)
		{
		freeobj(invobj);
		return(NULL);
		}

/* #ifdef DEBUG
	fprintf(dbfile,"\n\nidom %d \n",idom);
	fprintf(dbfile,"invobj %d \n",invobj);
	fprintf(dbfile,"invobj->idom %d \n",invobj->idom);
	fprintf(dbfile,"invobj->vdom %d \n",invobj->vdom);
	fclose(dbfile);
#endif DEBUG */

	Free(co_ords);		/* added by MG 4/8/93 */

	return(invobj);
}

