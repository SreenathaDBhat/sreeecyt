/*
 *      M O V E . C  --  Basic object moving stuff
 *
 *
 *  Written: Clive A Stubbings
 *           Image Recognition Systems
 *           720 Birchwood Boulevard
 *           Birchwood Science Park
 *           Warrington
 *           Cheshire
 *           WA3 7PX
 *
 *	Copyright (c) and intellectual property rights Image Recognition Systems (1988)
 *
 *  Date:    15th August 1988
 *
 * Mods:
 *
 *	9/20/96		BP:		Returnm type NULL for moveidom/movevdom.
 *
 *	BP	5/6/96:		Lets at least check for NULL domains...!!
 *	2 Mar 1993	MG	added moveobject
 *
 */

#include "woolz.h"

//#include <wstruct.h>


/*
 *	M O V E I D O M  --
 *
 */
#ifdef WIN32
void
moveidom(struct intervaldomain *idom, int x, int y)
#endif
#ifdef i386
void
moveidom(idom, x, y)
struct intervaldomain *idom;
int x;
int y;
#endif
{
	if (!idom)
		return;

	idom->kol1 += x;
	idom->lastkl += x;
	idom->line1 += y;
	idom->lastln += y;
}

/*
 *	M O V E V D O M  --
 *
 */
#ifdef WIN32
void
movevdom(struct valuetable *vdom, int x, int y)
#endif
#ifdef i386
void
movevdom(vdom, x, y)
struct valuetable *vdom;
int x;
int y;
#endif
{
	int		height;
	struct	valueline **vline;

	if (!vdom)
		return;

	height = vdom->lastln - vdom->line1 + 1;
	vdom->line1 += y;
	vdom->lastln += y;
	switch (vdom->type) {
		case 1:
			vline = vdom->vtblines;
			while (height--) {
				(*vline)->vkol1 += x;
				(*vline)->vlastkl += x;
				vline++;
			}
			break;

		case 11:
			((struct rectvaltb *)vdom)->kol1+=x;
			break;

		default:
			break;
	}
}


/* 
 * MOVEOBJECT ( ****** GENERATES NEW WOOLZ OBJECT ******** )
 */
#ifdef WIN32
struct object *
moveobject(struct object *obj, int x, int y)
#endif
#ifdef i386
struct object *
moveobject(obj, x, y)
struct object *obj;
int x;
int y;
#endif
{
	struct object *moveobj, *duplicate_obj_plus();

	/* first duplicate original object - including any plist */
	moveobj=duplicate_obj_plus(obj);

	/* now move the object */
	moveidom(moveobj->idom,x,y);
	movevdom(moveobj->vdom,x,y);

	return(moveobj);
}

