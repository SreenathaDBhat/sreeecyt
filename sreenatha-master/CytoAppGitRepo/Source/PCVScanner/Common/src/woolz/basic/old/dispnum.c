/*
 * dispnum.c	Jim Piper	10 August 1983
 *
 * Modifications
 *
 *	9/19/96		BP:		Fix function return type.
 *
 *	BP	7/20/95:	Compiler warnings fixed.
 *	12 Sep 1986		CAS		Includes
 */

#include <stdio.h>
#include <wstruct.h>

#ifdef WIN32
void
dispnum(int num, int x, int y)
#endif
#ifdef i386
void
dispnum(num, x, y)
int num;
int x;
int y;
#endif
{
	char s[10];

#ifdef C68000

	int l;
	l = decode(s,10,"%d",num);
	s[l] = '\0';

#else

	sprintf(s,"%d",num);

#endif

	dispstring(s,x,y);
}
