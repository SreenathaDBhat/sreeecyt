/*
 * dispstring.c	Jim Piper	10 August 1983
 *
 * Modifications
 *
 *	12 Sep 1986		CAS		Includes
 */

#include <stdio.h>
#include <wstruct.h> 

#ifdef WIN32
int
dispstring(char *s, int x, int y)
#endif
#ifdef i386
int
dispstring(s, x, y)
char *s;
int x;
int y;
#endif
{
	moveto(x,y);
	text(s);
}
