/*
 *	invertobj.h	3/3/93	Mike Castling
 *
 *	Modifications:
 *	
 *	9/19/96		BP:		These declarations are in basic.h for WIN32
 *
 */


#ifndef INVERTOBJ_H

#include <wstruct.h>

/***********************************************************************/
/*** The following routines are in invertobj.c ***/

#ifdef i386

struct object *invertobject(/* struct object *obj */);
struct intervaldomain *invidom(/* struct intervaldomain *idom,  struct interval **spareitv, int spc */);
struct valuetable *invvdom(/* struct object *obj */);

#endif

#define INVERTOBJ_H
#endif

