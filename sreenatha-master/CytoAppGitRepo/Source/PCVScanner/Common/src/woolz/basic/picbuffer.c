
/*
 * picbuffer.c	MG	based on set_obj_buffer by BP: 13/5/93
 *
 * Mods:
 *
 *	10Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *					Also, made minor mods to stop compiler warnings and removed
 *					standard C parameter lists (were '#ifdef i386' only).
 *	WH	09Jan98		Check that vdom isn't null before trying to dereference for values.
 *	WH	25Jun96:	Force image width to multiple of 4 in picrawbuffer() 
 *					as implemented in picbuffer().
 *	BP	3/21/96:	Fixed missing mask bits from backward interval order!
 *					Done by ORing mask bits into mask buffer, instead of
 *					just assigning.
 *	BP	5/22/95:	Make sure buffer is multiple of 4 pixels wide
 *					so that XShmPutImage can work.
 *	BP	9/20/94:	No more mallocing (now uses ddgs buffers).
 *	MG	23/6/93	Malloc buffer only once and clip drawing to buffer
 *	MG	 1/7/93 Added picrawbuffer() for rawimage objects
 */




/*
 * Functions to draw a type 1 object into a
 * rectangular buffer and create a single-
 * plane mask buffer from same.
 * This file supercedes an older version with
 * the same name which didn't work properly
 * with some objects (not my fault - woolz's).
 */

#include <woolz.h>

#include <ddgs.h>

#include <stdio.h>
#include <memory.h>
//#include <wstruct.h>
//#include <ddgs.h>




#define ABS(i)	(i >= 0 ? i : -i)

unsigned char *picbuff = NULL;
unsigned char *picmask = NULL;




/****************************************************** picbuffer */

/*
 * Traverse the object and draw it into an
 * 8-bit buffer.
 * Must be type 1 object.
 * The buffer is malloc'd here (a pointer to
 * it is returned) and must be freed when
 * finished with.
 * The return values are in screen pixel
 * coordinates, with origin at the top left.
 */

unsigned char *
picbuffer(struct object *obj, int *xout, int *yout, int *wout, int *hout, struct pframe *f, unsigned char * *maskbuffer)
{
	struct iwspace iwsp;
	struct gwspace gwsp;
	GREY *temp;
	unsigned char *pictemp, *pictempstart;
	unsigned picbuffend;
	int x, y, w, h, fullw, fullh, maskw;
	int abs_xs, abs_ys, xs, ys, lasty, lastlinpos;
	register short i, j;
	register int offset_inc, offset_big;
	register unsigned char maskbyte, maskbit;
	unsigned char *masktemp, *masktempstart;
	struct valuetable *vdom;
	int retval;
	DDGS *dg;
	int maxx,maxy;
	int offscreen = 0/*, firstline*/;
	int origfullw, origfullh, xorig, yorig, newy;
	int xtemp,wtemp;
	int ytoporig;
	int xoffset, yoffset;


	vdom = obj->vdom;

	xs = f->ix*f->scale;
	ys = f->iy*f->scale;

	abs_xs = ABS(xs);
	abs_ys = ABS(ys);

/* Calculate the width and height of the
 * required buffer. These are defined by
 * the limits of the idom. */
	fullw = (abs_xs*(1 + obj->idom->lastkl - obj->idom->kol1)) >> 3;
	fullh = (abs_ys*(1 + obj->idom->lastln - obj->idom->line1)) >> 3;
	origfullw=fullw;
	origfullh=fullh;

/* Get the position of the BOTTOM-LEFT
 * corner of the rectangle in SCALED-DOWN
 * dogs coordinates. */

	if (ys < 0)
		y = (f->dy - ys*f->oy + ys*obj->idom->lastln) >> 3;
	else
		y = (f->dy - ys*f->oy + ys*obj->idom->line1) >> 3;

	if (xs < 0)
		x = (f->dx - xs*f->ox + xs*obj->idom->lastkl) >> 3;
	else
		x = (f->dx - xs*f->ox + xs*obj->idom->kol1) >> 3;


	xorig=x;
	yorig=y;

	ytoporig=yorig+fullh;

/* check object is on screen, stop now if isn't */
	dg=getcurdg();
	maxx=dg->ddgsImageFormat.MaxX;	//dg->maxx;
	maxy=dg->ddgsImageFormat.MaxY;  //dg->maxy;

	if (x < 0)
	{
		fullw = fullw + x;
		offscreen = (fullw <= 0);
		x = 0;
	}
	else
		offscreen = (x > maxx);

	if (offscreen)
		return(NULL);

	if (y < 0)
	{
		fullh = fullh + y;
		offscreen = (fullh <= 0);
		y = 0;
	}
	else
		offscreen = (y > maxy);

	if (offscreen)
		return(NULL);

	if (fullw + x > maxx + 1)
		fullw = maxx - x + 1;

/* Force buffer to be multiple of 4 bytes wide. */
	fullw = (fullw + 3) & 0xFFFFFFFC;

	if (fullh + y > maxy + 1)
		fullh = maxy - y + 1;

/* calculate clipping offsets */
	if (ys > 0)
		yoffset = y - yorig;
	else
		yoffset = ytoporig - (y + fullh);

	xoffset = x - xorig;

/* Output TOP_LEFT corner position, width and height. */
	*xout = x;
	*yout = y+fullh;
	*wout = fullw;
	*hout = fullh;

/* Use ddgs drawing buffers. Note that too
 * much is allocated (h + 1) - this is necessary
 * to prevent occasional buffer overflow on
 * scaled-up objects, theres some rounding errors
 * in here somewhere... */
	picbuff = (unsigned char *)ddgs_get_aux_buffer(fullw*(fullh + 1));
	picmask = (unsigned char *)ddgs_get_draw_mask(fullw, fullh + 1);
	if (maskbuffer)
		*maskbuffer = picmask;
	picbuffend = (unsigned)picbuff + fullw*(fullh + 1);
	maskw = (fullw + 7) >> 3;

/* Fixed-point incrementer for scaling. */
	offset_inc = 64/xs;

/* linpos initialisation */
	lasty = -abs_ys/8;
	lastlinpos = obj->idom->line1 - 1;

/* Scan through object on a interval-by-
 * interval basis. */
	initrasterscan(obj, &iwsp, 0);

	if (vdom)
	{
		initgreywsp(obj, &iwsp, &gwsp, 0);
		retval = nextgreyinterval(&iwsp);
	}
	else
		retval = nextinterval(&iwsp);

	while (retval == 0)
	{
/* Height "above" first line. */
		y = (abs_ys*(iwsp.linpos - obj->idom->line1)) >> 3;
		newy = y;

/* Height depends on scale.
 * Don't recalculate if interval is
 * on same line as last one. */

		if (iwsp.linpos > lastlinpos) {
			if (iwsp.linpos - lastlinpos > 1) {
				/* handle empty intervals */
				lasty=(abs_ys*(iwsp.linpos - 1 - obj->idom->line1)) >> 3;
			}

			h = y - lasty;

if (abs_ys == 12)
	h++;

		}
				
		y=y-yoffset;	

		if (y<0) {
			lasty = newy;
			lastlinpos=iwsp.linpos;
		}

/* Height may be 0 when object
 * is scaled-down. In which case
 * ignore this interval.
 * Also ignore if interval is off screen */
		if ((h < 1) || (y<0) || (y>fullh)) {
			if (vdom!=NULL) 
				retval=nextgreyinterval(&iwsp);
			else
				retval=nextinterval(&iwsp);
			continue;
		}

/* Find start point in buffer. This
 * depends on the sign of yscale. If
 * negative, start from top of buffer
 * if positive, start from end and work
 * back up. These XXXXtempstart pointers
 * are used for convenience and point to
 * the first location in the particular
 * line in the buffer. X offsets are
 * therfore always relative to these. */
		if (ys < 0)
		{
			pictempstart = picbuff + (y*fullw);

			if (maskbuffer != NULL)
				masktempstart = (*maskbuffer) + (y*maskw);
		}
		else
		{
			pictempstart = picbuff + (fullw*(fullh - 1 - y));

			if (maskbuffer != NULL)
				masktempstart = (*maskbuffer) +
						(maskw*(fullh - 1 - y));
		}

/* Calculate distance of start of
 * interval from edge of buffer, and
 * the scaled width of the interval. */
		x = (abs_xs*(iwsp.lftpos - obj->idom->kol1)) >> 3;
		w = (abs_xs*iwsp.colrmn) >> 3;
		wtemp=w;

/* If xscale is negative we scan from
 * the end of the interval backwards. */
		if (xs < 0) {
			offset_big = (iwsp.colrmn << 3) - 1;
			xtemp=origfullw-x-w;
		}
		else {
			offset_big = 0;
			xtemp=x;
		}

/*check if interval in display box */

		x=xtemp-xoffset;

		offscreen=0;

		if (x<0) {
			w=w+x;
			if (w<=0)
				offscreen=1;
			else {
				/* update offset */
				if (xs < 0)
					offset_big += x*64/abs_xs;
				else 
					offset_big -= x*64/abs_xs;

				x=0;
			}
		}
		else
			offscreen=(x>fullw) ? 1 : 0;
		
		if (w+x>fullw)
			w=fullw-x;

/* if interval off screen - next interval */
		if (offscreen==1) {
			if (vdom!=NULL) 
				retval=nextgreyinterval(&iwsp);
			else
				retval=nextinterval(&iwsp);
			continue;
		}

/* If this interval is not on same
 * line as last one reset these bits. */
		if (iwsp.linpos > lastlinpos)
		{
			lasty = (abs_ys*(iwsp.linpos - obj->idom->line1)) >> 3;
			lastlinpos = iwsp.linpos;
		}

/* Point to first required location
 * in buffer. */
		pictemp = pictempstart + x;



/* Quick check that we're not going
 * beyond the buffer. This does happen
 * frequently with scaled-down objects,
 * but need not be considered a major
 * error - buffers are usually filled
 * upwards, so most of it will be done
 * by now. */
		if ((unsigned)pictemp < (unsigned)picbuff)
			return(picbuff);

/* This overflow happens very, very
 * rarely (if at all). */
		if ((unsigned)pictemp >= picbuffend - w)
			return(picbuff);

/* Calculate the byte position of the
 * first pixel in the mask buffer. Also
 * find its offset within the byte.
 * Note: at the start of each interval we
 * read the existing mask buffer value
 * for the start byte in case a previous
 * interval finished in the same byte. */
		if (maskbuffer != NULL)
		{
			masktemp = masktempstart + (x >> 3);
			maskbit = 1 << (x & 0x07);
			maskbyte = *masktemp;
		}


/* For each point along SCALED interval. */
		for (i = 0; i < w; i++)
		{
			if (vdom != NULL)
			{
/* Get pointer to grunt value. Note
 * fixed-point scaling is x8. */
				if(vdom->freeptr)
				{
					temp = gwsp.grintptr + (offset_big >> 3);

					*pictemp++ = *temp;
				}
				else
					/* WIN32 can't dereference a NULL pointer, not suprising really,
					but makerect rely on it so give it a value.*/
					*pictemp++ = 1;	
			
/* Increment offset by 8x required
 * offset. */
				offset_big += offset_inc;
			}

			if (maskbuffer != NULL)
			{
/* Set this bit in the mask. */
				maskbyte = maskbyte | maskbit;
				maskbit = maskbit << 1;
/* If we have just done the top bit
 * we need to go to next byte and
 * start again from LSB. */
				if (maskbit == 0)
				{
					*masktemp++ |= maskbyte;
					maskbit = 1;
					maskbyte = 0;
				}
			}
		}

/* Just make sure we put end of
 * interval into mask (remember these
 * were only put in when the byte was
 * full). */
		if (maskbuffer != NULL)
			if (maskbyte != 0)
				*masktemp |= maskbyte;

/* Increment pointers ready for
 * next line in buffer. */
		if (ys < 0)
		{
			pictempstart += fullw;
			if (maskbuffer != NULL) masktempstart += maskw;
		}
		else
		{
			pictempstart -= fullw;
			if (maskbuffer != NULL) masktempstart -= maskw;
		}

/* If h was > 1, (ie we are scaled-up)
 * there are a number of lines which
 * exact copies of what we just did. */
		j = 1;
		while (j < h)
		{
			j++;

/* Repeated lines are AFTER the last. */
			if (ys < 0)
			{
/* Check for overflow. But I've NEVER
 * seen this one happen! */
				if ((unsigned)pictempstart >= picbuffend - fullw)
					return(picbuff);

				if (vdom != NULL)
					memcpy(pictempstart, pictempstart - fullw, fullw);

				pictempstart += fullw;

				if (maskbuffer != NULL)
				{
					memcpy(masktempstart, masktempstart - maskw, maskw);
					masktempstart += maskw;
				}
			}
			else
/* Repeated lines are BEFORE the last. */
			{
/* I've never seen this one happen
 * either, but what the hell.... */
				if ((unsigned)pictempstart < (unsigned)picbuff)
					return(picbuff);

				if (vdom != NULL)
					memcpy(pictempstart, pictempstart + fullw, fullw);

				pictempstart -= fullw;

				if (maskbuffer != NULL)
				{
					memcpy(masktempstart, masktempstart + maskw, maskw);
					masktempstart -= maskw;
				}
			}
		}
		if (vdom!=NULL) 
			retval=nextgreyinterval(&iwsp);
		else
			retval=nextinterval(&iwsp);

	}

	return(picbuff);
} /* picbuffer */




/****************************************************** picrawbuffer */

/*
 * Traverse the raw image object and draw it into an
 * 8-bit buffer.
 * The buffer is malloc'd here (a pointer to
 * it is returned) if it does not already exist.
 * The return values are in screen pixel
 * coordinates, with origin at the top left.
 */

unsigned char *
picrawbuffer(struct rawimage *obj, int *xout, int *yout, int *wout, int *hout, struct pframe *f, unsigned char * *maskbuffer)
{
	unsigned char *pictemp, *pictempstart;
	unsigned picbuffend;
	int x, y, w, h, fullw, fullh, maskw;
	int abs_xs, abs_ys, xs, ys, lasty, linpos, lastlinpos;
	register short i, j;
	register int offset_inc, offset_big;
	register unsigned char maskbyte, maskbit;
	unsigned char *masktemp, *masktempstart;
	char *vdom, *srcptr, *temp;
	DDGS *dg;
	int maxx,maxy;
	int offscreen/*, firstline*/;
	int origfullw, origfullh, xorig, yorig, newy;
	int xtemp,wtemp;
	int ytoporig;
	int xoffset, yoffset;
	int xss,yss,wss,hss;
	double ss, sc;

	vdom=obj->values;

	xs = f->ix*f->scale;
	ys = f->iy*f->scale;

	abs_xs = ABS(xs);
	abs_ys = ABS(ys);

/* Calculate the width and height of the
 * required buffer. */
	fullw = (abs_xs*(obj->width)) >> 3;
	fullh = (abs_ys*(obj->height)) >> 3;
	origfullw=fullw;
	origfullh=fullh;

	x = y = 0;
	xorig=x;
	yorig=y;

	ytoporig=yorig+fullh;

		
/* check object is on screen
 * stop now if isn't
 */
	dg=getcurdg();
	maxx=dg->ddgsImageFormat.MaxX;	//dg->maxx;
	maxy=dg->ddgsImageFormat.MaxY;	//dg->maxy;

	offscreen=0;

/* Force buffer to be multiple of 4 bytes wide. */
	fullw = (fullw + 3) & 0xFFFFFFFC;

	xoffset = 0;
	yoffset = 0;

/* Output TOP_LEFT corner position, width and height. */

			//set image size
	dg->ddgsImageFormat.MaxX = fullw;
	dg->ddgsImageFormat.MaxY = fullh;

		//get screen coordinates of image
		//to accommodate all format camera images
	xss = *xout;
	yss = fullh - *yout;
	wss = *wout;
	hss = *hout;
	imagetoscreen (&xss,&yss,&wss,&hss,&ss, &sc,FALSE);
	*xout = xss;
	*yout = yss;
	*wout = wss;
	*hout = hss;

	if (offscreen==1)
		return(NULL);

	/* BP */
	picbuff = (unsigned char *)ddgs_get_aux_buffer(fullw*fullh);
	picmask = (unsigned char *)ddgs_get_draw_mask(fullw, fullh);
	if (maskbuffer)
		*maskbuffer = picmask;
	picbuffend = (unsigned)picbuff + fullw*fullh;
	maskw = (fullw + 7) >> 3;

/* Fixed-point incrementer for scaling. */
	offset_inc = 64/xs;

/* linpos initialisation */
	lasty = -(abs_ys/8);
	lastlinpos =  - 1;


/* Scan through object on a line by line. */
	linpos=0;
	srcptr=obj->values;


	while (linpos<obj->height)
	{
/* Height "above" first line. */
		y = (abs_ys*linpos) >> 3;
		newy=y;

/* Height depends on scale.
 * Don't recalculate if interval is
 * on same line as last one. */

		if (linpos > lastlinpos) {
			if (linpos-lastlinpos > 1) {
				/* handle empty intervals */
				lasty=(abs_ys*(linpos-1)) >> 3;
			}
			h = y - lasty;
		}

		y=y-yoffset;	

		if (y<0) {
			lasty = newy;
			lastlinpos=linpos;
		}
			


/* Height may be 0 when object
 * is scaled-down. In which case
 * ignore this interval.
 * Also ignore if interval is off screen */
		if ((h < 1) || (y<0) || (y>fullh)) {
			if (linpos<obj->height) {
				linpos++;
				srcptr+=obj->width;
			}
			continue;
		}

/* Find start point in buffer. 
 * These XXXXtempstart pointers
 * are used for convenience and point to
 * the first location in the particular
 * line in the buffer. X offsets are
 * therfore always relative to these. */
		pictempstart = picbuff + (y*fullw);

		if (maskbuffer != NULL)
			masktempstart = (*maskbuffer) + (y*maskw);
	

/* Calculate distance of start of
 * interval from edge of buffer, and
 * the scaled width of the interval. */
		x = 0;
		w = (abs_xs*obj->width) >> 3;
		wtemp=w;

/* If xscale is negative we scan from
 * the end of the interval backwards. */
		if (xs < 0) {
			offset_big = (obj->width << 3) - 1;
			xtemp=origfullw-x-w;
		}
		else {
			offset_big = 0;
			xtemp=x;
		}

/*check if interval in display box */

		x=xtemp-xoffset;

		offscreen=0;

		if (x<0) {
			w=w+x;
			if (w<=0)
				offscreen=1;
			else {
				/* update offset */
				if (xs < 0)
					offset_big += x*64/abs_xs;
				else 
					offset_big -= x*64/abs_xs;

				x=0;
			}
		}
		else
			offscreen=(x>fullw) ? 1 : 0;
		
		if (w+x>fullw)
			w=fullw-x;

/* if interval off screen - next interval */
		if (offscreen==1) {
			if (linpos<obj->height) {
				linpos++;
				srcptr+=obj->width;
			}
			continue;
		}

/* If this interval is not on same
 * line as last one reset these bits. */
		if (linpos > lastlinpos)
		{
			lasty = (abs_ys*linpos) >> 3;
			lastlinpos = linpos;
		}

/* Point to first required location
 * in buffer. */
		pictemp = pictempstart + x;

/* Calculate the byte position of the
 * first pixel in the mask buffer. Also
 * find its offset within the byte.
 * Note: at the start of each interval we
 * read the existing mask buffer value
 * for the start byte in case a previous
 * interval finished in the same byte. */
		if (maskbuffer != NULL)
		{
			masktemp = masktempstart + (x >> 3);
			maskbit = 1 << (x & 0x07);
			maskbyte = *masktemp;
		}

/* Quick check that we're not going
 * beyond the buffer. Probably unnecessary. */
		if ((unsigned)pictemp < (unsigned)picbuff)
			return(picbuff);

		if ((unsigned)pictemp >= picbuffend - w)
			return(picbuff);

/* For each point along SCALED interval. */
		for (i = 0; i < w; i++)
		{
			if (vdom != NULL)
			{
/* Get pointer to grunt value. Note
 * fixed-point scaling is x8. */
				temp = srcptr + (offset_big >> 3);

				*pictemp++ = *temp;


/* Increment offset by 8x required
 * offset. */
				offset_big += offset_inc;
			}
				

			if (maskbuffer != NULL)
			{
/* Set this bit in the mask. */
				maskbyte = maskbyte | maskbit;
				maskbit = maskbit << 1;
/* If we have just done the top bit
 * we need to go to next byte and
 * start again from LSB. */
				if (maskbit == 0)
				{
					*masktemp++ = maskbyte;
					maskbit = 1;
					maskbyte = 0;
				}
			}
		}

/* Just make sure we put end of
 * interval into mask (remember these
 * were only put in when the byte was
 * full). */
		if (maskbuffer != NULL)
			if (maskbyte != 0)
				*masktemp = maskbyte;

/* Increment pointers ready for
 * next line in buffer. */
		if (ys < 0)
		{
			pictempstart += fullw;
			masktempstart += maskw;
		}
		else
		{
			pictempstart -= fullw;
			masktempstart -= maskw;
		}

/* If h was > 1, (ie we are scaled-up)
 * there are a number of lines which
 * exact copies of what we just did. */
		j = 1;
		while (j < h)
		{
			j++;

/* Repeated lines are AFTER the last. */
			if (ys < 0)
			{
				if ((unsigned)pictempstart >=
							picbuffend - fullw)
					return(picbuff);

				if (vdom != NULL)
					memcpy(pictempstart,
						pictempstart - fullw, fullw);

				pictempstart += fullw;

				if (maskbuffer != NULL)
				{
					memcpy(masktempstart,
						masktempstart - maskw, maskw);
					masktempstart += maskw;
				}
			}
			else
/* Repeated lines are BEFORE the last. */
			{
				if ((unsigned)pictempstart <
							(unsigned)picbuff)
					return(picbuff);


				if (vdom != NULL)
					memcpy(pictempstart,
						pictempstart + fullw, fullw);

				pictempstart -= fullw;

				if (maskbuffer != NULL)
				{
					memcpy(masktempstart,
						masktempstart + maskw, maskw);
					masktempstart -= maskw;
				}
			}
		}
		if (linpos<obj->height) {
			linpos++;
			srcptr+=obj->width;
		}

	}

	return(picbuff);
} /* picrawbuffer */


