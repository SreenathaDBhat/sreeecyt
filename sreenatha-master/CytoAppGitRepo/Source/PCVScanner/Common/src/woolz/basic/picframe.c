/*
 * picframe.c	Jim Piper	10 August 1983
 *
 * Draw an object in DIGS.
 *
 * Mods:
 *
 *	16May2003	JMB	Now always create a mask for fastpixobj() for GREYIM images,
 *					as fastpixobj has been modified such that it can now use
 *					its mask parameter (previously always ignored) for such images. 
 *	KS	30Jun00		picrawim now handles 12bit RawIm structures, i.e. converts the
 *					12bit data to 8bits for display.
 *	WH 09May97:		BP has changed Fastpixobj, so do approp changes to Picrawim.
 *					Is this going to be okay on UNIX ?
 *	BP	19Dec96:	Proper cast all types - domain/obj and float/int - to
 *					eliminate compiler warnings.
 *	WH	17Dec96:	PicFrame returns 1 if NULL obj
 *	9/19/96		BP:		Change COORD to WZCOORD. Text functions do nothing.
 *						Add forward declarations.
 *						Fix bug - text_gc is a pointer!
 *	BP	5/2/96:		Removed all refernces to grab buffer (already commented
 *					but was ugly).
 *	BP	7/18/95:	Use temp int values for Picpoly to avoid overflow.
 *	BP	6/15/95:	Two tweaks to Picpoly: now uses ddgs_aux_buffer for
 *					the array of points (no more malloc), and special
 *					hack for Univision to reduce broken lines.
 *	BP	5/10/95:	Another tweak to txtobj() which now has to make sure
 *					the static pixmap it uses is of the right depth -
 *					multiple depth dogs are now permissable for Univision
 *					printing.
 *					Also now use qbuffer() for regular object drawing.
 *					This is faster than picbuffer.
 *	BP	12/10/94	Rehash of txtobj() to use fixed pixmap and don't call
 *					XGetWindowAttributes each time. Also removed call to
 *					threshold(). Result is faster drawing that is clearer
 *					when scaled down.
 *	BP	9/20/94		No more malloc/grab_buffer stuff. Picintobj now
 *					uses ddgs buffers.
 *	 6 Jan 1993		MG	picintobj now uses fill instead of line drawing
 *	 2 Sep 1992		MG 	modified to allow all routines to draw into
 *					either the grey, overlay or marker images
 *					as defined by ddgs. Generic routines begin
 *					with capital P - specific routines call these :
 *					picpoly, opicpoly and mpicpoly all call Picpoly
 *	11/6/92			mc	commented out call to arrowhead()
 *					will be removed eventully
 *	 7 Feb 1991		CAS		voids
 *	02 Dec 1988		SCG		rewrite picpoly to be more stack efficient
 *  18 Nov 1988		dcb		woolz_check_obj() instead of wzcheckobj()
 *	20 Aug 1987	jimp@IRS	arrowhead() call for arrow cursor.
 *					CHECK that this mod matches the MRC
 *					version of 04-03-87.
 *	06 May 1987		BDP		protection against null or empty objs
 *	12 Sep 1986		CAS		Includes
 */

#include <ddgs.h>

#include "woolz.h"

#include <stdio.h>
#include <assert.h>

//#include <wstruct.h>
//#include <ddgs.h>

#define ABS(i)	(i>=0? i: -i)

			/* where to draw graphics or text */
#define GREYIM 	0	/* grey image planes */
#define	OVLYIM	1	/* overlay plane */
#define MARKIM	2	/* marker image planes */

#ifdef PICDEBUG
extern FILE *pic_chan;
#endif



//old_style_picintobj = 0;		/* picintobj method - normally 0 */
//int dont_use_clipmask = 0;

extern unsigned char *picbuff;		/* picbuffer.c */
extern unsigned char *picmask;		/* picbuffer.c */


/*
 * Protection added to this module assumes that picframe function is the
 * only public function in this module.  The only checking necessary is
 * for the object existing. If the object is null immediate return is
 * effected. All routines should copy with an empty object, either by
 * their own for loops , or by protection added to  the interval scanning
 * routines.  bdp 6/5/87 
 */

/************************************************************************/


int
Picframe(struct object *obj, struct pframe *f, SMALL where)
{
	DDGS * cur;
	int oldthick = 1;
	/* Return fail on NULL obj */
	if (!obj)
		return(1);

	if (woolz_check_obj(obj, "picframe") != 0)
		return(1);		/* return a failed status to anyone interested */

	if ((cur = getcurdg())!= NULL)
		oldthick = cur->cur_lwidth;

	// some types we can modify the line style and thickness, if it has a propertylist on the Wobj
	switch(obj->type)
	{
		case 10:
		case 12:
//		case 20:
//		case 30:
//		case 31:
			if (obj->plist)
			{
				GraphicPlist *gplist = (GraphicPlist *)obj->plist;
				int scale = (gplist->linewidth * f->scale)/8;
				if (scale < 1)
					scale = 1;

				lwidth(scale);	// scale line width

				if (gplist->graphicstyle == 0x00000001) // Fill solid colour
					fillcolour(gplist->colour);
			}
			break;
	}

	/* switch on object type */
	switch(obj->type) 
	{
	case 0 :		/* interval/grey-table object */
	case 1 :
		Picintobj(obj, f, where);
		break;

	case 10 :	/* polygon */
	case 12 :	/* convexhull */
		Picpoly((WZPoly *)obj->idom, f, where);
		break;

	case 11 :	/* boundary list */
		Picbound((WZBound *)obj->idom, f, where);
		break;

	case 13:	/* histogram */
		Pichisto((WZHisto *)obj->idom, f, where);
		break;

	case 20 :	/* rectangle - vertices specified */
		Picrect((WZIrect *)obj->idom, f, where);
		break;

	case 30 :	/* vectors */
	case 31 :
		Picvec((WZIvector *)obj, f, where);
		break;

	case 40 :	/* point (cursor) */
	case 41 :
		Picpoint((WZIpoint *)obj, f, where);
		break;

	case 70 :	/* text object */
		Pictext((WZTextObj *)obj, f, where);
		break;

	case 90:
		Picrawim(obj,f,where);
		break;

	case 100:
		Piccircle((WZCircle *)obj, f, where);
		break;

	default:
		break;
	}
	lwidth(oldthick);
	fillcolour(-1);
	return(0);	/* operation completed ok */
}


/* versions to work in grey, overlay or marker image planes */


void
picframe(struct object *obj, struct pframe *f)
{
	Picframe (obj, f, GREYIM);
}

void
opicframe(struct object *obj, struct pframe *f)

{
	Picframe (obj, f, OVLYIM);
}


void
mpicframe(struct object *obj, struct pframe *f)
{
	Picframe (obj, f, MARKIM);
}


#if 0


/**************************************************************************/

/* old style - slow - picintobj - has capability to draw at funny scales */

#ifdef WIN32
static void
oldPicintobj(struct object *obj, register struct pframe *f, SMALL where)
#endif
#ifdef i386
static void
oldPicintobj(obj, f, where)
struct object *obj;
register struct pframe *f;
SMALL where;
#endif
{
	struct iwspace iwsp;
	struct gwspace gwsp;
	register xo,yo,xs,ys;
	int intlength;

	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	xo = f->dx - xs*f->ox;
	yo = f->dy - ys*f->oy;
	if ((obj->vdom == NULL) || (where!=GREYIM)) {
		/* scanning routines will protect against empty objects */

		/* modify fill rectangle origin for -ve xs or ys */
		if (xs<0) xo=xo+xs;
		if (ys<0) yo=yo+ys;

		initrasterscan(obj,&iwsp,0);
		while (nextinterval(&iwsp) == 0) {
			intlength = xs*iwsp.colrmn;

			switch (where) {
			case OVLYIM:
				ofill(xo + xs*iwsp.lftpos, yo + ys*iwsp.linpos,
				      abs(intlength), abs(ys));
				break;
			case MARKIM: 
				mfill(xo + xs*iwsp.lftpos, yo + ys*iwsp.linpos,
				      abs(intlength), abs(ys));
				break;
			case GREYIM:
			default:			
				fill(xo + xs*iwsp.lftpos, yo + ys*iwsp.linpos,
				     abs(intlength), abs(ys));
				break;
			}
		}
	}
	else
	{
		initgreyscan(obj,&iwsp,&gwsp);
		while (nextgreyinterval(&iwsp) == 0) {
			moveto(xo + xs*iwsp.lftpos, yo + ys*iwsp.linpos);
			pixline(iwsp.colrmn,ABS(xs),ABS(ys),gwsp.grintptr,
				(f->ix>0? 1: -1));
		}
	}
}


#endif


/************************************************************************/
void Picintobj(struct object *obj, register struct pframe *f, SMALL where)
{
	int x, y, w, h;
	unsigned char *vdata, *mdata;
	int dispx = 0;
	int dispy = 0;
	int dispw = 0;
	int disph = 0;
	double dispscale, sc;

	// Only GREYIM will use mdata
	if (where != GREYIM)
	{
		if ((vdata = qbuffer(obj, &x, &y, &w, &h, f, NULL)) != NULL){
		//	fastpixobj(x, y, w, h, vdata, NULL, where);
			dispx = x;
			dispy = y;
			dispw = w;
			disph = h;
				//get display coorindates of image
				//this takes care of scaling and translation (scrolling)
			imagetoscreen (&dispx,&dispy,&dispw,&disph,&dispscale, &sc,FALSE);
			x = y = 0;
				//draw image into display
			nfastpixobj(x, y, w, h, dispx,dispy,dispw,disph,vdata, NULL, where);
			}
	}
	else
	{
		if ((vdata = qbuffer(obj, &x, &y, &w, &h, f, &mdata)) != NULL){
			dispx = x;
			dispy = y;
			dispw = w;
			disph = h;
				//get display coorindates of image
				//this takes care of scaling and translation (scrolling)
			imagetoscreen (&dispx,&dispy,&dispw,&disph,&dispscale, &sc,FALSE);
			x = y = 0;
				//draw image into display
			nfastpixobj(x, y, w, h, dispx,dispy,dispw,disph,vdata, mdata, where);
			}
	}

}


/* versions to work in grey, overlay or marker image planes */

void
picintobj(struct object *obj, register struct pframe *f)
{
	Picintobj(obj,f,GREYIM);
}


void
opicintobj(struct object *obj, register struct pframe *f)
{
	Picintobj(obj,f,OVLYIM);
}



void
mpicintobj(struct object *obj, register struct pframe *f)
{
	Picintobj(obj,f,MARKIM);
}


/**********************************************************************/


void
Picrawim(RawIm *im, register struct pframe *f, SMALL where)
{
	int x, y, w, h;
	char *srcdata, *mdata;
	RawIm *temp_im;
	int dispx = 0;
	int dispy = 0;
	int dispw = im->width;
	int disph = im->height;

	if (im->bits == 16)
	{
		temp_im = (RawIm *)Calloc(1, sizeof(RawIm));
		temp_im->type = 90;
		temp_im->width = im->width;
		temp_im->height = im->height;
		temp_im->bits = 16;
		temp_im->values = (unsigned char *)Malloc(2 * temp_im->width * temp_im->height * sizeof(unsigned char));
		memcpy(temp_im->values, im->values, 2 * temp_im->width * temp_im->height);


		fs12to8(temp_im, TRUE);

		// Only GREYIM will use mdata
		if (where != GREYIM)
		{
			if ((srcdata = (char *)picrawbuffer(temp_im, &dispx, &dispy, &dispw, &disph, f, NULL)) != NULL){
				x = 0;
				y = 0;
				w = im->width;
				h = im->height;

				nfastpixobj(x, y, w, h, dispx,dispy,dispw,disph,srcdata, NULL, where);
				}
		}
		else
		{
			/* Fastpixobj now uses 1 less param, ddgs_get_draw_buffer no longer needed. WH */
			if ((srcdata = (char *)picrawbuffer(temp_im, &dispx, &dispy, &dispw, &disph, f, &mdata)) != NULL)
			{
				x = 0;
				y = 0;
				w = im->width;
				h = im->height;

				nfastpixobj(x, y, w, h, dispx,dispy,dispw,disph,srcdata, mdata, where);

				#if 0
				if ((vdata = (char *)ddgs_get_draw_buffer(w*h)) != NULL)
					fastpixobj(x, y, w, h, vdata, mdata, where, srcdata);
				#endif
			}
		}


		Free(temp_im->values);
		Free(temp_im);
	}
	else
	{
		// Only GREYIM will use mdata
		if (where != GREYIM)
		{
			if ((srcdata = (char *)picrawbuffer(im, &x, &y, &w, &h, f, NULL)) != NULL){
				x = 0;
				y = 0;
				w = im->width;
				h = im->height;

				nfastpixobj(x, y, w, h, dispx,dispy,dispw,disph,srcdata, NULL, where);
				}
		}
		else
		{
			/* Fastpixobj now uses 1 less param, ddgs_get_draw_buffer no longer needed. WH */
			if ((srcdata = (char *)picrawbuffer(im, &dispx, &dispy, &dispw, &disph, f, &mdata)) != NULL)
			{
				x = 0;
				y = 0;
				w = im->width;
				h = im->height;
					//draw image into display
				nfastpixobj(x, y, w, h, dispx,dispy,dispw,disph,srcdata, mdata, where);
			}
		}
	}
}


/* versions to work in grey, overlay or marker image planes */


void
picrawim(RawIm *im, register struct pframe *f)
{
	Picrawim(im,f,GREYIM);
}


void
opicrawim(RawIm *im, register struct pframe *f)
{
	Picrawim(im,f,OVLYIM);
}


void
mpicrawim(RawIm *im, register struct pframe *f)
{
	Picrawim(im,f,MARKIM);
}




/***********************************************************************/


/*
 * Picpoly - display a polygon domain.
 */
void
Picpoly(struct polygondomain *pdom, struct pframe *f, SMALL where)
{
	struct fvertex *fverts;
	struct ivertex *iverts;
	int npoints, i, xo, yo, xs, ys;
	register int xdum, ydum;
	DDGSPoint *xp;
	DDGS *dg;
	int xx,yy;

/* Do nothing if nothing to do. */
	if (pdom->nvertices < 2)
		return;

/*
 * BP - now use ddgs buffer instead.
	xp = (XPoint *)Malloc(sizeof(XPoint)*pdom->nvertices);
*/
	xp = (DDGSPoint *)ddgs_get_aux_buffer(sizeof(DDGSPoint)*pdom->nvertices);

	dg = getcurdg();

	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	xo = f->dx - xs*f->ox + ABS(xs)/2;	/* to line up centrally with pixels */
	yo = f->dy - ys*f->oy + ABS(ys)/2;

/* Convert the polygon coordinates into sensible ones. */
	switch (pdom->type)
	{
	case 1 :		/* Integer poly domain. */
		iverts = pdom->vtx;

		for (i = 0; i < pdom->nvertices; i++)
		{
/* Calculate via int values to avoid overflow
 * at large scales. */
			xdum = xo + xs*(int)iverts->vtX;
			ydum = yo + ys*(int)iverts->vtY;

				//convert from image to screen coordinates
			xx = xdum >> 3;
			yy = ydum >> 3;
			imagetoscreen_pt (&xx, &yy,FALSE);
			xp[i].x = xx;
			xp[i].y = yy;

			iverts++;
		}
		break;

	case 2 :		/* Float poly domain (hopefully not used at all!) */
		fverts = (struct fvertex *)pdom->vtx;

		for (i = 0; i < pdom->nvertices; i++)
		{
			xdum = xo + (int)((float)xs*fverts->vtX);
			ydum = yo + (int)((float)ys*fverts->vtY);

				//convert from image to screen coordinates
			xx = xdum >> 3;
			yy = ydum >> 3;
			imagetoscreen_pt (&xx, &yy,FALSE);
			xp[i].x = xx;
			xp[i].y = yy;

			fverts++;
		}

	default:
		break;
	}

#if 0

/* If Univision */
	if (flip_probe_colours)
	{
		register short xdiff, ydiff, j;

/* Run through all the points to see if
 * any of them are adjacent. These will
 * be ignored. */
		if ((npoints = pdom->nvertices) > 1)
			for (i = 0; i < npoints - 1; i++)
			{
				xdiff = xp[i].x - xp[i + 1].x;
				ydiff = xp[i].y - xp[i + 1].y;

/* If the next point is adjacent to this
 * one, skip it. */
				if ((xdiff >= -1) && (xdiff <= 1) &&
					(ydiff >= -1) && (ydiff <= 1))
				{
/* Pull all the remaining points back
 * by one place, so the list of points
 * is still cointiguous. */
					for (j = i + 1; j < npoints; j++)
						xp[j] = xp[j + 1];

/* Now we have one less point. */
					npoints--;
				}
			}
	}
	else
#endif

		npoints = pdom->nvertices;		/* Normally just use them all. */

	switch (where)
	{			
//		case GREYIM: drawpoly(xp, npoints); break;
		case OVLYIM: odrawpoly(xp, npoints, (dg->fillbrush) ? 1 : 0); break;
		case MARKIM: mdrawpoly(xp, npoints, (dg->fillbrush) ? 1 : 0); break;
	}

	
/*
 * BP - don't free: now using ddgs buffer.
	Free(xp);
 */
}




/* versions to work in grey, overlay or marker image planes */


void
picpoly(struct polygondomain *pdom, struct pframe *f)
{
	Picpoly(pdom,f,GREYIM);
}


void
opicpoly(struct polygondomain *pdom, struct pframe *f)
{
	Picpoly(pdom,f,OVLYIM);
}


void
mpicpoly(struct polygondomain *pdom, struct pframe *f)
{
	Picpoly(pdom,f,MARKIM);
}


/*************************************************************************/

/*
 * display a circle object
 */

void
Piccircle(struct circle *circ, struct pframe *f, SMALL where)
{
	int xo,yo,xs,ys;
	int x,y,r;
	int x1,y1;
	double ss, sc;

	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	xo = f->dx - xs*f->ox + ABS(xs)/2;	/* to line up centrally with pixels */
	yo = f->dy - ys*f->oy + ABS(ys)/2;

		//convert from image to screen coordinates
	x1 = (xo+xs*circ->centre.vtX) >> 3;
	y1 = (yo+ys*circ->centre.vtY) >> 3;
	imagetoscreen_pt_scale (&x1,&y1,&ss,&sc, TRUE);
	x = x1 << 3;
	y = y1 << 3;

	r = xs * circ->radius * ss;
	
	switch (where) {			
//		case GREYIM:	drawcircle(x,y,r); break;
		case OVLYIM:	odrawcircle(x,y,r); break;
		case MARKIM:	mdrawcircle(x,y,r); break;
		default: 	break;
	}

}

/* versions to work in grey, overlay or marker image planes */


void
piccircle(struct circle *circ, struct pframe *f)
{
	Piccircle(circ,f,GREYIM);
}


void
opiccircle(struct circle *circ, struct pframe *f)
{
	Piccircle(circ,f,OVLYIM);
}


void
mpiccircle(struct circle *circ, struct pframe *f)
{
	Piccircle(circ,f,MARKIM);
}

/**********************************************************************/

	

/*
 * display boundary list of polygons recursively
 */

void
Picbound(struct boundlist *bound, struct pframe *f, SMALL where)
{
	struct boundlist *bp;

	/*
	 * This code has been replaced with code below as the latter is more
	 * stack efficient
	 *	if (bound != NULL) {
	 *		picpoly(bound->poly,f);
	 *		picbound(bound->next,f);
	 *		picbound(bound->down,f);
	 *	}
	 */	
		
	if((bp = bound) != NULL) {
		do {
			Picpoly(bp->poly, f, where);
			Picbound(bp->down, f, where);
		} while((bp = bp->next) != NULL);
	}
}

/* versions to work in grey, overlay or marker image planes */


void
picbound(struct boundlist *bound, struct pframe *f)
{
	Picbound(bound,f,GREYIM);
}


void
opicbound(struct boundlist *bound, struct pframe *f)
{
	Picbound(bound,f,OVLYIM);
}


void
mpicbound(struct boundlist *bound, struct pframe *f)
{
	Picbound(bound,f,MARKIM);
}


/*************************************************************************/


/*
 * display a vector
 */

void
Picvec(struct ivector *vec, struct pframe *f, SMALL where)
{
	register struct fvector *fvec;
	register xo,yo,xs,ys;
	int x1,y1;

	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	xo = f->dx - xs*f->ox + ABS(xs)/2;	/* to line up centrally with pixels */
	yo = f->dy - ys*f->oy + ABS(ys)/2;

	switch (vec->type) {

		case 30 :
				//convert from image to screen coordinates
			x1 = xo+xs*vec->k1;
			y1 = yo+ys*vec->l1;
			imagetoscreen_pt (&x1,&y1, TRUE);
			x1 = x1 << 3;
			y1 = y1 << 3;
			moveto(x1, y1);

			switch (where) {
				case OVLYIM:
						//convert from image to screen coordinates
					x1 = xo+xs*vec->k2;
					y1 = yo+ys*vec->l2;
					imagetoscreen_pt (&x1,&y1,TRUE);
					x1 = x1 << 3;
					y1 = y1 << 3;

					olineto(x1,y1);
					break;
				case MARKIM:
						//convert from image to screen coordinates
					x1 = xo+xs*vec->k2;
					y1 = yo+ys*vec->l2;
					imagetoscreen_pt (&x1,&y1,TRUE);
					x1 = x1 << 3;
					y1 = y1 << 3;

					mlineto(x1, y1);
					break;
				case GREYIM:
				default:			
					break;
				}
				break;

			case 31 :
				fvec = (struct fvector *) vec;

					//convert from image to screen coordinates
				x1 = xo+(int)(xs*fvec->k1);
				y1 = yo+(int)(ys*fvec->l1);
				imagetoscreen_pt (&x1,&y1,TRUE);
				x1 = x1 << 3;
				y1 = y1 << 3;

				moveto(x1, y1);

				switch (where) {

					case OVLYIM:
							//convert from image to screen coordinates
						x1 = xo+(int)(xs*fvec->k2);
						y1 = yo+(int)(ys*fvec->l2);
						imagetoscreen_pt (&x1,&y1,TRUE);
						x1 = x1 << 3;
						y1 = y1 << 3;

						olineto(x1,y1);
						break;

					case MARKIM:
							//convert from image to screen coordinates
						x1 = xo+(int)(xs*fvec->k2);
						y1 = yo+(int)(ys*fvec->l2);
						imagetoscreen_pt (&x1,&y1,TRUE);
						x1 = x1 << 3;
						y1 = y1 << 3;

						mlineto(x1,y1 );
						break;

					case GREYIM:
					default:			
						break;
					}
	/**/
				break;

		default:
			break;
		}
}

/* versions to work in grey, overlay or marker image planes */


void
picvec(struct ivector *vec, struct pframe *f)
{
	Picvec(vec,f,GREYIM);
}


void
opicvec(struct ivector *vec, struct pframe *f)
{
	Picvec(vec,f,OVLYIM);
}


void
mpicvec(struct ivector *vec, struct pframe *f)
{
	Picvec(vec,f,MARKIM);
}


/************************************************************************/

/*
 * display a point (in future, in a variety of styles; just a cross for now)
 */

void
Picpoint(struct ipoint *point, struct pframe *f, SMALL where)
{
	register struct fpoint *fpoint;
	register xo,yo,xs,ys;
	int x1,y1;
	double ss, sc, aa;

	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	xo = f->dx - xs*f->ox + ABS(xs)/2;	/* to line up centrally with pixels */
	yo = f->dy - ys*f->oy + ABS(ys)/2;


	switch (point->type) {
	case 40 :
			//convert from image to screen coordinates
		x1 = (xo+xs*point->k) >> 3;
		y1 = (yo+ys*point->l) >> 3;
		imagetoscreen_pt_scale (&x1,&y1,&ss,&sc, TRUE);
		x1 = x1 << 3;
		y1 = y1 << 3;

		moveto (x1,y1);
		aa= abs(xs)*sc * ss;
		Piccursor((int)aa,point->style,where);
		break;
	case 41 :
/**/
		fpoint = (struct fpoint *) point;
			//convert from image to screen coordinates
		x1 = (xo+(int)(xs*fpoint->k)) >> 3;
		y1 = (xo+(int)(xs*fpoint->l)) >> 3;
		imagetoscreen_pt_scale (&x1,&y1,&ss,&sc, TRUE);
		x1 = x1 << 3;
		y1 = y1 << 3;

		moveto (x1,y1);
		Piccursor(abs(xs)*ss,point->style,where);
/**/
		break;
	default:
		break;
	}
}
/* versions to work in grey, overlay or marker image planes */


void
picpoint(struct ipoint *point, struct pframe *f)
{
	Picpoint(point,f,GREYIM);
}


void
opicpoint(struct ipoint *point, struct pframe *f)
{
	Picpoint(point,f,OVLYIM);
}


void
mpicpoint(struct ipoint *point, struct pframe *f)
{
	Picpoint(point,f,MARKIM);
}


/***********************************************************************/


void
Piccursor(int scale, int style, SMALL where)
{
	switch (style) {
	default :
	case 0:				/* constant sized cross */
		scale = 16;
	case 1:				/* scaled cross */
		scale = scale*4;
		switch (where) {
		case OVLYIM:
			moveby(-scale/2,-scale/2);
			olineby(scale,scale);
			moveby(0,-scale);
			olineby(-scale,scale);
			break;
		case MARKIM:
			moveby(-scale/2,-scale/2);
			mlineby(scale,scale);
			moveby(0,-scale);
			mlineby(-scale,scale);
			break;
		case GREYIM:
		default:			
			moveby(-scale/2,-scale/2);
//			lineby(scale,scale);
			moveby(0,-scale);
//			lineby(-scale,scale);
			break;
		}

		return;
	case 2:				/* constant sized blob */
		switch (where) {
		case OVLYIM:
			moveby(-8,-8);
			olineby(16,0);
			moveby(0,8);
			olineby(-16,0);
			moveby(0,8);
			olineby(16,0);
			break;
		case MARKIM:
			moveby(-8,-8);
			mlineby(16,0);
			moveby(0,8);
			mlineby(-16,0);
			moveby(0,8);
			mlineby(16,0);
			break;
		case GREYIM:
		default:			
			moveby(-8,-8);
//			lineby(16,0);
			moveby(0,8);
//			lineby(-16,0);
			moveby(0,8);
//			lineby(16,0);
			break;
		}
		break;

	case 4:				/* constant sized hollow square */
		scale = 32;
		lwidth(2);
	case 5:				/* scaled hollow square */
		switch (where) {
		case OVLYIM:
			moveby(-scale,-scale);
			scale *= 2;
			olineby(scale,0);
			olineby(0,scale);
			olineby(-scale,0);
			olineby(0,-scale);
			break;
		case MARKIM:
			moveby(-scale,-scale);
			scale *= 2;
			mlineby(scale,0);
			mlineby(0,scale);
			mlineby(-scale,0);
			mlineby(0,-scale);
			break;
		case GREYIM:
		default:			
			moveby(-scale,-scale);
			scale *= 2;
//			lineby(scale,0);
//			lineby(0,scale);
//			lineby(-scale,0);
//			lineby(0,-scale);
			break;
		}
		if (style == 4)
			lwidth(0);
		break;

	case 6:				/* tilted arrow head */
		/* arrowhead(32); mc 11/6/92 (in DDGS) */
		break;

	case 8:	lwidth(3);	/* FAT version of  */
	case 7: 		/* + shaped cross  */
		scale = scale*8;
		switch (where) {
		case OVLYIM:
			moveby(-scale/2,0);
			olineby(scale,0);
			moveby(-scale/2,-scale/2);
			olineby(0,scale);
			break;
		case MARKIM:
			moveby(-scale/2,0);
			mlineby(scale,0);
			moveby(-scale/2,-scale/2);
			mlineby(0,scale);
			break;
		case GREYIM:
		default:			
			moveby(-scale/2,0);
//			lineby(scale,0);
			moveby(-scale/2,-scale/2);
//			lineby(0,scale);
			break;
		}

		if (style==8)
			lwidth(0);
		break;
	}
}

/* versions to work in grey, overlay or marker image planes */


void
piccursor(int scale, int style)
{
	Piccursor(scale,style,GREYIM);
}


void
opiccursor(int scale, int style)
{
	Piccursor(scale,style,OVLYIM);
}


void
mpiccursor(int scale, int style)
{
	Piccursor(scale,style,MARKIM);
}


/**********************************************************************/


void
Picrect(struct irect *rdom, struct pframe *f, SMALL where)
{
	register i, j;
	WZCOORD *k, *l;
	float *fk, *fl;
	struct frect *fdom;
	register xo,yo,xs,ys;
	int xdum, ydum;
	int xx,yy;

	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	xo = f->dx - xs*f->ox + ABS(xs)/2;	/* to line up centrally with pixels */
	yo = f->dy - ys*f->oy + ABS(ys)/2;


	switch(rdom->type) {
	case 1 :
		k = rdom->irk;
		l = rdom->irl;

			//convert from image to screen coordinates
		xx = (xo+xs*k[0]) >> 3;
		yy = (yo+ys*l[0]) >> 3;
		imagetoscreen_pt (&xx,&yy,TRUE);
		xx = xx << 3;
		yy = yy << 3;
		moveto(xx,yy);

		switch (where) {
		case OVLYIM:
			for (i=1; i<=4; i++) {
				j = i==4? 0: i;
					//convert from image to screen coordinates
				xx = (xo+xs*k[j]) >> 3;
				yy = (yo+ys*l[j]) >> 3;
				imagetoscreen_pt (&xx,&yy,TRUE);
				xx = xx << 3;
				yy = yy << 3;
				olineto(xx,yy);
				}
			break;
		case MARKIM:
			for (i=1; i<=4; i++) {
				j = i==4? 0: i;

					//convert from image to screen coordinates
				xx = (xo+xs*k[j]) >> 3;
				yy = (yo+ys*l[j]) >> 3;
				imagetoscreen_pt (&xx,&yy,TRUE);
				xx = xx << 3;
				yy = yy << 3;

				mlineto(xx, yy);
				}
			break;
		case GREYIM:
		default:			
			for (i=1; i<=4; i++) {
				j = i==4? 0: i;
//				lineto(xo+xs*k[j], yo+ys*l[j]);
			}
			break;
		}
		break;

	case 2 :
		fdom = (struct frect *) rdom;
		fk = fdom->frk;
		fl = fdom->frl;
		xdum = (int)((float)xs*fk[0]);
		ydum = (int)((float)ys*fl[0]);
		moveto(xo+xdum, yo+ydum);
		switch (where) {
		case OVLYIM:
			for (i=1; i<=4; i++) {
				j = i==4? 0: i;
				xdum = (int)((float)xs*fk[j]);
				ydum = (int)((float)ys*fl[j]);

					//convert from image to screen coordinates
				xx = (xo+xdum) >> 3;
				yy = (yo+ydum) >> 3;
				imagetoscreen_pt (&xx,&yy,TRUE);
				xx = xx << 3;
				yy = yy << 3;

				olineto(xx, yy);
			}
			break;
		case MARKIM:
			for (i=1; i<=4; i++) {
				j = i==4? 0: i;
				xdum = (int)((float)xs*fk[j]);
				ydum = (int)((float)ys*fl[j]);

					//convert from image to screen coordinates
				xx = (xo+xdum) >> 3;
				yy = (yo+ydum) >> 3;
				imagetoscreen_pt (&xx,&yy,TRUE);
				xx = xx << 3;
				yy = yy << 3;

				mlineto(xx,yy);
			}
			break;
		case GREYIM:
		default:			
			for (i=1; i<=4; i++) {
				j = i==4? 0: i;
				xdum = (int)((float)xs*fk[j]);
				ydum = (int)((float)ys*fl[j]);
//				lineto(xo+xdum, yo+ydum);
			}
			break;
		}
		break;
	}
}

/* versions to work in grey, overlay or marker image planes */


void
picrect(struct irect *rdom, struct pframe *f)
{
	Picrect(rdom,f,GREYIM);
}


void
opicrect(struct irect *rdom, struct pframe *f)
{
	Picrect(rdom,f,OVLYIM);
}


void
mpicrect(struct irect *rdom, struct pframe *f)
{
	Picrect(rdom,f,MARKIM);
}

/**********************************************************************/



void
Pichisto(struct histogramdomain *h, struct pframe *f, SMALL where)
{
	int *iv;
	float *fv;
	register i, k, l;
	register xs,ys;


	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	k = f->dx - xs*(f->ox - h->k) + xs/2;
	l = f->dy - ys*(f->oy - h->l) + ys/2;

	moveto(k, l);
	switch (h->type) {
	default:
	case 1:
		iv = h->hv;
		switch (h->r) {
		default:
		case 0:
			switch (where) {
			case OVLYIM:
				for (i=0; i<h->npoints; i++) {
					olineto(k+xs* (*iv), l);
					l += ys;
					iv++;
				}
				olineto(k, l-ys);
				break;
			case MARKIM:
				for (i=0; i<h->npoints; i++) {
					mlineto(k+xs* (*iv), l);
					l += ys;
					iv++;
				}
				mlineto(k, l-ys);
				break;
			case GREYIM:
			default:			
				for (i=0; i<h->npoints; i++) {
//					lineto(k+xs* (*iv), l);
					l += ys;
					iv++;
				}
//				lineto(k, l-ys);
				break;
			}
			break;

		case 1:
			switch (where) {
			case OVLYIM:
				for (i=0; i<h->npoints; i++) {
					olineto(k, l+ys* (*iv));
					k += xs;
					iv++;
				}
				olineto(k-xs, l);
				break;
			case MARKIM:
				for (i=0; i<h->npoints; i++) {
					mlineto(k, l+ys* (*iv));
					k += xs;
					iv++;
				}
				mlineto(k-xs, l);
				break;
			case GREYIM:
			default:			
				for (i=0; i<h->npoints; i++) {
//					lineto(k, l+ys* (*iv));
					k += xs;
					iv++;
				}
//				lineto(k-xs, l);
				break;
			}
			return;
		}

	case 2:
/**/
		fv = (float *) h->hv;
		switch (h->r) {
		default:
		case 0:
			switch (where) {
			case OVLYIM:
				for (i=0; i<h->npoints; i++) {
					olineto(k+ (int)(xs* *fv), l);
					l += ys;
					fv++;
				}
				olineto(k, l-ys);
				break;
			case MARKIM:
				for (i=0; i<h->npoints; i++) {
					mlineto(k+ (int)(xs* *fv), l);
					l += ys;
					fv++;
				}
				mlineto(k, l-ys);
				break;
			case GREYIM:
			default:			
				for (i=0; i<h->npoints; i++) {
//					lineto(k+ (int)(xs* *fv), l);
					l += ys;
					fv++;
				}
//				lineto(k, l-ys);
				break;
			}
			return;

		case 1:
			switch (where) {
			case OVLYIM:
				for (i=0; i<h->npoints; i++) {
					olineto(k, l+ (int)(ys* *fv));
					k += xs;
					fv++;
				}
				olineto(k-xs, l);
				break;
			case MARKIM:
				for (i=0; i<h->npoints; i++) {
					mlineto(k, l+ (int)(ys* *fv));
					k += xs;
					fv++;
				}
				mlineto(k-xs, l);
				break;
			case GREYIM:
			default:			
				for (i=0; i<h->npoints; i++) {
//					lineto(k, l+ (int)(ys* *fv));
					k += xs;
					fv++;
				}
//				lineto(k-xs, l);
				break;
			}
			return;
		}
/**/
		break;
	}
}

/* versions to work in grey, overlay or marker image planes */


void
pichisto(struct histogramdomain *h, struct pframe *f)
{
	Pichisto(h,f,GREYIM);
}


void
opichisto(struct histogramdomain *h, struct pframe *f)
{
	Pichisto(h,f,OVLYIM);
}


void
mpichisto(struct histogramdomain *h, struct pframe *f)
{
	Pichisto(h,f,MARKIM);
}




/**************************************************************************/

/*
 * Text object.
 *
 * Completely rewritten, because UNIX version was
 * full of X calls....
 * Now passes string, position and scale to ddgs.
 *
 * .... but this is only adequate for single lines - hence its rename (MG)
 */


void
OnelinePictext(struct textobj *tobj, struct pframe *f, SMALL where)
{
	LPCTSTR str;
	int xo, yo, xs, ys;
	int x, y;
	double ss,sc;
	//short scale;

	str = tobj->text;

	xs = f->ix*f->scale;
	ys = f->iy*f->scale;
	xo = f->dx;		// - xs*f->ox + ABS(xs)/2;	/* to line up centrally with pixels */
	yo = f->dy;		// - ys*f->oy + ABS(ys)/2;


	x = xo + xs*tobj->tdom->k;
	y = yo + ys*tobj->tdom->l;

		//convert from image to screen coordinates
	x = x >> 3;
	y = y >> 3;
	imagetoscreen_pt_scale (&x,&y,&ss,&sc, TRUE);
	x = x << 3;
	y = y << 3;	

	setfont(tobj->tdom->font);
	setTrueFont(tobj->tdom->orientation);
//	if(sc < 1)
//		sc = 1;
//	scale = (short) ((f->scale * ss * sc) + 0.5); // round up or we get scaling error because of cast
//	if (scale < 1)
//		scale = 1;

	switch (where) {			
//		case GREYIM:	text(x, y, str, f->scale); break;
		case OVLYIM:	otext(x, y, str, f->scale); break;
		case MARKIM:	mtext(x, y, str, f->scale); break;
		default: 	break;
	}
}

/*
 * text object - original version from UNIX (MG)
 * a quick and dirty fix here for text with new-line characters
 */

void
Pictext(struct textobj *tobj, struct pframe *f, SMALL where)
{
	TCHAR 	*c;
	TCHAR 	*keepstr;
	int 	i,len,newlines,txtheight,txtwidth;
	int		savedy;
	TCHAR	s[]= _T("TestString");

	savedy=f->dy;

	c = keepstr = tobj->text;

	if (c==NULL)
		return;

	/* get number of newlines in text */
	newlines=0;
	len=_tcslen(c);

	if (len==0)
		return;

	for (i=0; i<len; i++) {
		if (c[i]=='\n')
		newlines++;
	}
	
	setfont(tobj->tdom->font);
	setTrueFont(tobj->tdom->orientation);

	/* deteremine height of one line of text */
	textsize(s, &txtwidth, &txtheight);
	txtheight=(txtheight * f->scale) >> 3;
	/* set position of first line of text */
	f->dy+=newlines*txtheight;

	while (*c != '\0') {
		if (*c == '\n') {
			*c = '\0';

			if (_tcslen(tobj->text)>0) {
				OnelinePictext(tobj, f, where);
			}

			*c = '\n';
			tobj->text = c+1;

			/* update text position */
			f->dy-=txtheight;
		}
		c++;
	}
	if (c != tobj->text) {
		if (_tcslen(tobj->text)>0) {
			OnelinePictext(tobj, f, where);
		}
	}

	tobj->text = keepstr;

	/* restore frame pointer */
	f->dy=savedy;
}



/* versions to work in grey, overlay or marker image planes */


void
pictext(struct textobj *tobj, struct pframe *f)
{
	Pictext(tobj,f,GREYIM);
}


void
opictext(struct textobj *tobj, struct pframe *f)
{
	Pictext(tobj,f,OVLYIM);
}


void
mpictext(struct textobj *tobj, struct pframe *f)
{
	Pictext(tobj,f,MARKIM);
}

