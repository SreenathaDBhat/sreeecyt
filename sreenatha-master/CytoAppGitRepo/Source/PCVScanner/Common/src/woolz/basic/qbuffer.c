
/*
 *	qbuffer.c		BP	12/15/94
 *
 * Quick zap object into buffer.
 * Take no account of ix, iy or scale.
 *
 * Mods:
 *
 *	10Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *					Also, made minor mods to stop compiler warnings and removed
 *					standard C parameter lists (were '#ifdef i386' only).
 *	WH	26Oct98	Protect against NULL vdom.
 *	dcb	3Oct97	Clear excess padded data to background when taking account of
 *				4byte pixel padding for ix inverted objects in qbuffer
 *	MG	1May96		Modified qbuffer() to handle inverted type 1 objects
 *				without slowing down display of non-inverted objects
 *				Basically draws object non-inverted and flips the
 *				data and mask buffers at the end. Not as fast as 
 *				normal objects obviously, but a damn sight faster
 *				and more accurate (better scaling) than picbuffer.
 *
 *	BP	4/5/96:		Tweaked y position to match polygon drawing and old
 *					grey drawing.
 *  BP  11/4/95:    Fixed scalued-up qbuffer interval length.
 *	BP	6/20/95:	Increase fixed-point integer resolution to 8192 so
 *					that top pixel at weird scales (eg 12) is not lost.
 *					Only on scaled objects. Also calculate position and
 *					size more accurately by rounding rather than truncating
 *					(add 4 before dividing by 8).
 *					Added clipping for maximum buffer size of 1152x864 (1Mb).
 *	BP	5/25/95:	Call picbuffer if idom is not type 1. Also
 *					force width to multiple of 4 (for possible
 *					future use of XShmPutImage which appears to
 *					require this!).
 *					Also added clipping so that memory requirements
 *					will not be too big with scaled-up object.
 *	BP	5/5/95:		Even faster version now, which:
 *					(a) DOES take account of scale,
 *					(b) also generates clip masks, if required.
 *					Typically, this is twice as fast (or more) as
 *					picbuffer. Also, it is more accurate at scales
 *					less than 8 (picbuffer cut rows short).
 *					If an object is supplied with ix!=1 or iy!=1
 *					this function calls picbuffer.
 */

#include <woolz.h>

#include <ddgs.h>

#include <stdio.h>
#include <string.h> // For memset, memcpy
//#include <wstruct.h>


#define	MAXWIDTH	1152
#define	MAXHEIGHT	864



/*
 * Fill-in an interval line in the mask
 * buffer.
 * Would be a *tad* faster if this was in-
 * line, but its repeated 3 times...
 */

static void
mask_interval(unsigned char *mask_row, short left, short right)
{
	register unsigned char *mtemp, firstbyte;
	register short masklen;

/* Find the first byte in this row of
 * the buffer. */
	mtemp = mask_row + (left >> 3);
	firstbyte = 255 << (left & 0x07);

/* Fill the rest of the row depending on
 * how many bytes it spans:

	If left and right are in the
	same byte, AND together a mask
	for each. eg 00011111 AND 11111100 = 00011100

	If left and right are in adjacent
	bytes, join the separate masks
	end-to-end. eg 00011111 with
	11111100 = 00011111 11111100

	If left and right are separated
	by some other bytes, do the same
	as the above but with a number of
	255's in-between. Note that there
	are different versions of this -
	I figured that calling memset()
	would be inefficient if there are
	only one or two intermediate bytes.
*/
	switch (masklen = (right >> 3) - (left >> 3)) {
	case 0 :
		firstbyte &= 255 >> (7 - (right & 0x07));
		*mtemp |= firstbyte;
		return;

	case 1 : *mtemp++ |= firstbyte; break;
	case 2 : *mtemp++ |= firstbyte; *mtemp++ = 255; break;
	case 3 : *mtemp++ |= firstbyte; *mtemp++ = 255; *mtemp++ = 255; break;

	default :
		*mtemp++ |= firstbyte;
		--masklen;
		memset(mtemp, 255, masklen);
		mtemp += masklen;
		break;
	}

	*mtemp = 255 >> (7 - (right & 0x07));
} /* mask_interval */


/*
 * Do the business.
 * The mask will be filled only if
 * maskbuffer is non-NULL.
 */

unsigned char *
qbuffer(struct object *obj, int *xout, int *yout, int *wout, int *hout, 
        struct pframe *f, unsigned char * *maskbuffer)
{
	int i, x, y, w, h, line_index;
	unsigned char *outbuffer, *temp, *temp1, *temp2;
	register unsigned char *stemp;
	struct intervaldomain *idom;
	struct valuetable *vdom;
	struct valueline *vtbline;
	struct intervalline *lines;
	struct interval *intval;
	GREY *g;
	register GREY *gtemp, *g1temp , *g2temp, val, val2;
	register int left, right;
	register int j, k, scale;
	int maskw;
	unsigned char *outmask, *masktemp;
	int start_clip_kol, end_clip_kol;
	int start_clip_line, end_clip_line;
//	unsigned short *wordtemp, wordval;
	int ixoffset, padshift, revshift;
	int scl, sck;

/* Use the old version if ix or iy are
 * not unity, or if this is not a type 1 idom/vdom. */

/* MG : now handle simple inverted objects - unity but negative ix, iy */

	if ((abs(f->ix) != 1) || (abs(f->iy) != 1) || (obj->idom->type != 1))
		return((unsigned char *)picbuffer(obj,
				xout, yout, wout, hout, f, maskbuffer));


/* Also use the old version if the vdom
 * is not a type 1. */
	if (obj->vdom)
		if (obj->vdom->type != 1)
			return((unsigned char *)picbuffer(obj,
					xout, yout, wout, hout, f, maskbuffer));

/* MG : modify ox,oy for inverted objects - reset on completion of this code */
	if (f->ix==-1)
		f->ox-=(obj->idom->lastkl - obj->idom->kol1);
	if (f->iy==-1)
		f->oy-=(obj->idom->lastln - obj->idom->line1);


/* Provide quick access to the domains. */
	idom = obj->idom;
	vdom = obj->vdom;

/* Calculate the scaled DDGS position. */
/* This is actually wrong, but consistent with the rest
 * of ddgs/woolz (polygon etc drawing). */
	x = (f->dx - f->scale*f->ox + f->scale*idom->kol1) >> 3;
	y = (f->dy - f->scale*f->oy + f->scale*idom->line1) >> 3;
/* This is what it should be...
	x = (f->dx - f->scale*f->ox + f->scale*idom->kol1 + 4) >> 3;
	y = (f->dy - f->scale*f->oy + f->scale*idom->line1 + 4) >> 3;
*/

/* If we are zoomed up, clip the object
 * at x = y = 0 if necessary. */
	if (f->scale > 8)
	{
		if (x < 0)
		{
			start_clip_kol = (f->scale*f->ox - f->dx)/f->scale;
			x = (f->dx - f->scale*f->ox + f->scale*start_clip_kol) >> 3;
//			x = (f->dx - f->scale*f->ox + f->scale*start_clip_kol + 4) >> 3;
		}
		else
			start_clip_kol = idom->kol1;

		if (y < 0)
		{
			start_clip_line = (f->scale*f->oy - f->dy)/f->scale;
			y = (f->dy - f->scale*f->oy + f->scale*start_clip_line) >> 3;
//			y = (f->dy - f->scale*f->oy + f->scale*start_clip_line + 4) >> 3;
		}
		else
			start_clip_line = idom->line1;
	}
	else
	{
		start_clip_kol = idom->kol1;
		start_clip_line = idom->line1;
	}

/* Calculate scaled width and height.
 * Make sure value is ROUNDED to required
 * size rather than TRUNCATED by adding 4. */
	w = (f->scale*(idom->lastkl - start_clip_kol + 1) + 4) >> 3;
	h = (f->scale*(idom->lastln - start_clip_line + 1) + 4) >> 3;

/* If we are zoomed up, clip the objects
 * total size to MAXWIDTH x MAXHEIGHT. */
	if (f->scale > 8)
	{
		if (w > MAXWIDTH)
		{
			end_clip_kol = (MAXWIDTH << 3)/f->scale + start_clip_kol - 1;
			w = (f->scale*(end_clip_kol - start_clip_kol + 1) + 4) >> 3;
		}
		else
			end_clip_kol = idom->lastkl;

		if (h > MAXHEIGHT)
		{
			end_clip_line = (MAXHEIGHT << 3)/f->scale + start_clip_line - 1;
			h = (f->scale*(end_clip_line - start_clip_line + 1) + 4) >> 3;
		}
		else
			end_clip_line = idom->lastln;
	}
	else
	{
		end_clip_kol = idom->lastkl;
		end_clip_line = idom->lastln;
	}

/* MG : handle clipping on inverted objects */
	if (f->iy==-1) {
		scl=start_clip_line;
		start_clip_line=idom->line1 + (idom->lastln - end_clip_line);
		end_clip_line=idom->lastln - (scl - idom->line1);
	}

	if (f->ix==-1) {
		sck=start_clip_kol;
		start_clip_kol=idom->kol1 + (idom->lastkl - end_clip_kol);
		end_clip_kol=idom->lastkl - (sck - idom->kol1);
	}
	

/* Make sure buffer is a multiple of 4 bytes wide. */
/* MG note the value added in case ix=-1, see horizontal flip below */
	ixoffset=-w;
	w = (w + 3) & 0xFFFFFFFC;
	ixoffset+=w;

	*xout = x;
	*yout = y + h - 1;
//	*yout = y + h;

	*wout = w;
	*hout = h;

/* Don't do anything if its geet small. This
 * will also protect against objects that
 * are off-screen (the clipping will result
 * in negative width and height). */
	if ((w < 1) || (h < 1))
	{
		*wout = *hout = 1;
		outbuffer = (unsigned char *)ddgs_get_aux_buffer(1);
		outmask = (unsigned char *)ddgs_get_draw_mask(1, 1);
	    	if (maskbuffer)
			*maskbuffer = outmask;

		/* reset f->ox, f->oy ... if modified at 
		   start of code for inverted objects */
		if (f->ix == -1)
			f->ox+=(obj->idom->lastkl - obj->idom->kol1);
		if (f->iy == -1)
			f->oy+=(obj->idom->lastln - obj->idom->line1);

		return(outbuffer);
	}

/* Allocate the required size of DDGS draw
 * buffer, and point to the last line. */
	outbuffer = (unsigned char *)ddgs_get_aux_buffer(w*h);
	temp = outbuffer + (h - 1)*w;

/* Allocate the required size of DDGS mask
 * buffer, and point to its last line. */
	outmask = (unsigned char *)ddgs_get_draw_mask(w, h);
	if (maskbuffer)
		*maskbuffer = outmask;
	maskw = (w + 7) >> 3;		/* Width of mask in bytes. */
	masktemp = outmask + (h - 1)*maskw;

/* Point to the vdom line which corresponds
 * to the first idom interval. */
	line_index = idom->line1 - ((vdom) ? vdom->line1 : 0);
	lines = idom->intvlines;

/* For scaled-up objects, skip to the
 * first visible interval. */
	if (f->scale > 8)
		for (i = idom->line1; i < start_clip_line; i++)
		{
			line_index++;
			lines++;
		}

/*********************************/
/* Rapid method for unity scale. */
/*********************************/

	if (f->scale == 8)
	{
		for (y = 0; y < h; y++)
		{
			if (vdom)
			{
				vtbline = vdom->vtblines[line_index];
				g = vtbline->values;
			}

			intval = lines->intvs;
			for (i = 0; i < lines->nintvs; i++, intval++)
			{
				left = intval->ileft;
				right = intval->iright;

				if (vdom)
				{
					gtemp = g + idom->kol1 + left - vtbline->vkol1;
					stemp = temp + left;

					for (j = left; j <= right; j++) {
						*stemp++ = *gtemp++;
					}
				}

				if (maskbuffer)
					mask_interval(masktemp, (short)left, (short)right);
			}

			masktemp -= maskw;
			temp -= w;
			lines++;
			line_index++;
		}
	}


/*********************************/
/* Method for smaller scale.     */
/*********************************/

/*
 * This is based on scanning the destination
 * buffer and undersampling from the object.
 * This means that not every interval will
 * be used (eg half scale just uses every
 * other interval).
 * This is *much* faster than the old version
 * (quarter scale is somewhere around 4 to 5
 * times faster).
 * Also, if you look at the resulting data in
 * zoom detail, you will see that the object
 * is actually drawn better (rows were cut
 * short with the old version).
 */

	if (f->scale < 8)
	{
		int increment, xoffset, yoffset;
		struct intervalline *lines_start;
		GREY *gtemp_start;
		int line_index_start;

		scale = 8192/f->scale;

		lines_start = lines;
		line_index_start = line_index;

/* Start offset by half a scaled pixel so
 * that the object is centralized. */
		yoffset =  1 - (scale >> 1);

		for (y = 0; y < h; y++)
		{
			if (vdom)
			{
				vtbline = vdom->vtblines[line_index];
				g = vtbline->values;
			}

			intval = lines->intvs;
			for (i = 0; i < lines->nintvs; i++, intval++)
			{
				left = (f->scale*intval->ileft + 4) >> 3;
/* Under some circumstances, scaled
 * down right ends can be a pixel beyond
 * the scaled down width! I've only seen
 * this happen at 1/4 scale... This fix
 * clips the edge to the object width. */
				if ((right = (f->scale*intval->iright + 4) >> 3) >= w)
					right = w - 1;

				if (right < left)
					continue;

				xoffset = 1 - (scale >> 1);

				if (vdom)
				{
					gtemp = g + idom->kol1 + intval->ileft - vtbline->vkol1;
					gtemp_start = gtemp;

					stemp = temp + left;

					for (j = left; j <= right; j++)
					{
						*stemp++ = *gtemp;

						xoffset += scale;
						increment = xoffset >> 10;

						gtemp = gtemp_start + increment;
					}
				}

				if (maskbuffer)
					mask_interval(masktemp, (short)left, (short)right);
			}

			masktemp -= maskw;
			temp -= w;

			yoffset += scale;
			increment = yoffset >> 10;

			lines = lines_start + increment;
			line_index = line_index_start + increment;
		}
	}


/*********************************/
/* Method for larger scale.      */
/*********************************/

/*
 * This uses undersampling of the object to
 * fill each row, and row duplication with
 * memcpy().
 * This is up to 2 times faster than the old
 * version, and does not leave gaps like the
 * old one did!
 */

	if (f->scale > 8)
	{
		int increment, xoffset, yoffset = 0;
		GREY *gtemp_start;
		int line_index_start, old_line_index;
		int clip_left, clip_right;

		clip_left = start_clip_kol - idom->kol1;
		clip_right = end_clip_kol - idom->kol1;

		scale = 8192/f->scale;

		line_index_start = line_index;

		for (y = 0; y < h; )
		{
			if (vdom)
			{
				vtbline = vdom->vtblines[line_index];
				g = vtbline->values;
			}

			intval = lines->intvs;
			for (i = 0; i < lines->nintvs; i++, intval++)
			{
/* If extremities of interval are outside
 * the clip limits, do nothing. */
				if ((intval->iright < clip_left) ||
					(intval->ileft > clip_right))
					continue;

				if (intval->ileft < clip_left)
					left = 0;
				else
/*
					left = (f->scale*(intval->ileft - clip_left) + 4) >> 3;
*/
					left = (f->scale*(intval->ileft - clip_left)) >> 3;

/*
				if (intval->iright > clip_right)
					right = (f->scale*(clip_right - clip_left) + 4) >> 3;
				else
					right = (f->scale*(intval->iright - clip_left) + 4) >> 3;
*/
				if (intval->iright > clip_right)
					right = (f->scale*(clip_right - clip_left + 1) - 7) >> 3;
				else
					right = (f->scale*(intval->iright - clip_left + 1) -7) >> 3;



				xoffset = 0;

				if (vdom)
				{
					if (intval->ileft < clip_left)
						gtemp = g + start_clip_kol - vtbline->vkol1;
					else
						gtemp = g + idom->kol1 + intval->ileft - vtbline->vkol1;

					gtemp_start = gtemp;

					stemp = temp + left;

					for (j = left; j <= right; j++)
					{
						*stemp++ = *gtemp;

						xoffset += scale;
						increment = xoffset >> 10;

						gtemp = gtemp_start + increment;
					}
				}

				if (maskbuffer)
					mask_interval(masktemp, (short)left, (short)right);
			}

/* Duplicate this line if required. */
			old_line_index = line_index;
			while (1)
			{
				yoffset += scale;
				increment = yoffset >> 10;

				line_index = line_index_start + increment;
				y++;

				masktemp -= maskw;
				temp -= w;

				if ((line_index == old_line_index) && (y < h))
				{
					if (vdom)
						memcpy(temp, temp + w, w);

					if (maskbuffer)
						memcpy(masktemp, masktemp + maskw, maskw);
				}
				else
					break;
			}

			lines++;
		}
	}




/* MG:  Now flip data and mask buffers for inverted objects 
	This is totally by-passed for non-inverted objects 
	... so no slow down Bill, OK */

/* MG : handle horizontal flip */
	if (f->ix == -1) {

		/* reset f->ox ... modified at start of code */
		f->ox+=(obj->idom->lastkl - obj->idom->kol1);

		/* reverse the grey data */
		temp=outbuffer;
		for (i=0; i<h; i++) {
			g1temp=temp; g2temp=temp+w-1;
			for (j=0; j<w/2; j++) {
				val=*g1temp;
				*g1temp++=*g2temp;
				*g2temp--=val;
			}

			/* shift data to account for 4byte pixel padding */
			if (ixoffset) {
				unsigned char b;

				g1temp = temp;
				g2temp = temp + ixoffset;
				b = vdom ? vdom->bckgrnd : 0;

				for (j = 0; j < w - ixoffset; j++)
					*g1temp++ = *g2temp++;

				/* Clear excess padded data */
				for (j=0; j < ixoffset; j++)
					*g1temp++ = b;
			}

			temp+=w;
		}

		if (maskbuffer) {	/* do nothing if no mask */

			/* reverse the mask data*/
			temp=*maskbuffer;
			padshift=maskw*8 - w + ixoffset;
			revshift=8-padshift;

			for (i=0; i<h; i++) {

				/* reverse mask bits */
				g1temp=temp;
				for (j=0; j<maskw; j++) {
					val=*g1temp; val2=0;
					for (k=0;k<8;k++) {
						val2 >>= 1;
						val2 |= (val & 128);
						val <<= 1;
					}
					*g1temp++=val2;
				}
	
				/* reverse mask bytes */
				g1temp=temp; g2temp=temp+maskw-1;
				for (j=0; j<maskw/2; j++) {
					val=*g1temp;
					*g1temp++=*g2temp;
					*g2temp--=val;
				}

				/* ok now remove the mask padding shift */
				if (padshift) {
					g1temp=temp; g2temp=temp+1;
					for (j=0; j<maskw-1; j++) {
						*g1temp >>= padshift;
						*g1temp++ |= *g2temp++ << revshift;
					}
					*g1temp >>= padshift;	/* last byte on line*/
				}
				
				temp+=maskw;
			}
		}
	}


/* MG : handle vertical flip */		
	if (f->iy == -1) {

		/* reset f->oy .... modified at the start of the code */
		f->oy+=(obj->idom->lastln - obj->idom->line1);

		/* reverse grey data */
		temp1=outbuffer; temp2=outbuffer+(h-1)*w;
		for (i=0; i<h/2; i++) {
			g1temp=temp1; g2temp=temp2;
			for (j=0; j<w;j++) {
				val=*g1temp;
				*g1temp++=*g2temp;
				*g2temp++=val;
			}
			temp1+=w; temp2-=w;
		}

		if (maskbuffer) {	/* do nothing if no mask */

			/* reverse mask data */
			temp1=*maskbuffer; temp2=*maskbuffer+(h-1)*maskw;
			for (i=0; i<h/2; i++) {
				g1temp=temp1; g2temp=temp2;
				for (j=0; j<maskw; j++) {
					val=*g1temp;
					*g1temp++=*g2temp;
					*g2temp++=val;
				}
				temp1+=maskw; temp2-=maskw;
			}
		}
	}


/* Finally : return the data buffer */

	return(outbuffer);


} /* qbuffer */

