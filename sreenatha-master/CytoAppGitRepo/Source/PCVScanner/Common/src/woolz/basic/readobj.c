/*
 * readobj.c		Jim Piper		August 1983
 *
 * Routines to input object structures from sequential data streams.
 * When disc files are intended,
 * any necessary "fseek"-ing must be done elsewhere.
 *
 * The basic philosophy of the sequential form of an object is that
 * sub-objects are ordered in such a way that pointers are unnecessary.
 * See ../doc/filestructs.d for details.
 *
 *
*************************************************************************
 * #ifdef VTO - for reading files produced by VAX Woolz into OS9 Woolz.
 * This is very restricted - only handles type 1 objects without plists.
 * A problem is that 16-bit words are byte-swapped between VAX and
 * MC68000 but anything written a byte at a time is in the correct order.
 * So we have to ensure that "getword()" not only reads 32 bits, but in
 * the correct byte order.
 *************************************************************************
 *
 * Modifications
 *
 *	10Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *					Also, made minor mods to stop compiler warnings and removed
 *					standard C parameter lists (were '#ifdef i386' only).
 *	20Nov98		SN:	readrect: integer rects not read properly under NT cos of packing.
 *					Now does same as writerect.
 *	9/19/96		BP:		Cleanup VTO ifdefs to allow portable code.
 *						Forward declarations and return types.
 *						Return 0, not NULL.
 *	BP	7/18/95:	New pframe structure - change to readframe. Note file
 *					is stored in old format (for compatibility).
 *	29 Jul 92	MG	readpropertylist now uses fbread to match
 *					writepropertylist in fbwrite, however PC cannot read
 * 					MC68000 plists - pc has different internal data packing
 *					layout for the chromplist struct.
 *	13 Jul 92	MG	readrect assuming too much about float/int data storage
 *					-was OK on 68000 but not on 80x86 - same in writerect
 *	 7 Feb 91	CAS		Mizar/OSK defines
 * 29-01-91	JimP@MRC	put in a readpropertlist() for polygons, to match
 *						the change in writeobj.c.  ALL appropriate objects
 *						should have propertylist read/written.
 * 23-10-90	JimP@MRC	put in readcompounda for compound objects.  This
 *						also required the definition of a deliberate NULL
 *						object, obtained by type==0 and nothing else.
 *	17 Jan 89	CAS		readtext used 1 char more than allocated for string
 *	22 Nov 88	dcb		Added param to woolz_exit to say where called from
 *  2  Oct 88	dcb		Add read frame
 *	23 Nov 87	BDP		Changed convhull and histo to use only one block of
 *						memory
 *	20 Nov 87	BDP		Fixed readrect to read into correct field
 *	16 Nov 87	BDP		Added read convhull in the readobj 'style'.
 *	29 Oct 87	BDP		Protected Mallocs in readintervaldomain and readvaluetb
 *						against zero requests.
 *	14 May 87	BDP		protection against null or empty objs
 *	 9 Mar 87	CAS/GJP		Return a signed value from getword...
 *	 2 Mar 87	GJP			Woolz_exit
 *	17 Dec 86	CAS		Moved mfread into gplib + renamed fbread
 *	02 Dec 86	jimp	Use fast macro instead of getc()
 *	27 Nov 86	jimp	Return NULL if data type incorrect
 *	15 Oct 86	CAS		Get linkcount correct
 *	12 Sep 86	CAS		Includes
 */


#include <woolz.h>


#include <stdio.h>
#include <malloc.h> // For malloc
//#include <fbio.h>
#include "fbio.h"
//#include <wstruct.h>
//#include <gplib.h>
#include "gplib.h"

#ifdef WIN32
#include <fcntl.h>
#include <io.h>
#endif


/* #define	WTRACE */



#ifdef VTO
typedef	int	SGREY;
typedef	int	SCOORD;
typedef	int	SSMALL;
#else
typedef	short	SGREY;
typedef	short	SCOORD;
typedef	short	SSMALL;
#endif



/* Forward devlarations. */
static int readvaluetable();
static int readboundlist(FILE *fp, struct boundlist * *blist);


#ifdef VTO

/* "putw()" on Vax was 32 bits, least significant 16 first */
static
getword(FILE *fp)
{
	register unsigned i,j,k;
	i = mgetc(fp);
	j = mgetc(fp);
	k = mgetc(fp);
	return((int)((mgetc(fp)<<24)+(k<<16)+(j<<8)+i));
}

#else

static
getword(register FILE *fp)
{
	int	i;
	i = mgetc(fp);
	if (i > 127) i -= 256;
	i = i << 8;
	return(mgetc(fp)+i);
}

#endif

#ifdef VTO
struct object *
vreadobj(FILE *fp)
{
	printf("My old man's a banana\n");
	return(NULL);
}
#else

struct object *
readobj(FILE *fp)
{
	int type;
	struct object *obj,*makemain(),*readvector(),*readpoint();
	struct object *readtext(), *readrawim(), *readcircle();
	struct intervaldomain *idmn, *readintervaldomain();
	struct propertylist *ipp, *readpropertylist();
	struct polygondomain *poly, *readpolygon();
	struct boundlist *blist;
	struct cvhdom *cd, *readcvhdom();
	struct irect *rect, *readrect();
	struct histogramdomain *hdom, *readhisto();
	struct pframe *rframe, *readframe();
	struct object *readcompounda();

#ifdef WIN32
	/* Make sure the file is in binary mode. */
	_setmode(_fileno(fp), _O_BINARY);
#endif

	type = mgetc(fp);
#ifdef WTRACE
	fprintf(stderr,"Readobj, reading type %d object\n",type);
#endif

	switch (type) {
	case EOF:
	case 0:
		return(NULL);

	case 1:
		idmn = readintervaldomain(fp);
		obj = makemain( type, idmn, NULL, NULL, NULL);
		readvaluetable(fp,obj);
		obj->plist = readpropertylist(fp);
		if (feof(fp) != 0)
		{

#ifdef WTRACE
fprintf(stderr,"readobj unexpected eof found\n");
#endif

			return(NULL);
		}
		else
			return(obj);
		
#ifndef VTO
	case 10:
		poly = readpolygon(fp);
		if (poly == NULL)
			return(NULL);
		obj = makemain(type, (struct intervaldomain *)poly, NULL, NULL, NULL);
		obj->plist = readpropertylist(fp);
		if (feof(fp) != 0)
		{
#ifdef WTRACE
			fprintf(stderr,"readobj unexpected eof found\n");
#endif
			return(NULL);
		}
		else
			return(obj);

	case 11:
		if (readboundlist(fp, &blist) != 0)
			return(NULL);
		else
			return( makemain(type, (struct intervaldomain *)blist, NULL, NULL, NULL));

	case 12:
#ifdef CVTEST
		fprintf(stderr,"readobj: type 12 object being read \n");
#endif /* CVTEST */
		poly = readpolygon(fp);
		if (poly == NULL)
			return(NULL);
		else {
			cd = readcvhdom(fp);
			if (cd == NULL)
				return(NULL);
			else
				return( makemain(type, (struct intervaldomain *)poly, 
				                 (struct valuetable *)cd, NULL, NULL));
		}

	case 13:
		hdom = readhisto(fp);
		if (hdom == NULL)
			return(NULL);
		else
			return(makemain(type, (struct intervaldomain *)hdom, NULL, NULL, NULL));
	
	case 20:
		rect = readrect(fp);
		if (rect == NULL)
			return(NULL);
		else
			return(makemain(type, (struct intervaldomain *)rect, NULL, NULL, NULL));
		
	case 30:
	case 31:
		return(readvector(fp, type));

	case 40:
	case 41:
		return(readpoint(fp, type));
	case 60: /* frame */
		if ((rframe = readframe(fp)) == NULL)
			return(NULL);
		else {
			rframe->type = type;
			return((struct object *)rframe);
		}

	case 70:
		return(readtext(fp,type));

	case 80:
	case 81:
		return(readcompounda(fp,type));

	case 90:
		return(readrawim(fp));

	case 100:
		return(readcircle(fp));

	case 110:
		ipp=readpropertylist(fp);
		if (ipp==NULL)
			return(NULL);
		else
			return(makemain(110,NULL,NULL,ipp,NULL));
#endif

	default:
		fprintf(stderr,"Can't read object type %d\n",type);
		return(NULL);

	}
}

/* object protection against empty objects has been added to prevent zero
Malloc requests occuring. bdp 14/5/87  */

static struct intervaldomain *
readintervaldomain(register FILE *fp)
{
	register /*i,*/ l, ll;
	int type,l1,k1,kl,nints;
	struct intervaldomain *idmn, *makedomain();
	register struct intervalline *ivln;
	register struct interval *itvl;

	type = mgetc(fp);
	if (type == 0)
		return(NULL);

#ifdef WTRACE
fprintf(stderr,"readintervaldomain type %d\n", type);
#endif

	l1 = getword(fp);
	ll = getword(fp);
	k1 = getword(fp);
	kl = getword(fp);
	if (feof(fp) != 0)
		return(NULL);
	idmn = makedomain(type,l1,ll,k1,kl);

	if (wzemptyidom(idmn) == 0)
		switch (type) {
		case 1:
			nints = 0;
			ivln = idmn->intvlines;
			for (l=l1; l<=ll; l++) {
				ivln->nintvs = getword(fp);
				nints += ivln->nintvs;
				ivln++;
			}
			if (feof(fp) != 0)
				return(NULL);
			itvl = NULL;
			if (nints > 0)
				itvl = (struct interval *) Malloc(nints * sizeof(struct interval));
			idmn->freeptr = (char *) itvl;
			idmn->linkcount = 0;
			ivln = idmn->intvlines;
#ifdef VTO
			for (i=0; i<nints; i++,itvl++) {
				itvl->ileft = getword(fp);
				itvl->iright = getword(fp);
			}
			itvl = (struct interval *) idmn->freeptr;
#else
			fbread((char *)itvl, sizeof(struct interval), nints, fp);
#endif
			for (l=l1; l<=ll; l++) {
				nints = ivln->nintvs;
				makeinterval(l, idmn, nints, itvl);
				ivln++;
				itvl += nints;
			}
			break;
	
		case 2:
			break;
	
		default:
			fprintf(stderr,"illegal domain type %d\n",type);
			woolz_exit(52, "readintervaldomain");
	
		}

	if (feof(fp) != 0)
		return(NULL);
	else
		return(idmn);
}


static int
readvaluetable(register FILE *fp, struct object *obj)
{
	struct iwspace iwsp;
	struct intervaldomain *idmn;
	struct valuetable *vtb, *makevaluetb();
	int packing, type, kstart, l1, ll, vtblinearea;
	register int i;
	short sht;
	GREY *v;
    SGREY bckgrnd;
	register GREY *g, *gtop;

	type = mgetc(fp);
	
#ifdef WTRACE
fprintf(stderr,"readvaluetable type %d\n", type);
#endif

	switch (type) {
	case 0:
	case EOF:
		obj->vdom = NULL;
		return(0);

	case 1:
	case 11:
	case 21:
		type = 1;	/* a fiddle for case 11 */
		packing = mgetc(fp);

#ifdef WTRACE
fprintf(stderr,"readvaluetable packing %d\n", packing);
#endif

		idmn = obj->idom;
		l1 = idmn->line1;
		ll = idmn->lastln;
		fbread((char *) &bckgrnd, sizeof(SGREY), 1, fp);
		vtb = makevaluetb(1, l1, ll, bckgrnd, obj);
		obj->vdom = vtb;
		vtblinearea = linearea(obj);
		v = NULL;	/* default */
		vtb->linkcount = 0;
		if (vtblinearea > 0)
		{
			v = (GREY *) Malloc(vtblinearea * sizeof(GREY));
			vtb->linkcount = 1;
		}
		vtb->freeptr = (char *) v;
		initrasterscan(obj,&iwsp,0);
		while (nextinterval(&iwsp) == 0) {
			if (iwsp.nwlpos)
				kstart = iwsp.lftpos;
			switch (packing) {
#ifndef VTO
			case 1:
				fbread(v+iwsp.lftpos-kstart, sizeof(GREY), iwsp.colrmn, fp);
				break;
#ifndef OSK
			case 2:
				g = v+iwsp.lftpos-kstart;
				for (i=0; i<iwsp.colrmn; i++) {
					fread(&sht, sizeof(short), 1, fp);
					*g++ = (int) sht;
				}
				break;
#endif
#endif
			case 3:
				/*
				 * byte packing.
				 */
				g = v+iwsp.lftpos-kstart;
				gtop = g + iwsp.colrmn;
				while (g < gtop)
					*g++ = mgetc(fp);
				break;
			default:
				fprintf(stderr,"illegal grey-table packing %d\n",packing);
				woolz_exit(52, "readvaluetable.1");
			}
			if (iwsp.intrmn == 0) {
				makevalueline(vtb, iwsp.linpos, kstart, iwsp.rgtpos, v);
				v += (iwsp.rgtpos - kstart + 1);
			}
		}

		if (feof(fp) != 0)
			return(1);
		else
			return(0);

	case 2:
	case 12:
	case 22:
		fprintf(stderr,"Can't read float grey-table\n",type);
		woolz_exit(52, "readvaluetable.2");
	default:
		fprintf(stderr,"illegal grey-table type %d\n",type);
		woolz_exit(52, "readvaluetable.3");
	}
}




static struct propertylist *
readpropertylist(register FILE *fp)
{
	register int type, si/*, i*/;
	struct propertylist *pl;
	register char *ch;
#ifdef VTO
	static int warn=1;
#endif

	type = mgetc(fp);

#ifdef WTRACE
fprintf(stderr,"readpropertylist type %d\n", type);
#endif

	switch (type) {
	case EOF:
	case 0:
	default:
		return(NULL);
	case 1:
#ifdef VTO
		if (warn) {
			warn = 0;
			fprintf(stderr,"Warning - propertylist may not be converted\n");
		}
#endif
		si = getword(fp);

		/* check for silly length plist - MG 21/3/94 */
		if (si==0) {
			fprintf(stderr,"\nError: 0 length property list / cell header \n");
			return(NULL);
		}

		pl = (struct propertylist *) Malloc(si);
		pl->size = si;
		ch = (char *)pl;
		ch += sizeof(SMALL);
		si -= sizeof(SSMALL);

/* MG replace following with fbread to ensure byte swapping on 386
		for (; si > 0; si--)
			*ch++ = mgetc(fp);*/

		fbread(ch, 1, si, fp);
		if (feof(fp) != 0)
			return(NULL);
		else
			return(pl);
	}
}

#ifndef VTO


static struct polygondomain *
readpolygon(FILE *fp)
{
	int type;
	register int nvertices;
	struct polygondomain *poly, *makepolydmn();

	type = mgetc(fp);
	nvertices = getword(fp);
	if (feof(fp) != 0)
		return(NULL);
	if (type < 1 || type > 2)
		fprintf(stderr,"Illegal polygon domain type %d\n",type);
	poly = makepolydmn(type,NULL,0,nvertices,1);
	poly->nvertices = nvertices;
	switch (type) {

	case 1:
		fbread((char*)poly->vtx, sizeof(struct ivertex), nvertices, fp);
		break;

	case 2:
		fbread((char*)poly->vtx, sizeof(struct fvertex), nvertices, fp);
		break;
	}
	if (feof(fp) != 0)
		return(NULL);
	else
		return(poly);
}



static int
readboundlist(FILE *fp, struct boundlist * *blist)
{
	int type = mgetc(fp);
	switch (type) {
	case EOF :
		return(1);
	case 0:
		*blist = NULL;
		break;
	case 1:
		*blist = (struct boundlist *) Malloc (sizeof(struct boundlist));
		if (((*blist)->type = mgetc(fp)) == EOF)
			return(1);
		if (readboundlist(fp,&((*blist)->next)) != 0)
			return(1);
		if (readboundlist(fp,&((*blist)->down)) != 0)
			return(1);
		(*blist)->wrap = getword(fp);
		if (feof(fp))
			return(1);
		(*blist)->poly = readpolygon(fp);
		if ((*blist)->poly == NULL)
			return(1);
		break;
	}
	return(0);
}


static struct cvhdom *
readcvhdom(FILE *fp)
{
	int type;
	struct cvhdom cdomst;
	struct cvhdom *c;
	
	type = mgetc(fp);
	switch(type){
	case EOF:
		return(NULL);
	case 1:
		c = (struct cvhdom *) Malloc (sizeof( struct cvhdom));
		cdomst.type = type;
		cdomst.nchords = getword(fp);
		cdomst.nsigchords = getword(fp);
		cdomst.mdlin = getword(fp);
		cdomst.mdkol = getword(fp);
		if (feof(fp) != 0)
			return(NULL);
#ifdef CVTEST
		fprintf(stderr,"readobj: %d chords being read \n",cdomst.nchords);
#endif /* CVTEST */
		c = (struct cvhdom *) Malloc(sizeof( struct cvhdom) + 
			cdomst.nchords * sizeof( struct chord));

/*		_strass( c, &cdomst, sizeof(struct cvhdom));
	            ^ structure assignment - compiler should handle MG 30/6/92 */

		*c=cdomst;

		c->ch = (struct chord *) (c + 1);
		fbread((char*)c->ch, sizeof(struct chord), c->nchords, fp);
		if (feof(fp) != 0)
			return(NULL);
		return(c);
	}
	return(NULL);
}

//
//	MODS
//	SN 20Nov98	integer rects not read properly cos of packing.
//				Now does same as writerect.
//
static struct irect *
readrect(FILE *fp)
{
	register type;
	struct irect *ir;
	struct frect *fr;

	type = mgetc(fp);
	if (feof(fp) != 0)
		return(NULL);
	/* 20/11/87 bdp . following reads now to k[0] not rangle, this is still
	a horrible way of doing this even it is quick ! */
	
	switch (type) {
	case 1:
		ir = (struct irect *) Malloc (sizeof(struct irect));
		ir->type = type;
		//fbread(&(ir->irk[0]), sizeof(struct irect) - sizeof(SMALL), 1, fp);
		// SN 20Nov98 Changed as NT uses packing of 4 so above sizeof incorrect.
		fbread((char*)&(ir->irk[0]), sizeof(ir->irk[0])*8 + sizeof(ir->rangle), 1, fp);
		if (feof(fp) != 0)
			return(NULL);
		return(ir);
	case 2:
		fr = (struct frect *) Malloc (sizeof(struct frect));
		fr->type = type;
		fbread((char*)&(fr->frk[0]), sizeof(fr->frk[0])*8 + sizeof(fr->rangle), 1, fp);
		if (feof(fp) != 0)
			return(NULL);
		
		return((struct irect *)fr);

	}
}


static struct object *
readcircle(FILE *fp)
{
	struct circle *circ;

	circ = (struct circle *) Malloc (sizeof(struct circle));

	circ->type = 100;

	fbread((char*)&(circ->centre), sizeof(struct circle) - sizeof(SMALL), 1, fp);
	if (feof(fp) != 0)
		return(NULL);
	return((struct object *)circ);
}



static struct object *
readvector(FILE *fp, int type)
{
	struct ivector *iv;
	struct fvector *fv;
	switch (type) {
	case 30:
		iv = (struct ivector *) Malloc (sizeof(struct ivector));
		iv->type = type;
		fbread((char*)&(iv->style), sizeof(struct ivector) - sizeof(SMALL), 1, fp);
		if (feof(fp) != 0)
			return(NULL);
		return((struct object *)iv);
	case 31:
		fv = (struct fvector *) Malloc (sizeof(struct fvector));
		fv->type = type;
		fbread((char*)&(fv->style), sizeof(struct fvector) - sizeof(SMALL), 1, fp);
		if (feof(fp) != 0)
			return(NULL);
		return((struct object *)fv);
	}
}



static struct object *
readpoint(FILE *fp, int type)
{
	struct ipoint *iv;
	struct fpoint *fv;
	switch (type) {
	case 40:
		iv = (struct ipoint *) Malloc (sizeof(struct ipoint));
		iv->type = type;
		fbread((char*)&(iv->style), sizeof(struct ipoint) - sizeof(SMALL), 1, fp);
		if (feof(fp) != 0)
			return(NULL);
		return((struct object *)iv);
	case 41:
		fv = (struct fpoint *) Malloc (sizeof(struct fpoint));
		fv->type = type;
		fbread((char*)&(fv->style), sizeof(struct fpoint) - sizeof(SMALL), 1, fp);
		if (feof(fp) != 0)
			return(NULL);
		return((struct object *)fv);
	}
}



static struct object *
readrawim(FILE *fp)
{
	struct rawimage *raw;
	int size;
	
	raw = (struct rawimage *) Malloc (sizeof(struct rawimage));
	raw->type = 90;

	fbread((char*)&raw->width, sizeof(struct rawimage) - sizeof(SMALL), 1, fp);

	if (feof(fp) != 0)
		return(NULL);

	size=(sizeof(char) * raw->width * raw->height);

	raw->values=(char *)Malloc(size);

	fbread(raw->values, size, 1, fp);

	if (feof(fp) != 0)
		return(NULL);

	return((struct object *)raw);
}


static struct histogramdomain *
readhisto(FILE *fp)
{
	struct histogramdomain histost;
	struct histogramdomain *h;
	
	histost.type = mgetc(fp);
	fbread((char*)&histost.r, sizeof(struct histogramdomain) - sizeof(SMALL), 1, fp);
	if (feof(fp) != 0)
		return(NULL);
	h = (struct histogramdomain *) Malloc (sizeof(struct histogramdomain) +
			histost.npoints * (histost.type == 1? sizeof(int): sizeof(float)));

/*	_strass( h, &histost , sizeof(struct histogramdomain));
            ^ structure assignment - compiler should handle MG 30/6/92 */

	*h=histost;

	h->hv = (int *) (h + 1);
	fbread((char*)h->hv, (h->type == 1? sizeof(int): sizeof(float)), h->npoints, fp);
	if (feof(fp) != 0)
		return(NULL);
	return(h);
}

/*
 *	R E A D F R A M E  --  
 *
 * Old format structure is in file:
 * load and convert to new.
 */
static struct pframe *
readframe(FILE *fp)
{
	struct pframe *rf;
	struct old_pframe ff;

	fbread((char*)&(ff.scale), sizeof(struct old_pframe) - sizeof(SMALL), 1, fp);
	if (feof(fp) != 0)
		return(NULL);

	rf = (struct pframe *)Malloc(sizeof(struct pframe));

	rf->scale = ff.scale;
	rf->dx = (int)ff.dx;
	rf->dy = (int)ff.dy;
	rf->ox = ff.ox;
	rf->oy = ff.oy;
	rf->ix = ff.ix;
	rf->iy = ff.iy;

	return(rf);
}

/*
 *	R E A D T E X T  --
 *
 */
static struct object *
readtext(FILE *fp, int type)
{
	struct textobj *tobj;
	struct textdomain *tdom;
	char *tstring;
	int i;
	
	tdom = (struct textdomain *) Malloc(sizeof(struct textdomain));
	fbread((char*)tdom,sizeof(struct textdomain),1,fp);
	tstring = (char *) Malloc(tdom->stringlen+2);
	tobj = (struct textobj *)makemain(type, (struct intervaldomain *)tdom, 
	                                  (struct valuetable *)tstring, NULL, NULL);
	for(i = 0;i <= tdom->stringlen; i++)
		*tstring++ = mgetc(fp);
	*tstring = '\0';		/* just in case ! */
	return((struct object *) tobj);
}


static struct object *
readcompounda(FILE *fp, int type)
{
	extern struct object *readobj();
	struct compounda *c, *makecompounda();
	int i,n,otype;
	otype = getc(fp);
	n = getword(fp);
	c = makecompounda(type,1,n,NULL,otype); 
	for (i=0; i<n; i++)
		c->o[i] = readobj(fp);
	c->p = readpropertylist(fp);
	return ((struct object *) c);
}
	

#endif	/* VTO banana */

#endif


