
/*
 * textimage.c    Dr. Bill
 */

#include <woolz.h>


#include <commonmacro.h>
#include <stdio.h>




#define NTEXTFONTS	10

static TCHAR *text_fonts[NTEXTFONTS];

TCHAR *text_font_names[NTEXTFONTS] = {
	_T("helv.med.11"),
	_T("helv.med.14"),
	_T("helv.med.20"),
	_T("helv.med.25"),
	_T("helv.med.34"),
	_T("helv.bold.11"),
	_T("helv.bold.14"),
	_T("helv.bold.20"),
	_T("helv.bold.25"),
	_T("helv.bold.34")
};




/**************************************************** load_text_fonts */

/*
 * Load the text fonts from file.
 */

int load_text_fonts()
{
	FILE *f;
	int i, size;


	for (i = 0; i < NTEXTFONTS; i++)
	{
		if ((f = _tfopen(text_font_names[i], _T("rb"))) == NULL)
		{
			printf("Error opening font file %s.\n",
							text_font_names[i]);
			exit(1);
		}

		fseek(f, 0, SEEK_END);
		size = ftell(f);
		rewind(f);

		text_fonts[i] = (TCHAR *)malloc(size * sizeof(TCHAR));

		fread(text_fonts[i], sizeof(TCHAR), size, f);
	}

	return(1);
} /* load_text_fonts */




/**************************************************** create_bitmap_text */

/*
 * Draw a text string into a rectangular
 * char buffer.
 * The size of the buffer is calculated
 * here and returned through outw & outh.
 * A pointer to the buffer (which is
 * allocated here) is returned and must
 * be freed when finished with.
 */
// NB KR: This is never used - which is a good thing as this ain't the right code

TCHAR *create_bitmap_text(TCHAR *str, int *outw, int *outh, char colour, char bg, int font)
{
	TCHAR *outdata, *outtemp, *outtempsave, *c;
	TCHAR *fontptr, *fonttemp;
	int *offsetptr;
	TCHAR thisbit;
	register short i, j, w, h, totw = 0, toth = 0;


	if ((font < 0) ||
	    (font >= NTEXTFONTS))
		fontptr = text_fonts[1];
	else
		fontptr = text_fonts[font];

	c = str;

	while (*c != 0)
	{
		offsetptr = (int *)fontptr + (*c & 127);
		fonttemp = fontptr + *offsetptr;

		w = *fonttemp++;
		h = *fonttemp++;

		if (h > toth)
			toth = h;

		totw += w;

		c++;
	}

	*outw = totw;
	*outh = toth;

	if (totw*toth < 1)
		return(NULL);

	outdata = TStringAlloc(totw*toth);

	memset(outdata, bg, totw*toth);

	outtempsave = outdata;

	c = str;

	while (*c != 0)
	{
		offsetptr = (int *)fontptr + (*c & 127);
		fonttemp = fontptr + *offsetptr;

		w = *fonttemp++;
		h = *fonttemp++;
		thisbit = 1;

		for (j = 0; j < h; j++)
		{
			outtemp = outtempsave + (totw*j);

			for (i = 0; i < w; i++)
			{
				if (*fonttemp & thisbit)
					*outtemp = colour;

				outtemp++;

				thisbit = thisbit << 1;
				if (thisbit == 0)
				{
					thisbit = 1;
					fonttemp++;
				}
			}
		}

		outtempsave += w;
		c++;
	}

	return(outdata);
} /* create_bitmap_text */

