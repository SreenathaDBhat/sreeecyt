//////////////////////////////////////////////////////////////////////////////////////
//
//
//	TXTOBJ
//	Converts string into type 1 object
//
//	Dave Burgess, 02Dec98 (Windows version)
//
//	Modifications
//

#include <woolz.h>

#include <ddgs.h>

#include <stdio.h>
//#include <wstruct.h>
//#include <ddgs.h>

//extern HFONT _makefont(int, int);

//////////////////////////////////////////////////////////////////////////////////////////////
//
//	struct object *txtobj(char *s, int fontid, int scale)
//	Convert a text string to a type 1 woolz object
//	Use the specified (woolz) fontid, and scale
//
struct object *txtobj(LPTSTR s, int fontid, int scale)
{
	DDGS *dg;
	int len, w, nlines;
	struct object *newobj;
	HDC dc, memdc;
	HBITMAP bmp;
	char *rastarr;
	SIZE size;
	BITMAPINFO bi;
	HGDIOBJ orig_font, oldobj;
	HFONT font;

	if (s == NULL)
		return(NULL);

	if ((len = _tcslen(s)) < 1)
		return(NULL);

	if ((dg = getcurdg()) == NULL)
		return NULL;
										
	// Create a new memory DC which we can draw into invisibly.
	dc = GetDC(dg->window);
	if ((memdc = CreateCompatibleDC(dc)) == NULL) {
		ReleaseDC(dg->window, dc);
		// OK a woolz error would be more appropriate here
		MessageBox(dg->window, _T("Create DC failed"), _T("txtobj"), MB_OK);
		return NULL;
	}

	// May need to set direct 1:1 pixel:logical units
	//	SetMapMode(memdc, MM_TEXT);

	// Create a DDGS compatible font
	font = _makefont(fontid, scale);
	orig_font = SelectObject(memdc, font);

	// Get text size
	GetTextExtentPoint32(memdc, s, len, &size);

	// The size of the bitmap doesn't matter as long as it's large enough
	if ((bmp = CreateCompatibleBitmap(memdc, (size.cx * len), size.cy)) == NULL) {
		ReleaseDC(dg->window, dc);
		// ... again, a woolz error would be more appropriate
		MessageBox(dg->window, _T("Create Bitmap failed"), _T("txtobj"), MB_OK);
		return NULL;
	}
 	ReleaseDC(dg->window, dc);

	// Select the bitmap into the memory DC. (notwithstanding the manual page for GetDIBits())
	oldobj = SelectObject(memdc, bmp);

	// draw the text
	SetBkMode(memdc, TRANSPARENT);
	SetTextColor(memdc, RGB(255, 255, 255));
	TextOut(memdc, 0, 0, s, len);

	// Retrieve the bitmap
	// Allocate memory for raster array
	rastarr = malloc(size.cx * len * size.cy * 3 + 100);

	bi.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
	bi.bmiHeader.biWidth       = size.cx;
	bi.bmiHeader.biHeight      = -(LONG)size.cy;	// Negate height to invert.
	bi.bmiHeader.biPlanes      = 1;					// Always 1.
	bi.bmiHeader.biBitCount    = 24;				// For our purpose
	bi.bmiHeader.biCompression = BI_RGB;

	// Get the bitmap data. The biSizeImage parameter will also be filled in
	nlines = GetDIBits(memdc, bmp, 0, size.cy, rastarr, &bi, DIB_RGB_COLORS);

	// Finished with the DC & font
	SelectObject(memdc, oldobj);
	SelectObject(memdc, orig_font);
	DeleteDC(memdc);
	DeleteObject(font);

	// create object from framestore
	// work out the bitmap width
	w = bi.bmiHeader.biSizeImage / nlines;

	// Reduce width by 3 (Since it is an array of RGB)
	// Note: The background is also set to non-zero
	// to prevent splits etc. from chewing up text.
	{
		char *p1, *p2;
		int i, j;
		
		p1 = p2 = rastarr;
		for (i = 0; i < nlines; i++) {
			for (j = 0; j < w/3; j++) {
				*p1 = *p2;
				// Change background
// This has the unfortunate side-effect of generating
// an ouitliner around the text block
// so until this is fixed, we use 0 background
//				if (*p1 == 0)
//					*p1 = 0x1;
				p1++;
				p2 += 3;
			}

			p2 = rastarr + (i * w);
		}
	}

	newobj = (struct object *)fsconstruct(rastarr, (short)nlines, (short)(w/3), 1, 0);
	free(rastarr);

	/* return object  */
	return(newobj);
}

#if 0
Reference code
/**************************************************************************/

/* 
 *	TXTOBJ
 *
 *	converts string into type 1 text object
 *
 * BP - now remeber depth of pixmap, so we can
 * only reuse it if the dog depths are the
 * same. If the dog depth has changed, we need
 * to create a new pixmap.
 */

/* Keep hold of pixmap, so we don't need to
 * create/destroy every time... */
static int text_pixmap_width = 0;
static int text_pixmap_height = 0;
static Pixmap text_pixmap = 0;
static short text_pixmap_depth = 0;
static GC text_gc = NULL;

struct object *txtobj(s)
char *s;
{
	Display *display;
	Window window;
	int direction, ascent, descent;
	XCharStruct overall;
	DDGS *dg;
	XGCValues gcv;
	XImage *ximage;
	int i, width, height;
	short len;
	struct object *obj, *newobj;
	char *frame, *temp;

	if (!s)
		return(NULL);

	if ((len = _tcslen(s)) < 1)
		return(NULL);

	dg = getcurdg();

	display = dg->display;
	window = dg->window;

	XGetGCValues(display, dg->gc, GCFont, &gcv);

	XQueryTextExtents(display, gcv.font, s, len,
				&direction, &ascent, &descent, &overall);

	height = ascent + descent;
	width = overall.width;

	if ((temp = (char *)ddgs_get_aux_buffer(width*height)) == NULL)
		return(NULL);

	ximage = XCreateImage(display, dg->visual, dg->depth,
				ZPixmap, 0, temp, width, height,
				dg->depth == 8 ? 8 : 32, 0);

/* Destoy the existing pixmap if we need a bigger
 * one for this draw... */
	if ((width > text_pixmap_width) || (height > text_pixmap_height) ||
		(dg->depth != text_pixmap_depth))
		if (text_pixmap)
		{
			XFreeGC(display, text_gc);
			XFreePixmap(display, text_pixmap);
			text_pixmap = 0;
		}

/* Create pixmap to draw string into. */
	if (!text_pixmap)
	{
		text_pixmap_depth = dg->depth;
		text_pixmap = XCreatePixmap(display, window,
							width, height, dg->depth);
		text_pixmap_width = width;
		text_pixmap_height = height;

		gcv.background = 0;
		gcv.plane_mask = 0x00FFFFFF;
		gcv.function   = GXcopy;

		text_gc = (GC)XCreateGC(display, text_pixmap, GCBackground |
						GCFunction | GCPlaneMask | GCFont, &gcv);
	}
	else
		XSetFont(display, text_gc, gcv.font);

/* Clear the pixmap to zero. We need to do
 * this because XDrawImageString() may not cover
 * the whole area.... */
	XSetForeground(display, text_gc, 0);
	XFillRectangle(display, text_pixmap, text_gc, 0, 0,
				width + 1, height + 15);
	XSetForeground(display, text_gc, 0x00FFFFFF);

/* Draw string in pixmap. */
/* We can now just use XDrawString() because
 * of the above...
	XDrawImageString(display, text_pixmap, text_gc, 0, ascent, s, len);
*/
	XDrawString(display, text_pixmap, text_gc, 0, ascent, s, len);

/* Pull pixmap back into framestore - but do
 * it line-by-line for Univision. */
/*
	for (i = 0; i < height; i++)
		XGetSubImage(display, text_pixmap, 0, i, width, 1,
				0x00FFFFFF, ZPixmap, ximage, 0, i);
*/
	XGetSubImage(display, text_pixmap, 0, 0, width, height,
				0x00FFFFFF, ZPixmap, ximage, 0, 0);

/* If we are running 24-bit, need to use
 * every 4th byte from image. */
	if (dg->depth != 8)
	{
		frame = (char *)ximage->data;
		for (i = 0; i < width*height; i++)
			frame[i] = frame[i << 2];
	}

	/* create object from framestore */		
	newobj = (struct object *)fsconstruct(ximage->data, height, width, 1, 0);

/* BP - I still don't understand why we need this
 * stage. It works fine without for regular text display,
 * but crud appears when doing text on prints!!! Eh!?!?!?
 * Anyway, threshold() appears to clean this up - but it
 * should NOT be necessary! */
/*
	newobj = (struct object *)threshold(obj, 1);
	freeobj(obj);
*/

	ximage->data=NULL;
	XDestroyImage(ximage);							

	/* return object  */
	return(newobj);
}

#endif
