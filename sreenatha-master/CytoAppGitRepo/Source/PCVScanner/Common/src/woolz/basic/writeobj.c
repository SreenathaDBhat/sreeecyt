/*
 * writeobj.c		Jim Piper		August 1983
 *
 * Routines to convert object structures to sequential data streams.
 * When disc files are intended,
 * any necessary "fseek"-ing must be done elsewhere.
 *
 * The basic philosophy of the sequential form of an object is that
 * sub-objects are ordered in such a way that pointers are unnecessary.
 * See ../doc/filestructs.d for details.
 *************************************************************************
 * #ifdef OTV - for writing files produced by OS9 Woolz into VAX Woolz.
 * This is very restricted - only handles type 1 objects without plists.
 * A problem is that 16-bit words are byte-swapped between VAX and
 * MC68000 but anything written a byte at a time is in the correct order.
 * So we have to ensure that "putword()" not only writes 32 bits, but in
 * the correct byte order.
 *************************************************************************
 *
 * Modifications
 *
 *	10Mar2000	JMB		Changed headers included, as part of conversion to DLL.
 *						Also, made minor mods to stop compiler warnings and removed
 *						standard C parameter lists (were '#ifdef i386' only).
 *	10/22/96	BP:		Don't do stuff to errno!!!
 *	9/19/96		BP:		Change COORD to WZCOORD.
 *						Tweak VTO/OTV (??!!) ifdefs to allow portability.
 *						Put writeobj at bottom to avoid lots of forward decs.
 *
 *	BP	7/18/95:	New pframe structure (int dx/dy) required mod to
 *					writeframe (file still contains old format).
 *	13 Jul 91	MG	writerect assuming too much about float/int data storage
 *					-was OK on 68000 but not on 80x86 - same in readrect.
 *					The same technique (using fbwrite) is used in
 *					writevector, point, histo, frame, propertylist -
 *					however these appear to perform OK so far, so we are
 *					working on the principal of if its not broke dont fix
 *					it !
 *	28 Mar 91	CAS		OTV support of vwriteobj
 *	7 Feb 91	CAS		Added write stus checking to write compound object
 *						Mizar/OSK defines
 * 29-01-91	JimP@MRC	put in a writepropertlist() for polygons, to
 *						match the change in readobj.c. ALL appropriate
 *						objects should have	propertylist read/written.
 * 23-10-90	Jimp@MRC	put in writecompounda for compound objects
 *	 1 Jun 89	CAS		Global def of errno for V2.2
 *	22 Feb 89	CAS		Rest of mputc checking
 *	 1 Feb 89	CAS		More status checking (full for type 1 + 110)
 *	30 Jan 89	CAS		Start to do somthing about returning a status
 *						from writeobj
 *	22 Nov 88	dcb		Added param to woolz_exit to say where called from
 * 	2  Oct 88	dcb		Add type 60
 *	25 Nov 87	BDP		Protect write polygon against NULL domain
 *	20 Nov 87	BDP		Write of type 20 done from wrong field !
 *	16 Nov 87	BDP		Added type 12 object capability in same style as
 *						the rest
 *	2  Mar 87	GJP		Woolz_exit
 *	17 Dec 86	CAS		Moved mfwrite into gplib + renamed fbwrite
 * 03 Dec 86	jimp	Use fast macro instead of putc()
 * 27 Nov 86	jimp	Don't write type if incorrect, don't exit,
 *				instead print error and return.
 *	12 Sep 86	CAS		Includes
 *************************************************************************
 */


#include <woolz.h>


#include <stdio.h>
//#include <fbio.h>
#include "fbio.h"
//#include <wstruct.h>
//#include <gplib.h>
#include "gplib.h"

#ifdef WIN32
#include <fcntl.h>
#include <io.h>
#endif


#ifdef OTV
typedef	int	SGREY;
typedef	int	SCOORD;
typedef	int	SSMALL;
#else
typedef	short	SGREY;
typedef	short	SCOORD;
typedef	short	SSMALL;
#endif


#ifdef OTV

/* "getw()" on Vax is 32 bits, least significant 16 first */
static
putword(register unsigned w, FILE *fp)
{
	if (mputc(w&255, fp) != -1) {
		w >>= 8;
		if (mputc(w&255, fp) != -1) {
			w >>= 8;
			if (mputc(w&255, fp) != -1) {
				w >>= 8;
				if (mputc(w&255, fp) != -1)
					return(0);
			}
		}
	}
	return(-1);
}

#else

static
putword(register unsigned w, FILE *fp)
{
	if (mputc((w>>8)&255, fp) != -1)
		if (mputc(w&255, fp) != -1)
			return(0);
	return(-1);
}

#endif


#ifdef OTV

int
vwriteobj(FILE *fp, struct object *obj)
{
	printf("...and he wears a banana's hat.\n");
	return(0);
}

#else


int packcheck = 0;



static
writeintervaldomain(register FILE *fp, struct intervaldomain *idom)
{
	register int i, /*j,*/ nlines;
	register struct intervalline *ivln;
//	register struct interval *jtvl;

	if (idom == NULL) {
		return(mputc(0,fp));
	}
	if (mputc((char) idom->type, fp) == -1) return(-1);
	if (putword(idom->line1, fp) == -1) return(-1);
	if (putword(idom->lastln, fp) == -1) return(-1);
	if (putword(idom->kol1, fp) == -1) return(-1);
	if (putword(idom->lastkl, fp) == -1) return(-1);

	switch (idom->type) {

	case 1:
		nlines = idom->lastln - idom->line1;
		for (i=0; i<=nlines; i++)
			if (putword(idom->intvlines[i].nintvs, fp) == -1)
				return(-1);
		ivln = idom->intvlines;
		for (i=0; i<=nlines; i++) {
#ifdef OTV
			jtvl = ivln->intvs;
			for (j=0; j<ivln->nintvs; j++,jtvl++) {
				putword(jtvl->ileft,fp);
				putword(jtvl->iright,fp);
			}
#else
			/* much faster in OS9-68000 unfortunately */
			if (fbwrite((char *)ivln->intvs, sizeof(struct interval), ivln->nintvs, fp) != ivln->nintvs)
				return(-1);
#endif
			ivln++;
		}
		break;

	case 2:
		break;

	default:
		fprintf(stderr,"Can't write domain type %d\n",idom->type);
		woolz_exit(51, "writeintervaldomain");
		return(-1);
	}
	return(0);
}




static
writepropertylist(register FILE *fp, struct propertylist *plist)
{
	register char *pl;
	register int /*i, */si;
#ifdef VTO
	static int warn=1;
#endif
	if (plist == NULL) {
		return(mputc((char) 0, fp));
	} else {
#ifdef VTO
		if (warn) {
			warn = 0;
			fprintf(stderr,"Warning - propertylist may not be converted\n");
		}
#endif
		if (mputc((char) 1, fp) == -1)
			return(-1);
		if (putword(plist->size, fp) == -1)
			return(-1);
		pl = (char *) plist;
		pl += sizeof(SMALL);
		si = plist->size - sizeof(SSMALL);
		if (fbwrite(pl,1,si,fp) != si)
			return(-1);
	}
}



static
writevaluetable(register FILE *fp, struct object *obj)
{
	struct iwspace iwsp;
	struct gwspace gwsp;
	int packing, type;
	register int i, min, max;
	register GREY *g, *gtop;
	short sht;

	if (obj->vdom == NULL) {
		return(mputc(0,fp));
	}

	type = obj->vdom->type;
	if (mputc((char) (type%10), fp) == -1)
		return(-1);
	switch (type) {
		case 1:
		case 11:
		case 21:
			if (packcheck != 0) {
				min = 0;
				max = 255;
				initgreyscan(obj,&iwsp,&gwsp);
				while (nextgreyinterval(&iwsp) == 0) {
					g = gwsp.grintptr;
					for (i=0; i<iwsp.colrmn; i++) {
						if (*g < min)
							min = *g;
						else if (*g > max)
							max = *g;
						g++;
					}
				}
				if (min == 0 && max == 255)
					packing = 3;
#ifndef OSK
				else if (min > -32768 && max < 32768)
					packing = 2;
#endif
				else
					packing = 1;
			} else
				packing = 3;
			if (mputc((char) packing, fp) == -1) return(-1);
			if (putword(obj->vdom->bckgrnd, fp) == -1) return(-1);
			initgreyscan(obj,&iwsp,&gwsp);
			while(nextgreyinterval(&iwsp) == 0)
				switch (packing) {
#ifndef OTV
					case 1:
						if (fbwrite(gwsp.grintptr, sizeof(GREY), iwsp.colrmn, fp) != iwsp.colrmn)
							return(-1);
						break;
#ifndef OSK
					case 2:
						g = gwsp.grintptr;
						for (i=0; i<iwsp.colrmn; i++) {
							sht = (short) *g;
							if (fbwrite((char *)&sht, sizeof(short), 1, fp) != 1)
								return(-1);
							g++;
						}
						break;
#endif
#endif
					case 3:
						/*
						 * byte packing.
						 */
						g = gwsp.grintptr;
						gtop = g + iwsp.colrmn;
						while (g < gtop)
							if (mputc(*g++, fp) == -1)
								return(-1);
						break;

					default:
						fprintf(stderr,"Can't write packing type %d\n",packing);
						woolz_exit(53, "writevaluetable.1");
						return(-1);
					}
				break;
		case 2:
		case 12:
		case 22:
			fprintf(stderr,"Can't write float grey tables\n");
			woolz_exit(53, "writevaluetable.2");
			return(-1);
		default:
			fprintf(stderr,"Unknown grey table type %d\n",type);
			woolz_exit(53, "writevaluetable.3");
			return(-1);
	}
}

/*
 *	W R I T E P O L Y G O N  --
 *
 */
static
writepolygon(FILE *fp, struct polygondomain *poly)
{
	register int nvertices;

	nvertices = poly->nvertices;
	if (mputc((char) poly->type, fp) == -1) return(-1);
	if (putword(nvertices, fp) == -1) return(-1);
	switch (poly->type) {

	case 1:
		if (fbwrite((char*)poly->vtx, sizeof(struct ivertex), nvertices, fp) != nvertices)
			return(-1);
		break;

	case 2:
		if (fbwrite((char*)poly->vtx, sizeof(struct fvertex), nvertices, fp) != nvertices)
			return(-1);
		break;
	
	default:
		fprintf(stderr,"Illegal polygon type %d\n",poly->type);
		woolz_exit(51, "writepolygon");
	}
	return(0);
}


/*
 *	W R I T E B O U N D L I S T  --
 *
 */
static
writeboundlist(FILE *fp, struct boundlist *blist)
{
	if (blist == NULL) {
		return(mputc(0,fp));
	} else {
		if (mputc(1,fp) == -1) return(-1);
		if (mputc((char)blist->type,fp) == -1) return(-1);
		if (writeboundlist(fp,blist->next) == -1) return(-1);
		if (writeboundlist(fp,blist->down) == -1) return(-1);
		if (putword(blist->wrap,fp) == -1) return(-1);
		if (writepolygon(fp,blist->poly) == -1) return(-1);
	}
	return(0);
}

/*
 *	W R I T E C O N V H U L L  --
 *
 */
static
writeconvhull(register FILE *fp, struct cvhdom *cdom)
{
	if ( cdom == NULL ) {
		return(mputc((char) 0, fp));
	} else {
#ifdef CVTEST
		fprintf(stderr,"writeobj: %d chords being written \n",cdom->nchords);
#endif /* CVTEST */
		if (mputc((char) cdom->type ,fp) == -1) return(-1);
		if (putword(cdom->nchords, fp) == -1) return(-1);
		if (putword(cdom->nsigchords, fp) == -1) return(-1);
		if (putword(cdom->mdlin, fp) == -1) return(-1);
		if (putword(cdom->mdkol, fp) == -1) return(-1);
		if (fbwrite((char*)cdom->ch, sizeof(struct chord), cdom->nchords, fp) != cdom->nchords) return(-1);
	}
	return(0);	
}

/*
 *	W R I T E R E C T  --
 *
 */
static
writerect(FILE *fp, struct irect *rdom)
{
	struct frect *fr;

	if (rdom == NULL) {
		return(mputc(0,fp));
	} else {
		if (mputc(rdom->type,fp) == -1) return(-1);
		switch (rdom->type) {
			case 1:
				if (fbwrite((char*)&(rdom->irk[0]), sizeof(rdom->irk[0])*8 + sizeof(rdom->rangle), 1, fp) != 1) return(-1);
				break;
			case 2:
				fr=(struct frect *) rdom;
				if (fbwrite((char*)&(fr->frk[0]), sizeof(fr->frk[0])*8 + sizeof(fr->rangle), 1, fp) != 1) return(-1);
				break;
		}
	}
	return(0);
}
			
/*
 *	W R I T E C I R C L E  --
 *
 */
static
writecircle(FILE *fp, struct circle *circ)
{
	if (fbwrite((char*)&circ->centre, sizeof(struct circle) - sizeof(SMALL), 1, fp) != 1)
		return(-1);
	else
		return(0);
}
			
/*
 *	W R I T E V E C T O R  --
 *
 */
static
writevector(int fp, struct ivector *vec)
{
	switch (vec->type) {
		case 30:
			if (fbwrite((char*)&vec->style, sizeof(struct ivector) - sizeof(SMALL), 1, fp) != 1) return(-1);
			break;
		case 31:
			if (fbwrite((char*)&vec->style, sizeof(struct fvector) - sizeof(SMALL), 1, fp) != 1) return(-1);
			break;
	}
	return(0);
}
			
/*
 *	W R I T E P O I N T  --
 *
 */
static
writepoint(int fp, struct ipoint *pnt)
{
	switch (pnt->type) {
		case 40:
			if (fbwrite((char*)&pnt->style, sizeof(struct ipoint) - sizeof(SMALL), 1, fp) != 1) return(-1);
			break;
		case 41:
			if (fbwrite((char*)&pnt->style, sizeof(struct fpoint) - sizeof(SMALL), 1, fp) != 1) return(-1);
			break;
	}
	return(0);
}

/*
 *	W R I T E R A W I M  --
 *
 */
static
writerawim(FILE *fp, struct rawimage *raw)
{
	if (fbwrite((char*)&raw->width, sizeof(struct rawimage) - sizeof(SMALL),1,fp) != 1) return(-1);
	if (fbwrite(raw->values, (sizeof(char) * raw->width * raw->height), 1, fp) != 1) return(-1);
	return(0);
}


/*
 *	W R I T E H I S T O  --
 *
 */
static
writehisto(FILE *fp, struct histogramdomain *h)
{
	if (mputc(h->type, fp) == -1) return(-1);
	if (fbwrite((char*)&h->r, sizeof(struct histogramdomain) - sizeof(SMALL),1,fp) != 1) return(-1);
	if (fbwrite((char*)h->hv, (h->type == 1? sizeof(int): sizeof(float)), h->npoints, fp) != h->npoints) return(-1);
	return(0);
}


/*
 *	W R I T E F R A M E  --
 *
 */
static
writeframe(FILE *fp, struct pframe *frame)
{
	struct old_pframe ff;

	ff.scale = frame->scale;
	ff.dx = (WZCOORD)frame->dx;
	ff.dy = (WZCOORD)frame->dy;
	ff.ox = frame->ox;
	ff.oy = frame->oy;
	ff.ix = frame->ix;
	ff.iy = frame->iy;
	ff.func = NULL;

	if (fbwrite((char*)&(ff.scale), sizeof(struct old_pframe) - sizeof(SMALL), 1, fp) != 1)
		return(-1);

	return(0);
}


/*
 *	W R I T E T E X T  --
 *
 */
static
writetext(FILE *fp, struct textobj *tobj)
{
	struct textdomain *tdom;
	TCHAR *tstring,c;
	int i;

	if (fbwrite((char*)tobj->tdom,sizeof(struct textdomain),1,fp) != 1) return (-1);
	tstring = tobj->text;
	tdom = tobj->tdom;
	for(i = 0;i <= tdom->stringlen; i++) {
		c = tstring[i];
		if (mputc(c,fp) == -1)
			return(-1);
	}
	return(0);
}


static
writecompounda(FILE *fp, struct compounda *c)
{
	int i;
	if (mputc(c->otype, fp) == -1)
		return(-1);
	if (putword(c->n, fp) == -1)
		return(-1);
	for (i=0; i<c->n; i++) {
		if (c->o[i] == NULL) {
			if (mputc(0, fp) == -1)
				return(-1);
		} else {
#ifdef OTV
			if (vwriteobj(fp,c->o[i]) != 0)
#else
			if (writeobj(fp,c->o[i]) != 0)
#endif
				return(-1);
		}
	}
	return(writepropertylist(fp,c->p));
}


int
writeobj(FILE *fp, struct object *obj)
{
	int stat = 1;

#ifdef WIN32
	/* Make sure the file is in binary mode. */
	_setmode(_fileno(fp), _O_BINARY);
#endif

	if (obj == NULL) {
		fprintf(stderr,"Writeobj: NULL object ignored\n");
		return(1);
	}

	switch (obj->type) {
	case 1:
		if (mputc((char) obj->type, fp) != -1)
			if (writeintervaldomain(fp,obj->idom) != -1)
				if (writevaluetable(fp,obj) != -1)
					if (writepropertylist(fp,obj->plist) != -1)
						stat = 0;
		break;

	case 10:
		/* better to ignore this rather than write a NULL polygon cos readobj
		would at best return a NULL and terminate an input stream */
		
		if (obj->idom == NULL){
			fprintf(stderr,"Writeobj: NULL polygon ignored\n");
			return(stat);
		}
		if (mputc((char) obj->type, fp) != -1)
			if (writepolygon(fp, (struct polygondomain *)obj->idom) != -1)
				if (writepropertylist(fp,obj->plist) != -1)
					stat = 0;
		break;

	case 11:
		if (mputc((char) obj->type, fp) != -1) {
			writeboundlist(fp, (struct boundlist *)obj->idom);
			stat = 0;
		}
		break;

	case 12:
		if (mputc((char) obj->type, fp) != -1) {
			writepolygon(fp, (struct polygondomain *)obj->idom);
			writeconvhull(fp, (struct cvhdom *)obj->vdom);
			stat = 0;
		}
		break;
		
	case 13:
		if (mputc((char) obj->type, fp) != -1) {
			writehisto(fp, (struct histogramdomain *)obj->idom);
			stat = 0;
		}
		break;

	case 20:
		if (mputc((char) obj->type, fp) != -1) {
			writerect(fp, (struct irect *)obj->idom);
			stat = 0;
		}
		break;

	case 30:
	case 31:
		if (mputc((char) obj->type, fp) != -1) {
			writevector(fp, obj);
			stat = 0;
		}
		break;

	case 40:
	case 41:
		if (mputc((char) obj->type, fp) != -1) {
			writepoint(fp, (struct ipoint *)obj);
			stat = 0;
		}
		break;

	case 60:	/* frame */
		if (mputc((char) obj->type, fp) != -1) {
			writeframe(fp, (struct pframe *)obj);
			stat = 0;
		}
		break;

	case 70:
		if (mputc((char) obj->type, fp) != -1) {
			writetext(fp, (struct textobj *)obj);
			stat = 0;
		}
		break;

	case 80:
	case 81:
		if (mputc((char) obj->type, fp) != -1) {
			if (writecompounda(fp, (struct compounda *)obj) != -1)
				stat = 0;
		}
		break;

	case 90:
		if (mputc((char) obj->type, fp) != -1) {
			writerawim(fp, (struct rawimage *)obj);
			stat = 0;
		}
		break;

	case 100:
		if (mputc((char) obj->type, fp) != -1) {
			writecircle(fp, (struct circle *)obj);
			stat = 0;
		}
		break;

	case 110:
		if (mputc((char) obj->type, fp) != -1)
			if (writepropertylist(fp,obj->plist) != -1)
				stat = 0;
		break;

	default:
		fprintf(stderr,"Can't write object type %d\n",obj->type);
	}
	return(stat);
}


#endif	/* OTV banana */

