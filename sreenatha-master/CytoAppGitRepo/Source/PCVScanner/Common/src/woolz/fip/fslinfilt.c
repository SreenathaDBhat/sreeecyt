
/*
 *	fslinfilt.c     M.Gregson 23/6/94
 *
 *      linfilt :
 *  Routine to perform a linear nxm convolution over an image.
 *  Note pixels in the border of the image (n/2, m/2 ) are undefined
 *  and set to 0.
 *
 *  Convolution is assumed integer.
 *
 * Modifications:
 *
 *	10Mar2000	JMB		Changed headers included, as part of conversion to DLL.
 *						Also, made minor mods to stop compiler warnings and removed
 *						standard C parameter lists (were '#ifdef i386' only).
 *	9/20/96		BP:		Use FSCHAR, not FSCHAR - remove local definition
 *						of FSCHAR.
 *
 */

#include <woolz.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>
//#include <wstruct.h>




FSCHAR *
linfilt(FSCHAR *image1, int width, int height, struct convolution *filter)
{
	
	FSCHAR *imptr1, *imptr2, *image2;
	FSCHAR *mask/*, *mptr*/;
	int *fptr, sum, /*i,*/ x, y, col, line, halfx, halfy, size;
	int image_offset, mask_offset, masksize;
	

	size=width*height;

	/* check filter is OK */

	if ((filter->xsize <= 0) || ((filter->xsize & 1) == 0)
	|| (filter->ysize <= 0) || ((filter->ysize & 1) == 0)) {
		fprintf(stderr,"\nERROR: linfilt sent a stupid sized convolution\n");
		return(NULL);
	}

	if (filter->type != 50) {
		fprintf(stderr,"\nERROR: linfilt only accepts integer convolutions\n");
		return(NULL);
	}

	/* allocate space for resultant image */

	image2=(FSCHAR *)Calloc(size,1);
	if (image2 == NULL) {
		fprintf(stderr,"\nERROR: linfilt failed to malloc new image buffer\n");
		return(NULL);
	}

	/* allocate space for filter mask */
	masksize=filter->xsize * filter->ysize;
	mask=(FSCHAR *)Malloc(masksize);

	/* get filter halfwidths */
	halfx=filter->xsize / 2;
	halfy=filter->ysize / 2;

	/* set up image pointers */
	image_offset=halfx + halfy * width;
	mask_offset=width-filter->xsize;		
	
	/* Process image */
	for (line=halfy; line<height-halfy; line++) {

		imptr2=image2+halfx+line*width;

		for (col=halfx; col<width-halfx; col++) {
		
			imptr1=image1 + ((imptr2-image_offset)-image2);
			fptr=filter->cv; 
			sum=0;

			/* convolve data from image1 with filter */
			for (y=0; y<filter->ysize; y++) {

				for (x=0; x<filter->xsize; x++) {
					sum+=(*fptr) * (int)(*imptr1);
					fptr++;
					imptr1++;
				}

				imptr1+=mask_offset;
			}

			/* scale into image */
			sum/=filter->divscale;
			sum+=filter->offset;
			
			/* get modulus if requested */
			if (filter->modflag)
				sum=abs(sum);

			/* store result in image2 */
			*imptr2=(FSCHAR)sum;

			imptr2++;
		}
	}

	return(image2);
}
