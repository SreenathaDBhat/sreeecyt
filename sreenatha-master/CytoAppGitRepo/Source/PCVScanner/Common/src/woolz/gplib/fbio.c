/*
 *      F B I O . C  --  Fast buffered i/o routines 
 *
 *
 *  Written: Clive A Stubbings
 *           Image Recognition Systems
 *           720 Birchwood Boulevard
 *           Birchwood Science Park
 *           Warrington
 *           Cheshire
 *           WA3 7PX
 *
 *	Copyright (c) and intellectual property rights Image Recognition Systems
 *
 *  Date:    17th December 1986
 *
 *	Original code by Jim Piper MRC Dec 1986
 *
 *  Modifications
 *
 *	9/20/96		BP:		Remove comment from fbread() function declaration.
 *
 *	22 Jun 1992		MC			Commented out fbopen(), fbclose(), also modified fbread()
 *							and fbwrite() to byte swapping if i386 m/c.
 *
 *	22 Feb 1991		CAS			Whoops.. Make sure we flush the file on close
 *	30 Jan 1991		CAS			Mod to fbclose - make sure clib can't get at buffer
 *	15 Dec 1989		CAS			Added 'x' + fixed open mode in fbopen
 *	31 Jan 1989		CAS			Mod fbopen to explicitly use open + create
 *								rather than use fopen which sees to have file
 *								lock problems
 *								Return status from fwrite
 *	 3 Oct 1988		CAS			Make malloc into Malloc + free buffer if open fails
 *	 3 Jun 1987		CAS			Return propper count from fbread
 *	22 Dec 1986		CAS			Make fbread return count for compatability
 *								fread
 *	19 Dec 1986		CAS			Clear _UNBUF in flags on open
 *								and check malloc.
 *	18 dec 1986		CAS			Removed the bloody 'exit()' from fbopen
 *
 */

#include <woolz.h>

#include <stdio.h>
//#include <fbio.h>
#include "../basic/fbio.h"
/*#include <modes.h>*/

/*
 *   F B O P E N
 *
FILE *fbopen(name,flag,bufsiz)
char *name, *flag;
{
	char	*buf, *fptr, *Malloc();
	FILE	*f;
	int		fd, mode, append, forceopen, forcecreate;

	if ((buf = Malloc(bufsiz)) == 0)
		return(NULL);
	mode = append = forceopen = forcecreate = 0;
	fptr = flag;
	while (*fptr) {
		switch (*fptr++) {
			case 'r':
				mode |= S_IREAD;
				forceopen = 1;
				break;
			case 'w':
				mode |= S_IWRITE;
				break;
			case 'a':
				append = 1;
				break;
			case '+':
				if (mode & S_IWRITE) forcecreate = 1;
				mode |= (S_IREAD|S_IWRITE);
				break;
			case 'd':
				mode |= S_IFDIR;
				break;
			case 'x':
				mode |= S_IEXEC|S_IOEXEC;
				break;
		}
	}
	fd = -1;
	if (!forcecreate)
		fd = open(name,mode);
	if ((fd == -1) && !forceopen)
		fd = create(name,mode,S_IREAD|S_IWRITE|S_IOREAD);
	if (fd == -1) {
		Free(buf);
		return(NULL);
	}
	f = fdopen(fd,flag);
	if (append) fseek(f,0,2);
	f->_base = buf;
	f->_flag |= _BIGBUF;
	f->_flag &= ~_UNBUF;
	f->_ptr = f->_end = f->_base + bufsiz;
	f->_bufsiz = bufsiz;
	return(f);
} fbopen() commented out 22/6/92 mc */

/*
 *   F B C L O S E
 *
fbclose(f)
FILE *f;
{
	char *buf = f->_base;
	fflush(f);
	f->_base = f->_ptr = f->_end = NULL;
	f->_bufsiz = 0;
	fclose(f);
	Free(buf);
} fbclose() commented out 22/6/92 mc */

/*
 * simplified fread making use of mgetc macro
 *
 * modified for byte swapping if i386 m/c 22/6/92 mc
 * s is size of structure, n is no. of blocks
 *
 * Returns no of bytes read
 */
#ifdef WIN32
int
fbread(register char *p, register int s, register int n, register FILE *fp)
#endif
#ifdef i386
int
fbread(p, s, n, fp)
register char *p;
register int s;
register int n;
register FILE *fp;
#endif
{
	register	int	t;
	char temp;
	s *= n; t = s;
	
#ifdef i386

	s = s/2;
	while (s-- > 0) {
		*++p = mgetc(fp);
		*--p = mgetc(fp);
		p+=2;
		if (feof(fp)) {
			break;
		}
	}
	return(t-s-1);

#elif defined(WIN32)

	s >>= 1;
	while (s-- > 0)
	{
		temp = mgetc(fp);
		*p++ = mgetc(fp);
		*p++ = temp;
		if (feof(fp))
			break;
	}
	return(t - s - 1);

#else

	while (s-- > 0) {
		*p++ = mgetc(fp);
		if (feof(fp)) {
			break;
		}
	}
	return(t-s-1);
#endif i386
}

/*
 * simplified fwrite making use of mputc macro
 *
 * modified for byte swapping 22/6/92 mc
 */
#ifdef WIN32
int
fbwrite(register char *p, register int s, register int n, register FILE *fp)
#endif
#ifdef i386
int
fbwrite(p, s, n, fp)
register char *p;
register int s;
register int n;
register FILE *fp;
#endif
{
	char temp;

	s *= n;
#ifdef i386

	s = s/2;
	while (s-- > 0)
		{
		if (mputc(*++p,fp) == -1)
			return(0);
		mputc(*--p,fp);
		p+=2;
		}
	return(n);

#elif defined(WIN32)

	s >>= 1;
	while (s-- > 0)
	{
		temp = *p++;
		mputc(*p++, fp);
		mputc(temp, fp);
	}
	return(n);

#else

	while (s-- > 0)
		if (mputc(*p++,fp) == -1)
			return(0);
	return(n);
#endif i386
}



