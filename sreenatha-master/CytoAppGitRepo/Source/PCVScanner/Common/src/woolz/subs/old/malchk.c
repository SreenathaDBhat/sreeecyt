
/*
 *	malchk	M.Gregson 28/7/93
 *
 *	Scans file malloc.log for unmatched malloc/calloc - free pairs
 *	Reports total number of mallocs and frees and generates malerr.log
 *	marking unmatched mallocs and frees if any occur.
 *	7/9/93 dcb	Increased size of scan list, by using no. of lines in file as
 *				maximum limit
 */
#include <stdio.h>

#ifdef WIN32
int
main(int argc, char * *argv)
#endif
#ifdef i386
int
main(argc, argv)
int argc;
char * *argv;
#endif
{
	FILE *mal, *malerr, *pfp;
	char c;
	char s[80];
	unsigned int n, nlines, max_free_errors;
	unsigned int *list, *freelist;
	unsigned int index, maxindex, count, freecount, freeindex;
	unsigned int malloc_errors, free_errors;

	printf("\nChecking malloc.log for errors ....\n");

	if ((mal=fopen("malloc.log","r"))==NULL) {
		printf("cant open malloc.log");
		exit(0);
	}

	pfp = popen("wc malloc.log", "r");
	fscanf(pfp, "%d", &nlines);
	pclose(pfp);

	list=(unsigned int *)malloc (nlines * sizeof(unsigned int));
	max_free_errors = nlines/4;
	freelist=(unsigned int *)malloc (max_free_errors * sizeof(unsigned int));
	index=0;

	/* scan malloc.log and build up list of Mallocs and Callocs */
	
	while ( !(feof(mal)) && (index < nlines) ) {
		fscanf(mal,"%c",&c);
		if (c=='#')
			fscanf(mal,"%s\n",s);	/* ignore comments */
		else 
			fscanf(mal,"%x\n",&n);

		if ((c=='M') || (c=='C') || (c=='R')) {
			list[index++]=n;
		}
	}

	maxindex=index;
	printf("number of mallocs=%d\n",maxindex);



	/* rescan and check off all Frees against Malloc/Calloc list */
	freecount=0;
	free_errors=0;

	rewind(mal);

	while ( !(feof(mal)) ) {
		fscanf(mal,"%c",&c);
		if (c=='#')
			fscanf(mal,"%s\n",s);	/* ignore comments */
		else 
			fscanf(mal,"%x\n",&n);

		if (c=='F') {
			for (index=0; index<maxindex; index++) {
				if (list[index]==n) {
					list[index]=0;
					n=0;
					break;
				}
			}
			if (n!=0) {
				/* unmatched free */
				if (free_errors < max_free_errors)
					freelist[free_errors++]=freecount;
			}

			freecount++;
		}
	}
	printf("number of frees=%d\n",freecount);

	malloc_errors=0;
	for (index=0; index<maxindex; index++) {
		if (list[index]!=0)
			malloc_errors++;
	}

	printf("number of unmatched mallocs=%d \n",malloc_errors);
	printf("number of unmatched frees  =%d \n",free_errors);

	if (malloc_errors>0 || free_errors>0) {

		printf("see malerr.log for details\n");

		/* generate malerr.log marking unmatched mallocs and frees */

		index=0;
		freeindex=0;
		freecount=0;

		rewind(mal);

		malerr=fopen("malerr.log","w");

		while ( !(feof(mal)) && (index<=maxindex) ) {
			fscanf(mal,"%c",&c);
			if (c=='#') {
				fscanf(mal,"%s\n",s);	/* ignore comments */
				fprintf(malerr,"%c %s\n",c,s);
			}
			else {
				fscanf(mal,"%x\n",&n);

				switch (c) {
					/* mark unmatched mallocs */
					case 'M':
						if (list[index++]!=0) {
							fprintf(malerr,"%c %x ERROR : unmatched malloc\n",c,n);
						}
						else fprintf(malerr,"%c %x\n",c,n);
						break;
					case 'C':
						if (list[index++]!=0) {
							fprintf(malerr,"%c %x ERROR : unmatched calloc\n",c,n);
						}
						else fprintf(malerr,"%c %x\n",c,n);
						break;
					case 'R':
						if (list[index++]!=0) {
							fprintf(malerr,"%c %x ERROR : unmatched realloc\n",c,n);
						}
						else fprintf(malerr,"%c %x\n",c,n);
						break;
					case 'F':
						/* mark unmatched frees */
						if ((freecount==freelist[freeindex])
						&& (freeindex<free_errors)) {
							fprintf(malerr,"%c %x ERROR : unmatched free\n",c,n);
							freeindex++;
						}
						else fprintf(malerr,"%c %x\n",c,n);
						freecount++;
						break;
					default:
						break;
				}

			}
		}
		
		fclose(malerr);

	}

	fclose(mal);
	
}

	

	
