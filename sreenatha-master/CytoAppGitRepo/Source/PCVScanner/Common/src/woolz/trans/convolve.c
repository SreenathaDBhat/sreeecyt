
/*
 * convolve.c	Jim Piper	July 1983
 *
 * Mods:
 *
 *	10Mar2000	JMB	Changed headers included, as part of conversion to DLL.
 *					Also, made minor mods to stop compiler warnings and removed
 *					standard C parameter lists (were '#ifdef i386' only).
 *	WH	10Dec96:	Change WIN32 parameter to long then cast to struct 
 *					convolution *. passed to seqpar which also takes (*median())
 *	BP	7/28/95:	Re-rewrite! Do scaling based on cvl values only.
 *					cvl->divscale and cvl->offset must now be supplied
 *					with sensible values - this function now clips at
 *					0-255 (for 8-bit woolz).
 *	BP	12/13/94:	Rewrite for 8-bit woolz.
 * 13-01-92	ipoole	fields `scale`, `offset` and `modflag` added to 
 *			struct convolution and respected in convolve().
 *			Note that these facilites are particularly useful
 *			when processing UBYTE images.
 * 13-01-92	ipoole  functions convsum(), applyconvolution_ow() and
 *			applyconvolution_new() added. 
 * 14-01-92	ipoole	convolve() now respects xsize and ysize in convolution.
 *			so it can be used to implement 1D convolutions.
 */


#include <woolz.h>

#include <stdlib.h>
#include <stdio.h>
//#include <wstruct.h>
//#include <seqpar.h>
#include "seqpar.h"


extern struct sppar sppar;


/*
 * General space-domain convolution, needs a convolution structure,
 * which is itself the domain structure of a type 50 object,
 * though not usually used in this fashion.
 * Typical use :
 *	seqpar(obj,0,0,0,2,0,&laplac4,convolve);
 * or better :
 *	applyconvolution_ow (obj, &laplac4);
 *
 * BP - 12/13/94: Major rewrite.
 *	This function will now handle byte grey values.
 *	The function will always return a valid grey
 *	level (ie 0 thru 255) by clipping (regardless
 *	of scaling).
 *	cvl->divscale should normally be set to 1 (one)
 *	so that the return value will be properly scaled.
 *	Automatic 'true' scaling is performed if divscale
 *	is 1. If divscale is 0 (zero) then no scaling is
 *	performed. Other values of divscale will be applied
 *	directly.
 *
 * BP - 7/28/95: Re-rewrite (update to above comments).
 *	No longer attempt "true" internal scaling here.
 *	Scaling is done by cvl->divscale and cvl->offset,
 *	and the result is clipped at 0-255.
 */

int
convolve(int cv_long)
{
	register int k, *c;
	register GREY *a;
	short i, ly;
	register short j, lx;
	register int val;

/* 
** Because seqpar takes this and other functions as parameters which have
** different calling parameters and the VC++ compilers not as sloppy
*/

#ifdef WIN32
	struct convolution *cvl = (struct convolution *)cv_long;
#endif

	lx = (cvl->xsize - 1)/2; 
	ly = (cvl->ysize - 1)/2;

	k = 0;
	c = cvl->cv;
	for (i = -ly; i <= ly; i++)
	{
		a = sppar.adrptr[i] - lx;
		for (j = -lx; j <= lx; j++)
		{
			if ((val = (*a++)) == 0)
				val = *(sppar.adrptr[0]);

			val *= (*c++);
			k += val;
		}
	}

/* Rescale the value, if sensible! */
	if (cvl->divscale != 0)
		k /= cvl->divscale;

/* Add the offset. */
	k += cvl->offset;

/* Make sure returned grey level is a
 * sensible value. This is a new addition -
 * it is now more important to make sure
 * that divscale is set appropriately (one
 * would/should normally set divscale = 1). */
	if (k > 255)
		k = 255;
	if (k < 0)
		k = 0;

	if (cvl->modflag)
		k = abs (k);

	return (k);
}


/*
 * applyconvolution_ow - apply the given convolution operator to an object,
 * over-writing the original object.
 */
int
applyconvolution_ow(struct object *obj, struct convolution *conv)
{
	/* 
	 * Must pass half-size of largest dimention to seqpar
	 */
	int size = ((conv->xsize > conv->ysize ? conv->xsize: conv->ysize)-1)/2;
	seqpar(obj, 0, 0, 0, size, 0, (int)conv, convolve);	
}

/*
 * applyconvolution_new - apply the given convolution operator to an object,
 * returning a new object leaving the original unchanged.
 */
int
applyconvolution_new(struct object *obj, struct convolution *conv)
{
	/* 
	 * Must pass half-size of largest dimension to seqpar
	 * (we should modify seqpar to deliver rectangular neighbourhoods
	 * and so gain efficiency ...)
	 */
	int size = ((conv->xsize > conv->ysize ? conv->xsize: conv->ysize)-1)/2;
	return (int)(seqpar(obj, 1, 0, 0, size, 0, (int)conv, convolve));	
}



/*
 * convsum - return the sum of values in a convolution operator.
 */
int
convsum(struct convolution *conv)
{
	int             i, sum = 0;

	for (i = 0; i < (conv->xsize * conv->ysize); i++)
		sum += conv->cv[i];
	return (sum);
}


/*
 * onedxconv - convert a convolution to one dimension (in x), being the
 * middle line of the original.
 */
int
onedx_conv(struct convolution *conv)
{
	conv->cv += conv->xsize * ((conv->ysize - 1) / 2);
	conv->ysize = 1;
}

/*
 * Adjust the scaling of a convolution such that the components effectively
 * sum to one.
 */
int
normalise_conv(struct convolution *conv)
{
	int s = convsum (conv);
	if (s==0) {
		fprintf (stderr, "Can't normalise a zero summing convolution!\n");
		return(0);
	}
	conv->divscale = s;
}


/*
 * Adjust a convolution structure so that convolve() will perform
 * a modulus on the convolution result.
 */
int
modulus_conv(struct convolution *conv)
{
	conv->modflag = 1;
}
