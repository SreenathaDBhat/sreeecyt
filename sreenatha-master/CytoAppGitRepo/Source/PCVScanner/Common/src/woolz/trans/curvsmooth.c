/*
 * curvsmooth.c		Jim Piper
 *
 * Modifications:
 *
 *	9/20/96		BP:		Return type void, remove unnecessary return call.
 *
 */

#include <woolz.h>

#include <stdio.h>
//#include <wstruct.h>


/*
 * smooth a histogram by applying a low pass filter.
 * It is assumed to be a curvature histogram, which has last point
 * and first point referring to same boundary location,
 * so we wrap round at the ends.
 */
#ifdef WIN32
void
curvsmooth(struct object *histo, int iterations)
#endif
#ifdef i386
void
curvsmooth(histo, iterations)
struct object *histo;
int iterations;
#endif
{
	register i;
	register int *iv;
	register int k, km2, km1, kml, n, id;
	struct histogramdomain *hdom;

	hdom = (struct histogramdomain *) histo->idom;
	id = 1;
	/*
	 * iterate 1-1-1-1-1 smoothing
	 */
	for (n=0; n<iterations; n++) {
		id *= 5;
		iv = hdom->hv;
		km2 = *(iv + hdom->npoints -3);
		km1 = *(iv + hdom->npoints -2);
		kml = *(iv+1);
		for (i=3; i<hdom->npoints; i++) {
			k = *iv;
			*iv = km2 + km1 + k + *(iv+1) + *(iv+2);
			iv++;
			km2 = km1;
			km1 = k;
		}
		/* last point requires saving of wrap-by-2 */
		*(iv++) = km2 + km1 + *iv + *(iv+1) + kml;
		/* wrap round point */
		*iv = *(hdom->hv);
	}
	/*
	 * divide vertex values by 5 ** iterations
	 */
	iv = hdom->hv;
	for (i=0; i<hdom->npoints; i++) {
		*iv /= id;
		iv++;
	}
}
