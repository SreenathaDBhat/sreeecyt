
#include <woolz.h>
#define MAX_WOOLZ_SPLITS 2000


long rec_deagglomerate(struct object *AgglomObj, struct object **DeAgglomObjList, BOOL init)
{
	struct object *newobj, *ObjList_before[200], *ObjList_after[200];
	int count_before, count_after, i;
	static BOOL lowest_found = FALSE;
	static int pos=0;

	if (init)
		pos=0;

   	label_area(AgglomObj, &count_before, ObjList_before, 200, 1);      // Min area of one
	for (i = 0; i < count_before; i++)
		vdom21to1(ObjList_before[i]);
	newobj = erosion(AgglomObj);
   	label_area(newobj, &count_after, ObjList_after, 200, 1);      // Min area of one						
	for (i = 0; i < count_after; i++)
		vdom21to1(ObjList_after[i]);

	// has something dissappeared altogether
	if (count_after <= 0)
	{
		// Do nothing
	}
	else
	{

		BOOL split = FALSE;
		if (count_after >  count_before)
			split = TRUE;
		lowest_found = FALSE;

		for (int i=0; i<count_after; i++)
		{
			rec_deagglomerate(ObjList_after[i], DeAgglomObjList, FALSE);
			if (split && lowest_found == FALSE)
			{
				// found a core draw obj as 0 in whiteim
				DeAgglomObjList[pos++] = duplicate_obj(ObjList_after[i]);
				lowest_found = TRUE;
			}
		}
	}

	for (i=0; i<count_before; i++)
		freeobj(ObjList_before[i]);
	for (i=0; i<count_after; i++)
		freeobj(ObjList_after[i]);
	freeobj(newobj);

	return pos;
}




struct object *deagglomerate(struct object *AgglomObj)
{
	int i, line1, lastln, kol1, lastkl;
	struct object *skel_inv_agglom_obj, *agglom_obj, *ret_obj=NULL;
	struct object *TempObjList[MAX_WOOLZ_SPLITS];

	for (i = 0; i < MAX_WOOLZ_SPLITS; i++)
		TempObjList[i] = NULL;

	int pos = rec_deagglomerate(AgglomObj, TempObjList, TRUE);

	if (pos == 0)
	{
		ret_obj = duplicate_obj(AgglomObj);
	}
	if (pos > 0)
	{
		// Construct a rectangular white object
		line1=AgglomObj->idom->line1-100;
		lastln=AgglomObj->idom->lastln+100;
		kol1=AgglomObj->idom->kol1-100;
		lastkl=AgglomObj->idom->lastkl+100;
		agglom_obj=makerect(line1, lastln, kol1, lastkl, NULL, NULL, NULL, NULL);
		agglom_obj->vdom = newvaluetb(agglom_obj,1,255);

		// Diff the eroded seperated objects out of the white rectangle
		for (i=0; i<pos; i++)
		{
			agglom_obj = diffdom(agglom_obj, TempObjList[i]);
			freeobj(TempObjList[i]);
		}

		// Skeletonise to leave cut lines
		skel_inv_agglom_obj = skeleton(agglom_obj, 1);

		// mask the cutlines out of the original agglomerated object
		ret_obj = diffdom(AgglomObj, skel_inv_agglom_obj);

		freeobj(skel_inv_agglom_obj);
		freeobj(agglom_obj);
	}


	return ret_obj;
}

