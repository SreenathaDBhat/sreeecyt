/*
 * intersect2.c	Jim Piper	August 1983
 * make intersections of 2 domain, value table objects.
 *
 * Modifications
 *
 *	13 Sep 1986		CAS		Includes
 */

#include "woolz.h"

#include <stdio.h>
//#include <wstruct.h>


#ifdef WIN32
struct object *
intersect2(struct object *obj1, struct object *obj2)
#endif
#ifdef i386
struct object *
intersect2(obj1, obj2)
struct object *obj1;
struct object *obj2;
#endif
{
	struct object *objs[2], *intersectn();
	objs[0] = obj1;
	objs[1] = obj2;
	return (intersectn(objs,2,0));
}
