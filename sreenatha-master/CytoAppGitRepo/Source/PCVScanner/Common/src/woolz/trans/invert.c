/*
 * invert.c	Jim Piper	July 1983
 *
 * Modifications
 *
 *	9/20/96		BP:		Return type void.
 *
 *	03 Jul 2000		MC		Added invertimage function
 *	10 Dec 1987		BDP		Fixed a stray printf with a missing parameter
 *	08 May 1987		BDP		protection against null or empty objs
 *	13 Sep 1986		CAS		Includes
 */

#include <woolz.h>

#include <stdio.h>
//#include <wstruct.h>

/*
 * invert grey-table of object.
 * protection in this case in fact provided by protection in greyrange
 * and the interval scanning routines themselves, no additional changes
 * necessary here.  bdp 8/5/87
 */
#ifdef WIN32
void
invert(struct object *obj)
#endif
#ifdef i386
void
invert(obj)
struct object *obj;
#endif
{
	GREY min,max,range;
	register GREY *g;
	register int i;
	struct iwspace iwsp;
	struct gwspace gwsp;

#ifdef WTRACE
	fprintf(stderr,"invert object\n");
#endif /* WTRACE */

	greyrange(obj,&min,&max);
	range = max + min;
#ifdef DEBUG
	fprintf(stderr,"invert: greyrange aggregate is %d\n",range);
#endif /* DEBUG */
	initgreyscan(obj,&iwsp,&gwsp);
	while (nextgreyinterval(&iwsp) == 0) {
		g = gwsp.grintptr;
		for (i=0; i<iwsp.colrmn; i++) {
			*g = range - *g;
			g++;
		}
	}
}

#ifdef WIN32
void
invert_direct(struct object *obj)
#endif
#ifdef i386
void
invert(obj)
struct object *obj;
#endif
{
	GREY min,max,range;
	register GREY *g;
	register int i;
	struct iwspace iwsp;
	struct gwspace gwsp;

#ifdef WTRACE
	fprintf(stderr,"invert object\n");
#endif /* WTRACE */

	greyrange(obj,&min,&max);
	range = max + min;
#ifdef DEBUG
	fprintf(stderr,"invert: greyrange aggregate is %d\n",range);
#endif /* DEBUG */
	initgreyscan(obj,&iwsp,&gwsp);
	while (nextgreyinterval(&iwsp) == 0) {
		g = gwsp.grintptr;
		for (i=0; i<iwsp.colrmn; i++) {
			*g = 255 - *g;
			g++;
		}
	}
}

void fsinvertimage(FSCHAR *frame, int cols, int lines)
{
	register FSCHAR *imptr;
	register int size;
	int val;

	imptr = frame;

	size = cols * lines;
	do {
		val = *imptr;
		*imptr++ = (FSCHAR)(255 - val);
	} while (--size);
}
	
		

