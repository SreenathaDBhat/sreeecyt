/*
 * newconvolve.c	Jim Piper	July 1983
 *
 *	Modifications
 *
 *	2-4-91		GJP		Taken from MRC JS mods
 */

#include <stdio.h>
#include <wstruct.h>
#include <seqpar.h>

extern struct sppar sppar;


/*
 * General space-domain convolution, needs a convolution structure,
 * which is itself the domain structure of a type 50 object,
 * though not usually used in this fashion.
 * Typical use :
 *	seqpar(obj,0,0,0,2,0,&laplac4,convolve);
 */

#ifdef WIN32
int
convolve(struct convolution *cvl)
#endif
#ifdef i386
int
convolve(cvl)
struct convolution *cvl;
#endif
{
	register int k, *c;
	register GREY *a, bkgr = 0;
	register i,j,l;
	l = sppar.brdrsz;
	k = 0;
	c = cvl->cv;
	for (i= -l; i <= l; i++) {
		a = sppar.adrptr[i] - l;
		for (j= -l; j <= l; j++)
				if (*a == 0 ) {
					k += *(sppar.adrptr[0]) * ((short) (*c++));
					a++;
				}
				else
		 			k += *a++ * ((short) (*c++));
	}
	return (k);
}
