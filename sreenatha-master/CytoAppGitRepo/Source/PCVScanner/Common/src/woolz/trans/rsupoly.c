/*
 * rsupoly.c		Jim Piper	November 30 1983
 * polygon domain operations
 *
 * Modifications
 *
 *	10 Mar 2000		JMB		Changed headers included, as part of conversion to DLL.
 *							Also, made minor mods to stop compiler warnings and removed
 *							standard C parameter lists (were '#ifdef i386' only).
 *	18 Nov 1988		dcb		woolz_check_obj() instead of wzcheckobj()
 *	25 Nov 1987		BDP		Fixed a problem with Null polygon handling
 *	08 May 1987		BDP		protection against null or empty objs
 *	24 Sep 1986		GJP		Big Free not little free
 *	13 Sep 1986		CAS		Includes
 */

#include <woolz.h>

#include <stdio.h>
#include <malloc.h> // For free
//#include <wstruct.h>

/*
 * convert an integer polygon with unspecified inter-point
 * spacing into a real-valued polygon by constructing approximately 
 * equally spaced points and then smoothing (iter iterations).
 *
 * then move along this polygon in unit steps, and output the
 * resulting unit-spaced polygon
 * Protection added on the input object. if NULL object  supplied then a
 * null is returned . bdp 8/5/87
 */

#ifdef WIN32
struct object *
realsmoothunitpoly(struct object *polyobj, int iter)
#endif
#ifdef i386
struct object *
realsmoothunitpoly(polyobj, iter)
struct object *polyobj;
int iter;
#endif
{
	struct object /* *newpolyobj,*/ *makemain();
	struct polygondomain *poly,*newpoly,*unitspace();

	if (woolz_check_obj(polyobj, "realsmoothunitpoly") != 0)
		return(NULL);
	/*
	 * copy input polygon to new, real polygon with new vertices
	 * so that points spaced 1 apart
	 */
	poly = unitspace((struct polygondomain *)polyobj->idom);
	/*
	 * smooth - linear filter for now (try splines later ?)
	 */
	polysmooth(poly, iter);
	/*
	 * copy smoothed polygon to new, real polygon with new vertices
	 * so that points spaced 1 apart
	 */
	newpoly = unitspace(poly);
	/*
	 * remove unwanted space and
	 * return a complete object
	 */
	/* 25/11/87 check that neither of these routines are NULL */
	
	if ( poly != NULL )
		Free(poly);
	if ( newpoly == NULL)
		return(NULL);
	else
		return(makemain(10, (struct intervaldomain *)newpoly, NULL, NULL, polyobj));
}
