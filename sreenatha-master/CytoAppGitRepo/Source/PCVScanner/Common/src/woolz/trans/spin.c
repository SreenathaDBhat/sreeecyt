    /*
     *
     *	S P I N . C --
     *
     *           Image Recognition Systems
     *           720 Birchwood Boulevard
     *           Birchwood Science Park
     *           Warrington
     *           Cheshire
     *           WA3 7PX
     *
     *	Written: J. Piper
     *           Medical Research Council
     *           Clinical and Population Cytogenetics Unit
     *           Edinburgh.
     *
     *	Copyright (c) and intellectual property rights Image Recognition Systems
     *
     *  Date:    November 1983
     *
     *	Description: Rotate object by specified number of degrees. Alternatively, 
     *			 perform a more general rotational linear transformation specified 
     *			 by rotation matrix.
     *
     * 			 If type 1 object, do this by rotating into a sufficiently
     * 			 large object with 1 interval per line.
     * 			 If original object non-negative everywhere, then threshold
     * 			 rotated object to remove blank bays, inclusions, and
     * 			 structures less than 2 pixels high or wide.
     *
     * 			 The rotation is performed about the object mid-point, and
     * 			 in scaled integer arithmetic so that it will run reasonably
     * 			 fast on machines without hardware floating point (in such cases
     * 			 it may be necessary to re-code much of this with "long"
     * 			 integer declarations).
     * 			 In particular, it will produce funny results if the grey values
     * 			 lie outside the range [1, 32767]
     *
     * Modifications
     *
     *	10 Mar 2000		JMB		Changed headers included, as part of conversion to DLL.
     *							Also, made minor mods to stop compiler warnings and removed
     *							standard C parameter lists (were '#ifdef i386' only).
	 *	04 Nov 1998		SN		For MFISH. Modified mode_spinsqueeze() so no thresholding is applied
	 *							otherwise 0 valued points are thrown out of object.
     *	18 Mar 1997		MG		added mode_spinsqueeze() and nothresh_spinsqueeze()
     *	9/20/96		BP:		Forward declarations and return types for functions.
     *	17 May 1993		MG		added new parameter 'extent' to handle scale up of objects
     *						- used when generating scaled up idom 
     *						- note rottrans was never intended to scale up !
     *	20 Oct 1992		MG		wzemptyidom test only needed for type 1 object - moved
     *	29 Sep 1992		MG		poly and boundary now spin round midpoint - same as type 1 objects
     *	 7 Feb 1991		CAS		Mizar define -> OSK
     *	22 Nov 1988		dcb		Added param. to woolz_exit to say where called from
     *	18 Nov 1988		dcb		woolz_check_obj() instead of wzcheckobj()
     *	11 May 1987		BDP		protection against null or empty objs
     *	 2 Mar 1987		GJP		Woolz_exit
     *	13 Oct 1986		GJP		Little escapaded into newvaluttables
     *	 9 Oct 1986		CAS		Changed a freedom, Free to a freeobj
     *	24 Sep 1986		GJP		Big Free not little free
     *	13 Sep 1986		CAS		Includes
     */
    
    /* #define DEBUG */
    #ifdef DEBUG
    	FILE *dbfile;
    	struct tm *gm;
    	time_t t;
    #endif /* DEBUG */
    
    

    #include <woolz.h>
    
    #include <stdio.h>
    #include <math.h>
    #include <malloc.h>
    //#include <wstruct.h>
    
    static int sin1 = 0;
    static int cos1 = 0;
    static int sin2 = 0;
    static int cos2 = 0;
    static int nll = 0;
    static int nl1 = 0;
    static int nkl = 0;
    static int nk1 = 0;
    static int turnline = 0;
    static int turnkol = 0;
    static int fractline = 0;
    static int fractkol = 0;
    static int lineorigin = 0;
    static int kolorigin = 0;
    
    
    
    /* use mode instead of weighted mean */
    static int usemode = 0;
    
    /* do zero pixel threshold at end of rotation */
    static int dothresh = 1;

    /* Forward declarations. */
    static void turn();
    static void limits();
    static void stepkl();
    
    
    static double xscale,yscale,s,c;
    
    /*
     * Protection added. the functions spin, spinsqueeze, squeeze are the public
     * functions, all of which perform no direct operations upon an object, but
     * pas it to rotrans for processing. for this reason objec tchecking is in
     * that module. for null objects null is returned, for empty a copy of the 
     * original obejct is returned.  bdp  11/5/87
     */
     
    #ifdef WIN32
    struct object *
    spin(struct object *obj, double radians)
    #endif
    #ifdef i386
    struct object *
    spin(obj, radians)
    struct object *obj;
    double radians;
    #endif
    {
    	struct object *rottrans();
    
    #ifdef WTRACE
    	fprintf(stderr,"spin by %d radians\n",radians);
    #endif /* WTRACE */
    	xscale = 1.0;
    	yscale = 1.0;
    	return(rottrans(obj,radians));
    }
    
    
    #ifdef WIN32
    struct object *
    spinsqueeze(struct object *obj, double *radians, double *xs, double *ys)
    #endif
    #ifdef i386
    struct object *
    spinsqueeze(obj, radians, xs, ys)
    struct object *obj;
    double *radians;
    double *xs;
    double *ys;
    #endif
    {
    	struct object *rottrans();
    	double norot=0.0;
//    	double scale;
//    	int i,n;
//    	struct object *oldobj, *newobj;
    
    #ifdef WTRACE
    	fprintf(stderr,"spinsqueeze %d radians, xscale %d, yscale %d\n",
    *radians,*xs,*ys);
    #endif /* WTRACE */
    	xscale = *xs;
    	yscale = *ys;
    	return(rottrans(obj,*radians));
    }
    
    
    /*
     *	mode_spinsqueeze 
     *
     *	Calculates mode of 4 pixels during rotation - rather than mean
     *	- gives better results on solid colour objects - don't get
     *	intermediate colours produced at colour boundaries 
	 *
	 *	MODS
	 *	SN	04Nov98	Dont allow thresholding to remove 0 valued points
	 *				from object. TODO IN UNIX CV.
     */
#ifdef WIN32
    struct object *
	mode_spinsqueeze(struct object *obj, double *radians, double *xs, double *ys)
#endif
#ifdef i386
    struct object *mode_spinsqueeze (obj,radians,xs,ys)
    struct object *obj;
    double *radians,*xs,*ys;
#endif
    {
    	struct object *rottrans();
    	struct object *newobj;
    
    	usemode=1;
    	dothresh=0;			//SN04Nov98
    	xscale = *xs;
    	yscale = *ys;
    	newobj=rottrans(obj,*radians);
    	usemode=0;
    	dothresh=1;			//SN04Nov98
    	return(newobj);
    }
    
    /*
     *	nothresh_spinsqueeze
     *
     *	Does not perform threshold to remove zero pixels at the end of rotation
     *	- very useful when you don't want your object to get chopped to shit
     *	by rotation.
     */
#ifdef WIN32
	struct object *
	nothresh_spinsqueeze(struct object *obj, double *radians, double *xs, double *ys)
#endif
#ifdef i386
	struct object *nothresh_spinsqueeze (obj,radians,xs,ys)
    struct object *obj;
    double *radians,*xs,*ys;
#endif
    {
    	struct object *rottrans();
//    	double scale;
//    	int i,n;
    	struct object *newobj;
    
    	dothresh=0;
    	xscale = *xs;
    	yscale = *ys;
    	newobj=rottrans(obj,*radians);
    	dothresh=1;
    	return(newobj);
    }
    
    
    

    #ifdef WIN32
    struct object *
    squeeze(struct object *obj, double xs, double ys)
    #endif
    #ifdef i386
    struct object *
    squeeze(obj, xs, ys)
    double xs;
    double ys;
    #endif
    {
    	struct object *rottrans();
    #ifdef WTRACE
    	fprintf(stderr,"squeeze, xscale %d, yscale %d\n",xs,ys);
    #endif /* WTRACE */
    
    	xscale = xs;
    	yscale = ys;
    	return(rottrans(obj,0.0));
    }
    
    
    #ifdef WIN32
    struct object *
    rottrans(struct object *obj, double radians)
    #endif
    #ifdef i386
    struct object *
    rottrans(obj, radians)
    struct object *obj;
    double radians;
    #endif
    {
    	register l,i,j,k;
    	int extent;
    	int x,y,minx,maxx,miny,maxy;
    	int mfractline,mfractkol;
    	register GREY *g;
    	GREY g4[4];
    	struct object *makemain(), *nobj, *tobj, *threshold();
    	struct intervaldomain *idom, *jdom, *makedomain();
    	struct iwspace iwsp;
    	struct gwspace gwsp;
    	register struct interval *jtvl, *itvl;
    	struct valuetable *newvaluetb();
    	struct polygondomain *pdom,*npoly,*spinpoly();
    	struct boundlist *blist,*nblist,*spinblist();
    	double radius;
    	int mode, max, test, count;
    
    	/* first check the supplied object */
    	
    	if ( woolz_check_obj(obj, "rottrans") != 0) 
    		return(NULL);
    
    	s = 256.0 * sin(radians);
    	c = 256.0 * cos(radians);
    	sin1 = (int) (xscale*s);
    	cos1 = (int) (yscale*c);
    	sin2 = (int) (xscale*c);
    	cos2 = - (int) (yscale*s);
    
    	switch (obj->type) {
    	case 1:
    		if ( wzemptyidom(obj->idom) > 0) 
    			return(makemain(1, newidomain(obj->idom), NULL, NULL, NULL ));
    
    		if (obj->vdom == NULL)
    		{
    #ifdef WTRACE
    			fprintf(stderr,"spin, value domain does not exist\n");
    #endif /* WTRACE */
    			return(makemain(1, newidomain(obj->idom), NULL, NULL, NULL));
    		}
    		idom = obj->idom;
    		/*
    		 * find line limits of rotated object
    		 */
    		lineorigin = (idom->line1 + idom->lastln)/2;
    		kolorigin = (idom->kol1 + idom->lastkl)/2;
    		turn (lineorigin,kolorigin);
    		nll = turnline;
    		nl1 = nll;
    		nkl = turnkol;
    		nk1 = nkl;
    		initrasterscan(obj,&iwsp,0);
    		while (nextintervalendpoint(&iwsp) == 0)
    			limits(iwsp.linpos,iwsp.colpos);
    		nl1--;
    		nll++;
    		nk1--;
    		nkl++;
    		/*
    		 * make a domain structure
    		 */
    		
    		/* deal with scale up : MG 17/5/93 */
    		extent=1+(int) yscale;
    		if (extent<2)
    			extent=2;
    
    		jdom = makedomain(1, nl1, nll, nk1, nkl);
    		itvl = (struct interval *) Malloc ((nll-nl1+extent+1)*sizeof(struct interval));
    		jtvl = itvl;
    		jdom->freeptr = (char *) itvl;
    		for (l=nl1; l<=nll; l++) {
    			jtvl->ileft = nkl;
    			jtvl->iright = nk1;
    			makeinterval(l, jdom, 1, jtvl);
    			jtvl++;
    		}
    		/*
    		 * find column bounds for each line in rotated object.
    		 * when considering effect of any given line in input
    		 * picture, extend to adjacent lines to ensure enough
    		 * space at edges.
    		 */
    
    
    		initrasterscan(obj,&iwsp,0);
    		while (nextinterval(&iwsp) == 0) {
    			turn(iwsp.linpos, iwsp.colpos);
    			for (j=0; j<iwsp.colrmn; j++) {
    				jtvl = itvl + turnline - nl1;
    				for (i=0; i<extent; i++) {
    					if (jtvl->ileft > turnkol)
    						jtvl->ileft = turnkol;
    					if (jtvl->iright < turnkol+1)
    						jtvl->iright = turnkol+1;
    					jtvl++;
    				}
    				stepkl();
    			}
    		}
    		/*
    		 * correct interval list by finding minimum column of
    		 * entire object, and subtract this from all endpoints.
    		 * Also clear those lines with empty interval.
    		 */
    		jtvl = itvl;
    		nk1 = jtvl->ileft;
    		nkl = jtvl->iright;
    		for (l=nl1; l<=nll; l++) {
    			if (jtvl->ileft < nk1)
    				nk1 = jtvl->ileft;
    			if (jtvl->iright > nkl)
    				nkl = jtvl->iright;
    			jtvl++;
    		}
    		jtvl = itvl;
    		for (l=nl1; l<=nll; l++) {
    			if (jtvl->ileft > jtvl->iright) {
    				jtvl->ileft = jtvl->iright = 0;
    				jdom->intvlines[l-nl1].nintvs = 0;
    			} else {
    				jtvl->ileft -= nk1;
    				jtvl->iright -= nk1;
    			}
    			jtvl++;
    		}
    		jdom->kol1 = nk1;
    		jdom->lastkl = nkl;
    		/*
    		 * make a values table
    		 */
    		nobj = makemain(1,jdom,NULL,NULL,obj);
    		nobj->vdom = newvaluetb(nobj,1,obj->vdom->bckgrnd);
    		nobj->vdom->linkcount++;
    			
    		/*
    		 * scan new object, filling in grey table
    		 * BEWARE - coordinates are scaled up by 256,
    		 * interpolated grey values by 65536.  This requires
    		 * 32-bit integers to work for most objects.
    		 *
    		 * Invert the rotation.  Take care with the scaling.
    		 */
    		sin1 = - (int) (s/yscale);
    		cos1 = (int) (c/yscale);
    		sin2 = (int) (c/xscale);
    		cos2 = (int) (s/xscale);
    
    		initgreyscan(nobj,&iwsp,&gwsp);
    		while (nextgreyinterval(&iwsp) == 0) {
    			g = gwsp.grintptr;
    			turn(iwsp.linpos,iwsp.colpos);
    			for (i=0; i<iwsp.colrmn; i++) {

    				grey4val(obj,turnline,turnkol,g4);
    				if (usemode) {	/* get mode of 4 pixels */
    					mode=g4[0];
    					max=1;
    					for (k=0; k<4; k++) {
    						test=g4[k];
    						count=0;
    						for (j=0;j<4;j++) {
    							if (g4[j]==test)
    								count++;
    						}
    
    						if (count>max) {
    							max=count;
    							mode=test;
    						}
    					}
    					*g++ = mode;
    				}
    				else {		/* get weighted mean of 4 pixels */
    
   #ifdef OSK
    				*g++ = interp(g4,fractline,fractkol);

    #else 
    				mfractline = 256 - fractline;
    				mfractkol = 256 - fractkol;
    				*g++ = (g4[0] * mfractline*mfractkol
    				   + g4[1] * mfractline*fractkol
    				   + g4[2] * fractline*mfractkol
    				   + g4[3] * fractline*fractkol
    				   + 32767) / 65536;
    #endif
    
    				}

    				stepkl();
    			}
    		}
    		if (dothresh) {	/* default */
    			/*
    			 * threshold to remove unset zero background
    			 */
    			tobj = threshold (nobj, 1);
    			freeobj(nobj);
    			return(tobj);
    		}
    		
    		return(nobj);	// return non-thresholded object
							// see nothresh_spinsqueeze() above
    			
    	case 10:
    		pdom = (struct polygondomain *) obj->idom;
    
    /*	MG 29/9/92 use midpoint instead
    		lineorigin = pdom->vtx->vtY;
    		kolorigin = pdom->vtx->vtX;
    */
    		/* determine object midpoint */
    		minx=maxx=pdom->vtx->vtX;
    		miny=maxy=pdom->vtx->vtY;
    		for (i=0;i<pdom->nvertices;i++) {
    			x=pdom->vtx[i].vtX;
    			y=pdom->vtx[i].vtY;
    			if (x<minx) minx=x;
    			if (x>maxx) maxx=x;
    			if (y<miny) miny=y;
    			if (y>maxy) maxy=y;
    		}
    		lineorigin=(maxy+miny)/2;
    		kolorigin=(maxx+minx)/2;
    
    		npoly = spinpoly(pdom);
    		nobj = makemain(10, (struct intervaldomain *)npoly, NULL, NULL, obj);
    		return(nobj);
    	case 11:
    		blist = (struct boundlist *) obj->idom;
    
    /*	MG 29/9/92 use midpoint instead
    		lineorigin = blist->poly->vtx->vtY;
    		kolorigin = blist->poly->vtx->vtX;
    */
    		/* determine object midpoint */
    		minx=maxx=blist->poly->vtx->vtX;
    		miny=maxy=blist->poly->vtx->vtY;
    		for (i=0;i<blist->poly->nvertices;i++) {
    			x=blist->poly->vtx[i].vtX;
    			y=blist->poly->vtx[i].vtY;
    			if (x<minx) minx=x;
    			if (x>maxx) maxx=x;
    			if (y<miny) miny=y;
    			if (y>maxy) maxy=y;
    		}
    		lineorigin=(maxy+miny)/2;
    		kolorigin=(maxx+minx)/2;
    			
    		nblist = spinblist(blist,NULL);
    		nobj = makemain(11, (struct intervaldomain *)nblist, NULL, NULL, obj);
    		return(nobj);
    
    	case 100: /* circle */
    		nobj=(struct object *)duplicate_obj(obj);
    		radius=((struct circle *)obj)->radius;
    		((struct circle *)nobj)->radius = (int) (radius * xscale);
    		return(nobj);
    
    	default:
    		fprintf(stderr,"can't rotate type %d object\n",obj->type);
    		woolz_exit(61, "rottrans");
    	}
    }
    
    
    /*
     *
     *	S P I N _ O F F S E T. C -- rotate objects offset from a central position
     *				    written specifically for Cprobe objects for flexible
     *				    screen use in Cytovision project.
     *
     *	Written: M.Castling based on spin.c. 
     *	Main differances are that lineorigin & kolorigin are arguments to spin_offset()
     *	rottrans(). lineorigin & kolorigin no longer calculated in this module.
     *
     */
    
    
    #ifdef WIN32
    struct object *
    spin_offset(struct object *obj, int ycent, int xcent, double radians)
    #endif
    #ifdef i386
    struct object *
    spin_offset(obj, ycent, xcent, radians)
    struct object *obj;
    int ycent;
    int xcent;
    double radians;
    #endif
    {
    struct object *rottrans_offset();
    
    /* #ifdef DEBUG
    	if ((dbfile = fopen("spinfile","w")) == NULL)
    		{
    		fprintf(stderr,"spinfile could not be opened \n");
    		exit();
    		}
    	t = time(NULL);
    	gm = gmtime(&t);
    	fprintf(dbfile,"spinfile : spin_offset() at %s \n",asctime(gm));
    	fprintf(dbfile,"spin by %f radians  xscale = %f yscale = %f \n",radians,xscale,yscale);
    #endif DEBUG */
    
    	xscale = 1.0;
    	yscale = 1.0;
    	return(rottrans_offset(obj, ycent, xcent, radians));
    }
    
    #ifdef WIN32
    struct object *
    rottrans_offset(struct object *obj, int ycent, int xcent, double radians)
    #endif
    #ifdef i386
    struct object *
    rottrans_offset(obj, ycent, xcent, radians)
    struct object *obj;
    int ycent;
    int xcent;
    double radians;
    #endif
    {
    	register l,i,j;
    	int extent;
//    	int x, y, minx, maxx, miny, maxy;
    	int mfractline,mfractkol;
    	register GREY *g;
    	GREY g4[4];
    	struct object *makemain(), *nobj, *tobj, *threshold();
    	struct intervaldomain/* *idom,*/ *jdom, *makedomain();
    	struct iwspace iwsp;
    	struct gwspace gwsp;
    	register struct interval *jtvl, *itvl;
    	struct valuetable *newvaluetb();
       	struct polygondomain *pdom, *npoly;
    
    	/* first check the supplied object */
    	if ( woolz_check_obj(obj, "rottrans_offset") != 0) 
    		return(NULL);
    
    	s = 256.0 * sin(radians);
    	c = 256.0 * cos(radians);
    	sin1 = (int) (xscale*s);
    	cos1 = (int) (yscale*c);
    	sin2 = (int) (xscale*c);
    	cos2 = - (int) (yscale*s);
    
    /* #ifdef DEBUG
    	fprintf(dbfile,"s = %f c = %f sin1 = %d cos1 = %d sin2 = %d cos2 = %d \n",s,c,sin1,cos1,sin2,cos2);
    #endif DEBUG */
    
    	switch (obj->type) {
    	case 1:
    		if ( wzemptyidom(obj->idom) > 0) 
    			return(makemain(1, newidomain(obj->idom), NULL, NULL, NULL ));
    
    		if (obj->vdom == NULL)
    			{
    #ifdef DEBUG
    	fprintf(stderr,"spin, value domain does not exist\n");
    	fprintf(dbfile,"spin, value domain does not exist\n");
    #endif /* DEBUG */
    			return(makemain(1, newidomain(obj->idom), NULL, NULL, NULL));
    			}
    
    		/*
    		 * find line limits of rotated object
    		 */
    		lineorigin = ycent;
    		kolorigin = xcent;
    /* #ifdef DEBUG
    	fprintf(dbfile,"lineorigin = %d kolorigin = %d\n",lineorigin,kolorigin);
    #endif DEBUG */
    
    		turn (lineorigin,kolorigin);
    		nll = turnline;
    		nl1 = nll;
    		nkl = turnkol;
    		nk1 = nkl;
    /* #ifdef DEBUG
    	fprintf(dbfile,"fractline = %d fractkol = %d \n",fractline,fractkol);
    	fprintf(dbfile,"turnline = %d turnkol = %d \n",turnline,turnkol);
    	fprintf(dbfile,"nl1 = %d nll = %d nk1 = %d nkl = %d\n",nl1,nll,nk1,nkl);
    #endif DEBUG */
    
    		initrasterscan(obj,&iwsp,0);
    		while (nextintervalendpoint(&iwsp) == 0)
    			limits(iwsp.linpos,iwsp.colpos);
    		nl1--;
    		nll++;
    		nk1--;
    		nkl++;
    
    
    		/*
    		 * make a domain structure
    		 */
    		
    		/* deal with scale up : MG 17/5/93 */
    		extent=1+(int) yscale;
    		if (extent<2)
    			extent=2;
    
    /* #ifdef DEBUG
    	fprintf(dbfile,"\nmakedomain() ... \n");
    #endif DEBUG */
    
    		jdom = makedomain(1, nl1, nll, nk1, nkl);
    		itvl = (struct interval *) Malloc ((nll-nl1+extent+1)*sizeof(struct interval));
    		jtvl = itvl;
    		jdom->freeptr = (char *) itvl;
    		for (l=nl1; l<=nll; l++) {
    			jtvl->ileft = nkl;
    			jtvl->iright = nk1;
    			makeinterval(l, jdom, 1, jtvl);
    			jtvl++;
    		}
    		/*
    		 * find column bounds for each line in rotated object.
    		 * when considering effect of any given line in input
    		 * picture, extend to adjacent lines to ensure enough
    		 * space at edges.
    		 */
    /* #ifdef DEBUG
    	fprintf(dbfile,"\nfind column bounds for each line ... \n");
    #endif DEBUG */
    
    		initrasterscan(obj,&iwsp,0);
    		while (nextinterval(&iwsp) == 0) {
    			turn(iwsp.linpos, iwsp.colpos);
    
    
    			for (j=0; j<iwsp.colrmn; j++) {
    				jtvl = itvl + turnline - nl1;
    				for (i=0; i<extent; i++) {
    					if (jtvl->ileft > turnkol)
    						jtvl->ileft = turnkol;
    					if (jtvl->iright < turnkol+1)
    						jtvl->iright = turnkol+1;
    
    					jtvl++;
    				}
    				stepkl();
    			}
    		}
    		/*
    		 * correct interval list by finding minimum column of
    		 * entire object, and subtract this from all endpoints.
    		 * Also clear those lines with empty interval.
    		 */
    
    /* #ifdef DEBUG
    	fprintf(dbfile,"correct interval list ...\n");
    #endif DEBUG */
    
    		jtvl = itvl;
    		nk1 = jtvl->ileft;
    		nkl = jtvl->iright;
    		for (l=nl1; l<=nll; l++) {
    			if (jtvl->ileft < nk1)
    				nk1 = jtvl->ileft;
    			if (jtvl->iright > nkl)
    				nkl = jtvl->iright;
    			jtvl++;
    		}
    		jtvl = itvl;
    		for (l=nl1; l<=nll; l++) {
    			if (jtvl->ileft > jtvl->iright) {
    				jtvl->ileft = jtvl->iright = 0;
    				jdom->intvlines[l-nl1].nintvs = 0;
    			} else {
    				jtvl->ileft -= nk1;
    				jtvl->iright -= nk1;
    			}
    /* #ifdef DEBUG
    	fprintf(dbfile,"l = %d jtvl->ileft %d jtvl->iright %d \n",l,jtvl->ileft,jtvl->iright);
    #endif DEBUG */
    
    			jtvl++;
    		}
    		jdom->kol1 = nk1;
    		jdom->lastkl = nkl;
    		/*
    		 * make a values table
    		 */
    		nobj = makemain(1,jdom,NULL,NULL,obj);
    		nobj->vdom = newvaluetb(nobj,1,obj->vdom->bckgrnd);
    		nobj->vdom->linkcount++;
    			
    		/*
    		 * scan new object, filling in grey table
    		 * BEWARE - coordinates are scaled up by 256,
    		 * interpolated grey values by 65536.  This requires
    		 * 32-bit integers to work for most objects.
    		 *
    		 * Invert the rotation.  Take care with the scaling.
    		 */
    		sin1 = - (int) (s/yscale);
    		cos1 = (int) (c/yscale);
    		sin2 = (int) (c/xscale);
    		cos2 = (int) (s/xscale);
    
    		initgreyscan(nobj,&iwsp,&gwsp);
    		while (nextgreyinterval(&iwsp) == 0) {
    			g = gwsp.grintptr;
    			turn(iwsp.linpos,iwsp.colpos);
    			for (i=0; i<iwsp.colrmn; i++) {
    #ifdef OSK
    				grey4val(obj,turnline,turnkol,g4);
    				*g++ = interp(g4,fractline,fractkol);
    #else /* OSK */
    				mfractline = 256 - fractline;
    				mfractkol = 256 - fractkol;
    				grey4val(obj,turnline,turnkol,g4);
    				*g++ = (g4[0] * mfractline*mfractkol
    				   + g4[1] * mfractline*fractkol
    				   + g4[2] * fractline*mfractkol
    				   + g4[3] * fractline*fractkol
    				   + 32767) / 65536;
    #endif /* OSK */
    				stepkl();
    			}
    		}
    
    		/*
    		 * threshold to remove unset zero background
    		 */
    		tobj = threshold (nobj, 1);
    		freeobj(nobj);
    		return(tobj);
    	case 10:
    		pdom = (struct polygondomain *) obj->idom;
    
    		lineorigin = pdom->vtx->vtY;
    		kolorigin = pdom->vtx->vtX;

    		npoly = spinpoly(pdom);
    		nobj = makemain(10, (struct intervaldomain *)npoly, NULL, NULL, obj);
    		return(nobj);

    	case 11:
    		return(obj);
    	default:
    		fprintf(stderr,"can't rotate type %d object\n",obj->type);
    		woolz_exit(61, "rottrans_offset");
    	}
    }
    
    
    
    
    
    
    #ifdef WIN32
    static void
    limits(int l, int k)
    #endif
    #ifdef i386
    static void
    limits(l, k)
    int l;
    int k;
    #endif
    {
    	turn(l,k);
    	if (nl1 > turnline)
    		nl1 = turnline;
    	if (nll < turnline)
    		nll = turnline;
    	if (nk1 > turnkol)
    		nk1 = turnkol;
    	if (nkl < turnkol)
    		nkl = turnkol;
    }
    
    
    #ifdef WIN32
    static void
    turn(register int l, register int k)
    #endif
    #ifdef i386
    static void
    turn(l, k)
    register int l;
    register int k;
    #endif
    {
    	register l256,k256;
    	l -= lineorigin;
    	k -= kolorigin;
    	l256 = l*cos1 + k*sin1;
    	k256 = l*cos2 + k*sin2;
    	turnline = l256 / 256;
    	turnkol = k256 / 256;
    	fractline = l256 - 256*turnline;
    	if (fractline < 0) {
    		turnline--;
    		fractline += 256;
    	}
    	fractkol = k256 - 256*turnkol;
    	if (fractkol < 0) {
    		turnkol--;
    		fractkol += 256;
    	}
    	turnline += lineorigin;
    	turnkol += kolorigin;
    }
    
    
    #ifdef WIN32
    static void
    stepkl()
    #endif
    #ifdef i386
    static void
    stepkl()
    #endif
    {
    	fractline += sin1;
    	while (fractline >= 256) {
    		fractline -= 256;
    		turnline++;
    	}
    	while (fractline < 0) {
    		fractline += 256;
    		turnline--;
    	}
    	fractkol += sin2;
    	while (fractkol >= 256) {
    		fractkol -= 256;
    		turnkol++;
    	}
    	while (fractkol < 0) {
    		fractkol += 256;
    		turnkol--;
    	}
    }
    
    
    #ifdef WIN32
    static struct polygondomain *
    spinpoly(struct polygondomain *pdom)
    #endif
    #ifdef i386
    static struct polygondomain *
    spinpoly(pdom)
    struct polygondomain *pdom;
    #endif
    {
    	struct polygondomain *npoly,*makepolydmn();
    	register struct ivertex *vertex,*nvertex;
    	register i;

    	vertex = pdom->vtx;
    	npoly = makepolydmn(pdom->type, (short *)vertex, pdom->nvertices, pdom->maxvertices, 1);
    	nvertex = npoly->vtx;
    	for (i=0; i<pdom->nvertices; i++) {
    		turn (vertex->vtY,vertex->vtX);
    		nvertex->vtY = turnline;
    		nvertex->vtX = turnkol;
    		vertex++;
    		nvertex++;
    	}
    	return(npoly);
    }
    
    
    #ifdef WIN32
    static struct boundlist *
    spinblist(struct boundlist *blist, struct boundlist *up)
    #endif
    #ifdef i386
    static struct boundlist *
    spinblist(blist, up)
    struct boundlist *blist;
    struct boundlist *up;
    #endif
    {
    	struct boundlist *nblist;
    	struct polygondomain *spinpoly();
    	if (blist == NULL)
    		return(NULL);
    	else {
    		nblist = (struct boundlist *) Malloc(sizeof(struct boundlist));
    		nblist->type = blist->type;
    		nblist->up = up;
    		nblist->next = spinblist(blist->next,up);
    		nblist->down = spinblist(blist->down,nblist);
    		nblist->wrap = blist->wrap;
    		nblist->poly = spinpoly(blist->poly);
    		return(nblist);
    	}
    }
    
