// zBlob.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>  
#include <math.h>
#include "yBlob.h"
#include "zGeneric.h"
#include "yMemRect.h"


/**************************** Exported Data ************************/

long Blob2DebugFlag = 0;

/***************** Spot Analysis Data and Functions ****************/

     							// the list begins from (in Spots)

#define WB    (char*)(&WorkBuffer[0])



BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}

CZBlob::CZBlob(CZMemRect *pMem)
{
	SpotBuffer = NULL;
	SpotBufferSize = 0;
	SpotsNumber = 0; // length of the list (in Spots)
	FirstFreeSpot = 0;
	MaxSpotsNumber = 0;
	CallSum = 0;
	MaxTime = 60000; // msec
	pMR = pMem;

}

CZBlob::~CZBlob()
{
}

/************************************************* spBlobDebugFlag */
/***
	Function:	spBlobDebugFlag

	Description:
		Sets new value, returns old one

	Arguments:

	Returns: None

***/
long  CZBlob::spBlobDebugFlag(long newval)
{
	long ret = Blob2DebugFlag;
		Blob2DebugFlag = newval;
	return ret;
}


/******************************  end of function:  spBlobDebugFlag */

/***************************************************** ClearBuffer */
/***
	Function:	ClearBuffer

	Description:
			Creates if no; clears the buffer
	Arguments:

	Returns:

***/
long CZBlob::ClearBuffer()
{
    long i;

	if (!SpotBuffer)
    {
        MaxSpotsNumber = 0;
		SpotBuffer = (Spot *)malloc(SpotBufferSizeDefault);
        if (SpotBuffer)
        {
			SpotBufferSize = SpotBufferSizeDefault;
            MaxSpotsNumber = SpotBufferSize / sizeof(Spot) - 1;
        } else goto Fin;
    }
    FirstFreeSpot = SpotBegIndex;	// =1
    i = SpotsNumber = 0;
    while (i++ < MaxSpotsNumber)
    {
    	SpotBuffer[i].PixNum = 0; 	// is free
        SpotBuffer[i].PixSum = 0;
    	SpotBuffer[i].Tag = -1;
    }
Fin:
    return MaxSpotsNumber;
}


/************************************************** FreeSmallSpots */
/***
	Function:	FreeSmallSpots

	Description:

	Arguments:
		hndl -
	Returns:
		index of a free spot
***/
long CZBlob::FreeSmallSpots(long minarea, long lastY)
{
	long i, begind, k = 0, ind = 0;
   Spot *sp;

	if (!SpotBuffer) if (!ClearBuffer()) goto Fin;
   begind = SpotBegIndex;
	for (i = begind;  i < MaxSpotsNumber;  i++)
   {
     	sp = &SpotBuffer[i];
      if (sp->PixNum > 0)
      {
        	if (sp->PixNum < minarea)
	      {	// is busy and small enough
    	     	if (sp->Ymax + 1 < lastY)
      		{	// and can't grow
               sp->PixNum = 0;  // free
             	SpotsNumber--;
	            ind = i; // last freed
               continue;
            }
         }
         k = i;	// last busy
      }
   }
   FirstFreeSpot = k + 1;
Fin:
	return ind;
}


/******************************************************* CleanTail */
/***
	Function:	CleanTail

	Description:

	Arguments:
		hndl -
	Returns:
***/
void CZBlob::CleanTail(long tail)
{
    long i, begind;
    Spot *sp;

    if (tail < 0) tail = SpotsNumber;
    else SpotsNumber = tail;
    i = begind = SpotBegIndex + tail;
	while (i < FirstFreeSpot)
    {
     	sp = &SpotBuffer[i++];
        sp->PixNum = 0;  // free
    }
    FirstFreeSpot = begind;
}


/***************************************************** GetFreeSpot */
/***
	Function:	GetFreeSpot

	Description:

	Arguments:
		hndl -
	Returns:
		index of a free spot
***/
long CZBlob::GetFreeSpot(long minarea, long lastY)
{
    long i, begind, newsize,
    	 ind = 0, addportions = 100;
    Spot *sp;
    bool lastchance = true;

	if (!SpotBuffer) if (!ClearBuffer()) goto Fin;
Try:
	begind = SpotBegIndex;  ind = 0;
	for (i = begind;  i < MaxSpotsNumber;  i++)
    {
     	sp = &SpotBuffer[i];
        if (sp->PixNum == 0)
        {	// is free
         	ind = i;  break;
        }
    }
    if ((!ind) && (minarea))
    {
    	FreeSmallSpots(minarea, lastY);
        minarea = 0;   goto Try;
    }
    if ((!ind) && (lastchance))
    { 	// expansion if possible:
		newsize = SpotBufferSize + addportions * sizeof(Spot);
		SpotBuffer = (Spot *)realloc(SpotBuffer, newsize);
        if (!SpotBuffer) goto Fin;
        SpotBufferSize = newsize;
        MaxSpotsNumber = SpotBufferSize / sizeof(Spot) - 1;
	    i = FirstFreeSpot;
    	while (i < MaxSpotsNumber)
	    {
    		SpotBuffer[i].PixNum = 0; 	// free
        	SpotBuffer[i].PixSum = 0;
	    	SpotBuffer[i].Tag = -1;
    	    i++;
	    }
        lastchance = false;  goto Try;
    }
Fin:
	if (ind)
    {
        SpotsNumber++;
        if (ind >= FirstFreeSpot) FirstFreeSpot = ind + 1;
    }
    return ind;
}


/**************************************************** CompactSpots */
/***
	Function:	CompactSpots

	Description:

	Arguments:
		hndl -
	Returns:
		number of found spots
***/
long CZBlob::CompactSpots(void)
{
    long i, begind, found = 0;
    DWORD k;
    Spot *sp;
    BYTE *r, *w;

    begind = SpotBegIndex;
    for (i = begind;  i < MaxSpotsNumber;  i++)
    {
     		sp = &SpotBuffer[i];
        	if (sp->PixNum > 0)
        	{	// is found
         	if (i > begind + found)
            {
					w = (BYTE *)&SpotBuffer[begind + found];
	            r = (BYTE *)sp;
    	        	for (k = 0;  k < sizeof(Spot);  k++) w[k] = r[k];
               sp->PixNum = 0;	// make free
            }
        		found++;
        	}
    }
    SpotsNumber = found;
    FirstFreeSpot = found + 1;
    return found;
}


/******************************************************* Find8plus */
/***
	Function:	Find8plus

	Description:
		regular blob on 8 bit/pix image
	Arguments:
    	sphndl -
        imagemem -
        xe, ye, pitch -
		thresh -
        inverse - if>0 then inverse pixels and thresh
        minarea -
	Returns:
		number of found spots
***/
long CZBlob::Find8plus (
        BYTE *imagemem,
        long xe, long ye, long pitch,
		long thresh, long inverse, long minarea,
        long deftag)
{
    long begind, i, k, rnum, wrkl,
    	leftlabel, toplabel;
    long *lastrow, found = 0;
    BYTE *row;
    WORD pix, inv;
    Spot *topspot, *leftspot, *wrkspot;

    CleanTail(-1);
    begind = SpotBegIndex;
    if (xe * sizeof(long) > WorkBufferSize) goto Fin;
    lastrow = (long *)WorkBuffer;
    for (i = 0;  i < xe;  i++) lastrow[i] = 0;
    rnum = 0;  row = imagemem;
	if (inverse) { inv = 0xff;  thresh ^= 0xff; }
    else inv = 0;

RowsLoop:               //  processing of the next scanline
	leftlabel = toplabel = 0;
	for (i = 0;  i < xe;  i++)
	{
		pix = (WORD)((row[i] ^ inv) & 0xff);   // next pixel value
		if (pix < thresh)
		{   	                  // pixel value is below the threshold
			leftlabel = lastrow[i] = 0;
			continue;
		}

		// join the pixel to any of neighboring spots or make a new one:
		toplabel = lastrow[i];
		if (toplabel)
		{	// OK, to upper one
			topspot = SpotBuffer + (begind + toplabel - 1);
			if (leftlabel) goto MergeLabels;  // merge upper and left spots:
			else goto UseTopLabel;  // join the pixel to upper spots:
		} else
		{  // no top label, try left one
			if (leftlabel) goto UseLeftLabel; // join the pixel to left spots:
			else goto MakeNewLabel; // initialize a new spot
		}

MakeNewLabel:
		// new label number:
		leftlabel = GetFreeSpot(minarea, rnum);
        if (!leftlabel) goto Fin;
		leftspot = SpotBuffer + (begind + leftlabel - 1);
		leftspot->Xmin = leftspot->Xmax = leftspot->Xrecon = i; // X coordinate
		leftspot->Ymin = leftspot->Yrecon = rnum;        		// Y coordinate
        	// number, sum and max of pixels in the new spot:
		leftspot->PixNum = leftspot->PixSum = 0;
        leftspot->PixMax = pix;
	 	leftspot->Thresh = thresh; // threshold for the spot
        leftspot->Tag = deftag;
	 	leftspot->ThreshNum = 0; // # of pixels at the threshold
	 	leftspot->NextThr = 256; // next threshold value
	 	leftspot->NextNum = 0; // # of pixels at the next threshold
		goto Tail;

UseLeftLabel:
		// leftspot is OK; Xmin and Ymin can't be decreased in this branch,
		// nothing to do
		goto Tail;

UseTopLabel:
		// Xmax and Ymin can't change in this branch
		leftlabel = toplabel;		// left label for next pixel
		leftspot = topspot;
		if (leftspot->Xmin > i) leftspot->Xmin = i;
		goto Tail;

MergeLabels:
		// leftspot, topspot are OK
		if (leftlabel == toplabel) goto Tail;	// nothing to do
		if (leftlabel > toplabel)
        {   // keep the earlier label, and call it 'left':
         	wrkl = leftlabel;  wrkspot = leftspot;
            leftlabel = toplabel;  leftspot = topspot;
            toplabel = wrkl;  topspot = wrkspot;
        }
		if (leftspot->Xmin > topspot->Xmin)
			leftspot->Xmin = topspot->Xmin;   // left point
		if (leftspot->Xmax < topspot->Xmax)
			leftspot->Xmax = topspot->Xmax;	// right point
		if (leftspot->Ymin > topspot->Ymin)
			leftspot->Ymin = topspot->Ymin;	// top point
		leftspot->PixNum += topspot->PixNum;  // # of pixels
		leftspot->PixSum += topspot->PixSum;  // sum of pixels
		if (leftspot->PixMax < topspot->PixMax)
        {  // max of pixels and its position
			leftspot->PixMax = topspot->PixMax;
			leftspot->Xrecon = topspot->Xrecon;
			leftspot->Yrecon = topspot->Yrecon;
        }
        leftspot->ThreshNum += topspot->ThreshNum;
        if (leftspot->NextThr > topspot->NextThr)
        {
         	leftspot->NextThr = topspot->NextThr;
            leftspot->NextNum = topspot->NextNum;
        } else if (leftspot->NextThr == topspot->NextThr)
               		leftspot->NextNum += topspot->NextNum;
		for (k = topspot->Xmin;  k <= topspot->Xmax;  k++)
		{	// toggle references from top label to the left one:
			if (lastrow[k] == toplabel)	lastrow[k] = leftlabel;
		}
		topspot->PixNum = topspot->PixSum = 0;  // free the top spot

Tail:
		if (leftspot->Xmax < i) leftspot->Xmax = i;  // Max X-coordinate
		leftspot->Ymax = rnum;    // Current Y-coordinate is always max
		leftspot->PixNum += 1;    // number of pixels
		leftspot->PixSum += pix;  // sum of pixels
		if (leftspot->PixMax < (long)pix)
        {  // max of pixels and its position
			leftspot->PixMax = pix;
			leftspot->Xrecon = i;
			leftspot->Yrecon = rnum;
        }
        if ((long)pix == leftspot->Thresh)
        {  leftspot->ThreshNum++;  goto TailTail; }
        if ((long)pix == leftspot->NextThr)
        {  leftspot->NextNum++;  goto TailTail; }
		if ((long)pix < leftspot->NextThr)
        {
		 	leftspot->NextThr = pix; // next threshold value
		 	leftspot->NextNum = 1; // # of pixels at the next threshold
        }
TailTail:
		lastrow[i] = leftlabel;
	}
    row += pitch;  rnum++;
    if (rnum < ye) goto RowsLoop;  // next scanline

    FreeSmallSpots(minarea, rnum + 1);
	// move found spots together:
    found = CompactSpots();
Fin:
	return found;
}


/*************************************************** FillTagValues */
/***
	Function:	FillTagValues

	Description:
		fills tag with values given by operation
	Arguments:

	Returns: 0 - '+';  1 - '-';  -1 - failure.

***/
static char *SpotSortTagOps =
    " YX Xmin Ymin Xmax Ymax Thresh "
    " Area Psum Pmax Xrec Yrec Tag "
    " Tag1 Tag2 Tag3 Tag4 Tag5 ";
static long SortMap[] =
	{ 10, -1,  // 0, 1 ('tag' is default)
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
     11, 12, 13, 14, 15};

long CZBlob::FillTagValues(char *operation)
{
	long opcase, ibeg, ifin, curval, i, n,
   	  *spl, minus = -1;
   DWORD offs = 0;

   if (!SpotBuffer) goto Fin;	// empty list
   minus = 0;
   if (*operation == '+') operation++;
   if (*operation == '-') { minus = 1;  operation++; }
   opcase = WordInText (SpotSortTagOps, &offs, 0, operation, 0);
   ibeg = SpotBegIndex;  ifin = ibeg + SpotsNumber - 1;
   n = SortMap[opcase];
   for (i = ibeg;  i <= ifin;  i++)
   {
      if (n < 0)
        	curval = SpotBuffer[i].Ymin * 10000
            		  + SpotBuffer[i].Xmin;
      else
      {
      	spl = (long *)&SpotBuffer[i];   curval = spl[n];
      }
      if (minus) curval = (-curval);
      SpotBuffer[i].Tag = curval;
   }
Fin:
	return  minus;
}


/******************************************************* SortChain */
/***
	Function:	SortChain

	Description:
		fills <first> with index of the 1-st Spot,
      Tag members of other spots with indexes to the next spot
        in sorted order (0 to the last spot)

	Arguments:

	Returns: number of spots / 0

***/
long CZBlob::SortChain(char *operation, long *first)
{
   long ibeg, i, k, *r, *q, spots = 0;

   if (FillTagValues(operation) < 0) goto Fin;
   spots = SpotsNumber;    ibeg = SpotBegIndex;
   r = (long *)WorkBuffer;  q = r + (spots + 1);
//   IndexSortL (&SpotBuffer[ibeg].Tag, spots, sizeof(Spot), q);
	IndexSortElems (&SpotBuffer[ibeg].Tag, sizeof(Spot), spots,	q, 4, 0);

   // sort indexes:
   for (i = 0;  i < spots;  i++)	r[q[i]] = i + 1;

   r[spots] = 0;  k = *first = r[0];
   for (i = 1;  i <= spots;  i++)
   {
   	k = SpotBuffer[ibeg + k - 1].Tag = r[i];
   }
Fin:
	return spots;
}


/********************************************************* spFind8 */
/***
	Function:	spFind8
	Description:
		regular blob on 8 bit/pix image
	Arguments:
        imagemem -
        xe, ye, pitch -
		thresh -
        inverse - if>0 then inverse pixels and thresh
        minarea -
	Returns:
    	# of found spots
***/
long  CZBlob::spFind8 (
        BYTE *imagemem,
        long xe, long ye, long pitch,
		long thresh, long inverse, long minarea)
{
    long found = 0;

	if (ClearBuffer())
    {
		found = Find8plus (imagemem, xe, ye, pitch,
                thresh, inverse, minarea, -1);
	}
	return found;
}


/********************************************************* spFind1 */
/***
	Function:	spFind1

	Description:
		regular blob on 1 bit/pix image
	Arguments:
        imagemem -
        xe, ye, pitch -
        inverse - if != 0 then find zeros
        minarea -
	Returns:

***/
long  CZBlob::spFind1 (
        BYTE *imagemem,
        long xe, long ye, long pitch,
        long inverse, long minarea)
{
    long begind, i, k, rnum, // found,
    	leftlabel, toplabel;
    long *lastrow, ret = 0;
    BYTE *row;
    BYTE pix, inv;
    Spot *topspot, *leftspot;

    if (!ClearBuffer()) goto Fin;
    begind = SpotBegIndex;
    if (xe * sizeof(long) > WorkBufferSize) goto Fin;
    lastrow = (long *)WorkBuffer;
    for (i = 0;  i < xe;  i++) lastrow[i] = 0;
    rnum = 0;  row = imagemem;
	if (inverse) inv = 0xff;  else inv = 0;

RowsLoop:               //  processing of the next scanline
	leftlabel = toplabel = 0;
	for (i = 0;  i < xe;  i++)
	{
        // next pixel value:
    	pix = (BYTE)(((row[i >> 3] ^ inv) >> (i & 7)) & 1);
		if (!pix)
		{   // pixel value is below the threshold
			leftlabel = lastrow[i] = 0;
			continue;
		}
		// join the pixel to any of neighboring spots or make a new one:
		toplabel = lastrow[i];
		if (toplabel)
		{	// OK, to upper one
			topspot = SpotBuffer + (begind + toplabel - 1);
			if (leftlabel) goto MergeLabels;  // merge upper and left spots:
			else goto UseTopLabel;  // join the pixel to upper spots:
		} else
		{  // no top label, try left one
			if (leftlabel) goto UseLeftLabel; // join the pixel to left spots:
			else goto MakeNewLabel; // initialize a new spot
		}

MakeNewLabel:
		// new label number:
		leftlabel = GetFreeSpot(minarea, rnum);
        if (!leftlabel) goto Fin;
		leftspot = SpotBuffer + (begind + leftlabel - 1);
		leftspot->Xmin = leftspot->Xmax = leftspot->Xrecon = i; // X coordinate
		leftspot->Ymin = leftspot->Yrecon = rnum;        		// Y coordinate
        	// number and sum of pixels in the new spot:
		leftspot->PixNum = leftspot->PixSum = 0L;
	 	leftspot->Thresh = inv & 1; // "threshold" for the spot
		goto Tail;

UseLeftLabel:
		// leftspot is OK; Xmin and Ymin can't be decreased in this branch,
		// nothing to do
		goto Tail;

UseTopLabel:
		// Xmax and Ymin can't change in this branch
		leftlabel = toplabel;		// left label for next pixel
		leftspot = topspot;
		if (leftspot->Xmin > i) leftspot->Xmin = i;
		goto Tail;

MergeLabels:
		// leftspot, topspot are OK
		if (leftlabel == toplabel) goto Tail;	// nothing to do
		if (leftspot->Xmin > topspot->Xmin)
			leftspot->Xmin = topspot->Xmin;   // left point
		if (leftspot->Xmax < topspot->Xmax)
			leftspot->Xmax = topspot->Xmax;	// right point
		if (leftspot->Ymin > topspot->Ymin)
			leftspot->Ymin = topspot->Ymin;	// top point
		leftspot->PixNum += topspot->PixNum;  // # of pixels
		leftspot->PixSum += topspot->PixSum;  // sum of pixels
		for (k = topspot->Xmin;  k <= topspot->Xmax;  k++)
		{	// toggle references from top label to the left one:
			if (lastrow[k] == toplabel)	lastrow[k] = leftlabel;
		}
		topspot->PixNum = topspot->PixSum = 0;  // free the top spot

Tail:
		if (leftspot->Xmax < i) leftspot->Xmax = i;  // Max X-coordinate
		leftspot->Ymax = rnum;    // Current Y-coordinate is always max
		leftspot->PixNum += 1;    // number of pixels
		leftspot->PixSum += pix;  // sum of pixels
		lastrow[i] = leftlabel;
	}
    row += pitch;  rnum++;
    if (rnum < ye) goto RowsLoop;  // next scanline

    FreeSmallSpots(minarea, rnum + 1);
	// move found spots together:
    ret = CompactSpots();
Fin:
	return ret;
}


/************************************************ spCodeAccumLines */
/***
 	Calculates

		pcodes - pointer to the buffer of incremental codes
        ncodes - the number of code bytes
        xstart, xstart - coordinates of the start pixel
        period - if !=0 then code is periodical
        distmin, distmax - acceptable distance range
	    hacc - handle of the accumulating picture, 8 or 32 bit per pixel;
        		the function does not clear it
 	returns:
   	 number of lines if OK,
     0 otherwise

static BYTE WorkBuffer[WorkBufferSize];
***/
long  CZBlob::spCodeAccumLines (void *pcodes, long ncodes,
                long xstart, long ystart, long period,
				long hacc,
				double distmin, double distmax)
{
    long pix, x1, y1, x2, y2, dx, dy, xc, yc, d2, d2min, d2max,
         cdist, cdist1, cdist2, xe, ye, i, k,
         data[6], wmask = 0, linesnum = 0 ,
         sop = sizeof(POINT);
    char *oper;
    POINT *ps;

	pix = pMR->mrGetRectInfo (hacc, &xe, &ye);
    if (pix == 8) oper = "+S";
    else
    {
     	if (pix != 32) goto Fin;
        oper = "+";
    }
    ps = (POINT *)malloc(ncodes * sop);
    if (!ps) goto Fin;
	CodesToCoords (pcodes, ncodes, xstart, ystart,
    				&ps[0].x, sop, &ps[0].y, sop);

    cdist1 = floor(distmin);   cdist2 = floor(2 * distmax);
    d2min = floor(distmin * distmin);
    d2max = floor(distmax * distmax + 0.99999);
    wmask = pMR->mrCreate(1, xe, ye);
    for (i = 0;  i < ncodes;  i++)
    {
	    x1 = ps[i].x; 	y1 = ps[i].y;
        for (cdist = cdist1;  cdist <= cdist2;  cdist++)
        {
            if ((k = i + cdist) >= ncodes)
            {
                if (period) k %= ncodes;  else break;
            }
		    x2 = ps[k].x; 	y2 = ps[k].y;
    	   	dy = x2 - x1;  dx = y1 - y2; // ortogonal vector
        	d2 = dx * dx + dy * dy;
	        if (d2 > d2max) break;
	        if (d2 >= d2min)
    	    {	// distance between points is in the range
        		xc = (x1 + x2) / 2;  yc = (y1 + y2) / 2;
            	data[0] = xc + dx;   data[1] = yc + dy;
	            data[2] = xc - dx;   data[3] = yc - dy;
    	        // no checking because the line is inside for sure:
        	    if (LineEndsInRect (0, 0, xe - 1, ye - 1,
            			data, data + 1, data + 2, data + 3) == 2)
                {
	            	pMR->mrOperMask(wmask, "Zeros");
		            pMR->mrMaskLine (wmask, "Line", 1, 1, data);
    		        pMR->mrConstPixelMask (1, oper, hacc, wmask);
        		    linesnum++;	// number of lines
                }
            }
        }
    }
    free(ps);  pMR->mrFree(wmask);
Fin:
    return linesnum;
}


/**************************************************** spAccumLines */
/***
 	Calculates

    hacc - handle of the accumulating picture, 8 or 32 bit per pixel
 	returns:
   	 number of lines if OK,
     0 otherwise

static BYTE WorkBuffer[WorkBufferSize];
***/
long  CZBlob::spAccumLines (long mask, long hacc,
				double distmin, double distmax)
{
    long xstart, ystart, ncodes, limit,
         data[8];
    BYTE *pc;

    pc = WorkBuffer;  limit = WorkBufferSize;
	ncodes = pMR->mrCodeContour (mask, 0, data, pc, limit);
    xstart = data[0]; 	ystart = data[1];
	pMR->mrConstPixelMask (0, "Copy", hacc, 0);	// clear accumulator
	return spCodeAccumLines (pc, ncodes, xstart, ystart, 1, // periodical
				hacc, distmin, distmax);

}


/************************************************* spAccumLinesSeg */
/***
 	Calculates

    hacc - handle of the accumulating picture, 8 or 32 bit per pixel
 	returns:
   	 number of lines if OK,
     0 otherwise

static BYTE WorkBuffer[WorkBufferSize];
***/
//static BYTE DebugBuffer[2000];
//#define D (char*)(&DebugBuffer[0])

long  CZBlob::spAccumLinesSeg (long mask, long hacc,
                long halfstepmin, long halfstepmax, long curvthresh,
				double distmin, double distmax)
{
    long xstart, ystart, ncodes, limit, nextlen, lines,
         offset = 0, ret = 0, data[16];
    BYTE c, *pcodes, *pcurvs;

	pMR->mrConstPixelMask (0, "Copy", hacc, 0);	// clear accumulator
    pcodes = WorkBuffer;  limit = WorkBufferSize;
	ncodes = pMR->mrCodeContour (mask, 0, data, pcodes, limit);
    pcurvs = pcodes + ncodes;  limit -= ncodes;
    xstart = data[0]; 	ystart = data[1];
	CodeCurvature (pcodes, ncodes, xstart, ystart, pcurvs, data,
                halfstepmin, halfstepmax, curvthresh);
    // fills data[0 - 4]
    xstart = data[0]; 	ystart = data[1];	// may be changed
    nextlen = data[2];
/*************
long i,kkk;
kkk=sprintf(D,"ncds: %lu;  Strt: %lu, %lu :: ",ncodes,xstart,ystart);
for(i=0;i<5;i++)kkk+=sprintf(D+kkk," %lu;",data[i]);
kkk+=sprintf(D+kkk,".\n| ");
for(i=0;i<ncodes;i++)DebugBuffer[kkk++]=0x30+pcodes[i];
DebugBuffer[kkk]=0;
MessageBox(NULL, D, "spAccumLinesSeg", MB_OK);
**************/
    while (nextlen)
	{
	    if ((double)nextlen >= distmin)
        {
        	lines = spCodeAccumLines (pcodes + offset, nextlen,
            		xstart, ystart,	0, hacc, distmin, distmax);
            ret += lines;
        }
        // move to the end of current segment, then
        // skip greater than threshold curvatures:
        nextlen += offset;
        while ((offset < nextlen) ||
        	   ((offset < ncodes) &&
        	   	(pcurvs[offset] >= curvthresh)))
        {
            c = pcodes[offset++];
         	CodeUpdateXY(&xstart, &ystart, c);
        }
        // find next segment lenght:
        nextlen = 0;
        while ((offset + nextlen < ncodes) &&
        	   (pcurvs[offset + nextlen] < curvthresh))
            nextlen++;
    }
    return ret;
}


/***************************************************** spArcCenter */
/***
 	Calculates

    For *radmin, *radmax - NULL is OK.
 	returns:
   	 number of pairs if OK,
     0 otherwise

static BYTE WorkBuffer[WorkBufferSize];
***/
long  CZBlob::spArcCenter (long mask, double distmin,
                    long *xc, long *yc, double *radaver,
                    double *radmin, double *radmax)
{
    POINT *points;
    long npoints, x, y, cx, cy, nlim, i, ret;
    double dx, dy, r, r2, rmin, rmax, dcx, dcy;

    points = (POINT *)WorkBuffer;
    npoints = x = y = 0;
    nlim = WorkBufferSize / sizeof(POINT);
    while (npoints < nlim)
	{
    	if (pMR->mrFindNext (mask, &x, &y, 1) <= 0) break;
        points[npoints].x = x;
        points[npoints].y = y;
        npoints++;  x++;
    }
    ret = ArcCenter(points, npoints, distmin, &dcx, &dcy);
    if (ret > 0)
    {
        cx = *xc = floor(dcx + 0.5);
        cy = *yc = floor(dcy + 0.5);
        r = 0;  rmin = 1e+20;  rmax = -1.0;
        for (i = 0;  i < npoints;  i++)
        {
            dx = (double)(points[i].x - cx);
            dy = (double)(points[i].y - cy);
            r += (r2 = dx * dx + dy * dy);
            if (r2 < rmin) rmin = r2;
            if (r2 > rmax) rmax = r2;
        }
        *radaver = sqrt(r / npoints);
        if (radmin) *radmin = sqrt(rmin);
        if (radmax) *radmax = sqrt(rmax);
    }
    return ret;
}



/*************************************************** spSpotsNumber */
/***
	Function:	spSpotsNumber

	Description:

	Arguments:

	Returns:

***/
long  CZBlob::spSpotsNumber(void)
{
 	return SpotsNumber;
}



/************************************************** spSpotListMask */
/***
 Using current Spot List, fills mask with thresholding
 	each Spot at it's natural level.
 returns:
   	 number of spots

***/
long  CZBlob::spSpotListMask (long hndl8, long hmask)
{
    long xe, ye, thresh, spotnum,
    		spots = 0, hseed = 0, hlimit = 0;
	long oh8 = 0, omask = 0, olimit, oseed;
    long xrec, yrec, xl, yt, xre, ybe;
    Spot *sp;
    long memalign = 0xfffffff8; // MEMORY ALIGNMENT MASK
// RESET MEMORY ALIGNMENT MASK for INTEL IMAGE PROCESSING LIBRARY:
#define Intel_IP_LibValue 1
    if (pMR->mrUseIntelIPlib(-1) == Intel_IP_LibValue)
        memalign = 0xffffffc0;
#undef Intel_IP_LibValue

    if (pMR->mrGetRectInfo(hmask, &xe, &ye) != 1) goto Fin;
	if ((spots = spSpotsNumber()) <= 0) goto Fin;
    pMR->mrOperMask (hmask, "Zeros");
    hseed = pMR->mrCreateAs (hmask);
    hlimit = pMR->mrCreateAs (hmask);

	oh8 = pMR->mrCreateOverlay(hndl8, 0, 0, xe, ye);
	omask = pMR->mrCreateOverlay(hmask, 0, 0, xe, ye);
	oseed = pMR->mrCreateOverlay(hseed, 0, 0, xe, ye);
	olimit = pMR->mrCreateOverlay(hlimit, 0, 0, xe, ye);
    for (spotnum = 1;  spotnum <= spots;  spotnum++)
	{
    	sp = SpotBuffer + spotnum;
        thresh = sp->Thresh;
        xrec = sp->Xrecon;  yrec = sp->Yrecon;
        if ((xl = sp->Xmin) > 0) xl = (xl - 1) & memalign;
        if ((yt = sp->Ymin) > 0) yt--;
        if ((xre = (sp->Xmax + 9) & 0xfffffff8) > xe) xre = xe;
        if ((ybe = sp->Ymax + 2) > ye) ybe = ye;
        xre -= xl;  ybe -= yt;
        pMR->mrResizeOverlay(oh8, xl, yt, xre, ybe);
        pMR->mrResizeOverlay(omask, xl, yt, xre, ybe);
        pMR->mrResizeOverlay(oseed, xl, yt, xre, ybe);
        pMR->mrResizeOverlay(olimit, xl, yt, xre, ybe);
        pMR->mrThreshold (oh8, olimit, "NotLess", thresh);
        pMR->mrReconFast (olimit, oseed, xrec - xl, yrec - yt);
        pMR->mrPixelwise (oseed, "Or", omask);
    }
Fin:
    if (oh8) pMR->mrFree (oh8);
    if (omask) pMR->mrFree (omask);
    if (hseed) pMR->mrFree (hseed);
    if (hlimit) pMR->mrFree (hlimit);
	 return spots;
}



/*********************************************** spConstrWaterShed */
/***
 	Starts from the Spot list after spFindMarkers()
      and from 'hndl1' mask of markers (source and result).
	'borders' - calculated mask (subset) of watershed borders;
   Any of 'hndl1' and 'borders' (or both) may be set to 0;
   in this case the mask of markers is to be calculated with
   spSpotListMask() function.
   Form Factor plays if and only if the threshold is less
   than 'formthresh'
   Updates the Spot structure; sets
      Tag1:
         0 - limited by surrounding
      	1 - limited by threshold
      	2 - limited by area
      	3 - limited by 'formfactor'
      Tag2 - measure of 'formfactor';
 	returns:
   	 number of spots if OK,
      -1 otherwise
***/
long  CZBlob::spConstrWaterShed (long hndl8, long hndl1, long borders,
				long thrmin, long areamax, long formfactor,
            long formthresh, long formtype)
{
	long pix, xe, ye, spots = -1;
   	long mseed, mbord, mwrk1, mwrk2, mwater, mdebug, mthr;
   	long ibeg, ifin, spind, rep, armin;
   	long xrec, yrec, area, psum, pmax, thresh, cut, cxmax, cymax;
   	long csum, cmax, cyl, oldcyl, curdata[16], data[16];
   	long level, extra, oldmin, perim;
   	long xmin, xmax, ymin, ymax, flags, nmax, xe1, ye1, emin, emax;
   	int nei4;

   	if ((!SpotBuffer) || (!SpotsNumber))
   	{
      	goto Fin;	// empty list
   	}
    // prepare list:
 	pix = pMR->mrGetRectInfo (hndl8, &xe, &ye);
	if (pix != 8) goto Fin;
   	spots = SpotsNumber;
   	if (hndl1) mseed = hndl1;
   	else
   	{
   		mseed = pMR->mrCreate(1, xe, ye);
      	spSpotListMask (hndl8, mseed);
   	}
   	mbord = pMR->mrCreateAs (mseed);
   	mwrk1 = pMR->mrCreateAs (mseed);
   	mwrk2 = pMR->mrCreateAs (mseed);
   	mwater = pMR->mrCreateAs (mseed);
   	mthr = pMR->mrCreateAs (mseed);
	mdebug = pMR->mrCreateAs (mseed);
   	nei4 = 1;
   	ibeg = SpotBegIndex;  ifin = ibeg + spots - 1;

// *** find initial bord mask:
   	pMR->mrThreshold (hndl8, mthr, "Less", thrmin); // less then min threshold
// *** main loop:
	for (spind = ibeg;  spind <= ifin;  spind++)
   	{
      	xmin = SpotBuffer[spind].Xmin;
      	xmax = SpotBuffer[spind].Xmax;
      	ymin = SpotBuffer[spind].Ymin;
      	ymax = SpotBuffer[spind].Ymax;
    	xrec = SpotBuffer[spind].Xrecon;
      	yrec = SpotBuffer[spind].Yrecon;
	 	pMR->mrReconFastD (mseed, mwrk1, xrec, yrec, data); // current seed
    	pMR->mrPixelwise (mseed, "Copy", mwrk2);
    	pMR->mrPixelwise (mwrk1, "Less", mwrk2); // seeds without current
      	pMR->mrNeiFun (mwrk2, "Dilate4", 1); // dilate it
    	pMR->mrPixelwise (mbord, "Or", mwrk2); // prohibited pixels mask
    	pMR->mrPixelwise (mthr, "Or", mwrk2); // prohibited pixels mask
    	pMR->mrPixelwise (mwrk1, "Copy", mwater); // current seed
      	pMR->mrWaterMask (hndl8, mwrk2, mwater);
    	pMR->mrPixelwise (mwater, "Copy", mwrk2); // current watershed mask
		pMR->mrPixelwise (mwater, "Or", mdebug); //
      	pMR->mrNeiFun (mwrk2, "Dilate4", 1); // dilate it
    	pMR->mrPixelwise (mwrk2, "Or", mbord); // add to prohibited pixels
//    	pMR->mrPixelwise (mbord, "Less", mthr); // prohibited because of spot threshold

//      xe1 = xmax + 1 - xmin;  ye1 = ymax + 1 - ymin;
//      if (xe1 < ye1) { emin = xe1;  emax = ye1; }
//      else { emin = ye1;  emax = xe1; }
      	pMR->mrRectMaskData (hndl8, mwrk1, curdata);
      	area = curdata[0];  flags = oldcyl = 0;
      	psum = curdata[1];
   		oldmin = thresh = curdata[2];
      	extra = 8;  pmax = curdata[3];
      	if (area == 0) goto FillSpotData;
		if ((armin = area / 4) < 5) armin = 5;

      	level = thresh - extra;
      	csum = psum - level * area + (pmax - level) * armin;
      	oldcyl = (100 * csum)
      			/ ((pmax - level) * (area + armin));
      	if (thresh < thrmin) { flags = 1;  goto FillSpotData; }
      	if (area >= areamax) { flags = 2;  goto FillSpotData; }
	// *** spot expansion loop:
ExpandLoop:
		perim = pMR->mrRecOutCont (mseed, mwrk2, xrec, yrec, nei4);
    	pMR->mrPixelwise (mwater, "And", mwrk2);
      	rep = 0;
CheckPix:
      	if (!(pMR->mrRectMaskData (hndl8, mwrk2, data)))
      	{
         	if (rep == 1) goto ExpandLoop; // it was the last pixel
         	if (rep == 2)
         	{  // last pixel was rejected because of formfactor
          		flags = 3;  goto FillSpotData;
         	}
			pMR->mrRecOutCont (mseed, mwrk2, xrec, yrec, nei4);
         	pMR->mrPixelwise (mthr, "And", mwrk2);
	      	if (pMR->mrRectMaskData (hndl8, mwrk2, data))
         	{
            	flags = 1; // threshold!!
         	}
         	goto FillSpotData; // spot is surrounded
      	}
      	cmax = data[3];   nmax = data[10];
      	cxmax = data[6];  cymax = data[7];
      	xe1 = xmax + 1 - xmin;  ye1 = ymax + 1 - ymin;
      	if ((xmin > cxmax) || (xmax < cxmax)) xe1++;
      	if ((ymin > cymax) || (ymax < cymax)) ye1++;

      	switch (formtype)
      	{
       		case 1:
      			if (xe1 < ye1) { emin = xe1;  emax = ye1; }
		      	else { emin = ye1;  emax = xe1; }
         		break;
       		case 2:
         		emin = 2 * (xe1 + ye1);  emax = perim + 4;
         		break;
       		case 3:
         		emin = 16 * area * area + 100;
            	emax = perim * perim * xe1 * ye1 + 100;
         		break;
         	default: emin = emax = 1;
      	}
      	if (cmax < oldmin) cut = cmax;   else cut = oldmin;
      	level = cut - extra;
      	csum = psum + cmax - level * (area + 1) + (pmax - level) * armin;
      	cyl = (100 * csum) / ((pmax - level) * (area + armin + 1));
      	cyl = cyl * emin / emax;
//      if (cmax < thrmin) { flags = 1;  goto FillSpotData; } - never happens
		if ((formfactor > 0) && (cut < formthresh) && (cyl < formfactor))
      	{
        	pMR->mrPutPixel (mwrk2, cxmax, cymax, 0);
         	rep = 2;  goto CheckPix;
      	}
      	pMR->mrPutPixel (mseed, cxmax, cymax, 1);
      	area++;  psum += cmax;
      	oldcyl = cyl; oldmin = cut;
      	if (area >= areamax) { flags = 2;  goto FillSpotData; }
      	if (xmin > cxmax) xmin = cxmax;
      	if (xmax < cxmax) xmax = cxmax;
      	if (ymin > cymax) ymin = cymax;
      	if (ymax < cymax) ymax = cymax;
      	if (nmax > 1)
      	{
        	pMR->mrPutPixel (mwrk2, cxmax, cymax, 0);
         	rep = 1;  goto CheckPix;
      	}
      	goto ExpandLoop;
FillSpotData:
		SpotBuffer[spind].PixNum = area;
      	SpotBuffer[spind].PixSum = psum;
      	SpotBuffer[spind].Thresh = oldmin;
      	SpotBuffer[spind].Tag1 = flags;
      	SpotBuffer[spind].Tag2 = oldcyl;
      	SpotBuffer[spind].Xmin = xmin;
      	SpotBuffer[spind].Xmax = xmax;
      	SpotBuffer[spind].Ymin = ymin;
      	SpotBuffer[spind].Ymax = ymax;
	}
   	if (mseed != hndl1) pMR->mrFree (mseed);
   	if (borders) pMR->mrMove (mdebug, borders);
   	pMR->mrFree (mwrk1);
   	pMR->mrFree (mwrk2);
   	pMR->mrFree (mwater);
   	pMR->mrFree (mthr);
	pMR->mrFree (mdebug);
   	pMR->mrFree (mbord);
Fin:
	return spots;
}



/*************************************************** spGetReadable */
/***
	Function:	spGetReadable

	Description: creates ASCII description of a given
    	Spot and sends it to given location

	Arguments:
        sp - pointer to a Spot structure
        	!! if (sp == NULL) : sends string of column' names
        pget - pointer to the receiving buffer
	Returns: number of BYTEs actually sent
    	(without terminating zero)

***/
static char *headlines =
	"  #  Thr Area SumPix Aver Xext Yext MaxP Xmin Ymin Xrec Yrec      Tag Flg";

#define SCc  20

static long mapping[] =
	{ -1,   4,    5,      6, SCc+2,SCc,SCc+1,  7,   0,   1,   8,   9,  10,  11};

long  CZBlob::spGetReadable(long spotnum, void *pget)
{
	long i, val, len, ret, wlen,
    	spl[40];
   Spot *sp;
   DWORD oldoffs, posnum, offs;
   char *r;

   r = (char *)pget;  len = strlen (headlines);
   if ((spotnum < 1) || (spotnum > SpotsNumber))
   {
      ret = len;
      MovMemory (headlines, r, ret + 1);
      goto Fin;
   }
   sp = &SpotBuffer[SpotBegIndex + spotnum - 1];
   MovMemory (sp, spl, sizeof(Spot));
   spl[SCc] = sp->Xmax + 1 - sp->Xmin;
   spl[SCc+1] = sp->Ymax + 1 - sp->Ymin;
   spl[SCc+2] = (sp->PixSum + sp->PixNum / 2) / sp->PixNum;
   i = offs = ret = 0;
   while (i < 24)
   {
		oldoffs = offs;
		wlen = GetTheWord (headlines, &offs, len);
      if (!wlen) break;
      posnum = (offs += wlen) - oldoffs;;
      if (i) val = spl[mapping[i]];
      else val = spotnum;
      ret += sprintf (r + ret, "%*li", posnum, val);
      if (!i) { r[ret++] = ':';  offs++; }
      i++;
	}
Fin:
   return ret;
}



/***************************************************** spShowSpots */
/***
	Function:	spShowSpots

	Description:

	Arguments:
        spotnum - from 1
	Returns:
***/
void CZBlob::spShowSpots(long spotnum, long numof,	long specnum)
{
	long i;
   char *w;

 	if ((!spotnum) || (spotnum > SpotsNumber)) return;
   if (spotnum + numof > SpotsNumber + 1)
	   numof = SpotsNumber + 1 - spotnum;
   w = (char *)WorkBuffer;
	w += spGetReadable(0, w); // headlines
	for (i = spotnum;  i < spotnum + numof;  i++)
   {
      w += sprintf(w, "\n");
      if (i == specnum) *w++ = '=';
		w += spGetReadable(i, w);
   }
   w += sprintf(w, "\n # of calls: %lu", CallSum);
   w++;
   sprintf(w, "Spots: %lu %lu %lu %lu",
    		SpotBegIndex, SpotsNumber,
         FirstFreeSpot, MaxSpotsNumber);
	MessageBox(NULL, (char *)WorkBuffer, w, MB_SYSTEMMODAL | MB_OK);
}



/*********************************************************** IsNew */
bool CZBlob::IsNew (long num, long bordnum, long maxnum)
{
    long i, ifin, ib, xrec, yrec, xmin, ymin, xmax, ymax;
    Spot *sp;

    i = SpotBegIndex;  ifin = i + maxnum;
    ib = i + bordnum;
   	sp = &SpotBuffer[i + num];
    xrec = sp->Xrecon;  yrec = sp->Yrecon;
    xmin = sp->Xmin;  ymin = sp->Ymin;
    xmax = sp->Xmax;  ymax = sp->Ymax;

    while (i <= ifin)
    {
     	sp = &SpotBuffer[i++];
        if (sp->PixNum == 0) continue;  // ignore empty ones
        if ((sp->Xrecon == xrec) && (sp->Yrecon == yrec))
        	return false;
        if ((sp->Xmin <= xmin) && (sp->Ymin <= ymin)
            && (sp->Xmax >= xmax) && (sp->Ymax >= ymax))
        { // is inside
            if (i >= ib)
                return false;
        }
        // else only partial overlapping
    }
    return true;
}



/******************************************************** spCount8 */
/***
	Function:	spCount8
	Description:
		regular blob on 8 bit/pix image
	Arguments:
   	imagemem -
      xe, ye, pitch -
		begthresh -
		thrstep -
      minarea -
      maxarea -
	Returns:
    	spot's number
***/
long  CZBlob::spCount8 (
      BYTE *imagemem,
      long xe, long ye, long pitch,
		long begthresh,
      long minarea, long maxarea)
{
	BYTE *mem;
   	Spot *sp, *spr;
   	long thresh, waiting, i, k, n, oldarea, newarea,
    	spots, newspots, thrstep, pixmax,
      	tag, ftag, nextthr, NN,
    	cxr, cyb, cxl, cyt,
      	begstep = 32, ret = -1;
   	DWORD begtime, delta = 0;

   if (!ClearBuffer()) goto Fin;
   	begtime = GetTimeMsec();
	thresh = begthresh;
   	waiting = Find8plus (imagemem,
    			xe, ye, pitch, thresh, 0, minarea, 256);
   	CallSum = 1;
   	i = 0;  thrstep = begstep;
   	while (i < waiting)
   	{
		if (Blob2DebugFlag & 1)
     		spShowSpots(1, waiting + 2, i + 1);
     	sp = &SpotBuffer[SpotBegIndex + i];
      	oldarea = sp->PixNum;
      	cxl = sp->Xmin;  cyt = sp->Ymin;
      	cxr = sp->Xmax;  cyb = sp->Ymax;
	   	mem = imagemem + (cyt * pitch + cxl);
      	pixmax = sp->PixMax;
      	ftag = sp->Tag;  NN = ftag >> 16;
      	tag = ftag & 0xffff;
      	if (NN == 1)
      	{
        	thrstep = tag;
         	tag = pixmax + 1;
      	}
	   	// Tag = NN * 0x10000 + T
   		// NN==0: T = min threshold already checked where (spots == 0)
	   	// NN==1: T = last successful step
   		// NN>1:  T = min threshold already checked where (spots > 0)
   		thresh = sp->Thresh;
      	if (thresh == pixmax)
        	goto NextSpot; // is nonsplitable
		nextthr = thresh + thrstep;
      	while ((nextthr > pixmax) || (nextthr >= tag))
      	{
			if (thrstep > 1)
         	{
				thrstep = (thrstep + 1) / 2;
          		nextthr = thresh + thrstep;
            } else break;
      	}
      	if (sp->ThreshNum >= minarea) thrstep = 1;
      	if ((thrstep == 1) || (nextthr > pixmax)) goto NewSearch;
CheckThresh:
		nextthr = thresh + thrstep;
      	if ((nextthr > pixmax) || (nextthr >= tag))
      	{
KeepIt:
			if (thrstep > 1)
         	{
           		thresh -= thrstep;
            	thrstep = (thrstep + 1) / 2;
            	goto CheckThresh;
         	} else
         	{
NextSpot:
            	if (IsNew(i, i, i - 1))
            	{ // check that is not one of already included
	        		i++;
            	} else
            	{
		         	sp->PixNum = 0;
			      	CompactSpots();
			      	waiting--;
            	}
            	CleanTail(waiting);
            	thrstep = begstep;
    	      	continue;
			}
		}
NewSearch:
      	if (Blob2DebugFlag == 0)
      	{
    		delta = GetTimeMsec() - begtime; // overflow is ok!
      		if (delta >= MaxTime) break;
      	}
      	if (oldarea < 2 * minarea)
        	goto NextSpot; // is nonsplitable
      	if ((thresh += thrstep) > pixmax)
        	goto KeepIt;
		if (thresh >= tag)
      	{
        	thresh = tag;  thrstep = 1;
      	}
	   	// Tag: father's one
	   	spots = Find8plus (mem,
    			cxr + 1 - cxl, cyb + 1 - cyt, pitch,
            	thresh, 0, minarea, ftag);
		CallSum++;
		newspots = spots - waiting;
      	if (newspots == 0)
      	{ // no new ones, keep father
         	NN = 0;
        	if (thresh < tag)
           		ftag = sp->Tag = thresh;
LowTag:
        	if (thresh < tag)
         	{
           		tag = thresh;
            	if (newspots > 1)
            	{
               		NN = newspots;
              		ftag = newspots * 0x10000 + tag;
            	}
         	}
        	goto KeepIt;
      	}
		spr = &SpotBuffer[SpotBegIndex + waiting];
      	if (newspots == 1)
      	{   // check max area:
         	newarea = spr->PixNum;
			if (oldarea > maxarea)
         	{ 	// old is big:
				if (((newarea >= maxarea) && (oldarea < newarea + minarea))
              		|| (thrstep == 1))
            	{
               		if (spr->ThreshNum == 0)
                		thresh = spr->NextThr - 1;
               		if ((oldarea - newarea) * 2 < minarea)
                		thrstep *= 2;
               		if ((NN == 1) || (thresh + thrstep < tag))
               		{
                 		ftag = 0x10000 + thrstep;
               		}
                	goto UseNewSpots;
            	}
           		goto UseOldSpot;
			}

        	if ((oldarea < newarea + minarea) || (thrstep == 1))
        	{
            	if (oldarea < newarea + minarea + spr->ThreshNum)
            	{
              		thresh = spr->NextThr - 1;
               		newarea -= spr->ThreshNum;
            	}
            	if ((oldarea - newarea) * 2 < minarea)
					thrstep *= 2;
            	oldarea = newarea;
            	CleanTail(waiting);
            	goto NewSearch;
        	}
        	goto UseOldSpot;
		}

      	// erase new and try to low the threshold:
      	if (thrstep > 1)
      	{
UseOldSpot:
		   	CleanTail(waiting);
         	if  (newspots > 1)
           		goto LowTag;
        	goto KeepIt;
		}
UseNewSpots:
      	// add new spots, erase the old one
		if ((newspots > 1) && (thrstep == 1)) ftag = 256;
      	sp->PixNum = 0;  k = spots - 1;
      	for (n = waiting;  n < spots;  n++)
      	{
          	spr->Xmin += cxl;  spr->Ymin += cyt;
          	spr->Xmax += cxl;  spr->Ymax += cyt;
          	spr->Xrecon += cxl;  spr->Yrecon += cyt;
          	spr->Tag = ftag;
          	if (!IsNew(n, i, waiting - 1))
          	{
             	spr->PixNum = 0;  k--;
          	}
          	if ((spr->ThreshNum == 0)
           	 	&& (spr->NextThr <= spr->PixMax))
          	{	spr->Thresh = spr->NextThr - 1; }
          	spr++;
      	}

      	CompactSpots();
      	waiting = k;
	   	CleanTail(waiting);
      	thrstep = begstep;
   	}
	CleanTail(waiting);
   	if (delta < MaxTime) ret = waiting;
Fin:
	if (Blob2DebugFlag & 2)
    	spShowSpots(1, waiting + 2, 0);
	return ret;
}


/******************************************************* spCount81 */
/***
	Function:	spCount81
	Description:
		regular blob on 8 bit/pix image
	Arguments:
        imagemem -
        xe, ye, pitch -
		begthresh -
		thrstep -
        minarea -
        maxarea -
	Returns:

***/
long  CZBlob::spCount81 (
			BYTE *imagemem,
			long xe, long ye, long pitch,
			long begthresh,
        	long minarea, long maxarea)
{
	BYTE *mem;
   Spot *sp, *spr;
   long thresh, waiting, i, area,
    	spots, newspots, oldthr,
    	cxr, cyb, cxl, cyt, low, high;
   long ret = -1;

   if (!ClearBuffer()) goto Fin;
   thresh = begthresh;
   // Tag==-1 : found in the first portion
   waiting = Find8plus (imagemem,
   			xe, ye, pitch, thresh, 0, minarea, -1);
   i = 0;
   while (i < waiting)
   {
     	sp = &SpotBuffer[SpotBegIndex + i];
      thresh = sp->Thresh;
      low = thresh + 1;
      high = sp->PixMax;
CheckThresh:
      if ((sp->PixNum < 2 * minarea) // is nonsplitable
        	|| (low > high)
        	|| ((low == high) && (thresh == high)))
      {
NextSpot:
			i++;  continue;
		}
		oldthr = thresh;
      thresh = (high + low) / 2;
      if (thresh == oldthr) thresh++;
      cxl = sp->Xmin;  cyt = sp->Ymin;
      cxr = sp->Xmax;  cyb = sp->Ymax;
		mem = imagemem + (cyt * pitch + cxl);
	   // Tag: father's index
	   spots = Find8plus (mem,
    			cxr + 1 - cxl, cyb + 1 - cyt, pitch,
            thresh, 0, minarea, i + 1);
		newspots = spots - waiting;
//spShowSpots(hndl, 1, spots, i);
      if (newspots == 0)
      { // no new ones at this level
        	high = thresh - 1;
        	goto CheckThresh;
      }
      spr = &SpotBuffer[SpotBegIndex + waiting];
      if (newspots == 1)
      {   // check max area:
        	area = spr->PixNum;
			if (sp->PixNum > maxarea)
         {
           	if ((area > maxarea) || (low + 2 > high))
              	goto MoreThanOne;
            // both are  big; keep new
            goto ToLow;
         }
         // erase new and try to grow threshold up:
//**        SpotLists[hndl].SpotsNumber = waiting;
			CleanTail(waiting);
         if (area < 2 * minarea) goto NextSpot; // unsplitable
         low = thresh;
         goto CheckThresh;
		}
      if (low + 1 < high)
		{
ToLow:
//**		    SpotLists[hndl].SpotsNumber = waiting;
		    CleanTail(waiting);
          high = thresh;
          goto CheckThresh;
		}
      // add new spots, erase the old one
MoreThanOne:
      while (newspots--)
      {
         spr->Xmin += cxl;  spr->Ymin += cyt;
         spr->Xmax += cxl;  spr->Ymax += cyt;
         spr->Xrecon += cxl;  spr->Yrecon += cyt;
         spr->Tag = -1;   spr++;
      }
      sp->PixNum = sp->PixSum = 0;
      CompactSpots();
//**	    SpotLists[hndl].SpotsNumber =
      waiting = spots - 1;
	   CleanTail(waiting);
	}
   ret = waiting;
Fin:
	return ret;
}


/***************************************************** spCountFast */
/***
 	Calculates number of spots using subset of thresholds.
    Updates mask if is given.
    If spots are Black on White then threshstep must be negative.
 	returns:
   	 number of spots if OK,
      -1 otherwise
***/
long  CZBlob::spCountFast (long hndl, long resmask, long begthresh,
                    long minarea, long maxarea, long threshstep)
{
	long xe, ye, thr, hmi, hma, ones, mask = 0, hwrk = 0,
    	 spotnum = 0, ret = -1,
         data[4];
    char *operation;

    if (pMR->mrGetRectInfo(hndl, &xe, &ye) != 8) goto Fin;
    if (resmask) { mask = resmask;  pMR->mrOperMask (mask, "Zeros"); }
    else mask = pMR->mrCreateAsWithPix (hndl, 1);
   	hwrk = pMR->mrCreateAs (mask);
    hmi = pMR->mrCreateAs (mask);
   	hma = pMR->mrCreateAs (mask);  pMR->mrOperMask (hma, "Ones");
    thr = begthresh;
    if (threshstep > 0) operation = ">="; else operation = "<=";
MainLoop:
    ones = pMR->mrThreshold (hndl, hwrk, operation, thr);
    if (ones < 0) goto Fin;
    pMR->mrPixelwise (hma, "And", hwrk);
    pMR->mrNeiFun (hwrk, "Open4", 1);
	pMR->mrSplitRange (hwrk, hmi, hma, minarea, maxarea, data);
    if (data[1] > 0)
    { // there are components inside given range
	    pMR->mrPixelwise (hwrk, "Or", mask);
    	spotnum += data[1];
        pMR->mrNeiFun (hwrk, "Dilate4", 1);
	    pMR->mrPixelwise (hwrk, "Less", hma);
    }
    if (data[2])
    {
	    thr += threshstep;
	    if ((threshstep > 0) && (thr < 255)) goto MainLoop;
    	if ((threshstep < 0) && (thr > 0)) goto MainLoop;
    }
	ret = spotnum;
Fin:
	if (hwrk) { pMR->mrFree(hwrk); pMR->mrFree(hmi); pMR->mrFree(hma); }
	if ((mask != resmask) && (mask != 0)) pMR->mrFree(mask);
	return ret;
}



/***************************************************** spFindSpots */
/***
 	Calculates Spot list
 	returns:
   	 number of spots if OK,
      -1 otherwise

***/
static char *SpotsOps = "<= < >= > ";

long  CZBlob::spFindSpots (long hndl, char *operation,
								long threshold, long minarea)
{
	long opcase, pix, xe, ye, pitch, inverse,
    	 spots = -1;
    DWORD offs = 0;
    BYTE *mem;
    long invadd[] = {255, 255, 0, 0,
    				   0,  -1, 0, 1};

    opcase = WordInText (SpotsOps, &offs, 0, operation, 0);
    if (!(opcase--)) goto Fin;
    inverse = invadd[opcase];
    threshold += invadd[opcase + 4];
 	 pix = pMR->mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
    if (pix == 8)
    {
    	  spots =  spFind8 (mem, xe, ye, pitch,
								  threshold, inverse, minarea);
        goto Fin;
    }
Fin:
	return spots;
}


/*************************************************** spCountRadius */
/***
 	Calculates number of spots using given "radius".
    Updates mask if is given.
 	returns:
   	 number of spots if OK,
      -1 otherwise

***/
long  CZBlob::spCountRadius (long hndl,
				long radius, long thresh, long minarea,
				long invert, long mask)
{
	long xe, ye, maxval, h8 = 0, ret = -1;

    if (pMR->mrGetRectInfo(hndl, &xe, &ye) != 8) goto Fin;
    if (radius < 1) goto Fin;
   	h8 = pMR->mrCopyAs (hndl);
    if (invert) pMR->mrConstPixelwise (255, "-", h8);
	maxval = pMR->mrOvusDistThr(h8, radius, thresh);
    if (maxval > 0)
    {
		ret = spFindSpots (h8, ">=", thresh, minarea);
    	if ((ret > 0) && (mask)) spSpotListMask (h8, mask);
    } else ret = 0;
Fin:
	if (h8 > 0) pMR->mrFree(h8);
    return ret;
}


/*************************************************** spFindMarkers */
/***
 	Calculates Spot list using recursive algorithm
 	returns:
   	 number of spots if OK,
      -1 otherwise
***/
long  CZBlob::spFindMarkers (long hndl, long threshold,
                    long minarea, long maxarea)
{
	long pix, xe, ye, pitch, spots = -1;
   BYTE *mem;

	pix = pMR->mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
   if (pix == 8)
   {
    	spots = spCount8 (mem, xe, ye, pitch,
        				threshold, minarea, maxarea);
	}
	return spots;
}



/***************************************************** spCountRange */
/***
 	Calculates number of spots using subset of quantiles.
    If <resmask> is given (i.e. != 0):
    	Updates <resmask>.
    If <begmask> is given (i.e. != 0):
    	Searches spots only inside <begmask> ones;

 	Returns:
   	 number of spots if OK,
      -1 otherwise
***/
long  CZBlob::spCountRange (long hndl, long resmask, long begmask,
                    long minarea, long maxarea,
					double quantbeg, double quantfin,
                    long maxrep, long nopen)
{
	long xe, ye, thr, hma, nn, i, ibeg, ifin, istep, minar,
    	 mask = 0, hwrk = 0, spotnum = 0, ret = -1,
         data[4], thrs[255];
    char *operation;
    double qmin, qmax, fct, dwrk;

    if ((maxarea < 1) || (maxarea < minarea)) goto Fin;
    if (pMR->mrGetRectInfo(hndl, &xe, &ye) != 8) goto Fin;
    hwrk = pMR->mrCreate (1, xe, ye);
    if (begmask > 0)
    {
    	hma = pMR->mrCopyAs (begmask);
	    if ((nn = pMR->mrMaskSubrectOnes (hma, 0, 0, 0, 0)) < 1)
        { ret = 0;  goto Fin; }
    } else
    {
    	hma = pMR->mrCreateAs (hwrk);  pMR->mrOperMask (hma, "Ones");
        nn = xe * ye;
    }
    if (resmask > 0) { mask = resmask;  pMR->mrOperMask (mask, "Zeros"); }
    if (maxrep > 255) maxrep = 255;
	else if (maxrep < 2) maxrep = 2;
    if (maxarea > nn) maxarea = nn;
    if (minarea <= 1) { dwrk = 1.0 / nn;  minar = 1; }
    else { dwrk = (minarea - 1.0) / nn;  minar = minarea; }
    if (quantbeg < dwrk) quantbeg = dwrk;
    if (quantfin < dwrk) quantfin = dwrk;
    if (quantbeg > quantfin)
    {   // decrease threshold
    	qmax = quantbeg;  qmin = quantfin;
        operation = "<=";
        ibeg = maxrep - 1;  ifin = 0;  istep = -1;
    } else
    {   // increase threshold
    	qmin = quantbeg;  qmax = quantfin;
        operation = ">=";
        ifin = maxrep - 1;  ibeg = 0;  istep = 1;
    }
    fct = exp(log(qmax / qmin) / (maxrep - 1.0));
    for (i = 0;  i < maxrep;  i++)
    {
    	thrs[i] = floor(qmin * nn + 0.5);
        qmin *= fct;
    }
	pMR->mrMaskQuantiles(hndl, begmask, (DWORD *)thrs, maxrep);
    i = ibeg;  thr = -1;
    while (true)
    {
    	if (thr != thrs[i])
        {
	        thr = thrs[i];
    		pMR->mrThreshold (hndl, hwrk, operation, thr);
		    pMR->mrPixelwise (hma, "And", hwrk);
    		if (nopen > 0 ) pMR->mrNeiFun (hwrk, "Open48", nopen);
            if (pMR->mrMaskSubrectOnes (hwrk, 0, 0, 0, 0) < minar) break;
			pMR->mrSplitRange (hwrk, 0, hma, minarea, maxarea, data);
	    	if (data[1] > 0)
		    { // there are components inside given range
    			spotnum += data[1];
    			if (mask) pMR->mrPixelwise (hwrk, "Or", mask);
		        pMR->mrNeiFun (hwrk, "Dilate4", 1);
			    pMR->mrPixelwise (hwrk, "Less", hma);
            }
	    }
	    if ((!data[2]) || (i == ifin)) break;
    	i += istep;
	}
	ret = spotnum;
Fin:
	if (hwrk) { pMR->mrFree(hwrk);  pMR->mrFree(hma); }
	return ret;
}


/***************************************************** spSombreros */
/***
    Transformation with matrix

    All MepMR->mrects are of 8 bit/pix;
    <rad> == n corresponds to the
    	{ -5 -5 <rad times 0> 6 8 6 <rad times 0> -5 -5}

    	where k = 2 * n + 1;   1 <= n <= 10;
    Returns: zero level if OK
    		 -1 otherwise
***/
long  CZBlob::spSombreros (long hsrc, long hres, long rad, long invert)
{
	long xe, ye, xew, yew, i, k, xmin, ymin, xmax, ymax;
    long *cfs, h32x = 0, h32y = 0, ret = -1;
    DWORD zero;

	// Check conformity:
    if (pMR->mrGetRectInfo(hsrc, &xe, &ye) != 8) goto Fin;
    if (pMR->mrGetRectInfo(hres, &xew, &yew) != 8) goto Fin;
    if ((xe != xew) || (ye != yew)) goto Fin;
    // Create intermediate rectangles:
    if ((h32x = pMR->mrCreate (32, xe, ye)) < 1) goto Fin;
	pMR->mrConvert(hsrc, h32x, 0);
    if (invert) pMR->mrConstPixelwise(255, "-", h32x);
    if ((h32y = pMR->mrCopyAs (h32x)) < 1) goto Fin;
	// Ajust radius:
    if (rad < 1) rad = 1;
    if (rad > xe / 3) rad = xe / 3;
    // create coeffitients:
    k = 2 * rad + 7;  cfs = (long *)WB;
    for (i = 0;  i < k;  i++) cfs[i] = 0;
    cfs[0] = cfs[1] = cfs[k-1] = cfs[k-2] = -5;
    cfs[rad+2] = cfs[rad+4] = 6; cfs[rad+3] = 8;
    // Calculate components:
	if (pMR->mrConvolution1D (h32x, cfs, k, 0, &xmin, &xmax)
    	 < 0) goto Fin;
	if (pMR->mrConvolution1D (h32y, cfs, k, 1, &ymin, &ymax)
    	 < 0) goto Fin;
    if (ymin > xmin) zero = -xmin; else zero = -ymin;
    pMR->mrConstPixelwise(zero, "+", h32x);
    pMR->mrConstPixelwise(zero, "+", h32y);
    pMR->mrPixelwise(h32y, "Min", h32x);
    // make the result:
    if ((ret = pMR->mrConvert(h32x, hres, -2)) > 0)
    	ret = zero * 255 / ret;
Fin:
	if (h32x > 0) pMR->mrFree(h32x);  if (h32y > 0) pMR->mrFree(h32y);
	return ret;
}



/************************************************ spDeleteOutbreaks */
/***
 	Delete spots with
        Area > Mean + Sigma * <sigfactor>
    Updates <border> (if not NULL)
 	returns:
   	 number of spots if OK,
      -1 otherwise

***/
long  CZBlob::spDeleteOutbreaks (double sigfactor, long *border)
{
    long ibeg, i, *indices, ifin, armax, spots, arbord;
    double area, sum, sum2, mean, sigma;

    ibeg = SpotBegIndex;
    arbord = SpotBuffer[ibeg].PixNum;
    if ((spots = SpotsNumber) <= 1) goto Fin;
    indices = (long *)WorkBuffer;
	IndexSortElems (&SpotBuffer[ibeg].PixNum, sizeof(Spot), spots, indices, 4, 0);
	sum = sum2 = 0;  ifin = ibeg + spots - 1;
 	for (i = ibeg;  i <= ifin;  i++)
   	{
        area = (double)SpotBuffer[i].PixNum;
      	sum += area;    sum2 += (area * area);
	}
Repeat:
    if (spots <= 1) goto Fin;
    mean = (double)sum / spots;
    sigma = (double)sum2 / spots - mean * mean;
    if (sigma < 0) sigma = 0;  else sigma = sqrt(sigma);
    arbord = floor(mean + sigma * sigfactor + 0.5);
    i = ibeg + indices[spots - 1];
    armax = SpotBuffer[i].PixNum;
    if (arbord < armax)
    {
        spots--;  area = armax;
      	sum -= area;    sum2 -= (area * area);
        SpotBuffer[i].PixNum = 0;
        goto Repeat;
    }
    if (spots < SpotsNumber) spots = CompactSpots();
Fin:
    if (border) *border = arbord;
    return spots;
}


/***************************************************** spDeleteTag */
/***
	Function:	spDeleteTag

	Description:
		delete spots with given tag value
	Arguments:
      tagvalue
	Returns: new number of spots / 0

***/

long  CZBlob::spDeleteTag(long tagvalue)
{
    long ibeg, ifin, i, spots = 0;

    if (!SpotBuffer) goto Fin;	// empty list
    ibeg = SpotBegIndex;  ifin = ibeg + SpotsNumber - 1;
    for (i = ibeg;  i <= ifin;  i++)
    {
        if (SpotBuffer[i].Tag == tagvalue)
        		SpotBuffer[i].PixNum = 0;
    }
    spots = CompactSpots();
Fin:
	return spots;
}


/******************************************************* spGetSpot */
/***
	Function:	spGetSpot

	Description:

	Arguments:
        spotnum - from 1
	Returns: number of BYTEs actually sent

***/
long  CZBlob::spGetSpot(long spotnum, BYTE *pget)
{
    long k, ret = 0;
    BYTE *r;

    if ((spotnum > 0) && (spotnum <= SpotsNumber))
    {
    	r = (BYTE *)&SpotBuffer[spotnum];
        ret = sizeof(Spot);
        for (k = 0;  k < ret;  k++) pget[k] = r[k];
    }
    return ret;
}


/************************************************* spContShapeInfo */
/***
    Updates:
    	resdata[0] = square of the min diameter;
    	resdata[1] = square of the max diameter;
    	resdata[2] = X1min
    	resdata[3] = Y1min
    	resdata[4] = X2min
    	resdata[5] = Y2min
    	resdata[6] = X1max
    	resdata[7] = Y1max
    	resdata[8] = X2max
    	resdata[9] = Y2max
 	Returns:
   	 100 * (square of the min diameter)/(square of the max diameter)

***/
long  CZBlob::spContShapeInfo (void *pcodes, long ncodes,
				long xstart, long ystart, long *resdata)
{
    long i, k, dx, dy, d2, nc2, d2min, d2max, imin, imax,
    	 sop = sizeof(POINT);
    POINT *pnt;

    pnt = (POINT *)malloc(ncodes * sop + sop);
	CodesToCoords (pcodes, ncodes, xstart, ystart,
    				&pnt[0].x, sop, &pnt[0].y, sop);
    d2min = ncodes * ncodes;
    d2max = 0;  nc2 = ncodes / 2;
    for (i = 0;  i < ncodes - nc2;  i++)
    {
        k = i + nc2;
	    dx = pnt[i].x - pnt[k].x;
        dy = pnt[i].y - pnt[k].y;
        d2 = dx * dx + dy * dy;
        if (d2min > d2) { d2min = d2;  imin = i; }
        if (d2max < d2) { d2max = d2;  imax = i; }
	}
    resdata[0] = d2min;
    resdata[1] = d2max;
    resdata[2] = pnt[imin].x;		// X1min
    resdata[3] = pnt[imin].y;		// Y1min
    resdata[4] = pnt[imin + nc2].x;	// X2min
    resdata[5] = pnt[imin + nc2].y;	// Y2min
    resdata[6] = pnt[imax].x;		// X1max
    resdata[7] = pnt[imax].y;		// Y1max
    resdata[8] = pnt[imax + nc2].x;	// X2max
    resdata[9] = pnt[imax + nc2].y;	// Y2max
    free(pnt);
    return ((200 * d2min + d2max) / (2 * d2max));
}


/************************************************ spGray8ToNormArg */
/***
    Pixel-to-pixel transformation:

    	hnorm = Abs(Grad(hsrc))
    	harg = Argument(Grad(hsrc))

    All MepMR->mrects are of 8 bit/pix;
    Argument range is mapped:
    	{-pi, pi} => {0, 255} if flag==0;
    	{0, pi} => {0, 255} if flag!=0;
    0 is OK for <harg>.
    Norm is mapped approximately to the range 0 - 255, with
    	saturation at 255.
    <vicinity> == n corresponds to the matrix {k*k},
    	where k = 2 * n + 1;   1 <= n <= 10;
    Returns: max pixel value before saturation in hnorm
    		 or -1
***/
long  CZBlob::spGray8ToNormArg (long hsrc,
				long hnorm, long harg, long vicinity, long flag)
{
	long xe, ye, xew, yew, i, k, lwrk, xmax, ymax;
    long *cfs, *cfs1, h32x = 0, h32y = 0, ret = -1;
    double mul255, normf = 255.0;

	// Check conformity:
    if (pMR->mrGetRectInfo(hsrc, &xe, &ye) != 8) goto Fin;
    if (pMR->mrGetRectInfo(hnorm, &xew, &yew) != 8) goto Fin;
    if ((xe != xew) || (ye != yew)) goto Fin;
    if (harg)
    {	// 0 is OK for <harg>
	    if (pMR->mrGetRectInfo(harg, &xew, &yew) != 8) goto Fin;
    	if ((xe != xew) || (ye != yew)) goto Fin;
    }
    // Create intermediate rectangles:
    if ((h32x = pMR->mrCreate (32, xe, ye)) < 1) goto Fin;
	pMR->mrConvert(hsrc, h32x, 0);
    if ((h32y = pMR->mrCopyAs (h32x)) < 1) goto Fin;
	// Ajust vicinity:
    if (vicinity < 1) vicinity = 1;
    if (vicinity > WorkBufferSize / 20) vicinity = WorkBufferSize / 20;
    k = 2 * vicinity + 1;
    cfs = (long *)WB;  cfs1 = cfs + k;
    for (i = 0;  i < k;  i++)
    {
    	cfs[i] = i - vicinity;  cfs1[i] = 1;
    }
    // Calculate components of gradient:
	if (pMR->mrConvolution1D (h32x, cfs, k, 0, &lwrk, &xmax)
    	 < 0) goto Fin;
	if (pMR->mrConvolution1D (h32x, cfs1, k, 1, &lwrk, &xmax)
    	 < 0) goto Fin;
    if ((lwrk = abs(lwrk)) > xmax) xmax = lwrk;
	if (pMR->mrConvolution1D (h32y, cfs, k, 1, &lwrk, &ymax)
    	 < 0) goto Fin;
	if (pMR->mrConvolution1D (h32y, cfs1, k, 0, &lwrk, &ymax)
    	 < 0) goto Fin;
    if ((lwrk = abs(lwrk)) > ymax) ymax = lwrk;
    // norm factor:
    mul255 = ( xmax > ymax ) ? normf / xmax : normf / ymax ;
    // make the result:
    ret = pMR->mrXYtoNA (h32x, h32y, hnorm, harg, mul255, flag);
Fin:
	if (h32x > 0) pMR->mrFree(h32x);  if (h32y > 0) pMR->mrFree(h32y);
	return ret;
}


/************************************************* spLongSmoothArc */
/***
 	Calculates

    For  *codes and/or *curvs  NULL is OK.
    For  maskarc and/or maskcont  0 is OK.
    Updates:
    	resdata[0] = start X coordinate of longest smooth arc;
    	resdata[1] = start Y coordinate of longest smooth arc;
    	resdata[2] = number of codes in longest smooth arc;
    	resdata[3] = maximal curvature;
        resdata[4] - number of elements with (curvature >= curvthresh);
    	resdata[5] = number of codes in contour;
    	resdata[6] = lenght of longest smooth arc;
    	resdata[7] = lenght of contour;
    	if (codes != NULL) then it is filled with incremental
        	codes of the contour, begining from the start point;
 	Returns:
   	 (100 * (lenght of longest smooth arc)/(lenght of contour)) if OK,
     0 otherwise

static BYTE WorkBuffer[WorkBufferSize];
***/
long  CZBlob::spLongSmoothArc (long masksrc, long maskcont, long maskarc,
                long halfstepmin, long halfstepmax, long curvthresh,
                long *resdata, void *codes, void *curvs, long codeslim)
{
    long xstart, ystart, ncodes, limit, longest, i, ret,
         ccdata[8];
    BYTE *pc;

    if (codes) { pc = (BYTE *)codes;  limit = codeslim; }
    else { pc = WorkBuffer;  limit = WorkBufferSize; }
	ncodes = pMR->mrCodeContour (masksrc, maskcont, ccdata, pc, limit);
    if (!ncodes)
    {
        for (i = 0;  i < 6;  i++) resdata[i] = 0;
        ret = 0;	goto Fin;
    }
    xstart = ccdata[0]; 	ystart = ccdata[1];
	CodeCurvature (pc, ncodes, xstart, ystart,
                curvs, resdata,
                halfstepmin, halfstepmax, curvthresh);
    // fills resdata[0 - 4]
	resdata[5] = ncodes;	// number of codes in contour
    longest = resdata[2];
	resdata[6] = CodesLength(pc, longest); // lenght of longest smooth arc
	resdata[7] = CodesLength(pc, ncodes);  // lenght of contour

    if (maskarc)
    {
    	pMR->mrOperMask(maskarc, "Zeros");	// clear result mask
        if (longest)
        {
	    	xstart = resdata[0]; 	ystart = resdata[1];
			pMR->mrMaskDrawCode (maskarc, &xstart, &ystart, pc, longest, 1);
        }
	}
    ret =  (200 * resdata[6] + resdata[7]) / (resdata[7] * 2);
Fin:
    return ret;
}


/************************************************ spMaskShapesInfo */
/***
    Fills first 13(?) pixels in each row of h32 with information
    about linked components in source mask:
    	{Ni -  number of points in the component
         Xi, Yi - coordinates of some point belonging to
                    the i-th linked component,
   		 Xmini, Ymini, Xmaxi, Ymaxi - min/max X,Y coordinates
        			of the i-th linked component.
         AxRati - main Axis ratio (*1000)
         Cmpi - measure of compactness (*1000)
         CalMini, CalMaxi - Caliper Min/Max measures
         Rmaxi - Max Radius of the 'best fitting' circle
         Rndi - measure of roundness
        },
    maxcomp - max number of components to fit in <data>
?    The result array is sorted by Ni in decreased order.
 	Returns:
   	 number of linked components bigger than <minarea>
             (may be more than <maxcomp>),
     or -1

***/
long  CZBlob::spMaskShapesInfo (long hmask, long h32, long minarea)
{
    long i, k, lastx, lasty, hseed, hseed1, hlimit, ones, onesmin,
         operation, elsize, sortoffset, portions, xc, yc,
         xc1, yc1, imin, imax,
         compnum = -1;
    long sizedata[13], stat1data[mrStat1DataLength],
    	 calipers[16],
         onesoffset = 0,
         xcycoffset = 1,
         minmaxoffset = 3,
         axratoffset = 7,
         cmpoffset = 8,
         calminoffset = 9,
         calmaxoffset = 10,
         radmaxoffset = 11,
         roundoffset = 12;
    long xe32, maxcomp, pitch32, *mem32, xemin = 13, datasize;
    BYTE *data;
    double distmin = 5.0, radaver, radmin, radmax;

    lastx = lasty = hseed = hseed1 = hlimit = 0;
	datasize = xemin * sizeof(long);
    if ((hseed = pMR->mrCreateAs (hmask)) < 1) goto Fin;
    if ((hseed1 = pMR->mrCreateAs (hmask)) < 1) goto Fin;
    if ((hlimit = pMR->mrCopyAs (hmask)) < 1) goto Fin;
	if (pMR->mrGetExtendedInfo (h32, &xe32, &maxcomp, &pitch32, &data) != 32)
		goto Fin;
    if (xe32 < xemin) goto Fin;
    // fill array of coordinates of components:
    compnum = i = portions = 0;
    while (pMR->mrFindNext (hlimit, &lastx, &lasty, 1) > 0)
	{
        ones = pMR->mrReconFastD (hlimit, hseed, lastx, lasty,
        					 sizedata + minmaxoffset);
        if (ones < minarea)
        {
	        pMR->mrPixelwise (hseed, "Less", hlimit);
    	    continue;
        }
        // set xc, yc to the center of the rectangle:
        // (xmin + xmax)/2, (ymin + ymax)/2:
        xc = (sizedata[minmaxoffset + 0] + sizedata[minmaxoffset + 2]) / 2;
        yc = (sizedata[minmaxoffset + 1] + sizedata[minmaxoffset + 3]) / 2;
        if (pMR->mrGetPixel (hseed, xc, yc) == 0) pMR->mrDistToMask (hseed, &xc, &yc);
        sizedata[xcycoffset] = xc;
        sizedata[xcycoffset + 1] = yc;
        sizedata[onesoffset] = ones;
		pMR->mrStat1 (hseed, stat1data);
//   Data[10]:= 1000 * (shortest main axis / longest main axis)
//   Data[11]:= 1000 * (measure of compactness)
		sizedata[axratoffset] = stat1data[10];
		sizedata[cmpoffset] = stat1data[11];
		pMR->mrMaskCalipers (hseed, calipers, &imin, &imax, &xc1, &yc1);
		sizedata[calminoffset] = calipers[imin];
		sizedata[calmaxoffset] = calipers[imax];

		pMR->mrRecOutCont (hlimit, hseed1, lastx, lasty, 1);
		spArcCenter (hseed1, distmin, &xc, &yc, &radaver, &radmin, &radmax);
		sizedata[radmaxoffset] = (long)(radmax + 0.5);
		sizedata[roundoffset] = (long)(1000 * radmin / radmax);

        compnum++;
    	if (compnum <= maxcomp)
        {
            i = portions++;
        } else
        {
         	i = -1;  onesmin = ones;
            for (k = 0;  k < maxcomp;  k++)
            {
            	mem32 = (long *)(data + (pitch32 * k));
             	if (mem32[onesoffset] < onesmin)
                {
                    i = k;
                 	onesmin = mem32[onesoffset];
                }
            }
        }
        if (i >= 0)
        {
            MovMemory(sizedata, data + (pitch32 * i), datasize);
        }
        pMR->mrPixelwise (hseed, "Less", hlimit);
    }
    if (portions > 1)
    {	// is sortable
	    elsize = pitch32;
        sortoffset = onesoffset * sizeof(long);
        operation = (sortoffset << 8) | 0x84;   // 0x<00>84;
        SortInPlace (data, elsize, portions, operation);
    }
Fin:
    if (hseed > 0) pMR->mrFree(hseed);
    if (hseed1 > 0) pMR->mrFree(hseed1);
    if (hlimit > 0) pMR->mrFree(hlimit);
	return compnum;
}


/****************************************************** spNearSpot */
/***
	Function:	spNearSpot

	Description:
		returns a 'nearest' spot's number
	Arguments:
        xc, yc - coordinates of point
        	IF (xc < 0):  yc is a number of spot to
            find nearest to.
	Returns: number of spot / 0

***/
long  CZBlob::spNearSpot(long xc, long yc)
{
	long ibeg, ifin, i, outind, outval,
   		recind, recval, dx, dy, dist,
        	exclude = -1;

   	outind = recind = 0;
   	if ((!SpotBuffer) || (!SpotsNumber)) goto Fin;	// empty list
   	outval = recval = 999999999;
   	ibeg = SpotBegIndex;  ifin = ibeg + SpotsNumber - 1;
   	if (xc < 0)
   	{
   		exclude = ibeg + yc - 1;
      	if ((exclude < ibeg) || (exclude > ifin))
      		goto Fin;	// wrong number
      	xc = SpotBuffer[exclude].Xrecon;
		yc = SpotBuffer[exclude].Yrecon;
   	}
 	for (i = ibeg;  i <= ifin;  i++)
   	{
   		if (i == exclude) continue;
      	dx = dy = 0;
      	if (xc < SpotBuffer[i].Xmin) dx = SpotBuffer[i].Xmin - xc;
      	if (xc > SpotBuffer[i].Xmax) dx = xc - SpotBuffer[i].Xmax;
      	if (yc < SpotBuffer[i].Ymin) dy = SpotBuffer[i].Ymin - yc;
      	if (yc > SpotBuffer[i].Ymax) dy = yc - SpotBuffer[i].Ymax;
      	if (dx + dy)
      	{ // is outside
      		if ((dist = dx * dx + dy * dy) < outval)
    	   	{
        		outval = dist;  outind = i;
         	}
      	} else
      	{ // is inside
	      	dx = xc - SpotBuffer[i].Xrecon;
    		dy = yc - SpotBuffer[i].Yrecon;
     		if ((dist = dx * dx + dy * dy) < recval)
	      	{
    	   		recval = dist;  recind = i;
         	}
      	}
   	}
	if ((recind == 0) || (outval < recval)) recind = outind;
Fin:
	return recind;
}


/************************************************** spMinDistances */
/***
	Function:	spMinDistances

	Description:
    	Distances are between max pixel points.
		Fills  Tag3 with 'nearest' spot's number,
               Tag4 with square of the distance
	Arguments:
	Returns:

***/
void  CZBlob::spMinDistances()
{
    long ibeg, ifin, i, k, xc, yc,
    	recind, recval, dx, dy, dist;

    if ((!SpotBuffer) || (!SpotsNumber)) goto Fin;	// empty list
    ibeg = SpotBegIndex;  ifin = ibeg + SpotsNumber - 1;
 	for (i = ibeg;  i <= ifin;  i++)
   	{
     	SpotBuffer[i].Tag3 = 0;
     	SpotBuffer[i].Tag4 = 0x40000000;
    }
    if (ifin == ibeg) goto Fin;
 	for (k = ibeg;  k < ifin;  k++)
   	{
	    recind = SpotBuffer[k].Tag3;
        recval = SpotBuffer[k].Tag4;
        xc = SpotBuffer[k].Xrecon;
        yc = SpotBuffer[k].Yrecon;
 		for (i = k + 1;  i <= ifin;  i++)
	   	{
	        dx = xc - SpotBuffer[i].Xrecon;
    	    dy = yc - SpotBuffer[i].Yrecon;
     		if ((dist = dx * dx + dy * dy) < recval)
	        {
    	     	recval = dist;  recind = i;
            }
     		if (dist  < SpotBuffer[i].Tag4)
	        {
	    		SpotBuffer[i].Tag3 = k;
        		SpotBuffer[i].Tag4 = dist;
            }
        }
	     SpotBuffer[k].Tag3 = recind;
        SpotBuffer[k].Tag4 = recval;
    }
Fin: ;
}


/******************************************************** spSetTag */
/***
	Function:	spSetTag

	Description:

	Arguments:
        spotnum - from 1
	Returns:

***/
void  CZBlob::spSetTag(long spotnum, long tagval)
{
 	if ((spotnum) && (spotnum <= SpotsNumber))
   {
    	SpotBuffer[spotnum].Tag = tagval;
   }
}


/*************************************************** spMinsAndMaxs */
/**********

**********/
long  CZBlob::spMinsAndMaxs (BYTE *data, long pnpnt,
                    long noise, long thresh, long cycle,	// 0|1
                    long *res, long reslen) // NULL, 0 are OK
{
    long i, k, m, n, type, nmima, rl,
    	 ret = 0,
         *pres;
    bool isbig;

    if (cycle) type = 0x101;  else type = 0x01;
    if ((res) && (reslen > 0)) { pres = res;  rl = reslen; }
    else { pres = (long *)(WB);  rl = WorkBufferSize / sizeof(long); }

	nmima = MinsAndMaxs (data, pnpnt, type, pres, rl);
    if (nmima < 3) goto Fin;
    if (cycle)
    {
        if (pres[nmima - 1] > pres[0]) pres[nmima - 1] = pres[0];
        else pres[0] = pres[nmima - 1];
    } else pres[0] = pres[nmima - 1] = -1;
	m = n = 0;
	for(i = 1;  i < nmima;  i += 2)
    {
		k = 0;
        if (!m) m = i;
        else
        {
           	if (data[pres[m]] < data[pres[i]])
            	m = i;
        }
        isbig = (data[pres[m]] >= thresh);
        if (pres[n] < 0) k |= 0x11;
        else
        	if ((data[pres[m]] - data[pres[n]] >= noise) && isbig)
        		k |= 0x01;
        if (pres[i + 1] < 0) k |= 0x22;
        else
        	if ((data[pres[m]] - data[pres[i + 1]] >= noise) && isbig)
   				k |= 0x02;
        if ((k & 3) == 3)
        {	// true maximum
            pres[++n] = pres[m];
            pres[++n] = pres[i + 1];
        	ret++;      m = 0;
        } else
        { 	// false maximun
            if ((n) || ((k & 0x10) == 0))
            {
            	if (k & 0x20) pres[n] = pres[i + 1];
                else
                {
            		if (data[pres[i + 1]] < data[pres[n]])
                    {
                	 	pres[n] = pres[i + 1];   m = 0;
                        if ((!n) && (cycle))
                        	pres[nmima - 1] = pres[0];
                    }
                }
            }
        }
    }
Fin:
    return ret;
}


/******************************************************* spSortTag */
/***
	Function:	spSortTag

	Description:
		sort existing spots by increasing tag values
	Arguments:

	Returns: number of spots / 0

***/
long  CZBlob::spSortTag(char *operation)
{
	long ibeg, ifin, curind, curmin, i, curval,
    	 spots = 0;
   DWORD k, klim, c, *r, *w;

   if (FillTagValues(operation) < 0) goto Fin;
   ibeg = SpotBegIndex;  ifin = ibeg + SpotsNumber - 1;
   klim = sizeof(Spot) / 4;
   while (ibeg < ifin)
   {
      curind = ibeg;  curmin = SpotBuffer[curind].Tag;
      for (i = ibeg + 1;  i <= ifin;  i++)
    	{
     		if ((curval = SpotBuffer[i].Tag) < curmin)
         {
           	curind = i;  curmin = curval;
         }
      }
      if (curind != ibeg)
      {
    		w = (DWORD *)&SpotBuffer[curind];
        	r = (DWORD *)&SpotBuffer[ibeg];
        	for (k = 0;  k < klim;  k++)
         {
				c = w[k];  w[k] = r[k];  r[k] = c;
         }
      }
      ibeg++;
   }
   spots = SpotsNumber;
Fin:
	return spots;
}


/**************************************************** spSortTagInd */
/***
	Function:	spSortTagInd

	Description:
		fills Tag members in existing spots with numbers
        corresponding to sorted order
	Arguments:

	Returns: number of spots / 0

***/
long  CZBlob::spSortTagInd(char *operation)
{
    long ibeg, i, *r, spots = 0;

    if (FillTagValues(operation) < 0) goto Fin;
    spots = SpotsNumber;    ibeg = SpotBegIndex;
    r = (long *)WorkBuffer;
	IndexSortElems (&SpotBuffer[ibeg].Tag, sizeof(Spot), spots,	r, 4, 0);
    for (i = 0;  i < spots;  i++)
   	SpotBuffer[ibeg + i].Tag = r[i] + 1;
Fin:
	return spots;
}


/************************************************** spSpotListForm */
/***
 Using current Spot List, fills mask with thresholding
 	each Spot at it's natural level.
 Fills:
        sp->Tag = number of 1-bits in 32-oid
        sp->Tag5 = 1000 * Int(100 * (axes ratio))
        				+ Int(100 * (compactness))
 returns:
   	 number of spots

***/
long  CZBlob::spSpotListForm (long hmask)
{
    long xe, ye, spotnum, arat, comp,
    		spots = 0, hseed = 0;
	long omask = 0, oseed;
    long xrec, yrec, xl, yt, xre, ybe;
    long stat[mrStat1DataLength];
    Spot *sp;
    long memalign = 0xfffffff8; // MEMORY ALIGNMENT MASK
// RESET MEMORY ALIGNMENT MASK for INTEL IMAGE PROCESSING LIBRARY:
#define Intel_IP_LibValue 1
    if (pMR->mrUseIntelIPlib(-1) == Intel_IP_LibValue)
        memalign = 0xffffffc0;
#undef Intel_IP_LibValue

    if (pMR->mrGetRectInfo(hmask, &xe, &ye) != 1) goto Fin;
	if ((spots = spSpotsNumber()) <= 0) goto Fin;
    hseed = pMR->mrCreateAs (hmask);

	omask = pMR->mrCreateOverlay(hmask, 0, 0, xe, ye);
	oseed = pMR->mrCreateOverlay(hseed, 0, 0, xe, ye);
    for (spotnum = 1;  spotnum <= spots;  spotnum++)
	{
    	sp = SpotBuffer + spotnum;
        xrec = sp->Xrecon;  yrec = sp->Yrecon;
        if ((xl = sp->Xmin) > 0) xl = (xl - 1) & memalign;
        if ((yt = sp->Ymin) > 0) yt--;
        if ((xre = (sp->Xmax + 9) & 0xfffffff8) > xe) xre = xe;
        if ((ybe = sp->Ymax + 2) > ye) ybe = ye;
        xre -= xl;  ybe -= yt;
        pMR->mrResizeOverlay(omask, xl, yt, xre, ybe);
        pMR->mrResizeOverlay(oseed, xl, yt, xre, ybe);
        pMR->mrReconFast (omask, oseed, xrec - xl, yrec - yt);
        pMR->mrStat1 (oseed, stat);
        arat = (stat[10] + 5) / 10; // 100 * (axes ratio)
        comp = (stat[11] + 5) / 10; // 100 * (compactness)
        sp->Tag5 = 1000 * arat + comp;
        sp->Tag = pMR->mrOperMask (oseed, "32oid");
    }
Fin:
    if (omask) pMR->mrFree (omask);
    if (hseed) pMR->mrFree (hseed);
	 return spots;
}


/***************************************************** spWaterShed */
/***
 	Starts from the Spot list after spFindMarkers()
      and from 'hndl1' mask of markers (source and result).
	'borders' - calculated mask (subset) of watershed borders;
   Any of 'hndl1' and 'borders' (or both) may be set to 0;
   in this case the mask of markers is to be calculated with
   spSpotListMask() function.
   Form Factor plays if and only if the threshold is less
   than 'formthresh'
   Updates the Spot structure; sets
      Tag1:
         0 - limited by surrounding
      	1 - limited by threshold
      	2 - limited by area
      	3 - limited by 'formfactor'
      Tag2 - measure of 'formfactor';
 	returns:
   	 number of spots if OK,
      -1 otherwise

pMR->mrRectMaskData:
        	data[0] = NUMBER of one bits in mask
        	data[1] = SUM of bytes in 'mem' corresponding to one bits in mask
        	data[2] = MIN of bytes in 'mem' corresponding to one bits in mask
        	data[3] = MAX of bytes in 'mem' corresponding to one bits in mask
        	data[4] = X coordinate one of the MIN pixel(s)
        	data[5] = Y coordinate one of the MIN pixel(s)
        	data[6] = X coordinate one of the MAX pixel(s)
        	data[7] = Y coordinate one of the MAX pixel(s)
        	data[8] = standard deviation:
            			(long)(sqrt(Sum((P-Aver(P))^2)/NumOf)+0.5)
        	data[9] = number of pixels with MIN value
        	data[10] = number of pixels with MAX value

***/
long  CZBlob::spWaterShed (long hndl8, long hndl1, long borders,
			long thrmin, long areamax, long formfactor,
            long formthresh, long formtype)
{
	long firstind, nextind, nextthr, i, k,
   	 	 pix, xe, ye, spots = -1;
   	long mseed, mbord, mwrk1, mwrk2, mwater, mdebug, mthr;
   	long ibeg, spind, rep, oldmin, perim;
   	long xrec, yrec, area, psum, pmax, thresh, cut, cxmax, cymax;
   	long csum, cmax, cyl, oldcyl, curdata[16], data[16];
//############   long level, extra, armin;
   	long xmin, xmax, ymin, ymax, flags, nmax, xe1, ye1, emin, emax;
   	int nei4;
   	bool yield;

   	if ((!SpotBuffer) || (!SpotsNumber))
   	{
      	goto Fin;	// empty list
   	}
    // prepare list:
 	pix = pMR->mrGetRectInfo (hndl8, &xe, &ye);
	if (pix != 8) goto Fin;
   	if (hndl1) mseed = hndl1;
   	else
   	{
   		mseed = pMR->mrCreate(1, xe, ye);
      	spSpotListMask (hndl8, mseed);
   	}
   	mbord = pMR->mrCreateAs (mseed);
   	mwrk1 = pMR->mrCreateAs (mseed);
   	mwrk2 = pMR->mrCreateAs (mseed);
   	mwater = pMR->mrCreateAs (mseed);
   	mthr = pMR->mrCreateAs (mseed);
	mdebug = pMR->mrCreateAs (mseed);
   	nei4 = 1;
// *** find initial bord mask:
   	pMR->mrThreshold (hndl8, mthr, "Less", thrmin); // less then min threshold

   	spots = SortChain("-Thresh", &firstind); // SpotsNumber;
   	ibeg = SpotBegIndex;  // ifin = ibeg + spots - 1;

// *** main loop:
	while (firstind)
   	{
      // get info about the next
      	spind = ibeg + firstind - 1;
      	if ((nextind = SpotBuffer[spind].Tag) > 0)
      	{
	      	nextthr = SpotBuffer[ibeg + nextind - 1].Thresh;
//	      l = SpotBuffer[ibeg + nextind - 1].Tag;
      	} else nextthr = 0;
      	yield = false;
      	// handle current spot:
      	xmin = SpotBuffer[spind].Xmin;
      	xmax = SpotBuffer[spind].Xmax;
      	ymin = SpotBuffer[spind].Ymin;
      	ymax = SpotBuffer[spind].Ymax;
    	xrec = SpotBuffer[spind].Xrecon;
      	yrec = SpotBuffer[spind].Yrecon;
	 	pMR->mrReconFastD (mseed, mwrk1, xrec, yrec, data); // current seed
    	pMR->mrPixelwise (mseed, "Copy", mwrk2);
    	pMR->mrPixelwise (mwrk1, "Less", mwrk2); // seeds without current
      	pMR->mrNeiFun (mwrk2, "Dilate4", 1); // dilate it
    	pMR->mrPixelwise (mbord, "Or", mwrk2); // prohibited pixels mask
    	pMR->mrPixelwise (mthr, "Or", mwrk2); // prohibited pixels mask
    	pMR->mrPixelwise (mwrk1, "Copy", mwater); // current seed
      	pMR->mrWaterMask (hndl8, mwrk2, mwater);
    	pMR->mrPixelwise (mwater, "Copy", mwrk2); // current watershed mask
		pMR->mrPixelwise (mwater, "Or", mdebug); //

      	pMR->mrRectMaskData (hndl8, mwrk1, curdata);
      	area = curdata[0];  flags = oldcyl = 0;
      	psum = curdata[1];
   		oldmin = thresh = curdata[2];
//###################      extra = 8;
      	pmax = curdata[3];
      	if (area == 0) goto FillSpotData;
		oldcyl = (100 * psum) / (area * pmax);
/*###################
		if ((armin = area / 4) < 5) armin = 5;
      level = thresh - extra;
      csum = psum - level * area + (pmax - level) * armin;
      oldcyl = (100 * csum)
      			/ ((pmax - level) * (area + armin));
###################*/
      	if (thresh < thrmin) { flags = 1;  goto FillSpotData; }
      	if (area >= areamax) { flags = 2;  goto FillSpotData; }
	// *** spot expansion loop:
ExpandLoop:
		perim = pMR->mrRecOutCont (mseed, mwrk2, xrec, yrec, nei4);
    	pMR->mrPixelwise (mwater, "And", mwrk2);
      	rep = 0;
CheckPix:
      	if (!(pMR->mrRectMaskData (hndl8, mwrk2, data)))
      	{
         	if (rep == 1) goto ExpandLoop; // it was the last pixel
         	if (rep == 2)
         	{  // last pixel was rejected because of formfactor
          		flags = 3;  goto FillSpotData;
         	}
            pMR->mrRecOutCont (mseed, mwrk2, xrec, yrec, nei4);
         	pMR->mrPixelwise (mthr, "And", mwrk2);
	      	if (pMR->mrRectMaskData (hndl8, mwrk2, data))
         	{
            	flags = 1; // threshold!!
         	}
         	goto FillSpotData; // spot is surrounded
      	}
      	cmax = data[3];   nmax = data[10];
      	cxmax = data[6];  cymax = data[7];
      	xe1 = xmax + 1 - xmin;  ye1 = ymax + 1 - ymin;
      	if ((xmin > cxmax) || (xmax < cxmax)) xe1++;
      	if ((ymin > cymax) || (ymax < cymax)) ye1++;

      	switch (formtype)
      	{
       		case 1:
      			if (xe1 < ye1) { emin = xe1;  emax = ye1; }
		      	else { emin = ye1;  emax = xe1; }
         		break;
       		case 2:
         		emin = 2 * (xe1 + ye1);  emax = perim + 4;
         		break;
       		case 3:
         		emin = 16 * area * area + 100;
            	emax = perim * perim * xe1 * ye1 + 100;
         		break;
         	default: emin = emax = 1;
      	}
      	if (cmax < oldmin) cut = cmax;   else cut = oldmin;
		csum = psum + cmax;
		cyl = (100 * csum) / (area * pmax + pmax);
/*########################
      level = cut - extra;
      csum = psum + cmax - level * (area + 1) + (pmax - level) * armin;
      cyl = (100 * csum) / ((pmax - level) * (area + armin + 1));
########################*/
      	cyl = cyl * emin / emax;
		if ((formfactor > 0) && (cut < formthresh) && (cyl < formfactor))
      	{
         	if (nmax > 1)
      		{
        		pMR->mrPutPixel (mwrk2, cxmax, cymax, 0);
         		rep = 2;  goto CheckPix;
         	} else
         	{
          		flags = 3;  goto FillSpotData;
         	}
      	}
      	pMR->mrPutPixel (mseed, cxmax, cymax, 1);
      	area++;  psum += cmax;
      	oldcyl = cyl; oldmin = cut;
      	if (area >= areamax) { flags = 2;  goto FillSpotData; }
      	if (xmin > cxmax) xmin = cxmax;
      	if (xmax < cxmax) xmax = cxmax;
      	if (ymin > cymax) ymin = cymax;
      	if (ymax < cymax) ymax = cymax;
      	if ((nextthr) && (oldmin <= nextthr))
      	{ // yield turn:
      		yield = true;  goto FillSpotData;
      	}
      	if (nmax > 1)
      	{
        	pMR->mrPutPixel (mwrk2, cxmax, cymax, 0);
         	rep = 1;  goto CheckPix;
      	}
      	goto ExpandLoop;
FillSpotData:
		SpotBuffer[spind].PixNum = area;
      	SpotBuffer[spind].PixSum = psum;
      	SpotBuffer[spind].Thresh = oldmin;
      	SpotBuffer[spind].Tag1 = flags;
      	SpotBuffer[spind].Tag2 = oldcyl;
      	SpotBuffer[spind].Xmin = xmin;
      	SpotBuffer[spind].Xmax = xmax;
      	SpotBuffer[spind].Ymin = ymin;
      	SpotBuffer[spind].Ymax = ymax;
      	if ((yield) && (nextind))
      	{
         	k = nextind;
FindPlace:
         	i = SpotBuffer[ibeg + k - 1].Tag;
         	if (i)
         	{
            	if (oldmin <= SpotBuffer[ibeg + i - 1].Thresh)
            	{
         			k = i;  goto FindPlace;
				}
         	}
         	// replace first spot inside the chain:
         	SpotBuffer[spind].Tag = i;
         	SpotBuffer[ibeg + k - 1].Tag = firstind;
      	}
      	firstind = nextind;
	}
   	if (mseed != hndl1) pMR->mrFree (mseed);
   	if (borders) pMR->mrMove (mdebug, borders);
   	pMR->mrFree (mwrk1);
   	pMR->mrFree (mwrk2);
   	pMR->mrFree (mwater);
   	pMR->mrFree (mthr);
	pMR->mrFree (mdebug);
   	pMR->mrFree (mbord);
Fin:
	return spots;
}


long  CZBlob::spMaskShapesInfoNew (long hmask, long h32, long minarea, long sortByThisCol)
{
    long i, k, lastx, lasty, hseed, hseed1, hlimit, ones, onesmin,
         operation, elsize, sortoffset, portions, xc, yc,
         xc1, yc1, imin, imax,
         compnum = -1;
    long sizedata[13], 
		 stat1data[mrStat1DataLength],
    	 calipers[16],
         onesoffset = 0,
         xcycoffset = 1,
         minmaxoffset = 3,
         axratoffset = 7,
         cmpoffset = 8,
         calminoffset = 9,
         calmaxoffset = 10,
         radmaxoffset = 11,
         roundoffset = 12;
    long xe32, maxcomp, pitch32, *mem32, xemin = 13, datasize;
    BYTE *data;
    double distmin = 5.0, radaver, radmin, radmax;

    lastx = lasty = hseed = hseed1 = hlimit = 0;
	datasize = xemin * sizeof(long);
    if ((hseed = pMR->mrCreateAs (hmask)) < 1) goto Fin;
    if ((hseed1 = pMR->mrCreateAs (hmask)) < 1) goto Fin;
    if ((hlimit = pMR->mrCopyAs (hmask)) < 1) goto Fin;
	if (pMR->mrGetExtendedInfo (h32, &xe32, &maxcomp, &pitch32, &data) != 32)
		goto Fin;
    if (xe32 < xemin) goto Fin;
    // fill array of coordinates of components:
    compnum = i = portions = 0;
    while (pMR->mrFindNext (hlimit, &lastx, &lasty, 1) > 0)
	{
        ones = pMR->mrReconFastD (hlimit, hseed, lastx, lasty,
        					 sizedata + minmaxoffset);
        if (ones < minarea)
        {
	        pMR->mrPixelwise (hseed, "Less", hlimit);
    	    continue;
        }
        // set xc, yc to the center of the rectangle:
        // (xmin + xmax)/2, (ymin + ymax)/2:
        xc = (sizedata[minmaxoffset + 0] + sizedata[minmaxoffset + 2]) / 2;
        yc = (sizedata[minmaxoffset + 1] + sizedata[minmaxoffset + 3]) / 2;
        if (pMR->mrGetPixel (hseed, xc, yc) == 0) pMR->mrDistToMask (hseed, &xc, &yc);
        sizedata[xcycoffset] = xc;
        sizedata[xcycoffset + 1] = yc;
        sizedata[onesoffset] = ones;
		pMR->mrStat1 (hseed, stat1data);
//   Data[10]:= 1000 * (shortest main axis / longest main axis)
//   Data[11]:= 1000 * (measure of compactness)
		sizedata[axratoffset] = stat1data[10];
		sizedata[cmpoffset] = stat1data[11];
		pMR->mrMaskCalipers (hseed, calipers, &imin, &imax, &xc1, &yc1);
		sizedata[calminoffset] = calipers[imin];
		sizedata[calmaxoffset] = calipers[imax];

		pMR->mrRecOutCont (hlimit, hseed1, lastx, lasty, 1);
		spArcCenter (hseed1, distmin, &xc, &yc, &radaver, &radmin, &radmax);
		sizedata[radmaxoffset] = (long)(radmax + 0.5);
		sizedata[roundoffset] = (long)(1000 * radmin / radmax);

        compnum++;
    	if (compnum <= maxcomp)
        {
            i = portions++;
        } else
        {
         	i = -1;  onesmin = ones;
            for (k = 0;  k < maxcomp;  k++)
            {
            	mem32 = (long *)(data + (pitch32 * k));
             	if (mem32[onesoffset] < onesmin)
                {
                    i = k;
                 	onesmin = mem32[onesoffset];
                }
            }
        }
        if (i >= 0)
        {
            MovMemory(sizedata, data + (pitch32 * i), datasize);
        }
        pMR->mrPixelwise(hseed, "Less", hlimit);
    }
    if (portions > 1)
    {	// is sortable
	    elsize = pitch32;
//sortByThisCol = onesoffset;
        sortoffset = sortByThisCol * sizeof(long);
        operation = (sortoffset << 8) | 0x84;   // 0x<00>84;
        SortInPlace (data, elsize, portions, operation);
    }
Fin:
    if (hseed > 0)  pMR->mrFree(hseed);
    if (hseed1 > 0) pMR->mrFree(hseed1);
    if (hlimit > 0) pMR->mrFree(hlimit);
	return compnum;
}


/************************************************ spRemoveSpots */
/***
    This function removes the spots based on the input criterion.

    Returns:
   	 number of linked components removed
         or 0

***/
long  CZBlob::spRemoveSpots(long hmask, long h32,
         long numComp, long thCol, long thValue, int fillLevel, long hseed)
{
    long n, par;
    long xc, yc, numSpotsRemoved;
    long xe32, maxcomp, pitch32, nByPitch32, *pShape;
    BYTE *data;

    if (pMR->mrGetExtendedInfo(h32, &xe32, &maxcomp, &pitch32, &data) != 32)
      goto Fin;

    numSpotsRemoved = 0;
    pShape = (long*)data;
    for (n=0; n<numComp; n++)
    {
        nByPitch32 = n * xe32;
        par = pShape[nByPitch32 + thCol];
        if (par > thValue)
        {
          numSpotsRemoved ++;
          xc = pShape[nByPitch32 + 1];
          yc = pShape[nByPitch32 + 2];
          pMR->mrPutPixel(hseed, xc, yc, fillLevel);
          pMR->mrReconstruct (hmask, hseed);
        }
    }

Fin:
    return numSpotsRemoved;

}











