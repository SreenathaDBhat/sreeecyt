
#include "stdafx.h"

#include <malloc.h>
#include <math.h>
#include <stdio.h>
#include <errno.h>

#include "MemRiipl.h"
#include "zGeneric.h"
#include "zImProc.h"
#include "Intelc5.h"

//#include "IntelIpl.h"

#ifdef  PentiumIIwithMMX
#include "asmoptim.h"
#include "Constnts.h"
#endif

//****************************  statics:

static long BackgroundLibraryFlag = AIC_ImProc_Lib; // KS TEMP Intel_IP_Lib;  // use by default

/*********************************************** iiplUseIntelIPlib */
/**************** command:
 -1 - return current Background Library Flag,
  set new <BackgroundLibraries> value otherwise:

  SmartChoiceLib, Intel_IP_Lib, AIC_ImProc_Lib, TEEM_MMX_Lib

****************/


#if 0

 void _stdcall iC5Convol2 (BYTE *mem,	long xe, long ye,	long pitch,
	 long xf, long yf, long xc, long yc, long *coeffs, long rshift){};

 void _stdcall iC5ConvolSep2 (BYTE *mem, long xe, long ye,	long pitch,
               long xn, long xc, long *xcoeffs, long xrshift,
			   long yn, long yc, long *ycoeffs, long yrshift){};

 void _stdcall iC5Filter (BYTE *mem,	long xe, long ye,	long pitch,
	 long xf, long yf, long xc, long yc, long oper){};

 void _stdcall iC5MaskNei (BYTE *mem, DWORD xe, DWORD ye,
	 DWORD pitch, long oper, long rep){};

 void _stdcall iC5MemOpMem (BYTE *mem1, BYTE *mem2, long size, WORD oper){};

 void _stdcall iC5MaskBitwise (BYTE *mem1,
				DWORD xe, DWORD ye, DWORD pitch1,
            BYTE *mem2, DWORD pitch2,
			short oper){};

 void _stdcall iC5MemConstMem (
					BYTE *mem1, BYTE *mem2, long size,
					long cnst, short oper){};

 void _stdcall iC5NeiMem (BYTE *mem,	long xe, long ye,	long pitch,
	 long oper, long rep){};


#endif





long iiplUseIntelIPlib(long command)
{
   long oldval;

   oldval = BackgroundLibraryFlag;
   switch (command)
   {
   		case SmartChoiceLib:	// 0
   			BackgroundLibraryFlag = SmartChoiceLib;	break;
	   	case Intel_IP_Lib:		// 1
   			BackgroundLibraryFlag = Intel_IP_Lib;   break;
	   	case AIC_ImProc_Lib:	// 2
   			BackgroundLibraryFlag = AIC_ImProc_Lib; break;
	   	case TEEM_MMX_Lib:		// 3
#ifdef  PentiumIIwithMMX
			if (CPU_MMX() & 1)
        	{	// MMX is allowed and presents
         		BackgroundLibraryFlag = TEEM_MMX_Lib;
	        }
#endif // PentiumIIwithMMX
   			break;
      	default: ;
	}
	return oldval;
}

/****************************************************** PreferTeem */

long PreferTeem()
{
    if (BackgroundLibraryFlag == SmartChoiceLib)
    {
#ifdef  PentiumIIwithMMX
		if (CPU_MMX() & 1)
        {	// MMX is allowed and presents
        	return TEEM_MMX_Lib;
	    }
#endif // PentiumIIwithMMX
		return Intel_IP_Lib;
    }
	return BackgroundLibraryFlag;
}

/***************************************************** PreferIntel */

long PreferIntel()
{
    if (BackgroundLibraryFlag == SmartChoiceLib)
    {
		return Intel_IP_Lib;
    }
	return BackgroundLibraryFlag;
}

/**************************************************** iiplMemOpMem */

short iiplMemOpMem (BYTE *mem1, BYTE *mem2, long size, WORD oper)
{
	long flag;

    flag = PreferTeem();
	if (flag == Intel_IP_Lib)
   	{
	 	iC5MemOpMem (mem1, mem2, size, oper);
    	return 0;
	}

#ifdef  PentiumIIwithMMX
   	if (flag == TEEM_MMX_Lib)
   	{
if (((oper&63) != momAbsDiff) && ((oper&63) != momAbsDiff8)) // wait for tm...
      	return (short)tmMMM (mem1, mem2, mem2, size, 1,
         				size, size, size, oper);
   	}
#endif // PentiumIIwithMMX

   	return MemOpMem (mem1, mem2, size, oper);
}

/************************************************* iiplMemConstMem */
/***
***/
void iiplMemConstMem (BYTE *mem1, BYTE *mem2, long size,
			 			WORD cnst, short oper)
{
	long flag;

    flag = PreferTeem();
	if (flag == Intel_IP_Lib)
   	{
	 	iC5MemConstMem (mem1, mem2, size, cnst, oper);
      	return;
	}

#ifdef  PentiumIIwithMMX
   	if (flag == TEEM_MMX_Lib)
   	{
if (((oper&63) != momAbsDiff) && ((oper&63) != momAbsDiff8)) // wait for tm...
      	tmMCMb (mem1, cnst, mem2, size, 1, size, size, oper);
      	return;
   	}
#endif // PentiumIIwithMMX

  	MemConstMem (mem1, mem2, size, cnst, oper);
}

/************************************************ iiplMaskBitwiseR */
/***
	xe in bits!
	return value is ok
***/
long iiplMaskBitwiseR (void *mem1, DWORD xe, DWORD ye, DWORD pitch1,
            void *mem2, DWORD pitch2, short oper)
{
	long flag,
    	 ret = 2,   // 'ones and zeros' return value
   	  	 data[6] = {0, 0, 1, 0, 0, 0};

    flag = PreferTeem();
	if ((flag == Intel_IP_Lib) &&
   	  	(((xe + 7) >> 3) == pitch1) && (pitch1 == pitch2))
   	{
	 	iC5MaskBitwise ((BYTE *)mem1, xe, ye, pitch1, (BYTE *)mem2, pitch2, oper);
		if (!MaskOper ((BYTE *)mem2, xe, ye, pitch2, moFIND, data))
      	{
    	  	ret = 0;  goto Fin;  // no ones
      	}
    	if (((data[0] | data[1]) != 0) || ((*(DWORD *)mem2) != 0xffffffff))
    	  	goto Fin;  // there are ones and zeros
	 	if (!MaskOper ((BYTE *)mem2, xe, ye, pitch2, moFIND, data + 3))
    	  	ret = 1;  // no zeros
      	goto Fin;
	}
#ifdef  PentiumIIwithMMX
   	if ((flag == TEEM_MMX_Lib) && ((xe & 7) == 0))
   	{
if (((oper&63) != momAbsDiff) && ((oper&63) != momAbsDiff8)) // wait for tm...
{      	tmMMM (mem1, mem2, mem2, xe >> 3, ye, pitch1, pitch2, pitch2, oper);
		if (!tmFindBit (mem2, xe, ye, pitch2, data, data + 1, 1))
      	{
    	  	ret = 0;  goto Fin;  // no ones
      	}
    	if (data[0] | data[1]) goto Fin;  // there are ones and zeros
		if (!tmFindBit (mem2, xe, ye, pitch2, data, data + 1, 0))
    	  	ret = 1;  // no zeros
        goto Fin;
}
   	}
#endif // PentiumIIwithMMX
 	ret = MaskBitwise (mem1, xe, ye, pitch1, mem2, pitch2, oper);
Fin:
 	return ret;
}

/****************************************************** iiplFilter */
/***
***/
long iiplFilter (BYTE *mem,
					long xe, long ye,	long pitch,
               long xf, long yf, long xc, long yc,
               long oper)
{
	iC5Filter (mem, xe, ye, pitch, xf, yf, xc, yc, oper);
 	return 0;
}

/****************************************************** iiplConv2D */
/***
***/
long iiplConv2D (BYTE *mem,
					long xe, long ye,	long pitch,
               long xf, long yf, long xc, long yc,
               long *coeffs, long rshift)
{
    iC5Convol2 (mem, xe, ye, pitch, xf, yf, xc, yc,
               coeffs, rshift);
 	return 0;
}

/*************************************************** iiplConvSep2D */
/***
***/
long iiplConvSep2D (BYTE *mem,
					long xe, long ye,	long pitch,
               long xn, long xc, long *xcoeffs, long xrshift,
               long yn, long yc, long *ycoeffs, long yrshift)
{
 	iC5ConvolSep2 (mem, xe, ye, pitch,
    					xn, xc, xcoeffs, xrshift,
               	yn, yc, ycoeffs, yrshift);
 	return 0;
}

/***************************************************** iiplMaskNei */
/***

  	mnm124=12, mnmMED=13, mnmBLUR=14,
   	mnmMIN=0, mnmMAX=1, mnmMIN4=20, mnmMAX4=21,
  	mnmMINH2=22, mnmMAXH2=23, mnmMINV2=24, mnmMAXV2=25,
	mnmOPEN8=26, mnmCLOSE8=27,	mnmOPEN4=28, mnmCLOSE4=29
#define tmDILATEH 0x10 // max of self and 2 horizontal neighbors;
#define tmERODEH 	0x11 // min of self and 2 horizontal neighbors;
#define tmDILATEV	0x20 // max of self and 2 bit vertical neighbors;
#define tmERODEV 	0x21 // min of self and 2 bit vertical neighbors;
#define tmDILATE4	0x40 // max of self and 4 neighbors;
#define tmERODE4 	0x41 // min of self and 4 neighbors;
#define tmCLOSE4	0x42 // same as tmDILATE4 followed by tmERODE4;
#define tmOPEN4 	0x43 // same as tmERODE4 followed by tmDILATE4;
#define tmDILATE8	0x80 // max of self and 8 neighbors;
#define tmERODE8 	0x81 // min of self and 8 neighbors;
#define tmCLOSE8	0x82 // same as tmDILATE8 followed by tmERODE8;
#define tmOPEN8 	0x83 // same as tmERODE8 followed by tmDILATE8;
***/
#ifdef  PentiumIIwithMMX
static BYTE IprToTeem[] =
{
	tmERODE8, tmDILATE8, 			 			// 0, 1
	0, 0, 0,  0, 0, 0, 0, 0, 					// 2 - 9
	0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 				// 10 - 19
   	tmERODE4, tmDILATE4,           				// 20, 21
   	tmERODEH, tmDILATEH, tmERODEV, tmDILATEV, 	// 22 - 25
   	tmOPEN8,  tmCLOSE8,  tmOPEN4,  tmCLOSE4   	// 26 - 29
};
#endif // PentiumIIwithMMX
void iiplMaskNei (BYTE *mem, DWORD xe, DWORD ye,
				DWORD pitch, long oper, long rep)
{
#ifdef  PentiumIIwithMMX
   	DWORD tmoper;
   	if (PreferTeem() == TEEM_MMX_Lib)
   	{
      	if ((tmoper = IprToTeem[oper]) != 0)
      		if (tmBitNeiInPlace (mem, xe, ye, pitch, tmoper, rep))
         		return;
   	}
#endif // PentiumIIwithMMX
   	MaskNei (mem, xe, ye, pitch, oper, rep);
}
/**********************************
- needs inverted bit order
- is very slow
   if (BackgroundLibraryFlag == 1)
   {
      iC5MaskNei (mem, xe, ye, pitch, oper, rep);
    	return;
   }
**********************************/

/****************************************************** iiplNeiMem */
/***
  	mnm124, mnmMED, mnmBLUR,
   	mnmMIN, mnmMAX, mnmMIN4, mnmMAX4,
  	mnmMINH2, mnmMAXH2, mnmMINV2, mnmMAXV2,
	mnmOPEN8, mnmCLOSE8,	mnmOPEN4, mnmCLOSE4
***/
void iiplNeiMem (BYTE *mem,
				long xe, long ye, long pitch,
               	long oper, long rep)
{
#ifdef  PentiumIIwithMMX
	long tmop, pitch2;
   	BYTE *mem2, *p2;
#endif // PentiumIIwithMMX
	long flag;

   	flag = PreferTeem();
#ifdef  PentiumIIwithMMX
   	if (flag == TEEM_MMX_Lib)
   	{
      	if (oper == mnmMIN)
      	{
   			tmop = 1;
DoLoop8:
			tmByteNeiH2 (mem, mem, xe, ye, pitch, pitch, tmop, rep);
         	tmByteNeiV2 (mem, mem, xe, ye, pitch, pitch, tmop, rep);
      		return;
      	}
      	if (oper == mnmMAX) { tmop = 0;  goto DoLoop8; }
      	if (oper == mnmMIN4)
      	{
   			tmop = 3;
DoLoop4:
         	pitch2 = (xe + 7) & 0xfffffff8;
         	mem2 = p2 = (BYTE *)malloc(pitch2 * ye + 16);
         	if (((DWORD)mem2) & 4) mem2 += 4;
         	if (rep & 1)
         		tmMoveByteRect (mem, mem2, xe, ye,	pitch, pitch2, 0);
            while (rep--)
         	{
         		if (rep & 1)
            	{
					tmByteNeiH2 (mem, mem2, xe, ye, pitch, pitch2, tmop, 1);
					tmByteNeiV2 (mem, mem2, xe, ye, pitch, pitch2, tmop, 1);
            	} else
            	{
					tmByteNeiH2 (mem2, mem, xe, ye, pitch2, pitch, tmop, 1);
					tmByteNeiV2 (mem2, mem, xe, ye, pitch2, pitch, tmop, 1);
            	}
         	}
         	free(p2);
      		return;
      	}
      	if (oper == mnmMAX4) { tmop = 2;  goto DoLoop4; }

      	if (oper == mnmMINH2)
      	{
   			tmop = 1;
DoLoopH:
         	tmByteNeiH2 (mem, mem, xe, ye, pitch, pitch, tmop, rep);
      		return;
      	}
      	if (oper == mnmMAXH2) { tmop = 0;  goto DoLoopH; }
      	if (oper == mnmMINV2)
      	{
   			tmop = 1;
DoLoopV:
         	tmByteNeiV2 (mem, mem, xe, ye, pitch, pitch, tmop, rep);
      		return;
      	}
      	if (oper == mnmMAXV2) { tmop = 0;  goto DoLoopV; }
      	if (oper == mnmOPEN8)
      	{
			iiplNeiMem (mem, xe, ye, pitch, mnmMIN, rep);
			iiplNeiMem (mem, xe, ye, pitch, mnmMAX, rep);
      		return;
      	}
      	if (oper == mnmCLOSE8)
      	{
			iiplNeiMem (mem, xe, ye, pitch, mnmMAX, rep);
			iiplNeiMem (mem, xe, ye, pitch, mnmMIN, rep);
      		return;
      	}
      	if (oper == mnmOPEN4)
      	{
			iiplNeiMem (mem, xe, ye, pitch, mnmMIN4, rep);
			iiplNeiMem (mem, xe, ye, pitch, mnmMAX4, rep);
      		return;
      	}
      	if (oper == mnmCLOSE4)
      	{
			iiplNeiMem (mem, xe, ye, pitch, mnmMAX4, rep);
			iiplNeiMem (mem, xe, ye, pitch, mnmMIN4, rep);
      		return;
      	}
   	}
#endif // PentiumIIwithMMX

	if (flag == Intel_IP_Lib)
   	{
	 	iC5NeiMem (mem, xe, ye, pitch, oper, rep);
      	return;
	}
   	NeiMem (mem, xe, ye, pitch, oper, rep);
}

/************************************************ iiplMemToBitMask */
/***
 Opcase = {1 - 6}: {NotLess,NotGreater,UnEqual,Equal,Less,Greater}
***/
static WORD ThreshConsts[] =
	{mtbmGE, mtbmBE, mtbmEQ, mtbmNE, mtbmBE, mtbmGE};
#ifdef  PentiumIIwithMMX
static DWORD tmThreshOps[] =
	{tmNLESS, tmNMORE, tmNEQU, tmEQU, tmLESS, tmMORE};
#endif // PentiumIIwithMMX
long iiplMemToBitMask (
				void *mem, DWORD mempix,
                DWORD xe, DWORD ye, DWORD pitch,
                void *memmask, DWORD pitchmask,
                DWORD threshold, long opcase)
{
	long ret;
   	DWORD oper;

#ifdef  PentiumIIwithMMX
if (mempix == 8) // @@@@@@@@@@@@@@@@@@ wait for tmThreshMaskD()
   	if (PreferTeem() == TEEM_MMX_Lib)
   	{
	   	oper = tmThreshOps [opcase - 1];
      	tmThreshMaskB (mem, threshold, memmask, xe, ye,
         				pitch, pitchmask, oper);
      	// ret = Count ones in mask?
		ret = tmMaskOnes (mem, 0, 0, xe, ye, pitchmask);
   		return ret;
   	}
#endif // PentiumIIwithMMX

   	oper = ThreshConsts [opcase - 1];
    if (mempix == 8)
    {
        threshold &= 0xff;
   		if (opcase == 5)
		{  // Less
    		if (!threshold--)
      		{
Zero:
    			MaskOper ((BYTE *)memmask, xe, ye, pitchmask, moZERO, NULL);
            	ret = 0;  goto Fin;
			}
   		}
   		if (opcase == 6)
   		{  // "Greater"
     		if (threshold++ == 255) goto Zero;
   		}
		ret = MemToBitMask ((BYTE *)mem, xe, ye, pitch,
    			(BYTE *)memmask, pitchmask, threshold, oper);
        goto Fin;
	}
    if (mempix == 32)
    {
   		if (opcase == 5)
		{  // Less
    		if (!threshold--) goto Zero;
   		}
   		if (opcase == 6)
   		{  // "Greater"
     		if (++threshold == 0) goto Zero;
   		}
		ret = MemToBitMask32 (mem, xe, ye, pitch,
    			memmask, pitchmask, threshold, oper);
	}
Fin:
	return ret;
}

/********************************************* iiplMemMaskConstMem */
/***********
DWORD _stdcall MemMaskConstMem (
			BYTE *mem1, BYTE *mem2, BYTE *mask,
			DWORD xe, DWORD ye,
            DWORD pitch1, DWORD pitch2, DWORD pitchmask,
            WORD cnst, short oper)
DWORD _stdcall MemMaskConstMem32 (
			void *mem1, void *mem2, void *mask,
			DWORD xe, DWORD ye,
            DWORD pitch1, DWORD pitch2, DWORD pitchmask,
            DWORD cnst, DWORD oper)
Traded(long) tmMCmaskMb (
			void *ps, long cnst, void *pd, void *pm,
         long xe, long ye,
         long pitchs, long pitchd, long pitchm,
         long oper, long notmask);
one of the following modifiers can be ORed with the <oper> code:
			mmcmNEG    =0x80   (oper | mmcmNEG) means "use negative mask"
			mmcmIGNORE =0x40   (oper | mmcmIGNORE) means "ignore mask";
***********/
DWORD iiplMemMaskConstMem (
			void *mem1, void *mem2, void *mask,
			DWORD mempix, DWORD xe, DWORD ye,
         	DWORD pitch1, DWORD pitch2, DWORD pitchmask,
         	DWORD cnst, DWORD oper)
{
#ifdef  PentiumIIwithMMX
	long tmoper, notmask = 0, ret = 3;

if (mempix == 8) // @@@@@@@@@@@@@@@@@@ wait for tmMCmaskMd()
if (((oper&63) != momAbsDiff) && ((oper&63) != momAbsDiff8)) // wait for tm...
   	if (PreferTeem() == TEEM_MMX_Lib)
   	{
      	tmoper = (long)(oper ^ (oper & mmcmNEG));
      	if (tmoper & mmcmIGNORE)
      	{
      		tmoper ^= mmcmIGNORE;
			tmMCMb (mem1, cnst, mem2, xe, ye, pitch1, pitch2, tmoper);
      	} else
      	{
			if (oper & mmcmNEG) notmask = 1; // tmoper is OK
      		tmMCmaskMb (mem1, cnst, mem2, mask, xe, ye,
              		pitch1, pitch2, pitchmask, tmoper, notmask);
      	}
      	return ret;
   	}
#endif // PentiumIIwithMMX
	if (mempix == 8)
  		MemMaskConstMem ((BYTE *)mem1, (BYTE *)mem2, (BYTE *)mask, xe, ye,
              	pitch1, pitch2, pitchmask, (WORD)cnst, (short)oper);
	if (mempix == 32)
  		MemMaskConstMem32 (mem1, mem2, mask, xe, ye,
              	pitch1, pitch2, pitchmask, cnst, oper);
   	return 0;
}

/************************************************** iiplMemMaskMem */
/***********
DWORD _stdcall MemMaskMem (
				BYTE *mem1, BYTE *mem2, BYTE *mask,
				DWORD xe, DWORD ye,
            	DWORD pitch1, DWORD pitch2, DWORD pitchmask,
            	short oper)
Traded(long) tmMMmaskMb (
			void *ps, void *ps2, void *pd, void *pm,
         long xe, long ye,
         long pitchs, long pitchs2, long pitchd, long pitchm,
         long oper, long notmask);
one of the following modifiers can be ORed with the <oper> code:
			mmcmNEG    =0x80   (oper | mmcmNEG) means "use negative mask"
			mmcmIGNORE =0x40   (oper | mmcmIGNORE) means "ignore mask";
***********/
DWORD iiplMemMaskMem (
				BYTE *mem1, BYTE *mem2, BYTE *mask,
				DWORD mempix, DWORD xe, DWORD ye,
            	DWORD pitch1, DWORD pitch2, DWORD pitchmask,
            	DWORD oper)
{
#ifdef  PentiumIIwithMMX
	long tmoper, notmask = 0, ret = 3;

if (mempix == 8) // @@@@@@@@@@@@@@@@@@ wait for tmMMmaskMd()
if (((oper&63) != momAbsDiff) && ((oper&63) != momAbsDiff8)) // wait for tm...
   	if (PreferTeem() == TEEM_MMX_Lib)
   	{
      	tmoper = (long)(oper ^ (oper & mmcmNEG));
      	if (tmoper & mmcmIGNORE)
      	{
      		tmoper ^= mmcmIGNORE;
			tmMMM (mem1, mem2, mem2, xe, ye, pitch1, pitch2, pitch2, tmoper);
      	} else
      	{
			if (oper & mmcmNEG) notmask = 1; // tmoper is OK
      		tmMMmaskMb (mem1, mem2, mem2, mask, xe, ye,
              	pitch1, pitch2, pitch2, pitchmask, tmoper, notmask);
      	}
      	return ret;
   	}
#endif // PentiumIIwithMMX
	if (mempix == 8)
		MemMaskMem (mem1, mem2, mask, xe, ye,
              	pitch1, pitch2, pitchmask, oper);
	if (mempix == 32)
		MemMaskMemMem32 (mem1, mem2, mem2, mask, xe, ye,
              	pitch1, pitch2, pitch2, pitchmask, oper);
   	return 0;
}

/************************************************* iiplInverseBits */
/***
***/
void iiplInverseBits (BYTE *mem1, DWORD xe, DWORD ye, DWORD pitch1,
            BYTE *mem2, DWORD pitch2)
{

#ifdef  PentiumIIwithMMX
   	if (PreferTeem() == TEEM_MMX_Lib)
   	{
		tmReverseBits (mem1, mem2, xe, ye, pitch1, pitch2);
      	return;
   	}
#endif // PentiumIIwithMMX
	if ((xe == pitch1) && (xe == pitch2))
   	{
    	xe *= ye;  ye = 1;
   	}
   	while (ye--)
   	{
   		if (mem1 != mem2) MovMemory(mem1, mem2, xe);
			InverseBytes ((char *)mem2, xe);
      	mem1 += pitch1;  mem2 += pitch2;
   	}
}

/************************************************** iiplBitsDiffer */
/***
	xe in bits!!!
***/
long iiplBitsDiffer (BYTE *mem1, DWORD xe, DWORD ye, DWORD pitch1,
            BYTE *mem2, DWORD pitch2)
{

#ifdef  PentiumIIwithMMX
   	if (PreferTeem() == TEEM_MMX_Lib)
   	{
		return tmDiffer (mem1, mem2, xe, ye, pitch1, pitch2);
   	}
#endif // PentiumIIwithMMX

	return BitsDiffer (mem1, mem2, xe, ye, pitch1, pitch2);
}

/*************************************************** iiplMask3bord */
/***
	xe in bits!!!
***/
long iiplMask3bord (BYTE *mem1, DWORD xe, DWORD ye, DWORD pitch1,
            	BYTE *mem2, DWORD pitch2, BYTE *mem3, DWORD pitch3)
{

#ifdef  PentiumIIwithMMX
   	if (PreferTeem() == TEEM_MMX_Lib)
   	{
		return tmMask3bord (mem1, mem2, mem3, xe, ye,
      					 pitch1, pitch2, pitch3);
   	}
#endif // PentiumIIwithMMX

	return Mask3bord (mem1, xe, ye, pitch1,
         		mem2, pitch2, mem3, pitch3);
}

/************************************************** iiplFindInMask */
/***
	xe in bits!!!
***/
long iiplFindInMask (BYTE *mem, DWORD xe, DWORD ye,
							DWORD pitch, long *Data)
{

#ifdef  PentiumIIwithMMX
   	if (PreferTeem() == TEEM_MMX_Lib)
   	{
		return tmFindBit (mem, xe, ye, pitch,
    				Data, Data + 1, Data[2] & 1);
   	}
#endif // PentiumIIwithMMX

	return MaskOper (mem, xe, ye, pitch, moFIND, Data);
}

/**************************************************** iiplMaskOnes */
/***
	Returns number of 1-bits in given 1 bit per pixel subrectangle
***/
long iiplMaskOnes (BYTE *mem, DWORD xl, DWORD yt,
                    DWORD xe, DWORD ye, DWORD pitch)
{
	if (((long)xe < 0) || ((long)ye < 0)) return -1;
	if ((!xe) || (!ye)) return 0;  

#ifdef  PentiumIIwithMMX
   	if (PreferTeem() == TEEM_MMX_Lib)
   	{
		return tmMaskOnes (mem, xl, yt, xe, ye, pitch);
   	}
#endif // PentiumIIwithMMX

	return MaskOnes (mem, xl, yt, xe, ye, pitch);
}

/****************************************************** Convol3by3 */
/***
	Convolution with matrix 3*3
    updates min i max pix values
***/
void Convol3by3 (BYTE *mem8, long xe, long ye, long pitch8,
				long *mem32, long pitch32, long *coeffs,
                long *pmin, long *pmax)
{
    long ix, iy, pval, ixp, ixn, minval, maxval, *res32;
    BYTE *bPrev, *bCurr, *bNext, *wrk;

    bPrev = bCurr = mem8;  bNext = mem8 + pitch8;
    iy = 0;  wrk = (BYTE *)mem32;
    minval = 0x7fffffff;  maxval = 0x80000000;
    while (iy < ye)
    {
        res32 = (long *)wrk;
        ixp = ix = 0;  ixn = 1;
        while (ixp < ixn)
        {
        	pval = bPrev[ixp] * coeffs[0] + bPrev[ix] * coeffs[1] + bPrev[ixn] * coeffs[2]
            	 + bCurr[ixp] * coeffs[3] + bCurr[ix] * coeffs[4] + bCurr[ixn] * coeffs[5]
                 + bNext[ixp] * coeffs[6] + bNext[ix] * coeffs[7] + bNext[ixn] * coeffs[8];
            res32[ix] = pval;
            if (minval > pval) minval = pval;
            if (maxval < pval) maxval = pval;
            ixp = ix;  ix = ixn;
            if (ix < xe - 1) ixn++;
        }
        iy++;   wrk += pitch32;
        bPrev = bCurr;  bCurr = bNext;
        if (iy < ye - 1) bNext += pitch8;
    }
    *pmin = minval;  *pmax = maxval;
}

/****************************************************** ConvolX1D */
/***
	Horizontal convolution with given coefficients;
    updates min i max pix values
***/
void ConvolX1D (long *mem, long xe, long ye, long pitch,
                long *coeffs, long ncoeffs,
                long *pmin, long *pmax)
{
    long nwrk, shift, ix, iy, pval, minval, maxval, r0, r1;
    long *wrk, *shwrk, *sums, *row;
    BYTE *brow;

    minval = 0x7fffffff;  maxval = 0x80000000;
    nwrk = xe + ncoeffs;  shift = ncoeffs / 2;
    wrk = (long *)malloc((nwrk + ncoeffs) * sizeof(long));
    if (!wrk) goto Fin;
    shwrk = wrk + shift;  sums = wrk + nwrk;  brow = (BYTE *)mem;
    if (shift)
    {
        r0 = r1 = 0;
    	for (ix = 0;  ix < shift;  ix++)
        {
         	r0 += coeffs[ix];  r1 += coeffs[ncoeffs - 1 - ix];
            sums[ix] = r0;  sums[shift + ix] = r1;
        }
    }
    for (iy = 0;  iy < ye;  iy++)
    {
        row = (long *)brow;  brow += pitch;
        r0 = row[0];  r1 = row[xe - 1];
		PolynomMultLong (wrk, row, coeffs, xe, ncoeffs, 0);

	    if (shift)
    	{
    		for (ix = 0;  ix < shift;  ix++)
        	{
         		shwrk[shift - 1 - ix] += (sums[shift + ix] * r0);
                shwrk[xe - shift + ix] += (sums[ix] * r1);
        	}
    	}

        for (ix = 0;  ix < xe;  ix++)
        {
        	pval = row[ix] = shwrk[ix];
            if (minval > pval) minval = pval;
            if (maxval < pval) maxval = pval;
        }
    }
    free(wrk);
Fin:
    *pmin = minval;  *pmax = maxval;
}

/****************************************************** ConvolY1D */
/***
	Vertical convolution with given coefficients;
    updates min i max pix values
***/
void ConvolY1D (long *mem, long xe, long ye, long pitch,
                long *coeffs, long ncoeffs,
                long *pmin, long *pmax)
{
    long nwrk, shift, ix, iy, pval, minval, maxval, nr, yeext;
    long *wrk, *shwrk, *copycol;
    BYTE *bcol;

    minval = 0x7fffffff;  maxval = 0x80000000;
    nwrk = (ye + ncoeffs * 2) * 2;  nr = ncoeffs / 2;
    wrk = (long *)malloc(nwrk * sizeof(long));
    if (!wrk) goto Fin;
    shift = nr * 2;  yeext = ye + shift;
    shwrk = wrk + shift;
    copycol = wrk + (ye + ncoeffs * 2);
    for (ix = 0;  ix < xe;  ix++)
    {
    	bcol = (BYTE *)mem;
        for (iy = 0;  iy < yeext;  iy++)
        {
			copycol[iy] = *(long *)bcol;
            if ((iy >= nr) && (iy < ye + nr - 1))
            		bcol += pitch;
        }
		PolynomMultLong (wrk, copycol, coeffs, yeext, ncoeffs, 0);
    	bcol = (BYTE *)mem;
        for (iy = 0;  iy < ye;  iy++)
        {
        	pval = shwrk[iy];
            *(long *)bcol = pval;    bcol += pitch;
            if (minval > pval) minval = pval;
            if (maxval < pval) maxval = pval;
        }
        mem++;
    }
    free(wrk);
Fin:
    *pmin = minval;  *pmax = maxval;
}

/****************************************************** XY32toN8A8 */
/***
    Pixel-to-pixel transformation:

    	hN[i,j] = sqr( hX[i,j]**2 + hY[i,j]**2 ) * mul255
    	hA[i,j] = atan2( hX[i,j], hY[i,j] )

    hX32, hY32 - 32 bit/pix;    hN8, hA8 - 8 bit/pix;
    Argument range is mapped:
    	{-pi, pi} => {0, 255} if flag==0;
    	{0, pi} => {0, 255} if flag!=0;
    Norm is saturated at 255
    NULL is OK for <mema>
    Returns max Norm value before saturation
***/
long XY32toN8A8 (long *memx, long *memy, long xe, long ye,
				long pitchx, long pitchy,
                BYTE *memn, BYTE *mema,
				long pitchn, long pitcha,
                double mul255, long flag)
{
    long ix, iy, nl, al, maxval = -1;
    long *mx, *my;
    double px, py, a, n, pi, pi255;
   	BYTE *mn, *ma, *bmx, *bmy;

    bmx = (BYTE *)memx;    bmy = (BYTE *)memy;
	mn = memn;
    if (mema)
    {
    	ma = mema;
    	pi = 4 * atan(1.0);
	    if (!flag) pi255 = 127.999999 / pi;
    	else pi255 = 255.999999 / pi;
    }
    for (iy = 0;  iy < ye;  iy++)
    {
    	mx = (long *)bmx;
    	my = (long *)bmy;
        for (ix = 0;  ix < xe;  ix++)
        {
        	px = (double)mx[ix];  py = (double)my[ix];
            n = sqrt(px * px + py * py);
            nl = floor(n * mul255 + 0.5);
            if (maxval < nl) maxval = nl;
            if (nl > 255) nl = 255;
            mn[ix] = nl;
            if (mema)
            {
	            if (n > 0)
    	        {
        	    	a = atan2(py, px);
            	    if ((!flag) || (a < 0.0)) a += pi;
                	al = a * pi255;
	                if (al < 1) al = 1;
    	        } else al = 0;
            	ma[ix] = al;
            }
        }
        bmx += pitchx;  bmy += pitchy;
        mn += pitchn;
        if (mema) ma += pitcha;
    }
    return maxval;
}

/******************************************************** Mem32OpMem32 */
/***
	Implements given pixelwise operation on 32 bit per pixel rectangles:
    	<mem2> = <mem1> <oper> <mem2>
    List of operations:

	Op32_Abs 		0 - Abs
	Op32_MulS 		1 - Signed multiplication
	Op32_MulU 		2 - Unsigned multiplication
    Op32_Diod:		3 - Signed: negatives => 0

***/
void Mem32OpMem32 (void *mem1, long xe, long ye, long pitch1,
				void *mem2, long pitch2, long oper)
{
    long ix, iy, lval, *lm1, *lm2;
    DWORD *dwm1, *dwm2;
    BYTE *bm1, *bm2;

    bm1 = (BYTE *)mem1;  bm2 = (BYTE *)mem2;
    for (iy = 0;  iy < ye;  iy++)
    {
	   	switch (oper)
   		{
   			case Op32_Abs:	// 0
                lm1 = (long *)bm1;   dwm2 = (DWORD *)bm2;
		        for (ix = 0;  ix < xe;  ix++)
        		{
                    if ((lval = lm1[ix]) < 0) lval = -lval;
                    dwm2[ix] = (DWORD)lval;
		        }
            	break;
		   	case Op32_MulS:	// 1
                lm1 = (long *)bm1;   lm2 = (long *)bm2;
		        for (ix = 0;  ix < xe;  ix++)
        		{
                    lm2[ix] = lm1[ix] * lm2[ix];
		        }
        	    break;
		   	case Op32_MulU:	// 2
                dwm1 = (DWORD *)bm1;   dwm2 = (DWORD *)bm2;
		        for (ix = 0;  ix < xe;  ix++)
        		{
                    dwm2[ix] = dwm1[ix] * dwm2[ix];
		        }
        	    break;
   			case Op32_Diod:	// 3
                lm1 = (long *)bm1;   dwm2 = (DWORD *)bm2;
		        for (ix = 0;  ix < xe;  ix++)
        		{
                    if ((lval = lm1[ix]) < 0) lval = 0;
                    dwm2[ix] = (DWORD)lval;
		        }
            	break;
	      	default: ;
		}
        bm1 += pitch1;  bm2 += pitch2;
    }
}

/******************************************************** GammaMem32 */
/***
	Implements operation 32 bit per pixel rectangles:
    	<mem2> = <mem1> ^ <gamma>
	Returns new sum of pixels
***/
long GammaMem32 (void *mem1, long xe, long ye, long pitch1,
				void *mem2, long pitch2, double gamma)
{
    long ix, iy, sum = 0;
    DWORD dwval, *dwm1, *dwm2;
    BYTE *bm1, *bm2;
    double vgamma;

    bm1 = (BYTE *)mem1;  bm2 = (BYTE *)mem2;
    for (iy = 0;  iy < ye;  iy++)
    {
    	dwm1 = (DWORD *)bm1;   dwm2 = (DWORD *)bm2;
        for (ix = 0;  ix < xe;  ix++)
        {
        	if ((dwval = dwm1[ix]) > 0)
            {
	        	vgamma = exp(gamma * log((double)dwval)); // pix**Gamma
                dwval = (DWORD)floor(vgamma + 0.5); 	 //rounding
            }
            dwm2[ix] = dwval;
            sum += dwval;
		}
        bm1 += pitch1;  bm2 += pitch2;
    }
    return sum;
}

/******************************************************** Convol2D */
/***
	2-dimensional convolution with given coefficients;
    updates min i max pix values
***/
void Convol2D (long *mem1, long xe, long ye, long pitch1,
				long *mem2, long pitch2,
                long *coeffs, long xcoeffs, long ycoeffs,
                long *pmin, long *pmax)
{
    long nwrk, xshift, yshift, ix, iy, icfs, i, k,
    	 pval, minval, maxval, r0, r1, nx;
    long *wrk, *shwrk, *wrkrow, *row1, *row2, *rowc;
    BYTE *brow1, *brow2;

    minval = 0x7fffffff;  maxval = 0x80000000;
    nwrk = xe + 2 * xcoeffs + 4;
    wrk = (long *)malloc(nwrk * 2 * sizeof(long));
    if (!wrk) goto Fin;
    xshift = (xcoeffs + 1) / 2;  nx = xe + xshift * 2;
    shwrk = wrk + xcoeffs;  wrkrow = wrk + nwrk;
    brow1 = (BYTE *)mem1;
    brow2 = (BYTE *)mem2;  yshift = ycoeffs / 2;
    for (iy = 0;  iy < ye;  iy++)
    { // output rows loop
        row2 = (long *)brow2;  rowc = coeffs;
        for (icfs = 0;  icfs < ycoeffs;  icfs++)
        { // coeffs rows loop
            if ((k = iy + icfs - yshift) < 0) k = 0;
            if (k >= ye) k = ye - 1;
	        row1 = (long *)(brow1 + (k * pitch1));
            r0 = row1[0];  r1 = row1[xe - 1];
            for (i = 0;  i < xshift;  i++)
            {
                wrkrow[i] = r0;  wrkrow[xe + xshift + i] = r1;
            }
            for (i = 0;  i < xe;  i++) wrkrow[xshift + i] = row1[i];
            rowc = coeffs + (icfs * xcoeffs);
			PolynomMultLong (wrk, wrkrow, rowc, nx, xcoeffs, icfs);
            // icfs==0 (1-st time) - means "clean"; !=0 - "add"
        }
	    for (ix = 0;  ix < xe;  ix++)
    	{ // move to result and calculate min, max values
        	pval = row2[ix] = shwrk[ix];
            if (minval > pval) minval = pval;
            else if (maxval < pval) maxval = pval;
        }
        brow2 += pitch2;
    }
    free(wrk);
Fin:
    *pmin = minval;  *pmax = maxval;
}

//===================================================================
/******
  ConvolX1D           ConvolY1D           Convol2D
  Convol3by3          GammaMem32          Mem32OpMem32
  XY32toN8A8

  iiplBitsDiffer      iiplConv2D          iiplConvSep2D
  iiplFilter          iiplFindInMask      iiplInverseBits
  iiplMask3bord       iiplMaskBitwiseR    iiplMaskNei
  iiplMaskOnes        iiplMemConstMem     iiplMemMaskConstMem
  iiplMemMaskMem      iiplMemOpMem        iiplMemToBitMask

  iiplNeiMem          iiplUseIntelIPlib

******/
// ****************************************  end of file:  MEMRIIPL.C
