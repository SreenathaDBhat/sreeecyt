
/*#################################################################*/
//
//	Applied Imaging Corporation,
//	2380 Walsh Avenue, Bldg B,  Santa Clara,  California 95051
//	Telephone (408) 562-0250,  Fax (408) 562-0264
//
/*#################################################################*/

// ************************************************ file:  MEMRIIPL.H

#include "pentmmx.h"
//#include "comincls.h"

typedef enum {
  SmartChoiceLib=0,
  Intel_IP_Lib=1,
  AIC_ImProc_Lib=2,
  TEEM_MMX_Lib=3
} BackgroundLibraries;

// ================== Operations of Mem32OpMem32()
#define Op32_Abs 	0 // Abs
#define Op32_MulS 	1 // Signed multiplication
#define Op32_MulU 	2 // Unsigned multiplication
#define Op32_Diod	3 // Keep positives, convert negatives to zero

//########################## Prototypes from MEMRIIPL.C

void Convol3by3 (BYTE *mem8, long xe, long ye, long pitch8,
				long *mem32, long pitch32, long *coeffs,
                long *pmin, long *pmax);
void ConvolX1D (long *mem, long xe, long ye, long pitch,
                long *coeffs, long ncoeffs,
                long *pmin, long *pmax);
void ConvolY1D (long *mem, long xe, long ye, long pitch,
                long *coeffs, long ncoeffs,
                long *pmin, long *pmax);
void Convol2D (long *mem1, long xe, long ye, long pitch1,
				long *mem2, long pitch2,
                long *coeffs, long xcoeffs, long ycoeffs,
                long *pmin, long *pmax);
long GammaMem32 (void *mem1, long xe, long ye, long pitch1,
				void *mem2, long pitch2, double gamma);
void Mem32OpMem32 (void *mem1, long xe, long ye, long pitch1,
				void *mem2, long pitch2, long oper);
long XY32toN8A8 (long *memx, long *memy, long xe, long ye,
				long pitchx, long pitchy,
                BYTE *memn, BYTE *mema,
				long pitchn, long pitcha,
                double mul255, long flag);


long iiplBitsDiffer (BYTE *mem1, DWORD xe, DWORD ye, DWORD pitch1,
            BYTE *mem2, DWORD pitch2);
long iiplConv2D (BYTE *mem, long xe, long ye, long pitch,
            long xf, long yf, long xc, long yc,
            long *coeffs, long rshift);
long iiplConvSep2D (BYTE *mem, long xe, long ye,	long pitch,
               long xn, long xc, long *xcoeffs, long xrshift,
               long yn, long yc, long *ycoeffs, long yrshift);
long iiplFilter (BYTE *mem, long xe, long ye, long pitch,
				long xf, long yf, long xc, long yc,
            long oper);
long iiplFindInMask (BYTE *mem, DWORD xe, DWORD ye,
				DWORD pitch, long *Data);
void iiplInverseBits (BYTE *mem1, DWORD xe, DWORD ye, DWORD pitch1,
            BYTE *mem2, DWORD pitch2);
long iiplMask3bord (BYTE *mem1, DWORD xe, DWORD ye, DWORD pitch1,
            	BYTE *mem2, DWORD pitch2, BYTE *mem3, DWORD pitch3);
long iiplMaskBitwiseR (void *mem1,
				DWORD xe, DWORD ye, DWORD pitch1,
            void *mem2, DWORD pitch2, short oper);
void iiplMaskNei (BYTE *mem, DWORD xe, DWORD ye,
				DWORD pitch, long oper, long rep);
long iiplMaskOnes (BYTE *mem, DWORD xl, DWORD yt,
                    DWORD xe, DWORD ye, DWORD pitch);
// 10
void iiplMemConstMem (BYTE *mem1, BYTE *mem2, long size,
			WORD cnst, short oper);
DWORD iiplMemMaskConstMem (
			void *mem1, void *mem2, void *mask,
            DWORD mempix, DWORD xe, DWORD ye,
      	   	DWORD pitch1, DWORD pitch2, DWORD pitchmask,
         	DWORD cnst, DWORD oper);
DWORD iiplMemMaskMem (
				BYTE *mem1, BYTE *mem2, BYTE *mask,
				DWORD mempix, DWORD xe, DWORD ye,
            	DWORD pitch1, DWORD pitch2, DWORD pitchmask,
            	DWORD oper);
short iiplMemOpMem (BYTE *mem1, BYTE *mem2, long size, WORD oper);
long iiplMemToBitMask (
				void *mem, DWORD mempix,
                DWORD xe, DWORD ye, DWORD pitch,
                void *memmask, DWORD pitchmask,
                DWORD threshold, long opcase);
void iiplNeiMem (BYTE *mem, long xe, long ye, long pitch,
            long oper, long rep);
long iiplUseIntelIPlib(long command);
/**************** command:
// -1 - return current Background Library Flag
// 0 - smart choice for library (currently same as 2)
// 1 - use Intel IP Library
// 2 - use old AIC 'ImProc' Library
// 3 - use TEEM MMX function
****************/

// 17

// ****************************************  end of file:  MEMRIIPL.H