

#include "stdafx.h"
#include <malloc.h>

#include "mrTables.h"
#include "zGeneric.h"

//---------------------------------------------------------
//---------------------------------------------------------

char *ViciNames =
	"Prune ";

static char *Vicinities[] =
{
 	"",	// skiped
 	"*1*000004 100000004 ", // Prune
 	"",
 	""
};

//---------------------------------------------------------
void SetIdentityTable (BYTE *tab)
{
 	int i = 0;
    while (i < 64) tab[i++] = 0xCC;
}
//---------------------------------------------------------
void WordRound (char *vicword, long len, long rnd)
{
    char *p;
    long rem;
    if ((rnd < 1) || (rnd >= len)) return;
	p = (char *)alloca(len + 1);
    rem = len - rnd;
    memmove (p, vicword + rnd, rem);
    memmove (p + rem, vicword, rnd);
    memmove (vicword, p, len);
}
//---------------------------------------------------------
void ChangeTable (BYTE *tab, long zerone,
				long up, long mid, long dn)
{
    long ind, shft;

    ind = ((up & 7) << 3) | (dn & 7);
    shft = mid & 7;
    if (((tab[ind] >> shft) ^ zerone) & 1)
    	tab[ind] ^= (BYTE)(1 << shft);
}
//---------------------------------------------------------
static short transl[9] = {0, 1, 2, 8, 5, 4, 3, 6, 7};
void ChangeTableAscii (BYTE *tab, long zerone, char *ascii9)
{
    long i, k, wrk, up, mid, dn, num, signif, isi,
    	rnd = 0;
    char c, buf[24];

    num = signif = 0;
	for (i = 0;  i < 9;  i++)
    {
        wrk = 1 << transl[i];
     	buf[i] = c = ascii9[i];
        if ((c & 0xFE) == '0')
        {	// '0' or '1'
            signif |= wrk;
            if (c & 1) num |= wrk;	// '1'
        }
    }
    for (i = 0;  i < 512;  i++)
    {
        isi = i & signif;
     	if (isi == num)
        {
        	up = i & 7;  dn = (i >> 3) & 7;
            mid = (i >> 6) & 7;
			ChangeTable (tab, zerone, up, mid, dn);
        }
    }
    if (c == '8') rnd = 1;
    else if (c == '4') rnd = 2; else if (c == '2') rnd = 4;
    if (rnd)
    {
        k = 8 / rnd;  buf[8] = ' ';
        while (k--)
        {
            WordRound (buf, 8, rnd);
            ChangeTableAscii (tab, zerone, buf);
        }
    }
}
//---------------------------------------------------------
long CopyVicDescr (char *NameOrDescr, char *buf, long limit)
{
	long ind, tlen, wlen;
    DWORD offs = 0;
    char *p;

    tlen = strlen(NameOrDescr);
    wlen = WordLength(NameOrDescr, 0, tlen);
    ind = WordInText(ViciNames, &offs, 0, NameOrDescr, wlen);
	if (ind)
    {
        p = Vicinities[ind];
        tlen = strlen(p);
    } else p = NameOrDescr;
    if (tlen++ >= limit) tlen = limit;
   	memmove(buf, p, tlen);
    return ind;
}
//------------------------------------- end of mrTABLES.CPP
