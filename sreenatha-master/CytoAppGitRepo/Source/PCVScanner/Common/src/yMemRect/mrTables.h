//---------------------------------------------- mrTABLES.H

void ChangeTableAscii (BYTE *tab, long zerone, char *ascii9);
long CopyVicDescr (char *NameOrDescr, char *buf, long limit);
void SetIdentityTable (BYTE *tab);
void WordRound (char *vicword, long len, long rnd);

//---------------------------------------------------------
//--------------------------------------- end of mrTABLES.H