// zMemRect.cpp : Defines the entry point for the DLL application.
//



#include "stdafx.h"

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>   
#include <math.h>

#include "IPL.h"
#include "ijl.h"
#include "zGeneric.h"
#include "zImProc.h"
#include "MemRiipl.h"
#include "mrTables.h"
#include "zFiles.h"
#include "yMemRect.h"



BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}


/*
 *		C R E A T E P R I V A T E H E A P
 *			-- create a new heep for use here
 */
void CZMemRect::CreatePrivateHeap()
{
	DWORD InitialHeapSize;
	DWORD MaximumHeapSize = 0;  //heap may grow, restricted only by available memory
	SYSTEM_INFO SysInfo;

	GetSystemInfo (&SysInfo);

			//allocate 100 Mbytes
	DWORD nPages = (100000000 / SysInfo.dwPageSize) + 0.5;
	InitialHeapSize = nPages * SysInfo.dwPageSize;
	HeapHandle = HeapCreate (HEAP_GENERATE_EXCEPTIONS, InitialHeapSize, MaximumHeapSize); 

}

/*
 *		M Y _ M A L L O C
 *			-- allocate a block o f memory on the heap
 */
void *CZMemRect::My_malloc(DWORD nBytes)
{
	void *pMem = NULL;

	if (nBytes <= 0)
		return NULL;

	if (HeapHandle == NULL)
		CreatePrivateHeap();

	if (HeapHandle)
		pMem = HeapAlloc (HeapHandle,HEAP_GENERATE_EXCEPTIONS | HEAP_ZERO_MEMORY, (SIZE_T) nBytes);	
	else
		pMem = malloc (nBytes);

	return pMem;
}

/*
 *		M Y _ R E A L L O C
 *			-- change the size of a previously allocated block of memory on the heap
 */
void *CZMemRect::My_realloc(void *pCurrentMem, DWORD nBytes)
{
	void *pMem = NULL;

	if (nBytes <= 0)
		return NULL;

	if (HeapHandle == NULL)
		CreatePrivateHeap();

	if (HeapHandle)
		pMem = HeapReAlloc (HeapHandle,HEAP_GENERATE_EXCEPTIONS | HEAP_ZERO_MEMORY, pCurrentMem, (SIZE_T) nBytes);	
	else
		pMem = realloc (pCurrentMem, nBytes);

	return pMem;
}
/*
 *		M Y _ F R E E
 *			-- free a previously allocated block of memeory from the heap
 */
void CZMemRect::My_free(void * pMem)
{
	if (pMem == NULL)
		return;

	if (HeapHandle)
		HeapFree (HeapHandle,0,pMem);
	else
		free (pMem);
}

//extern "C" {

#define MemRectLabelSize	16
#define MemRectIsExtern		0x7FFFFF33	// located in Extern memory
#define MemRectIsOverlay	0x7FFFFF34	// lays over another MR




/**************************** S T A T I C s ************************/




static char *BitwiseOps =
	"AND 1&2 "
   	"MORE 1-2 1&~2 "
   	"Copy 1=>2 "
   	"LESS 2-1 ~1&2 "
   	"XOR MOD2 1^2 "
   	"OR 1|2 "
   	"NOR ~(1|2) "
   	"EQUAL ~(1^2) "
   	"MoreEq 1|~2 "
   	"CopyNot ~1=>2 "
   	"LessEq ~1|2 "
   	"Nand ~(1&2) ";


static DWORD BitwiseConsts[] =
{	momAND,		momAND,
	momMORE,	momMORE, 	momMORE,
   	momCOPY1, 	momCOPY1,
   	momLESS, 	momLESS, 	momLESS,
   	momEXOR, 	momEXOR, 	momEXOR,
   	momOR, 		momOR,
   	momNOR,    	momNOR,
   	momEQUAL,  	momEQUAL,
   	momEQMORE, 	momEQMORE,
   	momNOT1, 	momNOT1,
   	momEQLESS, 	momEQLESS,
   	momNAND, 	momNAND
};



static char *BytewiseOps =
	 "MIN "
	 "MAX "
	 "Plus + 1+2 "
	 "+S  Min(1+2,255) "
	 "Minus  -  1-2 "
	 "-L  2-1 "
	 "-S  Max(1-2,0) "
	 "-LS  -SL  Max(2-1,0) "
	 "LessTo0  (1>=2)?1:0 "
	 "MoreToFF  (1<=2)?1:FF "
	 "ByteMask  (1<2)?0:FF "
     "-A "
     "-A8 ";

static DWORD BytewiseConsts[] =
{
	momMIN,
   	momMAX,
   	momPLUS, 	momPLUS, 	momPLUS,
   	momSPLUS, 	momSPLUS,
   	momMINUS, 	momMINUS, 	momMINUS,
   	momIMINUS, 	momIMINUS,
   	momSMINUS, 	momSMINUS,
   	momSIMINUS, momSIMINUS, momSIMINUS,
   	momLOWto0,  momLOWto0,
   	momHIGHtoFF, momHIGHtoFF,
   	mom0orFF, 	mom0orFF,
    momAbsDiff,
    momAbsDiff8
};


/*************************************************** ConstBytewise */
/***
 hndlR must be a valid rectangle ; the function operates
 on 8-bit pixel data:
			 handlR = constant <operation> hndlR
  the operation is defined by 1st word of the parameter
  (case insensitive):
	 "MIN" 							C MIN R
	 "MAX" 							C MAX R
	 "plus", "+", "1+2" 		   (C + R) mod 256
	 "+S", "Min(1+2,255)"   	(C + R) min 255 (saturation)
	 "Minus", "-", "1-2"    	(C - R) mod 256
	 "-L", "2-1" 			   	(R - C) mod 256 (left minus; inverse order)
	 "-S", "Max(1-2,0)" 	   	(C - R) max 0   (saturation)
	 "-LS", "-SL", "Max(2-1,0)"  (R - C) max 0   (left and saturation)
	 "LessTo0", "(1>=2)?1:0"   if R >= C then R else 0
	 "MoreToFF", "(1<=2)?1:FF" if R <= C then R else 255
	 "ByteMask", "(1<2)?0:FF"  if R < C then 0 else 255
	 "-A"					   if A < B then (B - A) else (A - B)
	 "-A8"					   Abs(A - B) MIN (256 - Abs(A - B))

 returns:
 		number of processed bytes if OK,
     	0 otherwise
***/


static WORD ConstBytewiseConsts[] =
{
	momMIN,
   momMAX,
   momPLUS, 	momPLUS,  	momPLUS,
   momSPLUS, 	momSPLUS,
   momIMINUS, 	momIMINUS, 	momIMINUS,
   momMINUS, 	momMINUS,
   momSIMINUS, momSIMINUS,
   momSMINUS, 	momSMINUS, 	momSMINUS,
   momLOWto0,  momLOWto0,
   momHIGHtoFF, momHIGHtoFF,
   mom0orFF, 	mom0orFF,
   momAbsDiff,
   momAbsDiff8
};

/**************************************************** ConstBitwise */
/***
 hndlR must be a valid rectangle ; the function operates
 on 1, 8, 24 , 32 bit/pixel data:
			 handlR = constant <operation> hndlR
  the operation is defined by 1st word of the parameter
  (case insensitive):
			"And", "1&2"        C & R
			"More", "1-2", "1&~2"  C & ~R  (1 if C > R; C without R)
			"Copy", "1=>2"      C (copy C into R)
			"Less", "2-1", "~1&2" 	  ~C & R  (1 if C < R; R without C)
			"Xor", "Mod2", "1^2"  C ^ R  (mod 2 {exclusive OR})
			"Or", "1|2" 	     C | R
			"Nor", "~(1|2)" 	  ~(C | R)  (not OR)
			"Equal", "~(1^2)"   ~(C ^ R)  (1 if C = R)
			"MoreEq", "1|~2" 	  C | ~R  (1 if C >= R)
			"CopyNot", "~1=>2" 	  copy ~C into R
			"LessEq", "~1|2" 	  ~C | R  (1 if C <= R)
			"Nand", "~(1&2)" 	  ~(C & R)  (not AND)
 returns:
 		number of processed bytes if OK,
     	0 otherwise
***/

static WORD ConstBitwiseConsts[] =
{	momAND,		momAND,
   	momLESS, 	momLESS, momLESS,
   	momCOPY2, 	momCOPY2,
	momMORE,	momMORE, momMORE,
   	momEXOR, 	momEXOR, momEXOR,
   	momOR, 		momOR,
   	momNOR,    	momNOR,
   	momEQUAL,  	momEQUAL,
   	momEQLESS, 	momEQLESS,
   	momNOT2, 	momNOT1,
   	momEQMORE, 	momEQMORE,
   	momNAND, 	momNAND
};


/**************************** debug section ************************/

#define DebugBufferLen  4160
static char DebugBuffer[DebugBufferLen];
#define D   (char *)&DebugBuffer[0]

CZMemRect::CZMemRect()
{
	NumberOfMemRects = 0;
	FreeMemRect = 0;
	MemRectAddress = NULL;
	ThisRectIsBased = false;
	HeapHandle = NULL;
}

CZMemRect::~CZMemRect()
{
	mrFreeAllHandles();

	if (HeapHandle)
		HeapDestroy(HeapHandle);
}



/******************************************************** GetEntry */
/***
    Returns pointer to the entry or NULL
***/
MemRectangle *CZMemRect::GetEntry(long hndl)
{
   MemRectangle *memr = NULL;

 	if ((hndl > 0) && (hndl <= (long)NumberOfMemRects))
   {
    	memr = MemRectAddress + hndl;
   }
   return memr;
}


/***************************************************** ActiveEntry */
/***
    Returns pointer to the entry or NULL
***/
MemRectangle *CZMemRect::ActiveEntry (long hndl)
{
   	MemRectangle *memr, *retmem = NULL;

   	if ((memr = GetEntry(hndl)) != NULL)
   	{
    	if (memr->Pixelsize)
      	{
        	retmem = memr;
         	ThisRectIsBased = (memr->Memorysize < MemRectIsExtern);
      	}
   	}
   	return retmem;
}


/************************************************** FreeJustHandle */
/***
	returns 0 if bad, nonzero if OK
***/
long CZMemRect::FreeJustHandle(long hndl)
{
	long cnt, i, ret = 0;
   MemRectangle *mr;
   long *lmr;

	mr = GetEntry(hndl);
   if (mr)
   {
     	lmr = (long *)mr;
      cnt = sizeof(MemRectangle) / sizeof(long);
     	for (i = 0;  i < cnt;  i++) lmr[i] = 0;
     	mr->Memory = mr->BaseMemory = NULL; // if NULL != 0L
     	mr->Tag = FreeMemRect;
     	FreeMemRect = ret = hndl;
   }
   return ret;
}


/*************************************************** mrFreeOverlay */
/***
    Frees all the rectangles which overlay the given one
***/
void  CZMemRect::mrFreeOverlay (long hndl)
{
	MemRectangle *mrhndl, *mrh;
   	DWORD h;

	mrhndl = ActiveEntry(hndl);
   	if (!mrhndl) return;
   	while ((h = mrhndl->OwnOverList) != 0)
   	{
	  	mrh = MemRectAddress + h;
      	mrhndl->OwnOverList = mrh->SubOverList;
      	mrFreeOverlay (h);
     	FreeJustHandle (h);
   	}
}


/********************************************************** Free */
/***
    Returns sizeof(MemRectangle) if OK, 0 otherwise
***/
void  CZMemRect::mrFree(long hndl)
{
	MemRectangle *memr, *mrh;
  	long h, hnext;
   	bool isbased;

	memr = ActiveEntry(hndl);
  	if (!memr) return;
   	isbased = ThisRectIsBased;
  	mrFreeOverlay(hndl);
  	if (isbased)
  	{
  		My_free ((void *)memr->BaseMemory);
     	goto Fin;
  	}
  	if (memr->Memorysize == MemRectIsOverlay)
  	{ // delete from parent's Overlay List
     	h = memr->Memory; // parent
    	mrh = MemRectAddress + h;
      	if ((hnext = mrh->OwnOverList) == hndl)
      	{
      		mrh->OwnOverList = memr->SubOverList;
        	goto Fin;
      	}
      	while ((h = hnext) != 0)
      	{
      		mrh = MemRectAddress + h;
	     	if ((hnext = mrh->SubOverList) == hndl)
    	 	{
      			mrh->SubOverList = memr->SubOverList;
        		break;
	     	}
    	}
	}
Fin:
	FreeJustHandle(hndl);
}

void  CZMemRect::Free(long hndl)
{
	mrFree(hndl);
}

/************************************************ mrFreeAllHandles */
void  CZMemRect::mrFreeAllHandles(void)
{
	DWORD hndl = 0;
  	while (hndl++ < NumberOfMemRects)
  	{
  		Free (hndl);
  	}
   	if (MemRectAddress) My_free(MemRectAddress);
  	MemRectAddress = NULL;
	NumberOfMemRects = 0;
	FreeMemRect = 0;
}


/*********************************************** mrGetExtendedInfo */
/***
 hndl must be valid; updates Xe, Ye, Pitch, Memory
 returns:
 		BitsPerPixel if OK,
     	-1 otherwise
***/
long  CZMemRect::mrGetExtendedInfo (long hndl,
		long *Xe, long *Ye, long *Pitch, void *Memory)
{
	long xl, yt, basexe, baseye, mem, pix = -1;
   MemRectangle *mr;
   char *pnt = NULL;

   mr = ActiveEntry(hndl);
   if (!mr) goto Fin;
	xl = yt = 0;
   pix = mr->Pixelsize;
	*Xe = mr->Xext;   *Ye = mr->Yext;  *Pitch = mr->Pitch;
   mem = mr->Memory;
   while (mr->Memorysize == MemRectIsOverlay)
   {
      xl += mr->Xoffset;  yt += mr->Yoffset;
	   mr = ActiveEntry(mem);
      ThisRectIsBased = false; // correction
      if (!mr) { pix = -1;  break; }
      mem = mr->Memory;  pnt = (char *)mem;
      basexe = mr->Xext;  baseye = mr->Yext;
   }
	if (pix > 0)
   {
   	if (pnt)
      {
      	if ((xl + *Xe > basexe) || (yt + *Ye > baseye))
         {
           	pix = -1;   goto Fin;
         }
         pnt += (yt * (*Pitch) + ((xl * pix + 7) >> 3));
        	mem = (long)pnt;
      }
      *(long *)Memory = mem;
   }
Fin:
	return pix;
}


/******************************************************* mrPermute */
/***
		This function copies a given buffer, separated
        on "groups" <grouplen> bytes each (<=256), with
        permutation of bytes inside each group;
        the permutation is given by sequence of <group>
        bytes: dest[i] = src[permutation[i]],
        where 0 <= i < grouplen
	Returns:
		0 - OK,
	    -1 - wrong handle
***/
long  CZMemRect::mrPermute (long hndl, long grouplen, BYTE *permutation)
{
   long pix, xe, ye, pitch, bytecnt, ret = -1;
   char *mem;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
   if (pix < 1) goto Fin;
   bytecnt = (pix * xe + 7) >> 3;
	while (ye--)
   {
		GroupPermut (mem, mem, grouplen, permutation, bytecnt);
		mem += pitch;
   }
    ret = 0;
Fin:
	return ret;
}


/******************************************************** mrGetTag */
/***
    Returns Tag value if OK or -1 if failure
***/
long  CZMemRect::mrGetTag(long hndl)
{
	long ret = -1;
   	MemRectangle *mr;

   	mr = ActiveEntry(hndl);
   	if (mr) ret = mr->Tag;
	return ret;
}


/*************************************************** mrGetRectInfo */
/***
 hndl must be valid; updates Xe, Ye
 returns:
 		BitsPerPixel if OK,
     	-1 otherwise
***/
long  CZMemRect::mrGetRectInfo (long hndl, long *Xe, long *Ye)
{
	long pix = -1;
   MemRectangle *mr;

   mr = ActiveEntry(hndl);
   if (mr)
   {
   		pix = mr->Pixelsize;
	   	*Xe = mr->Xext;
   		*Ye = mr->Yext;
	}
   return pix;
}

/************************************************ mrGetRectOriginalBPP */
/***
 Returns the original bits per pixel when the rectangle was first created.
 For 10, 12, 14 bit images it returns 10, 12 or 14 while mrGetRectInfo()
 will return 16 which is the underlying bit depth in memory.
 For other original bit depths mrGetRectOriginalBPP() and mrGetRectInfo()
 return the same value.

 hndl must be valid;
 returns:
 		OriginalBitsPerPixel if OK,
     	-1 otherwise
***/
long  CZMemRect::mrGetRectOriginalBPP (long hndl)
{
	long pix = -1;
	MemRectangle *mr;

	mr = ActiveEntry(hndl);
	if (mr)
	{
		pix = mr->OriginalPixelsize;
	}
	return pix;
}

/************************************************ AddMemRectangles */
/***
    if OK - <FreeMemRect> is set to positive value
***/
void CZMemRect::AddMemRectangles(void)
{
	DWORD new1, cnt, newsize;

	if ((new1 = NumberOfMemRects) == 0)
  	{  	// the very beginning
  		NumberOfMemRects = StartNumberOfMemRects;
     	// number of bytes to allocate:
     	newsize = (NumberOfMemRects + 1) * sizeof(MemRectangle);
     	MemRectAddress = (MemRectangle *)My_malloc(newsize);
  	} else
  	{	// expand the list of entries
  		NumberOfMemRects += StartNumberOfMemRects;
     	// number of bytes to reallocate:
     	newsize = (NumberOfMemRects + 1) * sizeof(MemRectangle);
     	MemRectAddress = (MemRectangle *)My_realloc(MemRectAddress, newsize);
  	}
  	if (MemRectAddress)
	{  	// create or expand the list of free entries
	  	for (cnt = NumberOfMemRects;  cnt > new1;  cnt--)
      		FreeJustHandle(cnt);
   	} else FreeMemRect = 0;
}



/**************************************************** GetNewHandle */
/***
    Returns handle (long > 0) or 0 if failure
***/
long CZMemRect::GetNewHandle(void)
{
  	long ret;
  	MemRectangle *mr;

 	if (!FreeMemRect) AddMemRectangles();
 	if ((ret = FreeMemRect) > 0)
  	{
    	mr = MemRectAddress + ret;
  		FreeMemRect = mr->Tag;
  	}
  	return ret;
}



/********************************************************* MakeNew */
/***
    Returns handle (long > 0) or 0 if failure
    PitchRound:
    	1 - byte round
    	2 - word round
    	4 - doubleword round (default)
    	8 - quadword round
***/


long CZMemRect::MakeNew(long OriginalBitsPerPixel, long Xe, long Ye, long PitchRound)
{
   	long pitch, size, bitsinrow, hndl = 0;
   	MemRectangle *mr;
   	char *rectmem;

	// 10, 12, 14 bit images are stored in memory in 16 bits 
	long BitsPerPixel = (8 < OriginalBitsPerPixel && OriginalBitsPerPixel < 16)  ?  16 : OriginalBitsPerPixel;

   	if (((1L << BitsPerPixel) & AcceptablePixelSizes) == 0)
   	{
      	if ((BitsPerPixel != 32) && (BitsPerPixel != 64))
   	 	goto Fin;
   	}
   	// pointer to the current entry:
	hndl = GetNewHandle();
   	if (hndl == 0) goto Fin;
   	// calculate memory parameters:
    bitsinrow = BitsPerPixel * Xe;
	switch (PitchRound)
    {
    	case 1 : // byte round
   			pitch = (bitsinrow + 7) >> 3;
            break;
    	case 2 : // word round
   			pitch = ((bitsinrow + 15) >> 4) << 1;
            break;
    	case 8 : // quadword round
   			pitch = ((bitsinrow + 63) >> 6) << 3;
            break;
		default : // doubleword round
   			pitch = ((bitsinrow + 31) >> 5) << 2;
	}
   	size = pitch * Ye;
   	rectmem = (char *)My_malloc(size + 28);
   	if (!rectmem)
   	{
      	Free(hndl);	hndl = 0;
    	goto Fin;
   	}
   	memset(rectmem, 0, size + 16);
   	mr = GetEntry(hndl);	// handle is OK
   	if (!mr)
   	{
      	Free(hndl);	hndl = 0;
    	goto Fin;
   	}
   	//	set members:
   	mr->Pixelsize = BitsPerPixel;
	mr->OriginalPixelsize = OriginalBitsPerPixel;
   	mr->Xext = Xe;
   	mr->Yext = Ye;
   	mr->Pitch = pitch;
   	mr->Memorysize = size;
   	mr->Xoffset = mr->Yoffset =
   	mr->OwnOverList = mr->SubOverList = 0;
   	mr->Tag = 0;
   	mr->Label[0] = 0;
   	mr->BaseMemory = (DWORD)rectmem;
   	mr->Memory = (mr->BaseMemory + 7) & 0xFFFFFFF8; // 8 byte boundary alignment
Fin:
	return hndl;
}


/************************************************** mrCreate */
/***
    Returns handle (long > 0) or -1 if failure
***/
long  CZMemRect::mrCreate(long BitsPerPixel, long Xe, long Ye)
{
   long hndl;

   hndl = MakeNew (BitsPerPixel, Xe, Ye, 4);  // DWORD rounded pitch
   if (!hndl) hndl = -1;
   return hndl;
}


/****************************************************** mrCreateAs */
/***
 create new mem rect with same parameters as SampleHandle;
 returns created handle if OK,
     -1 otherwise
***/
long  CZMemRect::mrCreateAs(long sample)
{
	long hndl = -1;
   	MemRectangle *mr;

   	mr = ActiveEntry(sample);
   	if (mr)
    	hndl = mrCreate (mr->Pixelsize, mr->Xext, mr->Yext);
	return hndl;
}



/*********************************************** mrCreateAsWithPix */
/***
 create new mem rect with same width and height
 as samplehndl, but with given pixel size
 returns created handle if OK,
     -1 otherwise
***/
long  CZMemRect::mrCreateAsWithPix(long samplehndl, long pix)
{
	long rethndl = -1;
   	MemRectangle *mr;

   	mr = ActiveEntry(samplehndl);
   	if (mr)
	  	rethndl = mrCreate (pix, mr->Xext, mr->Yext);
	return rethndl;
}


/********************************************************** mrMove */
/***
 Both hndlfrom, hndlto must be valid and be of the same size
 (defined as pitch*yext );
  the function copies pixel data
 returns:
 		number of moved bytes if OK,
     	-1 otherwise
***/
long  CZMemRect::mrMove(long hndlfrom, long hndlto)
{
	long pix1, xe1, ye1, pitch1, size1, n1,
    	pix2, xe2, ye2, pitch2, size2, n2,
      	retsize = -1;
   	char *mem1, *mem2;
    bool b1, b2;

	pix1 = mrGetExtendedInfo (hndlfrom, &xe1, &ye1, &pitch1, &mem1);
	if (pix1 < 1) goto Fin;
    b1 = ThisRectIsBased;
	pix2 = mrGetExtendedInfo (hndlto, &xe2, &ye2, &pitch2, &mem2);
	if (pix2 < 1) goto Fin;
    b2 = ThisRectIsBased;
    n1 = (pix1 * xe1 + 7) >> 3;
    n2 = (pix2 * xe2 + 7) >> 3;
   	if ((ye1 == ye2) && (n1 == n2))
   	{
      	retsize = ye1 * n1;
     	while (ye2--)
      	{
        	MovMemory(mem1, mem2, n1);
         	mem1 += pitch1;  mem2 += pitch2;
      	}
       	goto Fin;
   	}
   	size1 = pitch1 * ye1;  size2 = pitch2 * ye2;
   	if ((size1 == size2) && b1 && b2)
   	{
   		retsize = size1;
   		if (hndlfrom != hndlto) MovMemory(mem1, mem2, retsize);
        	goto Fin;
   	}
Fin:
	return retsize;
}


/******************************************************* mrCopyAs */
/***
 create new mem rect with same parameters and pixel data
 as samplehndl;
 returns created handle if OK,
     -1 otherwise
***/
long  CZMemRect::mrCopyAs(long samplehndl)
{
	long rethndl;

	rethndl = mrCreateAs (samplehndl);
   	if (rethndl > 0)
   	{
   		if ( ! mrMove(samplehndl, rethndl) )
        {
     		Free (rethndl);  rethndl = -1;
     	}
   	}
   	return rethndl;
}


/***************************************************** mrRGBtoGray */
/***
 The function transforms 24 bit pixels into 8 according
   to color coefficients:  let
	     sum = red + green + blue
   If (sum > 0) then
     pix = (r * red + g * green + b * blue) / sum;
   Else
     if (sum == 0):  pix = MIN (r, g, b)
     if (sum == -1): pix = MEDIAN (r, g, b)
     if (sum == -2): pix = MAX (r, g, b)
     if (sum <= -3): pix = (r + g + b + 2) / 3
 Returns:
 		0 if OK,
     	-1 otherwise
***/
long  CZMemRect::mrRGBtoGray(long hndl24, long hndl8, long red, long green, long blue)
{
	long pix8, pix24, xe, ye, xe24, ye24, pitch8, pitch24,
    	 ret = -1;
   BYTE *mem8, *mem24;

	pix8 = mrGetExtendedInfo (hndl8, &xe, &ye, &pitch8, &mem8);
	pix24 = mrGetExtendedInfo (hndl24, &xe24, &ye24, &pitch24, &mem24);
   if ((pix8 != 8) || (pix24 != 24) || (xe != xe24) || (ye != ye24))
    	goto Fin;
   RGBtoGray (mem8, xe, ye, pitch8, mem24, pitch24, red, green, blue);
   ret = 0;
Fin:
	return ret;
}


/**************************************************** mrBytesToRGB */
/***
 hndl8 must be 8/1 bits per pixel;
 for hndl24 row size (defined as BitsPerPixel*Xext) must be
  3/24 times more than for hndl81; number of rows must be the same;
 the function transforms 8/1 bit(s) pixels into 24 according
   to color coefficients (in range from 0 to 255); saturation at 255.
 For 8 bpp: negative coefficient means 'use inverted pixel value'.
   operations: "Fill" , "Add"
   Returns: nothing
 returns:
 		0 if OK,
     	-1 otherwise
***/
long  CZMemRect::mrBytesToRGB(long hndl81, long hndl24, char *operation, long red, long green, long blue)
{
	long pix81, pix24, xe, ye, xe24, ye24, pitch81, pitch24,
   		add, ret = -1;
   	DWORD offs = 0;
   	BYTE *mem81, *mem24;

   	add = WordInText ("FILL ADD ", &offs, 0, operation, 0);
   	if (! add--) goto Fin;
	pix81 = mrGetExtendedInfo (hndl81, &xe, &ye, &pitch81, &mem81);
	pix24 = mrGetExtendedInfo (hndl24, &xe24, &ye24, &pitch24, &mem24);
   	if (((pix81 != 1) && (pix81 != 8)) || (ye != ye24)) goto Fin;
   	if ((pix81 == 1) && (pix24 * xe24 >= 24 * xe))
	{
   		BitsToRGB (mem81, xe, ye, pitch81, mem24, pitch24,
						red, green, blue, add);
     	ret = 0;
	}
   	if ((pix81 == 8) && (pix24 * xe24 >= 3 * xe))
	{
   		BytesToRGB (mem81, xe, ye, pitch81, mem24, pitch24,	red, green, blue, add);
     	ret = 0;
	}
Fin:
	return ret;
}




/************************************************** ConstPixelMask */
/***
 hndlR must be valid rectangle with 8 bit pixel;
 hndlMask may be 0 (do not use mask) or valid rectangle  {1*xe*ye}
 the function operates pixelwide, but only on pixels where
 corresponding mask pixel is set to ONE (ZERO if negative mask
 is used, see below):
	handlR(x,y) =
		IF hndlMask(x,y) THEN const <operation> hndlR(x,y)
		ELSE hndlR(x,y)  /old value/
 the operation is defined by 1st word of the parameter
 (case insensitive):
    "And", "1&2"        A & R
    "More", "1-2", "1&~2"  A & ~R  (1 if A > R; A without R)
    "Copy", "1=>2"      A (copy A into R)
    "Less", "2-1", "~1&2" 	  ~A & R  (1 if A < R; R without A)
    "Xor", "Mod2", "1^2"  A ^ R  (mod 2 {exclusive OR})
    "Or", "1|2" 	     A | R
    "Nor", "~(1|2)" 	  ~(A | R)  (not OR)
    "Equal", "~(1^2)"   ~(A ^ R)  (1 if A = R)
    "MoreEq", "1|~2" 	  A | ~R  (1 if A >= R)
    "CopyNot", "~1=>2" 	  copy ~A into R
    "LessEq", "~1|2" 	  ~A | R  (1 if A <= R)
    "Nand", "~(1&2)" 	  ~(A & R)  (not AND)

	 "MIN" 							A MIN R
	 "MAX" 							A MAX r
	 "plus", "+", "1+2" 		   (A + R) mod 256
	 "+S", "Min(1+2,255)"   	(A + R) min 255 (saturation)
	 "Minus", "-", "1-2"    	(A - R) mod 256
	 "-L", "2-1" 			   	(R - A) mod 256 (left minus; inverse order)
	 "-S", "Max(1-2,0)" 	   	(A - R) max 0   (saturation)
	 "-LS", "-SL", "Max(2-1,0)"  (R - A) max 0   (left and saturation)
	 "LessTo0", "(1>=2)?1:0"   if A >= R then A else 0
	 "MoreToFF", "(1<=2)?1:FF" if A <= R then A else 255
	 "ByteMask", "(1<2)?0:FF"  if A < R then 0 else 255
	 "-A"					   if A < B then (B - A) else (A - B)
	 "-A8"					   Abs(A - B) MIN (256 - Abs(A - B))

   if (hndlM==0) - do not use mask
   if neg != 0 - operates witn negative mask

 returns:
 		if OK: >=0;
     	-1 otherwise
***/


long CZMemRect::ConstPixelMask (long constant, char *operation, long hndlR, long hndlM, short neg)
{
    DWORD offs = 0;
    long xe, ye, xeM, yeM, pitchR, pitchM, opcase, pix, hr, hg, hb,
    	ret = -1;
    WORD oper;
    BYTE *memR, *memM;

    opcase = WordInText (BitwiseOps, &offs, 0, operation, 0);
    if (opcase)	oper = ConstBitwiseConsts [opcase - 1];
	else
    {
    	opcase = WordInText (BytewiseOps, &offs, 0, operation, 0);
   	 	if (! opcase) goto Fin;
	    oper = ConstBytewiseConsts [opcase - 1];
	}
	pix = mrGetExtendedInfo (hndlR, &xe, &ye, &pitchR, &memR);
	if ((pix == 8) || (pix == 32))
    {
		if (hndlM)
	   	{
			if (mrGetExtendedInfo (hndlM, &xeM, &yeM, &pitchM, &memM)
				!= 1) goto Fin;
	   		if ((xe != xeM) || (ye != yeM))	goto Fin;
    	 	if (neg) oper |= mmcmNEG;
	   	} else
   		{
   			memM = memR;
	     	pitchM = 0;
    	 	oper |= mmcmIGNORE;
   		}
		ret = iiplMemMaskConstMem (memR, memR, memM, pix, xe, ye,
					pitchR, pitchR, pitchM,
               		constant, oper);
    	goto Fin;
    }
    if (pix == 24)
    {
   		if ((hr = mrCreateAsWithPix(hndlR, 8)) <= 0) goto Fin;
   		if ((hg = mrCreateAs(hr)) <= 0) goto Fin;
   		if ((hb = mrCreateAs(hr)) <= 0) goto Fin;

		mrRGBtoGray(hndlR, hr, 255, 0, 0);
		mrRGBtoGray(hndlR, hg, 0, 255, 0);
		mrRGBtoGray(hndlR, hb, 0, 0, 255);

		ret = ConstPixelMask ((constant >> 16) & 0xff, operation,	hr, hndlM, neg);
		ret |= ConstPixelMask ((constant >> 8) & 0xff, operation, hg, hndlM, neg);
		ret |= ConstPixelMask ((constant) & 0xff, operation, hb, hndlM, neg);

		mrBytesToRGB(hr, hndlR, "Fill", 255, 0, 0);
		mrBytesToRGB(hg, hndlR, "Add", 0, 255, 0);
		mrBytesToRGB(hb, hndlR, "Add", 0, 0, 255);

        Free(hr);  Free(hg);  Free(hb);
    	goto Fin;
	}
Fin:
	return ret;
}


/************************************************ mrConstPixelMask */
long  CZMemRect::mrConstPixelMask(long constant, char *operation, long hndlR, long hndlM)
{
	return ConstPixelMask (constant, operation, hndlR, hndlM, 0);
}


/********************************************* mrConstPixelNotMask */
long  CZMemRect::mrConstPixelNotMask (long constant, char *operation, long hndlR, long hndlM)
{
	return ConstPixelMask (constant, operation, hndlR, hndlM, 1);
}


/************************************************** mrMaskDrawCode */
/***
	Function:	mrMaskDrawCode

	Description:
		This function draws a line given by its code
        begining from a given point on a given BitMask;

	Arguments:
		hmask - handle of mask
        x0, y0 - pointers to coordinates of the begining point;
                to be updated to the last (nondrawn) point
		pcodes - pointer to the array of codes to retrieve;
                is <period> periodical
		count - number of codes  to retrieve
        color -	0, 1 - put 0/1 pixel; >1 - inverse the pixel.

	Returns:
		number of retrieved pixels if OK,
        -1 if bad

***/


long  CZMemRect::mrMaskDrawCode (long hndl, long *x0, long *y0, void *codes, long count, long color)
{
    BYTE *maskmem;
    long hmask, pix, xe, ye, pitch, pen, ret = -1;

	pix = mrGetRectInfo (hndl, &xe, &ye);
    if (pix == 1) { hmask = hndl;  pen = color ; }
    else { hmask = mrCreate(1, xe, ye);  pen = 1; }
	mrGetExtendedInfo (hmask, &xe, &ye, &pitch, &maskmem);
    ret = MaskDrawCode (maskmem, xe, ye, pitch, x0, y0,
    			 codes, count, pen);
    if (hmask != hndl)
   	{
		ConstPixelMask (color, "Copy", hndl, hmask, 0);
      	Free(hmask);
   	}
    return ret;
}


/**************************************************** GetBasedInfo */
/***
 hndl must be valid; updates Xoffs, Yoffs
 	(relatively BASE rectangle), Xe, Ye,	Pitch, Memory
 returns:
 		BitsPerPixel if OK,
     	-1 otherwise
***/
long CZMemRect::GetBasedInfo (long hndl, long *Xoffs, long *Yoffs, long *Xe, long *Ye, long *Pitch, void *baseMem)
{
	long xl, yt, basexe, baseye, mem, pix = -1;
   	MemRectangle *mr;

    mr = ActiveEntry(hndl); 	if (!mr) goto Fin;
	xl = yt = 0;
    pix = mr->Pixelsize;
	basexe = *Xe = mr->Xext;
    baseye = *Ye = mr->Yext;
    *Pitch = mr->Pitch;
    mem = mr->Memory;
    while (mr->Memorysize == MemRectIsOverlay)
    {
        xl += mr->Xoffset;  yt += mr->Yoffset;
	    mr = ActiveEntry(mem);
        if (!mr) { pix = -1;  break; }
        mem = mr->Memory;
        basexe = mr->Xext;  baseye = mr->Yext;
    }
    if (pix > 0)
    {
        *Xoffs = xl;  *Yoffs = yt;
        *(long *)baseMem = mem;
    	 if ((xl + *Xe > basexe) || (yt + *Ye > baseye))
        {
        	pix = -1;   goto Fin;
        }
    }
Fin:
	return pix;
}




/*************************************************** mrMoveSubRect */
/***
 	moves data between given "small" memory rectangle
 	and same size subrectangle in "big" one
 	the way of moving is defined by 2 bytes of <way> parameter:
 	"=>" : from big to small; keep order of rows
 	">R" : from big to small; reverse order of rows
   "<=" : from small to big; keep order of rows
   "<R" : from small to big; reverse order of rows

    Note: (ytop < 0) is acceptable; it means "inverse Y coordinate",
    	in this case numeration begins from -1
	Returns:
		a number of moved lines if OK,
      -1 otherwise
***/
static char *MovSubOps = "=> >R <= <R ";

long  CZMemRect::mrMoveSubRect(long hndlbig,	long xleft, long ytop, long hndlsmall, char *way)
{
	long pixb, xoffb, yoffb, xeb, yeb, pitchb,
    	 pixs, xes, yes, pitchs, xl, xesb,
      	 ret = -1;	//  wait worse
   	DWORD oper, offs = 0;
   	WORD shift;
   	BYTE *memb, *mems;

   	oper = WordInText (MovSubOps, &offs, 0, way, 2);
   	if (! oper--) goto Fin;
	pixb =  GetBasedInfo (hndlbig, &xoffb, &yoffb,
				&xeb, &yeb, &pitchb, &memb);
	pixs = mrGetExtendedInfo (hndlsmall, &xes, &yes, &pitchs, &mems);
   	if ((pixb < 1) || (pixs < 1) || (pixb != pixs))
    	goto Fin;
   	if (ytop < 0) ytop += (yeb - yes);
   	if ((xleft + xes > xeb) || (ytop + yes > yeb)
    	|| (xleft < 0) || (ytop < 0)) goto Fin;
   	xoffb += xleft;  yoffb += ytop;
   	xl = (pixb * xoffb) >> 3;		// bytes
	memb += (pitchb * yoffb + xl);
	if (pixb != 1)
   	{
      	xesb = (xes * pixb) >> 3;  // row size in bytes
    	if (oper < 2)
      	{
    		iiplMemMaskMem (memb, mems, NULL, 8, xesb, yes,
        			pitchb, pitchs, 0, momCOPY1 | mmcmIGNORE);
      	} else
      	{
			if (oper & 1)
            	ReverseRows ((char *)mems, (char *)mems, pitchs, yes);
    			iiplMemMaskMem (mems, memb, NULL, 8, xesb, yes,
        			pitchs, pitchb, 0, momCOPY1 | mmcmIGNORE);
        }
    } else
    {	// here pix=1
        shift = (WORD)(xoffb & 7);
        if (oper < 2)
        {	// big to small
				ShiftBitMask (memb, xes, yes, pitchb,
            		mems, pitchs, shift, 0); // left shift
        } else
        {	// small to big
				if (oper & 1)
            	ReverseRows ((char *)mems, (char *)mems, pitchs, yes);
				ShiftBitMask (mems, xes, yes, pitchs,
            		memb, pitchb, shift, 1); // right shift
        }
    }
	if (oper & 1)
   	ReverseRows ((char *)mems, (char *)mems, pitchs, yes);
   	ret = yes;
Fin:
	return ret;
}


/********************************************************* Bitwise */
/***
 Both hndlfrom, hndlto must be valid rectangles of the same size
 (defined as pitch*yext ); the function operates on pixel data:
			 handlR = hndlA <operation> hndlR
  the operation is defined by 1st word of the parameter
  (case insensitive):
			"And", "1&2"        A & R
			"More", "1-2", "1&~2"  A & ~R  (1 if A > R; A without R)
			"Copy", "1=>2"      A (copy A into R)
			"Less", "2-1", "~1&2" 	  ~A & R  (1 if A < R; R without A)
			"Xor", "Mod2", "1^2"  A ^ R  (mod 2 {exclusive OR})
			"Or", "1|2" 	     A | R
			"Nor", "~(1|2)" 	  ~(A | R)  (not OR)
			"Equal", "~(1^2)"   ~(A ^ R)  (1 if A = R)
			"MoreEq", "1|~2" 	  A | ~R  (1 if A >= R)
			"CopyNot", "~1=>2" 	  copy ~A into R
			"LessEq", "~1|2" 	  ~A | R  (1 if A <= R)
			"Nand", "~(1&2)" 	  ~(A & R)  (not AND)
 returns:
        if rectangles are conform:
			0 - if all bits in mem2 are 0s;
			1 - if all bits in mem2 are 1s;
        	2 - otherwise
        else if sizes are equal:
 			number of processed bytes;
     	-1 otherwise
***/
long CZMemRect::Bitwise(long hndlA, char *operation, long hndlR)
{
	long pix1, xe1, ye1, pitch1,
    	 pix2, xe2, ye2, pitch2,
    	 size, retsize = -1;
    DWORD opcase, offs = 0;
    WORD oper;
    BYTE *mem1, *mem2;
    bool based1;

    opcase = WordInText (BitwiseOps, &offs, 0, operation, 0);
    if (! opcase) goto Fin;
    oper = BitwiseConsts [opcase - 1];
	pix1 = mrGetExtendedInfo (hndlA, &xe1, &ye1, &pitch1, &mem1);
	if (pix1 < 1) goto Fin;
    based1 = ThisRectIsBased;
    size = pitch1 * ye1;
    pix2 = mrGetExtendedInfo (hndlR, &xe2, &ye2, &pitch2, &mem2);
    if (pix2 < 1) goto Fin;
    xe1 *= pix1;  xe2 *= pix2;
    if ((pix1 | pix2) > 1)
    {
    	if ((size == pitch2 * ye2) &&
        		((xe1 >> 3) == pitch1) && ((xe2 >> 3) == pitch2))
        	goto MemIsFaster;
    } else
    { // both pixs = 1
    	if (based1 && ThisRectIsBased && (size == pitch2 * ye2))
          	goto MemIsFaster;
    }
	if ((xe1 == xe2) && (ye1 == ye2))
	{
//    	  retsize = MaskBitwise (mem1, xe1, ye1, pitch1,
//            		mem2, pitch2, oper);
		retsize = iiplMaskBitwiseR (mem1, xe1, ye1, pitch1,
        							mem2, pitch2, oper);
        goto Fin;
    }
    if (size != pitch2 * ye2) goto Fin;
MemIsFaster:
//    MemOpMem (mem1, mem2, size, oper);
	iiplMemOpMem (mem1, mem2, size, oper);
    retsize = size;
Fin:
	return retsize;
}




/******************************************************** Bytewise */
/***
 Both hndlfrom, hndlto must be valid rectangles of the same size
 (defined as pitch*yext ); the function operates on 8-bit pixel data:
			 handlR = hndlA <operation> hndlR
  the operation is defined by 1st word of the parameter
  (case insensitive):
	 "MIN" 							A MIN R
	 "MAX" 							A MAX r
	 "plus", "+", "1+2" 		   (A + R) mod 256
	 "+S", "Min(1+2,255)"   	(A + R) min 255 (saturation)
	 "Minus", "-", "1-2"    	(A - R) mod 256
	 "-L", "2-1" 			   	(R - A) mod 256 (left minus; inverse order)
	 "-S", "Max(1-2,0)" 	   	(A - R) max 0   (saturation)
	 "-LS", "-SL", "Max(2-1,0)"  (R - A) max 0   (left and saturation)
	 "LessTo0", "(1>=2)?1:0"   if A >= R then A else 0
	 "MoreToFF", "(1<=2)?1:FF" if A <= R then A else 255
	 "ByteMask", "(1<2)?0:FF"  if A < R then 0 else 255
	 "-A"					   if A < B then (B - A) else (A - B)
	 "-A8"					   Abs(A - B) MIN (256 - Abs(A - B))

    "InverseBits"			inverse the order of bits in each byte
    "InverseRows"			inverse the order of rows
	"Abs32" 	 R = Abs(A)
	"MulS32" 	 R = A * R (Signed multiplication)
	"MulU32" 	 R = A * R (Unsigned multiplication)
    "Diod32"  	 R = (A>0) ? A : 0
 returns:
 		number of processed bytes if OK,
     	0 otherwise
***/
static char *SpecBytewiseOps =
    "InverseBits InverseRows ";
static WORD SpecBytewiseConsts[] = {1001, 1002};

static char *Pixelwise32Ops = "Abs32 MulS32 MulU32 Diod32 ";
static long Pixelwise32Consts[] =
{ Op32_Abs, Op32_MulS, Op32_MulU, Op32_Diod };



long CZMemRect::Bytewise(long hndlA, char *operation, long hndlR)
{
   	long pix1, xe1, ye1, pitch1, pix2, xe2, ye2, pitch2,
    	 size, retsize = -1;
   	DWORD oper, opcase, offs = 0;
   	BYTE *mem1, *mem2;

   	opcase = (WORD)WordInText (BytewiseOps, &offs, 0, operation, 0);
   	if (opcase)
   	{
	   	oper = BytewiseConsts [opcase - 1];
      	pix1 = mrGetExtendedInfo (hndlA, &xe1, &ye1, &pitch1, &mem1);
      	pix2 = mrGetExtendedInfo (hndlR, &xe2, &ye2, &pitch2, &mem2);
	   	if ((pix1 <= 1) || (pix1 != pix2)) goto Fin;
        if (pix1 == 24) { pix1 = 8;  xe1 *= 3; }
		iiplMemMaskMem (mem1, mem2, NULL, pix1, xe1, ye1,
            	pitch1, pitch2, 0, oper | mmcmIGNORE);
//  	   	iiplMemOpMem (mem1, mem2, size, oper);
      	retsize = ((pix1 * xe1) >> 3) * ye1;
        goto Fin;
   	}
   	opcase = WordInText (SpecBytewiseOps, &offs, 0, operation, 0);
   	if (opcase)
   	{
      	oper = SpecBytewiseConsts [opcase - 1];
      	if (oper == 1001)
      	{	// inverse bits in each byte
         	pix1 = mrGetExtendedInfo (hndlA, &xe1, &ye1, &pitch1, &mem1);
         	pix2 = mrGetExtendedInfo (hndlR, &xe2, &ye2, &pitch2, &mem2);
         	size = (pix1 * xe1 + 7) >> 3;
         	if ((((pix2 * xe2 + 7) >> 3) == size) && (ye1 == ye2))
         	{
            	iiplInverseBits (mem1, size, ye1, pitch1, mem2, pitch2);
            	retsize = 1;
         	}
      	}
      	if (oper == 1002)
      	{	// reverse the order of rows
         	if (mrMoveSubRect (hndlA, 0, 0, hndlR, ">R") >= 0)
            retsize = 1;
      	}
      	goto Fin;
   	}
    opcase = WordInText (Pixelwise32Ops, &offs, 0, operation, 0);
    if (opcase)
    {
    	oper = Pixelwise32Consts [opcase - 1];
      	pix1 = mrGetExtendedInfo (hndlA, &xe1, &ye1, &pitch1, &mem1);
      	pix2 = mrGetExtendedInfo (hndlR, &xe2, &ye2, &pitch2, &mem2);
		if ((pix1 != 32) || (pix2 != 32) || (xe1 != xe2) || (ye1 != ye2))
        	goto Fin;
		Mem32OpMem32 (mem1, xe1, ye1, pitch1, mem2, pitch2, oper);
        retsize = 1;
      	goto Fin;
    }
   	retsize = Bitwise (hndlA, operation, hndlR);
Fin:
	return retsize;
}



/***************************************************** mrPixelwise */
/***
 Both hndlfrom, hndlto must be valid rectangles of the same size
 (defined as pitch*yext ); the function operates on 8-bit pixel data:
			 handlR = hndlA <operation> hndlR
  the operation is defined by 1st word of the parameter
  (case insensitive):
			"And", "1&2"        A & R
			"More", "1-2", "1&~2"  A & ~R  (1 if A > R; A without R)
			"Copy", "1=>2"      A (copy A into R)
			"Less", "2-1", "~1&2" 	  ~A & R  (1 if A < R; R without A)
			"Xor", "Mod2", "1^2"  A ^ R  (mod 2 {exclusive OR})
			"Or", "1|2" 	     A | R
			"Nor", "~(1|2)" 	  ~(A | R)  (not OR)
			"Equal", "~(1^2)"   ~(A ^ R)  (1 if A = R)
			"MoreEq", "1|~2" 	  A | ~R  (1 if A >= R)
			"CopyNot", "~1=>2" 	  copy ~A into R
			"LessEq", "~1|2" 	  ~A | R  (1 if A <= R)
			"Nand", "~(1&2)" 	  ~(A & R)  (not AND)
	 "MIN" 							A MIN R
	 "MAX" 							A MAX r
	 "plus", "+", "1+2" 		   (A + R) mod 256
	 "+S", "Min(1+2,255)"   	(A + R) min 255 (saturation)
	 "Minus", "-", "1-2"    	(A - R) mod 256
	 "-L", "2-1" 			   	(R - A) mod 256 (left minus; inverse order)
	 "-S", "Max(1-2,0)" 	   	(A - R) max 0   (saturation)
	 "-LS", "-SL", "Max(2-1,0)"  (R - A) max 0   (left and saturation)
	 "LessTo0", "(1>=2)?1:0"   if A >= R then A else 0
	 "MoreToFF", "(1<=2)?1:FF" if A <= R then A else 255
	 "ByteMask", "(1<2)?0:FF"  if A < R then 0 else 255
	 "-A"					   if A < B then (B - A) else (A - B)
	 "-A8"					   Abs(A - B) MIN (256 - Abs(A - B))

	"Abs32" 	 R = Abs(A)
	"MulS32" 	 R = A * R (Signed multiplication)
	"MulU32" 	 R = A * R (Unsigned multiplication)
    "Diod32" 	 Keep positives, convert negatives to zero
 returns:
 		number of processed bytes if OK,
     	-1 otherwise
***/

long  CZMemRect::mrPixelwise (long hndlA, char *operation, long hndlR)
{
   long ret;

   ret = Bytewise (hndlA, operation, hndlR);
   return ret;
}



/**************************************************** ConstBitwise */
/***
 hndlR must be a valid rectangle ; the function operates
 on 1, 8, 24 , 32 bit/pixel data:
			 handlR = constant <operation> hndlR
  the operation is defined by 1st word of the parameter
  (case insensitive):
			"And", "1&2"        C & R
			"More", "1-2", "1&~2"  C & ~R  (1 if C > R; C without R)
			"Copy", "1=>2"      C (copy C into R)
			"Less", "2-1", "~1&2" 	  ~C & R  (1 if C < R; R without C)
			"Xor", "Mod2", "1^2"  C ^ R  (mod 2 {exclusive OR})
			"Or", "1|2" 	     C | R
			"Nor", "~(1|2)" 	  ~(C | R)  (not OR)
			"Equal", "~(1^2)"   ~(C ^ R)  (1 if C = R)
			"MoreEq", "1|~2" 	  C | ~R  (1 if C >= R)
			"CopyNot", "~1=>2" 	  copy ~C into R
			"LessEq", "~1|2" 	  ~C | R  (1 if C <= R)
			"Nand", "~(1&2)" 	  ~(C & R)  (not AND)
 returns:
 		number of processed bytes if OK,
     	0 otherwise
***/



long CZMemRect::ConstBitwise(long constant, char *operation, long hndlR)
{
	long pix, xe, ye, pitch, hr, hg, hb, retsize = 0;
   	DWORD oper, opcase, offs = 0;
   	BYTE *mem;

   	opcase = WordInText (BitwiseOps, &offs, 0, operation, 0);
   	if (! opcase) goto Fin;
   	oper = ConstBitwiseConsts [opcase - 1];

	pix = mrGetExtendedInfo (hndlR, &xe, &ye, &pitch, &mem);
    if (pix < 1) goto Fin;
    retsize = pitch * ye;
    if (pix != 24)
    {
		iiplMemMaskConstMem (mem, mem, NULL, pix, xe, ye,
        	 	pitch, pitch, 0, constant, oper | mmcmIGNORE);
    	goto Fin;
	}
    if (pix == 24)
    {
   		if ((hr = mrCreateAsWithPix(hndlR, 8)) <= 0) goto Fin;
   		if ((hg = mrCreateAs(hr)) <= 0) goto Fin;
   		if ((hb = mrCreateAs(hr)) <= 0) goto Fin;

		mrRGBtoGray(hndlR, hr, 255, 0, 0);
		mrRGBtoGray(hndlR, hg, 0, 255, 0);
		mrRGBtoGray(hndlR, hb, 0, 0, 255);

        ConstBitwise ((constant >> 16) & 0xff, operation, hr);
        ConstBitwise ((constant >> 8) & 0xff, operation, hg);
        ConstBitwise ((constant) & 0xff, operation, hb);

		mrBytesToRGB(hr, hndlR, "Fill", 255, 0, 0);
		mrBytesToRGB(hg, hndlR, "Add", 0, 255, 0);
		mrBytesToRGB(hb, hndlR, "Add", 0, 0, 255);

        Free(hr);  Free(hg);  Free(hb);
    	goto Fin;
	}
Fin:
	return retsize;
}



/*************************************************** ConstBytewise */
/***
 hndlR must be a valid rectangle ; the function operates
 on 8-bit pixel data:
			 handlR = constant <operation> hndlR
  the operation is defined by 1st word of the parameter
  (case insensitive):
	 "MIN" 							C MIN R
	 "MAX" 							C MAX R
	 "plus", "+", "1+2" 		   (C + R) mod 256
	 "+S", "Min(1+2,255)"   	(C + R) min 255 (saturation)
	 "Minus", "-", "1-2"    	(C - R) mod 256
	 "-L", "2-1" 			   	(R - C) mod 256 (left minus; inverse order)
	 "-S", "Max(1-2,0)" 	   	(C - R) max 0   (saturation)
	 "-LS", "-SL", "Max(2-1,0)"  (R - C) max 0   (left and saturation)
	 "LessTo0", "(1>=2)?1:0"   if R >= C then R else 0
	 "MoreToFF", "(1<=2)?1:FF" if R <= C then R else 255
	 "ByteMask", "(1<2)?0:FF"  if R < C then 0 else 255
	 "-A"					   if A < B then (B - A) else (A - B)
	 "-A8"					   Abs(A - B) MIN (256 - Abs(A - B))

 returns:
 		number of processed bytes if OK,
     	0 otherwise
***/

long CZMemRect::ConstBytewise(long constant, char *operation, long hndlR)
{
	long pix, xe, ye, pitch, hr, hg, hb, retsize = 0;
    DWORD oper, opcase, offs = 0;
    BYTE *mem;

    opcase = WordInText (BytewiseOps, &offs, 0, operation, 0);
    if (opcase)
    {
   	  	oper = ConstBytewiseConsts [opcase - 1];
		pix = mrGetExtendedInfo (hndlR, &xe, &ye, &pitch, &mem);
    	if (pix < 1) goto Fin;
	    retsize = pitch * ye;
	    if (pix != 24)
    	{
			iiplMemMaskConstMem (mem, mem, NULL, pix, xe, ye,
         		pitch, pitch, 0, constant, oper | mmcmIGNORE);
            goto Fin;
		}
    	if (pix == 24)
	    {
   			if ((hr = mrCreateAsWithPix(hndlR, 8)) <= 0) goto Fin;
   			if ((hg = mrCreateAs(hr)) <= 0) goto Fin;
	   		if ((hb = mrCreateAs(hr)) <= 0) goto Fin;

			mrRGBtoGray(hndlR, hr, 255, 0, 0);
			mrRGBtoGray(hndlR, hg, 0, 255, 0);
			mrRGBtoGray(hndlR, hb, 0, 0, 255);

        	ConstBytewise ((constant >> 16) & 0xff, operation, hr);
	        ConstBytewise ((constant >> 8) & 0xff, operation, hg);
    	    ConstBytewise ((constant) & 0xff, operation, hb);

			mrBytesToRGB(hr, hndlR, "Fill", 255, 0, 0);
			mrBytesToRGB(hg, hndlR, "Add", 0, 255, 0);
			mrBytesToRGB(hb, hndlR, "Add", 0, 0, 255);

        	Free(hr);  Free(hg);  Free(hb);
	    	goto Fin;
		}
    } else
    {
   	  	retsize = ConstBitwise (constant, operation, hndlR);
    }
Fin:
	return retsize;
}



/************************************************ mrConstPixelwise */
/***
 hndlR must be a valid rectangle ; the function operates
 on 1- or 8-bit pixel data:
			 handlR = hndlA <operation> hndlR
  the operation is defined by 1st word of the parameter
  (case insensitive):
			"And", "1&2"        A & R
			"More", "1-2", "1&~2"  A & ~R  (1 if A > R; A without R)
			"Copy", "1=>2"      A (copy A into R)
			"Less", "2-1", "~1&2" 	  ~A & R  (1 if A < R; R without A)
			"Xor", "Mod2", "1^2"  A ^ R  (mod 2 {exclusive OR})
			"Or", "1|2" 	     A | R
			"Nor", "~(1|2)" 	  ~(A | R)  (not OR)
			"Equal", "~(1^2)"   ~(A ^ R)  (1 if A = R)
			"MoreEq", "1|~2" 	  A | ~R  (1 if A >= R)
			"CopyNot", "~1=>2" 	  copy ~A into R
			"LessEq", "~1|2" 	  ~A | R  (1 if A <= R)
			"Nand", "~(1&2)" 	  ~(A & R)  (not AND)
	 "MIN" 							A MIN R
	 "MAX" 							A MAX R
	 "plus", "+", "1+2" 		   (A + R) mod 256
	 "+S", "Min(1+2,255)"   	(A + R) min 255 (saturation)
	 "Minus", "-", "1-2"    	(A - R) mod 256
	 "-L", "2-1" 			   	(R - A) mod 256 (left minus; inverse order)
	 "-S", "Max(1-2,0)" 	   	(A - R) max 0   (saturation)
	 "-LS", "-SL", "Max(2-1,0)"  (R - A) max 0   (left and saturation)
	 "LessTo0", "(1>=2)?1:0"   if A >= R then A else 0
	 "MoreToFF", "(1<=2)?1:FF" if A <= R then A else 255
	 "ByteMask", "(1<2)?0:FF"  if A < R then 0 else 255
	 "-A"					   if A < B then (B - A) else (A - B)
	 "-A8"					   Abs(A - B) MIN (256 - Abs(A - B))

 returns:
 		number of processed bytes if OK,
     	-1 otherwise
***/

long  CZMemRect::mrConstPixelwise(long constant, char *operation, long hndlR)
{
	long ret;

	ret = ConstBytewise (constant, operation, hndlR);
   if (!ret) ret = -1;
   return ret;
}


/********************************************************* mrStat8 */
/***
	This function calculates some statistics for pixels
	with values which fit the condition:
    	pixval <operation> threshold ;
    operations: { < | <= | > | >= }
    puts the results in double array <stat>
    (must be at least 13 doubles; use bigger constant
        <mrStat8DataLength> defined in <memrectb.h>)
     stat[0] = N   	: number of pixels which met the condition
     stat[1] = Av	: Sum(P) / N
     stat[2] = Gx	: Sum(P * X) / Sum(P)
     stat[3] = Gy	: Sum(P * Y) / Sum(P)
     stat[4] = Dx	: Sum(P * (X-Gx)*(X-Gx)) / Sum(P)
     stat[5] = Dy	: Sum(P * (Y-Gy)*(Y-Gy)) / Sum(P)
     stat[6] = Cov	: Sum(P * (X-Gx)*(Y-Gy)) / Sum(P)
     stat[7] = AllP	: Sum of values for all pixels in the picture
     stat[8] = Dmax : Dispersion along the longest main axis
     stat[9] = Dmin : Dispersion along the shortest main axis
     stat[10] = Acos : COS and SIN for the angle of X-axis
     stat[11] = Asin :  and longest main axis
     stat[12] = Dp : Dispersion of pixel values
	Returns:
		number of pixels which met the condition (i.e. >=0)
      -1 if bad
***/
long  CZMemRect::mrStat8 (long hndl, char *operation,	long threshold, double *stat)
{
	long xe, ye, pitch, thr, inv, ret = -1;	//  wait worse
   BYTE *mem;
   DWORD opcase, offs = 0;

   opcase = WordInText ("< <= > >= ", &offs, 0, operation, 0);
   if (! opcase) goto Fin;
	if (mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem) != 8)
    	goto Fin;
   thr = threshold & 0xff;  inv = 0;
   if (opcase < 3) { inv = 1;  thr ^= 0xff; }
   if ((opcase & 1) && (thr < 255)) thr++;
	ret = PictThreshStat (mem, xe, ye, pitch, thr, inv, stat);
Fin:
	return ret;
}


/********************************************************* mrStat1 */
/***
 Data must be at least 15 longs; use bigger constant
	 <mrStat1DataLength>  defined in <ymemrect.h>
 for 1 bit per pixel memory rectangle calculates:
	Data[0]:=Sum(1), Data[1]:=Sum(X), Data[2]:=Sum(Y),
   Data[3]:=Sum(X*X), Data[4]:=Sum(Y*Y),
   Data[5]:=Sum(X*Y),
   Data[6]:=Min(X), Data[7]:=Max(X),
   Data[8]:=Min(Y), Data[9]:=Max(Y),
   Data[10]:= 1000 * (shortest main axis / longest main axis)
   Data[11]:= 1000 * (measure of compactness)
   Data[12]:= 1000 * cos(ALPHA)
   Data[13]:= 1000 * sin(ALPHA)
   Data[14]:= 1000 * longest main axis (in pixel/1000)

   	where ALPHA is an angle between X-axis and longest main axis
 returns: number of ONEs (same as in Data[0]) if OK,
   	-1 if bad
***/
long  CZMemRect::mrStat1(long hndl, long *Data)
{
	long xe, ye, pitch, s = -1;
   	double Nd, Gx, Gy, Dx, Dy, Cov, eps = 1.0e-8;
   	double T, Dd, R, RR, Dmax, Dmin, Cmp, Acos, Asin;
   	BYTE *mem;
    double DData[mrStat1DataLength];
    int i;

	if (mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem) == 1)
   	{
		s = MaskOperStat (mem, xe, ye, pitch, DData);

      	if (s > 0)
      	{
	  		Nd = (double)s;
  			Gx = (double)DData[1] / Nd;
      		Gy = (double)DData[2] / Nd;
	      	Dx = (double)DData[3] / Nd - Gx * Gx;
  			Dy = (double)DData[4] / Nd - Gy * Gy;
      		Cov = (double)DData[5] / Nd - Gx * Gy;
	      	T = (Dx + Dy) / 2;  Dd = (Dx - Dy) / 2;
   	   		RR = Dd * Dd + Cov * Cov;
         	R = sqrt(RR);
         	if (T < R) T = R;
         	if (T <= eps) T = 1;
     		Dmax = T + R;   Dmin = T - R;
      		Data[10] = (long)(Dmin / Dmax * 1000.0 + 0.5);
    		Cmp = ((1000 / 12.5663704) * Nd / T) + 0.5;
         	if (Cmp > 1000) Cmp = 1000;
    		Data[11] = (long)Cmp;
         	Acos = 1.0;    	Asin = 0.0;
      		if (RR > eps)
      		{
         		Acos = Dd + R;  Asin = Cov;	// for doubled angle (proportional)
         		RR = sqrt(Acos * Acos + Asin * Asin);
            	if (RR > 0.0)
            	{
         			Acos /= RR;    Asin /= RR;	// for longest main axis angle
            	} else
            	{
         			Acos = 0.0;	   Asin = 1.0;
            	}
      		}
      	   	Data[12] = (long)(Acos * 1000.0 + 0.5);
      		Data[13] = (long)(Asin * 1000.0 + 0.5);
    		Data[14] = (long)(sqrt(Dmax) * 1000.0 + 0.5);

            for(i=0;i<=9;i++)
            {
                Data[i]=(long) DData[i];
            }

      	}
   	}

	return s;
}


// works as mrStat1 but uses douyble data type array
long  CZMemRect::mrStat1D (long hndl, double *Data)
{
	long xe, ye, pitch, s = -1;
   	double Nd, Gx, Gy, Dx, Dy, Cov, eps = 1.0e-8;
   	double T, Dd, R, RR, Dmax, Dmin, Cmp, Acos, Asin;
   	BYTE *mem;

	if (mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem) == 1)
   	{
		s = MaskOperStat (mem, xe, ye, pitch, Data);
      	if (s > 0)
      	{
	  		Nd = (double)s;
  			Gx = (double)Data[1] / Nd;
      		Gy = (double)Data[2] / Nd;
	      	Dx = (double)Data[3] / Nd - Gx * Gx;
  			Dy = (double)Data[4] / Nd - Gy * Gy;
      		Cov = (double)Data[5] / Nd - Gx * Gy;
	      	T = (Dx + Dy) / 2;  Dd = (Dx - Dy) / 2;
   	   		RR = Dd * Dd + Cov * Cov;
         	R = sqrt(RR);
         	if (T < R) T = R;
         	if (T <= eps) T = 1;
     		Dmax = T + R;   Dmin = T - R;
      		Data[10] = (long)(Dmin / Dmax * 1000.0 + 0.5);
    		Cmp = ((1000 / 12.5663704) * Nd / T) + 0.5;
         	if (Cmp > 1000) Cmp = 1000;
    		Data[11] = (long)Cmp;
         	Acos = 1.0;    	Asin = 0.0;
      		if (RR > eps)
      		{
         		Acos = Dd + R;  Asin = Cov;	// for doubled angle (proportional)
         		RR = sqrt(Acos * Acos + Asin * Asin);
            	if (RR > 0.0)
            	{
         			Acos /= RR;    Asin /= RR;	// for longest main axis angle
            	} else
            	{
         			Acos = 0.0;	   Asin = 1.0;
            	}
      		}
      	   	Data[12] = (long)(Acos * 1000.0 + 0.5);
      		Data[13] = (long)(Asin * 1000.0 + 0.5);
    		Data[14] = (long)(sqrt(Dmax) * 1000.0 + 0.5);
      	}
   	}
	return s;
}


/****************************************************** mrFindNext */
/***
	returns:
   	0 if no
      1 if found (and updates coordinates)
      -1 if bad handle or coordinates
***/
long  CZMemRect::mrFindNext (long hndl, long *px, long *py, long pixtofind)
{
	long x, y, pix, xe, ye, pitch,
    	ret = -1, Data[8];
   	BYTE val, *mem, *row;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
   	if (pix < 1) goto Fin;
   	x = *px;  y = *py;
   	if (x >= xe) { x = 0;  y++; }  // go to next row
   	if ((x < 0) || (y < 0) || (y >= ye)) goto Fin;
   	if (pix == 1)
   	{
   		Data[0] = x;   Data[1] = y;	Data[2] = pixtofind;
//   	ret = MaskOper (mem, xe, ye, pitch, moFIND, Data);
      	ret = iiplFindInMask (mem, xe, ye, pitch, Data);
     	if (ret) { *px = Data[0];  *py = Data[1]; }
      	goto Fin;
   	}
   	if (pix == 8)
   	{
      	val = (BYTE)pixtofind;
      	row = mem + pitch * y;  ret = 0;
      	while (y++ < ye)
      	{
        	while (x < xe)
         	{
           		if (row[x++] == val)
            	{
               		*px = x - 1;  *py = y - 1;
               		return 1;
            	}
         	}
         	row += pitch;  x = 0;
      	}
//        goto Fin;
	}
Fin:
	return ret;
}



/****************************************************** mrOperMask */
/***
   "Zeros"	 fill mask with 0; returns 0
   "Ones"    fill mask with 1; returns 0
   "Inverse" inverse mask; returns 0
   "16oid"	 expand mask to convex 16-oid
				 returns number of ONEs in the 16-oid
   "32oid"	 expand mask to convex 32-oid
				 returns number of ONEs in the 32-oid
   "FillHoles0" fills 0-holes in a bit mask;
   			 a 0-hole is a connected set of ZERO pixels not
             connected with the boundary.
   "FillHoles1" fills 1-holes in a bit mask;	a 1-hole is a
   			 connected set of ONE pixels not connected with
             the boundary.
   "ERODE4", "DILATE4" - erosion/dilation with 4 neighboring;
   "ERODE8", "DILATE8" - erosion/dilation with 8 neighboring,
   "Frame"   frame (1 on sides, 0 inside);
   "Frame0"  not frame (0 on sides, 1 inside);
   "OnesRight"  expand ones left to right
   "OnesLeft"   expand ones right to left
   "OnesDown"   expand ones top to bottom
   "OnesUp"   	 expand ones bottom to top
   "ZerosRight" expand zeros left to right
   "ZerosLeft"  expand zeros right to left
   "ZerosDown"  expand zeros top to bottom
   "ZerosUp"    expand zeros bottom to top

	returns  -1 if something wrong
***/
static char *OperMaskOps =
	"Zeros Ones Inverse 16oid "
   	"32oid FillHoles0 FillHoles1 "
	"ERODE8 DILATE8 ERODE4 DILATE4 Frame Frame0 "
   	"OnesRight  OnesLeft  OnesDown  OnesUp "
   	"ZerosRight ZerosLeft ZerosDown ZerosUp ";

static WORD OperMaskConsts[] =
{
	moZERO, moFULL, moINV, 	 mo16OID,
   	1000,	  1001,	 1002,
   	mnmMIN, mnmMAX, mnmMIN4, mnmMAX4, mnmFRAME, mnmNFRAME,
   	mnmShadowLR1, mnmShadowRL1, mnmShadowTB1, mnmShadowBT1,
   	mnmShadowLR0, mnmShadowRL0, mnmShadowTB0, mnmShadowBT0
};


long  CZMemRect::mrOperMask(long hndl, char *operation)
{
	long pix, xe, ye, pitch, ret = -1;
   	BYTE *mem;
   	long Data[8];
   	DWORD opcase, offs = 0;
   	WORD oper,
    	 ops13[3] = {momZERO, momFULL, momNOT1};

   	opcase = WordInText (OperMaskOps, &offs, 0, operation, 0);
   	if (!opcase) goto Fin;
   	oper = OperMaskConsts [opcase - 1];
    pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
	if (pix < 1) goto Fin;
	if (opcase <= 3)
   	{	// ZEROS ONES INVERSE
    	oper = ops13[opcase - 1];
		ret = iiplMaskBitwiseR (mem, pix * xe, ye, pitch,
            			mem, pitch, oper);
     	goto Fin;
   	}
	if (opcase == 4)
   	{	// 16OID
   		ret = MaskOper (mem, xe, ye, pitch, oper, Data);
     	goto Fin;
   	}
   	if (oper == 1000)
   	{	// 32-oid
    	ret = MaskConvex (mem, xe, ye, pitch, 0);
     	goto Fin;
   	}
   	if (oper == 1001)
   	{	// fill 0-holes with 1s
		MaskFillHoles (mem, xe, ye, pitch, mem, pitch);
     	ret = 0;   goto Fin;
   	}
   	if (oper == 1002)
   	{	// fill 1-holes with 0s
     	MaskOper (mem, xe, ye, pitch, moINV, Data);
		MaskFillHoles (mem, xe, ye, pitch, mem, pitch);
     	MaskOper (mem, xe, ye, pitch, moINV, Data);
     	ret = 0;   goto Fin;
   	}
	if (opcase <= 11)
   	{	// Erode8 Dilate8  Erode4 Dilate4
		iiplMaskNei (mem, xe, ye, pitch, oper, 1);
      	ret = 0;   goto Fin;
   	}
      // Frame Frame0
      // OnesRight  OnesLeft  OnesDown  OnesUp
   		// ZerosRight ZerosLeft ZerosDown ZerosUp
   	MaskOperMask (mem, xe, ye, pitch, mem, pitch, oper);
   	ret = 0;		goto Fin;

Fin:
	return ret;
}


/****************************************************** mrPutPixel */
/***
	returns 0 (-1 if bad coordinates)
***/
long  CZMemRect::mrPutPixel (long hndl, long x, long y, long newval)
{
	long pix, xe, ye, pitch, offs, shift, ret = -1;
   	BYTE val, and, or, *mem, *addr;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
   	if ((pix < 1) || (x < 0) || (x >= xe)
    	|| (y < 0) || (y >= ye)) goto Fin;
   	offs = ((pix * x) >> 3) + pitch * y;
   	addr = mem + offs;
   	if (pix < 8)
	{
       	and = (BYTE)((1L << pix) - 1);	// 1=>1, 2=>3, 4=>15
       	shift = (x * pix) & 7;
       	or = (BYTE)((newval & and) << shift);
       	and <<= shift;
       	val = *addr;  val ^= (val & and);  val |= or;
       	*addr = val;
   	} else
   	{
    	while (pix > 0)
   	 	{
        	*addr++ = (BYTE)newval;
	      	newval >>= 8;  pix -= 8;
       	}
   	}
   	ret = 0;
Fin:
	return ret;
}


/**************************************************** mrReconFastD */
/***
		Reconstructs in 'Seed' bit mask within 'Limit'
        begining from point (xc, yc). Sizedata to be filled:
       		sizedata[0] = xmin;  sizedata[1] = ymin;
     		sizedata[2] = xmax;  sizedata[3] = ymax;

		Algorithm of reconstruction is as follows:
		Do
			OldSeed := Seed
			Seed := Dilate(Seed) MIN Limit
		While OldSeed <> Seed

		Dilate() means "set each pixel to MAX of its neighbors' value"
		This expansion is limited by Limit.

 returns:
 		Ones in Seed mask if OK, -1 otherwise
***/
long  CZMemRect::mrReconFastD (long hndlLimit, long hndlSeed, long xc, long yc, long *sizedata)
{
	long pix, pixS, xe, ye, xeS, yeS, pitchL, pitchS,
         ret = -1;
   	DWORD xmin, ymin, xmax, ymax;
   	BYTE *memL, *memS;

   	mrOperMask (hndlSeed, "Zeros");
   	if (mrPutPixel (hndlSeed, xc, yc, 1) < 0) goto Fin;
	pix = mrGetExtendedInfo (hndlLimit, &xe, &ye, &pitchL, &memL);
	pixS = mrGetExtendedInfo (hndlSeed, &xeS, &yeS, &pitchS, &memS);
   	if ((pix != 1) || (pixS != 1) || (xe != xeS) || (ye != yeS))
    	goto Fin;
   	xmin = xmax = xc;  ymin = ymax = yc;
	MaskReconFast (memL, xe, ye, pitchL, memS, pitchS,
                   &xmin, &ymin, &xmax, &ymax);
   	ret = iiplMaskOnes (memS, xmin, ymin,
    			xmax + 1 - xmin, ymax + 1 - ymin, pitchS);
   	if (ret >= 0)
    {
   		if (sizedata)
   		{
     		sizedata[0] = xmin;  sizedata[1] = ymin;
     		sizedata[2] = xmax;  sizedata[3] = ymax;
   		}
    }
Fin:
	return ret;
}


/***************************************************** mrReconFast */
/***
		Reconstructs in 'Seed' bit mask within 'Limit'
        begining from point (xc, yc).
		Algorithm of reconstruction is as follows:
		Do
			OldSeed := Seed
			Seed := Dilate(Seed) MIN Limit
		While OldSeed <> Seed

		Dilate() means "set each pixel to MAX of its neighbors' value"
		This expansion is limited by Limit.

 returns:
 		Ones in Seed mask if OK, -1 otherwise
***/
long  CZMemRect::mrReconFast(long hndlLimit, long hndlSeed, long xc, long yc)
{
	return mrReconFastD (hndlLimit, hndlSeed, xc, yc, NULL);
}


/************************************************* mrMinMaxSumInfo */
/***
  For 1/8/32 bit per pixel image fills Data[] as follows:
  Data[0] = pixel size
  Data[1] = X extend
  Data[2] = Y extend
  Data[3] = Min pixel value
  Data[4] = Max pixel value
  Data[5] = Sum of pixel values
  Data[6] = X coordinate of a (first) pixel with Min value
  Data[7] = Y coordinate of a (first) pixel with Min value
  Data[8] = X coordinate of a (first) pixel with Max value
  Data[9] = Y coordinate of a (first) pixel with Max value
  Data[10] = number of pixels with Min value
  Data[11] = number of pixels with Max value
  Returns: pix size of OK, -1 if bad
***/
long  CZMemRect::mrMinMaxSumInfo (long hndl, long *Data)
{
	long pix, xe, ye, pitch, ix, iy,
    	 h8 = 0, nmin = 0, nmax = 0,
      	 xmin = 0, ymin = 0, xmax = 0, ymax = 0,
      	 ret = -1;	//  wait worse
    long DDD[8];
   	DWORD pval, pmin, pmax, sum = 0, *dwmem;
   	BYTE *mem;

   	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
    dwmem = (DWORD *)mem;
	if (pix < 1) goto Fin;  // bad handle
	if (pix == 8)
    { // 8 bit per pixel:
   		pmin = pmax = mem[0] & 0xff;  goto Pix8or32;
    }
	if (pix == 32)
    { // 32 bit per pixel:
        pmin = pmax = dwmem[0];  goto Pix8or32;
    }
    goto Try1Or24;
Pix8or32:
	for (iy = 0;  iy < ye;  iy++)
	{
		for (ix = 0;  ix < xe;  ix++)
      	{
        	if (pix == 8) pval = (DWORD)(mem[ix] & 0xff);
            else  pval = dwmem[ix];
         	sum += pval;
         	if (pval < pmin)
         	{
           		pmin = pval;  xmin = ix;  ymin = iy;  nmin = 1;
         	} else if (pval == pmin) nmin++;
         	if (pval > pmax)
         	{
           		pmax = pval;  xmax = ix;  ymax = iy;  nmax = 1;
         	} else if (pval == pmax) nmax++;
      	}
		mem += pitch;  dwmem = (DWORD *)mem;
	}
   	goto Bye;
Try1Or24:
	if (pix == 1)
   	{
        pmin = 0;  pmax = 1;
		nmax = iiplMaskOnes (mem, 0, 0, xe, ye, pitch);
        nmin = xe * ye - nmax;  sum = nmax;
        if (nmax == 0) { pmax = 0;  nmax = nmin;  goto Bye; }
        if (nmin == 0) { pmin = 1;  nmin = nmax;  goto Bye; }
   		DDD[0] =  DDD[1] = 0;	DDD[2] = 0; // find zero
      	iiplFindInMask (mem, xe, ye, pitch, DDD);
        xmin = DDD[0];  ymin = DDD[1];  pmax = 1;
   		DDD[0] =  DDD[1] = 0;	DDD[2] = 1; // find one
      	iiplFindInMask (mem, xe, ye, pitch, DDD);
      	xmax = DDD[0];  ymax = DDD[1];
        goto Bye;
   	}
	if (pix == 24)
   	{
    	h8 = mrCreateAsWithPix(hndl, 8);
		mrRGBtoGray(hndl, h8, -3, 0, 0); // Average
        ret = mrMinMaxSumInfo (h8, Data);
        Free(h8);
        goto ByeBye;
   	}
Bye:
   	Data[3] = pmin;  	Data[4] = pmax;  	Data[5] = sum;
   	Data[6] = xmin;  	Data[7] = ymin;
   	Data[8] = xmax;  	Data[9] = ymax;
   	Data[10] = nmin; 	Data[11] = nmax;
ByeBye:
	Data[0] = pix;   	Data[1] = xe;    	Data[2] = ye;
   	ret = pix;
Fin:
	return ret;
}


/******************************************************** mrNeiFun */
/***
 executes neighborhood operation defined by
 1st word of the parameter (case insensitive):
   "AVER124" - averaging with matrix {1,2,1}; {2,4,2}; {1,2,1};
						8 neighboring, 8-bit pixels
   "MEDIAN" - middle value of (8-bit) pixel and it's 8 neighbors
   "ERODE4", "DILATE4" - erosion/dilation with 4 neighboring;
   					 1 or 8 bit pixels supported
   "ERODE8", "DILATE8" - erosion/dilation with 8 neighboring,
   					 1 or 8 bit pixels supported
   "ERODEh2", "DILATEh2" - erosion/dilation with 2 horizontal neighboring,
   					 1 or 8 bit pixels supported
   "ERODEv2", "DILATEv2" - erosion/dilation with 2 vertical neighboring,
   					 1 or 8 bit pixels supported
   "ERODE84", "DILATE84" - erosion/dilation with toggling 8/4 neighboring,
   					 1 or 8 bit pixels supported
   "ERODE48", "DILATE48" - erosion/dilation with toggling 4/8 neighboring,
   					 1 or 8 bit pixels supported
   "Open8", "Close8", "Open4", "Close4",
   "Open84", "Close84", "Open48", "Close48" - opening/closing with
         8, 4 or toggling 8/4 or 4/8 neighboring
 operation repeats <rep> times;
 Open - eroding <rep> times followed by dilating <rep> times
 Close - dilating <rep> times followed by eroding <rep> times
 returns 0 if OK, -1 otherwise
***/
static char *NeiFunOps =
	"AVER124 MEDIAN BLUR "
   	"ERODE8 DILATE8  ERODE4 DILATE4 "
	"ERODEh2 DILATEh2  ERODEv2 DILATEv2 "
   	"Open8 Close8 Open4 Close4 ";

static char *NeiFunOpsMix =
   	"ERODE84 DILATE84  ERODE48 DILATE48 "
   	"Open84 Close84 Open48 Close48 ";

static DWORD NeiFunOpVals[] =
 {
  	mnm124, mnmMED, mnmBLUR,
   	mnmMIN, mnmMAX, mnmMIN4, mnmMAX4,
  	mnmMINH2, mnmMAXH2, mnmMINV2, mnmMAXV2,
	mnmOPEN8, mnmCLOSE8,	mnmOPEN4, mnmCLOSE4
 };

long CZMemRect::NeiFunction(long hndl, long oper, long rep)
{
	long pix, xe, ye, pitch, ret = -1;
    BYTE *mem;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
	if (pix < 1) goto Fin;

    if (pix == 8)
    {
        iiplNeiMem (mem, xe, ye, pitch, oper, rep);
        ret = 0;  goto Fin;
    }
    if ((pix == 1) && (oper >= mnmMIN) && (oper <= mnmCLOSE4))
    { // allowed <oper> values
   	    iiplMaskNei (mem, xe, ye, pitch, oper, rep);
        ret = 0;  goto Fin;
    }
Fin:
	return ret;
}


long  CZMemRect::mrNeiFun(long hndl, char *operation, long rep)
{
	long i, oper, ret = -1;	//  wait worse
    long op1, op2, op3, op4, num;
    DWORD opcase, offs = 0;

    if (rep < 1) { ret = 0;  goto Fin; } // goto Bye; }
    opcase = WordInText (NeiFunOps, &offs, 0, operation, 0);
    if (opcase)
    {
		oper = NeiFunOpVals[opcase-1];
        ret = NeiFunction (hndl, oper, rep);
        goto Fin;
	}
    opcase = WordInText (NeiFunOpsMix, &offs, 0, operation, 0);
    if (! opcase) goto Fin;
    // Mixed operations:
    switch (opcase)
    {
    	case 1: // ERODE84
            op1 = mnmMIN;  op2 = mnmMIN4;  num = 2;
            break;
        case 2: // DILATE84
            op1 = mnmMAX;  op2 = mnmMAX4;  num = 2;
            break;
        case 3: // ERODE48
            op1 = mnmMIN4;  op2 = mnmMIN;  num = 2;
            break;
        case 4: // DILATE48
            op1 = mnmMAX4;  op2 = mnmMAX;  num = 2;
            break;
        case 5: // OPEN84
            op1 = mnmMIN;  op2 = mnmMIN4;
            op3 = mnmMAX;  op4 = mnmMAX4;  num = 4;
            break;
        case 6: // CLOSE84
            op1 = mnmMAX;  op2 = mnmMAX4;
            op3 = mnmMIN;  op4 = mnmMIN4;  num = 4;
            break;
        case 7: // OPEN48
            op1 = mnmMIN4;  op2 = mnmMIN;
            op3 = mnmMAX4;  op4 = mnmMAX;  num = 4;
            break;
        case 8: // CLOSE48
            op1 = mnmMAX4;  op2 = mnmMAX;
            op3 = mnmMIN4;  op4 = mnmMIN;  num = 4;
            break;
        default: num = 0;
    }
    i = 1;
    if (num > 0)
    {
    	while (i <= rep)
	    {
    		if ((i++) & 1) ret = NeiFunction (hndl, op1, 1);
			else  ret = NeiFunction (hndl, op2, 1);
		}
    }
    if (num > 2)
    {
        rep *= 2;
    	while (i <= rep) // continue i-count
	    {
    		if ((i++) & 1) ret = NeiFunction (hndl, op3, 1);
			else  ret = NeiFunction (hndl, op4, 1);
		}
    }
Fin:
	return ret;
}



/*************************************************** mrReconstruct */
/***
		Reconstructs 'Seed' bit mask within 'Limit'.
		Algorithm of reconstruction is as follows:
		Do
			OldSeed := Seed
			Seed := Dilate(Seed) MIN Limit
		While OldSeed <> Seed

		Dilate() means "set each pixel to MAX of its neighbors' value"
		This expansion is limited by Limit.

 returns:
 		if OK: Ones in Seed mask if pix=1; summa of bytes if pix=8;
        -1 otherwise
***/

long  CZMemRect::mrReconstruct(long hndlLimit, long hndlSeed)
{
	long pix, pixS, xe, ye, xeS, yeS, pitchL, pitchS, // sum, 
         oldsum = -1, ret = -1, Data[16];
   	BYTE *memL, *memS;

	pix = mrGetExtendedInfo (hndlLimit, &xe, &ye, &pitchL, &memL);
	pixS = mrGetExtendedInfo (hndlSeed, &xeS, &yeS, &pitchS, &memS);
   	if ((pix < 1) || (pixS < 1) || (pix != pixS)
   		|| (xe != xeS) || (ye != yeS)) goto Fin;
   	if (pix == 1)
   	{
		MaskReconMask (memL, xe, ye, pitchL, memS, pitchS);
	  	ret = iiplMaskOnes (memS, 0, 0, xeS, yeS, pitchS);
       	goto Fin;
   	}
   	if (pix != 8) goto Fin;
	if (Bytewise (hndlLimit, "MIN", hndlSeed) < 0) goto Fin;
	mrMinMaxSumInfo (hndlSeed, Data);
//   	sum = mrProjection0 (hndlSeed, "SUM");
//	while (sum != oldsum)
	while (oldsum != Data[5])
   	{
    	oldsum = Data[5];
		mrNeiFun (hndlSeed, "DILATE8", 1);
      	Bytewise (hndlLimit, "MIN", hndlSeed);
        mrMinMaxSumInfo (hndlSeed, Data);
//     	oldsum = sum;
//		sum = mrProjection0 (hndlSeed, "SUM");
   	}
	ret = Data[5];
Fin:
	return ret;
}


/********************************************************* mrSplit */
/***
    Splits a mask into array of linked components
    (not more than maxcomps)
 	Returns:
   	 number of linked components / 0

***/
long  CZMemRect::mrSplit(long hsource, long *complist, long maxcomps)
{
   	long hlimit, x, y, compnum;

   	x = y = compnum = 0;
   	hlimit = mrCopyAs (hsource);
   	while (compnum < maxcomps)
	{
    	if (mrFindNext (hlimit, &x, &y, 1) <= 0)
        	break; // no more
      	complist[compnum] = mrCreateAs (hsource);  // filled with 0-s
      	if (complist[compnum] <= 0) break;
      	mrReconFast (hlimit, complist[compnum], x, y);
      	Bitwise (complist[compnum], "Less", hlimit);
      	compnum++;
   	}
   	Free(hlimit);
	return compnum;
}


/************************************************** mrCreateOverlay */
/***
    Returns handle (long > 0) or -1 if failure
***/
long  CZMemRect::mrCreateOverlay(long onhndl, long xl, long yt, long xe, long ye)
{
	long hndl, ret = -1;
  	MemRectangle *onmr, *mr;

	onmr = ActiveEntry(onhndl);
  	if (!onmr) goto Fin;
  	if ((xl + xe > (long)onmr->Xext)
    		|| (yt + ye > (long)onmr->Yext))
    	goto Fin;
	hndl = GetNewHandle(); 	if (!hndl) goto Fin;
   	mr = MemRectAddress + hndl;
  	mr->Pixelsize = onmr->Pixelsize;
	mr->OriginalPixelsize = onmr->OriginalPixelsize;
  	mr->Xext = xe;
  	mr->Yext = ye;
  	mr->Pitch = onmr->Pitch;
  	mr->Memory = onhndl;
  	mr->Memorysize = MemRectIsOverlay;
  	mr->Xoffset = xl;
   	mr->Yoffset = yt;
  	mr->Tag = 0;
  	mr->Label[0] = 0;
   	// insert in parent's Overlay List:
   	mr->SubOverList = onmr->OwnOverList;
   	onmr->OwnOverList = hndl;
	ret = hndl;
Fin:
	return ret;
}


/************************************************** mrMoveOverlay */
/***
    Returns 0 if OK, or -1 if failure
***/
long  CZMemRect::mrMoveOverlay(long hndl, long newxl, long newyt)
{
  	long onhndl, ret = -1;
  	MemRectangle *onmr, *mr;

	mr = ActiveEntry(hndl); 	if (!mr) goto Fin;
  	if (mr->Memorysize != MemRectIsOverlay) goto Fin;
   	onhndl = mr->Memory;
	onmr = ActiveEntry(onhndl); if (!onmr) goto Fin;
  	if ((newxl + mr->Xext > onmr->Xext)
	    	|| (newyt + mr->Yext > onmr->Yext))
    	goto Fin;
  	mr->Xoffset = newxl;
   	mr->Yoffset = newyt;
	ret = 0;
Fin:
	return ret;
}


/********************************************* mrResizeOverlay */
/***
    Returns 0 if OK, or -1 if failure
***/
long  CZMemRect::mrResizeOverlay(long hndl, long newxl, long newyt, long newxe, long newye)
{
  	long onhndl, ret = -1;
  	MemRectangle *onmr, *mr;

	mr = ActiveEntry(hndl); 	if (!mr) goto Fin;
  	if (mr->Memorysize != MemRectIsOverlay) goto Fin;
   	onhndl = mr->Memory;
	onmr = ActiveEntry(onhndl); if (!onmr) goto Fin;
	if ((newxe + newxl > (long)onmr->Xext)
	    	|| (newye + newyt > (long)onmr->Yext))
    	goto Fin;
   	mr->Xoffset = newxl;
   	mr->Yoffset = newyt;
   	mr->Xext = newxe;
   	mr->Yext = newye;
	ret = 0;
Fin:
	return ret;
}



/**************************************************** mrSplitRange */
/***
    Splits given mask in 3:
    	minmask contains linked components with less than minarea pixels;
    	maxmask contains linked components with more than maxarea pixels;
    	srcmask contains linked components in range from minarea
        		to maxarea pixels;
    data must be at least 3 longs to update numbers of linked
    	components in minmask, srcmask and maxmask.
 	Returns:
   	 	number of all linked components / 0

***/

long  CZMemRect::mrSplitRange (long srcmask, long minmask, long maxmask,	long minarea, long maxarea, long *data)
{
   	long hlimit, hwrk, x, y, compnum, mic, mac, ones,
    	 olimit, owrk, osrc, omi, oma, oxmin, oymin, oxe, oye,
         sizedata[4];

   	x = y = mic = mac = compnum = 0;
   	hlimit = mrCopyAs (srcmask);
   	hwrk = mrCreateAs (srcmask);
    osrc = mrCreateOverlay(srcmask, 0, 0, 1, 1);
    olimit = mrCreateOverlay(hlimit, 0, 0, 1, 1);
    owrk = mrCreateOverlay(hwrk, 0, 0, 1, 1);
    if (minmask)
    {
    	mrOperMask (minmask, "zeros");
	    omi = mrCreateOverlay(minmask, 0, 0, 1, 1);
    }
    if (maxmask)
    {
    	mrOperMask (maxmask, "zeros");
	    oma = mrCreateOverlay(maxmask, 0, 0, 1, 1);
    }
   	while (true)
	{
    	if (mrFindNext (hlimit, &x, &y, 1) <= 0)
        	break; // no more
      	ones = mrReconFastD (hlimit, hwrk, x, y, sizedata);
   		oxmin = sizedata[0] & 0xfffffff0;
        oymin = sizedata[1];
   		oxe = sizedata[2] + 1 - oxmin;
   		oye = sizedata[3] + 1 - oymin;
		mrResizeOverlay(olimit, oxmin, oymin, oxe, oye);
		mrResizeOverlay(owrk, oxmin, oymin, oxe, oye);
      	Bitwise (owrk, "Less", olimit);
        compnum++;
        if ((ones >= minarea) && (ones <= maxarea)) continue;
        // not in the range, erase from source:
        mrResizeOverlay(osrc, oxmin, oymin, oxe, oye);
        Bitwise (owrk, "Less", osrc);
        if (ones < minarea)
        {
        	mic++;
            if (minmask)
            {
				mrResizeOverlay(omi, oxmin, oymin, oxe, oye);
        		Bitwise (owrk, "Or", omi);
            }
        } else
        {	// ones > maxarea)  hm = maxmask;
        	mac++;
            if (maxmask)
            {
				mrResizeOverlay(oma, oxmin, oymin, oxe, oye);
        		Bitwise (owrk, "Or", oma);
            }
        }
   	}
    if (data)
    {
     	data[0] = mic;
        data[1] = compnum - mic - mac;
        data[2] = mac;
    }
    Free(osrc);
   	Free(hlimit);  Free(hwrk);  // and also olimit, owrk
    if (minmask) Free(omi);    if (maxmask) Free(oma);
	return compnum;
}


/******************************************************* mrConvert */
/***
 Both hndlfrom, hndlto must be valid and be of the same dimension
 (defined as xext*yext ), but of different pixel sizes;
  the function converts pixel data with transformation defined by
  <lparam> value:
  pixel sizes:
  	from	to	<lparam> value
*  	1		1		ignored
*	1		8       0 => 0, 1 => <lparam> value
*	1		32		0 => 0, 1 => <lparam> value
*	8		1		pixels >= <lparam> value go to 1; 0 otherwise
*	8		8		ignored
*	8		32		left bit shift for BYTE to DWORD convertion
*	32		1 		pixels >= <lparam> value go to 1; 0 otherwise
*	32		8       right bit shift for DWORD to BYTE convertion
					BUT: if ((lparam == -1) and (max pixel > 255))
                    		then puts most significant bit of
                        	the maximal pixel value into 0x80 bit
        				 if (lparam == -2)
            				then makes MulDiv to max=255
*	32		32  	ignored
 returns:
 		number of moved pixels (pmax for [32=>8]) if OK,
     	-1 otherwise
***/
long  CZMemRect::mrConvert(long hndlfrom, long hndlto, long lparam)
{
	long pix1, xe1, ye1, pitch1,
    	pix2, xe2, ye2, pitch2;
    DWORD pixcase, rowsize, dwrk, i, k, pmax;
    long hwrk = 0, ret = -1, data[16];
   	char *mem1, *mem2;

	pix1 = mrGetExtendedInfo (hndlfrom, &xe1, &ye1, &pitch1, &mem1);
	if (pix1 < 1) goto Fin;
	pix2 = mrGetExtendedInfo (hndlto, &xe2, &ye2, &pitch2, &mem2);
	if ((pix2 < 1) || (xe1 != xe2) || (ye1 != ye2)) goto Fin;
    pixcase = (pix1 << 8) | pix2;
   	if (pixcase == 0x101)
   	{ // mask to mask
     	while (ye2--)
      	{
    		BitRowToRow (mem1, xe1, mem2, xe2);
         	mem1 += pitch1;  mem2 += pitch2;
      	}
        goto RetFin;
    }
   	if ((pixcase == 0x0808) || (pixcase == 0x2020))
   	{ // same row size BYTE/DWORD rectangles
        rowsize = (pix1 >> 8) * xe1;
     	while (ye2--)
      	{
    		memmove (mem2, mem1, rowsize);
         	mem1 += pitch1;  mem2 += pitch2;
      	}
        goto RetFin;
    }
   	if (pixcase == 0x0820)
   	{ // BYTE to DWORD rectangles
		Convert8to32 (mem1, pitch1, xe1, ye1,
					  mem2, pitch2, lparam);
        goto RetFin;
    }
   	if (pixcase == 0x2008)
   	{ // DWORD to BYTE rectangles
    	ret = Convert32to8 (mem1, pitch1, xe1, ye1,
					  mem2, pitch2, lparam);
        goto Fin;
    }
   	if (pixcase == 0x0801)
   	{ 	// BYTE rectangle to mask
 		// {1 - 6}: {NotLess,NotGreater,UnEqual,Equal,Less,Greater}
		iiplMemToBitMask (mem1, 8, xe1, ye1, pitch1,
        				  mem2, pitch2, lparam, 1);
        goto RetFin;
    }
   	if (pixcase == 0x0108)
   	{ 	// mask to BYTE rectangle
		mrConstPixelwise (0, "Copy", hndlto);
		mrConstPixelMask (lparam, "Copy", hndlto, hndlfrom);
        goto RetFin;
    }
   	if (pixcase == 0x2001)
   	{ 	// DWORD rectangle to mask
    	hwrk = mrCreate(8, xe1, ye1);
		mrMinMaxSumInfo (hndlfrom, data);
        pmax = data[4];	// max pixel value
        dwrk = k = 0;
        // find max 1-bit number in pmax:
        for (i = 0;  i < 32;  i++)
        	if ((1 << i) & pmax) dwrk = i;
        if (dwrk > 7) k = dwrk - 7;
        mrConvert(hndlfrom, hwrk, k);
        mrConvert(hwrk, hndlto, lparam >> k);
        goto RetFin;
    }
   	if (pixcase == 0x0120)
   	{ 	// mask to DWORD rectangle
    	hwrk = mrCreate(8, xe1, ye1);
        dwrk = k = 0;
        // find max 1-bit number in lparam:
        for (i = 0;  i < 32;  i++)
        	if ((1 << i) & lparam) dwrk = i;
        if (dwrk > 7) k = dwrk - 7;
		mrConstPixelMask (lparam >> k, "Copy", hwrk, hndlfrom);
        mrConvert(hwrk, hndlto, k);
        goto RetFin;
    }
    goto Fin;
RetFin:
	ret = xe1 * ye1;
Fin:
    if (hwrk) Free(hwrk);
	return ret;
}


/************************************************* mrUseIntelIPlib */
/**************** command:
 -1 - return current Background Library Flag;

  set new <BackgroundLibraries> value otherwise:
   SmartChoiceLib, Intel_IP_Lib, AIC_ImProc_Lib, TEEM_MMX_Lib

****************/
long  CZMemRect::mrUseIntelIPlib(long command)
{
	 return iiplUseIntelIPlib(command);
}


/*************************************************** mrRecOutContD */
/***
		Reconstructs in 'Seed' bit mask within 'Limit'
        begining from point (xc, yc), then builds outer contour.
        nei4: 0 - N8, 1 - N4.
 returns:
 		Ones in Seed mask if OK, -1 otherwise
***/
long  CZMemRect::mrRecOutContD(long hndlLimit, long hndlSeed, long xc, long yc, int nei4, long *sizedata)
{
	long olduse, pix, pixS, xe, ye, xeS, yeS, pitchL, pitchS,
         ret = -1;
    DWORD xmin, ymin, xmax, ymax;
    BYTE *memL, *memS;

    if ((olduse = mrUseIntelIPlib(-1)) == Intel_IP_Lib)
        mrUseIntelIPlib(0);
    mrOperMask (hndlSeed, "Zeros");
    if (mrPutPixel (hndlSeed, xc, yc, 1) < 0) goto Fin;
	pix = mrGetExtendedInfo (hndlLimit, &xe, &ye, &pitchL, &memL);
	pixS = mrGetExtendedInfo (hndlSeed, &xeS, &yeS, &pitchS, &memS);
    if ((pix != 1) || (pixS != 1) || (xe != xeS) || (ye != yeS))
        goto Fin;
    xmin = xmax = xc;  ymin = ymax = yc;
	MaskReconFast (memL, xe, ye, pitchL, memS, pitchS,
                   &xmin, &ymin, &xmax, &ymax);
    if (xmin > 0) xmin--;
    if (xmax < (DWORD)xe - 1) xmax++;
    if (ymin > 0) ymin--;
    if (ymax < (DWORD)ye - 1) ymax++;
    OutContFast (memS, xe, ye, pitchS, xmin, ymin, xmax, ymax, nei4);
    ret = iiplMaskOnes (memS, xmin, ymin,
    			xmax + 1 - xmin, ymax + 1 - ymin, pitchS);
    if (sizedata)
    {
     	sizedata[0] = xmin;  sizedata[1] = ymin;
     	sizedata[2] = xmax;  sizedata[3] = ymax;
    }
Fin:
    mrUseIntelIPlib(olduse);
	return ret;
}


/**************************************************** mrRecOutCont */
/***
		Reconstructs in 'Seed' bit mask within 'Limit'
        begining from point (xc, yc), then builds outer contour.
        nei4: 0 - N8, 1 - N4.
 returns:
 		Ones in Seed mask if OK, -1 otherwise
***/
long  CZMemRect::mrRecOutCont(long hndlLimit, long hndlSeed, long xc, long yc, int nei4)
{
	return mrRecOutContD (hndlLimit, hndlSeed, xc, yc, nei4, NULL);
}



/******************************************************* mrBorders */
/***
    Calculates the mask of borders separating linked components
    in source mask.
    nei = 8 or 4;
 	Returns:
   	 number of linked components / -1

***/

long  CZMemRect::mrBorders(long hsource, long hborders, long nei)
{
   long xe, ye, maxlen, i, keep, nei4,	lastx, lasty, compnum,
      hextnd, hlimit, hwrk1, hwrk2,
      obord, oextnd, owrk1, owrk2,
      xl, yt, xre, ybe;
   long oind, onum, overs[8], data[4], *coord;
   DWORD memalign = 0xfffffff8; // MEMORY ALIGNMENT MASK

// RESET MEMORY ALIGNMENT MASK for INTEL IMAGE PROCESSING LIBRARY:
   if (mrUseIntelIPlib(-1) == Intel_IP_Lib) memalign = 0xffffffc0;

   nei4 = lastx = lasty = compnum = hextnd = onum = 0;
   if (nei == 4) nei4++;
 	mrGetRectInfo (hsource, &xe, &ye);
   mrOperMask (hborders, "Zeros");
   maxlen = 4000;
   coord = (long *)My_malloc(maxlen * 2 * sizeof(long));
   if (!coord) goto Fin;
   hextnd = mrCopyAs (hsource);
   hlimit = mrCopyAs (hsource);
   hwrk1 = mrCopyAs (hsource);
   hwrk2 = mrCreateAs (hsource);
   overs[onum++] = oextnd = mrCreateOverlay(hextnd, 0, 0, xe, ye);
   overs[onum++] = obord = mrCreateOverlay(hborders, 0, 0, xe, ye);
   overs[onum++] = owrk1 = mrCreateOverlay(hwrk1, 0, 0, xe, ye);
   overs[onum++] = owrk2 = mrCreateOverlay(hwrk2, 0, 0, xe, ye);
    // fill array of coordinates of components:
   while (compnum < maxlen)
   {
      if (mrFindNext (hwrk1, &lastx, &lasty, 1) <= 0)
        	break; // no more
      coord[2 * compnum] = lastx;
      coord[2 * compnum + 1] = lasty;
      mrReconFastD (hwrk1, hwrk2, lastx, lasty, data);
      xl = data[0] & memalign;  yt = data[1];
      xre = (data[2] + 7) & 0xfffffff8;
      if (xre > xe) xre = xe;
      xre = xre - xl;  ybe = data[3] + 1 - yt;
      mrResizeOverlay(owrk1, xl, yt, xre, ybe);
      mrResizeOverlay(owrk2, xl, yt, xre, ybe);
      Bitwise (owrk2, "Less", owrk1);
      compnum++;
   }
   if (compnum == 0) goto Fin;
   keep = 1;
   while (keep > 0)
   {
      Bitwise (hborders, "Less", hextnd);
      mrMove (hextnd, hlimit);
      keep = 0;
      for (i = 0;  i < compnum; i++)
      {
      	lastx = coord[2 * i];  lasty = coord[2 * i + 1];
         if (lastx < 0) continue;
		 	mrRecOutContD (hlimit, hwrk1, lastx, lasty, nei4, data);
      	xl = data[0] & memalign;  yt = data[1];
      	xre = (data[2] + 8) & 0xfffffff8;
      	if (xre > xe) xre = xe;
      	xre = xre - xl;  ybe = data[3] + 1 - yt;
   		for (oind = 0;  oind < onum;  oind++)
            mrResizeOverlay(overs[oind], xl, yt, xre, ybe);
		 	mrMove (owrk1, owrk2);  // copy the border
         Bitwise (oextnd, "And", owrk1);
         Bitwise (owrk1, "Or", obord);
	      Bitwise (obord, "Less", owrk2);
         lastx = lasty = 0;
    	 	if (mrFindNext (owrk2, &lastx, &lasty, 1) > 0)
         {
         	Bitwise (owrk2, "Or", oextnd);
            Bitwise (obord, "Less", oextnd);
            keep++;
         } else
         {
            coord[2 * i] = -1;
         }
      }
   }
Fin:
   if (coord) My_free (coord);
   if (hextnd)
   {
   	  Free(hextnd);   Free(hlimit);
      Free(hwrk1);    Free(hwrk2);
   }
   return compnum;
}


/***************************************************** mrAlphaBeta */
/***
	This function calculates according to the formula:
    	hndl3 = ( (hndl1 * alpha) + (hndl2 * beta)) / 256 + gamma
   with two side saturation if <hndl3> is 8 bits per pixel,
   zero-side only if <hndl3> is 32 bits per pixel,
   !!! note:
   <hndl3> may be 8 or 32 bits per pixel

   Calculates elements of data as if to work without saturation:
   	data[0] = minimum of result pixels;
   	data[1] = maximum of result pixels;
   	data[2] = number of negative result pixels;
   	data[3] = number of greater then 255 result pixels
      				(8 bits per pixel only);
   NULL is OK for <data>.
	Returns:
        -1: something wrong;
        0: otherwise
***/
long  CZMemRect::mrAlphaBeta(long hndl1, long hndl2, long hndl3, long alpha, long beta, long gamma, long *data)
{
	long xe1, ye1, pitch1, xe2, ye2, pitch2, pix3, xe3, ye3, pitch3,
   		i, mi, ma, s1, s2, res, maxval = 0xff00, ret = -1;
   	BYTE *mem1, *mem2, *mem3;
   	long *tab1, *tab2, *lmem3;

   	if (mrGetExtendedInfo (hndl1, &xe1, &ye1, &pitch1, &mem1) != 8)
     		goto Fin;
   	if (mrGetExtendedInfo (hndl2, &xe2, &ye2, &pitch2, &mem2) != 8)
     		goto Fin;
   	pix3 = mrGetExtendedInfo (hndl3, &xe3, &ye3, &pitch3, &mem3);
   	if ((pix3 != 8) && (pix3 != 32)) goto Fin;
   	if ((xe1 != xe2) || (ye1 != ye2) || (xe1 != xe3) || (ye1 != ye3))
    		goto Fin;
   	tab1 = (long *)D;  	tab2 = tab1 + 256;
   	s1 = gamma * 256;   s2 = 0;
   	for (i = 0;  i < 256;  i++)
   	{
   		tab1[i] = s1;  s1 += alpha;
   		tab2[i] = s2;  s2 += beta;
   	}
   	s1 = s2 = 0;  mi = 9999999;  ma = -9999999;
	while (ye1--)
   	{
      	if (pix3 == 8)
      	{
   			for (i = 0;  i < xe1;  i++)
	     	{
   	  			res = tab1[mem1[i]] + tab2[mem2[i]];
            	if (data)
            	{
	      	  		if (res < mi) mi = res;
   	     			if (res > ma) ma = res;
            	}
	        	if (res < 0) { res = 0;  s1++; }
   	     		else if (res > maxval) { res = maxval;  s2++; }
     			mem3[i] = (BYTE)(res >> 8);
	     	}
   		}
      	if (pix3 == 32)
      	{
      		lmem3 = (long *)mem3;
   			for (i = 0;  i < xe1;  i++)
	     	{
   	  			res = tab1[mem1[i]] + tab2[mem2[i]];
            	if (data)
            	{
      	  			if (res < mi) mi = res;
        			if (res > ma) ma = res;
            	}
            	if (res < 0) { res = 0;  s1++; }
     			lmem3[i] = (res >> 8);
	     	}
      	}
     	mem1 += pitch1;  mem2 += pitch2;  mem3 += pitch3;
   	}
	if (data)
   	{
   		data[0] = mi >> 8;  data[1] = ma >> 8;
     	data[2] = s1;  data[3] = s2;
   	}
   	ret = 0;
Fin:
	return ret;
}


/************************************************** mrAlphaBetaLim */
/***
	This function calculates according to the formula:
    	hndl3 = ( (hndl1 * alpha) + (hndl2 * beta)) / 256 + gamma
   with two side saturation at {minval - maxval},

   Calculates elements of data as if to work without saturation:
   	data[0] = minimum of result pixels;
   	data[1] = maximum of result pixels;
   	data[2] = number of result pixels less than minval;
   	data[3] = number of result pixels more than maxval;
   NULL is OK for <data>.
	Returns:
        -1: something wrong;
        0: otherwise
***/
long  CZMemRect::mrAlphaBetaLim(long hndl1, long hndl2, long hndl3, long alpha, long beta, long gamma, long minval, long maxval, long *data)
{
	long xe1, ye1, pitch1, xe2, ye2, pitch2, xe3, ye3, pitch3,
   		i, mi, ma, s1, s2, res, ret = -1;
   BYTE *mem1, *mem2, *mem3;
   long *tab1, *tab2;

   if (mrGetExtendedInfo (hndl1, &xe1, &ye1, &pitch1, &mem1) != 8)
     		goto Fin;
   if (mrGetExtendedInfo (hndl2, &xe2, &ye2, &pitch2, &mem2) != 8)
     		goto Fin;
   if (mrGetExtendedInfo (hndl3, &xe3, &ye3, &pitch3, &mem3) != 8)
     		goto Fin;
   if ((xe1 != xe2) || (ye1 != ye2) || (xe1 != xe3) || (ye1 != ye3))
    		goto Fin;
   if ((minval = (minval & 0xff) << 8) > (maxval = (maxval & 0xff) << 8))
   { minval = 0;  maxval = 0xff00; }
   tab1 = (long *)D;  	tab2 = tab1 + 256;
   s1 = gamma * 256;   s2 = 0;
   for (i = 0;  i < 256;  i++)
   {
   	tab1[i] = s1;  s1 += alpha;
   	tab2[i] = s2;  s2 += beta;
   }
   s1 = s2 = 0;  mi = 9999999;  ma = -9999999;
	while (ye1--)
   {
  		for (i = 0;  i < xe1;  i++)
     	{
  	  		res = tab1[mem1[i]] + tab2[mem2[i]];
         if (data)
         {
      	  	if (res < mi) mi = res;
  	     		if (res > ma) ma = res;
         }
        	if (res < minval) { res = minval;  s1++; }
  	     	else if (res > maxval) { res = maxval;  s2++; }
  			mem3[i] = (BYTE)(res >> 8);
   	}
     	mem1 += pitch1;  mem2 += pitch2;  mem3 += pitch3;
   }
	if (data)
   {
   	data[0] = mi >> 8;  data[1] = ma >> 8;
     	data[2] = s1;  data[3] = s2;
   }
   ret = 0;
Fin:
	return ret;
}





/******************************************************** mrDiffer */
/***
    Compares 2 pictures.
 	Returns:
   	 0 (pictures are equal) / 1 (are not)

***/
long  CZMemRect::mrDiffer (long hndl1, long hndl2)
{
	long pix1, xe1, ye1, pitch1, pix2, xe2, ye2, pitch2,
        ret = 1;
   	BYTE *mem1, *mem2;

	pix1 = mrGetExtendedInfo (hndl1, &xe1, &ye1, &pitch1, &mem1);
	pix2 = mrGetExtendedInfo (hndl2, &xe2, &ye2, &pitch2, &mem2);
   	if (((xe1 *= pix1) == (xe2 * pix2)) && (ye1 == ye2))
		ret = iiplBitsDiffer (mem1, xe1, ye1, pitch1, mem2, pitch2);
	return ret;
}



/************************************************** mrBorder2Masks */
/***
    Calculates the mask of borders separating 2 masks.
    <nei>: "N8"(default), "N4", "N84", "N48";
    Doesn't clear <hborders>, but OR-s new pixels there;
    Returns:
     	number of a dilations when 1-st pixel(s) were added to border,
      or -1 if error
***/
static char *BorderNeis = "N8 N4 N84 N48 ";
static DWORD BorderOps[] = {mnmMAX, mnmMAX, mnmMAX4, mnmMAX4, mnmMAX, mnmMAX4, mnmMAX4, mnmMAX};

long  CZMemRect::mrBorder2Masks(long h1, long h2, long hborders, char *nei)
{
	long pix, xe, ye, pitch1, xew, yew, pitch2, pitchb, hw,
    	 oper, alter, diff, cnt, ret3, ret = -1;
   	DWORD opcase, offs = 0;
   	BYTE *mem1, *mem2, *memb;

    opcase = WordInText (BorderNeis, &offs, 0, nei, 0);
    if (opcase) opcase = (opcase - 1) * 2;
	pix = mrGetExtendedInfo (h1, &xe, &ye, &pitch1, &mem1);
   	if (pix != 1) return ret;
	pix = mrGetExtendedInfo (h2, &xew, &yew, &pitch2, &mem2);
   	if ((pix != 1) || (xe != xew) || (ye != yew)) return ret;
	pix = mrGetExtendedInfo (hborders, &xew, &yew, &pitchb, &memb);
   	if ((pix != 1) || (xe != xew) || (ye != yew)) return ret;
   	hw = mrCreateAs (h1);  ret = cnt = alter = 0;  diff = 1;
	while (diff)
   	{
   		mrMove (h1, hw);  cnt++;
        oper = BorderOps[opcase + alter];
      	iiplMaskNei (mem1, xe, ye, pitch1, oper, 1);  // "Dilate8/4"
		ret3 = iiplMask3bord (mem1, xe, ye, pitch1, mem2, pitch2, memb, pitchb);
      	diff = mrDiffer (h1, hw);
   		mrMove (h2, hw);
        oper = BorderOps[opcase + (alter ^= 1)];
      	iiplMaskNei (mem2, xe, ye, pitch2, oper, 1);  // "Dilate8/4"
		ret3 |= iiplMask3bord (mem1, xe, ye, pitch1, mem2, pitch2, memb, pitchb);
        if (ret3 > ret) ret = cnt;
        // ret3==(0 or 1); so, ret may be changed only once or never
      	diff |= mrDiffer (h2, hw);
   	}
   	Free(hw);
	return ret;
}


/*************************************************** mrCodeContour */
/***
    For maskcont 0 is OK.
    For *codes NULL is OK.
    Updates:
    	resdata[0] = start X coordinate to draw the contour;
    	resdata[1] = start Y coordinate to draw the contour;
    	resdata[2] = lenght of contour;
    	if (codes != NULL) then it is filled with incremental
        	codes of the contour, begining from the start point;
 	Returns:
   	 lenght of contour if OK,
     0 otherwise
***/
long  CZMemRect::mrCodeContour (long masksrc, long maskcont, long *resdata, void *codes, long codeslim)
{
    long xstart, ystart, ret, xe, ye, pitch, dir,
         ncodes, limit, gotpc;
    BYTE *mem, *pc;

    ret = gotpc = 0;
    if (codes)
    {
    	pc = (BYTE *)codes;  limit = codeslim;
    } else
    {
        limit = 0x4000;
    	if ((pc = (BYTE *)My_malloc(limit)) == NULL)
        	goto Fin;
        gotpc = 1;
    }
    if (mrGetExtendedInfo (masksrc, &xe, &ye, &pitch, &mem) != 1)
    	goto Fin;

    resdata[0] = resdata[1] = 0;	resdata[2] = 1;
    if (!iiplFindInMask (mem, xe, ye, pitch, resdata))
    	goto Fin;
    xstart = resdata[0];  ystart = resdata[1];

  	dir = 3; // counter clockwise, from left-to-right direction
    ncodes = CodeContour (pc, limit,	// where to send codes;
    			mem, xe, ye, pitch,		// sourse mask;
                xstart, ystart, dir);
    resdata[2] = ret = ncodes;
	if (maskcont)
    {
    	mrOperMask(maskcont, "Zeros");	// clear result mask
		mrMaskDrawCode (maskcont, &xstart, &ystart, pc, ncodes, 1);
	}
Fin:
    if (gotpc) My_free(pc);
    return ret;
}


/****************************************************** mrGetPixel */
/***
	returns pixel value (-1 if bad coordinates)
***/
long  CZMemRect::mrGetPixel (long hndl, long x, long y)
{
    long pix, xe, ye, pitch, offs, k, ret = -1;
    BYTE *mem;

    pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
    if ((pix == -1) || (x < 0) || (x >= xe)
      	|| (y < 0) || (y >= ye)) goto Fin;
    offs = ((pix * x) >> 3) + pitch * y;
    mem += offs;
    if (pix < 8)
    {
    		ret = ((*mem) >> ((x * pix) & 7)) & ((1 << pix) - 1);
    } else
    {
	   	ret = k = 0;
			while (k < pix)
	   	{
				ret |= (((*mem++) & 0xFF) << k);
            k += 8;
        }
    }
Fin:
	 return ret;
}



/************************************************* mrCodeGetPixels */
/***
	Function:	mrCodeGetPixels

	Description:
		This function gets pixels along the line given by
        incremental code begining from a given point on
        given MemRect, and sends pixels in a separate buffer

	Arguments:
		hndl - handle of mask
        x0, y0 - pointers to coordinates of the begining point;
                to be updated to the last (nondrawn) point
		pcodes - pointer to the array of codes to retrieve;
		count - number of codes  to retrieve
		pixels - pointer to the array where to send pixel values;
        		it must be at least <count> elements of the same
                pixel size as <hndl>


	Returns:
		number of retrieved pixels if OK,
        -1 if bad

***/
long  CZMemRect::mrCodeGetPixels (long hndl, long *x0, long *y0, void *pcodes, long count, void *pixels)
{
    long pix, xe, ye, i, ix, iy, ret = -1;
    DWORD *dwpixels;
	BYTE code, *codes, *bpixels;

	pix = mrGetRectInfo (hndl, &xe, &ye);
    ix = *x0;  iy = *y0;  codes = (BYTE *)pcodes;
    if (pix == 8)
    {
	    ret = 0;
        bpixels = (BYTE *)pixels;
    	for (i = 0;  i < count;  i++)
    	{
        	if ((ix >= 0) && (ix < xe) && (iy >= 0) && (iy < ye))
        	{
                bpixels[i] = mrGetPixel (hndl, ix, iy);
                ret++;
            }
        	code = codes[i];  CodeUpdateXY (&ix, &iy, code);
        }
	    *x0 = ix;  *y0 = iy;
	}
    if (pix == 32)
    {
	    ret = 0;
        dwpixels = (DWORD *)pixels;
    	for (i = 0;  i < count;  i++)
    	{
        	if ((ix >= 0) && (ix < xe) && (iy >= 0) && (iy < ye))
        	{
                dwpixels[i] = mrGetPixel (hndl, ix, iy);
                ret++;
            } 
        	code = codes[i];  CodeUpdateXY (&ix, &iy, code);
        }
	    *x0 = ix;  *y0 = iy;
	}
    return ret;
}


/************************************************* mrCodePutPixels */
/***
	Function:	mrCodePutPixels

	Description:
		This function draws a line given by its code
        begining from a given point on given MemRect;
        pixels are given in a separate buffer

	Arguments:
		hndl - handle of mask
        x0, y0 - pointers to coordinates of the begining point;
                to be updated to the last (nondrawn) point
		pcodes - pointer to the array of codes to retrieve;
		count - number of codes  to retrieve
		pixels - pointer to the array of pixel values; it must
        		be of the same pixel size as <hndl>, and must contain
                <count> elements

	Returns:
		number of retrieved pixels if OK,
        -1 if bad

***/
long  CZMemRect::mrCodePutPixels (long hndl, long *x0, long *y0, void *pcodes, long count, void *pixels)
{
    long pix, xe, ye, i, ix, iy, ret = -1;
    DWORD pixval, *dwpixels;
	BYTE code, *codes, *bpixels;

	pix = mrGetRectInfo (hndl, &xe, &ye);
    ix = *x0;  iy = *y0;  codes = (BYTE *)pcodes;
    if (pix == 8)
    {
	    ret = 0;
        bpixels = (BYTE *)pixels;
    	for (i = 0;  i < count;  i++)
    	{
        	if ((ix >= 0) && (ix < xe) && (iy >= 0) && (iy < ye))
        	{
				pixval = bpixels[i];
				mrPutPixel (hndl, ix, iy, pixval);
                ret++;
            }
        	code = codes[i];  CodeUpdateXY (&ix, &iy, code);
        }
	    *x0 = ix;  *y0 = iy;
	}
    if (pix == 32)
    {
	    ret = 0;
        dwpixels = (DWORD *)pixels;
    	for (i = 0;  i < count;  i++)
    	{
        	if ((ix >= 0) && (ix < xe) && (iy >= 0) && (iy < ye))
        	{
				pixval = dwpixels[i];
				mrPutPixel (hndl, ix, iy, pixval);
                ret++;
            }
        	code = codes[i];  CodeUpdateXY (&ix, &iy, code);
        }
	    *x0 = ix;  *y0 = iy;
	}
    return ret;
}


// ===================================== Definition:
#define CS_max 16


/************************************************* mrColorDistance */
/***
   <hndls> contains mrHandles of <hnum> color components,
   <hcolors> contains "base" color values.
	Fills hsim with values of "distance" from a pixel to
    the "base" direction, normalized to the range {0-255}:
    	"distance" = summa(abs(normalized colors difference))
    Returns maximal distance if OK,
    		-1 otherwise
***/
long  CZMemRect::mrColorDistance(long *hndls, long hnum,	BYTE *hbase, long hdist)
{
	long pix, xe, ye, xew, yew, pitchdist,
	     ih, ix, iy, cc, ival, bmax, cmax;
    long ret = 0, bnorm = 100,
    	 pitchs[CS_max], bcolors[CS_max], ccolors[CS_max];
   	BYTE *memdist, *mems[CS_max];

    if (hnum > CS_max) hnum = CS_max;
	pix = mrGetExtendedInfo (hdist, &xe, &ye, &pitchdist, &memdist);
	if (pix != 8) goto Fin;
    bmax = 0;
	for (ih = 0;  ih < hnum;  ih++)
    {
		pix = mrGetExtendedInfo (hndls[ih], &xew, &yew, &pitchs[ih], &mems[ih]);
		if ((pix != 8) || (xew != xe) || (yew != ye)) { bmax = 0;  break; }
        cc = bcolors[ih] = hbase[ih] & 0xff;
        if (bmax < cc) bmax = cc;
    }
    if (bmax < 1) { ret = -1;  goto Fin; }
    // normalize
	for (ih = 0;  ih < hnum;  ih++) bcolors[ih] = (bcolors[ih] * bnorm) / bmax;
    for (iy = 0;  iy < ye;  iy++)
    {
	    for (ix = 0;  ix < xe;  ix++)
    	{
            cmax = 0;
			for (ih = 0;  ih < hnum;  ih++)
            {
				cc = ccolors[ih] = mems[ih][ix];
                if (cmax < cc) cmax = cc;
            }
            if (cmax > 0)
            {
        	    ival = 0;
                for (ih = 0;  ih < hnum;  ih++)
                {
                    cc = (ccolors[ih] * bnorm) / cmax; // normalized value
                	ival += abs(cc - bcolors[ih]);
                }
                if (ival > 255) ival = 255;
            } else ival = 255;
        	memdist[ix] = (unsigned char) ival;
            if (ret < ival) ret = ival;
        }
        for (ih = 0;  ih < hnum;  ih++)
        	mems[ih] += pitchs[ih];
        memdist += pitchdist;
    }
Fin:
    return ret;
}


/*********************************************** mrColorSaturImage */
/***
   <hndls> contains mrHandles of <hnum> color components.
	Fills hsat with ColorSaturation normalized to the range {0-255}
    Returns maximal ColorSaturation if OK,
    		-1 otherwise
***/
long  CZMemRect::mrColorSaturImage(long *hndls, long hnum, long hsat)
{
	long pix, xe, ye, xew, yew, wrk, pitchsat;
	long ih, ix, iy, cc, cmin, ival;
    long ret = -1, pitchs[CS_max];
   	BYTE *memsat, *mems[CS_max];

    if (hnum > CS_max) hnum = CS_max;
	pix = mrGetExtendedInfo (hsat, &xe, &ye, &pitchsat, &memsat);
	if (pix != 8) goto Fin;
	for (ih = 0;  ih < hnum;  ih++)
    {
		pix = mrGetExtendedInfo (hndls[ih], &xew, &yew, &pitchs[ih], &mems[ih]);
		if ((pix != 8) || (xew != xe) || (yew != ye)) { ret = -1;  break; }
    }
    if (ih < hnum) goto Fin;

    for (iy = 0;  iy < ye;  iy++)
    {
	    for (ix = 0;  ix < xe;  ix++)
    	{
            cmin = 255;  cc = 0;   // sum
			for (ih = 0;  ih < hnum;  ih++)
            {
				wrk = mems[ih][ix];
				cc += wrk;
                if (cmin > wrk) cmin = wrk;
            }
            if ((wrk = (cc - cmin * hnum)) > 0)
            {
        	    ival = (wrk * 256 - 1) / cc;
            } else ival = 0;
        	memsat[ix] = ival;
            if (ret < ival) ret = ival;
        }
        for (ih = 0;  ih < hnum;  ih++)
        	mems[ih] += pitchs[ih];
        memsat += pitchsat;
    }
Fin:
    return ret;
}


/*********************************************** mrColorSaturTrans */
/***
   <hndls> contains mrHandles of <hnum> color components.
   the transformation is pixelwise, pixel values to be changed so
   that: 1) sum of values is invariant; 2) new saturation value is
   as close as possible to piece linear function:
   		(0,0) - (x1,y1) - (x2,y2) - (1,1)
	Returns number of changed pixels if OK,
    		-1 otherwise
***/
long  CZMemRect::mrColorSaturTrans(long *hndls, long hnum, double x1, double y1, double x2, double y2)
{
	long pix, xe, ye, xew, yew, wrk;
	long ih, ix, iy, cc, cmin, cmax, dmin, dmax;
    long ret = 0, c[CS_max], pitchs[CS_max];
    double sat, t, tmin, v, ctarget, tw, cw;
   	BYTE *mems[CS_max];

    if (hnum > CS_max) hnum = CS_max;
	for (ih = 0;  ih < hnum;  ih++)
    {
		pix = mrGetExtendedInfo (hndls[ih], &xew, &yew, &pitchs[ih], &mems[ih]);
		if (pix != 8) { ret = -1;  break; }
        if (ih == 0) { xe = xew;  ye = yew; }
        else if ((xew != xe) || (yew != ye)) { ret = -1;  break; }
    }
    if (ret < 0) goto Fin;
    for (iy = 0;  iy < ye;  iy++)
    {
	    for (ix = 0;  ix < xe;  ix++)
    	{
            cmin = 255 * hnum;  cmax = cc = 0;   // sum
			for (ih = 0;  ih < hnum;  ih++)
            {
				wrk = mems[ih][ix];
				cc += wrk;
                c[ih] = (wrk *= hnum);
                if (cmin > wrk) cmin = wrk;
                if (cmax < wrk) cmax = wrk;
            }
            if ((cc == 0) || (cc == cmin)) continue;	// all zeros
            dmin = cmin - cc;  dmax = cmax - cc;
            // dmin<=0, dmax>=0;
            if ((!dmin) || (!dmax)) continue;	// do not change
            // dmin<0, dmax>0; so, sat>0.0:
            sat = (double)(-dmin) / cc;

            if (sat <= x1)
            {
                v = sat * y1 / x1;
                goto CalcT;
            }
            if (sat < x2)
            {
                v = y1 + (sat - x1) * (y2 - y1) / (x2 - x1);
                goto CalcT;
            }
            v = y2 + (sat - x2) * (1.0 - y2) / (1.0 - x2);
CalcT:
            ctarget = (1 - v) * cc;
            tmin = (ctarget - cmin) / dmin;
            if (tmin > (t = (255.0 * hnum - cmax) / dmax)) tmin = t;
            tw = tmin * cc;
			for (ih = 0;  ih < hnum;  ih++)
            {
            	cw = (tmin + 1.0) * c[ih];
				wrk = (cw - tw) / hnum + 0.5;
				mems[ih][ix] = wrk;
            }
            ret++;
        }
        for (ih = 0;  ih < hnum;  ih++) mems[ih] += pitchs[ih];
    }
Fin:
    return ret;
}

#undef CS_max


/*********************************************** mrConvolution1D */
/***
	1-dimensional convolution with given coeffs;
    if vert== 0: along X-axis; Y-axis otherwise.
    Updates min and max pix values
    Returns number of pixels or -1
***/
long  CZMemRect::mrConvolution1D (long hndl32, long *coeffs, long ncoeffs, long vert, long *pmin, long *pmax)
{
	long xe, ye, pitch, *mem, ret = -1;

	if (mrGetExtendedInfo (hndl32, &xe, &ye, &pitch, &mem) != 32)
    	goto Fin;
    if (vert)
    {
		ConvolY1D (mem, xe, ye, pitch, coeffs, ncoeffs, pmin, pmax);
    } else
    {
		ConvolX1D (mem, xe, ye, pitch, coeffs, ncoeffs, pmin, pmax);
    }
    if (*pmin <= *pmax) ret = xe * ye;
Fin:
	return ret;
}


/*********************************************** mrConvolution2D */
/***
	2-dimensional convolution with given matrix of coeffs;
    Updates min and max pix values
    Returns number of pixels or -1
***/
long  CZMemRect::mrConvolution2D(long hsrc32, long hdst32, long *coeffs, long xcoeffs, long ycoeffs, long *pmin, long *pmax)
{
	long xe1, ye1, pitch1, xe2, ye2, pitch2, *mem1, *mem2,
    	 ret = -1;

	if (mrGetExtendedInfo (hsrc32, &xe1, &ye1, &pitch1, &mem1) != 32)
    	goto Fin;
	if (mrGetExtendedInfo (hdst32, &xe2, &ye2, &pitch2, &mem2) != 32)
    	goto Fin;
    if ((xe1 != xe2) || (ye1 != ye2)) goto Fin;
	Convol2D (mem1, xe1, ye1, pitch1, mem2, pitch2,
                coeffs, xcoeffs, ycoeffs, pmin, pmax);
    if (*pmin <= *pmax) ret = xe1 * ye1;
Fin:
	return ret;
}


/********************************************** mrConvolution2Dpic */
/***
	2-dimensional convolution with matrix of coeffs given by a memrect;
    Updates min and max pix values
    Returns number of pixels or -1
***/
long  CZMemRect::mrConvolution2Dpic (long hsrc32, long hdst32,	long hcoeffs, long *pmin, long *pmax)
{
	long pixc, xc, yc, pitchc, *coeffs, hndl = 0, ret = -1;

	pixc = mrGetExtendedInfo (hcoeffs, &xc, &yc, &pitchc, &coeffs);
	if (pixc < 8) goto Fin;
	if ((pixc == 8) || (xc * 4 != pitchc))
    {
        hndl = mrCreate(32, xc, yc);
        if (mrConvert(hcoeffs, hndl, 0) <= 0) goto Fin;
        if (mrGetExtendedInfo (hndl, &xc, &yc, &pitchc, &coeffs) <= 0)
        	goto Fin;
    }
	ret = mrConvolution2D (hsrc32, hdst32, coeffs, xc, yc, pmin, pmax);
    if (hndl) Free(hndl);
Fin:
	return ret;
}


/*********************************************** mrConvolution3by3 */
/***
	Convolution with matrix 3*3 given by <coeffs>;
    Updates min and max pix values
    Returns number of pixels or -1
***/
long  CZMemRect::mrConvolution3by3(long hndl8, long hndl32, long *coeffs, long *pmin, long *pmax)
{
	long xe, ye, pitch8, xe32, ye32, pitch32,
    	 *mem32, ret = -1;
   	BYTE *mem8;

	if (mrGetExtendedInfo (hndl8, &xe, &ye, &pitch8, &mem8)
    	 != 8) goto Fin;
	if (mrGetExtendedInfo (hndl32, &xe32, &ye32, &pitch32, &mem32)
    	 != 32)	goto Fin;
    if ((xe != xe32) || (ye != ye32))	goto Fin;
    Convol3by3 (mem8, xe, ye, pitch8, mem32, pitch32, coeffs, pmin, pmax);
    ret = xe * ye;
Fin:
	return ret;
}


/*************************************************** mrCorrelation */
/***
// Calculates statistics for two 8 bit/pix Rectangles;
// First one is to rotate around point {xc1,yc1}; after
// rotation this point moves to {xc2,yc2};
// second rectangle as is.
// The procedure is controlled by Mask <m3> corresponding
// to the first rectangle; NULL is ok for <m3> (use all pixels)
// Results go to <data> (at least 6 doubles):
// data[0]=Number of used pixels;
// data[1]=Average1;   data[2]=Average2;
// data[3]=Sigma1;     data[4]=Sigma2;
// data[5]=Correlation Coefficient
// returned value: number of processed pixels (equal to data[0]);
//		-1 if something wrong
***/
long  CZMemRect::mrCorrelation (long hndl1, long hndl2, long hndl3, long xc1, long yc1, long xc2, long yc2, double angle, double *data)
{
	long pix1, xe1, ye1, pitch1, pix2, xe2, ye2, pitch2,
   		pix3, xe3, ye3, pitch3,
   		ret = -1;
   BYTE *mem1, *mem2, *mask;

	pix1 = mrGetExtendedInfo (hndl1, &xe1, &ye1, &pitch1, &mem1);
   if (pix1 != 8) goto Fin;
	pix2 = mrGetExtendedInfo (hndl2, &xe2, &ye2, &pitch2, &mem2);
   if ((pix2 != 8) || (xe2 != xe1) || (ye2 != ye1) ) goto Fin;
   if (hndl3)
   {
		pix3 = mrGetExtendedInfo (hndl3, &xe3, &ye3, &pitch3, &mask);
   	if ((pix3 != 1) || (xe3 < xe1) || (ye3 < ye1)) goto Fin;
   } else { mask = NULL;  pitch3 = 0; }
   ret = GrayCorrelation (mem1, xe1, ye1, pitch1,
						  mem2, xe2, ye2, pitch2, mask, pitch3,
                    xc1, yc1, xc2, yc2, angle, data);
Fin:
	return ret;
}


/*********************************************** mrCreateRoundPitch */
/***
    PitchRound:
    	1 - byte round
    	2 - word round
    	4 - doubleword round (default)
     	8 - quadword round
   Returns handle (long > 0) or -1 if failure
***/
long  CZMemRect::mrCreateRoundPitch(long BitsPerPixel, long Xe, long Ye, long PitchRound)
{
   long hndl;

   hndl = MakeNew (BitsPerPixel, Xe, Ye, PitchRound);
   if (!hndl) hndl = -1;
   return hndl;
}


/************************************************ mrCrossHistogram */
/***
 Fills cross-histogram of byte values; if hmask>0 - only for pixels
 corresponding to ones in the mask.
 <hist> must be 32 bit/pixel.

 returns: max value in the cross-histogram if OK,
          -1 otherwise

***/
long  CZMemRect::mrCrossHistogram (long h1, long h2, long hist, long hmask)
{
	long xe1, ye1, pitch1, xe2, ye2, pitch2,
    	 xehist, yehist, pitchhist, x, y,
         div1 = 256, div2 = 256, ret = -1;
    BYTE *mem1, *mem2, *r1, *r2, *memhist;
    DWORD pix1, pix2, hc, *rowhist;

	mrConstPixelwise (0, "Copy", hist);
	if (mrGetExtendedInfo (h1, &xe1, &ye1, &pitch1, &mem1) != 8)
    	goto Fin;
	if (mrGetExtendedInfo (h2, &xe2, &ye2, &pitch2, &mem2) != 8)
    	goto Fin;
    if ((xe1 != xe2) || (ye1 != ye2)) goto Fin;
	if (mrGetExtendedInfo (hist, &xehist, &yehist, &pitchhist, &memhist) != 32)
    	goto Fin;
    if (hmask)
    {
    	if (mrGetRectInfo (hmask, &xe2, &ye2) != 1) goto Fin;
        if ((xe1 != xe2) || (ye1 != ye2)) goto Fin;
        x = y = ret = 0;
   	    while (mrFindNext (hmask, &x, &y, 1) > 0)
    	{
            r1 = mem1 + (pitch1 * y);
            r2 = mem2 + (pitch2 * y);
            pix1 = r1[x] * xehist / div1;
            pix2 = r2[x] * yehist / div2;
            rowhist = (DWORD *)(memhist + pitchhist * pix2);
            if ((long)(hc = ++rowhist[pix1]) > ret) ret = hc;
            x++;
        }
   	} else
    {
        for (y = 0;  y < ye1;  y++)
        {
            for (x = 0;  x < xe1;  x++)
            {
	            pix1 = mem1[x] * xehist / div1;
    	        pix2 = mem2[x] * yehist / div2;
                rowhist = (DWORD *)(memhist + pitchhist * pix2);
                if ((long)(hc = ++rowhist[pix1]) > ret) ret = hc;
            }
            mem1 += pitch1;  mem2 += pitch2;
        }
    }
Fin:
	return ret;
}


/**************************************************** mrZoomMask */
/***
 zooms <from> mask rectangle into <to> one by
 replication of bits
		ye2 if Ok,
		-1 otherwise
***/
long  CZMemRect::mrZoomMask(long hndlfrom, long hndlto)
{
	long pix1, xe1, ye1, pitch1, pix2, xe2, ye2, pitch2,
    	 i, k, n, ret = -1;
   BYTE *mem1, *mem2, *src, *first;

	pix1 = mrGetExtendedInfo (hndlfrom, &xe1, &ye1, &pitch1, &mem1);
	pix2 = mrGetExtendedInfo (hndlto, &xe2, &ye2, &pitch2, &mem2);
   if ((pix1 != 1) || (pix2 != 1)) goto Fin;
   i = 0;  k = -1;
   while (i < ye2)
   {
     	n = i * ye1 / ye2;
      if (n != k)
      {   // another source line
         src = mem1 + (pitch1 * n);
			if (!BitRowToRow ((char *)src, xe1, (char *)mem2, xe2))
            	break;
         first = mem2;  k = n;
      } else
      {
        	MovMemory(first, mem2, pitch2);
      }
      mem2 += pitch2;  i++;
   }
   if (i == ye2) ret = ye2;
Fin:
	return ret;
}


/********************************************** mrCrossHistInvMask */
/***
 Fills cross-histogram of byte values; if hmask>0 - only for pixels
 corresponding to ones in the mask.
 <crosshistmask> must be 1*256*256.

 returns: max value in the cross-histogram if OK,
          -1 otherwise

***/
long  CZMemRect::mrCrossHistInvMask (long h1, long h2, long resmask, long crosshistmask)
{
	long xe1, ye1, pitch1, xe2, ye2, pitch2,
    	 pitchhist, pitchres, x, y,
         histmask = 0, ret = -1;
    BYTE *mem1, *mem2, *r1, *r2, *memhist, *memres;
    DWORD pix1, pix2;

	if (mrGetExtendedInfo (h1, &xe1, &ye1, &pitch1, &mem1) != 8)
    	goto Fin;
	if (mrGetExtendedInfo (h2, &xe2, &ye2, &pitch2, &mem2) != 8)
    	goto Fin;
    if ((xe1 != xe2) || (ye1 != ye2)) goto Fin;
	pix1 = mrGetExtendedInfo (resmask, &xe2, &ye2, &pitchres, &memres);
    if ((pix1 != 1) ||(xe1 != xe2) || (ye1 != ye2)) goto Fin;
	if (mrGetRectInfo (crosshistmask, &xe2, &ye2) != 1) goto Fin;
    if ((xe2 == 256) && (ye2 == 256))
    {
    	histmask = crosshistmask;
    } else
    {
     	if ((histmask = mrCreate(1, 256, 256)) < 1) goto Fin;
        mrZoomMask(crosshistmask, histmask);
    }
    mrGetExtendedInfo (histmask, &xe2, &ye2, &pitchhist, &memhist);
	mrOperMask (resmask, "Zeros");
    ret = 0;
    for (y = 0;  y < ye1;  y++)
    {
        for (x = 0;  x < xe1;  x++)
        {
            pix1 = mem1[x];
  	        pix2 = mem2[x];
            //if (mrGetPixel(histmask,pix1,pix2))
            r1 = memhist + ((pitchhist * pix2) + (pix1 >> 3));
            if ((1 << (pix1 & 7)) & (*r1))
            { // mrPutPixel (resmask, x, y, 1);
            	r2 = memres + ((pitchres * y) + (x >> 3));
                *r2 |= (1 << (x & 7));
                ret++;
            }
        }
        mem1 += pitch1;  mem2 += pitch2;
    }
Fin:
	if((histmask > 0) && (histmask != crosshistmask)) Free(histmask);
	return ret;
}


/********************************************************** mrZoom */
/***
 zooms <from> memory rectangle into <to> one, according
  first 3 bytes of <way> parameter (case insensitive):
 	"Int" : interpolation
 	"Sub" : subsampling
   "Rep" : replication
 e.g.
 	if ( mrZoom (Picture, Icon, "Subsampling") > 0) ...
 good pixel sizes: 1, 8, 24.
 returns
		ye2 if Ok,
		-1 otherwise
***/
static char *ZoomOps = "INT SUB REP ";

long  CZMemRect::mrZoom(long hndlfrom, long hndlto, char *way)
{
	long pix1, xe1, ye1, pitch1, pix2, xe2, ye2, pitch2, portion,
    	  ret = -1;
   	BYTE *mem1, *mem2;
   	DWORD opcase, offs = 0;

	pix1 = mrGetExtendedInfo (hndlfrom, &xe1, &ye1, &pitch1, &mem1);
	pix2 = mrGetExtendedInfo (hndlto, &xe2, &ye2, &pitch2, &mem2);
	if ((pix1 < 1) || (pix2 < 1) || (pix1 != pix2)) goto Fin;
   	if (pix1 == 1) { ret = mrZoomMask(hndlfrom, hndlto);  goto Fin; }
   	opcase = WordInText (ZoomOps, &offs, 0, way, 3);
   	if (! opcase) goto Fin;
   	if ((opcase == 1) && (pix1 == 8))
   	{
   		ret = ZoomRectSmooth (
               mem1, xe1, ye1, pitch1,
               mem2, xe2, ye2, pitch2);

   	} else
   	{
    	portion = pix1 >> 3;
   		ret = ZoomRectFast(
               mem1, xe1, ye1, pitch1,
               mem2, xe2, ye2, pitch2, portion);
   	}

Fin:
   	if (ret == 0) ret = -1;
	return ret;
}


/**************************************************** mrDistToMask */
/***
	returns:
		square of the distance or -1 if empty;
      updates xp, yp
***/
long  CZMemRect::mrDistToMask (long hndl,	long *px, long *py)
{
	long pix, xe, ye, pitch, ret = -1;
   	BYTE *mem;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
   	if (pix == 1)
   	{
		ret = MaskDistToPoint (mem, 0, 0, xe, ye, pitch, px, py);
   	}
	return ret;
}


/******************************************************* mrEllipse */
/***
    draw ellipse:
        x = xc + a * cos (t + phi)
        y = yc + b * sin (t + phi)
    where  (0 <= t < 2 * pi);

    hndl - rectangle to draw ellipse on
    xc, yc - coordinates of the ellipse's center
    a, b - main half-axes
    phi - angle (in radian) between ellipse's a-axis and x-axis
    color - value of the pixel to draw

	returns: number of written pixels
***/
long  CZMemRect::mrEllipse(long hndl, long xc, long yc,
			 double a, double b, double phi,
             long color)
{
	long xstart, ystart, m, limit,
    	 n = 0;
    BYTE *code;

    limit = 4 * (a + b);
    code = (BYTE *)My_malloc(limit + 1);
    if (code)
    {
    	m = CodeEllipse(code, limit, a, b, phi, &xstart, &ystart);
	    xstart += xc;   ystart += yc;
		n = mrMaskDrawCode (hndl, &xstart, &ystart, code, m, color);
    	My_free(code);
    }
    return n;
}


/*************************************************** mrFillMaskTab */

void  CZMemRect::mrFillMaskTab (long zerone, char *tabdescr, BYTE *tab)
{
    DWORD offs, limit, wlen;
 	long whatset;

    offs = 0;  limit = strlen(tabdescr);
 	whatset = zerone & 1;
    SetIdentityTable (tab);

	while ((wlen = GetTheWord (tabdescr, &offs, limit)) > 0)
    {
     	if ((wlen & 0xFE) == 8)
        {	// wlen == 8 or 9
	    	ChangeTableAscii (tab, whatset, tabdescr + offs);
        }
        offs += wlen;
    }
}


// New Code
/********************************************************* mrSubst */
/***
 	Substitutes 8-bits or 16bits pixels in a given memory rectangle
 	with bytes from a given table (table must match image pixel size)

	Returns:
		new sum of pixels in the mem. rect. if OK,
      -1 otherwise
***/
long  CZMemRect::mrSubst (long hndl, void *table)
{
	long pix, xe, ye, pitch, i, k, ret = -1;
	BYTE c, *mem, *t, *row;
	WORD *w, *wrow;


	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);

   	if (pix == 8) {
   		t = (BYTE *)table;
		i = ret = 0;
   		while (i++ < ye) {
      		        row = mem;
			mem += pitch;
     		        for (k = 0;  k < xe;  k++) {
   				c = t[row[k]];
        		        ret += (row[k] = c);
      		        }
                }
   	}
        else
         if (pix == 16) {

   		w = (WORD *)table; // 128K
		i = ret = 0;
   		while (i++ < ye) {
      		        wrow = (WORD*) mem;
			mem += pitch;
     		        for (k = 0;  k < xe;  k++) {
   				c = w[wrow[k]];
        		        ret += (wrow[k] = c);
                        }
      		}
 	}

	return ret;
}



/********************************************************* mrGamma */
/***
    For 8-bits pixels:
 		substitutes pixel value in a given memory rectangle
 		according to the formula:
    		newval = 255 * (oldval / 255) ** gamma
    	note: 0 and 255 are the fixed points of the transformation.
    For 32-bits pixels:
 		substitutes pixel value in a given memory rectangle
 		according to the formula:
    		newval = oldval ** gamma
	Returns:
		new sum of pixels in the mem. rect. if OK,
      	-1 otherwise
***/
long  CZMemRect::mrGamma (long hndl, double gamma)
{
    long pix, xe, ye, pitch, ret = -1;
   	BYTE *mem, lut[256];

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
    if (pix == 8)
    {
    	Gamma8Table(lut, gamma);
		ret = mrSubst (hndl, lut);
    }
    if (pix == 32)
    {
		ret = GammaMem32 (mem, xe, ye, pitch, mem, pitch, gamma);
    }
    return ret;
}


/****************************************************** mrGetLabel */
/***
    Returns label's length
    (from 0 to MemRectLabelSize) if OK
     or -1 if failure
***/
long  CZMemRect::mrGetLabel(long hndl, char *mem)
{
	long k = 0;
   char c;
   MemRectangle *mr;

   mr = ActiveEntry(hndl);
   if (mr)
   {
     	while (k < MemRectLabelSize)
     	{
        	if ((c = mr->Label[k]) < ' ') break;
        	mem[k++] = c;
     	}
     	mem[k] = 0;
   } else k = -1;
	return k;
}


/************************************************* mrGetOverlayPos */
/***
 hndl must be valid; updates Xoffs, Yoffs
 returns:
 		handle of sublay MR if the rectangle is
        of Ovarlay type,
     	0 otherwise
***/
long  CZMemRect::mrGetOverlayPos (long hndl, long *Xoffs, long *Yoffs)
{
	long subhndl = 0;
   MemRectangle *mr;

   mr = ActiveEntry(hndl);
   if (mr)
	{
      if (mr->Memorysize == MemRectIsOverlay)
      {
		    subhndl = mr->Memory;
		    *Xoffs = mr->Xoffset;
		    *Yoffs = mr->Yoffset;
      }
   }
   return subhndl;
}


/**************************************************** mrHandleInfo */
/***
   fills <mem> with ASCII info about given handle;
 	returns text size
typedef struct{
   DWORD Pixelsize; // pixel size in bits; 0 means FREE
   DWORD OriginalPixelsize;		// original pixel size in bits, useful when camera image bitdepth is less than storage bitdepth
								// eg for 10, 12, 14 bit images (OriginalPixelsize) the storage bitdepth is 16 bit (Pixelsize) 
   DWORD Xext;     	// width in pixels
   DWORD Yext;     	// height in pixels
   DWORD Pitch;     // pitch in bytes
   DWORD Memory;  	// pointer to image data;
   					// may be aligned on 8 byte boundary (see BaseMemory);
   					// for MemRectIsExtern - pointer to Extern memory
   					// for MemRectIsOverlay - handle of sublaying
   DWORD Memorysize;	// in bytes;
   					// values MemRectIsExtern and MemRectIsOverlay
                    // are possible
   DWORD Xoffset;   // horizintal offset for Overlays
   DWORD Yoffset;   // vertical offset for Overlays
   DWORD Tag;    	// working var; for free entry
   	  				//	references to next free, or equal to 0
   char Label[MemRectLabelSize+4];	// working short text
   DWORD BaseMemory; // pointer from malloc(); 0 for extern/overlay rects
}  MemRectangle;
***/
// if ((!mem)||(!memlim)) : MessageBox(NULL, D, "mrHandleInfo", MB_SYSTEMMODAL | MB_OK);

long  CZMemRect::mrHandleInfo (long hndl, char *mem, long memlim)
{
	long k;
   	DWORD msz;
   	MemRectangle *mr;

   	k = sprintf(D,"#%li: ",hndl);
   	mr = GetEntry(hndl);
   	if (!mr)
   	{
		k += sprintf(D + k,"is not a handle");
      	goto Fin;
   	}
   	k += sprintf(D + k,"%li*%li*%li [%li] (%li) Mem: ",
		mr->Pixelsize, mr->Xext, mr->Yext, mr->OriginalPixelsize, mr->Pitch);
   	msz = mr->Memorysize;
   	switch ( msz )
   	{
  		case MemRectIsOverlay :
      		k += sprintf(D + k,"overlay %lu at (%lu, %lu) ",
         		mr->Memory, mr->Xoffset, mr->Yoffset);
      		break;
  		case MemRectIsExtern :
        	k += sprintf(D + k,"extern %.8lx ", mr->Memory);
      		break;
  		default :
      		k += sprintf(D + k," %.8lx ", mr->Memory);
	}
    if (mr->Tag)
	   	k += sprintf(D + k,"  tag: %li; ", mr->Tag);
    if (mr->Label[0])
   		k += sprintf(D + k," label: <%s> ", mr->Label);
Fin:
	if ((mem) && (memlim))
    {
   		if (k >= memlim) k = memlim - 1;
   		MovMemory (D, mem, k);  mem[k] = 0;
	} else
    {
    	MessageBox(NULL, D, "mrHandleInfo", MB_SYSTEMMODAL | MB_OK);
    }
   	return k;
}


/************************************* mrHistogram, mrHistogramLim */
/***
 fills histogram of byte values (DWORD array)
 returns: number of pixels in the whole rectangle
  if bad - returns	-1
***/

//==============================
long  CZMemRect::mrHistogramLim (long hndl, void *res, long reslim)
{
	long xe, ye, pitch, i, sum, ret = -1;	//  wait worse
   	DWORD hist[256], *r;
   	BYTE *mem;

	if (mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem) != 8)
    	goto Fin;
    sum = ret = xe * ye;   r = (DWORD *)res;
    if (reslim >= 256)
   	{
	  	PictHist (mem, xe, ye, pitch, r);
	} else
    {
	  	PictHist (mem, xe, ye, pitch, hist);
    	for (i = 0;  i < reslim;  i++) sum -= (r[i] = hist[i]);
        r[reslim - 1] += sum;
    }
Fin:
	return ret;
}


long  CZMemRect::mrHistogram (long hndl, void *res)
{
	return mrHistogramLim (hndl, res, 256);
}




/*********************************************** mrBitmapToMemRect */
/***
 The function creates Mem Rect from given object of TBitmap type
 Returns:
 		(created) handle if OK,
     	0 otherwise
enum TPixelFormat {
 pfDevice, pf1bit, pf4bit, pf8bit, pf15bit, pf16bit, pf24bit, pf32bit, pfCustom
};
***/

DWORD  CZMemRect::mrBitmapToMemRect (HBITMAP hbmp, long invrows)
{
 	long pix, xe, ye, xx, yy, yind, xe1, ye1, rez, pitch;//, pitch1;
    DWORD hndl = 0;
	DWORD hndl1 = 0;
    BYTE *mem;//, *mem1, *sline;
	BITMAP bmp;
	HDC hdc;
	BITMAPINFOHEADER bmpiheader;
	RGBQUAD rgbq;

	BITMAPINFO bmpinfo;

	if (GetObject(hbmp, sizeof(bmp), &bmp) == 0)
		return 0;


	hdc=GetDC(NULL);
    switch (bmp.bmBitsPixel)
    {
      	case 1:  pix = 1;  break;
      	case 8:  pix = 8;  break;
      	case 24: pix = 24; break;
      	case 32: pix = 32; break;
  		default : pix = 24;
	}
    xe = bmp.bmWidth;
    ye = bmp.bmHeight;
    hndl = mrCreate(pix, xe, ye);
	
	mrGetExtendedInfo (hndl, &xe1, &ye1, &pitch, &mem);

    //xx = (pix * xe + 7) >> 3;

	bmpiheader.biSize=sizeof(BITMAPINFOHEADER);
	bmpiheader.biWidth = xe;
	bmpiheader.biHeight = ye;
	bmpiheader.biPlanes = 1;
	bmpiheader.biBitCount = bmp.bmBitsPixel;
	bmpiheader.biCompression = BI_RGB;
	bmpiheader.biSizeImage = 0;

	rgbq.rgbBlue=255;
	rgbq.rgbGreen=255;
	rgbq.rgbRed=255;

	//bmpinfo.bmiColors = NULL;
	bmpinfo.bmiHeader = bmpiheader;
 
	xx = xe >> 3 ;
	yy = ye;
	if (invrows)
	{
		//inverse here
		while (yy--)
		{
			yind = ye - 1 - yy;
			rez=GetDIBits(hdc, hbmp, yind, 1, mem, &bmpinfo, DIB_RGB_COLORS);
			
			mem += pitch;
		}

	}
	else
	{
		while (yy--)
		{
			yind = yy;
			rez=GetDIBits(hdc, hbmp, yind, 1, mem, &bmpinfo, DIB_RGB_COLORS);

			mem += pitch;
		}
	}

	
	rez=ReleaseDC(NULL, hdc);
	return hndl;
}


/************************************************* mrHistogramMask */
/***
 Fills histogram of byte values (array of <histlen> DWORDs) for pixels
 corresponding to ones in the mask;
 returns: number of ones in the mask

 ############ to be optimized!
***/
long  CZMemRect::mrHistogramMask (long hpic, long hmask, long *hist, long histlen)
{
   	long x, y, ret;
   	DWORD ih, hlen;

   	x = y = ret = 0;  hlen = histlen;
	for (ih = 0;  ih < hlen;  ih++) hist[ih] = 0;
   	while (mrFindNext (hmask, &x, &y, 1) > 0)
	{
        ih = mrGetPixel (hpic, x, y);
		if (ih >= hlen) ih = hlen - 1;
        hist[ih]++;  x++;  ret++;
   	}
	return ret;
}


/************************************************** mrHistogramSub */
/***
 fills histogram of (byte|DWORD) values (DWORD array)
 returns: sum of pixels in subsample
  if bad - returns	-1
***/
long  CZMemRect::mrHistogramSub (long hndl,
			long xl, long yt, long xsub, long ysub,
         	long xstep, long ystep,
            void *histmem, long histmax)
{
	long pix, xe, ye, pitch, ix, iy, xfin,
    	 ret = -1;	//  wait worse
   	DWORD ih, indlim, *hist, *mem32;
   	BYTE *mem;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
	if ((pix != 8) & (pix != 32)) goto Fin;  // bad pixel size
   	if ((xl < 0) || (xl + xsub > xe) || (yt < 0)
        || (yt + ysub > ye) || (histmax < 2)) goto Fin;
   	ret = 0;  hist = (DWORD *)histmem;
    indlim = histmax - 1;  xfin = xl + xsub;
   	mem += (yt * pitch);  pitch *= ystep;
	for (ih = 0;  ih <= indlim;  ih++) hist[ih] = 0;
	for (iy = 0;  iy < ysub;  iy += ystep)
	{
        mem32 = (DWORD *)mem;
		for (ix = xl;  ix < xfin;  ix += xstep)
      	{
        	ih = (pix == 8) ? mem[ix] : mem32[ix];
         	ret += ih;  // sum before limitation
            if (ih > indlim) ih = indlim;
            hist[ih]++;
      	}
		mem += pitch;
	}
Fin:
	return ret;
}


/*********************************************** mrHistogramXarray */
/***
		This function calculates histogram of DWORD pixel values
        and sum of pixel values. Xarray must be sorted in ascending
        order. For i=0, ... XHlen - 1:
        	Hist[i] = number of pixel values:
              { Xarray[i-1] < Pix <= Xarray[i] }
            where  Xarray[-1] = -1
	Returns:
		Sum of pixel values if OK,
        -1 otherwise
***/
long  CZMemRect::mrHistogramXarray (long h32, long *xarray, long *hist, long histlen)
{
   	long xe, ye, pitch, *mem, ret = -1;

	if (mrGetExtendedInfo (h32, &xe, &ye, &pitch, &mem) == 32)
	{
        ret = PictHistXarray ((DWORD *)mem, xe, ye, pitch,
        			(DWORD *)xarray, (DWORD *)hist, histlen);
    }
	return ret;
}


/****************************************************** mrHSItoRGB */
/***
	Inversion for mrRGBtoHSI.
    Transforms Hue, Saturation, Intensity color components
    (normalized to the range {0-255}) into Red, Green, Blue.
    Hue values are about:
    0 - Cyan, 42 = Blue, 85 - Magenta, 127 - Red,
    170 - Yellow, 212 - Green, 255 - Cyan again
    If ANGLE corresponds to Hue, then
    Norm = (2/3)*Saturation / ( Abs(cos(ANGLE)) + sqr(3)*Abs(sin(ANGLE)) );
    Cmax = I + Norm * Abs(cos(ANGLE));
    Cmin = I * (1 - Saturation);
    Cmid = 3*I - Cmin - Cmax;
    Returns maximal result Intensity ((r+g+b)/3) if OK,
    		-1 otherwise
***/
long  CZMemRect::mrHSItoRGB(long H, long S, long I, long R, long G, long B)
{
	long pix, xe, ye, xew, yew;
	long ix, iy, rv, gv, bv, hv, sv, iv;
    long Rpitch, Gpitch, Bpitch, Hpitch, Spitch, Ipitch;
    long ret = -1;
   	BYTE *Rp, *Gp, *Bp, *Hp, *Sp, *Ip;

	pix = mrGetExtendedInfo (H, &xe, &ye, &Hpitch, &Hp);
 	if (pix != 8) goto Fin;
	pix = mrGetExtendedInfo (S, &xew, &yew, &Spitch, &Sp);
 	if ((pix != 8) || (xew != xe) || (yew != ye)) goto Fin;
	pix = mrGetExtendedInfo (I, &xew, &yew, &Ipitch, &Ip);
 	if ((pix != 8) || (xew != xe) || (yew != ye)) goto Fin;
	pix = mrGetExtendedInfo (R, &xe, &ye, &Rpitch, &Rp);
 	if ((pix != 8) || (xew != xe) || (yew != ye)) goto Fin;
	pix = mrGetExtendedInfo (G, &xew, &yew, &Gpitch, &Gp);
 	if ((pix != 8) || (xew != xe) || (yew != ye)) goto Fin;
	pix = mrGetExtendedInfo (B, &xew, &yew, &Bpitch, &Bp);
 	if ((pix != 8) || (xew != xe) || (yew != ye)) goto Fin;
    for (iy = 0;  iy < ye;  iy++)
    {
	    for (ix = 0;  ix < xe;  ix++)
    	{
            hv = Hp[ix];  sv = Sp[ix];  iv = Ip[ix];
			HSItoRGB8(hv, sv, iv, &rv, &gv, &bv);
            Rp[ix] = rv;  Gp[ix] = gv;  Bp[ix] = bv;
            if (ret < (rv + gv + bv)) ret = (rv + gv + bv);
        }
        Hp += Hpitch;  Sp += Spitch;  Ip += Ipitch;
        Rp += Rpitch;  Gp += Gpitch;  Bp += Bpitch;
    }
    ret = (ret + 2) / 3;
Fin:
    return ret;
}


/********************************************* mrInverseColumns */
/***
  the function inverses the order of columns
 returns:
 		0 if OK,
     	-1 otherwise
***/
long  CZMemRect::mrInverseColumns (long hndl)
{
	long pix, xe, ye, pitch, n,
    	k = 0, invbytes = 0, ret = -1;
	char *row;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &row);
   if (pix < 1) goto Fin;
   if (pix == 1) invbytes = 1;
   n = (xe * pix + 7) >> 3;	// byte counter
   while (k++ < ye)
   {
      LeftToRight (row, row, n, invbytes);
     	row += pitch;
   }
   ret = 0;
Fin:
	return ret;
}


/*************************************************** mrInverseRows */
/***
  the function inverses the order of rows
 returns:
 		0 if OK,
     	-1 otherwise
***/
long  CZMemRect::mrInverseRows(long hndl)
{
	long pix, xe, ye, pitch, n, i, k,
    	 ret = -1;
	BYTE c, *low, *high;;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &low);
	if (pix < 1) goto Fin;
	high = low + (ye * pitch);
   	k = ye / 2;		// rows counter
   	n = (xe * pix + 7) >> 3;	// byte counter
   	while (k--)
   	{
   		high -= pitch;
     	for (i = 0;  i < n;  i++)
     	{
     		c = high[i];  high[i] = low[i];  low[i] = c;
		}
     	low += pitch;
   	}
   	ret = 0;
Fin:
	return ret;
}


/************************************************** mrIplConvolSep */
long  CZMemRect::mrIplConvolSep (long hndl,
               long xn, long xc, long *xcoeffs, long xrshift,
               long yn, long yc, long *ycoeffs, long yrshift)
{
	long xe, ye, pitch, ret = -1;
   BYTE *mem;

	if (mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem) == 8)
	{
      ret = iiplConvSep2D (mem, xe, ye, pitch,
               				xn, xc, xcoeffs, xrshift,
               				yn, yc, ycoeffs, yrshift);
   }
	return ret;
}


/************************************************ mrIplConvolution */
long  CZMemRect::mrIplConvolution (long hndl,
               long xf, long yf, long xc, long yc,
               long *coeffs, long rshift)
{
	long xe, ye, pitch, ret = -1;
   BYTE *mem;

	if (mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem) == 8)
	{
      ret = iiplConv2D (mem, xe, ye, pitch,
               		xf, yf, xc, yc, coeffs, rshift);
   }
	return ret;
}


/***************************************************** mrIplFilter */
static char *FilterNames = "MIN MAX MEDIAN BLUR ";
static DWORD FilterConsts[] =
 {
   mnmMIN, mnmMAX, mnmMED, mnmBLUR
 };

long  CZMemRect::mrIplFilter (long hndl, char *filtname, long xf, long yf, long xc, long yc)
{
	long xe, ye, pitch, oper, ret = -1;
   DWORD opcase, offs = 0;
   BYTE *mem;

   opcase = WordInText (FilterNames, &offs, 0, filtname, 0);
   if (! opcase) goto Fin;
	if (mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem) != 8)
   	 goto Fin;
   oper = FilterConsts[opcase-1];
   ret = iiplFilter (mem, xe, ye, pitch,
               		xf, yf, xc, yc, oper);
Fin:
	return ret;
}


/************************************************* mrLabelToHandle */
/***
    Returns handle of the first rectangle with given label
    if OK, or -1 if no
***/
long  CZMemRect::mrLabelToHandle(char *labeltext)
{
	long ret = -1;
   	MemRectangle *mr;
   	DWORD hndl = 0;

   	if (!strlen(labeltext)) goto Fin;
   	while (hndl++ < NumberOfMemRects)
   	{
   		mr = ActiveEntry(hndl);
   		if (mr)
   		{
         	if (stricmp(mr->Label, labeltext) == 0)
         	{	// not case sensitive
            	ret = hndl;  break;
     		}
   		}
   	}
Fin:
	return ret;
}


/****************************************************** mrMaskLine */
/***
		<option> :
		 "Line"			// polyline from {data[0], data[1]}
						// to {data[2], data[3]}, then
						// to {data[4], data[5]}, and so on
         "SplClosed"	// closed spline
         "SplFree"  	// unclosed spline with free ends
         "SplStrF"		// unclosed spline with straight first interval
         "SplStrL"		// unclosed spline with straight last interval

		 "Circle"   	// circle with center in {data[0], data[1]}
                        // and radius= data[2]
		 "Arc"     		// arc with center in {data[0], data[1]}
                        // from {data[2], data[3]} to {data[4], data[5]}
                        // in clockwise direction

      <color> (if 1 bit/pix) - 0, 1 - put 0/1 pixel; >1 - inverse the pixel.
	  <count> - number of sections for polyline/spline

	returns  -1 if something wrong
***/
static char *MaskLineOps = "Line SplClosed SplFree SplStrF SplStrL Circle Arc";
static long MaskLineConsts[] =
	{mlPOLY, mlSPLINE0, mlSPLINE1, mlSPLINE2, mlSPLINE3, mlCIRCLE, mlARC};

long  CZMemRect::mrMaskLine (long hndl, char *option, long color,	long nsects, long *data)
{
	long linemode, pix, xe, ye, pitch, pen, mask, ret = -1;
   	DWORD opcase, offs = 0;
    BYTE *mem;

   	opcase = WordInText (MaskLineOps, &offs, 0, option, 0);
   	if (!opcase) goto Fin;
  	linemode = MaskLineConsts [opcase - 1];
	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
   	if (pix < 1) goto Fin;
   	if (pix == 1)
   	{
    	mask = 0;  pen = color;
   	} else
   	{ 	// 8/24/32 bit
      	mask = mrCreateAsWithPix(hndl, 1);
    	if (mask < 1) goto Fin;
      	mrGetExtendedInfo (mask, &xe, &ye, &pitch, &mem);
	  	pen = 1;
   	}
	ret = MaskLine (mem, xe, ye, pitch,
                linemode, pen, nsects, data);
   	if (mask > 0)
   	{
		ConstPixelMask (color, "Copy", hndl, mask, 0);
      	Free(mask);
   	}
Fin:
	return ret;
}


/************************************************ mrLineEndsInMask */
/***
	returns:
      number of pixels in the line and mask crossing;
      -1 if error
***/
long  CZMemRect::mrLineEndsInMask (long mask, long *px1, long *py1, long *px2, long *py2)
{
    long xe, ye, x1, y1, x2, y2, dx, dy, dd,
        wmask = 0, ret = -1,
        wrk[mrStat1DataLength];
    long wrkmaskstat [mrStat1DataLength];

    if (mrGetRectInfo(mask, &xe, &ye) != 1) goto Fin;  // not a mask
    if ((wmask = mrCreateAs(mask)) < 1) goto Fin;
    x1 = *px1;  y1 = *py1;  x2 = *px2;  y2 = *py2;
    if (x2 < x1)
    {
        x1 = *px2;  y1 = *py2;  x2 = *px1;  y2 = *py1;
    }
    dx = x2 - x1;  dy = y2 - y1;  dd = dx * dy;

    if (dy >= 0)
    {
        while ((x2 < xe) && (y2 < ye)) { x2 += dx;  y2 += dy; }
        while ((x1 > 0) && (y1 > 0)) { x1 -= dx;  y1 -= dy; }
    } else
    {
        while ((x2 < xe) && (y2 > 0)) { x2 += dx;  y2 += dy; }
        while ((x1 > 0) && (y1 < ye)) { x1 -= dx;  y1 -= dy; }
    }
    wrk[0] = x1;  wrk[1] = y1;  wrk[2] = x2;  wrk[3] = y2;
    mrMaskLine (wmask, "Line", 1, 1, wrk);  // draw line on mask
    mrPixelwise (mask, "And", wmask);   // cut the line by mask
    ret = mrStat1 (wmask, wrkmaskstat);  if (ret < 1) goto Fin;
    *px1 = (long) wrkmaskstat[6];
    *px2 = (long) wrkmaskstat[7]; // Min(X), Max(X)
    if (dd >= 0)
    {
        *py1 = (long) wrkmaskstat[8];
        *py2 = (long) wrkmaskstat[9];
    } // Min(Y), Max(y):
    else
    {
        *py1 = (long) wrkmaskstat[9];
        *py2 = (long) wrkmaskstat[8];
    }         // Max(Y), Min(y):
Fin:
    if (wmask > 0) Free(wmask);
	return ret;
}


/*************************************************** mrLinkBorders */
/***
    Calculates the mask of borders separating linked components
    in source mask.
    Doesn't clear <hborders>, but OR-s new pixels there;
    <nei>: "N8"(default), "N4", "N84" "N48"
 	Returns:
   	 number of linked components / -1

***/
long  CZMemRect::mrLinkBorders (long hsource, long hborders, char *nei)
{
    long i,	one = 1, lastx = 0, lasty = 0,
    	compnum = 0, comps = 0, hseed = 0, hlimit = 0,
    	maxlen = 8000, *coord;

//    mrOperMask (hborders, "Zeros");
    coord = (long *)My_malloc(maxlen * 2 * sizeof(long));
    if (!coord) goto Fin;
    hseed = mrCreateAs (hsource);
    hlimit = mrCopyAs (hsource);
    // fill array of coordinates of components:
    while (compnum < maxlen * 2)
	{
    	if (mrFindNext (hlimit, &lastx, &lasty, 1) <= 0)
        	break; // no more
        coord[compnum++] = lastx;   coord[compnum++] = lasty;
        mrReconFast (hlimit, hseed, lastx, lasty);
        Bitwise (hseed, "Less", hlimit);
    }
	if ((comps = compnum / 2) == 0) goto Fin;
    // Log loop first:
	while (one < comps)
    {
        mrMove (hsource, hlimit);
        mrOperMask (hseed, "Zeros");
        for (i = 0;  i < comps; i++)
        {
        	if (i & one)
            {
       		    mrPutPixel (hseed,
                	coord[2 * i], coord[2 * i + 1], 1);
            }
        }
        mrReconstruct (hlimit, hseed);
        Bitwise (hseed, "Less", hlimit);
		// find border for 2 masks:
		mrBorder2Masks (hlimit, hseed, hborders, nei);
    	one <<= 1;
    }
Fin:
    if (coord) My_free (coord);
    if (hseed) Free(hseed);
    if (hlimit) Free(hlimit);
	return comps;
}


/************************************************** mrListOfActive */
/***
   fills <mem> with active handles
 	returns the number of filled entries
***/
long  CZMemRect::mrListOfActive (long *mem, long limit)
{
	long ret = 0;
   	DWORD hndl = 0;

   	while ((hndl++ < NumberOfMemRects) && (ret < limit))
   	{
   		if (ActiveEntry(hndl))
    		mem[ret++] = hndl;
   	}
   	return ret;
}


/**************************************************** mrMakeExtern */
/***
    Returns handle (long > 0) or -1 if failure
    if pitch==0 - calculate as for regular rectangle
***/
long  CZMemRect::mrMakeExtern(void *rectmem, long OriginalBitsPerPixel, long Xe, long Ye, long pitch)
{
   	long hndl = -1;
   	MemRectangle *mr;

	// 10, 12, 14 bit images are stored in memory in 16 bits 
	long BitsPerPixel = (8 < OriginalBitsPerPixel && OriginalBitsPerPixel < 16)  ?  16 : OriginalBitsPerPixel;

   	if (((1L << BitsPerPixel) & AcceptablePixelSizes) == 0)
   	{
    	if ((BitsPerPixel != 32) && (BitsPerPixel != 64))
   	 		goto Fin;
   	}
   	// calculate memory parameters:
   	if (pitch == 0)
	   	pitch = ((BitsPerPixel * Xe + 31) >> 3) & 0xFFFFFFFC;	// bytes
   	// pointer to the current entry:
	hndl = GetNewHandle();
   	if (hndl == 0) goto Fin;
   	mr = GetEntry(hndl);
   	//	set members:
   	mr->Pixelsize = BitsPerPixel;
	mr->OriginalPixelsize = OriginalBitsPerPixel;
   	mr->Xext = Xe;
   	mr->Yext = Ye;
   	mr->Pitch = pitch;
   	mr->Memory = (DWORD)rectmem;
   	mr->Memorysize = MemRectIsExtern;
   	mr->Xoffset = mr->Yoffset = 0;
    mr->OwnOverList = mr->SubOverList = 0;
   	mr->Tag = 0;
   	mr->Label[0] = 0;
Fin:
	if (hndl <= 0) hndl = -1;
	return hndl;
}


/************************************************** mrMaskCalipers */
/***
	Calculates extends in 16 directions, finds extreme ones,
    calculetes 'centroid' coordinates
        calipers - array[16] to fill with min/max 'caliper' sizes
        			of mask ONEs in 16 directions:
                Angle[I] = Pi/2 - I*(Pi/16) rad	= 90 - I * 11.5 degrees
                	where I = 0...15
                	All projections are rounded to longs
        imin, imax - pointers to get directions of min and max calipers
        xc, yc - pointers to get X,Y coordinates of the centroid
        		(average of extreme points); NULL is ok

	Returns: number of ONEs in the mask if OK,
    		-1 otherwise
***/
long  CZMemRect::mrMaskCalipers (long hndl, long *calipers, long *imin, long *imax, long *xc, long *yc)
{
	long pix, xe, ye, pitch, ret = -1;
   	char *mem;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
   	if (pix == 1)
   	{
    	ret = MaskCalipers (mem, xe, ye, pitch,
                		calipers, imin, imax, xc, yc);
	}
	return ret;
}


/************************************************ mrMaskConvexHull */
/***
	This function expands mask to convex 32-oid with given thickness
	Returns: number of ONEs in the 32-oid if OK,
    		-1 otherwise
***/
long  CZMemRect::mrMaskConvexHull (long hndl, double thickness)
{
	long pix, xe, ye, pitch, pixpercentile, ret = -1;
   	char *mem;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
   	if (pix == 1)
   	{
        pixpercentile = thickness * 100.0;
    	ret = MaskConvex (mem, xe, ye, pitch, pixpercentile);
	}
	return ret;
}


/************************************************** mrMaskDistHist */
/***
    Calculates histogtam of distances from
    	given point to points of the mask.
 	Returns:
   	 length of the histogram without high zeros
     	(may be more than histlen)
     0 if empty mask

***/
long  CZMemRect::mrMaskDistHist (long hmask,	long cx, long cy, long *hist, long histlim)
{
    long i,	lastx, lasty, dx, dy, imax;

    lastx = lasty = imax = 0;
    for (i = 0;  i < histlim;  i++) hist[i] = 0;
    while (mrFindNext (hmask, &lastx, &lasty, 1) > 0)
	{
    	dx = lastx - cx;   dy = lasty - cy;
        i = (long)floor(sqrt((double)(dx * dx + dy * dy)) + 0.5);
        imax = max(imax, i + 1);	// return value
        i = min(i, histlim - 1);	// limit the index with histlen
        hist[i]++;	lastx++;
    }
	return imax;
}


/************************************************** mrMaskLinkInfo */
/***
    Fills data with information about linked components in source
    mask as an array of 7-plets of longs:
    	{Ni, Xi, Yi, Xmini, Ymini, Xmaxi, Ymaxi},
	    where
        	Ni - number of points in the component,
    		Xi,Yi - coordinates of some point belonging to
                    the i-th linked component,
    		Xmini, Ymini, Xmaxi, Ymaxi - min/max X,Y coordinates
        			of the i-th linked component.
    maxcomp - max number of components to fit in <data>
    The result array is sorted by Ni in decreased order.
 	Returns:
   	 number of linked components bigger than <minarea>
             (may be more than <maxcomp>),
     or -1

***/
long  CZMemRect::mrMaskLinkInfo (long hmask, void *data,	long maxcomp, long minarea)
{
    long i, ii,	k, lastx, lasty, hseed, hlimit, ones, onesmin,
         operation, elsize, sortoffset, portions, xc, yc,
         compnum = -1;
    long *coord, sizedata[mrMaskLinkInfoPortion],
         onesoffset = 0,
         xcycoffset = 1,
         minmaxoffset = 3;

    lastx = lasty = hseed = hlimit = 0;
    coord = (long *)data;
    if ((hseed = mrCreateAs (hmask)) < 1) goto Fin;
    if ((hlimit = mrCopyAs (hmask)) < 1) goto Fin;
    // fill array of coordinates of components:
    compnum = i = portions = 0;
    while (mrFindNext (hlimit, &lastx, &lasty, 1) > 0)
	{

        ones = mrReconFastD (hlimit, hseed, lastx, lasty,
        					 sizedata + minmaxoffset);
        Bitwise (hseed, "Less", hlimit);
        if (ones < minarea) continue;
        // set xc, yc to the center of the rectangle:
        // (xmin + xmax)/2, (ymin + ymax)/2:
        xc = (sizedata[minmaxoffset + 0] + sizedata[minmaxoffset + 2]) / 2;
        yc = (sizedata[minmaxoffset + 1] + sizedata[minmaxoffset + 3]) / 2;
        if (mrGetPixel (hseed, xc, yc) == 0) mrDistToMask (hseed, &xc, &yc);
        sizedata[xcycoffset] = xc;
        sizedata[xcycoffset + 1] = yc;
        sizedata[onesoffset] = ones;
//        Bitwise (hseed, "Less", hlimit);
//        if (ones < minarea) continue;
        compnum++;
        if ((!data) || (maxcomp < 1)) continue;
    	if (compnum <= maxcomp)
        {
            portions++;
        } else
        {
         	i = -1;  onesmin = ones;
            for (k = 0;  k < maxcomp;  k++)
            {
             	if (coord[mrMaskLinkInfoPortion * k + onesoffset] < onesmin)
                {
                    i = mrMaskLinkInfoPortion * k;
                 	onesmin = coord[i + onesoffset];
                }
            }
        }
        if (i >= 0)
        {
        	for (ii = 0;  ii < mrMaskLinkInfoPortion;  ii++)
            	coord[i + ii] = sizedata[ii];
            i += mrMaskLinkInfoPortion;
        }
    }
    if (portions > 1)
    {	// is sortable
	    elsize = mrMaskLinkInfoPortion * sizeof(long);
        sortoffset = onesoffset * sizeof(long);
        operation = (sortoffset << 8) | 0x84;   // 0x<00>84;
        SortInPlace (data, elsize, portions, operation);
    }
Fin:
    if (hseed > 0) Free(hseed);
    if (hlimit > 0) Free(hlimit);
	return compnum;
}


/************************************************* mrMaskQuantiles */
/***
 fills DWORD array <res>[numof] with "quantiles" of
 previous values (must go in increasing order):
 	res[i] = qi, where number of pixels in area
    of interest with values < qi is less than
    previous res[i], but number of pixels with
    values <= qi is more or equal to previous res[i];
 Works for area of interest given with ones in <hmask>
***/
void  CZMemRect::mrMaskQuantiles (long hndl, long hmask, DWORD *res, long numof)
{
	long i, k;
	DWORD sum, r, hist[256];

	if (hmask > 0)
    {
		mrHistogramMask (hndl, hmask, (long *)hist, 256);
    } else
    {
		mrHistogramLim (hndl, hist, 256);
    }
    i = k = 0;  sum = hist[0];
    while (i < numof)
	{
      	r = res[i];
    	while ((r > sum) && (k < 255))
        {
     		sum += hist[++k];
        }
	   	res[i++] = k;
   	}
}


/*********************************************** mrMaskSubrectOnes */
/***
 returns: number of 1-bits in given 1 bit per pixel
            subrectangle if OK,
 note: value 0 for xe,ye means 'whole dimension'
   	-1 if bad
***/
long  CZMemRect::mrMaskSubrectOnes (long hndl, long xl, long yt, long xe, long ye)
{
	long xeh, yeh, pitch, s = -1;
    BYTE *mem;

	if (mrGetExtendedInfo (hndl, &xeh, &yeh, &pitch, &mem) != 1)
        goto Fin;
    if (!xe) xe = xeh;
    if (!ye) ye = yeh;
    s = 0;
    if (xl < 0)
    {
        if (xl + xe <= 0) goto Fin;
        xe += xl;  xl = 0;
    }
    if (yt < 0)
    {
        if (yt + ye <= 0) goto Fin;
        ye += yt;  yt = 0;
    }
    if (xl >= xeh) goto Fin;
    if (xl + xe > xeh) xe = xeh - xl;
    if (yt >= yeh) goto Fin;
    if (yt + ye > yeh) ye = yeh - yt;
    s = iiplMaskOnes (mem, xl, yt, xe, ye, pitch);
Fin:
	return s;
}


/***************************************************** mrMaskTable */
/*********

*********/
static char *MaskTableOps =
	"R0 R2 R4 R8 RR0 RR2 RR4 RR8 ";
static long Rotations[] = {0, 4, 2 , 1, 0, 4, 2 , 1};

long  CZMemRect::mrMaskTable (long hndl, char *NameOrDescr, char *oper, long zeroone, long rep)
{
    long i, k, rot, nrot = 1, hndlR = 0, ret = -1;
    long xe, ye, pitch1, pitch2, opcase;
    DWORD offs = 0;
    MemRectangle *mrR;
    BYTE *mem1, *mem2, *wrkt, tab[64 * 8];
    char buf[512];
    bool recurs;

    opcase = WordInText (MaskTableOps, &offs, 0, oper, 0);
    if (! opcase) goto Fin;
    if (mrGetExtendedInfo (hndl, &xe, &ye, &pitch1, &mem1) < 1)
    		goto Fin;
    rot = Rotations[opcase - 1];	recurs = opcase > 4;
    if (!recurs)
    {	// not RECursive
			hndlR = mrCopyAs(hndl);
	    	mrR = ActiveEntry(hndlR);  if (! mrR) goto Fin;
        	pitch2 = mrR->Pitch;	mem2 = (BYTE *)mrR->Memory;
    }

    CopyVicDescr (NameOrDescr, buf, 512);
    if (rot) nrot = 8 / rot;
    i = 0;  wrkt = tab;
    while (i++ < nrot)
    {
    		mrFillMaskTab (zeroone, buf, wrkt);
        	if (rot) WordRound (buf, 8, rot);
        	wrkt += 64;
    }

//    mrFillMaskTab (zeroone, buf, tab);

    if (rep <= 0) rep = xe * ye;
    ret = 0;
    while (rep)
    {
        if (rot)
        {	// rotate
        	i = k = 0;  wrkt = tab;
            while (i++ < nrot)
            {
//			    mrFillMaskTab (zeroone, buf, tab);
				if (recurs)
               	{
    					k += MaskNeiTableR (mem1, xe, ye,
                    				pitch1, wrkt);
               	} else
               	{
	    				k += MaskNeiTable(mem2, xe, ye, pitch2,
    	    		 				mem1, pitch1, wrkt);
        	        	mrMove (hndl, hndlR);
            	}
               	wrkt += 64;
//                WordRound (buf, 8, rot);
            }
        } else
        {	// do not rotate
    	    	if (recurs)
        		{	// recursive
	    			k = MaskNeiTableR (mem1, xe, ye, pitch1, tab);
    	        	mrInverseRows (hndl);
        	    	mrInverseColumns (hndl);
    				k += MaskNeiTableR (mem1, xe, ye, pitch1, tab);
	            	mrInverseRows (hndl);
    	        	mrInverseColumns (hndl);
            	} else
        		{	// parallel
		        	if (rep & 1)
    					k = MaskNeiTable(mem2, xe, ye, pitch2,
        		 				mem1, pitch1, tab);
					else
    					k = MaskNeiTable(mem1, xe, ye, pitch1,
        						mem2, pitch2, tab);
	         }
        }
        if (k == 0) break;
        ret += k;   rep --;
    }
Fin:
	if (hndlR) Free(hndlR);
	return ret;
}


/********************************************* mrMaskToNeiContrast */
/***
		This function fills <h8> with 'pixels' depending on operation:
        	operation =	"Nei" -  		nei
            operation =	"Contrast" -  	contrast
            operation =	"Combine" - 	(nei<<4) + contrast    (default)
        where
        	0 <= nei <= 8 - number of (N8) neighbors
            0 <= contrast <= 4 - number of changes 0->1 (same as 1->0)
        Returns:
        	1 if OK, -1 otherwise
***/
long  CZMemRect::mrMaskToNeiContrast (long h1, long h8, char *operation)
{
	long xe1, ye1, pitch1, xe2, ye2, pitch2, oper,
    	 ret = -1;
    BYTE *mem1, *mem8;
    char *operations = "Nei Contrast Combine";
    DWORD offs = 0;

	if (mrGetExtendedInfo (h1, &xe1, &ye1, &pitch1, &mem1) != 1)
    	goto Fin;
	if (mrGetExtendedInfo (h8, &xe2, &ye2, &pitch2, &mem8) != 8)
    	goto Fin;
    if ((xe1 != xe2) || (ye1 != ye2)) goto Fin;
    oper = WordInText (operations, &offs, 0, operation, 0);
	MaskToNeiContrast (mem1, xe1, ye1, pitch1, mem8, pitch2, oper);
	ret = 1;
Fin:
	return ret;
}


/*************************************************** PixelwiseMask */
/***
 Both hndlArg, hndlRes must be valid rectangles of the same xe, ye
 and 8 bit pixel; hndlMask may be 0 (do not use mask) or valid
 rectangle  {1*xe*ye}
 the function operates pixelwide, but only on pixels where
 corresponding mask pixel is set to ONE (ZERO if negative mask
 is used, see below):
	handlR(x,y) =
		IF hndlMask(x,y) THEN hndlA(x,y) <operation> hndlR(x,y)
		ELSE hndlR(x,y)  /old value/
 the operation is defined by 1st word of the parameter
 (case insensitive):
    "And", "1&2"        A & R
    "More", "1-2", "1&~2"  A & ~R  (1 if A > R; A without R)
    "Copy", "1=>2"      A (copy A into R)
    "Less", "2-1", "~1&2" 	  ~A & R  (1 if A < R; R without A)
    "Xor", "Mod2", "1^2"  A ^ R  (mod 2 {exclusive OR})
    "Or", "1|2" 	     A | R
    "Nor", "~(1|2)" 	  ~(A | R)  (not OR)
    "Equal", "~(1^2)"   ~(A ^ R)  (1 if A = R)
    "MoreEq", "1|~2" 	  A | ~R  (1 if A >= R)
    "CopyNot", "~1=>2" 	  copy ~A into R
    "LessEq", "~1|2" 	  ~A | R  (1 if A <= R)
    "Nand", "~(1&2)" 	  ~(A & R)  (not AND)

	 "MIN" 							A MIN R
	 "MAX" 							A MAX r
	 "plus", "+", "1+2" 		   (A + R) mod 256
	 "+S", "Min(1+2,255)"   	(A + R) min 255 (saturation)
	 "Minus", "-", "1-2"    	(A - R) mod 256
	 "-L", "2-1" 			   	(R - A) mod 256 (left minus; inverse order)
	 "-S", "Max(1-2,0)" 	   	(A - R) max 0   (saturation)
	 "-LS", "-SL", "Max(2-1,0)"  (R - A) max 0   (left and saturation)
	 "LessTo0", "(1>=2)?1:0"   if A >= R then A else 0
	 "MoreToFF", "(1<=2)?1:FF" if A <= R then A else 255
	 "ByteMask", "(1<2)?0:FF"  if A < R then 0 else 255
	 "-A"					   if A < B then (B - A) else (A - B)
	 "-A8"					   Abs(A - B) MIN (256 - Abs(A - B))

   if (hndlM==0) - do not use mask
   if (neg!=0) - operates witn negative mask

 returns:
 		if OK: >=0;
     	-1 otherwise
***/
long CZMemRect::PixelwiseMask (long hndlA, char *operation, long hndlR, long hndlM, long neg)
{
    DWORD offs = 0;
    long pix, xe, ye, pix1, xe1, ye1, pitchA, pitchR, pitchM, opcase,
   	     ret = -1;
    DWORD oper;
    BYTE *memA, *memR, *memM;

    opcase = WordInText (BitwiseOps, &offs, 0, operation, 0);
    if (opcase)	oper = BitwiseConsts [opcase - 1];
	else
    {
   	    opcase = WordInText (BytewiseOps, &offs, 0, operation, 0);
   	    if (! opcase) goto Fin;
	  	oper = BytewiseConsts [opcase - 1];
     	if (oper > 1000) goto Fin;
    }
	pix = mrGetExtendedInfo (hndlA, &xe, &ye, &pitchA, &memA);
	pix1 = mrGetExtendedInfo (hndlR, &xe1, &ye1, &pitchR, &memR);
    if ((pix != pix1) || (xe != xe1) || (ye != ye1)) goto Fin;
    if ((pix != 8) && (pix != 32)) goto Fin;
    if (hndlM)
    {
		pix1 = mrGetExtendedInfo (hndlM, &xe1, &ye1, &pitchM, &memM);
       	if ((pix1 != 1) || (xe != xe1) || (ye != ye1)) goto Fin;
     	if (neg) oper |= mmcmNEG;
    } else
    {
   	    memM = memA;
     	pitchM = 0;
     	oper |= mmcmIGNORE;
    }
	ret = iiplMemMaskMem (memA, memR, memM, pix, xe, ye,
					pitchA, pitchR, pitchM, oper);

Fin:
	return ret;
}


/***************************************************** mrThreshold */
/***
 Calculates the Bit Mask on the basis of the contents
 of the given byte rectangle, the threshold and the operation
 defined by 1st word of the <operation> parameter (case insensitive):
	"NotLess", ">="	- less-than-threshold bytes => 0
			   	greater-than or equal-to-threshold bytes => 1
	"NotGreater", "<=" - greater-than-threshold bytes => 0
			   		less-than or equal-to-threshold bytes => 1
	"UnEqual", "<>"	- equal-to-threshold bytes => 0, others => 1
	"Equal", "=" 	- equal-to-threshold bytes => 1, others => 0
	"Less" , "<"	- less-than-threshold bytes => 1
			   	greater-than or equal-to-threshold bytes => 0
	"Greater", ">" - greater-than-threshold bytes => 1
			   		less-than or equal-to-threshold bytes => 0
 	returns:
   	 number of 1-bits in the Bit Mask if OK,
      -1 otherwise
***/
static char *ThreshOps = "NotLess NotGreater UnEqual Equal Less Greater "
						 " >=  <=  <>  =  <  > ";
long  CZMemRect::mrThreshold (
		long hndl, long hndlmask, char *operation, DWORD threshold)
{
	long pix, xe, ye, xemask, yemask, pitch, pitchmask,
    	 ret = -1;
   	DWORD opcase, offs = 0;
   	BYTE *mem, *memmask;

   	opcase = WordInText (ThreshOps, &offs, 0, operation, 0);
   	if (! opcase) goto Fin;   if (opcase > 6) opcase -= 6;
    pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
	if (mrGetExtendedInfo (hndlmask, &xemask, &yemask, &pitchmask, &memmask)
    	 != 1) goto Fin;
   	if ((xe != xemask) || (ye != yemask)) goto Fin;
	if ((pix == 8) || (pix == 32))
		ret = iiplMemToBitMask (mem, pix, xe, ye, pitch,
					  memmask, pitchmask, threshold, opcase);
Fin:
	return ret;
}


/***************************************************** mrWaterMask */
long  CZMemRect::mrWaterMask (long hpic8, long hprohibmsk, long hmask)
{
   	long dpic, wrk1, wrk2, px, py, ret = 0;

   	dpic = mrCreateAs(hpic8);
   	wrk1 = mrCreateAs(hmask);
   	wrk2 = mrCreateAs(hmask);
   	do
   	{
   		Bitwise (dpic, "Xor", dpic);
      	PixelwiseMask (hpic8, "copy", dpic, hmask, 0);
      	mrNeiFun (dpic, "Dilate4", 1);
      	Bytewise (hpic8, "-S", dpic);
      	mrThreshold (dpic, wrk1, "Greater", 0); // greater neighbors mask
      	Bitwise (hmask, "Copy", wrk2);
      	mrNeiFun (wrk2, "Dilate4", 1);
      	Bitwise (hmask, "Less", wrk2); // contour
      	Bitwise (hprohibmsk, "Less", wrk2); // erase prohibited pixels
      	Bitwise (wrk1, "Less", wrk2); // erase greater neighbors
      	Bitwise (wrk2, "Or", hmask); // add watershed pixels
      	px = py = 0;  ret++;
   	} while (mrFindNext (wrk2, &px, &py, 1) > 0);
   	Free(dpic);   Free(wrk1);   Free(wrk2);
	return ret;
}


/************************************************* mrMaskWatershed */
/***
 Returns # of used components
 if (maxarea < 0) - it means 'UP-direction' of water flow
***/
long  CZMemRect::mrMaskWatershed (long hndl8, long srcmask, long hresmask, long minarea, long maxarea, long margin, long nopen)
{
   	long xe, ye, hlimit, hprohib, hwrk, x, y, compnum, ones, hpic8,
    	 oh8, olimit, owrk, ores, opro, oxmin, oymin, oxe, oye,
         sizedata[4];

   	x = y = compnum = 0;
    hpic8 = mrCreateAs(hndl8);
    if (maxarea < 0)
    {
    	Bitwise (hndl8, "CopyNot", hpic8);
        maxarea = -maxarea;
    } else Bitwise (hndl8, "Copy", hpic8);
    mrGetRectInfo(hpic8, &xe, &ye);
    mrOperMask (hresmask, "Zeros");
   	hlimit = mrCopyAs (srcmask);
   	hprohib = mrCopyAs (srcmask);
   	hwrk = mrCreateAs (srcmask);
    oh8 = mrCreateOverlay(hpic8, 0, 0, 1, 1);
    ores = mrCreateOverlay(hresmask, 0, 0, 1, 1);
    olimit = mrCreateOverlay(hlimit, 0, 0, 1, 1);
    opro = mrCreateOverlay(hprohib, 0, 0, 1, 1);
    owrk = mrCreateOverlay(hwrk, 0, 0, 1, 1);

   	while (true)
	{
    	if (mrFindNext (hlimit, &x, &y, 1) <= 0)
        	break; // no more
      	ones = mrReconFastD (hlimit, hwrk, x, y, sizedata);
        if ((oxmin = sizedata[0]) < margin) oxmin = 0;
        else oxmin = (oxmin - margin) & 0xfffffff0;
        if ((oymin = sizedata[1] - margin) < 0) oymin = 0;
  		if ((oxe = sizedata[2] + margin + 1) > xe) oxe = xe;
  		if ((oye = sizedata[3] + margin + 1) > ye) oye = ye;
        oxe -= oxmin;  oye -= oymin;

		mrResizeOverlay(oh8, oxmin, oymin, oxe, oye);
		mrResizeOverlay(olimit, oxmin, oymin, oxe, oye);
		mrResizeOverlay(ores, oxmin, oymin, oxe, oye);
		mrResizeOverlay(opro, oxmin, oymin, oxe, oye);
		mrResizeOverlay(owrk, oxmin, oymin, oxe, oye);
      	Bitwise (owrk, "Less", olimit);
        if ((ones >= minarea) && (ones <= maxarea))
        {
			mrWaterMask (oh8, opro, owrk);
	      	Bitwise (owrk, "Or", opro);
            mrNeiFun (owrk, "Erode4", 1);
            if (nopen)
            {
//	            mrOperMask (owrk, "FillHoles0");
            	mrNeiFun (owrk, "Open84", nopen);
            }
	      	Bitwise (owrk, "Or", ores);
	        compnum++;
        }
   	}
    Free(ores);
   	Free(hpic8);  Free(hlimit);  Free(hwrk);  Free(hprohib);
    // also oh8,olimit,owrk,opro
	return compnum;
}


/**************************************************** mrMoveExtern */
/***
 hndl must be valid; extmem is a pointer to any memory of enough size.
 the function copies pixel data; <direction> may be:
 	"=>"	- from <extmem> to <hndl> content
 	"<="	- from <hndl> content to <extmem>

 returns:
 		number of moved bytes if OK,
     	-1 otherwise
***/
long  CZMemRect::mrMoveExtern(char *extmem, char *direction, long hndl)
{
	long size, retsize = -1;
  	MemRectangle *mr;
  	char *hndlmem;
  	DWORD opcase, offs = 0;

  	opcase = WordInText ("=> <= ", &offs, 0, direction, 2);
  	if (! opcase) goto Fin;
  	mr = ActiveEntry(hndl);  if (! mr) goto Fin;
   	if (mr->Memorysize == MemRectIsOverlay) goto Fin;
  	hndlmem = (char *)mr->Memory;
  	size = mr->Pitch * mr->Yext;
  	if (opcase == 1)
  		MovMemory (extmem, hndlmem, size);
  	else
  		MovMemory (hndlmem, extmem, size);
  	retsize = size;
Fin:
	return retsize;
}




/******************************************************** mrMulDiv */
/***
		mul - multiplier
		div - divisor
	Returns:
		0 - no byte overflow, 1 - byte overflow,
     	-1 - wrong handle
***/
long  CZMemRect::mrMulDiv (long hndl, DWORD mul, DWORD div)
{
	long pix, xe, ye, pitch, ret = -1;
    BYTE *mem, table[256];
	DWORD size, maxpix = 255, i = 0;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
    if (pix < 1) goto Fin;
    if (div < 1) div = 1;
    if (mul) maxpix = ((256 * div - 1) / mul);
    if (maxpix < 255) ret = 1;
    else { ret = 0;  maxpix = 255; }
    while (i <= maxpix)
    {
       	table[i] = i * mul / div;
       	i++;
    }
    while (i < 256) table[i++] = 255;
	size = (pix * xe + 7) >> 3;
	while (ye--)
    {
   	 	for (i = 0;  (DWORD)i < size;  i++)
      	{
        	mem[i] = table[(BYTE)mem[i]];
      	}
      	mem += pitch;
   	}
Fin:
	return ret;
}


/****************************************************** mrMulDivPN */
/***
		hndl[] = MIN(hndl[] * mulhndl[] / div, MaxPixVal)
        both rects must be of 8 bit/pixel
	Returns:
		if OK - number of overflow pixels,
     	-1 - wrong handle
***/
long  CZMemRect::mrMulDivPN (long hndl, long mulhndl, DWORD div)
{
	long pix, xe, ye, pitch, pix1, xe1, ye1, pitch1, i, ret = -1;
    BYTE *mem, *mem1;
	DWORD pval, maxpix = 255;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
    if (pix != 8) goto Fin;
	pix1 = mrGetExtendedInfo (mulhndl, &xe1, &ye1, &pitch1, &mem1);
    if ((pix1 != 8) || (xe != xe1) || (ye != ye1)) goto Fin;
    if (div < 1) div = 1;
    ret = 0;  maxpix = 255;
	while (ye--)
    {
   	 	for (i = 0;  i < xe;  i++)
      	{
        	pval = mem[i] * mem1[i] / div;
            if (pval > maxpix) { pval = maxpix;  ret++; }
        	mem[i] = pval;
      	}
      	mem += pitch;  mem1 += pitch1;
   	}
Fin:
	return ret;
}


/************************************************ mrNumberOfActive */
/***
 returns number of active handles
***/
long  CZMemRect::mrNumberOfActive (DWORD *usedmem, DWORD *portions)
{
   	MemRectangle *memr;
   	DWORD memsize, hndl, ret;

    ret = memsize = hndl = 0;
   	while (hndl++ < NumberOfMemRects)
   	{
        memr = ActiveEntry(hndl);
   		if (memr)
        {
        	ret++;
            if (ThisRectIsBased)
            {
             	memsize += memr->Memorysize;
            }
        }
   	}
    if (usedmem) *usedmem = memsize;
    if (portions) *portions = NumberOfMemRects;
   	return ret;
}


/*************************************************** mrOvusDistThr */
/***
***/
long  CZMemRect::mrOvusDistThr(long hndl, long dist, long thr)
{
	long hndl1, xe, ye, pitch, pitch1, x, y,
    	wminmax, w2, w3, ix, iy, dist2, sum, num,
    	ret = -1;
   	BYTE *memh, *mem1, *wrk;

	if (mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &memh) != 8)
    	goto Fin;
    if ((hndl1 = mrCopyAs(hndl)) <= 0) goto Fin;
	mrGetExtendedInfo (hndl1, &xe, &ye, &pitch1, &mem1);
	if (((xe - 1) / 2 < dist) || ((ye - 1) / 2 < dist))
    	goto Fin;
    dist2 = (long)(sqrt((double)dist / 2) + 0.5);    ret = 0;

    for (y = 0;  y < ye;  y++)
    {
        for (x = 0;  x < xe;  x++)
        {
            sum = num = wminmax = 0;
            if ((w3 = memh[pitch * y + x] - thr) <= 0)
            	goto LoopFin;
            if ((iy = y - dist) >= 0)
            {
                wrk = mem1 + (pitch1 * iy);
                if (wminmax < (w2 = wrk[x]))
                	if ((wminmax = w2) >= w3) goto LoopFin;
                sum += w2;  num++;
            }
            if ((iy = y - dist2) >= 0)
            {
                wrk = mem1 + (pitch1 * iy);
                if ((ix = x - dist2) >= 0)
                {
                	if (wminmax < (w2 = wrk[ix]))
	                	if ((wminmax = w2) >= w3) goto LoopFin;
	                sum += w2;  num++;
	            }
                if ((ix = x + dist2) < xe)
                {
                	if (wminmax < (w2 = wrk[ix]))
	                	if ((wminmax = w2) >= w3) goto LoopFin;
	                sum += w2;  num++;
	            }
            }
            wrk = mem1 + (pitch1 * y);
            if ((ix = x - dist) >= 0)
            {
            	if (wminmax < (w2 = wrk[ix]))
                	if ((wminmax = w2) >= w3) goto LoopFin;
                sum += w2;  num++;
            }
            if ((ix = x + dist2) < xe)
            {
            	if (wminmax < (w2 = wrk[ix]))
                	if ((wminmax = w2) >= w3) goto LoopFin;
                sum += w2;  num++;
            }
            if ((iy = y + dist2) < ye)
            {
                wrk = mem1 + (pitch1 * iy);
                if ((ix = x - dist2) >= 0)
                {
                	if (wminmax < (w2 = wrk[ix]))
	                	if ((wminmax = w2) >= w3) goto LoopFin;
	                sum += w2;  num++;
	            }
                if ((ix = x + dist2) < xe)
                {
                	if (wminmax < (w2 = wrk[ix]))
	                	if ((wminmax = w2) >= w3) goto LoopFin;
	                sum += w2;  num++;
	            }
            }
            if ((iy = y + dist) < ye)
            {
                wrk = mem1 + (pitch1 * iy);
                if (wminmax < (w2 = wrk[x])) wminmax = w2;
                sum += w2;  num++;
            }
            if ((w3 > wminmax) && (num))
            {
                w3 = w3 + thr - (sum / num);
            } else
            {
LoopFin:
            	w3 = 0;
            }
            memh[pitch * y + x] = (BYTE)w3;
            if (w3 > ret) ret = w3;
		}
    }
Fin:
    if (hndl1 > 0) Free(hndl1);
	return ret;
}


/************************************************* mrPixelwiseMask */
long  CZMemRect::mrPixelwiseMask (
			long hndlA, char *operation, long hndlR, long hndlM)
{
   	long ret;

	ret = PixelwiseMask (hndlA, operation, hndlR, hndlM, 0);
	return ret;
}

/********************************************** mrPixelwiseNotMask */
long  CZMemRect::mrPixelwiseNotMask (
			long hndlA, char *operation, long hndlR, long hndlM)
{
    long ret;

	ret = PixelwiseMask (hndlA, operation, hndlR, hndlM, 1);
	return ret;
}


/*************************************************** mrProjection0 */
/***
 calculates the value defined by 1st word
 of the <operation> parameter (case insensitive):
 returns:
		for "Min    : MIN of byte values
		for "Max"   : MAX of byte values
		for "Sum"   : SUM of byte values
		for "Ones"  : number of ONE bits
		for "Zeros" : number of ZERO bits
      -1 if bad
***/
static char *Proj0Ops =
	"MIN MAX SUM ONES ZEROS ";
static WORD Proj0Consts[] =
	{mtvMIN, mtvMAX, mtvSUM, mtvONES, mtvZEROS};

long  CZMemRect::mrProjection0 (long hndl, char *operation)
{
	long size, ret = -1;	//  wait worse
   	DWORD opcase, offs = 0;
   	WORD oper;
   	MemRectangle *mr;
   	BYTE *mem;

   	opcase = WordInText (Proj0Ops, &offs, 0, operation, 0);
   	if (! opcase) goto Fin;
   	oper = Proj0Consts [opcase - 1];

   	mr = ActiveEntry(hndl);  if (!mr) goto Fin;
   	if (mr->Memorysize == MemRectIsOverlay) goto Fin;
  	mem = (BYTE *)mr->Memory;
  	size = mr->Pitch * mr->Yext;
  	ret = MemToValue (mem, size, oper);
Fin:
	return ret;
}


/*************************************************** mrProjection1 */
/***
		This function fills the projection of a rectangle:
       a row of <xe> elements for "~ROW" operations, or
      a column of <ye> elements for "~COL" operations.
      Result must be of size of the corresponding side of the
      rectangle. The size of an element in the result array is:
      1 byte for "MIN~" and "MAX~" operations;
      4 bytes for "SUM~" operation.

			rectangle				    |  column projection
											 |  (for "SUMCOL")
		____________________________|______________________
		a(0,0)       ...   a(0,n)   | Sum(a(0,i)), i=0...n
		 ...         ...     ...    | ...
		a(k,0)       ...   a(k,n)   | Sum(a(k,i)), i=0...n
		____________________________|______________________
											 |
		Min(a(i,0))  ... Min(a(i,n))|<  row projection
		i=0...k          i=0...k    |< (for "MINROW")

 Returns:
 	Min of byte values of the whole rectangle for "MinRow" and "MinCol";
	Max of byte values of the whole rectangle for "MaxRow" and "MaxCol";
	Sum of byte values of the whole rectangle	for "SumRow" and "SumCol";
   -1 if something wrong
***/
static char *Proj1Ops =
	"MINROW MAXROW SUMROW MINCOL MAXCOL SUMCOL ";
static WORD Proj1Consts[] =
	{prjMIN, prjMAX, prjSUM, prjMIN, prjMAX, prjSUM};

long  CZMemRect::mrProjection1 (long hndl,
						char *operation, void *res)
{
	long pix, xe, ye, pitch, ret = -1;	//  wait worse
   	DWORD opcase, offs = 0;
   	WORD oper, rowcol;
   	BYTE *mem;

   	opcase = WordInText (Proj1Ops, &offs, 0, operation, 0);
   	if (! opcase) goto Fin;
   	oper = Proj1Consts [opcase - 1];
    if (opcase > 3) rowcol = 1; else rowcol = 0;
    pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
	if (pix == 8)
    {
   		ret = Projection (mem, (BYTE *)res, pitch, ye, rowcol, oper);
    	goto Fin;
    }
	if (pix == 32)
    {
		ret = Projection32 (mem, (DWORD *)res,
        				xe, ye, pitch, rowcol, oper);
    }
Fin:
	return ret;
}


/***************************************************** mrQuantiles */
/***
 fills DWORD array <res>[numof] with "quantiles" of
 previous values (must go in increasing order):
 	res[i] = qi, where number of pixels with
    values < qi is less than previous res[i], but
	number of pixels with values <= qi is more
    or equal to previous res[i]
***/
void  CZMemRect::mrQuantiles (long hndl, DWORD *res, long numof)
{
	long xe, ye, pitch, sum, i, k, r;
   	DWORD hist[256];
   	BYTE *mem;

	if (mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem) != 8)
    	return;
   	PictHist (mem, xe, ye, pitch, hist);
   	i = k = 0;  sum = hist[0];
   	while (i < numof)
   	{
      	r = res[i];
      	while ((r > sum) && (k < 255))
    	 	sum += hist[++k];
     	res[i++] = k;
   	}
}


/*************************************************** mrRecRecon */
/***
		Reconstructs 'Seed' 8-bit picture within 'Limit'.
		Algorithm is recursive reconstruction in 4 directions

 returns:
 		0 if OK, -1 otherwise
***/
long  CZMemRect::mrRecRecon (long hndlLimit, long hndlSeed)
{
	long pix, pixS, xe, ye, xeS, yeS, pitchL, pitchS,
    	 i, ret = -1;
   BYTE *memL, *memS;

	pix = mrGetExtendedInfo (hndlLimit, &xe, &ye, &pitchL, &memL);
	pixS = mrGetExtendedInfo (hndlSeed, &xeS, &yeS, &pitchS, &memS);
   if ((pix != pixS) || (pix != 8) || (xe != xeS) || (ye != yeS))
      goto Fin;
   for (i = 0;  i < 10;  i += 3)
   {
		MinMaxDirection (memS, pitchS, memL, pitchL,
		    xe, ye, 1,  // min(max(seed neis), limit))
          i & 3);     // 0, 3, 2, 1
		//  0 - TopLeft_to_BottomRight
    	//	1 - TopRight_to_BottomLeft
    	//	2 - BottomLeft_to_TopRight
        //	3 - BottomRight_to_TopLeft
   }
   ret = 0;
Fin:
	return ret;
}


/************************************************** mrRectMaskData */
/***
		data - pointer to array of not less than 11 longs;
        		fills it with:
        	data[0] = NUMBER of one bits in mask
        	data[1] = SUM of bytes in 'mem' corresponding to one bits in mask
        	data[2] = MIN of bytes in 'mem' corresponding to one bits in mask
        	data[3] = MAX of bytes in 'mem' corresponding to one bits in mask
        	data[4] = X coordinate one of the MIN pixel(s)
        	data[5] = Y coordinate one of the MIN pixel(s)
        	data[6] = X coordinate one of the MAX pixel(s)
        	data[7] = Y coordinate one of the MAX pixel(s)
        	data[8] = standard deviation:
            			(long)(sqrt(Sum((P-Aver(P))^2)/NumOf)+0.5)
        	data[9] = number of pixels with MIN value
        	data[10] = number of pixels with MAX value

	Returns
		number of 1-bits in mask (same as data[0]),
        -1 if failure
***/
long  CZMemRect::mrRectMaskData (long hndl8, long hndl1, long *data)
{
	long xe8, ye8, xe1, ye1, pitch8, pitch1, ret = -1;
    BYTE *mem8, *mem1;

	if (mrGetExtendedInfo (hndl8, &xe8, &ye8, &pitch8, &mem8) != 8)
    	goto Fin;
	if (mrGetExtendedInfo (hndl1, &xe1, &ye1, &pitch1, &mem1) != 1)
    	goto Fin;
    if ((xe8 != xe1) || (ye8 != ye1))
    	goto Fin;
	ret = MemMaskData (mem8, xe8, ye8, pitch8, mem1, pitch1, data);

Fin:
	return ret;
}


/************************************************** mrRectToBitMap */
/***
	This function creates Windows BITMAP object from
		given rectangle using current LUT
   hwndpar - window's handle to get the device context;
      		 zero means 'GetActiveWindow()'
	Returns:
		handle of the created BitMap if OK,
		0 otherwise.
***/
long  CZMemRect::mrRectToBitMap (long hndl, long hwndpar)
{
	long pix, xe, ye, pitch, bmap = 0;
    char *mem;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);

   	if (pix > 0)
   	{
		bmap = MemToBitMap (hwndpar, pix, xe, ye, mem);
	}
	return bmap;
}


/*************************************************** mrRedimension */
/***
    Returns:
     1  - if regular rectangle
     2  - if extern rectangle
     3  - if overlay rectangle
     -1  - something wrong
***/
long  CZMemRect::mrRedimension (long hndl,
				long BitsPerPixel, long Xe, long Ye)
{
  	DWORD pitch, size, addr, baddr;
    long ret = -1;
  	MemRectangle *memr;
  	char *rectmem;
  	if (((1L << BitsPerPixel) & AcceptablePixelSizes) == 0)
  	{
     	if ((BitsPerPixel != 32) && (BitsPerPixel != 64))
  			goto Fin;
  	}
   	// pointer to the current entry:
	memr = ActiveEntry(hndl);
  	if (!memr) goto Fin;
  	// calculate memory parameters?
  	pitch = ((BitsPerPixel * Xe + 31) >> 5) << 2;	// bytes
  	if (memr->Memorysize < MemRectIsExtern)
  	{
  		size = pitch * Ye;
     	if (memr->Memorysize < size)
     	{
  			rectmem = (char *)My_malloc (size + 16);
  			if (!rectmem) goto Fin;
  			memr->Memorysize = size;
            baddr = (DWORD)rectmem;
 			addr = (baddr + 7) & 0xFFFFFFF8; // 8 byte boundary alignment
            My_free ((char *)memr->BaseMemory);
            memr->BaseMemory = baddr;
            memr->Memory = addr;
     	}
     	ret = 1;  goto OK;
  	}
  	if (memr->Memorysize == MemRectIsExtern)
  	{	// user's responsibility!!!
  		ret = 2;  goto OK;
  	}
  	if (memr->Memorysize == MemRectIsOverlay)
  	{
        if (memr->Pixelsize == (DWORD)BitsPerPixel)
      	if (mrResizeOverlay(hndl, 0, 0, Xe, Ye) == 0)
	        	ret = 3;
        goto Fin; // everything is filled while resizing
  	}
OK:   	//	set other members:
  	memr->Pixelsize = BitsPerPixel;
  	memr->Xext = Xe;
  	memr->Yext = Ye;
  	memr->Pitch = pitch;
Fin:
	return ret;
}


/************************************************* mrReplaceOrCopy */
/***
	Returns:
     0 - <*htarget> was possibly redimensioned, pixels copied
	 1 - <*htarget> wasn't a valid handle
	 2 - <*htarget> was valid handle, but not redimensionable,
     	 so it was deleted

***/
long  CZMemRect::mrReplaceOrCopy (long hsrc, long *htarget)
{
    long hndl, pix, xe, ye, pixt, xet, yet,
    	 ret = -1;
  	MemRectangle *memr;

    hndl = *htarget;
    memr = ActiveEntry(hndl);
    if (!memr)
    {
     	*htarget = hsrc;  ret = 1;  goto Fin;
    }
	pix = mrGetRectInfo (hsrc, &xe, &ye);
	pixt = mrGetRectInfo (hndl, &xet, &yet);
    if ((pixt == pix) && (xet == xe) && (yet == ye))
    {
MovePixels:
    	mrMove(hsrc, hndl);  ret = 0;  goto Fin;
    }
    if ((memr->Memorysize < MemRectIsExtern) ||
    	(memr->Memorysize == MemRectIsOverlay))
    {
		mrRedimension (hndl, pix, xe, ye);
        goto MovePixels;
    }
	Free(hndl);
   	*htarget = hsrc;  ret = 2;
Fin:
	return ret;
}


/****************************************************** mrRGBtoHSI */
/***
    Transforms Red, Green, Blue color components into
    Hue, Saturation, Intensity. Every H, S, U are normalized
    to the range {0-255}.
	H = arctan{ ((G-B)/sqr(3)) / (R-I) } =
		arctan{ ((G-B)*sqr(3)) / (3*R-(R+G+B)) }
    Returns maximal Intensity if OK,
    		-1 otherwise
***/
long  CZMemRect::mrRGBtoHSI(long R, long G, long B, long H, long S, long I)
{
	long pix, xe, ye, xew, yew;
	long ix, iy, rv, gv, bv, hv, sv, iv; //, cc, cmin, ax, ay;
    long Rpitch, Gpitch, Bpitch, Hpitch, Spitch, Ipitch;
    long ret = -1;
   	BYTE *Rp, *Gp, *Bp, *Hp, *Sp, *Ip;

	pix = mrGetExtendedInfo (R, &xe, &ye, &Rpitch, &Rp);
 	if (pix != 8) goto Fin;
	pix = mrGetExtendedInfo (G, &xew, &yew, &Gpitch, &Gp);
 	if ((pix != 8) || (xew != xe) || (yew != ye)) goto Fin;
	pix = mrGetExtendedInfo (B, &xew, &yew, &Bpitch, &Bp);
 	if ((pix != 8) || (xew != xe) || (yew != ye)) goto Fin;
	pix = mrGetExtendedInfo (H, &xew, &yew, &Hpitch, &Hp);
 	if ((pix != 8) || (xew != xe) || (yew != ye)) goto Fin;
	pix = mrGetExtendedInfo (S, &xew, &yew, &Spitch, &Sp);
 	if ((pix != 8) || (xew != xe) || (yew != ye)) goto Fin;
	pix = mrGetExtendedInfo (I, &xew, &yew, &Ipitch, &Ip);
 	if ((pix != 8) || (xew != xe) || (yew != ye)) goto Fin;
    for (iy = 0;  iy < ye;  iy++)
    {
	    for (ix = 0;  ix < xe;  ix++)
    	{
            rv = Rp[ix];  gv = Gp[ix];  bv = Bp[ix];
			RGB8toHSI(rv, gv, bv, &hv, &sv, &iv);
            Hp[ix] = hv;  Sp[ix] = sv;  Ip[ix] = iv;
            if (ret < iv) ret = iv;
        }
        Rp += Rpitch;  Gp += Gpitch;  Bp += Bpitch;
        Hp += Hpitch;  Sp += Spitch;  Ip += Ipitch;
    }
Fin:
    return ret;
}


/****************************************************** mrRotation */
/***
  Move Rectangle with Rotation around point {xc1,yc1};
  it goes to {xc2,yc2}
  Mask <hndl3> controls moving; it corresponds to <hndl1>
  (source rectangle);
  0 is ok for <hndl3> (no mask)
  returns pix or -1
 ....... FOR NOW: only 1 or 8 bit/pixel
***/
long  CZMemRect::mrRotation (long hndl1, long hndl2, long hndl3,
         	long xc1, long yc1, long xc2, long yc2, double angle)
{
	long pix1, xe1, ye1, pitch1, pix2, xe2, ye2, pitch2,
   		pix3, xe3, ye3, pitch3,
   		ret = -1;
   BYTE *mem1, *mem2, *mask;

	pix1 = mrGetExtendedInfo (hndl1, &xe1, &ye1, &pitch1, &mem1);
   if (pix1 < 1) goto Fin;
	pix2 = mrGetExtendedInfo (hndl2, &xe2, &ye2, &pitch2, &mem2);
   if (pix1 != pix2) goto Fin;
   if (hndl3)
   {
		pix3 = mrGetExtendedInfo (hndl3, &xe3, &ye3, &pitch3, &mask);
   	if ((pix3 != 1) || (xe3 < xe1) || (ye3 < ye1)) goto Fin;
   } else { mask = NULL;  pitch3 = 0; }
   if (pix1 == 1)
   {
		MaskRotation (mem1, xe1, ye1, pitch1,
						  mem2, xe2, ye2, pitch2, mask, pitch3,
                    xc1, yc1, xc2, yc2, angle);
      ret = 1;  goto Fin;
   }
   if (pix1 == 8)
   {
		GrayRotation (mem1, xe1, ye1, pitch1,
						  mem2, xe2, ye2, pitch2, mask, pitch3,
                    xc1, yc1, xc2, yc2, angle);
      ret = 8;  goto Fin;
   }
Fin:
	return ret;
}


/****************************************************** mrSetLabel */
/***
    Returns label's length including terminating 0
    (from 0 to MemRectLabelSize) if OK
     or -1 if failure
***/
long  CZMemRect::mrSetLabel(long hndl, char *label)
{
	long k = -1;
   	char c;
   	MemRectangle *mr;

   	mr = ActiveEntry(hndl);
   	if (mr)
   	{
      	k = 0;
      	while (k < MemRectLabelSize)
      	{
      		if ((c = label[k]) < ' ') break;
         	mr->Label[k++] = c;
      	}
      	mr->Label[k] = 0;
   	}
	return k;
}


/******************************************************** mrSetTag */
/***
    Returns sizeof(MemRectangle) if OK or -1 if failure
***/
long  CZMemRect::mrSetTag(long hndl, long tagvalue)
{
	long ret = -1;
   	MemRectangle *mr;

   	mr = ActiveEntry(hndl);
   	if (mr)
   	{
   		ret = sizeof(MemRectangle);
      	mr->Tag = tagvalue;
   	}
	return ret;
}


/********************************************************* mrSlide */
/***
 Moves pixels from hsrc to hdest with given position's shift;
 hsrc, hdest must be different but of the same pix*xe*ye.
 If (adjust >= 0) then fills free part with this value,
 else   repeats nearest row and column.
 Returns:
 		if OK: 0;
        -1 otherwise
***/
long  CZMemRect::mrSlide (long hfrom, long htarg,
						long dx, long dy, long adjust)
{
	long pixs, pixd, xes, yes, xed, yed,
    	 sxl, syt, dxl, dyt, xe, ye, cnt, beg,
         hsrc, hdest, hs, hd, hnew = 0, ret = -1;
    char *op[] = {"Xor", "Equal"};

	pixs = mrGetRectInfo (hfrom, &xes, &yes);
   	if (pixs < 1) goto Fin;
    if ((!htarg) || (htarg == hfrom))
    {
        if ((hnew = mrCopyAs(hfrom)) <= 0) goto Fin;
     	hsrc = hnew;  hdest = hfrom;
    } else
    {
        hsrc = hfrom; 	hdest = htarg;
		pixd = mrGetRectInfo (hdest, &xed, &yed);
   		if ((pixs != pixd) || (xes != xed) || (yes != yed))
    		goto Fin;
    }
    if (dx >= 0) { sxl = 0;  dxl = dx;  xe = xes - dx; }
    else { sxl = -dx;  dxl = 0;  xe = xes + dx; }
    if (dy >= 0) { syt = 0;  dyt = dy;  ye = yes - dy; }
    else { syt = -dy;  dyt = 0;  ye = yes + dy; }
    if ((xe <= 0) || (ye <= 0)) { ret = 0;  goto Fin; }
   	if (pixs == 1)
   	{
	    if (adjust >= 0) Bitwise (hdest, op[adjust & 1], hdest);
        if ((hs = mrCreate(1, xe, ye)) < 1) goto Fin;
		if (mrMoveSubRect(hsrc, sxl, syt, hs, "=>") <= 0) goto Pix1Fin;
		if (mrMoveSubRect(hdest, dxl, dyt, hs, "<=") <= 0) goto Pix1Fin;
//        if (adjust < 0) ...
	  	ret = 0;
Pix1Fin:
        Free(hs);  goto Fin;
   	}
   	if ((pixs == 8) || (pixs == 24) || (pixs == 32))
    {
	   	if (pixs == 24) adjust = -1; // ConstBitwise() doesn't accept 24 bpp
	    else if (adjust >= 0) ConstBitwise (adjust, "Copy", hdest);
        if ((hs = mrCreateOverlay(hsrc, sxl, syt, xe, ye)) < 1) goto Fin;
        if ((hd = mrCreateOverlay(hdest, dxl, dyt, xe, ye)) < 1)
        {  Free(hs);  goto Fin;  }
        mrMove(hs, hd);
        if (adjust < 0)
        {
            Free(hs);
            hs = mrCreateOverlay(hdest, 0, 0, 1, 1);
        	if (dx)
            {
             	if (dx > 0)
                {
					mrResizeOverlay(hs, dx, 0, 1, yes);
                    beg = 0;
                } else
                {
					beg = xes + dx;
					mrResizeOverlay(hs, beg - 1, 0, 1, yes);
                }
                mrResizeOverlay(hd, beg, 0, 1, yes);
			    cnt = abs(dx);
                while (cnt--)
                {
                 	mrMove(hs, hd);
                    if (++beg < xes) mrMoveOverlay(hd, beg, 0);
                }
            }
        	if (dy)
            {
             	if (dy > 0)
                {
					mrResizeOverlay(hs, 0, dy, xes, 1);
                    beg = 0;
                } else
                {
					beg = yes + dy;
					mrResizeOverlay(hs, 0, beg - 1, xes, 1);
                }
                mrResizeOverlay(hd, 0, beg, xes, 1);
			    cnt = abs(dy);
                while (cnt--)
                {
                 	mrMove(hs, hd);
                    if (++beg < yes) mrMoveOverlay(hd, 0, beg);
                }
            }
        }
        Free(hs);  Free(hd);  ret = 0;  goto Fin;
    }
Fin:
	if (hnew) Free(hnew);
	return ret;
}


/************************************************ mrSmoothByZoom */
/***
  Smooth 8 bit per pixel rectangle, according
  <zoommode> and <neioper>
  <zoommode> values (case insensitive) are same
  as for mrZoom():
 	"Int" : interpolation
 	"Sub" : subsampling
    "Rep" : replication
  <neioper> values (case insensitive) are same
  as for mrNeiFun():
     "AVER124" | "ERODE8" | "DILATE8" | "ERODE4" | "DILATE4"
 e.g.
 	if ( SmoothWithZoom (Pict, 5, 16, "Sub", "AVER124") > 0) ...
 returns
		ye if Ok,
		-1 otherwise
***/
long  CZMemRect::mrSmoothByZoom(long hndl,
					long iter, long zoomval,
                    char *zoommode, char *neioper)
{
	long xe, ye, zxe, zye, small = 0, ret = -1;

    if (mrGetRectInfo(hndl, &xe, &ye) != 8)
    	goto Fin;
    zxe = xe / zoomval;  zye = ye / zoomval;
    small = mrCreate(8, zxe, zye);
    if (mrZoom(hndl, small, zoommode) < 0)
    	goto Fin;
    if (iter > 0) mrNeiFun(small, neioper, (short)iter);
    if (mrZoom(small, hndl, zoommode) > 0)
    	ret = ye;
Fin:
    if (small) Free(small);
	return ret;
}


/************************************************** mrSubstSubRect */
/***
 	Substitutes 8-bits pixels in a given subrectangle
    in a memory rectangle with bytes from a given table

    Note: (ytop < 0) is acceptable; it means "inverse Y coordinate",
    	in this case numeration begins from -1
	Returns:
		new sum of pixels in the mem. sub. rect. if OK,
      -1 otherwise
***/
long  CZMemRect::mrSubstSubRect (
			long hndl, long xleft, long ytop,
            long xesub, long yesub, void *table)
{
	long pix, xe, ye, pitch, i, k,
    	ret = -1;
   BYTE c, *mem, *t, *row;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
   if (pix != 8) goto Fin;
   if (ytop < 0) ytop += (ye - yesub);
   if ((xleft + xesub > xe) || (ytop + yesub > ye))
    	goto Fin;
	mem += (pitch * ytop + xleft);
   t = (BYTE *)table;  i = ret = 0;
   while (i++ < yesub)
   {
      row = mem;  mem += pitch;
     	for (k = 0;  k < xesub;  k++)
      {
       	c = t[row[k]];
        	ret += (row[k] = c);
      }
   }
Fin:
	return ret;
}


/************************************************** mrThickProfile */
/***

   fills array <points> with <npoints> BYTE pixel values;
	returns:
      (min pixel << 8) | max pixel;
      -1 if error or wrong parameters
***/
long  CZMemRect::mrThickProfile (long hndl,
                        void *points, long npoints,
                        double x1, double y1,
                        double x2, double y2, double rectwidth)
{
	long xe, ye, pitch, ix, iy, ip1, ip2, minp, maxp,
   	    ret = -1;
    BYTE curr, *mem, *bytepnts;
    double dn, vx, vy, ux, uy, vmax, x, y,
   		spv, byteval, wu, wv1, wv2,
   		*sump, *sumw, eps = 1.0e-8;

    dn = (double)npoints;
    vx = x2 - x1;  vy = y2 - y1;  vmax = vx * vx + vy * vy;
    if ((rectwidth < eps) || (npoints < 2) || (vmax < eps))
    	goto Fin;
	if (mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem) != 8)
       	goto Fin;
    sump = (double *)My_malloc(2 * npoints * sizeof(double));
    if (!sump) goto Fin;

    sumw = sump + npoints;
    bytepnts = (BYTE *)points;
    for (ix = 0;  ix < npoints;  ix++)
    {
   	    sump[ix] = 	sumw[ix] = 0.0;
        bytepnts[ix] = 0;
    }
    wu = sqrt(vmax) * rectwidth / 2.0;
    ux = vy / wu;  uy = -vx / wu;
    vx = (vx / vmax) * (dn - eps);
    vy = (vy / vmax) * (dn - eps);
    for (iy = 0;  iy < ye;  iy++)
    {
	    for (ix = 0;  ix < xe;  ix++)
   	    {
      	    x = (double)(ix - x1);  y = (double)(iy - y1);
            spv = x * vx + y * vy;
            if ((spv < 0.0) || (spv >= dn)) continue;
            if ((wu = 1.0 - abs(x * ux + y * uy)) <= 0.0) continue;
			byteval = (double)mem[pitch * iy + ix];
            ip1 = (long)spv;
            if ((wv1 = spv - ip1) < 0.5)
            {
         	    if (ip1 > 0)
                {
             	    ip2 = ip1 - 1;  wv2 = wv1;  wv1 = 1.0 - wv1;
                } else
            {
             	ip2 = ip1;  wv2 = wv1 = 0.5;
            }
            } else
            {
         	    if (ip1 < npoints - 1)
                {
             	    ip2 = ip1 + 1;  wv2 = 1.0 - wv1;
                } else
                {
             	    ip2 = ip1;  wv2 = wv1 = 0.5;
                }
            }
            wv1 *= wu;  wv2 *= wu;
            sump[ip1] += (byteval * wv1);  sumw[ip1] += wv1;
            sump[ip2] += (byteval * wv2);  sumw[ip2] += wv2;
        }
    }
    minp = 255;   maxp = 0;
    for (ix = 0;  ix < npoints;  ix++)
    {
        if (sumw[ix] > 0.0)
        {
   		    bytepnts[ix] = curr = (BYTE)(sump[ix] / sumw[ix] + 0.5);
            if (minp > curr) minp = curr;
            if (maxp < curr) maxp = curr;
        }
    }
    My_free (sump);
    if (minp <= maxp) ret = (minp << 8) | maxp;
Fin:
	return ret;
}


/******************************************************** mrTransp */
/***

 returns:
 		if OK: 0;
        -1 otherwise
***/
long  CZMemRect::mrTransp (long hsrc, long hdest)
{
	long pixs, pixd, xes, yes, xed, yed, pitchs, pitchd,
         rs, rd, no = 0, ret = -1;
   	BYTE *mems, *memd, *bs, *bd;
    DWORD *ls, *ld;

	pixs = mrGetExtendedInfo (hsrc, &xes, &yes, &pitchs, &mems);
Try:
	pixd = mrGetExtendedInfo (hdest, &xed, &yed, &pitchd, &memd);
   	if ((pixs < 1) || (pixd < 1) || (pixs != pixd)) goto Fin;
   	if ((xes != yed) || (yes != xed))
    {
		if (mrRedimension (hdest, pixs, yes, xes) < 0) goto Fin;
        if (no++) goto Fin;
        goto Try;
	}

//   	MaskOper (memd, pixd * xed, yed, pitchd, moZERO, NULL);

   	if (pixs == 1)
   	{
		BitTranspose (mems, memd, xes, yes, pitchs, pitchd);
	  	ret = 0;   	goto Fin;
   	}
   	if (pixs == 8)
    {
	    for (rs = 0;  rs < xes;  rs++)
    	{
			bd = (BYTE *)memd;   bs = (BYTE *)mems;
	        for (rd = 0;  rd < yes;  rd++)
    	    {
                bd[rd] = bs[rs];
            	bs += pitchs;;
            }
            memd += pitchd;
        }
	  	ret = 0;   	goto Fin;
    }
   	if (pixs == 24)
    {
	    for (rs = 0;  rs < xes * 3;  rs += 3)
    	{
			bd = (BYTE *)memd;   bs = (BYTE *)mems;
	        for (rd = 0;  rd < yes * 3;  rd += 3)
    	    {
                bd[rd] = bs[rs];
                bd[rd + 1] = bs[rs + 1];
                bd[rd + 2] = bs[rs + 2];
            	bs += pitchs;;
            }
            memd += pitchd;
        }
	  	ret = 0;   	goto Fin;
    }
   	if (pixs == 32)
    {
	    for (rs = 0;  rs < xes;  rs++)
    	{
            bs = mems;	ld = (DWORD *)memd;
	        for (rd = 0;  rd < yes;  rd++)
    	    {
                ls = (DWORD *)bs;
                ld[rd] = ls[rs];
            	bs += pitchs;;
            }
            memd += pitchd;
        }
	  	ret = 0;   	goto Fin;
    }

Fin:
	return ret;
}


/*********************************************** mrXYtoNA */
/***
    Pixel-to-pixel transformation:

    	hN[i,j] = sqr( hX[i,j]**2 + hY[i,j]**2 ) * mul255
    	hA[i,j] = atan2( hX[i,j], hY[i,j] )

    hX32, hY32 - 32 bit/pix;    hN8, hA8 - 8 bit/pix;
    Argument range is mapped:
    	{-pi, pi} => {0, 255} if flag==0;
    	{0, pi} => {0, 255} if flag!=0;
    Norm is saturated at 255.
    0 is OK for <hA8>.
    Returns: max pixel value before saturation in hN8
    		 or -1
***/
long  CZMemRect::mrXYtoNA (long hX32, long hY32,	long hN8, long hA8, double mul255, long flag)
{
	long xe, ye, xew, yew, pitchx, pitchy, pitchn,
    	 pitcha = 0;
    long *memx, *memy, ret = -1;
   	BYTE *memn,
    	 *mema = NULL;

	if (mrGetExtendedInfo (hX32, &xe, &ye, &pitchx, &memx)
    	 != 32) goto Fin;
	if (mrGetExtendedInfo (hY32, &xew, &yew, &pitchy, &memy)
    	 != 32) goto Fin;
    if ((xe != xew) || (ye != yew))	goto Fin;
	if (mrGetExtendedInfo (hN8, &xew, &yew, &pitchn, &memn)
    	 != 8)	goto Fin;
    if ((xe != xew) || (ye != yew))	goto Fin;
    if (hA8 > 0)
    {
		if (mrGetExtendedInfo (hA8, &xew, &yew, &pitcha, &mema)
    			 != 8)	goto Fin;
	    if ((xe != xew) || (ye != yew))	goto Fin;
    }
 	ret = XY32toN8A8(memx, memy, xe, ye, pitchx, pitchy,
					 memn, mema, pitchn, pitcha, mul255, flag);

Fin:
	return ret;
}


/********************************************************* mrTwoDLUT */
/***
 	Substitutes 8-bit pixels in a given memory rectangle
 	with bytes from a given 2-D table as
	hRow[i, j] = t[hRow[i, j], hCol[i, j]]

	Returns:
		new sum of pixels in the mem. rect. if OK,
     		-1 otherwise
***/
long  CZMemRect::mrTwoDLUT (long hndlRow, long hndlCol, long hndlTab)
{
	long i, k, ret = -1;
	long pixR, xeR, yeR, pitchR;
	long pixC, xeC, yeC, pitchC;
	long pixT, xeT, yeT, pitchT;
   	BYTE c, *row, *col;
	BYTE *memR, *memC, *memT, *t;

	pixR = mrGetExtendedInfo (hndlRow, &xeR, &yeR, &pitchR, &memR);
	pixC = mrGetExtendedInfo (hndlCol, &xeC, &yeC, &pitchC, &memC);
	pixT = mrGetExtendedInfo (hndlTab, &xeT, &yeT, &pitchT, &memT);

   	if (pixR != 8 || pixC != 8 || pixT != 8) goto Fin;
	if (xeR != xeC || yeR != yeC ) goto Fin;
	if (xeT != 256 || yeT != 256 )   goto Fin;

   	t = memT;
	row = memR;
        col = memC;
	i = ret = 0;
   	while (i++ < yeR)
   	{
      		row = memR;  memR += pitchR;
		col = memC;  memC += pitchC;
     		for (k = 0;  k < xeR;  k++)
      		{
   			c = t[row[k]* pitchT + col[k]];
        		ret += (row[k] = c);
      		}
   	}
Fin:
	return ret;
}


/********************************************************* mrGenTwoDLUT */
/***
 	Generates a 2-D look-up table as
	t[i, j] = i * (H - L) / j,
        where H = 255, L = 0, i, j = 0, 1, ..., 255

	Returns:
		new sum of pixels in the mem. rect. if OK,
     		-1 otherwise
***/
long  CZMemRect::mrGenTwoDLUT (long hndl)

{
	long  i, j, ret = -1;
   	long  pix, xe, ye, pitch;
	BYTE  *row, *mem, H = 255, L = 0;
        float temp;
        int   gInt;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
   	if (pix != 8 || xe != 256 || ye != 256 )
	goto Fin;


	for (i=0; i<ye; i++)
	{
		row = i*pitch + mem; 
      		for (j=0; j<xe; j++)
		{
		  if (j == 0)
		     row[j] = 255;
		  else
		  {
		     temp = (float) (i * (H - L)) / (float)j;
		     temp = temp + 0.5f;
		     gInt = (int)temp;
		     if (gInt < 0)   gInt = 0;
		     if (gInt > 255) gInt = 255;
             	     ret += (row[j] = (BYTE)gInt);
		  }
		}
	  }

Fin:
  	return ret;
}


/****************************************************** mrGetMaskPicInfo */
/***
	returns numObjProcessed (long > 0)
        or -1 if failure
***/
long  CZMemRect::mrGetMaskPicInfo(long h1, long h8, long h32, long hSp32, long numComps)
{
	long xe1,  ye1,  pitch1;
	long xe8,  ye8,  pitch8;
	long xe32, ye32, pitch32;
	long xeSp32, yeSp32, pitchSp32;
        long nByPitch32, hseed, nByPitchSp32;
        long ret = -1;
        long n, numObjProcessed;//, j;
        long xc, yc, *pShape, Data[16], *pData;
    	BYTE *mem1,  *mem8,  *mem32, *memSp32;

	if (mrGetExtendedInfo (h1,  &xe1,  &ye1,  &pitch1,  &mem1)  != 1)
    	goto Fin;
	if (mrGetExtendedInfo (h8,  &xe8,  &ye8,  &pitch8,  &mem8)  != 8)
    	goto Fin;
	if (mrGetExtendedInfo (h32, &xe32, &ye32, &pitch32, &mem32) != 32)
    	goto Fin;
	if (mrGetExtendedInfo (hSp32, &xeSp32, &yeSp32, &pitchSp32, &memSp32)
            != 32)
    	goto Fin;
        if (xe1 != xe8 || ye1 != ye8)
    	goto Fin;
        if (xe32 < 3 || ye32 < numComps)
    	goto Fin;

    	numObjProcessed = 0;
    	pShape = (long*)memSp32;
        pData  = (long *)mem32;
        if ((hseed = mrCreateAs(h1)) <= 0) goto Fin;
    	for (n=0; n<numComps; n++)
    	{
      	  nByPitchSp32 = n * xeSp32;
          nByPitch32   = n * xe32;
      	  xc = pShape[nByPitchSp32 + 1];
      	  yc = pShape[nByPitchSp32 + 2];
      	  mrReconFast (h1, hseed, xc, yc);
          mrRectMaskData (h8, hseed, Data);
          if (Data[0] == 0) goto Fin;
          else
            pData[nByPitch32+1] =
            (long)((double)Data[1] / (double)Data[0] + 0.5f);
          pData[nByPitch32+2] = Data[8];
          pData[nByPitch32]   = n;
          numObjProcessed ++;
   	}

        ret = numObjProcessed;

Fin:
        if (hseed > 0) Free (hseed);
	return ret;
}


/****************************************************** mrReadJPEGfile */
/***
	returns handle (long > 0) or -1 if failure
***/

long  CZMemRect::mrReadJPEGfile(char *pathname)
{

    long hndl;
	long BPP=0;
	long P, W, H;
    void *Addr;
	long rez;

	JPEG_CORE_PROPERTIES jcProps;

	 // Initialize jcProps
    IJLERR result = ijlInit(&jcProps);

	// Specify input filename
    jcProps.JPGFile = TEXT(pathname);

	 // Read JPEG parameters from the file
    result = ijlRead(&jcProps, IJL_JFILE_READPARAMS);

	// Get Bits PerPixel
    switch (jcProps.JPGChannels)
    {
        case 1: 
            jcProps.DIBColor = IJL_G; 
            BPP = 8;
            break;
        case 3:  
            jcProps.DIBColor = IJL_BGR;
            BPP = 24;
            break;
        default: 
			// Clean up
			ijlFree(&jcProps);
            return -1;
	}

	hndl=mrCreate(BPP,jcProps.JPGWidth, jcProps.JPGHeight);

	rez=mrGetExtendedInfo(hndl, &W, &H, &P, &Addr);
	
	// Set up the image info for the JPEG decoder
	jcProps.DIBWidth = W;
	jcProps.DIBHeight = -H;      
	jcProps.DIBChannels = jcProps.JPGChannels;
            
    jcProps.DIBPadBytes = P - (jcProps.DIBChannels * W);
	
	// Tell IJL where to store the image
    jcProps.DIBBytes = (unsigned char *) Addr;

	// Read data from the JPEG image into the UTS image
    result = ijlRead(&jcProps, IJL_JFILE_READWHOLEIMAGE);
    if (result != IJL_OK)
    {
       // Image read not successful
       Free(hndl);
       return -1;
    }

     // Clean up
    ijlFree(&jcProps);

	return hndl;
}



/*************************************************** mrDisplayMask */
/***
   if <mirror> != 0 then reverse row order.
	returns:
   	number of ones in the mask if OK
      -1 if bad handle or coordinates
***/
long  CZMemRect::mrDisplayMask (long windhndl, long xshift, long yshift, long hndl, long color, long mirror)

{
	long x, y, pix, xe, ye, pitch,
    	ret = -1, Data[8];
   	BYTE *mem;
	HWND hwnd;
	HDC hdc;

	pix = mrGetExtendedInfo (hndl, &xe, &ye, &pitch, &mem);
   	if (pix != 1) goto Fin;
	hwnd = (HWND)windhndl;
	hdc = GetDC (hwnd);
   	Data[0] = Data[1] = ret = 0;	Data[2] = 1;
   	while (iiplFindInMask(mem, xe, ye, pitch, Data))
   	{
      	if (mirror) { y = ye - 1 - Data[1]; }
      	else { y = Data[1]; }
      	x = Data[0] + xshift;  y += yshift;
      	SetPixel(hdc, x, y, (COLORREF) color);
      	Data[0]++;   ret++;
   	}
	ReleaseDC (hwnd, hdc);
Fin:
	return ret;
}


/*********************************************** mrMemRectToScreen */
/***
 The function copies given 24 bit/pix Mem Rect into given screen part
 Returns:
 		1 if OK,
     	0 otherwise
***/
long  CZMemRect::mrMemRectToScreen (long hndl, long xl, long yt, long invrows)
{
 	long xe, ye, ret = 0;
	HDC hdcScreen, hdcCompatible = 0;
	HBITMAP hbmp;
	//long rez;
    //Graphics::TBitmap *bmap = new Graphics::TBitmap();

    if (mrGetRectInfo(hndl, &xe, &ye) != 24) goto Fin;
/* Create a normal DC and a memory DC for the entire screen. The
 * normal DC provides a "snapshot" of the screen contents. The
 * memory DC keeps a copy of this "snapshot" in the associated
 * bitmap. */
	hdcScreen = CreateDC("DISPLAY", NULL, NULL, NULL);

	if (!hdcScreen) goto Fin;
	
	hdcCompatible = CreateCompatibleDC(hdcScreen);
	
	if (!hdcCompatible) goto Fin;
    
	if (invrows) mrInverseRows(hndl);
	
	
	hbmp = (HBITMAP)mrRectToBitMap (hndl, 0);
    
	if (invrows) mrInverseRows(hndl);
	
	if (!(hbmp)) goto Fin;
/* Select the bitmaps into the compatible DC. */
	if (!SelectObject(hdcCompatible, hbmp)) goto Fin;

/* Copy color data from the bitmap that is selected into
	a compatible DC to the display. */
   	BitBlt(hdcScreen, xl, yt, xe, ye,
                 hdcCompatible, 0, 0, SRCCOPY);
    ret = 1;
Fin:
	if (hdcScreen) DeleteDC (hdcScreen);

   	if (hdcCompatible) DeleteDC (hdcCompatible);
    
	delete hbmp;
	
	return ret;
}





/*************************************************** mrTextLine */
/***
	returns: ((yef << 16) | xef);
***/
long  CZMemRect::mrTextLine(long hndl, long x, long y, char *txt, long height, long color)
{
 	long pix, xe, ye, xef, yef, yy, xe0,
    	 xe1, ye1, pitch1, h1, h2;//, hh;
    BYTE *mem1;//, *sline;
    char *stxt = txt;
    //Graphics::TBitmap *bmap1 = new Graphics::TBitmap();
	int len;

	HBITMAP hbmp;
	HDC hdc;
	HDC hdcCompatible;
	SIZE pSize;
	BOOL rez;
	HFONT hfnt;
	HGDIOBJ hgdi;
	//BITMAP bmp;
	BITMAPINFOHEADER bmpiheader;
	BITMAPINFO bmpinfo;
	RGBQUAD rgbq;

	len = strlen(txt);

	//Create font
	hfnt= (HFONT) GetStockObject(DEFAULT_GUI_FONT);
	
	hdc = GetDC(NULL);

	//Create compatible DC
	hdcCompatible = CreateCompatibleDC(hdc);

	if (!hdcCompatible) 
	{
		ReleaseDC(NULL,hdc);
		return 0;
	}

	// Fill out pSize structure
	rez = GetTextExtentPoint32(hdcCompatible, stxt, strlen(stxt), &pSize);

	xef = pSize.cx;
	//xe0 = (x + xef + 31) & 0xffffffe0;
	xe0 = (xef + 31) & 0xffffffe0;
	//xe0=xef;

	yef = pSize.cy;
	
	//Assign font 
	hgdi=SelectObject(hdcCompatible, hfnt);
	
	bmpiheader.biSize=sizeof(BITMAPINFOHEADER);
	bmpiheader.biWidth = xe0;
	bmpiheader.biHeight = yef;
	bmpiheader.biBitCount = 1;
	bmpiheader.biCompression = BI_RGB;
	bmpiheader.biSizeImage = 0;
	bmpiheader.biPlanes = 1;

	rgbq.rgbBlue=255;
	rgbq.rgbGreen=255;
	rgbq.rgbRed=255;

	bmpinfo.bmiHeader = bmpiheader;

	pix = mrGetRectInfo (hndl, &xe, &ye);

    if (y < 0) y = 0;
    if (xe0 > xe)
    {
    	x = (xef > xe) ? 0 : (xe - xef) & 0xffffffe0;
        xe0 = xe & 0xffffffe0;
    }
    if (y + yef > ye)
    {
    	if (yef > ye)
        {
        	y = 0;  yef = ye;
        } else y = ye - yef;
    }

	
	hbmp=CreateCompatibleBitmap(hdcCompatible, xe0 , yef) ;

	hgdi=SelectObject(hdcCompatible, hbmp);
	
	//SetMapMode(hdcCompatible, MM_TEXT);
	SetBkMode(hdcCompatible, TRANSPARENT);
	SetTextColor(hdcCompatible,RGB(255,255,255));
	
	//Do TextOut
	rez=TextOut(hdcCompatible, 0,0 , stxt, len);
	
	//h1 = mrCreate(1, xe0, yef);
   	h2 = mrCreateOverlay(hndl, x, y, xe0, yef);
	
	h1 = mrBitmapToMemRect(hbmp, (height < 0) ? 1 : 0); 

	long h3 = 0;

	mrGetExtendedInfo (h1, &xe1, &ye1, &pitch1, &mem1);

	yy=yef;

	long yind=0;
	while (yy--)
	{
		yind = yy;
		rez=GetDIBits(hdc, hbmp, yind, 1, mem1, &bmpinfo, DIB_RGB_COLORS);
		TableSubst((char*)mem1,(char*) mem1, xe0 >> 3, NULL);
		//memcpy(mem1, mem3, sizeof mem3);		
		mem1 += pitch1;
	}
	
	mrInverseRows(h1);
	//rez = mrWriteToFile(h1, "C:\\AK\\h1.bmp",0);

    if (pix == 1)
    {
        if (color & 1)
        	Bitwise (h1, "~1|2", h2);
        else
			Bitwise (h1, "And", h2);
	} else
		mrConstPixelNotMask(color, "Copy", h2, h1);


	//rez=mrWriteToFile(h2, "C:\\AK\\h2.bmp",0);
	
	ReleaseDC(NULL,hdc);
	DeleteDC(hdcCompatible);
	DeleteObject(hfnt);
	DeleteObject(hbmp);
    Free(h1);
    Free(h2);


	return ((yef << 16) | xef);
}


/*********************************************** mrScreenToMemRect */
/***
 The function creates 24 bit/pix Mem Rect from given screen part
 Returns:
 		(created) handle if OK,
     	0 otherwise
***/
DWORD  CZMemRect::mrScreenToMemRect (long xl, long yt, long wid, long hei, long invrows)
{
    DWORD hndl = 0;
   	HBITMAP hbmp = 0;
    BITMAP bmp;

	//Graphics::TBitmap *bmap = new Graphics::TBitmap();



    hbmp = (HBITMAP)ScreenToBitMap (xl, yt, wid, hei);
    if (! hbmp) goto Fin;

	if (GetObject(hbmp, sizeof(bmp), &bmp) == 0)
		return 0;
	
	bmp.bmBitsPixel=24;

	//bmap->Handle = hbmp;
    //bmap->PixelFormat = pf24bit;
    hndl = mrBitmapToMemRect (hbmp, invrows);
    delete hbmp;

Fin:
	return hndl;
}


void CZMemRect::mrCutImageFrag(long SrcImage, long CentreX, long CentreY, long DestImage, long *Xleft, long *Ytop)
{
    long Xl, Yt, Xe, Ye, FragSizeW, FragSizeH;

    // Got a valid source image
    if (SrcImage > 0 && DestImage > 0)
    {
        // Get the extents of the image
        mrGetRectInfo(SrcImage, &Xe, &Ye);
        // Get the extents of the fragment
        mrGetRectInfo(DestImage, &FragSizeW, &FragSizeH);
        
        // Find XLeft and YTop of fragment and
        // re-adjust extents so that we dont cut over the image edges
        Xl = CentreX - FragSizeW / 2;
        
        if (Xl < 0)
            Xl = 0;
        else
        {
            if (Xl + FragSizeW > Xe)
                Xl = Xe - FragSizeW;
        }    
        Yt = CentreY - FragSizeH / 2;
        
        if (Yt < 0)
            Yt = 0;
        else
        {
            if (Yt + FragSizeH > Ye)
                Yt = Ye - FragSizeH;
        }    
        // Perform the cut
		mrMoveSubRect(SrcImage, Xl, Yt, DestImage, "=>");
    }
}












