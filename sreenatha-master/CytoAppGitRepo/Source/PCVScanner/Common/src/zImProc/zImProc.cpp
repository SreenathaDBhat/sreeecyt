// zImProc.cpp : Defines the entry point for the DLL application.
//
#include "stdafx.h"


#include <stdio.h>
#include <stdlib.h>
#include <math.h>


#include "zImProc.h"
#include "zGeneric.h"



BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}






/*#################################################################*/

//  0  1  2  3   4  5  6  7   8  9  A  B   C  D  E  F
static BYTE BitsInByte[] =
{
	0, 1, 1, 2,  1, 2, 2, 3,  1, 2, 2, 3,  2, 3, 3, 4,  // 0..
	1, 2, 2, 3,  2, 3, 3, 4,  2, 3, 3, 4,  3, 4, 4, 5,  // 1..
	1, 2, 2, 3,  2, 3, 3, 4,  2, 3, 3, 4,  3, 4, 4, 5,  // 2..
    2, 3, 3, 4,  3, 4, 4, 5,  3, 4, 4, 5,  4, 5, 5, 6,  // 3..
	1, 2, 2, 3,  2, 3, 3, 4,  2, 3, 3, 4,  3, 4, 4, 5,  // 4..
    2, 3, 3, 4,  3, 4, 4, 5,  3, 4, 4, 5,  4, 5, 5, 6,  // 5..
    2, 3, 3, 4,  3, 4, 4, 5,  3, 4, 4, 5,  4, 5, 5, 6,  // 6..
    3, 4, 4, 5,  4, 5, 5, 6,  4, 5, 5, 6,  5, 6, 6, 7,  // 7..
	1, 2, 2, 3,  2, 3, 3, 4,  2, 3, 3, 4,  3, 4, 4, 5,  // 8..
    2, 3, 3, 4,  3, 4, 4, 5,  3, 4, 4, 5,  4, 5, 5, 6,  // 9..
    2, 3, 3, 4,  3, 4, 4, 5,  3, 4, 4, 5,  4, 5, 5, 6,  // A..
    3, 4, 4, 5,  4, 5, 5, 6,  4, 5, 5, 6,  5, 6, 6, 7,  // B..
    2, 3, 3, 4,  3, 4, 4, 5,  3, 4, 4, 5,  4, 5, 5, 6,  // C..
    3, 4, 4, 5,  4, 5, 5, 6,  4, 5, 5, 6,  5, 6, 6, 7,  // D..
    3, 4, 4, 5,  4, 5, 5, 6,  4, 5, 5, 6,  5, 6, 6, 7,  // E..
    4, 5, 5, 6,  5, 6, 6, 7,  5, 6, 6, 7,  6, 7, 7, 8   // F..
};

//  0  1  2  3   4  5  6  7   8  9  A  B   C  D  E  F
static BYTE MaskToContrastTable[] =
{
	0, 1, 1, 1,  1, 2, 1, 1,  1, 1, 2, 1,  2, 2, 2, 1,  // 0..
	1, 2, 2, 2,  1, 2, 1, 1,  2, 2, 3, 2,  2, 2, 2, 1,  // 1..
	1, 2, 2, 2,  2, 3, 2, 2,  1, 1, 2, 1,  2, 2, 2, 1,  // 2..
    2, 3, 3, 3,  2, 3, 2, 2,  2, 2, 3, 2,  2, 2, 2, 1,  // 3..
	1, 2, 2, 2,  2, 3, 2, 2,  2, 2, 3, 2,  3, 3, 3, 2,  // 4..
    2, 3, 3, 3,  2, 3, 2, 2,  3, 3, 4, 3,  3, 3, 3, 2,  // 5..
	1, 2, 2, 2,  2, 3, 2, 2,  1, 1, 2, 1,  2, 2, 2, 1,  // 6..
    2, 3, 3, 3,  2, 3, 2, 2,  2, 2, 3, 2,  2, 2, 2, 1,  // 7..
	1, 2, 2, 2,  2, 3, 2, 2,  2, 2, 3, 2,  3, 3, 3, 2,  // 8..
	1, 2, 2, 2,  1, 2, 1, 1,  2, 2, 3, 2,  2, 2, 2, 1,  // 9..
	2, 3, 3, 3,  3, 4, 3, 3,  2, 2, 3, 2,  3, 3, 3, 2,  // A..
    2, 3, 3, 3,  2, 3, 2, 2,  2, 2, 3, 2,  2, 2, 2, 1,  // B..
	1, 2, 2, 2,  2, 3, 2, 2,  2, 2, 3, 2,  3, 3, 3, 2,  // C..
	1, 2, 2, 2,  1, 2, 1, 1,  2, 2, 3, 2,  2, 2, 2, 1,  // D..
	1, 2, 2, 2,  2, 3, 2, 2,  1, 1, 2, 1,  2, 2, 2, 1,  // E..
    1, 2, 2, 2,  1, 2, 1, 1,  1, 1, 2, 1,  1, 1, 1, 0   // F..
};





#define Work4KSize  0x4000
static BYTE Work4K[Work4KSize+16];


/*********************************************************** MinMax */
/*  oper: 0 - MIN, else MAX. */
BYTE MinMax (BYTE a, BYTE b, long oper)
{
	register BYTE ret;

   	if (a <= b)
   	{  if (oper) ret = b;  else ret = a; }
   	else
   	{  if (oper) ret = a;  else ret = b; }
   	return ret;
}


/***************************************************** InterpolRow */
/***
	Function:	InterpolRow

	Description:
		This function interpolates bytes from one
		row to another, which may be of different size;
		new row valuew and old ones averages with
		given wages
		memres = ( wage1 * Interpolation(mem1)+ wage2 * mem2 )
				 / (wage1 + wage2)

	Arguments:
		mem1 - pointer to the source row
		len1 - size of the source row
		mem2 - pointer to the destination row
		memres - pointer to the destination row
		len2 - (common) size of the destination and mem2 rows

	Returns:
		0 if not Ok,
		1 if Small To Big,
		2 if Big To Small

***/
long InterpolRow (BYTE *mem1, DWORD len1,
			BYTE *mem2, BYTE *memres, DWORD len2,
			DWORD wage1, DWORD wage2)
{
	long cnt, scnt, bcnt, blim, slim, ww,
		 n, nn, k, i, m, ret = 0;
	BYTE *big, *small, *wrk, *res;
	DWORD prev, oldval, newval, nxt;

	if ((len1 < 2) || (len2 < 2)) goto Fin;
	ww = wage1 + wage2;
	res = memres;
	if (len1 > len2) goto LongToShort;
// Short to Long:
	ret = 1;
	big   = mem2; 	blim = len2;
	small = mem1; 	slim = len1;
	bcnt = 0;	cnt = blim + 1;
	nxt = *small;
	while (bcnt++ < blim)
	{
		if (cnt > blim)
		{
			cnt -= blim;
			prev = nxt;
			nxt = *small++;
		}
		i = (prev * (blim - cnt) + nxt * cnt) + (blim / 2);
		newval = i = i / blim;
		if (wage2 > 0)
		{
			oldval = *big++;
			newval = i * wage1 + oldval * wage2 + ww / 2;
			newval /= ww;
		}
		*res++ = (char)newval;
		cnt += slim;
	}
	goto Fin;

LongToShort:
	ret = 2;
	big   = mem1; 	blim = len1;
	small = mem2; 	slim = len2;
	scnt = 0;	cnt = blim / 2;
	nn = (blim / slim) + 1;	 n = nn / 2;
	while (scnt++ < slim)
	{
		bcnt = cnt / slim;
		i = bcnt - n;  	if (i < 0) i = 0;
		k = i + nn;		if (k > blim) k = blim;
		m = k - i;		prev = m / 2;
		wrk = big;  	wrk += i;
		while (i++ < k) prev += *wrk++;
		newval = prev / m;
		if (wage2 > 0)
		{
			oldval = *small++;
			newval = newval * wage1 + oldval * wage2;
			newval /= ww;
		}
		*res++ = (BYTE)newval;
		cnt += blim;
	}
Fin:
	return ret;
}


/**************************************************** MaskPolyLine */
/***
	Function:	MaskPolyLine

	Description:
		This function makes code of polyline in given buffer;
		additional data is located in the array of longs

	Arguments:
		pcodes - pointer to the buffer
        bufsize - size of the buffer
		data - pointer to the array of longs; draw polyline
      		from {Data[0], Data[1]} to {Data[2], Data[3]},
            then to {Data[4], Data[5]}, and so on
		count - number of sections

	Returns:
		number of written to <pcodes> pixels

***/
long MaskPolyLine (void *pcodes, long bufsize,
					long count, long *data)
{
    long dx, dy, i, ix, iy, s, tail;
    BYTE *codes;

    i = s = 0;   codes = (BYTE *)pcodes;
    ix = data[i++];  iy = data[i++];
    while ((count--) && ((tail = bufsize - s) > 0))
    {
		dx = data[i++] - ix;	ix += dx;
        dy = data[i++] - iy;    iy += dy;
       	s += CodeLine(codes + s, tail, dx, dy);
	}
	return s;
}



/********************************************************* MaskArc */
/***
	Function:	MaskArc

	Description:
		This function draws an arc on given BitMask;
		additional data is located in the array of longs

	Statics/Globals:
		None

	Input Arguments:
		mask - pointer to the source Rectangle
		xe - number of pixels in each row
		ye - number of rows
		pitch - address difference between neighboring rows (in bytes)
				in the source Rectangle
		data - pointer to the array of longs:
            ark with center in {Data[0], Data[1]}
            from {Data[2], Data[3]} to {Data[4], Data[5]}
            in clockwise direction
        pen -	0, 1 - put 0/1 pixel; >1 - inverse the pixel.

	Returns:
		number of written bytes

***/
long MaskArc (void *dst, long dstsize,
              long *period, long *pbegoffset,
              long *data)
{
    long xc, yc, count, s, n,
        dx, dy, fdx, fdy, adx, ady, afdx, afdy,
        rad, radrad, radmax, begoffset, finoffset;

	xc = data[0];  			yc = data[1];
    dx = data[2] - xc;  	dy = data[3] - yc;
	fdx = data[4] - xc; 	fdy = data[5] - yc;
    radrad = dx * dx + dy * dy;
    rad = max((long)(sqrt((long double)radrad) + 0.5), 1L);
    radmax = dstsize * 10 / 57;  // limit/4sqrt(2)
    if (rad > radmax)
    {  // straight line
    	n = MaskPolyLine (dst, dstsize, 1, data + 2);
    	s = n;   *period = 0;   *pbegoffset = 0;
        goto Fin;
    }
    n = CodeCircle(dst, dstsize, rad, radrad);
    adx = abs(dx);  ady = abs(dy);
    if (adx <= ady)
    {  // octants near vertical axis, use x
        if (dy > 0) begoffset = (n + dx) % n;
        else begoffset = n / 2 - dx;
    } else
    {  // octants near horizontal axis
        if (dx > 0) begoffset = n / 4 - dy;
        else begoffset = n * 3 / 4 + dy;
    }
    afdx = abs(fdx);  afdy = abs(fdy);
    if (afdx <= afdy)
    {  // octants near vertical axis, use x
        if (fdy > 0) finoffset = (n + fdx) % n;
        else finoffset = n / 2 - fdx;
    } else
    {  // octants near horizontal axis
        if (fdx > 0) finoffset = n / 4 - fdy;
        else finoffset = n * 3 / 4 + fdy;
    }
    count = (n + finoffset - begoffset) % n;
    if (count == 0) count = n;
    s = count;   *period = n;   *pbegoffset = begoffset;
Fin:
	return s;
}





/********************************************************* PictHist */
/***
	Function:	PictHist

	Description:
		This function calculates histogram of pixel values and
		sum of pixel values

	Statics/Globals:
		None

	Input Arguments:
		Pmem - pointer to the source Rectangle

		Xe - number of pixels (bytes) in each row

		Ye - number of rows

		Pitch - address difference between neighboring rows (in bytes)
				in the source Rectangle
	Output Arguments:

		hist - pointer to the array of longs with histogram values,
				at least 256 entries.
				hist[i] = number of pixels with value 'i', i=0-255

	Returns:
		Sum of pixel values
****/
DWORD _stdcall PictHist (
			BYTE *Pict, DWORD Xe, DWORD Ye, DWORD Pitch,
            DWORD *hist)
{
	DWORD ix, iy, summa = 0L;
	WORD ih;
	BYTE *pixels;

	for (ih = 0;  ih < 256;  ih++) hist[ih] = 0L;
	for (iy = 0;  iy < Ye;  iy++)
	{
		pixels = Pict;  Pict += Pitch;  ix = Xe;
		while (ix--)
        { ih = *pixels++;  summa += ih;  hist[ih]++; }
	}
	return summa;
}


/***************************************************** Spline3by2 */
/***
	Function:	Spline3by2

	Description:
		This function calculates spline matrix 3*2 for
        <count> intervals

	Statics/Globals:
		None

	Input Arguments:
		count - number of intervals
        incs - pointer to array of increments
        m3by2 - pointer to result:
        		a11 a12 d1
        		a21 a22 d2
	Returns:

***/
void _stdcall  Spline3by2 (long count, double *incs, double *m3by2)
{
	double a11, a12, a21, a22, d1, d2, w1, w2, ww,
    	c11 = -2.0, c12 = -1.0, c21 = -3.0, c22 = -2.0;
    WORD i = 0;

    a11 = a22 = 1.0;  a12 = a21 = d1 = d2 = 0.0;
    while (i < count)
    {
     	w1 = c11 * a11 + c12 * a21;
     	w2 = c21 * a11 + c22 * a21;
        a11 = w1;  a21 = w2;
     	w1 = c11 * a12 + c12 * a22;
     	w2 = c21 * a12 + c22 * a22;
        a12 = w1;  a22 = w2;
        ww = 3.0 * incs[i++];
     	w1 = c11 * d1 + c12 * d2 + ww;
     	w2 = c21 * d1 + c22 * d2 + ww;
        d1 = w1;  d2 = w2;
    }
    i = 0;
    m3by2[i++] = a11;  m3by2[i++] = a12;  m3by2[i++] = d1;
    m3by2[i++] = a21;  m3by2[i++] = a22;  m3by2[i] = d2;
}


/***************************************************** SplineStart */
/***
	Function:	SplineStart

	Description:
		This function calculates spline start coefficients
		y = a1*X + a2*X*X + (DELTA-a1-a2)*X*X*X
	Statics/Globals:
		None

	Input Arguments:
		count - number of intervals
        incs - pointer to array of increments
        a1, a2 - pointers to result coefficients
        mode:
        	mlSPLINE0 - 1st and 2nd derivatives are equal in
            	first and last points
        	mlSPLINE1 - 2nd derivatives are equal to zero in
            	first and last points
        	mlSPLINE2 - unclosed spline with straight first interval
        	mlSPLINE3 - unclosed spline with straight last interval

	Returns:

***/
void _stdcall  SplineStart (long count, double *incs,
					double *a1, double *a2, long mode)
{
	double w, det, m3by2[6], eps = 1.0E-9;

    *a1 = *a2 = 0.0;  // bad result
    Spline3by2 (count, incs, m3by2);
	switch (mode)
    {
       case mlSPLINE0:
         // m11*a1 + m12*a2 + m13 = a1
         // m21*a1 + m22*a2 + m23 = a2
         // a1 = (m23*m12 - m13 * (m22-1)) / det;
         // a2 = (m13*m21 - m23 * (m11-1)) / det;
    	 // det = (m11-1)*(m22-1) - m12*m21
         det = (m3by2[0] - 1.0) * (m3by2[4] - 1.0)
         		 - m3by2[1] * m3by2[3];
		 if (fabs(det) > eps)
         {
		 	*a1 = (m3by2[5] * m3by2[1]
            	 - m3by2[2] * (m3by2[4] - 1.0)) / det;
		 	*a2 = (m3by2[2] * m3by2[3]
            	 - m3by2[5] * (m3by2[0] - 1.0)) / det;
         }
         break;
       case mlSPLINE1:
		 // a2==0 in both ends
         // m11*a1 + m13 = a1'
         // m21*a1 + m23 = 0
         // a1 = -(m23 / m21);
         // a1' = m13 + m11 * a1;
		 if (fabs(m3by2[3]) > eps)
         {
		 	*a1 = -(m3by2[5] / m3by2[3]);
		 	*a2 = 0;
         }
         break;
       case mlSPLINE2:
         *a1 = incs[0];
         *a2 = 0;
         break;
       case mlSPLINE3:
         // m11*a1 + m12*a2 + m13 = -incs[count-1]
         // m21*a1 + m22*a2 + m23 = 0
         // a1 = (m23*m12 - (m13 + incs[count-1]) * m22) / det;
         // a2 = ((m13 + incs[count-1]) * m21 - m23*m11) / det;
    	 // det = m11*m22 - m12*m21
         det = m3by2[0] * m3by2[4] - m3by2[1] * m3by2[3];
		 if (fabs(det) > eps)
         {
            w = m3by2[2] + incs[count-1];
		 	*a1 = (m3by2[5] * m3by2[1]
            	   - w * m3by2[4]) / det;
		 	*a2 = (w * m3by2[3]
            	   - m3by2[5] * m3by2[0]) / det;
         }
         break;

       default :  ;
    }
}


/***************************************************** SplineNext */
/***
	Function:	SplineNext

	Description:
		This function being given a1, a2 and DELTA
        recalculates a1 and a2 for next step:
		a1 = -2*a1 -   a2 + 3*DELTA
		a2 = -3*a1 - 2*a2 + 3*DELTA
	Statics/Globals:
		None

	Input Arguments:
        a1, a2 - pointers to coefficients
        DELTA - function increment
	Returns:

***/
void SplineNext (double delta, double *a1, double *a2)
{
	double w1, w2;

    w1 = *a1;  w2 = *a2;
    *a1 = 3.0 * delta - 2.0 * w1 - w2;
    *a2 = 3.0 * (delta - w1) - 2.0 * w2;
}


/***************************************************** MaskSpline */
/***
	Description:
		This function draws a spline on given BitMask;
		additional data is located in the array of longs

	Statics/Globals:
		Work4K, Work4KSize

	Input Arguments:
		pcodes - pointer to the buffer
        bufsize - size of the buffer
		data - pointer to the array of longs; draw spline
      		from {Data[0], Data[1]} to {Data[2], Data[3]},
            then to {Data[4], Data[5]}, and so on
		count - number of sections

	Returns:
		number of written to <pcodes> pixels

***/
long MaskSpline (void *pcodes, long bufsize,
           	    long count, long *data, long linemode)
{
	long ix, iy, oldx, oldy, mdx, mdy,
        codesize, nc,
    	i = 0, k = 0, s = 0;
    double xa1, xa2, xa3, ya1, ya2, ya3, dx, dy,
    	*dxs, *dys;
	BYTE *codes;

    codes = (BYTE *)pcodes;
    codesize = bufsize - 2 * count * sizeof(double);
    dxs = (double *)(codes + codesize);
    dys = dxs + count;
    oldx = data[i++];  oldy = data[i++];
    while (k < count)
    {
    	ix = data[i++];  iy = data[i++];
        dxs[k] = (double)(ix - oldx);
        dys[k++] = (double)(iy - oldy);
	  	oldx = ix;  oldy = iy;
    }
	SplineStart (count, dxs, &xa1, &xa2, linemode);
	SplineStart (count, dys, &ya1, &ya2, linemode);
    for (k = 0;  k < count;  k++)
    {
    	dx = dxs[k];  dy = dys[k];
        xa3 = dx - xa1 - xa2;  ya3 = dy - ya1 - ya2;
		nc = CodeSplineSeg(codes + s, codesize - s, &mdx, &mdy,
                	xa1, xa2, xa3, ya1, ya2, ya3);
        if ((s += nc) >= codesize) break;
		SplineNext (dx, &xa1, &xa2);
		SplineNext (dy, &ya1, &ya2);
    }
	return s;
}


/************************************************** MaskShadowMask */
/***
	Function:	MaskShadowMask - NOT EXPORTED

	Description:
		This function implements 8 listed operations of type
				Oper(bitmask1) => bitmask2.
		All operations expand ONEs or ZEROs in the specified direction

	Statics/Globals:
		None

	Input Arguments:
		pBitMem1 - pointer to the source Bit Mask

		Xe - number of pixels (bits) in each row

		Ye - number of rows

		Pitch1 - address difference between neighboring rows (in bytes)
				in the source Bit Mask
		pBitMem2 - pointer to the target Bit Mask

		Pitch2 - address difference between neighboring rows (in bytes)
				in the target Bit Mask
		oper - 	Oper(bitmask1) => bitmask2

			mnmShadowLR1  =4   expand ones left to right
			mnmShadowRL1  =5   expand ones right to left
			mnmShadowTB1  =6   expand ones top to bottom
			mnmShadowBT1  =7   expand ones bottom to top
			mnmShadowLR0  =8   expand zeros left to right
			mnmShadowRL0  =9   expand zeros right to left
			mnmShadowTB0 =10   expand zeros top to bottom
			mnmShadowBT0 =11   expand zeros bottom to top

	Output Arguments:
		None

	Returns:
		None

***/
void MaskShadowMask (
				BYTE *BitMem1,
                DWORD Xe, DWORD Ye, DWORD Pitch1,
				BYTE *BitMem2, DWORD Pitch2,
                short oper)
{
	WORD prev, curr, ll, before, after, mask, cmask, wrk;
	DWORD xcnt, ycnt, bytecnt;
	BYTE *BitRowR, *BegBitRowR, *BitRowW, *BegBitRowW;
	WORD frame[] = {0xff00, 0xff00, 0xff00, 0xff00, 0x00ff, 0x00ff, 0x00ff, 0x00ff};

	BegBitRowR = BitMem1;  BegBitRowW = BitMem2;
	bytecnt = (Xe + 7) >> 3;
	before = (WORD)(frame[oper - 4] & 0xff);
	after = (WORD)(frame[oper - 4] >> 8);
	mask = (WORD)(0xff >> ((8 - (Xe & 7)) & 7));
	if ((oper == mnmShadowLR1) || (oper == mnmShadowLR0))
	{  // left to right
		ycnt = 0;
		while (ycnt++ < Ye)
		{
			BitRowR = BegBitRowR;  BitRowW = BegBitRowW;
			xcnt = 0;  prev = before;
			while ((xcnt++) < bytecnt)
			{
				if (prev == after)
				{
					curr = after;
				} else
				{
					curr = *BitRowR++;
					if (curr != before)
					{
						prev = after;
						curr ^= before;
						curr = (WORD)(((curr - 1) ^ 0xff) | curr);
						curr ^= before;
					}
				}
				if (xcnt == bytecnt)
				{
					 ll = *BitRowW;
					 ll = (ll & mask) ^ ll;
					 curr = (curr & mask) | ll;
				}
				*BitRowW++ = curr;
			}
			BegBitRowR += Pitch1;  BegBitRowW += Pitch2;
		}
		goto Fin;
	}

	if ((oper == mnmShadowRL1) || (oper == mnmShadowRL0))
	{  // right to left
		ycnt = 0;
		BegBitRowR += bytecnt;
		BegBitRowW += bytecnt;
		while (ycnt++ < Ye)
		{
			BitRowR = BegBitRowR;  BitRowW = BegBitRowW;
			xcnt = bytecnt;  prev = before;  cmask = mask;
			while (xcnt--)
			{
				BitRowR--;  BitRowW--;
				if (prev == after)
				{
					curr = after;
				} else
				{
					curr = (WORD)((*BitRowR) & cmask);
					if (curr != before)
					{
						prev = after;   wrk = curr;
						curr ^= before;
						while ((curr |= (WORD)((curr >> 1))) != wrk)
							wrk = curr;
						curr ^= before;
					}
				}
				if (cmask != 0xff)
				{
					 ll = *BitRowW;
					 ll = (ll & mask) ^ ll;
					 curr = (curr & mask) | ll;
				}
				*BitRowW = curr;  cmask = 0xff;
			}
			BegBitRowR += Pitch1;  BegBitRowW += Pitch2;
		}
		goto Fin;
	}

	if ((oper == mnmShadowTB1) || (oper == mnmShadowTB0))
	{  // top to bottom
		xcnt = 0;  cmask = 0xff;
		while (xcnt++ < bytecnt)
		{
			BitRowR = BegBitRowR;  BitRowW = BegBitRowW;
			ycnt = 0;  prev = before;
			if (xcnt == bytecnt) cmask = mask;
			while ((ycnt++) < Ye)
			{
				curr = (*BitRowR);
				if (oper == mnmShadowTB1) prev |= curr;
				else prev &= curr;
				if (cmask != 0xff)
				{
					 ll = *BitRowW;
					 ll = (ll & cmask) ^ ll;
					 curr = (prev & cmask) | ll;
				} else
					curr = prev;
				*BitRowW = curr;
				BitRowR += Pitch1;	BitRowW += Pitch2;
			}
			BegBitRowR++;  BegBitRowW++;
		}
		goto Fin;
	}

	if ((oper == mnmShadowBT1) || (oper == mnmShadowBT0))
	{  // bottom to top
		xcnt = 0;  cmask = 0xff;
		BegBitRowR += (Pitch1 * (Ye - 1));  BegBitRowW += (Pitch2 * (Ye - 1));
		while (xcnt++ < bytecnt)
		{
			BitRowR = BegBitRowR;  BitRowW = BegBitRowW;
			BegBitRowR++;  BegBitRowW++;
			ycnt = Ye;  prev = before;
			if (xcnt == bytecnt) cmask = mask;
			while (ycnt--)
			{
				curr = (*BitRowR);
				if (oper == mnmShadowBT1) prev |= curr;
				else prev &= curr;
				if (cmask != 0xff)
				{
					 ll = *BitRowW;
					 ll = (ll & cmask) ^ ll;
					 curr = (prev & cmask) | ll;
				} else
					curr = prev;
				*BitRowW = curr;
				BitRowR -= Pitch1;	BitRowW -= Pitch2;
			}
		}
		goto Fin;
	}
Fin:
	return;
}


//********************************************************** RowToRow
/***
	Function:	RowToRow

	Description:
		This function moves (replicates) bytes from one
		row to another, which may be of different size

	Arguments:
		mem1 - pointer to the source row
		len1 - length of the source row
		mem2 - pointer to the destination row
		len2 - length of the destination row
        portion - bytes per pixel count

	Returns:
		0 if not Ok,
		1 if Small To Big,
		2 if Big To Small

***/
DWORD RowToRow (BYTE *mem1, DWORD len1,
				BYTE *mem2, DWORD len2, DWORD portion)
{
	DWORD cnt, scnt, bcnt, blim, slim, i, ret = 0;
	BYTE *big, *small, *wrk;

	if ((len1 < 2) || (len2 < 2) || (portion < 1)) goto Fin;
	if (len1 > len2) goto BigToSmall;
// Small To Big:
	ret = 1;
	big   = mem2; 	blim = len2;
	wrk = small = mem1; 	slim = len1;
	bcnt = 0;	cnt = blim + 1;
	while (bcnt++ < blim)
	{
		if (cnt > blim)
		{
			cnt -= blim;
            wrk = small;
			small += portion;
		}
        for (i = 0;  i < portion;  i++) big[i] = wrk[i];
        big += portion;
		cnt += slim;
	}
	goto Fin;

BigToSmall:
	ret = 2;
	big   = mem1; 	blim = len1;
	small = mem2; 	slim = len2;
	scnt = 0;	cnt = blim / 2;
	while (scnt++ < slim)
	{
		bcnt = cnt / slim;
		wrk = big + (bcnt * portion);
        for (i = 0;  i < portion;  i++) small[i] = wrk[i];
		small += portion;
		cnt += blim;
	}
Fin:
	return ret;
}


/***************************************************** BitRowToRow */
/***
	Function:	BitRowToRow

	Description:
		This function moves (replicates) bits from one
		row to another, which may be of different length

	Arguments:
		mem1 - pointer to the source row
		len1 - length of the source row in bits
		mem2 - pointer to the destination row
		len2 - length of the destination row in bits

	Returns:
		0 if not Ok,
		1 if Small To Big,
		2 if Big To Small

***/
long _stdcall BitRowToRow (char *mem1, DWORD len1,
				char *mem2, DWORD len2)
{
	DWORD cnt, scnt, bcnt, blim, slim;
    WORD accum, one, mask, yes, no;
    long ret = 0;
	BYTE c, *r, *w;

	if ((len1 < 2) || (len2 < 2)) goto Fin;
    r = (BYTE *)mem1;  w = (BYTE *)mem2;
    bcnt = scnt = 0;
    accum = 0;   one = 1;
	if (len1 > len2) goto BigToSmall;
// Small To Big:
	blim = len2;  slim = len1;
	cnt = blim - slim;   ret = 1;
	while (bcnt++ < blim)
	{
		if ((cnt += slim) >= blim)
		{	// next source bit
			cnt -= blim;
	    	if (!((scnt++) & 7)) c = *r++; else c >>= 1;
            mask = (WORD)(c & 1);
		}
        if (mask) accum |= one;
        if ((one <<= 1) & 0x100)
        {
        	*w++ = accum;  accum = 0;  one = 1;
        }
	}
	goto Fin;
BigToSmall:
    blim = len1;   slim = len2;
    cnt = 0;  yes = no = 0;	 ret = 2;
	while (bcnt++ < blim)
	{
		// next source bit
        if ((bcnt & 7) == 1) c = *r++; else c >>= 1;
        if (c & 1) yes++; else no++;
		if ((cnt += slim) >= blim)
		{	// next dest bit
            if (yes >= no) accum |= one;
//	        accum |= (one & mask);
    	    if ((one <<= 1) & 0x100)
        	{
        		*w++ = accum;
	            accum = 0;  one = 1;
    	    }
			cnt -= blim;  yes = no = 0;
        }
	}
Fin:
    if (one != 1) *w = accum;
	return ret;
}


/****************************************************** BitsToRGB */
/****
	Transform 1 bit pixels into 24 according to color coefficients
   (in range from 0 to 255); saturation at 255.
 	Negative coefficient means 'use inverted pixel value'.
   add: 0 - Fill;  1 - Add
   Returns: nothing
****/
void _stdcall BitsToRGB (
			BYTE *mem1, DWORD xe, DWORD ye, DWORD pitch1,
			BYTE *mem24, DWORD pitch24,
			long red, long green, long blue, long add)
{
	short i;
	DWORD xcnt, ycnt, calc, pix, one;
    WORD colors[3];
	BYTE *rd, *wr, *rdrow, *wrrow;

	rd = mem1;   wr = mem24;	ycnt = ye;
    colors[0] = (WORD)(blue & 0xff);      // this order!
    colors[1] = (WORD)(green & 0xff);
   	colors[2] = (WORD)(red & 0xff);
	while (ycnt--)
	{  // loop by rows
   		rdrow = rd;	 wrrow = wr;
        xcnt = 0;  one = 1;
		while (xcnt < xe)
		{  // make 24-bit row

			pix = ((DWORD)*rdrow) & one;
            if ((one <<= 1) & 0x100)
            {
            	rdrow++;  one = 1;
            }
         	for (i = 0;  i < 3;  i++)
         	{
         		if (add) calc = *wrrow;	else calc = 0;
                if (pix) calc += colors[i];
				if (calc > 255) calc = 255;	// saturation
         		*wrrow++ = (BYTE)calc;
         	}
      	}
      	rd += pitch1;	wr += pitch24;
	}
}


////////////////////////////////////////////////////////////////////////////////
//
//	BytesToRGB - 
//
//	Transform 8 bits pixels into 24 according to color coefficients
//	(in range from 0 to 255); saturation at 255.
//
//	Negative coefficient means 'use inverted pixel value'.
//
//	Inputs:
//	add		0 - Fill
//			1 - Add
//
//	Complete re-write of original BytesToRGB function by
//	David Burgess 8th April 2002
//
void _stdcall BytesToRGB (
			BYTE *mem8, DWORD xe, DWORD ye, DWORD pitch8,
			BYTE *mem24, DWORD pitch24,
			long red, long green, long blue, long add)
{
	register BYTE *rdrow, *wrrow;
	register int i, xcount, ycount;
	BYTE *rd, *wr;
	BYTE blut[256], glut[256], rlut[256];
	BYTE pix;

	// Assign local variables to inputs
	rd = mem8;
	wr = mem24;
	ycount = ye;

	// Build up Look-up-tables to do the transform

	// Blue
	if (blue < 0) {
		blue = abs(blue);
		for (i = 0; i < 256; i++)
			blut[i] = ((i ^ 0xff) * blue) / 255;

	} else {
		for (i = 0; i < 256; i++)
			blut[i] =  (i * blue) / 255;
	}

	// Green
	if (green < 0) {
		green = abs(green);
		for (i = 0; i < 256; i++)
			glut[i] = ((i ^ 0xff) * green) / 255;

	} else {
		for (i = 0; i < 256; i++)
			glut[i] =  (i * green) / 255;
	}

	// Red
	if (red < 0) {
		red = abs(red);
		for (i = 0; i < 256; i++)
			rlut[i] = ((i ^ 0xff) * red) / 255;

	} else {
		for (i = 0; i < 256; i++)
			rlut[i] =  (i * red) / 255;
	}

	// Fill (add = 0) or Add (add = 1) ?
	if (add) {
		// Adding to existing colour
		while (ycount--) {  // loop by rows
   			rdrow = rd;
			wrrow = wr;
			xcount = xe;

			while (xcount--) {  // make 24-bit row
				pix = *rdrow++;

                                if (*wrrow > (255 - blut[pix]))
                                        *wrrow = 255;
                                else
				        *wrrow += blut[pix];
				wrrow++;

                                if (*wrrow > (255 - glut[pix]))
                                        *wrrow = 255;
                                else
				        *wrrow += glut[pix];
				wrrow++;

                                if (*wrrow > (255 - rlut[pix]))
                                        *wrrow = 255;
                                else
				        *wrrow += rlut[pix];
				wrrow++;
			}
     		rd += pitch8;
			wr += pitch24;
		}
	} else {
		// Fill with this colour
		while (ycount--) {  // loop by rows
   			rdrow = rd;
			wrrow = wr;
			xcount = xe;

			while (xcount--) {  // make 24-bit row
				pix = *rdrow++;

				*wrrow++ = blut[pix];
				*wrrow++ = glut[pix];
				*wrrow++ = rlut[pix];
			}
     		        rd += pitch8;
			wr += pitch24;
		}
	}
}




/***************************************************** GetLineCode */
/***
	Function:	GetLineCode

	Description:
		This function draws a line on given BitMask;
		additional data is located in the array of longs

	Statics/Globals:
		None

	Input Arguments:
		pcodes - pointer to the buffer
        bufsize - size of the buffer
		pdata - pointer to the array of longs; it's contents for each
				operation is described below:

		linemode -
            mlPOLY    0x100     // polyline from {Data[0], Data[1]}
							    // to {Data[2], Data[3]}, then
							    // to {Data[4], Data[5]}, and so on
            mlSPLINE0 0x200     // closed spline
            mlSPLINE1 0x300     // unclosed spline with free ends
            mlSPLINE2 0x400	    // unclosed spline with straight first interval
            mlSPLINE3 0x500	    // unclosed spline with straight last interval

            mlCIRCLE  0x800     // circle with center in {Data[0], Data[1]}
                                // and radius Data[2]
            mlARC     0x900     // ark with center in {Data[0], Data[1]}
                                // from {Data[2], Data[3]} to {Data[4], Data[5]}
                                // in clockhand direction

		count - number of sections
        pxstart, pystart - addresses to get start point coordinates
                            (NULL is OK)

	Returns:
		number of bytes drawn

***/
long _stdcall  GetLineCode (void *pcodes, long bufsize,
                  long linemode, long count, long *pdata,
                  long *pxstart, long *pystart)
{
    long xstart, ystart, n, period, begoffset,
         *data, circle[8];
    BYTE *codes;

    data = (long *)pdata;   codes = (BYTE *)pcodes;
    xstart = data[0];   ystart = data[1];
    period = begoffset = 0;
    switch (linemode)
    {
   	    case mlPOLY:
			n = MaskPolyLine (codes, bufsize, count, data);
      	    break;
   	    case mlSPLINE0:
   	    case mlSPLINE1:
   	    case mlSPLINE2:
   	    case mlSPLINE3:
            n = MaskSpline (codes, bufsize, count, data, linemode);
      	    break;
   	    case mlCIRCLE:
            circle[0] = circle[2] = circle[4] = data[0];	// Xc
            circle[1] = data[1]; 	// Yc
            circle[3] = circle[5] = data[1] + data[2];  // Yc + Radius
            data = circle;
      	    // no break!
   	    case mlARC:
    		xstart = data[2];   ystart = data[3];
			n = MaskArc (codes, bufsize, &period, &begoffset, data);
            if ((n > 0) && (begoffset))
            {
                if (!period) period = n;
                ReorderBufs (codes, begoffset, period - begoffset);
            }
      	    break;
		default: n = -1;
    }
    if (pxstart) *pxstart = xstart;
    if (pystart) *pystart = ystart;
	return n;
}


//=================================================== GrayCorrelation
// Calculates statistics for two 8 bit/pix Rectangles;
// First one is to rotate around point {xc1,yc1}; after
// rotation this point moves to {xc2,yc2};
// second rectangle as is.
// The procedure is controlled by Mask <m3> corresponding
// to the first rectangle; NULL is ok for <m3> (use all pixels)
// Results go to <data> (at least 6 doubles):
// data[0]=Number of used pixels;
// data[1]=Average1;   data[2]=Average2;
// data[3]=Sigma1;     data[4]=Sigma2;
// data[5]=Correlation Coefficient
// returned value: number of processed pixels (equal to data[0]);
DWORD _stdcall GrayCorrelation (
			void *m1, long xe1, long ye1, long pitch1,
			void *m2, long xe2, long ye2, long pitch2,
			void *m3, long pitch3,
         long xc1, long yc1, long xc2, long yc2,
         double angle, double *data)
{
	double tanhalf, sine, dwrk;
	double a1, a2, s1, s2, corr, nm;
   long *dt, *ds;
   long xs1, ys1, i, k, dx, dy, ix, iy,
   		xmin, xmax, ymin, ymax, n,
         s, sx, sy, sxx, syy, sxy;
   BYTE *r, *w, *msk;
   DWORD b1, b2;
   BOOL rotpi = false;

   if (cos(angle) < 0)
   {
   	angle += (atan((double)1) * 4); // + pi
      rotpi = true;
   }
   tanhalf = tan(angle / 2);  sine = -sin(angle);
   dt = (long *)Work4K;   ds = dt + 2 * (ye1 + 1);
   xs1 = xe1 / 2;   ys1 = ye1 / 2;
   dwrk = tanhalf * (ys1 + yc1) + 0.5;
   for (i = 0;  i < 2 * ye1;  i++)
   {  dt[i] = floor(dwrk);  dwrk -= tanhalf; }
   dwrk = sine * (xs1 + xc1) + 0.5;
   for (i = 0;  i < 2 * xe1;  i++)
   {  ds[i] = floor(dwrk);  dwrk -= sine; }
   dx = xc2 - xc1;  dy = yc2 - yc1;
   r = (BYTE *)m1;
   if (m3) msk = (BYTE *)m3; else msk = r;
   xmin = -xs1;  xmax = 2 * xe1 - xs1 - 1;
   ymin = -ys1;  ymax = 2 * ye1 - ys1 - 1;
   a1 = a2 = s1 = s2 = corr = nm = 0.0;   n = 0;
   for (k = 0;  k < ye1;  k++)
   {
	   s = sx = sy = sxx = syy = sxy = 0;
    	for (i = 0;  i < xe1;  i++)
      {
         if (m3) if (!((msk[i >> 3] >> (i & 7)) & 1)) continue;
        	ix = i + dt[k + ys1];	// horizontal shear
         if ((ix < xmin) || (ix > xmax)) continue;
       	iy = k + ds[ix + xs1]; 	// vertical shear
         if ((iy < ymin) || (iy > ymax)) continue;
         ix += dt[iy + ys1];  	// 2-nd horizontal shear
         ix += dx;   iy += dy;
	      if (rotpi)
         {  // rotation 2*pi
         	ix = 2 * xc2 - ix;   iy = 2 * yc2 - iy;
         }
         if ((ix >= 0) && (ix < xe2) && (iy >= 0) && (iy < ye2))
         {

            w = (BYTE *)m2 + (iy * pitch2);
            b1 = (DWORD)r[i];  b2 = (DWORD)w[ix];
            s++;  sx += b1;  sy += b2;
            sxx += b1 * b1;  syy += b2 * b2;  sxy += b1 * b2;
         }
      }
      if (s)
      {
       	n += s;  a1 += (double)sx;  a2 += (double)sy;
       	s1 += (double)sxx;  s2 += (double)syy;
         corr += (double)sxy;
      }
      r += pitch1;   msk += pitch3;
   }
   data[0] = nm = (double)n;
   if (n)
   {
    	data[1] = a1 = a1 / nm;   data[2] = a2 = a2 / nm;  // averages
    	s1 = s1 / nm - a1 * a1;   if (s1 < 0.0) s1 = 0.0;  // standart
    	s2 = s2 / nm - a2 * a2;   if (s2 < 0.0) s2 = 0.0;  // deviations
      data[3] = s1 = sqrt(s1);  data[4] = s2 = sqrt(s2); // sigmas
      if ((s1 > 0.0) && (s2 > 0.0))
      {
       	data[5] = (corr / nm - a1 * a2) / (s1 * s2);
      } else data[5] = 0.0;
   }
	return (DWORD)n;
}


//====================================================== GrayRotation
// Move Grayscale (8 bit/pix) Rectangle with Rotation around
// point {xc1,yc1}; after rotation it goes to {xc2,yc2}
// Mask <m3> controls moving; it corresponds to <m1> (source rectangle)
// NULL is ok for <m3> (no mask)
void _stdcall GrayRotation (
			void *m1, long xe1, long ye1, long pitch1,
			void *m2, long xe2, long ye2, long pitch2,
			void *m3, long pitch3,
         long xc1, long yc1, long xc2, long yc2, double angle)
{
	double tanhalf, sine, dwrk;
   long *dt, *ds;
   long xs1, ys1, i, k, dx, dy, ix, iy, xmin, xmax, ymin, ymax;
   BYTE *r, *w, *msk;
   BOOL rotpi = false;

   if (cos(angle) < 0)
   {
   	angle += (atan((double)1) * 4); // + pi
      rotpi = true;
   }
   tanhalf = tan(angle / 2);  sine = -sin(angle);
   dt = (long *)Work4K;   ds = dt + 2 * (ye1 + 1);
   xs1 = xe1 / 2;   ys1 = ye1 / 2;
   dwrk = tanhalf * (ys1 + yc1) + 0.5;
   for (i = 0;  i < 2 * ye1;  i++)
   {  dt[i] = floor(dwrk);  dwrk -= tanhalf; }
   dwrk = sine * (xs1 + xc1) + 0.5;
   for (i = 0;  i < 2 * xe1;  i++)
   {  ds[i] = floor(dwrk);  dwrk -= sine; }
   dx = xc2 - xc1;  dy = yc2 - yc1;
   r = (BYTE *)m1;
   if (m3) msk = (BYTE *)m3; else msk = r;
   xmin = -xs1;  xmax = 2 * xe1 - xs1 - 1;
   ymin = -ys1;  ymax = 2 * ye1 - ys1 - 1;
   for (k = 0;  k < ye1;  k++)
   {
    	for (i = 0;  i < xe1;  i++)
      {
         if (m3) if (!((msk[i >> 3] >> (i & 7)) & 1)) continue;
        	ix = i + dt[k + ys1];	// horizontal shear
         if ((ix < xmin) || (ix > xmax)) continue;
       	iy = k + ds[ix + xs1]; 	// vertical shear
         if ((iy < ymin) || (iy > ymax)) continue;
         ix += dt[iy + ys1];  	// 2-nd horizontal shear
         ix += dx;   iy += dy;
	      if (rotpi)
         {  // rotation 2*pi
         	ix = 2 * xc2 - ix;   iy = 2 * yc2 - iy;
         }
         if ((ix >= 0) && (ix < xe2) && (iy >= 0) && (iy < ye2))
         {
            w = (BYTE *)m2 + (iy * pitch2);
            w[ix] = r[i];
         }
      }
      r += pitch1;   msk += pitch3;
   }
}


//========================================================= Mask3bord
// xe in bits
// all 3 masks to be changed; let m0 = (m1 & m2) & ~m3; then:
// m3 |= m0;  m1 &= (!m3);  m2 &= (!m3);
// returns 1 if m0 is not empty, 0 otherwise
long _stdcall Mask3bord (
			void *m1, long xe, long ye, long pitch1,
         	void *m2, long pitch2,
         	void *m3, long pitch3)
{
   	DWORD ycnt, shift, mask, xdw, xcnt, ms,
   		l1, l2, l3, l0, lw,
   		ones = 0xffffffff, ret = 0;
   	BYTE  *q1, *q2, *q3;
   	DWORD *p1, *p2, *p3;

   	if ((shift = xe & 0x1f) != 0) mask = ones >> (32 - shift);
	q1 = (BYTE *)m1;	 q2 = (BYTE *)m2;  q3 = (BYTE *)m3;
   	xdw = (xe + 31) >> 5;
	for (ycnt = 0;  ycnt < (DWORD)ye;  ycnt++)
   	{
		p1 = (DWORD *)q1;	 p2 = (DWORD *)q2;  p3 = (DWORD *)q3;
        ms = ones;
    	for (xcnt = 0;  xcnt < xdw;  xcnt++)
      	{
		   	l1 = p1[xcnt];  l2 = p2[xcnt];	l3 = p3[xcnt];
        	if (xcnt == xdw - 1) ms = mask;
            l0 = (l3 ^ ones) & l1 & l2 & ms; 	// new bits in (m1&m2)
            lw = (((l1 | l2) & l3) | l0) & ms; 	// bits to change
            if (lw)
            {	// there is something to change
            	l3 |= l0;  ret |= l0;
                lw ^= ones;  l1 &= lw;  l2 &= lw;
        	 	p1[xcnt] = l1;  p2[xcnt] = l2;  p3[xcnt] = l3;
            }
      	}
      	q1 += pitch1;  q2 += pitch2;  q3 += pitch3;
   	}
    if (ret) ret = 1;
   	return ret;
}


/***************************************************** MaskBitwise */
/***
	Function:	MaskBitwise

	Description:
		This function implements 27 listed below bytewise
		operations of type
				mem1 <oper> mem2 => mem2

	Statics/Globals:
		None

	Input Arguments:
		mem1 - pointer to 1-st argument memory buffer ("A")
		xe - number of pixels (bits) in each row
		ye - number of rows
		pitch1 - address difference between neighboring rows (in bytes)
				in 1-st argument memory rectangle ("A")

		mem2 - pointer to 2-nd argument and result memory buffer ("B")
		pitch2 - address difference between neighboring rows (in bytes)
				in target memory rectangle ("B")
		oper - 	A <oper> B => B
			operations are bitwise, values of symbolic
			constants correspond to truth table:

			momZERO = 0   set all bits to zero
			momAND 	= 1   A & B
			momMORE = 2   A & ~B  (1 if A > B)
			momCOPY1 = 3   A  (copy A into B)
			momLESS = 4   ~A & B  (1 if A < B)
			momCOPY2 = 5   B  (copy B {or const} into B)
			momEXOR = 6   A ^ B  (mod 2 {exclusive OR})
			momOR 	= 7   A | B
			momNOR 	= 8   ~(A | B)  (not OR)
			momEQUAL = 9   ~(A ^ B)  (1 if A = B)
			momNOT2 =10   copy ~B into B
			momEQMORE=11   A | ~B  (1 if A >= B)
			momNOT1 =12   copy ~A into B
			momEQLESS=13   ~A | B  (1 if A <= B)
			momNAND =14   ~(A & B)  (not AND)
			momFULL =15   set all bits to 1 (bytes to 255)

	Returns:
		0 - if all bits in mem2 are 0s;
		1 - if all bits in mem2 are 1s;
        2 - otherwise
**/
long _stdcall MaskBitwise (
			void *mem1,
			DWORD xe, DWORD ye, DWORD pitch1,
            void *mem2, DWORD pitch2,
            short oper)
{
	WORD op;
	BYTE *bm1, *bm2;
	DWORD l1, l2, neg, *m1, *m2, ones, sol,
	      xcnt, ycnt, lmask, rmask,
          retand, retor;
    long ret = 2;

	oper &= 0x0f;  	sol = sizeof(long) << 3;
	retand = ones = 0xffffffffL;  retor = 0;
    rmask = ones << (xe & 0x1f);
    lmask = rmask ^ ones;
	if (oper < 8) {  op = oper;   neg = 0; }
	else {  op = (WORD)(15 - oper);  neg = ones; }
	bm1 = (BYTE *)mem1;   bm2 = (BYTE *)mem2;
    ycnt = 0;
	while (ycnt < ye)
	{
		m1 = (DWORD *)bm1;  m2 = (DWORD *)bm2;
        xcnt = 0;
		while (xcnt < xe)
		{
			l1 = *m1++;   l2 = *m2;
			switch (op)
			{
				case momAND:    l1 &= l2;   break;     // 1st and  2nd
				case momOR:     l1 |= l2;   break;     // 1st or   2nd
				case momEXOR:   l1 ^= l2;   break;     // 1st exor 2nd
				case momMORE:   l1 = (l1 ^ l2) & l1;   break;  // 1st and ~2nd
				case momLESS:   l1 = (l1 ^ l2) & l2;   break;  // 2nd and ~1st
				case momCOPY2:  l1 = l2;    break;     // 2nd
				case momZERO:   l1 = 0;     break;     // zero
				default:  ;   // case momCOPY1 - 1st, nothing to do
            }
			l1 ^= neg;   // possible negation {for operations 8 - 15}
			if ((xcnt += sol) <= xe)
            {
            	*m2++ = l1;
                retand &= l1;  retor |= l1;
			} else
            {
            	*m2 = (l1 &= lmask) | (l2 & rmask);
                retand &= (l1 | rmask);  retor |= l1;
            }
		}
        bm1 += pitch1;  bm2 += pitch2;
        ycnt++;
	}
    if (retor) { if (retand == ones) ret = 1; }
    else ret = 0;
    return ret;
}



/********************************************************* MaskConvex */
/***
	Function:	MaskConvex

	Description:
		Expand mask to convex 32-oid

	Statics/Globals:
		None

	Input Arguments:
		mem - pointer to the source Rectangle

		xe - number of pixels in each row

		ye - number of rows

		pitch - address difference between neighboring rows (in bytes)
				in the source Rectangle
        pixperc - percentile of pixel to "outdraw"

	Returns: number of ONEs in the 32-oid

***/
    // directions coefficients from 90 degrees with -11.25 increment:
static long MaskConvexCx[16] = {
    	0,    19509,  38268,  55557,  70711,  83147,  92388,  98079,
      100000, 98079,  92388,  83147,  70711,  55557,  38268,  19509};
static long MaskConvexCy[16] = {
      100000, 98079,  92388,  83147,  70711,  55557,  38268,  19509,
        0,   -19509, -38268, -55557, -70711, -83147, -92388, -98079};

long _stdcall  MaskConvex (
				void *mem, DWORD xe, DWORD ye, DWORD pitch,
                long pixperc)
{
	long big, small, val, ix, iy, icxy, ic,
    	 xmin, xmax, ymin, ymax, s, base, thick;
	DWORD one, p32, begone, *lpix;
	BYTE *cpix;
	long Dat[32];

    base = MaskConvexCy[0];
	big = (xe + ye + 1) * 2 * base;
    small = -big;  s = 0;
	// Set  min/max for scalar products
	cpix = (BYTE *)mem;
    for (ix = 0;  ix < 32;  ix += 2)
    {
		Dat[ix] = big;   Dat[ix + 1] = small;
    }
    // Calculate minima and maxima for all directions:
	for (iy = 0;  iy < (long)ye;  iy++)
	{
		lpix = (DWORD *)cpix;  cpix += pitch;
        one = ix = 0;
        while (ix < (long)xe)
        {
			if (!one)
            {
            	p32 = *lpix++;
                if (!p32) { ix += 32;  continue; }
                one = 1;
            }
            if (p32 & one)
			{
				s++;
        		for (icxy = 0;  icxy < 16;  icxy++)
        		{
					val = ix * MaskConvexCx[icxy] + iy * MaskConvexCy[icxy];
                    ic = icxy * 2;
					Dat[ic] = min(Dat[ic], val);
                    ic++;
					Dat[ic] = max(Dat[ic], val);
        		}
			}
			one <<= 1;  ix++;
		}
    }
	if (s == 0) goto Fin;
    // set appropriate zero pixels to ones:
    thick = base * pixperc / 100;
    ymin = (max(Dat[0], thick) - thick) / base;
    ymax = min((Dat[1] + thick) / base, (long)(ye - 1));
	cpix = (BYTE *)mem;  cpix += (pitch * ymin);
    xmin = (max(Dat[16], thick) - thick) / base;
    xmax = min((Dat[17] + thick) / base, (long)(xe - 1));
    cpix += ((xmin >> 5) << 2);
    begone = 1 << (xmin & 0x1f);
	for (iy = ymin;  iy <= ymax;  iy++)
	{
    	lpix = (DWORD *)cpix;	cpix += pitch;
        one = begone;
        for (ix = xmin;  ix <= xmax;  ix++)
        {
			p32 = *lpix;
            if (p32 & one) goto FinX32;  // already 1
            icxy = 0;
       		while (icxy < 16)
       		{	// check
            	val = ix * MaskConvexCx[icxy] + iy * MaskConvexCy[icxy];
                ic = icxy * 2;
				if (Dat[ic++] > val + thick) break;
                if (Dat[ic] < val - thick) break;
                icxy++;
			}
            if (icxy == 16)
            {   // all conditions OK
            	*lpix |= one;  s++;
            }
FinX32:
			if (!(one <<= 1)) { one = 1;  lpix++; }
		}
	}
Fin:
	return s;
}



/**************************************************** MaskCalipers */
/***
	Function:	MaskCalipers

	Description:
        Calculates extends in 16 directions, finds extreme ones,
        calculetes 'centroid' coordinates

	Statics/Globals:
		None

	Input Arguments:
		mem - pointer to the source Rectangle

		xe - number of pixels in each row

		ye - number of rows

		pitch - address difference between neighboring rows (in bytes)
				in the source Rectangle
        calipers - array[16] to fill with min/max 'caliper' sizes
        			of mask ONEs in 16 directions:
                Angle[I] = Pi/2 - I*(Pi/16) rad	= 90 - I * 11.5 degrees
                	where I = 0...15
                	All projections are rounded to longs
        imin, imax - pointers to get directions of min and max calipers
        xc, yc - pointers to get X,Y coordinates of the centroid
        		(average of extreme points); NULL is ok

	Returns: number of ONEs in the mask

***/
long _stdcall  MaskCalipers (
				void *mem, DWORD xe, DWORD ye, DWORD pitch,
                long *calipers, long *imin, long *imax,
                long *xc, long *yc)
{
	long val, ix, iy, icxy, ic, base, b2, cmin, cmax,
    	 ret = 0, minsmaxs[32], xs[32], ys[32];
	DWORD one, p32, *lpix;
	BYTE *cpix;

    base = MaskConvexCy[0];  b2 = base * 3 / 2;
	cmin = (xe + ye + 1) * 2 * base;  cmax = -cmin;
	// Set min/max for scalar products
	cpix = (BYTE *)mem;
    for (ix = 0;  ix < 32;  ix += 2)
    {
		minsmaxs[ix] = cmin;		// future mins
        minsmaxs[ix + 1] = cmax; // future maxs
    }
    // Calculate minima and maxima for all directions:
	for (iy = 0;  iy < (long)ye;  iy++)
	{
		lpix = (DWORD *)cpix;  cpix += pitch;
        one = ix = 0;
        while (ix < (long)xe)
        {
			if (!one)
            {
            	p32 = *lpix++;
                if (!p32) { ix += 32;  continue; }
                one = 1;
            }
            if (p32 & one)
			{
        		for (icxy = 0;  icxy < 16;  icxy++)
        		{
					val = ix * MaskConvexCx[icxy] + iy * MaskConvexCy[icxy];
                    ic = icxy * 2;
					if (minsmaxs[ic] > val)
                    {
                    	minsmaxs[ic] = val;	// min
                        xs[ic] = ix;   ys[ic] = iy;
					}
                    ic++;
					if (minsmaxs[ic] < val)
                    {
                    	minsmaxs[ic] = val;	// max
                        xs[ic] = ix;   ys[ic] = iy;
					}
        		}
                ret++;
			}
			one <<= 1;  ix++;
		}
    }
    if (ret)
    {
        ix = iy = 0;
        for (icxy = 0;  icxy < 16;  icxy++)
        {
            ic = icxy * 2;
            val = calipers[icxy] =
            	(minsmaxs[ic + 1] - minsmaxs[ic] + b2) / base;
            if (cmin > val) { cmin = val;  *imin = icxy; }
            if (cmax < val) { cmax = val;  *imax = icxy; }
         	ix += (xs[ic] + xs[ic + 1]);
            iy += (ys[ic] + ys[ic + 1]);
        }
        if (xc) *xc = (ix + 16) / 32;
        if (yc) *yc = (iy + 16) / 32;
    }
    return ret;
}


/************************************************* MaskDistToPoint */
/***
	Function:	MaskDistToPoint

	Description:
		This function finds 1-pixel in a given mask
      nearest to given point

	Statics/Globals:
		None

	Input Arguments:
		pmem - pointer to the Bit Mask

      xl, yt - subrectangle left top corner (in bits)

		xe - number of pixels (bits) in each row
		ye - number of rows

		pitch - address difference between neighboring rows (in bytes)
				in the limiting Bit Mask

	Returns:
		square of the distance or -1 if empty;
      updates xp, yp

***/
long _stdcall MaskDistToPoint (
					BYTE *pmem,
               long xl, long yt,
               long xe, long ye, long pitch,
               long *xp, long *yp)
{
	WORD bval;
	long xcnt, ycnt, xstrt, xmin, xmax, ymin, ymax,
   		xfin, wrk, x, y, changed,
   		xx, yy, xpp, ypp, d, dist, dmax = 9999999;
    bool maydown = true, mayup = true;
	BYTE *row;

   dist = dmax;
   xpp = *xp;  ycnt = ypp = *yp;
   xmin = xl;  xmax = xl + xe;
   if (ycnt < yt) ycnt = yt;
   if (ycnt >= yt + ye) ycnt = yt + ye - 1;
   ymin = ymax = ycnt;

DoOneRow:
   row = pmem + (ycnt * pitch);
   xstrt = xmin >> 3;  xfin = (xmax + 7) >> 3;
   yy = ypp - ycnt;  yy = yy * yy;  changed = 0;
   for (xcnt = xstrt;  xcnt < xfin;  xcnt++)
   {
    	bval = row[xcnt];
      xx = xcnt << 3;
      while (bval)
      {
         if ((bval & 1) && (xx >= xmin) && (xx < xmax))
         {
          	d = xx - xpp;  d = d * d + yy;
            if (d < dist)
            {
             	x = xx;  y = ycnt;  dist = d;  changed++;
            }
         }
         bval >>= 1;  xx++;
      }
   }
   if (changed)
   {
      wrk = xpp - x + xpp;
      if (x <= xpp)
      {
       		xmin = x;  if (wrk < xmax) xmax = wrk + 1;
      } else
      {
        	xmax = x + 1;  if (wrk > xmin) xmin = wrk;
	  }
   }
TryDown:
   if (((ycnt >= ymax) || (!mayup)) && (maydown))
   {
      ycnt = ymin - 1;
      if (ycnt >= yt)
      {
      	  yy = ypp - ycnt;  yy = yy * yy;
          if (yy < dist) { ymin = ycnt;  goto DoOneRow; }
      }
      maydown = false;
   }
   if (((ycnt <= ymin) || (!maydown)) && (mayup))
   {
      ycnt = ymax + 1;
      if (ycnt < yt + ye)
      {
      	 yy = ypp - ycnt;  yy = yy * yy;
         if (yy < dist) { ymax = ycnt;  goto DoOneRow; }
      } else
      {
      	 if ((ymin > yt) && (maydown)) goto TryDown;
      }
      mayup = false;
   } 
   if (maydown || mayup) goto TryDown;
   if (dist == dmax) dist = -1;
   else { *xp = x;  *yp = y; }
   return dist;
}


/**************************************************** MaskDrawCode */
/***
	Function:	MaskDrawCode

	Description:
		This function draws a line given by its code
        begining from a given point on a given BitMask;

	Arguments:
		pmask - pointer to the mask Rectangle
		xe - number of pixels in each row
		ye - number of rows
		pitch - address difference between neighboring rows (in bytes)
				in the source Rectangle
        x0, y0 - pointers to coordinates of the begining point;
                to be updated to the last (nondrawn) point
		pcodes - pointer to the array of codes to retrieve;
                is <period> periodical
		count - number of codes  to retrieve
        pen -	0, 1 - put 0/1 pixel; >1 - inverse the pixel.

	Returns:
		number of retrieved pixels

***/
long _stdcall MaskDrawCode (void *pmask,
            long xe, long ye, long pitch,
            long *x0, long *y0,
            void *pcodes, long count,
            long pen)
{
    long i, ix, iy, ret;
	BYTE p, oldp, code, *cpix, *codes, *mask;

    ret = 0;  ix = *x0;  iy = *y0;
    codes = (BYTE *)pcodes;  mask = (BYTE *)pmask;
    for (i = 0;  i <= count;  i++)
    {
        if ((ix >= 0) && (ix < xe) && (iy >= 0) && (iy < ye))
        {
            cpix = mask + ((iy * pitch) + (ix >> 3));
            oldp = *cpix;	p = (BYTE)(1 << (ix & 7));
            switch (pen)
            {
	            case 0:  oldp ^= (oldp & p);  break;
	            case 1:  oldp |= p;  break;
                default: oldp ^= p;
            }
    		*cpix = oldp;  ret++;
        }
        if (i < count)
        {	// do not if the last point
        	code = codes[i];  CodeUpdateXY (&ix, &iy, code);
        }
	}
    *x0 = ix;  *y0 = iy;
	return ret;
}


/****************************************************** MemMaskMem */
/***
	Function:	MemMaskMem

	Description:
		This function implements 27 listed bytewise operations of type
				mem1 <oper if mask> mem2 => mem2

	Statics/Globals:
		None

	Input Arguments:
		pmem1 - pointer to 1-st argument memory rectangle ("A")

		pmem2 - pointer to 2-nd argument and result memory rectangle ("B")

		pmask - pointer to the argument bit mask

		xe - number of pixels in each row

		ye - number of rows

		pitch1 - address difference between neighboring rows (in bytes)
				in the 1-st argument memory rectangle ("A")
		pitch2 - address difference between neighboring rows (in bytes)
				in the 2-nd (argument and result) memory rectangle ("B")
		pitchmask - address difference between neighboring rows (in bytes)
				in the bit mask
		oper - 	A <oper if mask> B => B
				operation symbolic constants are the same as for MemOpMem(),
				but 2 modifiers may be used;

			momZERO 	= 0   set all bits to zero
			momAND 	= 1   A & B
			momMORE 	= 2   A & ~B  (1 if A > B)
			momCOPY1 = 3   A  (copy A into B)
			momLESS 	= 4   ~A & B  (1 if A < B)
			momCOPY2 = 5   B  (copy B {or const} into B)
			momEXOR 	= 6   A ^ B  (mod 2 {exclusive OR})
			momOR 	= 7   A | B
			momNOR 	= 8   ~(A | B)  (not OR)
			momEQUAL = 9   ~(A ^ B)  (1 if A = B)
			momNOT2 	=10   copy ~B into B
			momEQMORE=11   A | ~B  (1 if A >= B)
			momNOT1 	=12   copy ~A into B
			momEQLESS=13   ~A | B  (1 if A <= B)
			momNAND 	=14   ~(A & B)  (not AND)
			momFULL 	=15   set all bits to 1 (bytes to 255)

			momMIN 	=16   A MIN B
			momMAX 	=17   A MAX B
			momPLUS 	=18   (A + B) mod 256
			momSPLUS 	=19   (A + B) min 255 (saturation)
			momMINUS 	=20   (A - B) mod 256
			momIMINUS 	=21   (B - A) mod 256 (inverse order)
			momSMINUS 	=22   (A - B) max 0   (saturation)
			momSIMINUS 	=23   (B - A) max 0   (saturation and inverse order)
			momLOWto0 	=24   if A >= B then A else 0
			momHIGHtoFF =25   if A <= B then A else 255
			mom0orFF 	=26   if A < B then 0 else 255
			momAbsDiff  =27   if A < B then (B - A) else (A - B)
			momAbsDiff8 =28   Abs(A - B) MIN (256 - Abs(A - B))

					one of the following modifiers can be ORed with the
					operation code:
			mmcmNEG    =0x80   (oper | mmcmNEG) means "use negative mask"
			mmcmIGNORE =0x40   (oper | mmcmIGNORE) means "ignore mask";
								last modifier is useful because MemMaskMem()
								and MemMaskConstMem() work on rectangles, while
								MemOpMem() and MemConstMem() work on buffers only

	Output Arguments:
		None

	Returns:
		0 - if all involved bytes are 0;
		1 - if all involved bytes are 0xff;
        2 - otherwise

***/
DWORD _stdcall MemMaskMem (
				BYTE *mem1, BYTE *mem2, BYTE *mask,
				DWORD xe, DWORD ye,
            	DWORD pitch1, DWORD pitch2, DWORD pitchmask,
            	short oper)
{
	WORD ignore, b1, b2, op, neg;
	BYTE *m1, *m2, *bm1, *bm2, *bmm;
	DWORD p1, p2, pm, *mmdw;
	DWORD xcnt, ycnt, bmask, negoper, ones,
    	retand, retor, ret = 2;

	bm1 = mem1;  p1 = pitch1;
	bm2 = mem2;  p2 = pitch2;
	bmm = mask;  pm = pitchmask;
	ones = 0xffffffff;  negoper = ignore = 0;
	if (oper & mmcmNEG) negoper = ones;
	if (oper & mmcmIGNORE) ignore = 1;
	oper &= (mmcmIGNORE - 1);
    ycnt = 0;  retand = 0xff;  retor = 0;
	if (oper > 15) goto Sixteen;
// Bool0to15:
	if (oper < 8) {  op = oper;   neg = 0; }
	else {  op = (WORD)(15 - oper);  neg = 0xff; }

	while (ycnt++ < ye)
	{
		m1 = bm1;  m2 = bm2;  mmdw = (DWORD *)bmm;
        xcnt = 0;  bmask = 0;
		while (xcnt < xe)
		{
            if (!bmask)
            {
                if (ignore) bmask = ones;
                else bmask = (*mmdw++) ^ negoper;
			}
            if (bmask & 1)
			{
				b1 = m1[xcnt];   b2 = m2[xcnt];
				switch (op)
				{
					case momAND:   b1 &= b2;   break;     // A and  B
					case momOR:    b1 |= b2;   break;     // A or   B
					case momEXOR:  b1 ^= b2;   break;     // A exor B
					case momMORE:  b1 = (b1 ^ b2) & b1;  break;  // A and ~B
					case momLESS:  b1 = (b1 ^ b2) & b2;  break;  // ~A and B
					case momCOPY2: b1 = b2;    break;     // B
					case momZERO:  b1 = 0;     break;     // zero
					default:  ;   // case momCOPY1 - A, nothing to do
				}
				m2[xcnt] = (b1 ^= neg);	  // possible negation
                retand &= b1;  retor |= b1;
			}
            if (ignore) xcnt++;
            else
            {
				if (bmask >>= 1) xcnt++;
            	else xcnt = (xcnt & 0xffffffe0) + 32;
            }
		}
		bm1 += p1;  bm2 += p2;
		if (!ignore) bmm += pm;
	}
    goto Fin;
Sixteen:
	while (ycnt++ < ye)
	{
		m1 = bm1;  m2 = bm2;  mmdw = (DWORD *)bmm;
        xcnt = 0;  bmask = 0;
		while (xcnt < xe)
		{
            if (!bmask)
            {
                if (ignore) bmask = ones;
                else bmask = (*mmdw++) ^ negoper;
			}
            if (bmask & 1)
			{
				b1 = m1[xcnt];   b2 = m2[xcnt];
				switch (oper)
				{
					case momMIN:      if (b1 > b2) b1 = b2;
									  break;  // A min B
					case momMAX:   	  if (b1 < b2) b1 = b2;
									  break;  // A max B
					case momPLUS:     b1 += b2;
									  break;  // (A + B) mod 256
					case momSPLUS:    if ((b1 += b2) > 255) b1 = 255;
									  break;  // (A + B) min 255 (saturation)
					case momMINUS:    b1 -= b2;
									  break;  // (A - B) mod 256
					case momIMINUS:   b1 = (WORD)(b2 - b1);
									  break;  // (B - A) mod 256
					case momSMINUS:   if (b1 >= b2) b1 -= b2; else b1 = 0;
									  break;  // (A - B) max 0
					case momSIMINUS:  if (b1 <= b2) b1 = (WORD)(b2 - b1);
                    				  else b1 = 0;
									  break;  // (B - A) max 0
					case momLOWto0:   if (b1 < b2) b1 = 0;
									  break;  // if A<B then 0 else A
					case momHIGHtoFF: if (b1 > b2) b1 = 255;
									  break;  // if A>B then 255 else A
					case mom0orFF:    if (b1 < b2) b1 = 0; else b1 = 255;
									  break;  // if A<B then 0 else 255
			     	case momAbsDiff:  if (b1 < b2) b1 = b2 - b1; else b1 -= b2;
						       		  break;  // Abs(b1-b2)
			     	case momAbsDiff8: if (b1 < b2) b1 = b2 - b1; else b1 -= b2;
					                  if (b1 > 128) b1 = 256 - b1;
						       	      break; // Abs(b1-b2) MIN (256 - Abs(b1-b2))

					default:  ;
				}
				m2[xcnt] = b1;
                retand &= b1;  retor |= b1;
			}
            if (ignore) xcnt++;
            else
            {
				if (bmask >>= 1) xcnt++;
            	else xcnt = (xcnt & 0xffffffe0) + 32;
			}
		}
		bm1 += p1;  bm2 += p2;
		if (!ignore) bmm += pm;
	}
Fin:
    if (retor) { if (retand == 0xff) ret = 1; }
    else ret = 0;
	return ret;
}


/************************************************** MaskOperMask */
/***
	Function:	MaskOperMask

	Description:
		This function implements listed below bitwise operations of type
				Oper(bitmask1) => bitmask2.

	Statics/Globals:
		None

	Input Arguments:
		pBitMem1 - pointer to the source Bit Mask

		Xe - number of pixels (bits) in each row

		Ye - number of rows

		Pitch1 - address difference between neighboring rows (in bytes)
				in the source Bit Mask
		pBitMem2 - pointer to the target Bit Mask

		Pitch2 - address difference between neighboring rows (in bytes)
				in the target Bit Mask
		oper - 	Oper(bitmask1) => bitmask2

			mnmMIN 		  =0   min of the pixel and it's 8 neighbors;
			mnmMAX 		  =1   max of the pixel and it's 8 neighbors;
			mnmFRAME 	  =2   frame (1 on sides, 0 inside);
			mnmNFRAME 	  =3   not frame (0 on sides, 1 inside);
			mnmShadowLR1  =4   expand ones left to right
			mnmShadowRL1  =5   expand ones right to left
			mnmShadowTB1  =6   expand ones top to bottom
			mnmShadowBT1  =7   expand ones bottom to top
			mnmShadowLR0  =8   expand zeros left to right
			mnmShadowRL0  =9   expand zeros right to left
			mnmShadowTB0 =10   expand zeros top to bottom
			mnmShadowBT0 =11   expand zeros bottom to top
			mnmMIN4		 =20   min of the pixel and it's 4 neighbors;
			mnmMAX4		 =21   max of the pixel and it's 4 neighbors;
			mnmMINH2	 =22   min of self and 2 bit horizontal neighbors;
			mnmMAXH2	 =23   max of self and 2 bit horizontal neighbors;
			mnmMINV2	 =24   min of self and 2 bit vertical neighbors;
			mnmMAXV2	 =25   max of self and 2 bit vertical neighbors;

	Output Arguments:
		None

	Returns:
		None

***/
void _stdcall MaskOperMask (
				BYTE *BitMem1,
                DWORD Xe, DWORD Ye, DWORD Pitch1,
				BYTE *BitMem2, DWORD Pitch2,
                short oper)
{
	DWORD prev, curr, nxt, ll, pitch12, msize;
	WORD pr, cu, nx, mask, edge, right;
	DWORD xcnt, ycnt, bytecnt;
   	WORD flag4 = 0;
   	BYTE *othermem = NULL;
	BYTE *BitRowR, *BegBitRowR, *BitRowW, *BegBitRowW;
   	short switchoper;

   	if ((oper == mnmMIN4) || (oper == mnmMAX4))
   	{
    	flag4 = 1;
	  	if (BitMem1 == BitMem2)
   		{
//			othermem = (BYTE *)malloc (Pitch1 * Ye);
   			msize = Pitch1 * Ye;
   			if (msize > Work4KSize)
        	{
           		othermem = (BYTE *)malloc(msize);
	      		if (!othermem) return;
        	} else othermem = Work4K;
     		MemMaskMem (BitMem1, othermem, othermem, Pitch1, Ye,
            		Pitch1, Pitch1, Pitch1, momCOPY1 | mmcmIGNORE);
			BitMem1 = othermem;	Pitch2 = Pitch1;
      	}
	}

	if ((oper >= mnmShadowLR1) && (oper <= mnmShadowBT0))
	{
		MaskShadowMask(BitMem1, Xe, Ye, Pitch1, BitMem2, Pitch2, oper);
		return;
	}
	BegBitRowR = BitMem1;  BegBitRowW = BitMem2;
	bytecnt = (Xe + 7) >> 3;
	if ((oper == mnmMIN) || (oper == mnmMIN4)) edge = 0xff;
	else edge = 0;
	mask = (WORD)(0xff >> ((8 - (Xe & 7)) & 7));
	right = (WORD)((mask + 1) << 7);
    switchoper = oper;
	if ((oper == mnmMIN) || (oper == mnmMIN4)
		 || (oper == mnmMINV2) || (oper == mnmMINH2))
		switchoper = mnmMIN;
	if ((oper == mnmMAX) || (oper == mnmMAX4)
		 || (oper == mnmMAXV2) || (oper == mnmMAXH2))
    	switchoper = mnmMAX;
	if ((oper == mnmMINV2) || (oper == mnmMAXV2))
    	goto VertLoop;
// Horizontal loop:
	ycnt = 0;
	while (ycnt++ < Ye)
	{
		BitRowR = BegBitRowR;  BitRowW = BegBitRowW;
		xcnt = 0;  curr = edge;
		while ((xcnt++) < bytecnt)
		{
			prev = curr;  curr = *BitRowR++;
			if (xcnt < bytecnt - 1) nxt = *BitRowR;
			else
			{
				if (xcnt == bytecnt - 1) nxt = *BitRowR & mask;
				else {	curr &= mask;  nxt = edge; }
			}
			ll = (((nxt << 8) | curr) << 8) | prev;
			switch (switchoper)
			{
				case mnmMIN:	ll &= ((ll << 1) & (ll >> 1));  break; // min
				case mnmMAX:	ll |= ((ll << 1) | (ll >> 1));  break; // max
				case mnmFRAME: ll = 0;
					if ((ycnt == 1) || (ycnt == Ye))
					{
						ll = 0xff00;
						if (xcnt == bytecnt) ll += right;
					}
					if (xcnt == 1) ll |= 0x100;
					if (xcnt == bytecnt) ll |= right;
					break; // frame
				case mnmNFRAME: ll = 0xff00;
					if ((ycnt == 1) || (ycnt == Ye)) ll = 0;
					if (xcnt == 1) ll &= 0xfe00;
					if (xcnt == bytecnt) ll &= (ll + right);
					break; // frame
				default:  ;
			}
			*BitRowW++ = (BYTE)(ll >> 8);
		}
		BegBitRowR += Pitch1;  BegBitRowW += Pitch2;
	}
VertLoop:
	if ((Ye < 2) || (oper == mnmFRAME) || (oper == mnmNFRAME)
		|| (oper == mnmMINH2) || (oper == mnmMAXH2))
	{
    	goto Fin;
    }
// Verticval loop:
	xcnt = 0;	BegBitRowW = BitMem2;
	if ((flag4)	|| (oper == mnmMINV2) || (oper == mnmMAXV2))
    {
     	BegBitRowR = BitMem1;  pitch12 = Pitch1;
    } else
    {
    	BegBitRowR = BitMem2;  pitch12 = Pitch2;
    }
	while (xcnt++ < bytecnt)
	{
		BitRowW = BegBitRowW;
		BitRowR = BegBitRowR;
		ycnt = 0;  cu = edge;  nx = *BitRowR;
		while ((ycnt++) < Ye)
		{
			pr = cu;  cu = nx;
			if (ycnt < Ye)
	        {	// not the last row to write
    	        BitRowR += pitch12;
        	 	nx = *BitRowR;
	        } else nx = edge;
			switch (oper)
			{
				case mnmMIN:	pr &= (cu & nx);  break; // min8
				case mnmMAX:	pr |= (cu | nx);  break; // max8
				case mnmMINV2:	pr &= (cu & nx);  break; // min8
				case mnmMAXV2:	pr |= (cu | nx);  break; // max8
				case mnmMIN4:	pr &= ((cu & nx) & (*(WORD *)BitRowW));
                				break; // min4
				case mnmMAX4:	pr |= ((cu | nx) | (*(WORD *)BitRowW));
                				break; // max4
				default:  pr = cu;
			}
			if (xcnt == bytecnt) pr &= mask;
			*BitRowW = (BYTE)pr;
			BitRowW += Pitch2;
		}
		BegBitRowW++; 	BegBitRowR++;
	}
Fin:
	if (othermem)
	   	if (msize > Work4KSize) free (othermem);
}



/********************************************************* MaskOper */
/***
	Function:	MaskOper

	Description:
		This function implements 9 listed miscellaneous
		operations on the given BitMask; for some operations
		additional data may be used as input and/or output

	Statics/Globals:
		None

	Input Arguments:
		Pmask - pointer to the source Rectangle

		Xe - number of pixels in each row

		Ye - number of rows

		Pitch - address difference between neighboring rows (in bytes)
				in the source Rectangle
		Oper - list of operations, see below

		Data - pointer to the array of longs; it's contents for each
				operation is described below:


			moGET      0    // Get pixel value at coords: (Data[0], Data[1]);
								 // returns 0 or 1 (-1 if coordinates are wrong)
			moINVPIX   1    // Inverse pixel at coords: (Data[0], Data[1]);
								 // returns new pixel (-1 if coordinates are wrong)
			moSET      2    // Set pixel at coords: (Data[0], Data[1])
								 // to (Data[2] & 1), return old pixel value;
								 //  (-1 if coordinates are wrong)
			moSTAT     3    // Data[0]:=Sum(1), Data[1]:=Sum(X), Data[2]:=Sum(Y),
								 // Data[3]:=Sum(X*X), Data[4]:=Sum(Y*Y),
								 // Data[5]:=Sum(X*Y),
								 // Data[6]:=Min(X), Data[7]:=Max(X),
								 // Data[8]:=Min(Y), Data[9]:=Max(Y)
								 // returns number of ONEs (same as in Data[0])
			moZERO     4    // fill mask with 0; returns 0
			moFULL     5    // fill mask with 1; returns 0
			moINV      6    // Inverse mask; returns 0
			moFIND     7    // find (Data[2] & 1) beginning from point
								 // (Data[0], Data[1]);
								 // returns: 1 if OK (and sets the point
								 // to new coordinates), 0 if did not find.
			mo16OID    8    // Expand mask to convex 16-oid
								 // returns number of ONEs in the 16-oid

	Returns:
		see the list of operations

***/
long _stdcall  MaskOper (
				BYTE *Mask, DWORD Xe, DWORD Ye, DWORD Pitch,
               	WORD Oper, long *Data)
{
	long big, small, val, ix, iy, d0, d1, d2, d3, mu, sx, s = 0;
	DWORD rmask, dwcnt, bytecnt, one, p32, andmask, negmask,
    		full = 0xffffffffL;
	DWORD *lpix;
	BYTE *cpix, *wrk;
	WORD id, k, p, oldp, byterem;
	long *Dat, polygon[32];

	bytecnt = (Xe + 7) >> 3;
	byterem = (WORD)(bytecnt & 3);  // remainder of bytes on right end
	dwcnt = (Xe + 31) >> 5;
	rmask = full >> ((32 - (Xe & 31)) & 31);
	cpix = Mask;

	if (Oper == moFIND)
	{
		if (Data[2] & 1) negmask = 0L;	else negmask = full;
		one = Data[0];	iy = Data[1];
      if (one >= Xe) { one = 0;  iy++; }
      andmask = full << (one & 0x1f);
      one >>= 5;   dwcnt--;
      cpix += (Pitch * iy);
		while (iy < (long)Ye)
		{
			ix = one;
			lpix = (DWORD *)cpix;
			while (ix <= (long)dwcnt)
			{
				p32 = (lpix[ix] ^ negmask) & andmask;
				if (ix == (long)dwcnt) p32 &= rmask;
				if (p32) break;
                andmask = full;   ix++;
			}
         if (p32)
			{
				ix <<= 5;
				while ((p32 & 0xff) == 0) { ix += 8;  p32 >>= 8; }
				while ((p32 & 1) == 0) { ix++;  p32 >>= 1; }
				Data[0] = ix;  Data[1] = iy;
				s = 1;	break;
			}
         cpix += Pitch;  iy++;  one = 0L;
		}
		goto Fin;
	}
	if ((Oper == moGET) || (Oper == moINVPIX) || (Oper == moSET))
	{
		ix = Data[0];	iy = Data[1];
		if (((DWORD)ix >= Xe) || ((DWORD)iy >= Ye) || (ix < 0) || (iy < 0))
			return -1;
		k = (WORD)(ix & 7);  ix >>= 3;  p = 0;
		cpix = Mask + (iy * Pitch + ix);
		oldp = *cpix;	s = (oldp >> k) & 1;
        if (Oper != moGET)
        {
			if (Oper == moSET) p = (WORD)((s ^ Data[2]) & 1);
			else if (Oper == moINVPIX) p = 1;
			*cpix = (BYTE)(oldp ^ (p << k));
        }
		goto Fin;
	}
	if (Oper == moSTAT)
	{
		for (id = 0;  id < 10;  id++) Data[id] = 0L;
		Data[6] = Xe + 1;  Data[8] = Ye + 1;
		for (iy = 0;  (DWORD)iy < Ye;  iy++)
		{
			lpix = (DWORD *)cpix;
			cpix += Pitch;  one = 0L;
			mu = sx = 0L;
			for (ix = 0;  (DWORD)ix < Xe;  ix++)
			{
				if (!one) { p32 = *lpix++; one = 1L; }
				if (p32 & one)
				{
					mu++;		// ones in row
					sx += ix;	// sum of Xi
					Data[3] += (ix * ix);
					if (Data[6] > ix) Data[6] = ix;
					if (Data[7] < ix) Data[7] = ix;
					if (Data[8] > iy) Data[8] = iy;
					if (Data[9] < iy) Data[9] = iy;
				}
				one <<= 1;
			}
			s += mu;
			Data[1] += sx;
			Data[2] += (mu * iy);
			Data[4] += (mu * iy * iy);
			Data[5] += (sx * iy);
		}
		Data[0] = s;   goto Fin;
	}
	if ((Oper == moZERO) || (Oper == moFULL) || (Oper == moINV))
	{
		if (Oper == moZERO) andmask = negmask = 0L;
		if (Oper == moFULL) { andmask = 0L;  negmask = 0xffffffffL; }
		if (Oper == moINV) { andmask = negmask = 0xffffffffL; }
		iy = 0;
		while (iy++ < (long)Ye)
		{
			lpix = (DWORD *)cpix;  cpix += Pitch;  ix = 0L;
			while (ix++ < (long)dwcnt)
			{
				p32 = (*lpix & andmask) ^ negmask;
				if ((ix < (long)dwcnt) || (byterem == 0))
				{
					*lpix++ = p32;
				} else
				{
					wrk = (BYTE *)lpix;  k = byterem;
					while (k--) { *wrk++ = (BYTE)p32;  p32 >>= 8; }
				}
			}
		}
		goto Fin;
	}
	if (Oper == mo16OID)
	{
		Dat = polygon;
		big = (Xe + Ye + 1) * 7;  small = -big;
		// Set opposite values:
		Dat[0] = big;   Dat[1] = small;   // min/max X
		Dat[2] = big;   Dat[3] = small;   // min/max Y
		Dat[4] = big;   Dat[5] = small;   // min/max X+Y
		Dat[6] = big;   Dat[7] = small;   // min/max X-Y
		Dat[8] = big;   Dat[9] = small;   // min/max 5*X+12*Y
		Dat[10] = big;  Dat[11] = small;  // min/max 5*X-12*Y
		Dat[12] = big;  Dat[13] = small;  // min/max 12*X+5*Y
		Dat[14] = big;  Dat[15] = small;  // min/max 12*X-5*Y
		for (iy = 0;  iy < (long)Ye;  iy++)
		{
			lpix = (DWORD *)cpix;  cpix += Pitch;  one = 0L;
			for (ix = 0;  ix < (long)Xe;  ix++)
			{
				if (!one) { p32 = *lpix++; one = 1L; }
				if (p32 & one)
				{
					s++;
					if (Dat[0] > ix) Dat[0] = ix;
					if (Dat[1] < ix) Dat[1] = ix;
					if (Dat[2] > iy) Dat[2] = iy;
					if (Dat[3] < iy) Dat[3] = iy;
					val = ix + iy;
					if (Dat[4] > val) Dat[4] = val;
					if (Dat[5] < val) Dat[5] = val;
					val = ix - iy;
					if (Dat[6] > val) Dat[6] = val;
					if (Dat[7] < val) Dat[7] = val;
					val = ix * 5 + iy * 12;
					if (Dat[8] > val) Dat[8] = val;
					if (Dat[9] < val) Dat[9] = val;
					val = ix * 5 - iy * 12;
					if (Dat[10] > val) Dat[10] = val;
					if (Dat[11] < val) Dat[11] = val;
					val = ix * 12 + iy * 5;
					if (Dat[12] > val) Dat[12] = val;
					if (Dat[13] < val) Dat[13] = val;
					val = ix * 12 - iy * 5;
					if (Dat[14] > val) Dat[14] = val;
					if (Dat[15] < val) Dat[15] = val;
				}
				one <<= 1;
			}
		}
		if (s == 0) goto Fin;
		s = 0;  d0 = Dat[0];  d1 = Dat[1];  d2 = Dat[2];  d3 = Dat[3];
		cpix = Mask;  cpix += (Pitch * d2 + ((d0 >> 5) << 2));
		for (iy = d2;  iy <= d3;  iy++)
		{
			lpix = (DWORD *)cpix;
			cpix += Pitch;
            one = 1L << (d0 & 31L);
			for (ix = d0;  ix <= d1;  ix++)
			{
//				if ( (ix < Dat[0]) || (ix > Dat[1]) ) goto EndX16;
//				if ( (iy < Dat[2]) || (iy > Dat[3]) ) goto EndX16;
                if (*lpix & one) goto EndX16plus;  // already 1
				val = ix + iy;
				if ( (val < Dat[4]) || (val > Dat[5]) ) goto EndX16;
				val = ix - iy;
				if ( (val < Dat[6]) || (val > Dat[7]) ) goto EndX16;
				val = ix * 5 + iy * 12;
				if ( (val < Dat[8]) || (val > Dat[9]) ) goto EndX16;
				val = ix * 5 - iy * 12;
				if ( (val < Dat[10]) || (val > Dat[11]) ) goto EndX16;
				val = ix * 12 + iy * 5;
				if ( (val < Dat[12]) || (val > Dat[13]) ) goto EndX16;
				val = ix * 12 - iy * 5;
				if ( (val < Dat[14]) || (val > Dat[15]) ) goto EndX16;

				*lpix |= one;
EndX16plus:
                s++;
EndX16:
				if (!(one <<= 1)) { one = 1L;  lpix++; }
			}
		}
		goto Fin;
	}

Fin:
	return s;
}



/******************************************************* MaskReconMask */
/***
	Function:	MaskReconMask

	Description:
		This function reconstructs BitMem2 within BitMem1.
		Algorithm of reconstruction is as follows:
		Do
			OldBitMem2 := BitMem2
			BitMem2 := Dilate(BitMem2) AND BitMem1
		While OldBitMem2 <> Bitmem2

		Dilate() means "set to ONE every bit which has ONE-neighbor(s)"
		This expansion is limited by BitMem1.

	Statics/Globals:
		None

	Input Arguments:
		pBitMem1 - pointer to the limiting Bit Mask

		Xe - number of pixels (bits) in each row

		Ye - number of rows

		Pitch1 - address difference between neighboring rows (in bytes)
				in the limiting Bit Mask
		pBitMem2 - pointer to the Bit Mask, to reconstruct

		Pitch2 - address difference between neighboring rows (in bytes)
				in the Bit Mask, being reconstructed

	Output Arguments:
		None

	Returns:
		None

***/
void _stdcall MaskReconMask (
				BYTE *BitMem1,
                DWORD Xe, DWORD Ye, DWORD Pitch1,
				BYTE *BitMem2, DWORD Pitch2)
{
	DWORD prv, cur, nxt, ll, rmask, tmask, vmask;
	DWORD xcnt, ycnt, dwordcnt, flag, chng, bytecnt;
	DWORD *BitRowRR, *BitRowR, *BitRowW;
	BYTE *BegBitRowR, *BegBitRowW, *wrk;
	WORD k, byterem;

	bytecnt = (Xe + 7) >> 3;
	byterem = (WORD)(bytecnt & 3);  // remainder of bytes on right end
	dwordcnt = (Xe + 31) >> 5;
	rmask = 0xffffffffL >> ((32 - (Xe & 31)) & 31);

	// momAND | mmcmIGNORE: (BitMem1 & BitMem2) => BitMem2, ignore mask
	MemMaskMem (BitMem1, BitMem2, NULL, bytecnt, Ye, Pitch1, Pitch2,
   			0L, momAND | mmcmIGNORE);

LeftToRight:
	BegBitRowR = BitMem1;  BegBitRowW = BitMem2;  ycnt = flag = 0;
	while (ycnt++ < Ye)
	{
		BitRowW = BitRowR = (DWORD *)BegBitRowW;
		BitRowRR = (DWORD *)BegBitRowR;  xcnt = cur = 0;
		while ((xcnt++) < dwordcnt)
		{
			prv = cur;  cur = *BitRowR++;
			tmask = *BitRowRR++;
			if (xcnt < dwordcnt - 1)  nxt = *BitRowR;
			else
			{
				if (xcnt == dwordcnt - 1) nxt = *BitRowR & rmask;
				else {	cur &= rmask;  tmask &= rmask;  nxt = 0; }
			}
			ll = cur;
			if (nxt & 1) ll |= 0x80000000L;
			if (prv & 0x80000000L) ll |= 1;
			ll &= tmask;
			while ((ll = (ll | (ll << 1) | (ll >> 1)) & tmask) != cur)
			{
				flag |= (ll ^ cur);   cur = ll;
			}
			if ((xcnt == dwordcnt) && (byterem))
			{
				k = byterem;  wrk = (BYTE *)BitRowW;
				while (k--) {  *wrk++ = (BYTE)ll;  ll >>= 8; }
			} else
				*BitRowW++ = ll;
		}
		BegBitRowR += Pitch1;	BegBitRowW += Pitch2;
	}

// Top To Bottom:
	BegBitRowR = BitMem1;  BegBitRowW = BitMem2;
	xcnt = 0;  vmask = 0xffffffffL;
	while (xcnt++ < dwordcnt)
	{
		BitRowRR = (DWORD *)BegBitRowR;
		BitRowW = (DWORD *)BegBitRowW;
		BitRowR = (DWORD *)(BegBitRowW + Pitch2);
		if (xcnt == dwordcnt) vmask = rmask;
		ycnt = cur = 0;  nxt = *BitRowW & vmask;
		while ((ycnt++) < Ye)
		{
			prv = cur;  cur = nxt;  tmask = *BitRowRR & vmask;
			if (ycnt < Ye) nxt = *BitRowR; else nxt = 0;
			ll = (prv | cur | nxt) & tmask;
			chng = (ll ^ cur) & vmask;  // new ones
			flag |= chng;	cur = ll;
			if ((xcnt == dwordcnt) && (byterem))
			{
				k = byterem;  wrk = (BYTE *)BitRowW;
				while (k--) {  *wrk++ = (BYTE)ll;  ll >>= 8; }
			} else
				*BitRowW = ll;
			BitRowW = BitRowR;
			BitRowR = (DWORD *)((BYTE *)BitRowW + Pitch2);
			BitRowRR = (DWORD *)((BYTE *)BitRowRR + Pitch1);
		}
		BegBitRowW += sizeof(DWORD);  BegBitRowR += sizeof(DWORD);
	}

// Right To Left:
	BegBitRowR = BitMem1 + (dwordcnt * sizeof(DWORD));
	BegBitRowW = BitMem2 + (dwordcnt * sizeof(DWORD));
	ycnt = 0;
	while (ycnt++ < Ye)
	{
		BitRowW = BitRowR = (DWORD *)BegBitRowW;
		BitRowRR = (DWORD *)BegBitRowR;
		xcnt = dwordcnt;  cur = 0;
		prv = *(--BitRowR) & rmask;  vmask = rmask;
		while (xcnt--)
		{
			nxt = cur;  cur = prv;	tmask = *(--BitRowRR) & vmask;
			if (xcnt) prv = *(--BitRowR); else prv = 0;
			ll = cur;
			if (nxt & 1) ll |= 0x80000000L;
			if (prv & 0x80000000L) ll |= 1;
			ll &= tmask; 
			while ((ll = (ll | (ll << 1) | (ll >> 1)) & tmask) != cur)
			{
				flag |= (ll ^ cur);   cur = ll;
			}
			BitRowW--;
			if ((xcnt == dwordcnt - 1) && (byterem))
			{
				k = byterem;  wrk = (BYTE *)BitRowW;
				while (k--) {  *wrk++ = (BYTE)ll;  ll >>= 8; }
			} else
				*BitRowW = ll;
			vmask = 0xffffffffL;
		}
		BegBitRowR += Pitch1;	BegBitRowW += Pitch2;
	}
// Bottom To Top:
	BegBitRowR = BitMem1 + Pitch1 * (Ye - 1);
	BegBitRowW = BitMem2 + Pitch2 * (Ye - 1);
	xcnt = 0;  vmask = 0xffffffffL;
	while (xcnt++ < dwordcnt)
	{
		BitRowRR = (DWORD *)BegBitRowR;
		BitRowW = (DWORD *)BegBitRowW;
		BitRowR = (DWORD *)(BegBitRowW - Pitch2);
		if (xcnt == dwordcnt) vmask = rmask;
		ycnt = Ye;  cur = 0;  prv = *BitRowW & vmask;
		while (ycnt--)
		{
			nxt = cur;  cur = prv;  tmask = *BitRowRR & vmask;
			if (ycnt)
			{
				prv = *BitRowR;
				BitRowRR = (DWORD *)((BYTE *)BitRowRR - Pitch1);
			} else prv = 0;
			ll = (prv | cur | nxt) & tmask;
			flag |= ((ll ^ cur) & vmask);  // new ones
			cur = ll;
			if ((xcnt == dwordcnt) && (byterem))
			{
				k = byterem;  wrk = (BYTE *)BitRowW;
				while (k--) {  *wrk++ = (BYTE)ll;  ll >>= 8; }
			} else
				*BitRowW = ll;
			BitRowW = BitRowR;
			if (ycnt)
				BitRowR = (DWORD *)((BYTE *)BitRowW - Pitch2);
		}
		BegBitRowW += sizeof(DWORD);  BegBitRowR += sizeof(DWORD);
	}
	if (flag) goto LeftToRight;
}


/************************************************************ MaskFillHoles */
/***
	Function:	MaskFillHoles

	Description:
		This function fills holes in a bit mask.
		A hole is a set of zero pixel not connected with the boundary.
		Algorithm:
		BitMask2 := ~(Reconstruction(Boundary) within (~BitMask1))

	Statics/Globals:
		None

	Input Arguments:
		BitMask1 - pointer to the source Bit Mask

		Xe - number of pixels (bits) in each row

		Ye - number of rows

		Pitch1 - address difference between neighboring rows (in bytes)
				in the source Bit Mask
		BitMask2 - pointer to the target Bit Mask

		Pitch2 - address difference between neighboring rows (in bytes)
				in the target Bit Mask

	Returns:
		None

***/
void _stdcall MaskFillHoles (
				BYTE *BitMask1,
                DWORD Xe, DWORD Ye, DWORD Pitch1,
				BYTE *BitMask2, DWORD Pitch2)
{
	DWORD msize, bytecnt;
	BYTE *mem;

	bytecnt = ((Xe + 31) >> 5) << 2;
//	memlen = bytecnt * Ye;
//    mem = (BYTE *)malloc(memlen);
//	if (!mem) return;
	msize = bytecnt * Ye;
   if (msize > Work4KSize)
   {
     	mem = (BYTE *)malloc(msize);
	  	if (!mem) return;
	} else mem = Work4K;
	  // CopyNot BitMask1 into mem (ignore mask):
	MemMaskMem (BitMask1, mem, mem,
    		bytecnt, Ye, Pitch1, bytecnt, bytecnt,
			momNOT1 | mmcmIGNORE);
	  // Make FRAME in BitMask2:
	MaskOperMask (BitMask2, Xe, Ye, Pitch1,
    			  BitMask2, Pitch2, mnmFRAME);
	  // Reconstruct FRAME BitMask2 by mask mem (which is NOT BitMem1):
	MaskReconMask (mem, Xe, Ye, bytecnt, BitMask2, Pitch2);
	  // Inverse result: Copy2Not BitMask2 into itself (ignore mask):
	MaskOper (BitMask2, Xe, Ye, Pitch2, moINV, NULL);
   if (msize > Work4KSize) free(mem);
}


/********************************************************* MaskLine */
/***
	Function:	MaskLine

	Description:
		This function draws a line on given BitMask;
		additional data is located in the array of longs

	Statics/Globals:
		None

	Input Arguments:
		mask - pointer to the source Rectangle
		xe - number of pixels in each row
		ye - number of rows
		pitch - address difference between neighboring rows (in bytes)
				in the source Rectangle
		data - pointer to the array of longs; it's contents for each
				operation is described below:

		linemode -
            mlPOLY    0x100   // polyline from {Data[0], Data[1]}
							      // to {Data[2], Data[3]}, then
							      // to {Data[4], Data[5]}, and so on
            mlSPLINE0 0x200   // closed spline
            mlSPLINE1 0x300  	// unclosed spline with free ends
            mlSPLINE2 0x400	// unclosed spline with straight first interval
            mlSPLINE3 0x500	// unclosed spline with straight last interval

            mlCIRCLE  0x800   // circle with center in {Data[0], Data[1]}
                           // and radius Data[2]
            mlARC     0x900   // ark with center in {Data[0], Data[1]}
                           // from {Data[2], Data[3]} to {Data[4], Data[5]}
                           // in clockhand direction

        pen -	0, 1 - put 0/1 pixel; >1 - inverse the pixel.
		count - number of sections

	Returns:
		number of bytes drawn

***/
long _stdcall  MaskLine (void *mask, long xe, long ye, long pitch,
                  long linemode, long pen, long count, long *data)
{
    long xstart, ystart, n, s;

    n = GetLineCode (Work4K, Work4KSize, linemode, count, data,
                  &xstart, &ystart);
    if (n > 0)
    {
        s = MaskDrawCode (mask, xe, ye, pitch,
                &xstart, &ystart, Work4K, n, pen);
    } else s = -1;
	return s;
}


/*********************************************************** MaskNei */
/***
	Function:	MaskNei

	Description:
		This function implements dilation and erosion operations.
		Dilation is maximum of a pixel and it's neighbors.
		Erosion is minimum of a pixel and it's neighbors

	Statics/Globals:
		None

	Arguments:
		mem - pointer to the Rectangle

		xe - number of pixels (bytes) in each row

		ye - number of rows

		pitch - address difference between neighboring rows (in bytes)
				in the Rectangle
		oper :
			mnmMIN   =0   min of self and 8 bit neighbors (erosion);
			mnmMAX   =1   max of self and 8 bit neighbors (dilation);
			mnmMIN4	 =20  min of the pixel and it's 4 neighbors;
			mnmMAX4	 =21  max of the pixel and it's 4 neighbors;
			mnmMINH2 =22  min of self and 2 bit horizontal neighbors;
			mnmMAXH2 =23  max of self and 2 bit horizontal neighbors;
			mnmMINV2 =24  min of self and 2 bit vertical neighbors;
			mnmMAXV2 =25  max of self and 2 bit vertical neighbors;
			mnmOPEN8 =26  same as mnmMIN followed by mnmMAX;
			mnmCLOSE8 =27 same as mnmMAX followed by mnmMIN;
			mnmOPEN4 =28  same as mnmMIN4 followed by mnmMAX4;
			mnmCLOSE4 =29 same as mnmMAX4 followed by mnmMIN4;

      rep - number of repetitions

	Returns:
		None

***/
void _stdcall MaskNei (BYTE *mem, DWORD xe, DWORD ye,
				DWORD pitch, long oper, long rep)
{
	DWORD pcurr, prev, curr, nxt, ncurr;
	DWORD dmask, dcompl, full = 0xffffffff;
	DWORD xcnt, ycnt, i, dwords, l, ll, lr;
	BYTE *r, *rn;
    DWORD *rdw, *wdw, *rpdw, *rndw;
    long repcnt;

    dwords = (xe + 31) >> 5;
    dmask = (DWORD)(full >> ((32 - (xe & 31)) & 31));
    dcompl = dmask ^ full;

    repcnt = (long)rep;
    rpdw = (DWORD *)Work4K;
	if (oper == mnmMIN)
    {
//   	rpdw = (DWORD *)malloc(dwords * sizeof(DWORD));  // get memory for 1 row
MNmnmMIN:
        for (i = 0;  i < dwords;  i++) rpdw[i] = full;
		ycnt = 0;  r = mem;  wdw = rpdw;
		while (ycnt++ < ye)
		{
			xcnt = 0;  curr = full;
            rdw = (DWORD *)r;   nxt = rdw[0];
			while ((i = xcnt++) < dwords)
			{
				prev = (curr >> 31);  curr = nxt;
                l = ((curr << 1) | prev) & curr;
				if (xcnt < dwords) prev = ((nxt = rdw[xcnt]) << 31);
                else { nxt = full;  curr |= dcompl;  prev = 0x80000000; }
                l &= ((curr >> 1) | prev);
                if (xcnt == dwords) l &= dmask;
                wdw[i] &= l;	// correct previous row
                rdw[i] = rpdw[i] & l; // current row
                rpdw[i] = l;	// for next pass
    		}
            wdw = (DWORD *)r;
            r += pitch;
        }
        if ((--repcnt) > 0) goto MNmnmMIN;
//      free(rpdw);
        return;
    }
	if (oper == mnmMAX)
    {
//   	rpdw = (DWORD *)malloc(dwords * sizeof(DWORD));  // get memory for 1 row
MNmnmMAX:
        for (i = 0;  i < dwords;  i++) rpdw[i] = 0;
		ycnt = 0;  r = mem;  wdw = rpdw;
		while (ycnt++ < ye)
		{
			xcnt = 0;  curr = 0;
            rdw = (DWORD *)r;  nxt = rdw[0];
			while ((i = xcnt++) < dwords)
			{
         	    prev = curr >> 31;  curr = nxt;
				if (xcnt < dwords) prev |= ((nxt = rdw[xcnt]) << 31);
                else { nxt = 0;  curr &= dmask; }
                l = (curr | (curr << 1) | (curr >> 1) | prev);
				if (xcnt == dwords) l &= dmask;
                wdw[i] |= l;	// correct previous row
                rdw[i] = rpdw[i] | l; // current row
                rpdw[i] = l;	// for next pass
      	    }
            wdw = (DWORD *)r;
            r += pitch;
        }
        if ((--repcnt) > 0) goto MNmnmMAX;
//      free(rpdw);
        return;
    }
	if (oper == mnmMINH2)
    {
MNmnmMINH2:
		ycnt = 0;  r = mem;
		while (ycnt++ < ye)
		{
			xcnt = 0;  rdw = (DWORD *)r;
            curr = full;  nxt = rdw[0];
			while ((i = xcnt++) < dwords)
			{
				prev = curr;  curr = nxt;
				if (xcnt < dwords) nxt = rdw[xcnt];
                else
                {
            	    nxt = full;
                    curr |= dcompl;
                }
                ll = (curr << 1) | (prev >> 31);
                lr = (curr >> 1) | (nxt << 31);
                l = ll & curr & lr;
				if (xcnt == dwords) l &= dmask;
                rdw[i] = l;
    		}
            r += pitch;
        }
        if ((--repcnt) > 0) goto MNmnmMINH2;
        return;
    }
	if (oper == mnmMINV2)
    {
MNmnmMINV2:
		xcnt = 0;
		while ((i = xcnt++) < dwords)
		{
			ycnt = 0;  r = mem;  rdw = (DWORD *)r;
            curr = full;  nxt = rdw[i];
			while (ycnt++ < ye)
			{
         	    rdw = (DWORD *)r;
				prev = curr;  curr = nxt;
                if (ycnt < ye) r += pitch;
                wdw = (DWORD *)r;
                nxt = wdw[i];
                prev &= (curr & nxt);
                rdw[i] = prev;
    		}
        }
        if ((--repcnt) > 0) goto MNmnmMINV2;
    	return;
    }
	if (oper == mnmMAXH2)
    {
MNmnmMAXH2:
		ycnt = 0;  r = mem;
		while (ycnt++ < ye)
		{
			xcnt = 0;  rdw = (DWORD *)r;
            curr = 0;  nxt = rdw[0];
			while ((i = xcnt++) < dwords)
			{
				prev = curr;  curr = nxt;
				if (xcnt < dwords) nxt = rdw[xcnt];
                else
                {
            	    nxt = 0;
                    curr &= dmask;
                }
                ll = (curr << 1) | (prev >> 31);
                lr = (curr >> 1) | (nxt << 31);
                l = ll | curr | lr;
                if (xcnt == dwords) l &= dmask;
                rdw[i] = l;
    		}
            r += pitch;
        }
        if ((--repcnt) > 0) goto MNmnmMAXH2;
        return;
	}
	if (oper == mnmMAXV2)
    {
MNmnmMAXV2:
		xcnt = 0;
		while ((i = xcnt++) < dwords)
		{
			ycnt = 0;  r = mem;  rdw = (DWORD *)r;
            curr = nxt = rdw[i];
			while (ycnt++ < ye)
			{
         	    rdw = (DWORD *)r;
				prev = curr;  curr = nxt;
                if (ycnt < ye) r += pitch;
                wdw = (DWORD *)r;
                nxt = wdw[i];
                prev |= (curr | nxt);
                rdw[i] = prev;
    		}
        }
        if ((--repcnt) > 0) goto MNmnmMAXV2;
    	return;
	}
	if (oper == mnmMIN4)
    {
//   	rpdw = (DWORD *)malloc(dwords * sizeof(DWORD));  // get memory for 1 row
MNmnmMIN4:
        rdw = (DWORD *)mem;
        for (i = 0;  i < dwords;  i++) rpdw[i] = rdw[i];
		ycnt = 0;  r = mem;
		while (ycnt++ < ye)
		{
			xcnt = 0;  curr = full;
            rdw = (DWORD *)r;  nxt = rdw[0];
            if (ycnt < ye) rn = r + pitch;
            else rn = r;
            rndw = (DWORD *)rn;
			while ((i = xcnt++) < dwords)
			{
				prev = curr;  curr = nxt;
				if (xcnt < dwords) nxt = rdw[xcnt];
                else
                {
            	    nxt = full;
                    curr |= dcompl;
                }
                pcurr = rpdw[i];  rpdw[i] = curr;	// for next pass
                ncurr = rndw[i];
                ll = (curr << 1) | (prev >> 31);
                lr = (curr >> 1) | (nxt << 31);
                l = ll & curr & lr & pcurr & ncurr;
				if (xcnt == dwords) l &= dmask;
                rdw[i] = l;
    		}
            r += pitch;
        }
        if ((--repcnt) > 0) goto MNmnmMIN4;
//      free(rpdw);
        return;
	}
	if (oper == mnmMAX4)
    {
//   	rpdw = (DWORD *)malloc(dwords * sizeof(DWORD));  // get memory for 1 row
MNmnmMAX4:
        rdw = (DWORD *)mem;
        for (i = 0;  i < dwords;  i++) rpdw[i] = rdw[i];
		ycnt = 0;  r = mem;
		while (ycnt++ < ye)
		{
			xcnt = 0;  curr = 0;
            rdw = (DWORD *)r;  nxt = rdw[0];
            if (ycnt < ye) rn = r + pitch;
            else rn = r;
            rndw = (DWORD *)rn;
			while ((i = xcnt++) < dwords)
			{
				prev = curr;  curr = nxt;
				if (xcnt < dwords) nxt = rdw[xcnt];
                else
                {
            	    nxt = 0;
                    curr &= dmask;
                }
                pcurr = rpdw[i];  rpdw[i] = curr;	// for next pass
                ncurr = rndw[i];
                ll = (curr << 1) | (prev >> 31);
                lr = (curr >> 1) | (nxt << 31);
                l = ll | curr | lr | pcurr | ncurr;
				if (xcnt == dwords) l &= dmask;
                rdw[i] = l;
    		}
            r += pitch;
        }
        if ((--repcnt) > 0) goto MNmnmMAX4;
//      free(rpdw);
    	return;
	}
	if (oper == mnmOPEN8)
    {
   	    MaskNei (mem, xe, ye, pitch, mnmMIN, rep);
        MaskNei (mem, xe, ye, pitch, mnmMAX, rep);
        return;
    }
	if (oper == mnmCLOSE8)
    {
		MaskNei (mem, xe, ye, pitch, mnmMAX, rep);
		MaskNei (mem, xe, ye, pitch, mnmMIN, rep);
   	    return;
    }
	if (oper == mnmOPEN4)
    {
		MaskNei (mem, xe, ye, pitch, mnmMIN4, rep);
		MaskNei (mem, xe, ye, pitch, mnmMAX4, rep);
  		return;
    }
	if (oper == mnmCLOSE4)
    {
		MaskNei (mem, xe, ye, pitch, mnmMAX4, rep);
		MaskNei (mem, xe, ye, pitch, mnmMIN4, rep);
   	    return;
    }
}


/**************************************************** MaskNeiTable */
/***
	Function:	MaskNeiTable

	Description:
		This function ...

	Statics/Globals:
		None

	Input Arguments:
		mask1, mask2 - pointers to the source and target bit masks
        		!!! must be different !!!

		xe - number of pixels in each row (for both masks)

		ye - number of rows

		pitch1, pitch2 - address difference between neighboring
        		rows (in bytes)	in the source and target bit masks

		table - pointer to the array of 512 bits (64 bytes);

    Returns: a number of changed pixels

***/
long _stdcall  MaskNeiTable (
				BYTE *mask1, DWORD xe, DWORD ye, DWORD pitch1,
               	BYTE *mask2, DWORD pitch2, BYTE *table)
{
	BYTE *upline, *curline, *downline, *up, *cur, *down, *wr;
	DWORD k, pos, i = 0;
	WORD uppix, curpix, downpix, wrk, ind, outpix;
    long ret = 0;

    upline = curline = downline = mask1;
    while (i < ye)
    {
        wr = mask2 + pitch2 * (i++);
    	if (i < ye) downline += pitch1;
        up = upline;  cur = curline;  down = downline;
        wrk = (WORD)*up++;
        uppix = (WORD)(((wrk & 1) | (wrk << 1)) << 3);
        wrk = (WORD)*cur++;
        curpix = (WORD)((wrk & 1) | (wrk << 1));
        wrk = (WORD)*down++;
        downpix = (WORD)((wrk & 1) | (wrk << 1));
        k = outpix = 0;
        while (k++ < xe)
        {
         	ind =(WORD)((uppix & 0x38) | (downpix & 7));
            wrk = (WORD)((table[ind] >> (curpix & 7)) & 0x01);
            outpix = (WORD)((outpix >> 1) | (wrk << 7));
            uppix >>= 1;  curpix >>= 1;  downpix >>= 1;
            ret += ((wrk ^ curpix) & 1);	// 1 if was changed
            pos = (k & 7);
            if (pos == 0)
            {
            	*wr++ = outpix;  outpix = 0;
            }
            if ((pos == 1) && (k + 7 < xe))
            {
        		wrk = (WORD)*cur++;
                curpix = (WORD)((curpix & 0xff) | (wrk << 8));
		        wrk = (WORD)*down++;
                downpix = (WORD)((downpix & 0xff) | (wrk << 8));
            }
            if ((pos == 4) && (k + 4 < xe))
            {
        		wrk = (WORD)*up++;
                uppix = (WORD)((uppix & 0xff) | (wrk << 8));
			}
        }
        if (xe & 7)
           *wr = (BYTE)((outpix & 0xff) >> (8 - (xe & 7)));
        upline = curline;  curline = downline;
    }
    return ret;
}


/*************************************************** MaskNeiTableR */
/***
	Function:	MaskNeiTableR (recursive)

	Description:
		This function ...

	Statics/Globals:
		None

	Input Arguments:
		mask - pointer to the bit mask

		xe - number of pixels in each row

		ye - number of rows

		pitch - address difference between neighboring
        		rows (in bytes)	in the bit mask

		table - pointer to the array of 512 bits (64 bytes);

    Returns: a number of changed pixels

***/
long _stdcall  MaskNeiTableR (
				BYTE *mask, DWORD xe, DWORD ye, DWORD pitch,
                BYTE *table)
{
	BYTE *upline, *curline, *downline, *up, *cur, *down, *wr;
	DWORD k, pos, i = 0;
	WORD uppix, curpix, downpix, wrk, ind, outpix, chng;
    long ret = 0;

    upline = curline = downline = mask;
    while (i++ < ye)
    {
    	if (i < ye) downline += pitch;
        up = upline;  cur = wr = curline;  down = downline;
        wrk = (WORD)*up++;
        uppix = (WORD)(((wrk & 1) | (wrk << 1)) << 3);
        wrk = (WORD)*cur++;
        curpix = (WORD)((wrk & 1) | (wrk << 1));
        wrk = (WORD)*down++;
        downpix = (WORD)((wrk & 1) | (wrk << 1));
        k = outpix = 0;
        while (k++ < xe)
        {
         	ind =(WORD)((uppix & 0x38) | (downpix & 7));
            wrk = (WORD)((table[ind] >> (curpix & 7)) & 0x01);
            outpix = (WORD)((outpix >> 1) | (wrk << 7));
            uppix >>= 1;  curpix >>= 1;  downpix >>= 1;
            chng = (WORD)((wrk ^ curpix) & 1);	// 1 if was changed
            ret += chng;
            curpix ^= chng;		// recursion is here!!!
            pos = (k & 7);
            if (pos == 0)
            {
            	*wr++ = outpix;  outpix = 0;
            }
            if ((pos == 1) && (k + 7 < xe))
            {
        		wrk = (WORD)*cur++;
                curpix = (WORD)((curpix & 0xff) | (wrk << 8));
		        wrk = (WORD)*down++;
                downpix = (WORD)((downpix & 0xff) | (wrk << 8));
            }
            if ((pos == 4) && (k + 4 < xe))
            {
        		wrk = (WORD)*up++;
                uppix = (WORD)((uppix & 0xff) | (wrk << 8));
			}
        }
        if (xe & 7)
           *wr = (BYTE)((outpix & 0xff) >> (8 - (xe & 7)));
        upline = curline;  curline = downline;
    }
    return ret;
}


/******************************************************** MaskOnes */
/***
	Function:	MaskOnes

	Description:
		This function counts number of 1-bits in given subrectangle

	Statics/Globals:
		None

	Input Arguments:
		pmem - pointer to the Bit Mask

        xl, yt - subrectangle left top corner (in bits)

		xe - number of pixels (bits) in each row
		ye - number of rows

		pitch - address difference between neighboring rows (in bytes)
				in the limiting Bit Mask

	Returns:
		number of 1-bits in the subrectangle

***/
long _stdcall MaskOnes (
				BYTE *pmem,
                DWORD xl, DWORD yt,
                DWORD xe, DWORD ye, DWORD pitch)
{
	WORD lmask, rmask, bval, ones = 0xff;
	DWORD xcnt, ycnt, xstrt, xfin, yfin, wrk, sum = 0;
    BYTE *row;

    xstrt = xl >> 3;
    lmask = (WORD)((ones << (xl & 7)) & ones);
    wrk = xl + xe;  xfin = wrk >> 3;
    if (wrk &= 7) rmask = (WORD)(ones >> (8 - wrk));
    else { xfin--;  rmask = ones; }
    row = pmem + (yt * pitch);
    ycnt = yt;  yfin = yt + ye;
    while (ycnt++ < yfin)
    {
        for (xcnt = xstrt;  xcnt <= xfin;  xcnt++)
        {
        	bval = row[xcnt];
            if (xcnt == xstrt) bval &= lmask;
            if (xcnt == xfin) bval &= rmask;
            sum += BitsInByte[bval];
        }
        row += pitch;
    }
	return (long)sum;
}


/********************************************************* MaskOperStat */
/***
	Function:	MaskOperStat

	Description:
		This function implements moSTAT part of MaskOper function
        using double data type array so x & y moments of inertia are
        returned correctly.

	Statics/Globals:
		None

	Input Arguments:
		Pmask - pointer to the source Rectangle

		Xe - number of pixels in each row

		Ye - number of rows

		Pitch - address difference between neighboring rows (in bytes)
				in the source Rectangle

		Data - pointer to the array of doubles (moSTAT)
                Data[0]:=Sum(1)
                Data[1]:=Sum(X)
                Data[2]:=Sum(Y)
                Data[3]:=Sum(X*X)
                Data[4]:=Sum(Y*Y)
                Data[5]:=Sum(X*Y)
				Data[6]:=Min(X)
                Data[7]:=Max(X)
				Data[8]:=Min(Y)
                Data[9]:=Max(Y)
	Returns:
	        returns number of ONEs (same as in Data[0])

***/
long _stdcall  MaskOperStat(
				BYTE *Mask, DWORD Xe, DWORD Ye, DWORD Pitch,
               	double *Data)
{
	long ix, iy, mu, sx, s = 0;
	DWORD one, p32, full = 0xffffffffL;
	DWORD *lpix;
	BYTE *cpix;
	WORD id;

	cpix = Mask;

    for (id = 0;  id < 10;  id++)
        Data[id] = 0.0;

    Data[6] = Xe + 1;
    Data[8] = Ye + 1;

    for (iy = 0;  (DWORD)iy < Ye;  iy++)
    {
        lpix = (DWORD *)cpix;
        cpix += Pitch;
        one = 0L;
        mu = sx = 0L;
        for (ix = 0;  (DWORD)ix < Xe;  ix++)
        {
            if (!one) { p32 = *lpix++; one = 1L; }
            if (p32 & one)
            {
                mu++;		// ones in row
                sx += ix;	// sum of Xi
                Data[3] += (ix * ix);
                if (Data[6] > ix) Data[6] = ix;
                if (Data[7] < ix) Data[7] = ix;
                if (Data[8] > iy) Data[8] = iy;
                if (Data[9] < iy) Data[9] = iy;
            }
            one <<= 1;
        }
        s += mu;
        Data[1] += sx;
        Data[2] += (mu * iy);
        Data[4] += (mu * iy * iy);
        Data[5] += (sx * iy);
    }
    Data[0] = s;
    return s;
}


/******************************************************* MaskReconFast */
/***
	Function:	MaskReconFast

	Description:
		This function reconstructs BitMem2 within BitMem1.
		Algorithm of reconstruction is as follows:
		Do
			OldBitMem2 := BitMem2
			BitMem2 := Dilate(BitMem2) AND BitMem1
		While OldBitMem2 <> Bitmem2

		Dilate() means "set to ONE every bit which has ONE-neighbor(s)"
		This expansion is limited by BitMem1.

	Statics/Globals:
		None

	Input Arguments:
		pBitMem1 - pointer to the limiting Bit Mask

		Xe - number of pixels (bits) in each row

		Ye - number of rows

		Pitch1 - address difference between neighboring rows (in bytes)
				in the limiting Bit Mask
		pBitMem2 - pointer to the Bit Mask, to reconstruct

		Pitch2 - address difference between neighboring rows (in bytes)
				in the Bit Mask, being reconstructed
        Xmin, Ymin, Xmax, Ymax - start limits (for example, if
        		the reconstruction starts from a single point,
                these parameters are it's coordinates repeated twice)

	Output Arguments:
		None

	Returns:
		None

    Flag bits:
     0x10, 0x01 - LeftToRight
     0x20, 0x02 - Top To Bottom
     0x40, 0x04 - Right To Left
     0x80, 0x08 - Bottom To Top

***/
void _stdcall MaskReconFast (
				BYTE *BitMem1,
                DWORD Xe, DWORD Ye, DWORD Pitch1,
				BYTE *BitMem2, DWORD Pitch2,
                DWORD *xmin, DWORD *ymin,
                DWORD *xmax, DWORD *ymax)
{
	DWORD prv, cur, ll, rmask, tmask, smask,
    	left = 0, right = 0, ones = 0xffffffffL;
	DWORD xcnt, ycnt, dwordcnt, dwcnt1, flag, rflag, i, chng;
   DWORD xstrt, ystrt, xfin, yfin, xnext, ynext, xwrk, ywrk;
	DWORD *BitRowRR, *BitRowR, *prevrow;
	BYTE *BegBitRowR, *BegBitRowW;
   BYTE *info;

	dwordcnt = (Xe + 31) >> 5;  dwcnt1 = dwordcnt - 1;
	rmask = ones >> ((32 - (Xe & 31)) & 31);
	smask = ones ^ rmask;
   xstrt = (*xmin) >> 5;  xfin = (*xmax) >> 5; // in DWORDs
   if ((long)(ystrt = *ymin) < 0) ystrt = 0;
   if ((yfin = *ymax) >= Ye) yfin = Ye - 1;
   flag = 0xff;
   if (Ye + 1 < Work4KSize) info = Work4K;
   else info = (BYTE *)malloc(Ye + 2);
   for (i = 1;  i <= Ye;  i++) info[i] = 0xff;
   info[0] = info[Ye + 1] = 0;

LeftToRight:
   if ((flag &= 0xfe) == 0) goto Fin;
   if ((flag & 0xfa) == 0) goto TopToBottom;
   ycnt = ystrt;
	BegBitRowR = BitMem1 + (Pitch1 * ystrt);
   BegBitRowW = BitMem2 + (Pitch2 * ystrt);
   xnext = xfin;
	while (ycnt++ <= yfin)
	{
      rflag = 0;   info[ycnt] &= 0xfe;
      if ((info[ycnt] & 0xaa) == 0) goto LtoRfin;
		BitRowR = (DWORD *)BegBitRowW;
		BitRowRR = (DWORD *)BegBitRowR;
      xcnt = xstrt;  xwrk = xfin;  prv = 0; // 0/1 here
		while (xcnt <= xwrk)
		{
			tmask = BitRowRR[xcnt];
         if (xcnt == dwcnt1) tmask &= rmask;
         if (tmask)
         {
           	cur = BitRowR[xcnt] & tmask;
				while ((ll = (cur | (cur << 1) | prv) & tmask) != cur)
            {
              	cur = ll;  rflag |= 1;
            }
				if ((prv = ll >> 31) == 1)
            {
            	if ((xcnt == xwrk) && (xwrk < dwordcnt - 1))
					{
  		         	xwrk++;
                  if (xnext < xwrk)
                  {
                     xnext = xwrk;  right = 0;
                  }
					}
            }
         } else ll = prv = 0;
			if (xcnt == dwcnt1) ll |= (BitRowR[xcnt] & smask);
         BitRowR[xcnt] = ll;
         if (xcnt == xnext) right |= ll;
         xcnt++;
		}
      flag |= rflag;
      info[ycnt] |= (BYTE)rflag;
LtoRfin:
		BegBitRowR += Pitch1;	BegBitRowW += Pitch2;
	}
   xfin = xnext;

TopToBottom:
   if ((flag &= 0xfd) == 0) goto Fin;
   if ((flag & 0xf5) == 0) goto RightToLeft;
	BegBitRowR = BitMem1 + (Pitch1 * ystrt);
   BegBitRowW = BitMem2 + (Pitch2 * ystrt);
   if ((yfin < Ye - 1) && (info[yfin + 1] & 0xf5)) yfin++;
   ynext = ywrk = yfin;  ycnt = ystrt;
   prevrow = (DWORD *)BegBitRowW;
   while ((ycnt++) <= ywrk)
	{
      info[ycnt] &= 0xfd;
      if ((info[ycnt - 1] & 0xf7) == 0) goto TtoBfin;
		BitRowRR = (DWORD *)BegBitRowR;
		BitRowR = (DWORD *)BegBitRowW;
      xcnt = xstrt;  rflag = chng = 0;
		while (xcnt <= xfin)
      {
         tmask = BitRowRR[xcnt];
         if (tmask)
         {
            if (xcnt == dwcnt1) tmask &= rmask;
           	prv =  BitRowR[xcnt];
           	cur =  prv & tmask;
				ll = ((prevrow[xcnt] | cur) & tmask);
				if (ll != cur) { rflag |= 2; }
            chng |= ll;
				if (xcnt == dwcnt1) ll |= (prv & smask);
            BitRowR[xcnt] = ll;
         }
         xcnt++;
      }
      if (rflag)
      {
         flag |= rflag;
         info[ycnt] |= (BYTE)rflag;
      }
      if (chng)
      {
         if ((ycnt > ywrk) && (ywrk < Ye - 1))
			{
            ywrk++;  if (ynext < ywrk) ynext = ywrk;
         }
      }
TtoBfin:
      prevrow = (DWORD *)BegBitRowW;
		BegBitRowR += Pitch1;	BegBitRowW += Pitch2;
	}
   yfin = ynext;

RightToLeft:
   if ((flag &= 0xfb) == 0) goto Fin;
   if ((flag & 0xfa) == 0) goto BottomToTop;
   ycnt = ystrt;    xnext = xstrt;
	BegBitRowR = BitMem1 + (Pitch1 * ystrt);
   BegBitRowW = BitMem2 + (Pitch2 * ystrt);
	while (ycnt++ <= yfin)
	{
      rflag = 0;   info[ycnt] &= 0xfb;
      if ((info[ycnt] & 0xaa) == 0) goto RtoLfin;
		BitRowR = (DWORD *)BegBitRowW;
		BitRowRR = (DWORD *)BegBitRowR;
      xcnt = xfin;  xwrk = xstrt;  prv = 0; // 0/0x80000000 here
		while (xcnt >= xwrk)
		{
			tmask = BitRowRR[xcnt];
         if (xcnt == dwordcnt - 1) tmask &= rmask;
         if (tmask)
         {
            cur = BitRowR[xcnt] & tmask;
				while ((ll = (cur | (cur >> 1) | prv) & tmask) != cur)
            {
              	cur = ll;  rflag |= 4;
            }
            if ((prv = (ll << 31)) != 0)
            {
           		if ((xcnt == xwrk) && (xwrk > 0))
					{
  		         	xwrk--;
                  if (xnext > xwrk)
                  {
                     xnext = xwrk;  left = 0;
                  }
					}
            }
         } else
            prv = ll = 0;
			if (xcnt == dwordcnt - 1) ll |= (BitRowR[xcnt] & smask);
         BitRowR[xcnt] = ll;
         if (xcnt == xnext) left |= ll;
         if (xcnt == 0) break;
         xcnt--;
		}
      flag |= rflag;
      info[ycnt] |= (BYTE)rflag;
RtoLfin:
		BegBitRowR += Pitch1;	BegBitRowW += Pitch2;
	}
   xstrt = xnext;

BottomToTop:
   if ((flag &= 0xf7) == 0) goto Fin;
   if ((flag & 0xf5) == 0) goto LastCheck;
   if ((ystrt > 0) && (info[ystrt + 1] & 0xf5)) ystrt--;
   ynext = ywrk = ystrt;   ycnt = yfin;
	BegBitRowR = BitMem1 + (Pitch1 * yfin);
   BegBitRowW = BitMem2 + (Pitch2 * yfin);
   prevrow = (DWORD *)BegBitRowW;
	while (ycnt >= ywrk)
	{
      info[ycnt + 1] &= 0xf7;
      if ((info[ycnt + 2] & 0xfd) == 0) goto BtoTfin;
		BitRowRR = (DWORD *)BegBitRowR;
		BitRowR = (DWORD *)BegBitRowW;
      xcnt = xstrt;  rflag = chng = 0;
     	while (xcnt <= xfin)
      {
         tmask = BitRowRR[xcnt];
         if (tmask)
         {
            if (xcnt == dwcnt1) tmask &= rmask;
          	prv =  BitRowR[xcnt];
           	cur =  prv & tmask;
				ll = ((prevrow[xcnt] | cur) & tmask);
				if (ll != cur) { rflag |= 8; }
            chng |= ll;
		    	if (xcnt == dwcnt1) ll |= (prv & smask);
            BitRowR[xcnt] = ll;
         }
         xcnt++;
      }
      if (rflag)
      {
         flag |= rflag;  info[ycnt + 1] |= 8;
      }
      if (chng)
      {
         if ((ycnt == ywrk) && (ywrk > 0))
         {
             ywrk--;  if (ynext > ywrk) ynext = ywrk;
         }
      }
BtoTfin:
      prevrow = (DWORD *)BegBitRowW;
		BegBitRowR -= Pitch1;	BegBitRowW -= Pitch2;
      if (ycnt) ycnt--; else break;
	}
   ystrt = ynext;

LastCheck:
	if (flag & 0xf0)
   {
       for (i = 1;  i <= Ye;  i++) info[i] &= 0x0f;
   }
	if (flag &= 0x0f) goto LeftToRight;
Fin:
   ll = (xstrt + 1) << 5;
   while (left) { ll--;  left <<= 1; }
   *xmin = ll;     ll = xfin << 5;
   while (right >>= 1) ll++;
   *xmax = ll;
   *ymin = ystrt;  *ymax = yfin;
   if (Ye + 1 >= Work4KSize) free(info);
}


//====================================================== MaskRotation
// Move Mask Rectangle with Rotation around point {xc1,yc1};
// after rotation it goes to {xc2,yc2}
// Mask <m3> controls moving; it corresponds to <m1> (source mask)
// NULL is ok for <m3> (no mask)
void _stdcall MaskRotation (
			void *m1, long xe1, long ye1, long pitch1,
			void *m2, long xe2, long ye2, long pitch2,
			void *m3, long pitch3,
         long xc1, long yc1, long xc2, long yc2, double angle)
{
	double tanhalf, sine, dwrk;
   long *dt, *ds;
   long xs1, ys1, i, k, dx, dy, ix, iy, xmin, xmax, ymin, ymax;
   BYTE c1, c2, *r, *w, *msk;
   BOOL rotpi = false;

   if (cos(angle) < 0)
   {
   	angle += (atan((double)1) * 4); // + pi
      rotpi = true;
   }
   tanhalf = tan(angle / 2);  sine = -sin(angle);
   dt = (long *)Work4K;   ds = dt + 2 * (ye1 + 1);
   xs1 = xe1 / 2;   ys1 = ye1 / 2;
   dwrk = tanhalf * (ys1 + yc1) + 0.5;
   for (i = 0;  i < 2 * ye1;  i++)
   {  dt[i] = floor(dwrk);  dwrk -= tanhalf; }
   dwrk = sine * (xs1 + xc1) + 0.5;
   for (i = 0;  i < 2 * xe1;  i++)
   {  ds[i] = floor(dwrk);  dwrk -= sine; }
   dx = xc2 - xc1;  dy = yc2 - yc1;
   r = (BYTE *)m1;
   if (m3) msk = (BYTE *)m3; else msk = r;
   xmin = -xs1;  xmax = 2 * xe1 - xs1 - 1;
   ymin = -ys1;  ymax = 2 * ye1 - ys1 - 1;
   for (k = 0;  k < ye1;  k++)
   {
    	for (i = 0;  i < xe1;  i++)
      {
         if (m3) if (!((msk[i >> 3] >> (i & 7)) & 1)) continue;
       	ix = i + dt[k + ys1];	// horizontal shear
         if ((ix < xmin) || (ix > xmax)) continue;
       	iy = k + ds[ix + xs1]; 	// vertical shear
         if ((iy < ymin) || (iy > ymax)) continue;
         ix += dt[iy + ys1];  	// 2-nd horizontal shear
         ix += dx;   iy += dy;
	      if (rotpi)
         {  // rotation 2*pi
         	ix = 2 * xc2 - ix;   iy = 2 * yc2 - iy;
         }
         if ((ix >= 0) && (ix < xe2) && (iy >= 0) && (iy < ye2))
         {
            c1 = (BYTE)(((r[i >> 3] >> (i & 7)) & 1) << (ix & 7));
            w = (BYTE *)m2 + (iy * pitch2);
            c2 = w[ix >> 3];
            c2 ^= (BYTE)((1 << (ix & 7)) & c2) ^ c1;
            w[ix >> 3] = c2;
         }
      }
      r += pitch1;   msk += pitch3;
   }
}


/**************************************************** MaskToNeiContrast */
/***
	Function:	MaskToNeiContrast
	Description:
		This function fills <mem8> with 'pixels' depending on oper:
        	oper=1 -  	nei
            oper=2 -  	contrast
              			(nei<<4) + contrast    otherwise
        where
        	0 <= nei <= 8 - number of (N8) neighbors
            0 <= contrast <= 4 - number of changes 0->1 (same as 1->0)
	Statics/Globals:
		BitsInByte, MaskToContrastTable
	Input Arguments:
		mem1 - pointer to source bit mask
		mem8 - pointer to target 8 bit/pix MemRect
		xe - number of pixels in each row (for both)
		ye - number of rows
		pitch1, pitch2 - address difference between neighboring
        		rows (in bytes)	in the source and target MemRects
    Returns: nothing

***/
void _stdcall  MaskToNeiContrast (
				BYTE *mem1, DWORD xe, DWORD ye, DWORD pitch1,
               	BYTE *mem8, DWORD pitch2, long oper)
{
	BYTE *upline, *curline, *downline, *wr;
    WORD *up, *cur, *down;
	DWORD k, shft, i = 0;
	DWORD uppix, curpix, downpix, wrk, msk, lastbit, ind;

    upline = curline = downline = mem1;
    wr = mem8;
    if ((wrk = xe & 7) != 0)
    {
     	msk = 0x1ff >> (8 - wrk);
        lastbit = 1 << wrk;
    }
    while (i++ < ye)
    {
    	if (i < ye) downline += pitch1;
        uppix = (DWORD)upline[0];
        curpix = (DWORD)curline[0];
        downpix = (DWORD)downline[0];
        k = 0;
        while (k < xe)
        {
            if ((k & 7) == 0)
            {   // read next word
            	uppix &= 1;  curpix &= 1;  downpix &= 1;
            	shft = k >> 3;
                if (xe - k > 8)
                {
	        		up = (WORD *)(upline + shft);
    	            cur = (WORD *)(curline + shft);
        	        down = (WORD *)(downline + shft);
			        uppix |= ((DWORD)up[0] << 1);
			        curpix |= ((DWORD)cur[0] << 1);
			        downpix |= ((DWORD)down[0] << 1);
            	} else
                {
			        uppix |= ((DWORD)upline[shft] << 1);
                    uppix &= msk;
                    uppix |= ((uppix & lastbit) << 1);
			        curpix |= ((DWORD)curline[shft] << 1);
                    curpix &= msk;
                    curpix |= ((curpix & lastbit) << 1);
			        downpix |= ((DWORD)downline[shft] << 1);
                    downpix &= msk;
                    downpix |= ((downpix & lastbit) << 1);
                }
            }
            ind = ((uppix & 7) << 5)
               	| ((curpix & 1) << 3) | ((curpix & 4) << 2)
               	| (downpix & 7);
            switch (oper)
            {
               	case 1: // nei
            		wr[k] = BitsInByte[ind];
                    break;
               	case 2: // contrast
            		wr[k] = MaskToContrastTable[ind];
                    break;
              	default: // combine both
            		wr[k] = (BitsInByte[ind] << 4) | MaskToContrastTable[ind];
            }
            k++;  uppix >>= 1;  curpix >>= 1;  downpix >>= 1;
        }
        upline = curline;  curline = downline;
        wr += pitch2;
    }
}


/***************************************************** MemConstMem */
/***
	Function:	MemConstMem

	Description:
		This function implements bytewise operations listed below of type
				mem1 <oper> const => mem2
		All operations are the same as for function  MemOpMem(),
		but 2-nd argument of the operations is the given constant

	Statics/Globals:
		None

	Input Arguments:
		buff1 - pointer to 1-st argument memory buffer ("A")

		buff2 - pointer to the result memory buffer ("B")

		size - size of each buffer (bytes)

		cnst - 2-nd argument of operation ("C"), accepted values 0 to 255

		oper - 	A <oper> C => B
			operations 0 to 15 are bitwise, values of symbolic
			constants correspond to truth table;
			operations 16+ are bytewise;

			momZERO 	= 0   set all bits to zero
			momAND 	= 1   A & C
			momMORE 	= 2   A & ~C  (1 if A > C)
			momCOPY1 = 3   A  (copy A into B)
			momLESS 	= 4   ~A & C  (1 if A < C)
			momCOPY2 = 5   C  (copy const into B)
			momEXOR 	= 6   A ^ C  (mod 2 {exclusive OR})
			momOR 	= 7   A | C
			momNOR 	= 8   ~(A | C)  (not OR)
			momEQUAL = 9   ~(A ^ C)  (1 if A = C)
			momNOT2 	=10   copy ~C into B
			momEQMORE=11   A | ~C  (1 if A >= C)
			momNOT1 	=12   copy ~A into B
			momEQLESS=13   ~A | C  (1 if A <= C)
			momNAND 	=14   ~(A & C)  (not AND)
			momFULL 	=15   set all bits to 1 (bytes to 255)

			momMIN 	=16   A MIN C
			momMAX 	=17   A MAX C
			momPLUS 	=18   (A + C) mod 256
			momSPLUS =19   (A + C) min 255 (saturation)
			momMINUS =20   (A - C) mod 256
			momIMINUS=21   (C - A) mod 256 (inverse order)
			momSMINUS=22   (A - C) max 0   (saturation)
			momSIMINUS 	=23   (C - A) max 0   (saturation and inverse order)
			momLOWto0 	=24   if A >= C then A else 0
			momHIGHtoFF =25   if A <= C then A else 255
			mom0orFF 	=26   if A < C then 0 else 255
			momAbsDiff  =27   if A < B then (B - A) else (A - B)
			momAbsDiff8 =28   Abs(A - B) MIN (256 - Abs(A - B))


	Returns
		for operations  0 to 15 - always 0
		for operations 16 to 28 - number of changed or 'saturated' bytes

***/
DWORD _stdcall MemConstMem (
					BYTE *mem1, BYTE *mem2, long psize,
					WORD cnst, short oper)
{
	DWORD *m1, *m2, ones, sol, lmask, rmask,
    		cnt, rem, i, l1, l2, size;
	WORD b1, b2, opermax = momAbsDiff8;
    short ret = -1;

    size = psize;
	if (oper > 15) goto ByteWise;
// BitWise:
	if (oper < 0)  goto Fin;
	ones = 0xffffffffL;   sol = sizeof(DWORD);
   	cnt = size / sol;   rem = size - (cnt * sol);
   	if (rem)
   	{
   	 	rmask = ones << (rem << 3);
   	 	lmask = ones ^ rmask;
   	}
	l1 = cnst & 0xff;  l1 |= (l1 << 8);
   	l1 |= (l1 << 16);  // repeate bytes
   	l2 = l1 ^ ones;
	m1 = (DWORD *)mem1;   m2 = (DWORD *)mem2;
   	switch (oper)
	{
     case momZERO:  // 0
       memset(mem2, 0, size);
       break;     // zero
     case momAND:  // 1
	   for (i = 0;  i < cnt;  i++) m2[i] = m1[i] & l1;
       if (rem) m2[cnt] = (m2[cnt] & rmask) | ((m1[cnt] & l1) & lmask);
       break;     // 1st and  2nd
	 case momMORE: // 2
	   for (i = 0;  i < cnt;  i++) m2[i] = m1[i] & l2;
       if (rem) m2[cnt] = (m2[cnt] & rmask) | ((m1[cnt] & l2) & lmask);
       break;  // A & ~C  (1 if A > C)
	 case momCOPY1: // 3
       memmove(mem2, mem1, size);
       break;     // A  (copy A into B)
	 case momLESS: // 4
	   for (i = 0;  i < cnt;  i++) m2[i] = (m1[i] ^ ones) & l1;
       if (rem) m2[cnt] = (m2[cnt] & rmask) |
       						(((m1[cnt] ^ ones) & l1) & lmask);
       break;  // ~A & C  (1 if A < C)
     case momCOPY2:  // 5
       memset(mem2, (int)cnst, size);
       break;     // C  (copy const into B)
	 case momEXOR: // 6
	   for (i = 0;  i < cnt;  i++) m2[i] = m1[i] ^ l1;
       if (rem) m2[cnt] = (m2[cnt] & rmask) | ((m1[cnt] ^ l1) & lmask);
       break;     // 1st exor 2nd
     case momOR:  // 7
	   for (i = 0;  i < cnt;  i++) m2[i] = m1[i] | l1;
       if (rem) m2[cnt] = (m2[cnt] & rmask) | ((m1[cnt] | l1) & lmask);
       break;     // 1st or  2nd
     case momNOR:  // 8
	   for (i = 0;  i < cnt;  i++) m2[i] = (m1[i] | l1) ^ ones;
       if (rem) m2[cnt] = (m2[cnt] & rmask) |
       						(((m1[cnt] | l1) ^ ones) & lmask);
       break;     // ~(A | C)  (not OR)
     case momEQUAL:  // 9
	   for (i = 0;  i < cnt;  i++) m2[i] = (m1[i] ^ l1) ^ ones;
       if (rem) m2[cnt] = (m2[cnt] & rmask) |
       						(((m1[cnt] ^ l1) ^ ones) & lmask);
       break;     // ~(A ^ C)  (1 if A = B)
     case momNOT2:  // 10
       memset(mem2, (int)l2, size);
       break;     // copy ~C into B
	 case momEQMORE: // 11
	   for (i = 0;  i < cnt;  i++) m2[i] = m1[i] | l2;
       if (rem) m2[cnt] = (m2[cnt] & rmask) | ((m1[cnt] | l2) & lmask);
       break;  //  A | ~C  (1 if A >= C)
	 case momNOT1: // 12
       for (i = 0;  i < cnt;  i++) m2[i] = (m1[i] ^ ones);
       if (rem) m2[cnt] = (m2[cnt] & rmask) | ((m1[cnt] ^ ones) & lmask);
       break;     // copy ~A into B
	 case momEQLESS: // 13
	   for (i = 0;  i < cnt;  i++) m2[i] = (m1[i] ^ ones) | l1;
       if (rem) m2[cnt] = (m2[cnt] & rmask) |
       						(((m1[cnt] ^ ones) | l1) & lmask);
       break;  //  ~A | C  (1 if A <= C)
     case momNAND:  // 14
	   for (i = 0;  i < cnt;  i++) m2[i] = (m1[i] & l1) ^ ones;
       if (rem) m2[cnt] = (m2[cnt] & rmask) |
       						(((m1[cnt] & l1) ^ ones) & lmask);
       break;     // ~(A & C)  (not AND)
     case momFULL:  // 15
       memset(mem2, 255, size);
       break;     // set all bits to 1 (bytes to 255)
	 default:  ;   // 5: case momCOPY2 - 2nd, nothing to do
	}
	goto Ok;

ByteWise:
	if (oper > opermax)  goto Fin;
	b2 = (WORD)(cnst & 0xff);
    switch (oper)
	{
     case momMIN:  // 16
	   for (i = 0;  i < size;  i++)
       if ((b1 = mem1[i]) < b2) mem2[i] = b1;
       else mem2[i] = b2;
       break;     // A min C
     case momMAX:  // 17
	   for (i = 0;  i < size;  i++)
       if ((b1 = mem1[i]) > b2) mem2[i] = b1;
       else mem2[i] = b2;
       break;     // 1st max 2nd
     case momPLUS:  // 18
	   for (i = 0;  i < size;  i++) mem2[i] = (BYTE)((WORD)mem1[i] + b2);
       break;     // (1st + 2nd) mod 256
     case momSPLUS:  // 19
	   for (i = 0;  i < size;  i++)
       {
       	  b1 =(WORD)((WORD)mem1[i] + b2);
       	  if (b1 & 0x100) mem2[i] = 255; else mem2[i] = b1;
       }
       break;     // (1st + 2nd) min 255 (saturation)
     case momMINUS:  // 20
	   for (i = 0;  i < size;  i++)
       mem2[i] = (BYTE)(mem1[i] - b2);
       break;     // (1st - 2nd) mod 256
     case momIMINUS:  // 21
		 for (i = 0;  i < size;  i++) mem2[i] = (BYTE)(b2 - mem1[i]);
       break;     // (2nd - 1st) mod 256
     case momSMINUS:  // 22
	   for (i = 0;  i < size;  i++)
       {
       	  if ((b1 = (WORD)mem1[i]) <= b2) mem2[i] = 0;
          else mem2[i] = (BYTE)(b1 - b2);
       }
       break;     // (1st - 2nd) max 0
     case momSIMINUS:  // 23
	   for (i = 0;  i < size;  i++)
       {
       	  if ((b1 = (WORD)mem1[i]) >= b2) mem2[i] = 0;
          else mem2[i] = (BYTE)(b2 - b1);
       }
       break;     // (2nd - 1st) max 0
     case momLOWto0:  // 24
	   for (i = 0;  i < size;  i++)
       {
          if ((b1 = (WORD)mem1[i]) < b2) mem2[i] = 0;
          else mem2[i] = b1;
       }
       break;     // set bytes below mem2 to 0
     case momHIGHtoFF:  // 25
	   for (i = 0;  i < size;  i++)
       {
       	  if ((b1 = (WORD)mem1[i]) > b2) mem2[i] = 255;
          else mem2[i] = b1;
       }
       break;     // set bytes above mem2 to 255
     case mom0orFF:  // 26
	   for (i = 0;  i < size;  i++)
       {
       	  if (mem1[i] < b2) mem2[i] = 0;
          else mem2[i] = (BYTE)0xFF;
       }
       break;     // set bytes below mem2 to 0,
      			  // above or equal mem2 to 255
	 case momAbsDiff:  // 27
	   for (i = 0;  i < size;  i++)
       {
       	  		if ((b1 = (WORD)mem1[i]) < b2) mem2[i] = (BYTE)(b2 - b1);
           		else mem2[i] = (BYTE)(b1 - b2);
       }
       break;     // Abs(b1-b2)
     case momAbsDiff8:  // 28
	   for (i = 0;  i < size;  i++)
       {
       	  		if ((b1 = (WORD)mem1[i]) < b2) b1 = b2 - b1;
           		else b1 -= b2;
                if (b1 > 128) b1 = 256 - b1;
                mem2[i] = (BYTE)b1;
       }
       break;     // Abs(b1-b2) MIN (256 - Abs(b1-b2))
 	 default:  ;  // nothing to do
   }
Ok:
	ret = 0;
Fin:
	return ret;
}


/************************************************* MemMaskConstMem */
/***
	Function:	MemMaskConstMem

	Description:
		This function implements listed below bytewise operations of type
				mem1 <oper if mask> const => mem2.

	Statics/Globals:
		None

	Input Arguments:
		pmem1 - pointer to the 1-st argument memory rectangle ("A")

		pmem2 - pointer to the target memory rectangle ("B")

		pmask - pointer to the argument bit mask

		xe - number of pixels in each row

		ye - number of rows

		pitch1 - address difference between neighboring rows (in bytes)
				in 1-st argument memory rectangle ("A")
		pitch2 - address difference between neighboring rows (in bytes)
				in target memory rectangle ("B")
		pitchmask - address difference between neighboring rows (in bytes)
				in the bit mask
		cnst - 2-nd argument of operation ("C"), accepted values 0 to 255

		oper - 	A <oper if mask> C => B
				operation symbolic constants are the same as for MemOpMem(),
				plus one more operation, and 2 modifiers may be used;

			momZERO 	= 0   set all bits to zero
			momAND 	= 1   A & C
			momMORE 	= 2   A & ~C  (1 if A > C)
			momCOPY1 = 3   A  (copy A into B)
			momLESS 	= 4   ~A & C  (1 if A < C)
			momCOPY2 = 5   C  (copy const into B)
			momEXOR 	= 6   A ^ C  (mod 2 {exclusive OR})
			momOR 	= 7   A | C
			momNOR 	= 8   ~(A | C)  (not OR)
			momEQUAL = 9   ~(A ^ C)  (1 if A = C)
			momNOT2 	=10   copy ~C into B
			momEQMORE=11   A | ~C  (1 if A >= C)
			momNOT1 	=12   copy ~A into B
			momEQLESS=13   ~A | C  (1 if A <= C)
			momNAND 	=14   ~(A & C)  (not AND)
			momFULL 	=15   set all bits to 1 (bytes to 255)

			momMIN 	=16   A MIN C
			momMAX 	=17   A MAX C
			momPLUS 	=18   (A + C) mod 256
			momSPLUS 	=19   (A + C) min 255 (saturation)
			momMINUS 	=20   (A - C) mod 256
			momIMINUS 	=21   (C - A) mod 256 (inverse order)
			momSMINUS 	=22   (A - C) max 0   (saturation)
			momSIMINUS 	=23   (C - A) max 0   (saturation and inverse order)
			momLOWto0 	=24   if A >= C then A else 0
			momHIGHtoFF =25   if A <= C then A else 255
			mom0orFF 	=26   if A < C then 0 else 255
			momAbsDiff  =27   if A < B then (B - A) else (A - B)
			momAbsDiff8 =28   Abs(A - B) MIN (256 - Abs(A - B))

			mmcmMERGE   =47   if (mask) then B=A  else B=C

					one of the following modifiers can be ORed with the
					operation code:
			mmcmNEG    =0x80   (oper | mmcmNEG) means "use negative mask"
			mmcmIGNORE =0x40   (oper | mmcmIGNORE) means "ignore mask";
								last modifier is useful because MemMaskMem()
								and MemMaskConstMem() work on rectangles, while
								MemOpMem() and MemConstMem() work on buffers only

	Returns:
		0 - if all involved bytes are 0;
		1 - if all involved bytes are 0xff;
        2 - otherwise

***/
DWORD _stdcall MemMaskConstMem (
			BYTE *mem1, BYTE *mem2, BYTE *mask,
			DWORD xe, DWORD ye,
            DWORD pitch1, DWORD pitch2, DWORD pitchmask,
            WORD cnst, short oper)
{
	WORD ignore, b1, b2, op, neg;
	BYTE *m1, *m2, *bm1, *bm2, *bmm;
	DWORD p1, p2, pm, *mmdw;
	DWORD xcnt, ycnt, bmask, negoper, ones,
    	retand, retor, ret = 2;

	b2 = (WORD)(cnst & 0xff);
	bm1 = mem1;  p1 = pitch1;
	bm2 = mem2;  p2 = pitch2;
	bmm = mask;  pm = pitchmask;
	ones = 0xffffffff;  negoper = ignore = 0;
	if (oper & mmcmNEG) negoper = ones;
	if (oper & mmcmIGNORE) ignore = 1;
	oper &= (mmcmIGNORE - 1);
    ycnt = 0;  retand = 0xff;  retor = 0;
	if (oper > 15) goto Sixteen;

// Bool0to15:
	if (oper < 8) {  op = oper;   neg = 0; }
	else {  op = (WORD)(15 - oper);  neg = 0xff; }

	while (ycnt++ < ye)
	{
		m1 = bm1;  m2 = bm2;  mmdw = (DWORD *)bmm;
        xcnt = 0;  bmask = 0;
		while (xcnt < xe)
		{
            if (!bmask)
            {
                if (ignore) bmask = ones;
                else bmask = (*mmdw++) ^ negoper;
			}
            if (bmask & 1)
			{
				b1 = m1[xcnt];
				switch (op)
				{
					case momAND:   b1 &= b2;   break;     // A and  C
					case momOR:    b1 |= b2;   break;     // A or   C
					case momEXOR:  b1 ^= b2;   break;     // A exor C
					case momMORE:  b1 = (b1 ^ b2) & b1;  break;  // A and ~C
					case momLESS:  b1 = (b1 ^ b2) & b2;  break;  // ~A and C
					case momCOPY2: b1 = b2;    break;     // C
					case momZERO:  b1 = 0;     break;     // zero
					default:  ;   // case momCOPY1 - A, nothing to do
				}
				m2[xcnt] = (b1 ^= neg);	  // possible negation
                retand &= b1;  retor |= b1;
			}
            if (ignore) xcnt++;
            else
            {
				if (bmask >>= 1) xcnt++;
            	else xcnt = (xcnt & 0xffffffe0) + 32;
            }
		}
		bm1 += p1;  bm2 += p2;
		if (!ignore) bmm += pm;
	}
    goto Fin;

Sixteen:
	while (ycnt++ < ye)
	{
		m1 = bm1;  m2 = bm2;  mmdw = (DWORD *)bmm;
        xcnt = 0;  bmask = 0;
		while (xcnt < xe)
		{
            if (!bmask)
            {
                if (ignore) bmask = ones;
                else bmask = (*mmdw++) ^ negoper;
			}
            if (bmask & 1)
			{
				b1 = m1[xcnt];
				switch (oper)
				{
					case momMIN:      if (b1 > b2) b1 = b2;
									  break;  // A min C
					case momMAX:   	  if (b1 < b2) b1 = b2;
									  break;  // A max C
					case momPLUS:     b1 += b2;
									  break;  // (A + C) mod 256
					case momSPLUS:    if ((b1 += b2) > 255) b1 = 255;
									  break;  // (A + C) min 255 (saturation)
					case momMINUS:    b1 -= b2;
									  break;  // (A - C) mod 256
					case momIMINUS:   b1 = (WORD)(b2 - b1);
									  break;  // (C - A) mod 256
					case momSMINUS:   if (b1 >= b2) b1 -= b2; else b1 = 0;
									  break;  // (A - C) max 0
					case momSIMINUS:  if (b1 <= b2) b1 = (WORD)(b2 - b1);
                    				  else b1 = 0;
									  break;  // (C - A) max 0
					case momLOWto0:   if (b1 < b2) b1 = 0;
									  break;  // if A<C then 0 else A
					case momHIGHtoFF: if (b1 > b2) b1 = 255;
									  break;  // if A>C then 255 else A
					case mom0orFF:    if (b1 < b2) b1 = 0; else b1 = 255;
									  break;  // if A<C then 0 else 255
			     	case momAbsDiff:  if (b1 < b2) b1 = b2 - b1; else b1 -= b2;
						       		  break;  // Abs(b1-b2)
			     	case momAbsDiff8: if (b1 < b2) b1 = b2 - b1; else b1 -= b2;
					                  if (b1 > 128) b1 = 256 - b1;
						       	      break; // Abs(b1-b2) MIN (256 - Abs(b1-b2))

					case mmcmMERGE:   break;  // A if mask, see below
					default:  ;
				}
				m2[xcnt] = b1;
                retand &= b1;  retor |= b1;
			} else
			{
				if (oper == mmcmMERGE)
				{
					m2[xcnt] = b2;  // C if not mask
				}
			}
            if (ignore) xcnt++;
            else
            {
				if (bmask >>= 1) xcnt++;
            	else xcnt = (xcnt & 0xffffffe0) + 32;
            }
		}
		bm1 += p1;  bm2 += p2;
		if (!ignore) bmm += pm;
	}
Fin:
    if (retor) { if (retand == 0xff) ret = 1; }
    else ret = 0;
	return ret;
}


/************************************************* MemMaskConstMem32 */
/***
	Function:	MemMaskConstMem32

	Description:
		This function implements listed below DWORDwise operations of type
				mem1 <oper if mask> const => mem2.

	Statics/Globals:
		None

	Input Arguments:
		pmem1 - pointer to the 1-st argument memory rectangle ("A")

		pmem2 - pointer to the target memory rectangle ("B")

		pmask - pointer to the argument bit mask

		xe - number of pixels in each row

		ye - number of rows

		pitch1 - address difference between neighboring rows (in bytes)
				in 1-st argument memory rectangle ("A")
		pitch2 - address difference between neighboring rows (in bytes)
				in target memory rectangle ("B")
		pitchmask - address difference between neighboring rows (in bytes)
				in the bit mask
		cnst - 2-nd argument of operation ("C"), accepted values 0 to 0xffffffff

		oper - 	A <oper if mask> C => B
				operation symbolic constants are the same as for MemOpMem(),
				plus one more operation, and 2 modifiers may be used;

			momZERO 	= 0   set all bits to zero
			momAND 	= 1   A & C
			momMORE 	= 2   A & ~C  (1 if A > C)
			momCOPY1 = 3   A  (copy A into B)
			momLESS 	= 4   ~A & C  (1 if A < C)
			momCOPY2 = 5   C  (copy const into B)
			momEXOR 	= 6   A ^ C  (mod 2 {exclusive OR})
			momOR 	= 7   A | C
			momNOR 	= 8   ~(A | C)  (not OR)
			momEQUAL = 9   ~(A ^ C)  (1 if A = C)
			momNOT2 	=10   copy ~C into B
			momEQMORE=11   A | ~C  (1 if A >= C)
			momNOT1 	=12   copy ~A into B
			momEQLESS=13   ~A | C  (1 if A <= C)
			momNAND 	=14   ~(A & C)  (not AND)
			momFULL 	=15   set all bits to 1 (pixels to 0xffffffff)

			momMIN 	=16   A MIN C
			momMAX 	=17   A MAX C
			momPLUS 	=18   (A + C) mod 2^^32
			momSPLUS 	=19   (A + C) min 0xffffffff (saturation)
			momMINUS 	=20   (A - C) mod 2^^32
			momIMINUS 	=21   (C - A) mod 2^^32 (inverse order)
			momSMINUS 	=22   (A - C) max 0   (saturation)
			momSIMINUS 	=23   (C - A) max 0   (saturation and inverse order)
			momLOWto0 	=24   if A >= C then A else 0
			momHIGHtoFF =25   if A <= C then A else 0xffffffff
			mom0orFF 	=26   if A < C then 0 else 0xffffffff
			momAbsDiff 	=27   if A < C then (C - A) else (A - C)
			momAbsDiff8 =28   Abs((A=&0xff) - C) MIN (256 - Abs(A - C))

			mmcmMERGE   =47   if (mask) then B=A  else B=C

					one of the following modifiers can be ORed with the
					operation code:
			mmcmNEG    =0x80   (oper | mmcmNEG) means "use negative mask"
			mmcmIGNORE =0x40   (oper | mmcmIGNORE) means "ignore mask";
								last modifier is useful because MemMaskMem()
								and MemMaskConstMem32() work on rectangles, while
								MemOpMem() and MemConstMem() work on buffers only

	Returns:
		0 - if all involved pixels are 0;
		1 - if all involved pixels are 0xffffffff;
        2 - otherwise

***/
DWORD _stdcall MemMaskConstMem32 (
			void *mem1, void *mem2, void *mask,
			DWORD xe, DWORD ye,
            DWORD pitch1, DWORD pitch2, DWORD pitchmask,
            DWORD cnst, DWORD oper)
{
	DWORD ignore, b1, b2, op, neg, p1, p2, pm;
	DWORD *m1, *m2, *mmdw;
    BYTE *bm1, *bm2, *bmm;
	DWORD xcnt, ycnt, bmask, negoper, ones, xnext,
    	retand, retor, ret = 2;

	b2 = cnst;
    bm1 = (BYTE *)mem1;  p1 = pitch1;
    bm2 = (BYTE *)mem2;  p2 = pitch2;
	bmm = (BYTE *)mask;  pm = pitchmask;
	ones = 0xffffffff;  negoper = ignore = 0;
	if (oper & mmcmNEG) { negoper = ones;  oper ^= mmcmNEG; }
	if (oper & mmcmIGNORE) { ignore = 1;  oper ^= mmcmIGNORE; }
    ycnt = 0;  retand = ones;  retor = 0;
	if (oper > 15) goto Sixteen;

// Bool0to15:
	if (oper < 8) {  op = oper;   neg = 0; }
	else {  op = 15 - oper;  neg = ones; }

	while (ycnt++ < ye)
	{
		m1 = (DWORD *)bm1;  m2 = (DWORD *)bm2;
        mmdw = (DWORD *)bmm;  xcnt = bmask = 0;
		while (xcnt < xe)
		{
            if (!bmask)
            {
                if (ignore) bmask = ones;
                else bmask = (*mmdw++) ^ negoper;
                xnext = xcnt + 32;
                if (!bmask) { xcnt = xnext;  continue; }
			}
            if (bmask & 1)
			{
				b1 = m1[xcnt];
				switch (op)
				{
					case momZERO:  b1 = 0;     break;     // zero (0)
					case momAND:   b1 &= b2;   break;     // A and  C (1)
					case momMORE:  b1 = (b1 ^ b2) & b1;  break;  // A and ~C (2)
					case momLESS:  b1 = (b1 ^ b2) & b2;  break;  // ~A and C (4)
					case momCOPY2: b1 = b2;    break;     // C (5)
					case momEXOR:  b1 ^= b2;   break;     // A exor C (6)
					case momOR:    b1 |= b2;   break;     // A or C (7)
					default:  ;   // case momCOPY1 - A, nothing to do
				}
				m2[xcnt] = (b1 ^= neg);	  // possible negation
                retand &= b1;  retor |= b1;
			}
            if (bmask >>= 1) xcnt++;
            else xcnt = xnext;
		}
		bm1 += p1;  bm2 += p2;
		if (!ignore) bmm += pm;
	}
    goto Fin;

Sixteen:
	if (oper == mmcmMERGE) negoper ^= ones;  // C if not mask
	while (ycnt++ < ye)
	{
		m1 = (DWORD *)bm1;  m2 = (DWORD *)bm2;
        mmdw = (DWORD *)bmm;  xcnt = bmask = 0;
		while (xcnt < xe)
		{
            if (!bmask)
            {
                if (ignore) bmask = ones;
                else bmask = (*mmdw++) ^ negoper;
                xnext = xcnt + 32;
                if (!bmask) { xcnt = xnext;  continue; }
			}
            if (bmask & 1)
			{
				b1 = m1[xcnt];
				switch (oper)
				{
					case momMIN:      if (b1 > b2) b1 = b2;
									  break;  // A min C
					case momMAX:   	  if (b1 < b2) b1 = b2;
									  break;  // A max C
					case momPLUS:     b1 += b2;
									  break;  // (A + C) mod 2^^32
					case momSPLUS:    if ((b1 += b2) < b2) b1 = ones;
									  break;  // (A + C) min 0xffffffff (saturation)
					case momMINUS:    b1 -= b2;
									  break;  // (A - C) mod 2^^32
					case momIMINUS:   b1 = (WORD)(b2 - b1);
									  break;  // (C - A) mod 2^^32
					case momSMINUS:   if (b1 >= b2) b1 -= b2; else b1 = 0;
									  break;  // (A - C) max 0
					case momSIMINUS:  if (b1 <= b2) b1 = b2 - b1; else b1 = 0;
									  break;  // (C - A) max 0
					case momLOWto0:   if (b1 < b2) b1 = 0;
									  break;  // if A<C then 0 else A
					case momHIGHtoFF: if (b1 > b2) b1 = ones;
									  break;  // if A>C then 0xffffffff else A
					case mom0orFF:    if (b1 < b2) b1 = 0; else b1 = ones;
									  break;  // if A<C then 0 else 0xffffffff
			     	case momAbsDiff:  if (b1 < b2) b1 = b2 - b1; else b1 -= b2;
						       		  break;  // Abs(b1-b2)
			     	case momAbsDiff8: if (b1 < b2) b1 = b2 - b1; else b1 -= b2;
					                  if ((b1 &= 0xff) > 128) b1 = 256 - b1;
						       	      break; // Abs(b1-b2) MIN (256 - Abs(b1-b2))

                    case mmcmMERGE:   b1 = b2;
                    				  break;  // mask is negated for the operation
					default:  ;
				}
				m2[xcnt] = b1;
                retand &= b1;  retor |= b1;
			}
            if (bmask >>= 1) xcnt++;
            else xcnt = xnext;
		}
		bm1 += p1;  bm2 += p2;
		if (!ignore) bmm += pm;
	}
Fin:
    if (retor) { if (retand == ones) ret = 1; }
    else ret = 0;
	return ret;
}


/************************************************* MemMaskMemMem32 */
/***
	Function:	MemMaskMemMem32

	Description:
		This function implements listed below DWORDwise operations of type
				mem1 <oper if mask> mem2 => mem3.

	Statics/Globals:
		None

	Input Arguments:
		pmem1 - pointer to the 1-st argument memory rectangle ("A")
		pmem2 - pointer to the 2-nd argument memory rectangle ("C")
		pmem3 - pointer to the target memory rectangle ("B")
		pmask - pointer to the argument bit mask
		xe - number of pixels in each row
		ye - number of rows
		pitch1 - address difference between neighboring rows (in bytes)
				in 1-st argument memory rectangle ("A")
		pitch2 - address difference between neighboring rows (in bytes)
				in 2-nd argument memory rectangle ("C")
		pitch3 - address difference between neighboring rows (in bytes)
				in target memory rectangle ("B")
		pitchmask - address difference between neighboring rows (in bytes)
				in the bit mask
		oper - 	A <oper if mask> C => B
				operation symbolic constants are the same as for MemOpMem(),
				plus one more operation, and 2 modifiers may be used;

			momZERO 	= 0   set all bits to zero
			momAND 	= 1   A & C
			momMORE 	= 2   A & ~C  (1 if A > C)
			momCOPY1 = 3   A  (copy A into B)
			momLESS 	= 4   ~A & C  (1 if A < C)
			momCOPY2 = 5   C  (copy const into B)
			momEXOR 	= 6   A ^ C  (mod 2 {exclusive OR})
			momOR 	= 7   A | C
			momNOR 	= 8   ~(A | C)  (not OR)
			momEQUAL = 9   ~(A ^ C)  (1 if A = C)
			momNOT2 	=10   copy ~C into B
			momEQMORE=11   A | ~C  (1 if A >= C)
			momNOT1 	=12   copy ~A into B
			momEQLESS=13   ~A | C  (1 if A <= C)
			momNAND 	=14   ~(A & C)  (not AND)
			momFULL 	=15   set all bits to 1 (pixels to 0xffffffff)

			momMIN 	=16   A MIN C
			momMAX 	=17   A MAX C
			momPLUS 	=18   (A + C) mod 2^^32
			momSPLUS 	=19   (A + C) min 0xffffffff (saturation)
			momMINUS 	=20   (A - C) mod 2^^32
			momIMINUS 	=21   (C - A) mod 2^^32 (inverse order)
			momSMINUS 	=22   (A - C) max 0   (saturation)
			momSIMINUS 	=23   (C - A) max 0   (saturation and inverse order)
			momLOWto0 	=24   if A >= C then A else 0
			momHIGHtoFF =25   if A <= C then A else 0xffffffff
			mom0orFF 	=26   if A < C then 0 else 0xffffffff
			momAbsDiff 	  =27   if A < C then (C - A) else (A - C)
			momAbsDiff8 =28   Abs((A=&0xff) - C) MIN (256 - Abs(A - C))

			mmcmMERGE   =47   if (mask) then B=A  else B=C

					one of the following modifiers can be ORed with the
					operation code:
			mmcmNEG    =0x80   (oper | mmcmNEG) means "use negative mask"
			mmcmIGNORE =0x40   (oper | mmcmIGNORE) means "ignore mask";
								last modifier is useful because MemMaskMem()
								and MemMaskConstMem32() work on rectangles, while
								MemOpMem() and MemConstMem() work on buffers only

	Returns:
		0 - if all result pixels are 0;
		1 - if all result pixels are 0xffffffff;
        2 - otherwise

***/
DWORD _stdcall MemMaskMemMem32 (
			void *mem1, void *mem2, void *mem3, void *mask,
			DWORD xe, DWORD ye,
            DWORD pitch1, DWORD pitch2, DWORD pitch3,
            DWORD pitchmask, DWORD oper)
{
	DWORD ignore, b1, b2, op, neg, p1, p2, p3, pm;
	DWORD *m1, *m2, *m3, *mmdw;
    BYTE *bm1, *bm2, *bm3, *bmm;
	DWORD xcnt, ycnt, bmask, negoper, ones, xnext,
    	retand, retor, ret = 2;

    bm1 = (BYTE *)mem1;  p1 = pitch1;
    bm2 = (BYTE *)mem2;  p2 = pitch2;
    bm3 = (BYTE *)mem3;  p3 = pitch3;
	ones = 0xffffffff;  negoper = ignore = 0;
	if (oper & mmcmNEG) { negoper = ones;  oper ^= mmcmNEG; }
	if (oper & mmcmIGNORE) { ignore = 1;  oper ^= mmcmIGNORE; }
	else { bmm = (BYTE *)mask;  pm = pitchmask; }
    ycnt = 0;  retand = ones;  retor = 0;
	if (oper > 15) goto Sixteen;

// Bool0to15:
	if (oper < 8) {  op = oper;   neg = 0; }
	else {  op = 15 - oper;  neg = ones; }

	while (ycnt++ < ye)
	{
		m1 = (DWORD *)bm1;  m2 = (DWORD *)bm2;  m3 = (DWORD *)bm3;
        mmdw = (DWORD *)bmm;  xcnt = bmask = 0;
		while (xcnt < xe)
		{
            if (!bmask)
            {
                if (ignore) bmask = ones;
                else bmask = (*mmdw++) ^ negoper;
                xnext = xcnt + 32;
                if (!bmask) { xcnt = xnext;  continue; }
			}
            if (bmask & 1)
			{
				b1 = m1[xcnt];  b2 = m2[xcnt];
				switch (op)
				{
					case momZERO:  b1 = 0;     break;     // zero (0)
					case momAND:   b1 &= b2;   break;     // A and  C (1)
					case momMORE:  b1 = (b1 ^ b2) & b1;  break;  // A and ~C (2)
					case momLESS:  b1 = (b1 ^ b2) & b2;  break;  // ~A and C (4)
					case momCOPY2: b1 = b2;    break;     // C (5)
					case momEXOR:  b1 ^= b2;   break;     // A exor C (6)
					case momOR:    b1 |= b2;   break;     // A or C (7)
					default:  ;   // case momCOPY1 - A, nothing to do
				}
				m3[xcnt] = (b1 ^= neg);	  // possible negation
                retand &= b1;  retor |= b1;
			}
            if (bmask >>= 1) xcnt++;
            else xcnt = xnext;
		}
		bm1 += p1;  bm2 += p2;  bm3 += p3;
		if (!ignore) bmm += pm;
	}
    goto Fin;

Sixteen:
	if (oper == mmcmMERGE) negoper ^= ones;  // C if not mask
	while (ycnt++ < ye)
	{
		m1 = (DWORD *)bm1;  m2 = (DWORD *)bm2;  m3 = (DWORD *)bm3;
        mmdw = (DWORD *)bmm;  xcnt = bmask = 0;
		while (xcnt < xe)
		{
            if (!bmask)
            {
                if (ignore) bmask = ones;
                else bmask = (*mmdw++) ^ negoper;
                xnext = xcnt + 32;
                if (!bmask) { xcnt = xnext;  continue; }
			}
            if (bmask & 1)
			{
				b1 = m1[xcnt];  b2 = m2[xcnt];
				switch (oper)
				{
					case momMIN:      if (b1 > b2) b1 = b2;
									  break;  // A min C
					case momMAX:   	  if (b1 < b2) b1 = b2;
									  break;  // A max C
					case momPLUS:     b1 += b2;
									  break;  // (A + C) mod 2^^32
					case momSPLUS:    if ((b1 += b2) < b2) b1 = ones;
									  break;  // (A + C) min 0xffffffff (saturation)
					case momMINUS:    b1 -= b2;
									  break;  // (A - C) mod 2^^32
					case momIMINUS:   b1 = b2 - b1;
									  break;  // (C - A) mod 2^^32
					case momSMINUS:   if (b1 >= b2) b1 -= b2; else b1 = 0;
									  break;  // (A - C) max 0
					case momSIMINUS:  if (b1 <= b2) b1 = b2 - b1; else b1 = 0;
									  break;  // (C - A) max 0
					case momLOWto0:   if (b1 < b2) b1 = 0;
									  break;  // if A<C then 0 else A
					case momHIGHtoFF: if (b1 > b2) b1 = ones;
									  break;  // if A>C then 0xffffffff else A
					case mom0orFF:    if (b1 < b2) b1 = 0; else b1 = ones;
									  break;  // if A<C then 0 else 0xffffffff
			     	case momAbsDiff:  if (b1 < b2) b1 = b2 - b1; else b1 -= b2;
						       		  break;  // Abs(b1-b2)
			     	case momAbsDiff8:
                    				  if (b1 < b2) b1 = b2 - b1; else b1 -= b2;
					                  if ((b1 &= 0xff) > 128) b1 = 256 - b1;
						       	      break; // Abs(b1-b2) MIN (256 - Abs(b1-b2))

                    case mmcmMERGE:   b1 = b2;
                    				  break;  // mask is negated for the operation
					default:  ;
				}
				m3[xcnt] = b1;
                retand &= b1;  retor |= b1;
			}
            if (bmask >>= 1) xcnt++;
            else xcnt = xnext;
		}
		bm1 += p1;  bm2 += p2;  bm3 += p3;
		if (!ignore) bmm += pm;
	}
Fin:
    if (retor) { if (retand == ones) ret = 1; }
    else ret = 0;
	return ret;
}


/********************************************************* MemMulDivMem */
/***
	Function:	MemMulDivMem

	Description:
		This function calculates buffer mem2 from buffer mem1
		according to the formula:
				(mem1 * mul) / div => mem2
		div = 0 is treated as 1.

	Statics/Globals:
		None

	Input Arguments:
		pmem1 - pointer to the source memory buffer

		pmem2 - pointer to the target memory buffer

		size - size of each buffer (in bytes)

		mul - multiplier

		div - divisor

	Output Arguments:
		None

	Returns:
		0 - no byte overflow, 1 - byte overflow.

***/
DWORD _stdcall MemMulDivMem (
					BYTE *mem1, BYTE *mem2, DWORD size,
					WORD mul, WORD div)
{
	DWORD d, over = 0;
	WORD actdiv;

	if (div > 1) actdiv = div;	else actdiv = 1;
	while (size--)
	{
		d = (DWORD)(*mem1++) * mul;
		d /= actdiv;
		over |= d;
		*mem2++ = (BYTE)d;
	}
	return (over >> 8);
}


/***************************************************** MemMaskData */
/***
	Function:	MemMaskData

	Description:
		This function calculates the Bit Mask on the basis of the contents
		of the given byte rectangle, the threshold and the operation:
		Oper (ByteMem, threshold) => BitMem

	Statics/Globals:
		None

	Input Arguments:
		mem - pointer to byte rectangle (pixel size = 8)

		xe - number of pixels (bytes) in each row

		ye - number of rows

		mempitch - address difference between neighboring rows
					(may be other than Xe)
		mask - pointer to bit mask (pixel size = 1)

		maskpitch - address difference between neighboring rows in
					the bit mask; is given in BYTES

		data - pointer to array of not less than 9 longs;
        		fills it with:
        	data[0] = NUMBER of one bits in mask
        	data[1] = SUM of bytes in 'mem' corresponding to one bits in mask
        	data[2] = MIN of bytes in 'mem' corresponding to one bits in mask
        	data[3] = MAX of bytes in 'mem' corresponding to one bits in mask
        	data[4] = X coordinate of the MIN pixel
        	data[5] = Y coordinate of the MIN pixel
        	data[6] = X coordinate of the MAX pixel
        	data[7] = Y coordinate of the MAX pixel
        	data[8] = standard deviation:
            			(long)(Sqr(Sum((P-Aver(P))^2)/NumOf)+0.5)
        	data[9] = number of pixels with MIN value
        	data[10] = number of pixels with MAX value

	Returns
		number of 1-bits in mask (same as data[0])
***/
DWORD _stdcall MemMaskData (
				BYTE *mem, DWORD xe, DWORD ye, DWORD mempitch,
				BYTE *mask, DWORD maskpitch,
                long *data)
{
	DWORD xcnt, ycnt, c, bits;
  	long Num, Sum, Minval, Maxval,
    	 Xmi, Ymi, Xma, Yma, Nmi, Nma;
	BYTE *memrow, *begmemrow, *begmaskrow;
  	DWORD *maskrow, Dpl;
  	double Dp, wrk;

	begmemrow = mem;  begmaskrow = mask;
  	ycnt = Num = Sum = Xmi = Ymi = Xma = Yma = Nmi = Nma = 0;
  	Minval = 256;  Maxval = -1;  Dp = 0.0;
	while (ycnt < ye)
	{
		memrow = begmemrow;
     	maskrow = (DWORD *)begmaskrow;
     	xcnt = 0;  bits = 0;  Dpl = 0;
		while (xcnt < xe)
		{
            if (!bits) bits = *maskrow++;
            if (bits & 1)
            {
                c = ((DWORD)memrow[xcnt]) & 0xff;
                Num++;  Sum += c;  Dpl += c * c;
                if ((long)c <= Minval)
                {
                	if ((long)c < Minval)
                 	{
                    	Minval = c;  Nmi = 1;
                        Xmi = xcnt;  Ymi = ycnt;
                    } else Nmi++;
                }
                if ((long)c >= Maxval)
                {
                	if ((long)c > Maxval)
                    {
                		Maxval = c;  Nma = 1;
                        Xma = xcnt;  Yma = ycnt;
                    } else Nma++;
                }
            }
            if (bits >>= 1) xcnt++;
         	else xcnt = (xcnt & 0xffffffe0) + 32;
		}
     	begmemrow += mempitch;  begmaskrow += maskpitch;
      	Dp += (double)Dpl;
     	ycnt++;
	}
    data[0] = Num;        	data[1] = Sum;
    data[2] = Minval;      	data[3] = Maxval;
    data[4] = Xmi;        	data[5] = Ymi;
    data[6] = Xma;        	data[7] = Yma;
    if (Num)
    {
   		wrk = (double)Sum / Num; // mean
   		wrk = Dp / Num - wrk * wrk;
	   	data[8] = (long)(sqrt(wrk) + 0.5);
    } else data[8] = 0;
    data[9] = Nmi;        	data[10] = Nma;
	return Num;
}


/*********************************************************** MemNeiMem */
/***
	Function:	MemNeiMem

	Description:
		This function implements dilation and erosion operations:
				Dilation(Rectangle1) => Rectangle2 , or
				Erosion(Rectangle1) => Rectangle2
		Dilation is maximum of a pixel and it's 8 neighbors.
		Erosion is minimum of a pixel and it's 8 neighbors

	Statics/Globals:
		None

	Arguments:
		pMem1 - pointer to the source Rectangle

		Xe - number of pixels (bytes) in each row

		Ye - number of rows

		Pitch1 - address difference between neighboring rows (in bytes)
				in the source Rectangle
		pMem2 - pointer to the target Rectangle

		Pitch2 - address difference between neighboring rows (in bytes)
				in the target Rectangle
		oper - 	Oper(Rectangle1) => Rectangle2
			mnmMIN   =0   min of self and 8 byte neighbors (erosion);
			mnmMAX   =1   max of self and 8 byte neighbors (dilation);
			mnm124   =12  averaging with matrix {1,2,1; 2,4,2; 1,2,1}/16
			mnmMED   =13  middle of self and 8 bit neighbors;
			mnmMIN4	 =20  min of the pixel and it's 4 neighbors;
			mnmMAX4	 =21  max of the pixel and it's 4 neighbors;
			mnmMINH2 =22  min of self and 2 bit horizontal neighbors;
			mnmMAXH2 =23  max of self and 2 bit horizontal neighbors;
			mnmMINV2 =24  min of self and 2 bit vertical neighbors;
			mnmMAXV2 =25  max of self and 2 bit vertical neighbors;

	Returns:
		None

***/
void _stdcall MemNeiMem (BYTE *Mem1,
               DWORD Xe, DWORD Ye, DWORD Pitch1,
					BYTE *Mem2, DWORD Pitch2, short oper)
{
	WORD prev, curr, cur1, nxt, flag4 = 0;
	DWORD xcnt, ycnt, pitch12, msize;
	BYTE *RowR, *BegRowR, *RowW, *BegRowW,
         *othermem = NULL;

   if ((oper == mnmMIN4) || (oper == mnmMAX4))
   {
		flag4 = 1;
   	if (Mem1 == Mem2)
   	{
//   	  	othermem = (BYTE *)malloc (Pitch1 * Ye);
//	     	if (!othermem) return;
   		msize = Pitch1 * Ye;
   		if (msize > Work4KSize)
         {
           	othermem = (BYTE *)malloc(msize);
	      	if (!othermem) return;
		   } else othermem = Work4K;
    	  	MemMaskMem (Mem1, othermem, othermem, Pitch1, Ye,
        	    		Pitch1, Pitch1, Pitch1, momCOPY1 | mmcmIGNORE);
			Mem1 = othermem;	Pitch2 = Pitch1;
      }
   }

	BegRowR = Mem1;  BegRowW = Mem2;
	if ((oper == mnmMINV2) || (oper == mnmMAXV2))
    	goto VertLoop;
// Horizontal loop:
	ycnt = 0;
	while (ycnt++ < Ye)
	{
		RowR = BegRowR;  RowW = BegRowW;
		xcnt = 0;  curr = nxt = *RowR++;

      switch (oper)
		{
	  case mnmMIN:// mnmMIN4, mnmMINH2:
				while ((xcnt++) < Xe)
				{
					prev = curr;  curr = nxt;
					if (xcnt < Xe) nxt = *RowR++;
					if (prev > curr) prev = curr;
					if (prev > nxt) prev = nxt;
					*RowW++ = (BYTE)prev;
				}
            break; // min

	    case mnmMIN4:
		  	while ((xcnt++) < Xe)
			{
				prev = curr;  curr = nxt;
				if (xcnt < Xe) nxt = *RowR++;
				if (prev > curr) prev = curr;
				if (prev > nxt) prev = nxt;
				*RowW++ = (BYTE)prev;
			}
           break; // min

	    case mnmMINH2:
		  	while ((xcnt++) < Xe)
			{
				prev = curr;  curr = nxt;
				if (xcnt < Xe) nxt = *RowR++;
				if (prev > curr) prev = curr;
				if (prev > nxt) prev = nxt;
				*RowW++ = (BYTE)prev;
			}
           break; // min

        case mnmMAX:// mnmMAX4, mnmMAXH2:
				while ((xcnt++) < Xe)
				{
					prev = curr;  curr = nxt;
					if (xcnt < Xe) nxt = *RowR++;
					if (prev < curr) prev = curr;
					if (prev < nxt) prev = nxt;
					*RowW++ = (BYTE)prev;
				}
            break; // max
		case mnmMAX4:
				while ((xcnt++) < Xe)
				{
					prev = curr;  curr = nxt;
					if (xcnt < Xe) nxt = *RowR++;
					if (prev < curr) prev = curr;
					if (prev < nxt) prev = nxt;
					*RowW++ = (BYTE)prev;
				}
			break; // max
        
		case mnmMAXH2:
				while ((xcnt++) < Xe)
				{
					prev = curr;  curr = nxt;
					if (xcnt < Xe) nxt = *RowR++;
					if (prev < curr) prev = curr;
					if (prev < nxt) prev = nxt;
					*RowW++ = (BYTE)prev;
				}
			break; // max
		
		case mnm124:
				while ((xcnt++) < Xe)
				{
					prev = curr;  curr = nxt;
					if (xcnt < Xe) nxt = *RowR++;
					*RowW++ = (WORD)((prev + curr + curr + nxt + 2) >> 2);
				}
            break; // aver
        case mnmMED:
				while ((xcnt++) < Xe)
				{
					prev = curr;  curr = nxt;
					if (xcnt < Xe) nxt = *RowR++;
					if (prev < curr)
                    {
                    	if (curr < nxt)	prev = curr;
                        else if (prev < nxt) prev = nxt;
					} else
                    {
                    	if (curr > nxt)	prev = curr;
                        else if (prev > nxt) prev = nxt;
					}
					*RowW++ = (BYTE)prev;
				}
            break; // middle
        default:
        		ycnt = Ye; // to break loop
		}
		BegRowR += Pitch1;  BegRowW += Pitch2;
	}
VertLoop:
	if ((Ye < 2) || (oper == mnmMINH2) || (oper == mnmMAXH2))
	{
    	goto Fin;
   }

// Verticval loop:
	BegRowW = Mem2;  xcnt = 0;
	if ((flag4)	|| (oper == mnmMINV2) || (oper == mnmMAXV2))
    {
     	BegRowR = Mem1;  pitch12 = Pitch1;
    } else
    {
    	BegRowR = Mem2;  pitch12 = Pitch2;
    }
	while (xcnt++ < Xe)
	{
		RowW = BegRowW;   RowR = BegRowR;
		ycnt = 0;  curr = nxt = *RowR;

      switch (oper)
	   {
        case mnmMIN:
			while ((ycnt++) < Ye)
			{
				prev = curr;  curr = nxt;
				if (ycnt < Ye) RowR += pitch12;
				nxt = *RowR;
				if (prev > curr) prev = curr;
				if (prev > nxt) prev = nxt;
				*RowW = (BYTE)prev;
				RowW += Pitch2;
         }
			break; // min
        case mnmMAX:
			while ((ycnt++) < Ye)
			{
				prev = curr;  curr = nxt;
				if (ycnt < Ye) RowR += pitch12;
				nxt = *RowR;
				if (prev < curr) prev = curr;
				if (prev < nxt) prev = nxt;
				*RowW = (BYTE)prev;
				RowW += Pitch2;
         }
			break; // max
        case mnm124:
			while ((ycnt++) < Ye)
			{
				prev = curr;  curr = nxt;
				if (ycnt < Ye) RowR += pitch12;
				nxt = *RowR;
				*RowW = (BYTE)((prev + (curr << 1) + nxt + 2) >> 2);
				RowW += Pitch2;
         }
			break; // aver
        case mnmMIN4:
			while ((ycnt++) < Ye)
			{
				prev = curr;  curr = nxt;
				if (ycnt < Ye) RowR += pitch12;
				nxt = *RowR;  cur1 = *RowW;
				if (prev > cur1) prev = cur1;
				if (prev > nxt) prev = nxt;
				*RowW = (BYTE)prev;
				RowW += Pitch2;
         }
			break; // min4
        case mnmMAX4:
			while ((ycnt++) < Ye)
			{
				prev = curr;  curr = nxt;
				if (ycnt < Ye) RowR += pitch12;
				nxt = *RowR;  cur1 = *RowW;
				if (prev < cur1) prev = cur1;
				if (prev < nxt) prev = nxt;
				*RowW = (BYTE)prev;
				RowW += Pitch2;
         }
			break; // max4
        case mnmMINV2:
			while ((ycnt++) < Ye)
			{
				prev = curr;  curr = nxt;  // cur1 = *RowR;
				if (ycnt < Ye) RowR += pitch12;
				nxt = *RowR;
//				if (prev > cur1) prev = cur1;
				if (prev > curr) prev = curr;
				if (prev > nxt) prev = nxt;
				*RowW = (BYTE)prev;
				RowW += Pitch2;
         }
			break; // minV2
        case mnmMAXV2:
			while ((ycnt++) < Ye)
			{
				prev = curr;  curr = nxt;  // cur1 = *RowR;
				if (ycnt < Ye) RowR += pitch12;
				nxt = *RowR;
//				if (prev < cur1) prev = cur1;
				if (prev < curr) prev = curr;
				if (prev < nxt) prev = nxt;
				*RowW = (BYTE)prev;
				RowW += Pitch2;
         }
			break; // maxV2
        case mnmMED:
			while ((ycnt++) < Ye)
			{
				prev = curr;  curr = nxt;
				if (ycnt < Ye) RowR += pitch12;
				nxt = *RowR;
				if (prev < curr)
            {
              	if (curr < nxt)	prev = curr;
               else if (prev < nxt) prev = nxt;
				} else
            {
              	if (curr > nxt)	prev = curr;
               else if (prev > nxt) prev = nxt;
				}
				*RowW = (BYTE)prev;
				RowW += Pitch2;
         }
			break; // middle
		  default: ;
      }
      BegRowW++;  BegRowR++;
	}
Fin:
	if (othermem)
  		if (msize > Work4KSize) free(othermem);
}


/************************************************************** MemOpMem */
/***
	Function:	MemOpMem

	Description:
		This function implements 27 listed below bytewise
		operations of type
				mem1 <oper> mem2 => mem2

	Statics/Globals:
		None

	Input Arguments:
		mem1 - pointer to 1-st argument memory buffer ("A")

		mem2 - pointer to 2-nd argument and result memory buffer ("B")

		size - size of each argument buffer (bytes)

		oper - 	A <oper> B => B
			operations 0 to 15 are bitwise, values of symbolic
			constants correspond to truth table;
			operations 16+ are bytewise;

			momZERO 	= 0   set all bits to zero
			momAND 		= 1   A & B
			momMORE 	= 2   A & ~B  (1 if A > B)
			momCOPY1 	= 3   A  (copy A into B)
			momLESS 	= 4   ~A & B  (1 if A < B)
			momCOPY2 	= 5   B  (copy B {or const} into B)
			momEXOR 	= 6   A ^ B  (mod 2 {exclusive OR})
			momOR 		= 7   A | B
			momNOR 		= 8   ~(A | B)  (not OR)
			momEQUAL 	= 9   ~(A ^ B)  (1 if A = B)
			momNOT2 	=10   copy ~B into B
			momEQMORE	=11   A | ~B  (1 if A >= B)
			momNOT1 	=12   copy ~A into B
			momEQLESS	=13   ~A | B  (1 if A <= B)
			momNAND 	=14   ~(A & B)  (not AND)
			momFULL 	=15   set all bits to 1 (bytes to 255)

			momMIN 		=16   A MIN B
			momMAX 		=17   A MAX B
			momPLUS 	=18   (A + B) mod 256
			momSPLUS 	=19   (A + B) min 255 (saturation)
			momMINUS 	=20   (A - B) mod 256
			momIMINUS	=21   (B - A) mod 256 (inverse order)
			momSMINUS   =22   (A - B) max 0   (saturation)
 			momSIMINUS  =23   (B - A) max 0   (saturation and inverse order)
			momLOWto0   =24   if A >= B then A else 0
			momHIGHtoFF =25   if A <= B then A else 255
			mom0orFF    =26   if A < B then 0 else 255
			momAbsDiff  =27   if A < B then (B - A) else (A - B)
			momAbsDiff8 =28   Abs(A - B) MIN (256 - Abs(A - B))

	Output Arguments:
		None

	Returns: 0/-1

**/
short _stdcall MemOpMem (
				BYTE *mem1, BYTE *mem2,
				DWORD size, short oper)
{
	DWORD *m1, *m2, ones, sol, lmask, rmask, cnt, rem, i;
	WORD b1, b2, opermax = momAbsDiff8;
   	short ret = -1;

	if (oper > 15) goto ByteWise;
// BitWise:
	if (oper < 0)  goto Fin;
	ones = 0xffffffffL;   sol = sizeof(DWORD);
   	cnt = size / sol;   rem = size - (cnt * sol);
   	if (rem)
   	{
   	 	rmask = ones << (rem << 3);
   	 	lmask = ones ^ rmask;
   	}
	m1 = (DWORD *)mem1;   m2 = (DWORD *)mem2;
   	switch (oper)
	{
     	case momZERO:  // 0
       		memset(mem2, 0, size);
       		break;     // zero
     	case momAND:  // 1
		 	for (i = 0;  i < cnt;  i++) m2[i] &= m1[i];
       		if (rem) m2[cnt] = (m2[cnt] & rmask) | ((m2[cnt] & m1[cnt]) & lmask);
       		break;     // 1st and  2nd
	  	case momMORE: // 2
		 	for (i = 0;  i < cnt;  i++) m2[i] = (m2[i] ^ ones) & m1[i];
       		if (rem) m2[cnt] = (m2[cnt] & rmask) |
       							(((m2[cnt] ^ ones) & m1[cnt]) & lmask);
      		 break;  // 1st and ~2nd
	  	case momCOPY1: // 3
       		memmove (mem2, mem1, size);
       		break;     // 1st
	  	case momLESS: // 4
		 	for (i = 0;  i < cnt;  i++) m2[i] &= (m1[i] ^ ones);
       		if (rem) m2[cnt] = (m2[cnt] & rmask) |
       							(((m1[cnt] ^ ones) & m2[cnt]) & lmask);
      		break;  // 2nd and ~1st
	  	// 5: see default
	  	case momEXOR: // 6
       		for (i = 0;  i < cnt;  i++) m2[i] ^= m1[i];
       		if (rem) m2[cnt] = (m2[cnt] & rmask) | ((m2[cnt] ^ m1[cnt]) & lmask);
       		break;     // 1st exor 2nd
     	case momOR:  // 7
		 	for (i = 0;  i < cnt;  i++) m2[i] |= m1[i];
       		if (rem) m2[cnt] = (m2[cnt] & rmask) | ((m2[cnt] | m1[cnt]) & lmask);
       		break;     // 1st or  2nd
        case momNOR:  // 8
		 	for (i = 0;  i < cnt;  i++) m2[i] = (m2[i] | m1[i]) ^ ones;
       		if (rem) m2[cnt] = (m2[cnt] & rmask) |
       						(((m2[cnt] | m1[cnt]) ^ ones) & lmask);
       		break;     // ~(A | B)  (not OR)
        case momEQUAL:  // 9
		 	for (i = 0;  i < cnt;  i++) m2[i] = (m2[i] ^ m1[i]) ^ ones;
       		if (rem) m2[cnt] = (m2[cnt] & rmask) |
       						(((m2[cnt] ^ m1[cnt]) ^ ones) & lmask);
       		break;     // ~(A ^ B)  (1 if A = B)
     	case momNOT2:  // 10
		 	for (i = 0;  i < cnt;  i++) m2[i] ^= ones;
       		if (rem) m2[cnt] = ((m2[cnt] ^ ones) & rmask);
       		break;     // copy ~B into B
	  	case momEQMORE: // 11
		 	for (i = 0;  i < cnt;  i++) m2[i] = (m2[i] ^ ones) | m1[i];
       		if (rem) m2[cnt] = (m2[cnt] & rmask) |
       						(((m2[cnt] ^ ones) | m1[cnt]) & lmask);
       		break;  //  A | ~B  (1 if A >= B)
	  	case momNOT1: // 12
        	for (i = 0;  i < cnt;  i++) m2[i] = (m1[i] ^ ones);
       		if (rem) m2[cnt] = (m2[cnt] & rmask) | ((m1[cnt] ^ ones) & lmask);
       		break;     // copy ~A into B
	  	case momEQLESS: // 13
        	for (i = 0;  i < cnt;  i++) m2[i] |= (m1[i] ^ ones);
       		if (rem) m2[cnt] = (m2[cnt] & rmask) |
       						(((m1[cnt] ^ ones) | m2[cnt]) & lmask);
       		break;  //  ~A | B  (1 if A <= B)
     	case momNAND:  // 14
		 	for (i = 0;  i < cnt;  i++) m2[i] = (m2[i] & m1[i]) ^ ones;
       		if (rem) m2[cnt] = (m2[cnt] & rmask) |
       						(((m2[cnt] & m1[cnt]) ^ ones) & lmask);
       		break;     // ~(A & B)  (not AND)
     	case momFULL:  // 15
		 	for (i = 0;  i < cnt;  i++) m2[i] = ones;
       		if (rem) m2[cnt] = (m2[cnt] & rmask) | lmask;
       		break;     // set all bits to 1 (bytes to 255)
	  	default:  ;   // 5: case momCOPY2 - 2nd, nothing to do
	}
	goto Ok;

ByteWise:
	if (oper > opermax)  goto Fin;
   	switch (oper)
	{
     	case momMIN:  // 16
        	for (i = 0;  i < size;  i++)
       	  		if ((b1 = mem1[i]) < mem2[i]) mem2[i] = b1;
       		break;     // 1st min 2nd
     	case momMAX:  // 17
		 	for (i = 0;  i < size;  i++)
       	  		if ((b1 = mem1[i]) > mem2[i]) mem2[i] = b1;
       		break;     // 1st max 2nd
     	case momPLUS:  // 18
		 	for (i = 0;  i < size;  i++) mem2[i] += mem1[i];
       		break;     // (1st + 2nd) mod 256
     	case momSPLUS:  // 19
		 	for (i = 0;  i < size;  i++)
       		{
       	  		b1 =(WORD)((WORD)mem2[i] + (WORD)mem1[i]);
       	  		if (b1 & 0x100) mem2[i] = 255; else mem2[i] = b1;
       		}
       		break;     // (1st + 2nd) min 255 (saturation)
     	case momMINUS:  // 20
		 	for (i = 0;  i < size;  i++)
       	  		mem2[i] = (BYTE)(mem1[i] - mem2[i]);
       		break;     // (1st - 2nd) mod 256
     	case momIMINUS:  // 21
		 	for (i = 0;  i < size;  i++) mem2[i] -= mem1[i];
       		break;     // (2nd - 1st) mod 256
     	case momSMINUS:  // 22
		 	for (i = 0;  i < size;  i++)
       		{
       	  		if ((b1 = (WORD)mem1[i]) <= (b2 = (WORD)mem2[i])) mem2[i] = 0;
           		else mem2[i] = (BYTE)(b1 - b2);
       		}
       		break;     // (1st - 2nd) max 0
     	case momSIMINUS:  // 23
		 	for (i = 0;  i < size;  i++)
       		{
       	  		if ((b1 = (WORD)mem1[i]) >= (b2 = (WORD)mem2[i])) mem2[i] = 0;
           		else mem2[i] = (BYTE)(b2 - b1);
       		}
       		break;     // (2nd - 1st) max 0
     	case momLOWto0:  // 24
		 	for (i = 0;  i < size;  i++)
            {
       	  		if ((b1 = (WORD)mem1[i]) < mem2[i]) mem2[i] = 0;
           		else mem2[i] = b1;
            }
       		break;     // set bytes below mem2 to 0
     	case momHIGHtoFF:  // 25
		 	for (i = 0;  i < size;  i++)
       	  	{
            	if ((b1 = (WORD)mem1[i]) > mem2[i]) mem2[i] = (BYTE)0xFF;
           		else mem2[i] = b1;
            }
       		break;     // set bytes above mem2 to 255
     	case mom0orFF:  // 26
		 	for (i = 0;  i < size;  i++)
       	  	{
            	if (mem1[i] < mem2[i]) mem2[i] = 0;
           		else mem2[i] = (BYTE)0xFF;
            }
       		break;     // set bytes below mem2 to 0,
       				// above or equal mem2 to 255
     	case momAbsDiff:  // 27
		 	for (i = 0;  i < size;  i++)
       	  	{
       	  		if ((b1 = (WORD)mem1[i]) < (b2 = (WORD)mem2[i]))
                	mem2[i] = (BYTE)(b2 - b1);
           		else mem2[i] = (BYTE)(b1 - b2);
            }
       		break;     // Abs(b1-b2)
     	case momAbsDiff8:  // 28
		 	for (i = 0;  i < size;  i++)
       	  	{
       	  		if ((b1 = (WORD)mem1[i]) < (b2 = (WORD)mem2[i])) b1 = b2 - b1;
           		else b1 = b1 - b2;
                if (b1 < 128) mem2[i] = (BYTE)b1;
                else mem2[i] = (BYTE)(256 - b1);
            }
       		break;     // Abs(b1-b2) MIN (256 - Abs(b1-b2))
 	  	default:  ;   // nothing to do
   	}
Ok:
	ret = 0;
Fin:
	return ret;
}


/**************************************************** MemThreshMem */
/***
	Function:	MemThreshMem

	Description:
		This function transforms the given buffer into new buffer
		according to the threshold and the operation.
		It implements 5 listed below bytewise operations of the type
		Oper (buff1, threshold) => buff2
		All elements, which satisfy the operation condition, are changed,
		other elements are left unchanged.

	Statics/Globals:
		None

	Input Arguments:
		buff1 - pointer to source memory buffer

		buff2 - pointer to target memory buffer

		size - size of each buffer (bytes)

		threshold - argument of operation, accepted values are 0 to 255

		oper - defines the operation:

			mtmBZERO 	=16   less-than-threshold bytes => 0
			mtmBTHRESH 	=17   less-than-threshold bytes => threshold
			mtmGFULL 	=18   greater-than-threshold bytes => 255
			mtmGTHRESH  =19   greater-than-threshold bytes => threshold
			mtmTHRESH  	=20   less-than-threshold bytes => 0,
									greater-than or equal-to-threshold bytes => 255

	Returns: number of elements, satisfying the operation condition

***/
DWORD _stdcall MemThreshMem (
					BYTE *mem1, BYTE *mem2, DWORD size,
					WORD threshold, short oper)
{
	BYTE b;
	DWORD d = 0;

	switch (oper)
	{
		case mtmBZERO:
			while (size--)
			{
				if ((b = *mem1++) < threshold) { b = 0;  d++; }
				*mem2++ = b;
			}
			break;
		case mtmBTHRESH:
			while (size--)
			{
				if ((b = *mem1++) < threshold) { b = threshold;  d++; }
				*mem2++ = b;
			}
			break;
		case mtmGFULL:
			while (size--)
			{
				if ((b = *mem1++) > threshold) { b = 255;  d++; }
				*mem2++ = b;
			}
			break;
		case mtmGTHRESH:
			while (size--)
			{
				if ((b = *mem1++) > threshold) { b = threshold;  d++; }
				*mem2++ = b;
			}
			break;
		case mtmTHRESH:
			while (size--)
			{
				if (*mem1++ < threshold) b = 0;
				else  { b = 255;  d++; }
				*mem2++ = b;
			}
			break;

		default:  ;   
	}
	return d;
}


/**************************************************** MemThreshVal */
/***
	Function:	MemThreshVal

	Description:
		This function calculates numerical functions listed below,
		of the given threshold and the given buffer containing
		byte elements

	Statics/Globals:
		None

	Input Arguments:
		buff - pointer to argument memory buffer

		size - size of the buffer (bytes)

		threshold - argument of operation, accepted values are 0 to 255

		oper - defines which value to calculate

			mtvBELOW 	  =16   number of elements below the threshold
			mtvGREATER 	  =17   number of elements above the threshold
			mtvEQUAL      =18   number of elements equal to the threshold
			mtvSUMBELOW   =19   sum of elements below the threshold
			mtvSUMGREATER =20   sum of elements above the threshold
			mtvSUMGEQ 	  =21   sum of elements equal to or above the threshold

	Returns for operation
		mtvBELOW 		: number of below the threshold elements
		mtvGREATER 		: number of above the threshold elements
		mtvEQUAL 		: number of equal to the threshold elements
		mtvSUMBELOW 	: sum of below the threshold elements
		mtvSUMGREATER 	: sum of above the threshold elements
		mtvSUMGEQ 		: sum of equal to or above the threshold elements

***/
DWORD _stdcall MemThreshVal (
				BYTE *mem, DWORD size,
				WORD threshold, short oper)
{
	DWORD d = 0;
	BYTE p;

	switch (oper)
	{
		case mtvBELOW:
			while (size--) if (*mem++ < threshold) d++;
			break;
		case mtvGREATER:
			while (size--) if (*mem++ > threshold) d++;
			break;
		case mtvEQUAL:
			while (size--) if (*mem++ == threshold) d++;
			break;
		case mtvSUMBELOW:
			while (size--) if ((p = *mem++) < threshold) d += p;
			break;
		case mtvSUMGREATER:
			while (size--) if ((p = *mem++) > threshold) d += p;
			break;
		case mtvSUMGEQ:
			while (size--) if ((p = *mem++) >= threshold) d += p;
			break;

		default:  ;
	}
	return d;
}


/*********************************************************** NeiMem */
/***
	Function:	NeiMem

	Description:
		This function implements dilation and erosion operations.
		Dilation is maximum of a pixel and it's neighbors.
		Erosion is minimum of a pixel and it's neighbors

	Statics/Globals:
		None

	Arguments:
		pMem - pointer to the Rectangle

		Xe - number of pixels (bytes) in each row

		Ye - number of rows

		Pitch - address difference between neighboring rows (in bytes)
				in the Rectangle
		oper :
			mnmMIN   =0   min of self and 8 byte neighbors (erosion);
			mnmMAX   =1   max of self and 8 byte neighbors (dilation);
			mnm124   =12  averaging with matrix {1,2,1; 2,4,2; 1,2,1}/16
			mnmMED   =13  middle of self and 8 bit neighbors;
			mnmBLUR  =14  averaging with matrix {1,1,1; 1,1,1; 1,1,1}/9
			mnmMIN4	 =20  min of the pixel and it's 4 neighbors;
			mnmMAX4	 =21  max of the pixel and it's 4 neighbors;
			mnmMINH2 =22  min of self and 2 bit horizontal neighbors;
			mnmMAXH2 =23  max of self and 2 bit horizontal neighbors;
			mnmMINV2 =24  min of self and 2 bit vertical neighbors;
			mnmMAXV2 =25  max of self and 2 bit vertical neighbors;
			mnmOPEN8 =26  same as mnmMIN followed by mnmMAX;
			mnmCLOSE8 =27 same as mnmMAX followed by mnmMIN;
			mnmOPEN4 =28  same as mnmMIN4 followed by mnmMAX4;
			mnmCLOSE4 =29 same as mnmMAX4 followed by mnmMIN4;

      rep - number of repetitions

	Returns:
		None

***/
void _stdcall NeiMem (BYTE *mem, DWORD xe, DWORD ye,
				DWORD pitch, long oper, long rep)
{
	WORD pcurr, prev, curr, nxt, ncurr;
	DWORD xcnt, ycnt, i, k;
    long repcnt;
	BYTE *r, *rp, *rc, *rn, *w;

    repcnt = (long)rep;
    rp = Work4K; // memory for 1 row
    switch (oper)
	{
        case mnmMIN:
        {
//        	rp = (BYTE *)malloc(xe);  // get memory for 1 row
LmnmMIN:
        	for (i = 0;  i < xe;  i++) rp[i] = 255;
			ycnt = 0;  r = mem;  w = rp;
			while (ycnt++ < ye)
			{
				xcnt = 0;  curr = 255;  nxt = r[0];
            if (ycnt < ye) rn = r + pitch;
            else rn = r;
				while ((i = xcnt++) < xe)
				{
					prev = curr;  curr = nxt;
					if (xcnt < xe) nxt = r[xcnt];
               else nxt = 255;
               pcurr = rp[i];
               ncurr = rn[i];
               if (prev > curr) prev = curr;
					if (prev > nxt) prev = nxt;
               if (w[i] > prev) w[i] = prev;  // correct previous row
               rp[i] = prev;	// for next pass
               if (prev > pcurr) prev = pcurr;
               if (prev > ncurr) prev = ncurr;
               r[i] = prev;
    			}
            w = r;
         	r += pitch;
        	}
         if ((--repcnt) > 0) goto LmnmMIN;
//        	free(rp);
    		return;
      }

      case mnmMAX:
      {
//      	rp = (BYTE *)malloc(xe);  // get memory for 1 row
LmnmMAX:
        	for (i = 0;  i < xe;  i++) rp[i] = 0;
			ycnt = 0;  r = mem;  w = rp;
			while (ycnt++ < ye)
			{
				xcnt = 0;  curr = 0;  nxt = r[0];
            if (ycnt < ye) rn = r + pitch;
            else rn = r;
				while ((i = xcnt++) < xe)
				{
					prev = curr;  curr = nxt;
					if (xcnt < xe) nxt = r[xcnt];
               else nxt = 0;
               pcurr = rp[i];
               ncurr = rn[i];
               if (prev < curr) prev = curr;
					if (prev < nxt) prev = nxt;
               if (w[i] < prev) w[i] = prev;  // correct previous row
               rp[i] = prev;	// for next pass
               if (prev < pcurr) prev = pcurr;
               if (prev < ncurr) prev = ncurr;
               r[i] = prev;
    			}
            w = r;
         	r += pitch;
        	}
         if ((--repcnt) > 0) goto LmnmMAX;
//        	free(rp);
    		return;
      }

      case mnmMINH2:
      {
LmnmMINH2:
			ycnt = 0;  r = mem;
			while (ycnt++ < ye)
			{
				xcnt = 0;  curr = nxt = r[0];
				while ((i = xcnt++) < xe)
				{
					prev = curr;  curr = nxt;
					if (xcnt < xe) nxt = r[xcnt];
               if (prev > curr) prev = curr;
					if (prev > nxt) prev = nxt;
               r[i] = prev;
    			}
         	r += pitch;
        	}
         if ((--repcnt) > 0) goto LmnmMINH2;
        	return;
      }

	  	case mnmMINV2:
      {
LmnmMINV2:
			xcnt = 0;
			while ((i = xcnt++) < xe)
			{
				ycnt = 0;  r = mem;  k = pitch;
            curr = nxt = r[i];
				while (ycnt++ < ye)
				{
					prev = curr;  curr = nxt;
               if (ycnt == ye) k = 0;
               nxt = r[i + k];
               if (prev > curr) prev = curr;
					if (prev > nxt) prev = nxt;
               r[i] = prev;  r += pitch;
    			}
        	}
         if ((--repcnt) > 0) goto LmnmMINV2;
    		return;
      }
	  	case mnmMAXH2:
      {
LmnmMAXH2:
			ycnt = 0;  r = mem;
			while (ycnt++ < ye)
			{
				xcnt = 0;  curr = nxt = r[0];
				while ((i = xcnt++) < xe)
				{
					prev = curr;  curr = nxt;
					if (xcnt < xe) nxt = r[xcnt];
               if (prev < curr) prev = curr;
					if (prev < nxt) prev = nxt;
               r[i] = prev;
    			}
         	r += pitch;
        	}
         if ((--repcnt) > 0) goto LmnmMAXH2;
        	return;
      }
	  	case mnmMAXV2:
      {
LmnmMAXV2:
			xcnt = 0;
			while ((i = xcnt++) < xe)
			{
				ycnt = 0;  r = mem;  k = pitch;
            curr = nxt = r[i];
				while (ycnt++ < ye)
				{
					prev = curr;  curr = nxt;
               if (ycnt == ye) k = 0;
               nxt = r[i + k];
               if (prev < curr) prev = curr;
					if (prev < nxt) prev = nxt;
               r[i] = prev;  r += pitch;
    			}
        	}
         if ((--repcnt) > 0) goto LmnmMAXV2;
    		return;
      }
	  	case mnm124:
      {
Lmnm124:
			ycnt = 0;  r = mem;
			while (ycnt++ < ye)
			{
				xcnt = 0;  curr = nxt = r[0];
				while (xcnt < xe)
				{
					prev = curr;  curr = nxt;
               i = xcnt++;
					if (xcnt < xe) nxt = r[xcnt];
               r[i] = (BYTE)((prev + curr * 2 + nxt + 2) >> 2);
    			}
         	r += pitch;
        	}
			xcnt = 0;  rc = mem;
			while (xcnt++ < xe)
			{
				ycnt = 0;  r = rc;
            curr = nxt = r[0];
				while (ycnt++ < ye)
				{
					prev = curr;  curr = nxt;
               if (ycnt < ye) nxt = r[pitch];
               r[0] = (BYTE)((prev + curr * 2 + nxt + 2) >> 2);
               r += pitch;
    			}
         	rc++;
        	}
         if ((--repcnt) > 0) goto Lmnm124;
    		return;
	  	}
	  	case mnmMED:
      {
LmnmMED:
			ycnt = 0;  r = mem;
			while (ycnt++ < ye)
			{
				xcnt = 0;  curr = nxt = r[0];
				while (xcnt < xe)
				{
					prev = curr;  curr = nxt;
               i = xcnt++;
					if (xcnt < xe) nxt = r[xcnt];
               if (prev < curr)
               {
                	if (curr < nxt)	prev = curr;
                    else if (prev < nxt) prev = nxt;
                } else
                {
                	 if (curr > nxt)	prev = curr;
                   else if (prev > nxt) prev = nxt;
                }
                r[i] = (BYTE)prev;
    			}
         	r += pitch;
        	}
			xcnt = 0;  rc = mem;
			while (xcnt++ < xe)
			{
				ycnt = 0;  r = rc;
            curr = nxt = r[0];
				while (ycnt++ < ye)
				{
					prev = curr;  curr = nxt;
               if (ycnt < ye) nxt = r[pitch];
               if (prev < curr)
               {
                	if (curr < nxt)	prev = curr;
                  else if (prev < nxt) prev = nxt;
               } else
               {
                	if (curr > nxt)	prev = curr;
                  else if (prev > nxt) prev = nxt;
               }
               r[0] = (BYTE)prev;
               r += pitch;
    			}
         	rc++;
        	}
         if ((--repcnt) > 0) goto LmnmMED;
    		return;
      }
	  	case mnmBLUR:
      {
LmnmBLUR:
			ycnt = 0;  r = mem;
			while (ycnt++ < ye)
			{
				xcnt = 0;  curr = nxt = r[0];
				while (xcnt < xe)
				{
					prev = curr;  curr = nxt;
               i = xcnt++;
					if (xcnt < xe) nxt = r[xcnt];
               r[i] = (BYTE)((prev + curr + nxt + 2) / 3);
    			}
         	r += pitch;
        	}
			xcnt = 0;  rc = mem;
			while (xcnt++ < xe)
			{
				ycnt = 0;  r = rc;
            curr = nxt = r[0];
				while (ycnt++ < ye)
				{
					prev = curr;  curr = nxt;
               if (ycnt < ye) nxt = r[pitch];
               r[0] = (BYTE)((prev + curr + nxt + 1) / 3);
               r += pitch;
    			}
         	rc++;
        	}
         if ((--repcnt) > 0) goto LmnmBLUR;
    		return;
	  	}

	  	case mnmMIN4:
      {
//        	rp = (BYTE *)malloc(xe);  // get memory for 1 row
LmnmMIN4:
        	for (i = 0;  i < xe;  i++) rp[i] = mem[i];
			ycnt = 0;  r = mem;
			while (ycnt++ < ye)
			{
				xcnt = 0;  curr = nxt = r[0];
            if (ycnt < ye) rn = r + pitch;
            else rn = r;
				while ((i = xcnt++) < xe)
				{
					prev = curr;  curr = nxt;
					if (xcnt < xe) nxt = r[xcnt];
               pcurr = rp[i];  rp[i] = curr;	// for next pass
               ncurr = rn[i];
               if (prev > curr) prev = curr;
					if (prev > nxt) prev = nxt;
               if (prev > pcurr) prev = pcurr;
               if (prev > ncurr) prev = ncurr;
               r[i] = prev;
    			}
         	r += pitch;
        	}
         if ((--repcnt) > 0) goto LmnmMIN4;
//        	free(rp);
    		return;
      }
	  	case mnmMAX4:
      {
//        	rp = (BYTE *)malloc(xe);  // get memory for 1 row
LmnmMAX4:
        	for (i = 0;  i < xe;  i++) rp[i] = mem[i];
			ycnt = 0;  r = mem;
			while (ycnt++ < ye)
			{
				xcnt = 0;  curr = nxt = r[0];
            if (ycnt < ye) rn = r + pitch;
            else rn = r;
				while ((i = xcnt++) < xe)
				{
					prev = curr;  curr = nxt;
					if (xcnt < xe) nxt = r[xcnt];
               pcurr = rp[i];  rp[i] = curr;	// for next pass
               ncurr = rn[i];
               if (prev < curr) prev = curr;
					if (prev < nxt) prev = nxt;
               if (prev < pcurr) prev = pcurr;
               if (prev < ncurr) prev = ncurr;
               r[i] = prev;
    			}
         	r += pitch;
        	}
         if ((--repcnt) > 0) goto LmnmMAX4;
//        	free(rp);
    		return;
      }
	  	case mnmOPEN8:
      {
			NeiMem (mem, xe, ye, pitch, mnmMIN, rep);
			NeiMem (mem, xe, ye, pitch, mnmMAX, rep);
    		return;
      }
	  	case mnmCLOSE8:
      {
			NeiMem (mem, xe, ye, pitch, mnmMAX, rep);
			NeiMem (mem, xe, ye, pitch, mnmMIN, rep);
    		return;
      }
	  	case mnmOPEN4:
      {
			NeiMem (mem, xe, ye, pitch, mnmMIN4, rep);
			NeiMem (mem, xe, ye, pitch, mnmMAX4, rep);
    		return;
      }
	  	case mnmCLOSE4:
      {
			NeiMem (mem, xe, ye, pitch, mnmMAX4, rep);
			NeiMem (mem, xe, ye, pitch, mnmMIN4, rep);
    		return;
      }
   }
}


/**************************************************** MemToBitMask */
/***
	Function:	MemToBitMask

	Description:
		This function calculates the Bit Mask on the basis of the contents
		of the given byte rectangle, the threshold and the operation:
		Oper (ByteMem, threshold) => BitMem

	Statics/Globals:
		None

	Input Arguments:
		ByteMem - pointer to byte rectangle (pixel size = 8)

		Xe - number of pixels (bytes) in each row

		Ye - number of rows

		BytePitch - address difference between neighboring rows
					(may be other than Xe)
		pBitMem - pointer to bit mask (pixel size = 1)

		BitPitch - address difference between neighboring rows in
					the bit mask; is given in BYTES

		threshold - argument of operation, accepted values are 0 to 255

		oper - defines the operation:

			mtbmGE  =16   less-than-threshold bytes => 0
							  greater-than or equal-to-threshold bytes => 1
			mtbmBE  =17   greater-than-threshold bytes => 0
							  less-than or equal-to-threshold bytes => 1
			mtbmEQ  =18   equal-to-threshold bytes => 1, others => 0
			mtbmNE  =19   equal-to-threshold bytes => 0, others => 1

	Returns
		number of 1-bits in the resulting BitMem
***/
DWORD _stdcall MemToBitMask (
				BYTE *ByteMem, DWORD Xe, DWORD Ye, DWORD BytePitch,
				BYTE *BitMem, DWORD BitPitch, DWORD thresh, DWORD operation)
{
	WORD b, c, one, threshold, oper;
	DWORD xcnt, d;
	BYTE *BitRow, *BegBitRow, *ByteRow, *BegByteRow;

    threshold = (WORD)thresh;  oper = (WORD)operation;
	BegBitRow = BitMem;  BegByteRow = ByteMem;  d = 0;
	while (Ye--)
	{
		BitRow = BegBitRow;  ByteRow = BegByteRow;
		xcnt = 0;  b = 0;  one = 1;
		while ((xcnt++) < Xe)
		{
			c = *ByteRow++;
			if (oper == mtbmGE)
			{	if (c >= threshold) goto AddOne; else goto EndXloop; }
			if (oper == mtbmBE)
			{	if (c <= threshold) goto AddOne; else goto EndXloop; }
			if (oper == mtbmEQ)
			{	if (c == threshold) goto AddOne; else goto EndXloop; }
			if (oper == mtbmNE)
			{	if (c != threshold) goto AddOne; else goto EndXloop; }
			goto EndXloop;
AddOne:
			b |= one;  d++;
EndXloop:
			one <<= 1;
			if (one == 0x100)
			{
				*BitRow++ = b;  b = 0;  one = 1;
			}
		}
		if (one != 1)
      {
         c = *BitRow;
         b |= (WORD)(((one - 1) & c) ^ c);
         *BitRow = b;
      }
		BegBitRow += BitPitch;  BegByteRow += BytePitch;
	}
	return d;
}


/******************************************************* OutContFast */
/***
	Function:	OutContFast

	Description:
		This function builds the contour mask in BitMem.
	Input Arguments:
		mem - pointer to the Bit Mask

		xe - number of pixels (bits) in each row

		ye - number of rows

		pitch - address difference between neighboring rows (in bytes)
				in the limiting Bit Mask
        xmin, ymin, xmax, ymax - pointers to limits of the subrectangle
        nei4 - 0 for 8 neighbors, 1 for 4

	Returns:
		None

***/

void _stdcall OutContFast ( BYTE *mem,
              DWORD xe, DWORD ye, DWORD pitch,
              DWORD xmin, DWORD ymin,
              DWORD xmax, DWORD ymax, int nei4)
{
	DWORD prprev, prev, curr, nxt;
	DWORD dmask, full = 0xffffffff;
	DWORD xcnt, ycnt, i, dwords, l, msize;
   DWORD xstrt, xfin, ystrt, yfin;
	BYTE *r;
   DWORD *rdw, *wdw, *rpdw, *origdw;

   dwords = (xe + 31) >> 5;
   dmask = (DWORD)(full >> ((32 - (xe & 31)) & 31));
    // get memory for 2 rows:
   msize = 2 * dwords * sizeof(DWORD);
   if (msize > Work4KSize) rpdw = (DWORD *)malloc(msize);
   else rpdw = (DWORD *)Work4K;
   origdw = rpdw + dwords;
   for (i = 0;  i < dwords;  i++)
      rpdw[i] = origdw[i] = 0;
   if (xmax >= xe) xmax = xe - 1;
   xstrt = xmin >> 5;    xfin = xmax >> 5;
   ystrt = ymin;
   if ((yfin = ymax) >= ye) yfin = ye - 1;
	ycnt = ystrt;    r = mem + ystrt * pitch;
   wdw = rpdw;
	while (ycnt++ <= yfin)
	{
		xcnt = xstrt;  curr = 0;
      rdw = (DWORD *)r;  nxt = rdw[xcnt];
		while ((i = xcnt++) <= xfin)
		{
         prev = (curr >> 31);
         prprev = origdw[i];  curr = origdw[i] = nxt;
			if (xcnt < dwords) prev |= ((nxt = rdw[xcnt]) << 31);
         else { nxt = 0;  curr &= dmask; }
         l = (curr | prev | (curr << 1) | (curr >> 1));
			if (xcnt == dwords) l &= dmask;
         rdw[i] = rpdw[i] | l; // current row
         if (nei4) l = curr;
         wdw[i] = (wdw[i] | l) ^ prprev;	// correct previous row
         rpdw[i] = l;	// for next pass
      }
      wdw = (DWORD *)r;
      r += pitch;
   }
   for (i = 0;  i < dwords;  i++) wdw[i] ^= origdw[i];
   if (msize > Work4KSize) free(rpdw);
   return;
}


/************************************************** PictHistXarray */
/***
	Function:	PictHistXarray
	Description:
		This function calculates histogram of DWORD pixel values
        and sum of pixel values. Xarray must be sorted in ascending
        order. For i=0, ... XHlen - 1:
        	Hist[i] = number of pixel values:
              { Xarray[i-1] < Pix <= Xarray[i] }
            where  Xarray[-1] = -1
	Statics/Globals:
		None
	Input Arguments:
		Pmem - pointer to the source Rectangle
		Xe - number of pixels (bytes) in each row
		Ye - number of rows
		Pitch - address difference between neighboring rows (in bytes)
				in the source Rectangle
        Xarray - address of borders between "range cells"
        XHlen - length of Xarray, Hist arrays
	Output Arguments:
		hist - pointer to the array of longs with histogram values,
				at least XHlen entries.
				hist[i] = number of pixels with value 'i', i=0-255

	Returns:
		Sum of pixel values
****/
DWORD _stdcall PictHistXarray (
			DWORD *Pmem, DWORD Xe, DWORD Ye, DWORD Pitch,
            DWORD *Xarray, DWORD *Hist, DWORD XHlen)
{
	DWORD ih, ix, iy, pval, il, ir, k, *dwmem, summa = 0L;
    BYTE *bmem;

	for (ih = 0;  ih < XHlen;  ih++) Hist[ih] = 0;
    bmem = (BYTE *)Pmem;
	for (iy = 0;  iy < Ye;  iy++)
	{
    	dwmem = (DWORD *)bmem;
        for (ix = 0;  ix < Xe;  ix++)
        {
            pval = dwmem[ix];  summa += pval;
            il = 0;  ir = XHlen - 1;  k = 2 * XHlen;
            while ((il <= ir) && (k /= 2))
            {
            	ih = (il + ir) / 2;
                if (pval == Xarray[ih]) break;
                if (pval > Xarray[ih])
                {
                	if ((il = ih + 1) >= XHlen) { ih = XHlen - 1;  break; }
                    if (pval <= Xarray[il]) { ih = il;  break; }
                    continue;
                }
                // here (pval < Xarray[ih])
                if (ih == 0) break;
                ir = ih - 1;
                if (pval > Xarray[ir]) break;
            }
            Hist[ih]++;
        }
		bmem += Pitch;
	}
	return summa;
}


/************************************************** PictThreshStat */
/***
	Function:	PictThreshStat

	Description:
		This function calculates some statistics for pixels
      with values greater or equal to given threshold
      (the value must be inverted if parameter 'invert' is not zero):
      - number of pixels (N)
      - average pixel value (Av)
      - gravity center (Gx, Gy)
      - 2-nd central moments (Dx, Dy, Cov)
      - sum of pixels for whole picture

	Statics/Globals:
		None

	Input Arguments:
		Pmem - pointer to the source Rectangle

		Xe - number of pixels (bytes) in each row

		Ye - number of rows

		Pitch - address difference between neighboring rows (in bytes)
				in the source Rectangle

      threshold - only the pixels which values are greater or equal
      		to this parameter participate in calculations

      invert - if not zero then the pixel value must be inverted
      		before calculations

	Output Arguments:

		stat - pointer to the array of doubles where to put the results
				at least 12 entries (P means 'pixel value'):
				stat[0] = N   	: number of pixels which met the condition
				stat[1] = Av	: Sum(P) / N
				stat[2] = Gx	: Sum(P * X) / Sum(P)
				stat[3] = Gy	: Sum(P * Y) / Sum(P)
				stat[4] = Dx	: Sum(P * (X-Gx)*(X-Gx)) / Sum(P)
				stat[5] = Dy	: Sum(P * (Y-Gy)*(Y-Gy)) / Sum(P)
				stat[6] = Cov	: Sum(P * (X-Gx)*(Y-Gy)) / Sum(P)
				stat[7] = AllP	: Sum of values for all pixels in the picture
   			stat[8] = Dmax : Dispersion along the longest main axis
		      stat[9] = Dmin : Dispersion along the shortest main axis
      		stat[10] = Acos : COS and SIN for the angle of X-axis
		      stat[11] = Asin :  and longest main axis
				stat[12] = Dp : Dispersion of pixel values

	Returns:
		number of pixels which met the condition
****/
DWORD _stdcall PictThreshStat (
				BYTE *Pict, DWORD Xe, DWORD Ye, DWORD Pitch,
               	DWORD threshold, DWORD invert, double *stat)
{
	DWORD X, Y, summa;
	DWORD N, PV, PPL;
    double Nd, dwrk, PX, PXX, Av, SP, Gx, Gy, Dx, Dy, Cov, AllP, PP;
    double T, D, RR, R, Dmax, Dmin, Acos, Asin, Dp;
	WORD P, inv;
	BYTE *pixels;

    if (invert) inv = 255;  else inv = 0;
    N = 0L;
    SP = Gx = Gy = 0.0;
    Dx = Dy = Cov = AllP = PP = 0.0;
 	 for (Y = 0;  Y < Ye;  Y++)
	 {
			pixels = Pict;  Pict += Pitch;
  			PV = summa = PPL = 0L;  PX = PXX = 0.0;
			for (X = 0;  X < Xe;  X++)
      	{
      	 	 P = (WORD)((*pixels++) ^ inv);
          	 if ((DWORD)P >= threshold)
             {
             		N++;				// number of pixels in picture
              		PV += P;        	// sum of pix values in row
               	PPL += ((DWORD)P * P);
                	dwrk = (double)(P * X);
              		PX += dwrk; 		// sum of (P * X) in row
              		PXX += (dwrk * X);  // sum of (P * X * X) in row
          	  }
          	  summa += P;				// sum of all pix values in row
      	}
         dwrk = (double)PV;
      	SP += dwrk;				// sum of pix values in picture
      	Gx += PX;				// sum of (P * X) in picture
      	Gy += (dwrk * Y);		// sum of (P * Y) in picture
      	Dx += PXX;				// sum of (P * X * X) in picture
      	Dy += (dwrk * Y * Y); 	// sum of (P * Y * Y) in picture
      	Cov += (PX * Y);		// sum of (P * X * Y) in picture
      	AllP += (double)summa;	// sum of all pix values in picture
         PP += (double)PPL;
	 }
    if (SP != 0.0)
    {	// and for sure (N != 0)
   		Nd = (double)N;
   		Av = SP / Nd;
   		Dp = PP / Nd - Av * Av;
   		Gx /= SP;	Gy /= SP;
   		Dx = Dx / SP - Gx * Gx;
   		Dy = Dy / SP - Gy * Gy;
   		Cov = Cov / SP - Gx * Gy;
      	T = (Dx + Dy) / 2;  D = (Dx - Dy) / 2;
      	RR = D * D + Cov * Cov;
      	if (RR > 0.0)
      	{
       		R = sqrt(RR);
         	Acos = D + R;  Asin = Cov;	// for doubled angle (proportional)
         	RR = sqrt(Acos * Acos + Asin * Asin);
            if (RR > 0.0)
            {
         		Acos /= RR;    Asin /= RR;	// for longest main axis angle
            } else
            {
         		Acos = 0.0;	   Asin = 1.0;
            }
      	} else
      	{
       		R = 0.0;
         	Acos = 1.0;    	Asin = 0.0;
      	}
      	Dmax = T + R;   Dmin = T - R;
   		stat[0] = Nd;  	// number of pixels which met the condition
   		stat[1] = Av;	// Sum(P) / N
   		stat[2] = Gx;	// Sum(P * X) / Sum(P)
   		stat[3] = Gy;	// Sum(P * Y) / Sum(P)
   		stat[4] = Dx;	// Sum(P * (X-Gx)*(X-Gx)) / Sum(P)
   		stat[5] = Dy;	// Sum(P * (Y-Gy)*(Y-Gy)) / Sum(P)
   		stat[6] = Cov;	// Sum(P * (X-Gx)*(Y-Gy)) / Sum(P)
   		stat[7] = AllP;	// Sum of values for all pixels in the picture
   		stat[8] = Dmax; // Dispersion along the longest main axis
      	stat[9] = Dmin;	// Dispersion along the shortest main axis
      	stat[10] = Acos; // COS and SIN for the angle of X-axis
      	stat[11] = Asin; //  and longest main axis
      	stat[12] = Dp; //  Dispersion of pixel values
    }
	 return N;
}


/****************************************************** Projection */
/***
	Function:	Projection

	Description:
		This function calculates a row (rc=0) or column (rc!=0) projection
		of a rectangle. Result must have the size of the corresponding
		side of the rectangle

			rectangle				    |  column projection
											 |  (for operation "SUM")
		____________________________|______________________
		a(0,0)       ...   a(0,n)   | Sum(a(0,i)), i=0...n
		 ...         ...     ...    | ...
		a(k,0)       ...   a(k,n)   | Sum(a(k,i)), i=0...n
		____________________________|______________________
											 |
		Min(a(i,0))  ... Min(a(i,n))|   row projection
		i=0...k          i=0...k    | (for operation "MIN")

	Statics/Globals:
		None

	Input Arguments:
		prect - pointer to rectangle in memory

		prowcol - pointer to resulting memory (row or column)

		xe - number of bytes in each row of the rectangle

		ye - number of rows of the rectangle

		rc - make row (0) or column (1) projection

		oper - defines the projection operation:
			prjMIN 	  =16    // MIN in row/column
			prjMAX 	  =17    // MAX in row/column
			prjSUM 	  =18    // SUM of bytes in row/column

	Returns
		for operation	returned value
		prjMIN 			Min of byte values of the whole rectangle
		prjMAX 			Max of byte values of the whole rectangle
		prjSUM 			Sum of byte values of the whole rectangle

***/
DWORD _stdcall Projection (
				BYTE *rect, BYTE *rowcol,
				DWORD xe, DWORD ye, short rc, short oper)
{
	DWORD i, k, *sum, dd, cc;
	WORD b, c;
	BYTE *mima;

	if (rc) goto Column;
// Row:
	switch (oper)
	{
		case prjMIN:
			mima = (BYTE *)rowcol;  dd = 255;  i = k = 0;
			while (i++ < xe) *mima++ = 255;
			while (k++ < ye)
			{
				mima = (BYTE *)rowcol;	i = 0;
				while (i++ < xe)
				{
					if ((b = *rect++) < *mima)
					{
						*mima = b;   // set to min
						if ((DWORD)b < dd)	dd = b;
					}
					mima++;
				}
			}
			break;
		case prjMAX:
			mima = (BYTE *)rowcol;  dd = 0;  i = k = 0;
			while (i++ < xe) *mima++ = 0;
			while (k++ < ye)
			{
				mima = (BYTE *)rowcol;	i = 0;
				while (i++ < xe)
				{
					if ((b = *rect++) > *mima)
					{
						*mima = b;   // set to max
						if ((DWORD)b > dd)	dd = b;
					}
					mima++;
				}
			}
			break;
		case prjSUM:
			sum = (DWORD *)rowcol;   dd = 0;  i = k = 0;
			while (i++ < xe) *sum++ = 0;
			while (k++ < ye)
			{
				sum = (DWORD *)rowcol;   i = 0;
				while (i++ < xe)
				{
					dd += (b = *rect++);
					*sum++ += b;
				}
			}
			break;

		default:  ;   // rest - 1st, nothing to do
	}
	goto Fin;

Column:
	switch (oper)
	{
		case prjMIN:
			mima = (BYTE *)rowcol;  dd = 255;  k = 0;
			while (k++ < ye)
			{
				i = 0;   c = 255;
				while (i++ < xe)
				{
					if ((b = *rect++) < c) c = b;   // set to min
				}
				*mima++ = c;
				if ((DWORD)c < dd)	dd = c;
			}
			break;
		case prjMAX:
			mima = (BYTE *)rowcol;  dd = k = 0;
			while (k++ < ye)
			{
				i = 0;   c = 0;
				while (i++ < xe)
				{
					if ((b = *rect++) > c) c = b;   // set to max
				}
				*mima++ = c;
				if ((DWORD)c > dd)	dd = c;
			}
			break;
		case prjSUM:
			sum = (DWORD *)rowcol;   dd = k = 0;
			while (k++ < ye)
			{
				i = cc = 0;
				while (i++ < xe) cc += (DWORD)(*rect++);
				*sum++ = cc;  dd += cc;
			}
			break;

		default:  ;   // rest - 1st, nothing to do
	}

Fin:
	return dd;
}


/**************************************************** Projection32 */
DWORD _stdcall Projection32 (
				void *rect, DWORD *rowcol,
				DWORD xe, DWORD ye, DWORD pitch,
                long rc, long oper)
{
	DWORD pitchv, pitch0, nv, n0, minv, maxv, sumv,	ret, i0;
    BYTE *v;

    if (rc)
    { 	// column
     	pitchv = sizeof(DWORD);   nv = xe;
        pitch0 = pitch;		n0 = ye;
    } else
    {	// row
     	pitch0 = sizeof(DWORD);   n0 = xe;
        pitchv = pitch;     nv = ye;
    }
    v = (BYTE *)rect;  ret = 0;
    for (i0 = 0;  i0 < n0;  i0++)
    {
     	VectorInfo (v, pitchv, nv, 5, &minv, &maxv, &sumv, NULL, NULL);
		switch (oper)
		{
			case prjMIN:
            	rowcol[i0] = minv;
                if ((!i0) || (ret > minv)) ret = minv;
                break;
			case prjMAX:
            	rowcol[i0] = maxv;
                if ((!i0) || (ret < maxv)) ret = maxv;
                break;
			case prjSUM:
            	rowcol[i0] = sumv;
                ret += sumv;
                break;
			default:  ;   // nothing to do
        }
        v += pitch0;
    }
	return ret;
}


/***************************************************** ReplicateMem */
/***
	Function:	ReplicateMem

	Description:
		This function copies memory buffer into another location
		with replication of each pixel the given number of times;
		allowed pixel size may be 1 or 8

	Statics/Globals:
		None

	Input Arguments:
		from - pointer to the source buffer

		pix - pixel size (in bits), may be only 1 or 8

		xe - number of pixels in the buffer

		to - pointer to the target buffer
			(must be at least rep times bigger than the source buffer)

		rep - how many times to replicate each pixel

	Output Arguments:
		None

	Returns:
		0 if OK, -1 otherwise

****/
short _stdcall ReplicateMem (
				BYTE *from, DWORD pix, DWORD xe,
				BYTE *to, DWORD rep)
{
	BYTE c, *r, *w;
	DWORD k, cnt, bitnum;
    WORD accum, one, mask;

    r = from;  w = to;
	if (pix == 8)
	{
      	k = xe;
		while (k--)
		{
			c = *r++;  cnt = rep;
			while (cnt--) *w++ = c;
		}
		return 0;
	}
	if (pix == 1)
	{
		bitnum = 0;   accum = 0;   one = 1;
		while (bitnum++ < xe)
		{

			if ((bitnum & 7) == 1)	c = *r++;
            cnt = mask = 0;
            if (c & 1) mask = 0xff;
			while (cnt++ < rep)
			{
				accum |= (one & mask);
				if ((one <<= 1) == 0x100)
				{
					*w++ = accum;
					accum = 0;  one = 1;
				}
			}
			c >>= 1;
		}
		if (one != 1) *w = accum;
		return 0;
	}
	return -1;
}


/******************************************************* RGBtoGray */
/****
	Transform 24 bit pixels into 8 according to color coefficients
   (in range from 0 to 255). Let
	     sum = red + green + blue
   If (sum > 0) then
     pix = (r * red + g * green + b * blue) / sum;
   Else
     if (sum == 0):  pix = MIN (r, g, b)
     if (sum == -1): pix = MEDIAN (r, g, b)
     if (sum == -2): pix = MAX (r, g, b)
     if (sum <= -3): pix = (r + g + b + 2) / 3
   Returns: nothing
****/
void _stdcall RGBtoGray (
			BYTE *mem8, DWORD xe, DWORD ye, DWORD pitch8,
			BYTE *mem24, DWORD pitch24,
			long red, long green, long blue)
{
	DWORD xcnt, ycnt, pix, r, g, b,
    		ind = 3, ord[4];
	BYTE *rd, *wr, *rdrow, *wrrow;
    long sum;

	wr = mem8;  rd = mem24;  ycnt = ye;
    sum = red + green + blue;
    // MAX by default
    if (sum == 0)  ind = 0;	// MIN
 	if (sum == -1) ind = 1; // MEDIAN
 	if (sum == -2) ind = 2; // MAX

	while (ycnt--)
	{  // rows loop
   		rdrow = rd;		wrrow = wr;		xcnt = xe;
		while (xcnt--)
		{  // make 8-bit row
			b = (DWORD)*rdrow++;  // this order!
            g = (DWORD)*rdrow++;
            r = (DWORD)*rdrow++;
            if (sum > 0)
            	pix = (r * red + g * green + b * blue) / sum;
            else
            {
             	if (r > g) { ord[0] = g;  ord[2] = r; }
                else { ord[0] = r;  ord[2] = g; }
             	if (b > ord[2]) { ord[1] = ord[2];  ord[2] = b; }
                else
                {
                	if (b < ord[0]) { ord[1] = ord[0];  ord[0] = b; }
                    else { ord[1] = b; }
                }
                ord[3] = (r + g + b + 2) / 3;
                pix = ord[ind];
            }
            *wrrow++ = (BYTE)pix;
      	}
      	wr += pitch8;	rd += pitch24;
	}
}


/**************************************************** ShiftBitMask */
/***
	Function:	ShiftBitMask

	Description:
		This function transforms the Bit Mask according to
		BIT shift, given by value (0 to 7) and direction

	Statics/Globals:
		None

	Input Arguments:
		pBitMem1 - pointer to the source bit mask

		Xe - number of pixels (bits) in each row

		Ye - number of rows

		Pitch1 - address difference between neighboring rows (in bytes)
					in source bit mask
		pBitMem2 - pointer to the target bit mask

		Pitch2 - address difference between neighboring rows (in bytes)
					in target bit mask

		shift - is given in bits, accepted values are 0 to 7;

		lr - left (0) / right (1) shift in memory

	Returns
		None

***/
void _stdcall ShiftBitMask (
				BYTE *BitMem1,
                DWORD xee, DWORD yee, DWORD Pitch1,
				BYTE *BitMem2, DWORD Pitch2,
                WORD shift, short lr)
{
	DWORD xcnt, xe, ye;
	WORD curr;
    BYTE lastAND, lastOR;
	BYTE *BitRowR, *BegBitRowR, *BitRowW, *BegBitRowW;

    BegBitRowR = BitMem1;  BegBitRowW = BitMem2;
    xe = xee;  shift &= 7;
	if ((lr) && (shift)) goto Rshift;
Lshift:
    lastOR = (BYTE)(0xff << (xe & 7));
    lastAND = (BYTE)(0xff ^ lastOR);
    ye = yee;
	while (ye--)
	{
		BitRowR = BegBitRowR;  BitRowW = BegBitRowW;
        xcnt = xe;
		while (xcnt)
		{
			if (xcnt > 7)
            { 	// send 8 bits
            	curr = *(WORD *)BitRowR;
                curr >>= shift;
                xcnt -= 8;
			} else
			{	// less then 8 bits
				if (xcnt + shift > 8) curr = (*(WORD *)BitRowR);
				else curr = *BitRowR;
                curr = (BYTE)((curr >> shift) & lastAND);
                curr |= ((*BitRowW) & lastOR);
				xcnt = 0;
			}
			*BitRowW++ = (BYTE)curr;
			BitRowR++;
		}
		BegBitRowR += Pitch1;  BegBitRowW += Pitch2;
	}
	return;
Rshift:
    // here shift > 0
    lastAND = (BYTE)(0xff << shift);
    if (shift + xe < 8)
    	lastAND &= (BYTE)((0xff >> (8 - shift - xe)));
    lastOR = (BYTE)(0xff ^ lastAND);
    ye = yee;
	BitRowR = BegBitRowR;  BitRowW = BegBitRowW;
    // send 1st column:
	while (ye--)
	{
    	*BitRowW = (BYTE)((*BitRowW & lastOR) |
        			((*BitRowR << shift) & lastAND));
		BitRowR += Pitch1;  BitRowW += Pitch2;
	}
    if (shift + xe > 8)
    {
		shift = (WORD)(8 - shift); // left shift
        xe -= shift;   // remaining bits
        BegBitRowW++;  // start to write from next column
     	goto Lshift;
    }
}


/**************************************************** ZoomRectFast */
/***
	Function:	ZoomRectFast

	Description:
		This function moves (replicates) bytes from one
		rectangle to another, which may be of different size

	Statics/Globals:
		None

	Arguments:
		pict1 - pointer to the source rectangle
		xe1 - size of the row in source rectangle
		ye1 - number of rows in source rectangle
		pitch1 - address difference between neighboring
				rows for source rectangle
		pict2 - pointer to the destination rectangle
		xe2 - size of the row in destination rectangle
		ye2 - number of rows in destination rectangle
		pitch2 - address difference between neighboring
				rows for destination rectangle
        portion - bytes per pixel count

	Returns:
		0 if not Ok,
		ye2 otherwise

***/
DWORD _stdcall ZoomRectFast (
			BYTE *pict1, DWORD xe1, DWORD ye1, DWORD pitch1,
			BYTE *pict2, DWORD xe2, DWORD ye2, DWORD pitch2, DWORD portion)
{
	DWORD cnt, scnt, bcnt, blim, slim, bpt, spt, bxe, sxe, ret = 0;
	BYTE *big, *small, *wrk;

	if ((ye1 < 2) || (ye2 < 2) || (xe1 < 2) || (xe2 < 2) || (portion < 1))
    	goto Fin;
	ret = ye2;
	if (ye1 > ye2) goto BigToSmall;
// Small Height To Big One:
	big   = pict2; 	blim = ye2;	 bxe = xe2;  bpt = pitch2;
	small = pict1; 	slim = ye1;	 sxe = xe1;  spt = pitch1;
	bcnt = 0;	cnt = blim + 1;
	while (bcnt++ < blim)
	{
		if (cnt > blim)
		{
			cnt -= blim;
			wrk = small;
			small += spt;
		}
		RowToRow(wrk, sxe, big, bxe, portion);
		big += bpt;
		cnt += slim;
	}
	goto Fin;

BigToSmall:
		// Big Height To Small One
	big   = pict1; 	blim = ye1;  bxe = xe1;  bpt = pitch1;
	small = pict2; 	slim = ye2;  sxe = xe2;  spt = pitch2;
	scnt = 0;	cnt = blim / 2;
	while (scnt++ < slim)
	{
		bcnt = cnt / slim;
		wrk = big;  wrk += (bcnt * bpt);
		RowToRow(wrk, bxe, small, sxe, portion);
		small += spt;
		cnt += blim;
	}
Fin:
	return ret;
}


/************************************************** ZoomRectSmooth */
/***
	Function:	ZoomRectSmooth

	Description:
		This function interpolates bytes from one
		rectangle to another, which may be of different size

	Statics/Globals:
		None

	Arguments:
		pict1 - pointer to the source rectangle
		xe1 - size of the row in source rectangle
		ye1 - number of rows in source rectangle
		pitch1 - address difference between neighboring
				rows for source rectangle
		pict2 - pointer to the destination rectangle
		xe2 - size of the row in destination rectangle
		ye2 - number of rows in destination rectangle
		pitch2 - address difference between neighboring
				rows for destination rectangle

	Returns:
		0 if not Ok,
		ye2 otherwise

***/
DWORD _stdcall ZoomRectSmooth (
			BYTE *pict1, DWORD xe1, DWORD ye1, DWORD pitch1,
			BYTE *pict2, DWORD xe2, DWORD ye2, DWORD pitch2)
{
	long cnt, scnt, bcnt, blim, slim, bpt, spt,
		  n, nn, k, i,
		  wnew, wold, bxe, sxe, ret = 0;
	BYTE *big, *small, *wrk;

	if ((ye1 < 2) || (ye2 < 2) || (xe1 < 2) || (xe2 < 2))
    	goto Fin;
	ret = ye2;	wnew = 1;
	if (ye1 > ye2) goto BigToSmall;
// Small Height To Big One:
	big   = pict2; 	blim = ye2;	 bxe = xe2;  bpt = pitch2;
	small = pict1; 	slim = ye1;	 sxe = xe1;  spt = pitch1;
	bcnt = 0;	cnt = blim + 1;		n = 1;
	while (bcnt++ < blim)
	{
		if (cnt > blim)
		{
			cnt -= blim;
			InterpolRow(small, sxe, NULL, big, bxe, 1, 0);
			wrk = big;
			if ((n++) < slim) small += spt;
		} else
		{
			wnew = cnt;  wold = blim - cnt;
			InterpolRow(small, sxe, wrk, big, bxe, wnew, wold);
		}
		big += bpt;
		cnt += slim;
	}
	goto Fin;

BigToSmall:		// Big Height To Small One:
	big   = pict1; 	blim = ye1;  bxe = xe1;  bpt = pitch1;
	small = pict2; 	slim = ye2;  sxe = xe2;  spt = pitch2;
	scnt = 0;	cnt = blim / 2;
	nn = (blim / slim) + 1;	 n = nn / 2;
	while (scnt++ < slim)
	{
		bcnt = cnt / slim;
		i = bcnt - n;  	if (i < 0) i = 0;
		k = i + nn;		if (k > blim) k = blim;
		wold = 0;
		wrk = big;  	wrk += (i * bpt);
		while (i++ < k)
		{
			InterpolRow(wrk, bxe, small, small, sxe, wnew, wold);
			wold++;
			wrk += bpt;
		}
		small += spt;
		cnt += blim;
	}
Fin:
	return ret;
}


/**************************************************** MemToBitMask32 */
/***
	Function:	MemToBitMask32

	Description:
		This function calculates the Bit Mask on the basis of the contents
		of the given DWORD rectangle, the threshold and the operation:
		Oper (ByteMem, threshold) => BitMem

	Statics/Globals:
		None

	Input Arguments:
		DwordMem - pointer to byte rectangle (pixel size = 32)
		Xe - number of pixels (dwords) in each row
		Ye - number of rows
		DwordPitch - address difference between neighboring rows
					(may be other than Xe)
		BitMem - pointer to bit mask (pixel size = 1)
		BitPitch - address difference between neighboring rows in
					the bit mask; is given in BYTES
		threshold - argument of operation
		oper - defines the operation:
			mtbmGE  =16   less-than-threshold bytes => 0
							  greater-than or equal-to-threshold bytes => 1
			mtbmBE  =17   greater-than-threshold bytes => 0
							  less-than or equal-to-threshold bytes => 1
			mtbmEQ  =18   equal-to-threshold bytes => 1, others => 0
			mtbmNE  =19   equal-to-threshold bytes => 0, others => 1

	Returns
		number of 1-bits in the resulting BitMem
***/
DWORD _stdcall MemToBitMask32 (
				void *Mem, DWORD Xe, DWORD Ye, DWORD Pitch,
				void *BitMem, DWORD BitPitch, DWORD threshold, DWORD oper)
{
	DWORD b, c, one, xcnt, d = 0, *BitRow, *DWrow;
	BYTE *BegBitRow, *BegDWrow;

	BegBitRow = (BYTE *)BitMem;  BegDWrow = (BYTE *)Mem;
	while (Ye--)
	{
		BitRow = (DWORD *)BegBitRow;
        DWrow = (DWORD *)BegDWrow;
		xcnt = 0;  b = 0;  one = 1;
		while (xcnt < Xe)
		{
			c = DWrow[xcnt++];
            switch (oper)
			{
  				case mtbmGE:
					if (c >= threshold) { b |= one;  d++; }
  					break;
  				case mtbmBE:
					if (c <= threshold) { b |= one;  d++; }
  					break;
  				case mtbmEQ:
					if (c == threshold) { b |= one;  d++; }
  					break;
  				case mtbmNE:
					if (c != threshold) { b |= one;  d++; }
  					break;
                default: ;
			}
			if ((one <<= 1) == 0)
			{
				*BitRow++ = b;  b = 0;  one = 1;
			}
		}
		if (--one)
      	{
         	c = *BitRow;
         	*BitRow = ((one & c) ^ c) | b;
      	}
		BegBitRow += BitPitch;  BegDWrow += Pitch;
	}
	return d;
}


/************************************************************ MemToValue */
/***
	Function:	MemToValue

	Description:
		This function calculates 5 numerical functions of
		the given buffer

	Statics/Globals:
		None

	Input Arguments:
		mem  - pointer to argument memory buffer

		size - size of the buffer (bytes)

		oper - defines which function to calculate

			mtvMIN 	=16     MIN of byte values
			mtvMAX 	=17     MAX of byte values
			mtvSUM 	=18     SUM of byte values
			mtvONES =19     number of ONE bits
			mtvZEROS =20     number of ZERO bits

	Returns:
		for mtvMIN   : MIN of byte values
		for mtvMAX   : MAX of byte values
		for mtvSUM   : SUM of byte values
		for mtvONES  : number of ONE bits
		for mtvZEROS : number of ZERO bits

***/
DWORD _stdcall MemToValue (BYTE *mem, DWORD size, short oper)
{
	WORD b, m;
	DWORD d = 0;
	switch (oper)
	{
		case mtvMIN:
			m = 255;
			while (size--) if ((b = *mem++) < m) m = b;   // set to min
			d = m;	break;
		case mtvMAX:
			m = 0;
			while (size--) if ((b = *mem++) > m) m = b;   // set to max
			d = m;	break;
		case mtvSUM:
			while (size--) d += *mem++;   // sum
			break;
		case mtvONES:
			while (size--)
			{
				b = *mem++;
				d += BitsInByte[b];
			}
			break;
		case mtvZEROS:
			while (size--)
			{
				b = (WORD)(*mem++ ^ 0xff);
				d += BitsInByte[b];
			}
			break;

		default:  ;   // rest - 1st, nothing to do
	}
	return d;
}


/************************************************* MinMaxDirection */
/*  a11 a01
    a10 a00
	a00 = operation2 (operation1 (a00, a10, a01, a11), bb);
   operation1:  (oper & 1) : 0/1 = MIN/MAX;
   operation2:  (oper & 2) : 0/2 = MIN/MAX;
   direction bits:   0/1 - from left/right,  0/2 - from top/bottom:
   	0 -  TopLeft_to_BottomRight
    3 - BottomRight_to_TopLeft
    1 - TopRight_to_BottomLeft
    2 - BottomLeft_to_TopRight

*/
void _stdcall  MinMaxDirection (
			BYTE *aa, long pitcha,
			BYTE *bb, long pitchb,
		    long xe, long ye,
		    long oper, long direct)
{
	BYTE *a0, *a1, *b, c0, c1;
	long nx, op1, op2, xe1, ye1;

   	op1 = oper & 1;  op2 = oper & 2;
   	xe1 = xe - 1;  ye1 = ye - 1;
   	if (direct & 2)
   	{
    	aa += (pitcha * ye1);
      	bb += (pitchb * ye1);
   	}
   	if (direct & 1) {  aa += xe1;   bb += xe1; }
   	a1 = aa;
   	while (ye--)
   	{
    	a0 = aa;  b = bb;  c0 = *a0;  c1 = *a1;
      	c0 = MinMax (c0, c1, op1);
      	c0 = MinMax (c0, *b, op2);
        *a0 = c0;   nx = xe1;
      	while (nx--)
      	{
	 		if (direct & 1) {  a0--;   a1--;   b--; }
	 		else {  a0++;   a1++;   b++; }
	 		c0 = MinMax (c0, c1, op1);    c1 = *a1;
	 		c0 = MinMax (c0, c1, op1);
	 		c0 = MinMax (c0, *a0, op1);
	 		c0 = MinMax (c0, *b, op2);
            *a0 = c0;
      	}
      	a1 = aa;
      	if (direct & 2) {  aa -= pitcha;  bb -= pitchb; }
      	else {  aa += pitcha;  bb += pitchb; }
   	}
}





















