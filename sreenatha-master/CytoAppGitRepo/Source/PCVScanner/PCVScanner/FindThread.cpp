// FindThread.cpp : implementation file
//
// MODS
//
//	26Jun02	SN	Added objective name to fragment measurements uploaded to database.
//

#include "stdafx.h"
#include "aiishared.h"
#include "threaddefs.h"
#include "ScriptParams.h"
#include "ScanPass.h"
#include "CMeasurement.h"
#include "ScanPassList.h"
#include "SpectraChrome.h"
#include "FindThread.h"
#include "SlideList.h"
#include "FrameReader.h"

#include "UTS.h"
#include "Slide.h"		
#include "AIQueue.h"		
#include "ScriptProc.h"
#include "CSyncObjects.h"
#include "ScanData.h"

//#include "Capable.h"
#include <direct.h>
#include "ScriptDirective.h"
#include "CommonDefs.h"
#include <ProcessorCount.h>
#include <ImageBits.h>
#include "..\..\Common\Src\NLog\src\NLogC\NLogC.h"

#define MAX_COMP_NAMELEN	256

#ifdef _DEBUG
#include "HRTimer.h"
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const int FRAG_BPP = 8;


/////////////////////////////////////////////////////////////////////////////

typedef struct
{
	CRect Rect;
	CString GUID;
} RectInfo;

typedef std::list<RectInfo> Rectangles;

int GetIntValue(Measurement *pMeas)
{
    switch (pMeas->m_Value.vt)
    {
        case VT_I4:
            return pMeas->m_Value.lVal;
        case VT_R8:
            return (int) pMeas->m_Value.dblVal;
    }

    return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CFindThread

CFindThread::CFindThread()
{
	m_hFindThread = NULL;
}

CFindThread::~CFindThread()
{
}


/// NOTE THE FOLLOWING 2 FUNCTIONS ARE ESSENTIALLY EQUIVALENT TO THE PCV VERSIONS
/// Not ideal - need to refactor PCV to use these
BYTE *ScaleTopEndByValue(ImageData &ID)
{
	float max, min;
	double maxPixelValue = pow(2.0, ID.Bits);

	int peakLocation = 0;

	int endOfScale = (int)maxPixelValue;

	int *pHist = new int[endOfScale];

	memset(pHist, 0, endOfScale * sizeof(int));

	double tinyPercentage = 0.05 * ID.W * ID.H;

	WORD *pSrc = ID.pImageMem;
	for (int i = 0; i < ID.W * ID.H; i++)
	{
		pHist[*pSrc++]++;
	}

	int Total = 0;
	// NOTE start from one forgets any zero pixels
	for (int i = 1; i < maxPixelValue; i++)
	{
		Total += pHist[i];
		if (Total > tinyPercentage)
		{
			peakLocation = i;
			break;
		}
	}

	for (int i = peakLocation; i < maxPixelValue; i++)
	{
		if (pHist[i] > 0)
			endOfScale = i;

	}

	delete [] pHist;

	max = (float)endOfScale;
	min = (float)peakLocation;

	float range = max - min;
	if (range == 0.0) range = 1.0;		// Stop div by zero


	BYTE *pResult = new BYTE[ID.W * ID.H];

	BYTE *pDest = pResult;
	pSrc = ID.pImageMem;

	for (int i = 0; i < ID.W * ID.H; i++)
	{
		float B = 255.0f * ((float)(*pSrc++) - min) / range;
		if (B > 255.0f) B = 255.0f;
		if (B < 0.0f) B = 0.0f;
		*pDest++ = (BYTE)B;
	}

	return pResult;
}


BYTE *Aggressive(ImageData &ID)
{

	float max, min;
	double maxPixelValue = pow(2.0, ID.Bits);


	int peakLocation = 0;
	int peakValue = 0;
	int endOfScale = (int)maxPixelValue;

	int *pHist = new int[endOfScale];

	memset(pHist, 0, endOfScale * sizeof(int));

	WORD *pSrc = ID.pImageMem;
	for (int i = 0; i < ID.W * ID.H; i++)
	{
		pHist[*pSrc++]++;
	}

	double tinyPercentage = 0.00005 * ID.W * ID.H;

	for (int i = 0; i < maxPixelValue; i++)
	{
		int bin = pHist[i];

		if (bin > peakValue)
		{
			peakValue = bin;
			peakLocation = i;
		}
	}

	int Total = 0;
	for (int i = (int)maxPixelValue - 1; i >= 0; i--)
	{
		Total += pHist[i];
		if (Total >= tinyPercentage)
		{
			endOfScale = i;
			break;
		}
	}

	if (endOfScale < peakLocation + 128)
		endOfScale = peakLocation + 128;

	peakLocation += 20;
	
	delete [] pHist;

	max = (float)endOfScale;
	min = (float)peakLocation;

	float range = max - min;
	if (range == 0.0) range = 1.0;		// Stop div by zero

	BYTE *pResult = new BYTE[ID.W * ID.H];

	BYTE *pDest = pResult;
	pSrc = ID.pImageMem;

	for (int i = 0; i < ID.W * ID.H; i++)
	{
		float B = 255.0f * ((float)(*pSrc++) - min) / range;
		if (B > 255.0f) B = 255.0f;
		if (B < 0.0f) B = 0.0f;
		*pDest++ = (BYTE)B;
	}

	return pResult;
}

BYTE *HistogramStretchToNBPP(ImageData &ID, int outputBitsPerPixel)
{
	BYTE *pResult = new BYTE[ ImageBits::GetImageSizeInBytes(ID.W, ID.H, outputBitsPerPixel) ];
	
	if (!ImageBits::HistogramStretchToNBPP(ID.pImageMem, ID.W, ID.H, ID.Bits, pResult, outputBitsPerPixel))
	{
		delete pResult;
		pResult = NULL;
	}

	return pResult;
}

BYTE *CopyImage( ImageData &ID)
{
	int sizeOfImage = ImageBits::GetImageSizeInBytes(ID.W, ID.H, ID.Bits);

	BYTE * pImage = new BYTE[sizeOfImage];
	memcpy( pImage, ID.pImageMem, sizeOfImage);

	return pImage;
}



class FragControl
{
	Rectangles *m_pRects;
	ImageFrame *m_pFrame;
	CString m_FileRoot;
	CSyncObjectSemaphore *pFragSem;
	CSpectraChromeList *pFluorlist;
	CString	m_RegionScoresPath;

public:
	FragControl(Rectangles *pRects, ImageFrame *pFrame, LPCTSTR FRoot, CSyncObjectSemaphore *pSem, CSpectraChromeList *pfluorlist, CString RegionScoresPath)
	{
		m_pRects = pRects;
		m_pFrame = pFrame;
		m_FileRoot = FRoot;
		pFragSem = pSem; 
		pFluorlist = pfluorlist;
		m_RegionScoresPath = RegionScoresPath;
	}

	~FragControl()
	{
		
		Rects().clear();
		delete m_pFrame;
		delete m_pRects;
	}

	CSyncObjectSemaphore &FragSem()
	{
		return *pFragSem;
	}

	Rectangles Rects() 
	{ 
		return *m_pRects; 
	}

	ImageFrame Frame()
	{
		return *m_pFrame;
	}

	CString &FileRoot()
	{
		return m_FileRoot;
	}

	CSpectraChromeList *FluorList()
	{
		return pFluorlist;
	}

	CString &ScoresPath()
	{
		return m_RegionScoresPath;
	}

};

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

static UINT ProcessFragmentData(FragControl *pFCB)
{

	if (pFCB->Rects().size())
	{				
		FileList ImageFiles;

		// Add frame components in fluorochrome order as defined by the assay, not capture order
		POSITION pos = pFCB->FluorList()->m_list.GetHeadPosition();
		int nComponents = pFCB->FluorList()->m_list.GetCount();

		TCHAR * header = new TCHAR[MAX_COMP_NAMELEN *nComponents];
		memset(header, 0, MAX_COMP_NAMELEN *nComponents);

		TCHAR * nextHeaderComponent = header;

		while (pos != NULL)
		{
			CSpectraChrome * fluor =  pFCB->FluorList()->m_list.GetNext(pos);
			// Load in the actual image data
			
			FrameComponent FC(_T(""), _T(""));
			pFCB->Frame().GetFrameComponent(fluor->m_Name, FC);

			_stprintf(nextHeaderComponent, _T("%s"), fluor->m_Name);
			nextHeaderComponent += MAX_COMP_NAMELEN;

			ImageData MaxProj;

			MaxProj.IsCounter = FC.IsCounterstain;
			MaxProj.IsMaxProj = TRUE;

			if (FC.ProjectionImage != _T(""))
			{									
				MaxProj.FileName = FC.ProjectionImage;
				MaxProj.LoadBlob(pFCB->FileRoot() + _T("\\") + MaxProj.FileName);

				ImageFiles.push_back(MaxProj);
			}

			// Stacks
			for each (ImageData ID in FC.ImageFiles)
			{
				ImageData X;

				X = ID;

				X.IsCounter = FC.IsCounterstain;
				X.LoadBlob(pFCB->FileRoot() + _T("\\") + ID.FileName);

				ImageFiles.push_back(X);
			}	

		}


		
		// Do the CONVERSION FIRST FOR SOME REASON THE CODE HAD BEEN CHANGED SO THAT
		// THE HISTOGRAM SCALING WAS DONE ON EVERY IMAGE FOR EVERY THUMBNAIL

		std::list<ImageData>::iterator FCIT;

		std::vector<ImageData*> elements; //my_element is whatever is in list
		for(FCIT = ImageFiles.begin(); FCIT != ImageFiles.end(); ++FCIT)
			elements.push_back(&(*FCIT));

#pragma omp parallel
		{
#pragma omp for
			for(size_t i = 0; i < elements.size(); ++i) // or use iterators in newer OpenMP
			{
				if (elements[i]->IsCounter)
					elements[i]->pConvertedMem = ScaleTopEndByValue(*elements[i]);
				else
					elements[i]->pConvertedMem = Aggressive(*elements[i]);
			}
		}

	

		// For each rectangle coming out of the analysis cut out and produce the fragment z stack data
		for each (RectInfo CR in pFCB->Rects())
		{				
			CFile BlbFile;
			CFileException FE;
			int x = 0, y = 0;
			CR.GUID.Remove(_T('{'));
			CR.GUID.Remove(_T('}'));

			if (BlbFile.Open(pFCB->FileRoot() + _T("\\") + CR.GUID + _T(".thumb.blb"), CFile::modeCreate | CFile::modeWrite, &FE))
			{
				unsigned int MagicNumber = 0xABCDEF12;
				int W = CR.Rect.Width();
				int H = CR.Rect.Height();

				BlbFile.Write(&MagicNumber, sizeof(unsigned int));
				BlbFile.Write(&W, sizeof(int));
				BlbFile.Write(&H, sizeof(int));
				BlbFile.Write(&FRAG_BPP, sizeof(int));
				BlbFile.Write(&nComponents, sizeof(int));
				BlbFile.Write(header, MAX_COMP_NAMELEN *nComponents);
				    
				CUTSMemRect MR;

				for each (ImageData ID in ImageFiles)
				{
					
					long H1 = MR.MakeExtern(ID.pConvertedMem, FRAG_BPP, ID.W, ID.H, 0);
					
					long D1 = MR.Create(FRAG_BPP, CR.Rect.Width(), CR.Rect.Height());
					long Xl, Yt, Duh;
					MR.CutImageFrag(H1, CR.Rect.left, CR.Rect.top, D1, &Xl, &Yt);
					
					void *FragAddr;

					MR.GetExtendedInfo(D1, &Duh, &Duh, &Duh, &FragAddr);
					BlbFile.Write(FragAddr, W * H);

					MR.FreeImage(H1);
					MR.FreeImage(D1);
				}
			
				MR.FreeAll();

				BlbFile.Close();
			}
		}
		
		// Clear out any loaded image data
		for each (FrameComponent FC in pFCB->Frame().m_Components)
		{
			for each (ImageData ID in FC.ImageFiles)
				ID.Release();
		}


		for each (ImageData ID in ImageFiles)
			ID.Release();

		delete header;
	}

	if (pFCB->ScoresPath() != _T(""))
	{
		CStdioFile CSF;
		CString Scores;

		Scores.Format(_T("%s\\HasScores.cvs"), pFCB->ScoresPath());

		if (CSF.Open(Scores, CFile::modeCreate | CFile::modeWrite))
		{
			CSF.Close();
		}
	}
		
	pFCB->FragSem().Release();

	delete pFCB;

	
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
// Take the output of the script processing and turn into a list of rectangles
/////////////////////////////////////////////////////////////////////////////////////////////////////

void BuildRectangleList(CScriptProcessor *VBProc, Rectangles &Rects)
{
	Measurement *pMeas;
	int ix = 0, iy = 0, iz = 0, i;
	CMeasures Meas;
	BOOL GotMeasurements;
	int Fragw = 0, Fragh = 0;

	i = 0;

	
	while (TRUE)
	{
		GotMeasurements = FALSE;
		// If there are measurements then there is a fragment to upload
		// Repeat until no more measurements from analysis
		while (VBProc->GetMeasurement(&pMeas) && pMeas->m_Name != _T("Quit"))
		{
			// Do not send X, Y, W or H measurements - these go with the frag
			if (pMeas->m_Name == _T("X"))
				ix = GetIntValue(pMeas);
			else
			if (pMeas->m_Name == _T("Y"))
				iy = GetIntValue(pMeas);
			else	
			if (pMeas->m_Name == _T("WIDTH"))
				Fragw = GetIntValue(pMeas);
			else
			if (pMeas->m_Name == _T("HEIGHT"))
				Fragh = GetIntValue(pMeas);
			else
				break;

			GotMeasurements = TRUE;

			delete pMeas;
		}

		if (pMeas)
			delete pMeas;

		if (!GotMeasurements)	
			break;
		else
		{
			CString S;

			VBProc->GetString(S);

			RectInfo R;

			R.Rect.left = ix;
			R.Rect.right = ix + Fragw;
			R.Rect.top = iy;
			R.Rect.bottom = iy + Fragh;
			R.GUID = S;
			
			Rects.push_back(R);
		}
	}
		
}

CString SetPathForScoresFile(CScriptProcessor *VBProc, CString & root, BOOL isCytovision)
{
	if (isCytovision)
	{
		CString Blank = _T(""); 
		VBProc->SetStringPersistent(_T("REGIONSCOREPATH"), Blank); 	
		return Blank;	
	}
	else
	{
		VBProc->SetStringPersistent(_T("REGIONSCOREPATH"), root);
		return root;
	}
}

void AddFrameMaskToScript(CScriptProcessor *VBProc, CString & root, CString & Filename)
{
	CFileFind file;
	BOOL FileExists = file.FindFile(root + "\\" + Filename);
	
	VBProc->SetLongPersistent(_T("REGIONMODE"), 0);
 
	if (!FileExists) 
    { 
		CString Blank = _T(""); 
		VBProc->SetStringPersistent(_T("REGIONID"), Blank);
       return; 
    } 

	VBS_IMAGE_REC imageInfo;
	ImageData mask;

	CString path = root + "\\" + Filename;

	mask.IsCounter = FALSE;
	mask.Red = 255;
	mask.Green = 255;
	mask.Blue = 255;
	mask.ComponentName = "RegionMask";
	mask.FileName = path;
	mask.LoadBlob(mask.FileName);

	imageInfo.BitsPerPixel = mask.Bits;
	imageInfo.Address = CopyImage(mask);
	imageInfo.Width = mask.W;
	imageInfo.Height = mask.H;

	VBProc->PutImage(&imageInfo);
	
	CFileFind finder;
	if (finder.FindFile(root + "\\*.region"))
	{
		finder.FindNextFile();
		CString title = finder.GetFileTitle();

		VBProc->SetLongPersistent(_T("REGIONMODE"), 1);

		VBProc->SetStringPersistent("REGIONID", title);

		finder.Close();
	}
	mask.Release();
	
}

//////////////////////////////////////////////////////////////////////////////////////
// Load in the actual image data
//////////////////////////////////////////////////////////////////////////////////////

bool ObjectCountIsSet( long & objectCount, const CString & root)
{
	bool isSet = false;

	// Check if ".objcnt" file exists
	CString filename = root + "\\.objcnt";
	CFileFind finder;
	if (finder.FindFile(filename) == TRUE)
	{
		// Read object count
		CStdioFile file = CStdioFile(filename, CFile::modeRead);
		CString sObjectCount;
		file.ReadString( sObjectCount);
		file.Close();
		
		finder.Close();

		DeleteFile( filename);
		
		objectCount = atoi( sObjectCount);

		isSet = true;
	}

	return isSet;
}

void AddFrameDataToScript(CScriptProcessor *VBProc, CSpectraChromeList * fluorlist, ImageFrame Frame, CString FileRoot, void *addr[], BYTE requiredBpp)
{
	FileList ImageFiles;
	

	// Add frame components in fluorochrome order as defined by the assay, not capture order
	POSITION pos = fluorlist->m_list.GetHeadPosition();

	while (pos != NULL)
	{
		CSpectraChrome * fluor = fluorlist->m_list.GetNext(pos);

		FrameComponent FC(_T(""), _T(""));
		Frame.GetFrameComponent(fluor->m_Name, FC);

		ImageData X;

		X.IsCounter = FC.IsCounterstain;
		X.Red = FC.Red;
		X.Green = FC.Green;
		X.Blue = FC.Blue;
		X.ComponentName = FC.ComponentName;
		if (FC.ProjectionImage != _T(""))
		{				
			X.FileName = FC.ProjectionImage;
			X.LoadBlob(FileRoot + _T("\\") + X.FileName);
			ImageFiles.push_back(X);
		}
		else
		{
			// No projection image load any other image data
			for each (ImageData ID in FC.ImageFiles)
			{
				X.FileName = ID.FileName;
				X.IsCounter = FC.IsCounterstain;
				X.LoadBlob(FileRoot + _T("\\") + ID.FileName);
				ImageFiles.push_back(X);
			}	
		}
	}

	int Idx = 0;
	CString filename;
	static int imgFileCount = 0;
	// Load in the actual image data
	for each (ImageData ID in ImageFiles)
	{
		VBS_IMAGE_REC imageInfo;
		imageInfo.Width = ID.W;
		imageInfo.Height = ID.H;


		// Acceptable bit depths for UTS images are 1,4,8,16,24,32,64 (see CZMemRect::MakeNew()).
		// 10,12,14 are also accepted but stored in memory as 16 bit.

		// If the output bit depth is smaller than the input, do a histogram stretch, in order
		// to make the most of the reduced resolution. Must also do it like this so as to
		// be compatible with older scripts, which always use a reduced bit-depth (10bpp => 8bpp).
		if (requiredBpp < ID.Bits)
		{
			imageInfo.BitsPerPixel = requiredBpp;
			imageInfo.Address = HistogramStretchToNBPP(ID, requiredBpp);
		}
		else
		{
			imageInfo.BitsPerPixel = ID.Bits;
			imageInfo.Address = CopyImage( ID);
		}

		ID.Release();
		VBProc->PutImage(&imageInfo);	
		addr[Idx] = imageInfo.Address;

		CString SpecName;
		SpecName.Format("%s%d", SPECTRACHROME_NAME, Idx);
		VBProc->SetStringPersistent(SpecName, ID.ComponentName);
		VBProc->SetLongPersistent(ID.ComponentName, (ID.Blue << 16) + (ID.Green << 8) + ID.Red);

//		_stprintf(BlkHdr[Idx], _T("%s"), ID.ComponentName);

		Idx++;
	}

	VBProc->SetLongPersistent(SPECTRACHROME_NUMBER, ImageFiles.size());  


}



/////////////////////////////////////////////////////////////////////////////////////////////////////
// The main Find thread, pulls images out of the queue, processes them,
// and uploads results to the database.  
/////////////////////////////////////////////////////////////////////////////////////////////////////

static UINT FindSlides(LPVOID pSlideList)
{
	POSITION			pos;
	HRESULT				hr = CoInitialize(NULL);
	CScriptProcessor*	VBProc = NULL;
	CScriptDirective	scriptDirectives;
	int					num_sc;
	BYTE*				QueueData;
	UINT				PeekType;
	UINT				PeekSize;
	CString				scriptMode = _T("RUN");
	CString				classifier_path = _T("No Classifier");
	CString				classifier_type = _T("NONE");
	BOOL				DontQuit = TRUE;
	POSITION			next_scan_pass_pos = NULL;
	CScanPass*			nextScanPass       = NULL;
	int					frames = 0;
//	CCapable			capable;
    QScriptParam		SP;
	CSlideList*			m_pSlideList;
	CSlide*				pSlide;
	CAssay*				pAssay;
	CScanPass*			scanPass;
	POSITION			new_slide_pos;
	POSITION			new_pass_pos;
	POSITION			end_slide_pos;
	POSITION			end_pass_pos;
	CString				m_outputDir;
	BOOL				m_Aborted = FALSE;
	int					m_pass_no = 0;
	CString				FrameId;
	CString				FrameMaskFolder;
	CSyncObjectSemaphore *pFragSem;
	HANDLE				hLastThreadRunning = NULL;
	static	CString scriptFile;

	m_pSlideList = (CSlideList *)pSlideList;

	// Enable the low-fragmentation heap.
	// This is only available on Windows XP, Windows Server 2003 and
	// later OS versions, as is the HeapSetInformation() function
	// used to enable it. So that this program will run on earlier
	// versions of Windows, must dynamically link to the function.
	// Note that the function always fails when called from a Debug build.
	HINSTANCE hKernelLib = LoadLibrary("KERNEL32");
	if (hKernelLib)
	{
		FARPROC pHeapSetInformation = GetProcAddress(hKernelLib, "HeapSetInformation");
		if (pHeapSetInformation)
		{
  			HANDLE hHeap = GetProcessHeap();
			HEAP_INFORMATION_CLASS heapInformationClass = HeapCompatibilityInformation;
			ULONG val=2;
			PVOID heapInformation = &val;
			SIZE_T heapInformationLength = sizeof(val);

			BOOL status = ((BOOL (WINAPI *)(PVOID, HEAP_INFORMATION_CLASS, PVOID, SIZE_T))
			                                    pHeapSetInformation)(hHeap,
			                                                         heapInformationClass,
			                                                         heapInformation,
			                                                         heapInformationLength);
			
		}
 
		FreeLibrary(hKernelLib);
    } 

	// Clear slide pointer
	pSlide = NULL;


    // Try and create a unique directory for outputting information to
	if (m_pSlideList->m_ProcessName != "")
		m_outputDir.Format("%s\\UTS%s", m_pSlideList->m_QPath, m_pSlideList->m_ProcessName);
	else
		m_outputDir.Format("%s\\UTSOUT%ld", m_pSlideList->m_QPath, (long) GetTickCount());

	// Make it again
    mkdir(m_outputDir);

	CCPUCount CPU;
	
	CPU.Count();
	int NProcessors = CPU.LogicalProcessors();

	if (NProcessors <= 2)
		pFragSem = new CSyncObjectSemaphore(1, 1);	// Doing this will allow one core to do the thumbnail cutting the other to do the analysis
	else
		pFragSem = new CSyncObjectSemaphore(4, 4);


	// While we haven't been killed
	while (DontQuit && (m_pSlideList->m_KillFindFlag == FALSE))
	{
		// Wait for something to appear in the queue (or to be killed) and gets its ID
		BOOL data_from_queue = FALSE;
		QueueData = NULL;

		while (!data_from_queue && !m_pSlideList->m_KillFindFlag)
		{
			// First try to lock the queue
			if (m_pSlideList->m_findQ->Lock(TRUE, 10) == CQueue::LockOk)
			{
				// Peek the queue to see whats in it
				if (m_pSlideList->m_findQ->Peek((UINT *) &PeekType, &PeekSize) == CQueue::Ok)
				{
					// Remove the data from it
					QueueData = new BYTE[PeekSize];
					if (m_pSlideList->m_findQ->Remove(QueueData, (UINT *) &PeekSize, (UINT *) &PeekType) == CQueue::Ok)
						data_from_queue = TRUE;
				}

				m_pSlideList->m_findQ->ClearLock();
			}

		
			// If no data, wait a while
			if (data_from_queue == FALSE)
				Sleep(100);
		}

		////////////////////////////////////////////////////////////////////////////////
		//
		//	Process commands from the queue
		//
		if (QueueData)
		{
			//TRACE1("FindThread - Received queue command %d\r\n", PeekType);
			switch (PeekType)
			{
			case QNEWSLIDE:

				m_Aborted = FALSE;
			

				new_slide_pos = *(POSITION *) QueueData;
				if (new_slide_pos)
				{
					pSlide = m_pSlideList->m_list.GetAt(new_slide_pos);
					pAssay = pSlide->m_Assay;
				}
				else
					pSlide = NULL;
				break;

			case QNEWPASS:

                m_pSlideList->m_bStopCapture = FALSE;
				
                m_pSlideList->m_FindCurrentRegion = 0;

				
				new_pass_pos = *(POSITION *) QueueData;
				scanPass     = pAssay->m_ScanPassList->m_list.GetAt(new_pass_pos);

				m_pass_no    = pAssay->m_ScanPassList->GetPassNumber(scanPass);
				ASSERT(m_pass_no >= 0);				
				
				next_scan_pass_pos = pAssay->m_ScanPassList->m_list.FindIndex(m_pass_no+1);
				if (next_scan_pass_pos)
					nextScanPass = pAssay->m_ScanPassList->m_list.GetAt(next_scan_pass_pos);
				else
					nextScanPass = NULL;
			
				m_pSlideList->m_FindCurrentFrame = 0;
			

				if (scanPass)
				{
					// Number of components per frame
					num_sc   = scanPass->m_SpectraChromeList->m_list.GetCount();

					// if we have one, delete the VB script processor
					if (VBProc)
					{
						VBProc->ReleaseScriptProcessor();
						delete VBProc;
						VBProc = NULL;
					}
                    
					// And get a new one
					VBProc = new CScriptProcessor;

					// Check return and flag if error
					BOOL vbproc_init = VBProc->InitialiseScriptProcessor(m_pSlideList->m_VarFile,
																		m_pSlideList->m_QPath,
																		m_pSlideList->m_VBQFile + "IN",
																		m_pSlideList->m_VBQFile + "OUT");

					if (vbproc_init == FALSE)
						NLog_Fatal(_T("PCVSCANNER"), _T("Failed to initialise script processor"));

					////////////////////////////////////////////////////////////////////////////////
					//
					//	Load script
					//
					CString scriptPath = CAII_Shared::ScriptsPath() + _T("\\Spot\\");
				
					NLog_Trace(_T("PCVSCANNER"), _T("scriptPath: %s"), scriptPath);
					
					if (pAssay->GetVersion() <= ASSAYVERSION_PRE72)
						scriptFile = scriptPath + _T("SPOTHiMagCV7.script");
					else
						scriptFile = scriptPath + _T("SPOTHiMagCV72.script");

				    if (VBProc->LoadScript(scriptFile, TRUE) == FALSE)
					    NLog_Error(_T("PCVSCANNER"), _T("Error, script %s didn't load"), scriptFile);
					    

					scriptDirectives.FreeDirectives();
					scriptDirectives.AddDirective("Image", "BPP");
					scriptDirectives.ParseScript(scriptFile);
                    
					// Set up persistent variables
					VBProc->ResetPersistentVariables();
					// Set the scan pass parameters as persistents
					pos = scanPass->m_ScriptParams->m_ParamSetList.GetHeadPosition();
					while (pos != NULL)
					{
						CParamSet *param_set = scanPass->m_ScriptParams->m_ParamSetList.GetNext(pos);
						VBProc->SetPersistent(param_set->m_PersistentName, param_set->m_Value);
					}

					CString classPath = CAII_Shared::ClassifiersPath() + _T("\\SpotCell\\");
					CString classFile = _T("Everything");
					
					if (pAssay->GetVersion() > ASSAYVERSION_PRE72)
					{
						classFile = classPath + scanPass->m_ClassifierName + _T(".model");
						VBProc->SVMModel(classFile);
					}
					
					VBProc->SetStringPersistent(_T("ASSAY_NAME"), pAssay->m_Name);
					VBProc->SetStringPersistent(_T("ROOT_FILENAME"), pSlide->m_RootName);
					VBProc->SetStringPersistent(_T("SESSION_NAME"), pSlide->m_SessionName);
				}
				break;
				
			case QSTRING:
				{
					QString QSP;

					memcpy(&QSP, QueueData, sizeof(QSP));
					CString Val;
					Val.Format(_T("%s"), QSP.Value);
					VBProc->SetStringPersistent(QSP.Name, Val);

					if (_tcscmp(QSP.Name, FRAME_ID) == 0)
						FrameId = Val;

					if (_tcscmp(QSP.Name, FRAME_MASK_PATH) == 0)
						FrameMaskFolder = Val;
				}
				break;

			case QFRAMEMEAS:
					
				FrameMeas			FM;

				// Get +data from Q BEFORE doing anything with it !
				memcpy(&FM, QueueData, sizeof(FM));
			
				if (FM.MeasDataType == MEAS_DATATYPE_NUMBER_LONG) 
                {
					VBProc->SetLongPersistent(FM.title, FM.lval);
				} 
				else if (FM.MeasDataType == MEAS_DATATYPE_NUMBER_DOUBLE) 
				{
					VBProc->SetDoublePersistent(FM.title, FM.dval);
				} 
				break;

			case QSCRIPTPARAM:
				// Get +data from Q BEFORE doing anything with it !
				memcpy(&SP, QueueData, sizeof(QScriptParam));
                // Parameter already packaged as a variant so just send to script processor
                VBProc->SetPersistent(SP.PersistentName, SP.Value);
				break;

			case QIM:
				{				

					int r;
					memcpy(&r, QueueData, sizeof(r));

					m_pSlideList->m_FindCurrentFrame++;

					void *addr[20];
					for (int x = 0; x < 20; x++)
						addr[x] = NULL;

                    // NOTE if this is set TRUE by a script then no analysis will take place but it will 
                    // flush any queued images.
                    if (!m_pSlideList->m_bStopCapture)
                    {
                        //Set some other persistents
                        VBProc->SetStringPersistent(SCRIPT_MODE, scriptMode);
                        VBProc->SetLongPersistent(FRAMES_TOTAL, m_pSlideList->m_FramesInScan);
                        VBProc->SetLongPersistent(FRAME_NUMBER, m_pSlideList->m_FindCurrentFrame);

						// Process the output cells, first get the full bit depth frames they came from
						FrameReader FR(pSlide->m_FramesFile, _T(""), _T(""));
						ImageFrame *pFrame = new ImageFrame();
						long cellIdFrameNumber = 0;

#ifdef _DEBUG
						if (VBProc->LoadScript(scriptFile, TRUE) == FALSE)
						{
							CString err;
							err.Format("Error, script %s didn't load\r\n", scriptFile);
							AfxMessageBox(err);
							DEBUGSTRING(err);
						}
#endif

						if (FR.GetFrameData(FrameId, *pFrame, cellIdFrameNumber))
						{
							//Frame.m_Components.sort();
							// The frame number used by the spot script to compose the CellId (AKA MEASID)
							VBProc->SetLongPersistent(CELLID_FRAME, cellIdFrameNumber);
							// The object number used by the spot script to compose the CellId (AKA MEASID)
							long objectCount = 0;
							if (ObjectCountIsSet( objectCount, FrameMaskFolder))
							{
								VBProc->SetLongPersistent( OBJECT_COUNT, objectCount);
							}

							// If there is a script directive requiring 16 bpp, send images down in that bit depth.
							// Otherwise, default to 8 bpp, as that is what is required by older scripts.
							int requiredBpp = EIGHT_BITS_PER_PIXEL;
							if (scriptDirectives.GetDirectiveIntValue("Image", "BPP") > 8)
								requiredBpp = SIXTEEN_BITS_PER_PIXEL;

							// Convert the frame data to the required bit depth and queue it for the script.
							AddFrameDataToScript(VBProc, scanPass->m_SpectraChromeList, *pFrame, pSlide->m_FileRoot, addr, requiredBpp);

							CString HasScoresFolder = SetPathForScoresFile(VBProc, FrameMaskFolder, m_pSlideList->m_CallingProcess.CompareNoCase("cytovision.exe") == 0);

							// add region mask if it exists
							AddFrameMaskToScript(VBProc, FrameMaskFolder, CString("regionmask.stk"));


							// Run the script.
							VBProc->SetExecutionMode(modePlay);
							if (!VBProc->ExecuteScript())
								m_pSlideList->m_KillFindFlag = TRUE; // If there was a script error, don't continue to the next frame,
								                                     // otherwise the user will be forced to acknowledge the error message
								                                     // once for every frame.

							long objects_found = 0;
							VBProc->GetLongPersistent(OBJECT_COUNT, &objects_found);
							
							for (int x = 0; x < 20; x++)
								if (addr[x]) 
									delete addr[x];

							Rectangles *pRects = new Rectangles();
							BuildRectangleList(VBProc, *pRects);

							VBProc->Reset();


							// Wait for a free slot
							//pFragSem->Signalled(5 * 60000);		// 5 minutes

							FragControl *pFCB = new FragControl(pRects, pFrame, pSlide->m_FileRoot, pFragSem, scanPass->m_SpectraChromeList, HasScoresFolder);

							DWORD Tid;
							hLastThreadRunning = CreateThread(NULL, 
										NULL, 
										(LPTHREAD_START_ROUTINE)ProcessFragmentData, 
										pFCB, 
										THREAD_QUERY_INFORMATION | THREAD_TERMINATE,
										(LPDWORD) &Tid);
						

							long SC;

							// If this is set TRUE then stop capture of anymore objects
							if (VBProc->GetLongPersistent(STOP_CAPTURE, &SC))
							{
								if (SC > 0)
								{
									NLog_Info(_T("CVSCANNER"), _T("STOP CAPTURE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!"));
									m_pSlideList->m_bSkipScanRegion = TRUE;
									// Reset the count may be moving to a new region
									VBProc->SetLongPersistent(STOP_CAPTURE, 0);
								}
							}
						}

						FrameId = _T("");
					}
				}
				break;

			case QSCANABORTED:
				m_Aborted = TRUE;
				
				break;

			case QSLIDEABORTED:
				m_Aborted = TRUE;
				
				break;		

			case QPASSEND:
				
                
				end_pass_pos = (POSITION) *QueueData;
				

				// if we have one, delete the VB script processor
				if (VBProc)
				{
					VBProc->ReleaseScriptProcessor();
					delete VBProc;
					VBProc = NULL;
				}

		
                // If we get to here then the scan thread has queued a QPASSEND so safe to set this back again
                m_pSlideList->m_bSkipScanPass = FALSE;              
				break;
				
			case QSLIDEEND:
				end_slide_pos = (POSITION ) *QueueData;
				break;

			case QEND:
				m_pSlideList->m_KillFindFlag = TRUE;
				DontQuit = FALSE;
				// NLog_Trace(_T("PCVSCANNER"), _T("QEND"));
				break;
			}

			// Fix mem leaks
			delete [] QueueData;
		}
		
	}

	// if we have one, delete the VB script processor
	if (VBProc)
	{
		VBProc->ReleaseScriptProcessor();
		delete VBProc;
		VBProc = NULL;
	}

	if (hLastThreadRunning)
		WaitForSingleObject(hLastThreadRunning, 5 * 60000);		// 5 minutes should be sufficient

	delete pFragSem;

	// Send end of update Q message
	// Get the results queue
	CQueue*Q = m_pSlideList->m_resultsQ;

	if (Q)
	{
        // First try to lock the queue
		if (Q->Lock(TRUE, 10) == CQueue::LockOk)
		{
			// Q up end of measurements
			if (Q->Add(0, 0, QEND) != CQueue::Ok)
				TRACE0("FindSlides:: Couldn't Q End on resultsQ!\r\n");

			// Now remove lock
			Q->ClearLock();
		}
	}

	CoUninitialize();

	// Clear out the queue
	BOOL empty=FALSE;
	while (!empty)
	{
		// First try to lock the queue
		if (m_pSlideList->m_findQ->Lock(TRUE, 10) == CQueue::LockOk)
		{
			// Peek the queue to see whats in it
			int ret = m_pSlideList->m_findQ->Peek((UINT *) &PeekType, &PeekSize);
			if (ret == CQueue::Ok)
			{
				// Remove the data from it
				QueueData = new BYTE[PeekSize];
				if (m_pSlideList->m_findQ->Remove(QueueData, (UINT *) &PeekSize, (UINT *) &PeekType) == CQueue::Ok)
					delete[] QueueData;
			}
			if (ret == CQueue::NoItems)
				empty = TRUE;

			m_pSlideList->m_findQ->ClearLock();
		}
	}


    // Tidy temp directory
    CFileFind CFF;
    CString UTSDirPath;
    CString FN;
    BOOL IsDir = FALSE;

	SetCurrentDirectory(m_outputDir);
	if (CFF.FindFile("Component*"))
	{
		while (CFF.FindNextFile())
		{
			FN = CFF.GetFilePath();
			RemoveDirectory(FN);
		}
		FN = CFF.GetFilePath();
		CFF.Close();
		SetCurrentDirectory("\\");
		RemoveDirectory(FN);
	}
    
    // Remove our output directory
    RemoveDirectory(m_outputDir);
	// And signal that we are actually dead
	m_pSlideList->m_KillFindFlag = TRUE;

	// NLog_Trace(_T("PCVSCANNER"), _T("FIND THREAD END"));

	return 0;
}


void CFindThread::Launch(CSlideList *pSlides)
{
	DWORD Id;
	pSlides->m_KillFindFlag = FALSE;
	// Attach image cache and region tools
	m_hFindThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) FindSlides, (LPVOID) pSlides, 0, (LPDWORD) &Id);
}



