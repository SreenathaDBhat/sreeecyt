#if !defined(AFX_FINDTHREAD_H__8BE7341E_DBAE_41A5_8E8B_D997A7CDCECC__INCLUDED_)
#define AFX_FINDTHREAD_H__8BE7341E_DBAE_41A5_8E8B_D997A7CDCECC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FindThread.h : header file
//
#include <afxtempl.h>
#include "AITiff.h"		
#include "stdafx.h"

// The cell overlap distance defines how close two cells are in overlapping frames
// to be the same cell.
#define CELL_OVERLAPDIST               5.0

typedef struct {
    float x, y;           // Cell position
} CellPos;


 
#define MEAS_DATATYPE_NUMBER_DOUBLE		1
#define	MEAS_DATATYPE_NUMBER_LONG		2
#define MEAS_TYPE_FEATURE				3
#define MEAS_TYPE_PASSNUMBER			4
#define MEAS_TYPE_POSITION				5
#define MEAS_TYPE_SCALE					6



class AI_Tiff;
class CQueue;
class CSlide;
class CAssay;
class CSlideList;
class CScanPass;
class CScriptDirective;
class CMLock;

// Really a wrapper for a CList
class CellXYList {
public:
    CellXYList();
    ~CellXYList();

    void AddToList(float x, float y);       // Add a cell to a list
    BOOL AlreadyInList(float x, float y);   // Check to see if cell already in list
    void DisposeAll(void);              // Delete all cells in the list

private:
    CList<CellPos, CellPos> m_CellList;
};


/////////////////////////////////////////////////////////////////////////////
// ScanThread window

class CFindThread
{
// Construction
public:
	HANDLE m_hFindThread;

	CFindThread();
	virtual ~CFindThread();
	void Launch(CSlideList *pSlides);

};




/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FINDTHREAD_H__8BE7341E_DBAE_41A5_8E8B_D997A7CDCECC__INCLUDED_)
