#ifndef __FRAMEDATA_H
#define __FRAMEDATA_H

#include <afx.h>
#include <list>
#include <iostream>

#include "Compressor.h"

class ImageData
{
public:
	ImageData()
	{
		FileName = _T("");
		pImageMem = NULL;
		pConvertedMem = NULL;
		W = 0;
		H = 0;
		Bits = 0;
		IsCounter = FALSE;
		IsMaxProj = FALSE;
		ComponentName = _T("");
		Red = 0;
		Green = 0;
		Blue = 0;
	}

	~ImageData()
	{
	}

	void *LoadBlob(CString FileName)
	{

		if (FileName.Find(_T(".blb")) != -1)
		{
			CFile myFile;
			CFileException fileException;

			if (!myFile.Open(FileName, CFile::modeRead, &fileException))
				return NULL;
				
			myFile.Read(&W,    sizeof(int));
			myFile.Read(&H,    sizeof(int));
			myFile.Read(&Bits, sizeof(int));

			long Size = W * H * ((Bits + 7) / 8);

			pImageMem = (WORD *)malloc(Size);

			myFile.Read(pImageMem, Size);
			myFile.Close();
		}
		else
		if (FileName.Find(_T(".stk")) != -1)
		{
			Compressor CompressIt;

			std::vector<BYTE> DecodedRawData;
		
			CompressIt.DecompressAndLoad(FileName, DecodedRawData, W, H, Bits);

			long Size = W * H * ((Bits + 7) / 8);

			pImageMem = (WORD *)malloc(Size);

			memcpy(pImageMem, &DecodedRawData[0], Size);
		}
		else
		if (FileName.Find(_T(".channel")) != -1)
		{
			array<unsigned char>^ data = System::IO::File::ReadAllBytes(gcnew System::String((LPCTSTR)FileName));
            System::IO::MemoryStream ^ stream = gcnew System::IO::MemoryStream(data);
                
			System::IO::BinaryReader ^reader = gcnew System::IO::BinaryReader(stream);
                
            int type = reader->ReadInt32();
            bool compressed = false;

            if (type == 80085)
            {
                compressed = true;
                W = reader->ReadInt32();
            }
            else
            {
                W = type;
            }

            H = reader->ReadInt32();
            Bits = reader->ReadInt32();
            long Size =  W * H * ((Bits + 7) / 8);
            array<unsigned char>^ bytes = gcnew array<unsigned char>(Size);

            if (compressed)
            {
                int n = (int)(stream->Length - stream->Position);
                array<unsigned char>^ compressedBytes = gcnew array<unsigned char>(n);
                stream->Read(compressedBytes, 0, n);
                reader->Close();

                System::IO::MemoryStream^ ms = gcnew System::IO::MemoryStream(compressedBytes);
                System::IO::Compression::GZipStream^ zs = gcnew System::IO::Compression::GZipStream(ms, System::IO::Compression::CompressionMode::Decompress);

                zs->Read(bytes, 0, Size);
                reader->Close();
            }
            else
            {
                stream->Read(bytes, 0, Size);
                reader->Close();
            }

			pImageMem = (WORD *)malloc(Size);

			pin_ptr<unsigned char> pinSrc = &bytes[0];
			unsigned char * src = pinSrc;

			memcpy(pImageMem, src, Size);
		}
		return pImageMem;
	}

	void SaveBlob(LPCTSTR FileName)
	{
		FILE *fh;
		if ((fh = fopen(FileName, "wb")))
		{			
			fwrite(&W, sizeof(int), 1, fh);
			fwrite(&H, sizeof(int), 1, fh);
			fwrite(&Bits, sizeof(int), 1, fh);
			fwrite(pImageMem, sizeof(WORD) * W * H, 1, fh);
			fclose(fh);
		}
	}

	void Release(void)
	{
		if (pImageMem)
		{
			free(pImageMem);
			pImageMem = NULL;
		}
		if (pConvertedMem)
		{
			free(pConvertedMem);
			pConvertedMem = NULL;
		}
	}

	CString FileName;
	WORD	*pImageMem;
	BYTE    *pConvertedMem;

	int		W, H, Bits;
	
	BOOL	IsCounter;
	BOOL	IsMaxProj;
	CString ComponentName;
	BYTE	Red;
	BYTE	Green;
	BYTE	Blue;
};

typedef std::list<ImageData> FileList;

class FrameComponent
{
public:
	FrameComponent(LPCTSTR Name, LPCTSTR Id)
	{
		ComponentName = Name;
		ComponentId = Id;
		ProjectionImage = _T("");
		ImageFiles.clear();
		IsCounterstain = FALSE;
		IsStacked = FALSE;
		Red = 0;
		Green = 0;
		Blue = 0;
		
	}

	void AddFile(LPCTSTR FileName, WORD *Mem = NULL)
	{
		ImageData ID;

		ID.FileName = FileName;
		ID.pImageMem = Mem;
		ImageFiles.push_back(ID);
	}

	void AddFile(ImageData &ID)
	{
		ImageFiles.push_back(ID);
	}

	bool operator < (const FrameComponent& rhs)
    {
		return ComponentName < rhs.ComponentName;
	}

	CString GetImageFileName(int Idx)
	{
		CString Name = _T("");
		int i = 0;
		for each (ImageData ID in ImageFiles)
		{
			if (i == Idx)
			{
				Name = ID.FileName;
				break;
			}
		}

		return Name;
	}

	BYTE	  Red;
	BYTE	  Green;
	BYTE	  Blue;
	BOOL	  IsCounterstain;
	BOOL	  IsStacked;
	CString   ComponentName;
	CString   ComponentId;
	FileList  ImageFiles;
	CString   ProjectionImage;
	
};

typedef std::list<FrameComponent> FrameComponents;

class ImageFrame
{
public:
	ImageFrame() 
	{
		m_FramePosition = _T("");
		m_SlideName = _T("");
		m_ObjectiveMag = 0.0;
		m_XScale = 1.0;
		m_YScale = 1.0;
		GUID guid;

		CoCreateGuid(&guid);

		wchar_t szGUID[39];
		::StringFromGUID2(guid, szGUID, 39);
		
		CString newID = szGUID;
		m_Id = newID.Mid(1,newID.GetLength()-2);
	}

	void AddComponent(FrameComponent &FC)
	{
		m_Components.push_back(FC);
	}

	void GetFrameComponent(LPCTSTR Name, FrameComponent &FrameComp)
	{
		FrameComponent *pFC = NULL;

		for each (FrameComponent FC in m_Components)
		{
			if (_tcsicmp(FC.ComponentName, Name) == 0)
			{
				FrameComp = FC;
				break;
			}
		}

	}

	FrameComponents m_Components;
	CString			m_FramePosition;
	CString			m_Id;
	double			m_ObjectiveMag;
	double			m_XScale;
	double			m_YScale;
	CString			m_SlideName;

	CString dbg;
};

#endif