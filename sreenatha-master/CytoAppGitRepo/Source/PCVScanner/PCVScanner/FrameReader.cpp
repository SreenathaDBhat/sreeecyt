#include "StdAfx.h"
#include <strsafe.h>

#include "FrameReader.h"

FrameReader::FrameReader(LPCTSTR FrameFile, LPCTSTR ScanArea, LPCTSTR AssayName)
{
	USES_CONVERSION;

//	CFileFind CFF;
//	BOOL FileExists = CFF.FindFile(FrameFile);

	m_FrameFile = FrameFile;
	m_ScanArea = ScanArea;
	m_Assay = AssayName;
}

FrameReader::~FrameReader(void)
{
}

BYTE B2HEX(CString X)
{
	BYTE R, S;

	if (X[0] >= 'A') 
		R = X[0] - 'A' + 10;
	else
		R = X[0] - '0';
	if (X[1] >= 'A') 
		S = X[1] - 'A' + 10;
	else
		S = X[1] - '0';

	return R * 16 + S;
}

BOOL FrameReader::GetFrameData(CString FrameId, ImageFrame &IF, long & frameIndex)
{
	USES_CONVERSION;

	const WCHAR *pAttrName, *pAttrValue, *pQName;

	CVXMLReaderWriter XML;
	HRESULT hr;
	BOOL GotF = FALSE, GotC = FALSE, GotN;
	XmlNodeType nodeType;

	XML.CreateReader(A2W(m_FrameFile));

	if (XML.Reader() != NULL)
	{
		frameIndex = 0;
		while ((XML.Reader()->Read(&nodeType) == S_OK) && !GotF)
		{
			if (nodeType == XmlNodeType_Element)
			{
				if (XML.Reader()->GetQualifiedName(&pQName, NULL) == S_OK)
				{	
					if (wcscmp(pQName, L"Frame") == 0)
					{
						const WCHAR *pAttrName, *pAttrValue;

						frameIndex++;
						XML.Reader()->MoveToFirstAttribute();					
						XML.Reader()->GetQualifiedName(&pAttrName, NULL);
						if (wcscmp(pAttrName, L"Id") == 0)
						{
							XML.Reader()->GetValue(&pAttrValue, NULL);
							if (_tcsicmp(FrameId, W2T(pAttrValue)) == 0)
							{
								GotF = TRUE;
								break;
							}
						}
					}
				}
			}
		}

		if (!GotF)
			return FALSE;

		CString ComponentId, ImagePath, CompName, Str;
		BOOL IsStacked = FALSE, IsCounter = FALSE;
		BYTE R = 0, G = 0, B = 0;

		while (XML.Reader()->Read(&nodeType) == S_OK)
		{
			FrameComponent *pFC;
			ComponentId = _T("");
			CompName = _T("");

			GotC = FALSE;
			GotN = FALSE;
			if (nodeType == XmlNodeType_Element)
			{
				if (XML.Reader()->GetQualifiedName(&pQName, NULL) == S_OK)
				{	
					if (wcscmp(pQName, L"Component") == 0)
					{
						XML.Reader()->MoveToFirstAttribute();
					
						hr = XML.Reader()->GetQualifiedName(&pAttrName, NULL);
						if ((hr == S_OK) && wcscmp(pAttrName, L"Id") == 0)
						{
							XML.Reader()->GetValue(&pAttrValue, NULL);
							ComponentId.Format(_T("%s"), W2T(pAttrValue));
							GotC = TRUE;
							hr = XML.Reader()->MoveToNextAttribute();
						}

						hr = XML.Reader()->GetQualifiedName(&pAttrName, NULL);
						if ((hr == S_OK) && wcscmp(pAttrName, L"Name") == 0)
						{
							XML.Reader()->GetValue(&pAttrValue, NULL);
							CompName.Format(_T("%s"), W2T(pAttrValue));
							GotN = TRUE;
							hr = XML.Reader()->MoveToNextAttribute();
						}
							
						hr = XML.Reader()->GetQualifiedName(&pAttrName, NULL);
						if ((hr == S_OK) && wcscmp(pAttrName, L"IsCounterstain") == 0)
						{
							XML.Reader()->GetValue(&pAttrValue, NULL);
							Str.Format(_T("%s"), W2T(pAttrValue));
							if (Str.CompareNoCase(_T("true")) == 0)
								IsCounter = TRUE;
							else
								IsCounter = FALSE;
							hr = XML.Reader()->MoveToNextAttribute();
						}

						hr = XML.Reader()->GetQualifiedName(&pAttrName, NULL);
						if ((hr == S_OK) && wcscmp(pAttrName, L"IsStacked") == 0)
						{
							XML.Reader()->GetValue(&pAttrValue, NULL);
							Str.Format(_T("%s"), W2T(pAttrValue));
							if (Str.CompareNoCase(_T("true")) == 0)
								IsStacked = TRUE;
							else
								IsStacked = FALSE;
							hr = XML.Reader()->MoveToNextAttribute();
						}

						hr = XML.Reader()->GetQualifiedName(&pAttrName, NULL);
						if ((hr == S_OK) && wcscmp(pAttrName, L"RenderColor") == 0)
						{
							XML.Reader()->GetValue(&pAttrValue, NULL);
							Str.Format(_T("%s"), W2T(pAttrValue));
							CString C;
							R = B2HEX(Str.Mid(1, 2));
							G = B2HEX(Str.Mid(3, 2));
							B = B2HEX(Str.Mid(5, 2));
							//hr = XML.Reader()->MoveToNextAttribute();
						}

						

						
						ImagePath = _T("");

						pFC = new FrameComponent(_T("X"), ComponentId);
						pFC->ComponentId = ComponentId;
						pFC->ComponentName = CompName;
						pFC->IsStacked = IsStacked;
						pFC->IsCounterstain = IsCounter;
						pFC->Red = R;
						pFC->Green = G;
						pFC->Blue = B;
						

						if (GotC && GotN)
						{

							while ((XML.Reader()->Read(&nodeType) == S_OK) && (nodeType == XmlNodeType_Element || nodeType == XmlNodeType_Whitespace))
							{
								if (XML.Reader()->GetQualifiedName(&pQName, NULL) == S_OK)
								{
									XML.Reader()->MoveToFirstAttribute();

									if (wcscmp(pQName, L"Image") == 0)
									{
										XML.Reader()->GetValue(&pAttrValue, NULL);
										ImagePath.Format(_T("%s"), W2T(pAttrValue));
								
										pFC->AddFile(ImagePath);										
									}

									if (wcscmp(pQName, L"ProjectionImage") == 0)
									{
										XML.Reader()->GetValue(&pAttrValue, NULL);
										ImagePath.Format(_T("%s"), W2T(pAttrValue));
										
										pFC->ProjectionImage = ImagePath;
									}
								}
							}

							IF.AddComponent(*pFC);
						}
					}
					else
						break;
				}
			}
		}

		return TRUE;
	}

	return FALSE;
}