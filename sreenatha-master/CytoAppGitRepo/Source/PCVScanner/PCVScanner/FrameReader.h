#pragma once


#include <iostream>


#include <CVXMLReaderWriter.h>

#include "FrameData.h"

class FrameReader
{
public:
	FrameReader(LPCTSTR FrameFile, LPCTSTR ScanArea, LPCTSTR AssayName);
	~FrameReader(void);

	BOOL GetFrameData(CString FrameId, ImageFrame &IF, long & frameIndex);
private:
	CVXMLReaderWriter	*pXML;
	CString				m_FrameFile;
	CString				m_ScanArea;
	CString				m_Assay;
};
