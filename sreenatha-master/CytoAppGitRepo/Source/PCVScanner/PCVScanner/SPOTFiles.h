#pragma once

// name of the file marking the dir as processed
#define MARK_PROCESSED_FILE         "PROC.CVS"

// Frame ids
#define FRAMEID_FILE				"FRAME.ID"

// the assay used to score the cell
#define ASSAYNAME_FILE				"assay.name"

// last assay used for the slide
#define LASTASSAY_FILE				".assay"

#define NULL_SCAN_AREA_NAME				_T("(NULL)")
