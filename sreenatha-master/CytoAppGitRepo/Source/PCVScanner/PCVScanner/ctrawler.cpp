//////////////////////////////////////////////////////////////////
// CTRawler class
//
// Trawls directories for information rpdocued by autocapture 
// and sends it up a Q to the CVFinder process.
//
// Written By Karl Ratcliff 19092001
//
//////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "cvscan.h"
#include "ctrawler.h"
#include "dirman.h"
#include <aiqueue.h>

#include <stdio.h>
#include <tiffio.h>
#include <UTS.h>
#include <scriptproc.h>
#include <threaddefs.h>
#include "spotfiles.h"
#include <math.h>
#include "FindThread.h"

// MDS 2 
#include "aiishared.h"
#include "aitiff.h"
#include "Spectrachrome.h"
#include "Woolzif.h"
#include "assay.h"

#include "scanpass.h"
#include "scanpasslist.h"
#include "slide.h"
#include "slidelist.h"

#include "scandata.h"
#include "Registry.h"
#include "reghelper.h"
#include "procapi.h"
#include "spotmsg.h"

#include "CFuncs.h"
#include "..\..\Common\Src\NLog\src\NLogC\NLogC.h"


// NOTE the scan pass number is always 1 - Dont know whether this'll change in the future....
#define SCAN_PASSNO					1

//////////////////////////////////////////////////////////////////
// Constructor/Destructor
//////////////////////////////////////////////////////////////////

// Create a Q
CTrawler::CTrawler()
{
    m_pFindQ = new CQueue;
    m_pTrawlPath = new CString;
	m_pCasePath = new CString;
	m_pAssayName = new CString;
	m_pScanAreaName = new CString;
	m_pInternalAssayName = new CString;

	*m_pAssayName = _T("");
	*m_pInternalAssayName = _T("");
    *m_pTrawlPath =_T("");
	*m_pCasePath = _T("");
    m_pDirMan = NULL;
	m_Reprocess = FALSE;
}

CTrawler::~CTrawler()
{
    if (m_pFindQ)
        delete m_pFindQ;
    if (m_pTrawlPath)
        delete m_pTrawlPath;
	if (m_pCasePath)
		delete m_pCasePath;
    if (m_pDirMan)
        delete m_pDirMan;
	if (m_pAssayName)
		delete m_pAssayName;
}

//////////////////////////////////////////////////////////////////
// Initialise the Q etc
//////////////////////////////////////////////////////////////////

BOOL CTrawler::Initialise(CString TempDir, CString QueueName)
{
    // If we got to here then both queues created so set them up
    if (m_pFindQ->Initialize(CVFINDER_QSIZE, TempDir, QueueName) != CQueue::Ok)
        return FALSE;

    // Create the directory management system
    m_pDirMan = new CDirManager;
    if (!m_pDirMan) 
        return FALSE;

    return TRUE;
}

////////////////////////////////////////////////////////////////////
// Set the assay
////////////////////////////////////////////////////////////////////
	
void CTrawler::SetAssayAndScanArea(LPCTSTR AssayName, LPCTSTR InternalAssayName, LPCTSTR ScanAreaName, LPCTSTR SessionId)
{
	m_pAssayName->Format(_T("%s"), AssayName);
	m_pScanAreaName->Format(_T("%s"), ScanAreaName);
	m_pInternalAssayName->Format(_T("%s"), InternalAssayName);
	m_SessionId = SessionId;
	if (m_pDirMan)
		m_pDirMan->SetAssayAndScanArea(AssayName, InternalAssayName, ScanAreaName);
}

////////////////////////////////////////////////////////////////////
// Delete the scores file
////////////////////////////////////////////////////////////////////

void CTrawler::DeleteScoresFile(void)
{
	m_pDirMan->DeleteFragResults();
	m_pDirMan->DeleteScoresFile();
}

////////////////////////////////////////////////////////////////////
// Create the path for our CTrawler class to trawl
// This is based on cytovisions:
// Machine//Cases//CaseName//SlideXX
// type structure.
////////////////////////////////////////////////////////////////////

void CTrawler::SetCaseToTrawl(LPCTSTR caseFolderPath, LPCTSTR CaseName, LPCTSTR Slide, BOOL Reprocess)
{		
    m_pCasePath->Format(_T("%s\\cases\\%s"), caseFolderPath, CaseName);
    m_pTrawlPath->Format(_T("%s\\cases\\%s\\%s"), caseFolderPath, CaseName, Slide);

	// Initialise the dir management
    m_pDirMan->Initialise(*m_pTrawlPath);
	if (Reprocess)
	{
		m_pDirMan->ClearList();
		m_Reprocess = TRUE;
	}
}


///////////////////////////////////////////////////////////////////
// Get the calibration data for the magnification we are working at
///////////////////////////////////////////////////////////////////
    
BOOL CTrawler::GetObjectiveCalData(float *ImageScaleX, float *ImageScaleY, float *StageScaleX, float *StageScaleY)
{
    int     i;
    BOOL    Ok = FALSE;
    CString Keyname, SubKeyname;
    int     LP = 0;
    CString ScaleX, ScaleY;
    TCHAR   str[256];
    CString MicFile;

    // Defaults folder is a subdirectory of wherever the exe is running from.
    MicFile.Format(_T("%s%s"), GetExecDir(), _T("\\defaults\\Microscope.mic"));

    // Safe default values
    *StageScaleX = *StageScaleY = *ImageScaleX = *ImageScaleY = 0.0;

    // Scan through all objectives in the mic file
    for (i = 0; i < 10; i++)
    {
        Keyname.Format(_T("ImageScale%d"), i);
        LP = GetPrivateProfileInt(Keyname, _T("LensPower"), 0, MicFile);
        // Found an objective with the required magnification
        if (LP == m_nObjectiveMag)
        {
            // Get the image scale
            Keyname.Format(_T("ImageScale%d"), i);
            GetPrivateProfileString(Keyname, _T("X"), _T("1.0"), str, 256, MicFile);
            if (_stscanf(str, _T("%f"), ImageScaleX))
            {
                GetPrivateProfileString(Keyname, _T("Y"), _T("1.0"), str, 256, MicFile);
                if (_stscanf(str, _T("%f"), ImageScaleY))
                {
                    // Now get the pixels per stage step - used for Chromscan coordinates
                    Keyname.Format(_T("XYScale%d"), i);
                    GetPrivateProfileString(Keyname, _T("X"), _T("1.0"), str, 256, MicFile);
                    if (_stscanf(str, _T("%f"), StageScaleX))
                    {
                        GetPrivateProfileString(Keyname, _T("Y"), _T("1.0"), str, 256, MicFile);
                        if (_stscanf(str, _T("%f"), StageScaleY))
                            Ok = TRUE;
                    }
                }
            }
        }
    }
    
    return Ok;
}

////////////////////////////////////////////////////////////////////
// Upload the frame id
////////////////////////////////////////////////////////////////////

BOOL CTrawler::UploadFrameId(CString &DirName)
{
	BOOL GotId = FALSE;
	CString FileName;
	int i;
	HANDLE	hFileFind;
    WIN32_FIND_DATA     FileInfo;

	// Create the filename for the reading of ideal coordinate information
	FileName.Format(_T("%s\\" FRAMEID_FILE), DirName);
	
	// Search for it
	for (i = 0; ((i < 20) && !GotId); i++)
	{
		hFileFind = FindFirstFile(FileName, &FileInfo);
		if (hFileFind != INVALID_HANDLE_VALUE)
		{
			CStdioFile IOF;
			CFileException fe;
			
			if (IOF.Open(FileName, CFile::modeRead, &fe))
			{
				CString FrameId;
				
				if (IOF.ReadString(FrameId))
				{
					QueueString(FRAME_ID, FrameId, m_pSlideList);
					GotId = TRUE;
				}
				
				IOF.Close();
			}
			
		}
		else 
			Sleep(100);
		
		FindClose(hFileFind);
	}

	if (!GotId)
		TRACE0("PCVSCANNER: Error getting frameid file\r\n");
	
	return GotId;
}

////////////////////////////////////////////////////////////////////
// Read coordinates in and upload to DB if possible
////////////////////////////////////////////////////////////////////

BOOL CTrawler::UploadCoords(CString &DirName)
{
	BOOL GotCoords = FALSE;
	CString FileName;
	int i;
	HANDLE	hFileFind;
    float   StageX, StageY, StageZ;
    WIN32_FIND_DATA     FileInfo;

	// Create the filename for the reading of ideal coordinate information
	FileName.Format(_T("%s\\.ideal"), DirName);
	
	// Search for it
	for (i = 0; ((i < 20) && !GotCoords); i++)
	{
		hFileFind = FindFirstFile(FileName, &FileInfo);
		if (hFileFind != INVALID_HANDLE_VALUE)
		{
			CStdioFile IOF;
			CFileException fe;
			
			if (IOF.Open(FileName, CFile::modeRead, &fe))
			{
				CString Coordstr;
				
				if (IOF.ReadString(Coordstr))
				{
					if (_stscanf(Coordstr, _T("%f %f %f"), &StageX, &StageY, &StageZ) == 3)
					{
						SendFrameDetailsToQ(m_pSlideList, StageX, StageY, StageX, StageY, StageZ);
						GotCoords = TRUE;
					}
				}
				
				IOF.Close();
			}
		}
		else 
		{
			// Create the filename for the reading of ideal coordinate information
			FileName.Format(_T("%s\\.coords"), DirName);

			hFileFind = FindFirstFile(FileName, &FileInfo);
			if (hFileFind != INVALID_HANDLE_VALUE)
			{
				CStdioFile IOF;
				CFileException fe;

				if (IOF.Open(FileName, CFile::modeRead, &fe))
				{
					CString Coordstr;

					if (IOF.ReadString(Coordstr))
					{
						int SX, SY;

						if (_stscanf(Coordstr, _T("%d %d"), &SX, &SY) == 2)
						{
							StageX = (float)SX;
							StageY = (float)SY;
							StageZ = 0.0f;
							SendFrameDetailsToQ(m_pSlideList, StageX, StageY, StageX, StageY, StageZ);
							GotCoords = TRUE;
						}
					}

					IOF.Close();
				}
			}
		}

		FindClose(hFileFind);
	}

	if (!GotCoords)
		TRACE0("PCVSCANNER: Error getting coordinate file\r\n");
	
	return GotCoords;
}

BOOL CTrawler::UploadFrameMaskPath(CString & DirName)
{
	BOOL GotMask = FALSE;
	CString FileName;
    WIN32_FIND_DATA     FileInfo;

	FileName.Format(_T("%s"), DirName);
	
	HANDLE	hFileFind = FindFirstFile(FileName, &FileInfo);
	if (hFileFind != INVALID_HANDLE_VALUE)
	{
		QueueString(FRAME_MASK_PATH, FileName, m_pSlideList);		
		FindClose(hFileFind);
		GotMask = TRUE;
	}

	TRACE0((GotMask) ? "PCVSCANNER: Got frame Region mask\r\n" : "PCVSCANNER: No frame region mask\r\n" );
	
	return GotMask;
}

///
/// Checks for end flag file, deletes flag if exists; Messy file
///
BOOL CTrawler::IsExitRequested()
{
	BOOL ret = FALSE;
	CString filename;
    WIN32_FIND_DATA	fileinfo;

	// search for end flag in slide folder
	filename.Format(_T("%s\\%s.END"), *m_pTrawlPath, m_SessionId);

	HANDLE hFileFind = FindFirstFile(filename, &fileinfo);
	if (hFileFind != INVALID_HANDLE_VALUE)
	{
		DeleteFile(filename);
		ret = TRUE;
		TRACE0("PCVSCANNER: End requested\r\n");
		FindClose(hFileFind);
	}

	return ret;
}

////////////////////////////////////////////////////////////////////
// This is the main trawling process of CVscan. Once given a path
// to the case and slide, trawl that path looking for any new 
// captured images. Load these into memory and then convert
// for use by the scripting for analysis. This is done by adding
// them to the input Q for the CVFinder thread.
////////////////////////////////////////////////////////////////////

void CTrawler::TrawlDirectory(CSlideList *pSlides, BOOL autospotcounting)
{
    WIN32_FIND_DATA     FileInfo;
    HANDLE              hFileFind;
    CString             FileName, DirName, LastDir;
    BOOL                Ok;
    CFile               F, ProgressFile;
	
    CFileException      ex, fe;
    BOOL                FoundAnother = FALSE;
	int					NumSpectrachromes;
	CSlide				*pTheSlide;
	POSITION			P, ScanPassPos, SlidePos;
	CScanPass			*pScanPass = NULL;
	int					PretendX = 0, PretendY = 0;		// For manual mode make up some pretend coordinate values
	int					FrameCount = 0;
	CProcessMonitor		CPM;


	// Take a pointer to the slide list
	m_pSlideList = pSlides;
	
	// Get the first and only slide in the list 
	P = m_pSlideList->m_list.GetHeadPosition();
	pTheSlide = m_pSlideList->m_list.GetAt(P);
	// Remember the slide position
	SlidePos = P;
	
	// Now calc the spectrachromes
    NumSpectrachromes = 0;
    // Now create & initialise our script processor
	if (pTheSlide->m_Assay->m_ScanPassList->m_list.GetHeadPosition())
    {
        // Find the first scan pass info to eventually get the script name to run
        P = pTheSlide->m_Assay->m_ScanPassList->m_list.GetHeadPosition();
        if (P)
        {
            pScanPass = pTheSlide->m_Assay->m_ScanPassList->m_list.GetNext(P);
            // Get the actual second scan pass
            if (P)
            {
				// Remember this position
				ScanPassPos = P;
				pTheSlide->m_Assay->m_ScanPassList->m_currentPos = P;
                pScanPass = pTheSlide->m_Assay->m_ScanPassList->m_list.GetNext(P);
				
				// Get the number of spectrachromes in the assay		     
                NumSpectrachromes = pScanPass->m_SpectraChromeList->m_list.GetCount();
            }
        }
    }
    
    if (!pScanPass)
	{
		// There is no scan pass to be trawled. Put a QEND item in the queue, otherwise
		// the find thread will never end (it would just wait forever for data to arrive            
		NLog_Error(_T("PCVSCANNER"), _T("Assay doesn't have a scan pass, exitting!!"));

        QueueEnd(m_pSlideList);
	}
	else
    {
        TRACE1("PCVSCANNER: NUMBER OF SPECTRACHROMES %d\r\n", NumSpectrachromes);

        // Q a new slide
        QueueNewSlide(SlidePos, m_pSlideList);

        // Q a scan pass
        QueueNewPass(ScanPassPos, m_pSlideList);

        m_pSlideList->m_bSkipScanRegion = FALSE;

		// Output results here
		QueueString(OUTPUT_DIRECTORY, *m_pTrawlPath, m_pSlideList);

		//ASSERT(0);
		// Trawl through casebase 
        hFileFind = INVALID_HANDLE_VALUE;

		// if we go idle for a given time, we'll exit and a new instance will have to be created to process spots
		CTime time = CTime::GetCurrentTime();

		BOOL bTrawlSpotFolders = TRUE;

        while (bTrawlSpotFolders)
        {
            // Keep monitoring directories until at least one unprocessed dir found
            if (m_pDirMan->BuildList(autospotcounting, DirName))
            {
                TRACE1("PCVSCANNER: DIRECTORY %s\r\n", DirName);

				// Make doubly sure this is a SPOT cell
                FileName.Format(_T("%s\\FRAME.ID"), DirName);
				// Search for it
                hFileFind = FindFirstFile(FileName, &FileInfo);
				if (hFileFind != INVALID_HANDLE_VALUE)
				{
					FoundAnother = TRUE;
					time = CTime::GetCurrentTime();
				}
				FindClose(hFileFind);

				if (FoundAnother)
				{
					SendCalDetailsToQ(m_pSlideList, m_ImageScaleX, m_ImageScaleY);

					UploadFrameId(DirName);
					UploadFrameMaskPath(DirName);

					if (autospotcounting)
						UploadCoords(DirName);
					else
					{ 
					   // Manual spot counting - no .ideal file if truly manual, but may be using 
						// manual mode with a stage so try and upload coords first.
						if (!UploadCoords(DirName))
						{
							FrameCount++;
							PretendX = 1000 + (FrameCount / 10) * 1280;
							PretendY = 1000 + (FrameCount % 10) * 1024;
							SendFrameDetailsToQ(m_pSlideList, (float)PretendX, (float)PretendY, (float)PretendX, (float)PretendY, 0);
						}
					}
	                
	           				
					// Create the filename for the searching of images
					FileName.Format(_T("%s\\*.probe"), DirName);
					// Search for it
					hFileFind = FindFirstFile(FileName, &FileInfo);
					if (hFileFind != INVALID_HANDLE_VALUE)
					{
						TCHAR ProbeFileName[MAX_PATH];
						// Construct the actusal file name and full path to it
						_stprintf(ProbeFileName, _T("%s\\%s"), DirName, FileInfo.cFileName);
						DeleteFile(ProbeFileName);
					}
					FindClose(hFileFind);

					// Remove any RAW files no longer used in the analysis
					FileName.Format(_T("%s\\*.raw"), DirName);
					hFileFind = FindFirstFile(FileName, &FileInfo);

					BOOL RawOk = (hFileFind != INVALID_HANDLE_VALUE);
					while (RawOk)
					{
						TCHAR RawFileName[MAX_PATH];
						// Construct the actusal file name and full path to it
						_stprintf(RawFileName, _T("%s\\%s"), DirName, FileInfo.cFileName);
						DeleteFile(RawFileName);
						RawOk = FindNextFile(hFileFind, &FileInfo);

					}
					FindClose(hFileFind);
					
					Ok = TRUE;

					// Send the scan area name
					QueueString(SCAN_AREA_NAME, *m_pScanAreaName, m_pSlideList);
	               
					QueueFrameMeasurement(MEAS_DATATYPE_NUMBER_LONG,   MEAS_TYPE_PASSNUMBER, PASS_NUMBER, 1, 0.0, m_pSlideList);
					QueueFrameMeasurement(MEAS_DATATYPE_NUMBER_LONG,   MEAS_TYPE_FEATURE,    FRAME_NUMBER, (long)m_pSlideList->m_ScanCurrentFrame, (double)0.0, m_pSlideList);
					// Q the image
					TRACE0(_T("PCVSCANNER: QUEUING IMAGE\r\n"));
					QueueImage(m_pSlideList);

					// Update the frame count
					m_pSlideList->m_ScanCurrentFrame++;

					// Images are now processed
					m_pDirMan->MarkDirProcessed(DirName);
				}

				if (m_pSlideList->m_bSkipScanRegion && autospotcounting)
				{
					OutputDebugString(_T("AUTOMATIC SPOT COUNTING STOP\r\n"));
					break;
				}

				// Give the find thread time to do something
				Sleep(100);
            }
            else 
                Sleep(100);     // Dont starve other threads

			// if we've been running for longer than x minutes without any action, finish up
			CTimeSpan howLong = CTime::GetCurrentTime() - time;
			if (IsExitRequested() || howLong.GetTotalSeconds() > (60 * 3))
				bTrawlSpotFolders = FALSE;
        }

        // Q End of pass, end of slide and end of everything
        QueuePassEnd(ScanPassPos, m_pSlideList);
        QueueSlideEnd(SlidePos, m_pSlideList);
        QueueEnd(m_pSlideList);
    }

    OutputDebugString(_T("PCVSCANNER: TRAWLER END\r\n"));
}

////////////////////////////////////////////////////////////////////
// Send image frame X, Y details
////////////////////////////////////////////////////////////////////

void CTrawler::SendFrameDetailsToQ(CSlideList *pSlides, 
								   float X, float Y, 
								   float Sx, float Sy, float Sz)
{
	// Q Ideal coordinates for the frame
	QueueFrameMeasurement(MEAS_DATATYPE_NUMBER_LONG, MEAS_TYPE_POSITION, FRAME_X_POS, (long)X, X, pSlides);
	QueueFrameMeasurement(MEAS_DATATYPE_NUMBER_LONG, MEAS_TYPE_POSITION, FRAME_Y_POS, (long)Y, Y, pSlides);
	// Upload a value of zero for Z - frameviewer needs it for some reason
	QueueFrameMeasurement(MEAS_DATATYPE_NUMBER_LONG, MEAS_TYPE_POSITION, FRAME_Z_POS, (long)Sz, Sz, pSlides);

	//Q the stage coordinates
	QueueFrameMeasurement(MEAS_DATATYPE_NUMBER_LONG, MEAS_TYPE_POSITION, STAGE_X_POS, (long)Sx, Sx, pSlides);
	QueueFrameMeasurement(MEAS_DATATYPE_NUMBER_LONG, MEAS_TYPE_POSITION, STAGE_Y_POS, (long)Sy, Sy, pSlides);
	QueueFrameMeasurement(MEAS_DATATYPE_NUMBER_LONG, MEAS_TYPE_POSITION, STAGE_Z_POS, (long)Sz, Sz, pSlides);
}

////////////////////////////////////////////////////////////////////
// Send calibration  X, Y scaling details to Q
////////////////////////////////////////////////////////////////////

void CTrawler::SendCalDetailsToQ(CSlideList *pSlides, double IX, double IY)
{
	// Q Image scales ideal coords/pixel
	QueueFrameMeasurement(MEAS_DATATYPE_NUMBER_DOUBLE, MEAS_TYPE_SCALE, FRAME_X_SCALE, (long)IX, IX, pSlides);
	QueueFrameMeasurement(MEAS_DATATYPE_NUMBER_DOUBLE, MEAS_TYPE_SCALE, FRAME_Y_SCALE, (long)IY, IY, pSlides);
}

////////////////////////////////////////////////////////////////////
// Send end of thread
////////////////////////////////////////////////////////////////////

BOOL CTrawler::SendEndThread(int Status)
{
    BOOL                Ok = FALSE;

    // First try and lock the queue
    if (m_pFindQ->Lock(TRUE, Q_TIMEOUT))
    {
        // Put the data on it
        if (m_pFindQ->Add(&Status, sizeof(Status), QEND) == CQueue::Ok)
            Ok = TRUE;

        m_pFindQ->ClearLock();
    }

    return Ok;
}

//////////////////////////////////////////////////////////////////////
// Q Frame measurements of various types
//////////////////////////////////////////////////////////////////////

void CTrawler::QueueFrameMeasurement(UINT MeasType, UINT MeasDataType, TCHAR title[], long lval, double dval, CSlideList* pSlideList)
{
	int ret;

	FrameMeas* frame_meas = new FrameMeas;
	frame_meas->MeasDataType = MeasType;
	frame_meas->MeasType     = MeasDataType;
	_stprintf(frame_meas->title, _T("%s"), title);
	frame_meas->lval         = lval;
	frame_meas->dval         = dval;


	// Add to Queue. must first lock it, this will block but can test if bWait is False
	BOOL added=FALSE;
	while (!added) 
	{
		if (m_pFindQ->Lock(TRUE, 10) == CQueue::LockOk) 
		{
			ret = m_pFindQ->Add(frame_meas, sizeof(FrameMeas), QFRAMEMEAS);
			// Any errors?
			if ((ret == CQueue::Full)||(ret == CQueue::NoLock))
			{
				added=FALSE;
				Sleep(100);
			}
			else
				added=TRUE;
			m_pFindQ->ClearLock();
		}
	}

	if (frame_meas)
		delete frame_meas;
}

//////////////////////////////////////////////////////////////////////
// Q a string for the script
//////////////////////////////////////////////////////////////////////

void CTrawler::QueueString(LPCTSTR Name, CString Value, CSlideList* pSlideList)
{
	// Add to Queue. must first lock it, this will block but can test if bWait is False
	BOOL added=FALSE;
	int ret;
	QString QSP;

	_tcscpy(QSP.Name, Name);
	_tcscpy(QSP.Value, Value);

	while (!added) 
	{
		if (m_pFindQ->Lock(TRUE, 10) == CQueue::LockOk) 
		{
			ret = m_pFindQ->Add(&QSP, sizeof(QSP), QSTRING);
			// Any errors?
			if ((ret == CQueue::Full)||(ret == CQueue::NoLock))
			{
				added=FALSE;
				Sleep(100);
			}
			else
				added=TRUE;
			m_pFindQ->ClearLock();
		}
	}
}

//////////////////////////////////////////////////////////////////////
// Q a tiff blob
//////////////////////////////////////////////////////////////////////

void CTrawler::QueueImage(CSlideList* pSlideList)
{
	// Add to Queue. must first lock it, this will block but can test if bWait is False
	BOOL added=FALSE;
	int ret;
	
	while (!added) 
	{
		if (m_pFindQ->Lock(TRUE, 10) == CQueue::LockOk) 
		{
			ret = QIM;
			ret = m_pFindQ->Add(&ret, sizeof(ret), QIM);	// QIM is the ID for images

			// Any errors?
			if ((ret == CQueue::Full)||(ret == CQueue::NoLock))
			{
				added=FALSE;
				Sleep(100);
			}
			else
				added=TRUE;
			m_pFindQ->ClearLock();
		}
	}
}

//////////////////////////////////////////////////////////////////////
// Q a new scan pass
//////////////////////////////////////////////////////////////////////

void CTrawler::QueueNewPass(POSITION pos, CSlideList* pSlideList)
{
	// Add to Queue. must first lock it, this will block but can test if bWait is False
	BOOL added=FALSE;
	
	while (!added) 
	{
		if (m_pFindQ->Lock(TRUE, 10) == CQueue::LockOk) 
		{
			int ret = m_pFindQ->Add(&pos, sizeof(POSITION), QNEWPASS);	
			// Any errors?
			if ((ret == CQueue::Full)||(ret == CQueue::NoLock))
			{
				Sleep(100);
				added=FALSE;
			}
			else
				added=TRUE;
			m_pFindQ->ClearLock();
		}
	}
}

//////////////////////////////////////////////////////////////////////
// Q a new slide
//////////////////////////////////////////////////////////////////////

void CTrawler::QueueNewSlide(POSITION pos, CSlideList* pSlideList)
{
	// Add to Queue. must first lock it, this will block but can test if bWait is False
	BOOL added=FALSE;

	while (!added) 
	{
		if (m_pFindQ->Lock(TRUE, 10) == CQueue::LockOk) 
		{
			int ret = m_pFindQ->Add(&pos, sizeof(POSITION), QNEWSLIDE);	// QEND is the ID for End
			// Any errors?
			if ((ret == CQueue::Full)||(ret == CQueue::NoLock))
			{
				Sleep(100);
				added=FALSE;
			}
			else
				added=TRUE;
			m_pFindQ->ClearLock();
		}
	}
}

//////////////////////////////////////////////////////////////////////
// Q end of pass
//////////////////////////////////////////////////////////////////////

void CTrawler::QueuePassEnd(POSITION pos, CSlideList* pSlideList)
{
	// Add to Queue. must first lock it, this will block but can test if bWait is False
	BOOL added=FALSE;
	
	while (!added) 
	{
		if (m_pFindQ->Lock(TRUE, 10) == CQueue::LockOk) 
		{
			int ret = m_pFindQ->Add(&pos, sizeof(POSITION), QPASSEND);	// QEND is the ID for End
			// Any errors?
			if ((ret == CQueue::Full)||(ret == CQueue::NoLock))
			{
				Sleep(100);
				added=FALSE;
			}
			else
				added=TRUE;

			m_pFindQ->ClearLock();
		}
	}
}

//////////////////////////////////////////////////////////////////////
// Q end of slide
//////////////////////////////////////////////////////////////////////

void CTrawler::QueueSlideEnd(POSITION pos, CSlideList* pSlideList)
{
	// Add to Queue. must first lock it, this will block but can test if bWait is False
	BOOL added=FALSE;

	while (!added) 
	{
		if (m_pFindQ->Lock(TRUE, 10) == CQueue::LockOk) 
		{
			int ret = m_pFindQ->Add(&pos, sizeof(POSITION), QSLIDEEND);	// QEND is the ID for End
			// Any errors?
			if ((ret == CQueue::Full)||(ret == CQueue::NoLock))
			{
				Sleep(100);
				added=FALSE;
			}
			else
				added=TRUE;
			m_pFindQ->ClearLock();
		}
	}
}

//////////////////////////////////////////////////////////////////////
// Q end of world
//////////////////////////////////////////////////////////////////////


void CTrawler::QueueEnd(CSlideList* pSlideList)
{
	// Add to Queue. must first lock it, this will block but can test if bWait is False
	BOOL added = FALSE;
	int i = 0;

	while (!added) 
	{
		if (m_pFindQ->Lock(TRUE, 10) == CQueue::LockOk) 
		{
			int ret = m_pFindQ->Add(&i, sizeof(int), QEND);	// QEND is the ID for End
			// Any errors?
			if ((ret == CQueue::Full)||(ret == CQueue::NoLock))
			{
				Sleep(100);
				added=FALSE;
			}
			else
				added=TRUE;

			m_pFindQ->ClearLock();
		}
	}
}
