////////////////////////////////////////////////////////////////////////////////
// Class for trawling directories and putting info on finder Qs
//
// Written By Karl Ratcliff 19092001
////////////////////////////////////////////////////////////////////////////////

#ifndef __CTRAWLER_H_
#define __CTRAWLER_H_
///////////////////////////////////////////////////////////////////
// Defines the name and size of the finder Q
///////////////////////////////////////////////////////////////////

#define CVFINDER_QNAME              _T("CVFINDERQ")

#define CVFINDER_QSIZE              10 * 1024 * 1024

// Second seconds timeout for access to the Q
#define Q_TIMEOUT       60000

class AI_Tiff;

///////////////////////////////////////////////////////////////////
// Defines the max number of images/spectrachromes
///////////////////////////////////////////////////////////////////

#define MAX_IMAGES                  10

///////////////////////////////////////////////////////////////////
// Filename for progress info
///////////////////////////////////////////////////////////////////


class CQueue;
class CDirManager;
class CSlideList;

class CTrawler {

public:
    CTrawler();
    ~CTrawler();

    BOOL Initialise(CString TempDir, CString QueueName);              // Initialise the class
    // Sets the path and directory structure to trawl for images
    void SetCaseToTrawl(LPCTSTR MachineName, LPCTSTR CaseName, LPCTSTR Slide, BOOL Reprocess);
    // Trawl a case/slide etc.
    void TrawlDirectory(CSlideList *pSlides, BOOL autospotcounting);//, int *NumImagesInQ);
    // An an end thread to the Q
    BOOL SendEndThread(int Status);
	// Set the obejcteive mag
	void SetObjectiveMag(int Mag) { m_nObjectiveMag = Mag; };
    // Set the scales
    void SetScales(double Sx, double Sy) { m_ImageScaleX = Sx; m_ImageScaleY = Sy; };
	// Set the assay
	void SetAssayAndScanArea(LPCTSTR AssayName, LPCTSTR InternalName, LPCTSTR ScanAreaName, LPCTSTR SessionId);
	// Delete the scores file
	void DeleteScoresFile();
	// Check if a end flag file has been detected, deletes file
	BOOL IsExitRequested();

private:
	// Upload stage and ideal coordinates to DB
	BOOL UploadCoords(CString &DirName);
	// Upload the frame id
	BOOL UploadFrameId(CString &DirName);
	// Upload path to region mask if it exists
	BOOL UploadFrameMaskPath(CString &DirName);
    // Reads a RAW TIFF image straight into memory
    void *ReadRAWTiffFile(LPCTSTR FileName, int *Width, int *Height, int *BPP);
	// Get objective calibration data
	BOOL GetObjectiveCalData(float *ImageScaleX, float *ImageScaleY, float *StageScaleX, float *StageScaleY);
    // Send XY Ideal and Stage Frame Position to Q
    void SendFrameDetailsToQ(CSlideList *pSlides, float X, float Y, float Sx, float Sy, float Sz);
	// Set calibration details to the Q
	void SendCalDetailsToQ(CSlideList *pSlides, double Ix, double Iy);
	// Q Various frame measurements
	void QueueFrameMeasurement(UINT MeasType, UINT MeasDataType, TCHAR title[], long lval, double dval, CSlideList* pSlideList);
	// Q A TIFF Blob
	void QueueImage(CSlideList* pSlideList);
	// Q A New Scan Pass
	void QueueNewPass(POSITION pos, CSlideList* pSlideList);
	// Q A New Slide
	void QueueNewSlide(POSITION pos, CSlideList* pSlideList);
	// Q the end of a pass
	void QueuePassEnd(POSITION pos, CSlideList* pSlideList);
	// Q the end of a slide
	void QueueSlideEnd(POSITION pos, CSlideList* pSlideList);
	// Q the end of the world
	void QueueEnd(CSlideList* pSlideList);
	// Q a string for the script
	void QueueString(LPCTSTR Name, CString Value, CSlideList* pSlideList);

public:
    CQueue      *m_pFindQ;              // Queue to put data on

private:
    CString     *m_pTrawlPath;          // Path to trawl for new data
	CString		*m_pCasePath;
	CString		m_SessionId;			// Unique name for a session

    // Directory management
    CDirManager *m_pDirMan;   
	// Pointer to the slide list
	CSlideList	*m_pSlideList;
	// Objecteive magnification
	int			m_nObjectiveMag;
	// Calibration data
	double		m_ImageScaleX, m_ImageScaleY;
	// Set the assay
	CString		*m_pAssayName;
	CString		*m_pInternalAssayName;
	// Set the scan area name
	CString		*m_pScanAreaName;
	// Reprocess flag
	BOOL		m_Reprocess;
};

#endif