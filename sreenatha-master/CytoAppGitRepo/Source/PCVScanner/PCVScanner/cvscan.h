/////////////////////////////////////////////////////////////////////////////
//
// Shared CV Scan Defs etc.
//
// Written By Karl Ratcliff 17092001
//
//////////////////////////////////////////////////////////////////////////////

#ifndef __CVSCAN_H
#define __CVSCAN_H

// Application defined messages
// Message with the current total number of cells
//#define WM_NUMCELLS_FOUND           WM_USER + 1
// Stop the autoscan function
//#define WM_STOPAUTOSCAN             WM_USER + 2

// This defines the max numbner of images that autocapture can store
// without them being processed by the finder thread
#define MAX_AUTOCAPTURE_LIMIT       2048

// Number of CLI arguments expected by the software
#define NUM_CLI_ARGS                9

// System wide named events and synchronisation objects

// Defines the number of images in the Q - this is incremented
// by the trawler thread and decremented by the finder thread
   #define CVSCAN_PROCESSNAME      _T("CVSCANNER.EXE")




#endif