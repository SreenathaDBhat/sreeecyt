/////////////////////////////////////////////////////////////////////////////
// cvscanner.cpp : Defines the class behaviors for the application.
//
// CVSCAN process main 
// Written By Karl Ratcliff 17092001
//
/////////////////////////////////////////////////////////////////////////////
//
// NOTE To launch this process, it needs handle of the autocapture window,
// the assay name, case name and slide name as arguments in this order.
//
// CVScanner <X> <HWND> <ASSAY NAME> <CASE NAME> <SLIDE NAME>
//
// In order for this to work a few synchronisation objects and one
// windows message are used.
//
// A single event is also used which signals the end of the cvscanner
// process. See CVSCAN_END
//
// One windows user message is used (WM_STOPAUTOCAPTURE) to signal to 
// autocapture that a predefined limit has been reached for finding
// by the script, thus allowing autocapture to end.

#include "stdafx.h"
#include "direct.h"

#include "aiqueue.h"
#include "threaddefs.h"
#include "cvscan.h"
#include "cvscanner.h"
#include "ctrawler.h"
#include "procapi.h"
#include "registry.h"
#include "mlock.h"
#include "CFuncs.h"

// MDS 2 includes

#include "assay.h"
#include "scanpass.h"
#include "scanpasslist.h"
#include "AITIFF.h"
#include "spectrachrome.h"
#include "spectrachromelist.h"
#include "slide.h"
#include "slidelist.h"
#include "reghelper.h"
#include "findthread.h"
#include "capable.h"
#include "resource.h"
#include "aiishared.h"
#include "..\..\Common\Src\NLog\src\NLogC\NLogC.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

int captureMode;

#define StringAlloc(x)		malloc(x * sizeof(TCHAR))

/////////////////////////////////////////////////////////////////////////////
// PcvScannerApp
/////////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(PcvScannerApp, CWinApp)
	//{{AFX_MSG_MAP(PcvScannerApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PcvScannerApp construction
/////////////////////////////////////////////////////////////////////////////

PcvScannerApp::PcvScannerApp()
{
	m_reprocess		   = FALSE;
	m_autospotcounting = TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only PcvScannerApp object

PcvScannerApp theApp;

/////////////////////////////////////////////////////////////////////////////
// PcvScannerApp initialization
/////////////////////////////////////////////////////////////////////////////

BOOL PcvScannerApp::InitInstance()
{
    CTrawler				*pTrawler;
	CSlideList				*pSlides;				// Use MDS2 Slidelist class
	CSlide					*pTheSlide;				// A single slide to put into the slide list
	int						ObjectiveMag;
	TCHAR					CVAppDataDir[MAX_PATH];
	CString					FinderVarsName, TempDir;
	CString					dbgstr;
	CString					CLI;
    double                  FrameXScale, FrameYScale;
	CFindThread				*pFinder;
	CString					guidId;

    try
    {
		CString cfgpath;
		TCHAR ExeDir[_MAX_PATH];
		
		if (GetExecDir(ExeDir, _MAX_PATH))
			cfgpath.Format(_T("%s\\cvscanner.config"), ExeDir);
		else
			cfgpath.Format(_T("cvscanner.config"));

		if (NLog_ConfigureFromFile(cfgpath) == 0)
		{
			OutputDebugString(_T("**************************************************************************\r\n"));
			OutputDebugString(_T("********* cvscanner.config FILE FAILED TO LOAD\r\n"));
			OutputDebugString(cfgpath);
			OutputDebugString(_T("\r\n**************************************************************************\r\n"));
		}

		NLog_Init(_T("PCVSCANNER"));

		// Parse the command line and output a message if not enough args
        CLI = m_lpCmdLine;
		
		TCHAR callingProcess[MAX_PATH];
		TCHAR imageFramesFile[MAX_PATH];
		TCHAR pAssayName[MAX_PATH], pCaseFolderPath[MAX_PATH], pCaseName[MAX_PATH], pSlideName[MAX_PATH];
		TCHAR pScanAreaName[MAX_PATH];
		TCHAR pSessionId[MAX_PATH]; // ID for scanArea casename_slidename_scanarea

        if  (_stscanf(CLI, 
            _T("%s %s %s %s %s %s %d %d %d %lf %lf %s %s"), 
			pSessionId,						// Id created for this scanarea to allow naming for multiple pcvscanners to run, 1 per scanarea/session
			pCaseFolderPath,				// e.g. \\MyServerName
			pAssayName,
			pScanAreaName,
            pCaseName,						// The case name
            pSlideName,						// The slide name
            &ObjectiveMag,						// The objective magnification
            &m_autospotcounting,				// AX spot counting
            &m_reprocess,						// Reprocessing mode
            &FrameXScale, 
            &FrameYScale,
			callingProcess,
			imageFramesFile) != 13)				    // Frame image scales
        {
            // Output a suitable message
            NLog_Error(_T("PCVSCANNER"), _T("Not enough CLI arguments %s"), m_lpCmdLine);
            // Do not run anything
            return FALSE;
        }

		// assay and image files root name from probecaseview call
		CString rootFileName(imageFramesFile);
		CString assayName(pAssayName);
		CString scanAreaName(pScanAreaName);

		// we fill spaces with % to make cmd line easier, replace no
		rootFileName.Replace("%", " "); 
		assayName.Replace("%", " ");
		scanAreaName.Replace("%", " ");

		CString scanAreaEventName(_T("PCVEVENT_"));
		scanAreaEventName += pSessionId;

		// Creates or opens an existing event
		HANDLE hScanProcessingEvent = CreateSystemEvent(scanAreaEventName);
		if (hScanProcessingEvent == NULL)
		{
            NLog_Error(_T("PCVSCANNER:"), _T("Cant create scan processing event %s"), scanAreaEventName);
			return FALSE;
		}

		TRACE1(_T("PCVSCANNER: Create ScanArea Event %s"), scanAreaEventName);

		// Check if its signalled if so processing is already happening so can exit and let other process handle
		if (WaitForSingleObject(hScanProcessingEvent, 0) != WAIT_TIMEOUT) // Non-blocking
		{
			TRACE0("PCVSCANNER: another pcvscanner already processing\r\n");
			return TRUE;
		}

		// Signal we are processing
		SetEvent(hScanProcessingEvent);

		//ASSERT(0);

        CoInitialize(NULL);

        // Get the CV APP Data dir
        if(!GetTempPath(MAX_PATH, CVAppDataDir))
        {
            NLog_Error(_T("PCVSCANNER"), _T("Can't get Temp folder path %s"), CVAppDataDir);
            return FALSE;
        }

		// Create a guid Id to be used to name queue's as these need to be unique for multiple processes to run
		// i.e. a queue creates global mutex and events
		guidId = NewGuidString();

        // Create a temp directory and make it if neccessary
        TempDir.Format(_T("%s"), CVAppDataDir);
        TempDir += _T("\\PCVSCANNER_");
		TempDir += guidId;
        _tmkdir(TempDir);

		TRACE1(_T("Created Temp folder %s"), TempDir);

		// Note that FinderVarsName is just the file name - it will be appended to the path created
		// in TempDir above, when the find thread calls CScriptProcessor::InitialiseScriptProcessor().
		FinderVarsName = _T("CVSCANVARS.DAT");

        // Create the actual class that does the work
        pTrawler = new CTrawler;

        TRACE2(_T("PCVSCANNER: Launched. cases root = %s, CLI = %s"), pCaseFolderPath, m_lpCmdLine);

		// statically assign this, all static methods can access it
		CAII_Shared::CaseBasePath = pCaseFolderPath;
		

        // Create a single slide to add to the slide list
        pTheSlide = new CSlide;
		pTheSlide->m_FileRoot.Format(_T("%s\\Cases\\%s\\%s"), pCaseFolderPath, pCaseName, pSlideName);
		pTheSlide->m_Assay = NULL;

		CAssay * pNamedAssay = new CAssay();

		CFileFind CFF;

		CString assayPath("");
		assayPath.Format(_T("%s\\%s"), pTheSlide->m_FileRoot, assayName);

		if (pNamedAssay->LoadFromPath(assayPath))
			pTheSlide->m_Assay = pNamedAssay;
		else
			NLog_Error(_T("PCVSCANNER"), _T("CAN'T FIND REQUESTED ASSAY IN SLIDE FOLDER"));

		pTheSlide->m_InternalAssayName = pTheSlide->m_Assay->m_Name;

		if (!pTheSlide->m_Assay)
		{
			CString msg;
			NLog_Error(_T("PCVSCANNER"), _T("Could not find an assay named %s"), assayName);
			return FALSE;
		}		
        
        // Create the MDS2 slide list class
        pSlides = new CSlideList;

        // Set Q's up
        pSlides->m_findQ = new CQueue();
		        
		// queue name needs to be unique as it uses asystem event with the name, bit pointless as same process
		CString queueName(CVFINDER_QNAME);
		queueName += guidId;

        if (pSlides->m_findQ->Initialize(CVFINDER_QSIZE, TempDir, queueName)!= CQueue::Ok)
        {
            NLog_Error(_T("PCVSCANNER"), _T("Can't Initialize Queue %s"), TempDir);
            return FALSE;
        }

        // Set the scan Q
        pSlides->m_scanQ = pTrawler->m_pFindQ;

        CoInitialize(NULL);
   
        CTime theTime;
        theTime = CTime::GetCurrentTime();
        CString scan_node_name = theTime.Format(_T("%b %d %y, %H.%M.%S"));

		if (_tcsstr(imageFramesFile, _T("NULL")) != 0) // inferr the image frames file name
		{
			if (_tcsstr(pScanAreaName, _T("NULL")) != 0)
				pTheSlide->m_FramesFile.Format(_T("%s\\%s.imageframes"), pTheSlide->m_FileRoot, assayName);
			else
				pTheSlide->m_FramesFile.Format(_T("%s\\%s_%s.imageframes"), pTheSlide->m_FileRoot, assayName, scanAreaName);
		}
		else // use the one given
		{
			pTheSlide->m_FramesFile.Format(_T("%s\\%s.imageframes"), pTheSlide->m_FileRoot, rootFileName);
		}

        // Fill it in
		pTheSlide->m_RootName = rootFileName;
		pTheSlide->m_AssayName.Format(_T("%s"), assayName);

		// Determine if in a session from the arsey name
		int F = pTheSlide->m_AssayName.Find(_T("$=Session"));

		if (F == -1)
			pTheSlide->m_SessionName = _T("");								// No session
		else
			pTheSlide->m_SessionName = pTheSlide->m_AssayName.Mid(F, _tcslen(_T("$=Session 1")));

        pSlides->m_list.AddHead(pTheSlide);						// Add a single slide to the list
        pSlides->m_pos = pSlides->m_list.GetHeadPosition();		// Start with this slide
       
        // Set up our trawler/scanner thread
        if (!pTrawler->Initialise(TempDir, queueName))
            return FALSE;

        // Use machine name etc to construct a path to case base
        // Load in the assays file
		// Set the assay
		pTrawler->SetAssayAndScanArea(assayName, pTheSlide->m_InternalAssayName, scanAreaName, pSessionId);
        pTrawler->SetCaseToTrawl(pCaseFolderPath, pCaseName, pSlideName, m_reprocess);

        // Set the objective power
        pTrawler->SetObjectiveMag(ObjectiveMag);
        pTrawler->SetScales(FrameXScale, FrameYScale);

		if (m_reprocess)
		{
			pTrawler->DeleteScoresFile();
		}
        // Set the process name so that CVSCANNER has its own find output dir
        pSlides->m_ProcessName = _T("CVSCAN");
		pSlides->m_CallingProcess = callingProcess;
	
		pFinder = new CFindThread();

        ////////////////////////////////////////////////////////
        // Now launch the CV find with its upload function
        ////////////////////////////////////////////////////////
        pSlides->OnFind(TempDir, FinderVarsName, CString(_T("CVSCANVB")) + guidId);

		pFinder->Launch(pSlides);

		// throw away any old end request, start afresh
		pTrawler->IsExitRequested();

        // Now start the process trawling CV output directories for information, will TIMEOUT if idle for a significant period
        pTrawler->TrawlDirectory(pSlides, m_autospotcounting);

		// Reset as we have stopped processing due to timeout
		ResetEvent(hScanProcessingEvent);
		CloseHandle(hScanProcessingEvent);

        // Wait until the finder thread ends before stopping completely
        WaitForSingleObject(pFinder->m_hFindThread, INFINITE);

		// Delete the locking object
		delete pTheSlide->m_applock;

		pTheSlide->m_applock = NULL;

        delete pSlides->m_findQ;
        delete pTrawler;

		// Tidy up temp folder
		DeleteDirectoryTree(TempDir);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        // NOTE if we delete this then CV scanner crashes on exit. This is probably due to the database
        // stuff running in two separate threads but if you do not upload anything to the database then
        // it goes bang. Unfortunately, the database stuff needs to be here as we cannot get the find
        // thread to update case details from wincv.
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //delete pSlides;

        CoUninitialize();

        CoUninitialize();

        TRACE0("PCVSCANNER: EXIT\r\n");

		// NOTE return FALSE to exit here without start the main app message pump
        return FALSE;
    }
    catch(...)
    {
        NLog_Fatal(_T("PCVSCANNER"),_T("PCVSCANNER EXCEPTION RAISED"));
        return FALSE;
    }
}

// Greate a new GUID string
CString PcvScannerApp::NewGuidString()
{
	wchar_t		szGUID[39];
	GUID		guid;

	CoCreateGuid(&guid);
	::StringFromGUID2 (guid, szGUID, 39);

	CString str = szGUID;
	return str;
}

HANDLE PcvScannerApp::CreateSystemEvent(LPCTSTR eventName) 
{
    SECURITY_ATTRIBUTES  sa;
    SECURITY_DESCRIPTOR  sd;

    InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION);
    
    sa.nLength = sizeof(sa);
    sa.bInheritHandle = TRUE;       // This must be TRUE so that cvscanner can inherit the handles
    sa.lpSecurityDescriptor = &sd;
    
    HANDLE hEvent = CreateEvent(&sa, TRUE, FALSE, eventName);
    
	return hEvent;
 }




/////////////////////////////////////////////////////////////////////////////
// Clear up here
/////////////////////////////////////////////////////////////////////////////

int PcvScannerApp::ExitInstance() 
{
	return CWinApp::ExitInstance();
}

