/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// cvscanner.h : main header file for the CVSCANNER application
//
// Written by Karl Ratcliff 18092001
/////////////////////////////////////////////////////////////////////////////
//
//

#if !defined(AFX_CVSCANNER_H__7697EDC5_ABBB_11D5_9E30_84D0B63E3838__INCLUDED_)
#define AFX_CVSCANNER_H__7697EDC5_ABBB_11D5_9E30_84D0B63E3838__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols


//class CVFindThread;
class CSlide;
class CDBNavigator;

extern "C" int captureMode;
extern "C" short screen_mode;

// BODGE BODGE BODGE BODGE BODGE BODGE for clr stuff
extern "C" int StartSpotCounting(int x)
{
    return x;
}

/////////////////////////////////////////////////////////////////////////////
// PcvScannerApp:
// See cvscanner.cpp for the implementation of this class
// Main process happens in the InitInstance function of this class
/////////////////////////////////////////////////////////////////////////////

class PcvScannerApp : public CWinApp
{
public:
	PcvScannerApp();

	BOOL			m_autospotcounting;
	BOOL			m_reprocess;

private:
	HANDLE			CreateSystemEvent(LPCTSTR eventName);
	CString			NewGuidString();



// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PcvScannerApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(PcvScannerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CVSCANNER_H__7697EDC5_ABBB_11D5_9E30_84D0B63E3838__INCLUDED_)
