/////////////////////////////////////////////////////////////////////////////////////
// Directory management for CVScanner
// Creates a class which manages a list of sub-directories off the case/slide
// directory. This class is stored on disk and keeps a track of each analysed
// set of probe images. 
// Written By Karl Ratcliff 26092001
/////////////////////////////////////////////////////////////////////////////////////

#include "spotfiles.h"
#include "dirman.h"
#include "cvscan.h"

////////////////////////////////////////////////////////////////////////////////////////
// Class maintains a list of the above structures, detects when new sub directories 
// are created and which ones have been processed.
////////////////////////////////////////////////////////////////////////////////////////

// Constructor/Destructor

CDirManager::CDirManager()
{
    m_pBaseDirectory = NULL;
	SetAssayAndScanArea(_T(""), _T(""), NULL_SCAN_AREA_NAME);
}

CDirManager::~CDirManager()
{
}


void CDirManager::GetScoresFileName(CString &ScoresFile)
{
	if (m_ScanAreaName == NULL_SCAN_AREA_NAME)
		ScoresFile.Format("%s\\%s.scores", *m_pBaseDirectory, m_AssayName);
	else
		ScoresFile.Format("%s\\%s_%s.scores", *m_pBaseDirectory, m_AssayName, m_ScanAreaName);
}

void CDirManager::GetDataFileName(CString &DataFile)
{
	if (m_ScanAreaName == NULL_SCAN_AREA_NAME)
		DataFile.Format("%s\\%s.txt", *m_pBaseDirectory, m_AssayName);
	else
		DataFile.Format("%s\\%s_%s.txt", *m_pBaseDirectory, m_AssayName, m_ScanAreaName);
}

void CDirManager::GetOverlaysFileName(CString &DataFile)
{
	if (m_ScanAreaName == NULL_SCAN_AREA_NAME)
		DataFile.Format("%s\\%s.overlays", *m_pBaseDirectory, m_AssayName);
	else
		DataFile.Format("%s\\%s_%s.overlays", *m_pBaseDirectory, m_AssayName, m_ScanAreaName);
}

/////////////////////////////////////////////////////////////////////////////////////////
// Init the dir manager with a base directory from which the sub-directories will be 
// monitored.
/////////////////////////////////////////////////////////////////////////////////////////

void CDirManager::Initialise(LPCTSTR BaseDir)
{
    m_pBaseDirectory = new CString;
    m_pBaseDirectory->Format("%s", BaseDir);

}

/////////////////////////////////////////////////////////////////////////////////////////
// Build a list of directories
// Returns TRUE if a dir has been added to the list, NewDirName = new dir to process
/////////////////////////////////////////////////////////////////////////////////////////

BOOL CDirManager::BuildList(BOOL autospotcounting, CString& NewDirName)
{
	WIN32_FIND_DATA     DirFindInfo, FileFindInfo, AssayFindInfo;
	HANDLE              hSubDirFind;
	HANDLE              hProcFileFind;
	HANDLE				hProcAssayNameFind;
	BOOL                DirOk = TRUE, CheckForIdealFile, OKbutdontuseIdealFile = FALSE;
	CString             DirName, FileName, AssayFileName, CellAssay, ScanAreaName;
	BOOL                OneToGo = FALSE, ProcessedAlready;
	BOOL				checkedCell = FALSE;
	BOOL				checkedSpot = FALSE;

	// Holds the cell directory
	NewDirName = "";

	do
	{
		hSubDirFind = INVALID_HANDLE_VALUE;

		if(!checkedCell)
		{
			// Now create the initial list of subdirs of the basedir
			DirName.Format("%s\\Cell*", *m_pBaseDirectory);

			// Find the most recent newly created cell directory 
			// as this will have the latest image files in for processing
			hSubDirFind = FindFirstFile(DirName, &DirFindInfo);
			checkedCell = TRUE;
		}
		else
		{
			if (!checkedSpot)
			{
				DirName.Format("%s\\Spot*", *m_pBaseDirectory);

				// Find the most recent newly created cell directory 
				// as this will have the latest image files in for processing
				hSubDirFind = FindFirstFile(DirName, &DirFindInfo);
				checkedSpot = TRUE;
			}
		}

		// Found a cell directory
		if (hSubDirFind != INVALID_HANDLE_VALUE)
		{
			do
			{
				// Search fopr the presence of the PROC.CVS file
				FileName.Format("%s\\%s\\" MARK_PROCESSED_FILE, *m_pBaseDirectory, DirFindInfo.cFileName);

				// Remember the cell directory level
				NewDirName.Format("%s\\%s", *m_pBaseDirectory, DirFindInfo.cFileName);

				// Check for proc.cvs
				hProcFileFind = FindFirstFile(FileName, &FileFindInfo);
				// If the file was found then it has been processed
				if (hProcFileFind == INVALID_HANDLE_VALUE)
					ProcessedAlready = FALSE;
				else
					ProcessedAlready = TRUE;

				FindClose(hProcFileFind);

				CheckForIdealFile = FALSE;

				// Not a cell directory that has already been processed so check the contents
				// to make sure that the files are there
				if (!ProcessedAlready)
				{		
					// Check to make sure the cell is to be processed with this assay
					CellAssay = _T("");
					AssayFileName.Format("%s\\%s\\" ASSAYNAME_FILE, *m_pBaseDirectory, DirFindInfo.cFileName);
					hProcAssayNameFind = FindFirstFile(AssayFileName, &AssayFindInfo);
					if (hProcAssayNameFind != INVALID_HANDLE_VALUE)
					{
						CStdioFile CSF;
						CFileException FE;

						// Look for an assay match
						CellAssay = _T("");
						ScanAreaName = NULL_SCAN_AREA_NAME;
						if (CSF.Open(AssayFileName, CFile::modeRead, &FE))
						{
							CSF.ReadString(CellAssay);
							CSF.ReadString(ScanAreaName);
							CSF.Close();
							CellAssay.Remove(_T('\r'));
							ScanAreaName.Remove(_T('\r'));
						}
					}
					FindClose(hProcAssayNameFind);

					if (CellAssay.CompareNoCase(m_AssayName) == 0 || CellAssay.CompareNoCase(m_InternalAssayName) == 0)
					{
						if (ScanAreaName.CompareNoCase(m_ScanAreaName) == 0)
						{
							// Check for the frame id
							FileName.Format("%s\\%s\\*.ID", *m_pBaseDirectory, DirFindInfo.cFileName);

							hProcFileFind = FindFirstFile(FileName, &FileFindInfo);
							FindClose(hProcFileFind);

							if (hProcFileFind != INVALID_HANDLE_VALUE)		// If there is no FRAME.ID then it is not a SPOT cell
							{
								OneToGo = TRUE;
								TRACE1("PCVSCANNER: Adding Dir %s\r\n", NewDirName);
							}
						}
					}
				}

			}
			while (FindNextFile(hSubDirFind, &DirFindInfo) && !OneToGo);

			// Close the find
			FindClose(hSubDirFind);
		}

		if(OneToGo)
			break;

	} while( !checkedCell || !checkedSpot);

	return OneToGo;        // At least one sub-dir needs processing
}
    
/////////////////////////////////////////////////////////////////////////////////////////
// Clears the reprocessed flags out of the cell directories
/////////////////////////////////////////////////////////////////////////////////////////

void CDirManager::ClearList(void)
{
    WIN32_FIND_DATA     DirFindInfo, FileFindInfo, AssayFindInfo;
    HANDLE              hSubDirFind;
    HANDLE              hProcFileFind;
	HANDLE				hProcAssayNameFind;
    CString             DirName, FileName, CellAssay, ScanAreaName;
	BOOL				checkedCell = FALSE;
	BOOL				checkedSpot = FALSE;

	do
	{
		hSubDirFind = INVALID_HANDLE_VALUE;

		if(!checkedCell)
		{
			// Now create the initial list of subdirs of the basedir
			DirName.Format("%s\\Cell*", *m_pBaseDirectory);

			// Find the most recent newly created cell directory 
			// as this will have the latest image files in for processing
			hSubDirFind = FindFirstFile(DirName, &DirFindInfo);
			checkedCell = TRUE;
		}
		else
		{
			if (!checkedSpot)
			{
				DirName.Format("%s\\Spot*", *m_pBaseDirectory);

				// Find the most recent newly created cell directory 
				// as this will have the latest image files in for processing
				hSubDirFind = FindFirstFile(DirName, &DirFindInfo);
				checkedSpot = TRUE;
			}
		}

		// Found a cell directory
		if (hSubDirFind != INVALID_HANDLE_VALUE)
		{
			do
			{
				// Search fopr the presence of the PROC.CVS file
				FileName.Format("%s\\%s\\" ASSAYNAME_FILE, *m_pBaseDirectory, DirFindInfo.cFileName);

				hProcAssayNameFind = FindFirstFile(FileName, &AssayFindInfo);
				if (hProcAssayNameFind != INVALID_HANDLE_VALUE)
				{
					CStdioFile CSF;
					CFileException FE;
					
					// Look for an assay match
					CellAssay = _T("");
					ScanAreaName = NULL_SCAN_AREA_NAME;
					if (CSF.Open(FileName, CFile::modeRead, &FE))
					{
						CSF.ReadString(CellAssay);
						CSF.ReadString(ScanAreaName);
						CSF.Close();
						CellAssay.Remove(_T('\r'));
						ScanAreaName.Remove(_T('\r'));
					}

					if (CellAssay.CompareNoCase(m_AssayName) == 0 && ScanAreaName.CompareNoCase(m_ScanAreaName) == 0)
					{
						// Search fopr the presence of the PROC.CVS file
						FileName.Format("%s\\%s\\" MARK_PROCESSED_FILE, *m_pBaseDirectory, DirFindInfo.cFileName);

						// Check for proc.cvs
						hProcFileFind = FindFirstFile(FileName, &FileFindInfo);
						// If the file was found then it has been processed
						if (hProcFileFind != INVALID_HANDLE_VALUE)
							DeleteFile(FileName);

						FindClose(hProcFileFind);
					}
				}

				FindClose(hProcAssayNameFind);
			}
			while (FindNextFile(hSubDirFind, &DirFindInfo));
	        
			// Close the find
			FindClose(hSubDirFind);
		}

	} while( !checkedCell || !checkedSpot);

}

/////////////////////////////////////////////////////////////////////////////////////////
// Delete the scores file
/////////////////////////////////////////////////////////////////////////////////////////

void CDirManager::DeleteScoresFile(void)
{
    WIN32_FIND_DATA     FileFindInfo;
    HANDLE              hProcFileFind;
    CString             FileName;
 
    // Now create the initial list of subdirs of the basedir
    //FileName.Format("%s\\%s.scores", *m_pBaseDirectory, Assay);
	GetScoresFileName(FileName);

    // Find the most recent newly created cell directory 
    // as this will have the latest image files in for processing
    hProcFileFind = FindFirstFile(FileName, &FileFindInfo);

    // Found a cell directory
    if (hProcFileFind != INVALID_HANDLE_VALUE)
    {
        DeleteFile(FileName);
        
        // Close the find
        FindClose(hProcFileFind);

    }

	// Delete the results.txt file associated with it
    GetDataFileName(FileName);

    // Find the most recent newly created cell directory 
    // as this will have the latest image files in for processing
    hProcFileFind = FindFirstFile(FileName, &FileFindInfo);

    // Found a cell directory
    if (hProcFileFind != INVALID_HANDLE_VALUE)
    {
        DeleteFile(FileName);
        
        // Close the find
        FindClose(hProcFileFind);

    }
}
	
#define SCOREDCELL_ID					_T("ScoredCell Id=\"")

void CDirManager::DeleteFragResults(void)
{
	CString ScoresFile;
	int T = _tcslen(SCOREDCELL_ID);

	GetScoresFileName(ScoresFile);

	 // Open Up File To Read From
    CStdioFile TheFile;
    CFileException FileErr;
    
    // Try and open the file 
    if (TheFile.Open(ScoresFile, CFile::modeRead | CFile::shareDenyNone, &FileErr))
	{
		BOOL FResult;
		CString FileLine;

		// Get the png files and delete
		do
		{
			FResult = TheFile.ReadString(FileLine);
			if (FResult)
			{
				int IdPos = FileLine.Find(SCOREDCELL_ID);
				if (IdPos != -1)
				{
					
					CString FragFile, FragFileWC;

					FragFile = FileLine.Mid(IdPos + T, 36);
					
					FragFileWC.Format(_T("%s\\%s.*.png"),  *m_pBaseDirectory, FragFile);

					CFileFind finder;
					// Find all files of type .png
					BOOL Found = finder.FindFile(FragFileWC);
					while (Found)
					{
						Found = finder.FindNextFile();
						CString Name = finder.GetFilePath();
						DeleteFile(Name);
					}
					finder.Close();

					FragFileWC.Format(_T("%s\\%s.thumb.blb"),  *m_pBaseDirectory, FragFile);

					// Find all files of type .png
					Found = finder.FindFile(FragFileWC);
					while (Found)
					{
						Found = finder.FindNextFile();
						CString Name = finder.GetFilePath();
						DeleteFile(Name);
					}
					finder.Close();					
				}
				
			}
		}
		while (FResult);

		TheFile.Close();
	}	
}


/////////////////////////////////////////////////////////////////////////////////////////
// Marks the directory specified by the position P as processed. This is simply done
// by dropping a PROC.CVS file in there.
/////////////////////////////////////////////////////////////////////////////////////////

void CDirManager::MarkDirProcessed(CString& DirName)
{
    CString         FileName;
    CFile           F;
    CFileException  fe;
    
    // Search fopr the presence of the PROC.CVS file
    FileName.Format("%s\\" MARK_PROCESSED_FILE, DirName);
    
    // Create the processed file
    if (F.Open(FileName, CFile::modeCreate | CFile::modeWrite, &fe))
        F.Close();
}


