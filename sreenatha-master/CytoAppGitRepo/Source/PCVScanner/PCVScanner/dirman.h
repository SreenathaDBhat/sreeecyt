/////////////////////////////////////////////////////////////////////////////////////
// Directory management for CVScanner
// Creates a class which manages a list of sub-directories off the case/slide
// directory. This class is stored on disk and keeps a track of each analysed
// set of probe images. 
// Written By Karl Ratcliff 26092001
/////////////////////////////////////////////////////////////////////////////////////

#ifndef CVSCAN_DIRMAN_H
#define CVSCAN_DIRMAN_H

#include "stdafx.h"
#include <afxtempl.h>



////////////////////////////////////////////////////////////////////////////////////////
// Class maintains a list of the above structures, detects when new sub directories 
// are created and which ones have been processed.
////////////////////////////////////////////////////////////////////////////////////////

class CDirManager {
public:
    CDirManager();
    ~CDirManager();
    void Initialise(LPCTSTR BaseDir);
    BOOL BuildList(BOOL autospotcounting, CString& NewDirName);
    POSITION GetDirToProcess(CString& DirName);
    void MarkDirProcessed(CString& DirName);
	void ClearList(void);
	void DeleteScoresFile(void);
	void SetAssayAndScanArea(LPCTSTR AssayName, LPCTSTR InternalAssayName, LPCTSTR ScanAreaName) { m_AssayName.Format(_T("%s"), AssayName); m_ScanAreaName.Format(_T("%s"), ScanAreaName); m_InternalAssayName.Format(_T("%s"), InternalAssayName); }
	void GetScoresFileName(CString &ScoresFile);
	void GetDataFileName(CString &DataFile);
	void GetOverlaysFileName(CString &DataFile);
	void DeleteFragResults(void);

private:
    CString                     *m_pBaseDirectory;  // Sets the base directory from which the sub-dirs are managed
	CString						m_AssayName;
	CString						m_ScanAreaName;
	CString						m_InternalAssayName;
};


#endif