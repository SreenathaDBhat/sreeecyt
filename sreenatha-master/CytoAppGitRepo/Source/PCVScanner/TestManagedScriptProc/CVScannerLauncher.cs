﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using System.Globalization;


namespace AI
{
    public class CVScannerController
    {
        private PCVScannerLauncher cvScanner;
        private string _casebaseRoot;
        private string _caseName;
        private string _slideName;
        private string _scanAreaName;
        private string _imageFramesName;
        private string _assayName;
        private double _objective, _frameXScale, _frameYScale;

        public CVScannerController(string casebaseRoot, string caseName, string slideName, string scanAreaName, string imageFramesName, string assayName, double objective, double frameXScale, double frameYScale)
        {
            _casebaseRoot = casebaseRoot;
            _caseName = caseName;
            _slideName = slideName;
            _scanAreaName = scanAreaName;
            _imageFramesName = imageFramesName;
            _assayName = assayName;
            _objective = objective;
            _frameXScale = frameXScale;
            _frameYScale = frameYScale;
        }

        public void StartCVScanner(string assayname)
        {
            this._assayName = assayname;

            cvScanner = new PCVScannerLauncher(_caseName, _slideName, _scanAreaName,
                                                  _assayName, _objective, _frameXScale, _frameYScale, _imageFramesName);
            cvScanner.Launch();
        }
    }
 
 
    /// <summary>
    /// Launches the pcvscanner process with appropriate cmdline parameters
    /// NOTE: pcvScanner will run for about 5 minutes in idle state then exit but will extend life if new scores added
    /// lifetime control is by the pcvscanner process itself based on ScanAreaId. Only one process per ScanAreaId
    /// </summary>
    public class PCVScannerLauncher
    {
        private Process cvscanner;
        private string ScanAreaId, rootPath, caseName, slideName, scanArea, assayName, imageFramesName;
        private double objective, frameXScale, frameYScale;
        private int autospotCounting = 0;
        private int reprocess = 0;

        public PCVScannerLauncher(string caseName, string slideName, string scanArea, string assayName, double objective, double frameXScale, double frameYScale, string imageFramesName)
        {
            this.ScanAreaId = caseName + "_" + slideName + "_" + assayName.Replace(" ", "-"); // for identifying if another pcvscanner is already processiing this scan area
            this.rootPath = @"\\wh-laptop";
            this.caseName = caseName;
            this.slideName = slideName;
            this.scanArea = scanArea.Replace(" ", "%");
            this.assayName = assayName.Replace(" ", "%");
            this.objective = objective;
            this.frameXScale = frameXScale;
            this.frameYScale = frameYScale;
            this.imageFramesName = imageFramesName.Replace(" ", "%");
        }

        private string CommandLine
        {
            get
            {
                // Note that floating point numbers are formatted in this string using the invariant culture,
                // so that when running in French, for example, the decimal separator does not become a comma.
                // This is because cvscanner is culture invariant and can only cope with numbers in this format.
                return string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12}",
                                     ScanAreaId, rootPath,
                                     assayName, scanArea, caseName, slideName,
                                     objective.ToString(CultureInfo.InvariantCulture), autospotCounting, reprocess,
                                     frameXScale.ToString(CultureInfo.InvariantCulture), frameYScale.ToString(CultureInfo.InvariantCulture),
                                     Path.GetFileName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName),
                                     imageFramesName);
            }
        }

        public void Launch()
        {
            Process process = new Process();

            // Path to exe is path to directory current exe is running from, as all our exes are in same folder.
            // Don't use a fixed path, so that the app folder can be put anywhere.
            process.StartInfo.FileName = Path.Combine(Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName), "pcvscanner.exe");

            if (File.Exists(process.StartInfo.FileName))
            {
                process.StartInfo.Arguments = CommandLine;
                process.StartInfo.UseShellExecute = false;
                process.Start();
                cvscanner = process;
            }
        }
    }
}
