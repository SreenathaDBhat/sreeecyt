﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using AI.Imp;
using ManagedScriptProcessing;
using System.IO;

namespace TestManagedScriptProc
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnTestLaunchScanner(object sender, RoutedEventArgs e)
        {
            string shortScanArea = "StacksOff";// not sure why this has the standard assat prefix removed?

            var launcher = new AI.PCVScannerLauncher("CV7.4_FLScanning", "Slide1", shortScanArea, "StandardAssay_StacksOFF$=Session 1.assay", 20.0, 0.184, 0.183, "StandardAssay_StacksOFF");
            launcher.Launch();
        }

        private void OnTestManagedWrapper(object sender, RoutedEventArgs e)
        {
            var path = @"c:\crap";
            var bm = ScriptProcessing.LoadBitmap(@"c:\crap\karyo.png");
            var mcImage = ScriptProcessing.ImageFromBitmap(bm);

            var sp = new ManagedScriptProc();
            var ok = sp.Initialise("scriptVar", path, "QIn", "QOut");

            sp.PutImage(mcImage.Width, mcImage.Height, mcImage.Channels.FirstOrDefault().Pixels);
            sp.LoadScript(Path.Combine(path, "objects.script"));
            sp.SetExecutionMode(ManagedScriptProc.ScriptMode.Debug);
            ok = sp.Execute();
            sp.ReleaseScriptProcessor();

            //            image.AddChannel(ScriptProcessing.CreateMaskForFrame(region, boundingRect, points));

        }
    }
}
