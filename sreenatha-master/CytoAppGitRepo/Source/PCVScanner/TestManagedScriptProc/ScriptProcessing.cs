﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;

namespace AI.Imp
{
    public enum HsiChannels
    {
        Hue = 0,
        Saturation = 1,
        Intensity = 2
    }

    public class ChannelStats
    {

        public int [] histo = new int[256];
        public long sum;
        public long num;
        public double sumSqDeviances;
        public int min;
        public int max;
        public double mean;
        public double stdDev;
        public int mode;

    }

    public class ScriptProcessing
    {
        public static System.Windows.Media.Color ExtractColour(int colour)
        {
            byte b = (byte)((colour >> 16) & 0x000000FF);
            byte g = (byte)((colour >> 8) & 0x000000FF);
            byte r = (byte)(colour & 0x000000FF);

            return System.Windows.Media.Color.FromRgb(r, g, b);
        }

        public static BitmapSource BitmapFromChannel(Channel channel)
        {
            Image image = new Image(channel.Width, channel.Height);
            image.AddChannel(channel);
            return BitmapFromImage(image);
        }

        public static BitmapSource BitmapFromChannel(Channel channel, Color transparent)
        {
            Image image = new Image(channel.Width, channel.Height);
            image.AddChannel(channel);
            return BitmapFromImage(image, true, transparent);
        }

        public static void DumpPng(Channel channel, string p)
        {
            Image image = new Image(channel.Width, channel.Height);
            image.AddChannel(channel);
            DumpPng(image, p);
        }

        public static void DumpPng(Image image, string p)
        {
            DumpPng(BitmapFromImage(image), p);
        }

        public static void DumpPng(BitmapSource bitmap, string p)
        {
            try
            {
                if (!Directory.Exists(Path.GetDirectoryName(p)))
                    Directory.CreateDirectory(Path.GetDirectoryName(p));

                using (Stream s = File.Create(p))
                {
                    PngBitmapEncoder encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(bitmap));
                    encoder.Save(s);
                    s.Flush();
                }
            }
            catch { }; // for debug so don't bomb if you can't dump
        }

        public static BitmapSource ApplyMask(BitmapSource input, BitmapSource mask, byte maskVal)
        {
            if (input.PixelWidth != mask.PixelWidth || input.PixelHeight != mask.PixelHeight)
                throw new Exception("Not same size");

            unsafe
            {
                byte[] inputPixels = new byte[input.PixelHeight * input.PixelWidth * 4];
                input.CopyPixels(inputPixels, input.PixelWidth * 4, 0);

                byte[] maskPixels = new byte[mask.PixelHeight * mask.PixelWidth * 4];
                mask.CopyPixels(maskPixels, mask.PixelWidth * 4, 0);

                byte[] outputPixels = new byte[mask.PixelHeight * mask.PixelWidth * 4];


                fixed (byte* pI = &inputPixels[0])
                fixed (byte* pM = &maskPixels[0])
                fixed (byte* pO = &outputPixels[0])
                {
                    byte* pi = pI;
                    byte* pm = pM;
                    byte* po = pO;

                    int len = input.PixelHeight * input.PixelWidth;

                    for (int i = 0; i < len; i++)
                    {
                        *po++ = (*pm > 0) ? *pi : (byte)maskVal;
                        *po++ = (*pm > 0) ? *(pi + 1) : (byte)maskVal;
                        *po++ = (*pm > 0) ? *(pi + 2) : (byte)maskVal;
                        *po++ = (*pm > 0) ? *(pi + 3) : (byte)maskVal;
                        pm += 4;
                        pi += 4;
                    }
                }
                BitmapSource bitmap = BitmapSource.Create((int)input.PixelWidth, (int)input.PixelHeight, 96, 96, input.Format, null, outputPixels, input.PixelWidth * 4);
                bitmap.Freeze();
                return bitmap;
            }
        }

        public static BitmapSource BitmapFromImage(Imp.Image theImage)
        {
            return BitmapFromImage(theImage, false, Color.FromRgb(0,0,0));
        }

        public static BitmapSource BitmapFromImage(Imp.Image theImage, bool setTransparent, Color transparent)
        {
            if (theImage == null)
                return null;

            unsafe
            {
                byte[] pixelBuffer = new byte[theImage.Width * theImage.Height * 4];

                foreach (Imp.Channel channel in theImage)
                {
                    Color c = channel.Color;
                    int r = c.R;
                    int g = c.G;
                    int b = c.B;
                    int len = theImage.Width * theImage.Height;

                    fixed (byte* pixels = &pixelBuffer[0])
                    fixed (byte* channelPixels = &channel.Pixels[0])
                    {
                        byte* inPixel = channelPixels;
                        byte* outPixel = pixels;

                        for (int index = 0; index < len; ++index)
                        {
                            double intensity = *(inPixel++) / 255.0;
                            byte br = (byte)Math.Min((b * intensity) + (*outPixel), 255);
                            *(outPixel) = br;
                            outPixel++;

                            byte bg = (byte)Math.Min((g * intensity) + (*outPixel), 255);
                            *(outPixel) = bg;
                            outPixel++;

                            byte bb = (byte)Math.Min((r * intensity) + (*outPixel), 255);
                            *(outPixel) = bb;
                            outPixel++;

                            if (transparent != null)
                                *(outPixel) = (br == transparent.R && bg == transparent.G && bb == transparent.B) ? (byte)0 : (byte)255;
                            else
                                *(outPixel) = (byte)255;
                            outPixel++;
                        }
                    }
                }


                BitmapSource bitmap = BitmapSource.Create(theImage.Width, theImage.Height, 96, 96, PixelFormats.Bgra32, null, pixelBuffer, theImage.Width * 4);
                bitmap.Freeze();
                return bitmap;
            }
        }

        public static Image RGBBitmapToHSI(BitmapSource bitmap)
        {
            Image rgb = ImageFromBitmap(bitmap);
            int h = 0, s = 0, i= 0;

            Image hsiImage = new Image(rgb.Width, rgb.Height);
            byte[] hPixel = new byte [hsiImage.Width * hsiImage.Height];
            byte[] sPixel = new byte[hsiImage.Width * hsiImage.Height];
            byte[] iPixel = new byte[hsiImage.Width * hsiImage.Height];

            for (int index = 0; index < rgb.Width * rgb.Height; index++)
            {
                byte r = rgb.Channels[0].Pixels[index];
                byte g = rgb.Channels[1].Pixels[index];
                byte b = rgb.Channels[2].Pixels[index];

                RGB8toHSI(r, g, b, ref h, ref s, ref i);

                hPixel[index] = (byte)h;
                sPixel[index] = (byte)s;
                iPixel[index] = (byte)i;
            }
            hsiImage.Channels.Add(new Channel(hPixel, hsiImage.Width, hsiImage.Height, "HUEIM"));
            hsiImage.Channels.Add(new Channel(sPixel, hsiImage.Width, hsiImage.Height, "SATIM"));
            hsiImage.Channels.Add(new Channel(iPixel, hsiImage.Width, hsiImage.Height, "INTIM"));
            return hsiImage;
        }

        public static BitmapSource HSIImageToRGBBitmap(Image source)
        {
            int r = 0, g = 0, b = 0;

            for (int y = 0; y < source.Height; y++)
            {
                for (int x = 0; x < source.Width; x++)
                {
                    int index = (source.Width * y) + x;
                    int h = (int)source.Channels[0].Pixels[index];
                    int s = (int)source.Channels[1].Pixels[index];
                    int i = (int)source.Channels[0].Pixels[index];

                    HSItoRGB8(h, s, i, ref r, ref g, ref b);

                    source.Channels[0].Pixels[index] = (byte)r;
                    source.Channels[1].Pixels[index] = (byte)g;
                    source.Channels[2].Pixels[index] = (byte)b;
                }
            }
            return ScriptProcessing.BitmapFromImage(source);
        }

        public static int HSItoRGB8(int hv, int sv, int iv, ref int Rp, ref int Gp, ref int Bp)
        {
            int ret, direction = 0;
            var maxmidmin = new int[3];
            double ygPi = 4.0 * Math.Atan(1.0);
            double ygSqr3 = Math.Sqrt(3.0);

            var tofix = new int[6] { 43, 43, 128, 128, 213, 213 };
            var indices = new int[6] { 0x210, 0x120, 0x021, 0x012, 0x102, 0x201 };
            double hvfixed, angle, abscos, abssin, norm, dmax, dmid, dmin, dwrk;

            maxmidmin[0] = maxmidmin[1] = maxmidmin[2] = iv;
            if (sv > 0)
            {
                direction = (int)((double)hv / 42.500001);  // 0 to 5
                dmin = 1.0 - sv / 255.0;  // min of r, g, b

                hvfixed = (double)(hv - tofix[direction]);  // -43 to 43
                angle = hvfixed * ygPi / 128.0; // -pi/3 to pi/3
                abscos = Math.Cos(angle); abssin = Math.Abs(Math.Sin(angle));
                norm = (2.0 / 255) * sv / (abscos + ygSqr3 * abssin); // 0 to 2
                dmax = (1.0 + norm * abscos);  // max of r, g, b; 0 to 3
                dmid = 3.0 - dmin - dmax;  // mid of r, g, b
                if ((dwrk = dmax * iv) > 255)
                    iv = (int)(255.0 * iv / dwrk);
                maxmidmin[0] = (int)(iv * dmax + 0.5);
                maxmidmin[1] = (int)(iv * dmid + 0.5);
                maxmidmin[2] = (int)(iv * dmin + 0.5);
            }

            ret = (Rp = maxmidmin[(indices[direction] >> 8) & 0x0f]) << 16;
            ret |= (Gp = maxmidmin[(indices[direction] >> 4) & 0x0f]) << 8;
            ret |= (Bp = maxmidmin[(indices[direction] >> 0) & 0x0f]);
            return ret;
        }

        public static int RGB8toHSI(int rv, int gv, int bv, ref int Hp, ref int Sp, ref int Ip)
        {
            int wrk, cmin, hv, sv, ax, ay;
            double ygPi = 4.0 * Math.Atan(1.0);
            double ygSqr3 = Math.Sqrt(3.0);

            double atn;

            int cc = rv + gv + bv; 
            int cc2 = cc / 2;

            if (rv < gv) 
                cmin = rv; 
            else 
                cmin = gv;

            if (cmin > bv) 
                cmin = bv;

            // saturation:
            if ((wrk = (cc - cmin * 3)) < 1) 
                sv = 0;
            else 
                sv = (wrk * 255 + cc2) / cc;

            // intensity:
            int iv = (cc + 2) / 3;

            // hue:
            ay = gv - bv; 
            ax = 3 * rv - cc;

            if (ax != 0 || ay != 0)
            {
                atn = Math.Atan2(ygSqr3 * ay, (double)ax) / ygPi; // -1 to +1
                hv = (int)Math.Floor((atn + 1.0) * 128.0 + 0.5); hv &= 0xff;
            } 
            else 
                hv = 0;

            Hp = hv; 
            Sp = sv; 
            Ip = iv;
            return (((rv << 8) | gv) << 8) | bv;
        }


        public static unsafe Image ImageFromBitmap(BitmapSource input)
        {
            byte[] inputPixels = new byte[input.PixelHeight * input.PixelWidth * 4];
            input.CopyPixels(inputPixels, input.PixelWidth * 4, 0);

            Channel r = new Channel(input.PixelWidth, input.PixelHeight, Color.FromRgb(255, 0, 0));
            Channel g = new Channel(input.PixelWidth, input.PixelHeight, Color.FromRgb(0, 255, 0));
            Channel b = new Channel(input.PixelWidth, input.PixelHeight, Color.FromRgb(0, 0, 255));
            r.Description = "Red";
            g.Description = "Green";
            b.Description = "Blue";

            fixed (byte* pI = &inputPixels[0])
            fixed (byte* pR = &r.Pixels[0])
            fixed (byte* pG = &g.Pixels[0])
            fixed (byte* pB = &b.Pixels[0])
            {
                byte* pi = pI;
                byte* pr = pR;
                byte* pg = pG;
                byte* pb = pB;
                int len = input.PixelHeight * input.PixelWidth;

                for (int i = 0; i < len; ++i)
                {
                    *pb++ = *pi++;
                    *pg++ = *pi++;
                    *pr++ = *pi++;
                    ++pi; // skip alpha
                }
            }

            Image img = new Image(input.PixelWidth, input.PixelHeight);
            img.AddChannel(r);
            img.AddChannel(g);
            img.AddChannel(b);
            return img;
        }

        public static Rect BoundingRect(IEnumerable<Point> maskPoints)
        {
            double rl = double.MaxValue;
            double rt = double.MaxValue;
            double rr = double.MinValue;
            double rb = double.MinValue;
            foreach (var pt in maskPoints)
            {
                rl = Math.Min(rl, pt.X);
                rt = Math.Min(rt, pt.Y);
                rr = Math.Max(rr, pt.X);
                rb = Math.Max(rb, pt.Y);
            }

            return new Rect(rl, rt, rr - rl, rb - rt);
        }

        private static Point OffsetAndScale(Point p, Size offset, Double scale)
        {
            Point np = new Point(p.X, p.Y);
            np.Offset(-offset.Width, -offset.Height);
            np.X*= scale;
            np.Y *= scale;
            return np;
        }

        public static Channel CreateMaskForFrame(BitmapSource frame, Rect view, IEnumerable<Point> points)
        {
            BitmapSource maskImage = CreateMaskBitmapForFrame(frame, view, points);
            Image mask = ImageFromBitmap(maskImage);

            mask.Channels[0].Color = Colors.White;
            mask.Channels[0].Description = "Mask";
            return mask.Channels[0];
        }

        public static BitmapSource CreateMaskBitmapForFrame(BitmapSource frame, Rect view, IEnumerable<Point> points)
        {
            double scale = frame.PixelWidth / view.Width;

            Point start = OffsetAndScale(points.First(), new Size(view.X, view.Y), scale);

            var segments = (from p in points.Skip(1)
                            select new LineSegment(OffsetAndScale(p, new Size(view.X, view.Y), scale), true)).ToArray();

            var geom = new PathGeometry(new PathFigure[] { new PathFigure(start, (IEnumerable<PathSegment>)segments, true) });
            DrawingVisual visual = new DrawingVisual();

            DrawingContext context = visual.RenderOpen();

            context.DrawRectangle(Brushes.Black, null, new Rect(0, 0, frame.PixelWidth, frame.PixelWidth));
            context.DrawGeometry(Brushes.White, null, geom);

            context.Close();

            var renderTarget = new RenderTargetBitmap((int)frame.PixelWidth, (int)frame.PixelHeight, 96, 96, PixelFormats.Pbgra32);

            renderTarget.Render(visual);

            return renderTarget;
        }

        public static BitmapSource LoadBitmap(string filename)
        {
            BitmapImage img = new BitmapImage();
            img.BeginInit();
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.UriSource = new Uri(filename);
            img.EndInit();
            img.Freeze();
            return img;
        }

        public static unsafe Channel Average(Channel c)
        {
            int ycnt = 0; 
            int xcnt = 0;

            fixed (byte* pI = &c.Pixels[0])
            {
                byte* r = pI;
                byte* rc;
                byte curr, prev, nxt;
                int i;

                while (ycnt++ < c.Width)
                {
                    xcnt = 0; curr = nxt = r[0];
                    while (xcnt < c.Width)
                    {
                        prev = curr; curr = nxt;
                        i = xcnt++;
                        if (xcnt < c.Width) 
                            nxt = r[xcnt];
                        r[i] = (byte)((prev + curr * 2 + nxt + 2) >> 2);
                    }
                    r += c.Width;
                }
                xcnt = 0; rc = pI;
                while (xcnt++ < c.Width)
                {
                    ycnt = 0; r = rc;
                    curr = nxt = r[0];
                    while (ycnt++ < c.Height)
                    {
                        prev = curr; curr = nxt;
                        if (ycnt < c.Height) nxt = r[c.Width];
                        r[0] = (byte)((prev + curr * 2 + nxt + 2) >> 2);
                        r += c.Width;
                    }
                    rc++;
                }
            }
            return c;
        }

        public static unsafe Channel Median(Channel c) // straight from UTS
        {
            int ycnt = 0; 
            int xcnt = 0;

            fixed (byte* pI = &c.Pixels[0])
            {
                byte* r = pI;
                byte* rc;
                byte curr, prev, nxt;
                int i;

                while (ycnt++ < c.Height)
                {
                    xcnt = 0; curr = nxt = r[0];
                    while (xcnt < c.Width)
                    {
                        prev = curr; curr = nxt;
                        i = xcnt++;
                        if (xcnt < c.Width) 
                            nxt = r[xcnt];
                        if (prev < curr)
                        {
                            if (curr < nxt) 
                                prev = curr;
                            else if (prev < nxt) 
                                prev = nxt;
                        }
                        else
                        {
                            if (curr > nxt) 
                                prev = curr;
                            else if (prev > nxt) 
                                prev = nxt;
                        }
                        r[i] = prev;
                    }
                    r += c.Width;
                }

                xcnt = 0; rc = pI;
                while (xcnt++ < c.Width)
                {
                    ycnt = 0; r = rc;
                    curr = nxt = r[0];
                    while (ycnt++ < c.Height)
                    {
                        prev = curr; curr = nxt;
                        if (ycnt < c.Width) nxt = r[c.Width];
                        if (prev < curr)
                        {
                            if (curr < nxt) 
                                prev = curr;
                            else if (prev < nxt) 
                                prev = nxt;
                        }
                        else
                        {
                            if (curr > nxt) 
                                prev = curr;
                            else if (prev > nxt) 
                                prev = nxt;
                        }
                        r[0] = prev;
                        r += c.Width;
                    }
                    rc++;
                }
            }
            return c;
        }
    }
}
