﻿using CytoApps.Infrastructure.UI.Controls;
using CytoApps.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CytoApps.SpotConfiguration
{
    /// <summary>
    /// Interaction logic for AddFusionWindow.xaml
    /// </summary>
    public partial class AddFusionWindow : Window
    {
        private Fusion fusion;
        private SpotConfigController controller;

        public AddFusionWindow(Fusion newFusion, SpotConfigController spotController)
        {
            if (newFusion == null)
                throw new ArgumentNullException("newFusion");

            fusion = newFusion;
            controller = spotController;

            DataContext = this;

            InitializeComponent();

            FusionNameTextBox.Focus();

            if (!spotController.MultipleFusionsAllowed)
            {   // If we're in backward-compatible single-fusion mode, fusions don't have names,
                // so assign a default name to the fusion and hide the controls for editing the name.
                fusion.Name = "Fusion";
                fusionNameStackPanel.Height = 0;
            }
        }

        public string FusionName
        {
            get { return fusion.Name; }
            set { fusion.Name = value; }
        }

        public IEnumerable<FusionComponent> ProbeNames
        {
            get { return fusion.Components; }
        }


        private void OnClickSelectFusionComponent(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;

            if (fusion.CountSelected() > Constants.MAXPROBESPERFUSION)
            {
                CustomPopUp.Show(string.Format(Literal.Strings.Lookup("cbStr-FusionMaxProbesMessage"), Constants.MAXPROBESPERFUSION), Literal.Strings.Lookup("cbStr-FusionTitle"), System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information, this);
                checkBox.IsChecked = !checkBox.IsChecked; // Roll the change back.
            }
            else if (!controller.IsFusionCombinationUnique(fusion))
            {
                CustomPopUp.Show(Literal.Strings.Lookup("cbStr-FusionNotUniqueMessage"), Literal.Strings.Lookup("cbStr-FusionTitle"), System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information, this);

                //  var result = MessageBox.Show((string)FindResource("scFusionNotUniqueMessage"));
                checkBox.IsChecked = !checkBox.IsChecked; // Roll the change back.
            }
        }

        private void OnOK(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(FusionName) || controller.IsFusionNameInUse(FusionName))
            {
                CustomPopUp.Show(string.Format(Literal.Strings.Lookup("cbStr-FusionUniqueNameMessage"), Constants.MAXPROBESPERFUSION), Literal.Strings.Lookup("cbStr-FusionTitle"), System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Information, this);
            }
            else if (fusion.CountSelected() < 2)
            {
                object message = Literal.Strings.Lookup("cbStr-FusionAtLeast2Message");
                if (Constants.MAXPROBESPERFUSION <= 2)
                    message = Literal.Strings.Lookup("cbStr-FusionTwoMessage");
                // message = FindResource("scFusionTwoMessage");

                CustomPopUp.Show((string)message, "Fusion  ", MessageBoxButton.OK, MessageBoxImage.Information, this);

                // MessageBox.Show((string)message);
            }
            else
                DialogResult = true;
        }
    }
}


