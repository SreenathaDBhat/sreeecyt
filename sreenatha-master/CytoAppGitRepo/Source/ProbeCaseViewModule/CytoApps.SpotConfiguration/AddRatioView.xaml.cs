﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media.Animation;

namespace CytoApps.SpotConfiguration
{
	public partial class AddRatioView
	{
        public AddRatioView()
		{
            DataContext = this;
            InitializeComponent();
            Loaded += (s, e) => nameTextBox.Focus();
            Reset();
		}

        public void Reset()
        {
            nameTextBox.Text = String.Empty;

            if (Constants.MAXRATIOS < 2)
            {   // If we're not making use of the multiple-ratio capability, the ratio doesn't need to be
                // given a name by the user, so assign a default name and hide the controls for editing it.
                nameTextBox.Text = "Ratio";
                ratioNameStackPanel.Height = 0;
            }

            numeratorCombo.SelectedIndex = -1;
            denominatorCombo.SelectedIndex = -1;
            errorView.Visibility = System.Windows.Visibility.Collapsed;
        }


        public static DependencyProperty ProbesProperty = DependencyProperty.Register("Probes", typeof(IEnumerable<Probe>), typeof(AddRatioView));
        public event EventHandler OK;

        public IEnumerable<Probe> Probes
        {
            get { return (IEnumerable<Probe>)GetValue(ProbesProperty); }
            set { SetValue(ProbesProperty, value); }
        }

        private Probe Numerator
        {
            get { return (Probe)numeratorCombo.SelectedItem; }
        }

        private Probe Denominator
        {
            get { return (Probe)denominatorCombo.SelectedItem; }
        }

        private string TrimmmedName
        {
            get { return nameTextBox.Text.Trim(); }
        }

        public Ratio Ratio
        {
            get { return new Ratio(TrimmmedName, Numerator.Name, Denominator.Name); }
        }

        private void OnOK(object sender, RoutedEventArgs e)
		{
            if (TrimmmedName.Length == 0 || Numerator == Denominator || Numerator == null || Denominator == null)
            {
                errorView.Visibility = System.Windows.Visibility.Visible;
                return;
            }

            errorView.Visibility = System.Windows.Visibility.Collapsed;

            if (OK != null)
                OK(this, EventArgs.Empty);
		}

        private void OnTextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            errorView.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void OnComboChange(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            errorView.Visibility = System.Windows.Visibility.Collapsed;
        }
    }
}


