﻿using System;
using System.Windows;
using Microsoft.Win32;

namespace CytoApps.SpotConfiguration
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private int exitCode;

		public App()
		{
			RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Applied Imaging\Cytovision\LanguageCharSet");
			if (key != null)
			{
				string culture = (string)key.GetValue("LanguageCulture", "en-US");
				System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(culture);
			}
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
		}

		void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			MessageBox.Show(e.ExceptionObject.ToString());
		}

        protected override void OnStartup(StartupEventArgs e)
        {
            exitCode = 1;
            Exit += OnExit;

            base.OnStartup(e);

#if DEBUG
            MessageBox.Show("Attach debugger now . . . then click OK");
#endif

            string[] commandLineArgs = Environment.GetCommandLineArgs();

            string assayFilePath = null;
            bool reprocessMode = false;
            if (commandLineArgs.Length >= 2)
            {
                assayFilePath = commandLineArgs[1];

                if (commandLineArgs.Length >= 3)
                    reprocessMode = commandLineArgs[2].Equals("reprocessmode", StringComparison.OrdinalIgnoreCase);
            }

            // HACK, use fromstream to make this stand alone
            //var c = SpotConfigController.FromFile(assayFilePath, reprocessMode);
            //if (c == null)
            //    c = new CytoApps.SpotConfiguration.SpotConfigController(assayFilePath, reprocessMode);

            //var dlg = new SpotConfigDialog(c);

            //if (dlg.ShowDialog() ?? false)
            //    // Use an exit code of 101 if the OK button has been pressed. Don't use 0 or 1, as these
            //    // are sometimes returned if the process is terminated prematurely by the debugger or the OS!
            //    exitCode = 101;
            //else
            //    exitCode = 100;
        }

        void OnExit(object sender, ExitEventArgs e)
        {
            e.ApplicationExitCode = exitCode;
        }
    }
}
