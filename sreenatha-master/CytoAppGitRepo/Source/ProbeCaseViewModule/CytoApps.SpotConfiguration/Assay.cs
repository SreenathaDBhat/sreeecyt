﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Windows.Media;
using System.Globalization;
using CytoApps.Infrastructure.UI.Utilities;

namespace CytoApps.SpotConfiguration
{
    public class ParamSet
    {
        public string ctrlType;
        public string label;
        public string min;
        public string max;
        public string value;
        public string defValue;
        public string type;
        public bool hasDefault;

        public ParamSet()
        {
        }

        public ParamSet(string description, string value)
        {
            label = description;
            this.value = value;
            type = "8"; // Variant type 8 (string).
            ctrlType = "Assay"; // Not a script param.
            min = "Min"; // Not used for strings.
            max = "Max"; // Not used for strings.
        }

        public ParamSet(string description, Color colour, bool hex) : this(description, "")
        {
            if (hex)
                value = ColorParse.ToHexString(colour).ToUpperInvariant();
            else
            {
                int colourValue = (int)colour.R | ((int)colour.G << 8) | ((int)colour.B << 16);

                value = colourValue.ToString();
            }
        }

        public ParamSet(string description, int value, int min, int max)
        {
            label = description;
            this.value = value.ToString();
            type = "3"; // Variant type 3 (long int).
            ctrlType = "Assay"; // Not a script param.
            this.min = min.ToString();
            this.max = max.ToString();
        }

        public ParamSet(string description, string ctrlType, string value, string min, string max, string defaultValue)
        {
            // This constructor is for script params.
            label = description;
            this.ctrlType = ctrlType;
            this.value = value;
            this.min = min;
            this.max = max;
            defValue = defaultValue;
            hasDefault = true;
            type = "3"; // Script params are always variant type 3 (long int).
        }
    }

    public class SpectraChromeData
    {
        public string zNumImages;
        public string zDist;
    }

    public class ClassFormulae
    {
        public List<string> probeFormulae;
        public List<string> ratioFormulae;
        public List<string> fusionFormulae;

        public ClassFormulae()
        {
            probeFormulae = new List<string>();
            ratioFormulae = new List<string>();
            fusionFormulae = new List<string>();
        }

        public ClassFormulae(int numProbes, int numRatios, int numFusions) : this()
        {
            for (int i = 0; i < numProbes; i++)
                probeFormulae.Add("");
            for (int i = 0; i < numRatios; i++)
                ratioFormulae.Add("");
            for (int i = 0; i < numFusions; i++)
                fusionFormulae.Add("");
        }
    }


    class Assay
    {
        private XElement assayNode;
        //private string aiiSharedPath;
    //////    private string prescanScriptFilePath;
        //private string spotScriptFilePath;
        AssayInfo _assayInfo;
        public Assay(AssayInfo assayInfo)
        {
            assayNode = null;
            _assayInfo = assayInfo;
            //this.aiiSharedPath = aiiSharedPath;
            //string spotScriptsPath = GetSpotScriptsPath(aiiSharedPath);
          //  prescanScriptFilePath = @"interphasefind.script";
            //spotScriptFilePath = GetSpotScriptFilePath(aiiSharedPath);
        }

        public static string GetSpotScriptsPath(string aiiSharedPath)
        {
            return Path.Combine(aiiSharedPath, "Scripts", "Spot");
        }

        public static string GetSpotScriptFilePath(string aiiSharedPath)
        {
            return GetSpotScriptsPath(aiiSharedPath) + @"\SpotHiMagCV72.script";
        }

        public string GetSpotAssayFilePath()
        {
            throw new NotImplementedException(); //return Path.Combine(aiiSharedPath, "Assays", "Spot");
        }



        // Assay reading ///////////////////////////////////////////////////////////////////////

        public bool Load(string assayFilePath)
        {
            bool status = false;

            if (File.Exists(assayFilePath))
            {
                using (var s = File.OpenRead(assayFilePath))
                    status = Load(s);
            }

            return status;
        }

        public bool Load(Stream assay)
        {
            // Load probes (fluorochromes/spectrachromes) and script parameters
            // from the last scan pass stored in the specified assay file.
            // Refer to CScanPass::XMLSerialize() in AssayDLL/ScanPass.cpp
            try
            {
                assayNode = XElement.Load(assay);
                return assayNode != null && assayNode.Name == "CAssay";
            }
            catch (XmlException ex)
            {
                Debug.WriteLine(ex.ToString());
                return false;
            }
        }

        public string GetInternalName()
        {
            return GetChildElementValue(assayNode, "m_Name");
        }

        public string GetClassifierName()
        {
            var finalScanPassNode = GetFinalScanPassNode();
            if (finalScanPassNode != null)
                return GetChildElementValue(finalScanPassNode, "m_ClassifierName");
            else
                return String.Empty;
        }

        public Dictionary<string, ParamSet> GetParamSets()
        {
            Dictionary<string, ParamSet> paramSets = new Dictionary<string, ParamSet>();

            AddParamsFromScript(_assayInfo.SpotScript, paramSets);

            var finalScanPassNode = GetFinalScanPassNode();
            if (finalScanPassNode != null)
            {
                var paramSetNodes = finalScanPassNode.Descendants("CParamSet");

                foreach (var p in paramSetNodes)
                {
                    string name = GetChildElementValue(p, "Name");
                    if (name.Length > 0)
                    {
                        ParamSet set = new ParamSet();

                        set.ctrlType = GetChildElementValue(p, "CtrlType");
                        set.label = GetChildElementValue(p, "Label");

                        set.min = GetGrandChildElementValue(p, "VtMin", "VarValue");
                        set.max = GetGrandChildElementValue(p, "VtMax", "VarValue");
                        set.value = GetGrandChildElementValue(p, "VtValue", "VarValue");
                        set.type = GetGrandChildElementValue(p, "VtValue", "Vt"); // Type should be same for all sibling values.

                        set.defValue = GetGrandChildElementValue(p, "VtDef", "VarValue");
                        set.hasDefault = (GetGrandChildElementValue(p, "VtDef", "Vt") == set.type) ? true : false;

                        // The currently active script params are those that are currently defined in the script itself and have
                        // aleady been loaded (by calling AddParamsFromScript(), above). So, if the param that has just been
                        // loaded from the assay is a script param (i.e. a slider or checkbox param) that wasn't defined in
                        // the script, discard it. Otherwise, just update the value field of the ParamSet read from the script.
                        if (paramSets.ContainsKey(name))
                        {
                            if (set.ctrlType.Equals("Assay"))
                                paramSets[name] = set;
                            else
                                paramSets[name].value = set.value;
                        }
                        else if (set.ctrlType.Equals("Assay"))
                            paramSets.Add(name, set);
                    }
                }
            }

            return paramSets;
        }


        public Dictionary<string, SpectraChromeData> GetSpectraChromes()
        {
            Dictionary<string, SpectraChromeData> spectraChromes = new Dictionary<string, SpectraChromeData>();

            var finalScanPassNode = GetFinalScanPassNode();
            if (finalScanPassNode != null)
            {
                var spectraChromeNodes = finalScanPassNode.Descendants("CSpectraChrome");

                foreach (var s in spectraChromeNodes)
                {
                    string name = GetChildElementValue(s, "m_Name");
                    int isCounterStain = GetChildElementValue(s, "m_Counterstain", 0);

                    if (name.Length > 0)
                    {
                        SpectraChromeData newSpectraChrome = new SpectraChromeData();

                        XElement zSettingsNode = s.Element("CZstackSettings");
                        if (zSettingsNode != null)
                        {
                            newSpectraChrome.zNumImages = GetChildElementValue(zSettingsNode, "m_num_images");
                            newSpectraChrome.zDist      = GetChildElementValue(zSettingsNode, "m_focus_dist");
                        };

                        spectraChromes.Add(name, newSpectraChrome);
                    }
                }
            }

            return spectraChromes;
        }


        private XElement GetFinalScanPassNode()
        {
            XElement finalScanPassNode = null;

            if (assayNode != null && assayNode.Name == "CAssay")
                finalScanPassNode = assayNode.Descendants("CScanPass").Reverse().FirstOrDefault();

            return finalScanPassNode;
        }

        private string GetChildElementValue(XElement e, XName childName)
        {
            string value = String.Empty;

            if (e != null)
            {
                XElement child = e.Element(childName);
                if (child != null)
                    value = child.Value;
            }

            return value;
        }

        private int GetChildElementValue(XElement e, XName childName, int defaultValue)
        {
            int value = defaultValue;
            string stringValue = GetChildElementValue(e, childName);
            if (!Int32.TryParse(stringValue, out value))
                value = defaultValue;
            return value;
        }

        private string GetGrandChildElementValue(XElement e, XName childName, XName grandChildName)
        {
            string value = String.Empty;

            if (e != null)
            {
                XElement child = e.Element(childName);
                value = GetChildElementValue(child, grandChildName);
            }

            return value;
        }


        // Assay creation //////////////////////////////////////////////////////////////////////////

        private XElement CreateSpectraChromeNode(XElement copyFrom, string name, SpectraChromeData data, bool counterstain)
        {
            // Create a new spectrachrome that is either a copy of a given spectrachrome,
            // or else uses default values (and the given name).
            XElement newSpectraChrome = new XElement("CSpectraChrome");
            if (copyFrom != null)
                newSpectraChrome = new XElement(copyFrom);
            else
            {
                newSpectraChrome.Add(new XElement("m_version", "10"));
                newSpectraChrome.Add(new XElement("m_Name", name != null ? name : String.Empty));
                newSpectraChrome.Add(new XElement("m_red",   "255"));
                newSpectraChrome.Add(new XElement("m_green", "255"));
                newSpectraChrome.Add(new XElement("m_blue",  "255"));
                newSpectraChrome.Add(new XElement("m_Description", String.Empty));
                newSpectraChrome.Add(new XElement("m_Fluorescent", "1"));
                newSpectraChrome.Add(new XElement("m_Counterstain", counterstain ? "1" : "0"));
                XElement zStackNode = new XElement("CZstackSettings");
                zStackNode.Add(new XElement("m_on", "0"));
                zStackNode.Add(new XElement("m_num_images", "0"));
                zStackNode.Add(new XElement("m_focus_dist", "0"));
                newSpectraChrome.Add(zStackNode);
            }

            // If SpectraChromeData is supplied, copy that into the new spectrachrome.
            if (data != null)
            {
                XElement zStackNode = newSpectraChrome.Element("CZstackSettings");
                if (zStackNode != null)
                {
                    XElement numImagesNode = zStackNode.Element("m_num_images");
                    if (numImagesNode != null)
                        numImagesNode.Value = data.zNumImages;

                    XElement focusDistNode = zStackNode.Element("m_focus_dist");
                    if (focusDistNode != null)
                        focusDistNode.Value = data.zDist;
                }
            }

            return newSpectraChrome;
        }

        private XElement ComposeSpectraChromes(string defaultSpectraChromesFileNamePath, Dictionary<string, SpectraChromeData> spectraChromeData, string counterStainName)
        {
            //
            // Get the spectrachromes corresponding with spectraChromeData and counterStainName.
            //
            XElement spectraChromeListNode = new XElement("CSpectraChromeList");
            spectraChromeListNode.Add(new XElement("m_version", "1"));

            XElement srcListNode = null;
            try
            {
                // Load all the spectrachromes from the default spectrachromes file.
                if (File.Exists(defaultSpectraChromesFileNamePath)) {
                    srcListNode = XElement.Load(defaultSpectraChromesFileNamePath);
                    if (srcListNode.Name != "CSpectraChromeList") {
                        srcListNode = null;
                    }
                }
            }
            catch
            {
                Debug.WriteLine("WARNING: failed to load default spectrachromes: " + defaultSpectraChromesFileNamePath);
            }


            // For the counterstain and each spectrachrome with SpectraChromeData,
            // if there is a matching spectrachrome in the defaults file, take a copy of that,
            // otherwise create a new spectrachrome node from scratch. In either case, copy in
            // values from the corresponding SpectraChromeData.

            // First deal with the counterstain.
            XElement defaultSpectraChromeNode = null;
            if (srcListNode != null)
            {
                var def = srcListNode.Descendants("CSpectraChrome").Where(s => GetChildElementValue(s, "m_Name") == counterStainName).FirstOrDefault();
                if (def != default(XElement))
                    defaultSpectraChromeNode = def;
            }

            spectraChromeListNode.Add(CreateSpectraChromeNode(defaultSpectraChromeNode, counterStainName, null, true));


            // Next deal with the other spectrachromes.
            foreach (var d in spectraChromeData)
            {
                defaultSpectraChromeNode = null;
                if (srcListNode != null)
                {
                    var def = srcListNode.Descendants("CSpectraChrome").Where(s => GetChildElementValue(s, "m_Name") == d.Key).FirstOrDefault();
                    if (def != default(XElement))
                        defaultSpectraChromeNode = def;
                }

                spectraChromeListNode.Add(CreateSpectraChromeNode(defaultSpectraChromeNode, d.Key, d.Value, false));
            }

            return spectraChromeListNode;
        }

        static public bool AddParamsFromScript(string[] scriptFile, Dictionary<string, ParamSet> paramSets)
        {
            bool status = false;

            const string paramString = "Param ";

            if (scriptFile != null && scriptFile.Length > 0)
            {
               // string[] lines = File.ReadAllLines(scriptFilePath);

                foreach (var line in scriptFile)
                {
                    string paramName = String.Empty;

                    // First non-whitespace character must be a single comment character.
                    var l = line.Trim();
                    if (l.Length > 0 && l[0].Equals('\''))
                    {
                        // Remove comment character.
                        l = l.Substring(1);

                        // Next must be whitespace-delimited script parameter directive.
                        l = l.Trim();
                        if (l.Length > paramString.Length && String.Compare(l.Substring(0, paramString.Length), paramString, true) == 0)
                        {
                            // Remove 'Param'
                            l = l.Substring(paramString.Length);

                            // Next should be whitespace-delimited parameter name.
                            l = l.Trim();
                            int index = l.IndexOf(' ');
                            if (index > 0)
                                paramName = l.Substring(0, index);
                        }
                    }

                    if (paramName.Length > 0)
                    {
                        string ctrlType = String.Empty;
                        string description = String.Empty;

                        // Remove name
                        l = l.Substring(paramName.Length);
                        l = l.Trim();

                        // Next should be apostrophe-delimited parameter type.
                        if (l.Length > 0 && l[0].Equals('\''))
                        {
                            l = l.Substring(1); // Remove leading apostrophe.
                            int index = l.IndexOf('\'');
                            if (index > 0)
                                ctrlType = l.Substring(0, index);
                        }

                        // Next be apostrophe-delimited parameter description.
                        if (ctrlType.Length > 0)
                        {
                            // Remove type, plus trailing apostrophe.
                            l = l.Substring(ctrlType.Length + 1);
                            l = l.Trim();

                            if (l.Length > 0 && l[0].Equals('\''))
                            {
                                l = l.Substring(1); // Remove leading apostrophe.
                                int index = l.IndexOf('\'');
                                if (index > 0)
                                    description = l.Substring(0, index);
                            }
                        }

                        // Finally there should be the min, max and default values, whitespace delimited.
                        if (description.Length > 0)
                        {
                            // Remove description, plus trailing apostrophe.
                            l = l.Substring(description.Length + 1);

                            l = l.Trim();
                            l = l.Trim(new char[]{ ',' }); // There is sometimes a comma for some reason!
                            l = l.Trim();
                            string[] stringValues = l.Split(' ');
                            if (stringValues.Length == 3)
                                // Create a new ParamSet for this, with its value initialised to the specified default value.
                                paramSets.Add(paramName, new ParamSet(description, ctrlType, stringValues[2], stringValues[0], stringValues[1], stringValues[2]));
                        }
                    }
                }

                status = true;
            }

            return status;
        }


        private void AddParamSetVtBlock(XElement parent, string name, string type, string value)
        {
            if (parent != null)
            {
                XElement currValNode = new XElement(name);
                currValNode.Add(new XElement("Vt", type));
                currValNode.Add(new XElement("VarValue", value));
                parent.Add(currValNode);
            }
        }

        private void AddParamSetToNode(XElement parent, string name, ParamSet paramSet)
        {
            if (parent != null)
            {
                XElement paramSetNode = new XElement("CParamSet");
                paramSetNode.Add(new XElement("Name", name));
                paramSetNode.Add(new XElement("CtrlType", paramSet.ctrlType));
                paramSetNode.Add(new XElement("Label", paramSet.label));
                AddParamSetVtBlock(paramSetNode, "VtMin", paramSet.type, paramSet.min);
                AddParamSetVtBlock(paramSetNode, "VtMax", paramSet.type, paramSet.max);
                AddParamSetVtBlock(paramSetNode, "VtValue", paramSet.type, paramSet.value);
                if (paramSet.hasDefault)
                    AddParamSetVtBlock(paramSetNode, "VtDef", paramSet.type, paramSet.defValue);
                else
                    AddParamSetVtBlock(paramSetNode, "VtDef", "0", String.Empty); // A null variant type.

                parent.Add(paramSetNode);
            }
        }


        public void Create(string assayName, string classifierName, Dictionary<string, ParamSet> spotParams, 
                           string spectrachromesFilePath, Dictionary<string, SpectraChromeData> spotSpectraChromeData, string counterStainName)
        {
            // Create an assay stub.
            assayNode = new XElement("CAssay");
            assayNode.Add(new XElement("m_version", 109));
            assayNode.Add(new XElement("m_Name", assayName));
            assayNode.Add(new XElement("AssayType")); // Is AssayType still used?? If not, remove it from here?
            assayNode.Add(new XElement("m_dbNodeType", "0")); // Is m_dbNodeType still used?? If not, remove it from here?

            XElement scanPassListNode = new XElement("CScanPassList");
            scanPassListNode.Add(new XElement("m_version", "109"));
            assayNode.Add(scanPassListNode);

            // Add the prescan pass /////////////////////////////////////////////
            XElement prePassNode = new XElement("CScanPass");
            prePassNode.Add(new XElement("m_version", "110"));
           prePassNode.Add(new XElement("m_ScriptName", _assayInfo.PreScanScriptFilePath)); // need to check with willams
            prePassNode.Add(new XElement("m_ClassifierName", "Everything"));

            // Load script params for the prescan.
            Dictionary<string, ParamSet> paramSets = new Dictionary<string, ParamSet>();
            AddParamsFromScript(_assayInfo.PreScanScript, paramSets);

            XElement scriptParamsNode = new XElement("CScriptParams");
            foreach (var ps in paramSets)
                AddParamSetToNode(scriptParamsNode, ps.Key, ps.Value); // Script params always variant type 3 (long int).

            prePassNode.Add(scriptParamsNode);

            scanPassListNode.Add(prePassNode);

            // Add the main pass //////////////////////////////////////////////
            XElement mainPassNode = new XElement("CScanPass");
            mainPassNode.Add(new XElement("m_version", "110"));
            mainPassNode.Add(new XElement("m_ScriptName", _assayInfo.SpotScriptFilePath)); // need to check with willams
            mainPassNode.Add(new XElement("m_ClassifierName", classifierName));

            scriptParamsNode = new XElement("CScriptParams");

            foreach (var ps in spotParams)
                AddParamSetToNode(scriptParamsNode, ps.Key, ps.Value);

            mainPassNode.Add(scriptParamsNode);

            // Add the spectrachromes.
            mainPassNode.Add(ComposeSpectraChromes(spectrachromesFilePath, spotSpectraChromeData, counterStainName));

            // Finally, add this pass to the pass list.
            scanPassListNode.Add(mainPassNode);
        }


        public byte[] CreateAssayAsXML(string assayName, string classifierName, Dictionary<string, ParamSet> spotParams,
                        string spectrachromesFilePath, Dictionary<string, SpectraChromeData> spotSpectraChromeData, string counterStainName)
        {
            // Create an assay stub.
            assayNode = new XElement("CAssay");
            assayNode.Add(new XElement("m_version", 109));
            assayNode.Add(new XElement("m_Name", assayName));
            assayNode.Add(new XElement("AssayType")); // Is AssayType still used?? If not, remove it from here?
            assayNode.Add(new XElement("m_dbNodeType", "0")); // Is m_dbNodeType still used?? If not, remove it from here?

            XElement scanPassListNode = new XElement("CScanPassList");
            scanPassListNode.Add(new XElement("m_version", "109"));
            assayNode.Add(scanPassListNode);

            // Add the prescan pass /////////////////////////////////////////////
            XElement prePassNode = new XElement("CScanPass");
            prePassNode.Add(new XElement("m_version", "110"));
            prePassNode.Add(new XElement("m_ScriptName", _assayInfo.PreScanScriptFilePath));
            prePassNode.Add(new XElement("m_ClassifierName", "Everything"));

            // Load script params for the prescan.
            Dictionary<string, ParamSet> paramSets = new Dictionary<string, ParamSet>();
            AddParamsFromScript(_assayInfo.PreScanScript, paramSets);

            XElement scriptParamsNode = new XElement("CScriptParams");
            foreach (var ps in paramSets)
                AddParamSetToNode(scriptParamsNode, ps.Key, ps.Value); // Script params always variant type 3 (long int).

            prePassNode.Add(scriptParamsNode);

            scanPassListNode.Add(prePassNode);

            // Add the main pass //////////////////////////////////////////////
            XElement mainPassNode = new XElement("CScanPass");
            mainPassNode.Add(new XElement("m_version", "110"));
            mainPassNode.Add(new XElement("m_ScriptName", _assayInfo.SpotScriptFilePath)); //need to check with willams
            mainPassNode.Add(new XElement("m_ClassifierName", classifierName));

            scriptParamsNode = new XElement("CScriptParams");

            foreach (var ps in spotParams)
                AddParamSetToNode(scriptParamsNode, ps.Key, ps.Value);

            mainPassNode.Add(scriptParamsNode);

            // Add the spectrachromes.
            mainPassNode.Add(ComposeSpectraChromes(spectrachromesFilePath, spotSpectraChromeData, counterStainName));

            // Finally, add this pass to the pass list.
            scanPassListNode.Add(mainPassNode);

          //  return ObjectToByteArray( assayNode.ToString());
            return Encoding.Default.GetBytes(assayNode.ToString());

        }

               
        public bool Save(string filePath)
        {
            using (var s = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                return Save(s);
        }
        public bool Save(Stream file)
        {
            bool status = false;

            try
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;
                settings.Indent = true;
                using (XmlWriter xw = XmlWriter.Create(file, settings))
                {
                    assayNode.Save(xw);
                } 

                status = true;
            }
            catch
            {
                Debug.WriteLine("WARNING: failed to save assay!");
            }

            return status;
        }


        // Classify and stop functions //////////////////////////////////////////////////////////////////////////

        static private bool IsNumeric(string s, bool onlyIntegersAllowed = false)
        {
            if (onlyIntegersAllowed)
            {
                int result;
                return int.TryParse(s, out result);
            }
            else
            {
                // Note that floating-point values are stored in the assay in the invariant culture
                // (even if the current culture uses commas as the decimal separator, e.g. French)
                // and because the values entered in the user interface are currently copied directly to
                // the assay as strings (if this function validates them as numbers), they must be entered
                // in the user interface in a culture-invariant way (i.e. with a dot as the decimal separator).
                // For the UI to use the user's culture, numbers from the UI would have to be parsed in their
                // culture into numeric values then converted back to strings using the invariant culture.
                double result;
                return double.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out result);
            }
        }

        static private bool ParseArrayAccess(string rawString, out string arrayName, out int arrayIndex, out string remainingString)
        {
            bool status = false;

            arrayName = String.Empty;
            arrayIndex = -1;
            remainingString = String.Empty;

            int openingBraceIndex = rawString.IndexOf('(');
            int closingBraceIndex = rawString.IndexOf(')');

            if (openingBraceIndex > 0 && closingBraceIndex - openingBraceIndex > 1)
            {
                arrayName = rawString.Substring(0, openingBraceIndex).Trim();
                if (arrayName.Length > 0)
                {
                    status = Int32.TryParse(rawString.Substring(openingBraceIndex + 1, closingBraceIndex - openingBraceIndex - 1), out arrayIndex);
                    if (status)
                        remainingString = rawString.Substring(closingBraceIndex + 1, rawString.Length - (closingBraceIndex + 1));
                }
            }

            return status;
        }


        static private string ConvertFormulaForDisplay1(string functionFormula)
        {
            string displayFormula = functionFormula.Trim();

            // ">=" or "<=" are shown as "*". Not sure how "<=" would get into an assay, as "*" is written back as ">= 0",
            // but this replicates what the old implementation did.
            if (displayFormula.Length > 1 && (displayFormula.Substring(0, 2).Equals(">=") || displayFormula.Substring(0, 2).Equals("<=")))
                displayFormula = "*";

            // Any equals sign is not shown.
            displayFormula = displayFormula.Trim(new char[] { '=', ' ' });

            return displayFormula;
        }

        static public Dictionary<string, List<ClassFormulae>> ParseClassifyFunction1(string functionString, int numProbes, int numFusions)
        {
            // Create a lookup table where the key is the class name. As there may be multiple classes with the same name,
            // there is a list of ClassFormulae for each class name. Each ClassFormulae contains a list of formulae for
            // each probe and a list of formulae for each fusion. The formula for a probe or fusion in each list is looked
            // up by index - e.g. the index for a probe comes from its param set, e.g. FLUOROCHROMENAMES3 gives the name
            // of the probe with index 3.
            Dictionary<string, List<ClassFormulae>> classesFormulae = new Dictionary<string, List<ClassFormulae>>();

            if (functionString != null)
            {
                functionString = functionString.Replace('\n', ' ');

                // First extract strings like this: If ((NProbes(0) = 2) And (NProbes(1) = 2)) Then Result = "2g2r"
                string[] classSplitStrings = new string[] { "SpotClassify(ByRef NProbes(), ByVal NF, ByRef Result)",
                                                            "Result = \"Uninformative\"",
                                                            "Sub", "Exit Sub", "End Sub", "If", "End If" };
                string[] perClassFormulae = functionString.Split(classSplitStrings, StringSplitOptions.RemoveEmptyEntries);
                foreach (var cf in perClassFormulae)
                {
                    string classFunction = cf.Trim();
                    if (classFunction.Length <= 0)
                        continue;

                    // Now split each string into the class name and the class formula.
                    string[] classExtractStrings = new string[] { "Then", "Result =", "\"" };
                    string[] classNameStrings = classFunction.Split(classExtractStrings, StringSplitOptions.RemoveEmptyEntries);
                    if (classNameStrings.Length > 0)
                    {
                        string className = classNameStrings[classNameStrings.Length - 1].Trim();
                        string classFormula = classNameStrings[0].Trim();

                        // Now extract the per-probeOrFusion formulae for this class - these are always separated by ") And ("
                        // - note that an 'Or' is done by having multiple classes with the same name.
                        classFormula = classFormula.Trim(new char[] { '(', ')' });
                        string[] formulaeExtractStrings = new string[] { ") And (" };
                        string[] formulaeStrings = classFormula.Split(formulaeExtractStrings, StringSplitOptions.RemoveEmptyEntries);


                        ClassFormulae formulae = new ClassFormulae(numProbes, 0, numFusions);

                        foreach (var f in formulaeStrings)
                        {
                            int openingBraceIndex = f.IndexOf('(');
                            int closingBraceIndex = f.IndexOf(')');
                            if (   f.Length > 7 && f.Substring(0, 7).Equals("NProbes")
                                && openingBraceIndex >= 0 && closingBraceIndex - openingBraceIndex > 1)
                            {
                                int probeOrFusionIndex = -1;
                                bool indexParseOk = Int32.TryParse(f.Substring(openingBraceIndex + 1, closingBraceIndex - openingBraceIndex - 1), out probeOrFusionIndex);

                                if (indexParseOk && probeOrFusionIndex >= 0 && probeOrFusionIndex < numProbes)
                                {
                                    string currFormula = f.Substring(closingBraceIndex + 1, f.Length - (closingBraceIndex + 1));

                                    formulae.probeFormulae[probeOrFusionIndex] = ConvertFormulaForDisplay1(currFormula);
                                }
                            }
                            else if (f.Length > 3 && f.Substring(0, 3).Equals("NF ")) // Old-style fusion
                            {
                                string currFormula = f.Substring(3);

                                formulae.fusionFormulae[0] = ConvertFormulaForDisplay1(currFormula);
                            }
                        }

                        List<ClassFormulae> formulaeForThisClassName;
                        if (classesFormulae.ContainsKey(className))
                            formulaeForThisClassName = classesFormulae[className];
                        else
                        {
                            formulaeForThisClassName = new List<ClassFormulae>();
                            classesFormulae.Add(className, formulaeForThisClassName);
                        }

                        formulaeForThisClassName.Add(formulae);
                    }
                }
            }

            return classesFormulae;
        }


        static private string ConvertFormulaForDisplay2(string functionFormula)
        {
            string displayFormula = functionFormula.Trim();

            // ">= 0" or ">= 0.0" becomes "*"
            // Note that floating-point values are stored in the assay in the invariant culture
            // (even if the current culture uses commas as the decimal separator, e.g. French).
            if (   displayFormula.Length > 1 && displayFormula.Substring(0, 2).Equals(">=")
                && IsNumeric(displayFormula.Substring(2)) && double.Parse(displayFormula.Substring(2), CultureInfo.InvariantCulture) == 0.0)
                displayFormula = "*";

            // Any leading equals sign is not shown.
            displayFormula = displayFormula.Trim(new char[] { '=', ' ' });

            return displayFormula;
        }

        static public Dictionary<string, List<ClassFormulae>> ParseClassifyFunction2(string functionString, int numProbes, int numRatios, int numFusions)
        {
            // Create a lookup table where the key is the class name. As there may be multiple classes with the same name,
            // there is a list of ClassFormulae for each class name. Each ClassFormulae contains a list of formulae for
            // each probe and a list of formulae for each fusion. The formula for a probe or fusion in each list is looked
            // up by index - e.g. the index for a probe comes from its param set, e.g. FLUOROCHROMENAMES3 gives the name
            // of the probe with index 3.
            Dictionary<string, List<ClassFormulae>> classesFormulae = new Dictionary<string, List<ClassFormulae>>();

            if (functionString != null)
            {
                functionString = functionString.Replace('\n', ' ');

                // First extract strings like this: If ((amp0 >= 2.0 AND amp0 <= 3.5) And (amp1 > 1.0)) Then result = "class1"
                string[] classSplitStrings = new string[] { "Sub SpotClassify2(ByVal nProbes(), ByVal ratios(), ByVal nFusions(), ByRef result)",
                                                            "result = \"Uninformative\"",
                                                            "Sub", "Exit Sub", "End Sub", "If", "End If" };
                string[] perClassFormulae = functionString.Split(classSplitStrings, StringSplitOptions.RemoveEmptyEntries);
                foreach (var cf in perClassFormulae)
                {
                    string classFunction = cf.Trim();
                    if (classFunction.Length <= 1)
                        continue;

                    // Now split each string into the class name and the class formula.
                    string[] classExtractStrings = new string[] { "Then", "result =", "\"" };
                    string[] classNameStrings = classFunction.Split(classExtractStrings, StringSplitOptions.RemoveEmptyEntries);
                    if (classNameStrings.Length > 0)
                    {
                        string className = classNameStrings[classNameStrings.Length - 1].Trim();
                        string classFormula = classNameStrings[0].Trim();

                        // Now extract the per-probe (or ratio or fusion) formulae for this class - these are always separated by ") And ("
                        // - note that an 'Or' is done by having multiple classes with the same name.
                        classFormula = classFormula.Trim(new char[] { '(', ')' });
                        string[] formulaeExtractStrings = new string[] { ") And (" };
                        string[] formulaeStrings = classFormula.Split(formulaeExtractStrings, StringSplitOptions.RemoveEmptyEntries);


                        ClassFormulae formulae = new ClassFormulae(numProbes, numRatios, numFusions);

                        foreach (var f in formulaeStrings)
                        {
                            string arrayName;
                            int arrayIndex;
                            string remainingString;
                            if (ParseArrayAccess(f, out arrayName, out arrayIndex, out remainingString) && arrayIndex >= 0)
                            {
                                string currFormula = remainingString.Trim();
                                string currFormula2 = String.Empty;

                                int indexOfAnd = remainingString.IndexOf("AND");
                                if (indexOfAnd > 0)
                                {
                                    currFormula = remainingString.Substring(0, indexOfAnd);

                                    remainingString = remainingString.Substring(indexOfAnd + "AND".Length);
                                    string arrayName2;
                                    int arrayIndex2;
                                    string remainingString2;
                                    if (   ParseArrayAccess(remainingString, out arrayName2, out arrayIndex2, out remainingString2)
                                        && arrayName2.Equals(arrayName) && arrayIndex2 == arrayIndex)
                                    {
                                        currFormula2 = remainingString2;
                                    }
                                }

                                currFormula = ConvertFormulaForDisplay2(currFormula);

                                // The '&' symbol is used in the display to represent an AND of two expressions.
                                if (currFormula2.Length > 0)
                                    currFormula += " & " + ConvertFormulaForDisplay2(currFormula2);


                                if (arrayName.Equals("nProbes") && arrayIndex < numProbes)
                                    formulae.probeFormulae[arrayIndex] = currFormula;
                                else if (arrayName.Equals("ratios") && arrayIndex < numRatios)
                                    formulae.ratioFormulae[arrayIndex] = currFormula;
                                else if (arrayName.Equals("nFusions") && arrayIndex < numFusions)
                                    formulae.fusionFormulae[arrayIndex] = currFormula;
                            }
                        }

                        List<ClassFormulae> formulaeForThisClassName;
                        if (classesFormulae.ContainsKey(className))
                            formulaeForThisClassName = classesFormulae[className];
                        else
                        {
                            formulaeForThisClassName = new List<ClassFormulae>();
                            classesFormulae.Add(className, formulaeForThisClassName);
                        }

                        formulaeForThisClassName.Add(formulae);
                    }
                }
            }

            return classesFormulae;
        }


        static private string PrepareFormulaForFunction1(string rawFormula)
        {
            // Takes a string of the form e.g.: "", or "2", or "*", or "< 3" (from the GUI) and 
            // turns it into "= 0", or "= 2", or ">= 0", "< 3" respectively.
            string preparedFormula = rawFormula.Trim();
            string formulaIfRawFormulaFailedValidation = "= -1"; // Should prevent this class being assigned to anything.

            if (preparedFormula.Length > 0)
            {
                if (IsNumeric(preparedFormula))
                    preparedFormula = "= " + preparedFormula;
                else if (preparedFormula[0].Equals('*'))
                    preparedFormula = ">= 0";
                else if (preparedFormula[0].Equals('<') || preparedFormula[0].Equals('>'))
                {
                    // If the symbol is followed by a numeric value, it is valid as it is.
                    // Note that >= or <= are not valid in this version of the classify function.
                    if (!IsNumeric(preparedFormula.Substring(1)))
                        preparedFormula = formulaIfRawFormulaFailedValidation;
                }
                else
                    preparedFormula = formulaIfRawFormulaFailedValidation;
            }
            else
                preparedFormula = "= 0";

            return preparedFormula;
        }

        static public string CreateClassifyFunction1(List<string> classNames, List<string[]> classesProbeFormulae, List<string[]> classesFusionFormulae)
        {
            // This function is compatible with the spot analysis script used by CytoVision v7.2 and earlier.
            // e.g.: 
            //  Sub SpotClassify(ByRef NProbes(), ByVal NF, ByRef Result)
            //      Result = "Uninformative"
            //      If ((NProbes(0) = 2) And (NProbes(1) = 2) And (NF = 2)) Then
            //          Result = "2R2G2F"
            //          Exit Sub
            //      End If
            //      If ((NProbes(0) = 1) And (NProbes(1) = 1) And (NF = 1)) Then
            //          Result = "1R1G1F"
            //          Exit Sub
            //      End If
            //  End Sub
            //
            string function = String.Empty;

            function += "\r\n";
            function += "Sub SpotClassify(ByRef NProbes(), ByVal NF, ByRef Result)\r\n";
            function += "    Result = \"Uninformative\"\r\n";

            if (classNames != null && classNames.Count > 0 && classesProbeFormulae.Count == classNames.Count && classesFusionFormulae.Count == classNames.Count)
            {
                for (int classIndex = 0; classIndex < classNames.Count; classIndex++)
                {
                    string cn = classNames[classIndex];
                    string[] probeFormulae = classesProbeFormulae[classIndex];
                    string[] fusionFormulae = classesFusionFormulae[classIndex];

                    if (probeFormulae.Length > 0)
                    {
                        function += "    If (";

                        int numFormulaAdded = 0; // For this class.
                        for (int probeIndex = 0; probeIndex < probeFormulae.Length; probeIndex++)
                        {
                            string formula = PrepareFormulaForFunction1(probeFormulae[probeIndex]);
                            if (formula.Length > 0)
                            {
                                if (numFormulaAdded > 0) function += " And ";
                                function += "(NProbes(" + probeIndex + ") " + formula + ")";
                                numFormulaAdded++;
                            }
                        }

                        if (fusionFormulae.Length > 0)
                        {
                            // Can only save one fusion in this version of the classify function - choose the first one.
                            string formula = PrepareFormulaForFunction1(fusionFormulae[0]);
                            if (formula.Length > 0)
                            {
                                if (numFormulaAdded > 0) function += " And ";
                                function += "(NF " + formula + ")";
                                numFormulaAdded++;
                            }
                        }

                        function += ") Then\r\n";
                        function += "        Result = \"" + cn + "\"\r\n";
                        function += "        Exit Sub\r\n";
                        function += "    End If\r\n";
                    }
                }
            }

            function += "End Sub\r\n";

            return function;
        }


        static public int ExtractDefaultCountFromFormula(string formula)
        {
            // This function extracts the numeric part of a formula, if any, otherwise returns zero.
            int defaultCount = 0;

            formula = PrepareFormulaForFunction1(formula); // Use PrepareFormulaForFunction, so that invalid data is determined consistently.

            formula = formula.Trim(new char[] { '=', '<', '>', '*' });

            int result;
            if (int.TryParse(formula, out result) && result > 0)
                defaultCount = result;

            return defaultCount;
        }

        static public bool IsFormulaValidForDisplay(string formula, bool onlyIntegersAllowed)
        {
            return !PrepareFormulaForFunction2(formula, "whatever", onlyIntegersAllowed).Equals("whatever = -1");
        }


        static private bool ValidateComparison2(string comparison, bool onlyIntegersAllowed)
        {
            // Comparison must start with a '>' or '<' (after trimming whitespace).
            comparison = comparison.Trim();
            if (   comparison != null && comparison.Length > 1
                && (comparison[0].Equals('>') || comparison[0].Equals('<')) )
            {
                comparison = comparison.Substring(1); // Eat the '<' or '>'.

                // Comparison can now optionally have an equals sign in the next position.
                if (comparison[0].Equals('='))
                    comparison = comparison.Substring(1); // Eat the '='.
                    
                // Only a number should now follow.
                if (IsNumeric(comparison, onlyIntegersAllowed))
                    return true;
            }

            return false;
        }

        static private string PrepareFormulaForFunction2(string rawFormula, string varName, bool onlyIntegersAllowed)
        {
            // Takes a string of the form e.g.: "> 1.0 & < 3.0" or "< 2.0", or "> 2.0" (from the GUI) and 
            // turns it into e.g. "ratio(0) > 1.0 AND ratio(0) < 3.0" or "ratio(0) < 2.0", or "ratio(0) > 2.0", respectively.
            const string formulaIfRawFormulaFailedValidation = "= -1"; // Should prevent this class being assigned to anything.
            string preparedFormula = formulaIfRawFormulaFailedValidation;

            rawFormula = rawFormula.Trim();

            preparedFormula = varName + " ";

            if (rawFormula.Length > 0)
            {
                if (IsNumeric(rawFormula, onlyIntegersAllowed))
                {
                    preparedFormula += "= " + rawFormula;
                }
                else if (rawFormula[0].Equals('*'))
                {
                    if (rawFormula.Length > 1)
                        preparedFormula += formulaIfRawFormulaFailedValidation; // Wildcard should be on its own.
                    else
                        preparedFormula += onlyIntegersAllowed ? ">= 0" : ">= 0.0";
                }
                else
                {
                    int indexOfAnd = rawFormula.IndexOf("&");
                    int lengthOfFirstComparison = indexOfAnd > 0 ? indexOfAnd : rawFormula.Length;
                    string firstComparison = rawFormula.Substring(0, lengthOfFirstComparison).Trim();

                    if (ValidateComparison2(firstComparison, onlyIntegersAllowed))
                    {
                        if (indexOfAnd > 0)
                        {
                            string secondComparison = rawFormula.Substring(lengthOfFirstComparison + "&".Length).Trim();
                            if (ValidateComparison2(secondComparison, onlyIntegersAllowed))
                            {
                                preparedFormula += firstComparison + " AND " + varName + " " + secondComparison;
                            }
                            else
                                preparedFormula += formulaIfRawFormulaFailedValidation;
                        }
                        else
                            preparedFormula += firstComparison;
                    }
                    else
                        preparedFormula += formulaIfRawFormulaFailedValidation;
                }
            }
            else
                preparedFormula += onlyIntegersAllowed ? "= 0" : "= 0.0";

            return preparedFormula;
        }

        static public string CreateClassifyFunction2(List<string> classNames,
                                                     List<string[]> classesProbeFormulae,
                                                     List<string[]> classesRatioFormulae, 
                                                     List<string[]> classesFusionFormulae)
        {
            // This function was created for the spot analysis script used by CytoVision v7.3.
            // It handles ratios in addition to counts and fusions.
            // e.g.:
            //
            //  Sub SpotClassify2(ByVal nProbes(), ByVal ratios(), ByVal nFusions(), ByRef result)
            //      result = "Uninformative"
            //      If ((nProbes(0) = 2) And (nProbes(1) = 2) And (nFusions(0) = 2)) Then
            //          result = "2R2G2F"
            //          Exit Sub
            //      End If
            //      If ((nProbes(0) = 1) And (nProbes(1) = 1) And (nFusions(0) = 1)) Then
            //          result = "1R1G1F"
            //          Exit Sub
            //      End If
            //      If ((ratios(0) >= 1.5 AND ratios(0) <= 2.5) And (ratios(1) > 2.0)) Then
            //          result = "class1"
            //          Exit Sub
            //      End If
            //      If ((ratios(0) >= 2.5) And (ratios(1) < 2.0)) Then
            //          result = "class2"
            //          Exit Sub
            //      End If
            //  End Sub
            //
            string function = String.Empty;

            function += "\r\n";
            function += "Sub SpotClassify2(ByVal nProbes(), ByVal ratios(), ByVal nFusions(), ByRef result)\r\n";
            function += "    result = \"Uninformative\"\r\n";

            if (   classNames != null && classNames.Count > 0
                && classesProbeFormulae.Count == classNames.Count
                && classesRatioFormulae.Count == classNames.Count
                && classesFusionFormulae.Count == classNames.Count)
            {
                for (int classIndex = 0; classIndex < classNames.Count; classIndex++)
                {
                    string cn = classNames[classIndex];
                    string[] probeFormulae = classesProbeFormulae[classIndex];
                    string[] ratioFormulae = classesRatioFormulae[classIndex];
                    string[] fusionFormulae = classesFusionFormulae[classIndex];

                    if (probeFormulae.Length > 0 || ratioFormulae.Length > 0)
                    {
                        function += "    If (";

                        int numFormulaAdded = 0; // For this class.

                        for (int probeIndex = 0; probeIndex < probeFormulae.Length; probeIndex++)
                        {
                            string formula = PrepareFormulaForFunction2(probeFormulae[probeIndex], "nProbes(" + probeIndex + ")", true);
                            if (formula.Length > 0)
                            {
                                if (numFormulaAdded > 0) function += " And ";
                                function += "(" + formula + ")";
                                numFormulaAdded++;
                            }
                        }

                        for (int ratioIndex = 0; ratioIndex < ratioFormulae.Length; ratioIndex++)
                        {
                            string formula = PrepareFormulaForFunction2(ratioFormulae[ratioIndex], "ratios(" + ratioIndex + ")", false);
                            if (formula.Length > 0)
                            {
                                if (numFormulaAdded > 0) function += " And ";
                                function += "(" + formula + ")";
                                numFormulaAdded++;
                            }
                        }

                        for (int fusionIndex = 0; fusionIndex < fusionFormulae.Length; fusionIndex++)
                        {
                            string formula = PrepareFormulaForFunction2(fusionFormulae[fusionIndex], "nFusions(" + fusionIndex + ")", true);
                            if (formula.Length > 0)
                            {
                                if (numFormulaAdded > 0) function += " And ";
                                function += "(" + formula + ")";
                                numFormulaAdded++;
                            }
                        }

                        function += ") Then\r\n";
                        function += "        result = \"" + cn + "\"\r\n";
                        function += "        Exit Sub\r\n";
                        function += "    End If\r\n";
                    }
                }
            }

            function += "End Sub\r\n";

            return function;
        }


        static public string CreateStopFunction(List <string> classNames)
        {
	        // This function will reset the counts once a stop count has been
	        // reached. Thus scanning can continue with another scan area

            string resetFunction = String.Empty;
            resetFunction += "\r\n";
	        resetFunction += "Sub ResetCounts\r\n";
	        resetFunction += "Dim V\r\n";
	        resetFunction += "V = CLng(0)\r\n";
            // Reset values
            resetFunction += "SetPersistent \"UNINFORMATIVECELLCOUNT\", V\r\n";
            foreach (var name in classNames)
                resetFunction += "SetPersistent \"" + name + "\", V\r\n";
            resetFunction += "SetPersistent \"ABNORMALCELLCOUNT\", V\r\n";
            resetFunction += "SetPersistent \"TOTALCOUNT\", V\r\n";
	        resetFunction += "DeleteClassMonitor\r\n";
            // DEBUG  ResetVarsFunc += "DebugString \"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&RESETTING VARIABLES\"\r\n";
            resetFunction += "End Sub\r\n\r\n";


            string stopFunction = String.Empty;
            stopFunction += "Function StopAutoCapture(ByVal ClassName)\r\n";
            stopFunction += "Dim StopCapture\r\n";
            stopFunction += "StopCapture = False\r\n\r\n";

	        stopFunction += "StopAutoCapture = FALSE\r\n\r\n";

            // DEBUG LINE  stopFunction += "DebugString ClassName\r\n";
            stopFunction += "If (ClassName = \"Uninformative\") Then\r\n";
	        stopFunction += "   Dim UninformativeCount, UninformativeTotalCount\r\n";
            stopFunction += "   InitLongPersistent \"UNINFORMATIVECELLCOUNT\", 0\r\n";
            stopFunction += "   InitLongPersistent \"UNINFORMATIVECELLCOUNTTOTALCOUNT\", 0\r\n\r\n";

	        // Deal with the uninformative cases
            stopFunction += "   GetPersistent \"UNINFORMATIVECELLCOUNT\", UninformativeCount\r\n";

            stopFunction += "   If (UninformativeCount > 0) Then\r\n";
            stopFunction += "      GetPersistent \"UNINFORMATIVECELLCOUNTTOTALCOUNT\", UninformativeTotalCount\r\n";
            stopFunction += "      UninformativeTotalCount = UninformativeTotalCount + 1\r\n";
            stopFunction += "      SetPersistent \"UNINFORMATIVECELLCOUNTTOTALCOUNT\", UninformativeTotalCount\r\n";
            stopFunction += "      If (UninformativeTotalCount >= UninformativeCount) Then\r\n";
            stopFunction += "         StopCapture = True\r\n";
            stopFunction += "      End If\r\n";
            stopFunction += "   End If\r\n\r\n";

            stopFunction += "   StopAutoCapture = StopCapture\r\n";
            stopFunction += "   Exit Function\r\n";     // Exit the function here for the uninformative case
            stopFunction += "End If\r\n\r\n";

            // Now add some code to initialise the class counts to zero,
            // even though the Spot Configuration UI will set them to the values the user enters.
            foreach (var name in classNames)
            {
		        // StopAutocapture var initialisation
                stopFunction += "InitLongPersistent \"" + name + "\", 0\r\n";
                stopFunction += "InitLongPersistent \"" + name + "TOTALCOUNT\", 0\r\n";
            }

            // Now do the same for the abnormal and overall total cell counts
            stopFunction += "InitLongPersistent \"ABNORMALCELLCOUNT\", 0\r\n";
            stopFunction += "InitLongPersistent \"ABNORMALCELLCOUNTTOTALCOUNT\", 0\r\n";
            stopFunction += "InitLongPersistent \"TOTALCELLCOUNT\", 0\r\n";
            stopFunction += "InitLongPersistent \"TOTALCOUNT\", 0\r\n";

            // Init the normal class name as a null string
            stopFunction += "InitStringPersistent \"NORMALCLASSNAME\", \"\"\r\n";
            stopFunction += "\r\n";

            // Now add some variables
            stopFunction += "Dim TotalCellCount, TotalSoFar\r\n";

            // Now some code to increment total so far
            stopFunction += "GetPersistent \"TOTALCOUNT\", TotalSoFar\r\n"; 
            stopFunction += "TotalSoFar = TotalSoFar + 1\r\n";
            stopFunction += "SetPersistent \"TOTALCOUNT\", TotalSoFar\r\n\r\n";

            // Now add the code to check for exceeding the overall total cell count
            stopFunction += "GetPersistent \"TOTALCELLCOUNT\", TotalCellCount\r\n";

            // DEBUG  stopFunction += "DebugString \"TOTAL CELL COUNT = \" & TotalCellCount & \" TotalSoFar = \" & TotalSoFar\r\n";
            stopFunction += "If (TotalCellCount > 0 And TotalSoFar >= TotalCellCount) Then\r\n";
            stopFunction += "   StopCapture = True\r\n";
            stopFunction += "End If\r\n\r\n";    

            // Now create some more vars
            stopFunction += "Dim CountVarName, TotalClassCountSoFar, TotalClassCount, NormalClassName\r\n";
            stopFunction += "Dim AbnormalCount, AbnormalTotalCount, PID\r\n\r\n";

            stopFunction += "PID = GetCurrentProcessID\r\n";
            stopFunction += "CountVarName = ClassName + \"TOTALCOUNT\"\r\n";

            // Add some code to increment the class count for this class
            stopFunction += "GetPersistent CountVarName, TotalClassCountSoFar\r\n";
            stopFunction += "TotalClassCountSoFar = TotalClassCountSoFar + 1\r\n";
            stopFunction += "SetPersistent CountVarName, TotalClassCountSoFar\r\n\r\n";

            // Get the threshold limit for this class
            stopFunction += "GetPersistent ClassName, TotalClassCount\r\n\r\n";

            // DEBUG  stopFunction += "DebugString \"CLASSNAME = \" & ClassName & \" TOTALCLASSCOUNT = \" & TotalClassCount\r\n";
            // DEBUG  stopFunction += "DebugString \"TOTALCOUNTSOFAR = \" & TotalClassCountSoFar\r\n";

            // Store counts information so that the progress dialog can pick it up
            stopFunction += "SetRegItem \"Software\\Applied Imaging\\ClassCounts\" & PID, ClassName, TotalClassCountSoFar & \",\" & TotalClassCount\r\n\r\n";

            // Add code to check to see whether the count for this class has been exceeded
            stopFunction += "If (TotalClassCount > 0 And TotalClassCountSoFar >= TotalClassCount) Then\r\n";
            stopFunction += "   StopCapture = True\r\n";
            stopFunction += "End If\r\n\r\n";

            // Now deal with the abnormal cases
            stopFunction += "GetPersistent \"NORMALCLASSNAME\", NormalClassName\r\n";
            stopFunction += "GetPersistent \"ABNORMALCELLCOUNT\", AbnormalCount\r\n";
            stopFunction += "If (AbnormalCount > 0 And ClassName <> NormalClassName) Then\r\n";
            stopFunction += "   GetPersistent \"ABNORMALCELLCOUNTTOTALCOUNT\", AbnormalTotalCount\r\n";
            stopFunction += "   AbnormalTotalCount = AbnormalTotalCount + 1\r\n";
            stopFunction += "   SetPersistent \"ABNORMALCELLCOUNTTOTALCOUNT\", AbnormalTotalCount\r\n";
            stopFunction += "   If (AbnormalTotalCount >= AbnormalCount) Then\r\n";
            stopFunction += "       StopCapture = True\r\n";
            stopFunction += "   End If\r\n";
	        stopFunction += "End If\r\n\r\n";

	        stopFunction += "StopAutoCapture = StopCapture\r\n";
            stopFunction += "End Function\r\n\r\n";

            return resetFunction + stopFunction;
        }
    }
}
