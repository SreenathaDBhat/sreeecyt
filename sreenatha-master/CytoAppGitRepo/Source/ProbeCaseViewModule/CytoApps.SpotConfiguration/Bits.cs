﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.IO;

using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using System.Collections.ObjectModel;
using System.Windows.Media;
using CytoApps.Infrastructure.UI.Utilities;

/*
 * This file is a bit of a city dump.
 * All of these classes used to be in SpotConfigController.cs.
 * JR got sick of getting lost in the file so dumped everything other
 * than the actual SpotConfigController class here.
 */

namespace CytoApps.SpotConfiguration
{
    public class CVPaths
    {
        public static CVPaths Standard
        {
            get
            {
                //CVPaths p = new CVPaths(Locations.AII_Shared.FullName);
                //return p;
                throw new NotImplementedException();
            }
        }

        public CVPaths()
        {
            string appPath = Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

            //AIISharedPath = aiishared;
            FluolistFilePath = Path.Combine(appPath, "defaults", "fluolist");
            SpectrachromesFilePath = Path.Combine(appPath, "defaults", "spectrachromes.def");
        }

        //public string AIISharedPath
        //{
        //    get;
        //    private set;
        //}

        public string FluolistFilePath
        {
            get;
            private set;
        }
        public string SpectrachromesFilePath
        {
            get;
            private set;
        }

        public IEnumerable<string> GetCellClassifierNames()
        {
            ObservableCollection<string> names = new ObservableCollection<string>();

            // There is always an 'Everything' option - there is no corresponding 'Everything'
            // classifier file, it just means let everything through the cell classification step.
            names.Add("Everything");

            if (Directory.Exists(Path.Combine(@"\\localhost\Casebase\cases\", @"..\aii_shared")))
            {
                string spotCellPath = Path.Combine(Path.Combine(@"\\localhost\Casebase\cases\", @"..\aii_shared"), "Classifiers", "SpotCell");

                if (Directory.Exists(spotCellPath))
                {
                    DirectoryInfo spotCellPathInfo = new DirectoryInfo(spotCellPath);
                    foreach (FileInfo modelFile in spotCellPathInfo.GetFiles("*.model"))
                    {
                        names.Add(Path.GetFileNameWithoutExtension(modelFile.Name));
                    }
                }
            }

            // Sort alphabetically by slide name, to match the order in the CytoVision navigator.
            return new ObservableCollection<string>(names.OrderBy(s => s));
            throw new NotImplementedException();
        }
    }


    public static class Constants
    {
        // Maximum values defined here are limited to what the analysis script and ProbeCaseView app can handle.
        public const int MAXPROBES = 6;
        public const int MAXRATIOS = 1;
        public const int MAXFUSIONS = 1;
        public const int MAXPROBESPERFUSION = 2;
        public const int MAXCLASSES = 20;
    }


    public class SixtyFourBit
    {
        static public bool IsWow64Process()
        {
            // Detect whether the current process is a 32-bit process running on a 64-bit system.
            bool flag;
            return (DoesWin32MethodExist("kernel32.dll", "IsWow64Process") && IsWow64Process(GetCurrentProcess(), out flag) && flag);
        }

        static public bool Is64BitProcess()
        {
            return IntPtr.Size != 8 ? false : true;
        }

        public static bool Is64BitOperatingSystem()
        {
            if (Is64BitProcess() || IsWow64Process())
                return true;
            else
                return false;
        }

        static bool DoesWin32MethodExist(string moduleName, string methodName)
        {
            IntPtr moduleHandle = GetModuleHandle(moduleName);
            if (moduleHandle == IntPtr.Zero)
                return false;

            return (GetProcAddress(moduleHandle, methodName) != IntPtr.Zero);
        }

        [DllImport("kernel32.dll")]
        static extern IntPtr GetCurrentProcess();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr GetModuleHandle(string moduleName);

        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr GetProcAddress(IntPtr hModule,
            [MarshalAs(UnmanagedType.LPStr)]string procName);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool IsWow64Process(IntPtr hProcess, out bool wow64Process);
    }


    public class AssayInfo
    {
        public AssayInfo() { }

        public static string SpotAssaysPath
        {
            get
            {
                throw new NotImplementedException();
                //    return Path.Combine(Locations.AII_Shared.FullName, "Assays", "Spot");
            }
        }

        public static string BuildPathForAssay(string assayName)
        {
            return Path.Combine(SpotAssaysPath, assayName + ".assay");
        }

        public static bool IsCEPXYAssayName(string assayName)
        {
            return assayName.Equals("CEP XY_BM Transplant", StringComparison.OrdinalIgnoreCase);
        }

        private static IEnumerable<string> GetFluorochromes(string path)
        {
            if (File.Exists(path))
            {
                var xml = XElement.Load(path);
                if (xml != null)
                {
                    var passes = (from sp in xml.Descendants("CScanPass") select sp).ToArray();
                    if (passes != null && passes.Count() > 1)
                    {
                        var pass = passes[1];
                        var fluoros = from f in pass.Descendants("CParamSet")
                                      where (f.Element("Name").Value == "COUNTERSTAIN"
                                                || f.Element("Name").Value.Contains("FLUOROCHROMENAMES"))
                                      orderby f.Element("Name").Value
                                      select f.Element("VtValue").Element("VarValue").Value;
                        return fluoros;
                    }
                }
            }

            return null;
        }

        public static IEnumerable<SpotClass> GetClassList(string path, out string assayName)
        {
            assayName = "";
            // Note: only populating with name and colour because thats all I need
            List<SpotClass> classes = new List<SpotClass>();

            if (File.Exists(path))
            {
                var xml = XElement.Load(path);
                if (xml != null)
                {
                    XElement assayNameNode = xml.Descendants("m_Name").FirstOrDefault();
                    if (assayNameNode != null)
                        assayName = assayNameNode.Value;

                    var passes = (from sp in xml.Descendants("CScanPass") select sp).ToArray();
                    if (passes != null && passes.Count() > 1)
                    {
                        var pass = passes[1];

                        XElement classesStoredNode = pass.Descendants("CParamSet").Where(x => x.Element("Name").Value == "CLASSESSTORED").FirstOrDefault();
                        if (classesStoredNode != null)
                        {
                            var cnt = int.Parse(classesStoredNode.Element("VtValue").Element("VarValue").Value);
                            for (int i = 0; i < cnt; i++)
                            {
                                XElement classNameNode = pass.Descendants("CParamSet").Where(x => x.Element("Name").Value == "CLASS" + i + "_NAME").FirstOrDefault();
                                if (classNameNode != null)
                                {
                                    var className = classNameNode.Element("VtValue").Element("VarValue").Value;
                                    var colourString = String.Empty;
                                    XElement classColourNode = pass.Descendants("CParamSet").Where(x => x.Element("Name").Value == "CLASS" + i + "_COLOUR").FirstOrDefault();
                                    if (classColourNode != null)
                                        colourString = classColourNode.Element("VtValue").Element("VarValue").Value;

                                    classes.Add(new SpotClass { Name = className, DisplayColour = ColorParse.HexStringToColor(colourString) });
                                }
                            }
                        }
                    }
                }
            }

            return classes;
        }

        public static bool IsCompatibleAssay(string path, CVFluoroChrome[] fluoros)
        {
            bool ret = false;
            var enumerableNames = GetFluorochromes(path);
            var fl = enumerableNames != null ? enumerableNames.ToArray() : null;

            // Assay has to have same number of fluorochromes, with same names (not case sensitive, as for manual capture
            // names come from a different list that may have different casing), but can be in different order.
            if (fl != null && fluoros != null && fluoros.Count() == fl.Count())
            {
                ret = true;
                foreach (var f in fluoros)
                {
                    if (fl.Where(a => a.Equals(f.Name, StringComparison.OrdinalIgnoreCase)).Count() != 1)
                        return false;
                }
            }

            return ret;
        }

        public static string GetInternalName(string assayFilePath)
        {
            // Get the internal name of the current assay. This should be the name of the default assay it was copied from.
            string internalName = String.Empty;
            if (File.Exists(assayFilePath))
            {
                var xml = XElement.Load(assayFilePath);
                if (xml != null)
                    internalName = xml.Element("m_Name").Value;
            }
            return internalName;
        }

        public static bool UpdateInternalName(string assayFilePath, string newName)
        {
            bool status = false;

            try
            {
                var xml = XElement.Load(assayFilePath);
                if (xml != null)
                {
                    XElement nameElement = xml.Element("m_Name");
                    if (nameElement != null)
                    {
                        if (!newName.Equals(nameElement.Value))
                        {
                            nameElement.Value = newName;

                            XmlWriterSettings settings = new XmlWriterSettings();
                            settings.OmitXmlDeclaration = true;
                            settings.Indent = true;
                            using (XmlWriter xw = XmlWriter.Create(assayFilePath, settings))
                            {
                                xml.Save(xw);
                            }
                        }

                        status = true;
                    }
                }
            }
            catch
            {

            }

            return status;
        }

        public static IEnumerable<string> EnumerateAllAssays()
        {
            var allAssays = Directory.EnumerateFiles(SpotAssaysPath, "*.assay");
            var names = (from a in allAssays
                         select Path.GetFileNameWithoutExtension(a)).ToList();
            return names;
        }

        public static IEnumerable<string> EnumerateCompatibleAssays(CVFluoroChrome[] fluoros)
        {
            var assaysPath = SpotAssaysPath;

            try
            {
                var allAssays = Directory.EnumerateFiles(assaysPath, "*.assay");

                List<string> compatible = new List<string>();
                foreach (var a in allAssays)
                {
                    if (IsCEPXYAssayName(Path.GetFileNameWithoutExtension(a)))
                        continue;

                    if (IsCompatibleAssay(a, fluoros))
                        compatible.Add(Path.GetFileNameWithoutExtension(a));
                }
                return compatible;
            }
            catch (IOException)
            {
                System.Diagnostics.Debug.WriteLine("Couldn't load assays from: " + assaysPath);
                return new string[0];
            }
        }

        public IEnumerable<string> CellClassifierNames { get; set; }
        public string[] SpotScript { get; set; }
        public string[] PreScanScript { get; set; }
        public string SpotScriptFilePath { get; set; }
        public string PreScanScriptFilePath { get; set; }
        public string DefaultAssayName { get; set; }
    }


    public class Probe : AI.Notifier
    {
        public string Name { get; set; }
        private int zPlanes;
        public int ZPlanes
        {
            get { return zPlanes; }
            set { zPlanes = value; Notify("ZPlanes"); }
        }
        private double zDistMicrons;
        public double ZDistMicrons
        {
            get { return zDistMicrons; }
            set { zDistMicrons = value; Notify("ZDistMicrons"); }
        }
        private Color pseudoColour;
        public Color PseudoColour
        {
            get { return pseudoColour; }
            set { pseudoColour = value; Notify("PseudoColour"); }
        }
        private bool isAmplified;
        public bool IsAmplified // Probes in amplified channels cannot easily be enumerated and require different image analysis.
        {
            get { return isAmplified; }
            set { isAmplified = value; Notify("IsAmplified"); }
        }

        public Probe()
        {
        }

        public Probe(CVFluoroChrome f)
        {
            Name = f.Name;
            PseudoColour = f.colour;
            ZPlanes = f.zStackNumImages;
            ZDistMicrons = f.zStackFocusDist / 100.0; // Convert to microns
        }
    }


    public class Ratio : AI.Notifier
    {
        public string Name { get; set; }

        public string NumeratorProbeName { get; set; }
        public string DenominatorProbeName { get; set; }

        public Ratio(string name, string numeratorProbe, string denominatorProbe)
        {
            Name = name;
            NumeratorProbeName = numeratorProbe;
            DenominatorProbeName = denominatorProbe;
        }
    }


    public class FusionComponent
    {
        public string ProbeName { get; set; }
        public bool IsSelected { get; set; }
    }

    public class Fusion : AI.Notifier
    {
        public string Name { get; set; }
        private Color colour;
        public Color Colour
        {
            get { return colour; }
            set { colour = value; Notify("Colour"); }
        }
        public ObservableCollection<FusionComponent> Components { get; set; }

        public int CountSelected()
        {
            int count = 0;
            foreach (var c in Components)
                if (c.IsSelected) count++;
            return count;
        }
        public void DeselectAll()
        {
            foreach (var c in Components)
                c.IsSelected = false;
        }

        public ObservableCollection<FusionComponent> SelectedComponents
        {
            get
            {
                var list = new ObservableCollection<FusionComponent>();
                foreach (var c in Components)
                {
                    if (c.IsSelected)
                        list.Add(c);
                }
                return list;
            }
        }

        public Fusion()
        {
            Components = new ObservableCollection<FusionComponent>();
        }
    }


    public class CountOrRatio : AI.Notifier // class needed? its a string. jr.
    {
        public CountOrRatio() { Formula = string.Empty; }
        public CountOrRatio(string f) { Formula = f; }

        private string _formula;
        public string Formula { get { return _formula; } set { _formula = value; Notify("Formula"); } }
    }

    public class SpotClass : AI.Notifier
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; Notify("Name"); }
        }
        private Color displayColour;
        public Color DisplayColour
        {
            get { return displayColour; }
            set { displayColour = value; Notify("DisplayColour"); }
        }
        private bool isNormal;
        public bool IsNormal
        {
            get { return isNormal; }
            set { isNormal = value; Notify("IsNormal"); }
        }
        public int StopCount { get; set; }
        public ObservableCollection<CountOrRatio> ProbeCounts { get; set; }
        public ObservableCollection<CountOrRatio> Ratios { get; set; }
        public ObservableCollection<CountOrRatio> FusionCounts { get; set; }

        public void SetProbeRatioFusionCounts(int p, int r, int f)
        {
            ProbeCounts.Clear();
            Ratios.Clear();
            FusionCounts.Clear();
            for (int i = 0; i < p; i++) ProbeCounts.Add(new CountOrRatio());
            for (int i = 0; i < r; i++) Ratios.Add(new CountOrRatio());
            for (int i = 0; i < f; i++) FusionCounts.Add(new CountOrRatio());
        }

        public SpotClass()
        {
            ProbeCounts = new ObservableCollection<CountOrRatio>();
            Ratios = new ObservableCollection<CountOrRatio>();
            FusionCounts = new ObservableCollection<CountOrRatio>();
        }
    }


    public class ScriptParam : AI.Notifier
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public int Default { get; set; }
        private int val;
        public int Value { get { return val; } set { val = value; Notify("Value"); } }

        public ScriptParam(string name, ParamSet ps)
        {
            Name = name;
            Description = ps.label;
            Min = StringToInt(ps.min, 0);
            Max = StringToInt(ps.max, Min);
            Default = StringToInt(ps.defValue, Min);
            Value = StringToInt(ps.value, Default);
        }

        private int StringToInt(string stringValue, int defaultValue)
        {
            int value;
            if (!Int32.TryParse(stringValue, out value))
                value = defaultValue;
            return value;
        }
    }

    public class ScriptParameters
    {
        public ObservableCollection<ScriptParam> Booleans { get; set; }
        public ObservableCollection<ScriptParam> Sliders { get; set; }

        public ScriptParameters(Dictionary<string, ParamSet> scanParameters = null)
        {
            Booleans = new ObservableCollection<ScriptParam>();
            Sliders = new ObservableCollection<ScriptParam>();
            if (scanParameters != null)
            {
                foreach (var p in scanParameters)
                {
                    if (p.Value.ctrlType.Equals("Button"))
                        Booleans.Add(new ScriptParam(p.Key, p.Value));
                    else if (p.Value.ctrlType.Equals("Slider"))
                        Sliders.Add(new ScriptParam(p.Key, p.Value));
                }
            }
        }

        public void SetToDefaults()
        {
            foreach (var b in Booleans)
                b.Value = b.Default;
            foreach (var s in Sliders)
                s.Value = s.Default;
        }

        public void AddToParamSets(Dictionary<string, ParamSet> paramSets)
        {
            foreach (var b in Booleans)
                paramSets.Add(b.Name, new ParamSet(b.Description, "Button", b.Value.ToString(), b.Min.ToString(), b.Max.ToString(), b.Default.ToString()));
            foreach (var s in Sliders)
                paramSets.Add(s.Name, new ParamSet(s.Description, "Slider", s.Value.ToString(), s.Min.ToString(), s.Max.ToString(), s.Default.ToString()));
        }
    }
}

